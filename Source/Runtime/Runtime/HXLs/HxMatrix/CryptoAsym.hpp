
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoAsym_HPP

#define INCLUDE_CryptoAsym_HPP

//////////////////////////////////////////////////////////////////////////
//
// Asymmetric Encryption Provider
//

class CCryptoAsym : public ICryptoAsym
{
	public:
		// Constructor
		CCryptoAsym(void);

		// Destructor
		virtual ~CCryptoAsym(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICryptoAsym
		UINT GetKeyBytes(void);
		BOOL Initialize(PCBYTE pKey, UINT uKey, CString const &Pass, UINT uFlags);
		BOOL Initialize(CByteArray const &Key, CString const &Pass, UINT uFlags);
		BOOL Encrypt(CByteArray &Out, CByteArray const &In);

	protected:
		// States
		enum
		{
			stateNew,
			stateActive,
			stateDone
			};

		// Data Members
		ULONG m_uRefs;
		UINT  m_uState;
		UINT  m_uKeyBits;
		UINT  m_uKeySize;
	};

// End of File

#endif
