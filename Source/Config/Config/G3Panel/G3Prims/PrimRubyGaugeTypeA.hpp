
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGagueTypeA_HPP
	
#define	INCLUDE_PrimRubyGagueTypeA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A Gauge Primitive
//

class CPrimRubyGaugeTypeA : public CPrimRubyGaugeBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeA(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT         m_GaugeStyle;
		CPrimColor * m_pColorFace;
		CPrimColor * m_pColorOuter;
		CPrimColor * m_pColorRing1;
		CPrimColor * m_pColorRing2;
		CPrimColor * m_pColorRing3;
		CPrimColor * m_pColorRing4;
		UINT	     m_ScaleBezel;

	protected:
		// Data Members
		CRubyPath     m_pathFace;
		CRubyPath     m_pathInner;
		CRubyPath     m_pathRing1;
		CRubyPath     m_pathRing2;
		CRubyPath     m_pathRing3;
		CRubyPath     m_pathRing4;
		CRubyGdiList  m_listFace;
		CRubyGdiList  m_listOuter;
		CRubyGdiList  m_listInner;
		CRubyGdiList  m_listRing1;
		CRubyGdiList  m_listRing2;
		CRubyGdiList  m_listRing3;
		CRubyGdiList  m_listRing4;
		number	      m_bezelBase;

		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void InitPaths(void);
		void MakeLists(void);

		// Color Schemes
		COLOR GetColor(UINT n);

		// Bezel Scaling
		number ScaleBezel(number n);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void FindBoundingRect(void);

		// Overridables
		CRubyPath & GetOuterPath(void);
	};

// End of File

#endif
