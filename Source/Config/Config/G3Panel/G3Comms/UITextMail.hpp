
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextMail_HPP

#define INCLUDE_UITextMail_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Mail Address
//

class CUITextMail : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextMail(void);

	protected:
		// Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnExpand(CWnd &Wnd);
	};

// End of File

#endif
