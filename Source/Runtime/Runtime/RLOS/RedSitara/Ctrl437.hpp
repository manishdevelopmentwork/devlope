
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Ctrl437_HPP
	
#define	INCLUDE_AM437_Ctrl437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Control Module
//

class CCtrl437
{
	public:
		// Constructor
		CCtrl437(void);

		// Device Control & Status
		DWORD GetRevsion(void) const;
		DWORD GetDeviceId(void) const;
		DWORD GetMaxFreq(void) const;
		DWORD GetRefFreq(void) const;
		DWORD GetOcrFreq(void) const;
		
		// DDR
		void EnableVtp(bool fEnable);
		void SetDdrAddrIO(UINT uClockS, UINT uClockI, UINT uCmdS, UINT uCmdI);
		void SetDdrAddrCtrl(DWORD dwMask);
		void SetDdrDataIO(UINT uClockS, UINT uClockI, UINT uDataS, UINT uDataI);
		void SetDdrIO(bool fResetEn);
		void SetDdrClockeEn(bool fEnable);

		// EMI
		void SetEmiPhyTraining(UINT uSamples, bool fLogic, bool fAllDqs);
		void SetEmiPhyConfig(UINT uDrive, bool fPhyEn1, bool fPhyEn0);

		// I/O Mux
		void  SetMux(UINT uPin, DWORD dwConfig);
		void  SetMux(PCDWORD pList, UINT uSize);
		DWORD GetMux(UINT uPin);

		// MII
		void SetMIIMode(UINT uPort, UINT uMode);

		// USB 
		void SetPhyPower(UINT uSelect, bool fOn);

		// PWM
		void SetPWM(UINT uModule, bool fEnable);

		// Mux Pins / Offsets
		enum
		{
			pinGPMCAD0		= 0x0800 / sizeof(DWORD),
			pinGPMCAD1		= 0x0804 / sizeof(DWORD),
			pinGPMCAD2		= 0x0808 / sizeof(DWORD),
			pinGPMCAD3		= 0x080C / sizeof(DWORD),
			pinGPMCAD4		= 0x0810 / sizeof(DWORD),
			pinGPMCAD5		= 0x0814 / sizeof(DWORD),
			pinGPMCAD6		= 0x0818 / sizeof(DWORD),
			pinGPMCAD7		= 0x081C / sizeof(DWORD),
			pinGPMCAD8		= 0x0820 / sizeof(DWORD),
			pinGPMCAD9		= 0x0824 / sizeof(DWORD),
			pinGPMCAD10		= 0x0828 / sizeof(DWORD),
			pinGPMCAD11		= 0x082C / sizeof(DWORD),
			pinGPMCAD12		= 0x0830 / sizeof(DWORD),
			pinGPMCAD13		= 0x0834 / sizeof(DWORD),
			pinGPMCAD14		= 0x0838 / sizeof(DWORD),
			pinGPMCAD15		= 0x083C / sizeof(DWORD),
			pinGPMCA0		= 0x0840 / sizeof(DWORD),
			pinGPMCA1		= 0x0844 / sizeof(DWORD),
			pinGPMCA2		= 0x0848 / sizeof(DWORD),
			pinGPMCA3		= 0x084C / sizeof(DWORD),
			pinGPMCA4		= 0x0850 / sizeof(DWORD),
			pinGPMCA5		= 0x0854 / sizeof(DWORD),
			pinGPMCA6		= 0x0858 / sizeof(DWORD),
			pinGPMCA7		= 0x085C / sizeof(DWORD),
			pinGPMCA8		= 0x0860 / sizeof(DWORD),
			pinGPMCA9		= 0x0864 / sizeof(DWORD),
			pinGPMCA10		= 0x0868 / sizeof(DWORD),
			pinGPMCA11		= 0x086C / sizeof(DWORD),
			pinGPMCWAIT0		= 0x0870 / sizeof(DWORD),
			pinGPMCWPN		= 0x0874 / sizeof(DWORD),
			pinGPMCBE1N		= 0x0878 / sizeof(DWORD),
			pinGPMCCSN0		= 0x087C / sizeof(DWORD),
			pinGPMCCSN1		= 0x0880 / sizeof(DWORD),
			pinGPMCCSN2		= 0x0884 / sizeof(DWORD),
			pinGPMCCSN3		= 0x0888 / sizeof(DWORD),
			pinGPMCCLK		= 0x088C / sizeof(DWORD),
			pinGPMCADVNALE		= 0x0890 / sizeof(DWORD),
			pinGPMCOENREN		= 0x0894 / sizeof(DWORD),
			pinGPMCWEN		= 0x0898 / sizeof(DWORD),
			pinGPMCBE0NCLE		= 0x089C / sizeof(DWORD),
			pinDSSDATA0		= 0x08A0 / sizeof(DWORD),
			pinDSSDATA1		= 0x08A4 / sizeof(DWORD),
			pinDSSDATA2		= 0x08A8 / sizeof(DWORD),
			pinDSSDATA3		= 0x08AC / sizeof(DWORD),
			pinDSSDATA4		= 0x08B0 / sizeof(DWORD),
			pinDSSDATA5		= 0x08B4 / sizeof(DWORD),
			pinDSSDATA6		= 0x08B8 / sizeof(DWORD),
			pinDSSDATA7		= 0x08BC / sizeof(DWORD),
			pinDSSDATA8		= 0x08C0 / sizeof(DWORD),
			pinDSSDATA9		= 0x08C4 / sizeof(DWORD),
			pinDSSDATA10		= 0x08C8 / sizeof(DWORD),
			pinDSSDATA11		= 0x08CC / sizeof(DWORD),
			pinDSSDATA12		= 0x08D0 / sizeof(DWORD),
			pinDSSDATA13		= 0x08D4 / sizeof(DWORD),
			pinDSSDATA14		= 0x08D8 / sizeof(DWORD),
			pinDSSDATA15		= 0x08DC / sizeof(DWORD),
			pinDSSVSYNC		= 0x08E0 / sizeof(DWORD),
			pinDSSHSYNC		= 0x08E4 / sizeof(DWORD),
			pinDSSPCLK		= 0x08E8 / sizeof(DWORD),
			pinDSSACBIASEN		= 0x08EC / sizeof(DWORD),
			pinMMC0DAT3		= 0x08F0 / sizeof(DWORD),
			pinMMC0DAT2		= 0x08F4 / sizeof(DWORD),
			pinMMC0DAT1		= 0x08F8 / sizeof(DWORD),
			pinMMC0DAT0		= 0x08FC / sizeof(DWORD),
			pinMMC0CLK		= 0x0900 / sizeof(DWORD),
			pinMMC0CMD		= 0x0904 / sizeof(DWORD),
			pinMII1COL		= 0x0908 / sizeof(DWORD),
			pinMII1CRS		= 0x090C / sizeof(DWORD),
			pinMII1RXERR		= 0x0910 / sizeof(DWORD),
			pinMII1TXEN		= 0x0914 / sizeof(DWORD),
			pinMII1RXDV		= 0x0918 / sizeof(DWORD),
			pinMII1TXD3		= 0x091C / sizeof(DWORD),
			pinMII1TXD2		= 0x0920 / sizeof(DWORD),
			pinMII1TXD1		= 0x0924 / sizeof(DWORD),
			pinMII1TXD0		= 0x0928 / sizeof(DWORD),
			pinMII1TXCLK		= 0x092C / sizeof(DWORD),
			pinMII1RXCLK		= 0x0930 / sizeof(DWORD),
			pinMII1RXD3		= 0x0934 / sizeof(DWORD),
			pinMII1RXD2		= 0x0938 / sizeof(DWORD),
			pinMII1RXD1		= 0x093C / sizeof(DWORD),
			pinMII1RXD0		= 0x0940 / sizeof(DWORD),
			pinMII1REFCLK		= 0x0944 / sizeof(DWORD),
			pinMDIODATA		= 0x0948 / sizeof(DWORD),
			pinMDIOCLK		= 0x094C / sizeof(DWORD),
			pinSPI0SCLK		= 0x0950 / sizeof(DWORD),
			pinSPI0D0		= 0x0954 / sizeof(DWORD),
			pinSPI0D1		= 0x0958 / sizeof(DWORD),
			pinSPI0CS0		= 0x095C / sizeof(DWORD),
			pinSPI0CS1		= 0x0960 / sizeof(DWORD),
			pinECAP0INPWM0OUT	= 0x0964 / sizeof(DWORD),
			pinUART0CTSN		= 0x0968 / sizeof(DWORD),
			pinUART0RTSN		= 0x096C / sizeof(DWORD),
			pinUART0RXD		= 0x0970 / sizeof(DWORD),
			pinUART0TXD		= 0x0974 / sizeof(DWORD),
			pinUART1CTSN		= 0x0978 / sizeof(DWORD),
			pinUART1RTSN		= 0x097C / sizeof(DWORD),
			pinUART1RXD		= 0x0980 / sizeof(DWORD),
			pinUART1TXD		= 0x0984 / sizeof(DWORD),
			pinI2C0SDA		= 0x0988 / sizeof(DWORD),
			pinI2C0SCL		= 0x098C / sizeof(DWORD),
			pinMCASP0ACLKX		= 0x0990 / sizeof(DWORD),
			pinMCASP0FSX		= 0x0994 / sizeof(DWORD),
			pinMCASP0AXR0		= 0x0998 / sizeof(DWORD),
			pinMCASP0AHCLKR		= 0x099C / sizeof(DWORD),
			pinMCASP0ACLKR		= 0x09A0 / sizeof(DWORD),
			pinMCASP0FSR		= 0x09A4 / sizeof(DWORD),
			pinMCASP0AXR1		= 0x09A8 / sizeof(DWORD),
			pinMCASP0AHCLKX		= 0x09AC / sizeof(DWORD),
			pinCAM0HD		= 0x09B0 / sizeof(DWORD),
			pinCAM0VD		= 0x09B4 / sizeof(DWORD),
			pinCAM0FIELD		= 0x09B8 / sizeof(DWORD),
			pinCAM0WEN		= 0x09BC / sizeof(DWORD),
			pinCAM0PCLK		= 0x09C0 / sizeof(DWORD),
			pinCAM0DATA8		= 0x09C4 / sizeof(DWORD),
			pinCAM0DATA9		= 0x09C8 / sizeof(DWORD),
			pinCAM1DATA9		= 0x09CC / sizeof(DWORD),
			pinCAM1DATA8		= 0x09D0 / sizeof(DWORD),
			pinCAM1HD		= 0x09D4 / sizeof(DWORD),
			pinCAM1VD		= 0x09D8 / sizeof(DWORD),
			pinCAM1PCLK		= 0x09DC / sizeof(DWORD),
			pinCAM1FIELD		= 0x09E0 / sizeof(DWORD),
			pinCAM1WEN		= 0x09E4 / sizeof(DWORD),
			pinCAM1DATA0		= 0x09E8 / sizeof(DWORD),
			pinCAM1DATA1		= 0x09EC / sizeof(DWORD),
			pinCAM1DATA2		= 0x09F0 / sizeof(DWORD),
			pinCAM1DATA3		= 0x09F4 / sizeof(DWORD),
			pinCAM1DATA4		= 0x09F8 / sizeof(DWORD),
			pinCAM1DATA5		= 0x09FC / sizeof(DWORD),
			pinCAM1DATA6		= 0x0A00 / sizeof(DWORD),
			pinCAM1DATA7		= 0x0A04 / sizeof(DWORD),
			pinCAM0DATA0		= 0x0A08 / sizeof(DWORD),
			pinCAM0DATA1		= 0x0A0C / sizeof(DWORD),
			pinCAM0DATA2		= 0x0A10 / sizeof(DWORD),
			pinCAM0DATA3		= 0x0A14 / sizeof(DWORD),
			pinCAM0DATA4		= 0x0A18 / sizeof(DWORD),
			pinCAM0DATA5		= 0x0A1C / sizeof(DWORD),
			pinCAM0DATA6		= 0x0A20 / sizeof(DWORD),
			pinCAM0DATA7		= 0x0A24 / sizeof(DWORD),
			pinUART3RXD		= 0x0A28 / sizeof(DWORD),
			pinUART3TXD		= 0x0A2C / sizeof(DWORD),
			pinUART3CTSN		= 0x0A30 / sizeof(DWORD),
			pinUART3RTSN		= 0x0A34 / sizeof(DWORD),
			pinGPIO5_8		= 0x0A38 / sizeof(DWORD),
			pinGPIO5_9		= 0x0A3C / sizeof(DWORD),
			pinGPIO5_10		= 0x0A40 / sizeof(DWORD),
			pinGPIO5_11		= 0x0A44 / sizeof(DWORD),
			pinGPIO5_12		= 0x0A48 / sizeof(DWORD),
			pinGPIO5_13		= 0x0A4C / sizeof(DWORD),
			pinSPI4SCLK		= 0x0A50 / sizeof(DWORD),
			pinSPI4D0		= 0x0A54 / sizeof(DWORD),
			pinSPI4D1		= 0x0A58 / sizeof(DWORD),
			pinSPI4CS0		= 0x0A5C / sizeof(DWORD),
			pinSPI2SCLK		= 0x0A60 / sizeof(DWORD),
			pinSPI2D0		= 0x0A64 / sizeof(DWORD),
			pinSPI2D1		= 0x0A68 / sizeof(DWORD),
			pinSPI2CS0		= 0x0A6C / sizeof(DWORD),
			pinXDMAEVTINTR0		= 0x0A70 / sizeof(DWORD),
			pinXDMAEVTINTR1		= 0x0A74 / sizeof(DWORD),
			pinCLKREQ		= 0x0A78 / sizeof(DWORD),
			pinNRESETINOUT		= 0x0A7C / sizeof(DWORD),
			pinNNMI			= 0x0A84 / sizeof(DWORD),
			pinTMS			= 0x0A90 / sizeof(DWORD),
			pinTDI			= 0x0A94 / sizeof(DWORD),
			pinTDO			= 0x0A98 / sizeof(DWORD),
			pinTCK			= 0x0A9C / sizeof(DWORD),
			pinNTRST		= 0x0AA0 / sizeof(DWORD),
			pinEMU0			= 0x0AA4 / sizeof(DWORD),
			pinEMU1			= 0x0AA8 / sizeof(DWORD),
			pinOSC1IN		= 0x0AAC / sizeof(DWORD),
			pinOSC1OUT		= 0x0AB0 / sizeof(DWORD),
			pinRTCPORZ		= 0x0AB4 / sizeof(DWORD),
			pinEXTWAKEUP0		= 0x0AB8 / sizeof(DWORD),
			pinPMICPWOEREN0		= 0x0ABC / sizeof(DWORD),
			pinUSB0DRVVBUS		= 0x0AC0 / sizeof(DWORD),
			pinUSB1DRVVBUS		= 0x0AC4 / sizeof(DWORD),
			};

		// Pad Control Register
		enum PadControl
		{
			padWakeEnable		= Bit(29),
			padSleepPullNo		= 0,
			padSleepPullUp		= Bit(27) | Bit(28),
			padSleepPullDn		= Bit(27),
			padSleepOutDriveOff	= Bit(25),
			padSleepOutDriveHi	= Bit(26),
			padSleepOutDriveLo	= 0,
			padSleepOverride	= Bit(24),
			padFast			= 0,
			padSlow			= Bit(19),
			padRxActive		= Bit(18),
			padPullUp		= Bit(17),
			padPullDn		= 0,
			padPullNo		= Bit(16),
			};

		// Mux Modes
		enum MuxMode
		{
			muxMode0,
			muxMode1,
			muxMode2,
			muxMode3,
			muxMode4,
			muxMode5,
			muxMode6,
			muxMode7,
			muxMode8,
			muxMode9
			};

		// MII Modes
		enum MIIMode
		{
			modeMII			= 0,
			modeGMII		= 0,
			modeRMII		= 1,
			modeRGMII		= 2,
			};
		
	protected:
		// Registers
		enum
		{
			regREVISION		= 0x0000 / sizeof(DWORD),
			regHWINFO		= 0x0004 / sizeof(DWORD),
			regSYSCONFIG		= 0x0010 / sizeof(DWORD),
			regSTATUS		= 0x0040 / sizeof(DWORD),
			regDEVICEID		= 0x0600 / sizeof(DWORD),
			regDEVICEATTR		= 0x0610 / sizeof(DWORD),
			regCTRLUSB		= 0x0620 / sizeof(DWORD),
			regGMIISEL		= 0x0650 / sizeof(DWORD),
			regPWMSS		= 0x0664 / sizeof(DWORD),
			regDDRIOCTRL		= 0x0E04 / sizeof(DWORD),
			regCTRLVTP		= 0x0E0C / sizeof(DWORD),
			regDDRCKE		= 0x131C / sizeof(DWORD),
			regDDRADDRIOCTRL	= 0x1404 / sizeof(DWORD),
			regDDRADDRIOCTRLWD0	= 0x1408 / sizeof(DWORD),
			regDDRADDRIOCTRLWD1	= 0x140C / sizeof(DWORD),
			regDDRDATA0IOCTRL	= 0x1440 / sizeof(DWORD),
			regDDRDATA1IOCTRL	= 0x1444 / sizeof(DWORD),
			regDDRDATA2IOCTRL	= 0x1448 / sizeof(DWORD),
			regDDRDATA3IOCTRL	= 0x144C / sizeof(DWORD),
			regEMISDRAMCONFIG	= 0x1460 / sizeof(DWORD),
			};

		// Data Members
		PVDWORD	m_pBase;
	};

// End of File

#endif
