
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMotronaMasterDriver;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define FRAME_TIMEOUT	500 
#define TYPE_FIXED_4	1
#define TYPE_INT	2
#define TYPE_HEX	3

//////////////////////////////////////////////////////////////////////////
//
// Lenze LECOM Type II Driver
//

class CMotronaMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CMotronaMasterDriver(void);

		// Destructor
		~CMotronaMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BYTE m_bAddr;
			
			};

		
		CContext * m_pCtx;

		// Data Members
		LPCTXT	pHex;
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		UINT	m_uPtr;
		BYTE	m_bCheck;

		// Implementation
				
		void PutRead(UINT uCode, UINT uSubCode);
		void PutWrite(AREF Addr, PDWORD pData);

		void StartFrame(void);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddCode(UINT uCode, UINT uSub);

		DWORD GetGeneric(UINT uRadix, UINT uLength, UINT uOffset, UINT &uDPlaces);
		DWORD GetData(UINT uOffset, WORD wSpace);
		WORD  GetDataLength(WORD wOffset);

		void  PutData(DWORD dData, UINT uTag);
		DWORD PowerOf(UINT uBase, UINT uPower);
		void  PutGeneric(DWORD dData, UINT uRadix, UINT uFactor);
		void  PutHexadecimal(DWORD dData, UINT uRadix, UINT uFactor);
		void  PutValue(DWORD dValue);
		DWORD Abs(DWORD dData);
		BOOL  Neg(DWORD dData);
		
		void Send(void);
		BOOL GetFrame(void);
	};

// End of File
