
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function Blocks Definition
//

// Constructor

CFunctionDefinition::CFunctionDefinition(DWORD dwIdent)
{
	m_dwIdent = dwIdent;

	Build();
	}

// Filter

BOOL CFunctionDefinition::CanLoad(void)
{
	if( IsStandard() ) {

		if( IsMulti() ) {
			
			return FALSE;
			}

		if( IsOperator() ) {

			return TRUE;
			}

		if( IsIec() ) {

			return TRUE;
			}

		if( IsMath() ) {

			return TRUE;
			}

		if( IsCategory(L"RTCLOCKSTAMP")  ) {
			
			return TRUE;
			}

		return IsException();
		}

	return IsException();
	}

// Properties

CString CFunctionDefinition::GetName(void)
{
	return m_Name;
	}

// Attributes

BOOL CFunctionDefinition::IsStandard(void)
{
	return m_Type == funcSO || m_Type == funcSF || m_Type == funcSB;
	}

BOOL CFunctionDefinition::IsCustom(void)
{
	return m_Type == funcCF || m_Type == funcCB;
	}

BOOL CFunctionDefinition::IsOperator(void)
{
	return m_Type == funcSO;
	}

BOOL CFunctionDefinition::IsFunction(void)
{
	return m_Type == funcSF || m_Type == funcCF;
	}

BOOL CFunctionDefinition::IsFuncBlock(void)
{
	return m_Type == funcSB || m_Type == funcCB;
	}

BOOL CFunctionDefinition::IsIec(void)
{
	return IsCategory(L"IEC");
	}

BOOL CFunctionDefinition::IsMath(void)
{
	return IsCategory(L"MATH") || IsCategory(L"TRIGO");
	}

BOOL CFunctionDefinition::Is64Bit(void)
{
	return Is64Bit(m_Input) || Is64Bit(m_Output);
	}

BOOL CFunctionDefinition::IsArray(void)
{
	return IsArray(m_Input) || IsArray(m_Output);
	}

BOOL CFunctionDefinition::IsMulti(void)
{
	CStringArray List( L">> [I,J,K]", 
			   L">> [I,J]", 
			   L"[I,J,K] >>", 
			   L"[I,J] >>"
			   );

	return List.Find(m_Name) < NOTHING;
	}

BOOL CFunctionDefinition::IsException(void)
{
	CStringArray List;

	List.Append(L"ALARM_A");
	List.Append(L"ALARM_M");
	List.Append(L"blink");
	List.Append(L"blinkA");
	List.Append(L"CMP");
	List.Append(L"CTDr");
	List.Append(L"CTUDr");
	List.Append(L"CTUr");
	List.Append(L"PLS");
	List.Append(L"TMD");
	List.Append(L"TMU");
	List.Append(L"TOFR");
	List.Append(L"TPR");

	if( List.Find(m_Name) < NOTHING ) {
		
		return TRUE; 
		}

	if( m_Name == L"GetSysInfo" ) {
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CFunctionDefinition::IsCategory(PCTXT pName)
{
	return m_Class.Find(pName) < NOTHING;
	}

// Implementation

BOOL CFunctionDefinition::Is64Bit(CStringArray const &List)
{
	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CString Type = List[n];

		if( Is64Bit(Type.StripToken(L":")) ) {
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CFunctionDefinition::Is64Bit(CString const &Type)
{
	CStringArray Type64Bit( L"LREAL", 
				L"LWORD", 
				L"LINT", 
				L"ULINT" 
				);

	return Type64Bit.Find(Type) < NOTHING;
	}

BOOL CFunctionDefinition::IsArray(CStringArray const &List)
{
	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CString Type = List[n];

		Type.StripToken(L":");

		if( IsArray(Type) ) {
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CFunctionDefinition::IsArray(CString const &Type)
{
	if( Type.Find(L"[") < NOTHING ) {

		if( Type.Find(L"]") < NOTHING ) {
			
			return TRUE;
			}		
		}

	return FALSE;
	}

//

void CFunctionDefinition::Clean(void)
{
	m_Type = funcNK;

	m_Name.Empty();

	m_Class.Empty();

	m_Input.Empty();

	m_Output.Empty();
	}

void CFunctionDefinition::Build(void)
{
	CString Path;
	
	Path += afxRegistry->GetLibFolderPath();

	Path += afxRegistry->GetBlockLibName(m_dwIdent);

	Path += L"\\";

	Path += L"blocks.def";	

	FILE * pFile = _wfopen(Path, L"rb");

	if( pFile ) {

		CString Find = afxRegistry->GetBlockName(m_dwIdent);

		for( UINT s = 0 ;;) {

			char sLine[256] = { 0 };

			fgets(sLine, sizeof(sLine), pFile);

			if( sLine[0] ) {

				char *pArg;

				if( (pArg = strchr(sLine, '\r')) || (pArg = strchr(sLine, '\n')) ) {
					
					*pArg = 0;
					}

				if( s == 0 ) {

					char *pArg;

					if( (pArg = strchr(sLine, '<')) ) {

						*pArg = 0;

						Clean();

						if( !strcmp(pArg+1, "SF") ) {
							
							m_Type = funcSF;
							}

						if( !strcmp(pArg+1, "SO") ) {
							
							m_Type = funcSO;
							}

						if( !strcmp(pArg+1, "SB") ) {
							
							m_Type = funcSB;
							}

						if( !strcmp(pArg+1, "CF") ) {
							
							m_Type = funcCF;
							}

						if( !strcmp(pArg+1, "CB") ) {
							
							m_Type = funcCB;
							}

						s = 1;
						}

					continue;
					}

				if( s == 1 ) {

					char *pArg;

					if( (pArg = strchr(sLine, ':')) ) {

						*pArg = 0;

						switch( sLine[0] ) {

							case 'L':
								break;

							case 'N':
								m_Name = CString(pArg+1);
								break;

							case 'C':
								m_Class.Append(CString(pArg+1));
								break;

							case 'I':
								m_Input.Append(CString(pArg+1));
								break;

							case 'O':
								m_Output.Append(CString(pArg+1));
								break;
							}

						continue;
						}

					if( (pArg = strchr(sLine, '>')) ) {

						if( m_Name == Find ) {

							break;
							}

						s = 0;
						}

					continue;
					}

				AfxAssert(FALSE);
				}

			break;
			}

		fclose(pFile);
		}	
	}

// End of File
