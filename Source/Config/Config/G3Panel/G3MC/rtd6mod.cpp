
#include "intern.hpp"

#include "legacy.h"

#include "rtd6mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module
//

// Dynamic Class

AfxImplementDynamicClass(CRTD6Module, CGenericModule);


// Constructor

CRTD6Module::CRTD6Module(void)
{
	m_pInput = New CRTD6Input;

	m_FirmID = FIRM_RTD6;

	m_Ident  = ID_CSRTD6;

	m_Model  = "CSRTD6";

	m_Conv.Insert(L"Graphite", L"CGMRTD6Module");
	}

// UI Management

CViewWnd * CRTD6Module::CreateMainView(void)
{
	return New CRTD6MainWnd;
	}

// Comms Object Access

UINT CRTD6Module::GetObjectCount(void)
{
	return 1;
	}

BOOL CRTD6Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_INP);
			
			Data.pItem = m_pInput;

			return TRUE;
		}

	return FALSE;
	}

// Implementation

void CRTD6Module::AddMetaData(void)
{
	Meta_AddObject(Input);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CRTD6MainWnd, CProxyViewWnd);

// Constructor

CRTD6MainWnd::CRTD6MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CRTD6MainWnd::OnAttach(void)
{
	m_pItem = (CRTD6Module *) CProxyViewWnd::m_pItem;

	AddRTD6Pages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CRTD6MainWnd::AddRTD6Pages(void)
{
	CRTD6Input *pInput = m_pItem->m_pInput;

	for( UINT n = 0; n < pInput->GetPageCount(); n++ ) {

		CViewWnd *pPage = pInput->CreatePage(n);

		m_pMult->AddView(pInput->GetPageName(n), pPage);

		pPage->Attach(pInput);
		}
	}

// End of File
