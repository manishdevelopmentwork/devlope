
#include "intern.hpp"

#include "dupmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CDuplineModbusDriver);

// Constructor

CDuplineModbusDriver::CDuplineModbusDriver(void)
{
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uMaxWords = 32;
	
	m_uMaxBits  = 1;

	m_fAscii    = FALSE;

	m_fTrack    = FALSE;
	}

// Destructor

CDuplineModbusDriver::~CDuplineModbusDriver(void)
{
	}

// Configuration

void MCALL CDuplineModbusDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fAscii   = FALSE;

		m_uTimeout = GetWord(pData);
		}
	
	AllocBuffers();
	}
	
void MCALL CDuplineModbusDriver::CheckConfig(CSerialConfig &Config)
{
	if( !m_fAscii && Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
		
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CDuplineModbusDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CDuplineModbusDriver::Open(void)
{
	}

// Device

CCODE MCALL CDuplineModbusDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fRLCAuto   = FALSE;
			m_pCtx->m_fDisable15 = TRUE;
			m_pCtx->m_fDisable16 = FALSE;
			m_pCtx->m_uMax01     = 1;
			m_pCtx->m_uMax02     = 1;
			m_pCtx->m_uMax03     = 32;
			m_pCtx->m_uMax04     = 32;
			m_pCtx->m_uMax15     = 1;
			m_pCtx->m_uMax16     = 32;

			m_pCtx->m_uPing	     = 0;
			m_pCtx->m_fDisable5  = FALSE;
			m_pCtx->m_fDisable6  = FALSE;
			m_pCtx->m_fNoCheck   = FALSE;
			m_pCtx->m_fNoReadEx  = FALSE;
			m_pCtx->m_uPoll	     = 0;
			m_pCtx->m_fSwapCRC   = FALSE;

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDuplineModbusDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CDuplineModbusDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = DUP_SPACE_ANALINK;
	Addr.a.m_Offset = 0x0100;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return DoWordRead(Addr, Data, 1);
	}

CCODE MCALL CDuplineModbusDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case DUP_SPACE_OUTPUT:

			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case DUP_SPACE_INPUT:
			
			return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax02));

		case DUP_SPACE_COUNTER:
			
			return DoLongRead(Addr, pData, min(uCount, m_pCtx->m_uMax03 / 2));
			
		case DUP_SPACE_ANALINK:
		case DUP_SPACE_MUX_IN:
		case DUP_SPACE_MUX_OUT:
		case DUP_SPACE_DIG_WORD:
		case DUP_SPACE_ANALIMIT:
		case DUP_SPACE_ANALIMITONLY:
		case DUP_SPACE_REALTIME:
		case DUP_SPACE_REALTIMETEXT:

			return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));

		case DUP_SPACE_RESET_CTR:

			return uCount;

		case DUP_SPACE_DIGITAL:
		case DUP_SPACE_DIGIN:
		case DUP_SPACE_DIGOUT:

			if( Addr.a.m_Type == addrBitAsBit ) {

				return DoBitRead(Addr, pData, min(uCount, m_pCtx->m_uMax02));
				}

			return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));

		case DUP_SPACE_CLOCKTEXT:

			return DoClockRead(Addr, pData, uCount);
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CDuplineModbusDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case DUP_SPACE_MUX_IN:
		case DUP_SPACE_MUX_OUT:
		case DUP_SPACE_ANALIMIT:
		case DUP_SPACE_ANALIMITONLY:
		case DUP_SPACE_REALTIME:
		case DUP_SPACE_REALTIMETEXT:

			return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16));

		case DUP_SPACE_INPUT:
		case DUP_SPACE_RESET_CTR:
			
			return DoBitWrite(Addr, pData, min(uCount, m_pCtx->m_uMax15));

		case DUP_SPACE_OUTPUT:
		case DUP_SPACE_COUNTER:
		case DUP_SPACE_ANALINK:
			return uCount;

		case DUP_SPACE_DIGITAL:
		case DUP_SPACE_DIGIN:
		case DUP_SPACE_DIGOUT:

			if( Addr.a.m_Type == addrBitAsBit ) {

				return DoBitWrite(Addr, pData, min(uCount, m_pCtx->m_uMax15));
				}

			return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16));

		case DUP_SPACE_CLOCKTEXT:

			return DoClockWrite(Addr, pData, uCount);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Read Handlers

CCODE CDuplineModbusDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount, BOOL fLimit)
{
	StartFrame(0x03);

	UINT uOffset = Addr.a.m_Offset & 0x7FFF;

	if( Addr.a.m_Type == addrBitAsWord ) {

		uOffset /= 16;
		}

	AdjustSpecial(Addr.a.m_Table, uCount, uOffset);

	AddWord(uOffset);
	
	AddWord(uCount);

	if( Transact(FALSE) ) {

		GetWords(Addr.a.m_Table, pData, uCount, uOffset, fLimit);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CDuplineModbusDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x03);

	UINT uOffset = (( Addr.a.m_Offset  & 0x7FFF ) - READ_OFFSET_COUNTER) * 2 + READ_OFFSET_COUNTER;
	
	AddWord(uOffset);

	AddWord(uCount * 2);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x = PU4(m_pRx + 3)[n];
			
			pData[n] = FromBCD(MotorToHost(x));
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CDuplineModbusDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case DUP_SPACE_OUTPUT:
		case DUP_SPACE_DIGOUT:

			StartFrame(0x01);

			break;
			
		case DUP_SPACE_INPUT:
		case DUP_SPACE_DIGITAL:
		case DUP_SPACE_DIGIN:

			StartFrame(0x02);

			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset & 0x7FFF);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CDuplineModbusDriver::DoClockRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x03);

	UINT uOffset = Addr.a.m_Offset & 0x7FFF;

	AddWord(uOffset);
	
	AddWord(4);

	if( Transact(FALSE) ) {

		GetClock(Addr.a.m_Table, pData);

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CDuplineModbusDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = 0;

	UINT u = 0;

	PDWORD pRead = NULL;

	switch ( Addr.a.m_Table ) {

		case DUP_SPACE_MUX_IN:

			StartFrame(16);

			uOffset = ( Addr.a.m_Offset & 0x7FFF ) + WRITE_OFFSET_MUX_IN;

			break;

		case DUP_SPACE_DIGITAL:
		
			StartFrame(16);

			uOffset = ( ( Addr.a.m_Offset & 0x7FFF ) / 16 ) + WRITE_OFFSET_DIGITAL;

			break;

		case DUP_SPACE_DIGIN:

			StartFrame(16);

			uOffset = ( ( Addr.a.m_Offset & 0x7FFF ) / 16 ) + WRITE_OFFSET_DIGIN;

			break;
		
		case DUP_SPACE_DIGOUT:

			StartFrame(16);

			uOffset = ( ( Addr.a.m_Offset & 0x7FFF ) / 16 ) + WRITE_OFFSET_DIGOUT;

			break;

		case DUP_SPACE_ANALIMITONLY:
		case DUP_SPACE_ANALIMIT:

			for( u = 0; u < uCount; u++ ) {

				UINT uData = pData[u] & 0x00FF;

				pData[u] = 0;

				pData[u] = uData;
				}
	       	
	
		case DUP_SPACE_MUX_OUT:
		case DUP_SPACE_REALTIME:
		case DUP_SPACE_REALTIMETEXT:
		
			StartFrame(16);

			uOffset = Addr.a.m_Offset & 0x7FFF;

			break;

		default:
			return CCODE_ERROR | CCODE_HARD;

		}

	AdjustSpecial(Addr.a.m_Table, uCount, uOffset);
	
	AddWord(uOffset);
			
	AddWord(uCount);
			
	AddByte(uCount * 2);

	AddWords(Addr.a.m_Table, pData, uCount, uOffset); 

	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CDuplineModbusDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
        UINT uOffset = 0;

	switch ( Addr.a.m_Table ) {

		case DUP_SPACE_INPUT:
		case DUP_SPACE_DIGITAL:
		case DUP_SPACE_DIGIN:
		
			StartFrame(5);

			uOffset = ( Addr.a.m_Offset & 0x7FFF ) + WRITE_OFFSET_INPUT;

			break;

		case DUP_SPACE_DIGOUT:
		
			StartFrame(5);

			uOffset = ( Addr.a.m_Offset & 0x7FFF ) + WRITE_OFFSET_OUTPUT;

			break;

		case DUP_SPACE_RESET_CTR:

			StartFrame(5);

			uOffset = ( Addr.a.m_Offset & 0x7FFF ) + WRITE_OFFSET_RS_CTR;

			break;

		default:
			return CCODE_ERROR | CCODE_HARD;

		}
			
	AddWord(uOffset);
			
	AddWord(pData[0] ? 0xFF00 : 0x0000);
		
	if( Transact(TRUE) ) {
			
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CDuplineModbusDriver::DoClockWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = 0;

	UINT u = 0;

	DWORD Read[4];

	CCODE cc = DoClockRead(Addr, Read, 1);

	if( COMMS_SUCCESS(cc) ) {
	
		StartFrame(16);

		uOffset = (Addr.a.m_Offset & 0x7FFF) - WRITE_OFFSET_CLOCK;

		AddWord(uOffset);
			
		AddWord(4);
			
		AddByte(4 * 2);

		AddClock(Addr.a.m_Table, pData);

		if( Transact(TRUE) ) {

			MakeMin(uCount, 4);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

DWORD CDuplineModbusDriver::FromBCD(DWORD d)
{
	DWORD dwBCD = 0;

	DWORD dwMult = 1;

	for( UINT u = 0; u < sizeof(DWORD); u++, dwMult *= 100 ) {

		BYTE bShift = u * 8;

		BYTE b = ( d & ( 0xFF << bShift ) ) >> bShift;

		dwBCD += ( ( (b % 16) + 10 * (b / 16) ) * dwMult );
		}

	return dwBCD;
	}

WORD CDuplineModbusDriver::FromBCD(WORD w)
{
	WORD wBCD = 0;

	WORD wMult = 1;

	for( UINT u = 0; u < sizeof(WORD); u++, wMult *= 100 ) {

		BYTE bShift = u * 8;

		BYTE b = ( w & ( 0xFF << bShift ) ) >> bShift;

		wBCD += ( ( (b % 16) + 10 * (b / 16) ) * wMult );
		}

	return wBCD;
	}

WORD CDuplineModbusDriver::ToBCD(WORD wData)
{
	WORD wWork = 0;
	
	WORD wHex  = 0x1000;

	WORD wDec  = 1000;
	
	while( wHex ) {
	
		wWork += wHex * ((wData / wDec) % 10);

		wHex /= 16;
		wDec /= 10;
		}
	
	return wWork;
	}

void CDuplineModbusDriver::AddWords(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset)
{
	BOOL fBCD = (uTable == DUP_SPACE_REALTIME) || (uTable == DUP_SPACE_REALTIMETEXT);
		
	for( UINT n = 0; n < uCount; n++ ) {

		if( fBCD && ((uOffset + n) % 4 != 2) ) {

			WORD Word = LOWORD(pData[n]);

			if( uTable == DUP_SPACE_REALTIMETEXT ) {

				LONG lData = (pData[n] / 3600 * 100);

				lData += ( (pData[n] - ( lData / 100 * 3600 )) / 60 );
				
				Word = LOWORD(lData);
				}
			
			AddWord(ToBCD(Word));
			}

		else {	
			AddWord(LOWORD(pData[n]));
			}
		}
	}

void CDuplineModbusDriver::AddClock(UINT uTable, PDWORD pData)
{
	if( uTable == DUP_SPACE_CLOCKTEXT ) {

		LONG lTime = pData[0];

		WORD x = GetHour(lTime) * 100 + GetMin(lTime);

		AddWord(ToBCD(x));

		x = GetSec(lTime) * 100 + LOBYTE(PWORD(m_pRx + 3)[1]);

		AddWord(ToBCD(x));

		x = GetMonth(lTime) * 100 + GetDate(lTime);

		AddWord(ToBCD(x));

		AddWord(ToBCD(GetYear(lTime)));
		}
	}

void CDuplineModbusDriver::GetWords(UINT uTable, PDWORD pData, UINT uCount, UINT uOffset, BOOL fLimit)
{
	BOOL fBCD = (uTable == DUP_SPACE_REALTIME) || (uTable == DUP_SPACE_REALTIMETEXT);
		
	for( UINT n = 0; n < uCount; n++ ) {
		
		WORD x = PU2(m_pRx + 3)[n];

		if( fBCD && ((uOffset + n) % 4 != 2) ) {

			pData[n] = FromBCD(MotorToHost(x));

			if( uTable == DUP_SPACE_REALTIMETEXT) {

				pData[n] = (pData[n] / 100 * 3600) + (pData[n] % 100 * 60);
				}
			}
		
		else if	( uTable == DUP_SPACE_ANALIMITONLY && !fLimit ) {

			pData[n] = LONG(SHORT(x & 0xFF));
			}
					
		else {	
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}
		
		}
	}

void CDuplineModbusDriver::GetClock(UINT uTable, PDWORD pData)
{
	if( uTable == DUP_SPACE_CLOCKTEXT ) {

		UINT uDay = 0, uMonth = 0, uYear = 0, uHour = 0, uMin = 0, uSec = 0;

		for( UINT n = 0; n < 4; n++ ) {

			WORD x = PU2(m_pRx + 3)[n];

			UINT uSeg = FromBCD(MotorToHost(x));

			if( n == 0 ) {

				uMin = uSeg % 100;

				uHour = uSeg / 100;
				}

			else if ( n == 1 ) {

				uSec = uSeg / 100;
				}
			
			else if( n == 2 ) {

				uDay   = uSeg % 100;

				uMonth = uSeg / 100;
				}
			
			else if( n == 3 ) {

				pData[0] = Date(uSeg, uMonth, uDay);

				pData[0] += Time(uHour, uMin, uSec);
				}
			}
		}
	}

void  CDuplineModbusDriver::AdjustSpecial(UINT uTable, UINT &uCount, UINT &uOffset)
{
	UINT uAdjust = 0;

	switch( uTable ) {

		case DUP_SPACE_REALTIME:
		case DUP_SPACE_REALTIMETEXT:

			uAdjust = uOffset % 3;

			MakeMin(uCount, 3 - uAdjust);

			uAdjust = (uOffset - 0x6000) / 3 ;

			uOffset += uAdjust;

			break;
		}
	}

// End of File
