
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FLOW_HPP
	
#define	INCLUDE_FLOW_HPP 

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Master Driver Options
//

class CFlowCommMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFlowCommMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
			
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Constants
//

#define FLOW_MAX	80
#define FLOW_OFFSET	0x20

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Master Driver
//

class CFlowCommMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CFlowCommMasterDriver(void);

		//Destructor
		~CFlowCommMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);


	protected:
		// Implementation
		void AddSpaces(void);

		// Helper
		BOOL SetHexadecimal(CString &Text, UINT &uTarget);

	};


//////////////////////////////////////////////////////////////////////////
//
// FlowComm Address Selection Dialog
//

class CFlowCommAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CFlowCommAddrDialog(CFlowCommMasterDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Data Members
		CFlowCommMasterDriver	* m_pDriver;
		CAddress		* m_pAddr;
		BOOL			  m_fPart;

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnClear(UINT uID);

		// Implementation
		void ShowAddress(CAddress &Addr);
		void ShowInformation(void);
	};
#endif

// End of File