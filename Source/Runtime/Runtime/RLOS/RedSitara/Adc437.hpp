
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Adc437_HPP
	
#define	INCLUDE_AM437_Adc437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CGpio437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 ADC
//

class CAdc437 : public IEventSink
{
	public:
		// Constructor
		CAdc437(void);

		// Destructor
		~CAdc437(void);

		// Operations
		DWORD GetReading(UINT uChannel);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum 
		{
			regSYSCONFIG	= 0x0010 / sizeof(DWORD),
			regIRQSTS_RAW	= 0x0024 / sizeof(DWORD),
			regIRQSTS	= 0x0028 / sizeof(DWORD),
			regIRQEN_SET	= 0x002C / sizeof(DWORD),
			regIRQEN_CLR	= 0x0030 / sizeof(DWORD),
			regCTRL		= 0x0040 / sizeof(DWORD),
			regCLKDIV	= 0x004C / sizeof(DWORD),
			regSTEPEN	= 0x0054 / sizeof(DWORD),
			regSTEPCONFIG1	= 0x0064 / sizeof(DWORD),
			regSTEPDELAY1	= 0x0068 / sizeof(DWORD),
			regSTEPCONFIG2	= 0x006C / sizeof(DWORD),
			regSTEPDELAY2	= 0x0070 / sizeof(DWORD),
			regFIFO0COUNT	= 0x00E4 / sizeof(DWORD),
			regFIFO0THR	= 0x00E8 / sizeof(DWORD),
			regFIFO0DATA	= 0x0100 / sizeof(DWORD),
			};

		// Interrupts
		enum 
		{
			intEndOfSeq = Bit(1)
			};

		// Data Members
		PVDWORD   m_pBase;
		IEvent  * m_pDone;
		UINT      m_uLine;
		DWORD     m_dwRaw;
		bool      m_fBusy;
		bool      m_fWait;

		// Implementation
		void InitController(void);
		void InitEvents(void);
		void OnConvert(void);
		void TestWait(void);
		void WaitDone(void);
	};

// End of File

#endif
