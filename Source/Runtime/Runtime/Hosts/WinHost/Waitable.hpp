
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Waitable_HPP

#define INCLUDE_Waitable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HANDLE;

//////////////////////////////////////////////////////////////////////////
//
// Generic Waitable Object
//

class CWaitable : public IWaitable
{
	public:
		// Constructor
		CWaitable(void);

		// Destructor
		virtual ~CWaitable(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime, BOOL fAlert);
		BOOL  METHOD HasRequest(void);

	protected:
		// Data Members
		ULONG  m_uRefs;
		ULONG  m_uWait;
		HANDLE m_hSync;
	};

// End of File

#endif
