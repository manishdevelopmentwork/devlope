
#include "Intern.hpp"

#include "DispFormat.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispFormatNumber.hpp"
#include "DispFormatSci.hpp"
#include "DispFormatTimeDate.hpp"
#include "DispFormatIPAddr.hpp"
#include "DispFormatFlag.hpp"
#include "DispFormatMulti.hpp"
#include "DispFormatLinked.hpp"
#include "DispFormatString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormat, CCodedHost);

// Class Enumeration

CLASS CDispFormat::GetClass(UINT uType)
{
	switch( uType ) {

		case 1:
			return AfxRuntimeClass(CDispFormatNumber);

		case 2:
			return AfxRuntimeClass(CDispFormatSci);

		case 3:
			return AfxRuntimeClass(CDispFormatTimeDate);

		case 4:
			return AfxRuntimeClass(CDispFormatIPAddr);

		case 5:
			return AfxRuntimeClass(CDispFormatFlag);

		case 6:
			return AfxRuntimeClass(CDispFormatMulti);

		case 7:
			return AfxRuntimeClass(CDispFormatLinked);

		case 8:
			return AfxRuntimeClass(CDispFormatString);
		}

	return NULL;
	}

// General Formatting

CString CDispFormat::GeneralFormat(DWORD Data, UINT Type, UINT Flags)
{
	if( !(Flags & fmtUnits) ) {

		CString Text = L"0000";

		if( Type == typeInteger ) {

			char s[32];

			sprintf(s, "%d", C3INT(Data));

			Text = s;
			}

		if( Type == typeReal ) {

			C3REAL r = I2R(Data);

			char   s[32];

			if( _isnan(r) ) {

				strcpy(s, "NAN");
				}
			else {
				if( _finite(r) ) {

					gcvt(r, 5, s);

					char *p = strchr(s, 'e');

					if( p ) {

						p[0] = 'E';

						if( p[2] == '0' ) {

							if( p[3] == '0' ) {

								p[2] = p[4];
								p[3] = 0;
								}
							else {
								p[2] = p[3];
								p[3] = p[4];
								p[4] = 0;
								}
							}

						if( *--p == '.' ) {

							memmove(p, p+1, strlen(p));
							}
						}
					else {
						int n = strlen(s);

						if( n && s[n-1] == '.' ) {

							s[n-1] = 0;
							}
						}
					}
				else {
					if( r > 0 ) {

						strcpy(s, "+INF");
						}
					else
						strcpy(s, "-INF");
					}
				}

			Text = s;
			}

		if( Type == typeString ) {

			Text = Data ? PCTXT(Data) : L"";

			return Text;
			}

		if( Flags & fmtPad ) {

			MakeDigitsFixed(Text);
			}

		return Text;
		}

	return L"";
	}

// Constructor

CDispFormat::CDispFormat(void)
{
	m_uType   = 0;

	m_fLimits = FALSE;
	}

// UI Update

void CDispFormat::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( !Tag.IsEmpty() ) {

		CItem *pParent = pItem->GetParent();

		if( pParent->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

			CUIItem *pUI = (CUIItem *) pParent;

			pUI->OnUIChange(pHost, pItem, L"FormatObject");
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

UINT CDispFormat::GetFormType(void) const
{
	return m_uType;
	}

BOOL CDispFormat::NeedsLimits(void) const
{
	return m_fLimits;
	}

// Formatting

CString CDispFormat::Format(DWORD Data, UINT Type, UINT Flags)
{
	return GeneralFormat(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormat::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return 0;
		}

	return R2I(0);
	}

DWORD CDispFormat::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return 999999;
		}

	return R2I(999999);
	}

// Property Preservation

BOOL CDispFormat::Preserve(CDispFormat *pOld)
{
	return FALSE;
	}

// Download Support

BOOL CDispFormat::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_uType));

	CCodedHost::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CDispFormat::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_SetName((IDS_DISPLAY_FORMAT));
	}

// Implementation

void CDispFormat::MakeDigitsFixed(CString &Text)
{
	MakeDigitsFixed(PTXT(PCTXT(Text)));
	}

void CDispFormat::MakeLettersFixed(CString &Text)
{
	MakeDigitsFixed(PTXT(PCTXT(Text)));
	}

void CDispFormat::MakeDigitsFixed(PTXT pText)
{
	while( *pText ) {

		if( *pText >= digitSimple && *pText < digitSimple + 10 ) {

			*pText -= digitSimple;

			*pText += digitFixed;
			}

		pText++;
		}
	}

void CDispFormat::MakeLettersFixed(PTXT pText)
{
	while( *pText ) {

		if( *pText >= letterSimple && *pText < letterSimple + 6 ) {

			*pText -= letterSimple;

			*pText += letterFixed;
			}

		pText++;
		}
	}

// End of File
