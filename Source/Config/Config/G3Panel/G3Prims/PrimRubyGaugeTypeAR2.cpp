
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A2 Radial Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR2, CPrimRubyGaugeTypeAR);

// Constructor

CPrimRubyGaugeTypeAR2::CPrimRubyGaugeTypeAR2(void)
{
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR2::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeAR::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR2::AddMetaData(void)
{
	CPrimRubyGaugeTypeAR::AddMetaData();

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE_3));
	}

// Path Management

void CPrimRubyGaugeTypeAR2::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeAR::MakePaths();

	m_bezelBase = 67;

	m_d.Circle(m_pathFace, m_pointCenter, 67, m_scale);

	Ring(m_pathOuter, 80, 90);
	Ring(m_pathInner, 70, 80);
	Ring(m_pathRing1, 89, 91);
	Ring(m_pathRing2, 79, 80);
	Ring(m_pathRing3, 66, 70);
	Ring(m_pathRing4, 74, 69);
	}

// Layout

void CPrimRubyGaugeTypeAR2::LoadLayout(void)
{
	m_radiusPivot     = 10 * m_ScalePivot / 100.0;
	
	m_pointPivot.m_x  = 100;
	m_pointPivot.m_y  = 100;

	m_angleMin        = -135 - 90;
	m_angleMax        = +135 - 90;
	
	m_radiusOuter.m_x = 60;
	m_radiusOuter.m_y = 60;

	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBug.m_x   = m_PointMode ? 65 : 65;
	m_radiusMajor.m_x = m_PointMode ? 52 : 45;
	m_radiusMinor.m_x = m_PointMode ? 54 : 50;
	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBand.m_x  = m_PointMode ? 55 : 55;
	m_radiusPoint.m_x = m_PointMode ? 51 : 52;
	m_radiusSweep.m_x = m_PointMode ? 40 : 52;

	CalcRadii();
	}

// End of File
