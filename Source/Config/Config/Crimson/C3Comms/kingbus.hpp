
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KINGBUS_HPP
	
#define	INCLUDE_KINGBUS_HPP

//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Driver Options
//

class CKingBusASCIIDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKingBusASCIIDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Timeout;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Communications
//

class CKingBusASCIIDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKingBusASCIIDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
				
		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);


		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
   
	
	protected:
		
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Address Selection Dialog
//

class CKingBusASCIIAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CKingBusASCIIAddrDialog(CKingBusASCIIDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);
		
	protected:
		UINT m_uDrop;
		
		// Overridables
		void    SetAddressText(CString Text);
	};


//////////////////////////////////////////////////////////////////////////
//
// King Space Wrapper
//

class CSpaceKing : public CSpace
{
	public:
		// Constructors
		CSpaceKing(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);
		
	};

// End of File

#endif
