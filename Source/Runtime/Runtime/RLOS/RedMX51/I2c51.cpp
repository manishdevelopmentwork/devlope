
#include "Intern.hpp"

#include "i2c51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 IC2 Controller
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice *Create_I2cInterface51(CCcm51 *pCcm)
{
	CI2c51 *p = New CI2c51(pCcm);

	p->Open();

	return (IDevice *)(II2c *) p;
	}

// Constructor

CI2c51::CI2c51(CCcm51 *pCcm)
{
	StdSetRef();

	m_pLock  = Create_Qutex();

	m_pDone  = Create_AutoEvent();

	m_pBase  = PVWORD(ADDR_I2C1);

	m_uLine  = INT_I2C1;

	m_uFreq  = pCcm->GetFreq(CCcm51::clkPer);

	m_uState = NOTHING;

	m_fBusy  = false;
	}

// Destructor

CI2c51::~CI2c51(void)
{
	m_pLock->Release();

	m_pDone->Release();
	}

// IUnknown

HRESULT CI2c51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(II2c);

	return E_NOINTERFACE;
	}

ULONG CI2c51::AddRef(void)
{
	StdAddRef();
	}

ULONG CI2c51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CI2c51::Open(void)
{
	InitController();

	InitEvents();

	return TRUE;
	}

// II2c

BOOL METHOD CI2c51::Lock(UINT uTime)
{
	if( CanWait() ) {

		return m_pLock->Wait(uTime);
		}

	return !m_fBusy;
	}

void METHOD CI2c51::Free(void)
{
	if( CanWait() ) {

		m_pLock->Free();

		CheckThreadCancellation();
		}
	}

BOOL METHOD CI2c51::Send(BYTE bChip, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData)
{
	TestWait();

	m_bAddr  = bChip;

	m_pHead  = pHead;

	m_uHead  = uHead;

	m_pData  = PBYTE(pData);

	m_uData  = uData;

	m_fSend  = true;

	m_uState = 0;

	OnEvent(m_uLine, 1);

	WaitDone();

	return m_fOkay;
	}

BOOL METHOD CI2c51::Recv(BYTE bChip, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData)
{
	TestWait();

	m_bAddr  = bChip;

	m_pHead  = pHead;

	m_uHead  = uHead;

	m_pData  = PBYTE(pData);

	m_uData  = uData;

	m_fSend  = false;

	m_uState = 0;

	OnEvent(m_uLine, 1);

	WaitDone();

	return m_fOkay;
	}

// IEventSink

void CI2c51::OnEvent(UINT uLine, UINT uParam)
{
	ClearPending();

	switch( m_uState ) {

		case 0:
			if( !m_uHead ) {

				if( m_fSend ) {

					StartSend();

					m_uPtr   = 0;

					m_uState = 2;
					}
				else {
					StartRecv();

					m_uState = 3;
					}
				}
			else {
				StartSend();

				m_uPtr   = 0;

				m_uState = 1;
				}
			break;

		case 1:
			if( Reg(Stat) & statRxNak ) {

				EndSequence(false);
				}
			else {
				if( m_uPtr == m_uHead ) {

					if( !m_uData ) {

						EndSequence(true);
						}
					else {
						StartRepeat();

						m_uState = 3;
						}
					}
				else {
					Reg(Data) = m_pHead[m_uPtr++];

					if( m_uPtr == m_uHead ) {

						if( m_fSend ) {

							m_uPtr   = 0;

							m_uState = 2;
							}
						}
					}
				}
			break;

		case 2:
			if( Reg(Stat) & statRxNak ) {

				EndSequence(false);
				}
			else {
				if( m_uPtr == m_uData ) {

					EndSequence(true);
					}
				else {
					Reg(Data) = m_pData[m_uPtr++];
					}
				}
			break;

		case 3:
			if( Reg(Stat) & statRxNak ) {

				EndSequence(false);
				}
			else {
				if( !m_uData ) {

					EndSequence(true);
					}
				else {
					m_uPtr   = 0;

					m_uState = 4;

					SetRecv();

					SetAck(m_uPtr < m_uData - 1);

					BYTE bDummy = Reg(Data);
					}
				}
			break;

		case 4:
			SetAck(m_uPtr < m_uData - 2);

			if( m_uPtr == m_uData - 1 ) {
									
				SetStop();
					
				m_pData[m_uPtr++] = Reg(Data);
					
				EndSequence(true);
				}
			else {
				m_pData[m_uPtr++] = Reg(Data);
				}

			break;
		}
	}

// Implementation

void CI2c51::InitController(void)
{
	Reg(Ctrl)  = 0;

	Reg(Freq)  = FindDivider(100000);

	Reg(Addr)  = 0x0000;

	Reg(Stat)  = 0;

	Reg(Ctrl)  = ctrlEnable;
	
	Reg(Ctrl) |= ctrlIntEnable;
	}

void CI2c51::InitEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);
	}

void CI2c51::StartSend(void)
{
	Reg(Ctrl) |= ctrlMaster;

	Reg(Ctrl) |= ctrlSend;

	Reg(Data)  = m_bAddr;
	}

void CI2c51::StartRepeat(void)
{
	Reg(Ctrl) |= ctrlMaster;

	Reg(Ctrl) |= ctrlRepeat;

	Reg(Data)  = m_bAddr | Bit(0);
	}

void CI2c51::StartRecv(void)
{
	Reg(Data)  = m_bAddr | Bit(0);

	Reg(Ctrl) |= (ctrlMaster | ctrlSend);
	}

void CI2c51::SetStop(void)
{
	Reg(Ctrl) &= ~ctrlMaster;

	while(Reg(Stat) & statBusBusy);
	}

void CI2c51::SetRecv(void)
{
	Reg(Ctrl) &= ~ctrlSend;
	}

void CI2c51::SetSend(void)
{
	Reg(Ctrl) |= ctrlSend;
	}

void CI2c51::SetAck(bool fAck)
{
	if( fAck ) {

		Reg(Ctrl) &= ~ctrlNoTxAck;
		}
	else {
		Reg(Ctrl) |= ctrlNoTxAck;
		}
	}

void CI2c51::ClearPending(void)
{
	Reg(Stat) = 0;
	}

void CI2c51::EndSequence(bool fOkay)
{
	SetStop();

	m_fOkay = fOkay;

	m_pDone->Set();
	}

bool CI2c51::CanWait(void)
{
	if( Hal_GetIrql() >= IRQL_DISPATCH ) {

		return false;
		}

	if( !GetCurrentThread() ) {

		return false;
		}

	if( GetCurrentThread()->GetIndex() <= 1 ) {

		return false;
		}

	return true;
	}

void CI2c51::TestWait(void)
{
	// LATER -- We can get a dummy interrupt is the
	// last operation was in polled mode. We ought
	// to have a method of clearing the pend.

	m_fWait  = CanWait();

	m_fBusy  = true;

	m_uState = NOTHING;

	phal->EnableLine(m_uLine, m_fWait);
	}

void CI2c51::WaitDone(void)
{
	if( !m_fWait ) {
		
		while( !m_pDone->Wait(0) ) {

			while( !(Reg(Stat) & statIntPend) );

			OnEvent(m_uLine, 0);
			}

		m_fBusy = false;

		return;
		}

	m_pDone->Wait(FOREVER);

	m_fBusy = false;
	}

UINT CI2c51::FindDivider(UINT uFreq) const
{
	const WORD wDiv[] = 
	{
		  30,   32,   36,   42,   48,   52,   60,   72,
		  80,   88,  104,  128,  144,  160,  192,  240,
		 288,  320,  384,  480,  576,  640,  768,  960,
		1152, 1280, 1536, 1920, 2304, 2560, 3072, 3840,
		  22,   24,   26,   28,   32,   36,   40,   44,
		  48,   56,   64,   72,   80,   96,  112,  128,
		 160,  192,  224,  256,  320,  384,  448,  512,
		 640,  768,  896, 1024, 1280, 1536, 1792, 2048
		};

	UINT uDiv = m_uFreq / uFreq;

	int  nFindErr = WORD(-1);

	UINT uFindIdx = NOTHING;

	for( UINT i = 0; i < elements(wDiv); i ++ ) {

		int nErr = wDiv[i] - uDiv;

		if( nErr >= 0 ) {
			
			if( nErr <= 1 ) {

				return i;
				}

			if( nErr < nFindErr ) {

				uFindIdx = i;

				nFindErr = nErr;
				}
			}
		}

	return uFindIdx;
	}

// End of File
