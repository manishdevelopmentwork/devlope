
#include "Intern.hpp"

#include "Executive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

#include "Waitable.hpp"

#include "BaseHal.hpp"

#include "../../HSL/ExecTimer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Exception Handling Data
//

extern void * _thread_eh_data;

//////////////////////////////////////////////////////////////////////////
//
// Sync Object Registration
//

extern void Register_Rutex(void);

//////////////////////////////////////////////////////////////////////////
//
// Executive Object
//

// Instance Pointer

CExecutive * CExecutive::m_pThis = NULL;

// Current Thread

CExecThread * volatile CExecutive::m_pThread = NULL;

CExecThread * volatile CExecutive::m_pAlias  = NULL;

// Instantiator

IUnknown * Create_Executive(void)
{
	return (IExecutive *) New CExecutive;
}

// Constructor

CExecutive::CExecutive(void)
{
	m_pThis = this;

	StdSetRef();

	m_pHeadThread = NULL;
	m_pTailThread = NULL;
	m_pHeadGroup  = NULL;
	m_pTailGroup  = NULL;
	m_pPend       = NULL;
	m_fPend       = false;
	m_uTick	      = phal->GetTimerResolution();
	m_nFract      = 0;
	m_nWhole      = m_uTick * 1000;
	m_IntTime     = 0;
	m_RefTime     = 0;
	m_pReapFlag   = NULL;
	m_pReapBusy   = NULL;
	m_pReapThread = NULL;

	phal->SetExecHook(this);

	DiagRegister();

	// We register this so it can be used during start up.

	Register_Rutex();

	piob->RegisterInstantiator("exec.mutex", Create_Mutex);
	piob->RegisterInstantiator("exec.qutex", Create_Qutex);
	piob->RegisterInstantiator("exec.semaphore", Create_Semaphore);
	piob->RegisterInstantiator("exec.event-m", Create_ManualEvent);
	piob->RegisterInstantiator("exec.event-a", Create_AutoEvent);
}

// Destructor

CExecutive::~CExecutive(void)
{
	DiagRevoke();

	AfxRelease(m_pReapFlag);

	AfxRelease(m_pReapBusy);

	m_pThis = NULL;
}

// IUnknown

HRESULT CExecutive::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IExecutive);

	StdQueryInterface(IExecutive);

	StdQueryInterface(IWaitMultiple);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CExecutive::AddRef(void)
{
	StdAddRef();
}

ULONG CExecutive::Release(void)
{
	StdRelease();
}

// IExecutive

void CExecutive::Open(void)
{
}

void CExecutive::Close(void)
{
}

IThread * CExecutive::CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam)
{
	if( !m_pThread ) {

		// We're not yet running, so we're going to create the
		// base level thread and then have it finish the setup
		// and create the thread requested by the API caller.

		DWORD d[4];

		// Arrange the parameters so we can pass
		// them down into our base level thread
		// for it to create the real new thread.

		d[0] = DWORD(pfnProc);
		d[1] = DWORD(uLevel);
		d[2] = DWORD(pParam);
		d[3] = DWORD(uParam);

		return CreateThread(BaseLevel, NULL, 0, d, 0, NULL);
	}

	return CreateThread(pfnProc, NULL, uLevel, pParam, uParam, NULL);
}

IThread * CExecutive::CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel)
{
	return CreateThread(ClientProc, pProc, uLevel, pProc, uIndex, NULL);
}

IThread * CExecutive::CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms)
{
	return CreateThread(ClientProc, pProc, uLevel, pProc, uIndex, pTms);
}

BOOL CExecutive::DestroyThread(IThread *pThread)
{
	return pThread->Destroy();
}

IThread * CExecutive::GetCurrentThread(void)
{
	return (IThread *) m_pAlias;
}

UINT CExecutive::GetCurrentIndex(void)
{
	return m_pAlias->GetIndex();
}

UINT CExecutive::ToTicks(UINT uTime)
{
	return phal->TimeToTicks(uTime);
}

UINT CExecutive::ToTime(UINT uTicks)
{
	return phal->TicksToTime(uTicks);
}

void CExecutive::Sleep(UINT uTime)
{
	if( m_pThread ) {

		if( !m_pThread->IsBaseLevel() ) {

			Hal_CheckIrql(IRQL_TASK);

			UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

			if( !m_pThread->IsCancelPending() ) {

				m_pThread->InitSleep(uTime);

				Schedule();
			}

			Hal_LowerIrql(irql);

			m_pThread->CheckCancellation();

			return;
		}

		AfxAssert(FALSE);
	}

	phal->SpinDelay(uTime);
}

BOOL CExecutive::ForceSleep(UINT uTime)
{
	if( m_pThread ) {

		if( !m_pThread->IsBaseLevel() ) {

			Hal_CheckIrql(IRQL_TASK);

			UINT uSlept = m_pThread->GetSlept();

			if( uSlept < uTime ) {

				UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

				if( !m_pThread->IsCancelPending() ) {

					m_pThread->InitSleep(uTime - uSlept);

					Schedule();
				}

				Hal_LowerIrql(irql);

				m_pThread->CheckCancellation();

				m_pThread->GetSlept();

				return TRUE;
			}

			return FALSE;
		}

		AfxAssert(FALSE);
	}

	phal->SpinDelay(uTime);

	return TRUE;
}

UINT CExecutive::GetTickCount(void)
{
	return phal->GetTickCount();
}

UINT CExecutive::AddNotify(IThreadNotify *pNotify)
{
	Hal_CheckMaxIrql(IRQL_DISPATCH);

	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	INDEX n = m_NotifyList.Append(pNotify);

	Hal_LowerIrql(irql);

	return UINT(n);
}

BOOL CExecutive::RemoveNotify(UINT uNotify)
{
	Hal_CheckMaxIrql(IRQL_DISPATCH);

	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	m_NotifyList.Remove(INDEX(uNotify));

	Hal_LowerIrql(irql);

	return TRUE;
}

// IWaitMultiple

UINT METHOD CExecutive::WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime)
{
	Hal_CheckIrql(IRQL_TASK);

	AfxAssert(!m_pThread->IsBaseLevel());

	if( !m_pThread || !uTime ) {

		UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

		for( UINT n = 0; n < uCount; n++ ) {

			CWaitable *pWait = (CWaitable *) pList[n]->GetWaitable();

			if( !pWait->WaitInit(m_pThread, n, false) ) {

				Hal_LowerIrql(irql);

				return 1 + n;
			}
		}

		Hal_LowerIrql(irql);

		return 0;
	}

	return m_pThread->WaitMultiple(pList, uCount, uTime);
}

UINT METHOD CExecutive::WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime)
{
	IWaitable *pList[3];

	pList[0] = pWait1;
	pList[1] = pWait2;
	pList[2] = pWait3;

	return WaitMultiple(pList, 3, uTime);
}

UINT METHOD CExecutive::WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime)
{
	IWaitable *pList[2];

	pList[0] = pWait1;
	pList[1] = pWait2;

	return WaitMultiple(pList, 2, uTime);
}

// IExecHook

void CExecutive::ExecTick(void)
{
	// This is called by the HAL every timer interrupt. Note
	// that all the usual interrupt processing occurs on either
	// side of this call, including the Accumulate calls and a
	// call to DispatchThread if we're returning to IRQL_TASK.

	for( CExecThread *pThread = m_pHeadThread; pThread; pThread = pThread->m_pNextInExec ) {

		pThread->OnTick(m_uTick);
	}

	ServiceTimers(1);
}

bool CExecutive::HasPendingDispatch(void)
{
	// This is called by the HAL when the IRQL is about
	// to return to IRQL_TASK as a result of anything
	// other than a return from interrupt. If we return
	// true, the HAL will force a software interrupt to
	// allow us to perform a dispatch.

	return m_fPend;
}

void CExecutive::DispatchThread(DWORD Frame)
{
	// This is called by the HAL when the IRQL is about
	// to return to IRQL_TASK as a result of a return from
	// an interrupt. This is the point at which we check
	// for the need to switch contexts to a new thread.

	Dispatch(Frame);
}

void CExecutive::AccumThreadTime(bool fTimer)
{
	// This is called by the HAL at the start of each interrupt,
	// allowing us to update the thread run time. The HAL lets us
	// know if this is a timer interrupt, or something else.

	if( !fTimer ) {

		// If it's something else, just get the delta
		// and add it to the accumulated time for the
		// currently executing thread.

		m_pThread->AddRunTime(GetTimeDelta());
	}
	else {
		// If it's the timer, increment the reference
		// timer we use to check our timing math, and
		// accumulate a whole tick less the fraction
		// at which we started running this thread.

		m_RefTime += m_nWhole;

		m_pThread->AddRunTime(m_nWhole - m_nFract);

		m_nFract = 0;
	}

// We can now check to see if the thread's timeslice
// has expired. Doing this on every interrupt lets
// us provide more accurate rescheduling that is at
// least somewhat independent of the timer frequency.

	if( m_pThread->IsSliceExpired() ) {

		// TODO -- We could implement the hog timer
		// using this same mechanism. The slice would
		// be the run time, and we'd then sleep for
		// the delay time and then come back to life.

		m_pThread->ResetSlice();

		if( !m_pThread->IsOnlyThread() ) {

			Schedule();
		}
	}
}

void CExecutive::AccumInterruptTime(void)
{
	// This is called by the HAL at the end of each interrupt,
	// allowing us to update the interrupt time and reset the
	// delta timer so we can accurately track thead run time.

	m_IntTime += GetTimeDelta();
}

bool CExecutive::SuspendThread(void)
{
	// This is called by the HAL in debug mode if the current
	// thread hits a breakpoint or software interrupt. We do
	// not allow the base level or reaper to be suspended.

	if( m_pThread->GetIdent() >= 3 ) {

		m_pThread->Suspend();

		return true;
	}

	return false;
}

// Impersonation

CExecThread * CExecutive::Impersonate(CExecThread *pThread)
{
	// This is deprecated !!!

	UINT ipr;

	HostRaiseIpr(ipr);

	CExecThread *pPrev  = m_pAlias;

	m_pAlias            = pThread ? pThread : m_pThread;

	m_pThread->SetAlias(pThread);

	HostLowerIpr(ipr);

	return pPrev;
}

// Operations

void CExecutive::RemoveThread(CExecThread *pThread)
{
	// Prevent any scheduling right now.

	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	// Call the thread cleanup functions.

	for( INDEX n = m_NotifyList.GetHead(); !m_NotifyList.Failed(n); m_NotifyList.GetNext(n) ) {

		m_NotifyList[n]->OnThreadDelete(pThread, pThread->GetIndex(), 0);
	}

// Remove from group's thread list.

	CExecGroup *pGroup = pThread->m_pGroup;

	AfxListRemove(pGroup->m_pHeadThread,
		      pGroup->m_pTailThread,
		      pThread,
		      m_pNextInGroup,
		      m_pPrevInGroup
	);

// If group is empty, it can be deleted.

	if( !pGroup->m_pHeadThread ) {

		AfxListRemove(m_pHeadGroup,
			      m_pTailGroup,
			      pGroup,
			      m_pNextInExec,
			      m_pPrevInExec
		);

		delete pGroup;
	}
	else {
		// If we were active (which we almost certainly were)
		// then we have to update the group's active pointer
		// to something more sensible. Find an active thread,
		// or failing that just use the next one.

		if( pGroup->m_pActive == pThread ) {

			CExecThread *pStart = pThread->m_pNextInGroup ? pThread->m_pNextInGroup : pGroup->m_pHeadThread;

			CExecThread *pCheck = pStart;

			bool         fFirst = true;

			for( ;;) {

				if( (!fFirst && pCheck == pStart) || (pCheck->IsReady() && !pCheck->IsSuspended()) ) {

					pGroup->m_pActive = pCheck;

					break;
				}

				pCheck = pCheck->m_pNextInGroup ? pCheck->m_pNextInGroup : pGroup->m_pHeadThread;

				fFirst = false;
			}
		}
	}

// Don't want timer interrupts here.

	Hal_RaiseIrql(IRQL_TIMER);

	// Remove from global thread list.

	AfxListRemove(m_pHeadThread,
		      m_pTailThread,
		      pThread,
		      m_pNextInExec,
		      m_pPrevInExec
	);

// And we're done.

	Hal_LowerIrql(irql);
}

bool CExecutive::Schedule(void)
{
	// This gets called when we yield the processor because
	// of a timeslice expiration or a wait. If we have more
	// than one thread in our group, we should have a look
	// to find a peer thread that is ready to run.

	if( !m_fPend ) {

		CExecGroup *pGroup = m_pThread->m_pGroup;

		if( pGroup->m_pHeadThread != pGroup->m_pTailThread ) {

			CExecThread *pWalk = m_pThread;

			for( ;;) {

				// Look for the next task in the group, looping
				// around if we reach the end of the linked list.

				pWalk = pWalk->m_pNextInGroup ? pWalk->m_pNextInGroup : pGroup->m_pHeadThread;

				if( pWalk == m_pThread ) {

					// If we are back to ourselves, there is no other
					// thread in our group ready to run at this point.

					if( m_pThread->IsWaiting() || !m_pThread->IsReady() ) {

						// We're not ready or we're waiting, so set the pending
						// flag without specifying a new thread, which indicates
						// that we want to yield the processor. In the waiting 
						// case, this fires WaitDone for us. This branch is not
						// taken on a timeslice expiration, as then we just want
						// to keep running.

						m_fPend = true;
					}

					return false;
				}

				UINT ipr;

				HostRaiseIpr(ipr);

				if( m_fPend ) {

					// Someone else has pended a thread of a higher
					// priority, so we can stop worry about all this.

					HostLowerIpr(ipr);

					return false;
				}

				if( pWalk->IsReady() && !pWalk->IsSuspended() ) {

					// We've found a thread that is ready to run.

					m_pPend = pWalk;

					m_fPend = true;

					// We store the active task in the group record,
					// so that if we get preempted, we will return to
					// the right task when we get to run again.

					pGroup->m_pActive = pWalk;

					HostLowerIpr(ipr);

					return true;
				}

				HostLowerIpr(ipr);
			}
		}

	// Send the pending flag to request a dispatch and
	// allow Dispatch to find a suitable task to run.

		m_fPend = true;
	}

	return false;
}

bool CExecutive::Schedule(CExecThread *pThread)
{
	// This gets called when a thread becomes capable of
	// running. We get that it is not suspended, and then
	// if it is in a higher priority group, we schedule it.

	UINT ipr;

	HostRaiseIpr(ipr);

	if( m_pPend != pThread ) {

		if( !pThread->IsSuspended() ) {

			UINT p1 = pThread->GetPriority();

			UINT p2 = m_pPend->GetPriority();

			if( p1 > p2 ) {

				m_pPend = pThread;

				m_fPend = true;

				// We can set the active pointer to the new task,
				// as there cannot have been a previous active task
				// in that group, or it would be running already.

				pThread->m_pGroup->m_pActive = pThread;

				// And we can reset this thread's timeslice as it
				// must have previously yielded for some reason.

				m_pPend->ResetSlice();

				HostLowerIpr(ipr);

				return true;
			}

		// We want to run but can't right now, and we're not
		// at the same priority as the current thread so we
		// should make sure the group active thread is updated.

			if( p1 < p2 ) {

				pThread->m_pGroup->m_pActive = pThread;
			}

		// And we still clear the timeslice as we must
		// have previously yielded and have transitioned
		// back to ready.

			m_pPend->ResetSlice();
		}
	}

	HostLowerIpr(ipr);

	return false;
}

void CExecutive::CheckGroup(CExecThread *pThread)
{
	// This called when a thread is suspended by a higher priority
	// thread. This is a special case as it's the only time a thread
	// can become non-ready other than when it's running. So we have
	// to make sure the group active pointer remains valid.

	CExecGroup *pGroup = pThread->m_pGroup;

	if( pGroup->m_pActive == pThread ) {

		if( pGroup->m_pHeadThread != pGroup->m_pTailThread ) {

			CExecThread *pWalk = pThread;

			for( ;;) {

				// Look for the next task in the group, looping
				// around if we reach the end of the linked list.

				pWalk = pWalk->m_pNextInGroup ? pWalk->m_pNextInGroup : pGroup->m_pHeadThread;

				if( pWalk == pThread ) {

					// If we are back to ourselves, there is no other
					// thread in our group ready to run at this point.

					break;
				}

				if( pWalk->IsReady() && !pWalk->IsSuspended() ) {

					// We've found a thread that is ready to run, so
					// we store the active task in the group record
					// so that when our group becomes active again,
					// the scheduler will find the right thread.

					pGroup->m_pActive = pWalk;

					break;
				}
			}
		}
	}
}

bool CExecutive::InitWait(IWaitable *pWait, UINT uTime)
{
	if( WaitMultiple(&pWait, 1, uTime) ) {

		return true;
	}

	return false;
}

void CExecutive::ReapThread(CExecThread *pThread)
{
	// Run the Reaper on the specified thread.

	Hal_CheckIrql(IRQL_TASK);

	if( m_pThread == pThread ) {

		if( !m_pReapThread ) {

			m_pReapBusy->Wait(FOREVER);

			UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

			m_pReapThread = pThread;

			m_pReapFlag->Set();

			Hal_LowerIrql(irql);

			// The call above should never return as the
			// Reaper will run and suspend us immediately.

			HostTrap(PANIC_EXECUTIVE_UNEXPECTED);
		}

		HostTrap(PANIC_EXECUTIVE_REAPER_BUSY);
	}

	HostTrap(PANIC_EXECUTIVE_WRONG_THREAD);
}

// Timer Support

void CExecutive::CreateTimer(CExecTimer *pTimer)
{
	UINT irql = Hal_RaiseIrql(IRQL_TIMER);

	AfxListAppend(m_pHeadTimer, m_pTailTimer, pTimer, m_pNext, m_pPrev);

	Hal_LowerIrql(irql);
}

void CExecutive::DeleteTimer(CExecTimer *pTimer)
{
	UINT irql = Hal_RaiseIrql(IRQL_TIMER);

	AfxListRemove(m_pHeadTimer, m_pTailTimer, pTimer, m_pNext, m_pPrev);

	Hal_LowerIrql(irql);
}

// Creation Helper

IThread * CExecutive::CreateThread(PENTRY pfnProc, IClientProcess *pProc, UINT uLevel, void *pParam, UINT uParam, ISemaphore *pTms)
{
	CExecThread *pThread = New CExecThread(this);

	pThread->AddRef();

	if( pThread->Create(m_uAlloc++, pfnProc, pProc, uLevel, pParam, uParam, pTms) ) {

		UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

		// Call the thread creation functions.

		for( INDEX n = m_NotifyList.GetHead(); !m_NotifyList.Failed(n); m_NotifyList.GetNext(n) ) {

			m_NotifyList[n]->OnThreadCreate(pThread, pThread->GetIndex());
		}

	// Find and if required create a group for us.

		UINT	    uLevel = pThread->GetPriority();

		CExecGroup *pGroup = m_pHeadGroup;

		while( pGroup && pGroup->m_uLevel < uLevel ) {

			pGroup = pGroup->m_pNextInExec;
		}

		if( !(pGroup && pGroup->m_uLevel == uLevel) ) {

			CExecGroup *pBefore = pGroup;

			pGroup = New CExecGroup;

			pGroup->m_pActive     = pThread;

			pGroup->m_pHeadThread = NULL;

			pGroup->m_pTailThread = NULL;

			pGroup->m_uLevel      = uLevel;

			AfxListInsert(m_pHeadGroup,
				      m_pTailGroup,
				      pGroup,
				      m_pNextInExec,
				      m_pPrevInExec,
				      pBefore
			);
		}

	// Add to the group list.

		AfxListAppend(pGroup->m_pHeadThread,
			      pGroup->m_pTailThread,
			      pThread,
			      m_pNextInGroup,
			      m_pPrevInGroup
		);

		pThread->m_pGroup = pGroup;

		// Don't want timer interrupts here.

		Hal_RaiseIrql(IRQL_TIMER);

		// Add to the global thread list.

		CExecThread *pBefore = m_pHeadThread;

		while( pBefore && pBefore->GetPriority() <= uLevel ) {

			pBefore = pBefore->m_pNextInExec;
		}

		AfxListInsert(m_pHeadThread,
			      m_pTailThread,
			      pThread,
			      m_pNextInExec,
			      m_pPrevInExec,
			      pBefore
		);

		Hal_LowerIrql(IRQL_DISPATCH);

		// Schedule the thread if appropriate.

		ScheduleNew(pThread);

		Hal_LowerIrql(irql);

		return pThread;
	}

	pThread->Release();

	pThread->Release();

	return NULL;
}

// Implementation

void CExecutive::ScheduleNew(CExecThread *pThread)
{
	if( !m_pThread ) {

		m_pPend  = pThread;

		m_fPend  = true;

		return;
	}

	Schedule(pThread);
}

void CExecutive::Dispatch(DWORD Frame)
{
	for( ;;) {

		UINT ipr;

		HostRaiseIpr(ipr);

		if( m_fPend ) {

			HostLowerIpr(ipr);

			if( m_pPend == m_pThread ) {

				// If we get here, we are trying to find a lower
				// priority task that is capable of execution. 

				HostMaxIpr();

				// Start with the active thread in our group. We
				// only look at these tasks, as if they are not
				// ready, the entire group can be skipped.

				m_pPend = m_pPend->m_pGroup->m_pActive;

				HostLowerIpr(ipr);

				for( ;;) {

					HostMaxIpr();

					// Select this thread if it is capable of execution.

					if( m_pPend->IsReady() && !m_pPend->IsSuspended() ) {

						break;
					}

				// Jump to the active thread in the previous group.

					m_pPend = m_pPend->m_pGroup->m_pPrevInExec->m_pActive;

					HostLowerIpr(ipr);
				}

				HostLowerIpr(ipr);
			}

			m_fPend = false;

			SwitchTask((CExecThread *) m_pPend, Frame);

			continue;
		}

		return;
	}
}

void CExecutive::SwitchTask(CExecThread *pThread, DWORD Frame)
{
	if( m_pThread != pThread ) {

		pThread->WaitDone();

		CExecThread *pActive = m_pThread;

		void *pSave = pActive ? pActive->GetContext() : NULL;

		void *pLoad = pThread ? pThread->GetContext() : NULL;

		m_pThread          = pThread;

		m_pAlias           = pThread->GetAlias();

		_thread_impure_ptr = (struct _reent *) m_pAlias->GetImpure();

		_thread_eh_data    = m_pThread->GetEhData();

		phal->SwitchContext(pSave, pLoad, Frame);

		return;
	}

	pThread->WaitDone();
}

UINT CExecutive::GetTimeDelta(void)
{
	// Report the ellapsed time in microseconds since the
	// last call to this function. To be safe, we make sure
	// the return value never goes negative, although in
	// reality this should never happen...

	INT nFract = phal->GetTickFraction();

	INT nDelta = nFract - m_nFract;

	m_nFract   = nFract;

	return Max(0, nDelta);
}

// IDiagProvider

UINT CExecutive::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagTasks(pOut, pCmd);

		case 2:
			return DiagGroups(pOut, pCmd);

		case 3:
			return DiagTrace(pOut, pCmd);

		case 4:
			return DiagClear (pOut, pCmd);
	}

	return 0;
}

// Diagnostics

BOOL CExecutive::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "exec");

		pDiag->RegisterCommand(m_uProv, 1, "threads");

		pDiag->RegisterCommand(m_uProv, 2, "groups");

		pDiag->RegisterCommand(m_uProv, 3, "trace");

		pDiag->RegisterCommand(m_uProv, 4, "clear");

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

BOOL CExecutive::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

UINT CExecutive::DiagTasks(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(8);

		pOut->SetColumn(0, "ID", "%u");
		pOut->SetColumn(1, "Name", "%s");
		pOut->SetColumn(2, "Level", "%u");
		pOut->SetColumn(3, "Time", "%+s");
		pOut->SetColumn(4, "Share", "%u.%1.1u%%");
		pOut->SetColumn(5, "Stack", "%u.%1.1u%%");
		pOut->SetColumn(6, "Flags", "%s");
		pOut->SetColumn(7, "Location", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		INT64 total = m_IntTime;

		for( CExecThread *pThread = m_pHeadThread; pThread; pThread = pThread->m_pNextInExec ) {

			total += pThread->GetRunTime();
		}

		UINT width = Format(total, 0).GetLength();

		for( CExecThread *pThread = m_pHeadThread; pThread; pThread = pThread->m_pNextInExec ) {

			char sFlags[128] = { 0 };

			char sFunc[256] = { 0 };

			pThread->GetFlagsAsText(sFlags);

			pThread->GetFuncName(sFunc);

			UINT64 t = pThread->GetRunTime();

			UINT   p = UINT((1000 * t + total / 2) / total);

			UINT   s = pThread->GetStackUsage();

			pOut->AddRow();

			pOut->SetData(0, pThread->GetIdent());
			pOut->SetData(1, pThread->GetName());
			pOut->SetData(2, pThread->GetPriority());
			pOut->SetData(3, PCTXT(Format(t, width)));
			pOut->SetData(4, p/10, p%10);
			pOut->SetData(5, s/10, s%10);
			pOut->SetData(6, sFlags);
			pOut->SetData(7, sFunc);

			pOut->EndRow();
		}

		UINT64 t = m_IntTime;

		UINT   p = UINT((1000 * t + total / 2) / total);

		pOut->AddRow();

		pOut->SetData(0, 0);
		pOut->SetData(1, "Interrupt");
		pOut->SetData(3, PCTXT(Format(t, width)));
		pOut->SetData(4, p/10, p%10);

		pOut->EndRow();

		pOut->AddRule('-');

		pOut->AddRow();

		pOut->SetData(3, PCTXT(Format(total, width)));
		pOut->SetData(4, 100, 0);

		pOut->EndRow();

		pOut->AddRow();

		pOut->SetData(3, PCTXT(Format(m_RefTime, width)));

		pOut->EndRow();

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CExecutive::DiagClear(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		UINT ipr;

		HostRaiseIpr(ipr);

		m_IntTime = 0;

		for( CExecThread *pThread = m_pHeadThread; pThread; pThread = pThread->m_pNextInExec ) {

			pThread->ResetRunTime();
		}

		m_RefTime = 0;

		HostLowerIpr(ipr);

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CExecutive::DiagGroups(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(3);

		pOut->SetColumn(0, "Level", "%u");
		pOut->SetColumn(1, "Count", "%u");
		pOut->SetColumn(2, "Active", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		for( CExecGroup *pGroup = m_pHeadGroup; pGroup; pGroup= pGroup->m_pNextInExec ) {

			UINT uCount = 0;

			for( CExecThread *pThread = pGroup->m_pHeadThread; pThread; pThread = pThread->m_pNextInGroup ) {

				uCount++;
			}

			pOut->AddRow();

			pOut->SetData(0, pGroup->m_uLevel);
			pOut->SetData(1, uCount);
			pOut->SetData(2, pGroup->m_pActive->GetName());

			pOut->EndRow();
		}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CExecutive::DiagTrace(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		CExecThread *pThread;

		if( FindThread(pOut, &pThread, pCmd->GetArg(0)) ) {

			pOut->Print("Stack trace for %s:\n\n", pThread->GetName());

			phal->StackTrace(pOut, pThread->GetContext());

			return 0;
		}

		return 1;
	}

	pOut->Error(NULL);

	return 1;
}

BOOL CExecutive::FindThread(IDiagOutput *pOut, CExecThread **ppThread, PCTXT pName)
{
	for( CExecThread *pThread = m_pHeadThread; pThread; pThread = pThread->m_pNextInExec ) {

		bool fHit = false;

		if( isdigit(pName[0]) ) {

			fHit = (pThread->GetIndex() == atoi(pName));
		}
		else
			fHit = !strcasecmp(pThread->GetName(), pName);

		if( fHit ) {

			if( m_pThread == pThread ) {

				pOut->Error("cannot use current thread");

				return FALSE;
			}

			*ppThread = pThread;

			return TRUE;
		}
	}

	pOut->Error("cannot find thread '%s'", pName);

	return FALSE;
}

CString CExecutive::Format(UINT64 t, int w)
{
	if( t ) {

		CString s;

		while( t ) {

			UINT64 m = t % 1000;

			UINT64 d = t / 1000;

			if( s.IsEmpty() ) {

				PCTXT  f = d ? "%3.3u" : "%u";

				s.Printf(f, UINT(m));
			}
			else {
				PCTXT  f = d ? "%3.3u,%s" : "%u,%s";

				s = CPrintf(f, UINT(m), PCTXT(s));
			}

			t = d;
		}

		if( w && s.GetLength() < w ) {

			return CString(' ', w - s.GetLength()) + s;
		}

		return s;
	}

	if( w > 1 ) {

		return CString(' ', w - 1) + "0";
	}

	return "0";
}

// Entry Point

int CExecutive::BaseLevel(IThread *pThread, void *pParam, UINT uParam)
{
	AfxTrace("exec: base level created\n");

	pThread->SetName("Base");

	// We start by registering our sync objects.

//	piob->RegisterInstantiator("exec.mutex",     Create_Mutex       );
//	piob->RegisterInstantiator("exec.qutex",     Create_Qutex       );
//	piob->RegisterInstantiator("exec.semaphore", Create_Semaphore   );
//	piob->RegisterInstantiator("exec.event-m",   Create_ManualEvent );
//	piob->RegisterInstantiator("exec.event-a",   Create_AutoEvent   );

	// All is good, so we can indicate the exec is running.

	phal->SetExecRunning();

	// And create the thread used to clean-up other threads.

	m_pThis->m_pReapFlag = ::Create_AutoEvent();

	m_pThis->m_pReapBusy = ::Create_AutoEvent();

	::CreateThread(ReaperProc, 99000, NULL, 0);

	// Now we're all set we can extract the
	// parameters for the real new thread
	// and create it per the original call.

	PENTRY pfnProc = PENTRY(PDWORD(pParam)[0]);
	UINT   uLevel  = UINT(PDWORD(pParam)[1]);
	PVOID  pParam1 = PVOID(PDWORD(pParam)[2]);
	UINT   uParam1 = UINT(PDWORD(pParam)[3]);

	::CreateThread(pfnProc, uLevel, pParam1, uParam1);

	// This is now the base level, so we can just
	// loop around, entering the system idle state.

	for( ;;) phal->EnterIdleState();
}

int CExecutive::ReaperProc(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("Reaper");

	for( ;;) {

		m_pThis->m_pReapBusy->Set();

		m_pThis->m_pReapFlag->Wait(FOREVER);

		((CExecThread *) m_pThis->m_pReapThread)->ReapThread();

		m_pThis->m_pReapThread = NULL;
	}

	return 0;
}

int CExecutive::ClientProc(IThread *pThread, void *pParam, UINT uParam)
{
	CBaseExecThread *pBase = (CBaseExecThread *) pThread->GetObject();

	return pBase->ClientProc(pParam, uParam);
}

// End of File
