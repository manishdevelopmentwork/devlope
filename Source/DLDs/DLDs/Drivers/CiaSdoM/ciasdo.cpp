
#include "intern.hpp"

#include "ciasdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Master Driver
//

// Instantiator

INSTANTIATE(CCANOpenSDOMaster);

// Constructor

CCANOpenSDOMaster::CCANOpenSDOMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CCANOpenSDOMaster::~CCANOpenSDOMaster(void)
{
	}

// Configuration

void MCALL CCANOpenSDOMaster::Load(LPCBYTE pData)
{
	}
	
void MCALL CCANOpenSDOMaster::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = 0xFF;

	m_uBaud = Config.m_uBaudRate;
	}
	
// Management

void MCALL CCANOpenSDOMaster::Attach(IPortObject *pPort)
{
	m_pData = new CCANPortHandler;

	pPort->Bind(m_pData);
	}

void MCALL CCANOpenSDOMaster::Detach(void)
{
	m_pData->Release();
	}

void MCALL CCANOpenSDOMaster::Open(void)
{
	}

// Device

CCODE MCALL CCANOpenSDOMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop     = GetByte(pData);

			m_pCtx->m_fSendInit = GetByte(pData);

			m_pCtx->m_fSentInit = FALSE;

			m_pCtx->m_uPing     = GetWord(pData);

			m_pCtx->m_uTime     = GetWord(pData);

			m_pCtx->m_bRestrict = GetByte(pData);

			m_pCtx->m_bDecode   = GetByte(pData);
			
			m_pCtx->m_uDelay    = GetWord(pData);

			m_pCtx->m_bTransfer = GetByte(pData);

			m_pCtx->m_uBackOff  = ToTicks(GetWord(pData));

			if( m_uBaud <= 10000 ) {

				m_pCtx->m_uTime *= 2;
				}

			if( m_uBaud <= 20000 ) {

				m_pCtx->m_uTime *= 2;
				}

			ClearHistory();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CCANOpenSDOMaster::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CCANOpenSDOMaster::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{	
	UINT uValue  = 0;
	
	switch( uFunc ) {
		
		case 1:
			uValue = atoi(Value);

			CContext * pCtx = (CContext *) pContext;

			pCtx->m_fSendInit = uValue & 0x01;

			pCtx->m_fSentInit = FALSE;

			return 1;
		}

	return 0;
	}


// Entry Points

CCODE MCALL CCANOpenSDOMaster::Ping(void)
{
	if( SendInit() ) {

		WORD wData;

		if( ReadWord(m_pCtx->m_bDrop, m_pCtx->m_uPing, 0, wData, TRUE) ) {

			return CCODE_SUCCESS;
			}

		SetError();
		}

	return CCODE_ERROR;
	}

CCODE MCALL CCANOpenSDOMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( SendInit() ) {

		UINT uSub   = GetSub(Addr);

		UINT uType  = Addr.a.m_Type;

		UINT uIndex = GetIndex(Addr);

		if( uType == addrByteAsByte ) {

			BYTE bData;

			if( ReadByte(m_pCtx->m_bDrop, uIndex, uSub, bData, FALSE) ) {

				pData[0] = bData;
			
				return 1;
				}

			SetError();

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		if( uType == addrWordAsWord ) {

			WORD wData;

			if( ReadWord(m_pCtx->m_bDrop, uIndex, uSub, wData, FALSE) ) {

				pData[0] = wData;

				return 1;
				}		
				
			SetError();

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		if( uType == addrLongAsLong ) {

			LONG lData;

			if( ReadLong(m_pCtx->m_bDrop, uIndex, uSub, lData, FALSE) ) {

				pData[0] = lData;

				return 1;
				}
	  
			SetError();

			return CCODE_ERROR | CCODE_NO_RETRY;
			} 

		SetError();
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CCANOpenSDOMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_uBackOff ) {

		UINT i = FindHistory(Addr.m_Ref);

		if( i != NOTHING ) {

			if( !TestHistory(i) ) {
			
				return CCODE_ERROR | CCODE_SILENT | CCODE_NO_RETRY;
				}
			
			UpdateHistory(i);
			}
		else {
			AllocHistory(Addr.m_Ref);
			}
		}
	
	////////
		
	if( SendInit() ) {

		UINT uSub   = GetSub(Addr);

		UINT uType  = Addr.a.m_Type;

		UINT uIndex = GetIndex(Addr);
			
		if( uType == addrByteAsByte ) {

			if( WriteByte(m_pCtx->m_bDrop, uIndex, uSub, BYTE(pData[0])) ) {
				
				return 1;
				}

			SetError();

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		if( uType == addrWordAsWord ) {

			if( WriteWord(m_pCtx->m_bDrop, uIndex, uSub, WORD(pData[0])) ) {

				return 1;
				}		
			
			SetError();

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		if( uType == addrLongAsLong ) {

			if( WriteLong(m_pCtx->m_bDrop, uIndex, uSub, LONG(pData[0])) ) {

				return 1;
				}		

			SetError();

			return CCODE_ERROR | CCODE_NO_RETRY;
			}

		SetError();
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL CCANOpenSDOMaster::SendInit(void)
{
	if( m_pCtx->m_fSendInit && !m_pCtx->m_fSentInit ) {

		m_TxData.wID      = 0;

		m_TxData.bCount   = 2;

		m_TxData.bData[0] = 0x1;

		m_TxData.bData[1] = m_pCtx->m_bDrop;

		for( UINT u = 0; u < 3; u++ ) {

			if( PutFrame() ) {
			
				m_pCtx->m_fSentInit = TRUE;

				if( m_pCtx->m_uDelay > 0 ) {

					Sleep(m_pCtx->m_uDelay);
					}
				else {
					Sleep(5);
					}
				break;
				}
			}

		return m_pCtx->m_fSentInit;
		}

	return TRUE;
	}

void CCANOpenSDOMaster::SetError(void)
{
	m_pCtx->m_fSentInit = FALSE;
	}

BOOL CCANOpenSDOMaster::GetFrame(void)
{
	return m_pData->GetFrame(m_RxData, m_pCtx->m_uTime);
	}

BOOL CCANOpenSDOMaster::PutFrame(void)
{
	return m_pData->PutFrame(m_TxData, 0);
	}

BOOL CCANOpenSDOMaster::WriteByte(BYTE bDrop, WORD wIndex, BYTE bSub, BYTE bData)
{
	StartSDO(bDrop);

	UINT u = 0;

	m_TxData.bCount     = 8;

	m_TxData.bData[u++] = 0x1 << 5 | FindUnusedBytes(3) << 2 | FindTransfer();

	m_TxData.bData[u++] = LOBYTE(wIndex);

	if( !IsIndexLimited() ) {
	
		m_TxData.bData[u++] = HIBYTE(wIndex);
		}

	if( !IsSubDisabled() ) {

		m_TxData.bData[u++] = bSub;
		}

	m_TxData.bData[u++] = bData;

	while( u < 8 ) {

		m_TxData.bData[u++] = 0;
		}

	return SendWrite(bDrop);
	}

BOOL CCANOpenSDOMaster::WriteWord(BYTE bDrop, WORD wIndex, BYTE bSub, WORD wData)
{
	StartSDO(bDrop);

	UINT u = 0;
	
	m_TxData.bCount     = 8;

	m_TxData.bData[u++] = 0x1 << 5 | FindUnusedBytes(2) << 2 | FindTransfer();

	m_TxData.bData[u++] = LOBYTE(wIndex);

	if( !IsIndexLimited() ) {

		m_TxData.bData[u++] = HIBYTE(wIndex);
		}

	if( !IsSubDisabled() ) {
	
		m_TxData.bData[u++] = bSub;
		}

	m_TxData.bData[u++] = LOBYTE(wData);

	m_TxData.bData[u++] = HIBYTE(wData);

	while( u < 8 ) {

		m_TxData.bData[u++] = 0;
		}

	return SendWrite(bDrop);
	}

BOOL CCANOpenSDOMaster::WriteLong(BYTE bDrop, WORD wIndex, BYTE bSub, LONG lData)
{
	StartSDO(bDrop);

	UINT u = 0;

	m_TxData.bCount     = 8;

	m_TxData.bData[u++] = 0x1 << 5 | FindUnusedBytes(0) << 2 | FindTransfer();

	m_TxData.bData[u++] = LOBYTE(wIndex);

	if( !IsIndexLimited() ) {

		m_TxData.bData[u++] = HIBYTE(wIndex);
		}

	if( !IsSubDisabled() ) {
	
		m_TxData.bData[u++] = bSub;
		}

	m_TxData.bData[u++] = LOBYTE(LOWORD(lData));
	m_TxData.bData[u++] = HIBYTE(LOWORD(lData));
	m_TxData.bData[u++] = LOBYTE(HIWORD(lData));
	m_TxData.bData[u++] = HIBYTE(HIWORD(lData));

	while( u < 8 ) {

		m_TxData.bData[u++] = 0;
		}

	return SendWrite(bDrop);
	}

BOOL CCANOpenSDOMaster::SendWrite(BYTE bDrop)
{
	for( int n = 0; n < 3; n++ ) {

		PutFrame();

		while( GetFrame() ) {

			if( CheckSDO(bDrop) && MatchMulti() ) {

				if( m_RxData.bData[0] >> 5 == 0x4 ) {

					if( EatError(m_RxData.bData + 4) ) {

						return TRUE;
						}

					return FALSE;
					}

				if( m_RxData.bData[0] >> 5 == 0x3 ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CCANOpenSDOMaster::ReadByte(BYTE bDrop, WORD wIndex, BYTE bSub, BYTE &bData, BOOL fFast)
{
	InitRead(bDrop, wIndex, bSub);

	if( SendRead(bDrop, fFast) ) {

		bData = m_RxData.bData[4 - m_pCtx->m_bRestrict];

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANOpenSDOMaster::ReadWord(BYTE bDrop, WORD wIndex, BYTE bSub, WORD &wData, BOOL fFast)
{
	InitRead(bDrop, wIndex, bSub);

	if( SendRead(bDrop, fFast) ) {

		wData = MAKEWORD( m_RxData.bData[4 - m_pCtx->m_bRestrict],
				  m_RxData.bData[5 - m_pCtx->m_bRestrict]
				  );

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANOpenSDOMaster::ReadLong(BYTE bDrop, WORD wIndex, BYTE bSub, LONG &lData, BOOL fFast)
{
	InitRead(bDrop, wIndex, bSub);

	if( SendRead(bDrop, fFast) ) {

		WORD w1 = MAKEWORD( m_RxData.bData[4 - m_pCtx->m_bRestrict],
				    m_RxData.bData[5 - m_pCtx->m_bRestrict]
				    );
		
		WORD w2 = MAKEWORD( m_RxData.bData[6 - m_pCtx->m_bRestrict],
				    m_RxData.bData[7 - m_pCtx->m_bRestrict]
				    );

		lData   = MAKELONG( w1,
				    w2
				    );

		return TRUE;
		}

	return FALSE;
	}

void CCANOpenSDOMaster::InitRead(BYTE bDrop, WORD wIndex, BYTE bSub)
{
	StartSDO(bDrop);

	UINT u = 0;

	m_TxData.bCount     = 8;

	m_TxData.bData[u++] = 0x2 << 5;

	m_TxData.bData[u++] = LOBYTE(wIndex);

	if( !IsIndexLimited() ) {

		m_TxData.bData[u++] = HIBYTE(wIndex);
		}

	if( !IsSubDisabled() ) {

		m_TxData.bData[u++] = bSub;
		}

	while( u < 8 ) {

		m_TxData.bData[u++] = 0;
		}
	}

BOOL CCANOpenSDOMaster::SendRead(BYTE bDrop, BOOL fFast)
{
	UINT t = fFast ? 1 : 3;

	for( UINT n = 0; n < t; n++ ) {

		PutFrame();

		while( GetFrame() ) {

			if( CheckSDO(bDrop) && MatchMulti() ) {

				if( m_RxData.bData[0] >> 5 == 0x2 ) {

					return TRUE;
					}

				return FALSE;
				}
			}
		}

	return FALSE;
	}

BOOL CCANOpenSDOMaster::MatchMulti(void)
{
	return !memcmp(m_TxData.bData + 1, m_RxData.bData + 1, 3);
	}

BOOL CCANOpenSDOMaster::CheckSDO(BYTE bDrop)
{
	return (m_RxData.wID == ((0xB << 7) | bDrop));
	}

void CCANOpenSDOMaster::StartSDO(BYTE bDrop)
{
	memset(&m_TxData, 0, sizeof(FRAME));
	
	m_TxData.wID = ((0xC << 7) | bDrop);
	}

BOOL CCANOpenSDOMaster::EatError(PCBYTE pData)
{
	return TRUE;
	}

BOOL CCANOpenSDOMaster::IsSubDisabled(void)
{
	return m_pCtx->m_bRestrict > 0;
	}

BOOL CCANOpenSDOMaster::IsIndexLimited(void)
{
	return m_pCtx->m_bRestrict > 1;
	}

UINT CCANOpenSDOMaster::GetSub(AREF Addr)
{
	if( m_pCtx->m_bDecode ) {

		return Addr.a.m_Offset & 0xFF;
		}

	return Addr.a.m_Extra | ((Addr.a.m_Table & 0x0F) << 4);
	}

UINT CCANOpenSDOMaster::GetIndex(AREF Addr)
{
	if( m_pCtx->m_bDecode ) {

		return Addr.a.m_Extra | ((Addr.a.m_Table & 0x0F) << 4) | (Addr.a.m_Offset & 0xFF00);
		}

	return Addr.a.m_Offset;
	}

UINT CCANOpenSDOMaster::FindUnusedBytes(UINT uCount)
{
	switch( m_pCtx->m_bTransfer ) {

		case TRANSFER_EXP:
			
			return uCount;

		case TRANSFER_NORM:
			
			return 0;
		}

	return uCount;
	}

UINT CCANOpenSDOMaster::FindTransfer(void)
{
	switch( m_pCtx->m_bTransfer ) {

		case TRANSFER_EXP:

			return 3;
		}

	return 0;
	}

// Write Throttling

void CCANOpenSDOMaster::ClearHistory(void)
{
	memset(m_pCtx->m_dwHistAddr, 0, sizeof(m_pCtx->m_dwHistAddr));

	memset(m_pCtx->m_dwHistTime, 0, sizeof(m_pCtx->m_dwHistTime));
	}

UINT CCANOpenSDOMaster::FindHistory(DWORD dwAddr)
{
	for( UINT i = 0; i < elements(m_pCtx->m_dwHistAddr); i ++ ) {

		if( m_pCtx->m_dwHistAddr[i] == dwAddr ) {

			return i;
			}
		}

	return NOTHING;
	}

UINT CCANOpenSDOMaster::AllocHistory(DWORD dwAddr)
{
	UINT iIdx = 0;

	for( UINT i = 1; i < elements(m_pCtx->m_dwHistAddr); i ++ ) {

		if( !m_pCtx->m_dwHistAddr[i] ) {

			iIdx = i;

			break;
			}
		
		if( m_pCtx->m_dwHistTime[i] < m_pCtx->m_dwHistTime[iIdx] ) {
		
			iIdx = i;
			}
		}

	m_pCtx->m_dwHistAddr[iIdx] = dwAddr;

	m_pCtx->m_dwHistTime[iIdx] = GetTickCount();

	return iIdx;
	}

void CCANOpenSDOMaster::UpdateHistory(UINT i)
{
	m_pCtx->m_dwHistTime[i] = GetTickCount();
	}

BOOL CCANOpenSDOMaster::TestHistory(UINT i)
{
	UINT uTime = GetTickCount() - m_pCtx->m_dwHistTime[i];

	return uTime >= m_pCtx->m_uBackOff;
	}

// End of File
