
#include "Intern.hpp"

#include "NandIdentity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Identity Manager
//

// Instantiator

global ICrimsonIdentity * Create_NandIdentity(UINT uStart, UINT uEnd)
{
	CNandBlock Start = CNandBlock(0, uStart);

	CNandBlock End   = CNandBlock(0, uEnd);

	return New CNandIdentity(Start, End);
}

// Constructor

CNandIdentity::CNandIdentity(CNandBlock const &Start, CNandBlock const &End)
{
	StdSetRef();

	m_BlockStart = Start;

	m_BlockEnd   = End;
}

// Destructor

CNandIdentity::~CNandIdentity(void)
{
}

// IUnknown

HRESULT CNandIdentity::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICrimsonIdentity);

	StdQueryInterface(ICrimsonIdentity);

	return E_NOINTERFACE;
}

ULONG CNandIdentity::AddRef(void)
{
	StdAddRef();
}

ULONG CNandIdentity::Release(void)
{
	StdRelease();
}

// ICrimsonIdentity

void CNandIdentity::Init(void)
{
	CNandClient::Init();

	m_PageCurrent = m_BlockStart;

	m_PageProps   = m_BlockStart;

	m_uPropsGen   = NOTHING;

	memset(m_App, 0, sizeof(m_App));

	memset(m_Name, 0, sizeof(m_Name));

	memset(m_Key, 0, sizeof(m_Key));

	memset(m_Serial, 0, sizeof(m_Serial));

	FindItems();
}

int CNandIdentity::GetOemName(PTXT pName, UINT uSize)
{
	if( m_uPropsGen < NOTHING ) {

		if( m_Name[0] ) {

			memcpy(pName, m_Name, uSize);

			return uSize;
		}
	}

	return 0;
}

int CNandIdentity::GetOemApp(PTXT pName, UINT uSize)
{
	if( m_uPropsGen < NOTHING ) {

		if( m_App[0] ) {

			memcpy(pName, m_App, uSize);

			return uSize;
		}
	}

	return 0;
}

bool CNandIdentity::GetKeyData(PBYTE pData, UINT uSize)
{
	if( m_uPropsGen < NOTHING ) {

		if( m_Key[0] || m_Key[1] ) {

			memcpy(pData, m_Key, uSize);

			return true;
		}
	}

	return false;
}

bool CNandIdentity::GetSerial(PTXT pName, UINT uSize)
{
	if( m_uPropsGen < NOTHING ) {

		if( m_Serial[0] ) {

			memcpy(pName, m_Serial, uSize);

			return true;
		}
	}

	return false;
}

bool CNandIdentity::SetOemName(PCTXT pName, UINT uSize)
{
	if( m_uPropsGen == NOTHING || memcmp(m_Name, pName, uSize) ) {

		memset(m_Name, 0, sizeof(m_Name));

		memcpy(m_Name, pName, uSize);

		return CommitPropPage();
	}

	return true;
}

bool CNandIdentity::SetOemApp(PCTXT pName, UINT uSize)
{
	if( m_uPropsGen == NOTHING || memcmp(m_App, pName, uSize) ) {

		memset(m_App, 0, sizeof(m_App));

		memcpy(m_App, pName, uSize);

		return CommitPropPage();
	}

	return true;
}

bool CNandIdentity::SetKeyData(PCBYTE pData, UINT uSize)
{
	if( m_uPropsGen == NOTHING || memcmp(m_Key, pData, uSize) ) {

		memset(m_Key, 0, sizeof(m_Key));

		memcpy(m_Key, pData, uSize);

		return CommitPropPage();
	}

	return true;
}

bool CNandIdentity::SetSerial(PCTXT  pName, UINT uSize)
{
	if( m_uPropsGen == NOTHING || memcmp(m_Serial, pName, uSize) ) {

		memset(m_Serial, 0, sizeof(m_Serial));

		memcpy(m_Serial, pName, uSize);

		return CommitPropPage();
	}

	return true;
}

// Implementation

bool CNandIdentity::FindItems(void)
{
	CNandPage Page;

	Page.Invalidate();

	while( GetNextGoodPage(Page) ) {

		if( ReadPage(Page) ) {

			PCPAGE pPage = PCPAGE(m_pPageData);

			if( pPage->Magic == magicEmpty ) {

				break;
			}

			if( pPage->Magic == magicOem ) {

				PCPROPS pProps = PCPROPS(m_pPageData);

				m_uPropsGen    = pPage->Generation;

				memcpy(m_App, pProps->App, sizeof(m_App));

				memcpy(m_Name, pProps->Name, sizeof(m_Name));

				memcpy(m_Key, pProps->Key, sizeof(m_Key));

				memcpy(m_Serial, pProps->Serial, sizeof(m_Serial));

				continue;
			}
		}

		m_PageCurrent = m_BlockStart;

		EraseAll(true);

		return false;
	}

	m_PageCurrent = Page;

	return true;
}

bool CNandIdentity::CommitPropPage(void)
{
	PPROPS p = PPROPS(m_pPageData);

	memset(p, 0xFF, m_uPageSize);

	memcpy(&p->App, &m_App, sizeof(m_App));

	memcpy(&p->Name, &m_Name, sizeof(m_Name));

	memcpy(&p->Key, &m_Key, sizeof(m_Key));

	memcpy(&p->Serial, &m_Serial, sizeof(m_Serial));

	p->Page.Generation = ++m_uPropsGen;

	p->Page.Magic      = magicOem;

	return WriteWithReloc(m_PageProps);
}

// Overridables

bool CNandIdentity::OnPageReloc(PCBYTE pData, CNandPage const &Dest)
{
	PCPAGE pPage = PCPAGE(m_pPageData);

	if( pPage->Magic == magicOem ) {

		m_PageProps = Dest;

		return true;
	}

	return false;
}

// End of File
