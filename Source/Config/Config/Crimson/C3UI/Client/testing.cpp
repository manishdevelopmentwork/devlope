
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Dynamic Class

AfxImplementDynamicClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	CButtonBitmap *pCats16 = New CButtonBitmap( CBitmap(L"ToolCats16"),
						    CSize(16, 16),
						    7
						    );

	CButtonBitmap *pCats24 = New CButtonBitmap( CBitmap(L"ToolCats24"),
						    CSize(24, 24),
						    7
						    );

	afxButton->AppendBitmap(0x2000, pCats16);
	
	afxButton->AppendBitmap(0x2100, pCats24);
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestFrameWnd;
	
	CRect Rect = CRect(230, 200, 1100, 900);

	if( GetSystemMetrics(SM_CXSCREEN) == 1024 ) {

		Rect = CRect(100, 50, 970, 750);
		}

	pWnd->Create( L"Crimson 3.0 UI Prototype",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect,
		      AfxNull(CWnd),
		      CMenu(L"HeadMenu"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());
	
	afxThread->SetStatusText(L"Hello World");

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	if( Msg.message == WM_MOUSEWHEEL ) {

		CPoint Pos = CPoint(Msg.lParam);

		Msg.hwnd = WindowFromPoint(Pos);
		}

	return CDialogViewWnd::IsModelessMessage(Msg);
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:
			
			Src.EnableItem(TRUE);
			
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestFrameWnd, CMainWnd);

// Constructor

CTestFrameWnd::CTestFrameWnd(void)
{
	m_Accel.Create(L"TestMenu");

	ShowEdge(FALSE);

	m_pDbase = New CDatabase(L"Test");

	m_pDbase->Init();
	}

// Destructor

CTestFrameWnd::~CTestFrameWnd(void)
{
	m_pDbase->Kill();

	delete m_pDbase;
	}

// Interface Control

void CTestFrameWnd::OnUpdateInterface(void)
{
	CMainWnd::OnUpdateInterface();
	}

void CTestFrameWnd::OnCreateStatusBar(void)
{
	m_pSB->AddGadget(New CTextGadget(200, L"Recompile", textRed));
	}

void CTestFrameWnd::OnUpdateStatusBar(void)
{
	m_pSB->GetGadget(200)->AdjustFlag(MF_DISABLED, !m_pDbase->GetRecomp());
	}

// Message Map

AfxMessageMap(CTestFrameWnd, CMainWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_SHOWUI)

	AfxDispatchControlType(IDM_FILE, OnCommandControl)
	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)

	AfxMessageEnd(CTestFrameWnd)
	};

// Accelerators

BOOL CTestFrameWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CTestFrameWnd::OnPostCreate(void)
{
	m_pView = m_pDbase->CreateView(viewSystem);

	m_pView->Attach(m_pDbase);

	afxMainWnd->PostMessage(WM_UPDATEUI);

	CMainWnd::OnPostCreate();
	}

void CTestFrameWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		m_pView->SendMessage( m_MsgCtx.Msg.message,
				      m_MsgCtx.Msg.wParam,
				      m_MsgCtx.Msg.lParam
				      );
		}

	CMainWnd::OnInitPopup(Menu, nIndex, fSystem);
	}

void CTestFrameWnd::OnShowUI(BOOL fShow)
{
	}

// Command Handlers

BOOL CTestFrameWnd::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_OPEN ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestFrameWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	Src.EnableItem(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Item
//

// Dynamic Class

AfxImplementDynamicClass(CTestItem, CMetaItem);

// Constructor

CTestItem::CTestItem(void)
{
	m_pTags = New CTagManager;
	}

// UI Creation

CViewWnd * CTestItem::CreateView(UINT uType)
{
	if( uType == viewSystem ) {

		return New CSystemWnd;
		}

	return NULL;
	}

// Item Naming

CString CTestItem::GetHumanName(void) const
{
	return CString();
	}

CString CTestItem::GetFixedName(void) const
{
	return CString();
	}

// Persistance

void CTestItem::Init(void)
{
	CMetaItem::Init();

	AddData();
	}

void CTestItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	AddData();
	}

// System Data

void CTestItem::AddData(void)
{
	CDatabase *pDbase = GetDatabase();

	pDbase->AddCategory(L"Data Tags", L"Tags",  0x21000001, 0x20000001);
	}

// Meta Data

void CTestItem::AddMetaData(void)
{
	Meta_AddObject(Tags);

	CMetaItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager
//

// Dynamic Class

AfxImplementDynamicClass(CTagManager, CMetaItem);

// Constructor

CTagManager::CTagManager(void)
{
	m_pTags = New CTagList;
	}

// UI Creation

CViewWnd * CTagManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		return New CNavTreeWnd( L"Tags",
					AfxRuntimeClass(CFolderItem),
					AfxRuntimeClass(CTagItem)
					);
		}

	if( uType == viewResource ) {

		return New CResTreeWnd( L"Tags",
					AfxRuntimeClass(CFolderItem),
					AfxRuntimeClass(CTagItem)
					);
		}

	if( uType == viewResPane ) {

		return New CResPaneWnd;
		}

	return CMetaItem::CreateView(uType);
	}

// Item Naming

CString CTagManager::GetHumanName(void) const
{
	return L"Tags";
	}

// Meta Data

void CTagManager::AddMetaData(void)
{
	Meta_AddCollect(Tags);

	CMetaItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag List
//

// Dynamic Class

AfxImplementDynamicClass(CTagList, CItemIndexList);

// Constructor

CTagList::CTagList(void)
{
	}

// Persistance

void CTagList::Init(void)
{
	for( UINT n = 0; n < 6; n++ ) {

		CMetaItem *pItem = New CTagItem;

		pItem->Init();

		pItem->SetName(CPrintf(L"Tag%u", n));

		AppendItem(pItem);
		}

	CItemIndexList::Init();
	}

//////////////////////////////////////////////////////////////////////////
//
// Tag Item
//

// Dynamic Class

AfxImplementDynamicClass(CTagItem, CUIItem);

// Constructor

CTagItem::CTagItem(void)
{
	m_Expr1 = "WAS Fred + Jim";
	}

// UI Update

void CTagItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	}

// Item Naming

CString CTagItem::GetHumanName(void) const
{
	return m_Name;
	}

// Meta Data

void CTagItem::AddMetaData(void)
{
	Meta_AddString(Name);
	Meta_AddString(Expr1);
	Meta_AddString(Expr2);
	Meta_AddString(Expr3);
	Meta_AddString(Expr4);

	CMetaItem::AddMetaData();

	Meta_SetName(L"Tag");
	}


//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Expression
//

// Dynamic Class

AfxImplementDynamicClass(CUIExpression, CUICategorizer);

// Constructor

CUIExpression::CUIExpression(void)
{
	}

// Category Overridables

BOOL CUIExpression::LoadModeButton(void)
{
	m_pModeCtrl->ClearOptions();

	CUIModeButton::COption Opt;

	Opt.m_uID     = 100;
	Opt.m_Text    = L"WAS";
	Opt.m_fEnable = TRUE;
	Opt.m_fHidden = TRUE;
	Opt.m_Image   = 0;

	m_pModeCtrl->AddOption(Opt);

	Opt.m_uID     = 0;
	Opt.m_Text    = L"Internal";
	Opt.m_fEnable = TRUE;
	Opt.m_fHidden = FALSE;
	Opt.m_Image   = 0;

	m_pModeCtrl->AddOption(Opt);

	Opt.m_uID     = 1;
	Opt.m_Text    = L"General";
	Opt.m_fEnable = TRUE;
	Opt.m_fHidden = FALSE;
	Opt.m_Image   = 0;

	m_pModeCtrl->AddOption(Opt);

	return TRUE;
	}

BOOL CUIExpression::SwitchMode(UINT uMode)
{
	if( uMode == 0 ) {

		StdSave(FALSE, L"");

		ForceUpdate();

		return TRUE;
		}

	if( uMode == 1 ) {

		if( m_uMode == 0 ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(L"");

			m_pEditCtrl->SetModify(TRUE);

			m_pEditCtrl->SetFocus();

			return TRUE;
			}

		if( m_uMode == 100 ) {

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(m_pShowCtrl->GetWindowText());

			m_pEditCtrl->SetModify(TRUE);

			m_pEditCtrl->SetFocus();

			return TRUE;
			}

		return TRUE;
		}

	return TRUE;
	}

UINT CUIExpression::FindDispMode(CString Text)
{
	if( Text.IsEmpty() ) {

		return 0;
		}

	if( Text.Left(4) == L"WAS " ) {

		return 100;
		}

	return 1;
	}

CString CUIExpression::FindDispText(CString Text, UINT uMode)
{
	if( uMode == 100 ) {

		return Text.Mid(4);
		}

	return Text;
	}

CString CUIExpression::FindDataText(CString Text, UINT uMode)
{
	if( uMode == 100 ) {

		return L"WAS " + Text;
		}

	return Text;
	}

UINT CUIExpression::FindDispType(UINT uMode)
{
	if( uMode == 100 ) {

		return 1;
		}

	if( uMode == 0 ) {

		return 0;
		}

	return 2;
	}

CString CUIExpression::FindDispVerb(UINT uMode)
{
	return L"";
	}

// End of File
