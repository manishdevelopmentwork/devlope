#include "intern.hpp"

#include "ti500v2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TI 500 NITP v2 Master Serial Driver
//

// Instantiator

INSTANTIATE(CTi500v2Driver);

// Constructor

CTi500v2Driver::CTi500v2Driver(void)
{
	m_Ident         = DRIVER_ID; 
	
	}

// Destructor

CTi500v2Driver::~CTi500v2Driver(void)
{
	}

// Configuration

void MCALL CTi500v2Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CTi500v2Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CTi500v2Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CTi500v2Driver::Open(void)
{
	}

// Device

CCODE MCALL CTi500v2Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_fBlock = GetByte(pData) ? TRUE : FALSE;
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CTi500v2Driver::DeviceClose(BOOL fPersist)
{
      	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

BOOL CTi500v2Driver::Transact(void)
{
	if( Send() && RecvFrame() ) {

		return CheckFrame();
		}

	return FALSE; 
	}

BOOL CTi500v2Driver::Send(void)
{
	m_pData->ClearRx();
	
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_uPtr; u++ ) {

		AfxTrace("%c", m_bTxBuff[u]);
		}    */

	return TRUE;
	}

BOOL CTi500v2Driver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	BOOL fBegin = FALSE;

//	AfxTrace("\nRx : ");

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%c", uData);

		if( !fBegin && uData == ':' ) {

			fBegin = TRUE;

			continue;
			}

		if( fBegin ) {

			if( uData == CR ) {

				m_uPtr -= 1;

				return TRUE;
				}

			m_bRxBuff[m_uPtr] = uData;

			m_uPtr++;
			}
		}

	return FALSE;
	}

BOOL CTi500v2Driver::IsBlockOp(void)
{
	if( m_pCtx->m_fBlock ) {

		return TRUE;
		}

	if( m_pItem ) {

		if( m_pItem->m_bRead == 0x9D ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CTi500v2Driver::AddElement(UINT uOffset, UINT uCount)
{
	if( m_pItem ) {

		UINT uType = 0;

		switch( m_pItem->m_bTable ) {

			case tableC:
			case tableCP:

				uType = 8;
				break;

			case tableX:
			case tableXP:

				uType = 6;
				break;

			case tableY:
			case tableYP:

				uType = 7;
				break;
			}

		if( uType > 0 ) {
		     
			AddByte(uType);

			AddByte(0x00);

			AddWord(uOffset - 1);

			AddByte(uCount);

			return;
			}

		MakeCategory1(uOffset);
		}
	}

void CTi500v2Driver::AddTaskCode(BOOL fWrite)
{
	if( m_pItem ) {

		switch( m_pItem->m_bTable ) {

			case tableC:
			case tableCP:
			case tableX:
			case tableXP:
			case tableY:
			case tableYP:

				AddByte(fWrite ? 0x59 : 0x6B);
				return;
			}
				
		AddByte(fWrite ? 0x5A : 0x7F);
		}
	}

// End of File
