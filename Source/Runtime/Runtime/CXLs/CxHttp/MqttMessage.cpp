
#include "Intern.hpp"

#include "MqttMessage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT Message
//

// Persistance

UINT CMqttMessage::GetBytes(void) const
{
	return 4 + m_Topic.GetLength() + m_Data.GetCount();
	}

BOOL CMqttMessage::Save(CByteArray &Data)
{
	WORD wLen = WORD(m_Data.GetCount());

	Data.Append(BYTE(m_uCode));

	Data.Append(PCBYTE(PCTXT(m_Topic)), m_Topic.GetLength() + 1);

	Data.Append(PCBYTE(&wLen), sizeof(wLen));

	Data.Append(m_Data);

	return TRUE;
	}

UINT CMqttMessage::Load(PCBYTE &pData)
{
	PCBYTE pInit = pData;

	m_uCode = *pData;

	pData += 1;

	m_Topic = PCTXT(pData);

	pData += 1 + m_Topic.GetLength();

	WORD wSize = PWORD(pData)[0];

	pData += 2;

	m_Data.Empty ();

	m_Data.Append(pData, wSize);

	pData += wSize;

	return pData - pInit;
	}

// End of File
