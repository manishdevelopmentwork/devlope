
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_UserManager_HPP

#define INCLUDE_UserManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// User Manager
//

class CUserManager : public IConfigUpdate
{
public:
	// Constructor
	CUserManager(void);

	// Destructor
	~CUserManager(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IConfigUpdate
	void METHOD OnConfigUpdate(char cTag);

	// Operations
	BOOL GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info);
	BOOL SetUserPass(CString const &User, CString const &From, CString const &To);

protected:
	// Data Members
	ULONG              m_uRefs;
	bool		   m_fLoad;
	IMutex	         * m_pLock;
	ISchemaGenerator * m_pSchema;
	IConfigStorage   * m_pConfig;
	CJsonConfig      * m_pUcon;

	// Implementation
	bool LoadUserConfig(void);
	bool AddToCache(CAuthInfo const &Auth, CUserInfo const &Info);
	bool RemoveFromCache(CString const &User);
	bool FindInCache(CString const &User, CAuthInfo const &Auth, CUserInfo &Info);
	bool CheckUser(CJsonConfig *pUser, CAuthInfo const &Auth);
	bool LoadUser(CJsonConfig *pUser, CUserInfo &Info);
};

// End of File

#endif
