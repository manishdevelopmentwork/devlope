
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbGeneric_HPP
	
#define	INCLUDE_UsbGeneric_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Generic Port
//

class CUsbGeneric : public CUsbPort
{
	public:
		// Constructor
		CUsbGeneric(IUsbHostFuncDriver *pDriver);

		// IDevice
		BOOL METHOD Open(void);

		// IPortObject
		void METHOD Bind(IPortHandler *pHandler);
		UINT METHOD GetPhysicalMask(void);
		BOOL METHOD Open(CSerialConfig const &Config);
		void METHOD Close(void);
		void METHOD Send(BYTE bData);

		// IUsbHostFuncEvents
		void METHOD OnData(void);

	protected:
		// Poll State
		enum
		{
			stateIdle,
			stateSend,
			stateRecv,
			stateWait,
			stateFail,
			};

		// Data
		IUsbHostModuleGeneric * m_pDriver;
		CSerialConfig           m_Config;
		IPortHandler          * m_pHandler;
		UINT volatile           m_uSendState;
		UINT volatile           m_uRecvState;
		BYTE			m_bSendData[512];
		BYTE			m_bRecvData[512];
		UINT			m_uSendCount;
		UINT			m_uRecvCount;

		// Overridables
		void OnDriverBind(IUsbHostFuncDriver *pDriver);
		void OnStartDriver(void);
		void OnTermDriver(void);
		
		// Implementation 
		void InitData(void);
		void OnRecv(void);
		void OnSend(void);
		bool StartSend(void);
		bool StartRecv(void);
		void SuckDry(void);
		bool PushData(void);
		bool PullData(void);
		bool WaitDone(UINT uTimeout);
	};

// End of File

#endif
