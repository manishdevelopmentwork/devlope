
#include "Intern.hpp"

#include "mbtcpmTester.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Master Driver Tester
//

// Instantiator

INSTANTIATE(CModbusTCPMasterTester);

// Constructor

CModbusTCPMasterTester::CModbusTCPMasterTester(void)
{
	}

// Destructor

CModbusTCPMasterTester::~CModbusTCPMasterTester(void)
{
	}

// IDriverTester

BOOL CModbusTCPMasterTester::GetSerialConfig(CSerialConfig &Config)
{
	return FALSE;
	}

BOOL CModbusTCPMasterTester::GetDriverConfig(PBYTE &pData)
{
	return TRUE;
	}

BOOL CModbusTCPMasterTester::GetDeviceConfig(PBYTE &pData)
{
	AddAddr(pData, CIpAddr("192.168.100.50"));	// Addr
	AddWord(pData, 502);				// Socket
	AddByte(pData, 1);				// Unit
	AddByte(pData, 0);				// Keep
	AddByte(pData, 0);				// Ping
	AddWord(pData, 5000);				// Time1
	AddWord(pData, 2500);				// Time2
	AddWord(pData, 200);				// Time3
	AddByte(pData, 0);				// Disable15 
	AddByte(pData, 0);				// Disable16 
	AddByte(pData, 0);				// Disable5  
	AddByte(pData, 0);				// Disable6  
	AddWord(pData, 1);				// PingReg
	AddWord(pData, 512);				// Max01     
	AddWord(pData, 512);				// Max02     
	AddWord(pData, 32);				// Max03     
	AddWord(pData, 32);				// Max04     
	AddWord(pData, 512);				// Max15     
	AddWord(pData, 32);				// Max16     
	AddAddr(pData, IP_EMPTY);			// Addr2
	AddByte(pData, 0);				// IgnoreReadEx
	AddByte(pData, 0);				// FlipLong
	AddByte(pData, 0);				// FlipReal

	return TRUE;
	}

BOOL CModbusTCPMasterTester::ExecDriverCtrl(ICommsDriver *pDriver)
{
	return TRUE;
	}

BOOL CModbusTCPMasterTester::ExecDeviceCtrl(ICommsDriver *pDriver)
{
	return TRUE;
	}

BOOL CModbusTCPMasterTester::ExecTest(CCODE &Code, ICommsMaster *pDriver, UINT uIndex)
{
	if( uIndex == 0 ) {

		CAddress Addr;

		Addr.a.m_Table  = 0x01;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1;

		DWORD Data[4];

		Code = pDriver->Read(Addr, Data, elements(Data));

		ShowRead(Code, uIndex, Addr, Data, elements(Data), TRUE);

		return TRUE;
		}

	if( uIndex == 1 ) {

		static WORD n = 0;

		CAddress Addr;

		Addr.a.m_Table  = 0x01;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1;

		DWORD Data[1] = { n++ };

		Code = pDriver->Write(Addr, Data, elements(Data));

		ShowWrite(Code, uIndex, Addr, Data, elements(Data), FALSE);

		return TRUE;
		}

	return FALSE;
	}

// End of File
