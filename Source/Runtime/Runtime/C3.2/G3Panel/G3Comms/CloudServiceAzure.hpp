
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudServiceAzure_HPP

#define	INCLUDE_CloudServiceAzure_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudServiceCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMqttClientOptionsAzure;

class CMqttClientAzure;

//////////////////////////////////////////////////////////////////////////
//
// Azure MQTT Service
//

class CCloudServiceAzure : public CCloudServiceCrimson
{
	public:
		// Constructor
		CCloudServiceAzure(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Service ID
		UINT GetID(void);
	};

// End of File

#endif
