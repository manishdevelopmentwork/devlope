#include "WatchList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Native Tag Watch List
//

// Constructors

CWatchList::CWatchList(void)
{
	m_pClient = NULL;

	m_fDirty = TRUE;

	m_fRebuild = FALSE;

	m_fData = FALSE;

	m_uThresh = 5;

	m_uTouch = 0;

	m_uInst = 0;

	m_uSize = 0;

	m_uCount = 0;

	m_pHead = NULL;

	m_pTail = NULL;

	m_pData = NULL;

	m_pNext = NULL;

	m_pTail = NULL;
	}

CWatchList::CWatchList(CCipClient *pClient)
{
	m_pClient = pClient;

	m_fDirty = TRUE;

	m_fRebuild = FALSE;

	m_fData = FALSE;

	m_uThresh = 5;

	m_uTouch = 0;

	m_uInst = 0;

	m_uSize = 0;

	m_uCount = 0;

	m_pHead = NULL;

	m_pTail = NULL;

	m_pData = NULL;

	m_pNext = NULL;

	m_pTail = NULL;
	}


// Destructor

CWatchList::~CWatchList(void)
{
	delete [] m_pData;
	}

// Tag Management

BOOL CWatchList::CreateInstance(void)
{
	m_uInst = m_pClient->CreateWatchList();

	if( m_uInst ) {
		
		return TRUE;
		}

	return FALSE;
	}

void CWatchList::AddTag(CTagInfo *pTag)
{
	AfxListAppend(  m_pHead,
			m_pTail,
			pTag,
			m_pNext,
			m_pPrev
			);

	pTag->m_pWatch = this;

	m_uCount += 1;

	m_uTouch += 1;

	m_uSize += pTag->GetSize() + 2;

	m_fDirty = TRUE;
	}

void CWatchList::RemoveTag(CTagInfo *pTag)
{
	AfxListRemove(  m_pHead,
			m_pTail,
			pTag,
			m_pNext,
			m_pPrev
			);

	pTag->m_pWatch   = NULL;

	m_uSize  -= (pTag->GetSize() + 2);

	m_uCount -= 1;
	}

BOOL CWatchList::ConfigureAllTags(void)
{
	if( m_uCount ) {

		m_fDirty = FALSE;

		UINT uPos = 2;
					
		CTagInfo *pTag;

		for( pTag = m_pHead; pTag; pTag = pTag->m_pNext ) {

			// Note -- Failure here isn't necessarily fatal for all tags
			// (it could just be a mistyped tag name), but there's no way
			// to recover at this level for this particular tag.

			if( !pTag->m_uPos ) {

				if( m_pClient->ConfigureWatchList(pTag->m_Path,
									m_uInst,
									pTag->m_uSize) ) {
					pTag->m_uPos = uPos;

					// Each tag has a two byte index in the watch list data buffer
					uPos += pTag->m_uSize + 2;
					}
				}
			else {
				uPos += pTag->m_uSize + 2;
				}
			}
				
		if( m_pClient->IsLayerUp() ) {
					
			m_uSize = uPos;

			m_pData = New BYTE [ m_uSize ];

			return TRUE;
			}
		}

	return FALSE;
	}

// Data Management

BOOL CWatchList::IsTooLarge(UINT TagSize)
{
	// Note -- The maximum watch list size was determined by observation.
	// It appears that they can contain ~500 bytes, so we'll be conservative
	// and use 480 bytes to ensure we don't lose data.
	
	if( (TagSize + m_uSize) > 480 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CWatchList::HasData(void)
{
	return m_fData;
	}

PBYTE CWatchList::GetData(void)
{
	return m_pData;
	}

void CWatchList::Touch(void)
{
	m_uTouch++;
	}

void CWatchList::ClearTouch(void)
{
	m_uTouch = 0;
	}

BOOL CWatchList::IsSparse(void)
{
	// NOTE -- Lists with many unused items get rebuilt with
	// the relevant members removed. If this results in a list
	// becoming empty, we delete it later in this method.
	// The threshold for deletion is currently 1/5, but
	// this can be changed arbitrarily if needed.

	if( m_uCount > m_uThresh ) {

		return m_uTouch == 0 || m_uTouch < (m_uCount / m_uThresh);
		}

	return FALSE;
	}

BOOL CWatchList::IsEmpty(void)
{
	return m_uInst != 0 && m_uCount == 0;
	}

void CWatchList::SetRebuild(void)
{
	m_fDirty = TRUE;

	m_fRebuild = TRUE;
	}

BOOL CWatchList::IsDirty(void)
{
	return m_fDirty;
	}

void CWatchList::Rebuild(void)
{
	if( m_fRebuild ) {

		if( m_uInst ) {

			m_pClient->DeleteWatchList(m_uInst);

			m_uInst = m_pClient->CreateWatchList();
			}

		delete [] m_pData;

		m_pData = NULL;

		m_fRebuild = FALSE;
		}
	}

BOOL CWatchList::UpdateData(void)
{
	m_fData = FALSE;

	if( m_uCount ) {

		if( m_uTouch ) {

			// Read the watch list into its buffer for
			// later consumption by calls to the Read methods.

			if( m_pClient->IsLayerUp() ) {

				if( m_pClient->GetWatchListData(m_pData,
								m_uInst,
								m_uSize) ) {

					m_fData = TRUE;

					return TRUE;
					}
				}

			return FALSE;
			}
		}

	// We have no data, but no error has occurred.
	return TRUE;
	}

void CWatchList::Delete(void)
{
	m_pClient->DeleteWatchList(m_uInst);
	}

// End of File
