
#include "intern.hpp"

#include "testing.hpp"

#include "uxtheme.h"

#include "tmschema.h"

#pragma	comment(lib, "uxtheme.lib")

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	CTestApp *pApp = New CTestApp(2);

	pApp->CreateSuspended();

	CRawThread &Thread = pApp->GetRawThread();

	CWaitable   Cloned = Thread;

	Thread.Resume();

	Cloned.WaitForObject();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Runtime Class

AfxImplementRuntimeClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(UINT n)
{
	m_n = n;
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pManager = New CTestWnd;

	CPoint Pos = CPoint(100, 100) + int(m_n) * CSize(80, 60);

	CSize Size = CSize (628, 494);

	CRect Rect = CRect (Pos, Size);

	pManager->Create( L"Test Application",
		          WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		          Rect, AfxNull(CWnd), CMenu(L"Test"),
		          NULL
		          );

	pManager->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_FILE_EXIT:

			Src.EnableItem(TRUE);
			
			break;
		
		default:
			return FALSE;
		}
		
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	}

// Message Map

AfxMessageMap(CTestWnd, CWnd)
{
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_SETSTATUSBAR)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_MOUSEWHEEL)

	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	afxStdTools->OpenTheme(L"Tab");

	DC.FillRect(GetClientRect(), afxBrush(MAGENTA));

	CRect Rect(100, 100, 400, 200);

	DrawThemeBackground( afxStdTools->m_hTheme,
			     DC,
			     TABP_TABITEM,
			     TIS_HOT,
			     Rect,
			     NULL
			     );

	afxStdTools->CloseTheme();
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CTestWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	CClientDC DC(ThisObject);

	CColor Col = DC.GetPixel(Pos);

	DC.Select(afxFont(Fixed));

	DC.TextOut( 0,
		    GetClientRect().bottom - 16,
		    CPrintf( "%3.3u %3.3u %3.3u",
			     Col.GetRed(),
			     Col.GetGreen(),
			     Col.GetBlue()
			     )
		    );

	DC.Deselect();
	}

void CTestWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	}

void CTestWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	}

void CTestWnd::OnSetStatusBar(PCTXT pText)
{
	}

void CTestWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	}

void CTestWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	}

// End of File
