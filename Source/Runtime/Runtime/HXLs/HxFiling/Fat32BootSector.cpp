
#include "Intern.hpp"

#include "Fat32BootSector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat32 Boot Sector Object
//

// Constructor

CFat32BootSector::CFat32BootSector(void)
{
	memset(this, 0x00, sizeof(Fat32BootSector));
	}

CFat32BootSector::CFat32BootSector(CPartitionEntry const &Partition)
{
	Init(Partition);
	}

// Initialisation

void CFat32BootSector::Init(CPartitionEntry const &Partition)
{
	memset(this, 0x00, sizeof(Fat32BootSector));

	m_bBootCode[0] = 0xEB;

	m_bBootCode[1] = 0x3C;

	m_bBootCode[2] = 0x90;
	
	m_bDriveNum    = 0x80;
	
	m_bBootSig     = constExtended;

	m_wMagic       = constMagic;
	
	m_dwVolId      = GetTickCount();

	memcpy(m_bOemName,    "MSDOS5.0",    sizeof(m_bOemName));

	memcpy(m_bVolLabel,   "NO NAME    ", sizeof(m_bVolLabel));

	memcpy(m_bFileSystem, "FAT32   ",    sizeof(m_bFileSystem));

	GetBiosParamBlock().Init(Partition);
	}

// Conversion

void CFat32BootSector::HostToLocal(void)
{
	GetBiosParamBlock().HostToLocal();
	
	m_dwVolId = HostToIntel(m_dwVolId);

	m_wMagic  = HostToIntel(m_wMagic);
	}

void CFat32BootSector::LocalToHost(void)
{
	GetBiosParamBlock().LocalToHost();
	
	m_dwVolId = IntelToHost(m_dwVolId);

	m_wMagic  = IntelToHost(m_wMagic);
	}

// Attributes

BOOL CFat32BootSector::IsValid(void) const
{
	if( m_wMagic != constMagic ) {

		return false;
		}
	
	if( m_bBootSig == constExtended ) {

		if( memcmp(m_bFileSystem, "FAT32   ", 8) ) {
			
			return false;
			}
		}

	if( !GetBiosParamBlock().IsValid() ) {

		return false;
		}

	return true;
	}

UINT CFat32BootSector::GetClusterSize(void) const
{
	return GetBiosParamBlock().GetClusterSize();
	}

UINT CFat32BootSector::GetRootDirSectors(void) const
{
	return GetBiosParamBlock().GetRootDirSectors();
	}

INT64 CFat32BootSector::GetDataSize(void) const
{
	return GetBiosParamBlock().GetDataSize();
	}

DWORD CFat32BootSector::GetDataSectors(void) const
{
	return GetBiosParamBlock().GetDataSectors();
	}

DWORD CFat32BootSector::GetDataClusters(void) const
{
	return GetBiosParamBlock().GetDataClusters();
	}

DWORD CFat32BootSector::GetFirstSector(DWORD dwCluster) const
{
	return GetBiosParamBlock().GetFirstSector(dwCluster);
	}

DWORD CFat32BootSector::GetFirstFatSector(WORD wFat) const
{
	return GetBiosParamBlock().GetFirstFatSector(wFat);
	}

DWORD CFat32BootSector::GetFirstDataSector(void) const
{
	return GetBiosParamBlock().GetFirstDataSector();
	}

DWORD CFat32BootSector::GetFatSectors(void) const
{
	return m_BiosParamBlock.m_dwFat32Size;
	}

DWORD CFat32BootSector::GetFatPerSector(void) const
{
	return GetBiosParamBlock().GetFatPerSector();
	}

DWORD CFat32BootSector::GetRootDirCluster(void) const
{
	return GetBiosParamBlock().m_dwRootCluster;
	}

// Bios Parameter Block

CFat32BiosParamBlock & CFat32BootSector::GetBiosParamBlock(void) const
{
	return (CFat32BiosParamBlock &) m_BiosParamBlock;
	}

// Dump

void CFat32BootSector::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nFat32 Boot Sector\n");

	AfxTrace("Status              = %s\n",      IsValid() ? "OK" : "Invalid");
	AfxTrace("Boot Code           = %2.2X %2.2X %2.2X\n", m_bBootCode[0], m_bBootCode[1], m_bBootCode[2]);
	AfxTrace("OEM Name            = %s\n",      m_bOemName);
	AfxTrace("Driver Number       = 0x%2.2X\n", m_bDriveNum);
	AfxTrace("Boot Signature      = 0x%2.2X\n", m_bBootSig);
	AfxTrace("Voluem Id           = 0x%8.8X\n", m_dwVolId);
	AfxTrace("Volume Label        = %11s\n",    m_bVolLabel);
	AfxTrace("File System         = %8s\n",     m_bFileSystem);
	AfxTrace("Magic               = 0x%4.4X\n", m_wMagic);

	GetBiosParamBlock().Dump();

	#endif
	}

// End of File