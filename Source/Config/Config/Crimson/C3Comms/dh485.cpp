
#include "intern.hpp"

#include "dh485.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley SLC500 via DH485 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSLCDH485DriverOptions, CUIItem);

// Instantiator

ICommsDriver *	Create_SLCDH485Driver(void)
{
	return New CSLCDH485Driver;
	}

// Constructor

CSLCDH485DriverOptions::CSLCDH485DriverOptions(void)
{
	m_ThisDrop  = 0;

	m_LastDrop  = 31;
	
	m_MakeToken = 0;
	}

// Download Support

BOOL CSLCDH485DriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_ThisDrop));

	Init.AddByte(BYTE(m_LastDrop));

	Init.AddByte(BYTE(m_MakeToken));

	return TRUE;
	}

// Meta Data Creation

void CSLCDH485DriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(ThisDrop);

	Meta_AddInteger(LastDrop);

	Meta_AddInteger(MakeToken);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DH485 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDH485MasterSerialDeviceOptions, CUIItem);

// Constructor

CDH485MasterSerialDeviceOptions::CDH485MasterSerialDeviceOptions(void)
{
	m_Device = 0;

	m_Drop	 = 1;

	m_Root   = "";

	m_Name   = 0x02;

	m_Label  = 0x7C;
	}

// Download Support	     

BOOL CDH485MasterSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddByte(BYTE(m_Drop));
		
	return TRUE;
	}

// Meta Data Creation

void CDH485MasterSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Drop);
	Meta_AddString (Root);
	Meta_AddInteger(Name);
	Meta_AddInteger(Label);
	} 

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley SLC500 via DH485
//

CSLCDH485Driver::CSLCDH485Driver(void)
{
	m_wID		= 0x3351;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "DH485 Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "DH485 Master";

	AddSpaces();

	C3_PASSED();
	}

// Configuration

CLASS CSLCDH485Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSLCDH485DriverOptions);
	}

CLASS CSLCDH485Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDH485MasterSerialDeviceOptions);
	}

// Binding Control

UINT CSLCDH485Driver::GetBinding(void)
{
	return bindRawSerial;
	}

void CSLCDH485Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Management

BOOL CSLCDH485Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CABDH485Dialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void CSLCDH485Driver::AddSpaces(void)
{
	AddSpace(New CSpaceAB(1, "O", "Outputs",	10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(2, "I", "Inputs",		10, 0, 9999, addrWordAsWord, 0,		   0, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(3, "S", "Status",		10, 0, 9999, addrWordAsWord, AB_FILE_NONE, AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(4, "B", "Bits",		10, 0, 9999, addrWordAsWord, 3,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(5, "N", "Integers",	10, 0, 9999, addrWordAsWord, 7,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(6, "T", "Timers",		10, 0, 9999, addrWordAsWord, 4,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));
	
	AddSpace(New CSpaceAB(7, "C", "Counters",	10, 0, 9999, addrWordAsWord, 5,		   AB_FILE_MIN, AB_FILE_MAX, TRUE));
	
	AddSpace(New CSpaceAB(8, "F", "Floating Point", 10, 0, 9999, addrLongAsReal, 8,		   AB_FILE_MIN, AB_FILE_MAX));
	
	AddSpace(New CSpaceAB(9, "L", "Long Word",	10, 0, 9999, addrLongAsLong, 9,		   AB_FILE_MIN, AB_FILE_MAX));
	
//	AddSpace(New CSpaceAB(10,"R","String Area",	10, 0,  124, addrByteAsByte, 10,	   AB_FILE_MIN, AB_FILE_MAX));
       	}

// End of File
