
#include "Intern.hpp"

#include "G3RequestRouter.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern ILinkService * Create_BootService(ICrimsonPxe *pPxe);

extern ILinkService * Create_PromService(ICrimsonPxe *pPxe);

extern ILinkService * Create_ConfigService(ICrimsonPxe *pPxe);

extern ILinkService * Create_DataService(ICrimsonPxe *pPxe);

extern ILinkService * Create_StratonService(ICrimsonPxe *pPxe);

extern ILinkService * Create_TunnelService(ICrimsonPxe *pPxe);

extern ILinkService * Create_AuthService(ICrimsonPxe *pPxe);

//////////////////////////////////////////////////////////////////////////
//
// Request Router
//

// Instantiator

global ILinkService * Create_RequestRouter(ICrimsonPxe *pPxe)
{
	return New CG3RequestRouter(pPxe);
}

// Constructor

CG3RequestRouter::CG3RequestRouter(ICrimsonPxe *pPxe)
{
	memset(m_p, 0, sizeof(m_p));

	m_p[servConfig]  = Create_ConfigService(pPxe);

	m_p[servBoot]    = Create_BootService(pPxe);

	m_p[servProm]    = Create_PromService(pPxe);

	m_p[servData]    = Create_DataService(pPxe);

	m_p[servStraton] = Create_StratonService(pPxe);

	m_p[servTunnel]  = Create_TunnelService(pPxe);

	m_p[servAuth]    = Create_AuthService(pPxe);
}

// Destructor

CG3RequestRouter::~CG3RequestRouter(void)
{
	for( UINT n = 0; n < elements(m_p); n++ ) {

		AfxRelease(m_p[n]);
	}
}

// IUnknown

HRESULT CG3RequestRouter::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3RequestRouter::AddRef(void)
{
	StdAddRef();
}

ULONG CG3RequestRouter::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3RequestRouter::Timeout(void)
{
	for( UINT n = 0; n < elements(m_p); n++ ) {

		if( m_p[n] ) {

			m_p[n]->Timeout();
		}
	}
}

UINT CG3RequestRouter::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	UINT n = Req.GetService();

	if( n < elements(m_p) ) {

		if( m_p[n] ) {

			return m_p[n]->Process(Req, Rep, pTrans);
		}
	}

	return procError;
}

void CG3RequestRouter::EndLink(CG3LinkFrame &Req)
{
	for( UINT n = 0; n < elements(m_p); n++ ) {

		if( m_p[n] ) {

			m_p[n]->EndLink(Req);
		}
	}
}

// End of File
