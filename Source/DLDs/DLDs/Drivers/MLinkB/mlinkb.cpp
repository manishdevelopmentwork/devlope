#include "intern.hpp"

#include "mlinkb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SEW Movilink B Master Serial Driver
//

// Instantiator

INSTANTIATE(CSEWMovilinkB);

// Constructor

CSEWMovilinkB::CSEWMovilinkB(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CSEWMovilinkB::~CSEWMovilinkB(void)
{
	}

// End of File
