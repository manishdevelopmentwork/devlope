
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHashSha1_HPP

#define INCLUDE_CryptoHashSha1_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA1 Cryptographic Hash
//

class CCryptoHashSha1 : public CCryptoHash
{
	public:
		// Constructor
		CCryptoHashSha1(void);

		// ICryptoHash
		CString GetName(void);
		void    GetHashOid(CByteArray &oid);
		void    Initialize(void);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE     m_bHash[SHA1_HASH_SIZE];
		psSha1_t m_Ctx;
	};

// End of File

#endif
