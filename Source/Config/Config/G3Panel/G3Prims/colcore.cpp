
#include "intern.hpp"

#include "colexpr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Color
//

class CUITextColor : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextColor(void);

	protected:
		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		void FindReqType(CTypeDef &Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Virtual Color
//

class CUITextVirtualColor : public CUITextColor
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVirtualColor(void);

	protected:
		// Overridables
		UINT OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Color
//

class CUIColor : public CUICategorizer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIColor(void);

		// Modes
		enum {
			modeNone       = 0,
			modeFixed      = 1,
			modeTagFore    = 2,
			modeTagBack    = 3,
			modeFlash      = 4,
			modePick2      = 5,
			modePick4      = 6,
			modeBlend      = 7,
			modeExpression = 8,
			modeComplex    = 9,
			modeExpressWas = 1000,
			modeComplexWas = 1001,
			};

	protected:
		// Data Members
		CColorComboBox * m_pListCtrl;
		UINT             m_cfIdent;
		BOOL             m_fEmpty;
		BOOL	         m_fFixed;

		// Core Overridables
		void OnBind(void);
		void OnCreate(CWnd &Wnd, UINT &uID);

		// Notification Overridables
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Category Overridables
		BOOL    LoadModeButton(void);
		void    LoadListControl(CString Data);
		BOOL    SwitchMode(UINT uMode);
		UINT    FindDispMode(CString Text);
		CString FindDispText(CString Text, UINT uMode);
		CString FindDataText(CString Text, UINT uMode);
		UINT    FindDispType(UINT uMode);
		CString FindDispVerb(UINT uMode);

		// Implementation
		BOOL SelectTag(CString &Tag);
		BOOL PickColor(COLOR &Color);
		BOOL IsTagName(CString Text);
		BOOL IsSimpleFunc(CString Text, UINT uInit);
		BOOL EditColor(CString &Text, UINT uMode);
		BOOL IsAnimMode(UINT uMode);
		BOOL EditCode(CString &Text);
		BOOL IsComplex(CString Text);
		void MakeComplex(CString &Code);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Color
//

// Dynamic Class

AfxImplementDynamicClass(CPrimColor, CCodedItem);

// Constructor

CPrimColor::CPrimColor(void)
{
	}

// Operations

void CPrimColor::Set(COLOR Color)
{
	Compile(CPrintf(L"0x%4.4X", Color));
	}

void CPrimColor::SetInitial(COLOR Color)
{
	if( IsEmpty() ) {

		Set(Color);
		}
	}

// Color Access

COLOR CPrimColor::GetColor(void)
{
	if( !IsEmpty() ) {

		if( !IsBroken() ) {

			DWORD r = ExecVal();

			return COLOR(r);
			}

		return 0;
		}

	return 0xFFFF;
	}

// Meta Data Creation

void CPrimColor::AddMetaData(void)
{
	CCodedItem::AddMetaData();

	Meta_SetName((IDS_COLOR));
	}

// Implementation

COLOR CPrimColor::ImportColor(BYTE bData)
{
	return C3GetGdiColor(C3GetPaletteEntry(bData));
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Color
//

// Dynamic Class

AfxImplementDynamicClass(CUITextColor, CUITextElement);

// Constructor

CUITextColor::CUITextColor(void)
{
	m_uFlags = textEdit | textExpand;
	}

// Overridables

void CUITextColor::OnBind(void)
{
	CUITextElement::OnBind();

	m_uWidth = 30;

	m_uLimit = 30;
	}

CString CUITextColor::OnGetAsText(void)
{
	CItem *pItem = m_pData->GetObject(m_pItem);

	if( pItem ) {

		CCodedItem *pCoded = (CCodedItem *) pItem;

		return pCoded->GetSource(TRUE);
		}

	return L"";
	}

UINT CUITextColor::OnSetAsText(CError &Error, CString Text)
{
	CItem      * pItem = m_pData->GetObject(m_pItem);

	CCodedItem * pCoded = (CCodedItem *) pItem;

	if( pCoded ) {

		if( Text.IsEmpty() ) {

			if( !pCoded->IsEmpty() ) {

				pCoded->Empty();

				return saveChange;
				}

			return saveSame;
			}
		else {
			if( pCoded->Compile(Error, Text) ) {

				return saveChange;
				}

			if( !Error.AllowUI() ) {

				// LATER -- This ensures that we don't get any
				// failures during an Undo, but is it needed?

				pCoded->Compile(Error, L"WAS " + Text);

				return saveChange;
				}

			return saveError;
			}
		}

	AfxAssert(FALSE);

	return saveSame;
	}

// Implementation

void CUITextColor::FindReqType(CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Virtual Color
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVirtualColor, CUITextColor);

// Constructor

CUITextVirtualColor::CUITextVirtualColor(void)
{
	}

// Overridables

UINT CUITextVirtualColor::OnSetAsText(CError &Error, CString Text)
{
	CItem      * &pItem = m_pData->GetObject(m_pItem);

	CCodedItem * pCoded = (CCodedItem *) pItem;

	if( Text.IsEmpty() ) {

		if( pCoded ) {

			pCoded->Kill();

			delete pCoded;
			
			pItem = NULL;

			return saveChange;
			}

		return saveSame;
		}

	if( pCoded ) {

		if( pCoded->Compile(Error, Text) ) {

			return saveChange;
			}

		if( !Error.AllowUI() ) {

			// LATER -- This ensures that we don't get any
			// failures during an Undo, but is it needed?

			pCoded->Compile(Error, L"WAS " + Text);

			return saveChange;
			}

		return saveError;
		}
	else {
		CTypeDef Type;

		pCoded = New CCodedItem;

		pCoded->SetParent(m_pItem);

		pCoded->Init();

		FindReqType(Type);

		pCoded->SetReqType(Type);

		if( pCoded->Compile(Error, Text) ) {

			pItem = pCoded;

			return saveChange;
			}

		if( !Error.AllowUI() ) {

			// LATER -- This ensures that we don't get any
			// failures during an Undo, but is it needed?

			pCoded->Compile(Error, L"WAS " + Text);

			pItem = pCoded;

			return saveChange;
			}

		pCoded->Kill();

		delete pCoded;

		return saveError;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Color
//

// Dynamic Class

AfxImplementDynamicClass(CUIColor, CUICategorizer);

// Constants

static TCHAR const cSep = 0xB6;

// Constructor

CUIColor::CUIColor(void)
{
	m_dwListStyle = WS_TABSTOP | WS_VSCROLL	|CBS_DROPDOWNLIST | CBS_OWNERDRAWFIXED;

	m_pListCtrl   = New CColorComboBox(this);

	m_cfIdent     = RegisterClipboardFormat(L"C3.1 Identifier");

	m_fEmpty      = FALSE;

	m_fFixed      = FALSE;

	CUICategorizer::m_pListCtrl = m_pListCtrl;
	}

// Core Overridables

void CUIColor::OnBind(void)
{
	CUICategorizer::OnBind();
	}

void CUIColor::OnCreate(CWnd &Wnd, UINT &uID)
{
	CUICategorizer::OnCreate(Wnd, uID);

	if( m_UIData.m_Format[0] == 'n' ) {

		m_fEmpty = TRUE;
		}

	if( m_UIData.m_Format[0] == 'c' ) {

		m_fFixed = TRUE;
		}

	m_pListCtrl->LoadList();
	}

// Notification Overridables

BOOL CUIColor::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pListCtrl->GetID() ) {

		if( uCode == CBN_SELCHANGE ) {

			if( !m_fBusy ) {

				COLOR   Data = m_pListCtrl->GetColor();

				CString Text = CPrintf(L"%4.4X", Data);

				StdSave(TRUE, FindDataText(Text, m_uMode));

				ForceUpdate();

				return TRUE;
				}
			}
		}

	if( uID == m_pPickCtrl->GetID() ) {

		if( !m_pItem->GetDatabase()->IsReadOnly() ) {

			if( m_uMode == modeFixed ) {

				CString Text  = m_pText->GetAsText().Mid(2);

				COLOR   Color = COLOR(wcstol(Text, NULL, 16));

				if( PickColor(Color) ) {

					CString Text = CPrintf(L"%4.4X", Color);

					StdSave(TRUE, FindDataText(Text, m_uMode));

					m_pListCtrl->Invalidate(FALSE);

					ForceUpdate();
					
					return TRUE;
					}

				return FALSE;
				}

			if( m_uMode == modeTagFore || m_uMode == modeTagBack ) {

				CString Tag = m_pShowCtrl->GetWindowText();

				if( SelectTag(Tag) ) {

					StdSave(TRUE, FindDataText(Tag, m_uMode));

					ForceUpdate();

					return TRUE;
					}

				return FALSE;
				}

			if( IsAnimMode(m_uMode) ) {

				CString Text = m_pText->GetAsText();

				if( EditColor(Text, m_uMode) ) {

					StdSave(TRUE, FindDataText(Text, m_uMode));

					ForceUpdate();
					
					return TRUE;
					}
				
				return FALSE;
				}

			if( m_uMode == modeExpression || m_uMode == modeComplex ) {

				CString Text = m_pText->GetAsText();

				if( EditCode(Text) ) {

					StdSave(TRUE, FindDataText(Text, m_uMode));

					ForceUpdate();

					return TRUE;			
					}

				return FALSE;
				}

			}
		}

	return CUICategorizer::OnNotify(uID, uCode);
	}

// Data Overridables

BOOL CUIColor::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptText(pData, m_cfIdent) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIColor::OnAcceptData(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfIdent), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		CStringArray List;

		Text.Tokenize(List, '|');

		if( List[0] == m_pItem->GetDatabase()->GetUniqueSig() ) {

			if( List[1] == L"CDataTag" ) {

				CError Error = CError(TRUE);

				CString Text = List[2] + ((m_uMode == modeTagBack) ? L".Back" : L".Fore");

				UINT   uCode = m_pText->SetAsText(Error, Text);

				if( uCode == saveError ) {

					Error.Show(*afxMainWnd);

					return TRUE;
					}

				if( uCode == saveChange ) {

					LoadUI();

					ForceUpdate();

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Category Overridables

BOOL CUIColor::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	if( TRUE ) {
	
		Opt.m_uID     = modeExpressWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {
	
		Opt.m_uID     = modeComplexWas;
		Opt.m_Text    = L"WAS";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = TRUE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( m_fEmpty ) {

		Opt.m_uID     = 0;
		Opt.m_Text    = IDS_PUI_NONE;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( TRUE ) {

		Opt.m_uID     = modeFixed;
		Opt.m_Text    = IDS_FIXED;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {

		Opt.m_uID     = NOTHING;
		Opt.m_Text    = L"";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modeTagFore;
		Opt.m_Text    = IDS_TAG_FORE;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modeTagBack;
		Opt.m_Text    = IDS_TAG_BACK;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {

		Opt.m_uID     = NOTHING;
		Opt.m_Text    = L"";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modeFlash;
		Opt.m_Text    = IDS_FLASHING;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modePick2;
		Opt.m_Text    = IDS_STATE_1;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {

		Opt.m_uID     = modePick4;
		Opt.m_Text    = IDS_STATE_2;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modeBlend;
		Opt.m_Text    = IDS_BLEND;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {

		Opt.m_uID     = NOTHING;
		Opt.m_Text    = L"";
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modeExpression;
		Opt.m_Text    = IDS_EXPRESSION;
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	if( !m_fFixed ) {
	
		Opt.m_uID     = modeComplex;
		Opt.m_Text    = CString(IDS_COMPLEX);
		Opt.m_fEnable = TRUE;
		Opt.m_fHidden = FALSE;
		Opt.m_Image   = 0;

		m_pModeCtrl->AddOption(Opt);
		}

	return TRUE;
	}

void CUIColor::LoadListControl(CString Data)
{
	if( m_uMode == modeFixed ) {

		COLOR Color = COLOR(wcstol(Data, NULL, 16));

		m_pListCtrl->SetColor(Color);
		}
	}

BOOL CUIColor::SwitchMode(UINT uMode)
{
	if( uMode == NOTHING ) {

		return SwitchMode(modeExpression);
		}

	if( uMode == modeNone ) {

		StdSave(FALSE, L"");

		ForceUpdate();

		return TRUE;
		}

	if( uMode == modeFixed ) {

		// REV3 -- How about trying to find some sensible
		// default colors based upon the original state?

		m_pModeCtrl->SetData(m_uMode = uMode);

		StdSave(FALSE, L"0x0000");

		ForceUpdate();

		m_pListCtrl->ShowColors();

		return TRUE;
		}

	if( uMode == modeTagFore || uMode == modeTagBack ) {

		CString Tag;

		if( m_uMode == modeTagFore || m_uMode == modeTagBack ) {

			Tag = m_pShowCtrl->GetWindowText();
			}
		else {
			if( !SelectTag(Tag) ) {

				return FALSE;
				}
			}

		m_pModeCtrl->SetData(m_uMode = uMode);
			
		StdSave(TRUE, FindDataText(Tag, uMode));

		ForceUpdate();

		return TRUE;
		}

	if( IsAnimMode(uMode) ) {

		// REV3 -- How about trying to find some sensible
		// default colors based upon the original state?

		CString Text;

		if( EditColor(Text, uMode) ) {

			m_pModeCtrl->SetData(m_uMode = uMode);
				
			StdSave(TRUE, FindDataText(Text, uMode));

			ForceUpdate();

			return TRUE;
			}

		return FALSE;
		}

	if( uMode == modeExpression ) {

		if( m_uMode == modeComplex || m_uMode == modeComplexWas ) {

			// LATER -- If there's only one none-empty line of code,
			// we can convert this to General without actually having
			// to drop anything except return. Add support for this.

			CString Warn;
			
			Warn += CString(IDS_THIS_WILL_DELETE);

			Warn += L"\n\n";

			Warn += CString(IDS_DO_YOU_WANT_TO);

			if( CWnd::GetActiveWindow().NoYes(Warn) == IDNO ) {

				return FALSE;
				}

			m_pModeCtrl->SetData(m_uMode = uMode);

			LoadData(L"0x0000", TRUE);

			return TRUE;
			}

		m_pModeCtrl->SetData(m_uMode = uMode);

		LoadData(m_pText->GetAsText(), TRUE);

		return TRUE;
		}

	if( uMode == modeComplex ) {

		CString Text = m_pText->GetAsText();

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);
			}

		if( !IsComplex(Text) ) {

			MakeComplex(Text);
			}

		if( EditCode(Text) ) {

			m_pModeCtrl->SetData(m_uMode = uMode);
				
			StdSave(TRUE, FindDataText(Text, uMode));

			ForceUpdate();

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

UINT CUIColor::FindDispMode(CString Text)
{
	if( m_fEmpty && Text.IsEmpty() ) {

		return 0;
		}

	if( Text.StartsWith(L"WAS ") ) {

		Text = Text.Mid(4);

		if( IsComplex(Text) ) {

			return modeComplexWas;
			}

		return modeExpressWas;
		}

	if( Text.StartsWith(L"0x") ) {

		UINT n;

		for( n = 2; Text[n]; n++ ) {

			if( !isxdigit(Text[n]) ) {

				break;
				}
			}

		if( !Text[n] ) {

			return modeFixed;
			}
		}

	if( Text.EndsWith(L".Fore") ) {

		UINT uLen = Text.GetLength();

		if( IsTagName(Text.Left(uLen - 5)) ) {

			return modeTagFore;
			}
		}

	if( Text.EndsWith(L".Back") ) {

		UINT uLen = Text.GetLength();

		if( IsTagName(Text.Left(uLen - 5)) ) {

			return modeTagBack;
			}
		}

	if( Text.StartsWith(L"ColSelFlash") ) {

		if( IsSimpleFunc(Text, 2) ) {

			return modeFlash;
			}
		}

	if( Text.StartsWith(L"ColFlash") ) {

		if( IsSimpleFunc(Text, 1) ) {

			return modeFlash;
			}
		}

	if( Text.StartsWith(L"ColPick2") ) {

		if( IsSimpleFunc(Text, 1) ) {

			return modePick2;
			}
		}

	if( Text.StartsWith(L"ColPick4") ) {

		if( IsSimpleFunc(Text, 2) ) {

			return modePick4;
			}
		}

	if( Text.StartsWith(L"ColBlend") ) {

		if( IsSimpleFunc(Text, 3) ) {

			return modeBlend;
			}
		}

	if( IsComplex(Text) ) {

		return modeComplex;
		}

	return modeExpression;
	}

CString CUIColor::FindDispText(CString Text, UINT uMode)
{
	if( uMode == 0 ) {

		return L"None";
		}

	if( uMode == modeExpressWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text;
			}
		}

	if( uMode == modeComplexWas ) {

		if( Text.StartsWith(L"WAS ") ) {

			Text = Text.Mid(4);

			return Text.StripToken(L"\r\n").Mid(3);
			}		
		}

	if( uMode == modeFixed ) {

		return Text.Mid(2);
		}

	if( uMode == modeTagFore || uMode == modeTagBack ) {

		return Text.Left(Text.GetLength() - 5);
		}

	if( IsAnimMode(uMode) ) {

		Text = Text.Mid (Text.Find('(')   + 1);

		Text = Text.Left(Text.GetLength() - 1);

		CStringArray List;

		Text.Tokenize(List, L',', L'(', L')');

		Text.Empty();

		UINT t = List.GetCount();

		UINT c = 1;

		switch( uMode ) {

			case modeFlash: c = (t==5) ? 2 : 1; break;
			case modePick2: c =              1; break;
			case modePick4: c =		 2; break;
			case modeBlend: c =		 3; break;
			}

		UINT n;

		for( n = 0; n < c; n++ ) {

			Text += List[n];

			Text += cSep;
			}

		while( n < t ) {
			
			COLOR Col = COLOR(wcstol(List[n++].Mid(2), NULL, 16));

			DWORD Win = C3GetWinColor(Col);
			
			Text += CPrintf(L"{{%X}}", Win);

			Text += cSep;
			}

		return Text;
		}

	if( uMode == modeComplex ) {

		return Text.StripToken(L"\r\n").Mid(3);
		}

	return Text;
	}

CString CUIColor::FindDataText(CString Text, UINT uMode)
{
	if( uMode == modeExpressWas ) {

		return L"WAS " + Text;
		}

	if( uMode == modeComplexWas ) {

		return L"WAS " + Text;
		}

	if( uMode == modeFixed ) {

		return L"0x" + Text;
		}

	if( uMode == modeTagFore ) {

		return Text + L".Fore";
		}

	if( uMode == modeTagBack ) {

		return Text + L".Back";
		}

	return Text;
	}

UINT CUIColor::FindDispType(UINT uMode)
{
	if( uMode == 0 ) {

		return 0;
		}

	if( uMode == modeExpressWas ) {

		return 1;
		}

	if( uMode == modeComplexWas ) {

		return 1;
		}

	if( uMode == modeFixed ) {

		return 3;
		}

	if( uMode == modeTagFore || uMode == modeTagBack ) {

		return 1;
		}

	if( IsAnimMode(uMode) ) {
		
		return 1;
		}

	if( uMode == modeExpression ) {

		return 2;
		}

	return 1;
	}

CString CUIColor::FindDispVerb(UINT uMode)
{
	if( uMode == modeFixed ) {

		return IDS_PICK;
		}

	if( uMode == modeTagFore || uMode == modeTagBack ) {

		return IDS_PICK;
		}

	if( IsAnimMode(uMode) ) {
		
		return IDS_EDIT;
		}

	if( uMode == modeExpression || uMode == modeComplex ) {

		return IDS_EDIT;
		}

	return L"";
	}

// Implementation

BOOL CUIColor::SelectTag(CString &Tag)
{
	CTagSelectDialog Dlg(m_pItem->GetDatabase(), Tag);

	CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

	if( System.ExecSemiModal(Dlg, L"Tags") ) {

		Tag = Dlg.GetTag();

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIColor::PickColor(COLOR &Color)
{
	CColorDialog Dlg;

	Dlg.ShowRainbowOnly();

	Dlg.SetColor(C3GetWinColor(Color));

	if( Dlg.Execute(*m_pPickCtrl) ) {

		Color = C3GetGdiColor(Dlg.GetColor());

		CDatabase     *pDbase  = m_pItem->GetDatabase();

		CUISystem     *pSystem = (CUISystem *) pDbase->GetSystemItem();

		CColorManager *pColor  = pSystem->m_pColor;

		pColor->MarkColor(Color);

		m_pPickCtrl->SetFocus();

		return TRUE;
		}

	m_pPickCtrl->SetFocus();
	
	return FALSE;
	}

BOOL CUIColor::IsTagName(CString Text)
{
	CDatabase   *pDbase  = m_pItem->GetDatabase();

	CUISystem   *pSystem = (CUISystem *) pDbase->GetSystemItem();

	CNameServer *pName   = pSystem->GetNameServer();

	INameServer *pFind   = pName;

	CError      *pError  = New CError(FALSE);

	CTypeDef     Type    = { 0, 0 };

	WORD         ID      = 0;

	pName->ResetServer();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CCodedHost *pHost = (CCodedHost *) m_pItem;

		pFind             = pHost->GetNameServer(pName);
		}

	if( pFind->FindIdent(pError, Text, ID, Type) ) {

		if( ID & 0x8000 ) {

			delete pError;

			return TRUE;
			}
		}

	delete pError;

	return FALSE;
	}

BOOL CUIColor::IsSimpleFunc(CString Text, UINT uInit)
{
	Text = Text.Mid (Text.Find('(')   + 1);

	Text = Text.Left(Text.GetLength() - 1);

	CStringArray List;

	Text.Tokenize(List, L',', L'(', L')');

	Text.Empty();

	for( UINT n = uInit; n < List.GetCount(); n++ ) {

		CString Arg = List[n];

		if( Arg.StartsWith(L"0x") ) {

			CString Line = Arg.Mid(2);

			PTXT    pEnd = NULL;

			// cppcheck-suppress ignoredReturnValue

			wcstol(Line, &pEnd, 16);

			if( pEnd && *pEnd ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CUIColor::EditColor(CString &Text, UINT uMode)
{
	CColorExprItem *pItem = NULL;

	switch( uMode ) {

		case modeFlash: pItem = New CColFlashItem; break;
		case modePick2: pItem = New CColPick2Item; break;
		case modePick4: pItem = New CColPick4Item; break;
		case modeBlend: pItem = New CColBlendItem; break;
		}

	if( pItem ) {

		pItem->SetParent(m_pItem);

		pItem->Init();

		pItem->SetExpr(Text);

		CItemDialog Dlg(pItem, pItem->GetHumanName());

		CSysProxy System;

		System.Bind();

		if( System.ExecSemiModal(Dlg) ) {

			Text = pItem->GetExpr();

			pItem->Kill();

			delete pItem;

			return !Text.IsEmpty();
			}

		pItem->Kill();

		delete pItem;
		}
	
	return FALSE;
	}

BOOL CUIColor::IsAnimMode(UINT uMode)
{
	switch( uMode ) {

		case modeFlash:
		case modePick2:
		case modePick4:
		case modeBlend:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CUIColor::EditCode(CString &Text)
{
	CCodedHost *pHost = (CCodedHost *) m_pItem;

	if( pHost->IsKindOf(AfxRuntimeClass(CCodedHost)) ) {

		CCodeEditingDialog Dlg( pHost,
					m_UIData.m_Tag,
					L"",
					Text
					);

		CSysProxy System;

		System.Bind();

		if( System.ExecSemiModal(Dlg) ) {

			Text = Dlg.GetAsText();
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUIColor::IsComplex(CString Text)
{
	return Text.Find(L"\r\n") < NOTHING;
	}

void CUIColor::MakeComplex(CString &Code)
{
	CString Text;

	Text += L"// ";
	
	Text += CString(IDS_COMPLEX_CODE);

	Text += L"\r\n";

	if( !Code.IsEmpty() ) {

		Text += "return ";

		Text += Code;

		Text += L";";

		Text += L"\r\n";
		}

	Code  = Text;
	}

// End of File
