
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_BlobbedBase_HPP

#define INCLUDE_BlobbedBase_HPP

/////////////////////////////////////////////////////////////////////////
//
// Blob-Persisted Base Class
//

class DLLAPI CBlobbedBase
{
	protected:
		// Init Check
		static void ValidateLoad(PCTXT pName,PCBYTE &pData);

		// Init Helpers
		static BYTE   GetByte(PCBYTE &pData);
		static WORD   GetWord(PCBYTE &pData);
		static DWORD  GetLong(PCBYTE &pData);
		static DWORD  GetAddr(PCBYTE &pData);
		static PBYTE  GetCopy(PCBYTE &pData);
		static PBYTE  GetCopy(PCBYTE &pData, UINT &uSize);
		static PCBYTE GetData(PCBYTE &pData);
		static PCUTF  GetWide(PCBYTE &pData);
		static PCUTF  GetCryp(PCBYTE &pData);

		// Alignment
		static void Align2(PCBYTE &pData);
		static void Align4(PCBYTE &pData);

		// Code Helper
		void GetCoded(PCBYTE &pData, CString &Text);
};

/////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Alignment

STRONG_INLINE void CBlobbedBase::Align2(PCBYTE &pData)
{
	#if 1 || defined(_M_ARM)

	UINT n = (DWORD(pData) & 1);

	pData += n ? (2-n) : 0;

	#endif
	}

STRONG_INLINE void CBlobbedBase::Align4(PCBYTE &pData)
{
	#if 1 || defined(_M_ARM)

	UINT n = (DWORD(pData) & 3);

	pData += n ? (4-n) : 0;

	#endif
	}

// Init Helpers

STRONG_INLINE BYTE CBlobbedBase::GetByte(PCBYTE &pData)
{
	BYTE x = *PCBYTE(pData);

	pData += sizeof(x);

	return x;
	}

STRONG_INLINE WORD CBlobbedBase::GetWord(PCBYTE &pData)
{
	Align2(pData);

	WORD x = MotorToHost(*PCWORD(pData));

	pData += sizeof(x);

	return x;
	}

STRONG_INLINE DWORD CBlobbedBase::GetLong(PCBYTE &pData)
{
	Align4(pData);

	DWORD x = MotorToHost(*PCDWORD(pData));

	pData += sizeof(x);

	return x;
	}

STRONG_INLINE DWORD CBlobbedBase::GetAddr(PCBYTE &pData)
{
	Align4(pData);

	DWORD x = *PCDWORD(pData);

	pData += sizeof(x);

	return x;
	}

STRONG_INLINE PBYTE CBlobbedBase::GetCopy(PCBYTE &pData)
{
	UINT s = GetLong(pData);

	if( s ) {

		PBYTE p = _New("(Blob)") BYTE [ s ];

		memcpy(p, pData, s);

		pData += s;

		return p;
		}

	return NULL;
	}

STRONG_INLINE PBYTE CBlobbedBase::GetCopy(PCBYTE &pData, UINT &uSize)
{
	UINT s = GetLong(pData);

	if( s ) {

		PBYTE p = _New("(Blob)") BYTE [ s ];

		memcpy(p, pData, s);

		pData += s;

		uSize  = s;

		return p;
		}

	uSize = 0;

	return NULL;
	}

STRONG_INLINE PCBYTE CBlobbedBase::GetData(PCBYTE &pData)
{
	UINT s = GetWord(pData);

	if( s ) {
	
		PCBYTE p = PCBYTE(pData);

		pData += s;

		return p;
		}

	return NULL;
	}

STRONG_INLINE PCUTF CBlobbedBase::GetWide(PCBYTE &pData)
{
	UINT  s = GetWord(pData);

	PCUTF p = PCUTF(pData);

	pData  += s * sizeof(WCHAR);

	#if 1 || defined(_E_LITTLE)

	// TODO -- This is really nasty! !!C3!!

	static WCHAR t[256];

	for( UINT n = 0; n < s; n++ ) {

		t[n] = MotorToHost(WORD(p[n]));
		}

	t[s] = 0;

	return t;

	#else

	return p;

	#endif
	}

// End of File

#endif
