
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FatFileSystem_HPP

#define	INCLUDE_FatFileSystem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "FilingSystem.hpp"

#include "FatDirEntry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Objects
//

class CFatDirName;

//////////////////////////////////////////////////////////////////////////
//
// Fat File System
//

class CFatFileSystem : public CFilingSystem
{
	public:
		// Constructor
		CFatFileSystem(UINT iDisk, UINT iVolume, UINT uBoot);

		// Destructor
		~CFatFileSystem(void);

		// Enumeration
		BOOL   GetFirst(FSIndex &Index);
		BOOL   GetNext(FSIndex &Index);
		BOOL   GetName(FSIndex const &Index, CFilename &Name);
		BOOL   GetPath(FSIndex const &Index, CFilename &Name);
		time_t GetUnixTime(FSIndex const &Index);
		UINT   GetPackedTime(FSIndex const &Index);
		UINT   GetSize(FSIndex const &Index);
		BOOL   FindName(CFilename &Name, FSIndex &Index);
		BOOL   IsValid(FSIndex const &Index);
		BOOL   IsDirectory(FSIndex const &Index);
		BOOL   IsFile(FSIndex const &Index);
		BOOL   IsVolume(FSIndex const &Index);
		BOOL   IsSpecial(FSIndex const &Index);
		BOOL   GetArchive(FSIndex const &Index);

		// Directory
		BOOL LocateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL CreateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL MoveDir(FSIndex const &Dir, FSIndex &Index);
		UINT ScanDir(FSIndex const &Dir, FSIndex *&pList, UINT uFlags);
		BOOL RenameDir(CFilename const &Name, FSIndex &Index);
		BOOL EmptyDir(FSIndex const &Index);
		BOOL RemoveDir(FSIndex const &Index);
		UINT ParentDir(FSIndex const &Index);

		// File
		HFILE  OpenDirectory(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL   LocateFile(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		HFILE  CreateFile(CFilename const &Name, FSIndex const &Dir);
		HFILE  CreateFile(HFILE hDir, CFilename const &Name);
		HFILE  OpenFile(CFilename const &Name, FSIndex const &Dir, UINT Mode);
		HFILE  OpenFile(FSIndex const &Index, UINT Mode);
		BOOL   CloseFile(HFILE hFile);
		BOOL   DeleteFile(CFilename const &Name, FSIndex const &Dir);
		BOOL   DeleteFile(FSIndex const &Index);
		BOOL   DeleteFile(HFILE hFile);
		BOOL   RenameFile(HFILE hFile, CFilename const &Name);
		BOOL   FileSetLength(HFILE hFile, UINT uSize);
		UINT   FileGetLength(HFILE);
		BOOL   FileSetUnixTime(HFILE hFile, time_t time);
		time_t FileGetUnixTime(HFILE hFile);
		BOOL   FileGetIndex(HFILE hFile, FSIndex &Index);
		BOOL   FileAllocData(HFILE hFile, UINT uSize);
		UINT   FileWriteData(HFILE hFile, PCBYTE pData, UINT uOffset, UINT uCount);
		UINT   FileReadData(HFILE hFile, PBYTE pData, UINT uOffset, UINT uCount);

	protected:
		// Constants
		static UINT const entryInvalid = 0xFFFFFFFF;
		static UINT const entryFree    = 0x00000000;
		static UINT const rootEntry    = 0x00000000;
		static UINT const indexCluster = 0xFFFFFFFF;
		static UINT const indexFirst   = 0x00000000;
		
		// Transfer
		enum
		{
			xferRead,
			xferWrite,
			xferZero
			};

		// Handle
		struct CHandle32 
		{
			INT	     m_nOpen;
			UINT         m_Mode;
			FSIndex      m_Short;
			CFatDirEntry m_Entry;
			};

		// Data
		CHandle32 m_Handle32[constMaxHandles];
		IMutex  * m_pMutex;
		UINT      m_uData;
		UINT	  m_uFat0;
		UINT	  m_uFat1;
		UINT      m_uRoot;
		UINT      m_uRootSize;
		UINT      m_uSectorSize;
		UINT      m_uClusterSize;

		// File Handles
		void InitFileHandles(void);
		void OnInitHandle(HFILE hFile);

		// Layout
		virtual UINT GetRootCluster(void) const;
		virtual UINT GetFirstSector(UINT uCluster) const;

		// Indexes
		BOOL IsRefIndex(FSIndex const &Index) const;
		BOOL IsDataIndex(FSIndex const &Index) const;
		void MakeRefIndex(FSIndex &Index) const;
		void MakeDataIndex(FSIndex &Index) const;
		BOOL AssertRefIndex(FSIndex const &Index) const;
		BOOL AssertDataIndex(FSIndex const &Index) const;

		// Names
		BOOL AllocName(UINT uCount, FSIndex &Index);
		BOOL FreeName(FSIndex const &Index);
		BOOL LookupName(FSIndex const &Index, CFilename &Name);
		BOOL LookupData(FSIndex const &Index, UINT iData, CFilename &Name);
		BOOL LocateName(CFilename const &Name, FSIndex &Index);
		BOOL CreateName(CFilename const &Name, FSIndex &Index);
		BOOL ModifyName(CFilename const &Name, FSIndex &Index); 
		BOOL LocateNext(FSIndex &Index);
		BOOL LocateShort(FSIndex &Index);
		BOOL LocateShort(FSIndex &Index, CFatDirEntry &Entry);
		BOOL LocateShort(FSIndex const &Index, CFatDirEntry &Entry);
		BOOL MakeUnique(CFatDirName &Filename, FSIndex const &Index);

		// Directory
		BOOL GetEntryLimit(UINT uCluster);
		BOOL MapEntry(UINT uCluster, UINT uEntry, CDataSector &Ptr);
		BOOL ReadEntry(UINT uCluster, UINT uEntry, CFatDirEntry &Entry);
		BOOL WriteEntry(UINT uCluster, UINT uEntry, CFatDirEntry &Entry);
		BOOL ReadEntry(FSIndex const &Index, CFatDirEntry &Entry);
		BOOL WriteEntry(FSIndex const &Index, CFatDirEntry &Entry);

		// Data IO
		BOOL MapData(UINT uCluster, UINT uSector, UINT uOffset, CDataSector &Ptr) const;
		UINT WriteData(UINT uCluster, UINT uOffset, PCBYTE pData, UINT uCount);
		UINT ReadData(UINT uCluster, UINT uOffset, PBYTE pData, UINT uCount); 
		BOOL ZeroData(UINT uCluster, UINT uOffset, UINT uCount);
		UINT XferData(UINT uCluster, UINT uOffset, PBYTE pData, UINT uCount, UINT Type); 

		// Data Allocation
		virtual UINT GetAlloc(UINT uCluster);
		virtual UINT AllocData(UINT uAlloc, BOOL fZero);
		virtual UINT ReAllocData(UINT uCluster, UINT uAlloc, BOOL fZero);
		virtual BOOL FreeData(UINT uCluster);

		// Chain
		virtual UINT FindNext(UINT uCluster);
		virtual UINT FindNext(UINT uCluster, UINT uCount);
		virtual UINT FindLast(UINT uCluster);
		virtual UINT FindCount(UINT uCluster);
		virtual UINT FindFree(void);
		virtual UINT FindFreeCount(void);

		// Data
		BOOL ReadCluster(UINT uCluster, PBYTE pData);
		BOOL WriteCluster(UINT uCluster, PCBYTE pData);
		BOOL ReadCluster(UINT uCluster, PBYTE pData, UINT uCount);
		BOOL WriteCluster(UINT uCluster, PCBYTE pData, UINT uCount);
	};

// End of File

#endif

