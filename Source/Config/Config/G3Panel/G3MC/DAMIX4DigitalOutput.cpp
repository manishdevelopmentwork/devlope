
#include "intern.hpp"

#include "DAMix4DigitalOutput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4DigitalOutput, CCommsItem);

// Property List

CCommsList const CDAMix4DigitalOutput::m_CommsList[] = {

	{ 1, "Output1",  PROPID_OUTPUT1,  usageWriteUser,  IDS_NAME_O1 },
	{ 1, "Output2",  PROPID_OUTPUT2,  usageWriteUser,  IDS_NAME_O2 },
	{ 1, "Output3",  PROPID_OUTPUT3,  usageWriteUser,  IDS_NAME_O3 },

};

// Constructor

CDAMix4DigitalOutput::CDAMix4DigitalOutput(void)
{
	m_Output1 = 0;
	m_Output2 = 0;
	m_Output3 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAMix4DigitalOutput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Digital Outputs");
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDAMix4DigitalOutput::AddMetaData(void)
{
	Meta_AddInteger(Output1);
	Meta_AddInteger(Output2);
	Meta_AddInteger(Output3);

	CCommsItem::AddMetaData();
}

// End of File
