
#include "Intern.hpp"

#include "UdpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack Implementation
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PseudoHeader.hpp"

#include "UdpHeader.hpp"

#include "UdpProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// UDP Socket
//

// Static Data

UINT CUdpSocket::m_NextPort = NOTHING;

// Constructor

CUdpSocket::CUdpSocket(void)
{
	StdSetRef();

	m_fUsed   = FALSE;

	m_fOpen   = FALSE;

	m_pUdp    = NULL;

	m_pTxBuff = NULL;
	}

// Binding

void CUdpSocket::Bind(CUdp *pUdp)
{
	m_pUdp = pUdp;
	}

// Attributes

BOOL CUdpSocket::IsFree(void) const
{
	return !m_fUsed;
	}

// Operations

void CUdpSocket::Create(void)
{
	m_fUsed    = TRUE;

	m_uAddress = 0;

	m_uRxHead  = 0;

	m_uRxTail  = 0;

	m_uRxSize  = 2;

	AddRef();
	}

void CUdpSocket::NetStat(IDiagOutput *pOut)
{
	if( m_fUsed ) {

		pOut->AddRow();

		pOut->SetData(0, "UDP");
		
		pOut->SetData(1, PCTXT(CPrintf("%-15s : %-5u", PCTXT(m_LocIp.GetAsText()), m_LocPort)));
		
		pOut->SetData(2, PCTXT(CPrintf("%-15s : %-5u", PCTXT(m_RemIp.GetAsText()), m_RemPort)));
		
		pOut->SetData(3, "");

		pOut->EndRow();
		}
	}

// IUnknown

HRESULT CUdpSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
	}

ULONG CUdpSocket::AddRef(void)
{
	StdAddRef();
	}

ULONG CUdpSocket::Release(void)
{
	if( m_uRefs > 1 ) {

		if( AtomicDecrement(&m_uRefs) == 1 ) {

			WaitTx();

			Abort();

			m_pUdp->FreeSocket(this);

			m_fUsed = FALSE;

			return 1;
			}

		return m_uRefs;
		}
	
	AfxAssert(FALSE);

	return m_uRefs;
	}

// ISocket

HRESULT CUdpSocket::Listen(WORD Loc)
{
	if( likely(!m_fOpen) ) {

		m_Multi.MakeEmpty();

		m_LocIp   = IP_EMPTY;

		m_RemIp   = IP_EMPTY;

		m_LocPort = Loc;

		m_RemPort = 0;

		m_fActive = FALSE;

		m_fOpen   = TRUE;

		m_pUdp->Enable(Loc);

		return S_OK;
		}

	return E_FAIL;
	}

HRESULT CUdpSocket::Listen(IPADDR const &Ip, WORD Loc)
{
	if( Listen(Loc) == S_OK ) {

		if( IPREF(Ip).IsMulticast() ) {

			m_pUdp->EnableMulticast(Ip, TRUE);

			m_Multi = Ip;
			}

		return S_OK;
		}

	return E_FAIL;
	}

HRESULT CUdpSocket::Connect(IPADDR const &Ip, WORD Rem)
{
	if( m_NextPort == NOTHING ) {

		m_NextPort = rand();
		}

	return Connect(Ip, Rem, 32768 + (m_NextPort++ % 16384));
	}

HRESULT CUdpSocket::Connect(IPADDR const &Ip, WORD Rem, WORD Loc)
{
	if( likely(!m_fOpen) ) {

		m_Multi.MakeEmpty();

		m_LocIp   = IP_EMPTY;

		m_RemIp	  = Ip;

		m_LocPort = Loc;

		m_RemPort = Rem;

		m_fActive = TRUE;

		m_fOpen   = TRUE;

		m_pUdp->Enable(Loc);

		return S_OK;
		}

	return E_FAIL;
	}

HRESULT CUdpSocket::Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc)
{
	// TODO -- This should be implemented for RLOS...

	return Connect(Ip, Rem, Loc);
}


HRESULT CUdpSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CUdpSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( likely(m_fOpen) ) {

		if( m_uRxHead - m_uRxTail ) {

			CBuffer *pBuff = m_pRxBuff[m_uRxHead];

			m_uRxHead      = (m_uRxHead + 1) % m_uRxSize;

			UINT     uData = pBuff->GetSize();

			UINT     uCopy = min(uSize, uData);

			memcpy(pData, pBuff->GetData(), uCopy);

			BuffRelease(pBuff);

			uSize = uCopy;

			return S_OK;
			}
		}

	uSize = 0;

	return E_FAIL;
	}

HRESULT CUdpSocket::Send(PBYTE pData, UINT &uSize)
{
	if( likely(m_fOpen && (m_RemPort || m_uAddress)) ) {

		for( UINT p = 0; p < 2; p++ ) {

			if( likely(!m_pTxBuff) ) {
		
				CBuffer *pBuff = BuffAllocate(uSize);

				if( likely(pBuff) ) {

					pBuff->AddTail(uSize);

					memcpy(pBuff->GetData(), pData, uSize);

					CAutoLock Lock(m_pUdp->m_pLock);

					m_pTxBuff = pBuff;

					m_pUdp->SendReq(TRUE);

					return S_OK;
					}

				TcpDebug(OBJ_UDP, LEV_WARN, "cannot allocate buffer\n");

				uSize = 0;

				return E_FAIL;
				}

			Sleep(5);
			}

		TcpDebug(OBJ_UDP, LEV_WARN, "socket tx overflow\n");
		}

	uSize = 0;

	return E_FAIL;
	}

HRESULT CUdpSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CUdpSocket::Recv(CBuffer * &pBuff)
{
	if( likely(m_fOpen) ) {

		if( likely(m_uRxHead - m_uRxTail) ) {
			
			pBuff     = m_pRxBuff[m_uRxHead];

			m_uRxHead = (m_uRxHead + 1) % m_uRxSize;

			return S_OK;
			}
		}

	pBuff = NULL;

	return E_FAIL;
	}

HRESULT CUdpSocket::Send(CBuffer *pBuff)
{
	if( likely(m_fOpen && (m_RemPort || m_uAddress)) ) {

		for( UINT p = 0; p < 2; p++ ) {

			if( likely(!m_pTxBuff) ) {
		
				CAutoLock Lock(m_pUdp->m_pLock);

				m_pTxBuff = pBuff;

				m_pUdp->SendReq(TRUE);

				return S_OK;
				}

			Sleep(5);
			}

		TcpDebug(OBJ_UDP, LEV_WARN, "socket tx overflow\n");
		}

	return E_FAIL;
	}

HRESULT CUdpSocket::GetLocal(IPADDR &Ip)
{
	Ip = m_LocIp;

	return S_OK;
	}

HRESULT CUdpSocket::GetRemote(IPADDR &Ip)
{
	Ip = m_RemIp;

	return S_OK;
	}

HRESULT CUdpSocket::GetLocal(IPADDR &Ip, WORD &Port)
{
	Ip   = m_LocIp;

	Port = m_LocPort;

	return S_OK;
	}

HRESULT CUdpSocket::GetRemote(IPADDR &Ip, WORD &Port)
{
	Ip   = m_RemIp;

	Port = m_RemPort;

	return S_OK;
	}

HRESULT CUdpSocket::GetPhase(UINT &Phase)
{
	Phase = m_fOpen ? PHASE_OPEN : PHASE_IDLE;

	return S_OK;
	}

HRESULT CUdpSocket::SetOption(UINT uOption, UINT uValue)
{
	switch( uOption ) {

		case OPT_RECV_QUEUE:

			if( !m_fOpen ) {

				m_uRxSize = min(uValue, elements(m_pRxBuff));

				return S_OK;
				}

			return E_FAIL;

		case OPT_ADDRESS:

			m_uAddress = uValue;

			return S_OK;
		}

	return E_FAIL;
	}

HRESULT CUdpSocket::Abort(void)
{
	if( likely(m_fOpen) ) {

		if( m_Multi.IsMulticast() ) {

			m_pUdp->EnableMulticast(m_Multi, FALSE);
			}

		CAutoLock Lock(m_pUdp->m_pLock);

		m_fOpen = FALSE;

		ClearRx();

		ClearTx();
		}

	return S_OK;
	}

HRESULT CUdpSocket::Close(void)
{
	if( likely(m_fOpen) ) {

		if( m_Multi.IsMulticast() ) {

			m_pUdp->EnableMulticast(m_Multi, FALSE);
			}

		CAutoLock Lock(m_pUdp->m_pLock);

		m_fOpen = FALSE;

		ClearRx();
		}

	return S_OK;
	}

// Event Handlers

BOOL CUdpSocket::OnRecv(IPREF Face, PSREF Ps, CUdpHeader *pUdp, CBuffer *pBuff)
{
	// Port match was already done in OnTest.

	CAutoLock   Lock(m_pUdp->m_pLock);

	CAutoBuffer Buff(pBuff);

	if( likely(m_fOpen) ) {

		UINT uNext = (m_uRxTail + 1) % m_uRxSize;

		if( likely(uNext - m_uRxHead) ) {

			WORD RemPort = pUdp->m_RemPort;

			WORD LocPort = pUdp->m_LocPort;

			if( m_uAddress ) {

				if( m_uAddress >= 3 ) {

					*BuffAddHead(Buff, IPADDR) = Face;
					}

				if( m_uAddress >= 2 ) {

					*BuffAddHead(Buff, WORD  ) = HostToMotor(RemPort);

					*BuffAddHead(Buff, IPADDR) = Ps.m_Dest;
					}

				if( m_uAddress >= 1 ) {

					*BuffAddHead(Buff, WORD  ) = HostToMotor(LocPort);

					*BuffAddHead(Buff, IPADDR) = Ps.m_Src;
					}
				}
			else {
				if( !m_fActive ) {

					if( !m_pUdp->AcceptIP(Ps.m_Src) ) {
							
						return TRUE;
						}

					m_RemIp   = Ps.m_Src;
					
					m_RemPort = LocPort;

					m_LocIp   = Ps.m_Dest;
					}
				}

			m_pRxBuff[m_uRxTail] = Buff.TakeOver();

			m_uRxTail            = uNext;

			return TRUE;
			}

		Lock.Free();

		TcpDebug(OBJ_UDP, LEV_WARN, "socket rx overflow\n");

		return TRUE;
		}

	Buff.TakeOver();

	return FALSE;
	}

BOOL CUdpSocket::OnSend(void)
{
	if( m_pTxBuff ) {

		if( m_uAddress ) {

			IPADDR *pSrc  = BuffStripHead(m_pTxBuff, IPADDR);

			WORD   *pPort = BuffStripHead(m_pTxBuff, WORD);

			m_RemIp       = *pSrc;

			m_RemPort     = MotorToHost(*pPort);
			}

		CUdpHeader *pUdp = BuffAddHead(m_pTxBuff, CUdpHeader);

		pUdp->m_RemPort = m_RemPort;

		pUdp->m_LocPort = m_LocPort;

		pUdp->m_Length  = WORD(m_pTxBuff->GetSize());

		m_pUdp->Send(m_RemIp, m_LocIp, m_pTxBuff);

		CAutoLock Lock(m_pUdp->m_pLock);

		m_pTxBuff = NULL;

		m_pUdp->SendReq(FALSE);

		return TRUE;
		}

	return TRUE;
	}

// Implementation

void CUdpSocket::ClearRx(void)
{
	while( m_uRxHead - m_uRxTail ) {

		CBuffer *pBuff = m_pRxBuff[m_uRxHead];

		m_uRxHead      = (m_uRxHead + 1) % m_uRxSize;

		BuffRelease(pBuff);
		}
	}

void CUdpSocket::ClearTx(void)
{
	if( m_pTxBuff ) {

		BuffRelease(m_pTxBuff);

		m_pTxBuff = NULL;

		m_pUdp->SendReq(FALSE);
		}
	}

void CUdpSocket::WaitTx(void)
{
	while( m_pTxBuff ) {
		
		Sleep(10);
		}
	}

// End of File
