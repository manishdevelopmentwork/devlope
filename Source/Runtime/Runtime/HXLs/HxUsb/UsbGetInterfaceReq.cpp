
#include "Intern.hpp"

#include "UsbGetInterfaceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Interface Standard Device Request
//

// Constructor

CUsbGetInterfaceReq::CUsbGetInterfaceReq(WORD wIndex)
{
	Init(wIndex);
	} 

// Operations

void CUsbGetInterfaceReq::Init(WORD wIndex)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqGetInterface;

	m_Direction = dirDevToHost;

	m_Recipient = recInterface;

	m_wLength   = 1;

	m_wIndex    = wIndex;
	}

// End of File
