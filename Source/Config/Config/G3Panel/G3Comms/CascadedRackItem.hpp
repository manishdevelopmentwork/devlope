
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CascadedRackItem_HPP

#define INCLUDE_CascadedRackItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OptionCardRackItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cascaded Rack Item
//

class DLLAPI CCascadedRackItem : public COptionCardRackItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCascadedRackItem(void);

		// Attributes
		BOOL HasRoom(void) const;

		// Operations
		void AddRack(void);
		void DelRack(COptionCardRackItem *pRack);
		void RemapSlots(BOOL fForce);
		UINT GetPowerBudget(void);
		UINT GetPowerTotal(void);
		UINT GetPowerUsage(void);
		BOOL CheckPower(void);

	protected:
		// Data
		BOOL m_fRemap;

		// Dirty Control
		void OnSetDirty(void);

		// Path Config
		virtual DWORD GetRackPath(UINT uRack) const;
		virtual DWORD GetSlotPath(UINT uRack, UINT uSlot) const;

		// Rack Types
		void GetPortsRack(UINT &p1, UINT &p2, UINT &p3, UINT &p4) const;
	};

// End of File

#endif
