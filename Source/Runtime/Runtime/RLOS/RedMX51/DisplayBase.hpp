
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DisplayBase_HPP
	
#define	INCLUDE_DisplayBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Display Driver
//

class CDisplayBase : public IDisplay
{	
	public:
		// Constructor
		CDisplayBase(void);

		// Destructor
		virtual ~CDisplayBase(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IDisplay
		void METHOD Claim(void);
		void METHOD Free(void);
		void METHOD GetSize(int &cx, int &cy);
		void METHOD Update(PCVOID pData);
		BOOL METHOD SetBacklight(UINT pc);
		UINT METHOD GetBacklight(void);
		BOOL METHOD EnableBacklight(BOOL fOn);
		BOOL METHOD IsBacklightEnabled(void);

	protected:
		// Data Members
		ULONG	        m_uRefs;
		IMutex        * m_pLock;
		PDWORD          m_pBuff;
		ISerialMemory * m_pMem;
		int	        m_cx;
		int	        m_cy;

		// Implementation
		WORD LoadBacklight(WORD wDefault);
		BOOL SaveBacklight(WORD wLevel);
	};

// End of File

#endif
