
#include "Intern.hpp"

#include "HalEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Edge Controller
//

// Instantiator

bool Create_HalEdge(void)
{
	(New CHalEdge)->Open();

	return true;
	}

// Constructor

CHalEdge::CHalEdge(void)
{
	m_uDebugAddr = ADDR_UART1;

	m_uDebugLine = INT_UART1;
	}

// End of File
