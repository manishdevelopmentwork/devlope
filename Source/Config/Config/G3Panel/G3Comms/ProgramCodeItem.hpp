
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramCodeItem_HPP

#define INCLUDE_ProgramCodeItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Code
//

class DLLNOT CProgramCodeItem : public CCodedItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramCodeItem(void);

		// Destructor
		~CProgramCodeItem(void);

		// Attributes
		BOOL    HasError(void) const;
		CRange  GetErrorPos(void) const;
		CString GetAsText(void) const;

		// Operations
		void SetAsText(CString Text);
		void ClearCache(void);

		// Overridables
		void LoadParams(CCompileIn &In, BOOL &fFunc);
		void KillParams(CCompileIn &In);

		// Parameters Access
		void GetParameter(UINT uParam, CFuncParam &Param);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Data Members
		UINT	   m_Error;
		INT	   m_From;
		INT	   m_To;
		CWordArray m_Cache;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Recompile Hook
		BOOL OnRecompile(CError &Error, BOOL fExpand);

		// Implementation
		UINT         GetParamCount(void);
		CFuncParam * GetParamList(void);
	};

// End of File

#endif
