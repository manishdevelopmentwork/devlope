
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#include "galil.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Galil Serial Driver : Yaskawa Universal SMC Driver
//

// Instantiator

ICommsDriver *	Create_GalilSerialDriver(void)
{
	return New CGalilSerialDriver;
	}

// Constructor

CGalilSerialDriver::CGalilSerialDriver(void)
{
	m_wID		= 0x402E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Galil";
	
	m_DriverName	= "Serial Driver";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Galil Serial Driver";
	}

//////////////////////////////////////////////////////////////////////////
//
// Galil TCP Driver : Yaskawa TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_GalilTCPDriver(void)
{
	return New CGalilTCPDriver;
	}

// Constructor

CGalilTCPDriver::CGalilTCPDriver(void)
{
	m_wID		= 0x3520;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Galil";
	
	m_DriverName	= "TCP/IP Driver";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Galil TCP/IP Driver";
	}
 
// End of File
