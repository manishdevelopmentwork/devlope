
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Graduated Button
//

// Dynamic Class

AfxImplementDynamicClass(CPrimGradButton, CPrimWithText);

// Static Data

BOOL  CPrimGradButton::m_ShadeMode = 0;

COLOR CPrimGradButton::m_ShadeCol1 = 0;

COLOR CPrimGradButton::m_ShadeCol2 = 0;

// Constructor

CPrimGradButton::CPrimGradButton(void)
{
	m_uType    = 0x0F;

	m_pColor1  = New CPrimColor;

	m_pColor2  = New CPrimColor;

	m_pEdge    = New CPrimPen;

	m_uActMode = actAlways;
}

// UI Overridables

BOOL CPrimGradButton::OnLoadPages(CUIPageList *pList)
{
	// LATER -- Can't this go back into CPrimWithText?

	LoadHeadPages(pList);

	pList->Append(New CUIStdPage(CString(IDS_FIGURE),
				     AfxPointerClass(this),
				     1
	));

	pList->Append(New CUIStdPage(CString(IDS_SHOW),
				     AfxPointerClass(this),
				     2
	));

	LoadTailPages(pList);

	return TRUE;
}

// Overridables

BOOL CPrimGradButton::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
}

void CPrimGradButton::SetInitState(void)
{
	m_pColor1->Set(GetRGB(24, 24, 31));

	m_pColor2->Set(GetRGB(0, 0, 16));

	AddText();

	m_pTextItem->m_Lead = 0;

	m_pTextItem->Set(L"TEXT", TRUE);

	SetInitSize(64, 32);
}

void CPrimGradButton::FindTextRect(void)
{
	int xAdjust = 4;

	int yAdjust = 2;

	m_TextRect.x1 = m_DrawRect.x1 + xAdjust;

	m_TextRect.y1 = m_DrawRect.y1 + yAdjust;

	m_TextRect.x2 = m_DrawRect.x2 - xAdjust;

	m_TextRect.y2 = m_DrawRect.y2 - yAdjust;
}

CSize CPrimGradButton::GetMinSize(IGDI *pGDI)
{
	CSize Size(16, 16);

	if( m_pTextItem ) {

		Size.cy = m_pTextItem->GetLineSize() + 4;
	}

	return Size;
}

void CPrimGradButton::Draw(IGDI *pGDI, UINT uMode)
{
	R2 r = m_DrawRect;

	m_pEdge->AdjustRect(r);

	r.x2 -= 1;
	r.y2 -= 1;

	int nCorn = 8;

	MakeMin(nCorn, (r.y2 - r.y1) / 2);

	MakeMin(nCorn, (r.x2 - r.x1) / 2);

	P2 p[8];

	p[0].x = r.x1;
	p[0].y = r.y1 + nCorn;

	p[1].x = r.x1 + nCorn,
		p[1].y = r.y1;

	p[2].x = r.x2 - nCorn;
	p[2].y = r.y1;

	p[3].x = r.x2;
	p[3].y = r.y1 + nCorn;

	p[4].x = r.x2;
	p[4].y = r.y2 - nCorn;

	p[5].x = r.x2 - nCorn;
	p[5].y = r.y2;

	p[6].x = r.x1 + nCorn;
	p[6].y = r.y2;

	p[7].x = r.x1;
	p[7].y = r.y2 - nCorn;

	m_ShadeMode = 0;

	if( IsPressed() ) {

		m_ShadeCol1 = m_pColor2->GetColor();

		m_ShadeCol2 = m_pColor1->GetColor();
	}
	else {
		m_ShadeCol1 = m_pColor1->GetColor();

		m_ShadeCol2 = m_pColor2->GetColor();
	}

	pGDI->SelectBrush(brushFore);

	pGDI->ShadePolygon(p, 8, 0x000055, Shader);

	m_pEdge->DrawPolygon(pGDI, p, 8, 0x000055, uMode);

	CPrimWithText::Draw(pGDI, uMode);
}

// Download Support

BOOL CPrimGradButton::MakeInitData(CInitData &Init)
{
	CPrimWithText::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pColor1);

	Init.AddItem(itemSimple, m_pColor2);

	Init.AddItem(itemSimple, m_pEdge);

	return TRUE;
}

// Meta Data

void CPrimGradButton::AddMetaData(void)
{
	CPrimWithText::AddMetaData();

	Meta_AddObject(Color1);
	Meta_AddObject(Color2);
	Meta_AddObject(Edge);

	Meta_SetName((IDS_GRADUATED_BUTTON));
}

// Color Mixing

COLOR CPrimGradButton::Mix16(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED(a);
	int ga = GetGREEN(a);
	int ba = GetBLUE(a);

	int rb = GetRED(b);
	int gb = GetGREEN(b);
	int bb = GetBLUE(b);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return GetRGB(rc, gc, bc);
}

DWORD CPrimGradButton::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED(a);
	int ga = GetGREEN(a);
	int ba = GetBLUE(a);

	int rb = GetRED(b);
	int gb = GetGREEN(b);
	int bb = GetBLUE(b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
}

// Shaders

BOOL CPrimGradButton::Shader(IGDI *pGDI, int p, int c)
{
	static COLOR q16 = 0;

	static DWORD q32 = 0;

	static BOOL  f32 = FALSE;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k32 - q32 ) {

				pGDI->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
			}
		}
		else {
			COLOR k16 = Mix16(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k16 - q16 ) {

				pGDI->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
			}
		}

		return FALSE;
	}

	q16 = 0xFFFF;

	q32 = 0x0000;

	f32 = TRUE; // !!!!!

	return m_ShadeMode;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageBtnBase, CPrimWithText);

// Constructor

CPrimImageBtnBase::CPrimImageBtnBase(void)
{
	m_Keep = 0;

	memset(m_pImage, 0, sizeof(m_pImage));
}

// UI Overridables

BOOL CPrimImageBtnBase::OnLoadPages(CUIPageList *pList)
{
	// LATER -- Can't this go back into CPrimWithText?

	LoadHeadPages(pList);

	pList->Append(New CUIStdPage(CString(IDS_BUTTON),
				     AfxPointerClass(this),
				     1
	));

	pList->Append(New CUIStdPage(CString(IDS_SHOW),
				     AfxPointerClass(this),
				     2
	));

	LoadTailPages(pList);

	return TRUE;
}

// UI Update

void CPrimImageBtnBase::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		if( m_pDataItem ) {

			CUITextElement *pUI = pHost->FindUI(m_pDataItem, L"AlignV");

			if( pUI ) {

				((CUITextEnum *) pUI)->SetMask(0x00FF);
			}
		}

		if( m_pTextItem ) {

			CUITextElement *pUI = pHost->FindUI(m_pTextItem, L"AlignV");

			if( pUI ) {

				((CUITextEnum *) pUI)->SetMask(0x007F);
			}
		}
	}

	if( Tag == L"Keep" ) {

		for( UINT n = 0; n < elements(m_pImage); n++ ) {

			if( m_pImage[n] ) {

				m_pImage[n]->m_Opts.m_Keep = m_Keep;

				pHost->UpdateUI(CPrintf(L"Image%2.2u", 1 + n));
			}
		}
	}

	CPrimWithText::OnUIChange(pHost, pItem, Tag);
}

// Operations

void CPrimImageBtnBase::SetImage(UINT n, PCTXT pFile)
{
	CPrimImage * &pImage = m_pImage[n];

	if( !pImage ) {

		pImage = New CPrimImage;

		pImage->SetParent(this);

		pImage->Init();
	}

	if( n == 0 ) {

		CSize Max = CSize(500, 500);

		SetInitSize(pImage->LoadFromFile(pFile, Max));

		UpdateLayout();
	}
	else {
		CSize Max;

		Max.cx = m_DrawRect.x2 - m_DrawRect.x1;

		Max.cy = m_DrawRect.y2 - m_DrawRect.y1;

		pImage->LoadFromFile(pFile, Max);
	}
}

// Overridables

BOOL CPrimImageBtnBase::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
}

void CPrimImageBtnBase::FindTextRect(void)
{
	BOOL fHoney  = C3OemFeature(L"OemSD", FALSE);

	int  xAdjust = fHoney ? 4 : 0;

	int  yAdjust = fHoney ? 2 : 0;

	m_TextRect.x1  = m_DrawRect.x1 + xAdjust;

	m_TextRect.y1  = m_DrawRect.y1 + yAdjust;

	m_TextRect.x2  = m_DrawRect.x2 - xAdjust;

	m_TextRect.y2  = m_DrawRect.y2 - yAdjust;

	m_ImageRect.x1 = m_DrawRect.x1;

	m_ImageRect.y1 = m_DrawRect.y1;

	m_ImageRect.x2 = m_DrawRect.x2;

	m_ImageRect.y2 = m_DrawRect.y2;

	if( m_pDataItem || m_pTextItem ) {

		CPrimBlock *pBlock = (m_pTextItem != NULL) ? (CPrimBlock *) m_pTextItem : (CPrimBlock *) m_pDataItem;

		UINT uLines = (m_pDataItem && m_pDataItem->m_Layout == 1 && m_pDataItem->m_Content == 1) ? 2 : 1;

		UINT uTextH = pBlock->GetFontSize(uLines) + uLines * pBlock->m_Lead;

		switch( pBlock->m_AlignV ) {

			case 5:
				m_TextRect.y2  = m_DrawRect.y1 + uTextH + 2 * yAdjust;

				m_ImageRect.y1 = m_TextRect.y2;

				break;

			case 6:
				m_TextRect.y1  = m_DrawRect.y2 - uTextH - 2 * yAdjust;

				m_ImageRect.y2 = m_TextRect.y1;

				break;

			case 7:
				uTextH = pBlock->GetFontSize(1) + pBlock->m_Lead;

				if( m_pDataItem ) {

					switch( m_pDataItem->m_Content ) {

						case 0:
							m_TextRect.y1  = m_DrawRect.y2 - uTextH - 2 * yAdjust;

							m_ImageRect.y2 = m_TextRect.y1;

							break;

						case 1:
							m_ImageRect.y1 = m_DrawRect.y1 + uTextH + 2 * yAdjust;

							m_ImageRect.y2 = m_DrawRect.y2 - uTextH - 2 * yAdjust;

							break;

						case 2:
							m_TextRect.y2  = m_DrawRect.y1 + uTextH + 2 * yAdjust;

							m_ImageRect.y1 = m_TextRect.y2;

							break;
					}
				}
				break;
		}
	}
}

void CPrimImageBtnBase::Draw(IGDI *pGDI, UINT uMode)
{
	UINT n = NOTHING;

	for( UINT i = 0; i < elements(m_pImage); i++ ) {

		if( m_pImage[i] ) {

			m_pImage[i]->Draw(NULL, m_ImageRect, uMode, 0U);

			n = i;
		}
	}

	if( n < NOTHING ) {

		UINT i = GetIndex();

		if( m_pImage[i] ) {

			m_pImage[i]->Draw(pGDI, m_ImageRect, uMode, 0U);
		}
	}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCTXT pt = L"PRESS";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
	}

	CPrimWithText::Draw(pGDI, uMode);
}

void CPrimImageBtnBase::GetRefs(CPrimRefList &Refs)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {

			m_pImage[n]->GetRefs(Refs);
		}
	}

	CPrimWithText::GetRefs(Refs);
}

void CPrimImageBtnBase::EditRef(UINT uOld, UINT uNew)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {

			m_pImage[n]->EditRef(uOld, uNew);
		}
	}

	CPrimWithText::EditRef(uOld, uNew);
}

// Persistance

void CPrimImageBtnBase::PostLoad(void)
{
	FindTextRect();

	CPrimWithText::PostLoad();
}

// Download Support

BOOL CPrimImageBtnBase::MakeInitData(CInitData &Init)
{
	CPrimWithText::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pImage[0]);

	Init.AddItem(itemVirtual, m_pImage[1]);

	Init.AddItem(itemVirtual, m_pImage[2]);

	Init.AddItem(itemVirtual, m_pImage[3]);

	Init.AddWord(WORD(m_ImageRect.x1));

	Init.AddWord(WORD(m_ImageRect.y1));

	Init.AddWord(WORD(m_ImageRect.x2));

	Init.AddWord(WORD(m_ImageRect.y2));

	return TRUE;
}

// Meta Data

void CPrimImageBtnBase::AddMetaData(void)
{
	CPrimWithText::AddMetaData();

	Meta_AddInteger(Keep);

	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		UINT uOffset = (PBYTE(m_pImage + n) - PBYTE(this));

		AddMeta(CPrintf(L"Image%2.2u", 1 + n), metaVirtual, uOffset);
	}

	Meta_SetName((IDS_IMAGE_BUTTON_BASE));
}

// Image Index

UINT CPrimImageBtnBase::GetIndex(void)
{
	return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Indicator
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageIndicator, CPrimImageBtnBase);

// Constructor

CPrimImageIndicator::CPrimImageIndicator(void)
{
	m_uType  = 0x10;

	m_pState = NULL;
}

// UI Overridables

BOOL CPrimImageIndicator::OnLoadPages(CUIPageList *pList)
{
	// LATER -- Can't this go back into CPrimWithText?

	LoadHeadPages(pList);

	pList->Append(New CUIStdPage(CString(IDS_INDICATOR),
				     AfxPointerClass(this),
				     1
	));

	pList->Append(New CUIStdPage(CString(IDS_SHOW),
				     AfxPointerClass(this),
				     2
	));

	LoadTailPages(pList);

	return TRUE;
}

// Overridables

void CPrimImageIndicator::SetInitState(void)
{
	CPrimImageBtnBase::SetInitState();

	SetImage(0, L"SYM:30,963959974,1");

	SetImage(1, L"SYM:30,1185399849,1");
}

BOOL CPrimImageIndicator::StepAddress(void)
{
	BOOL f1 = StepCoded(m_pState);

	BOOL f2 = CPrimImageBtnBase::StepAddress();

	return f1 || f2;
}

// Download Support

BOOL CPrimImageIndicator::MakeInitData(CInitData &Init)
{
	CPrimImageBtnBase::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pState);

	return TRUE;
}

// Meta Data

void CPrimImageIndicator::AddMetaData(void)
{
	CPrimImageBtnBase::AddMetaData();

	Meta_AddVirtual(State);

	Meta_SetName((IDS_INDICATOR));
}

// Image Index

UINT CPrimImageIndicator::GetIndex(void)
{
	if( m_pState && m_pState->ExecVal() ) {

		return 1;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Illuminated Button
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageIllumButton, CPrimImageIndicator);

// Constructor

CPrimImageIllumButton::CPrimImageIllumButton(void)
{
	m_uType    = 0x11;

	m_pValue   = NULL;

	m_Button   = 0;

	m_Delay    = 0;

	m_pEnable  = NULL;

	m_Local    = 0;

	m_Protect  = 0;

	m_uActMode = actAlways;
}

// UI Overridables

BOOL CPrimImageIllumButton::OnLoadPages(CUIPageList *pList)
{
	// LATER -- Can't this go back into CPrimWithText?

	LoadHeadPages(pList);

	pList->Append(New CUIStdPage(CString(IDS_BUTTON),
				     AfxPointerClass(this),
				     1
	));

	pList->Append(New CUIStdPage(CString(IDS_SHOW),
				     AfxPointerClass(this),
				     2
	));

	LoadTailPages(pList);

	return TRUE;
}

// UI Update

void CPrimImageIllumButton::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == L"Button" ) {

		if( m_Button == 6 ) {

			if( m_pValue ) {

				KillCoded(pHost, L"Value", m_pValue);
			}
		}
		else {
			if( m_pAction->m_Mode ) {

				m_pAction->m_Mode = 0;
			}
		}

		pHost->EnableUI(this, L"Value", m_Button <= 5);

		pHost->EnableUI(m_pAction, L"Mode", m_Button == 6);

		pHost->EnableUI(this, L"Delay", IsMomentary());
	}

	CPrimImageIndicator::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimImageIllumButton::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		Type.m_Type  = typeNumeric;

		Type.m_Flags = flagInherent | flagWritable;

		return TRUE;
	}

	return CPrimImageBtnBase::GetTypeData(Tag, Type);
}

// Overridables

void CPrimImageIllumButton::SetInitState(void)
{
	CPrimImageBtnBase::SetInitState();

	SetImage(0, L"SYM:29,1921268141,1");

	SetImage(1, L"SYM:29,827802936,1");

	SetImage(2, L"SYM:29,1521950253,1");

	SetImage(3, L"SYM:29,733818113,1");
}

BOOL CPrimImageIllumButton::StepAddress(void)
{
	BOOL f1 = StepCoded(m_pValue);

	BOOL f2 = CPrimImageIndicator::StepAddress();

	return f1 || f2;
}

// Download Support

BOOL CPrimImageIllumButton::MakeInitData(CInitData &Init)
{
	CPrimImageIndicator::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);

	Init.AddByte(BYTE(m_Button));

	Init.AddWord(WORD(IsMomentary() ? m_Delay : 0));

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddByte(BYTE(m_Local));

	Init.AddByte(BYTE(m_Protect));

	return TRUE;
}

// Meta Data

void CPrimImageIllumButton::AddMetaData(void)
{
	CPrimImageIndicator::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddInteger(Button);
	Meta_AddInteger(Delay);
	Meta_AddVirtual(Enable);
	Meta_AddInteger(Local);
	Meta_AddInteger(Protect);

	Meta_SetName((IDS_ILLUMINATED_1));
}

// Image Index

UINT CPrimImageIllumButton::GetIndex(void)
{
	CCodedItem *pData = m_pState ? m_pState : m_pValue;

	UINT       nState = 0;

	if( IsPressed() ) {

		nState += 1;
	}

	if( pData && pData->ExecVal() ) {

		nState += 2;
	}

	return nState;
}

// Implementation

BOOL CPrimImageIllumButton::IsMomentary(void)
{
	return m_Button == 2 || m_Button == 3;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Button
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageButton, CPrimImageBtnBase);

// Constructor

CPrimImageButton::CPrimImageButton(void)
{
	m_uType    = 0x12;

	m_uActMode = actAlways;
}

// Overridables

void CPrimImageButton::SetInitState(void)
{
	CPrimImageBtnBase::SetInitState();

	SetImage(0, L"SYM:27,1698001239,1");

	SetImage(1, L"SYM:27,617971084,1");
}

// Download Support

BOOL CPrimImageButton::MakeInitData(CInitData &Init)
{
	CPrimImageBtnBase::MakeInitData(Init);

	return TRUE;
}

// Meta Data

void CPrimImageButton::AddMetaData(void)
{
	CPrimImageBtnBase::AddMetaData();

	Meta_SetName((IDS_IMAGE_BUTTON));
}

// Image Index

UINT CPrimImageButton::GetIndex(void)
{
	return IsPressed();
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image Toggle Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageToggle, CPrimImageBtnBase);

// Constructor

CPrimImageToggle::CPrimImageToggle(void)
{
	m_pValue    = NULL;

	m_pEnable   = NULL;

	m_Local     = 0;

	m_Protect   = 0;

	m_Axis      = 0;

	m_Mode      = 0;

	m_Delay     = 0;

	m_Default   = 0;

	m_pPress1   = NULL;

	m_pRelease1 = NULL;

	m_pPress2   = NULL;

	m_pRelease2 = NULL;

	m_uActMode  = actNone;
}

// UI Overridables

BOOL CPrimImageToggle::OnLoadPages(CUIPageList *pList)
{
	// LATER -- Can't this go back into CPrimWithText?

	LoadHeadPages(pList);

	pList->Append(New CUIStdPage(CString(IDS_SWITCH),
				     AfxPointerClass(this),
				     1
	));

	pList->Append(New CUIStdPage(CString(IDS_ADVANCED),
				     AfxPointerClass(this),
				     3
	));

	pList->Append(New CUIStdPage(CString(IDS_SHOW),
				     AfxPointerClass(this),
				     2
	));

	LoadTailPages(pList);

	return TRUE;
}

// UI Update

void CPrimImageToggle::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == "Mode" ) {

		UpdateType(pHost, L"Value", m_pValue, TRUE);

		DoEnables(pHost);
	}

	CPrimImageBtnBase::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimImageToggle::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		if( m_Mode == 4 ) {

			Type.m_Type  = typeNumeric;

			Type.m_Flags = flagInherent;
		}
		else {
			Type.m_Type  = typeNumeric;

			Type.m_Flags = flagInherent | flagWritable;
		}

		return TRUE;
	}

	if( Tag == "Press1" || Tag == "Press2" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
	}

	if( Tag == "Release1" || Tag == "Release2" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
	}

	return CPrimImageBtnBase::GetTypeData(Tag, Type);
}

// Overridables

BOOL CPrimImageToggle::StepAddress(void)
{
	BOOL f1 = StepCoded(m_pValue);

	BOOL f2 = CPrimImageBtnBase::StepAddress();

	return f1 || f2;
}

// Download Support

BOOL CPrimImageToggle::MakeInitData(CInitData &Init)
{
	CPrimImageBtnBase::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);
	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddByte(BYTE(m_Local));
	Init.AddByte(BYTE(m_Protect));
	Init.AddByte(BYTE(m_Axis));
	Init.AddByte(BYTE(m_Mode));
	Init.AddWord(WORD(IsMomentary() ? m_Delay : 0));
	Init.AddByte(BYTE(m_Default));

	if( m_Mode == 4 ) {

		Init.AddItem(itemVirtual, m_pPress1);
		Init.AddItem(itemVirtual, m_pRelease1);
		Init.AddItem(itemVirtual, m_pPress2);
		Init.AddItem(itemVirtual, m_pRelease2);
	}

	return TRUE;
}

// Meta Data

void CPrimImageToggle::AddMetaData(void)
{
	CPrimImageBtnBase::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddVirtual(Enable);
	Meta_AddInteger(Local);
	Meta_AddInteger(Protect);
	Meta_AddInteger(Axis);
	Meta_AddInteger(Mode);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Default);
	Meta_AddVirtual(Press1);
	Meta_AddVirtual(Release1);
	Meta_AddVirtual(Press2);
	Meta_AddVirtual(Release2);

	Meta_SetName((IDS_IMAGE_TOGGLE_BASE));
}

// Implementation

void CPrimImageToggle::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Press1", m_Mode == 4);
	pHost->EnableUI("Release1", m_Mode == 4);
	pHost->EnableUI("Press2", m_Mode == 4);
	pHost->EnableUI("Release2", m_Mode == 4);

	pHost->EnableUI("Delay", IsMomentary());
}

BOOL CPrimImageToggle::IsMomentary(void)
{
	return m_Mode == 1 || m_Mode == 2 || m_Mode == 3;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 2-State Toggle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageToggle2, CPrimImageToggle);

// Constructor

CPrimImageToggle2::CPrimImageToggle2(void)
{
	m_uType = 0x13;

	m_ValA  = 0;
	m_ValB  = 1;
}

// Overridables

void CPrimImageToggle2::SetInitState(void)
{
	CPrimImageToggle::SetInitState();

	SetImage(0, L"SYM:33,1139011800,1");

	SetImage(1, L"SYM:33,417387774,1");
}

// Download Support

BOOL CPrimImageToggle2::MakeInitData(CInitData &Init)
{
	CPrimImageToggle::MakeInitData(Init);

	Init.AddWord(WORD(m_ValA));
	Init.AddWord(WORD(m_ValB));

	return TRUE;
}

// Meta Data

void CPrimImageToggle2::AddMetaData(void)
{
	CPrimImageToggle::AddMetaData();

	Meta_AddInteger(ValA);
	Meta_AddInteger(ValB);

	Meta_SetName((IDS_STATE_TOGGLE_1));
}

// Image Index

UINT CPrimImageToggle2::GetIndex(void)
{
	if( m_pValue ) {

		if( !m_pValue->IsBroken() ) {

			UINT n = m_pValue->ExecVal();

			if( n == m_ValA ) {

				return 0;
			}

			if( n == m_ValB ) {

				return 1;
			}
		}
	}

	return m_Default;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Image 3-State Toggle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimImageToggle3, CPrimImageToggle);

// Constructor

CPrimImageToggle3::CPrimImageToggle3(void)
{
	m_uType = 0x14;

	m_ValA  = 0;
	m_ValB  = 1;
	m_ValC  = 2;
}

// Overridables

void CPrimImageToggle3::SetInitState(void)
{
	CPrimImageToggle::SetInitState();

	SetImage(0, L"SYM:33,1139011800,1");

	SetImage(1, L"SYM:33,42582924,1");

	SetImage(2, L"SYM:33,417387774,1");
}

// Download Support

BOOL CPrimImageToggle3::MakeInitData(CInitData &Init)
{
	CPrimImageToggle::MakeInitData(Init);

	Init.AddWord(WORD(m_ValA));
	Init.AddWord(WORD(m_ValB));
	Init.AddWord(WORD(m_ValC));

	return TRUE;
}

// Meta Data

void CPrimImageToggle3::AddMetaData(void)
{
	CPrimImageToggle::AddMetaData();

	Meta_AddInteger(ValA);
	Meta_AddInteger(ValB);
	Meta_AddInteger(ValC);

	Meta_SetName((IDS_STATE_TOGGLE_2));
}

// Image Index

UINT CPrimImageToggle3::GetIndex(void)
{
	if( m_pValue ) {

		if( !m_pValue->IsBroken() ) {

			UINT n = m_pValue->ExecVal();

			if( n == m_ValA ) {

				return 0;
			}

			if( n == m_ValB ) {

				return 1;
			}

			if( n == m_ValC ) {

				return 2;
			}
		}
	}

	return m_Default;
}

// End of File
