
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubGetDescReq_HPP

#define	INCLUDE_UsbHubGetDescReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Get Descriptor Request
//

class CUsbHubGetDescReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubGetDescReq(WORD wIndex);

		// Operations
		void Init(WORD wIndex);
	};

// End of File

#endif
