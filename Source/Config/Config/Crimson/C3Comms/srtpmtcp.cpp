
#include "intern.hpp"

#include "srtpmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GE TCP/IP Master via SRTP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CGeSrtpMasterTCPDeviceOptions, CUIItem);

// Constructor

CGeSrtpMasterTCPDeviceOptions::CGeSrtpMasterTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Addr2  = DWORD(MAKELONG(MAKEWORD(  0, 0),   MAKEWORD(0, 0)));

	m_Port	 = 18245;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CGeSrtpMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}


// Download Support

BOOL CGeSrtpMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddLong(LONG(m_Addr2));

	return TRUE;
	}

// Meta Data Creation

void CGeSrtpMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(Addr2);
	
	}

//////////////////////////////////////////////////////////////////////////
//
// GE TCP/IP Master via SRTP Driver
//

// Instantiator

ICommsDriver *Create_GeSrtpMasterTCPDriver(void)
{
	return New CGeSrtpMasterTCPDriver;
	}

// Constructor

CGeSrtpMasterTCPDriver::CGeSrtpMasterTCPDriver(void)
{
	m_wID		= 0x3506;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "GE";
	
	m_DriverName	= "TCP/IP Master via SRTP";
	
	m_Version	= "1.04";
	
	m_ShortName	= "SRTP TCP/IP Master";

	}

// Binding Control

UINT CGeSrtpMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CGeSrtpMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CGeSrtpMasterTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CGeSrtpMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CGeSrtpMasterTCPDeviceOptions);
	}

// End of File
