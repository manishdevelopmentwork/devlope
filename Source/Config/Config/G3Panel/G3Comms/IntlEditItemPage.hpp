
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_IntlEditItemPage_HPP

#define INCLUDE_IntlEditItemPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIntlEditItem;

//////////////////////////////////////////////////////////////////////////
//
// International String Editing Page
//

class CIntlEditItemPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CIntlEditItemPage(CIntlEditItem *pIntlEditItem);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CIntlEditItem * m_pIntlEditItem;
	};

// End of File

#endif
