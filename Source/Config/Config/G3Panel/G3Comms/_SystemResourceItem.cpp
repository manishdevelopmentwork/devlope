
#include "Intern.hpp"

#include "SystemResourceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "FunctionItem.hpp"
#include "FunctionList.hpp"
#include "NameServer.hpp"
#include "SystemResourceWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Resource Libary
//

// Dynamic Class

AfxImplementDynamicClass(CSystemResourceItem, CMetaItem);

// Constructor

CSystemResourceItem::CSystemResourceItem(void)
{
	m_pLib = New CFunctionList;
	}

// UI Creation

CViewWnd * CSystemResourceItem::CreateView(UINT uType)
{
	if( uType == viewResource ) {

		return New CSystemResourceWnd;
		}

	return CMetaItem::CreateView(uType);
	}

// Operations

void CSystemResourceItem::LoadLibrary(void)
{
	LoadVariables();

	LoadFunctions();
	}

// Persistance

void CSystemResourceItem::Init(void)
{
	CMetaItem::Init();

	FindNameServer();
	}

// Meta Data

void CSystemResourceItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddObject(Lib);

	Meta_SetName((IDS_SYSTEM_LIB));
	}

// Implementation

void CSystemResourceItem::LoadVariables(void)
{
	CString Root = IDS_VARIABLES;

	AddCategory(Root);

	for( WORD wIdent = 0; wIdent < 128; wIdent++ ) {

		CString Name;

		if( m_pServer->NameIdent(WORD(0x7C00 + wIdent), Name) ) {

			WORD     ID;

			CTypeDef Type;

			if( FindIdent(Name, ID, Type) ) {

				CFunctionItem * pItem = New CFunctionItem;

				pItem->m_Ident = ID;

				pItem->m_Name  = CFormat(L"%1.%2", Root, Name);

				m_pLib->AppendItem(pItem);
				}
			}
		}
	}

void CSystemResourceItem::LoadFunctions(void)
{
	CString Root = IDS_FUNCTIONS;

	AddCategory(Root);

	WORD wBase[] = { 0x1000, 0x2000 };

	for( UINT n = 0; n < elements(wBase) * 2; n++ ) {

		BOOL fCats = n < elements(wBase);

		if( !fCats ) {

			AddCategory(CFormat(L"%1.%2", Root, CString(IDS_ALL)));
			}

		for( WORD wIdent = 0; wIdent < 256; wIdent++ ) {

			CString  Name;

			WORD       ID = WORD(wBase[n % elements(wBase)] + wIdent);

			CTypeDef Type;

			UINT   uCount;

			if( FindFunction(Name, ID, Type, uCount) ) {

				if( !fCats ) {

					CFunctionItem * pItem = New CFunctionItem;

					pItem->m_Ident = ID;

					pItem->m_Name  = CFormat(L"%1.%2.%3", Root, CString(IDS_ALL), Name);

					m_pLib->AppendItem(pItem);
					}
				else {
					CString Cat;

					if( m_pServer->FindCategory(ID, Cat) ) {

						AddCategory(CFormat(L"%1.%2", Root, Cat));

						CFunctionItem * pItem = New CFunctionItem;

						pItem->m_Ident = ID;

						pItem->m_Name  = CFormat(L"%1.%2.%3", Root, Cat, Name);

						m_pLib->AppendItem(pItem);
						}
					}
				}
			}
		}
	}

void CSystemResourceItem::FindNameServer(void)
{
	CCommsSystem * pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	m_pServer    = pSystem->GetNameServer();
	}

BOOL CSystemResourceItem::FindFunction(CString &Name, WORD ID, CTypeDef &Type, UINT &uCount)
{
	if( m_pServer->NameFunction(ID, Name) ) {

		CError *pError = New CError(FALSE);

		if( m_pServer->FindFunction(pError, Name, ID, Type, uCount) ) {

			delete pError;

			return TRUE;
			}

		delete pError;
		}

	return FALSE;
	}

BOOL CSystemResourceItem::FindIdent(CString Name, WORD &ID, CTypeDef &Type)
{
	CError *pError = New CError(FALSE);

	if( m_pServer->FindIdent(pError, Name, ID, Type) ) {

		delete pError;

		return TRUE;
		}

	delete pError;

	return FALSE;
	}

void CSystemResourceItem::AddCategory(CString Name)
{
	if( m_pLib->FindNamePos(Name) == NOTHING ) {

		CFolderItem *pFolder = New CFolderItem;

		m_pLib->AppendItem(pFolder);

		pFolder->SetName(Name);
		}
	}

// End of File
