
#include "Intern.hpp"

#include "DevConTabWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// TODO -- Tooltips!!!

// TODO -- Scrolling!!!

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConTabWnd_CmdEdit.hpp"

#include "DevCon.hpp"

#include "DevConNode.hpp"

#include "DevConPart.hpp"

#include "DevConEditCtrl.hpp"

#include "DevConElementGroup.hpp"

#include "DevConElementMessage.hpp"

#include "DevConElementLabel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration View Window
//

// Base Class

#define CBaseClass CDialogViewWnd

// Runtime Class

AfxImplementRuntimeClass(CDevConTabWnd, CBaseClass);

// Constructor

CDevConTabWnd::CDevConTabWnd(CJsonData *pData, CJsonData *pSchema, CJsonData *pPerson, CString const &Path, CJsonData *pTab)
{
	m_pData   = pData;

	m_pSchema = pSchema;

	m_pPerson = NULL;

	m_Path    = Path;

	m_pTab    = pTab;

	if( !m_Path.IsEmpty() && pTab->HasName(L"name") ) {

		m_Path += L'.';

		m_Path += pTab->GetValue(L"name");
	}

	if( pPerson ) {

		CJsonData *pSet = pPerson->GetChild(L"set");

		if( pSet ) {

			CJsonData *pKeys = pSet->GetChild(L"keys");

			if( pKeys ) {

				if( pKeys->GetCount() ) {

					m_pPerson = pPerson;
				}
			}
		}
	}

	m_fHasUndo  = TRUE;

	m_fReadOnly = FALSE;

	m_fStatus   = FALSE;

	m_pFocus    = NULL;

	m_fLoaded   = FALSE;

	m_uFocus    = NOTHING;

	m_fScroll   = TRUE;

	m_pForm     = NULL;

	m_uMinId    = 1000;

	m_uMaxId    = m_uMinId;
}

// Destructor

CDevConTabWnd::~CDevConTabWnd(void)
{
	delete m_pForm;
}

// IUnknown

HRESULT CDevConTabWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
		}

		return E_NOINTERFACE;
	}

	return E_POINTER;
}

ULONG CDevConTabWnd::AddRef(void)
{
	return 1;
}

ULONG CDevConTabWnd::Release(void)
{
	return 1;
}

// IDropTarget

HRESULT CDevConTabWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConTabWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

HRESULT CDevConTabWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	return S_OK;
}

HRESULT CDevConTabWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
}

// Overribables

void CDevConTabWnd::OnAttach(void)
{
}

CString CDevConTabWnd::OnGetNavPos(void)
{
	CString Nav = m_pItem->GetFixedPath();

	if( m_uFocus < NOTHING ) {

		CElement const &Elem = m_Elements[m_uFocus];

		if( !Elem.m_Name.IsEmpty() ) {

			Nav += L':';

			Nav += Elem.m_Name;
		}
	}

	return Nav;
}

BOOL CDevConTabWnd::OnNavigate(CString const &Nav)
{
	UINT    p1  = Nav.Find(L'!');

	CString Src = Nav.Left(p1);

	CString Loc = Nav.Mid(p1+1);

	UINT    p2  = Loc.Find(L':');

	if( p2 < NOTHING ) {

		CString Path = Loc.Left(p2);

		CString Name = Loc.Mid(p2+1);

		if( Path == m_pItem->GetFixedPath() ) {

			INDEX i = m_Names.FindName(Name);

			if( !m_Names.Failed(i) ) {

				CElement const &Elem = m_Elements[m_Names[i]];

				CWnd *pWnd = NULL;

				if( Elem.m_pElem->FindFocus(pWnd) ) {

					if( !IsCurrent() || !afxMainWnd->IsActive() ) {

						m_pFocus = pWnd;
					}
					else {
						pWnd->SetFocus();
					}
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CDevConTabWnd::OnExec(CCmd *pCmd)
{
	if( m_fHasUndo ) {

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdEdit)) ) {

			PlayEditCmd((CCmdEdit *) pCmd, TRUE);
		}
	}
}

void CDevConTabWnd::OnUndo(CCmd *pCmd)
{
	if( m_fHasUndo ) {

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdEdit)) ) {

			PlayEditCmd((CCmdEdit *) pCmd, FALSE);

		}
	}
}

// Message Map

AfxMessageMap(CDevConTabWnd, CBaseClass)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_GETMINMAXINFO)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_NOTIFY)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_SHOWWINDOW)
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_HSCROLL)
	AfxDispatchMessage(WM_VSCROLL)
	AfxDispatchMessage(WM_CTLCOLORSTATIC)
	AfxDispatchMessage(WM_CTLCOLORBTN)

	AfxMessageEnd(CDevConTabWnd)
};

// Message Handlers

void CDevConTabWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	if( GetParent(AfxRuntimeClass(CDialog)).IsWindow() ) {

		m_Color    = afxColor(3dFace);

		m_fHasUndo = FALSE;

		m_Brush.Create(m_Color);
	}
	else {
		m_Color = afxColor(TabFace);

		m_Brush.Create(m_Color);
	}

	for( UINT n = 1; n <= 2; n++ ) {

		CWnd &Wnd = GetParent(n);

		if( Wnd.IsKindOf(AfxRuntimeClass(CItemDialog)) ) {

			if( ((CItemDialog &) Wnd).IsReadOnly() ) {

				m_fReadOnly = TRUE;
			}

			break;
		}
	}

	m_DropHelp.Bind(m_hWnd, this);

	m_nScroll = 0;

	m_pScroll = New CScrollBar;

	CRect Rect = GetClientRect();

	Rect.left  = Rect.right - 18;

	m_pScroll->Create(SBS_VERT, Rect, ThisObject, 0);

	MakeUI();

	EnableUI(L"");
}

void CDevConTabWnd::OnCancelMode(void)
{
	HideToolTip();
}

void CDevConTabWnd::OnSetCurrent(BOOL fCurrent)
{
	if( !fCurrent ) {

		if( m_fStatus ) {

			afxThread->SetStatusText(L"");

			m_fStatus = FALSE;
		}

		HideToolTip();
	}
	else {
		UINT c = m_Sections.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CSection const &Sect = m_Sections[n];

			BOOL fEnable = Evaluate(Sect.m_Enable);

			if( Sect.m_fState != fEnable ) {

				KillUI();

				MakeUI();
			}
		}

		EnableUI(L"");
	}
}

void CDevConTabWnd::OnDestroy(void)
{
	DestroyToolTip();
}

void CDevConTabWnd::OnPaint(void)
{
	OnPaint(CPaintDC(ThisObject));
}

void CDevConTabWnd::OnPaint(CDC &DC)
{
	if( !m_pForm ) {

		CRect Rect = GetClientRect();

		DC.SetBkMode(OPAQUE);

		DC.SetBkColor(afxColor(TabFace));

		DC.SetTextColor(afxColor(ButtonText));

		DC.Select(afxFont(Dialog));

		CStringArray List;

		List.Append(IDS("This item has no editable properties."));

		int cy = DC.GetTextExtent(L"X").cy;

		int nn = List.GetCount();

		int ct = ((nn > 1) ? (4 * nn - (nn - 1)) : 2) * cy / 2;

		int yp = Rect.top + (2 * (Rect.cy() - ct) / 5);

		for( int n = 0; n < nn; n++ ) {

			int cx = DC.GetTextExtent(List[n]).cx;

			int xp = Rect.left + (Rect.cx() - cx) / 2;

			DC.TextOut(xp, yp, List[n]);

			yp += (n ? 3 : 4) * cy / 2;

			DC.Select(afxFont(Dialog));
		}

		DC.Deselect();
	}

	DC.TextOut(10, 10, m_Root);
}

BOOL CDevConTabWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	DC.FillRect(Rect, m_Brush);

	return TRUE;
}

void CDevConTabWnd::OnSize(UINT uCode, CSize Size)
{
	if( m_fLoaded ) {

		CRect Rect = GetClientRect();

		Rect.left  = Rect.right - 18;

		m_pScroll->MoveWindow(Rect, FALSE);

		HideToolTip();

		MoveToolTips();

		if( !CheckScroll() ) {

			Scroll(0);
		}
	}
}

void CDevConTabWnd::OnGetMinMaxInfo(MINMAXINFO &Info)
{
	if( m_pForm ) {

		CSize Size1 = m_pForm->GetMinSize();
		CSize Size2 = m_pForm->GetMaxSize();

		CRect m_Border;

		Size1.cx += m_Border.left + m_Border.right;
		Size2.cx += m_Border.left + m_Border.right;

		Size1.cx += 20; /* Scroll Bar!!!! */
		Size2.cx += 20;

		Size1.cy += m_Border.top + m_Border.bottom;
		Size2.cy += m_Border.top + m_Border.bottom;

		Info.ptMinTrackSize = CPoint(Size1);
		Info.ptMaxTrackSize = CPoint(Size2);

		return;
	}

	AfxCallDefProc();
}

BOOL CDevConTabWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID >= 0x8000 ) {

		afxMainWnd->PostMessage(WM_COMMAND, uID);

		return TRUE;
	}

	if( uID == IDOK ) {

		if( m_pFocus ) {

			AfxNull(CWnd).SetFocus();

			SetDlgFocus(*m_pFocus);
		}

		return TRUE;
	}

	if( uID == 100 ) {

		OnSetCurrent(TRUE);

		return TRUE;
	}

	if( uID >= m_uMinId && uID < m_uMaxId ) {

		CElement const &Elem = m_Elements[m_Ids[uID]];

		TakeAction(Elem, Elem.m_pElem->OnNotify(uID, uNotify, Wnd));
	}

	return FALSE;
}

BOOL CDevConTabWnd::OnNotify(UINT uID, NMHDR &Info)
{
	if( uID >= m_uMinId && uID < m_uMaxId ) {

		CElement const &Elem = m_Elements[m_Ids[uID]];

		TakeAction(Elem, Elem.m_pElem->OnNotify(uID, Info));
	}

	return FALSE;
}

void CDevConTabWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	SetCurrent(TRUE);

	m_pFocus = &Wnd;

	UINT  id = Wnd.GetID();

	INDEX np = m_Ids.FindName(id);

	if( m_Ids.Failed(np) ) {

		if( m_uFocus < NOTHING ) {

			if( m_fStatus ) {

				afxThread->SetStatusText(L"");

				m_fStatus = FALSE;
			}

			m_uFocus = NOTHING;

			/*TrackToolTip(FALSE);*/
		}
	}
	else {
		UINT uFocus = m_Ids[np];

		if( m_uFocus != uFocus ) {

			m_uFocus = uFocus;

			/*if( !m_fFirst ) {

				// NOTE -- We don't do this on the first field that
				// the app displays, as otherwise we can get in knots
				// with respect to the splash screen.

				TrackToolTip(FALSE);
			}

			m_fFirst = FALSE;*/
		}
	}

	CheckScroll();
}

void CDevConTabWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_pFocus ) {

		if( m_pFocus->IsWindowVisible() ) {

			SetDlgFocus(*m_pFocus);
		}

		return;
	}

	AfxCallDefProc();
}

void CDevConTabWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	if( !fShow ) {

		HideToolTip();

		return;
	}

//	SendEnable();
}

void CDevConTabWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
	}

	Scroll(nDelta / 5);
}

void CDevConTabWnd::OnHScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	OnScroll(uCode, nPos, Ctrl);
}

void CDevConTabWnd::OnVScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( m_pScroll == &Ctrl ) {

		switch( uCode ) {

			case SB_LINEUP:

				Scroll(+12);

				break;

			case SB_LINEDOWN:

				Scroll(-12);

				break;

			case SB_PAGEUP:

			//	Scroll(+m_UIRect.cy());

				break;

			case SB_PAGEDOWN:

			//	Scroll(-m_UIRect.cy());

				break;

			case SB_THUMBTRACK:

				Scroll(m_nScroll - nPos);

				break;
		}
	}

	OnScroll(uCode, nPos, Ctrl);
}

void CDevConTabWnd::OnScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
}

HOBJ CDevConTabWnd::OnCtlColorStatic(CDC &DC, CWnd &Wnd)
{
	if( Wnd.IsKindOf(AfxRuntimeClass(CEditCtrl)) ) {

		if( Wnd.GetWindowStyle() & ES_READONLY ) {

			DC.SetBkColor(afxColor(TabLock));

			return afxBrush(TabLock);
		}
	}

	DC.SetBkColor(m_Color);

	return m_Brush;
}

HOBJ CDevConTabWnd::OnCtlColorBtn(CDC &DC, CWnd &Wnd)
{
	DC.SetBkColor(m_Color);

	return m_Brush;
}

// Tool Tips

void CDevConTabWnd::CreateToolTip(void)
{
}

void CDevConTabWnd::DestroyToolTip(void)
{
}

void CDevConTabWnd::HideToolTip(void)
{
}

void CDevConTabWnd::MoveToolTips(void)
{
}

// String Helpers

CString CDevConTabWnd::Join(CString Base, CString const &More)
{
	if( !More.IsEmpty() ) {

		if( !Base.IsEmpty() ) {

			Base += L'.';
		}

		Base += More;
	}

	return Base;
}

// Implementation

void CDevConTabWnd::MakeUI(void)
{
	m_Elements.Empty();

	m_Sections.Empty();

	m_Ids.Empty();

	m_Names.Empty();

	CJsonData *pSections = m_pTab->GetChild(L"sections");

	CJsonData *pFields   = m_pTab->GetChild(L"fields");

	if( pSections || pFields ) {

		m_pForm = New CLayFormGrid(1);

		if( pSections ) {

			UINT c = pSections->GetCount();

			for( UINT n = 0; n < c; n++ ) {

				CJsonData *pSection = pSections->GetChild(n);

				LoadSection(pSection);
			}
		}
		else {
			CLayFormation *pGrid = New CLayFormGrid(2);

			LoadFields(pGrid, pFields);

			m_pForm->AddItem(pGrid);
		}

		if( m_fHasUndo ) {

			if( m_pItem->IsKindOf(AfxRuntimeClass(CDevConNode)) ) {

				CDevConNode *pNode = (CDevConNode *) m_pItem;

				CDevConPart *pPart = (CDevConPart *) pNode->GetParent();

				if( !pPart->m_Enable ) {

					CString Head(IDS("Important Note"));

					CString Name(pPart->GetHumanName());

					CPrintf Text(IDS("This element is excluded from download. Click %s if you want to change this."), PCTXT(Name));

					CDevConElement *pElem;

					pElem = New CDevConElementGroup(Head);

					pElem->AddLayout(m_pForm);

					m_Elements.Append(CElement(pElem));

					pElem = New CDevConElementMessage(Text);

					pElem->AddLayout(m_pForm);

					m_Elements.Append(CElement(pElem));
				}
			}
		}

		LayoutUI();

		CreateUI();
	}
}

void CDevConTabWnd::LoadSection(CJsonData *pSection)
{
	if( !pSection->HasName(L"tools") ) {

		CString         Label  = pSection->GetValue(L"label");

		CDevConElement *pElem  = New CDevConElementGroup(Label);

		pElem->AddLayout(m_pForm);

		m_Elements.Append(CElement(pElem));

		CString Enable = pSection->GetValue(L"enable");

		BOOL    fHide  = FALSE;

		if( !Enable.IsEmpty() ) {

			CSection Section;

			Section.m_Enable = Enable;

			Section.m_fState = Evaluate(Enable);

			m_Sections.Append(Section);

			if( !Section.m_fState ) {

				CString         Message = pSection->GetValue(L"message");

				CDevConElement *pElem   = New CDevConElementMessage(Message);

				pElem->AddLayout(m_pForm);

				m_Elements.Append(CElement(pElem));

				fHide = TRUE;
			}
		}

		if( TRUE ) {

			CJsonData *pFields = pSection->GetChild(L"fields");

			if( pFields ) {

				if( fHide ) {

					LoadFields(NULL, pFields);
				}
				else {
					CLayFormation *pGrid = New CLayFormGrid(2);

					LoadFields(pGrid, pFields);

					m_pForm->AddItem(pGrid);
				}
			}
		}

		if( TRUE ) {

			CJsonData *pTable = pSection->GetChild(L"table");

			if( pTable ) {

				if( fHide ) {

					LoadTable(NULL, pTable);
				}
				else {
					LoadTable(m_pForm, pTable);
				}
			}
		}
	}
}

void CDevConTabWnd::LoadFields(CLayFormation *pGrid, CJsonData *pFields)
{
	UINT c = pFields->GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CJsonData *pField = pFields->GetChild(n);

		LoadField(pGrid, pField);
	}
}

void CDevConTabWnd::LoadField(CLayFormation *pGrid, CJsonData *pField)
{
	CString Type = pField->GetValue(L"type");

	if( !Type.IsEmpty() ) {

		if( Type == L"hidden" ) {

			return;
		}

		if( Type == L"const" ) {

			CString Name = pField->GetValue(L"name");

			CString Data = pField->GetValue(L"data");

			if( Data.StartsWith(L"=") ) {

				CJsonData *pDict = m_pSchema->GetChild(L"aliases");

				Data = pDict->GetValue(Data.Mid(1));
			}

			CString Full = Join(m_Path, Name);

			m_pData->AddValue(Full, Data);

			return;
		}

		if( pGrid ) {

			CElement Elem;

			Elem.m_Label = pField->GetValue(L"label");

			Elem.m_Label.Replace(L"&reg;", L"\xAE");

			Elem.m_pElem = New CDevConElementLabel(Elem.m_Label + L':', m_fHasUndo);

			Elem.m_pElem->AddLayout(pGrid);

			m_Elements.Append(Elem);
		}

		if( pGrid || !m_fReadOnly ) {

			CString Name = pField->GetValue(L"name");

			CString Full, Data;

			if( !pGrid ) {

				// This is a hack to ensure that default values of non-created
				// fields always get loaded to ensure enables work correctly...
				
				if( Type != L"enum" ) {

					return;
				}

				if( !(Data = m_pData->GetValue(Full = Join(m_Path, Name))).IsEmpty() ) {

					return;
				}
			}

			CElement Elem(CDevConElement::Create(Type));

			Elem.m_pField  = pField;
			Elem.m_Name    = Name;
			Elem.m_Label   = pField->GetValue(L"label");
			Elem.m_Default = pField->GetValue(L"default");
			Elem.m_Enable  = pField->GetValue(L"enable");

			Elem.m_pElem->BindItem(m_pItem);

			Elem.m_pElem->SetPersonality(m_pPerson);

			Elem.m_pElem->ParseConfig(m_pSchema, pField);

			if( !pGrid ) {

				CString Load = Elem.m_pElem->GetDefault();

				if( !Load.IsEmpty() ) {

					m_pData->AddValue(Full, Load, jsonString);
				}

				delete Elem.m_pElem;

				return;
			}

			Elem.m_pElem->AddLayout(pGrid);

			UINT n = m_Elements.Append(Elem);

			m_Names.Insert(Elem.m_Name, n);
		}
	}
}

void CDevConTabWnd::LoadTable(CLayFormation *pForm, CJsonData *pTable)
{
	if( pForm ) {

		CString Name = pTable->GetValue(L"name");

		CString Full = Join(m_Path, Name);

		MakeTable(pTable, m_pData, Full);

		CElement Elem;

		Elem.m_pElem  = AfxNewObject(CDevConElement, AfxNamedClass(L"CDevConElementTable"));
		Elem.m_pField = pTable;
		Elem.m_Name   = Name;
		Elem.m_fTable = TRUE;

		Elem.m_pElem->BindItem(m_pItem);

		Elem.m_pElem->SetPersonality(m_pPerson);

		Elem.m_pElem->ParseConfig(m_pSchema, pTable);

		Elem.m_pElem->AddLayout(pForm);

		UINT n = m_Elements.Append(Elem);

		m_Names.Insert(Elem.m_Name, n);
	}
}

void CDevConTabWnd::MakeTable(CJsonData *pTable, CJsonData *pRoot, CString const &Full)
{
	CStringArray List;

	Full.Tokenize(List, '.');

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		CJsonData *pWalk = pRoot->GetChild(List[n]);

		if( !pWalk ) {

			pRoot->AddChild(List[n], n == List.GetCount() - 1, pWalk);
		}

		pRoot = pWalk;
	}

	if( pTable->HasName(L"rows") ) {

		ApplyRowCount(pRoot, pTable->GetChild(L"fields"), watoi(pTable->GetValue(L"rows")));
	}
	else {
		if( pTable->HasName(L"template") ) {

			ApplyTemplate(pRoot, pTable->GetChild(L"template"));
		}
		else {
			CJsonData *pFields = pTable->GetChild(L"fields");

			if( pFields->GetChild(0U)->GetValue(L"name") == L"key" ) {

				ApplyKeyFields(pRoot);
			}
		}
	}
}

void CDevConTabWnd::ApplyRowCount(CJsonData *pData, CJsonData *pFields, UINT uRows)
{
	if( uRows ) {

		for( UINT r = pData->GetCount(); r < uRows; r++ ) {

			CJsonData *pRow;

			pData->AddObject(pRow);

			pRow->AddValue(L"order", CPrintf(L"%u", r));

			for( UINT f = 0; f < pFields->GetCount(); f++ ) {

				CJsonData *pField = pFields->GetChild(f);

				CString    Name   = pField->GetValue(L"name");

				CString    Data   = pField->GetValue(L"default");

				pRow->AddValue(Name, Data);
			}
		}

		if( pData->GetCount() > uRows ) {

			for( UINT r = uRows; r < pData->GetCount(); r++ ) {

				pData->Delete(r);
			}

			// Hack to close up missing array elemenmts. We
			// really need to re-write our JSON handler...

			pData->Parse(pData->GetAsText(FALSE));
		}
	}
}

void CDevConTabWnd::ApplyTemplate(CJsonData *pData, CJsonData *pTemplate)
{
	if( pTemplate ) {

		UINT tc = pTemplate->GetCount();

		for( UINT t = 0; t < tc; t++ ) {

			CJsonData *pTemp = pTemplate->GetChild(t);

			CString    Key   = pTemp->GetValue(L"key");

			if( !FindKey(pData, Key) ) {

				CJsonData *pRow;

				pData->AddObject(pRow);

				pRow->Parse(pTemp->GetAsText(FALSE));
			}
		}

		UINT rc = pData->GetCount();

		BOOL rb = FALSE;

		for( UINT r = 0; r < rc; r++ ) {

			CJsonData *pRow = pData->GetChild(r);

			CString    Key  = pRow->GetValue(L"key");

			if( !FindKey(pTemplate, Key) ) {

				pData->Delete(r);

				rb = TRUE;
			}
		}

		if( rb ) {

			// Hack to close up missing array elemenmts. We
			// really need to re-write our JSON handler...

			pData->Parse(pData->GetAsText(FALSE));
		}
	}
}

void CDevConTabWnd::ApplyKeyFields(CJsonData *pData)
{
	UINT m = 1;

	UINT c = pData->GetCount();

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; n < c; n++ ) {

			CJsonData *pRow = pData->GetChild(n);

			CString    Key  = pRow->GetValue(L"key");

			if( Key.IsEmpty() ) {

				if( p == 1 ) {

					pRow->AddValue(L"key", CPrintf("%u", ++m), jsonString);
				}
			}
			else {
				if( p == 0 ) {

					UINT k = watoi(Key);

					MakeMax(m, k);
				}
			}
		}
	}
}

BOOL CDevConTabWnd::FindKey(CJsonData *pData, CString const &Key)
{
	UINT c = pData->GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( pData->GetChild(n)->GetValue(L"key") == Key ) {

			return TRUE;
		}
	}

	return FALSE;
}

void CDevConTabWnd::CreateUI(void)
{
	UINT c  = m_Elements.GetCount();

	UINT id = m_uMaxId;

	for( UINT n = 0; n < c; n++ ) {

		CElement const &Elem = m_Elements[n];

		UINT	       uFrom = id;

		Elem.m_pElem->CreateControls(ThisObject, id);

		if( Elem.m_pField ) {

			CString Full = Join(m_Path, Elem.m_Name);

			CString Data;

			if( Elem.m_fTable ) {

				CJsonData *pTable = m_pData->GetChild(Full);

				Data = pTable->GetAsText(FALSE);
			}
			else {
				Data = m_pData->GetValue(Full);
			}

			Elem.m_pElem->SetData(Data);

			if( !m_fReadOnly ) {

				CString Read = Elem.m_pElem->GetData();

				if( Read.CompareC(Data) ) {

					if( Elem.m_fTable ) {

						CJsonData *pTable = m_pData->GetChild(Full);

						pTable->Parse(Read);
					}
					else {
						m_pData->AddValue(Full, Read, jsonString);
					}
				}
			}
		}

		while( uFrom < id ) {

			m_Ids.Insert(uFrom, n);

			uFrom++;
		}

		/*if( m_fRead ) {

		}*/

		Elem.m_pElem->ShowControls(TRUE);
	}

	m_uMaxId = id;
}

void CDevConTabWnd::EnableUI(CString const &Name)
{
	BOOL u = FALSE;

	UINT c = m_Sections.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CSection const &Sect = m_Sections[n];

		if( Sect.m_Enable.Find(Name) < NOTHING ) {

			BOOL fEnable = Evaluate(Sect.m_Enable);

			if( Sect.m_fState != fEnable ) {

				u = TRUE;
			}
		}
	}

	if( !u ) {

		UINT c = m_Elements.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CElement const &Elem = m_Elements[n];

			if( Elem.m_pField ) {

				CString const &Enable = Elem.m_Enable;

				if( !Enable.IsEmpty() ) {

					if( Name.IsEmpty() || Enable.Find(Name) < NOTHING ) {

						EnableUI(Elem);
					}
				}
			}
		}

		return;
	}

	PostMessage(WM_COMMAND, 100);
}

void CDevConTabWnd::EnableUI(CElement const &Elem)
{
	BOOL fEnable = Evaluate(Elem.m_Enable);

	Elem.m_pElem->EnableControls(fEnable);
}

void CDevConTabWnd::LayoutUI(void)
{
	CRect m_UIRect;

	CRect m_Active;

	int  nExtra  = m_fScroll ? 60000 : 0;

	BOOL fScroll = m_fScroll;

	for( ;;) {

		try {
			CClientDC DC(ThisObject);

			m_pForm->Prepare(DC);

			m_UIRect = GetClientRect() - 6;

			if( fScroll ) {

				CRect Shift   = m_UIRect;

				Shift.top    -= m_nScroll;

				Shift.bottom += nExtra;

				m_pForm->SetRect(Shift);

				int xSize = m_UIRect.cx();

				int ySize = m_pForm->GetMinSize().cy;

				m_Active  = CRect(Shift.GetTopLeft(), CSize(xSize, ySize));

				if( ySize > m_UIRect.cy() ) {

					m_pScroll->SetScrollRange(0, ySize, FALSE);

					m_pScroll->SetPageSize(m_UIRect.cy(), FALSE);

					m_pScroll->SetScrollPos(m_nScroll, TRUE);

					m_pScroll->ShowWindow(SW_SHOW);
				}
				else
					m_pScroll->ShowWindow(SW_HIDE);
			}
			else {
				m_pForm->SetRect(m_UIRect);

				m_pScroll->ShowWindow(SW_HIDE);
			}

			/*if( !m_fValid ) {

				ShowUI(TRUE);

				m_fValid = TRUE;
			}*/

			return;
		}

		catch( CUserException const & ) {

			if( !fScroll ) {

				int ySize = m_UIRect.cy() - m_nScroll;

				int yNeed = m_pForm->GetMinSize().cy;

				fScroll = TRUE;

				nExtra  = yNeed - ySize;

				continue;
			}

			/*if( m_fValid ) {

				ShowUI(FALSE);

				m_fValid = FALSE;
			}*/

			m_pScroll->ShowWindow(SW_HIDE);

			return;
		}
	}
}

void CDevConTabWnd::KillUI(void)
{
	UINT c = m_Elements.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CElement const &Elem = m_Elements[n];

		Elem.m_pElem->DestroyControls();

		delete Elem.m_pElem;
	}

	delete m_pForm;

	m_pForm = NULL;
}

BOOL CDevConTabWnd::CheckScroll(void)
{
/*	if( IsCurrent() && m_pFocus ) {

		CRect Ctrl = m_pFocus->GetWindowRect();

		ScreenToClient(Ctrl);

		int nMargin = 32;

		int nScroll = 0;

		if( Ctrl.bottom > m_UIRect.bottom - nMargin ) {

			nScroll = m_UIRect.bottom - nMargin - Ctrl.bottom;
		}

		if( Ctrl.top < m_UIRect.top + nMargin ) {

			nScroll = m_UIRect.top + nMargin - Ctrl.top;
		}

		return Scroll(nScroll);
	}

*/	return FALSE;
}

BOOL CDevConTabWnd::Scroll(int nScroll)
{
/*	int nLimit = m_Active.cy() - m_UIRect.cy();

	MakeMax(nLimit, 0);

	MakeMin(nScroll, m_nScroll);

	MakeMax(nScroll, m_nScroll - nLimit);

	if( nScroll ) {

		HideToolTip();

		UpdateWindow();

		ScrollWindowEx(m_hWnd,
			       0,
			       nScroll,
			       m_Active,
			       NULL,
			       NULL,
			       NULL,
			       SW_SCROLLCHILDREN | SW_INVALIDATE | SW_ERASE
		);

		m_Active.top    += nScroll;

		m_Active.bottom += nScroll;

		m_nScroll -= nScroll;

		m_pScroll->SetScrollPos(m_nScroll, TRUE);

		OffsetUI();

		PositionUI();

		MoveToolTips();

		return TRUE;
	}

*/	return FALSE;
}

BOOL IsNumber(CString const &Data)
{
	for( UINT n = 0; Data[n]; n++ ) {

		if( !isdigit(Data[n]) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CDevConTabWnd::Evaluate(CString const &Code)
{
	CStringArray List;

	Code.Tokenize(List, ',');

	CStringArray Stack;

	UINT c = List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		UINT s = Stack.GetCount();

		if( IsNumber(List[n]) ) {

			Stack.Append(List[n]);

			continue;
		}

		if( List[n] == "==" ) {

			CString b = Stack[s-1];
			CString a = Stack[s-2];

			Stack.Remove(s-2, 2);

			Stack.Append((a.Mid(0, 1) == L"=" || b.Mid(0, 1) == L"=" || (watoi(a) == watoi(b))) ? L"1" : L"0");

			continue;
		}

		if( List[n] == "!=" ) {

			CString b = Stack[s-1];
			CString a = Stack[s-2];

			Stack.Remove(s-2, 2);

			Stack.Append((a.Mid(0, 1) == "=" || b.Mid(0, 1) == "=" || (watoi(a) != watoi(b))) ? L"1" : L"0");

			continue;
		}

		if( List[n] == ">=" ) {

			CString b = Stack[s-1];
			CString a = Stack[s-2];

			Stack.Remove(s-2, 2);

			Stack.Append((a.Mid(0, 1) == "=" || b.Mid(0, 1) == "=" || (watoi(a) >= watoi(b))) ? L"1" : L"0");

			continue;
		}

		if( List[n] == "&&" ) {

			CString b = Stack[s-1];
			CString a = Stack[s-2];

			Stack.Remove(s-2, 2);

			Stack.Append((watoi(a) && watoi(b)) ? L"1" : L"0");

			continue;
		}

		if( List[n] == "||" ) {

			CString b = Stack[s-1];
			CString a = Stack[s-2];

			Stack.Remove(s-2, 2);

			Stack.Append((watoi(a) || watoi(b)) ? L"1" : L"0");

			continue;
		}

		if( List[n][0] == '~' ) {

			CString Name = Join(m_Path, List[n].Mid(1));

			CString Data = m_pData->GetValue(Name);

			Stack.Append(Data.IsEmpty() ? L"1" : L"0");

			continue;
		}

		UINT    uPos = List[n].Find('.');

		CString Full = (uPos == NOTHING) ? Join(m_Path, List[n]) : List[n];

		CString Data = m_pData->GetValue(Full);

		Stack.Append(Data.IsEmpty() ? L"0" : Data);
	}

	if( Stack.GetCount() == 1 ) {

		return watoi(Stack[0]) ? TRUE : FALSE;
	}

	return TRUE;
}

void CDevConTabWnd::TakeAction(CElement const &Elem, UINT uCode)
{
	if( uCode == actionChange || uCode == actionLayout ) {

		CString Data = Elem.m_pElem->GetData();

		CString Full = Join(m_Path, Elem.m_Name);

		CString Prev;

		if( Elem.m_fTable ) {

			CJsonData *pTable = m_pData->GetChild(Full);

			Prev = pTable->GetAsText(FALSE);

			pTable->Parse(Data);
		}
		else {
			Prev = m_pData->GetValue(Full);

			m_pData->AddValue(Full, Data, jsonString);
		}

		if( m_fHasUndo ) {

			CString Item  = m_System.GetNavPos();

			CCmd *  pTest = m_System.GetLastCmd();

			BOOL    fSave = TRUE;

			if( pTest && pTest->IsKindOf(AfxRuntimeClass(CCmdEdit)) ) {

				CCmdEdit *pLast = (CCmdEdit *) pTest;

				if( pLast->m_Item == Item && pLast->m_Name == Elem.m_Name ) {

					if( Data == pLast->m_Prev ) {

						m_System.KillLastCmd();

						fSave = FALSE;
					}
					else {
						if( !Elem.m_fTable ) {

							pLast->m_Data = Data;

							fSave = FALSE;
						}
					}
				}
			}

			if( fSave ) {

				CCmdEdit *pCmd = New CCmdEdit(CPrintf(IDS("Change to %s"), Elem.m_Label),
							      Item,
							      Elem.m_Name,
							      Prev,
							      Data
				);

				m_System.SaveCmd(pCmd);
			}

			SetDirty();
		}

		if( uCode == actionLayout ) {

			// This is where we should relayout the UI to deal with the
			// fact that tables etc. might have changed in height...!!!
		}

		EnableUI(Elem);

		EnableUI(Elem.m_Name);
	}
}

void CDevConTabWnd::PlayEditCmd(CCmdEdit *pCmd, BOOL fExec)
{
	INDEX i = m_Names.FindName(pCmd->m_Name);

	if( !m_Names.Failed(i) ) {

		CElement const &Elem = m_Elements[m_Names[i]];

		CString Data = fExec ? pCmd->m_Data : pCmd->m_Prev;

		CString Full = Join(m_Path, Elem.m_Name);

		if( Elem.m_fTable ) {

			CJsonData *pTable = m_pData->GetChild(Full);

			pTable->Parse(Data);
		}
		else {
			m_pData->AddValue(Full, Data, jsonString);
		}

		SetDirty();

		Elem.m_pElem->SetData(Data);

		EnableUI(Elem);

		EnableUI(Elem.m_Name);
	}
}

void CDevConTabWnd::SetDirty(void)
{
	if( m_pItem ) {

		m_pItem->SetDirty();
	}
}

// End of File
