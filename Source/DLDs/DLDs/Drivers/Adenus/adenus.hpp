
//////////////////////////////////////////////////////////////////////////
//
// Adenus Telnet Slave
//

class CAdenusDriver : public CSlaveDriver
{
	public:
		// Constructor
		CAdenusDriver(void);

		// Destructor
		~CAdenusDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:
		// Data Members
		ISocket	      *	m_pSock;
		IExtraHelper  *	m_pExtra;
		UINT		m_uState;
		char		m_sLine[40];
		char		m_sUser[40];
		char		m_sPass[40];
		UINT		m_uPtr;
		UINT		m_uTime;
		
		// Implementation
		BOOL TestSocket(void);
		void SendPrompt(void);
		void Send(PCTXT pText);
		void GetLine(void);
		void CheckLine(void);
		void Error(UINT uCode);
		void Logon(void);
	};

// End of File
