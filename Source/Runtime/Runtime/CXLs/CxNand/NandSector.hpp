
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Utilities
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandSector_HPP

#define	INCLUDE_NandSector_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "NandPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Sector Selector
//

class DLLAPI CNandSector : public CNandPage
{
	public:
		// Constructor
		CNandSector(void);
		CNandSector(CNandBlock const &Block);
		CNandSector(CNandPage  const &Page);
		CNandSector(CNandPage  const &Page, UINT uSector);
		CNandSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSector);

		// Public Data
		UINT m_uSector;

		// Operators
		bool operator == (CNandSector const &That) const;
		bool operator != (CNandSector const &That) const;
		bool operator  > (CNandSector const &That) const;
		bool operator  < (CNandSector const &That) const;

		// Attributes
		bool IsValid(void) const;

		// Operations
		void Invalidate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructor

STRONG_INLINE CNandSector::CNandSector(void)
{
	m_uSector = 0;
	}

STRONG_INLINE CNandSector::CNandSector(CNandBlock const &Block) : CNandPage(Block, 0)
{
	m_uSector = 0;
	}

STRONG_INLINE CNandSector::CNandSector(CNandPage const &Page) : CNandPage(Page)
{
	m_uSector = 0;
	}

STRONG_INLINE CNandSector::CNandSector(CNandPage const &Page, UINT uSector) : CNandPage(Page)
{
	m_uSector = uSector;
	}

STRONG_INLINE CNandSector::CNandSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSector) : CNandPage(uChip, uBlock, uPage)
{
	m_uSector = uSector;
	}

// Operators

STRONG_INLINE bool CNandSector::operator == (CNandSector const &That) const
{	
	if( CNandPage::operator == (That) ) {

		if( m_uSector == That.m_uSector ) {

			return true;
			}
		}

	return false;
	}

STRONG_INLINE bool CNandSector::operator != (CNandSector const &That) const
{	
	if( CNandPage::operator == (That) ) {

		if( m_uSector == That.m_uSector ) {

			return false;
			}
		}

	return true;
	}

STRONG_INLINE bool CNandSector::operator > (CNandSector const &That) const
{
	if( CNandPage::operator > (That) ) {

		return true;
		}

	if( CNandPage::operator < (That) ) {

		return false;
		}

	return m_uSector > That.m_uSector;
	}

STRONG_INLINE bool CNandSector::operator < (CNandSector const &That) const
{	
	if( CNandPage::operator < (That) ) {

		return true;
		}

	if( CNandPage::operator > (That) ) {

		return false;
		}

	return m_uSector < That.m_uSector;
	}

// Attributes

STRONG_INLINE bool CNandSector::IsValid(void) const
{
	if( m_uSector == NOTHING ) {

		return false;
		}

	return CNandPage::IsValid();
	}

// Operations

STRONG_INLINE void CNandSector::Invalidate(void)
{
	m_uSector = NOTHING;

	CNandPage::Invalidate();
	}

// End of File

#endif
