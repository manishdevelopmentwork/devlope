
#include "intern.hpp"

#include "quickag.hpp"

//#define QDEBUG FALSE

// Command Definitions

//struct FAR QUICKSILVERCmdDef {
//	UINT	uID;		// Config Table/Offset
//	UINT	uCmd;		// Quicksilver command value/Cache position indicator
//	UINT	uWrite;		// Read/Write Status
//	UINT	uSize;		// Integer size, or # of cached items
//	BOOL	fSigned;	// Signed int
//	UINT	uArrPos;	// Array position
//	};
	
QUICKSILVERCmdDef CODE_SEG CQuicksilverDriver::CL[] = {

	// Table Items
	{IDCIO,		OPCIO,		WO0,	0,	0,	0},
	{IDRPB,		OPRPB,		RO,	0,	1,	0},
	{IDREG,		OPREG,		RW,	0,	1,	0},
	{IDWRP,		OPWRP,		WO,	4,	1,	0},
	{IDCII,		OPCII,		WO0,	0,	1,	0},
	{IDCIE,		OPCIO,		WO0,	0,	1,	0},
	{IDCIIE,	OPCII,		WO0,	0,	1,	0},

	// Named Items
	{IDACR,		OPACR,		WO,	2,	0,	APACRC},
	{IDACRC,	OPACRC,		WO,	2,	1,	APACRC},
	{IDACRD,	OPACRD,		WO,	2,	1,	APACRD},
	{IDADL,		OPADL,		WO0,	2,	1,	0},
	{IDAHC,		OPAHC,		WO,	2,	0,	APAHCOC},
	{IDAHCOC,	OPAHCOC,	WO,	2,	1,	APAHCOC},
	{IDAHCCO,	OPAHCCO,	WO,	2,	1,	APAHCCO},
	{IDAHD,		OPAHD,		WO0,	2,	0,	0},
	{IDARI,		OPARI,		WO,	2,	0,	APARIC},
	{IDARIC,	OPARIC,		WO,	2,	1,	APARIC},
	{IDARID,	OPARID,		WO,	2,	1,	APARID},
	{IDATR,		OPATR,		WO,	2,	0,	APATRA},
	{IDATRA,	OPATRA,		WO,	2,	0,	APATRA},
	{IDATRD,	OPATRD,		WO,	4,	1,	APATRD},
	{IDCIS,		OPCIS,		WC,	0,	0,	0},
	{IDCKS,		OPCKS,		WO,	2,	0,	APCKSE},
	{IDCKSE,	OPCKSE,		WO,	2,	0,	APCKSE},
	{IDCKSS,	OPCKSS,		WO,	2,	0,	APCKSS},
	{IDCLC,		OPCLC,		WO,	2,	0,	APCLCO},
	{IDCLCO,	OPCLCO,		WO,	2,	0,	APCLCO},
	{IDCLCD,	OPCLCD,		WO,	2,	0,	APCLCD},
	{IDCME,		OPCME,		WC,	0,	0,	0},
	{IDCOB,		OPCOB,		WO,	2,	1,	0},
	{IDCPL,		OPCPL,		WO,	0,	0,	0},
	{IDCTC,		OPCTC,		WO,	7,	0,	APCTC1},
	{IDCTC1,	OPCTC1,		WO,	2,	0,	APCTC1},
	{IDCTC2,	OPCTC2,		WO,	2,	0,	APCTC2},
	{IDCTCV,	OPCTCV,		WO,	2,	0,	APCTCV},
	{IDCTCB,	OPCTCB,		WO,	2,	0,	APCTCB},
	{IDCTCF,	OPCTCF,		WO,	2,	0,	APCTCF},
	{IDCTCP,	OPCTCP,		WO,	2,	0,	APCTCP},
	{IDCTCI,	OPCTCI,		WO,	2,	0,	APCTCI},
	{IDDEM,		OPDEM,		WO,	0,	0,	0},
	{IDDDB,		OPDDB,		WC,	0,	0,	0},
	{IDDIF,		OPDIF,		WO,	2,	0,	APDIFL},
	{IDDIFL,	OPDIFL,		WO,	2,	0,	APDIFL},
	{IDDIFF,	OPDIFF,		WO,	2,	0,	APDIFF},
	{IDDIR,		OPDIR,		WO0,	2,	1,	0},
	{IDDLC,		OPDLC,		WC,	0,	0,	0},
	{IDDLY,		OPDLY,		WO,	4,	1,	0},
	{IDDMD,		OPDMD,		WC,	0,	0,	0},
	{IDDMT,		OPDMT,		WC,	0,	0,	0},
	{IDEDH,		OPEDH,		WC,	0,	0,	0},
	{IDEDL,		OPEDL,		WO,	0,	0,	0},
	{IDEEM,		OPEEM,		WC,	0,	0,	0},
	{IDEMD,		OPEMD,		WC,	0,	0,	0},
	{IDEMT,		OPEMT,		WC,	0,	0,	0},
	{IDEND,		OPEND,		WC,	0,	0,	0},
	{IDERL,		OPERL,		WO,	3,	0,	APERLM},
	{IDERLM,	OPERLM,		WO,	2,	1,	APERLM},
	{IDERLH,	OPERLH,		WO,	2,	1,	APERLH},
	{IDERLD,	OPERLD,		WO,	2,	1,	APERLD},
	{IDFLC,		OPFLC,		WO,	3,	0,	APFLC1},
	{IDFLC1,	OPFLC1,		WO,	2,	1,	APFLC1},
	{IDFLC2,	OPFLC2,		WO,	2,	1,	APFLC2},
	{IDFLCA,	OPFLCA,		WO,	2,	1,	APFLCA},
	{IDGCL,		OPGCL,		WC,	0,	0,	0},
	{IDGOC,		OPGOC,		WO0,	2,	1,	0},
	{IDGOP,		OPGOP,		WC,	0,	0,	0},
	{IDHSM,		OPHSM,		WC,	0,	0,	0},
	{IDIDT,		OPIDT,		WO,	2,	0,	APIDTG},
	{IDIDTG,	OPIDTG,		WO,	2,	0,	APIDTG},
	{IDIDTU,	OPIDTU,		WO,	2,	0,	APIDTU},
	{IDJAN,		OPJAN,		WO,	3,	0,	APJANE},
	{IDJANE,	OPJANE,		WO,	2,	0,	APJANE},
	{IDJANS,	OPJANS,		WO,	2,	0,	APJANS},
	{IDJANL,	OPJANL,		WO,	2,	0,	APJANL},
	{IDJMP,		OPJMP,		WO,	3,	0,	APJMPE},
	{IDJMPE,	OPJMPE,		WO,	2,	0,	APJMPE},
	{IDJMPS,	OPJMPS,		WO,	2,	0,	APJMPS},
	{IDJMPA,	OPJMPA,		WO,	2,	0,	APJMPA},
	{IDJNA,		OPJNA,		WO,	3,	0,	APJNAE},
	{IDJNAE,	OPJNAE,		WO,	2,	0,	APJNAE},
	{IDJNAS,	OPJNAS,		WO,	2,	0,	APJNAS},
	{IDJNAL,	OPJNAL,		WO,	2,	0,	APJNAL},
	{IDJOI,		OPJOI,		WO,	3,	0,	APJOIC},
	{IDJOIC,	OPJOIC,		WO,	2,	1,	APJOIC},
	{IDJOIS,	OPJOIS,		WO,	2,	1,	APJOIS},
	{IDJOIA,	OPJOIA,		WO,	2,	0,	APJOIA},
	{IDJOR,		OPJOR,		WO,	3,	0,	APJORE},
	{IDJORE,	OPJORE,		WO,	2,	0,	APJORE},
	{IDJORS,	OPJORS,		WO,	2,	0,	APJORS},
	{IDJORL,	OPJORL,		WO,	2,	0,	APJORL},
	{IDKDD,		OPKDD,		WC,	0,	0,	0},
	{IDKED,		OPKED,		WC,	0,	0,	0},
	{IDKMC,		OPKMC,		WO,	2,	0,	APKMCE},
	{IDKMCE,	OPKMCE,		WO,	2,	0,	APKMCE},
	{IDKMCS,	OPKMCS,		WO,	2,	0,	APKMCS},
	{IDKMR,		OPKMR,		WO0,	2,	1,	0},
	{IDLPR,		OPLPR,		WO,	2,	0,	APLPRA},
	{IDLPRA,	OPLPRA,		WO,	2,	0,	APLPRA},
	{IDLPRC,	OPLPRC,		WO,	2,	0,	APLPRC},
	{IDLRP,		OPLRP,		WO0,	2,	0,	0},
	{IDLVT,		OPLVT,		WO0,	2,	0,	0},
	{IDMAT,		OPMAT,		WO,	5,	0,	APMATP},
	{IDMATP,	OPMATP,		WO,	4,	1,	APMATP},
	{IDMATA,	OPMATA,		WO,	4,	0,	APMATA},
	{IDMATT,	OPMATT,		WO,	4,	0,	APMATT},
	{IDMATE,	OPMATE,		WO,	2,	1,	APMATE},
	{IDMATS,	OPMATS,		WO,	2,	1,	APMATS},
	{IDMAV,		OPMAV,		WO,	5,	0,	APMAVP},
	{IDMAVP,	OPMAVP,		WO,	4,	1,	APMAVP},
	{IDMAVA,	OPMAVA,		WO,	4,	0,	APMAVA},
	{IDMAVV,	OPMAVV,		WO,	4,	0,	APMAVV},
	{IDMAVE,	OPMAVE,		WO,	2,	1,	APMAVE},
	{IDMAVS,	OPMAVS,		WO,	2,	1,	APMAVS},
	{IDMDC,		OPMDC,		WC,	0,	0,	0},
	{IDMDS,		OPMDS,		WO,	3,	0,	APMDSC},
	{IDMDSC,	OPMDSC,		WO,	2,	1,	APMDSC},
	{IDMDSE,	OPMDSE,		WO,	2,	1,	APMDSE},
	{IDMDSF,	OPMDSF,		WO,	2,	0,	APMDSF},
	{IDMDT,		OPMDT,		WO0,	2,	1,	0},
	{IDMRT,		OPMRT,		WO,	5,	0,	APMRTD},
	{IDMRTD,	OPMRTD,		WO,	4,	1,	APMRTD},
	{IDMRTA,	OPMRTA,		WO,	4,	0,	APMRTA},
	{IDMRTT,	OPMRTT,		WO,	4,	0,	APMRTT},
	{IDMRTE,	OPMRTE,		WO,	2,	1,	APMRTE},
	{IDMRTS,	OPMRTS,		WO,	2,	1,	APMRTS},
	{IDMRV,		OPMRV,		WO,	5,	0,	APMRVD},
	{IDMRVD,	OPMRVD,		WO,	4,	1,	APMRVD},
	{IDMRVA,	OPMRVA,		WO,	4,	0,	APMRVA},
	{IDMRVV,	OPMRVV,		WO,	4,	0,	APMRVV},
	{IDMRVE,	OPMRVE,		WO,	2,	1,	APMRVE},
	{IDMRVS,	OPMRVS,		WO,	2,	1,	APMRVS},
	{IDMTT,		OPMTT,		WO0,	2,	0,	0},
	{IDOLP,		OPOLP,		WO,	2,	1,	0},
	{IDOVT,		OPOVT,		WO,	2,	0,	0},
	{IDPAC,		OPPAC,		WO,	3,	0,	0},
	{IDPAC1A,	OPPAC1A,	WO,	2,	1,	0},
	{IDPAC2A,	OPPAC2A,	WO,	2,	1,	APPACL},
	{IDPACL,	OPPACL,		WO,	2,	1,	APPACL},
	{IDPCG,		OPPCG,		WC,	0,	0,	0},
	{IDPCI,		OPPCI,		WO,	3,	0,	APPCIL},
	{IDPCIL,	OPPCIL,		WO,	2,	1,	APPCIL},
	{IDPCIE,	OPPCIE,		WO,	2,	1,	APPCIE},
	{IDPCIS,	OPPCIS,		WO,	2,	0,	APPCIS},
	{IDPCL,		OPPCL,		WO,	3,	0,	APPCLL},
	{IDPCLL,	OPPCLL,		WO,	2,	0,	APPCLL},
	{IDPCLE,	OPPCLE,		WO,	2,	0,	APPCLE},
	{IDPCLS,	OPPCLS,		WO,	2,	0,	APPCLS},
	{IDPCM,		OPPCM,		WC,	0,	0,	0},
	{IDPCP,		OPPCP,		WO0,	2,	0,	0},
	{IDPIM,		OPPIM,		WO,	3,	0,	APPIMF},
	{IDPIMF,	OPPIMF,		WO,	2,	1,	APPIMF},
	{IDPIME,	OPPIME,		WO,	2,	1,	APPIME},
	{IDPIMS,	OPPIMS,		WO,	2,	1,	APPIMS},
	{IDPLG,		OPPLG,		RO,	0,	0,	0},
	{IDPLR,		OPPLR,		WO0,	2,	1,	0},
	{IDPMC,		OPPMC,		WO,	2,	0,	APPMCE},
	{IDPMCE,	OPPMCE,		WO,	2,	1,	APPMCE},
	{IDPMCS,	OPPMCS,		WO,	2,	1,	APPMCS},
	{IDPMO,		OPPMO,		WO,	2,	0,	APPMOE},
	{IDPMOE,	OPPMOE,		WO,	2,	1,	APPMOE},
	{IDPMOS,	OPPMOS,		WO,	2,	1,	APPMOS},
	{IDPMV,		OPPMV,		WO,	2,	0,	APPMVE},
	{IDPMVE,	OPPMVE,		WO,	2,	1,	APPMVE},
	{IDPMVS,	OPPMVS,		WO,	2,	1,	APPMVS},
	{IDPMX,		OPPMX,		WC,	0,	0,	0},
	{IDPOL,		OPPOL,		RO,	0,	0,	0},
	{IDPRI,		OPPRI,		WO,	2,	0,	APPRIE},
	{IDPRIE,	OPPRIE,		WO,	2,	1,	APPRIE},
	{IDPRIS,	OPPRIS,		WO,	2,	1,	APPRIS},
	{IDPRT,		OPPRT,		WO,	2,	0,	APPRTE},
	{IDPRTE,	OPPRTE,		WO,	2,	0,	APPRTE},
	{IDPRTS,	OPPRTS,		WO,	2,	0,	APPRTS},
	{IDPUN,		OPPUN,		RW,	0,	0,	0},
	{IDRAT,		OPRAT,		WO,	5,	0,	APRATD},
	{IDRATD,	OPRATD,		WO,	4,	1,	APRATD},
	{IDRATA,	OPRATA,		WO,	4,	0,	APRATA},
	{IDRATT,	OPRATT,		WO,	4,	0,	APRATT},
	{IDRATE,	OPRATE,		WO,	2,	1,	APRATE},
	{IDRATS,	OPRATS,		WO,	2,	1,	APRATS},
	{IDRAV,		OPRAV,		WO,	5,	0,	APRAVD},
	{IDRAVD,	OPRAVD,		WO,	4,	1,	APRAVD},
	{IDRAVA,	OPRAVA,		WO,	4,	0,	APRAVA},
	{IDRAVV,	OPRAVV,		WO,	4,	0,	APRAVV},
	{IDRAVE,	OPRAVE,		WO,	2,	1,	APRAVE},
	{IDRAVS,	OPRAVS,		WO,	2,	1,	APRAVS},
	{IDRIO,		OPRIO,		RO,	0,	0,	0},
	{IDRIS,		OPRIS,		RO,	0,	0,	0},
	{IDRLM,		OPRLM,		WO,	3,	0,	APRLMN},
	{IDRLMN,	OPRLMN,		WO,	2,	1,	APRLMN},
	{IDRLMD,	OPRLMD,		WO,	2,	1,	APRLMD},
	{IDRLMA,	OPRLMA,		WO,	2,	0,	APRLMA},
	{IDRLN,		OPRLN,		WO,	2,	0,	APRLND},
	{IDRLND,	OPRLND,		WO,	2,	1,	APRLND},
	{IDRLNA,	OPRLNA,		WO,	2,	0,	APRLNA},
	{IDRRT,		OPRRT,		WO,	5,	0,	APRRTD},
	{IDRRTD,	OPRRTD,		WO,	4,	1,	APRRTD},
	{IDRRTA,	OPRRTA,		WO,	4,	0,	APRRTA},
	{IDRRTT,	OPRRTT,		WO,	4,	0,	APRRTT},
	{IDRRTE,	OPRRTE,		WO,	2,	1,	APRRTE},
	{IDRRTS,	OPRRTS,		WO,	2,	1,	APRRTS},
	{IDRRV,		OPRRV,		WO,	5,	0,	APRRVD},
	{IDRRVD,	OPRRVD,		WO,	4,	1,	APRRVD},
	{IDRRVA,	OPRRVA,		WO,	4,	0,	APRRVA},
	{IDRRVV,	OPRRVV,		WO,	4,	0,	APRRVV},
	{IDRRVE,	OPRRVE,		WO,	2,	1,	APRRVE},
	{IDRRVS,	OPRRVS,		WO,	2,	1,	APRRVS},
	{IDRSD,		OPRSD,		WO,	2,	0,	0},
	{IDRSM,		OPRSM,		WO,	2,	0,	APRSMN},
	{IDRSMN,	OPRSMN,		WO,	3,	1,	APRSMN},
	{IDRSMD,	OPRSMD,		WO,	2,	1,	APRSMD},
	{IDRSMA,	OPRSMA,		WO,	2,	0,	APRSMA},
	{IDRSN,		OPRSN,		WO,	2,	0,	APRSND},
	{IDRSND,	OPRSND,		WO,	2,	1,	APRSND},
	{IDRSNA,	OPRSNA,		WO,	2,	0,	APRSNA},
	{IDRST,		OPRST,		WC,	0,	0,	0},
	{IDRUN,		OPRUN,		WC,	0,	0,	0},
	{IDSCF,		OPSCF,		WO0,	0,	1,	0},
	{IDSEE,		OPSEE,		WO,	3,	0,	APSEEI},
	{IDSEEI,	OPSEEI,		WO,	2,	1,	APSEEI},
	{IDSEES,	OPSEES,		WO,	2,	1,	APSEES},
	{IDSEEE,	OPSEEE,		WO,	2,	0,	APSEEE},
	{IDSIF,		OPSIF,		WO0,	2,	1,	0},
	{IDSLC,		OPSLC,		WC,	0,	0,	0},
	{IDSOB,		OPSOB,		WO,	2,	1,	0},
	{IDSSD,		OPSSD,		WO0,	2,	1,	0},
	{IDSSE,		OPSSE,		WC,	0,	0,	0},
	{IDSSL,		OPSSL,		WO0,	2,	1,	0},
	{IDSSP,		OPSSP,		WC,	0,	0,	0},
	{IDSTP,		OPSTP,		WO0,	4,	1,	0},
	{IDTQL,		OPTQL,		WO,	4,	0,	0},
	{IDTQLCH,	OPTQLCH,	WO,	2,	0,	0},
	{IDTQLCM,	OPTQLCM,	WO,	2,	0,	0},
	{IDTQLOH,	OPTQLOH,	WO,	2,	0,	0},
	{IDTQLOM,	OPTQLOM,	WO,	2,	0,	0},
	{IDTRU,		OPTRU,		WO,	2,	0,	APTRUF},
	{IDTRUF,	OPTRUF,		WO,	2,	1,	APTRUF},
	{IDTRUI,	OPTRUI,		WO,	2,	1,	APTRUI},
	{IDTTP,		OPTTP,		WC,	0,	0,	0},
	{IDVIM,		OPVIM,		WO,	3,	0,	APVIMF},
	{IDVIMF,	OPVIMF,		WO,	2,	1,	APVIMF},
	{IDVIME,	OPVIME,		WO,	2,	1,	APVIME},
	{IDVIMS,	OPVIMS,		WO,	2,	1,	APVIMS},
	{IDVMI,		OPVMI,		WO,	4,	0,	APVMIA},
	{IDVMIA,	OPVMIA,		WO,	4,	1,	APVMIA},
	{IDVMIV,	OPVMIV,		WO,	4,	1,	APVMIV},
	{IDVMIE,	OPVMIE,		WO,	2,	1,	APVMIE},
	{IDVMIS,	OPVMIS,		WO,	2,	1,	APVMIS},
	{IDVMP,		OPVMP,		WO,	4,	0,	APVMPA},
	{IDVMPA,	OPVMPA,		WO,	4,	0,	APVMPA},
	{IDVMPV,	OPVMPV,		WO,	4,	1,	APVMPV},
	{IDVMPE,	OPVMPE,		WO,	2,	1,	APVMPE},
	{IDVMPS,	OPVMPS,		WO,	2,	1,	APVMPS},
	{IDWBE,		OPWBE,		WO,	2,	0,	APWBEC},
	{IDWBEC,	OPWBEC,		WO,	2,	1,	APWBEC},
	{IDWBET,	OPWBET,		WO,	2,	1,	APWBET},
	{IDWBS,		OPWBS,		WO,	2,	0,	APWBSC},
	{IDWBSC,	OPWBSC,		WO,	2,	1,	APWBSC},
	{IDWBSS,	OPWBSS,		WO,	2,	1,	APWBSS},
	{IDWDL,		OPWDL,		WC,	0,	0,	0},
	{IDXAT,		OPXAT,		WO,	3,	0,	APXATD},
	{IDXATD,	OPXATD,		WO,	4,	1,	APXATD},
	{IDXATE,	OPXATE,		WO,	2,	1,	APXATE},
	{IDXATS,	OPXATS,		WO,	2,	1,	APXATS},
	{IDXAV,		OPXAV,		WO,	3,	0,	APXAVD},
	{IDXAVD,	OPXAVD,		WO,	4,	1,	APXAVD},
	{IDXAVE,	OPXAVE,		WO,	2,	1,	APXAVE},
	{IDXAVS,	OPXAVS,		WO,	2,	1,	APXAVS},
	{IDXRT,		OPXRT,		WO,	3,	0,	APXRTD},
	{IDXRTD,	OPXRTD,		WO,	2,	1,	APXRTD},
	{IDXRTE,	OPXRTE,		WO,	2,	1,	APXRTE},
	{IDXRTS,	OPXRTS,		WO,	2,	1,	APXRTS},
	{IDXRV,		OPXRV,		WO,	3,	0,	APXRVD},
	{IDXRVD,	OPXRVD,		WO,	2,	1,	APXRVD},
	{IDXRVE,	OPXRVE,		WO,	2,	1,	APXRVE},
	{IDXRVS,	OPXRVS,		WO,	2,	1,	APXRVS},
	{IDZTG,		OPZTG,		WC,	2,	0,	0},
	{IDZTP,		OPZTP,		WC,	0,	0,	0},
	{IDERR,		OPERR,		RW,	0,	0,	0},
// Added 22 Dec 2005 for Silverdust
	{IDAHM,		OPAHM,		WO0,	2,	1,	0},
	{IDCLM,		OPCLM,		WO0,	2,	0,	0},
	{IDLVP,		OPLVP,		WO0,	2,	0,	0},
	{IDSEF,		OPSEF,		WO0,	2,	1,	0},
	{IDSPR,		OPSPR,		WO0,	4,	0,	0},
	{IDIMQ,		OPIMQ,		WC,	0,	0,	0},
	{IDIMS,		OPIMS,		WC,	0,	0,	0},
	{IDRSP,		OPRSP,		WC,	0,	0,	0},
	{IDPWO,		OPPWO,		WO,	2,	0,	APPWOR},
	{IDPWOR,	OPPWOR,		WO,	2,	1,	APPWOR},
	{IDPWOM,	OPPWOM,		WO,	2,	1,	APPWOM},
	{IDKMX,		OPKMX,		WO,	6,	0,	APKMX1},
	{IDKMX1,	OPKMX1,		WO,	2,	0,	APKMX1},
	{IDKMX2,	OPKMX2,		WO,	2,	0,	APKMX2},
	{IDKMX3,	OPKMX3,		WO,	2,	0,	APKMX3},
	{IDKMX4,	OPKMX4,		WO,	2,	0,	APKMX4},
	{IDKMX5,	OPKMX5,		WO,	2,	0,	APKMX5},
	{IDKMX6,	OPKMX6,		WO,	2,	0,	APKMX6},
	{IDMCT,		OPMCT,		WO,	8,	0,	APMCT1},
	{IDMCT1,	OPMCT1,		WO,	4,	1,	APMCT1},
	{IDMCT2,	OPMCT2,		WO,	4,	1,	APMCT2},
	{IDMCT3,	OPMCT3,		WO,	4,	1,	APMCT3},
	{IDMCT4,	OPMCT4,		WO,	4,	1,	APMCT4},
	{IDMCT5,	OPMCT5,		WO,	4,	1,	APMCT5},
	{IDMCT6,	OPMCT6,		WO,	4,	1,	APMCT6},
	{IDMCT7,	OPMCT7,		WO,	4,	1,	APMCT7},
	{IDMCT8,	OPMCT8,		WO,	4,	1,	APMCT8},
	{IDSDL,		OPSDL,		WC,	0,	0,	0},
	{IDCLCA,	OPCLCA,		WO,	2,	0,	APCLC1},
	{IDCLC1,	OPCLC1,		WO,	2,	0,	APCLC1},
	{IDCLC2,	OPCLC2,		WO,	2,	0,	APCLC2},
	{IDCLD,		OPCLD,		WO,	4,	0,	APCLDR},
	{IDCLDR,	OPCLDR,		WO,	2,	0,	APCLDR},
	{IDCLDD,	OPCLDD,		WO,	4,	1,	APCLDD},
	{IDCLDO,	OPCLDO,		WO,	2,	0,	APCLDO},
	{IDCLDA,	OPCLDA,		WO,	2,	0,	APCLDA},
	{IDCLX,		OPCLX,		WO,	4,	0,	APCLX1},
	{IDCLX1,	OPCLX1,		WO,	2,	0,	APCLX1},
	{IDCLX2,	OPCLX2,		WO,	2,	0,	APCLX2},
	{IDCLXO,	OPCLXO,		WO,	2,	0,	APCLXO},
	{IDCLXA,	OPCLXA,		WO,	2,	0,	APCLXA},
//	{IDCTW,		OPCTW,		WO,	2,	0,	APCTWO},
//	{IDCTWO,	OPCTWO,		WO,	2,	0,	APCTWO},
//	{IDCTWR,	OPCTWR,		WO,	2,	0,	APCTWR},
	{IDRRW,		OPRRW,		WO,	3,	0,	APRRWO},
	{IDRRWO,	OPRRWO,		WO,	2,	0,	APRRWO},
	{IDRRWR,	OPRRWR,		WO,	2,	0,	APRRWR},
	{IDRRWD,	OPRRWD,		WO,	4,	1,	APRRWD},
	{IDWCL,		OPWCL,		WO,	2,	0,	APWCLR},
	{IDWCLR,	OPWCLR,		WO,	2,	1,	APWCLR},
	{IDWCLA,	OPWCLA,		WO,	2,	1,	APWCLA},
	{IDWCW,		OPWCW,		WO,	2,	0,	APWCWR},
	{IDWCWR,	OPWCWR,		WO,	2,	1,	APWCWR},
	{IDWCWA,	OPWCWA,		WO,	2,	1,	APWCWA},
	{IDWRF,		OPWRF,		WO,	2,	0,	APWRFR},
	{IDWRFR,	OPWRFR,		WO,	2,	0,	APWRFR},
	{IDWRFD,	OPWRFD,		WO,	4,	1,	APWRFD},
	{IDWRX,		OPWRX,		WO,	3,	0,	APWRXO},
	{IDWRXO,	OPWRXO,		WO,	2,	0,	APWRXO},
	{IDWRXR,	OPWRXR,		WO,	2,	0,	APWRXR},
	{IDWRXD,	OPWRXD,		WO,	4,	0,	APWRXD},
	{IDPUP,		OPPUP,		WO,	2,	0,	APPUPC},
	{IDPUPC,	OPPUPC,		WO,	2,	0,	APPUPC},
	{IDPUPA,	OPPUPA,		WO,	2,	0,	APPUPA},
	{IDTIM,		OPTIM,		WO,	3,	0,	APTIMF},
	{IDTIMF,	OPTIMF,		WO,	2,	1,	APTIMF},
	{IDTIME,	OPTIME,		WO,	2,	1,	APTIME},
	{IDTIMS,	OPTIMS,		WO,	2,	1,	APTIMS},
	// Added 24 Oct 2006
	{IDHLT,		OPHLT,		WC,	0,	0,	0},
	{IDADX,		OPADX,		WO0,	2,	1,	0},
	{IDCER,		OPCER,		WO0,	2,	1,	0},
	{IDT1F,		OPT1F,		WO0,	2,	0,	0},
	{IDT2K,		OPT2K,		WO0,	2,	0,	0},
	{IDC2T,		OPC2T,		WO,	8,	0,	APC2T1},
	{IDC2T1,	OPC2T1,		WO,	2,	0,	APC2T1},
	{IDC2T2,	OPC2T2,		WO,	2,	0,	APC2T2},
	{IDC2TV,	OPC2TV,		WO,	2,	0,	APC2TV},
	{IDC2TB,	OPC2TB,		WO,	2,	0,	APC2TB},
	{IDC2TC,	OPC2TC,		WO,	2,	0,	APC2TC},
	{IDC2TF,	OPC2TF,		WO,	2,	0,	APC2TF},
	{IDC2TP,	OPC2TP,		WO,	2,	0,	APC2TP},
	{IDC2TI,	OPC2TI,		WO,	2,	0,	APC2TI},
	{IDT2S,		OPT2S,		WO,	2,	0,	APT2SA},
	{IDT2SA,	OPT2SA,		WO,	2,	0,	APT2SA},
	{IDT2SZ,	OPT2SZ,		WO,	2,	0,	APT2SZ},
	{IDEMN,		OPEMN,		WO,	3,	0,	APEMNM},
	{IDEMNM,	OPEMNM,		WO,	2,	1,	APEMNM},
	{IDEMNI,	OPEMNI,		WO,	2,	1,	APEMNI},
	{IDEMNR,	OPEMNR,		WO,	2,	0,	APEMNR},
	{IDELR,		OPELR,		WO,	2,	0,	APELRP},
	{IDELRP,	OPELRP,		WO,	2,	0,	APELRP},
	{IDELRE,	OPELRE,		WO,	2,	0,	APELRE},
	{IDETN,		OPETN,		WO,	6,	0,	APETN1},
	{IDETN1,	OPETN1,		WO,	2,	1,	APETN1},
	{IDETN2,	OPETN2,		WO,	2,	1,	APETN2},
	{IDETN3,	OPETN3,		WO,	2,	1,	APETN3},
	{IDETN4,	OPETN4,		WO,	2,	1,	APETN4},
	{IDETN5,	OPETN5,		WO,	2,	1,	APETN5},
	{IDETN6,	OPETN6,		WO,	2,	1,	APETN6},
	{IDETP,		OPETP,		WO,	6,	0,	APETP1},
	{IDETP1,	OPETP1,		WO,	2,	1,	APETP1},
	{IDETP2,	OPETP2,		WO,	2,	1,	APETP2},
	{IDETP3,	OPETP3,		WO,	2,	1,	APETP3},
	{IDETP4,	OPETP4,		WO,	2,	1,	APETP4},
	{IDETP5,	OPETP5,		WO,	2,	1,	APETP5},
	{IDETP6,	OPETP6,		WO,	2,	1,	APETP6},
	{IDF2L,		OPF2L,		WO,	6,	0,	APF2LD},
	{IDF2LD,	OPF2LD,		WO,	2,	1,	APF2LD},
	{IDF2LS,	OPF2LS,		WO,	2,	1,	APF2LS},
	{IDF2LA,	OPF2LA,		WO,	2,	1,	APF2LA},
	{IDF2LV,	OPF2LV,		WO,	2,	1,	APF2LV},
	{IDF2L1,	OPF2L1,		WO,	2,	1,	APF2L1},
	{IDF2L2,	OPF2L2,		WO,	2,	1,	APF2L2},
	{IDIMW,		OPIMW,		WO,	4,	0,	APIMWT},
	{IDIMWT,	OPIMWT,		WO,	4,	1,	APIMWT},
	{IDIMWP,	OPIMWP,		WO,	4,	1,	APIMWP},
	{IDIMWA,	OPIMWA,		WO,	4,	1,	APIMWA},
	{IDIMWV,	OPIMWV,		WO,	4,	1,	APIMWV},
	{IDPVC,		OPPVC,		WO,	4,	0,	APPVCM},
	{IDPVCM,	OPPVCM,		WO,	2,	0,	APPVCM},
	{IDPVCR,	OPPVCR,		WO,	2,	0,	APPVCR},
	{IDPVCE,	OPPVCE,		WO,	2,	1,	APPVCE},
	{IDPVCS,	OPPVCS,		WO,	2,	1,	APPVCS},
	{IDRGG,		OPRGG,		WO,	3,	0,	APRGGM},
	{IDRGGM,	OPRGGM,		WO,	2,	0,	APRGGM},
	{IDRGGR,	OPRGGR,		WO,	2,	0,	APRGGR},
	{IDRGGC,	OPRGGC,		WO,	2,	0,	APRGGC},
	{IDSSI,		OPSSI,		WO,	3,	0,	APSSIM},
	{IDSSIM,	OPSSIM,		WO,	2,	0,	APSSIM},
	{IDSSIR,	OPSSIR,		WO,	2,	0,	APSSIR},
	{IDSSIO,	OPSSIO,		WO,	2,	0,	APSSIO},
	{IDVLL,		OPVLL,		WO,	2,	0,	APVLLM},
	{IDVLLM,	OPVLLM,		WO,	2,	1,	APVLLM},
	{IDVLLH,	OPVLLH,		WO,	2,	1,	APVLLH},
	{ID_CR1,	0xFFF,		RW,	8,	1,	0},
	{ID_RR1,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CR2,	0xFFF,		RW,	8,	1,	0},
	{ID_RR2,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CR3,	0xFFF,		RW,	8,	1,	0},
	{ID_RR3,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CR4,	0xFFF,		RW,	8,	1,	0},
	{ID_RR4,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW1,	0xFFF,		RW,	8,	1,	0},
	{ID_RW1,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW2,	0xFFF,		RW,	8,	1,	0},
	{ID_RW2,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW3,	0xFFF,		RW,	8,	1,	0},
	{ID_RW3,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW4,	0xFFF,		RW,	8,	1,	0},
	{ID_RW4,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW5,	0xFFF,		RW,	8,	1,	0},
	{ID_RW5,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW6,	0xFFF,		RW,	8,	1,	0},
	{ID_RW6,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW7,	0xFFF,		RW,	8,	1,	0},
	{ID_RW7,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_CW8,	0xFFF,		RW,	8,	1,	0},
	{ID_RW8,	0xFFF,		RW,	8,	1,	USRRSP},
	{ID_SEND,	0xFFF,		WO,	8,	1,	0},
	};

//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Driver
//

// Instantiator

INSTANTIATE(CQuicksilverDriver);

// Constructor

CQuicksilverDriver::CQuicksilverDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_bWriteErr = 0;

	ClearUserStrings();
	}

// Destructor

CQuicksilverDriver::~CQuicksilverDriver(void)
{
	}

// Configuration

void MCALL CQuicksilverDriver::Load(LPCBYTE pData)
{
	if( GetWord( pData ) == 0x1234 ) {

		m_bGroup = GetByte(pData);

		if( m_bGroup < 200 ) m_bGroup = 200;

		return;
		}
	}
	
void MCALL CQuicksilverDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CQuicksilverDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CQuicksilverDriver::Open(void)
{
	m_pCL = (QUICKSILVERCmdDef FAR * )CL;

	m_dErrorCount = 0;

	m_t4000 = (DWORD)ToTicks(4000);

	m_t1000 = m_t4000 >> 2;
	}

// Device

CCODE MCALL CQuicksilverDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop          = GetByte(pData);

			m_pCtx->m_bActiveDrop    = m_pCtx->m_bDrop;
			m_pCtx->m_bPingCount	 = 0;
			m_pCtx->m_dDropStartTime = GetTickCount();
			m_pCtx->m_dDropInterval  = m_t4000;

			if( m_pCtx->Cache[0] != 0x12345678 ) memset( m_pCtx->Cache, 0, sizeof(m_pCtx->Cache) );

			m_pCtx->Cache[0]      = 0x12345678;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CQuicksilverDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CQuicksilverDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = IDREG;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	CCODE cc = Read(Addr, Data, 1);

	if( cc != 1 ) {

		if( m_pCtx->m_bPingCount++ > 60 ) {

			m_pCtx->m_bActiveDrop = m_pCtx->m_bDrop;

			m_pCtx->m_bPingCount  = 0;
			}
		}

	else m_pCtx->m_bPingCount = 0;

	return cc;
	}

CCODE MCALL CQuicksilverDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( Addr );

	if( m_pQItem == NULL ) return CCODE_NO_DATA | CCODE_NO_RETRY;

	if( NoReadTransmit( pData, uCount ) ) return uCount;

	if( m_pCtx->m_bActiveDrop >= m_bGroup ) { // don't read if addressed in Group
		
		*pData = 0;
		
		return 1;
		}

	if( Read() ) {

		if( Transact() ) {

			if( IsReadOrSendCommand() ) return uCount;

			if( m_bRx[0] == '#' ) {

				if( m_pQItem->fSigned && m_DataResponse > 0x8000 ) {
		
					if( m_pQItem->uSize == 2 ) m_DataResponse |= 0xFFFF0000;
					}

				*pData = m_DataResponse;

				return 1;
				}

			if( m_bRx[0] == '*' && BaseCmd() == OPPOL ) {

				*pData = 0;

				return 1;
				}
			}
		}

	if( IsUserCommand() ) return uCount;

	return CCODE_ERROR;
	}

CCODE MCALL CQuicksilverDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == addrNamed && Addr.a.m_Offset == IDRUN ) {

		CAddress A;
		DWORD Data[1];

		A.a.m_Table   = addrNamed;
		A.a.m_Offset  = IDCPL;
		A.a.m_Extra   = 0;
		A.a.m_Type    = addrLongAsLong;

		UINT Count    = 1;
		Data[0]       = 0;

		if( Write( A, Data, Count ) != 1 ) return FALSE;
		}

	SetpItem( Addr );

	if( m_pQItem == NULL ) return CCODE_NO_DATA | CCODE_NO_RETRY;

//**/	AfxTrace0("\r\n********* WRITE *********");

	if( NoWriteTransmit( pData, uCount ) ) return uCount;

	if( Write( *pData ) ) {

		if( Transact() ) {

			m_bWriteErr = 0;

			return 1;
			}
		}

	if( m_bWriteErr++ > 2 ) {

		m_bWriteErr = 0;

		return uCount;
		}

	return m_bRx[0] == ERRNAK ? 1 : !IsUserCommand() ? CCODE_ERROR : uCount;
	}

// PRIVATE METHODS

// Opcode Handlers

BOOL CQuicksilverDriver::Read(void)
{
	StartFrame();

	if( IsReadOrSendCommand() ) return UserToTx(m_pQItem->uID);

	AddData( DWORD(BaseCmd()), 2, FALSE );

	AddByte(' ');

	switch ( BaseCmd() ) {

		case OPRPB:
		
			AddByte('1');
		
			AddByte(' ');
			
			AddData(DWORD(m_uAddress), 2, FALSE );
			
			AddByte(' ');
			
			break;

		case OPREG:
			
			AddData(DWORD(m_uAddress), (m_uAddress < 100) ? 3 : 4, FALSE  );

			break;
		}

	return TRUE;
	}

UINT CQuicksilverDriver::Write(DWORD dData)
{
	StartFrame();

	if( m_pQItem->uID == ID_SEND ) return UserToTx((2*LOBYTE(dData)) + ID_CW1 - 2);

	UINT uBC  = BaseCmd();

	if( uBC == OPREG ) {

		AddData( DWORD(11), 2, FALSE );
		
		AddByte(' ');
		
		AddData( m_uAddress, m_uAddress < 100 ? 2 : 3, FALSE  );
		
		AddByte(' ');
		
		AddData( dData, 4, TRUE );
		
		AddByte(' ');
		
		return 1;
		}

	AddData( DWORD(uBC), uBC < 10 ? 1 : uBC < 100 ? 2 : 3, FALSE );

	AddByte(' ');

	if( IsCommandOnly() ) return 1;

	switch ( uBC ) {

		case OPCIO:
		case OPCII:
		
			AddData( m_uAddress, 1, FALSE );
			
			AddByte( ' ' );
			
			if( dData & 0x80000000 ) {

				AddByte('-');

				AddByte('1');
				}
			
			else AddByte( dData & 1 ? '1' : '0');
			
			AddByte(' ');
			
			return 1;

		case OPSIF:
			
			AddByte( dData ? '1' : '0' );
			
			AddByte( ' ' );
			
			return 1;

		case OPWRP:
			
			AddData( m_uAddress, 2, FALSE );
			
			AddByte(' ');
			
			AddData( dData, 4, TRUE );
			
			AddByte(' ');

			return 1;

		case OPSTP:
			
			if( dData & 0x80000000 ) {
			
				AddByte( '-' );
				
				AddByte( '1' );
				}
			
			else AddData( dData, 4, FALSE );
			
			AddByte(' ');
			
			return 1;

		default:
			break;
		}

	QUICKSILVERCmdDef * p = m_pQItem;

	if( OneWriteSize() ) {

		AddData( dData, p->uSize, p->fSigned );
		
		AddByte(' ');
		
		return 1;
		}

	else {
		if( p->uID == IDIDT ) {
			
			AddData( (m_pCtx->Cache[APIDTG] << 8) + m_pCtx->Cache[APIDTU], 2, p->fSigned );
			
			AddByte(' ');
			
			return 1;
			}

		UINT uQty = (p->uSize);

		for( UINT i = 1; i <= uQty; i++ ) {

			AddData( m_pCtx->Cache[(p+i)->uArrPos], (p+i)->uSize, (p+i)->fSigned );
			
			AddByte(' ');
			}

		return 1;
		}

	return 0;
	}

// Frame Building

void CQuicksilverDriver::StartFrame(void)
{
	m_uPtr = 0;

	AddByte('@');

	AddByte(' ');

	char sDrop[4];

	SPrintf( sDrop, "%d", m_pCtx->m_bActiveDrop );

	for( UINT i = 0; i < strlen(sDrop); i++ ) AddByte(sDrop[i]);

	AddByte(' ');
	}

void CQuicksilverDriver::EndFrame(void)
{
	AddByte( CR );

	m_bRx[0] = 0;

	m_pData->ClearRx();
	}

void CQuicksilverDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CQuicksilverDriver::AddData(DWORD dData, UINT uCount, BOOL fSigned)
{
	UINT u;

	DWORD dFactor = 1000000000;

	BOOL fFirst = FALSE;

	if( fSigned ) {

		if( !(dData & 0x7FFFFFFF) ) {
		
			AddByte('0');
		
			return;
			}

		if( dData & 0x80000000 ) {
		
			AddByte('-');
		
			dData = -dData;
			}
		}

	switch ( uCount ) {

		case 1:
			dData &= 0xFF;
			break;
		
		case 2:
			dData &= 0xFFFF;
			break;

		case 3:
			dData &= 0xFFFFFF;
			break;
		
		default:
			break;
		}

	while( dFactor ) {

		u = (dData / dFactor) % 10;

		if( u || fFirst ) {

			fFirst = TRUE;

			AddByte( m_pHex[u] );
			}

		dFactor /= 10;
		}

	if( !fFirst ) AddByte( '0' );
	}

// Transport Layer

BOOL CQuicksilverDriver::Transact()
{
	Send();

	if( BaseCmd() == OPRST || BaseCmd() == OPRSP ) {

		while( m_pData->Read(0) < NOTHING );

		return TRUE;
		}

	return GetReply();
	}

void CQuicksilverDriver::Send(void)
{
	EndFrame();

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );
	}

BOOL CQuicksilverDriver::GetReply(void)
{
	UINT uCount = 0;

	UINT uTime  = 1000;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		WORD wData = Get(uTime);

		if( wData == LOWORD(NOTHING) ) {			
		
			Sleep(20);
			
			continue;
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		m_bRx[uCount++] = bData;

		if( bData == CR ) {

			if( IsReadOrSendCommand() ) {

				RxToUser();

				return TRUE;
				}

			return CheckResponse();
			}

		if( uCount > sizeof(m_bRx) ) return FALSE;
		}

	return FALSE;
	}

BOOL CQuicksilverDriver::CheckResponse(void)
{
	UINT uPos = 1;

	if( FindSpace( &uPos ) ) {

		m_AddrResponse = GetValue( &uPos );

		if( m_AddrResponse != m_pCtx->m_bActiveDrop ) return FALSE;
		}

	else return FALSE;

	if( m_bRx[0] == '#' ) { // Read Data Response

		if( FindSpace( &uPos ) ) {

			m_CmndResponse = GetValue( &uPos );

			if( m_CmndResponse != BaseCmd() ) return FALSE;
			}
		
		else return FALSE;

		if( FindSpace( &uPos ) ) {

			m_DataResponse = GetValue( &uPos );
			}
		
		else return FALSE;

		if( FindSpace( &uPos ) ) {

			m_DataResponse = (m_DataResponse<<16) + GetValue( &uPos );
			}

		return TRUE;
		}

	if( m_bRx[0] == '*' ) { // Valid Write Response

		return TRUE;
		}

	if( m_bRx[0] == '!' ) { // Nak

		if( FindSpace(&uPos) ) {

			m_dErrorCount = GetValue(&uPos);

			m_dErrorCount <<= 16;

			FindSpace(&uPos);

			m_dErrorCount += GetValue(&uPos);
			}
		}

	return FALSE;
	}

// Port Access

void CQuicksilverDriver::Put(BYTE b)
{
	m_pData->Write(b, FOREVER);
	}

UINT CQuicksilverDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Find Command Item

void CQuicksilverDriver::SetpItem(AREF Addr)
{
	QUICKSILVERCmdDef * p = m_pCL;

	UINT uID = Addr.a.m_Table;

	if( uID == addrNamed ) {

		uID = Addr.a.m_Offset;

		if( uID >= 5 && uID <= 7 ) uID += 400; // correction

		for( UINT i = 0; i < elements(CL); i++ ) {

			if( uID == p->uID ) {

				m_pQItem    = p;
				m_uCmd     = m_pQItem->uCmd;
				m_uAddress = 0;

				return;
				}

			p++;
			}

		m_pQItem = NULL;

		return;
		}

	if( uID >= USRMIN && uID <= USRMAX ) uID += 400;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == p->uID ) {

			m_pQItem    = p;
			m_uCmd     = m_pQItem->uCmd;
			m_uAddress = Addr.a.m_Offset;

			return;
			}

		p++;
		}

	m_pQItem = NULL;

	return;
	}

// Helpers

BOOL	CQuicksilverDriver::IsCommandOnly(void)
{
	return m_pQItem->uWrite == WC;
	}

BOOL	CQuicksilverDriver::OneWriteSize(void)
{
	switch( m_uCmd ) {

		case	OPSTP:
		case	OPDLY: 
		case	OPCPL:
		case	OPOLP:
		case	OPLRP:
		case	OPADL:
		case	OPSSD:
		case	OPKMR:
		case	OPDIR:
		case	OPMDT:
		case	OPSOB:
		case	OPCOB:
		case	OPPLR:
		case	OPLVT:
		case	OPOVT:
		case	OPMTT:
		case	OPSSL:
		case	OPRSD:
		case	OPAHD:
		case	OPGOC:
		case	OPPCP:
		case	OPSIF:
		case	OPAHM:
		case	OPCLM:
		case	OPLVP:
		case	OPSEF:
		case	OPSPR:
		// Commands Added Oct 06
		case	OPADX:
		case	OPCER:
		case	OPT1F:
		case	OPT2K:
			return TRUE;
		}

	return FALSE;
	}

DWORD	CQuicksilverDriver::GetValue( UINT * pPos )
{
	DWORD dData = 0;

	UINT u = *pPos;

	UINT uChar;

	while ( m_bRx[u] >= '0' ) {

		uChar = m_bRx[u++];

		uChar -= '0';

		if( uChar >= 0x11 && uChar <= 0x16 ) uChar -= 7; // A - F
		
		else if( uChar >= 0x31 && uChar <= 0x36 ) uChar -= 39; // a - f

		if( uChar > 15 ) uChar = 0;

		dData = (dData*16) + uChar;
		}

	*pPos = u;

	return dData;
	}

BOOL	CQuicksilverDriver::FindSpace( UINT * pPos )
{
	UINT u = *pPos;

	while ( m_bRx[u] < '0' ) {

		if( m_bRx[u] == CR ) {
		
			*pPos = u;
			
			return FALSE;
			}
		u++;
		}

	*pPos = u;

	return TRUE;
	}

UINT	CQuicksilverDriver::BaseCmd(void)
{
	return m_uCmd & 0x1FF;
	}

BOOL	CQuicksilverDriver::NoReadTransmit( PDWORD pData, UINT uCount )
{
	if( m_pQItem->uWrite > RW ) {

		if( m_uCmd & CACHED ) *pData = m_pCtx->Cache[m_pQItem->uArrPos];

		else *pData = m_pQItem->uWrite == WO0 ? RTNZEROS : 0;

		return TRUE;
		}

	SelectUserArray(m_pQItem->uID, m_pQItem->uArrPos);

	if( m_pCtx->m_pArray ) {

		GetUserArray(pData, uCount);

		switch( m_pQItem->uID ) {

			case ID_CR1:
			case ID_CR2:
			case ID_CR3:
			case ID_CR4:

				BYTE b = m_pCtx->m_pArray[0] >> 24;

				return b < '1' || b > '9';
			}

		return TRUE;
		}

	switch( m_pQItem->uID ) {

		case IDERR:
			*pData = m_dErrorCount;
			return TRUE;

		case IDPLG:
			*pData = m_bGroup;
			return TRUE;

		case IDPUN:

			CContext * p;
			BYTE bA;
			BYTE bD;

			p  = m_pCtx;

			bA = p->m_bActiveDrop;
			bD = p->m_bDrop;

			if( bA == bD ) {

				*pData = DWORD(bD);

				return TRUE;
				}

			BOOL  fIntvl;
			BYTE  bDrop;
			DWORD dTick;

			fIntvl = p->m_dDropInterval == m_t4000;

			bDrop  = fIntvl ? bA : bD; // assume no change yet

			dTick  = GetTickCount();

			if( dTick > p->m_dDropStartTime + p->m_dDropInterval ) {

				p->m_dDropStartTime = dTick;

				p->m_dDropInterval  = fIntvl ? m_t1000 : m_t4000;

				bDrop = fIntvl ? bD : bA;
				}

			*pData = DWORD(bDrop);

			return TRUE;
		}

	return FALSE;
	}

BOOL CQuicksilverDriver::NoWriteTransmit( PDWORD pData, UINT uCount )
{
	if( !m_pQItem->uWrite ) return TRUE;

	if( m_uCmd == OPERR ) {
		
		m_dErrorCount = 0;

		return TRUE;
		}

	if( m_pQItem->uID == IDPUN ) {

		BYTE b;

		b = LOBYTE(*pData);

		if( b ) {

			m_pCtx->m_bActiveDrop    = b;
			m_pCtx->m_dDropStartTime = GetTickCount();
			m_pCtx->m_dDropInterval  = m_t4000;
			}

		return TRUE;
		}

	DWORD d = *pData;

	if( m_uCmd & CACHED ) {

		if( m_uCmd == OPIDTG && d > 0 && d < m_bGroup ) return TRUE;

		if( m_uCmd == OPIDTU && ( d < 1 || d > 254 ) )  return TRUE;

		m_pCtx->Cache[m_pQItem->uArrPos] = d;

		return TRUE;
		}

	if( (m_pQItem->uID == ID_SEND) && (d < 1 || d > 8 ) ) return TRUE;

	SelectUserArray(m_pQItem->uID, m_pQItem->uArrPos);

	if( m_pCtx->m_pArray ) {

		PutUserArray(pData, uCount);

		return TRUE;
		}

	return FALSE;
	}

void CQuicksilverDriver::SelectUserArray(UINT uID, UINT uArrPos)
{
	PBYTE p = NULL;

	switch( uID ) {

		case ID_CR1:
		case ID_RR1:
			p = UserR1;
			break;

		case ID_CR2:
		case ID_RR2:
			p = UserR2;
			break;

		case ID_CR3:
		case ID_RR3:
			p = UserR3;
			break;

		case ID_CR4:
		case ID_RR4:
			p = UserR4;
			break;

		case ID_CW1:
		case ID_RW1:
			p = UserW1;
			break;

		case ID_CW2:
		case ID_RW2:
			p = UserW2;
			break;

		case ID_CW3:
		case ID_RW3:
			p = UserW3;
			break;

		case ID_CW4:
		case ID_RW4:
			p = UserW4;
			break;

		case ID_CW5:
		case ID_RW5:
			p = UserW5;
			break;

		case ID_CW6:
		case ID_RW6:
			p = UserW6;
			break;

		case ID_CW7:
		case ID_RW7:
			p = UserW7;
			break;

		case ID_CW8:
		case ID_RW8:
			p = UserW8;
			break;
		}

	if( p ) p += uArrPos;

	m_pCtx->m_pArray = (PDWORD)p;
	}

void CQuicksilverDriver::GetUserArray(PDWORD pData, UINT uCount)
{
	PDWORD p = m_pCtx->m_pArray;

	UINT u = min( uCount, USRQTY );

	for( UINT i = 0; i < u; i++, p++ ) {

		pData[i] = *p;
		}
	}

void CQuicksilverDriver::PutUserArray(PDWORD pData, UINT uCount)
{
	PDWORD p = m_pCtx->m_pArray;

	for( UINT i = 0; i < USRQTY; i++, p++ ) {

		if( (!m_pQItem->uArrPos) && (i < uCount) ) *p = pData[i];

		else *p = 0L;
		}
	}

UINT CQuicksilverDriver::UserToTx(UINT uID)
{
	SelectUserArray(uID, 0 );

	PBYTE p = (PBYTE)m_pCtx->m_pArray;

	UINT uLen = FindUserEnd(p);

	if( !uLen ) return 0;

	if( p[0] < '1' || p[0] > '9' ) return 0;

	StartFrame();

	for( UINT i = 0; i < uLen; i++ ) AddByte(p[i]);

	memset(m_bRx, 0, sizeof(m_bRx));

	return 1;
	}

void CQuicksilverDriver::RxToUser(void)
{
	UINT uLen = min(FindUserEnd(m_bRx), USRRSP);

	PBYTE p   = PBYTE(m_pCtx->m_pArray);

	p += USRRSP;

	for( UINT i = 0; i < USRRSP ; i++ ) {

		if( i < uLen ) p[i] = m_bRx[i];

		else p[i] = 0;
		}
	}

UINT CQuicksilverDriver::FindUserEnd(PBYTE pBuff)
{
	for( UINT i = 0; i < (pBuff == m_bRx ? USRRSP : USRCMD); i++ ) {

		if( pBuff[i] == CR || !pBuff[i] ) return i;
		}

	return USRCMD;
	}

void CQuicksilverDriver::ClearUserStrings(void)
{
	memset(UserR1, 0, USRSIZE);
	memset(UserR2, 0, USRSIZE);
	memset(UserR3, 0, USRSIZE);
	memset(UserR4, 0, USRSIZE);
	memset(UserW1, 0, USRSIZE);
	memset(UserW2, 0, USRSIZE);
	memset(UserW3, 0, USRSIZE);
	memset(UserW4, 0, USRSIZE);
	memset(UserW5, 0, USRSIZE);
	memset(UserW6, 0, USRSIZE);
	memset(UserW7, 0, USRSIZE);
	memset(UserW8, 0, USRSIZE);
	}

BOOL CQuicksilverDriver::IsReadOrSendCommand(void)
{
	switch( m_pQItem->uID ) {

		case ID_CR1:
		case ID_CR2:
		case ID_CR3:
		case ID_CR4:
		case ID_SEND:
			   return TRUE;
		}

	return FALSE;
	}

BOOL CQuicksilverDriver::IsUserCommand(void)
{
	switch( m_pQItem->uID ) {

		case ID_CW1:
		case ID_RW1:
		case ID_CW2:
		case ID_RW2:
		case ID_CW3:
		case ID_RW3:
		case ID_CW4:
		case ID_RW4:
		case ID_CW5:
		case ID_RW5:
		case ID_CW6:
		case ID_RW6:
		case ID_CW7:
		case ID_RW7:
		case ID_CW8:
		case ID_RW8:
			return TRUE;
		}

	return IsReadOrSendCommand();
	}

// End of File
