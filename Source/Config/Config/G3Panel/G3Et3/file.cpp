
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

// Constructor

CFileData::CFileData(PCSTR pFile)
{
	m_pFile  = pFile;

	m_pData	 = NULL;

	m_uSize  = 0;

	m_fValid = FALSE;
	}

// IFileData

void CFileData::Release(void)
{
	delete this;
	}

PCSTR CFileData::GetFile(void)
{
	return m_pFile;
	}

BOOL CFileData::IsValid(void)
{
	return m_fValid;
	}

void CFileData::SetValid(BOOL fValid)
{
	m_fValid = fValid;
	}

void CFileData::Create(UINT uSize)
{
	//AfxAssert(m_pData == NULL);

	//m_uSize = uSize;

	//m_pData = New BYTE [ m_uSize ];
	}

PBYTE CFileData::GetDataBuffer(void)
{
	return m_pData;
	}

UINT CFileData::GetDataSize(void)
{
	return m_uSize;
	}

BYTE CFileData::GetByte(UINT &uPtr)
{
	return m_pData[uPtr++];
	}

WORD CFileData::GetWord(UINT &uPtr)
{
	BYTE hi = GetByte(uPtr);
	
	BYTE lo = GetByte(uPtr);

	return MAKEWORD(lo, hi);
	}

DWORD CFileData::GetLong(UINT &uPtr)
{
	WORD hi = GetWord(uPtr);
	
	WORD lo = GetWord(uPtr);

	return MAKELONG(lo, hi);
	}

PCBYTE CFileData::GetData(UINT &uPtr, UINT uSize)
{
	PCBYTE pData = m_pData + uPtr;

	uPtr += uSize;

	return pData;
	}

CString CFileData::GetText(UINT &uPtr, UINT uSize)
{
	uPtr += uSize;

	return CString();
	}

void CFileData::PutByte(UINT &uPtr, BYTE Data)
{
	m_pData[uPtr++] = Data;
	}

void CFileData::PutWord(UINT &uPtr, WORD Data)
{
	PutByte(uPtr, HIBYTE(Data));
	PutByte(uPtr, LOBYTE(Data));
	}

void CFileData::PutLong(UINT &uPtr, DWORD Data)
{
	PutWord(uPtr, HIWORD(Data));
	PutWord(uPtr, LOWORD(Data));
	}

void CFileData::PutData(UINT &uPtr, PCBYTE pData, UINT uSize)
{
	PBYTE pDest = m_pData + uPtr;

	memcpy(pDest, pData, uSize);

	uPtr += uSize;
	}

void CFileData::PutText(UINT &uPtr, CString Data, UINT uSize)
{
	PBYTE pDest = m_pData + uPtr;

	memset(pDest, 0, uSize);

	CAnsiString Name(Data);

	memcpy(pDest, Name, Name.GetLength());

	uPtr += uSize;
	}

void CFileData::PutZero(UINT &uPtr, UINT uSize)
{
	memset(m_pData + uPtr, 0, uSize);

	uPtr += uSize;
	}

// Development

void CFileData::Dump(void)
{
	}

// End of File
