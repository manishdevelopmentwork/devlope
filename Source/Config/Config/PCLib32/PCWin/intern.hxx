
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_DO_YOU_WANT_TO      0x4000
#define IDS_FILE_NAMED_FMT      0x4001
#define IDS_FONT_FIXED          0x4002
#define IDS_FONT_MARLETT        0x4003
#define IDS_FONT_PROP_1         0x4004
#define IDS_FONT_PROP_2         0x4005
#define IDS_SRC_NONE            0x400A

// End of File

#endif
