
#include "Intern.hpp"

#include "UsbHost437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Hardware Drivers
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define RegWrap(x)	(m_pBaseWrap[reg##x])

#define RegCore(x)	(m_pBaseCore[reg##x])

#define RegPhy(x)	(m_pBasePhy [reg##x])

//////////////////////////////////////////////////////////////////////////
//
// AM437 Usb Host Device
//

// Instantiator

IUsbDriver * Create_UsbHost437(UINT iIndex)
{
	CUsbHost437 *p = New CUsbHost437(iIndex);

	return p;
	}

// Constructor

CUsbHost437::CUsbHost437(UINT iIndex) : CUsbBase437(iIndex)
{
	m_pDriver = NULL;

	WaitReady();
	
	ResetWrapper();
	
	ResetCore();
	
	SetMode(modeHost);
	}

// Destructor

CUsbHost437::~CUsbHost437(void)
{
	AfxRelease(m_pDriver);
	}

// IUnknown

HRESULT CUsbHost437::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostHardwareDriver);

	StdQueryInterface(IUsbHostHardwareDriver);

	StdQueryInterface(IUsbDriver);

	return E_NOINTERFACE;
	}

ULONG CUsbHost437::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHost437::Release(void)
{
	StdRelease();
	}

// IUsbDriver

BOOL CUsbHost437::Bind(IUsbEvents *pDriver)
{
	if( !m_pDriver && pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {

			m_pDriver->OnBind(this);
			
			return true;
			}
		}

	return false;
	}

BOOL CUsbHost437::Bind(IUsbDriver *pDriver)
{
	if( !m_pDriver && pDriver ) {

		pDriver->QueryInterface(AfxAeonIID(IUsbHostHardwareEvents), (void **) &m_pDriver);

		if( m_pDriver ) {

			m_pDriver->OnBind((IUsbDriver *)(IUsbHostHardwareDriver *) this);
			
			return true;
			}
		}

	return false;
	}

BOOL CUsbHost437::Init(void)
{
	if( m_pDriver ) {

		m_pDriver->OnInit();

		return true;
		}

	return false;
	}

BOOL CUsbHost437::Start(void)
{
	if( m_pDriver ) {	

		m_pDriver->OnStart();

		EnableEvents();

		EnableCoreInts();

		return true;
		}

	return false;
	}

BOOL CUsbHost437::Stop(void)
{
	if( m_pDriver ) {

		m_pDriver->OnStop();

		DisableCoreInts();

		DisableEvents();

		return true;
		}
	
	return false;
	}
	
// IUsbHostHardwareDriver

DWORD CUsbHost437::GetBaseAddr(void)
{
	return DWORD(m_pBaseCore);
	}

UINT CUsbHost437::GetMemAlign(UINT uType)
{
	return 1;
	}

void CUsbHost437::MemCpy(PVOID d, PCVOID s, UINT n)
{
	ArmMemCpy(d, s, n);
	}

// IEventSink

void CUsbHost437::OnEvent(UINT uLine, UINT uParam)
{
	for(;;) {

		DWORD Data = RegWrap(IRQSTATUSMAIN);

		if( Data ) {

			RegWrap(IRQSTATUSMAIN) = Data;

			if( m_pDriver ) {

				m_pDriver->OnEvent();
				}
			
			continue;
			}

		break;
		}	
	}

void CUsbHost437::ResetCore(void)
{
	RegWrap(FLADJ) |= Bit(31);

	RegWrap(FLADJ) &= ~Bit(31);
	
	WaitReady();
	}

void CUsbHost437::WaitReady(void)
{
	while( RegCore(USBSTS) & Bit(11) );
	}

// End of File
