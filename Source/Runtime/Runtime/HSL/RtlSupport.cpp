
#include "Intern.hpp"

#include "RtlSupport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Printf Control
//

global int _fp_printf = 1;

//////////////////////////////////////////////////////////////////////////
//
// RTL Data
//

clink struct _reent _impure_tls * _thread_impure_ptr;

clink struct _reent             * _global_impure_ptr;

//////////////////////////////////////////////////////////////////////////
//
// RTL Functions
//

clink int    _vfprintf_r(struct _reent *, FILE *, PCTXT, va_list);
clink int    _svfprintf_r(struct _reent *, FILE *, PCTXT, va_list);
clink int    _vfiprintf_r(struct _reent *, FILE *, PCTXT, va_list);
clink int    _svfiprintf_r(struct _reent *, FILE *, PCTXT, va_list);
clink int    __ssvfscanf_r(struct _reent *, FILE *, PCTXT, va_list);
clink int    gettimeofday(struct timeval *pt, void const *tz);
clink int    settimeofday(struct timeval const *pt, void const *tz);
clink time_t getmonosecs(void);
	
//////////////////////////////////////////////////////////////////////////
//
// RTL Support Object
//

// Instantiator

IUnknown * Create_RtlSupport(void)
{
	return New CRtlSupport;
	}

// Thunks

static struct _reent * ThreadImpurePtrThunk(void)
{
	#if !defined(AEON_PLAT_WIN32) && !defined(AEON_PLAT_RLOS)

	AfxAssert(FALSE);

	#endif

	return _thread_impure_ptr;
	}

static struct _reent * GlobalImpurePtrThunk(void)
{
	return _global_impure_ptr;
	}

// Constructor

CRtlSupport::CRtlSupport(void)
{
	StdSetRef();
	}

// Destructor

CRtlSupport::~CRtlSupport(void)
{
	}

// IUnknown

HRESULT CRtlSupport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IRtlSupport);

	StdQueryInterface(IRtlSupport);

	return E_NOINTERFACE;
	}

ULONG CRtlSupport::AddRef(void)
{
	StdAddRef();
	}

ULONG CRtlSupport::Release(void)
{
	StdRelease();
	}

// IRtlSupport

void CRtlSupport::GetThreadImpurePtrThunk(struct _reent *(**pfnFunc)(void))
{
	*pfnFunc = ThreadImpurePtrThunk;
	}

void CRtlSupport::GetGlobalImpurePtrThunk(struct _reent *(**pfnFunc)(void))
{
	*pfnFunc = GlobalImpurePtrThunk;
	}

UINT CRtlSupport::VSNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, va_list pArgs)
{
	// TODO -- We need the uLimit version!!!

	return _fp_printf ? vsprintf(pBuff, pText, pArgs) : vsiprintf(pBuff, pText, pArgs);
	}

int CRtlSupport::VFPrintf(FILE *pFile, PCTXT pText, va_list pArgs)
{
	return _fp_printf ? _vfprintf_r(_REENT, pFile, pText, pArgs) : _vfiprintf_r(_REENT, pFile, pText, pArgs);
	}

int CRtlSupport::SVFPrintf(FILE *pFile, PCTXT pText, va_list pArgs)
{
	return _fp_printf  ? _svfprintf_r(_REENT, pFile, pText, pArgs) : _svfiprintf_r(_REENT, pFile, pText, pArgs);
	}

int CRtlSupport::VFIPrintf(FILE *pFile, PCTXT pText, va_list pArgs)
{
	return _vfiprintf_r(_REENT, pFile, pText, pArgs);
	}

int CRtlSupport::SVFIPrintf(FILE *pFile, PCTXT pText, va_list pArgs)
{
	return _svfiprintf_r(_REENT, pFile, pText, pArgs);
	}

int CRtlSupport::SSVFScanf(FILE *pFile, PCTXT pText, va_list pArgs)
{
	return __ssvfscanf_r(_REENT, pFile, pText, pArgs);
	}

int CRtlSupport::GetTimeOfDay(struct timeval *pt, void const *tz)
{
	return gettimeofday(pt, tz);
	}

int CRtlSupport::SetTimeOfDay(struct timeval const *pt, void const *tz)
{
	return settimeofday(pt, tz);
	}

time_t CRtlSupport::GetMonoSecs(void)
{
	return getmonosecs();
}

// End of File
