
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// System Command Support
//

// Data

var m_page_cmd;
var m_page_update;
var m_page_init;
var m_page_req;

// Code

function pageMain() {

	m_page_cmd = getParam("cmd");

	m_page_init = true;

	m_page_update = false;

	$("#real-time-div").css("display", "");

	$("#real-time-box").change(function () { onClick($(this).prop("checked")); });

	sendCommand();
}

function onClick(check) {

	m_page_update = check;

	if (m_page_update) {

		m_page_init = true;

		sendCommand();
	}
	else {

		if (m_page_req) {

			var req = m_page_req;

			m_page_req = null;

			req.abort();
		}
	}
}

function sendCommand() {

	var url = "/ajax/syscmd-execute.ajax?cmd=" + m_page_cmd;

	url += "&tab=" + m_tab_id;

	url += "&init=" + (m_page_init ? 1 : 0);

	m_page_req = $.get({
		url: makeAjax(url),
		success: onCommandOkay,
		error: onCommandFailed,
		timeout: m_page_init ? 15000 : 0,
		xhrFields: { withCredentials: true },
	});
}

function onCommandOkay(data) {

	var disp = $("#cmd-data");

	var pre1 = disp.find("pre");

	var vert = pre1.length ? pre1[0].scrollTop : 0;

	var horz = pre1.length ? pre1[0].scrollLeft : 0;

	disp.html(data);

	var pre2 = disp.find("pre");

	if (pre2.length) {

		pre2[0].scrollTop = vert;

		pre2[0].scrollLeft = horz;
	}

	if (m_page_update) {

		m_page_init = false;

		setTimeout(sendCommand, 100);
	}

	m_page_req = null;
}

function onCommandFailed(error) {

	if (!check401(error.status)) {

		if (m_page_req) {

			$("#cmd-data").html("<pre>Failed to Load</pre>");

			setTimeout(sendCommand, 5000);
		}

		m_page_init = true;
	}

	m_page_req = null;
}

// End of File
