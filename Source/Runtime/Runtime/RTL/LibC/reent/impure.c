#include <reent.h>

/* Note that there is a copy of this in sys/reent.h.  */
#ifndef __ATTRIBUTE_IMPURE_PTR__
#define __ATTRIBUTE_IMPURE_PTR__
#endif

#ifndef __ATTRIBUTE_IMPURE_DATA__
#define __ATTRIBUTE_IMPURE_DATA__
#endif

/* Redeclare these symbols locally as weak so that the file containing
   their definitions (along with a lot of other stuff) isn't sucked in
   unless they are actually used by other compilation units.  This is
   important to reduce image size for targets with very small amounts
   of memory.  */
#ifdef _REENT_SMALL
extern const struct __sFILE_fake __sf_fake_stdin  _ATTRIBUTE ((weak));
extern const struct __sFILE_fake __sf_fake_stdout _ATTRIBUTE ((weak));
extern const struct __sFILE_fake __sf_fake_stderr _ATTRIBUTE ((weak));
#endif

struct _reent * (*_thread_impure_ptr_thunk)(void) = NULL;
struct _reent * (*_global_impure_ptr_thunk)(void) = NULL;

#if defined(__GNUC__)
struct _reent _impure_tls * __ATTRIBUTE_IMPURE_PTR__ _ATTRIBUTE ((weak))_thread_impure_ptr = NULL;
struct _reent             * __ATTRIBUTE_IMPURE_PTR__ _ATTRIBUTE ((weak))_global_impure_ptr = NULL;
#endif

#if defined(_MSC_VER)
struct _reent _impure_tls * __ATTRIBUTE_IMPURE_PTR__ _weak_thread_impure_ptr = NULL;
struct _reent             * __ATTRIBUTE_IMPURE_PTR__ _weak_global_impure_ptr = NULL;
#pragma comment(linker, "/alternatename:__thread_impure_ptr=__weak_thread_impure_ptr")
#pragma comment(linker, "/alternatename:__global_impure_ptr=__weak_global_impure_ptr")
#endif
