
#include "Intern.hpp"

#include "Gic437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Generalized Interrupt Controller
//

// Register Access

#define RegD(x) (m_pDist[regd##x])

#define RegC(x) (m_pCtrl[regc##x])

// Instantiator

IPic * Create_Gic437(void)
{
	CGic437 *pDevice = New CGic437;

	pDevice->DevInit();

	return pDevice;
	}

// Constructor

CGic437::CGic437(void)
{
	m_pDist  = PVDWORD(ADDR_MPU_DIST);

	m_pCtrl  = PVDWORD(ADDR_MPU_INTC);

	m_uSets  = (RegD(TYPE) & 0x1F) + 1;

	m_uLines = m_uSets * 32;

	m_pSink  = New CSink [ m_uLines ];

	memset(m_pSink, 0, sizeof(CSink) * m_uLines);

	AfxTrace("PIC has %u lines\n", m_uLines);
	}

// Attributes

UINT CGic437::GetLineCount(void) const
{
	return m_uLines;
	}

UINT CGic437::GetPriorityCount(void) const
{
	return 31;
	}

UINT CGic437::GetPriorityMask(void) const
{
	return 31 - (RegC(PRIMASK) >> 3);
	}

UINT CGic437::GetLinePriority(UINT uLine) const
{
	if( uLine < m_uLines ) {

		PVBYTE pPri = PVBYTE(&RegD(PRIORITY));

		return 30 - (pPri[uLine] >> 3);
		}

	return 0;
	}

bool CGic437::IsLineEnabled(UINT uLine) const
{
	if( uLine < m_uLines ) {

		PVDWORD pTest = &RegD(ENABLESET);

		if( pTest[uLine/32] & (1 << (uLine%32)) ) {

			return true;
			}
		}

	return false;
	}

bool CGic437::IsLinePending(UINT uLine) const
{
	if( uLine < m_uLines ) {

		PVDWORD pPend = &RegD(PENDSET);

		if( pPend[uLine/32] & (1 << (uLine%32)) ) {

			return true;
			}
		}

	return false;
	}

// IDeviceDriver

void CGic437::DevInit(void)
{
	RegD(CTRL) = 7;

	DisableAll();

	SetPriorityMask(0);

	RegC(CTRL) = 7;
	}

void CGic437::DevTerm(void)
{
	DisableAll();

	RegD(CTRL) = 0;

	RegC(CTRL) = 0;
	}

// IPic

void CGic437::OnInterrupt(UINT uSource)
{
	while( Dispatch() );
	}

bool CGic437::SetLinePriority(UINT uLine, UINT uPriority)
{
	if( uLine < m_uLines ) {

		if( uPriority < 31 ) {

			PVBYTE pPri = PVBYTE(&RegD(PRIORITY));

			pPri[uLine] = (30 - uPriority) << 3;

			return true;
			}
		}

	return false;
	}

bool CGic437::SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam)
{
	if( uLine < m_uLines ) {

		m_pSink[uLine].m_pSink  = pSink;

		m_pSink[uLine].m_uParam = uParam;

		return true;
		}

	return false;
	}

bool CGic437::EnableLine(UINT uLine, bool fEnable)
{
	if( fEnable ) {

		return EnableLine(uLine);
		}

	return DisableLine(uLine);
	}

bool CGic437::EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable)
{
	if( uLine < m_uLines ) {

		if( !fEnable ) {

			UINT i = GetLinePriority(uLine) ? 1 : 2;

			uSave  = Hal_RaiseIrql(IRQL_HARDWARE + i);

			uSave |= (RegC(PRIMASK) << 8); 

			PVBYTE pPri = PVBYTE(&RegD(PRIORITY));

			if( pPri[uLine] < RegC(PRIMASK) ) {

				RegC(PRIMASK) = pPri[uLine];
				}

			return true;
			}

		RegC(PRIMASK) = (uSave >> 8);
			
		uSave &= 0xFF;

		Hal_LowerIrql(uSave);

		return true;
		}

	return false;
	}

// Operations

bool CGic437::SetPriorityMask(UINT uPriority)
{
	if( uPriority < 32 ) {

		RegC(PRIMASK) = (31 - uPriority) << 3;

		return true;
		}

	return false;
	}

void CGic437::DebugRegister(void)
{
	// TODO -- Bring over debug from iMX51 PIC !!!
	}

void CGic437::DebugRevoke(void)
{
	}

// Implementation

bool CGic437::DisableAll(void)
{
	PVDWORD pSec = &RegD(SECURE);

	PVDWORD pClr = &RegD(ENABLECLR);

	PVDWORD pPri = &RegD(PRIORITY);

	for( UINT uSet = 0; uSet < m_uSets; uSet++ ) {

		pSec[uSet] = uSet == 3 ? 0xFFFFFFFD : 0xFFFFFFFF;

		pClr[uSet] = 0xFFFFFFFF;
		}

	for( UINT n = 0; n < m_uLines / 4; n++ ) {

		pPri[n] = 0xF0F0F0F0;
		}

	return true;
	}

bool CGic437::Dispatch(void)
{
	PVDWORD pPend = &RegC(PENDING);

	PVDWORD pClr  = &RegD(PENDCLR);

	bool    fFind = false;

	for(;;) {
	
		UINT uLine = RegC(PENDING);

		if( unlikely(uLine == intidNoPend) ) {

			break;
			}

		if( likely(uLine < m_uLines) ) {
		
			CSink &Sink = m_pSink[uLine];

			if( likely(Sink.m_pSink) ) {

				Sink.m_pSink->OnEvent(uLine, Sink.m_uParam);
				}

			pClr[uLine/32] = 1 << (uLine%32);
						
			fFind = true;

			continue;
			}

		HostTrap(0x0302);
		}

	return fFind;
	}

bool CGic437::EnableLine(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVDWORD pSet   = &RegD(ENABLESET);

		pSet[uLine/32] = 1 << (uLine%32);

		return true;
		}

	return false;
	}

bool CGic437::DisableLine(UINT uLine)
{
	if( uLine < m_uLines ) {

		PVDWORD pClr   = &RegD(ENABLECLR);

		pClr[uLine/32] = 1 << (uLine%32);

		return true;
		}

	return false;
	}

// End of File
