
#include <atcacert/atcacert_def.h>

const uint8_t DEVICE_CERTIFICATE_TEMPLATE[] = {
    0x30, 0x82, 0x02, 0x12, 0x30, 0x82, 0x01, 0xb8,  0xa0, 0x03, 0x02, 0x01, 0x02, 0x02, 0x10, 0x7d,
    0x5f, 0x63, 0x16, 0xfe, 0x51, 0xd9, 0x9a, 0x60,  0xf7, 0xf6, 0xb1, 0xec, 0xc8, 0xfc, 0xf4, 0x30,
    0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d,  0x04, 0x03, 0x02, 0x30, 0x72, 0x31, 0x0b, 0x30,
    0x09, 0x06, 0x03, 0x55, 0x04, 0x06, 0x13, 0x02,  0x55, 0x53, 0x31, 0x15, 0x30, 0x13, 0x06, 0x03,
    0x55, 0x04, 0x08, 0x0c, 0x0c, 0x50, 0x65, 0x6e,  0x6e, 0x73, 0x79, 0x6c, 0x76, 0x61, 0x6e, 0x69,
    0x61, 0x31, 0x0d, 0x30, 0x0b, 0x06, 0x03, 0x55,  0x04, 0x07, 0x0c, 0x04, 0x59, 0x6f, 0x72, 0x6b,
    0x31, 0x1a, 0x30, 0x18, 0x06, 0x03, 0x55, 0x04,  0x0a, 0x0c, 0x11, 0x52, 0x65, 0x64, 0x20, 0x4c,
    0x69, 0x6f, 0x6e, 0x20, 0x43, 0x6f, 0x6e, 0x74,  0x72, 0x6f, 0x6c, 0x73, 0x31, 0x21, 0x30, 0x1f,
    0x06, 0x03, 0x55, 0x04, 0x03, 0x0c, 0x18, 0x48,  0x61, 0x6c, 0x6f, 0x20, 0x53, 0x69, 0x67, 0x6e,
    0x69, 0x6e, 0x67, 0x20, 0x53, 0x65, 0x72, 0x76,  0x65, 0x72, 0x20, 0x46, 0x46, 0x46, 0x46, 0x30,
    0x20, 0x17, 0x0d, 0x31, 0x39, 0x30, 0x39, 0x31,  0x33, 0x31, 0x38, 0x30, 0x30, 0x30, 0x30, 0x5a,
    0x18, 0x0f, 0x33, 0x30, 0x30, 0x30, 0x31, 0x32,  0x33, 0x31, 0x32, 0x33, 0x35, 0x39, 0x35, 0x39,
    0x5a, 0x30, 0x5e, 0x31, 0x0b, 0x30, 0x09, 0x06,  0x03, 0x55, 0x04, 0x06, 0x13, 0x02, 0x55, 0x53,
    0x31, 0x15, 0x30, 0x13, 0x06, 0x03, 0x55, 0x04,  0x08, 0x0c, 0x0c, 0x50, 0x65, 0x6e, 0x6e, 0x73,
    0x79, 0x6c, 0x76, 0x61, 0x6e, 0x69, 0x61, 0x31,  0x0d, 0x30, 0x0b, 0x06, 0x03, 0x55, 0x04, 0x07,
    0x0c, 0x04, 0x59, 0x6f, 0x72, 0x6b, 0x31, 0x1a,  0x30, 0x18, 0x06, 0x03, 0x55, 0x04, 0x0a, 0x0c,
    0x11, 0x52, 0x65, 0x64, 0x20, 0x4c, 0x69, 0x6f,  0x6e, 0x20, 0x43, 0x6f, 0x6e, 0x74, 0x72, 0x6f,
    0x6c, 0x73, 0x31, 0x0d, 0x30, 0x0b, 0x06, 0x03,  0x55, 0x04, 0x03, 0x0c, 0x04, 0x48, 0x61, 0x6c,
    0x6f, 0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2a,  0x86, 0x48, 0xce, 0x3d, 0x02, 0x01, 0x06, 0x08,
    0x2a, 0x86, 0x48, 0xce, 0x3d, 0x03, 0x01, 0x07,  0x03, 0x42, 0x00, 0x04, 0xe6, 0x89, 0x83, 0x9a,
    0x24, 0x63, 0x37, 0xc5, 0x14, 0x60, 0x6b, 0x7d,  0xf5, 0x96, 0x4c, 0xdf, 0x61, 0x3b, 0x07, 0x64,
    0xde, 0x98, 0x96, 0x37, 0x75, 0x22, 0xaf, 0x30,  0x2c, 0x75, 0x20, 0x15, 0xba, 0x94, 0x95, 0xbf,
    0xe0, 0xc3, 0x9a, 0x11, 0xf7, 0xf0, 0x07, 0x52,  0x72, 0xe5, 0x0b, 0x4d, 0x4f, 0xf1, 0x7c, 0xb8,
    0x84, 0x52, 0x7b, 0xde, 0x4c, 0x95, 0x9a, 0x8d,  0xf7, 0xf4, 0x1f, 0xc6, 0xa3, 0x42, 0x30, 0x40,
    0x30, 0x1f, 0x06, 0x03, 0x55, 0x1d, 0x23, 0x04,  0x18, 0x30, 0x16, 0x80, 0x14, 0x39, 0xfb, 0x99,
    0x1f, 0x87, 0x85, 0x20, 0x62, 0xce, 0x6a, 0xae,  0xd8, 0xb9, 0xbd, 0x39, 0x0e, 0xe6, 0x16, 0xd4,
    0x8b, 0x30, 0x1d, 0x06, 0x03, 0x55, 0x1d, 0x0e,  0x04, 0x16, 0x04, 0x14, 0x15, 0xcc, 0x4e, 0xe2,
    0xc1, 0x52, 0xcb, 0x76, 0x46, 0x1e, 0xe2, 0x95,  0x2b, 0x6b, 0x0c, 0x09, 0xdb, 0x94, 0xeb, 0x0d,
    0x30, 0x0a, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce,  0x3d, 0x04, 0x03, 0x02, 0x03, 0x48, 0x00, 0x30,
    0x45, 0x02, 0x20, 0x09, 0x80, 0xb8, 0x78, 0xc5,  0xac, 0x5f, 0x70, 0x84, 0x87, 0x21, 0xda, 0xae,
    0xff, 0x08, 0xe2, 0xbc, 0x4f, 0xfb, 0x84, 0x08,  0x06, 0xea, 0xe0, 0x1a, 0xfa, 0xad, 0x42, 0xc6,
    0xa0, 0x21, 0x2c, 0x02, 0x21, 0x00, 0xa2, 0x44,  0x39, 0x60, 0xa5, 0xcf, 0xcf, 0x19, 0xce, 0x58,
    0xaa, 0xe7, 0xa7, 0x4d, 0x53, 0xb6, 0xcf, 0x4f,  0x0a, 0xe6, 0x75, 0xe4, 0x49, 0x5b, 0xaf, 0xde,
    0x49, 0x3b, 0xbd, 0xd8, 0x4e, 0x0f
};

const atcacert_def_t DEVICE_CERTIFICATE_DEFINITION = {
    .type                   = CERTTYPE_X509,
    .template_id            = 2,
    .chain_id               = 0,
    .private_key_slot       = 0,
    .sn_source              = SNSRC_PUB_KEY_HASH,
    .cert_sn_dev_loc        = {
        .zone      = DEVZONE_NONE,
        .slot      = 0,
        .is_genkey = 0,
        .offset    = 0,
        .count     = 0
    },
    .issue_date_format      = DATEFMT_RFC5280_UTC,
    .expire_date_format     = DATEFMT_RFC5280_GEN,
    .tbs_cert_loc           = {
        .offset = 4,
        .count  = 444
    },
    .expire_years           = 0,
    .public_key_dev_loc     = {
        .zone      = DEVZONE_DATA,
        .slot      = 0,
        .is_genkey = 1,
        .offset    = 0,
        .count     = 64
    },
    .comp_cert_dev_loc      = {
        .zone      = DEVZONE_DATA,
        .slot      = 10,
        .is_genkey = 0,
        .offset    = 0,
        .count     = 72
    },
    .std_cert_elements      = {
        { // STDCERT_PUBLIC_KEY
            .offset = 316,
            .count  = 64
        },
        { // STDCERT_SIGNATURE
            .offset = 460,
            .count  = 74
        },
        { // STDCERT_ISSUE_DATE
            .offset = 163,
            .count  = 13
        },
        { // STDCERT_EXPIRE_DATE
            .offset = 0,
            .count  = 0
        },
        { // STDCERT_SIGNER_ID
            .offset = 155,
            .count  = 4
        },
        { // STDCERT_CERT_SN
            .offset = 15,
            .count  = 16
        },
        { // STDCERT_AUTH_KEY_ID
            .offset = 397,
            .count  = 20
        },
        { // STDCERT_SUBJ_KEY_ID
            .offset = 428,
            .count  = 20
        }
    },
    .cert_elements          = NULL,
    .cert_elements_count    = 0,
    .cert_template          = DEVICE_CERTIFICATE_TEMPLATE,
    .cert_template_size     = sizeof(DEVICE_CERTIFICATE_TEMPLATE)
};
