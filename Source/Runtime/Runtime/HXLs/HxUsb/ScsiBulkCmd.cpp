
#include "Intern.hpp"

#include "ScsiBulkCmd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Command Wrapper
//

// Constructor

CScsiBulkCmd::CScsiBulkCmd(void)
{
	Init();
	}

// Endianess

void CScsiBulkCmd::HostToScsi(void)
{
	m_dwSig      = HostToIntel(m_dwSig);

	m_dwTag      = HostToIntel(m_dwTag);
	
	m_dwTransfer = HostToIntel(m_dwTransfer);
	}

void CScsiBulkCmd::ScsiToHost(void)
{
	m_dwSig      = IntelToHost(m_dwSig);

	m_dwTag      = IntelToHost(m_dwTag);
	
	m_dwTransfer = IntelToHost(m_dwTransfer);
	}

// Attributes

bool CScsiBulkCmd::IsValid(void) const
{
	return m_dwSig == sigBulkCmd && !m_bReserved1 && !m_bReserved2;
	}

bool CScsiBulkCmd::IsDataIn(void) const
{
	return !!(m_bFlags & 0x80);
	}

bool CScsiBulkCmd::IsDataOut(void) const
{
	return !(m_bFlags & 0x80);
	}
		
bool CScsiBulkCmd::HasData(void) const
{
	return m_dwTransfer != 0;
	}

// Operations

void CScsiBulkCmd::Init(void)
{
	memset(this, 0, sizeof(ScsiBulkCmd));

	m_dwSig = sigBulkCmd;
	}

void CScsiBulkCmd::Init(UINT uCmdLen)
{
	Init();

	m_bCmdLength = uCmdLen;
	}

void CScsiBulkCmd::Init(UINT uCmdLen, UINT uTransfer, bool fDataIn)
{
	Init();

	m_dwTransfer = uTransfer;

	m_bCmdLength = uCmdLen;

	SetDataIn(fDataIn);
	}

void CScsiBulkCmd::SetDataIn(bool fIn)
{
	if( fIn ) {
	
		m_bFlags |=  0x80;
		}
	else {
		m_bFlags &= ~0x80;
		}
	}

// Debug

void CScsiBulkCmd::Dump(void)
{
	#if defined(_XDEBUG)

	AfxTrace("CSCI CBW\n");
		
	AfxTrace("Valid         0x%1.1X\n", IsValid());     
	AfxTrace("m_dwSig       0x%8.8X\n", m_dwSig);
	AfxTrace("m_dwTag       0x%8.8X\n", m_dwTag);
	AfxTrace("m_dwTransfer  0x%8.8X\n", m_dwTransfer);
	AfxTrace("m_bFlags      0x%2.2X\n", m_bFlags);
	AfxTrace("m_bLun        0x%2.2X\n", m_bLun);
	AfxTrace("m_bCmdLength  0x%2.2X\n", m_bCmdLength);
	AfxTrace("CB           ");

	AfxDump(PBYTE(m_bCmd), sizeof(m_bCmd));
	
	#endif
	}

// End of File
