#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "rcxbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha RCX Series Base Driver
//

Cmd CODE_SEG CYamahaRcxMbaseDriver::m_Cmd[] = {

{addrNamed +  1,	"AUTO",		formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  2,	"PROGRAM",	formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  3,	"MANUAL",	formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  4,	"SYSTEM",	formCmd,	respOK,		accW,	radixDec	},

{addrNamed +  5,	"RESET",	formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  6,	"RUN",		formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  7,	"STEP",		formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  8,	"SKIP",		formCmd,	respOK,		accW,	radixDec	},
{addrNamed +  9,	"NEXT",		formCmd,	respOK,		accW,	radixDec	},
{addrNamed + 10,	"STOP",		formCmd,	respOK,		accW,	radixDec	},
//////////////////////////////////////////////////////////////////////////

{	      8,	"DI",		formDevice,	respVal,	accR,	radixOct	},
{	      9,	"DO",		formDevice,	respVal,	accRW,	radixOct	},
{	     10,	"MO",		formDevice,	respVal,	accRW,	radixOct	},
{	     11,	"TO",		formDevice,	respVal,	accRW,	radixOct	},
{	     12,	"LO",		formDevice,	respVal,	accRW,	radixOct	},
{	     13,	"SI",		formDevice,	respVal,	accR,	radixOct	},
{	     14,	"SO",		formDevice,	respVal,	accRW,	radixOct	},  
{	     15,	"SIW",		formDevice,	respVal,	accR,	radixDec	},
{	     16,	"SOW",		formDevice,	respVal,	accRW,	radixDec	},
{	     17,	"SGI",		formVar,	respVal,	accRW,  radixDec	},
{	     18,	"SGR",		formVar,	respVal,	accRW,	radixDec	},

/////////////////////////////////////////////////////////////////////////

{addrNamed + 14,	"BRKPT",	formInternal,	respNone,	accW,	radixDec	},
{addrNamed + 15,	"BRKLN",	formInternal,	respNone,	accW,	radixDec	},
{addrNamed + 16,	"BREAK",	formCmdArg2,	respOK,		accW,	radixDec	},
{addrNamed + 11,	"CHGTSK",	formCmd,	respOK,		accW,	radixDec	},

{addrNamed + 12,	"MSPEED",	formCmdArg1,	respVal,	accRW,	radixDec	},
{addrNamed + 26,	"ASPEED",	formCmdArg1,	respVal,	accRW,  radixDec	},
{addrNamed + 13,	"TEACH",	formCmdArg1,	respOK,		accW,	radixDec	},

{addrNamed + 17,	"EMG",		formStatus,	respVal,	accR,	radixDec	},
{addrNamed + 18,	"LANGUAGE",	formCmdArg1,	respOK,		accW,	radixDec	},
{addrNamed + 19,	"UNIT",		formCmdArg1,	respOK,		accW,	radixDec	},
{addrNamed + 20,	"ACCESS",	formCmdArg1,	respOK,		accW,	radixDec	},
{addrNamed + 21,	"EXELVL",	formCmdArg1,	respOK,		accW,	radixDec	},
{addrNamed + 22,	"SEQUENCE",	formCmdArg1,	respOK,		accW,	radixDec	},
{addrNamed + 23,	"EMGRST",	formCmd,	respOK,		accW,	radixDec	},

{	      2,	"ABSADJ",	formCmdArg2,	respWait,	accW,	radixDec	},
{	      3,	"ABSRESET",	formCmdArg1,	respWait,	accW,	radixDec	},
{addrNamed + 30,	"ABSRST",	formCmd,	respWait,	accW,	radixDec	},
{	      4,	"ORGRTN",	formCmdArg1,	respWait,	accW,	radixDec	},
{	      5,	"INCH",		formCmdArg2,	respOK,		accW,	radixDec	},
{	      6,	"JOG",		formCmdArg2,	respWait,	accW,	radixDec	},
{	     21,	"WHERE",	formStatus,	respMult,	accR,	radixDec	},
{	     22,	"WHRXY",	formStatus,	respMult,	accR,	radixDec	},
{	     23,	"LANGUAGE",	formStatus,	respText,	accR,	radixDec	},
{	     24,	"ACCESS",	formStatus,	respText,	accR,	radixDec	},
{	     25,	"ARM",		formStatus,	respText,	accR,	radixDec	},
{	     26,	"BREAK",	formStatus,	respText,	accR,	radixDec	},
{	     27,	"CONFIG",	formStatus,	respText,	accR,	radixDec	},
{	     28,	"EXELVL",	formStatus,	respText,	accR,	radixDec	},
{	     29,	"MOD",		formStatus,	respText,	accR,	radixDec	},
{	     30,	"ORIGIN",	formStatus,	respText,	accR,	radixDec	},
{	     31,	"ABSRST",	formStatus,	respText,	accR,	radixDec	},
{	     32,	"SERVO",	formStatus,	respText,	accR,	radixDec	},
{	     33,	"SEQUENCE",	formStatus,	respText,	accR,	radixDec	},
{	     34,	"SPEED",	formStatus,	respText,	accR,	radixDec	},
{	     35,	"UNIT",		formStatus,	respText,	accR,	radixDec	},
{	     36,	"VER",		formStatus,	respText,	accR,	radixDec	},
{	     37,	"TASKS",	formStatus,	respText,	accR,	radixDec	},
{	     38,	"TSKMON",	formStatus,	respText,	accR,	radixDec	},
{	     39,	"SHIFT",	formStatus,	respText,	accR,	radixDec	},
{	     40,	"HAND",		formStatus,	respText,	accR,	radixDec	},
{	     41,	"MEM",		formStatus,	respText,	accR,	radixDec	},
{	     42,	"PADDR",	formCmd,	respText,	accR,	radixDec	},
{	     43,	"DRIVEI",	formFunc,	respOK,		accW,	radixDec	}, 		
{	     44,	"PMOVE",	formFunc,	respOK,		accW,	radixDec	},
{	     45,	"SERVO",	formServ,	respOK,		accW,	radixDec	},
{addrNamed + 31,	"SERVO",	formServ,	respOK,		accW,	radixDec	},
{	     46,	"MOVE",		formMove,	respOK,		accW,	radixDec	},
{	     47,	"P",		formStatus,	respSpec,	accR,   radixDec	},
{	     48,	"P",		formVar,	respNone,	accW,	radixDec	},
{	     49,	"P",		formCmd,	respOK,		accW,	radixDec	},
{	     50,	"P",		formStatus,	respSpec,	accR,	radixDec	},
{	     51,	"P",		formVar,	respNone,	accW,	radixDec	},
/////////////////////////////////////////////////////////////////////////
{addrNamed + 24,	"DATE",		formDate,	respSync,	accW,	radixDec	},
{addrNamed + 25,	"TIME",		formTime,	respSync,	accW,	radixDec	},
/////////////////////////////////////////////////////////////////////////
{	      7,	"SWI",		formCmdArg1,	respOK,		accW,	radixDec	},
{	      1,	"ERROR",	formInternal,	respNone,	accW,	radixDec	},
{	     19,	"",		formTerminal,	respTerm,	accW,	radixDec	},
{	     20,	"",		formTerminal,	respTerm,	accR,	radixDec	},
/////////////////////////////////////////////////////////////////////////
{addrNamed + 27,	"LOGIN",	formValidate,	respText,	accRW,	radixDec	},
{addrNamed + 28,	"BYE",		formValidate,	respNone,	accW,	radixDec	},
{addrNamed + 29,	"^C",		formTerminal,	respAbort,	accW,	radixDec	},

	};

// Constructor

CYamahaRcxMbaseDriver::CYamahaRcxMbaseDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_pBase = NULL;

	m_pCmd	= (Cmd FAR *)m_Cmd;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;
	
	m_uRxPtr	= 0;

	m_uTxPtr	= 0;

	m_pExtra	= NULL;
	}

// Destructor

CYamahaRcxMbaseDriver::~CYamahaRcxMbaseDriver(void)
{

	}

// Entry Points

CCODE MCALL CYamahaRcxMbaseDriver::Ping(void)
{
	if( m_pBase->m_fLogOff ) {

		return CCODE_SUCCESS;
		}
	
	DWORD    Data[1];
	
	CAddress Addr;

	Addr.a.m_Table  = 13;
		
	Addr.a.m_Offset = 0;
		
	Addr.a.m_Type   = addrByteAsByte;

	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CYamahaRcxMbaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	FindCmd(FindID(Addr));

	if( m_pItem ) {

		if( m_pBase->m_fLogOff ) {

			if( memcmp(m_pItem->m_Text, PBYTE("LOGIN"), 5) ) {

				for(UINT u = 0; u < uCount; u++ ) {

					pData[u] = 0;
					}
				
				return uCount;
				}
			}

		if( !memcmp(m_pItem->m_Text, PBYTE("LOGIN"), 5) ) { 
			
			MakeMin(uCount, 1);

			pData[0] = !m_pBase->m_fLogOff;

			return uCount;
			}
		
		if( GetInternal(pData, uCount) ) {

			return uCount;
			}

		if( m_pItem->m_Acc == accW ) {

			return uCount;
			}

		if( m_pItem->m_Resp == respTerm ) {

			UINT Count = uCount;

			MakeMin(Count, 80);

			for( UINT u = 0; u < Count - 1; u++ ) {

				pData[u] = m_pBase->m_Term[u];
				}

			return uCount;
			}

		if( !HandleBusy() ) {

			return CCODE_BUSY;
			}

		if( m_pItem->m_Resp != respText ) {

			if( m_pItem->m_Resp != respMult && m_pItem->m_Resp != respSpec ) {

				MakeMin(uCount, 1);
				}

			else if( m_pItem->m_Resp == respSpec ) {

				if( Addr.a.m_Table == 50 ) {

					MakeMin(uCount, 1);
					}
				else {
					MakeMin(uCount, UINT(8 - (Addr.a.m_Offset & 0x7)));
					}
				}
			else {
				MakeMin(uCount, 6);
				}
			}

		Begin();

		Construct(Addr, FALSE);

		End();

		if( Transact() ) {

			if( GetData(pData, uCount, Addr.a.m_Type, Addr.a.m_Offset) ) {

				return uCount;
				}
			} 

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_NO_RETRY; 
	}

CCODE MCALL CYamahaRcxMbaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	FindCmd(FindID(Addr));

	if( m_pItem ) {

		if( m_pBase->m_fLogOff ) {

			if( memcmp(m_pItem->m_Text, PBYTE("LOGIN"), 5) ) {

				for(UINT u = 0; u < uCount; u++ ) {

					pData[u] = 0;
					}
				
				return uCount;
				}

			MakeMin(uCount, 1);
						
			m_pBase->m_fLogOff = FALSE;

			for( UINT uTry = 0; uTry < 3; uTry++ ) {

				if( COMMS_SUCCESS(Ping()) ) {

					return uCount;
					}
				}

			m_pBase->m_fLogOff = TRUE;
				
			return uCount;
			}	

		if( IsAbort() ) {  // Control C support

			if( pData[0] ) {

				Begin();

				AddByte(0x03);

				m_pBase->m_Busy= 0;

				Transact();
				}

			return uCount;
			}

		if( SetInternal(pData[0]) ) {

			return uCount;
			}

		if( !CanWrite(Addr.a.m_Offset) ) {

			return uCount;
			} 

		if( !IsLive(pData) ) {

			return uCount;
			}

		if( !HandleBusy() ) {

			return CCODE_BUSY;
			}

		if( m_pItem->m_Form == formVar && m_pItem->m_Resp == respNone ) {

			BOOL fAxis = Addr.a.m_Table != 51;

			UINT uOffset = fAxis ? FindPointTableOffset(Addr) : Addr.a.m_Offset;
			
			POINTS * pTable = FindTable(uOffset);

			if( pTable ) {

				if( fAxis ) {

					MakeMin(uCount, UINT(8 - (Addr.a.m_Offset & 0x7)));
				      
					UINT u = ((Addr.a.m_Offset & 0x7) % elements(cAxis));
				
					UINT i = 0; 

					while ( u < elements(cAxis) && i < uCount ) {
					
						pTable->m_Point[u] = pData[i];
				
						u++;

						i++;
						}

					return uCount;
					}

				MakeMin(uCount, 1);

				pTable->m_Flag = pData[0] & 0xFF;

				return uCount;
				}

			return CCODE_ERROR;
			}

		if( Addr.a.m_Table == 49 ) {  

			if( pData[0] == 0 ) {

				return uCount;
				}

			if( SendPointTableCmd(Addr) ) {

				return uCount;
				}

			return CCODE_ERROR;
			}

		if( IsMin() ) {

			MakeMin(uCount, 1);
			}

		Begin();

		if( m_pItem->m_Form == formTerminal ) {

			for( UINT u = 0; u < uCount; u++ ) {

				AddByte(pData[u] & 0xFF);
				}
			}
		else {
			Construct(Addr, TRUE);

			AddArgs(Addr, pData, uCount);
			}

		End();

		if( Transact() ) {

			if( SendData(Addr.a.m_Type, pData, uCount) ) {

				return uCount;
				} 
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}
 
// Implementation

void CYamahaRcxMbaseDriver::FindCmd(UINT uID)
{
	m_pItem = m_pCmd;
	
	for ( UINT u = 0; u < elements(m_Cmd); u++ ) {

		if ( m_pItem->m_ID == uID ) {

			return;
			}

		m_pItem++;
		}

	m_pItem = NULL;	
	}

UINT CYamahaRcxMbaseDriver::FindID(AREF Addr)
{
	UINT uID = Addr.a.m_Table;

	if( uID == addrNamed ) {

		uID += Addr.a.m_Offset;
		}

	return uID;
	}

void CYamahaRcxMbaseDriver::Begin(void)
{
	m_uTxPtr = 0;

	if( m_pItem->m_Form == formTerminal ) {

		return;
		}

	if( m_pItem->m_Resp == respAbort ) {

		return;
		}

	if( !memcmp(m_pItem->m_Text, PBYTE("BYE"), 3) ) {
		
		return;
		}

	AddByte('@');
	}

void CYamahaRcxMbaseDriver::End(void)
{
	AddByte(CR);

	AddByte(LF);
	}

void CYamahaRcxMbaseDriver::Construct(AREF Addr, BOOL fWrite)
{
	if( m_pItem->m_Form == formDevice || m_pItem->m_Form == formVar ) {

		if( !fWrite ) {

			AddText("READ ");
			}
		else {
			AddText("WRITE ");
			}
		}

	if( IsStatus() && !fWrite ) {

		AddByte('?');
		}
		
	AddText(m_pItem->m_Text);

	if( Addr.a.m_Table == 47 ) {

		AddValue( FindPointTableOffset(Addr) );

		return;
		}

	if( Addr.a.m_Table == 50 ) {

		AddValue(Addr.a.m_Offset);
		
		return;
		}

	if( IsStatus()			     ||
	    (m_pItem->m_Form <= formCmdArg2) || 
	    (m_pItem->m_Form == formFunc )   || 
	    (m_pItem->m_Form == formServ)    ||
	    (m_pItem->m_Form == formMove)	) {

		if( Addr.a.m_Extra & 0x1 ) {

			AddByte('2');		// Sub Robot
			}

		return;
		}

	if( m_pItem->m_Form == formDevice ) {

		UINT uOffset = Addr.a.m_Offset;

		if( Addr.a.m_Type == addrByteAsByte ) {

			BOOL fSet = FALSE;

			if( uOffset >> 6 ) {

				AddByte(m_pHex[(uOffset >> 6)]);

				uOffset = uOffset & 0x3F;
				
				fSet = TRUE;
				}

			if( fSet || uOffset >> 3 ) {

				AddByte(m_pHex[(uOffset >> 3)]);

				uOffset = uOffset & 0x7;
				}
				
			AddByte(m_pHex[uOffset]);

			AddByte('(');

			AddByte(')');

			return;
			}

		AddByte('(');

		if( uOffset > (m_pItem->m_Radix - 1) ) {

			AddByte(m_pHex[uOffset / m_pItem->m_Radix]);
			}
		else {
			AddByte(' ');
			}

		AddByte(m_pHex[uOffset % m_pItem->m_Radix]);

		AddByte(')');
		}

	if( m_pItem->m_Form == formVar ) {

		AddValue(Addr.a.m_Offset);
		}
	}

void CYamahaRcxMbaseDriver::AddArgs(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pItem->m_Form > formCmd && m_pItem->m_Form <= formCmdArg2 ) {

		AddByte(' ');

		switch( m_pItem->m_ID ) {

			case addrNamed + 16:

				AddValue(m_pBase->m_uBrkPt);

				AddByte(',');

				AddValue(m_pBase->m_uBrkLn);

				return;

			case addrNamed + 12:
			case addrNamed + 13:
			case addrNamed + 18:
			case addrNamed + 19:
			case addrNamed + 20:
			case addrNamed + 21:
			case addrNamed + 22:
			case addrNamed + 26:

				AddValue(pData[0]);

				return;

			case 2:
				AddValue(Addr.a.m_Offset);

				AddByte(',');

				AddByte((Addr.a.m_Extra & 0x2) ? '1' : '0');

				return;

			case 3:
			case 4:
				AddValue(Addr.a.m_Offset);

				return;

			case 5:
			case 6:
				AddByte(cAxis[(Addr.a.m_Offset & 0x7) - 1]);

				AddByte(cDir[(Addr.a.m_Extra & 0x2) >> 1]);

				return;

			case 7:
				AddByte(' ');

				AddByte('<');
				
				for( UINT u = 0; u < uCount; u++ ) {

					AddByte(pData[u] & 0xFF);
					}

				AddByte('>');

				return;
			}
		}

	if( m_pItem->m_Form == formFunc ) {

		AddByte('(');

		AddValue(Addr.a.m_Offset);

		AddByte(',');

		if( Addr.a.m_Type == addrRealAsReal ) {

			AddReal(pData[0]);
			}
		else {
			AddValue(pData[0]);
			}

		AddByte(')');

		return;
		}

	if( m_pItem->m_Form == formServ ) {

		AddByte(' ');

		switch(pData[0]) {

			case 1:	AddText("ON");		break;

			case 2: AddText("OFF");		break;
							
			case 3: AddText("FREE");	break;
			}

		if( m_pItem->m_ID < addrNamed ) {

			AddByte('(');

			AddValue(Addr.a.m_Offset);

			AddByte(')');
			}

		return;
		}

	if( m_pItem->m_Form == formMove ) {

		AddByte(' ');

		if( Addr.a.m_Extra & 0x2 ) {

			AddByte('L');
			}
		else {
			AddByte('P');
			}

		AddByte(',');

		AddByte('P');

		AddValue(pData[0]);

		return;
		}
	}

void CYamahaRcxMbaseDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uTxPtr++] = bByte;
	}

void CYamahaRcxMbaseDriver::AddValue(UINT uValue)
{
	BOOL fSet = FALSE;
	
	for( int u = 1000000000; u > 0; u /= 10  ) {

		UINT uDig = uValue / u;
		
		if( uDig || fSet ) {

			AddByte(m_pHex[uDig % 10]);

			uValue = uValue % u;

			fSet = TRUE;
			}
		}

	if( !fSet ) {

		AddByte('0');
		}
	
	}

void CYamahaRcxMbaseDriver::AddReal(UINT uValue)
{
	char sFloat[12];

	SPrintf(sFloat, "%8.2f", PassFloat(uValue));

	for( UINT u = 0; sFloat[u]; u++ ) {

		AddByte(sFloat[u]);
		}
	}

void CYamahaRcxMbaseDriver::AddText(LPCTXT pText)
{
	for( UINT u = 0; pText[u]; u++ ) {

		AddByte( pText[u] );
		}		 
	}

BOOL CYamahaRcxMbaseDriver::GetData(PDWORD pData, UINT uCount, UINT uType, UINT uOffset)
{
	pData[0] = 0;

	if( m_pItem->m_Form == formDevice ) {

		FindFormat();

		if( m_bRxBuff[m_uRxPtr] == 'B' ) {	// Binary

			m_uRxPtr++;

			for( int b = 7; b > -1; b-- ) {

				pData[0] |= ((m_bRxBuff[m_uRxPtr] & 0x1) << b);

				m_uRxPtr++;
				}
		
			return TRUE;
			}

		if( m_bRxBuff[m_uRxPtr] == 'H' ) {	// Hexadecimal 

			m_uRxPtr++;

			for( int h = 3; h > -1; h-- ) {

				pData[0] |= (FromAscii(m_bRxBuff[m_uRxPtr]) << (h * 4));

				m_uRxPtr++;
				}

			return TRUE;
			}
		}

	if( m_pItem->m_Form == formVar ) {

		if( uType == addrRealAsReal ) {

			GetReal(pData);

			return TRUE;
			}

		GetValue(pData);

		return TRUE;
		}

	if( IsStatus() ) {
		
		if( (m_pItem->m_Resp == respMult) ) {

			return GetMulti(pData, uCount, uType, uOffset);
			}

		if( (m_pItem->m_Resp == respSpec) ) {

			if( m_pItem->m_ID == 50 ) {

				return GetMulti(pData, uCount, uType, 6);
				}

			return GetMulti(pData, uCount, uType, ((uOffset & 0x7) % elements(cAxis)));
			}

		if( (m_pItem->m_Resp == respVal) ) {

			if( uType == addrRealAsReal ) {

				GetReal(pData);

				return TRUE;
				}
			
			GetValue(pData);
				
			return TRUE;
			}
		}

	if( m_pItem->m_Resp == respText ) {

		memset(PBYTE(pData), 0, sizeof(DWORD) * uCount);

		for( UINT u = 0; u < uCount; u++ ) {

			if( u < m_uRxPtr - 1) {

				pData[u] = m_bRxBuff[u];
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::GetMulti(PDWORD pData, UINT uCount, UINT uType, UINT uOffset)
{
	BOOL fSuccess = FALSE;

	UINT uAdd = (m_pItem->m_Resp == respMult) ? 1 : 0;

	for( UINT x = 0, i = 0; x + uAdd < uOffset + uCount;  x++ ) {

		BOOL fFound = FALSE;

		UINT y;

		for( y = 0; y < m_uRxPtr; y++ ) {

			if( fFound ) {

				if( !IsDigit(m_bRxBuff[y]) ) {

					m_bRxBuff[y] = CR;

					break;
					}

				continue;
				}

			if( IsDigit(m_bRxBuff[y]) ) {

				m_uRxPtr -= y;

				memcpy(m_bRxBuff, m_bRxBuff + y, m_uRxPtr);

				y = 0;

				fFound = TRUE;

				continue;
				}
			}

		if( uOffset <= x + uAdd ) {

			if( uType == addrRealAsReal ) {

				GetReal(&pData[i]);
				}
			else {	
				GetValue(&pData[i]);
				}
			
			fSuccess = TRUE;

			i++;
			}
		
		m_uRxPtr -= y;

		memcpy(m_bRxBuff, m_bRxBuff + y, m_uRxPtr);
   		}
	
	return fSuccess;
	}

void CYamahaRcxMbaseDriver::GetValue(PDWORD pData)
{
	BOOL fNeg = FALSE;
	
	for( UINT u = 0; m_bRxBuff[u] != CR; u++ ) {

		BYTE bByte = m_bRxBuff[u];

		if( bByte == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( bByte >= '0' && bByte <= '9' ) {

			pData[0] *= 10;

			pData[0] += (bByte - '0');

			continue;
			}

		break;
		}

	if( fNeg ) {

		pData[0] = -pData[0];
		}
	}

void CYamahaRcxMbaseDriver::GetReal(PDWORD pData)
{
	BOOL fNeg  = FALSE;

	BOOL fDP   = FALSE;

	BOOL fE	   = FALSE;

	BOOL fENeg = FALSE;

	DWORD dwDP = 10;

	DWORD dwE  = 0;

	float Real = 0.0;

	for( UINT u = 0; m_bRxBuff[u] != CR; u++ ) {

		BYTE bByte = m_bRxBuff[u];

		if( !fE && bByte == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( bByte == '.' ) {

			fDP = TRUE;

			continue;
			}

		if( fE && bByte == '-' ) {

			fENeg = TRUE;

			continue;
			}

		if( bByte == 'E' ) {

			fE = TRUE; 

			continue;
			}

		if( bByte >= '0' && bByte <= '9' ) {

			UINT uDig = bByte - '0';

			if( fE ) {

				dwE *= 10;

				dwE += uDig;
				}
			else {	
				float f = (bByte - '0');

				if( fDP ) {

					Real += ( f / dwDP );

					dwDP *= 10;
					}
				else {
			       		Real *= 10.0;

					Real += uDig;
					}
				}

			continue;
			}
		break;
		}

	Real = fNeg ? -Real : +Real;

	if( fE ) {

		for( UINT u = 0; u < dwE; u++ ) {

			if( fENeg ) {

				Real /= 10.0;
				}
			else {
				Real *= 10.0;
				}
			}
		}

	pData[0] = *((DWORD*) &Real);
	}

void CYamahaRcxMbaseDriver::FindFormat(void)
{
	m_uRxPtr = 0;

	while( m_uRxPtr < sizeof(m_bRxBuff) && m_bRxBuff[m_uRxPtr] != '=') {

		m_uRxPtr++;
		}

	m_uRxPtr += 2;
	}

BYTE CYamahaRcxMbaseDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )

		return bByte - '0';

	return	bByte - '@' + 9;
	}

BOOL CYamahaRcxMbaseDriver::SendData(UINT uType, PDWORD pData, UINT uCount)
{
	if( m_pItem->m_Form < formDevice ) {

		return TRUE;
		}

	if( m_pItem->m_Form >= formFunc ) {

		return TRUE;
		}

	if( m_pItem->m_Form == formTerminal ) {

		memcpy(PBYTE(m_pBase->m_Term), m_bRxBuff, m_uRxPtr);

		return TRUE;
		}

	
	m_uTxPtr = 0;

	if( m_pItem->m_Form == formDevice ) {
	
		if( uType == addrByteAsByte ) {

			AddByte('&');

			AddByte('B');
	 
			BYTE bByte = pData[0] & 0xFF;
		
			for( int b = 7; b > -1; b-- ) {

				if( (bByte >> b) & 0x1 ) {

					AddByte('1');
					}
				else {
					AddByte('0');
					}
				}  
			}
		else {
			AddByte('&');

			AddByte('H');

			WORD Hex = pData[0] & 0xFFFF;
		
			for( int h = 3; h > -1; h-- ) {

				UINT uDiv = (0x1 << (h * 4));
				
				AddByte(m_pHex[Hex / uDiv % 0x10]);

				Hex = Hex % uDiv;
				}	
			}
		}

	if( m_pItem->m_Form == formVar ) {

		if( uType == addrLongAsLong ) {

			AddValue(pData[0]);
			}

		if( uType == addrRealAsReal ) {

			char sFloat[10];

			SPrintf(sFloat, "%f", PassFloat(pData[0]));

			for( UINT u = 0; u < 10; u++ ) {

				AddByte(sFloat[u]);
				}
			}
		}

	if( m_pItem->m_Form == formDate ) {

		DWORD dwNow = m_pExtra->GetNow();
		
		UINT uYear  = GetYear(dwNow);

		UINT uMonth = GetMonth(dwNow);

		UINT uDay   = GetDate(dwNow);

		char sDate[8];

		SPrintf(sDate, "%2.2u/%2.2u/%2.2u", uYear % 100, uMonth, uDay);

		for( UINT u = 0; u < 8; u++ ) {

			AddByte(sDate[u]);
			}
		}

	if( m_pItem->m_Form == formTime ) {

		DWORD dwNow = m_pExtra->GetNow();
		
		UINT uHour  = GetHour(dwNow);

		UINT uMin   = GetMin(dwNow);

		UINT uSec   = GetSec(dwNow);

		char sDate[8];

		SPrintf(sDate, "%2.2u:%2.2u:%2.2u", uHour, uMin, uSec);

		for( UINT u = 0; u < 8; u++ ) {

			AddByte(sDate[u]);
			}
		}

	End();

	return Transact();
	}

// Helpers

BOOL CYamahaRcxMbaseDriver::IsLive(PDWORD pData)
{
	if( m_pItem->m_Form <= formCmdArg2 ) {

		if( IsZeroAllowed() ) {

			return TRUE;
			}

		return (pData[0] ? TRUE : FALSE);
		}

	return TRUE;
	}

BOOL CYamahaRcxMbaseDriver::CanWrite(UINT uOffset)
{
	if( m_pItem->m_Acc == accR ) {

		return FALSE;
		}

	switch( m_pItem->m_ID ) {

		case  9:
		case 10:
		case 14:
		case 16:

			if( uOffset < 2 ) {

				return FALSE;
				}
		}

	return TRUE;
	}

BOOL CYamahaRcxMbaseDriver::IsZeroAllowed(void)
{
	if( m_pItem ) {

		switch( m_pItem->m_ID ) {

			case addrNamed + 13:
			case addrNamed + 18:
			case addrNamed + 19:
			case addrNamed + 20:
			case addrNamed + 21:
			case addrNamed + 22:
		
				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::SetInternal(DWORD dwData)
{
	if( m_pItem->m_Form == formInternal ) {

		switch( m_pItem->m_ID ) {

			case 1:			memset(m_pBase->m_Error, ' ', sizeof(m_pBase->m_Error)); break;
			case addrNamed + 14:	m_pBase->m_uBrkPt = dwData;	break;
			case addrNamed + 15:	m_pBase->m_uBrkLn = dwData;	break;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::GetInternal(PDWORD pData, UINT uCount)
{
	if( m_pItem->m_Form == formInternal ) {

		switch( m_pItem->m_ID ) {

			case addrNamed + 14:	pData[0] = m_pBase->m_uBrkPt;	break;
			case addrNamed + 15:	pData[0] = m_pBase->m_uBrkLn;	break;

			case 1:	
				memcpy(PBYTE(pData), PCBYTE(m_pBase->m_Error), uCount * 4);	break;

			}

		return TRUE;
		}

	return FALSE;
	}

void CYamahaRcxMbaseDriver::SetError(void)
{	
	memset(m_pBase->m_Error, 0, sizeof(m_pBase->m_Error));

	if( m_uTxPtr && m_uRxPtr ) {

		for( UINT x = 0; x < 4; x++ ) {

			if( !memcmp(m_bRxBuff + x, PBYTE("login:"), 6) ) {

				return;
				}

			if( !memcmp(m_bRxBuff + x, PBYTE("Pass"), 4) ) {

				return;
				}
			}

		if( m_pBase->m_fInit ) {

			memcpy(m_pBase->m_Error, PCBYTE(m_bRxBuff), m_uRxPtr - 1);

			memcpy(m_pBase->m_Error + m_uRxPtr - 1, PCBYTE(m_bTxBuff), m_uTxPtr - 2);
			}
		}
	}

BOOL CYamahaRcxMbaseDriver::IsMin(void)
{
	if( m_pItem->m_ID == 7 || m_pItem->m_ID == 19 ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CYamahaRcxMbaseDriver::IsDigit(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' ) {

		return TRUE;
		}

	if( bByte == '-' || bByte == '.' ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::IsStatus(void)
{
	if( m_pItem ) {

		if( m_pItem->m_Form == formStatus ) {

			return TRUE;
			}

		switch ( m_pItem->m_ID ) {

			case addrNamed + 12:
			case addrNamed + 26:

				return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::IsAbort(void) 
{
	if( m_pItem ) {

		if( m_pItem->m_Resp == respAbort ) {

			return TRUE;
			}

		}

	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::HandleBusy(void)
{
	if( m_pBase->m_Busy ) {

		if( RecvFrame(FALSE) ) {

			m_pBase->m_Busy = 0;

			if( memcmp(m_bRxBuff, PBYTE("OK"), 2) ) { 

				SetError();
				}
			
			return TRUE;
			}

		return FALSE;
		}
       	
	return TRUE;
	}

// Point Data Support

POINTS * CYamahaRcxMbaseDriver::FindTable(UINT uIndex)
{
	if( m_pBase->m_Table ) {
	
		for( UINT u = 0; u < m_pBase->m_uPoints; u++ ) {

			if( m_pBase->m_Table[u].m_Index == uIndex ) {

				return &m_pBase->m_Table[u];
				}
			}
		}

	// New point table needed !

	m_pBase->m_uPoints++;

	POINTS * pOld = m_pBase->m_Table;

	POINTS * pNew = new POINTS[m_pBase->m_uPoints];
	
	POINTS * pAdd = pNew + m_pBase->m_uPoints - 1;

	memset(pNew, 0, sizeof(POINTS) * m_pBase->m_uPoints);

	if( pOld ) {

		memcpy(pNew, pOld, sizeof(POINTS) * (m_pBase->m_uPoints - 1));

		delete [] pOld;
		} 

	pAdd->m_Index = uIndex;

	m_pBase->m_Table = pNew;
 	
	return &m_pBase->m_Table[m_pBase->m_uPoints - 1];
	}

UINT CYamahaRcxMbaseDriver::FindPointTableOffset(AREF Addr)
{
	return (Addr.a.m_Extra << 12) | (Addr.a.m_Offset >> 3);
	}

BOOL CYamahaRcxMbaseDriver::SendPointTableCmd(AREF Addr)
{
	POINTS * pTable = FindTable(Addr.a.m_Offset);

	if( pTable ) {
	
		Begin();
		
		AddText(m_pItem->m_Text);

		AddValue(pTable->m_Index);

		AddByte('=');

		for( UINT u = 0; u < elements(cAxis); u++ ) {

			AddReal(pTable->m_Point[u]);
			
			AddByte(' ');
			}

		AddValue(pTable->m_Flag);

		End();

		return Transact();
		}

	return FALSE;
	}

// Transport Layer

BOOL CYamahaRcxMbaseDriver::RecvFrame(BOOL fLogin)
{		
	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::Transact(void)
{
	return FALSE;
	}

BOOL CYamahaRcxMbaseDriver::CheckFrame(void)
{
	if( m_pItem->m_Resp == respNone ) {

		if( !memcmp(m_pItem->m_Text, PBYTE("BYE"), 3) ) {

			m_pBase->m_fLogOff = TRUE;
			}
		}
	
	if( m_pItem->m_Resp == respTerm || m_pItem->m_Resp == respText ) { 

		return TRUE;
		}
	
	if( m_pItem->m_Resp == respVal ) {

		if( !memcmp(m_bRxBuff, PBYTE("OK"), 2) ) {

			return TRUE;
			}

		if( !memcmp(m_bRxBuff, m_bTxBuff + 6, m_uTxPtr - 10) ) {

			return TRUE;
			}

		PCTXT pWrite = "*** Please enter !\r";

		if( !memcmp(m_bRxBuff, PCBYTE(pWrite), m_uRxPtr) ) {

			return TRUE;
			}

		if( m_pItem->m_Form == formVar && m_uRxPtr ) {

			return TRUE;
			}

		if( m_pItem->m_Form == formStatus && m_uRxPtr ) {

			return TRUE;
			}
		}

	if( m_pItem->m_Resp == respMult ) {

		if( !memcmp(m_bRxBuff, PBYTE("[POS]"), 5 ) ) {

			return TRUE;
			}
		}

	if( m_pItem->m_Resp == respSpec ) {

		if( m_bRxBuff[0] == ' ' ) {

			return TRUE;
			}
		}

	if( m_pItem->m_Resp == respSync ) {

		if( !memcmp(m_bRxBuff, PBYTE("OK"), 2) ) {

			return TRUE;
			}

		if( !memcmp(m_bRxBuff, PBYTE("current"), 7) ) {

			return TRUE;
			}
		}

	if( m_pItem->m_Resp == respAbort ) {

		if( !memcmp(m_bRxBuff, PBYTE("*** Aborted"), 11) ) {
			
			return TRUE;
			}
		}

	if( !memcmp(m_bRxBuff, PBYTE("OK"), 2) ) { 

		return TRUE;
		}

	SetError();

	return FALSE;
	} 

// End of File
