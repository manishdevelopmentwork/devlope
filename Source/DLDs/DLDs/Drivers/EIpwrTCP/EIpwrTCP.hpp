
//////////////////////////////////////////////////////////////////////////
//
// Data Spaces
//

#define	SP_4	 4	// Modbus Read/Write Words 40000
// The following are Modbus Holding Registers
#define	SP_ACC	10	// Access
#define	SP_COM	11	// Comms
#define	SP_CON	12	// Control
#define	SP_COU	13	// Counter
#define	SP_CUS	14	// CustPage
#define	SP_CST	15	// CustPage Strings
#define	SP_ENE	16	// Energy
#define	SP_EVE	17	// EventLog
#define	SP_EVS	18	// EventLog Status
#define	SP_FAU	19	// Faultdet
#define	SP_FIR	20	// FiringOP
#define	SP_INS	21	// Instrument
#define	SP_IOI	22	// Analog IP IO
#define	SP_IOO	23	// Analog OP IO
#define	SP_IOD	24	// Digital IO
#define	SP_IOR	25	// Relay IO
#define	SP_IPM	26	// IPMonitor
#define	SP_LG2	27	// LGC2
#define	SP_LG8	28	// LGC8
#define	SP_LTC	29	// LTC
#define	SP_MAT	30	// Math2
#define	SP_MOD	31	// Modultr
#define	SP_AAC	32	// Network AlmAck
#define	SP_ADE	33	// Network AlmDet
#define	SP_ADI	34	// Network AlmDis
#define	SP_ALA	35	// Network AlmLat
#define	SP_ASI	36	// Network AlmSig
#define	SP_AST	37	// Network AlmStop
#define	SP_MEA	38	// Network Meas
#define	SP_NSU	39	// Network Setup
#define	SP_PLM	40	// PLM
#define	SP_PLC	41	// PLM Channel
#define	SP_QST	42	// QStart
#define	SP_SET	43	// SetProv
#define	SP_TIM	44	// Timer
#define	SP_TOT	45	// Total
#define	SP_USR	46	// UsrVal

#define	MAXREAD	20
#define	RDONE	0x0FFF

#define	RXGOOD	0
#define	RXRERR	1	// register not found
#define	RXBAD	2	// no response or unintelligible

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm / Invensys EPower Driver
//

class CEIepowerTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEIepowerTCPDriver(void);

		// Destructor
		~CEIepowerTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	m_IP1;
			UINT	m_uPort;
			BYTE	m_bUnit;
			BOOL	m_fKeep;
			UINT	m_uTime1;
			UINT	m_uTime2;
			UINT	m_uTime3;
			WORD	m_wTrans;
			ISocket	*m_pSock;
			UINT	m_uLast;
			BOOL	m_fDirty;

			UINT	uArrCount;
			UINT	uWriteErrCt;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];

		UINT	   m_uPtr;
		UINT	   m_uKeep;

		UINT	   m_uTickCount;

		// Modbus Address Arrays
		UINT	*m_pAddrArr;
		UINT	*m_pPosnArr;
		UINT	*m_pTypeArr;
		UINT	*m_pSortDataArr;
		UINT	*m_pSortPosnArr;
		UINT	*m_pSortTypeArr;
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL Transact(BOOL fIgnore);
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);
		
		// Read Handlers
		CCODE	HandleRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	HandleModbusRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	HandleWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// EPower handling
		CCODE	HandleEPower (AREF Addr, PDWORD pData, UINT uCount, BOOL fIsWrite);
		CCODE	DoEPowerRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoEPowerWrite(AREF Addr, PDWORD pData, UINT uCount);
		UINT	Check4Error  (UINT ccode);
		CCODE	CheckDoneCt  (UINT uDone, UINT uCount);

		// Helpers
		void	SetArrCount(UINT uTable);
		void	SetArrAddr(UINT uTable);
		UINT	Fix32(UINT uOffset, UINT uInc);

		void	ClearArr(void);
		void	MakeArr (UINT uTable);
		void	MakeAddr(void);
		UINT	MakeBlock(UINT uStart, UINT uCount);
		void	DoAddrSort(UINT uCount, UINT * pA, UINT * pP, UINT *pT);
		void	SwapPositions(UINT *pAdd, UINT *pPos, UINT *pTyp, UINT ui, UINT uj);
		void	SwapItem(UINT *p, UINT ui, UINT uj);
				
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File
