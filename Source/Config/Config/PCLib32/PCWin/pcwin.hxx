
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCWIN_HXX
	
#define	INCLUDE_PCWIN_HXX

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <pccore.hxx>

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define	IDS_MSGBOX_ERROR	0x1100
#define	IDS_MSGBOX_INFO		0x1101
#define	IDS_MSGBOX_QUESTION	0x1102
#define	IDS_MSGBOX_LAST		0x1103

//////////////////////////////////////////////////////////////////////////
//
// Standard Menu Commands
//

#define	IDM_FILE		0x81
#define	IDM_FILE_TITLE		0x8100
#define	IDM_FILE_NEW		0x8101
#define	IDM_FILE_OPEN		0x8102
#define	IDM_FILE_CLOSE		0x8103
#define	IDM_FILE_SAVE		0x8104
#define	IDM_FILE_SAVE_AS	0x8105
#define	IDM_FILE_PRINT		0x8106
#define	IDM_FILE_PRINT_SETUP	0x8107
#define	IDM_FILE_PROPERTIES	0x8108
#define	IDM_FILE_EXIT		0x8109
#define	IDM_FILE_SYMBOLS	0x810A
#define	IDM_FILE_RECENT		0x8180

#define	IDM_EDIT		0x82
#define	IDM_EDIT_TITLE		0x8200
#define	IDM_EDIT_UNDO		0x8201
#define	IDM_EDIT_REDO		0x8202
#define	IDM_EDIT_CUT		0x8203
#define	IDM_EDIT_COPY		0x8204
#define	IDM_EDIT_PASTE		0x8205
#define	IDM_EDIT_DELETE		0x8206
#define	IDM_EDIT_CLEAR		0x8207
#define	IDM_EDIT_SELECT_ALL	0x8208
#define	IDM_EDIT_INSERT		0x8209
#define	IDM_EDIT_PROPERTIES	0x820A
#define	IDM_EDIT_FILL_DOWN	0x820B
#define	IDM_EDIT_DUPLICATE	0x820C
#define	IDM_EDIT_SMART_DUP	0x820D
#define	IDM_EDIT_FIND		0x820E
#define	IDM_EDIT_FIND_NEXT	0x820F
#define	IDM_EDIT_FIND_PREV	0x8210
#define	IDM_EDIT_FIND_ALL	0x8211
#define	IDM_EDIT_FIND_ALL_NEXT	0x8212
#define	IDM_EDIT_FIND_ALL_PREV	0x8213
#define	IDM_EDIT_REPLACE	0x8214
#define	IDM_EDIT_EXPAND		0x8215
#define	IDM_EDIT_BACK		0x8216
#define	IDM_EDIT_PASTE_SPECIAL	0x8217

#define	IDM_VIEW		0x83
#define	IDM_VIEW_MENU_TITLE	0x8300
#define	IDM_VIEW_TOOLBAR	0x8301
#define	IDM_VIEW_GHOSTBAR	0x8302
#define	IDM_VIEW_STATUS		0x8303
#define	IDM_VIEW_FIND_RESULTS	0x8304
#define	IDM_VIEW_OPTIONS	0x8305
#define	IDM_VIEW_REFRESH	0x8306
#define	IDM_VIEW_SORT_POS	0x8307
#define	IDM_VIEW_SORT_NEG	0x8308
#define	IDM_VIEW_SORT_CLEAR	0x8309
#define	IDM_VIEW_WATCH		0x830A
#define	IDM_VIEW_CYCLE_LANG	0x830B
#define	IDM_VIEW_OUTPUT		0x830C

#define	IDM_WINDOW		0x84
#define	IDM_WINDOW_MENU_TITLE	0x8400
#define	IDM_WINDOW_CASCADE	0x8401
#define	IDM_WINDOW_TILE		0x8402
#define	IDM_WINDOW_ARRANGE	0x8403
#define	IDM_WINDOW_DUPLICATE	0x8404
#define	IDM_WINDOW_CLOSE_ALL	0x8405
#define	IDM_WINDOW_CLOSE	0x8406
#define	IDM_WINDOW_NEXT_PANE	0x8407
#define IDM_WINDOW_PREV_PANE	0x8408
#define	IDM_WINDOW_ADJUST	0x8409
#define	IDM_WINDOW_HIDE_UPPER	0x840A
#define	IDM_WINDOW_HIDE_LOWER	0x840B
#define	IDM_WINDOW_CHILD	0x8480
#define	IDM_WINDOW_MORE		0x8489

// End of File

#endif
