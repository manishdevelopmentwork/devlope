
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Textor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Block Menu

BOOL CPageEditorWnd::OnBlockGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{ IDM_BLOCK_ALIGN_HL,   MAKELONG(0x0002, 0x3001),
		IDM_BLOCK_ALIGN_HC,   MAKELONG(0x0000, 0x3001),
		IDM_BLOCK_ALIGN_HR,   MAKELONG(0x0003, 0x3001),
		IDM_BLOCK_ALIGN_VT,   MAKELONG(0x0009, 0x3001),
		IDM_BLOCK_ALIGN_VU,   MAKELONG(0x000A, 0x3001),
		IDM_BLOCK_ALIGN_VM,   MAKELONG(0x000B, 0x3001),
		IDM_BLOCK_ALIGN_VL,   MAKELONG(0x000C, 0x3001),
		IDM_BLOCK_ALIGN_VB,   MAKELONG(0x000D, 0x3001),
		IDM_BLOCK_LARGER,     MAKELONG(0x0014, 0x3001),
		IDM_BLOCK_SMALLER,    MAKELONG(0x0015, 0x3001),
		IDM_BLOCK_BOLD,       MAKELONG(0x0012, 0x3001),
		IDM_BLOCK_UNDERLINE,  MAKELONG(0x0013, 0x3001),
		IDM_BLOCK_INC_MARG_H, MAKELONG(0x0016, 0x3001),
		IDM_BLOCK_DEC_MARG_H, MAKELONG(0x0017, 0x3001),
		IDM_BLOCK_INC_MARG_V, MAKELONG(0x0018, 0x3001),
		IDM_BLOCK_DEC_MARG_V, MAKELONG(0x0019, 0x3001),

	};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
		}
	}

	return FALSE;
}

BOOL CPageEditorWnd::OnBlockControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
	}

	CPrimWithText *pHost  = NULL;

	CPrimBlock    *pBlock = NULL;

	if( !ArePropsLocked() ) {

		// LATER -- Handle more than one item at once.

		if( HasLoneSelect() ) {

			CPrim *pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				pHost = (CPrimWithText *) pPrim;

				if( pHost->HasText() ) {

					pBlock = pHost->m_pTextItem;
				}

				if( pHost->HasData() ) {

					pBlock = pHost->m_pDataItem;
				}
			}
		}
	}

	switch( uID ) {

		case IDM_BLOCK_ALIGN_HL:
		case IDM_BLOCK_ALIGN_HC:
		case IDM_BLOCK_ALIGN_HR:

			if( pBlock ) {

				if( pBlock->IsKindOf(AfxRuntimeClass(CPrimData)) ) {

					CPrimData *pData = (CPrimData *) pBlock;

					if( pData->m_Content == 1 ) {

						Src.EnableItem(FALSE);

						return TRUE;
					}
				}

				Src.EnableItem(TRUE);

				Src.CheckItem(pBlock->m_AlignH == int(uID - IDM_BLOCK_ALIGN_HL));
			}

			break;

		case IDM_BLOCK_ALIGN_VT:
		case IDM_BLOCK_ALIGN_VU:
		case IDM_BLOCK_ALIGN_VM:
		case IDM_BLOCK_ALIGN_VL:
		case IDM_BLOCK_ALIGN_VB:

			if( pBlock ) {

				Src.EnableItem(TRUE);

				Src.CheckItem(pBlock->m_AlignV == int(uID - IDM_BLOCK_ALIGN_VT));
			}

			break;

		case IDM_BLOCK_LARGER:

			Src.EnableItem(pBlock && m_pFonts->CanMakeLarger(pBlock->m_Font));

			break;

		case IDM_BLOCK_SMALLER:

			Src.EnableItem(pBlock && m_pFonts->CanMakeSmaller(pBlock->m_Font));

			break;

		case IDM_BLOCK_BOLD:

			if( pBlock ) {

				Src.EnableItem(m_pFonts->CanFlipBold(pBlock->m_Font));

				Src.CheckItem(m_pFonts->IsBold(pBlock->m_Font));
			}

			break;

		case IDM_BLOCK_INC_MARG_H:

			if( pBlock ) {

				BOOL fx1 = pBlock->m_Margin.left  < 80;
				BOOL fx2 = pBlock->m_Margin.right < 80;

				Src.EnableItem(fx1 || fx2);
			}

			break;

		case IDM_BLOCK_DEC_MARG_H:

			if( pBlock ) {

				BOOL fx1 = pBlock->m_Margin.left  > 0;
				BOOL fx2 = pBlock->m_Margin.right > 0;

				Src.EnableItem(fx1 || fx2);
			}

			break;

		case IDM_BLOCK_INC_MARG_V:

			if( pBlock ) {

				BOOL fy1 = pBlock->m_Margin.top    < 80;
				BOOL fy2 = pBlock->m_Margin.bottom < 80;

				Src.EnableItem(fy1 || fy2);
			}

			break;

		case IDM_BLOCK_DEC_MARG_V:

			if( pBlock ) {

				BOOL fy1 = pBlock->m_Margin.top    > 0;
				BOOL fy2 = pBlock->m_Margin.bottom > 0;

				Src.EnableItem(fy1 || fy2);
			}

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CPageEditorWnd::OnBlockCommand(UINT uID)
{
	if( m_uMode == modeText ) {

		return FALSE;
	}

	CPrimBlock *pBlock = NULL;

	CPrim      *pPrim  = NULL;

	if( !ArePropsLocked() ) {

		// LATER -- Handle modification of more than one item.

		if( HasLoneSelect() ) {

			pPrim = GetLoneSelect();

			if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

				CPrimWithText *pHost = (CPrimWithText *) pPrim;

				if( pHost->HasText() ) {

					pBlock = pHost->m_pTextItem;
				}

				if( pHost->HasData() ) {

					pBlock = pHost->m_pDataItem;
				}
			}
		}
	}

	if( pBlock ) {

		switch( uID ) {

			case IDM_BLOCK_ALIGN_HL:
			case IDM_BLOCK_ALIGN_HC:
			case IDM_BLOCK_ALIGN_HR:

				if( pBlock->m_AlignH != int(uID - IDM_BLOCK_ALIGN_HL) ) {

					MakeBlockCmd(pPrim, CString(IDS_CHANGE_TEXT_1));

					pBlock->m_AlignH = int(uID - IDM_BLOCK_ALIGN_HL);

					SaveBlockCmd();
				}

				break;

			case IDM_BLOCK_ALIGN_VT:
			case IDM_BLOCK_ALIGN_VU:
			case IDM_BLOCK_ALIGN_VM:
			case IDM_BLOCK_ALIGN_VL:
			case IDM_BLOCK_ALIGN_VB:

				if( pBlock->m_AlignV != int(uID - IDM_BLOCK_ALIGN_VT) ) {

					MakeBlockCmd(pPrim, CString(IDS_CHANGE_TEXT_1));

					pBlock->m_AlignV = int(uID - IDM_BLOCK_ALIGN_VT);

					SaveBlockCmd();
				}

				break;

			case IDM_BLOCK_LARGER:

				// LATER -- Make item larger to hold text?

				MakeBlockCmd(pPrim, CString(IDS_MAKE_TEXT_LARGER));

				pBlock->m_Font = m_pFonts->GetLarger(pBlock->m_Font);

				SaveBlockCmd();

				ShowNewFont(pBlock->m_Font);

				break;

			case IDM_BLOCK_SMALLER:

				MakeBlockCmd(pPrim, CString(IDS_MAKE_TEXT_SMALLER));

				pBlock->m_Font = m_pFonts->GetSmaller(pBlock->m_Font);

				SaveBlockCmd();

				ShowNewFont(pBlock->m_Font);

				break;

			case IDM_BLOCK_BOLD:

				MakeBlockCmd(pPrim, CString(IDS_CHANGE_TEXT));

				pBlock->m_Font = m_pFonts->FlipBold(pBlock->m_Font);

				SaveBlockCmd();

				ShowNewFont(pBlock->m_Font);

				break;

			case IDM_BLOCK_INC_MARG_H:

				MakeBlockCmd(pPrim, CString(IDS_INCREASE));

				pBlock->m_Margin.left  = min(pBlock->m_Margin.left  +1, 80);
				pBlock->m_Margin.right = min(pBlock->m_Margin.right +1, 80);

				SaveBlockCmd();

				break;

			case IDM_BLOCK_DEC_MARG_H:

				MakeBlockCmd(pPrim, CString(IDS_DECREASE));

				pBlock->m_Margin.left  = max(pBlock->m_Margin.left  -1, 0);
				pBlock->m_Margin.right = max(pBlock->m_Margin.right -1, 0);

				SaveBlockCmd();

				break;

			case IDM_BLOCK_INC_MARG_V:

				MakeBlockCmd(pPrim, CString(IDS_INCREASE_VERTICAL));

				pBlock->m_Margin.top    = min(pBlock->m_Margin.top   +1, 80);
				pBlock->m_Margin.bottom = min(pBlock->m_Margin.bottom+1, 80);

				SaveBlockCmd();

				break;

			case IDM_BLOCK_DEC_MARG_V:

				MakeBlockCmd(pPrim, CString(IDS_DECREASE_VERTICAL));

				pBlock->m_Margin.top    = max(pBlock->m_Margin.top   -1, 0);
				pBlock->m_Margin.bottom = max(pBlock->m_Margin.bottom-1, 0);

				SaveBlockCmd();

				break;

			default:
				return FALSE;
		}
	}

	return TRUE;
}

void CPageEditorWnd::MakeBlockCmd(CPrim *pPrim, CString Menu)
{
	m_pTextPrim = (CPrimWithText *) pPrim;

	m_hTextPrev = pPrim->TakeSnapshot();

	m_TextMenu  = Menu;
}

void CPageEditorWnd::KillBlockCmd(void)
{
	GlobalFree(m_hTextPrev);
}

BOOL CPageEditorWnd::SaveBlockCmd(void)
{
	CCmdItem *pCmd = New CCmdItem(m_TextMenu,
				      m_pTextPrim,
				      m_hTextPrev
	);

	if( pCmd->IsNull() ) {

		delete pCmd;

		return FALSE;
	}

	if( m_pTextPrim->m_pTextItem ) {

		m_pTextPrim->m_pTextItem->TextDirty();
	}

	m_pTextPrim->SetDirty();

	LocalSaveCmd(pCmd);

	UpdateImage();

	Invalidate(FALSE);

	return TRUE;
}

void CPageEditorWnd::ShowNewFont(UINT uFont)
{
	CString Name = m_pFonts->GetName(uFont);

	afxThread->SetStatusText(CFormat(CString(IDS_NEW_FONT_IS_FMT), Name));
}

// End of File
