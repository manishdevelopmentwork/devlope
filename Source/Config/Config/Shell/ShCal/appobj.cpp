
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Runtime Class

AfxImplementRuntimeClass(CCrimsonCalApp, CThread);

// Constructor

CCrimsonCalApp::CCrimsonCalApp(void)
{
	
	}

// Destructor

CCrimsonCalApp::~CCrimsonCalApp(void)
{
	}

// Overridables

BOOL CCrimsonCalApp::OnInitialize(void)
{
	CWnd *pWnd = New CWnd;

	pWnd->Create( CString(IDS_APP_CAPTION),
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      CRect(),
		      AfxNull(CWnd),
		      AfxNull(CMenu),
		      NULL
		      );

	Link_GetCalibrateDlg(pWnd, TRUE);
	
	pWnd->SendMessage(WM_CLOSE);

	return TRUE;
	}

void CCrimsonCalApp::OnTerminate(void)
{
	delete this;
	}

// End of File
