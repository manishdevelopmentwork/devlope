
#include "Intern.hpp"

#include "MqttJsonData.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MQTT JSON Data
//
// This class exists as the CJsonData implementation does not support a 
// node containing a '.' in the name as it breaks it into a child.  MQTT
// support requires the dot notation to support Folder.Tag names.
//

// Static Data

TCHAR const CMqttJsonData::m_cEsc[] = T("\\\"\'\b\f\n\r\t");

TCHAR const CMqttJsonData::m_cRep[] = T("\\\"\'bfnrt");

// Constructor

CMqttJsonData::CMqttJsonData(void)
{
	m_fList  = FALSE;

	m_uIndex = 0;
}

CMqttJsonData::CMqttJsonData(BOOL fList)
{
	m_fList  = fList;

	m_uIndex = 0;
}

// Destructor

CMqttJsonData::~CMqttJsonData(void)
{
	for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

		CJsonPair const &Pair = m_Tree[n];

		if( Pair.m_pSub ) {

			delete Pair.m_pSub;
		}
	}
}

// Operations

void CMqttJsonData::Empty(void)
{
	if( !m_Tree.IsEmpty() ) {

		for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

			CJsonPair const &Pair = m_Tree[n];

			if( Pair.m_pSub ) {

				delete Pair.m_pSub;
			}
		}

		m_Tree.Empty();
	}

	m_uIndex = 0;
}

BOOL CMqttJsonData::Parse(PCTXT pText)
{
	Empty();

	if( ParseBlock(pText) ) {

		if( *pText ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void CMqttJsonData::AddObject(CString Name)
{
	m_Tree.Insert(CJsonPair(Name, New CMqttJsonData));
}

void CMqttJsonData::AddChild(CString Name, BOOL fList, CMqttJsonData * &pSub)
{
	m_Tree.Insert(CJsonPair(Name, pSub = New CMqttJsonData(fList)));
}

void CMqttJsonData::AddObject(CString Name, CMqttJsonData * &pSub)
{
	AddChild(Name, FALSE, pSub);
}

void CMqttJsonData::AddList(CString Name, CMqttJsonData * &pSub)
{
	AddChild(Name, TRUE, pSub);
}

void CMqttJsonData::AddValue(CString Name, CString Value, UINT Type)
{
	m_Tree.Remove(Name);

	m_Tree.Insert(CJsonPair(Name, Value, Type));
}

void CMqttJsonData::AddValue(CString Name, CString Value)
{
	m_Tree.Remove(Name);

	m_Tree.Insert(CJsonPair(Name, Value));
}

void CMqttJsonData::AddNull(CString Name)
{
	m_Tree.Insert(CJsonPair(Name, T("null"), jsonNull));
}

void CMqttJsonData::AddChild(BOOL fList, CMqttJsonData * &pSub)
{
	m_Tree.Insert(CJsonPair(CreateName(m_uIndex++), pSub = New CMqttJsonData(fList)));
}

void CMqttJsonData::AddObject(CMqttJsonData * &pSub)
{
	AddChild(FALSE, pSub);
}

void CMqttJsonData::AddList(CMqttJsonData * &pSub)
{
	AddChild(TRUE, pSub);
}

void CMqttJsonData::AddMember(CString Value)
{
	m_Tree.Insert(CJsonPair(CreateName(m_uIndex++), Value));
}

BOOL CMqttJsonData::Delete(CString Name)
{
	INDEX n = m_Tree.Find(Name);

	if( !m_Tree.Failed(n) ) {

		CJsonPair const &Pair = m_Tree[n];

		if( Pair.m_pSub ) {

			delete Pair.m_pSub;
		}

		m_Tree.Remove(n);

		return TRUE;
	}
	
	return FALSE;
}

BOOL CMqttJsonData::Delete(UINT uIndex)
{
	return Delete(CreateName(uIndex));
}

void CMqttJsonData::SetValue(INDEX Index, CString Value)
{
	((CJsonPair &) m_Tree[Index]).m_Value = Value;
}

// Attributes

CString CMqttJsonData::GetAsText(BOOL fPretty) const
{
	CString t;

	UINT uLevel = 0;

	if( fPretty ) {

		t += CString(' ', 2 * uLevel);
	}

	if( TRUE ) {

		t += GetAsText(fPretty, uLevel);
	}

	if( fPretty ) {

		t += "\r\n";
	}

	return t;
}

BOOL CMqttJsonData::GetNames(CStringArray &Names) const
{
	for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

		CJsonPair const &Pair = m_Tree[n];

		Names.Append(Pair.m_Name);
	}

	return !Names.IsEmpty();
}

BOOL CMqttJsonData::HasName(CString Name) const
{
	if( m_Tree.Failed(m_Tree.Find(Name)) ) {

		return FALSE;
	}

	return TRUE;
}

BOOL CMqttJsonData::HasName(UINT uIndex) const
{
	if( m_Tree.Failed(m_Tree.Find(CreateName(uIndex))) ) {

		return FALSE;
	}

	return TRUE;
}

UINT CMqttJsonData::GetType(CString Name) const
{
	INDEX n = m_Tree.Find(Name);

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Type;
	}

	return jsonAuto;
}

UINT CMqttJsonData::GetType(UINT uIndex) const
{
	INDEX n = m_Tree.Find(CreateName(uIndex));

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Type;
	}

	return jsonAuto;
}

CString CMqttJsonData::GetValue(CString Name) const
{
	return GetValue(Name, CString());
}

CString CMqttJsonData::GetValue(UINT uIndex) const
{
	return GetValue(uIndex, CString());
}

CString CMqttJsonData::GetValue(CString Name, CString Default) const
{
	INDEX n = m_Tree.Find(Name);

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Value;
	}
	
	return Default;
}

CString CMqttJsonData::GetValue(UINT uIndex, CString Default) const
{
	INDEX n = m_Tree.Find(CreateName(uIndex));

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_Value;
	}

	return Default;
}

CMqttJsonData * CMqttJsonData::GetChild(CString Name) const
{
	INDEX n = m_Tree.Find(Name);

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_pSub;
	}

	return NULL;
}

CMqttJsonData * CMqttJsonData::GetChild(UINT uIndex) const
{
	INDEX n = m_Tree.Find(CreateName(uIndex));

	if( !m_Tree.Failed(n) ) {

		return m_Tree[n].m_pSub;
	}

	return NULL;
}

CString CMqttJsonData::GetPathValue(PCTXT pPath) const
{
	PCTXT pFind = strchr(pPath, '.');

	if( pFind ) {

		CMqttJsonData *pChild;

		if( isdigit(pPath[0]) ) {

			pChild = GetChild(atoi(pPath));
		}
		else
			pChild = GetChild(CString(pPath, pFind - pPath));

		if( pChild ) {

			return pChild->GetPathValue(pFind + 1);
		}

		return CString();
	}

	return GetValue(pPath);
}

CMqttJsonData * CMqttJsonData::GetPathChild(PCTXT pPath) const
{
	PCTXT pFind = strchr(pPath, '.');

	if( pFind ) {

		CMqttJsonData *pChild;

		if( isdigit(pPath[0]) ) {

			pChild = GetChild(atoi(pPath));
		}
		else
			pChild = GetChild(CString(pPath, pFind - pPath));

		if( pChild ) {

			return pChild->GetPathChild(pFind + 1);
		}

		return NULL;
	}

	if( isdigit(pPath[0]) ) {

		return GetChild(atoi(pPath));
	}

	return GetChild(pPath);
}

// Implementation

BOOL CMqttJsonData::ParseBlock(PCTXT &pText)
{
	UINT    uState = 0;

	CString Name, Data;

	while( *pText ) {

		TCHAR c = *pText++;

		if( isspace(c) ) {

			if( uState != 2 && uState != 6 && uState != 7 && uState != 8 ) {

				continue;
			}
		}

		switch( uState ) {

			case 0: // Look for opening brace or opening bracket.

				if( c == (m_fList ? '[' : '{') ) {

					if( m_fList ) {

						Name   = CreateName(m_uIndex++);

						uState = 5;
					}
					else
						uState = 1;

					continue;
				}

				return FALSE;

			case 1: // Look for opening quote of name or closing brace or bracket.

				if( c == (m_fList ? ']' : '}') ) {

					uState = 10;

					continue;
				}

				if( c == '\"' ) {

					Name.Empty();

					uState = 2;

					continue;
				}

				return FALSE;

			case 2: // Build up name while looking for closing quote.

				if( c == '\\' ) {

					uState = 3;

					continue;
				}

				if( c == '\"' ) {

					uState = 4;

					continue;
				}

				Name += c;

				continue;

			case 3: // Include escaped character and continue building name.

				if( c == 'u' ) {

					if( strlen(pText) >= 4 ) {

						DWORD u = strtoul(CString(pText, 4), NULL, 16);

						if( u ) {

							if( !HIBYTE(u) ) {

								Name += char(u);
							}
							else
								Name += CPrintf("&#%u;", u);
						}

						pText += 4;

						uState = 3;

						continue;
					}

					return FALSE;
				}
				else {
					PCTXT pFind;

					if( (pFind = strchr(m_cRep, c)) ) {

						Name += m_cEsc[pFind - m_cRep];
					}
					else
						Name += c;
				}

				uState = 3;

				continue;

			case 4: // Look for colon

				if( c == ':' ) {

					uState = 5;

					continue;
				}

				return FALSE;

			case 5: // Look for brace, bracket, quote or bare value.

				if( m_fList && c == ']' ) {

					m_uIndex--;

					uState = 10;

					continue;
				}

				if( c == '{' || c == '[' ) {

					pText--;

					CMqttJsonData *pSub;

					AddChild(Name, c == '[', pSub);

					if( pSub->ParseBlock(pText) ) {

						uState = 9;

						continue;
					}

					return FALSE;
				}

				if( c == '\"' ) {

					Data.Empty();

					uState = 6;

					continue;
				}

				Data   = CString(c, 1);

				uState = 8;

				continue;

			case 6: // Build up data while looking for closing quote.

				if( c == '\\' ) {

					uState = 7;

					continue;
				}

				if( c == '\"' ) {

					AddValue(Name, Data, jsonString);

					uState = 9;

					continue;
				}

				Data += c;

				continue;

			case 7: // Include escaped character and continue building data.

				if( c == 'u' ) {

					if( strlen(pText) >= 4 ) {

						DWORD u = strtoul(CString(pText, 4), NULL, 16);

						if( !HIBYTE(u) ) {

							Data += char(u);
						}
						else
							Data += CPrintf("&#%u;", u);

						pText += 4;

						uState = 6;

						continue;
					}

					return FALSE;
				}
				else {
					PCTXT pFind;

					if( (pFind = strchr(m_cRep, c)) ) {

						Data += m_cEsc[pFind - m_cRep];
					}
					else
						Data += c;
				}

				uState = 6;

				continue;

			case 8: // Build up bare value while looking space, comma or child close.

				if( isspace(c) || c == ',' || c == '}' || c == ']' ) {

					pText--;

					if( Data == "null" ) {

						AddValue(Name, Data, jsonNull);
					}
					else
						AddValue(Name, Data, jsonNumber);

					uState = 9;

					continue;
				}

				Data += c;

				continue;

			case 9: // Look for comma or closing brace or bracket.

				if( c == ',' ) {

					if( m_fList ) {

						Name   = CreateName(m_uIndex++);

						uState = 5;
					}
					else
						uState = 1;

					continue;
				}

				if( c == (m_fList ? ']' : '}') ) {

					uState = 10;

					continue;
				}

				return FALSE;

			case 10: // Return next non-space characters to caller.

				pText--;

				return TRUE;
		}
	}

	if( uState == 10 ) {

		return TRUE;
	}

	return FALSE;
}

CString CMqttJsonData::GetAsText(BOOL fPretty, UINT uLevel) const
{
	if( !IsEmpty() ) {

		CString t;

		t += fPretty ? (m_fList ? "[\r\n" : "{\r\n") : (m_fList ? "[" : "{");

		uLevel++;

		BOOL fFirst = TRUE;

		for( INDEX n = m_Tree.GetHead(); !m_Tree.Failed(n); m_Tree.GetNext(n) ) {

			CJsonPair const &Pair = m_Tree[n];

			if( !fFirst ) {

				t += fPretty ? ",\r\n" : ",";
			}

			if( fPretty ) {

				t += CString(' ', 2 * uLevel);
			}

			if( !m_fList ) {

				t += "\"";

				t += UniEncode(Pair.m_Name);

				t += fPretty ? "\": " : "\":";
			}

			if( Pair.m_pSub ) {

				t += Pair.m_pSub->GetAsText(fPretty, uLevel);
			}
			else {
				UINT Type = Pair.m_Type;

				if( Type == jsonAuto ) {

					Type = FindAutoType(Pair.m_Value);
				}

				if( Type == jsonNull ) {

					t += "null";
				}

				if( Type == jsonNumber ) {

					t += Pair.m_Value;
				}

				if( Type == jsonBool ) {

					t += Pair.m_Value;
				}

				if( Type == jsonString ) {

					t += "\"";

					if( Pair.m_Value.FindOne(m_cEsc) < NOTHING ) {

						CString e;

						PCTXT   v = Pair.m_Value;

						for( UINT n = 0; v[n]; v++ ) {

							PCTXT f = strchr(m_cEsc, v[n]);

							if( f ) {

								e += '\\';

								e += m_cRep[f - m_cEsc];
							}
							else
								e += v[n];
						}

						t += UniEncode(e);
					}
					else
						t += UniEncode(Pair.m_Value);

					t += "\"";
				}
			}

			fFirst = FALSE;
		}

		if( fPretty ) {

			if( !fFirst ) {

				t += "\r\n";
			}

			t += CString(' ', 2 * --uLevel);
		}

		t += m_fList ? T("]") : T("}");

		return t;
	}

	return m_fList ? T("[]") : T("{}");
}

CString CMqttJsonData::UniEncode(CString Text) const
{
	PCTXT pHex = T("0123456789ABCDEF");

	for( ;;) {

		UINT uPos = Text.Find(T("&#"));

		if( uPos < NOTHING ) {

			PCTXT pFind = PCTXT(Text) + uPos;

			PTXT  pEnd  = NULL;

			UINT  uChar = strtoul(pFind + 2, &pEnd, 10);

			if( *pEnd == ';' ) {

				UINT uLen = pEnd - pFind + 1;

				if( uLen > 6 ) {

					Text.Delete(uPos, uLen - 6);
				}

				if( uLen < 6 ) {

					TCHAR s[] = T("0123456");

					s[6 - uLen] = 0;

					Text.Insert(uPos, s);
				}

				Text.SetAt(uPos+0, '\\');
				Text.SetAt(uPos+1, 'u');
				Text.SetAt(uPos+2, pHex[(uChar>>12)&15]);
				Text.SetAt(uPos+3, pHex[(uChar>> 8)&15]);
				Text.SetAt(uPos+4, pHex[(uChar>> 4)&15]);
				Text.SetAt(uPos+5, pHex[(uChar>> 0)&15]);

				continue;
			}
		}

		break;
	}

	return Text;
}

CString CMqttJsonData::CreateName(UINT uIndex) const
{
	return CPrintf("%4.4X", uIndex);
}

UINT CMqttJsonData::FindAutoType(CString const &Data) const
{
	if( !Data.IsEmpty() ) {

		if( isspace(Data[0]) || isspace(Data[Data.GetLength()-1]) ) {

			CString Text = Data;

			Text.TrimBoth();

			return IsNumber(Text) ? jsonNumber : jsonString;
		}

		return IsNumber(Data) ? jsonNumber : jsonString;
	}

	return jsonString;
}

BOOL CMqttJsonData::IsNumber(PCTXT pText) const
{
	// Check if a string conforms to the rather strict JSON
	// numeric formatting rules. This means no leading plus, no
	// leading zeroes, no leading decimal point. If it doesn't
	// conform, we're going to have to treat it as a string.

	if( *pText == '-' ) {

		pText++;
	}

	if( isdigit(*pText) ) {

		if( *pText == '0' ) {

			pText++;
		}
		else {
			do {
				pText++;

			} while( isdigit(*pText) );
		}

		if( *pText == '.' ) {

			pText++;

			if( !isdigit(*pText) ) {

				return FALSE;
			}

			do {
				pText++;

			} while( isdigit(*pText) );
		}

		if( *pText == 'E' || *pText == 'e' ) {

			pText++;

			if( *pText == '-' || *pText == '+' ) {

				pText++;
			}

			if( !isdigit(*pText) ) {

				return FALSE;
			}

			do {
				pText++;

			} while( isdigit(*pText) );
		}

		return !*pText;
	}

	return FALSE;
}

// End of File
