
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Multi Item Proxy
//

// Dynamic Class

AfxImplementDynamicClass(CMultiItem, CItem);

// Constructor

CMultiItem::CMultiItem(void)
{
	}

// UI Management

CViewWnd * CMultiItem::CreateView(UINT uType)
{
	return New CDummyViewWnd;
	}

// End of File
