
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PROJVIEW_HPP
	
#define	INCLUDE_PROJVIEW_HPP

/////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CProgramExecutionCtrlWnd;
class CProgramExecutionViewWnd;

/////////////////////////////////////////////////////////////////////////
//
// Move Description
//

struct CMoveDesc {
			
	DWORD m_dwProg;
	DWORD m_dwMove;
	};

/////////////////////////////////////////////////////////////////////////
//
// Type
//

typedef CArray <CMoveDesc> CMoveList;

/////////////////////////////////////////////////////////////////////////
//
// Control Project View
//

class CControlProjectView : public CUIItemMultiWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlProjectView(CUIPageList *pList);

		// Edit Commands
		class CCmdMove;

	protected:
		// Data Members
		CProgramExecutionCtrlWnd * m_pEditor;

		// Overridables
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Implementation
		BOOL FindEditor(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Project View -- Move Command
//

class CControlProjectView::CCmdMove : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdMove(CString Item, DWORD dwProgram, CMoveList Move, CMoveList Undo);

		// Data Members
		CString		m_Menu;
		DWORD		m_dwProg;
		DWORD		m_dwParent;
		CMoveList	m_Move;
		CMoveList	m_Undo;
	};

//////////////////////////////////////////////////////////////////////////
//
// Program Execution Order Window
//

class CProgramExecutionViewWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CProgramExecutionViewWnd(void);

		// Operations
		void    Load(void);
		CString Save(void);
		void    Undo(CCmd *pCmd);
		void    Exec(CCmd *pCmd);

	protected:
		// Language
		enum {
			langSfc	     = 1,
			langSt	     = 2,
			langFbd	     = 4,
			langLd	     = 8,
			langIl	     = 16,
			};

		// Section
		enum {
			sectBegin    = 1,
			sectSfcMain  = 2,
			sectEnd	     = 4,
			sectSfcChild = 8,
			sectUdfb     = 16,
			sectMain     = sectBegin | sectSfcMain | sectEnd,
			};

		// Move
		enum {
			moveUp      = 1,
			moveDown    = 2,
			moveBegin   = 3,
			moveEnd     = 4,
			moveSfcMain = 5,
			moveBefore  = 7,
			moveAfter   = 8,
			};

		// Object Maps
		typedef CZeroMap <CString, HTREEITEM> CNameMap;

		// Data
		CImageList    m_Images;
		CToolbarWnd * m_pTool;
		CTreeView   * m_pTree;
		int           m_nTop;
		int	      m_nBotton;
		HTREEITEM     m_hRoot;
		HTREEITEM     m_hSelect;
		BOOL	      m_fLoading;
		CNameMap      m_MapNames;
		
		//
		CControlProject * m_pProject;
		DWORD	          m_dwProject;

		// Drop Context
		UINT          m_uDrop;
		CDropHelper   m_DropHelp;
		CSysProxy     m_Proxy;
		
		// Overribables
		void    OnAttach(void);
		BOOL	OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);
		void OnPostCreate(void);
		void OnGoingIdle(void);
		void OnUpdateUI(void);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);

		// Command Handlers
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);
		BOOL CanMoveUp(void);
		BOOL CanMoveDown(void);
		void OnMoveUp(void);
		void OnMoveDown(void);

		// Tree Loading
		void LoadRoot(void);
		void LoadTree(void);

		// Implementation
		BOOL	GetNodeCode(CTreeViewItem &Node, CString Code);
		BOOL	GetNodeCode(CTreeViewItem &Node, DWORD dwProgram);
		UINT	GetTreeImage(DWORD dwProgram);
		void	RefreshTree(void);
		void	KillTree(HTREEITEM hRoot);
		CString SaveSelState(void);
		void    LoadSelState(CString Sel);
		void	NewItemSelected(void);
		void    CreateTool(CRect Tool);
		void    CreateTree(CRect Tree);
		void	ExecMoveList(DWORD dwProg, CMoveList const &List);

		// Debug
		void    ShowDebug(void);

		// Move Helpers
		BOOL CanMoveProgram(DWORD dwProgram, DWORD dwMove, DWORD dwParent);
		BOOL MoveProgram   (DWORD dwProgram, DWORD dwMove, DWORD dwParent);
	};

//////////////////////////////////////////////////////////////////////////
//
// Project Execution Ctrl Window
//

class CProgramExecutionCtrlWnd : public CCtrlWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CProgramExecutionCtrlWnd(void);

		// Operations
		void Attach(CItem *pItem);

		void Load(void);
		void Save(void);
		void Exec(CCmd *pCmd);
		void Undo(CCmd *pCmd);

	protected:
		// Data
		CProgramExecutionViewWnd * m_pView;
		CControlProject          * m_pProject;		

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnSize(UINT uCode, CSize Size);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnPostCreate(void);
	};

// End of File

#endif
