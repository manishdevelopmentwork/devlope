
#include "intern.hpp"

#include "monj1939.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// Monico J1939 Driver
//

// Import

#include "..\j1939\j1939.cpp"

// Instantiator

INSTANTIATE(CMonicoJ1939Driver);

// Constructor

CMonicoJ1939Driver::CMonicoJ1939Driver(void)
{
	m_Ident = DRIVER_ID;
       	}

// End of File

