#include "intern.hpp"

#include "pcommsb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM Binary Master Serial Driver
//
//

// Instantiator

INSTANTIATE(CPcomMsBDriver);

// Constructor

CPcomMsBDriver::CPcomMsBDriver(void)
{
	m_Ident         = DRIVER_ID; 
	}

// Destructor

CPcomMsBDriver::~CPcomMsBDriver(void)
{
	}

// Configuration

void MCALL CPcomMsBDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CPcomMsBDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CPcomMsBDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CPcomMsBDriver::Open(void)
{
	}

// Device

CCODE MCALL CPcomMsBDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bUnit  = GetByte(pData);

			UINT uCount      = GetByte(pData);

			m_pCtx->m_TableCount = uCount;
			
			m_pCtx->m_pTables = new CTable[uCount];

			for( UINT x = 0; x < uCount; x++ ) {

				CTable * pTable = new CTable;

				pTable->m_Rows  = GetWord(pData);

				UINT uCols = GetByte(pData);

				UINT y = 0;

				for( ; y < uCols; y++ ) { 

					pTable->m_Cols[y].m_Type  = GetByte(pData); 

					pTable->m_Cols[y].m_Bytes = GetWord(pData);
					}

				for( ; y < elements(pTable->m_Cols); y++ ) {

					pTable->m_Cols[y].m_Type  = NOTHING; 

					pTable->m_Cols[y].m_Bytes = NOTHING;
					}

				m_pCtx->m_pTables[x] = *pTable;
				}
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CPcomMsBDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pCtx->m_pTables;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Implementation

BOOL CPcomMsBDriver::Transact(void)
{
	if( Send() && RecvFrame() && CheckFrame() ) {
		
		return TRUE;
		}

	return FALSE; 
	}

BOOL CPcomMsBDriver::Send(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CPcomMsBDriver::RecvFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	m_uPtr = 0;

	BOOL fSTX = FALSE;

	UINT uTotal = NOTHING;

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {
			
			continue;
			}

		if( !fSTX ) {

			if( uData != '/' ) {

				continue;
				}

			fSTX = TRUE;
			}

		m_bRx[m_uPtr] = uData;

		m_uPtr++;

		if( m_uPtr == 21 ) {

			uTotal = m_uPtr + uData + 6;
			}

		if( m_uPtr == uTotal ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// End of file

