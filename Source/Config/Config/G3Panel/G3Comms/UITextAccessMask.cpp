
#include "Intern.hpp"

#include "UITextAccessMask.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "AccessMask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Access Mask
//

// Dynamic Class

AfxImplementDynamicClass(CUITextAccessMask, CUITextElement);

// Constructor

CUITextAccessMask::CUITextAccessMask(void)
{
	m_uFlags = textExpand | textLocking;

	m_Verb   = CString(IDS_EDIT);
	}

// Overridables

void CUITextAccessMask::OnBind(void)
{
	CUITextElement::OnBind();

	m_uWidth = 20;
	}

void CUITextAccessMask::OnRebind(void)
{
	CUITextElement::OnRebind();
	}

CString CUITextAccessMask::OnGetAsText(void)
{
	return Format(m_pData->ReadInteger(m_pItem));
	}

UINT CUITextAccessMask::OnSetAsText(CError &Error, CString Text)
{
	UINT uPrev = m_pData->ReadInteger(m_pItem);

	UINT uData = Parse(Text);

	if( uData ^ uPrev ) {

		m_pData->WriteInteger(m_pItem, uData);

		return saveChange;
		}

	return saveSame;
	}

BOOL CUITextAccessMask::OnExpand(CWnd &Wnd)
{
	UINT uLast = m_pData->ReadInteger(m_pItem);

	CAccessMask Item;

	Item.SetParent(m_pItem);

	Item.Init();

	if( m_UIData.m_Format[0] == L'D' ) {

		Item.BlockDefault();
		}

	if( m_UIData.m_Format[1] == L'P' ) {

		Item.SetDispPage();
		}
	else {
		CString Class = m_pItem->GetParent()->GetClassName();

		if( Class.Find(L"Disp") < NOTHING ) {

			Item.SetDispPage();
			}
		}

	Item.LoadData(uLast);

	CItemDialog Dialog(&Item, CString(IDS_EDIT_ACCESS));

	if( Dialog.Execute(Wnd) ) {

		CString Text = Format(Item.ReadData());

		SetAsText(CError(FALSE), Text);

		return TRUE;
		}

	return FALSE;
	}

// Implementaiton

CString CUITextAccessMask::Format(UINT uData)
{
	CString Text;

	UINT uMode = (uData >> 30);

	if( !uMode && (uData & ~allowCheck) ) {

		if( uData & (1<<15) ) {

			Text += L"M";
			}

		for( UINT n = 0; n < 8; n++ ) {

			if( uData & (1 << (16+n)) ) {

				if( !Text.IsEmpty() ) {

					Text += L",";
					}

				Text += CPrintf(L"R%u", 1+n);
				}
			}

		if( uData & allowProgram ) {

			if( !Text.IsEmpty() ) {

				Text += L",";

				Text += L"P";
				}
			else
				Text += CString(IDS_PROGRAMS_ONLY);
			}
		}
	else {
		switch( uData & allowMask ) {

			case allowNone:
				Text = CString(IDS_NO_ACCESS);
				break;

			case allowUsers:
				Text = CString(IDS_AUTHENTICATED);
				break;

			case allowAnyone:
				Text = CString(IDS_UNAUTHENTICATED);
				break;

			case allowDefault:
				Text = CString(IDS_DEFAULT_FOR);
				break;
			}

		if( uData & allowProgram ) {

			Text += CString(IDS_AND_PROGRAMS);
			}
		}

	if( uData & allowCheck ) {

		Text += CString(IDS_WITH_CBO);
		}

	return Text;
	}

UINT CUITextAccessMask::Parse(CString Text)
{
	BOOL    fMatch = FALSE;

	UINT    uData  = 0;

	CString Test   = CString(IDS_WITH_CBO);

	CString Prog   = CString(IDS_AND_PROGRAMS);

	UINT    uLen1  = Test.GetLength();

	UINT    uLen2  = Prog.GetLength();

	if( Text.Right(uLen1) == Test ) {

		Text   = Text.Left(Text.GetLength() - uLen1);

		uData |= allowCheck;
		}

	if( Text.Right(uLen2) == Prog ) {

		Text   = Text.Left(Text.GetLength() - uLen2);

		fMatch = TRUE;

		uData |= allowProgram;
		}

	if( Text == CString(IDS_PROGRAMS_ONLY) ) {

		fMatch = TRUE;

		uData |= allowProgram;
		}

	if( Text == CString(IDS_NO_ACCESS) ) {

		fMatch = TRUE;

		uData |= allowNone;
		}

	if( Text == CString(IDS_AUTHENTICATED) ) {

		fMatch = TRUE;

		uData |= allowUsers;
		}

	if( Text == CString(IDS_UNAUTHENTICATED) ) {

		fMatch = TRUE;

		uData |= allowAnyone;
		}

	if( Text == CString(IDS_DEFAULT_FOR) ) {

		fMatch = TRUE;

		uData |= allowDefault;
		}

	if( !fMatch ) {

		CStringArray List;

		Text.Tokenize(List, ',');

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			if( List[n] == "M" ) {

				uData |= (1<<15);

				continue;
				}

			if( List[n] == "P" ) {

				uData |= allowProgram;

				continue;
				}

			if( List[n][0] == 'R' ) {

				UINT r = watoi(PCTXT(List[n])+1);

				uData |= (1<<(16+r-1));

				continue;
				}

			return FALSE;
			}
		}

	return uData;
	}

// End of File
