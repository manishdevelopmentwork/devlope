
#include "Intern.hpp"

#include "OpcReference.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OpcDataModel.hpp"

#include "OpcReferenceTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Reference
//

// Constructors

COpcReference::COpcReference(void)
{
	m_fInverse = false;
	}

COpcReference::COpcReference(COpcReference const &That) : m_pModel   (That.m_pModel),
							  m_TypeId   (That.m_TypeId),
							  m_TargetId (That.m_TargetId),
							  m_fInverse (That.m_fInverse),
							  m_TargetUri(That.m_TargetUri)
{
	}

COpcReference::COpcReference(COpcDataModel *pModel, COpcNodeId const &TypeId, COpcNodeId const &TargetId, bool fInverse) : m_pModel   (pModel),
															   m_TypeId   (TypeId),
															   m_TargetId (TargetId),
															   m_fInverse (fInverse),
															   m_TargetUri("SOMETHING")
{
	}

// Assignment

COpcReference & COpcReference::operator = (COpcReference const &That)
{
	m_pModel    = That.m_pModel;

	m_TypeId    = That.m_TypeId;

	m_TargetId  = That.m_TargetId;

	m_fInverse  = That.m_fInverse;

	m_TargetUri = That.m_TargetUri;

	return ThisObject;
	}

// Attributes

bool COpcReference::NeedInverse(void) const
{
	return !m_fInverse && m_pModel->FindReferenceType(m_TypeId, false)->NeedInverse();
	}

// End of File
