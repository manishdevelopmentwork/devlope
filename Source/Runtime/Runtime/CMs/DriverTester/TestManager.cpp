
#include "Intern.hpp"

#include "TestManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Driver Test Manager
//

// Instantiator

clink IUnknown * Create_TestManager(PCSTR pName)
{
	return (IClientProcess *) New CTestManager;
	}

// Constructor

CTestManager::CTestManager(void)
{
	StdSetRef();

	m_fOpen   = FALSE;

	m_pThread = NULL;

	m_pDriver = NULL;

	m_pTester = NULL;

	m_pPort   = NULL;

	memset(m_bConfig, 0, sizeof(m_bConfig));

	memset(m_bDevice, 0, sizeof(m_bDevice));

	m_pStop = Create_ManualEvent();

	piob->RegisterSingleton("helper-slave", 0, (ISlaveHelper *) this);

	DiagRegister();

	ClearStats();
	}

// Destructor

CTestManager::~CTestManager(void)
{
	Close();
	
	DiagRevoke();

	piob->RevokeSingleton("helper-slave", 0);

	m_pStop->Release();
	}

// IUnknown

HRESULT CTestManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(ICommsDevice);

	StdQueryInterface(ISlaveHelper);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CTestManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CTestManager::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CTestManager::TaskInit(UINT uTask)
{
	if( DriverCreate() && TesterCreate() ) { 

		DriverLoad();

		if( DriverOpen() ) {

			m_fOpen = TRUE;

			return TRUE;
			}

		DriverClose();
			
		DriverFree();
		}

	return TRUE;
	}

INT CTestManager::TaskExec(UINT uTask)
{
	SetThreadName("DriverTest");

	ClearStats();

	m_pStop->Clear();

	while( !m_pStop->Wait(10) ) {

		if( m_fOpen ) {

			DriverService();
			}
		else
			Sleep(100);
		}

	return 0;
	}

void CTestManager::TaskStop(UINT uTask)
{
	}

void CTestManager::TaskTerm(UINT uTask)
{
	if( m_fOpen ) {

		if( GetThreadExitCode(m_pThread) < 0 ) {

			DeviceClose();
			}

		DriverClose();

		DriverFree();
		}
	}

// ICommsDevice

LPCBYTE CTestManager::GetConfig(void)
{
	return m_bDevice;
	}

UINT CTestManager::GetConfigSize(void)
{
	return m_uDevice;
	}

void CTestManager::SetContext(PVOID pData)
{
	m_pContext = pData;
	}

PVOID CTestManager::GetContext(void)
{
	return m_pContext;
	}

// ISlaveHelper

void CTestManager::Bind(ICommsDevice *pDevice)
{
	}

CCODE CTestManager::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pTester->SlaveRead(Addr, pData, uCount);
	}

CCODE CTestManager::Read(AREF Addr, PDWORD pData, IMetaData **ppm)
{
	return m_pTester->SlaveRead(Addr, pData, ppm);
	}

CCODE CTestManager::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pTester->SlaveWrite(Addr, pData, uCount);
	}

// IDiagProvider

UINT CTestManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagOpen(pOut, pCmd);

		case 2: 
			return DiagClose(pOut, pCmd);

		case 3:
			return DiagStats(pOut, pCmd);
		}

	return 0;
	}

// Entry

void CTestManager::Open(void)
{
	if( !m_pThread ) {

		m_pThread = CreateClientThread(this, 0, 500);
		}
	}

void CTestManager::Close(void)
{
	if( m_pThread ) {

		UINT uTimeout = (m_pDriver->GetCategory() == DC_COMMS_SLAVE) ? 2500 : FOREVER;

		m_pStop->Set();

		if( !m_pThread->Wait(uTimeout) ) {

			m_pThread->Destroy();
			}
		else
			m_pThread->Release();

		m_pThread = NULL;
		}
	}

// Driver

BOOL CTestManager::DriverCreate(void)
{
	AfxNewObject("driver", ICommsDriver, m_pDriver);

	m_pMaster = NULL;

	m_pSlave  = NULL;

	if( m_pDriver ) {

		m_pDriver->QueryInterface(AfxAeonIID(ICommsMaster), (void **) &m_pMaster);

		m_pDriver->QueryInterface(AfxAeonIID(ICommsSlave),  (void **) &m_pSlave);

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestManager::DriverLoad(void)
{
	InitConfig();

	InitDevice();

	m_pDriver->Load(m_bConfig);

	return TRUE;
	}

BOOL CTestManager::DriverOpen(void)
{
	CSerialConfig Config;

	if( m_pTester->GetSerialConfig(Config) ) {

		AfxGetObject("uart", 0, IPortObject, m_pPort);

		m_pDriver->CheckConfig(Config);

		m_pDriver->Attach(m_pPort);

		if( m_pPort->Open(Config) ) {

			m_pDriver->Open();

			return TRUE;
			}

		AfxRelease(m_pPort);

		m_pPort = NULL;
		}
	else {
		m_pDriver->Attach(NULL);

		m_pDriver->Open();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestManager::DriverService(void)
{
	if( m_pDriver->GetCategory() == DC_COMMS_MASTER ) {

		if( DeviceOpen() ) {

			m_pTester->ExecDeviceCtrl(m_pDriver);

			CCODE Error = m_pMaster->Ping();

			if( COMMS_SUCCESS(Error) ) { 

				UINT i = 0;
				
				UINT c = 0;

				while( !m_pStop->Wait(0) ) {

					CCODE Code;

					if( i == 0 ) {

						AfxTrace("==== CYCLE %u ====\n\n", ++c);
						}

					if( !m_pTester->ExecTest(Code, m_pMaster, i++) ) {

						m_pMaster->Service();

						i = 0;

						continue;
						}

					UpdateStats(Code);

					Sleep(10);
					}

				AfxTrace("=== DONE ===\n\n");
				}
			else
				AfxTrace("Driver Ping Failure : 0x%8.8X\n", Error);

			DeviceClose();
			}
		else
			AfxTrace("Device Open Failure\n");
		
		return TRUE;
		}

	if( m_pDriver->GetCategory() == DC_COMMS_SLAVE ) {

		if( DeviceOpen() ) {

			AfxTrace("=== SLAVE ACTIVE ===\n\n");

			while( !m_pStop->Wait(0) ) {

				m_pSlave->Service();
				}

			DeviceClose();

			AfxTrace("=== DONE ===\n\n");
			}
		else
			AfxTrace("Device Open Failure\n");

		return FALSE;
		}

	return FALSE;
	}

BOOL CTestManager::DriverClose(void)
{
	m_pDriver->Close();

	if( m_pPort ) {

		m_pPort->Close();

		AfxRelease(m_pPort);

		m_pPort = NULL;
		}

	return TRUE;
	}

BOOL CTestManager::DriverFree(void)
{
	m_pDriver->Detach();

	m_pDriver->Release();
	
	AfxRelease(m_pMaster);

	AfxRelease(m_pSlave);

	return FALSE;
	}

BOOL CTestManager::DeviceOpen(void)
{
	switch( m_pDriver->GetCategory() ) {

		case DC_COMMS_MASTER:
		case DC_COMMS_SLAVE:

			return COMMS_SUCCESS(m_pDriver->DeviceOpen((ICommsDevice *) this));
		}

	return FALSE;
	}

BOOL CTestManager::DeviceClose(void)
{
	switch( m_pDriver->GetCategory() ) {

		case DC_COMMS_MASTER:
		case DC_COMMS_SLAVE:

			return COMMS_SUCCESS(m_pDriver->DeviceClose(FALSE));
		}

	return FALSE;
	}

void CTestManager::InitConfig(void)
{
	PBYTE pData = m_bConfig;

	*pData++ = 0x12;

	*pData++ = 0x34;

	m_pTester->GetDriverConfig(pData);

	m_uConfig = pData - m_bConfig;

	AfxTrace("Driver config is %u bytes:\n\n", m_uConfig);

	AfxDump(m_bConfig, m_uConfig);

	AfxTrace("\n");
	}

void CTestManager::InitDevice(void)
{
	PBYTE pData = m_bDevice;

	*pData++ = 0x12;

	*pData++ = 0x34;

	m_pTester->GetDeviceConfig(pData);

	m_uDevice  = pData - m_bDevice;

	m_pContext = NULL;

	AfxTrace("Device config is %u bytes:\n\n", m_uDevice);

	AfxDump(m_bDevice, m_uDevice);

	AfxTrace("\n");
	}

// Tester

BOOL CTestManager::TesterCreate(void)
{
	AfxNewObject("tester", ICommsTester, m_pTester);

	if( m_pTester ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestManager::TesterFree(void)
{
	m_pTester->Release();

	return TRUE;
	}

// Statistics

void CTestManager::ClearStats(void)
{
	memset(&m_Stats, 0, sizeof(CError));
	}

void CTestManager::UpdateStats(CCODE Code)
{
	if( COMMS_ERROR(Code) ) {

		if( Code & CCODE_HARD ) {

			m_Stats.m_uHard++;
			}

		if( Code & CCODE_BUSY ) {

			m_Stats.m_uBusy++;
			}

		if( Code & CCODE_SILENT ) {
			
			m_Stats.m_uSilent++;
			}
			
		if( Code & CCODE_NO_RETRY ) {	
		
			m_Stats.m_uRetry++;
			}

		if( Code & CCODE_NO_CONFIG ) {
			
			m_Stats.m_uConfig++;
			}

		return;
		}

	if( Code & CCODE_NO_DATA ) {
		
		m_Stats.m_uData++;
		}

	m_Stats.m_uGood++;
	}

// Diagnostics

BOOL CTestManager::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "driver");

		pDiag->RegisterCommand(m_uProv, 1, "open");

		pDiag->RegisterCommand(m_uProv, 2, "close");

		pDiag->RegisterCommand(m_uProv, 3, "stats");

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestManager::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
		}

	return FALSE;
	}

UINT CTestManager::DiagOpen(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		Open();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CTestManager::DiagClose(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		Close();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CTestManager::DiagStats(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->Print("Comms.Good   = %d\n", m_Stats.m_uGood);
		pOut->Print("Comms.Hard   = %d\n", m_Stats.m_uHard);
		pOut->Print("Comms.Busy   = %d\n", m_Stats.m_uBusy);
		pOut->Print("Comms.Silent = %d\n", m_Stats.m_uSilent);
		pOut->Print("Comms.Retry  = %d\n", m_Stats.m_uRetry);
		pOut->Print("Comms.Data   = %d\n", m_Stats.m_uData);
		pOut->Print("Comms.Config = %d\n", m_Stats.m_uConfig);

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// End of File
