
REM *** Macro definitions:

REM	%1 = $(ToolDir)
REM	%2 = $(TargetDir)
REM	%3 = $(TargetName)
REM	%4 = $(TargetPath)

REM *** Generate RedHostEdge.cbf for testing

"%~1fixup.exe" -m 1 -b 0x20000C00 -c -o "%~2%~3.cbf" %4

REM *** Generate model-specific files for deployment

"%~1fixup.exe" -m 1 -b 0x20000C00 -c -g -o "%~2gc.bin"  %4
"%~1fixup.exe" -m 1 -b 0x20000C00 -c -g -o "%~2gcn.bin" %4
"%~1fixup.exe" -m 2 -b 0x20000C00 -c -g -o "%~2gsr.bin" %4

REM *** Done
