
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DH485_HPP
	
#define	INCLUDE_DH485_HPP

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Base Driver
//

#include "abbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley SLC500 via DH485 Driver Options
//

class CSLCDH485DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSLCDH485DriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_ThisDrop;
		UINT m_LastDrop;
		UINT m_MakeToken;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DH485 Device Options
//

class CDH485MasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDH485MasterSerialDeviceOptions(void);
				
		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Device;
		UINT m_Drop;
	
	protected:

		// Data Members
		CString m_Root;
		UINT	m_Name;
		UINT	m_Label;

		
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley SLC500 via DH485
//

class CSLCDH485Driver : public CABDriver
{
	public:
		// Constructor
		CSLCDH485Driver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info); 

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
	
		void AddSpaces(void);		
	};

// End of File

#endif
