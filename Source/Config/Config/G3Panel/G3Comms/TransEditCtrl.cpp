
#include "Intern.hpp"

#include "TransEditCtrl.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Translation Edit Control
//

// Runtime Class

AfxImplementRuntimeClass(CTransEditCtrl, CDropEditCtrl);

// Constructor

CTransEditCtrl::CTransEditCtrl(CUIElement *pUI) : CDropEditCtrl(pUI)
{
	m_pLang = NULL;

	m_fKeyb = FALSE;
	}

// Operations

void CTransEditCtrl::SetSlot(CLangManager *pLang, UINT uSlot)
{
	m_pLang = pLang;

	m_uSlot = uSlot;
	}

// Message Map

AfxMessageMap(CTransEditCtrl, CDropEditCtrl)
{
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)

	AfxMessageEnd(CTransEditCtrl)
	};

// Message Handlers

void CTransEditCtrl::OnSetFocus(CWnd &Wnd)
{
	if( m_pLang ) {

		if( m_pLang->SetKeyboard(m_uSlot) ) {

			m_fKeyb = TRUE;
			}
		}

	AfxCallDefProc();
	}

void CTransEditCtrl::OnKillFocus(CWnd &Wnd)
{
	if( m_pLang ) {

		if( m_fKeyb ) {

			m_pLang->ClearKeyboard();

			m_fKeyb = FALSE;
			}
		}

	CDropEditCtrl::OnKillFocus(Wnd);
	}

// End of File
