
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ASI_HPP
	
#define	INCLUDE_ASI_HPP

//////////////////////////////////////////////////////////////////////////
//
// IFM CoDeSys SP Serial Master Driver
//

class CIfmCoDeSysSpDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CIfmCoDeSysSpDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
					
	protected:
					
		// Implementation
		void	AddSpaces(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// IFM Space Wrapper
//

class CSpaceIFM : public CSpace
{
	public:
		// Constructors
		CSpaceIFM(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};



// End of File

#endif
