
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Command Builder
//

// Constructor

CCommandBuilder::CCommandBuilder(PCTXT pFormat, va_list pArgs)
{
	CString(pFormat).Tokenize(m_List, L'%');

	UINT c = m_List.GetCount();

	m_pArg = New LPSTR [ c ];

	for( UINT n = 0; n < c; n ++ ) {

		if( AddName(n       ) || 
		    AddBool(n, pArgs) ||
		    AddLong(n, pArgs) ||
		    AddInt (n, pArgs) ||
		    AddText(n, pArgs) ){ 
			
			continue;
			}

		AfxAssert(FALSE);
		}
	}

// Destructor

CCommandBuilder::~CCommandBuilder(void)
{
	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n ++ ) {
			
		delete m_pArg[n];
		}

	delete [] m_pArg;
	}

// Attributes

int CCommandBuilder::GetCount(void)
{
	return m_List.GetCount();
	}

LPSTR * CCommandBuilder::GetArgs(void)
{
	return m_pArg;
	}

// Implementation

BOOL CCommandBuilder::AddName(UINT uIndex)
{
	if( uIndex == 0 ) {

		CString    Arg = m_List[uIndex];

		m_pArg[uIndex] = WideToAnsi(Arg);
			
		return TRUE;
		}

	return FALSE;
	}

BOOL CCommandBuilder::AddBool(UINT uIndex, va_list & pArgs)
{
	if( m_List[uIndex] == L"b" ) {

		CString    Arg = CString(va_arg(pArgs, int) ? "1" : "0");

		m_pArg[uIndex] = WideToAnsi(Arg);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommandBuilder::AddLong(UINT uIndex, va_list & pArgs)
{
	if( m_List[uIndex] == L"ld" ) {

		CString    Arg = CPrintf(L"%ld", va_arg(pArgs, long unsigned int));

		m_pArg[uIndex] = WideToAnsi(Arg);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommandBuilder::AddInt (UINT uIndex, va_list & pArgs)
{
	if( m_List[uIndex] == L"d" ) {

		CString    Arg = CPrintf(L"%d", va_arg(pArgs, int));

		m_pArg[uIndex] = WideToAnsi(Arg);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommandBuilder::AddText(UINT uIndex, va_list & pArgs)
{
	if( m_List[uIndex] == L"s" ) {

		CString    Arg = CPrintf(L"%s", va_arg(pArgs, char*));

		m_pArg[uIndex] = WideToAnsi(Arg);

		return TRUE;
		}

	return FALSE;
	}

// End of File
