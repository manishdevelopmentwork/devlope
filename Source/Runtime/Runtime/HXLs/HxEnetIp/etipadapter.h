/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** EtIPAdapter.h
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Adapter API declaration. Should be included in the client application
** project for either static or dynamic linking to the EtIPAdapter DLL.
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

/* The following ifdef block is the standard way of creating macros which make exporting 
   from a DLL simpler. All files within this DLL are compiled with the ETIPADAPTER_EXPORTS
   symbol defined on the command line. this symbol should not be defined on any project
   that uses this DLL. This way any other project whose source files include this file see 
   ETIPADAPTER_API functions as being imported from a DLL, wheras this DLL sees symbols
   defined with this macro as being exported.
*/

#define ETIPADAPTER_API __declspec(dllexport)

#define ET_IP_ADAPTER

#define MAX_CONNECTION_PATH_SIZE	256		/* Maximum size of the connection path string */
#define MAX_CONNECTION_NAME_SIZE	256		/* Maximum size of the connection name string */
#define MAX_REQUEST_TAG_SIZE		256		/* Maximum size of the tag string */
#define MAX_MODULE_CONFIG_SIZE	    512		/* Maximum size of the module specific data */
#define MAX_ERROR_DESC_SIZE			512		/* Maximum size of the error description string */

#define MAX_SESSIONS				128		/* Maximum number of sessions that can be opened at the same time */
#define MAX_CONNECTIONS				128		/* Maximum number of connections that can be opened at the same time */

#define MAX_CLIENT_APP_PROC_CLASS_NBR		64     /* Maximum number of classes the client application can register for its own processing */
#define MAX_CLIENT_APP_PROC_SERVICE_NBR		64     /* Maximum number of services the client application can register for its own processing */

#define MAX_REQUEST_DATA_SIZE		2000	/* Maximum data size that can be send with the UCMM message = maximum packet size of 2K minus 48 bytes of UCMM header */

#define INVALID_SLOT_INDEX			0xff	/* Invalid slot index */
#define INVALID_CONN_POINT			0xffff	/* Invalid connection point */
#define INVALID_CLASS				0xffff	/* Class is not valid flag should be used when no class should be specified in the UCMM */
#define INVALID_INSTANCE			0xffff	/* Instance is not valid flag should be used when no instance should be specified in the UCMM */
#define INVALID_ATTRIBUTE			0xffff	/* Attribute is not valid flag should be used when no attribute should be specified in the UCMM */
#define INVALID_MEMBER				0xffff	/* Member is not valid flag should be used when no member should be specified in the UCMM */
#define INVALID_OFFSET				0xffff	/* Data Table offset should be assigned automatically by the stack */

#define REQUEST_INDEX_BASE			0x100	/* Request IDs will start with this number. Used to separate request IDs from the returned errors */

/* Connection related notification messages. Second parameter in the callback function will be Connection Instance number */			
#define NM_CONNECTION_ESTABLISHED					1	/* New connection successfully established */
#define NM_CONNECTION_TIMED_OUT						5	/* Active connection timed out. iWatchdogTimeoutAction member of the connection configuration will determine the associated action */
#define NM_CONNECTION_CLOSED						6	/* Connection closed based either on the local or remote request */
#define NM_CONNECTION_NEW_INPUT_DATA				7	/* New input data received on the specified connection */
#define NM_CONNECTION_NEW_OUTPUT_DATA				8	/* New output data was set for this connection by an explicit message to the Assembly object */

/* Object Request related notification messages. Second parameter in the callback function will be the Request ID */			
#define NM_REQUEST_RESPONSE_RECEIVED				10	/* Response received on the previously sent request */
#define NM_REQUEST_FAILED_INVALID_NETWORK_PATH		11	/* Unable to connect to the path specified in the request */
#define NM_REQUEST_TIMED_OUT						12	/* Request timed out - response never received. */
#define NM_CLIENT_OBJECT_REQUEST_RECEIVED			13	/* Request received for the object registered for the client application processing */

/* General error messages. Second parameter in the callback function is not used. */ 
#define NM_OUT_OF_MEMORY							20	/* Out of available memory. */
#define NM_UNABLE_INTIALIZE_WINSOCK					21	/* Unable to initialize Windows Sockets library. */
#define NM_UNABLE_START_THREAD						22  /* Unable to start a thread */
#define NM_ERROR_USING_WINSOCK						23	/* Received an error when using Windows Sockets library functions */
#define NM_ERROR_SETTING_SOCKET_TO_NONBLOCKING		24  /* Error encountered when setting the socket to non-blocking mode */
#define NM_ERROR_SETTING_TIMER						25	/* Error encountered when setting the required timer object */
#define NM_SESSION_COUNT_LIMIT_REACHED				26  /* Number of network peers exceeded MAX_SESSIONS constant */
#define NM_CONNECTION_COUNT_LIMIT_REACHED			27  /* Number of connections exceeded MAX_CONNECTIONS constant */
#define NM_PENDING_REQUESTS_LIMIT_REACHED			28  /* Number of outstanding object requests exceeded MAX_REQUESTS constant */

/* Following error codes may be returned to one of the function calls. They will not be used when invoking the callback function */
#define ERR_OBJECT_REQUEST_UNKNOWN_INDEX			30  /* Unknown index specified when trying to get an object response */
#define ERR_NO_AVAILABLE_OBJECT_RESPONSE			31  /* The response on the request sent has not been received yet */
#define ERR_INVALID_CONNECTION_INSTANCE_SPECIFIED	33  /* There is no active connection available for the specified instance */

typedef struct tagEtIPObjectRequest				/* Structure used to transfer UCMM message */
{
	unsigned char	bService;							/* Service to be performed */
	unsigned short	iClass;								/* Target class */
	unsigned short	iInstance;							/* Target instance. Can be 0 if the target is class */
	unsigned short	iAttribute;							/* Target attribute. Must be INVALID_ATTRIBUTE if the attribute should not be a part of the path */
	unsigned short	iMember;							/* Target member. Must be INVALID_MEMBER if the member should not be a part of the path */
	char			requestTag[MAX_REQUEST_TAG_SIZE];	/* Target extended symbol segment. If not used iTagSize must be 0. */
	unsigned short	iTagSize;							/* The actual size of the requestTag */
	unsigned char	requestData[MAX_REQUEST_DATA_SIZE];	/* Data that should be sent to the target. If not used iDataSize must be 0. */
	unsigned short	iDataSize;							/* The actual size of the requestData */
	unsigned long   lExplicitMessageTimeout;			/* Message Timeout in milliseconds. If 0, DEFAULT_EXPLICIT_REQUEST_TIMEOUT will be used instead. */
}
EtIPObjectRequest;

typedef struct tagEtIPObjectResponse			/* Structure used to get the response to a UCMM or an Unconnected Send message */
{		
	unsigned char   bGeneralStatus;							/* 0 in case of successful response, or one of the CIP general codes. */
	unsigned short  iExtendedStatus;						/* Together with General Status is used to provide more error information. */
	char		    errorDescription[MAX_ERROR_DESC_SIZE];	/* Null terminated error description string. It is filled only if bGeneralStatus is other than 0. */
	unsigned char	responseData[MAX_REQUEST_DATA_SIZE];	/* Response Data in case of a success. */
	unsigned short  iDataSize;								/* The size of the responseData in bytes */
}
EtIPObjectResponse;

typedef enum tagEtIPTransportClass		/* Class 1 and Class 3 transports supported */
{
	Class1  = 1,
    Class3  = 3
}
EtIPConnectionClass;

typedef enum tagEtIPTransportType
{
	Cyclic                = 0,			/* Produce and consume cyclicly on the connection */
    ChangeOfState         = 0x10,		/* Produce and consume on the state change as well as cyclicly to keep the connection open */
	ApplicationTriggered  = 0x20		/* Treat the same as ChangeOfState */
}
EtIPTransportType;

typedef enum tagEtIPConnectionType
{
	PointToPoint          = 0x4000,		/* Should be always used for Originator to Target transports */
    Multicast             = 0x2000,		/* Could be used for Target to Originator transports */
	NullConnType          = 0			/* Null connection type */
}
EtIPConnectionType;

typedef enum tagEtIPConnectionPriority
{
	LowPriority           = 0x0,		
    HighPriority          = 0x400,		
	ScheduledPriority     = 0x800,		
	UrgentPriority        = 0xC00,		
}
EtIPConnectionPriority;

typedef enum tagEtIPConnectionState
{
	ConnectionNonExistent         = 0x0,	// Connection is not instantiated
    ConnectionConfiguring         = 0x1,	// In the process of opening a new connection	
	ConnectionWaitingForID        = 0x2,	// This status will never be assigned since Connection object services are not supported
	ConnectionEstablished         = 0x3,	// Connection is active
	ConnectionTimedOut			  = 0x4,	// Connection has timed out; will stay in this state at least for some time if iWatchdogTimeoutAction is set to TimeoutManualReset or TimeoutDelayAutoReset.
	ConnectionDeferredDelete      = 0x5,	// Connection is about to be deleted and waiting for child connections to be closed first.
	ConnectionClosing             = 0x6,	// In the process of closing the connection
}
EtIPConnectionState;

typedef enum tagEtIPWatchdogTimeoutAction
{
	TimeoutAutoDelete		     = 0x0,		// Immediately Delete connection on watchdog timeout
    TimeoutDeferredDelete        = 0x1,		// Delete connection after child connections are closed. 
	TimeoutAutoReset		     = 0x2,		// Immediately attempt to reconnect on watchdog timeout
	TimeoutDelayAutoReset        = 0x3,		// Attempt to reconnect after predetermined delay period	
	TimeoutManualReset           = 0x4,		// Wait in time out state until the user calls EtIPResetConnection
}
EtIPWatchdogTimeoutAction;

typedef struct tagEtIPDeviceID			/* Could be used for device keying */
{
	unsigned short iVendorID;
    unsigned short iProductType;
    unsigned short iProductCode;
    unsigned char  bMajorRevision;
    unsigned char  bMinorRevision;
}
EtIPDeviceID;

typedef struct tagEtIPConnectionConfig							
{
	int			   nInstance;									/* Instance of the Connection object. Will be between 1 and MAX_CONNECTIONS */
	BOOL           bOriginator;									/* Indicates whether this stack is the originator of the connection */
	char		   connectionPath[MAX_CONNECTION_PATH_SIZE];	/* Null terminated connection path string: the IP address (i.e."216.233.160.112") or network recognizable path (i.e."Jornada720A") */
	unsigned char  bSlot;										/* Target device slot number starting with 0 if applicable, or INVALID_SLOT_INDEX if not applicable */
	unsigned short iConnectionFlag;								/* Should be set to INVALID_CONNECTION_POINT when calling EtIPOpenConnection. EtIPGetConnectionFlag() returns valid flag compatible with RSNetworx format */			
	unsigned short iConfigConnInstance;							/* Target device object (usually Assembly) instance. Could be INVALID_INSTANCE. */	
	unsigned short iProducingConnPoint;							/* Target device object (usually Assembly) Producing by the Target (Out) connection point. Could be INVALID_CONN_POINT. */
	unsigned short iConsumingConnPoint;							/* Target device object (usually Assembly) Consuming by the Target (In) connection point. Could be INVALID_CONN_POINT. */	
	char		   connectionTag[MAX_REQUEST_TAG_SIZE];			/* Optional connection tag. Could be used instead of specifying the connection points for some devices. */
	unsigned short iConnectionTagSize;							/* Connection tag size. Must be set to 0 if connection tag is not used */
	unsigned long  lProducingDataRate;							/* Producing by this stack I/O data rate in microseconds */
	unsigned long  lConsumingDataRate;							/* Consuming by this stack I/O data rate in microseconds */
	unsigned char  bProductionOTInhibitInterval;				/* The scanner (originator) will not produce with the higher rate than inhibit interval. Value of 0 disables it. */
	unsigned char  bProductionTOInhibitInterval;				/* The adapter (target) will not produce with the higher rate than inhibit interval. Value of 0 disables it. */
	BOOL		   bOutputRunProgramHeader;						/* Set to TRUE if the first 4 bytes of the output data represent Run/Program header */
	unsigned short iOutputDataOffset;							/* Output I/O data offset in bytes in the output Assembly data table starting with 0  */
	unsigned short iOutputDataSize;								/* Output I/O size in bytes in the output Assembly data table (not including data sequence count and not including Run/Idle information) */
	BOOL		   bInputRunProgramHeader;						/* Set to TRUE if the first 4 bytes of the input data represent Run/Program header */
	unsigned short iInputDataOffset;							/* Input I/O data offset in bytes in the input Assembly data table starting with 0 */	
	unsigned short iInputDataSize;								/* Input I/O size in bytes in the input Assembly data table (including the Run/Idle information, but not including data sequence count) */	
    unsigned short iProducingConnectionType;					/* Producing by this stack connection type. Must be PointToPoint when Originator and could be either PointToPoint or Multicast when Target. */
	unsigned short iConsumingConnectionType;					/* Consuming by this stack connection type. Could be either PointToPoint or Multicast when Originator and must be PointToPoint when Target. */    
	unsigned short iProducingPriority;							/* Must be one of the EtIPConnectionPriority values */    
	unsigned short iConsumingPriority;							/* Must be one of the EtIPConnectionPriority values */    
	unsigned char  bTransportClass;								/* Indicates whether transport is Class 1 or Class 3. Must be one of the EtIPConnectionClass values. */
	unsigned char  bTransportType;								/* One of the EtIPTransportType enumeration values */
	unsigned char  bTimeoutMultiplier;							/* Timeout multiplier of 0 indicates that timeout should be 4 times larger than the consuming data rate, 1 - 8 times larger, 2 - 16 times, and so on */
	unsigned short iWatchdogTimeoutAction;						/* Watchdog Timeout Action from the EtIPWatchdogTimeoutAction enumeration */
	unsigned long  lWatchdogTimeoutReconnectDelay;				/* If Watchdog Timeout Action is TimeoutDelayAutoReset, Watchdog Timeout Reconnect Delay specifies reconnect delay in milliseconds */	
	EtIPDeviceID   deviceId;									/* Optional target device identification. All members must be set to 0 to turn off identification. If set, it will be specified in the Forward Open Connection Path. */	
	char		   connectionName[MAX_CONNECTION_NAME_SIZE];	/* Optional user recognizable connection name. */
	unsigned short iConnectionNameSize;							/* Connection name size. Must be set to 0 if connection name is not used */
	unsigned char  moduleConfig1[MAX_MODULE_CONFIG_SIZE];		/* Optional first part of the target device specific configuration */
	unsigned short iModuleConfig1Size;							/* The size of the first part of the target device specific configuration. Must be set to 0 if configuration is not passed to the target device */
	unsigned char  moduleConfig2[MAX_MODULE_CONFIG_SIZE];		/* Optional second part of the target device specific configuration */
	unsigned short iModuleConfig2Size;							/* The size of the second part of the target device specific configuration. Must be set to 0 if configuration is not passed to the target device */	
	EtIPObjectRequest request;									/* Class3 request for the Class3 connections. Not used for Class1 connections. */
}
EtIPConnectionConfig;

#define MAX_PRODUCT_NAME_SIZE	33					/* Maximum number of 32 characters and the ending 0 */

typedef struct tagEtIPIdentityInfo
{
   unsigned short  iVendor;
   unsigned short  iProductType;
   unsigned short  iProductCode;
   unsigned char   bMajorRevision;
   unsigned char   bMinorRevision;
   unsigned short  iStatus;
   unsigned long   lSerialNumber;   
   char		       productName[MAX_PRODUCT_NAME_SIZE];
}
EtIPIdentityInfo;


typedef void LogEventCallbackType( INT32 nEvent, INT32 nParam );		/* Callback function to be called in case of any notification or error event */


/*---------------------------------------------------------------------------
** EtIPAdapterStart( )
**
** Start executing the Adapter.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API void EtIPAdapterStart();

/*---------------------------------------------------------------------------
** EtIPAdapterStop( )
**
** Stop running the Adapter. Should always be called before unloading the DLL.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API void EtIPAdapterStop();						

/*---------------------------------------------------------------------------
** EtIPRegisterEventCallBack( )
**
** Register client application callback function
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API void EtIPRegisterEventCallBack( LogEventCallbackType *pfnLogEvent );


/*---------------------------------------------------------------------------
** EtIPGetNumConnections( )
**
** Returns total number of connections in the state other than 
** ConnectionNonExistent.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetNumConnections();
	
/*---------------------------------------------------------------------------
** EtIPGetConnectionInstances( )
**
** Returns total number of connections in the state other than 
** ConnectionNonExistent.
** pnConnectionIDArray should point to the array of MAX_CONNECTIONS integers.
** It will receive the connection instances.
** Example:
**
** int ConnectionInstanceArray[MAX_CONNECTIONS];
** int nNumConnections = EtIPGetConnectionInstances(ConnectionInstanceArray);
**
** After return ConnectionInstanceArray[0] will have the first connection instance,
** ConnectionInstanceArray[1] will have the second connection instance ...
** ConnectionInstanceArray[nNumConnections-1] will have the last connection 
** instance.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetConnectionInstances(int *pnConnectionInstanceArray);

/*---------------------------------------------------------------------------
** EtIPGetConnectionState( )
**
** Return the state of the particular connection from the EtIPConnectionState
** enumeration.
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances() and should be between 1 and MAX_CONNECTIONS. 
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetConnectionState(int nConnectionInstance);					

/*---------------------------------------------------------------------------
** EtIPGetConnectionConfig( )
**
** Return configuration for the particular connection.
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pConfig is the pointer to the EtIPConnectionConfig structure allocated
** by the calling application. This structure is filled by this stack
** in case of a successful response.
** Returns 0 in case of a success, or ERR_INVALID_CONNECTION_INSTANCE_SPECIFIED
** if nConnectionInstance is out of the connection instance range: from 1 to 
** MAX_CONNECTIONS, or is in ConnectionNonExistent state.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetConnectionConfig(int nConnectionInstance, EtIPConnectionConfig* pConfig);	


/*---------------------------------------------------------------------------
** EtIPGetInputData( )
**
** Return input data table contents for some or all connections.
** pBuf is the buffer allocated by the calling application to be filled
** with the input table data. 
** nOffset is the starting offset in the input data table we should start 
** copying from.
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetInputData(unsigned char* pBuf, int nOffset, int nLength);	

/*---------------------------------------------------------------------------
** EtIPSetInputData( )
**
** Set input data table contents for some or all connections.
** pBuf contains the input data that should be used to populate input data
** table. 
** nOffset is the starting offset in the input data table we should start 
** copying to.
** nLength is the size of the input data in bytes.
** Returns the actual size of the data copied from the pBuf buffer to the
** input data table.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPSetInputData(unsigned char* pBuf, int nOffset, int nLength);

/*---------------------------------------------------------------------------
** EtIPGetOutputData( )
**
** Return output data table contents for some or all connections.
** pBuf is the buffer allocated by the calling application to be filled
** with the output table data. 
** nOffset is the starting offset in the output data table we should start 
** copying from.
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetOutputData(unsigned char* pBuf, int nOffset, int nLength);	

/*---------------------------------------------------------------------------
** EtIPSetOutputData( )
**
** Set output data table contents for some or all connections.
** pBuf contains the output data that should be used to populate output data
** table. 
** nOffset is the starting offset in the output data table we should start 
** copying to.
** nLength is the size of the output data in bytes.
** Returns the actual size of the data copied from the pBuf buffer to the
** output data table.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPSetOutputData(unsigned char* pBuf, int nOffset, int nLength);

/*---------------------------------------------------------------------------
** EtIPGetConnectionInputData( )
**
** Return input I/O data including possible 4 bytes of Run/Idle status preceding 
** the data for the particular Class1 connection.
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pBuf is the buffer allocated by the calling application to be filled
** with the input I/O data. 
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer, 
** or 0 if the nConnectionInstance is invalid or not in the 
** ConnectionEstablished state.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetConnectionInputData(int nConnectionInstance, unsigned char* pBuf, int nLength);

/*---------------------------------------------------------------------------
** EtIPSetConnectionInputData( )
**
** Set input I/O data for the particular connection. Include 
** 4 bytes of Run/Idle status if appropriate.
** nConnectionIntance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pBuf contains the input data.
** nLength is the size of input data in bytes.
** Returns the actual size of the data copied from the pBuf buffer,
** or 0 if the nConnectionInstance is invalid or not in the 
** ConnectionEstablished state.
** This function is ignored for Class3 connections.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPSetConnectionInputData(int nConnectionInstance, unsigned char* pBuf, int nLength);

/*---------------------------------------------------------------------------
** EtIPGetConnectionOutputData( )
**
** Return output I/O data. 
** nConnectionInstance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pBuf is the buffer allocated by the calling application to be filled
** with the output I/O data. 
** nLength is the size of the preallocated buffer.
** Returns the actual size of the data copied into the pBuf buffer, 
** or 0 if the nConnectionInstance is invalid or not in the 
** ConnectionEstablished state.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetConnectionOutputData(int nConnectionInstance, unsigned char* pBuf, int nLength);


/*---------------------------------------------------------------------------
** EtIPSetConnectionOutputData( )
**
** Set output I/O data for the particular connection. 
** nConnectionIntance is the connection instance obtained by calling 
** EtIPGetConnectionInstances().
** pBuf contains the output data that should be used to send to the 
** target device.
** nLength is the size of output data in bytes.
** Returns the actual size of the data copied from the pBuf buffer,
** or 0 if the nConnectionInstance is invalid or not in the 
** ConnectionEstablished state.
** This function is ignored for Class3 connections.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPSetConnectionOutputData(int nConnectionInstance, unsigned char* pBuf, int nLength);

/*---------------------------------------------------------------------------
** EtIPSendUnconnectedRequest( )
**
** Send either UCMM or Unconnected Send depending on whether szNetworkPath 
** specifies local or remote target.
** EtIPGetResponse will be used to get the response.
** szNetworkPath is zero terminated network path string (i.e."216.233.160.112" 
** or "216.233.160.112,2,216.233.160.145" or "216.233.160.112,1,0" or "Jornada720A").
** pRequest points to a structure containing the request parameters.
** Check the return value. If it's greater than or equal to REQUEST_INDEX_BASE,
** then it is a request Id to be used when calling EtIPGetResponse.
** If it is less than REQUEST_INDEX_BASE then it's one of the 
** errors listed above.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPSendUnconnectedRequest(const char* szNetworkPath, EtIPObjectRequest* pRequest );

/*---------------------------------------------------------------------------
** EtIPGetResponse( )
**
** Get a response for the previously transferred UCMM or an
** Unconnected Send message.
** nRequestId is a request Id returned from the previous call to the  
** EtIPSendUnconnectedRequest function. 
** pResponse contain response data in case of a success and
** extended error indormation in case of an error.
** Returns 0 if able to get the response or one of the error constants 
** listed above. If return value is 0 it does not guarantee the successful
** CIP response. You need to check bGeneralStatus member of pResponse to 
** determine whether the target returned success or an error. bGeneralStatus
** of 0 indicates a success. If bGeneralStatus is other than 0, iExtendedStatus
** will contain an optional extended error code and errorDescription will
** contain the error text corresponding to the general and extended error codes.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPGetResponse(int nRequestId, EtIPObjectResponse* pResponse );


/*---------------------------------------------------------------------------
** EtIPRegisterObjectsForClientProcessing( )
**
** Provides the list of the objects that will be processed by the client 
** application. When the request is received for one of these objects
** the NM_CLIENT_OBJECT_REQUEST_RECEIVED notification message will be 
** sent to the client app. The client application can use EtIPGetClientRequest()
** and EtIPSendClientResponse() after that to get the request and 
** send the response.
** pClassNumberList is the pointer to the integer array. Each integer represent
** the class number of the object that will be proccessed by the client 
** application.
** nNumberOfClasses is the number of classes in the list.
** pServiceNumberList is the pointer to the integer array. Each integer represent
** the service that will be proccessed by the client application.
** nNumberOfServices is the number of the services in the list.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API void EtIPRegisterObjectsForClientProcessing(int* pClassNumberList, int nNumberOfClasses,
								int* pServiceNumberList, int nNumberOfServices);


/*---------------------------------------------------------------------------
** EtIPGetClientRequest( )
**
** Gets the pending request that should be processed by the client
** application. The objects for the client application processing must
** be registered in advance by calling EtIPRegisterObjectsForClientProcessing()
** function.
** The client application will get NM_CLIENT_OBJECT_REQUEST_RECEIVED callback
** with a new request Id. The same request Id should be passed when calling
** EtIPGetClientRequest() function.
** pRequest is the pointer to the EtIPObjectRequest structure that will have 
** the request parameters on return.
** Returns 0 if successful or ERR_OBJECT_REQUEST_UNKNOWN_INDEX if there is no 
** pending request with this Id.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API INT32 EtIPGetClientRequest( INT32 nRequestId, EtIPObjectRequest* pRequest );


/*---------------------------------------------------------------------------
** clientSendClientResponse( )
**
** Send a response to the previously received request registered to be
** processed by the client application.
** The client application previously polled the request by calling
** EtIPGetClientRequest() function and now sends the response to that
** request.
** Returns 0 if successful or ERR_OBJECT_REQUEST_UNKNOWN_INDEX if there is no 
** pending request with this Id.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API int EtIPSendClientResponse(int nRequestId, EtIPObjectResponse* pResponse);

/*---------------------------------------------------------------------------
** EtIPGetIdentityInfo( )
**
** Returns identity information.
** The client application is responsible for allocating EtIPIdentityInfo
** structure and passing its pointer with the EtIPGetIdentityInfo function
** call. On return the structure fill be filled with identity information.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API void EtIPGetIdentityInfo(EtIPIdentityInfo* pInfo);

/*---------------------------------------------------------------------------
** EtIPSetIdentityInfo( )
**
** Updates identity information.
** pInfo is the pointer to the EtIPIdentityInfo structure with the new
** identity information.
** To change only some of the identity parameters the client application
** can call EtIPGetIdentityInfo, modify only appropriate structure members
** and then call EtIPSetIdentityInfo.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API void EtIPSetIdentityInfo(EtIPIdentityInfo* pInfo);

/*---------------------------------------------------------------------------
** EtIPGetHostIPAddress( )
**
** Returns a string with the host IP address.
** szHostIPAddress should point to the preallocated string where IP address
** should be stored.
** Returns TRUE on success, or FALSE if unable to obtain the IP address.
**---------------------------------------------------------------------------
*/
ETIPADAPTER_API BOOL EtIPGetHostIPAddress(char* szHostIPAddress);


