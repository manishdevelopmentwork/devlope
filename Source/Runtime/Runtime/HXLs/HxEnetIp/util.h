/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** UTIL.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** General utilities.
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef UTIL_H
#define UTIL_H

/* The following macros are required for certain types of processors (i.e. ARM).
   They replace word and double word addressing with individual byte handling */

/* Please note that we assume that over the wire the data always is transferred
   in little-endian format, even if target and/or originator are using big-endian */

#define MEMORY_POOL_SIZE		0xffff

#define INVALID_MEMORY_OFFSET	0xffff

#define MEM_PTR(x)		        (&gMemoryPool[x])		

#define IS_TICK_GREATER(tick1, tick2)  ( (tick1 < 0xffff && tick2 > 0xffff0000) ? TRUE : ( (tick2 < 0xffff && tick1 > 0xffff0000) ? FALSE : (tick1 > tick2) ) )
#define MAX_TICK(tick1, tick2)	( IS_TICK_GREATER(tick1, tick2) ? tick1 : tick2 )

extern UINT8  gMemoryPool[/*MEMORY_POOL_SIZE*/];
extern UINT16 giMemoryPoolOffset;

extern UINT16 utilAddToMemoryPool( UINT8* data, UINT16 iDataSize );
extern void utilRemoveFromMemoryPool( UINT16* piDataOffset, UINT16* piDataSize );
extern void utilResetMemoryPoolOffset( UINT16* piDataOffset, UINT16* piDataSize, UINT8* data, UINT16 iDataSize );
extern UINT32 utilAddrFromPath( const char* szPath );
extern BOOL utilParseNetworkPath( char* szNetworkPath, BOOL* pbLocal, char* extendedPath, UINT16* piExtendedPathLen );
extern UINT32 utilGetUniqueID();
extern BOOL   utilIsIDUnique(UINT32 lVal);

#endif /* #ifndef UTIL_H */
