
#include "intern.hpp"

#include "emsontcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Driver
//

// Instantiator

INSTANTIATE(CEmersonEPTCPDriver);

// Constructor

CEmersonEPTCPDriver::CEmersonEPTCPDriver(void)
{
	m_Ident		= DRIVER_ID;

	m_uKeep		= 0;

	m_ReadDelay	= 0;

	m_uWriteErrCt	= 0;
	}

// Destructor

CEmersonEPTCPDriver::~CEmersonEPTCPDriver(void)
{
	}

// Configuration

void MCALL CEmersonEPTCPDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) m_ReadDelay = GetWord(pData);
	}

// Management

void MCALL CEmersonEPTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEmersonEPTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonEPTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_bType		= GetByte(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonEPTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonEPTCPDriver::Ping(void)
{
//	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) return CCODE_ERROR;

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SPACE_HOLD;
		Addr.a.m_Offset = 1;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		InitDoNotCall();

		m_fInPing = TRUE;

		CCODE c = DoWordRead(Addr, Data, 1);

		m_fInPing = FALSE;

		return c;
//		}

//	return CCODE_ERROR;
	}

CCODE MCALL CEmersonEPTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	if( m_ReadDelay ) Sleep(m_ReadDelay);

	CAddress CAdd;

	if( !ConvertAddress(Addr, &CAdd) ) {

		for(UINT k = 0; k < uCount; k++ ) pData[k] = 0;

		return uCount;
		}

	UINT uCt = uCount;

	if( m_pCtx->m_bType != DVMC ) {

		m_pCtx->m_fInDNCList = FALSE;

		UINT uMax = CAdd.a.m_Type == WW ? EPMAXWORDS : CAdd.a.m_Type == LL ? EPMAXLONG : EPMAXBITS;

		uCt = DoNotCall(CAdd.a.m_Offset, min(uCt, uMax));

		if( !uCt ) { // this address is in list

			if( !ReadDNCAnyway(CAdd.a.m_Offset) ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

			m_pCtx->m_fGetSingles = FALSE;

			m_pCtx->m_fInDNCList  = TRUE;

			uCt = 1;
			}
		}

	CCODE ccReturn;

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				ccReturn = DoWordRead(CAdd, pData, uCt);
				}

			else
				ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_ANALOG:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				ccReturn = DoWordRead(CAdd, pData, uCt);
				}

			else
				ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_HOLD32:
			
			ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_ANALOG32:
			
			ccReturn = DoLongRead(CAdd, pData, uCt);
			break;

		case SPACE_OUTPUT:
			
			ccReturn = DoBitRead (CAdd, pData, uCt);
			break;

		case SPACE_INPUT:
			
			ccReturn = DoBitRead (CAdd, pData, uCt);
			break;

		default: return CCODE_ERROR | CCODE_HARD;
		}

	if( !(ccReturn & CCODE_ERROR) ) {

		if( ccReturn > 1 ) m_pCtx->m_fGetSingles = FALSE;

		if( m_pCtx->m_fInDNCList ) RemoveDNC(CAdd.a.m_Offset);
		}

	return ccReturn;
	}

CCODE MCALL CEmersonEPTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	CAddress CAdd;

	if( !ConvertAddress(Addr, &CAdd) ) return uCount; // string item, allocation exceeded

	if( m_pCtx->m_bType != DVMC ) {

		if( !DoNotCall(CAdd.a.m_Offset, 1) ) return 1; // previously logged invalid address
		}

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			if( CAdd.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(CAdd, pData, 1);
				}

			return DoLongWrite(CAdd, pData, 1);

		case SPACE_HOLD32:
			
			return DoLongWrite(CAdd, pData, 1);

		case SPACE_OUTPUT:
			
			return DoBitWrite (CAdd, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL CEmersonEPTCPDriver::ConvertAddress(AREF Addr, CAddress *pAddr)
{
	UINT uTab  = Addr.a.m_Table;
	UINT u;

	if( m_pCtx->m_bType == DVMC ) {

		switch( uTab ) {

			case MCO:	uTab = SPACE_OUTPUT;	break;
			case MCI:	uTab = SPACE_INPUT;	break;
			case MCH:	uTab = SPACE_HOLD;	break;
			default:	return FALSE;
			}

		pAddr->a.m_Table  = uTab;

		pAddr->a.m_Offset = Addr.a.m_Offset + 1; // MC Config addr are 0 based
		pAddr->a.m_Type   = Addr.a.m_Type;

		return TRUE;
		} 

	if( IsGenericAddress(uTab) ) {

		u = Addr.a.m_Offset;

		switch( uTab ) {

			case GMBO:	uTab = SPACE_OUTPUT;	break;

			case GMBI:	uTab = SPACE_INPUT;	break;

			case GMBA:
			case GMBB:	uTab = SPACE_ANALOG;	break;

			case GMBC:	uTab = SPACE_ANALOG32;	break;

			case GMBD:
			case GMBE:	uTab = SPACE_HOLD;	break;

			case GMBF:	uTab = SPACE_HOLD32;	break;
			}
		}

	else {
		if( StringAllocBad(Addr) ) return FALSE;

		if( !GetAddressFromItem(uTab, Addr.a.m_Offset, &u) ) u = Addr.a.m_Offset;

		switch( u/10000 ) {

			case 0:	uTab = SPACE_OUTPUT;	break;
			case 1: uTab = SPACE_INPUT;	break;
			case 3: uTab = SPACE_ANALOG;	break;

			case 4:
			case 5:
				uTab = SPACE_HOLD;

				if( IsCommander() && Addr.a.m_Type == addrLongAsLong ) {

					uTab = SPACE_HOLD32;
					}

				break;

			default:			return FALSE;
			}
		}

	pAddr->a.m_Table  = uTab;
	pAddr->a.m_Extra  = 0;
	pAddr->a.m_Type   = Addr.a.m_Type;
	pAddr->a.m_Offset = u % 10000;

	return TRUE;
	}

BOOL CEmersonEPTCPDriver::GetAddressFromItem(UINT uTable, UINT uOffset, UINT * pAddress)
{
	UINT uBase = 0;

	switch( uTable ) {

		case IAC:
			uBase = 43006;
			break;

		case ICR:
			uBase = 43014;
			break;

		case IXC:
			uBase = 43013;
			break;

		case IDC:
			uBase = 43008;
			break;

		case IDS:
			uBase = 43002;
			break;

		case IDW:
			uBase = 43012;
			break;

		case IXT:
			uBase = 43001;
			break;

		case IXV:
			uBase = 43004;
			break;

		case RGO:
			uBase = 43010;
			break;

		case CNX:
			uBase = 43015;
			break;

		case FPC:
			*pAddress = 31002 + ( (10 - uOffset) * 4 );
			return TRUE;

		case FPT:
			*pAddress = 31003 + ( (10 - uOffset) * 4 );
			return TRUE;

		case FTY:
			*pAddress = 31001 + ( (10 - uOffset) * 4 );
			return TRUE;

		case M15:
		case M16:
		case M17:
			if( IsCommander() ) {

				*pAddress = uOffset + ((uTable + MOFFSET) * 100);

				return TRUE;
				}

			return FALSE;
		}

	if( uBase ) {

		*pAddress = uBase + (uOffset * 25);
		return TRUE;
		}

	return FALSE;
	}

// String Item Handling
BOOL CEmersonEPTCPDriver::StringAllocBad(AREF Addr)
{
	if( m_pCtx->m_bType == DVEPB || m_pCtx->m_bType == DVEPI ) {

		UINT uOff = Addr.a.m_Offset;

		switch( Addr.a.m_Table ) {

			case TFPN:	return uOff > OFPNL;
			case TBPN:	return uOff > OBPNL;
			case TFRB:	return uOff > OFRBL;
			case TFRO:	return uOff > OFROL;
			case TBRS:	return uOff > OBRSL;
			case TDAN:	return uOff > ODANL;
			case TUUS:	return uOff > OUUSL;
			case TUDM:	return uOff > OUDML;
			case TPSN:	return uOff > OPSNL;
			}
		}

	return FALSE;
	}

// Frame Building

void CEmersonEPTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);

	AddByte(bOpcode);
	}

void CEmersonEPTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CEmersonEPTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEmersonEPTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CEmersonEPTCPDriver::SendFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for(UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEmersonEPTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

//**/		for( UINT k = uPtr; k < uSize + uPtr; k++ ) AfxTrace1("<%2.2x>", m_bRx[k] );

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CEmersonEPTCPDriver::Transact(BOOL fIgnore)
{
	m_bRx[0] = 0xFF;
	m_bRx[1] = 0xFE;

	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CEmersonEPTCPDriver::CheckFrame(void)
{
	if( !(m_bRx[1] & 0x80) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonEPTCPDriver::HasReadException(void)
{
	return m_bRx[1] & 0x80;
	}

// Read Handlers

CCODE CEmersonEPTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:
			StartFrame(0x03);
			break;

		case SPACE_ANALOG:
			StartFrame(0x04);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(AdjustAddress(Addr, &uCount));

	AddWord(WORD(uCount));

	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			WORD x   = PU2(m_bRx + 3)[n];

			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return m_fInPing ? CCODE_ERROR : CheckDoNotCall(Addr, uCount);
	}

CCODE CEmersonEPTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable    = Addr.a.m_Table;

	BOOL fSingle32 = uTable == SPACE_HOLD32 || uTable == SPACE_ANALOG32;

	BOOL fSP       = IsCommander();

	switch( uTable ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			StartFrame(0x03);
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	UINT uAdd   = AdjustAddress(Addr, &uCount);

	UINT uEffCt = uCount;

	if( fSP && fSingle32 ) {

		uAdd   += 16384; // 32 bit holding registers get 4000 hex offset

		uEffCt *= 2; // each single address requests 2 words
		}

	else {
		uEffCt += 1; // 1 gets 1&2, 2 gets 2&3...
		}
		
	AddWord(uAdd);
	
	AddWord(uEffCt); // set 16 bit word count
	
	if( Transact(FALSE) ) {

		if( m_bRx[2] < uEffCt ) {

			uCount = m_bRx[2] / 2; // byte count returned
			}
		
		for( UINT n = 0; n < uCount; n++ ) {

			UINT u   = 3 + (fSingle32 && fSP ? 4 * n : 2 * n);

			DWORD x  = *(PU4(&m_bRx[u]));

			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return m_fInPing ? CCODE_ERROR : CheckDoNotCall(Addr, uCount);
	}

CCODE CEmersonEPTCPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;

		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(WORD(Addr.a.m_Offset - 1));

	AddWord(WORD(uCount));

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return m_fInPing ? CCODE_ERROR : CheckDoNotCall(Addr, uCount);
	}

// Write Handlers

CCODE CEmersonEPTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {

		if( uCount == 1 ) {

			StartFrame(6);

			AddWord(WORD(Addr.a.m_Offset - 1));

			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);

			AddWord(WORD(Addr.a.m_Offset - 1));

			AddWord(WORD(uCount));

			AddByte(BYTE(uCount * 2));

			for( UINT n = 0; n < uCount; n++ ) {

				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) || (m_uWriteErrCt++ > 3) ) {

			m_uWriteErrCt = 0;

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonEPTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable    = Addr.a.m_Table;

	BOOL fSingle32 = uTable == SPACE_HOLD32;
		
	if( fSingle32 || ( uTable == SPACE_HOLD ) ) {

		StartFrame(16);

		UINT uAdd = AdjustAddress(Addr, &uCount);

		if( fSingle32 && IsCommander() ) {

			uAdd += 16384;
			}
		
		AddWord(uAdd);
		
		AddWord(2 * uCount);
		
		AddByte(uCount * 4);

		for( UINT n = 0; n < uCount; n++ ) {

			AddLong(pData[n]);
			}

		if( Transact(TRUE) || (m_uWriteErrCt++ > 3) ) {

			m_uWriteErrCt = 0;

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonEPTCPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( uCount == 1 ) {

			StartFrame(5);

			AddWord(WORD(Addr.a.m_Offset - 1));

			AddWord(pData[0] ? WORD(0xFF00) : WORD(0x0000));
			}
		else {
			StartFrame(15);

			AddWord(WORD(Addr.a.m_Offset - 1));

			AddWord(WORD(uCount));

			AddByte(BYTE((uCount + 7) / 8));

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {

					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(BYTE(b));

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(BYTE(b));
				}
			}

		if( Transact(TRUE) || (m_uWriteErrCt++ > 3) ) {

			m_uWriteErrCt = 0;

			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Helpers
UINT	CEmersonEPTCPDriver::AdjustAddress(AREF Addr, UINT *pCount)
{
	if( m_fAdjustedAddr ) *pCount = 1;

	m_fAdjustedAddr = 0;

	return Addr.a.m_Offset - 1;
	}

BOOL	CEmersonEPTCPDriver::IsGenericAddress(UINT uTable)
{
	return uTable >= GMBO && uTable <= GMBF;
	}

BOOL	CEmersonEPTCPDriver::IsCommander(void)
{
	return m_pCtx->m_bType >= DVSP && m_pCtx->m_bType <= DVMP;
	}

UINT	CEmersonEPTCPDriver::IsCmdrSolModule(UINT uTable)
{
	if( uTable >= M15 && uTable <= M17 ) return (15 + uTable - M15) * 100;

	return 0;
	}

// Do Not Call

UINT CEmersonEPTCPDriver::DoNotCall(UINT uAddress, UINT uCount)
{
	UINT uMinFound = 10000;

	for( UINT i = 0; i < DONOTCALLMAX; i++ ) {

		UINT uDNCAddr = m_pCtx->m_uDoNotCall[i];

		if( (uDNCAddr >= uAddress) && (uDNCAddr <= uAddress + uCount - 1) ) {

			MakeMin(uMinFound, uDNCAddr);
			}
		}

	return uMinFound == 10000 ? (m_pCtx->m_fGetSingles ? 1 : uCount) : uMinFound - uAddress;
	}

UINT CEmersonEPTCPDriver::FindDNCItem(UINT uAddress)
{
	for( UINT uPos = 0; uPos < DONOTCALLMAX; uPos++ ) {
		
		if( m_pCtx->m_uDoNotCall[uPos] == uAddress ) return uPos;
		}

	return DONOTCALLMAX;
	}

void CEmersonEPTCPDriver::RemoveDNC(UINT uAddress)
{
	UINT uFind = FindDNCItem(uAddress);

	if( uFind >= DONOTCALLMAX ) return; // address not found

	UINT *pAd = m_pCtx->m_uDoNotCall;
	UINT *pCt = m_pCtx->m_uDoNotCallCt;

	UINT uCpy = (DONOTCALLMAX - uFind - 1) * sizeof(UINT);

	memcpy( &pAd[uFind], &pAd[uFind+1], uCpy );
	memcpy( &pCt[uFind], &pCt[uFind+1], uCpy );

	uFind = 0;

	while( pAd[uFind] && uFind < DONOTCALLMAX ) {

		uFind++;
		}

	UINT uArrSz = (DONOTCALLMAX - uFind - 1) * sizeof(UINT);

	memset( &pAd[uFind], 0, uArrSz );
	memset( &pCt[uFind], 0, uArrSz );

	m_pCtx->m_uDoNotCallPos = uFind;
	}

void CEmersonEPTCPDriver::AddDoNotCall(UINT uAddress)
{
	if( DoNotCall(uAddress, 1) ) { // address is not in list

		UINT u = m_pCtx->m_uDoNotCallPos;

		m_pCtx->m_uDoNotCall[u]   = uAddress;

		m_pCtx->m_uDoNotCallCt[u] = m_pCtx->m_uInitCount;

		m_pCtx->m_uInitCount = (m_pCtx->m_uInitCount + 1) % 16;

		u = (u + 1) % DONOTCALLMAX;

		m_pCtx->m_uDoNotCallPos = u;
		}
	}

BOOL CEmersonEPTCPDriver::ReadDNCAnyway(UINT uAddress)
{
	UINT uFind = FindDNCItem(uAddress);

	if( uFind >= DONOTCALLMAX ) return TRUE;

	m_pCtx->m_uDoNotCallCt[uFind] = (m_pCtx->m_uDoNotCallCt[uFind] + 1) % 16;

	return !m_pCtx->m_uDoNotCallCt[uFind];
	}

CCODE CEmersonEPTCPDriver::CheckDoNotCall(AREF Addr, UINT uCount)
{
	if( HasReadException() && (m_bRx[2] != INVALIDADDRESS) ) {

		return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
		}

	if( m_bRx[0] == 0xFF && m_bRx[1] == 0xFE ) { // No Response

		return CCODE_ERROR;
		}

	DWORD    Data[1];

	CAddress CAdd;

	CAdd.a.m_Table  = Addr.a.m_Table;
	CAdd.a.m_Offset = Addr.a.m_Offset;
	CAdd.a.m_Type   = Addr.a.m_Type;

	CCODE c = CCODE_ERROR;

	if( uCount > 1 ) {

		switch( Addr.a.m_Type ) {

			case addrWordAsWord:	c = DoWordRead(CAdd, Data, 1);	break;
			case addrLongAsLong:	c = DoLongRead(CAdd, Data, 1);	break;
			case addrBitAsBit:	c = DoBitRead(CAdd, Data, 1);	break;
			}

		if( c == 1 ) { // This address is valid

			m_pCtx->m_fGetSingles = TRUE; // Read one at a time

			return CCODE_ERROR; // reread this address
			}
		}

	if( uCount == 1 ) {

		CAdd.a.m_Table  = SPACE_HOLD;
		CAdd.a.m_Offset = 1;
		CAdd.a.m_Type   = addrWordAsWord;

		m_fInPing = TRUE;

		if( DoWordRead(CAdd, Data, 1) != 1 ) return Ping(); // disconnect

		m_fInPing = FALSE;

		AddDoNotCall(Addr.a.m_Offset); // invalid address to add to list

		m_pCtx->m_fGetSingles = FALSE;

		if( m_pCtx->m_fInDNCList ) return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
		}

	return 1; // pretend valid read was done
	}

void CEmersonEPTCPDriver::InitDoNotCall(void)
{
	m_pCtx->m_fGetSingles	= FALSE;

	m_pCtx->m_fInDNCList	= FALSE;

	m_pCtx->m_uDoNotCallPos = 0;

	m_pCtx->m_uInitCount    = 0;

	UINT uArrSz = DONOTCALLMAX * sizeof(UINT);

	memset( m_pCtx->m_uDoNotCall,   0, uArrSz );

	memset( m_pCtx->m_uDoNotCallCt, 0, uArrSz );
	}

// Socket Management

BOOL CEmersonEPTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonEPTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		}

	return FALSE;
	}

void CEmersonEPTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
