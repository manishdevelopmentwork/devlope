
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlFilter;

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Page
//

class CSqlFilterPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSqlFilterPage(CSqlFilter *pFilter);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CSqlFilter * m_pFilter;
	};

// End of File
