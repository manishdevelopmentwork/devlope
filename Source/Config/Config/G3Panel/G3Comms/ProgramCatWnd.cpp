
#include "intern.hpp"

#include "ProgramCatWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"
#include "ProgramItemView.hpp"
#include "ProgramNavTreeWnd.hpp"
#include "ProgramManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Category Window
//

// Dynamic Class

AfxImplementDynamicClass(CProgramCatWnd, CViewWnd);

// Constructors

CProgramCatWnd::CProgramCatWnd(void)
{
	m_pNavTree  = NULL;

	m_pItemView = NULL;

	m_pManager  = NULL;
	
	m_fSemi     = FALSE;
	
	m_fDisable  = FALSE;
	
	m_fPend     = FALSE;

	m_Accel.Create(L"ProgramCatAccel");
	}

// Destructors

CProgramCatWnd::~CProgramCatWnd(void)
{
	}

// Attributes

// Operations

void CProgramCatWnd::SetNavTree(CProgramNavTreeWnd *pNavTree)
{
	m_pNavTree = pNavTree;
	}

void CProgramCatWnd::SetItemView(CProgramItemView *pItemView)
{
	m_pItemView = pItemView;
	}

void CProgramCatWnd::OnReload(CProgramItem *pProg)
{
	if( m_pItemView ) {
		
		if( m_pItemView->GetProgram() == pProg ) {

			m_pItemView->Reload();
			}
		}

	m_System.ItemUpdated(pProg, updateProps);
	}

// Overridables

void CProgramCatWnd::OnAttach(void)
{
	m_pManager = (CProgramManager *) m_pItem;

	m_pManager->SetCatWnd(this);
	}

// Message Map

AfxMessageMap(CProgramCatWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchCommandType(IDM_GLOBAL, OnGlobalCommand)
	
	AfxDispatchControlType(IDM_PROGRAM, OnProgramControl)
	AfxDispatchCommandType(IDM_PROGRAM, OnProgramCommand)

	AfxMessageEnd(CProgramCatWnd)
	};

// Accelerators

void CProgramCatWnd::OnPostCreate(void)
{
	m_System.Bind();
	}

BOOL CProgramCatWnd::OnAccelerator(MSG &Msg)
{
	if( m_Accel.Translate(Msg) ) {

		return TRUE;
		}

	return FALSE;
	}

// Message Handlers

void CProgramCatWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	}

void CProgramCatWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {
		
		Menu.AppendMenu(CMenu(L"ProgramCatTool"));
		}
	}

// Command Handlers

BOOL CProgramCatWnd::OnGlobalCommand(UINT uID)
{
	if( uID == IDM_GLOBAL_SEMI_START ) {

		m_fSemi = TRUE;
		}

	if( uID == IDM_GLOBAL_DISABLE ) {

		m_fDisable = TRUE;
		}

	if( uID == IDM_GLOBAL_SEMI_END ) {

		if( !m_fDisable ) {
			
			if( m_fPend ) {

				m_pManager->MonitoredFilesChanged();

				m_fPend = FALSE;
				}
			}

		m_fSemi = FALSE;
		}

	if( uID == IDM_GLOBAL_ENABLE ) {

		if( !m_fSemi ) {
			
			if( m_fPend ) {

				m_pManager->MonitoredFilesChanged();

				m_fPend = FALSE;
				}
			}

		m_fDisable = FALSE;
		}

	return FALSE;
	}

BOOL CProgramCatWnd::OnProgramControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_PROGRAM_TRANSLATE ) {

		Src.EnableItem(m_pNavTree->IsProgramSelected() && m_pItemView && m_pItemView->CanTranslate());

		return TRUE;
		}

	if( uID == IDM_PROGRAM_CHANGED ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CProgramCatWnd::OnProgramCommand(UINT uID)
{
	if( uID == IDM_PROGRAM_TRANSLATE ) {

		if( m_pNavTree->IsProgramSelected() ) {

			m_pItemView->Translate();
			}

		return TRUE;
		}

	if( uID == IDM_PROGRAM_CHANGED ) {

		if( !m_fSemi && !m_fDisable ) {

			m_pManager->MonitoredFilesChanged();
			}
		else
			m_fPend = TRUE;

		return TRUE;
		}

	return FALSE;
	}

// End of File
