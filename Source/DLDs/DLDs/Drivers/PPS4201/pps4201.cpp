
#include "intern.hpp"

#include "pps4201.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Klockner Moeller PS4-201 Driver
//

// Instantiator

INSTANTIATE(CPPS4201Driver);

// Constructor

CPPS4201Driver::CPPS4201Driver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CPPS4201Driver::~CPPS4201Driver(void)
{
	}

// Configuration

void MCALL CPPS4201Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CPPS4201Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);	
	}
	
// Management

void MCALL CPPS4201Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CPPS4201Driver::Open(void)
{
	

	}

// Device

CCODE MCALL CPPS4201Driver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CPPS4201Driver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CPPS4201Driver::Ping(void)
{
	Sleep(200);

	m_uMarkerOffset = 0x4080;		// old ps306 default
	
	m_uMarkerEndRange = 0x160E;		// default experienced
	
	m_fMarkerReadReqrd = TRUE;

	if ( MarkerOffsetRead() ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR | CCODE_HARD;
		
	}

CCODE MCALL CPPS4201Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	MarkerOffsetRead();

	switch( Addr.a.m_Table ) {
	
		case 'C':				// special Misc value
			switch( Addr.a.m_Offset ) {

				case 1:
					pData[0] = m_uMarkerEndRange;
					break;

				default:
					pData[0] = m_uMarkerOffset;
					break;
				}
			
			return uCount;
			
		}
	
	// check end-of-range invalid reads

	MakeMin(uCount, 16);

	if(( m_uMarkerOffset + (Addr.a.m_Offset + 2 * uCount - 1)) > m_uMarkerEndRange ) {

		for( UINT uScan = 0; uScan < uCount; uScan++ ) {

			pData[uScan] = 0;
			}

		m_fMarkerReadReqrd = TRUE;
		
		return uCount;
		} 
	
	Clear();
	
	AddByte(PS_USER_NO);

	AddByte(0x82);

	AddByte((m_uMarkerOffset + Addr.a.m_Offset) / 256);
	AddByte((m_uMarkerOffset + Addr.a.m_Offset) % 256);
	
	AddByte((m_uMarkerOffset + (Addr.a.m_Offset + 2 * uCount - 1)) / 256);
	AddByte((m_uMarkerOffset + (Addr.a.m_Offset + 2 * uCount - 1)) % 256);

	for( UINT uRetry = 0; uRetry < RETRIES; uRetry++ ) {
		
		Send();

		UINT uRecv = Receive();

		if( uRecv == RX_FRAME ) {

			for( UINT uScan = 0; uScan < uCount; uScan++ ) {

				WORD wData = PWORD(m_bRxBuff)[uScan];

				pData[uScan] = SHORT(IntelToHost(wData));

				}

			return uCount;
			}
		}

	m_fMarkerReadReqrd = TRUE;

	return CCODE_ERROR;
	}

CCODE MCALL CPPS4201Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	MarkerOffsetRead();

	switch( Addr.a.m_Table ) {

		case 'C':				// special Misc value
			
			return uCount;
			
		}
	
	// check end-of-range overwrites

	MakeMin(uCount, 16);

	if(( m_uMarkerOffset + (Addr.a.m_Offset + 2 * uCount - 1)) > m_uMarkerEndRange ) {

		m_fMarkerReadReqrd = TRUE;
		
		return uCount;
		}

	Clear();
	
	AddByte(PS_USER_NO);

	AddByte(0x83);

	AddByte((m_uMarkerOffset + Addr.a.m_Offset) / 256);
	AddByte((m_uMarkerOffset + Addr.a.m_Offset) % 256);
	
	AddByte((m_uMarkerOffset + (Addr.a.m_Offset + 2 * uCount - 1)) / 256);
	AddByte((m_uMarkerOffset + (Addr.a.m_Offset + 2 * uCount - 1)) % 256);

	for( UINT uRetry = 0; uRetry < RETRIES; uRetry++ ) {
		
		Send();
	
		if( Receive() == RX_ACK )
			break;
		}

	if( uRetry < RETRIES ) {
	
		Clear();
		
		AddByte(PS_USER_NO);

		AddByte(0x14);
		
		AddByte(2 * uCount);
		
		for( UINT uScan = 0; uScan < uCount; uScan++) {

			AddWord(pData[uScan]);

			}
		
		for( UINT uRetry = 0; uRetry < RETRIES; uRetry++ ) {
		
			Send();
	
			if( Receive() == RX_ACK ) {

				return uCount;
				}
			}
		}

	m_fMarkerReadReqrd = TRUE;

	return CCODE_ERROR;
	}

// Implementation

void CPPS4201Driver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CPPS4201Driver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

void CPPS4201Driver::Clear(void)
{
	m_uPtr = 0;
	}
	
void CPPS4201Driver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr++] = bByte;
	}

void CPPS4201Driver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));

	}
	
void CPPS4201Driver::Send(void)
{
	m_pData->ClearRx();		// clear Rx buffer anew

	TxByte(0x0C);
	
	BYTE bCheck = 0;

	for( UINT uScan = 0; uScan < m_uPtr; uScan++ ) {
	
		TxByte(m_bTxBuff[uScan]);

		bCheck ^= m_bTxBuff[uScan];

		}

	TxByte(bCheck);

	}

WORD CPPS4201Driver::Receive(void)
{
	UINT uState = 0;
	
	BOOL fLoop = FALSE;
	
	UINT uLimit = 0;
	
	BYTE bCheck = 0; 

	SetTimer(FRAME_TIMEOUT);
	
	while( GetTimer() ) {
	
		UINT uByte = RxByte(GetTimer());

		if( uByte == NOTHING ) {
			
			Sleep(10);
			
			continue;
			}
		
		switch( uState ) {
		
			case 0:
				if( uByte == PS_ACK ) 
					return RX_ACK;
										
				if( uByte == PS_NAK )
					return RX_NAK;
					
				if( uByte == PS_STX )
					uState = 1;
				break;
				
			case 1:
				if( uByte == 0x03 ) {
					fLoop  = TRUE;
					uLimit = 5;
					m_uPtr   = 0;
					uState = 4;
					}
				else {
					fLoop  = FALSE;
					bCheck = uByte;
					uState = 2;
					}
				break;

			case 2:
				bCheck = uByte;
				uState = 3;
				break;
				
			case 3:
				bCheck = bCheck ^ uByte;
				uLimit = uByte;
				m_uPtr   = 0;
				uState = 4;
				break;
				
			case 4:
				bCheck = bCheck ^ uByte;

				m_bRxBuff[m_uPtr++] = uByte;
				
				if( m_uPtr == uLimit )
					uState = 5;
				break;
				
			case 5:
				if( fLoop )
					uState = 0;
				else {
					if( uByte == bCheck )
						return RX_FRAME;
					else
						return RX_ERROR;
					}
				break;
			}
		}

	return RX_NOTHING;
	}

BOOL CPPS4201Driver::MarkerOffsetRead()
/*

Validation will be made for the end of the retentive-data range (0x160E)....

TX: <C><7><82><0><40><0><4F><LRC>
RX: <C><0><14><10><6><D><0><0><0><0><0><0><0><0><16><E><0><0><0><0><17>

*/
{
	if( m_fMarkerReadReqrd ) {

		Clear();
	
		AddByte(PS_USER_NO);

		AddByte(0x82);
	
		AddByte(PS_MARKER_RANGE_START / 256);
		AddByte(PS_MARKER_RANGE_START % 256);

		AddByte(PS_MARKER_RANGE_END / 256);
		AddByte(PS_MARKER_RANGE_END % 256);

		for( UINT uRetry = 0; uRetry < RETRIES; uRetry++ ) {
		
			Send();

			UINT uRecv = Receive();

			if( uRecv == RX_FRAME ) {

				/*
				usual casting would be wrong (expects LO 1st)
				[6][D] would become 0x0D06 (right for data!)
				*/

				m_uMarkerOffset  = m_bRxBuff[0] * 256;
				m_uMarkerOffset += m_bRxBuff[1];

				m_uMarkerEndRange  = m_bRxBuff[10] * 256;
				m_uMarkerEndRange += m_bRxBuff[11];

				m_fMarkerReadReqrd = FALSE;

				return TRUE;
				}
			}
		}

	return FALSE;
	}
	
// End of File
