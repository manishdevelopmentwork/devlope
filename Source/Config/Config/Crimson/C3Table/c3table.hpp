
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Table Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3TABLE_HPP
	
#define	INCLUDE_C3TABLE_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3table.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3TABLE

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3table.lib")

#endif

// End of File

#endif
