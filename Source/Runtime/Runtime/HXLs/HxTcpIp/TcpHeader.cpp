
#include "Intern.hpp"

#include "TcpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PseudoHeader.hpp"

////////////////////////////////////////////////////////////////////////
//
// TCP Header Wrapper Class
//

// Attributes

BOOL CTcpHeader::TestChecksum(PSREF PS, UINT uSize)
{
	if( !CalcChecksum(PS.GetChecksum(), uSize) ) {

		return TRUE;
		}

	if( m_Checksum + PS.GetPrimeSum() == 0xFFFF ) {

		return TRUE;
		}

	return FALSE;
	}

UINT CTcpHeader::GetHeaderSize(void) const
{
	return (m_Offset * sizeof(DWORD));
	}

UINT CTcpHeader::GetOptionsSize(void) const
{
	return GetHeaderSize() - sizeof(ThisObject);
	}

UINT CTcpHeader::GetDataSize(void) const
{
	// We cache the size in the checksum field.

	return m_Checksum;
	}

UINT CTcpHeader::GetSymbols(void) const
{
	UINT uSize = GetDataSize();

	if( m_FlagSYN ) uSize += 1;

	if( m_FlagFIN ) uSize += 1;

	return uSize;
	}

UINT CTcpHeader::GetSymbolsNoFin(void) const
{
	UINT uSize = GetDataSize();

	if( m_FlagSYN ) uSize += 1;

	return uSize;
	}

BOOL CTcpHeader::GetMaxSegSize(WORD &wSize) const
{
	PBYTE pData = PBYTE(this) + sizeof(ThisObject);

	UINT  uLeft = GetOptionsSize();
		      
	while( uLeft && *pData ) {

		if( pData[0] == 0x01 ) {

			uLeft -= 1;

			pData += 1;

			continue;
			}

		if( pData[0] == 0x02 ) {

			wSize = MAKEWORD(pData[3], pData[2]);

			return TRUE;
			}

		uLeft -= pData[1];

		pData += pData[1];
		}

	return FALSE;
	}

// Operations

void CTcpHeader::Init(void)
{
	UINT uSize = sizeof(CTcpHeader);

	memset(this, 0, uSize);

	m_Offset = uSize / sizeof(DWORD);
	}

void CTcpHeader::AddMaxSegSize(CBuffer *pBuff, WORD wSize)
{
	PBYTE pb = pBuff->GetData();

	PBYTE p1 = pb + sizeof(ThisObject);

	PBYTE p2 = p1 + 4;

	UINT  nb = pBuff->GetSize() - sizeof(ThisObject);

	memmove(p2, p1, nb);

	pBuff->AddTail(4);

	p1[0] = 2;
	p1[1] = 4;
	p1[2] = HIBYTE(wSize);
	p1[3] = LOBYTE(wSize);

	m_Offset += 1;
	}

void CTcpHeader::AddChecksum(DWORD Load, UINT uSize)
{
	m_Checksum = 0x0000;

	m_Checksum = CalcChecksum(Load, uSize);
	}

void CTcpHeader::SetDataSize(UINT uSize)
{
	// Cache the size in the checksum field for now.

	m_Checksum = WORD(uSize);
	}

void CTcpHeader::PrintShort(void)
{
	AfxTrace( "LEN=%4.4u SEQ=%5.5u ACK=%5.5u WND=%5.5u ",
		  m_Checksum,
		  LOWORD(m_Seq),
		  LOWORD(m_Ack),
		  m_Wnd
		  );

	if( m_FlagURG ) AfxTrace("URG ");
	if( m_FlagACK ) AfxTrace("ACK ");
	if( m_FlagPSH ) AfxTrace("PSH ");
	if( m_FlagRST ) AfxTrace("RST ");
	if( m_FlagSYN ) AfxTrace("SYN ");
	if( m_FlagFIN ) AfxTrace("FIN ");

	AfxTrace("\n");
	}

void CTcpHeader::Print(void)
{
	TcpDebug(OBJ_TCP, LEV_INFO, "TCP packet\n");
	TcpDebug(OBJ_TCP, LEV_INFO, "  loc port = %u\n",  m_LocPort);
	TcpDebug(OBJ_TCP, LEV_INFO, "  rem port = %u\n",  m_RemPort);
	TcpDebug(OBJ_TCP, LEV_INFO, "  seq      = %lu\n", m_Seq);
	TcpDebug(OBJ_TCP, LEV_INFO, "  ack      = %lu\n", m_Ack);
	TcpDebug(OBJ_TCP, LEV_INFO, "  offset   = %u\n",  m_Offset);
	TcpDebug(OBJ_TCP, LEV_INFO, "  URG      = %u\n",  m_FlagURG);
	TcpDebug(OBJ_TCP, LEV_INFO, "  ACK      = %u\n",  m_FlagACK);
	TcpDebug(OBJ_TCP, LEV_INFO, "  PSH      = %u\n",  m_FlagPSH);
	TcpDebug(OBJ_TCP, LEV_INFO, "  RST      = %u\n",  m_FlagRST);
	TcpDebug(OBJ_TCP, LEV_INFO, "  SYN      = %u\n",  m_FlagSYN);
	TcpDebug(OBJ_TCP, LEV_INFO, "  FIN      = %u\n",  m_FlagFIN);
	TcpDebug(OBJ_TCP, LEV_INFO, "  wnd      = %u\n",  m_Wnd);
	TcpDebug(OBJ_TCP, LEV_INFO, "  check    = %u\n",  m_Checksum);
	TcpDebug(OBJ_TCP, LEV_INFO, "  urgent   = %u\n",  m_Urgent);
	}

// Implementation

WORD CTcpHeader::CalcChecksum(DWORD Load, UINT uSize) const
{
	return Checksum(PBYTE(this), uSize, Load);
	}

// End of File
