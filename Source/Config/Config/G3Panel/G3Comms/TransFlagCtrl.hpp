
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TransFlagCtrl_HPP

#define INCLUDE_TransFlagCtrl_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangManager;

//////////////////////////////////////////////////////////////////////////
//
// Translation Flag Control
//

class CTransFlagCtrl : public CCtrlWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTransFlagCtrl(void);

		// Operations
		void SetFlag(CLangManager *pLang, UINT uFlag);

	protected:
		// Data Members
		CLangManager * m_pLang;
		UINT           m_uFlag;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPaint(void);
	};

// End of File

#endif
