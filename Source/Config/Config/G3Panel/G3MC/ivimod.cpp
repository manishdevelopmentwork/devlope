
#include "intern.hpp"

#include "legacy.h"

#include "ivimod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current and Process Volt Modules
//

// Dynamic Class

AfxImplementDynamicClass(CIVIModule, CGenericModule);

// UI Management

CViewWnd * CIVIModule::CreateMainView(void)
{
	return New CIVIMainWnd;
	}

// Comms Object Access

UINT CIVIModule::GetObjectCount(void)
{
	return 1;
	}

BOOL CIVIModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_INP);
			
			Data.pItem = m_pInput;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CIVIModule::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pInput->Convert(pValue->GetChild(L"Config"));

		return TRUE;
		}
	
	return FALSE;
	}

// Download Support

void CIVIModule::MakeObjectData(CInitData &Init)
{
	if( m_pInput->m_IVIModel == ID_CSINI8L || m_pInput->m_IVIModel == ID_CSINV8L ) {

		for( UINT Channel = 0; Channel < HDW_CHANNELS; Channel++ ) {

			Init.AddWord(GetProp(PROPID_LIN_SEGMENTS));

			if( m_pInput->m_Linearize[Channel] ) {

				Init.AddWord(MAKEWORD(m_pInput->m_LinSegments[Channel], Channel));

				for( UINT SegPoint = 0; SegPoint <= m_pInput->m_LinSegments[Channel]; SegPoint++ ) {

					Init.AddWord(GetProp(PROPID_LIN_INPUT));

					Init.AddWord(OffsetInput(m_pInput->m_LinInput[Channel][SegPoint]));

					Init.AddWord(GetProp(PROPID_LIN_PV));

					Init.AddWord(OffsetPV(m_pInput->m_LinPV[Channel][SegPoint], Channel));
					}
				}

			else
				Init.AddWord(MAKEWORD(0, Channel));
			}

		Init.AddWord(GetProp(PROPID_LIN_STORE));

		Init.AddWord(1);
		}
	}

// Implementation

void CIVIModule::AddMetaData(void)
{
	Meta_AddObject(Input);

	CGenericModule::AddMetaData();
	}

WORD CIVIModule::GetProp(WORD PropID)
{
	WORD WriteObjLoop1 = (CMD_WRITE << 8) | (1 << 11);

	PropID |= WriteObjLoop1;

	return PropID;
	}

WORD CIVIModule::OffsetInput(INT nInput)
{
	LONG InputValue = 0;

	if( m_pInput->m_IVIModel == ID_CSINV8L ) {

		if( m_pInput->m_PVRange == 1 ) {

			InputValue = LONG(nInput * 3);
			}
		else
			InputValue = LONG(((nInput + 10000) * 3) / 2);
		}

	if( m_pInput->m_IVIModel == ID_CSINI8L ) {

		if( m_pInput->m_PIRange == 1 ) {

			InputValue = LONG((nInput * 3) / 2);
			}
		else
			InputValue = LONG(((nInput - 4000) * 15) / 8);
		}

	return WORD(InputValue + 10000);
	}

WORD CIVIModule::OffsetPV(INT nPV, UINT Channel)
{
	if( nPV < -30000 ) nPV = -30000;

	if( nPV > +30000 ) nPV = +30000;

	return WORD(nPV + 30000);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current Module
//

// Dynamic Class

AfxImplementDynamicClass(CII8Module, CIVIModule);

// Constructor

CII8Module::CII8Module(void)
{
	m_pInput = New CIVIInput(ID_CSINI8);

	m_Model  = "CSINI8";

	m_Ident  = ID_CSINI8;

	m_FirmID = FIRM_8IN;

	m_Conv.Insert(L"Graphite", L"CGMINI8Module");
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt Module
//

// Dynamic Class

AfxImplementDynamicClass(CIV8Module, CIVIModule);

// Constructor

CIV8Module::CIV8Module(void)
{
	m_pInput = New CIVIInput(ID_CSINV8);

	m_Model  = "CSINV8";

	m_Ident  = ID_CSINV8;

	m_FirmID = FIRM_8IN;

	m_Conv.Insert(L"Graphite", L"CGMINV8Module");
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current Linearizer Module
//

// Dynamic Class

AfxImplementDynamicClass(CII8LModule, CIVIModule);

// Constructor

CII8LModule::CII8LModule(void)
{
	m_pInput = New CIVIInput(ID_CSINI8L);

	m_Model  = "CSINI8L";

	m_Ident  = ID_CSINI8L;

	m_FirmID = FIRM_8INL;
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt Linearizer Module
//

// Dynamic Class

AfxImplementDynamicClass(CIV8LModule, CIVIModule);

// Constructor

CIV8LModule::CIV8LModule(void)
{
	m_pInput = New CIVIInput(ID_CSINV8L);

	m_Model  = "CSINV8L";

	m_Ident  = ID_CSINV8L;

	m_FirmID = FIRM_8INL;
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current and Process Volt Modules Window
//

// Runtime Class

AfxImplementRuntimeClass(CIVIMainWnd, CProxyViewWnd);

// Constructor

CIVIMainWnd::CIVIMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CIVIMainWnd::OnAttach(void)
{
	m_pItem = (CIVIModule *) CProxyViewWnd::m_pItem;

	AddIVIPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CIVIMainWnd::AddIVIPages(void)
{
	CIVIInput *pInput = m_pItem->m_pInput;

	for( UINT n = 0; n < pInput->GetPageCount(); n++ ) {

		CViewWnd *pPage = pInput->CreatePage(n);

		m_pMult->AddView(pInput->GetPageName(n), pPage);

		pPage->Attach(pInput);
		}
	}

// End of File
