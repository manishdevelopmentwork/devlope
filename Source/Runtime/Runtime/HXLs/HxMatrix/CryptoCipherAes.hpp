
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoCipherAes_HPP

#define INCLUDE_CryptoCipherAes_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoSym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AES-CBC Symmetric Cipher
//

class CCryptoCipherAes : public CCryptoSym
{
	public:
		// Constructor
		CCryptoCipherAes(void);

		// ICryptoSym
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Finalize(void);

	protected:
		// Data Members
		psAesCbc_t m_Ctx;
	};

// End of File

#endif
