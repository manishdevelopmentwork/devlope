
#include "Intern.hpp"

#include "CredentialsDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Link Find Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCredentialsDialog, CStdDialog);

// Constructor

CCredentialsDialog::CCredentialsDialog(CString const &Device, CString const &User, CString const &Pass)
{
	m_Device = Device.ToUpper();

	m_User   = User;

	m_Pass   = Pass;

	m_fKeep  = TRUE;

	SetName(L"CredentialsDlg");
}

// Attributes

CString CCredentialsDialog::GetUser(void) const
{
	return m_User;
}

CString CCredentialsDialog::GetPass(void) const
{
	return m_Pass;
}

BOOL CCredentialsDialog::GetKeep(void) const
{
	return m_fKeep;
}

// Message Map

AfxMessageMap(CCredentialsDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK)
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)
	
	AfxDispatchNotify(2001, EN_CHANGE, OnEditChange)
	AfxDispatchNotify(2002, EN_CHANGE, OnEditChange)

	AfxMessageEnd(CCredentialsDialog)
};

// Message Handlers

BOOL CCredentialsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(CPrintf(GetWindowText(), PCTXT(m_Device)));

	GetDlgItem(2001).SetWindowText(m_User);

	GetDlgItem(2002).SetWindowText(m_Pass);

	((CButton &) GetDlgItem(2003)).SetCheck(TRUE);
	
	DoEnables();

	SetDlgFocus(2001);

	return TRUE;
}

// Command Handlers

BOOL CCredentialsDialog::OnCommandOK(UINT uID)
{
	m_User  = GetDlgItem(2001).GetWindowText();

	m_Pass  = GetDlgItem(2002).GetWindowText();

	m_fKeep = ((CButton &) GetDlgItem(2003)).IsChecked();

	EndDialog(TRUE);

	return TRUE;
}

BOOL CCredentialsDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
}

// Notification Handlers

void CCredentialsDialog::OnEditChange(UINT uID, CWnd &Ctrl)
{
	DoEnables();
}

// Implementation

void CCredentialsDialog::DoEnables(void)
{
	BOOL fEnable = (GetDlgItem(2001).GetWindowTextLength() && GetDlgItem(2002).GetWindowTextLength());

	GetDlgItem(IDOK).EnableWindow(fEnable);
}

// End of File
