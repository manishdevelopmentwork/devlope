
#include "Intern.hpp"

#include "OpcNodeIdPtr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "OpcNodeId.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Node Identifier Pointer
//

// Constructors

COpcNodeIdPtr::COpcNodeIdPtr(COpcNodeIdPtr const &That)
{
	m_pPtr = That.m_pPtr;
	}

COpcNodeIdPtr::COpcNodeIdPtr(COpcNodeId const &Node)
{
	m_pPtr = &Node;
	}

// Assignment

COpcNodeIdPtr & COpcNodeIdPtr::operator = (COpcNodeIdPtr const &That)
{
	m_pPtr = That.m_pPtr;

	return ThisObject;
	}

// Comparison

int AfxCompare(COpcNodeIdPtr const &a, COpcNodeIdPtr const &b)
{
	return AfxCompare(*a.m_pPtr, *b.m_pPtr);
	}

// End of File
