
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	CPinger Pinger;

	Pinger.SetTimeout(1000);

	CIPAddr IP;

	IP = "192.168.2.1";
	
	AfxTrace("Pinging %s - %s\n", IP.GetDotted(), Pinger.Ping(IP) ? "OK" : "Failed");

	IP = "192.168.2.100";
	
	AfxTrace("Pinging %s - %s\n", IP.GetDotted(), Pinger.Ping(IP) ? "OK" : "Failed");
	}

// End of File
