
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_NOTMSG_HPP
	
#define	INCLUDE_NOTMSG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Notification Dispatch Helpers
//

#define	AfxDispatchNotify(i, n, f)		No_##n(i,i,n,f)

#define	AfxDispatchNotifyRange(i1, i2, n, f)	No_##n(i1,i2,n,f)

#define	DPN(a, b, c, d, e, f)			AfxDispatchWinNot(a, b, c, d, e, f)

#define	HOBJ					HANDLE

//////////////////////////////////////////////////////////////////////////
//
// Notification Dispatch Macros
//

#define	No_STN_CLICKED(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_STN_DBLCLK(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_STN_DISABLE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_STN_ENABLE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")

#define	No_WM_HSCROLL(i1,i2,n,f)		DPN(WM_HSCROLL,i1,i2,0,(void(CTP)(UINT,int,CWnd &))&f,"T-5L6W2w")
#define	No_WM_VSCROLL(i1,i2,n,f)		DPN(WM_VSCROLL,i1,i2,0,(void(CTP)(UINT,int,CWnd &))&f,"T-5L6W2w")

#define No_BN_CLICKED(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_BN_DBLCLK(i1,i2,n,f)			DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_BN_DOUBLECLICKED(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_BN_KILLFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_BN_SETFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")

#define No_LBN_DBLCLK(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_LBN_ERRSPACE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_LBN_KILLFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_LBN_SELCANCEL(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_LBN_SELCHANGE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_LBN_SETFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")

#define No_CBN_CLOSEUP(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_DBLCLK(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_DROPDOWN(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_EDITCHANGE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_EDITUPDATE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_ERRSPACE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_KILLFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_SELCHANGE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_SELENDCANCEL(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_SELENDOK(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define No_CBN_SETFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")

#define	No_EN_CHANGE(i1,i2,n,f)			DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_ERRSPACE(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_HSCROLL(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_KILLFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_MAXTEXT(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_SETFOCUS(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_UPDATE(i1,i2,n,f)			DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")
#define	No_EN_VSCROLL(i1,i2,n,f)		DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L3w")

#define	No_NM_CHAR(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMCHAR      &))&f,"T-5L2V")
#define	No_NM_CLICK(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_CUSTOMDRAW(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(UINT(CTP)(UINT,NMCUSTOMDRAW&))&f,"L-5L2V")
#define	No_NM_DBLCLK(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_HOVER(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_KEYDOWN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMKEY       &))&f,"L-5L2V")
#define	No_NM_KILLFOCUS(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_NCHITTEST(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(UINT(CTP)(UINT,NMMOUSE     &))&f,"L-5L2V")
#define	No_NM_OUTOFMEMORY(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"L-5L2V")
#define	No_NM_RCLICK(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHDR       &))&f,"L-5L2V")
#define	No_NM_RDBLCLICK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_RELEASEDCAPTURE(i1,i2,n,f)	DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_RETURN(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_NM_SETCURSOR(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMMOUSE     &))&f,"L-5L2V")
#define	No_NM_SETFOCUS(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHDR       &))&f,"L-5L2V")

#define No_HDN_BEGINDRAG(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHEADER           &))&f,"L-5L2V")
#define No_HDN_BEGINTRACK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHEADER           &))&f,"L-5L2V")
#define No_HDN_DIVIDERDBLCLK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")
#define No_HDN_ENDDRAG(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHEADER           &))&f,"L-5L2V")
#define No_HDN_ENDTRACK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")
#define No_HDN_FILTERBTNCLK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHDFILTERBTNCLICK &))&f,"L-5L2V")
#define No_HDN_FILTERCHANGE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")
#define No_HDN_GETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDDISPINFO       &))&f,"T-5L2V")
#define No_HDN_ITEMCHANGED(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")
#define No_HDN_ITEMCHANGING(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHEADER           &))&f,"L-5L2V")
#define No_HDN_ITEMCLICK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")
#define No_HDN_ITEMDBLCLK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")
#define No_HDN_TRACK(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHEADER           &))&f,"T-5L2V")

#define	No_LVN_BEGINDRAG(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_BEGINLABELEDIT(i1,i2,n,f)	DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLVDISPINFO   &))&f,"L-5L2V")
#define	No_LVN_BEGINRDRAG(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_COLUMNCLICK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_DELETEALLITEMS(i1,i2,n,f)	DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_DELETEITEM(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_ENDLABELEDIT(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLVDISPINFO   &))&f,"L-5L2V")
#define	No_LVN_GETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLVDISPINFO   &))&f,"L-5L2V")
#define	No_LVN_GETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLVDISPINFO   &))&f,"L-5L2V")
#define	No_LVN_HOTTRACK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_INSERTITEM(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_ITEMACTIVATE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMITEMACTIVATE &))&f,"T-5L2V")
#define	No_LVN_ITEMCHANGED(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLISTVIEW     &))&f,"T-5L2V")
#define	No_LVN_ITEMCHANGING(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLISTVIEW     &))&f,"L-5L2V")
#define	No_LVN_KEYDOWN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLVKEYDOWN    &))&f,"L-5L2V")
#define	No_LVN_MARQUEEBEGIN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMLISTVIEW     &))&f,"L-5L2V")
#define	No_LVN_SETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMLVDISPINFO   &))&f,"T-5L2V")

#define	No_TVN_BEGINDRAG(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_BEGINLABELEDIT(i1,i2,n,f)	DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMTVDISPINFO   &))&f,"L-5L2V")
#define	No_TVN_BEGINRDRAG(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_DELETEITEM(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_ENDLABELEDIT(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMTVDISPINFO   &))&f,"L-5L2V")
#define	No_TVN_GETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTVDISPINFO   &))&f,"T-5L2V")
#define	No_TVN_ITEMEXPANDED(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_ITEMEXPANDING(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMTREEVIEW     &))&f,"L-5L2V")
#define	No_TVN_KEYDOWN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMTVKEYDOWN    &))&f,"L-5L2V")
#define	No_TVN_SELCHANGED(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_SELCHANGING(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMTREEVIEW     &))&f,"L-5L2V")
#define	No_TVN_SETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTVDISPINFO   &))&f,"T-5L2V")
#define	No_TVN_CONTEXTMENU(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_CHANGEMULTI(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_OFFERFOCUS(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_PICKITEM(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTREEVIEW     &))&f,"T-5L2V")
#define	No_TVN_GETINFOTIP(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTVGETINFOTIP &))&f,"T-5L2V")

#define	No_ACN_START(i1,i2,n,f)			DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L2w")
#define	No_ACN_STOP(i1,i2,n,f)			DPN(WM_COMMAND,i1,i2,n,(void(CTP)(UINT,CWnd &))&f,"T-5L2w")

#define	No_MCN_GETDAYSTATE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMDAYSTATE  &))&f,"L-5L2V")
#define	No_MCN_SELCHANGE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMSELCHANGE &))&f,"L-5L2V")
#define	No_MCN_SELECT(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMSELCHANGE &))&f,"L-5L2V")

#define	No_DTN_CLOSEUP(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR               &))&f,"L-5L2V")
#define	No_DTN_DATETIMECHANGE(i1,i2,n,f)	DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMDATETIMECHANGE    &))&f,"L-5L2V")
#define	No_DTN_DROPDOWN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR               &))&f,"L-5L2V")
#define	No_DTN_DATETIMEFORMAT(i1,i2,n,f)	DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMDATETIMEFORMAT    &))&f,"L-5L2V")
#define	No_DTN_USERSTRING(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMDATETIMESTRING    &))&f,"L-5L2V")
#define	No_DTN_WMKEYDOWN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMDATETIMEWMKEYDOWN &))&f,"L-5L2V")

#define	No_IPN_FIELDCHANGE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMIPADDRESS &))&f,"L-5L2V")

#define	No_TTN_GETDISPINFO(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTTDISPINFO &))&f,"T-5L2V")
#define	No_TTN_POP(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(UINT(CTP)(UINT,NMHDR        &))&f,"L-5L2V")
#define	No_TTN_SHOW(i1,i2,n,f)			DPN(WM_NOTIFY,i1,i2,n,(UINT(CTP)(UINT,NMHDR        &))&f,"L-5L2V")
#define	No_TTN_LINKCLICK(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(UINT(CTP)(UINT,NMHDR        &))&f,"L-5L2V")

#define	No_TCN_FOCUSCHANGE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_TCN_KEYDOWN(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMTCKEYDOWN &))&f,"T-5L2V")
#define	No_TCN_SELCHANGE(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(void(CTP)(UINT,NMHDR       &))&f,"T-5L2V")
#define	No_TCN_SELCHANGING(i1,i2,n,f)		DPN(WM_NOTIFY,i1,i2,n,(BOOL(CTP)(UINT,NMHDR       &))&f,"L-5L2V")

// End of File

#endif
