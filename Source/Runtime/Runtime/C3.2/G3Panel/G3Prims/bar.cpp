
#include "intern.hpp"

#include <float.h>

#include "bar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar
//

// Constructor

CPrimLegacyBar::CPrimLegacyBar(void)
{
	m_pEdge     = New CPrimPen;

	m_pFill     = New CPrimBrush;

	m_pColor3   = New CPrimColor(naFeature);
	}

// Destructor

CPrimLegacyBar::~CPrimLegacyBar(void)
{
	delete m_pEdge;

	delete m_pFill;

	delete m_pColor3;
	}

// Initialization

void CPrimLegacyBar::Load(PCBYTE &pData)
{
	CPrimRich::Load(pData);

	m_pEdge  ->Load(pData);

	m_pFill  ->Load(pData);

	m_pColor3->Load(pData);

	m_uMode   = GetByte(pData);

	m_fShowSP = GetByte(pData);
	}

// Overridables

void CPrimLegacyBar::SetScan(UINT Code)
{
	m_pEdge  ->SetScan(Code);

	m_pFill  ->SetScan(Code);

	m_pColor3->SetScan(Code);
	
	CPrimRich::SetScan(Code);
	}

void CPrimLegacyBar::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRich::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_pFill->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}

		if( m_pEdge->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimLegacyBar::DrawExec(IGDI *pGDI, IRegion *pErase)
{
	CPrimRich::DrawExec(pGDI, pErase);
	}

// Context Creation

void CPrimLegacyBar::FindCtx(CCtx &Ctx)
{
	if( m_pValue ) {

		if( IsAvail() ) {

			IDataServer *pServer = CUISystem::m_pThis->GetDataServer();

			UINT    Type = m_pValue->GetType();

			DWORD   Data = m_pValue->Execute(Type);

			DWORD      n = m_pValue->Execute(typeLValue);

			DWORD     SP = pServer->GetProp(n, tpSetPoint, Type);
			
			Ctx.m_fAvail = TRUE;

			Ctx.m_Val    = Data;

			Ctx.m_Min    = GetMinValue(Type);
			
			Ctx.m_Max    = GetMaxValue(Type);

			Ctx.m_Pos    = 0;

			Ctx.m_SP     = 0;
					
			if( Type == typeReal ) {

				C3REAL rVal = I2R(Ctx.m_Val);

				C3REAL rMin = I2R(Ctx.m_Min);

				C3REAL rMax = I2R(Ctx.m_Max);

				MakeMin(rVal, rMax);

				MakeMax(rVal, rMin);

				if( rMax - rMin ) {

					Ctx.m_Pos = (rVal - rMin) / (rMax - rMin);
					
					Ctx.m_SP  = m_fShowSP ? (I2R(SP) - rMin) / (rMax - rMin) : 0;
					}
				}
			else {
				C3INT nVal = Ctx.m_Val;

				C3INT nMin = Ctx.m_Min;

				C3INT nMax = Ctx.m_Max;

				MakeMin(nVal, nMax);

				MakeMax(nVal, nMin);

				if( nMax - nMin ) {

					Ctx.m_Pos = C3REAL(nVal - nMin) / C3REAL(nMax - nMin);					
					
					Ctx.m_SP  = m_fShowSP ? C3REAL(SP - nMin) / C3REAL(nMax - nMin) : 0;
					}				
				}
			}
		else {
			Ctx.m_fAvail = FALSE;

			Ctx.m_Pos    = 0.0;

			Ctx.m_SP     = 0.0;
			}
		}
	else {
		Ctx.m_fAvail = TRUE;
		
		Ctx.m_Pos    = 0.0;

		Ctx.m_SP     = 0.0;
		}

	Ctx.m_Color3 = m_pColor3->GetColor();
	}

// Context Check

BOOL CPrimLegacyBar::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail  == That.m_fAvail   &&
	       m_Val     == That.m_Val      &&
	       m_Min     == That.m_Min      &&
	       m_Max     == That.m_Max      &&
	       m_Pos     == That.m_Pos      &&
	       m_SP      == That.m_SP       &&
	       m_Color3  == That.m_Color3   ;;
	}

// Implementation

void CPrimLegacyBar::FillRect(IGDI *pGDI, R2 Rect)
{
	if( m_uMode ) {

		m_pFill->FillRect(pGDI, Rect);

		pGDI->SetBrushStyle(brushFore);

		pGDI->SetBrushFore(m_Ctx.m_Color3);

		switch( m_uMode ) {
			
			case 1:
				Rect.y2 -= Round(m_Ctx.m_Pos * (Rect.y2 - Rect.y1));
				break;
			
			case 2:
				Rect.y1 += Round(m_Ctx.m_Pos * (Rect.y2 - Rect.y1));
				break;
			
			case 3:
				Rect.x1 += Round(m_Ctx.m_Pos * (Rect.x2 - Rect.x1));
				break;
			
			case 4:
				Rect.x2 -= Round(m_Ctx.m_Pos * (Rect.x2 - Rect.x1));
				break;
			}

		pGDI->FillRect(PassRect(Rect));
		
		return;
		}

	m_pFill->FillRect(pGDI, Rect);
	}

// Rounding

C3INT CPrimLegacyBar::Round(C3REAL Data)
{
	return int(Data + 0.5);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar
//

// Constructor

CPrimLegacyVertBar::CPrimLegacyVertBar(void)
{
	}

// Initialization

void CPrimLegacyVertBar::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyVertBar", pData);

	CPrimLegacyBar::Load(pData);
	}

// Overridables

void CPrimLegacyVertBar::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));
		}

	if( m_Content < 3 ) {

		SelectFont(pGDI);

		if( m_Content > 0 ) {

			int cx = GetTextWidth(pGDI, CPrimRich::m_Ctx.m_Label);
			
			int cy = GetTextHeight(pGDI, CPrimRich::m_Ctx.m_Label);

			R2 Fill = Rect;
			
			Fill.y1 = Fill.y2 - cy;
			
			Rect.y2 = Fill.y1 - 1;

			pGDI->SelectBrush(brushFore);
			
			pGDI->SetForeColor(m_pColor3->GetColor());

			pGDI->SetBackMode(modeTransparent);
			
			pGDI->FillRect(PassRect(Fill));

			pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());		

			pGDI->DrawLine(Rect.x1, Rect.y2, Rect.x2, Rect.y2);

			pGDI->SetTextFore(m_pEdge->m_pColor->GetColor());
			
			DrawLabel(pGDI, Fill);
			}
		}

	FillRect(pGDI, Rect);

	if( m_Content < 2 ) {

		SetColors(pGDI, 2);
		
		DrawValue(pGDI, Rect);			
		}

	DrawSP  (pGDI, Rect);
	}

// Implementation

void CPrimLegacyVertBar::DrawSP(IGDI *pGDI, R2 Rect)
{
	if( m_fShowSP ) {

		int x1 = Rect.x1 - (m_pEdge->m_Width ? 2 : 1);
		int y1 = Rect.y1;
		int x2 = Rect.x2 + (m_pEdge->m_Width ? 2 : 1);
		int y2 = Rect.y2 - 1;

		pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());

		int yp = y2 - C3INT((y2 - y1) * m_Ctx.m_SP);

		MakeMax(yp, y1);

		MakeMin(yp, y2);

		for( int n = 0; n < 4; n++ ) {

			int y11 = yp-n;

			int y21 = yp+n+1;

			MakeMax(y11, y1);

			MakeMin(y21, y2+1);

			pGDI->DrawLine(x1-n+4, y11, x1-n+4, y21);
			
			pGDI->DrawLine(x2+n-5, y11, x2+n-5, y21);
			}

		pGDI->SelectPen(penGray);

		pGDI->DrawLine(x1+5, yp, x2-5, yp);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar
//

// Constructor

CPrimLegacyHorzBar::CPrimLegacyHorzBar(void)
{
	}

// Initialization

void CPrimLegacyHorzBar::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyHorzBar", pData);

	CPrimLegacyBar::Load(pData);
	}

// Overridables

void CPrimLegacyHorzBar::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;	

	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));
		}

	if( m_Content < 3 ) {

		SelectFont(pGDI);

		if( m_Content > 0 ) {			

			int cx = GetTextWidth(pGDI, CPrimRich::m_Ctx.m_Label);
			
			int cy = GetTextHeight(pGDI, CPrimRich::m_Ctx.m_Label);

			R2 Fill = Rect;
			
			Fill.x2 = Fill.x1 + cx;
			
			Rect.x1 = Fill.x2 + 1;

			pGDI->SelectBrush(brushFore);
			
			pGDI->SetForeColor(m_pColor3->GetColor());

			pGDI->SetBackMode(modeTransparent);
			
			pGDI->FillRect(PassRect(Fill));

			pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());		

			pGDI->DrawLine(Rect.x1-1, Rect.y1, Rect.x1-1, Rect.y2);

			pGDI->SetTextFore(m_pEdge->m_pColor->GetColor());
			
			DrawLabel(pGDI, Fill);
			}
		}

	FillRect(pGDI, Rect);

	if( m_Content < 2 ) {

		SetColors(pGDI, 2);
		
		DrawValue(pGDI, Rect);			
		}

	DrawSP  (pGDI, Rect);
	}

// Implementation

void CPrimLegacyHorzBar::DrawSP(IGDI *pGDI, R2 Rect)
{
	if( m_fShowSP ) {

		int x1 = Rect.x1;
		int y1 = Rect.y1 - (m_pEdge->m_Width ? 2 : 1);
		int x2 = Rect.x2 - 1;
		int y2 = Rect.y2 + (m_pEdge->m_Width ? 2 : 1);

		pGDI->SetPenFore(m_pEdge->m_pColor->GetColor());

		int xp = x1 + C3INT((x2 - x1) * m_Ctx.m_SP);

		MakeMax(xp, x1);

		MakeMin(xp, x2);

		for( int n = 0; n < 4; n++ ) {

			int x11 = xp-n;

			int x21 = xp+n+1;

			MakeMax(x11, x1);

			MakeMin(x21, x2+1);

			pGDI->DrawLine(x11, y1-n+4, x21, y1-n+4);
			
			pGDI->DrawLine(x11, y2+n-5, x21, y2+n-5);
			}			

		pGDI->SelectPen(penGray);

		pGDI->DrawLine(xp, y1+5, xp, y2-5);
		}
	}

// End of File
