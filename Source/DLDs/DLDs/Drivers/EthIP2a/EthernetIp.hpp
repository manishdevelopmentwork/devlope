
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_EthernetIp_HPP

#define	INCLUDE_EthernetIp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CipCommon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTcpIpSock;

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Encapsulation
//

class CEthernetIp : public CCipCommon
{
	public:
		// Constructor
		CEthernetIp(CTcpIpSock *pSock, UINT uTimeout);

		// Destructor
		~CEthernetIp(void);

		// Attributes
		bool IsLayerUp(void) const;

		// Operations
		bool Open(void);
		bool Close(void);
		bool ExchangeUnconnect(CBuffer * &pBuff);
		bool ExchangeConnected(CBuffer * &pBuff, DWORD O2T, DWORD T2O, WORD Sequence);

	protected:
		// Segment Types
		enum Segment
		{
			segNullAddr      = 0x00,
			segConnectedAddr = 0xA1,
			segConnectedData = 0xB1,
			segUnconnectData = 0xB2,
			};

		// Command Values
		enum Command
		{
			cmdRegisterSession   = 0x65,
			cmdUnregisterSession = 0x66,
			cmdSendRrData	     = 0x6F,
			cmdSendUnitData	     = 0x70,
			};

		// Data Structures
		struct CPacket;
		struct CRegisterPacket;
		struct CDataPacket;
		struct CGeneralItem;
		struct CConnectedData;
		struct CConnectedAddress;

		// Data Members
		CTcpIpSock * m_pSock;
		UINT	     m_uTimeout;
		bool	     m_fLayerUp;
		DWORD	     m_Session;

		// Layer Control
		bool LayerUp(void);
		bool LayerDown(void);
		void ClearData(void);

		// Session Control
		bool RegisterSession(void);
		bool UnregisterSession(void);

		// Transport Access
		bool Send(CBuffer *  pBuff, WORD Command);
		bool Recv(CBuffer * &pBuff, WORD Command, UINT uTime);
	};

//////////////////////////////////////////////////////////////////////////
//
// Packet Header
//

#pragma pack(1)

struct CEthernetIp::CPacket
{
	WORD	Command;
	WORD	Length;
	DWORD	Session;
	DWORD	Status;
	BYTE	Context[8];
	DWORD	Options;

	void ReOrder(void)
	{
		CipRo(Command);
		CipRo(Length);
		CipRo(Session);
		CipRo(Status);
		CipRo(Options);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Registration
//

#pragma pack(1)

struct CEthernetIp::CRegisterPacket
{
	WORD	Version;
	WORD	OptFlags;

	void ReOrder(void)
	{
		CipRo(Version);
		CipRo(OptFlags);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Data Exchange
//

#pragma pack(1)

struct CEthernetIp::CDataPacket
{
	DWORD	Interface;
	WORD	Timeout;
	WORD	ItemCount;

	void ReOrder(void)
	{
		CipRo(Interface);
		CipRo(Timeout);
		CipRo(ItemCount);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// General Item
//

#pragma pack(1)

struct CEthernetIp::CGeneralItem
{
	WORD	TypeId;
	WORD	Length;

	void ReOrder(void)
	{
		CipRo(TypeId);
		CipRo(Length);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Connected Data
//

#pragma pack(1)

struct CEthernetIp::CConnectedData
{
	WORD	TypeId;
	WORD	Length;
	WORD	Sequence;

	void ReOrder(void)
	{
		CipRo(TypeId);
		CipRo(Length);
		CipRo(Sequence);
		}
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Connected Address
//

#pragma pack(1)

struct CEthernetIp::CConnectedAddress
{
	WORD	TypeId;
	WORD	Length;
	DWORD	Connection;

	void ReOrder(void)
	{
		CipRo(TypeId);
		CipRo(Length);
		CipRo(Connection);
		}
	};

#pragma pack()

// End of File

#endif
