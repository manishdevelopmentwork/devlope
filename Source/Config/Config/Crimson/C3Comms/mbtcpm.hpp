
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MBTCPM_HPP
	
#define	INCLUDE_MBTCPM_HPP 

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Master Driver Options
//

class CModbusMasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CModbusMasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Addr2;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		BOOL m_fDisable16;
		BOOL m_fDisable15;
		BOOL m_fDisable5;
		BOOL m_fDisable6;
		UINT m_PingReg;
		UINT m_Max01;
		UINT m_Max02;
		UINT m_Max03;
		UINT m_Max04;
		UINT m_Max15;
		UINT m_Max16;
		BOOL m_fIgnoreReadEx;
		UINT m_FlipLong;
		UINT m_FlipReal;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Master Driver
//

class CModbusMasterTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CModbusMasterTCPDriver(void);

		//Destructor
		~CModbusMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL CheckAlignment(CSpace *pSpace);
						
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP Address Selection
//

class CModbusTCPAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CModbusTCPAddrDialog(CModbusMasterTCPDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		BOOL	AllowSpace(CSpace *pSpace);
	};

// End of File

#endif
