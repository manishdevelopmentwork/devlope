
#include "intern.hpp"

#include "execute.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Byte Code Executer
//

DLLAPI DWORD C3ExecuteCode( PCBYTE	  pCode,
			    IDataServer * pData,
			    PDWORD	  pParam
			    )
{
	BYTE   bType  = pCode[0];

	BYTE   bDepth = pCode[1];

	BYTE   bLocal = pCode[2];

	BYTE   bParam = pCode[3];

	MakeMax(bLocal, bParam);

	PDWORD pLocal = PDWORD(alloca(bLocal * sizeof(DWORD)));

	PDWORD pAlloc = PDWORD(alloca(bDepth * sizeof(DWORD)));

	PDWORD pInit  = pAlloc + bDepth - 1;

	PDWORD pStack = pInit;

	if( bLocal           ) memset(pLocal,      0, bLocal * sizeof(DWORD));

	if( bParam && pParam ) memcpy(pLocal, pParam, bParam * sizeof(DWORD));

	pCode += 4;

	DWORD Temp;

	PUTF  pNew;

	BYTE  b;

	if( *pCode == bcDebugInfo ) {

		pCode += 3;
		}

	while( (b = *pCode++) != bcNull ) {

		if( b < bcNull ) {

			SI0 = b;
			
			PUSH();

			continue;
			}

		if( b == bcReturn ) {

			break;
			}

		if( b == bcDebugFunc ) {

			b       = *pCode++;

			pCode  += b;

			Temp    = pData->RunFunc(ReadWord(pCode), b, pStack + 1);

			pCode  += 2;

			pStack += b;

			SX0 = Temp;
			
			PUSH();

			continue;
			}

		if( b == bcFunction ) {

			b       = *pCode++;

			Temp    = pData->RunFunc(ReadWord(pCode), b, pStack + 1);

			pCode  += 2;

			pStack += b;

			SX0 = Temp;
			
			PUSH();

			continue;
			}

		switch( b ) {

			// Conversions

			case bcToInteger:
				SI1 = C3INT(SR1);
				break;

			case bcToReal:
				SR1 = C3REAL(SI1);
				break;

			case bcTestInteger:
				SI1 = SI1?1:0;
				break;

			case bcTestReal:
				SI1 = SR1?1:0;
				break;

			// Stack Helpers

			case bcPop:
				PULL();
				break;

			case bcPopString:
				if( SS1 ) free(SS1);
				PULL();
				break;

			case bcDupTop:
				SX0 = SX1;
				PUSH();
				break;

			case bcDupTopString:
				SS0 = wstrdup(SS1);
				PUSH();
				break;

			case bcDup3rd:
				SX0 = SX1;
				SX1 = SX2;
				SX2 = SX0;
				PUSH();
				break;

			case bcDup3rdString:
				SS0 = SS1;
				SS1 = SS2;
				SS2 = wstrdup(SS0);
				PUSH();
				break;

			case bcSwap:
				Temp = SX1;
				SX1  = SX2;
				SX2  = Temp;
				break;

			// Branches
			
			case bcBranchZero:
				pCode += SI1 ? 2 : SHORT(ReadWord(pCode));
				PULL();
				break;

			case bcBranchNonZero:
				pCode += SI1 ? SHORT(ReadWord(pCode)) : 2;
				PULL();
				break;

			case bcBranch:
				pCode += SHORT(ReadWord(pCode));
				break;

			// Referencing
			
			case bcLoadAddr:
				SX0     = ReadWord(pCode);
				pCode  += 2;
				PUSH();
				break;
			
			case bcIndexString:
				Temp = (SI1 < wstrlen(SS2)) ? SS2[SI1] : 0;
				free(SS2);
				SI2  = ((WCHAR) Temp);
				PULL();
				break;
			
			case bcIndexArray:
				((CDataRef &) SX2).t.m_Array = SX1;
				PULL();
				break;

			case bcIndexExtend:
				((CDataRef &) SX2).x.m_Array = SX1;
				PULL();
				break;
			
			case bcIndexBit:
				((CDataRef &) SX2).t.m_HasBit = 1;
				((CDataRef &) SX2).t.m_BitRef = SX1;
				PULL();
				break;

			// Save Local

			case bcPutLocal:
				pLocal[*pCode++] = SX1;
				PULL();
				break;

			case bcPutLocalString:
				free(PTXT(pLocal[*pCode]));
				pLocal[*pCode++] = SX1;
				PULL();
				break;

			case bcPutLocal0:
				pLocal[0] = SX1;
				PULL();
				break;

			case bcPutLocal1:
				pLocal[1] = SX1;
				PULL();
				break;

			case bcPutLocal2:
				pLocal[2] = SX1;
				PULL();
				break;

			case bcPutLocal3:
				pLocal[3] = SX1;
				PULL();
				break;

			case bcPutLocal4:
				pLocal[4] = SX1;
				PULL();
				break;

			case bcPutLocal5:
				pLocal[5] = SX1;
				PULL();
				break;

			// Load Local

			case bcGetLocal:
				SX0 = pLocal[*pCode++];
				PUSH();
				break;

			case bcGetLocalString:
				SS0 = wstrdup(PCUTF(pLocal[*pCode++]));
				PUSH();
				break;

			case bcGetLocal0:
				SX0 = pLocal[0];
				PUSH();
				break;

			case bcGetLocal1:
				SX0 = pLocal[1];
				PUSH();
				break;

			case bcGetLocal2:
				SX0 = pLocal[2];
				PUSH();
				break;

			case bcGetLocal3:
				SX0 = pLocal[3];
				PUSH();
				break;

			case bcGetLocal4:
				SX0 = pLocal[4];
				PUSH();
				break;

			case bcGetLocal5:
				SX0 = pLocal[5];
				PUSH();
				break;

			// Save Direct

			case bcPutInteger:
				pData->SetData(ReadWord(pCode), typeInteger, setNone, SX1);
				pCode += 2;
				PULL();
				break;

			case bcPutReal:
				pData->SetData(ReadWord(pCode), typeReal, setNone, SX1);
				pCode += 2;
				PULL();
				break;

			case bcPutString:
				pData->SetData(ReadWord(pCode), typeString, setNone, SX1);
				pCode += 2;
				PULL();
				break;

			// Load Direct

			case bcGetInteger:
				SX0     = pData->GetData(ReadWord(pCode), typeInteger, getNone);
				pCode  += 2;
				PUSH();
				break;

			case bcGetReal:
				SX0     = pData->GetData(ReadWord(pCode), typeReal, getNone);
				pCode  += 2;
				PUSH();
				break;

			case bcGetString:
				SX0     = pData->GetData(ReadWord(pCode), typeString, getNone);
				pCode  += 2;
				PUSH();
				break;

			// Save Indirect

			case bcSaveInteger:
				pData->SetData(SI2, typeInteger, setNone, SX1);
				PULL();
				PULL();
				break;

			case bcSaveReal:
				pData->SetData(SI2, typeReal, setNone, SX1);
				PULL();
				PULL();
				break;

			case bcSaveString:
				pData->SetData(SI2, typeString, setNone, SX1);
				PULL();
				PULL();
				break;

			// Load Indirect

			case bcLoadInteger:
				SX1 = pData->GetData(SX1, typeInteger, getNone);
				break;

			case bcLoadReal:
				SX1 = pData->GetData(SX1, typeReal, getNone);
				break;

			case bcLoadString:
				SX1 = pData->GetData(SX1, typeString, getNone);
				break;

			// Load Property

			case bcLoadPropInteger:
				SX2 = pData->GetProp(SX2, WORD(SX1), typeInteger);
				PULL();
				break;

			case bcLoadPropReal:
				SX2 = pData->GetProp(SX2, WORD(SX1), typeReal);
				PULL();
				break;

			case bcLoadPropString:
				SX2 = pData->GetProp(SX2, WORD(SX1), typeString);
				PULL();
				break;

			// Constants

			case bcPushInteger1:
				SI0     = ReadByte(pCode);
				pCode  += 1;
				PUSH();
				break;

			case bcPushInteger2:
				SI0     = ReadWord(pCode);
				pCode  += 2;
				PUSH();
				break;

			case bcPushInteger4:
				SI0     = ReadLong(pCode);
				pCode  += 4;
				PUSH();
				break;

			case bcPushReal:
				SI0     = ReadLong(pCode);
				pCode  += 4;
				PUSH();
				break;

			case bcPushString:
				SS0    = wstrdup(PSTR(pCode + 2));
				pCode += 2 + 1 * ReadWord(pCode);
				PUSH();
				break;

			case bcPushWideString:
				SS0    = ReadText(PUTF(pCode + 2));
				pCode += 2 + 2 * ReadWord(pCode);
				PUSH();
				break;

			// Integer Operators

			case bcLogicalNot:
				SI1 = !SI1;
				break;

			case bcBitwiseNot:
				SI1 = ~SI1;
				break;

			case bcUnaryMinus:
				SI1 = -SI1;
				break;

			case bcIncrement:
				SI1++;
				break;

			case bcDecrement:
				SI1--;
				break;

			case bcBitSelect:
				SI2 = !!(SI2&1<<SI1);
				PULL();
				break;

			case bcDivide:
				SI2 = SI1 ? (SI2 / SI1) : 0;
				PULL();
				break;
			
			case bcRemainder:
				SI2 = SI1 ? (SI2 % SI1) : 0;
				PULL();
				break;
			
			case bcMultiply:
				SI2 = (SI2 *  SI1);
				PULL();
				break;

			case bcAdd:
				SI2 = (SI2 +  SI1);
				PULL();
				break;

			case bcSubtract:
				SI2 = (SI2 -  SI1);
				PULL();
				break;
			
			case bcLeftShift:
				SI2 = (SI2 << SI1);
				PULL();
				break;
			
			case bcRightShift:
				SI2 = (SI2 >> SI1);
				PULL();
				break;
			
			case bcLessThan:
				SI2 = (SI2 <  SI1);
				PULL();
				break;
			
			case bcLessOr:
				SI2 = (SI2 <= SI1);
				PULL();
				break;
			
			case bcGreaterThan:
				SI2 = (SI2 >  SI1);
				PULL();
				break;
			
			case bcGreaterOr:
				SI2 = (SI2 >= SI1);
				PULL();
				break;
			
			case bcEqual:
				SI2 = (SI2 == SI1);
				PULL();
				break;
			
			case bcNotEqual:
				SI2 = (SI2 != SI1);
				PULL();
				break;

			case bcBitwiseAnd:
				SI2 = (SI2 &  SI1);
				PULL();
				break;
			
			case bcBitwiseXor:
				SI2 = (SI2 ^  SI1);
				PULL();
				break;
			
			case bcBitwiseOr:
				SI2 = (SI2 |  SI1);
				PULL();
				break;

			case bcBitChange:
				SI1 ? (SI3 |= (1<<SI2)) : (SI3 &= ~(1<<SI2));
				PULL();
				PULL();
				break;

			// Real Operators

			case bcRealLogicalNot:
				SI1 = !SR1;
				break;

			case bcRealUnaryMinus:
				SR1 = -SR1;
				break;

			case bcRealIncrement:
				SR1++;
				break;

			case bcRealDecrement:
				SR1--;
				break;
			
			case bcRealDivide:
				SR2 = (SR2 / SR1);
				PULL();
				break;

			case bcRealMultiply:
				SR2 = (SR2 *  SR1);
				PULL();
				break;

			case bcRealAdd:
				SR2 = (SR2 +  SR1);
				PULL();
				break;

			case bcRealSubtract:
				SR2 = (SR2 -  SR1);
				PULL();
				break;

			case bcRealLessThan:
				SI2 = (SR2 <  SR1);
				PULL();
				break;

			case bcRealLessOr:
				SI2 = (SR2 <= SR1);
				PULL();
				break;

			case bcRealGreaterThan:
				SI2 = (SR2 >  SR1);
				PULL();
				break;

			case bcRealGreaterOr:
				SI2 = (SR2 >= SR1);
				PULL();
				break;

			case bcRealEqual:
				SI2 = (SR2 == SR1);
				PULL();
				break;

			case bcRealNotEqual:
				SI2 = (SR2 != SR1);
				PULL();
				break;

			// String Operators

			case bcStringAdd:
				Temp = wstrlen(SS2)+wstrlen(SS1)+1;
				pNew = PUTF(malloc(sizeof(WCHAR)*Temp));
				wstrcpy(pNew, SS2); free(SS2);
				wstrcat(pNew, SS1); free(SS1);
				SS2  = pNew;
				PULL();
				break;

			case bcStringAddChar:
				Temp = wstrlen(SS2);
				pNew = PUTF(malloc(sizeof(WCHAR)*(Temp+2)));
				memcpy(pNew, SS2, sizeof(WCHAR)*Temp);
				pNew[Temp+0] = WCHAR(SI1);
				pNew[Temp+1] = WCHAR(0);
				free(SS2);
				SS2  = pNew;
				PULL();
				break;

			case bcStringLessThan:
				Temp = (wstricmp(SS2, SS1)< 0);
				free(SS2);
				free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringLessOr:
				Temp = (wstricmp(SS2, SS1)<=0);
				free(SS2);
				free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringGreaterThan:
				Temp = (wstricmp(SS2, SS1)> 0);
				free(SS2);
				free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringGreaterOr:
				Temp = (wstricmp(SS2, SS1)>=0);
				free(SS2);
				free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringEqual:
				Temp = (wstricmp(SS2, SS1)==0);
				free(SS2);
				free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringNotEqual:
				Temp = (wstricmp(SS2, SS1)!=0);
				free(SS2);
				free(SS1);
				SI2  = Temp;
				PULL();
				break;
			}
		}

	return (bType & 0x3F) ? SI1 : 0;
	}

// End of File
