
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextOptionCard_HPP

#define INCLUDE_UITextOptionCard_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Option Card List
//

class CUITextOptionCard : public CUITextEnumPick
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextOptionCard(void);

	protected:
		// Overridables
		void OnBind(void);
	};

// End of File

#endif
