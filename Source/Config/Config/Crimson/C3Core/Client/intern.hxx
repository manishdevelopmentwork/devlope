
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_INTERN_HXX

#define INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_CANT                0x4001
#define IDS_COPY                0x4002
#define IDS_CREATE              0x4003
#define IDS_CUT_1               0x4004
#define IDS_CUT_2               0x4005
#define IDS_DELETE_1            0x4006
#define IDS_DELETE_2            0x4007
#define IDS_DELETE_3            0x4008
#define IDS_DO_YOU_WANT         0x4009
#define IDS_FORMAT              0x400A
#define IDS_GROUP               0x400B
#define IDS_MOVE                0x400C
#define IDS_NAVIGATION_PANE     0x400D
#define IDS_NEXT                0x400E
#define IDS_PASTE               0x400F
#define IDS_PREVIOUS            0x4010
#define IDS_REDO                0x4011
#define IDS_RENAME              0x4012
#define IDS_SELECT_THE          0x4013
#define IDS_TCTRL               0x4014
#define IDS_TCTRLU              0x4015
#define IDS_THE_NAME            0x4016
#define IDS_UNDO                0x4017

// End of File

#endif
