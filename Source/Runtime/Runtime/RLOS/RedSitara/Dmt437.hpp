
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Dmt437_HPP
	
#define	INCLUDE_AM437_Dmt437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCtrl437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 DM Timer Module
//

class CDmt437 : public IEventSink
{
	public:
		// Constructor
		CDmt437(CCtrl437 *pCtrl, UINT iIndex);
				
		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Attributes
		UINT GetFraction(void);

		// Operations
		void Wait(UINT uPeriod);
		void Delay(UINT uPeriod);
		void SetEventHandler(IEventSink *pSink, UINT uParam);
		void SetPeriodic(UINT uPeriod);
		void SetSingleShot(UINT uPeroid);
		void Start(void);
		void Stop(void);
		void EnableEvents(void);

	protected:
		// Registers
		enum
		{
			regCFG		= 0x0010 / sizeof(DWORD),
			regIRQEOI	= 0x0020 / sizeof(DWORD),
			regIRQRAW	= 0x0024 / sizeof(DWORD),
			regIRQSTS	= 0x0028 / sizeof(DWORD),
			regIRQENSET	= 0x002C / sizeof(DWORD),
			regIRQENCLR	= 0x0030 / sizeof(DWORD),
			regIRQWAKEEN	= 0x0034 / sizeof(DWORD),
			regTCLR		= 0x0038 / sizeof(DWORD),
			regTCRR		= 0x003C / sizeof(DWORD),
			regTLDR		= 0x0040 / sizeof(DWORD),
			regTTGR		= 0x0044 / sizeof(DWORD),
			regTWPS		= 0x0048 / sizeof(DWORD),
			regTMAR		= 0x004C / sizeof(DWORD),
			regTCAR1	= 0x0050 / sizeof(DWORD),
			regTSICR	= 0x0054 / sizeof(DWORD),
			regTCAR2	= 0x0058 / sizeof(DWORD),
			};
					
		// Bits
		enum
		{
			bitReset	= Bit(0),
			bitStart	= Bit(0),
			bitPeriodic	= Bit(1),
			bitCompare	= Bit(6),
			};

		// Interrupts
		enum
		{
			intCompare	= Bit(0),
			intOverflow	= Bit(1),
			intCapture	= Bit(2),
			};

		// Data Members
		PVDWORD	     m_pBase;
		UINT	     m_uLine;
		UINT         m_uClock;
		IEventSink * m_pSink;
		UINT	     m_uParam;
		UINT         m_uPeriod;
		UINT         m_uLoad;

		// Implementation
		void  Reset(void);
		void  WaitWrite(void);
		UINT  FindLoadValue(UINT uPeriod);
		DWORD FindBase(UINT iIndex);
		UINT  FindLine(UINT iIndex);
	};

// End of File

#endif
