
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_RackItem_HPP

#define INCLUDE_RackItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPortList;

//////////////////////////////////////////////////////////////////////////
//
// Backplane Configuration
//

class DLLAPI CRackItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRackItem(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);

		// Item Naming
		BOOL    IsHumanRoot(void) const;
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCommsPortList * m_pPorts;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
