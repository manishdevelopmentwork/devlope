
#include "Intern.hpp"

#include "UsbSetInterfaceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Interface Standard Device Request
//

// Constructor

CUsbSetInterfaceReq::CUsbSetInterfaceReq(WORD wAlt, WORD wIndex)
{
	Init(wAlt, wIndex);
	} 

// Operations

void CUsbSetInterfaceReq::Init(WORD wAlt, WORD wIndex)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqSetInterface;

	m_Direction = dirHostToDev;

	m_Recipient = recInterface;

	m_wValue    = wAlt;

	m_wIndex    = wIndex;
	}

// End of File
