
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PPS4201_HPP
	
#define	INCLUDE_PPS4201_HPP



//////////////////////////////////////////////////////////////////////////
//
// Klockner Moeller PS4-201 Driver
//

class CPPS4201Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CPPS4201Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		
	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
