
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextMapDirection_HPP

#define INCLUDE_UITextMapDirection_HPP

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Mapping Block Direction Selection
//

class CUITextMapDirection : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextMapDirection(void);

	protected:
		// Overridables
		void OnBind(void);
		void OnRebind(void);

		// Implementation
		void LoadEnum(void);
	};

// End of File

#endif
