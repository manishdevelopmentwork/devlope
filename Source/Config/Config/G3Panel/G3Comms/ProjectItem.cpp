
#include "Intern.hpp"

#include "ProjectItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Project Item Base Class
//

// Runtime Class

AfxImplementRuntimeClass(CProjectItem, CCodedHost);

// Constructor

CProjectItem::CProjectItem(void)
{
	}

// End of File
