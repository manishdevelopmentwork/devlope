
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostEnhanced_HPP

#define	INCLUDE_UsbHostEnhanced_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostHardwareDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Enhanced Host Controller Interface
//

class CUsbHostEnhanced : public CUsbHostHardwareDriver
{
	public:
		// Constructor
		CUsbHostEnhanced(void);

		// Destructor
		~CUsbHostEnhanced(void);

		// IUsbDriver
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);
		
		// IUsbHostInterfaceDriver
		UINT  GetType(void);
		UINT  GetPortCount(void);
		BOOL  GetPortConnect(UINT iPort);
		BOOL  GetPortEnabled(UINT iPort);
		BOOL  GetPortCurrent(UINT iPort);
		BOOL  GetPortSuspend(UINT iPort);
		BOOL  GetPortReset(UINT iPort);
		BOOL  GetPortPower(UINT iPort);
		UINT  GetPortSpeed(UINT iPort);
		void  SetPortEnable(UINT iPort, BOOL fSet);
		void  SetPortSuspend(UINT iPort, BOOL fset);
		void  SetPortResume(UINT iPort, BOOL fSet);
		void  SetPortReset(UINT iPort, BOOL fSet);
		void  SetPortPower(UINT iPort, BOOL fSet);
		void  EnableEvents(BOOL fEnable);
		BOOL  SetEndptDevAddr(DWORD iQh, BYTE bAddr);
		UINT  GetEndptDevAddr(DWORD iQh);
		BOOL  SetEndptAddr(DWORD iQh, BYTE bAddr);
		BOOL  SetEndptMax(DWORD iQh, WORD wMaxPacket);
		BOOL  SetEndptHub(DWORD iQh, BYTE bHub, BYTE bPort);
		DWORD MakeEndptCtrl(DWORD iDev, UINT uSpeed);
		DWORD MakeEndptAsync(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		DWORD MakeEndptInt(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc);
		BOOL  KillEndpt(DWORD iQh);
		BOOL  ResetEndpt(DWORD iQh);
		BOOL  Transfer(UsbIor &Urb);
		BOOL  AbortPending(DWORD iQh);
		
		// IUsbHostEnhancedDriver
		BOOL GetGlobalOwner(void);
		BOOL GetPortOwner(UINT iPort);
		void SetGlobalOwner(BOOL fSet);
		void SetPortOwner(UINT iPort, BOOL fSet);

		// IUsbEvents
		void OnBind(IUsbDriver *pDriver);
		
		// IUsbHostHardwareEvents
		void OnEvent(void);

	protected:
		// Structures
		struct CLink;
		struct CTransferDesc;
		struct CQueueHead;
		struct CTransfer;
		
		// Capability Registers
		enum
		{
			capCapLength		= 0x0000 / sizeof(DWORD),
			capHcsParams		= 0x0004 / sizeof(DWORD),
			capHccParams		= 0x0008 / sizeof(DWORD),
			capHcspRoute		= 0x00C0 / sizeof(DWORD),
			};

		// Operational Registers
		enum
		{
			regUsbCmd		= 0x0000 / sizeof(DWORD),
			regUsbSts		= 0x0004 / sizeof(DWORD),
			regUsbIntr		= 0x0008 / sizeof(DWORD),
			regFrIndex		= 0x000C / sizeof(DWORD),
			regCtrlSegt		= 0x0010 / sizeof(DWORD),
			regFrameList		= 0x0014 / sizeof(DWORD),
			regAsyncList		= 0x0018 / sizeof(DWORD),
			regCfgFlag		= 0x0040 / sizeof(DWORD),
			regPortSc		= 0x0044 / sizeof(DWORD),
			};

		// Structural Parameters
		struct CSParams
		{
			DWORD  m_dwPorts	:  4;
			DWORD  m_dwPwrCtrl	:  1;
			DWORD			:  2;
			DWORD  m_dwRouting	:  1;
			DWORD  m_dwNPPC		:  4;
			DWORD  m_dwNCC		:  4;
			DWORD  m_dwPortLeds	:  1;
			DWORD			:  3;
			DWORD  m_dwDebugPort	:  4;
			DWORD			:  8;
			};

		// Capability Parameters
		struct CCParams
		{
			DWORD  m_dwWide		:  1;
			DWORD  m_dwVarFrame	:  1;
			DWORD  m_dwAsyncPark	:  1;
			DWORD			:  1;
			DWORD  m_dwIsoSched	:  4;
			DWORD  m_dwExtCaps	:  8;
			DWORD			: 16;
			};

		// Command Register Bits
		enum 
		{
			cmdRun			= Bit( 0),
			cmdReset		= Bit( 1),
			cmdFrameListSize	= Bit( 2),
			cmdPeriodicEnable	= Bit( 4),
			cmdAsyncEnable		= Bit( 5),
			cmdDoorbell		= Bit( 6),
			cmdLightReset		= Bit( 7),
			cmdAsyncParkMode	= Bit( 8),
			cmdAsyncParkEnable	= Bit(11),
			cmdIntThreshold		= Bit(16),
			};

		// Status Register Bits
		enum 
		{
			statTransfer		= Bit( 0),
			statError		= Bit( 1),
			statPortChange		= Bit( 2),
			statRollover		= Bit( 3),
			statSysErr		= Bit( 4),
			statAsyncAdv		= Bit( 5),
			statHalted		= Bit(12),
			statReclamation		= Bit(13),
			statPeriodic		= Bit(14),
			statAsync		= Bit(15),
			};

		// Interrupts Register Bits
		enum
		{
			intTransfer		= Bit( 0),
			intError		= Bit( 1),
			intPortChange		= Bit( 2),
			intRollover 		= Bit( 3),
			intSysError		= Bit( 4),
			intAsyncAdv		= Bit( 5),
			};

		// Config Flag Register Bits
		enum
		{
			cfgFlag			= Bit( 0),
			};

		// Port Register Bits
		enum
		{
			portConnect		= Bit( 0),
			portConnectChange	= Bit( 1),
			portEnable		= Bit( 2),
			portEnableChange	= Bit( 3),
			portHiCurrent		= Bit( 4),
			portHiCurrentChange	= Bit( 5),
			portResume		= Bit( 6),
			portSuspend		= Bit( 7),
			portReset		= Bit( 8),
			portLineStatus		= Bit(10),
			portPower		= Bit(12),
			portOwner		= Bit(13),
			portIndicator		= Bit(14),
			portTest		= Bit(16),
			portWakeConnect		= Bit(20),
			portWakeDisconnect	= Bit(21),
			portWakeHiCurrent	= Bit(22),
			};

		// PID Codes
		enum
		{
			pidOut			= 0,
			pidIn			= 1,
			pidSetup		= 2,
			};

		// Link Pointer Types
		enum
		{
			ptrIso			= 0,
			ptrQueueHead		= 1,
			ptrSplitIso		= 2,
			prtSpan			= 3,
			};

		// Path Link
		struct CLink
		{
			DWORD		m_dwTerm	  :  1;
			DWORD		m_dwType	  :  2;
			DWORD		                  :  2;
			DWORD		m_dwPtr		  : 27;
			};

		// Transfer Descriptor
		struct CTransferDesc
		{
			CLink		m_Next;
			CLink		m_Alt;
			DWORD volatile	m_dwPing	  :  1;
			DWORD volatile	m_dwSplit	  :  1;
			DWORD volatile	m_dwMissed	  :  1;
			DWORD volatile	m_dwError	  :  1;
			DWORD volatile	m_dwBabble	  :  1;
			DWORD volatile	m_dwBuffer	  :  1;
			DWORD volatile	m_dwHalted	  :  1;
			DWORD volatile	m_dwActive	  :  1;
			DWORD		m_dwPID		  :  2;
			DWORD volatile	m_dwErrorCount	  :  2;
			DWORD volatile	m_dwCurrPage	  :  3;
			DWORD     	m_dwIoc		  :  1;
			DWORD volatile	m_dwTotal	  : 15;
			DWORD volatile	m_dwToggle	  :  1;
			DWORD		m_BufPtr[5];
			};

		// Queue Head
		struct ALIGN(32) CQueueHead
		{
			CLink		m_Link;
			DWORD		m_dwDevAddr	  :  7;
			DWORD		m_dwInactiveReq	  :  1;
			DWORD		m_dwEndpt	  :  4;
			DWORD		m_dwSpeed	  :  2;
			DWORD		m_dwToggleCtrl	  :  1;
			DWORD		m_dwHeadFlag	  :  1;
			DWORD		m_dwMaxPacket	  : 11;
			DWORD		m_dwControl	  :  1;
			DWORD		m_dwNakReload	  :  4;
			DWORD		m_dwFrameSMask	  :  8;
			DWORD		m_dwFrameCMask	  :  8;
			DWORD		m_dwHubAddr	  :  7;
			DWORD		m_dwPortNum	  :  7;
			DWORD		m_dwPipeMult	  :  2;
			DWORD		m_CurrentTDPtr;
			CTransferDesc	m_Transfer;
			};

		// Transfer Request
		struct CTransfer
		{
			CQueueHead    * m_pQh;
			CTransferDesc * m_pTd;
			DWORD		m_dwTdPhy;
			UsbIor        * m_pUrb;
			PBYTE           m_pData;
			CTransfer     * m_pNext;
			CTransfer     * m_pPrev;
			};

		// Resouces
		enum
		{
			constQhLimit	  = 256,
			constTdLimit	  = 1024,
			constTdData	  = 16384,
			constPeriodicSize = 1024,
			constPeriodicPoll = 16,
			};

		// Data
		IBufferManager	* m_pBuffMan;
		IMutex		* m_pLock;
		PCDWORD	          m_pCaps;
		PVDWORD           m_pBase;
		CLink		* m_pPfLink;
		CQueueHead      * m_pPfHead;
		CQueueHead      * m_pQhList;
		CQueueHead	* m_pQhHead;
		CQueueHead      * m_pQhFree;
		ISemaphore	* m_pQhLimit;
		UINT              m_uQhSize;
		CTransferDesc   * m_pTdList;
		CTransfer	* m_pTrList;
		PBYTE		  m_pTrData;
		UINT              m_uTdSize;
		IEvent		* m_pAdvance;
		CTransfer       * m_pUsedHead;
		CTransfer       * m_pUsedTail;
		CTransfer	* m_pFreeList[constTdLimit];
		UINT		  m_iFreeHead;
		UINT		  m_iFreeTail;

		// Init
		void MakeFlags(void);
		void InitController(void);
		void ResetController(void);
		void StartController(void);
		void StopController(void);

		// Capabilities
		UINT	   GetCapsLen(void) const;
		CSParams & GetSParams(void) const;
		CCParams & GetCParams(void) const;

		// Event Handlers
		void OnPortChange(void);
		void OnPortConnect(UINT nPort);
		void OnPortEnable(UINT nPort);
		void OnPortCurrent(UINT nPort);
		void OnTransfer(void);
		void OnAsyncAdvance(void);
		void OnError(void);
		void OnSysError(void);

		// Transfer Descriptors
		void	    InitTransfers(void);
		void	    MakeTransfers(void);
		void	    KillTransfers(void);
		CTransfer * AllocTransfer(CQueueHead *pQh);
		void	    StartTransfer(CQueueHead *pQh, CTransfer *pTr);
		void	    FreeTransfers(void);
		void	    FreeTransfer(CTransfer *pTr);
		void	    FreeTransfers(CQueueHead *pQh);
		void        AbortTransfers(CQueueHead *pQh);
		
		// Async Schedule
		void InitAsync(void);
		void AsyncInsert(CQueueHead *pQh);
		void AsyncRemove(CQueueHead *pQh);
		bool AsyncListed(CQueueHead *pQh);	
		void AsyncWaitAdvance(void);
		void AsyncEnable(BOOL fSet);

		// Perdiodic Schedule
		void InitPeriodic(void);
		void MakePeriodic(void);
		void KillPeriodic(void);
		void PeriodicInsert(CQueueHead *pQh, UINT uPoll);
		void PeriodicRemove(CQueueHead *pQh);
		bool PeriodicListed(CQueueHead *pQh);
		void PeriodicEnable(BOOL fSet);

		// Queue Heads
		void         InitQueueHeads(void);
		void         MakeQueueHeads(void);
		void         KillQueueHeads(void);
		CQueueHead * AllocQueueHead(void);
		void         FreeQueueHead(CQueueHead *pQh);

		// Port Helpers
		void PortPower(BOOL fOn);
		BOOL PortGet(UINT i, DWORD dwMask);
		void PortSet(UINT i, DWORD dwMask, BOOL fSet);
		void PortSet(UINT i, DWORD dwMask);
		void PortClr(UINT i, DWORD dwMask);

		// Link Helpers
		DWORD MemToReg(DWORD  d) const;
		DWORD MemToReg(PCVOID p) const;
		DWORD RegToMem(DWORD  d) const;
		DWORD PtrToLink(PCVOID p) const;
		DWORD PtrToLink(DWORD  d) const;
		DWORD LinkToPtr(DWORD  d) const;

		// Debug
		void DumpHardware(void) const;
		void DumpHardwareCaps(void) const;
		void DumpAsyncList(void) const;
		void DumpPeriodic(void);
		void DumpQueueList(void);
		void DumpQueueHead(CQueueHead const &q) const;
		void DumpTransferDesc(CTransferDesc const &t) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Link Helpers

INLINE DWORD CUsbHostEnhanced::MemToReg(PCVOID p) const
{
	return phal->VirtualToPhysical(DWORD(p));
	}

INLINE DWORD CUsbHostEnhanced::MemToReg(DWORD d) const
{
	return phal->VirtualToPhysical(d);
	}

INLINE DWORD CUsbHostEnhanced::RegToMem(DWORD d) const
{
	return phal->PhysicalToVirtual(d, false);
	}

INLINE DWORD CUsbHostEnhanced::PtrToLink(PCVOID p) const
{
	return PtrToLink(DWORD(p));
	}

INLINE DWORD CUsbHostEnhanced::PtrToLink(DWORD d) const
{
	return d >> 5;
	}

INLINE DWORD CUsbHostEnhanced::LinkToPtr(DWORD d) const
{
	return d << 5;
	}

// End of File

#endif
