
#include "Intern.hpp"

#include "OptionCardList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPortList.hpp"
#include "OptionCardItem.hpp"
#include "UsbTreePath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Option Card List
//

// Dynamic Class

AfxImplementDynamicClass(COptionCardList, CItemIndexList);

// Constructor

COptionCardList::COptionCardList(void)
{
	m_Class = NULL;
	}

// Item Access

COptionCardItem * COptionCardList::GetItem(INDEX Index) const
{
	return (COptionCardItem *) CItemIndexList::GetItem(Index);
	}

COptionCardItem * COptionCardList::GetItem(UINT uSlot) const
{
	return (COptionCardItem *) CItemIndexList::GetItem(uSlot);
	}

// Operations

void COptionCardList::Validate(BOOL fExpand)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n);

		pOption->Validate(fExpand);
		}
	}

void COptionCardList::ClearSysBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n);

		pOption->ClearSysBlocks();
		}
	}

void COptionCardList::NotifyInit(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n);

		pOption->NotifyInit();
		}
	}

void COptionCardList::CheckMapBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n);

		pOption->CheckMapBlocks();
		}
	}

UINT COptionCardList::GetTotalPower(void)
{
	UINT uTotal = 0;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n);

		uTotal += pOption->GetPower();
		}

	return uTotal;
	}

BOOL COptionCardList::HasType(UINT uType) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n);

		if( pOption->GetCardClass() == uType ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Ports List Access

BOOL COptionCardList::GetPortList(CCommsPortList * &pList, UINT uIndex) const
{
	COptionCardItem *pOption = GetItem(uIndex);

	if( pOption ) {

		pList = pOption->m_pPorts;

		return TRUE;
		}

	pList = NULL;

	return FALSE;
	}

// Slot

COptionCardItem * COptionCardList::Find(CUsbTreePath const &Slot) const
{
	INDEX i = GetHead();

	while( !Failed(i) ) {

		COptionCardItem *p = GetItem(i);

		if( p->m_Slot == Slot.dw ) {

			return p;
			}

		GetNext(i);
		}

	return NULL;
	}

COptionCardItem * COptionCardList::Find(UINT Slot) const
{
	INDEX i = GetHead();

	while( !Failed(i) ) {

		COptionCardItem *p = GetItem(i);

		if( p->m_Number == Slot ) {

			return p;
			}

		GetNext(i);
		}

	return NULL;
	}

// Firmware

void COptionCardList::GetFirmwareList(CArray<UINT> &List) const
{
	INDEX i = GetHead();

	while( !Failed(i) ) {

		COptionCardItem *p = GetItem(i);

		if( p->GetFirmwareID() ) {

			List.Append(p->GetFirmwareID());
			}

		GetNext(i);
		}
	}

// End of File
