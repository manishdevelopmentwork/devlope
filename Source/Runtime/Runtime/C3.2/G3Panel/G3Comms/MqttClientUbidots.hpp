
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientUbidots_HPP

#define	INCLUDE_MqttClientUbidots_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceUbidots;

class CMqttClientOptionsUbidots;

//////////////////////////////////////////////////////////////////////////
//
// Ubidots MQTT Client
//

class CMqttClientUbidots : public CMqttClientJson
{
public:
	// Constructor
	CMqttClientUbidots(CCloudServiceUbidots *pService, CMqttClientOptionsUbidots &Opts);

protected:
	// Sub Topics
	enum
	{
		subData = 0,
	};

// Options
	CMqttClientOptionsUbidots &m_Opts;

	// Client Hooks
	void OnClientPhaseChange(void);
	BOOL OnClientNewData(CMqttMessage const *pMsg);

	// Publish Hook
	BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);
};

// End of File

#endif
