
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PxeIpClient_HPP

#define	INCLUDE_PxeIpClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// IP Address List
//

typedef CArray <CIpAddr> CIpList;

//////////////////////////////////////////////////////////////////////////
//
// Multiple IP Client
//

class CPxeIpClient
{
public:
	// Constructor
	CPxeIpClient(UINT uFaces);

	// Destructor
	~CPxeIpClient(void);

protected:
	// Data Members
	UINT		m_uFaces;
	BYTE		m_bOption;
	INetUtilities * m_pUtils;
	UINT		m_uMode;
	CIpList		m_ConfigIps;
	CIpList		m_DefaultIps;
	CIpList		m_Faces;
	CIpList		m_ServerIps;
	UINT		m_uActive;

	// Implementation
	BOOL FindServerList(void);
};

// End of File

#endif
