
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Padder
//

// Constructor

CLayFormPad::CLayFormPad(CLayItem *pItem, UINT Flags)
{
	m_List.Append(pItem);

	m_Flags = Flags;

	m_Pad.Set(4, 4, 4, 4);
	}

CLayFormPad::CLayFormPad(CLayItem *pItem, int nPad, UINT Flags)
{
	m_List.Append(pItem);

	m_Flags = Flags;

	m_Pad.Set(nPad, nPad, nPad, nPad);
	}

CLayFormPad::CLayFormPad(CLayItem *pItem, CRect const &Pad, UINT Flags)
{
	m_List.Append(pItem);

	m_Flags = Flags;

	m_Pad   = Pad;
	}

CLayFormPad::CLayFormPad(CRect const &Pad, UINT Flags)
{
	m_Flags = Flags;

	m_Pad   = Pad;
	}

CLayFormPad::CLayFormPad(int nPad, UINT Flags)
{
	m_Flags = Flags;

	m_Pad.Set(nPad, nPad, nPad, nPad);
	}

// Attributes

CRect CLayFormPad::GetChildRect(void) const
{
	return m_Child;
	}

// Overridables

void CLayFormPad::OnPrepare(CDC &DC)
{
	CSize Size;
	
	Size.cx = m_Pad.left + m_Pad.right;
	
	Size.cy = m_Pad.top + m_Pad.bottom;

	if( m_List.GetCount() ) {

		CLayItem *pItem = m_List[0];

		pItem->Prepare(DC);

		m_MinSize = pItem->GetMinSize() + Size;

		m_MaxSize = pItem->GetMaxSize() + Size;

		if( m_Flags & horzGrow ) {

			m_MaxSize.cx = 10000;
			}

		if( m_Flags & vertGrow ) {

			m_MaxSize.cy = 10000;
			}
		}
	else {
		m_MinSize = Size;

		m_MaxSize = CSize(10000, 10000);
		}
	}

void CLayFormPad::OnSetRect(void)
{
	CRect Rect = m_Rect;

	if( m_Flags & vertTest ) {

		if( Rect.cy() > m_MaxSize.cy ) {

			int nExtra = Rect.cy() - m_MaxSize.cy;

			int nCode  = 0;

			if( m_Flags & vertCenter ) {

				nCode = 1;
				}

			if( m_Flags & vertBottom ) {

				nCode = 2;
				}

			Rect.top    += (0 + nCode) * nExtra / 2;

			Rect.bottom -= (2 - nCode) * nExtra / 2;
			}
		}

	if( m_Flags & horzTest ) {

		if( Rect.cx() > m_MaxSize.cx ) {

			int nExtra = Rect.cx() - m_MaxSize.cx;

			int nCode  = 0;

			if( m_Flags & horzCenter ) {

				nCode = 1;
				}

			if( m_Flags & horzRight ) {

				nCode = 2;
				}

			Rect.left   += (0 + nCode) * nExtra / 2;

			Rect.right  -= (2 - nCode) * nExtra / 2;
			}
		}

	Rect.left   += m_Pad.left;

	Rect.right  -= m_Pad.right;

	Rect.top    += m_Pad.top;

	Rect.bottom -= m_Pad.bottom;

	if( m_List.GetCount() ) {

		CLayItem *pItem = m_List[0];

		pItem->SetRect(Rect);
		}

	m_Child = Rect;
	}

// End of File
