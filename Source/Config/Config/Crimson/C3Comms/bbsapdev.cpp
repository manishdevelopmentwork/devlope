 
#include "intern.hpp"

#include "bbbsap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTagsDeviceOptions, CUIItem);

// Constructor

CBBBSAPTagsDeviceOptions::CBBBSAPTagsDeviceOptions(void)
{
	}

// Attributes

CString CBBBSAPTagsDeviceOptions::GetDeviceName(void)
{
#if defined(C3_VERSION)
	
	return ((CItem *) GetParent())->GetHumanName();

#else
	CString s = ((CNamedItem *) GetParent())->m_Name;

	return s;
#endif
	}

// Operations

BOOL CBBBSAPTagsDeviceOptions::CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fNew, BOOL fUpdate)
{
	INDEX Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		Error.Set(TEXT(STRUSED), 0);

		return FALSE;
		}

	if( fNew ) {

		UINT uOffset = CreateIndex(fUpdate);

		if( m_fSlave ) uOffset |= Addr.a.m_Offset & 0xFC00; // restore List Number

		if( !SetStringOffset(Addr, uOffset) ) {

			Addr.a.m_Offset = uOffset;
			}

		Addr.a.m_Extra  = 0;
		}

	if( IsString(Addr) && !GetIndex(Addr) ) {

		m_StrIndex.Append(CreateIndex(fUpdate));
		}

	m_TagsFwdMap.Insert(Name, Addr.m_Ref);
	m_TagsRevMap.Insert(Addr.m_Ref, Name);

	return TRUE;
	}

void CBBBSAPTagsDeviceOptions::DeleteTag(CAddress const &Addr, BOOL fKill)
{
	INDEX Index = m_TagsFwdMap.FindData(Addr.m_Ref);

	if( !m_TagsFwdMap.Failed(Index) ) {

		CString Name = m_TagsFwdMap.GetName(Index);
		
		m_TagsFwdMap.Remove(Name);

		m_TagsRevMap.Remove(Addr.m_Ref);

		if( fKill ) {

			UINT uIndex = GetIndex(Addr);
		
			m_AddrArray.Append(uIndex);
			}
		}
	}

BOOL CBBBSAPTagsDeviceOptions::RenameTag(CString const &Name, CAddress const &Addr)
{
	CString Old;

	ExpandAddress(Old, Addr);
	
	DeleteTag(Addr, FALSE);

	CError Error(TRUE);

	if( !CreateTag(Error, Name, Addr, FALSE, FALSE) ) {
		
		Error.Show(*afxMainWnd);

		CreateTag(Error, Old, Addr, FALSE, FALSE);

		return FALSE;
		}

	return TRUE;
	}

CString CBBBSAPTagsDeviceOptions::CreateName(CAddress Addr)
{
	CString Text = m_Previous;

	if( Text.IsEmpty() || Text.EndsWith(TEXT(".ext.att")) ) {
	
		CString Type = GetVarType(Addr.a.m_Type);

		Text.Printf(TEXT("%sV%s.ext.att"), Type, TEXT("%u"));

		return CreateName(Text);
		}

	return Text;
	}

void CBBBSAPTagsDeviceOptions::ListTags(CTagDataBArray &List)
{
	INDEX Index = m_TagsFwdMap.GetHead();

	while( !m_TagsFwdMap.Failed(Index) ) {

		CTagDataB Data;

		Data.m_Name  = m_TagsFwdMap.GetName(Index);
		
		Data.m_Addr  = m_TagsFwdMap.GetData(Index);

		Data.m_Index = GetIndex((CAddress &)Data.m_Addr);

		List.Append(Data);
		
		m_TagsFwdMap.GetNext(Index);
		}
	}

void CBBBSAPTagsDeviceOptions::WalkTags(BOOL fForce)
{
	if( fForce || !m_AddrArray.IsEmpty() ) {

		m_StrIndex.Empty();

		CAddress Addr;

		CTagDataBArray List;

		ListTags(List);
		
		List.Sort();

		UINT uCount   = List.GetCount();

		BOOL fRebuild = FALSE;

		for( UINT u = 0, uOffset = 1; u < uCount; u++, uOffset++ ) {

			CTagDataB Data = List.GetAt(u);

			Addr.m_Ref    = Data.m_Addr;

			if( GetIndex(Addr) != uOffset ) {

				m_TagsFwdMap.Remove(Data.m_Name);

				m_TagsRevMap.Remove(Data.m_Addr);

				if( !SetStringOffset(Addr, uOffset) ) {

					Addr.a.m_Offset = uOffset;
					}
				
				m_TagsFwdMap.Insert(Data.m_Name, Addr.m_Ref);

				m_TagsRevMap.Insert(Addr.m_Ref, Data.m_Name);

				fRebuild = TRUE;
				}
			}

		m_AddrArray.Empty();

		if( fRebuild ) {
		
			Rebuild();
			}
		}
	}

void CBBBSAPTagsDeviceOptions::SetPrevious(CString Text)
{
	UINT u = Text.GetLength() - 1;

	while( u && isdigit(Text.GetAt(u)) ) {

		Text = Text.Mid(0, u);

		u--;
		}

	m_Previous = Text;
	}

// Address Management

BOOL CBBBSAPTagsDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CString Name = Text;

	INDEX  Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		Addr.m_Ref = m_TagsFwdMap.GetData(Index);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPTagsDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	CAddress Find = Addr;

	INDEX Index = m_TagsRevMap.FindName(Find.m_Ref);

	if( !m_TagsRevMap.Failed(Index) ) {

		Text = m_TagsRevMap.GetData(Index);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPTagsDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CBBBSAPTagsBaseDriver Driver;
	
	CBBBSAPTagsDialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	if( fPart ) {

		CString Name;

		ExpandAddress(Name, Addr);

		Title = CPrintf("Select Address for Tag %s", Name); 
		}

	else
		Title = CPrintf("Select Variable for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CBBBSAPTagsDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			m_List.Empty();
			
			ListTags(m_List);

			if( !m_List.GetCount() ) {

				return FALSE;
				}
			}
		else {
			if( uItem >= m_List.GetCount() ) {

				return FALSE;
				}
			}
		
		CTagDataB &TagData = (CTagDataB &) m_List[uItem];

		CAddress Addr = (CAddress &) TagData.m_Addr;
			
		Data.m_Name   = TagData.m_Name;
		Data.m_Addr   = Addr;
		Data.m_fPart  = FALSE;

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:      Data.m_Type = TEXT("Flag");  break;

			case addrByteAsByte:	Data.m_Type = TEXT("Byte");  break;
			case addrWordAsWord:	Data.m_Type = TEXT("Word");  break;
			case addrLongAsLong:	Data.m_Type = TEXT("Dword"); break;

			case addrRealAsReal:	Data.m_Type = TEXT("Real");  break;
			}

		return TRUE;
		}

	return FALSE;
	}

// UI Management

void CBBBSAPTagsDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == TEXT("Push") ) {

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					OnImport(pWnd);
					break;

				case 2:
					OnExport(pWnd);
					break;
				}

			return;
			}
		}
	}

// Persistance

void CBBBSAPTagsDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == TEXT("Tags") ) {

			Tree.GetObject();

			LoadTags(Tree);

			Tree.EndObject();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CBBBSAPTagsDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);	

	if( m_TagsFwdMap.GetCount() ) {

		Tree.PutObject(TEXT("Tags"));

		SaveTags(Tree);

		Tree.EndObject();
		}
	}

// Download Support

void CBBBSAPTagsDeviceOptions::PrepareData()
{
	m_Tags.Empty();

	m_PrepAddr.Empty();

	CTagDataBArray List;

	ListTags(List);

	CTagDataBArray LSorted = SortBBList(List);

	for( UINT i = 0; i < LSorted.GetCount(); i++ ) {

		CTagDataB &Data = (CTagDataB &) LSorted[i];

		INDEX Index = m_TagsFwdMap.FindName(Data.m_Name);

		if( !m_TagsFwdMap.Failed(Index) ) {

			m_Tags.Append(Data.m_Name);

			m_PrepAddr.Append(Data.m_Addr);
			}
		}
	}

CTagDataBArray CBBBSAPTagsDeviceOptions::SortBBList(CTagDataBArray List)
{
	CTagDataBArray L2;

	L2.Empty();

	UINT i = 0;

	UINT uCount = List.GetCount();

	UINT uOffset = 1;

	UINT uSlaveMask = m_fSlave ? 0x3FF : 0xFFFF;

	while( i < uCount ) {

		UINT j = 0;

		while( j < uCount ) {

			CTagDataB &Data = (CTagDataB &) List[j];

			CAddress A;

			A.m_Ref        = Data.m_Addr;

			if( (GetIndex(A) & uSlaveMask) == uOffset ) {

				L2.Append(Data);

				uOffset++;

				break;
				}

			else j++;
			}

		i++;
		}

	return L2;
	}

BOOL CBBBSAPTagsDeviceOptions::MakeInitData(CInitData &Init)
{
	#if defined(C3_VERSION)
		
	Init.AddWord((WORD)0xBCDE);

	#else

	Init.AddWord((WORD)0xABCD);

	#endif
		
	UINT uVarCount = m_Tags.GetCount();

	if( TRUE ) {

		Init.AddText(GetDeviceName());

		Init.AddWord(WORD(uVarCount));

		UINT i;

		for( i = 0; i < uVarCount; i ++ ) {

			DWORD d = m_PrepAddr[i];

			Init.AddLong(d);
			}

		UINT uTotalSize = 0;
	
		CArray <CString> m_NameData;

		for( i = 0; i < uVarCount; i ++ ) {

			CString Name = m_Tags[i];

			uTotalSize  += Name.GetLength() + 1; // Null added to end

			m_NameData.Append(Name);
			}

		UINT uSize = m_NameData.GetCount();

		if( (uSize == uVarCount) && uTotalSize ) {

			Init.AddWord(WORD(uTotalSize));

			for( i = 0; i < uSize; i ++ ) {

				Init.AddText(m_NameData[i]);
				};

			UINT uStrIndex = m_StrIndex.GetCount();

			Init.AddWord((WORD)uStrIndex);

			for( i = 0; i < uStrIndex; i ++ ) {

				Init.AddWord((WORD)m_StrIndex[i]);
				}

			return TRUE;
			}
		}
	
	return FALSE;
	}

// Meta Data Creation

void CBBBSAPTagsDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	
	Meta_AddInteger(Push);
	}

// Property Save Filter

BOOL CBBBSAPTagsDeviceOptions::SaveProp(CString Tag) const
{
	if( Tag == TEXT("Push") ) {
		
		return FALSE;
		}

	return CUIItem::SaveProp(Tag);
	}

// Implementation

void CBBBSAPTagsDeviceOptions::OnManage(CWnd *pWnd)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	CBBBSAPTagsBaseDriver Driver;
	
	CBBBSAPTagsDialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Variable Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

void CBBBSAPTagsDeviceOptions::OnImport(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Bristol Babcock BSAP"));

	Dlg.SetCaption(CPrintf("Import Tags for %s", GetDeviceName()));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			if( m_TagsFwdMap.GetCount() ) {
				
				CString Text = TEXT("All tags will be replaced with data from the selected file.\n\nDo you want to continue?");

				if( pWnd->YesNo(Text) == IDYES ) {

					m_TagsFwdMap.Empty();
					
					m_TagsRevMap.Empty();
				
					m_AddrArray.Empty();

					m_StrIndex.Empty();
					}
				else {
					fclose(pFile);
					
					return;
					}
				}

			afxThread->SetWaitMode(TRUE);

			if( Import(pFile) ) {

				Rebuild();

				SetDirty();

				Dlg.SaveLastPath(TEXT("Bristol Babcock BSAP"));
				}

			else ShowErrors(pWnd);
			
			fclose(pFile);

			afxThread->SetWaitMode(FALSE);
			}
		}
	}

void CBBBSAPTagsDeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("Bristol Babcock BSAP"));

	Dlg.SetCaption(CPrintf("Export Tags for %s", GetDeviceName()));

	Dlg.SetFilter (TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\r\nDo you want to overwrite it?");

			if( pWnd->YesNo(Text) == IDNO ) {
			
				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = TEXT("Unable to open file for writing.");

			pWnd->Error(Text);

			return;
			}

		Export(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(TEXT("Bristol Babcock BSAP"));
		}
	}

CString CBBBSAPTagsDeviceOptions::CreateName(PCTXT pFormat)
{
	for( UINT n = 1; ; n++ ) {

		CPrintf Name(pFormat, n);

		INDEX Index = m_TagsFwdMap.FindName(Name);

		if( m_TagsFwdMap.Failed(Index) ) {
			
			return Name;
			}
		}
	}

UINT CBBBSAPTagsDeviceOptions::CreateIndex(BOOL fUpdate)
{
	if( !fUpdate && m_AddrArray.GetCount() ) {

		m_AddrArray.Sort();

		UINT n = m_AddrArray[0];

		m_AddrArray.Remove(0);

		return n;
		}

	return GetNewIndex();
	}

DWORD CBBBSAPTagsDeviceOptions::CreateIndexS(BOOL fUpdate, DWORD Ref)
{
	if( !fUpdate && m_AddrArray.GetCount() ) {

		m_AddrArray.Sort();

		UINT n = m_AddrArray[0];

		m_AddrArray.Remove(0);

		return n;
		}

	return GetNewIndexS(Ref);
	}

UINT CBBBSAPTagsDeviceOptions::GetNewIndex(void)
{
	CTagDataBArray List;

	ListTags(List);

//	List.Sort();

	UINT uIndex = 1;

	UINT uCount = List.GetCount();

	for( UINT i = 0; i < uCount; i ++ ) {

		INDEX Index = m_TagsFwdMap.GetHead();

		while( !m_TagsFwdMap.Failed(Index) ) {

			CTagDataB Data;
		
			Data.m_Addr   = m_TagsFwdMap.GetData(Index);

			CAddress Addr = (CAddress &) Data.m_Addr;

			if( GetIndex(Addr) == uIndex ) {

				break;
				}

			m_TagsFwdMap.GetNext(Index);
			}

		uIndex++;
		}

	return uIndex;
	}

DWORD CBBBSAPTagsDeviceOptions::GetNewIndexS(DWORD Ref)
{
	CTagDataBArray List;

	ListTags(List);

	UINT uIndex = 1;

	UINT uCount = List.GetCount();

	DWORD dMask = Ref & 0xFFFF03FF; // remove list number

	for( UINT i = 0; i < uCount; i ++ ) {

		INDEX Index = m_TagsFwdMap.GetHead();

		while( !m_TagsFwdMap.Failed(Index) ) {

			CTagDataB Data;
		
			Data.m_Addr   = m_TagsFwdMap.GetData(Index);

			CAddress Addr = (CAddress &) Data.m_Addr;

			Addr.m_Ref   &= 0xFFFF03FF;      // remove list number

			if( Addr.m_Ref == (dMask + uIndex) ) break; // next index

			m_TagsFwdMap.GetNext(Index);
			}

		uIndex++;
		}

	Ref &= 0xF0FFFFFF; // Extra = 0;

	return Ref + uIndex;
	}

CString CBBBSAPTagsDeviceOptions::GetVarType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:	return TEXT("f");
		case addrByteAsByte:	return TEXT("b");
		case addrWordAsWord:	return TEXT("w");
		case addrRealAsReal:	return TEXT("r");
		}

	return TEXT("d");
	}

void CBBBSAPTagsDeviceOptions::SaveTags(CTreeFile &Tree)
{
	WalkTags(TRUE);

	CTagDataBArray List;
	
	ListTags(List);

	CTagDataBArray LSorted = SortBBList(List);

	for( UINT n = 0; n < LSorted.GetCount(); n ++ ) {
		
		Tree.PutValue(TEXT("Name"), LSorted[n].m_Name);

		Tree.PutValue(TEXT("Data"), LSorted[n].m_Addr);
		}
	}

void CBBBSAPTagsDeviceOptions::LoadTags(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == TEXT("Name") ) {
			
			CString Name = Tree.GetValueAsString();

			Tree.GetName();

			CAddress Addr;

			Addr.m_Ref = Tree.GetValueAsInteger();

			CreateTag( CError(FALSE), Name, Addr, FALSE, FALSE);

			continue;
			}
		}
	}

void CBBBSAPTagsDeviceOptions::Rebuild(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		pSystem->Rebuild(1);
		}
	}

CString CBBBSAPTagsDeviceOptions::GetTagName(CString const &Text)
{
	return Text;
	}

WORD CBBBSAPTagsDeviceOptions::GetIndex(CAddress const &Addr)
{
	if( IsString(Addr) ) {

		UINT uOffset = Addr.a.m_Offset / (MAX_CHARS / sizeof(DWORD));

		if( uOffset < m_StrIndex.GetCount() ) {

			return (WORD)m_StrIndex.GetAt(uOffset);
			}

		return 0;
		}

	return Addr.a.m_Offset;
	}

BOOL CBBBSAPTagsDeviceOptions::IsString(CAddress Addr)
{
	return Addr.a.m_Table >= BTYPESTR;
	}

BOOL CBBBSAPTagsDeviceOptions::SetStringOffset(CAddress &Addr, UINT uIndex)
{
	if( IsString(Addr) ) {

		Addr.a.m_Offset = m_StrIndex.GetCount() * (MAX_CHARS / sizeof(DWORD));

		m_StrIndex.Append(uIndex);

		return TRUE;
		}

	return FALSE;
	}

// Import/Export Support

void CBBBSAPTagsDeviceOptions::Export(FILE *pFile)
{
	CTagDataBArray List;

	ListTags(List);

	List.Sort();

	fprintf(pFile, TEXT("%s%s\n"), TEXT(STRHDR), PCTXT(GetDeviceName()));

	fprintf(pFile, TEXT("NAME,INDEX,TYPE\n"));

	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		CString  Name = List[n].m_Name;

		CAddress Addr = (CAddress &) List[n].m_Addr;

		CString Type;

		ExpandType(Type, Addr);

		fprintf(pFile, TEXT("%s,%d,%s\n"), PCTXT(Name), GetIndex(Addr), PCTXT(Type));
		}
	}

BOOL CBBBSAPTagsDeviceOptions::Import(FILE *pFile)
{
	m_Errors.Empty();

	DWORD dwPos = ftell(pFile);

	UINT uLine = 0;

	fseek(pFile, dwPos, SEEK_SET);

	while( !feof(pFile) ) {

		char sLine[256] = {0};

		fgets(sLine, sizeof(sLine), pFile);

		if( !uLine ) {

			sLine[strlen(sLine)-1] = 0;

			CString ID  = CString(sLine);

			CString Hdr = STRHDR;

			UINT hlen   = Hdr.GetLength();

			if( ID.Left(hlen) != STRHDR ) {

				return FALSE;
				}

			fgets(sLine, sizeof(sLine), pFile); // "NAME,INDEX,TYPE"

			fgets(sLine, sizeof(sLine), pFile); // First Entry

			uLine = 2;
			}

		if( sLine[0] ) {

			uLine ++;

			CStringArray List;

			CString csLine = FixLine(CString(sLine));

			if( !csLine[0] ) {

				csLine = CPrintf("Bad_Format_Line_%d,0,XXX", uLine );
				}

			csLine.Tokenize(List, ',');

			if( List.GetCount() == 3 ) {
					     
				CString Name  = List[0];

				CString Index = List[1];

				CString Type  = List[2];

				Name.Remove('"');
				Index.Remove('"');
				Type.Remove('"');

				if(	Name.IsEmpty()  ||
					Index.IsEmpty() ||
					Type.IsEmpty()  ||
					Name.GetLength() > MAX_TAG_LENGTH
					) {

					m_Errors.Append(uLine);

					continue;
					}
					
				CAddress Addr;

				Addr.a.m_Offset = tatoi(Index);
				Addr.a.m_Extra  = 0;

				BOOL fGoodType = ParseType(Addr, Type);

				switch( Addr.a.m_Type ) {

					case addrBitAsBit:	Addr.a.m_Table = SPBIT;		break;
					case addrByteAsByte:	Addr.a.m_Table = SPBYTE;	break;
					case addrWordAsWord:	Addr.a.m_Table = SPWORD;	break;
					case addrLongAsLong:	Addr.a.m_Table = SPLONG;	break;
					case addrRealAsReal:	Addr.a.m_Table = SPREAL;	break;
					}

				if( Type == TEXT("STRING") ) {

					Addr.a.m_Table = SPSTR;

					SetStringOffset(Addr, tatoi(Index));
					}

				BOOL fOk = CreateTag(CError(FALSE), Name, Addr, !GetIndex(Addr), FALSE);

				if( !fOk || !fGoodType ) {
						
					if( m_Errors.Find(uLine) == NOTHING ) {

						m_Errors.Append(uLine);
						}
					}
				}
			}
		}

	WalkTags(TRUE);

	return m_Errors.GetCount() == 0;
	}

CString CBBBSAPTagsDeviceOptions::FixLine(CString sCheck)
{
	UINT uPos = 0;

	sCheck.MakeUpper();

	if( NextComma(sCheck, &uPos) && NextComma(sCheck, &uPos) ) {

		if( !CheckItem(sCheck.Mid(uPos, 3), TEXT("BIT")) ) {

			return sCheck.Left(uPos + 3);
			}

		CString s1 = sCheck;

		CString sChk = s1.Mid(uPos, 4);

		if( !CheckItem(sChk, TEXT("BYTE")) ||
		    !CheckItem(sChk, TEXT("WORD")) ||
		    !CheckItem(sChk, TEXT("LONG")) ||
		    !CheckItem(sChk, TEXT("REAL"))
		    ) {
			return sCheck.Left(uPos + 4);
			}

		if( !CheckItem(sChk, TEXT("STRING")) ) {

			return sCheck.Left(uPos + 6);
			}

		sChk = s1.Mid(uPos, 5);

		if( !CheckItem(sChk, TEXT("DWORD")) ||
		    !CheckItem(sChk, TEXT("UINT3")) ||
		    !CheckItem(sChk, TEXT("SINT3"))
		    )
			return sCheck.Left(uPos) + TEXT("LONG");

		if( !CheckItem(sChk, TEXT("UINT")) ||
		    !CheckItem(sChk, TEXT("SINT"))
		    ) {
			return sCheck.Left(uPos) + TEXT("WORD");
			}

		if( !CheckItem(sChk, TEXT("FLOAT")) ) {

			return sCheck.Left(uPos) + TEXT("REAL");
			}
		}

	return "";
	}

BOOL CBBBSAPTagsDeviceOptions::NextComma(CString sCheck, UINT *pPos)
{
	UINT uPos = *pPos;

	uPos      = sCheck.Find(',', uPos);

	if( uPos < NOTHING ) {

		*pPos = uPos + 1;

		return TRUE;
		}

	return FALSE;
	}

UINT CBBBSAPTagsDeviceOptions::CheckItem(PCTXT sCheck, PCTXT sItem)
{
#if defined(C3_VERSION)

	WORD len = (WORD)wstrlen(sCheck);

	len      = min(len, (WORD)wstrlen(sItem));

 	return wcsncmp(PTXT(sCheck), PTXT(sItem), len);

#else
	return strcmp(sCheck, sItem);

#endif
	}


BOOL CBBBSAPTagsDeviceOptions::ParseType(CAddress &Addr, CString Text)
{
	UINT uType = addrWordAsWord;

	Text.MakeUpper();

	PCTXT pTxt = Text;

	switch( Text[0] ) {

		case 'W':
			uType = addrWordAsWord;
			break;

		case 'L':
		case 'S':
			uType = addrLongAsLong;
			break;

		case 'R':
			uType = addrRealAsReal;
			break;

		case 'B':
			uType = Text[1] == 'Y' ? addrByteAsByte : addrBitAsBit;
			break;

		case 'X':
			if( !strcmp((const char *)pTxt, "XXX") ) {

				Addr.a.m_Type = addrBitAsBit;

				return FALSE;
				}
			break;
		}

	Addr.a.m_Type = uType;

	return TRUE;
	}

void CBBBSAPTagsDeviceOptions::ExpandType(CString &Text, CAddress const &Addr)
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	Text = TEXT("BIT");  return;
		case addrByteAsByte:	Text = TEXT("BYTE"); return;
		case addrWordAsWord:	Text = TEXT("WORD"); return;
		case addrRealAsReal:	Text = TEXT("REAL"); return;
		}

	if( IsString(Addr) ) {

		Text = TEXT("STRING");

		return;
		}

	Text = TEXT("LONG");
	}

void CBBBSAPTagsDeviceOptions::ShowErrors(CWnd *pWnd)
{
	if( m_Errors.GetCount() ) {
		
		afxThread->SetWaitMode(TRUE);
		
		CString Lines;

		UINT uErrMax = 101;
		
		for( UINT i = 0; i < m_Errors.GetCount(); ) {

			if( i )
				Lines += ", ";
			
			Lines += CPrintf("%d", m_Errors[i]);

			if( ++ i >= uErrMax ) {

				CString Text = TEXT("...\nFile contains more than %d errors");
				
				Lines += CPrintf(Text, uErrMax);

				break;
				}
			}

		afxThread->SetWaitMode(FALSE);

		CString Text1 = TEXT("The following lines in the file contained errors :\n\n%s.");
		CString Text2 = TEXT("\n\nIt is possibly a duplicate/invalid name or a bad data type.");
		CString Text3 = TEXT("\n\nThe lines without errors were still imported.");

		CString Text  = Text1 + Text2 + Text3;

		pWnd->Error(CPrintf(Text, Lines));
		}
	else {
		CString Text = TEXT("The operation completed without error.");

		pWnd->Information(Text);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTagsTCPDeviceOptions, CUIItem);

// Constructor

CBBBSAPTagsTCPDeviceOptions::CBBBSAPTagsTCPDeviceOptions(void)
{
	m_fSlave = FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTagsTCPSDeviceOptions, CUIItem);

// Constructor

CBBBSAPTagsTCPSDeviceOptions::CBBBSAPTagsTCPSDeviceOptions(void)
{
	m_fSlave = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTagsSerialDeviceOptions, CUIItem);

// Constructor

CBBBSAPTagsSerialDeviceOptions::CBBBSAPTagsSerialDeviceOptions(void)
{
	m_fSlave = FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTagsSerialSDeviceOptions, CUIItem);

// Constructor

CBBBSAPTagsSerialSDeviceOptions::CBBBSAPTagsSerialSDeviceOptions(void)
{
	m_fSlave = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTCPDeviceOptions, CBBBSAPTagsTCPDeviceOptions);

// Constructor

CBBBSAPTCPDeviceOptions::CBBBSAPTCPDeviceOptions(void)
{
	m_Addr		= DWORD(MAKELONG(MAKEWORD(20, 21), MAKEWORD(16, 172) ));

	m_Socket	= 1234;

	m_Keep		= TRUE;

	m_Time1		= 5000;

	m_Time2		= 2500;

	m_Time3		= 200;
	}

// UI Management

void CBBBSAPTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}

	CBBBSAPTagsTCPDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBBBSAPTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	CBBBSAPTagsTCPDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBBBSAPTCPDeviceOptions::AddMetaData(void)
{
	CBBBSAPTagsTCPDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP/IP Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTCPSDeviceOptions, CBBBSAPTagsTCPSDeviceOptions);

// Constructor

CBBBSAPTCPSDeviceOptions::CBBBSAPTCPSDeviceOptions(void)
{
	}

// UI Management

void CBBBSAPTCPSDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBBBSAPTagsTCPSDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBBBSAPTCPSDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	CBBBSAPTagsTCPSDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBBBSAPTCPSDeviceOptions::AddMetaData(void)
{
	CBBBSAPTagsTCPSDeviceOptions::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPSerialDeviceOptions, CBBBSAPTagsSerialDeviceOptions);

// Constructor

CBBBSAPSerialDeviceOptions::CBBBSAPSerialDeviceOptions(void)
{
	m_Addr	= 1;
	}

// UI Management

void CBBBSAPSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBBBSAPTagsSerialDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBBBSAPSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));

	CBBBSAPTagsSerialDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBBBSAPSerialDeviceOptions::AddMetaData(void)
{
	CBBBSAPTagsSerialDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPSerialSDeviceOptions, CBBBSAPTagsSerialSDeviceOptions);

// Constructor

CBBBSAPSerialSDeviceOptions::CBBBSAPSerialSDeviceOptions(void)
{
	}

// UI Management

void CBBBSAPSerialSDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	CBBBSAPTagsSerialSDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBBBSAPSerialSDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	CBBBSAPTagsSerialSDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBBBSAPSerialSDeviceOptions::AddMetaData(void)
{
	CBBBSAPTagsSerialSDeviceOptions::AddMetaData();
	}

// End of File
