
#include "intern.hpp"

#include "abbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Base Driver
//

// Constructor

CABDriver::CABDriver(void)
{
	}

// Destructor

CABDriver::~CABDriver(void)
{
	DeleteAllSpaces();
	}

// Driver Data

UINT CABDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagImport;
	}

// Tag Import

BOOL CABDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CAbTagsCreator AbTags(DevName);

	AbTags.SetParent(pConfig);

	AbTags.GetTagConfig(pConfig);
	
	CItemDialog TagDlg(&AbTags, CString(IDS_AB_TAG));

	if( TagDlg.Execute(*afxMainWnd) ) {

		COpenFileDialog Dlg;

		Dlg.LoadLastPath(L"RSLogix");

		Dlg.SetCaption(CString(IDS_CREATE_TAGS_FROM));

		Dlg.SetFilter (CString(IDS_RSLOGIX_EXPORT));

		if( Dlg.Execute(*afxMainWnd) ) {

			CString   Root = AbTags.m_Root;

			CFilename File = Dlg.GetFilename();

			if( TRUE ) {

				CTextStreamMemory Stm;

				Stm.SetPrev();

				if( Stm.LoadFromFile(File) ) {

					if( pTags->InitImport(Root, TRUE, File) ) {

						WCHAR sLine[1024];

						while( Stm.GetLine(sLine, elements(sLine)) ) {

							CStringArray List;

							CString(sLine).Tokenize(List, L',', L'"');

							CreateTag(pTags, List, AbTags);
							}

						pTags->ImportDone(TRUE);

						AbTags.SaveTagConfig(pConfig);
						}
					}
				else {
					CString Text = CString(IDS_UNABLE_TO_OPEN);

					afxMainWnd->Error(Text);

					return FALSE;
					}
				}

			Dlg.SaveLastPath(L"RSLogix");

			return TRUE;
			}
		}

	return FALSE;
	}

// Address Management

BOOL CABDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	Text.MakeUpper();

	CSpaceAB *pSpace = GetSpace(Text);

	if( pSpace ) {

		UINT uPos = Text.Find(':');
		
		if( uPos == NOTHING ) {
		
			Error.Set( CString(IDS_DRIVER_ADDR_COLON),
				   0
				   );
				   
			return FALSE;
			}

		UINT uType = pSpace->m_uType;

		UINT uFile = tatoi(Text.Mid(1, uPos - 1));
	
		UINT uAddr = tatoi(Text.Mid(uPos + 1));

		UINT uCode = pSpace->m_uTable;

		UINT uSub = 0;

		if( pSpace->m_fTriplet ) {

			uPos = Text.Find('.');

			if( uPos == NOTHING ) {

				Error.Set( CString(IDS_AB_ADDR_PERIOD),
					   0
					   );

				return FALSE;
				}

			uSub = tatoi(Text.Mid(uPos + 1));
			}
		
		if( pSpace->m_FileDef != AB_FILE_NONE ) {
			
			if( uFile != pSpace->m_FileDef  ) {
				
				if( uFile < pSpace->m_FileMin || uFile > pSpace->m_FileMax ) {

					CString Text;

					Text.Printf( CString(IDS_AB_FILE_RANGE), 
						     pSpace->m_FileDef,
						     pSpace->m_FileMin,
						     pSpace->m_FileMax
						     );
				
					Error.Set( Text,
						   0
						   );
						   
					return FALSE;
					}
				}
			}
		else {	
			// Add IO file support.
			
			if( ( uCode == 1 ) || ( uCode == 2 ) ) {

				uFile = 0;
				}
			else {
				uFile = AB_FILE_NONE; 
				}
			}
	
		if( uAddr > AB_ADDR_MAX ) {
		
			Error.Set( CString(IDS_AB_ADDR_RANGE),
				   0
				   );
				   
			return FALSE;
			}

		if ( pSpace->m_fTriplet ) {

			uAddr = uAddr * 3 + uSub;
			}

		Addr.a.m_Offset = uAddr;
		Addr.a.m_Table  = uFile;
		Addr.a.m_Extra  = uCode;
		Addr.a.m_Type   = uType;

		if( IsString(uCode) ) {

			Addr.a.m_Offset *= AB_STR_MAX;
			}

		return TRUE;
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CABDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpaceAB *pSpace = GetSpace(Addr);

		UINT uFile = Addr.a.m_Table;
		
		UINT uAddr = Addr.a.m_Offset;

		if( IsString(Addr.a.m_Extra) ) {

			uAddr /= AB_STR_MAX;
			}

		if( uFile == AB_FILE_NONE ) {

			Text.Printf( "%s:%s",   
				     pSpace->m_Prefix,
				     pSpace->GetValueAsText(uAddr)
				     );
			}
		else {
			if( pSpace->m_fTriplet ) {
				
				Text.Printf( "%s%3.3d:%s.%2.2u", 
					     pSpace->m_Prefix,
					     uFile, 
					     pSpace->GetValueAsText(uAddr / 3),
					     uAddr % 3
					     );
				}
			
			else {
				Text.Printf( "%s%3.3d:%s", 
					     pSpace->m_Prefix,
					     uFile, 
					     pSpace->GetValueAsText(uAddr)
					     );
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CABDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CABDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {
		
		if( m_List.GetCount() ) {

			if( uItem == 0 ) {

				m_n = m_List.GetHead();
				}
			else {
				if( !m_List.GetNext(m_n) ) {

					return FALSE;
					}
				}

			CSpaceAB *pSpace = (CSpaceAB *) m_List[m_n];
			
			Data.m_Name           = pSpace->m_Caption;
			Data.m_Addr.a.m_Type  = pSpace->m_uType;
			Data.m_Addr.a.m_Table = pSpace->m_FileDef;
			Data.m_Addr.a.m_Extra = pSpace->m_uTable;
			Data.m_fPart          = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

// Space List Access

CSpaceAB * CABDriver::GetSpace(INDEX Index)
{
	return (CSpaceAB *) CStdCommsDriver::GetSpace(Index);
	}

CSpaceAB * CABDriver::GetSpace(CAddress const &Addr)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CSpace *pSpace = m_List[n];

			UINT         uExtra = Addr.a.m_Extra;

			if( pSpace->m_uTable == uExtra ) {

				return (CSpaceAB *) pSpace;
				}

			m_List.GetNext(n);
			}
		}
		
	return NULL;
	}

CSpaceAB * CABDriver::GetSpace(CString Text)
{
	return (CSpaceAB *) CStdCommsDriver::GetSpace(Text);
	}

// Helpers

BOOL CABDriver::IsString(UINT uCode)
{
	return (uCode == 10);
	}

// Implementation

void CABDriver::CreateTag(IMakeTags *pTags, CStringArray const &List, CAbTagsCreator &AbTags)
{
	UINT uHead = List[0].Find(':');

	if( uHead < NOTHING ) {

		CString Head = List[0].Left(uHead);

		if( GetSpace(Head) ) {

			CString Name[2];

			CString Value = L'[' + List[0] + L']';

			UINT uBit  = NOTHING;

			BOOL fRead = FALSE;

			for( UINT u = 0;  u < 2; u++ ) {

				UINT uIndex = 0;

				AbTags.GetNext(uIndex, u);

				Name[u] = L"";

				while ( uIndex > 0 )
				{
				      	CString Text = List[uIndex];

					if( uIndex == 1 ) {

						for( UINT n = 0; List[0][n]; n++ ) {

							if( isalnum(List[0][n]) ) {

								Name[u] += List[0][n];
								}
							else
								Name[u] += '_';
							}
						}

					else if( !Text.IsEmpty() ) {

						if( Text[0] == '"' ) {

							Text = Text.Mid(1, Text.GetLength() - 2);
							
							Text.Replace(L"\"\"", L"\"");
							}

						if( !Text.IsEmpty() ) {

							if( !Name[u].IsEmpty() ) {

								Name[u] += L'_';
								}

							Text.TrimBoth();

							for( UINT n = 0; Text[n]; n++ ) {

								if( isalnum(Text[n]) ) {

									Name[u] += Text[n];
									}
								else
									Name[u] += L'_';
								}
							}
						}
					
					AbTags.GetNext(uIndex, u);
					} 
				}

			if( Name[TAG_NAME ].IsEmpty() ) {

				for( UINT n = 0; List[0][n]; n++ ) {

					if( isalnum(List[0][n]) ) {

						Name[TAG_NAME] += List[0][n];
						}
					else
						Name[TAG_NAME] += '_';
					}
				}

			if( Name[TAG_LABEL].IsEmpty() ) {

				Name[TAG_LABEL] = Name[TAG_NAME];
				}
		
			if( List[0][0] == 'C' ) {

				if( !ParseCount(Value, uBit, fRead, List[0]) ) {

					return;
					}
				}

			if( List[0][0] == 'T' ) {

				if( !ParseTimer(Value, uBit, fRead, List[0]) ) {

					return;
					}
				}

			if( Value.Find('/') < NOTHING ) {

				UINT uFind = List[0].Find('/');

				Value = L'[' + List[0].Left(uFind) + L']';

				uBit  = watoi(List[0].Mid(uFind+1));
				}

			if( uBit < NOTHING ) {

				pTags->AddFlag( Name[TAG_NAME],
						Name[TAG_LABEL],
						Value,
						0,
						MAKEWORD(3, uBit),
						!fRead,
						L"",
						L""
						);
				}
			else {
				if( List[0][0] == 'F' ) {

				pTags->AddReal( Name[TAG_NAME],
						Name[TAG_LABEL],
						Value,
						0,
						!fRead,
						L"",
						L"",
						0
						);
					}
				else {
					pTags->AddInt( Name[TAG_NAME],
						       Name[TAG_LABEL],
						       Value,
						       0,
						       !fRead,
						       L"",
						       L"",
						       0,
						       0
						       );
					}
				}
			}
		} 
	}

BOOL CABDriver::ParseCount(CString &Value, UINT &uBit, BOOL &fRead, CString const &Text)
{
	UINT uDot = Text.Find('.');

	UINT uDiv = Text.Find('/');

	if( uDot < NOTHING ) {

		CString Sub  = Text.Mid(uDot+1);

		if( Sub == L"PRE" ) {

			Value = L'[' + Text.Left(uDot) + L".01]";

			return TRUE;
			}

		if( Sub == L"ACC" ) {

			Value = L'[' + Text.Left(uDot) + L".02]";

			return TRUE;
			}
		}

	if( uDiv < NOTHING ) {

		CString Sub = Text.Mid(uDiv+1);

		if( Sub == L"OV" ) {

			Value = L'[' + Text.Left(uDiv) + L".00]";

			uBit  = 12;

			fRead = TRUE;

			return TRUE;
			}

		if( Sub == L"DN" ) {

			Value = L'[' + Text.Left(uDiv) + L".00]";

			uBit  = 13;

			fRead = TRUE;

			return TRUE;
			}

		if( Sub == L"CU" ) {

			Value = L'[' + Text.Left(uDiv) + L".00]";

			uBit  = 15;

			fRead = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CABDriver::ParseTimer(CString &Value, UINT &uBit, BOOL &fRead, CString const &Text)
{
	UINT uDot = Text.Find('.');

	UINT uDiv = Text.Find('/');

	if( uDot < NOTHING ) {

		CString Sub  = Text.Mid(uDot+1);

		if( Sub == L"PRE" ) {

			Value = L'[' + Text.Left(uDot) + L".01]";

			return TRUE;
			}

		if( Sub == L"ACC" ) {

			Value = L'[' + Text.Left(uDot) + L".02]";

			return TRUE;
			}
		}

	if( uDiv < NOTHING ) {

		CString Sub = Text.Mid(uDiv+1);

		if( Sub == L"DN" ) {

			Value = L'[' + Text.Left(uDiv) + L".00]";

			uBit  = 13;

			fRead = TRUE;

			return TRUE;
			}

		if( Sub == L"TT" ) {

			Value = L'[' + Text.Left(uDiv) + L".00]";

			uBit  = 14;

			fRead = TRUE;

			return TRUE;
			}

		if( Sub == L"EN" ) {

			Value = L'[' + Text.Left(uDiv) + L".00]";

			uBit  = 15;

			fRead = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CABDialog, CStdDialog);
		
// Constructor

CABDialog::CABDialog(CABDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	m_uDevice = 1;
	
	if( pConfig ) {

		IDataAccess *pData = pConfig->GetDataAccess("Device");

		if( pData ) {
	
			m_uDevice = pData->ReadInteger(pConfig);
			}
		}
	
	SetName(fPart ? L"PartAllenBradleyDlg" : L"FullAllenBradleyDlg");
	}

// Message Map

AfxMessageMap(CABDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk   )
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSelChange)

	AfxMessageEnd(CABDialog)
	};

// Message Handlers

BOOL CABDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

	Combo.AddString(L"0 - STAT");

	Combo.AddString(L"1 - PRS");

	Combo.AddString(L"2 - ACC");

	UINT uOffset = m_pAddr->a.m_Offset;

	if( m_pDriver->IsString(m_pAddr->a.m_Extra) ) {

		uOffset /= AB_STR_MAX;
		}
		
	if( m_fPart ) {

		FindSpace();
		
		ShowAddress(m_pAddr->a.m_Table, uOffset);

		SetDlgFocus(2002);
		}
	else {
		SetCaption();

		LoadList();

		ShowAddress(m_pAddr->a.m_Table, uOffset);

		SetDlgFocus(m_pAddr->m_Ref ? 2002 : 1001);
		}

	return FALSE;
	}

// Notification Handlers

void CABDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CABDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uFind = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uFind));

	if( (DWORD &) Index != NOTHING ) {

		m_pSpace = m_pDriver->GetSpace(Index);

		ShowAddress(m_pSpace->m_FileDef, m_pSpace->m_uMinimum);

		ShowDetails();

		return;
		}

	m_pSpace = NULL;

	ClearAddress();

	ClearDetails();
	}

// Command Handlers

BOOL CABDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		UINT uOffset = GetOffset();

		if( !ValidOffset(uOffset) ) {
			
			SetDlgFocus(2002);
				
			return TRUE;
			}

		CError   Error;

		CString  Text;

		CAddress Addr;

		if( m_pSpace->m_FileDef == AB_FILE_NONE ) {
		
			Text.Printf( "%s:%s", 
				     m_pSpace->m_Prefix,
				     m_pSpace->GetValueAsText(uOffset));
			}
		else {
			if ( m_pSpace->m_fTriplet ) {

				CComboBox &Combo = (CComboBox &) GetDlgItem(2005);
				
				Text.Printf( "%s%s:%s.%u", 
					     m_pSpace->m_Prefix,
					     GetDlgItem(2003).GetWindowText(),
					     GetDlgItem(2002).GetWindowText(),
					     Combo.GetCurSel()
					     );
				}
			else {
				Text.Printf( "%s%s:%s", 
					     m_pSpace->m_Prefix,
					     GetDlgItem(2003).GetWindowText(), 
					     m_pSpace->GetValueAsText(uOffset)
					     );
				}
			}
		
		if( m_pDriver->ParseAddress(Error, Addr, NULL, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			};

		Error.Show(ThisObject);

		SetDlgFocus(2003);

		return FALSE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CABDialog::SetCaption(void)
{
	CString Text;

	Text.Printf( CString(IDS_DRIVER_CAPTION), 
		     GetWindowText(), 
		     m_pDriver->GetString(stringShortName)
		     );

	SetWindowText(Text);
	}

void CABDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);
	
	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	int nTab[] = { 32, 96, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>\t%s", CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING );

	CSpaceList &List = m_pDriver->GetSpaceList();

	INDEX            Find = INDEX(NOTHING);

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpaceAB *pSpace = (CSpaceAB *) List[n];

			if( AllowSpace(pSpace) ) {

				CString Entry;
			
				Entry.Printf("%s\t%s", pSpace->m_Prefix, pSpace->m_Caption);

				ListBox.AddString(Entry, DWORD(n) );

				if( m_pAddr->a.m_Extra == pSpace->m_uTable ) {

					Find = n;
					}
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSelChange(1001, ListBox);
	}

void CABDialog::FindSpace(void)
{
	m_pSpace = (CSpaceAB *) m_pDriver->GetSpace(*m_pAddr);
	}

UINT CABDialog::GetOffset(void)
{
	CString Text = GetDlgItem(2002).GetWindowText();

	UINT uOffset = (UINT) wcstoul(Text, NULL, m_pSpace->m_uRadix);

	if( m_pSpace->m_fTriplet ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

		uOffset *= 3;

		uOffset += Combo.GetCurSel();
		}

       	return uOffset;
	}

BOOL CABDialog::ValidOffset(UINT uOffset)
{
	if( m_pSpace->m_fTriplet ) {

		uOffset /= 3;
		}
	
	if( m_pSpace->IsOutOfRange(uOffset) ) {

		UINT uMin = m_pSpace->m_uMinimum;

		UINT uMax = m_pSpace->m_uMaximum;

		CString Text;
		
		Text = CPrintf( CString(IDS_DRIVER_ADDR_RANGE), 
				m_pSpace->GetValueAsText(uMin), 
				m_pSpace->GetValueAsText(uMax)
				);

		Error(Text);

		return FALSE;
		}

	if( m_pSpace->IsBadAlignment(uOffset) ) {

		Error(CString(IDS_DRIVER_BAD_ADDR));

		return FALSE;
		}

	return TRUE;
	}

void CABDialog::ShowAddress(UINT uFile, UINT uOffset)
{
	if( m_pSpace ) {

		GetDlgItem(2006).ShowWindow(FALSE);

		GetDlgItem(2001).ShowWindow(TRUE);

		GetDlgItem(2001).SetWindowText( m_pSpace->m_Prefix );

		if( m_pSpace->m_fTriplet ) {

			CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

			Combo.SetCurSel(uOffset % 3);

			Combo.ShowWindow(TRUE);
			}
		else
			GetDlgItem(2005).ShowWindow(FALSE);
		
		if( m_pSpace->m_FileDef == AB_FILE_NONE ) {

			GetDlgItem(2003).ShowWindow(FALSE);
			}
		else {
			GetDlgItem(2003).SetWindowText(CPrintf("%3.3d", uFile));

			GetDlgItem(2003).EnableWindow(TRUE);
		
			GetDlgItem(2003).ShowWindow(TRUE);
			}

		if( m_pSpace->m_fTriplet ) {

			uOffset /= 3;
			}
				
		GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(uOffset));

		GetDlgItem(2002).EnableWindow(TRUE);

		GetDlgItem(2002).ShowWindow(TRUE);

		GetDlgItem(2004).ShowWindow(TRUE);
		}
	}

void CABDialog::ClearAddress(void)
{
	GetDlgItem(2006).ShowWindow(TRUE);

	GetDlgItem(2001).ShowWindow(FALSE);

	GetDlgItem(2006).SetWindowText(CString(IDS_DRIVER_NONE));

	GetDlgItem(2003).SetWindowText("");

	GetDlgItem(2003).EnableWindow(FALSE);

	GetDlgItem(2002).SetWindowText("");

	GetDlgItem(2002).EnableWindow(FALSE);

	GetDlgItem(2002).ShowWindow(FALSE);

	GetDlgItem(2003).ShowWindow(FALSE);

	GetDlgItem(2004).ShowWindow(FALSE);

	GetDlgItem(2005).ShowWindow(FALSE);
	}

void CABDialog::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(m_pSpace->GetTypeText());

	CString Def;

	CString Min;

	CString Max;

	if( m_pSpace->m_FileDef == AB_FILE_NONE ) {

		Def.Printf( "%s:%s", 
			    m_pSpace->m_Prefix, 
			    m_pSpace->GetValueAsText(m_pSpace->m_uMinimum)
			    );

		Min.Printf( "%s:%s", 
			    m_pSpace->m_Prefix, 
			    m_pSpace->GetValueAsText(m_pSpace->m_uMinimum)
			    );

		Max.Printf( "%s:%s", 
			    m_pSpace->m_Prefix, 
			    m_pSpace->GetValueAsText(m_pSpace->m_uMaximum)
			    );
		}
	else {
		Def.Printf( "%s%3.3d:%s", 
			    m_pSpace->m_Prefix, 
			    m_pSpace->m_FileDef, 
			    m_pSpace->GetValueAsText(m_pSpace->m_uMinimum)
			    );

		Min.Printf( "%s%3.3d:%s", 
			    m_pSpace->m_Prefix, 
			    m_pSpace->m_FileMin, 
			    m_pSpace->GetValueAsText(m_pSpace->m_uMinimum)
			    );

		Max.Printf( "%s%3.3d:%s", 
			    m_pSpace->m_Prefix, 
			    m_pSpace->m_FileMax, 
			    m_pSpace->GetValueAsText(m_pSpace->m_uMaximum)
			    );
		}

	GetDlgItem(3004).SetWindowText(Def);

	GetDlgItem(3006).SetWindowText(Min);
	
	GetDlgItem(3008).SetWindowText(Max);
	
	GetDlgItem(3010).SetWindowText(m_pSpace->GetRadixAsText());
	}

void CABDialog::ClearDetails(void)
{
	GetDlgItem(3002).SetWindowText("");
	GetDlgItem(3004).SetWindowText("");
	GetDlgItem(3006).SetWindowText("");
	GetDlgItem(3008).SetWindowText("");
	GetDlgItem(3010).SetWindowText("");
	}

// Overridables

BOOL CABDialog::AllowSpace(CSpace *pSpace)
{
	if( m_uDevice == 0 && pSpace->m_uTable == 10) {

		return FALSE;
		}

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// DH485 Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CABDH485Dialog, CABDialog);
		
// Constructor

CABDH485Dialog::CABDH485Dialog(CSLCDH485Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CABDialog((CABDriver &)Driver, Addr, pConfig, fPart)
{

	}

BOOL CABDH485Dialog::AllowSpace(CSpace *pSpace)
{
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Space Wrapper Class
//

// Constructors

CSpaceAB::CSpaceAB(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT fd, UINT fn, UINT fx, BOOL fTriplet)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_FileDef	= fd;
	m_FileMin	= fn;
	m_FileMax	= fx;
	m_fTriplet	= fTriplet;

	FindAlignment();

	FindWidth();
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Tags Creator Page
//

class CAbTagsCreatorPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAbTagsCreatorPage();

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);
	};


//////////////////////////////////////////////////////////////////////////
//
// AB Tag Creator Page
//

// Runtime Class

AfxImplementRuntimeClass(CAbTagsCreatorPage, CUIStdPage);

// Constructor

CAbTagsCreatorPage::CAbTagsCreatorPage()
{
	}

// Operations

BOOL CAbTagsCreatorPage::LoadIntoView(IUICreate *pView, CItem *pItem) {

	pView->StartGroup(CString(IDS_INFORMATION), 1);

	CString Text;
	
	Text += CString(IDS_ONLY_THOSE_TAG);
	Text += CString(IDS_WILL_BE_USED_TO);

	pView->AddNarrative(Text);

	pView->EndGroup(FALSE);

	return FALSE;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Data Tag Create Helper
//

// Dynamic Class

AfxImplementDynamicClass(CAbTagsCreator, CUIItem);

// Constructor

CAbTagsCreator::CAbTagsCreator(void)
{	       
	CAbTagsCreator(L"_");
   	}

CAbTagsCreator::CAbTagsCreator(CString Name)
{
	m_Root	 = L"Tags" + Name;

	m_Define = 0;

	SetDefaults();

	UpdateFields(m_Name);

	AddMeta();
   	}

// UI Managament

void CAbTagsCreator::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || (Tag != "Root" && Tag != "Define") ) {

		UINT uType = pItem->GetDataAccess("Define")->ReadInteger(pItem);

		UINT uMask = m_File   << 0 |
			     m_Symbol << 1 |
			     m_Desc1  << 2 |
			     m_Desc2  << 3 |
			     m_Desc3  << 4 |
			     m_Desc4  << 5 |
			     m_Desc5  << 6;

		if( uMask == 0 ) {

			uMask = 1;
			}

		if( uType == TAG_LABEL ) 

			m_Label = uMask;
		else 
			m_Name  = uMask;

		pWnd->EnableUI("File", uMask > 1);

		UpdateFields(uMask);

		pWnd->UpdateUI();
		}

	if( Tag == "Define" ) {

		UINT uValue = pItem->GetDataAccess("Define")->ReadInteger(pItem) ? m_Label : m_Name;

		UpdateFields(uValue);

		pWnd->UpdateUI();
		}		
	}

// Persistance

void CAbTagsCreator::GetTagConfig(CItem * pItem)
{
	if( pItem ) {
		
		m_Name  = pItem->GetDataAccess("Name" )->ReadInteger(pItem);

		m_Label = pItem->GetDataAccess("Label")->ReadInteger(pItem);

		CString Root = pItem->GetDataAccess("Root" )->ReadString (pItem);

		if( !Root.IsEmpty() ) {

			m_Root = Root;
			}

		UpdateFields(m_Name);

		return;
		}

	SetDefaults();
	}

void CAbTagsCreator::SaveTagConfig(CItem * pItem)
{	 
	if( pItem ) {

		pItem->GetDataAccess("Name" )->WriteInteger(pItem, m_Name);

		pItem->GetDataAccess("Label")->WriteInteger(pItem, m_Label);

		pItem->GetDataAccess("Root" )->WriteString (pItem, m_Root);
		}
	}

// UI Management

CViewWnd * CAbTagsCreator::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;
		
		pList->Append(New CUIStdPage(AfxPointerClass(this)));
		
		pList->Append(New CAbTagsCreatorPage());

		CUIViewWnd *pView = New CUIItemViewWnd(pList);

		pView->SetBorder(6);
		
		return pView;
		}
	
	return NULL;
	}

// Access

UINT CAbTagsCreator::GetMask(UINT uMask)
{
	switch( uMask ) {

		case TAG_NAME:	return m_Name;
		case TAG_LABEL:	return m_Label;
		}

	return 0;
	}

void CAbTagsCreator::GetNext(UINT &uIndex, UINT uMask)
{
	for( UINT i = uIndex; i <= 7; i++ ) {

		uIndex++;

		if( CheckIndex(uIndex, uMask) ) {

			return;
			}
		}

	uIndex = 0;
	}

// Implementation

BOOL CAbTagsCreator::CheckIndex(UINT uIndex, UINT uMask)
{
	UINT uValue = (uMask == TAG_NAME) ? m_Name : m_Label;
	
	switch( uIndex ) {

		case 1:	return (uValue & 0x01);

		case 2: return (uValue & 0x02);

		case 3: return (uValue & 0x04);
	
		case 4: return (uValue & 0x08);

		case 5: return (uValue & 0x10);

		case 6: return (uValue & 0x20);

		case 7: return (uValue & 0x40);
		}
	
	return FALSE;
	}

void CAbTagsCreator::SetDefaults(void)
{
	m_Name   = 0x02;

	m_Label  = 0x7C;
	}

void CAbTagsCreator::UpdateFields(UINT uValue)
{
	m_File   = (uValue & 0x01) >> 0;

	m_Symbol = (uValue & 0x02) >> 1;

	m_Desc1  = (uValue & 0x04) >> 2;

	m_Desc2  = (uValue & 0x08) >> 3;

	m_Desc3	 = (uValue & 0x10) >> 4;

	m_Desc4  = (uValue & 0x20) >> 5;

	m_Desc5  = (uValue & 0x40) >> 6;
	}

// Meta Data Creation

void CAbTagsCreator::AddMetaData(void)	
{
	CUIItem::AddMetaData();		  

	Meta_AddString(Root);

	Meta_AddInteger(Define);
	
	Meta_AddInteger(File);
	
	Meta_AddInteger(Symbol);

	Meta_AddInteger(Desc1);

	Meta_AddInteger(Desc2);

	Meta_AddInteger(Desc3);

	Meta_AddInteger(Desc4);

	Meta_AddInteger(Desc5);	

	Meta_SetName(IDS_AB_TAG);
	} 

// End of File
