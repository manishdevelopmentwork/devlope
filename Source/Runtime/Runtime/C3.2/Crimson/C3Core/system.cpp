
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic System Item
//

// Instance Pointer

CSystemItem * CSystemItem::m_pThis = NULL;

// Constructor

CSystemItem::CSystemItem(void)
{
	m_pThis = this;
	}

// Destructor

CSystemItem::~CSystemItem(void)
{
	m_pThis = NULL;
	}

// System Calls

void CSystemItem::SystemStart(void)
{
	}

void CSystemItem::SystemStop(void)
{
	}

void CSystemItem::SystemSave(void)
{
	}
	
void CSystemItem::SystemInit(void)
{
	}

void CSystemItem::SystemTerm(void)
{
	}

void CSystemItem::GetTaskList(CTaskList &List)
{
	}

BOOL CSystemItem::IsPriority(void)
{
	return FALSE;
	}

UINT CSystemItem::GetCommsStatus(void)
{
	return 0;
	}

BOOL CSystemItem::GetDataServer(IDataServer * &pData)
{
	pData = NULL;
	
	return FALSE;
	}

BOOL CSystemItem::IsRunningControl(void)
{
	return FALSE;
	}

// End of File
