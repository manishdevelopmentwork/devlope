
#include "intern.hpp"

#include "virtser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linked Serial Port Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CLinkedSerialDriverOptions, CUIItem);

// Constructor

CLinkedSerialDriverOptions::CLinkedSerialDriverOptions(void)
{
	m_Port = 4000;
	}

// Download Support

BOOL CLinkedSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Port));

	return TRUE;
	}

// Meta Data Creation

void CLinkedSerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Port);
	}

//////////////////////////////////////////////////////////////////////////
//
// Linked Serial Port Driver
//

// Instantiator

ICommsDriver *	Create_LinkedSerialDriver(void)
{
	return New CLinkedSerialDriver;
	}

// Constructor

CLinkedSerialDriver::CLinkedSerialDriver(void)
{
	m_wID		= 0x3706;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Generic Program-Thru";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Program-Thru";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Configuration

CLASS CLinkedSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CLinkedSerialDriverOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dummy Serial Port Driver
//

// Instantiator

ICommsDriver *	Create_DummySerialDriver(void)
{
	return New CDummySerialDriver;
	}

// Constructor

CDummySerialDriver::CDummySerialDriver(void)
{
	m_wID		= 0x3705;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Virtual Serial Port";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Virtual Port";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// End of File
