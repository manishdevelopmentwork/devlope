
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix2DigitalOutput_HPP

#define INCLUDE_DAMix2DigitalOutput_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output 
//

class CDAMix2DigitalOutput : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix2DigitalOutput(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_Output1;
	UINT m_Output2;
	UINT m_Output3;
	UINT m_Output4;
	UINT m_Output5;
	UINT m_Output6;
	UINT m_Output7;
	UINT m_Output8;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
