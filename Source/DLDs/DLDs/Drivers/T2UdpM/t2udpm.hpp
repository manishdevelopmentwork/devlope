#include "toshbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Master UDP Driver
//

class CToshT2MasterUDPDriver : public CToshibaBaseMasterDriver
{
	public:
		// Constructor
		CToshT2MasterUDPDriver(void);

		// Destructor
		~CToshT2MasterUDPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CContextE : CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContextE * m_pCtxE;
		UINT	    m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);
		BOOL CheckFrame(void);

		BOOL Start(UINT &uCount, UINT uType);

		
	};

// End of File
