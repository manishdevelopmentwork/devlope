
#include "Intern.hpp"

#include "HttpAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Authentication Base Class
//

// Attributes

CString CHttpAuth::GetParam(CString Name) const
{
	INDEX n = m_Keys.FindName(Name);

	if( !m_Keys.Failed(n) ) {

		return m_List[m_Keys.GetData(n)];
		}

	return "";
	}

// Implementation

BOOL CHttpAuth::ParseLine(CString Line)
{
	m_List.Empty();

	m_Keys.Empty();

	Line.Tokenize(m_List, ',');

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CString Data = m_List[n];
		
		CString Name = Data.StripToken('=');

		Data.TrimBoth();

		Name.TrimBoth();

		if( Data[0] == '"' ) {

			Data = Data.Mid(1, Data.GetLength() - 2);
			}

		m_List.SetAt(n, Data);

		m_Keys.Insert(Name, n);
		}

	return TRUE;
	}

// End of File
