
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Item for Text
//

// Constructor

CLayItemText::CLayItemText(PCTXT pText)
{
	m_Text     = pText;

	m_Extra.cx = 4;

	m_Extra.cy = 4;

	m_Grow.cx  = 1;

	m_Grow.cy  = 1;
	}

CLayItemText::CLayItemText(PCTXT pText, int nExtra, int nGrow)
{
	m_Text     = pText;

	m_Extra.cx = nExtra;

	m_Extra.cy = nExtra;

	m_Grow.cx  = nGrow;

	m_Grow.cy  = 1;
	}

CLayItemText::CLayItemText(PCTXT pText, CSize Extra, CSize Grow)
{
	m_Text  = pText;

	m_Extra = Extra;

	m_Grow  = Grow;
	}

CLayItemText::CLayItemText(UINT uCount)
{
	m_Text     = CString('X', uCount);

	m_Extra.cx = 4;

	m_Extra.cy = 4;

	m_Grow.cx  = 1;

	m_Grow.cy  = 1;
	}

CLayItemText::CLayItemText(UINT uCount, int nExtra, int nGrow)
{
	m_Text     = CString('X', uCount);

	m_Extra.cx = nExtra;

	m_Extra.cy = nExtra;

	m_Grow.cx  = nGrow;

	m_Grow.cy  = 1;
	}

CLayItemText::CLayItemText(UINT uCount, CSize Extra, CSize Grow)
{
	m_Text  = CString('X', uCount);

	m_Extra = Extra;

	m_Grow  = Grow;
	}

// Overidables

void CLayItemText::OnPrepare(CDC &DC)
{
	DC.Select(afxFont(Dialog));

	CSize Size = DC.GetTextExtent(m_Text);

	m_MinSize  = Size + 2 * m_Extra;

	m_MaxSize  = m_MinSize * m_Grow;

	DC.Deselect();
	}

// End of File
