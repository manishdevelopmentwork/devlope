
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOOutputWnd_HPP

#define INCLUDE_DADIDOOutputWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDADIDOOutputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Config Window
//

class CDADIDOOutputWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDADIDOOutputConfig * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);

	// Implementation
	void AddOutputs(void);
};

// End of File

#endif
