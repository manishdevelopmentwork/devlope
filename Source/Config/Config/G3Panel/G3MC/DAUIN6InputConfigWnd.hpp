
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAUIN6InputConfigWnd_HPP

#define INCLUDE_DAUIN6InputConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAUIN6InputConfig;

class CDAUIN6Module;

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 AI Config Window
//

class CDAUIN6InputConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAUIN6InputConfigWnd(UINT uPage);

protected:
	// Data Members
	CDAUIN6Module      * m_pModule;
	CDAUIN6InputConfig * m_pItem;
	UINT		     m_uPage;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Implementation
	void AddOperation(void);
	void AddUnits(void);
	void AddRates(void);

	// Data Access
	UINT GetInteger(CMetaItem *pItem, CString Tag);
	void PutInteger(CMetaItem *pItem, CString Tag, UINT Data);

	// Enabling
	void DoEnables(UINT uIndex);
};

// End of File

#endif
