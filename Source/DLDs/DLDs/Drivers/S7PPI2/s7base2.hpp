
//////////////////////////////////////////////////////////////////////////
//
// PPI ASCII Codes
//

#define	SD1		0x10
#define	SD2		0x68
#define	SD4		0xDC
#define	SC		0xE5
#define	ED		0x16

//////////////////////////////////////////////////////////////////////////
//
// Table Identifiers
//

#define	TABLE_VX	1
#define	TABLE_IX	2
#define	TABLE_QX	3
#define	TABLE_MX	4
#define	TABLE_AI	5
#define	TABLE_AQ	6
#define TABLE_T		7
#define TABLE_C		8
#define	TABLE_I		12
#define	TABLE_Q		13
#define	TABLE_M		14

//////////////////////////////////////////////////////////////////////////
//
// Rx Results
//

#define	RX_NONE		0
#define	RX_ERROR	1
#define	RX_FRAME	2
#define	RX_SC		3
#define	RX_NAK		4

//////////////////////////////////////////////////////////////////////////
//
// S7 Type Codes
//

#define	TC_BOOL		0x01
#define	TC_BYTE		0x02
#define	TC_WORD		0x04
#define	TC_DWORD	0x06
#define	TC_COUNTER	0x1E
#define	TC_TIMER	0x1F
#define	TC_HSC		0x20

//////////////////////////////////////////////////////////////////////////
//
// S7 Area Codes
//

#define	AC_S		0x04
#define	AC_SM		0x05
#define	AC_AI		0x06
#define	AC_AQ		0x07
#define	AC_C		0x1E
#define	AC_T		0x1F
#define	AC_HC		0x20
#define	AC_I		0x81
#define	AC_Q		0x82
#define	AC_M		0x83
#define	AC_V		0x84

//////////////////////////////////////////////////////////////////////////
//
// Response Data
//

#define MSG_START	3
#define	ROC_STR		(MSG_START +  1)
#define PDU_REF 	(MSG_START +  4)
#define	NUM_VARS	(MSG_START + 13)
#define	MSG_RESULT	(MSG_START + 14)

//////////////////////////////////////////////////////////////////////////
//
// Timing Values
//

#define	START_TIMEOUT	400
#define	FRAME_TIMEOUT	800
#define	POLL_TIMEOUT	1000
#define	POLL_RETRY	10

//////////////////////////////////////////////////////////////////////////
//
// S7 PPI Driver Revision 2
//

class CS7BaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CS7BaseDriver(void);

		// Destructor
		~CS7BaseDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
	
		// Device Data
		struct CBaseCtx
		{
			BOOL m_fFast;
			BYTE m_bDrop;
			};
	protected:
		// Device Data
		CBaseCtx *m_pBase;
		
		// Comms Data
		BYTE m_bTxBuff[300];
		BYTE m_bRxBuff[300];

		// Data Members
		BYTE	m_bSrcAddr;
		BYTE	m_bLastFC;
		WORD	m_wLastPDU;
		UINT	m_uPtr;
		BYTE	m_bCheck;
		BYTE	m_bParamHead;
		BYTE	m_bParamTail;
		BYTE	m_bNak;
		BOOL	m_fDebug;

		// Read Helpers
		CCODE DoReadSingleBits(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoReadMultipleBits(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoReadStandard(AREF Addr, PDWORD pData, UINT uCount);

		// Write Helpers
		CCODE DoWriteSingleBit(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoWriteStandard(AREF Addr, PDWORD pData, UINT uCount);

		// Implementation
		BOOL  FakeRead(UINT uTable);
		void  InitFrame(void);
		void  AddByte(BYTE bData);
		void  AddWord(WORD wData);
		void  AddLong(LONG lData);
		BYTE  GetNextFC(void);
		BYTE  CalcCheck(void);
		void  AddPDUHeader(void);
		void  SetPPIHeader(void);
		void  DumpReply(void);
		BOOL  CheckReply(void);
		WORD  FindBlock(UINT uTable);
		DWORD FindAddr (UINT uType, UINT uTable, UINT uAddr);
		BOOL  Poll(BOOL fFast);
		void  SendPoll(void);
	
		// Overridables
		virtual void TermFrame(void);
		virtual void AddPPIHeader(void);
		virtual void SetPDUHeader(void);
		virtual	void AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT uCount, AREF Addr);
	  	virtual	BYTE FindType (UINT uType, UINT uTable);
		virtual	BYTE FindArea (UINT uTable);
		virtual	UINT FindBits (UINT uType, UINT uTable);
		virtual	void AddPadding(UINT uTable);
		virtual BOOL IsTimer(UINT uTable);
		virtual BOOL IsCounter(UINT uTable);
		virtual BOOL EatWrite(UINT uTable);
		virtual UINT GetTable(UINT uTable);
		virtual UINT GetMaxBits(void);
	      
		// Transport Layer
		virtual BOOL Connect(void);
		virtual BOOL Disconnect(void);
		virtual BOOL Transact(void);
		virtual UINT GetReply(void);
			
		};

// End of File
