
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostStack_HPP

#define	INCLUDE_UsbHostStack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDeviceList.hpp"

#include "UsbDriverLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Host Stack
//

class CUsbHostStack : public IUsbHostStack, public IUsbHostControllerEvents, public IUsbHubEvents, public IEventSink, public IDiagProvider
{
	public:
		// Constructor
		CUsbHostStack(UINT uPriorty);

		// Destructor
		~CUsbHostStack(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostStack
		BOOL			   METHOD Init(void);
		BOOL			   METHOD Start(void);
		BOOL			   METHOD Stop(void);
		BOOL			   METHOD Attach(IUsbSystem *pSink);
		IUsbHostControllerDriver * METHOD GetHostController(UINT iCtrl);
		IUsbHostInterfaceDriver  * METHOD GetHostInterface(UINT iCtrl, UINT iHost);
		IUsbDevice               * METHOD GetDevice(UsbPortPath const &Path);
		IUsbDevice               * METHOD GetDevice(UsbDevPath const &Path);
		IUsbDevice               * METHOD GetDevice(UsbTreePath const &Path);
		IUsbHostFuncDriver       * METHOD GetDriver(UsbDevPath const &Path);
		IUsbPipe		 * METHOD GetPipe(UsbDevPath const &Path);
		UINT			   METHOD GetPortSpeed(UsbPortPath const &Path);
		BOOL			   METHOD ResetPort(UsbPortPath const &Path);
		BOOL                       METHOD ResetPort(UsbTreePath const &Path);
		BOOL			   METHOD SetAddress(IUsbPipe *pPipe, BYTE bAddr);
		BOOL			   METHOD ClearDevFeature(IUsbPipe *pPipe, WORD wSelect);
		BOOL			   METHOD ClearIntFeature(IUsbPipe *pPipe, WORD wSelect, WORD wInt);
		BOOL			   METHOD ClearEndFeature(IUsbPipe *pPipe, WORD wSelect, WORD wEnd);
		BOOL			   METHOD SetDevFeature(IUsbPipe *pPipe, WORD wSelect);
		BOOL			   METHOD SetIntFeature(IUsbPipe *pPipe, WORD wSelect, WORD wInt);
		BOOL			   METHOD SetEndFeature(IUsbPipe *pPipe, WORD wSelect, WORD wEnd);
		BOOL			   METHOD GetConfig(IUsbPipe *pPipe, BYTE &bConfig);
		BOOL			   METHOD SetConfig(IUsbPipe *pPipe, BYTE  bConfig);
		BOOL			   METHOD GetInterface(IUsbPipe *pPipe, WORD wInt, BYTE &bAlt);
		BOOL			   METHOD SetInterface(IUsbPipe *pPipe, WORD wInt, BYTE  bAlt);
		BOOL			   METHOD GetDevStatus(IUsbPipe *pPipe, WORD &wStatus);
		BOOL			   METHOD GetIntStatus(IUsbPipe *pPipe, WORD wInt, WORD &wStatus);
		BOOL			   METHOD GetEndStatus(IUsbPipe *pPipe, WORD wEnd, WORD &wStatus);
		BOOL			   METHOD GetDevDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen);
		BOOL			   METHOD GetConfigDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen);
		BOOL			   METHOD GetHubDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen);
		BOOL			   METHOD GetStringDesc(IUsbPipe *pPipe, UINT iIdx, PTXT pStr, UINT uLen);
		UsbIor			 * METHOD AllocUrb(void);
		void			   METHOD FreeUrb(UsbIor *pUrb);
		void			   METHOD FreeUrbs(IUsbPipe *pPipe);
		BOOL			   METHOD Transfer(IUsbPipe *pPipe, UsbIor *pUrb);
		BOOL 		           METHOD GetDriverClassOverride(UINT uVendor, UINT uProduct);
		BOOL			   METHOD GetDriverClassFilter(UINT uVendor, UINT uProduct);
		IUsbHostFuncDriver       * METHOD CreateDriver(UINT uClass, UINT uSubClass, UINT uProtocol);
		IUsbHostFuncDriver       * METHOD CreateDriver(UINT uVendor, UINT uProduct, UINT uClass, UINT uSubClass, UINT uProtocol); 

		// IUsbEvents
		BOOL METHOD GetDriverInterface(IUsbDriver *&pDriver);
		void METHOD OnBind(IUsbDriver *pDriver);
		void METHOD OnInit(void);
		void METHOD OnStart(void);
		void METHOD OnStop(void);

		// IUsbHostControllerEvents
		void METHOD OnDeviceArrival(UsbPortPath &Path);
		void METHOD OnDeviceRemoval(UsbPortPath &Path);
		void METHOD OnDriverStarted(IUsbHostFuncDriver *pDriver);
		void METHOD OnDriverStopped(IUsbHostFuncDriver *pDriver);
		void METHOD OnTransferDone(UsbIor &Urb);

		// IUsbHubEvents
		void METHOD OnHubDeviceArrival(UsbPortPath &Path);
		void METHOD OnHubDeviceRemoval(UsbPortPath &Path);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// IDiagProvider
		UINT RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data
		ULONG                      m_uRefs;
		UINT                       m_uProv;
		CUsbDriverLib		   m_DriverLib;
		IUsbHostControllerDriver * m_pCtrl[2];
		CUsbDeviceList		   m_DevList;
		UsbIor                     m_Urbs[1024];
		UsbIor			 * m_pUrbFree[1024];
		UINT	 		   m_iFreeHead;
		UINT	 		   m_iFreeTail;
		IThread			 * m_pThread;
		ITimer                   * m_pTimer;
		IEvent                   * m_pRun;
		IEvent                   * m_pStop;
		IEvent			 * m_pPoll;
		IEvent		         * m_pDone;
		IUsbSystem		 * m_pSink[4];
		DWORD			   m_dwTimer;
		UINT			   m_uDiagTime;
		UINT			   m_uDiagCount;
		UINT			   m_uDiagData;
		UINT			   m_uDiagUrb;
		UINT			   m_uDiagUrbMax;
		
		// Task Entry
		void TaskEntry(void);

		// Controllers
		void InitControllerList(void);
		BOOL InitControllers(void);
		BOOL StartControllers(void);
		BOOL StopControllers(void);
		BOOL BindController(IUsbDriver *pDriver);
		void PollControllers(UINT uLapsed);
		void EnableEvents(BOOL fEnable);

		// Topology
		UsbTreePath FindTree(UsbPortPath const &Path);

		// Devices 
		void         KillDevices(void);
		CUsbDevice * MakeDevice(void);
		void         KillDevice(CUsbDevice *pDevice);
		BOOL         ResetDevice(CUsbDevice *pDevice);
		void         PollDevices(UINT uLapsed);
		void	     RemoveDevices(void);

		// Drivers
		void InitDriver(IUsbHostFuncDriver *pDriver);
		
		// Sink List
		void InitSink(void);
		void FreeSink(void);

		// IO Request Blocks
		void InitUrbs(void);
		void DumpUrbs(void) const;
		void DumpUrb(UINT i) const;

		// Diagnostics
		void ResetCounters(void);
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagDevices(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagReset(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagComms(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Friends
		static int TaskUsbHostStack(IThread *pThread, void *pParam, UINT uParam); 
	};

// End of File

#endif
