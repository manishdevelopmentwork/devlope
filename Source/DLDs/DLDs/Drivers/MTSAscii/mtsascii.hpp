
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMTSDDADriver;
class CMTSDDAHandler;

// Spaces
#define	CADD	1
#define	COUT1	2
#define	COUT2	3
#define	CTAVG	4
#define	CTIDT	5
#define	CCADD	8
#define	CSGEN	9
#define	CCOMM	10

// Device Addresses
#define	ADEF	192
#define	AMAX	253

// Configuration
#define	QTYCFG	8
#define	CFGLO	1
#define	CFGHI	(CFGLO + QTYCFG - 1)
#define	ARRONE	(QTYCFG + 1) // OUT and TMP (arrays are not 0 based)
#define	QTYIDT	(5 * QTYCFG) // up to 5 items per command
#define	ARRIDT	(QTYIDT + 1) // IDT's array (not 0 based)
#define	GENSTR	81 // max size for unrecognized command string

// Active Comms Indicators
#define	COMMSZ	5 // number of items that may receive comms
#define	COMMO1	0
#define	COMMO2	1
#define	COMMTA	2
#define	COMMTI	3
#define	COMMSG	4

// Buffer Positions
#define	POSADD	0
#define	POSCMD	1
#define	POSSTX	2
#define	POSDATA	3
#define	POSDAT2	4

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Application Master / Network Slave Driver
//

class CMTSDDADriver : public CMasterDriver
{
	public:
		// Constructor
		CMTSDDADriver(void);

		// Destructor
		~CMTSDDADriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			UINT m_Drop;
			};

		CContext *	m_pCtx;

		// Data Members
		CMTSDDAHandler * m_pHandler;
	};

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Application Master / Network Slave Handler
//

class CMTSDDAHandler : public IPortHandler
{
	public:
		// Constructor
		CMTSDDAHandler(IHelper *pHelper);
		
		// Destructor
		~CMTSDDAHandler(void);

		// Operation
		void	SetDefaultAddress(BYTE bAddress);
		void	AccessHandler(AREF Addr, PDWORD pData, BOOL fIsWrite);

		// Event Initialization
		void	CreateEvents(void);

		// IUnknown
		HRM	QueryInterface(REFIID riid, void **ppObject);
		ULM	AddRef(void);
		ULM	Release(void);

		// Event Handlers
		void	MCALL Bind(IPortObject *pPort);
		void	MCALL OnOpen(CSerialConfig const &Config);
		void	MCALL OnClose(void);
		void	MCALL OnTimer(void);
		BOOL	MCALL OnTxData(BYTE &bData);
		void	MCALL OnTxDone(void);
		void	MCALL OnRxData(BYTE bData);
		void	MCALL OnRxDone(void);

	protected:
		// Data
		ULONG m_uRefs;

		// Helper
		IHelper *m_pHelper;
		
		// Port Object
		IPortObject *m_pPort;
		
		// Rx State Machine
		UINT	m_uRxState;
		UINT	m_uRxPtr;
		BYTE	m_bRx[80];

		// Tx State Machine
		BOOL	m_fTxEnable;
		UINT	m_uTxScan;
		BOOL	m_fTxReady;
		BYTE	m_bTx[10];
		BOOL	m_fDisableTx;
		BYTE	m_bNewAddress;

		// Application Data
		IEvent *m_pRxAppFlag;
		char	m_Hex[16];
		char *	m_pHex;

		// Data Cache
		BYTE	m_Addr[ARRONE];
		DWORD	m_Out1[ARRONE];
		DWORD	m_Out2[ARRONE];
		DWORD	m_TAvg[ARRONE];
		DWORD	m_TIDT[ARRIDT];
		BYTE	m_SGEN[GENSTR];
		BYTE	m_Comm[COMMSZ];

		// Transmit Machine
		void	SetTx(BYTE bOldAddr, BYTE bNewAddr);
		void	Send(void);
		
		// Receive Machine
		void	RxByte(BYTE bData);	
		BYTE	FindAddress(BYTE bAddress);
		void	ProcessFrame(void);
		DWORD	GetData(UINT uPos);
		void	StoreTemperatures(BYTE bAdd, UINT uStart);

		// General Support
		BOOL	IsValidUnitAddress(BYTE bAddress);
		void	StoreData(UINT uField, PDWORD pItem);
		BOOL	FindDataPosition(UINT uField, UINT * pPos);
		void	FillGenericString(void);
	};

// End of File
