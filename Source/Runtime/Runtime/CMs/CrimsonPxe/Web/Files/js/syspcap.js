
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Network Capture Support
//

// Data

var m_capt_start;

var m_page_busy;

// Code

function pageMain() {

	m_page_busy = true;

	m_capt_start = 0;

	var src = getParam("source");

	if (src) {

		$("#pcap-source").val(src);
	}

	setDownload();

	fetchStatus();

	$("#pcap-source").change(changeSource);

	$("#pcap-tcp-enable").change(doEnables);

	$("#pcap-udp-enable").change(doEnables);

	$("#pcap-icmp-enable").change(doEnables);

	$("#pcap-arp-enable").change(doEnables);

	$("#pcap-start").on("click", onClick);
}

function setDownload() {

	var src = $("#pcap-source").val();

	var url = "/ajax/syspcap-read.pcap?src=" + src;

	$("#pcap-href").attr("href", url);
}

function changeSource() {

	setDownload();

	$("#pcap-info").css("display", "hide");

	$("#pcap-source").prop("disabled", 1);

	setUrlArg("source", $("#pcap-source").val());

	m_page_busy = true;

	fetchStatus();
}

function updateUi(text) {

	var list = text.split(",");

	if (parseInt(list[0]) > 0) {

		if (true) {

			var msg;
			var rem;
			var add;

			if (parseInt(list[2]) > 0) {

				m_capt_start = 0;

				msg = $("#text-stop").html();
				rem = "btn-success";
				add = "btn-danger";

				$("#pcap-port").prop("disabled", true);
			}
			else {

				m_capt_start = 1;

				msg = $("#text-start").html();
				rem = "btn-danger";
				add = "btn-success";

				$("#pcap-port").prop("disabled", false);
			}

			$("#pcap-start").html(msg).removeClass(rem).addClass(add);
		}

		if (true) {

			var tcp = 0;
			var udp = 0;
			var arp = 0;
			var icmp = 0;
			var web = 0;

			var filter = list[1].split(";");

			for (var n = 0; n < filter.length; n++) {

				if (filter[n] != "") {

					var p = filter[n].split('=');

					if (p[0] == "tcp") {

						tcp = (p[1] == "all" || p[1] == "") ? 0xFFFF : parseInt(p[1]);
					}

					if (p[0] == "udp") {

						udp = (p[1] == "all" || p[1] == "") ? 0xFFFF : parseInt(p[1]);
					}

					if (p[0] == "arp") {

						arp = 1;
					}

					if (p[0] == "icmp") {

						icmp = 1;
					}

					if (p[0] == "web") {

						web = 1;
					}
				}
			}

			$("#pcap-tcp-port").val((!tcp || tcp == 0xFFFF) ? "" : tcp.toString());

			$("#pcap-udp-port").val((!udp || udp == 0xFFFF) ? "" : udp.toString());

			$("#pcap-tcp-enable").val(tcp ? ((tcp == 0xFFFF) ? (web ? 1 : 3) : 2) : 0);

			$("#pcap-udp-enable").val(udp ? ((udp == 0xFFFF) ? 1 : 2) : 0);

			$("#pcap-icmp-enable").val(icmp);

			$("#pcap-arp-enable").val(arp);

			doEnables();
		}

		$("#pcap-size").html(list[3] + " " + $("#text-avail").html());

		$("#pcap-link").css("display", parseInt(list[3]) ? "" : "none");

		$("#pcap-form").css("display", "");

		$("#pcap-port-fail").css("display", "none");

		$("#pcap-port-okay").css("display", "");
	}
	else {

		$("#pcap-source").prop("disabled", 0);

		$("#pcap-form").css("display", "");

		$("#pcap-port-okay").css("display", "none");

		$("#pcap-port-fail").css("display", "");
	}

	m_page_busy = false;
}

function doEnables() {

	if (m_capt_start) {

		var src = $("#pcap-source").val();

		$("#pcap-tcp-enable").prop("disabled", 0);

		$("#pcap-udp-enable").prop("disabled", src == "99");

		$("#pcap-arp-enable").prop("disabled", src == "99");

		$("#pcap-icmp-enable").prop("disabled", src == "99");

		$("#pcap-tcp-port").prop("disabled", $("#pcap-tcp-enable").val() != "2");

		$("#pcap-udp-port").prop("disabled", $("#pcap-udp-enable").val() != "2");

		$("#pcap-tcp-label").css("color", $("#pcap-tcp-enable").val() != "2" ? "#888" : "#000");

		$("#pcap-udp-label").css("color", $("#pcap-udp-enable").val() != "2" ? "#888" : "#000");

		var none = ($("#pcap-tcp-enable").val() == "0" &&
			$("#pcap-udp-enable").val() == "0" &&
			$("#pcap-icmp-enable").val() == "0" &&
			$("#pcap-arp-enable").val() == "0"
		);

		$("#pcap-start").prop("disabled", none);
	}
	else {

		$("#pcap-entry select").prop("disabled", 1);

		$("#pcap-entry input").prop("disabled", 1);

		$("#pcap-start").prop("disabled", 0);
	}

	$("#pcap-source").prop("disabled", 0);
}

function fetchStatus() {

	var src = $("#pcap-source").val();

	var url = "/ajax/syspcap-status.ajax?src=" + src;

	$.get({
		url: makeAjax(url),
		success: statusLoaded,
		error: statusFailed,
		xhrFields: { withCredentials: true }
	});
}

function statusLoaded(data) {

	updateUi(data);
}

function statusFailed(error) {

	if (check401(error.status)) {

		return;
	}

	m_page_busy = false;
}

function onClick() {

	if (!m_page_busy) {

		var src = $("#pcap-source").val();

		var url = "/ajax/syspcap-control.ajax?src=" + src;

		if (m_capt_start) {

			var filter = "";

			switch (parseInt($("#pcap-tcp-enable").val())) {
				case 1:
					filter += "tcp=all;web;";
					break;
				case 2:
					filter += "tcp=" + $("#pcap-tcp-port").val() + ";";
					break;
				case 3:
					filter += "tcp=all;";
					break;
			}

			switch (parseInt($("#pcap-udp-enable").val())) {
				case 1:
					filter += "udp=all;";
					break;
				case 2:
					filter += "udp=" + $("#pcap-udp-port").val() + ";";
					break;
			}

			switch (parseInt($("#pcap-icmp-enable").val())) {
				case 1:
					filter += "icmp;";
					break;
			}

			switch (parseInt($("#pcap-arp-enable").val())) {
				case 1:
					filter += "arp;";
					break;
			}

			url += "&action=start&filter=" + filter;
		}
		else
			url += "&action=stop";


		$("#pcap-source").prop("disabled", 1);

		$.get({
			url: makeAjax(url),
			success: commandLoaded,
			error: commandFailed,
			xhrFields: { withCredentials: true }
		});

		m_page_busy = true;
	}
}

function commandLoaded(data) {

	updateUi(data);
}

function commandFailed(error) {

	if (check401(error.status)) {

		return;
	}

	$("#pcap-source").prop("disabled", 0);

	m_page_busy = false;
}

// End of File
