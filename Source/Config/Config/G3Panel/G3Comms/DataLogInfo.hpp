
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DataLogInfo_HPP

#define INCLUDE_DataLogInfo_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Log Info (Honeywell Specific)
//

class CDataLogInfo : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataLogInfo(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pEnable[8];
		CCodedText * m_pName  [8];
		CCodedItem * m_pReady [8];
		CCodedItem * m_pRetain[8];
		CCodedText * m_pField [8];

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
