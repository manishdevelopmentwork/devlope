
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//								
// User Interface Page List
//

// Runtime Class

AfxImplementRuntimeClass(CUIPageList, CObject);

// Constructors

CUIPageList::CUIPageList(void)
{
	}

// Destructor

CUIPageList::~CUIPageList(void)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		delete m_List[n];
		}
	}

// Attributes

UINT CUIPageList::GetCount(void) const
{
	return m_List.GetCount();
	}

CUIPage * CUIPageList::GetEntry(UINT uIndex) const
{
	return m_List[uIndex];
	}

// Operations

void CUIPageList::Append(CUIPage *pPage)
{
	m_List.Append(pPage);
	}

void CUIPageList::Replace(UINT uIndex, CUIPage *pPage)
{
	delete m_List[uIndex];

	m_List.SetAt(uIndex, pPage);
	}

// Indexing

CUIPage * CUIPageList::operator [] (UINT uIndex) const
{
	return m_List[uIndex];
	}

//////////////////////////////////////////////////////////////////////////
//								
// User Interface Page Base Class
//

// Runtime Class

AfxImplementRuntimeClass(CUIPage, CObject);

// Constructor

CUIPage::CUIPage(void)
{
	}

// Attributes

CString CUIPage::GetTitle(void) const
{
	return m_Title;
	}

// Operations

BOOL CUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	UIError(L"cannot load page");

	return FALSE;
	}

// Implementation

void CUIPage::UIError(PCTXT pText, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pText);

	CString Text;

	Text.VPrintf(pText, pArgs);

	va_end(pArgs);

	AfxTrace(L"ERROR: %s\n", Text);

	AfxAssert(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//								
// User Interface Standard Page
//

// Runtime Class

AfxImplementRuntimeClass(CUIStdPage, CUIPage);

// Constructor

CUIStdPage::CUIStdPage(void)
{
	m_Class  = NULL;

	m_uPage  = 0;
	}

CUIStdPage::CUIStdPage(CString Title, CLASS Class, UINT uPage)
{
	m_Title  = Title;

	m_Class  = Class;

	m_uPage  = uPage;
	}

CUIStdPage::CUIStdPage(CLASS Class, UINT uPage)
{
	m_Class  = Class;

	m_uPage  = uPage;
	}

CUIStdPage::CUIStdPage(CLASS Class)
{
	m_Class = Class;

	m_uPage = 0;
	}

// Creation

void CUIStdPage::Create(CLASS Class, UINT &uPage, CString Text)
{
	CStringArray Part;

	Text.Tokenize(Part, '|');

	m_Title = Part[0];

	m_Class = Part[1].IsEmpty() ? Class : AfxNamedClass(Part[1]);

	m_uPage = Part[2].IsEmpty() ? uPage : watoi(Part[2]);

	uPage   = m_uPage + 1;
	}

// Operations

void CUIStdPage::SetSubstitute(CString Obj1, CString Obj2)
{
	m_Obj1 = Obj1;

	m_Obj2 = Obj2;
	}

BOOL CUIStdPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CLASS Class = m_Class;

	for(;;) {

		if( Class == AfxRuntimeClass(CMetaItem) ) {

			break;
			}

		if( Class == AfxRuntimeClass(CUIItem) ) {

			break;
			}

		CString NameW = GetNameW(Class);

		CString NameA = GetNameA(Class);

		CEntity EntityW(NameW, Class->GetModule());

		CEntity EntityA(NameA, Class->GetModule());

		LCID Local = GetThreadLocale();

		LCID Basic = MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT);

		UINT uPass = (Local == Basic ? 1 : 2);

		BOOL fAnsi = FALSE;

		for( UINT n = 0; n < uPass; n++ ) {

			HANDLE hLayout = NULL;
			
			// cppcheck-suppress knownConditionTrueFalse

			if( hLayout == NULL ) {
				
				if( hLayout = afxModule->LoadResource(EntityW, RT_RCDATA) ) {

					fAnsi = FALSE;
					}
				}	

			if( hLayout == NULL ) {
				
				if( hLayout = afxModule->LoadResource(EntityA, RT_RCDATA) ) {

					fAnsi = TRUE;
					}
				}	

			if( hLayout ) {

				PCTXT pLayout = PCTXT(LockResource(hLayout));

				PTXT  pDelete = NULL;

				UINT  uLength = 0;

				BOOL  fDone   = FALSE;

				if( fAnsi ) {

					PCSTR pAnsi = PCSTR(pLayout);

					UINT  uSize = 1;

					while( pAnsi[uSize] || pAnsi[uSize-1] ) {

						uSize++;
						}

					pDelete = New TCHAR [ ++uSize ];

					for( UINT n = 0; n < uSize; n++ ) {

						pDelete[n] = BYTE(pAnsi[n]);
						}

					UnlockResource(hLayout);

					FreeResource  (hLayout);

					pLayout = pDelete;
					}

				while( uLength = wstrlen(pLayout) ) {

					if( pLayout[1] == ':' ) {

						CStringArray Part;

						CString(pLayout + 2).Tokenize(Part, ',');

						if( pLayout[0] == 'P' ) {

							UINT uCols = watoi(Part[0]);

							pView->ResetPage(uCols);
							}

						if( pLayout[0] == 'T' ) {

							UINT uCols = watoi(Part[0]);

							pView->StartTable(Part[1], uCols);

							for( UINT n = 2; n < Part.GetCount(); n++ ) {

								pView->AddColHead(Part[n]);
								}
							}

						if( pLayout[0] == 'R' ) {

							if( Part[0] == L"done" ) {

								pView->EndTable();
								}
							else {
								CString Object = Part[0];

								CString Header = Part[1];

								pView->AddRowHead(Header);

								if( Object == m_Obj1 ) {

									Object = m_Obj2;
									}

								for( UINT n = 2; n < Part.GetCount(); n++ ) {

									pView->AddUI(pItem, Object, Part[n]);
									}
								}
							}

						if( pLayout[0] == 'G' ) {

							UINT uCols = watoi(Part[0]);

							if( m_Group.IsEmpty() ) {

								CString Group = Part[2];

								C3OemStrings(Group);

								pView->StartGroup(Group, uCols);
								}
							else {
								C3OemStrings(m_Group);

								pView->StartGroup(m_Group, uCols);
								}

							CString Object = Part[1];

							if( Object == m_Obj1 ) {

								Object = m_Obj2;
								}

							for( UINT n = 3; n < Part.GetCount(); n++ ) {

								pView->AddUI(pItem, Object, Part[n]);
								}

							pView->EndGroup(TRUE);
							}

						if( pLayout[0] == 'B' ) {

							UINT uCount = watoi(Part[0]);

							pView->StartGroup(Part[1], 1);

							UINT uFrom  = 2;

							while( uCount-- ) {

								pView->AddButton(Part[uFrom+0], Part[uFrom+1], Part[uFrom+2]);

								uFrom += 3;
								}

							pView->EndGroup(TRUE);
							}

						if( pLayout[0] == 'O' ) {

							pView->StartOverlay();
							}

						if( pLayout[0] == 'E' ) {

							pView->EndOverlay();
							}

						fDone = TRUE;
						}

					pLayout += uLength + 1;
					}

				if( fAnsi ) {

					delete[] pDelete;
					}
				else {
					UnlockResource(hLayout);

					FreeResource  (hLayout);
					}

				SetThreadLocale(Local);

				return fDone;
				}

			SetThreadLocale(n ? Local : Basic);
			}

		Class = Class->GetBaseClass();
		}

	UIError(L"cannot load page schema %s", GetNameW(m_Class));

	return FALSE;
	}

// Implementation

CString CUIStdPage::GetNameW(CLASS Class)
{
	CString Base = Class->GetClassName();

	CString Name = Base + L"_Page";

	if( m_uPage ) {

		Name += CPrintf(L"%u", m_uPage);
		}

	return Name;
	}

CString CUIStdPage::GetNameA(CLASS Class)
{
	CString Base = Class->GetClassName();

	CString Name = Base + L"UIPage";

	if( m_uPage ) {

		Name += CPrintf(L"%u", m_uPage);
		}

	return Name;
	}

// End of File
