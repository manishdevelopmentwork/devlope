
#include "intern.hpp"

#include "fam3base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC Base Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CFam3BaseMasterDriver::CFam3BaseMasterDriver(void)
{
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 
	}

// Destructor

CFam3BaseMasterDriver::~CFam3BaseMasterDriver(void)
{
	}

// Entry Points

CCODE MCALL CFam3BaseMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_D;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CFam3BaseMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Start() ) {

		UINT uType = Addr.a.m_Type;

		AddPreamble(uType, FALSE);

		AddCpuNumber(uType, FALSE);

		GetCount(uCount, uType);

		AddCount(uType, uCount, FALSE);

		AddDeviceName(Addr.a.m_Table, Addr.a.m_Offset);

		AddPointCount(uType, uCount, FALSE);

		if( Transact(uType == addrBitAsBit ? CMD_BR : CMD_WR) ) {

			GetData(Addr.a.m_Type, pData, uCount);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CFam3BaseMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) {

		return uCount;
		}
	
	if( Start() ) {

		UINT uType = Addr.a.m_Type;

		AddPreamble(uType, TRUE);

		AddCpuNumber(uType, TRUE);

		GetCount(uCount, uType);

		AddCount(uType, uCount, TRUE);

		AddDeviceName(Addr.a.m_Table, Addr.a.m_Offset);

		AddPointCount(uType, uCount, TRUE);

		AddData(uType, pData, uCount);

		if( Transact(uType == addrBitAsBit ? CMD_BW : CMD_WW) ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Overridables

BOOL CFam3BaseMasterDriver::Start(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));
	
	m_uPtr = 0;

	m_bCheck = 0;

	return TRUE;
	}

void CFam3BaseMasterDriver::AddPreamble(UINT uType, BOOL fWrite)
{
       }

void CFam3BaseMasterDriver::AddCpuNumber(UINT uType, BOOL fWrite)
{
	if( m_pBase->m_uMode ) {

		AddAsciiPreamble(uType, fWrite);

		return;
		}

	AddByte(m_pBase->m_uCpu);
	}

void CFam3BaseMasterDriver::AddCount(UINT uType, UINT uCount, BOOL fWrite)
{
	if( m_pBase->m_uMode ) {

		return;
		}

	UINT Count = 8;

	if( fWrite ) {

		switch(uType) {

			case addrBitAsBit:
			case addrBitAsByte:
			case addrBitAsWord:

				Count += uCount;
				break;

			case addrWordAsWord:
								
				Count = Count + uCount * 2;
				break;

			case addrWordAsLong:
			case addrWordAsReal:

				Count = Count + uCount * 4;
				break;
			}
		}

	AddWord(Count);
	}    

void CFam3BaseMasterDriver::AddPointCount(UINT uType, UINT uCount, BOOL fWrite)
{
	if( uType == addrWordAsLong || uType == addrWordAsReal ) {

		uCount *= 2;
		}
	
	if( m_pBase->m_uMode ) { 

		AddDec( uCount, uType == addrBitAsBit ? 100 : 10 ); 

		if( !fWrite ) {

			AddTerm();
			}
		return;
		}
	
	AddWord(uCount);    
	}

void CFam3BaseMasterDriver::AddWaitTime(void)
{

	}

void CFam3BaseMasterDriver::AddTerm(void)
{

	}

void CFam3BaseMasterDriver::AddData(UINT uType, PDWORD pData, UINT uCount)
{
	if( m_pBase->m_uMode ) {

		AddByte(',');
		}
	
	switch(uType) {

		case addrBitAsBit:

			SetBits(pData, uCount);
			break;

		case addrBitAsByte:
			
			break;

		case addrBitAsWord:
		case addrWordAsWord:

			SetWords(pData, uCount);
		    	break;

		case addrWordAsLong:
		case addrWordAsReal:
	
			SetLongs(pData, uCount);			
			break;
		}

	if( m_pBase->m_uMode ) {

		AddTerm();
		}
	}

void CFam3BaseMasterDriver::GetData(UINT uType, PDWORD pData, UINT uCount)
{
	switch(uType) {

		case addrBitAsBit:

			GetBits(pData, uCount);
			break;

		case addrBitAsByte:
			
			break;

		case addrBitAsWord:
		case addrWordAsWord:

			GetWords(pData, uCount);
		    	break;

		case addrWordAsLong:
		case addrWordAsReal:
	
			GetLongs(pData, uCount);			
			break;
		}
	}

void CFam3BaseMasterDriver::GetCount(UINT &uCount, UINT uType)
{
	switch(uType) {

		case addrBitAsBit:

			MakeMin(uCount, 128);
			break;

		case addrBitAsByte:
			
			break;

		case addrBitAsWord:
		case addrWordAsWord:

			MakeMin(uCount, 64);
			break;

		case addrWordAsLong:
		case addrWordAsReal:
	
			MakeMin(uCount, 32);
			break;
		}
	}

BOOL CFam3BaseMasterDriver::Transact(UINT uCmd) 
{
	return FALSE;
	}

// Implementation

CCODE CFam3BaseMasterDriver::GetBits(PDWORD pData, UINT Count)
{
	if( m_pBase->m_uMode ) {

		return GetASCIIBits(pData, Count);
		}

	for( UINT n = 0; n < Count; n++ ) {

		pData[n] = (m_bRxBuff[n + 4]) ? TRUE : FALSE;
		}
       	
	return Count;
	}

CCODE CFam3BaseMasterDriver::GetASCIIBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = GetBitData(PCSTR(m_bRxBuff + 4 + u));
		
		}

	return uCount;
	}

BOOL CFam3BaseMasterDriver::GetBitData(PCTXT pText)
{
	char cData = *(pText);

	if( cData == '1' ) {

		return TRUE;
		}

	return FALSE;
      	}

CCODE CFam3BaseMasterDriver::GetWords(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_uMode ) {

		return GetASCIIWords(pData, uCount);
		}

	
	for( UINT u = 0; u < uCount; u++ ) { 
			
		WORD x = PU2(m_bRxBuff + 4)[u];

		pData[u] = LONG(SHORT(MotorToHost(x)));

		}
	
	return uCount;
	}

CCODE CFam3BaseMasterDriver::GetASCIIWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = GetWordData(PCSTR(m_bRxBuff + 4 + 4 * u), 4);

		}

	return uCount;
	}

WORD CFam3BaseMasterDriver::GetWordData(PCTXT pText, UINT uCount)
{
	WORD wData = 0;

	while( uCount-- )  {

		char cData = *(pText++);

		if( cData >= '0' && cData <= '9' ) {

			wData = 16 * wData + cData - '0';
			}

		else if ( cData >= 'A' && cData <= 'F' ) {

			wData = 16 * wData + cData - 'A' + 10;
			}

		else if ( cData >= 'a' && cData <= 'f' ) {

			wData = 16 * wData + cData - 'a' + 10;
			}

		else
			break;
		}

	return wData;
	}

CCODE CFam3BaseMasterDriver::GetLongs(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_uMode ) {

		return GetASCIILongs(pData, uCount);
		}
		

	for( UINT u = 0; u < uCount; u++ ) { 
			
		DWORD x = PU4(m_bRxBuff + 4)[u];

		DWORD y = LOWORD(x) << 16;

		y |= HIWORD(x);

		pData[u] = MotorToHost(y);

		}
	
	return uCount;
	}

CCODE CFam3BaseMasterDriver::GetASCIILongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		WORD Hi =  GetWordData(PCSTR(m_bRxBuff + 4 + (8 * u) + 4), 4);

		WORD Lo =  GetWordData(PCSTR(m_bRxBuff + 4 + (8 * u)), 4);

		pData[u] = MAKELONG(Lo, Hi);

		}

	return uCount;
	}

void CFam3BaseMasterDriver::SetWords(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_uMode ) {

		SetASCIIWords(pData, uCount);

		return;
		}

	for ( UINT u = 0; u < uCount; u++ ) {

		AddWord(WORD(pData[u]));
		}
	}

void CFam3BaseMasterDriver::SetASCIIWords(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddHex(pData[u], 0x1000);
		}
	}

void CFam3BaseMasterDriver::SetLongs(PDWORD pData, UINT uCount)
{
	if( m_pBase->m_uMode ) {

		SetASCIILongs(pData, uCount);

		return;
		}

	for ( UINT u = 0; u < uCount; u++ ) {

		DWORD x = LOWORD(pData[u]) << 16;

		x |= HIWORD(pData[u]);

		AddLong(x);
		}
	}

void CFam3BaseMasterDriver::SetASCIILongs(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = pData[u] << 16;

		x = x |  (pData[u] >> 16);

		AddHex(x, 0x10000000);
		}
	}

void CFam3BaseMasterDriver::SetBits(PDWORD pData, UINT Count)
{
	if( m_pBase->m_uMode ) {

		SetASCIIBits(pData, Count);

		return;
		}

	for( UINT n = 0; n < Count; n++ ) {

		AddByte(pData[n] ? TRUE : FALSE);
		}
	}

void CFam3BaseMasterDriver::SetASCIIBits(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddByte( pData[u] ? 0x31 : 0x30);
		}
	}

// Frame Building
	 
void CFam3BaseMasterDriver::AddByte(BYTE bValue)
{
	m_bTxBuff[m_uPtr++] = bValue; 
	
	m_bCheck += bValue; 
	}

void CFam3BaseMasterDriver::AddHex(UINT uValue, UINT uMask)
{
	if( m_pBase->m_uMode ) { 

		for( ; uMask; uMask /= 16 ) {

			AddByte(m_pHex[(uValue / uMask) % 16]);
			}

		return;
		}

	for( UINT u = 1; u <= uMask; u *= 16 ) {

		AddByte((uValue / u) % 16);
		}
	}

void CFam3BaseMasterDriver::AddDec(UINT uValue, UINT uMask)
{
	for( ; uMask; uMask /= 10 ) {

		AddByte(m_pHex[(uValue / uMask) % 10]);

		}
	}

void CFam3BaseMasterDriver::AddWord(WORD Word)
{
	if( m_pBase->m_uMode ) {

		AddHex(Word, 0x1000);

		return;
	       	}

	AddByte(HIBYTE(Word));

	AddByte(LOBYTE(Word));

	}

void CFam3BaseMasterDriver::AddLong(DWORD dwWord)
{
	if( m_pBase->m_uMode ) {

		AddHex(dwWord, 0x10000000);

		return;
	       	}

	AddWord(HIWORD(dwWord));

	AddWord(LOWORD(dwWord));
	}

void CFam3BaseMasterDriver::AddDeviceName(UINT uTable, UINT uOffset)
{	
	if( m_pBase->m_uMode ) {

		AddAsciiDeviceName(uTable, uOffset);

		return;
		}
       	
	AddWord(uTable);

	AddLong(uOffset);
	}

void CFam3BaseMasterDriver::AddAsciiDeviceName(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case SPACE_TS:
		case SPACE_TP:
		case SPACE_TI:
		case SPACE_T:	
				AddByte('T');	
				break;

		case SPACE_CS:
		case SPACE_CP:
		case SPACE_CI:
		case SPACE_C:	
				AddByte('C');
				break;
		}
       	
	switch( uTable ) {

		case SPACE_X:	AddByte('X');	break;
		case SPACE_Y:	AddByte('Y');	break;
		case SPACE_I:	AddByte('I');	break;
		case SPACE_E:	AddByte('E');	break;
		case SPACE_M:	AddByte('M');	break;
		case SPACE_L:	AddByte('L');	break;
		case SPACE_D:	AddByte('D');	break;
		case SPACE_B:	AddByte('B');	break;
		case SPACE_R:	AddByte('R');	break;
		case SPACE_V:	AddByte('V');	break;
		case SPACE_Z:	AddByte('Z');	break;
		case SPACE_W:	AddByte('W');	break;
		
		case SPACE_T:	
		case SPACE_C:	AddByte('U');	break;

		case SPACE_TS:
		case SPACE_CS:	AddByte('S');	break;	

		case SPACE_TP:
		case SPACE_CP:	AddByte('P');	break;

		case SPACE_TI:
		case SPACE_CI:	AddByte('I');	break;
		}

	AddDec(uOffset, 10000);

	AddByte(',');
	}

void CFam3BaseMasterDriver::AddAsciiPreamble(UINT uType, BOOL fWrite)
{
	AddByte('0');

	AddDec(m_pBase->m_uCpu, 1);

	AddWaitTime();

	switch( uType ) {

		case addrBitAsBit:

			AddByte('B');
			break;

		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:
			
			AddByte('W');
			break;
		}

	if( fWrite ) {

		AddByte('W');

		AddByte('R');

		return;
		}

	AddByte('R');

	AddByte('D');
	}

BOOL CFam3BaseMasterDriver::IsReadOnly(UINT uTable)
{
	switch( uTable ) {

		case SPACE_X:
		case SPACE_TS:
		case SPACE_CS:

			return TRUE;
		}

	return FALSE;
	}

// End of File
