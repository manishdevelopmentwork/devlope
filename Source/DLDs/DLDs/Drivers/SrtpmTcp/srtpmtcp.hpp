
//////////////////////////////////////////////////////////////////////////

//
// Constants
//

#define DATA_SMALL	44
#define DATA_OFFSET	56
#define DATA_MIN	56
#define	CODE_READ	0x04
#define CODE_WRITE	0x07
#define CODE_ESTAB	0x4F
#define CODE_ACCESS	0x21
#define OP_SING		0xC0
#define OP_TEXT		0x80

//////////////////////////////////////////////////////////////////////////
//
// GE TCP/IP Master via SRTP Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

class CGeSrtpTCPMaster : public CMasterDriver
{
	public:
		// Constructor
		CGeSrtpTCPMaster(void);

		// Destructor
		~CGeSrtpTCPMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			DWORD    m_IP2;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			BYTE	 m_bTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	   m_fInit;
			BOOL     m_fAux;
			BOOL     m_fDirty;
			
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;
		

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Frame Building
		void StartFrame(void);
		void AddByte(BYTE bData);
		void AddRepeatedByte(int uCount, BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddHeader(BYTE bBegin, UINT uBytes, UINT uOp, BYTE bLong);
		void AddRegSpec(UINT uTable, UINT uAddr, UINT uCount, UINT uType);
		void EndFrame(void);
		void SwapBytes(WORD &wData);
		
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(void);
		BOOL CheckFrame(void);
		BOOL EstablishSession(void);
		BOOL ChangeAccessLevel(void);

		// Initialization
		CCODE DoInit(void);
		
		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		BOOL IsWritable(UINT uTable);
		BOOL IsReadOnly(UINT uTable);
		BYTE GetTypeID(UINT uTable, UINT uType);
		BOOL IsSmall(UINT uCount);
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File
