#!/bin/sh

# c3-ipsec-event
#
# Process events from the IPSec infrastructure by starting
# or stopping the appropriate interface. This will call the
# custom scripts created by the net applicator.

if [ -z "$PLUTO_VERB" ]
then
	echo no verb
	exit 1
fi

if [[ $PLUTO_VERB == up-* ]]
then
	/opt/crimson/scripts/c3-ifmake $PLUTO_CONNECTION $PLUTO_ME
fi

if [[ $PLUTO_VERB == down-* ]]
then
	/opt/crimson/scripts/c3-ifbreak $PLUTO_CONNECTION
fi
