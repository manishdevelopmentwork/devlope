
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagFlag_HPP

#define INCLUDE_TagFlag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DataTag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagEventFlag;

class CTagTriggerFlag;

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Item
//

class DLLNOT CTagFlag : public CDataTag
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagFlag(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;
		UINT GetDataType(void) const;
		UINT GetTypeFlags(void) const;
		BOOL HasSetpoint(void) const;
		BOOL NeedSetpoint(void) const;

		// Operations
		void UpdateTypes(BOOL fComp);
		void MakeLite(void);

		// Reference Check
		BOOL RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag);

		// Circular Check
		void UpdateCircular(void);

		// Evaluation
		DWORD GetProp(WORD ID, UINT Type);

		// Searching
		void FindAlarms  (CStringArray &List);
		void FindTriggers(CStringArray &List);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Persistance
		void Load(CTreeFile &File);
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT		  m_FlagTreatAs;
		UINT		  m_TakeBit;
		UINT              m_Manipulate;
		UINT		  m_Atomic;
		UINT              m_HasSP;
		CCodedItem      * m_pSetpoint;
		CTagEventFlag   * m_pEvent1;
		CTagEventFlag   * m_pEvent2;
		CTagTriggerFlag * m_pTrigger1;
		CTagTriggerFlag * m_pTrigger2;
		CSecDesc        * m_pSecDesc;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Property Access
		DWORD FindAsText(void);
		DWORD FindLabel(void);
		DWORD FindSP(UINT Type);
		DWORD FindFore(void);
		DWORD FindBack(void);

		// Memory Sizing
		UINT GetAllocSize(void);
		UINT GetCommsSize(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void LimitTreatAs(IUIHost *pHost);
		void LimitTakeBit(IUIHost *pHost);
		void LimitAccess(IUIHost *pHost);
		void BuildList(IUIHost *pHost, CString Tag);
	};

// End of File

#endif
