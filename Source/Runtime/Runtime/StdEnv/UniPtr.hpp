
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UniPtr_HPP
	
#define	INCLUDE_UniPtr_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/_YKgE

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUnicode;

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

class DLLAPI CUniPtr
{
	public:
		// Constructors
		CUniPtr(CUniPtr const &That);
		CUniPtr(CUnicode const &That);
		CUniPtr(PCUTF pText);

		// Conversion
		operator PCUTF (void) const;

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetLength(void) const;
		UINT GetHashValue(void) const;

		// Read-Only Access
		WCHAR GetAt(UINT uIndex) const;

		// Array Conversion
		UINT GetCharCodes(CWordArray &Code);

		// Comparison Functions
		int CompareC(PCUTF pText) const;
		int CompareN(PCUTF pText) const;
		int CompareC(PCUTF pText, UINT uCount) const;
		int CompareN(PCUTF pText, UINT uCount) const;
		int GetScore(PCUTF pText) const;

		// Comparison Helper
		friend DLLAPI int AfxCompare(CUniPtr const &a, CUniPtr const &b);

		// Comparison Operators
		BOOL operator == (PCUTF pText) const;
		BOOL operator != (PCUTF pText) const;
		BOOL operator <  (PCUTF pText) const;
		BOOL operator >  (PCUTF pText) const;
		BOOL operator <= (PCUTF pText) const;
		BOOL operator >= (PCUTF pText) const;

		// Substring Extraction
		CUnicode Left(UINT uCount) const;
		CUnicode Right(UINT uCount) const;
		CUnicode Mid(UINT uPos, UINT uCount) const;
		CUnicode Mid(UINT uPos) const;

		// Case Conversion
		CUnicode ToUpper(void) const;
		CUnicode ToLower(void) const;

		// Searching
		UINT Find(WCHAR cChar) const;
		UINT Find(WCHAR cChar, WCHAR cQuote) const;
		UINT Find(WCHAR cChar, WCHAR cOpen, WCHAR cClose) const;
		UINT Find(WCHAR cChar, UINT uPos) const;
		UINT Find(WCHAR cChar, UINT uPos, WCHAR cQuote) const;
		UINT Find(WCHAR cChar, UINT uPos, WCHAR cOpen, WCHAR cClose) const;
		UINT FindRev(WCHAR cChar) const;
		UINT FindRev(WCHAR cChar, UINT uPos) const;
		UINT Find(PCUTF pText) const;
		UINT Find(PCUTF pText, UINT uPos) const;
		UINT FindNot(PCUTF pList) const;
		UINT FindNot(PCUTF pList, UINT uPos) const;
		UINT FindOne(PCUTF pList) const;
		UINT FindOne(PCUTF pList, UINT uPos) const;

		// Searching
		UINT Search(PCUTF pText, UINT uMethod) const;
		UINT Search(PCUTF pText, UINT uFrom, UINT uMethod) const;

		// Partials
		BOOL StartsWith(PCUTF pText) const;
		BOOL EndsWith(PCUTF pText) const;

		// Counting
		UINT Count(WCHAR cChar) const;

		// Diagnostics
		void AssertValid(void) const;

	protected:
		// Data Members
		PUTF m_pText;

		// Protected Constructors
		CUniPtr(void) { }
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

INLINE CUniPtr::CUniPtr(CUniPtr const &That)
{
	That.AssertValid();
	
	m_pText = That.m_pText;
	}
	
INLINE CUniPtr::CUniPtr(PCUTF pText)
{
	AfxAssertStringPtr(pText);
	
	m_pText = (PUTF) pText;
	}

// Attributes

INLINE BOOL CUniPtr::IsEmpty(void) const
{
	return !*m_pText;
	}
	
INLINE BOOL CUniPtr::operator ! (void) const
{
	return !*m_pText;
	}

INLINE UINT CUniPtr::GetLength(void) const
{
	return wstrlen(m_pText);
	}

// Conversions

INLINE CUniPtr::operator PCUTF (void) const
{
	return m_pText;
	}

// Comparison Functions

INLINE int CUniPtr::CompareC(PCUTF pText) const
{
	AfxAssertStringPtr(pText);
	
	return wstrcmp(m_pText, pText);
	}

INLINE int CUniPtr::CompareN(PCUTF pText) const
{
	AfxAssertStringPtr(pText);
	
	return wstricmp(m_pText, pText);
	}

INLINE int CUniPtr::CompareC(PCUTF pText, UINT uCount) const
{
	AfxAssertStringPtr(pText);
	
	return wstrncmp(m_pText, pText, uCount);
	}

INLINE int CUniPtr::CompareN(PCUTF pText, UINT uCount) const
{
	AfxAssertStringPtr(pText);
	
	return wstrnicmp(m_pText, pText, uCount);
	}      

// Comparison Operators

INLINE BOOL CUniPtr::operator == (PCUTF pText) const
{
	return CompareN(pText) == 0;
	}

INLINE BOOL CUniPtr::operator != (PCUTF pText) const
{
	return CompareN(pText) != 0;
	}

INLINE BOOL CUniPtr::operator < (PCUTF pText) const
{
	return CompareN(pText) < 0;
	}

INLINE BOOL CUniPtr::operator > (PCUTF pText) const
{
	return CompareN(pText) > 0;
	}

INLINE BOOL CUniPtr::operator <= (PCUTF pText) const
{
	return CompareN(pText) <= 0;
	}

INLINE BOOL CUniPtr::operator >= (PCUTF pText) const
{
	return CompareN(pText) >= 0;
	}

// End of File

#endif
