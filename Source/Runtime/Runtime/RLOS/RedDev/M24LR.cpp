
#include "Intern.hpp"

#include "M24LR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// M24LR NFC RFID Serial EEPROM 
//

// Instantiator

ISerialMemory * Create_M24LR(void)
{
	CM24LR *pDevice = New CM24LR();

	pDevice->Open();

	return pDevice;
	}

// Constructor

CM24LR::CM24LR(void)
{
	StdSetRef();

	m_bChip = 0xA6;

	AfxGetObject("i2c", 0, II2c, m_pI2c);
	}

// Destructor

CM24LR::~CM24LR(void)
{
	m_pI2c->Release();
	}

// IUnknown

HRESULT CM24LR::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISerialMemory);

	return E_NOINTERFACE;
	}

ULONG CM24LR::AddRef(void)
{
	StdAddRef();
	}

ULONG CM24LR::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CM24LR::Open(void)
{
	return TRUE;
	}

// ISerialMemory

UINT METHOD CM24LR::GetSize(void)
{
	return 8192;
	}

BOOL METHOD CM24LR::IsFast(void)
{
	return FALSE;
	}

BOOL METHOD CM24LR::GetData(UINT uAddr, PBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		BYTE bChip = (uAddr & Bit(31)) ? m_bChip | Bit(3) : m_bChip;

		WORD wAddr = ::HostToMotor(WORD(uAddr));
		
		if( m_pI2c->Recv(bChip, PBYTE(&wAddr), sizeof(WORD), pData, uCount) ) {

			m_pI2c->Free();

			return TRUE;
			}
		
		m_pI2c->Free();
		}

	return FALSE;
	}

BOOL METHOD CM24LR::PutData(UINT uAddr, PCBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		while( uCount ) {

			UINT uSize = Min(uCount, UINT(4));

			WORD wAddr = ::HostToMotor(WORD(uAddr));

			if( m_pI2c->Send(m_bChip, PBYTE(&wAddr), sizeof(WORD), pData, uSize) ) {

				uCount -= uSize;

				pData  += uSize;

				uAddr  += uSize;

				while( !m_pI2c->Send(m_bChip, NULL, 0, NULL, 0) ) {

					Sleep(5);
					}
				
				continue;
				}

			m_pI2c->Free();

			return FALSE;
			}

		m_pI2c->Free();

		return TRUE;
		}

	return FALSE;
	}

// End of File
