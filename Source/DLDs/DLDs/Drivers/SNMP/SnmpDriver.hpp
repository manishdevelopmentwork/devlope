
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "SnmpAgent.hpp"

#include "Asn1BerDecoder.hpp"

#include "Asn1BerEncoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SNMP Driver
//

class CSnmpDriver : public CSlaveDriver, public ISnmpSource
{
	public:
		// Constructor
		CSnmpDriver(void);

		// Destructor
		~CSnmpDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:
		// Configuration
		UINT    m_Port1;
		UINT    m_Port2;
		char    m_sComm[64];
		UINT	m_TabLimit;
		UINT	m_AclAddr1;
		UINT	m_AclMask1;
		UINT	m_AclAddr2;
		UINT	m_AclMask2;
		UINT	m_TrapFrom;
		UINT	m_TrapAddr1;
		UINT	m_TrapMode1;
		UINT	m_TrapAddr2;
		UINT	m_TrapMode2;
		UINT	m_ChgLimit;

		// Data Members
		ISocket    * m_pSock;
		CSnmpAgent * m_pAgent;
		UINT         m_uTicks;
		UINT	     m_genData;
		UINT	     m_genTrap;
		UINT	     m_genNotify;
		BYTE	     m_Trap2[1000];
		UINT	     m_uBase1;
		UINT	     m_uBase2;
		UINT         m_genTrapA;
		UINT	     m_genNotifyA;
		DWORD	     m_TrapA[1000];
		UINT	     m_uBaseA;
		WORD	     m_Change[1000];

		// Implementation
		bool CheckRequest(void);
		bool AllowIP(DWORD IP);
		void CheckTraps(void);
		void CheckTraps(PBYTE pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uNotify, UINT uTrap);
		void CheckTraps(PDWORD pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uChange, UINT uNotify, UINT uTrap);
		bool SendTrap(UINT &Mode, UINT &Addr, UINT uTable, UINT uPos, UINT uNotify, UINT uTrap);
		void FindMappedTraps(void);
		void FindMappedTraps(PBYTE pHist, UINT uTable, UINT uCount);
		void FindMappedTraps(PDWORD pHist, UINT uTable, UINT uCount);

		// Data Source
		bool IsSpace(COid const &Oid, UINT uTag, UINT uPos);
		bool GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos);

		// Helpers
		void SetAddr(CAddress &Addr, UINT uTable, UINT uPos);
		bool IsChanged(UINT uLast, UINT uCurrent, UINT uChg);
		bool IsFound(UINT uPos);
	};

// End of File
