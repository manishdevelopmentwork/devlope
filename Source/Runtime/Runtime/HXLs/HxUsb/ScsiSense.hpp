
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiSense_HPP

#define	INCLUDE_ScsiSense_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Sense Data Object
//

class CScsiSense : public ScsiSense
{
	public:
		// Constructor
		CScsiSense(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Operations
		void Init(void);
		void Clear(void);
		void Set(BYTE bSense);
		void Set(BYTE bSense, BYTE bAsc);
		void Set(BYTE bSense, BYTE bAsc, BYTE bAscq);
		void SetInfo(DWORD dwInfo);

		// Debug
		void Debug(void);
	};

// End of File

#endif
