
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3mc.hpp"

#include "../g3comms/proxy.hpp"

#include "../g3comms/queue.hpp"

#include "import/modelid.h"

#include "import/comms.h"

#include "import/props.h"

//////////////////////////////////////////////////////////////////////////
//
// Alarm Modes
//

#define	ALARM_NULL		0x00
#define	ALARM_ABS_LO		0x01
#define	ALARM_ABS_HI		0x02
#define	ALARM_DEV_LO		0x03
#define	ALARM_DEV_HI		0x04
#define	ALARM_BAND_IN		0x05
#define	ALARM_BAND_OUT		0x06

//////////////////////////////////////////////////////////////////////////
//
// Error States
//

#define ERROR_OKAY		0
#define	ERROR_BAD_CONFIG	2
#define	ERROR_BAD_COUNT		3
#define	ERROR_BAD_RACK		4
#define	ERROR_BAD_DRIVER	5

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern UINT AddrGetCount(BOOL fSample);

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern void PersistGet(BYTE bSlot, PBYTE pData, UINT uSize);
extern void PersistPut(BYTE bSlot, PBYTE pData, UINT uSize);
extern void RackSetError(UINT uError);

// End of File

#endif
