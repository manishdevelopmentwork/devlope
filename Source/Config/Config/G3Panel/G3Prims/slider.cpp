
#include "intern.hpp"

#include "slider.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Slider
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacySlider, CPrimRich);

// Constructor

CPrimLegacySlider::CPrimLegacySlider(void)
{
	m_Mode     = 1;

	m_Accel    = 1;

	m_Entry    = 1;

	m_pKnob    = New CPrimBrush;

	m_pEdge    = New CPrimPen;

	m_pBack    = New CPrimColor;

	m_ShowMask = propEntry | propLimits | propFormat;
	}

// UI Creation

BOOL CPrimLegacySlider::OnLoadPages(CUIPageList *pList)
{
	LoadFirstPage(pList);

	CPrimRichPage *pPage = New CPrimRichPage(this, CString(IDS_FIGURE), 2);

	pList->Append(pPage);

	LoadRichPages(pList);
	
	return TRUE;
	}

// UI Update

void CPrimLegacySlider::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Entry" ) {

		DoEnables(pHost);
		}

	CPrimRich::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimLegacySlider::SetInitState(void)
{
	CPrimRich::SetInitState();

	m_pKnob->Set(GetRGB(31,0,0));

	m_pBack->Set(GetRGB(12,12,12));

	m_pEdge->m_Corner = 1;
	}

// Download Support

BOOL CPrimLegacySlider::MakeInitData(CInitData &Init)
{
	CPrimRich::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mode));

	Init.AddByte(BYTE(m_Accel));

	Init.AddByte(BYTE(m_Entry));

	Init.AddItem(itemSimple, m_pKnob);
	
	Init.AddItem(itemSimple, m_pEdge);
	
	Init.AddItem(itemSimple, m_pBack);

	return TRUE;
	}

// Meta Data

void CPrimLegacySlider::AddMetaData(void)
{
	CPrimRich::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddInteger(Accel);
	Meta_AddObject (Edge);
	Meta_AddObject (Knob);
	Meta_AddObject (Back);

	Meta_SetName((IDS_RICH_SLIDER));
	}

// Field Requirements

UINT CPrimLegacySlider::GetNeedMask(void) const
{
	return propLimits | (m_Entry ? propFormat : 0);
	}

// Implementation

void CPrimLegacySlider::DrawBase(IGDI *pGDI, R2 &Rect)
{
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	pGDI->SetBrushFore(m_pBack->GetColor());

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimLegacySlider::DrawBtnFrame(IGDI *pGDI, R2 &Rect)
{
	P2 h[6];
	
	P2 s[6];

	FindPoints(h, s, Rect, 2);

	pGDI->SelectBrush(brushFore);

	pGDI->SetBrushFore(GetRGB(8,8,8));

	pGDI->FillPolygon(s, 6, 0);

	pGDI->SetBrushFore(GetRGB(24,24,24));

	pGDI->FillPolygon(h, 6, 0);
	}

void CPrimLegacySlider::FindPoints(P2 *t, P2 *b, R2 &r, int s)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, s - 1, s - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;

	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

C3REAL CPrimLegacySlider::FindPosition(void)
{
	if( m_pValue ) {

		if( m_pValue->GetType() == typeReal ) {

			C3REAL rVal = I2R(m_pValue->ExecVal());

			C3REAL rMin = I2R(GetMinValue(typeReal));

			C3REAL rMax = I2R(GetMaxValue(typeReal));

			MakeMin(rVal, rMax);

			MakeMax(rVal, rMin);

			if( rMax - rMin ) {

				return (rVal - rMin) / (rMax - rMin);
				}

			return 0.0;
			}
		else {
			C3INT nVal = m_pValue->ExecVal();

			C3INT nMin = GetMinValue(typeInteger);

			C3INT nMax = GetMaxValue(typeInteger);

			MakeMin(nVal, nMax);

			MakeMax(nVal, nMin);

			if( nMax - nMin ) {

				return C3REAL(nVal - nMin) / C3REAL(nMax - nMin);
				}

			return 0.0;
			}
		}

	return 0.25;
	}

void CPrimLegacySlider::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Accel", m_Entry);

	pHost->EnableUI("Mode",  m_Entry);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Slider
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyVertSlider, CPrimLegacySlider);

// Constructor

CPrimLegacyVertSlider::CPrimLegacyVertSlider(void)
{
	m_uType = 0x18;
	}

// Overridables

void CPrimLegacyVertSlider::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	DrawBase(pGDI, Rect);

	R2 Slot = Rect;

	DrawSlot(pGDI, Slot);

	int yMin = Slot.y2 - 2;
	
	int yMax = Slot.y1 + 2;
	
	int yPos = yMin + C3INT((yMax - yMin) * FindPosition());

	R2 Knob;

	Knob.x1 = Rect.x1 + 2;
	
	Knob.x2 = Rect.x2 - 2;
	
	Knob.y1 = yPos - 8;
	
	Knob.y2 = yPos + 8;

	DrawBtnFrame(pGDI, Knob);

	m_pKnob->FillRect(pGDI, Knob);
	}

void CPrimLegacyVertSlider::SetInitState(void)
{
	CPrimLegacySlider::SetInitState();

	SetInitSize(49, 161);
	}

// Download Support

BOOL CPrimLegacyVertSlider::MakeInitData(CInitData &Init)
{
	CPrimLegacySlider::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimLegacyVertSlider::AddMetaData(void)
{
	CPrimLegacySlider::AddMetaData();

	Meta_SetName((IDS_RICH_VERTICAL));
	}

// Implementation

void CPrimLegacyVertSlider::DrawSlot(IGDI *pGDI, R2 &Rect)
{
	int xm = (Rect.x1 + Rect.x2) / 2;

	if( m_Mode ) {

		int cy = (Rect.y2 - Rect.y1);

		int cx = (Rect.x2 - Rect.x1);

		MakeMin(cx, cy / 3);

		R2 Btn1 = Rect;
		R2 Btn2 = Rect;

		Btn1.x1 = Rect.x1 + 2;
		Btn1.x2 = Rect.x2 - 2;
		Btn1.y1 = Rect.y1 + 2;
		Btn1.y2 = Rect.y1 + cx - 4;

		Btn2.x1 = Rect.x1 + 2;
		Btn2.x2 = Rect.x2 - 2;
		Btn2.y1 = Rect.y2 - cx + 4;
		Btn2.y2 = Rect.y2 - 2;

		Rect.y1 = Btn1.y2 + 1;
		Rect.y2 = Btn2.y1 - 1;

		DrawBtn1(pGDI, Btn1);
		DrawBtn2(pGDI, Btn2);
		}

	Rect.x1 = xm - 2;
	Rect.x2 = xm + 2;
	
	Rect.y1 = Rect.y1 + 8;
	Rect.y2 = Rect.y2 - 8;

	pGDI->SetBrushFore(COLOR(0));

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimLegacyVertSlider::DrawBtn1(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x1 + 4;
	
	t[1].x = Rect.x2 - 5;
	
	t[2].x = (t[0].x + t[1].x) / 2;

	t[0].y = Rect.y2 - 5;
	
	t[1].y = Rect.y2 - 5;
	
	t[2].y = Rect.y1 + 4;

	pGDI->SetBrushFore(GetRGB(0,0,0));

	pGDI->FillPolygon(t, 3, 0);
	}

void CPrimLegacyVertSlider::DrawBtn2(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x1 + 4;
	
	t[1].x = Rect.x2 - 5;
	
	t[2].x = (t[0].x + t[1].x) / 2;

	t[0].y = Rect.y1 + 4;
	
	t[1].y = Rect.y1 + 4;
	
	t[2].y = Rect.y2 - 5;

	pGDI->SetBrushFore(GetRGB(0,0,0));

	pGDI->FillPolygon(t, 3, 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Slider
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyHorzSlider, CPrimLegacySlider);

// Constructor

CPrimLegacyHorzSlider::CPrimLegacyHorzSlider(void)
{
	m_uType = 0x19;
	}

// Overridables

void CPrimLegacyHorzSlider::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	DrawBase(pGDI, Rect);

	R2 Slot = Rect;

	DrawSlot(pGDI, Slot);

	int xMin = Slot.x1 + 2;
	
	int xMax = Slot.x2 - 2;
	
	int xPos = xMin + C3INT((xMax - xMin) * FindPosition());

	R2 Knob;

	Knob.x1 = xPos - 8;
	
	Knob.x2 = xPos + 8;

	Knob.y1 = Rect.y1 + 2;
	
	Knob.y2 = Rect.y2 - 2;

	DrawBtnFrame(pGDI, Knob);

	m_pKnob->FillRect(pGDI, Knob);
	}

void CPrimLegacyHorzSlider::SetInitState(void)
{
	CPrimLegacySlider::SetInitState();

	SetInitSize(161, 49);
	}

// Download Support

BOOL CPrimLegacyHorzSlider::MakeInitData(CInitData &Init)
{
	CPrimLegacySlider::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimLegacyHorzSlider::AddMetaData(void)
{
	CPrimLegacySlider::AddMetaData();

	Meta_SetName((IDS_RICH_HORIZONTAL));
	}

// Implementation

void CPrimLegacyHorzSlider::DrawSlot(IGDI *pGDI, R2 &Rect)
{
	int ym = (Rect.y1 + Rect.y2) / 2;

	if( m_Mode ) {

		int cx = (Rect.x2 - Rect.x1);

		int cy = (Rect.y2 - Rect.y1);

		MakeMin(cy, cx / 3);

		R2 Btn1 = Rect;
		R2 Btn2 = Rect;

		Btn1.x1 = Rect.x1 + 2;
		Btn1.x2 = Rect.x1 + cy - 4;
		Btn1.y1 = Rect.y1 + 2;
		Btn1.y2 = Rect.y2 - 2;

		Btn2.x1 = Rect.x2 - cy + 4;
		Btn2.x2 = Rect.x2 - 2;
		Btn2.y1 = Rect.y1 + 2;
		Btn2.y2 = Rect.y2 - 2;

		Rect.x1 = Btn1.x2 + 1;
		Rect.x2 = Btn2.x1 - 1;

		DrawBtn1(pGDI, Btn1);
		DrawBtn2(pGDI, Btn2);
		}
 	
	Rect.x1 = Rect.x1 + 8;
	Rect.x2 = Rect.x2 - 8;

	Rect.y1 = ym - 2;
	Rect.y2 = ym + 2;

	pGDI->SetBrushFore(COLOR(0));

	pGDI->FillRect(PassRect(Rect));
	}

void CPrimLegacyHorzSlider::DrawBtn1(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x2 - 5;
	
	t[1].x = Rect.x2 - 5;
	
	t[2].x = Rect.x1 + 4;

	t[0].y = Rect.y1 + 4;
	
	t[1].y = Rect.y2 - 5;
	
	t[2].y = (t[0].y + t[1].y) / 2;

	pGDI->SetBrushFore(GetRGB(0,0,0));

	pGDI->FillPolygon(t, 3, 0);
	}

void CPrimLegacyHorzSlider::DrawBtn2(IGDI *pGDI, R2 Rect)
{
	DrawBtnFrame(pGDI, Rect);

	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->FillRect(PassRect(Rect));

	P2 t[3];

	t[0].x = Rect.x1 + 4;
	
	t[1].x = Rect.x1 + 4;
	
	t[2].x = Rect.x2 - 5;

	t[0].y = Rect.y1 + 4;
	
	t[1].y = Rect.y2 - 5;
	
	t[2].y = (t[0].y + t[1].y) / 2;

	pGDI->SetBrushFore(GetRGB(0,0,0));

	pGDI->FillPolygon(t, 3, 0);
	}

// End of File
