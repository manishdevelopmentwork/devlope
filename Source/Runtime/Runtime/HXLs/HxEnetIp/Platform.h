
//////////////////////////////////////////////////////////////////////////
//
// EthernetIP Gateway Environment
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PLATFORM_H

#define	INCLUDE_PLATFORM_H

//////////////////////////////////////////////////////////////////////////
//
// Standard Type Definitions
//

typedef	char			INT8;
typedef unsigned char		UINT8;
typedef short			INT16;
typedef unsigned short		UINT16;
//typedef __int32			INT32;
//typedef unsigned CHAR		UINT8;
//typedef unsigned __int16	UINT16;
//typedef unsigned __int32	UINT32;

//////////////////////////////////////////////////////////////////////////
//
// Threading
//

#define PLATFORM_THREAD_RET		DWORD 

#define PLATFORM_THREAD_MOD		/**/

#ifndef THREAD_PRIORITY_TIME_CRITICAL

#define THREAD_PRIORITY_TIME_CRITICAL	0

#endif

typedef PLATFORM_THREAD_RET (*ThreadFuncType)(PVOID pParam);

//////////////////////////////////////////////////////////////////////////
//
// Byte Ordering
//

#define UINT32_SET(p, v)	*PDWORD(p) = HostToIntel(DWORD(v))	

#define UINT16_SET(p, v)	*PSHORT(p) = HostToIntel(WORD(v))

#define UINT32_GET(p)		IntelToHost(* PDWORD(p))

#define UINT16_GET(p)		IntelToHost(* PWORD(p))

//////////////////////////////////////////////////////////////////////////
//
// ENCAP Memory Model
//

#define ENCAP_CVT_HL(x)         (x) = HostToIntel(DWORD(x))
#define ENCAP_CVT_PL(x)         (x) = HostToIntel(DWORD(x))
#define ENCAP_CVT_HS(x)         (x) = HostToIntel(WORD(x))
#define ENCAP_CVT_PS(x)         (x) = HostToIntel(WORD(x))

#define ENCAP_TO_HL(x)          HostToIntel(DWORD(x))
#define ENCAP_TO_PL(x)          HostToIntel(DWORD(x))
#define ENCAP_TO_HS(x)          HostToIntel(WORD(x))
#define ENCAP_TO_PS(x)          HostToIntel(WORD(x))

#define ENCAP_VALUE_LONG(x)     HostToIntel(DWORD(x))
#define ENCAP_VALUE_SHORT(x)    HostToIntel(WORD(x))

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define IS_BYTE(x)		((HIBYTE(UINT16(x))==0) ? TRUE : FALSE)

#define PLATFORM_MIN(x,y)	min(x,y)

#define PLATFORM_MAX(x,y)	max(x,y)

#define _T(x)			PSTR(x)

#undef  _UNICODE

//////////////////////////////////////////////////////////////////////////
//
// Synchronsiation Objects
//

#define PLATFORM_EVENT_TYPE	IEvent *

#define PLATFORM_MUTEX_TYPE	IMutex *

//////////////////////////////////////////////////////////////////////////
//
// Complex Types
//

typedef struct
{
	BOOL	bDhcpState;
	UINT32	lSubnetMask;
	UINT32	lGatewayAddr;
	UINT32	lNameServer;
	UINT32	lNameServer2;
	UINT8	szDomainName[48];   

	} TCPIP_CONFIG_DATA;

//////////////////////////////////////////////////////////////////////////
//
// Tuning Constants
//

#define TARGET_RESOLUTION	0

#define TIMER_INTERVAL		1

#define MUTEX_TIMEOUT		FOREVER

#define PING_TIMEOUT		1000

//////////////////////////////////////////////////////////////////////////
//
// Platform Timer
//

extern IEvent * platformStartTimer(void);
extern void     platformStopTimer(void);
extern BOOL     platformCreateThread(ThreadFuncType pfnSeedRoutine, void *pParam, int iPriority);
extern void     platformInitLogFile(void);
extern void     platformCloseLogFile(void);
extern void     platformWriteLog(PTXT pBuf, BOOL bTimeStamp);
extern void     platformInit(void);
extern void     platformStop(void);
extern void     platformSocketLibInit(void);
extern void     platformSocketLibCleanup(void);
extern void     platformGetMacID(PBYTE pMacId);
extern void     platformGetTcpIpCfgData(TCPIP_CONFIG_DATA *psCfg);
extern BOOL     platformGetDhcpState(void);
extern UINT32   platformGetGatewayAddr(void);
extern BOOL     platformStartPing(INT32 nSession, PBOOL pbDone);
extern BOOL     platformContinuePing(INT32 nSession, PBOOL pbDone);
extern BOOL     platformPing(UINT32 lIPAddress);  
extern BOOL     platformFindEthernet(CIpAddr &IP, CIpAddr &Mask, CMacAddr &MAC);
extern INT32    platformCreateSocket(WORD wID);
extern void     platformCloseSocket(UINT32 &uSock, BOOL fAbort = FALSE);
extern UINT32	platformGetTickCount(void);
extern void	platformSleep(UINT32);
extern IMutex * platformInitMutex(PCSTR pName);
extern void	platformReleaseMutex(IMutex *pMutex);
extern void     platformDiscardMutex(IMutex *pMutex);
extern void	platformWaitMutex(IMutex *pMutex, UINT32 uTimeout);
extern void	platformSetEvent(IEvent *pEvent);
extern void     platformWaitEvent(IEvent *pEvent, UINT32 lWait);

// End of File

#endif
