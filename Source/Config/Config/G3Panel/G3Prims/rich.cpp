
#include "intern.hpp"

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rich Legacy Primitive Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimRichPage, CUIStdPage);

// Constructor

CPrimRichPage::CPrimRichPage(CPrimRich *pData, CString Title, UINT uPage)
{
	m_pData  = pData;

	m_Title  = Title;

	m_Class  = AfxPointerClass(pData);

	m_uPage  = uPage;
	}

// Operations

BOOL CPrimRichPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 1 ) {

		LoadPage(pView, 0);

		return TRUE;
		}

	if( m_uPage == 2 ) {

		LoadPage(pView, 0);

		return TRUE;
		}

	if( m_uPage == 3 ) {

		if( m_pData->ShowProp(propLimits) ) {

			if( !m_pData->UseTagLimits() ) {

				LoadPage(pView, 1);
				}
			else {
				LoadNone( pView,
					  CString(IDS_LIMITS),
					  CString(IDS_DATA_LIMITS_ARE)
					  );
				}
			}

		if( m_pData->ShowProp(propEntry) ) {

			LoadPage(pView, 0);
			}

		return TRUE;
		}

	if( m_uPage == 5 ) {

		if( m_pData->ShowProp(propLabel) ) {

			if( !m_pData->UseTagLabel() ) {

				LoadPage(pView, 0);
				}
			else {
				LoadNone( pView,
					  CString(IDS_DATA_LABEL),
					  CString(IDS_LABEL_IS)
					  );
				}
			}

		if( m_pData->ShowProp(propFormat) ) {

			if( !m_pData->UseTagFormat() ) {

				if( !m_pData->HasOwnFormat() ) {

					LoadPage(pView, 1);
					}

				LoadFormat(pView);
				}
			else {
				LoadNone( pView,
					  CString(IDS_DATA_FORMAT),
					  CString(IDS_FORMAT_IS)
					  );
				}
			}

		pView->NoRecycle();

		return TRUE;
		}

	if( m_uPage == 7 ) {

		if( m_pData->ShowProp(propColor) ) {

			LoadColor(pView);
			}

		pView->NoRecycle();

		return TRUE;
		}

	return TRUE;
	}

// Implementation

BOOL CPrimRichPage::LoadFormat(IUICreate *pView)
{
	CDispFormat *pFormat = m_pData->m_pFormat;

	if( pFormat ) {

		// TODO -- In the case of sliders, we only need
		// the radix. We don't need all the other crap.

		CUIPageList List;

		pFormat->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pFormat);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRichPage::LoadColor(IUICreate *pView)
{
	if( !m_pData->UseTagColor() ) {

		CDispColor *pColor = m_pData->m_pColor;

		if( pColor ) {

			LoadPage(pView, 0);

			CUIPageList List;

			pColor->OnLoadPages(&List);

			if( List.GetCount() ) {

				List[0]->LoadIntoView(pView, pColor);

				return TRUE;
				}
			}

		LoadPage(pView, 1);

		LoadPage(pView, 3);

		return FALSE;
		}

	LoadNone(pView, CString(IDS_TEXT_COLORS), CString(IDS_TEXT_COLORS_ARE));

	LoadPage(pView, 2);

	return FALSE;
	}

BOOL CPrimRichPage::LoadNone(IUICreate *pView, PCTXT pGroup, PCTXT pWhat)
{
	pView->StartGroup(pGroup, 1);

	pView->AddNarrative(CFormat(CString(IDS_FMT_DEFINED_BY), pWhat));

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL CPrimRichPage::LoadPage(IUICreate *pView, UINT uSub)
{
	CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pData), m_uPage + uSub);

	pPage->LoadIntoView(pView, m_pData);

	delete pPage;

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rich Legacy Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRich, CPrim);

// Constructor

CPrimRich::CPrimRich(void)
{
	m_pValue       = NULL;
	m_Entry        = 0;
	m_TagLimits    = 1;
	m_TagLabel     = 1;
	m_TagFormat    = 1;
	m_TagColor     = 0;
	m_pEnable      = NULL;
	m_pValidate    = NULL;
	m_pOnSetFocus  = NULL;
	m_pOnKillFocus = NULL;
	m_pOnComplete  = NULL;
	m_pLabel       = NULL;
	m_pLimitMin    = NULL;
	m_pLimitMax    = NULL;
	m_FormType     = 0;
	m_ColType      = 0;
	m_UseBack      = 1;
	m_pFormat      = NULL;
	m_pColor       = NULL;
	m_pTextColor   = New CPrimColor;
	m_pTextShadow  = New CPrimColor;
	m_Font         = fontHei16;
	m_Content      = 1;
	m_Flash        = 0;
	m_ShowMask     = NOTHING;
	m_Format       = NULL;
	}

// UI Creation

BOOL CPrimRich::OnLoadPages(CUIPageList *pList)
{
	LoadFirstPage(pList);

	LoadRichPages(pList);
	
	return TRUE;
	}

// UI Update

void CPrimRich::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		m_pLast = m_pValue;

		LimitTypes(pHost);

		DoEnables(pHost);
		}

	if( Tag == "Value" ) {

		BOOL fMake = FALSE;

		if( m_pValue ) {

			if( !m_pLast ) {

				fMake = TRUE;
				}

			if( m_TagLimits && !HasTagLimits() ) {

				m_TagLimits = 0;

				fMake = TRUE;
				}

			if( m_TagLabel && !HasTagLabel() ) {

				m_TagLabel = 0;

				fMake = TRUE;
				}

			if( m_TagFormat && !HasTagFormat() ) {

				m_TagFormat = 0;

				fMake = TRUE;
				}

			if( m_TagColor && !HasTagColor() ) {

				m_TagColor = 0;

				fMake = TRUE;
				}
			}
		else {
			m_TagLimits = 1;
			m_TagLabel  = 1;
			m_TagFormat = 1;
			m_TagColor  = 0;

			fMake = TRUE;
			}

		LimitTypes(pHost);

		KillUnused(pHost);

		UpdateLimitTypes(pHost);

		UpdateChildTypes();

		if( m_Entry && !CanWrite() ) {

			m_Entry = 0;

			pHost->UpdateUI("Entry");
			}

		if( fMake ) {

			pHost->RemakeUI();

			return;
			}

		DoEnables(pHost);

		m_pLast = m_pValue;
		}

	if( Tag == "TagLimits" ) {

		if( m_TagLimits ) {

			KillCoded(pHost, L"LimitMin", m_pLimitMin);

			KillCoded(pHost, L"LimitMax", m_pLimitMax);
			}
		else {
			CString Name = m_pValue->GetTagName();

			InitCoded(pHost, L"LimitMin", m_pLimitMin, Name + L".Min");

			InitCoded(pHost, L"LimitMax", m_pLimitMax, Name + L".Max");
			}

		pHost->RemakeUI();
		}

	if( Tag == "TagLabel" ) {

		if( m_TagLabel ) {

			KillCoded(pHost, L"Label", m_pLabel);
			}
		else
			CopyTagLabel(pHost);

		pHost->RemakeUI();
		}

	if( Tag == "TagFormat" ) {

		KillFormat();

		if( !m_TagFormat ) {

			if( !CopyTagFormat() ) {

				if( m_Format ) {
		
					SetFormatClass(m_pFormat, m_Format);

					m_FormType = m_pFormat->GetFormType();
					}
				}
			}

		pHost->RemakeUI();
		}

	if( Tag == "TagColor" ) {

		KillColor();

		if( !m_TagColor ) {

			CopyTagColor();
			}

		pHost->RemakeUI();
		}

	if( Tag == "Entry" ) {

		UpdateType(pHost, L"Value", m_pValue, TRUE);

		DoEnables(pHost);
		}

	if( Tag == "FormType" ) {

		CLASS Class = CDispFormat::GetClass(m_FormType);

		if( SetFormatClass(m_pFormat, Class) ) {

			pHost->RemakeUI();
			}
		}

	if( Tag == "ColType" ) {

		CLASS Class = CDispColor::GetClass(m_ColType);

		if( SetColorClass(m_pColor, Class) ) {

			pHost->RemakeUI();
			}
		}

	if( Tag == "Content" ) {

		DoEnables(pHost);
		}

	CPrim::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimRich::ShowProp(UINT Prop) const
{
	UINT Mask = m_ShowMask;

	return (Mask & Prop) ? TRUE : FALSE;
	}

BOOL CPrimRich::NeedProp(UINT Prop) const
{
	UINT Mask = GetNeedMask();

	return (Mask & Prop) ? TRUE : FALSE;
	}

BOOL CPrimRich::CanWrite(void) const
{
	if( m_pValue ) {

		if( m_pValue->GetFlags() & flagWritable ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::IsTagRef(void) const
{
	if( m_pValue ) {

		if( m_pValue->IsTagRef() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::IsUnboundTag(void) const
{
	if( m_pValue ) {

		if( m_pValue->IsTagRef() ) {

			CTag *pTag = NULL;

			if( m_pValue->GetTagItem(pTag) ) {

				return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagItem(CTag * &pTag) const
{
	if( IsTagRef() ) {

		if( m_pValue->GetTagItem(pTag) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagLabel(CCodedText * &pLabel) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pLabel ) {

			pLabel = pTag->m_pLabel;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagFormat(CDispFormat * &pFormat) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pFormat ) {

			pFormat = pTag->m_pFormat;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetTagColor(CDispColor * &pColor) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pColor ) {

			pColor = pTag->m_pColor;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetDataLabel(CCodedText * &pLabel) const
{
	if( pLabel = m_pLabel ) {

		return TRUE;
		}

	if( m_TagLabel ) {
		
		if( GetTagLabel(pLabel) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetDataFormat(CDispFormat * &pFormat) const
{
	if( pFormat = m_pFormat ) {

		return TRUE;
		}

	if( m_TagFormat ) {
		
		if( GetTagFormat(pFormat) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::GetDataColor(CDispColor * &pColor) const
{
	if( pColor = m_pColor ) {

		return TRUE;
		}

	if( m_TagColor ) {
			
		if( GetTagColor(pColor) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::HasTagLimits(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		if( pTag->IsKindOf(AfxRuntimeClass(CTagString)) ) {

			return TRUE;
			}

		if( pTag->IsKindOf(AfxRuntimeClass(CTagNumeric)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::HasTagLabel(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRich::HasTagFormat(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( m_Format ) {

		CDispFormat *pFormat = NULL;

		if( GetTagFormat(pFormat) ) {
			
			return pFormat->IsKindOf(m_Format);
			}

		return FALSE;
		}

	if( GetTagItem(pTag) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRich::HasTagColor(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRich::UseTagLimits(void) const
{
	return m_pValue && m_TagLimits;
	}

BOOL CPrimRich::UseTagLabel(void) const
{
	return m_pValue && m_TagLabel;
	}

BOOL CPrimRich::UseTagFormat(void) const
{
	return m_pValue && m_TagFormat;
	}

BOOL CPrimRich::UseTagColor(void) const
{
	return m_pValue && m_TagColor;
	}

BOOL CPrimRich::HasOwnFormat(void) const
{
	return m_Format != NULL;
	}

// Limit Access

DWORD CPrimRich::GetMinValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			return pTag->GetMinValue(Type);
			}
		}

	if( m_pLimitMin ) {

		return m_pLimitMin->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return 0;
	}

DWORD CPrimRich::GetMaxValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			return pTag->GetMaxValue(Type);
			}
		}

	if( m_pLimitMax ) {

		return m_pLimitMax->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(100);
		}

	return 100;
	}

// Overridables

void CPrimRich::SetInitState(void)
{
	CPrim::SetInitState();

	if( m_Format ) {
		
		SetFormatClass(m_pFormat, m_Format);

		m_FormType = m_pFormat->GetFormType();
		}

	m_pTextColor->Set(GetRGB(31,31,31));
	}

void CPrimRich::Validate(BOOL fExpand)
{
	if( m_pValue ) {

		m_pValue->Recompile(fExpand);

		UpdateLimitTypes(NULL);

		UpdateChildTypes();
		}
	}

void CPrimRich::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	if( m_pValue ) {

		if( m_pValue->IsTagRef() ) {

			UINT uTag = m_pValue->GetTagIndex();

			if( !Tags.Failed(Tags.Find(uTag)) ) {

				Validate(TRUE);

				Done.Insert(m_pValue);
				}
			}
		}
	}

// Type Access

BOOL CPrimRich::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		if( m_Entry ) {

			Type.m_Type  = typeNumeric;

			Type.m_Flags = flagInherent | flagWritable;

			return TRUE;
			}

		Type.m_Type  = typeNumeric;

		Type.m_Flags = flagInherent | flagSoftWrite;
	
		return TRUE;
		}

	if( Tag == "SubValue" || Tag == "LimitMin" || Tag == "LimitMax" ) {

		Type.m_Type  = m_pValue ? m_pValue->GetType() : typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Label" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.Left(2) == "On" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CPrimRich::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);

	Init.AddByte(BYTE(m_Entry));

	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		Init.AddWord(WORD(pTag->GetIndex() + 1));

		Init.AddByte(BYTE(m_TagLimits));
		Init.AddByte(BYTE(m_TagLabel));
		Init.AddByte(BYTE(m_TagFormat));
		Init.AddByte(BYTE(m_TagColor));
		}
	else
		Init.AddWord(WORD(0));

	if( m_Entry ) {

		Init.AddItem(itemVirtual, m_pEnable);
		Init.AddItem(itemVirtual, m_pOnSetFocus);
		Init.AddItem(itemVirtual, m_pOnKillFocus);
		Init.AddItem(itemVirtual, m_pOnComplete);
		}

	Init.AddItem(itemVirtual, m_pLabel);
	Init.AddItem(itemVirtual, m_pLimitMin);
	Init.AddItem(itemVirtual, m_pLimitMax);

	if( m_Format ) {

		CDispFormat *pFormat = NULL;

		if( GetDataFormat(pFormat) ) {

			if( pFormat->IsKindOf(m_Format) ) {

				Init.AddItem(itemVirtual, pFormat);
				}
			else 
				Init.AddByte(BYTE(0));
			}
		else 
			Init.AddByte(BYTE(0));
		}
	else 
		Init.AddItem(itemVirtual, m_pFormat);

	Init.AddItem(itemVirtual, m_pColor);

	if( !m_pColor ) {
	
		Init.AddItem(itemSimple,  m_pTextColor);
		Init.AddItem(itemSimple,  m_pTextShadow);
		}

	Init.AddByte(BYTE(m_UseBack));

	Init.AddWord(WORD(m_Font));

	Init.AddByte(BYTE(m_Content));

	Init.AddByte(BYTE(m_Flash));

	return TRUE;
	}

// Meta Data

void CPrimRich::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddInteger(Entry);
	Meta_AddInteger(TagLimits);
	Meta_AddInteger(TagLabel);
	Meta_AddInteger(TagFormat);
	Meta_AddInteger(TagColor);
	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Validate);
	Meta_AddVirtual(OnSetFocus);
	Meta_AddVirtual(OnKillFocus);
	Meta_AddVirtual(OnComplete);
	Meta_AddVirtual(Label);
	Meta_AddVirtual(LimitMin);
	Meta_AddVirtual(LimitMax);
	Meta_AddInteger(FormType);
	Meta_AddInteger(ColType);
	Meta_AddInteger(UseBack);
	Meta_AddVirtual(Format);
	Meta_AddVirtual(Color);
	Meta_AddObject (TextColor);
	Meta_AddObject (TextShadow);
	Meta_AddInteger(Font);
	Meta_AddInteger(Flash);
	Meta_AddInteger(Content);
	Meta_AddRect   (Margin);

	Meta_SetName((IDS_RICH_PRIMITIVE));
	}

// Type Updates

void CPrimRich::UpdateLimitTypes(IUIHost *pHost)
{
	UpdateType(pHost, L"LimitMin", m_pLimitMin, TRUE);

	UpdateType(pHost, L"LimitMax", m_pLimitMax, TRUE);
	}

void CPrimRich::UpdateChildTypes(void)
{
	if( m_pFormat ) {

		m_pFormat->UpdateTypes(TRUE);
		}

	if( m_pColor ) {

		m_pColor->UpdateTypes(TRUE);
		}
	}

// Child Updates

BOOL CPrimRich::SetFormatClass(CDispFormat * &pFormat, CLASS Class)
{
	CDispFormat * pOld = pFormat;

	if( AfxPointerClass(pOld) == Class ) {

		return FALSE;
		}

	if( Class ) {

		pFormat = AfxNewObject(CDispFormat, Class);

		pFormat->SetParent(this);

		pFormat->Init();

		if( pOld ) {

			pFormat->Preserve(pOld);
			}
		}
	else
		pFormat = NULL;

	if( pOld ) {

		pOld->Kill();

		delete pOld;
		}

	return TRUE;
	}

BOOL CPrimRich::SetColorClass(CDispColor * &pColor, CLASS Class)
{
	CDispColor * pOld = pColor;

	if( AfxPointerClass(pOld) == Class ) {

		return FALSE;
		}

	if( Class ) {

		pColor = AfxNewObject(CDispColor, Class);

		pColor->SetParent(this);

		pColor->Init();

		if( pOld ) {

			pColor->Preserve(pOld);
			}
		}
	else
		pColor = NULL;

	if( pOld ) {

		pOld->Kill();

		delete pOld;
		}

	return TRUE;
	}

// Copying From Tag

BOOL CPrimRich::CopyTagLabel(IUIHost *pHost)
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pLabel ) {

			CString Text = pTag->m_pLabel->GetSource(TRUE);

			InitCoded(pHost, L"Label", m_pLabel, Text);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimRich::CopyTagFormat(void)
{
	CDispFormat *pFormat = NULL;

	if( GetTagFormat(pFormat) ) {

		m_pFormat  = (CDispFormat *) CItem::MakeFromItem(this, pFormat);

		m_FormType = m_pFormat ? m_pFormat->GetFormType() : 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRich::CopyTagColor(void)
{
	CDispColor *pColor = NULL;

	if( GetTagColor(pColor) ) {

		m_pColor  = (CDispColor *) CItem::MakeFromItem(this, pColor);

		m_ColType = m_pColor ? m_pColor->GetColType() : 0;

		return TRUE;
		}

	return FALSE;
	}

// Color Extraction

void CPrimRich::FindColors(IGDI *pGDI, COLOR &Color, COLOR &Shadow, DWORD Data, UINT Type, BOOL fEntry)
{
	CDispColor *pColor;

	if( GetDataColor(pColor) ) {

		DWORD Pair = pColor->GetColorPair(Data, Type);

		if( fEntry ) {

			pGDI->SetTextTrans(modeOpaque);

			pGDI->SetTextBack (LOWORD(Pair));

			pGDI->SetBrushFore(LOWORD(Pair));

			pGDI->SetTextFore (HIWORD(Pair));
			}
		else {
			if( m_UseBack ) {

				pGDI->SetTextTrans(modeOpaque);

				pGDI->SetTextFore (LOWORD(Pair));

				pGDI->SetTextBack (HIWORD(Pair));

				pGDI->SetBrushFore(HIWORD(Pair));
				}
			else {
				pGDI->SetTextTrans(modeTransparent);

				pGDI->SetTextFore (LOWORD(Pair));

				pGDI->SelectBrush (brushNull);
				}
			}
		}
	else {
		if( fEntry ) {

			pGDI->SetTextTrans(modeOpaque);

			COLOR Color = m_pTextColor->GetColor();

			pGDI->SetTextBack (Color);

			pGDI->SetBrushFore(Color);

			pGDI->SetTextFore (FindCompColor(Color));
			}
		else {
			pGDI->SetTextTrans(modeTransparent);

			Color  = m_pTextColor->GetColor();

			Shadow = m_pTextShadow->GetColor();

			pGDI->SetTextFore(Color);

			pGDI->SelectBrush(brushNull);
			}
		}
	}

// Text Extraction

CString CPrimRich::FindLabelText(void)
{
	CCodedText *pLabel;

	if( GetDataLabel(pLabel) ) {

		DWORD n = 0;

		if( m_pValue ) {

			CDataRef &Ref = (CDataRef &) n;

			n = m_pValue->Execute(typeLValue);

			// cppcheck-suppress redundantAssignment

			n = Ref.x.m_Array;
			}

		return pLabel->GetText(&n);
		}
	else {
		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			CString Name = pTag->m_Name.TokenLast('.');

			return Name;
			}
		}

	return L"Label";
	}

CString CPrimRich::FindValueText(DWORD Data, UINT Type, UINT Flags)
{
	CDispFormat *pFormat;

	if( GetDataFormat(pFormat) ) {

		return pFormat->Format(Data, Type, Flags);
		}

	return CDispFormat::GeneralFormat(Data, Type, Flags);
	}

// Operations

BOOL CPrimRich::SelectFont(IGDI *pGDI, UINT Font)
{
	if( m_Font < 0x100 ) {

		pGDI->SelectFont(Font);

		return TRUE;
		}

	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	return pFonts->Select(pGDI, Font);
	}

void CPrimRich::DrawLabel(IGDI *pGDI, R2 Rect)
{
	CString Label = FindLabelText();

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	int cx = GetTextWidth(Label);

	int cy = GetFontSize();

	int xp = (Rect.x2 + Rect.x1 - cx) / 2;

	int yp = (Rect.y2 + Rect.y1 - cy) / 2;

	pGDI->TextOut(xp, yp, UniVisual(Label));
	}

void CPrimRich::DrawValue(IGDI *pGDI, R2 Rect)
{
	pGDI->SelectBrush(brushFore);
	
	DWORD Data   = 0;

	UINT  Type   = typeVoid;

	COLOR Color  = 0xFFFF;
	
	COLOR Shadow = 0xFFFF;

	if( m_pValue ) {

		Data = m_pValue->ExecVal();

		Type = m_pValue->GetType();
		}

	FindColors( pGDI,
		    Color,
		    Shadow,
		    Data,
		    Type,
		    FALSE
		    );

	CString Value = FindValueText(Data, Type, fmtPad);

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	int xPos = (Rect.x2 + Rect.x1 - GetTextWidth(Value)) / 2;

	int yPos = (Rect.y2 + Rect.y1 - GetFontSize()) / 2;

	if( !(Shadow & 0x8000) ) {

		pGDI->SetTextFore(Shadow);

		pGDI->TextOut(xPos + 1, yPos + 2, Value);

		pGDI->SetTextFore(Color);
		}

	pGDI->TextOut(xPos, yPos, Value);
	}

// Attributes

int CPrimRich::GetFontSize(void) const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = pFonts->GetHeight(m_Font);

	nSize += m_Margin.top;

	nSize += m_Margin.bottom;

	return nSize;
	}

int CPrimRich::GetTextWidth(PCTXT pText) const
{
	CUISystem    * pSystem = (CUISystem *) GetDatabase()->GetSystemItem();

	CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

	int            nSize   = pFonts->GetWidth(m_Font, pText);

	nSize += m_Margin.left;

	nSize += m_Margin.right;

	return nSize;
	}

// Field Requirements

UINT CPrimRich::GetNeedMask(void) const
{
	return propLimits | propFormat | propColor;
	}

// Implementation

COLOR CPrimRich::FindCompColor(COLOR c)
{
	BYTE  r = GetRED  (c);
	BYTE  g = GetGREEN(c);
	BYTE  b = GetBLUE (c);

	BYTE  i = BYTE((14*r + 45*g + 5*b) / 64);

	if( i >= 15 ) {

		return GetRGB(0,0,0);
		}

	return GetRGB(31,31,31);
	}

void CPrimRich::LimitTypes(IUIHost *pHost)
{
	LimitEnum(pHost, L"FormType", m_FormType, 0x0FF);

	LimitEnum(pHost, L"ColType",  m_ColType,  0x01F);
	}

void CPrimRich::KillUnused(IUIHost *pHost)
{
	if( UseTagLimits() ) {

		KillCoded(pHost, L"LimitMin", m_pLimitMin);

		KillCoded(pHost, L"LimitMax", m_pLimitMax);
		}

	if( UseTagLabel() ) {

		KillCoded(pHost, L"Label", m_pLabel);
		}

	if( UseTagFormat() ) {

		KillFormat();
		}

	if( UseTagColor() ) {

		KillColor();
		}

	}

BOOL CPrimRich::KillFormat(void)
{
	if( m_pFormat ) {

		m_pFormat->Kill();

		delete m_pFormat;

		m_pFormat  = NULL;

		m_FormType = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRich::KillColor(void)
{
	if( m_pColor ) {

		m_pColor->Kill();

		delete m_pColor;

		m_pColor  = NULL;

		m_ColType = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimRich::LoadFirstPage(CUIPageList *pList)
{
	CPrimRichPage *pPage = New CPrimRichPage(this, CString(IDS_DATA), 1);

	pList->Append(pPage);

	return TRUE;
	}

BOOL CPrimRich::LoadRichPages(CUIPageList *pList)
{
	if( ShowProp(propLimits) || ShowProp(propEntry) ) {

		CString Name = ShowProp(propEntry) ? CString(IDS_ENTRY) : CString(IDS_LIMITS);

		CPrimRichPage *pPage = New CPrimRichPage(this, Name, 3);
	
		pList->Append(pPage);
		}

	if( ShowProp(propLabel) || ShowProp(propFormat) ) {
	
		CPrimRichPage *pPage = New CPrimRichPage(this, CString(IDS_FORMAT_1), 5);
	
		pList->Append(pPage);
		}

	if( ShowProp(propColor) ) {
	
		CPrimRichPage *pPage = New CPrimRichPage(this, CString(IDS_COLORS), 7);
	
		pList->Append(pPage);
		}

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       11
				       ));

	return TRUE;
	}

void CPrimRich::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("TagLimits",   NeedProp(propLimits) && HasTagLimits());

	pHost->EnableUI("TagLabel",    NeedProp(propLabel)  && HasTagLabel());

	pHost->EnableUI("TagFormat",   NeedProp(propFormat) && HasTagFormat());
	
	pHost->EnableUI("TagColor",    HasTagColor());
	
	pHost->EnableUI("Enable",      ShowProp(propEntry) && m_Entry);
	
	pHost->EnableUI("OnSetFocus",  ShowProp(propEntry) && m_Entry);
	
	pHost->EnableUI("OnKillFocus", ShowProp(propEntry) && m_Entry);
	
	pHost->EnableUI("OnComplete",  ShowProp(propEntry) && m_Entry);
	
	pHost->EnableUI("LimitMin",    NeedProp(propLimits));

	pHost->EnableUI("LimitMax",    NeedProp(propLimits));
				    
	pHost->EnableUI("Label",       NeedProp(propLabel)  && !UseTagLabel());

	pHost->EnableUI("FormType",    NeedProp(propFormat) && !UseTagFormat());

	pHost->EnableUI("ColType",     NeedProp(propColor)  && !UseTagColor());
	}

// End of File
