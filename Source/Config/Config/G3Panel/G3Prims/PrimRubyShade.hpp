
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyShade_HPP
	
#define	INCLUDE_RubyShade_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyTankFill.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Shading Brush
//

class DLLAPI CPrimRubyShade : public CPrimRubyTankFill
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyShade(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Recoloring
		void Recolor(IGdi *pGdi, PDWORD pData, int cx, int cy);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Mask;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void  DoEnables(IUIHost *pHost);
		DWORD To888(WORD a, WORD c);
	};

// End of File

#endif
