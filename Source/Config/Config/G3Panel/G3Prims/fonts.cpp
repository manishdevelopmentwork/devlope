
#include "intern.hpp"

#include "fontdlg.hpp"

#include "chinese.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////
//
// Byte Order Conversions
//

// Move to PCCore!

inline static WORD HostToMotor(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
}

inline static WORD MotorToHost(WORD Data)
{
	WORD wHi = WORD(Data >> 8);

	WORD wLo = WORD(Data << 8);

	return WORD(wHi | wLo);
}

inline static DWORD HostToMotor(DWORD Data)
{
	DWORD dwHi = HostToMotor(WORD(Data >> 16));

	DWORD dwLo = HostToMotor(WORD(Data)) << 16;

	return dwHi | dwLo;
}

inline static DWORD MotorToHost(DWORD Data)
{
	DWORD dwHi = HostToMotor(WORD(Data >> 16));

	DWORD dwLo = HostToMotor(WORD(Data)) << 16;

	return dwHi | dwLo;
}

//////////////////////////////////////////////////////////////////////////
//
// Font List
//

// REV3 -- Allow smoothing to be turned on and off.

// REV3 -- Size gylphs within different sets.

// REV3 -- Automatically select correct glyphs?

// Dynamic Class

AfxImplementDynamicClass(CFontList, CItemIndexList);

// Constructor

CFontList::CFontList(void)
{
	m_Class = AfxRuntimeClass(CFontItem);
}

// Destructor

CFontList::~CFontList(void)
{
}

// Item Location

CFontItem * CFontList::GetItem(INDEX Index) const
{
	return (CFontItem *) CItemIndexList::GetItem(Index);
}

CFontItem * CFontList::GetItem(UINT uPos) const
{
	return (CFontItem *) CItemIndexList::GetItem(uPos);
}

CFontItem * CFontList::GetFont(UINT uFont) const
{
	if( uFont >= 0x100 ) {

		return GetItem(uFont - 0x100);
	}

	return NULL;
}

// Operations

void CFontList::MarkAsUnused(void)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->m_Used = 0;

		GetNext(Index);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Font Item
//

// Dynamic Class

AfxImplementDynamicClass(CFontItem, CUIItem);

// Constructor

CFontItem::CFontItem(void)
{
	if( !m_fSorted ) {

		qsort(m_Chinese, elements(m_Chinese), 2, SortFunc);

		m_fSorted = TRUE;
	}

	m_Used   = 0;

	m_Handle = HANDLE_NONE;

	m_Auto   = 0;

	m_Glyphs = 0;

	m_Level  = 0;

	m_Smooth = 2;

	m_pDC    = NULL;

	m_pFont  = NULL;

	m_pData  = NULL;

	m_fFill  = FALSE;
}

// Destructor

CFontItem::~CFontItem(void)
{
	if( m_pDC ) {

		m_pDC->Deselect();

		delete m_pFont;

		delete m_pDC;

		delete m_pData;
	}
}

// IBase

UINT CFontItem::Release(void)
{
	delete this;

	return 0;
}

// Font Attributes

BOOL CFontItem::IsProportional(void)
{
	return TRUE;
}

int CFontItem::GetBaseLine(void)
{
	return m_nBase;
}

int CFontItem::GetGlyphWidth(WORD c)
{
	if( !CanDraw(c) ) {

		return GetGlyphWidth('?');
	}
	else {
		GLYPHMETRICS Metrics;

		BOOL         fDraw;

		if( FindGlyph(Metrics, c, FALSE, fDraw) ) {

			return Metrics.gmCellIncX;
		}

		return m_Size;
	}
}

int CFontItem::GetGlyphHeight(WORD c)
{
	return m_Size;
}

// Font Operations

void CFontItem::InitBurst(IGDI *pGDI, CLogFont const &Font)
{
	if( Font.m_Trans == modeOpaque ) {

		pGDI->PushBrush();

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Font.m_Back);

		m_fFill = TRUE;
	}

	m_Fore = Font.m_Fore;

	m_Back = Font.m_Back;

	m_Edit = Font.m_Smooth == 0;
}

void CFontItem::DrawGlyph(IGDI *pGDI, int &x, int &y, WORD c)
{
	if( !CanDraw(c) ) {

		DrawGlyph(pGDI, x, y, '?');
	}
	else {
		GLYPHMETRICS Metrics;

		BOOL         fDraw;

		BOOL         fSmooth;

		if( !m_Edit && IsSmooth() ) {

			fSmooth = TRUE;
		}
		else
			fSmooth = FALSE;

		if( FindGlyph(Metrics, c, fSmooth, fDraw) ) {

			if( fDraw ) {

				int xPos  = x + Metrics.gmptGlyphOrigin.x;

				int yCell = m_nFrom - Metrics.gmptGlyphOrigin.y;

				int yPos  = y + max(0, yCell);

				if( m_fFill ) {

					// LATER -- Just drawing missing sections?

					pGDI->FillRect(x,
						       y,
						       x + Metrics.gmCellIncX,
						       y + m_Size
					);
				}

				if( fSmooth ) {

					int nStride = 2 * ((Metrics.gmBlackBoxX + 3) / 4);

					if( m_fFill ) {

						pGDI->CharBlt(xPos,
							      yPos,
							      Metrics.gmBlackBoxX,
							      Metrics.gmBlackBoxY,
							      nStride,
							      m_pData,
							      m_Back,
							      m_Fore
						);
					}
					else {
						pGDI->CharBlt(xPos,
							      yPos,
							      Metrics.gmBlackBoxX,
							      Metrics.gmBlackBoxY,
							      nStride,
							      m_pData,
							      m_Fore
						);
					}
				}
				else {
					int nStride = 4 * ((Metrics.gmBlackBoxX + 31) / 32);

					pGDI->CharBlt(xPos,
						      yPos,
						      Metrics.gmBlackBoxX,
						      Metrics.gmBlackBoxY,
						      nStride,
						      m_pData
					);
				}
			}
			else {
				if( m_fFill ) {

					pGDI->FillRect(x,
						       y,
						       x + Metrics.gmCellIncX,
						       y + m_Size
					);
				}
			}

			x += Metrics.gmCellIncX;
		}
		else {
			if( c == '?' ) {

				FindGlyph(Metrics, c, fSmooth, fDraw);

				if( m_fFill ) {

					pGDI->FillRect(x,
						       y,
						       x + m_Size,
						       y + m_Size
					);
				}

				pGDI->PushBrush();

				pGDI->SetBrushFore(m_Fore);

				pGDI->FillRect(x + 1,
					       y + 1,
					       x + m_Size - 2,
					       y + m_Size - 2
				);

				pGDI->PullBrush();

				x += m_Size;
			}
			else
				DrawGlyph(pGDI, x, y, '?');
		}
	}
}

void CFontItem::BurstDone(IGDI *pGDI, CLogFont const &Font)
{
	if( m_fFill ) {

		pGDI->PullBrush();

		m_fFill = FALSE;
	}
}

// Attributes

CString CFontItem::Describe(void) const
{
	CString Name = m_Face;

	Name += CPrintf(L" %u ", m_Size);

	Name += m_Bold ? CString(IDS_BOLD) : CString(IDS_REGULAR);

	return Name;
}

BOOL CFontItem::IsSame(CFontDialog &Dlg) const
{
	if( Dlg.GetPointSize() == UINT(m_Size) ) {

		if( Dlg.IsFontBold() == m_Bold ) {

			if( Dlg.GetFaceName() == m_Face ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CFontItem::IsSame(CString Face, UINT uSize, BOOL fBold) const
{
	if( uSize == UINT(m_Size) ) {

		if( fBold == m_Bold ) {

			if( Face == m_Face ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CFontItem::IsSame(CFontItem *pFont) const
{
	if( pFont->m_Size == m_Size ) {

		if( pFont->m_Bold == m_Bold ) {

			if( pFont->m_Face == m_Face ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

UINT CFontItem::GetFontID(void) const
{
	return 0x100 + GetIndex();
}

BOOL CFontItem::IsSmooth(void) const
{
	if( m_Glyphs == glyphLegacy ) {

		return FALSE;
	}

	switch( m_Smooth ) {

		case 0:
			return FALSE;

		case 1:
			return TRUE;
	}

	return m_Size >= 32;
}

// Operations

void CFontItem::Create(CFontDialog &Dlg)
{
	m_Face = Dlg.GetFaceName();

	m_Size = Dlg.GetPointSize();

	m_Bold = Dlg.IsFontBold();

	CreateObjects();

	RenderFont();
}

void CFontItem::Create(CString Face, UINT uSize, BOOL fBold)
{
	m_Face = Face;

	m_Size = uSize;

	m_Bold = fBold;

	CreateObjects();

	RenderFont();
}

BOOL CFontItem::LoadFromTreeFile(CTreeFile &Tree)
{
	Tree.GetObject();

	Load(Tree);

	Tree.EndObject();

	PostPaste();

	PostLoad();

	return TRUE;
}

BOOL CFontItem::Edit(CWnd &Wnd)
{
	CStringArray List;

	List.Append(CString(IDS_NUMERIC_OUTPUT));
	List.Append(CString(IDS_BASIC_ASCII));
	List.Append(CString(IDS_FRACTIONS_AND));
	List.Append(CString(IDS_BASIC_ACCENTED));
	List.Append(CString(IDS_RUSSIAN_AND_GREEK));
	List.Append(CString(IDS_MORE_ACCENTED));
	List.Append(CString(IDS_HIRAGANA));
	List.Append(CString(IDS_FULLWIDTH));
	List.Append(CString(IDS_HALFWIDTH));
	List.Append(CString(IDS_CHINESE_PINYIN));

	if( C3OemFeature(L"OemSD", FALSE) ) {

		List.Append(CString(IDS_R_LEGACY_SET));
	}
	else
		List.Append(L"");

	List.Append(CString(IDS_KOREAN_JAMO));
	List.Append(CString(IDS_KOREAN_HANGUL));
	List.Append(CString(IDS_HEBREW));
	List.Append(CString(IDS_CJK_IDEOGRAPHS));

	CBitMaskData BitMask;

	BitMask.SetParent(this);

	BitMask.Init();

	BitMask.m_Data = m_Glyphs;

	BitMask.SetColumns(1);

	BitMask.LoadNames(List);

	CItemDialog Dialog(&BitMask, CString(IDS_SELECT_INCLUDED));

	if( Dialog.Execute(Wnd) ) {

		m_Glyphs = BitMask.m_Data;

		Wnd.UpdateWindow();

		CreateFont();

		RenderFont();

		SetDirty();

		return TRUE;
	}

	return FALSE;
}

// Persistance

void CFontItem::Init(void)
{
	CUIItem::Init();

	m_Glyphs = glyphCore | glyphCoreAccented;

	m_Level  = 1;
}

void CFontItem::PostLoad(void)
{
	CUIItem::PostLoad();

	if( !m_Glyphs ) {

		if( C3OemFeature(L"OemSD", FALSE) ) {

			m_Glyphs = glyphLegacy;
		}
		else
			m_Glyphs = glyphCore | glyphCoreAccented;
	}

	if( m_Level < 1 ) {

		m_Level = 1;
	}

	CreateObjects();

	RenderFont();
}

// Property Save Filter

BOOL CFontItem::SaveProp(CString const &Tag) const
{
	if( Tag == "Data" ) {

		return FALSE;
	}

	return CUIItem::SaveProp(Tag);
}

// Download Support

BOOL CFontItem::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_FONT);

	CUIItem::MakeInitData(Init);

	UINT   uCount = m_Data.GetCount();

	PCBYTE pData  = m_Data.GetPointer();

	Init.AddLong(DWORD(uCount));

	Init.AddData(pData, uCount);

	return TRUE;
}

// Meta Data Creation

void CFontItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(Used);
	Meta_AddInteger(Auto);
	Meta_AddInteger(Glyphs);
	Meta_AddInteger(Level);
	Meta_AddInteger(Smooth);
	Meta_AddString(Face);
	Meta_AddInteger(Size);
	Meta_AddInteger(Bold);
	Meta_AddBlob(Data);

	Meta_SetName((IDS_FONT));
}

// Implementation

void CFontItem::CreateObjects(void)
{
	m_uSize = 2 * m_Size * m_Size;

	m_pData = New BYTE[m_uSize];

	m_pDC   = New CMemoryDC;

	m_pFont = New CFont;

	CreateFont();
}

void CFontItem::CreateFont(void)
{
	LOGFONT	LogFont;

	LogFont.lfHeight	 = m_Size;
	LogFont.lfWidth		 = 0;
	LogFont.lfEscapement	 = 0;
	LogFont.lfOrientation	 = 0;
	LogFont.lfWeight	 = m_Bold ? FW_BOLD : FW_NORMAL;
	LogFont.lfItalic	 = FALSE;
	LogFont.lfUnderline	 = FALSE;
	LogFont.lfStrikeOut	 = FALSE;
	LogFont.lfCharSet	 = DEFAULT_CHARSET;
	LogFont.lfOutPrecision	 = OUT_DEFAULT_PRECIS;
	LogFont.lfClipPrecision	 = CLIP_DEFAULT_PRECIS;
	LogFont.lfQuality	 = DEFAULT_QUALITY;
	LogFont.lfPitchAndFamily = DEFAULT_PITCH | FF_DONTCARE;

	wstrcpy(LogFont.lfFaceName, m_Face);

	if( m_Glyphs == glyphLegacy ) {

		TEXTMETRIC TextMetric;

		m_pFont->Attach(CreateFontIndirect(&LogFont));

		m_pDC->Select(m_pFont[0]);

		m_pDC->GetTextMetrics(TextMetric);

		m_pDC->Deselect();

		LogFont.lfHeight += TextMetric.tmInternalLeading;

		m_pFont->Attach(CreateFontIndirect(&LogFont));

		m_pDC->Select(m_pFont[0]);

		OUTLINETEXTMETRIC OutlineMetric;

		UINT uSize = sizeof(OutlineMetric);

		OutlineMetric.otmSize = uSize;

		GetOutlineTextMetrics(m_pDC->GetHandle(), uSize, &OutlineMetric);

		m_nBase = m_Size - OutlineMetric.otmAscent;

		m_nFrom = m_Size - m_nBase;
	}
	else {
		BOOL fDown = FALSE;

		for( ;;) {

			m_pFont->Attach(CreateFontIndirect(&LogFont));

			m_pDC->Select(m_pFont[0]);

			INT Span = ScanGlyphs();

			if( Span == m_Size ) {

				break;
			}

			if( Span < m_Size ) {

				if( fDown ) {

					break;
				}

				LogFont.lfHeight += m_Size - Span;
			}
			else {
				if( LogFont.lfHeight < m_Size / 2 ) {

					break;
				}

				LogFont.lfHeight -= 1;

				fDown = TRUE;
			}

			m_pDC->Deselect();
		}

		TEXTMETRIC TextMetric;

		m_pDC->GetTextMetrics(TextMetric);

		// LATER -- Is this right? It's not actually used...

		m_nBase = TextMetric.tmDescent;
	}

	m_xDigit = GetGlyphWidth('4');

	m_xSpace = GetGlyphWidth(' ');
}

INT CFontItem::ScanGlyphs(void)
{
	CByteArray CharList;

	CByteArray CharWidth;

	CByteArray CharPos;

	CByteArray CharData;

	int       nMin    = 10000000;

	int	  nMax    = 0;

	BOOL      fSmooth = IsSmooth();

	HDC       hDC     = m_pDC->GetHandle();

	UINT      uSize   = GetFontUnicodeRanges(hDC, NULL);

	PBYTE     pData   = New BYTE[uSize];

	GLYPHSET *pSet    = (GLYPHSET *) pData;

	GetFontUnicodeRanges(hDC, pSet);

	for( UINT n = 0; n < pSet->cRanges; n++ ) {

		WORD c = pSet->ranges[n].wcLow;

		for( UINT i = 0; i < pSet->ranges[n].cGlyphs; i++ ) {

			if( UseGlyph(c) ) {

				GLYPHMETRICS Metrics;

				BOOL         fDraw;

				if( FindGlyph(Metrics, c, fSmooth, fDraw) ) {

					if( fDraw ) {

						int nTo   = Metrics.gmptGlyphOrigin.y;

						int nFrom = nTo - Metrics.gmBlackBoxY;

						MakeMax(nMax, nTo);

						MakeMin(nMin, nFrom);
					}
				}
			}

			c++;
		}
	}

	delete[] pData;

	if( nMin > nMax ) {

		// REV3 -- This happens if we get a non-TrueType font
		// during import. It would be nice to do a better job
		// rendering such fonts, but it's rather painful.

		return m_Size;
	}

	m_nFrom = nMax;

	return nMax - nMin;
}

BOOL CFontItem::FindGlyph(GLYPHMETRICS &Metrics, TCHAR cData, BOOL fSmooth, BOOL &fDraw)
{
	if( IsFixed(cData) ) {

		if( IsFixedDigit(cData) ) {

			cData -= digitFixed;

			cData += digitSimple;
		}
		else {
			cData -= letterFixed;

			cData += letterSimple;
		}

		if( FindGlyph(Metrics, cData, fSmooth, fDraw) ) {

			int x1 = (m_xDigit - Metrics.gmCellIncX) / 2;

			Metrics.gmCellIncX = short(m_xDigit);

			Metrics.gmptGlyphOrigin.x += x1;

			return TRUE;
		}

		return FALSE;
	}

	if( IsSpace(cData) ) {

		CSize Size;

		switch( cData ) {

			case spaceHair:

				cData   = spaceNormal;

				Size.cx = 1;

				Size.cy = m_pDC->GetTextExtent(&cData, 1).cy;

				break;

			case spaceNormal:
			case spaceNoBreak:

				cData = spaceNormal;

				Size  = m_pDC->GetTextExtent(&cData, 1);

				break;

			case spaceNarrow:

				cData = spaceNormal;

				Size  = m_pDC->GetTextExtent(&cData, 1) / CSize(2, 1);

				break;

			case spaceFigure:

				cData = L'4';

				Size  = m_pDC->GetTextExtent(&cData, 1);

				break;

			default:

				Size  = m_pDC->GetTextExtent(&cData, 1);

				break;
		}

		Metrics.gmBlackBoxX       = 0;

		Metrics.gmBlackBoxY       = 0;

		Metrics.gmCellIncX        = short(Size.cx);

		Metrics.gmCellIncY        = short(Size.cy);

		Metrics.gmptGlyphOrigin.x = 0;

		fDraw = FALSE;

		return TRUE;
	}

	if( !IsExplicitCode(cData) ) {

		WORD wIndex = 0xFFFF;

		GetGlyphIndices(m_pDC->GetHandle(),
				&cData,
				1,
				&wIndex,
				GGI_MARK_NONEXISTING_GLYPHS
		);

		if( wIndex < 0xFFFF ) {

			MAT2 Mat;

			memset(&Mat, 0, sizeof(Mat));

			Mat.eM11.value = 1;

			Mat.eM22.value = 1;

			UINT uFormat = fSmooth ? GGO_GRAY4_BITMAP : GGO_BITMAP;

			int  nCount  = GetGlyphOutline(m_pDC->GetHandle(),
						       cData,
						       uFormat,
						       &Metrics,
						       m_uSize,
						       m_pData,
						       &Mat
			);

			if( nCount > 0 ) {

				if( fSmooth ) {

					int i = 0;

					for( int j = 0; j < nCount; j += 2 ) {

						BYTE b0 = m_pData[j+0];

						BYTE b1 = m_pData[j+1];

						if( b0 > 7 ) b0--;

						if( b1 > 7 ) b1--;

						m_pData[i++] = BYTE(b1|(b0<<4));
					}
				}
				else {
					// TODO -- Seems like we do a lot of byte swapping to
					// Motorola ordering when our platsform are all now
					// little endian!!!!

					PDWORD p = PDWORD(m_pData);

					int    r = (Metrics.gmBlackBoxX + 31) / 32;

					int    c = Metrics.gmBlackBoxY * r;

					for( int n = 0; n < c; n++ ) {

						p[n] = HostToMotor(p[n]);
					}
				}

				if( Metrics.gmptGlyphOrigin.x < 0 ) {

					Metrics.gmCellIncX        = short(Metrics.gmCellIncX - Metrics.gmptGlyphOrigin.x);

					Metrics.gmptGlyphOrigin.x = 0;
				}

				fDraw = TRUE;

				return TRUE;
			}
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CFontItem::IsFixed(TCHAR cData)
{
	return IsFixedDigit(cData) || IsFixedLetter(cData);
}

BOOL CFontItem::IsFixedDigit(TCHAR cData)
{
	if( cData >= digitFixed && cData < digitFixed + 10 ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CFontItem::IsFixedLetter(TCHAR cData)
{
	if( cData >= letterFixed && cData < letterFixed + 6 ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CFontItem::IsSpace(TCHAR cData)
{
	switch( cData ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
	}

	return FALSE;
}

BOOL CFontItem::IsExplicitCode(TCHAR cData)
{
	switch( cData ) {

		case uniLRE:
		case uniRLE:
		case uniLRO:
		case uniRLO:
		case uniLRM:
		case uniRLM:
		case uniPDF:

			return TRUE;
	}

	return FALSE;
}

void CFontItem::AddByte(CByteArray &Data, UINT n)
{
	Data.Append(LOBYTE(n));
}

void CFontItem::AddWord(CByteArray &Data, UINT n)
{
	Data.Append(HIBYTE(n));

	Data.Append(LOBYTE(n));
}

void CFontItem::AddLong(CByteArray &Data, UINT n)
{
	AddWord(Data, HIWORD(n));

	AddWord(Data, LOWORD(n));
}

void CFontItem::RenderFont(void)
{
	CByteArray CharList;

	CByteArray CharWidth;

	CByteArray CharPos;

	CByteArray CharData;

	BOOL        fSmooth = IsSmooth();

	UINT        uCount  = 0;

	HDC         hDC     = m_pDC->GetHandle();

	UINT        uSize   = GetFontUnicodeRanges(hDC, NULL);

	PBYTE       pData   = New BYTE[uSize];

	GLYPHSET  * pSet    = (GLYPHSET *) pData;

	GetFontUnicodeRanges(hDC, pSet);

	for( UINT n = 0; n < pSet->cRanges; n++ ) {

		WORD c = pSet->ranges[n].wcLow;

		for( UINT i = 0; i < pSet->ranges[n].cGlyphs; i++ ) {

			if( UseGlyph(c) ) {

				GLYPHMETRICS Metrics;

				BOOL         fDraw;

				AddWord(CharList, c);

				if( FindGlyph(Metrics, c, fSmooth, fDraw) ) {

					if( fDraw ) {

						AddLong(CharPos, CharData.GetCount());

						int nSource = 0;

						int nUsed   = 0;

						int nStride = 0;

						int xCell   = Metrics.gmptGlyphOrigin.x;

						int yCell   = m_nFrom - Metrics.gmptGlyphOrigin.y;

						AddByte(CharData, xCell);

						AddByte(CharData, max(0, yCell));

						AddByte(CharData, 0);

						AddByte(CharData, fSmooth);

						AddWord(CharData, Metrics.gmBlackBoxX);

						AddWord(CharData, Metrics.gmBlackBoxY);

						if( fSmooth ) {

							nSource = 2 * ((Metrics.gmBlackBoxX + 3) / 4);

							nUsed   = 1 * ((Metrics.gmBlackBoxX + 1) / 2);

							nStride = nUsed;
						}
						else {
							nSource = 4 * ((Metrics.gmBlackBoxX + 31) / 32);

							nUsed   = 1 * ((Metrics.gmBlackBoxX +  7) /  8);

							nStride = nSource;
						}

						if( fSmooth ) {

							for( UINT r = 0; r < Metrics.gmBlackBoxY; r++ ) {

								PCBYTE pFrom = m_pData + nSource * r;

								CharData.Append(pFrom, nUsed);

								for( int p = nUsed; p < nStride; p++ ) {

									AddByte(CharData, 0);
								}
							}
						}
						else {
							for( UINT r = 0; r < Metrics.gmBlackBoxY; r++ ) {

								PCDWORD pFrom = PCDWORD(m_pData + nSource * r);

								for( int n = 0; n < nStride / 4; n++ ) {

									DWORD d = pFrom[n];

									AddByte(CharData, LOBYTE(LOWORD(d)));
									AddByte(CharData, HIBYTE(LOWORD(d)));
									AddByte(CharData, LOBYTE(HIWORD(d)));
									AddByte(CharData, HIBYTE(HIWORD(d)));
								}
							}
						}

						while( CharData.GetCount() % 4 ) {

							AddByte(CharData, 0);
						}
					}
					else
						AddLong(CharPos, NOTHING);

					AddWord(CharWidth, Metrics.gmCellIncX);
				}
				else {
					// Can happen with legacy non-TrueType font.

					AddLong(CharPos, NOTHING);

					AddWord(CharWidth, m_Size);
				}

				uCount++;
			}

			c++;
		}
	}

	if( uCount % 2 ) {

		// NOTE -- Maintain DWORD alignment.

		AddWord(CharList, 0xFFFF);

		AddWord(CharWidth, 0x0001);

		AddLong(CharPos, 0x0000);

		uCount++;
	}

	m_Data.Empty();

	AddWord(m_Data, m_Size);

	AddWord(m_Data, m_nBase);

	AddWord(m_Data, uCount);

	AddWord(m_Data, 0);

	m_Data.Append(CharList);

	m_Data.Append(CharWidth);

	m_Data.Append(CharPos);

	m_Data.Append(CharData);

	delete[] pData;
}

BOOL CFontItem::CanDraw(WORD cData)
{
	if( cData >= digitFixed && cData < digitFixed + 10 ) {

		return TRUE;
	}

	switch( cData ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;

		case uniLRM:
		case uniRLM:
		case uniPDF:

			return TRUE;
	}

	return UseGlyph(cData);
}

BOOL CFontItem::UseGlyph(WORD cData)
{
	if( cData ) {

		if( cData == '?' ) {

			return TRUE;
		}

		if( cData == 0x478 || cData == 0x479 || cData == 0x333 ) {

			return FALSE;
		}

		if( m_Glyphs & glyphLegacy ) {

			if( cData >= 0x0020 && cData < 0x00A0 ) {

				return TRUE;
			}

			if( cData == 0x00B0 ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphNumeric ) {

			if( cData >= 0x0030 && cData <= 0x0039 ) {

				return TRUE;
			}

			if( cData == 0x00B0 ) {

				return TRUE;
			}

			if( wstrchr(L"ABCDEF-+,.%?/E", cData) ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphCore ) {

			if( cData >= 0x0020 && cData <= 0x009F ) {

				return TRUE;
			}

			if( cData == 0x00B0 ) {

				return TRUE;
			}

			if( cData == 0x2018 || cData == 0x2019 ) {

				return TRUE;
			}

			if( cData == 0x201C || cData == 0x201D ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphSymbols ) {

			if( cData >= 0x00A0 && cData <= 0x00BF ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphCoreAccented ) {

			if( cData >= 0x00C0 && cData <= 0x00FF ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphExtended ) {

			if( cData >= 0x0100 && cData <= 0x04FF ) {

				if( !(cData != 0x37E && cData >= 0x2BD && cData <= 0x386) ) {

					return TRUE;
				}
			}
		}

		if( m_Glyphs & glyphExtAccented ) {

			if( cData >= 0x1E00 && cData <= 0x1FFF ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphHiragana ) {

			if( cData >= 0x3000 && cData <= 0x303F ) {

				return TRUE;
			}

			if( cData >= 0x3041 && cData <= 0x309F ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphFullKatakana ) {

			if( cData >= 0x3000 && cData <= 0x303F ) {

				return TRUE;
			}

			if( cData >= 0x30A0 && cData <= 0x30FF ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphHalfKatakana ) {

			if( cData >= 0x3000 && cData <= 0x303F ) {

				return TRUE;
			}

			if( cData >= 0xFF66 && cData <= 0xFF9F ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphPinYin ) {

			if( cData >= 0x3000 && cData <= 0x303F ) {

				return TRUE;
			}

			if( bsearch(&cData, m_Chinese, elements(m_Chinese), 2, SortFunc) ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphJamo ) {

			if( cData >= 0x3000 && cData <= 0x303F ) {

				return TRUE;
			}

			if( cData >= 0x1100 && cData <= 0x11FF ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphHangul ) {

			if( cData >= 0x3000 && cData <= 0x303F ) {

				return TRUE;
			}

			if( cData >= 0xAC00 && cData <= 0xD7A3 ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphHebrew ) {

			if( cData >= 0x0591 && cData <= 0x05F4 ) {

				return TRUE;
			}
		}

		if( m_Glyphs & glyphIdeographs ) {

			if( cData >= 0x4E00 && cData <= 0x9FFF ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

int CFontItem::SortFunc(PCVOID p1, PCVOID p2)
{
	WORD w1 = PWORD(p1)[0];

	WORD w2 = PWORD(p2)[0];

	if( w1 < w2 ) return -1;

	if( w1 > w2 ) return +1;

	return 0;
}

// End of File
