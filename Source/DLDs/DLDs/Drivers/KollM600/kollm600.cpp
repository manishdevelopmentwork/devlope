
#include "intern.hpp"

#include "kollm600.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen Driver
//

// Instantiator

INSTANTIATE(CKollmorgen600Driver);

// Constructor

CKollmorgen600Driver::CKollmorgen600Driver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CKollmorgen600Driver::~CKollmorgen600Driver(void)
{
	}

// Transport Specific

// Start Frame

void CKollmorgen600Driver::StartFrame(UINT * pCount)
{
	*pCount = 0;
	}

// Transport Layer

void CKollmorgen600Driver::Send(PCBYTE pTx, UINT uCount)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < uCount; i++ ) AfxTrace1("[%2.2x]", pTx[i] );

	m_pData->Write( pTx, uCount, FOREVER );
	}

BOOL CKollmorgen600Driver::GetReply(PBYTE pRx, UINT uMax, BYTE bTerminator)
{
	UINT uCount = 0;

	UINT uTime  = 1000;

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		WORD wData = Get(uTime);

		if( wData == LOWORD(NOTHING) ) {

			Sleep(20);

			continue;
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		if( uCount < uMax - 1 ) pRx[uCount++] = bData;

		if( bData == bTerminator ) return TRUE;

		if( !bTerminator ) {

			uTime = 50;
			SetTimer(uTime); // reduce timeout for string endings
			}
		}

	return !bTerminator && uCount;
	}

// Port Access

UINT CKollmorgen600Driver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
