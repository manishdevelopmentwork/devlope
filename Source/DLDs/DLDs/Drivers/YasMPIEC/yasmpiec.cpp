
#include "intern.hpp"

#include "yasmpiec.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP IEC Driver
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

#if !defined(SHARED_CODE)

INSTANTIATE(CYaskawaMPIECMaster);

#endif

// Constructor

CYaskawaMPIECMaster::CYaskawaMPIECMaster(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CYaskawaMPIECMaster::~CYaskawaMPIECMaster(void)
{
	}

// Configuration

void MCALL CYaskawaMPIECMaster::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CYaskawaMPIECMaster::Attach(IPortObject *pPort)
{
	}

void MCALL CYaskawaMPIECMaster::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaMPIECMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_ExtendedWrite	= GetByte(pData);
			m_pCtx->m_Endian	= GetByte(pData);
			// the following Gets are obsolete
			m_pCtx->m_IXHead	= GetWord(pData);
			m_pCtx->m_QXHead	= GetWord(pData);
			m_pCtx->m_IBHead	= GetWord(pData);
			m_pCtx->m_QBHead	= GetWord(pData);
			m_pCtx->m_QHead		= GetWord(pData);
			m_pCtx->m_Model		= GetByte(pData);

			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_fDirty	= FALSE;

			m_pCtx->m_fQHigh	= m_pCtx->m_QHead > m_pCtx->m_IBHead;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaMPIECMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaMPIECMaster::Ping(void)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}
						
	if( m_pCtx->m_bUnit == 255 ) {

		return CCODE_SUCCESS;
		}	

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SP_4;
	
	Addr.a.m_Offset = 0;
	
	Addr.a.m_Type   = addrWordAsWord;

	Addr.a.m_Extra  = 0;

	return DoWordRead(Addr, Data, 1);
	}

CCODE MCALL CYaskawaMPIECMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	m_fIs64     = Is64Bit(Addr.a.m_Table);

	MakeMin(uCount, 16);

//**/	AfxTrace3("\r\nRead: Table=%d Type=%x Off_1=%x ", Addr.a.m_Table, Addr.a.m_Type, Addr.a.m_Offset);
//**/	AfxTrace3("Off_2=%d Count=%d Extra=%d ", AWork.a.m_Offset, uCount, Addr.a.m_Extra);
//**/	Sleep(50); // slow down for debug

	switch( Addr.a.m_Type ) {

		case BB:
			return DoBitRead(Addr, pData, uCount);

		case WW:
			return DoWordRead(Addr, pData, min(uCount, 16));

		case LL:
		case RR:
			return DoRealRead(Addr, pData, min(uCount, 16));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CYaskawaMPIECMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	m_fIs64     = Is64Bit(Addr.a.m_Table);

//**/	AfxTrace3("\r\n\nWRITE:\r\nTable=%d Type=%x Off_1=%x ", Addr.a.m_Table, Addr.a.m_Type, Addr.a.m_Offset);
//**/	AfxTrace1("Off_2\r\n", AWork.a.m_Offset);

	if( AllowWrite(Addr) ) {

		switch( Addr.a.m_Type ) {

			case BB:
				return DoBitWrite(Addr, pData, 1);

			case WW:
				return DoWordWrite(Addr, pData, min(uCount, 16));

			case LL:
			case RR:
				return DoRealWrite(Addr, pData, min(uCount, 16));
			}
		}

	return uCount;
	}

// Frame Building

void CYaskawaMPIECMaster::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddMotorOrder(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CYaskawaMPIECMaster::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CYaskawaMPIECMaster::AddWord(WORD wData)
{
	BYTE b	= 0;

	if( m_pCtx->m_Endian ) b  = 1;

	if( m_pCtx->m_Model  ) b += 2;

	switch( b ) {

		case 0:	// Little Endian and swap bytes
		case 3:	// Big Endian and don't swap bytes
			AddByte(HIBYTE(wData));
			AddByte(LOBYTE(wData));
			break;

		case 1: // Little Endian and don't swap bytes
		case 2: // Big Endian and swap bytes
			AddByte(LOBYTE(wData));
			AddByte(HIBYTE(wData));
			break;
		}
	}

void CYaskawaMPIECMaster::AddLong(DWORD dwData)
{
	if( m_pCtx->m_Endian ) {

		AddWord(HIWORD(dwData));
		AddWord(LOWORD(dwData));
		}

	else {
		AddWord(LOWORD(dwData));
		AddWord(HIWORD(dwData));
		}
	}

void CYaskawaMPIECMaster::AddDblLong(PDWORD pdwData)
{
	WORD w = m_pCtx->m_Endian ? 0 : 1;

	AddLong(pdwData[w]);

	AddLong(pdwData[1 - w]);
	}

void CYaskawaMPIECMaster::AddMotorOrder(WORD wData) // register and count bytes
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

// Transport Layer

BOOL CYaskawaMPIECMaster::SendFrame(void)
{
	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("[%2.2x]", m_bTxBuff[k]);

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CYaskawaMPIECMaster::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

//**/			for( UINT k = 0; k < uPtr; k++ ) AfxTrace1("<%2.2x>", m_bRxBuff[k]);

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CYaskawaMPIECMaster::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CYaskawaMPIECMaster::CheckFrame(void)
{
	if( !(m_bRxBuff[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Read Handlers

CCODE CYaskawaMPIECMaster::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( GetModbusSpace(Addr) ) {
			
		case SP_0:
			StartFrame(0x01);
			break;
			
		case SP_1:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddMotorOrder((WORD)Addr.a.m_Offset);

	AddMotorOrder((WORD)uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRxBuff[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CYaskawaMPIECMaster::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( GetModbusSpace(Addr) ) {
	
		case SP_4:
			StartFrame(0x03);
			break;
			
		case SP_3:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddMotorOrder((WORD)Addr.a.m_Offset);
	
	AddMotorOrder((WORD)uCount);

	if( Transact(FALSE) ) {

		BOOL fBig = m_pCtx->m_Endian;

		for( UINT n = 0; n < uCount; n++ ) {

			WORD x = PU2(m_bRxBuff + 3)[n];

			if( m_pCtx->m_Model ) { // bytes not swapped within words

				x = fBig ? (WORD)MotorToHost(x) : (WORD)IntelToHost(x);
				}

			else { // bytes are swapped within words

				x = fBig ? (WORD)IntelToHost(x) : (WORD)MotorToHost(x);
				}

			pData[n] = (LONG)((SHORT)x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaMPIECMaster::DoRealRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( GetModbusSpace(Addr) ) {

		case SP_4:
			StartFrame(0x03);
			break;

		case SP_3:
			StartFrame(0x04);
			break;
		}
	
	AddMotorOrder((WORD)Addr.a.m_Offset);
	
	BOOL f64 = m_fIs64;

	UINT uSz = f64 ? 4 : 2; // word count per item

	AddMotorOrder((WORD)(1 + ((uCount - 1) / uSz )) * uSz);

	DWORD d64[2]; // d64[0] = Low DWORD

	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n += uSz ) {

			WORD w1 = PU2(m_bRxBuff + 3)[n];
			WORD w2 = PU2(m_bRxBuff + 5)[n];

			if( f64 ) {

				WORD w3  = PU2(m_bRxBuff + 7)[n];
				WORD w4  = PU2(m_bRxBuff + 9)[n];

				pData[n] = Make64(w1, w2, w3, w4, Addr.a.m_Table);
				}

			else {
				pData[n] = Make32(w1, w2);
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

DWORD CYaskawaMPIECMaster::Make64(WORD w1, WORD w2, WORD w3, WORD w4, UINT uTable)
{
	BOOL fBig = m_pCtx->m_Endian;	// is high word first?

	DWORD d64[2];

	d64[0] = fBig ? Make32(w3, w4) : Make32(w1, w2); // Low DWORD
	d64[1] = fBig ? Make32(w1, w2) : Make32(w3, w4); // High DWORD

	return Convert64To32(d64, uTable);
	}

DWORD CYaskawaMPIECMaster::Make32(WORD w1, WORD w2)
{
	BOOL fBig = m_pCtx->m_Endian;	// is high word first of 2?

	WORD wH;
	WORD wL;

	if( m_pCtx->m_Model ) { // MP26xx selected
		// Byte order = Endian. fBig = (B1HH,B2HL)(B3LH,B4LL). !fBig = (B1LL,B2LH)(B3HL,B4HH)
		wH = fBig ? (WORD)MotorToHost(w1) : (WORD)IntelToHost(w2);
		wL = fBig ? (WORD)MotorToHost(w2) : (WORD)IntelToHost(w1);
		}

	else {	// MP23xx selected
		// Byte order != Endian. fBig = (B1HL,B2HH)(B3LL,B4LH). !fBig = (B1LH,B2LL)(B3HH,B4HL)
		wH = fBig ? (WORD)IntelToHost(w1) : (WORD)MotorToHost(w2);
		wL = fBig ? (WORD)IntelToHost(w2) : (WORD)MotorToHost(w1);
		}

	return MAKELONG(wL, wH);
	}

// Write Handlers

CCODE CYaskawaMPIECMaster::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(5);

	AddMotorOrder((WORD)Addr.a.m_Offset);

	AddMotorOrder( (BOOL)*pData ? 0xFF00 : 0);

	return Transact(TRUE) ? 1 : CCODE_ERROR;
	}

CCODE CYaskawaMPIECMaster::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {
		
		StartFrame(6);

		AddMotorOrder((WORD)Addr.a.m_Offset);
		
		AddWord(LOWORD(pData[0]));
		}
	else {
		StartFrame(16);

		AddMotorOrder((WORD)Addr.a.m_Offset);
		
		AddMotorOrder((WORD)uCount);
		
		AddByte(BYTE(uCount * 2));

		for( UINT n = 0; n < uCount; n++ ) {
			
			AddWord(LOWORD(pData[n]));
			}
		}

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaMPIECMaster::DoRealWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(16);

	AddMotorOrder((WORD)Addr.a.m_Offset);
	
	BOOL f64 = m_fIs64;

	AddMotorOrder(f64 ? 4 : 2);
	
	AddByte((BYTE)(f64 ? 8 : 4));

	if( f64 ) {

		DWORD d64Data[2];

		Convert32To64(*pData, d64Data, Addr.a.m_Table);

		AddDblLong(d64Data);
		}

	else {
		AddLong(*pData);
		}

	return Transact(TRUE) ? 1 : CCODE_ERROR;
	}

// Overridables

UINT CYaskawaMPIECMaster::GetModbusSpace(CAddress Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_IX:	return SP_0;

		case SP_QX:	return SP_1;

		case SP_5:
		case SP_6:
		case SP_QB:
		case SP_Q32:
		case SP_Q64:	return SP_3;

		case SP_7:
		case SP_8:
		case SP_IB:
		case SP_Q:
		case SP_L32:
		case SP_L64:	return SP_4;
		}

	return Addr.a.m_Table;
	}

BOOL CYaskawaMPIECMaster::Is64Bit(UINT uTable)
{
	switch( uTable ) {

		case SP_6:
		case SP_8:
		case SP_Q64:
		case SP_L64:
				return TRUE;
		}

	return FALSE;
	}

DWORD CYaskawaMPIECMaster::Convert64To32( PDWORD p, UINT uTable )
{
	DWORD dOut = 0;

	if( p[1] || (p[0] & 0x7FFFFFFF) ) {

		DWORD dExp = (p[1] >> 20) & 0x7FF;

		dOut  = p[1] & 0x80000000; // Sign Bit

		dOut |= (dExp - 896) << 23;

		dOut |= (p[1] & 0xFFFFF) << 3;

		dOut |= p[0] >> 29;
		}

	return dOut;
	}

void CYaskawaMPIECMaster::Convert32To64( DWORD d, PDWORD p, UINT uTable )
{
	if( !(d & 0x7FFFFFFF) ) { // Zero or Negative Zero

		p[0] = 0;

		p[1] = 0;

		return;
		}

	DWORD dOut = d & 0x80000000; // Sign Bit

	DWORD dExp = (d>>23) & 0xFF; // 8 bit exponent in 32 bit IEEE

	dExp += 896; // 1023 - 127

	dOut |= dExp << 20; // 11 bit exponent in 64 bit IEEE

	p[0]  = dOut | ( (d & 0x7FFFFF) >> 3 );

	p[1]  = ( ( d & 7 ) << 29 );
	}

// Helpers

BOOL CYaskawaMPIECMaster::AllowWrite(AREF Addr)
{
	switch( GetModbusSpace(Addr) ) {

		case SP_3:
		case SP_5:
		case SP_6:
		case SP_QX:
		case SP_QB:
		case SP_Q32:
		case SP_Q64:
				return FALSE;
		}

	return TRUE;
	}

// Socket Management

BOOL CYaskawaMPIECMaster::CheckSocket(void)
{
	if( !m_pCtx->m_fDirty && m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaMPIECMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, (WORD)uPort) == S_OK ) {

			m_uKeep++;

			if( m_pCtx->m_fDirty ) {

				SetTimer(0);

				m_pCtx->m_fDirty = FALSE;
				}
			else {
				SetTimer(m_pCtx->m_uTime1);
				}

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CYaskawaMPIECMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
