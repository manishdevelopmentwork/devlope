
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DeviceResTreeWnd_HPP

#define INCLUDE_DeviceResTreeWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsManager;
class CCommsPortList;

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Resource Window
//

class CDeviceResTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CDeviceResTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		CCommsManager * m_pComms;
		UINT            m_cfCode;
		UINT		m_cfPart;
		HTREEITEM       m_hRack;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Notification Handlers
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData);

		// Drag Hooks
		DWORD FindDragAllowed(void);

		// Tree Loading
		void LoadTree(void);
		void LoadRoot(void);
		void LoadRack(void);
		void LoadNull(HTREEITEM hRoot);
		void LoadPorts(CCommsPortList *pPorts);
		void LoadDevice(HTREEITEM hRoot, CCommsDevice *pDevice);
		BOOL LoadDriver(HTREEITEM hRoot, CCommsDevice *pDevice, CAddrData *pRoot);

		// Item Hooks
		void KillItem(HTREEITEM hItem, BOOL fRemove);

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Implementation
		UINT FindAddrImage(ICommsDriver *pDriver, CItem *pConfig, CAddress const &Addr);
	};

// End of File

#endif
