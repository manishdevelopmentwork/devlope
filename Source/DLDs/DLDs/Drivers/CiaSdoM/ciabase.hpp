
#ifndef INCLUDE_CIABASE_HPP

#define INCLUDE_CIABASE_HPP

//////////////////////////////////////////////////////////////////////////
//
// CAN Frame
//

struct FRAME
{
	BYTE	bCount;
	BYTE	bZero;
	WORD	wID;
	BYTE	bData[8];
	};

//////////////////////////////////////////////////////////////////////////
//
// CAN Port Handler
//

class CCANPortHandler : public IPortHandler
{	
	public:
		// Constructor
		CCANPortHandler(void);

		// Destructor
		~CCANPortHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);
		
		// Operations
		BOOL GetFrame(FRAME &Frame, UINT uTime);
		BOOL PutFrame(FRAME &Frame, UINT uTime);
		
	protected:
		// Data
		ULONG           m_uRefs;
		IPortObject   * m_pPort;
		IEvent        *	m_pTxFlag;
		UINT		m_uTxCount;
		PCBYTE		m_pTxData;
		ISemaphore    * m_pRxFlag;
		UINT		m_uRxHead;
		UINT		m_uRxTail;
		UINT		m_uRxByte;
		FRAME		m_RxData[32];
		UINT		m_uMode;
	};

// End of File

#endif
