#!/bin/sh

# c3-ifbreak <interface>
#
# This script is called when an interface loses its IP address,
# typically because the link has gone down. It may in theory also
# be called by the DHCP client script on loss of lease. The script
# takes down any dependent interfaces, drops any associated DNS
# servers, runs the optional interface-specific script created by
# the net applicator, and then removes the host files entry.

test=/tmp/crimson/up/$1.make

if [ -e $test ]
then
	logger -t c3-net "$1 breaking"

	/opt/crimson/scripts/c3-dep ifdn $1

	/opt/crimson/scripts/c3-drop-dns $1
	
	script=/vap/opt/crimson/config/net/$1.break
	
	if [ -f $script ]
	then
		$script
	fi

	/opt/crimson/scripts/c3-drop-host $1

	ip route flush cache

	rm $test

	logger -t c3-net "$1 broken"
fi
