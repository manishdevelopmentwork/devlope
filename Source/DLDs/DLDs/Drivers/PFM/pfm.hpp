
////////////////////////////////////////////////////////////////////////
//
// Plant Floor Marquee Driver
//

class CPFMDriver : public CDisplayDriver 
{
	public:
		// Constructor
		CPFMDriver(void);

		// Destructor
		~CPFMDriver(void);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(UINT) GetSize(int cx, int cy);
		DEFMETH(void) Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait);
		DEFMETH(void) Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize);

	protected:
		// Data
		UINT		 m_uType;
		DWORD		 m_dwInit;
		IDisplayHelper * m_pDisplayHelper;
		BYTE		 m_bTxData[8192];

		// Implementation
		BOOL SendDrop(BYTE bDrop);
		BOOL SendData(BYTE bDrop, PBYTE pData);
		BOOL WaitReply(void);
		void CaptureVGA (PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait);
		void CaptureMono(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize);
		void GetHelp(void);

		// Helpers
		BYTE FindColor(PCBYTE pSrc, int &p, BOOL fPortrait);
		BOOL IsColor16(void);
	};

// End of File
