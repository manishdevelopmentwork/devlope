
#include "Intern.hpp"

#include "TouchNone.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dummy Touch Screen Driver
//

// Constants

#define cellSize 4

// Instantiator

IDevice * Create_TouchNone(void)
{
	return New CTouchNone;
}

// Constructor

CTouchNone::CTouchNone(void)
{
	StdSetRef();
}

// Destructor

CTouchNone::~CTouchNone(void)
{
}

// IUnknown

HRESULT CTouchNone::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ITouchScreen);

	return E_NOINTERFACE;
}

ULONG CTouchNone::AddRef(void)
{
	StdAddRef();
}

ULONG CTouchNone::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CTouchNone::Open(void)
{
	CAutoObject<IDisplay> pDisplay("display", 0, AfxAeonIID(IDisplay));

	pDisplay->GetSize(m_xDisp, m_yDisp);

	m_xCells = m_xDisp / cellSize;

	m_yCells = m_yDisp / cellSize;

	return TRUE;
}

// ITouchScreen

void CTouchNone::GetCellSize(int &xCell, int &yCell)
{
	xCell = cellSize;

	yCell = cellSize;
}

void CTouchNone::GetDispCells(int &xDisp, int &yDisp)
{
	xDisp = m_xCells;

	yDisp = m_yCells;
}

void CTouchNone::SetBeepMode(bool fBeep)
{
}

void CTouchNone::SetDragMode(bool fDrag)
{
}

PCBYTE CTouchNone::GetMap(void)
{
	return NULL;
}

void CTouchNone::SetMap(PCBYTE pMap)
{
}

bool CTouchNone::GetRaw(int &xRaw, int &yRaw)
{
	xRaw = 0;

	yRaw = 0;

	return true;
}

// End of File
