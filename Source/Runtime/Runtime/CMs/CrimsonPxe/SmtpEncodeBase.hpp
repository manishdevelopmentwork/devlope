
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SmtpEncodeBase_HPP

#define	INCLUDE_SmtpEncodeBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CStdEndpoint;

//////////////////////////////////////////////////////////////////////////
//
// Basic Mail Enoder
//

class CSmtpEncodeBase
{
public:
	// Constructor
	CSmtpEncodeBase(CStdEndpoint *pEndpoint);

	// Operations
	virtual BOOL Encode(CMsgPayload const *pMessage);

protected:
	// Data Members
	CStdEndpoint * m_pEndpoint;

	// Implementation
	BOOL SendHead(CMsgPayload const *pMessage);
	BOOL SendFrom(CString const &Name, CString const &Addr);
	BOOL SendTo(CString const &Name, CString const &Addr);
	BOOL SendDate(void);
	BOOL SendSubj(PCTXT pText);
	BOOL SendBody(PCTXT pText);

	// Formatting
	CString GetAddr(CString const &Name, CString const &Addr);
	CString GetDate(void);
	PCTXT   GetDayName(UINT d);
	PCTXT   GetMonthName(UINT m);

	// Transport
	BOOL Send(PCTXT pText);
};

// End of File

#endif
