
#include "ml1sert.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Tester Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CML1Tester::CML1Tester(void)
{
	m_pExtra = NULL;
	}	

// Destructor

CML1Tester::~CML1Tester(void)
{
	m_pExtra->Release();
	}

// Configuration

void MCALL CML1Tester::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Server	= 0;

		m_AuthInterval	= GetLong(pData) * 1000;
		}

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// User Access

UINT MCALL CML1Tester::DrvCtrl(UINT uFunc, PCTXT Value)
{
	UINT uValue = ATOI(Value);

	switch( uFunc ) {

		case 1:	return m_Server;

		case 2: return m_Unit;

		case 3: return m_Authority;

		case 4: m_Server     = uValue & 0xFF;
			m_fPush      = TRUE;
			m_fAuthentic = FALSE;
			return 1;

		case 5: if( uValue <= 0xFFFFFF - 101 ) {

				m_Unit       = uValue;
				m_fPush      = TRUE;
				m_fAuthentic = FALSE;
				return 1;
				}

			return 0;

		case 6: m_Authority  = uValue;
			m_fPush	     = TRUE;
			m_fAuthentic = FALSE;
			return 1;
		}

	return 0;
	}

UINT MCALL CML1Tester::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CML1Dev * pDev = (CML1Dev *) pContext;

	if( pDev ) {

		UINT uValue = ATOI(Value);

		switch( uFunc ) {

			case 1:	return pDev->m_Server;

			case 2: return pDev->m_Unit;

			case 3: return pDev->m_Authority;

			case 4: pDev->m_Server     = uValue & 0xFF;
				pDev->m_fPush      = TRUE;	
				pDev->m_fOnline    = FALSE;
				pDev->m_fAuthentic = FALSE;
				return 1;

			case 5: if( uValue <= 0xFFFFFF - 101 ) {

					pDev->m_Unit	   = uValue;
					pDev->m_fPush	   = TRUE;
					pDev->m_fOnline    = FALSE;
					pDev->m_fAuthentic = FALSE;
					return 1;
					}

				return 0;

			case 6: pDev->m_Authority  = uValue;
				pDev->m_fPush	   = TRUE;
				pDev->m_fOnline	   = FALSE;
				pDev->m_fAuthentic = FALSE;
				return 1;
			}
		}

	return 0;
	}

// Entry Points

void MCALL CML1Tester::Service(void)
{
	// Server Servicing

	Recv();

	Listen();

	CML1Dev * pDev = m_pHead;
	
	while( pDev ) {

		if( IsAuthenticated(pDev) && pDev->m_fOnline ) {

			CML1Blk * pBlock = pDev->m_pHead;

			while( pBlock ) {

				BOOL fSend = pBlock->Execute(m_pExtra);

				for( UINT o = opRead; o < opWrite; o++ ) {

					if( pBlock->Request(o, fSend) ) {

						SendBlockRequest(pDev, pBlock, o);

						pBlock->SetNext(o);

						fSend = FALSE;
						}
					}

				Listen();

				ForceSleep(100);

				pBlock = pBlock->m_pNext;
				}
			}

		pDev = pDev->m_pNext;
		}
	}

CCODE MCALL CML1Tester::Ping(void)
{
	if( GetMacId() ) {

		if( IsAuthenticated(m_pDev) ) {

			m_pDev->m_fOnline = COMMS_SUCCESS(CMasterDriver::Ping());

			return m_pDev->m_fOnline ? CCODE_SUCCESS : CCODE_ERROR;
			}

		PshAuthentication(m_pDev);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CML1Tester::Read(AREF Address, PDWORD pData, UINT uCount)
{
	ShowDebug("\nTESTER Read %u", uCount);

	if( IsBroadcast(m_pDev->m_Server) ) {

		return CCODE_SUCCESS | CCODE_NO_DATA;
		}

	if( !IsAuthenticated(m_pDev) || !m_pDev->m_fOnline ) {

		return CCODE_ERROR;
		}

	CAddress Addr;

	Addr.m_Ref = Address.m_Ref - 1;

	CML1Blk * pBlock = CML1Base::FindBlock(m_pDev, Addr);

	if( pBlock ) {

		pBlock->Mark(opRead);

		UINT uReturn = pBlock->GetData(Addr, pData, uCount);

		if( pBlock->IsTimedOut(opRead, modeReply) ) {

			if( pBlock->IsExhausted(opRead) ) {

				return CCODE_ERROR;
				}
			}

		GetEntitySize(Addr, uReturn);

		MakeMaxCount(Addr, uReturn);

		if( uReturn < uCount ) {

			pBlock->SetNext(opWrite);
			}

		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CML1Tester::Write(AREF Address, PDWORD pData, UINT uCount)
{
	ShowDebug("\nTESTER Write %u", uCount);

	if( (!IsAuthenticated(m_pDev) && !IsBroadcast(m_pDev->m_Server)) || !m_pDev->m_fOnline ) {

		return CCODE_ERROR;
		}

	CAddress Addr;

	Addr.m_Ref = Address.m_Ref - 1;

	BOOL fSpecial = IsSpecial(Addr);

	if( fSpecial ) {

		// It was decided to use Authentication messages when changing special registers...

		PWORD pWord = PWORD(pData);

		for( UINT u = Addr.a.m_Offset - SR_MIN, c = 0; c < uCount; c++, u++, pWord++ ) {

			if( IsLong(Addr) ) {
				
				SetSr(m_pDev, u, &PWORD(pData)[c]);
				
				pWord++;

				continue;
				}

			pWord++;

			SetSr(m_pDev, u, pWord);
			}

		if( FrcAuthentication(m_pDev, FindSpecialWriteCount(Addr.a.m_Offset - SR_MIN) ) ) {

			return 1;
			}

		// Instead of direct writes...

		/*if( IsLong(Addr) ) {

			if( DoLongWrite(m_pDev, Addr, PBYTE(pData), 1 ) ) {

				return 1;
				}

			return CCODE_ERROR;
			}

		PBYTE pBytes = PBYTE(alloca(uCount * 2));

		for( UINT u = 0; u < uCount; u++ ) {

			PWORD(pBytes + 0)[u] = LOWORD(pData[u]);
			}
		
		if( DoWordWrite(m_pDev, Addr, PBYTE(pBytes), 1) ) {

			return 1;
			}*/

		return CCODE_ERROR;
		}

	CML1Blk * pBlock = CML1Base::FindBlock(m_pDev, Addr);

	if( pBlock ) {

		pBlock->Mark(opWrite);

		UINT uReturn = pBlock->SetData(Addr, pData, uCount);

		if( pBlock->SendOnChange() || fSpecial ) {

			UINT Offset = 0;

			UINT Count  = uReturn;

			if( pBlock->FindOpParams(Addr, Offset, Count) ) {

				pBlock->ClrPend(opWrite);
				
				if( DoBlockWrite(m_pDev, pBlock, Offset, Count) ) {

					pBlock->SetTxTime(modeReqst);

					if( IsBroadcast(m_pDev->m_Server) ) {

						UINT uSent = Count;

						while( uSent < pBlock->GetSize() ) {

							UINT uNext = pBlock->GetNext(Offset + uSent, opWrite); 

							if( uNext ) {

								Count = pBlock->GetSize() - uSent;

								if( DoBlockWrite(m_pDev, pBlock, uNext, Count ) ) {

									uSent += Count;

									Sleep(100);
									}
								}
							}
						
						pBlock->MarkComplete(opWrite, modeReply);

						return uSent;
						}

					GetEntitySize(Addr, uReturn);

					MakeMaxCount(Addr, uReturn);

					if( uReturn < uCount ) {

						pBlock->SetNext(opWrite);
						}

					return uCount;
					}
				}
			}

		if( pBlock->IsTimedOut(opWrite, modeReply) ) {

			if( pBlock->IsExhausted(opWrite) ) {

				return CCODE_ERROR;
				}
			}

		return uReturn;
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL CML1Tester::SendBlockRequest(CML1Dev * pDev, CML1Blk * pBlock, BYTE bOp)
{
	if( pBlock ) {

		if( bOp == opRead ) {

			if( DoBlockRead(pDev, pBlock) ) {

				pBlock->SetRxTime(modeReqst);

				return TRUE;
				}

			return FALSE;
			}

		if( bOp == opWrite ) {

			if( DoBlockWrite(pDev, pBlock) ) {

				pBlock->SetTxTime(modeReqst);

				if( IsBroadcast(m_pDev->m_Server) ) {
					
					pBlock->MarkComplete(opWrite, modeReply);
					}			

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CML1Tester::DoBlockRead(CML1Dev * pDev, CML1Blk * pBlock)
{
	BOOL fOk = FALSE;

	if( pBlock ) {

		AREF Addr  = pBlock->GetAddr();

		UINT uSize = pBlock->GetSize();

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	fOk = DoBitRead (pDev, Addr, uSize);	break;

			case addrWordAsWord:	fOk = DoWordRead(pDev, Addr, uSize);	break;

			case addrWordAsLong:
			case addrWordAsReal:	fOk = DoLongRead(pDev, Addr, uSize);	break;
			}
		}

	if( fOk ) {

		pBlock->IncPend(opRead);
		}
		
	return fOk;
	}

BOOL CML1Tester::DoBlockWrite(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset, UINT uCount)
{
	BOOL fOk = FALSE;

	if( pBlock ) {

		CAddress Addr;
		
		Addr.m_Ref = pBlock->GetAddr().m_Ref;

		UINT uSize = pBlock->GetSize();

		if( uCount ) {

			MakeMin(uSize, uCount);
			}

		Addr.a.m_Offset += uOffset;
		
		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	fOk = DoBitWrite (pDev, Addr, pBlock->m_pTx + uOffset, uSize);	break;

			case addrWordAsWord:	uOffset *= sizeof(WORD);

						fOk = DoWordWrite(pDev, Addr, pBlock->m_pTx + uOffset, uSize);	break;

			case addrWordAsLong:
			case addrWordAsReal:	uOffset *= sizeof(WORD);

						fOk = DoLongWrite(pDev, Addr, pBlock->m_pTx + uOffset, uSize);	break;
				
			}		
		}

	if( fOk ) {

		pBlock->IncPend(opWrite);
		}
	
	return fOk;
	}

// Read Handlers

BOOL CML1Tester::DoWordRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileRead(pDev, Addr, uCount);
		}

	UINT uSent = 0;
	
	while( uSent < uCount ) {

		switch( Addr.a.m_Table ) {
	
			case spaceHR:
			
				StartFrame(pDev, 0x03);
			
				break;
			
			case spaceAI:
					
				StartFrame(pDev, 0x04);
			
				break;
			
			default:
			
				return FALSE;
			}

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddWord(Addr.a.m_Offset + uSent);
	
		AddWord(uSend);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CML1Tester::DoLongRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileRead(pDev, Addr, uCount);
		}

	UINT uSent = 0;

	while( uSent < uCount ) {

		switch( Addr.a.m_Table ) {
	
			case spaceHR:
			
				StartFrame(pDev, 0x03);
			
				break;
			
			case spaceAI:
					
				StartFrame(pDev, 0x04);
			
				break;
			
			default:
				return FALSE;
			}

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddWord(Addr.a.m_Offset + uSent * 2);
	
		AddWord(uSend * 2);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	} 

BOOL CML1Tester::DoBitRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	UINT uSent = 0;
	
	while( uSent < uCount ) {

		switch( Addr.a.m_Table ) {

			case spaceDC:
			
				StartFrame(pDev, 0x01);
			
				break;
			
			case spaceDI:
			
				StartFrame(pDev, 0x02);
			
				break;

			default:
				return FALSE;
			}

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddWord(Addr.a.m_Offset + uSent);
	
		AddWord(uSend);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CML1Tester::DoFileRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	UINT uSent = 0;

	UINT uMult = IsLong(Addr) ? 2 : 1;

	UINT uFile = CML1Base::GetFileNumber(Addr);

	UINT uRec  = CML1Base::GetFileRecord(Addr);
	
	while( uSent < uCount ) {

		StartFrame(pDev, 0x14);
		
		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddByte(0x9);	

		AddByte(0x6);	

		AddWord(uFile);

		AddWord(uRec + uSent * uMult);

		AddWord(uSend * uMult);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	}

// Write Handlers

BOOL CML1Tester::DoWordWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileWrite(pDev, Addr, pData, uCount);
		}

	if( Addr.a.m_Table == spaceHR || Addr.a.m_Table == spaceSR ) {

		if( uCount == 1 ) {
			
			StartFrame(pDev, 6);
			
			AddWord(Addr.a.m_Offset);
			
			AddWord(PWORD(pData)[0]);
			}
		else {
			UINT uSent = 0;

			while( uSent < uCount ) {

				UINT uSend = uCount - uSent;

				MakeMaxCount(Addr, uSend);
			
				StartFrame(pDev, 16);
			
				AddWord(Addr.a.m_Offset + uSent);
			
				AddWord(uSend);
			
				AddByte(uSend * 2);
				
				for( UINT n = uSent; n < uSent + uSend; n++ ) {
				
					AddWord(PWORD(pData)[n]);
					}

				if( Send() ) {

					return TRUE;

					//uSent += uSend;

					//Sleep(100);

					//continue;
					}

				return FALSE;
				}

			return TRUE;
			}

		return Send();
		}

	return FALSE;
	}

BOOL CML1Tester::DoLongWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileWrite(pDev, Addr, pData, uCount);
		}

	if( Addr.a.m_Table == spaceHR || Addr.a.m_Table == spaceSR ) {

		UINT uSent = 0;

		while( uSent < uCount ) {

			UINT uSend = uCount - uSent;

			MakeMaxCount(Addr, uSend);

			StartFrame(pDev, 16);
		
			AddWord(Addr.a.m_Offset + uSent * 2);
		
			AddWord(uSend * 2);
		
			AddByte(uSend * 4);

			for( UINT n = uSent; n < uSent + uSend; n++ ) {

				AddLong(PDWORD(pData)[n]);
				}

			if( Send() ) {

				return TRUE;

				//uSent += uSend;

				//Sleep(100);

				//continue;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CML1Tester::DoBitWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( Addr.a.m_Table == spaceDC ) {

		if( uCount == 1 ) {
			
			StartFrame(pDev, 5);
			
			AddWord(Addr.a.m_Offset);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			UINT uSent = 0;

			while( uSent < uCount ) {

				UINT uSend = uCount - uSent;

				MakeMaxCount(Addr, uSend);
			
				StartFrame(pDev, 15);
			
				AddWord(Addr.a.m_Offset + uSent);
			
				AddWord(uSend);
			
				AddByte((uSend + 7) / 8);
				
				UINT b = 0;

				BYTE m = 1;

				for( UINT n = uSent; n < uSent + uSend; n++ ) {

					if( pData[n] ) {
					
						b |= m;
						}

					if( !(m <<= 1) ) {

						AddByte(b);

						b = 0;

						m = 1;
						}
					}

				if( m > 1 ) {

					AddByte(b);
					}

				if( Send() ) {

					return TRUE;

					//uSent += uSend;

					//Sleep(100);

					//continue;
					}

				return FALSE;
				}

			return TRUE;
			}
			
		return Send();
		}

	return FALSE;
	}

BOOL CML1Tester::DoFileWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	UINT uSent = 0;

	BOOL fLong = IsLong(Addr);

	UINT uMult = fLong ? 2 : 1;

	UINT uFile = CML1Base::GetFileNumber(Addr);

	UINT uRec  = CML1Base::GetFileRecord(Addr);

	while( uSent < uCount ) {

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);
			
		StartFrame(pDev, 0x15);

		AddByte(10 + uSend * 2 * uMult);

		AddByte(6);

		AddWord(uFile);

		AddWord(uRec + uSent * uMult);
			
		AddWord(uSend * uMult);
			
		AddByte(uSend * uMult * 2);
				
		for( UINT n = uSent; n < uSent + uSend; n++ ) {
				
			fLong ? AddLong(PDWORD(pData)[n]) : AddWord(PWORD(pData)[n]);
			}

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}
	
	return TRUE;
	}

// Authentication Support

BOOL CML1Tester::GetMacId(void)
{
	if( IsMacNULL() ) {

		return m_pExtra->GetMacId(0, m_Mac.m_Addr);
		}


	return TRUE;
	}

BOOL CML1Tester::IsAuthenticated(CML1Dev * pDev)
{
	if( pDev ) {

		return pDev->m_fAuthentic && pDev->m_Unit > 0 && !IsMacNULL(pDev) ? TRUE : FALSE;
		}

	return m_fAuthentic && m_Unit > 0 && !IsMacNULL() ? TRUE : FALSE;
	}

BOOL CML1Tester::IsAuthPollTimedOut(CML1Dev * pDev)
{
	if( pDev ) {

		return IsTimedOut(pDev->m_uAuthPoll, m_AuthInterval);
		}

	return IsTimedOut(m_uAuthPoll, m_AuthInterval);
	}

void CML1Tester::SetAuthPoll(CML1Dev * pDev)
{
	if( pDev ) {

		pDev->m_uAuthPoll = GetTickCount();

		return;
		}

	m_uAuthPoll = GetTickCount();
	}

BOOL CML1Tester::PshAuthentication(CML1Dev * pDev)
{
	ShowDebug("\nPush Authentication %u %u", GetTickCount(), pDev ? pDev->m_Server : m_Server); 

	if( pDev && IsAuthPollTimedOut(pDev) ) {

		m_uTxPtr = 0;

		AddByte(authPush);

		AddByte(pDev->m_Server);

		AddMac(pDev);

		AddByte(LOBYTE(HIWORD(pDev->m_Unit)));

		AddWord(LOWORD(pDev->m_Unit));

		AddLong(pDev->m_Authority);
		
		AddSpecial(pDev, GetSpecialExtra());

		AddKey(m_pTx);

		if( Send() ) {

			SetAuthPoll(pDev);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CML1Tester::FrcAuthentication(CML1Dev * pDev, UINT uCount)
{
	ShowDebug("\nForce Authentication %u", uCount); 

	if( pDev ) {

		m_uTxPtr = 0;

		AddByte(authPush);

		AddByte(pDev->m_Server);

		AddMac(pDev);

		AddByte(LOBYTE(HIWORD(pDev->m_Unit)));

		AddWord(LOWORD(pDev->m_Unit));

		AddLong(pDev->m_Authority);
		
		AddSpecial(pDev, uCount);

		AddKey(m_pTx);

		if( Send() ) {

			SetAuthPoll(pDev);

			return TRUE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Tester
//

// Instantiator

INSTANTIATE(CML1SerialTester);

// Constructor

CML1SerialTester::CML1SerialTester(void)
{
	m_Ident   = DRIVER_ID;	

	m_fTrack  = TRUE;

	m_pDev	  = NULL;
		
	m_fClient = FALSE;
	}

// Destructor

CML1SerialTester::~CML1SerialTester(void)
{
	}

// Configuration

void MCALL CML1SerialTester::Load(LPCBYTE pData)
{
	CML1Tester::Load(pData);

	m_fTrack = GetByte(pData);
	}

void MCALL CML1SerialTester::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	Config.m_uFlags |= flagNoCheckError;

	m_Baud = Config.m_uBaudRate;
	}

// Management

void MCALL CML1SerialTester::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);

	CML1Base::Attach(pPort);
	}

void MCALL CML1SerialTester::Open(void)
{

	}

// Device

CCODE MCALL CML1SerialTester::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pDev = (CML1Dev *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pDev  = new CML1Dev;

			m_pDev->m_PingReg = GetWord(pData);

			m_pDev->m_TO	  = GetWord(pData);

			m_pDev->m_Retry   = GetByte(pData);

			m_pDev->m_Blocks  = GetWord(pData);

			InitDev();

			// Test Target (Client) MAC ID
			m_pDev->m_Mac.m_Addr[1] = 0x05;
			m_pDev->m_Mac.m_Addr[2] = 0xE4;
			m_pDev->m_Mac.m_Addr[3] = 0x01;
			m_pDev->m_Mac.m_Addr[4] = 0x10;
			m_pDev->m_Mac.m_Addr[5] = 0x10;

			m_pDev->m_Unit = 0x123456;

			m_pDev->m_Server = 1;

			// At present, ML1 is a point to point protocol with the Server ID residing on the 
			// device level, likewise, the Unit ID resides on the driver level.  This 
			// implementation (Server ID and UINT ID residing on both levels) is maintained
			// in event that ML1 evolves to a multi drop protocol!

			// Set driver level for current point to point communications!

			m_Unit   = m_pDev->m_Unit;

			m_Server = m_pDev->m_Server;

			//////////////////////////////////////////////////////////////

			LoadBlocks(m_pExtra, pData);

			InitSpecial();

			AfxListAppend(m_pHead, m_pTail, m_pDev, m_pNext, m_pPrev);

			pDevice->SetContext(m_pDev);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CML1SerialTester::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		RemoveBlocks();

		AfxListRemove(m_pHead, m_pTail, m_pDev, m_pNext, m_pPrev);
		
		m_pDev = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Transport

BOOL CML1SerialTester::Send(void)
{
	if( !IsAuthentication(m_pDev, m_pTx) && !IsAuthenticationReq(m_pTx) ) {

		m_pTx[5] = BYTE(m_uTxPtr - 6);
		}

	m_CRC.Preset();

	for( UINT i = 0; i < m_uTxPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));
		
	m_pData->Write(m_pTx, m_uTxPtr, FOREVER);

	if( ML1_SER_TEST ) {

		AfxTrace("\nTx %u ptr %u: ", GetTickCount(), m_uTxPtr);

		for( UINT u = 0; u < m_uTxPtr; u++ ) {

			AfxTrace("%2.2x ", m_pTx[u]);
			}
		}

	return TRUE;
	}

BOOL CML1SerialTester::Recv(void)
{
	UINT uByte = 0;

	UINT uSize = NOTHING;

	UINT uPtr  = 0;

	UINT uGap  = 0;

	UINT uEnd  = ToTicks(25);

	if( ML1_SER_TEST ) {

		AfxTrace("\nTest Rx : ");
		}

	SetTimer(600);

	while( GetTimer() ) {

		if( (uByte = m_pData->Read(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

			if( ML1_SER_TEST ) {

				AfxTrace("%2.2x ", m_pRx[uPtr - 1]);
				}

			if( uPtr == elements(m_pRx) ) {

				return FALSE;
				}

			if( uPtr == 8 ) {

				uSize = FindRxSize();
				}

			if( m_fTrack ) {

				SetTimer(100);
				}

			uGap = 0;
			}
		else
			uGap = uGap + 1;

		if( uPtr >= uSize || uGap >= uEnd ) {

			if( uPtr >= 9 ) {

				m_CRC.Preset();
				
				PBYTE p = m_pRx;
				
				UINT  n = uPtr - 2;
			
				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);
				
				WORD c2 = m_CRC.GetValue();

				if( c1 == c2 ) {

					memcpy(m_pBuff, m_pRx, uPtr);

					return HandleFrame(m_pBuff);
					}
				}

			uSize = NOTHING;
				
			uPtr  = 0;
			
			uGap  = 0;
			}
		}

	return FALSE;
	}

BOOL CML1SerialTester::Respond(PBYTE pBuff)
{
	return Send();
	}

BOOL CML1SerialTester::Respond(CML1Dev * pDev)
{
	return Send();
	}

// Transport Helpers

UINT CML1SerialTester::FindRxSize(void)
{
	if( m_fTrack ) {

		if( IsAuthentication(m_pDev, m_pRx) || IsAuthenticationReq(m_pRx) ) {

			switch( m_pRx[0] ) {

				case authPush:	return 20 + GetSpecialBytes(GetSpecialCount(m_pRx));

				case authAck:	return 20 + GetSpecialBytes(GetSpecialCount(m_pRx));

				case authPoll:	return  9;

				case authResp:	return 20 + GetSpecialBytes(GetSpecialCount(m_pRx));
				}
			}

		return m_pRx[5] + 6 + 2;
		}	
			
	return NOTHING;
	}

// End of File
