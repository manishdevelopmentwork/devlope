
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDeviceList_HPP

#define	INCLUDE_UsbDeviceList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUsbDevice;

//////////////////////////////////////////////////////////////////////////
//
// Usb Device List
//

class CUsbDeviceList
{
	public:
		// Constructor
		CUsbDeviceList(void);

		// Attributes
		CUsbDevice * GetHead(void) const;
		CUsbDevice * GetTail(void) const;
		CUsbDevice * GetPrev(CUsbDevice *pDevice) const;
		CUsbDevice * GetNext(CUsbDevice *pDevice) const;
		
		// Operations
		void Append(CUsbDevice *pDevice);
		void Remove(CUsbDevice *pDevice);
		void Insert(CUsbDevice *pDevice, CUsbDevice *pBefore); 

	protected:
		// Data
		CUsbDevice * m_pHead;
		CUsbDevice * m_pTail;
	};

// End of File

#endif
