
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RLOS Linker Hack
//

#if defined(AEON_PLAT_RLOS)

#if !defined(__INTELLISENSE__)

static DWORD zss __attribute__((section(".zss"))) = 0;

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// Platform Hook
//

extern IDevice * Create_Platform(void);

//////////////////////////////////////////////////////////////////////////
//
// Executive Hooks
//

extern void Open_Executive(void);
extern void Close_Executive(void);

//////////////////////////////////////////////////////////////////////////
//
// Stub Binding
//

extern void Bind_ExceptionIndex(void);
extern void Free_ExceptionIndex(void);

//////////////////////////////////////////////////////////////////////////
//
// External APIs
//

extern BOOL InitConsole(void);
extern BOOL ExecConsole(HTHREAD hThread);
extern void TermConsole(void);
extern BOOL LoadModules(void);
extern void FreeModules(void);
extern BOOL ServiceHook(int &nCode);

//////////////////////////////////////////////////////////////////////////
//
// Prototypes
//

static void SerializeDiagManager(BOOL fLock);

//////////////////////////////////////////////////////////////////////////
//
// Main Process
//

global int AeonHostMain(IThread *pThread, void *pParam, UINT uParam)
{
	SetThreadName("Main");

	Bind_ExceptionIndex();

	Open_Executive();

	int nCode = -1;

	piob->SerializeAccess(TRUE);

	SerializeDiagManager(TRUE);

	IDevice *pPlatform = Create_Platform();

	if( pPlatform->Open() ) {

		BOOL fTest = uParam;

		if( fTest || InitConsole() ) {

			if( LoadModules() ) {

				if( !ServiceHook(nCode) ) {

					IClientProcess *pClient  = NULL;

					if( AfxNewObject("app", IClientProcess, pClient) == S_OK ) {

						#if defined(_TRACKING)

						AfxMarkMemory();

						#endif

						HTHREAD hThread = CreateClientThread(pClient, 0, 97000);

						if( fTest ) {

							if( WaitThread(hThread, 30000) ) {

								nCode = hThread->GetExitCode();

								CloseThread(hThread);
							}
							else {
								if( DestroyThread(hThread) ) {

									pClient->TaskTerm(0);
								}
							}
						}
						else {
							if( !ExecConsole(hThread) ) {

								TermConsole();

								DestroyThread(hThread);

								nCode = 0;
							}
							else {
								TermConsole();

								WaitThread(hThread, 5000);

								nCode = hThread->GetExitCode();

								CloseThread(hThread);
							}
						}

						pClient->Release();
					}
				}
			}

			TermConsole();

			FreeModules();
		}
	}

	pPlatform->Release();

	SerializeDiagManager(FALSE);

	piob->SerializeAccess(FALSE);

	Close_Executive();

	Free_ExceptionIndex();

	return nCode;
}

static void SerializeDiagManager(BOOL fLock)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("aeon.diagmanager", 0, IDiagManager, pDiag);

	pDiag->SerializeAccess(fLock);

	pDiag->Release();
}

// End of File
