
#include "intern.hpp"

#include "touch.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Calibration
//

// Constructor

CPrimTouchCalib::CPrimTouchCalib(void)
{
	m_pSuccess = NULL;

	m_pFailure = NULL;

	m_Factory  = 0;

	m_Clear    = TRUE;
	}

// Destructor

CPrimTouchCalib::~CPrimTouchCalib(void)
{
	delete m_pSuccess;

	delete m_pFailure;
	}

// Initialization

void CPrimTouchCalib::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimTouchCalib", pData);

	CPrim::Load(pData);
	
	GetCoded(pData, m_pSuccess);

	GetCoded(pData, m_pFailure);

	m_nCount  = 9;

	m_nSelect = 0;

	m_nLast   = 0;

	m_pText   = "TOUCH EACH RED SQUARE IN TURN TO CALIBRATE";

	m_uEvent  = 0;

	m_uTimer  = 30;
	}

// Overridables

void CPrimTouchCalib::SetScan(UINT Code)
{
	SetItemScan(m_pSuccess, Code);

	SetItemScan(m_pFailure, Code);

	CPrim::SetScan(Code);
	}

void CPrimTouchCalib::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_nLast != m_nSelect ) {

			m_nLast   = m_nSelect;

			m_fChange = TRUE;
			}		
		}	
	}

void CPrimTouchCalib::DrawPrim(IGDI *pGDI)
{
	pGDI->SetBrushFore(GetRGB(16,16,16));

	pGDI->SetPenFore(GetRGB(31,31,31));

	pGDI->FillRect(PassRect(m_DrawRect));

	pGDI->DrawRect(PassRect(m_DrawRect));

	int ns;

	if( DispGetCx() <= 320 ) {
		
		ns = 15;
		}
	else
		ns = 20;

	for( int n = 0; n < m_nCount; n++ ) {

		int x = GetXPos(n);

		int y = GetYPos(n);

		pGDI->DrawRect(x-ns, y-ns, x+ns, y+ns);

		if( n == m_nSelect ) {

			int np = ns + 2;

			pGDI->SetBrushFore(GetRGB(31,16,16));
	
			pGDI->FillRect(x-ns, y-ns, x+ns, y+ns);

			pGDI->DrawRect(x-np, y-np, x+np, y+np);

			pGDI->DrawRect(x-2,  y-2,  x+2,  y+2 );
			}
		else {
			pGDI->SetBrushFore(GetRGB(16,31,16));
	
			pGDI->FillRect(x-ns, y-ns, x+ns, y+ns);
			}
		}

	if( m_pText ) {

		if( DispGetCx() <= 320 ) {

			if( DispGetCx() <= 240 ) {
			
				SelectFont(pGDI, fontSwiss0512);
				}
			else 
				SelectFont(pGDI, fontSwiss0712);
			}
		else
			SelectFont(pGDI, fontHei16Bold);

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SetBrushFore(GetRGB(16,16,31));
	
		int cx = pGDI->GetTextWidth (m_pText);

		int cy = pGDI->GetTextHeight(m_pText);

		int xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 3;

		pGDI->FillRect(xp - 20, yp - 10, xp + cx + 20, yp + cy + 10);

		pGDI->TextOut (xp, yp, m_pText);
		}
	}

void CPrimTouchCalib::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

UINT CPrimTouchCalib::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgTakeFocus ) {

		return 1;
		}

	if( uMsg == msgOneSecond ) {

		if( m_uTimer && !--m_uTimer ) {

			if( m_uEvent == 0 ) {

				if( m_Clear && !m_Factory ) {

					m_pText = "CALIB TIMED OUT -- DEFAULTS RESTORED";

					TouchClearCalib();
					}
				else
					m_pText = "CALIBRATION TIMED OUT";

				m_nSelect = m_nCount;

				m_uTimer = 3;

				m_uEvent = 2;

				Beep(48, 200);

				return TRUE;
				}

			if( m_uEvent == 1 ) {

				if( m_pSuccess ) {

					m_pSuccess->Execute(NULL);
					}

				return TRUE;
				}

			if( m_uEvent == 2 ) {

				if( m_pFailure) {

					m_pFailure->Execute(NULL);
					}

				return TRUE;
				}
			}
		}

	if( uMsg == msgTouchDown || uMsg == msgTouchInit ) {

		if( m_nSelect < m_nCount ) {

			int xHit = LOWORD(uParam);
			
			int yHit = HIWORD(uParam);

			int xPos = GetXPos(m_nSelect);
			
			int yPos = GetYPos(m_nSelect);

			int xRaw = 0;

			int yRaw = 0;

			TouchGetRaw(xRaw, yRaw);

			if( xRaw && yRaw ) {
				
				m_xRaw[m_nSelect] = xRaw;
				
				m_yRaw[m_nSelect] = yRaw;

				if( ++m_nSelect == m_nCount ) {

					if( DoCalc() ) {

						m_pText  = "CALIBRATION WAS SUCCESSFUL";

						m_uEvent = 1;

						m_uTimer = 3;
						}
					else {
						m_pText = "CALIBRATION FAILED";

						m_uEvent = 2;

						m_uTimer = 3;

						Beep(48, 200);
						}
					}
				else {
					m_pText  = NULL;

					m_uTimer = 30;
					}
				}
			else {
				m_nSelect = m_nCount;

				m_pText   = "CALIBRATION NOT REQUIRED";

				m_uEvent  = 1;

				m_uTimer  = 3;
				}

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// Implementation

int CPrimTouchCalib::GetXPos(int nPos)
{
	int no;

	if( DispGetCx() <= 320 ) {
		
		no = 30;
		}
	else
		no = 50;			

	return m_DrawRect.x1 + no + MulDiv(m_DrawRect.x2 - m_DrawRect.x1 - (no*2), nPos % 3, 2);
	}

int CPrimTouchCalib::GetYPos(int nPos)
{
	int no;

	if( DispGetCx() <= 320 ) {
		
		no = 30;
		}
	else
		no = 50;			

	return m_DrawRect.y1 + no + MulDiv(m_DrawRect.y2 - m_DrawRect.y1 - (no*2), nPos / 3, 2);
	}

BOOL CPrimTouchCalib::DoCalc(void)
{
	long swx = 0, swy = 0;

	long sxo = 0, syo = 0;
	
	int  nwx = 0, nwy = 0;

	int  cx  = 0, cy  = 0;

	GetTouchSize(cx, cy);

	BOOL fNegX = FALSE;
	
	BOOL fNegY = FALSE;

	for( int a = 0; a < m_nCount; a++ ) {
	
		for( int b = 0; b < m_nCount; b++ ) {
		
			int dxg = GetXPos(a) - GetXPos(b);

			if( dxg ) {
			
				int   dxt = m_xRaw[a] - m_xRaw[b];
				
				BOOL fNeg = dxg > 0 ? dxt < 0 : dxt > 0;

				if( !nwx ) {

					fNegX = fNeg;
					}
				else {
					if( fNegX != fNeg ) {

						return FALSE;
						}
					}
					
				int wx  = MulDivRound(cx, dxt, dxg);
				
				swx += wx;
				
				nwx += 1;
				}

			int dyg = GetYPos(a) - GetYPos(b);
			
			if( dyg ) {
			
				int  dyt  = m_yRaw[a] - m_yRaw[b];
				
				BOOL fNeg = dyg > 0 ? dyt < 0 : dyt > 0;

				if( !nwy ) {

					fNegY = fNeg;
					}
				else {
					if( fNegY != fNeg ) {

						return FALSE;
						}
					}

				int wy  = MulDivRound(cy, dyt, dyg);
				
				swy += wy;
				
				nwy += 1;
				}
			}
		}
		
	int wx = (swx + nwx / 2) / nwx;

	int wy = (swy + nwy / 2) / nwy;

	int c;
	
	for( c = 0; c < m_nCount; c++ ) {
	
		int xg = GetXPos(c);

		int xt = m_xRaw[c];
		
		int xo = xt - MulDivRound(xg, wx, cx);

		int yg = GetYPos(c);

		int yt = m_yRaw[c];
		
		int yo = yt - MulDivRound(yg, wy, cy);
		
		sxo += xo;

		syo += yo;
		}
		
	int xo = (sxo + c / 2) / c;

	int yo = (syo + c / 2) / c;

	if( cx < cy ) {

		yo += wy;

		wy =- wy;

		Swap(xo, yo);

		Swap(wx, wy);
		}

	AfxTrace("Calib is (%d, %d) to (%d, %d)\n",
		 xo + wx,
		 yo + wy,
		 xo, yo
		 );

	return TouchSetCalibEx(m_Factory, xo + wx, yo + wy, xo, yo);
	}

void CPrimTouchCalib::Swap(int &a, int &b)
{
	int c;

	c = a;
	a = b;
	b = c;
	}

int CPrimTouchCalib::MulDivRound(int a, int b, int c)
{
	return (a * b + c / 2) / c;
	}

int CPrimTouchCalib::MulDiv(int a, int b, int c)
{
	return (a * b) / c;
	}

void CPrimTouchCalib::GetTouchSize(int &cx, int &cy)
{
	#if defined(USING_STDENV)

	TouchDispSize(cx, cy);

	#else

	cx = DispGetCx();
	
	cy = DispGetCy();

	#endif
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Tester
//

// Constructor

CPrimTouchTester::CPrimTouchTester(void)
{
	m_pBack = New CPrimColor(naBack);

	m_uHead = 0;

	m_uTail = 0;

	m_uLast = 0;
	}

// Destructor

CPrimTouchTester::~CPrimTouchTester(void)
{
	delete m_pBack;
	}

// Initialization

void CPrimTouchTester::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimTouchTester", pData);

	CPrim::Load(pData);

	m_pBack->Load(pData);
	}

// Overridables

void CPrimTouchTester::SetScan(UINT Code)
{
	m_pBack->SetScan(Code);

	CPrim::SetScan(Code);
	}

// Overridables

void CPrimTouchTester::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_uLast != m_uTail ) {

			m_uLast   = m_uTail;

			m_fChange = TRUE;
			}		
		}
	}

void CPrimTouchTester::DrawPrim(IGDI *pGDI)
{
	pGDI->ResetAll();

	pGDI->SetBrushFore(m_pBack->GetColor());

	pGDI->SetPenFore(GetRGB(31,31,31));

	pGDI->FillRect(PassRect(m_DrawRect));

	pGDI->DrawRect(PassRect(m_DrawRect));

	pGDI->SetBrushFore(GetRGB(31,0,0));

	for( UINT n = m_uHead; n != m_uTail; ) {

		int x = m_xPos[n];

		int y = m_yPos[n];

		MakeMax(x, 4);

		MakeMax(y, 4);

		pGDI->FillRect(x-4, y-4, x+4, y+4);

		pGDI->DrawRect(x-4, y-4, x+4, y+4);

		n = (n + 1) % elements(m_xPos);
		}
	}

void CPrimTouchTester::LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch)
{
	pTouch->FillRect(PassRect(m_DrawRect));
	}

UINT CPrimTouchTester::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgTakeFocus ) {

		return 1;
		}

	if( uMsg == msgTouchDown || uMsg == msgTouchInit ) {

		m_xPos[m_uTail] = LOWORD(uParam);
		
		m_yPos[m_uTail] = HIWORD(uParam);

		m_uTail = (m_uTail + 1) % elements(m_xPos);

		if( m_uTail == m_uHead ) {

			m_uHead = (m_uHead + 1) % elements(m_xPos);
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
