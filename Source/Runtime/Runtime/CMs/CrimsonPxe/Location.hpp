
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Location_HPP

#define	INCLUDE_Location_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Location Service
//

class CLocation : public ILocationSource
{
public:
	// Constructor
	CLocation(CJsonConfig *pJson);

	// Destructor
	~CLocation(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILocationSource
	BOOL METHOD GetLocationData(CLocationSourceInfo &Info);
	BOOL METHOD GetLocationTime(CTimeSourceInfo &Info);

protected:
	// Data Members
	ULONG		    m_uRefs;
	UINT		    m_uSource;
	IMutex		  * m_pLock;
	CLocationSourceInfo m_Info;
	double		    m_Delta;

	// Implementation
	void   ApplyConfig(CJsonConfig *pJson);
	double GetDistance(CLocationSourceInfo const &a, CLocationSourceInfo const &b);
	double ToRad(double deg);
};

// End of File

#endif
