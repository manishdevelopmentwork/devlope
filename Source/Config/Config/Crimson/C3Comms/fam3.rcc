
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "fam3.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "fam3.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "fam3.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "fam3.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CYokoFam3DeviceOptions
//

CYokoFam3DeviceOptionsUIList RCDATA
BEGIN
	"Station,Station Number,,CUIEditInteger,|0||1|32,"
	"\0"

	"Cpu,CPU Number,,CUIEditInteger,|0||1|4,"
	"\0"

	"Wait,Response Wait Time (ms),,CUIDropDown,0|10|20|30|40|50|60|70|80|90|100|200|300|400|500|600,"
	"\0"

	"Check,Enable Checksum,,CUIDropDown,No|Yes,"
	"\0"

	"Term,Enable Termination Character,,CUIDropDown,No|Yes,"
	"\0"

	"\0"
END

CYokoFam3DeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Station,Cpu\0"
	"G:1,root,Device Options,Wait,Check,Term\0"
	"\0"
END


//////////////////////////////////////////////////////////////////////////
//
// UI for CYokoFam3TCPDeviceOptions
//

CYokoFam3TCPDeviceOptionsUIList RCDATA
BEGIN
	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the Yokogawa device."
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|60000,"
	"Indicate the TCP port number on which this Yokogawa device is to operate. "
	"\0"

	"Cpu,CPU Number,,CUIEditInteger,|0||1|4,"
	"\0"

	"Mode,Mode,,CUIDropDown,Binary|ASCII,"
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the Yokogawa driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the Yokogawa server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a Yokogawa request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CYokoFam3TCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Port,Cpu\0"
	"G:1,root,Protocol Options,Mode,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END




// End of File
