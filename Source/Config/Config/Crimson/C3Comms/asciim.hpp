
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ASCIIM_HPP

#define	INCLUDE_ASCIIM_HPP

#define	BB	addrByteAsByte
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	SPTXT	1
#define	SPTXR	2
#define	SPTXS	3
#define	SPWTX	4
#define	SPWTXR	5
#define	SPWTXS	6
#define	SPWCMD	7
#define	SPWRSP	8

#define	SZMAX	127

#define	ISSINO	0
#define	ISSIR	1 // Real Selection SubItem
#define	ISSIS	2 // String Selection SubItem
	
//////////////////////////////////////////////////////////////////////////
//
// ASCII Master Device Options
//

class CASCIIMDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CASCIIMDeviceOptions(void);

		// UI Management
		void	OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL	MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString	m_RFrame;
		CString	m_WFrame;

		UINT	m_RspTerm;
		UINT	m_RspLen;
		UINT	m_RspTime;
		CString	m_NakInd;

		UINT	m_Radix;

		BOOL	m_fRspEcho;
		UINT	m_EchoCmd;
		UINT	m_EchoRsp;

		UINT	m_RspStart;
		UINT	m_RspSkip;
		UINT	m_fRspBack;
		BOOL	m_fRspWrite;

	protected:
		// Meta Data Creation
		void	AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ASCII Master Driver
//

class CASCIIMDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CASCIIMDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void	AddSpaces(void);

		// Friend
		friend class CASCIIMAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// ASCII Master Address Selection
//

class CASCIIMAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CASCIIMAddrDialog(CASCIIMDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:

		// Data Members

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Overridables
		void ShowAddress(CAddress Addr);

		// Helpers
		BOOL GetsMaxDP(UINT uTable);
	};

// End of File

#endif
