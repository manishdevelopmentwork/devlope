
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_L5KITEM_HPP
	
#define	INCLUDE_L5KITEM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "l5kfile.hpp"

//////////////////////////////////////////////////////////////////////////
//
// External References
//

class CABL5kDeviceOptions;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kList;
class CABL5kItem;
class CABL5kAtomicItem;
class CABL5kStructItem;

//////////////////////////////////////////////////////////////////////////
//
// Address Decoding Macros
//

#define IsTable(Addr)	((Addr).a.m_Table  < addrNamed)

#define IsNamed(Addr)	((Addr).a.m_Table == addrNamed)

#define GetTable(Addr)	((Addr).a.m_Table)

#define GetOffset(Addr)	((Addr).a.m_Offset)

#define GetIndex(Addr)	(IsTable(Addr) ? GetTable(Addr) : GetOffset(Addr))

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Item List
//

class CABL5kList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kList(void);

		// Destructor
		~CABL5kList(void);

		// Item Access
		CABL5kItem * GetItem(INDEX Index) const;
		CString      GetName(INDEX Index) const;
		CString      GetType(INDEX Index) const;
		CABL5kItem * GetItem(UINT uPos)   const;

		// Item Location
		CABL5kItem * FindItem(CString const &Name) const;

		// Overridables
		virtual BOOL LoadConfig(CError &Error, CABL5kController &Config);

		// Debug
		void ShowList(CABL5kList *pList);

	protected:
		// Data
		UINT	m_uDepth;

		// Implementation
		void ThrowError(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Item
//

class CABL5kItem : public CNamedItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kItem(void);

		// Attributes
		BOOL    IsAtomic(void);
		BOOL    IsStruct(void);
		BOOL    IsArray(void);
		CString GetName(void);
		CString GetType(void);

		// Overridables
		virtual CABL5kList * GetElements(void);

	protected:
		// Item Properties
		CString	m_Type;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Item
//

class CABL5kAtomicItem : public CABL5kItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kAtomicItem(void);

		// Persistance
		void Load(CTreeFile &File);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Structure Item
//

class CABL5kStructItem : public CABL5kItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kStructItem(void);

		// Overridables
		CABL5kList * GetElements(void);

		// Operations
		CABL5kItem * AddMember(CABL5kItem *pItem);

		// Public Data
		CABL5kList    * m_pElements;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kTypeList;
class CABL5kAtomicType;
class CABL5kStructType;

//////////////////////////////////////////////////////////////////////////
//
// Legacy Support
//

enum {
	addrString	= 1,
	addrTimer	= 2,
	addrCounter	= 3,
	addrControl	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Data Type List
//

class CABL5kTypeList : public CABL5kList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kTypeList(void);

		// Look ups
		BOOL    IsAtomicType (CString const &Type);
		BOOL    IsPredefinedType(CString const &Type);
		BOOL    IsUserType(CString Name);
		CString GetAtomicName(CAddress const &Addr);

		// Parsing
		BOOL  IsArrayType  (CString const &Type);
		BOOL  IsAtomicArray(CString const &Type);
		DWORD GetAtomicType(CString Type);

		// Persistance
		void Save(CTreeFile &File);

		// Debug
		void ShowList(void);

		// Overridables
		BOOL LoadConfig(CError &Error, CABL5kController &Config);

	protected:
		// Raw Atomic Data Type
		struct CAtomicEntry {
			
			PCTXT	m_pName;
			DWORD	m_Type;
			};

		// Raw Structured Info
		struct CStructEntry {
			
			PCTXT	m_pName;
			PCTXT	m_pType;
			};

		// Raw Predefined Data Type

		struct CPredefinedEntry {
			
			PCTXT		m_pName;
			CStructEntry	m_Elements[244];
			};

		// Raw Atomic Types
		static CAtomicEntry	m_AtomicList[];

		// Raw Predefined Types
		static CPredefinedEntry	m_PredefinedList[];

		// Atomic Type Maps
		CMap <DWORD, CString>	m_AtomicFwd;
		CMap <CString, DWORD>	m_AtomicRev;

	protected:
		// Implementation
		void ScanAtomic(void);
		void ScanPredefined(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Data Type
//

class CABL5kAtomicType : public CABL5kAtomicItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kAtomicType(void);
		CABL5kAtomicType(CString Name, CString Type);
		CABL5kAtomicType(CString Name, CString Type, UINT uType);
		CABL5kAtomicType(CString Name/*, UINT uType*/);

		// Data
		UINT	m_uType;
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Data Type
//

class CABL5kStructType : public CABL5kStructItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kStructType(void);
		CABL5kStructType(CString Name);
		CABL5kStructType(CString Name, CString Type);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kTagList;
class CABL5kAtomicTag;
class CABL5kStructTag;

//////////////////////////////////////////////////////////////////////////
//
// Name Address
//

struct CNameIndex {
	
	CString		m_Name;
	CString		m_Type;
	UINT		m_uIndex;
	CAddress	m_Addr;
	};

//////////////////////////////////////////////////////////////////////////
//
// Utility Templates
//

inline int AfxCompare(CNameIndex const &d1, CNameIndex const &d2)
{
	if( GetIndex(d1.m_Addr) > GetIndex(d2.m_Addr) )	return +1;

	if( GetIndex(d1.m_Addr) < GetIndex(d2.m_Addr) )	return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Tag List
//

class CABL5kTagList : public CABL5kList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kTagList(void);
		
		// Destructor
		~CABL5kTagList(void);

		// Operations
		BOOL CreateTag(CError &Error, CString const &Name, CString const &Type, CString Info = "");

		// Dimension Expansion Helpers
		void DimGetIndices(CArray <UINT> const &Dims, CArray <UINT> &List, UINT uIndex);
		UINT DimGetIndex  (CArray <UINT> const &Dims, CArray <UINT> const &List);
		UINT DimGetSize   (CArray <UINT> const &Dims);
		void DimParse     (CArray <UINT> &List, CString const &Type);
		BOOL DimExpand    (CArray <UINT> const &List, UINT uIndex, CString &Text);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Debug
		void ShowList(void);

		// Overridables
		BOOL LoadConfig(CError &Error, CABL5kController &Config);

	protected:
		// Static Data

		// Data
		CABL5kDeviceOptions	* m_pConfig;
		CABL5kTypeList		* m_pTypes;

		// Implementation
		void ResolveAliasTags(CError &Error, CMap <CString, CString> &AliasList);
		void FindManager(void);

		BOOL ExpandType (CError &Error, CString Name, CString Type, CString Info);
		BOOL ExpandType (CError &Error, CABL5kStructTag *pItem);
		BOOL ExpandArray(CError &Error, CString Name, CString Type);
		BOOL ExpandArray(CError &Error, CABL5kStructTag *pItem);

		void CABL5kTagList::ShowStatus(CString Text);

		// debug
		void ShowList(CArray <CNameIndex> const &List);

		// Friends
		friend class CABL5kDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Data Type
//

class CABL5kAtomicTag : public CABL5kAtomicItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kAtomicTag(void);
		CABL5kAtomicTag(CString const &Name, CString const &Type, CString Info);
		
		// Operations
		CString GetFullName(void);

		// Data Members
		CString m_Info;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Data Type
//

class CABL5kStructTag : public CABL5kStructItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CABL5kStructTag(void);
		CABL5kStructTag(CString Name, CString Type);
		CABL5kStructTag(CString Name, CString Type, CString Info);

		// Data Members
		CString m_Info;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
