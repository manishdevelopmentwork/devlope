
#include "intern.hpp"

#include "honeyudc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CHoneywellUDC9000DeviceOptions, CUIItem);       

// Constructor

CHoneywellUDC9000DeviceOptions::CHoneywellUDC9000DeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 6000;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// Download Support

BOOL CHoneywellUDC9000DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));


	return TRUE;
	}

// Meta Data Creation

void CHoneywellUDC9000DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);

       	}

//////////////////////////////////////////////////////////////////////////
//
// Honeywell UDC9000 Master Driver
//

// Instantiator

ICommsDriver *Create_HoneywellUDC9000Driver(void)
{
	return New CHoneywellUDC9000Driver;
	}

// Constructor

CHoneywellUDC9000Driver::CHoneywellUDC9000Driver(void)
{
	m_wID		= 0x351D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Honeywell";
	
	m_DriverName	= "S9000";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Honeywell S9000";

	AddSpaces();
	}

// Destructor

CHoneywellUDC9000Driver::~CHoneywellUDC9000Driver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CHoneywellUDC9000Driver::GetBinding(void)
{
	return bindEthernet;
	}

void CHoneywellUDC9000Driver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CHoneywellUDC9000Driver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CHoneywellUDC9000Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CHoneywellUDC9000DeviceOptions);
	}

// Address Management

BOOL CHoneywellUDC9000Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CHoneywellUDC9000AddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CHoneywellUDC9000Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uTable  = pSpace->m_uTable;

	UINT uType   = pSpace->m_uType;

	BOOL fHasAddr = BOOL(pSpace->m_uMaximum);

	Text.MakeUpper();

	UINT uOffset = fHasAddr ? tatoi(Text) : 0;

	UINT uFind   = Text.FindRev('.');

	if( uFind < NOTHING ) {

		switch( Text[uFind+1] ) {

			case 'R':	uType = addrRealAsReal; break;
			case 'B':	uType = addrBitAsBit;	break;
			case 'W':	uType = addrWordAsWord;	break;
			case 'L':	uType = addrLongAsLong; break;
			default:	break;
			}
		}

	if( uType < pSpace->m_uType || (uTable >= SPIW && uTable <= SPS) ) uType = pSpace->m_uType;

	if( IsControlBlock(uTable) ) {

		uFind = Text.FindRev('(');

		UINT uPar  = 1;
		UINT uItem = 0;

		if( uFind < NOTHING ) {

			uPar  = uOffset;

			uItem = tatoi(Text.Mid(uFind+1, uFind + 1) );
			}

		uOffset = (uPar * 100) + uItem;
		}

	if( (uOffset >= pSpace->m_uMinimum && uOffset <= pSpace->m_uMaximum) || !fHasAddr ) {

		Addr.a.m_Table  = uTable;

		Addr.a.m_Type   = uType;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = uOffset;

		return TRUE;
		}

	CString sErr;

	if( IsControlBlock(uTable) ) {

		sErr.Printf( "%s001(00) - %s255(99)",

			pSpace->m_Prefix,
			pSpace->m_Prefix
			);
		}

	else {
		sErr.Printf( "%s%d - %s%d",

			pSpace->m_Prefix,
			pSpace->m_uMinimum,
			pSpace->m_Prefix,
			pSpace->m_uMaximum
			);
		}

	Error.Set( sErr, 0 );

	return FALSE;
	}

BOOL CHoneywellUDC9000Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uOffset = Addr.a.m_Offset;

		UINT uType   = Addr.a.m_Type;

		CString sTyp = uType != addrBitAsBit ? "." + pSpace->GetTypeModifier(uType) : ".BIT";
		CString sAdd = "";

		if( IsControlBlock(Addr.a.m_Table) ) {

			sAdd.Printf( "%3.3d(%2.2d)",
			     uOffset/100,
			     uOffset%100
			     );
			}

		else {
			if( pSpace->m_uMaximum ) sAdd.Printf( "%5.5d", uOffset );
			}

		Text.Printf( "%s%s%s",

			pSpace->m_Prefix,
			sAdd,
			sTyp
			);
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CHoneywellUDC9000Driver::AddSpaces(void)
{
	AddSpace(New CSpace(SPR, "R",	"Registers",		10, 0, 65535,	addrWordAsWord, addrRealAsReal));
	AddSpace(New CSpace(SPIW,"IW",	"I/O Status as Words",	10, 0, 65535,	addrWordAsWord));
	AddSpace(New CSpace(SPIB,"IB",	"I/O Status as Bits",	10, 0, 65535,	addrBitAsBit));
	AddSpace(New CSpace(SPM, "M",	"Mode Select",		10, 0, 0,	addrWordAsWord));
	AddSpace(New CSpace(SPS, "S",	"Status Request",	10, 0, 0,	addrWordAsWord));
	AddSpace(New CSpace(SPT, "ST",	"Device/Comms Status",	10, 0, 0,	addrWordAsWord));
	AddSpace(New CSpace(SPB, "CB",	"Control Block I/O",	10, 100, 25599,	addrLongAsLong, addrRealAsReal));
	AddSpace(New CSpace(SPC, "CC",	"Control Block Configuration",	10, 100, 25599, addrLongAsLong, addrRealAsReal));
	}

BOOL CHoneywellUDC9000Driver::IsControlBlock(UINT uTable)
{
	return uTable == SPB || uTable == SPC;
	}

//////////////////////////////////////////////////////////////////////////
//
// HoneywellUDC9000 Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CHoneywellUDC9000AddrDialog, CStdAddrDialog);
		
// Constructor

CHoneywellUDC9000AddrDialog::CHoneywellUDC9000AddrDialog(CHoneywellUDC9000Driver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "HoneywellUDCElementDlg";
	}

// Overridables

BOOL CHoneywellUDC9000AddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrWordAsWord:
		case addrRealAsReal: return TRUE;

		case addrLongAsLong:

			if( m_pSpace ) {

				return m_pSpace->m_uTable == SPC || m_pSpace->m_uTable == SPB;
				}

			return TRUE;
		}

	return FALSE;
	}

void CHoneywellUDC9000AddrDialog::SetAddressText(CString Text)
{
	BOOL fEnable = !Text.IsEmpty();

	if( m_pSpace ) {

		UINT uTable  = m_pSpace->m_uTable;

		BOOL fBlock  = IsControlBlock(uTable);

		BOOL fStatus = uTable == SPM || uTable == SPS || uTable == SPT;

		if( fEnable ) {

			if( fBlock ) {

				UINT uFind = Text.Find('(');

				if( uFind == NOTHING ) {

					Text.Printf( "%s(0)", Text );
					uFind = Text.Find('(');
					}

				UINT uLast = Text.Find(')', uFind);

				if( uLast == NOTHING ) uLast = Text.GetLength();

				GetDlgItem(2002).SetWindowText(Text.Left(uFind));

				GetDlgItem(2004).SetWindowText("(");

				GetDlgItem(2005).SetWindowText(Text.Mid(uFind+1, uLast-uFind-1));

				GetDlgItem(2006).SetWindowText(")");
				}
			else {
				GetDlgItem(2002).SetWindowText(fStatus ? "" : Text);

				GetDlgItem(2004).SetWindowText("");

				GetDlgItem(2005).SetWindowText("");

				GetDlgItem(2006).SetWindowText("");
				}
			}

		else {
			if( fStatus ) {

				GetDlgItem(2002).SetWindowText("");
				GetDlgItem(2004).SetWindowText("");
				GetDlgItem(2005).SetWindowText("");
				GetDlgItem(2006).SetWindowText("");
				}
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(fBlock);
		
		GetDlgItem(2005).EnableWindow(fBlock);
		
		GetDlgItem(2006).EnableWindow(fBlock);
		
		GetDlgItem(2007).EnableWindow(fBlock);
		
		GetDlgItem(2008).EnableWindow(fBlock);
		}
	else {
		if( fEnable ) {

			GetDlgItem(2002).SetWindowText(Text);

			GetDlgItem(2005).SetWindowText("");

			GetDlgItem(2004).SetWindowText("");

			GetDlgItem(2006).SetWindowText("");
			}

		GetDlgItem(2002).EnableWindow(fEnable);
		
		GetDlgItem(2004).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		
		GetDlgItem(2006).EnableWindow(FALSE);
		
		GetDlgItem(2007).EnableWindow(FALSE);
		
		GetDlgItem(2008).EnableWindow(FALSE);
		}
	}

CString CHoneywellUDC9000AddrDialog::GetAddressText(void)
{
	CString Text = "";

	if( m_pSpace ) {

		if( m_pSpace->m_uMaximum ) {

			Text = GetDlgItem(2002).GetWindowText();

			if( IsControlBlock(m_pSpace->m_uTable) ) {

				Text += "(";
		
				Text += GetDlgItem(2005).GetWindowText();

				Text += ")";
				}
			}
		}

	return Text;
	}

// Helpers

BOOL CHoneywellUDC9000AddrDialog::IsControlBlock(UINT uTable)
{
	return uTable == SPB || uTable == SPC;
	}

// End of File
