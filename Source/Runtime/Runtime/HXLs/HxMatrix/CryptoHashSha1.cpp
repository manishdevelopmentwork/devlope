
#include "Intern.hpp"

#include "CryptoHashSha1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA1 Cryptographic Hash
//

// Instantiator

static IUnknown * Create_CryptoHashSha1(PCTXT pName)
{
	return New CCryptoHashSha1;
	}

// Registration

global void Register_CryptoHashSha1(void)
{
	piob->RegisterInstantiator("crypto.hash-sha1", Create_CryptoHashSha1);
	}

// Constructor

CCryptoHashSha1::CCryptoHashSha1(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;

	psSha1PreInit(&m_Ctx);
	}

// ICryptoHash

CString CCryptoHashSha1::GetName(void)
{
	return "sha1";
	}

void CCryptoHashSha1::GetHashOid(CByteArray &oid)
{
	BYTE b[] = { 0x2B, 0x0E, 0x03, 0x02, 0x1A };

	oid.Empty();

	oid.Append(b, sizeof(b));
	}

void CCryptoHashSha1::Initialize(void)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psSha1Init(&m_Ctx);

	m_uState = stateActive;
	}

void CCryptoHashSha1::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psSha1Update(&m_Ctx, pData, uData);
	}

void CCryptoHashSha1::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psSha1Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
