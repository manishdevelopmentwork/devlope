#ifndef INCLUDE_UNIDRIVE_M_SERIAL
#define INCLUDE_UNIDRIVE_M_SERIAL

#include "intern.hpp"

#include "UnidriveMBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson - Control Techniques Unidrive M Serial Master
//

class CUnidriveMSerial : public CUnidriveMBase
{
	public:
		// Constructor
		CUnidriveMSerial(void);

		// Destructor
		~CUnidriveMSerial(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Entry Point
		DEFMETH(CCODE) Ping(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		// Device data
		struct CContext : CUnidriveMBase::UnidriveMBaseContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			BYTE	   m_bDrop;
			};

		// Data Members
		CContext	* m_pCtx;
		CContext	* m_pHead;
		CContext	* m_pTail;
		UINT		  m_uTimeout;
		CRC16		  m_CRC;

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);

		// Frame building
		void StartFrame(BYTE bOpcode);

		// Transport Layer
		BOOL Transact(BOOL fIgnore);
		BOOL PutFrame(void);
		BOOL GetFrame(BOOL fIgnore);
		BOOL CheckReply(BOOL fIgnore);
		BOOL BinaryRx(BOOL fWrite);
		BOOL BinaryTx(void);
		BOOL HasReadException(void);
	};

#endif

// End of File
