
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_GpbBase_HPP

#define INCLUDE_GpbBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Google Protocol Buffer Base Class
//

class DLLAPI CGpbBase
{
	protected:
		// Wire Types
		enum
		{
			wireVarint          = 0,
			wire64Bit           = 1,
			wireLengthDelimited = 2,
			wireStartGroup      = 3,
			wireEndGroup        = 4,
			wire32Bit           = 5
			};
	};

// End of File

#endif
