
#include "Intern.hpp"  

#include "UsbDevice.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbHubGetDescReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Device
//

// Instantiator 

global IUsbDevice * Create_UsbDevice(void)
{
	CUsbDevice *p = New CUsbDevice();

	return (IUsbDevice *) p;
	}

// Constructor

CUsbDevice::CUsbDevice(void)
{
	StdSetRef();

	m_pStack = NULL;

	m_pEvent = NULL;

	m_pNext	 = NULL;
	
	m_pPrev	 = NULL;
	
	m_pHostC = NULL;
	
	m_pHostI = NULL;
	
	m_uState = devInit;

	m_bAddr	 = 0;

	m_nLock  = 0;

	m_iDev   = NOTHING;

	m_pCtrl  = Create_UsbPipe();

	memset(&m_IntList, 0, sizeof(m_IntList));
	}

// Destructor

CUsbDevice::~CUsbDevice(void)
{
	m_pCtrl->Release();

	AfxRelease(m_pEvent);
	}

// IUnknown

HRESULT CUsbDevice::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbDevice);

	StdQueryInterface(IUsbDevice);

	StdQueryInterface(IUsbDeviceEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbDevice::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbDevice::Release(void)
{
	StdRelease();
	}

// IUsbDevice

UsbDeviceDesc const & CUsbDevice::GetDevDesc(void)
{
	return m_DevDesc;
	}

CUsbDescList const & CUsbDevice::GetCfgDesc(void)
{
	return m_CfgDesc;
	}

UsbHubDesc const & CUsbDevice::GetHubDesc(void)
{
	return m_HubDesc;
	}

UINT CUsbDevice::GetSpeed(void)
{
	return m_uSpeed;
	}

BYTE CUsbDevice::GetAddr(void)
{
	return m_bAddr;
	}

IUsbPipe * CUsbDevice::GetCtrlPipe(void)
{
	return m_pCtrl;
	}

IUsbPipe * CUsbDevice::GetPipe(BYTE bAddr, BOOL fDirIn)
{
	if( bAddr == 0 ) {

		return m_pCtrl;
		}
	
	for( UINT i = 0; i < elements(m_IntList); i ++ ) {

		CInterfaceCtx const &Int = m_IntList[i];

		if( Int.m_pDesc ) {

			for( UINT n = 0; n < elements(Int.m_PipeList); n ++ ) {
		
				CPipeCtx const &Pipe = Int.m_PipeList[n];

				if( Pipe.m_pDesc ) {

					if( Pipe.m_pDesc->m_bAddr == bAddr && Pipe.m_pDesc->m_bDirIn == fDirIn ) {

						return Pipe.m_pPipe;
						}
					
					continue;
					}
				
				break;
				}
			}
		}
	
	return NULL;
	}

void CUsbDevice::SetHostStack(IUsbHostStack *pStack)
{
	m_pStack = pStack;

	m_pEvent = NULL;

	m_pStack->QueryInterface(AfxAeonIID(IUsbHostControllerEvents), (void **) &m_pEvent);

	m_pCtrl->SetDevice((IUsbDevice *) this);
	}

IUsbHostStack * CUsbDevice::GetHostStack(void)
{
	return m_pStack;
	}

IUsbHostControllerDriver * CUsbDevice::GetHostC(void)
{
	return m_pHostC;
	}

IUsbHostInterfaceDriver * CUsbDevice::GetHostI(void)
{
	return m_pHostI;
	}

IUsbHubDriver * CUsbDevice::GetParentHub(void)
{
	if( m_Path.a.dwHubAddr && m_Path.a.dwHubPort ) {

		UsbDevPath DevPath = { { m_Path.a.dwHubAddr, m_Path.a.dwHost, m_Path.a.dwCtrl, 0, 0, 0 } };

		IUsbDevice *pDev = m_pStack->GetDevice(DevPath);

		if( pDev ) {
			
			IUsbHostFuncDriver *pDriver = pDev->GetDriver(0);

			if( pDriver && pDriver->GetClass() == devHub ) {

				return (IUsbHubDriver *) pDriver;
				}
			} 
		}

	return NULL;
	}

UINT CUsbDevice::GetDriverCount(void)
{
	return m_uDrvCount;
	}

IUsbHostFuncDriver * CUsbDevice::GetDriver(UINT i)
{
	if( i < elements(m_IntList) ) {
		
		return m_IntList[i].m_pDriver;
		}
	
	return NULL;
	}

UsbPortPath const & CUsbDevice::GetPortPath(void)
{
	return m_Path;
	}

UsbTreePath const & CUsbDevice::GetTreePath(void)
{
	return m_Tree;
	}

void CUsbDevice::SetObject(UINT i, PVOID p)
{
	if( i < elements(m_IntList) ) {
		
		m_IntList[i].m_pObject = p;
		}
	}

PVOID CUsbDevice::GetObject(UINT i)
{
	if( i < elements(m_IntList) ) {
		
		return m_IntList[i].m_pObject;
		}
	
	return NULL;
	}

BOOL CUsbDevice::SetRemoveLock(BOOL fLock)
{
	if( fLock ) {

		Hal_Critical(true);

		if( m_uState != devRemoved && m_uState != devDestroyed ) {

			AtomicIncrement(&m_nLock);

			Hal_Critical(false);
	
			return true; 
			}

		Hal_Critical(false);

		return false;
		}

	AtomicDecrement(&m_nLock);

	return true;
	}

BOOL CUsbDevice::HasError(void) const
{
	return m_uState == devError;
	}

void CUsbDevice::SetError(void)
{
	if( m_uState == devReady ) {

		m_uState = devError;
		}
	}

UINT CUsbDevice::GetState(void) const
{
	return m_uState;
	}

PCTXT CUsbDevice::GetStateText(void) const
{
	switch( m_uState ) {

		case devInit:	   return "INIT";
		case devAddress:   return "ADDR";
		case devDiscover:  return "ENUM";
		case devConfigure: return "CONFIG";
		case devReady:	   return "READY";
		case devRemoved:   return "REMOVED";
		case devDestroyed: return "DESTROYED";
		case devError:	   return "ERROR";
		}

	return "Unknown";
	}

BOOL CUsbDevice::IsHub(void) const
{
	return m_DevDesc.m_bClass == devHub;	
	}

BOOL CUsbDevice::IsReady(void) const
{
	return m_uState == devReady;
	}

BOOL CUsbDevice::IsRemoved(void) const
{
	switch( m_uState ) {
		
		case devRemoved:
		case devDestroyed:

			return true;
		}

	return false;
	}

// IUsbDeviceEvents

void CUsbDevice::OnArrival(void)
{
	m_uState = devInit;

	m_bAddr  = 255;
	}

void CUsbDevice::OnRemoval(void)
{
	AfxAssert(m_uState != devRemoved);

	m_uState = devRemoved;
	}

void CUsbDevice::OnPoll(UINT uLapsed)
{
	switch( m_uState ) {

		case devInit:

			if( OnInit() ) {
				
				m_uState = devAddress;
				
				break;
				}
			
			m_uState = devError;

			break;

		case devAddress:

			if( OnAddress() ) {
				
				m_uState = devDiscover;
				
				break;
				}
	
			m_uState = devError;
			
			break;

		case devDiscover:

			if( OnDiscover() ) {
				
				m_uState = devConfigure;
				
				break;
				}
	
			m_uState = devError;

			break;

		case devConfigure:

			if( OnConfigure() ) {
				
				m_uState = devReady;

				break;
				}
			
			m_uState = devError;

			break;

		case devRemoved:

			if( OnDestroy() ) {
				
				m_uState = devDestroyed;
				}
			
			break;

		case devReady:

			OnReady(uLapsed);

			break;
		}
	}

// Operations

void CUsbDevice::SetPath(UsbPortPath const &Path)
{
	m_Path.dw = Path.dw;
	}

void CUsbDevice::SetPath(UsbTreePath const &Path)
{
	m_Tree.dw = Path.dw;
	}

void CUsbDevice::SetHostC(IUsbHostControllerDriver *pHost)
{
	m_pHostC = pHost;
	}

void CUsbDevice::SetHostI(IUsbHostInterfaceDriver *pHost)
{
	m_pHostI = pHost;
	}

void CUsbDevice::SetSpeed(UINT uSpeed)
{
	m_uSpeed = uSpeed;
	}

BOOL CUsbDevice::SetAltInterface(UINT iInt, UINT iAlt)
{
	if( iInt < elements(m_IntList) ) {

		CInterfaceCtx &Ctx = m_IntList[iInt];

		UINT iIdent = Ctx.m_pDesc ? Ctx.m_pDesc->m_bThis : NOTHING;

		if( iIdent != NOTHING ) {

			UINT iIndex;

			UsbInterfaceDesc *pDesc = (UsbInterfaceDesc *) m_CfgDesc.FindInterface(iIndex, iIdent, iAlt);

			if( pDesc ) {

				FreePipes(iIdent);

				Ctx.m_pDesc = pDesc;

				MakePipes(iIdent);

				m_pStack->SetInterface(m_pCtrl, iIdent, iAlt);
			
				return true;
				}
			}
		}

	return false;
	}

BOOL CUsbDevice::GetString(UINT iStr, PTXT pStr, UINT uLen)
{
	if( m_pStack && m_pCtrl ) {

		return m_pStack->GetStringDesc(m_pCtrl, iStr, pStr, uLen);
		}
	
	return false;
	}

// Device State Handlers

BOOL CUsbDevice::OnInit(void)
{
	memset(&m_IntList, 0, sizeof(m_IntList));

	m_uDrvCount = 0;

	MakeDevice();

	return InitControl();
	}

BOOL CUsbDevice::OnAddress(void)
{
	if( m_pHostI->GetType() == usbXhci ) {

		FreeAddress();

		m_bAddr = m_pHostI->GetEndptDevAddr(m_pCtrl->GetEndpoint());

		/*AfxTrace("OnAddress : 0x%2.2X\n", m_bAddr);*/

		return true;
		}

	UINT uAddr = m_pHostI->AllocAddress();

	if( uAddr < NOTHING ) {

		if( m_pStack->SetAddress(m_pCtrl, uAddr) ) {

			/*AfxTrace("OnAddress : 0x%2.2X\n", uAddr);*/

			FreeAddress();
		
			return SetAddr(uAddr);
			}

		m_pHostI->FreeAddress(uAddr);
		}

	FreeAddress();

	return false;
	}

BOOL CUsbDevice::OnDiscover(void)
{
	if( m_pStack->GetDevDesc(m_pCtrl, PBYTE(&m_DevDesc), 8) ) {

		m_pCtrl->SetMaxPacket(m_DevDesc.m_bMaxPacket);

		if( m_pStack->GetDevDesc(m_pCtrl, PBYTE(&m_DevDesc), m_DevDesc.m_bLength) ) {

			m_DevDesc.Debug();

			if( IsHub() ) {

				if( m_pStack->GetHubDesc(m_pCtrl, PBYTE(&m_HubDesc), sizeof(m_HubDesc)) ) {
				
					m_HubDesc.Debug();
					
					return CheckDevice();
					}

				return false;
				}

			return CheckDevice();
			}
		}

	return false;
	}

BOOL CUsbDevice::OnConfigure(void)
{
	UsbConfigDesc Desc;

	if( m_pStack->GetConfigDesc(m_pCtrl, PBYTE(&Desc), sizeof(Desc)) ) {

		UINT  uSize = Desc.m_wTotal;
		
		PBYTE pData = New BYTE [ uSize ];
		
		if( m_pStack->GetConfigDesc(m_pCtrl, pData, uSize) ) {

			m_CfgDesc.Take(pData, uSize);

			m_CfgDesc.Debug();

			MakeInterfaces(Desc.m_bInterfaces);

			MakeDrivers();

			MakePipes();

			if( CheckConfig() ) {

				if( m_pStack->SetConfig(m_pCtrl, Desc.m_bConfig ? Desc.m_bConfig : 1) ) {

					OpenDrivers();

					return true;
					}
				}

			return false;
			}

		delete [] pData;
		}

	return false;
	}

BOOL CUsbDevice::OnDestroy(void)
{
	StopDrivers();

	if( !m_nLock ) {

		FreeDrivers();

		FreePipes();

		FreeInterfaces();
	
		FreeControl();

		FreeAddress();

		FreeDevice();

		return true;
		}
		
	return false;
	}

BOOL CUsbDevice::OnReady(UINT uLapsed)
{
	PollDrivers(uLapsed);

	PollPipes(uLapsed);

	return true;
	}

// Device 

void CUsbDevice::MakeDevice(void)
{
	m_iDev = m_pHostI->MakeDevice(this);
	}

void CUsbDevice::FreeDevice(void)
{
	if( m_iDev != NOTHING ) {

		m_pHostI->KillDevice(m_iDev);

		m_iDev = NOTHING;
		}
	}

// Interfaces

void CUsbDevice::MakeInterfaces(UINT uCount)
{
	MakeMin(uCount, elements(m_IntList));

	UINT iIndex = m_CfgDesc.GetIndexStart();

	for( UINT i = 0; i < uCount; i ++ ) {

		m_IntList[i].m_pDesc = (UsbInterfaceDesc *) m_CfgDesc.EnumInterface(iIndex, 0);
		}
	}

void CUsbDevice::FreeInterfaces(void)
{
	}

// Drivers

void CUsbDevice::MakeDrivers(void)
{
	if( CheckClass(true) ) {

		CInterfaceCtx &i = m_IntList[0];

		i.m_pDriver = NULL;

		if( !m_pStack->GetDriverClassFilter(m_DevDesc.m_wVendorID, m_DevDesc.m_wProductID) ) {

			i.m_pDriver = m_pStack->CreateDriver( m_DevDesc.m_bClass,
							      m_DevDesc.m_bSubClass,
							      m_DevDesc.m_bProtocol
							      );

			if( !i.m_pDriver ) {

				i.m_pDriver = m_pStack->CreateDriver( m_DevDesc.m_wVendorID,
								      m_DevDesc.m_wProductID,
								      m_DevDesc.m_bClass,
								      m_DevDesc.m_bSubClass,
								      m_DevDesc.m_bProtocol
								      );
				}

			if( i.m_pDriver ) {

				i.m_pDriver->Bind(this, 0);

				m_uDrvCount = 1;
				}
			}

		return;
		}

	for( UINT n = 0; n < elements(m_IntList); n ++ ) {

		CInterfaceCtx &i = m_IntList[n];

		if( i.m_pDesc )  {

			i.m_pDriver = NULL;

			if( !m_pStack->GetDriverClassFilter(m_DevDesc.m_wVendorID, m_DevDesc.m_wProductID) ) {

				i.m_pDriver = m_pStack->CreateDriver( i.m_pDesc->m_bClass,
								      i.m_pDesc->m_bSubClass,
								      i.m_pDesc->m_bProtocol
								      );

				if( !i.m_pDriver ) {

					i.m_pDriver = m_pStack->CreateDriver( m_DevDesc.m_wVendorID,
									      m_DevDesc.m_wProductID,
									      i.m_pDesc->m_bClass,
									      i.m_pDesc->m_bSubClass,
									      i.m_pDesc->m_bProtocol
									      );
					}

				if( i.m_pDriver ) {

					m_uDrvCount ++;

					i.m_pDriver->Bind(this, n);
					}
				}

			continue;
			}

		break;
		}
	}

void CUsbDevice::OpenDrivers(void)
{
	for( UINT n = 0; n < elements(m_IntList); n ++ ) {

		CInterfaceCtx &i = m_IntList[n];

		if( i.m_pDesc && i.m_pDriver )  {

			CUsbDescList List;

			List.Attach(PBYTE(i.m_pDesc), -1);
			
			if( i.m_pDriver->Open(List) ) {

				if( m_pEvent ) {

					m_pEvent->OnDriverStarted(i.m_pDriver);
					}
				}
			else {
				i.m_pDriver->Release();

				i.m_pDriver = NULL;
				}
			}
		}
	}

void CUsbDevice::PollDrivers(UINT uLapsed)
{
	for( UINT n = 0; n < elements(m_IntList); n ++ ) {

		CInterfaceCtx &i = m_IntList[n];

		if( i.m_pDriver ) {

			i.m_pDriver->Poll(uLapsed);
			
			continue;
			}
		}
	}

void CUsbDevice::StopDrivers(void)
{
	for( UINT n = 0; n < elements(m_IntList); n ++ ) {

		CInterfaceCtx &i = m_IntList[n];

		if( i.m_pDriver ) {

			if( i.m_pDriver->GetActive() ) {
				
				i.m_pDriver->Close();

				if( m_pEvent ) {
				
					m_pEvent->OnDriverStopped(i.m_pDriver);
					}
				}
			}
		}
	}

void CUsbDevice::FreeDrivers(void)
{
	for( UINT n = 0; n < elements(m_IntList); n ++ ) {

		CInterfaceCtx &i = m_IntList[n];

		if( i.m_pDriver ) {

			i.m_pDriver->Release();

			i.m_pDriver = NULL;
			}
		}
	}

// Pipes

void CUsbDevice::MakePipes(void)
{
	for( UINT i = 0; i < elements(m_IntList); i ++ ) {

		CInterfaceCtx &Ctx = m_IntList[i];

		if( Ctx.m_pDriver != NULL || (CheckClass(false) && m_IntList[0].m_pDriver) ) {

			MakePipes(i);
			}
		}
	}

void CUsbDevice::MakePipes(UINT iInterface)
{
	if( iInterface < elements(m_IntList) ) {

		CInterfaceCtx &Ctx = m_IntList[iInterface];

		if( Ctx.m_pDriver != NULL || (CheckClass(false) && m_IntList[0].m_pDriver) ) {

			UINT iIndex = m_CfgDesc.FindIndex(PCBYTE(Ctx.m_pDesc));

			UINT iPipe  = 0;

			for( UINT e = 0; e < Ctx.m_pDesc->m_bEndpoints; e++ ) {

				UsbEndpointDesc *pEp = (UsbEndpointDesc *) m_CfgDesc.EnumEndpoint(iIndex);

				if( pEp && iPipe < elements(Ctx.m_PipeList) ) {

					DWORD iEndpt = NOTHING;

					if( pEp->m_bTransfer == eptBulk ) {
						
						iEndpt = m_pHostI->MakeEndptAsync(m_iDev, m_uSpeed, *pEp);
						}
					
					else if( pEp->m_bTransfer == eptInterrupt ) {

						iEndpt = m_pHostI->MakeEndptInt(m_iDev, m_uSpeed, *pEp);
						}

					else if( pEp->m_bTransfer == eptIsoch ) {

						// TODO --

						continue;
						}

					if( iEndpt != NOTHING ) {

						CPipeCtx &Pipe = Ctx.m_PipeList[iPipe++];

						Pipe.m_pDesc  = pEp;

						Pipe.m_pPipe  = Create_UsbPipe();

						Pipe.m_pEvent = NULL;

						Pipe.m_pPipe->QueryInterface(AfxAeonIID(IUsbPipeEvents), (void **) &Pipe.m_pEvent);
						
						Pipe.m_pPipe->SetDevice(this);

						Pipe.m_pPipe->SetDirIn(pEp->m_bDirIn);

						Pipe.m_pPipe->SetEndpoint(iEndpt);

						Pipe.m_pPipe->SetEndpointAddr(pEp->m_bAddr);
						
						m_pHostI->SetEndptDevAddr(iEndpt, m_bAddr);

						m_pHostI->SetEndptHub(iEndpt, m_Path.a.dwHubAddr, m_Path.a.dwHubPort);

						/*AfxTrace("UsbDevice.MakePipes(Dev=%d, iInt=%d, iPipe=%d, Addr=%2.2X, IN=%d)\n", m_bAddr, iInterface, iPipe-1, pEp->m_bAddr, pEp->m_bDirIn);*/
												
						continue;
						}
					}

				break;
				}
			}
		}
	}

void CUsbDevice::PollPipes(UINT uLapsed)
{
	for( UINT i = 0; i < elements(m_IntList); i ++ ) {

		CInterfaceCtx &Ctx = m_IntList[i];

		if( Ctx.m_pDesc ) {

			for( UINT e = 0; e < elements(Ctx.m_PipeList); e ++ ) {
		
				IUsbPipeEvents *p = Ctx.m_PipeList[e].m_pEvent;
			
				if( p ) {
				
					p->OnPoll(uLapsed);
					
					continue;
					}
				
				break;
				}
			}
		}
	}

void CUsbDevice::FreePipes(UINT iInterface)
{
	if( iInterface < elements(m_IntList) ) {

		CInterfaceCtx &Ctx = m_IntList[iInterface];

		if( Ctx.m_pDesc ) {

			for( UINT e = 0; e < elements(Ctx.m_PipeList); e ++ ) {
		
				IUsbPipe       * &pPipe  = Ctx.m_PipeList[e].m_pPipe;

				IUsbPipeEvents * &pEvent = Ctx.m_PipeList[e].m_pEvent;
			
				if( pPipe ) {

					m_pHostI->KillEndpt(pPipe->GetEndpoint());

					m_pStack->FreeUrbs(pPipe);
				
					pPipe->Release();

					pEvent->Release();

					pPipe  = NULL;
					
					pEvent = NULL;
					
					continue;
					}
				
				break;
				}
			}
		}
	}

void CUsbDevice::FreePipes(void)
{
	for( UINT i = 0; i < elements(m_IntList); i ++ ) {

		FreePipes(i);
		}
	}

// Pipes

BOOL CUsbDevice::SetAddr(BYTE bAddr)
{
	m_bAddr = bAddr;

	return m_pHostI->SetEndptDevAddr(m_pCtrl->GetEndpoint(), bAddr);
	}

BOOL CUsbDevice::InitControl(void)
{
	DWORD iEndpt = m_pHostI->MakeEndptCtrl(m_iDev, m_uSpeed);

	if( iEndpt != NOTHING ) {

		m_pCtrl->SetEndpoint(iEndpt);
		
		m_pHostI->SetEndptDevAddr(iEndpt, 0);

		m_pHostI->SetEndptAddr(iEndpt, 0);

		m_pHostI->SetEndptHub(iEndpt, m_Path.a.dwHubAddr, m_Path.a.dwHubPort);

		return true;
		}

	return false;
	}

void CUsbDevice::FreeAddress(void)
{
	if( m_bAddr == 255 ) {

		m_pHostI->LockDefAddr(false);
		}

	if( m_bAddr && m_bAddr < 128 ) {

		m_pHostI->FreeAddress(m_bAddr);
		}

	m_bAddr = 0;
	}

void CUsbDevice::FreeControl(void)
{
	DWORD iEndpt = m_pCtrl->GetEndpoint();

	if( iEndpt != NOTHING ) {

		m_pHostI->KillEndpt(iEndpt);

		m_pCtrl->SetEndpoint(NOTHING);
		}
	}

// Check

bool CUsbDevice::CheckClass(bool fStrict)
{
	if( m_pStack->GetDriverClassOverride(m_DevDesc.m_wVendorID, m_DevDesc.m_wProductID) ) {

		return true;
		}

	if( m_DevDesc.m_bClass && (!fStrict || m_DevDesc.m_bClass != devMisc) ) {

		return true;
		}

	return false;
	}

bool CUsbDevice::CheckDevice(void)
{
	return !IsHub() || m_Tree.a.dwTier < 6;
	}

bool CUsbDevice::CheckConfig(void)
{
	return m_pHostI->CheckConfig(m_iDev);	
	}

// End of File
