
#include "phxnbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Phoenix NanoLC Driver
//

class CPhoenixNanoLCDriver : public CPhoenixNanoBase
{
	public:
		// Constructor
		CPhoenixNanoLCDriver(void);

		// Destructor
		~CPhoenixNanoLCDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};

		// Data Members
		CContext	*m_pCtx;

		CRC16		m_CRC;

		PBYTE		m_pTx;
		PBYTE		m_pRx;

		BYTE		m_bTx[BUFFSIZE];
		BYTE		m_bRx[BUFFSIZE];

		UINT		m_uTimeout;

		// Port Access
		UINT	RxByte(UINT uTime);
		
		// Transport Layer
		BOOL	Transact(UINT uSize, BOOL fIgnore);
		BOOL	PutFrame(UINT uSize);
		BOOL	GetFrame(void);
		BOOL	BinaryTx(UINT uSize);
		BOOL	BinaryRx(void);

		// Frame Header
		void AddFrameHeader(BYTE bOpcode);

		// Transport Helpers
		UINT	FindEndTime(void);

		// Device Info
		void	GetDeviceInfo(PBYTE pbDrop, BOOL *pfTCP, PBYTE *pTx, PBYTE *pRx);
		};

// End of File
