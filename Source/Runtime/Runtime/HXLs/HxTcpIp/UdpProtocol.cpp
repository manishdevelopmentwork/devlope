
#include "Intern.hpp"

#include "UdpProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PseudoHeader.hpp"

#include "UdpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// UDP Protocol
//

// Creation Function

IProtocol * Create_UDP(void)
{
	return New CUdp;
	}

// Constructor

CUdp::CUdp(void)
{
	StdSetRef();

	m_pLock   = Create_Rutex();

	m_pRouter = NULL;

	m_pUtils  = NULL;

	m_uMask   = 0;

	m_uSend   = 0;

	memset(m_Used, 0, sizeof(m_Used));

	BindSockets();
	}

// Destructor

CUdp::~CUdp(void)
{
	AfxRelease(m_pLock);
	}

// Attributes

BOOL CUdp::AcceptIP(IPREF Ip) const
{
	return m_pUtils->IsSafeAddress(Ip);
	}

// Operations

void CUdp::Enable(WORD wPort)
{
	m_Used[wPort / 32] |= (1 << (wPort % 32));
	}

BOOL CUdp::Send(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	CUdpHeader *pUDP = (CUdpHeader *) pBuff->GetData();

	pUDP->HostToNet();

	if( !m_pRouter->Send(Dest, From, IP_UDP, pBuff, this) ) {

		TcpDebug(OBJ_UDP, LEV_TRACE, "failed to send\n");

		return FALSE;
		}

	return TRUE;
	}

void CUdp::SendReq(BOOL fState)
{
	if( fState ) {

		if( !m_uSend++ ) {

			m_pRouter->SendReq( m_uMask,
					    TRUE
					    );
			}
		}
	else {
		if( !--m_uSend ) {

			m_pRouter->SendReq( m_uMask,
					    FALSE
					    );
			}
		}
	}

void CUdp::EnableMulticast(IPREF Ip, BOOL fEnable)
{
	m_pRouter->EnableMulticast(Ip, fEnable);
	}

void CUdp::FreeSocket(CUdpSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) pSock->m_pRoot;

	AfxListRemove(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);
	}

// IUnknown

HRESULT CUdp::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IProtocol);

	StdQueryInterface(IThreadNotify);

	StdQueryInterface(IProtocol);

	return E_NOINTERFACE;
	}

ULONG CUdp::AddRef(void)
{
	StdAddRef();
	}

ULONG CUdp::Release(void)
{
	StdRelease();
	}

// IThreadNotify

UINT CUdp::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	CListRoot *pRoot = New CListRoot;

	pRoot->m_pHead = NULL;

	pRoot->m_pTail = NULL;

	return UINT(pRoot);
	}

void CUdp::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam)
{
	CListRoot *pRoot = (CListRoot *) uParam;

	if( pRoot->m_pHead ) {

		AfxTrace("tcpip: thread %u exiting with open udp sockets\n", pThread->GetIdent());

		while( pRoot->m_pHead ) {

			CUdpSocket *pSock = pRoot->m_pHead;

			pSock->Release();

			AfxAssert(pRoot->m_pHead != pSock);
			}
		}

	delete pRoot;
	}

// IProtocol

BOOL CUdp::Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask)
{
	m_pRouter = pRouter;

	m_pUtils  = NULL;

	m_uMask   = uMask;

	return TRUE;
	}

BOOL CUdp::CreateSocket(ISocket * &pSocket)
{
	CAutoLock Lock(m_pLock);

	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CUdpSocket &Sock = m_Sock[n];

		if( Sock.IsFree() ) {

			Sock.Create();

			AddSocket(&Sock);

			pSocket = &Sock;

			return TRUE;
			}
		}

	pSocket = NULL;

	return FALSE;
	}

BOOL CUdp::Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	UINT uSize = pBuff->GetSize();

	if( likely(uSize >= sizeof(UDPHEADER)) ) {

		CUdpHeader *pUDP = BuffStripHead(Buff, CUdpHeader);

		if( likely(pBuff->TestFlag(packetSumsValid) || pUDP->TestChecksum(Ps)) ) {

			pUDP->NetToHost();

			if( IsPortUsed(pUDP->m_RemPort) ) {

				for( UINT n = 0; n < elements(m_Sock); n++ ) {

					CUdpSocket &Sock = m_Sock[n];

					if( Sock.OnTest(pUDP) ) {
					
						if( Sock.OnRecv(Face, Ps, pUDP, Buff) ) {

							Buff.TakeOver();

							return TRUE;
							}
						}
					}
				}

			if( Ps.m_Dest.IsBroadcast() || Ps.m_Dest.IsMulticast() ) {

				return TRUE;
				}

			TcpDebug(OBJ_UDP, LEV_TRACE, "no socket %u\n", pUDP->m_RemPort);

			Buff.TakeOver();

			return FALSE;
			}

		TcpDebug(OBJ_UDP, LEV_WARN, "checksum invalid\n");

		return TRUE;
		}

	TcpDebug(OBJ_UDP, LEV_WARN, "packet too small\n");

	return TRUE;
	}

BOOL CUdp::Send(void)
{
	BOOL fFail = FALSE;

	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CUdpSocket &Sock = m_Sock[n];

		if( !Sock.OnSend() ) {

			fFail = TRUE;
			}
		}

	return fFail ? FALSE : TRUE;
	}

BOOL CUdp::Poll(void)
{
	return TRUE;
	}

BOOL CUdp::NetStat(IDiagOutput *pOut)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CUdpSocket &Sock = m_Sock[n];

		Sock.NetStat(pOut);
		}

	return TRUE;
	}

BOOL CUdp::SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	CUdpHeader *pUDP = (CUdpHeader *) pBuff->GetData();

	if( pUDP ) {

		DWORD PsSum = m_pRouter->GetPseudoSum(Dest, From, IP_UDP, pBuff);
	
		pUDP->AddChecksum(PsSum);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CUdp::BindSockets(void)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CUdpSocket &Sock = m_Sock[n];

		Sock.Bind(this);
		}
	}

BOOL CUdp::IsPortUsed(WORD wPort)
{
	if( m_Used[wPort / 32] & (1 << (wPort % 32)) ) {

		return TRUE;
		}

	return FALSE;
	}

void CUdp::AddSocket(CUdpSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) AddThreadNotify(this);

	AfxListAppend(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);

	pSock->m_pRoot = pRoot;
	}

// End of File
