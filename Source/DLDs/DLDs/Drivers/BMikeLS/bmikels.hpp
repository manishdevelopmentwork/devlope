#ifndef INCLUDE_BMIKELS_HPP

#define INCLUDE_BMIKELS_HPP

/////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Enumerations
//

enum {
	spaceMPIM	=  1,
	spaceMPDT	=  2,
	spaceMPVT	=  3,
	spaceMPT	=  4,
	spaceWT		=  5,
	spaceMFL	=  6,
	spaceIPL	=  7,
	spaceIPE	=  8,
	spaceHSPR	=  9,
	spaceLSPR	= 10,
	spaceMU		= 11,
	
	spaceDIS	= 65,
	spaceHVIA	= 66,
	spaceVHT	= 67,
	spaceCT		= 68,
	spaceUUR	= 69,
	spaceLRV	= 70,
	spaceMinVL	= 71,
	spaceMaxVL	= 72,
	spaceLRIA	= 73,
	spaceQFWThr	= 74,
	spaceQFWTo	= 75,
	spaceVALM	= 76,
	spaceCLC	= 77,
	spaceAT		= 78,
	spaceSL		= 79,
	spaceSLE	= 80,
	spaceRILC	= 81,
	spaceHSPCLR	= 82,
	spaceLSPCLR	= 83,
	spaceDLCM	= 84,
	spaceSTS	= 85,
	spaceSF		= 86,
	spaceRZH	= 87,
	spaceARB	= 88,
	spaceARBRES	= 89,
	
	spaceAFS	= 129,
	spaceADOF	= 130,
	spaceHSPC	= 131,
	spaceLSPC	= 132,
	spaceLTA	= 133,
	spaceLTB	= 134,
	spaceAZS	= 135,

	spaceGSN	= 193,
	spaceFVS	= 194,
	spaceHMCV	= 195,
	spaceCTmp	= 196,
	spaceMTmp	= 197,
	spaceMVer	= 198,
	
	space232BRS	= 257,
	space232POM	= 258,
	space422BRS	= 259,
	space422POM	= 260,
	spaceEE		= 261,
	spaceELS	= 262,
	spaceEHI	= 263,
	spaceDHCP	= 264,
	spaceHN		= 265,
	spaceIA		= 266,
	spaceIDG	= 267,
	spaceINM	= 268,
	spaceUDPDP	= 269,
	spaceUDPDIA	= 270,
	spaceUDPDIP	= 271,
	spaceUDPPOM	= 272,
	
	spaceRTL	= 321,
	spaceRTV	= 322,
	spaceRTQF	= 323,
	spaceRTS	= 324,	
	spaceAQF	= 325,
	spaceFL		= 326,
	};

enum {
	LS9K		= 1,
	LS8K		= 2,
	LS4K		= 3,
	};


//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Serial Master Driver
//

class CBMikeLSMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CBMikeLSMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void ) Service(void);

				
	protected:
		BYTE   m_bRx  [300];
		BYTE   m_bTx  [300];
		UINT   m_uPtr;
		UINT   m_uSize;
		UINT   m_uCheck;
		BOOL   m_fTB;
		BOOL   m_fTF;
		BOOL   m_fKickTF;
		BYTE   m_bMask;

		// Device Context
		struct CBaseCtx
		{
			DWORD	m_Scan;
			WORD	m_Timeout;
			DWORD	m_Time  [512];
			DWORD	m_Record[512];
			DWORD   m_Info  [1024];
			DWORD	m_Comms [1024];
			BOOL	m_fError;
			BYTE	m_bModel;
			DWORD   m_ARB[16];
			BOOL	m_bAuto;
			BOOL    m_fInit;
			};
		  
		// Data Members
		CBaseCtx * m_pBase;
		
		// Transport Layer
		BOOL Transact(UINT uSpace);
		virtual BOOL Send(void);
		virtual BOOL RecvFrame(void);
		void EndRealTime(void);
		virtual BOOL CheckFrame(void);
		BOOL ChecksumCheck(void);

		// Implementation
		BOOL  Read(UINT uSpace);
		BOOL  Write(UINT uSpace, PDWORD pData, UINT uCount, UINT uType);
		virtual BOOL  Begin(UINT uSpace);
		void  AddExtra(UINT uSpace, BOOL fWrite);
		void  AddArg(UINT uSpace, PDWORD pData, UINT uCount, UINT uType);
		void  End(UINT uSpace);
		virtual void  AddByte(BYTE bByte);
		void  AddNumber(DWORD dwValue, BOOL fReal);
		BOOL  AddText(PCTXT pTxt);
		void  AddString(PDWORD pData, UINT uCount);
		DWORD GetReal(UINT uSpace);
		DWORD GetDec(UINT uSpace);
		void  GetData(AREF Addr, PDWORD pData, UINT uCount);
		void  CacheData(AREF Addr, UINT uCount);
		void  CacheString(PDWORD pData, UINT uSpace, UINT uPos);
		BOOL  FindModel(void);
		BOOL  ArbCmd(UINT uSpace, PDWORD pData);
		BOOL  LSAutoBaudSeq(void);
		
		// Helpers
		BOOL IsWriteOnly(UINT uSpace);
		BOOL IsArbCmd(UINT uSpace);
		BOOL IsArbRes(UINT uSpace);
	virtual	BOOL IsRealTime(UINT uSpace);
		BOOL IsTimedOut(UINT uSpace);
		BOOL IsValid(UINT uSpace);
		BOOL IsModelValid(UINT uSpace);
		BOOL IsExempt(UINT uSpace);
		BYTE FromAscii(BYTE bByte);
		UINT GetItemNumber(AREF Addr);
		
	};

#endif

// End of File
