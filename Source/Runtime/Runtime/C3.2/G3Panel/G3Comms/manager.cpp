
#include "intern.hpp"

#include "service.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Communications Manager
//

// Constructor

CCommsManager::CCommsManager(void)
{
	StdSetRef();

	m_MaxDevice  = 0;

	m_MaxBlock   = 0;

	m_StrPad     = 0;

	m_pOnEarly   = NULL;

	m_pOnStart   = NULL;

	m_pOnApply   = NULL;

	m_pOnSecond  = NULL;

	m_pEthernet  = New CEthernetItem;
	
	m_pPorts     = New CCommsPortList;
	
	m_pServices  = New CServices;

	m_pRack      = New CRackItem;

	m_pDisplays  = Create_ProxyDisplayList();

	m_pCameras   = Create_ProxyCameraList();

	m_Handle     = NOTHING;

	m_ppDevice   = NULL;

	m_ppBlock    = NULL;

	DiagRegister();
	}

// Destructor

CCommsManager::~CCommsManager(void)
{
	DiagRevoke();

	delete m_pOnEarly;

	delete m_pOnStart;

	delete m_pOnApply;

	delete m_pOnSecond;

	delete m_pCameras;

	delete m_pDisplays;

	delete m_pEthernet;

	delete m_pPorts;
	
	delete m_pServices;

	delete m_pRack;

	delete m_ppPort;

	delete m_ppDevice;

	delete m_ppBlock;
	}

// Initialization

void CCommsManager::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsManager", pData);

	m_StrPad    = GetByte(pData);

	GetCoded(pData, m_pOnEarly);

	GetCoded(pData, m_pOnStart);

	GetCoded(pData, m_pOnApply);

	GetCoded(pData, m_pOnSecond);

	m_MaxPort   = GetWord(pData);

	m_MaxDevice = GetWord(pData);

	m_MaxBlock  = GetWord(pData);

	m_ppPort    = New CCommsPort     * [ m_MaxPort   + 1 ];

	m_ppDevice  = New CCommsDevice   * [ m_MaxDevice + 1 ];

	m_ppBlock   = New CCommsSysBlock * [ m_MaxBlock  + 1 ];

	memset(m_ppPort,   0, sizeof(PVOID) * (m_MaxPort   + 1));

	memset(m_ppDevice, 0, sizeof(PVOID) * (m_MaxDevice + 1));

	memset(m_ppBlock,  0, sizeof(PVOID) * (m_MaxBlock  + 1));

	m_pEthernet->Load(pData);

	m_pPorts   ->Load(pData);

	m_pServices->Load(pData);

	m_pRack    ->Load(pData);

	if( GetByte(pData) ) {

		UINT uLedAlarm = GetByte(pData);

		UINT   uLedOrb = GetByte(pData);

		UINT  uLedHome = GetByte(pData);

		g_pPxe->SetLedMode( (uLedAlarm << 0) | 
				    (uLedOrb   << 1) | 
				    (uLedHome  << 2) );
		}
	}

// Attributes

DWORD CCommsManager::GetCommsError(void) const
{
	DWORD e = 0;

	DWORD m = 1;

	for( UINT n = 1; m && n <= m_MaxDevice; n++ ) {

		CCommsDevice *pDevice = m_ppDevice[n];

		if( pDevice ) {
			
			if( pDevice->HasError() ) {

				e |= m;
				}
			}

		m <<= 1;
		}

	return e;
	}

UINT CCommsManager::GetCommsStatus(void) const
{
	BOOL a = FALSE;

	for( UINT n = 1; n <= m_MaxDevice; n++ ) {

		CCommsDevice *pDevice = m_ppDevice[n];

		if( pDevice ) {

			if( pDevice->IsEnabled() ) {
				
				if( pDevice->HasError() ) {

					return 1;
					}

				a = TRUE;
				}
			}
		}

	return a ? 2 : 0;
	}

// Registration

void CCommsManager::RegPort(UINT uPort, CCommsPort *pPort)
{
	if( uPort <= m_MaxPort ) {

		if( !m_ppPort[uPort] ) {

			m_ppPort[uPort] = pPort;

			return;
			}

		AfxTrace("comms: attempt to re-register port %u\n", uPort);

		return;
		}

	AfxTrace("comms: attempt to register invalid port %u\n", uPort);
	}

void CCommsManager::RegDevice(UINT uDevice, CCommsDevice *pDevice)
{
	if( uDevice <= m_MaxDevice ) {

		if( !m_ppDevice[uDevice] ) {

			m_ppDevice[uDevice] = pDevice;

			return;
			}

		AfxTrace("comms: attempt to re-register device %u\n", uDevice);

		return;
		}

	AfxTrace("comms: attempt to register invalid device %u\n", uDevice);
	}

void CCommsManager::RegBlock(UINT uBlock, CCommsSysBlock *pBlock)
{
	if( uBlock <= m_MaxBlock ) {

		if( !m_ppBlock[uBlock] ) {

			m_ppBlock[uBlock] = pBlock;

			return;
			}

		AfxTrace("comms: attempt to re-register block %u\n", uBlock);

		return;
		}

	AfxTrace("comms: attempt to register invalid block %u\n", uBlock);
	}

// Item Lookup

ICommsRawPort * CCommsManager::FindRawPort(UINT uPort) const
{
	// TODO -- Client of this must call Release!!!

	ICommsRawPort *pRaw = NULL;

	if( uPort <= m_MaxPort ) {

		CCommsPort *pPort = m_ppPort[uPort];

		if( pPort && pPort->m_pDriver ) {

			pPort->m_pDriver->QueryInterface(AfxAeonIID(ICommsRawPort), (void **) &pRaw);
			}
		}

	return pRaw;
	}

CCommsPort * CCommsManager::FindPort(UINT uPort) const
{
	if( uPort <= m_MaxPort ) {

		return m_ppPort[uPort];
		}

	return NULL;
	}

CCommsDevice * CCommsManager::FindDevice(UINT uDevice) const
{
	if( uDevice <= m_MaxDevice ) {

		return m_ppDevice[uDevice];
		}

	return NULL;
	}

CCommsSysBlock * CCommsManager::FindBlock(UINT uBlock) const
{
	if( uBlock <= m_MaxBlock ) {

		return m_ppBlock[uBlock];
		}

	return NULL;
	}

// Block Resolution

CCommsSysBlock * CCommsManager::Resolve(CDataRef const &Src, int &nPos)
{
	CCommsSysBlock *pBlock = FindBlock(Src.b.m_Block);

	if( pBlock ) {

		INT nShift = pBlock->m_Base - pBlock->m_Index;

		nPos       = Src.b.m_Offset + nShift / pBlock->m_Factor;

		return pBlock;
		}

	return NULL;
	}

// Address Lookup

BOOL CCommsManager::FindAddress(CAddress const &Addr, CDataRef &Ref, CCommsDevice * pDev)
{
	for( UINT b = 0; b <= m_MaxBlock; b++ ) {

		CCommsSysBlock *pBlock = m_ppBlock[b];

		if( pBlock ) {

			int nPos; 

			if( pBlock->HasAddress((DWORD &) Addr, nPos, pDev) ) {

				Ref.b.m_Block  = b;

				Ref.b.m_Offset = (Addr.a.m_Offset - pBlock->m_Base) / pBlock->m_Factor;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Task List

void CCommsManager::GetTaskList(CTaskList &List)
{
	UINT uLevel = 4000;

	m_pEthernet->GetTaskList(List, uLevel);

	m_pPorts   ->GetTaskList(List, uLevel);

	m_pServices->GetTaskList(List);

	// TODO -- Rack should be a runtime feature!

	m_pRack->GetTaskList(List);
	}

// IUnknown

HRESULT CCommsManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CCommsManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CCommsManager::Release(void)
{
	StdRelease();
	}

// IDiagProvider

UINT CCommsManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagBlocks(pOut, pCmd);

		case 2:
			return DiagDevs(pOut, pCmd);

		case 3:
			return DiagPorts(pOut, pCmd);
		}

	return 1;
	}

// Diagnostics

BOOL CCommsManager::DiagRegister(void)
{
	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "comms");

		m_pDiag->RegisterCommand(m_uProv, 1, "blocks");

		m_pDiag->RegisterCommand(m_uProv, 2, "devices");

		m_pDiag->RegisterCommand(m_uProv, 3, "ports");

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsManager::DiagRevoke(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;

		return TRUE;
		}

	return FALSE;
	}

UINT CCommsManager::DiagBlocks(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		BOOL fHead = TRUE;

		for( UINT b = 1; b <= m_MaxBlock; b++ ) {

			CCommsSysBlock *pBlock = m_ppBlock[b];

			if( pBlock ) {

				pBlock->CommsDebug(pOut, fHead);
				}
			}

		if( fHead ) {

			pOut->Print("comms: no blocks defined\n");
			}
		else {
			pOut->AddRule('-');

			pOut->EndTable();
			}

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CCommsManager::DiagDevs(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		BOOL fHead = TRUE;

		for( UINT d = 1; d <= m_MaxDevice; d++ ) {

			CCommsDevice *pDevice = m_ppDevice[d];

			if( pDevice ) {

				pDevice->CommsDebug(pOut, fHead);
				}
			}

		if( fHead ) {

			pOut->Print("comms: no devices defined\n");
			}
		else {
			pOut->AddRule('-');

			pOut->EndTable();
			}

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CCommsManager::DiagPorts(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		BOOL fHead = TRUE;

		for( UINT p = 1; p <= m_MaxPort; p++ ) {

			CCommsPort *pPort = m_ppPort[p];

			if( pPort ) {

				pPort->CommsDebug(pOut, fHead);
				}
			}

		if( fHead ) {

			pOut->Print("comms: no ports defined\n");
			}
		else {
			pOut->AddRule('-');

			pOut->EndTable();
			}

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// End of File
