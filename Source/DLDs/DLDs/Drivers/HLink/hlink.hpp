//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define T_A	1
#define T_G	2
#define T_N	3
#define T_T	4
#define T_DE	5
#define T_P	6
#define T_S	7
#define T_DI	8
#define T_L	9
#define T_REL	10
#define T_REM	11
#define T_C	12
#define T_E	13
#define T_I	14
#define T_IREM	15
#define T_M	16
#define T_U	17
#define T_H	18
#define T_AT	19
#define T_AZ	20
#define T_NTOT	21
#define T_ERR	22

#define M_LB	0
#define M_KG	1

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	R2I(x)	(*((long  *) &(x)))

#define	I2R(x)	(*((float *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Hardy Instruments HardyLink Master Serial Driver
//
//

class CHardyLinkSerialMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CHardyLinkSerialMasterDriver(void);

		// Destructor
		~CHardyLinkSerialMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bID;
			BYTE	m_bMode;
			DWORD	m_Error;
			BOOL	m_Check;
			};

		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		BYTE	   m_bTx[300];
		BYTE	   m_bRx[300];
		UINT	   m_uPtr;
		BYTE       m_bCheck;
		char 	   m_Data[7];
							
		// Implementation
		void Start(void);
		void End(void);
		void AddByte(BYTE bByte);
		void AddSub(UINT uOffset);
		void AddData(PDWORD pData, UINT uType);
										
		// Transport Layer
		BOOL Transact(void);
		BOOL Send(void);
		BOOL Recv(void);
		BOOL CheckFrame(void);

		// Helpers
		BOOL  IsReadOnly(UINT uOffset);
		BOOL  IsWriteOnly(UINT uOffset);
		BOOL  IsCommand(UINT uOffset);
		BOOL  IsAuto(UINT uOffset);
		BOOL  IsReal(UINT uType);
		BOOL  IsRaw(UINT uOffset);
		DWORD GetReal(UINT uIndex, UINT uChars);
		DWORD GetInt(UINT uIndex, UINT uChars, BOOL fRaw);
		DWORD Convert(DWORD Data, UINT uIndex);
		BYTE  ByteFromAscii(BYTE bByte);
		BOOL  IsError(void);
		BOOL  IsAck(void);
		UINT  FindData(UINT uIndex);
		UINT  GetChars(UINT uOffset);

	};

// End of File

