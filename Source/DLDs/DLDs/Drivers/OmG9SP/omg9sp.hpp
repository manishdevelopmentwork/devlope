//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP Serial Master Driver
//

class COmronFinsG9spMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		COmronFinsG9spMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(void ) Service(void);

				
	protected:
		BYTE   m_bRx  [256];
		BYTE   m_bTx  [256];
		BYTE   m_bBuff[256];
		UINT   m_uData;
		UINT   m_uPoll;
		UINT   m_uTime;
		UINT   m_uPtr;
		UINT   m_uCheck;
		BOOL   m_fConn;

		// Transport Layer
		BOOL Transact(void);
		BOOL Send(void);
		BOOL RecvFrame(void);
		BOOL CheckFrame(void);

		// Implementation
		void Start(void);
		void AddByte(BYTE bByte);
		void AddData(void);
		void AddCheck(void);
		void End(void);
		UINT GetData(AREF Addr, PDWORD pData, UINT uCount);
		void PutHeader(void);
		void PutData(PDWORD pData, UINT uCount, UINT uType);

		void GetLongsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetWordsAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetBytesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetNibblesAt(PDWORD pData, UINT uAt, UINT uIndex, UINT uCount);
		void GetBitsAt (PDWORD pData, UINT uAt, UINT uIdnex, UINT uCount);

		// Helpers
		BOOL IsTimedOut(void);
		BOOL IsWriteOnly(UINT uTable, UINT uOffset);

	};

// End of File
