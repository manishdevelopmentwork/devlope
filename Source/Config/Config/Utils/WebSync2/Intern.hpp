
#pragma once

#define	_WIN32_WINNT		_WIN32_WINNT_WIN7

#define	WINVER			_WIN32_WINNT_WIN7

#define	NO_STRICT		TRUE

#include <Windows.h>
#include <malloc.h>
#include <wininet.h>

using namespace std;

#include <string>
#include <vector>
#include <map>

typedef BYTE const * PCBYTE;

typedef vector<BYTE> bytes;

#define global

#include "Printf.hpp"

#pragma comment(lib, "Ws2_32.lib")

#pragma comment(lib, "wininet")

#pragma comment(lib, "rasapi32")
