
//////////////////////////////////////////////////////////////////////////
//
// EtherTrak3 Modbus TCP Driver
//

#include "E3ModbusTCP.hpp"

// Instantiator

INSTANTIATE(CE3ModbusTCPDriver);

// Constructor

CE3ModbusTCPDriver::CE3ModbusTCPDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_uKeep     = 0;

	m_pCtx      = NULL;

	m_uMaxWords = 120;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CE3ModbusTCPDriver::~CE3ModbusTCPDriver(void)
{
	}

// Device

CCODE MCALL CE3ModbusTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_PingReg	= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fFlipLong	= GetByte(pData);
			m_pCtx->m_fFlipReal	= GetByte(pData);

			m_pCtx->m_IP1		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_fDisable15	= FALSE;
			m_pCtx->m_fDisable16	= TRUE;
			m_pCtx->m_fDisable5	= FALSE;
			m_pCtx->m_fDisable6	= FALSE;			
			m_pCtx->m_fNoReadEx	= FALSE;
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			m_pCtx->m_uMax01	= 512;
			m_pCtx->m_uMax02	= 512;
			m_pCtx->m_uMax03	= 32;
			m_pCtx->m_uMax04	= 32;
			m_pCtx->m_uMax15	= 512;
			m_pCtx->m_uMax16	= 32;

			m_pCtx->m_fDirty	= FALSE;
			m_pCtx->m_fAux          = FALSE;
			m_pCtx->m_IP2		= 0;

			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

// Entry Points

CCODE MCALL CE3ModbusTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address = Addr;

	if( Address.a.m_Table > addrNamed ) {

		Address.a.m_Table -= addrNamed;
		}

	return CModbusTCPMaster::Read(Address, pData, uCount);
	}

CCODE MCALL CE3ModbusTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	CAddress Address = Addr;

	if( Address.a.m_Table > addrNamed ) {

		Address.a.m_Table -= addrNamed;
		}

	return CModbusTCPMaster::Write(Address, pData, uCount);
	}

// End of File
