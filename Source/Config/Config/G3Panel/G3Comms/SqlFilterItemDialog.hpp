
#include "Intern.hpp"

#include "SqlFilter.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Item Dialog
//

class CSqlFilterItemDialog : public CItemDialog
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CSqlFilterItemDialog(void);
		CSqlFilterItemDialog(CItem *pItem, CString Caption);

	protected:
		// Data Members
		CSqlFilter *m_pFilter;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handler
		BOOL OnOkay(UINT uId);

	};

// End of File
