
#include "Intern.hpp"

#include "Driver.hpp"

#include "Align.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Driver Base Object
//

// Constructor

CDriver::CDriver(void)
{
	m_Ident = 0x0000;

	m_Flags = 0x0000;

	StdSetRef();
	}

// IUnknown

HRESULT CDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDriver);

	StdQueryInterface(IDriver);

	return E_NOINTERFACE;
	}

ULONG CDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CDriver::Release(void)
{
	StdRelease();
	}

// IDriver

WORD METHOD CDriver::GetIdentifier(void)
{
	return m_Ident;
	}

WORD METHOD CDriver::GetCategory(void)
{
	return DC_NULL;
	}

WORD METHOD CDriver::GetFlags(void)
{
	return m_Flags;
	}

// Data Handlers

IDataHandler * CDriver::MakeSingleDataHandler(void)
{
	IDataHandler *pHandler = NULL;

	AfxNewObject("data-s", IDataHandler, pHandler);

	return pHandler;
	}

IDataHandler * CDriver::MakeDoubleDataHandler(void)
{
	IDataHandler *pHandler = NULL;

	AfxNewObject("data-d", IDataHandler, pHandler);

	return pHandler;
	}

// Operating System

PVOID CDriver::Alloc(UINT uSize)
{
	return ::malloc(uSize);
	}

void CDriver::Free(PVOID lpData)
{
	return ::free(lpData);
	}

ISocket * CDriver::CreateSocket(WORD wProt)
{
	ISocket *pSock = NULL;

	if( wProt == IP_TCP ) {

		AfxNewObject("sock-tcp", ISocket, pSock);
		}

	if( wProt == IP_UDP ) {

		AfxNewObject("sock-udp", ISocket, pSock);
		}

	if( wProt == IP_RAW ) {

		AfxNewObject("sock-raw", ISocket, pSock);
		}

	AfxAssert(pSock);

	return pSock;
	}

CBuffer * CDriver::CreateBuffer(UINT uSize, BOOL fFlag)
{
	return BuffAllocate(uSize);
	}

UINT CDriver::CheckIP(IPREF IP, UINT uLimit)
{
	return 10;
	}

void CDriver::ForceSleep(UINT uTime)
{
	::ForceSleep(uTime);
	}

// Init Helpers

BYTE CDriver::GetByte(PCBYTE &pData)
{
	BYTE x = *PCBYTE(pData);

	pData += sizeof(x);

	return x;
	}

WORD CDriver::GetWord(PCBYTE &pData)
{
	Item_Align2(pData);

	WORD x = MotorToHost(*PCWORD(pData));

	pData += sizeof(x);

	return x;
	}

DWORD CDriver::GetLong(PCBYTE &pData)
{
	Item_Align4(pData);

	DWORD x = MotorToHost(*PCDWORD(pData));

	pData += sizeof(x);

	return x;
	}

DWORD CDriver::GetAddr(PCBYTE &pData)
{
	Item_Align4(pData);

	DWORD x = *PCDWORD(pData);

	pData += sizeof(x);

	return x;
	}

PTXT CDriver::GetString(PCBYTE &pData)
{
	WORD wLength = GetWord(pData);

	PTXT pString = PTXT(Alloc(wLength + 1));

	memset(pString, 0, wLength + 1);

	for( UINT n = 0; n < wLength; n++ ) {

		pString[n] = TCHAR(GetWord(pData));
		}

	return pString;
	}

PCBYTE CDriver::GetData(PCBYTE &pData, UINT uSize)
{
	PCBYTE pWork = pData;

	pData += uSize;

	return pWork;
	}

// Additional Helpers

BOOL CDriver::MoreHelp(WORD ID, void **pHelp)
{
	IHelper *pHelper = NULL;

	AfxGetObject("driver-helper", 0, IHelper, pHelper);

	if( pHelper ) {

		if( pHelper->MoreHelp(ID, pHelp) ) {

			pHelper->Release();

			return TRUE;
			}

		pHelper->Release();
		}

	pHelp = NULL;

	return FALSE;
	}

// End of File
