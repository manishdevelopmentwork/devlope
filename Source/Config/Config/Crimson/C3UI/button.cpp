
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Action Button
//

// Runtime Class

AfxImplementRuntimeClass(CUIButton, CUIControl);

// Constructor

CUIButton::CUIButton(PCTXT pLabel, PCTXT pTip, UINT ID)
{
	m_Label          = pLabel;

	m_UIData.m_Label = m_Label;

	m_UIData.m_Tip   = pTip;

	m_ID             = ID;

	m_pCtrlLayout    = NULL;

	m_pButton        = New CHotLinkCtrl;
	}

CUIButton::CUIButton(PCTXT pLabel, PCTXT pTip, PCTXT pTag)
{
	m_Label          = pLabel;

	m_UIData.m_Label = m_Label;

	m_UIData.m_Tip   = pTip;

	m_UIData.m_Tag   = pTag;

	m_ID             = 0;

	m_pCtrlLayout    = NULL;

	m_pButton        = New CHotLinkCtrl;
	}

// Core Overridables

void CUIButton::OnLayout(CLayFormation *pForm)
{
	m_pCtrlLayout = New CLayItemText( m_Label,
					  3,
					  0
					  );

	m_pMainLayout = New CLayFormPad(  m_pCtrlLayout,
					  CRect(2, 1, 2, 1),
					  horzNone | vertNone
					  );

	pForm->AddItem(m_pMainLayout);
	}

void CUIButton::OnCreate(CWnd &Wnd, UINT &uID)
{
	if( m_ID ) {

		m_pButton->Create( m_Label,
				   BS_PUSHBUTTON | WS_TABSTOP,
				   m_pCtrlLayout->GetRect(),
				   Wnd,
				   m_ID
				   );

		m_pButton->SetFont(afxFont(Dialog));

		AddControl(m_pButton);
		}
	else {
		m_pButton->Create( m_Label,
				   BS_PUSHBUTTON | WS_TABSTOP,
				   m_pCtrlLayout->GetRect(),
				   Wnd,
				   uID++
				   );

		m_pButton->SetFont(afxFont(Dialog));

		AddControl(m_pButton);
		}
	}

void CUIButton::OnPosition(void)
{
	m_pButton->MoveWindow(m_pCtrlLayout->GetRect(), TRUE);
	}

// Notification Handlers

BOOL CUIButton::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pButton->GetID() ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
