
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Windows Thread Object
//

// Dynamic Class

AfxImplementDynamicClass(CThread, CCmdTarget);

// Static Data

static TLS CThread * m_pThis = NULL;

// Object Location

CThread * CThread::FindInstance(void)
{
	return m_pThis;
	}

// Constructor

CThread::CThread(void)
{
	m_pThread    = NULL;

	m_pMainWnd   = NULL;

	m_uLockCount = 0;

	m_nWaitCount = 0;

	m_WaitCursor.Create(IDC_WAIT);
	}

// Destructor

CThread::~CThread(void)
{
	delete m_pThread;
	}

// Creation

void CThread::Create(void)
{
	AfxAssert(!m_pThread);

	m_pThread = New CThreadHelper(this);

	m_pThread->Create();
	}

void CThread::CreateSuspended(void)
{
	AfxAssert(!m_pThread);

	m_pThread = New CThreadHelper(this);

	m_pThread->CreateSuspended();
	}

// Execution

UINT CThread::Execute(void)
{
	AfxAssert(!m_pThis);

	m_pThis = this;

	try {
		UINT uResult = 1;

		if( OnInitialize() ) {

			uResult = OnExecute();

			OnTerminate();
			}

		m_pThis = NULL;

		return uResult;
		}

	catch(CException &) {
		
		AfxTrace(L"ERROR: Exception in CThread::Execute\n");

		AfxAbort();
		}

	return 0;
	}

// Helper Entries

BOOL CThread::HelperInit(void)
{
	AfxAssert(!m_pThis);

	m_pThis = this;

	return OnInitialize();
	}

UINT CThread::HelperExec(void)
{
	return OnExecute();
	}

void CThread::HelperTerm(void)
{
	OnTerminate();

	m_pThis = NULL;
	}

// Raw Thread Access

CRawThread & CThread::GetRawThread(void) const
{
	AfxAssert(m_pThread);

	return *m_pThread;
	}

// Attributes

CWnd * CThread::GetMainWnd(void) const
{
	return m_pMainWnd;
	}

CString CThread::GetStatusText(void)
{
	CString Text;
	
	if( m_pMainWnd ) {

		m_pMainWnd->SendMessage( WM_GETSTATUSBAR,
					 WPARAM(&Text),
					 0
					 );
		}
	
	return Text;
	}

// Operations

void CThread::ClearMainWnd(void)
{
	m_pMainWnd = NULL;
	}

void CThread::SetMainWnd(CWnd &MainWnd)
{
	AfxAssert(!m_pMainWnd);

	AfxValidateObject(MainWnd);

	m_pMainWnd = &MainWnd;

	afxStdTools->SetMainWnd();
	}

void CThread::SetStatusText(PCTXT pText)
{
	if( m_pMainWnd ) {

		m_pMainWnd->SendMessage( WM_SETSTATUSBAR,
					 WPARAM(pText),
					 0
					 );
		}
	}

void CThread::LockUpdate(BOOL fLock)
{
	if( fLock ) {

		if( !m_uLockCount++ ) {
		
			if( m_pMainWnd ) {
			
				HWND hWnd = m_pMainWnd->GetHandle();
				
				LockWindowUpdate(hWnd);
				}
			}
		}
	else {
		AfxAssert(m_uLockCount);
		
		if( !--m_uLockCount ) {
		
			if( afxMainWnd ) {
			
				HWND hWnd = NULL;
				
				LockWindowUpdate(hWnd);
				}
			}
		}
	}

// Hour-Glass Cursor Management

BOOL CThread::InWaitMode(void) const
{
	return m_nWaitCount > 0;
	}
	
int CThread::GetWaitCount(void) const
{
	return m_nWaitCount;
	}
	
void CThread::SetWaitMode(BOOL fWait)
{
	if( fWait ) {

		AfxAssert(m_nWaitCount < 100);
	
		if( !m_nWaitCount++ ) {
			
			SetWaitCursor();
		
			ShowCursor(TRUE);
			}
		}
	else {
		AfxAssert(m_nWaitCount > 0);
			
		if( !--m_nWaitCount ) {
		
			ShowCursor(FALSE);

			SetCursor(LoadCursor(NULL, IDC_ARROW));

			CPoint Pos;
				
			GetCursorPos(Pos);
				
			HWND   hWnd     = WindowFromPoint(Pos);
				
			UINT   uHitCode = UINT(SendMessage(hWnd, WM_NCHITTEST, 0, LPARAM(Pos)));
				
			LPARAM lParam   = MAKELONG(uHitCode, WM_MOUSEMOVE);
				
			SendMessage(hWnd, WM_SETCURSOR, WPARAM(hWnd), lParam);
			}
		}
	}

int CThread::SuspendWaitMode(void)
{
	int nCount = m_nWaitCount;
	
	if( m_nWaitCount ) {
	
		m_nWaitCount = 1;
		
		SetWaitMode(FALSE);
		}
		
	return nCount;
	}

void CThread::RestoreWaitMode(int nCount)
{
	AfxAssert(!m_nWaitCount);
	
	if( nCount ) {
		
		SetWaitMode(TRUE);
		
		m_nWaitCount = nCount;
		}
	}

void CThread::SetWaitCursor(void)
{
	SetCursor(m_WaitCursor);
	}

// Routing Control

BOOL CThread::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	return CCmdTarget::OnRouteMessage(Message, lResult);
	}

// Core Overridables

BOOL CThread::OnInitialize(void)
{
	return TRUE;
	}

UINT CThread::OnExecute(void)
{
	return MessageLoop();
	}

BOOL CThread::OnIdle(UINT uCount)
{
	return FALSE;
	}
	
BOOL CThread::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CThread::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

void CThread::OnTerminate(void)
{
	delete this;
	}

// Implementation

UINT CThread::MessageLoop(void)
{
	if( m_pMainWnd ) {

		BOOL fIdle = TRUE;

		UINT uIdle = 0;

		for(;;) {

			MSG Msg;

			while( fIdle && !IsMessageReady() ) {

				afxMap->Lock();

				try {
					fIdle = OnIdle(uIdle);

					uIdle = uIdle + 1;
					}

				catch(CException &Exception) {

					AfxTrace(L"ERROR: Exception in OnIdle\n");
					
					OnException(Exception);
					}

				if( !fIdle ) {

					SendGoingIdle();
					}

				afxMap->Unlock();
				}

			afxMap->Lock();

			do {
				GetMessage(&Msg, NULL, NULL, NULL);

				if( Msg.message == WM_QUIT ) {
				
					afxMap->Unlock();

					return 0;
					}

				PumpMessage(Msg);

				if( !IsTrivialMessage(Msg) ) {

					uIdle = 0;

					fIdle = TRUE;
					}

				} while( IsMessageReady() );

			afxMap->Unlock();
			}
		}

	return 1;
	}

BOOL CThread::IsMessageReady()
{
	UINT uFlags = PM_NOREMOVE;

	MSG Msg;

	return PeekMessage(&Msg, NULL, NULL, NULL, uFlags);
	}

BOOL CThread::IsTrivialMessage(MSG &Msg)
{
	if( Msg.message == WM_MOUSEMOVE || Msg.message == WM_NCMOUSEMOVE ) {

		if( m_uLastMsg == Msg.message ) {
		
			CPoint ThisPos;

			GetCursorPos(&ThisPos);

			if( m_LastPos == ThisPos ) {

				return TRUE;
				}
			}

		m_uLastMsg = Msg.message;

		GetCursorPos(&m_LastPos);
		}

	if( Msg.message == WM_PAINT ) {

		return TRUE;
		}

	if( Msg.message == 0x0118 ) {

		return TRUE;
		}

	return FALSE;
	}

void CThread::PumpMessage(MSG &Msg)
{
	try {
		switch( Msg.message ) {
		
			case WM_KEYDOWN:
			case WM_KEYUP:
			case WM_SYSKEYDOWN:
			case WM_SYSKEYUP:
				
				if( afxMainWnd->RouteAccelerator(Msg) ) {

					return;
					}
				break;
			}
	
		if( !OnTranslateMessage(Msg) ) {

			TranslateMessage(&Msg);

			DispatchMessage(&Msg);
			}
		}

	catch(CException &Exception) {

		AfxTrace(L"ERROR: Exception in PumpMessage\n");
					
		OnException(Exception);
		}
	}

void CThread::SendGoingIdle(void)
{
	try {
		MSG Msg = { NULL, WM_GOINGIDLE, 0, 0 };

		LRESULT lResult = 0;

		afxMainWnd->RouteMessage(Msg, lResult);
		}

	catch(CException &Exception) {

		AfxTrace(L"ERROR: Exception in SendGoingIdle\n");
					
		OnException(Exception);
		}
	}

// End of File
