
#include "Intern.hpp"

#include "TimeSync.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Time Sync Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CTimeSync, CServiceItem);

// Constructor

CTimeSync::CTimeSync(void)
{
	m_pEnable   = NULL;
	m_pExpose   = NULL;
	m_pSyncUDP  = NULL;
	m_pSyncDST  = NULL;
	m_pDHCP     = NULL;
	m_pServer   = NULL;
	m_pSyncGPS  = NULL;
	m_pSyncGSM  = NULL;
	m_pDelay    = NULL;
	m_pLogMode  = NULL;
	m_pLogSync  = NULL;
	m_pZulu     = NULL;
	m_pLogSrc   = NULL;
}

// Initial Values

void CTimeSync::SetInitValues(void)
{
	SetInitial(L"Enable", m_pEnable, 0);
	SetInitial(L"Expose", m_pExpose, 0);
	SetInitial(L"SyncUDP", m_pSyncUDP, 0);
	SetInitial(L"SyncDST", m_pSyncDST, 0);
	SetInitial(L"DHCP", m_pDHCP, 0);
	SetInitial(L"Server", m_pServer, 0x81060F1C);
	SetInitial(L"SyncGPS", m_pSyncGPS, 0);
	SetInitial(L"SyncGSM", m_pSyncGSM, 0);
	SetInitial(L"Delay", m_pDelay, 480);
	SetInitial(L"LogMode", m_pLogMode, 0);
	SetInitial(L"LogSync", m_pLogSync, 0);
	SetInitial(L"Zulu", m_pZulu, 0);
}

// UI Creation

BOOL CTimeSync::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIStdPage(AfxPointerClass(this), 1));

	return FALSE;
}

// UI Update

void CTimeSync::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == "SyncGPS" || Tag == "SyncGSM" || Tag == "SyncUDP" ) {

		DoEnables(pHost);
	}

	if( Tag == "Enable" || Tag == "DHCP" || Tag == "LogMode" ) {

		DoEnables(pHost);
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CTimeSync::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Server" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"Delay" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	return FALSE;
}

// Attributes

BOOL CTimeSync::IsEnabled(void) const
{
	return FALSE;
}

UINT CTimeSync::GetTreeImage(void) const
{
	return IDI_TIME;
}

// Download Support

BOOL CTimeSync::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(5));

	CServiceItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pEnable);
	Init.AddItem(itemVirtual, m_pExpose);
	Init.AddItem(itemVirtual, m_pSyncUDP);
	Init.AddItem(itemVirtual, m_pSyncDST);
	Init.AddItem(itemVirtual, m_pDHCP);
	Init.AddItem(itemVirtual, m_pServer);
	Init.AddItem(itemVirtual, m_pSyncGPS);
	Init.AddItem(itemVirtual, m_pSyncGSM);
	Init.AddItem(itemVirtual, m_pDelay);
	Init.AddItem(itemVirtual, m_pLogMode);
	Init.AddItem(itemVirtual, m_pLogSync);
	Init.AddItem(itemVirtual, m_pZulu);
	Init.AddItem(itemVirtual, m_pLogSrc);

	return TRUE;
}

// Meta Data Creation

void CTimeSync::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Expose);
	Meta_AddVirtual(SyncUDP);
	Meta_AddVirtual(SyncDST);
	Meta_AddVirtual(DHCP);
	Meta_AddVirtual(Server);
	Meta_AddVirtual(SyncGPS);
	Meta_AddVirtual(SyncGSM);
	Meta_AddVirtual(Delay);
	Meta_AddVirtual(LogMode);
	Meta_AddVirtual(LogSync);
	Meta_AddVirtual(Zulu);
	Meta_AddVirtual(LogSrc);

	Meta_SetName((IDS_TIME_MANAGER));
}

// Implementation

void CTimeSync::DoEnables(IUIHost *pHost)
{
	BOOL fEnable  = CheckEnable(m_pEnable, L">=", 1, TRUE);

	BOOL fSyncGSM = CheckEnable(m_pSyncGSM, L">=", 1, TRUE);

	BOOL fSyncGPS = CheckEnable(m_pSyncGPS, L">=", 1, TRUE);

	BOOL fSyncUDP = CheckEnable(m_pSyncUDP, L">=", 1, TRUE);

	BOOL fLogMode = CheckEnable(m_pLogMode, L">=", 1, TRUE);

	BOOL fDHCP    = CheckEnable(m_pDHCP, L">=", 1, TRUE);

	BOOL fSync    = (fSyncUDP || fSyncGSM || fSyncGPS);

	pHost->EnableUI("Expose", fEnable);

	pHost->EnableUI("SyncGPS", fEnable);

	pHost->EnableUI("SyncGSM", fEnable);

	pHost->EnableUI("SyncUDP", fEnable);

	pHost->EnableUI("SyncDST", fEnable && fSyncUDP);

	pHost->EnableUI("DHCP", fEnable && fSyncUDP);

	pHost->EnableUI("Server", fEnable && fSyncUDP && !fDHCP);

	pHost->EnableUI("Delay", fEnable && fSync);

	pHost->EnableUI("Zulu",    /*fEnable &&*/ !fLogMode);

	pHost->EnableUI("LogSync", fLogMode);

	pHost->EnableUI("LogSrc", fLogMode);
}

// End of File
