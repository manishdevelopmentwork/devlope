
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Image List
//

// Dynamic Class

AfxImplementDynamicClass(CImageList, CObject);

// Constructors

CImageList::CImageList(void)
{
	m_hList   = NULL;

	m_fExtern = TRUE;
	}

CImageList::CImageList(CImageList const &That)
{
	CheckException(Create(That));
	}

CImageList::CImageList(PCTXT pName, int xSize, UINT uExtra, UINT uType, UINT uFlags)
{
	CheckException(Create(pName, xSize, uExtra, uType, uFlags));
	}

CImageList::CImageList(CSize const &Size, UINT uFlags, UINT uInitial, UINT uExtra)
{
	CheckException(Create(Size, uFlags, uInitial, uExtra));
	}

CImageList::CImageList(int xSize, int ySize, UINT uFlags, UINT uInitial, UINT uExtra)
{
	CheckException(Create(xSize, ySize, uFlags, uInitial, uExtra));
	}

CImageList::CImageList(UINT uFlags, UINT uInitial, UINT uExtra)
{
	CheckException(Create(uFlags, uInitial, uExtra));
	}

CImageList::CImageList(IStream *pStream)
{	
	CheckException(Create(pStream));
	}

// Destructor

CImageList::~CImageList(void)
{
	Detach(TRUE);
	}

// Creation

BOOL CImageList::Create(CImageList const &That)
{
	return Attach(ImageList_Duplicate(That.m_hList));
	}

BOOL CImageList::Create(PCTXT pName, int xSize, UINT uExtra, UINT uType, UINT uFlags)
{
	return Attach(ImageList_LoadImage(afxModule->LocateImage(pName, uType), pName, xSize, uExtra, afxColor(MASK), uType, uFlags));
	}

BOOL CImageList::Create(CSize const &Size, UINT uFlags, UINT uInitial, UINT uExtra)
{
	return Attach(ImageList_Create(Size.cx, Size.cy, uFlags, uInitial, uExtra));
	}

BOOL CImageList::Create(int xSize, int ySize, UINT uFlags, UINT uInitial, UINT uExtra)
{
	return Attach(ImageList_Create(xSize, ySize, uFlags, uInitial, uExtra));
	}

BOOL CImageList::Create(UINT uFlags, UINT uInitial, UINT uExtra)
{
	return Attach(ImageList_Create(32, 32, uFlags, uInitial, uExtra));
	}

BOOL CImageList::Create(IStream *pStream)
{
	return Attach(ImageList_Read(pStream));
	}

// Attachment

BOOL CImageList::Attach(HIMAGELIST hList)
{
	if( hList ) {

		Detach(TRUE);

		m_hList   = hList;

		m_fExtern = FALSE;

		afxMap->InsertPerm(hList, NS_HIMAGELIST, this);

		return TRUE;
		}

	return FALSE;
	}

void CImageList::Detach(BOOL fDelete)
{
	if( m_hList ) {

		if( fDelete ) {
			
			if( !m_fExtern ) {

				ImageList_Destroy(m_hList);
				}
			}

		afxMap->RemoveBoth(m_hList, NS_HIMAGELIST, this);

		m_hList   = NULL;

		m_fExtern = TRUE;
		}
	}

// Conversion

CImageList::operator HIMAGELIST (void) const
{
	return m_hList;
	}

// Attributes

HIMAGELIST CImageList::GetHandle(void) const
{
	return m_hList;
	}

CIcon & CImageList::ExtractIcon(UINT uImage) const
{
	return CIcon::FromHandle(ImageList_ExtractIcon(NULL, m_hList, uImage));
	}

CColor CImageList::GetBkColor(void) const
{
	return CColor(ImageList_GetBkColor(m_hList));
	}

CIcon & CImageList::GetIcon(UINT uImage, UINT uFlags) const
{
	return CIcon::FromHandle(ImageList_GetIcon(m_hList, uImage, uFlags));
	}

CSize CImageList::GetIconSize(void) const
{
	int xSize, ySize;

	ImageList_GetIconSize(m_hList, &xSize, &ySize);

	return CSize(xSize, ySize);
	}

UINT CImageList::GetImageCount(void) const
{
	return ImageList_GetImageCount(m_hList);
	}

BOOL CImageList::GetImageInfo(UINT uImage, IMAGEINFO &Info) const
{
	return ImageList_GetImageInfo(m_hList, uImage, &Info);
	}

// Operations

UINT CImageList::Add(CBitmap const &Image, CBitmap const &Mask)
{
	return ImageList_Add(m_hList, Image, Mask);
	}

UINT CImageList::Add(CBitmap const &Image)
{
	return ImageList_Add(m_hList, Image, NULL);
	}

UINT CImageList::AddCursor(CCursor const &Cursor)
{
	return ImageList_AddIcon(m_hList, Cursor);
	}

UINT CImageList::AddIcon(CIcon const &Icon)
{
	return ImageList_AddIcon(m_hList, Icon);
	}

UINT CImageList::AddMasked(CBitmap const &Image, CColor const &Color)
{
	return ImageList_AddMasked(m_hList, Image, Color);
	}

UINT CImageList::AddMasked(CBitmap const &Image)
{
	return ImageList_AddMasked(m_hList, Image, afxColor(MASK));
	}

BOOL CImageList::BeginDrag(UINT uImage, CPoint const &Pos)
{
	return ImageList_BeginDrag(m_hList, uImage, Pos.x, Pos.y);
	}

BOOL CImageList::BeginDrag(UINT uImage, int xPos, int yPos)
{
	return ImageList_BeginDrag(m_hList, uImage, xPos, yPos);
	}

BOOL CImageList::BeginDrag(UINT uImage)
{
	int xSize, ySize;

	ImageList_GetIconSize(m_hList, &xSize, &ySize);

	return ImageList_BeginDrag(m_hList, uImage, xSize / 2, ySize / 2);
	}

BOOL CImageList::Copy(UINT uDest, UINT uSource, UINT uFlags)
{
	return ImageList_Copy(m_hList, uDest, m_hList, uSource, uFlags);
	}

BOOL CImageList::Draw(UINT uImage, HDC hDC, CPoint const &Pos, UINT uStyle)
{
	return ImageList_Draw(m_hList, uImage, hDC, Pos.x, Pos.y, uStyle);
	}

BOOL CImageList::Draw(UINT uImage, HDC hDC, int xPos, int yPos, UINT uStyle)
{
	return ImageList_Draw(m_hList, uImage, hDC, xPos, yPos, uStyle);
	}

BOOL CImageList::DrawEx(UINT uImage, HDC hDC, CPoint const &Pos, CSize const &Size, CColor const &Back, CColor const &Fore, UINT uStyle)
{
	return ImageList_DrawEx(m_hList, uImage, hDC, Pos.x, Pos.y, Size.cx, Size.cy, Back, Fore, uStyle);
	}

BOOL CImageList::DrawEx(UINT uImage, HDC hDC, CRect const &Rect, CColor const &Back, CColor const &Fore, UINT uStyle)
{
	return ImageList_DrawEx(m_hList, uImage, hDC, Rect.left, Rect.top, Rect.cx(), Rect.cy(), Back, Fore, uStyle);
	}

BOOL CImageList::DrawEx(UINT uImage, HDC hDC, int xPos, int yPos, int xSize, int ySize, CColor const &Back, CColor const &Fore, UINT uStyle)
{
	return ImageList_DrawEx(m_hList, uImage, hDC, xPos, yPos, xSize, ySize, Back, Fore, uStyle);
	}

BOOL CImageList::Remove(UINT uImage)
{
	return ImageList_Remove(m_hList, uImage);
	}

BOOL CImageList::RemoveAll(void)
{
	return ImageList_Remove(m_hList, -1);
	}

BOOL CImageList::Replace(UINT uIndex, CBitmap const &Image, CBitmap const &Mask)
{
	return ImageList_Replace(m_hList, uIndex, Image, Mask);
	}

BOOL CImageList::Replace(UINT uIndex, CBitmap const &Image)
{
	return ImageList_Replace(m_hList, uIndex, Image, NULL);
	}

UINT CImageList::ReplaceCursor(UINT uIndex, CCursor const &Cursor)
{
	return ImageList_ReplaceIcon(m_hList, uIndex, Cursor);
	}

UINT CImageList::ReplaceIcon(UINT uIndex, CIcon const &Icon)
{
	return ImageList_ReplaceIcon(m_hList, uIndex, Icon);
	}

void CImageList::SetBkColor(CColor const &Color)
{
	ImageList_SetBkColor(m_hList, Color);
	}

BOOL CImageList::SetDragCursorImage(UINT uIndex, CPoint const &Pos)
{
	return ImageList_SetDragCursorImage(m_hList, uIndex, Pos.x, Pos.y);
	}

BOOL CImageList::SetDragCursorImage(UINT uIndex, int xPos, int yPos)
{
	return ImageList_SetDragCursorImage(m_hList, uIndex, xPos, yPos);
	}

void CImageList::SetExtern(BOOL fExtern)
{
	m_fExtern = fExtern;
	}

BOOL CImageList::SetIconSize(CSize const &Size)
{
	return ImageList_SetIconSize(m_hList, Size.cx, Size.cy);
	}

BOOL CImageList::SetIconSize(int xSize, int ySize)
{
	return ImageList_SetIconSize(m_hList, xSize, ySize);
	}

BOOL CImageList::SetImageCount(UINT uCount)
{
	return ImageList_SetImageCount(m_hList, uCount);
	}

BOOL CImageList::SetOverlayImage(UINT uImage, UINT uIndex)
{
	return ImageList_SetOverlayImage(m_hList, uImage, uIndex);
	}

BOOL CImageList::Write(IStream *pStream)
{
	return ImageList_Write(m_hList, pStream);
	}

// Drag Attributes

CImageList & CImageList::GetDragImage(void)
{
	return CImageList::FromHandle(ImageList_GetDragImage(NULL, NULL));
	}

// Drag Operations

BOOL CImageList::DragEnter(HWND hWnd, CPoint const &Pos)
{
	return ImageList_DragEnter(hWnd, Pos.x, Pos.y);
	}

BOOL CImageList::DragEnter(HWND hWnd, int xPos, int yPos)
{
	return ImageList_DragEnter(hWnd, xPos, yPos);
	}

BOOL CImageList::DragEnter(CPoint const &Pos)
{
	return ImageList_DragEnter(NULL, Pos.x, Pos.y);
	}

BOOL CImageList::DragEnter(int xPos, int yPos)
{
	return ImageList_DragEnter(NULL, xPos, yPos);
	}

BOOL CImageList::DragLeave(HWND hWnd)
{
	return ImageList_DragLeave(hWnd);
	}

BOOL CImageList::DragLeave(void)
{
	return ImageList_DragLeave(NULL);
	}

BOOL CImageList::DragMove(CPoint const &Pos)
{
	return ImageList_DragMove(Pos.x, Pos.y);
	}

BOOL CImageList::DragMove(int xPos, int yPos)
{
	return ImageList_DragMove(xPos, yPos);
	}

BOOL CImageList::DragShowNoLock(BOOL fShow)
{
	return ImageList_DragShowNolock(fShow);
	}

void CImageList::EndDrag(void)
{
	ImageList_EndDrag();
	}

// Handle Lookup

CImageList & CImageList::FromHandle(HIMAGELIST hList)
{
	if( hList ) {

		CImageList *pList = (CImageList *) afxMap->FromHandle(hList, NS_HIMAGELIST);

		if( pList == NULL ) {

			pList = AfxNewObject(CImageList, AfxStaticClassInfo());

			afxMap->InsertTemp(hList, NS_HIMAGELIST, pList);

			pList->m_hList = hList;
			}

		return *pList;
		}
	else {
		static CImageList NullObject;
	
		return NullObject;
		}
	}

// Implementation

void CImageList::CheckException(BOOL fCheck)
{
	if( !fCheck ) {
		
		AfxThrowResourceException();
		}
	}

// End of File
