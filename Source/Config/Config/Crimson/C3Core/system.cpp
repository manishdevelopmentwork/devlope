
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Item Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CSystemItem, CMetaItem);

// Constructor

CSystemItem::CSystemItem(void)
{
	}

// Destructor

CSystemItem::~CSystemItem(void)
{
	}

// Version Test

BOOL CSystemItem::NeedNewSoftware(void) const
{
	return FALSE;
	}

// Model Control

BOOL CSystemItem::SetModel(CString const &Model)
{
	return FALSE;
}

CString CSystemItem::GetModel(void) const
{
	return L"";
}

CString CSystemItem::GetEmulatorModel(void) const
{
	return L"";
}

CString CSystemItem::GetDisplayName(void) const
{
	return L"";
}

// Model Editing

BOOL CSystemItem::SetModelSpec(CString const &Model)
{
	return TRUE;
}

CString CSystemItem::GetModelSpec(void)
{
	return L"";
}

// Model Mapping

CString CSystemItem::GetProcessor(void) const
{
	return L"";
}

CString CSystemItem::GetModelList(void) const
{
	return L"";
	}

CString CSystemItem::GetModelInfo(CString Model) const
{
	return L"";
	}

// Conversion

CString CSystemItem::GetConvList(void) const
{
	return L"";
	}

void CSystemItem::PostConvert(void)
{
	}

CString CSystemItem::GetSpecies(void) const
{
	return L"";
	}

// Download Config

CString CSystemItem::GetDownloadConfig(void) const
{
	return L"Default";
	}

void CSystemItem::SetDownloadTarget(CString Target)
{
	}

// Device Config

UINT CSystemItem::GetSoftwareGroup(void) const
{
	return 0;
}

BOOL CSystemItem::HasExpansion(void) const
{
	return FALSE;
}

BOOL CSystemItem::GetDeviceConfig(CByteArray &Data, UINT uItem) const
{
	return FALSE;
}

BOOL CSystemItem::DisableDeviceConfig(UINT uItem)
{
	return FALSE;
}

// Extra Commands

UINT CSystemItem::GetExtraCount(void)
{
	return 0;
	}

CString CSystemItem::GetExtraText(UINT uCmd)
{
	return L"";
	}

BOOL CSystemItem::GetExtraState(UINT uCmd)
{
	return TRUE;
	}

BOOL CSystemItem::RunExtraCmd(CWnd &Wnd, UINT uCmd)
{
	return FALSE;
	}

// Validation

BOOL CSystemItem::HasBroken(void) const
{
	return FALSE;
	}

BOOL CSystemItem::HasCircular(void) const
{
	return FALSE;
	}

BOOL CSystemItem::HasHardware(void) const
{
	return FALSE;
}

BOOL CSystemItem::FindBroken(void)
{
	return FALSE;
	}

BOOL CSystemItem::FindCircular(void)
{
	return FALSE;
	}

void CSystemItem::Rebuild(UINT uAction)
{
	}

void CSystemItem::ShowHardware(void)
{
}

void CSystemItem::UpdateHardware(void)
{
}

// Control Project

BOOL CSystemItem::NeedBuild(void)
{
	return FALSE;
	}

BOOL CSystemItem::PerformBuild(void)
{
	return FALSE;
	}

// SQL Queries

BOOL CSystemItem::CheckSqlQueries(void)
{
	return FALSE;
	}

// Watch Window

CWnd * CSystemItem::GetWatchWindow(void)
{
	return NULL;
	}

BOOL CSystemItem::HasControl(void)
{
	return FALSE;
	}

// Searching

BOOL CSystemItem::GlobalFind(void)
{
	return FALSE;
	}

// End of File
