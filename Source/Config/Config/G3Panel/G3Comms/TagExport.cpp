
#include "Intern.hpp"

#include "TagExport.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CTVueTagImport.hpp"
#include "Tag.hpp"
#include "TagFolder.hpp"
#include "TagList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Tag Export Helpers
//

// Runtime Class

AfxImplementRuntimeClass(CTagExport, CObject);

// Constructor

CTagExport::CTagExport(CTagList *pTags)
{
	m_pTags = pTags;
	}

// Operations

BOOL CTagExport::Export(ITextStream &Stm, WCHAR cSep)
{
	if( FindTagClasses() ) {

		INDEX i = m_List.GetHead();

		while( !m_List.Failed(i) ) {

			CString Class = m_List[i];

			WriteClass(Stm, Class);

			if( FindClassCols(Class) ) {

				WriteCols(Stm, cSep);

				WriteTags(Stm, cSep, Class);
				}

			m_List.GetNext(i);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagExport::Import(ITextStream &Stm, WCHAR cSep, CLongArray &Lines)
{
	CStringArray Cols;

	CLASS Class = NULL;

	UINT  uLine = 0x100000;

	PTXT  sLine = New WCHAR [ uLine ];

	BOOL fGo    = FALSE;

	CCTVueTagImport    CTTI;

	CString sCustom = L"";

	UINT n;

	for( n = 1;; n++ ) {

		sLine[0] = 0;

		switch( m_Info.uState ) {

			case 0:
				fGo = Stm.GetLine(sLine, uLine);
				break;

			case 1:	// Initialize Custom Import

				if( m_Info.uIDS & CTPRESENT ) {

					if( (fGo = Stm.GetLine(sLine, uLine)) ) {

						if( CTTI.IsCTVueHeader(sLine) ) {

							CTTI.SetCTInfoPtr(&m_Info);
							}
						}

					else m_Info.uState = 0;
					}

				if( fGo ) {

					if( (BOOL)(m_Info.uIDS & CTSELECTED) ) {

						cSep = ',';

						m_Info.uState = 2;
						}
					}

				fGo = FALSE;

				continue;

			case 2: // process custom header lines.

				sLine[0] = 0;

				fGo = Stm.GetLine(sLine, uLine);

				if( fGo ) {

					if( IsCTSelected() ) {

						sCustom = CTTI.ProcessCTVueHeader(sLine);
						}

					if( sCustom[0] ) {

						m_Info.uState	= 4;

						m_Info.sTagLine	= sCustom;

						fGo	= TRUE;
						}
					}

				continue;

			case 3:	// get next line

				fGo = Stm.GetLine(sLine, uLine);

				if( fGo ) {

					switch( sLine[0] ) {

						case '#':
						case ';':
						case ',':
						case 0xA:
						case 0xD:
						case 0:
							break;

						default:
							m_Info.uState = 4;

							m_Info.sTagLine	= (CString)sLine;
							break;
						}
					}

				else {
					break;
					}

				continue;

			case 4:	// process entry for Class

				if( IsCTSelected() ) {

					sCustom = CTTI.GetCTVueLine();
					}

				else {
					sCustom	= L"";

					fGo	= FALSE;
					}

				if( sCustom.GetLength() ) {

					if( sCustom == L"No Device" ) {

						m_Info.uState = 3;

						Lines.Append(0);

						fGo = FALSE;
						}

					else {

						MakeUnicode(sLine, sCustom, TRUE);

						m_Info.uState = 5;
						}

					break;
					}

				break;

			case 5: // add column headers

				if( IsCTSelected() ) {

					sCustom = CTTI.GetColHeaders();
					}

				else {
					sCustom	= L"";

					fGo	= FALSE;
					}

				if( sCustom[0] ) {

					MakeUnicode(sLine, sCustom, TRUE);

					fGo = TRUE;

					m_Info.uState = 6;
					}

				break;

			case 6: // Process rest of entry

				 if( IsCTSelected() ) {

					sCustom = CTTI.GetCTVueTagData();
					}

				else {
					sCustom = L"";

					fGo	= FALSE;
					}

				if( sCustom[0] ) {

					MakeUnicode(sLine, sCustom, TRUE);

					fGo = TRUE;
					}

				m_Info.uState = 3;

				break;

			case 7:
				m_Info.uState = 0;

				continue;
			}

		if( fGo ) {

			UINT uUsed = wstrlen(sLine);

			while( uUsed ) {

				if( sLine[uUsed-1] == '\r' || sLine[uUsed-1] == '\n' || sLine[uUsed-1] == cSep ) {

					uUsed--;

					continue;
					}

				break;
				}

			if( uUsed ) {

				CStringArray Fields;

				CString Line(sLine, uUsed);

				Line.TrimRight();

				if( Line.IsEmpty() ) {

					continue;
					}

				Line.Tokenize(Fields, cSep);

				if( Fields[0][0] == '[' ) {

					UINT    uLen = Fields[0].GetLength();

					CString Full = Fields[0].Mid(1, uLen - 2);

					CString Name = L"CTag" + Full.TokenLeft('.');

					if( !(Class = AfxNamedClass(Name)) ) {

						Lines.Append(n);
						}

					Cols.Empty();

					continue;
					}

				if( !Class ) {

					continue;
					}

				if( Cols.IsEmpty() ) {

					UINT f;

					for( f = 1; f < Fields.GetCount(); f++ ) {

						CString Field = Fields[f];

						UINT    uPos  = Field.Find('/');

						if( uPos < NOTHING ) {

							Field = Field.Left(uPos-1) + L'\n' + Field.Mid(uPos+2);
							}
						else
							Field = L"Core\n" + Field;

						Cols.Append(Field);
						}

					continue;
					}

				CString Name = Fields[0];

				Name.TrimRight();

				Name.Replace(' ', '_');

				CTag   *pTag = NULL;

				if( CheckTagName(Name) ) {

					pTag = (CTag *) m_pTags->FindByName(Name);

					if( !pTag ) {

						pTag = CreateTag(Class, Name);
						}
					}

				if( pTag ) {

					CPrintf Text(CString(IDS_IMPORTING_FMT), Name);

					afxThread->SetStatusText(Text);

					CStringArray Props;

					for( UINT f = 1; f < Fields.GetCount(); f++ ) {

						if( !Fields[f].IsEmpty() ) {

							CString Prop = Cols[f-1] + L'\n';

							if( Fields[f] == L"((EMPTY))" ) {

								Prop += L"\"\"";
								}
							else
								Prop += Fields[f];

							Props.Append(Prop);
							}
						}

					// TODO -- Should check failures flag here.

					// TODO -- Anyway to make fails code have a WAS?

					if( CUIGetSet().SetProps(pTag, Props) ) {

						continue;
						}
					}

				Lines.Append(n);
				}

			continue;
			}

		break;
		}

	delete [] sLine;

	afxThread->SetStatusText(L"");

	return Lines.IsEmpty();
	}

BOOL CTagExport::SetCustomImport(UINT uIDS, CString sFileName, CCommsSystem *pCSys)
{
	m_Info.uState	= uIDS ? 1 : 0;

	m_Info.uIDS	= 0;

	if( !uIDS ) return TRUE;	// not a custom tag import file

	sFileName.MakeUpper();

	if( !ParseFileName(sFileName) ) {

		return FALSE;
		}

	m_Info.uIDS	= uIDS;
	m_Info.pCSys	= pCSys;

	return TRUE;
	}

// Implementation

BOOL CTagExport::FindTagClasses(void)
{
	afxThread->SetStatusText(CString(IDS_FINDING_TAG));

	INDEX Tag = m_pTags->GetHead();

	while( !m_pTags->Failed(Tag) ) {

		CTag *pTag = m_pTags->GetItem(Tag);

		if( !pTag->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

			m_List.Insert(pTag->GetExportClass());
			}

		m_pTags->GetNext(Tag);
		}

	afxThread->SetStatusText(L"");

	return !m_List.IsEmpty();
	}

BOOL CTagExport::FindClassCols(CString Class)
{
	afxThread->SetStatusText(CPrintf(CString(IDS_FINDING_COLUMNS), Class));

	m_ColDict.Empty();

	m_ColList.Empty();

	INDEX a = NULL;

	INDEX t = m_pTags->GetHead();

	while( !m_pTags->Failed(t) ) {

		CTag *pTag = m_pTags->GetItem(t);

		if( pTag->GetExportClass() == Class ) {

			if( !IsPrivate(pTag) ) {

				CStringArray Props;

				CUIGetSet().GetProps(Props, pTag);

				for( UINT p = 0; p < Props.GetCount(); p++ ) {

					CString n = MakeName(Props[p]);

					INDEX   d = m_ColDict.FindName(n);

					if( m_ColDict.Failed(d) ) {

						INDEX b = a;

						m_ColList.GetNext(b);

						a = m_ColList.Insert(b, n);

						m_ColDict.Insert(n, a);
						}
					else
						a = m_ColDict.GetData(d);
					}
				}
			}

		m_pTags->GetNext(t);
		}

	afxThread->SetStatusText(L"");

	return !m_ColDict.IsEmpty();
	}

BOOL CTagExport::BuildPropMap(CStringArray const &Props)
{
	m_Map.Empty();

	for( UINT p = 0; p < Props.GetCount(); p++ ) {

		CString n = MakeName(Props[p]);

		INDEX   d = m_ColDict.FindName(n);

		if( !m_ColDict.Failed(d) ) {

			INDEX c = m_ColDict.GetData(d);

			m_Map.Insert(c, p);
			}
		}

	return !m_Map.IsEmpty();
	}

void CTagExport::WriteClass(ITextStream &Stm, CString Class)
{
	Stm.PutLine(CPrintf(L"\r\n[%s]\r\n\r\n", Class));
	}

void CTagExport::WriteCols(ITextStream &Stm, WCHAR cSep)
{
	Stm.PutLine(L"Name");

	INDEX c = m_ColList.GetHead();

	while( !m_ColList.Failed(c) ) {

		CStringArray Parts;

		m_ColList[c].Tokenize(Parts, '\n');

		if( Parts[0] == L"Core" ) {

			Stm.PutLine(CPrintf(L"%c%s", cSep, Parts[1]));
			}
		else
			Stm.PutLine(CPrintf(L"%c%s / %s", cSep, Parts[0], Parts[1]));

		m_ColList.GetNext(c);
		}

	Stm.PutLine(L"\r\n");
	}

BOOL CTagExport::WriteTags(ITextStream &Stm, WCHAR cSep, CString Class)
{
	afxThread->SetStatusText(CPrintf(CString(IDS_EXPORTING_TAGS_OF), Class));

	INDEX t = m_pTags->GetHead();

	while( !m_pTags->Failed(t) ) {

		CTag *pTag = m_pTags->GetItem(t);

		if( pTag->GetExportClass() == Class ) {

			if( !IsPrivate(pTag) ) {

				CStringArray Props;

				CUIGetSet().GetProps(Props, pTag);

				if( BuildPropMap(Props) ) {

					Stm.PutLine(pTag->m_Name);

					INDEX c = m_ColList.GetHead();

					while( !m_ColList.Failed(c) ) {

						Stm.PutLine(CPrintf("%c", cSep));

						INDEX m = m_Map.FindName(c);

						if( !m_Map.Failed(m) ) {

							CStringArray Parts;

							UINT p = m_Map.GetData(m);

							Props[p].Tokenize(Parts, '\n');

							if( Parts[2] == L"\"\"" ) {

								Parts.SetAt(2, L"((EMPTY))");
								}

							Stm.PutLine(Parts[2]);
							}

						m_ColList.GetNext(c);
						}

					Stm.PutLine(L"\r\n");
					}
				}
			}

		m_pTags->GetNext(t);
		}

	afxThread->SetStatusText(L"");

	return TRUE;
	}

CString CTagExport::MakeName(CString Prop)
{
	CStringArray Parts;

	Prop.Tokenize(Parts, '\n');

	return MakeName(Parts);
	}

CString CTagExport::MakeName(CStringArray const &Parts)
{
	return Parts[0] + L'\n' + Parts[1];
	}

CTag * CTagExport::CreateTag(CLASS Class, CString Name)
{
	if( CheckParents(Name) ) {

		CTag *pTag = AfxNewObject(CTag, Class);

		m_pTags->AppendItem(pTag);

		pTag->SetName(Name);

		return pTag;
		}

	return NULL;
	}

BOOL CTagExport::CheckParents(CString Name)
{
	UINT uPos = Name.Find('.');

	if( uPos < NOTHING ) {

		CStringArray List;

		Name.Tokenize(List, '.');

		for( UINT n = 0; n < List.GetCount() - 1; n++ ) {

			CString Fold;

			for( UINT f = 0; f <= n; f++ ) {

				if( f ) {

					Fold += '.';
					}

				Fold += List[f];
				}

			CTag *pTag = (CTag *) m_pTags->FindByName(Fold);

			if( pTag ) {

				if( !pTag->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

					return FALSE;
					}

				continue;
				}

			pTag = New CTagFolder;

			m_pTags->AppendItem(pTag);

			pTag->SetName(Fold);
			}
		}

	return TRUE;
	}

BOOL CTagExport::CheckTagName(CString Name)
{
	CStringArray List;

	Name.Tokenize(List, '.');

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CError Error(FALSE);

		if( !C3ValidateName(Error, List[n]) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CTagExport::IsPrivate(CTag *pTag)
{
	return pTag->GetDatabase()->IsPrivate() && pTag->GetPrivate() == 1;
	}

// Import Custom Tag Processing

void CTagExport::MakeUnicode(PTXT sLine, CString sCustom, BOOL fAddNull)
{
	UINT u = sCustom.GetLength();

	memset(sLine, 0, u + 1);

	UINT i;

	for( i = 0; i < u; i++ ) {

		sLine[i] = (WORD)sCustom[i];
		}

	if( fAddNull ) {

		sLine[i] = (WORD)0;
		}
	}

BOOL CTagExport::ParseFileName(CString sFileName)
{
	UINT uFind = sFileName.FindRev('.');

	if( uFind < NOTHING ) {

		sFileName = sFileName.Left(uFind);
		}

	else {
		return FALSE;
		}

	uFind = sFileName.FindRev('\\');

	if( uFind < NOTHING ) {

		uFind++;
		}

	else {
		uFind = sFileName.FindRev(':');

		if( uFind < NOTHING ) {

			uFind++;
			}

		else {
			uFind = 0;
			}
		}

	m_Info.sFileName = sFileName.Mid(uFind);

	return TRUE;
	}

// Custom Import Helpers

BOOL CTagExport::IsCTSelected(void)
{
	return m_Info.uIDS & CTSELECTED;
	}

// End of File
