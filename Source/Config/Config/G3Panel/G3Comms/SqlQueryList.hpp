
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryList_HPP

#define INCLUDE_SqlQueryList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlQuery;

//////////////////////////////////////////////////////////////////////////
//
// SQL Query List
//

class CSqlQueryList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlQueryList(void);

		// Item Access
		CSqlQuery * GetItem(INDEX Index) const;
		CSqlQuery * GetItem(UINT uPos)   const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
