
#include "Intern.hpp"

#include "DispFormatString.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// String Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatString, CDispFormat);

// Constructor

CDispFormatString::CDispFormatString(void)
{
	m_uType  = 8;

	m_Length = 40;
	}

// UI Update

void CDispFormatString::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Template" ) {

		if( m_Template.GetLength() ) {

			m_Length = m_Template.GetLength();

			pHost->UpdateUI(this, "Length");
			}

		DoEnables(pHost);

		return;
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatString::GetTypeData(CString Tag, CTypeDef &Type)
{
	return FALSE;
	}

// Formatting

CString CDispFormatString::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Type == typeString ) {

		CString Text = PCUTF(Data);

		Text.TrimBoth();

		return Text.Left(m_Length);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Download Support

BOOL CDispFormatString::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	if( m_Template.IsEmpty() ) {

		Init.AddWord(WORD(m_Length));
		}
	else {
		Init.AddWord(0);

		Init.AddText(m_Template);
		}

	return TRUE;
	}

// Meta Data Creation

void CDispFormatString::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddInteger(Length);
	Meta_AddString (Template);

	Meta_SetName((IDS_STRING_FORMAT));
	}

// Implementation

void CDispFormatString::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Length", m_Template.IsEmpty());
	}

// End of File
