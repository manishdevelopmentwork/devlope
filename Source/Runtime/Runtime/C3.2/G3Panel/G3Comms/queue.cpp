
#include "intern.hpp"

#include "queue.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Write Queue
//

// Constructor

CWriteQueue::CWriteQueue(CCommsDevice *pDevice, UINT uSize, UINT uShare)
{
	m_pDevice = pDevice;

	m_uSize   = uSize;

	m_uShare  = uShare;

	m_pLock   = Create_Rutex();

	m_pData   = New CWrite [ m_uSize ];

	m_uHead   = 0;

	m_uTail   = 0;

	m_uBusy   = 0;
	}

// Destructor

CWriteQueue::~CWriteQueue(void)
{
	AfxRelease(m_pLock);

	delete [] m_pData;
	}

// Serialization

void CWriteQueue::LockQueue(void) const
{
	m_pLock->Wait(FOREVER);
	}

void CWriteQueue::FreeQueue(void) const
{
	m_pLock->Free();
	}

// Attributes

BOOL CWriteQueue::IsEmpty(void) const
{
	return m_uHead == m_uTail;
	}

BOOL CWriteQueue::IsFilling(void) const
{
	return GetCount() >= m_uSize / 2;
	}

UINT CWriteQueue::GetCount(void) const
{
	LockQueue();

	UINT uCount = (m_uTail + m_uSize - m_uHead) % m_uSize;

	FreeQueue();

	return uCount;
	}

UINT CWriteQueue::GetLimit(void) const
{
	return min(GetCount(), m_uSize / m_uShare);
	}

// Operations

BOOL CWriteQueue::GetEntry(UINT uSlot, CWrite &Write)
{
	LockQueue();

	UINT uScan = m_uHead;

	UINT uStep = uSlot;

	for(;;) {

		if( uScan == m_uTail ) {

			break;
			}

		if( uStep-- ) {

			uScan = (uScan + 1) % m_uSize;

			continue;
			}
		
		Write = m_pData[uScan];

		MakeMax(m_uBusy, uSlot + 1);

		FreeQueue();

		return TRUE;
		}

	FreeQueue();

	return FALSE;
	}

void CWriteQueue::DropLast(void)
{
	m_uBusy = m_uBusy - 1;
	}

void CWriteQueue::WriteDone(UINT uCount)
{
	LockQueue();

	while( uCount-- ) {

		CWrite const &Write = m_pData[m_uHead];

		Write.m_pBlock->SetQueueDone(Write.m_nPos);

		m_uHead = (m_uHead + 1) % m_uSize;

		m_uBusy = m_uBusy - 1;
		}

	FreeQueue();
	}

void CWriteQueue::WriteFail(void)
{
	m_uBusy = 0;
	}

void CWriteQueue::StepHead(UINT uCount)
{
	LockQueue();

	m_uHead = (m_uHead + uCount) % m_uSize;

	m_uBusy = 0;

	FreeQueue();
	}

void CWriteQueue::AddWrite(CWrite const &Write)
{
	for(;;) {

		LockQueue();

		UINT uNext = (m_uTail + 1) % m_uSize;

		if( uNext == m_uHead ) {

			FreeQueue();

			Sleep(10);

			continue;
			}

		m_pData[m_uTail] = Write;

		m_uTail          = uNext;

		FreeQueue();

		break;
		}
	}

void CWriteQueue::AddWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data, DWORD dwMask)
{
	for(;;) {

		LockQueue();

		UINT uNext = (m_uTail + 1) % m_uSize;

		if( uNext == m_uHead ) {

			FreeQueue();

			Sleep(10);

			continue;
			}

		CWrite &Write  = m_pData[m_uTail];

		Write.m_pBlock = pBlock;

		Write.m_nPos   = nPos;
		
		Write.m_Data   = Data;

		Write.m_dwMask = dwMask;

		m_uTail        = uNext;

		FreeQueue();

		break;
		}

	Throttle();
	}

void CWriteQueue::AddWrite(CCommsSysBlock *pBlock, INT nPos, DWORD Data)
{
	AddWrite(pBlock, nPos, Data, NOTHING);
	}

void CWriteQueue::FlushEntry(CCommsSysBlock *pBlock, INT nPos)
{
	LockQueue();

	UINT uScan = (m_uHead + m_uBusy) % m_uSize;

	UINT uCopy = uScan;

	while( uScan != m_uTail ) {

		CWrite const &Write = m_pData[uScan];

		if( Write.m_pBlock == pBlock ) {

			if( Write.m_nPos == nPos ) {

				Write.m_pBlock->SetQueueDone(Write.m_nPos);

				uScan = (uScan + 1) % m_uSize;

				continue;
				}
			}

		if( uCopy - uScan ) {

			m_pData[uCopy] = Write;
			}

		uCopy = (uCopy + 1) % m_uSize;

		uScan = (uScan + 1) % m_uSize;
		}

	m_uTail = uCopy;

	FreeQueue();
	}
	
void CWriteQueue::FlushBlock(CCommsSysBlock *pBlock)
{
	LockQueue();

	UINT uScan = m_uHead;

	UINT uCopy = uScan;

	while( uScan != m_uTail ) {

		CWrite const &Write = m_pData[uScan];

		if( Write.m_pBlock == pBlock ) {

			if( Write.m_dwMask < NOTHING ) {

				Write.m_pBlock->SetWriteData( Write.m_nPos,
							      setFlush,
							      Write.m_Data,
							      Write.m_dwMask
							      );

				}
			else {
				Write.m_pBlock->SetWriteData( Write.m_nPos,
							      setFlush,
							      Write.m_Data
							      );
				}

			uScan = (uScan + 1) % m_uSize;

			continue;
			}

		if( uCopy - uScan ) {

			m_pData[uCopy] = Write;
			}

		uCopy = (uCopy + 1) % m_uSize;

		uScan = (uScan + 1) % m_uSize;
		}

	m_uTail = uCopy;

	FreeQueue();
	}

void CWriteQueue::FlushAll(void)
{
	LockQueue();

	UINT uScan = m_uHead;

	while( uScan != m_uTail ) {

		CWrite const &Write = m_pData[uScan];

		if( Write.m_dwMask < NOTHING ) {

			Write.m_pBlock->SetWriteData( Write.m_nPos,
						      setFlush,
						      Write.m_Data,
						      Write.m_dwMask
						      );

			}
		else {
			Write.m_pBlock->SetWriteData( Write.m_nPos,
						      setFlush,
						      Write.m_Data
						      );
			}

		uScan = (uScan + 1) % m_uSize;
		}

	m_uHead = 0;

	m_uTail = 0;

	FreeQueue();
	}

void CWriteQueue::EmptyAll(void)
{
	LockQueue();

	UINT uScan = m_uHead;

	while( uScan != m_uTail ) {

		CWrite const &Write = m_pData[uScan];

		Write.m_pBlock->SetQueueDone(Write.m_nPos);

		uScan = (uScan + 1) % m_uSize;
		}

	m_uHead = 0;

	m_uTail = 0;

	FreeQueue();
	}

// Implementation

BOOL CWriteQueue::Throttle(void)
{
	// REV3 -- We used to use a simple kind of PID algortihm
	// here. Was that really any better? Or should we just
	// keep using this kind of exponential timing algorithm?

	UINT uCount = GetCount();

	UINT uSleep = 0;

	for( UINT f = 2; f <= 16; f <<= 1 ) {

		UINT uLimit = ((f - 1) * m_uSize) / f;

		if( uCount >= uLimit ) {

			uSleep += f * 10;

			continue;
			}

		break;
		}

	if( uSleep ) {

		Sleep(uSleep);

		return TRUE;
		}

	return FALSE;
	}

// End of File
