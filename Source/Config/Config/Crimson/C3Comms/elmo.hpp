
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ELMO_HPP
	
#define	INCLUDE_ELMO_HPP

// Space Contractions
#define	BB	addrBitAsBit
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// List Selections
#define	HMO	190 // Motion Commands Header
#define	HIO	191 // I/O Commands Header
#define	HST	192 // Status Commands Header
#define	HFB	193 // Feedback Commands Header
#define	HCG	194 // Configuration Commands Header
#define	HCF	195 // Control Filter Commands Header
#define	HPR	196 // Protection Commands Header
#define	HDR	197 // Data Recording Commands Header
#define	HUP	198 // User Parameters Header
#define	HGC	199 // General Commands Header

#define HAA	201 // 'A' Header
#define HAB	202
#define HAC	203
#define HAD	204
#define HAE	205
#define HAF	206
#define HAG	207
#define HAH	208
#define HAI	209
#define HAJ	210
#define HAK	211
#define HAL	212
#define HAM	213
#define HAN	214
#define HAO	215
#define HAP	216
#define HAQ	217
#define HAR	218
#define HAS	219
#define HAT	220
#define HAU	221
#define HAV	222
#define HAW	223
#define HAX	224
#define HAY	225
#define HAZ	226 // 'Z' Header

// Alpha Header Offset from letter
#define	AOFF	HAA - 'A'

// List Type Change
#define	TMF	237 // Select Machine Functions
#define	TAZ	238 // Select Alphabetic Listing
#define	TKW	239 // Select Keyword Search

// Command Definitions
// Motion Commands
#define	CAC	1
#define	CBG	2
#define	CBT	3
#define	CDC	4
#define	CIL	5
#define	CJV	6
#define	CMO	7
#define	CPA	8
#define	CPR	9
#define	CSD	10
#define	CSF	11
#define	CSP	12
#define	CST	13
#define	CTC	14

// I/O Commands
#define	CAN	21
#define	CIB	22
#define	CIF	23
#define	CIP	24
#define	COB	25
#define	COC	26
#define	COL	27
#define	COP	28

// Status Commands
#define	CBV	31
#define	CDD	32
#define	CDV	33
#define	CEC	34
#define	CLC	35
#define	CMF	36
#define	CMS	37
#define	CPK	38
#define	CSN	39
#define	CSR	40
#define	CTI	41
#define	CVR	42

// Feedback Commands
#define	CAB	51
#define	CID	52
#define	CIQ	53
#define	CPE	54
#define	CPX	55
#define	CPY	56
#define	CVE	57
#define	CVX	58
#define	CVY	59
#define	CYA	60

// Configuration Commands
#define	CAG	71
#define	CAS	72
#define	CBP	73
#define	CCA	74
#define	CCL	75
#define	CEF	76
#define	CEM	77
#define	CET	78
#define	CFF	79
#define	CFR	80
#define	CHM	81
#define	CHY	82
#define	CMC	83
#define	CMP	84
#define	CPL	85
#define	CPM	86
#define	CPT	87
#define	CPV	88
#define	CPW	89
#define	CQP	90
#define	CQT	91
#define	CQV	92
#define	CRM	93
#define	CUM	94
#define	CTR	95
#define	CVH	96
#define	CVL	97
#define	CXM	98
#define	CYM	99

// Control Filter Commands
#define	CGS	111
#define	CKG	112
#define	CKR	113
#define	CKI	114
#define	CKP	115
#define	CKV	116
#define	CXA	117
#define	CXP	118

// Protection Commands
#define	CER	121
#define	CHL	122
#define	CLL	123

// Data Recording Commands
#define	CBH	131
#define	CRC	132
#define	CRG	133
#define	CRL	134
#define	CRP	135
#define	CRR	136
#define	CRV	137

// User Program Commands
#define	CHP	141
#define	CKL	142
#define	CMI	143
#define	CPS	144
#define	CXC	145
#define	CXQ	146

// General Commands
#define	CLD	151
#define	CRS	152
#define	CSV	153
#define	CTM	154
#define	CTS	155
#define	CUF	156
#define	CUI	157
#define	CWI	158
#define	CWS	159
#define	CZX	160

//////////////////////////////////////////////////////////////////////////
//
// Elmo Comms Driver
//

class CElmoDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CElmoDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	CheckAlignment(CSpace *pSpace);

		// Helpers
		BOOL	NeedsParam(UINT uTable);
		void	SetListSelect(UINT * pSelect);

	protected:
		// Data
		UINT	m_uListSel;

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Elmo Address Selection
//

class CElmoAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CElmoAddrDialog(CElmoDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		UINT	m_uAllowSpace;
		UINT	m_uListSel;
		BOOL	m_fListTypeChange;
		CString	m_KWText;
		UINT	m_KWPrev;
		CElmoDriver * m_pDriverData;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrideables
//		void	ShowAddress(CAddress Addr);
//		CString	GetAddressText(void);
//		void	ShowDetails(void);

		// Load List Functions
		void	LoadList(void);
		void	LoadEntry(CListBox * pBox, CString Prefix, CString Caption, DWORD n);
		void	DoDesignatedHeader(CListBox * pBox);
		INDEX	DoSelections(CListBox * pBox);
		INDEX	DoAlpha(CListBox * pBox);
		INDEX	DoFunct(CListBox * pBox);
		INDEX	DoKeyWd(CListBox * pBox);
		void	DoRemainingHeaders(CListBox * pBox);

		// Helpers
		void	SetAllow(void);
		BOOL	NeedDefaultSpace(UINT uHead);
		void	SelectDefaultSpace(void);
		BOOL	IsHeaderSpace(UINT uHead);
		BOOL	IsAlphaHeader(UINT uHead);
		BOOL	IsFunctHeader(UINT uHead);
		BOOL	IsListChoice(UINT uSel);
		BOOL	IsListTypeChange(UINT uSel);
		BOOL	ShowOK(UINT uTable);
		UINT	GetListSelFromHeader(UINT uHeader);
		BOOL	IsHMO(UINT uTable);
		BOOL	IsHIO(UINT uTable);
		BOOL	IsHST(UINT uTable);
		BOOL	IsHFB(UINT uTable);
		BOOL	IsHCG(UINT uTable);
		BOOL	IsHCF(UINT uTable);
		BOOL	IsHPR(UINT uTable);
		BOOL	IsHDR(UINT uTable);
		BOOL	IsHUP(UINT uTable);
		BOOL	IsHGC(UINT uTable);
		BOOL	NeedsParam(UINT uTable);
	}; 

// End of File

#endif
