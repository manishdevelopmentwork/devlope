
#include "Intern.hpp"

#include "OptionCardRackItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPortList.hpp"
#include "OptionCardItem.hpp"
#include "OptionCardList.hpp"
#include "OptionCardRackList.hpp"
#include "UsbTreePath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Option Card Rack Item
//

// Dynamic Class

AfxImplementDynamicClass(COptionCardRackItem, CUIItem);

// Constructor

COptionCardRackItem::COptionCardRackItem(void)
{
	m_Slot   = NOTHING;

	m_Number = NOTHING;

	m_pSlots = New COptionCardList;

	m_pRacks = New COptionCardRackList;
	}

COptionCardRackItem::COptionCardRackItem(UINT uSlot)
{
	m_Slot   = uSlot;

	m_Number = NOTHING;

	m_pSlots = New COptionCardList;

	m_pRacks = New COptionCardRackList;
	}

// Attributes

UINT COptionCardRackItem::GetTreeImage(void) const
{
	return IDI_RACK_GRAPHITE;
	}

CString COptionCardRackItem::GetTreeLabel(void) const
{
	return GetHumanName();
	}

UINT COptionCardRackItem::GetRackCount(void) const
{
	return m_pRacks->GetRackCount();
	}

UINT COptionCardRackItem::GetSlotCount(void) const
{
	return m_pSlots->GetItemCount() + m_pRacks->GetSlotCount();
	}

BOOL COptionCardRackItem::HasType(UINT uType) const
{
	return m_pSlots->HasType(uType) || m_pRacks->HasType(uType);
	}

// Operations

BOOL COptionCardRackItem::AppendItem(COptionCardItem *pItem)
{
	return m_pSlots->AppendItem(pItem);
	}

BOOL COptionCardRackItem::AppendItem(COptionCardRackItem *pItem)
{
	return m_pRacks->AppendItem(pItem);
	}

BOOL COptionCardRackItem::DeleteItem(COptionCardItem *pItem)
{
	return m_pSlots->DeleteItem(pItem);
	}

BOOL COptionCardRackItem::DeleteItem(COptionCardRackItem *pItem)
{
	return m_pRacks->DeleteItem(pItem);
	}

void COptionCardRackItem::Validate(BOOL fExpand)
{
	m_pSlots->Validate(fExpand);

	m_pRacks->Validate(fExpand);
	}

void COptionCardRackItem::ClearSysBlocks(void)
{
	m_pSlots->ClearSysBlocks();

	m_pRacks->ClearSysBlocks();
	}

void COptionCardRackItem::NotifyInit(void)
{
	m_pSlots->NotifyInit();

	m_pRacks->NotifyInit();
	}

void COptionCardRackItem::CheckMapBlocks(void)
{
	m_pSlots->CheckMapBlocks();

	m_pRacks->CheckMapBlocks();
	}

UINT COptionCardRackItem::GetPowerBudget(void)
{
	return m_pSlots->GetItemCount() ? m_pSlots->GetItemCount() * 43 : 1;
	}

UINT COptionCardRackItem::GetPowerTotal(void)
{
	return m_pSlots->GetTotalPower();
	}

UINT COptionCardRackItem::GetPowerUsage(void)
{
	return (GetPowerTotal() * 100) / GetPowerBudget();
	}

BOOL COptionCardRackItem::CheckPower(void)
{
	return GetPowerUsage() <= 100 && m_pRacks->CheckPower();
	}

// UI Management

CViewWnd * COptionCardRackItem::CreateView(UINT uType)
{
	return NULL;
	}

// Ports List Access

BOOL COptionCardRackItem::GetPortList(CCommsPortList * &pList, UINT uIndex) const
{
	if( m_pSlots->GetPortList(pList, uIndex) ) {

		return TRUE;
		}

	uIndex -= m_pSlots->GetItemCount();

	return m_pRacks->GetPortList(pList, uIndex);
	}

// Slot

COptionCardItem * COptionCardRackItem::FindSlot(CUsbTreePath const &Slot) const
{
	COptionCardItem *pOption = m_pSlots->Find(Slot);

	if( !pOption ) {

		pOption = m_pRacks->FindSlot(Slot);
		}

	return pOption;
	}

COptionCardItem * COptionCardRackItem::FindSlot(UINT Slot) const
{
	COptionCardItem *pOption = m_pSlots->Find(Slot);

	if( !pOption ) {

		pOption = m_pRacks->FindSlot(Slot);
		}

	return pOption;
	}

// Rack

COptionCardRackItem * COptionCardRackItem::FindRack(CUsbTreePath const &Rack) const
{
	if( m_Slot != NOTHING && Rack.dw == m_Slot ) {

		return (COptionCardRackItem *) this;
		}

	return m_pRacks->FindRack(Rack);
	}

COptionCardRackItem * COptionCardRackItem::FindRack(UINT Rack) const
{
	if( m_Number != NOTHING && Rack == m_Number ) {

		return (COptionCardRackItem *) this;
		}

	return m_pRacks->FindRack(Rack);
	}

// Firmware

void COptionCardRackItem::GetFirmwareList(CArray<UINT> &List) const
{
	m_pSlots->GetFirmwareList(List);

	m_pRacks->GetFirmwareList(List);
	}

// Item Naming

CString COptionCardRackItem::GetHumanName(void) const
{
	COptionCardRackList *pList = (COptionCardRackList *) GetParent();

	UINT uPos = pList->FindItemPos(this);

	return CPrintf(L"Rack %d", uPos + 1);
	}

// Conversion

void COptionCardRackItem::PostConvert(void)
{
	if( !GetDatabase()->HasFlag(L"Graphite") ) {

		INDEX n = m_pRacks->GetHead();

		while( !m_pRacks->Failed(n) ) {

			INDEX i = n;

			m_pRacks->GetNext(n);

			m_pRacks->DeleteItem(i);
			}
		}
	}

// Download Support

BOOL COptionCardRackItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pSlots);

	Init.AddItem(itemSimple, m_pRacks);

	return TRUE;
	}

// Meta Data Creation

void COptionCardRackItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Slot);

	Meta_AddInteger(Number);

	Meta_AddCollect(Slots);

	Meta_AddCollect(Racks);
	}

// End of File
