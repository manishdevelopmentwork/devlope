
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instruction List Window
//

// Dynamic Class

AfxImplementDynamicClass(CStratonILWnd, CStratonWnd);

// Constructor

CStratonILWnd::CStratonILWnd(void)
{
	m_pLib = CSTLibrary::FindInstance();
	}

// End of File
