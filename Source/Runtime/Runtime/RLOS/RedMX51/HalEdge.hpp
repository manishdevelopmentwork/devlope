
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalEdge_HPP

#define INCLUDE_HalEdge_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HalMX51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Edge Controller
//

class CHalEdge : public CHalMX51
{
	public:
		// Constructor
		CHalEdge(void);
	};

// End of File

#endif
