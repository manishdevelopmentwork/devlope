
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR6.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Semi Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR6, CPrimRubyGaugeTypeAR);

// Constructor

CPrimRubyGaugeTypeAR6::CPrimRubyGaugeTypeAR6(void)
{
	m_lineFact = 0.7;
	}

// Overridables

void CPrimRubyGaugeTypeAR6::SetInitState(void)
{
	CPrimRubyGaugeTypeAR::SetInitState();

	SetInitSize(300, 200);
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR6::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeAR::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR6::AddMetaData(void)
{
	CPrimRubyGaugeTypeAR::AddMetaData();

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE_7));
	}

// Path Management

void CPrimRubyGaugeTypeAR6::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeAR::MakePaths();

	m_bezelBase = 66;

	Half(m_pathFace,  66, 67);

	Half(m_pathOuter, 66, 80, 90);
	Half(m_pathInner, 66, 70, 80);
	Half(m_pathRing1, 66, 89, 91);
	Half(m_pathRing2, 66, 79, 80);
	Half(m_pathRing3, 66, 66, 70);
	Half(m_pathRing4, 66, 74, 69);
	}

// Implementation

void CPrimRubyGaugeTypeAR6::LoadLayout(void)
{
	m_radiusPivot     =   8 * m_ScalePivot / 100.0;

	m_pointPivot.m_x  = 100;
	m_pointPivot.m_y  =  98 - m_radiusPivot;
	
	m_angleMin        = -90 - 90;
	m_angleMax        = +90 - 90;
	
	m_radiusOuter.m_x = 55;
	m_radiusOuter.m_y = 55;

	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBug.m_x   = m_PointMode ? 60 : 60;
	m_radiusMajor.m_x = m_PointMode ? 47 : 40;
	m_radiusMinor.m_x = m_PointMode ? 49 : 45;
	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBand.m_x  = m_PointMode ? 50 : 50;
	m_radiusPoint.m_x = m_PointMode ? 46 : 47;
	m_radiusSweep.m_x = m_PointMode ? 35 : 47;

	CalcRadii();
	}

// End of File
