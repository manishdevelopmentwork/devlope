
#include "intern.hpp"

#include "kingbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();


//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CKingBusASCIIDriverOptions, CUIItem);

// Constructor

CKingBusASCIIDriverOptions::CKingBusASCIIDriverOptions(void)
{
	m_Timeout  = 1000;

	}

// Download Support

BOOL CKingBusASCIIDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}

// Meta Data Creation

void CKingBusASCIIDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Timeout);
	}


//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Communications Driver
//

// Instantiator

ICommsDriver *	Create_KingBusASCIIDriver(void)
{
	return New CKingBusASCIIDriver;
	}

// Constructor

CKingBusASCIIDriver::CKingBusASCIIDriver(void)
{
	m_wID		= 0x4014;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "King";
	
	m_DriverName	= "KingBus ASCII";
	
	m_Version	= "1.01";
	
	m_ShortName	= "KingBus";

	AddSpaces();
	}


// Configuration

CLASS CKingBusASCIIDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CKingBusASCIIDriverOptions);
	}


// Binding Control

UINT CKingBusASCIIDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CKingBusASCIIDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CKingBusASCIIDriver::AddSpaces(void)
{
	AddSpace(New CSpaceKing(1,	"S",	"Specific Gravity (S.SSS)",	10, 1,  256, addrLongAsLong));
	AddSpace(New CSpaceKing(2,	"X",	"Status Code (Read Only)",	10, 1,  256, addrWordAsWord));
	AddSpace(New CSpaceKing(3,	"L",	"Level Value (Read Only)",	10, 1,  256, addrRealAsReal));
	AddSpace(New CSpaceKing(4,	"U",	"Display Unit (Read Only)",	10, 1,  256, addrLongAsLong));
	AddSpace(New CSpaceKing(5,	"UW",	"Display Unit (Write Only)",	10, 1,  256, addrByteAsByte));
	}

// Address Management

BOOL CKingBusASCIIDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CKingBusASCIIAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CKingBusASCIIDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			Addr.a.m_Table |= addrNamed;

			return TRUE;
			}
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CKingBusASCIIDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CAddress Address = Addr;

	Address.a.m_Table = Addr.a.m_Table & 0x0F;

	UINT uDrop = pConfig->GetDataAccess("Drop")->ReadInteger(pConfig);

	if( uDrop ) {

		Address.a.m_Offset = uDrop;
		}

	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Address) ) {

		return TRUE;
		}
	
	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CKingBusASCIIAddrDialog, CStdAddrDialog);
		
// Constructor

CKingBusASCIIAddrDialog::CKingBusASCIIAddrDialog(CKingBusASCIIDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_uDrop = pConfig->GetDataAccess("Drop")->ReadInteger(pConfig);
	}
 
// Overridables

void CKingBusASCIIAddrDialog::SetAddressText(CString Text)
{
	if( m_uDrop ) {

		Text.Printf("%3.3u", m_uDrop);

		GetDlgItem(2002).SetWindowText(Text);

		GetDlgItem(2002).EnableWindow(FALSE);

		return;
		}
	
	CStdAddrDialog::SetAddressText(Text);
	}



//////////////////////////////////////////////////////////////////////////
//
// King Space Wrapper
//

// Constructor

CSpaceKing::CSpaceKing(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t) : CSpace(uTable, p, c, r, n, x, t)
{
	
	}

// Matching

BOOL CSpaceKing::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x0F);
	}


// End of File
