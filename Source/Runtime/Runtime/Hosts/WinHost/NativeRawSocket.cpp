
#include "Intern.hpp"

#include "NativeRawSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Libraries
//

#pragma comment(lib, "Ws2_32.lib")

#pragma comment(lib, "wpcap.lib")

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket Object
//

// Static Data

CArray <CNativeRawSocket::CFace *> CNativeRawSocket::m_Faces;

// Instantiator

static IUnknown * Create_NativeRawSocket(PCTXT pName)
{
	return (ISocket *) New CNativeRawSocket;
	}

// Registration

global void Register_NativeRawSocket(void)
{
	CNativeRawSocket::FindInterfaces();

	piob->RegisterInstantiator("sock-raw", Create_NativeRawSocket);
	}

global void Revoke_NativeRawSocket(void)
{
	piob->RevokeInstantiator("sock-raw");
	}

// Constructor

CNativeRawSocket::CNativeRawSocket(void)
{
	StdSetRef();

	m_pLock = Create_Qutex();

	ResetParams();

	m_uRxCount = 0;

	m_pRxHead  = NULL;

	m_pRxTail  = NULL;
	}

// Destructor

CNativeRawSocket::~CNativeRawSocket(void)
{
	Abort();

	AfxRelease(m_pLock);
	}

// IUnknown

HRESULT CNativeRawSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ISocket);

	StdQueryInterface(ISocket);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CNativeRawSocket::AddRef(void)
{
	StdAddRef();
	}

ULONG CNativeRawSocket::Release(void)
{
	StdRelease();
	}

// ISocket

HRESULT CNativeRawSocket::Listen(WORD Loc)
{
	return Listen(IP_EMPTY, Loc);
	}

HRESULT CNativeRawSocket::Listen(IPADDR const &IP, WORD Loc)
{
	CIpAddr const &Ip = (CIpAddr const &) IP;

	for( UINT n = 0; n < m_Faces.GetCount(); n++ ) {

		CFace const *pFace = m_Faces[n];

		bool         fDone = false;

		bool         fUsed = false;

		if( Ip.IsEmpty() || Ip.IsMulticast() || Ip.IsBroadcast() ) {

			// Monitor all adapters if we are using any
			// of these special IP addresses. WinPCAP
			// does not support the any-adapter option.

			fUsed = true;
			}
		else {
			for( UINT m = 0; m < pFace->m_IpAddrs.GetCount(); m++ ) {

				if( Ip == pFace->m_IpAddrs[m] ) {

					// If we've got a specific match, we will
					// listen on this port alone and then stop
					// looking for other ports to consider.

					fUsed = true;

					fDone = true;

					break;
					}
				}
			}

		if( fUsed ) {

			pcap_t *hPort = pcap_open_live(pFace->m_Adapter, 4096, true, 0, m_sError);

			if( hPort >= 0 ) {

				bpf_program Program;

				CPrintf     Filter("ether proto 0x%4.4X", Loc);

				if( pcap_compile(hPort, &Program, PTXT(PCTXT(Filter)), 1, 0) >= 0 ) {

					pcap_setfilter  (hPort, &Program);

					pcap_setnonblock(hPort, 1, m_sError);

					pcap_freecode   (&Program);

					UINT t = m_Ports.Append(hPort);

					m_Threads.Append(CreateClientThread(this, t, 500));

					AfxTrace("rawport: opened %s with thread %u\n", PCTXT(pFace->m_Adapter), t);
					}
				else
					pcap_close(hPort);
				}
			}

		if( fDone ) {

			break;
			}
		}

	return m_Ports.GetCount() ? S_OK : E_FAIL;
	}

HRESULT CNativeRawSocket::Connect(IPADDR const &IP, WORD Rem)
{
	return Connect(IP, Rem, 0);
	}

HRESULT CNativeRawSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	return E_FAIL;
}

HRESULT CNativeRawSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CNativeRawSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( m_Ports.GetCount() ) {

		m_pLock->Wait(FOREVER);

		if( m_pRxHead ) {

			UINT uCopy = Min(uSize, m_pRxHead->GetSize());

			memcpy(m_pRxHead->StripHead(uCopy), pData, uCopy);

			if( !m_pRxHead->GetSize() ) {

				m_pRxHead->Release();

				AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);

				m_uRxCount--;
				}

			m_pLock->Free();

			uSize = uCopy;

			return S_OK;
			}
		}

	uSize = 0;

	return E_FAIL;
	}

HRESULT CNativeRawSocket::Send(PBYTE pData, UINT &uSize)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CNativeRawSocket::Recv(CBuffer * &pBuff)
{
	if( m_Ports.GetCount() ) {

		m_pLock->Wait(FOREVER);

		if( m_pRxHead ) {

			pBuff = m_pRxHead;

			AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);

			m_uRxCount--;

			m_pLock->Free();

			return S_OK;
			}

		m_pLock->Free();
		}

	pBuff = NULL;

	return E_FAIL;
	}

HRESULT CNativeRawSocket::Send(CBuffer *pBuff)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::GetLocal(IPADDR &IP)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::GetRemote(IPADDR &IP)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	return E_FAIL;
	}

HRESULT CNativeRawSocket::GetPhase(UINT &Phase)
{
	Phase = m_Ports.GetCount() ? PHASE_OPEN : PHASE_IDLE;

	return S_OK;
	}

HRESULT CNativeRawSocket::SetOption(UINT uOption, UINT uValue)
{
	switch( uOption ) {

		case OPT_RECV_QUEUE:

			m_uRxLimit = Max(1, Min(uValue, 64));

			break;

		case OPT_ADDRESS:

			m_uAddress = uValue;

			break;
		
		default:

			return E_FAIL;
		}

	return S_OK;
	}

HRESULT CNativeRawSocket::Abort(void)
{
	if( m_Ports.GetCount() ) {

		for( UINT n = 0; n < m_Threads.GetCount(); n++ ) {

			DestroyThread(m_Threads[n]);
			}

		while( m_pRxHead ) {

			m_pRxHead->Release();

			AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);
			}

		ResetParams();

		m_Threads.Empty();

		m_Ports.Empty();
		}

	return S_OK;
	}

HRESULT CNativeRawSocket::Close(void)
{
	return Abort();
	}

// IClientProcess

BOOL CNativeRawSocket::TaskInit(UINT uTask)
{
	return TRUE;
	}

INT CNativeRawSocket::TaskExec(UINT uTask)
{
	for(;;) {

		if( !pcap_dispatch(m_Ports[uTask], 1, PacketProc, PBYTE(this)) ) {
			
			Sleep(10);
			}
		}

	return 0;
	}

void CNativeRawSocket::TaskStop(UINT uTask)
{
	}

void CNativeRawSocket::TaskTerm(UINT uTask)
{
	pcap_close(m_Ports[uTask]);
	}

// Interface Location

void CNativeRawSocket::FindInterfaces(void)
{
	DWORD		       dwSize = 65536;

	win32::IP_ADAPTER_INFO *pData = (win32::IP_ADAPTER_INFO *) malloc(dwSize);

	if( win32::GetAdaptersInfo(pData, &dwSize) == ERROR_SUCCESS ) {

		win32::IP_ADAPTER_INFO *pWalk = pData;

		while( pWalk ) {

			if( pWalk->Type == MIB_IF_TYPE_ETHERNET ) {

				if( pWalk->AddressLength == 6 ) {

					// Build a list of all Ethernet ports fitted
					// to the system, including their addresses.

					CFace * pFace    = New CFace;

					pFace->m_Adapter = CString("\\Device\\NPF_") + pWalk->AdapterName;

					pFace->m_MacAddr = pWalk->Address;

					for( win32::IP_ADDR_STRING *p = &pWalk->IpAddressList; p; p = p->Next ) {

						pFace->m_IpAddrs.Append(CIpAddr(p->IpAddress.String));
						}

					m_Faces.Append(pFace);
					}
				}

			pWalk = pWalk->Next;
			}
		}

	free(pData);
	}

// Implementation

void CNativeRawSocket::OnPacket(PCBYTE pData, UINT uData)
{
	CBuffer *pBuff = BuffAllocate(0);

	if( m_uAddress & 1 ) {

		// Override the destination address with that of
		// the adapter on which the packet was recieved.

		CFace const *pFace = m_Faces[GetCurrentThread()->GetIntParam()];

		memcpy(pBuff->AddTail(6), pFace->m_MacAddr.m_Addr, 6);

		pData += 6;

		uData -= 6;
		}

	memcpy(pBuff->AddTail(uData), pData, uData);

	m_pLock->Wait(FOREVER);

	if( m_uRxCount < m_uRxLimit ) {

		AfxListAppend(m_pRxHead, m_pRxTail, pBuff, m_pNext, m_pPrev);

		m_uRxCount++;
		}

	m_pLock->Free();
	}

void CNativeRawSocket::ResetParams(void)
{
	m_uAddress = 0;

	m_uRxLimit = 32;
	}

// Packet Handler

void CNativeRawSocket::PacketProc(PBYTE pParam, pcap_pkthdr const *pInfo, PCBYTE pData)
{
	((CNativeRawSocket *) pParam)->OnPacket(pData, pInfo->len);
	}

// End of File
