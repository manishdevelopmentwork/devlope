
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Variable Map Wrapper
//

// Constructor

CVariableMap::CVariableMap(void)
{
	Bind(NULL);
	}

CVariableMap::CVariableMap(T5PTR_DB pBase)
{
	Bind(pBase);
	}

// Managemenet

void CVariableMap::Bind(T5PTR_DB pBase)
{
	m_pBase = pBase;
	}

// Operations

T5PTR_DBMAP CVariableMap::GetFirst(void)
{
	return T5Map_GetFirst(m_pBase, &m_wPos);
	}

T5PTR_DBMAP CVariableMap::GetNext(void)
{
	return T5Map_GetNext(m_pBase, &m_wPos);
	}

// Fast Access

T5PTR_DBMAP CVariableMap::GetByIndex(T5_WORD wType, T5_WORD wIndex)
{
	return T5Map_GetByIndex(m_pBase, wType, wIndex);
	}

T5PTR_DBMAP CVariableMap::GetByNumTag(T5_WORD wIndex)
{
	return T5Map_GetByNumTag(m_pBase, wIndex);
	}

T5PTR_DBMAP CVariableMap::GetBySymbol(T5_PTCHAR sSymbol)
{
	return T5Map_GetBySymbol(m_pBase, sSymbol);
	}

// Array Data

T5_PTR CVariableMap::IsArrayOfSingleVar(T5PTR_DBMAP pVar, T5_WORD &wCount, T5_WORD &wType)
{
	return T5Map_IsArrayOfSingleVar(m_pBase, pVar, &wCount, &wType);
	}

// External Address

T5_RET CVariableMap::SetXVAddress(T5PTR_DBMAP pVar, T5_PTR pData)
{
	return T5Map_SetXVAddress(m_pBase, pVar, pData);
	}

// End of File
