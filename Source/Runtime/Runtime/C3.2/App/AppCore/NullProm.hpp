
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NullProm_HPP

#define INCLUDE_NullProm_HPP

//////////////////////////////////////////////////////////////////////////
//
// Null Memory Manager
//

class CNullProm : public IProm
{
	public:
		// Constructor
		CNullProm(void);

		// IProm
		void  Init(DWORD dwParam);
		void  Task(HTASK hTask, DWORD dwParam);
		void  Term(DWORD dwParam);
		void  Sync(BOOL fTerm);
		void  Lock(void);
		void  Free(void);
		UINT  GetCount(void);
		UINT  GetSize(void);
		PBYTE GetAddr(UINT uSect);
		void  BatchStart(void);
		void  BatchEnd(void);
		void  QueueErase(UINT uSect);
		void  QueueWrite(UINT uSect, UINT uAddr, PCBYTE pData, UINT uCount);
		void  QueueWrite(UINT uSect, UINT uAddr, PCBYTE pData, UINT uCount, BOOL fBuff);
	};

// End of File

#endif
