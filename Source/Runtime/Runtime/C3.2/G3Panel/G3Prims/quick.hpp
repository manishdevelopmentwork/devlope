
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_QUICK_HPP
	
#define	INCLUDE_QUICK_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "legacy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Quick Plot
//

class CPrimQuickPlot : public CPrimLegacyFigure
{
	public:
		// Constructor
		CPrimQuickPlot(void);

		// Denstructor
		~CPrimQuickPlot(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT         m_uTag;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		UINT         m_Align;
		UINT         m_Pad;
		CPrimColor * m_pPen;

	protected:
		// Data Members
		CTagQuickPlot * m_pQuick;

		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL   m_fAvail;
			DWORD  m_Seq;
			C3REAL m_Min;
			C3REAL m_Max;
			COLOR  m_Pen;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Implementation
		BOOL FindQuick(void);
		int  Scale(C3REAL Data, int nSize);
	};

// End of File

#endif
