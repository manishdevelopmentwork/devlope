
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MATFP_HPP
	
#define	INCLUDE_MATFP_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CMatsushitaFPTCPDeviceOptions; 
class CMatsushitaFPTCPDriverOptions;


//////////////////////////////////////////////////////////////////////////
//
// Matsushita Series
//

class CMatsushitaFPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMatsushitaFPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series v2 - MEWTOCOL-COM (Ascii)
//

class CMatsushitaFPDriver2 : public CMatsushitaFPDriver
{
	public:
		// Constructor
		CMatsushitaFPDriver2(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series v2 - MEWTOCOL-DAT (Binary)    
//

class CMatsushitaFPDatDriver : public CMatsushitaFPDriver2
{
	public:
		// Constructor
		CMatsushitaFPDatDriver(void);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series - TCP/IP Master Driver
//

class CMatsushitaFPTCPDriver : public CMatsushitaFPDriver2
{
	public:
		// Constructor
		CMatsushitaFPTCPDriver(void);

		//Destructor
		~CMatsushitaFPTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series - TCP/IP Master Device Options
//

class CMatsushitaFPTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMatsushitaFPTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_ETLan;
		UINT m_Rs1;
		UINT m_Rs2;
		UINT m_Rs3;
		UINT m_Rs1R;
		UINT m_Rs2R;
		UINT m_Rs3R;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series - Master Driver Options
//

class CMatsushitaFPTCPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMatsushitaFPTCPDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Source;
		UINT m_Port;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


// End of File

#endif
