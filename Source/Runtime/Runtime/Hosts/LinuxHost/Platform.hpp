
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Platform_HPP

#define INCLUDE_Platform_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/PlatformBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object
//

class CPlatform : public CPlatformBase, public IPortSwitch
{
public:
	// Constructor
	CPlatform(void);

	// Destructor
	~CPlatform(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPlatform
	PCTXT METHOD GetFamily(void);
	PCTXT METHOD GetModel(void);
	PCTXT METHOD GetVariant(void);
	UINT  METHOD GetFeatures(void);
	BOOL  METHOD IsService(void);
	BOOL  METHOD IsEmulated(void);
	BOOL  METHOD HasMappings(void);
	void  METHOD ResetSystem(void);
	void  METHOD SetWebAddr(PCTXT pAddr);

protected:
	// Data Members
	CSecureDeviceInfo m_Info;
	CString		  m_Model;
	CString		  m_Variant;
	UINT		  m_uSleds;

	// IPortSwitch
	UINT METHOD GetCount(UINT uUnit);
	UINT METHOD GetMask(UINT uUnit);
	UINT METHOD GetType(UINT uUnit, UINT uLog);
	BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable);
	BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485);
	BOOL METHOD SetFull(UINT uUnit, BOOL fFull);
	BOOL METHOD SetMode(UINT uUnit, BOOL fAuto);

	// Implementation
	BOOL FindDeviceInfo(void);
	BOOL SetGpio(UINT uPort, UINT uGpio, UINT uState);
	BOOL SetEdge(UINT uDev, UINT uGpio, UINT uState);
	BOOL GetEdge(UINT uDev, UINT uGpio);
	UINT GetEdge(UINT uDev, UINT uGpio, UINT uMask);
};

// End of File

#endif
