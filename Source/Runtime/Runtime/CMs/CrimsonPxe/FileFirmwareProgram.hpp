
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_FileFirmwareProgram_HPP

#define INCLUDE_FileFirmwareProgram_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "FileFirmwareProps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Filing System Firmware Programming Object
//

class CFileFirmwareProgram : public CFileFirmwareProps, public IFirmwareProgram
{
public:
	// Constructor
	CFileFirmwareProgram(CString const &Root);

	// Destructor
	~CFileFirmwareProgram(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IFirmwareProps
	bool   METHOD IsCodeValid(void);
	PCBYTE METHOD GetCodeVersion(void);
	UINT   METHOD GetCodeSize(void);
	PCBYTE METHOD GetCodeData(void);

	// IFirmwareProgram
	bool METHOD ClearProgram(UINT uBlocks);
	bool METHOD WriteProgram(PCBYTE pData, UINT uCount);
	bool METHOD WriteVersion(PCBYTE pData);
	bool METHOD StartProgram(UINT uTimeout);

protected:
	// Data Members
	CString	    m_Image;
	CAutoFile * m_pFile;
	bool	    m_fValid;
};

// End of File

#endif
