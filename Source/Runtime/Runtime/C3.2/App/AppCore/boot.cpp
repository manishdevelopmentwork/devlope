
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Runtime
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Boot Loader Service
//

class CBootLoader : public ILinkService
{
	public:
		// Constructor
		CBootLoader(void);

		// ILinkService
		void Timeout(void);
		UINT Process(CLinkFrame &Req, CLinkFrame &Rep);
		void EndLink(CLinkFrame &Req);

	protected:
		// Implementation
		UINT ReadModel(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ReadOem(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckVersion(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ForceReset(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckHardware(CLinkFrame &Req, CLinkFrame &Rep);
		UINT StartProgram(CLinkFrame &Req, CLinkFrame &Rep);
		UINT ReadRevision(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteMAC(CLinkFrame &Req, CLinkFrame &Rep);
		UINT WriteSerialNumber(CLinkFrame &Req, CLinkFrame &Rep);
		UINT CheckLevel(CLinkFrame &Req, CLinkFrame &Rep);
	};

//////////////////////////////////////////////////////////////////////////
//
// Boot Loader Service
//

global	ILinkService *	Create_BootService(void)
{
	return New CBootLoader;
	}

// Constructor

CBootLoader::CBootLoader(void)
{
	}

// ILinkService

void CBootLoader::Timeout(void)
{
	}

UINT CBootLoader::Process(CLinkFrame &Req, CLinkFrame &Rep)
{
	if( Req.GetService() == servBoot ) {

		switch( Req.GetOpcode() ) {

			case bootReadModel:
				return ReadModel(Req, Rep);

			case bootReadOem:
				return ReadOem(Req, Rep);

			case bootCheckVersion:
				return CheckVersion(Req, Rep);

			case bootForceReset:
				return ForceReset(Req, Rep);

			case bootCheckHardware:
				return CheckHardware(Req, Rep);

			case bootStartProgram:
				return StartProgram(Req, Rep);

			case bootReadRevision:
				return ReadRevision(Req, Rep);

			case bootWriteMAC:				
				return WriteMAC(Req, Rep);

			case bootWriteSerial:
				return WriteSerialNumber(Req, Rep);

			case bootCheckLevel:
				return CheckLevel(Req, Rep);
			}

		return procError;
		}

	return procError;
	}

void CBootLoader::EndLink(CLinkFrame &Req)
{
	if( Req.GetService() == servBoot ) {

		switch( Req.GetOpcode() ) {

			case bootForceReset:

				if( g_pProm ) {
				
					g_pProm->Lock();
					}

				UINT  uPtr      = 0;
				
				DWORD dwTimeout = Req.ReadLong(uPtr);

				if( dwTimeout ) {

					SystemReboot();
					}
				else {
					SystemRestart();
					}

				break;
			}
		}
	}

// Implementation

UINT CBootLoader::ReadModel(CLinkFrame &Req, CLinkFrame &Rep)
{
	PCTXT pName = WhoGetName(FALSE);

	Rep.StartFrame(servBoot, opReply);

	Rep.AddData(PBYTE(pName), strlen(pName) + 1);

	return procOkay;
	}

UINT CBootLoader::ReadOem(CLinkFrame &Req, CLinkFrame &Rep)
{
	PCTXT pName = GetOemName();

	Rep.StartFrame(servBoot, opReply);

	Rep.AddData(PBYTE(pName), strlen(pName) + 1);

	return procOkay;
	}

UINT CBootLoader::CheckVersion(CLinkFrame &Req, CLinkFrame &Rep)
{
	AfxTrace("BUILD=%u\n", C3_BUILD);

	#if defined(AEON_PLAT_WIN32) || C3_BUILD == 9999

	Rep.StartFrame(servBoot, opReply);

	Rep.AddByte(TRUE);

	#else

	UINT   uPtr  = 0;

	PCBYTE pData = Req.ReadData(uPtr, 16);

	PCBYTE pComp = g_pFirmProps->GetCodeVersion();

	for( UINT n = 0; n < 16; n++ ) {	

		if( pComp[n] != pData[n] ) {

			break;
			}	
		}

	Rep.StartFrame(servBoot, opReply);

	Rep.AddByte(n == 16);

	#endif

	return procOkay;
	}

UINT CBootLoader::ForceReset(CLinkFrame &Req, CLinkFrame &Rep)
{
	#if defined(AEON_PLAT_WIN32)

	return procOkay;

	#else

	Rep.StartFrame(servBoot, opAck);

	return procEndLink;

	#endif
	}

UINT CBootLoader::CheckHardware(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servBoot, opReply);

	BYTE b1[] = {	0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF, 
			0xFF, 0xFF, 0xFF, 0xFF
			};

	BYTE b2[] = {	0x00, 0x00, 0x00, 0x00, 
			0x00, 0x00, 0x00, 0x00, 
			0x00, 0x00, 0x00, 0x00, 
			0x00, 0x00, 0x00, 0x00
			};

	Rep.AddData(b1, 16);

	Rep.AddWord(1);

	Rep.AddWord(0);

	Rep.AddData(b2, 16);

	return procOkay;
	}

UINT CBootLoader::StartProgram(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servBoot, opNak);

	return procOkay;
	}

UINT CBootLoader::ReadRevision(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servBoot, opReply);

	Rep.AddLong(AppGetBootVersion());

	return procOkay;
	}

UINT CBootLoader::WriteMAC(CLinkFrame &Req, CLinkFrame &Rep)
{
	static const MACADDR Rlc = { { 0x00, 0x05, 0xE4, 0x00, 0x00, 0x00 } };

	UINT   uPtr = 0;

	for( UINT n = 0; n < 2; n ++ ) {

		PCBYTE pMac = Req.ReadData(uPtr, 6);

		if( !memcmp(pMac, &Rlc, 3) ) {

			WORD wAddr = (n == 0) ? Mem(MacId0) : Mem(MacId1);

			FRAMPutData(wAddr, PBYTE(pMac), 6);
			}
		}

	Rep.StartFrame(servBoot, opAck);
		
	return procOkay;
	}

UINT CBootLoader::WriteSerialNumber(CLinkFrame &Req, CLinkFrame &Rep)
{
	UINT  uPtr   = 0;

	UINT  uCount = Req.ReadWord(uPtr);

	PCTXT pData  = PCTXT(Req.ReadData(uPtr, uCount));

	if( g_pIdentity->SetSerial(pData, uCount) ) {

		Rep.StartFrame(servBoot, opAck);

		return procOkay;
		}

	return procError;
	}

UINT CBootLoader::CheckLevel(CLinkFrame &Req, CLinkFrame &Rep)
{
	Rep.StartFrame(servBoot, opReply);

	Rep.AddByte(1);

	return procOkay;
	}

// End of File
