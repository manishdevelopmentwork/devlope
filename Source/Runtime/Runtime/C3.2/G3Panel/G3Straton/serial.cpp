
#include "intern.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifdef T5DEF_SERIAL

// Code

clink	void	serialInitialize(T5_PTSERIAL pSerial)
{
	AfxTrace("serialInitialize\n");
	}

clink	T5_BOOL	serialIsValid (T5_PTSERIAL pSerial)
{
	AfxTrace("serialIsValid\n");

	return FALSE;
	}

clink	T5_BOOL	serialOpen (T5_PTSERIAL pSerial, T5_PTCHAR szConfig)
{
	AfxTrace("serialOpen\n");

	return FALSE;
	}

clink	void	serialClose (T5_PTSERIAL pSerial)
{
	AfxTrace("serialClose\n");
	}

clink	T5_WORD	serialSend (T5_PTSERIAL pSerial, T5_WORD wSize, T5_PTR pData)
{
	AfxTrace("serialSend\n");

	return 0;
	}

clink	T5_WORD serialReceive (T5_PTSERIAL pSerial, T5_WORD wSize, T5_PTR pData)
{
	AfxTrace("serialRecv\n");

	return 0;
	}

#endif

// End of File
