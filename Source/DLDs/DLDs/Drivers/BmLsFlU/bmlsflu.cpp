#include "intern.hpp"

#include "bmlsflu.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Final Length Mode Slave UDP Driver
//

// Instantiator

INSTANTIATE(CBMikeLSFinalLenSlaveUdpDriver);

// Constructor

CBMikeLSFinalLenSlaveUdpDriver::CBMikeLSFinalLenSlaveUdpDriver(void)
{
	m_Ident   = DRIVER_ID;

	m_fTF     = FALSE;

	m_uPtr    = 0;

	m_pTxData = NULL;

	m_pTxBuff = NULL;

	m_pRxBuff = NULL;

	m_dwFL	  = 0;
	
	m_dwQF	  = 0;
	}

// Config

void MCALL CBMikeLSFinalLenSlaveUdpDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		UINT uScan	= ToTicks(GetWord(pData));
		UINT uTimeout	= GetWord(pData);
		UINT uAuto	= GetByte(pData);
		m_RxMac.m_IP	= GetAddr(pData);
		m_RxMac.m_Port	= GetWord(pData);
		}
	}

// Management

void MCALL CBMikeLSFinalLenSlaveUdpDriver::Attach(IPortObject *pPort)
{	
	for( UINT u = 0; u < elements(m_pSock); u++ ) {

		m_pSock[u] = CreateSocket(IP_UDP);
		}
	}

void MCALL CBMikeLSFinalLenSlaveUdpDriver::Detach(void)
{
	for( UINT u = 0; u < elements(m_pSock); u++ ) {

		if( m_pSock[u] ) {

			m_pSock[u]->Release();

			m_pSock[u] = NULL;
			}
		}
	}

void MCALL CBMikeLSFinalLenSlaveUdpDriver::Open(void)
{
	if( m_pSock[0] ) {

		m_pSock[0]->SetOption(OPT_ADDRESS, 1);

		m_pSock[0]->SetOption(OPT_RECV_QUEUE, 2);
		}

	if( m_pSock[1] ) {

		m_pSock[1]->SetOption(OPT_ADDRESS, 1);

		m_pSock[1]->SetOption(OPT_RECV_QUEUE, 2);
		}
	}

// User Access

UINT MCALL CBMikeLSFinalLenSlaveUdpDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( EndRealTime() ) {
		
			m_RxMac.m_IP = MotorToHost(dwValue);

			m_fTF = FALSE;

			Free(pText);
			
			return 1;    
			}

		return 0;
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			if( EndRealTime() ) {

				m_RxMac.m_Port   = uValue;

				m_fTF = FALSE;

				return 1;
				}
			}

		return 0;
		}

	if( uFunc == 3 ) {
		
		// Get Current IP Address

		return MotorToHost(m_RxMac.m_IP);
		}
	
	if( uFunc == 4 ) {
		
		// Get Current Port

		return m_RxMac.m_Port;
		}
	
	return 0;
	}


// Entry Points

void MCALL CBMikeLSFinalLenSlaveUdpDriver::Service(void)
{	
	m_pSock[0]->Listen(1001);

	m_pSock[1]->Listen(m_RxMac.m_Port);

	if( !m_fTF ) { 

		Begin();

		AddByte('.');

		Send();

		Sleep(20);

		Begin();

		AddByte('T');
		AddByte('F');
			
		Send();
		}

	IPADDR IP;
	
	WORD Port;

	m_pSock[1]->GetLocal(IP, Port);

	if( Port != m_RxMac.m_Port ) {

		m_pSock[1]->Close();

		m_pSock[1]->Listen(m_RxMac.m_Port);
		}

	if( RecvFrame() ) {

		m_dwFL = GetReal();
		m_dwQF = GetDec();

		m_fTF = TRUE;
		}

	m_pSock[1]->Close();

	m_pSock[0]->Close();
	}

CCODE MCALL CBMikeLSFinalLenSlaveUdpDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Reg;

	Reg.a.m_Table = addrNamed;
	Reg.a.m_Extra = 0;
	Reg.a.m_Offset = 2;
	Reg.a.m_Type = addrRealAsReal;

	if( Reg.m_Ref == Addr.m_Ref ) {

		pData[0] = m_dwFL;

		return 1;
		}

	Reg.a.m_Offset++;
	Reg.a.m_Type = addrByteAsByte;

	if( Reg.m_Ref == Addr.m_Ref ) {

		pData[0] = m_dwQF;

		return 1;
		}

	return CCODE_ERROR | CCODE_HARD | CCODE_SILENT;
	}

CCODE MCALL CBMikeLSFinalLenSlaveUdpDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return CCODE_ERROR | CCODE_HARD | CCODE_SILENT;
	}

// Implementation

DWORD CBMikeLSFinalLenSlaveUdpDriver::GetReal(void)
{
	float dwFloat = 0.0;

	BOOL fNeg = FALSE;

	BOOL fDP  = FALSE;

	UINT uDiv = 10;

	for( m_uPtr = 0; m_bRx[m_uPtr] != CR && m_uPtr < sizeof(m_bRx); m_uPtr++ ) {

		if( m_bRx[m_uPtr] == '-' ) {

			fNeg = TRUE;

			continue;
			}

		if( m_bRx[m_uPtr] == '.' ) {

			fDP = TRUE;

			continue;
			}

		if( m_bRx[m_uPtr] == ',' ) {		

			break;
			}

		BYTE bByte = m_bRx[m_uPtr] - 0x30;

		if( bByte <= 9 ) {

			if( !fDP ) {

				dwFloat *= 10;

				dwFloat += bByte;
				}
			else {
				float Add = float(bByte) / float(uDiv);
				
				dwFloat += Add;

				uDiv *= 10;
				}
	   		}
		}

	if( fNeg ) {

		dwFloat = -dwFloat;
		}

	return R2I(dwFloat);
	}

DWORD CBMikeLSFinalLenSlaveUdpDriver::GetDec(void)
{
	BOOL fNeg = FALSE;

	DWORD dwData = 0;

	while( m_bRx[m_uPtr] != CR && m_uPtr < sizeof(m_bRx) ) {

		if( m_bRx[m_uPtr] == '+' ) {

			m_uPtr++;

			continue;
			}

		if( m_bRx[m_uPtr] == '-' ) {

			fNeg = TRUE;

			m_uPtr++;

			continue;
			}

		if( m_bRx[m_uPtr] == '=' ) {

			dwData = 0;
			
			m_uPtr++;

			continue;
			}

		if( m_bRx[m_uPtr] == ',' ) {

			m_uPtr++;

			continue;
			}

		BYTE bByte = FromAscii(m_bRx[m_uPtr]);

		if( bByte < 10 ) {

			dwData = dwData * 10;

			dwData += FromAscii(m_bRx[m_uPtr]);
			}

		m_uPtr++;
		}

	return fNeg ? -dwData : dwData;
	}

BYTE CBMikeLSFinalLenSlaveUdpDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' ) {
	
		if( bByte <= '9' ) {

			return bByte - '0';
			}

		return	bByte - '@' + 9;
		}

	return bByte;
	}

BOOL CBMikeLSFinalLenSlaveUdpDriver::Begin(void)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();

		m_uPtr = 0;

		return TRUE;
		}
	
	Sleep(10);

	return FALSE;
	}

void CBMikeLSFinalLenSlaveUdpDriver::AddByte(BYTE bByte)
{
	m_pTxData[m_uPtr++] = bByte;	
	}

BOOL CBMikeLSFinalLenSlaveUdpDriver::EndRealTime(void)
{
	if( (m_pTxBuff = CreateBuffer(600, TRUE)) ) {

		m_pTxBuff->AddTail(600);

		m_pTxData = m_pTxBuff->GetData();
	
		m_uPtr = 0;

		AddByte('.');

		AddByte(CR);

		m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uPtr + 1);
	
		AddTransportHeader();
	
		for( UINT n = 0; n < 5; n++ ) {

			if( m_pSock[0]->Send(m_pTxBuff) == S_OK ) {

				m_pTxBuff = NULL;

				m_dwFL = 0;
				
				m_dwQF = 0;

				m_fTF  = FALSE;

				return TRUE;
				}
		
			Sleep(5);
			}

		m_pTxBuff->Release();

		m_pTxBuff = NULL;
		}

	return FALSE;
	}


// Transport

BOOL CBMikeLSFinalLenSlaveUdpDriver::Send(void)
{
	m_pTxBuff->StripTail(m_pTxBuff->GetSize() - m_uPtr);
	
	AddTransportHeader();
	
	for( UINT n = 0; n < 5; n++ ) {

		if( m_pSock[0]->Send(m_pTxBuff) == S_OK ) {

			m_pTxBuff = NULL;
			
			return TRUE;
			}
		
		Sleep(5);
		}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBMikeLSFinalLenSlaveUdpDriver::RecvFrame(void)
{
	for( UINT uTry = 0; uTry < 3; uTry++ ) {

		SetTimer(5000);

		while( GetTimer() ) {
		
			m_pSock[1]->Recv(m_pRxBuff);

			if( m_pRxBuff ) {

				IPADDR IP;

				WORD Port;
	
				m_pSock[1]->GetLocal(IP, Port);

				if( Port != m_RxMac.m_Port ) {

					m_pRxBuff->Release();

					m_pRxBuff = NULL;

					return FALSE;
					}

				if( StripTransportHeader() ) {

					PBYTE pData = m_pRxBuff->GetData();

					m_uPtr = m_pRxBuff->GetSize();

					if( m_uPtr ) {

						for( UINT u = 0; u < m_uPtr; u++ ) {

							m_bRx[u] = pData[u];
							}

						for( UINT n = 0; n < 5; n++ ) {

							if( m_bRx[n] == 0xFF ) {

								if( n == 4 ) {

									m_pRxBuff->Release();

									m_pRxBuff = NULL;

									return FALSE;
									}

								continue;
								}
							}

						m_bRx[m_uPtr++] = CR;
					
						m_pRxBuff->Release();

						m_pRxBuff = NULL;
				
						return TRUE;
						}
					}
					
				m_pRxBuff->Release();

				m_pRxBuff = NULL;
				}

			Sleep(5);
			}
		}

	m_fTF = FALSE;

	return FALSE;
	}

// Transport Header
		
void CBMikeLSFinalLenSlaveUdpDriver::AddTransportHeader(void)
{
	DWORD IP	= m_RxMac.m_IP;

	WORD Port	= 1001;

	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 6;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	pData[0] = PBYTE(&IP)[0];
	pData[1] = PBYTE(&IP)[1];
	pData[2] = PBYTE(&IP)[2];
	pData[3] = PBYTE(&IP)[3];

	pData[4] = HIBYTE(Port);
	pData[5] = LOBYTE(Port);
	}

BOOL CBMikeLSFinalLenSlaveUdpDriver::StripTransportHeader(void)
{	
	PBYTE pData = m_pRxBuff->GetData();

	CMacAddr Addr;

	((PBYTE) &Addr.m_IP)[0] = pData[0];
	((PBYTE) &Addr.m_IP)[1] = pData[1];
	((PBYTE) &Addr.m_IP)[2] = pData[2];
	((PBYTE) &Addr.m_IP)[3] = pData[3];

	Addr.m_Port = MAKEWORD(pData[5], pData[4]);

	if( Addr.m_IP == m_RxMac.m_IP && Addr.m_Port == 1002 ) {

		m_pRxBuff->StripHead(6);

		return TRUE;
		}

	return FALSE;
	}

// Helpers

BOOL CBMikeLSFinalLenSlaveUdpDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CBMikeLSFinalLenSlaveUdpDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CBMikeLSFinalLenSlaveUdpDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}


// End of File
