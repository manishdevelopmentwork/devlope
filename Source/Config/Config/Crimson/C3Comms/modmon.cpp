
#include "intern.hpp"

#include "modmon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor Base Driver
//

// Constructor

CModbusMonitorDriver::CModbusMonitorDriver(void)
{
	AddSpaces();
	}

// Configuration

CLASS CModbusMonitorDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Implementation

void CModbusMonitorDriver::AddSpaces(void)
{
	AddSpace(New CSpace(2, "4", "Holding Registers", 10,  1, 65535, addrLongAsLong));

	AddSpace(New CSpace(1, "3", "Analog Inputs",	 10,  1, 65535, addrLongAsLong));

	AddSpace(New CSpace(3, "0", "Digital Coils",	 10,  1, 65535, addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor RTU Driver
//

// Instantiator

ICommsDriver *	Create_ModbusMonitorRTUDriver(void)
{
	return New CModbusMonitorRTUDriver;
	}

// Constructor

CModbusMonitorRTUDriver::CModbusMonitorRTUDriver(void)
{
	m_wID		= 0x3405;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "RTU Monitor";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Modbus RTU Monitor";
	}

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor ASCII Driver
//

// Instantiator

ICommsDriver *	Create_ModbusMonitorASCIIDriver(void)
{
	return New CModbusMonitorASCIIDriver;
	}

// Constructor

CModbusMonitorASCIIDriver::CModbusMonitorASCIIDriver(void)
{
	m_wID		= 0x3406;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Modbus";
	
	m_DriverName	= "ASCII Monitor";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Modbus ASCII Monitor";
	}

// End of File
