
//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Stack
//

#include "CipCommon.hpp"

#include "CipClient.hpp"

#include "CipConnection.hpp"

#include "CipPath.hpp"

#include "EthernetIp.hpp"

#include "TcpIpSocket.hpp"

#include "WatchList.hpp"

#include "TagInfo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Driver
//

class CEthernetIpDriver : public CMasterDriver
{
	public:
		// Constructor
		CEthernetIpDriver(void);

		// Destructor
		~CEthernetIpDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Access
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:

		// Device Config
		struct CCtx
		{
			DWORD	      m_Addr;
			UINT	      m_Port;
			UINT          m_uTags;
			CTagInfo    * m_pTags;
			CWatchList  * m_pHead;
			CWatchList  * m_pTail;
			BOOL	      m_fOpen;
			CTcpIpSock  * m_pSocket;
			CEthernetIp * m_pSession;
			CCipConnect * m_pConnect;
			CCipClient  * m_pClient;
			PTXT	      m_pRoute;
			BOOL	      m_fDirty;
			};

		// Current Device
		CCtx * m_pCtx;

		// CIP Stack
		void CreateStack(void);
		BOOL CheckStack(void);
		BOOL OpenStack(void);

		// Implementation
		UINT TouchTags(void);
		void ClearTagTouches(void);
		BOOL UpdateWatchLists(void);
		void CleanupEmptyWatchLists(void);
		void FreeWatchLists(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
	};

// End of File
