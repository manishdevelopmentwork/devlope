
#include "intern.hpp"

#include "cattest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAT Test Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CCatLinkDriver);

// Constructor

CCatLinkDriver::CCatLinkDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_bSelf  = 0x55;

	m_bDummy = 0x56;
	}

// Destructor

CCatLinkDriver::~CCatLinkDriver(void)
{
	}

// Configuration

void MCALL CCatLinkDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CCatLinkDriver::CheckConfig(CSerialConfig &Config)
{
	// NOTE : Link actually runs at 62,500 Baud, but the
	// port object doesn't know it's got a 4MHz crystal.

	Config.m_uBaudRate = 56000;
	Config.m_uDataBits = 8;
	Config.m_uStopBits = 1;
	Config.m_uParity   = parityNone;
	Config.m_uFlags    = flagFastRx | flagTimeout;
	Config.m_uPhysical = 2;
	}
	
// Management

void MCALL CCatLinkDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCatLinkHandler(m_pHelper, this, m_bSelf);

	pPort->Bind(m_pHandler);
	}

void MCALL CCatLinkDriver::Detach(void)
{
	m_pHandler->Stop();

	m_pHandler->Release();
	}

void MCALL CCatLinkDriver::Open(void)
{
	memset(m_Data,  0, sizeof(m_Data));

	m_uScan   = 0;

	m_uHead   = 0;

	m_uTail   = 0;

	m_pHandler->Start();
	}

// Device

CCODE MCALL CCatLinkDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

static WORD xxx = 0;

void MCALL CCatLinkDriver::Service(void)
{
	static UINT uLast = 0;

	if( !uLast ) uLast = GetTickCount();

	if( GetTickCount() - uLast > ToTicks(1000) ) {

		QueueSend((CDataItem *) 0x0E);

		QueueSend((CDataItem *) 0x0D);

		uLast = GetTickCount();
		}

	if( xxx ) {

		QueueSend((CDataItem *) DWORD(xxx));

		xxx = 0;
		}

	UpdateItems();
	}

CCODE MCALL CCatLinkDriver::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CDataItem *pItem = NULL;

	UINT       uName = Addr.a.m_Offset;

	UINT       uType = Addr.a.m_Table;

	UINT       uFunc = 0;

	if( uType >= addrNamed ) {

		uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

		uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;
		}

	switch( uType ) {

		case 1:	uName += 0x000000; break;
		case 2:	uName += 0xD00000; break;
		case 3:	uName += 0xD10000; break;
		}

	if( IsValidName(uName) ) {

		if( (pItem = GetItem(uName)) ) {

			if( uFunc == 0 ) {

				switch( pItem->m_wState ) {

					case stateIdle:	

						SetTime(pItem, timeDelay);

						pItem->m_wState = stateDelay;

						break;
					}

				*pData = pItem->m_Data;

				return 1;
				}
			}
		}

	*pData = 0;

	return 1;
	}

CCODE MCALL CCatLinkDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CDataItem *pItem = NULL;

	UINT       uName = Addr.a.m_Offset;

	UINT       uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

	UINT       uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;

	switch( uType ) {

		case 1:	uName += 0x000000; break;
		case 2:	uName += 0xD00000; break;
		case 3:	uName += 0xD10000; break;
		}

	if( IsValidName(uName) ) {

		if( (pItem = GetItem(uName)) ) {

			if( uFunc == 0 ) {

				switch( pItem->m_wState ) {

					case stateIdle:	

						SetTime(pItem, timeDelay);

						pItem->m_wState = stateDelay;

						break;
					}

				pItem->m_Data = *pData;

				return 1;
				}
			}
		}

	return 1;
	}

// State Machine

void CCatLinkDriver::UpdateItems(void)
{
	for( UINT n = 0; n < elements(m_Data); n++ ) {

		UINT uName = GetName(n);

		if( IsValidName(uName) ) {

			CDataItem *pItem = m_Data + n;

			if( pItem->m_wState ) {

				UpdateItem(uName, pItem);
				}
			}
		}
	}

void CCatLinkDriver::UpdateItem(UINT uName, CDataItem *pItem)
{
	switch( pItem->m_wState ) {

		case stateDelay:

			if( TimeOut(pItem) ) {

				if( uName == 0x0040 ) {

					SetTime(pItem, timePeriodic);
					
					pItem->m_bFrom  = m_bDummy;

					pItem->m_wState = statePeriodic;
					}
				else
					pItem->m_wState = stateListen;
				}
			break;

		case stateListen:

			if( pItem->m_bPoll ) {

				if( !QueueSend(pItem) ) {

					pItem->m_wState = stateRetry;

					SetTime(pItem, timeRetry);
					}

				pItem->m_bPoll = 0;
				}
			break;

		case stateRetry:

			if( TimeOut(pItem) ) {

				if( !QueueSend(pItem) ) {

					SetTime(pItem, timeRetry);

					break;
					}

				pItem->m_wState = stateListen;
				}
			break;

		case statePeriodic:

			if( TimeOut(pItem) ) {

				if( QueueSend(pItem) ) {

					SetTime(pItem, timePeriodic);

					break;
					}

				SetTime(pItem, timeRetry);
				}
			break;
		}
	}

void CCatLinkDriver::SetTime(CDataItem *pItem, UINT uTime)
{
	pItem->m_uTime = GetTickCount() + ToTicks(uTime);
	}

BOOL CCatLinkDriver::TimeOut(CDataItem *pItem)
{
	return int(GetTickCount() - pItem->m_uTime) >= 0;
	}

BOOL CCatLinkDriver::QueueSend(CDataItem *pItem)
{
	UINT uNext = (m_uTail + 1) % elements(m_pSend);

	if( uNext != m_uHead ) {

		m_pSend[m_uTail] = pItem;

		m_uTail          = uNext;

		return TRUE;
		}

	return FALSE;
	}

// Handler Calls

void CCatLinkDriver::SeePoll(BYTE bSrc, UINT uName)
{
	CDataItem *pItem = GetItem(uName);

	if( pItem ) {

		switch( pItem->m_wState ) {

			case stateListen:
			case stateRetry:

				pItem->m_bFrom = bSrc;

				pItem->m_bPoll = 1;

				break;

			case statePeriodic:

				pItem->m_bFrom = bSrc;

				break;
			}
		}
	}

void CCatLinkDriver::SeeFault(WORD wCode)
{
	AfxTrace("%4.4X (%4.4X)\n", wCode, xxx);

	xxx = wCode;
	}

UINT CCatLinkDriver::GetSend(PBYTE pData)
{
	if( m_uHead != m_uTail ) {

		CDataItem *pItem = m_pSend[m_uHead];

		UINT       uSize = 0;

		if( HIWORD(pItem) ) { 

			UINT uName    = GetName(pItem - m_Data);

			UINT uData    = pItem->m_Data;

			UINT uNameLen = GetNameLen(uName);

			UINT uDataLen = GetDataLen(uName);

			pData[uSize++] = m_bSelf;

			pData[uSize++] = pItem->m_bFrom;

			SetName(pData + uSize, uName, uNameLen);
			
			uSize += uNameLen;

			SetData(pData + uSize, uData, uDataLen);
			
			uSize += uDataLen;
			}
		else {
			UINT uType = LOWORD(pItem);

			if( uType == 0x0D ) {

				static UINT x = 0;

				pData[uSize++] = m_bSelf;

				pData[uSize++] = 0x08;

				pData[uSize++] = 0xFA;

				pData[uSize++] = 0x0D;

				if( x % 16 == 0 ) {

					static BYTE b[] = { 0x70, 0x00, 0x11, 0x25, 0x04, 0x02, 0x0C, 0x24, 0x06 };

					pData[uSize++] = 0x01 + sizeof(b);

					pData[uSize++] = x++;

					memcpy(pData + uSize, b, sizeof(b));

					uSize += sizeof(b);
					}
				else {
					pData[uSize++] = 0x02;

					pData[uSize++] = x++;

					pData[uSize++] = 0x80;
					}
				}

			if( uType == 0x0E ) {

				static UINT x = 0;

				pData[uSize++] = m_bSelf;

				pData[uSize++] = 0x08;

				pData[uSize++] = 0xFA;

				pData[uSize++] = 0x0E;

				if( x % 16 == 0 ) {

					static BYTE b[] = { 0x70, 0x01, 0x08, 0x64, 0x01, 0x0C, 0x66, 0x02, 0x9A, 0x26 };

					pData[uSize++] = 0x01 + sizeof(b);

					pData[uSize++] = x++;

					memcpy(pData + uSize, b, sizeof(b));

					uSize += sizeof(b);
					}
				else {
					pData[uSize++] = 0x02;

					pData[uSize++] = x++;

					pData[uSize++] = 0x80;
					}
				}

			if( uType & 0x4000 ) {

				AfxTrace("Send %4.4X\n", uType);

				static UINT x = 0;

				pData[uSize++] = m_bSelf;

				pData[uSize++] = 0xAA;

				pData[uSize++] = 0xFA;

				pData[uSize++] = 0x10;

				pData[uSize++] = 0x0E;

				pData[uSize++] = HIBYTE(uType & 0x3FFF);

				pData[uSize++] = LOBYTE(uType & 0x3FFF);

				pData[uSize++] = 0x25;

				pData[uSize++] = 0x02;

				pData[uSize++] = 222;
				pData[uSize++] = 0;

				DWORD f = 100;
				
				DWORD l = (1000 - (uType & 0x3FFF)) * 3600;

				pData[uSize++] = LOBYTE(LOWORD(f));
				pData[uSize++] = HIBYTE(LOWORD(f));
				pData[uSize++] = LOBYTE(HIWORD(f));
				pData[uSize++] = HIBYTE(HIWORD(f));

				pData[uSize++] = LOBYTE(LOWORD(l));
				pData[uSize++] = HIBYTE(LOWORD(l));
				pData[uSize++] = LOBYTE(HIWORD(l));
				pData[uSize++] = HIBYTE(HIWORD(l));
				}

			if( uType & 0x8000 ) {

				AfxTrace("Send %4.4X\n", uType);

				static UINT x = 0;

				pData[uSize++] = m_bSelf;

				pData[uSize++] = 0xAA;

				pData[uSize++] = 0xFA;

				pData[uSize++] = 0x12;

				pData[uSize++] = 0x0E;

				pData[uSize++] = HIBYTE(uType & 0x3FFF);

				pData[uSize++] = LOBYTE(uType & 0x3FFF);

				pData[uSize++] = 0x25;

				pData[uSize++] = 0x02;

				pData[uSize++] = 111;
				pData[uSize++] = 0;

				DWORD f = 100;
				
				DWORD l = (1000 - (uType & 0x3FFF)) * 3600;

				pData[uSize++] = LOBYTE(LOWORD(f));
				pData[uSize++] = HIBYTE(LOWORD(f));
				pData[uSize++] = LOBYTE(HIWORD(f));
				pData[uSize++] = HIBYTE(HIWORD(f));

				pData[uSize++] = LOBYTE(LOWORD(l));
				pData[uSize++] = HIBYTE(LOWORD(l));
				pData[uSize++] = LOBYTE(HIWORD(l));
				pData[uSize++] = HIBYTE(HIWORD(l));
				}
			}

		if( uSize ) {

			BYTE bSum = 0;

			for( UINT n = 0; n < uSize; n++ ) {

				bSum += pData[n];
				}

			pData[n] = BYTE(256 - bSum);

			uSize    = uSize + 1;
			}

		return uSize;
		}

	return 0;
	}

BOOL CCatLinkDriver::GetNext(void)
{
	if( m_uHead != m_uTail ) {

		m_uHead = (m_uHead + 1) % elements(m_pSend);

		return TRUE;
		}

	return FALSE;
	}

// Data Item Access

CCatLinkDriver::CDataItem * CCatLinkDriver::GetItem(UINT uName)
{
	if( uName >= 0x000000 && uName <= 0x00007F ) {

		return m_Data + 0x0000 + (uName - 0x000000);
		}

	if( uName >= 0xD00000 && uName <= 0xD00FFF ) {

		return m_Data + 0x0080 + (uName - 0xD00000);
		}
	
	if( uName >= 0xD10000 && uName <= 0xD10FFF ) {

		return m_Data + 0x1080 + (uName - 0xD10000);
		}

	if( uName >= 0x00F000 && uName <= 0x00FFFF ) {

		return m_Data + 0x2080 + (uName - 0x00F000);
		}

	if( uName >= 0xD01000 && uName <= 0xD01FFF ) {

		return m_Data + 0x3080 + (uName - 0xD01000);
		}

	return NULL;
	}

// Item Name Helpers

BOOL CCatLinkDriver::IsValidName(UINT uName)
{
	switch( uName ) {
		
		case 0x0001:
		case 0x0002:
		case 0x0003:
		case 0x0007:
		case 0x0008:
		case 0x000C:
		case 0x000D:
		case 0x000E:
		case 0x000F:
		case 0x0011:
		case 0x0015:
		case 0x0016:
		case 0x0017:
		case 0x0018:
		case 0x0040:
		case 0x0041:
		case 0x0042:
		case 0x0044:
		case 0x0045:
		case 0x0046:
		case 0x0047:
		case 0x004B:
		case 0x004D:
		case 0x004E:
		case 0x0053:
		case 0x0054:
		case 0x0055:
		case 0x0058:
		case 0x0059:
		case 0x005A:
		case 0x005B:
		case 0x005C:
		case 0x005D:
		case 0x005E:
		case 0x005F:
		case 0x0080:
		case 0x0082:
		case 0x0083:
		case 0x0084:
		case 0x0086:
		case 0x00C8:
		case 0xF001:
		case 0xF002:
		case 0xF00D:
		case 0xF00E:
		case 0xF013:
		case 0xF014:
		case 0xF016:
		case 0xF018:
		case 0xF01B:
		case 0xF021:
		case 0xF022:
		case 0xF023:
		case 0xF024:
		case 0xF025:
		case 0xF026:
		case 0xF027:
		case 0xF029:
		case 0xF02A:
		case 0xF02B:
		case 0xF02C:
		case 0xF02D:
		case 0xF031:
		case 0xF032:
		case 0xF062:
		case 0xF066:
		case 0xF074:
		case 0xF07C:
		case 0xF08F:
		case 0xF090:
		case 0xF094:
		case 0xF09C:
		case 0xF0A6:
		case 0xF0A8:
		case 0xF0A9:
		case 0xF0AA:
		case 0xF0AC:
		case 0xF0B0:
		case 0xF0B1:
		case 0xF0B2:
		case 0xF0B3:
		case 0xF0B4:
		case 0xF0B5:
		case 0xF0B6:
		case 0xF0BA:
		case 0xF0C1:
		case 0xF0C2:
		case 0xF0E8:
		case 0xF0F1:
		case 0xF0F2:
		case 0xF0FD:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF111:
		case 0xF112:
		case 0xF113:
		case 0xF115:
		case 0xF116:
		case 0xF117:
		case 0xF118:
		case 0xF119:
		case 0xF11B:
		case 0xF11C:
		case 0xF11D:
		case 0xF121:
		case 0xF122:
		case 0xF123:
		case 0xF124:
		case 0xF125:
		case 0xF127:
		case 0xF129:
		case 0xF14F:
		case 0xF153:
		case 0xF154:
		case 0xF189:
		case 0xF191:
		case 0xF192:
		case 0xF1AD:
		case 0xF1C4:
		case 0xF1D0:
		case 0xF1D1:
		case 0xF1D3:
		case 0xF1D4:
		case 0xF1D5:
		case 0xF1D6:
		case 0xF212:
		case 0xF213:
		case 0xF22A:
		case 0xF22B:
		case 0xF22C:
		case 0xF231:
		case 0xF24D:
		case 0xF24F:
		case 0xF257:
		case 0xF287:
		case 0xF28A:
		case 0xF290:
		case 0xF299:
		case 0xF2B3:
		case 0xF2C3:
		case 0xF2C6:
		case 0xF2CB:
		case 0xF2CC:
		case 0xF2CE:
		case 0xF2D4:
		case 0xF2D5:
		case 0xF2D6:
		case 0xF2D7:
		case 0xF2DD:
		case 0xF2DE:
		case 0xF2F6:
		case 0xF2F7:
		case 0xF2FA:
		case 0xF2FC:
		case 0xF2FD:
		case 0xF30B:
		case 0xF30C:
		case 0xF312:
		case 0xF313:
		case 0xF402:
		case 0xF40E:
		case 0xF410:
		case 0xF411:
		case 0xF412:
		case 0xF413:
		case 0xF415:
		case 0xF417:
		case 0xF419:
		case 0xF41C:
		case 0xF41F:
		case 0xF420:
		case 0xF421:
		case 0xF422:
		case 0xF425:
		case 0xF426:
		case 0xF427:
		case 0xF428:
		case 0xF42A:
		case 0xF42C:
		case 0xF42E:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF442:
		case 0xF443:
		case 0xF444:
		case 0xF445:
		case 0xF446:
		case 0xF447:
		case 0xF448:
		case 0xF449:
		case 0xF44A:
		case 0xF44B:
		case 0xF44C:
		case 0xF44D:
		case 0xF44E:
		case 0xF44F:
		case 0xF450:
		case 0xF451:
		case 0xF452:
		case 0xF453:
		case 0xF455:
		case 0xF456:
		case 0xF457:
		case 0xF45B:
		case 0xF45C:
		case 0xF45D:
		case 0xF45E:
		case 0xF45F:
		case 0xF460:
		case 0xF461:
		case 0xF462:
		case 0xF463:
		case 0xF464:
		case 0xF465:
		case 0xF466:
		case 0xF467:
		case 0xF468:
		case 0xF469:
		case 0xF46A:
		case 0xF46B:
		case 0xF46C:
		case 0xF46D:
		case 0xF46F:
		case 0xF477:
		case 0xF478:
		case 0xF48D:
		case 0xF48F:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4A4:
		case 0xF4A5:
		case 0xF4B2:
		case 0xF4B3:
		case 0xF4B4:
		case 0xF4B5:
		case 0xF4B6:
		case 0xF4C3:
		case 0xF4C4:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4CF:
		case 0xF4D0:
		case 0xF4D1:
		case 0xF4D2:
		case 0xF4D3:
		case 0xF4D4:
		case 0xF4D5:
		case 0xF4E1:
		case 0xF4EA:
		case 0xF4F7:
		case 0xF4F8:
		case 0xF4FE:
		case 0xF4FF:
		case 0xF508:
		case 0xF509:
		case 0xF50A:
		case 0xF50B:
		case 0xF50C:
		case 0xF50D:
		case 0xF50E:
		case 0xF50F:
		case 0xF510:
		case 0xF511:
		case 0xF512:
		case 0xF513:
		case 0xF514:
		case 0xF515:
		case 0xF516:
		case 0xF517:
		case 0xF518:
		case 0xF519:
		case 0xF51A:
		case 0xF51B:
		case 0xF51C:
		case 0xF51D:
		case 0xF51E:
		case 0xF51F:
		case 0xF520:
		case 0xF524:
		case 0xF525:
		case 0xF527:
		case 0xF53E:
		case 0xF540:
		case 0xF541:
		case 0xF542:
		case 0xF54F:
		case 0xF557:
		case 0xF55A:
		case 0xF55B:
		case 0xF55C:
		case 0xF55D:
		case 0xF55E:
		case 0xF55F:
		case 0xF560:
		case 0xF561:
		case 0xF562:
		case 0xF563:
		case 0xF564:
		case 0xF565:
		case 0xF566:
		case 0xF567:
		case 0xF568:
		case 0xF569:
		case 0xF56A:
		case 0xF56B:
		case 0xF56C:
		case 0xF56D:
		case 0xF56E:
		case 0xF56F:
		case 0xF570:
		case 0xF571:
		case 0xF572:
		case 0xF573:
		case 0xF574:
		case 0xF575:
		case 0xF577:
		case 0xF578:
		case 0xF579:
		case 0xF57B:
		case 0xF57C:
		case 0xF57E:
		case 0xF57F:
		case 0xF58B:
		case 0xF58D:
		case 0xF58E:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF597:
		case 0xF598:
		case 0xF599:
		case 0xF59A:
		case 0xF59B:
		case 0xF59C:
		case 0xF59D:
		case 0xF59E:
		case 0xF59F:
		case 0xF5A0:
		case 0xF5A1:
		case 0xF5A2:
		case 0xF5A3:
		case 0xF5A4:
		case 0xF5A5:
		case 0xF5A6:
		case 0xF5A7:
		case 0xF5A8:
		case 0xF5A9:
		case 0xF5AA:
		case 0xF5AB:
		case 0xF5B1:
		case 0xF5B7:
		case 0xF5B8:
		case 0xF5BA:
		case 0xF5BB:
		case 0xF5C4:
		case 0xF5C9:
		case 0xF5D1:
		case 0xF5D6:
		case 0xF5D8:
		case 0xF5D9:
		case 0xF5DA:
		case 0xF5DB:
		case 0xF5E0:
		case 0xF602:
		case 0xF603:
		case 0xF604:
		case 0xF605:
		case 0xF61E:
		case 0xF62B:
		case 0xF636:
		case 0xF701:
		case 0xF702:
		case 0xF703:
		case 0xF704:
		case 0xF705:
		case 0xF706:
		case 0xF707:
		case 0xF708:
		case 0xF709:
		case 0xF70A:
		case 0xF70B:
		case 0xF70C:
		case 0xF70D:
		case 0xF70E:
		case 0xF70F:
		case 0xF710:
		case 0xF711:
		case 0xF7FA:
		case 0xF7FC:
		case 0xF803:
		case 0xF806:
		case 0xF810:
		case 0xF811:
		case 0xF814:
		case 0xF81A:
		case 0xF81C:
		case 0xFC07:
		case 0xFC08:
		case 0xFC09:
		case 0xFC0D:
		case 0xFC0F:
		case 0xFC10:
		case 0xFC11:
		case 0xFC12:
		case 0xFC13:
		case 0xFC14:
		case 0xFC15:
		case 0xFC16:
		case 0xFC17:
		case 0xFC18:
		case 0xFC19:
		case 0xFC1A:
		case 0xFC1B:
		case 0xFC1C:
		case 0xFC1D:
		case 0xFC1E:
		case 0xFC1F:
		case 0xFC21:
		case 0xFC22:
		case 0xFC27:
		case 0xFC28:
		case 0xFC29:
		case 0xFC2D:
		case 0xFC2E:
		case 0xFC32:
		case 0xFC33:
		case 0xFC88:
		case 0xD00020:
		case 0xD00021:
		case 0xD00022:
		case 0xD00023:
		case 0xD00024:
		case 0xD00025:
		case 0xD00026:
		case 0xD00027:
		case 0xD00028:
		case 0xD00029:
		case 0xD0002A:
		case 0xD0002B:
		case 0xD0002C:
		case 0xD0002D:
		case 0xD0002E:
		case 0xD0002F:
		case 0xD00030:
		case 0xD00031:
		case 0xD00032:
		case 0xD00033:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:
		case 0xD00050:
		case 0xD00051:
		case 0xD00052:
		case 0xD00053:
		case 0xD000EB:
		case 0xD000EC:
		case 0xD000ED:
		case 0xD000EE:
		case 0xD000EF:
		case 0xD000F0:
		case 0xD000F1:
		case 0xD000F2:
		case 0xD000F3:
		case 0xD000F4:
		case 0xD000F5:
		case 0xD000F6:
		case 0xD000F7:
		case 0xD000F8:
		case 0xD000F9:
		case 0xD000FA:
		case 0xD000FB:
		case 0xD000FC:
		case 0xD000FD:
		case 0xD000FE:
		case 0xD00109:
		case 0xD0010A:
		case 0xD0012F:
		case 0xD00130:
		case 0xD00131:
		case 0xD00148:
		case 0xD00149:
		case 0xD0014A:
		case 0xD0014B:
		case 0xD0014C:
		case 0xD0014D:
		case 0xD0014E:
		case 0xD0014F:
		case 0xD00150:
		case 0xD00151:
		case 0xD00188:
		case 0xD00189:
		case 0xD001A1:
		case 0xD001C7:
		case 0xD00259:
		case 0xD00260:
		case 0xD00261:
		case 0xD00262:
		case 0xD0027D:
		case 0xD0027E:
		case 0xD0027F:
		case 0xD00281:
		case 0xD002AE:
		case 0xD0036F:
		case 0xD00370:
		case 0xD00371:
		case 0xD00375:
		case 0xD00377:
		case 0xD00378:
		case 0xD00379:
		case 0xD003A5:
		case 0xD003A6:
		case 0xD003E0:
		case 0xD003E1:
		case 0xD003E2:
		case 0xD003E3:
		case 0xD003E4:
		case 0xD003E5:
		case 0xD003E6:
		case 0xD003E7:
		case 0xD003E8:
		case 0xD003E9:
		case 0xD003EA:
		case 0xD003EB:
		case 0xD003EC:
		case 0xD003ED:
		case 0xD003EE:
		case 0xD003EF:
		case 0xD003F0:
		case 0xD003F1:
		case 0xD003F2:
		case 0xD003F3:
		case 0xD003F4:
		case 0xD003F5:
		case 0xD003F6:
		case 0xD003F7:
		case 0xD003F8:
		case 0xD003F9:
		case 0xD003FA:
		case 0xD003FB:
		case 0xD003FC:
		case 0xD003FD:
		case 0xD003FE:
		case 0xD003FF:
		case 0xD00400:
		case 0xD00401:
		case 0xD00402:
		case 0xD00403:
		case 0xD00404:
		case 0xD00405:
		case 0xD00406:
		case 0xD00407:
		case 0xD00418:
		case 0xD00419:
		case 0xD0041A:
		case 0xD00453:
		case 0xD00478:
		case 0xD00479:
		case 0xD0047A:
		case 0xD0047B:
		case 0xD0047C:
		case 0xD0047D:
		case 0xD0047E:
		case 0xD0047F:
		case 0xD00480:
		case 0xD00481:
		case 0xD00482:
		case 0xD00483:
		case 0xD004B5:
		case 0xD004B6:
		case 0xD004B7:
		case 0xD004DB:
		case 0xD004DC:
		case 0xD0050B:
		case 0xD0050C:
		case 0xD0050D:
		case 0xD0050E:
		case 0xD0050F:
		case 0xD00510:
		case 0xD00511:
		case 0xD00512:
		case 0xD00513:
		case 0xD00514:
		case 0xD00515:
		case 0xD00516:
		case 0xD00517:
		case 0xD00518:
		case 0xD00519:
		case 0xD0051A:
		case 0xD005B1:
		case 0xD005B2:
		case 0xD005B3:
		case 0xD005F5:
		case 0xD006BB:
		case 0xD0098D:
		case 0xD0099E:
		case 0xD0099F:
		case 0xD009A0:
		case 0xD009A1:
		case 0xD009A2:
		case 0xD009A3:
		case 0xD00AF4:
		case 0xD00AF5:
		case 0xD00AF6:
		case 0xD00B01:
		case 0xD00B18:
		case 0xD00B3D:
		case 0xD00B3E:
		case 0xD00BB3:
		case 0xD00BB7:
		case 0xD00C81:
		case 0xD00D14:
		case 0xD00D15:
		case 0xD00D16:
		case 0xD00D17:
		case 0xD00D18:
		case 0xD00D19:
		case 0xD00D3B:
		case 0xD00D5A:
		case 0xD00D61:
		case 0xD00E9D:
		case 0xD00EEC:
		case 0xD00F5D:
		case 0xD00F5E:
		case 0xD00F5F:
		case 0xD00F60:
		case 0xD00F75:
		case 0xD00FCA:
		case 0xD00FCB:
		case 0xD01075:
		case 0xD01076:
		case 0xD01078:
		case 0xD01079:
		case 0xD01092:
		case 0xD0109E:
		case 0xD0109F:
		case 0xD010F0:
		case 0xD010F1:
		case 0xD010F2:
		case 0xD010F3:
		case 0xD01124:
		case 0xD01125:
		case 0xD0112C:
		case 0xD0112D:
		case 0xD0113A:
		case 0xD0113B:
		case 0xD0113C:
		case 0xD0113D:
		case 0xD0113E:
		case 0xD0113F:
		case 0xD01140:
		case 0xD01141:
		case 0xD0114A:
		case 0xD0114B:
		case 0xD0114C:
		case 0xD0114D:
		case 0xD01152:
		case 0xD01154:
		case 0xD01153:
		case 0xD01155:
		case 0xD0115A:
		case 0xD0115C:
		case 0xD0115B:
		case 0xD0115D:
		case 0xD0115E:
		case 0xD01160:
		case 0xD0115F:
		case 0xD01161:
		case 0xD01164:
		case 0xD01165:
		case 0xD01192:
		case 0xD01209:
		case 0xD0120A:
		case 0xD0120B:
		case 0xD0120C:
		case 0xD0120D:
		case 0xD0120E:
		case 0xD0120F:
		case 0xD01255:
		case 0xD01257:
		case 0xD01258:
		case 0xD016E9:
		case 0xD016EA:
		case 0xD016EB:
		case 0xD016EC:
		case 0xD016ED:
		case 0xD016EE:
		case 0xD016EF:
		case 0xD016F0:
		case 0xD016F1:
		case 0xD016F2:
		case 0xD016F3:
		case 0xD016F4:
		case 0xD016F5:
		case 0xD016F6:
		case 0xD016F7:
		case 0xD10018:
		case 0xD1004A:
		case 0xD10050:
		case 0xD10051:
		case 0xD1005D:
		case 0xD10060:
		case 0xD10066:
		case 0xD1009A:
		case 0xD100A0:
		case 0xD100AA:
		case 0xD100AB:
		case 0xD100C5:
		case 0xD100C6:
		case 0xD100C7:
		case 0xD100C8:
		case 0xD100C9:
		case 0xD100CA:
		case 0xD100CD:
		case 0xD100CE:
		case 0xD100CF:
		case 0xD100EC:
		case 0xD10104:
		case 0xD10120:
		case 0xD1013A:
		case 0xD10167:
		case 0xD101EF:
		case 0xD101FE:
		case 0xD10230:
		case 0xD10293:
		case 0xD10986:
		case 0xD10987:
		case 0xD10D21:
		case 0xD10D22:
		case 0xD10D23:
		case 0xD10D24:
		case 0xD10D25:
		case 0xD10D26:
		case 0xD10D27:

			return TRUE;
		}

	return FALSE;
	}

UINT CCatLinkDriver::GetName(UINT uIndex)
{
	if( uIndex >= 0x0000 && uIndex <= 0x007F ) {

		return uIndex - 0x0000 + 0x000000;
		}

	if( uIndex >= 0x0080 && uIndex <= 0x107F ) {

		return uIndex - 0x0080 + 0xD00000;
		}

	if( uIndex >= 0x1080 && uIndex <= 0x207F ) {

		return uIndex - 0x1080 + 0xD10000;
		}

	if( uIndex >= 0x2080 && uIndex <= 0x307F ) {

		return uIndex - 0x2080 + 0x00F000;
		}

	if( uIndex >= 0x3080 && uIndex <= 0x407F ) {

		return uIndex - 0x3080 + 0xD01000;
		}

	return 0;
	}

// Framing Helpers

UINT CCatLinkDriver::GetNameLen(UINT uName)
{
	if( uName >= 0x01 && uName <= 0x1F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( uName >= 0x21 && uName <= 0x5F ) {

		// Data with 1-byte PID and 2-byte data field

		return 1;
		}

	if( LOBYTE(HIWORD(uName)) >= 0xD0 && LOBYTE(HIWORD(uName)) <= 0xD1 ) {

		// Data with 3-byte PID and 2-byte data field

		return 3;
		}

	if( HIBYTE(uName) >= 0xF0 && HIBYTE(uName) <= 0xF3 ) {

		// Data with 2-byte PID and 1-byte data field

		return 2;
		}

	if( HIBYTE(uName) >= 0xF4 && HIBYTE(uName) <= 0xF7 ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( HIBYTE(uName) >= 0xFC /* && bData <= 0xFF */ ) {

		// Data with 2-byte PID and 4-byte data field

		return 2;
		}

	return 0;
	}

UINT CCatLinkDriver::GetDataLen(UINT uName)
{
	if( uName >= 0x01 && uName <= 0x1F ) {

		// Data with 1-byte PID and 1-byte data field

		return 1;
		}

	if( uName >= 0x21 && uName <= 0x5F ) {

		// Data with 1-byte PID and 2-byte data field

		return 2;
		}

	if( LOBYTE(HIWORD(uName)) >= 0xD0 && LOBYTE(HIWORD(uName)) <= 0xD1 ) {

		// Data with 3-byte PID and 2-byte data field

		return 2;
		}

	if( HIBYTE(uName) >= 0xF0 && HIBYTE(uName) <= 0xF3 ) {

		// Data with 2-byte PID and 1-byte data field

		return 1;
		}

	if( HIBYTE(uName) >= 0xF4 && HIBYTE(uName) <= 0xF7 ) {

		// Data with 2-byte PID and 2-byte data field

		return 2;
		}

	if( HIBYTE(uName) >= 0xFC /* && bData <= 0xFF */ ) {

		// Data with 2-byte PID and 4-byte data field

		return 4;
		}

	return 0;
	}

void CCatLinkDriver::SetName(PBYTE pName, UINT uName, UINT uLen)
{
	if( uLen == 1 ) {

		pName[0] = LOBYTE(uName);
		}

	if( uLen == 2 ) {

		pName[0] = HIBYTE(uName);
		pName[1] = LOBYTE(uName);
		}
	
	if( uLen == 3 ) {

		pName[0] = LOBYTE(HIWORD(uName));
		pName[1] = HIBYTE(LOWORD(uName));
		pName[2] = LOBYTE(LOWORD(uName));
		}
	}

void CCatLinkDriver::SetData(PBYTE pData, UINT uData, UINT uLen)
{
	if( uLen == 1 ) {

		pData[0] = LOBYTE(uData);
		}

	if( uLen == 2 ) {

		pData[0] = LOBYTE(uData);
		pData[1] = HIBYTE(uData);
		}
	
	if( uLen == 4 ) {

		pData[0] = LOBYTE(LOWORD(uData));
		pData[1] = HIBYTE(LOWORD(uData));
		pData[2] = LOBYTE(HIWORD(uData));
		pData[3] = HIBYTE(HIWORD(uData));
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Custom Port Handler
//

// Constructor

CCatLinkHandler::CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver, BYTE bSelf)
{
	StdSetRef();

	m_pHelper = pHelper;

	m_pDriver = pDriver;

	m_bSelf   = bSelf;

	m_fRun    = FALSE;
	}

// Destructor

CCatLinkHandler::~CCatLinkHandler(void)
{
	}

// Operations

void CCatLinkHandler::Start(void)
{
	m_fSync   = FALSE;

	m_fSelf   = FALSE;

	m_uRxPtr  = 0;

	m_uTxPtr  = 0;

	m_uTxSize = 0;

	m_fRun    = TRUE;

	StartEdgeTimer(TRUE);
	}

void CCatLinkHandler::Stop(void)
{
	m_fRun = FALSE;
	}

// IUnknown

HRESULT CCatLinkHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CCatLinkHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CCatLinkHandler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void CCatLinkHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

void CCatLinkHandler::OnRxData(BYTE bData)
{
	if( m_fRun && m_fSync ) {

		if( m_uRxPtr < elements(m_bRxData) ) {

			m_bRxData[m_uRxPtr++] = bData;
			}
		}
	}

void CCatLinkHandler::OnRxDone(void)
{
	if( m_fRun ) {

		StartEdgeTimer(FALSE);

		if( m_fSync ) {

			if( m_uRxPtr ) {

				if( m_fSelf ) {

					if( !CheckContention() ) {

						m_pDriver->GetNext();
						}

					m_fSelf = FALSE;
					}

				if( m_uRxPtr == elements(m_bRxData) ) {

					m_fSync = FALSE;

					return;
					}
				else {
					BYTE bSum = 0;

					for( UINT n = 0; n < m_uRxPtr; n++ ) {

						bSum += m_bRxData[n];
						}

					if( !bSum ) {

						BYTE  bSrc   = m_bRxData[0];

						BYTE  bDest  = m_bRxData[1];

						PBYTE pFrame = m_bRxData + 2;

						UINT  uSize  = m_uRxPtr  - 3;

						if( IsSafe(bSrc) ) {

							if( IsSelf(bDest) ) {

								Process(bSrc, bDest, pFrame, uSize);
								}
							}
						else {
							AfxTrace("REAL ENGINE!!!\n");

							Stop();
							}
						}
					}
				}
			}

		m_fSync  = TRUE;

		m_fSelf  = FALSE;

		m_uRxPtr = 0;
		}
	}

BOOL CCatLinkHandler::OnTxData(BYTE &bData)
{
	if( m_uTxPtr < m_uTxSize ) {

		bData = m_bTxData[m_uTxPtr++];

		return TRUE;
		}

	return FALSE;
	}

void CCatLinkHandler::OnTxDone(void)
{
	}

void CCatLinkHandler::OnOpen(CSerialConfig const &Config)
{
	}

void CCatLinkHandler::OnClose(void)
{
	}

void CCatLinkHandler::OnTimer(void)
{
	if( m_fRun ) {

		if( (m_uTxSize = m_pDriver->GetSend(m_bTxData)) ) {

			ClearContention();

			m_uTxPtr = 1;

			m_fSelf  = TRUE;

			m_pPort->Send(m_bTxData[0]);
			}
		else
			StartEdgeTimer(TRUE);
		}
	}

// Implementation

void CCatLinkHandler::Process(BYTE bSrc, BYTE bDest, PBYTE pData, UINT uCount)
{
	if( pData[0] == 0xFA && pData[1] == 0x0F ) {

		WORD wCode = MAKEWORD(pData[4], pData[3]);

		m_pDriver->SeeFault(wCode | 0x4000);
		}

	if( pData[0] == 0xFA && pData[1] == 0x11 ) {

		WORD wCode = MAKEWORD(pData[4], pData[3]);

		m_pDriver->SeeFault(wCode | 0x8000);
		}

	if( pData[0] == 0x20 ) {

		UINT uName = GetName(pData+1, 1);

		m_pDriver->SeePoll(bSrc, uName);
		}

	if( pData[0] == 0x60 ) {

		UINT uName = GetName(pData+1, 2);

		m_pDriver->SeePoll(bSrc, uName);
		}

	if( pData[0] == 0xCF ) {

		UINT uName = GetName(pData+2, 3);

		m_pDriver->SeePoll(bSrc, uName);
		}
	}

UINT CCatLinkHandler::GetName(PBYTE pName, UINT uLen)
{
	if( uLen == 1 ) {

		return pName[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pName[1],
				 pName[0]
				 );
		}
	
	if( uLen == 3 ) {

		return MAKELONG( MAKEWORD(pName[2], pName[1]),
				 MAKEWORD(pName[0], 0)
				 );
		}

	return NOTHING;
	}

void CCatLinkHandler::ClearContention(void)
{
	m_pPort->SetOutput(0x80, 0);
	}

BOOL CCatLinkHandler::CheckContention(void)
{
	return m_pPort->GetInput(0x80);
	}

void CCatLinkHandler::StartEdgeTimer(BOOL fSlow)
{
	m_pPort->SetOutput(0x81, fSlow);
	}

// End of File
