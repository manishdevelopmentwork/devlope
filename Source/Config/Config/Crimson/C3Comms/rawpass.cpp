
#include "intern.hpp"

#include "rawpass.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CRawTCPPassiveDriverOptions, CUIItem);

// Constructor

CRawTCPPassiveDriverOptions::CRawTCPPassiveDriverOptions(void)
{
	m_pService = NULL;

	m_Port     = 1234;

	m_Keep     = FALSE;
	}

// Download Support

BOOL CRawTCPPassiveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pService);

	Init.AddWord(WORD(m_Port));

	Init.AddByte(BYTE(m_Keep));

	return TRUE;
	}

// Meta Data Creation

void CRawTCPPassiveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Service);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);

	Meta_SetName(IDS_DRIVER_OPTIONS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Port
//

// Instantiator

ICommsDriver *	Create_RawTCPPassiveDriver(void)
{
	return New CRawTCPPassiveDriver;
	}

// Constructor

CRawTCPPassiveDriver::CRawTCPPassiveDriver(void)
{
	m_wID		= 0x3702;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Raw TCP/IP Passive";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Raw TCP/IP Passive";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Configuration

CLASS CRawTCPPassiveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CRawTCPPassiveDriverOptions);
	}

// Binding Control

UINT CRawTCPPassiveDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
