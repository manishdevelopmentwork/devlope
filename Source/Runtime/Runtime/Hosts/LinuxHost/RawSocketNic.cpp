
#include "Intern.hpp"

#include "RawSocketNic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Socket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCAP-Based NIC Driver
//

// Static Data

CArray <CRawSocketNic::CFace *> CRawSocketNic::m_Faces;

// Instantiator

IDevice * Create_RawSocketNic(UINT uInst)
{
	if( uInst < 1 ) {

		return New CRawSocketNic(uInst, 0);
	}

	return NULL;
}

// Constructor

CRawSocketNic::CRawSocketNic(UINT uInst, UINT uFace)
{
	StdSetRef();

	m_pLock = Create_Qutex();

	m_pFlag = Create_Semaphore();

	m_uInst    = uInst;

	m_uFace    = uFace;

	m_hSocket  = NULL;

	m_hThread  = NULL;

	m_uRxLimit = 32;

	m_uRxCount = 0;

	m_pRxHead  = NULL;

	m_pRxTail  = NULL;

	m_uMulti   = 0;

	m_pMulti   = NULL;

	FindFace();
}

// Destructor

CRawSocketNic::~CRawSocketNic(void)
{
	delete[] m_pMulti;

	AfxRelease(m_pFlag);

	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CRawSocketNic::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(INic);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CRawSocketNic::AddRef(void)
{
	StdAddRef();
}

ULONG CRawSocketNic::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CRawSocketNic::Open(void)
{
	return TRUE;
}

// INic

bool CRawSocketNic::Open(bool fFast, bool fFull)
{
	if( m_pFace ) {

		WORD pr = ETH_P_ALL;

		if( (m_hSocket = _socket(AF_PACKET, SOCK_RAW, HostToNet(pr))) > 0 ) {

			SetAdapterAndProtocol(m_pFace->m_uIndex, pr);

			SetPromiscuous(m_pFace->m_uIndex, m_pFace->m_Adapter);

		//	SetNonBlocking();

			m_hThread = CreateClientThread(this, 0, 81000);

			AfxTrace("RawSocketNic: opened %s as interface nic%u with MAC %s\n",
				 PCTXT(m_pFace->m_Adapter),
				 m_uInst,
				 PCTXT(m_Mac.GetAsText())
			);

			return true;
		}
	}

	return false;
}

bool CRawSocketNic::Close(void)
{
	if( m_hThread ) {

		DestroyThread(m_hThread);

		while( m_pRxHead ) {

			m_pRxHead->Release();

			AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);
		}
	}

	return true;
}

bool CRawSocketNic::InitMac(MACADDR const &Addr)
{
	return true;
}

void CRawSocketNic::ReadMac(MACADDR &Addr)
{
	Addr = m_Mac;
}

UINT CRawSocketNic::GetCapabilities(void)
{
	return 0;
}

void CRawSocketNic::SetFlags(UINT uFlags)
{
}

bool CRawSocketNic::SetMulticast(MACADDR const *pList, UINT uList)
{
	m_pLock->Wait(FOREVER);

	if( pList && uList ) {

		if( !m_uMulti ) {

			/*LoadFilter(m_hPort, TRUE);*/
		}

		if( uList != m_uMulti ) {

			delete[] m_pMulti;

			m_pMulti = New CMacAddr[uList];
		}

		memcpy(m_pMulti, pList, sizeof(MACADDR) * uList);

		m_uMulti = uList;
	}
	else {
		if( m_uMulti ) {

			/*LoadFilter(m_hPort, FALSE);*/

			delete[] m_pMulti;

			m_pMulti = NULL;

			m_uMulti = 0;
		}
	}

	m_pLock->Free();

	return true;
}

bool CRawSocketNic::IsLinkActive(void)
{
	return true;
}

bool CRawSocketNic::WaitLink(UINT uTime)
{
	return true;
}

bool CRawSocketNic::SendData(CBuffer *pBuff, UINT uTime)
{
	PBYTE pData = pBuff->GetData();

	UINT  uData = pBuff->GetSize();

	(MACADDR &) pData[6] = m_Mac;

	_send(m_hSocket, pData, uData, 0);

	return true;
}

bool CRawSocketNic::ReadData(CBuffer * &pBuff, UINT uTime)
{
	if( m_pFlag->Wait(uTime) ) {

		m_pLock->Wait(FOREVER);

		if( m_pRxHead ) {

			pBuff = m_pRxHead;

			AfxListRemove(m_pRxHead, m_pRxTail, m_pRxHead, m_pNext, m_pPrev);

			m_uRxCount--;

			m_pLock->Free();

			return true;
		}

		AfxAssert(FALSE);

		m_pLock->Free();
	}

	return false;
}

void CRawSocketNic::GetCounters(NICDIAG &Diag)
{
	memset(&Diag, 0, sizeof(Diag));
}

void CRawSocketNic::ResetCounters(void)
{
}

// IClientProcess

BOOL CRawSocketNic::TaskInit(UINT uTask)
{
	return TRUE;
}

INT CRawSocketNic::TaskExec(UINT uTask)
{
	UINT  uSize = 16384;

	PBYTE pData = New BYTE[uSize];

	for( ;;) {

		int nRead = _recv(m_hSocket, PSTR(pData), uSize, 0);

		if( nRead > 0 ) {

			UINT uData = nRead;

			m_pLock->Wait(FOREVER);

			if( m_uRxCount < m_uRxLimit ) {

				CMacAddr const &Dest = (CMacAddr const &) pData[0];

				CMacAddr const &From = (CMacAddr const &) pData[6];

				BOOL           fDrop = TRUE;

				if( Dest.IsMulticast() ) {

					for( UINT n = 0; n < m_uMulti; n++ ) {

						if( Dest == m_pMulti[n] ) {

							fDrop = FALSE;

							break;
						}
					}
				}
				else
					fDrop = FALSE;

				if( !fDrop ) {

					if( uData <= 1600 ) {

						CBuffer *pBuff = BuffAllocate(0);

						memcpy(pBuff->AddTail(uData), pData, uData);

						if( From == m_pFace->m_MacAddr ) {

							// The packet came from the local MAC address, so
							// it is quite likely to have an incomplete checksum
							// as the Windows NIC driver will be off-loading the
							// checksum generation to the hardware.

							pBuff->SetFlag(packetSumsValid);
						}

						AfxListAppend(m_pRxHead, m_pRxTail, pBuff, m_pNext, m_pPrev);

						m_uRxCount++;

						m_pFlag->Signal(1);
					}
					else {
						// This should not happen! Why are we seeing
						// packets larger than the expected maximum?
					}
				}
			}

			m_pLock->Free();
		}
	}

	return 0;
}

void CRawSocketNic::TaskStop(UINT uTask)
{
}

void CRawSocketNic::TaskTerm(UINT uTask)
{
	_close(m_hSocket);
}

// Interface Location

UINT CRawSocketNic::FindInterfaces(void)
{
	if( m_Faces.IsEmpty() ) {

		int fd = _socket(AF_INET, SOCK_DGRAM, 0);

		struct ifconf conf;

		conf.ifc_len           = 0;

		conf.ifc_ifcu.ifcu_buf = NULL;

		_ioctl(fd, SIOCGIFCONF, &conf);

		conf.ifc_ifcu.ifcu_buf = malloc(conf.ifc_len);

		_ioctl(fd, SIOCGIFCONF, &conf);

		UINT c = conf.ifc_len / sizeof(struct ifreq);

		for( UINT n = 0; n < c; n++ ) {

			struct ifreq *ifr = conf.ifc_ifcu.ifcu_req + n;

			if( ifr->ifr_ifru.ifru_addr.sa_family == AF_INET ) {

				CIpAddr Ip  = (DWORD &) ifr->ifr_ifru.ifru_addr.sa_data[2];

				if( !Ip.IsLoopback() ) {

					bool hit = false;

					for( UINT f = 0; f < m_Faces.GetCount(); f++ ) {

						CFace *pFace = m_Faces[f];

						if( pFace->m_Adapter == ifr->ifr_ifrn.ifrn_name ) {

							pFace->m_IpAddrs.Append(Ip);

							hit = true;

							break;
						}
					}

					if( !hit ) {

						CFace *pFace     = New CFace;

						pFace->m_Adapter = ifr->ifr_ifrn.ifrn_name;

						pFace->m_IpAddrs.Append(Ip);

						_ioctl(fd, SIOCGIFINDEX, ifr);

						pFace->m_uIndex  = ifr->ifr_ifru.ifru_ivalue;

						_ioctl(fd, SIOCGIFHWADDR, ifr);

						pFace->m_MacAddr = PBYTE(ifr->ifr_ifru.ifru_addr.sa_data);

						m_Faces.Append(pFace);
					}
				}
			}
		}

		free(conf.ifc_ifcu.ifcu_buf);

		_close(fd);
	}

	return m_Faces.GetCount();
}

// Implementation

bool CRawSocketNic::FindFace(void)
{
	if( m_uFace < CRawSocketNic::FindInterfaces() ) {

		m_pFace = m_Faces[m_uFace];

		m_Mac   = m_pFace->m_MacAddr;

		FlipMac();

		return true;
	}

	m_pFace = NULL;

	return false;
}

void CRawSocketNic::FlipMac(void)
{
	// We have to generate a unique MAC address that we
	// can use with this port. This isn't the best way of
	// doing it, but it will work for now...

	for( UINT n = 1; n < elements(m_Mac.m_Addr); n++ ) {

		m_Mac.m_Addr[n] ^= 0xFF;
	}
}

void CRawSocketNic::SetAdapterAndProtocol(UINT uIndex, WORD Protocol)
{
	struct sockaddr_ll addr;

	memset(&addr, 0, sizeof(addr));

	addr.sll_family   = AF_PACKET;

	addr.sll_protocol = HostToNet(Protocol);

	addr.sll_ifindex  = uIndex;

	_bind(m_hSocket, (struct sockaddr *) &addr, sizeof(addr));
}

void CRawSocketNic::SetPromiscuous(UINT uIndex, PCTXT pName)
{
	struct packet_mreq mreq;

	memset(&mreq, 0, sizeof(mreq));

	mreq.mr_ifindex = uIndex;

	mreq.mr_type    = PACKET_MR_PROMISC;

	mreq.mr_alen    = 6;

	_setsockopt(m_hSocket, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mreq, sizeof(mreq));

	AfxTrace("raw: promiscuous mode selected for %s\n", pName);
}

// End of File
