
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConNavWnd_HPP

#define INCLUDE_DevConNavWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevCon;

class CDevConNode;

class CDevConPart;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Navigation Window
//

class CDevConNavWnd : public CViewWnd, public IUpdate, public IDropSource, public IDropTarget
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConNavWnd(void);

	// Destructor
	~CDevConNavWnd(void);

	// IUnknown
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
	ULONG   METHOD AddRef(void);
	ULONG   METHOD Release(void);

	// IUpdate
	HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	// IDropSource
	HRESULT METHOD QueryContinueDrag(BOOL fEscape, DWORD dwKeys);
	HRESULT METHOD GiveFeedback(DWORD dwEffect);

	// IDropTarget
	HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragLeave(void);
	HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

protected:
	// Node Info
	struct CNode
	{
		CString       m_Path;
		CJsonData   * m_pNode;
		CDevConNode * m_pItem;
	};

	// Object Maps
	typedef CMap <HTREEITEM, CNode>   CNodeMap;
	typedef CMap <CString, HTREEITEM> CNameMap;

	// Static Data
	static UINT m_timerDouble;

	// Data Members
	CSysProxy	m_System;
	CDropHelper	m_DropHelp;
	CCursor		m_LinkCursor;
	CCursor		m_MoveCursor;
	CCursor		m_CopyCursor;
	CAccelerator    m_Accel;
	CImageList	m_Images;
	CTreeView     * m_pTree;
	DWORD		m_dwStyle;
	HTREEITEM	m_hRoot;
	HTREEITEM	m_hSelect;
	CNode const   * m_pSelect;
	CDevCon	      * m_pItem;
	BOOL            m_fLoading;

	// Drag Context
	CPoint		m_DragPos;
	CSize		m_DragOffset;
	CSize		m_DragSize;
	CBitmap		m_DragImage;

	// System Data
	CNodeMap m_Nodes;
	CNameMap m_Names;

	// Need Navigation!!!

	// Overridables
	void    OnAttach(void);
	CString OnGetNavPos(void);
	BOOL    OnNavigate(CString const &Nav);

	// Message Map
	AfxDeclareMessageMap();

	// Accelerator
	BOOL OnAccelerator(MSG &Msg);

	// Message Handlers
	void OnPostCreate(void);
	void OnLoadTool(UINT uCode, CMenu &Menu);
	void OnSize(UINT uCode, CSize Size);
	void OnShowWindow(BOOL fShow, UINT uStatus);
	void OnPaint(void);
	void OnSetFocus(CWnd &Wnd);
	UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);

	// Notification Handlers
	void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
	BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
	UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
	void OnTreeReturn(UINT uID, NMHDR &Info);
	void OnTreeDblClk(UINT uID, NMHDR &Info);
	void OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info);
	BOOL OnTreeExpanding(UINT uID, NMTREEVIEW &Info);
	void OnTreeContextMenu(UINT uID, NMTREEVIEW &Info);
	void OnTreeOfferFocus(UINT uID, NMTREEVIEW &Info);

	// Data Object Construction
	BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
	BOOL MakeDataObject(IDataObject * &pData);

	// Drag Support
	BOOL DragItem(void);

	// Drag Hooks
	void  FindDragMetrics(void);
	void  MakeDragImage(void);
	DWORD FindDragAllowed(void);
	void  DragComplete(DWORD dwEffect);

	// Tree Loading
	void LoadImageList(void);
	void LoadTree(void);
	void LoadPart(HTREEITEM hRoot, CDevConPart *pPart);
	void LoadChildren(HTREEITEM hRoot, CDevConPart *pPart, CString const &Path, CString const &Label, CJsonData *pNode);

	// Implementation
	BOOL IsParent(void);
	BOOL IsParent(HTREEITEM hItem);
	BOOL ExpandItem(HTREEITEM hItem, UINT uAction);
	void ExpandItem(HTREEITEM hItem);
	void ToggleItem(HTREEITEM hItem);
	void SelectRoot(void);
	BOOL SelectInit(void);
	void SetRedraw(BOOL fRedraw);
	void RefreshTree(void);
	BOOL IsReadOnly(void);
	BOOL IsPrivate(void);
	void SetViewedItem(BOOL fCheck);

	// Selection State
	CString SaveSelState(void);
	void    LoadSelState(CString State);
};

// End of File

#endif
