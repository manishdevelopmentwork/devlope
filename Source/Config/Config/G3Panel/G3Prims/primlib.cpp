
#include "intern.hpp"

#include "resview.hpp"

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenLine.hpp"

#include "PrimRubyPenEdge.hpp"

#include "PrimMurphyTrendViewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Resource
//

// Dynamic Class

AfxImplementDynamicClass(CPrimResourceItem, CMetaItem);

// Constructor

CPrimResourceItem::CPrimResourceItem(void)
{
	m_pLib = New CPrimList;
	}

// UI Creation

CViewWnd * CPrimResourceItem::CreateView(UINT uType)
{
	if( uType == viewResource ) {

		return New CPrimResourceWnd;
		}						    

	return CMetaItem::CreateView(uType);
	}

// Operations

BOOL CPrimResourceItem::LoadWidgets(void)
{
	CModule * pApp = afxModule->GetApp();

	CRegKey   Reg  = pApp->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Widgets");

	if( Reg.GetValue(L"Load", 1U) ) {

		CModule * pApp = afxModule->GetApp();

		CFilename Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Widgets");

		LoadWidgets(Path, L"",         CSize(   0,   0));

		LoadWidgets(Path, L"Common\\", CSize(   0,   0));
		
		LoadWidgets(Path, L"VGA\\",    CSize( 640, 480));
		
		LoadWidgets(Path, L"SVGA\\",   CSize( 800, 600));
		
		LoadWidgets(Path, L"XVGA\\",   CSize(1024, 768));

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimResourceItem::LoadWidget(CFilename Base, CFilename Full, INDEX Before)
{
	CTextStreamMemory Stream;

	if( Stream.LoadFromFile(Full) ) {

		CString File   = Full.Mid(Base.GetLength());

		CSize   Target = CSize(0, 0);

		CString Init   = File.TokenLeft('\\');

		if( Init == L"VGA" ) {

			Target = CSize(640, 480);
			}

		if( Init == L"SVGA" ) {

			Target = CSize(800, 600);
			}

		if( Init == L"XVGA" ) {

			Target = CSize(1024, 768);
			}

		if( AcceptPrims(Stream, File, Target, Before) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CPrimResourceItem::KillWidgets(void)
{
	for(;;) {
		
		INDEX n = m_pLib->GetHead();

		if( !m_pLib->Failed(n) ) {

			CPrim *pPrim = m_pLib->GetItem(n);

			pPrim->Kill();

			delete pPrim;

			m_pLib->RemoveItem(n);

			continue;
			}
		
		break;
		}
	}

// Persistance

void CPrimResourceItem::Init(void)
{
	CMetaItem::Init();

	m_pSystem = (CUISystem *) m_pDbase->GetSystemItem();
	}

void CPrimResourceItem::PostLoad(void)
{
	CMetaItem::PostLoad();

	m_pSystem = (CUISystem *) m_pDbase->GetSystemItem();
	}

// Meta Data

void CPrimResourceItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Lib);
	}

// Implementation

void CPrimResourceItem::LoadWidgets(CString Base, CString Path, CSize Target)
{
	WIN32_FIND_DATA Data;

	HANDLE hFind = FindFirstFile(Base + Path + L"*.wid*", &Data);

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			CTextStreamMemory Stream;

			CFilename Name = Base + Path + Data.cFileName;

			if( Stream.LoadFromFile(Name) ) {

				CString File = Path + Data.cFileName;

				AcceptPrims(Stream, File, Target, NULL);
				}

			} while( FindNextFile(hFind, &Data) );

		FindClose(hFind);
		}
	}

BOOL CPrimResourceItem::AcceptPrims(ITextStream &Stream, CString File, CSize Target, INDEX Before)
{
	CTreeFile Tree;

	if( Tree.OpenLoad(Stream) ) {

		CPrimRefMap Refs;

		CArray <CPrim *> List;

		while( !Tree.IsEndOfData() ) {

			CString Name = Tree.GetName();

			if( !Name.IsEmpty() ) {

				BOOL fFixup = File.EndsWith(L".wid");

				if( !m_pSystem->ReadRef(Tree, Name, Refs, TRUE, fFixup) ) {

					CLASS  Class = AfxNamedClass(PCTXT(L"C" + Name));

					CPrim *pItem = AfxNewObject(CPrim, Class);

					Tree.GetObject();

					pItem->SetParent(m_pLib);

					pItem->Load(Tree);

					m_pLib->InsertItem(pItem, Before);

					pItem->PostPaste();

					pItem->PostLoad();

					if( pItem->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

						CPrimWidget *pWidget = (CPrimWidget *) pItem;

						pWidget->m_File   = File;

						pWidget->m_Target = Target;
						}

					Normalize(pItem);

					Tree.EndObject();

					List.Append(pItem);
					}
				}
			}

		if( Refs.GetCount() ) {

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				CPrim *pPrim = List[n];

				m_pSystem->FixRefs(pPrim, Refs, 0);

				m_pSystem->FixRefs(pPrim, Refs, 1);
				}
			}
		}

	return TRUE;
	}

void CPrimResourceItem::Normalize(CPrim *pPrim)
{
	if( FALSE ) {

		if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

			CPrimWidget *pWidget = (CPrimWidget *) pPrim;

			pWidget->m_LockData = 1;

			pWidget->m_LockList = 2;
			}
		}

	CRect  Rect = pPrim->GetRect();

	CSize  Size = Rect.GetSize();

	CRect  Move = CRect(Size);

	pPrim->SetRect(Rect, Size);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CPrimResourceWnd, CImageResourceWnd);

// Constructor

CPrimResourceWnd::CPrimResourceWnd(void)
{
	m_CatIcon = 0x20000009;

	m_pWin    = Create_GdiWindowsA888(1024, 768);

	m_pGdi	  = m_pWin->GetGdi();
	}

// Destructor

CPrimResourceWnd::~CPrimResourceWnd(void)
{
	for( UINT n = 0; n < m_CatList.GetCount(); n++ ) {

		CPrimCat *pCat = m_CatList[n];

		delete pCat;
		}

	m_pGdi->Release();
	}

// Overridables

void CPrimResourceWnd::OnAttach(void)
{
	m_pItem   = (CPrimResourceItem *) CImageResourceWnd::m_pItem;

	m_pSystem = (CUISystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pLib    = m_pItem->m_pLib;
	}

// Image Hooks

void CPrimResourceWnd::OnBuildCatList(void)
{
	UINT n;

	for( n = 0;; n++ ) {
		
		CPrimCat *pCat = MakePrimCat(n);

		if( pCat ) {

			pCat->Bind(m_pItem);

			m_CatList.Append(pCat);

			m_CatName.Append(pCat->GetTitle());

			continue;
			}

		break;
		}

	m_uFixed = n;

	MakeWidgetCats();
	}

void CPrimResourceWnd::OnRefresh(void)
{
	CStringArray List;

	for( UINT n = 0; n < m_Monitor.GetCount(); n++  ) {

		m_Monitor[n]->GetChangeList(List);
		}

	if( !List.IsEmpty() ) {

		UINT    uMode = m_uMode;

		UINT    uCat  = 0;

		CString Name  = L"";

		if( uMode >= 1 ) {

			uCat = m_uCat;

			Name = m_CatName[uCat];
			}

		SetMode(NOTHING);

		while( m_uFixed < m_CatList.GetCount() ) {

			CPrimCat *pCat = m_CatList[m_uFixed];

			m_CatList.Remove(m_uFixed);

			m_CatName.Remove(m_uFixed);

			delete pCat;
			}

		CModule * pApp = afxModule->GetApp();

		CFilename Base = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Widgets");

		for( UINT i = 0; i < List.GetCount(); i++ ) {

			INDEX n = m_pLib->GetHead();

			INDEX b = NULL;

			while( !m_pLib->Failed(n) ) {

				CPrim *pPrim = m_pLib->GetItem(n);

				if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

					CPrimWidget *pWidget = (CPrimWidget *) pPrim;

					if( Base + pWidget->m_File == List[i] ) {

						b = n;

						m_pLib->GetNext(b);

						pPrim->Kill();

						delete pPrim;

						m_pLib->RemoveItem(n);

						break;
						}
					}

				m_pLib->GetNext(n);
				}

			m_pItem->LoadWidget(Base, List[i], b);
			}

		MakeWidgetCats();

		if( uMode >= 1 && uCat >= m_uFixed ) {

			if( m_CatName[uCat] != Name ) {

				uMode = 0;
				}
			else
				m_uCat = uCat;
			}

		SetMode(uMode);
		}
	}

BOOL CPrimResourceWnd::OnMonitor(CStringArray &List)
{
	CModule * pApp = afxModule->GetApp();

	CString   Base = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Widgets");

	List.Append(Base);
	
	List.Append(Base + L"Common\\");
	
	List.Append(Base + L"VGA\\");
	
	List.Append(Base + L"SVGA\\");
	
	List.Append(Base + L"XVGA\\");
	
	return TRUE;
	}

void CPrimResourceWnd::OnLoadImages(void)
{
	CPrimCat *pCat = m_CatList[m_uCat];

	if( m_uMode == 1 ) {

		for( UINT n = 0;; n++ ) {

			CPrimInfo *pInfo = NULL;

			if( pCat->LoadPrim(pInfo, m_pItem, n) ) {

				m_PrimList.Append(pInfo);

				continue;
				}

			break;
			}

		m_uImageCount = m_PrimList.GetCount();

		m_Title       = m_CatName[m_uCat];

		m_fZoom       = m_CatList[m_uCat]->CanZoom();
		}

	if( m_uMode == 2 ) {

		for( UINT n = 0;; n++ ) {

			CPrimInfo *pInfo = NULL;

			if( pCat->LoadPrim(pInfo, m_pItem, m_uImage, n) ) {

				m_PrimList.Append(pInfo);

				continue;
				}

			break;
			}

		m_uImageCount = m_PrimList.GetCount();

		m_fZoom       = m_CatList[m_uCat]->CanZoom();
		}
	}

void CPrimResourceWnd::OnFreeImages(void)
{
	CPrimCat *pCat = m_CatList[m_uCat];

	for( UINT n = 0; n < m_PrimList.GetCount(); n++ ) {

		CPrimInfo * pInfo = m_PrimList[n];

		if( !pCat->DoNotDelete() ) {
		
			pInfo->m_pPrim->Kill();

			delete pInfo->m_pPrim;
			}

		delete pInfo;
		}
	
	m_PrimList.Empty();
	}

CString CPrimResourceWnd::OnGetImageText(UINT uImage)
{
	return m_PrimList[uImage]->m_Text;
	}

void CPrimResourceWnd::OnAddImageData(CDataObject *pData, UINT uImage)
{
	CPrimInfo * pInfo = m_PrimList[uImage];

	AddPrimToDataObject(pData, pInfo->m_pPrim);
	}

BOOL CPrimResourceWnd::OnZoomImage(UINT uImage)
{
	CPrimInfo * pInfo = m_PrimList[uImage];

	return pInfo->m_fZoom;
	}

void CPrimResourceWnd::OnDrawImage(CDC &DC, CRect Rect, UINT uImage)
{
	if( !Rect.IsEmpty() ) {

		CPrimInfo * pInfo = m_PrimList[uImage];

		CPrim     * pPrim = pInfo->m_pPrim;

		if( Rect.cx() < 40 ) {

			if( pInfo->m_uMode == modeSimple ) {

				DrawAspect(DC, Rect - 2, pPrim);
				}
			else {
				if( pInfo->m_uIcon ) {

					DrawIcon(DC, Rect - 2, pInfo->m_uIcon);
					}
				else
					DrawScale(DC, Rect - 2, pPrim);
				}
			}
		else {
			switch( pInfo->m_uMode ) {

				case modeWhole:

					DrawWhole(DC, Rect - 2, pPrim);

					break;

				case modeSimple:
				case modeAspect:

					DrawAspect(DC, Rect - 2, pPrim);

					break;

				case modeScale:

					DrawScale(DC, Rect - 2, pPrim);

					break;

				case modeIcon:

					DrawIcon(DC, Rect - 2, pInfo->m_uIcon);

					break;
				}
			}
		}
	}

BOOL CPrimResourceWnd::OnGetAlpha(UINT uImage)
{
	return TRUE;
	}

// Drawing Helpers

void CPrimResourceWnd::DrawWhole(CDC &DC, CRect Rect, CPrim *pPrim)
{
	CSize Size = Rect.GetSize();

	CRect Prev = pPrim->GetRect();

	CRect Move = CRect(Size);

	if( wstrstr(pPrim->GetClassName(), L"Ruby") ) {

		Move -= 2; // Hack to deal with overflow !!!
		}

	pPrim->SetRect(Prev, Move);

	pPrim->SetHand(TRUE);

	m_pGdi->ResetAll();

	m_pWin->ClearAlpha();

	pPrim->Draw(m_pGdi, drawThumb);

	DC.AlphaBlend(Rect, CDC::FromHandle(m_pWin->GetDC()), CPoint(0, 0));

	pPrim->SetRect(Move, Prev);

	pPrim->SetHand(TRUE);
	}

void CPrimResourceWnd::DrawAspect(CDC &DC, CRect Rect, CPrim *pPrim)
{
	CSize  Size = Rect.GetSize();

	CRect  Prev = pPrim->GetRect();

	CSize  Init = Prev.GetSize();

	double xDiv = double(Size.cx) / Init.cx;

	double yDiv = double(Size.cy) / Init.cy;

	double nDiv = min(xDiv, yDiv);

	BOOL   fNew = !!wstrstr(pPrim->GetClassName(), L"Ruby");

	CSize  Move = CSize(int(Init.cx * nDiv), int(Init.cy * nDiv));

	CSize  Min  = pPrim->GetMinSize(m_pGdi);

	MakeMax(Move.cx, Min.cx + (fNew ? 4 : 0));

	MakeMax(Move.cy, Min.cy + (fNew ? 4 : 0));

	CPoint Pos = Rect.GetTopLeft() + (Size - Move) / 2;

	if( Pos.x >= 0 && Pos.y >= 0 ) {

		CRect Draw = CRect(Pos, Move);

		if( fNew ) {

			Draw -= 2; // Hack to deal with overflow !!!
			}

		if( Draw.cx() >= Min.cx && Draw.cy() >= Min.cy ) {

			pPrim->SetRect(Prev, Draw);

			pPrim->SetHand(TRUE);

			m_pGdi->ResetAll();

			m_pWin->ClearAlpha();

			pPrim->Draw(m_pGdi, drawThumb);

			DC.AlphaBlend(Draw, CDC::FromHandle(m_pWin->GetDC()), Draw.GetTopLeft());

			pPrim->SetRect(Draw, Prev);

			pPrim->SetHand(TRUE);
			}
		}
	}

void CPrimResourceWnd::DrawScale(CDC &DC, CRect Rect, CPrim *pPrim)
{
	CSize  Size = Rect.GetSize();

	CSize  Prim = pPrim->GetRect().GetSize();

	double xDiv = double(Size.cx) / Prim.cx;

	double yDiv = double(Size.cy) / Prim.cy;

	double nDiv = min(xDiv, yDiv);

	MakeMin(nDiv, 1.0);

	CSize  Used = CSize(int(Prim.cx * nDiv), int(Prim.cy * nDiv));

	CPoint Pos  = Rect.GetTopLeft() + (Size - Used) / 2;

	CRect  Draw = CRect(Pos, Used);

	m_pGdi->ResetAll();

	m_pGdi->SetBrushFore(COLOR(0));

	m_pGdi->FillRect(0, 0, Prim.cx + 4, Prim.cy + 4);

	pPrim->SetRect(CRect(Prim), CRect(Prim) + CPoint(1,1));

	pPrim->SetHand(TRUE);

	pPrim->Draw(m_pGdi, drawWhole);

	pPrim->SetRect(CRect(Prim) + CPoint(1,1), CRect(Prim));

	GpBitmap *pImage = NULL;

	m_pWin->ReleaseBitmap();

	GdipCreateBitmapFromHBITMAP( m_pWin->GetBitmap(),
				     NULL,
				     &pImage
				     );

	m_pWin->RestoreBitmap();

	GpGraphics *pGraph = NULL;

	DC.SetBkColor(afxColor(TabFace));

	GdipCreateFromHDC(DC.GetHandle(), &pGraph);

	GdipDrawImageRectRectI( pGraph,
				pImage,
				Draw.left,
				Draw.top,
				Draw.cx(),
				Draw.cy(),
				0,
				0,
				Prim.cx + 2,
				Prim.cy + 2,
				UnitPixel,
				NULL,
				NULL,
				NULL
				);
	
	GdipDeleteGraphics(pGraph);

	GdipDisposeImage(pImage);
	}

void CPrimResourceWnd::DrawIcon(CDC &DC, CRect Rect, UINT uIcon)
{
	}

// Data Object Construction

BOOL CPrimResourceWnd::AddPrimToDataObject(CDataObject *pInfo, CPrim *pPrim)
{
	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		if( AddPrimToStream(Stream, pPrim) ) {

			UINT m_cfData = RegisterClipboardFormat(L"C3.1 Graphics");

			pInfo->AddStream(m_cfData, Stream);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimResourceWnd::AddPrimToStream(ITextStream &Stream, CPrim *pPrim)
{
	CPoint  Pos  = pPrim->GetRect().GetCenter();

	CString Head = CPrintf(L"CREATE|FIXED|%u|%u\r\n", Pos.x, Pos.y);

	Stream.PutLine(Head);

	CTreeFile Tree;

	if( Tree.OpenSave(Stream) ) {

		AddPrimToTreeFile(Tree, pPrim);

		Tree.Close();

		return TRUE;
		}

	return FALSE;
	}

void CPrimResourceWnd::AddPrimToTreeFile(CTreeFile &Tree, CPrim *pPrim)
{
	CPrimRefList Refs;

	CString Class = pPrim->GetClassName();

	Tree.PutObject(Class.Mid(1));

	pPrim->PreCopy();

	pPrim->GetRefs(Refs);

	pPrim->Save(Tree);

	Tree.EndObject();

	m_pSystem->SaveRefs(Tree, Refs);
	}

// Implementation

void CPrimResourceWnd::MakeWidgetCats(void)
{
	CStringTree Cats;

	for( INDEX n = m_pLib->GetHead(); !m_pLib->Failed(n); m_pLib->GetNext(n) ) {

		CPrim *pPrim = m_pLib->GetItem(n);

		if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

			CPrimWidget *pWidget = (CPrimWidget *) pPrim;

			if( UseWidget(pWidget) ) {

				Cats.Insert(pWidget->m_Cat);
				}
			}
		}

	for( INDEX s = Cats.GetHead(); !Cats.Failed(s); Cats.GetNext(s) ) {

		CPrimCat *pCat = New CPrimCatWidget(m_pLib, Cats[s]);

		m_CatList.Append(pCat);

		m_CatName.Append(pCat->GetTitle());
		}
	}

BOOL CPrimResourceWnd::UseWidget(CPrimWidget *pWidget)
{
	if( pWidget->m_Target.cx && pWidget->m_Target.cy ) {

		if( pWidget->m_Target == m_pSystem->m_DispSize ) {

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

// Category Objects

CPrimResourceWnd::CPrimCat * CPrimResourceWnd::MakePrimCat(UINT uCat)
{
	switch( uCat ) {

		case  0: return New CPrimCatRuby;
		case  1: return New CPrimCatGauges;
		case  2: return New CPrimCatGraphs;
		case  3: return New CPrimCatButtons;
		case  4: return New CPrimCatIllumButtons;
		case  5: return New CPrimCatIndicators;
		case  6: return New CPrimCatToggle2;
		case  7: return New CPrimCatToggle3;
		case  8: return New CPrimCatSelect2;
		case  9: return New CPrimCatSelect3;
		case 10: return New CPrimCatViewers;
		case 11: return New CPrimCatLegacy;
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive Category Base Class
//

// Constructor

CPrimResourceWnd::CPrimCat::CPrimCat(void)
{
	m_pItem = NULL;
	}

// Destructor

CPrimResourceWnd::CPrimCat::~CPrimCat(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCat::GetTitle(void)
{
	return L"Untitled";
	}

BOOL CPrimResourceWnd::CPrimCat::DoNotDelete(void)
{
	return FALSE;
	}

BOOL CPrimResourceWnd::CPrimCat::LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage)
{
	if( StdLoad(pInfo, pParent, uImage) ) {

		if( CanColor(uImage) ) {

			if( ApplyColors(pInfo->m_pPrim) ) {

				pInfo->m_fZoom = TRUE;
				}
			}

		Adjust(pInfo, uImage);

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimResourceWnd::CPrimCat::LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage, UINT uStyle)
{
	if( uStyle < GetSchemeCount(GetClass(uImage)) ) {

		if( StdLoad(pInfo, pParent, uImage) ) {

			ApplyColors(pInfo->m_pPrim, uStyle);

			Adjust(pInfo, uImage);

			return TRUE;
			}
		}

	return FALSE;
	}

CLASS CPrimResourceWnd::CPrimCat::GetClass(UINT uImage)
{
	return NULL;
	}

BOOL CPrimResourceWnd::CPrimCat::CanColor(UINT uImage)
{
	return TRUE;
	}

BOOL CPrimResourceWnd::CPrimCat::CanZoom(void)
{
	return TRUE;
	}

void CPrimResourceWnd::CPrimCat::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	}

// Implementation

void CPrimResourceWnd::CPrimCat::Bind(CItem *pItem)
{
	m_pItem = pItem;
	}

BOOL CPrimResourceWnd::CPrimCat::StdLoad(CPrimInfo * &pInfo, CItem *pParent, UINT uImage)
{
	CLASS Class = GetClass(uImage);

	if( Class ) {

		CPrim *pPrim = AfxNewObject(CPrim, Class);

		pPrim->SetParent(pParent);

		pPrim->Init();

		pPrim->SetHand(TRUE);

		pInfo = New CPrimInfo;

		pInfo->m_pPrim = pPrim;

		pInfo->m_Text  = pPrim->GetHumanName();

		pInfo->m_uMode = CPrimResourceWnd::modeSimple;

		pInfo->m_uIcon = 0;

		pInfo->m_fZoom = FALSE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimResourceWnd::CPrimCat::AdjustPolygon(CPrimInfo *pInfo, UINT uImage)
{
	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimPoly)) ) {

		CPrimPoly *pFind = (CPrimPoly *) pInfo->m_pPrim;

		switch( pFind->m_Orient = (uImage % 4) ) {

			case 0: pInfo->m_Text += CString(IDS_RIGHT); break;
			case 1: pInfo->m_Text += CString(IDS_LEFT);  break;
			case 2: pInfo->m_Text += CString(IDS_DOWN);  break;
			case 3: pInfo->m_Text += CString(IDS_UP);    break;
			}

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimResourceWnd::CPrimCat::GetSchemeCount(CLASS Class)
{
	if( Class->IsKindOf(AfxRuntimeClass(CPrimRubyGaugeTypeA)) ) {

		return 6;
		}
	else {
		CCommsSystem  *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

		CColorManager *pColor  = pSystem->m_pColor;

		UINT ng = pColor->GetGroupCount();

		UINT nc = 0;

		for( UINT g = 0; g < ng; g++ ) {

			if( g < 2 || g > 5 ) {

				nc += pColor->GetCount(g);
				}
			}

		return nc - 2;
		}
	}

BOOL CPrimResourceWnd::CPrimCat::ApplyColors(CPrim *pPrim)
{
	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyGaugeTypeA)) ) {

		return ApplyColors(pPrim, 0);
		}

	return ApplyColors(pPrim, 2);
	}

BOOL CPrimResourceWnd::CPrimCat::ApplyColors(CPrim *pPrim, UINT uScheme)
{
	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyGaugeTypeA)) ) {

		CPrimRubyGaugeTypeA *pFind = (CPrimRubyGaugeTypeA *) pPrim;

		pFind->m_GaugeStyle = (uScheme == 5) ? 200 : uScheme + 1;

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimBevelBase)) ) {

		CPrimBevelBase *pFind = (CPrimBevelBase *) pPrim;

		pFind->m_pFace  ->Set(GetColor(uScheme, 0));
		
		pFind->m_pHilite->Set(GetColor(uScheme, 1));
		
		pFind->m_pShadow->Set(GetColor(uScheme, 2));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyBevelBase)) ) {

		CPrimRubyBevelBase *pFind = (CPrimRubyBevelBase *) pPrim;

		pFind->m_pFill  ->Set(GetColor(uScheme, 0));
		
		pFind->m_pHilite->Set(GetColor(uScheme, 1));
		
		pFind->m_pShadow->Set(GetColor(uScheme, 5));

		pFind->m_pEdge  ->Set(GetColor(uScheme, 6));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimGradButton)) ) {

		CPrimGradButton *pFind = (CPrimGradButton *) pPrim;

		pFind->m_pColor1->Set(GetColor(uScheme, 3));

		pFind->m_pColor2->Set(GetColor(uScheme, 4));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyGradButton)) ) {

		CPrimRubyGradButton *pFind = (CPrimRubyGradButton *) pPrim;

		pFind->m_pColor1->Set(GetColor(uScheme, 3));

		pFind->m_pColor2->Set(GetColor(uScheme, 4));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimGeom)) ) {

		CPrimGeom *pFind = (CPrimGeom *) pPrim;

		pFind->m_pFill->Set(GetColor(uScheme, 0));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimLegacyShadow)) ) {

		CPrimLegacyShadow *pFind = (CPrimLegacyShadow *) pPrim;

		pFind->m_pBack->Set(GetColor(uScheme, 0));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimLine)) ) {

		CPrimLine *pFind = (CPrimLine *) pPrim;

		pFind->m_pPen->Set(GetColor(uScheme, 0));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyGeom)) ) {

		CPrimRubyGeom *pFind = (CPrimRubyGeom *) pPrim;

		pFind->m_pFill->Set(GetColor(uScheme, 0));

		return TRUE;
		}

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyLine)) ) {

		CPrimRubyLine *pFind = (CPrimRubyLine *) pPrim;

		pFind->m_pLine->Set(GetColor(uScheme, 0));

		return TRUE;
		}

	return FALSE;
	}

COLOR CPrimResourceWnd::CPrimCat::GetColor(UINT uScheme, UINT uShade)
{
	COLOR Col = 0;

	if( TRUE ) {

		CCommsSystem  *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

		CColorManager *pColor  = pSystem->m_pColor;

		UINT ng = pColor->GetGroupCount();

		UINT nc = 0;

		for( UINT g = 0; g < ng; g++ ) {

			UINT tg;

			if( g < ng - 6 ) {

				tg = 6 + g;
				}
			else
				tg = g - ng + 2;

			if( tg < 2 || tg > 5 ) {

				if( tg == 0 ) {

					uScheme += 2;
					}

				if( uScheme >= nc && uScheme < nc + pColor->GetCount(tg) ) {

					Col = pColor->GetColor(tg, uScheme - nc);

					break;
					}

				nc += pColor->GetCount(tg);
				}
			}
		}

	int r = GetRED  (Col);
	int g = GetGREEN(Col);
	int b = GetBLUE (Col);

	switch( uShade ) {

		case 1:
			r = 3 * r / 2;
			g = 3 * g / 2;
			b = 3 * b / 2;

			if( r > 30 ) r = 30;
			if( g > 30 ) g = 30;
			if( b > 30 ) b = 30;
			
			break;

		case 2:
			r = 1 * r / 2;
			g = 1 * g / 2;
			b = 1 * b / 2;
			
			if( r < 15 ) r = r / 2;
			if( g < 15 ) g = g / 2;
			if( b < 15 ) b = b / 2;
			
			break;

		case 3:
			r = 2 * r;
			g = 2 * g;
			b = 2 * b;

			if( r > 31 ) r = 31;
			if( g > 31 ) g = 31;
			if( b > 31 ) b = 31;

			break;

		case 4:
			r = 2 * r / 3;
			g = 2 * g / 3;
			b = 2 * b / 3;

			if( r < 15 ) r = 0;
			if( g < 15 ) g = 0;
			if( b < 15 ) b = 0;
		
			break;

		case 5:
			r = 2 * r / 3;
			g = 2 * g / 3;
			b = 2 * b / 3;
			
			if( r < 12 ) r = 3 * r / 4;
			if( g < 12 ) g = 3 * g / 4;
			if( b < 12 ) b = 3 * b / 4;
			
			break;

		case 6:
			r = 1 * r / 2;
			g = 1 * g / 2;
			b = 1 * b / 2;
			
			if( r < 8 ) r = 2 * r / 3;
			if( g < 8 ) g = 2 * g / 3;
			if( b < 8 ) b = 2 * b / 3;
			
			break;
		}

	if( r == 0x1F && g == 0x00 && b == 0x1F ) {

		// NOTE -- Hack to avoid going transparent!

		r = 0x1E;
		
		b = 0x1E;
		}

	return GetRGB(r,g,b);
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatWidget::CPrimCatWidget(CPrimList *pLib, CString Cat)
{
	m_pLib    = pLib;

	m_Cat     = Cat;

	m_pSystem = (CUISystem *) m_pLib->GetDatabase()->GetSystemItem();
	}

// Overridables

CString CPrimResourceWnd::CPrimCatWidget::GetTitle(void)
{
	return m_Cat;
	}

BOOL CPrimResourceWnd::CPrimCatWidget::DoNotDelete(void)
{
	return TRUE;
	}

BOOL CPrimResourceWnd::CPrimCatWidget::LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage)
{
	if( !uImage ) {

		m_n = m_pLib->GetHead();
		}

	while( !m_pLib->Failed(m_n) ) {

		CPrim *pPrim = m_pLib->GetItem(m_n);

		if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWidget)) ) {

			CPrimWidget *pWidget = (CPrimWidget *) pPrim;

			if( pWidget->m_Cat == m_Cat ) {

				if( UseWidget(pWidget) ) {

					pInfo = New CPrimInfo;

					pInfo->m_pPrim = pWidget;

					pInfo->m_Text  = pWidget->GetDesc();

					pInfo->m_uMode = CPrimResourceWnd::modeScale;

					pInfo->m_uIcon = 0;

					pInfo->m_fZoom = FALSE;

					m_pLib->GetNext(m_n);

					return TRUE;
					}
				}
			}

		m_pLib->GetNext(m_n);
		}

	return FALSE;
	}

// Implementation

BOOL CPrimResourceWnd::CPrimCatWidget::UseWidget(CPrimWidget *pWidget)
{
	if( pWidget->m_Target.cx && pWidget->m_Target.cy ) {

		if( pWidget->m_Target == m_pSystem->m_DispSize ) {

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Core Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatCore::CPrimCatCore(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatCore::GetTitle(void)
{
	return IDS_CORE_PRIMITIVES;
	}

CLASS CPrimResourceWnd::CPrimCatCore::GetClass(UINT uImage)
{
	switch( uImage ) {

		case 0: return AfxRuntimeClass(CPrimAnimImage);
		case 1: return AfxRuntimeClass(CPrimQuickPlot);
		case 2: return AfxRuntimeClass(CPrimVertScale);
		}

	return NULL;
	}

BOOL CPrimResourceWnd::CPrimCatCore::CanColor(UINT uImage)
{
	return FALSE;
	}

void CPrimResourceWnd::CPrimCatCore::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimVertScale)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatRuby::CPrimCatRuby(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatRuby::GetTitle(void)
{
	return CString(IDS_CORE_PRIMITIVES_2);
	}

CLASS CPrimResourceWnd::CPrimCatRuby::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimRubyTextBox);
		case  1: return AfxRuntimeClass(CPrimRubyDataBox);
		case  2: return AfxRuntimeClass(CPrimRubyAnimImage);
		case  3: return AfxRuntimeClass(CPrimRubyRect);
		case  4: return AfxRuntimeClass(CPrimRubyEllipse);
		case  5: return AfxRuntimeClass(CPrimRubyBeveled);
		case  6: return AfxRuntimeClass(CPrimRubyRounded);
		case  7: return AfxRuntimeClass(CPrimRubyFilleted);
		case  8: return AfxRuntimeClass(CPrimRubyLine);
		case  9: return AfxRuntimeClass(CPrimRubyPolygon3);
		case 10: return AfxRuntimeClass(CPrimRubyPolygon5);
		case 11: return AfxRuntimeClass(CPrimRubyPolygon6);
		case 12: return AfxRuntimeClass(CPrimRubyPolygon8);
		case 13: return AfxRuntimeClass(CPrimRubyStar4);
		case 14: return AfxRuntimeClass(CPrimRubyStar5);
		case 15: return AfxRuntimeClass(CPrimRubyStar6);
		case 16: return AfxRuntimeClass(CPrimRubyStar8);
		case 17: return AfxRuntimeClass(CPrimRubyWedge);
		case 18: return AfxRuntimeClass(CPrimRubyArrow);
		case 19: return AfxRuntimeClass(CPrimRubyBalloon);
		case 20: return AfxRuntimeClass(CPrimRubyTriangle);
		case 21: return AfxRuntimeClass(CPrimRubyParallelogram);
		case 22: return AfxRuntimeClass(CPrimRubyTrapezium);
		case 23: return AfxRuntimeClass(CPrimRubyConicalTank);
		case 24: return AfxRuntimeClass(CPrimRubyRoundTank);
		case 25: return AfxRuntimeClass(CPrimRubyQuadrant);
		case 26: return AfxRuntimeClass(CPrimRubySemi);
		case 27: return AfxRuntimeClass(CPrimRubyBevel);
		case 28: return AfxRuntimeClass(CPrimRubyBevelButton);
		case 29: return AfxRuntimeClass(CPrimRubyGradButton);
		case 30: return AfxRuntimeClass(CPrimRubyTimeDate);			
		case 31: return AfxRuntimeClass(CPrimBarcode);
		}

	return NULL;
	}

BOOL CPrimResourceWnd::CPrimCatRuby::CanColor(UINT uImage)
{
	switch( uImage ) {
		
		case  0:
		case  1:
		case  2:
		case 30:
		case 31:
			break;

		default:
			return TRUE;
		}

	return FALSE;
	}

void CPrimResourceWnd::CPrimCatRuby::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyTextBox)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeWhole;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyDataBox)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeWhole;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyAnimImage)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeWhole;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyTimeDate)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimBarcode)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeWhole;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Ruby Gauges Category
//

// Constructor

CPrimResourceWnd::CPrimCatGauges::CPrimCatGauges(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatGauges::GetTitle(void)
{
	return CString(IDS_GAUGES);
	}

CLASS CPrimResourceWnd::CPrimCatGauges::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimRubyGaugeTypeAR1);
		case  1: return AfxRuntimeClass(CPrimRubyGaugeTypeAR2);
		case  2: return AfxRuntimeClass(CPrimRubyGaugeTypeAR3);
		case  3: return AfxRuntimeClass(CPrimRubyGaugeTypeAR4);
		case  4: return AfxRuntimeClass(CPrimRubyGaugeTypeAR5);
		case  5: return AfxRuntimeClass(CPrimRubyGaugeTypeAR6);
		case  6: return AfxRuntimeClass(CPrimRubyGaugeTypeAR1);
		case  7: return AfxRuntimeClass(CPrimRubyGaugeTypeAR2);
		case  8: return AfxRuntimeClass(CPrimRubyGaugeTypeAR3);
		case  9: return AfxRuntimeClass(CPrimRubyGaugeTypeAR4);
		case 10: return AfxRuntimeClass(CPrimRubyGaugeTypeAR5);
		case 11: return AfxRuntimeClass(CPrimRubyGaugeTypeAR6);
		case 12: return AfxRuntimeClass(CPrimRubyGaugeTypeALV);
		case 13: return AfxRuntimeClass(CPrimRubyGaugeTypeALH);
		case 14: return AfxRuntimeClass(CPrimRubyGaugeTypeALV);
		case 15: return AfxRuntimeClass(CPrimRubyGaugeTypeALH);
		}

	return NULL;
	}

BOOL CPrimResourceWnd::CPrimCatGauges::CanColor(UINT uImage)
{
	return TRUE;
	}

void CPrimResourceWnd::CPrimCatGauges::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyGaugeTypeAR)) ) {

		CPrimRubyGaugeTypeAR *pFind = (CPrimRubyGaugeTypeAR *) pInfo->m_pPrim;

		if( uImage / 6 % 2 ) {

			pFind->m_PointMode = 1;
			}
		}
	
	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimRubyGaugeTypeAL)) ) {

		CPrimRubyGaugeTypeAL *pFind = (CPrimRubyGaugeTypeAL *) pInfo->m_pPrim;

		if( uImage / 2 % 2 ) {

			pFind->m_PointMode = 1;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Arrows Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatArrows::CPrimCatArrows(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatArrows::GetTitle(void)
{
	return IDS_ARROWS;
	}

CLASS CPrimResourceWnd::CPrimCatArrows::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0:
		case  1:
		case  2:
		case  3: return AfxRuntimeClass(CPrimPolyArrow);
		}

	return NULL;
	}

void CPrimResourceWnd::CPrimCatArrows::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	AdjustPolygon(pInfo, uImage);
	}

//////////////////////////////////////////////////////////////////////////
//
// Polygons Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatPolygons::CPrimCatPolygons(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatPolygons::GetTitle(void)
{
	return IDS_POLYGONS_AND;
	}

CLASS CPrimResourceWnd::CPrimCatPolygons::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimPolygon3);
		case  1: return AfxRuntimeClass(CPrimPolygon5);
		case  2: return AfxRuntimeClass(CPrimPolygon6);
		case  3: return AfxRuntimeClass(CPrimPolygon8);
		case  4: return AfxRuntimeClass(CPrimPolyStar4);
		case  5: return AfxRuntimeClass(CPrimPolyStar5);
		case  6: return AfxRuntimeClass(CPrimPolyStar6);
		case  7: return AfxRuntimeClass(CPrimPolyStar8);
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Balloons Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatBalloons::CPrimCatBalloons(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatBalloons::GetTitle(void)
{
	return IDS_BALLOONS_AND;
	}

CLASS CPrimResourceWnd::CPrimCatBalloons::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimPolyBalloon);
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Semi-Trimmed Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatSemiTrim::CPrimCatSemiTrim(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatSemiTrim::GetTitle(void)
{
	return IDS_SEMITRIMMED;
	}

CLASS CPrimResourceWnd::CPrimCatSemiTrim::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case  1: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case  2: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case  3: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case  4: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case  5: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case  6: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case  7: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case  8: return AfxRuntimeClass(CPrimPolySemiPlaque);
		case  9: return AfxRuntimeClass(CPrimPolySemiPlaque);
		case 10: return AfxRuntimeClass(CPrimPolySemiPlaque);
		case 11: return AfxRuntimeClass(CPrimPolySemiPlaque);
		}

	return NULL;
	}

void CPrimResourceWnd::CPrimCatSemiTrim::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	AdjustPolygon(pInfo, uImage);
	}
	
//////////////////////////////////////////////////////////////////////////
//
// Viewers Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatViewers::CPrimCatViewers(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatViewers::GetTitle(void)
{
	return IDS_SYSTEM_PRIMITIVES;
	}

CLASS CPrimResourceWnd::CPrimCatViewers::GetClass(UINT uImage)
{
	BOOL fMurphy = C3OemFeature(L"TrendViewer", FALSE);	// !!! Set FALSE for Red Lion

	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimAlarmViewer);
		case  1: return AfxRuntimeClass(CPrimEventViewer);
		case  2: return AfxRuntimeClass(CPrimFileViewer);
		case  3: return AfxRuntimeClass(CPrimPDFViewer);
		case  4: return AfxRuntimeClass(CPrimTrendViewer);
		case  5: return AfxRuntimeClass(CPrimUserManager);
		case  6: return AfxRuntimeClass(CPrimLegacyAlarmTicker);
		case  7: return AfxRuntimeClass(CPrimCamera);
		case  8: return AfxRuntimeClass(CPrimTouchCalib);
		case  9: return AfxRuntimeClass(CPrimTouchTester);
		case 10: return fMurphy ? AfxRuntimeClass(CPrimMurphyTrendViewer) : NULL;
		}

	return NULL;
	}

BOOL CPrimResourceWnd::CPrimCatViewers::CanZoom(void)
{
	return FALSE;
	}

void CPrimResourceWnd::CPrimCatViewers::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	pInfo->m_uMode = CPrimResourceWnd::modeScale;
	}
	
//////////////////////////////////////////////////////////////////////////
//
// Graphs Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatGraphs::CPrimCatGraphs(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatGraphs::GetTitle(void)
{
	return CString(IDS_BAR_AND_LINE);
	}

CLASS CPrimResourceWnd::CPrimCatGraphs::GetClass(UINT uImage)
{
	switch( uImage ) {

		case 0: return AfxRuntimeClass(CPrimLegacyVertBarGraph);
		case 1: return AfxRuntimeClass(CPrimLegacyHorzBarGraph);
		case 2: return AfxRuntimeClass(CPrimLegacyScatterGraph);
		}

	return NULL;
	}

void CPrimResourceWnd::CPrimCatGraphs::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	pInfo->m_uMode = CPrimResourceWnd::modeScale;
	}

//////////////////////////////////////////////////////////////////////////
//
// Legacy Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatLegacy::CPrimCatLegacy(void)
{
	}

// Overridables

CString CPrimResourceWnd::CPrimCatLegacy::GetTitle(void)
{
	return IDS_LEGACY_PRIMITIVES;
	}

CLASS CPrimResourceWnd::CPrimCatLegacy::GetClass(UINT uImage)
{
	switch( uImage ) {

		case  0: return AfxRuntimeClass(CPrimEllipseQuad);
		case  1: return AfxRuntimeClass(CPrimEllipseHalf);
		case  2: return AfxRuntimeClass(CPrimLegacyVertSlider);
		case  3: return AfxRuntimeClass(CPrimLegacyHorzSlider);
		case  4: return AfxRuntimeClass(CPrimLegacyVertScale);
		case  5: return AfxRuntimeClass(CPrimLegacyHorzScale);
		case  6: return AfxRuntimeClass(CPrimLegacyWholeDial);
		case  7: return AfxRuntimeClass(CPrimLegacyHalfDial);
		case  8: return AfxRuntimeClass(CPrimLegacyQuadDial);
		case  9: return AfxRuntimeClass(CPrimLegacyHorzBar);
		case 10: return AfxRuntimeClass(CPrimLegacyVertBar);
		case 11: return AfxRuntimeClass(CPrimLegacySelectorTwo);
		case 12: return AfxRuntimeClass(CPrimLegacySelectorMulti);
		case 13: return AfxRuntimeClass(CPrimLegacyCfImage);

		// Core
		case 14: return AfxRuntimeClass(CPrimRect);
		case 15: return AfxRuntimeClass(CPrimEllipse);
		case 16: return AfxRuntimeClass(CPrimPolyTrimRect);
		case 17: return AfxRuntimeClass(CPrimPolyRoundRect);
		case 18: return AfxRuntimeClass(CPrimPolyPlaque);
		case 19: return AfxRuntimeClass(CPrimWedge);
		case 20: return AfxRuntimeClass(CPrimPolyConicalTank);
		case 21: return AfxRuntimeClass(CPrimPolyRoundTank);
		case 22: return AfxRuntimeClass(CPrimLegacyShadow);
		case 23: return AfxRuntimeClass(CPrimBevel);
		case 24: return AfxRuntimeClass(CPrimBevel);
		case 25: return AfxRuntimeClass(CPrimBevel);
		case 26: return AfxRuntimeClass(CPrimBevelButton);
		case 27: return AfxRuntimeClass(CPrimGradButton);
		case 28: return AfxRuntimeClass(CPrimLine);
		case 29: return AfxRuntimeClass(CPrimTextBox);
		case 30: return AfxRuntimeClass(CPrimDataBox);
		case 31: return AfxRuntimeClass(CPrimTimeDate);
		case 32: return AfxRuntimeClass(CPrimAnimImage);
		case 33: return AfxRuntimeClass(CPrimQuickPlot);
		case 34: return AfxRuntimeClass(CPrimVertScale);

		// Ballons		
		case 35: return AfxRuntimeClass(CPrimPolyBalloon);

		// Arrows
		case 36:
		case 37:
		case 38:
		case 39: return AfxRuntimeClass(CPrimPolyArrow);

		// Polygons
		case 40: return AfxRuntimeClass(CPrimPolygon3);
		case 41: return AfxRuntimeClass(CPrimPolygon5);
		case 42: return AfxRuntimeClass(CPrimPolygon6);
		case 43: return AfxRuntimeClass(CPrimPolygon8);
		case 44: return AfxRuntimeClass(CPrimPolyStar4);
		case 45: return AfxRuntimeClass(CPrimPolyStar5);
		case 46: return AfxRuntimeClass(CPrimPolyStar6);
		case 47: return AfxRuntimeClass(CPrimPolyStar8);

		// Semi-Trimmed Polygons
		case 48: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case 49: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case 50: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case 51: return AfxRuntimeClass(CPrimPolySemiTrimRect);
		case 52: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case 53: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case 54: return AfxRuntimeClass(CPrimPolySemiRoundRect);
		case 55: return AfxRuntimeClass(CPrimPolySemiRoundRect);		
		case 56: return AfxRuntimeClass(CPrimPolySemiPlaque);
		case 57: return AfxRuntimeClass(CPrimPolySemiPlaque);
		case 58: return AfxRuntimeClass(CPrimPolySemiPlaque);
		case 59: return AfxRuntimeClass(CPrimPolySemiPlaque);
		}

	return NULL;
	}

BOOL CPrimResourceWnd::CPrimCatLegacy::CanColor(UINT uImage)
{
	if( uImage >= 0 && uImage <= 2 ) {

		// Legacy
		
		return TRUE;
		}

	if( uImage >= 14 && uImage <= 28 ) {

		// Core
		
		return TRUE;
		}

	if( uImage >= 35 && uImage <= 35 ) {

		// Balloons
		
		return TRUE;
		}

	if( uImage >= 36 && uImage <= 39 ) {

		// Arrows
		
		return TRUE;
		}

	if( uImage >= 40 && uImage <= 47 ) {

		// Polygons
		
		return TRUE;
		}

	if( uImage >= 48 && uImage <= 59 ) {

		// Semi-Trimmed Polygons
		
		return TRUE;
		}

	return FALSE;
	}

void CPrimResourceWnd::CPrimCatLegacy::Adjust(CPrimInfo *pInfo, UINT uImage)
{
	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimLegacySlider)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimLegacyAlarmTicker)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimLegacyDial)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimLegacyBar)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimLegacySelector)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimBevel)) ) {

		CPrimBevel *pFind = (CPrimBevel *) pInfo->m_pPrim;

		switch( pFind->m_Type = (uImage % 3) ) {

			case 0: pInfo->m_Text += CString(IDS_RAISED); break;
			case 1: pInfo->m_Text += CString(IDS_SUNKEN); break;
			case 2: pInfo->m_Text += CString(IDS_BORDER); break;
			}
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimTimeDate)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimVertScale)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimPolyArrow)) ) {

		AdjustPolygon(pInfo, uImage);
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimPolySemiTrimRect)) ) {

		AdjustPolygon(pInfo, uImage);
		}

	if( pInfo->m_pPrim->IsKindOf(AfxRuntimeClass(CPrimVertScale)) ) {

		pInfo->m_uMode = CPrimResourceWnd::modeScale;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Image-Based Primitive Category
//

// Constructor

CPrimResourceWnd::CPrimCatImageBased::CPrimCatImageBased(void)
{
	m_Title = L"";

	m_Class = NULL;

	m_State[0] = NOTHING;
	m_State[1] = NOTHING;
	m_State[2] = NOTHING;
	m_State[3] = NOTHING;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatImageBased::GetTitle(void)
{
	return m_Title;
	}

BOOL CPrimResourceWnd::CPrimCatImageBased::LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage)
{
	return LoadPrim(pInfo, pParent, uImage, 0);
	}

BOOL CPrimResourceWnd::CPrimCatImageBased::LoadPrim(CPrimInfo * &pInfo, CItem *pParent, UINT uImage, UINT uStyle)
{
	UINT uCat = GetTypeCategory(uImage);

	if( uCat ) {

		PUINT pData = GetTypeData(uImage, uStyle);

		if( pData ) {

			CPrimImageBtnBase *pPrim = AfxNewObject(CPrimImageBtnBase, m_Class);

			pPrim->m_Keep = 1;

			pPrim->SetParent(pParent);

			pPrim->Init();

			pPrim->SetHand(TRUE);

			pInfo = New CPrimInfo;

			pInfo->m_pPrim = pPrim;

			pInfo->m_Text  = GetTypeDesc(uImage);

			pInfo->m_uMode = CPrimResourceWnd::modeScale;

			pInfo->m_uIcon = 0;

			pInfo->m_fZoom = TRUE;

			if( !GetTypeData(uImage, 1) ) {

				pInfo->m_fZoom = FALSE;
				}

			for( UINT n = 0; n < elements(m_State); n++ ) {

				if( m_State[n] < NOTHING ) {

					pPrim->SetImage(n, MakeName(uCat, pData[m_State[n]]));
					}
				}

			Adjust(pPrim, uImage);

			return TRUE;
			}
		}

	return FALSE;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatImageBased::GetTypeDesc(UINT uType)
{
	return L"";
	}

UINT CPrimResourceWnd::CPrimCatImageBased::GetTypeCategory(UINT uType)
{
	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatImageBased::GetTypeData(UINT uType, UINT uStyle)
{
	return NULL;
	}

void CPrimResourceWnd::CPrimCatImageBased::Adjust(CPrim *pPrim, UINT uType)
{
	}

// Implementation

CString CPrimResourceWnd::CPrimCatImageBased::MakeName(UINT uCat, UINT uHandle)
{
	return CPrintf(L"SYM:%u,%u,1", uCat, uHandle);
	}

//////////////////////////////////////////////////////////////////////////
//
// Illuminated Buttons Category
//

// Constructor

CPrimResourceWnd::CPrimCatIllumButtons::CPrimCatIllumButtons(void)
{
	m_Title = CString(IDS_ILLUMINATED_2);

	m_Class = AfxRuntimeClass(CPrimImageIllumButton);

	m_State[0] = 2;
	m_State[1] = 3;
	m_State[2] = 0;
	m_State[3] = 1;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatIllumButtons::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		// 4 images per style, 5 styles per type.

		case 0: return IDS_ROUND_WITH_LEGEND;
		case 1: return IDS_ROUND;
		case 2: return IDS_ROUND_ON_ENAMEL;
		case 3: return IDS_ROUND_ON_TEXTURE;
		case 4: return IDS_RECTANGULAR;
		case 5: return IDS_CANDY_BUTTON;

		// 4 images per style, 2 styles per type.

		case 6: return IDS_LARGE_WITH_LED;
		case 7: return IDS_MEDIUM_WITH_LED;
		case 8: return IDS_SMALL_WITH_LED;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatIllumButtons::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 4 images per style, 5 styles per type.

		case  0: return 28;
		case  1: return 28;
		case  2: return 28;
		case  3: return 28;
		case  4: return 28;
		case  5: return 29;

		// 4 images per style, 2 styles per type.

		case  6: return 29;
		case  7: return 29;
		case  8: return 29;
		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatIllumButtons::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 4 images per style, 5 styles per type.

		867739050,
		2131997361,
		23839009,
		1042141362,
		705291430,
		2024600231,
		742461077,
		1861626379,
		447170132,
		1469339690,
		1316841514,
		1470133222,
		1305577233,
		1692570859,
		1735370103,
		712683997,
		1534493316,
		194678897,
		1570288781,
		2102382455,
		1715989490,
		2004088941,
		845154403,
		2117473201,
		219227658,
		282997380,
		504119239,
		1874217839,
		105537769,
		2051238827,
		669677706,
		808354526,
		434804984,
		1921950700,
		248426338,
		1349827358,
		1836895437,
		1089132681,
		2109541100,
		1450448853,
		231514626,
		150646497,
		1119731075,
		650365567,
		1295310020,
		252110846,
		1678767876,
		665388483,
		506469602,
		441134015,
		1540766775,
		919226466,
		1134648205,
		246704105,
		950367652,
		965050841,
		440507717,
		1873134521,
		1671168407,
		1518408207,
		803588314,
		707910211,
		299166034,
		1166604105,
		1610615512,
		1334580765,
		1694376880,
		1436102488,
		426330800,
		1405309961,
		2135318329,
		1765305308,
		828280253,
		615159714,
		243180673,
		1359589477,
		1741210913,
		756470772,
		309912895,
		211677295,
		2104143906,
		451856924,
		1015601892,
		1949887434,
		383648709,
		876220269,
		779631031,
		488588809,
		347691266,
		854288532,
		440892752,
		1804504119,
		974563946,
		1041544522,
		2135474769,
		1079365394,
		1789191338,
		1895953848,
		620561167,
		1861947183,
		1521950253,
		733818113,
		1921268141,
		827802936,
		962286914,
		436887756,
		2067229992,
		1169390227,
		1965593901,
		1865851063,
		108192888,
		1409347847,
		1722944777,
		2147358928,
		899884312,
		1862480395,
		1462257951,
		945775725,
		74740585,
		1526169722,
		};

	if( uType < elements(n0) / 20 ) {

		if( uStyle < 5 ) {

			PUINT p = n0;

			p += uType  * 20;

			p += uStyle * 4;

			return p;
			}
		}

	uType -= elements(n0) / 20;

	////////

	static UINT n1[] = {

		// 4 images per style, 2 styles per type.

		1839142777,
		1888173700,
		1831881550,
		557212369,
		414546085,
		54166565,
		1838321594,
		436247514,
		430260149,
		1054953216,
		1859607720,
		715417960,
		1411479071,
		1093829434,
		2094240009,
		1126472861,
		381340224,
		277206955,
		1449256204,
		960754185,
		1219035707,
		839192005,
		809376971,
		1138437222

		};

	if( uType < elements(n1) / 8 ) {

		if( uStyle < 2 ) {

			PUINT p = n1;

			p += uType  * 8;

			p += uStyle * 4;

			return p;
			}
		}

	uType -= elements(n1) / 8;

	////////

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Indicators Category
//

// Constructor

CPrimResourceWnd::CPrimCatIndicators::CPrimCatIndicators(void)
{
	m_Title = CString(IDS_INDICATORS);

	m_Class = AfxRuntimeClass(CPrimImageIndicator);

	m_State[0] = 1;
	m_State[1] = 0;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatIndicators::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		case  0: return IDS_PILOT;
		case  1: return IDS_PILOT_WITH_LEGEND;
		case  2: return IDS_PILOT_ON_ENAMEL;
		case  3: return IDS_PILOT_ON_TEXTURE;
		case  4: return IDS_ANNUNCIATOR_1;
		case  5: return IDS_ANNUNCIATOR_2;
		case  6: return IDS_ANNUNCIATOR_3;
		case  7: return IDS_GROOVED_BEZEL;
		case  8: return IDS_CANDY_LAMP;
		case  9: return IDS_BASIC_INDICATOR;

		case 10: return IDS_GLASSY_LIGHT;
		case 11: return IDS_SQUARE;
		case 12: return IDS_RECTANGULAR_WITH;
		case 13: return IDS_NARROW;
		case 14: return IDS_LABELED_LED;
		case 15: return IDS_INCANDESCENT;
		case 16: return IDS_ROUND_LED;
		case 17: return IDS_HORIZONTAL_LED;
		case 18: return IDS_VERTICAL_LED;
		case 19: return IDS_RECESSED_LED;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatIndicators::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 5 styles per type.

		case  0: return 30;
		case  1: return 30;
		case  2: return 30;
		case  3: return 30;
		case  4: return 30;
		case  5: return 30;
		case  6: return 30;
		case  7: return 30;
		case  8: return 30;
		case  9: return 30;
		case 10: return 31;
		case 11: return 31;
		case 12: return 31;
		case 13: return 31;
		case 14: return 31;
		case 15: return 31;
		case 16: return 31;
		case 17: return 31;
		case 18: return 31;
		case 19: return 31;
		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatIndicators::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 2 images per style, 5 styles per type.

		1185399849,
		963959974,
		2129988032,
		1827151484,
		1595644872,
		772846411,
		57853968,
		1673255269,
		1812000048,
		947916981,
		1090962998,
		1134098044,
		1961102887,
		1302994140,
		1984642017,
		951929505,
		2094423119,
		216821576,
		192512898,
		2006159617,
		1444082877,
		707463014,
		147200807,
		284436836,
		777665068,
		243681566,
		1985568338,
		1368027557,
		1156153674,
		1697288192,
		968532305,
		1857800924,
		2140527544,
		631498933,
		823785753,
		1066076291,
		1151804169,
		455488104,
		1084113772,
		101339492,
		1111323922,
		510495021,
		1156436454,
		566419932,
		1962187484,
		399223456,
		789060725,
		1788358470,
		1638614889,
		1129928733,
		1024375630,
		1499778261,
		1995302536,
		1508045731,
		80085432,
		897650597,
		1225599361,
		794769523,
		587024809,
		2014599242,
		1039038394,
		1580435376,
		609655065,
		667264884,
		2078457040,
		75287129,
		994764880,
		1186723038,
		1875577152,
		1101207161,
		492711956,
		1421623837,
		683890833,
		783903085,
		90473769,
		1001362759,
		1310435663,
		173914066,
		11979026,
		1262344576,
		371174790,
		1552334604,
		1625280689,
		656006774,
		214430413,
		1545207834,
		817566443,
		1610489962,
		737227349,
		1465074221,
		1788140038,
		713304602,
		609763630,
		1705202427,
		1113417875,
		59408535,
		1315279710,
		1163960411,
		297383782,
		822396213,
		65818971,
		1635607235,
		1374384545,
		1391624709,
		1339727758,
		878357573,
		597739661,
		875419078,
		156755748,
		1335359268,
		957926857,
		236510405,
		1123281876,
		1237251915,
		1115031441,
		2037846681,
		1470637758,
		1064121255,
		490815562,
		801721831,
		304856325,
		714460721,
		1186062494,
		1103360278,
		604832272,
		2107182410,
		1055399182,
		1670785136,
		74201200,
		1530479781,
		1909916650,
		95359317,
		659760186,
		693121114,
		860570010,
		1466912167,
		251026024,
		2113759827,
		734296129,
		1053065967,
		1241010853,
		1345609594,
		681996394,
		1976597753,
		50498408,
		1422890718,
		753904007,
		1323873493,
		1898300629,
		476434357,
		1873381281,
		383818634,
		1415360101,
		1051900980,
		1529078851,
		2056583783,
		582797677,
		724358912,
		749941885,
		1236354052,
		627805329,
		1744083812,
		1303816139,
		30192529,
		757842543,
		756665999,
		1549589689,
		1641867751,
		1983117088,
		1855324612,
		963091307,
		7776109,
		712227010,
		233620346,
		1650709099,
		1053154785,
		1492774339,
		105052413,
		753731287,
		113514058,
		1130644964,
		1409766093,
		533096348,
		268532113,
		530352599,
		2144966055,
		1646699232,
		1637920443,
		566280117,
		1353108577,
		894647351,
		899827579,
		1650488550,
		1762754211,
		745639337,
		560503492,
		1961734646,
		1410841924,
		379375207,
		661838241,
		};

	if( uType < elements(n0) / 10 ) {

		if( uStyle < 5 ) {

			PUINT p = n0;

			p += uType  * 10;

			p += uStyle * 2;

			return p;
			}
		}

	uType -= elements(n0) / 10;

	////////

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Buttons Category
//

// Constructor

CPrimResourceWnd::CPrimCatButtons::CPrimCatButtons(void)
{
	m_Title = CString(IDS_ACTION_BUTTONS);

	m_Class = AfxRuntimeClass(CPrimImageButton);

	m_State[0] = 0;
	m_State[1] = 1;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatButtons::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 3 styles per type.

		case  0: return IDS_BUTTON_WITH;
		case  1: return IDS_BUTTON;
		case  2: return IDS_BUTTON_ON_ENAMEL;
		case  3: return IDS_BUTTON_ON_TEXTURE;
		case  4: return IDS_MUSHROOM_WITH;
		case  5: return IDS_MUSHROOM;
		case  6: return IDS_MUSHROOM_ON_1;
		case  7: return IDS_MUSHROOM_ON_2;
		case  8: return IDS_FIRE;
		case  9: return IDS_SQUARE;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatButtons::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 3 styles per type.

		case  0: return 27;
		case  1: return 27;
		case  2: return 27;
		case  3: return 27;
		case  4: return 27;
		case  5: return 27;
		case  6: return 27;
		case  7: return 27;
		case  8: return 29;
		case  9: return 29;
		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatButtons::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 2 images per style, 3 styles per type.

		1693721982,
		1078932211,
		1338344639,
		379293940,
		932819999,
		634243080,
		1698001239,
		617971084,
		2017561785,
		1560778940,
		264676853,
		432124638,
		81250269,
		426824402,
		1196090025,
		1933766256,
		628685056,
		678102735,
		1593914123,
		408960381,
		1117014540,
		938825400,
		1460298657,
		714174552,
		871498611,
		1654789460,
		304817411,
		557003875,
		1776304730,
		41766893,
		619485911,
		1815850548,
		1881653008,
		1707986634,
		332062179,
		1668738091,
		339290557,
		1552759249,
		98912170,
		206787951,
		358310779,
		2051109023,
		214077890,
		236167237,
		710258446,
		1423621442,
		2006829741,
		131192976,
		199638867,
		1931896223,
		1708231108,
		1807461585,
		2058079742,
		2089217704,
		1721667411,
		204906463,
		1035381469,
		512472750,
		1199555308,
		670551640,
		};

	if( uType < elements(n0) / 6 ) {

		if( uStyle < 3 ) {

			PUINT p = n0;

			p += uType  * 6;

			p += uStyle * 2;

			return p;
			}
		}

	uType -= elements(n0) / 6;

	////////

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// 2-State Toggle Category
//

// Constructor

CPrimResourceWnd::CPrimCatToggle2::CPrimCatToggle2(void)
{
	m_Title = CString(IDS_STATE_TOGGLES_1);

	m_Class = AfxRuntimeClass(CPrimImageToggle2);

	m_State[0] = 1;
	m_State[1] = 0;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatToggle2::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 3 styles per type.

		case  0: return IDS_PADDLE_1;
		case  1: return IDS_PADDLE_2;
		case  2: return IDS_TBAR;
		case  3: return IDS_BASIC_ROCKER;
		case  4: return IDS_MINI_ROCKER;
		case  5: return IDS_SLIDER;

		// 2 images per style, 2 style per type.

		case  6: return IDS_LED_ROCKER;
		case  7: return IDS_LED_ROCKER_WITH;

		// 2 images per style, 1 style per type.

		case  8: return IDS_STANDARD_TOGGLE_1;
		case  9: return IDS_STANDARD_TOGGLE_2;
		case 10: return IDS_STANDARD_TOGGLE_3;
		case 11: return IDS_STANDARD_TOGGLE_4;
		case 12: return IDS_BEEFY_TOGGLE;
		case 13: return IDS_GRAY_TOGGLE_1;
		case 14: return IDS_GRAY_TOGGLE_2;
		case 15: return IDS_MINI_SILVER;
		case 16: return IDS_MINI_GRAY_TOGGLE;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatToggle2::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 3 styles per type.

		case  0: return 33;
		case  1: return 33;
		case  2: return 33;
		case  3: return 34;
		case  4: return 34;
		case  5: return 34;

		// 2 images per style, 2 style per type.

		case  6: return 34;
		case  7: return 34;

		// 2 images per style, 1 style per type.

		case  8: return 33;
		case  9: return 33;
		case 10: return 33;
		case 11: return 33;
		case 12: return 33;
		case 13: return 33;
		case 14: return 33;
		case 15: return 33;
		case 16: return 33;

		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatToggle2::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 2 images per style, 3 styles per type.

		417387774,
		1139011800,
		708380743,
		566695116,
		1499473039,
		1039807040,
		2070875211,
		418978771,
		932064080,
		1264924609,
		26233400,
		145709143,
		979448548,
		1145848698,
		754209918,
		1970300120,
		328430898,
		362120909,
		1333366940,
		248159761,
		1912187391,
		863680352,
		41863879,
		483186187,
		839365111,
		391496082,
		1142806124,
		77522209,
		398586448,
		617807983,
		655121768,
		404056636,
		1869641519,
		317622604,
		1895144076,
		1842128627,
		};

	if( uType < elements(n0) / 6 ) {

		if( uStyle < 3 ) {

			PUINT p = n0;

			p += uType  * 6;

			p += uStyle * 2;

			return p;
			}
		}

	uType -= elements(n0) / 6;

	////////

	static UINT n1[] = {

		// 2 images per style, 2 style per type.

		1337167058,
		1717809579,
		79610223,
		1196936111,
		398364146,
		1248238389,
		1099082078,
		2128983703,
		};

	if( uType < elements(n1) / 4 ) {

		if( uStyle < 2 ) {

			PUINT p = n1;

			p += uType  * 4;

			p += uStyle * 2;

			return p;
			}
		}

	uType -= elements(n1) / 4;

	////////

	static UINT n2[] = {

		// 2 images per style, 1 style per type.

		399557869,
		1846994747,
		1972821422,
		617681864,
		1474220137,
		112261591,
		1136412391,
		197040027,
		1664449390,
		126528256,
		785205504,
		2129624658,
		9844826,
		1866186095,
		1957833960,
		2032293922,
		840801682,
		234603375,

		};

	if( uType < elements(n2) / 2 ) {

		if( uStyle < 1 ) {

			PUINT p = n2;

			p += uType  * 2;

			p += uStyle * 2;

			return p;
			}
		}

	uType -= elements(n2) / 2;

	////////

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// 2-State Toggle Category
//

// Constructor

CPrimResourceWnd::CPrimCatToggle3::CPrimCatToggle3(void)
{
	m_Title = CString(IDS_STATE_TOGGLES_2);

	m_Class = AfxRuntimeClass(CPrimImageToggle3);

	m_State[0] = 2;
	m_State[1] = 1;
	m_State[2] = 0;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatToggle3::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		// 3 images per style, 3 styles per type.

		case  0: return IDS_PADDLE_1;
		case  1: return IDS_PADDLE_2;

		// 3 images per style, 1 style per type.

		case  2: return IDS_GRAY_TOGGLE_1;
		case  3: return IDS_GRAY_TOGGLE_2;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatToggle3::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 3 images per style, 3 styles per type.

		case  0: return 33;
		case  1: return 33;

		// 3 images per style, 1 style per type.

		case  2: return 33;
		case  3: return 33;
		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatToggle3::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 3 images per style, 3 styles per type.

		417387774,
		42582924,
		1139011800,
		708380743,
		201315242,
		566695116,
		1499473039,
		405661578,
		1039807040,
		2070875211,
		1147322093,
		418978771,
		932064080,
		775080818,
		1264924609,
		26233400,
		1130814191,
		145709143,

		};

	if( uType < elements(n0) / 9 ) {

		if( uStyle < 3 ) {

			PUINT p = n0;

			p += uType  * 9;

			p += uStyle * 3;

			return p;
			}
		}

	uType -= elements(n0) / 9;

	////////

	static UINT n1[] = {

		// 3 images per style, 1 style per type.

		785205504,
		1247819968,
		2129624658,
		9844826,
		637823246,
		1866186095,

		};

	if( uType < elements(n1) / 3 ) {

		if( uStyle < 1 ) {

			PUINT p = n1;

			p += uType  * 3;

			p += uStyle * 3;

			return p;
			}
		}

	uType -= elements(n1) / 3;

	////////

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// 2-State Selector Category
//

// Constructor

CPrimResourceWnd::CPrimCatSelect2::CPrimCatSelect2(void)
{
	m_Title = CString(IDS_STATE_SELECTORS_1);

	m_Class = AfxRuntimeClass(CPrimImageToggle2);

	m_State[0] = 0;
	m_State[1] = 1;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatSelect2::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 1 style per type.

		case  0: return IDS_SELECTOR_WITH;
		case  1: return IDS_SELECTOR;
		case  2: return IDS_SELECTOR_ON_1;
		case  3: return IDS_SELECTOR_ON_2;
		case  4: return IDS_KEYSWITCH_WITH;
		case  5: return IDS_KEYSWITCH;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatSelect2::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 2 images per style, 1 style per type.

		case  0: return 33;
		case  1: return 33;
		case  2: return 33;
		case  3: return 33;
		case  4: return 33;
		case  5: return 33;

		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatSelect2::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 2 images per style, 1 style per type.

		490636072,
		10346392,
		1601216010,
		2005647338,
		328744594,
		1485334166,
		481856484,
		1863489982,
		1653528665,
		1505641763,
		630519354,
		128666578,

		};

	if( uType < elements(n0) / 2 ) {

		if( uStyle < 1 ) {

			PUINT p = n0;

			p += uType  * 2;

			p += uStyle * 2;

			return p;
			}
		}

	uType -= elements(n0) / 2;

	////////

	return NULL;
	}

void CPrimResourceWnd::CPrimCatSelect2::Adjust(CPrim *pPrim, UINT uType)
{
	CPrimImageToggle2 *pToggle = (CPrimImageToggle2 *) pPrim;

	pToggle->m_Axis = 1;
	}

//////////////////////////////////////////////////////////////////////////
//
// 2-State Select Category
//

// Constructor

CPrimResourceWnd::CPrimCatSelect3::CPrimCatSelect3(void)
{
	m_Title = CString(IDS_STATE_SELECTORS_2);

	m_Class = AfxRuntimeClass(CPrimImageToggle3);

	m_State[0] = 0;
	m_State[1] = 1;
	m_State[2] = 2;
	}

// Overridables

CString CPrimResourceWnd::CPrimCatSelect3::GetTypeDesc(UINT uType)
{
	switch( uType ) {

		// 3 images per style, 1 style per type.

		case  0: return IDS_SELECTOR_WITH;
		case  1: return IDS_SELECTOR;
		case  2: return IDS_SELECTOR_ON_1;
		case  3: return IDS_SELECTOR_ON_2;
		case  4: return IDS_HANDOFFAUTO;
		case  5: return IDS_KEYSWITCH_WITH;
		case  6: return IDS_KEYSWITCH;
		}

	return L"";
	}

UINT CPrimResourceWnd::CPrimCatSelect3::GetTypeCategory(UINT uType)
{
	switch( uType ) {

		// 3 images per style, 1 style per type.

		case  0: return 33;
		case  1: return 33;
		case  2: return 33;
		case  3: return 33;
		case  4: return 33;
		case  5: return 33;
		case  6: return 33;
		}

	return 0;
	}

PUINT CPrimResourceWnd::CPrimCatSelect3::GetTypeData(UINT uType, UINT uStyle)
{
	static UINT n0[] = {

		// 3 images per style, 1 style per type.

		490636072,
		2132056527,
		10346392,
		1601216010,
		957198515,
		2005647338,
		328744594,
		1536775830,
		1485334166,
		481856484,
		416392800,
		1863489982,
		1891615473,
		409196000,
		537059990,
		1653528665,
		1861681306,
		1505641763,
		630519354,
		687807983,
		128666578,

		};

	if( uType < elements(n0) / 3 ) {

		if( uStyle < 1 ) {

			PUINT p = n0;

			p += uType  * 3;

			p += uStyle * 3;

			return p;
			}
		}

	uType -= elements(n0) / 3;

	return NULL;
	}

void CPrimResourceWnd::CPrimCatSelect3::Adjust(CPrim *pPrim, UINT uType)
{
	CPrimImageToggle3 *pToggle = (CPrimImageToggle3 *) pPrim;

	pToggle->m_Axis = 1;
	}

// End of File
