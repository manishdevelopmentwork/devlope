
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbProfibus_HPP

#define	INCLUDE_UsbProfibus_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Profibus Port
//

class CUsbProfibus : public CUsbPort, public IProfibusPort
{
public:
	// Constructor
	CUsbProfibus(IUsbHostFuncDriver *pDriver);

	// Destructor
	~CUsbProfibus(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void  METHOD Bind(IPortHandler *pHandler);
	void  METHOD Bind(IPortSwitch  *pSwitch);
	UINT  METHOD GetPhysicalMask(void);
	BOOL  METHOD Open(CSerialConfig const &Config);
	void  METHOD Close(void);
	void  METHOD Send(BYTE bData);
	void  METHOD SetBreak(BOOL fBreak);
	void  METHOD EnableInterrupts(BOOL fEnable);
	void  METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL  METHOD GetInput(UINT uInput);
	DWORD METHOD GetHandle(void);

	// IProfibusPort
	void  METHOD Poll(void);
	BOOL  METHOD IsOnline(void);
	UINT  METHOD GetOutputSize(void);
	UINT  METHOD GetInputSize(void);
	PBYTE METHOD GetOutputBuffer(void);
	BOOL  METHOD PutInputData(PCBYTE pData, UINT uSize);

protected:
	// Data
	IUsbHostProfibus * m_pDriver;
	CSerialConfig      m_Config;
	bool		   m_fOnline;
	UINT		   m_uGetSize;
	UINT		   m_uPutSize;
	PBYTE		   m_pGetBuff[2];
	UINT               m_uGetBuff;
	PBYTE		   m_pGetData;
	bool		   m_fWaitGet;
	UINT               m_uDataErr;

	// Overridables
	void OnDriverBind(IUsbHostFuncDriver *pDriver);
	void OnInitDriver(void);
	void OnStartDriver(void);
	void OnStopDriver(void);
	void OnTermDriver(void);

	// Implementation 
	bool CheckOnline(void);
	void SetOnline(void);
	void SetOffline(void);
	bool CheckComms(void);
	void AbortComms(void);
	void InitData(void);
	void FreeData(void);
};

// End of File

#endif
