
#include "Intern.hpp"

#include "CryptoHmacSha1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA1 Cryptographic HMAC
//

// Instantiator

static IUnknown * Create_CryptoHmacSha1(PCTXT pName)
{
	return New CCryptoHmacSha1;
	}

// Registration

global void Register_CryptoHmacSha1(void)
{
	piob->RegisterInstantiator("crypto.hmac-sha1", Create_CryptoHmacSha1);
	}

// Constructor

CCryptoHmacSha1::CCryptoHmacSha1(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;
	}

// ICryptoHmac

CString CCryptoHmacSha1::GetName(void)
{
	return "sha1";
	}

void CCryptoHmacSha1::Initialize(PCBYTE pPass, UINT uPass)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psHmacSha1Init(&m_Ctx, pPass, uPass);

	m_uState = stateActive;
	}

void CCryptoHmacSha1::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psHmacSha1Update(&m_Ctx, pData, uData);
	}

void CCryptoHmacSha1::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psHmacSha1Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
