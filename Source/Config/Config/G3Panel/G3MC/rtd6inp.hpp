
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RTD6INP_HPP

#define INCLUDE_RTD6INP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRTD6Input;
class CRTD6ConfigWnd;

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Item
//

class CRTD6Input : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CRTD6Input(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Data Members
		UINT	m_TempUnits;
		UINT	m_InputType1;
		UINT	m_InputType2;
		UINT	m_InputType3;
		UINT	m_InputType4;
		UINT	m_InputType5;
		UINT	m_InputType6;
		UINT	m_InputFilter;
		INT	m_InputOffset1;
		INT	m_InputOffset2;
		INT	m_InputOffset3;
		INT	m_InputOffset4;
		INT	m_InputOffset5;
		INT	m_InputOffset6;
		INT	m_InputSlope1;
		INT	m_InputSlope2;
		INT	m_InputSlope3;
		INT	m_InputSlope4;
		INT	m_InputSlope5;
		INT	m_InputSlope6;
		UINT	m_ChanEnable1;
		UINT	m_ChanEnable2;
		UINT	m_ChanEnable3;
		UINT	m_ChanEnable4;
		UINT	m_ChanEnable5;
		UINT	m_ChanEnable6;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 6-Channel RTD Configuration View
//

class CRTD6ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CRTD6Input * m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Management
		void OnUICreate(void);
		void OnUIChange(CItem *pItem, CString Tag);

		// UI Creation
		void AddGeneral(void);
		void AddInputs(void);
	};

// End of File

#endif
