#include "intern.hpp"

#include "matfpdat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP MEWTOCOL-DAT (Binary) Base Driver
//

// Constructor

CMatFPDatBaseDriver::CMatFPDatBaseDriver(void)
{
	}

// Destructor

CMatFPDatBaseDriver::~CMatFPDatBaseDriver(void)
{
	}

CCODE CMatFPDatBaseDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Table;

	MakeMin(uCount, 26);

	StartFrame();

	AddByte(0x51);	// Read Data
	
	AddByte(FindAreaCode(uSpace));

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact() ) {

		GetData(Addr.a.m_Type, uCount, pData);

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CMatFPDatBaseDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uSpace = Addr.a.m_Table;
	
	MakeMin(uCount, 24);

	StartFrame();

	AddByte(0x50); // Write Data

	AddByte(FindAreaCode(uSpace));

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	PutData(Addr.a.m_Type, uCount, pData);
		
	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}
		
// Overrideables

void CMatFPDatBaseDriver::StartFrame(void)
{
	CMatFP2BaseDriver::StartFrame();
		
	AddByte( '%' );
		
	AddByte( m_pHex[m_pCtx->m_bDrop / 16] );
	AddByte( m_pHex[m_pCtx->m_bDrop % 16] );

	AddByte(0x80);
	}

UINT CMatFPDatBaseDriver::FindCommandCode(UINT uSpace, BOOL fWrite)
{

	return 0;
	}

UINT CMatFPDatBaseDriver::FindAreaCode(UINT uSpace)
{
	UINT uCode = 0;
	
	switch( uSpace ) {

		case SPACE_DT:		uCode = AREA_DT;	break;
		case SPACE_SDT:		uCode = AREA_SDT;	break;
		case SPACE_LD:		uCode = AREA_LD;	break;
		case SPACE_FL:		uCode = AREA_FL;	break;
		case SPACE_SV:		uCode = AREA_SV;	break;
		case SPACE_EV:		uCode = AREA_EV;	break;
		case SPACE_WR:		uCode = AREA_WR;	break;
		case SPACE_WL:		uCode = AREA_WL;	break;
		case SPACE_WX:		uCode = AREA_WX;	break;	
		case SPACE_WY:		uCode = AREA_WY;	break;
		case SPACE_SR:		uCode = AREA_SWR;	break;
		}

	return uCode;
	}

void CMatFPDatBaseDriver::AddWord(WORD wWord)
{
	AddByte(HIBYTE(wWord));

	AddByte(LOBYTE(wWord));
	}

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Driver via MEWTOCOL-DAT (Binary) Serial Driver
//

// Instantiator

INSTANTIATE(CMatFPDatSerialDriver);

// Constructor

CMatFPDatSerialDriver::CMatFPDatSerialDriver(void)
{
	m_Ident	      = DRIVER_ID;
	}

// Destructor

CMatFPDatSerialDriver::~CMatFPDatSerialDriver(void)
{
	}

// Configuration

void MCALL CMatFPDatSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CMatFPDatSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMatFPDatSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMatFPDatSerialDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CMatFPDatSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS; 
	}

CCODE MCALL CMatFPDatSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Overridables

BOOL CMatFPDatSerialDriver::SendFrame(void)
{
	AddByte(CR);
	
	m_pData->ClearRx();

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CMatFPDatSerialDriver::GetFrame(void)
{
	UINT uState = 0;

	UINT uByte = 0;

	UINT uTimer = 0;
	
	UINT uCount   = 0;	

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == '%' ) {

					uState = 1;
					}
				break;
				
			case 1:
				if( uByte == CR ) {
				
					if( CheckReply(uCount) ) {
						
						return TRUE;
						}
					
					return FALSE;
					}
								
				m_bRxBuff[uCount++] = uByte;
				
				if( uCount == sizeof(m_bRxBuff) ) {
					
					return FALSE;
					}
					
				break;
			}
		}
		
	return FALSE;
	}
	

BOOL CMatFPDatSerialDriver::CheckReply(UINT uCount)
{
	BYTE bCheck = '%';
	
	for( UINT uScan = 0; uScan < uCount - 2; uScan++ )  {

		bCheck ^= m_bRxBuff[uScan];
		}

	if( xtoin(PSTR(m_bRxBuff + uScan), 2) == bCheck ) {

		if( m_bRxBuff[2] == '!' ) {

			return FALSE;
			}

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CMatFPDatSerialDriver::Transact(void)
{
	SendFrame();
				
	return GetFrame();
	}

// End of File

