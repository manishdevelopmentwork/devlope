
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ProcCortexA8_HPP

#define INCLUDE_ProcCortexA8_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Flags
//

#define AEON_PROC_Cortex

#define AEON_PROC_CortexA8

// End of File

#endif
