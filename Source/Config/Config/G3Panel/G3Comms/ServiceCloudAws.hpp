
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudAws_HPP

#define INCLUDE_ServiceCloudAws_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AWS Cloud Client Configuration
//

class CServiceCloudAws : public CServiceCloudJson
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudAws(void);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Persistance
	void Init(void);
	void Load(CTreeFile &Tree);

	// Property Save Filter
	BOOL SaveProp(CString Tag) const;

	// Conversion
	void PostConvert(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Item Properties
	UINT         m_Shadow;
	CCodedItem * m_pPub;
	CCodedItem * m_pSub;
	CString      m_CertFile; // Orphan
	CString	     m_PrivFile; // Orphan
	CString      m_AuthFile; // Orphan

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	void InitOptions(void);
};

// End of File

#endif
