
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCmdStartStop_HPP

#define	INCLUDE_ScsiCmdStartStop_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Start Stop 
//

class CScsiCmdStartStop : public ScsiCmdStartStop
{
	public:
		// Constructor
		CScsiCmdStartStop(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(bool fStart);
	};

// End of File

#endif
