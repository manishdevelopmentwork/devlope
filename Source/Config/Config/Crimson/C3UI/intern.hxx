
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_BROWSE              0x4000
#define IDS_CHANGE_TO_S         0x4001
#define IDS_DEFAULT             0x4002
#define IDS_EDIT                0x4003
#define IDS_EDIT_CAPTION        0x4004
#define IDS_IS_NOT              0x4005
#define IDS_LARGEST             0x4006
#define IDS_NAME                0x4007
#define IDS_NONE                0x4008
#define IDS_OFF                 0x4009
#define IDS_ON                  0x400A
#define IDS_PICK_1              0x400B
#define IDS_PICK_2              0x400C
#define IDS_SELECT              0x400D
#define IDS_SELECT_OPTIONS      0x400E
#define IDS_SELECT_THE          0x400F
#define IDS_SMALLEST            0x4010
#define IDS_THAT_IS_NOT         0x4011
#define IDS_THE_SELECTION_IS    0x4012
#define IDS_THIS_PROPERTY_1     0x4013
#define IDS_THIS_PROPERTY_2     0x4014
#define IDS_UI_EDIT             0x4015
#define IDS_UI_FILE_PICK        0x4016
#define IDS_UI_WIN_SMALL        0x4017
#define IDS_VALUE               0x4018

// End of File

#endif
