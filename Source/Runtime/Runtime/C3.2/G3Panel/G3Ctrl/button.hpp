
//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BUTTON_HPP
	
#define	INCLUDE_BUTTON_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "stdctrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Button Base Class
//

class CStdButton : public CStdControl, public IButton
{
	public:
		// Constructor
		CStdButton(void);

		// Destructor
		~CStdButton(void);

		// Management
		UINT Release(void);
		
		// Creation
		void Create( IGdi     * pGDI,
			     INotify  * pNotify,
			     R2 const & Rect,
			     UINT       uID,
			     UINT       uFlags,
			     UINT	uStyle,
			     IStyle   * pStyle
			     );

		// Drawing
		void DrawPrep(IGdi *pGDI, IRegion *pErase);
		void DrawExec(IGdi *pGDI, IRegion *pDirty);

		// Touch Mapping
		void TouchMap(ITouchMap *pTouch);

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// Text Operations
		void SetText(PCUTF pText);
		void GetText(PUTF  pText);

		// State Operations
		void SetState(UINT uState);
		UINT GetState(void);

	protected:
		// Visual Style
		IGdiFont * m_pFont;
		COLOR	   m_colBorder;
		COLOR	   m_colText;
		COLOR	   m_colDisabled;

		// State Data
		WCHAR m_sText[256];
		BOOL  m_fPress;
		UINT  m_uState;

		// Overridables
		virtual void OnCreate  (IGdi *pGDI);
		virtual void OnDraw    (IGdi *pGDI);
		virtual void OnValidate(void);

		// Shader Selection
		PSHADER GetShader(UINT uState);

		// Implementation
		void DefStyle(void);
		void GetStyle(void);
	};

// End of File

#endif
