
#include "Intern.hpp"

#include "CommsSysBlockList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSysBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Block List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsSysBlockList, CItemIndexList);

// Constructor

CCommsSysBlockList::CCommsSysBlockList(void)
{
	m_Class = AfxRuntimeClass(CCommsSysBlock);
	}

// Item Access

CCommsSysBlock * CCommsSysBlockList::GetItem(INDEX Index) const
{
	return (CCommsSysBlock *) CItemIndexList::GetItem(Index);
	}

CCommsSysBlock * CCommsSysBlockList::GetItem(UINT uPos) const
{
	return (CCommsSysBlock *) CItemIndexList::GetItem(uPos);
	}

// Block Lookup

CCommsSysBlock * CCommsSysBlockList::FindBlock(UINT uBlock) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsSysBlock *pBlock = GetItem(n);

		if( pBlock->m_Number == uBlock ) {

			return pBlock;
			}

		GetNext(n);
		}

	return NULL;
	}

// Persistance

void CCommsSysBlockList::PreSnapshot(void)
{
	DeleteAllItems(TRUE);
	}

// End of File
