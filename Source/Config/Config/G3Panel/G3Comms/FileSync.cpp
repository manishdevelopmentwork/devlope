
#include "Intern.hpp"

#include "FileSync.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FTP Client Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CFileSync, CServiceItem);

// Constructor

CFileSync::CFileSync(void)
{
	m_pEnable    = NULL;
	m_pLogSync   = NULL;
	m_pFtpServer = NULL;
	m_pFtpPort   = NULL;
	m_pFtpSSL    = NULL;
	m_pUser      = NULL;
	m_pPass      = NULL;
	m_pPassive   = NULL;
	m_pPerFile   = NULL;
	m_pTestSize  = NULL;
	m_pKeep	     = NULL;
	m_pLogFile   = NULL;
	m_pFtpBase   = NULL;
	m_pFreq      = NULL;
	m_pFreqMins  = NULL;
	m_pDelay     = NULL;
	m_pDrive     = NULL;
	m_Debug      = 0;
	}

// UI Update

void CFileSync::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Enable" || Tag == "LogSync" ) {

		DoEnables(pHost);
		}

	if( Tag == "FtpSSL" ) {

		BOOL fUseSSL   = CheckEnable(m_pFtpSSL,  L">=", 1, TRUE);

		BOOL fImplicit = CheckEnable(m_pFtpSSL,  L"==", 1, TRUE);

		if( fUseSSL ) {

			m_pPassive->Compile(CError(FALSE), L"0x1");

			pHost->UpdateUI(L"Passive");
			}

		if( fImplicit ) {

			if( CheckEnable(m_pFtpPort,  L"==", 21, TRUE) ) {

				m_pFtpPort->Compile(CError(FALSE), CPrintf(L"0x%X", 990));

				pHost->UpdateUI(L"FtpPort");
				}
			}
		else {
			if( CheckEnable(m_pFtpPort,  L"==", 990, TRUE) ) {

				m_pFtpPort->Compile(CError(FALSE), CPrintf(L"0x%X", 21));

				pHost->UpdateUI(L"FtpPort");
				}
			}

		DoEnables(pHost);
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Initial Values

void CFileSync::SetInitValues(void)
{
	SetInitial(L"Enable",    m_pEnable,     0);
	SetInitial(L"LogSync",   m_pLogSync,    0);
	SetInitial(L"FtpServer", m_pFtpServer,  0);
	SetInitial(L"FtpPort",   m_pFtpPort,   21);
	SetInitial(L"FtpSSL",    m_pFtpSSL,	0);
	SetInitial(L"Passive",   m_pPassive,    0);
	SetInitial(L"PerFile",   m_pPerFile,    0);
	SetInitial(L"TestSize",  m_pTestSize,   0);
	SetInitial(L"Keep",      m_pKeep,      10);
	SetInitial(L"LogFile",   m_pLogFile,    0);
	SetInitial(L"Freq",      m_pFreq,       1);
	SetInitial(L"FreqMins",  m_pFreqMins,   0);
	SetInitial(L"Delay",     m_pDelay,      0);
	SetInitial(L"Drive",     m_pDrive,      0);
	}

// Type Access

BOOL CFileSync::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"User" || Tag == L"Pass" || Tag == L"FtpBase" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

UINT CFileSync::GetTreeImage(void) const
{
	return IDI_FTP_CLIENT;
	}

// Download Support

BOOL CFileSync::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(1));

	CServiceItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Debug));

	Init.AddItem(itemVirtual, m_pEnable);
	Init.AddItem(itemVirtual, m_pLogSync);
	Init.AddItem(itemVirtual, m_pFtpServer);
	Init.AddItem(itemVirtual, m_pFtpPort);
	Init.AddItem(itemVirtual, m_pFtpSSL);
	Init.AddItem(itemEncrypt, m_pUser);
	Init.AddItem(itemEncrypt, m_pPass);
	Init.AddItem(itemVirtual, m_pPassive);
	Init.AddItem(itemVirtual, m_pPerFile);
	Init.AddItem(itemVirtual, m_pTestSize);
	Init.AddItem(itemVirtual, m_pKeep);
	Init.AddItem(itemVirtual, m_pLogFile);
	Init.AddItem(itemVirtual, m_pFtpBase);
	Init.AddItem(itemVirtual, m_pFreq);
	Init.AddItem(itemVirtual, m_pFreqMins);
	Init.AddItem(itemVirtual, m_pDelay);
	Init.AddItem(itemVirtual, m_pDrive);

	return TRUE;
	}

// Meta Data Creation

void CFileSync::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(LogSync);
	Meta_AddVirtual(FtpServer);
	Meta_AddVirtual(FtpPort);
	Meta_AddVirtual(FtpSSL);
	Meta_AddVirtual(User);
	Meta_AddVirtual(Pass);
	Meta_AddVirtual(Passive);
	Meta_AddVirtual(PerFile);
	Meta_AddVirtual(TestSize);
	Meta_AddVirtual(Keep);
	Meta_AddVirtual(LogFile);
	Meta_AddVirtual(FtpBase);
	Meta_AddVirtual(Freq);
	Meta_AddVirtual(FreqMins);
	Meta_AddVirtual(Delay);
	Meta_AddVirtual(Drive);
	Meta_AddInteger(Debug);

	Meta_SetName((IDS_SYNC_MANAGER));
	}

// Implementation

void CFileSync::DoEnables(IUIHost *pHost)
{
	BOOL fEnable  = CheckEnable(m_pEnable,  L">=", 1, TRUE);

	BOOL fUseSSL  = CheckEnable(m_pFtpSSL,  L">=", 1, TRUE);

	BOOL fLogSync = CheckEnable(m_pLogSync, L">=", 1, TRUE);

	BOOL fLogs    = fEnable && fLogSync;

	pHost->EnableUI("LogSync",   fEnable);
	pHost->EnableUI("FtpBase",   fLogs  );
	pHost->EnableUI("Freq",      fLogs  );
	pHost->EnableUI("FreqMins",  fLogs  );
	pHost->EnableUI("Delay",     fLogs  );
	pHost->EnableUI("FtpServer", fEnable);
	pHost->EnableUI("FtpPort",   fEnable);
	pHost->EnableUI("FtpSSL",    fEnable);
	pHost->EnableUI("Passive",   fEnable && !fUseSSL);
	pHost->EnableUI("PerFile",   fEnable);
	pHost->EnableUI("TestSize",  fEnable);
	pHost->EnableUI("User",      fEnable);
	pHost->EnableUI("Pass",      fEnable);
	pHost->EnableUI("Keep",      fEnable);
	pHost->EnableUI("LogFile",   fEnable);
	pHost->EnableUI("Debug",     fEnable);
	pHost->EnableUI("Drive",     fEnable);
	}

// End of File
