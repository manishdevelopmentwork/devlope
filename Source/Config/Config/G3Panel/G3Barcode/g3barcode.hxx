
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Barcode Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3BARCODE_HXX
	
#define	INCLUDE_G3BARCODE_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pccore.hxx>

// End of File

#endif
