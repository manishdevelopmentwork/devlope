
#include "Intern.hpp"

#include "PxeHttpServerSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <errno.h>
#include <fcntl.h>
#include <io.h>

//////////////////////////////////////////////////////////////////////////
//
// Web Server Session
//

// Constructor

CPxeHttpServerSession::CPxeHttpServerSession(CHttpServer *pServer) : CHttpServerSession(pServer)
{
	m_dwLast	    = GetTickCount();

	m_Info.m_uWebAccess = NOTHING;
}

// Destructor

CPxeHttpServerSession::~CPxeHttpServerSession(void)
{
	AfxAssert(m_Pipes.IsEmpty());

	AfxAssert(m_Polls.IsEmpty());

	m_Console.Enable(FALSE);
}

// Attributes

UINT CPxeHttpServerSession::GetAccess(void) const
{
	return m_Info.m_uWebAccess;
}

BOOL CPxeHttpServerSession::IsLocalUser(void) const
{
	return m_Info.m_fLocal;
}

// Operations

void CPxeHttpServerSession::Throttle(UINT uLimit)
{
	DWORD dwTime = GetTickCount();

	DWORD dwGone = ToTime(dwTime - m_dwLast);

	if( dwGone < uLimit ) {

		Sleep(uLimit - dwGone);

		dwTime = GetTickCount();
	}

	m_dwLast = dwTime;
}

void CPxeHttpServerSession::SetUserInfo(CUserInfo const &Info)
{
	SetUser(Info.m_User);

	SetReal(Info.m_Real);

	m_Info = Info;
}

void CPxeHttpServerSession::ClearUserInfo(void)
{
	m_Info.m_uWebAccess = NOTHING;

	CHttpServerSession::ClearUserInfo();
}

BOOL CPxeHttpServerSession::ReadPipe(CString &Data, PCTXT pName, CHttpServerRequest *pReq)
{
	CString t = pReq->GetParamString("tab", "tab");

	bool    i = pReq->GetParamDecimal("init", 1) ? true : false;

	INDEX   n = m_Pipes.FindName(pName);

	CPipe * p = NULL;

	if( !m_Pipes.Failed(n) ) {

		p = m_Pipes[n];

		if( p->m_tab != t ) {

			if( i ) {

				Data = "Open in Another Tab\n";

				return FALSE;
			}

			return TRUE;
		}

		if( i ) {

			DeletePipe(p, n);

			Sleep(50);

			p = NULL;
		}
	}

	if( !p ) {

		int fd;
		
		if( (fd = open(pName, O_RDONLY | O_NDELAY)) < 0 ) {

			return FALSE;
		}

		if( i ) {

			Sleep(200);
		}

		p = New CPipe;

		p->m_name = pName;

		p->m_tab  = t;
		
		p->m_last = time(NULL);
		
		p->m_fd   = fd;

		m_Pipes.Insert(pName, p);
	}

	if( p ) {

		struct stat st;

		if( stat(p->m_name, &st) < 0 ) {

			close(p->m_fd);

			m_Pipes.Remove(p->m_name);

			delete p;

			Data = "\x01A";

			return FALSE;
		}

		for( ;;) {

			ssize_t s;

			char    c[256];

			if( (s = read(p->m_fd, c, sizeof(c))) < 0 ) {

				if( errno != EAGAIN ) {

					close(p->m_fd);

					m_Pipes.Remove(p->m_name);

					delete p;

					return FALSE;
				}

				break;
			}

			if( s > 0 ) {

				Data.Append(c, s);
			}

			if( s < sizeof(c) ) {

				break;
			}
		}

		p->m_last = time(NULL);

		return !i && Data.IsEmpty();
	}

	return FALSE;
}

BOOL CPxeHttpServerSession::SlowPoll(PCTXT pName, CString const &Data, CHttpServerRequest *pReq)
{
	CPrintf t = CPrintf("%s-%s", pName, PCTXT(pReq->GetParamString("tab", "tab")));

	INDEX   n = m_Polls.FindName(t);

	CPoll * p = NULL;

	if( !m_Polls.Failed(n) ) {

		p = m_Polls[n];

		if( pReq->GetParamDecimal("init", 1) ) {

			DeletePoll(p, n);

			p = NULL;
		}
	}

	if( !p ) {

	//	AfxTrace("created poll for %s\n", t.data());

		p = New CPoll;

		p->m_name = t;

		p->m_data = Data;

		p->m_last = time(NULL);

		m_Polls.Insert(t, p);

		return FALSE;
	}

	if( p ) {

		if( p->m_data == Data ) {

			p->m_last = time(NULL);

			return TRUE;
		}

	//	AfxTrace("updated poll for %s\n", p->m_name.data());

		p->m_data = Data;

		p->m_last = time(NULL);
	}

	return FALSE;
}

// Diagnostics

UINT CPxeHttpServerSession::GetDiagColCount(void)
{
	return 5;
}

void CPxeHttpServerSession::GetDiagCols(IDiagOutput *pOut)
{
	CHttpServerSession::GetDiagCols(pOut);
}

void CPxeHttpServerSession::GetDiagInfo(IDiagOutput *pOut)
{
	CHttpServerSession::GetDiagInfo(pOut);
}

// Overridables

void CPxeHttpServerSession::OnTickle(void)
{
	for( INDEX i = m_Pipes.GetHead(); !m_Pipes.Failed(i); m_Pipes.GetNext(i) ) {

		CPipe *p = m_Pipes[i];

		INDEX  n = i;

		m_Pipes.GetNext(n);

		if( time(NULL) >= p->m_last + 5 ) {

			DeletePipe(p, i);
		}

		i = n;
	}

	for( INDEX i = m_Polls.GetHead(); !m_Polls.Failed(i); m_Polls.GetNext(i) ) {

		CPoll *p = m_Polls[i];

		INDEX  n = i;

		m_Polls.GetNext(n);

		if( time(NULL) >= p->m_last + 5 ) {

		//	AfxTrace("dropped poll for %s\n", p->m_name.data());

			DeletePoll(p, i);
		}

		i = n;
	}
}

void CPxeHttpServerSession::FreeData(void)
{
	INDEX i;

	while( !m_Pipes.Failed(i = m_Pipes.GetHead()) ) {

		CPipe *p = m_Pipes[i];

		DeletePipe(p, i);
	}

	while( !m_Polls.Failed(i = m_Polls.GetHead()) ) {

		CPoll *p = m_Polls[i];

		DeletePoll(p, i);
	}
}

// Implementation

void CPxeHttpServerSession::DeletePipe(CPipe *p, INDEX i)
{
	close(p->m_fd);

	m_Pipes.Remove(i);

	delete p;
}

void CPxeHttpServerSession::DeletePoll(CPoll *p, INDEX i)
{
	m_Polls.Remove(i);

	delete p;
}

// End of File
