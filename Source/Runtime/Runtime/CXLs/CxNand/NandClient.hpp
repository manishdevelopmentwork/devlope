
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Utilities
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandClient_HPP

#define	INCLUDE_NandClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "NandSector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Support
//

class DLLAPI CNandClient
{
	public:
		// Constructor
		CNandClient(void);

		// Destructor
		virtual ~CNandClient(void);

	protected:
		// Data Members
		INandMemory   * m_pNandMemory;
		CNandGeometry	m_Geometry;
		UINT		m_uPageSize;
		PBYTE		m_pPageData;
		PBYTE		m_pTestData;
		CNandBlock	m_BlockStart;
		CNandBlock	m_BlockEnd;
		CNandPage	m_PageCurrent;
		UINT		m_uIndexStart;
		UINT		m_uIndexEnd;
		PDWORD		m_pBadBlocks;
		UINT		m_uBadBlocks;
		UINT		m_uGoodPages;
		UINT		m_uFreePages;

		// Initialisation
		void Init(void);
		bool EraseFirst(bool fForce);
		bool EraseAll(bool fForce);

		// Write with Relocation
		bool WriteWithReloc(CNandPage &Page);
		bool CopyPages(CNandPage &Dest, CNandPage &From, CNandPage Stop);

		// Bad Block Mapping
		bool CreateBadBlockMap(void);
		UINT GetBlockIndex(CNandBlock const &Block);
		bool IsBadBlock(CNandBlock const &Block);
		void MarkBlockBad(CNandBlock const &Block);

		// Navigation
		bool SkipBadBlocks    (CNandBlock  &Next);
		bool GetNextGoodBlock (CNandBlock  &Next, bool &fWrap);
		bool GetNextGoodBlock (CNandPage   &Next, bool &fWrap);
		bool GetNextGoodBlock (CNandBlock  &Next);
		bool GetNextGoodBlock (CNandPage   &Next);
		bool GetNextGoodPage  (CNandPage   &Next);
		bool GetNextGoodSector(CNandSector &Next);

		// NAND Device Wrappers
		bool EraseBlock (CNandBlock const &Block, bool fForce);
		bool ReadPage   (CNandPage const &Page, PBYTE pBuffer);
		bool WritePage  (CNandPage const &Page, PBYTE pBuffer, BOOL fCheck);
		bool ReadPage   (CNandPage const &Page);
		bool WritePage  (CNandPage const &Page, BOOL fCheck);
		bool ReadSector (CNandSector const &Sector, PBYTE pBuffer);
		bool WriteSector(CNandSector const &Sector, PBYTE pBuffer, BOOL fCheck);
		bool ReadSector (CNandSector const &Sector);
		bool WriteSector(CNandSector const &Sector, BOOL fCheck);

		// Implementation
		bool IsBlockEmpty(CNandBlock const &Block);

		// Overridables
		virtual bool OnPageReloc(PCBYTE pData, CNandPage const &Dest);

		// Debug
		void Trace(CNandPage   const &Page,   char const *pAction);
		void Trace(CNandSector const &Sector, char const *pAction);
		void Trace(char const *pText, ...);
	};

// End of File

#endif
