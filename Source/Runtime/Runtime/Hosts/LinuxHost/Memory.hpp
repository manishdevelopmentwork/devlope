
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Memory_HPP

#define	INCLUDE_Memory_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Emulated Memory Device
//

class CMemory
{
public:
	// Constructor
	CMemory(void);

	// Destructor
	~CMemory(void);

protected:
	// Data Members
	CString m_Name;
	UINT    m_uSize;
	BOOL    m_fMap;
	PBYTE	m_pData;
	int     m_fd;

	// Implementation
	void    AllocData(void);
	void    FreeData(void);
	CString FindPath(void);
	void    Commit(void);
};

// End of File

#endif
