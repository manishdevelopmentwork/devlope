
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostAddress_HPP

#define	INCLUDE_UsbHostAddress_HPP

/////////////////////////////////////////////////////////////////////////
//
// Usb Host Address Allocator
//

class CUsbHostAddress
{
	public:
		// Constructor
		CUsbHostAddress(void);

		// Destructor
		~CUsbHostAddress(void);

		// Operations
		void Init(void);
		UINT Allocate(void);
		void Free(UINT uAddr);
		bool LockDefault(void);
		bool FreeDefault(void);
		
	protected:
		// Data
		IMutex * m_pLock;
		bool     m_fDefLock;
		DWORD    m_Address[128 / 32];
	};

// End of File

#endif
