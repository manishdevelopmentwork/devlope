
#include "Intern.hpp"

#include "TagSimple.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Simple Tag Item
//

// Dynamic Class

AfxImplementDynamicClass(CTagSimple, CDataTag);

// Constructor

CTagSimple::CTagSimple(void)
{
	}

// UI Creation

BOOL CTagSimple::OnLoadPages(CUIPageList *pList)
{
	if( m_pDbase->GetSoftwareGroup() == SW_GROUP_2 ) {

		CUIPage *pPage1 = New CUIStdPage(CString(IDS_DATA), AfxRuntimeClass(CTagSimple), 8);

		pList->Append(pPage1);
		}
	else {
		CUIPage *pPage1 = New CUIStdPage(CString(IDS_DATA), AfxRuntimeClass(CTagSimple), 1);

		pList->Append(pPage1);
		}

	return TRUE;
	}

// UI Update

void CTagSimple::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Value" ) {

		if( m_pValue ) {

			if( m_pValue->IsConst() ) {

				KillCoded(pHost, L"Sim", m_pSim);
				}
			}

		CheckCircular(pHost, m_pValue);

		UpdateType(pHost, L"Sim", m_pSim, TRUE);

		Recompile(pHost);

		DoEnables(pHost);
		}

	if( Tag == "Extent" ) {

		Recompile(pHost);

		DoEnables(pHost);
		}

	CDataTag::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CTagSimple::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagInherent;

		return TRUE;
		}

	if( Tag == "Sim" ) {

		Type.m_Type  = GetDataType();

		Type.m_Flags = flagConstant;

		return TRUE;
		}

	return CDataTag::GetTypeData(Tag, Type);
	}

// Attributes

UINT CTagSimple::GetTreeImage(void) const
{
	if( m_pValue ) {

		return GetImage(IDI_RED_INTEGER);
		}

	return IDI_YELLOW_INTEGER;
	}

UINT CTagSimple::GetDataType(void) const
{
	if( m_pValue ) {

		UINT Type = m_pValue->GetType();

		if( Type != typeVoid ) {

			return Type;
			}
		}

	return typeInteger;
	}

UINT CTagSimple::GetTypeFlags(void) const
{
	if( m_Extent ) {

		return flagTagRef | flagArray;
		}

	return flagTagRef;
	}

// Operations

void CTagSimple::UpdateTypes(BOOL fComp)
{
	UpdateCircular();
	}

// Evaluation

DWORD CTagSimple::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText();

		case tpLabel:
			return FindLabel();
		}

	return CDataTag::GetProp(ID, Type);
	}

// Download Support

BOOL CTagSimple::MakeInitData(CInitData &Init)
{
	Init.AddByte(5);

	CDataTag::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CTagSimple::AddMetaData(void)
{
	CDataTag::AddMetaData();

	Meta_SetName((IDS_SIMPLE_TAG));
	}

// Property Access

DWORD CTagSimple::FindAsText(void)
{
	DWORD Data = Execute();

	UINT  Type = GetDataType();

	if( Type == typeInteger ) {

		CPrintf Text = CPrintf(L"%d", C3INT(Data));

		return DWORD(wstrdup(Text));
		}

	if( Type == typeReal ) {

		CPrintf Text = CPrintf(L"%0.0f", I2R(Data));

		return DWORD(wstrdup(Text));
		}

	return Data;
	}

DWORD CTagSimple::FindLabel(void)
{
	// LATER -- Refactor back into base class?

	// LATER -- This is a little dangerous as it is possible
	// that we have a circular reference in the label that
	// uses the tag property syntax to reference itself!!!

	if( m_pLabel ) {

		return DWORD(wstrdup(m_pLabel->GetText()));
		}

	return DWORD(wstrdup(m_Name));
	}

// Implementation

void CTagSimple::DoEnables(IUIHost *pHost)
{
	if( m_pValue ) {

		pHost->EnableUI("Sim", !m_pValue->IsConst());
		}
	else
		pHost->EnableUI("Sim", TRUE );
	}

// End of File
