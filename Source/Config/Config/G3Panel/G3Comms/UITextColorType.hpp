
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextColorType_HPP

#define INCLUDE_UITextColorType_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Color Type
//

class CUITextColorType : public CUITextEnumPick
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextColorType(void);

	protected:
		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(UINT Data, CString Text);
		void AddData(void);
	};

// End of File

#endif
