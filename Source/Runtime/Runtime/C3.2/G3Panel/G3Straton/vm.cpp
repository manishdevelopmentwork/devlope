
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Virtual Machine
//

// Return code

#define	IsOkay(x) ((x) == T5RET_OK)

// Pointer

global CVirtualMachine * g_pVM	= NULL;

// Instantiator

global void Create_VirtualMachine(CControlProject *pProject)
{
	New CVirtualMachine(pProject);
	}

// Constructor

CVirtualMachine::CVirtualMachine(CControlProject *pProject)
{
	StdSetRef();

	g_pVM   = this;

	m_pDiag = NULL;

	m_pBase	= NULL;

	m_pCode = NULL;

	m_pDiag = NULL;

	m_pProj = pProject;

	AddDiagCmds();	
	}

// Destructor

CVirtualMachine::~CVirtualMachine(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;
		}
	}

// Entry Points

void CVirtualMachine::Init(void)
{
	New CRetainMram;

	m_uState = vmIdle;
	}

void CVirtualMachine::Poll(void)
{
	switch( m_uState ) {

		case vmIdle:	DoIdle();	break;

		case vmOpen:	DoOpen();	break;

		case vmInit:	DoInit();	return;

		case vmPoll:	DoPoll();	return;

		case vmKill:	DoKill();	break;

		case vmFail:	DoFail();	break;

		case vmHalt:	/*DoHalt();*/	break;
		}
	}

BOOL CVirtualMachine::Service(PCBYTE pIn, PBYTE pOut)
{
	if( m_uState == vmPoll ) {

		T5VM_Serve(m_pBase, PBYTE(pIn), pOut);

		return TRUE;
		}

	return FALSE;
	}

void CVirtualMachine::Term(void)
{
	if( m_uState == vmInit || m_uState == vmPoll ) {

		SetState(vmKill);

		while( m_uState != vmHalt && m_uState != vmFail ) {

			Poll();
			}
		}
	}

// Operations

void CVirtualMachine::SetState(UINT uState)
{
	if( m_uState != uState ) {

		/*AfxTrace("state %d >> %d\n", m_uState, uState);*/
		
		m_uState = uState;
		}
	}

void CVirtualMachine::BuildMainLinks(void)
{
	T5VM_BuildDBMainLinks(m_pBase);
	}

DWORD CVirtualMachine::GetTotalDBSize(void)
{
	return T5VM_GetTotalDBSize(m_Table);
	}

BOOL CVirtualMachine::GetDBSize(void)
{
	return IsOkay(T5VM_GetDBSize(m_pCode, m_Table));
	}

BOOL CVirtualMachine::Build(void)
{
	return FALSE;
	}

BOOL CVirtualMachine::LoadCode(void)
{
	FreeCode();	
	
	if( g_pMM->LoadCode() ) {
		
		return (m_pCode = g_pMM->LinkCode()) != NULL;
		}

	return FALSE;
	}

void CVirtualMachine::FreeCode(void)
{
	if( m_pCode ) {

		g_pMM->UnlinkCode();

		g_pMM->FreeCode();

		m_pCode = NULL;
		}
	}

void CVirtualMachine::FreeVMDB(void)
{
	if( m_pBase ) {
			
		g_pMM->UnlinkVMDB();

		g_pMM->FreeVMDB();

		m_pBase = NULL;		
		}
	}

// IUnknown

HRESULT CVirtualMachine::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CVirtualMachine::AddRef(void)
{
	StdAddRef();
	}

ULONG CVirtualMachine::Release(void)
{
	StdRelease();
	}

// Implementation

BOOL CVirtualMachine::LoadVMDB(void)
{
	memset(&m_Table, 0, sizeof(m_Table));

	if( IsOkay(T5VM_GetDBSize(m_pCode, m_Table)) ) {

		DWORD dwSize = T5VM_GetTotalDBSize(m_Table);

		if( g_pMM->AllocRawVMDB(dwSize) ) {

			if( (m_pBase = (T5PTR_DB) g_pMM->LinkVMDB(dwSize)) ) {

				memcpy(m_pBase, m_Table, sizeof(m_Table));
				
				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CVirtualMachine::ColdStart(BOOL fWarm)
{
	return IsOkay(T5Def_ColdStart( m_pCode, 
				       m_pBase,
				       NULL,
				       NULL, 
				       fWarm, 
				       FALSE, 
				       "t5"				       
				       ));

	}

void CVirtualMachine::DoIdle(void)
{
	SetState(vmOpen);
	}

void CVirtualMachine::DoInit(void)
{
	g_pPM->SetData();

	m_dwTime = GetTickCount();

	SetState(vmPoll);
	}

void CVirtualMachine::DoPoll(void)
{
	if( m_pBase ) {

		g_pPM->GetData();

		T5Def_Cycle(m_pBase, ToTime(m_dwTime), NULL);

		g_pPM->SetData();

		DWORD t2 = GetTickCount();

		DWORD dt = t2 - m_dwTime;

		m_dwTime = t2;

		// MikeG -- Pretty sure cycle time is between subsequent calls? This
		// is only accurate to a tick, but it ought to be long-term accurate
		// in that the sum of the cycle times is equal to elapsed time.

		T5VM_RegisterCycleTime(m_pBase, 1000 * ToTime(dt));
		}
	}

void CVirtualMachine::DoKill(void)
{
	if( m_pCode ) {

		g_pMM->SaveVMDB("\0");

		T5Def_Stop( m_pBase, 
			    NULL, 
			    NULL, 
			    T5VMCode_GetAppName(m_pCode)
			    );

	//	g_pMM->SaveChange("\0");

		FreeVMDB();

		FreeCode();

		SetState(vmHalt);
		}
	}

void CVirtualMachine::DoFail(void)
{
	Sleep(FOREVER);
	}

BOOL CVirtualMachine::DoOpen(void)
{
	if( LoadCode() ) {

		if( LoadVMDB() ) {

			BuildMainLinks();

			BOOL fWarm = m_pProj->UseWarmStart();

			if( ColdStart(fWarm) ) {

				SetState(vmInit);
				
				return TRUE;
				}

			FreeVMDB();
			}

		FreeCode();
		}

	SetState(vmFail);

	return FALSE;
	}

// End of File
