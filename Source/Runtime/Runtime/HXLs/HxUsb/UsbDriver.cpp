
#include "Intern.hpp"

#include "UsbDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Driver 
//

// Constructor

CUsbDriver::CUsbDriver(void)
{
	StdSetRef();

	m_Debug = debugNone;

	m_pName = "Usb Driver";
	}

// Destructor

CUsbDriver::~CUsbDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");
	}

// Debugging

void CUsbDriver::Trace(UINT uMask, PCTXT pFormat, ...)
{
	#if defined(_DEBUG)

	if( uMask & m_Debug ) {

		va_list pArgs;

		va_start(pArgs, pFormat);

		char sText[256];

		vsprintf(sText, pFormat, pArgs);

		AfxTrace("%s : ", m_pName);

		AfxTrace(sText);

		AfxTrace("\n");
		}
	
	#endif
	}

// End of File
