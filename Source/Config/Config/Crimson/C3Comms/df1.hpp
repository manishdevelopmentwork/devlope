
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DF1_HPP
	
#define	INCLUDE_DF1_HPP

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Base Driver
//

#include "abbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Driver Options
//

class CDF1MasterSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDF1MasterSerialDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Address;
		BOOL m_fCRC;
		BOOL m_fHalf;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Device Options
//

class CDF1MasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDF1MasterSerialDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Device;
		UINT m_Dest;
		UINT m_Time1;	  		
		UINT m_Time2;

	protected:

		// Data Members
		CString m_Root;
		UINT	m_Name;
		UINT	m_Label;
	
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Serial Master Driver
//

class CDF1Driver : public CABDriver
{
	public:
		// Constructor
		CDF1Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
		
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Serial Slave Driver
//

class CDF1SerialSlaveDriver : public CABDriver
{
	public:
		// Constructor
		CDF1SerialSlaveDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDeviceConfig(void);


	protected:
		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master via DF1 over TCP/IP Device Options
//

class CPLC5DF1MasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPLC5DF1MasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT	m_Device;
		DWORD	m_Address;
		UINT	m_Port;
		UINT	m_Drop;
		UINT	m_Keep;
		UINT	m_Ping;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;

	protected:

		// Data Members
		CString m_Root;
		UINT	m_Name;
		UINT	m_Label;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Encapsulated DF1 CP/IP Device Options
//

class CEncDF1MasterTCPDeviceOptions : public CPLC5DF1MasterTCPDeviceOptions
{
 	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEncDF1MasterTCPDeviceOptions(void);

		// Public Data

		UINT m_TimeD;
		UINT m_TimeA;

	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);

       
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 over TCP/IP Base Driver
//

class CPLC5DF1TCPDriver : public CDF1Driver
{
	public:
		// Constructor
		CPLC5DF1TCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master via DF1 over TCP/IP
//

class CPLC5DF1MasterTCPDriver : public CPLC5DF1TCPDriver
{
	public:
		// Constructor
		CPLC5DF1MasterTCPDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Encapsulated DF1 TCP/IP Master
//

class CEncDF1MasterTCPDriver : public CPLC5DF1TCPDriver
{
	public:
		// Constructor
		CEncDF1MasterTCPDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley DF1 Slave TCP/IP Driver Options
//

class CDf1SlaveTCPDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDf1SlaveTCPDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Device;
		UINT m_Port;
		UINT m_Count;
		UINT m_Restrict;
		UINT m_SecAddr;
		UINT m_SecMask;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Slave via DF1 over TCP/IP
//

class CPLC5DF1SlaveTCPDriver : public CPLC5DF1TCPDriver
{
	public:
		// Constructor
		CPLC5DF1SlaveTCPDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Driver Data
		UINT GetFlags(void);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);

		// Helpers
		BOOL IsSlave(void);
		
	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
