
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosBaseModelData_HPP

#define INCLUDE_RlosBaseModelData_HPP

//////////////////////////////////////////////////////////////////////////
//
// RLOS Base Model Data
//

class CRlosBaseModelData : public IPxeModel
{
public:
	// Constructors
	CRlosBaseModelData(void);
	CRlosBaseModelData(CString Model);

	// Destructor
	virtual ~CRlosBaseModelData(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IPxeModel
	BOOL  METHOD AdjustHardware(CJsonData *pData);
	BOOL  METHOD ApplyModelSpec(CString Model);
	BOOL  METHOD AppendModelSpec(CString &Model, CJsonData *pData);
	UINT  METHOD GetPortType(UINT uPort);

protected:
	// Data Members
	ULONG	    m_uRefs;
	IPlatform * m_pPlatform;
	CString     m_Model;
	CString     m_Variant;
	CString	    m_Options;
	DWORD	    m_dwSize;

	// Implementation
	BOOL ParseSize(CString s);
	BOOL ParseOptions(CMap<CString, CString> &Map);
};

// End of File

#endif
