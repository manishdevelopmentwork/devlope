
#include "Intern.hpp"

#include "UsbEndpointDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Endpoint Descriptor
//

// Constructor

CUsbEndpointDesc::CUsbEndpointDesc(void)
{
	Init();
	}

// Endianess

void CUsbEndpointDesc::HostToUsb(void)
{
	m_wMaxPacket = HostToIntel(m_wMaxPacket);
	}

void CUsbEndpointDesc::UsbToHost(void)
{
	m_wMaxPacket = IntelToHost(m_wMaxPacket);
	}

// Attributes

BOOL CUsbEndpointDesc::IsValid(void) const
{
	return m_bType == descEndpoint && m_bLength == sizeof(UsbEndpointDesc);
	}

// Init

void CUsbEndpointDesc::Init(void)
{
	memset(this, 0, sizeof(UsbEndpointDesc));
	
	m_bLength = sizeof(UsbEndpointDesc);
	
	m_bType	  = descEndpoint;
	}

// Debug

void CUsbEndpointDesc::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nUsb Endpoint Descriptor\n");

	AfxTrace("Type         = %d\n",      m_bType);
	AfxTrace("Len          = %d\n",      m_bLength);
	AfxTrace("Addr         = %d\n",      m_bAddr);
	AfxTrace("DirIn        = %d\n",      m_bDirIn);
	AfxTrace("Transfers    = %d\n",      m_bTransfer);
	AfxTrace("Synch        = %d\n",      m_bSynch);
	AfxTrace("Usage        = %d\n",      m_bUsage);
	AfxTrace("Max Packet   = 0x%4.4X\n", m_wMaxPacket);
	AfxTrace("Interval     = %d\n",      m_bInterval);

	#endif
	}

// End of File
