
//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSetDescriptorReq_HPP

#define	INCLUDE_UsbSetDescriptorReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbGetDescriptorReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Descriptor Standard Device Requeset
//

class CUsbSetDescriptorReq : public CUsbGetDescriptorReq
{
	public:
		// Constructor
		CUsbSetDescriptorReq(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
