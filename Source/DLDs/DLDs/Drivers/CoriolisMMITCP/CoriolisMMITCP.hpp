#include "intern.hpp"

#include "ModbusTCP.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CoriolisMMI Modbus TCP Driver
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

enum Channels 
{
	holdingRegisters = 1,
	inputRegisters	 = 2,
	outputCoils	 = 3,
	inputCoils	 = 4,
	Coils		 = 5,
	Registers	 = 6,

	MAX_CHANNELS,
	};

class CCoriolisTCPDriver : public CModbusTCPMaster
{
	public:
		// Constructor
		CCoriolisTCPDriver(void);

		// Destructor
		~CCoriolisTCPDriver(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:	

		// Context
		struct CTCPContext : public CContext {

			UINT		m_uNumHoldingRegs;
			UINT		m_uNumCoilRegs;

			CAddress	*m_pHoldingRegisters;
			CAddress	*m_pCoilRegisters;
			};

		// Data Members
		CTCPContext *m_pTCPCtx;
		 
		// Implemetation
		BOOL GetMaxCount(AREF Address, UINT &uCount);
		UINT	   GetSize(AREF Addr);
		UINT	   GetRegTotal(BOOL fCoils);
		CAddress * GetRegArray(BOOL fCoils);

		// Helpers
		BOOL IsCoils(UINT uTable);
	};

// End of File
