
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_EtherNetIp_HPP

#define INCLUDE_EtherNetIp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Reference Objects
//

class CImplicit;
class CExplicit;

//////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Stack
//

class CEtherNetIp : public IEtherNetIp
{
	public:
		// Constructor
		CEtherNetIp(void);
		
		// Destructor
		~CEtherNetIp(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IEtherNetIp
		WORD        METHOD GetVendorId(void);
		BOOL        METHOD Open(void);
		BOOL        METHOD Close(void);
		BOOL        METHOD Connect(void);
		IImplicit * METHOD GetImplicit(void);
		IExplicit * METHOD GetExplicit(void);
		void	    METHOD FreeImplicit(IImplicit *p);
		void	    METHOD FreeExplicit(IExplicit *p);

	protected:
		// Static Data
		static CEtherNetIp * m_pThis;

		// Data
		ULONG	    m_uRefs;
		INT	    m_nOpen;
		INT	    m_nConnect;
		IMutex    * m_pMutex;
		IEvent    * m_pReady;
		CImplicit * m_pImplicit;
		CExplicit * m_pExplicit[4];

		// Object Helpers
		void InitObjects(void);
		void FreeObjects(void);

		// Entry Points
		void OnEvent (INT32 nEvent, INT32 nParam);
		void OnEventI(INT32 nEvent, INT32 nParam);
		void OnEventE(INT32 nEvent, INT32 nParam);
		
		// Friends
		friend void EthernetIPCallback(INT32 nEvent, INT32 nParam);
	};

// End of File

#endif
