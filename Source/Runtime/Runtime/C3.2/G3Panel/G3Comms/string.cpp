
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// String Display Format
//

// Constructor

CDispFormatString::CDispFormatString(void)
{
	m_Length = 4;
	}

// Destructor

CDispFormatString::~CDispFormatString(void)
{
	}

// Initialization

void CDispFormatString::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatString", pData);

	if( !(m_Length = GetWord(pData)) ) {

		m_Template = GetWide(pData);
		}
	}

// Operations

void CDispFormatString::SetLength(UINT Length)
{
	m_Length = Length;
	}

// Formatting

CUnicode CDispFormatString::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Type == typeVoid ) {

		return CUnicode('-', min(5, m_Length));
		}

	if( Type == typeString ) {

		if( m_Length ) {

			CUnicode Text = PCUTF(Data);

			Text.TrimBoth();

			return Text.Left(m_Length);
			}
		else {
			CUnicode Text = m_Template;

			PCUTF    pSrc = PCUTF(Data);

			for( UINT n = 0; n < Text.GetLength(); n++ ) {

				WCHAR cDef = 0;

				switch( Text[n] ) {

					case 'A':
					case 'a':
						cDef = 'A';
						break;

					case 'N':
					case 'n':
					case '0':
						cDef = '0';
						break;

					case 'X':
					case 'S':
					case 's':
					case 'M':
					case 'm':
						cDef = ' ';
						break;
					}

				if( cDef ) {

					if( *pSrc ) {

						Text.SetAt(n, *pSrc++);
						}
					else
						Text.SetAt(n, cDef);
					}
				}

			return Text;
			}
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormatString::Parse(DWORD &Data, CString Text, UINT Type)
{
	if( m_Length ) {

		if( Text.GetLength() <= m_Length ) {

			return GeneralParse(Data, Text, Type);
			}
		}
	else {
		CString Edit;

		for( UINT n = 0; n < Text.GetLength(); n++ ) {

			WCHAR cTemp = m_Template[n];

			if( cTemp ) {

				if( IsFormat(cTemp) ) {

					PCUTF pList = GetList(cTemp);

					if( wstrchr(pList, Text[n]) ) {

						Edit += Text[n];

						continue;
						}
					}
				else {
					if( cTemp == Text[n] ) {

						continue;
						}
					}
				}

			return FALSE;
			}

		return GeneralParse(Data, Edit, Type);
		}

	return FALSE;
	}

// Editing

UINT CDispFormatString::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		pEdit->m_uKeypad  = keypadAlpha;

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		return editUpdate;
		}

	if( uCode == 0x0D ) {

		CUnicode Text = pEdit->m_Edit;

		if( !m_Length ) {

			UINT c = 0;

			for( UINT n = 0; n < m_Template.GetLength(); n++ ) {

				if( !IsFormat(m_Template[n]) ) {

					Text.Delete(c, 1);

					continue;
					}

				c++;
				}
			}

		PUTF pText = wstrdup(Text);

		if( pEdit->m_pValue->SetValue(DWORD(pText), typeString, setNone) ) {

			return editCommit;
			}

		return editUpdate;
		}

	if( uCode == 0x1B ) {

		return editAbort;
		}

	if( uCode == 0x7F ) {

		if( pEdit->m_fDefault ) {

			pEdit->m_fDefault = FALSE;
			
			pEdit->m_uCursor  = cursorLast;

			return editUpdate;
			}
		else {
			UINT uLen = pEdit->m_Edit.GetLength();

			if( uLen ) {

				if( !m_Length ) {

					UINT n;

					for( n = 0; n < uLen - 1; n++ ) {

						if( !IsFormat(m_Template[n]) ) {

							break;
							}
						}

					if( n == uLen - 1 ) {

						return editNone;
						}

					while( !IsFormat(m_Template[uLen - 1]) ) {

						pEdit->m_Edit.Delete(--uLen, 1);
						}
					}

				pEdit->m_Edit.Delete(--uLen, 1);

				return editUpdate;
				}

			return editNone;
			}
		}

	if( uCode >= 0x20 ) {

		if( pEdit->m_fDefault ) {

			pEdit->m_Edit.Empty();
			
			pEdit->m_fDefault = FALSE;
			
			pEdit->m_uCursor  = cursorLast;
			}

		if( !m_Length ) {

			if( pEdit->m_Edit.GetLength() < m_Template.GetLength() ) {

				UINT  uLen  = pEdit->m_Edit.GetLength();

				WCHAR cData = m_Template[uLen];

				PCUTF pList = GetList(cData);

				if( wstrchr(pList, uCode) ) {

					pEdit->m_Edit += WCHAR(uCode);

					cData          = m_Template[++uLen];

					while( !IsFormat(cData) ) {

						pEdit->m_Edit += cData;

						cData          = m_Template[++uLen];
						}

					return editUpdate;
					}
				}
			}
		else {
			if( pEdit->m_Edit.GetLength() < m_Length ) {

				pEdit->m_Edit += WCHAR(uCode);

				return editUpdate;
				}
			}

		return editNone;
		}

	return editNone;
	}

// Implementation

BOOL CDispFormatString::IsFormat(WCHAR cData)
{
	switch( cData ) {

		case 'A':
		case 'a':
		case 'S':
		case 's':
		case 'N':
		case 'n':
		case 'M':
		case 'm':
		case '0':
		case 'X':
		case '*':
			return TRUE;
		}

	return FALSE;
	}

PCUTF CDispFormatString::GetList(WCHAR cData)
{
	switch( cData ) {
		
		case '0':
			return L"0123456789";

		case 'A':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		case 'a':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"abcdefghijklmnopqrstuvwxyz";
		
		case 'S':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L" ";

		case 's':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"abcdefghijklmnopqrstuvwxyz"
			       L" ";
		
		case 'N':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"0123456789";

		case 'n':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"abcdefghijklmnopqrstuvwxyz"
			       L"0123456789";
		
		case 'M':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"0123456789"
			       L" ";

		case 'm':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"abcdefghijklmnopqrstuvwxyz"
			       L"0123456789"
			       L" ";
		
		case 'X':
		case '*':
			return L"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			       L"abcdefghijklmnopqrstuvwxyz"
			       L"0123456789"
			       L" ,.:;+-=!?%/$";
		}

	return L" ";
	}

// End of File
