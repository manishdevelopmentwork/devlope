
#include "intern.hpp"

#include "cti2572.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CTI Space Wrapper
//

// Constructor

CSpaceCTI::CSpaceCTI(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)
{
	m_uTable	= uTable;
	
	m_Prefix	= p;

	m_Caption	= c;

	m_uRadix	= r;

	m_uMinimum	= n;
	
	m_uMaximum	= x;
	
	m_uType		= t;

	m_uSpan         = s;

	FindWidth();

	FindAlignment();
	}

// Limits

void CSpaceCTI::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	if( Addr.a.m_Table < 9 ) {

		Addr.a.m_Extra = 0xF;
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// CTI Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCTIMasterTCPDeviceOptions, CUIItem);       

// Constructor

CCTIMasterTCPDeviceOptions::CCTIMasterTCPDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD( 52, 1), MAKEWORD(  168, 192)));

	m_Port   = 1505;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CCTIMasterTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CCTIMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CCTIMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}


/////////////////////////////////////////////////////////////////////////
//
// CTI Master Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCTIMasterSerialDeviceOptions, CUIItem);

// Constructor

CCTIMasterSerialDeviceOptions::CCTIMasterSerialDeviceOptions(void)
{

	}


//////////////////////////////////////////////////////////////////////////
//
// CTI 2572 TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_CTI2572Driver(void)
{
	return New CCTI2572Driver;
	}

// Constructor

CCTI2572Driver::CCTI2572Driver(void)
{
	m_wID		= 0x4075;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "CTI";
	
	m_DriverName	= "2500/2572 CAMP TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "CAMP Master";

	AddSpaces();
	}

// Binding Control

UINT CCTI2572Driver::GetBinding(void)
{
	return bindEthernet;
	}

void CCTI2572Driver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CCTI2572Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCTIMasterTCPDeviceOptions);
	}

// Address Helpers

BOOL CCTI2572Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type  = StripType(pSpace, Text);

	UINT uExtra = tatoi(Text);

	Text = Text + "." + Type;

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Extra = ((uExtra >> 16) & 0xF);

		return TRUE; 
		}

	return FALSE;
	}

BOOL CCTI2572Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uType   = Addr.a.m_Type;

		UINT uOffset = Addr.a.m_Offset | (Addr.a.m_Extra << 16);

		if( uType == pSpace->m_uType ) {

			Text.Printf( "%s%7.7u", 
				     pSpace->m_Prefix, 
				     uOffset
				     );
			}
		else {
			Text.Printf( "%s%7.7u.%s", 
				     pSpace->m_Prefix, 
				     uOffset,
				     pSpace->GetTypeModifier(uType)
				     );
			}

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CCTI2572Driver::AddSpaces(void)
{
	AddSpace(New CSpaceCTI(0x01,	"V",	"Data Registers",		10, 1, 0xFFFFF,	addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpaceCTI(0x02,	"K",	"K Memory",			10, 1, 0xFFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x06,	"X",	"Discrete Input Packed",	10, 1, 0xFFFF,	addrBitAsWord,  addrBitAsWord ));
	AddSpace(New CSpaceCTI(0x07,	"Y",	"Discrete Output Packed",	10, 1, 0xFFFF,	addrBitAsWord,  addrBitAsWord ));
	AddSpace(New CSpaceCTI(0x08,	"C",	"Control Register Packed",	10, 1, 0xFFFF,	addrBitAsWord,  addrBitAsWord ));
	AddSpace(New CSpaceCTI(0x09,	"WX",	"Word Input",			10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x0A,	"WY",	"Word Output",			10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x0E,	"TCP",  "Timer/Counter Preset",		10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x0F,	"TCC",	"Timer/Counter Current",	10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x10,	"DSP",	"Drum Step Preset",		10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x11,	"DSC",	"Drum Step Current",		10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x12,	"DCP",	"Drum Count Preset (bits)",	10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x1A,	"STW",	"System Status Word",		10, 1, 0xFFFF,	addrWordAsWord,	addrWordAsReal));
	AddSpace(New CSpaceCTI(0x1B,	"DCC",	"Drum Current Count",		10, 1, 0xFFFF,  addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceCTI(0xF0,	"ERR",	"Latest Error Code",		10, 1, 1,	addrLongAsLong, addrLongAsLong));
	}



//////////////////////////////////////////////////////////////////////////
//
// CTI 25xx CAMP Master Serial Driver
//

// Instantiator

ICommsDriver * Create_CTICampMasterSerialDriver(void)
{
	return New CCTICampMasterSerialDriver;
	}

// Constructor

CCTICampMasterSerialDriver::CCTICampMasterSerialDriver(void)
{
	m_wID		= 0x4079;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "CTI";
	
	m_DriverName	= "25xx CAMP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "CAMP Master";
	}

// Binding Control

UINT CCTICampMasterSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CCTICampMasterSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CCTICampMasterSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCTIMasterSerialDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master Serial Driver
//

// Instantiator

ICommsDriver *	Create_CTINitpMasterDriver(void)
{
	return New CCTINitpMasterDriver;
	}

// Constructor

CCTINitpMasterDriver::CCTINitpMasterDriver(void)
{
	m_wID		= 0x4080;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "CTI";
			
	m_DriverName	= "NITP Serial Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "NITP Master";	
		
	}


//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master TCP/IP Driver
//

// Instantiator

ICommsDriver *	Create_CTINitpMasterTCPDriver(void)
{
	return New CCTINitpMasterTCPDriver;
	}

// Constructor

CCTINitpMasterTCPDriver::CCTINitpMasterTCPDriver(void)
{
	m_wID		= 0x4081;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "CTI";
			
	m_DriverName	= "NITP TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "NITP Master";
	}


// End of File
