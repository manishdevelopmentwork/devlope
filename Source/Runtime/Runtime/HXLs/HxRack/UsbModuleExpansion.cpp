
#include "Intern.hpp"

#include "UsbModuleExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Rack/Module Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbModuleExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = New CUsbModuleExpansion(pDriver);

	return p;
	}

// Constructor

CUsbModuleExpansion::CUsbModuleExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansion(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbModuleExpansion::GetName(void)
{
	switch( m_wProduct ) {

		case pidPid1:  return "GMPID1";
		case pidPid2:  return "GMPID2"; 
		case pidUin4:  return "GMUIN4";
		case pidOut4:  return "GMDOUT4";
		case pidDio14: return "GMDIO14";
		case pidTc8:   return "GMTC8";
		case pidIni8:  return "GMINI8";
		case pidInv8:  return "GMINV8";
		case pidRtd6:  return "GMRTD6";
		case pidSg1:   return "GMSG1";
		case pidRate:  return "GMRC";
		}
	
	return "Unknown Module";
	}

UINT METHOD CUsbModuleExpansion::GetClass(void)
{
	return rackClassRack;
	}

UINT METHOD CUsbModuleExpansion::GetPower(void)
{
	switch( m_wProduct ) {

		case pidPid1:  return 33;
		case pidPid2:  return 27; 
		case pidUin4:  return 18;
		case pidOut4:  return 43;
		case pidDio14: return 41;
		case pidTc8:   return 18;
		case pidIni8:  return 18;
		case pidInv8:  return 18;
		case pidRtd6:  return 18;
		case pidSg1:   return 60;
		case pidRate:  return 99; // TO DO
		}

	return 0;
	}

// End of File
