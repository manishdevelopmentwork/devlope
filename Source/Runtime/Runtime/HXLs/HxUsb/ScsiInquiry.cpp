
#include "Intern.hpp"

#include "ScsiInquiry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB SCSI Framework
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Inquiry Data Object
//

// Constructor

CScsiInquiry::CScsiInquiry(void)
{
	}

// Endianess

void CScsiInquiry::HostToScsi(void)
{
	m_dwRevision = HostToMotor(m_dwRevision);
	}

void CScsiInquiry::ScsiToHost(void)
{
	m_dwRevision = MotorToHost(m_dwRevision);
	}

// Operations

void CScsiInquiry::Init(void)
{
	memset(this, 0, sizeof(ScsiInquiry));

	m_bPDev       = 0x0;
	
	m_bRemoveable = 0x1;
	
	m_bAnsiVer    = 0x2;

	m_bRespFmt    = 0x2;

	m_bAddLen     = sizeof(ScsiInquiry) - 4;

	memset(m_sVendId, ' ',  8);
	
	memset(m_sProdId, ' ', 16);
	}

void CScsiInquiry::Init(PTXT pVend, PTXT pProd)
{
	memset(this, 0, sizeof(ScsiInquiry));

	m_bPDev       = 0x0;
	
	m_bRemoveable = 0x1;
	
	m_bAnsiVer    = 0x2;

	m_bRespFmt    = 0x2;

	m_bAddLen     = sizeof(ScsiInquiry) - 4;

	UINT uVendLen = strlen(pVend);

	UINT uProdLen = strlen(pProd); 

	MakeMin(uVendLen, sizeof(m_sVendId));

	MakeMin(uProdLen, sizeof(m_sProdId));

	memset(m_sVendId + uVendLen, ' ',  8 - uVendLen);
	
	memset(m_sProdId + uProdLen, ' ', 16 - uProdLen);

	memcpy(m_sVendId, pVend, uVendLen);
	
	memcpy(m_sProdId, pProd, uProdLen);
	}

// Debug

void CScsiInquiry::Dump(void)
{
	#if defined(_XDEBUG)

	AfxTrace("Scsi Inquiry\n");

	AfxTrace("bPQual      0x%2.2X\n", m_bPQual);
	AfxTrace("bPDev       0x%2.2X\n", m_bPDev);
	AfxTrace("bRemoveable 0x%2.2X\n", m_bRemoveable);
	AfxTrace("bISOVer     0x%2.2X\n", m_bIsoVer);
	AfxTrace("bECMAVer    0x%2.2X\n", m_bEcmaVer);
	AfxTrace("bANSIVer    0x%2.2X\n", m_bAnsiVer);
	AfxTrace("bAERC       0x%2.2X\n", m_bAerc);
	AfxTrace("bTrmTsk     0x%2.2X\n", m_bTrmTsk);
	AfxTrace("bNACA       0x%2.2X\n", m_bNaca);
	AfxTrace("bRespFmt    0x%2.2X\n", m_bRespFmt);
	AfxTrace("bAddLen     0x%2.2X\n", m_bAddLen);
	AfxTrace("bEncServ    0x%2.2X\n", m_bEncServ);
	AfxTrace("bVS1        0x%2.2X\n", m_bVs1);
	AfxTrace("bMultiP     0x%2.2X\n", m_bMultiP);
	AfxTrace("bMedChngr   0x%2.2X\n", m_bMedChngr);
	AfxTrace("bACKREQ     0x%2.2X\n", m_bAckReq);
	AfxTrace("bAddr32     0x%2.2X\n", m_bAddr32);
	AfxTrace("bAddr16     0x%2.2X\n", m_bAddr16);
	AfxTrace("bRelAddr    0x%2.2X\n", m_bRelAddr);
	AfxTrace("bWBus32     0x%2.2X\n", m_bWBus32);
	AfxTrace("bWBus16     0x%2.2X\n", m_bWBus16);
	AfxTrace("bSync       0x%2.2X\n", m_bSync);
	AfxTrace("bLinked     0x%2.2X\n", m_bLinked);
	AfxTrace("bTransDis   0x%2.2X\n", m_bTransDis);
	AfxTrace("bCmdQue     0x%2.2X\n", m_bCmdQue);
	AfxTrace("bVS2        0x%2.2X\n", m_bVs2);
	AfxTrace("dwRevision  0x%2.2X\n", m_dwRevision);

	#endif
	}

// End of File
