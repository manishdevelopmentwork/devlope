
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyRoundTank_HPP
	
#define	INCLUDE_PrimRubyRoundTank_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Round Tank Primitive
//

class CPrimRubyRoundTank : public CPrimRubyGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyRoundTank(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);

		// Data Members
		INT m_Base;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);
	};

// End of File

#endif
