
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagSelectDialog_HPP

#define INCLUDE_TagSelectDialog_HPP

////////////////////////////////////////////////////////////////////////
//
// Tag Selection Dialog
//

class DLLAPI CTagSelectDialog : public CStdDialog, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CTagSelectDialog(CDatabase *pDbase, CString Tag);
		CTagSelectDialog(CDatabase *pDbase, BOOL fFolder, CString Class);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Attributes
		CString GetTag(void) const;

	protected:
		// Data Members
		CDatabase * m_pDbase;
		BOOL        m_fBinding;
		BOOL        m_fFolder;
		CString     m_Class;
		CString     m_Tag;
		UINT        m_cfIdent;
		CDropHelper m_DropHelp;
		BOOL        m_fDrop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPaint(void);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCreate(UINT uID);
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);

		// Data Object Helpers
		BOOL CanAcceptDataObject(IDataObject *pData);
		BOOL AcceptDataObject(IDataObject *pData, CStringArray &List);
		BOOL AcceptDataObject(IDataObject *pData);

		// Implementation
		void FindBindTemplate(void);
		void PaintImage(CDC &DC, HGLOBAL hData);
	};

// End of File

#endif
