
#include "intern.hpp"

#include "graph.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Line Graph
//

// Scaling Macros

#define	ScaleX(v, mn, d)	(x1 + (v - (mn)) * (x2 - x1 - 3) / d + 1)

#define	ScaleY(v, mn, d)	(y2 - (v - (mn)) * (y2 - y1 - 3) / d - 2)

// Constructor

CPrimLineGraph::CPrimLineGraph(void)
{
	m_pValueX   = NULL;

	m_pValueY   = NULL;

	m_pCount    = NULL;

	m_pMinX     = NULL;
	
	m_pMaxX     = NULL;
		   
	m_pMinY     = NULL;
		   
	m_pMaxY     = NULL;

	m_pDataFill = New CPrimColor(naFeature);

	m_pDataLine = New CPrimColor(naFeature);

	m_pBestLine = New CPrimColor(naFeature);
	}

// Destructor

CPrimLineGraph::~CPrimLineGraph(void)
{
	delete m_pValueX;

	delete m_pValueY;

	delete m_pCount;

	delete m_pMinX;

	delete m_pMaxX;

	delete m_pMinY;

	delete m_pMaxY;

	delete m_pDataFill;

	delete m_pDataLine;

	delete m_pBestLine;

	FreeCtx(m_Ctx);
	}

// Operations

void CPrimLineGraph::SetScan(UINT Code)
{
	SetItemScan(m_pValueX, Code);

	SetItemScan(m_pValueY, Code);

	SetItemScan(m_pCount,  Code);

	SetItemScan(m_pMinX,   Code);
	SetItemScan(m_pMaxX,   Code);

	SetItemScan(m_pMinY,   Code);
	SetItemScan(m_pMaxY,   Code);

	m_pDataFill->SetScan(Code);

	m_pDataLine->SetScan(Code);

	m_pBestLine->SetScan(Code);
	}

void CPrimLineGraph::DrawItem(IGDI *pGDI, R2 Rect)
{
	C3REAL rDeltaX = m_Ctx.m_rMaxX - m_Ctx.m_rMinX;

	C3REAL rDeltaY = m_Ctx.m_rMaxY - m_Ctx.m_rMinY;

	if( rDeltaX && rDeltaY ) {

		int x1 = Rect.x1;
		int y1 = Rect.y1;
		int x2 = Rect.x2;
		int y2 = Rect.y2;

		if( m_Style > 0 ) {

			pGDI->SetPenFore(m_pDataLine->GetColor());

			int ox = 0;

			int oy = 0;
		
			for( UINT n = 0; n < m_Ctx.m_uCount; n++ ) {

				int xp = int(ScaleX(m_Ctx.m_pValX[n], m_Ctx.m_rMinX, rDeltaX));

				int yp = int(ScaleY(m_Ctx.m_pValY[n], m_Ctx.m_rMinY, rDeltaY));

				if( n ) pGDI->DrawLine(ox, oy, xp, yp);

				ox = xp;

				oy = yp;
				}
			}

		if( m_Style < 2 ) {

			pGDI->SetBrushFore(m_pDataFill->GetColor());
		
			for( UINT n = 0; n < m_Ctx.m_uCount; n++ ) {
				
				int xp = int(ScaleX(m_Ctx.m_pValX[n], m_Ctx.m_rMinX, rDeltaX));

				int yp = int(ScaleY(m_Ctx.m_pValY[n], m_Ctx.m_rMinY, rDeltaY));
				
				pGDI->FillRect(xp-1,yp-1,xp+2,yp+2);
				}
			}

		if( m_Fit > 0 ) {

			double SumX  = 0;

			double SumX2 = 0;

			double SumXY = 0;

			double SumY  = 0;

			UINT n;
		
			for( n = 0; n < m_Ctx.m_uCount; n++ ) {

				double x = m_Ctx.m_pValX[n];

				double y = m_Ctx.m_pValY[n];

				SumX  = SumX  + x;

				SumX2 = SumX2 + x * x;

				SumXY = SumXY + x * y;

				SumY  = SumY  + y;
				}

			double bd = (n * SumX2) - (SumX * SumX);

			double bn = (n * SumXY) - (SumX * SumY);

			if( bd && n ) {

				double b = (bn / bd);

				double a = (SumY - b * SumX) / n;

				////////

				double yMin = a + b * m_Ctx.m_rMinX;

				double yMax = a + b * m_Ctx.m_rMaxX;

				double xa = x1;

				double xb = x2 - 1;

				double ya = ScaleY(yMin, m_Ctx.m_rMinY, rDeltaY);

				double yb = ScaleY(yMax, m_Ctx.m_rMinY, rDeltaY);

				////////
				
				if( ya < y1 || ya >= y2 ) {

					if( b > 0 ) {

						double xMin = (m_Ctx.m_rMinX - a) / b;

						xa = ScaleX(xMin, m_Ctx.m_rMinX, rDeltaX);

						ya = y2 - 1;
						}
					else {
						double xMin = (m_Ctx.m_rMaxY - a) / b;

						xa = ScaleX(xMin, m_Ctx.m_rMinX, rDeltaX);

						ya = y1;
						}
					}

				if( yb < y1 || yb >= y2 ) {

					if( b > 0 ) {

						double xMax = (m_Ctx.m_rMaxY - a) / b;

						xb = ScaleX(xMax, m_Ctx.m_rMinX, rDeltaX);

						yb = y1;
						}
					else {
						double xMax = (m_Ctx.m_rMinY - a) / b;

						xb = ScaleX(xMax, m_Ctx.m_rMinX, rDeltaX);

						yb = y2 - 1;
						}
					}

				////////

				pGDI->SetPenFore(m_pBestLine->GetColor());

				pGDI->DrawLine( ToInt(xa), ToInt(ya),
						ToInt(xb), ToInt(yb)
						);
				}
			}
		}
	}

BOOL CPrimLineGraph::CheckCtx(void)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		FreeCtx(m_Ctx);

		m_Ctx = Ctx;		

		return FALSE;
		}

	FreeCtx(Ctx);

	return TRUE;
	}

void CPrimLineGraph::MakeCtx(CCtx &Ctx)
{
	Ctx.m_pValX = New C3REAL [ Ctx.m_uCount ];

	Ctx.m_pValY = New C3REAL [ Ctx.m_uCount ];
	}

void CPrimLineGraph::FreeCtx(CCtx &Ctx)
{
	delete [] Ctx.m_pValX;

	delete [] Ctx.m_pValY;
	}

// Attributes

BOOL CPrimLineGraph::IsAvail(void) const
{
	if( !IsItemAvail(m_pCount) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pValueX) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pValueY) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pMinX) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pMaxX) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pMinY) ) {
		
		return FALSE;
		}

	if( !IsItemAvail(m_pMaxY) ) {
		
		return FALSE;
		}

	return TRUE;
	}

C3REAL CPrimLineGraph::GetMinX(void)
{
	return m_Ctx.m_rMinX;
	}

C3REAL CPrimLineGraph::GetMaxX(void)
{
	return m_Ctx.m_rMaxX;
	}

C3REAL CPrimLineGraph::GetMinY(void)
{
	return m_Ctx.m_rMinY;
	}

C3REAL CPrimLineGraph::GetMaxY(void)
{
	return m_Ctx.m_rMaxY;
	}

// Initialization

void CPrimLineGraph::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLineGraph", pData);
	
	GetCoded(pData, m_pValueX);

	GetCoded(pData, m_pValueY);

	GetCoded(pData, m_pCount);

	GetCoded(pData, m_pMinX);

	GetCoded(pData, m_pMaxX);

	GetCoded(pData, m_pMinY);

	GetCoded(pData, m_pMaxY);

	m_Style = GetByte(pData);

	m_Fit   = GetByte(pData);

	m_pDataFill->Load(pData);

	m_pDataLine->Load(pData);

	m_pBestLine->Load(pData);
	
	m_Ctx.m_pValX = NULL;

	m_Ctx.m_pValY = NULL;
	}

// Context Creation

void CPrimLineGraph::FindCtx(CCtx &Ctx)
{
	if( m_pValueX && m_pValueY ) {

		if( (Ctx.m_fAvail = IsAvail()) ) {
			
			IDataServer *pData = CUISystem::m_pThis->GetDataServer();

			UINT  uCount = GetItemData(m_pCount, 0);
			
			C3REAL rMinX = m_pMinX ? I2R(m_pMinX->Execute(typeReal)) : I2R(0);

			C3REAL rMaxX = m_pMaxX ? I2R(m_pMaxX->Execute(typeReal)) : I2R(100);
			
			C3REAL rMinY = m_pMinY ? I2R(m_pMinY->Execute(typeReal)) : I2R(0);

			C3REAL rMaxY = m_pMaxY ? I2R(m_pMaxY->Execute(typeReal)) : I2R(100);
			
			Ctx.m_uCount = uCount;
			
			Ctx.m_rMinX  = rMinX;

			Ctx.m_rMaxX  = rMaxX;
			
			Ctx.m_rMinY  = rMinY;

			Ctx.m_rMaxY  = rMaxY;

			MakeCtx(Ctx);

			DWORD      x = m_pValueX->Execute(typeLValue);

			DWORD      y = m_pValueY->Execute(typeLValue);

			CDataRef  &X = (CDataRef &) x;

			CDataRef  &Y = (CDataRef &) y;
			
			for( UINT n = 0; n < uCount; n++ ) {

				C3REAL rValX = I2R(pData->GetData(x, typeReal, getNone));

				C3REAL rValY = I2R(pData->GetData(y, typeReal, getNone));

				MakeMax(rValX, rMinX);

				MakeMin(rValX, rMaxX);

				MakeMax(rValY, rMinY);

				MakeMin(rValY, rMaxY);

				Ctx.m_pValX[n] = rValX;

				Ctx.m_pValY[n] = rValY;

				X.x.m_Array++;

				Y.x.m_Array++;
				}

			return;
			}
		}

	Ctx.m_fAvail = FALSE;

	Ctx.m_uCount = 0;

	Ctx.m_pValX  = NULL;

	Ctx.m_pValY  = NULL;

	Ctx.m_rMinX = I2R(0);

	Ctx.m_rMaxX = I2R(100);
			
	Ctx.m_rMinY = I2R(0);

	Ctx.m_rMaxY = I2R(100);
	}

// Context Check

BOOL CPrimLineGraph::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail     == That.m_fAvail					  &&
	       m_uCount     == That.m_uCount					  &&
	       m_rMinX      == That.m_rMinX					  &&
	       m_rMaxX      == That.m_rMaxX					  &&
	       m_rMinY      == That.m_rMinY					  &&
	       m_rMaxY      == That.m_rMaxY			                  &&
	       memcmp(m_pValX, That.m_pValX, sizeof(C3REAL) * That.m_uCount) == 0 &&
	       memcmp(m_pValY, That.m_pValY, sizeof(C3REAL) * That.m_uCount) == 0 ;;
	}

// Implementation

int CPrimLineGraph::ToInt(double i)
{
	return (i<0) ? int(i-0.5) : int(i+0.5);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Scatter Graph
//

// Constructor

CPrimLegacyScatterGraph::CPrimLegacyScatterGraph(void)
{
	m_pLine       = NULL;
	
	m_uCount      = 0;

	m_GridType    = 0;

	m_pVertCol    = New CPrimColor(naBack);

	m_pHorzCol    = New CPrimColor(naBack);

	m_GridFont    = fontHei16;

	m_VertPrecise = 1;

	m_HorzPrecise = 1;
	
	memset(&m_Ctx, 0xFF, sizeof(m_Ctx));
	}

// Destructor

CPrimLegacyScatterGraph::~CPrimLegacyScatterGraph(void)
{
	for( UINT n = 0; n < m_uCount; n ++ ) {

		delete m_pLine[n];
		}

	delete [] m_pLine;

	delete m_pVertCol;

	delete m_pHorzCol;
	}

// Initialization

void CPrimLegacyScatterGraph::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLegacyScatterGraph", pData);

	CPrimLegacyFigure::Load(pData);

	if( (m_uCount = GetByte(pData) ) ) {

		m_pLine = New CPrimLineGraph * [ m_uCount ];
		
		for( UINT n = 0; n < m_uCount; n++ ) {
			
			m_pLine[n] = New CPrimLineGraph;

			m_pLine[n]->Load(pData);
			}
		}

	if( (m_GridType = GetByte(pData)) ) {

		if( m_GridType & 1 ) {

			m_pVertCol->Load(pData);

			m_GridFont    = GetWord(pData);

			m_VertPrecise = GetByte(pData);
			}

		if( m_GridType & 2 ) {

			m_pHorzCol->Load(pData);

			m_HorzGap = GetByte(pData);

			m_HorzPrecise = GetByte(pData);
			}
		}
	}

// Overridables

void CPrimLegacyScatterGraph::SetScan(UINT Code)
{
	for( UINT n = 0; n < m_uCount; n++ ) {
		
		m_pLine[n]->SetScan(Code);
		}

	m_pVertCol->SetScan(Code);

	m_pHorzCol->SetScan(Code);

	CPrimLegacyFigure::SetScan(Code);
	}

void CPrimLegacyScatterGraph::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimLegacyFigure::DrawPrep(pGDI, Erase, Trans);

	for( UINT n = 0; m_fShow && n < m_uCount; n++ ) {
		
		if( !m_pLine[n]->CheckCtx() ) {
			
			m_fChange = TRUE;
			}
		}

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			
			FindLayout(pGDI);
			}

		if( m_fChange ) {

			Erase.Append(m_DrawRect);
			}
		}
	}

void CPrimLegacyScatterGraph::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);

	DrawGrid(pGDI, Rect);
	
	for( UINT n = 0; n < m_uCount; n++ ) {
		
		m_pLine[n]->DrawItem(pGDI, Rect);
		}
	}

// Implementation

void CPrimLegacyScatterGraph::DrawGrid(IGDI *pGDI, R2 Rect)
{
	if( m_GridType ) {

		pGDI->ResetAll();

		if( m_GridType & 1 ) {

			pGDI->SetBrushFore(m_Ctx.m_VertCol);

			int StepSpan = m_VertHelper.GetStepSpan();

			int    Steps = max(1, StepSpan);

			for( int n = 0; n <= Steps; n++ ) {

				double Value = m_VertHelper.GetStepValue(n);

				int    yPos  = -1;

				if( !m_VertPrecise && StepSpan ) {

					yPos = m_y2 - (m_ySize * n) / StepSpan;
					}
				else {
					if( n == 0 ) {

						Value = m_Ctx.m_GridMinY;

						yPos  = m_y2;
						}

					if( n == Steps ) {

						Value = m_Ctx.m_GridMaxY;

						yPos  = m_y1;
						}

					if( yPos < 0 ) {

						double d = (m_Ctx.m_GridMaxY - m_Ctx.m_GridMinY);

						double f = (Value - m_Ctx.m_GridMinY) / d;

						yPos     = m_y2 - int(m_ySize * f);
						}
					}

				if( m_pEdge->m_Width ) {

					if( n == 0 ) {					
					
						continue;
						}

					if( n == Steps ) {
					
						continue;
						}
					}

				pGDI->FillRect(m_x1, yPos, m_x2, yPos + 1);
				}
			}

		if( m_GridType & 2 ) {

			pGDI->SetBrushFore(m_Ctx.m_HorzCol);

			int StepSpan = m_HorzHelper.GetStepSpan();

			int    Steps = max(1, StepSpan);

			for( int n = 0; n <= Steps; n++ ) {

				double Value = m_HorzHelper.GetStepValue(n);

				int    xPos  = -1;

				if( !m_HorzPrecise && StepSpan ) {

					xPos = m_x2 - (m_xSize * n) / StepSpan;
					}
				else {
					if( n == 0 ) {

						Value = m_Ctx.m_GridMinX;

						xPos  = m_x2;
						}

					if( n == Steps ) {

						Value = m_Ctx.m_GridMaxX;

						xPos  = m_x1;
						}

					if( xPos < 0 ) {

						double d = (m_Ctx.m_GridMaxX - m_Ctx.m_GridMinX);

						double f = (Value - m_Ctx.m_GridMinX) / d;

						xPos     = m_x2 - int(m_xSize * f);
						}
					}

				if( m_pEdge->m_Width ) {

					if( n == 0 ) {					
					
						continue;
						}

					if( n == Steps ) {
					
						continue;
						}
					}

				pGDI->FillRect(xPos, m_y1, xPos + 1, m_y2);
				}
			}
		}
	}

// Context Creation

void CPrimLegacyScatterGraph::FindCtx(CCtx &Ctx)
{
	Ctx.m_VertCol  = m_pVertCol->GetColor();

	Ctx.m_HorzCol  = m_pHorzCol->GetColor();

	if ( m_pLine ) {

		Ctx.m_GridMinX = m_pLine[0]->GetMinX();

		Ctx.m_GridMaxX = m_pLine[0]->GetMaxX();

		Ctx.m_GridMinY = m_pLine[0]->GetMinY();

		Ctx.m_GridMaxY = m_pLine[0]->GetMaxY();
		}
	else {
		Ctx.m_GridMinX = 0;

		Ctx.m_GridMaxX = 0;

		Ctx.m_GridMinY = 0;

		Ctx.m_GridMaxY = 0;
		}
	}

void CPrimLegacyScatterGraph::FindLayout(IGDI *pGDI)
{
	SelectFont(pGDI, m_GridFont);
	
	R2 Rect = m_DrawRect;

	if( m_pEdge->m_Width ) {

		DeflateRect(Rect, 2, 2);
		}

	m_x1    = Rect.x1;

	m_x2    = Rect.x2;

	m_y1    = Rect.y1;
	
	m_y2    = Rect.y2;

	m_fy    = pGDI->GetTextHeight(L"0");

	m_f    = m_HorzGap;

	m_ySize = m_y2 - m_y1;

	m_xSize = m_x2 - m_x1;

	if( m_GridType & 1 ) {

		double Limit = min(10, m_ySize / (m_fy + 4));

		m_VertHelper.SetLimits(m_Ctx.m_GridMinY, m_Ctx.m_GridMaxY);

		m_VertHelper.CalcDecimalStep(Limit);
		}

	if( m_GridType & 2 ) {
		
		double Limit = min(10, m_xSize / (m_f));

		m_HorzHelper.SetLimits(m_Ctx.m_GridMinX, m_Ctx.m_GridMaxX);

		m_HorzHelper.CalcDecimalStep(Limit);
		}
	}

// Context Check

BOOL CPrimLegacyScatterGraph::CCtx::operator == (CCtx const &That) const
{
	return m_VertCol  == That.m_VertCol  &&
	       m_HorzCol  == That.m_HorzCol  &&
	       m_GridMinX == That.m_GridMinX &&
	       m_GridMaxX == That.m_GridMaxX &&
	       m_GridMinY == That.m_GridMinY &&
	       m_GridMaxY == That.m_GridMaxY;
		;
	}

// End of File
