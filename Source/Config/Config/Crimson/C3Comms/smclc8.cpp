
#include "intern.hpp"

#include "smclc8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SMC LC8 Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSmcLc8SerialDriverOptions, CUIItem);
				   
// Constructor

CSmcLc8SerialDriverOptions::CSmcLc8SerialDriverOptions(void)
{
	m_Mask = 0;
	}

// Download Support

BOOL CSmcLc8SerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mask));
	
	return TRUE;
	}

// Meta Data Creation

void CSmcLc8SerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Mask);
	}


//////////////////////////////////////////////////////////////////////////
//
// SMC LC8 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSmcLc8SerialDeviceOptions, CUIItem);
				   
// Constructor

CSmcLc8SerialDeviceOptions::CSmcLc8SerialDeviceOptions(void)
{
	m_Axis = 1;
	}

// UI Managament

void CSmcLc8SerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{

	}

// Download Support

BOOL CSmcLc8SerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Axis));
	
	return TRUE;
	}

// Meta Data Creation

void CSmcLc8SerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Axis);
	}

//////////////////////////////////////////////////////////////////////////
//
// SMC LC8 Serial Driver
//

// Instantiator

ICommsDriver *	Create_SmcLc8Driver(void)
{
	return New CSmcLc8Driver;
	}

// Constructor

CSmcLc8Driver::CSmcLc8Driver(void)
{
	m_wID		= 0x404C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SMC";
			
	m_DriverName	= "LC8";
	
	m_Version	= "1.01";
	
	m_ShortName	= "LC8";
	
	m_DevRoot	= "AXIS";

	AddSpaces();
	}

// Binding Control

UINT CSmcLc8Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSmcLc8Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CSmcLc8Driver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSmcLc8SerialDriverOptions);
	}

CLASS CSmcLc8Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSmcLc8SerialDeviceOptions);
	}

// Implementation     

void CSmcLc8Driver::AddSpaces(void)
{
	AddSpace(New CSpace(addrNamed, "InReg", "Command I/O Input Register",   16, 0x3860, 0,	    addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "OutReg","Command I/O Output Register",	16, 0x3808, 0,	    addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "Alarms","Alarm Register",		16, 0x3811, 0,	    addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "Errors","Error Register",		16, 0x385B, 0,	    addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "EncRes","Encoder Resolution",		16, 0x3838, 0,	    addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "FdScrw","Feed Screw Lead (mm)",		16, 0x3845, 0,	    addrLongAsReal));
	AddSpace(New CSpace(addrNamed, "PosTim","Position Sample Time (s)",	16, 0x3836, 0,	    addrLongAsReal));
	AddSpace(New CSpace(addrNamed, "Motor",	"Motor Voltage and Power",	16, 0x3861, 0,	    addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "CurPos","Current Position (mm)",	16, 0x380B, 0,	    addrLongAsReal));
	AddSpace(New CSpace(addrNamed, "CurVel","Current Velocity (mm/s)",	16, 0x380D, 0,	    addrLongAsReal));
	AddSpace(New CSpace(addrNamed, "CurTor","Current Torque (%)",		16, 0x3814, 0,	    addrLongAsReal));
	
	AddSpace(New CSpace(addrNamed, "IOInp", "Enable/Disable IO Input Cmd",	16, 0xF009, 0,	    addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "RESET", "Assert a Reset",		16, 0xF001, 0,	    addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "HOME",  "Assert a Home",		16, 0xF002, 0,	    addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "STEP",  "Set Step",			16, 0xF003, 0,	    addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "START", "Assert a Step Start",		16, 0xF004, 0,	    addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "PAUSE", "Turn Pause On or Off",		16, 0xF005, 0,	    addrBitAsBit));
	AddSpace(New CSpace(addrNamed, "ESTOP", "Turn E-Stop On or Off",	16, 0xF006, 0,	    addrBitAsBit));
		
	AddSpace(New CSpace(addrNamed, "Steps", "Set Number of Steps",		16, 0xF007, 0,	    addrWordAsWord));
	AddSpace(New CSpace(	    3, "StpPos","Step Position (mm)",		10,	 1, 117,    addrLongAsReal));
	AddSpace(New CSpace(	    4, "StpVel","Step Velocity (mm/s)",		10,	 1, 117,    addrWordAsWord));
	AddSpace(New CSpace(	    5, "StpAcc","Step Acceleration (mm/s*s)",	10,	 1, 117,    addrWordAsWord));
	AddSpace(New CSpace(	    6, "StpDec","Step Deceleration( mm/s*s)",	10,	 1, 117,    addrWordAsWord));
	AddSpace(New CSpace(	    7, "StpTor","Step Torque (%)",		10,	 1, 117,    addrLongAsReal));
	AddSpace(New CSpace(	    8, "AbsRel","Step Abs/Rel Flag",		10,	 1, 117,    addrBitAsBit  ));
	
	AddSpace(New CSpace(addrNamed, "JogVel","Set Jog Velocity (mm/s)",	16, 0xF008, 0,	    addrLongAsReal));
		

	AddSpace(New CSpace(	    1, "RAM",	"RAM Memory Access",		16, 0x3800, 0x3925, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(	    2, "NV",	"Non-Volatile Memory Access",	16, 0x0000, 0x07FF, addrByteAsWord, addrByteAsReal));
	
	/*
	
	AddSpace(New CSpace(	    1, "POS_BLK ", "",				16,     0,  512, addrByteAsReal));
	AddSpace(New CSpace(	    2, "VEL_BLK ", "",				16,   512, 1536, addrByteAsWord));
	AddSpace(New CSpace(addrNamed, "0600",	"NV_I_GAIN_SCALE",		16, 0x600,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0602",	"NV_I_P_GAIN",			16, 0x602,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "0606",  "NV_POS_PI_GAINS",		16, 0x606,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "060A",	"NV_POS_AD",			16, 0x60A,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "060C",  "NV_POS_BD",			16, 0x60C,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "060E",	"NV_T_SAMPLE_POS",		16, 0x60E,    0, addrWordAsWord));
      
	AddSpace(New CSpace(addrNamed, "0610",	"NV_T_SAMPLE_I",		16, 0x610,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0612",	"NV_COUNTS_PER_REV",		16, 0x612,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0614",  "NV_MAX_STROKE",		16, 0x614,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0618",	"NV_POS_SOFT_LIMIT",		16, 0x618,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "061C",  "NV_HOME_OFFSET",		16, 0x61C,    0, addrLongAsLong));
	
	AddSpace(New CSpace(addrNamed, "0620",	"NV_TABLE_COUNT",		16, 0x620,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0622",	"NV_SLAVE_MASK",		16, 0x622,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0624",  "NV_Pos_PI_scale_power",	16, 0x624,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0626",	"NV_Pos_D_scale_power",		16, 0x626,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0628",  "NV_EncoderFactor",		16, 0x628,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "062C",  "NV_BALL_SCREW_PITCHL",		16, 0x62C,    0, addrLongAsLong));
	
	AddSpace(New CSpace(addrNamed, "0630",	"NV_PosMM",			16, 0x630,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "0634",	"NV_VelMM",			16, 0x634,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "0638",  "NV_AccelMM",			16, 0x638,    0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "063C",	"NV_PALLETAXIS",		16, 0x63C,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "063E",  "NV_OverloadResetThreshold",	16, 0x63E,    0, addrWordAsWord));
	
	AddSpace(New CSpace(addrNamed, "0640",	"NV_OverLoad_Limit",		16, 0x640,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0642",	"NV_OVERVOLT_TIME",		16, 0x642,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0644",  "NV_Home_Velocity",		16, 0x644,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0646",	"NV_Home_Acceleration",		16, 0x646,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0648",  "NV_Max_Velocity",		16, 0x648,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "064A",  "NV_Max_Acceleration",		16, 0x64A,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "064C",	"NV_InPosition_Range",		16, 0x64C,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "064E",  "NV_PwmBrake_Delay",		16, 0x64E,    0, addrWordAsWord));
	
	AddSpace(New CSpace(addrNamed, "0650",	"NV_Current_Limit",		16, 0x650,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0652",	"NV_TORQUE_M",			16, 0x652,    0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "0654",  "NV_TORQUE_B",			16, 0x654,    0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "07D8",  "NV_DSP_VERSION",		16, 0x7D8,    0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "07E0",  "NV_TEST_DATE",			16, 0x7E0,    0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "07E6",  "NV_SERIAL_NUMBER",		16, 0x7E6,    0, addrByteAsByte));
	AddSpace(New CSpace(addrNamed, "07F0",  "NV_PART_NUMBER",		16, 0x7F0,    0, addrByteAsByte));
	
	AddSpace(New CSpace(	    3, "PGM_BUF ", "",				16, 0x3800, 0x3805, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3806",	"PROGRAM_NUMBER",		16, 0x3806,	 0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3807",  "PLC_STATUS",			16, 0x3807,	 0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3808",	"DIG_OUT",			16, 0x3808,  	 0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3809",  "DIG_IN_X",			16, 0x3809,	 0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "380B",	"POSITION",			16, 0x308B,	 0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "380D",  "VELOCITY",			16, 0x308D,	 0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "380F",	"POS_TARGET",			16, 0x308F,	 0, addrLongAsLong));

	AddSpace(New CSpace(addrNamed, "3811",  "ALARM_REG",			16, 0x3811,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3813",	"DSP_STAT",			16, 0x3813,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3814",  "IQS",				16, 0x3814,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3815",	"VELCOMD",			16, 0x3815,      0, addrRealAsReal));
	AddSpace(New CSpace(addrNamed, "3817",  "ACCELCOMD",			16, 0x3817,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3819",	"DECELCOMD",			16, 0x3819,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "381B",  "TEST_REGISTER",		16, 0x381B,      0, addrWordAsWord));
	AddSpace(New CSpace(	    4, "VER_STR ", "",			16, 0x381C, 0x382E, addrWordAsWord));
      
	AddSpace(New CSpace(addrNamed, "382F",  "I_SCALEFACTOR",		16, 0x382F,      0, addrWordAsWord));
	
	AddSpace(New CSpace(addrNamed, "3830",	"IQ_PI_GAINS",			16, 0x3830,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3832",  "POS_PI_GAINS",			16, 0x3832,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3834",	"RAM_P_aD",			16, 0x3834,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3835",  "RAM_P_bD",			16, 0x3835,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3836",	"T_SAMPLE_POS",			16, 0x3836,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3837",  "T_SAMPLE_I",			16, 0x3837,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3838",	"COUNTS_PER_REV",		16, 0x3838,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3839",  "MAX_STROKE",			16, 0x3839,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "383B",	"SOFT_LIMIT_P",			16, 0x383B,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "383D",  "RAM_HOME_OFFSET",		16, 0x383D,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "383F",	"TABLE_COUNT",			16, 0x383F,      0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "3840",	"SLAVE_MASK",			16, 0x3840,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3841",  "RAM_Pos_PI_scale_power",	16, 0x3841,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3842",	"RAM_Pos_D_scale_power",	16, 0x3842,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3843",  "EncoderFactor",		16, 0x3843,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3845",	"ACTUATOR_PITCHL",		16, 0x3845,      0, addrRealAsReal));
	AddSpace(New CSpace(addrNamed, "3847",  "POS_MM",			16, 0x3847,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3849",	"VEL_MM",			16, 0x3849,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "384B",  "ACCEL_MM",			16, 0x384B,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "384D",	"PALLETAXIS",			16, 0x384D,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "384E",  "OverloadResetThreshold",	16, 0x384E,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "384F",	"OverLoad_Limit",		16, 0x384F,      0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "3850",	"OVERVOLT_TIME",		16, 0x3850,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3851",  "Home_Velocity",		16, 0x3851,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3852",	"Home_Acceleration",		16, 0x3852,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3853",  "Max_Velocity",			16, 0x3853,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3854",	"Max_Acceleration",		16, 0x3854,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3855",  "InPosition_Range",		16, 0x3855,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3856",	"PwmBrake_Delay",		16, 0x3856,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3857",  "Current_Limit",		16, 0x3857,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3858",	"Torque_Coeffs",		16, 0x3858,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "385A",  "na2",				16, 0x385A,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "385B",	"ERROR_REG",			16, 0x385B,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "385C",	"TYPE_ID",			16, 0x385C,      0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "385D",  "XT_FINAL",			16, 0x385D,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "385F",	"TORQUE_MAX",			16, 0x385F,      0, addrWordAsWord));

	AddSpace(New CSpace(addrNamed, "3860",	"DIG_IN",			16, 0x3860,      0, addrLongAsLong));
	AddSpace(New CSpace(addrNamed, "3862",  "SERVO_STAT",			16, 0x3862,      0, addrWordAsWord));	AddSpace(New CSpace(addrNamed, "3863",	"SYS_STAT",		10, 31,   0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed, "3864",  "TRACE_FREQ",			16, 0x3864,      0, addrWordAsWord));
	
	AddSpace(New CSpace(        5, "TRACE_1 ", "",				16, 0x3882,  0x38BF, addrWordAsWord));
	AddSpace(New CSpace(	    6, "TRACE_2 ", "",				16, 0x38C0,  0x3906, addrWordAsWord));
	AddSpace(New CSpace(	    7, "PalTbl ", "",				16, 0x3907,  0x3925, addrWordAsWord));
	*/
	}

// End of File
