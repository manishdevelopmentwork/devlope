
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MURATA_HPP
	
#define	INCLUDE_MURATA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Murata's Mitsubishi MELSEC 4 Serial Slave Driver
//

class CMurMelsec4SlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMurMelsec4SlaveDriver (void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		
	
	protected:
		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Murata's Mitsubishi MELSEC Driver Options
//

class CMurMelsec4SlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMurMelsec4SlaveDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
		UINT m_Melsec;
		UINT m_PC;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
