
#include "Intern.hpp"

#include "RawProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MacHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Protocol
//

// Creation Function

IProtocol * Create_Raw(void)
{
	return New CRaw;
}

// Constructor

CRaw::CRaw(void)
{
	StdSetRef();

	m_pLock   = Create_Rutex();

	m_pRouter = NULL;

	m_pUtils  = NULL;

	m_uMask   = 0;

	m_fEnable = FALSE;

	m_uSend   = 0;

	BindSockets();
}

// Destructor

CRaw::~CRaw(void)
{
	AfxRelease(m_pLock);
}

// Operations

void CRaw::Enable(void)
{
	m_fEnable = TRUE;
}

BOOL CRaw::Send(CRawSocket *pSock, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	if( pBuff->GetSize() >= 16 ) {

		CMacHeader *pHead = (CMacHeader *) pBuff->GetData();

		BOOL        fLoop = FALSE;

		if( pHead->m_Dest.IsGroup() ) {

			fLoop = TRUE;
		}
		else {
			INDEX Index = m_Seen.FindName(pHead->m_Dest);

			if( !m_Seen.Failed(Index) ) {

				UINT uFace = m_Seen.GetData(Index);

				if( pHead->m_Dest == m_Nics[uFace] ) {

					fLoop = TRUE;
				}
				else {
					if( !m_pRouter->Send(uFace, IP_EXPER, pBuff) ) {

						TcpDebug(OBJ_RAW, LEV_TRACE, "failed to send\n");

						return FALSE;
					}

					return TRUE;
				}
			}
		}

		if( fLoop ) {

			for( UINT n = 0; n < elements(m_Sock); n++ ) {

				CRawSocket &Sock = m_Sock[n];

				if( &Sock != pSock ) {

					if( Sock.OnTest(pBuff) ) {

						CBuffer *pCopy = pBuff->MakeCopy();

						if( pCopy ) {

							Sock.OnRecv(pCopy);
						}
					}
				}
			}
		}

		if( !m_pRouter->Send(IP_EXPER, IP_EXPER, IP_RAW, pBuff, this) ) {

			TcpDebug(OBJ_RAW, LEV_TRACE, "failed to send\n");

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

void CRaw::SendReq(BOOL fState)
{
	if( fState ) {

		if( !m_uSend++ ) {

			m_pRouter->SendReq(m_uMask,
					   TRUE
			);
		}
	}
	else {
		if( !--m_uSend ) {

			m_pRouter->SendReq(m_uMask,
					   FALSE
			);
		}
	}
}

void CRaw::EnableMulticast(IPREF Ip, BOOL fEnable)
{
	m_pRouter->EnableMulticast(Ip, fEnable);
}

void CRaw::FreeSocket(CRawSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) pSock->m_pRoot;

	AfxListRemove(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);
}

// IUnknown

HRESULT CRaw::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IProtocol);

	StdQueryInterface(IThreadNotify);

	StdQueryInterface(IProtocol);

	return E_NOINTERFACE;
}

ULONG CRaw::AddRef(void)
{
	StdAddRef();
}

ULONG CRaw::Release(void)
{
	StdRelease();
}

// IThreadNotify

UINT CRaw::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	CListRoot *pRoot = New CListRoot;

	pRoot->m_pHead = NULL;

	pRoot->m_pTail = NULL;

	return UINT(pRoot);
}

void CRaw::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam)
{
	CListRoot *pRoot = (CListRoot *) uParam;

	if( pRoot->m_pHead ) {

		AfxTrace("tcpip: thread %u exiting with open raw sockets\n", pThread->GetIdent());

		while( pRoot->m_pHead ) {

			CRawSocket *pSock = pRoot->m_pHead;

			pSock->Release();

			AfxAssert(pRoot->m_pHead != pSock);
		}
	}

	delete pRoot;
}

// IProtocol

BOOL CRaw::Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask)
{
	m_pRouter = pRouter;

	m_pUtils  = pUtils;

	m_uMask   = uMask;

	FindLocalMacs();

	return TRUE;
}

BOOL CRaw::CreateSocket(ISocket * &pSocket)
{
	CAutoLock Lock(m_pLock);

	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CRawSocket &Sock = m_Sock[n];

		if( Sock.IsFree() ) {

			Sock.Create();

			AddSocket(&Sock);

			pSocket = &Sock;

			return TRUE;
		}
	}

	pSocket = NULL;

	return FALSE;
}

BOOL CRaw::Recv(UINT uFace, IPREF Face, PSREF Ps, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	if( m_fEnable ) {

		if( StoreMacs(uFace, pBuff) ) {

			DWORD dwMask = 0;

			for( UINT n = 0; n < elements(m_Sock); n++ ) {

				CRawSocket &Sock = m_Sock[n];

				DWORD      dwBit = (1 << n);

				if( Sock.OnTest(pBuff) ) {

					dwMask |= dwBit;
				}
			}

			if( dwMask ) {

				for( UINT n = 0; n < elements(m_Sock); n++ ) {

					CRawSocket &Sock = m_Sock[n];

					DWORD      dwBit = (1 << n);

					if( dwMask & dwBit ) {

						if( (dwMask ^= dwBit) ) {

							CBuffer *pCopy = pBuff->MakeCopy();

							if( pCopy ) {

								Sock.OnRecv(pCopy);
							}
						}
						else {
							Sock.OnRecv(Buff.TakeOver());

							break;
						}
					}
				}
			}
		}
	}

	return TRUE;
}

BOOL CRaw::Send(void)
{
	BOOL fFail = FALSE;

	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CRawSocket &Sock = m_Sock[n];

		if( !Sock.OnSend() ) {

			fFail = TRUE;
		}
	}

	return fFail ? FALSE : TRUE;
}

BOOL CRaw::Poll(void)
{
	return TRUE;
}

BOOL CRaw::NetStat(IDiagOutput *pOut)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CRawSocket &Sock = m_Sock[n];

		Sock.NetStat(pOut);
	}

	return TRUE;
}

BOOL CRaw::SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	return TRUE;
}

// Implementation

void CRaw::BindSockets(void)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CRawSocket &Sock = m_Sock[n];

		Sock.Bind(this);
	}
}

BOOL CRaw::StoreMacs(UINT uFace, CBuffer *pBuff)
{
	if( pBuff->GetSize() >= 16 ) {

		CMacHeader *pHead = (CMacHeader *) pBuff->GetData();

		if( pHead->m_Src.IsGroup() ) {

			INDEX Index = m_Seen.FindName(pHead->m_Src);

			if( m_Seen.Failed(Index) ) {

				m_Seen.Insert(pHead->m_Src, uFace);
			}
			else
				m_Seen.SetData(Index, uFace);
		}

		return TRUE;
	}

	return FALSE;
}

void CRaw::FindLocalMacs(void)
{
	for( UINT uFace = 0;; uFace++ ) {

		CIpAddr  Addr, Mask;

		CMacAddr Mac;

		if( m_pRouter->GetConfig(uFace, Addr, Mask, Mac) ) {

			if( !Mac.IsEmpty() ) {

				m_Seen.Insert(Mac, uFace);
			}

			m_Nics.Append(Mac);

			continue;
		}

		break;
	}
}

void CRaw::AddSocket(CRawSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) AddThreadNotify(this);

	AfxListAppend(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);

	pSock->m_pRoot = pRoot;
}

// End of File
