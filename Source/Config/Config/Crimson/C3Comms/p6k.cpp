
#include "intern.hpp"

#include "p6k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Series
//

// Dynamic Class

AfxImplementDynamicClass(CParker6KDeviceOptions, CUIItem);
				   
// Constructor

CParker6KDeviceOptions::CParker6KDeviceOptions(void)
{
	m_Drop		= 0;

	m_InitTime	= 2000;

	m_Device	= 0;
	}

// UI Managament

void CParker6KDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL CParker6KDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddWord(WORD(m_InitTime));

	Init.AddByte(BYTE(m_Device));

	return TRUE;
	}

// Meta Data Creation

void CParker6KDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	
	Meta_AddInteger(InitTime);

	Meta_AddInteger(Device);
	}


//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Serial Master Driver
//

// Instantiator

ICommsDriver *	Create_Parker6KSerialDriver(void)
{
	return New CParker6KSerialDriver;
	}

// Constructor

CParker6KSerialDriver::CParker6KSerialDriver(void)
{
	m_wID		= 0x3342;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Parker";
			
	m_DriverName	= "Compumotor 6K Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "6K Master";	

	AddSpaces();

	}

// Binding Control

UINT CParker6KSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CParker6KSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CParker6KSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CParker6KDeviceOptions);
	}

// Implementation     

void CParker6KSerialDriver::AddSpaces(void)
{	
	AddSpace(New CSpaceParker6K(		1,	"A",		"Acceleration",						1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		2,	"AA",		"Average Acceleration",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		3,	"AD",		"Deceleration",						1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		4, 	"ADA",		"Average Deceleration",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	//AddSpace(New CSpaceParker6K(		5,	"ADDR",		"Multiple Unit Auto-Address",				0,   99,  addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		7,	"CMDDIR",	"Commanded Direction Polarity",				1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(	addrNamed,	"COMEXC",	"Continuous Command Processing Mode",			8,   0,   addrBitAsBit,   addrBitAsBit));
	AddSpace(New CSpaceParker6K(		9,	"COMEXL",	"Continue Execution on Limit",  			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(	addrNamed,	"COMEXR",	"Continue Motion on Pause/Continue Input",		10,   0,   addrBitAsBit,   addrBitAsBit));
	AddSpace(New CSpaceParker6K(	addrNamed,	"COMEXS",	"Continue Execution on Stop", 				11,   0,   addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceParker6K(		12,	"D",		"Distance",						1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		13,	"DACLIM",	"DAC Limit",						1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		23,	"DRES",		"Drive Resolution",					1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		24,	"DRFEN",	"Enable/Disable Checking Drive Input Fault",		1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		25,	"DRFLVL",	"Drive Fault Level",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		26,	"DRIVE",	"Drive Enable",						1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		31,	"EFAIL",	"Encoder Failure Detect",				1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		32,	"ENCCNT",	"Encoder Count Reference Enable",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		33,	"ENCPOL",	"Encoder Polarity",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		34,	"ENCSEND",	"Encoder Step and Direction Mode",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		39,	"ERES",		"Encoder Resolution",					1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		44,	"ESDB",		"Stall Backlash Deadband",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		45,	"ESK",		"Kill On Stall",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		46,	"ESTALL",	"Enable Stall Detect",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		47,	"FFILT",	"Following Filter",					1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		48,	"FGADV",	"Following Geared Advance",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		49,	"FMAXA",	"Follower Axis Maximum Acceleration",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		50,	"FMAXV",	"Follower Axis Maximum Velocity",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		51,	"FMCLEN",	"Master Cycle Length",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		52,	"FMCNEW",	"Restart Master Cycle Counting",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		53,	"FMCP",		"Initial Master Cycle Position",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		54,	"FOLEN",	"Follower Mode Enable",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		56,	"FOLMD",	"Master Distance",					1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		57,	"FOLRD",	"Denominator of Follower-To-Master Ratio",		1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		58,	"FOLRN",	"Numerator of Follower-To-Master Ratio",		1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		59,	"FPPEN",	"Master Position Prediction Enable",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		60,	"FSHFC",	"Continuous Shift",					1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		61,	"FSHFD",	"Preset Shift",						1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		62,	"FVMACC",	"Virtual Master Count Acceleration",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		63,	"FVMFRQ",	"Virtual Master Count Frequency",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		64,	"GO",		"Initiate Motion",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		65,	"GOL",		"Initiate Linear Interpolated Motion",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(	addrNamed,	"HALT",		"Terminate Program Execution",				66,   0,   addrBitAsBit,	  addrBitAsBit));
	AddSpace(New CSpaceParker6K(		67,	"HOM",		"Go Home",						1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		68,	"HOMA",		"Home Acceleration",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		69,	"HOMAA",	"Homing Average Acceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		70,	"HOMAD",	"Home Deceleration",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		71,	"HOMADA",	"Homing Average Decleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		72,	"HOMBAC",	"Home Backup Enabale",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		73,	"HOMDF",	"Home Final Direction",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		74,	"HOMDG",	"Home Reference Edge",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		75,	"HOMV",		"Home Velocity",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		76,	"HOMVF",	"Home Final Velocity",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		77,	"HOMZ",		"Home to Encoder Z-channel Enable",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(	addrNamed,	"INDUSE",	"Enable/Disable User Status",				79,   0,   addrBitAsBit,	  addrBitAsBit));
	AddSpace(New CSpaceParker6K(		84,	"JOG",		"Jog Mode Enable",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		85,	"JOGA",		"Jog Acceleration",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		86,	"JOGAA",	"Jogging Average Acceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		87,	"JOGAD",	"Jog Deceleration",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		88,	"JOGADA",	"Jog Average Deceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		89,	"JOGVH",	"Jog Velocity High",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		90,	"JOGVL",	"Jog Velocity Low",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		91,	"JOY",		"Joystick Mode Enable",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		92,	"JOYA",		"Joystick Acceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		93,	"JOYAA",	"Joystick Average Acceleration",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		94,	"JOYAD",	"Joystick Deceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		95,	"JOYADA",	"Joystick Average Deceleration",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		100,	"JOYVH",	"Joystick Velocity Select Input High",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		101,	"JOYVL",	"Joystick Velocity Select Input Low",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		104,	"LH",		"Hardware End-of-Travel Limit",				1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		105,	"LHAD",		"Hard Limit Deceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		106,	"LHADA",	"Hard Limit Average Deceleration",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		109,	"LS",		"Soft Limit Enable",					1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		110,	"LSAD",		"Soft Limit Deceleration",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		111,	"LSADA",	"Soft Limit Average Deceleration",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		112,	"LSNEG",	"Soft Limit Negative Travel Range",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		113,	"LSPOS",	"Soft Limit Positive Travel Range",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		114,	"MA",		"Absolute/Incremental Mode Enable",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		115,	"MC",		"Preset/Continuous Mode Enable",			1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(	addrNamed,	"MEPOL",	"Master Encoder Polarity",				116,   0,   addrBitAsBit,	  addrBitAsBit));
	AddSpace(New CSpaceParker6K(	addrNamed,	"MESND",	"Master Encoder Step and Direction Mode",		117,   0,   addrBitAsBit,	  addrBitAsBit));
	AddSpace(New CSpaceParker6K(		127,	"OUT",		"Output State",						1,  32,   addrBitAsBit,   addrBitAsLong,  P6K_OUTPUT));
	AddSpace(New CSpaceParker6K(	addrNamed,	"PA",		"Path Acceleration",					130,   0,   addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpaceParker6K(	addrNamed,	"PAA",		"Path Average Acceleration",				131,   0,   addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpaceParker6K(	addrNamed,	"PAD",		"Path Deceleration",					132,   0,   addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpaceParker6K(	addrNamed,	"PADA",		"Path Average Deceleration",				133,   0,   addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpaceParker6K(	addrNamed,	"PV",		"Path Velocity",					153,   0,   addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpaceParker6K(		155,	"RE",		"Registration Enable",					1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		157,	"REGLOD",	"Registration Lock-Out Distance",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		158,	"REGSS",	"Registration Single-Shot",				1,   8,   addrBitAsBit,   addrBitAsByte,  P6K_AXIS));
 	AddSpace(New CSpaceParker6K(		159,	"S",		"Stop Motion",						1,   8,   addrBitAsBit,	  addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(	addrNamed,	"SCALE",	"Enable/Disable Scale Factors",				160,   0,   addrBitAsBit,	  addrBitAsBit));
	AddSpace(New CSpaceParker6K(		161,	"SCLA",		"Acceleration Scale Factor",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		162,	"SCLD",		"Distance Scale Factor",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		163,	"SCLMAS",	"Master Scale Factor",					1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		164,	"SCLV",		"Velocity Scale Factor",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		165,	"SFB",		"Select Servo Feedback Source",				1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		166,	"SGAF",		"Acceleration Feedforward Gain",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		167,	"SGI",		"Integral Feedback Gain",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		168,	"SGILIM",	"Integral Windup Limit",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		169,	"SGP",		"Proportional Feedback Gain",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		170,	"SGV",		"Velocity Feedback Gain",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		171,	"SGVF",		"Velocity Feedforward Gain",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		172,	"SINAMP",	"Virtual Master Sine Wave Amplitude",			1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		173,	"SINANG",	"Virtual Master Sine Wave Angle",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		174,	"SINGO",	"Virtual Master - Initiate Internal Sine Wave",		1,   8,   addrBitAsBit,	  addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		175,	"SMPER",	"Maximum Allowable Position Error",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		176,	"SOFFS",	"Servo Control Signal Offset",				1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		177,	"STRGTD",	"Target Distance Zone",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		178,	"STRGTE",	"Enable Target Zone Settling Mode",			1,   8,   addrBitAsBit,	  addrBitAsByte,  P6K_AXIS));
	AddSpace(New CSpaceParker6K(		179,	"STRGTT",	"Target Settling Timeout Period",			1,   8,   addrWordAsWord, addrWordAsWord, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		180,	"STRGTV",	"Target Velocity Zone",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		183,	"TAS",		"Transfer Axis Status",					1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		184,	"TASX",		"Transfer Extended Axis Status",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		185,	"TDAC",		"Transfer DAC Voltage",					1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		187,	"TFB",		"Transfer Selected Feedback Device Position",		1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		188,	"TFS",		"Transfer Following Status",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TIN",		"Transfer Onboard Input Status",			189,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TINO",		"Transfer Other Input Status",				190,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(		191,	"TLIM",		"Transfer Limits",					1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TOUT",		"Transfer Output Status",				192,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(		193,	"TPC",		"Transfer Position Commanded",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TPCME",	"Transfer Captured Master Encoder Position",		194,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(		195,	"TPE",		"Transfer Position of Encoder",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		196,	"TPER",		"Transfer Position Error",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		197,	"TPM",		"Transfer Position of Motor",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		198,	"TPMAS",	"Transfer Current Master Cycle Position",		1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TPME",		"Transfer Position of Master Encoder",			199,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(		200,	"TPSHF",	"Transfer Net Position Shift",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		201,	"TPSLV",	"Transfer Position of Follower Axis",			1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TRGLOT",	"Trigger Interrupt Lockout Time",			202,   0,   addrRealAsReal, addrRealAsReal));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TSS", 		"Transfer System Status",				203,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(		204,	"TSTLT",	"Transfer Settling Time",				1,   8,   addrLongAsLong, addrLongAsLong, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TTASK",	"Transfer Task Number",					206,   0,   addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TTIM",		"Transfer Timer",					207,   0,   addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceParker6K(	addrNamed,	"TUS",		"Transfer User Status",					208,   0,   addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceParker6K(		209,	"TVEL",		"Transfer Current Commanded Velocity",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		210,	"TVELA",	"Transfer Current Actual Velocity",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		211,	"TVMAS",	"Transfer Current Master Velocity",			1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS | P6K_RO));
	AddSpace(New CSpaceParker6K(		212,	"V",		"Velocity",						1,   8,   addrRealAsReal, addrRealAsReal, P6K_AXIS));
	AddSpace(New CSpaceParker6K(		213,	"VAR",		"Numeric Variable Assignment",				1, 225,   addrRealAsReal, addrRealAsReal, P6K_VAR));
	AddSpace(New CSpaceParker6K(		214,	"VARB",		"Binary Variable Assignment",				1, 125,   addrLongAsLong, addrLongAsLong, P6K_VAR));
	AddSpace(New CSpaceParker6K(	addrNamed,	"VARCLR",	"Variable Clear",					215,   0,   addrBitAsBit,	  addrBitAsBit));
	AddSpace(New CSpaceParker6K(		216,	"VARI",		"Integer Variable Assigment",				1, 225,   addrLongAsLong, addrLongAsLong, P6K_VAR));
	AddSpace(New CSpaceParker6K(		217,	"VARS",		"String Variable Assignment",				1,  25,	  addrLongAsLong, addrLongAsLong, P6K_VAR));
	AddSpace(New CSpaceParker6K(		218,	"RUNARG",	"Program Name to be Executed on RUN",			0,   0,   addrByteAsByte, addrByteAsByte));
	AddSpace(New CSpaceParker6K(	addrNamed,	"RUN",		"Begin Executing Program (RUNARG)",			219,   0,   addrBitAsBit,	  addrBitAsBit));
	}

// Address Management

BOOL CParker6KSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CParker6KDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CParker6KSerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT uExtra = 0;

	UINT uPos = Text.Find('-');

	if ( uPos < NOTHING ) {

		if( Text.GetAt(uPos + 1) == 'G' ) {

			Text.Delete(uPos, 2);

			uExtra = 1;
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		Addr.a.m_Extra = uExtra;

		if( IsString(Addr) ) {

			if( Addr.a.m_Offset < 64 ) {

				Addr.a.m_Offset = (Addr.a.m_Offset * 13) + 64;
				}
			}

		return TRUE;
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CParker6KSerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	if( IsString(Address) ) {

		if( Address.a.m_Offset >= 64 ) {

			Address.a.m_Offset = (Address.a.m_Offset - 64) / 13;
			}
		}

	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Address) ) {

		if( Address.a.m_Table != addrNamed ) {

			if( Address.a.m_Extra ) {

				Text += "-G";
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CParker6KSerialDriver::IsString(CAddress const &Addr)
{
	return Addr.a.m_Table == 217;
	}

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Master TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CParker6KTCPDeviceOptions, CUIItem);

// Constructor

CParker6KTCPDeviceOptions::CParker6KTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));;

	m_Socket = 5002;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;

	m_TimeWD = 5;

	m_TickWD = 1;

	m_Device = 0;
	}

// UI Managament

void CParker6KTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}


// Download Support	     

BOOL CParker6KTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	Init.AddByte(BYTE(m_TimeWD));
	Init.AddByte(BYTE(m_TickWD));
	Init.AddByte(BYTE(m_Device));

	return TRUE;
	}

// Meta Data Creation

void CParker6KTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(TimeWD);
	Meta_AddInteger(TickWD);
	Meta_AddInteger(Device);
	}

//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Master TCP/IP Driver
//

// Instantiator

ICommsDriver *	Create_CParker6KTCPDriver(void)
{
	return New CParker6KTCPDriver;
	}

// Constructor

CParker6KTCPDriver::CParker6KTCPDriver(void)
{
	m_wID		= 0x3508;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Parker";
	
	m_DriverName	= "Compumotor 6K TCP/IP Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "6K TCP/IP Master";

	}

// Binding Control

UINT	CParker6KTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void	CParker6KTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}


// Configuration

CLASS   CParker6KTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}


CLASS	CParker6KTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CParker6KTCPDeviceOptions);
	}




//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CParker6KDialog, CStdAddrDialog);
		
// Constructor

CParker6KDialog::CParker6KDialog(CParker6KSerialDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "ParkerElementDlg";

	m_pConfig = pConfig;
	}

// Overridables

void CParker6KDialog::SetAddressText(CString Text)
{
	CEditCtrl &Prefix = (CEditCtrl &) GetDlgItem(2001);
	
	CButton   &Check = (CButton   &) GetDlgItem(2005);

	Prefix.SetWindowSize(50,12, FALSE);

	CSpaceParker6K *pSpace = (CSpaceParker6K *) m_pSpace;

	if( Text.IsEmpty() ) {

		SetGlobalWrite(FALSE);

		Check.EnableWindow(FALSE);

		}

	else {

		if( Text.Right(2) == "-G" ) {

			Text = Text.Left(Text.GetLength() - 2);

			SetGlobalWrite(TRUE);
			}

		else {

			SetGlobalWrite(FALSE);
			}

		CSpaceParker6K *pSpace = (CSpaceParker6K *) m_pSpace;

		BOOL fEnable = FALSE;

		if ( pSpace ) {

			if ( pSpace->m_Info == P6K_AXIS ) {

				fEnable = TRUE;
				}
			}

		Check.EnableWindow(fEnable);
		}
	
	CStdAddrDialog::SetAddressText(Text);

	if( pSpace && pSpace->m_Info == P6K_AXIS ) {

		UINT uDev = m_pConfig->GetDataAccess("Device")->ReadInteger(m_pConfig);

		CEditCtrl &Axis = (CEditCtrl &) GetDlgItem(2002);

		Axis.EnableWindow(uDev == 0);
		}
	}

CString CParker6KDialog::GetAddressText(void)
{
	CString Text = CStdAddrDialog::GetAddressText();

	if( IsGlobalWrite() ) {

		Text += "-G";
		}

	return Text;
	}

BOOL	CParker6KDialog::IsGlobalWrite(void)
{
	CButton &Button = (CButton &) GetDlgItem(2005);

	return Button.IsChecked();
	}

void   CParker6KDialog::SetGlobalWrite(BOOL fCheck)
{
	CButton &Button = (CButton &) GetDlgItem(2005);

	Button.SetCheck(fCheck);
	}
	
	
//////////////////////////////////////////////////////////////////////////
//
// Parker 6K Space Wrapper Class
//

// Constructors

CSpaceParker6K::CSpaceParker6K(UINT tag, CString p, CString c, UINT n, UINT x, AddrType t, AddrType s, UINT i) : CSpace(tag, p, c, 10, n, x, t, s)
{
	m_Info		= i;

	}



// End of File
