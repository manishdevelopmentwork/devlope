
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixServerContext_HPP

#define INCLUDE_MatrixServerContext_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MatrixContext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Server Context
//

class CMatrixServerContext : public CMatrixContext, public ITlsServerContext
{
	public:
		// Constructor
		CMatrixServerContext(CMatrixSsl *pSsl);

		// Destructor
		~CMatrixServerContext(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ITlsServerContext
		BOOL	  LoadTrustedRoots(PCBYTE pRoot, UINT uRoot);
		BOOL      LoadServerCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass);
		ISocket * CreateSocket(ISocket *pSock);
	};

// End of File

#endif
