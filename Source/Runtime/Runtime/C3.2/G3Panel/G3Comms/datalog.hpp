
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DATALOG_HPP
	
#define	INCLUDE_DATALOG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "events.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDataLogger;
class CDataLogInfo;
class CDataLogList;
class CDataLog;
class CDataLogCache;

//////////////////////////////////////////////////////////////////////////
//
// Data Logger
//

class CDataLogger : public CItem, public ITaskEntry
{
	public:
		// Constructor
		CDataLogger(void);

		// Destructor
		~CDataLogger(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Attributes
		BOOL  IsActive(void) const;
		PCTXT GetBatch(UINT uSlot) const;
		UINT  GetEachSize(void) const;

		// Operations
		void NewBatch(UINT uSlot, PCTXT pName);
		void LogEvent(CEventInfo const &Info);
		BOOL BatchComment(UINT uSlot, PCTXT pText, BOOL fHead);
		BOOL LogComment(UINT uLog, PCTXT pText, BOOL fHead);
		void ForceSave(void);
		void WaitSave(void);

		// Data Members
		UINT           m_Handle;
		UINT           m_EnableBatch;
		UINT	       m_EnableSets;
		UINT           m_BatchCount;
		UINT	       m_BatchDrive;
		UINT           m_CSVEncode;
		CDataLogList * m_pLogs;
		CDataLogInfo * m_pInfo;

		// Instance Pointer
		static CDataLogger * m_pThis;

	protected:
		// Data Members
		IEvent		 * m_pInitSave;
		IEvent		 * m_pInitScan;
		IEvent		 * m_pSave;
		IEvent		 * m_pDone;
		CEventLogger     * m_pEvents;
		CSecurityManager * m_pSecure;
		BOOL		   m_fChange;
		CString		   m_Batch[8];
		DWORD		   m_Start[8];
		UINT		   m_uPool;

		// Scan Task
		void ScanInit(void);
		void ScanExec(void);
		void ScanTerm(void);

		// Poll Task
		void PollInit(void);
		void PollExec(void);
		void PollTerm(void);

		// Save Task
		void SaveInit(void);
		void SaveExec(void);
		void SaveTerm(void);

		// Implementation
		DWORD GetLogTime(void);
		void  LoadBatches(void);
		void  LoadBatch(UINT uSlot);
		void  SaveBatch(UINT uSlot);
		void  FindPoolSize(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Log Info (Honeywell Specific)
//

class CDataLogInfo : public CCodedHost
{
	public:
		// Constructor
		CDataLogInfo(void);

		// Destructor
		~CDataLogInfo(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		CCodedText * m_pData[40];
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

class CDataLogList : public CItem
{
	public:
		// Constructor
		CDataLogList(void);

		// Destructor
		~CDataLogList(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attibutes
		BOOL HasPolled(void) const;
		BOOL IsActive(void) const;
		UINT GetHCF(void) const;

		// Operations
		void PreRegister(void);
		void LogCheck(void);
		void LogLoad(void);
		void LogData(DWORD dwTime);
		void LogPoll(DWORD dwTime);
		void LogSave(void);
		void LogNewBatch(UINT uSlot, DWORD Time, PCTXT pName);
		void LogEvent(CEventInfo const &Info);
		
		// Data Members
		UINT        m_uCount;
		UINT        m_uUsed;
		CDataLog ** m_ppLog;

	protected:
		// Implementation
		UINT HCF(UINT a, UINT b) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw File Entry
//

struct CRaw
{
	BOOL	  m_fValid;
	CFilename m_Name;
	UINT      m_uPitch;
	UINT      m_uCount;
	DWORD     m_dwPos1;
	DWORD     m_dwPos2;
	DWORD	  m_dwTime1;
	DWORD	  m_dwTime2;
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Log
//

class CDataLog : public CCodedHost
{
	public:
		// Constructor
		CDataLog(UINT uPos);

		// Destructor
		~CDataLog(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsPolled(void) const;
		BOOL IsActive(void) const;
		BOOL IsEnabled(void) const;
		BOOL LogTime(DWORD dwTime) const;

		// Operations
		void PreRegister(void);
		void LogCheck(void);
		void LogLoad(void);
		void LogData(DWORD dwTime);
		void LogPoll(DWORD dwTime);
		void LogSave(void);
		void LogNewBatch(UINT uSlot, DWORD Time, PCTXT pName);
		BOOL LogEvent(CEventInfo const &Info);
		void LockData(void);
		void FreeData(void);
		
		// Display Range
		BOOL LoadDisplayData(CDataLogCache *pCache, DWORD t1, DWORD t2);
		BOOL LoadDisplayData(CDataLogCache *pCache, DWORD t1, DWORD t2, UINT np);

		// Data Access
		DWORD  GetTickTime(void);
		UINT   GetChanCount(void);
		UINT   GetChanType(UINT uChan);
		CTag * GetChanItem(UINT uChan);
		DWORD  GetChanMin(UINT uChan);
		DWORD  GetChanMax(UINT uChan);

		// Log Access
		BOOL   IsEmpty(void);
		DWORD  GetLastTime(void);
		BOOL   HasCoverage(DWORD dwTime);
		DWORD  GetInitTime(void);

		// Data Members
		CString		m_Name;
		CString		m_Path;
		UINT            m_Type;
		UINT		m_Update;
		UINT		m_FileLimit;
		UINT		m_FileCount;
		UINT		m_WithBatch;
		UINT            m_SignLogs;
		UINT		m_Comments;
		UINT		m_Merge;
		UINT		m_Drive;
		CCodedItem    * m_pEnable;
		CCodedItem    * m_pTrigger;
		UINT		m_uTags;
		DWORD	      * m_pTags;
		BYTE	      * m_pType;
		CTree <WORD>    m_MergeTags;
		CTree <WORD>    m_MergeSigs;

	protected:
		// Log Comment Entry
		struct CComment
		{
			CEventInfo m_Info;
			CComment * m_pNext;
			CComment * m_pPrev;
			};

		// Data Members
		UINT         m_uPos;
		CTagList   * m_pSrc;
		IMutex     * m_pLock;
		CLogHelper * m_pHelp;
		CDataRef     m_Null;
		BOOL         m_fSaveRaw;
		DWORD      * m_pData;
		DWORD      * m_pTime;
		DWORD	     m_dwLast;
		BOOL	     m_fValid;
		BOOL	     m_fWrap;
		BOOL	     m_fLost;
		BOOL	     m_fText;
		UINT	     m_uBuff;
		UINT	     m_uHead;
		UINT	     m_uTail;
		UINT	     m_uSave;
		UINT	     m_uRaw;
		CRaw       * m_pRaw;
		CComment   * m_pHead;
		CComment   * m_pTail;
		BOOL	     m_fPoll;
		char	     m_cSep;

		// Implementation
		BOOL LoadTagList(PCBYTE &pData);
		void LoadMergeList(PCBYTE &pData);
		BOOL LoadMergeTag(UINT uTag);
		BOOL CheckPath(void);
		BOOL ConfigHelper(void);
		void AllocBuffer(void);
		BOOL FreeStrings(void);
		BOOL HasInterest(CEventInfo const &Info);

		// Buffer Access
		UINT   GetInitSlot(void);
		UINT   GetLastSlot(void);
		BOOL   GetNextSlot(UINT &uSlot);
		DWORD  GetSlotTime(UINT uSlot);
		DWORD  GetSlotData(UINT uSlot, UINT uChan);
		PDWORD GetSlotData(UINT uSlot);

		// Raw File List
		void MakeRawList(void);
		BOOL ReadRawList(void);
		void LoadRawData(CDataLogCache *pCache, DWORD t1, DWORD t2, UINT np);
		void CopyRawData(void);
		BOOL TestRawHead(FILE *pFile);
		void FreeRawList(void);

		// File Output
		BOOL WriteCsvHeader(CFastFile &File);
		BOOL WriteRawHeader(CFastFile &File);
		void WriteTime(CFastFile &csv, CFastFile &raw);
		void WriteData(CFastFile &csv, CFastFile &raw, BOOL fFree);
		void WriteComment(CFastFile &csv, CEventInfo const &Info);

		// CSV Encoding
		CString Encode(CUnicode Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Log Cache
//

class CDataLogCache
{
	public:
		// Constructor
		CDataLogCache(UINT uCount);

		// Destructor
		~CDataLogCache(void);

		// Management
		BOOL Alloc(UINT uAlloc);
		void Clean(void);

		// Operations
		BOOL Append(DWORD dwTime, PDWORD pData);
		void LoadRawData(CRaw const &Raw, DWORD t1, DWORD t2);

		// Data Access
		BOOL   IsEmpty(void);
		UINT   GetInitSlot(void);
		UINT   GetLastSlot(void);
		BOOL   GetNextSlot(UINT &uSlot);
		DWORD  GetSlotTime(UINT uSlot);
		DWORD  GetSlotData(UINT uSlot, UINT uChan);
		PDWORD GetSlotData(UINT uSlot);

	protected:
		// Data
		UINT         m_uCount;
		UINT	     m_uBuff;
		UINT	     m_uHead;
		UINT	     m_uTail;
		DWORD      * m_pData;
		DWORD      * m_pTime;
		UINT         m_uAlloc;
		DWORD	     m_dwLast;
		BOOL	     m_fValid;
	
		// Implementation
		UINT AdjustSize(UINT uAlloc);

		// Development
		PCTXT FormatTime(DWORD dwTime);	

		// Friends
		friend class CDataLog;
	};

// End of File

#endif
