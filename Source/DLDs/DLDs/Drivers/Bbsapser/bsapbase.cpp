
#include "intern.hpp"

#include "bsapbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Base Driver
//

// Constructor

CBBBSAPBaseDriver::CBBBSAPBaseDriver(void)
{
	
	}

// Destructor

CBBBSAPBaseDriver::~CBBBSAPBaseDriver(void)
{
	}

// Entry Points

CCODE MCALL CBBBSAPBaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nT = %d, O = %d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( !HasTags(pData, uCount) ) return uCount;

	if( m_pBase->m_fUDP && (m_pRx[UDPFLG] & 0x30) ) {

		m_pRx[UDPFLG] = 0;

		return CCODE_ERROR | CCODE_BUSY; // Low memory in RTU, skip one send
		}

	StartFrame(FALSE, FALSE);

	if( AddReadByName(Addr.a.m_Table, Addr.a.m_Offset) ) {

		BOOL fOk = FALSE;

		switch( Addr.a.m_Table ) {

			case SPBIT:	fOk = DoBitRead (Addr, pData); break;

			case SPBYTE:	fOk = DoByteRead(Addr, pData); break;

			case SPWORD:	fOk = DoWordRead(Addr, pData); break;

			case SPLONG:	fOk = DoLongRead(Addr, pData); break;

			case SPREAL:	fOk = DoRealRead(Addr, pData); break;
			}

		if( !fOk ) return CCODE_ERROR;

		if( !m_pRx[UDPPCT] ) { // RTU start up fix

//**/			AfxTrace0("\r\nNO DATA\r\n");

			return CCODE_ERROR | CCODE_NO_DATA;
			}
		}

	return 1; // Good receive, or name isn't configured
	}

CCODE MCALL CBBBSAPBaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n**** WRITE T = %d, O = %d ", Addr.a.m_Table, Addr.a.m_Offset);

	if( !HasTags(pData, uCount) ) return uCount;

	if( m_pBase->m_fUDP && (m_pRx[UDPFLG] & 0x20) ) {

		m_pRx[UDPFLG] = 0;

		return CCODE_ERROR | CCODE_BUSY; // Low memory in RTU, skip one send
		}

	StartFrame(FALSE, TRUE);

	if( AddWriteByName(Addr.a.m_Table, Addr.a.m_Offset) ) {

		BOOL fWResp = FALSE;

		switch( Addr.a.m_Table ) {

			case SPBIT:
				if( *pData > 15 ) return 1;

				fWResp = DoBitWrite(pData);
				break;

			case SPBYTE:
				fWResp = DoByteWrite(pData);
				break;

			case SPWORD:
				fWResp = DoWordWrite(pData);
				break;

			case SPLONG:
				fWResp = DoLongWrite(pData);
				break;

			case SPREAL:
				fWResp = DoRealWrite(pData);
				break;

			default:
				return CCODE_ERROR | CCODE_HARD;
		}

		m_uWriteError++;

		if( fWResp || (m_uWriteError > 2) ) {

			m_uWriteError = 0;

			return 1;
			}

		return CCODE_ERROR;
		}

	return 1; // Name doesn't exist
	}

// Frame Building

// Virtuals
void CBBBSAPBaseDriver::StartFrame(BOOL fFunc, BOOL fIsWrite)
{
	return;
	}

void CBBBSAPBaseDriver::PutStart(BOOL fFunc)
{
	return;
	}

void CBBBSAPBaseDriver::AddByte(BYTE bData)
{
	if( (!m_pBase->m_fUDP) && bData == DLE ) {

		m_pTx[m_uPtr++] = DLE;
		}

	m_pTx[m_uPtr++] = bData;
	}

void CBBBSAPBaseDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBBBSAPBaseDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBBBSAPBaseDriver::AddReal(DWORD dwData)
{
	AddLong(dwData);
	}

void CBBBSAPBaseDriver::AddText(PCTXT pText)
{
//**/	AfxTrace0("\r\nName: ");

	for( UINT i = 0; i < strlen(pText); i++ ) {

//**/		AfxTrace1("%c", pText[i]);

		AddByte(pText[i]);
		}
	}

BOOL CBBBSAPBaseDriver::AddVarName(UINT uTable, UINT uOffset)
{
	PWORD pAI = NULL;
	PWORD pAL = NULL;

	UINT  uSz = m_pBase->m_uCnt[uTable - 1];

	switch( uTable ) {

		case SPBIT:
			pAI = m_pBase->m_pwfAddrIndex;
			pAL = m_pBase->m_pwfAddrList;
			break;

		case SPBYTE:
			pAI = m_pBase->m_pwbAddrIndex;
			pAL = m_pBase->m_pwbAddrList;
			break;

		case SPWORD:
			pAI = m_pBase->m_pwwAddrIndex;
			pAL = m_pBase->m_pwwAddrList;
			break;

		case SPLONG:
			pAI = m_pBase->m_pwdAddrIndex;
			pAL = m_pBase->m_pwdAddrList;
			break;

		case SPREAL:
			pAI = m_pBase->m_pwrAddrIndex;
			pAL = m_pBase->m_pwrAddrList;
			break;

		default:
			return FALSE;
		}

	if( !pAI || !pAL || !uSz ) {

		return FALSE;
		}

	UINT uPos = FindNamePos(pAI, pAL, uSz, uOffset);

	if( uPos < 0xFFFF ) {

		AddText((PCTXT)(&m_pBase->m_pbNameList[uPos]));

		AddByte(0);

		return TRUE;
		}

	return FALSE;
	}

UINT CBBBSAPBaseDriver::FindNamePos(PWORD pAddrIndex, PWORD pAddrOffset, UINT uListSize, UINT uOffset)
{
	for( UINT i = 0; i < uListSize; i++ ) {

		if( uOffset == pAddrOffset[i] ) {

			return m_pBase->m_pwNameIndex[pAddrIndex[i]];
			}
		}

	return 0xFFFF;
	}

// Read Handlers

BOOL CBBBSAPBaseDriver::DoBitRead(AREF Addr, PDWORD pData)
{
	if( UINT uPos = Send() ) {

		*pData = m_pRx[uPos];

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPBaseDriver::DoByteRead(AREF Addr, PDWORD pData)
{
	if( UINT uPos = Send() ) {

		*pData = m_pRx[uPos];

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPBaseDriver::DoWordRead(AREF Addr, PDWORD pData)
{
	if( UINT uPos  = Send() ) {

		PU2 p  = (PU2)&m_pRx[uPos];

		*pData = IntelToHost(*p);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPBaseDriver::DoLongRead(AREF Addr, PDWORD pData)
{
	if( UINT uPos  = Send() ) {

		PU4 p  = (PU4)&m_pRx[uPos];

		*pData = IntelToHost(*p);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBBBSAPBaseDriver::DoRealRead(AREF Addr, PDWORD pData)
{
	return DoLongRead(Addr, pData);
	}

BOOL CBBBSAPBaseDriver::AddReadByName(UINT uTable, UINT uOffset)
{
	AddReadByNameHeader();

	if( AddVarName(uTable, uOffset) ) {

		if( m_pBase->m_fUDP ) {

			WORD uSize = m_uPtr - m_pTx[0];

			m_pTx[m_pTx[0]]     = LOBYTE(uSize);
			m_pTx[m_pTx[0] + 1] = HIBYTE(uSize);
			}

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPBaseDriver::AddReadByNameHeader(void)
{
	AddByte(OFCRDBYN);	// Read Signal by Name
	AddByte(0x01);	// FSS quantity
	AddByte(FS1VALUE);	// FSS Value
	AddByte(0xFF);	// Security level
	AddByte(0x01);	// 1 name
	}

// Write Handlers

BOOL CBBBSAPBaseDriver::DoBitWrite(PDWORD pData)
{
	AddWriteData(addrBitAsBit, *pData);

	return m_pBase->m_fUDP ? Transact() : Send();
	}

BOOL CBBBSAPBaseDriver::DoByteWrite(PDWORD pData)
{
	AddWriteData(addrByteAsByte, *pData);

	return m_pBase->m_fUDP ? Transact() : Send();
	}

BOOL CBBBSAPBaseDriver::DoWordWrite(PDWORD pData)
{
	AddWriteData(addrWordAsWord, *pData);

	return m_pBase->m_fUDP ? Transact() : Send();
	}

BOOL CBBBSAPBaseDriver::DoLongWrite(PDWORD pData)
{
	AddWriteData(addrLongAsLong, *pData);

	return m_pBase->m_fUDP ? Transact() : Send();
	}

BOOL CBBBSAPBaseDriver::DoRealWrite(PDWORD pData)
{
	return DoLongWrite(pData);
	}

BOOL CBBBSAPBaseDriver::AddWriteByName(UINT uTable, UINT uOffset)
{
	AddByte(OFCWRBYN);	// write by name opcode
	AddByte(0xFF);		// Security level
	AddByte(1);		// 1 element

	return AddVarName(uTable, uOffset);
	}

void CBBBSAPBaseDriver::AddWriteData(UINT uType, DWORD dData)
{
	switch(uType) {

		case addrBitAsBit:

			AddByte(dData ? 9 : 10);
			break;

		case addrByteAsByte:

			AddByte(LOBYTE(dData));
			break;

		case addrWordAsWord:

			AddByte(WFDANALG);
			AddWord(LOWORD(dData));
			break;

		case addrLongAsLong:
		case addrRealAsReal:

			AddByte(WFDANALG);
			AddLong(dData);
			break;
		}

	if( m_pBase->m_fUDP ) {

		WORD uSize = m_uPtr - m_pTx[0];

		m_pTx[m_pTx[0]]     = LOBYTE(uSize);
		m_pTx[m_pTx[0] + 1] = HIBYTE(uSize);
		}
	}

// Transport Layer
UINT CBBBSAPBaseDriver::Send(void)
{
	return 0;
	}

BOOL CBBBSAPBaseDriver::Transact(void)
{
	return FALSE;
	}

BOOL CBBBSAPBaseDriver::RecvFrame(void)
{
	return FALSE;
	}

// Tags

BOOL CBBBSAPBaseDriver::LoadTags(LPCBYTE &pData)
{
	if( GetWord(pData) == 0xABCD && StoreDevName(pData) ) {

		if( UINT uCount = GetWord(pData) ) {

			m_pBase->m_fHasTags  = TRUE;

			UINT   uAddrSize = sizeof(DWORD) * uCount;

			PDWORD pAddrList = PDWORD(alloca(uAddrSize));

			memset(pAddrList, 0, uAddrSize);

			GetTypeCounts(pData, pAddrList, uCount);

			StoreAddrIndices(pAddrList, uCount);

			StoreNames(pData, uCount);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBBBSAPBaseDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		m_pBase->m_pbDevName = new BYTE[uCount];

		for( UINT i = 0; i < uCount; i++ ) {

			m_pBase->m_pbDevName[i] = GetByte(pData);
			}

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPBaseDriver::GetTypeCounts(LPCBYTE &pData, PDWORD pAddrList, UINT uCount)
{
	memset(m_pBase->m_uCnt, 0, sizeof(m_pBase->m_uCnt));

	for( UINT i = 0; i < uCount; i++ ) {

		DWORD d		= GetLong(pData);

		pAddrList[i]	= d;

		CAddress Addr	= (CAddress &)d;

		m_pBase->m_uCnt[Addr.a.m_Table - 1] += 1;
		}
	}

void CBBBSAPBaseDriver::StoreAddrIndices(PDWORD pAddrList, UINT uCount)
{
	UINT uf = m_pBase->m_uCnt[0];
	UINT ub = m_pBase->m_uCnt[1];
	UINT uw = m_pBase->m_uCnt[2];
	UINT ud = m_pBase->m_uCnt[3];
	UINT ur = m_pBase->m_uCnt[4];

	PWORD pif = NULL;
	PWORD paf = NULL;
	PWORD pib = NULL;
	PWORD pab = NULL;
	PWORD piw = NULL;
	PWORD paw = NULL;
	PWORD pid = NULL;
	PWORD pad = NULL;
	PWORD pir = NULL;
	PWORD par = NULL;

	UINT s   = sizeof(WORD);

	if( uf ) {
		m_pBase->m_pwfAddrIndex	= new WORD[uf];
		m_pBase->m_pwfAddrList	= new WORD[uf];
		pif			= m_pBase->m_pwfAddrIndex;
		paf			= m_pBase->m_pwfAddrList;

		memset(pif, 0, uf * s);
		memset(paf, 0, uf * s);
		}

	if( ub ) {
		m_pBase->m_pwbAddrIndex	= new WORD[ub];
		m_pBase->m_pwbAddrList	= new WORD[ub];
		pib			= m_pBase->m_pwbAddrIndex;
		pab			= m_pBase->m_pwbAddrList;

		memset(pib, 0, ub * s);
		memset(pab, 0, ub * s);
		}

	if( uw ) {
		m_pBase->m_pwwAddrIndex	= new WORD[uw];
		m_pBase->m_pwwAddrList	= new WORD[uw];
		piw			= m_pBase->m_pwwAddrIndex;
		paw			= m_pBase->m_pwwAddrList;

		memset(piw, 0, uw * s);
		memset(paw, 0, uw * s);
		}

	if( ud ) {
		m_pBase->m_pwdAddrIndex	= new WORD[ud];
		m_pBase->m_pwdAddrList	= new WORD[ud];
		pid			= m_pBase->m_pwdAddrIndex;
		pad			= m_pBase->m_pwdAddrList;

		memset(pid, 0, ud * s);
		memset(pad, 0, ud * s);
		}

	if( ur ) {
		m_pBase->m_pwrAddrIndex	= new WORD[ur];
		m_pBase->m_pwrAddrList	= new WORD[ur];
		pir			= m_pBase->m_pwrAddrIndex;
		par			= m_pBase->m_pwrAddrList;

		memset(pir, 0, ur * s);
		memset(par, 0, ur * s);
		}

	uf = 0;
	ub = 0;
	uw = 0;
	ud = 0;
	ur = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		DWORD d		= pAddrList[i];

		CAddress Addr	= (CAddress &)d;

		switch( Addr.a.m_Table ) {

			case SPBIT:
				paf[uf] = Addr.a.m_Offset;
				pif[uf++] = i;
				break;

			case SPBYTE:
				pab[ub] = Addr.a.m_Offset;
				pib[ub++] = i;
				break;

			case SPWORD:
				paw[uw] = Addr.a.m_Offset;
				piw[uw++] = i;
				break;

			case SPLONG:
				pad[ud] = Addr.a.m_Offset;
				pid[ud++] = i;
				break;

			case SPREAL:
				par[ur] = Addr.a.m_Offset;
				pir[ur++] = i;
				break;
			}
		}
	}

void CBBBSAPBaseDriver::StoreNames(LPCBYTE &pData, UINT uCount)
{
	UINT uTotalSize		= GetWord(pData);

//**/	AfxTrace2("\r\nStore Names Total Size = %d Ct = %d", uTotalSize, uCount);

	m_pBase->m_pbNameList	= new BYTE[uTotalSize];
	m_pBase->m_pwNameIndex	= new WORD[uCount];

	PBYTE pL		= m_pBase->m_pbNameList;
	PWORD pI		= m_pBase->m_pwNameIndex;

	UINT uPos		= 0;

	memset(pI, 0, uCount * sizeof(WORD));

	for( UINT i = 0; i < uCount; i++ ) {

		UINT uByteCount	= GetWord(pData);

		pI[i]		= uPos;

		for( UINT k = 0; k < uByteCount; k++ ) {

			pL[k + uPos] = GetByte(pData);
			}

		uPos += uByteCount;
		}
	}

// Helpers
BOOL CBBBSAPBaseDriver::CheckHeader(void)
{
	return FALSE;
	}

BOOL CBBBSAPBaseDriver::HasTags(PDWORD pData, UINT uCount)
{
	return FALSE;
	}

WORD CBBBSAPBaseDriver::GetRcvWord(UINT uPos)
{
	WORD x = *((PU2)&m_pRx[uPos]);

	return IntelToHost((WORD)x);
	}

DWORD CBBBSAPBaseDriver::GetRcvDWord(UINT uPos)
{
	DWORD x = *((PU4)&m_pRx[uPos]);

	return IntelToHost((DWORD)x);
	}

// End of File
