
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Validater
//

DLLAPI BOOL C3CheckSource(PCBYTE pSource)
{
	if( pSource[0] == BYTE(0x80 + tokenWasComment) ) {

		return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Source Expander
//

static BOOL StripUsing(CString &Name, CStringArray const &Using)
{
	UINT uCount = Using.GetCount();

	UINT uTotal = Name.GetLength();

	for( UINT n = 0; n < uCount; n++ ) {

		UINT uSize = Using[n].GetLength();

		if( uSize < uTotal ) {

			if( Name[uSize] == '.' ) {

				if( !wstrnicmp(Name, Using[n], uSize) ) {

					Name = Name.Mid(uSize + 1);

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

static void SkipLiteral(PCBYTE &pSource)
{
	while( *pSource ) {

		if( *pSource == srcUpper ) {

			pSource += 2;

			continue;
			}

		if( *pSource == srcUnicode ) {

			pSource += 3;

			continue;
			}

		pSource += 1;
		}

	pSource += 1;
	}

static CString ReadLiteral(PCBYTE &pSource)
{
	CString Text;

	BYTE    b;

	while( (b = *pSource++) ) {

		if( b == srcUpper ) {

			Text += TCHAR(*pSource++ | 0x80);

			continue;
			}

		if( b == srcUnicode ) {

			BYTE lo = *pSource++;

			BYTE hi = *pSource++;

			Text   += TCHAR(MAKEWORD(lo, hi));

			continue;
			}

		Text += TCHAR(b);
		}

	return Text;
	}

DLLAPI BOOL C3ExpandSource( PCBYTE	         pSource,
			    INameServer        * pName,
			    CStringArray const & Using,
			    CString	       & Text
			    )
{
	Text.Expand(8192);

	BYTE b;

	while( b = *pSource++ ) {

		if( b <= srcLiteral ) {

			Text += TCHAR(b);

			continue;
			}

		if( b == srcUpper ) {

			Text += TCHAR(*pSource++ | 0x80);

			continue;
			}

		if( b == srcUnicode ) {

			BYTE lo = *pSource++;

			BYTE hi = *pSource++;

			Text += TCHAR(MAKEWORD(lo, hi));

			continue;
			}

		if( b <= srcToken ) {

			Text += afxLexTabs->Expand(b & 0x7F);

			continue;
			}

		if( b == srcFunction ) {

			CString Name;

 			WORD	ID = ((WORD &) *pSource);

			pSource += sizeof(ID);
		
			if( pName && pName->NameFunction(ID, Name) ) {

				SkipLiteral(pSource);

				Text += Name;
				}
			else
				Text += ReadLiteral(pSource);

			continue;
			}

		if( b == srcIdent ) {

			CString Name;

			WORD	ID = ((WORD &) *pSource);

			pSource += sizeof(ID);
		
			if( pName && pName->NameIdent(ID, Name) ) {

				SkipLiteral(pSource);

				StripUsing(Name, Using);

				Text += Name;
				}
			else
				Text += ReadLiteral(pSource);

			continue;
			}

		if( b == srcDirect ) {

			CString Name;

			DWORD	ID = ((DWORD &) *pSource);

			pSource += sizeof(ID);
		
			if( pName && pName->NameDirect(ID, Name) ) {

				SkipLiteral(pSource);

				Text += Name;
				}
			else
				Text += ReadLiteral(pSource);

			continue;
			}

		if( b == srcProp1 || b == srcProp2 ) {

			CString Name, Prop;

			DWORD	Both   = ((DWORD &) *pSource);

			WORD    ID     = LOWORD(Both);

			WORD    PropID = HIWORD(Both);

			pSource += sizeof(Both);

			if( pName ) {

				if( pName->NameIdent(ID, Name) ) {

					if( pName->NameProp(ID, PropID, Prop) ) {

						SkipLiteral(pSource);

						if( b == srcProp1 ) {

							StripUsing(Name, Using);

							Text += Name;

							Text += L'.';
							}

						Text += Prop;
						}
					else
						Text += ReadLiteral(pSource);
					}
				else
					Text += ReadLiteral(pSource);
				}
			else
				Text += ReadLiteral(pSource);

			continue;
			}
		}

	Text.Compress();

	return TRUE;
	}

// End of File
