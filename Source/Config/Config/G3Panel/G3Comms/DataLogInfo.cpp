
#include "Intern.hpp"

#include "DataLogInfo.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Log Info (Honeywell Specific)
//

// Dynamic Class

AfxImplementDynamicClass(CDataLogInfo, CCodedHost);

// Constructor

CDataLogInfo::CDataLogInfo(void)
{
	for( UINT n = 0; n < 8; n++ ) {

		m_pEnable[n] = NULL;
		m_pName  [n] = NULL;
		m_pReady [n] = NULL;
		m_pRetain[n] = NULL;
		m_pField [n] = NULL;
		}
	}

// Type Access

BOOL CDataLogInfo::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.StartsWith(L"Enable") ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"Name") ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"Ready") ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	if( Tag.StartsWith(L"Retain") ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"Field") ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CDataLogInfo::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	for( UINT n = 0; n < 8; n++ ) {

		Init.AddItem(itemVirtual, m_pEnable[n]);
		Init.AddItem(itemVirtual, m_pName  [n]);
		Init.AddItem(itemVirtual, m_pReady [n]);
		Init.AddItem(itemVirtual, m_pRetain[n]);
		Init.AddItem(itemVirtual, m_pField [n]);
		}

	return TRUE;
	}

// Meta Data Creation

void CDataLogInfo::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	for( UINT n = 0; n < 8; n++ ) {

		Meta_Add(CPrintf(L"Enable%u", n), m_pEnable[n], metaVirtual);
		Meta_Add(CPrintf(L"Name%u",   n), m_pName  [n], metaVirtual);
		Meta_Add(CPrintf(L"Ready%u",  n), m_pReady [n], metaVirtual);
		Meta_Add(CPrintf(L"Retain%u", n), m_pRetain[n], metaVirtual);
		Meta_Add(CPrintf(L"Field%u",  n), m_pField [n], metaVirtual);
		}

	Meta_SetName((IDS_LOG_INFO));
	}

// End of File
