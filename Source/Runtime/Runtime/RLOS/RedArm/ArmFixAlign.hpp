
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Arm Hardware Drivers
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ArmFixAlign_HPP
	
#define	INCLUDE_ArmFixAlign_HPP

//////////////////////////////////////////////////////////////////////////
//
// 32-bit LDR-STR Opcode
//

struct OP32
{
	union 
	{
		struct
		{
			DWORD Offset:12;
			DWORD Rd:4;
			DWORD Rn:4;
			DWORD L:1;
			DWORD W:1;
			DWORD B:1;
			DWORD U:1;
			DWORD P:1;
			DWORD Mode:1;
			DWORD Type:2;
			DWORD Cond:4;
		
			} x;

		DWORD d;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// 16-bit LDR-STR Opcode
//

struct OP16
{
	union 
	{
		struct
		{
			DWORD OffsetL:4;
			DWORD SubType0:1;
			DWORD H:1;
			DWORD S:1;
			DWORD SubType3:1;
			DWORD OffsetH:4;
			DWORD Rd:4;
			DWORD Rn:4;
			DWORD L:1;
			DWORD W:1;
			DWORD Mode:1;
			DWORD U:1;
			DWORD P:1;
			DWORD Type:3;
			DWORD Cond:4;
		
			} x;

		DWORD d;
		};
	};

// End of File

#endif
