
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KOYO_HPP
#define	INCLUDE_KOYO_HPP

//////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo - Device Options
//

class CKoyoDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKoyoDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_PingEnable;
		UINT m_PingReg;
		
	protected:

		UINT m_Backup;

		// Meta Data Creation
		void AddMetaData(void);

		// Helpers
		BOOL IsOctal(UINT uReg);
		UINT OctalToDec(UINT uReg);

	};

//////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo - Direct Net	Driver Options
//

class CKoyoDirectNetDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKoyoDirectNetDriverOptions(void);
	
		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_MasterID;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo - Direct Net
//

class CKoyoDirectNetDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKoyoDirectNetDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);


		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL CheckAlignment(CSpace *pSpace);


	protected:
		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PLC Direct Koyo - K Sequence
//

class CKoyoKSequenceDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKoyoKSequenceDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

	       	// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL CheckAlignment(CSpace *pSpace);


	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master Udp/IP Master
//

class CKoyoMasterUdpDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKoyoMasterUdpDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL CheckAlignment(CSpace *pSpace);

		// Configuration
		CLASS  GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);

	};
 
//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master Udp/IP Device Options
//

class CKoyoMasterUdpDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKoyoMasterUdpDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
			
	protected:

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Koyo PLC Master w/ Ping UDP/IP Device Options
//

class CKoyoPingMasterUdpDeviceOptions : public CKoyoMasterUdpDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKoyoPingMasterUdpDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT m_PingEnable;
		UINT m_PingReg;
			
	protected:

		UINT m_Backup;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);

		// Helpers
		BOOL IsOctal(UINT uReg);
		UINT OctalToDec(UINT uReg);

	};


//////////////////////////////////////////////////////////////////////////
//
// Koyo EBC Master UDP/IP Master
//

class CKoyoEbcMasterUdpDriver : public CKoyoMasterUdpDriver
{
	public:
		// Constructor
		CKoyoEbcMasterUdpDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);


	protected:
		// Implementation
		void AddSpaces(void);


	};

///////////////////////////////////////////////////////////////////////////
//
// Koyo EBC Dialog
//

class CKoyoEbcDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CKoyoEbcDialog(CKoyoEbcMasterUdpDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
       	};

//////////////////////////////////////////////////////////////////////////
//
// Koyo Ebc Space Wrapper Class
//

class CSpaceEbc : public CSpace
{
	public:
		// Constructors
		CSpaceEbc(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT sn, UINT sx);

		// Public Data
		UINT m_SlotMin;
		UINT m_SlotMax;

		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);

		
	};

        
// End of File

#endif
