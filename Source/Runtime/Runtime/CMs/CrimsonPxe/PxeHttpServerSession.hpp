
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PxeHttpServerSession_HPP

#define	INCLUDE_PxeHttpServerSession_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "WebConsole.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PXE Web Server Session
//

class CPxeHttpServerSession : public CHttpServerSession
{
public:
	// Constructor
	CPxeHttpServerSession(CHttpServer *pServer);

	// Destructor
	~CPxeHttpServerSession(void);

	// Attributes
	UINT GetAccess(void) const;
	BOOL IsLocalUser(void) const;

	// Operations
	void Throttle(UINT uLimit);
	void SetUserInfo(CUserInfo const &Info);
	void ClearUserInfo(void);
	BOOL ReadPipe(CString &Data, PCTXT pName, CHttpServerRequest *pReq);
	BOOL SlowPoll(PCTXT pName, CString const &Data, CHttpServerRequest *pReq);

	// Diagnostics
	UINT GetDiagColCount(void);
	void GetDiagCols(IDiagOutput *pOut);
	void GetDiagInfo(IDiagOutput *pOut);

	// Data Members
	DWORD       m_dwLast;
	CWebConsole m_Console;
	CUserInfo   m_Info;

protected:
	// Pipe Status
	struct CPipe
	{
		CString	m_name;
		CString m_tab;
		time_t  m_last;
		int     m_fd;
	};

	// Poll Data
	struct CPoll
	{
		CString m_name;
		time_t  m_last;
		CString m_data;
	};

	// Data Members
	CMap<CString, CPipe *> m_Pipes;
	CMap<CString, CPoll *> m_Polls;

	// Overridables
	void OnTickle(void);
	void FreeData(void);

	// Implementation
	void DeletePipe(CPipe *p, INDEX i);
	void DeletePoll(CPoll *p, INDEX i);
};

// End of File

#endif
