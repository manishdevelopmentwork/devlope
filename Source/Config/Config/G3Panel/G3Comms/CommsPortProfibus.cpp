
#include "Intern.hpp"

#include "CommsPortProfibus.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Profibus Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortProfibus, CCommsPort);

// Constructor

CCommsPortProfibus::CCommsPortProfibus(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindProfibus;
	}

// Overridables

CHAR CCommsPortProfibus::GetPortTag(void) const
{
	return 'P';
}

// Download Support

BOOL CCommsPortProfibus::MakeInitData(CInitData &Init)
{
	Init.AddByte(5);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// End of File
