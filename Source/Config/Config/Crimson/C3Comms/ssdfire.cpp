
#include "intern.hpp"

#include "ssdfire.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SSD Drives via FireWire
//

// Instantiator

ICommsDriver *	Create_SSDFireDriver(void)
{
	return New CSSDFireDriver;
	}

// Constructor

CSSDFireDriver::CSSDFireDriver(void)
{
	m_wID		= 0x4020;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SSD Drives";
	
	m_DriverName	= "890 via FireWire";
	
	m_Version	= "1.00";
	
	m_ShortName	= "FireWire 890";

	m_DevRoot	= "DRIVE";

	AddSpaces();
	}

// Binding Control

UINT CSSDFireDriver::GetBinding(void)
{
	return bindFireWire;
	}

void CSSDFireDriver::GetBindInfo(CBindInfo &Info)
{
	}

// Implementation

void CSSDFireDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "R", "Registers", 10, 1,	999, addrLongAsLong, addrLongAsReal));
	}

// End of File
