
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LANG_HPP
	
#define	INCLUDE_LANG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Language Codes
//

#define	C3L_SYSTEM		0x0000
#define	C3L_GENERIC		0x0001
#define	C3L_ENGLISH		0x0100
#define	C3L_ENGLISH_US		0x0101
#define	C3L_ENGLISH_UK		0x0102
#define	C3L_ENGLISH_IN		0x0103
#define	C3L_FRENCH		0x0200
#define	C3L_FRENCH_FR		0x0201
#define	C3L_FRENCH_CA		0x0202
#define	C3L_FRENCH_BE		0x0203
#define	C3L_FRENCH_CH		0x0204
#define	C3L_SPANISH		0x0300
#define	C3L_SPANISH_ES		0x0301
#define	C3L_SPANISH_MX		0x0302
#define	C3L_GERMAN		0x0400
#define	C3L_GERMAN_DE		0x0401
#define	C3L_GERMAN_AT		0x0402
#define	C3L_GERMAN_CH		0x0403
#define	C3L_ITALIAN		0x0500
#define	C3L_ITALIAN_IT		0x0501
#define	C3L_ITALIAN_CH		0x0502
#define	C3L_FINNISH		0x0600
#define	C3L_SIMP_CHINESE	0x8101
#define	C3L_TRAD_CHINESE	0x8201
#define	C3L_RUSSIAN		0x8301
#define	C3L_KOREAN_JAMO		0x8401
#define	C3L_KOREAN_FULL		0x8402
#define	C3L_JAPANESE		0x8500

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CLangManager;

//////////////////////////////////////////////////////////////////////////
//
// Language Manager
//

class CLangManager : public CItem
{
	public:
		// Constructor
		CLangManager(void);

		// Destructor
		~CLangManager(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		void RegisterUITask(void);
		void SetCurrentSlot(UINT uSlot);

		// Current Language
		UINT     GetCurrentSlot(void) const;
		UINT     GetEffectiveCode(void) const;
		UINT     GetNumericCode(void) const;
		WCHAR    GetDecPointChar(void) const;
		WCHAR    GetNumGroupChar(void) const;
		BOOL	 IsGroupBoundary(UINT p) const;
		CUnicode GetMonthName(UINT m) const;
		UINT	 GetTimeFormat(void) const;
		UINT     GetDateFormat(void) const;
		WCHAR    GetDateSepChar(void) const;
		WCHAR	 GetListSepChar(void) const;

		// Data Members
		UINT  m_System;
		UINT  m_Global;
		UINT  m_LocSep;
		UINT  m_uCount;
		PUINT m_pLang;
		PBOOL m_pNum;

	protected:
		// Data Members
		HTASK  m_hTask;
		UINT   m_uSlot;
		UINT   m_uDefLang;
		UINT   m_uDefNum;
		UINT   m_uLang;
		UINT   m_uNum;

		// Implementation
		BOOL LoadLang(void);
		BOOL SaveLang(void);
	};

// End of File

#endif
