
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modular Controller Master Runtime
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODSLC_HPP

#define	INCLUDE_MODSLC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SLC Module Configuration
//

class CSLCModule : public CModule
{
	public:
		// Constructor
		SLOW CSLCModule(void);

		// Destructor
		SLOW ~CSLCModule(void);

	public:
		// Config Data
		WORD	m_TempUnits;
		SHORT	m_ProcMin;
		SHORT	m_ProcMax;

	protected:
		// Data Members
		BOOL	m_fDelta[4];
		LONG	m_p1;
		LONG	m_p2;
		LONG	m_fs;
		BOOL	m_tc;
		BOOL	m_fTuning;

		// Overridables
		void OnLoad(PCBYTE &pData);
		void OnReadPersistData(CProxyRack *pProxy);
		void OnWritePersistData(CProxyRack *pProxy);
		void OnFilterInit(WORD PropID, DWORD Data);
		BOOL OnFilterWrite(WORD PropID, DWORD Data);
		DWORD LinkToDisp(WORD PropID, DWORD Data);
		DWORD DispToLink(WORD PropID, DWORD Data);

		// Implementation
		void FindScaling(void);
		LONG MakeDispLong(WORD Data, BOOL fDelta);
		LONG MakeLinkLong(WORD Data, BOOL fDelta);
		BOOL IsScaled(WORD PropID, BOOL &fDelta);
	};	

// End of File

#endif
