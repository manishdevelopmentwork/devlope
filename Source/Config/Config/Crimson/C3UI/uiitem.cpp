
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//								
// Data Item with User Interface Support
//

// Runtime Class

AfxImplementRuntimeClass(CUIItem, CMetaItem);

// Constructor

CUIItem::CUIItem(void)
{
	}

// Attributes

CString CUIItem::GetPropAsText(CString Tag) const
{
	CMetaData const *pMeta = FindMetaData(Tag);

	if( pMeta ) {

		CUISchema       *pSchema = New CUISchema((CItem *) this);

		CUIData   const *pUIData = pSchema->GetUIData(Tag);

		CUITextElement  *pText   = AfxNewObject(CUITextElement, pUIData->m_ClassText);

		if( pText ) {

			pText->Bind((CItem *) this, pUIData);

			CString Text = pText->GetAsText();

			delete pText;

			delete pSchema;

			return Text;
			}

		delete pSchema;
		}

	return L"";
	}

CString CUIItem::GetSubItemLabel(CItem const *pItem) const
{
	CMetaList *pList = FindMetaList();

	UINT      uCount = pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		UINT             uType = pMeta->GetType();

		if( uType == metaObject || uType == metaVirtual ) {

			CItem *pFind = pMeta->GetObject(PVOID(this));

			if( pFind == pItem ) {

				CUISchema       *pSchema = New CUISchema((CItem *) this);

				CUIData   const *pUIData = pSchema->GetUIData(pMeta->GetTag());

				CString          Text    = pUIData ? pUIData->m_Label : L"";

				delete pSchema;

				return Text;
				}
			}
		}

	return L"";
	}

CLASS CUIItem::GetSubItemClass(CItem const *pItem) const
{
	CMetaList *pList = FindMetaList();

	UINT      uCount = pList->GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CMetaData const *pMeta = pList->FindData(n);

		UINT             uType = pMeta->GetType();

		if( uType == metaObject || uType == metaVirtual ) {

			CItem *pFind = pMeta->GetObject(PVOID(this));

			if( pFind == pItem ) {

				CUISchema       *pSchema = New CUISchema((CItem *) this);

				CUIData   const *pUIData = pSchema->GetUIData(pMeta->GetTag());

				CLASS            Class   = pUIData->m_ClassText;

				delete pSchema;

				return Class;
				}
			}
		}

	return NULL;
	}

// UI Management

CViewWnd * CUIItem::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateItemView(TRUE);
		}

	return NULL;
	}

// UI Overridables

void CUIItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pHost->HasWindow() ) {

		CWnd &Wnd = pHost->GetWindow();

		if( Wnd.IsKindOf(AfxRuntimeClass(CUIViewWnd)) ) {

			CUIViewWnd *pView = (CUIViewWnd *) &Wnd;

			OnUIChange(pView, pItem, Tag);
			}
		}

	if( Tag.IsEmpty() ) {

		pHost->UpdateUI();
		}
	}

void CUIItem::OnUIChange(CUIViewWnd *pView, CItem *pItem, CString Tag)
{
	// NOTE -- Legacy use only. Use the IUIHost version!
	}

BOOL CUIItem::OnLoadPages(CUIPageList *pList)
{
	CLASS Class = AfxPointerClass(this);

	for(;;) {

		if( Class == AfxRuntimeClass(CMetaItem) ) {

			break;
			}

		if( Class == AfxRuntimeClass(CUIItem) ) {

			break;
			}

		CString Name  = CString(Class->GetClassName()) + L"_Pages";

		CEntity Entity(Name, Class->GetModule());

		LCID Local = GetThreadLocale();

		LCID Basic = MAKELCID(MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US), SORT_DEFAULT);

		UINT uPass = (Local == Basic ? 1 : 2);

		for( UINT n = 0; n < uPass; n++ ) {

			HANDLE hPages = afxModule->LoadResource(Entity, RT_RCDATA);

			if( hPages ) {

				CString Text = PCTXT(LockResource(hPages));

				CStringArray Page;

				Text.Tokenize(Page, ',');

				UINT uPage = 1;

				for( UINT p = 0; p < Page.GetCount(); p++ ) {

					CUIStdPage *pPage = New CUIStdPage;

					pPage->Create(AfxThisClass(), uPage, Page[p]);

					pList->Append(pPage);
					}

				UnlockResource(hPages);

				return TRUE;
				}
			}

		Class = Class->GetBaseClass();
		}

	CUIStdPage *pPage = New CUIStdPage(AfxThisClass());

	pList->Append(pPage);

	return FALSE;
	}

BOOL CUIItem::OnSummarize(CString &Text)
{
	return FALSE;
	}

// Implementation

CViewWnd * CUIItem::CreateItemView(BOOL fScroll)
{
	CUIPageList *pList = New CUIPageList;

	// LATER -- Setting this to TRUE forces everything
	// on to a single page. Should we support this???

	if( FALSE ) {

		OnLoadPages(pList);
		}
	else {
		if( OnLoadPages(pList) ) {

			return New CUIItemMultiWnd(pList, fScroll);
			}

		AfxAssert(pList->GetCount() == 1);
		}

	CUIViewWnd *pView = New CUIItemViewWnd(pList, fScroll);

	pView->SetBorder(6);
	
	return pView;
	}

//////////////////////////////////////////////////////////////////////////
//								
// Default View for UI Item
//

// Runtime Class

AfxImplementRuntimeClass(CUIItemViewWnd, CUIViewWnd);

// Constructor

CUIItemViewWnd::CUIItemViewWnd(CUIPageList *pList, BOOL fScroll)
{
	m_pList   = pList;

	m_pPage   = pList->GetEntry(0);

	m_fScroll = fScroll;

	m_pItem   = NULL;
	}

CUIItemViewWnd::CUIItemViewWnd(CUIPage *pPage, BOOL fScroll)
{
	m_pList   = NULL;

	m_pPage   = pPage;

	m_fScroll = fScroll;

	m_pItem   = NULL;
	}

// Destructor

CUIItemViewWnd::~CUIItemViewWnd(void)
{
	delete m_pList;
	}

// UI Update

void CUIItemViewWnd::OnUICreate(void)
{
	StartPage(1);

	if( m_pList ) {

		for( UINT n = 0; n < m_pList->GetCount(); n++ ) {

			CUIPage *pPage = m_pList->GetEntry(n);
			
			pPage->LoadIntoView(this, m_pItem);
			}
		}
	else
		m_pPage->LoadIntoView(this, m_pItem);

	EndPage(FALSE);
	}

void CUIItemViewWnd::OnUIChange(CItem *pItem, CString Tag)
{
	IUIHost *pHost = this;

	if( pItem ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

			CUIItem *pUI = (CUIItem *) pItem;

			pUI->OnUIChange(pHost, pItem, Tag);

			return;
			}
		}

	SendToAll(Tag);
	}

// Operations

void CUIItemViewWnd::SetPage(CUIPage *pPage)
{
	m_pPage = pPage;
	}

// Overridables

void CUIItemViewWnd::OnAttach(void)
{
	m_pItem = (CUIItem *) CViewWnd::m_pItem;

 	CUIViewWnd::OnAttach();
	}

BOOL CUIItemViewWnd::OnNavigate(CString const &Nav)
{
	if( CUIViewWnd::OnNavigate(Nav) ) {
	
		if( m_pList ) {

			SetFocus();
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CUIItemViewWnd::SendToAll(CString Tag)
{
	CTree <CItem *> Tree;

	Tree.Insert(m_pItem);

	IUIHost *pHost = this;
	
	m_pItem->OnUIChange(pHost, m_pItem, Tag);

	UINT c = m_UIList.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CUIElement *pUI   = m_UIList[n];

		CItem      *pItem = pUI->GetItem();

		if( pItem ) {
			
			if( Tree.Failed(Tree.Find(pItem)) ) {

				if( pItem->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

					CUIItem *pUI = (CUIItem *) pItem;

					pUI->OnUIChange(pHost, pItem, Tag);

					if( m_fRemake ) {

						break;
						}
					}

				Tree.Insert(pItem);
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Multi Page UI Item View
//

// Runtime Class

AfxImplementRuntimeClass(CUIItemMultiWnd, CMultiViewWnd);

// Constructor

CUIItemMultiWnd::CUIItemMultiWnd(CUIPageList *pList, BOOL fScroll)
{
	m_pList    = pList;

	m_fScroll  = fScroll;

	m_fBusy    = FALSE;

	m_fRecycle = TRUE;

	m_nMargin  = 4;
	}

// Destructor

CUIItemMultiWnd::~CUIItemMultiWnd(void)
{
	FreeList();
	}

// Operations

BOOL CUIItemMultiWnd::RemakeRest(CWnd *pSkip)
{
	if( !m_fBusy ) {

		m_fBusy = TRUE;

		for( UINT uPage = 0; uPage < m_Views.GetCount(); uPage++ ) {

			CreateView(uPage);

			CUIItemViewWnd *pView = (CUIItemViewWnd *) m_Views[uPage];

			if( pView != pSkip ) {

				pView->RemakeUI();
				}
			}

		m_fBusy = FALSE;

		return TRUE;
		}

	return FALSE;
	}

// Overridables

void CUIItemMultiWnd::OnAttach(void)
{
	FindItem();

	// LATER -- This is rather nasty. It would
	// be much nicer if we could reattach the
	// existing page objects...

	FreeList();

	m_pList = New CUIPageList;

	m_pItem->OnLoadPages(m_pList);

	AttachViews();
	}

CString CUIItemMultiWnd::OnGetNavPos(void)
{
	CString Item = m_Views[m_uFocus]->GetNavPos();

	CString Left = Item.StripToken(':');

	CString Page = CPrintf(L"%u", m_uFocus);

	CString Nav  = Left + L":" + Page + L":" + Item;

	return Nav;
	}

BOOL CUIItemMultiWnd::OnNavigate(CString const &Nav)
{
	UINT  uFind = Nav.Find(L'!');

	CString Src = Nav.Left(uFind);

	CString Loc = Nav.Mid(uFind+1);

	UINT uSep = Src.Count(':');

	if( uSep == 2 ) {

		CString Item = Src;

		CString Left = Item.StripToken(':');

		CString Page = Item.StripToken(':');

		CString Rest = Left + L":" + Item;

		if( !Page.IsEmpty() ) {

			UINT uPage = watoi(Page);

			if( uPage < m_Views.GetCount() ) {

				SelectFocus(uPage);

				if( m_Views[uPage]->Navigate(Rest) ) {

					if( !IsCurrent() ) {

						SetFocus();
						}

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( uSep == 1 ) {

		for( UINT uPage = 0; uPage < m_Views.GetCount(); uPage++ ) {

			CreateView(uPage);

			if( m_Views[uPage]->Navigate(Nav) ) {

				SelectFocus(uPage);

				if( !IsCurrent() ) {

					SetFocus();
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CUIItemMultiWnd::OnExec(CCmd *pCmd)
{
	m_Views[m_uFocus]->ExecCmd(pCmd);
	}

void CUIItemMultiWnd::OnUndo(CCmd *pCmd)
{
	m_Views[m_uFocus]->UndoCmd(pCmd);
	}

// Routing Control

BOOL CUIItemMultiWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	#if defined(_DEBUG)

	if( Message.message == WM_AFX_COMMAND ) {

		if( Message.wParam == IDM_VIEW_REFRESH ) {

			return FALSE;
			}
		}

	#endif

	return CMultiViewWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CUIItemMultiWnd, CMultiViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxMessageEnd(CUIItemMultiWnd)
	};

// Message Handlers

void CUIItemMultiWnd::OnPostCreate(void)
{
	SetTabStyle(0);

	MakeTabs();

	m_DropHelp.Bind(m_hWnd, this);

	CMultiViewWnd::OnPostCreate();
	}

// View Creation

CViewWnd * CUIItemMultiWnd::CreateViewObject(UINT n, CUIPage *pPage)
{
	return New CUIItemViewWnd(pPage, m_fScroll);
	}

// Implementation

void CUIItemMultiWnd::MakeTabs(void)
{
	for( UINT n = 0; n < m_pList->GetCount(); n++ ) {

		CUIPage  *pPage = m_pList->GetEntry(n);

		CViewWnd *pView = CreateViewObject(n, pPage);

		AddView(pPage->GetTitle(), pView);
		}

	AttachViews();
	}

void CUIItemMultiWnd::AttachViews(void)
{
	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {
	
		CViewWnd * pWnd  = m_Views[n];

		CUIPage  * pPage = m_pList->GetEntry(n);

		if( pWnd->IsWindow() ) {

			if( !pWnd->CanRecycle() ) {

				CViewWnd *pView = CreateViewObject(n, pPage);

				m_Views.SetAt(n, pView);

				pView->Attach(m_pItem);

				m_dwMade &= ~(1 << n);

				if( n == m_uFocus ) {

					CreateView(n);

					pView->SetCurrent(IsCurrent());

					pView->SetWindowOrder(HWND_TOP, TRUE);

					pView->ShowWindow(SW_SHOW);
					}

				pWnd->ShowWindow(SW_HIDE);

				pWnd->DestroyWindow(TRUE);

				continue;
				}
			}

		CUIItemViewWnd *pView = (CUIItemViewWnd *) pWnd;

		pView->SetPage(pPage);

		pView->Attach(m_pItem);
		}
	}

void CUIItemMultiWnd::FindItem(void)
{
	CMultiViewWnd::OnAttach();

	m_pItem = (CUIItem *) CMultiViewWnd::m_pItem;
	}

void CUIItemMultiWnd::FreeList(void)
{
	delete m_pList;

	m_pList = NULL;
	}

// End of File
