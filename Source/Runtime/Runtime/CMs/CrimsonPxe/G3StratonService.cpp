
#include "Intern.hpp"

#include "G3StratonService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Programming Service
//

// Instantiator

global ILinkService * Create_StratonService(ICrimsonPxe *pPxe)
{
	return New CG3StratonService(pPxe);
}

// Constructor

CG3StratonService::CG3StratonService(ICrimsonPxe *pPxe)
{
	m_pPxe = pPxe;

	StdSetRef();
}

// Destructor

CG3StratonService::~CG3StratonService(void)
{
}

// IUnknown

HRESULT CG3StratonService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3StratonService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3StratonService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3StratonService::Timeout(void)
{
}

UINT CG3StratonService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servStraton ) {

		if( Req.GetOpcode() == stratonService ) {

			ICrimsonApp *pApp;

			if( m_pPxe->LockApp(pApp) ) {

				UINT   uPtr  = 0;

				PCBYTE pHead = Req.ReadData(uPtr, 4);

				// Straton doens't always clear these...

				m_bData[0] = 0;
				m_bData[1] = 0;
				m_bData[2] = 0;
				m_bData[3] = 0;

				if( pApp->StratonCall(pHead, m_bData) ) {

					UINT uSize = MAKEWORD(m_bData[3], m_bData[2]);

					UINT uSend = 4 + uSize;

					Rep.StartFrame(servStraton, stratonService);

					Rep.AddData(m_bData, uSend);

					m_pPxe->FreeApp();

					return procOkay;
				}

				m_pPxe->FreeApp();
			}
		}
	}

	return procError;
}

void CG3StratonService::EndLink(CG3LinkFrame &Req)
{
}

// End of File
