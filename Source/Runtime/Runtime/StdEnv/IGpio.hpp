
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IGpio_HPP

#define INCLUDE_IGpio_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 20 -- Gpio Interfaces
//

// https://

interface IEventSink;

//////////////////////////////////////////////////////////////////////////
//
// Interrupt Types 
//

enum
{
	intLevelLo	= 0,
	intLevelHi	= 1,
	intEdgeRising	= 2,
	intEdgeFalling	= 3,
	intEdgeAny	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// General Purpose Input Ouput
//

interface IGpio : public IDevice
{
	// https://

	AfxDeclareIID(20, 1);
	
	virtual void METHOD SetDirection(UINT n, BOOL fOutput)					= 0;
	virtual void METHOD SetState(UINT n, BOOL fState)					= 0;
	virtual BOOL METHOD GetState(UINT n)							= 0;
	virtual void METHOD EnableEvents(void)							= 0;
	virtual void METHOD SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam)	= 0;
	virtual void METHOD SetIntEnable(UINT n, BOOL fEnable)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Expander
//

interface IGpioExpander : public IDevice
{
	// Identifier
	AfxDeclareIID(20, 2);

	// Methods
	virtual void METHOD SetBit(UINT uIndex, bool fState)	= 0;
	virtual BOOL METHOD GetBit(UINT uIndex)			= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Sensor
//

interface ISensor : public IDevice
{
	// Identifier
	AfxDeclareIID(20, 3);

	// Methods
	virtual INT  METHOD GetData(void)	= 0;
	virtual BOOL METHOD GetAlarm(void)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Device I/O
//

interface IDeviceIo : public IDevice
{
	// Identifier
	AfxDeclareIID(20, 4);

	// Methods
	virtual bool METHOD GetDigitalIn(void)         = 0;
	virtual UINT METHOD GetAnalogIn(void)          = 0;
	virtual UINT METHOD GetPsuVoltage(void)        = 0;
	virtual void METHOD SetDigitalOut(bool fState) = 0;
	virtual bool METHOD GetDigitalOut(void)        = 0;
};

// End of File

#endif
