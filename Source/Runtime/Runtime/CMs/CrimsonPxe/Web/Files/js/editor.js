
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// TOOD -- Refresh info tabs using status_func hook.
// TODO -- Change tooltip container size.

// TODO -- Some inconsitency in icon tooltips.
// TODO -- Add mac addresss type!
// TODO -- Switch colors to css style references.
// TODO -- Locked fields lack units!
// TODO -- Tidy row icon control.
// TODO -- Break rows and template into functions.
// TODO -- Status panes should be self-refreshing.

//////////////////////////////////////////////////////////////////////////
//
// Config Editor
//

// Static Data

var m_schema;
var m_person;
var m_pvalid;
var m_config;
var m_previous;
var m_dirty;
var m_uinode;
var m_uibase;
var m_busy;
var m_modal;
var m_dtag;
var m_nowarn;
var m_type;
var m_lock;

// Initialization

function doConfig() {

	m_busy = false;

	m_modal = [];

	m_dtag = 0;

	m_nowarn = false;

	window.onbeforeunload = function (e) {

		if (m_dirty && !m_nowarn) {

			return "Changes Will Be Lost";
		}
	};

	doSession();

	m_type = getParam("m");

	m_lock = (getParam("r") == "1");

	readFile("schema", m_type, onReadSchema);

	setTimeout(ping, 10000);
}

function ping() {

	var url = "/ajax/sysping.ajax";

	$.get({
		url: makeAjax(url),
		success: onPingOkay,
		error: onPingFailed,
		xhrFields: { withCredentials: true }
	});
}

function onPingOkay() {

	setTimeout(ping, 10000);
}

function onPingFailed(error) {

	if (error.status == 401) {

		m_dirty = false;

		check401(error.status);

		return;
	}

	setTimeout(ping, 10000);
}

function onReadSchema(data) {

	m_schema = data;

	m_pvalid = false;

	$("#page-title").html(m_schema.label);

	if (m_type == "s") {

		readFile("config", "p", onReadPerson);

		return;
	}

	readFile("config", m_type, onReadConfig);
}

function onReadPerson(data) {

	m_person = data;

	if ("set" in m_person) {

		if ("keys" in m_person.set) {

			if (m_person.set.keys.length > 0) {

				m_pvalid = true;
			}
		}
	}

	readFile("config", m_type, onReadConfig);
}

function onReadConfig(data) {

	m_config = data;

	if ("atype" in m_config && m_config.atype == m_type) {

		m_previous = deepCopy(m_config);

		m_dirty = null;

		if (!m_lock) {

			$("#tb-save").click(onSave);
			$("#tb-undo").click(onUndo);
			$("#tb-upld").click(onUpload);
		}

		$("#tb-down").click(onDownload);

		var nav = getParam("nav");

		var tab = getParam("tab");

		loadTree();

		selectNode(nav, tab ? parseInt(tab) : 0);

		setConfigDirty(false);

		return;
	}

	var error = {};

	error["status"] = 404;

	onReadFailed(error);
}

function onReadFailed(error) {

	if (error.status == 401) {

		m_dirty = false;
	}

	if (!check401(error.status)) {

		if (m_schema && !("label" in m_schema)) {

			$("<h1/>").html("Configuration").appendTo($("#page-header"));
		}

		var text = $("#tb-text");

		text.html("FAILED TO LOAD");

		text.css("color", "#FF0000");
	}
}

function readFile(file, type, action) {

	var url = "/ajax/sysread.ajax?f=" + file + "&t=" + type;

	$.get({
		url: makeAjax(url),
		success: action,
		error: onReadFailed,
		xhrFields: { withCredentials: true },
	});
}

// Save and Undo

function onSave() {

	if (m_dirty) {

		confirm("SAVE CHANGES",
			"Do you really want to save the changes made to the configuration?",
			doSave
		);
	}
}

function doSave() {

	var fail = function (error) {

		if (error.status == 401) {

			m_dirty = false;
		}

		if (!check401(error.status)) {

			display("ERROR", "Failed to save configuration.");
		}
	};

	var done = function (data) {

		m_config = data;

		m_previous = deepCopy(m_config);

		setAllNodesDirty(false);

		setConfigDirty(false);

		refreshPage();
	};

	var url = "/ajax/syswrite.ajax?f=config&t=" + m_type;

	$.ajax({
		type: "POST",
		url: url,
		data: JSON.stringify(sortObjByKey(m_config)),
		contentType: "application/json",
		error: fail,
		success: done
	});
}

function onUndo() {

	if (m_dirty) {

		var f = function () {

			m_config = deepCopy(m_previous);

			setAllNodesDirty(false);

			setConfigDirty(false);

			refreshPage();
		};

		confirm("REVERT CHANGES",
			"Do you really want to revert all the changes made to the configuration?",
			f
		);
	}
}

function onUpload() {

	if (!m_dirty) {

		var hid = $("#hidden");

		hid.empty();

		var sel = $("<input/>").appendTo(hid);

		sel.attr("type", "file").attr("accept", "application/json");

		sel.on("input", function (e) {

			var file = this.files[0];

			var read = new FileReader();

			read.onerror = function (e) {

				display("READ FAILED", "Unable to read from file.");
			};

			read.onload = function (e) {

				var text = this.result;

				try {

					var json = JSON.parse(text);

					if ("atype" in json && json["atype"] == m_type) {

						var save = function () {

							m_config = deepCopy(json);

							doSave();
						};

						confirm("UPLOAD FILE",
							"Do you really want to overwrite the " + m_schema.label + "?",
							save
						);

						return;
					}

					display("READ FAILED", "The file does not contain a valid " + m_schema.label + ".");
				}

				catch (e) {
					display("READ FAILED", "The file does not contain valid JSON.");
				}
			};

			read.readAsText(file);
		});

		sel.click();
	}
}

function onDownload() {

	if (!m_dirty) {

		var hid = $("#hidden");

		hid.empty();

		var url = "data:application/json," + encodeURIComponent(JSON.stringify(m_config));

		var act = $("<a/>").appendTo(hid);

		var def = m_schema["file"];

		act.attr("href", url).attr("download", def);

		act[0].click();
	}
}

// Modal Dialogs

function showButton(button, cname, label) {

	button.removeClass("btn-danger btn-success btn-primary btn-secondary btn-default");

	button.addClass(cname);

	button.html(label);

	button.show();
}

function hideButton(button) {

	button.hide();
}

function confirm(label, body, action) {

	$("#modal-label").html(label);

	$("#modal-body").html(body);

	showButton($("#modal-b1"), "btn-danger", "OK");

	hideButton($("#modal-b2"));

	showButton($("#modal-b3"), "btn-secondary", "Cancel");

	modal(false, action);
}

function display(label, body, action) {

	$("#modal-label").html(label);

	$("#modal-body").html(body);

	showButton($("#modal-b1"), "btn-primary", "OK");

	hideButton($("#modal-b2"));

	hideButton($("#modal-b3"));

	modal(true, action);
}

function yesNoCancel(label, body, action) {

	$("#modal-label").html(label);

	$("#modal-body").html(body);

	showButton($("#modal-b1"), "btn-success", "Yes");

	showButton($("#modal-b2"), "btn-danger", "No");

	showButton($("#modal-b3"), "btn-secondary", "Cancel");

	modal(false, action);
}

function modal(cancel, action) {

	var b1 = function () {

		hideModal($('#modal'));

		if (!!action) {

			action(1);
		}
	};

	var b2 = function () {

		hideModal($('#modal'));

		if (!!action) {

			action(2);
		}
	};

	var b3 = function () {

		hideModal($('#modal'));
	};

	$("#modal-b1").off("click").on("click", b1);

	$("#modal-b2").off("click").on("click", b2);

	$("#modal-b3").off("click").on("click", b3);

	$("#modal-bx").off("click").on("click", cancel ? b1 : b3);

	showModal($('#modal'));
}

function showModal(item) {

	if (false) {

		if (m_modal.length > 0) {

			m_modal[m_modal.length - 1].modal("hide");
		}
	}

	item.off("hidden.bs.modal").on("hidden.bs.modal", onModalHidden);

	item.modal("show");

	m_modal.push(item);
}

function hideModal(item) {

	item.modal("hide");
}

function onModalHidden() {

	m_modal.pop();

	if (false) {

		if (m_modal.length > 0) {

			m_modal[m_modal.length - 1].modal("show");
		}
	}
}

// Wait Display

function showWait() {

	$("body").addClass("wait");

	$("#waiter").show().css("opacity", "0.5");
}

function hideWait() {

	$("#waiter").css("opacity", "0").hide();

	$("body").removeClass("wait");
}

// Navigation Loading

function loadTree() {

	var td = [];

	addChildren(td, m_schema, "0");

	$('#tree').treeview({ data: td, onNodeSelected: nodeSelected, showTags: true });
}

function addChildren(td, data, root) {

	if ('children' in data) {

		for (var i = 0; i < data.children.length; i++) {

			if (!("visible" in data.children[i]) || data.children[i].visible) {

				var node = {};

				var nodes = [];

				node.text = data.children[i].label;

				node.path = root + "." + i.toString();

				addChildren(nodes, data.children[i], node.path);

				if (nodes.length) {

					node.nodes = nodes;

					node.state = {};

					node.state.expanded = true;
				}

				td.push(node);
			}
		}
	}
}

// Property Access

function getChild(node, path, pos) {

	if (pos < path.length) {

		return getChild(node.children[path[pos]], path, pos + 1);
	}

	return node;
}

function getBase(path) {

	var node = m_schema;

	var base = "";

	for (var i = 1; i < path.length; i++) {

		node = node.children[path[i]];

		if ('name' in node) {

			if (base != "") {

				base += ".";
			}

			base += node.name;
		}
	}

	return base;
}

function walkPath(object, list, force) {

	var walk = object;

	for (var i = 0; i < list.length - 1; i++) {

		var name = list[i];

		if (!(name in walk)) {

			if (!force) {

				return null;
			}
			else {

				if (isNumber(list[i + 1])) {

					walk[name] = [];
				}
				else {

					walk[name] = {};
				}
			}
		}

		walk = walk[name];
	}

	return walk;
}

function getObject(object, prop, force) {

	var list = prop.split('.');

	var find = walkPath(object, list, force);

	return find ? find : {};
}

function getValue(object, prop, force) {

	var list = prop.split('.');

	var find = walkPath(object, list, force);

	var name = list.pop();

	return (find && (name in find)) ? find[name] : "";
}

function setValue(object, prop, data) {

	var list = prop.split('.');

	var find = walkPath(object, list, true);

	var name = list.pop();

	if (!data || data == "") {

		delete find[name];
	}
	else {

		find[name] = data;
	}
}

function getPropValue(prop) {

	var list = prop.split('.');

	var find = walkPath(m_config, list, true);

	var name = list.pop();

	return (find && name in find) ? find[name] : "";
}

function getPrevValue(prop) {

	var list = prop.split('.');

	var find = walkPath(m_previous, list, false);

	var name = list.pop();

	return (find && name in find) ? find[name] : "";
}

function getPropObject(prop) {

	return getObject(m_config, prop, true);
}

function getPrevObject(prop) {

	return getObject(m_previous, prop, true);
}

function getPropTable(prop) {

	return getObject(m_config, prop + ".0", true);
}

function getPrevTable(prop) {

	return getObject(m_previous, prop + ".0", true);
}

function setPropValue(prop, data) {

	setValue(m_config, prop, data);
}

function setPrevValue(prop, data) {

	setValue(m_previous, prop, data);
}

// Page Loading

function refreshPage() {

	var tree = $('#tree').data('treeview')

	var path = tree.getSelected()[0].path;

	var tnum = $('.nav-tabs li.active').data("nth");

	showNode(path);

	if (tnum != undefined) {

		showTab(tnum);
	}
}

function refreshTab(item) {

	if ('tabs' in m_uinode) {

		if (m_uinode.tabs.length == 1 && m_uinode.tabs[0].label == "") {

			var tp = item.closest(".single-tab");

			tp.empty();

			loadTab(tp, m_uinode.tabs[0], m_uibase);
		}
		else {

			var tp = item.closest(".tab-pane");

			var tn = tp.data("tnum");

			tp.empty();

			loadTab(tp, m_uinode.tabs[tn], m_uibase);
		}

		bindAll();

		doEnables(item);
	}
}

function selectNode(path, tab) {

	var tree = $('#tree').data('treeview');

	var node = tree.getNode(0);

	if (path) {

		var list = path.split('.');

		var init = parseInt(list[1]);

		for (var i = 0; i < m_schema.children.length; i++) {

			if (!("visible" in m_schema.children[i]) || m_schema.children[i].visible) {

				break;
			}

			init--;
		}

		if (init > 0) {

			var sibs = tree.getSiblings(node);

			if (init <= sibs.length) {

				node = sibs[init - 1];
			}
		}

		for (var n = 2; n < list.length; n++) {

			var i = parseInt(list[n]);

			if (i < node.nodes.length) {

				node = node.nodes[i];
			}
		}
	}

	tree.revealNode(node);

	tree.selectNode(node);

	if (tab) {

		showTab(tab);
	}

	var li = $("#tree li[data-nodeid='" + node.nodeId.toString() + "']");

	li[0].scrollIntoView(0);
}

function nodeSelected(event, data) {

	showNode(data.path);

	setUrlArg("nav", data.path);

	showTab(0);
}

function showNode(nav) {

	var path = nav.split('.');

	m_uinode = getChild(m_schema, path, 1);

	m_uibase = getBase(path);

	var cdiv = $('#tabs').empty();

	if ('tabs' in m_uinode) {

		if (m_uinode.tabs.length == 1 && m_uinode.tabs[0].label == "") {

			var bc = $("<div/>").addClass("bordered-content single-tab").appendTo(cdiv);

			loadTab(bc, m_uinode.tabs[0], m_uibase);

			setUrlArg("tab", 0);
		}
		else {

			loadTabs(cdiv, m_uinode.tabs, m_uibase);
		}
	}
	else {

		var bc = $("<div/>").addClass("bordered-content").appendTo(cdiv);

		var h4 = $("<h4/>").css("color", "#808CC8").html("Settings").appendTo(bc);

		var pp = $("<p/>").html("This item has no editable properties.").appendTo(bc);

		setUrlArg("tab", 0);
	}

	bindAll();

	doEnables(cdiv);
}

function showTab(tab) {

	$(".nav-tabs li:nth-of-type(" + (1 + tab).toString() + ") a").tab('show');
}

function tabSelected(e) {

	var li = $(e.target.parentElement);

	var tn = li.data("nth");

	setUrlArg("tab", tn);
}

function loadTabs(div, tabs, base) {

	var bt = $("<div/>").addClass("bordered-tab-content").appendTo(div);

	var ul = $("<ul/>").addClass("nav nav-tabs").appendTo(bt);

	for (var t = 0; t < tabs.length; t++) {

		var li = $("<li/>").appendTo(ul);

		li.data("nth", t);

		var tn = "tab-" + t.toString();

		var hr = $("<a/>").attr("href", "#" + tn).attr("data-toggle", "tab").appendTo(li);

		hr.on("shown.bs.tab", tabSelected);

		if ("icon" in tabs[t]) {

			$("<span/>").addClass("fa " + tabs[t].icon).css("padding-right", "6px").appendTo(hr);
		}

		$("<span/>").html(tabs[t].label).appendTo(hr);
	}

	var tc = $("<div class='tab-content'/>").appendTo(bt);

	tc.css("overflow-x", "hidden");

	tc.css("overflow-y", "auto");

	tc.css("max-height", "calc(100vh - 265px)");

	for (var t = 0; t < tabs.length; t++) {

		var td = $("<div/>").attr("id", "tab-" + t.toString()).addClass("tab-pane").appendTo(tc);

		td.data("tnum", t);

		loadTab(td, tabs[t], base);
	}
}

function loadTab(div, tab, base) {

	if ("name" in tab) {

		base += "." + tab.name;
	}

	if ("sections" in tab) {

		for (var s = 0; s < tab.sections.length; s++) {

			var sect = tab.sections[s];

			if (s == 0 && ("note" in tab)) {

				var nt = $("<div/>").html("<i>" + tab.note + "</i>").css("color", "#808080").css("float", "right").appendTo(div);

				nt.css("margin-top", "5px").css("margin-right", "10px");
			}

			sect.dtag = (m_dtag++).toString();

			var mt = s ? "30px" : "10px";

			var h4 = $("<h4/>").css("color", "#808CC8").html(tab.sections[s].label).css("margin-top", mt).appendTo(div);

			if ("info" in sect) {

				var i1 = $("<span/>").addClass("fa fa-refresh refresh-icon").appendTo(h4);

				i1.css("margin-top", "5px").css("margin-left", "10px").css("font-size", "12px");
			}

			var fd = $("<div/>").addClass("form-horizontal").appendTo(div);

			var dd = $("<div/>").attr("id", "ddiv-" + sect.dtag).css("display", "none").appendTo(fd);

			var ed = $("<div/>").attr("id", "ediv-" + sect.dtag).appendTo(fd);

			var dm = ("message" in sect) ? sect.message : "This feature is disabled by another configuration option.";

			$("<p/>").html(dm).appendTo(dd);

			loadSection(ed, sect, base);
		}
	}
	else {

		$("<h4/>").css("color", "#808CC8").html("Settings").appendTo(div);

		$("<p/>").html("This item has no editable properties.").appendTo(div);
	}
}

function loadSection(div, sect, base) {

	if ('tools' in sect) {

		loadTools(div, sect.tools);
	}

	if ('info' in sect) {

		loadInfo(div, sect.info);
	}

	if ('table' in sect) {

		loadTable(div, sect.table, base);
	}

	if ('fields' in sect) {

		for (var f = 0; f < sect.fields.length; f++) {

			loadField(div, sect.fields[f], base);
		}
	}
}

function loadTools(div, tools) {

	for (var t = 0; t < tools.length; t++) {

		var tn = tools[t];

		var b1 = $("<button/>").attr("type", "button").addClass("btn btn-primary").css("margin-right", "8px").appendTo(div);

		var fn = function () {

			var run = function () {

				var prop = getPropObject(tn.bind);

				var done = function (okay) {

					if (okay) {

						showConfigChange(prop, true);

						refreshPage();
					}
				}

				window[tn.func](prop, done);
			};

			if (typeof window[tn.func] == "function") {

				run();
			}
			else {

				var tag = document.createElement("script");

				tag.onload = run;

				tag.src = tn.script + "?nc=" + (new Date()).getTime();

				document.head.appendChild(tag);
			}
		};

		b1.html(tn.label).click(fn);
	}
}

function loadInfo(div, info) {

	var tab = div.closest(".tab-pane");

	var ref = tab.find(".refresh-icon");

	var pre = $("<pre/>").html("Loading...").css("width", "100%").css("font-size", "12px").appendTo(div);

	var url = "/ajax/sysinfo.ajax?t=" + info.source;

	var good = function (data) {

		pre.html(data);
	};

	var fail = function (error) {

		if (!check401(error.status)) {

			pre.html("Failed to Read.");
		}
	};

	var fire = function () {

		$.get({
			url: makeAjax(url),
			success: good,
			error: fail,
			xhrFields: { withCredentials: true },
		});
	}

	fire();

	ref.on("click", fire);
}

function loadTable(div, table, base) {

	if ("fields" in table) {

		table.dtag = (m_dtag++).toString();

		var ddiv = $("<div/>").attr("id", "ddiv-" + table.dtag).css("display", "none").appendTo(div);

		var ediv = $("<div/>").attr("id", "ediv-" + table.dtag).appendTo(div);

		$("<p/>").html("This feature is disabled by another configuration option.").appendTo(ddiv);

		var rnum = ("rnum" in table) ? table.rnum : true;

		var prop = base + "." + table.name;

		var type = [];

		var tt = $("<table/>").appendTo(ediv);

		tt.addClass("table table-striped table-bordered").css("width", "0%").css("margin-top", "15px");

		tt.data("prop", prop);

		tt.data("info", table);

		var cc = 0;

		if (true) {

			var th = $("<thead/>").appendTo(tt);

			var tr = $("<tr/>").css("background", "#F0F0F0").appendTo(th);

			if (rnum) {

				var rn = $("<th/>").attr("scope", "col").appendTo(tr);

				if ("rname" in table) {

					rn.css("text-align", "center").html(table.rname);
				}

				rn.css("min-width", "30px");
			}

			for (var f = 0; f < table.fields.length; f++) {

				if (!table.fields[f].hide) {

					checkAliases(table.fields[f]);

					var th = $("<th/>").attr("scope", "col").html(table.fields[f].label).appendTo(tr);

					if ("width" in table.fields[f]) {

						th.css("min-width", table.fields[f].width.toString() + "px");
					}
					else {

						th.css("min-width", "150px");
					}

					cc++;
				}

				if (table.fields[f].type == "hidden") {

					type.push(null);
				}
				else {

					type.push(eval("new CField_" + table.fields[f].type + "(table.fields[f], {})"));
				}
			}

			$("<th/>").attr("scope", "col").css("min-width", "30px").appendTo(tr);
		}

		if ("rows" in table) {

			var data = getPropTable(prop);

			var prev = getPrevTable(prop);

			for (var r = data.length; r < table.rows; r++) {

				data[r] = {};

				data[r]["order"] = r;

				for (var f = 0; f < table.fields.length; f++) {

					var value = "";

					data[r][table.fields[f].name] = type[f].getDefault();
				}

				prev[r] = deepCopy(data[r]);
			}

			while (data.length > table.rows) {

				data.pop();
			}
		}

		if ("template" in table) {

			var name = table.fields[0].name;

			var temp = table.template;

			var data = getPropTable(prop);

			var prev = getPrevTable(prop);

			for (var t = 0; t < temp.length; t++) {

				var f = false;

				for (var r = 0; r < data.length; r++) {

					if (data[r][name] == temp[t][name]) {

						f = true;

						break;
					}
				}

				if (!f) {

					data.push(deepCopy(temp[t]));

					prev.push(deepCopy(temp[t]));
				}
			}

			for (var r = 0; r < data.length; r++) {

				var f = false;

				for (var t = 0; t < temp.length; t++) {

					if (data[r][name] == temp[t][name]) {

						f = true;

						break;
					}
				}

				if (!f) {

					data.splice(r, 1);

					prev.splice(r, 1);
				}
			}
		}

		if (true) {

			var data = getPropTable(prop);

			var tb = $("<tbody/>").appendTo(tt);

			for (var r = 0; r < data.length; r++) {

				var tr = $("<tr/>").appendTo(tb);

				tr.data("nrow", r);

				if (rnum) {

					$("<td/>").css("text-align", "center").html((r + 1).toString()).appendTo(tr);
				}

				for (var f = 0; f < table.fields.length; f++) {

					if (!table.fields[f].hide) {

						var pval = data[r][table.fields[f].name];

						var text = (pval == null || pval == undefined) ? "" : String(pval);

						if (text == "") {

							text = type[f].getPlaceholder();

							$("<td/>").css("padding-right", "20px").html(text).appendTo(tr);
						}
						else {

							text = type[f].getTableText(text);

							$("<td/>").css("padding-right", "20px").html(text).appendTo(tr);
						}
					}
				}

				var id = $("<td/>").css("white-space", "nowrap").appendTo(tr);

				if (!("template" in table)) {

					loadRowIcons(id, !("rows" in table), r == 0, r == data.length - 1);
				}
				else {

					loadRowEditIcon(id);
				}
			}

			if (!m_lock && !("rows" in table) && !("template" in table)) {

				var tr = $("<tr/>").css("background", "#F0F0F0").appendTo(tb);

				if (rnum) {

					$("<td/>").appendTo(tr);
				}

				var id = $("<td/>").attr("colspan", cc + 1).appendTo(tr);

				loadTableIcons(id, true);
			}
		}
	}
}

function loadRowEditIcon(div) {

	if (m_lock) {

		var i1 = $("<span/>").addClass("fa fa-search table-icon table-icon-edit").appendTo(div);

		i1.attr("data-toggle", "tooltip").attr("title", "View Row");

		i1.addClass("bind-table");

		i1.data("role", "row-edit");
	}
	else {

		var i1 = $("<span/>").addClass("fa fa-pencil table-icon table-icon-edit").appendTo(div);

		i1.attr("data-toggle", "tooltip").attr("title", "Edit Row");

		i1.addClass("bind-table");

		i1.data("role", "row-edit");
	}
}

function loadRowIcons(div, del, first, last) {

	if (m_lock) {

		var i1 = $("<span/>").addClass("fa fa-search table-icon table-icon-edit").appendTo(div);

		i1.attr("data-toggle", "tooltip").attr("title", "View Row");

		i1.addClass("bind-table");

		i1.data("role", "row-edit");
	}
	else {

		var i1 = $("<span/>").addClass("fa fa-pencil table-icon table-icon-edit").appendTo(div);

		i1.attr("data-toggle", "tooltip").attr("title", "Edit Row");

		i1.addClass("bind-table");

		i1.data("role", "row-edit");

		if (del) {

			var i2 = $("<span/>").addClass("fa fa-times-circle table-icon table-icon-delete").appendTo(div);

			i2.attr("data-toggle", "tooltip").attr("title", "Delete Row");

			i2.addClass("bind-table");

			i2.data("role", "row-delete");
		}
		else {

			var i2 = $("<span/>").addClass("fa fa-times-circle table-icon").appendTo(div);

			var nrow = div.closest("tr").data("nrow");

			var dtab = div.closest("table");

			var prop = dtab.data("prop");

			if (!canClearRow(div, dtab, prop)) {

				i2.addClass("table-icon-disable");
			}
			else {

				i2.addClass("table-icon-delete");

				i2.attr("data-toggle", "tooltip").attr("title", "Clear Row");

				i2.addClass("bind-table");

				i2.data("role", "row-clear");
			}
		}

		if (true) {

			var i3 = $("<span/>").addClass("fa fa-arrow-circle-down table-icon").appendTo(div);

			if (last) {

				i3.addClass("table-icon-disable");
			}
			else {

				i3.addClass("table-icon-move");

				i3.attr("data-toggle", "tooltip").attr("title", "Move Down");

				i3.addClass("bind-table");

				i3.data("role", "row-down");
			}
		}

		if (true) {

			var i4 = $("<span/>").addClass("fa fa-arrow-circle-up table-icon").appendTo(div);

			if (first) {

				i4.addClass("table-icon-disable");
			}
			else {

				i4.addClass("table-icon-move");

				i4.attr("data-toggle", "tooltip").attr("title", "Move Up");

				i4.addClass("bind-table");

				i4.data("role", "row-up");
			}
		}
	}
}

function loadTableIcons(div, add) {

	var i1 = $("<span/>").addClass("fa fa-plus-circle table-icon").appendTo(div);

	if (add) {

		i1.attr("data-toggle", "tooltip").attr("title", "Add Row");

		i1.addClass("table-icon-add bind-table");

		i1.data("role", "row-add");
	}
	else {

		i1.addClass("table-icon-disable");
	}

	var hd = $("<span/>").attr("id", "tab-undo").hide().appendTo(div);

	var i2 = $("<span/>").addClass("fa fa-edit table-icon").appendTo(hd);

	var i3 = $("<span/>").addClass("fa fa-undo table-icon table-icon-undo").appendTo(hd);

	i3.attr("data-toggle", "tooltip").attr("title", "Revert Table");

	i3.addClass("bind-table");

	i3.data("role", "tab-undo");
}

function loadField(div, info, base) {

	info.dtag = (m_dtag++).toString();

	var prop = base + "." + info.name;

	if (info.type == "const") {

		if (!m_lock) {

			var data = applyAliases(info.data);

			setPropValue(prop, data);

			setPrevValue(prop, data);
		}

		return;
	}

	if (info.type != "hidden") {

		var type = eval("new CField_" + info.type + "(info, prop)");

		if (info.type == "note") {

			loadValue(div, info, prop, type);
		}
		else {

			var fg = $("<div/>").addClass("form-group").css("padding-left", "0px").appendTo(div);

			checkAliases(info);

			fg.data("type", type);

			fg.data("info", info);

			fg.data("prop", prop);

			loadLabel(fg, info);

			loadSource(fg, info, prop);

			var vg = $("<div/>").addClass("col-sm-5").appendTo(fg);

			loadElement(vg, info, prop);

			loadValue(vg, info, prop, type);

			loadFieldIcons(fg, info, prop);
		}
	}
}

function loadLabel(div, info) {

	var lb = $("<label/>").appendTo(div);

	lb.addClass("control-label col-sm-3 form-label").attr("for", "s-" + info.dtag).css("text-align", "left");

	if (info.label.length > 0) {

		lb.html(info.label + ":");
	}
}

function loadSource(div, info, prop) {

	if (m_pvalid) {

		var sd = $("<div/>").addClass("col-sm-2").appendTo(div);

		if (m_lock) {

			var span = $("<span/>").appendTo(sd);

			span.attr("id", "s-" + info.dtag).addClass("bind-field form-tight form-control locked");

			span.data("role", "source");
		}
		else {

			var se = $("<select/>").appendTo(sd);

			se.attr("id", "s-" + info.dtag).addClass("bind-field form-tight form-control");

			se.data("role", "source");

			$("<option/>").attr("value", "0").appendTo(se).html("Regular");

			$("<option/>").attr("value", "1").appendTo(se).html("Custom");
		}
	}
}

function loadElement(div, info, prop) {

	if (m_pvalid) {

		var hd = $("<div>").addClass("show-me").hide().appendTo(div);

		if (m_lock) {

			var span = $("<span/>").appendTo(hd);

			span.attr("id", "e-" + info.dtag).addClass("bind-field form-tight form-control locked");

			span.data("role", "element");
		}
		else {

			var se = $("<select/>").appendTo(hd);

			se.attr("id", "e-" + info.dtag).addClass("bind-field form-tight form-control");

			se.data("role", "element");

			if (m_pvalid) {

				$("<option/>").attr("value", "[Undefined]").appendTo(se).html("[Undefined]");

				for (var k = 0; k < m_person.set.keys.length; k++) {

					var name = m_person.set.keys[k].name;

					$("<option/>").attr("value", name).appendTo(se).html(name);
				}
			}
		}
	}
}

function loadValue(div, info, prop, type) {

	var hd = $("<div>").appendTo(div);

	if (info.type != "note") {

		hd.addClass("show-me").hide();
	}

	type.loadField(hd, info, prop);
}

function loadFieldIcons(div, info, prop) {

	var id = $("<div/>").css("margin-top", "5px").appendTo(div);

	var i1 = $("<span/>").addClass("fa fa-question-circle field-icon").appendTo(id);

	if (!("help" in info)) {

		i1.addClass("field-icon-disable");
	}
	else {

		i1.data("role", "help");

		i1.addClass("bind-field field-icon-help");

		i1.attr("data-toggle", "tooltip").attr("title", "Show Help");
	}

	if (!div.closest(".form-horizontal").hasClass("no-undo")) {

		var hd = $("<span/>").addClass("show-me").hide().appendTo(id);

		var i2 = $("<span/>").addClass("fa fa-edit field-icon").appendTo(hd);

		var i3 = $("<span/>").attr("id", "u-" + info.dtag).addClass("bind-field fa fa-undo field-icon field-icon-undo").appendTo(hd);

		i3.data("role", "undo");

		i3.attr("data-toggle", "tooltip").attr("title", "Revert Field");
	}
}

function checkAliases(info) {

	if ("aliases" in m_schema) {

		if ("format" in info) {

			info.format = applyAliases(info.format);
		}
	}
}

function applyAliases(text) {

	if (text.substring(0, 1) == "=") {

		var name = text.substring(1);

		if (name in m_schema.aliases) {

			return m_schema.aliases[name].toString();
		}
	}

	if (text.substring(0, 1) == "!") {

	}

	return text;
}

// Binding and Events

function bindAll() {

	$('.bind-field').each(bindField);

	$('.bind-table').each(bindTable);

	bindToolTips();
}

function bindToolTips() {

	$("[data-toggle='tooltip']").tooltip({ trigger: 'hover' });
}

function bindField(i) {

	var item = $(this);

	var role = item.data("role");

	if (role == "source" || role == "element" || role == "data") {

		var form = item.closest(".form-group");

		var prop = form.data("prop");

		var data = String(getPropValue(prop));

		if (role == "source") {

			if (m_lock) {

				if (data.substring(0, 1) == "=") {

					item.html("Custom");
				}
				else {

					item.html("Regular");
				}
			}
			else {

				if (data.substring(0, 1) == "=") {

					item.val(1);
				}
				else {

					item.val(0);
				}

				item.change(onFieldAction);
			}
		}

		if (role == "element") {

			if (data.substring(0, 1) == "=") {

				var name = data.substring(1);

				if (m_lock) {

					item.html(name);
				}
				else {

					item.val(name);
				}

				item.closest('.show-me').show();

				if (m_pvalid) {

					for (var k = 0; k < m_person.set.keys.length; k++) {

						var key = m_person.set.keys[k];

						if (name == key.name) {

							var show = expandKey(form.data("type"), key);

							item.attr("data-toggle", "tooltip").attr("title", show);

							break;
						}
					}
				}
			}
			else {

				if (m_pvalid) {

					var show = expandKey(form.data("type"), m_person.set.keys[0]);

					item.attr("data-toggle", "tooltip").attr("title", show);
				}
			}

			item.on("click",
				function () {
					$(this).tooltip("hide");
				});

			if (!m_lock) {

				item.change(onFieldAction);
			}
		}

		if (role == "data") {

			var type = form.data("type");

			if (data.substring(0, 1) != "=") {

				if (data == "") {

					if ((data = type.getDefault()) != "") {

						setPropValue(prop, data);

						setPrevValue(prop, data);
					}
				}

				type.setFieldData(item, data);

				item.closest('.show-me').show();
			}

			type.bindEvents(item);

			showFieldChange(this.id, prop, false);
		}
	}

	if (role == "undo") {

		item.click(onFieldAction);
	}

	if (role == "help") {

		item.click(onFieldAction);
	}

	item.removeClass("bind-field");
}

function bindTable(i) {

	var item = $(this);

	var role = item.data("role");

	var dtab = item.closest("table");

	var prop = dtab.data("prop");

	item.click(onTableAction);

	if (role == "tab-undo") {

		showTableChange(prop, false);
	}

	item.removeClass("bind-table");
}

function onFieldAction() {

	var item = $(this);

	var form = item.closest(".form-group");

	var type = form.data("type");

	var info = form.data("info");

	var role = item.data("role");

	if (role == "source" || role == "element" || role == "data" || role == "undo") {

		var prop = form.data("prop");

		if (role == "source") {

			var ename = "e-" + this.id.substr(2);
			var vname = "v-" + this.id.substr(2);

			var eitem = $("#" + ename);
			var vitem = $("#" + vname);

			if (item.val() == "0") {

				if (vitem.val() == "") {

					vitem.val(type.getDefault(info));
				}

				eitem.closest('.show-me').hide();
				vitem.closest('.show-me').show();

				setPropValue(prop, vitem.val())
			}
			else {

				eitem.closest('.show-me').show();
				vitem.closest('.show-me').hide();

				setPropValue(prop, "=" + eitem.val())
			}
		}

		if (role == "element") {

			setPropValue(prop, "=" + item.val())

			if (m_pvalid) {

				for (var k = 0; k < m_person.set.keys.length; k++) {

					var key = m_person.set.keys[k];

					if (item.val() == key.name) {

						var show = expandKey(type, key);

						item.attr("title", show).tooltip("fixTitle");

						break;
					}
				}
			}
		}

		if (role == "undo") {

			var sname = "s-" + this.id.substr(2);
			var ename = "e-" + this.id.substr(2);
			var vname = "v-" + this.id.substr(2);

			var sitem = $("#" + sname);
			var eitem = $("#" + ename);
			var vitem = $("#" + vname);

			var data = getPrevValue(prop);

			setPropValue(prop, data);

			if (data.substring(0, 1) == '=') {

				sitem.val("1");

				eitem.val(data.substring(1));

				eitem.closest('.show-me').show();
				vitem.closest('.show-me').hide();
			}
			else {

				sitem.val("0");

				if (data == "") {

					data = type.getDefault();
				}

				type.setFieldData(vitem, data);

				eitem.closest('.show-me').hide();
				vitem.closest('.show-me').show();
			}
		}

		if (role == "data") {

			applyChange(item, type, prop);

			return;
		}

		showFieldChange(this.id, prop, true);

		doEnables(item, prop);
	}

	if (role == "help") {

		display("Help for " + info.label, info.help);
	}
}

function applyChange(item, type, prop) {

	m_busy = true;

	var data = type.getFieldData(item);

	if (type.validateData(item, data)) {

		type.onClearError(item, false);

		setPropValue(prop, data);

		type.setFieldData(item, getPropValue(prop));
	}

	m_busy = false;

	showFieldChange(item.attr("id"), prop, true);

	doEnables(item, prop);
}

function onTableAction() {

	var item = $(this);

	var dtab = item.closest("table");

	var prop = dtab.data("prop");

	var role = item.data("role");

	if (role == "row-up" || role == "row-down" || role == "row-delete") {

		var nrow = item.closest("tr").data("nrow");

		if (role == "row-up") {

			var data = getPropTable(prop);

			var drow = data.splice(nrow, 1)[0];

			data.splice(nrow - 1, 0, drow);
		}

		if (role == "row-down") {

			var data = getPropTable(prop);

			var drow = data.splice(nrow, 1)[0];

			data.splice(nrow + 1, 0, drow);
		}

		if (role == "row-delete") {

			var data = getPropTable(prop);

			data.splice(nrow, 1);
		}

		refreshTab(item);

		showTableChange(prop, true);
	}

	if (role == "row-add") {

		var nrow = onAddRow(item, dtab, prop);

		dtab = item.closest("table");

		prop = dtab.data("prop");

		onEditIndex(nrow, dtab, prop, true);
	}

	if (role == "row-edit") {

		onEditRow(item, dtab, prop, false);
	}

	if (role == "row-clear") {

		onClearRow(item, dtab, prop);
	}

	if (role == "tab-undo") {

		var f = function () {

			setPropValue(prop, deepCopy(getPrevTable(prop)));

			refreshTab(item);

			showTableChange(prop, true);
		}

		confirm(
			"REVERT TABLE",
			"Do you really want to revert the changes made to this table?",
			f
		);
	}
}

function onAddRow(item, dtab, prop) {

	var info = dtab.data("info").fields;

	var data = getPropTable(prop);

	var drow = {};

	for (var f = 0; f < info.length; f++) {

		if (info[f].type != "hidden") {

			var type = eval("new CField_" + info[f].type + "(info[f], {})");

			var dval = type.getDefault();

			drow[info[f].name] = dval;
		}
	}

	data.splice(data.length, 0, drow);

	return data.length - 1;
}

function canClearRow(item, dtab, prop) {

	var nrow = item.closest("tr").data("nrow");

	var info = dtab.data("info").fields;

	var full = prop + "." + nrow.toString();

	var data = getPropObject(full);

	for (var f = 0; f < info.length; f++) {

		if (info[f].type != "hidden") {

			var type = eval("new CField_" + info[f].type + "(info[f], {})");

			var dval = type.getDefault();

			if (data[info[f].name] !== dval) {

				return true;
			}
		}
	}

	return false;
}

function onClearRow(item, dtab, prop) {

	var nrow = item.closest("tr").data("nrow");

	var info = dtab.data("info").fields;

	var full = prop + "." + nrow.toString();

	var data = getPropObject(full + ".x");

	for (var f = 0; f < info.length; f++) {

		if (info[f].type != "hidden") {

			var type = eval("new CField_" + info[f].type + "(info[f], {})");

			var dval = type.getDefault();

			data[info[f].name] = dval;
		}
	}

	refreshTab(item);

	showTableChange(prop, true);
}

function onEditRow(item, dtab, prop, add) {

	var nrow = item.closest("tr").data("nrow");

	onEditIndex(nrow, dtab, prop, add);
}

function onEditIndex(nrow, dtab, prop, add) {

	var info = dtab.data("info");

	var full = prop + "." + nrow.toString();

	var data = getPropObject(full + ".x");

	var copy = deepCopy(data);

	$("#editor-label").html((add ? "Add " : (m_lock ? "View " : "Edit ")) + info.label);

	var eb = $("#editor-body").empty();

	var fd = $("<div/>").addClass("form-horizontal no-undo").appendTo(eb);

	eb.data("info", info);

	eb.data("base", full);

	eb.closest(".modal-dialog").css("width", "60%");

	loadSection(fd, info, full);

	bindAll();

	doEnables(fd);

	if (m_lock) {

		var b1 = function () {

			hideModal($("#editor"));

			eb.empty();
		};

		$("#editor-b1").off("click").on("click", b1).html("Close");

		$("#editor-b2").off("click").css("display", "none");

		$("#editor-bx").off("click").on("click", b1);
	}
	else {

		var b1 = function () {

			hideModal($("#editor"));

			if (add) {

				if (info.fields[0].name == "key") {

					var m = 99;

					var t = getPropTable(prop);

					for (var r = 0; r < t.length; r++) {

						if (r != nrow) {

							if ("key" in t[r]) {

								if (parseInt(t[r].key) > m) {

									m = parse(t[r].key);
								}
							}
						}
					}

					t[nrow]["key"] = (m + 1).toString();
				}
			}

			refreshTab(dtab);

			showTableChange(prop, true);

			eb.empty();
		};

		var b2 = function () {

			hideModal($("#editor"));

			if (add) {

				getPropTable(prop).splice(nrow, 1);

				refreshTab(dtab);
			}
			else {

				setPropValue(full, copy);
			}

			showTableChange(prop, true);

			eb.empty();
		};

		$("#editor-b1").off("click").on("click", b1).html("OK");

		$("#editor-b2").off("click").on("click", b2).css("display", "");

		$("#editor-bx").off("click").on("click", b2);
	}

	showModal($("#editor"));
}

function showFieldChange(name, bind, tool) {

	var uname = "u-" + name.substr(2);

	var uitem = $("#" + uname);

	if (getPropValue(bind) == getPrevValue(bind)) {

		if (tool) {

			showConfigChange(bind, false);
		}

		uitem.closest('.show-me').hide();
	}
	else {

		if (tool) {

			showConfigChange(bind, true);
		}

		uitem.closest('.show-me').show();
	}
}

function showTableChange(bind, tool) {

	if (deepCompare(getPropTable(bind), getPrevTable(bind))) {

		if (tool) {

			showConfigChange(bind, false);
		}

		$('#tab-undo').hide();
	}
	else {

		if (tool) {

			showConfigChange(bind, true);
		}

		$('#tab-undo').show();
	}
}

function showConfigChange(bind, dirty) {

	if (!dirty) {

		if (m_dirty) {

			if (deepCompare(m_config, m_previous)) {

				setCurrentNodeDirty(false);

				setConfigDirty(false);
			}
			else {

				var data = getPropObject(bind);

				var prev = getPrevObject(bind);

				if (deepCompare(data, prev)) {

					setCurrentNodeDirty(false);
				}
			}
		}
	}
	else {

		setCurrentNodeDirty(true);

		setConfigDirty(true);
	}
}

function onKillFocus(item) {

	if (!m_busy) {

		var form = item.closest(".form-group");

		var type = form.data("type");

		type.onClearError(item, true);
	}
}

function expandKey(type, key) {

	var show = key.name + " = ";

	var disp = type.getTableText(key.value ? key.value : "");

	if (!!disp && disp != key.value) {

		show += "[" + key.value + "] " + disp;
	}
	else {

		show += key.value;
	}

	return show;
}

// Deep Operations

function deepCopy(obj) {

	return JSON.parse(JSON.stringify(obj));
}

function sortObjByKey(value) {

	return (typeof value === 'object') ?
		(Array.isArray(value) ?
			value.map(sortObjByKey) :
			Object.keys(value).sort().reduce(
				(o, key) => {
					const v = value[key];
					o[key] = sortObjByKey(v);
					return o;
				}, {})
		) :
		value;
}

function deepCompare(o1, o2) {

	var s1 = JSON.stringify(sortObjByKey(o1));

	var s2 = JSON.stringify(sortObjByKey(o2));

	return s1 == s2;
}

// Dirty Flag Management

function setAllNodesDirty(dirty) {

	var tree = $('#tree').data('treeview');

	for (var i = 0; ; i++) {

		var node = tree.getNode(i);

		if (node == null) {

			break;
		}

		setNodeDirty(tree.getNode(i), false);
	}

	tree.selectNode(tree.getSelected()[0]);
}

function setCurrentNodeDirty(dirty) {

	var tree = $('#tree').data('treeview');

	var node = tree.getSelected()[0];

	setNodeDirty(node, dirty);

	tree.selectNode(node.nodeId);
}

function setNodeDirty(node, dirty) {

	var pos = node.text.indexOf("<span");

	if (dirty) {

		if (pos < 0) {

			node.text += "<span id='tb-save' class='pull-right fa fa-edit tree-icon'></span>";
		}
	}
	else {

		if (pos > 0) {

			node.text = node.text.substring(0, pos);
		}
	}
}

function setConfigDirty(dirty) {

	if (m_dirty != dirty) {

		var text = $("#tb-text");
		var save = $("#tb-save");
		var undo = $("#tb-undo");
		var down = $("#tb-down");
		var upld = $("#tb-upld");

		if (!m_lock) {

			text.html(dirty ? "CHANGED" : "UNCHANGED");

			text.css("color", dirty ? "#A00000" : "#00A000");
			save.css("color", dirty ? "#00A000" : "#C0C0C0");
			undo.css("color", dirty ? "#A00000" : "#C0C0C0");
			down.css("color", dirty ? "#C0C0C0" : "#808CC8");
			upld.css("color", dirty ? "#C0C0C0" : "#808CC8");
		}
		else {

			text.html("READ ONLY");

			text.css("color", "#808CC8");
			down.css("color", "#808CC8");
		}

		if (!m_lock) {
			save.css("display", "");
			undo.css("display", "");
			upld.css("display", "");
		}

		down.css("display", "");

		m_dirty = dirty;
	}

	$("#tb-div").show();
}

// Cascading Enables

function doEnables(item, delta) {

	var eb = item.closest("#editor-body");

	if (eb.length) {

		var info = eb.data("info");

		var base = eb.data("base");

		if (delta) {

			delta = delta.substring(base.length + 1) + ",";
		}

		doFieldEnables(delta, info);
	}
	else {

		if (delta) {

			delta = delta.substring(m_uibase.length + 1) + ",";
		}

		if ("tabs" in m_uinode) {

			for (var t = 0; t < m_uinode.tabs.length; t++) {

				var rel = delta;

				var tab = m_uinode.tabs[t];

				if (delta) {

					if ("name" in tab) {

						var dot = delta.indexOf('.');

						if (dot >= 0 && delta.substr(0, dot) != tab.name) {

							continue;
						}

						rel = delta.substr(dot + 1);
					}
				}

				if ("sections" in tab) {

					for (var s = 0; s < tab.sections.length; s++) {

						var sect = tab.sections[s];

						if ("enable" in sect) {

							if (!delta || sect.enable.indexOf(delta) >= 0) {

								var used = evalCode(m_uibase, sect.enable);

								$("#ediv-" + sect.dtag).css("display", used ? "" : "none");

								$("#ddiv-" + sect.dtag).css("display", used ? "none" : "");
							}
						}

						doFieldEnables(rel, sect);
					}
				}
			}
		}
	}
}

function doFieldEnables(delta, sect) {

	if ("fields" in sect) {

		for (var f = 0; f < sect.fields.length; f++) {

			var info = sect.fields[f];

			if ("enable" in info) {

				if (!delta || info.enable.indexOf(delta) >= 0) {

					var sname = "s-" + info.dtag;
					var ename = "e-" + info.dtag;
					var vname = "v-" + info.dtag;

					var sitem = $("#" + sname);
					var eitem = $("#" + ename);
					var vitem = $("#" + vname);

					var form = vitem.closest(".form-group");

					var type = form.data("type");

					var prop = form.data("prop");

					var base = prop.substring(0, prop.lastIndexOf("."));

					if (evalCode(base, info.enable)) {

						type.enableField(vitem, true);

						sitem.removeAttr('disabled');
						eitem.removeAttr('disabled');
					}
					else {

						type.enableField(vitem, false);

						sitem.attr("disabled", true);
						eitem.attr("disabled", true);
					}
				}
			}
			else {

				if (!delta) {

					var vname = "v-" + info.dtag;

					var vitem = $("#" + vname);

					if (vitem.length) {

						var form = vitem.closest(".form-group");

						var type = form.data("type");

						type.enableField(vitem, true);
					}
				}
			}
		}
	}
}

function evalCode(base, code) {

	var list = code.split(',');

	var stack = [];

	for (var i = 0; i < list.length; i++) {

		if (isNumber(list[i])) {

			stack.push(list[i]);

			continue;
		}

		if (list[i] == "==") {

			var b = stack.pop();
			var a = stack.pop();

			stack.push((a.substring(0, 1) == "=" || b.substring(0, 1) == "=" || (parseInt(a) == parseInt(b))) ? 1 : 0);

			continue;
		}

		if (list[i] == "!=") {

			var b = stack.pop();
			var a = stack.pop();

			stack.push((a.substring(0, 1) == "=" || b.substring(0, 1) == "=" || (parseInt(a) != parseInt(b))) ? 1 : 0);

			continue;
		}

		if (list[i] == ">=") {

			var b = stack.pop();
			var a = stack.pop();

			stack.push((a.substring(0, 1) == "=" || b.substring(0, 1) == "=" || (parseInt(a) >= parseInt(b))) ? 1 : 0);

			continue;
		}

		if (list[i] == "&&") {

			var b = stack.pop();
			var a = stack.pop();

			stack.push((a && b) ? 1 : 0);

			continue;
		}

		if (list[i] == "||") {

			var b = stack.pop();
			var a = stack.pop();

			stack.push((a || b) ? 1 : 0);

			continue;
		}

		if (list[i].substring(0, 1) == "~") {

			var name = base + "." + list[i].substring(1);

			stack.push((getPropValue(name) == "") ? 1 : 0);

			continue;
		}

		var name = (list[i].indexOf('.') >= 0) ? list[i] : base + "." + list[i];

		var data = getPropValue(name);

		stack.push((data == "") ? "0" : data);
	}

	if (stack.length == 1) {

		var res = stack.pop();

		if (typeof res == "string") {

			return parseInt(res);
		}

		if (typeof res == "number") {

			return res;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////
//
// Field Base Class
//

class CField {

	constructor(info, prop) {

		this.info = info;
		this.prop = prop;
	}

	loadLocked(div) {

		if (m_lock) {

			var span = $("<span/>").appendTo(div);

			span.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control locked");

			span.data("role", "data");

			return true;
		}

		return false;
	}

	getDefault() {

		return ("default" in this.info) ? this.info.default : "0";
	}

	getPlaceholder() {

		return "Not Defined";
	}

	getTableText(data) {

		if (data && data.length > 40) {

			return data.substr(0, 35) + "...";
		}

		return data;
	}

	validateData(item, data) {

		return true;
	}

	enableField(item, enable) {

		var icon = item.closest("div").find(".field-icon");

		if (enable) {

			if (icon) {

				icon.css("color", "#808CC8");
			}

			item.removeAttr("disabled");
		}
		else {

			if (icon) {

				icon.css("color", "#C0C0C0");
			}

			item.attr("disabled", true);
		}
	}

	setFieldData(item, data) {

		if (m_lock)
			item.html(this.getTableText(data));
		else
			item.val(data);
	}

	getFieldData(item) {

		return item.val();
	}

	bindEvents(item) {

		item.change(onFieldAction);

		item.on('focusout',
			function () {
				onKillFocus($(this));
			});

		item.on('keydown',
			function (e) {
				if (e.key == "Escape") {
					onKillFocus(item);
				}
			});
	}

	onClearError(item, read) {

		if (item.attr("error") == "true") {

			if (read) {

				item.val(getPropValue(this.prop));

				item.select();
			}

			item.attr("error", "false");

			item.css("border", "");
		}
	}

	setError(item) {

		item.attr("error", "true");

		item.css("border", "2px solid red");

		item.select().focus();
	}

	loadWithIcon(div, ip, icon, tip, func) {

		var ig = $("<div/>").addClass("input-group").appendTo(div).append(ip);

		var is = $("<span/>").addClass("input-group-addon").appendTo(ig);

		var i1 = $("<span/>").addClass("field-icon tb-icon fa " + icon).appendTo(is);

		i1.attr("data-toggle", "tooltip").attr("title", tip);

		i1.on("click", function () { func(this); });
	}

	loadWithIcons(div, ip, icon1, tip1, func1, icon2, tip2, func2) {

		var ig = $("<div/>").addClass("input-group").appendTo(div).append(ip);

		var is = $("<span/>").addClass("input-group-addon").appendTo(ig);

		var i1 = $("<span/>").addClass("field-icon tb-icon fa " + icon1).appendTo(is);

		i1.attr("data-toggle", "tooltip").attr("title", tip1);

		i1.on("click", function () { func1(this); });

		var i2 = $("<span/>").addClass("field-icon tb-icon fa " + icon2).appendTo(is);

		i2.attr("data-toggle", "tooltip").attr("title", tip2);

		i2.on("click", function () { func2(this); });
	}

	loadWithLabel(div, ip, text) {

		if (text.length) {

			var ig = $("<div/>").addClass("input-group").appendTo(div).append(ip);

			$("<span/>").addClass("input-group-addon").html(text).appendTo(ig);
		}
		else {

			div.append(ip);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Enumerated Field
//

class CField_enum extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var se = $("<select/>").appendTo(div);

			se.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			se.data("role", "data");

			this.makeLists();

			for (var v = 0; v < this.names.length; v++) {

				var text = this.names[v];

				var data = this.dvals[v];

				var disp = m_pvalid ? "[" + data + "] " + text : text;

				$("<option/>").attr("value", data).html(disp).appendTo(se);
			}
		}
	}

	getTableText(data) {

		if (data.substring(0, 1) == "=") {

			return data;
		}

		this.makeLists();

		for (var v = 0; v < this.names.length; v++) {

			if (data == this.dvals[v]) {

				return this.names[v];
			}
		}

		var dval = this.getDefault();

		return (data == dval) ? "" : this.getTableText(dval);
	}

	makeLists() {

		if (!("names" in this)) {

			this.names = [];

			this.dvals = [];

			var list = this.info.format.split('|');

			for (var v = 0; v < list.length; v++) {

				var text = list[v];

				if (text.substr(0, 1) == "<") {

					var path = text.substr(1, text.length - 2);

					var refs = getObject(m_config, path + ".0", false);

					if (refs) {

						var done = {};

						for (var r = 0; r < refs.length; r++) {

							var row = refs[r];

							if ("name" in row && "key" in row) {

								if (!(row.name in done)) {

									this.names.push(row.name);

									this.dvals.push(row.key);

									done[row.name] = 1;
								}
							}
						}
					}
				}
				else {

					var data = v.toString();

					var epos = 0;

					if ((epos = text.indexOf("=")) > 0) {

						data = text.substring(0, epos);

						text = text.substring(epos + 1);
					}

					this.names.push(text);

					this.dvals.push(data);
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Multi-Select Field
//

class CField_multi extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var se = $("<select/>").appendTo(div);

			se.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			se.prop("multiple", "multiple");

			se.data("role", "data");

			var list = this.info.format.split('|');

			for (var v = 1; v < list.length; v++) {

				var data = (1 << (v - 1)).toString();

				var text = list[v];

				var disp = m_pvalid ? "[" + data + "] " + text : text;

				$("<option/>").attr("value", data).html(disp).appendTo(se);
			}

			var button = function (o, s) {

				if (o.length) {

					var used = [];

					for (var i = 0; i < o.length; i++) {

						used.push(o[i].text.split(' ')[0]);
					}

					return used.join(", ");
				}

				return list[0];
			};

			se.multiselect({
				nonSelectedText: list[0],
				allSelectedText: false,
				buttonWidth: "100%",
				numberDisplayed: 0,
				buttonText: button
			});
		}
	}

	enableField(item, enable) {

		if (m_lock) {

			super.enableField(item, enable);
		}
		else {

			item.multiselect(enable ? "enable" : "disable");
		}
	}

	setFieldData(item, data) {

		if (m_lock) {

			item.html(this.getTableText(data));
		}
		else {

			item.multiselect("deselectAll", false);

			var n = parseInt(data);

			for (var i = 1; n; i <<= 1) {

				if (n & i) {

					item.multiselect("select", i.toString());

					n ^= i;
				}
			}

			item.multiselect("updateButtonText");
		}
	}

	getFieldData(item) {

		var v = item.val();

		var n = 0;

		for (var i = 0; i < v.length; i++) {

			n |= parseInt(v[i]);
		}

		return n.toString();
	}

	getTableText(data) {

		if (data.substring(0, 1) == "=") {

			return data;
		}

		var mask = parseInt(data);

		var list = this.info.format.split('|');

		if (mask) {

			var used = [];

			for (var v = 1; v < list.length; v++) {

				if (mask & (1 << (v - 1))) {

					used.push(list[v]);
				}
			}

			return used.join(", ");
		}

		return list[0];
	}
}

//////////////////////////////////////////////////////////////////////////
//
// IP Address Field
//

class CField_ip extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>").appendTo(div);

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			var list = this.info.format.split(',');

			if (list.length >= 2) {

				if (list[0] == "optional") {

					ip.attr("placeholder", list[1]);
				}
			}

			ip.attr("autocomplete", "off").attr("data-lpignore", "true");

			ip.data("role", "data");
		}
	}

	getDefault() {

		var list = this.info.format.split(',');

		if (list.length >= 2) {

			if (list[0] != "optional") {

				return list[1];
			}
		}

		return "";
	}

	getPlaceholder() {

		var list = this.info.format.split(',');

		if (list.length >= 2) {

			if (list[0] == "optional") {

				return list[1];
			}
		}

		return super.getPlaceholder();
	}

	getFieldData(item) {

		var data = item.val();

		if (isNumber(data)) {

			var list = this.info.format.split(',');

			if (list[0] == "mask" || list[0] == "rmask") {

				var bits = parseInt(data);

				if (bits >= 0 && bits <= 32) {

					bits = ((0xFFFFFFFF << (32 - bits)) >>> 0);

					var b1 = ((bits >>> 24) & 0xFF).toString();
					var b2 = ((bits >>> 16) & 0xFF).toString();
					var b3 = ((bits >>> 8) & 0xFF).toString();
					var b4 = ((bits >>> 0) & 0xFF).toString();

					return b1 + "." + b2 + "." + b3 + "." + b4;
				}
			}
		}

		return data;
	}

	validateData(item, data) {

		var list = this.info.format.split(',');

		if (data == "") {

			if (list.length < 1 || list[0] != "optional") {

				if (list.length >= 2 && list[1] == "0.0.0.0") {

					this.showBadAddress(item, true);
				}
				else {

					this.showBadAddress(item);
				}

				return false;
			}
		}
		else {

			var text = data.split('.');

			var bits = 0;

			if (text.length == 4) {

				for (var v = 0; v < 4; v++) {

					var b;

					if (!isNumber(text[v]) || (b = parseInt(text[v])) > 255) {

						this.showBadAddress(item);

						return false;
					}

					bits = ((bits << 8) >>> 0) + b;
				}
			}
			else {

				this.showBadAddress(item);

				return false;
			}

			if (list.length >= 1) {

				if (list[0] == "mask" || list[0] == "rmask") {

					var okay = false;

					if (bits != 0x00000000 && bits != 0xFFFFFFFF) {

						for (var mask = (1 << 31) >>> 0; mask; mask >>>= 1) {

							if (!(bits & mask)) {

								if (!(bits & (mask - 1))) {

									okay = true;
								}

								break;
							}
						}
					}
					else {

						if (list[0] == "rmask") {

							okay = true;
						}
					}

					if (!okay) {

						this.showBadMask(item);

						return false;
					}
				}
			}
		}

		item.select();

		return true;
	}

	showBadAddress(item, zero) {

		var bind = this;

		var text = "This field must contain a valid IP address.";

		if (!!zero) {

			text += "<br/><br/>";

			text += "To disable the associated feature, enter 0.0.0.0";
		}

		display("Invalid Data", text, function () { bind.setError(item) });
	}

	showBadMask(item) {

		var bind = this;

		var text = "This field must contain a valid network mask.";

		display("Invalid Data", text, function () { bind.setError(item) });
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Numeric Field
//

class CField_number extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>");

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			ip.attr("autocomplete", "off").attr("data-lpignore", "true");

			ip.attr("placeholder", this.getPlaceholder());

			ip.attr("type", "number");

			var list = this.info.format.split(',');

			if (list.length >= 3) {

				ip.attr("min", parseInt(list[1]));

				ip.attr("max", parseInt(list[2]));
			}

			ip.data("role", "data");

			this.loadWithLabel(div, ip, this.getUnits());
		}
	}

	getDefault() {

		var list = this.info.format.split(',');

		if (list.length >= 1) {

			return list[0];
		}

		return "0";
	}

	getPlaceholder() {

		var list = this.info.format.split(',');

		if (list.length >= 5) {

			return list[4];
		}

		return "";
	}

	getTableText(data) {

		var list = this.info.format.split(',');

		if (list.length >= 5) {

			if (data == list[0]) {

				return list[4];
			}
		}

		return data;
	}

	getUnits() {

		var list = this.info.format.split(',');

		if (list.length >= 4) {

			return list[3];
		}

		return "";
	}

	setFieldData(item, data) {

		var list = this.info.format.split(',');

		if (list.length >= 5) {

			if (data == list[0]) {

				data = list[4];
			}
		}

		if (m_lock) {

			item.html(this.getTableText(data));
		}
		else {

			item.val(data);
		}
	}

	validateData(item, data) {

		var list = this.info.format.split(',');

		if (list.length >= 5) {

			if (!data.length) {

				data = list[0];
			}
		}

		if (!isNumber(data)) {

			var bind = this;

			var text = "This field must contain an integer.";

			display("Invalid Data", text, function () { bind.setError(item) });

			return false;
		}

		if (list.length >= 3) {

			if (parseInt(data) < parseInt(list[1])) {

				this.showRange(item, list);

				return false;
			}

			if (parseInt(data) > parseInt(list[2])) {

				this.showRange(item, list);

				return false;
			}
		}

		item.select();

		return true;
	}

	showRange(item, list) {

		var bind = this;

		var text = "This field must contain a value between " + list[1] + " and " + list[2] + ".";

		display("Value Out of Range", text, function () { bind.setError(item) });
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Float Field
//

class CField_float extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>").appendTo(div);

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control no-spinners");

			ip.attr("autocomplete", "off").attr("data-lpignore", "true");

			ip.attr("type", "number").attr("step", "any");

			ip.data("role", "data");
		}
	}

	getDefault() {

		var list = this.info.format.split(',');

		if (list.length >= 1) {

			return list[0];
		}

		return "0";
	}

	validateData(item, data) {

		var list = this.info.format.split(',');

		if (!isFloat(data)) {

			var bind = this;

			var text = "This field must contain a number.";

			display("Invalid Data", text, function () { bind.setError(item) });

			return false;
		}

		if (list.length >= 3) {

			if (parseFloat(data) < parseFloat(list[1])) {

				this.showRange(item, list);

				return false;
			}

			if (parseFloat(data) > parseFloat(list[2])) {

				this.showRange(item, list);

				return false;
			}
		}

		item.select();

		return true;
	}

	showRange(item, list) {

		var bind = this;

		var text = "This field must contain a value between " + list[1] + " and " + list[2] + ".";

		display("Value Out of Range", text, function () { bind.setError(item) });
	}
}

//////////////////////////////////////////////////////////////////////////
//
// String Field
//

class CField_string extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>");

			if ("prompt" in this.info) {

				var ig = $("<div/>").addClass("input-group").appendTo(div);

				ip.appendTo(ig);

				var bg = $("<div/>").addClass("input-group-btn").appendTo(ig);

				var b1 = $("<button/>").attr("type", "button").addClass("btn btn-default dropdown-toggle").attr("data-toggle", "dropdown").appendTo(bg);

				$("<span/>").addClass("caret").appendTo(b1);

				$("<span/>").addClass("sr-only").appendTo(b1);

				var ul = $("<ul/>").addClass("dropdown-menu").appendTo(bg);

				var list = this.info.prompt.split('|');

				var bind = this;

				for (var v = 0; v < list.length; v++) {

					var sk = list[v].split(',');

					var sm = $("<li/>").addClass("dropdown-submenu").appendTo(ul);

					$("<a/>").attr("href", "#").html(sk[0]).appendTo(sm);

					var sl = $("<ul/>").addClass("dropdown-menu").appendTo(sm);

					for (var s = 1; s < sk.length; s++) {

						var si = $("<li/>").appendTo(sl);

						var fn = function () {

							ip.val(this.text);

							applyChange(ip, bind, bind.prop);
						};

						$("<a/>").attr("href", "#").on("click", fn).html(sk[s]).appendTo(si);
					}
				}
			}
			else {

				ip.appendTo(div);
			}

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			ip.attr("placeholder", this.getPlaceholder());

			ip.attr("autocomplete", "off").attr("data-lpignore", "true");

			ip.data("role", "data");
		}
	}

	getDefault() {

		return "";
	}

	getPlaceholder() {

		var list = this.info.format.split(',');

		if (list.length >= 2) {

			return list[1];
		}

		return super.getPlaceholder();
	}

	validateData(item, data) {

		// TODO -- Validate based on format type (any,hostname,etc.)

		return true;
	}
};

//////////////////////////////////////////////////////////////////////////
//
// Password Field
//

class CField_password extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>");

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			ip.attr("placeholder", this.getPlaceholder());

			ip.attr("data-lpignore", "true").attr("autocomplete", "off");

			ip.attr("type", "password");

			ip.data("role", "data");

			var bind = this;

			var func = function (item) { bind.onShowPassword(item); };

			if ("mkpass" in this.info) {

				var make = function () { bind.onMakePassword(); };

				this.loadWithIcons(div, ip, "fa-cog", "Generate", make, "fa-lock", "Show", func);
			}
			else {

				this.loadWithIcon(div, ip, "fa-lock", "Show", func);
			}
		}
	}

	getDefault() {

		return "";
	}

	getPlaceholder() {

		var list = this.info.format.split(',');

		if (list.length == 1) {

			return list[0];
		}

		if (list.length >= 2) {

			return list[1];
		}

		return super.getPlaceholder();
	}

	enableField(item, enable) {

		var icon = item.closest("div").find(".field-icon");

		if (enable) {

			if (item.attr("type") == "password") {

				icon.css("color", "#808CC8");
			}
			else {

				icon.css("color", "#C00000");
			}

			item.removeAttr("disabled");
		}
		else {

			icon.css("color", "#C0C0C0");

			item.attr("disabled", true);

			item.attr("type", "password");
		}
	}

	validateData(item, data) {

		var list = this.info.format.split(',');

		if (list.length >= 2) {

			if (isNumber(list[0])) {

				if (data.length < parseInt(list[0])) {

					this.showBadPassword(item, "The password must be at least " + list[0] + " characters.");

					return false;
				}

				return true;
			}

			if (list[0] == "general") {

				return true;
			}

			if (list[0] == "gre") {

				if (data.length > 0) {

					if (!isNumber(data)) {

						var good = true;

						var text = data.split('.');

						if (text.length == 4) {

							for (var v = 0; v < 4; v++) {

								var b;

								if (!isNumber(text[v]) || (b = parseInt(text[v])) > 255) {

									good = false;

									break;
								}
							}
						}
						else {

							good = false;
						}

						if (!good) {

							this.showBadPassword(item, "GRE keys must be numbers or dotted-decimal IP addresses.");

							return false;
						}
					}
				}

				return true;
			}

			if (list[0] == "pin") {

				if (data.length > 0 && (data.length < 4 || data.length > 8)) {

					this.showBadPassword(item, "SIM PINs must be between 4 and 8 characters.");

					return false;
				}

				if (data.length > 0 && !isNumber(data)) {

					this.showBadPassword(item, "SIM PINs must comprised numeric characters.");

					return false;
				}

				return true;
			}

			if (list[0] == "wifi") {

				if (data.length > 0 && (data.length < 8 || data.length > 63)) {

					this.showBadPassword(item, "Wi-Fi passwords must be between 8 and 63 characters.");

					return false;
				}

				return true;
			}
		}

		return true;
	}

	onShowPassword(item) {

		var i1 = $(item);

		var ip = i1.closest("div").find("input");

		if (!ip.attr("disabled")) {

			if (ip.attr("type") == "password") {

				i1.removeClass("fa-lock").addClass("fa-unlock-alt");

				i1.css("color", "#C00000");

				ip.removeAttr("type");
			}
			else {

				i1.removeClass("fa-unlock-alt").addClass("fa-lock");

				i1.css("color", "#808CC8");

				ip.attr("type", "password");
			}
		}
	}

	onMakePassword() {

		var bind = this;

		var item = $("#v-" + bind.info.dtag);

		var exec = function () {

			showWait();

			var okay = function (data) {

				hideWait();

				item.val(data);

				applyChange(item, bind, bind.prop);
			};

			var fail = function () {

				hideWait();

				display(lab, "Unable to complete request.");
			};

			var url = "/ajax/mkpass.ajax";

			$.get({
				url: makeAjax(url),
				success: okay,
				error: fail,
				timeout: 10000,
				xhrFields: { withCredentials: true }
			});
		};

		var lab = "GENERATE PASSWORD";

		var msg = "";

		msg += "Any existing password will be replaced.<br/><br/>";

		msg += "Do you want to proceed?";

		confirm(lab, msg, exec);
	}

	showBadPassword(item, text) {

		var bind = this;

		display("Invalid Data", text, function () { bind.setError(item) });
	}

};

//////////////////////////////////////////////////////////////////////////
//
// SSID Field
//

class CField_ssid extends CField {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>");

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control");

			ip.attr("placeholder", this.getPlaceholder());

			ip.attr("data-lpignore", "true").attr("autocomplete", "off");

			ip.data("role", "data");

			var bind = this;

			var func = function (item) { bind.onScanNetwork(item); };

			this.loadWithIcon(div, ip, "fa-wifi", "Scan", func);
		}

		this.busy = false;
	}

	getDefault() {

		return "";
	}

	getPlaceholder() {

		var list = this.info.format.split(',');

		if (list.length >= 1) {

			return list[0];
		}

		return super.getPlaceholder();
	}

	validateData(item, data) {

		return true;
	}

	onScanNetwork(item) {

		if (!this.busy) {

			var ip = $(item).closest("div").find("input");

			if (!ip.attr("disabled")) {

				var prop = this.prop;

				var base = prop.substring(0, prop.lastIndexOf("."));

				if (evalCode(base, this.info.scan)) {

					var list = this.info.format.split(',');

					var face = "auto";

					if (list.length >= 2) {

						face = list[1];
					}

					this.busy = true;

					var bind = this;

					var warn = "Ensure Station mode is selected and committed, and try again in a minute..."

					var path = "/ajax/wifi-scan.ajax?name=" + face;

					var fail = function () {

						hideWait();

						bind.busy = false;

						display("WI-FI SCAN FAILED", warn);
					}

					var okay = function (data) {

						hideWait();

						var bx = function () {

							hideModal($('#wifi'));
						};

						$("#wifi-cx").off("click").on("click", bx);

						$("#wifi-bx").off("click").on("click", bx);

						var body = $("#wifi-body");

						body.empty();

						if (data.scan.length) {

							var fn = function (button) {

								ip.val($(button).data("text"));

								applyChange(ip, bind, bind.prop);

								hideModal($('#wifi'));
							};

							var tt = $("<table/>").appendTo(body);

							tt.addClass("table table-striped table-bordered").css("width", "0%");

							var tb = $("<tbody/>").appendTo(tt);

							for (var i = 0; i < data.scan.length; i++) {

								var tr = $("<tr/>").appendTo(tb);

								var c1 = $("<td/>").css("min-width", "200px").appendTo(tr);

								var cv = $("<canvas width='24px' height='16px' style='background: transparent; margin-top: 8px; margin-right: 8px'/>").appendTo(c1);

								var ss = data.scan[i].ssid;

								$("<span/>").html(ss).appendTo(c1);

								var c2 = $("<td/>").appendTo(tr);

								var b1 = $("<button/>").attr("type", "button").addClass("btn btn-secondary").html("Select").appendTo(c2);

								b1.data("text", ss);

								b1.on("click", function () { fn(this); });

								drawWiFi(cv[0], data.scan[i].bars);
							}
						}
						else {

							body.html("<p><b>No Networks Found.</b></p><p>" + warn + "</p>");
						}

						showModal($('#wifi'));

						bind.busy = false;
					}

					showWait();

					$.get({
						url: makeAjax(path),
						success: okay,
						error: fail,
						xhrFields: { withCredentials: true }
					});
				}
			}
		}
	}
};

//////////////////////////////////////////////////////////////////////////
//
// Lat-Long Field
//

class CField_gps extends CField_float {

	loadField(div) {

		if (!this.loadLocked(div)) {

			var ip = $("<input/>");

			ip.attr("id", "v-" + this.info.dtag).addClass("bind-field form-tight form-control no-spinners");

			ip.attr("autocomplete", "off").attr("data-lpignore", "true");

			ip.attr("type", "number").attr("step", "any");

			ip.data("role", "data");

			var list = this.info.format.split(',');

			var bind = this;

			var loc1 = function (item) { bind.onFindBrowser(item); };

			if (list.length > 1 && list[1] == "ref") {

				var loc2 = function (item) { bind.onFindUnit(item); };

				this.loadWithIcons(div, ip, "fa-home", "Browser Location", loc1, "fa-map-marker", "Device Location", loc2);
			}
			else {

				this.loadWithIcon(div, ip, "fa-home", "Browser Location", loc1);
			}
		}
	}

	getDefault() {

		return "0";
	}

	onFindUnit(item) {

		if ('gps' in m_status_data) {

			if ('okay' in m_status_data.gps && m_status_data.gps.okay) {

				if ('lat' in m_status_data.gps && 'long' in m_status_data.gps) {

					var i1 = $(item);

					var ip = i1.closest("div").find("input");

					var list = this.info.format.split(',');

					if (list.length > 0) {

						if (list[0] == "lat") {

							ip.val(m_status_data.gps.lat);
						}

						if (list[0] == "long") {

							ip.val(m_status_data.gps.long);
						}

						applyChange(ip, this, this.prop);
					}

					return;
				}
			}
		}

		display("SET TO DEVICE LOCATION", "No location information is available from the device.");
	}

	onFindBrowser(item) {

		if (navigator.geolocation) {

			var i1 = $(item);

			var ip = i1.closest("div").find("input");

			var bind = this;

			var read = function (pos) {

				var list = bind.info.format.split(',');

				if (list.length > 0) {

					if (list[0] == "lat") {

						ip.val(pos.coords.latitude);
					}

					if (list[0] == "long") {

						ip.val(pos.coords.longitude);
					}

					applyChange(ip, bind, bind.prop);
				}
			};

			navigator.geolocation.getCurrentPosition(read);
		}
		else {

			display("SET TO BROWSER LOCATION", "This function is not available in this browser.");
		}
	}
};

//////////////////////////////////////////////////////////////////////////
//
// Note Field
//

class CField_note extends CField {

	loadField(div) {

		var p = $("<p/>").appendTo(div);

		p.attr("id", "v-" + this.info.dtag);

		p.html(this.info.format);
	}
};

//////////////////////////////////////////////////////////////////////////
//
// File Field
//

class CField_file extends CField {

	loadField(div) {

		var span = $("<span/>").appendTo(div);

		span.attr("id", "v-" + this.info.dtag).addClass("file-field bind-field form-tight form-control");

		span.data("role", "data");

		$("<span/>").addClass("file-icon fa fa-file-o").appendTo(span);

		$("<span/>").addClass("file-data").appendTo(span);

		$("<span/>").addClass("ff-icons pull-right").appendTo(span);

		if (m_lock) {

			span.addClass("locked");
		}
		else {

			this.addDragAndDrop(span);
		}
	}

	getTableText(data) {

		if (data == "") {

			return "Not Defined";
		}

		var list = data.split('|');

		return list[0];
	}

	setFieldData(item, data) {

		if (data == "") {

			var text = m_lock ? "Not Defined" : "<i>Drop Here or Click Upload</i>";

			item.find(".file-data").html(text).css("color", "#808080");

			item.find(".file-icon").css("color", "#C0C0C0");

			this["file"] = "";
			this["data"] = "";
		}
		else {

			var list = data.split('|');

			item.find(".file-data").text(list[0]).css("color", "");

			item.find(".file-icon").css("color", "");

			this["file"] = list[0];
			this["data"] = list[1];
		}

		var icons = item.find(".ff-icons");

		this.loadIcons(icons, true);
	}

	getFieldData(item) {

		if ("file" in this && this.file != "") {

			if ("data" in this && this.data != "") {

				return this.file + "|" + this.data;
			}
		}

		return "";
	}

	enableField(item, enable) {

		if (enable) {

			this.addDragAndDrop(item);

			item.removeAttr("disabled");
		}
		else {

			this.removeDragAndDrop(item);

			item.attr("disabled", true);
		}

		var icons = item.find(".ff-icons");

		this.loadIcons(icons, enable);
	}

	getDefault() {

		return "";
	}

	loadIcons(icons, enable) {

		var bind = this;

		icons.empty();

		if (m_lock) {

			var i1 = $("<span/>").addClass("tb-icon fa fa-arrow-down").appendTo(icons);

			if (enable && "data" in this && this.data != "") {

				i1.attr("data-toggle", "tooltip").attr("title", "Download");

				i1.css("color", "#808CC8");

				i1.on("click", function () { bind.onDownload(); });
			}
			else {

				i1.css("color", "#C0C0C0");
			}
		}
		else {

			if (enable) {

				if ("mkcert" in this.info) {

					var base = this.prop.substring(0, this.prop.lastIndexOf("."));

					var list = this.info.mkcert.split('|');

					if (evalCode(base, list[1])) {

						var i0 = $("<span/>").addClass("tb-icon fa fa-cog").appendTo(icons);

						i0.attr("data-toggle", "tooltip").attr("title", "Generate");

						i0.css("color", "#00A000");

						i0.on("click", function () { bind.onGenerate(list[0]); });
					}
				}
			}

			var i1 = $("<span/>").addClass("tb-icon fa fa-arrow-up").appendTo(icons);

			var i2 = $("<span/>").addClass("tb-icon fa fa-arrow-down").appendTo(icons);

			var i3 = $("<span/>").addClass("tb-icon fa fa-times-circle").appendTo(icons);

			if (enable) {

				i1.attr("data-toggle", "tooltip").attr("title", "Upload");

				i1.css("color", "#808CC8");

				i1.on("click", function () { bind.onUpload(); });
			}
			else {

				i1.css("color", "#C0C0C0");
			}

			if (enable && "data" in this && this.data != "") {

				i2.attr("data-toggle", "tooltip").attr("title", "Download");

				i2.css("color", "#808CC8");

				i2.on("click", function () { bind.onDownload(); });

				i3.attr("data-toggle", "tooltip").attr("title", "Clear");

				i3.css("color", "#A00000");

				i3.on("click", function () { bind.onClear(); });
			}
			else {

				i2.css("color", "#C0C0C0");

				i3.css("color", "#C0C0C0");
			}
		}

		icons.find("[data-toggle='tooltip']").tooltip({ trigger: 'hover' });
	}

	removeDragAndDrop(span) {

		span.off("dragenter");
		span.off("dragleave");
		span.off("dragover");
		span.off("drop");
	}

	addDragAndDrop(span) {

		this.removeDragAndDrop(span);

		var bind = this;

		var refs = 0;

		span.on('dragenter', function (e) {
			e.stopPropagation();
			e.preventDefault();
			if (refs++ == 0) {
				$(this).addClass("drag-enter");
			}
		});

		span.on('dragleave', function (e) {
			e.stopPropagation();
			e.preventDefault();
			if (--refs == 0) {
				$(this).removeClass("drag-enter");
			}
		});

		span.on('dragover', function (e) {
			e.stopPropagation();
			e.preventDefault();
		});

		span.on('drop', function (e) {
			e.stopPropagation();
			e.preventDefault();
			$(this).removeClass("drag-enter");
			refs = 0;
			var list = e.originalEvent.dataTransfer.files;
			var file = list[0];
			bind.onReadFile(file);
		});
	}

	onUpload() {

		var bind = this;

		var hid = $("#hidden");

		hid.empty();

		var sel = $("<input/>").appendTo(hid);

		sel.attr("type", "file").attr("accept", this.info.format);

		sel.on("input", function (e) {

			var file = this.files[0];

			bind.onReadFile(file);
		});

		sel.click();
	}

	onReadFile(file) {

		var bind = this;

		var item = $("#v-" + this.info.dtag);

		var read = new FileReader();

		read.onerror = function (e) {

			display("UPLOAD FAILED", "Unable to read from file.");
		};

		read.onload = function (e) {

			var text = this.result;

			var save = function () {

				bind["file"] = file.name;

				bind["data"] = text;

				applyChange(item, bind, bind.prop);
			};

			if ("data" in bind && bind.data != "") {

				confirm("UPLOAD FILE",
					"Do you really want to overwrite the current file?",
					save
				);
			}
			else {

				save();
			}
		};

		read.readAsDataURL(file);
	}

	saveData(data, file) {

		var hid = $("#hidden");

		hid.empty();

		var act = $("<a/>").appendTo(hid);

		act.attr("href", data).attr("download", file);

		m_nowarn = true;

		act[0].click();

		m_nowarn = false;
	}

	onDownload() {

		var bind = this;

		var item = $("#v-" + this.info.dtag);

		var saveNormal = function () {

			bind.saveData(bind.data, bind.file);
		};

		var saveDecrypt = function (pass) {

			var failDecrypt = function () {

				display("DOWNLOAD KEY", "Failed to decrypt key.");
			};

			var okayDecrypt = function (r) {

				if (("okay" in r) && r.okay) {

					if ("key" in r) {

						bind.saveData(r.key, bind.file);

						return;
					}
				}

				failDecrypt();
			};

			var req = {};

			req["key"] = bind.data;

			req["pass"] = pass;

			var url = "/ajax/decrypt.ajax";

			$.post({
				url: makeAjax(url),
				data: JSON.stringify(req),
				success: okayDecrypt,
				error: failDecrypt,
				dataType: 'json',
				contentType: 'application/json',
				xhrFields: { withCredentials: true }
			});
		};

		if ("keypass" in this.info) {

			var item2 = $("#v-" + (parseInt(bind.info.dtag) + 1));

			var bind2 = item2.closest(".form-group").data("type");

			var kpass = bind2.getFieldData(item2);

			if (kpass.length) {

				var action = function (r) {

					if (r == 1) {

						saveDecrypt(kpass);
					}

					if (r == 2) {

						saveNormal();
					}
				};

				yesNoCancel("DOWNLOAD KEY", "Do you want to decrypt the private key before downloading?", action);
			}
			else {

				saveNormal();
			}
		}
		else {

			saveNormal();
		}
	}

	onClear() {

		var bind = this;

		var item = $("#v-" + this.info.dtag);

		var save = function () {

			bind["file"] = "";

			bind["data"] = "";

			applyChange(item, bind, bind.prop);
		};

		confirm("CLEAR FILE",
			"Do you really want to clear the current file?",
			save
		);
	}

	onGenerate(type) {

		var bind1 = this;

		var item1 = $("#v-" + this.info.dtag);

		var exec = function () {

			showWait();

			var okay = function (data) {

				hideWait();

				if (type == "ovs") {

					bind1["file"] = data["name"] + ".key";

					bind1["data"] = data["priv"];

					applyChange(item1, bind1, bind1.prop);
				}

				if (type.substr(0, 4) == "x509") {

					var item2 = $("#v-" + (parseInt(bind1.info.dtag) + 1));

					var bind2 = item2.closest(".form-group").data("type");

					bind1["file"] = data["name"] + ".crt";

					bind1["data"] = data["cert"];

					bind2["file"] = data["name"] + ".key";

					bind2["data"] = data["priv"];

					applyChange(item1, bind1, bind1.prop);

					applyChange(item2, bind2, bind2.prop);

					if (type == "x509") {

						var item3 = $("#v-" + (parseInt(bind1.info.dtag) + 2));

						var bind3 = item3.closest(".form-group").data("type");

						bind3.setFieldData(item3, data["pass"]);

						applyChange(item3, bind3, bind3.prop);
					}
				}
			};

			var fail = function () {

				hideWait();

				display(lab, "Unable to complete request.");
			};

			var url = "/ajax/mkcert.ajax?type=" + type;

			$.get({
				url: makeAjax(url),
				success: okay,
				error: fail,
				timeout: 10000,
				xhrFields: { withCredentials: true }
			});
		};

		var lab = "GENERATE CERTIFICATE";

		var msg = "";

		if (type.substr(0, 4) == "x509") {

			msg += "This will generate a 1024-bit self-signed certificate for testing purposes.<br/><br/>";

			msg += "<b>The certificate may not be secure enough for production deployment.</b><br/><br/>";

			if (type == "x509") {

				msg += "Your current certificate, private key and password will be replaced.<br/><br/>";
			}
			else {

				msg += "Your current certificate and private key will be replaced.<br/><br/>";
			}
		}
		else {

			lab = "GENERATE SECRET";

			msg += "This will generate a 2048-bit static secret for authenticaion purposes.<br/><br/>";

			msg += "Your current secret will be replaced.<br/><br/>";
		}

		msg += "Do you want to proceed?";

		confirm(lab, msg, exec);
	}
};

//////////////////////////////////////////////////////////////////////////
//
// Classifiers
//

function isNumber(s) {

	return /^\d+$/.test(s);
}

function isFloat(s) {

	return /^[-]?\d+(\.\d+)?$/.test(s);
}

// End of File
