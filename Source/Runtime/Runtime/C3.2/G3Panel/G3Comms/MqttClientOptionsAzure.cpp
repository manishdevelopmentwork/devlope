
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsAzure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Matrix Cyrptography
//

#if !defined(AEON_ENVIRONMENT)

#include "../../../drive/extra/matrix/crypto.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS Options
//

// Constructor

CMqttClientOptionsAzure::CMqttClientOptionsAzure(void)
{
}

// Initialization

void CMqttClientOptionsAzure::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	m_Twin   = GetByte(pData);

	GetCoded(pData, m_Pub);

	GetCoded(pData, m_Sub);

	GetCoded(pData, m_Key);
}

// Config Fixup

BOOL CMqttClientOptionsAzure::FixConfig(void)
{
	FixCoded(m_Pub, FALSE);

	FixCoded(m_Sub, FALSE);

	FixCoded(m_Key, FALSE);

	if( m_Pub.IsEmpty() ) {

		m_Pub = "events";
	}

	if( m_Sub.IsEmpty() ) {

		m_Sub = "devicebound";
	}

	return CMqttClientOptionsJson::FixConfig();
}

// Attributes

CString CMqttClientOptionsAzure::GetExtra(void) const
{
	return m_ClientId;
}

// Operations

void CMqttClientOptionsAzure::MakeCredentials(UINT uMins)
{
	timeval t;

	gettimeofday(&t, NULL);

	CPrintf se = CPrintf("%u", t.tv_sec + uMins * 60);

	m_UserName = CString(GetPeerName()) + "/" + m_ClientId + "/api-version=2016-11-14";

	m_Password = MakeSas(se);
}

// Implementation

CString CMqttClientOptionsAzure::MakeSas(CString Time)
{
	UINT  uKey = CHttpBase64::GetDecodeSize(m_Key);

	PBYTE pKey = PBYTE(alloca(uKey));

	CHttpBase64::Decode(pKey, m_Key);

	CString UriRaw = CString(GetPeerName()) + "/devices/" + m_ClientId;

	CString UriEnc = CHttpUrlEncoding::Encode(UriRaw);

	CString SigEnc = GetHmac(pKey, uKey, UriEnc + "\n" + Time);

	return "SharedAccessSignature sr=" + UriEnc + "&sig=" + SigEnc + "&se=" + Time;
}

CString CMqttClientOptionsAzure::GetHmac(PCBYTE pKey, UINT uKey, CString const &Data)
{
	#if defined(AEON_ENVIRONMENT)

	ICryptoHmac *pHmac = NULL;

	AfxNewObject("hmac-sha256", ICryptoHmac, pHmac);

	if( pHmac ) {

		pHmac->Initialize(pKey, uKey);

		pHmac->Update(Data);

		pHmac->Finalize();

		CString Sig = pHmac->GetHashString(hashUrlEnc);

		pHmac->Release();

		return Sig;
	}

	AfxAssert(FALSE);

	return "";

	#else

	BYTE bHash[SHA256_HASH_SIZE];

	psHmacSha256_t Ctx;

	psHmacSha256Init(&Ctx, pKey, uKey);

	psHmacSha256Update(&Ctx, PCBYTE(PCTXT(Data)), Data.GetLength());

	psHmacSha256Final(&Ctx, bHash);

	return CHttpUrlEncoding::Encode(CHttpBase64::Encode(bHash, sizeof(bHash)), TRUE);

	#endif
	}

// End of File
