
#include "Intern.hpp"

#include "HttpServerNtlmAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Ntlm Authentication Method
//

// Constructor

CHttpServerNtlmAuth::CHttpServerNtlmAuth(CString Realm, UINT uCount)
{
	}

// Destructor

CHttpServerNtlmAuth::~CHttpServerNtlmAuth(void)
{
	}

// Operations

BOOL CHttpServerNtlmAuth::CanAccept(CString Meth)
{
	return Meth == "NTLM";
	}

CString CHttpServerNtlmAuth::GetAuthHeader(CString Opaque, BOOL fStale)
{
	return "";
	}

// End of File
