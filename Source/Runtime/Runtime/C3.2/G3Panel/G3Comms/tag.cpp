
#include "intern.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Data Tag Root
//

// Class Creation

CTag * CTag::MakeObject(UINT uType)
{
	switch( uType ) {

		case 1: return New CTagFolder;
		case 2: return New CTagNumeric;
		case 3: return New CTagFlag;
		case 4: return New CTagString;
		case 5: return New CTagSimple;
		}

	return NULL;
	}

// Property Helpers

UINT CTag::GetPropCount(void)
{
	return 17;
	}

DWORD CTag::GetDefProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return DWORD(wstrdup(L"As-Text"));

		case tpDescription:
			return DWORD(wstrdup(L"Desc"));

		case tpTextOff:
			return DWORD(wstrdup(L"OFF"));

		case tpTextOn:
			return DWORD(wstrdup(L"ON"));

		case tpForeColor:
			return GetRGB(31,31,31);

		case tpBackColor:
			return GetRGB(0,0,0);
		}

	if( ID == tpMaximum ) {

		if( Type == typeInteger ) {

			return 100;
			}

		if( Type == typeReal ) {

			return R2I(100);
			}
		}

	return GetNull(Type);
	}

PCTXT CTag::GetPropName(WORD ID)
{
	switch( ID ) {

		case tpAsText:      return "AsText";
		case tpLabel:	    return "Label";
		case tpDescription: return "Description";
		case tpPrefix:      return "Prefix";
		case tpUnits:	    return "Units";
		case tpSetPoint:    return "SetPoint";
		case tpMinimum:     return "Minimum";
		case tpMaximum:     return "Maximum";
		case tpForeColor:   return "ForeColor";
		case tpBackColor:   return "BackColor";
		case tpName:	    return "Name";
		case tpIndex:	    return "Index";
		case tpAlarms:      return "Alarms";
		case tpTextOff:     return "TextOff";
		case tpTextOn:      return "TextOn";
		case tpStateCount:  return "StateCount";
		case tpDeadband:    return "Deadband";
		}

	return "";
	}

UINT CTag::GetPropType(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:      return typeString;
		case tpLabel:	    return typeString;
		case tpDescription: return typeString;
		case tpPrefix:      return typeString;
		case tpUnits:	    return typeString;
		case tpSetPoint:    return Type;
		case tpMinimum:     return Type;
		case tpMaximum:     return Type;
		case tpForeColor:   return typeInteger;
		case tpBackColor:   return typeInteger;
		case tpName:	    return typeString;
		case tpIndex:	    return typeInteger;
		case tpAlarms:      return typeInteger;
		case tpTextOff:     return typeString;
		case tpTextOn:      return typeString;
		case tpStateCount:  return typeInteger;
		case tpDeadband:    return Type;
		}

	return typeVoid;
	}

// Constructor

CTag::CTag(void)
{
	m_Ref      = 0;
	m_uBits    = 0;
	m_uRegs    = 0;
	m_pValue   = NULL;
	m_pLabel   = NULL;
	m_pAlias   = NULL;
	m_pSim     = NULL;
	m_pFormat  = NULL;
	m_pColor   = NULL;
	m_FormType = 0;
	m_ColType  = 0;
	m_pBlock   = NULL;
	m_nPos     = 0;
	}

// Destructor

CTag::~CTag(void)
{
	delete m_pValue;
	delete m_pSim;
	delete m_pLabel;
	delete m_pAlias;
	delete m_pFormat;
	delete m_pColor;
	}

// Initialization

void CTag::Load(PCBYTE &pData)
{
	m_Name = GetWide(pData);

	m_Desc = GetWide(pData);

	switch( *pData ) {

		case 0:
			pData++;

			m_uBits = 32;

			break;

		case 1:
			GetCoded(pData, m_pValue);

			m_uBits = 32;

			break;

		case 2:
			pData++;

			m_Ref   = GetLong(pData);

			m_uBits = GetByte(pData);

			break;
		}
	
	GetCoded(pData, m_pSim);
	
	GetCoded(pData, m_pLabel);

	GetCoded(pData, m_pAlias);

	if( GetByte(pData) ) {

		m_FormType = GetByte(pData);

		m_pFormat  = CDispFormat::MakeObject(m_FormType);

		if( m_pFormat ) {

			m_pFormat->Load(pData);
			}
		}

	if( GetByte(pData) ) {

		m_ColType = GetByte(pData);
		
		m_pColor  = CDispColor::MakeObject(m_ColType);

		if( m_pColor ) {

			m_pColor->Load(pData);
			}
		}

	Resolve();
	}
// Attributes

CAddress CTag::GetAddress(void) const
{
	if( !m_pBlock ) {

		CAddress a;

		a.m_Ref = 0;

		return a;
		}

	return m_pBlock->GetAddress(m_nPos);
	}

UINT CTag::GetDataType(void) const
{
	return typeVoid;
	}

BOOL CTag::IsArray(void) const
{
	return FALSE;
	}

UINT CTag::GetExtent(void) const
{
	return 0;
	}

BOOL CTag::CanWrite(void) const
{
	return FALSE;
	}

// Limit Access

DWORD CTag::GetMinValue(UINT uPos, UINT Type)
{
	CDataRef Ref;

	((DWORD &) Ref) = 0;

	Ref.x.m_Array   = uPos;

	return GetProp(Ref, tpMinimum, Type);
	}

DWORD CTag::GetMaxValue(UINT uPos, UINT Type)
{
	CDataRef Ref;

	((DWORD &) Ref) = 0;

	Ref.x.m_Array   = uPos;

	return GetProp(Ref, tpMaximum, Type);
	}

// Service Access

DWORD CTag::GetColorPair(UINT uPos)
{
	if( m_pColor ) {

		CDataRef Ref;

		Ref.m_Ref     = 0;

		Ref.x.m_Array = uPos;

		UINT  Type = GetDataType();

		DWORD Data = GetData(Ref, Type, getNone);

		DWORD Pair = m_pColor->GetColorPair(Data, Type);

		FreeData(Data, Type);

		return Pair;
		}

	return 0x07FFF0000;
	}

CUnicode CTag::GetLabel(UINT uPos)
{
	return GetLabel(uPos, TRUE);
	}

CUnicode CTag::GetLabel(UINT uPos, BOOL fLoc)
{
	CLangManager *pLang = CCommsSystem::m_pThis->m_pLang;

	UINT          uLang = fLoc ? pLang->GetCurrentSlot() : 0;

	CUnicode Label;
	
	if( m_pLabel ) {

		Label = m_pLabel->GetText(uLang, L"", PDWORD(&uPos));
		}
	else {
		Label = m_Name;
		}

	if( IsArray() && uPos < NOTHING ) {

		if( m_pLabel ) {

			// This is a nasty hack but it avoids adding the [%d]
			// to a tag label if the label is already configured to
			// use the uPos parameter to differentiate itself.

			uPos += 1;

			CUnicode Other = m_pLabel->GetText(uLang, L"", PDWORD(&uPos));

			if( Label == Other ) {

				Label += UniConvert(CPrintf("[%d]", uPos));
				}

			return Label;
			}

		Label += UniConvert(CPrintf("[%d]", uPos));
		}

	return Label;
	}

CUnicode CTag::GetAlias(UINT uPos)
{
	CUnicode Alias;
	
	if( m_pAlias ) {

		Alias = m_pAlias->GetText(L"", PDWORD(&uPos));
		}
	else {
		Alias= m_Name;
		}

	if( IsArray() && uPos < NOTHING ) {

		if( m_pAlias ) {

			// This is a nasty hack but it avoids adding the [%d]
			// to a tag alias if the alias is already configured to
			// use the uPos parameter to differentiate itself.

			uPos += 1;

			CUnicode Other = m_pAlias->GetText(L"", PDWORD(&uPos));

			if( Alias == Other ) {

				Alias += UniConvert(CPrintf("[%d]", uPos));
				}

			return Alias;
			}

		Alias += UniConvert(CPrintf("[%d]", uPos));
		}

	return Alias;
	}

CUnicode CTag::GetAsText(UINT uPos, DWORD Data, UINT Type, UINT Flags)
{
	CUnicode Text;

	if( m_pFormat ) {

		Text = m_pFormat->Format( Data,
					  Type,
					  Flags
					  );
		}
	else {
		Text = CDispFormat::GeneralFormat( Data,
						   Type,
						   Flags
						   );
		}

	return Text;
	}

CUnicode CTag::GetAsText(UINT uPos, UINT Flags)
{
	CDataRef Ref;

	Ref.m_Ref     = 0;

	Ref.x.m_Array = uPos;

	UINT  Type    = typeVoid;

	DWORD Data    = 0;

	if( IsAvail(Ref) ) {

		Type = GetDataType();

		Data = GetData(Ref, Type, getNone);
		}

	CUnicode Text = GetAsText(uPos, Data, Type, Flags);

	FreeData(Data, Type);

	return Text;
	}

CString CTag::GetStates(void)
{
	if( m_pFormat ) {

		return m_pFormat->GetStates();
		}

	return "";
	}

BOOL CTag::SetAsText(UINT uPos, CString const &Text)
{
	BOOL  fHit = FALSE;

	UINT  Type = GetDataType();

	DWORD Data = 0;

	if( m_pFormat ) {

		fHit = m_pFormat->Parse( Data,
					 Text,
					 Type
					 );
		}
	else {
		fHit = CDispFormat::GeneralParse( Data,
						  Text,
						  Type
						  );
		}

	if( fHit ) {

		CDataRef Ref;

		Ref.m_Ref     = 0;

		Ref.x.m_Array = uPos;

		if( IsValid(Ref, Data, Type, setNone) ) {

			SetData(Ref, Data, Type, setNone);

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

void CTag::Purge(void)
{
	}

void CTag::Force(void)
{
	}

void CTag::SetIndex(UINT uIndex)
{
	m_uTag = uIndex;
	}

BOOL CTag::SetPollScan(UINT uCode)
{
	return FALSE;
	}

void CTag::Poll(UINT uDelta)
{
	}

// Text Access

BOOL CTag::GetEventText(CUnicode &Text, UINT uItem, UINT uPos)
{
	return FALSE;
	}

// Quick Plot

BOOL CTag::GetQuickPlot(CTagQuickPlot * &pQuick)
{
	pQuick = NULL;

	return FALSE;
	}

// Evaluation

BOOL CTag::IsAvail(CDataRef const &Ref)
{
	return IsAvail(Ref, 0);
	}

BOOL CTag::IsAvail(CDataRef const &Ref, UINT Flags)
{
	if( m_pValue ) {

		return m_pValue->IsAvail();
		}

	if( m_pLabel ) {

		return m_pLabel->IsAvail();
		}

	if( m_pAlias ) {

		return m_pAlias->IsAvail();
		}

	if( m_pSim ) {

		return m_pSim->IsAvail();
		}

	return TRUE;
	}

BOOL CTag::SetScan(CDataRef const &Ref, UINT Code)
{
	if( m_pValue ) {

		return m_pValue->SetScan(Code);
		}

	if( m_pLabel ) {

		return m_pLabel->SetScan(Code);
		}

	if( m_pAlias ) {

		return m_pAlias->SetScan(Code);
		}

	if( m_pSim ) {

		return m_pSim->SetScan(Code);
		}

	return TRUE;
	}

DWORD CTag::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( Flags & getNoString ) {

		if( GetDataType() == typeString ) {

			return 0;
			}
		}

	if( m_pValue ) {

		UINT uPos = Ref.x.m_Array;

		return m_pValue->ExecVal(PDWORD(&uPos));
		}

	if( m_pSim ) {

		return m_pSim->ExecVal();
		}

	return GetNull(Type);
	}

BOOL CTag::IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	return TRUE;
	}

BOOL CTag::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	FreeData(Data, Type);

	return TRUE;
	}

DWORD CTag::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText(Ref);

		case tpLabel:
			return FindLabel(Ref);

		case tpDescription:
			return DWORD(wstrdup(m_Desc));

		case tpForeColor:
			return FindFore(Ref);

		case tpBackColor:
			return FindBack(Ref);

		case tpName:
			return DWORD(wstrdup(m_Name));
		}

	switch( ID ) {

		case tpAsText:
		case tpLabel:
		case tpDescription:
		case tpPrefix:
		case tpUnits:
		case tpName:
		case tpTextOn:
		case tpTextOff:
			return GetNull(typeString);

		case tpSetPoint:
		case tpMinimum:
		case tpMaximum:
		case tpDeadband:
			return GetNull(Type);

		case tpForeColor:
		case tpBackColor:
		case tpIndex:
		case tpAlarms:
			return 0;
		}

	return 0;
	}

// Deadband

void CTag::InitPrevious(DWORD &Prev)
{
	Prev = 0x80000000;
	}

BOOL CTag::HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev)
{
	DWORD Read = GetData(Ref, typeVoid, getNone);

	if( Prev != Read ) {

		Data = Read;

		return TRUE;
		}

	return FALSE;
	}

void CTag::KillPrevious(DWORD &Prev)
{
	InitPrevious(Prev);
	}

// Property Access

DWORD CTag::FindAsText(CDataRef const &Ref, UINT Type, DWORD Data)
{
	if( m_pFormat ) {

		CUnicode Text = m_pFormat->Format(Data, Type, fmtANSI);

		FreeData(Data, Type);

		return DWORD(wstrdup(Text));
		}

	CUnicode Text = CDispFormat::GeneralFormat(Data, Type, fmtANSI);

	FreeData(Data, Type);

	return DWORD(wstrdup(Text));
	}

DWORD CTag::FindAsText(CDataRef const &Ref)
{
	UINT  Type = GetDataType();

	DWORD Data = GetData(Ref, Type, getNone);

	return FindAsText(Ref, Type, Data);
	}

DWORD CTag::FindLabel(CDataRef const &Ref)
{
	if( m_pLabel ) {

		DWORD n = Ref.x.m_Array;

		return DWORD(wstrdup(m_pLabel->GetText(L"", &n)));
		}

	return DWORD(wstrdup(m_Name));
	}

DWORD CTag::FindFore(CDataRef const &Ref)
{
	if( m_pColor ) {

		UINT  Type = GetDataType();

		DWORD Data = GetData(Ref, Type, getNone);

		COLOR Fore = m_pColor->GetForeColor(Data, Type);

		FreeData(Data, Type);

		return Fore;
		}

	return GetRGB(31,31,31);
	}

DWORD CTag::FindBack(CDataRef const &Ref)
{
	if( m_pColor ) {

		UINT  Type = GetDataType();

		DWORD Data = GetData(Ref, Type, getNone);

		COLOR Back = m_pColor->GetBackColor(Data, Type);

		FreeData(Data, Type);

		return Back;
		}

	return GetRGB(0,0,0);
	}

// Implementation

BOOL CTag::Resolve(void)
{
	if( m_Ref ) {

		CCommsManager  * pManager = CCommsSystem::m_pThis->m_pComms;

		CDataRef const & Src      = (CDataRef const &) m_Ref;
		
		m_pBlock                  = pManager->Resolve(Src, m_nPos);

		return TRUE;
		}

	return FALSE;
	}

// End of File
