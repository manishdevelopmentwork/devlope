@echo off

call target win32

cd /d "%~2"

nmake -nologo -f makefile %1 Tools="%~3" OutDir="..\..\..\..\..\Build\Bin\Runtime\LinuxA8\%~4" Config="%~4"
