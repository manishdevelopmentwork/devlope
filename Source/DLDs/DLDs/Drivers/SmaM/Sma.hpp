//////////////////////////////////////////////////////////////////////////
//
//  SMA Structures
//

//#pragma pack(1)

struct Channel {

	BYTE Index;
	BYTE Type1;
	BYTE Type2;
	BYTE Size;
	BYTE Depth;
	char Name[16];
	char Unit[8];
	U4   Gain;
	U4   Offset;
	char Lo[16];
	char Hi[16];
	U2   TSize;
	char Text[16];
	U4   Data;
	BOOL Error;
	};

//#pragma pack(0)


//#pragma pack(1)

struct Param {

	PTXT	 m_Name;
	UINT	 m_Channel;
	CAddress m_Addr;
	};

//#pragma pack(0)

//////////////////////////////////////////////////////////////////////////
//
//  SMA Enumerations
//

enum Type1 {

	typeAnalog  = 0,
	typeDigital = 1,
	typeCounter = 2,
	typeStatus  = 3,
	};

enum Size {

	sizeByte  = 0,
	sizeWord  = 1,
	sizeDWord = 2,
	sizeFloat = 4,
	};

//////////////////////////////////////////////////////////////////////////
//
//  SMA Commands
//

#define CMD_SEARCH_DEV  0x2
#define CMD_GET_CINFO	0x9
#define CMD_SYN_ONLINE  0xA
#define CMD_GET_DATA	0xB
#define CMD_SET_DATA    0xC
#define CMD_GET_SINFO	0xD

//////////////////////////////////////////////////////////////////////////
//
//  SMA Base Driver
//

class CSmaDriver : public CMasterDriver
{
	public:
		// Constructor
		CSmaDriver(void);

		// Destructor
		~CSmaDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
				
	protected:

		// Help
		IExtraHelper	 * m_pExtra;

		// Device Data
		struct CBaseCtx
		{
			BYTE	  m_Mode;
			BYTE	  m_Addr;
			BYTE	  m_Addr2;
			UINT	  m_Poll;
			UINT	  m_uBook;
			Channel * m_pBook;
			UINT	  m_uParam;
			Param *   m_pParam;
			BOOL	  m_fInit;
			BYTE  	  m_pDoPoll[0xF];
			UINT	  m_Time[0xF];
			BOOL	  m_fError;
			DWORD	  m_SN;
			BOOL	  m_fMemStore;

			// Fall back support
			BOOL	  m_fAux;
			UINT	  m_uBook2;
			Channel * m_pBook2;
			Param   * m_pParam2;
			DWORD	  m_SN2;
			BOOL	  m_fReset;
			BOOL	  m_fMemStore2;
			};

		// Multipacket Buffer

		BYTE	m_pBuff[32000];
		UINT	m_uBuff;

		
		CBaseCtx * m_pBase;

		// Data Members
		BYTE	m_pRx[300];
		BYTE	m_pTx[300];
		UINT	m_uPtr;
		WORD	m_FCS;
		WORD	m_Src;
		UINT    m_uBaud;
		
		// Implementation
		void Start(void);
		void AddAddress(void);
		void AddControl(void);
		void AddTele(BYTE bCode, BOOL fSearch = FALSE);
		void AddMask(BYTE bType1, BYTE bType2, BYTE bIndex);
		void AddFCS(void);
		void Stop(void);
		BOOL Check(void);
		void AddByte(BYTE bByte);
		void AddWord(WORD wWord);
		void AddLong(DWORD dwWord);
		void AddToFCS(BYTE bByte);
		BOOL MakeLookup(void);
		UINT FindIndex(PCTXT Name, UINT &Pos);
		UINT FindDepth(void);
		BOOL IsAnalog(BYTE bType);
		BOOL IsDigital(BYTE bType);
		BOOL IsCounter(BYTE bType);
		BOOL IsStatus(BYTE bType);
		BOOL LookupMap(void);
		CCODE Poll(void);
		CCODE DoPoll(BYTE bType2);
		CCODE Sync(void);
		CCODE Search(void);
		BYTE  FindPoll(BYTE bType2);
		BOOL  FindSN(void);
		void StoreData(BYTE bType2);
		BOOL GetData(AREF Addr, PDWORD pData, UINT uCount);
		
		// Transport
		BOOL Transact(void);
	virtual BOOL Send(void);
	virtual BOOL Recv(void);
		void SendAck(void);
		void Delay(void);
		
		// Helpers

		void	 ToggleAux(void);
		UINT	 GetBookCount(void);
		void	 SetBookCount(UINT uCount);
		Channel* GetBook(UINT uIndex);
		Param  * GetParam(UINT uIndex);
		DWORD    GetSN(void);
		BOOL	 IsTimedOut(UINT uTime);
		BOOL	 IsMultiPacket(void); 
		BOOL	 HandleEscape(BYTE bByte);
		Channel* FindChannel(AREF Addr);
		void	 PrintLookup(void);
		void	 PrintDeviceList(void);
		void	 PrintMap(void);

	};


//////////////////////////////////////////////////////////////////////////
//
// CRC-16 Lookup Table
//

static WORD const CRCTable16[] =
{
	
	0x0000, 0x1189, 0x2312, 0x329B, 0x4624, 0x57AD, 0x6536, 0x74BF,
	0x8C48, 0x9DC1, 0xAF5A, 0xBED3, 0xCA6C, 0xDBE5, 0xE97E, 0xF8F7,
	0x1081, 0x0108, 0x3393, 0x221A, 0x56A5, 0x472C, 0x75B7, 0x643E,
	0x9CC9, 0x8D40, 0xBFDB, 0xAE52, 0xDAED, 0xCB64, 0xF9FF, 0xE876,
	0x2102, 0x308B, 0x0210, 0x1399, 0x6726, 0x76AF, 0x4434, 0x55BD,
	0xAD4A, 0xBCC3, 0x8E58, 0x9FD1, 0xEB6E, 0xFAE7, 0xC87C, 0xD9F5,
	0x3183, 0x200A, 0x1291, 0x0318, 0x77A7, 0x662E, 0x54B5, 0x453C,
	0xBDCB, 0xAC42, 0x9ED9, 0x8F50, 0xFBEF, 0xEA66, 0xD8FD, 0xC974,
	0x4204, 0x538D, 0x6116, 0x709F, 0x0420, 0x15A9, 0x2732, 0x36BB,
	0xCE4C, 0xDFC5, 0xED5E, 0xFCD7, 0x8868, 0x99E1, 0xAB7A, 0xBAF3,
	0x5285, 0x430C, 0x7197, 0x601E, 0x14A1, 0x0528, 0x37B3, 0x263A,
	0xDECD, 0xCF44, 0xFDDF, 0xEC56, 0x98E9, 0x8960, 0xBBFB, 0xAA72,
	0x6306, 0x728F, 0x4014, 0x519D, 0x2522, 0x34AB, 0x0630, 0x17B9,
	0xEF4E, 0xFEC7, 0xCC5C, 0xDDD5, 0xA96A, 0xB8E3, 0x8A78, 0x9BF1,
	0x7387, 0x620E, 0x5095, 0x411C, 0x35A3, 0x242A, 0x16B1, 0x0738,
	0xFFCF, 0xEE46, 0xDCDD, 0xCD54, 0xB9EB, 0xA862, 0x9AF9, 0x8B70,
	0x8408, 0x9581, 0xA71A, 0xB693, 0xC22C, 0xD3A5, 0xE13E, 0xF0B7,
	0x0840, 0x19C9, 0x2B52, 0x3ADB, 0x4E64, 0x5FED, 0x6D76, 0x7CFF,
	0x9489, 0x8500, 0xB79B, 0xA612, 0xD2AD, 0xC324, 0xF1BF, 0xE036,
	0x18C1, 0x0948, 0x3BD3, 0x2A5A, 0x5EE5, 0x4F6C, 0x7DF7, 0x6C7E,
	0xA50A, 0xB483, 0x8618, 0x9791, 0xE32E, 0xF2A7, 0xC03C, 0xD1B5,
	0x2942, 0x38CB, 0x0A50, 0x1BD9, 0x6F66, 0x7EEF, 0x4C74, 0x5DFD,
	0xB58B, 0xA402, 0x9699, 0x8710, 0xF3AF, 0xE226, 0xD0BD, 0xC134,
	0x39C3, 0x284A, 0x1AD1, 0x0B58, 0x7FE7, 0x6E6E, 0x5CF5, 0x4D7C,
	0xC60C, 0xD785, 0xE51E, 0xF497, 0x8028, 0x91A1, 0xA33A, 0xB2B3,
	0x4A44, 0x5BCD, 0x6956, 0x78DF, 0x0C60, 0x1DE9, 0x2F72, 0x3EFB,
	0xD68D, 0xC704, 0xF59F, 0xE416, 0x90A9, 0x8120, 0xB3BB, 0xA232,
	0x5AC5, 0x4B4C, 0x79D7, 0x685E, 0x1CE1, 0x0D68, 0x3FF3, 0x2E7A,
	0xE70E, 0xF687, 0xC41C, 0xD595, 0xA12A, 0xB0A3, 0x8238, 0x93B1,
	0x6B46, 0x7ACF, 0x4854, 0x59DD, 0x2D62, 0x3CEB, 0x0E70, 0x1FF9,
	0xF78F, 0xE606, 0xD49D, 0xC514, 0xB1AB, 0xA022, 0x92B9, 0x8330,
	0x7BC7, 0x6A4E, 0x58D5, 0x495C, 0x3DE3, 0x2C6A, 0x1EF1, 0x0F78

	};


// End of File
