
//////////////////////////////////////////////////////////////////////////
//
// Eaton Data Spaces
//

// Space Definitions
enum {
	SP_D	= 1,
	SP_M	= 2,
	SP_S	= 3,
	SP_X	= 4,
	SP_Y	= 5,
	SP_T	= 6,
	SP_U	= 7,
	SP_C	= 8,
	SP_B	= 9,
	SP_A	= 10
	};

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Serial Driver
//

class CEatonELCSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CEatonELCSerialDriver(void);

		// Destructor
		~CEatonELCSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BOOL m_fNoReadEx;
			};

		// Data Members
		BOOL	   m_fAscii;
		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		UINT	   m_uTimeout;
		CRC16	   m_CRC;

		// Implementation
		void AllocBuffers(void);
		void FreeBuffers(void);
		BOOL IsHex(BYTE bData);
		WORD FromHex(BYTE bData);
		BOOL IgnoreException(void);

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL Transact(BOOL fIgnore);
		BOOL PutFrame(void);
		BOOL GetFrame(void);
		BOOL BinaryTx(void);
		BOOL BinaryRx(void);
		BOOL AsciiTx(void);
		BOOL AsciiRx(void);

		// Transport Helpers
		UINT FindReplySize(void);
		
		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead (AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite (AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		DWORD ConstructAddr(AREF Addr);
	};

// End of File
