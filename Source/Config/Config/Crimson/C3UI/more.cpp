
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- More HotLink
//

// Dynamic Class

AfxImplementDynamicClass(CUIMoreButton, CUIControl);

// Constructor


CUIMoreButton::CUIMoreButton(void)
{
	m_pCtrlLayout = NULL;

	m_pHotLink    = New CHotLinkCtrl;

	m_uPad        = 3;
	}

// Core Overridables

void CUIMoreButton::OnBind(void)
{
	if( !GetFormat().IsEmpty() ) {

		m_uPad = watoi(GetFormat());
		}

	m_Label = GetLabel();
	}

void CUIMoreButton::OnLayout(CLayFormation *pForm)
{
	m_pCtrlLayout = New CLayItemText(m_Label.GetLength(), m_uPad, 0);

	m_pMainLayout = New CLayFormPad (m_pCtrlLayout, CRect(2, 1, 2, 1), horzLeft | vertCenter);
	
	pForm->AddItem(New CLayItem());

	pForm->AddItem(m_pMainLayout);
	}

void CUIMoreButton::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pHotLink->Create( m_Label,
			    WS_TABSTOP,
			    GetRect(),
			    Wnd,
			    uID++
			    );

	m_pHotLink->SetFont(afxFont(Dialog));

	AddControl(m_pHotLink, BN_CLICKED);
	}

void CUIMoreButton::OnPosition(void)
{
	m_pHotLink->MoveWindow(GetRect(), TRUE);
	}

// Data Overridables

void CUIMoreButton::OnLoad(void)
{
	}

UINT CUIMoreButton::OnSave(BOOL fUI)
{
	return saveChange;
	}

// Notification Handlers

BOOL CUIMoreButton::OnNotify(UINT uID, UINT uCode)
{
	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

CRect CUIMoreButton::GetRect(void)
{
	CRect Rect = m_pCtrlLayout->GetRect();

	Rect.top    -= 3;

	Rect.bottom += 3;

	return Rect;
	}

// End of File
