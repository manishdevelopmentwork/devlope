
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Scroll Messages

void CSourceEditorWnd::OnVScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( &Ctrl == m_pScrollV ) {

		if( OnScroll(uCode, nPos, m_pScrollV, m_FontSize.cy, m_ViewOrg.y) ) {

			Invalidate(FALSE);

			Ctrl.Invalidate(FALSE);

			ShowDefaultStatus();
			}
		}
	}

void CSourceEditorWnd::OnHScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( &Ctrl == m_pScrollH ) {

		if( OnScroll(uCode, nPos, m_pScrollH, m_FontSize.cx, m_ViewOrg.x) ) {

			Invalidate(FALSE);

			Ctrl.Invalidate(FALSE);

			ShowDefaultStatus();
			}
		}
	}

// Scroll Support

void CSourceEditorWnd::ScrollBy(CSize Step)
{
	if( m_fScrollV || m_fScrollH ) {

		CPoint Pos = CPoint(Step * m_FontSize) - m_ViewOrg;

		if( m_fScrollH ) {

			MakeMax(Pos.x, 0);

			PostMessage( WM_HSCROLL, 
				     WPARAM(MAKELONG(SB_THUMBPOSITION, Pos.x)), 
				     LPARAM(m_pScrollH->GetHandle())
				     );
			}

		if( m_fScrollV ) {

			MakeMax(Pos.y, 0);

			PostMessage( WM_VSCROLL, 
				     WPARAM(MAKELONG(SB_THUMBPOSITION, Pos.y)), 
				     LPARAM(m_pScrollV->GetHandle())
				     );
			}
		}
	}

void CSourceEditorWnd::JumpIntoView(void)
{
	CPoint Pos = m_CaretPos;

	if( m_fScrollH || m_fScrollV ) {

		CRect Rect = GetClientRect() - m_nSpace;

		if( m_pScrollH->IsWindowVisible() ) {

			Rect.bottom -= 18;
			}

		if( m_pScrollV->IsWindowVisible() ) {

			Rect.right -= 18;
			}

		CRect Work = CRect(Pos, m_FontSize);

		if( !Rect.Encloses(Work) ) {

			if( m_fScrollH ) {

				if( Work.left < Rect.left ) {

					int nDelta = Work.left - Rect.left - m_FontSize.cx;
					
					ScrollBy(CSize(nDelta / m_FontSize.cx, 0));
					}

				if( Work.right > Rect.right ) {

					int nDelta = Work.right - Rect.right;
					
					ScrollBy(CSize(nDelta / m_FontSize.cx, 0));
					}					
				}

			if( m_fScrollV ) {

				if( Work.top < Rect.top ) {

					int nDelta = Work.top - Rect.top - m_FontSize.cy;

					ScrollBy(CSize(0, nDelta / m_FontSize.cy));
					}

				if( Work.bottom > Rect.bottom ) {

					int nDelta = Work.bottom - Rect.bottom + m_FontSize.cy;

					ScrollBy(CSize(0, nDelta / m_FontSize.cy));
					}
				}
			}
		}
	}

void CSourceEditorWnd::ScrollIntoView(CPoint Pos, BOOL fDrop)
{
	if( m_fScrollH || m_fScrollV ) {

		CRect Rect = GetClientRect() - m_nSpace;

		if( m_pScrollH->IsWindowVisible() ) {

			Rect.bottom -= 18;
			}

		if( m_pScrollV->IsWindowVisible() ) {

			Rect.right -= 18;
			}

		if( fDrop ) {
			
			Rect -= 32;
			}

		CRect Work = CRect(Pos, m_FontSize);

		if( !Rect.Encloses(Work) ) {

			BOOL  fHit = FALSE;			

			if( m_fScrollH ) {

				LPARAM lParam = LPARAM(m_pScrollH->GetHandle());

				if( Work.left < Rect.left ) {

					SendMessage(WM_HSCROLL, SB_LINELEFT, lParam);
					
					fHit = TRUE;
					}

				if( Work.right > Rect.right ) {

					SendMessage(WM_HSCROLL, SB_LINERIGHT, lParam);
					
					fHit = TRUE;
					}					
				}

			if( m_fScrollV ) {

				LPARAM lParam = LPARAM(m_pScrollV->GetHandle());

				if( Work.top < Rect.top ) {

					SendMessage(WM_VSCROLL, SB_LINELEFT, lParam);
					
					fHit = TRUE;
					}

				if( Work.bottom > Rect.bottom ) {

					SendMessage(WM_VSCROLL, SB_LINERIGHT, lParam);
					
					fHit = TRUE;
					}
				}

			if( fHit ) {

				SetTimer(m_timerScroll, 50);
				
				return;
				}
			}		
		}

	KillTimer(m_timerScroll);
	}

void CSourceEditorWnd::CreateScrollBars(void)
{
	CRect Null(0, 0, 1, 1);
	
	m_pScrollH->Create(SBS_HORZ,     Null, ThisObject, 2);
	
	m_pScrollV->Create(SBS_VERT,     Null, ThisObject, 2);

	m_pScrollC->Create(SS_WHITERECT, Null, ThisObject, 3);
	}

void CSourceEditorWnd::PlaceScrollBars(void)
{
	CRect Horz = GetClientRect() - 1;

	CRect Vert = GetClientRect() - 1;

	CRect Corn = GetClientRect() - 1;

	if( m_fScrollH ) {

		Horz.top = Horz.bottom - 18;

		if( m_fScrollV ) {

			Horz.right = Horz.right - 18;
			}

		m_pScrollH->MoveWindow(Horz, FALSE);
		}

	if( m_fScrollV ) {

		Vert.left = Vert.right - 18;

		if( m_fScrollH ) {

			Vert.bottom = Vert.bottom - 18;
			}

		m_pScrollV->MoveWindow(Vert, FALSE);
		}

	if( TRUE ) {

		Corn.left = Corn.right  - 18;

		Corn.top  = Corn.bottom - 18;

		m_pScrollC->MoveWindow(Corn, FALSE);
		}
	}

void CSourceEditorWnd::ShowScrollBar(CWnd *pWnd)
{
	if( pWnd->IsWindowVisible() ) {

		pWnd->Invalidate(FALSE);

		return;
		}

	pWnd->ShowWindow((m_uMode == 1) ? SW_HIDE : SW_SHOW);
	}

BOOL CSourceEditorWnd::OnScroll(UINT uCode, int nPos, CScrollBar *pBar, int nDelta, long &nValue)
{
	int  nOld = -nValue;

	int  nNew = nOld;

	int  nMax = pBar->GetRangeMax();
	
	int nPage = pBar->GetPageSize();

	switch( uCode ) {

		case SB_LEFT:

			nNew = 0;

			break;

		case SB_RIGHT:

			nNew = nMax - nPage;

			break;

		case SB_LINELEFT:

			nNew -= nDelta;
			
			break;

		case SB_LINERIGHT:

			nNew += nDelta;
			
			break;

		case SB_PAGELEFT:

			nNew -= nPage;
			
			break;

		case SB_PAGERIGHT :

			nNew += nPage;
			
			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:

			nNew = nPos;
			
			break;
		}

	MakeMax(nNew, 0);

	MakeMin(nNew, nMax - nPage);

	if( nNew - nOld ) {

		nValue = -nNew;

		pBar->SetScrollPos(nNew, FALSE);
		
		SetCaretPos();
		
		return TRUE;
		}

	return FALSE;
	}

// End of File
