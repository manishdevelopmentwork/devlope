
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Line
//

// Constructor

CPrimLine::CPrimLine(void)
{
	m_pPen = New CPrimPen;
	}

// Destructor

CPrimLine::~CPrimLine(void)
{
	delete m_pPen;
	}

// Initialization

void CPrimLine::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimLine", pData);

	CPrim::Load(pData);

	m_pPen->Load(pData);
	}

// Overridables

void CPrimLine::SetScan(UINT Code)
{
	m_pPen->SetScan(Code);

	CPrim::SetScan(Code);
	}

void CPrimLine::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		if( m_pPen->DrawPrep(pGDI) ) {

			m_fChange = TRUE;
			}
		}
	}

void CPrimLine::DrawPrim(IGDI *pGDI)
{
	m_pPen->Configure(pGDI);

	pGDI->DrawLine(PassRect(m_DrawRect));
	}

R2 CPrimLine::GetBackRect(void)
{
	R2 Rect;

	int n       = m_pPen->GetAdjust();
	
	Rect.left   = min(m_DrawRect.left, m_DrawRect.right ) - n;
	
	Rect.top    = min(m_DrawRect.top,  m_DrawRect.bottom) - n;

	Rect.right  = max(m_DrawRect.left, m_DrawRect.right ) + n + 1;
	
	Rect.bottom = max(m_DrawRect.top,  m_DrawRect.bottom) + n + 1;

	return Rect;
	}

// End of File

