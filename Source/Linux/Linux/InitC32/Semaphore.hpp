
#pragma  once

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linux Semaphore Class
//
// Copyright (c) 2018 Granby Consulting LLC
//
// Placed in the Public Domain.
//

#ifndef INCLUDE_Semaphore_HPP

#define INCLUDE_Semaphore_HPP

////////////////////////////////////////////////////////////////////////////////
//	
// Semaphore Class
//

class CSemaphore
{
public:
	// Constructor
	CSemaphore(void);

	// Destructor
	~CSemaphore(void);

	// Operations
	void Wait(void);
	void Post(void);

protected:
	// Data Members
	char    m_sName[32];
	size_t  m_nMem;
	int     m_hMem;
	sem_t * m_pSem;
};

// End of File

#endif
