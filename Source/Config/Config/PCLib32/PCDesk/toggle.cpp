
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Toggle Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CToggleGadget, CGadget);
		
// Constructors

CToggleGadget::CToggleGadget(UINT uID, DWORD Image, BOOL fRight)
{
	m_uID    = uID;

	m_Image  = Image;

	m_Size   = CSize(18, 18);
	
	m_uProps = tbgShowTip | (fRight ? tbgRight : 0);
	
	m_uFlags = 0;
	}

// Overridables

void CToggleGadget::OnCreate(CDC &DC)
{
	m_Size.cx += afxButton->Adjust(DC, m_Image, m_Text);
	}

void CToggleGadget::OnPaint(CDC &DC)
{
	UINT  uFlags = m_uFlags;

	DWORD Image  = m_Image;
	
	if( uFlags & MF_CHECKED ) {

		uFlags ^= MF_CHECKED;

		Image  += 1;
		}

	afxButton->Draw(DC, m_Rect, Image, m_Text, uFlags);
	}

BOOL CToggleGadget::OnMouseDown(CPoint const &Pos)
{
	if( !TestFlag(MF_DISABLED) ) {
		
		SetFlag(MF_PRESSED);
		
		Update();
		
		return TRUE;
		}
		
	return FALSE;
	}

void CToggleGadget::OnMouseMove(CPoint const &Pos, BOOL fHit)
{
	AdjustFlag(MF_PRESSED, fHit);
	}

void CToggleGadget::OnMouseUp(CPoint const &Pos, BOOL fHit)
{
	if( TestFlag(MF_PRESSED) ) {
	
		DoAction();
			
		ClearFlag(MF_PRESSED);
		}
	}

BOOL CToggleGadget::OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip)
{
	CCmdInfo Info;

	CWnd *pWnd = pPrivate ? pPrivate : afxMainWnd;
		
	pWnd->RouteGetInfo(m_uID, Info);
		
	if( !Info.m_ToolTip.IsEmpty() ) {
		
		if( fShift ) {

			Tip = Info.m_Prompt;
			}
		else
			Tip = Info.m_ToolTip;
		}
	else
		Tip = Info.m_Prompt;
	
	return TRUE;
	}

void CToggleGadget::OnSetFlags(void)
{
	Update();
	}

void CToggleGadget::OnIdle(CWnd *pPrivate)
{
	CCmdSourceData Src;
	
	Src.PrepareSource();

	CWnd *pWnd = pPrivate ? pPrivate : afxMainWnd;
	
	pWnd->RouteControl(m_uID, Src);
	
	UINT uMask = (MF_DISABLED | MF_CHECKED);
	
	UINT uTest = ((m_uFlags & ~uMask) | (Src.GetFlags() & uMask));
	
	if( m_uFlags != uTest ) {
	
		m_uFlags = uTest;
		
		Update();
		}
	}

// Overridables

void CToggleGadget::DoAction(void)
{
	CWnd &Wnd = CWnd::GetActiveWindow();
	
	Wnd.PostMessage(WM_COMMAND, m_uID);
	}

// End of File
