
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROINVENSYSNANODAC_HPP
	
#define	INCLUDE_EUROINVENSYSNANODAC_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CEuroInvensysNanoDacDriver;
class CEuroInvensysNanoDacSerialDriver;
class CEuroInvensysNanoDacTCPDeviceOptions;
class CEuroInvensysNanoDacTCPDriver;
class CEuroInvensysNanoDacAddrDialog;

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	WL	addrWordAsLong
#define	WR	addrWordAsReal
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Register Table Spaces
#define	SP_0	 101	// Modbus Digital Outputs  00000
#define	SP_1	 102	// Modbus Digital Inputs   10000
#define	SP_3	 103	// Modbus Read-Only Words  30000
#define	SP_4	 104	// Modbus Read/Write Words 40000
// The following are Modbus Holding Registers
#define	SP_ALSC	1	// Alarm Summary - Channel
#define	SP_ALSG	2	// Alarm Summary - Global
#define	SP_ALSS	3	// Alarm Summary - System
#define	SP_BCD	4	// BCDInput
#define	SP_CALA	5	// Channel Alarm
#define	SP_CHAS	6	// Channel/Alarm Ack and Status
#define	SP_CHMN	7	// Channel Main
#define	SP_CMSG	8	// Custom Message/Triggers
#define	SP_DCO	9	// DC Output
#define	SP_DIO	10	// Digital IO
#define	SP_GRCD	11	// Group Recording
#define	SP_GTRD	12	// Group Trend
#define	SP_HUM	13	// Humidity
#define	SP_INSG	14	// Instrument - General
#define	SP_INSS	15	// Instrument - Strings
#define	SP_LGC2	16	// Logic 2
#define	SP_LGC8	17	// Logic 8
#define	SP_LDG1	18	// Loop 1 Diagnostics/Main
#define	SP_LDG2	19	// Loop 2 Diagnostics/Main
#define	SP_LOP1	20	// Loop OP1
#define	SP_LOP2	21	// Loop OP2
#define	SP_LPD1	22	// Loop PID1
#define	SP_LPD2	23	// Loop PID2
#define	SP_LSET	24	// Loop Setup
#define	SP_LSP1	25	// Loop SP1
#define	SP_LSP2	26	// Loop SP2
#define	SP_LTUN	27	// Loop Tune
#define	SP_MATH	28	// Math2
#define	SP_MUX	29	// Mux8
#define	SP_NANO	30	// Nano access
#define	SP_NET	31	// Network Data
#define	SP_NETS	32	// Network Strings
#define	SP_OR	33	// OR Block access
#define	SP_STER	34	// Sterilizer
#define	SP_STRS	35	// String responses
#define	SP_TIME	36	// Timers
#define	SP_USRL	37	// User Linearize
#define	SP_USRV	38	// User Values
#define	SP_VAAS	39
#define	SP_VALA	40	// Virt Alarm
#define	SP_VMT	41	// Virt Main/Trend
#define	SP_ZIR	42	// Zirconia

// Dialog box numbers
#define	DPADD	2002
#define	DNAME	2021
#define	DLOOP	2030
#define	DNUMB	2031
#define	DMODB	2033
#define	DSEGT	2034
#define	DSEGM	2035

#define	NUMT	SP_ZIR + 1	// number of tables

struct EIESTRINGS
{
	UINT	uTable;		// Table number selected
	UINT	uBlock;		// Selected block
	UINT	uIncr;		// Increment to next block
	UINT	uNPos;		// Position of Name in List
	UINT	uBPos;		// Position of Block # in List
	UINT	uSPos;		// Position of Segment in List
	UINT	uSegm;		// 2nd parameter number
	UINT	uOffset;	// Addr.a.m_Offset value
	UINT	uAddr;		// Modbus Address
	UINT	uQty[NUMT];	// Number of strings entered for selection
	UINT	uBlkCt[NUMT];	// Number of blocks for selection
	CString	sName [NUMT];	// Leading part of all names
	CString sDCO  [4];	// Middle part of DCO names
	CString	sDIO  [7];	// Middle part of DIO names
	};

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys NanoDac TCP/IP Master Driver Options
//

class CEuroInvensysNanoDacTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEuroInvensysNanoDacTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

class CEuroInvensysNanoDacDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuroInvensysNanoDacDriver(void);

		// Destructor
		~CEuroInvensysNanoDacDriver(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		EIESTRINGS * GetEIEPtr(void);

		BOOL	IsNotStdModbus(UINT uTable);
		BOOL	HasBlocks(void);
		BOOL	IsUsrLin(void);
		CString	GetParamString(UINT uTable, UINT uPosition);
		CString	ParseParamString(CString sParam, UINT uOffset);
		UINT	GetPosFromOffset(UINT uTable, UINT uOffset);
		BOOL	IsStringSelect(UINT uTable);
		UINT	GetUsrLinSegm(UINT uOffset);

	protected:

		// Data
		EIESTRINGS	EIESTR;
		EIESTRINGS	*m_pEIE;

		CString m_sName;	// Selected string

		// Implementation
		void	AddSpaces(void);

		// Helpers
		void	InitEIE(void);
		void	SetSelectionInfo(void);

		// Destruction
		void	Clear_EIE(void);

			// String Loading
 		CString	LoadAlarmSumChan(UINT uSel);
		CString	LoadAlarmSumGbl(UINT uSel);
		CString	LoadAlarmSumSys(UINT uSel);
		CString	LoadBCD(UINT uSel);
		CString	LoadChanAlmData(UINT uSel);
 		CString	LoadChanAlarm(UINT uSel);
 		CString	LoadChanMain(UINT uSel);
 		CString	LoadCustom(UINT uSel);
		CString	LoadDCOut(UINT uSel);
		CString	LoadDigitalIO(UINT uSel);
		CString	LoadGroupRecord(UINT uSel);
		CString	LoadGroupTrend(UINT uSel);
		CString	LoadHumidity(UINT uSel);
		CString	LoadInstrGeneral(UINT uSel);
		CString	LoadInstrString(UINT uSel);
		CString	LoadLgc2(UINT uSel);
		CString	LoadLgc8(UINT uSel);
		CString	LoadLoopDiagMain1(UINT uSel);
		CString	LoadLoopDiagMain2(UINT uSel);
		CString	LoadLoopOP1(UINT uSel);
		CString	LoadLoopOP2(UINT uSel);
		CString	LoadLoopPID1(UINT uSel);
		CString	LoadLoopPID2(UINT uSel);
		CString	LoadLoopSetup(UINT uSel);
		CString	LoadLoopSP1(UINT uSel);
		CString	LoadLoopSP2(UINT uSel);
		CString	LoadLoopTune(UINT uSel);
		CString	LoadMath2(UINT uSel);
		CString	LoadMux8(UINT uSel);
		CString	LoadNano(UINT uSel);
		CString	LoadNetwork(UINT uSel);
		CString	LoadNetStr(UINT uSel);
		CString	LoadOR(UINT uSel);
		CString	LoadSterilizer(UINT uSel);
		CString	LoadStrings(UINT uSel);
		CString	LoadTimer(UINT uSel);
		CString	LoadUsrLin(UINT uSel);
		CString	LoadUsrVal(UINT uSel);
		CString	LoadVirtAlmStat(UINT uSel);
		CString	LoadVirtAlarm(UINT uSel);
		CString	LoadVirtMain(UINT uSel);
		CString	LoadZirconia(UINT uSel);
	};

class CEuroInvensysNanoDacSerialDriver : public CEuroInvensysNanoDacDriver
{
	public:
		// Constructor
		CEuroInvensysNanoDacSerialDriver(void);

		// Destructor
		~CEuroInvensysNanoDacSerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);
	};

class CEuroInvensysNanoDacTCPDriver : public CEuroInvensysNanoDacDriver
{
	public:
		// Constructor
		CEuroInvensysNanoDacTCPDriver(void);

		// Destructor
		~CEuroInvensysNanoDacTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Invensys EuroInvensys Selection Dialog
//

class CEuroInvensysNanoDacAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEuroInvensysNanoDacAddrDialog(CEuroInvensysNanoDacDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Destructor
		~CEuroInvensysNanoDacAddrDialog(void);
		                
	protected:
		CEuroInvensysNanoDacDriver * m_pGDrv;

		EIESTRINGS	*m_pEIE;

		UINT		m_uNType;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk     (UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnComboChange(UINT uID, CWnd &Wnd);
		void	OnTypeChange (UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrides
		void	LoadType(void);
		BOOL	AllowType(UINT uType);
		void	ShowDetails(void);

		// Selection Handling
		UINT	GetOffsetFromPos(UINT uTable, UINT uPos);
		void	InitItemInfo(UINT uTable);
		void	InitFromAddr(CAddress Addr);
		void	LoadInfo(void);
		void	LoadNames(void);
		void	SetBoxPosition(UINT uID, UINT uPos);
		UINT	GetBoxPosition(UINT uID);
		void	SetBlockNumbers(void);
		CString	GetBlockString(UINT uSel);
		void	SetSegmNumbers(void);
		void	ShowSelection(void);
		BOOL	EnableSegments(void);

		// Helpers
		void	InitSelects(void);
		void	ClearSelData(void);
		void	ClearBox(CComboBox & ccb);
		void	ClearBox(UINT uID);
		void	ShowNanoDacDetails(void);
		void	ShowModbusAddr(void);
		UINT	GetModbusAddr(void);
		CString	StripAddr(CString sSelect);
		UINT	GetDataType(UINT uTable, UINT uSelect);
	};

#define	SNOTU	L"Not Defined"
#define	SNONE	L""

#define	SALSC1	L"AlarmSummary.Channel.Alarm[n].Ack@4498#3|2"

// Special Block Selection Strings
#define	SDCOA	L"3A3B"
#define	SDCOB	L"2A2B"
#define	SDCOC	L"1A1B"
#define	SDCOD	L"_DCOP."
#define	SDIOA	L"1A1B."
#define	SDIOB	L"2A2B."
#define	SDIOC	L"DI_LALC."
#define	SDIOD	L"3A3B."
#define	SDIOE	L"DI_LBLC."
#define	SDIOF	L"RELAY_4AC."
#define	SDIOG	L"RELAY_5AC."

/*
// Main header strings

	case SP_ALSC: s = L"AlarmSummary.Channel.Alarm[n].Q3[36";	break;
	case SP_ALSG: s = L"AlarmSummary.Q9[1";				break;
	case SP_ALSS: s = L"AlarmSummary.System.Alarm[n].Q1[32";	break;
	case SP_BCD:  s = L"BCDInput[n].Q12[2";				break;
	case SP_CALA: s = L"Channel[n].AlarmQ34[4";			break;
	case SP_CHAS: s = L"Channel[n].Q4[4";				break;
	case SP_CHMN: s = L"Channel[n].Q37[4";				break;
	case SP_CMSG: s = L"CustomMessage.Q20[1";			break;
	case SP_DCO:  s = L"DCOutput.Q11[3";				break;
	case SP_DIO:  s = L"DigitalIO.Q11[7";				break;
	case SP_GRCD: s = L"Group.Recording.Q33[1";			break;
	case SP_GTRD: s = L"Group.Trend.Q10[1";				break;
	case SP_HUM:  s = L"Humidity.Q10[1";				break;
	case SP_INSG: s = L"Instrument.Q95[1";				break;
	case SP_INSS: s = L"Instrument.Q29[1";				break;
	case SP_LGC2: s = L"Lgc2[n].Q7[12";				break;
	case SP_LGC8: s = L"Lgc8[n].Q13[2";				break;
	case SP_LDG1: s = L"Loop.1.Q33[1A22B22";			break;
	case SP_LDG2: s = L"Loop.2.Q33[1A22B22";			break;
	case SP_LOP1: s = L"Loop1.OP.Q38[1";				break;
	case SP_LOP2: s = L"Loop.2.OP.Q38[1";				break;
	case SP_LPD1: s = L"Loop1.PID.Q35[1";				break;
	case SP_LPD2: s = L"Loop.2.PID.Q35[1";				break;
	case SP_LSET: s = L"Loop.Q18[1";				break;
	case SP_LSP1: s = L"Loop.1.SP.Q21[1";				break;
	case SP_LSP2: s = L"Loop.2.SP.Q21[1";				break;
	case SP_LTUN: s = L"Loop.Q16[1";				break;
	case SP_MATH: s = L"Math2[n].Q13[13";				break;
	case SP_MUX:  s = L"Mux8[n].Q16[4";				break;
	case SP_NANO: s = L"nano_ui.Q2[1";				break;
	case SP_NET:  s = L"Network.Q53[1A31B31";			break;
	case SP_NETS: s = L"Network.Q17[1";				break;
	case SP_OR:   s = L"OR[n].Q9[12";				break;
	case SP_STER: s = L"Steriliser.Q46[1";				break;
	case SP_STRS: s = L"Q65[1";					break;
	case SP_TIME: s = L"Timer[n].Q6[4";				break;
	case SP_USRL: s = L"UserLin[n].Q33[4";				break;
	case SP_USRV: s = L"UsrVal[n].Q5[12";				break;
	case SP_VAAS: s = L"VirtualChannel[n].AlarmQ4[14";		break;
	case SP_VALA: s = L"VirtualChannel[n].AlarmQ34[14";		break;
	case SP_VMT:  s = L"VirtualChannel[n].Q22[14";			break;
	case SP_ZIR:  s = L"Zirconia.Q70[1";				break;
	}

Parameter String Loading
CString CEuroInvensysNanoDacDriver::LoadAlarmSumChan(UINT uSelect)
{
// SP_ALSC
	switch( uSelect % 4 ) {

case 2: return	L"Num@4496#3|0";
case 3: return	L"Status@4497#3|1";
		}

case 1:	L"Ack@4498#3|2";
	}

CString CEuroInvensysNanoDacDriver::LoadAlarmSumGbl(UINT uSelect)
{
// SP_ALSG
	switch( uSelect % 10) {

case 2: return	L"AnyChanAlarm@416|0";
case 3: return	L"AnySystemAlarm@417|1";
case 4: return	L"GlobalAck@419|3";
case 5: return	L"StatusWord_1@420|4";
case 6: return	L"StatusWord_2@421|5";
case 7: return	L"StatusWord_3@422|6";
case 8: return	L"StatusWord_4@423|7";
case 9: return	L"StatusWord_5@424|8";
		}

case 1:	L"AnyAlarm@418|2";
	}

CString CEuroInvensysNanoDacDriver::LoadAlarmSumSys(UINT uSelect)
{
// SP_ALSS
case 1:	L"ID@4624#1|0";
	}

CString CEuroInvensysNanoDacDriver::LoadBCD(UINT uSelect)
{
// SP_BCD
	switch( uSelect % 13 ) {

case 2: return	L"DecByte@11984#12|8";
case 3: return	L"In1@11976#12|0";
case 4: return	L"In2@11977#12|1";
case 5: return	L"In3@11978#12|2";
case 6: return	L"In4@11979#12|3";
case 7: return	L"In5@11980#12|4";
case 8: return	L"In6@11981#12|5";
case 9: return	L"In7@11982#12|6";
case 10: return	L"In8@11983#12|7";
case 11: return	L"Tens@11987#12|11";
case 12: return	L"Units@11986#12|10";
		}

case 1:	L"BCDVal@11985#12|9";
	}

CString CEuroInvensysNanoDacDriver::LoadChanAlmData(UINT uSelect)
{
// SP_CALA
	switch( uSelect % 35 ) {

case 2: return	L"1.Active@6219#128|11";
case 3: return	L"1.Amount@6216#128F|8";
case 4: return	L"1.AverageTime@6218#128|10";
case 5: return	L"1.Block@6210#128|2";
case 6: return	L"1.ChangeTime@6217#128|9";
case 7: return	L"1.Deviation@6215#128F|7";
case 8: return	L"1.Dwell@6213#128|5";
case 9: return	L"1.Hysteresis@6212#128F|4";
case 10: return	L"1.Inactive@6222#128|14";
case 11: return	L"1.Latch@6209#128|1";
case 12: return	L"1.NotAcknowledged@6223#128|15";
case 13: return	L"1.Reference@6214#128F|6";
case 14: return	L"1.Threshold@6211#128F|3";
case 15: return	L"1.Type@6208#128|0";
case 16: return	L"2.Acknowledgement@6256#128|33";
case 17: return	L"2.Active@6251#128|28";
case 18: return	L"2.Amount@6248#128F|25";
case 19: return	L"2.AverageTime@6250#128|27";
case 20: return	L"2.Block@6242#128|19";
case 21: return	L"2.ChangeTime@6249#128|26";
case 22: return	L"2.Deviation@6247#128F|24";
case 23: return	L"2.Dwell@6245#128|22";
case 24: return	L"2.Hysteresis@6244#128F|21";
case 25: return	L"2.Inactive@6254#128|31";
case 26: return	L"2.Latch@6241#128|18";
case 27: return	L"2.NotAcknowledged@6255#128|32";
case 28: return	L"2.Reference@6246#128F|23";
case 29: return	L"2.Threshold@6243#128F|20";
case 30: return	L"2.Type@6240#128|17";
case 31: return	L"1.x6220n@6220#128|12";
case 32: return	L"1.x6221n@6221#128|13";
case 33: return	L"2.x6252n@6252#128|29";
case 34: return	L"2.x6253n@6253#128|30";
		}

case 1:	L"1.Acknowledgement@6224#128|16";
	}

CString CEuroInvensysNanoDacDriver::LoadChanAlarm(UINT uSelect)
{
// SP_CHAS
	switch( uSelect % 5 ) {

case 2: return	L"Alarm1.Status@258#4$2|0";
case 3: return	L"Alarm2.Acknowledge@433#2$2|9";
case 4: return	L"Alarm2.Status@259#4$2|1";
		}

case 1:	L"Alarm1.Acknowledge@432#2$2|8";
	}

CString CEuroInvensysNanoDacDriver::LoadChanMain(UINT uSelect)
{
// SP_CHMN
	switch( uSelect % 38 ) {

case 2: return	L"Main.ExtCJTemp@6157#128$35|21";
case 3: return	L"Main.FaultResponse@6160#128$35|24";
case 4: return	L"Main.Filter@6158#128$35F|22";
case 5: return	L"Main.InputHigh@6148#128$35F|12";
case 6: return	L"Main.InputLow@6147#128$35F|11";
case 7: return	L"Main.InternalCJTemp@6165#128$35F|29";
case 8: return	L"Main.IPAdjustState@6166#128$35|30";
case 9: return	L"Main.LinType@6150#128$35|14";
case 10: return	L"Main.MeasuredValue@6164#128$35F|28";
case 11: return	L"Main.Offset@6167#128$35F|31";
case 12: return	L"Main.PV@256#4$2F|0";
case 13: return	L"Main.RangeHigh@6152#128$35F|16";
case 14: return	L"Main.RangeLow@6151#128$35F|15";
case 15: return	L"Main.RangeUnits@6153#128$35|17";
case 16: return	L"Main.Resolution@6145#128$35|9";
case 17: return	L"Main.ScaleHigh@6155#128$35F|19";
case 18: return	L"Main.ScaleLow@6154#128$35F|18";
case 19: return	L"Main.SensorBreakType@6159#128$35|23";
case 20: return	L"Main.SensorBreakVal@6161#128$35|25";
case 21: return	L"Main.Shunt@6149#128$35F|13";
case 22: return	L"Main.Status@257#4$2|1";
case 23: return	L"Main.TestSignal@6146#128$35|10";
case 24: return	L"Main.Type@6144#128$35|8";
case 25: return	L"Trend.Colour@6176#128$35|40";
case 26: return	L"Trend.SpanHigh@6178#128$35F|42";
case 27: return	L"Trend.SpanLow@6177#128$35F|41";
case 28: return	L"x6162@6162#128$35|26";
case 29: return	L"x6163@6163#128$35|27";
case 30: return	L"x6168@6168#128$35|32";
case 31: return	L"x6169@6169#128$35|33";
case 32: return	L"x6170@6170#128$35|34";
case 33: return	L"x6171@6171#128$35|35";
case 34: return	L"x6172@6172#128$35|36";
case 35: return	L"x6173@6173#128$35|37";
case 36: return	L"x6174@6174#128$35|38";
case 37: return	L"x6175@6175#128$35|39";
		}

case 1:	L"Main.CJType@6156#128F|20";
	}

CString CEuroInvensysNanoDacDriver::LoadCustom(UINT uSelect)
{
// SP_CMSG
	switch( uSelect % 21 ) {

case 2: return	L"Message2@24165$101L|111";
case 3: return	L"Message3@24266$101L|212";
case 4: return	L"Message4@24367$101L|313";
case 5: return	L"Message5@24468$101L|414";
case 6: return	L"Message6@24569$101L|515";
case 7: return	L"Message7@24670$101L|616";
case 8: return	L"Message8@24771$101L|717";
case 9: return	L"Message9@24872$101L|818";
case 10: return	L"Message10@24973$101L|919";
case 11: return	L"Trigger1@10480$1|0";
case 12: return	L"Trigger2@10481$1|1";
case 13: return	L"Trigger3@10482$1|2";
case 14: return	L"Trigger4@10483$1|3";
case 15: return	L"Trigger5@10484$1|4";
case 16: return	L"Trigger6@10485$1|5";
case 17: return	L"Trigger7@10486$1|6";
case 18: return	L"Trigger8@10487$1|7";
case 19: return	L"Trigger9@10488$1|8";
case 20: return	L"Trigger10@10489$1|9";
		}

case 1:	L"Message1@24064$101L|10";
	}

CString CEuroInvensysNanoDacDriver::LoadDCOut(UINT uSelect)
{
// SP_DCO
	switch( uSelect % 12 ) {

case 2: return	L"MeasuredValue@5546#16F|10";
case 3: return	L"OPAdjustState@5539#16|3";
case 4: return	L"OutputHigh@5542#16F|6";
case 5: return	L"OutputLow@5541#16F|5";
case 6: return	L"PV@5537#16F|1";
case 7: return	L"Resolution@5540#16|4";
case 8: return	L"ScaleHigh@5544#16F|8";
case 9: return	L"ScaleLow@5543#16F|7";
case 10: return	L"Status@5538#16|2";
case 11: return	L"Type@5536#16|0";
		}

case 1:	L"FallbackPV@5545#16F|9";
	}

CString CEuroInvensysNanoDacDriver::LoadDigitalIO(UINT uSelect)
{
// SP_DIO
	switch( uSelect % 12 ) {

case 2: return	L"Inertia@5383#16F|7";
case 3: return	L"Invert@5379#16|3";
case 4: return	L"MinOnTime@5378#16F|2";
case 5: return	L"ModuleIdent@5386#16|10";
case 6: return	L"Output@5380#16|4";
case 7: return	L"PV@5377#16F|1";
case 8: return	L"StandbyAction@5385#16|9";
case 9: return	L"Type@5376#16|0";
case 10: return	L"x5n@5381#16|5";
case 11: return	L"x6n@5382#16|6";
		}

case 1:	L"Backlash@5384#16F|8";
	}

CString CEuroInvensysNanoDacDriver::LoadGroupRecord(UINT uSelect)
{
// SP_GRCD
	switch( uSelect % 34 ) {

case 2: return	L"Channel2En@4132|4";
case 3: return	L"Channel3En@4133|5";
case 4: return	L"Channel4En@4134|6";
case 5: return	L"Compression@4160|32";
case 6: return	L"Enable@4128|0";
case 7: return	L"FlashDuration@4153F|25";
case 8: return	L"FlashFree@4152F|24";
case 9: return	L"FlashSize@4151F|23";
case 10: return	L"Interval@4130L|2";
case 11: return	L"Status@4150|22";
case 12: return	L"Suspend@4149|21";
case 13: return	L"VirtualChan1En@4135|7";
case 14: return	L"VirtualChan2En@4136|8";
case 15: return	L"VirtualChan3En@4137|9";
case 16: return	L"VirtualChan4En@4138|10";
case 17: return	L"VirtualChan5En@4139|11";
case 18: return	L"VirtualChan6En@4140|12";
case 19: return	L"VirtualChan7En@4141|13";
case 20: return	L"VirtualChan8En@4142|14";
case 21: return	L"VirtualChan9En@4143|15";
case 22: return	L"VirtualChan10En@4144|16";
case 23: return	L"VirtualChan11En@4145|17";
case 24: return	L"VirtualChan12En@4146|18";
case 25: return	L"VirtualChan13En@4147|19";
case 26: return	L"VirtualChan14En@4148|20";
case 27: return	L"x4129@4129|1";
case 28: return	L"x4154@4154|26";
case 29: return	L"x4155@4155|27";
case 30: return	L"x4156@4156|28";
case 31: return	L"x4157@4157|29";
case 32: return	L"x4158@4158|30";
case 33: return	L"x4159@4159|31";
		}

case 1:	L"Channel1En@4131|3";
	}

CString CEuroInvensysNanoDacDriver::LoadGroupTrend(UINT uSelect)
{
// SP_GTRD
	switch( uSelect % 11 ) {

case 2: return	L"MajorDivisions@4100|2";
case 3: return	L"Point1@4102|4";
case 4: return	L"Point2@4103|5";
case 5: return	L"Point3@4104|6";
case 6: return	L"Point4@4105|7";
case 7: return	L"Point5@4106|8";
case 8: return	L"Point6@4107|9";
case 9: return	L"x4099|1";
case 10: return	L"x4101|3";
		}

case 1:	L"Interval@4098L|0";
	}

CString CEuroInvensysNanoDacDriver::LoadHumidity(UINT uSelect)
{
// SP_HUM
	switch( uSelect % 11 ) {

case 2: return	L"DryTemp@11901F|5";
case 3: return	L"Pressure@11904F|8";
case 4: return	L"PsychroConst@11903F|7";
case 5: return	L"RelHumid@11896F|0";
case 6: return	L"Resolution@11905|9";
case 7: return	L"SBrk@11902|6";
case 8: return	L"WetOffset@11899F|3";
case 9: return	L"WetTemp@11900F|4";
case 10: return	L"x11898@11898|2";
		}

case 1:	L"DewPoint@11897F|1";
	}

CString CEuroInvensysNanoDacDriver::LoadInstrGeneral(UINT uSelect)
{
// SP_INSG
	switch( uSelect % 96 ) {

case 2: return	L"Clock.Time@4225|1";
case 3: return	L"Display.AlarmPanel@4331|79";
case 4: return	L"Display.Brightness@4240|16";
case 5: return	L"Display.DualLoopControl@4251|27";
case 6: return	L"Display.FaceplateCycling@4254|30";
case 7: return	L"Display.HistoryBackground@4264|40";
case 8: return	L"Display.HomePage@4243|19";
case 9: return	L"Display.HorizontalBar@4248|24";
case 10: return	L"Display.HorizontalTrend@4246|22";
case 11: return	L"Display.HPageTimeout@4244|20";
case 12: return	L"Display.HTrendScaling@4253|29";
case 13: return	L"Display.LoopControl@4250|26";
case 14: return	L"Display.LoopSetpointColour@4255|31";
case 15: return	L"Display.Numeric@4249|25";
case 16: return	L"Display.PromoteListView@4330|78";
case 17: return	L"Display.ScreenSaverAfter@4241|17";
case 18: return	L"Display.ScreenSaverBrightness@4242|18";
case 19: return	L"Display.TrendBackground@4252|28";
case 20: return	L"Display.VerticalBar@4247|23";
case 21: return	L"Display.VerticalTrend@4245|21";
case 22: return	L"Info.ConfigRev@4256L|32";
case 23: return	L"Info.IM@199|0";
case 24: return	L"Info.LineVoltage@4262F|38";
case 25: return	L"Info.MicroBoardIssue@4266|42";
case 26: return	L"Info.NvolWrites@4261L|37";
case 27: return	L"Info.PSUType@4265|41";
case 28: return	L"Info.SecurityRev@4260L|36";
case 29: return	L"Info.Type@4258|34";
case 30: return	L"IOFitted.1A1B@4340|88";
case 31: return	L"IOFitted.2A2B@4341|89";
case 32: return	L"IOFitted.3A3B@4343|91";
case 33: return	L"IOFitted.4AC@4345|93";
case 34: return	L"IOFitted.5AC@4346|94";
case 35: return	L"IOFitted.LALC@4342|90";
case 36: return	L"IOFitted.LBLC@4344|92";
case 37: return	L"Locale.DateFormat@4273|49";
case 38: return	L"Locale.DSTenable@4275|51";
case 39: return	L"Locale.EndDay@4282|58";
case 40: return	L"Locale.EndMonth@4283|59";
case 41: return	L"Locale.EndOn@4281|57";
case 42: return	L"Locale.EndTime@4280|56";
case 43: return	L"Locale.Language@4272|48";
case 44: return	L"Locale.StartDay@4278|54";
case 45: return	L"Locale.StartMonth@4279|55";
case 46: return	L"Locale.StartOn@4277|53";
case 47: return	L"Locale.StartTime@4276|52";
case 48: return	L"Locale.TimeZone@4274|50";
case 49: return	L"PromoteList.PromoteParam1@4320L|68";
case 50: return	L"PromoteList.PromoteParam2@4321L|69";
case 51: return	L"PromoteList.PromoteParam3@4322L|70";
case 52: return	L"PromoteList.PromoteParam4@4323L|71";
case 53: return	L"PromoteList.PromoteParam5@4324L|72";
case 54: return	L"PromoteList.PromoteParam6@4325L|73";
case 55: return	L"PromoteList.PromoteParam7@4326L|74";
case 56: return	L"PromoteList.PromoteParam8@4327L|75";
case 57: return	L"PromoteList.PromoteParam9@4328L|76";
case 58: return	L"PromoteList.PromoteParam10@4329L|77";
case 59: return	L"Security.CommsPass@4289|65";
case 60: return	L"Security.DefaultConfig@4290|66";
case 61: return	L"Security.EngineerAccess@4288|64";
case 62: return	L"Security.FeaturePass@4291L|67";
case 63: return	L"x4227@4227|3";
case 64: return	L"x4228@4228|4";
case 65: return	L"x4229@4229|5";
case 66: return	L"x4230@4230|6";
case 67: return	L"x4231@4231|7";
case 68: return	L"x4232@4232|8";
case 69: return	L"x4233@4233|9";
case 70: return	L"x4234@4234|10";
case 71: return	L"x4235@4235|11";
case 72: return	L"x4236@4236|12";
case 73: return	L"x4237@4237|13";
case 74: return	L"x4238@4238|14";
case 75: return	L"x4239@4239|15";
case 76: return	L"x4257@4257|33";
case 77: return	L"x4259@4259|35";
case 78: return	L"x4263@4263|39";
case 79: return	L"x4267@4267|43";
case 80: return	L"x4268@4268|44";
case 81: return	L"x4269@4269|45";
case 82: return	L"x4270@4270|46";
case 83: return	L"x4271@4271|47";
case 84: return	L"x4284@4284|60";
case 85: return	L"x4285@4285|61";
case 86: return	L"x4286@4286|62";
case 87: return	L"x4287@4287|63";
case 88: return	L"x4332@4332|80";
case 89: return	L"x4333@4333|81";
case 90: return	L"x4334@4334|82";
case 91: return	L"x4335@4335|83";
case 92: return	L"x4336@4336|84";
case 93: return	L"x4337@4337|85";
case 94: return	L"x4338@4338|86";
case 95: return	L"x4339@4339|87";
		}

case 1:	L"Clock.DST@4226|2";
	}

CString CEuroInvensysNanoDacDriver::LoadInstrString(UINT uSelect)
{
// SP_INSS
	switch( uSelect % 30 ) {

case 2:  return	L"Info.Name@17503L|95";
case 3:  return	L"Info.Version@17524L|116";
case 4:  return	L"Info.Bootrom@17530L|122";
case 5:  return	L"Notes.Note@21760L|140";
case 6:  return	L"Notes.Note1@21888L|280";
case 7:  return	L"Notes.Note2@22016L|420";
case 8:  return	L"Notes.Note3@22144L|560";
case 9:  return	L"Notes.Note4@22272L|700";
case 10: return	L"Notes.Note5@22400L|840";
case 11: return	L"Notes.Note6@22528L|980";
case 12: return	L"Notes.Note7@22656L|1120";
case 13: return	L"Notes.Note8@22784L|1260";
case 14: return	L"Notes.Note9@22912L|1400";
case 15: return	L"Notes.Note10@23040L|1540";
case 16: return	L"PromoteList.PromoteParam1Desc@25344L|1700";
case 17: return	L"PromoteList.PromoteParam2Desc@25365L|1721";
case 18: return	L"PromoteList.PromoteParam3Desc@25386L|1742";
case 19: return	L"PromoteList.PromoteParam4Desc@25407L|1763";
case 20: return	L"PromoteList.PromoteParam5Desc@25428L|1784";
case 21: return	L"PromoteList.PromoteParam6Desc@25449L|1805";
case 22: return	L"PromoteList.PromoteParam7Desc@25470L|1826";
case 23: return	L"PromoteList.PromoteParam8Desc@25491L|1847";
case 24: return	L"PromoteList.PromoteParam9Desc@25512L|1868";
case 25: return	L"PromoteList.PromoteParam10Desc@25533L|1889";
case 26: return	L"Security.EngineerPassword@25555L|2000";
case 27: return	L"Security.OperatorPassword@25655L|2100";
case 28: return	L"Security.PassPhrase@17482L|74";
case 29: return	L"Security.SupervisorPassword@25605L|2050";
		}

case 1:	L"Clock.Date@17408|0";
	}

CString CEuroInvensysNanoDacDriver::LoadLgc2(UINT uSelect)
{
// SP_LGC2
	switch( uSelect % 8 ) {

case 2: return	L"In1@12025#7F|1";
case 3: return	L"In2@12026#7F|2";
case 4: return	L"Invert@12028#7|4";
case 5: return	L"Oper@12024#7|0";
case 6: return	L"Out@12029#7|5";
case 7: return	L"OutputStatus@12030#7|6";
		}

case 1:	L"FallbackType@12027#7|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLgc8(UINT uSelect)
{
// SP_LGC8
	switch( uSelect % 14 ) {

case 2: return	L"In2@12112#13|4";
case 3: return	L"In3@12113#13|5";
case 4: return	L"In4@12114#13|6";
case 5: return	L"In5@12115#13|7";
case 6: return	L"In6@12116#13|8";
case 7: return	L"In7@12117#13|9";
case 8: return	L"In8@12118#13|10";
case 9: return	L"InInvert@12109#13|1";
case 10: return	L"NumIn@12110#13|2";
case 11: return	L"Oper@12108#13|0";
case 12: return	L"Out@12119#13|11";
case 13: return	L"OutInvert@12120#13|12";
		}

case 1:	L"In1@12111#13|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopDiagMain1(UINT uSelect)
{
// SP_LDG1
	switch( uSelect % 34 ) {

case 2: return	L"Diag.Error@525F|13";
case 3: return	L"Diag.IntegralOutContrib@529F|17";
case 4: return	L"Diag.LoopBreakAlarm@527F|15";
case 5: return	L"Diag.LoopMode@5777|22";
case 6: return	L"Diag.PropOutContrib@528F|16";
case 7: return	L"Diag.SBrk@531|19";
case 8: return	L"Diag.SchedCBH@5781F|26";
case 9: return	L"Diag.SchedCBL@5782F|27";
case 10: return	L"Diag.SchedLPBrk@5784F|29";
case 11: return	L"Diag.SchedMR@5783F|28";
case 12: return	L"Diag.SchedOPHi@5786F|31";
case 13: return	L"Diag.SchedOPLo@5787F|32";
case 14: return	L"Diag.SchedPB@5778F|23";
case 15: return	L"Diag.SchedR2G@5785F|30";
case 16: return	L"Diag.SchedTd@5780F|25";
case 17: return	L"Diag.SchedTi@5779F|24";
case 18: return	L"Diag.TargetOutVal@526F|14";
case 19: return	L"Diag.WrkOPHi@533F|21";
case 20: return	L"Diag.WrkOPLo@532F|20";
case 21: return	L"Main.ActiveOut@516F|4";
case 22: return	L"Main.AutoMan@513|1";
case 23: return	L"Main.Inhibit@517|5";
case 24: return	L"Main.IntHold@518|6";
case 25: return	L"Main.PV@512F|0";
case 26: return	L"Main.TargetSP@514F|2";
case 27: return	L"Main.WorkingSP@515F|3";
case 28: return	L"x519@519|7";
case 29: return	L"x520@520|8";
case 30: return	L"x521@521|9";
case 31: return	L"x522@522|10";
case 32: return	L"x523@523|11";
case 33: return	L"x524@524|12";
		}

case 1:	L"Diag.DerivativeOutContrib@530F|18";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopDiagMain2(UINT uSelect)
{
// SP_LDG2
	switch( uSelect % 34 ) {

case 2: return	L"Diag.Error@653F|13";
case 3: return	L"Diag.IntegralOutContrib@657F|17";
case 4: return	L"Diag.LoopBreakAlarm@655|15";
case 5: return	L"Diag.LoopMode@6033|22";
case 6: return	L"Diag.PropOutContrib@656F|16";
case 7: return	L"Diag.SBrk@659|19";
case 8: return	L"Diag.SchedCBH@6037F|26";
case 9: return	L"Diag.SchedCBL@6038F|27";
case 10: return	L"Diag.SchedLPBrk@6040F|29";
case 11: return	L"Diag.SchedMR@6039F|28";
case 12: return	L"Diag.SchedOPHi@6042F|31";
case 13: return	L"Diag.SchedOPLo@6043F|32";
case 14: return	L"Diag.SchedPB@6034F|23";
case 15: return	L"Diag.SchedR2G@6041F|30";
case 16: return	L"Diag.SchedTd@6036F|25";
case 17: return	L"Diag.SchedTi@6035F|24";
case 18: return	L"Diag.TargetOutVal@654F|14";
case 19: return	L"Diag.WrkOPHi@661F|21";
case 20: return	L"Diag.WrkOPLo@660F|20";
case 21: return	L"Main.ActiveOut@644F|4";
case 22: return	L"Main.AutoMan@641|1";
case 23: return	L"Main.Inhibit@645|5";
case 24: return	L"Main.IntHold@646|6";
case 25: return	L"Main.PV@640F|0";
case 26: return	L"Main.TargetSP@642F|2";
case 27: return	L"Main.WorkingSP@643F|3";
case 28: return	L"x647@647|7";
case 29: return	L"x648@648|8";
case 30: return	L"x649@649|9";
case 31: return	L"x650@650|10";
case 32: return	L"x651@651|11";
case 33: return	L"x652@652|12";
		}

case 1:	L"Diag.DerivativeOutContrib@658F|18";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopOP1(UINT uSelect)
{
// SP_LOP1
	switch( uSelect % 39 ) {

case 2: return	L"Loop1.OP.Ch1Out@523F|0";
case 3: return	L"Loop1.OP.Ch1PotBreak@5753|14";
case 4: return	L"Loop1.OP.Ch1PotPosition@5752F|13";
case 5: return	L"Loop1.OP.Ch1TravelTime@5748F|9";
case 6: return	L"Loop1.OP.Ch2Deadband@5743F|4";
case 7: return	L"Loop1.OP.Ch2OnOffHysteresis@5747F|8";
case 8: return	L"Loop1.OP.Ch2Out@524F|1";
case 9: return	L"Loop1.OP.Ch2PotBreak@5755|16";
case 10: return	L"Loop1.OP.Ch2PotPosition@5754F|15";
case 11: return	L"Loop1.OP.Ch2TravelTime@5749F|10";
case 12: return	L"Loop1.OP.CoolType@5763|24";
case 13: return	L"Loop1.OP.EnablePowerFeedforward@5761|22";
case 14: return	L"Loop1.OP.FeedForwardGain@5765F|26";
case 15: return	L"Loop1.OP.FeedForwardOffset@5766F|27";
case 16: return	L"Loop1.OP.FeedForwardTrimLimit@5767F|28";
case 17: return	L"Loop1.OP.FeedForwardType@5764|25";
case 18: return	L"Loop1.OP.FeedForwardVal@5768F|29";
case 19: return	L"Loop1.OP.FF_Rem@5773F|34";
case 20: return	L"Loop1.OP.ForcedOP@5775F|36";
case 21: return	L"Loop1.OP.ManStartup@5776|37";
case 22: return	L"Loop1.OP.ManualMode@5759|20";
case 23: return	L"Loop1.OP.ManualOutVal@5760F|21";
case 24: return	L"Loop1.OP.MeasuredPower@5762F|23";
case 25: return	L"Loop1.OP.NudgeLower@5751|12";
case 26: return	L"Loop1.OP.NudgeRaise@5750|11";
case 27: return	L"Loop1.OP.OutputHighLimit@5741F|2";
case 28: return	L"Loop1.OP.OutputLowLimit@5742F|3";
case 29: return	L"Loop1.OP.PotBreakMode@5756|17";
case 30: return	L"Loop1.OP.Rate@5744F|5";
case 31: return	L"Loop1.OP.RateDisable@5745|6";
case 32: return	L"Loop1.OP.RemOPH@5772F|33";
case 33: return	L"Loop1.OP.RemOPL@5771F|32";
case 34: return	L"Loop1.OP.SafeOutVal@5758F|19";
case 35: return	L"Loop1.OP.SbrkOP@5774F|35";
case 36: return	L"Loop1.OP.SensorBreakMode@5757|18";
case 37: return	L"Loop1.OP.TrackEnable@5770|31";
case 38: return	L"Loop1.OP.TrackOutVal@5769F|30";
		}

case 1:	L"Ch1OnOffHysteresis@5746F|7";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopOP2(UINT uSelect)
{
// SP_LOP2
	switch( uSelect % 39 ) {

case 2: return	L"Ch1Out@651F|0";
case 3: return	L"Ch1PotBreak@6009|14";
case 4: return	L"Ch1PotPosition@6008F|13";
case 5: return	L"Ch1TravelTime@6004F|9";
case 6: return	L"Ch2Deadband@5999F|4";
case 7: return	L"Ch2OnOffHysteresis@6003F|8";
case 8: return	L"Ch2Out@652F|1";
case 9: return	L"Ch2PotBreak@6011|16";
case 10: return	L"Ch2PotPosition@6010F|15";
case 11: return	L"Ch2TravelTime@6005F|10";
case 12: return	L"CoolType@6019|24";
case 13: return	L"EnablePowerFeedforward@6017|22";
case 14: return	L"FeedForwardGain@6021F|26";
case 15: return	L"FeedForwardOffset@6022F|27";
case 16: return	L"FeedForwardTrimLimit@6023F|28";
case 17: return	L"FeedForwardType@6020|25";
case 18: return	L"FeedForwardVal@6024F|29";
case 19: return	L"FF_Rem@6029F|34";
case 20: return	L"ForcedOP@6031F|36";
case 21: return	L"ManStartup@6032|37";
case 22: return	L"ManualMode@6015|20";
case 23: return	L"ManualOutVal@6016F|21";
case 24: return	L"MeasuredPower@6018F|23";
case 25: return	L"NudgeLower@6007|12";
case 26: return	L"NudgeRaise@6006|11";
case 27: return	L"OutputHighLimit@5997F|2";
case 28: return	L"OutputLowLimit@5998F|3";
case 29: return	L"PotBreakMode@6012|17";
case 30: return	L"Rate@6000F|5";
case 31: return	L"RateDisable@6001|6";
case 32: return	L"RemOPH@6028F|33";
case 33: return	L"RemOPL@6027F|32";
case 34: return	L"SafeOutVal@6014F|19";
case 35: return	L"SbrkOP@6030F|35";
case 36: return	L"SensorBreakMode@6013|18";
case 37: return	L"TrackEnable@6026|31";
case 38: return	L"TrackOutVal@6025F|30";
		}

case 1:	L"Ch1OnOffHysteresis@6002F|7";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopPID1(UINT uSelect)
{
// SP_LPD1
	switch( uSelect % 36 ) {

case 2: return	L"Boundary1-2@5689F|4";
case 3: return	L"Boundary2-3@5690F|5";
case 4: return	L"CutbackHigh@5695F|10";
case 5: return	L"CutbackHigh2@5703F|18";
case 6: return	L"CutbackHigh3@5711F|26";
case 7: return	L"CutbackLow@5696F|11";
case 8: return	L"CutbackLow2@5704F|19";
case 9: return	L"CutbackLow3@5712F|27";
case 10: return	L"DerivativeTime@5693F|8";
case 11: return	L"DerivativeTime2@5701F|16";
case 12: return	L"DerivativeTime3@5709F|24";
case 13: return	L"IntegralTime@5692F|7";
case 14: return	L"IntegralTime2@5700F|15";
case 15: return	L"IntegralTime3@5708F|23";
case 16: return	L"LoopBreakTime@5698F|13";
case 17: return	L"LoopBreakTime2@5706F|21";
case 18: return	L"LoopBreakTime3@5714F|29";
case 19: return	L"ManualReset@5697F|12";
case 20: return	L"ManualReset2@5705F|20";
case 21: return	L"ManualReset3@5713F|28";
case 22: return	L"NumSets@5686|1";
case 23: return	L"OutputHi@5715F|30";
case 24: return	L"OutputHi2@5717F|32";
case 25: return	L"OutputHi3@5719F|34";
case 26: return	L"OutputLo@5716F|31";
case 27: return	L"OutputLo2@5718F|33";
case 28: return	L"OutputLo3@5720F|35";
case 29: return	L"ProportionalBand@5691F|6";
case 30: return	L"ProportionalBand2@5699F|14";
case 31: return	L"ProportionalBand3@5707F|22";
case 32: return	L"RelCh2Gain@5694F|9";
case 33: return	L"RelCh2Gain2@5702F|17";
case 34: return	L"RelCh2Gain3@5710F|25";
case 35: return	L"SchedulerRemoteInput@5687F|2";
case 36: return	L"SchedulerType@5685|0";
		}

case 1:	L"ActiveSet@5688|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopPID2(UINT uSelect)
{
// SP_LPD2
	switch( uSelect % 36 ) {

case 2: return	L"Boundary1-2@5945F|4";
case 3: return	L"Boundary2-3@5946F|5";
case 4: return	L"CutbackHigh@5951F|10";
case 5: return	L"CutbackHigh2@5959F|18";
case 6: return	L"CutbackHigh3@5967F|26";
case 7: return	L"CutbackLow@5952F|11";
case 8: return	L"CutbackLow2@5960F|19";
case 9: return	L"CutbackLow3@5968F|27";
case 10: return	L"DerivativeTime@5949F|8";
case 11: return	L"DerivativeTime2@5957F|16";
case 12: return	L"DerivativeTime3@5965F|24";
case 13: return	L"IntegralTime@5948F|7";
case 14: return	L"IntegralTime2@5956F|15";
case 15: return	L"IntegralTime3@5964F|23";
case 16: return	L"LoopBreakTime@5954F|13";
case 17: return	L"LoopBreakTime2@5962F|21";
case 18: return	L"LoopBreakTime3@5970F|29";
case 19: return	L"ManualReset@5953F|12";
case 20: return	L"ManualReset2@5961F|20";
case 21: return	L"ManualReset3@5969F|28";
case 22: return	L"NumSets@5942|1";
case 23: return	L"OutputHi@5971F|30";
case 24: return	L"OutputHi2@5973F|32";
case 25: return	L"OutputHi3@5975F|34";
case 26: return	L"OutputLo@5972F|31";
case 27: return	L"OutputLo2@5974F|33";
case 28: return	L"OutputLo3@5976F|35";
case 29: return	L"ProportionalBand@5947F|6";
case 30: return	L"ProportionalBand2@5955F|14";
case 31: return	L"ProportionalBand3@5963F|22";
case 32: return	L"RelCh2Gain@5950F|9";
case 33: return	L"RelCh2Gain2@5958F|17";
case 34: return	L"RelCh2Gain3@5966F|25";
case 35: return	L"SchedulerRemoteInput@5943F|2";
case 36: return	L"SchedulerType@5941|0";
		}

case 1:	L"ActiveSet@5944|3";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopSetup(UINT uSelect)
{
// SP_LSET
	switch( uSelect % 19 ) {

case 2: return	L"1.Setup.CH1ControlType@5633|1";
case 3: return	L"1.Setup.CH2ControlType@5634|2";
case 4: return	L"1.Setup.ControlAction@5635|3";
case 5: return	L"1.Setup.DerivativeType@5637|5";
case 6: return	L"1.Setup.LoopName@23808|16";
case 7: return	L"1.Setup.LoopType@5632|0";
case 8: return	L"1.Setup.PBUnits@5636|4";
case 9: return	L"1.Setup.SPAccess@5799|6";
case 10: return	L"2.Setup.AutoManAccess@6056|15";
case 11: return	L"2.Setup.CH1ControlType@5889|9";
case 12: return	L"2.Setup.CH2ControlType@5890|10";
case 13: return	L"2.Setup.ControlAction@5891|11";
case 14: return	L"2.Setup.DerivativeType@5893|13";
case 15: return	L"2.Setup.LoopName@23824|17";
case 16: return	L"2.Setup.LoopType@5888|8";
case 17: return	L"2.Setup.PBUnits@5892|12";
case 18: return	L"2.Setup.SPAccess@6055|14";
		}

case 1:	L"1.AutoManAccess@5800|7";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopSP1(UINT uSelect)
{
// SP_LSP1
	switch( uSelect % 22 ) {

case 2: return	L"AltSPSelect@5729|9";
case 3: return	L"ManualTrack@5735|15";
case 4: return	L"RangeHigh@5721F|1";
case 5: return	L"RangeLow@5722F|2";
case 6: return	L"Rate@5730F|10";
case 7: return	L"RateDisable@5731|11";
case 8: return	L"RateDone@522|0";
case 9: return	L"ServoToPV@5740|20";
case 10: return	L"SP1@5724F|4";
case 11: return	L"SP2@5725F|5";
case 12: return	L"SPHighLimit@5726F|6";
case 13: return	L"SPIntBal@5739|19";
case 14: return	L"SPLowLimit@5727F|7";
case 15: return	L"SPSelect@5723|3";
case 16: return	L"SPTrack@5736|16";
case 17: return	L"SPTrim@5732F|12";
case 18: return	L"SPTrimHighLimit@5733F|13";
case 19: return	L"SPTrimLowLimit@5734F|14";
case 20: return	L"TrackPV@5737F|17";
case 21: return	L"TrackSP@5738F|18";
		}

case 1:	L"AltSP@5728F|8";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopSP2(UINT uSelect)
{
// SP_LSP2
	switch( uSelect % 22 ) {

case 2: return	L"AltSPSelect@5985|9";
case 3: return	L"ManualTrack@5991|15";
case 4: return	L"RangeHigh@5977F|1";
case 5: return	L"RangeLow@5978F|2";
case 6: return	L"Rate@5986F|10";
case 7: return	L"RateDisable@5987|11";
case 8: return	L"RateDone@650|0";
case 9: return	L"ServoToPV@5996|20";
case 10: return	L"SP1@5980F|4";
case 11: return	L"SP2@5981F|5";
case 12: return	L"SPHighLimit@5982F|6";
case 13: return	L"SPIntBal@5995|19";
case 14: return	L"SPLowLimit@5983F|7";
case 15: return	L"SPSelect@5979|3";
case 16: return	L"SPTrack@5992|16";
case 17: return	L"SPTrim@5988F|12";
case 18: return	L"SPTrimHighLimit@5989F|13";
case 19: return	L"SPTrimLowLimit@5990F|14";
case 20: return	L"TrackPV@5993F|17";
case 21: return	L"TrackSP@5994F|18";
		}

case 1:	L"AltSP@5984F|8";
	}

CString CEuroInvensysNanoDacDriver::LoadLoopTune(UINT uSelect)
{
// SP_LTUN
	switch( uSelect % 17 ) {

case 2: return	L"Loop.1.Tune.AutoTuneR2G@5684|10";
case 3: return	L"1.Tune.OutputHighLimit@5682F|8";
case 4: return	L"1.Tune.OutputLowLimit@5683F|9";
case 5: return	L"1.Tune.Stage@520|1";
case 6: return	L"1.Tune.StageTime@521F|2";
case 7: return	L"1.Tune.State@519|0";
case 8: return	L"1.Tune.Type@5680|6";
case 9: return	L"2.Tune.AutotuneEnable@5937|12";
case 10: return	L"2.Tune.AutoTuneR2G@5940|15";
case 11: return	L"2.Tune.OutputHighLimit@5938|13";
case 12: return	L"2.Tune.OutputLowLimit@5939|14";
case 13: return	L"2.Tune.Stage@648|4";
case 14: return	L"2.Tune.StageTime@649|5";
case 15: return	L"2.Tune.State@647|3";
case 16: return	L"2.Tune.Type@5936|11";
		}

case 1:	L"1.Tune.AutotuneEnable@5681|7";
	}

CString CEuroInvensysNanoDacDriver::LoadMath2(UINT uSelect)
{
// SP_MATH
	switch( uSelect % 14 ) {
		
case 2: return	L"FallbackVal@12203#13F|5";
case 3: return	L"HighLimit@12204#13F|6";
case 4: return	L"In1@12199#13F|1";
case 5: return	L"In1Mul@12198#13F|0";
case 6: return	L"In2@12201#13F|3";
case 7: return	L"In2Mul@12200#13F|2";
case 8: return	L"LowLimit@12205#13F|7";
case 9: return	L"Oper@12202#13|4";
case 10: return	L"Out@12206#13F|8";
case 11: return	L"Resolution@12210#13|12";
case 12: return	L"Select@12208#13|10";
case 13: return	L"Status@12209#13|11";
		}

case 1:	L"Fallback@12207#13|9";
	}

CString CEuroInvensysNanoDacDriver::LoadMux8(UINT uSelect)
{
// SP_MUX
	switch( uSelect % 17 ) {

case 2: return	L"FallbackVal@12135#16F|1";
case 3: return	L"HighLimit@12137#16F|3";
case 4: return	L"In1@12139#16F|5";
case 5: return	L"In2@12140#16F|6";
case 6: return	L"In3@12141#16F|7";
case 7: return	L"In4@12142#16F|8";
case 8: return	L"In5@12143#16F|9";
case 9: return	L"In6@12144#16F|10";
case 10: return	L"In7@12145#16F|11";
case 11: return	L"In8@12146#16F|12";
case 12: return	L"LowLimit@12138#16F|4";
case 13: return	L"Out@12147#16F|13";
case 14: return	L"Resolution@12149#16|15";
case 15: return	L"Select@12136#16|2";
case 16: return	L"Status@12148#16|14";
		}

case 1:	L"Fallback@12134#16F|0";
	}

CString CEuroInvensysNanoDacDriver::LoadNano(UINT uSelect)
{
// SP_NANO
case 1: L"Access@11264|0";
	}

CString CEuroInvensysNanoDacDriver::LoadNetwork(UINT uSelect)
{
// SP_NET
	switch( uSelect % 54 ) {

case 2: return	L"Archive.CSVDateFormat@4381|27";
case 3: return	L"Archive.CSVHeaders@4379|25";
case 4: return	L"Archive.CSVHeadings@4380|26";
case 5: return	L"Archive.CSVIncludeValues@4377|23";
case 6: return	L"Archive.CSVMessages@4378|24";
case 7: return	L"Archive.CSVTabDelimiter@4382|28";
case 8: return	L"Archive.Destination@4369|15";
case 9: return	L"Archive.FileFormat@4373|19";
case 10: return	L"Archive.MediaDuration@4376F|22";
case 11: return	L"Archive.MediaFree@4384F|30";
case 12: return	L"Archive.MediaSize@4375F|21";
case 13: return	L"Archive.OnFull@4374|20";
case 14: return	L"Archive.Period@4437|52";
case 15: return	L"Archive.Trigger@4435|50";
case 16: return	L"DemandArchive.PrimaryStatus@4432|47";
case 17: return	L"DemandArchive.SecStatus@4433|48";
case 18: return	L"DemandArchive.Status@4434|49";
case 19: return	L"DemandArchive.SuspendSchedule@4436|51";
case 20: return	L"Interface.IPType@4354|0";
case 21: return	L"Modbus.Address@4416|31";
case 22: return	L"Modbus.InputTimeout@4417|32";
case 23: return	L"Modbus.SerialMode@4419|34";
case 24: return	L"Modbus.TimeFormat@4420|35";
case 25: return	L"Modbus.UnitIdEnable@4418|33";
case 26: return	L"x4355@4355|1";
case 27: return	L"x4356@4356|2";
case 28: return	L"x4357@4357|3";
case 29: return	L"x4358@4358|4";
case 30: return	L"x4359@4359|5";
case 31: return	L"x4360@4360|6";
case 32: return	L"x4361@4361|7";
case 33: return	L"x4362@4362|8";
case 34: return	L"x4363@4363|9";
case 35: return	L"x4364@4364|10";
case 36: return	L"x4365@4365|11";
case 37: return	L"x4366@4366|12";
case 38: return	L"x4367@4367|13";
case 39: return	L"x4368@4368|14";
case 40: return	L"x4370@4370|16";
case 41: return	L"x4371@4371|17";
case 42: return	L"x4383@4383|29";
case 43: return	L"x4421@4421|36";
case 44: return	L"x4422@4422|37";
case 45: return	L"x4423@4423|38";
case 46: return	L"x4424@4424|39";
case 47: return	L"x4425@4425|40";
case 48: return	L"x4426@4426|41";
case 49: return	L"x4427@4427|42";
case 50: return	L"x4428@4428|43";
case 51: return	L"x4429@4429|44";
case 52: return	L"x4430@4430|45";
case 53: return	L"x4431@4431|46";
		}

case 1:	L"Archive.ArchiveRate@4372|18";
	}

CString CEuroInvensysNanoDacDriver::LoadNetStr(UINT uSelect)
{
// SP_NETS
	switch( uSelect % 18 ) {

case 2:  return	L"Archive.PrimaryUser@17894L|348";
case 3:  return	L"Archive.PServerIPAddress@17876L|330";
case 4:  return	L"Archive.RemotePath@17775L|220";
case 5:  return	L"Archive.SecondaryPassword@25795L|1095";
case 6:  return	L"Archive.SecondaryUser@17994L|518";
case 7:  return	L"Archive.SServerIPAddress@17976L|500";
case 8:  return	L"Archive.Trigger@4435L|0";
case 9:  return	L"DemandArchive.LastWrittenOn@18176L|700";
case 10: return	L"FTPserver.Password@25885L|1185";
case 11: return	L"FTPserver.Username@18094L|618";
case 12: return	L"Interface.ClientIdentifier@18197L|721";
case 13: return	L"Interface.Gateway@17700L|136";
case 14: return	L"Interface.IPaddress@17664L|100";
case 15: return	L"Interface.MAC@17736L|172";
case 16: return	L"Interface.SubnetMask@17682L|118";
case 17: return	L"Modbus.PrefMasterIP@18076L|600";
		}

case 1:	L"Archive.PrimaryPassword@25705L|1000";
	}

CString CEuroInvensysNanoDacDriver::LoadOR(UINT uSelect)
{
// SP_OR
	switch( uSelect % 10 ) {

case 2: return	L"Input2@11521#16|1";
case 3: return	L"Input3@11522#16|2";
case 4: return	L"Input4@11523#16|3";
case 5: return	L"Input5@11524#16|4";
case 6: return	L"Input6@11525#16|5";
case 7: return	L"Input7@11526#16|6";
case 8: return	L"Input8@11527#16|7";
case 9: return	L"Output@11528#16|8";
		}

case 1:	L"Input1@11520#16|0";
	}

CString CEuroInvensysNanoDacDriver::LoadSterilizer(UINT uSelect)
{
// SP_STER
	switch( uSelect % 47 ) {

case 2: return	L"CycleNumber@11780L|4";
case 3: return	L"CycleStatus@11784|8";
case 4: return	L"CycleTime@11813|37";
case 5: return	L"EquilibrationTime@11788|12";
case 6: return	L"FailureDwell1@11810|34";
case 7: return	L"FailureDwell2@11819|43";
case 8: return	L"FailureDwell3@11820|44";
case 9: return	L"FailureDwell4@11821|45";
case 10: return	L"FileByTag@11809|33";
case 11: return	L"Fvalue@11814|38";
case 12: return	L"Input1PV@11776F|0";
case 13: return	L"Input2PV@11777F|1";
case 14: return	L"Input3PV@11778F|2";
case 15: return	L"Input4PV@11779F|3";
case 16: return	L"InputType1@11805|29";
case 17: return	L"InputType2@11806|30";
case 18: return	L"InputType3@11807|31";
case 19: return	L"InputType4@11808|32";
case 20: return	L"IP1BandHigh@11786F|10";
case 21: return	L"IP1BandLow@11787F|11";
case 22: return	L"IP1TargetSP@11783F|7";
case 23: return	L"IP2BandHigh@11792F|16";
case 24: return	L"IP2BandLow@11793F|17";
case 25: return	L"IP2TargetSP@11798F|22";
case 26: return	L"IP3BandHigh@11794F|18";
case 27: return	L"IP3BandLow@11795F|19";
case 28: return	L"IP3TargetSP@11799F|23";
case 29: return	L"IP4BandHigh@11796F|20";
case 30: return	L"IP4BandLow@11797F|21";
case 31: return	L"IP4TargetSP@11800F|24";
case 32: return	L"LowLimit@11818F|42";
case 33: return	L"MeasuredTemp@11815F|39";
case 34: return	L"PassedOutput@11804|28";
case 35: return	L"Remaining@11790|14";
case 36: return	L"RunningOutput@11803|27";
case 37: return	L"Start121@11801|25";
case 38: return	L"Start134@11802|26";
case 39: return	L"StartCycle@11781|5";
case 40: return	L"SterilisingTime@11789|13";
case 41: return	L"TargetTemperature@11817F|41";
case 42: return	L"TargetTime@11785|9";
case 43: return	L"TargetTime121@11811|35";
case 44: return	L"TargetTime134@11812|36";
case 45: return	L"ZTemperatureInterval@11816F|40";
case 46: return	L"zz11782@11782|6";
		}

case 1:	L"AutoCounter@11791|15";
	}

CString CEuroInvensysNanoDacDriver::LoadStrings(UINT uSelect)
{
// SP_STRS
	switch( uSelect % 66 ) {

case 2: return	L"Channel.1.Main.Units@18709L|21";
case 3: return	L"Channel.2.Main.Descriptor@18715L|27";
case 4: return	L"Channel.2.Main.Units@18736L|48";
case 5: return	L"Channel.3.Main.Descriptor@18742L|54";
case 6: return	L"Channel.3.Main.Units@18763L|75";
case 7: return	L"Channel.4.Main.Descriptor@18769L|81";
case 8: return	L"Channel.4.Main.Units@18790L|102";
case 9: return	L"Group.Trend.Descriptor@23296L|1100";
case 10: return	L"Loop.1.Setup.LoopName@23808L|1400";
case 11: return	L"Loop.2.Setup.LoopName@23824L|1424";
case 12: return	L"Math2.1.Units@26948L|1532";
case 13: return	L"Math2.2.Units@26954L|1538";
case 14: return	L"Math2.3.Units@26960L|1544";
case 15: return	L"Math2.4.Units@26966L|1550";
case 16: return	L"Math2.5.Units@26972L|1556";
case 17: return	L"Math2.6.Units@26978L|1562";
case 18: return	L"Math2.7.Units@26984L|1568";
case 19: return	L"Math2.8.Units@26990L|1574";
case 20: return	L"Math2.9.Units@26996L|1580";
case 21: return	L"Math2.10.Units@27002L|1586";
case 22: return	L"Math2.11.Units@27008L|1592";
case 23: return	L"Math2.12.Units@27014L|1598";
case 24: return	L"nano_ui.Password@21504L|1000";
case 25: return	L"Steriliser.FileTag@26871L|1450";
case 26: return	L"UserVal.1.Units@26876L|1460";
case 27: return	L"UserVal.2.Units@26882L|1466";
case 28: return	L"UserVal.3.Units@26888L|1472";
case 29: return	L"UserVal.4.Units@26894L|1478";
case 30: return	L"UserVal.5.Units@26900L|1484";
case 31: return	L"UserVal.6.Units@26906L|1490";
case 32: return	L"UserVal.7.Units@26912L|1496";
case 33: return	L"UserVal.8.Units@26918L|1502";
case 34: return	L"UserVal.9.Units@26924L|1508";
case 35: return	L"UserVal.10.Units@26930L|1514";
case 36: return	L"UserVal.11.Units@26936L|1520";
case 37: return	L"UserVal.12.Units@26942L|1526";
case 38: return	L"VirtualChannel.1.Main.Descriptor@19200L|130";
case 39: return	L"VirtualChannel.1.Main.Units@19221L|160";
case 40: return	L"VirtualChannel.2.Main.Descriptor@19227L|190";
case 41: return	L"VirtualChannel.2.Main.Units@19248L|220";
case 42: return	L"VirtualChannel.3.Main.Descriptor@19254L|250";
case 43: return	L"VirtualChannel.3.Main.Units@19275L|280";
case 44: return	L"VirtualChannel.4.Main.Descriptor@19281L|310";
case 45: return	L"VirtualChannel.4.Main.Units@19302L|340";
case 46: return	L"VirtualChannel.5.Main.Descriptor@19308L|370";
case 47: return	L"VirtualChannel.5.Main.Units@19329L|400";
case 48: return	L"VirtualChannel.6.Main.Descriptor@19335L|430";
case 49: return	L"VirtualChannel.6.Main.Units@19356L|460";
case 50: return	L"VirtualChannel.7.Main.Descriptor@19362L|490";
case 51: return	L"VirtualChannel.7.Main.Units@19383L|520";
case 52: return	L"VirtualChannel.8.Main.Descriptor@19389L|550";
case 53: return	L"VirtualChannel.8.Main.Units@19410L|580";
case 54: return	L"VirtualChannel.9.Main.Descriptor@19416L|610";
case 55: return	L"VirtualChannel.9.Main.Units@19437L|640";
case 56: return	L"VirtualChannel.10.Main.Descriptor@19443L|670";
case 57: return	L"VirtualChannel.10.Main.Units@19464L|700";
case 58: return	L"VirtualChannel.11.Main.Descriptor@19470L|730";
case 59: return	L"VirtualChannel.11.Main.Units@19491L|760";
case 60: return	L"VirtualChannel.12.Main.Descriptor@19497L|790";
case 61: return	L"VirtualChannel.12.Main.Units@19518L|820";
case 62: return	L"VirtualChannel.13.Main.Descriptor@19524L|850";
case 63: return	L"VirtualChannel.13.Main.Units@19545L|880";
case 64: return	L"VirtualChannel.14.Main.Descriptor@19551L|910";
case 65: return	L"VirtualChannel.14.Main.Units@19573L|940";
		}

case 1:	L"Channel.1.Main.Descriptor@18688L|0";
	}

CString CEuroInvensysNanoDacDriver::LoadTimer(UINT uSelect)
{
// SP_TIME
	switch( uSelect % 7 ) {

case 2: return	L"In@12005#6|5";
case 3: return	L"Out@12001#6|1";
case 4: return	L"Time@12002#6|2";
case 5: return	L"Triggered@12003#6|3";
case 6: return	L"Type@12004#6|4";
		}

case 1:	L"ElapsedTime@12000#6|0";
	}

CString CEuroInvensysNanoDacDriver::LoadUsrLin(UINT uSelect)
{
// SP_USRL
	switch( uSelect % 4 ) {

		case 2: return	L"X@10497#192F|1";
		case 3: return	L"Y@10498#192F|2";
		}

	return	L"NumberOfBreakpoints@10496#192$65|0";
	}

CString CEuroInvensysNanoDacDriver::LoadUsrVal(UINT uSelect)
{
// SP_USRV
	switch( uSelect % 6 ) {
case 2: return	L"LowLimit@11917#5F|1";
case 3: return	L"Resolution@11920#5|4";
case 4: return	L"Status@11919#5|3";
case 5: return	L"Val@11918#5F|2";
		}

case 1:	L"HighLimit@11916#5F|0";
	}

CString CEuroInvensysNanoDacDriver::LoadVirtAlmStat(UINT uSelect)
{
// SP_VAAS
	switch( uSelect % 5 ) {

case 2: return	L"1.Status@290#4$2|0";
case 3: return	L"2.Acknowledge@449#2$2|29";
case 4: return	L"2.Status@291#4$2|1";
		}

case 1:	L"1.Acknowledge@448#2$2|28";
	}

CString CEuroInvensysNanoDacDriver::LoadVirtAlarm(UINT uSelect)
{
// SP_VALA
	switch( uSelect % 35 ) {

case 2: return	L"1.Active@7243#128|11";
case 3: return	L"1.Amount@7240#128F|8";
case 4: return	L"1.AverageTime@7242#128|10";
case 5: return	L"1.Block@7234#128|2";
case 6: return	L"1.ChangeTime@7241#128|9";
case 7: return	L"1.Deviation@7239#128F|7";
case 8: return	L"1.Dwell@7237#128|5";
case 9: return	L"1.Hysteresis@7236#128F|4";
case 10: return	L"1.Inactive@7246#128|14";
case 11: return	L"1.Latch@7233#128|1";
case 12: return	L"1.NotAcknowledged@7247#128|15";
case 13: return	L"1.Reference@7238#128F|6";
case 14: return	L"1.Threshold@7235#128F|3";
case 15: return	L"1.Type@7232#128|0";
case 16: return	L"2.Acknowledgement@7280#128|33";
case 17: return	L"2.Active@7275#128|28";
case 18: return	L"2.Amount@7272#128F|25";
case 19: return	L"2.AverageTime@7274#128|27";
case 20: return	L"2.Block@7266#128|19";
case 21: return	L"2.ChangeTime@7273#128|26";
case 22: return	L"2.Deviation@7271#128F|24";
case 23: return	L"2.Dwell@7269#128|22";
case 24: return	L"2.Hysteresis@7268#128F|21";
case 25: return	L"2.Inactive@7278#128|31";
case 26: return	L"2.Latch@7265#128|18";
case 27: return	L"2.NotAcknowledged@7279#128|32";
case 28: return	L"2.Reference@7270#128F|23";
case 29: return	L"2.Threshold@7267#128F|20";
case 30: return	L"2.Type@7264#128|17";
case 31: return	L"1.x7244n@7244#128|12";
case 32: return	L"1.x7245n@7245#128|13";
case 33: return	L"2.x7276n@7276#128|29";
case 34: return	L"2.x7277n@7277#128|30";
		}

case 1:	L"1.Acknowledgement@7248#128|16";
	}

CString CEuroInvensysNanoDacDriver::LoadVirtMain(UINT uSelect)
{
// SP_VMT
	switch( uSelect % 23 ) {

case 2: return	L"Main.HighCutOff@7173#128$21F|33";
case 3: return	L"Main.Input1@7175#128$21F|35";
case 4: return	L"Main.Input2@7176#128$21F|36";
case 5: return	L"Main.LowCutOff@7172#128$21F|32";
case 6: return	L"Main.ModbusInput@7174#128$21F|34";
case 7: return	L"Main.Operation@7169#128$21|29";
case 8: return	L"Main.Period@7178#128$21L|38";
case 9: return	L"Main.Preset@7180#128$21|40";
case 10: return	L"Main.PresetValue@7181#128$21F|41";
case 11: return	L"Main.PV@288#4$2F|0";
case 12: return	L"Main.Reset@7179#128$21|39";
case 13: return	L"Main.Resolution@7170#128$21|30";
case 14: return	L"Main.Rollover@7185#128$21|43";
case 15: return	L"Main.Status@289#4$2|1";
case 16: return	L"Main.TimeRemaining@7177#128$21|37";
case 17: return	L"Main.Trigger@7182#128$21|42";
case 18: return	L"Main.Type@7168#128$21|28";
case 19: return	L"Main.UnitsScaler@7171#128$21F|31";
case 20: return	L"Trend.Colour@7200#128$21|44";
case 21: return	L"Trend.SpanHigh@7202#128$21F|46";
case 22: return	L"Trend.SpanLow@7201#128$21F|45";
		}

case 1:	L"Main.Disable@7203#128$21|47";
	}

CString CEuroInvensysNanoDacDriver::LoadZirconia(UINT uSelect)
{
// SP_ZIR
	switch( uSelect % 71 ) {

case 2: return	L"BalanceIntegral@10397|29";
case 3: return	L"CarbonPot@10386F|18";
case 4: return	L"Clean.AbortClean@10421|53";
case 5: return	L"Clean.CantClean@10435|67";
case 6: return	L"Clean.CleanAbort@10436|68";
case 7: return	L"Clean.CleanEnable@10418|50";
case 8: return	L"Clean.CleanFreq@10410|42";
case 9: return	L"Clean.CleanMaxTemp@10420F|52";
case 10: return	L"Clean.CleanMsgReset@10419|51";
case 11: return	L"Clean.CleanProbe@10416|48";
case 12: return	L"Clean.CleanRecoveryTime@10422|54";
case 13: return	L"Clean.CleanTemp@10437|69";
case 14: return	L"Clean.CleanTime@10411|43";
case 15: return	L"Clean.CleanValve@10415|47";
case 16: return	L"Clean.LastCleanMv@10423F|55";
case 17: return	L"Clean.MaxRcovTime@10413|45";
case 18: return	L"Clean.MinRcovTime@10412|44";
case 19: return	L"Clean.ProbeFault@10414|46";
case 20: return	L"Clean.Time2Clean@10417|49";
case 21: return	L"CleanFreq@10377|9";
case 22: return	L"CleanProbe@10394|26";
case 23: return	L"CleanState@10393|25";
case 24: return	L"CleanTime@10378|10";
case 25: return	L"CleanValve@10392|24";
case 26: return	L"DewPoint@10387F|19";
case 27: return	L"GasRef@10370F|2";
case 28: return	L"GasRefs.CO_Ideal@10409F|41";
case 29: return	L"GasRefs.CO_InUse@10404F|36";
case 30: return	L"GasRefs.CO_Local@10401F|33";
case 31: return	L"GasRefs.CO_Remote@10402F|34";
case 32: return	L"GasRefs.CO_RemoteEn@10403|35";
case 33: return	L"GasRefs.H2_InUse@10408F|40";
case 34: return	L"GasRefs.H2_Local@10405F|37";
case 35: return	L"GasRefs.H2_Remote@10406F|38";
case 36: return	L"GasRefs.H2_RemoteEn@10407|39";
case 37: return	L"MaxRcovTime@10380|12";
case 38: return	L"MinCalTemp@10374F|6";
case 39: return	L"MinRcovTime@10379|11";
case 40: return	L"NumResolution@10369|1";
case 41: return	L"Oxygen@10388F|20";
case 42: return	L"OxygenExp@10381|13";
case 43: return	L"OxygenType@10400|32";
case 44: return	L"ProbeFault@10390|22";
case 45: return	L"ProbeInput@10384F|16";
case 46: return	L"ProbeOffset@10385F|17";
case 47: return	L"ProbeState@10399|31";
case 48: return	L"ProbeStatus@10396|28";
case 49: return	L"ProbeType@10368|0";
case 50: return	L"ProcFactor@10376F|8";
case 51: return	L"PVFrozen@10391|23";
case 52: return	L"RemGasEn@10372|4";
case 53: return	L"RemGasRef@10371F|3";
case 54: return	L"SootAlm@10389|21";
case 55: return	L"TempInput@10382F|14";
case 56: return	L"TempOffset@10383F|15";
case 57: return	L"Time2Clean@10395|27";
case 58: return	L"Tolerance@10375F|7";
case 59: return	L"WrkGas@10373F|5";
case 60: return	L"x10424@10424|56";
case 61: return	L"x10425@10425|57";
case 62: return	L"x10426@10426|58";
case 63: return	L"x10427@10427|59";
case 64: return	L"x10428@10428|60";
case 65: return	L"x10429@10429|61";
case 66: return	L"x10430@10430|62";
case 67: return	L"x10431@10431|63";
case 68: return	L"x10432@10432|64";
case 69: return	L"x10433@10433|65";
case 70: return	L"x10434@10434|66";
		}

case 1:	L"aC_CO_O2@10398F|30";
	}
*/
// End of File
#endif
