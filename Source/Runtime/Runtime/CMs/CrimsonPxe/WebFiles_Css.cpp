
#include "Intern.hpp"

#include "WebFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Helper
//

#define Entry(f, p) { f, data_##p, size_##p }

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

#include "bootstrap.min.css.dat"
#include "bootstrap-grid.min.css.dat"
#include "bootstrap-theme.min.css.dat"
#include "bootstrap-multiselect.css.dat"
#include "bootstrap-treeview.min.css.dat"
#include "font-awesome.min.css.dat"
#include "ie10-viewport-bug-workaround.css.dat"
#include "logarea.css.dat"
#include "logon.css.dat"
#include "syscmd.css.dat"
#include "sysdebug.css.dat"
#include "theme.css.dat"

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

CWebFileData const CWebFiles::m_Files1[] = {

	Entry("/assets/css/bootstrap-grid.min.css",			bootstrap_grid_min_css),
	Entry("/assets/css/bootstrap-theme.min.css",			bootstrap_theme_min_css),
	Entry("/assets/css/bootstrap-multiselect.css",			bootstrap_multiselect_css),
	Entry("/assets/css/bootstrap-treeview.min.css",			bootstrap_treeview_min_css),
	Entry("/assets/css/bootstrap.min.css",				bootstrap_min_css),
	Entry("/assets/css/font-awesome.min.css",			font_awesome_min_css),
	Entry("/assets/css/ie10-viewport-bug-workaround.css",		ie10_viewport_bug_workaround_css),
	Entry("/assets/css/logarea.css",				logarea_css),
	Entry("/assets/css/logon.css",					logon_css),
	Entry("/assets/css/syscmd.css",					syscmd_css),
	Entry("/assets/css/sysdebug.css",				sysdebug_css),
	Entry("/assets/css/theme.css",					theme_css),
};

UINT const CWebFiles::m_uCount1 = elements(m_Files1);

// End of File
