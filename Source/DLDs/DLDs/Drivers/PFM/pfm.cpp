
#include "intern.hpp"

#include "pfm.hpp"

////////////////////////////////////////////////////////////////////////
//
// Plant Floor Marquee Driver
//

// Colors

enum
{
	colBlack	= 0x0F,
	colRed		= 0x00,
	colGreen	= 0x01,
	colOrange	= 0x05,
	colYellow	= 0x04,
	colDarkRed	= 0x02,
	colDarkGreen	= 0x03
	};

// Instantiator

INSTANTIATE(CPFMDriver);

// Constructor

CPFMDriver::CPFMDriver(void)
{
	m_Ident          = DRIVER_ID;

	m_pDisplayHelper = NULL;

	m_dwInit         = 0;
	}

// Destructor

CPFMDriver::~CPFMDriver(void)
{
	m_pDisplayHelper = NULL;
	}

// Configuration
	
void MCALL CPFMDriver::CheckConfig(CSerialConfig &Config)
{
	/*Config.m_uPhysical = 2;*/
	}

// Management

void MCALL CPFMDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL CPFMDriver::Detach(void)
{
	if( m_pData ) {
	
		m_pData->Release();

		m_pData = NULL;
		}
	}

void MCALL CPFMDriver::Open(void)
{
	GetHelp();
	}

// Entry Points

UINT MCALL CPFMDriver::GetSize(int cx, int cy)
{
	int nb = (cx + 1) / 2;

	int hs = 2 * sizeof(INT);

	return nb * cy + hs;
	}

void MCALL CPFMDriver::Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait)
{
	switch( m_uType ) {

		case 0:

			CaptureMono(pDest, pSrc, px, py, cx, cy, uSize);

			break;

		case 1 :
		case 2 :
			CaptureVGA(pDest, pSrc, px, py, cx, cy, uSize, fPortrait);
			
			break;
		}
	}

void MCALL CPFMDriver::Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize)
{	
	DWORD dwMask = (1 << uDrop);

	if( 1 || !(m_dwInit & dwMask) || memcmp(pPrev, pData, uSize) ) {

		if( SendData(BYTE(uDrop), pData) ) {

			m_dwInit |=  dwMask;
			}
		else
			m_dwInit &= ~dwMask;
		}
	}

// Implementation

BOOL CPFMDriver::SendDrop(BYTE bDrop)
{
	UINT uPtr = 0;

	m_bTxData[uPtr++] = 0x02;
	m_bTxData[uPtr++] = 0xFF;
	m_bTxData[uPtr++] = 0x41;
	m_bTxData[uPtr++] = 0x01; // Length (Lo)
	m_bTxData[uPtr++] = 0x00; // Length (Hi)
	
	m_bTxData[uPtr++] = bDrop;
	
	m_bTxData[uPtr++] = 0x03;

	BYTE s = 0;

	for( UINT j = 0; j < uPtr; j++ ) {

		s = s ^ m_bTxData[j];
		}

	m_bTxData[uPtr++] = s;

	m_pData->ClearRx();
		
	m_pData->Write(m_bTxData, uPtr, FOREVER);

	m_pData->Write(NULL, 0, FOREVER);

	return WaitReply();
	}

BOOL CPFMDriver::SendData(BYTE bDrop, PBYTE pData)
{
	int nb = PU4(pData)[0];

	int cy = PU4(pData)[1];

	if( nb && cy ) {

		int  hs   = 2 * sizeof(INT);

		int  cx   = nb * 2;

		UINT uPtr = 0;

		m_bTxData[uPtr++] = 0x02;
		m_bTxData[uPtr++] = bDrop ? bDrop : 0xFF;
		m_bTxData[uPtr++] = 0x40;
		m_bTxData[uPtr++] = 0x00; // Length (Lo)
		m_bTxData[uPtr++] = 0x00; // Length (Hi)

		m_bTxData[uPtr++] = 0x04;
		m_bTxData[uPtr++] = LOBYTE(cx);
		m_bTxData[uPtr++] = HIBYTE(cx);
		m_bTxData[uPtr++] = LOBYTE(cy);
		m_bTxData[uPtr++] = HIBYTE(cy);

		for( int i = 0; i < nb * cy; i++ ) {

			m_bTxData[uPtr++] = pData[hs+i];
			}

		m_bTxData[3] = LOBYTE(uPtr-5);
		m_bTxData[4] = HIBYTE(uPtr-5);

		m_bTxData[uPtr++] = 0x03;

		BYTE s = 0;

		for( UINT j = 0; j < uPtr; j++ ) {

			s = s ^ m_bTxData[j];
			}

		m_bTxData[uPtr++] = s;

		m_pData->ClearRx();
			
		m_pData->Write(m_bTxData, uPtr, FOREVER);

		m_pData->Write(NULL, 0, FOREVER);

		return WaitReply();
		}

	return TRUE;
	}

BOOL CPFMDriver::WaitReply(void)
{
	SetTimer(500);

	/*AfxTrace("\nRx: ");*/

	while( GetTimer() ) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData < NOTHING ) {

			/*AfxTrace("%2.2X ", uData);*/
			}
		}

	/*AfxTrace("\n");*/

	return TRUE;
	}

void CPFMDriver::CaptureVGA(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait)
{
	static BYTE const cTable[] =
	
		{	colBlack,	// Black
			colDarkRed,	// Dk Red
			colDarkGreen,	// Dk Green
			colOrange,	// Dk Yellow
			colRed,		// Dk Blue
			colRed,		// Dk Magenta
			colRed,		// Dk Cyan
			colYellow,	// Lt Grey
			colYellow,	// Dk Grey
			colRed,		// Lt Red
			colGreen,	// Lt Green
			colYellow,	// Lt Yellow
			colRed,		// Lt Blue
			colRed,		// Lt Magenta
			colRed,		// Lt Cyan
			colYellow,	// White

			};

	BOOL fHigh = IsColor16();

	if( fPortrait ) {

		// TODO:  16- bit color portrait support

		int nb = (cy + 1) / 2;

		int hs = 2 * sizeof(UINT);

		PBYTE pData = pDest + hs;

		pSrc += px + py * uSize;

		pSrc += cy * uSize;

		for( int x = 0; x < cx; x++ ) {
		
			int p = x;

			for( int y = 0; y < nb; y++ ) {

				BYTE b0 = cTable[pSrc[p -= uSize] & 15];

				BYTE b1 = cTable[pSrc[p -= uSize] & 15];

				*pData++ = (b1 | (b0 << 4));			
				}
			}

		((PU4) pDest)[0] = nb;

		((PU4) pDest)[1] = cx;
		}
	else {
		if( fHigh ) {

			px    *= 2;

			uSize *= 2;
			}
		
		int nb = (cx + 1) / 2;

		int hs = 2 * sizeof(UINT);

		PBYTE pData = pDest + hs;

		pSrc += px + py * uSize;

		for( int y = 0; y < cy; y++ ) {

			int p = 0;

			for( int x = 0; x < nb; x++ ) {

				BYTE p0 = fHigh ? FindColor(pSrc, p, FALSE) : cTable[pSrc[p++] & 15];

				BYTE p1 = fHigh ? FindColor(pSrc, p, FALSE) : cTable[pSrc[p++] & 15];

				*pData++ = (p1 | (p0 << 4));
				}

			pSrc += uSize;
			}

		((PU4) pDest)[0] = nb;

		((PU4) pDest)[1] = cy;	 
		}
	}

void CPFMDriver::CaptureMono(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize)
{
	((PU4) pDest)[0] = 0;

	((PU4) pDest)[1] = 0;
	}

void CPFMDriver::GetHelp(void)
{
	if( !m_pDisplayHelper ) {
		
		if( MoreHelp(IDH_DISPLAY, (void **) &m_pDisplayHelper) ) {

			m_uType = m_pDisplayHelper->GetType();
			}
		}
	}

// Helpers

BYTE CPFMDriver::FindColor(PCBYTE pSrc, int &p, BOOL fPortrait)
{
	// TODO:  Portrait support
	
	UINT uColor = HostToIntel(MAKEWORD(pSrc[p], pSrc[p + 1]));

	p += 2;
	
	switch( uColor ) {

		case 0x0000:	return colBlack;	// Black
		case 0x3C00:	return colDarkRed;	// Dk Red
		case 0x01E0:	return colDarkGreen;	// Dk Green
		case 0x3DE0:	return colOrange;	// Dk Yellow
		case 0x03FF:	return colOrange;	// Dk Yellow
		case 0x2D6B:	return colYellow;	// Lt Grey
		case 0x4E73:	return colYellow;	// Dk Grey
		case 0x03E0:	return colGreen;	// Lt Green
		case 0x7FE0:	return colYellow;	// Lt Yellow
		case 0x7FFF:	return colYellow;	// White
		}

	return colRed;
	}

BOOL CPFMDriver::IsColor16(void)
{
	return m_uType == 2;
	}

// End of File
