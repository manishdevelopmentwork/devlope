
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat32FSInfo_HPP

#define	INCLUDE_Fat32FSInfo_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FS Info Structure
//

#pragma pack(1)

struct Fat32FSInfo
{
	DWORD	m_dwLeadSig;
	BYTE	m_bReserved1[480];
	DWORD	m_dwStrucSig;
	DWORD	m_dwFreeCount;
	DWORD	m_dwNextFree;
	BYTE	m_bReserved2[12];
	DWORD	m_dwTrailSig;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat32 FS Info Structure
//

class CFat32FSInfo : public Fat32FSInfo
{
	public:
		// Constructor
		CFat32FSInfo(void);

		// Initialisation
		void Init(void);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Dump
		void Dump(void) const;

	protected:
		// Constants
		static DWORD const constLeadSig	 = 0x41615252;
		static DWORD const constStrucSig = 0x61417272;
		static DWORD const constTrailSig = 0xAA550000;
		static DWORD const constUnknown	 = 0xFFFFFFFF;
	};

// End of File

#endif

