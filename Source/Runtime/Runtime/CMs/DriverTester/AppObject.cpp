
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();

	AfxNewObject("test-manager", IClientProcess, m_pObj);
	}

// Destructor

CAppObject::~CAppObject(void)
{
	AfxRelease(m_pObj);
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	return TRUE;
	}

INT CAppObject::TaskExec(UINT uTask)
{
	for(;;) Sleep(FOREVER);

	return 0;
	}

void CAppObject::TaskStop(UINT uTask)
{
	}

void CAppObject::TaskTerm(UINT uTask)
{
	}

// End of File
