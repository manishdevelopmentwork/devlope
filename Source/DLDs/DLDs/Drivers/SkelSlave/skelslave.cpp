
#include "intern.hpp"

#include "SkelSlave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Slave Driver
//

// Instantiator

INSTANTIATE(CSkeletonSlaveDriver);

// Constructor

CSkeletonSlaveDriver::CSkeletonSlaveDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CSkeletonSlaveDriver::~CSkeletonSlaveDriver(void)
{
	}

// Configuration

void MCALL CSkeletonSlaveDriver::Load(LPCBYTE pData)
{
	AfxTrace("CSkeletonSlaveDriver::Load\n");

	if( GetWord(pData) == 0x1234 ) {

		m_Drop = GetByte(pData);

		AfxTrace("CSkeletonSlaveDriver::Load -- Drop is %2.2X\n", m_Drop);
		}
	}
	
void MCALL CSkeletonSlaveDriver::CheckConfig(CSerialConfig &Config)
{
	AfxTrace("CSkeletonSlaveDriver::CheckConfig\n");
	}
	
// Management

void MCALL CSkeletonSlaveDriver::Attach(IPortObject *pPort)
{
	AfxTrace("CSkeletonSlaveDriver::Bind\n");

	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSkeletonSlaveDriver::Open(void)
{
	AfxTrace("CSkeletonSlaveDriver::Open\n");
	}

// Entry Points

void MCALL CSkeletonSlaveDriver::Service(void)
{	
	AfxTrace("CSkeletonSlaveDriver::Service\n");

	for(;;) {

		AcceptRequest();
		}
	}

// Implementation

void CSkeletonSlaveDriver::AcceptRequest(void)
{
	UINT uState = 0;

	UINT uAddr  = 0;

	UINT uValue = 1;

	SetTimer(1000);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		SetTimer(1000);

		if( uData == NOTHING ) {

			uState = 0;

			continue;
			}

		switch( uState ) {

			case 0:
				if( uData == '?' ) {

					uAddr  = 0;

					uState = 1;
					}
				break;

			case 1:
				if( isdigit(uData) ) {

					uAddr *= 10;

					uAddr += uData - '0';
					}
				else {
					if( uData == '=' ) {

						uValue = 0;

						uState = 2;
						}
					else {
						if( uData == '\r' ) {

							ProcessRead(uAddr);
							}

						uState = 0;
						}
					}
				break;

			case 2:
				if( isdigit(uData) ) {

					uValue *= 10;

					uValue += uData - '0';
					}
				else {
					if( uData == '\r' ) {

						ProcessWrite(uAddr, uValue);
						}

					uState = 0;
					}
				break;
			}
		}
	}

void CSkeletonSlaveDriver::ProcessRead(UINT uAddr)
{
	char s[128];

	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = uAddr;

	DWORD dwData[1];

	if( COMMS_SUCCESS(Read(Addr, dwData, 1)) ) {

		SPrintf(s, "Value of D%u is %u\r\n", uAddr, dwData[0]);

		m_pData->Write(PCBYTE(s), strlen(s), NOTHING);

		return;
		}

	SPrintf(s, "Value of D%u is not mapped\r\n", uAddr);

	m_pData->Write(PCBYTE(s), strlen(s), NOTHING);
	}

void CSkeletonSlaveDriver::ProcessWrite(UINT uAddr, UINT uValue)
{
	char s[128];

	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Table  = 'D';
	Addr.a.m_Offset = uAddr;

	DWORD dwData[1] = { uValue };

	if( COMMS_SUCCESS(Write(Addr, dwData, 1)) ) {

		SPrintf(s, "Value of D%u set to %u\r\n", uAddr, dwData[0]);

		m_pData->Write(PCBYTE(s), strlen(s), NOTHING);

		return;
		}

	SPrintf(s, "Value of D%u is not mapped\r\n", uAddr);

	m_pData->Write(PCBYTE(s), strlen(s), NOTHING);
	}

// End of File
