
#include "Intern.hpp"

#include "PlatformGraphite.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IoMux51.hpp"

#include "Dpll51.hpp"

#include "Ccm51.hpp"

#include "Pmui51.hpp"

#include "Ipu51.hpp"

#include "Pwm51.hpp"

#include "Pic12F.hpp"

#include "Pic/Pic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Identifiers
//

#include "..\..\HXLs\HxUsb\UsbIdentifiers.hpp"

#include "..\..\HXLs\HxUsb\Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice     * Create_Uart51(UINT i, CCcm51 *pCcm);
extern IDevice     * Create_Display51(UINT uModel, CIpu51 *pIpu, CPwm51 *pPwm);
extern IDevice     * Create_Mr25Hxx(UINT uChan);
extern IDevice     * Create_Nand51(CCcm51 *pCcm);
extern IDevice     * Create_Beeper51(CPwm51 *pPwm);
extern IDevice     * Create_TouchScreen51(UINT uModel, CPmui51 *pPmui);
extern IDevice     * Create_SdHost51(CCcm51 *pCcm);
extern IDevice     * Create_M24LR(void);
extern IDevice     * Create_Rtc51(CPmui51 *pPmui);
extern IDevice     * Create_RtcAbmc(void);
extern IDevice     * Create_Nic51(void);
extern IDevice     * Create_Identity(UINT uAddr);
extern IDevice     * Create_Leds(ILeds *pLeds);
extern IDevice     * Create_InputSwitch(IInputSwitch *pInput);
extern IDevice	   * Create_UsbHub251x(void);
extern IUsbDriver  * Create_UsbHost51(UINT iIndex);
extern IUsbDriver  * Create_UsbFunc51(UINT iIndex);
extern IUsbDriver  * Create_UsbHostEnhanced(void);
extern IUsbDriver  * Create_UsbHostController(void);
extern IDevice     * Create_UsbSerialFtdi(IUsbHostFuncDriver *pDriver);
extern IPortSwitch * Create_UsbSwitchFtdi(IUsbHostFuncDriver *pDriver, UINT uType, UINT uBit);
extern IDevice	   * Create_UsbNetwork(IUsbHostFuncDriver *pDriver);
extern IDevice     * Create_UsbNetworkCdc(IUsbHostFuncDriver *pDriver);
extern IDevice     * Create_UsbSerialCdc(IUsbHostFuncDriver *pDriver);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Graphite HMI
//

// Instantiator

IDevice * Create_PlatformGraphite(UINT uModel)
{
	return (IDevice *) (CPlatformMX51 *) New CPlatformGraphite(uModel);
}

// Constructor

CPlatformGraphite::CPlatformGraphite(UINT uModel)
{
	m_uModel     = uModel;

	m_uRackLoad  = 0;

	m_uRackLimit = GetPortCount() * 43;

	m_pHub       = NULL;
}

// Destructor

CPlatformGraphite::~CPlatformGraphite(void)
{
	piob->RevokeGroup("dev.");

	delete m_pIpu;

	delete m_pPwm[0];

	delete m_pPwm[1];
}

// IUnknown

HRESULT CPlatformGraphite::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPortSwitch);

	StdQueryInterface(IInputSwitch);

	StdQueryInterface(ILeds);

	StdQueryInterface(IUsbSystem);

	StdQueryInterface(IUsbSystemPortMapper);

	StdQueryInterface(IUsbSystemPower);

	return CPlatformMX51::QueryInterface(riid, ppObject);
}

ULONG CPlatformGraphite::AddRef(void)
{
	return CPlatformMX51::AddRef();
}

ULONG CPlatformGraphite::Release(void)
{
	return CPlatformMX51::Release();
}

// IDevice

BOOL CPlatformGraphite::Open(void)
{
	if( CPlatformMX51::Open() ) {

		m_pIpu    = New CIpu51(m_pCcm);

		m_pPwm[0] = New CPwm51(0, m_pCcm);

		m_pPwm[1] = New CPwm51(1, m_pCcm);

		piob->RegisterSingleton("dev.uart", 0, Create_Uart51(0, m_pCcm));

		piob->RegisterSingleton("dev.uart", 1, Create_Uart51(1, m_pCcm));

		#if !defined(_DEBUG)

		piob->RegisterSingleton("dev.uart", 2, Create_Uart51(2, m_pCcm));

		#endif

		piob->RegisterSingleton("dev.rfid", 0, Create_M24LR());

		piob->RegisterSingleton("dev.nand", 0, Create_Nand51(m_pCcm));

		piob->RegisterSingleton("dev.beeper", 0, Create_Beeper51(m_pPwm[1]));

		piob->RegisterSingleton("dev.fram", 0, Create_Mr25Hxx(1));

		piob->RegisterSingleton("dev.ident", 0, Create_Identity(addrIdentity));

		piob->RegisterSingleton("dev.display", 0, Create_Display51(m_uModel, m_pIpu, m_pPwm[0]));

		piob->RegisterSingleton("dev.touch", 0, Create_TouchScreen51(m_uModel, m_pPmui));

		piob->RegisterSingleton("dev.sdhost", 0, Create_SdHost51(m_pCcm));

		piob->RegisterSingleton("dev.nic", 0, Create_Nic51());

		piob->RegisterSingleton("dev.leds", 0, Create_Leds(this));

		piob->RegisterSingleton("dev.input-s", 0, Create_InputSwitch(this));

		piob->RegisterSingleton("dev.usbctrl-d", 0, Create_UsbFunc51(0));

		piob->RegisterSingleton("dev.usbctrl-h", 0, Create_UsbHost51(1));

		piob->RegisterSingleton("dev.usbctrl-h", 1, Create_UsbHost51(2));

		piob->RegisterSingleton("dev.usbctrl-e", 0, Create_UsbHostEnhanced());

		piob->RegisterSingleton("dev.usbctrl-e", 1, Create_UsbHostEnhanced());

		piob->RegisterSingleton("dev.usbctrl-c", 0, Create_UsbHostController());

		piob->RegisterSingleton("dev.usbctrl-c", 1, Create_UsbHostController());

		piob->RegisterSingleton("dev.usbhub", 0, Create_UsbHub251x());

		if( ReadRfid() ) {

			piob->RegisterSingleton("dev.rtc", 0, Create_RtcAbmc());
		}
		else {
			piob->RegisterSingleton("dev.rtc", 0, Create_Rtc51(m_pPmui));
		}

		InitUarts();

		InitUsb();

		InitNicMac(0);

		FindSerial();

		ProgramPic();

		LoadLedLevel();

		RegisterRedCommon();

		InitRack();

		return TRUE;
	}

	return FALSE;
}

// IPortSwitch

UINT METHOD CPlatformGraphite::GetCount(UINT uUnit)
{
	switch( uUnit ) {

		case 0:	return 1;

		case 1:	return 1;

		case 2:	return 1;

		case 3:	return 1;
	}

	return 0;
}

UINT METHOD CPlatformGraphite::GetMask(UINT uUnit)
{
	switch( uUnit ) {

		case 0:	return
			(1 << physicalRS232);

		case 1:	return
			(1 << physicalRS485)       |
			(1 << physicalRS422Master) |
			(1 << physicalRS422Slave);

		case 2:	return
			(1 << physicalRS232);

		case 3:	return
			(1 << physicalRS485)       |
			(1 << physicalRS422Master) |
			(1 << physicalRS422Slave);
	}

	return physicalNone;
}

UINT METHOD CPlatformGraphite::GetType(UINT uUnit, UINT uLog)
{
	switch( uUnit ) {

		case 0:
			return physicalRS232;

		case 1:
			return physicalRS485;

		case 2:
			return physicalRS232;

		case 3:
			return physicalRS485;
	}

	return physicalNone;
}

BOOL METHOD CPlatformGraphite::EnablePort(UINT uUnit, BOOL fEnable)
{
	if( uUnit == 1 ) {

		if( !fEnable ) {

			m_pGpio[3]->SetState(20, false);
		}

		return true;
	}

	return false;
}

BOOL METHOD CPlatformGraphite::SetPhysical(UINT uUnit, BOOL fRS485)
{
	return false;
}

BOOL METHOD CPlatformGraphite::SetFull(UINT uUnit, BOOL fFull)
{
	if( uUnit == 1 ) {

		m_pGpio[2]->SetState(1, !fFull);

		return true;
	}

	return false;
}

BOOL METHOD CPlatformGraphite::SetMode(UINT uUnit, BOOL fAuto)
{
	if( uUnit == 1 ) {

		m_pGpio[3]->SetState(20, fAuto);

		return true;
	}

	return false;
}

// ILeds

void METHOD CPlatformGraphite::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 0 ) {

		m_pPmui->SetLed(iLed, uState == stateOff ? 0 : m_uLedLevel);
	}
}

void METHOD CPlatformGraphite::SetLedLevel(UINT uPercent, bool fPersist)
{
	m_uLedLevel = uPercent;

	if( fPersist ) {

		SaveLedLevel();
	}
}

UINT METHOD CPlatformGraphite::GetLedLevel(void)
{
	return m_uLedLevel;
}

// IInputSwitch

UINT METHOD CPlatformGraphite::GetSwitches(void)
{
	return 0xFFFFFFFE | GetSwitch(0);
}

UINT METHOD CPlatformGraphite::GetSwitch(UINT uSwitch)
{
	if( uSwitch == 0 ) {

		return m_pGpio[0]->GetState(6) ? 1 : 0;
	}

	return 1;
}

// IUsbSystem

void METHOD CPlatformGraphite::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDev = NULL;

	pDriver->GetDevice(pDev);

	UsbTreePath const &Path = pDev->GetTreePath();

	if( GetPortType(Path) == typeSystem ) {

		if( pDriver->GetClass() == devHub ) {

			if( Path.a.dwCtrl == 0 ) {

				m_pHub = (IUsbHubDriver *) pDriver;
			}
		}

		if( pDriver->GetVendor() == vidSmc && pDriver->GetProduct() == pidSmcLan9512 ) {

			piob->RegisterSingleton("dev.nic", 1, Create_UsbNetwork(pDriver));

			InitNicMac(1);

			return;
		}

		if( pDriver->GetVendor() == vidFtdi && pDriver->GetProduct() == pidFtdi2232 ) {

			if( pDriver->GetInterface() == 0 ) {

				pDev->SetObject(0, Create_UsbSwitchFtdi(pDriver, physicalRS485, 4));
			}

			if( pDriver->GetInterface() == 1 ) {

				IPortSwitch *pSwitch = (IPortSwitch *) pDev->GetObject(0);

				IPortObject *pObject = (IPortObject *) Create_UsbSerialFtdi(pDriver);

				pObject->Bind(pSwitch);

				piob->RegisterSingleton("dev.uart", 3, pObject);
			}

			return;
		}

		return;
	}

	if( GetPortType(Path) == typeOption ) {

		return;
	}

	if( GetPortType(Path) == typeExtern ) {

		return;
	}
}

void METHOD CPlatformGraphite::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
}

// IUsbSystemPortMapper

UINT METHOD CPlatformGraphite::GetPortCount(void)
{
	switch( m_uModel ) {

		case MODEL_GRAPHITE_07:	 return 5;
		case MODEL_GRAPHITE_09:	 return 6;
		case MODEL_GRAPHITE_10V: return 7;
		case MODEL_GRAPHITE_10S: return 7;
		case MODEL_GRAPHITE_12:  return 8;
		case MODEL_GRAPHITE_15:  return 8;
	}

	return 0;
}

UINT METHOD CPlatformGraphite::GetPortType(UsbTreePath const &Path)
{
	if( Path.a.dwCtrl == 0 && Path.a.dwHost == 0 && Path.a.dwPort1 == 0 ) {

		if( Path.a.dwTier == 1 ) {

			return typeSystem;
		}

		if( Path.a.dwTier == 2 ) {

			switch( Path.a.dwPort2 ) {

				case 2:
				case 3:
					return typeExtern;

				case 1:
				case 4:
				case 5:
				case 6:
				case 7:
					return typeOption;
			}
		}

		if( Path.a.dwTier > 2 ) {

			return typeExtern;
		}

		return typeUnknown;
	}

	if( Path.a.dwCtrl == 1 && Path.a.dwHost == 0 && Path.a.dwPort1 == 0 ) {

		if( Path.a.dwTier == 1 ) {

			return typeSystem;
		}

		if( Path.a.dwTier == 2 ) {

			switch( Path.a.dwPort2 ) {

				case 2:
				case 3:
				case 4:
					return typeOption;

				case 1:
				case 5:
					return typeSystem;
			}
		}

		return typeUnknown;
	}

	return typeUnknown;
}

UINT METHOD CPlatformGraphite::GetPortReset(UsbTreePath const &Path)
{
	return NOTHING;
}

// IUsbSystemPower

void METHOD CPlatformGraphite::OnNewDevice(UsbTreePath const &Path, UINT uPower)
{
	if( GetPortType(Path) == typeOption ) {

		m_uRackLoad += uPower;

		if( m_uRackLoad > m_uRackLimit ) {

			m_pHub->ClrPortFeature(1, selPortPower);
		}
	}
}

void METHOD CPlatformGraphite::OnDelDevice(UsbTreePath const &Path, UINT uPower)
{
	if( GetPortType(Path) == typeOption ) {

		if( m_uRackLoad > uPower ) {

			m_uRackLoad -= uPower;
		}
	}
}

// Initialisation

void CPlatformGraphite::InitPriorities(void)
{
	CPlatformMX51::InitPriorities();

	phal->SetLinePriority(INT_UART1, 1);
	phal->SetLinePriority(INT_UART2, 1);
	phal->SetLinePriority(INT_UART3, 1);
	phal->SetLinePriority(INT_SDHC1, 1);
	phal->SetLinePriority(INT_FEC, 1);
	phal->SetLinePriority(INT_IPUEX_S, 1);
	phal->SetLinePriority(INT_IPUEX_E, 1);
	phal->SetLinePriority(INT_GPU2D_GEN, 5);
	phal->SetLinePriority(INT_EMI_NFC, 1);
	phal->SetLinePriority(INT_USBOHOTG, 3);
	phal->SetLinePriority(INT_USBOH1, 4);
	phal->SetLinePriority(INT_USBOH2, 4);
}

void CPlatformGraphite::InitClocks(void)
{
	m_pCcm->SetGating(true);

	m_pCcm->Set(CCcm51::clkUsbPhy, CCcm51::clkOsc, 1, 1);

	if( m_pCcm->GetFreq(CCcm51::clkPll1) != 800000000 ) {

		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll2, 4);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkArmAxi, CCcm51::clkAxia);
		m_pCcm->Set(CCcm51::clkDdr, CCcm51::clkAxia, 4);

		// DPLL Erratum

		m_pDpll[0]->Set(CDpll51::refOsc, 8, 180, 180, 1, true);

		m_pDpll[0]->Set(60);

		for( UINT i = 0; i < 4000; i++ ) {

			ASM("nop\r\n");
		}

		m_pCcm->Set(CCcm51::clkArmRoot, CCcm51::clkPll1, 1);
		m_pCcm->Set(CCcm51::clkDdr, CCcm51::clkPll1, 4);
		m_pCcm->Set(CCcm51::clkSdhc1, CCcm51::clkPll1, 4, 4);
	}

	if( m_pCcm->GetFreq(CCcm51::clkPll2) != 665000000 ) {

		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll2, 6);
		m_pCcm->Set(CCcm51::clkAhb, CCcm51::clkPll2, 6);
		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll1, 5);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll1, 6);
		m_pCcm->Set(CCcm51::clkAhb, CCcm51::clkPll1, 6);

		m_pDpll[1]->Set(CDpll51::refOsc, 6, 89, 96, 1, true);

		for( UINT i = 0; i < 4000; i++ ) {

			ASM("nop\r\n");
		}

		m_pCcm->Set(CCcm51::clkAxia, CCcm51::clkPll2, 4);
		m_pCcm->Set(CCcm51::clkAxib, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkAhb, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkEmi, CCcm51::clkPll2, 5);
		m_pCcm->Set(CCcm51::clkIpg, CCcm51::clkPll2, 2);
		m_pCcm->Set(CCcm51::clkPer, CCcm51::clkPll2, 2, 6);
		m_pCcm->Set(CCcm51::clkGpu2, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkArmAxi, CCcm51::clkAxia);
		m_pCcm->Set(CCcm51::clkIpuHsp, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkGpu, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkVpu, CCcm51::clkAxib);
		m_pCcm->Set(CCcm51::clkUsboh3, CCcm51::clkPll2, 5, 2);
		m_pCcm->Set(CCcm51::clkEcspi, CCcm51::clkPll2, 5, 2);
		m_pCcm->Set(CCcm51::clkEnfc, CCcm51::clkEmi, 5);
	}

	if( m_pCcm->GetFreq(CCcm51::clkPll3) != 216000000 ) {

		m_pCcm->Set(CCcm51::clkUart, CCcm51::clkPll2, 4, 3);

		m_pDpll[2]->Set(CDpll51::refOsc, 9, 0, 1, 2, false);

		for( UINT i = 0; i < 4000; i++ ) {

			ASM("nop\r\n");
		}

		m_pCcm->Set(CCcm51::clkUart, CCcm51::clkPll3, 4, 1);
	}

	m_pCcm->SetFpm(CCcm51::fpmOff);

	m_pCcm->EnableAmps(false);

	m_pCcm->SetGating(false);
}

void CPlatformGraphite::InitRails(void)
{
	m_pPmui->SetSw1(1100);

	m_pPmui->SetVGen1(3150);

	m_pPmui->SetVGen2(3150);

	m_pPmui->SetVGen3(1800);

	m_pPmui->SetVVid(2775);
}

void CPlatformGraphite::InitMux(void)
{
	CPlatformMX51::InitMux();

	DWORD const muxUart[] = {

		CIoMux51::MUX_UART1_TXD,	0,
		CIoMux51::MUX_UART1_RXD,	0,
		CIoMux51::MUX_UART1_RTS,	0,
		CIoMux51::MUX_UART1_CTS,	0,
		CIoMux51::MUX_UART2_TXD,	0,
		CIoMux51::MUX_UART2_RXD,	0,
		CIoMux51::MUX_EIM_D26,		4,
		CIoMux51::MUX_EIM_D25,		4,
		CIoMux51::MUX_UART3_TXD,	1,
		CIoMux51::MUX_UART3_RXD,	1,
		CIoMux51::MUX_EIM_D27,		3,
		CIoMux51::MUX_EIM_D24,		3,

		CIoMux51::SEL_UART1_RX,		0,
		CIoMux51::SEL_UART1_RTS,	0,
		CIoMux51::SEL_UART2_RX,		2,
		CIoMux51::SEL_UART2_RTS,	3,
		CIoMux51::SEL_UART3_RX,		4,
		CIoMux51::SEL_UART3_RTS,	3,

		CIoMux51::PAD_UART1_TXD,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART1_RXD,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART1_CTS,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART1_RTS,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART2_TXD,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART2_RXD,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D25,		CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D26,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART3_TXD,	CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_UART3_RXD,	CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D24,		CIoMux51::PAD_DRIVE_HI | CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_D27,		CIoMux51::PAD_FAST,
	};

	DWORD const muxPic[] = {

		CIoMux51::MUX_AUD3_BB_RXD,	3,
		CIoMux51::MUX_AUD3_BB_CK,	3,
		CIoMux51::MUX_AUD3_BB_FS,	3,
	};

	DWORD const muxImage[] = {

		CIoMux51::MUX_DISP1_DAT6,	0,
		CIoMux51::MUX_DISP1_DAT7,	0,
		CIoMux51::MUX_DISP1_DAT8,	0,
		CIoMux51::MUX_DISP1_DAT9,	0,
		CIoMux51::MUX_DISP1_DAT10,	0,
		CIoMux51::MUX_DISP1_DAT11,	0,
		CIoMux51::MUX_DISP1_DAT12,	0,
		CIoMux51::MUX_DISP1_DAT13,	0,
		CIoMux51::MUX_DISP1_DAT14,	0,
		CIoMux51::MUX_DISP1_DAT15,	0,
		CIoMux51::MUX_DISP1_DAT16,	0,
		CIoMux51::MUX_DISP1_DAT17,	0,
		CIoMux51::MUX_DISP1_DAT18,	0,
		CIoMux51::MUX_DISP1_DAT19,	0,
		CIoMux51::MUX_DISP1_DAT20,	0,
		CIoMux51::MUX_DISP1_DAT21,	0,
		CIoMux51::MUX_DISP1_DAT22,	0,
		CIoMux51::MUX_DISP1_DAT23,	0,
		CIoMux51::MUX_DI1_D0_CS,	4,
		CIoMux51::MUX_DI1_D1_CS,	4,
		CIoMux51::MUX_DISPB2_SER_DIN,	4,
		CIoMux51::MUX_DISPB2_SER_DIO,	4,
		CIoMux51::MUX_DISPB2_SER_CLK,	4,
		CIoMux51::MUX_DISPB2_SER_RS,	4,
		CIoMux51::MUX_DI1_PIN3,		0,
		CIoMux51::MUX_DI1_PIN2,		0,

		CIoMux51::PAD_DISP1_DAT6,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT7,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT8,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT9,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT10,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT11,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT12,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT13,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT14,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT15,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT16,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT17,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT18,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT19,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT20,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT21,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT22,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISP1_DAT23,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DI1_D0_CS,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DI1_D1_CS,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISPB2_SER_DIN,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISPB2_SER_DIO,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISPB2_SER_CLK,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DISPB2_SER_RS,	CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DI1_PIN3,		CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
		CIoMux51::PAD_DI1_PIN2,		CIoMux51::PAD_DRIVE_LO | CIoMux51::PAD_KEEPER,
	};

	DWORD const muxSdHost[] = {

		CIoMux51::MUX_SD1_CMD,		16,
		CIoMux51::MUX_SD1_CLK,		0,
		CIoMux51::MUX_SD1_DATA0,	0,
		CIoMux51::MUX_SD1_DATA1,	0,
		CIoMux51::MUX_SD1_DATA2,	0,
		CIoMux51::MUX_SD1_DATA3,	0,
		CIoMux51::MUX_GPIO1_0,		1,
		CIoMux51::MUX_GPIO1_1,		1,

		CIoMux51::PAD_SD1_CMD,		CIoMux51::PAD_FAST    | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_CLK,		CIoMux51::PAD_FAST    | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA0,	CIoMux51::PAD_FAST    | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA1,	CIoMux51::PAD_FAST    | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA2,	CIoMux51::PAD_FAST    | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_SD1_DATA3,	CIoMux51::PAD_FAST    | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_GPIO1_0,		CIoMux51::PAD_HYST_ON | CIoMux51::PAD_PULL_UP_100K,
		CIoMux51::PAD_GPIO1_1,		CIoMux51::PAD_HYST_ON | CIoMux51::PAD_PULL_UP_100K,
	};

	DWORD const muxNic[] = {

		CIoMux51::MUX_EIM_EB2,		3,
		CIoMux51::MUX_EIM_EB3,		3,
		CIoMux51::MUX_EIM_CS2,		3,
		CIoMux51::MUX_EIM_CS3,		3,
		CIoMux51::MUX_EIM_CS4,		3,
		CIoMux51::MUX_EIM_CS5,		3,
		CIoMux51::MUX_NANDF_RB2,	1,
		CIoMux51::MUX_NANDF_RB3,	1,
		CIoMux51::MUX_NANDF_CS2,	2,
		CIoMux51::MUX_NANDF_CS3,	2,
		CIoMux51::MUX_NANDF_CS4,	2,
		CIoMux51::MUX_NANDF_CS5,	2,
		CIoMux51::MUX_NANDF_CS6,	2,
		CIoMux51::MUX_NANDF_CS7,	1,
		CIoMux51::MUX_NANDF_RDY_INT,	1,
		CIoMux51::MUX_NANDF_D8,		2,
		CIoMux51::MUX_NANDF_D9,		2,
		CIoMux51::MUX_NANDF_D11,	2,
		CIoMux51::MUX_EIM_A21,		1,
		CIoMux51::MUX_I2C1_CLK,		3,

		CIoMux51::PAD_EIM_EB2,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_EIM_EB3,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS2,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS3,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS4,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_EIM_CS5,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_NANDF_RB2,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_RB3,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_CS2,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS3,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS4,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS5,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS6,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_CS7,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_RDY_INT,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_D8,		CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V | CIoMux51::PAD_DRIVE_MED,
		CIoMux51::PAD_NANDF_D9,		CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_NANDF_D11,	CIoMux51::PAD_FAST | CIoMux51::PAD_LO_V,
		CIoMux51::PAD_EIM_A21,		CIoMux51::PAD_FAST,
		CIoMux51::PAD_I2C1_CLK,		CIoMux51::PAD_OPEN_DRAIN,
	};

	DWORD const muxDisp[] = {

		CIoMux51::MUX_CSI2_D12,		3,
		CIoMux51::MUX_GPIO1_5,		0,
		CIoMux51::MUX_GPIO1_2,		1,
	};

	DWORD const muxPort[] = {

		CIoMux51::MUX_DI1_PIN12,	4,
		CIoMux51::MUX_AUD3_BB_CK,	3,

		CIoMux51::PAD_DI1_PIN12,	CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_AUD3_BB_CK,	CIoMux51::PAD_DRIVE_HI,
	};

	DWORD const muxSwitch[] = {

		CIoMux51::PAD_GPIO1_6,		CIoMux51::PAD_PULL_DN_100K,
	};

	DWORD const muxUsb[] = {

		CIoMux51::MUX_USBH1_CLK,	0,
		CIoMux51::MUX_USBH1_DIR,	0,
		CIoMux51::MUX_USBH1_STP,	0,
		CIoMux51::MUX_USBH1_NXT,	0,
		CIoMux51::MUX_USBH1_DATA0,	0,
		CIoMux51::MUX_USBH1_DATA1,	0,
		CIoMux51::MUX_USBH1_DATA2,	0,
		CIoMux51::MUX_USBH1_DATA3,	0,
		CIoMux51::MUX_USBH1_DATA4,	0,
		CIoMux51::MUX_USBH1_DATA5,	0,
		CIoMux51::MUX_USBH1_DATA6,	0,
		CIoMux51::MUX_USBH1_DATA7,	0,
		CIoMux51::MUX_GPIO1_7,		0,
		CIoMux51::MUX_NANDF_D12,	3,

		CIoMux51::MUX_EIM_A24,		2,
		CIoMux51::MUX_EIM_A25,		2,
		CIoMux51::MUX_EIM_A26,		2,
		CIoMux51::MUX_EIM_A27,		2,
		CIoMux51::MUX_EIM_D16,		2,
		CIoMux51::MUX_EIM_D17,		2,
		CIoMux51::MUX_EIM_D18,		2,
		CIoMux51::MUX_EIM_D19,		2,
		CIoMux51::MUX_EIM_D20,		2,
		CIoMux51::MUX_EIM_D21,		2,
		CIoMux51::MUX_EIM_D22,		2,
		CIoMux51::MUX_EIM_D23,		2,
		CIoMux51::MUX_DI1_PIN13,	4,
		CIoMux51::MUX_EIM_A23,		1,

		CIoMux51::PAD_USBH1_CLK,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DIR,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_STP,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_NXT,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA0,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA1,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA2,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA3,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA4,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA5,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA6,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_USBH1_DATA7,	CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_GPIO1_7,		CIoMux51::PAD_SLOW | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_NANDF_D12,	CIoMux51::PAD_SLOW | CIoMux51::PAD_LO_V,

		CIoMux51::PAD_EIM_A24,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A25,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A26,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A27,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D16,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D17,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D18,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D19,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D20,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D21,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D22,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_D23,		CIoMux51::PAD_FAST | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_DI1_PIN13,	CIoMux51::PAD_SLOW | CIoMux51::PAD_DRIVE_HI,
		CIoMux51::PAD_EIM_A23,		CIoMux51::PAD_SLOW | CIoMux51::PAD_DRIVE_HI,
	};

	m_pMux->Set(muxUart, elements(muxUart));

	m_pMux->Set(muxPic, elements(muxPic));

	m_pMux->Set(muxImage, elements(muxImage));

	m_pMux->Set(muxSdHost, elements(muxSdHost));

	m_pMux->Set(muxNic, elements(muxNic));

	m_pMux->Set(muxDisp, elements(muxDisp));

	m_pMux->Set(muxPort, elements(muxPort));

	m_pMux->Set(muxSwitch, elements(muxSwitch));

	m_pMux->Set(muxUsb, elements(muxUsb));
}

void CPlatformGraphite::InitGpio(void)
{
	CPlatformMX51::InitGpio();

	m_pGpio[0]->SetDirection(0, false);

	m_pGpio[0]->SetDirection(1, false);

	m_pGpio[0]->SetDirection(6, false);

	m_pGpio[0]->SetDirection(7, true);

	m_pGpio[1]->SetDirection(15, false);

	m_pGpio[1]->SetDirection(17, true);

	m_pGpio[2]->SetDirection(1, true);

	m_pGpio[2]->SetDirection(2, true);

	m_pGpio[2]->SetDirection(28, true);

	m_pGpio[3]->SetDirection(16, true);

	m_pGpio[3]->SetDirection(20, true);
}

void CPlatformGraphite::InitUarts(void)
{
	for( UINT i = 0; i < 3; i++ ) {

		IPortObject *pPort = NULL;

		AfxGetObject("uart", i, IPortObject, pPort);

		if( pPort ) {

			pPort->Bind(this);

			pPort->Release();
		}
	}
}

void CPlatformGraphite::InitUsb(void)
{
	m_pGpio[0]->SetState(7, true);

	m_pGpio[1]->SetState(17, false);

	m_pGpio[2]->SetState(2, true);

	m_pGpio[2]->SetState(28, false);

	Sleep(5);

	for( UINT i = 0; i < 2; i++ ) {

		IUsbDriver *p0, *p1, *p2;

		piob->GetObject("usbctrl-h", i, AfxAeonIID(IUsbDriver), (void **) &p0);

		piob->GetObject("usbctrl-e", i, AfxAeonIID(IUsbDriver), (void **) &p1);

		piob->GetObject("usbctrl-c", i, AfxAeonIID(IUsbDriver), (void **) &p2);

		if( p0 && p1 && p2 ) {

			p0->Bind(p1);

			p1->Bind(p2);
		}

		AfxRelease(p0);

		AfxRelease(p1);

		AfxRelease(p2);
	}

	m_pGpio[0]->SetState(7, false);

	m_pGpio[2]->SetState(2, false);

	m_pGpio[2]->SetState(28, true);

	m_pGpio[1]->SetState(17, true);

	Sleep(50);

	ISerialMemory *pHub;

	piob->GetObject("usbhub", 0, AfxAeonIID(ISerialMemory), (void **) &pHub);

	BYTE bCfg  = 0x9B;

	BYTE bSwap = 0x10;

	BYTE bCmd  = 0x01;

	pHub->PutData(0x06, &bCfg, 1);

	pHub->PutData(0xFA, &bSwap, 1);

	pHub->PutData(0xFF, &bCmd, 1);

	pHub->Release();
}

void CPlatformGraphite::InitRack(void)
{
	IUsbHostStack    *pStack;

	IExpansionSystem *pExpSys;

	IUsbSystem       *pExpUsb;

	AfxGetObject("usb.host", 0, IUsbHostStack, pStack);

	AfxGetObject("usb.rack", 0, IExpansionSystem, pExpSys);

	AfxGetObject("usb.rack", 0, IUsbSystem, pExpUsb);

	if( pStack ) {

		pStack->Init();

		pStack->Attach((IUsbSystem *) this);

		pStack->Attach(pExpUsb);

		pExpSys->Attach((IUsbSystemPower *) this);
	}

	AfxRelease(pExpSys);

	AfxRelease(pStack);

	AfxRelease(pExpUsb);
}

BOOL CPlatformGraphite::ReadRfid(void)
{
	ISerialMemory *p = NULL;

	AfxGetObject("rfid", 0, ISerialMemory, p);

	if( p ) {

		BYTE bData;

		if( p->GetData(0x00, &bData, sizeof(bData)) ) {

			p->Release();

			return TRUE;
		}

		p->Release();
	}

	return FALSE;
}

void CPlatformGraphite::ProgramPic(void)
{
	UINT uPinM = 117;

	UINT uPinC = 116;

	UINT uPinD = 115;

	CPic12F Pic(m_pGpio, uPinD, uPinC, uPinM);

	Pic.Unlock(true);

	if( Pic.ReadVer() != PIC_VER ) {

		Pic.Erase();

		Pic.Load(PIC_CODE);

		Pic.LoadVer(PIC_VER);
	}

	Pic.Unlock(false);
}

// Persistence

void CPlatformGraphite::LoadLedLevel(void)
{
	ISerialMemory *pMem = NULL;

	AfxGetObject("fram", 0, ISerialMemory, pMem);

	if( pMem ) {

		WORD ds[2];

		pMem->GetData(addrSignalLed, PBYTE(&ds), sizeof(ds));

		if( ds[0] == MAKEWORD('L', 'S') ) {

			SetLedLevel(int(ds[1]), false);

			return;
		}

		m_uLedLevel = 100;

		pMem->Release();
	}
}

void CPlatformGraphite::SaveLedLevel(void)
{
	ISerialMemory *pMem = NULL;

	AfxGetObject("fram", 0, ISerialMemory, pMem);

	if( pMem ) {

		WORD ds[2];

		ds[0] = MAKEWORD('L', 'S');

		ds[1] = WORD(m_uLedLevel);

		pMem->PutData(addrSignalLed, PBYTE(&ds), sizeof(ds));

		pMem->Release();
	}
}

// End of File
