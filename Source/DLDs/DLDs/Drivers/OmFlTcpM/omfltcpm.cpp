
#include "intern.hpp"

#include "omfltcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Omni Flow TCP/IP Driver
//

// Instantiator

INSTANTIATE(COmniFlowTCPMaster);

// Constructor

COmniFlowTCPMaster::COmniFlowTCPMaster(void)
{
	m_Ident     = DRIVER_ID;
	}

// Destructor

COmniFlowTCPMaster::~COmniFlowTCPMaster(void)
{
	}

// Configuration

void MCALL COmniFlowTCPMaster::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL COmniFlowTCPMaster::Attach(IPortObject *pPort)
{
	}

void MCALL COmniFlowTCPMaster::Open(void)
{
	}

// Device

CCODE MCALL COmniFlowTCPMaster::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_wPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_PingReg	= GetWord(pData);
			m_pCtx->m_Disable5	= GetByte(pData);
			m_pCtx->m_Disable6	= GetByte(pData);

			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			m_pCtx->m_fDirty	= FALSE;

			m_pCtx->m_Exception	= 0;
			m_pCtx->m_CustomPt	= 0;
			m_pCtx->m_CustomQty	= 0;

			memset(m_pCtx->m_Custom, 0, elements(m_pCtx->m_Custom));

			m_pCtx->m_ReadPt	= 0;
			
			memset(m_pCtx->m_ReadBuff, 0x0, elements(m_pCtx->m_ReadBuff) * sizeof(DWORD));
			
			m_pCtx->m_WritePt	= 0;

			m_pCtx->m_WriteQty	= 0;

			memset(m_pCtx->m_WriteBuff, 0x0, elements(m_pCtx->m_WriteBuff) * sizeof(DWORD));
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL COmniFlowTCPMaster::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL COmniFlowTCPMaster::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP = dwValue;

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 2 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_wPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 3 )  { 
		
		// Set Drop Number

		UINT uValue = ATOI(Value);
		
		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bUnit  = BYTE(uValue);

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 4 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP);
		}

	if( uFunc == 5 ) {

		// Get Current Port Number

		return pCtx->m_wPort;
		}

	if( uFunc == 6 )  {

		// Get Current Drop Number

		return pCtx->m_bUnit;
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL COmniFlowTCPMaster::Ping(void)
{
	if( m_pCtx->m_fPing ) {
		
		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) == NOTHING ) {
			
			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {
						
		return COmniFlowBaseMaster::Ping();
		}

	return CCODE_ERROR;
	}

CCODE MCALL COmniFlowTCPMaster::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return COmniFlowBaseMaster::Read(Addr, pData, uCount);
	}

CCODE MCALL COmniFlowTCPMaster::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) {

		return CCODE_ERROR;
		}

	return COmniFlowBaseMaster::Write(Addr, pData, uCount);
	}

// Socket Management

BOOL COmniFlowTCPMaster::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;

	}

BOOL COmniFlowTCPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_wPort;

		m_pCtx->m_fDirty   = FALSE;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void COmniFlowTCPMaster::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void COmniFlowTCPMaster::StartFrame(BYTE bOpcode)
{
	COmniFlowBaseMaster::StartFrame(bOpcode);

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);

	m_bHead = m_uPtr;
	}

// Transport

BOOL COmniFlowTCPMaster::SendFrame(void)
{
	COmniFlowBaseMaster::SendFrame();

	m_bTxBuff[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL COmniFlowTCPMaster::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

//	AfxTrace("\nRx : ");

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

/*			for( UINT u = uPtr; u < uSize + uPtr; u++ ) {
				
				AfxTrace("%2.2x ", (m_bRxBuff + u)[0]);
				}
*/		
			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRxBuff[5];

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[0] == m_bTxBuff[0] ) {

						if( m_bRxBuff[1] == m_bTxBuff[1] ) {

							memcpy(m_bRxBuff, m_bRxBuff + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRxBuff[n] = m_bRxBuff[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}
		
		Sleep(10);
		}

	return FALSE;
	}

BOOL COmniFlowTCPMaster::Transact(void)
{
	if( COmniFlowBaseMaster::Transact() ) {

		return TRUE;
		}

	if( !m_fException ) {

		CloseSocket(TRUE);
		}

	return FALSE;
	}







// End of File
