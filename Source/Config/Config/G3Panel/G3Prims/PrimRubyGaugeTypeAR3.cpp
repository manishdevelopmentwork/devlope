
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A3 Radial Gauge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR3, CPrimRubyGaugeTypeAR);

// Constructor

CPrimRubyGaugeTypeAR3::CPrimRubyGaugeTypeAR3(void)
{
	m_lineFact = 0.5;

	m_Reflect   = 1;
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR3::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeAR::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR3::AddMetaData(void)
{
	CPrimRubyGaugeTypeAR::AddMetaData();

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE_4));
	}

// Path Management

void CPrimRubyGaugeTypeAR3::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeAR::MakePaths();

	m_bezelBase = 66;

	Quad(m_pathFace,  66, 67);

	Quad(m_pathOuter, 66, 80, 90);
	Quad(m_pathInner, 66, 70, 80);
	Quad(m_pathRing1, 66, 89, 91);
	Quad(m_pathRing2, 66, 79, 80);
	Quad(m_pathRing3, 66, 66, 70);
	Quad(m_pathRing4, 66, 74, 69);
	}

// Implementation

void CPrimRubyGaugeTypeAR3::LoadLayout(void)
{
	m_radiusPivot     =   7 * m_ScalePivot / 100.0;

	m_pointPivot.m_x  = 103 + m_radiusPivot;
	m_pointPivot.m_y  =  97 - m_radiusPivot;
	
	m_angleMin        = +90 - 90;
	m_angleMax        =   0 - 90;
	
	m_radiusOuter.m_x = 45;
	m_radiusOuter.m_y = 45;

	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBug.m_x   = m_PointMode ? 50 : 50;
	m_radiusMajor.m_x = m_PointMode ? 37 : 32;
	m_radiusMinor.m_x = m_PointMode ? 39 : 37;
	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBand.m_x  = m_PointMode ? 40 : 40;
	m_radiusPoint.m_x = m_PointMode ? 36 : 37;
	m_radiusSweep.m_x = m_PointMode ? 25 : 37;

	CalcRadii();
	}

// End of File
