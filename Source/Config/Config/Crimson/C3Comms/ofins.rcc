
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "ofins.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "ofins.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "ofins.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "ofins.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for COmronFINSMasterDeviceOptions
//

COmronFINSMasterDeviceOptionsUIList RCDATA
BEGIN
	"Mode,Mode,,CUIDropDown,CS/CJ|CV,"
	"Select the type of the Omron device."
	"\0"
      	
	"Dna,Destination Network,,CUIEditInteger,|0||0|127,"
	"Indicate the number of the network where the target node is located. "
	"The default value is suitable for local network applications."
	"\0"
    
	"Da1,Destination Node,,CUIEditInteger,|0||0|255,"
	"Indicate the node address of the target device. "
	"\0"

	"Da2,Destination Unit,,CUIEditInteger,|0||0|254,"
	"Indicate the unit address of the target device. "
	"\0"

	"Sna,Source Network,,CUIEditInteger,|0||0|127,"
	"Indicate the number of the network where the source node is located. "
	"The default value is suitable for a local network."
	"\0"

	"Sa1,Source Node,,CUIEditInteger,|0||0|255,"
	"Indicate the local node number."
	"\0"

	"Sa2,Source Unit,,CUIEditInteger,|0||0|254,"
	"Indicate the unit ID of the source node. "
	"\0"

	"\0"
END



//////////////////////////////////////////////////////////////////////////
//
// UI for COmronFINSMasterUDPDeviceOptions
//

COmronFINSMasterUDPDeviceOptionsUIList RCDATA
BEGIN
	"Sa1,Source Node,,CUIEditInteger,|0||0|255,"
	"Indicate the local node number.  For most applications, the Source Node should "
	"be set equivalent to the least significant byte of the G3's IP Address."
	"\0"

	"Addr,IP Address,,CUIIPAddress,,"
	"Indicate the primary IP address of the Omron device."
	"\0"

	"Addr2,Fallback IP Address,,CUIIPAddress,,"
	"Indicate the secondary IP address of the Omron device. This will be used "
	"if the primary address does not respond, and can be used to implement "
	"redundant communications. Leave the field at 0.0.0.0 to disable fallback."
	"\0"

	"Dna2,Fallback Destination Network,,CUIEditInteger,|0||0|127,"
	"Indicate the number of the secondary network where the target node is located. "
	"\0"
    
	"Da12,Fallback Destination Node,,CUIEditInteger,|0||0|255,"
	"Indicate the secondary node address of the target device. "
	"\0"

	"Da22,Fallback Destination Unit,,CUIEditInteger,|0||0|254,"
	"Indicate the secondary unit address of the target device. "
	"\0"

	"Port,UDP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the UDP port number on which the FINS protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"
		
	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the FINS driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the FINS server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"UDP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a FINS request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"Poll,Polling Rate,,CUIEditInteger,|0|ms|0|60000,"
	"Specify the rate for which the driver should request data from the Omron device.  "
	"\0"

	"NonFatal,Ignore Non-Fatal CPU Errors,,CUIDropDown,No|Yes,"
	"Specify whether non-fatal errors in the CPU should result in communication errors. "
	"\0"

	"Fatal,Ignore Fatal CPU Errors,,CUIDropDown,No|Yes,"
	"Specify whether fatal errors in the CPU should result in communication errors. "
	"\0"

	"\0"
END

COmronFINsG9spMasterDriverOptionsUIList RCDATA
BEGIN
	"Poll,Polling Rate,,CUIEditInteger,|0|ms|0|60000,"
	"Specify the rate for which the driver should request data from the Omron device.  "
	"\0"

	"\0"
END

COmronFINSMasterDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Mode\0"
	"G:2,root,FINS Addresses,Dna,Sna,Da1,Sa1,Da2,Sa2\0"
	"\0"
END

#if defined(C3_VERSION) //KEEP

COmronFINSMasterUDPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:2,root,Device Identification,Addr,Mode\0"
	"G:2,root,FINS Addresses,Dna,Sna,Da1,Sa1,Da2,Sa2\0"
	"G:1,root,Fallback Addresses,Addr2,Dna2,Da12,Da22\0"
	"G:1,root,Protocol Options,Fatal,NonFatal,Port,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

COmronFINsG9spMasterUDPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:2,root,Device Identification,Addr\0"
	"G:2,root,FINS Addresses,Da1,Sa1\0"
	"G:1,root,Fallback Addresses,Addr2,Da12\0"
	"G:1,root,Protocol Options,Port,Poll,Time2\0"
	"\0"
END

COmronFINsG9spMasterDriverOptionsUIPage RCDATA
BEGIN
	"G:1,root,Communication Settings,Poll\0"

	"\0"
END



#else //KEEP

COmronFINSMasterUDPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:2,root,Device Identification,Addr,Mode\0"
	"G:2,root,FINS Addresses,Dna,Sna,Da1,Sa1,Da2,Sa2\0"
	"G:1,root,Protocol Options,Port,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

#endif //KEEP

// End of File
