
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DF1EIP_HPP
	
#define	INCLUDE_DF1EIP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Base Driver
//

#include "abbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master DF1 over EIP Device Options
//

class CDF1MasterEIPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDF1MasterEIPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Public Data
		UINT	m_Device;
		DWORD	m_Address;
		UINT	m_Slot;
		UINT	m_Time;
		UINT	m_Abort;

	protected:

		// Data Members
		CString m_Root;
		UINT	m_Name;
		UINT	m_Label;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Master DF1 over EIP Driver
//

class CDF1MasterEIPDriver : public CABDriver
{
	public:
		// Constructor
		CDF1MasterEIPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	
	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
