
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Console_HPP

#define INCLUDE_Console_HPP

//////////////////////////////////////////////////////////////////////////
//
// Console APIs
//

extern BOOL InitConsole(void);
extern BOOL ExecConsole(HTHREAD hThread);
extern void TermConsole(void);
extern void LogToConsole(CString Text);

// End of File

#endif
