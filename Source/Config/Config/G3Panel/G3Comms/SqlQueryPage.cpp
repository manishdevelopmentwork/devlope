
#include "Intern.hpp"

#include "SqlQueryPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlQuery.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sql Query Page
//

// Runtime Class

AfxImplementRuntimeClass(CSqlQueryPage, CUIStdPage);

// Constructor

CSqlQueryPage::CSqlQueryPage(CSqlQuery *pQuery)
{
	m_Class   = AfxRuntimeClass(CSqlQuery);

	m_pQuery  = pQuery;
	}

// Operations

BOOL CSqlQueryPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadContents(pView, pItem);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

void CSqlQueryPage::LoadContents(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(L"Filters and Sorting", 1);

	pView->AddUI(pItem, L"root", L"SortMode");

	LoadColumns(pView, pItem);

	pView->AddUI(pItem, L"root", L"FilterPush");

	pView->AddUI(pItem, L"root", L"ShowSQL");
	
	pView->EndGroup(TRUE);
	}

void CSqlQueryPage::LoadColumns(IUICreate *pView, CItem *pItem)
{
	CString Form;

	UINT uCount = m_pQuery->GetColCount();

	if( uCount > 0 ) {

		Form = L"";

		for( UINT n = 0; n < uCount; n++ ) {

			CSqlColumn *pColumn = m_pQuery->GetColumn(n);

			if( pColumn ) {

				Form +=  m_pQuery->GetColumn(n)->m_SqlName;

				if( n < (uCount - 1) ) {

					Form += L"|";
					}
				}
			}
		}
	else {
		Form = L"No Columns";
		}

	CUIData Data;

	Data.m_Tag	 = L"SortColumn";

	Data.m_Label	 = L"Sort by Column";

	Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

	Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Data.m_Format	 = Form;

	Data.m_Tip	 = L"Choose the column to sort the results by.";

	pView->AddUI(pItem, L"root", &Data);
	}

// End of File
