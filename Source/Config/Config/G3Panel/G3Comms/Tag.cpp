
#include "Intern.hpp"

#include "Tag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "CommsManager.hpp"
#include "CommsSystem.hpp"
#include "DispColor.hpp"
#include "DispFormat.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Tag Root
//

// Dynamic Class

AfxImplementDynamicClass(CTag, CCodedHost);

// Constructor

CTag::CTag(void)
{
	m_pValue  = NULL;

	m_pSim    = NULL;

	m_pLabel  = NULL;

	m_pAlias  = NULL;

	m_pFormat = NULL;

	m_pColor  = NULL;

	m_Circle  = FALSE;

	m_Private = FALSE;
	}

// Default Prop Values

DWORD CTag::GetDefProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return DWORD(wstrdup(L"As-Text"));

		case tpDescription:
			return DWORD(wstrdup(L"Desc"));

		case tpTextOff:
			return DWORD(wstrdup(L"OFF"));

		case tpTextOn:
			return DWORD(wstrdup(L"ON"));

		case tpForeColor:
			return GetRGB(31,31,31);

		case tpBackColor:
			return GetRGB(0,0,0);

		case tpIndex:
			return 0;
		}

	if( ID == tpMaximum ) {

		if( Type == typeInteger ) {

			return 100;
			}

		if( Type == typeReal ) {

			return R2I(100);
			}
		}

	return GetNull(Type);
	}

// Item Location

CCommsSystem * CTag::FindSystem(void) const
{
	return (CCommsSystem *) GetDatabase()->GetSystemItem();
	}

CTagManager * CTag::FindManager(void) const
{
	return (CTagManager *) GetParent(2);
	}

// Attributes

BOOL CTag::IsBroken(void) const
{
	if( m_Circle ) {

		return TRUE;
		}

	if( m_pValue ) {

		return m_pValue->IsBroken();
		}

	return FALSE;
	}

BOOL CTag::IsCircular(void) const
{
	return m_Circle;
	}

CString CTag::GetExportClass(void) const
{
	return CPrintf( L"%s.%u.%u",
			GetClassName() + 4,
			m_pFormat ? m_pFormat->GetFormType() : 0,
			m_pColor  ? m_pColor ->GetColType () : 0
			);
	}

CString CTag::FormatData(DWORD Data)
{
	UINT Type = GetDataType();

	if( m_pFormat ) {

		return m_pFormat->Format(Data, Type, fmtBare);
		}

	return m_pFormat->GeneralFormat(Data, Type, fmtBare);
	}

CString CTag::FormatData(void)
{
	return FormatData(Execute());
	}

// Operations

void CTag::MakeLite(void)
{
	}

// Limit Access

DWORD CTag::GetMinValue(UINT Type)
{
	return GetProp(7, Type);
	}

DWORD CTag::GetMaxValue(UINT Type)
{
	return GetProp(8, Type);
	}

// Type Access

BOOL CTag::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Label" || Tag == "Alias" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "SubIndex" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Attributes

UINT CTag::GetTreeImage(void) const
{
	AfxAssert(FALSE);

	return 0;
	}

UINT CTag::GetDataType(void) const
{
	AfxAssert(FALSE);

	return 0;
	}

UINT CTag::GetTypeFlags(void) const
{
	AfxAssert(FALSE);

	return 0;
	}

// Reference Check

BOOL CTag::RefersToTag(CCodedTree &Busy, CTagList *pTags, UINT uTag)
{
	if( CodeRefersToTag(Busy, pTags, m_pValue, uTag) ) {

		return TRUE;
		}

	return FALSE;
	}

// Circular Check

void CTag::UpdateCircular(void)
{
	m_Circle = CheckCircular(m_pValue);
	}

// Item Naming

CString CTag::GetHumanName(void) const
{
	return m_Name;
	}

// Persistance

void CTag::Save(CTreeFile &File)
{
	m_Desc.Remove(L'\r');

	m_Desc.Remove(L'\n');

	CCodedHost::Save(File);
	}

// Evaluation

DWORD CTag::Execute(void)
{
	if( m_pSim ) {

		return m_pSim->Execute(GetDataType());
		}

	if( m_pValue ) {

		if( !m_pValue->IsCommsRef() ) {

			if( !m_Circle ) {

				return m_pValue->Execute(GetDataType());
				}
			}
		}

	return GetNull(GetDataType());
	}

DWORD CTag::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpDescription:
			return DWORD(wstrdup(m_Desc));

		case tpForeColor:
			return GetRGB(31,31,31);

		case tpBackColor:
			return GetRGB(0,0,0);

		case tpName:
			return DWORD(wstrdup(m_Name));

		case tpIndex:
			return m_uPos;

		case tpAlarms:
			return 0;
		}

	switch( ID ) {

		case tpAsText:
		case tpLabel:
		case tpDescription:
		case tpPrefix:
		case tpUnits:
			return GetNull(Type);

		case tpSetPoint:
		case tpMinimum:
		case tpMaximum:
			return GetNull(Type);

		case tpForeColor:
		case tpBackColor:
			return 0;

		case tpName:
		case tpIndex:
		case tpAlarms:
			return GetNull(Type);
		}

	return GetNull(Type);
	}

// Searching

void CTag::FindAlarms(CStringArray &List)
{
	}

void CTag::FindTriggers(CStringArray &List)
{
	}

// Download Support

BOOL CTag::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddText(m_Name);

	Init.AddText(m_Desc);

	if( m_pValue ) {

		if( IsBroken() ) {

			Init.AddByte(0);

			Init.AddItem(itemVirtual, m_pSim);
			}
		else {
			if( m_pValue->IsCommsRef() ) {

				if( FindSystem()->m_pComms->m_NoComms ) {

					Init.AddByte(0);

					Init.AddItem(itemVirtual, m_pSim);
					}
				else {
					DWORD Ref = (DWORD &) m_pValue->GetRef(0);

					Init.AddByte(2);

					Init.AddLong(Ref);

					Init.AddByte(BYTE(m_pValue->GetCommsBits()));

					Init.AddByte(0);
					}
				}
			else {
				Init.AddItem(itemVirtual, m_pValue);

				Init.AddByte(0);
				}
			}
		}
	else {
		if( FindSystem()->m_pComms->m_NoComms ) {

			Init.AddByte(0);

			Init.AddItem(itemVirtual, m_pSim);
			}
		else {
			Init.AddByte(0);

			Init.AddByte(0);
			}
		}

	Init.AddItem(itemVirtual, m_pLabel);
	Init.AddItem(itemVirtual, m_pAlias);
	Init.AddItem(itemVirtual, m_pFormat);
	Init.AddItem(itemVirtual, m_pColor);

	return TRUE;
	}

// Meta Data Creation

void CTag::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddString (Desc);
	Meta_AddString (Class);
	Meta_AddInteger(Circle);
	Meta_AddVirtual(Value);
	Meta_AddVirtual(Sim);
	Meta_AddVirtual(Label);
	Meta_AddVirtual(Alias);
	Meta_AddVirtual(Format);
	Meta_AddVirtual(Color);
	Meta_AddInteger(Private);

	Meta_SetName((IDS_TAG));
	}

// Implementation

UINT CTag::GetImage(UINT uBase) const
{
	UINT Type  = GetDataType();

	UINT Flags = GetTypeFlags();

	UINT uPos  = 0;

	if( Flags & flagArray ) {

		uPos = 12;
		}
	else {
		if( m_pValue ) {

			if( m_pValue->IsCommsRef() ) {

				if( Flags & flagWritable ) {

					uPos = 0;
					}
				else
					uPos = 4;
				}
			else {
				if( Flags & flagWritable ) {

					uPos = 0;
					}
				else
					uPos = 8;
				}
			}
		else
			uPos = 16;
		}

	switch( Type ) {

		case typeReal:

			uBase = IDI_RED_FLOAT;

			break;

		case typeString:

			uBase = IDI_RED_STRING;

			break;
		}

	return uBase + uPos;
	}

void CTag::Recompile(IUIHost *pHost)
{
	FindSystem()->TagCheck(GetIndex(), FALSE);

	pHost->SendUpdate(updateRename);

	pHost->SendUpdate(updateProps);
	}

BOOL CTag::CodeRefersToTag(CCodedTree &Busy, CTagList *pTags, CCodedItem *pCoded, UINT uTag)
{
	if( pCoded ) {

		if( Busy.Failed(Busy.Find(pCoded)) ) {

			Busy.Insert(pCoded);

			UINT uCount = pCoded->GetRefCount();

			for( UINT r = 0; r < uCount; r++ ) {

				CDataRef const &Ref = pCoded->GetRef(r);

				if( Ref.t.m_IsTag ) {

					UINT   uRef = Ref.t.m_Index;

					CTag * pRef = pTags->GetItem(uRef);

					if( uRef == uTag ) {

						Busy.Remove(pCoded);

						return TRUE;
						}

					if( pRef ) {

						if( pRef->RefersToTag(Busy, pTags, uTag) ) {

							Busy.Remove(pCoded);

							return TRUE;
							}
						}
					}
				}

			Busy.Remove(pCoded);
			}
		}

	return FALSE;
	}

BOOL CTag::CheckCircular(IUIHost *pHost, CCodedItem *pCoded)
{
	if( CheckCircular(pCoded) ) {

		m_Circle = TRUE;

		pHost->SendUpdate(updateProps);

		return TRUE;
		}

	CheckCircular(pHost);

	return FALSE;
	}

BOOL CTag::CheckCircular(CCodedItem *pCoded)
{
	if( pCoded && pCoded->CheckCircular(this, FALSE) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CTag::CheckCircular(IUIHost *pHost)
{
	UINT Circle = m_Circle;

	UpdateCircular();

	if( Circle == m_Circle ) {

		return FALSE;
		}

	pHost->SendUpdate(updateProps);

	return TRUE;
	}

BOOL CTag::SetFormatClass(CDispFormat * &pFormat, CLASS Class)
{
	CDispFormat * pOld = pFormat;

	if( Class ) {

		pFormat = AfxNewObject(CDispFormat, Class);

		pFormat->SetParent(this);

		pFormat->Init();

		if( pOld ) {

			pFormat->Preserve(pOld);
			}
		}
	else
		pFormat = NULL;

	if( pOld ) {

		pOld->Kill();

		delete pOld;
		}

	return TRUE;
	}

BOOL CTag::SetColorClass(CDispColor * &pColor, CLASS Class)
{
	CDispColor * pOld = pColor;

	if( Class ) {

		pColor = AfxNewObject(CDispColor, Class);

		pColor->SetParent(this);

		pColor->Init();

		if( pOld ) {

			pColor->Preserve(pOld);
			}
		}
	else
		pColor = NULL;

	if( pOld ) {

		pOld->Kill();

		delete pOld;
		}

	return TRUE;
	}

// End of File
