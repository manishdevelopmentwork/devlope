
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2004 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Socket Object Class
//

// Runtime Class

AfxImplementRuntimeClass(CSocket, CObject);

// Constructors

CSocket::CSocket(void)
{
	m_s = INVALID_SOCKET;
	}

CSocket::CSocket(SOCKET s)
{
	m_s = INVALID_SOCKET;

	Attach(s);
	}

CSocket::CSocket(CSocket const &That)
{
	AfxAssert(FALSE);
	}

// Destructor

CSocket::~CSocket(void)
{
	Detach();
	}

// Attachment

void CSocket::Attach(SOCKET s)
{
	Detach();

	m_s = s;
	}

void CSocket::Detach(void)
{
	if( IsValid() ) {

		closesocket(m_s);

		m_s = INVALID_SOCKET;
		}
	}

SOCKET CSocket::TakeOver(void)
{
	SOCKET s = m_s;

	m_s = INVALID_SOCKET;

	return s;
	}

// Assignment Operators

CSocket const & CSocket::operator = (CSocket const &That)
{
	AfxAssert(FALSE);

	return ThisObject;
	}

CSocket const & CSocket::operator = (SOCKET s)
{
	Attach(s);

	return ThisObject;
	}

// Conversion

CSocket::operator SOCKET (void) const
{
	return m_s;
	}

// Attributes

SOCKET CSocket::GetHandle(void) const
{
	return m_s;
	}

BOOL CSocket::IsValid(void) const
{
	return m_s != INVALID_SOCKET;
	}

BOOL CSocket::IsNull(void) const
{
	return m_s == INVALID_SOCKET;
	}

CIPPort CSocket::GetSocketName(void) const
{
	sockaddr addr;

	int size = sizeof(addr);

	memset(&addr, 0, size);

	getsockname(m_s, &addr, &size);

	return CIPPort(addr);
	}

CIPPort CSocket::GetRemoteName(void) const
{
	sockaddr addr;

	int size = sizeof(addr);

	memset(&addr, 0, size);

	getpeername(m_s, &addr, &size);

	return CIPPort(addr);
	}

int CSocket::GetLastError(void) const
{
	return WSAGetLastError();
	}

ULONG CSocket::GetRxCount(void) const
{
	ULONG uCount = 0;

	return ioctlsocket(m_s, FIONREAD, &uCount) ? 0 : uCount;
	}

// Validation

void CSocket::AssertValid(void) const
{
	CObject::AssertValid();
	
	AfxAssert(IsValid());
	}

// Creation

void CSocket::CreateTCP(void)
{
	Attach(socket(AF_INET, SOCK_STREAM, IPPROTO_IP));
	}

void CSocket::CreateUDP(void)
{
	Attach(socket(AF_INET, SOCK_DGRAM, IPPROTO_IP));
	}

void CSocket::CreateICMP(void)
{
	Attach(WSASocket(AF_INET, SOCK_RAW, IPPROTO_ICMP, NULL, 0, 0));
	}

// Operations

BOOL CSocket::Bind(CIPPort const &Local)
{
	sockaddr addr;

	Local.LoadSockAddr(addr);

	return bind(m_s, &addr, sizeof(addr)) ? FALSE : TRUE;
	}

BOOL CSocket::Listen(int Backlog)
{
	return listen(m_s, Backlog) ? FALSE : TRUE;
	}

BOOL CSocket::Accept(CSocket &Session)
{
	SOCKET s = accept(m_s, NULL, NULL);

	if( !(s == INVALID_SOCKET) ) {

		Session.Attach(s);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSocket::Connect(CIPPort const &Remote)
{
	sockaddr addr;

	Remote.LoadSockAddr(addr);

	return connect(m_s, &addr, sizeof(addr)) ? FALSE : TRUE;
	}

BOOL CSocket::CanRead(void)
{
	fd_set fd;

	FD_ZERO(&fd);

	FD_SET(m_s, &fd);

	timeval tv = { 0, 0 };

	if( select(0, &fd, NULL, NULL, &tv) == 1 ) {

		if( FD_ISSET(m_s, &fd) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CSocket::CanWrite(void)
{
	fd_set fd;

	FD_ZERO(&fd);

	FD_SET(m_s, &fd);

	timeval tv = { 0, 0 };
	
	if( select(0, NULL, &fd, NULL, &tv) == 1 ) {

		if( FD_ISSET(m_s, &fd) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CSocket::Abort(void)
{
	SetLinger(0);

	Detach();
	}

BOOL CSocket::Shutdown(BOOL fBoth)
{
	return shutdown(m_s, fBoth ? SD_BOTH : SD_SEND) ? FALSE : TRUE;
	}

BOOL CSocket::Unlisten(void)
{
	Detach();

	return TRUE;
	}

BOOL CSocket::SetNonBlocking(BOOL fEnable)
{
	ULONG uEnable = fEnable;
	
	return ioctlsocket(m_s, FIONBIO, &uEnable) ? FALSE : TRUE;
	}

INT CSocket::Recv(void *pData, UINT uLimit)
{
	return recv(m_s, PTXT(pData), uLimit, 0);
	}

INT CSocket::Send(void const *pData, UINT uCount)
{
	return send(m_s, PCTXT(pData), uCount, 0);
	}

INT CSocket::SendTo(void const *pData, UINT uCount, CIPPort const &Dest)
{
	sockaddr addr;

	Dest.LoadSockAddr(addr);
	
	return sendto(m_s, PCTXT(pData), uCount, 0, &addr, sizeof(addr));
	}

INT CSocket::RecvFrom(void *pData, UINT uLimit, CIPPort &Port)
{
	sockaddr addr;
	
	INT nSize = sizeof(addr);
	
	INT rd = recvfrom(m_s, PTXT(pData), uLimit, 0, &addr, &nSize);

	Port = addr;

	return rd;
	}

BOOL CSocket::SetOption(INT nOpt, PVOID pData, UINT  uLen)
{
	return !setsockopt(m_s, SOL_SOCKET, nOpt, (char *) pData, uLen);
	}

BOOL CSocket::GetOption(INT nOpt, PVOID pData, UINT &uLen) const
{
	return !getsockopt(m_s, SOL_SOCKET, nOpt, (char *) pData, (int *) &uLen);
	}

void CSocket::SetLinger(WORD wInterval)
{
	linger l;
	
	l.l_onoff  = 1;
	
	l.l_linger = wInterval;

	SetOption(SO_LINGER, &l, sizeof(linger));
	}

// Events

BOOL CSocket::EventClear(void)
{
	return WSAEventSelect(m_s, NULL, 0) ? FALSE : TRUE;
	}

BOOL CSocket::EventSelect(DWORD Mask, CEvent &Event)
{
	WSAEVENT hEvent = WSAEVENT(Event.GetHandle());

	return WSAEventSelect(m_s, hEvent, Mask) ? FALSE : TRUE;
	}

BOOL CSocket::EventEnum(CNetEvent &NetEvent)
{
	return WSAEnumNetworkEvents(m_s, NULL, &NetEvent) ? FALSE : TRUE;
	}

BOOL CSocket::EventEnum(CNetEvent &NetEvent, CEvent &Event)
{
	WSAEVENT hEvent = WSAEVENT(Event.GetHandle());

	return WSAEnumNetworkEvents(m_s, hEvent, &NetEvent) ? FALSE : TRUE;
	}

// End of File
