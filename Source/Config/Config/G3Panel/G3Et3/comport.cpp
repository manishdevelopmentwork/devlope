
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Port List
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsPortList, CItemIndexList);

// Constructor

CEt3CommsPortList::CEt3CommsPortList(void)
{
	m_Class  = NULL;

	m_uLimit = 100;
	}

CEt3CommsPortList::CEt3CommsPortList(UINT uLimit)
{
	m_Class  = NULL;

	m_uLimit = uLimit;
	}

// Item Access

CEt3CommsPort * CEt3CommsPortList::GetItem(INDEX Index) const
{
	return (CEt3CommsPort *) CItemIndexList::GetItem(Index);
	}

CEt3CommsPort * CEt3CommsPortList::GetItem(UINT uPort) const
{
	return (CEt3CommsPort *) CItemIndexList::GetItem(uPort);
	}

// Device Access

CEt3CommsPort * CEt3CommsPortList::FindPort(UINT uPort) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEt3CommsPort *pPort = GetItem(n);

		if( pPort->m_Number == uPort ) {

			return pPort;
			}

		GetNext(n);
		}

	return NULL;
	}

CEt3CommsDevice * CEt3CommsPortList::FindDevice(UINT uDevice) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEt3CommsDevice *pDevice;

		if( pDevice = GetItem(n)->FindDevice(uDevice) ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

class CEt3CommsPortPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEt3CommsPortPage(CEt3CommsPort *pPort);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEt3CommsPort * m_pPort;

		// Implementation
		void LoadPortConfig(IUICreate *pView);
		void LoadDriverSettings(IUICreate *pView);
		void LoadDriverSelect(IUICreate *pView);
		BOOL LoadDriverConfig(IUICreate *pView);
		BOOL LoadButtons(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsPortPage, CUIStdPage);

// Constructor

CEt3CommsPortPage::CEt3CommsPortPage(CEt3CommsPort *pPort)
{
	m_Class = AfxPointerClass(pPort);

	m_pPort = pPort;
	}

// Operations

BOOL CEt3CommsPortPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	LoadPortConfig(pView);

	LoadDriverConfig(pView);

	LoadButtons(pView);	

	pView->NoRecycle();

	return TRUE;
	}

void CEt3CommsPortPage::LoadPortConfig(IUICreate *pView)
{
	if( m_pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		LoadDriverSelect(pView);

		LoadDriverSettings(pView);

		CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pPort));

		pPage->LoadIntoView(pView, m_pPort);

		delete pPage;
		}
	}

void CEt3CommsPortPage::LoadDriverSelect(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_DRIVER_SELECTION), 1);

	CString Form;

	Form += "";	Form += CPrintf("%u|Disabled",		 CEt3CommsDriver::protDisabled);
	Form += "|";	Form += CPrintf("%u|Universal Master",	 CEt3CommsDriver::protSixnetMaster);
	Form += "|";	Form += CPrintf("%u|Modbus Master",	 CEt3CommsDriver::protModbusMaster);
	Form += "|";	Form += CPrintf("%u|Universal Slave",	 CEt3CommsDriver::protSixnetSlave);
	Form += "|";	Form += CPrintf("%u|Modbus RTU Slave",	 CEt3CommsDriver::protRtuSlave);
	Form += "|";	Form += CPrintf("%u|Modbus ASCII Slave", CEt3CommsDriver::protAscSlave);

	//
	CString Help = CString(IDS_SELECT);

	//
	CUIData Data;

	Data.m_Tag	 = L"Protocol";

	Data.m_Label	 = L"Protocol";

	Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

	Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Data.m_Format	 = Form;

	Data.m_Tip	 = Help;

	pView->AddUI(m_pPort, L"root", &Data);

	pView->EndGroup(FALSE);
	}

void CEt3CommsPortPage::LoadDriverSettings(IUICreate *pView)
{
	if( m_pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		CEt3CommsPortSerial *pPort = (CEt3CommsPortSerial *) m_pPort;

		pView->StartGroup(CString(IDS_DRIVER_SETTINGS), 1);

		if( pPort->m_pDriver->IsMaster() ) {

			pView->AddUI(m_pPort, L"Driver", L"Flavor");
			}
	
		pView->AddUI(m_pPort, L"root", L"LeadTime");
	
		pView->AddUI(m_pPort, L"root", L"LagTime");
	
		pView->AddUI(m_pPort, L"root", L"GapTime");

		pView->EndGroup(FALSE);
		}
	}

BOOL CEt3CommsPortPage::LoadDriverConfig(IUICreate *pView)
{
	if( m_pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		CEt3CommsPortSerial *pPort = (CEt3CommsPortSerial *) m_pPort;

		if( pPort->m_pDriver->IsMaster() ) {

			CUIPageList List;

			pPort->m_pDriver->OnLoadPages(&List);

			if( List.GetCount() ) {

				List[0]->LoadIntoView(pView, pPort->m_pDriver);

				return TRUE;
				}
		
			}
		}

	return FALSE;
	}

BOOL CEt3CommsPortPage::LoadButtons(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_PORT_COMMANDS), 1);

	pView->AddButton( CString(IDS_ADD_ADDITIONAL),
			  CString(IDS_FORMAT),
			  L"ButtonAddDevice"
			  );

	pView->EndGroup(FALSE);
	
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsPort, CEt3UIItem);

// Constructor

CEt3CommsPort::CEt3CommsPort(void)
{
	m_Name	   = CString(IDS_COMMS_NAME);

	m_Number   = 0;

	m_pDevices = New CEt3CommsDeviceList;

	m_uImage   = IDI_PORT;
	}

// Attributes

UINT CEt3CommsPort::GetTreeImage(void) const
{
	return m_uImage;
	}

CString CEt3CommsPort::GetTreeLabel(void) const
{
	return m_Name;
	}

// UI Creation

BOOL CEt3CommsPort::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CEt3CommsPortPage(this));

	return FALSE;
	}

// UI Update

void CEt3CommsPort::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "ButtonAddDevice" ) {

		afxMainWnd->PostMessage(WM_COMMAND, IDM_COMMS_ADD_DEVICE);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Item Naming

CString CEt3CommsPort::GetItemOrdinal(void) const
{
	return CPrintf(IDS_COMMS_PORT_ID, m_Number);
	}

// Item Lookup

CEt3CommsDevice * CEt3CommsPort::FindDevice(UINT uDevice) const
{
	return m_pDevices->FindDevice(uDevice);
	}

// Persistance

void CEt3CommsPort::Init(void)
{
	CEt3UIItem::Init();

	AllocNumber();
	}

// Meta Data Creation

void CEt3CommsPort::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Number);
	Meta_AddCollect(Devices);

	Meta_SetName((IDS_COMMS_PORT));
	}

// Implementation

BOOL CEt3CommsPort::AllocNumber(void)
{
	if( !m_Number ) {

		CEt3CommsManager *pManager = (CEt3CommsManager *) GetParent(AfxRuntimeClass(CEt3CommsManager));

		m_Number = pManager->AllocPortNumber();

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsPortSerial, CEt3CommsPort);

// Constructor

CEt3CommsPortSerial::CEt3CommsPortSerial(void)
{
	m_Protocol	= CEt3CommsDriver::protDisabled;

	m_BaudRate	= 6;

	m_DataBits	= 0;

	m_Parity	= 0;

	m_StopBits	= 0;

	m_LeadTime	= 8;

	m_LagTime	= 2;

	m_GapTime	= 4;

	m_pDriver	= New CEt3CommsDriver;
	}

// Attributes

CString CEt3CommsPortSerial::GetTreeLabel(void) const
{
	CString Label = m_Name;

	CString Short;

	if( !(Short = m_pDriver->GetShortName()).IsEmpty() ) {

		Label += L" - ";

		Label += Short;
		}

	return Label;
	}

// UI Update

void CEt3CommsPortSerial::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "DataBits" || Tag == "Parity" ) {

		if( m_DataBits == 0 ) {

			if( m_Parity != 0 ) {
			
				m_StopBits = 0;

				pHost->UpdateUI(L"StopBits");
				}
			}

		if( m_DataBits == 1 ) {

			if( m_Parity == 0 ) {

				m_StopBits = 1;

				pHost->UpdateUI(L"StopBits");
				}
			}

		DoEnables(pHost);
		}

	if( Tag == "Protocol" ) {

		UINT uLastIdent = m_pDriver->GetIdent();

		AfxTouch(uLastIdent);

		if( UpdateDriver() ) {

			CheckDevices(pHost);

			DoEnables(pHost);

			pHost->SendUpdate(updateRename);			

			pHost->RemakeUI();
			}

		return;
		}

	CEt3CommsPort::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

void CEt3CommsPortSerial::PrepareData(void)
{
	if( m_Protocol != CEt3CommsDriver::protDisabled ) {

		UINT uPort = 1;

		CFileDataBaseCfgComms          &File = m_pSystem->m_CfgComms;

		CFileDataBaseCfgComms::CSerial &Port = File.m_Serial[uPort];

		Port.SetBaud(m_BaudRate);
		Port.SetMode(BuildMode());
		Port.SetHand(BuildHand(Port));
		Port.SetLead(m_LeadTime);
		Port.SetLag (m_LagTime);
		Port.SetGap (m_GapTime);
		}

	CEt3CommsPort::PrepareData();
	}

// Persistance

void CEt3CommsPortSerial::Init(void)
{
	CEt3CommsPort::Init();
	}

// Meta Data Creation

void CEt3CommsPortSerial::AddMetaData(void)
{
	CEt3CommsPort::AddMetaData();

	Meta_AddInteger(Protocol);
	Meta_AddInteger(BaudRate);
	Meta_AddInteger(DataBits);
	Meta_AddInteger(Parity);
	Meta_AddInteger(StopBits);
	Meta_AddInteger(LeadTime);
	Meta_AddInteger(LagTime);
	Meta_AddInteger(GapTime);
	Meta_AddObject (Driver);

	Meta_SetName((IDS_SERIAL_PORT));
	}

// Implementation

void CEt3CommsPortSerial::DoEnables(IUIHost *pHost)
{
	BOOL fEnable     = m_Protocol != CEt3CommsDriver::protDisabled;

	BOOL fEnableStop = (m_DataBits == 0 && m_Parity == 0) ||
			   (m_DataBits == 1 && m_Parity != 0) ;;

	BOOL fMaster     = m_pDriver->IsMaster();

	pHost->EnableUI("BaudRate",	   fEnable);
	pHost->EnableUI("DataBits",	   fEnable);
	pHost->EnableUI("Parity",	   fEnable);
	pHost->EnableUI("StopBits",	   fEnable);
	pHost->EnableUI("LeadTime",	   fEnable);
	pHost->EnableUI("LagTime",	   fEnable);
	pHost->EnableUI("GapTime",	   fEnable);
	pHost->EnableUI("StopBits",	   fEnable && fEnableStop);
	pHost->EnableUI("ButtonAddDevice", fEnable && fMaster);
	}

BOOL CEt3CommsPortSerial::UpdateDriver(void)
{
	if( m_Protocol != CEt3CommsDriver::protDisabled ) {
		
		if( m_pDriver->GetIdent() == m_Protocol ) {
			
			return FALSE;
			}		
		}

	m_pDriver->m_Ident = m_Protocol;

	return TRUE;
	}

void CEt3CommsPortSerial::CheckDevices(IUIHost *pHost)
{
	if( !pHost->InReplay() ) {

		HGLOBAL hPrev = m_pDevices->TakeSnapshot();

		if( m_Protocol != CEt3CommsDriver::protDisabled ) {

			if( m_pDriver->IsSlave() ) {

				m_pDevices->DeleteAllItems(TRUE);
				}

			if( m_pDevices->GetItemCount() == 0 ) {

				CEt3CommsDevice *pDev = New CEt3CommsDevice;

				m_pDevices->AppendItem(pDev);
				}

			//m_pDevices->ClearTransferBlocks();
			}
		else {
			m_pDevices->DeleteAllItems(TRUE);
			}

		HGLOBAL hData = m_pDevices->TakeSnapshot();

		CCmd *  pCmd  = New CCmdSubItem( L"Devices",
						 hPrev,
						 hData
						 );

		pHost->SaveExtraCmd(pCmd);

		pHost->SendUpdate(updateChildren);

		return;
		}

	pHost->SendUpdate(updateChildren);
	}

// Config File Help

UINT CEt3CommsPortSerial::BuildMode(void)
{
	switch( m_DataBits ) {
		
		case 0:
			switch( m_Parity ) {
				
				case 0:
					switch( m_StopBits ) {

						case 0:	return 0;

						case 1:	return 1;
						}
					break;
				
				case 1:
					return 2;
				
				case 2:
					return 3;
				
				case 3:
					return 4;
				
				case 4:
					return 5;
				}
			break;
		
		case 1:
			switch( m_Parity ) {
				
				case 0:
					return 6;
				
				case 1:
					switch( m_StopBits ) {

						case 0:	return 7;

						case 1:	return 11;
						}

					break;
				
				case 2:
					switch( m_StopBits ) {

						case 0:	return 8;

						case 1:	return 12;
						}

					break;
				
				case 3:
					switch( m_StopBits ) {

						case 0:	return 9;

						case 1:	return 13;
						}

					break;
				
				case 4:
					switch( m_StopBits ) {

						case 0:	return 10;

						case 1:	return 14;
						}

					break;
				}
		}

	AfxAssert(FALSE);

	return 15;
	}

UINT CEt3CommsPortSerial::BuildHand(CFileDataBaseCfgComms::CSerial &Port)
{
	UINT uData = Port.GetHand();

	return uData;
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Network
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsPortNetwork, CEt3CommsPort);

// Constructor

CEt3CommsPortNetwork::CEt3CommsPortNetwork(void)
{
	m_uImage = IDI_PORT_ETHERNET;
	}

// Persistance

void CEt3CommsPortNetwork::Init(void)
{
	CEt3CommsPort::Init();
	}

// Meta Data Creation

void CEt3CommsPortNetwork::AddMetaData(void)
{
	CEt3CommsPort::AddMetaData();

	Meta_AddInteger(Port);

	Meta_SetName(IDS_NETWORK_PORT);
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Network Modbus
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsPortNetworkModbus, CEt3CommsPortNetwork);

// Constructor

CEt3CommsPortNetworkModbus::CEt3CommsPortNetworkModbus(void)
{
	m_Name = CString(IDS_MODBUS_UDP);

	m_Port = 502;
	}

// Meta Data Creation

void CEt3CommsPortNetworkModbus::AddMetaData(void)
{
	CEt3CommsPortNetwork::AddMetaData();

	Meta_SetName((IDS_MODBUS));
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port - Network Sixnet
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsPortNetworkSixnet, CEt3CommsPortNetwork);

// Constructor

CEt3CommsPortNetworkSixnet::CEt3CommsPortNetworkSixnet(void)
{
	m_Name = CString(IDS_SIXNET_UDR);

	m_Port = 1594;
	}

// Meta Data Creation

void CEt3CommsPortNetworkSixnet::AddMetaData(void)
{
	CEt3CommsPortNetwork::AddMetaData();

	Meta_SetName((IDS_SIXNET_UDR));
	}

// End of File
