
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "GraphiteHandler.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphite Rack Handler
//

// Instantiator

IRackHandler * Create_GraphiteHandler(UINT uCount)
{
	return New CGraphiteHandler(uCount);
}

// Constructor

CGraphiteHandler::CGraphiteHandler(UINT uCount)
{
	StdSetRef();

	m_pRack  = NULL;

	m_pMutex = Create_Mutex();

	AfxGetObject("usb.rack", 0, IExpansionSystem, m_pRack);

	if( m_pRack ) {

		m_pList  = New CModuleComms[uCount];

		m_uCount = uCount;

		memset(m_pList, 0x00, m_uCount * sizeof(CModuleComms));

		m_pRack->Attach(this);
	}
	else {
		m_pList  = NULL;

		m_uCount = 0;
	}
}

// Destructor

CGraphiteHandler::~CGraphiteHandler(void)
{
	if( m_pRack ) {

		m_pRack->Attach((IExpansionEvents *) NULL);

		Shutdown();

		m_pRack->Release();

		delete[] m_pList;
	}

	m_pMutex->Release();
}

HRESULT CGraphiteHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IRackHandler);

	StdQueryInterface(IRackHandler);

	StdQueryInterface(IExpansionEvents);

	return E_NOINTERFACE;
}

ULONG CGraphiteHandler::AddRef(void)
{
	StdAddRef();
}

ULONG CGraphiteHandler::Release(void)
{
	StdRelease();
}

// IRackHandler

UINT CGraphiteHandler::Transact(BYTE bDrop, PBYTE pData, UINT uCount)
{
	if( m_pRack ) {

		if( CheckModule(bDrop) ) {

			CModuleComms &Drop = m_pList[bDrop];

			if( Drop.m_uState == stateReady ) {

				GuardTask(true);

				if( Drop.m_uFlags & flagSend ) {

					Drop.m_pDriver->KillAsyncSend();

					Drop.m_uFlags &= ~flagSend;
				}

				if( Drop.m_uFlags & flagRecycle ) {

					Drop.m_uFlags  &= ~flagRecycle;

					Drop.m_uState   = stateRecycle;

					Drop.m_uTimeout = GetTickCount() + ToTicks(2500);
				}

				if( !Drop.m_pDriver->SendBulk(pData, uCount, true) ) {

					GuardTask(false);

					SetError(bDrop);

					return msgError;
				}

				Drop.m_uFlags |= flagSend;

				if( !(Drop.m_uFlags & flagRecv) ) {

					UINT r = Drop.m_pDriver->RecvBulk(Drop.m_bBuff, sizeof(Drop.m_bBuff), true);

					if( r == NOTHING ) {

						GuardTask(false);

						SetError(bDrop);

						return msgError;
					}

					Drop.m_uFlags |= flagRecv;
				}

				GuardTask(false);

				return msgAck;
			}

			return msgNone;
		}
	}

	return msgError;
}

UINT CGraphiteHandler::Transact(BYTE bDrop, UINT Msg)
{
	if( m_pRack ) {

		if( CheckModule(bDrop) ) {

			if( Msg == msgEnq ) {

				CModuleComms &Drop = m_pList[bDrop];

				if( Drop.m_uFlags & flagSend ) {

					Drop.m_uCount = Drop.m_pDriver->WaitAsyncSend(0);

					if( Drop.m_uCount == NOTHING ) {

						return msgBusy;
					}

					Drop.m_uFlags &= ~flagSend;

					if( Drop.m_uCount == 0 ) {

						SetError(bDrop);

						return msgError;
					}
				}

				if( Drop.m_uFlags & flagRecv ) {

					Drop.m_uCount = Drop.m_pDriver->WaitAsyncRecv(0);

					if( Drop.m_uCount == NOTHING ) {

						return msgBusy;
					}

					Drop.m_uFlags &= ~flagRecv;

					if( Drop.m_uCount == 0 ) {

						SetError(bDrop);

						return msgError;
					}

					return msgFrame;
				}

				switch( Drop.m_uState ) {

					case stateDetached:
					case stateAttached:
					case stateRecycle:

						return msgBusy;

					case stateReady:

						return msgNak;
				}
			}

			return msgNone;
		}
	}

	return msgError;
}

BOOL CGraphiteHandler::GetReply(BYTE bDrop, PBYTE pData)
{
	if( bDrop < m_uCount ) {

		CModuleComms &Drop = m_pList[bDrop];

		memcpy(pData, Drop.m_bBuff, Drop.m_uCount);

		return TRUE;
	}

	return FALSE;
}

BOOL CGraphiteHandler::Recycle(BYTE bDrop)
{
	if( bDrop < m_uCount ) {

		m_pList[bDrop].m_uFlags |= flagRecycle;

		return TRUE;
	}

	return FALSE;
}

// IExpansionEvents

void CGraphiteHandler::OnDeviceArrival(UINT uSlot, UINT uDriver)
{
	m_pMutex->Wait(FOREVER);

	INDEX iFind = m_Lookup.FindName(uSlot);

	if( iFind != m_Lookup.Failed() ) {

		UINT iDrop = m_Lookup.GetData(iFind);

		AbortComms(iDrop);

		FreeRemoveLock(iDrop);

		if( CheckDriver(iDrop) ) {

			CModuleComms &Drop = m_pList[iDrop];

			Drop.m_pDriver = (IUsbHostBulkDriver *) m_pRack->GetFuncDriver(uSlot, 0);

			Drop.m_uState  = stateAttached;

			Drop.m_pDriver->GetDevice(Drop.m_pDevice);

			SetRemoveLock(iDrop);
		}
	}

	m_pMutex->Free();
}

void CGraphiteHandler::OnDeviceRemoval(UINT uSlot, UINT uDriver)
{
	m_pMutex->Wait(FOREVER);

	INDEX iFind = m_Lookup.FindName(uSlot);

	if( iFind != m_Lookup.Failed() ) {

		UINT i = m_Lookup.GetData(iFind);

		m_pList[i].m_uState = stateDetached;
	}

	m_pMutex->Free();
}

// Implementation

BOOL CGraphiteHandler::CheckModule(BYTE bDrop)
{
	if( bDrop < m_uCount ) {

		m_pMutex->Wait(FOREVER);

		CModuleComms &Drop = m_pList[bDrop];

		if( Drop.m_uState == stateDiscover ) {

			if( FindSlot(bDrop) ) {

				if( CheckDriver(bDrop) ) {

					Drop.m_uState = stateAttached;
				}
				else {
					Drop.m_uState = stateNone;
				}
			}
		}

		if( Drop.m_uState == stateAttached ) {

			FindDriver(bDrop);

			if( SetRemoveLock(bDrop) ) {

				Drop.m_uState  = stateReady;

				Drop.m_uFlags &= ~(flagSend | flagRecv | flagRecycle);
			}
			else {
				Drop.m_uState = stateNone;
			}
		}

		if( Drop.m_uState == stateRecycle ) {

			if( GetTickCount() > Drop.m_uTimeout ) {

				Drop.m_uState = stateReady;
			}
		}

		if( Drop.m_uState == stateDetached ) {

			AbortComms(bDrop);

			FreeRemoveLock(bDrop);
		}

		m_pMutex->Free();

		return TRUE;
	}

	return FALSE;
}

BOOL CGraphiteHandler::FindSlot(BYTE bDrop)
{
	if( bDrop < m_uCount ) {

		AfxGetAutoObject(pModule, "pnp.pnp-io", bDrop, IUsbModule);

		if( pModule ) {

			DWORD Path = pModule->GetSlot();

			UINT  Slot = m_pRack->FindSlot((UsbTreePath &) Path);

			if( Slot != NOTHING ) {

				CModuleComms &Drop = m_pList[bDrop];

				Drop.m_uSlot = Slot;

				m_Lookup.Insert(Slot, UINT(bDrop));

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CGraphiteHandler::CheckDriver(BYTE bDrop)
{
	if( bDrop < m_uCount ) {

		UINT uSlot = m_pList[bDrop].m_uSlot;

		if( m_pRack->WaitSlot(uSlot, 0) ) {

			if( m_pRack->GetInterface(uSlot, 0)->GetClass() == rackClassRack ) {

				if( m_pRack->GetFuncDriver(uSlot, 0) ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CGraphiteHandler::FindDriver(BYTE bDrop)
{
	if( bDrop < m_uCount ) {

		CModuleComms &Drop = m_pList[bDrop];

		Drop.m_pDriver = (IUsbHostBulkDriver *) m_pRack->GetFuncDriver(Drop.m_uSlot, 0);

		if( Drop.m_pDriver ) {

			Drop.m_pDriver->GetDevice(Drop.m_pDevice);

			return TRUE;
		}
	}

	return FALSE;
}

void CGraphiteHandler::AbortComms(UINT uDrop)
{
	if( uDrop < m_uCount ) {

		CModuleComms &Drop = m_pList[uDrop];

		if( Drop.m_uFlags & flagRecv ) {

			GuardTask(TRUE);

			Drop.m_pDriver->KillAsyncRecv();

			Drop.m_uFlags &= ~flagRecv;

			GuardTask(FALSE);
		}

		if( Drop.m_uFlags & flagSend ) {

			GuardTask(TRUE);

			Drop.m_pDriver->KillAsyncSend();

			Drop.m_uFlags &= ~flagSend;

			GuardTask(FALSE);
		}
	}
}

BOOL CGraphiteHandler::SetRemoveLock(UINT uDrop)
{
	if( uDrop < m_uCount ) {

		CModuleComms &Drop = m_pList[uDrop];

		if( Drop.m_uFlags & flagLocked ) {

			return TRUE;
		}

		GuardTask(TRUE);

		if( Drop.m_pDriver->SetRemoveLock(true) ) {

			Drop.m_uFlags |= flagLocked;

			GuardTask(FALSE);

			return TRUE;
		}

		GuardTask(FALSE);
	}

	return FALSE;
}

void CGraphiteHandler::SetError(BYTE bDrop)
{
	if( bDrop < m_uCount ) {

		CModuleComms &Drop = m_pList[bDrop];

		if( Drop.m_pDevice ) {

			/*AfxTrace("CRackHandler Requesting Device Reset Drop %d\n", bDrop);*/

			Drop.m_pDevice->SetError();
		}
	}
}

BOOL CGraphiteHandler::FreeRemoveLock(UINT uDrop)
{
	if( uDrop < m_uCount ) {

		CModuleComms &Drop = m_pList[uDrop];

		if( Drop.m_uFlags & flagLocked ) {

			GuardTask(TRUE);

			Drop.m_pDriver->SetRemoveLock(false);

			Drop.m_uFlags &= ~flagLocked;

			GuardTask(FALSE);
		}
	}

	return TRUE;
}

void CGraphiteHandler::Shutdown(void)
{
	for(;;) {

		UINT uBusy = 0;

		for( UINT i = 0; i < m_uCount; i++ ) {

			CModuleComms &Drop = m_pList[i];

			m_pMutex->Wait(FOREVER);

			if( Drop.m_uFlags & flagRecv ) {

				uBusy++;

				if( Drop.m_pDriver->WaitAsyncRecv(0) != NOTHING ) {

					uBusy--;

					Drop.m_uFlags &= ~flagRecv;
				}
				else {
					m_pMutex->Free();

					continue;
				}
			}

			if( Drop.m_uFlags & flagSend ) {

				Drop.m_pDriver->KillAsyncSend();

				Drop.m_uFlags &= ~flagSend;
			}

			if( Drop.m_uFlags & flagLocked ) {

				Drop.m_pDriver->SetRemoveLock(false);

				Drop.m_uFlags &= ~flagLocked;
			}

			m_pMutex->Free();
		}

		if( !uBusy ) {

			break;
		}
	}
}

// End of File
