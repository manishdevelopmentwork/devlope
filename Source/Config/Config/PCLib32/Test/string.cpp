
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

// Null Data

CStringData CString::m_Empty = { 0, 0, 0, 0, { 0 } };

// Data Access

#define	GetData(x) (((CStringData *) ((x)->m_pText)) - 1)

// Constructors

CString::CString(void)
{
	m_pText = m_Empty.m_cData;
	}

/*CString::CString(CString const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		m_pText = That.m_pText;
		
		GetData(this)->m_nRefs++;

		return;
		}

	m_pText = m_Empty.m_cData;
	}*/

CString::CString(PCTXT pText, UINT uCount)
{
	AfxValidateStringPtr(pText, uCount);
	
	if( *pText && uCount ) {
		
		Alloc(uCount);
		
		wstrncpy(m_pText, pText, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(TCHAR cData, UINT uCount)
{
	AfxAssert(cData);
	
	if( uCount ) {
		
		Alloc(uCount);
		
		wmemset(m_pText, cData, uCount);
		
		m_pText[uCount] = 0;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(GUID const &Guid)
{
	m_pText = m_Empty.m_cData;

	InitFrom(Guid);
	}

/*CString::CString(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		Alloc(wstrlen(pText));
		
		wstrcpy(m_pText, pText);

		return;
		}

	m_pText = m_Empty.m_cData;
	}

CString::CString(ENTITY ID)
{
	PTXT pWork = IntLoadString(ID);
	
	Alloc(wstrlen(pWork));
	
	wstrcpy(m_pText, pWork);
	
	delete pWork;
	}*/

// Legacy Constructors

CString::CString(PCSTR pText)
{
	if( *pText ) {

		Alloc(strlen(pText));

		for( int n = 0; m_pText[n] = BYTE(pText[n]); n++ );

		return;
		}

	m_pText = m_Empty.m_cData;
	}

// Destructor

CString::~CString(void)
{
	Release();
	}

// Assignment Operators

CString const & CString::operator = (CString const &That)
{
	if( this != &That ) {
		
		That.AssertValid();
		
		Release();
		
		if( !That.IsEmpty() ) {
			
			m_pText = That.m_pText;
			
			GetData(this)->m_nRefs++;
			}
		}
	
	return ThisObject;
	}

CString const & CString::operator = (GUID const &Guid)
{
	Release();

	InitFrom(Guid);

	return ThisObject;
	}

CString const & CString::operator = (PCTXT pText)
{
	AfxValidateStringPtr(pText);

	if( *pText ) {
		
		if( pText >= m_pText && pText <= m_pText + GetLength() ) {
			
			CString Work = pText;
			
			Release();
			
			Alloc(Work.GetLength());
			
			wstrcpy(m_pText, Work.m_pText);
			}
		else {
			Release();
			
			Alloc(wstrlen(pText));
			
			wstrcpy(m_pText, pText);
			}
		}
	else
		Release();
	
	return ThisObject;
	}

CString const & CString::operator = (ENTITY ID)
{
	Release();

	PTXT pWork = IntLoadString(ID);
	
	Alloc(wstrlen(pWork));
	
	wstrcpy(m_pText, pWork);
	
	delete pWork;
	
	return ThisObject;
	}

// Legacy Assignment

CString const & CString::operator = (PCSTR pText)
{
	AfxValidateStringPtr(pText);

	if( *pText ) {
		
		Release();
		
		Alloc(strlen(pText));
		
		for( int n = 0; m_pText[n] = BYTE(pText[n]); n++ );
		}
	else
		Release();
	
	return ThisObject;
	}

// Quick Init

void CString::QuickInit(PCTXT pText, UINT uSize)
{
	CopyOnWrite();

	CStringData *pData = GetData(this);

	UINT        uAlloc = FindAlloc(uSize);
	
	if( pData->m_uAlloc >= uAlloc ) {
		
		pData->m_uLen = uSize;
		}
	else {
		delete pData;
		
		Alloc(uSize, uAlloc);
		}
		
	wmemcpy(m_pText, pText, uSize);

	m_pText[uSize] = 0;
	}

// Attributes

UINT CString::GetLength(void) const
{
	return GetData(this)->m_uLen;
	}

// Read-Only Access

TCHAR CString::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : char(0);
	}

// Substring Extraction

CString CString::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	return CString(m_pText, uCount);
	}

CString CString::Right(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);
	
	PCTXT pSrc = m_pText + uLen - uCount;
	
	return CString(pSrc, uCount);
	}

CString CString::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	MakeMin(uCount, uLen - uPos);
	
	PCTXT pSrc = m_pText + uPos;
	
	return CString(pSrc, uCount);
	}

CString CString::Mid(UINT uPos) const
{
	UINT uLen = GetLength();
	
	MakeMin(uPos, uLen);
	
	PCTXT pSrc = m_pText + uPos;
	
	return CString(pSrc, uLen - uPos);
	}

// Buffer Managment

void CString::Empty(void)
{
	Release();
	}

void CString::Expand(UINT uAlloc)
{
	CopyOnWrite();
	
	CStringData *pData = GetData(this);
	
	if( pData->m_uAlloc < uAlloc ) {
		
		PCTXT pText = m_pText;
		
		Alloc(pData->m_uLen, uAlloc);
		
		wstrcpy(m_pText, pText);
		
		delete pData;
		}
	}

void CString::Compress(void)
{
	CopyOnWrite();
	
	CStringData *pData = GetData(this);
	
	if( pData->m_uAlloc > FindAlloc(pData->m_uLen) ) {
		
		PCTXT pText = m_pText;
		
		Alloc(pData->m_uLen);
		
		wstrcpy(m_pText, pText);
		
		delete pData;
		}
	}

void CString::CopyOnWrite(void)
{
	CStringData *pData = GetData(this);
	
	if( pData->m_nRefs ) {
		
		if( pData->m_nRefs > 1 ) {
			
			PCTXT pText = m_pText;
			
			pData->m_nRefs--;
			
			Alloc(pData->m_uLen);
			
			wstrcpy(m_pText, pText);
			}
		}
	else {
		Alloc(0);
		
		GetData(this)->m_uLen = 0;
		
		*m_pText = 0;
		}
	}

void CString::FixLength(void)
{
	GetData(this)->m_uLen = wstrlen(m_pText);
	}

// Concatenation In-Place

CString const & CString::operator += (CString const &That)
{
	That.AssertValid();
	
	if( !That.IsEmpty() ) {
		
		UINT uLen = GetData(this)->m_uLen;
		
		CString Copy = That;
		
		GrowString(uLen + Copy.GetLength());
		
		wstrcpy(m_pText + uLen, Copy.m_pText);
		}
	
	return ThisObject;
	}

CString const & CString::operator += (PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	if( *pText ) {
		
		UINT uLen = GetData(this)->m_uLen;
		
		if( pText >= m_pText && pText <= m_pText + uLen ) {
			
			CString Copy = pText;
			
			GrowString(uLen + Copy.GetLength());
		
			wstrcpy(m_pText + uLen, pText);
			}
		else {
			GrowString(uLen + wstrlen(pText));
		
			wstrcpy(m_pText + uLen, pText);
			}
		}
	
	return ThisObject;
	}

CString const & CString::operator += (PCSTR pText)
{
	AfxValidateStringPtr(pText);

	return operator += (CString(pText));
	}

CString const & CString::operator += (TCHAR cData)
{
	AfxAssert(cData);
	
	UINT uLen = GetData(this)->m_uLen;
	
	GrowString(uLen + 1);
	
	m_pText[uLen + 0] = cData;
	
	m_pText[uLen + 1] = 0;
	
	return ThisObject;
	}

// Concatenation via Friends

CString operator + (CString const &A, CString const &B)
{
	A.AssertValid();
	
	B.AssertValid();
	
	return CString(A.m_pText, A.GetLength(), B.m_pText, B.GetLength());
	}

CString operator + (CString const &A, PCTXT pText)
{
	A.AssertValid();
	
	AfxValidateStringPtr(pText);
	
	return CString(A.m_pText, A.GetLength(), pText, wstrlen(pText));
	}

CString operator + (CString const &A, TCHAR cData)
{
	A.AssertValid();
	
	AfxAssert(cData);
	
	return CString(A.m_pText, A.GetLength(), &cData, 1);
	}

CString operator + (PCTXT pText, CString const &B)
{
	AfxValidateStringPtr(pText);
	
	B.AssertValid();
	
	return CString(pText, wstrlen(pText), B.m_pText, B.GetLength());
	}

CString operator + (TCHAR cData, CString const &B)
{
	AfxAssert(cData);
	
	B.AssertValid();
	
	return CString(&cData, 1, B.m_pText, B.GetLength());
	}

// Write Data Access

void CString::SetAt(UINT uIndex, TCHAR cData)
{
	AfxAssert(uIndex < GetLength());
	
	AfxAssert(cData);
	
	CopyOnWrite();
	
	m_pText[uIndex] = cData;
	}

// Comparison Helper

int AfxCompare(CString const &a, CString const &b)
{
	return wstricmp(a.m_pText, b.m_pText);
	}

// Insert and Remove

void CString::Insert(UINT uIndex, PCTXT pText)
{
	UINT uLength = GetLength();

	UINT uCount  = wstrlen(pText);

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	wmemmove( m_pText + uIndex + uCount,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	wmemcpy(  m_pText + uIndex,
		  pText,
		  uCount
		  );
	}

void CString::Insert(UINT uIndex, CString const &That)
{
	UINT uLength = GetLength();

	UINT uCount  = That.GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + uCount);

	wmemmove( m_pText + uIndex + uCount,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	wmemcpy(  m_pText + uIndex,
		  That.m_pText,
		  uCount
		  );
	}

void CString::Insert(UINT uIndex, TCHAR cData)
{
	UINT uLength = GetLength();

	MakeMin(uIndex, uLength);

	GrowString(uLength + 1);

	wmemmove( m_pText + uIndex + 1,
		  m_pText + uIndex,
		  uLength - uIndex + 1
		  );

	m_pText[uIndex] = cData;
	}

void CString::Delete(UINT uIndex, UINT uCount)
{
	UINT uLength = GetLength();

	if( uIndex < uLength ) {
		
		MakeMin(uCount, uLength - uIndex);

		CopyOnWrite();

		wmemcpy( m_pText + uIndex,
			 m_pText + uIndex + uCount,
			 uLength - uIndex - uCount + 1
			 );

		GetData(this)->m_uLen -= uCount;
		}
	}

// Whitespace Trimming

void CString::TrimLeft(void)
{
	UINT c = GetLength();

	UINT n = 0;

	while( m_pText[n] ) {

		if( !wisspace(m_pText[n]) ) {

			break;
			}

		n++;
		}

	if( n ) {

		if( !m_pText[n] ) {

			Empty();

			return;
			}

		CopyOnWrite();

		wmemcpy(m_pText, m_pText + n, c - n + 1);

		GetData(this)->m_uLen -= n;
		}
	}

void CString::TrimRight(void)
{
	UINT c = GetLength();

	UINT n = c;

	while( n ) {

		if( !wisspace(m_pText[n - 1]) ) {

			break;
			}

		n--;
		}

	if( n < c ) {

		if( !n ) {

			Empty();

			return;
			}

		CopyOnWrite();

		m_pText[n] = 0;

		GetData(this)->m_uLen = n;
		}
	}

void CString::TrimBoth(void)
{
	TrimRight();

	TrimLeft();
	}

void CString::StripAll(void)
{
	UINT s = 0;

	UINT e = 0;

	for(;;) { 
	
		while( m_pText[s] ) {

			if( wisspace(m_pText[s]) ) {

				break;
				}

			s++;
			}

		e = s;	
		
		while( m_pText[e] ) {

			if( !wisspace(m_pText[e]) ) {

				break;
				}

			e++;
			}
	
		UINT cb = e - s;
		
		if( cb ) {

			if( cb == GetLength() ) {
			
				Empty();

				return;
				}
			
			CopyOnWrite();

			if( e == GetLength() ) {

				m_pText[s] = 0;

				GetData(this)->m_uLen = s;
				
				return;
				}

			wmemcpy( m_pText + s,
				 m_pText + e,
				 GetLength() - s - cb + 1
				 );

			GetData(this)->m_uLen -= cb;

			e = s;
			}
		else
			break;
		}
	}

// Case Switching

void CString::MakeUpper(void)
{
	CopyOnWrite();
	
	wstrupr(m_pText);
	}

void CString::MakeLower(void)
{
	CopyOnWrite();
	
	wstrlwr(m_pText);
	}

// Unicode Transforms

BOOL CString::FoldUnicode(UINT uFlags)
{
	CopyOnWrite();

	CStringData *pData = GetData(this);

	PTXT pFrom = wstrdup(pData->m_cData);

	UINT uFrom = pData->m_uLen;

	UINT uNeed = FoldString( uFlags,
				 pFrom,
				 uFrom,
				 NULL,
				 0
				 );

	if( uNeed ) {

		if( GrowString(uNeed) ) {

			pData = GetData(this);
			}

		FoldString( uFlags,
			    pFrom,
			    uFrom,
			    pData->m_cData,
			    uNeed
			    );

		pData->m_cData[uNeed] = 0;

		free(pFrom);

		return TRUE;
		}

	free(pFrom);

	return FALSE;
	}

BOOL CString::RemoveUpperDiacriticals(void)
{
	FoldUnicode(MAP_COMPOSITE);

	CWordArray Type;

	GetTypeInfo3(Type);

	BOOL fHit = FALSE;

	for( UINT n = 1; n < Type.GetCount(); n++ ) {

		if( Type[n] & C3_DIACRITIC ) {

			if( IsCharUpper(GetAt(n-1)) ) {

				Type.Remove(n, 1);

				Delete     (n, 1);
				
				fHit = TRUE;

				n--;
				}
			}
		}

	FoldUnicode(MAP_PRECOMPOSED);

	return fHit;
	}

BOOL CString::RemoveDiacriticals(void)
{
	FoldUnicode(MAP_COMPOSITE);

	CWordArray Type;

	GetTypeInfo3(Type);

	BOOL fHit = FALSE;

	for( UINT n = 1; n < Type.GetCount(); n++ ) {

		if( Type[n] & C3_DIACRITIC ) {

			Type.Remove(n, 1);

			Delete     (n, 1);
			
			fHit = TRUE;

			n--;
			}
		}

	FoldUnicode(MAP_PRECOMPOSED);

	return fHit;
	}

// Replacement

UINT CString::Replace(TCHAR cFind, TCHAR cNew)
{
	UINT uCount = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cFind ) {

			if( !uCount ) {

				CopyOnWrite();
				}

			m_pText[n] = cNew;

			uCount++;
			}
		}

	return uCount;
	}

UINT CString::Replace(PCTXT pFind, PCTXT pNew)
{
	UINT uCount = 0;

	UINT uStart = 0;

	for(;;) {

		UINT uPos = Find(pFind, uStart);

		if( uPos < NOTHING ) {
			
			Delete(uPos, wstrlen(pFind));

			Insert(uPos, pNew);

			uStart = uPos + wstrlen(pNew);

			uCount ++;
			}
		else
			break;
		}

	return uCount;
	}

// Removal

void CString::Remove(TCHAR cFind)
{
	for(;;) {

		UINT uPos = Find(cFind);

		if( uPos == NOTHING ) {

			return;
			}

		Delete(uPos, 1);
		}
	}

CString CString::Without(TCHAR cFind)
{
	CString Work = ThisObject;

	Work.Remove(cFind);

	return Work;
	}

// Parsing

UINT CString::Tokenize(CStringArray &Array, TCHAR cFind) const
{
	return Tokenize(Array, NOTHING, cFind);
	}

UINT CString::Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cQuote) const
{
	return Tokenize(Array, NOTHING, cFind, cQuote);
	}

UINT CString::Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const
{
	return Tokenize(Array, NOTHING, cFind, cOpen, cClose);
	}

UINT CString::Tokenize(CStringArray &Array, PCTXT pFind) const
{
	return Tokenize(Array, NOTHING, pFind);
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind, TCHAR cQuote) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind, cQuote));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {

		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(cFind, cOpen, cClose));

		uCount++;
		}

	return uCount;
	}

UINT CString::Tokenize(CStringArray &Array, UINT uLimit, PCTXT pFind) const
{
	UINT    uCount = 0;

	CString Text   = ThisObject;

	while( !Text.IsEmpty() ) {
		
		if( uCount == uLimit - 1 ) {

			Array.Append(Text);

			return uLimit;
			}

		Array.Append(Text.StripToken(pFind));

		uCount++;
		}

	return uCount;
	}

CString CString::TokenLeft(TCHAR cFind) const
{
	UINT uPos = Find(cFind);

	return Left(uPos);
	}

CString CString::TokenFrom(TCHAR cFind) const
{
	UINT uPos = Find(cFind);

	if( uPos < NOTHING ) {

		return Mid(uPos+1);
		}

	return L"";
	}

CString CString::TokenLast(TCHAR cFind) const
{
	UINT uPos = FindRev(cFind);

	return Mid(uPos+1);
	}

CString CString::StripToken(TCHAR cFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return L"";
	}

CString CString::StripToken(TCHAR cFind, TCHAR cQuote)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind, cQuote);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return L"";
	}

CString CString::StripToken(TCHAR cFind, TCHAR cOpen, TCHAR cClose)
{
	if( !IsEmpty() ) {

		UINT    uPos = Find(cFind, cOpen, cClose);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + 1);

		return Text;
		}

	return L"";
	}

CString CString::StripToken(PCTXT pFind)
{
	if( !IsEmpty() ) {

		UINT    uPos = FindOne(pFind);

		CString Text = Left(uPos);

		if( uPos == NOTHING ) {

			Empty();
			}
		else
			Delete(0, uPos + wstrlen(pFind));

		return Text;
		}

	return L"";
	}

// Building

void CString::Build(CStringArray &Array, TCHAR cSep)
{
	Empty();

	UINT c = Array.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			operator += (cSep);
			}

		operator += (Array[n]);
		}
	}

void CString::Build(CStringArray &Array, PCTXT pSep)
{
	Empty();

	UINT c = Array.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( n ) {

			operator += (pSep);
			}

		operator += (Array[n]);
		}
	}

// Resource Loading

void CString::Load(ENTITY ID)
{
	Release();
	
	PTXT pText = IntLoadString(ID);
	
	Alloc(wstrlen(pText));
	
	wstrcpy(m_pText, pText);
	
	delete pText;
	}

// Printf Formatting

void CString::Printf(PCTXT pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntPrintf(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

void CString::Printf(CEntity Format, ...)
{
	va_list pArgs;
	
	va_start(pArgs, Format);

	PTXT pFormat = IntLoadString(Format);
	
	PTXT pResult = IntPrintf(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	
	delete pFormat;

	va_end(pArgs);
	}

void CString::VPrintf(PCTXT pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntPrintf(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	}

void CString::VPrintf(ENTITY Format, va_list pArgs)
{
	PTXT pFormat = IntLoadString(Format);
	
	PTXT pResult = IntPrintf(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	
	delete pFormat;
	}

// Legacy Printf

void CString::Printf(PCSTR pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntPrintf(CString(pFormat), pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

void CString::VPrintf(PCSTR pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntPrintf(CString(pFormat), pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	}

// Windows Formatting

void CString::Format(PCTXT pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

void CString::Format(CEntity Format, ...)
{
	va_list pArgs;
	
	va_start(pArgs, Format);

	PTXT pFormat = IntLoadString(Format);
	
	PTXT pResult = IntFormat(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	
	delete pFormat;

	va_end(pArgs);
	}

void CString::VFormat(PCTXT pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	}

void CString::VFormat(ENTITY Format, va_list pArgs)
{
	PTXT pFormat = IntLoadString(Format);
	
	PTXT pResult = IntFormat(pFormat, pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	
	delete pFormat;
	}

// Legacy Format

void CString::Format(PCSTR pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(CString(pFormat), pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

void CString::VFormat(PCSTR pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(CString(pFormat), pArgs);
	
	GrowString(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
	}

// Resource Name Encoding

void CString::SetResourceName(PCTXT pName)
{
	if( HIWORD(pName) ) {
		
		Release();
		
		Alloc(wstrlen(pName));
		
		wstrcpy(m_pText, pName);
		}
	else {
		CTEXT sHex[] = L"0123456789ABCDEF";
		
		Release();
		
		Alloc(5);
		
		m_pText[0] = '#';
		
		m_pText[1] = sHex[(LOWORD(pName) >> 12) & 15];
		m_pText[2] = sHex[(LOWORD(pName) >>  8) & 15];
		m_pText[3] = sHex[(LOWORD(pName) >>  4) & 15];
		m_pText[4] = sHex[(LOWORD(pName)      ) & 15];
		
		m_pText[5] = 0;
		}
	}

// Diagnostics

void CString::AssertValid(void) const
{
	AfxValidateReadPtr(this, sizeof(ThisObject));
	
	CStringData *pData = GetData(this);
	
	if( pData->m_nRefs == 0 ) {
		
		AfxAssert(m_pText == m_Empty.m_cData);
		
		AfxAssert(pData->m_uLen == 0);
		}
	else
		AfxAssert(pData->m_uAlloc > pData->m_uLen);
	
	AfxValidateStringPtr(m_pText);

	AfxAssert(m_Empty.m_nRefs == 0);
	}

// Protected Constructor

CString::CString(PCTXT p1, UINT u1, PCTXT p2, UINT u2)
{
	Alloc(u1 + u2);
	
	wstrncpy(m_pText, p1, u1);
	
	wstrncpy(m_pText + u1, p2, u2);
	
	m_pText[u1 + u2] = 0;
	}

// Initialisation

void CString::InitFrom(GUID const &Guid)
{
	AfxValidateReadPtr(&Guid, sizeof(GUID));

	LPOLESTR pText = NULL;

	StringFromCLSID(Guid, &pText);

	Alloc(wstrlen(pText));
	
	wstrcpy(m_pText, pText);

	CoTaskMemFree(pText);
	}

// Internal Helpers

PTXT CString::IntPrintf(PCTXT pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	UINT uSize = 256;
	
	for(;;) {
		
		PTXT pWork = New TCHAR [ uSize ];
		
		pWork[uSize - 1] = 0;
		
		if( _vsnwprintf(pWork, uSize - 1, pFormat, pArgs) < 0 ) {
			
			delete pWork;
			
			uSize <<= 1;
			
			AfxAssert(uSize);
			
			continue;
			}
		
		return pWork;
		}
	}

PTXT CString::IntFormat(PCTXT pFormat, va_list pArgs)
{
	AfxValidateStringPtr(pFormat);
	
	UINT uSize = 256;
	
	for(;;) {
		
		PTXT    pWork   = New TCHAR [ uSize ];

		va_list pCopy   = pArgs;
		
		DWORD   dwFlags = FORMAT_MESSAGE_FROM_STRING;

		if( !FormatMessage(dwFlags, pFormat, 0, 0, pWork, uSize, &pCopy) ) {
			
			delete pWork;
			
			uSize <<= 1;
			
			AfxAssert(uSize);
			
			continue;
			}
		
		return pWork;
		}
	}

PTXT CString::IntLoadString(ENTITY ID)
{
	UINT uSize = 256;
	
	for(;;) {
	
		PTXT pWork = New TCHAR [ uSize ];
		
		UINT uCopy = CModule::LoadString(ID, pWork, uSize);

		if( !uCopy ) {
		
			/*AfxTrace(L"ERROR: Cannot load string %4.4X\n", ID.GetID());*/
		
			swprintf(pWork, L"<%4.4X>", ID.GetID());
			
			return pWork;
			}

		if( uCopy < uSize - 1 ) {
			
			return pWork;
			}

		delete pWork;
			
		uSize <<= 1;
		
		AfxAssert(uSize);
		}
	}

// Clearing

void AfxSetZero(CString &Text)
{
	Text.Empty();
	}

// Implementation

void CString::Alloc(UINT uLen, UINT uAlloc)
{
	AfxAssert(uAlloc > uLen);
	
	uAlloc     = AdjustSize(uAlloc);
	
	UINT uSize = uAlloc + sizeof(CStringData);
	
	CStringData *pData = (CStringData *) New BYTE [ uSize ];
	
	pData->m_uLen   = uLen;
	
	pData->m_uAlloc = uAlloc;
	
	pData->m_nRefs  = 1;
	
	m_pText = pData->m_cData;
	}

void CString::Alloc(UINT uLen)
{
	Alloc(uLen, FindAlloc(uLen));
	}

BOOL CString::GrowString(UINT uLen)
{
	CopyOnWrite();
	
	CStringData *pData = GetData(this);
	
	if( FindAlloc(uLen) > pData->m_uAlloc ) {
		
		PCTXT pText = m_pText;
		
		Alloc(uLen);
		
		wstrcpy(m_pText, pText);
		
		delete pData;

		return TRUE;
		}

	pData->m_uLen = uLen;

	return FALSE;
	}

void CString::Release(void)
{
	CStringData *pData = GetData(this);
	
	if( pData->m_nRefs ) {
		
		if( !--(pData->m_nRefs) ) {

			delete pData;
			}
		
		m_pText = m_Empty.m_cData;
		}
	}

UINT CString::AdjustSize(UINT uAlloc)
{
	if( uAlloc < 1024 ) {

		return ((uAlloc + 3) & ~3);
		}

	for( UINT n = 1024;; n *= 4 ) {

		if( n >= uAlloc ) {

			return n;
			}
		}
	}

UINT CString::FindAlloc(UINT uLen)
{
	return sizeof(TCHAR) * (1 + uLen);
	}

// End of File
