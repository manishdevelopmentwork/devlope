
#include "Intern.hpp"

#include "Connectors.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Connectors Item
//

// Dynamic Class

AfxImplementDynamicClass(CConnectors, CMetaItem);

// Constructor

CConnectors::CConnectors(void)
{
	}

// Attributes

UINT CConnectors::GetTreeImage(void) const
{
	return IDI_SERVICES;
	}

// Meta Data Creation

void CConnectors::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_SetName((IDS_CONNECTORS));
	}

// End of File
