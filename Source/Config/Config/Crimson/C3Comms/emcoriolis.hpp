//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "emcoriolisdevices.hpp"

#ifndef INCLUDE_EMCORIOLIS_HPP

#define INCLUDE_EMCORIOLIS_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Device
//

class CCoriolisModel : public CEMCoriolisDevices 
{
	public:
		// Constructors
		CCoriolisModel(void);
		CCoriolisModel(CString Name, UINT uDeviceModel);

		// Destructor
		~CCoriolisModel(void);

		// Properties
		CString     GetName(void);
		CEMRegister GetRegister(UINT uIndex);
		CEMRegister GetSelectedReg(void);
		void	    SetSelectedReg(CEMRegister const &Reg);
		CRegArray   GetRegs(UINT uChannel, BOOL fSortAlpha);
		CRegArray   GetCombinedRegs(UINT uRegType, BOOL fSortAlpha);
		BOOL        AddRegister(CEMRegister Reg);
		BOOL	    FindRegister(CString Text);
		void	    DoSortRegs(BOOL fSortAlpha);
		CRegArray   GetUnifiedRegs(UINT uChannel, BOOL fSortAlpha);

	protected:
		
		// Properties
		CString		  m_Name;
		UINT		  m_uDeviceModel;
		CRegArray	  m_Registers;
		CRegArray         m_CoilRegisters;
		CRegArray         m_HoldingRegisters;
		CEMRegister	  m_SelectedRegister;
		
		UINT              GetAddressType(CString Text, UINT Default);

		BOOL MakeRegisters(UINT uChannel);

		// Sorting
		void	   SortRegs(CRegArray &Registers, BOOL fSortAlpha);
		static int SortHelpAlpha(const void *a, const void *b);	
		static int SortHelpNum(const void *a, const void *b);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Serial Driver Options
//

class CCoriolisSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCoriolisSerialDriverOptions(void);

		// Destructor
		~CCoriolisSerialDriverOptions(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	
		// Properties
		UINT m_Protocol;
		UINT m_Track;
		UINT m_Timeout;

	protected:
	
		// Meta Data Creation
		void AddMetaData(void);

	};

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Base Device Options
//

class CCoriolisBaseDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCoriolisBaseDeviceOptions(void);
	
		// Destructor
		~CCoriolisBaseDeviceOptions(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		void InitRegisterData(CInitData &Init);

		// Manage Models
		void		AddEMModel(CCoriolisModel Model);
		UINT		GetEMModelCount(void);
		CCoriolisModel	GetCurrentModel(void);
		CCoriolisModel	GetEMModel(UINT uPos);
		CRegArray	GetModelRegs(UINT uChannel);
		CRegArray	GetCombinedRegs(UINT uRegType);
		UINT            GetRegisterTable(UINT uChannel);
		CString         GetPrefixText(UINT uChannel);
		CString		GetCaptionText(UINT uChannel);

		CEMRegister	GetSelectedReg(void);
		void		SetSelectedReg(CEMRegister const &Reg);

		CString		GetTypeString(UINT uType);
		UINT		GetAddressType(CString Text, UINT Default);

		// Register Sorting
		void		SetSort(BOOL fSortAlpha);
		BOOL		GetSort(void);
		void		SortRegs(BOOL fSortAlpha);

		CRegArray       GetUnifiedRegs(UINT uRegType, BOOL fSortAlpha);

	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Load Models
		void LoadModels(void);
		void InitRegister(void);

		// Properties
		UINT			m_Type;
		UINT			m_Ping;
		UINT			m_Drop;
		BOOL			m_fSortAlpha;

		CArray<CCoriolisModel>	m_Models;

		CEMRegister		m_SelectedReg;
	};

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Serial Device Options
//

class CCoriolisSerialBaseDeviceOptions : public CCoriolisBaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCoriolisSerialBaseDeviceOptions(void);
	
		// Destructor
		~CCoriolisSerialBaseDeviceOptions(void);

		// UI Managament
		BOOL OnLoadPages(CUIPageList * pList);
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis TCP Base Device Options
//

class CCoriolisTCPBaseDeviceOptions : public CCoriolisBaseDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCoriolisTCPBaseDeviceOptions(void);

		// Destructor
		~CCoriolisTCPBaseDeviceOptions(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		BOOL OnLoadPages(CUIPageList * pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:

		//Meta Data Creation
		void AddMetaData(void);

		// Properties
		DWORD			m_Addr;
		DWORD			m_Addr2;
		UINT			m_Port;
		UINT			m_Keep;
		UINT			m_UsePing;
		UINT			m_Time1;
		UINT			m_Time2;
		UINT			m_Time3;
	};

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Device Page UI
//

class CCoriolisBaseDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCoriolisBaseDeviceOptionsUIPage(CCoriolisBaseDeviceOptions *pDevice, CLASS DeviceClass);
		
		// Load View
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		// Properties
		CLASS				 m_Class;
		CCoriolisBaseDeviceOptions	*m_pDevice;

		// Load UI
		void LoadModels(IUICreate *pView, CItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Base Driver
//

class CCoriolisBaseDriver : public CBasicCommsDriver
{	
	public:
		// Constructor
		CCoriolisBaseDriver();

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Properties
		UINT m_uCount;
	};

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Serial Driver
//

class CCoriolisSerialDriver : public CCoriolisBaseDriver
{
	public:
		// Constructor
		CCoriolisSerialDriver(void);
		
		// Destructor
		~CCoriolisSerialDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

//////////////////////////////////////////////////////////////////////////
//
// Coriolis TCP Driver
//

class CCoriolisTCPDriver : public CCoriolisBaseDriver
{
	public:
		// Constructor
		CCoriolisTCPDriver(void);

		// Destructor
		~CCoriolisTCPDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Dialog Management
//

class CCoriolisFullDlg : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCoriolisFullDlg(CCoriolisBaseDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Destructor
		~CCoriolisFullDlg(void);

	protected:

		// Message Map
		AfxDeclareMessageMap();

		// Properties
		UINT				m_uChan;
		UINT				m_uType;
		UINT				m_uInitSel;
		CEMRegister			m_Reg;
		CAddress			&m_Addr;
		CRegArray                       m_TableRegs;

		// Device Management
		CCoriolisBaseDriver		&m_Driver;
		CCoriolisBaseDeviceOptions     *m_pDevice;
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		BOOL OnOkay(UINT uID);
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSelChanged(UINT uID, CWnd &Wnd);
		void OnTypeChanged(UINT uID, CWnd &Wnd);
		void OnChannelChanged(UINT uID, CWnd &Wnd);
		void OnSortAlpha(UINT uID, CWnd &Wnd);

		// Implementation
		void LoadRegs(void);
		void LoadTypes(void);
		void LoadChannels(void);
		void AppendType(void);
		void ShowDetails(void);
		void ShowAddress(void);
		void ShowType(void);
		void DoEnables(void);

		// Text Handling
		CString GetTypeString(UINT uType);
		CString GetComboText(UINT uChannel); 
		void CleanText(CString &Text);
	};


#endif