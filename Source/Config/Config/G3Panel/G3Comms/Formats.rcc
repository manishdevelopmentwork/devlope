
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Item Schemas
//

CDispFormatNumber_Schema RCDATA
BEGIN
	L"Radix,Number Base,,Enum/DropDown,Decimal|Hexadecimal|Binary|Octal|Passcode,"
	L"Indicate the number base to be used for displaying this tag."
	L"\0"
	
	L"Before,Digits Before DP,,Integer/EditBox,0|0||1|32,"
	L"Indicate the number of digits to be shown before the decimal point, or, if "
	L"no decimal point is enabled, the total number of digits in the field."
	L"\0"

	L"After,Digits After DP,,Integer/EditBox,0|0||0|8,"
	L"Indicate the number of digits to be shown after the decimal point."
	L"\0"

	L"Leading,Lead Character,,Enum/DropDown,Zeroes|Spaces|None,"
	L"Indicate what leading characters should be shown."
	L"\0"

	L"Group,Group Digits,,Enum/DropDown,No|Yes,"
	L"Indicate whether groups of digits should be separated by comma or "
	L"dashes as appropriate to the number base. Decimal and octal digits are "
	L"grouped in threes, while hex and binary digits are grouped in fours."
	L"\0"

	L"Signed,Sign Mode,,Enum/DropDown,Unsigned|Soft Sign|Hard Sign,"
	L"Indicate whether or not a sign is to be displayed in front of the number. "
	L"A soft sign does not show as a '+' in front of positive numbers, whereas "
	L"a hard sign does."
	L"\0"

	L"Prefix,Prefix,,IntlString/IntlString,16,"
	L"Provide a textual prefix to be placed before the numeric value."
	L"\0"

	L"Units,Units,,IntlString/IntlString,16,"
	L"Provide a textual suffix to be placed after the numeric value."
	L"\0"

	L"DynDP,Dynamic DP,,Coded/Expression,|none,"
	L"Provide the dynamic number of digits to be shown after the decimal point. "
	L"The number of digits shown will be between 0 and the fixed number of digits "
	L"after the decimal place, configured in the Fixed Data Format."
	L"\0"

	L"\0"
END

CDispFormatSci_Schema RCDATA
BEGIN
	L"After,Digits After DP,,Integer/EditBox,0|0||0|8,"
	L"Indicate the number of digits to be shown after the decimal point."
	L"\0"

	L"ManSign,Mantissa Sign Mode,,Enum/DropDown,Soft Sign|Hard Sign,"
	L"Indicate the type of sign to be shown on the mantissa. A soft sign "
	L"does not show as a '+' in front of positive numbers, whereas "
	L"a hard sign does."
	L"\0"

	L"ExpSign,Exponent Sign Mode,,Enum/DropDown,Soft Sign|Hard Sign,"
	L"Indicate the type of sign to be shown on the exponent. A soft sign "
	L"does not show as a '+' in front of positive numbers, whereas "
	L"a hard sign does."
	L"\0"

	L"Prefix,Prefix,,IntlString/IntlString,16,"
	L"Provide a textual prefix to be placed before the numeric value."
	L"\0"

	L"Units,Units,,IntlString/IntlString,16,"
	L"Provide a textual suffix to be placed after the numeric value."
	L"\0"

	L"DynDP,Dynamic DP,,Coded/Expression,|none,"
	L"Provide the dynamic number of digits to be shown after the decimal point. "
	L"The number of digits shown will be between 0 and the fixed number of digits "
	L"after the decimal place, configured in the Fixed Data Format."
	L"\0"

	L"\0"
END

CDispFormatFlag_Schema RCDATA
BEGIN
	L"On,ON State,,IntlString/IntlString,20|ON,"
	L"Provide the text to be used to indicate a true state."
	L"\0"

	L"Off,OFF State,,IntlString/IntlString,20|OFF,"
	L"Provide the text to be used to indicate a false state."
	L"\0"

	L"\0"
END

CDispFormatMulti_Schema RCDATA
BEGIN
	L"Count,States,,Integer/Pick,0|0||2|500,"
	L"Indicate the number of states to be supported by this format."
	L"\0"

	L"Limit,Limit,,Coded/Expression,|none,"
	L"Indicate the number of states to be used during the decode "
	L"process. Unlike the Count property, this setting can be an "
	L"expression and is typically used to dynamically restrict the "
	L"number of states to be offered during data entry."
	L"\0"

	L"Default,Default,,IntlString/IntlString,20,"
	L"Indicate the string to be shown if no state is matched."
	L"\0"

	L"Range,Match Type,,Enum/DropDown,Discrete|Ranged,"
	L"Indicate whether the format should be treat each entry as "
	L"a value to be matched, or as the upper bound of a range."
	L"\0"

	L"List,Format States,,,"
	L"\0"

	L"\0"
END

CDispFormatMultiEntry_Schema RCDATA
BEGIN
	L"Data,Data,,ExprNumeric/ExprEditBox,20|Not Used,"
	L"Indicate the data value for this state."
	L"\0"

	L"Text,Text,,IntlString/IntlString,20,"
	L"Enter the text to be shown when this state is matched."
	L"\0"

	L"\0"
END

CDispFormatTimeDate_Schema RCDATA
BEGIN
	L"Mode,Field Contents,,Enum/DropDown,Time Only|Date Only|Time Then Date|Date Then Time|Elapsed Time (1-Digit Hours)|Elapsed Time (2-Digit Hours)|Elapsed Time (3-Digit Hours)|Elapsed Time (4-Digit Hours),"
	L"Indicate what the field should display. Elapsed time formats "
	L"treat their hours portion as a number that can be greater than 24 "
	L"while regular time formats consider the hours portion to be a time "
	L"of day and thus to be restricted in range."
	L"\0"

	L"Secs,Show Seconds,,Enum/DropDown,No|Yes,"
	L"Indicate whether the seconds field should be shown."
	L"\0"

	L"TimeForm,Time Format,,Enum/DropDown,Locale Default|24 Hour (Military)|12 Hour (Civil),"
	L"Indicate the format to be used when displaying the time portion of the field."
	L"\0"

	L"TimeSep,Time Separator,,TimeSep/DropDown,,"
	L"Select the character to be used to separate elements of the time."
	L"\0"

	L"AM,AM Suffix,,IntlString/IntlString,8| AM,"
	L"Provide the textual suffix to be used for times before noon."
	L"\0"

	L"PM,PM Suffix,,IntlString/IntlString,8| PM,"
	L"Provide the textual suffix to be used for times after noon."
	L"\0"

	L"DateForm,Date Format,,Enum/DropDown,Locale Default|Month Date Year|Date Month Year|Year Month Date,"
	L"Indicate the order of the elements that make up the date portion of the field."
	L"\0"

	L"DateSep,Date Separator,,DateSep/DropDown,,"
	L"Select the character to be used to separate elements of the date."
	L"\0"

	L"Year,Show Year,,Enum/DropDown,As 2 Digits|As 4 Digits|No,"
	L"Indicate whether the year portion of the date should be shown, "
	L"and if so, how many digits should be used."
	L"\0"
	
	L"Month,Show Month,,Enum/DropDown,As Digits|As Name,"
	L"Indicate whether months should be show numerically "
	L"or by using their three-letter abbreviated names."
	L"\0"

	L"\0"
END

CDispFormatLinked_Schema RCDATA
BEGIN
	L"Link,Format Like,,Coded/Expression,|None,"
	L"Indicate the source tag for the format."
	L"\0"

	L"\0"
END

CDispFormatString_Schema RCDATA
BEGIN
	L"Template,Template,,String/EditBox,200|None|32,"
	L"Define the optional template to limit the characters that can be entered "
	L"into the field, and to force the inclusion of appropriate separators."
	L"\0"

	L"Length,Max Length,,Integer/EditBox,0|0||1|200,"
	L"Indicate the maximum length of the string."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Page Schemas
//

CDispFormatNumber_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:2,root,Fixed Data Format,Radix,Signed,Before,After,Leading,Group\0"
	L"G:2,root,Dynamic Data Format,DynDP\0"
	L"G:1,root,Format Units,Prefix,Units\0"
	L"\0"
END

CDispFormatSci_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Fixed Data Format,ManSign,ExpSign,After\0"
	L"G:2,root,Dynamic Data Format,DynDP\0"
	L"G:1,root,Format Units,Prefix,Units\0"
	L"\0"
END

CDispFormatIPAddr_Page RCDATA
BEGIN
	L"P:1\0"
	L"\0"
END

CDispFormatFlag_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Data Format,On,Off\0"
	L"\0"
END

CDispFormatTimeDate_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Format Mode,Mode\0"
	L"G:1,root,Time Format,TimeForm,TimeSep,Secs,AM,PM\0"
	L"G:1,root,Date Format,DateForm,DateSep,Year,Month\0"
	L"\0"
END

CDispFormatLinked_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Format Data,Link\0"
	L"\0"
END

CDispFormatString_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Format Data,Template,Length\0"
	L"\0"
END

// End of File
