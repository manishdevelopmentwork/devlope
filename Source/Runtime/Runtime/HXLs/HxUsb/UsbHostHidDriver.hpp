
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostHidDriver_HPP

#define	INCLUDE_UsbHostHidDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Framework
//

#include "Hid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Host Hid Driver
//

class CUsbHostHidDriver : public CUsbHostFuncDriver, public IUsbHostHid
{
	public:
		// Constructor
		CUsbHostHidDriver(void);

		// Destructor
		~CUsbHostHidDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostHid
		UINT METHOD GetUsagePage(void);
		UINT METHOD GetUsageType(void);
		BOOL METHOD GetReportData(PCBYTE &pData, UINT &uSize);
		BOOL METHOD GetReportDesc(PBYTE pData, UINT uSize);
		BOOL METHOD GetReport(WORD wType, WORD wId, PBYTE pData, UINT uSize);
		BOOL METHOD SetReport(WORD wType, WORD wId, PBYTE pData, UINT uSize);
		BOOL METHOD GetIdle(BYTE &bIdle);
		BOOL METHOD SetIdle(BYTE bIdle);
		BOOL METHOD GetProtocol(BYTE &bProtocol);
		BOOL METHOD SetProtocol(BYTE bProtocol);
		BOOL METHOD RecvReport(PBYTE pData, UINT uCount);
		UINT METHOD WaitReport(UINT uTimeout);
		void METHOD SetConfig(PCBYTE pConfig, UINT uSize);

	protected:
		// Data
		IUsbHidMapper * m_pMapper;
		IUsbPipe      * m_pCtrl;
		IUsbPipe      * m_pRecv;
		UINT		m_iRecv;
		PBYTE		m_pReportData;
		UINT		m_uReportSize;

		// Implementation
		bool FindPipes(CUsbDescList const &List);
		bool FindConfig(CUsbDescList const &List);
		void ParseReport(void);
		void CreateMapper(UINT uPage, UINT uUsage);
		bool StartMapper(void);
		void FreeMapper(void);
		void AllocReport(UINT uSize);
		void FreeReport(void);
	};

// End of File

#endif
