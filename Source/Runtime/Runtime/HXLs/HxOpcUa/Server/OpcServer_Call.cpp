
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Services

OpcUa_StatusCode COpcServer::Call(OpcUa_Endpoint                  hEndpoint,
				  OpcUa_Handle                    hContext,
				  const OpcUa_RequestHeader *     pRequestHeader,
				  OpcUa_Int32                     nNoOfMethodsToCall,
				  const OpcUa_CallMethodRequest * pMethodsToCall,
				  OpcUa_ResponseHeader*           pResponseHeader,
				  OpcUa_Int32*                    pNoOfResults,
				  OpcUa_CallMethodResult**        pResults,
				  OpcUa_Int32*                    pNoOfDiagnosticInfos,
				  OpcUa_DiagnosticInfo**          pDiagnosticInfos
)
{
	AfxTrace("opc: call\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true) < NOTHING ) {

		if( !nNoOfMethodsToCall ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);

			return OpcUa_BadNothingToDo;
		}
		else {
			*pResults = OpcAlloc(nNoOfMethodsToCall, OpcUa_CallMethodResult);

			for( int m = 0; m < nNoOfMethodsToCall; m++ ) {

				OpcUa_CallMethodRequest const *pCall   = pMethodsToCall + m;

				OpcUa_CallMethodResult        *pResult = *pResults      + m;

				OpcUa_CallMethodResult_Initialize(pResult);

				pResult->StatusCode = OpcUa_BadNodeIdUnknown;

				AfxTouch(pCall);
			}

			*pNoOfResults = nNoOfMethodsToCall;

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			return OpcUa_Good;
		}
	}

	return pResponseHeader->ServiceResult;
}

// End of File
