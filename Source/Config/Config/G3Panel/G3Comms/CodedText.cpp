
#include "Intern.hpp"

#include "CodedText.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Coded Text
//

// Dynamic Class

AfxImplementDynamicClass(CCodedText, CCodedItem);

// Constructor

CCodedText::CCodedText(void)
{
	}

// Attributes

CString CCodedText::GetExportInfo(void) const
{
	CItem   *pItem = GetParent();

	CString  Path  = pItem->GetHumanPath();

	if( pItem->IsKindOf(AfxRuntimeClass(CUIItem)) ) {

		CUIItem *pUI = (CUIItem *) pItem;

		CString  Lab = pUI->GetSubItemLabel(this);

		if( !Lab.IsEmpty() ) {

			if( Lab != L"Text" ) {

				Path += L" - ";

				Path += Lab;
				}
			}
		}

	return Path;
	}

CString CCodedText::GetText(void) const
{
	if( this ) {

		return GetText(m_pSystem->m_pLang->m_Lang, NULL);
		}

	return L"";
	}

CString CCodedText::GetText(UINT uLang) const
{
	if( this ) {

		return GetText(uLang, NULL);
		}

	return L"";
	}

CString CCodedText::GetText(PDWORD pParam) const
{
	if( this ) {

		return GetText(m_pSystem->m_pLang->m_Lang, pParam);
		}

	return L"";
	}

CString CCodedText::GetText(UINT uLang, PDWORD pParam) const
{
	if( this ) {

		DWORD Data = Execute(typeString, pParam);

		if( Data ) {

			PTXT p = PTXT(Data);

			if( !IsConst() ) {

				CString t = p;

				free(p);

				return t;
				}

			if( uLang ) {

				UINT f = NOTHING;

				UINT i = 0;

				for(;;) {

					for(;;) {

						if( p[i] == 0x00 ) {

							break;
							}

						if( p[i] == LANG_SEP ) {

							if( f == NOTHING ) {

								f = i;
								}

							break;
							}

						i++;
						}

					if( p[i++] ) {

						if( !--uLang ) {

							if( p[i] && p[i] != LANG_SEP ) {

								PTXT s = wstrchr(p + i, LANG_SEP);

								if( s ) {

									*s = 0;
									}

								CString t = p + i;

								free(p);

								return t;
								}

							break;
							}

						continue;
						}

					break;
					}

				if( f < NOTHING ) {

					p[f] = 0;
					}

				CString t = p;

				free(p);

				return t;
				}
			else {
				PTXT s = wstrchr(p, LANG_SEP);

				if( s ) {

					*s = 0;
					}

				CString t = p;

				free(p);

				return t;
				}
			}
		}

	return L"";
	}

BOOL CCodedText::HasText(UINT uLang) const
{
	if( this ) {

		DWORD Data = Execute(typeString);

		if( Data ) {

			PTXT p = PTXT(Data);

			if( !IsConst() ) {

				free(p);

				return FALSE;
				}

			if( uLang ) {

				UINT f = NOTHING;

				UINT i = 0;

				for(;;) {

					for(;;) {

						if( p[i] == 0x00 ) {

							break;
							}

						if( p[i] == LANG_SEP ) {

							if( f == NOTHING ) {

								f = i;
								}

							break;
							}

						i++;
						}

					if( p[i++] ) {

						if( !--uLang ) {

							if( p[i] && p[i] != LANG_SEP ) {

								free(p);

								return TRUE;
								}

							break;
							}

						continue;
						}

					break;
					}

				free(p);

				return FALSE;
				}

			free(p);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodedText::IsStringConst(void) const
{
	return IsStringConst(GetSource(TRUE));
	}

// Operations

BOOL CCodedText::SetText(CString Text, BOOL fAll)
{
	return SetText(Text, m_pSystem->m_pLang->m_Lang, fAll);
	}

BOOL CCodedText::SetText(CString Text, UINT uLang, BOOL fAll)
{
	Text.Replace(L"\"", L"\\\"");

	if( !fAll ) {

		CString Prev = GetSource(TRUE);

		if( IsStringConst(Prev) ) {

			Prev = Prev.Mid(1, Prev.GetLength() - 2);

			CStringArray List;

			Prev.Tokenize(List, LANG_SEP);

			UINT uCount = List.GetCount();

			if( uLang >= uCount ) {

				uCount = uLang + 1;

				List.SetCount(uCount);
				}

			if( uLang ) {

				if( List.GetAt(0).IsEmpty() ) {

					List.SetAt(0, Text);
					}
				}

			List.SetAt(uLang, Text);

			if( TRUE ) {

				CString Expr;

				Expr += L'"';

				for( UINT n = 0; n < uCount; n++ ) {

					Expr += List[n];

					if( n < uCount - 1 ) {

						Expr += LANG_SEP;
						}
					}

				Expr += L'"';

				return Compile(Expr);
				}
			}
		}

	CString Expr = L'"' + Text + L'"';

	return Compile(Expr);
	}

// Implementation

BOOL CCodedText::IsStringConst(CString Text) const
{
	if( Text[0] == L'"' ) {

		UINT uLen = Text.GetLength();

		if( uLen > 1 ) {

			if( Text[uLen-1] == L'"' ) {

				if( Text.Count(L'"') > 2 ) {

					for( UINT n = 1; n < uLen - 2; n++ ) {

						if( Text[n] == L'"' ) {

							if( Text[n-1] == '\\' ) {

								continue;
								}

							return FALSE;
							}
						}
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// End of File
