
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BANNER_HPP
	
#define	INCLUDE_BANNER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Driver Options
//

class CBannerCameraDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBannerCameraDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT	m_Type;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Device Options
//

class CBannerCameraDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBannerCameraDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Camera;
		UINT m_IP;
		UINT m_Port;
		UINT m_Time4;

		// Persistence
		void Load(CTreeFile &File);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Driver
//

class CBannerCameraDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CBannerCameraDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
