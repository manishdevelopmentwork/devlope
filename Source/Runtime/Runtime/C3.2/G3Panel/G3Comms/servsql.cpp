
#include "intern.hpp"

#include "lang.hpp"

#include "TdsPreLogin.hpp"
#include "TdsLogin.hpp"
#include "TdsResponse.hpp"
#include "TdsSQLBatch.hpp"
#include "TdsBulkLoad.hpp"
#include "TdsDataSet.hpp"

#include "FakeSocket.hpp"

#include "servsql.hpp"

#include "datalog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Configuration
//

// Toggle packet tracing.

#undef TRACE_PACKETS

// Time Constants

static UINT const timeSyncCheck		 = 500;		// ms

static UINT const timeSocketReadDelay	 = 20;		// ms

static UINT const timeSocketReadMaxWait	 = 5000;	// ms

static UINT const timeSocketSendDelay	 = 10;		// ms

static UINT const countSocketSendRetries = 100;

// Root Directory for Log Files.

#define	LOG_ROOT		PCTXT("\\LOGS\\")

// Name of file containing timestamp of last sent record.

#define	LAST_TIMEDATE_FILENAME	PCTXT("TIMEDATE.BIN")

// Maximum size supported for all string data values sent to the database.

#define	STRING_MAX_SIZE		256

// Internal variable size constants.

#define	MAX_TDS_PACKET_SIZE	8196

#define	FILE_NAME_STR_SIZE	128

#define COLUMN_NAME_STR_SIZE	128

#define DATE_TIME_STR_SIZE	32

#define MAX_DIGITS		12

#define LINES_PER_READ		1024

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Service
//

// Instantiator

IService * Create_ServiceSqlSync(void)
{
	return New CSqlSync;
	}

// Constructor

CSqlSync::CSqlSync(void)
{
	m_pEnable         = NULL;
	m_pDatabaseServer = NULL;
	m_pPort           = NULL;
	m_pTlsMode	  = NULL;
	m_pUser           = NULL;
	m_pPass           = NULL;
	m_pDatabaseName   = NULL;
	m_pTablePrefix    = NULL;
	m_pFreq           = NULL;
	m_pFreqMins       = NULL;
	m_pDelay          = NULL;
	m_pLogFile        = NULL;
	m_pForce	  = NULL;
	m_pCheckLabels    = NULL;
	m_pFireTriggers   = NULL;
	m_pDrive          = NULL;
	m_ColValues       = NULL;
	m_uMaxPacketSize  = MAX_TDS_PACKET_SIZE;
	m_fForce	  = FALSE;
	m_fHasSignature   = FALSE;
	m_uLastStarted    = 0;
	m_uLastSuccess    = 0;
	m_uLastFailure    = 0;
	m_uStatus         = sqlStatPending;
	m_fIsRunning      = FALSE;
	m_pLock           = Create_Semaphore(1);
	m_uPrimaryKey     = 0;

	g_pServiceSqlSync = this;
	}

// Destructor

CSqlSync::~CSqlSync(void)
{
	delete m_pEnable;
	delete m_pDatabaseServer;
	delete m_pPort;
	delete m_pTlsMode;
	delete m_pUser;
	delete m_pPass;
	delete m_pDatabaseName;
	delete m_pTablePrefix;
	delete m_pFreq;
	delete m_pFreqMins;
	delete m_pDelay;
	delete m_pLogFile;
	delete m_pForce;
	delete m_pCheckLabels;
	delete m_pFireTriggers;
	delete m_pDrive;

	m_pLock->Release();

	g_pServiceSqlSync = NULL;
	}

// Initialization

void CSqlSync::Load(PCBYTE &pData)
{
	ValidateLoad("CSqlSync", pData);

	GetCoded(pData, m_pEnable);
	GetCoded(pData, m_pDatabaseServer);
	GetCoded(pData, m_pPort);
	GetCoded(pData, m_pTlsMode);
	GetCoded(pData, m_pUser);
	GetCoded(pData, m_pPass);
	GetCoded(pData, m_pDatabaseName);
	GetCoded(pData, m_pTablePrefix);
	GetCoded(pData, m_pFreq);
	GetCoded(pData, m_pFreqMins);
	GetCoded(pData, m_pDelay);
	GetCoded(pData, m_pLogFile);
	GetCoded(pData, m_pForce);
	GetCoded(pData, m_pCheckLabels);
	GetCoded(pData, m_pFireTriggers);
	GetCoded(pData, m_pDrive);

	m_uPrimaryKey = GetByte(pData);

	UINT uCount   = GetLong(pData);

	for( UINT n = 0; n < uCount; n++ ) {

		CString Log = UniConvert(GetWide(pData));

		m_LogNames.Append(Log);
		}
	}

// Service ID

UINT CSqlSync::GetID(void)
{
	return 7;
	}

// Force Access

void CSqlSync::Force(void)
{
	m_fForce = TRUE;
	}

// User Accessible Status Info

BOOL CSqlSync::IsRunning(void)
{
	BOOL fRunning = FALSE;

	m_pLock->Wait(FOREVER);

	fRunning = m_fIsRunning;

	m_pLock->Signal(1);

	return fRunning;
	}

UINT CSqlSync::GetSyncTime(UINT uType)
{
	UINT uTime = 0;

	m_pLock->Wait(FOREVER);

	switch( uType ) {

		case 0:
			uTime = m_uLastStarted;

			break;
		case 1:
			uTime = m_uLastSuccess;

			break;
		case 2:
			uTime = m_uLastFailure;

			break;
		}

	m_pLock->Signal(1);

	return uTime;
	}

UINT CSqlSync::GetLastStatus(void)
{
	UINT uStatus = 0;

	m_pLock->Wait(FOREVER);

	uStatus = m_uStatus;

	m_pLock->Signal(1);

	return uStatus;
	}

// Task List

void CSqlSync::GetTaskList(CTaskList &List)
{
	m_Cfg.m_fEnable = GetItemData(m_pEnable, C3INT(0));

	if( m_Cfg.m_fEnable ) {

		CTaskDef Task;

		Task.m_Name   = "SQL";
		Task.m_pEntry = this;
		Task.m_uID    = 0;
		Task.m_uCount = 1;
 		Task.m_uLevel = 1500;
		Task.m_uStack = 8192;

		List.Append(Task);
		}
	}

// Task Entries

void CSqlSync::TaskInit(UINT uID)
{
	}

void CSqlSync::TaskExec(UINT uID)
{
	FindConfig(m_Cfg);

	TraceInfo     ("Enter Task Exec:\n");

	TraceParameter("FreqH", m_Cfg.m_uFreqH);

	TraceParameter("FreqM", m_Cfg.m_uFreqM);

	TraceParameter("Delay", m_Cfg.m_uDelay);

	BOOL fPrev = FALSE;

	for(;;) {

		if( m_Cfg.m_fForce ) {
			
			if( m_fForce ) {

				m_cSep = char(CCommsSystem::m_pThis->m_pLang->GetListSepChar());

				BOOL fOK = ProcessAllLogFiles();

				UpdateEndStatus(fOK);

				m_fForce = FALSE;
				}

			Sleep(timeSyncCheck);

			continue;
			}

		UINT uTime  = GetNow();

		UINT uFreq  = m_Cfg.m_uFreqM * 60 + m_Cfg.m_uFreqH * 60 * 60;

		UINT uDelay = m_Cfg.m_uDelay * 60;

		if( uFreq ) {

			BOOL fTest = (uTime % uFreq > uDelay);

			if( fTest && !fPrev ) {

				m_cSep = char(CCommsSystem::m_pThis->m_pLang->GetListSepChar());

				BOOL fOK = ProcessAllLogFiles();

				UpdateEndStatus(fOK);
				}

			fPrev = fTest;
			}

		Sleep(timeSyncCheck);
		}
	}

void CSqlSync::TaskStop(UINT uID)
{
	}

void CSqlSync::TaskTerm(UINT uID)
{
	}

// Socket Information

UINT CSqlSync::GetCmdPort(void)
{
	return m_Cfg.m_uPort;
	}

DWORD CSqlSync::GetServer(void)
{
	if( m_pDatabaseServer ) {

		if( !m_pDatabaseServer->IsConst() ) {

			m_Cfg.m_uDatabaseServer = GetItemAddr(m_pDatabaseServer, DWORD(0));
			}
		}
	return m_Cfg.m_uDatabaseServer;
	}

// Connection Information

CString& CSqlSync::GetUser(void)
{
	return m_Cfg.m_User;
	}

CString& CSqlSync::GetPassword(void)
{
	return m_Cfg.m_Pass;
	}

CString& CSqlSync::GetDatabaseName(void)
{
	return m_Cfg.m_DatabaseName;
	}

CString& CSqlSync::GetTablePrefix(void)
{
	return m_Cfg.m_TablePrefix;
	}

// Implementation

void CSqlSync::FindConfig(CConfig &Cfg)
{
	Cfg.m_uDatabaseServer = GetItemAddr(m_pDatabaseServer, DWORD(0));
	Cfg.m_uPort           = GetItemData(m_pPort,           C3INT(0));
	Cfg.m_uTlsMode	      = GetItemData(m_pTlsMode,        C3INT(0));
	Cfg.m_User            = GetItemData(m_pUser,           "sa");
	Cfg.m_Pass            = GetItemData(m_pPass,           "");
	Cfg.m_DatabaseName    = GetItemData(m_pDatabaseName,   "");
	Cfg.m_TablePrefix     = GetItemData(m_pTablePrefix,    "");
	Cfg.m_uFreqH          = GetItemData(m_pFreq,           C3INT(0));
	Cfg.m_uFreqM          = GetItemData(m_pFreqMins,       C3INT(0));
	Cfg.m_uDelay          = GetItemData(m_pDelay,          C3INT(0));
	Cfg.m_fLogFile        = GetItemData(m_pLogFile,        C3INT(0));
	Cfg.m_fForce	      = GetItemData(m_pForce,	       C3INT(0));
	Cfg.m_fCheckLabels    = GetItemData(m_pCheckLabels,    C3INT(0));
	Cfg.m_fFireTriggers   = GetItemData(m_pFireTriggers,   C3INT(0));
	Cfg.m_uDrive          = GetItemData(m_pDrive,          C3INT(0));

	m_LogFile = Cfg.m_fLogFile ? "\\SQL.LOG" : "";
	}

bool CSqlSync::ProcessAllLogFiles()
{
	UpdateStartStatus();

	// Open a New socket to the configured SQL Server instance.
	if (! OpenCmdSocket(TRUE, FALSE)) {
		TraceError("ProcessAllLogFiles", "Error Opening Socket.");
		return false;
		}

	if (! ConnectToSql()) {
		TraceError("ProcessAllLogFiles", "Error Connecting to SQL.");
		return false;
		}


	char cDrive = 'C' + m_Cfg.m_uDrive;

	CPrintf LogRoot("%c:%s", cDrive, LOG_ROOT);

	// Change to the root directory for the log files.
	if (chdir(LogRoot)) {
		TraceError("ProcessAllLogFiles", "Error Changing Directory.");
		CloseCmdSocket();
		return false;
		}

	// Obtain list of all log directories (1 directory per log).

	CAutoDirentList Dirs;

	if( Dirs.ScanDirs(".") ) {

		// Process files in each log directory.

		for( UINT n = 0; n < Dirs.GetCount(); n++ ) {

			struct dirent *d = Dirs[n];

			// get the name of the directory.
			
			m_CurrentPathName = d->d_name;

			// Skip log directories that aren't included by the config.
			
			if( m_LogNames.Find(d->d_name) == NOTHING ) {
				continue;
				}

			// compose full path to the target directory.
			CString clsFullPath(LogRoot);
			clsFullPath += d->d_name;
			clsFullPath += "\\";

			// trace
			CString clsPathTrace = "Path = ";
			clsPathTrace += clsFullPath;
			clsPathTrace += "\n";
			TraceInfo("\n");
			TraceInfo("\n");
			TraceInfo(PCTXT(clsPathTrace));

			// change to the target directory.
			if (chdir(PCTXT(clsFullPath))) {
				TraceError("ProcessAllLogFiles", "Error Changing Directory.");
				return false;
				}

			// form database table name that will hold the data.
			CString  szTableName = GetTablePrefix();
			if (szTableName[0] != '\0')
				szTableName += "_";
			szTableName += d->d_name;

			// get time of last log entry that was processed.
			ReadLastTimeDateProcessed(m_LastDateSent, m_LastTimeSent);
			DWORD	uTimeOfLastRow = ParseTimeDate(m_LastDateSent, m_LastTimeSent);
			TraceTimeDate("Last Time Processed", uTimeOfLastRow);

			// get list of all CSV files in target directory that may contain log entries
			//	added after the last time already processed.
			CAutoDirentList Files;

			Files.Scan(".", CsvSelect, CAutoDirentList::StdSort);

			// process each of those files.

			bool use = false;

			for (UINT uFileIndex=0; uFileIndex < Files.GetCount(); uFileIndex++) {

				struct dirent *d = Files[uFileIndex];

				char s[MAX_PATH];

				PathMakeAbsolute(s, d->d_name);

				if( !use ) {
					
					CAutoFile File(s, "r");

					if( !File ) {

						continue;
						}
						
					DWORD time = GetFirstTimeDateInFile(File);
						
					if( time <= uTimeOfLastRow ) {

						continue;
						}

					use = true;
					}

				clsPathTrace = "   File = ";
				clsPathTrace += s;
				clsPathTrace += "\n";
				TraceInfo("\n");
				TraceInfo(PCTXT(clsPathTrace));

				ProcessFile(s, uTimeOfLastRow, szTableName);
				}
			}
		}

	// Close the socket.
	CloseCmdSocket();

	return true;
	}

bool CSqlSync::ProcessFile(PCSTR szFileName, DWORD uLastTime, PCSTR szTableName)
{
	CAutoFile File(szFileName, "r");

	if( !File ) {

		TraceFileError("ProcessFile", "Error Opening File", szFileName);

		return false;
		}
		
	// Read the column names.
	ReadColumnNames(File);

	TraceColumns();

	// Issue SQL command to verify database table exists and create it if not.
	CString*  pTheTableCommand = FormTableCreationSQLCommand(szTableName);
	TraceSqlCommand("Table Command", PCTXT(*pTheTableCommand));

	CTdsResponse	clsTableCreateResp;

	if (SendSqlBatchCommandsAndGetReply(clsTableCreateResp, PCTXT(*pTheTableCommand), "Create Table SQL Batch Packet")) {

		if (clsTableCreateResp.HaveErrorFromServer()) {
			
			TracePacketErrors(clsTableCreateResp);

			m_ColumnInfo.Empty();
			return false;
			}
		}
	else {
		m_ColumnInfo.Empty();

		return false;
		}

	DumpPacket("Create Table Response Packet", clsTableCreateResp);

	delete pTheTableCommand;

	SetPrimaryKey(szTableName);

	UINT uRows = 0;

	do {
		uRows = ReadColumnValues(File, false, true, uLastTime);

		LogProgress(File);

		} while (uRows != 0 );	

	// Form the SQL command string for the insert bulk command (it is the same for each packet).
	CString*  pTheInsertCommand = FormInsertBulkSQLCommand(File, szTableName);
	TraceSqlCommand("Insert Command", PCTXT(*pTheInsertCommand));

	bool fContinue = true;

	// Send to the database all remaining rows in the file.
	bool fNoError = true;
	while( fNoError && fContinue ) {

		CTdsSqlBatch	clsInsertCmd(m_uMaxPacketSize);
		CString		szFirstDate, szFirstTime;
		CString		szLastDate,  szLastTime;

		bool fRetry = false;
		do {
			fNoError = fRetry = false;
			CTdsResponse	clsInsertResp;

			// send insert bulk.
			TraceInfo("   Sending insert bulk\n");
			if (SendSqlBatchCommandsAndGetReply(clsInsertResp, PCTXT(*pTheInsertCommand), "Insert Bulk SQL Batch Packet")) {

				DumpPacket("Insert Bulk Response Packet", clsInsertResp);

				if (clsInsertResp.HaveErrorFromServer()) {

					TraceError("ProcessFile", "INSERT failed -- does table match log structure?");

					TracePacketErrors(clsInsertResp);
					}
				else {

					TraceInfo("   Preparing bulk load packet\n");
	
					CTdsResponse	clsBulkLoadResp;

					// send bulk load packet.
					
					if (FormAndSendBulkLoadPackets(File, szFirstDate, szFirstTime, szLastDate, szLastTime, clsBulkLoadResp)) {

						fContinue = false;

						DumpPacket("Bulk Load Response Packet", clsBulkLoadResp);

						if (clsBulkLoadResp.HaveErrorFromServer()) {
							
							TracePacketErrors(clsBulkLoadResp);
							
							USHORT uIndex = 0;
							if (clsBulkLoadResp.GetErrorCode(uIndex) ==  2627) {	// Handle duplicate key violation.

								// form delete command to remove all records within time span of bulk load packet.
								CString*  pTheDeleteCommand = FormDeleteSQLCommand(szTableName, szFirstDate, szFirstTime, szLastDate, szLastTime);
								TraceSqlCommand("Delete Command", PCTXT(*pTheDeleteCommand));

								CTdsSqlBatch	clsDeleteCmd(m_uMaxPacketSize);
								CTdsResponse	clsDeleteResp;

								// send delete.
								if (SendSqlBatchCommandsAndGetReply(clsDeleteResp, PCTXT(*pTheDeleteCommand), "Delete SQL Batch Packet")) {

									DumpPacket("Delete Response Packet", clsDeleteResp);

									if (clsDeleteResp.HaveErrorFromServer()) {

										TracePacketErrors(clsDeleteResp);

										}
									else
										fNoError = fRetry = true;

									}

								delete pTheDeleteCommand;

								}
							}
						else {

							m_LastDateSent = szLastDate;
							m_LastTimeSent = szLastTime;
							fNoError = true;

							}

						}
					}
				}

			} while (fRetry);

		if( !m_LastDateSent.IsEmpty() && !m_LastTimeSent.IsEmpty() ) {

			WriteLastTimeDateProcessed(m_LastDateSent, m_LastTimeSent);
			}
		}

	// Release the insert bulk SQL string.
	delete pTheInsertCommand;

	// Clear the internal values array.

	for( UINT n = 0; n < LINES_PER_READ; n++ ) {

		delete [] m_ColValues[n];
		}

	delete [] m_ColValues;
	m_ColValues = NULL;

	// Clear the column names array.
	m_ColumnInfo.Empty();

	return true;
	}

void CSqlSync::UpdateStartStatus(void)
{
	m_pLock->Wait(FOREVER);

	m_fIsRunning	= TRUE;

	m_uLastStarted	= GetNow();

	m_uStatus	= sqlStatPending;

	m_pLock->Signal(1);
	}

void CSqlSync::UpdateEndStatus(BOOL fSuccess)
{
	m_pLock->Wait(FOREVER);

	if( fSuccess ) {

		m_uStatus = sqlStatSuccess;

		m_uLastSuccess = GetNow();
		}
	else {
		m_uStatus = sqlStatFailure;

		m_uLastFailure = GetNow();
		}

	m_fIsRunning = FALSE;

	m_pLock->Signal(1);
	}

UINT CSqlSync::FindMaxLine(void)
{
	UINT uCount = m_ColumnInfo.GetCount();

	UINT uMax   = uCount;

	if( m_fHasSignature ) {

		uMax += 32;
		}

	for( UINT n = 0; n < uCount; n++ ) {

		if( m_ColumnInfo[n].m_eSQLDataType == typeBigVarChrType ) {

			uMax += STRING_MAX_SIZE;
			}
		else {
			uMax += 32;
			}
		}

	uMax = (uMax / 512) + 1;

	return uMax * 512;
	}

CTdsBytes* CSqlSync::MakeCommandBytes(CTdsBytes& bCommand, PCSTR pCommand)
{
	UINT uLength = strlen(pCommand);

	// Convert string to unicode bytes by inserting zeroes:
	for(int i=0; i<uLength; i++) {

		bCommand.AddByte(pCommand[i]);

		bCommand.AddByte(0);
		}
	return &bCommand;
	}

// Socket Commands

bool CSqlSync::ConnectToSql(void)
{
	CTdsPreLogin SendMsg, RecvMsg;

	SendMsg.Create("", 0x12345678, m_Cfg.m_uTlsMode > 0);

	if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

		BYTE bEncrypt = RecvMsg.GetEncryption();

		if( bEncrypt == 1 || bEncrypt == 3 ) {

			if( !SwitchToTls() ) {

				return false;
				}

			TraceInfo("Switched to TLS\n");
			}
		else {
			if( m_Cfg.m_uTlsMode == 2 ) {

				TraceError("ConnectToSql", "TLS declined by is required");

				CloseCmdSocket();

				return false;
				}
			}

		CTdsLogin    SendMsg;

		CTdsResponse RecvMsg;

		SendMsg.Create( "Crimson3Host",
				GetUser(),
				GetPassword(),
				"Crimson3",
				GetDatabaseName(),
				m_uMaxPacketSize
				);

		if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

			if( !RecvMsg.HaveErrorFromServer() ) {

				if( RecvMsg.GetPacketSizeValue() < m_uMaxPacketSize ) {

					m_uMaxPacketSize = RecvMsg.GetPacketSizeValue();

					TraceParameter("Max TDS Packet Size", m_uMaxPacketSize);
					}
			
				return true;
				}

			TracePacketErrors(RecvMsg);
			}
		}
	
	CloseCmdSocket();
	
	return false;
	}

bool CSqlSync::SwitchToTls(void)
{
	// TDS is a little strange in that the initial TLS negotiation is
	// carried out by embedding the handshake messages in TDS prelogin
	// packets. To handle this, we create a fake child socket that will
	// capture the TLS transmissions and let us feed respones back. Once
	// the TLS socket is happy, we switch its to use our TDS socket as
	// its child socket and let everything run in TLS from there.

	if( CreateTlsContext() ) {

		CFakeSocket *pFake = New CFakeSocket;

		ISocket	    *pSock = m_pTls->CreateSocket(pFake, NULL, 0);

		pSock->Connect(CIPAddr(), 0);

		for(;;) {

			UINT Phase = 0;

			for( UINT n = 0; n < 10; n++ ) {

				pSock->GetPhase(Phase);
				}

			if( Phase == PHASE_OPEN ) {

				pSock->SetOption(OPT_SET_CHILD, UINT(m_pCmdSock));

				pFake->Release();

				m_pCmdSock = pSock;

				return true;
				}

			if( Phase == PHASE_OPENING ) {

				PCBYTE pData;

				UINT   uData;

				// We should have data to send, so grab it.

				if( pFake->GetTxData(pData, uData) ) {

					/*AfxTrace("SEND:\n");

					AfxDump(pData, uData);

					AfxTrace("\n");*/

					// Create a prelogin packet with a payload.

					CTdsPreLogin SendMsg, RecvMsg;

					SendMsg.AddPayload(pData, uData);

					SendMsg.EndPacket();

					// Exchange packets with the server.

					if( SendCommandAndGetReply(SendMsg, RecvMsg) ) {

						// Find the reply payload contents.

						pData = RecvMsg.GetPayloadData();

						uData = RecvMsg.GetPayloadSize();

						if( uData ) {

							// And pass them on to the TLS socket.

							/*AfxTrace("RECV:\n");

							AfxDump(pData, uData);

							AfxTrace("\n");*/

							pFake->SetRxData(pData, uData);

							continue;
							}
						}
					}
				}

			break;
			}

		// This will also release pFake.

		pSock->Release();
		}

	return false;
	}

bool CSqlSync::SendSqlBatchCommandsAndGetReply(CTdsPacket& clsReceivedPacket, PCSTR pCommand, PCSTR sDumpTitle)
{
	// Create data portion of command:
	UINT uLength = strlen(pCommand);

	CTdsBytes bCommand(uLength * 2);

	MakeCommandBytes(bCommand, pCommand);

	// Split command into maximum sized packets:
	UINT uIndex = 0;

	while(uIndex != NOTHING) {

		CTdsSqlBatch clsPacket(m_uMaxPacketSize);

		// Append data to fill packet and return stopping point:
		uIndex = clsPacket.Append(bCommand, uIndex, m_uMaxPacketSize);

		DumpPacket(sDumpTitle, clsPacket);

		// Get reply when final packet is sent:
		if(uIndex != NOTHING) {

			SendCommand(clsPacket);
			}
		else {
			
			return SendCommandAndGetReply(clsPacket, clsReceivedPacket);
			}
		}
	return false;
	}

bool CSqlSync::SendCommand(CTdsPacket& clsSentPacket)
{
	// Send the first TDS packet.

	PBYTE pData = clsSentPacket.GetDataMod();

	UINT  uSize = clsSentPacket.GetSize();
	
	UINT  uTime = timeSocketSendDelay * countSocketSendRetries;

	SetTimer(uTime);

	while( GetTimer() ) {

		UINT Phase;

		m_pCmdSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			UINT uCopy = uSize;

			if( m_pCmdSock->Send(pData, uCopy) == S_OK ) {

				if( (uSize -= uCopy) ) {
	
					pData += uCopy;

					SetTimer(uTime);

					continue;
					}

				return true;
				}

			Sleep(timeSocketSendDelay);

			continue;
			}

		break;
		}

	TraceError("SendCommand", "Error Sending Packet.");

	return false;
	}

bool CSqlSync::SendCommandAndGetReply(CTdsPacket& clsSentPacket, CTdsPacket& clsReceivedPacket)
{
	if( SendCommand(clsSentPacket) ) {

		UINT uTime = timeSocketReadMaxWait;

		SetTimer(uTime);

		while( GetTimer() ) {

			UINT Phase;

			m_pCmdSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				CBuffer *pBuff;

				if( m_pCmdSock->Recv(pBuff) == S_OK ) {

					int n = clsReceivedPacket.Parse(pBuff->GetData(), pBuff->GetSize());

					if( n > 0 ) {

						pBuff->Release();

						return true;
						}

					if( n < 0 ) {

						TraceError("SendCommandAndGetReply", "Error Parsing Response Packet.");

						pBuff->Release();

						return false;
						}

					continue;
					}

				Sleep(timeSocketReadDelay);

				continue;
				}

			break;
			}

		TraceError("SendCommandAndGetReply", "Error Receiving Packet.");
		}

	return false;
	}

// Tracing

void CSqlSync::DumpPacket(PCSTR szDescriptor, CTdsPacket &clsPacket)
{

#ifdef	TRACE_PACKETS

	AfxTrace("\n");
	AfxTrace("%s:\n", szDescriptor);
	AfxTrace("\n");
	clsPacket.Dump();
	AfxTrace("\n");
	clsPacket.DumpASCII();
	AfxTrace("\n");

#endif

	}


void CSqlSync::TraceInfo(PCSTR szMessage)
{

	Log("%s", szMessage);

	}


void CSqlSync::TraceError(PCSTR szMethod, PCSTR szMessage)
{

	Log("ERROR: [%s] %s\n", szMethod, szMessage);

	}


void CSqlSync::TraceFileError(PCSTR szMethod, PCSTR szMessage, PCSTR szFileName)
{

	Log("ERROR: [%s] %s, File = '%s'\n", szMethod, szMessage, szFileName);

	}


void CSqlSync::TracePacketErrors(CTdsResponse &clsResponsePacket)
{
	for (USHORT uIndex = 0; uIndex < clsResponsePacket.GetNumErrMessages(); uIndex++) {
		
		Log("PACKET ERROR [%ld]:\n", clsResponsePacket.GetErrorCode(uIndex));

		CString ErrorMessage = CString(clsResponsePacket.GetErrorMessage(uIndex));

		while(strlen(ErrorMessage) > 0) {

			Log("  %s\n", PCTXT(ErrorMessage.Left(53)));

			ErrorMessage.Delete(0, 53);
			}
		}
	}


void CSqlSync::TraceParameter(PCSTR szParamName, int iValue)
{

	Log ("   Param '%s' = %d\n", szParamName, iValue);

	}


void CSqlSync::TraceTimeDate(PCSTR szDescriptor, DWORD uTimeDate)
{
	Log( "   Time '%s' = %2.2u/%2.2u/%2.2u %2.2u:%2.2u:%2.2u \n",
		  szDescriptor,
		  GetMonth(uTimeDate),
		  GetDate (uTimeDate),
		  GetYear (uTimeDate) % 100,
		  GetHour (uTimeDate),
		  GetMin  (uTimeDate),
		  GetSec  (uTimeDate)
		  );

	}


void CSqlSync::TraceColumns()
{
	for (UINT uIndex = 0; uIndex < m_ColumnInfo.GetCount(); uIndex++)
		Log("   Column = %s  Type = %x  Size = %d\n", PCTXT(m_ColumnInfo[uIndex].m_ColumnName), (int)m_ColumnInfo[uIndex].m_eSQLDataType, (int)m_ColumnInfo[uIndex].m_wSQLDataSize);
	}


void CSqlSync::TraceSqlCommand(PCSTR szCommandName, PCSTR szCommand)
{

	Log("   %s = \n", szCommandName);

	int i = 0;

	while (szCommand[i] != '\0') {

		char szNextMsg[129];
		int  j = 0;

		while ((j < 128) && (szCommand[i] != '\0')) {

			szNextMsg[j++] = szCommand[i++];

			}

		szNextMsg[j] = '\0';

		Log("      %s\n", szNextMsg);

		}

	}

void CSqlSync::LogProgress(CAutoFile &File)
{
	double progress = 100.0 * ((double) File.GetPos() / File.GetSize());

	UINT uPercent   = (UINT) progress;

	Log("   %u%% of file processed\n", uPercent);
	}

// File and File System Access

BOOL CSqlSync::ReadNextItem(CAutoFile &File, PSTR szItem, BOOL fSkip, BOOL fAllowComma)
{
	bool fQuoted = false;
	
	while( File.Read((unsigned char *)szItem, 1) ) {
		
		if( *szItem == '"' ) {

			fQuoted = !fQuoted;

			continue;
			}

		else if( *szItem == m_cSep ) {

			if( !fQuoted ) {
				
				*szItem = '\0';

				return false;
				}

			if( !fAllowComma ) {

				szItem--;
				}
			}

		else if( *szItem == '\r' ) {

			*szItem = '\0';
			}

		else if( *szItem == '\n' ) {

			*szItem = '\0';
			
			return true; 
			}

		if( !fSkip ) {
			
			szItem++;
			}
		}

	*szItem = '\0';

	return true;
	}

BOOL CSqlSync::ReadNextItem(PBYTE pData, UINT uSize, UINT &uPos, PSTR szItem, BOOL fSkip, BOOL fAllowComma)
{
	bool fQuoted = false;
	
	while( uPos < uSize ) {

		*szItem = pData[uPos];

		uPos++;

		if( *szItem == '"' ) {

			fQuoted = !fQuoted;

			continue;
			}

		else if( *szItem == m_cSep ) {

			if( !fQuoted ) {

				*szItem = '\0';

				return false;
				}

			if( !fAllowComma ) {

				szItem--;
				}
			}

		else if( *szItem == '\r' ) {

			*szItem = '\0';
			}

		else if( *szItem == '\n' ) {

			*szItem = '\0';

			return true; 
			}

		if( !fSkip ) {
			
			szItem++;
			}
		}

	*szItem = '\0';

	return true;
	}

BOOL CSqlSync::ReadNextLine(CAutoFile &File, PSTR szLine)
{
	while (File.Read((unsigned char *)szLine, 1)) {
		if (*szLine == '\r')
			*szLine = '\0';
		else if (*szLine == '\n') {
			*szLine = '\0';
			return true; 
			}

		szLine++;
		}

	*szLine = '\0';
	return true;
	}

void CSqlSync::ReplaceInvalidSQLChars(PSTR szValue)
{
	UINT uCount = strlen(szValue);
	
	UINT uIndex;

	for (uIndex=0; uIndex < uCount; uIndex++)
		if ((szValue[uIndex] == '[') || (szValue[uIndex] == ']'))
			szValue[uIndex] = '_';
		else if((szValue[uIndex] == '.') && !m_Cfg.m_fCheckLabels)
			szValue[uIndex] = '_';
	if (szValue[uIndex - 1] == '_')
		szValue[uIndex - 1] = '\0';
	}

void CSqlSync::CreateTypeLookupName(PSTR szColumnName, PSTR szColumnNameTypeLookup)
{
	UINT uLength = strlen(szColumnName);

	for (UINT i = 0; i <= uLength; i++) {

		szColumnNameTypeLookup[i] = szColumnName[i];

		if(szColumnName[i] == '[') {

			if(LooksLikeArray(szColumnName) && m_Cfg.m_fCheckLabels) {

				szColumnNameTypeLookup[i+1] = '0';
				szColumnNameTypeLookup[i+2] = ']';
				szColumnNameTypeLookup[i+3] = '\0';
				break;
				}
			}
		}
	}

BOOL CSqlSync::LooksLikeArray(PSTR szValue)
{
	UINT uCount = strlen(szValue);

	if(szValue[uCount-1] == ']'){

		for(UINT i = uCount-2; uCount >=0; i--) {

			if(szValue[i] == '[')
				return true;
			else if(!isdigit(szValue[i]))
				return false;
			}
		}
	return false;
	}

void CSqlSync::ReadColumnNames(CAutoFile &File)
{
	CTagList* pTheTagList = CCommsSystem::m_pThis->m_pTags->m_pTags;

	char			szColumnName[COLUMN_NAME_STR_SIZE + 1];
	char			szColumnNameTypeLookup[COLUMN_NAME_STR_SIZE + 1];
	WCHAR			szWideColumnName[COLUMN_NAME_STR_SIZE + 1];

	CColumnInfo		clsNextColumn;
	BOOL			fIsLastColumn = false;
	CDataLog		*pTheDataLog  = NULL;

	// Read date and time columns.  These become 1 single column.
	ReadNextItem(File, szColumnName, false, false);
	ReadNextItem(File, szColumnName, false, false);
	clsNextColumn.m_ColumnName = "DateTime";
	clsNextColumn.m_eSQLDataType = typeDateTimNType;
	clsNextColumn.m_wSQLDataSize = sizeof(ULONGLONG);
	m_ColumnInfo.Append(clsNextColumn);

	// Find relevant datalog for find by label.
	if(m_Cfg.m_fCheckLabels) {

		CDataLogList *pLogList = CCommsSystem::m_pThis->m_pLog->m_pLogs;

		for(UINT i = 0; i < pLogList->m_uCount; i++) {

			CDataLog *pDataLog = pLogList->m_ppLog[i];

			if(pDataLog->m_Path == m_CurrentPathName) {

				pTheDataLog = pDataLog;
				break;
				}
			}
		}

	// Read remaining data columns.
	do {
		fIsLastColumn = ReadNextItem(File, szColumnName, false, false);
		
		CreateTypeLookupName(szColumnName, szColumnNameTypeLookup);

		ReplaceInvalidSQLChars(szColumnName);

		clsNextColumn.m_ColumnName = szColumnName;

		for (UINT i = 0; i <= strlen(szColumnName); i++)
			szWideColumnName[i] = szColumnName[i];

		UINT uTagIndex = NOTHING;

		if(m_Cfg.m_fCheckLabels) {

			if(pTheDataLog) {

				for(UINT i = 0; i < pTheDataLog->GetChanCount(); i++) {
					
					CTag *pTag    = pTheDataLog->GetChanItem(i);
					CString Label = UniConvert(pTag->GetLabel(0));
				
					if(Label == szColumnNameTypeLookup) {

						uTagIndex = i;
						break;
						}
					}
				}
			}
		else {
			uTagIndex = pTheTagList->FindByName(szWideColumnName);
		}

		if(uTagIndex != NOTHING) {

			BYTE bFormatType;

			UINT uType = NOTHING;

			if(m_Cfg.m_fCheckLabels) {

				uType       = pTheDataLog->GetChanItem(uTagIndex)->GetDataType();
				bFormatType = pTheDataLog->GetChanItem(uTagIndex)->m_FormType;
				}
			else {
				uType       = pTheTagList->GetItem(uTagIndex)->GetDataType();
				bFormatType = pTheTagList->GetItem(uTagIndex)->m_FormType;
				}

			switch (uType) {

				case typeInteger:
					if (bFormatType <= 2) {
						clsNextColumn.m_eSQLDataType = typeIntNType;
						clsNextColumn.m_wSQLDataSize = sizeof(C3INT);
						}
					else {
						clsNextColumn.m_eSQLDataType = typeBigVarChrType;
						clsNextColumn.m_wSQLDataSize = STRING_MAX_SIZE;
						}
					break;

				case typeReal:
					if (bFormatType <= 2) {
						clsNextColumn.m_eSQLDataType = typeFltNType;
						clsNextColumn.m_wSQLDataSize = sizeof(C3REAL);
						}
					else {
						clsNextColumn.m_eSQLDataType = typeBigVarChrType;
						clsNextColumn.m_wSQLDataSize = STRING_MAX_SIZE;
						}
					break;
				
				case typeLogical:
					clsNextColumn.m_eSQLDataType = typeBigVarChrType;
					clsNextColumn.m_wSQLDataSize = STRING_MAX_SIZE;
					break;

				default:
					clsNextColumn.m_eSQLDataType = typeBigVarChrType;
					clsNextColumn.m_wSQLDataSize = STRING_MAX_SIZE;
				}
			}
		else {
			clsNextColumn.m_eSQLDataType = typeBigVarChrType;
			clsNextColumn.m_wSQLDataSize = STRING_MAX_SIZE;
			}

		m_ColumnInfo.Append(clsNextColumn);

		} while (! fIsLastColumn);

	if(m_ColumnInfo[m_ColumnInfo.GetCount()-1].m_ColumnName == "Signature") {

		m_fHasSignature = TRUE;

		m_ColumnInfo.Remove(m_ColumnInfo.GetCount()-1);
		}
	}


UINT CSqlSync::ReadColumnValues(CAutoFile &File, bool fOne, bool fTime, UINT uSearchTime)
{	
	UINT  uTarget = fOne ? 1 : LINES_PER_READ;

	UINT  uRows = 0;

	UINT  uMax  = FindMaxLine();

	UINT  uLine = uTarget * uMax;

	PBYTE pBuff = New BYTE[ uLine ];
	
	UINT  uRead = File.Read(pBuff, uLine);

	if( uRead == 0 || uRead == NOTHING ) {

		delete [] pBuff;

		return 0;
		}

	char szValue[STRING_MAX_SIZE + 1];

	if( m_ColValues == NULL ) {
		
		m_ColValues = New CColumnValues * [LINES_PER_READ];

		for( UINT i = 0; i < LINES_PER_READ; i++ ) {

			m_ColValues[i] = New CColumnValues[m_ColumnInfo.GetCount()];
			}
		}

	UINT uPos = 0;

	for( UINT n = 0; n < uTarget; n++ ) {

		UINT uItemStart = uPos;

		// Read date. If no more rows, then exit.
		if( ReadNextItem(pBuff, uRead, uPos, szValue, false, false) ) {

			delete [] pBuff;

			return uRows;
			}

		m_ColDate = szValue;
		m_ColValues[n][0].m_Date = szValue;

		// Read time column.
		ReadNextItem(pBuff, uRead, uPos, szValue, false, false);
		m_ColTime = szValue;
		m_ColValues[n][0].m_Time = szValue;
			
		if( fTime ) {
			
			UINT uTime = ParseTimeDate(m_ColDate, m_ColTime);
						
			if( uTime > uSearchTime ) {
				
				int iAdj = int(uItemStart - uRead);
		
				File.SeekFromHere(iAdj);

				delete [] pBuff;

				return 0;
				}
			}

		// Add datetime column.
		m_ColValues[n][0].m_Value = m_ColDate + " " + m_ColTime;

		// Read remaining columns from file.
		for( UINT uCol = 1; uCol < m_ColumnInfo.GetCount(); uCol++ ) {

			BOOL fString = m_ColumnInfo[uCol].m_eSQLDataType == typeBigVarChrType;
		
			ReadNextItem(pBuff, uRead, uPos, szValue, false, fString);

			m_ColValues[n][uCol].m_Value = szValue;

			if (m_ColumnInfo[uCol].m_eSQLDataType == typeBigVarChrType) {

				m_ColValues[n][uCol].m_Size = sizeof(USHORT) +  strlen(szValue);
				}
			else {
				m_ColValues[n][uCol].m_Size = sizeof(BYTE) + m_ColumnInfo[uCol].m_wSQLDataSize;
				}
			}

		if( m_fHasSignature ) {

			ReadNextItem(pBuff, uRead, uPos, szValue, true, false);
			}

		uRows++;
		}

	int iAdj = int(uPos - uRead);
		
	File.SeekFromHere(iAdj);

	delete [] pBuff;

	return uRows;
	}

DWORD CSqlSync::GetFirstTimeDateInFile(CAutoFile &File)
{
	// Read the column names.
	char	szColumnName[COLUMN_NAME_STR_SIZE + 1];
	while (! ReadNextItem(File, szColumnName, false, false));

	// Read the date and time.
	char	szDate[DATE_TIME_STR_SIZE], szTime[DATE_TIME_STR_SIZE];
	ReadNextItem(File, szDate, false, false);
	ReadNextItem(File, szTime, false, false);

	// Return the internal datetime value.
	return ParseTimeDate(szDate, szTime);
	}

void CSqlSync::WriteLastTimeDateProcessed(CString &szLastDate, CString &szLastTime)
{
	CAutoFile File(LAST_TIMEDATE_FILENAME, "r+", "w+");

	if( !File ) {

		TraceFileError("WriteLastTimeDateProcessed", "Error Creating File", LAST_TIMEDATE_FILENAME);

		return;
		}

	// Write the date and time.

	File.Write(PBYTE(PCTXT(szLastDate)), szLastDate.GetLength());

	File.Write(PBYTE(" "), 1);

	File.Write(PBYTE(PCTXT(szLastTime)), szLastTime.GetLength());
	}

void CSqlSync::ReadLastTimeDateProcessed(CString &szLastDate, CString &szLastTime)
{
	CAutoFile File(LAST_TIMEDATE_FILENAME, "r");

	if( !File ) {

		szLastDate = "1997/01/01";

		szLastTime = "00:00:00";

		return;
		}

	char szNextValue[DATE_TIME_STR_SIZE];

	File.Read(PBYTE(szNextValue), 11);
	szNextValue[10] = '\0';
	szLastDate = szNextValue;

	File.Read(PBYTE(szNextValue), 8);
	szNextValue[8] = '\0';
	szLastTime = szNextValue;
	}

// CSV Selection

int CSqlSync::CsvSelect(struct dirent const *p)
{
	if( p->d_type == DT_REG ) {

		size_t n = strlen(p->d_name);

		if( n >= 4 ) {
			
			if( !stricmp(p->d_name + n - 4, ".csv") ) {

				char s[MAX_PATH];

				PathMakeAbsolute(s, p->d_name);

				return 1;
				}
			}
		}

	return 0;
	}

// SQL Command Creation

void CSqlSync::FormSQLColumnList(CString* szTheCommand, bool fAddPrimaryKey)
{
	*szTheCommand += " (";

	for (UINT uIndex = 0; uIndex < m_ColumnInfo.GetCount(); uIndex++) {

		if (uIndex)
			*szTheCommand += ", ";

		*szTheCommand += "[";
		*szTheCommand += m_ColumnInfo[uIndex].m_ColumnName;
		*szTheCommand += "] ";

		switch (m_ColumnInfo[uIndex].m_eSQLDataType) {

			case typeIntNType:
				switch (m_ColumnInfo[uIndex].m_wSQLDataSize) {
					case 1:
						*szTheCommand += "tinyint";
						break;

					case 2:
						*szTheCommand += "smallint";
						break;

					case 4:
						*szTheCommand += "int";
						break;

					case 8:
						*szTheCommand += "bigint";
						break;
					}
				break;

			case typeFltNType:
				switch (m_ColumnInfo[uIndex].m_wSQLDataSize) {
					case 4:
						*szTheCommand += "real";
						break;

					case 8:
						*szTheCommand += "float";
						break;
					}
				break;


			case typeDateTimNType:
				*szTheCommand += "datetime";
				break;

			case typeBigVarChrType:
				char szSizeStr[MAX_DIGITS + 1];
				sprintf (szSizeStr, "%d", (int)m_ColumnInfo[uIndex].m_wSQLDataSize);
				*szTheCommand += "varchar(";
				*szTheCommand += szSizeStr;
				*szTheCommand += ")";
				break;

			}


		if (uIndex == 0) {
			if (fAddPrimaryKey)
				*szTheCommand += " PRIMARY KEY";
			else
				*szTheCommand += " NOT NULL";
			}
		}

	*szTheCommand += ")";

	}


CString* CSqlSync::FormTableCreationSQLCommand(PCSTR szTableName)
{
	CString*	szTheCommand = New CString();

	*szTheCommand = "if (not exists (select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = '";
	*szTheCommand += szTableName;
	*szTheCommand += "')) begin";
	*szTheCommand += " create table [";
	*szTheCommand += szTableName;
	*szTheCommand += "]";
	FormSQLColumnList(szTheCommand, FALSE);
	*szTheCommand += "; end";

	return szTheCommand;

	}


CString* CSqlSync::FormInsertSQLCommand(CAutoFile &File, PCSTR szTableName, PCSTR szDate, PCSTR szTime)
{
	CString*	szTheCommand = New CString();

	*szTheCommand = "insert into [";
	*szTheCommand += szTableName;
	*szTheCommand += "] values (";

	char	szNextLine[2048];
	ReadNextLine(File, szNextLine);

	*szTheCommand += "'";
	*szTheCommand += szDate;
	*szTheCommand += "', '";
	*szTheCommand += szTime;
	*szTheCommand += "', ";
	*szTheCommand += szNextLine;

	*szTheCommand += ");";

	return szTheCommand;

	}


CString* CSqlSync::FormInsertBulkSQLCommand(CAutoFile &File, PCSTR szTableName)
{
	CString* szTheCommand = New CString();

	*szTheCommand = "insert bulk [";
	*szTheCommand += szTableName;
	*szTheCommand += "]";
	FormSQLColumnList(szTheCommand, FALSE);

	if(m_Cfg.m_fFireTriggers) {

		*szTheCommand += " with (FIRE_TRIGGERS)";
		}

	* szTheCommand += ";";

	return szTheCommand;
	}

bool CSqlSync::ExecuteQuery(CString const &Query, CTdsPacket &Response)
{	
	if( SendSqlBatchCommandsAndGetReply(Response, PCSTR(Query), "Exec Query") ) {

		return true;
		}

	return false;
	}

// Primary Key Operations

bool CSqlSync::SetPrimaryKey(CString const &Table)
{
	if( !HasPrimaryKey(Table) ) {

		if( m_uPrimaryKey == sqlKeyAutoInc ) {

			SetAutoIncrementKey(Table);
			}
		else {
			SetDatetimeKey(Table);
			}
		}
	else {
		UINT uKeyType = GetPrimaryKeyType(Table);

		if( uKeyType != m_uPrimaryKey ) {

			switch( m_uPrimaryKey ) {

				case sqlKeyDatetime:

					RemoveIdentKey(Table);

					SetDatetimeKey(Table);

					break;

				case sqlKeyAutoInc:

					RemoveDatetimeKey(Table);

					SetAutoIncrementKey(Table);

					break;			
				}
			}
		}

	return true;
	}

bool CSqlSync::SetAutoIncrementKey(CString const &Table)
{
	CString Query = CPrintf("ALTER TABLE %s ADD PK_identity INT NOT NULL IDENTITY(1, 1) PRIMARY KEY", PCTXT(Table));

	CTdsPacket Response;

	return ExecuteQuery(Query, Response);
	}

bool CSqlSync::SetDatetimeKey(CString const &Table)
{
	CString Query = CPrintf("ALTER TABLE %s ADD PRIMARY KEY([Datetime])", PCTXT(Table));

	CTdsPacket Response;

	return ExecuteQuery(Query, Response);
	}

bool CSqlSync::HasPrimaryKey(CString const &Table)
{
	CString Query = CPrintf("SELECT name FROM sys.key_constraints WHERE OBJECT_NAME(parent_object_id) = '%s' AND type = 'PK'", PCTXT(Table));

	CTdsDataSet DataSet;

	if( ExecuteQuery(Query, DataSet) ) {

		return DataSet.GetNumRows() != 0;
		}

	return false;
	}

UINT CSqlSync::GetPrimaryKeyType(CString const &Table)
{
	CString Query = CPrintf("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '%s' AND COLUMN_NAME = 'PK_identity'", PCTXT(Table));

	CTdsDataSet DataSet;

	if( ExecuteQuery(Query, DataSet) ) {

		if( DataSet.GetNumRows() != 0 ) {

			return sqlKeyAutoInc;
			}
		else {
			return sqlKeyDatetime;
			}
		}

	return sqlKeyDatetime;
	}

bool CSqlSync::RemoveKeyConstraint(CString const &Table)
{
	CString Query = CPrintf("DECLARE @SQL VARCHAR(1024)\n"
				"DECLARE @TableName VARCHAR(128)\n"
				"DECLARE @PKNAME VARCHAR(128)\n"
				"SET @TableName = '%s'\n"
				"SET @SQL = 'ALTER TABLE ' + @TableName + ' DROP CONSTRAINT <PK>'\n"
				"SET @SQL = REPLACE(@SQL, '<PK>', (SELECT name FROM sys.key_constraints WHERE type = 'PK' AND OBJECT_NAME(parent_object_id) = '%s'))\n"
				"EXEC(@SQL)\n",
				PCTXT(Table),
				PCTXT(Table)
				);

	CTdsPacket Response;

	return ExecuteQuery(Query, Response);
	}

bool CSqlSync::RemoveDatetimeKey(CString const &Table)
{
	return RemoveKeyConstraint(Table);
	}

bool CSqlSync::RemoveIdentKey(CString const &Table)
{
	if( RemoveKeyConstraint(Table) ) {

		CString Query = CPrintf("ALTER TABLE %s DROP COLUMN PK_identity", PCTXT(Table));

		CTdsPacket Response;

		return ExecuteQuery(Query, Response);
		}

	return false;
	}

bool CSqlSync::SendBulkData(CTdsBulkLoad& clsPendingBulkData, CTdsPacket& clsResponse, bool fFinished)
{
	UINT uIndex = 0;

	// Create maximum-sized packets out of the pending bulk data until only one packet is left.
	while(clsPendingBulkData.GetSize() > m_uMaxPacketSize - TDS_HEADER_BYTES) {

		CTdsBulkLoad clsPacket(m_uMaxPacketSize, TRUE);

		// Fill packet with pending data until max size is reached.
		while(clsPacket.GetSize() < m_uMaxPacketSize) {

			clsPacket.AddByte( clsPendingBulkData.GetByte(uIndex) );
		}
		clsPacket.EndPacket(statusNormal);

		// Remove the sent pending data from buffer.
		clsPendingBulkData.RemoveHead(uIndex);
		uIndex = 0;

		TraceInfo("   Sending bulk load packet\n");

		// Send packet.
		SendCommand(clsPacket);
	}

	// If all data to be sent is pending, send the remainder out as the final packet and get reply.
	if(fFinished)
	{
		CTdsBulkLoad clsPacket(m_uMaxPacketSize, TRUE);

		// Fill packet with all remaining pending data.
		while(uIndex < clsPendingBulkData.GetSize()) {

			clsPacket.AddByte( clsPendingBulkData.GetByte(uIndex) );
		}

		// Set End of Message flag.
		clsPacket.EndPacket(statusEnd);

		TraceInfo("   Sending final bulk load packet\n");

		// Get response from server.
		return SendCommandAndGetReply(clsPacket, clsResponse);
	}
	return false;
}

bool CSqlSync::FormAndSendBulkLoadPackets(CAutoFile &File, CString& szFirstDateInPacket, CString& szFirstTimeInPacket,
						CString& szLastDateInPacket, CString& szLastTimeInPacket, CTdsPacket& clsResponse)
{
	// Create headerless packet to store pending data as buffer.
	CTdsBulkLoad clsBulkLoadPacket(m_uMaxPacketSize * 2, FALSE);

	// add metadata token to packet.
	clsBulkLoadPacket.AddByte(TDS_COLMETADATA_TOKEN);

	// Add column count and column info to the packet.
	clsBulkLoadPacket.AddColumnCount(m_ColumnInfo.GetCount());
	for (UINT uCol=0; uCol < m_ColumnInfo.GetCount(); uCol++)
	{
		clsBulkLoadPacket.AddColumn(PCTXT(m_ColumnInfo[uCol].m_ColumnName), (TdsDataType) m_ColumnInfo[uCol].m_eSQLDataType, m_ColumnInfo[uCol].m_wSQLDataSize, (uCol != 0));

		// Send out packets if maximum packet size has been exceeded:
		SendBulkData(clsBulkLoadPacket, clsResponse, false);
		}

	UINT uRows = ReadColumnValues(File, true);
	
	// Return first date and time.
	szFirstDateInPacket = m_ColDate;
	szFirstTimeInPacket = m_ColTime;

	// Send out packets if maxmimum packet size has been exceeded:
	SendBulkData(clsBulkLoadPacket, clsResponse, false);

	while( uRows ) {
				
		for( UINT n = 0; n < uRows; n++ ) {

			clsBulkLoadPacket.StartRow();

			// add to packet a value for each column (obtained from object's column values array).
			for( UINT uCol = 0; uCol < m_ColumnInfo.GetCount(); uCol++ ) {

				WORD wSize = m_ColumnInfo[uCol].m_wSQLDataSize;

				switch(m_ColumnInfo[uCol].m_eSQLDataType) {

					case typeIntNType:
						if (wSize == 1)
							clsBulkLoadPacket.AddFieldInt1((BYTE)atoi(m_ColValues[n][uCol].m_Value));
						else if (wSize == 2)
							clsBulkLoadPacket.AddFieldInt2((SHORT)atoi(m_ColValues[n][uCol].m_Value));
						else if (wSize == 4)
							clsBulkLoadPacket.AddFieldInt4((LONG)atol(m_ColValues[n][uCol].m_Value));
						break;

					case typeFltNType:
						if (wSize == 4) 
							clsBulkLoadPacket.AddFieldInt4(R2I(atof(m_ColValues[n][uCol].m_Value)));
						else if (wSize == 8)	// TODO:  Double to 64-bit long conversion needed
							clsBulkLoadPacket.AddFieldInt8((LONGLONG)atof(m_ColValues[n][uCol].m_Value));
						break;

					case typeDateTimNType:
						clsBulkLoadPacket.AddFieldDateTime(ConvertTimeDateToSQL(ParseTimeDate(m_ColValues[n][0].m_Date, m_ColValues[n][0].m_Time), GetMillis(m_ColValues[n][0].m_Time)));
						break;

					case typeBigVarChrType:
					default:
						clsBulkLoadPacket.AddFieldVarchar(m_ColValues[n][uCol].m_Value, m_ColValues[n][uCol].m_Size - sizeof(USHORT));
						break;

					}
				}

			// record last date and time.
			szLastDateInPacket = m_ColDate;
			szLastTimeInPacket = m_ColTime;

			// Send out packets if maxmimum packet size has been exceeded:
			SendBulkData(clsBulkLoadPacket, clsResponse, false);
			}

		LogProgress(File);

		// read next row of columns (including total required size in packet).
		uRows = ReadColumnValues(File, false);
		}

	// Add done token.
	clsBulkLoadPacket.AddByte(TDS_DONE_TOKEN);
	clsBulkLoadPacket.AddUShort(0x00);
	clsBulkLoadPacket.AddUShort(0x00);
	clsBulkLoadPacket.AddULong(0x00);
	clsBulkLoadPacket.AddULong(0x00);
		
	// Send out all remaining data and get reply:
	return SendBulkData(clsBulkLoadPacket, clsResponse, true);
	}


void CSqlSync::FormSQLDateTime(CString* szTheCommand, PCSTR szDate, PCSTR szTime)
{
	*szTheCommand += "'";
	*szTheCommand += szDate;
	*szTheCommand += " ";
	*szTheCommand += szTime;
	*szTheCommand += "'";

	}


CString* CSqlSync::FormDeleteSQLCommand(PCSTR szTableName, PCSTR szStartDate, PCSTR szStartTime, PCSTR szEndDate, PCSTR szEndTime)
{
	CString*	szTheCommand = New CString();

	*szTheCommand = "delete from [";
	*szTheCommand += szTableName;
	*szTheCommand += "] where DateTime >= ";
	FormSQLDateTime(szTheCommand, szStartDate, szStartTime);
	*szTheCommand += " and DateTime <= ";
	FormSQLDateTime(szTheCommand, szEndDate, szEndTime);
	*szTheCommand += ";";

	return szTheCommand;

	}


// Time and Date Methods

DWORD CSqlSync::FATTimeToInternalTime(CTime &clsTime)
{
	return Date(clsTime.uYear, clsTime.uMonth, clsTime.uDate) + Time(clsTime.uHours, clsTime.uMinutes, clsTime.uSeconds);
	}


DWORD CSqlSync::ParseTimeDate(PCSTR szDate, PCSTR szTime)
{
	// expect date as yyyy/mm/dd.
	UINT Year = atol(&szDate[0]);
	UINT Month = atol(&szDate[5]);
	UINT Day = atol(&szDate[8]);

	// expect time as hh:mm:ss.
	UINT Hour = atol(&szTime[0]);
	UINT Minute = atol(&szTime[3]);
	UINT Second = atol(&szTime[6]);

	return Date(Year, Month, Day) + Time(Hour, Minute, Second);

	}

DWORD CSqlSync::GetMillis(PCSTR szTime)
{
	PCSTR pDot = strchr(szTime, '.');

	if( pDot ) {

		UINT uLen = strlen(pDot + 1);

		DWORD dwMillis = atol(pDot + 1);

		switch( uLen ) {
			case 1:
				dwMillis *= 100;
				break;
			case 2:
				dwMillis *= 10;
				break;
		}

		return dwMillis;
	}

	return 0;
}

ULONGLONG CSqlSync::ConvertTimeDateToSQL(DWORD TimeDate, DWORD Millis)
{
	ULONGLONG  uDays = GetDays(TimeDate);	// Days since 01/01/1997.
		   
	ULONGLONG  uSecs = TimeDate - (uDays * 60 * 60 * 24);	// Seconds since 12:00 AM of that day.

	uDays += 35429;  // Convert from days since 01/01/1997 to days since 01/01/1900.

	uSecs *= 300;    // Convert from seconds to 300ths of second.

	return ((uDays << 32) + uSecs) + UINT(Millis / 3.333);
	}

// End of File
