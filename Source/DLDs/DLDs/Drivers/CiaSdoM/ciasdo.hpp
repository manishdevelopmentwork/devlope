
#include "ciabase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define TRANSFER_EXP	0
#define TRANSFER_NORM	1

#define NMTS_BOOT	0
#define NMTS_STOP	4
#define NMTS_OP		5
#define NMTS_PRE_OP	127

#define NMT_COB_ID	1792
#define NMT_START	1
#define NMT_STOP	2
#define NMT_PRE_OP	128
#define NMT_RESET	129
#define NMT_RESET_COMM	130
 
//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Master Driver
//

class CCANOpenSDOMaster : public CMasterDriver
{
	public:
		// Constructor
		CCANOpenSDOMaster(void);

		// Destructor
		~CCANOpenSDOMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void * pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE  m_bDrop;
			BOOL  m_fSendInit;
			BOOL  m_fSentInit;
			UINT  m_uPing;
			UINT  m_uTime;
			BYTE  m_bRestrict;
			BYTE  m_bDecode;
			UINT  m_uDelay;
			BYTE  m_bTransfer;
			UINT  m_uBackOff;
			DWORD m_dwHistAddr[16];
			UINT  m_dwHistTime[16];
			};

		// Data Members
		CCANPortHandler * m_pData;
		CContext        * m_pCtx;
		FRAME             m_TxData;
		FRAME             m_RxData;
		UINT		  m_uBaud;
	
		// Implementation
		BOOL SendInit(void);
		void SetError(void);
		BOOL GetFrame(void);
		BOOL PutFrame(void);
		BOOL WriteByte(BYTE bDrop, WORD wIndex, BYTE bSub, BYTE bData);
		BOOL WriteWord(BYTE bDrop, WORD wIndex, BYTE bSub, WORD wData);
		BOOL WriteLong(BYTE bDrop, WORD wIndex, BYTE bSub, LONG lData);
		BOOL SendWrite(BYTE bDrop);
		BOOL ReadByte(BYTE bDrop, WORD wIndex, BYTE bSub, BYTE &bData, BOOL fFast);
		BOOL ReadWord(BYTE bDrop, WORD wIndex, BYTE bSub, WORD &wData, BOOL fFast);
		BOOL ReadLong(BYTE bDrop, WORD wIndex, BYTE bSub, LONG &lData, BOOL fFast);
		void InitRead(BYTE bDrop, WORD wIndex, BYTE bSub);
		BOOL SendRead(BYTE bDrop, BOOL fFast);
		BOOL MatchMulti(void);
		BOOL CheckSDO(BYTE bDrop);
		void StartSDO(BYTE bDrop);
		BOOL EatError(PCBYTE pData);
		BOOL IsSubDisabled(void);
		BOOL IsIndexLimited(void);
		UINT GetSub(AREF Addr);
		UINT GetIndex(AREF Addr);
		UINT FindUnusedBytes(UINT uCount);
		UINT FindTransfer(void);

		// Write Throttling
		void ClearHistory(void);
		UINT FindHistory(DWORD dwAddr);
		UINT AllocHistory(DWORD dwAddr);
		void UpdateHistory(UINT i);
		BOOL TestHistory(UINT i);
	};

// End of File
