
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Variable List
//

// Constructor


CVariableList::CVariableList(void)
{
	m_uCount = 0;

	m_uUsed  = 0;

	m_ppVar  = NULL;
	}

// Destructor

CVariableList::~CVariableList(void)
{
	while( m_uCount-- ) {

		delete m_ppVar[m_uCount];
		}

	delete [] m_ppVar;
	}

// Initialization

void CVariableList::Load(PCBYTE &pData)
{
	ValidateLoad("CVariableList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppVar = New CVariableItem * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CVariableItem *pVar = NULL;

			if( GetByte(pData) ) {

				pVar = New CVariableItem;

				pVar->Load(pData);
				}			

			m_ppVar[n] = pVar;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Variable Item
//

// Constructor

CVariableItem::CVariableItem(void)
{
	m_pValue       = NULL;
	}

// Destructor

CVariableItem::~CVariableItem(void)
{
	delete m_pValue;
	}

//

void CVariableItem::SetScan(UINT uCode)
{
	SetItemScan(m_pValue,       uCode);
	}

DWORD CVariableItem::GetData(void)
{
	UINT  Type = typeVoid;

	DWORD Data = 0;

	if( m_pValue ) {

		if( m_pValue->IsAvail() ) {

			Type         = m_pValue->GetType();			

			Data         = m_pValue->Execute(Type);

			return Data;
			}

		AfxTrace(" value is not available\n");

		return 0;
		}

	AfxTrace(" value is null\n");

	return 0;
	}

void CVariableItem::SetData(DWORD dwData)
{
	if( m_pValue ) {

		UINT  Type = m_pValue->GetType();

		m_pValue->SetValue(dwData, Type, setNone);
		
		return;
		}

	AfxTrace(" value is null\n");
	}

// Initialization

void CVariableItem::Load(PCBYTE &pData)
{
	ValidateLoad("CVariableItem", pData);

	GetCoded(pData, m_pValue);
	}

// End of File
