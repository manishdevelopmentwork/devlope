
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SlaveCommsDriver_HPP

#define INCLUDE_SlaveCommsDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Slave Comms Driver Object
//

class DLLAPI CSlaveDriver : public CCommsDriver
{
	public:
		// Constructor
		CSlaveDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);

		// IDriver
		WORD METHOD GetCategory(void);

	protected:
		// Data
		ISlaveHelper * m_pSlaveHelper;

		// IO
		CCODE Read (AREF Addr, PDWORD pData, UINT uCount);
		CCODE Read (AREF Addr, PDWORD pData, IMetaData **ppm);
		CCODE Write(AREF Addr, PDWORD pData, UINT uCount);
		
		// Meta Data
		UINT GetMetaNumber(AREF Addr, WORD ID);
		BOOL GetMetaString(AREF Addr, WORD ID, PTXT pBuff, UINT uSize);
		UINT GetMetaNumber(IMetaData *pm, WORD ID);
		BOOL GetMetaString(IMetaData *pm, WORD ID, PTXT pBuff, UINT uSize);
	};

// End of File

#endif
