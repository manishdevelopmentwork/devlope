
#include "intern.hpp"

#include "PrimRubySimpleImage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyShade.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Simple Image Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubySimpleImage, CPrimRubyWithText);

// Constructor

CPrimRubySimpleImage::CPrimRubySimpleImage(void)
{
	m_uType  = 0x82;

	m_Keep   = 0;

	m_pColor = NULL;

	m_pShade = New CPrimRubyShade;

	m_pImage = NULL;
	}

// UI Overridables

BOOL CPrimRubySimpleImage::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_IMAGE),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHADING),
				       AfxPointerClass(this),
				       2
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// UI Update

void CPrimRubySimpleImage::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Keep" ) {

		if( m_pImage ) {

			m_pImage->m_Opts.m_Keep = m_Keep;

			pHost->UpdateUI(L"Image");
			}
		}

	if( Tag == L"Count" ) {

		DoEnables(pHost);
		}

	CPrimRubyWithText::OnUIChange(pHost, pItem, Tag);
	}

// Operations

void CPrimRubySimpleImage::LoadFromDataObject(IDataObject *pData, CSize MaxSize)
{
	if( !m_pImage ) {

		m_pImage = New CPrimImage;

		m_pImage->SetParent(this);

		m_pImage->Init();
		}

	SetRect(m_pImage->LoadFromDataObject(pData, MaxSize));
	}

// Overridables

BOOL CPrimRubySimpleImage::HitTest(P2 Pos)
{
	if( CPrimRubyWithText::HitTest(Pos) ) {

		return TRUE;
		}

	if( PtInRect(m_bound, Pos) ) {

		// TOOD -- Hit test the image alpha channel? !!!!

		return TRUE;
		}
		
	return FALSE;
	}

void CPrimRubySimpleImage::Draw(IGDI *pGDI, UINT uMode)
{
	if( m_pImage ) {

		UINT rop  = 0;
		
		if( m_pColor ) {
			
			if( !m_pColor->ExecVal() ) {

				rop = ropDisable;
				}
			}

		if( m_pShade->m_Pattern ) {

			m_pImage->Draw(pGDI, m_DrawRect, uMode, rop, m_pShade);
			}
		else
			m_pImage->Draw(pGDI, m_DrawRect, uMode, rop);
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCTXT pt = L"IMG";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimRubyWithText::Draw(pGDI, uMode);
	}

void CPrimRubySimpleImage::SetInitState(void)
{
	CPrimRubyWithText::SetInitState();

	m_pShade->m_Pattern = 0;
	}

void CPrimRubySimpleImage::UpdateLayout(void)
{
	CPrimRubyWithText::UpdateLayout();

	m_bound = m_DrawRect;
	}

void CPrimRubySimpleImage::GetRefs(CPrimRefList &Refs)
{
	if( m_pImage ) {
			
		m_pImage->GetRefs(Refs);
		}

	CPrimRubyWithText::GetRefs(Refs);
	}

void CPrimRubySimpleImage::EditRef(UINT uOld, UINT uNew)
{
	if( m_pImage ) {
			
		m_pImage->EditRef(uOld, uNew);
		}

	CPrimRubyWithText::EditRef(uOld, uNew);
	}

// Download Support

BOOL CPrimRubySimpleImage::MakeInitData(CInitData &Init)
{
	CPrimRubyWithText::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pImage);

	Init.AddItem(itemVirtual, m_pColor);

	Init.AddItem(itemSimple,  m_pShade);

	return TRUE;
	}

// Meta Data

void CPrimRubySimpleImage::AddMetaData(void)
{
	CPrimRubyWithText::AddMetaData();

	Meta_AddInteger(Keep);
	Meta_AddVirtual(Color);
	Meta_AddObject (Shade);
	Meta_AddVirtual(Image);

	Meta_SetName((IDS_SIMPLE_IMAGE_2));
	}

// Implementation

void CPrimRubySimpleImage::DoEnables(IUIHost *pHost)
{
	}

// End of File
