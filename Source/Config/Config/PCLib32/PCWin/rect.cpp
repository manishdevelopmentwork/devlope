
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rectangle Object
//

// Constructors

CRect::CRect(void)
{
	left   = 0;
	top    = 0;
	right  = 0;
	bottom = 0;
	}

CRect::CRect(RECT const &Src)
{
	left   = Src.left;
	top    = Src.top;
	right  = Src.right;
	bottom = Src.bottom;
	}

CRect::CRect(int x1, int y1, int x2, int y2)
{
	left   = x1;
	top    = y1;
	right  = x2;
	bottom = y2;
	}

CRect::CRect(CPoint const &Org, CPoint const &End)
{
	left   = Org.x;
	top    = Org.y;
	right  = End.x;
	bottom = End.y;
	}

CRect::CRect(CPoint const &Org, CSize const &Size)
{
	left   = Org.x;
	top    = Org.y;
	right  = Org.x + Size.cx;
	bottom = Org.y + Size.cy;
	}

CRect::CRect(int xSize, int ySize)
{
	left   = 0;
	top    = 0;
	right  = xSize;
	bottom = ySize;
	}

CRect::CRect(CSize const &Size)
{
	left   = 0;
	top    = 0;
	right  = Size.cx;
	bottom = Size.cy;
	}

// Clearing

void CRect::Empty(void)
{
	left   = 0;
	top    = 0;
	right  = 0;
	bottom = 0;
	}

// Setting

void CRect::Set(int x1, int y1, int x2, int y2)
{
	left   = x1;
	top    = y1;
	right  = x2;
	bottom = y2;
	}
		
// Assignment Operators

CRect & CRect::operator = (RECT const &Src)
{
	left   = Src.left;
	top    = Src.top;
	right  = Src.right;
	bottom = Src.bottom;

	return ThisObject;
	}

// Conversions

CRect::operator RECT * (void)
{
	return this;
	}

// Compraison Operators

BOOL CRect::operator == (CRect const &Arg) const
{
	return ::EqualRect(this, &Arg) == TRUE;
	}

BOOL CRect::operator != (CRect const &Arg) const
{
	return ::EqualRect(this, &Arg) == FALSE;
	}

// Basic Attributes

BOOL CRect::IsEmpty(void) const
{
	return ::IsRectEmpty(this);
	}

int CRect::GetWidth(void) const
{
	return right - left;
	}

int CRect::GetHeight(void) const
{
	return bottom - top;
	}

CPoint CRect::GetOrg(void) const
{
	return CPoint(left, top);
	}

CSize CRect::GetSize(void) const
{
	return CSize(right - left, bottom - top);
	}

CSize CRect::GetSign(void) const
{
	return CSize(Sgn(right - left), Sgn(bottom - top));
	}

// Simple Aliases

int CRect::cx(void) const
{
	return right - left;
	}

int CRect::cy(void) const
{
	return bottom - top;
	}

// Derrived Positions

CPoint CRect::GetTopLeft(void) const
{
	return CPoint(left, top);
	}

CPoint CRect::GetTopRight(void) const
{
	return CPoint(right, top);
	}

CPoint CRect::GetBottomLeft(void) const
{
	return CPoint(left, bottom);
	}

CPoint CRect::GetBottomRight(void) const
{
	return CPoint(right, bottom);
	}

CPoint CRect::GetCenter(void) const
{
	return CPoint((left + right) / 2, (bottom + top) / 2);
	}

CPoint CRect::GetCentre(void) const
{
	return CPoint((left + right) / 2, (bottom + top) / 2);
	}

// Clipping

void CRect::ClipRectTo(CRect const &Clip)
{
	CSize Sign = Normalize();

        if( left < Clip.left ) {
        
        	int n = Clip.left - left;
        	
        	operator += (CPoint(n, 0));
        	}
        
        if( right > Clip.right ) {
        
        	int n = right - Clip.right;
        	
        	operator -= (CPoint(n, 0));
        	}
        
        if( top < Clip.top ) {
        
        	int n = Clip.top - top;
        	
        	operator += (CPoint(0, n));
        	}
        
        if( bottom > Clip.bottom ) {
        
        	int n = bottom - Clip.bottom;
        	
        	operator -= (CPoint(0, n));
        	}
        	
        Denormalize(Sign);
        }

void CRect::ClipEndsTo(CRect const &Clip)
{
	CSize Sign = Normalize();

	MakeMax(left, Clip.left);

	MakeMin(left, Clip.right - 1);
	
	MakeMax(right, Clip.left + 1);
	
	MakeMin(right, Clip.right);

	MakeMax(top, Clip.top);

	MakeMin(top, Clip.bottom - 1);
	
	MakeMax(bottom, Clip.top + 1);
	
	MakeMin(bottom, Clip.bottom);
        	
        Denormalize(Sign);
	}

// Normalization

CSize CRect::Normalize(void)
{
	CSize Sign = GetSign();
	
	if( Sign.cx < 0 ) {

		Swap(left, right);
		}
		
	if( Sign.cy < 0 ) {

		Swap(top, bottom);
		}
		
	return Sign;
	}

// Denormalization

void CRect::Denormalize(CSize Sign)
{
	if( Sign.cx < 0 ) {

		Swap(left, right);
		}
		
	if( Sign.cy < 0 ) {

		Swap(top, bottom);
		}
	}

// Aspect Matching

void CRect::MatchAspect(CSize Aspect)
{
	int    cx   = right  - left;

	int    cy   = bottom - top;

	double Req  = double(Aspect.cx) / double(Aspect.cy);

	double Act  = double(cx)        / double(cy);

	if( Req > Act ) {

		int dy = cy - int(cy * Act / Req);

		top    += (dy + 1) / 2;

		bottom -= (dy + 0) / 2;
		}

	if( Req < Act ) {

		int dx = cx - int(cx * Req / Act);

		left  += (dx + 1) / 2;

		right -= (dx + 0) / 2;
		}
	}

// Hit Testing

BOOL CRect::PtInRect(CPoint const &Pos) const
{
	return ::PtInRect(this, Pos);
	}

BOOL CRect::Overlaps(CRect const &Rect) const
{
	return (ThisObject & Rect).IsEmpty() == FALSE;
	}

BOOL CRect::Encloses(CRect const &Rect) const
{
	return (ThisObject | Rect) == ThisObject;
	}

// Dragging Functions

void CRect::DragTopLeft(CPoint const &Pos)
{
	CSize Size = GetSize();

	left   = Pos.x;
	top    = Pos.y;
	right  = Pos.x + Size.cx;
	bottom = Pos.y + Size.cy;
	}

void CRect::DragTopRight(CPoint const &Pos)
{
	CSize Size = GetSize();

	left   = Pos.x - Size.cx;
	top    = Pos.y;
	right  = Pos.x;
	bottom = Pos.y + Size.cy;
	}

void CRect::DragBottomLeft(CPoint const &Pos)
{
	CSize Size = GetSize();

	left   = Pos.x;
	top    = Pos.y - Size.cy;
	right  = Pos.x + Size.cx;
	bottom = Pos.y;
	}

void CRect::DragBottomRight(CPoint const &Pos)
{
	CSize Size = GetSize();

	left   = Pos.x - Size.cx;
	top    = Pos.y - Size.cy;
	right  = Pos.x;
	bottom = Pos.y;
	}

void CRect::DragCentre(CPoint const &Pos)
{
	CSize Size = GetSize();

	left   = Pos.x - (Size.cx + 1) / 2;
	top    = Pos.y - (Size.cy + 1) / 2;
	right  = Pos.x + (Size.cx + 0) / 2;
	bottom = Pos.y + (Size.cy + 0) / 2;
	}

// Offset in Place

CRect const & CRect::operator += (CPoint const &Step)
{
	left   += Step.x;
	top    += Step.y;
	right  += Step.x;
	bottom += Step.y;
	
	return ThisObject;
	}

CRect const & CRect::operator -= (CPoint const &Step)
{
	left   -= Step.x;
	top    -= Step.y;
	right  -= Step.x;
	bottom -= Step.y; 
	
	return ThisObject;
	}

// Offset via Friends

CRect operator + (CRect const &a, CPoint const &b)
{
	CRect Result;

	Result.left   = a.left   + b.x;
	Result.top    = a.top    + b.y;
	Result.right  = a.right  + b.x;
	Result.bottom = a.bottom + b.y;

	return Result;
	}

CRect operator - (CRect const &a, CPoint const &b)
{
	CRect Result;

	Result.left   = a.left   - b.x;
	Result.top    = a.top    - b.y;
	Result.right  = a.right  - b.x;
	Result.bottom = a.bottom - b.y;

	return Result;
	}

// Inflation Functions

void CRect::Inflate(int nStep)
{
	left   -= nStep;
	top    -= nStep;
	right  += nStep;
	bottom += nStep;
	}

void CRect::Inflate(CSize const &Step)
{
	left   -= Step.cx;
	top    -= Step.cy;
	right  += Step.cx;
	bottom += Step.cy;
	}

void CRect::Deflate(int nStep)
{
	left   += nStep;
	top    += nStep;
	right  -= nStep;
	bottom -= nStep;
	}

void CRect::Deflate(CSize const &Step)
{
	left   += Step.cx;
	top    += Step.cy;
	right  -= Step.cx;
	bottom -= Step.cy;
	}

// Inflation Operators

CRect const & CRect::operator ++ (void)
{
	left   -= 1;
	top    -= 1;
	right  += 1;
	bottom += 1;
	
	return ThisObject;
	}

CRect CRect::operator ++ (int nDummy)
{
	CRect Prev = ThisObject;
	
	left   -= 1;
	top    -= 1;
	right  += 1;
	bottom += 1;
	
	return Prev;
	}

CRect const & CRect::operator -- (void)
{
	left   += 1;
	top    += 1;
	right  -= 1;
	bottom -= 1;
	
	return ThisObject;
	}

CRect CRect::operator -- (int nDummy)
{
	CRect Prev = ThisObject;
	
	left   += 1;
	top    += 1;
	right  -= 1;
	bottom -= 1;
	
	return Prev;
	}

// Inflation in Place

CRect const & CRect::operator += (int nStep)
{
	left   -= nStep;
	top    -= nStep;
	right  += nStep;
	bottom += nStep;
	
	return ThisObject;
	}

CRect const & CRect::operator += (CSize const &Step)
{
	left   -= Step.cx;
	top    -= Step.cy;
	right  += Step.cx;
	bottom += Step.cy;
	
	return ThisObject;
	}

CRect const & CRect::operator -= (int nStep)
{
	left   += nStep;
	top    += nStep;
	right  -= nStep;
	bottom -= nStep;
	
	return ThisObject;
	}

CRect const & CRect::operator -= (CSize const &Step)
{
	left   += Step.cx;
	top    += Step.cy;
	right  -= Step.cx;
	bottom -= Step.cy;
	
	return ThisObject;
	}

// Inflation via Friends

CRect operator + (CRect const &a, int nStep)
{
	CRect Result;

	Result.left   = a.left   - nStep;
	Result.top    = a.top    - nStep;
	Result.right  = a.right  + nStep;
	Result.bottom = a.bottom + nStep;

	return Result;
	}

CRect operator + (CRect const &a, CSize const &b)
{
	CRect Result;

	Result.left   = a.left   - b.cx;
	Result.top    = a.top    - b.cy;
	Result.right  = a.right  + b.cx;
	Result.bottom = a.bottom + b.cy;

	return Result;
	}

CRect operator - (CRect const &a, int nStep)
{
	CRect Result;

	Result.left   = a.left   + nStep;
	Result.top    = a.top    + nStep;
	Result.right  = a.right  - nStep;
	Result.bottom = a.bottom - nStep;

	return Result;
	}

CRect operator - (CRect const &a, CSize const &b)
{
	CRect Result;

	Result.left   = a.left   + b.cx;
	Result.top    = a.top    + b.cy;
	Result.right  = a.right  - b.cx;
	Result.bottom = a.bottom - b.cy;

	return Result;
	}

// Scaling in Place

void CRect::operator *= (int nScale)
{
	right  = left + (right - left) * nScale;
	bottom = top  + (bottom - top) * nScale;
	}

void CRect::operator *= (CSize const &Scale)
{
	right  = left + (right - left) * Scale.cx;
	bottom = top  + (bottom - top) * Scale.cy;
	}

void CRect::operator /= (int nScale)
{
	right  = left + (right - left) / nScale;
	bottom = top  + (bottom - top) / nScale;
	}

void CRect::operator /= (CSize const &Scale)
{
	right  = left + (right - left) / Scale.cx;
	bottom = top  + (bottom - top) / Scale.cy;
	}

// Scaling via Friends

CRect operator * (CRect const &a, int nScale)
{
	CRect Result;

	Result.right  = a.left + (a.right - a.left) * nScale;
	Result.bottom = a.top  + (a.bottom - a.top) * nScale;

	return Result;
	}

CRect operator * (int nScale, CRect const &b)
{
	CRect Result;

	Result.right  = b.left + (b.right - b.left) * nScale;
	Result.bottom = b.top  + (b.bottom - b.top) * nScale;

	return Result;
	}

CRect operator * (CRect const &a, CSize const &b)
{
	CRect Result;

	Result.right  = a.left + (a.right - a.left) * b.cx;
	Result.bottom = a.top  + (a.bottom - a.top) * b.cy;

	return Result;
	}

CRect operator / (CRect const &a, int nScale)
{
	CRect Result;

	Result.right  = a.left + (a.right - a.left) / nScale;
	Result.bottom = a.top  + (a.bottom - a.top) / nScale;

	return Result;
	}

CRect operator / (CRect const &a, CSize const &b)
{
	CRect Result;

	Result.right  = a.left + (a.right - a.left) / b.cx;
	Result.bottom = a.top  + (a.bottom - a.top) / b.cy;

	return Result;
	}

// Combination in Place

CRect const & CRect::operator &= (CRect const &Rect)
{
	::IntersectRect(this, this, &Rect);
	
	return ThisObject;
	}

CRect const & CRect::operator |= (CRect const &Rect)
{
	::UnionRect(this, this, &Rect);
	
	return ThisObject;
	}

// Combination via Friends

CRect operator & (CRect const &a, CRect const &b)
{
	CRect Result;

	::IntersectRect(&Result, &a, &b);

	return Result;
	}

CRect operator | (CRect const &a, CRect const &b)
{
	CRect Result;

	::UnionRect(&Result, &a, &b);

	return Result;
	}

// Zeroing

void AfxSetZero(CRect &r)
{
	r.left   = 0;
	r.top    = 0;
	r.right  = 0;
	r.bottom = 0;
	}

// End of File
