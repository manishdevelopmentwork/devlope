
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Out4MOD_HPP

#define INCLUDE_Out4MOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "out4conf.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class COut4Module;
class COut4MainWnd;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Analog Output Module
//

class COut4Module : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COut4Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		COut4Conf * m_pConf;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Analog Output Module Main Window
//

class COut4MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		COut4MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd     * m_pMult;
		COut4Module       * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddConfig(void);
	};

// End of File

#endif
