
#include "intern.hpp"

#include "modslc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Graphite PID1 Module Configuration
//

class CGraphitePid1Module : public CSLCModule
{
	public:
		// Constructor
		CGraphitePid1Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite PID1 Module Configuration
//

// Instantiator

CModule * Create_GMPID1(void)
{
	return New CGraphitePid1Module;
	}

// Constructor

CGraphitePid1Module::CGraphitePid1Module(void)
{	
	m_FirmID = FIRM_GMPID1;
	}

// Overridables

void CGraphitePid1Module::OnLoad(PCBYTE &pData)
{
	m_wNumber = GetWord(pData);

	m_dwSlot  = GetLong(pData);

	CSLCModule::OnLoad(pData);
	}

// End of File
