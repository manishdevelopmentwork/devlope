
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DiagCommand_HPP
	
#define	INCLUDE_DiagCommand_HPP

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics Command
//

class CDiagCommand : public IDiagCommand
{
	public:
		// Constructor
		CDiagCommand(PCTXT pLine, BOOL fConsole);

		// Destructor
		~CDiagCommand(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagCommand
		UINT  METHOD GetCode    (void);
		PCTXT METHOD GetCmdLine (void);
		UINT  METHOD GetArgCount(void);
		PCTXT METHOD GetArg     (UINT uArg);
		BOOL  METHOD FromConsole(void);

		// Attributes
		PCTXT GetCommand(void);

		// Operations
		void SetCode(UINT uCode);

	protected:
		// Data Members
		ULONG  m_uRefs;
		PCTXT  m_pLine;
		BOOL   m_fConsole;
		UINT   m_uCode;
		UINT   m_uTok;
		PTXT * m_pTok;

		// Implementation
		void Tokenize(void);
	};

// End of File

#endif
