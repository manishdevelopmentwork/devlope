
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LFD_HPP
	
#define	INCLUDE_LFD_HPP

//////////////////////////////////////////////////////////////////////////
//
// LFD Driver
//

class CLFDDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CLFDDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
