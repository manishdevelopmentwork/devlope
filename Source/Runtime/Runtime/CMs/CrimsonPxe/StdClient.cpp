
#include "Intern.hpp"

#include "StdClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Timing Constants
//

static UINT const timeInitTimeout  = (5*60*1000);

static UINT const timeTimeout      = (5*60*1000);

static UINT const timeRecvDelay	   = 5;

//////////////////////////////////////////////////////////////////////////
//
// Standard RFC Client
//

// Constructor

CStdClient::CStdClient(void)
{
	m_pConfig   = NULL;

	m_uCheck    = tlsCheckNone;

	m_uOption   = NOTHING;

	m_pTls      = NULL;

	m_pResolver = NULL;

	m_pUtils    = NULL;

	AfxGetObject("net.dns", 0, IDnsResolver, m_pResolver);

	AfxGetObject("ip", 0, INetUtilities, m_pUtils);
}

// Destructor

CStdClient::~CStdClient(void)
{
	AfxRelease(m_pTls);

	AfxRelease(m_pResolver);

	AfxRelease(m_pUtils);
}

// Operations

void CStdClient::SetConfig(CStdClientConfig const *pConfig)
{
	m_pConfig = pConfig;
}

// Transport

BOOL CStdClient::RecvReply(void)
{
	return RecvReply(timeTimeout);
}

BOOL CStdClient::RecvReply(UINT uTimeout)
{
	m_uReplyCode = 999;

	m_ReplyExtra.Empty();

	m_ReplyText.Empty();

	CString Line;

	SetTimer(uTimeout);

	while( GetTimer() ) {

		char cData = 0;

		UINT uData = 1;

		if( m_pCmdSock->Recv(PBYTE(&cData), uData) == S_OK ) {

			if( isprint(cData) ) {

				if( Line.IsEmpty() ) {

					Line.Expand(256);
				}

				Line += cData;
			}
			else {
				if( cData == '\n' ) {

					if( Line.GetLength() >= 4 ) {

						PCTXT pRest = PCTXT(Line) + 4;

						if( Line[3] == '-' ) {

							m_ReplyExtra.Append(pRest);

							AfxTrace("--- %s\n", pRest);
						}
						else {
							m_ReplyText  = pRest;

							m_uReplyCode = atoi(Line);

							AfxTrace("<<< %u %s\n", m_uReplyCode, pRest);

							return TRUE;
						}
					}

					Line.Empty();
				}
			}
		}
		else {
			UINT Phase;

			m_pCmdSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				Sleep(timeRecvDelay);

				continue;
			}

			return FALSE;
		}
	}

	return FALSE;
}

// Socket Management

BOOL CStdClient::OpenCmdSocket(CString const &Host, WORD wPort, BOOL fLarge, BOOL fTls)
{
	if( fTls && !m_pTls ) {

		if( !CreateTlsContext() ) {

			return FALSE;
		}
	}

	if( !CheckCmdSocket() ) {

		if( fTls && m_pTls ) {

			m_pCmdSock = m_pTls->CreateSocket(NULL, m_Host, m_uCheck);
		}
		else {
			m_pCmdSock = NULL;

			AfxNewObject("sock-tcp", ISocket, m_pCmdSock);
		}

		if( m_pCmdSock ) {

			CIpAddr Server;

			if( fLarge ) {

				UINT uOption = OPT_SEND_QUEUE;

				UINT uValue  = 255;

				m_pCmdSock->SetOption(uOption, uValue);
			}

			if( !Host.IsEmpty() ) {

				if( m_pResolver ) {

					Server = m_pResolver->Resolve(Host);

					m_Host = Host;
				}
			}

			if( Server.IsEmpty() ) {

				if( m_uOption < NOTHING ) {

					if( m_pUtils ) {

						Server = m_pUtils->GetOption(BYTE(m_uOption));

						m_Host = Server.GetAsText();
					}
				}
			}

			if( !Server.IsEmpty() ) {

				AfxTrace("connecting to %s:%u at %s\n",
					 PCSTR(m_Host),
					 wPort,
					 PCSTR(Server.GetAsText())
				);

				if( m_pCmdSock->Connect(Server, wPort) == S_OK ) {

					SetTimer(timeInitTimeout);

					while( GetTimer() ) {

						UINT Phase;

						m_pCmdSock->GetPhase(Phase);

						if( Phase == PHASE_OPEN ) {

							m_wPort = wPort;

							return TRUE;
						}

						if( Phase == PHASE_CLOSING ) {

							m_pCmdSock->Close();
						}

						if( Phase == PHASE_ERROR ) {

							break;
						}

						Sleep(timeRecvDelay);
					}

					AfxTrace("failed to connect\n");
				}
			}
			else {
				if( !Host.IsEmpty() ) {

					AfxTrace("cannot find server %s\n", PCSTR(Host));
				}
				else {
					AfxTrace("cannot find server\n");
				}
			}

			AbortCmdSocket();
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CStdClient::SwitchCmdSocket(void)
{
	if( CreateTlsContext() ) {

		m_pCmdSock = m_pTls->CreateSocket(m_pCmdSock, m_Host, m_uCheck);

		if( m_pCmdSock ) {

			IPADDR Ip;

			m_pCmdSock->GetRemote(Ip);

			if( m_pCmdSock->Connect(Ip, m_wPort) == S_OK ) {

				SetTimer(timeInitTimeout);

				while( GetTimer() ) {

					UINT Phase;

					m_pCmdSock->GetPhase(Phase);

					if( Phase == PHASE_OPENING ) {

						Sleep(10);

						continue;
					}

					if( Phase == PHASE_OPEN ) {

						return TRUE;
					}

					AbortCmdSocket();

					break;
				}
			}
		}
	}

	return FALSE;
}

BOOL CStdClient::CreateTlsContext(void)
{
	AfxGetAutoObject(pMatrix, "tls", 0, ITlsLibrary);

	if( pMatrix ) {

		if( !m_pTls ) {

			pMatrix->CreateClientContext(m_pTls);

			if( m_pTls ) {

				if( m_pConfig ) {

					if( m_pConfig->m_uCa ) {

						CByteArray CaCert;

						if( m_pConfig->m_uCa >= 2 ) {

							AfxGetAutoObject(pCertMan, "c3.certman", 0, ICertManager);

							if( m_pConfig->m_uCa >= 100 ) {

								if( !pCertMan->GetTrustedCert(m_pConfig->m_uCa, CaCert) ) {

									pCertMan->GetTrustedRoots(CaCert);
								}
							}
							else {
								pCertMan->GetTrustedRoots(CaCert);
							}
						}
						else {
							CaCert = m_pConfig->m_CaCert;
						}

						if( !CaCert.IsEmpty() ) {

							m_pTls->LoadTrustedRoots(CaCert.data(), CaCert.size());
						}

						m_uCheck = m_pConfig->m_uCheck + 1;
					}
				}

				return TRUE;
			}

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
