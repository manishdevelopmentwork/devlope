
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AppObject_HPP

#define INCLUDE_AppObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CThreadObject;

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

class CAppObject : public IClientProcess, public IThreadNotify
{
	public:
		// Constructor
		CAppObject(void);

		// Destructor
		~CAppObject(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IThreadNotify
		UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex);
		void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uData);

	protected:
		// Data Members
		ULONG           m_uRefs;
		UINT		m_uNotify;
		CThreadObject * m_pThread[2];
		HTHREAD         m_hThread[2];
		ISemaphore    * m_pTms;

		// Implementation
		void CreateThreads(void);
		void InitThreads(void);
		void StartThreads(void);
		void StopThreads(void);
		void TermThreads(void);
		void WaitThreads(void);
		void CloseThreads(void);
		BOOL WaitForTransition(void);
	};

// End of File

#endif
