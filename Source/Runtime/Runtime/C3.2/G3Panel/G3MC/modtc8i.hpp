
//////////////////////////////////////////////////////////////////////////
//
// Modular Controller Master Runtime
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODTC8I_HPP

#define	INCLUDE_MODTC8I_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Isolated TC8 Module Configuration
//

class CTC8IModule : public CModule
{
	public:
		// Constructor
		SLOW CTC8IModule(void);

		// Destructor
		SLOW ~CTC8IModule(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

// End of File

#endif
