
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// List Box Control
//

class CListBox : public CStdControl, public IListBox, public IContainer
{
	public:
		// Constructor
		CListBox(void);

		// Destructor
		~CListBox(void);

		// Management
		void Release(void);
		
		// Creation
		void Create( IContainer * pParent,
			     IManager   * pManager,
			     R2 const   & Rect,
			     UINT	  uID,
			     UINT	  uFlags,
			     UINT	  uStyle
			     );

		// Drawing
		void Draw( IManager *pManager,
			   UINT      uCode
			   );

		// Hit Testing
		BOOL HitTest(IRegion *pDirty);
		BOOL HitTest(P2 const &Point);

		// Core Attributes
		void GetRect(R2 &Rect);

		// Core Operations
		void Enable(BOOL fEnable);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

		// List Operations
		UINT  AddString(PCTXT pText, DWORD dwParam);
		UINT  AddString(PCUTF pText, DWORD dwParam);
		UINT  GetSelect(void);
		DWORD GetSelectData(void);
		UINT  GetText(PTXT pText, UINT uItem);
		UINT  GetText(PUTF pText, UINT uItem);
		void  SetSelect(UINT uSelect);
		void  SelectByParam(DWORD dwParam);
		void  SortData(void);

		// Notification
		void OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData);

		// Style Access
		void GetStyle(IStyle * &pStyle);

	protected:
		// Entry
		struct CEntry
		{
			WORD  m_sText[maxString];
			DWORD m_dwParam;
			};

		// Visual Style
		int   m_nSize;
		COLOR m_colBorder;
		COLOR m_colSelectFore;
		COLOR m_colSelectBack;
		COLOR m_colTextFore;
		COLOR m_colTextBack;
		COLOR m_colTextGrey;

		// Children
		IControl   *m_pTouch;
		IScrollBar *m_pScroll;

		// State Data
		CEntry * m_pList;
		UINT	 m_uAlloc;
		UINT	 m_uCount;
		UINT	 m_uFirst;
		UINT	 m_uSelect;
		UINT	 m_uDrawn;
		int	 m_nPitch;
		BOOL     m_fChange;
		BOOL	 m_fKeep;
		BOOL     m_fFocus;

		// Implementation
		void DefStyle(void);
		void GetStyle(void);
		void Draw(IGdi *pGDI);
		void GrowList(UINT uAlloc);
		void MakeIntegral(int cy, UINT uFlags);

		// Sort Function
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

//////////////////////////////////////////////////////////////////////////
//
// List Box Control
//

// Instantiator

IListBox * Create_ListBox(void)
{
	return New CListBox;
	}

// Constructor

CListBox::CListBox(void)
{
	m_pTouch   = NULL;

	m_pScroll  = Create_ScrollBar();

	m_pList    = NULL;

	m_uAlloc   = 0;

	m_uCount   = 0;

	m_uFirst   = 0;

	m_uSelect  = NOTHING;

	m_fChange  = FALSE;

	m_fKeep    = FALSE;

	m_fFocus   = FALSE;

	DefStyle();
	}

// Destructor

CListBox::~CListBox(void)
{
	delete [] m_pList;

	m_pScroll->Release();
	}

// Management

void CListBox::Release(void)
{
	delete this;
	}

// Creation

void CListBox::Create(IContainer *pParent, IManager *pManager, R2 const &Rect, UINT uID, UINT uFlags, UINT uStyle)
{
	m_pParent = pParent;

	m_Rect    = Rect;

	m_uID     = uID;

	m_uFlags  = uFlags;

	m_uStyle  = uStyle;

	GetStyle();

	MakeIntegral(pManager->GetGDI()->GetTextHeight("X"), uFlags);

	R2 Scroll;

	Scroll.x1 = m_Rect.x2 - 2 - m_nSize;
	Scroll.y1 = m_Rect.y1 + 2;
	Scroll.x2 = m_Rect.x2 - 2;
	Scroll.y2 = m_Rect.y2 - 2;

	m_pScroll->Create(this, pManager, Scroll, 0, 0, 0);
	}

// Drawing

void CListBox::Draw(IManager *pManager, UINT uCode)
{
	if( uCode == drawInitial || m_fDirty ) {

		IGdi *pGDI = pManager->GetGDI();

		Draw(pGDI);

		if( m_fEnable && m_uDrawn < m_uCount ) {

			m_pScroll->Enable  (TRUE);

			m_pScroll->SetRange(0, m_uCount - m_uDrawn);

			if( m_fKeep ) {

				if( m_uSelect < m_uFirst || m_uSelect >= m_uFirst + m_uDrawn ) {

					UINT uSpace = (m_uDrawn / 2) - 1;

					if( m_uSelect > uSpace ) {

						m_uFirst = m_uSelect - uSpace;
						}
					else
						m_uFirst = 0;

					if( m_uFirst > m_uCount - m_uDrawn ) {

						m_uFirst = m_uCount - m_uDrawn;
						}

					Draw(pGDI);
					}

				m_fKeep = FALSE;
				}

			m_pScroll->SetValue(m_uFirst);
			}
		else {
			m_pScroll->Enable(FALSE);

			m_fKeep = FALSE;
			}

		pManager->GetDirtyRegion()->AddRect(m_Rect);

		uCode    = drawInitial;

		m_fDirty = FALSE;
		}

	m_pScroll->Draw(pManager, uCode);
	}

// Hit Testing

BOOL CListBox::HitTest(IRegion *pDirty)
{
	return pDirty->HitTest(m_Rect);
	}

BOOL CListBox::HitTest(P2 const &Point)
{
	return m_fEnable && PtInRect(m_Rect, Point);
	}

// Core Attributes

void CListBox::GetRect(R2 &Rect)
{
	Rect = m_Rect;
	}

// Core Operations

void CListBox::Enable(BOOL fEnable)
{
	if( m_fEnable - fEnable ) {

		m_fEnable = fEnable;

		m_fDirty  = TRUE;
		}
	}

// Messages

BOOL CListBox::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uCode == msgSetFocus ) {

		m_fFocus = TRUE;

		m_fDirty = TRUE;

		return TRUE;
		}

	if( uCode == msgKillFocus ) {

		m_fFocus = FALSE;

		m_fDirty = TRUE;

		return TRUE;
		}

	if( uCode == msgTouchDown ) {

		P2 Point;

		Point.x = LOWORD(dwParam);
		Point.y = HIWORD(dwParam);

		if( m_pScroll->HitTest(Point) ) {

			if( m_pScroll->OnMessage(uCode, dwParam) ) {

				m_pTouch = m_pScroll;

				return TRUE;
				}

			return FALSE;
			}

		if( m_fEnable ) {

			int xp = m_Rect.x1 + 2;

			int yp = m_Rect.y1 + 2;

			int cy = m_nPitch;
			
			int np = 2;

			int rp = cy + 2 * np;

			int x2 = m_Rect.x2 - m_nSize - 3;

			if( Point.x >= xp && Point.x < x2 ) {

				if( Point.y >= yp && Point.y < yp + int(rp * m_uDrawn) ) {

					m_uSelect = m_uFirst + (Point.y - yp) / rp;

					m_fChange = TRUE;

					m_fDirty  = TRUE;

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( uCode == msgTouchRepeat ) {

		if( m_pTouch ) {

			if( m_pTouch->OnMessage(uCode, dwParam) ) {

				return TRUE;
				}

			return m_fDirty;
			}

		return FALSE;
		}

	if( uCode == msgTouchUp ) {

		if( m_pTouch ) {

			BOOL fDone = m_pTouch->OnMessage(uCode, dwParam);

			m_pTouch   = NULL;

			return fDone;
			}
		else {
			if( m_fChange ) {

				SendNotify(m_uSelect, m_pList[m_uSelect].m_dwParam);

				m_fChange = FALSE;

				return TRUE;
				}
			}

		return FALSE;
		}

	return FALSE;
	}

// List Box Operations

UINT CListBox::AddString(PCTXT pText, DWORD dwParam)
{
	GrowList(m_uCount + 1);

	for( int i = 0; !i || pText[i-1]; i++ ) {
		
		m_pList[m_uCount].m_sText[i] = pText[i];
		}

	m_pList[m_uCount].m_dwParam = dwParam;

	m_fDirty = TRUE;

	return m_uCount++;
	}

UINT CListBox::AddString(PCUTF pText, DWORD dwParam)
{
	GrowList(m_uCount + 1);

	for( int i = 0; !i || pText[i-1]; i++ ) {
		
		m_pList[m_uCount].m_sText[i] = pText[i];
		}

	m_pList[m_uCount].m_dwParam = dwParam;

	m_fDirty = TRUE;

	return m_uCount++;
	}

UINT CListBox::GetSelect(void)
{
	return m_uSelect;
	}

DWORD CListBox::GetSelectData(void)
{
	if( m_uSelect < NOTHING ) {

		return m_pList[m_uSelect].m_dwParam;
		}

	return NOTHING;
	}

UINT CListBox::GetText(PTXT pText, UINT uItem)
{
	if( m_uSelect < NOTHING ) {

		PCUTF pInit = m_pList[m_uSelect].m_sText;

		PCUTF pFrom = pInit;

		while( *pText++ = char(*pFrom++) );

		return pFrom - pInit;
		}

	*pText = 0;

	return 0;
	}

UINT CListBox::GetText(PUTF pText, UINT uItem)
{
	if( m_uSelect < NOTHING ) {

		PCUTF pInit = m_pList[m_uSelect].m_sText;

		PCUTF pFrom = pInit;

		while( *pText++ = *pFrom++ );

		return pFrom - pInit;
		}

	*pText = 0;

	return 0;
	}

void CListBox::SetSelect(UINT uSelect)
{
	if( m_uSelect - uSelect ) {

		if( uSelect < m_uCount ) {

			m_uSelect = uSelect;

			m_fKeep   = TRUE;

			m_fDirty  = TRUE;
			}
		}
	}

void CListBox::SelectByParam(DWORD dwParam)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_pList[n].m_dwParam == dwParam ) {

			SetSelect(n);

			break;
			}
		}
	}

void CListBox::SortData(void)
{
	qsort(m_pList, m_uCount, sizeof(CEntry), SortFunc);

	m_fDirty = TRUE;
	}

// Notification

void CListBox::OnNotify(IControl *pCtrl, UINT uID, UINT uCode, int nData)
{
	if( uID == 0 && uCode == 0 ) {

		m_uFirst = nData;

		m_fDirty = TRUE;
		}
	}

// Style Access

void CListBox::GetStyle(IStyle * &pStyle)
{
	if( m_pParent ) {
		
		m_pParent->GetStyle(pStyle);
		}
	}

// Implementation

void CListBox::DefStyle(void)
{
	m_nSize		= 30;

	m_colBorder     = GetRGB( 0, 0, 0);

	m_colSelectFore = GetRGB( 0, 0, 0);
	
	m_colSelectBack = GetRGB(18,18,24);

	m_colTextFore   = GetRGB( 0, 0, 0);
	
	m_colTextBack   = GetRGB(31,31,31);
	
	m_colTextGrey   = GetRGB(24,24,24);
	}

void CListBox::GetStyle(void)
{
	GetStyleMetric(metricScrollSize, m_nSize);

	GetStyleColor (colBorder,        m_colBorder);

	GetStyleColor (colSelectFore,    m_colSelectFore);
	
	GetStyleColor (colSelectBack,    m_colSelectBack);

	GetStyleColor (colTextFore,      m_colTextFore);

	GetStyleColor (colTextBack,      m_colTextBack);

	GetStyleColor (colTextGrey,      m_colTextGrey);
	}

void CListBox::Draw(IGdi *pGDI)
{
	pGDI->SetBrushFore(m_colTextBack);

	pGDI->SetPenFore  (m_colBorder);

	pGDI->FillRect(PassRect(m_Rect));
	
	pGDI->DrawRect(PassRect(m_Rect));

	int xp = m_Rect.x1 + 2;

	int yp = m_Rect.y1 + 2;

	int cy = pGDI->GetTextHeight("X");
	
	int np = 2;

	int rp = cy + 2 * np;

	int x2 = m_Rect.x2 - m_nSize - 3;

	for( UINT n = m_uFirst; n < m_uCount; n++ ) {

		if( m_fEnable ) {

			if( n == m_uSelect ) {

				if( m_fFocus ) {

					pGDI->SetTextFore (m_colSelectFore);

					pGDI->SetTextBack (m_colSelectBack);

					pGDI->SetBrushFore(m_colSelectBack);
					}
				else {
					pGDI->SetTextFore (m_colTextFore);

					pGDI->SetTextBack (m_colTextGrey);

					pGDI->SetBrushFore(m_colTextGrey);
					}
				}
			else {
				pGDI->SetTextFore (m_colTextFore);

				pGDI->SetTextBack (m_colTextBack);

				pGDI->SetBrushFore(m_colTextBack);
				}
			}
		else {
			pGDI->SetTextFore (m_colTextGrey);

			pGDI->SetTextBack (m_colTextBack);

			pGDI->SetBrushFore(m_colTextBack);
			}

		// REV3 -- Clip text horizontally?

		WORD sText[maxString];

		MakeNarrow(sText, m_pList[n].m_sText);

		int cx = pGDI->GetTextWidth(sText);

		pGDI->TextOut(xp, yp + np, sText);

		pGDI->FillRect(xp, yp, x2, yp + np);

		pGDI->FillRect(xp, yp + cy + np, x2, yp + cy + 2 * np);

		pGDI->FillRect(xp + cx, yp + np, x2, yp + cy + np);

		if( yp + 2 * rp >= m_Rect.y2 - 1 ) {

			n += 1;

			break;
			}

		yp += rp;
		}

	m_uDrawn = n - m_uFirst;

	m_nPitch = cy;
	}

void CListBox::GrowList(UINT uAlloc)
{
	if( uAlloc > m_uAlloc ) {

		UINT   uTotal = 8 * ((uAlloc + 7) / 8);

		CEntry *pList = New CEntry [ uTotal ];

		if( m_pList) {

			UINT uCopy = m_uAlloc * sizeof(CEntry);

			memcpy(pList, m_pList, uCopy);

			delete [] m_pList;
			}

		m_pList  = pList;

		m_uAlloc = uTotal;
		}
	}

void CListBox::MakeIntegral(int cy, UINT uFlags)
{
	int np = 2;

	int rp = cy + 2 * np;

	int bp = 4;

	int ny = rp * ((m_Rect.y2 - m_Rect.y1 - bp) / rp) + bp;

	if( uFlags & lbsInverted ) {

		m_Rect.y1 = m_Rect.y2 - ny;

		return;
		}

	m_Rect.y2 = m_Rect.y1 + ny;
	}

// Sort Function

int CListBox::SortFunc(PCVOID p1, PCVOID p2)
{
	CEntry *e1 = (CEntry *) p1;
	
	CEntry *e2 = (CEntry *) p2;

	return wstrcmp(e1->m_sText, e2->m_sText);
	}

// End of File
