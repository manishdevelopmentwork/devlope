
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceAzure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientAzure.hpp"

#include "MqttClientOptionsAzure.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Azure Cloud Service
//

// Instantiator

IService * Create_CloudServiceAzure(void)
{
	return New CCloudServiceAzure;
	}

// Constructor

CCloudServiceAzure::CCloudServiceAzure(void)
{
	m_Name = "AZURE";
	}

// Initialization

void CCloudServiceAzure::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceAzure", pData);

	CCloudServiceCrimson::Load(pData);

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		if( m_pSet[n]->m_Array == 0 ) {

			m_pSet[n]->AddRewrite('[', '-');

			m_pSet[n]->AddRewrite(']', 0);
			}

		if( m_pSet[n]->m_Tree == 0 ) {

			m_pSet[n]->AddRewrite('.', '-');
			}
		}

	CMqttClientOptionsAzure *pOpts = New CMqttClientOptionsAzure;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory();

	m_pClient = New CMqttClientAzure(this, *pOpts);
	}

// Service ID

UINT CCloudServiceAzure::GetID(void)
{
	return 12;
	}

// End of File
