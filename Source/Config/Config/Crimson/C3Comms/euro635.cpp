
#include "intern.hpp"

#include "euro635.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Euro635 Comms Driver
//

// Instantiator

ICommsDriver *	Create_Euro635Driver(void)
{
	return New CEuro635Driver;
	}

// Constructor

CEuro635Driver::CEuro635Driver(void)
{
	m_wID		= 0x3392;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SSD Drives";
	
	m_DriverName	= "635/637 ";
	
	m_Version	= "1.10";
	
	m_ShortName	= "SSD Drives 635/637";

	AddSpaces();
	}

// Binding Control

UINT	CEuro635Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CEuro635Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Implementation

void	CEuro635Driver::AddSpaces(void)
{
	AddSpace( New CSpace( 1, "VAR",	 "Read/Write One Variable",			10, 0, 255, LL ) );
	AddSpace( New CSpace( 2, "FLG",	 "Read/Write One Flag",				10, 0, 255, BB ) );
	AddSpace( New CSpace( 3, "FGP",	 "Read group of 32 Flags (31-0, 63-32...)",	10, 0, 7,  LL ) );
	AddSpace( New CSpace( AN, "C0",	 "Disable 635/637",				10, 4, 0,  BB ) );
	AddSpace( New CSpace( AN, "C1",	 "Enable 635/637",				10, 5, 0,  BB ) );
	AddSpace( New CSpace( AN, "C2",	 "Reset 635/637",				10, 6, 0,  BB ) );
	AddSpace( New CSpace( AN, "C3",	 "Host Login",					10, 7, 0,  BB ) );
	AddSpace( New CSpace( AN, "C4",	 "Host Logout",					10, 8, 0,  BB ) );
	AddSpace( New CSpace( AN, "C5",	 "Transfer Data in EEPROM",			10, 9, 0,  BB ) );

	AddSpace( New CSpace( AN, "C6A", "Firmware Version - Byte 1",			10, 10, 0, YY ) );
	AddSpace( New CSpace( AN, "C6B", "Firmware Version - Byte 2",			10, 11, 0, YY ) );
	AddSpace( New CSpace( AN, "C6C", "Firmware Version - Byte 3",			10, 12, 0, YY ) );
	AddSpace( New CSpace( AN, "C6D", "Firmware Version - Byte 4",			10, 13, 0, YY ) );
	AddSpace( New CSpace( AN, "C6E", "Firmware Version - Byte 5",			10, 14, 0, YY ) );
	AddSpace( New CSpace( AN, "C6F", "Firmware Version - Byte 6",			10, 15, 0, YY ) );
	AddSpace( New CSpace( AN, "C6G", "Firmware Version - Byte 7",			10, 16, 0, YY ) );
	AddSpace( New CSpace( AN, "C6H", "Firmware Version - Byte 8",			10, 17, 0, YY ) );
	AddSpace( New CSpace( AN, "C6I", "Firmware Version - Byte 9",			10, 18, 0, YY ) );
	AddSpace( New CSpace( AN, "C6J", "Firmware Version - Byte 10",			10, 19, 0, YY ) );

	AddSpace( New CSpace( AN, "C6K", "Firmware Version - Byte 11",			10, 20, 0, YY ) );
	AddSpace( New CSpace( AN, "C6L", "Firmware Version - Byte 12",			10, 21, 0, YY ) );
	AddSpace( New CSpace( AN, "C7A", "Diagnosis Information - Error Word",		10, 22, 0, WW ) );
	AddSpace( New CSpace( AN, "C7B", "Diagnosis Information - Status Word",		10, 23, 0, WW ) );
	AddSpace( New CSpace( AN, "C7C", "Diagnosis Information - Serial Status",	10, 24, 0, WW ) );
	AddSpace( New CSpace( AN, "C7D", "Diagnosis Information - Operating Mode",	10, 25, 0, YY ) );
	AddSpace( New CSpace( AN, "C7E", "Diagnosis Information - Input Definition",	10, 26, 0, YY ) );
	AddSpace( New CSpace( AN, "C7F", "Diagnosis Information - Actual Speed",	10, 27, 0, WW ) );
	AddSpace( New CSpace( AN, "C7G", "Diagnosis Information - Input State",		10, 28, 0, YY ) );
	AddSpace( New CSpace( AN, "C7H", "Diagnosis Information - Output State",	10, 29, 0, YY ) );

	AddSpace( New CSpace( AN, "C7I", "Diagnosis Information - Actual Position",	10, 30, 0, LL ) );
	AddSpace( New CSpace( AN, "C7J", "Diagnosis Information - Reserved",		10, 31, 0, LL ) );
	AddSpace( New CSpace( AN, "C13", "Set BIAS Process Pointer",			10, 32, 0, WW ) );
	AddSpace( New CSpace( AN, "C14", "Network Axis Number",				10, 33, 0, YY ) );

	AddSpace( New CSpace( AN, "C16A", "Diagnostics - Error Word",			10, 221, 0, WW ) );
	AddSpace( New CSpace( AN, "C16B", "Diagnostics - Status Word 1",		10, 222, 0, WW ) );
	AddSpace( New CSpace( AN, "C16C", "Diagnostics - Status Word 2",		10, 223, 0, WW ) );
	AddSpace( New CSpace( AN, "C16D", "Diagnostics - Operating Word",		10, 224, 0, WW ) );
	AddSpace( New CSpace( AN, "C16E", "Diagnostics - Speed",			10, 225, 0, WW ) );
	AddSpace( New CSpace( AN, "C16F", "Diagnostics - I2T Motor",			10, 226, 0, WW ) );
	AddSpace( New CSpace( AN, "C16G", "Diagnostics - Current",			10, 227, 0, WW ) );
	AddSpace( New CSpace( AN, "C16H", "Diagnostics - UCC",				10, 228, 0, WW ) );
	AddSpace( New CSpace( AN, "C16I", "Diagnostics - I2T Drive",			10, 229, 0, WW ) );
	AddSpace( New CSpace( AN, "C16J", "Diagnostics - Brake Circuit Power",		10, 230, 0, WW ) );
	AddSpace( New CSpace( AN, "C16K", "Diagnostics - Drive Temperature",		10, 231, 0, WW ) );
	AddSpace( New CSpace( AN, "C16L", "Diagnostics - Input/Output State",		10, 232, 0, WW ) );
	AddSpace( New CSpace( AN, "C16M", "Diagnostics - Feedback",			10, 233, 0, WW ) );
	AddSpace( New CSpace( AN, "C16N", "Diagnostics - Analog Set Point",		10, 234, 0, WW ) );
	AddSpace( New CSpace( AN, "C16O", "Diagnostics - Motor Temperature",		10, 235, 0, WW ) );
	AddSpace( New CSpace( AN, "C16P", "Diagnostics - Position",			10, 236, 0, LL ) );
	AddSpace( New CSpace( AN, "C16Q", "Diagnostics - Calculation Time",		10, 237, 0, LL ) );


	AddSpace( New CSpace( AN, "C17A", "Diagnostics - Drive Type",			10, 238, 0, WW ) );
	AddSpace( New CSpace( AN, "C17B", "Diagnostics - Serial Number",		10, 239, 0, WW ) );
	AddSpace( New CSpace( AN, "C17C", "Diagnostics - Production Data",		10, 240, 0, WW ) );
	AddSpace( New CSpace( AN, "C17D", "Diagnostics - Repair Number",		10, 241, 0, WW ) );
	AddSpace( New CSpace( AN, "C17E", "Diagnostics - I2T Motor Scaling Factor",	10, 242, 0, WW ) );
	AddSpace( New CSpace( AN, "C17F", "Diagnostics - Maximum Current",		10, 243, 0, WW ) );
	AddSpace( New CSpace( AN, "C17G", "Diagnostics - Current Scaling Factor",	10, 244, 0, WW ) );
	AddSpace( New CSpace( AN, "C17H", "Diagnostics - I2T Drive Scaling Factor",	10, 245, 0, WW ) );
	AddSpace( New CSpace( AN, "C17I", "Diagnostics - Brake Circuit Rated Power",	10, 246, 0, WW ) );
	AddSpace( New CSpace( AN, "C17J", "Diagnostics - Ucc Scaling Factor",		10, 247, 0, WW ) );
	AddSpace( New CSpace( AN, "C17K", "Diagnostics - Temperature Scaling Factor",	10, 248, 0, WW ) );
	AddSpace( New CSpace( AN, "C17L", "Diagnostics - Configuration",		10, 249, 0, WW ) );
	AddSpace( New CSpace( AN, "C17M", "Diagnostics - Maximum Speed",		10, 250, 0, WW ) );
	AddSpace( New CSpace( AN, "C17N", "Diagnostics - Speed Scaling Factor",		10, 251, 0, LL ) );

	AddSpace( New CSpace( AN, "C22A","Read EEPROM Pointer - Pointer",		10, 34, 0, YY ) );
	AddSpace( New CSpace( AN, "C22B","Read EEPROM Pointer - State",			10, 35, 0, YY ) );
	AddSpace( New CSpace( AN, "C23A","Positioning Command - Mode",			10, 36, 0, YY ) );
	AddSpace( New CSpace( AN, "C23B","Positioning Command - Speed",			10, 37, 0, WW ) );
	AddSpace( New CSpace( AN, "C23C","Positioning Command - Acceleration",		10, 38, 0, WW ) );
	AddSpace( New CSpace( AN, "C23D","Positioning Command - Deceleration",		10, 39, 0, WW ) );

	AddSpace( New CSpace( AN, "C23E","Positioning Command - Reached Window",	10, 40, 0, WW ) );
	AddSpace( New CSpace( AN, "C23F","Positioning Command - At Increments",		10, 41, 0, LL ) );
	AddSpace( New CSpace( AN, "C23", "Positioning Command - EXECUTE WRITE",		10, 42, 0, BB ) );
	AddSpace( New CSpace( AN, "C33A","Read BIAS Diagnosis - BIAS Pointer",		10, 43, 0, WW ) );
	AddSpace( New CSpace( AN, "C33B","Read BIAS Diagnosis - PLC Pointer",		10, 44, 0, WW ) );
	AddSpace( New CSpace( AN, "C33C","Read BIAS Diagnosis - Block Number",		10, 45, 0, WW ) );
	AddSpace( New CSpace( AN, "C33D","Read BIAS Diagnosis - BIAS Stack",		10, 46, 0, WW ) );
	AddSpace( New CSpace( AN, "C33E","Read BIAS Diagnosis - Wait Time",		10, 47, 0, WW ) );
	AddSpace( New CSpace( AN, "C33F","Read BIAS Diagnosis - BIAS Status",		10, 48, 0, WW ) );
	AddSpace( New CSpace( AN, "C33G","Read BIAS Diagnosis - PLC Status",		10, 49, 0, WW ) );

	AddSpace( New CSpace( AN, "C33H","Read BIAS Diagnosis - PLC Stack",		10, 50, 0, WW ) );
	AddSpace( New CSpace( AN, "C33I","Read BIAS Diagnosis - Position 1",		10, 51, 0, LL ) );
	AddSpace( New CSpace( AN, "C33J","Read BIAS Diagnosis - Position 2",		10, 52, 0, LL ) );
	AddSpace( New CSpace( AN, "C33K","Read BIAS Diagnosis - Position 3",		10, 53, 0, LL ) );
	AddSpace( New CSpace( AN, "C33L","Read BIAS Diagnosis - Reserve",		10, 54, 0, LL ) );
	AddSpace( New CSpace( AN, "C36", "Start Position Set - Block Select",		10, 55, 0, YY ) );
	AddSpace( New CSpace( AN, "C40A","Input/Output Diagnosis - ExBus2",		10, 56, 0, WW ) );
	AddSpace( New CSpace( AN, "C40B","Input/Output Diagnosis - Ext_E-100",		10, 57, 0, WW ) );
	AddSpace( New CSpace( AN, "C40C","Input/Output Diagnosis - Ext_A-100",		10, 58, 0, WW ) );
	AddSpace( New CSpace( AN, "C40D","Input/Output Diagnosis - Ext_E-200",		10, 59, 0, WW ) );

	AddSpace( New CSpace( AN, "C40E","Input/Output Diagnosis - Ext_A-200",		10, 60, 0, WW ) );
	AddSpace( New CSpace( AN, "C40F","Input/Output Diagnosis - Reserve",		10, 61, 0, WW ) );
	AddSpace( New CSpace( AN, "C40G","Input/Output Diagnosis - Reserve",		10, 62, 0, WW ) );
	AddSpace( New CSpace( AN, "C40H","Input/Output Diagnosis - Reserve",		10, 63, 0, WW ) );
	AddSpace( New CSpace( AN, "C47", "Serial Speed Setpoint",			10, 64, 0, WW ) );
	AddSpace( New CSpace( AN, "C62", "Rated Current of Motor",			10, 65, 0, WW ) );
	AddSpace( New CSpace( AN, "C65A","Configuration - Axis",			10, 66, 0, YY ) );
	AddSpace( New CSpace( AN, "C65B","Configuration - Configuration",		10, 67, 0, WW ) );
	AddSpace( New CSpace( AN, "C65C","Configuration - Operating Mode",		10, 68, 0, YY ) );
	AddSpace( New CSpace( AN, "C65D","Configuration - Input Definition",		10, 69, 0, YY ) );

	AddSpace( New CSpace( AN, "C65E","Configuration - Rated Current",		10, 70, 0, WW ) );
	AddSpace( New CSpace( AN, "C65F","Configuration - Pole Pair Number",		10, 71, 0, WW ) );
	AddSpace( New CSpace( AN, "C65G","Configuration - EMC at Volt",			10, 72, 0, WW ) );
	AddSpace( New CSpace( AN, "C65H","Configuration - Inductivity",			10, 73, 0, WW ) );
	AddSpace( New CSpace( AN, "C65I","Configuration - Resistance",			10, 74, 0, WW ) );
	AddSpace( New CSpace( AN, "C65J","Configuration - Monitoring Time",		10, 75, 0, WW ) );
	AddSpace( New CSpace( AN, "C65K","Configuration - T1",				10, 76, 0, WW ) );
	AddSpace( New CSpace( AN, "C65L","Configuration - T2",				10, 77, 0, WW ) );
	AddSpace( New CSpace( AN, "C65M","Configuration - PTC",				10, 78, 0, WW ) );
	AddSpace( New CSpace( AN, "C65N","Configuration - Deceleration",		10, 79, 0, WW ) );

	AddSpace( New CSpace( AN, "C65O","Configuration - Ucc Low",			10, 80, 0, WW ) );
	AddSpace( New CSpace( AN, "C65P","Configuration - Ucc Ballast",			10, 81, 0, WW ) );
	AddSpace( New CSpace( AN, "C65Q","Configuration - Ballast Resistance",		10, 82, 0, WW ) );
	AddSpace( New CSpace( AN, "C65R","Configuration - Ballast Power",		10, 83, 0, WW ) );
	AddSpace( New CSpace( AN, "C65", "Configuration - EXECUTE WRITE",		10, 84, 0, BB ) );
	AddSpace( New CSpace( AN, "CF65","Configuration - Read Data from Cache",	10, 85, 0, BB ) );
	AddSpace( New CSpace( AN, "CT65","Configuration - Read Data into Cache",	10, 86, 0, BB ) );
	AddSpace( New CSpace( AN, "C66A","Speed Controller - List Place P",		10, 87, 0, WW ) );
	AddSpace( New CSpace( AN, "C66B","Speed Controller - List Place I",		10, 88, 0, WW ) );
	AddSpace( New CSpace( AN, "C66C","Speed Controller - Max Current",		10, 89, 0, WW ) );

	AddSpace( New CSpace( AN, "C66D","Speed Controller - Setpoint 0",		10, 90, 0, WW ) );
	AddSpace( New CSpace( AN, "C66E","Speed Controller - Integrator",		10, 91, 0, WW ) );
	AddSpace( New CSpace( AN, "C66F","Speed Controller - Speed Norming",		10, 92, 0, WW ) );
	AddSpace( New CSpace( AN, "C66G","Speed Controller - Current Norming",		10, 93, 0, WW ) );
	AddSpace( New CSpace( AN, "C66H","Speed Controller - Norming 1",		10, 94, 0, WW ) );
	AddSpace( New CSpace( AN, "C66I","Speed Controller - Norming 2",		10, 95, 0, WW ) );
	AddSpace( New CSpace( AN, "C66J","Speed Controller - Norming Limit",		10, 96, 0, WW ) );
	AddSpace( New CSpace( AN, "C66K","Speed Controller - Offset Correct",		10, 97, 0, WW ) );
	AddSpace( New CSpace( AN, "C66", "Speed Controller - EXECUTE WRITE",		10, 98, 0, BB ) );
	AddSpace( New CSpace( AN, "CF66","Speed Controller - Read Data from Cache",	10, 99, 0, BB ) );

	AddSpace( New CSpace( AN, "CT66","Speed Controller - Read Data into Cache",	10, 100, 0, BB ) );
	AddSpace( New CSpace( AN, "C67A","Current Controller - List Place P",		10, 101, 0, WW ) );
	AddSpace( New CSpace( AN, "C67B","Current Controller - List Place I",		10, 102, 0, WW ) );
	AddSpace( New CSpace( AN, "C67C","Current Controller - Reserved",		10, 103, 0, WW ) );
	AddSpace( New CSpace( AN, "C67D","Current Controller - Reserved",		10, 104, 0, WW ) );
	AddSpace( New CSpace( AN, "C67E","Current Controller - Reserved",		10, 105, 0, WW ) );
	AddSpace( New CSpace( AN, "C67F","Current Controller - Reserved",		10, 106, 0, WW ) );
	AddSpace( New CSpace( AN, "C67G","Current Controller - Resolver",		10, 107, 0, WW ) );
	AddSpace( New CSpace( AN, "C67H","Current Controller - Ucc OV",			10, 108, 0, WW ) );
	AddSpace( New CSpace( AN, "C67I","Current Controller - Reserved",		10, 109, 0, WW ) );

	AddSpace( New CSpace( AN, "C67", "Current Controller - EXECUTE WRITE",		10, 110, 0, BB ) );
	AddSpace( New CSpace( AN, "CF67","Current Controller - Read Data from Cache",	10, 111, 0, BB ) );
	AddSpace( New CSpace( AN, "CT65","Current Controller - Read Data into Cache",	10, 112, 0, BB ) );
	AddSpace( New CSpace( AN, "C68A","Position Controller - Speed",			10, 113, 0, WW ) );
	AddSpace( New CSpace( AN, "C68B","Position Controller - Acceleration",		10, 114, 0, WW ) );
	AddSpace( New CSpace( AN, "C68C","Position Controller - Deceleration",		10, 115, 0, WW ) );
	AddSpace( New CSpace( AN, "C68D","Position Controller - Position Reached",	10, 116, 0, WW ) );
	AddSpace( New CSpace( AN, "C68E","Position Controller - P",			10, 117, 0, WW ) );
	AddSpace( New CSpace( AN, "C68F","Position Controller - I",			10, 118, 0, WW ) );
	AddSpace( New CSpace( AN, "C68G","Position Controller - Reserved",		10, 119, 0, WW ) );

	AddSpace( New CSpace( AN, "C68", "Position Controller - EXECUTE WRITE",		10, 120, 0, BB ) );
	AddSpace( New CSpace( AN, "CF68","Position Controller - Read Data from Cache",	10, 121, 0, BB ) );
	AddSpace( New CSpace( AN, "CT68","Position Controller - Read Data into Cache",	10, 122, 0, BB ) );
	AddSpace( New CSpace( AN, "C69A","Position Set - Position Set Number",		10, 123, 0, YY ) );
	AddSpace( New CSpace( AN, "C69B","Position Set - Command Mode",			10, 124, 0, YY ) );
	AddSpace( New CSpace( AN, "C69C","Position Set - Speed",			10, 125, 0, WW ) );
	AddSpace( New CSpace( AN, "C69D","Position Set - Acceleration",			10, 126, 0, WW ) );
	AddSpace( New CSpace( AN, "C69E","Position Set - Deceleration",			10, 127, 0, WW ) );
	AddSpace( New CSpace( AN, "C69F","Position Set - Position Reached",		10, 128, 0, WW ) );
	AddSpace( New CSpace( AN, "C69G","Position Set - Nominal Position",		10, 129, 0, LL ) );

	AddSpace( New CSpace( AN, "C69", "Position Set - EXECUTE WRITE",		10, 130, 0, BB ) );
	AddSpace( New CSpace( AN, "CF69","Position Set - Read Data from Cache",		10, 131, 0, BB ) );
	AddSpace( New CSpace( AN, "CT69","Position Set - Read Data into Cache",		10, 132, 0, BB ) );
	AddSpace( New CSpace( AN, "C72A","Cam Profile - Parameter Set Number",		10, 133, 0, YY ) );
	AddSpace( New CSpace( AN, "C72B","Cam Profile - Easyrider Reserved",		10, 134, 0, YY ) );
	AddSpace( New CSpace( AN, "C72C","Cam Profile - Corrections",			10, 135, 0, YY ) );
	AddSpace( New CSpace( AN, "C72D","Cam Profile - Profile Points",		10, 136, 0, WW ) );
	AddSpace( New CSpace( AN, "C72E","Cam Profile - Address",			10, 137, 0, WW ) );
	AddSpace( New CSpace( AN, "C72F","Cam Profile - Reserved",			10, 138, 0, WW ) );
	AddSpace( New CSpace( AN, "C72G","Cam Profile - Correct 1",			10, 139, 0, WW ) );

	AddSpace( New CSpace( AN, "C72H","Cam Profile - Correct 2",			10, 140, 0, WW ) );
	AddSpace( New CSpace( AN, "C72I","Cam Profile - Correct 3",			10, 141, 0, WW ) );
	AddSpace( New CSpace( AN, "C72J","Cam Profile - Correct 4",			10, 142, 0, WW ) );
	AddSpace( New CSpace( AN, "C72K","Cam Profile - Correct 5",			10, 143, 0, WW ) );
	AddSpace( New CSpace( AN, "C72L","Cam Profile - Correct 6",			10, 144, 0, WW ) );
	AddSpace( New CSpace( AN, "C72M","Cam Profile - Correct 7",			10, 145, 0, WW ) );
	AddSpace( New CSpace( AN, "C72N","Cam Profile - Correct 8",			10, 146, 0, WW ) );
	AddSpace( New CSpace( AN, "C72O","Cam Profile - Correct 9",			10, 147, 0, WW ) );
	AddSpace( New CSpace( AN, "C72P","Cam Profile - Correct 10",			10, 148, 0, WW ) );
	AddSpace( New CSpace( AN, "C72Q","Cam Profile - Master Stroke",			10, 149, 0, LL ) );

	AddSpace( New CSpace( AN, "C72R","Cam Profile - Slave Stroke",			10, 150, 0, LL ) );
	AddSpace( New CSpace( AN, "C72S","Cam Profile - Reserved Bytes 1-4",		10, 151, 0, LL ) );
	AddSpace( New CSpace( AN, "C72T","Cam Profile - Reserved Bytes 5-8",		10, 152, 0, LL ) );
	AddSpace( New CSpace( AN, "C72U","Cam Profile - Reserved Bytes 9-12",		10, 153, 0, LL ) );
	AddSpace( New CSpace( AN, "C72V","Cam Profile - Reserved Bytes 13-16",		10, 154, 0, LL ) );
	AddSpace( New CSpace( AN, "C72W","Cam Profile - Synchronmode",			10, 155, 0, YY ) );
	AddSpace( New CSpace( AN, "C72X","Cam Profile - Reserved Bytes 1-4",		10, 156, 0, LL ) );
	AddSpace( New CSpace( AN, "C72Y","Cam Profile - Reserved Bytes 5-8",		10, 157, 0, LL ) );
	AddSpace( New CSpace( AN, "C72Z","Cam Profile - Reserved Bytes 9-11",		10, 158, 0, LL ) );
	AddSpace( New CSpace( AN, "C72", "Cam Profile - EXECUTE WRITE",			10, 159, 0, BB ) );

	AddSpace( New CSpace( AN, "CF72","Cam Profile - Read Data from Cache",		10, 160, 0, BB ) );
	AddSpace( New CSpace( AN, "CT72","Cam Profile - Read Data into Cache",		10, 161, 0, BB ) );
	AddSpace( New CSpace( AN, "C73A","Profile Point Block - Set Number",		10, 162, 0, YY ) );
	AddSpace( New CSpace( AN, "C73B","Profile Point 1 - Bytes 1-4",			10, 163, 0, LL ) );
	AddSpace( New CSpace( AN, "C73C","Profile Point 1 - Bytes 5-8",			10, 164, 0, LL ) );
	AddSpace( New CSpace( AN, "C73D","Profile Point 2 - Bytes 1-4",			10, 165, 0, LL ) );
	AddSpace( New CSpace( AN, "C73E","Profile Point 2 - Bytes 5-8",			10, 166, 0, LL ) );
	AddSpace( New CSpace( AN, "C73F","Profile Point 3 - Bytes 1-4",			10, 167, 0, LL ) );
	AddSpace( New CSpace( AN, "C73G","Profile Point 3 - Bytes 5-8",			10, 168, 0, LL ) );
	AddSpace( New CSpace( AN, "C73H","Profile Point 4 - Bytes 1-4",			10, 169, 0, LL ) );

	AddSpace( New CSpace( AN, "C73I","Profile Point 4 - Bytes 5-8",			10, 170, 0, LL ) );
	AddSpace( New CSpace( AN, "C73J","Profile Point 5 - Bytes 1-4",			10, 171, 0, LL ) );
	AddSpace( New CSpace( AN, "C73K","Profile Point 5 - Bytes 5-8",			10, 172, 0, LL ) );
	AddSpace( New CSpace( AN, "C73L","Profile Point 6 - Bytes 1-4",			10, 173, 0, LL ) );
	AddSpace( New CSpace( AN, "C73M","Profile Point 6 - Bytes 5-8",			10, 174, 0, LL ) );
	AddSpace( New CSpace( AN, "C73N","Profile Point 7 - Bytes 1-4",			10, 175, 0, LL ) );
	AddSpace( New CSpace( AN, "C73O","Profile Point 7 - Bytes 5-8",			10, 176, 0, LL ) );
	AddSpace( New CSpace( AN, "C73P","Profile Point 8 - Bytes 1-4",			10, 177, 0, LL ) );
	AddSpace( New CSpace( AN, "C73Q","Profile Point 8 - Bytes 5-8",			10, 178, 0, LL ) );
	AddSpace( New CSpace( AN, "C73", "Profile Point Block - EXECUTE WRITE",		10, 179, 0, BB ) );

	AddSpace( New CSpace( AN, "CF73","Profile Point - Read Data from Cache",	10, 180, 0, BB ) );
	AddSpace( New CSpace( AN, "CT73","Profile Point - Read Data into Cache",	10, 181, 0, BB ) );
	AddSpace( New CSpace( AN, "C74A","I/O Definitions - I - X10.2",			10, 182, 0, YY ) );
	AddSpace( New CSpace( AN, "C74B","I/O Definitions - I - X10.4",			10, 183, 0, YY ) );
	AddSpace( New CSpace( AN, "C74C","I/O Definitions - I - X10.11",		10, 184, 0, YY ) );
	AddSpace( New CSpace( AN, "C74D","I/O Definitions - I - X10.14",		10, 185, 0, YY ) );
	AddSpace( New CSpace( AN, "C74E","I/O Definitions - I - X10.15",		10, 186, 0, YY ) );
	AddSpace( New CSpace( AN, "C74F","I/O Definitions - I - X10.24",		10, 187, 0, YY ) );
	AddSpace( New CSpace( AN, "C74G","I/O Definitions - I - X10.25",		10, 188, 0, YY ) );
	AddSpace( New CSpace( AN, "C74H","I/O Definitions - O - X10.12",		10, 189, 0, YY ) );

	AddSpace( New CSpace( AN, "C74I","I/O Definitions - O - X10.13",		10, 190, 0, YY ) );
	AddSpace( New CSpace( AN, "C74J","I/O Definitions - O - X10.20",		10, 191, 0, YY ) );
	AddSpace( New CSpace( AN, "C74K","I/O Definitions - O - X10.23",		10, 192, 0, YY ) );
	AddSpace( New CSpace( AN, "C74", "I/O Definitions - EXECUTE WRITE",		10, 193, 0, BB ) );
	AddSpace( New CSpace( AN, "CF74","I/O Definitions - Read Data from Cache",	10, 194, 0, BB ) );
	AddSpace( New CSpace( AN, "CT74","I/O Definitions - Read Data into Cache",	10, 195, 0, BB ) );
	AddSpace( New CSpace( AN, "C76A","BIAS Program - Set Number",			10, 196, 0, WW ) );
	AddSpace( New CSpace( AN, "C76B","BIAS Program - command code",			10, 197, 0, YY ) );
	AddSpace( New CSpace( AN, "C76C","BIAS Program - Byte 1",			10, 198, 0, YY ) );
	AddSpace( New CSpace( AN, "C76D","BIAS Program - Byte 2",			10, 199, 0, YY ) );

	AddSpace( New CSpace( AN, "C76E","BIAS Program - Byte 3",			10, 200, 0, YY ) );
	AddSpace( New CSpace( AN, "C76F","BIAS Program - Byte 4",			10, 201, 0, YY ) );
	AddSpace( New CSpace( AN, "C76G","BIAS Program - Byte 5",			10, 202, 0, YY ) );
	AddSpace( New CSpace( AN, "C76H","BIAS Program - Byte 6",			10, 203, 0, YY ) );
	AddSpace( New CSpace( AN, "C76I","BIAS Program - Byte 7",			10, 204, 0, YY ) );
	AddSpace( New CSpace( AN, "C76", "BIAS Program - EXECUTE WRITE",		10, 205, 0, BB ) );
	AddSpace( New CSpace( AN, "CF76","BIAS Program - Read Data from Cache",		10, 206, 0, BB ) );
	AddSpace( New CSpace( AN, "CT76","BIAS Program - Read Data into Cache",		10, 207, 0, BB ) );
	AddSpace( New CSpace( AN, "C78A","Extended Control - V-Gain",			10, 208, 0, WW ) );
	AddSpace( New CSpace( AN, "C78B","Extended Control - X40-Mode",			10, 209, 0, YY ) );

	AddSpace( New CSpace( AN, "C78C","Extended Control - X40-Resolution",		10, 210, 0, YY ) );
	AddSpace( New CSpace( AN, "C78D","Extended Control - Position Time",		10, 211, 0, WW ) );
	AddSpace( New CSpace( AN, "C78E","Extended Control - Reserved",			10, 212, 0, WW ) );
	AddSpace( New CSpace( AN, "C78F","Extended Control - Trail Window",		10, 213, 0, WW ) );
	AddSpace( New CSpace( AN, "C78G","Extended Control - Trail Fault",		10, 214, 0, YY ) );
	AddSpace( New CSpace( AN, "C78H","Extended Control - N-Filter",			10, 215, 0, YY ) );
	AddSpace( New CSpace( AN, "C78I","Extended Control - Reserved",			10, 216, 0, WW ) );
	AddSpace( New CSpace( AN, "C78", "Extended Control - EXECUTE WRITE",		10, 217, 0, BB ) );
	AddSpace( New CSpace( AN, "CF78","Extended Control - Read Data from Cache",	10, 218, 0, BB ) );
	AddSpace( New CSpace( AN, "CT78","Extended Control - Read Data into Cache",	10, 219, 0, BB ) );
	AddSpace( New CSpace( AN, "ERR", "Command could not be executed",		10, 220, 0, BB ) );
	}

// End of File
