/*****************************************************************************
T5RedSrv.c : Redundancy - server
(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"
#include "t5redapi.h"

/****************************************************************************/

static T5_BOOL bTfrTable[T5MAX_TABLE] = {
    TRUE,  /* T5TB_STATUS    */
    FALSE, /* T5TB_LOG       */
    FALSE, /* T5TB_EXEC      */
    FALSE, /* T5TB_PRIVATE   */
    TRUE,  /* T5TB_PROG      */
    TRUE,  /* T5TB_LOCK      */
    TRUE,  /* T5TB_DATA8     */
    TRUE,  /* T5TB_DATA16    */
    TRUE,  /* T5TB_DATA32    */
    TRUE,  /* T5TB_DATA64    */
    TRUE,  /* T5TB_TIME      */
    FALSE, /* T5TB_ACTIME    */
    FALSE, /* T5TB_FBCLASS   */
    FALSE, /* T5TB_FBINST    */
    TRUE,  /* T5TB_FBCDATA   */
    TRUE,  /* T5TB_FBIDATA   */
    FALSE, /* T5TB_FCLASS    */
    FALSE, /* T5TB_STRING    */
    TRUE,  /* T5TB_STRBUF    */
    FALSE, /* T5TB_SYSRSC    */
    TRUE,  /* T5TB_STEP      */
    TRUE,  /* T5TB_TRANS     */
    FALSE, /* T5TB_UNRES     */
    TRUE,  /* T5TB_IODATA    */
#ifdef T5RED_REPLICATEVSI
    TRUE, /* T5TB_VARMAP    */
#else /*T5RED_REPLICATEVSI*/
    FALSE, /* T5TB_VARMAP    */
#endif /*T5RED_REPLICATEVSI*/
    FALSE, /* T5TB_HASH      */
    FALSE, /* T5TB_XV        */
    FALSE, /* T5TB_BSAMPLING */
    FALSE, /* T5TB_TASK      */
    FALSE, /* T5TB_CALLSTACK */
    FALSE, /* T5TB_MBC       */
    FALSE, /* T5TB_ASI       */
    FALSE, /* T5TB_EACHANGE  */
    FALSE, /* T5TB_EACNX     */
    FALSE, /* T5TB_EAEXT     */
    TRUE,  /* T5TB_CTSEG     */
    FALSE, /* T5TB_CANPORT   */
    FALSE, /* T5TB_CANMSG    */
    FALSE, /* T5TB_CANVAR    */
    TRUE   /* T5TB_DDKC      */
};

/****************************************************************************/
/* main frame: {si} s=state=?|M|S i=info style=0|... */

/****************************************************************************/

#define SRV_RUN      1   /* hand shaking: VM is running */
#define SRV_KILLING  2   /* hand shaking: VM is asked to stop */
#define SRV_KILLED   0   /* hand shaking: VM has finished */

/****************************************************************************/

typedef struct
{
    T5_DWORD dwNbLost;
    T5_DWORD dwSwitchTime;
    T5_BOOL  bRedSrv;
    T5_BOOL  bRedCliHere;
    T5_BOOL  bRedCliOK;
    T5_BOOL  bSwitchScan;
} T5STR_REDSRVSTATE;

typedef T5STR_REDSRVSTATE *T5PTR_REDSRVSTATE;

typedef struct
{
    T5_PTR     pUtiData;
    T5_DWORD   dwVMID;
    T5_PTBYTE  pDB;
    T5_PTBYTE  pDBupd;
    T5_DWORD   dwSizing[T5MAX_TABLE];
    T5_DWORD   dwOffset[T5MAX_TABLE];
    T5_SOCKET  sockListen;
    T5_SOCKET  sockClient;
    T5_DWORD   dwDBSize;
    T5_DWORD   dwNbLost;
    T5_DWORD   dwSwitchTime;
    T5PTR_REDSRVSTATE pFBData;
    T5_BOOL    bCodeChanged;
    T5_BOOL    bBusy;
    T5_BOOL    bRunning;
    T5_BOOL    bSendingDB;
    T5_BOOL    bClientReady;
    T5_BOOL    bResendAll;
    T5_BYTE    srvAlive;
    T5_BOOL    bDelayedSwitch;
    T5_BOOL    bSwitchScan;
    T5_CHAR    bState;
} str_T5redsrv;

/****************************************************************************/

void T5RedSrv_Thread (T5_PTR pArgs);

static void _T5RedSrv_SendState (T5_PTR pSrvData);
static T5_BOOL _T5RedSrv_RecieveAck (T5_PTR pSrvData);
static void _OnClientHere (T5_PTR pSrvData);
static void _OnClientLost (T5_PTR pSrvData);
static void _GiveCodeChecksum (T5_PTR pSrvData);
static void _GiveCode (T5_PTR pSrvData);
static void _SendDB (T5_PTR pSrvData, T5_PTCHAR szClientState);
static void _SendPacket (T5_PTR pSrvData, T5_DWORD dwSize,
                         T5_PTR pSend, T5_PTBOOL pbFail);

/****************************************************************************/

void T5RedSrv_RegisterSwitch (T5_PTR pSrvData, T5_DWORD dwSwitchTime)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    pSrv->dwSwitchTime = dwSwitchTime;
    pSrv->bSwitchScan = TRUE;
}

void T5RedSrv_AllocDBMem (T5_PTR pSrvData, T5_DWORD dwSize, T5_PTR pDBcur)
{
    T5_WORD i;
    T5_DWORD dwTab, dwTotal;
    str_T5redsrv *pSrv;
    T5PTR_DB pCur;
    T5PTR_DBFBCLASS pClass;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    pCur = (T5PTR_DB)pDBcur;

    if (pSrv->pDB)
        T5RedSys_Free (pSrv->pDB);
    pSrv->pDB = T5RedSys_Malloc (dwSize);

    if (pSrv->pDBupd)
        T5RedSys_Free (pSrv->pDBupd);
    pSrv->pDBupd = T5RedSys_Malloc (dwSize);
    T5_MEMSET (pSrv->pDBupd, 0, dwSize);

    pSrv->dwDBSize = dwSize;
    T5_MEMCPY (pSrv->pDB, pDBcur, T5MAX_TABLE * sizeof (T5STR_DBTABLE));

	dwTotal = 0;
    for (i=0; i<T5MAX_TABLE; i++)
    {
        pSrv->dwOffset[i] = (T5_DWORD)(pCur[i].pData) - (T5_DWORD)pCur;
        if (bTfrTable[i] == FALSE)
            pSrv->dwSizing[i] = 0L;
        else
        {
		    dwTab = pCur[i].dwRawSize;
		    if (dwTab == 0)
                dwTab = (pCur[i].wNbItemAlloc * pCur[i].wSizeOf);
		    pSrv->dwSizing[i] = T5_ALIGNED(dwTab);
            T5_MEMSET (pSrv->pDB + pSrv->dwOffset[i], 0, pSrv->dwSizing[i]);
			dwTotal += pSrv->dwSizing[i];
        }
    }

    pClass = T5GET_DBFBCLASS(pCur);
    for (i=0; i<pCur[T5TB_FBCLASS].wNbItemUsed; i++, pClass++)
    {
        if (T5_STRCMP (pClass->szName, "GETACTIVERTSTAT") == 0)
            pSrv->pFBData = (T5PTR_REDSRVSTATE)(pClass->pData);
        else if (T5_STRCMP (pClass->szName, "ACTIVERTSWITCH") == 0)
            *(str_T5redsrv **)(pClass->pData) = pSrv;
    }

    pSrv->dwNbLost = 0L;
    pSrv->bResendAll = TRUE;

	T5REDLOG ("S> Exchange data = %lu bytes", dwTotal);
}

void T5RedSrv_ReleaseDBMem (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    while (pSrv->sockClient != T5_INVSOCKET && pSrv->pDB != NULL && pSrv->bSendingDB)
        T5RedSys_Sleep (10);

    if (pSrv->pDB)
        T5RedSys_Free (pSrv->pDB);
    if (pSrv->pDBupd)
        T5RedSys_Free (pSrv->pDBupd);
    pSrv->pDB = NULL;
    pSrv->pDBupd = NULL;
    pSrv->dwDBSize = 0;
    pSrv->pFBData = NULL;
}

void T5RedSrv_StartExchange (T5_PTR pSrvData, T5_PTR pDBcur)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    if (pSrv->pFBData != NULL)
    {
        pSrv->pFBData->bRedSrv = TRUE;
        pSrv->pFBData->bRedCliHere = (pSrv->sockClient != T5_INVSOCKET);
        pSrv->pFBData->bRedCliOK = pSrv->bClientReady;
        pSrv->pFBData->bSwitchScan = pSrv->bSwitchScan;
        pSrv->pFBData->dwNbLost = pSrv->dwNbLost;
        pSrv->pFBData->dwSwitchTime = pSrv->dwSwitchTime;
    }

    if (pSrv->sockClient == T5_INVSOCKET || pSrv->pDB == NULL || pSrv->bSendingDB)
        return;

    if (!pSrv->bClientReady)
        return;

    T5_MEMCPY (pSrv->pDBupd, pDBcur, pSrv->dwDBSize);
    pSrv->bSendingDB = TRUE;
}

void T5RedSrv_StopExchange (T5_PTR pSrvData)
{
    T5_WORD i, max;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    pSrv->bSwitchScan = FALSE;

    if (pSrv->bResendAll)
        return;

    max = (T5_WORD)T5RED_VM_TIMEOUT / 10;
    for (i=0; pSrv->bSendingDB && pSrv->sockClient != T5_INVSOCKET && i<max; i++)
        T5RedSys_Sleep (10);

    if (pSrv->bSendingDB)
    {
        pSrv->dwNbLost += 1L;
        T5REDLOG ("S> ALERT: timeout sending data!");
    }
}

T5_BOOL T5RedSrv_Busy (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return FALSE;

    return pSrv->bBusy;
}

T5_PTR T5RedSrv_Open (T5_DWORD dwVMID, T5_WORD wPort)
{
    T5_RET wRet;
    str_T5redsrv *pSrv;

    pSrv = T5RedSys_Malloc (sizeof (str_T5redsrv));
    pSrv->pUtiData = T5RedUti_AllocData ();
    pSrv->dwVMID = dwVMID;

    pSrv->sockListen = T5_INVSOCKET;
    pSrv->sockClient = T5_INVSOCKET;
    pSrv->srvAlive = SRV_KILLED;
    pSrv->bState = T5RED_UNKNOWN;
    pSrv->bCodeChanged = FALSE;
    pSrv->bBusy = FALSE;
    pSrv->bRunning = FALSE;
    pSrv->pDB = NULL;
    pSrv->pDBupd = NULL;
    pSrv->pFBData = NULL;
    pSrv->dwDBSize = 0L;
    pSrv->bSendingDB = FALSE;
    pSrv->dwNbLost = 0L;
    pSrv->bDelayedSwitch = FALSE;
    T5_MEMSET (pSrv->dwSizing, 0, sizeof (pSrv->dwSizing));
    T5_MEMSET (pSrv->dwOffset, 0, sizeof (pSrv->dwSizing));
    pSrv->bClientReady = FALSE;
    pSrv->bResendAll = TRUE;
    pSrv->dwSwitchTime = 0L;
    pSrv->bSwitchScan = FALSE;


    wRet = T5Rpl_CreateListeningSocket (wPort, 1, &(pSrv->sockListen));
    if (wRet != T5RET_OK)
    {
        T5RedSys_Free (pSrv);
        return NULL;
    }

    pSrv->srvAlive = SRV_RUN;
    T5RedSys_StartThread (T5RedSrv_Thread, TRUE, pSrv);
    return pSrv;
}

void T5RedSrv_Close (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    pSrv->srvAlive = SRV_KILLING;
    while (pSrv->srvAlive != SRV_KILLED)
        T5RedSys_Sleep (100);

    if (pSrv->sockClient != T5_INVSOCKET)
        T5Rpl_CloseSocket (pSrv->sockClient);
    pSrv->sockClient = T5_INVSOCKET;
    if (pSrv->sockListen != T5_INVSOCKET)
        T5Rpl_CloseSocket (pSrv->sockListen);
    pSrv->sockListen = T5_INVSOCKET;
    T5RedSrv_ReleaseDBMem (pSrv);

    T5RedUti_FreeData (pSrv->pUtiData);
    T5RedSys_Free (pSrv);
}

void T5RedSrv_SetState (T5_PTR pSrvData, T5_CHAR bNewState)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    pSrv->bState = bNewState;
}

void T5RedSrv_NotifyCodeChanged (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    if (pSrv->sockClient == T5_INVSOCKET)
        return;
    pSrv->bCodeChanged = TRUE;
}

void T5RedSrv_SetRunState (T5_PTR pSrvData, T5_BOOL bState)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    pSrv->bRunning = bState;

    if (!pSrv->bRunning)
    {
        pSrv->dwSwitchTime = 0L;
        pSrv->bSwitchScan = FALSE;
    }
}

T5_BOOL T5RedSrv_HasClient (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return FALSE;

    return (pSrv->sockClient != T5_INVSOCKET);
}

T5_BOOL T5RedSrv_HasClientReady (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return FALSE;

    if (pSrv->sockClient == T5_INVSOCKET)
        return FALSE;

    if (!pSrv->bClientReady)
        return FALSE;

    return TRUE;
}

void T5RedSrv_BePassive (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return;

    if (!pSrv->bClientReady)
        return;

    if (pSrv->sockClient != T5_INVSOCKET)
        _OnClientLost (pSrv);

    pSrv->bState = T5RED_UNKNOWN;

    pSrv->bCodeChanged = FALSE;
    pSrv->bRunning = FALSE;
    pSrv->pDB = NULL;
    pSrv->pDBupd = NULL;
    pSrv->dwDBSize = 0;
    pSrv->bSendingDB = FALSE;
    T5_MEMSET (pSrv->dwSizing, 0, sizeof (pSrv->dwSizing));
    T5_MEMSET (pSrv->dwOffset, 0, sizeof (pSrv->dwSizing));
    pSrv->bClientReady = FALSE;
    pSrv->bResendAll = TRUE;
}

T5_BOOL T5RedSrv_SwitchQueried (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;
    T5_BOOL bRet;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrv == NULL)
        return FALSE;

    bRet = pSrv->bDelayedSwitch;
    pSrv->bDelayedSwitch = FALSE;
    return bRet;
}

/****************************************************************************/

void T5RedSrv_Thread (T5_PTR pArgs)
{
    T5_DWORD dwLast, dwTimeout;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pArgs;

    T5REDLOG ("S> Server running");
    dwLast = T5RedSys_GetTickCount ();
    while (pSrv->srvAlive == SRV_RUN)
    {
        if (pSrv->sockClient == T5_INVSOCKET)
        {
            pSrv->sockClient = T5Rpl_Accept (pSrv->sockListen);
            if (pSrv->sockClient != T5_INVSOCKET)
            {
                _OnClientHere (pSrv);
                _T5RedSrv_SendState (pSrv);
                dwLast = T5RedSys_GetTickCount ();
            }
        }
        if (pSrv->sockClient == T5_INVSOCKET)
            T5RedSys_Sleep (100);
        else
        {
            if (_T5RedSrv_RecieveAck (pSrv))
            {
                _T5RedSrv_SendState (pSrv);
                dwLast = T5RedSys_GetTickCount ();
            }
            else
            {
                dwTimeout = (pSrv->bClientReady)
                            ? T5RED_SRV_TIMEOUT : T5RED_STR_TIMEOUT;

                if (T5_BOUNDTIME (T5RedSys_GetTickCount () - dwLast) > dwTimeout)
                    _OnClientLost (pSrv);
                T5RedSys_Sleep (10);
            }
        }
    }

    T5REDLOG ("S> Server stopped");
    pSrv->srvAlive = SRV_KILLED;
    return;
}

static void _T5RedSrv_SendState (T5_PTR pSrvData)
{
    T5_CHAR sz[16];
    T5_BOOL bFail;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    T5_STRCPY (sz, "{??}");
    sz[1] = pSrv->bState;
    sz[2] = (pSrv->bRunning) ? T5REDS_RUNNING : T5REDS_IDLE;
    if (pSrv->bCodeChanged)
    {
        T5REDLOG ("S> Notify client: App code has changed");
        sz[2] = T5REDS_CODECHG;
        pSrv->bCodeChanged = FALSE;
    }

    T5Rpl_Send (pSrv->sockClient, 4, sz, &bFail);
    if (bFail)
        _OnClientLost (pSrv);
}

static T5_BOOL _T5RedSrv_RecieveAck (T5_PTR pSrvData)
{
    T5_CHAR sz[16];
    T5_BOOL bFail;
    T5_WORD wNb;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    wNb = T5Rpl_Receive (pSrv->sockClient, 4, sz, &bFail);
    if (bFail)
    {
        _OnClientLost (pSrv);
        return FALSE;
    }
    if (wNb != 4)
        return FALSE;

    /* ask for code checksum */
    if (sz[2] == T5REDS_CODECRC)
    {
        _GiveCodeChecksum (pSrv);
        return FALSE;
    }
    /* ask for code */
    if (sz[2] == T5REDS_CODETFR)
    {
        pSrv->bClientReady = FALSE;
        pSrv->bResendAll = TRUE;
        _GiveCode (pSrv);
        return FALSE;
    }

    pSrv->bClientReady = (sz[2] == T5REDS_RUNNING);

    if (pSrv->bSendingDB)
        _SendDB (pSrv, sz);

    if (pSrv->sockClient == T5_INVSOCKET)
        return FALSE;

    return (wNb == 4);
}

static void _OnClientHere (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    T5RedRpl_SetBlockingSocket (pSrv->sockClient);

    if (pSrv->pDB != NULL)
        T5_MEMSET (pSrv->pDB, 0, pSrv->dwDBSize);
    pSrv->bResendAll = TRUE;

    T5REDLOG ("S> Client here");
}

static void _OnClientLost (T5_PTR pSrvData)
{
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    T5REDLOG ("S> Client lost");
    if (pSrv->sockClient != T5_INVSOCKET)
    {
        T5Rpl_CloseSocket (pSrv->sockClient);
        pSrv->sockClient = T5_INVSOCKET;
    }
    pSrv->bBusy = FALSE;
    pSrv->bSendingDB = FALSE;
    pSrv->bClientReady = FALSE;
}

static void _GiveCodeChecksum (T5_PTR pSrvData)
{
    T5_BOOL bFail;
    T5_CHAR sz[16];
    T5_DWORD dwCRC, dwSize;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    bFail = FALSE;
    T5_STRCPY (sz, "{??}cccczzzz");
    sz[1] = pSrv->bState;
    sz[2] = T5REDS_CODECRC;
    dwCRC = T5RedUti_GetLocalCodeCRC (pSrvData, &dwSize);
    T5_COPYFRAMEDWORD (sz+4, &dwCRC);
    T5_COPYFRAMEDWORD (sz+8, &dwSize);
    T5REDLOG ("S> Serve code CRC=%08lX - size=%lu", dwCRC, dwSize);
    T5Rpl_Send (pSrv->sockClient, 12, sz, &bFail);
    if (bFail)
        _OnClientLost (pSrv);
}

static void _GiveCode (T5_PTR pSrvData)
{
    T5STR_MMB mmb;
    T5_PTBYTE pCode;
    T5_DWORD dwSize;
    T5_BOOL bFail;
    T5_WORD w, wPacket;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    bFail = FALSE;
    pSrv->bBusy = TRUE;

    T5REDLOG ("S> Send app code to slave");
    pCode = (T5_PTBYTE)T5RedUti_GetLocalCode (pSrvData, &mmb, &dwSize);
    while (dwSize && !bFail)
    {
        wPacket = (dwSize > T5RED_PACK_SIZE) ? T5RED_PACK_SIZE : (T5_WORD)dwSize;
        w = T5Rpl_Send (pSrv->sockClient, wPacket, pCode, &bFail);
        if (w)
        {
            dwSize -= (T5_DWORD)w;
            pCode += w;
        }
        T5RedSys_Sleep (10);
    }
    T5RedUti_ReleaseLocalCode (pSrvData, &mmb);
    if (bFail)
        _OnClientLost (pSrv);

    pSrv->bBusy = FALSE;
}

static void _SendDB (T5_PTR pSrvData, T5_PTCHAR szClientState)
{
    T5_CHAR sz[16];
    T5_BOOL bFail;
    T5_BYTE i;
    T5_DWORD dwLast, dwBuf, dwOff;
    T5_DWORD dwTabSize, dwPakSize;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    if (szClientState[2] != T5REDS_RUNNING
        || pSrv->bState != T5RED_MASTER || pSrv->pDB == NULL)
    {
        pSrv->bSendingDB = FALSE;
        return;
    }

    bFail = FALSE;
    T5_STRCPY (sz, "{??}");
    sz[1] = pSrv->bState;
    sz[2] = T5REDS_SENDDB;
    T5Rpl_Send (pSrv->sockClient, 4, sz, &bFail);

    /* send each valid table */
    for (i=0; i<T5MAX_TABLE && !bFail; i++)
    {
        dwTabSize = pSrv->dwSizing[i];
        dwOff = 0;
        while (dwTabSize != 0 && !bFail)
        {
            dwPakSize = (dwTabSize > T5RED_PACK_SIZE) ? T5RED_PACK_SIZE : dwTabSize;
            if (pSrv->sockClient == T5_INVSOCKET)
                bFail = TRUE;
            else if (pSrv->bResendAll
                || memcmp (pSrv->pDB + pSrv->dwOffset[i] + dwOff,
                           pSrv->pDBupd + pSrv->dwOffset[i] + dwOff, dwPakSize))
            {
                _SendPacket (pSrv, 1, &i, &bFail);
                if (!bFail && pSrv->sockClient != T5_INVSOCKET)
                {
                    T5_COPYFRAMEDWORD (&dwBuf, &dwPakSize);
                    _SendPacket (pSrv, 4, &dwBuf, &bFail);
                }
                if (!bFail && pSrv->sockClient != T5_INVSOCKET)
                {
                    T5_COPYFRAMEDWORD (&dwBuf, &dwOff);
                    _SendPacket (pSrv, 4, &dwBuf, &bFail);
                }
                if (!bFail && pSrv->sockClient != T5_INVSOCKET)
                {
                    _SendPacket (pSrv, dwPakSize,
                                 pSrv->pDBupd + pSrv->dwOffset[i] + dwOff, &bFail);
                }
                if (pSrv->sockClient != T5_INVSOCKET)
                {
                    T5_MEMCPY (pSrv->pDB + pSrv->dwOffset[i] + dwOff,
                               pSrv->pDBupd + pSrv->dwOffset[i] + dwOff, dwPakSize);
                }
            }
            dwTabSize -= dwPakSize;
            dwOff += dwPakSize;
        }
    }

    if (pSrv->bResendAll && !bFail)
        T5REDLOG ("S> Complete app data sent to slave - system is ready");

    pSrv->bResendAll = FALSE;

    /* send end marker */
    if (!bFail)
    {
        i = 0xff;
        _SendPacket (pSrv, 1, &i, &bFail);
    }

    /* wait for acknoledge */
    dwLast = T5RedSys_GetTickCount ();
    while (!bFail && T5Rpl_Receive (pSrv->sockClient, 4, sz, &bFail) != 4)
    {
        if (pSrv->sockClient == T5_INVSOCKET)
            bFail = TRUE;
        else if (T5_BOUNDTIME(T5RedSys_GetTickCount () - dwLast) > T5RED_SRV_TIMEOUT)
            bFail = TRUE;
        else
        {
            /* no sleep here - T5RedSys_Sleep (10); */
        }
    }

    pSrv->bSendingDB = FALSE;

    if (bFail)
        _OnClientLost (pSrv);

}

static void _SendPacket (T5_PTR pSrvData, T5_DWORD dwSize,
                         T5_PTR pSend, T5_PTBOOL pbFail)
{
    T5_PTBYTE pData;
    T5_WORD w, wPacket;
    T5_DWORD dws;
    T5_DWORD dwLast;
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;

    pData = (T5_PTBYTE)pSend;
    dws = dwSize;
    dwLast = T5RedSys_GetTickCount ();
    while (dws && !(*pbFail))
    {
        if (pSrv->sockClient == T5_INVSOCKET)
            *pbFail = TRUE;
        else if (T5_BOUNDTIME(T5RedSys_GetTickCount () - dwLast) > T5RED_SRV_TIMEOUT)
            *pbFail = TRUE;
        else
        {
            wPacket = (dws > T5RED_PACK_SIZE) ? T5RED_PACK_SIZE : (T5_WORD)dws;
            w = T5Rpl_Send (pSrv->sockClient, wPacket, pData, pbFail);
            if (w)
            {
                dws -= (T5_DWORD)w;
                pData += w;
                dwLast = T5RedSys_GetTickCount ();
            }
        }
    }
}

T5_BOOL T5RedSrv_UserReq (T5_PTR pSrvData, T5PTR_CS pCS, T5_WORD wCaller)
{
    T5_PTBYTE pIn, pOut;
    T5_PTCHAR pCmd;
    T5_CHAR pAnswer[128];
    str_T5redsrv *pSrv;

    pSrv = (str_T5redsrv *)pSrvData;
    if (pSrvData == NULL)
        return FALSE;

    pIn = (T5_PTBYTE)T5CS_GetRequestFrame (pCS, wCaller);
    pOut = (T5_PTBYTE)T5CS_GetAnswerFrameBuffer (pCS, wCaller);

    pCmd = (T5_PTCHAR)(pIn + 5);
    pCmd[pIn[3]-2] = 0;

    if (T5_STRCMP (pCmd, "t5red?") == 0)
    {
        T5_STRCPY (pAnswer, "t5red=S;c=");
        if (pSrv->sockClient == T5_INVSOCKET)
            T5_STRCAT (pAnswer, "0;");
        else if (pSrv->bClientReady)
            T5_STRCAT (pAnswer, "R;");
        else
            T5_STRCAT (pAnswer, "U;");
    }
    else if (T5_STRCMP (pCmd, "t5red:PASSIVE") == 0)
    {
        if (pSrv->sockClient != T5_INVSOCKET)
        {
            T5_STRCPY (pAnswer, "t5red=C");
            pSrv->bDelayedSwitch = TRUE;
        }
        else
            T5_STRCPY (pAnswer, "FAIL");
    }
    else
        return FALSE;

    pOut[0] = pIn[0];
    pOut[1] = pIn[1];
    pOut[2] = 0;
    pOut[3] = (T5_BYTE)(T5_STRLEN(pAnswer) + 2);
    pOut[4] = T5CSRQ_USER;
    T5_STRCPY (pOut+5, pAnswer);

    T5CS_SendAnswerFrame (pCS, wCaller, (T5_WORD)(T5_STRLEN(pAnswer) + 6));

    return TRUE;
}

/****************************************************************************/
/* GetActiveRTStat Function block */

/* Argument list */

#define _P_QRED         (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[0]))
#define _P_QPASSHERE    (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[1]))
#define _P_QPASSOK      (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[2]))
#define _P_NLOSTEXCH    (*((T5_PTLONG)(T5GET_DBDATA32(pBase))+pArgs[3]))
#define _P_SWICTHCYCLE  (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[4]))
#define _P_SWITCHTIME   (*((T5_PTDWORD)(T5GET_DBTIME(pBase))+pArgs[5]))

/* handler */

T5_DWORD GETACTIVERTSTAT (T5_WORD wCommand, T5PTR_DB pBase, T5_PTR pClass,
                          T5_PTR pInst, T5_PTWORD pArgs)
{
    T5PTR_REDSRVSTATE pState;

    switch (wCommand)
    {
    case T5FBCMD_ACTIVATE :
        if (pClass == NULL)
        {
            _P_QRED = FALSE;
            _P_QPASSHERE = FALSE;
            _P_QPASSOK = FALSE;
            _P_NLOSTEXCH = 0L;
            _P_SWICTHCYCLE = FALSE;
            _P_SWITCHTIME = 0L;
        }
        else
        {
            pState = (T5PTR_REDSRVSTATE)pClass;
            _P_QRED = pState->bRedSrv;
            _P_QPASSHERE = pState->bRedCliHere;
            _P_QPASSOK = pState->bRedCliOK;
            _P_NLOSTEXCH = pState->dwNbLost;
            _P_SWICTHCYCLE = pState->bSwitchScan;
            _P_SWITCHTIME = pState->dwSwitchTime;
        }
        return 0L;
    case T5FBCMD_SIZEOFCLASS :
        return (T5_DWORD)sizeof(T5STR_REDSRVSTATE);
    case T5FBCMD_ACCEPTCT :
        return 1L;
    default :
        return 0L;
    }
}

/* Undefine argument list */

#undef _P_QRED
#undef _P_QPASSHERE
#undef _P_QPASSOK
#undef _P_NLOSTEXCH
#undef _P_SWICTHCYCLE
#undef _P_SWITCHTIME

/* ActiveRTSwitch - Source code (requires t5vm.h include) */
/* Function block */

/* Argument list */

#define _P_BEN   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[0]))
#define _P_BOK   (*((T5_PTBOOL)(T5GET_DBDATA8(pBase))+pArgs[1]))

/****************************************************************************/
/* private block data */

typedef struct
{
    T5_BOOL bPrevEN;
} _str_FB_ACTIVERTSWITCH;

/* handler */

T5_DWORD ACTIVERTSWITCH (T5_WORD wCommand, T5PTR_DB pBase, T5_PTR pClass,
                         T5_PTR pInst, T5_PTWORD pArgs)
{
    _str_FB_ACTIVERTSWITCH *pData;
    str_T5redsrv *pSrv;

    pData = (_str_FB_ACTIVERTSWITCH *)pInst;
    switch (wCommand)
    {
    case T5FBCMD_ACTIVATE :

        if (_P_BEN && !pData->bPrevEN && pClass != NULL)
        {
            pSrv = *(str_T5redsrv **)pClass;
            if (pSrv != NULL && pSrv->bClientReady && !pSrv->bSwitchScan)
            {
                pSrv->bDelayedSwitch = TRUE;
                _P_BOK = TRUE;
            }
            else
                _P_BOK = FALSE;
        }
        else
            _P_BOK = FALSE;
        pData->bPrevEN = _P_BEN;
        return 0L;
    case T5FBCMD_HOTRESTART :
        pData->bPrevEN = TRUE;
        return 0L;
    case T5FBCMD_SIZEOFINSTANCE :
        return (T5_DWORD)sizeof(_str_FB_ACTIVERTSWITCH);
    case T5FBCMD_SIZEOFCLASS :
        return (T5_DWORD)sizeof(str_T5redsrv *);
    case T5FBCMD_ACCEPTCT :
        return 1L;
    default :
        return 0L;
    }
}

/* Undefine argument list */

#undef _P_BEN
#undef _P_BOK
