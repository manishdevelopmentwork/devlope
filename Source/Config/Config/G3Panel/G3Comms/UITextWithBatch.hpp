
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextWithBatch_HPP

#define INCLUDE_UITextWithBatch_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Include With Batch
//

class CUITextWithBatch : public CUITextEnumPick
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextWithBatch(void);

	protected:
		// Overridables
		void OnBind(void);
		BOOL OnEnumValues(CStringArray &List);

		// Implementation
		void Reload(void);
	};

// End of File

#endif
