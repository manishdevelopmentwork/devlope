
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3DataService_HPP

#define	INCLUDE_G3DataService_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Access Service
//

class CG3DataService : public ILinkService
{
public:
	// Constructor
	CG3DataService(ICrimsonPxe *pPxe);

	// Destructor
	~CG3DataService(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Re, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	         m_uRefs;
	ICrimsonPxe    * m_pPxe;
};

// End of File

#endif
