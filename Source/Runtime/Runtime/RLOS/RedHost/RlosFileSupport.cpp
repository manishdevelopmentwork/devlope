
#include "Intern.hpp"

#include "RlosFileSupport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File System Support
//

// Instantiator

IUnknown * Create_RlosFileSupport(void)
{
	return (IFileSupport *) New CRlosFileSupport;
}

// Constructor

CRlosFileSupport::CRlosFileSupport(void)
{
	m_HostRoot = "";

	m_pFileMan = NULL;

	m_pDiskMan = NULL;
}

// Destructor

CRlosFileSupport::~CRlosFileSupport(void)
{
	AfxRelease(m_pFileMan);

	AfxRelease(m_pDiskMan);
}

// IUnknown

HRESULT CRlosFileSupport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IFileUtilities);

	return CBaseFileSupport::QueryInterface(riid, ppObject);
}

ULONG CRlosFileSupport::AddRef(void)
{
	return CBaseFileSupport::AddRef();
}

ULONG CRlosFileSupport::Release(void)
{
	return CBaseFileSupport::Release();
}

// IFileUtilities

BOOL CRlosFileSupport::IsDiskMounted(char cDrive)
{
	if( FindManagers() ) {

		IFilingSystem *pFileSys = m_pFileMan->FindFileSystem(char(toupper(cDrive)));

		if( pFileSys ) {

			if( pFileSys->IsMounted() ) {

				m_pFileMan->FreeFileSystem(pFileSys, false);

				return TRUE;
			}

			m_pFileMan->FreeFileSystem(pFileSys, false);
		}
	}

	return FALSE;
}

UINT CRlosFileSupport::GetDiskStatus(char cDrive)
{
	if( FindManagers() ) {

		UINT iDisk = m_pDiskMan->FindDisk(char(toupper(cDrive)));

		if( iDisk != NOTHING ) {

			return m_pDiskMan->GetDiskStatus(iDisk);
		}
	}

	return diskEmpty;
}

DWORD CRlosFileSupport::GetDiskIdent(char cDrive)
{
	return MAKELONG(MAKEWORD(cDrive, cDrive), MAKEWORD(cDrive, cDrive));
}

UINT64 CRlosFileSupport::GetDiskSize(char cDrive)
{
	if( FindManagers() ) {

		IFilingSystem *pFileSys = m_pFileMan->LockFileSystem(char(toupper(cDrive)));

		if( pFileSys ) {

			UINT64 s = pFileSys->GetSize();

			m_pFileMan->FreeFileSystem(pFileSys, true);

			return s;
		}
	}

	return 0;
}

UINT64 CRlosFileSupport::GetDiskFree(char cDrive)
{
	if( FindManagers() ) {

		IFilingSystem *pFileSys = m_pFileMan->LockFileSystem(char(toupper(cDrive)));

		if( pFileSys ) {

			UINT64 f = pFileSys->GetFree();

			m_pFileMan->FreeFileSystem(pFileSys, true);

			return f;
		}
	}

	return 0;
}

BOOL CRlosFileSupport::FormatDisk(char cDrive)
{
	if( FindManagers() ) {

		UINT iDisk = m_pDiskMan->FindDisk(char(toupper(cDrive)));

		if( iDisk != NOTHING ) {

			AfxTrace("Format %u\n", iDisk);

			if( m_pDiskMan->FormatDisk(iDisk) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CRlosFileSupport::EjectDisk(char cDrive)
{
	if( FindManagers() ) {

		UINT iDisk = m_pDiskMan->FindDisk(char(toupper(cDrive)));

		if( iDisk != NOTHING ) {

			if( m_pDiskMan->EjectDisk(iDisk) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Host Hooks

bool CRlosFileSupport::HostRename(CString const &From, CString const &To)
{
	AfxNewAutoObject(File, "fs.file", IFile);

	if( File->Open(From, fileWrite) ) {

		if( File->Rename(To) ) {

			return true;
		}
	}

	return false;
}

bool CRlosFileSupport::HostUnlink(CString const &Path)
{
	AfxNewAutoObject(File, "fs.file", IFile);

	if( File->Open(Path, fileWrite) ) {

		if( File->Delete() ) {

			return true;
		}
	}

	return false;
}

bool CRlosFileSupport::HostStat(CString const &Path, struct stat *buffer)
{
	if( FindManagers() ) {

		CFilename Name = Path;

		IFilingSystem *pFileSys = m_pFileMan->FindFileSystem(Name.GetDrive());

		if( pFileSys ) {

			FSIndex Index;

			if( pFileSys->LocateDir(Name, Index) || pFileSys->LocateFile(Name, Index) ) {

				buffer->st_atime = 0;
				buffer->st_ctime = 0;
				buffer->st_mtime = 0;
				buffer->st_dev   = 0;
				buffer->st_rdev  = 0;
				buffer->st_nlink = 1;
				buffer->st_size  = pFileSys->GetSize(Index);
				buffer->st_mtime = pFileSys->GetUnixTime(Index);
				buffer->st_atime = buffer->st_mtime;
				buffer->st_mode  = S_IREAD | S_IWRITE;

				buffer->st_mode |= pFileSys->IsDirectory(Index) ? S_IFDIR : S_IFREG;

				m_pFileMan->FreeFileSystem(pFileSys, false);

				return true;
			}

			m_pFileMan->FreeFileSystem(pFileSys, false);
		}
	}

	return false;
}

bool CRlosFileSupport::HostUTime(CString const &Path, time_t time)
{
	AfxNewAutoObject(File, "fs.file", IFile);

	if( File->Open(Path, fileWrite) ) {

		File->SetUnixTime(time);

		return true;
	}

	return false;
}

bool CRlosFileSupport::HostChMod(CString const &Path, mode_t mode)
{
	return true;
}

PVOID CRlosFileSupport::HostOpen(CString const &Path, int oflag, int pmode)
{
	IFile *pFile = NULL;

	AfxNewObject("fs.file", IFile, pFile);

	if( pFile ) {

		UINT uMode = fileRead;

		if( oflag & O_WRONLY ) {

			uMode = fileWrite;
		}

		if( oflag & O_RDWR ) {

			uMode = fileRead | fileWrite;
		}

		if( oflag & O_CREAT ) {

			uMode |= fileCreate;
		}

		if( pFile->Open(Path, uMode) ) {

			if( oflag & O_TRUNC ) {

				pFile->SetLength(0);
			}

			if( oflag & O_APPEND ) {

				pFile->SetEnd();
			}

			return PVOID(pFile);
		}

		pFile->Release();
	}

	return NULL;
}

void CRlosFileSupport::HostClose(PVOID hFile)
{
	IFile *pFile = (IFile *) hFile;

	pFile->Close();

	pFile->Release();
}

bool CRlosFileSupport::HostStat(PVOID hFile, struct stat *buffer)
{
	IFile *pFile = (IFile *) hFile;

	buffer->st_atime = 0;
	buffer->st_ctime = 0;
	buffer->st_mtime = 0;
	buffer->st_dev   = 0;
	buffer->st_rdev  = 0;
	buffer->st_nlink = 1;
	buffer->st_size  = pFile->GetLength();
	buffer->st_mode  = S_IFREG | S_IREAD | S_IWRITE;
	buffer->st_mtime = pFile->GetUnixTime();
	buffer->st_atime = buffer->st_mtime;

	return true;
}

int CRlosFileSupport::HostRead(PVOID hFile, void *buffer, unsigned int count)
{
	IFile *pFile = (IFile *) hFile;

	return pFile->Read(PBYTE(buffer), count);
}

int CRlosFileSupport::HostWrite(PVOID hFile, void const *buffer, unsigned int count)
{
	IFile *pFile = (IFile *) hFile;

	return pFile->Write(PCBYTE(buffer), count);
}

int CRlosFileSupport::HostSeek(PVOID hFile, long offset, int origin)
{
	IFile *pFile = (IFile *) hFile;

	if( origin == SEEK_SET ) {

		return pFile->SetPos(offset, seekBeg) ? pFile->GetPos() : -1;
	}

	if( origin == SEEK_CUR ) {

		return pFile->SetPos(offset, seekCur) ? pFile->GetPos() : -1;
	}

	if( origin == SEEK_END ) {

		return pFile->SetPos(offset, seekEnd) ? pFile->GetPos() : -1;
	}

	return -1;
}

bool CRlosFileSupport::HostTruncate(PVOID hFile, DWORD size)
{
	IFile *pFile = (IFile *) hFile;

	return pFile->SetLength(size) ? true : false;
}

int CRlosFileSupport::HostIoCtl(PVOID hFile, int func, void *data)
{
	return -1;
}

bool CRlosFileSupport::HostIsValidDir(CString const &Path)
{
	if( FindManagers() ) {

		CFilename Name = Path;

		IFilingSystem *pFileSys = m_pFileMan->FindFileSystem(Name.GetDrive());

		if( pFileSys ) {

			FSIndex Index;

			if( pFileSys->LocateDir(Name, Index) ) {

				m_pFileMan->FreeFileSystem(pFileSys, false);

				return true;
			}

			m_pFileMan->FreeFileSystem(pFileSys, false);
		}
	}

	return false;
}

bool CRlosFileSupport::HostRmDir(CString const &Path)
{
	if( FindManagers() ) {

		CFilename Name = Path;

		IFilingSystem *pFileSys = m_pFileMan->FindFileSystem(Name.GetDrive());

		if( pFileSys ) {

			if( pFileSys->RemoveDir(Name) ) {

				m_pFileMan->FreeFileSystem(pFileSys, false);

				return true;
			}

			m_pFileMan->FreeFileSystem(pFileSys, false);
		}
	}

	return false;
}

bool CRlosFileSupport::HostMkDir(CString const &Path)
{
	if( FindManagers() ) {

		CFilename Name = Path;

		IFilingSystem *pFileSys = m_pFileMan->FindFileSystem(Name.GetDrive());

		if( pFileSys ) {

			if( pFileSys->CreateDir(Name) ) {

				m_pFileMan->FreeFileSystem(pFileSys, false);

				return true;
			}

			m_pFileMan->FreeFileSystem(pFileSys, false);
		}
	}

	return false;
}

int CRlosFileSupport::HostScanDir(CString const &Path, struct dirent ***list, int (*selector)(struct dirent const *))
{
	if( FindManagers() ) {

		CFilename Name = Path;

		IFilingSystem *pFileSys = m_pFileMan->FindFileSystem(Name.GetDrive());

		if( pFileSys ) {

			FSIndex *pList = NULL;

			UINT     uScan = pFileSys->ScanDir(Name, pList, scanAll);

			CAutoArray<FSIndex> List(pList);

			int nCount = 0;

			for( UINT uPass = 0; uPass < 2; uPass++ ) {

				int n = 0;

				for( UINT i = 0; i < uScan; i++ ) {

					struct dirent d = { 0 };

					d.d_type = BYTE(pFileSys->IsDirectory(List[i]) ? DT_DIR : DT_REG);

					pFileSys->GetName(List[i], Name);

					strcpy(d.d_name, PCTXT(Name));

					if( !selector || selector(&d) ) {

						if( strcmp(d.d_name, "..") || !IsRoot(Path) ) {

							if( uPass == 0 ) {

								nCount++;
							}

							if( uPass == 1 && n < nCount ) {

								(*list)[n] = (dirent *) malloc(sizeof(dirent));

								memcpy((*list)[n], &d, sizeof(d));

								n++;
							}
						}
					}
				}

				if( uPass == 0 ) {

					if( nCount ) {

						*list = (dirent **) calloc(nCount, sizeof(dirent *));

						continue;
					}

					break;
				}
			}

			m_pFileMan->FreeFileSystem(pFileSys, false);

			return nCount;
		}
	}

	return -1;
}

bool CRlosFileSupport::HostSync(void)
{
	return true;
}

// Implementation

BOOL CRlosFileSupport::FindManagers(void)
{
	if( m_pFileMan && m_pDiskMan ) {

		return TRUE;
	}

	if( !m_pFileMan ) {

		AfxGetObject("fs.fileman", 0, IFileManager, m_pFileMan);
	}

	if( !m_pDiskMan ) {

		AfxGetObject("fs.diskman", 0, IDiskManager, m_pDiskMan);
	}

	if( m_pFileMan && m_pDiskMan ) {

		return TRUE;
	}

	return FALSE;
}

// End of File
