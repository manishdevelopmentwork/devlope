
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDanfoss;

//////////////////////////////////////////////////////////////////////////
//
// Danfoss Commands
//

#define	CMD_NONE	0x0
#define CMD_READ	0x1
#define CMD_WRITE	0x3		

//////////////////////////////////////////////////////////////////////////
//
// Danfoss Functions
//

#define	FNC_NONE	0x0000
#define FNC_USED	0x0400

//////////////////////////////////////////////////////////////////////////
//
// Danfoss Replys
//

#define REPLY_WORD	0x1
#define REPLY_DWORD	0x2

//////////////////////////////////////////////////////////////////////////
//
// Danfoss Types
//

struct CParam {

	UINT	Cmd	: 4;
	UINT	Num	: 12;
	};

//////////////////////////////////////////////////////////////////////////
//
// Danfoss VLT 6000 Driver
//

class CDVLT6000Driver : public CMasterDriver
{
	public:
		// Constructor
		CDVLT6000Driver(void);

		// Destructor
		~CDVLT6000Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			WORD m_wBus;
			WORD m_wPing;
		      	};

		// Data Members
		CContext * m_pCtx;
		BYTE m_bBuff[32];
		BYTE m_Ptr;
		BYTE m_bBcc;
		BYTE m_bLen;

		// Frame Building
		void  StartFrame(BYTE bLen);
		void  AddByte(BYTE bData);
		void  AddWord(WORD wData);
		void  AddDWord(DWORD dwData);
		void  AddDrop(void);
		BYTE  ReadByte(void);
		WORD  ReadWord(void);
		DWORD ReadDWord(void);
		
		// Transport Layer
		BOOL TxFrame(void);
		BOOL RxFrame(void);

		// Checksum Handler
		void ClearCheck(void);
		void AddToCheck(BYTE b);
		void SendCheck(void);

		// Helpers
		BOOL IsStat(UINT uTable, UINT uOffset);
		BOOL IsCmd(UINT uTable, UINT uOffset);
		BOOL IsBus(UINT uTable, UINT uOffset);
		BOOL IsOut(UINT uTable, UINT uOffset);

	};

// End of File
