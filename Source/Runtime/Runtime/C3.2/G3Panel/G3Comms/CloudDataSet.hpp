
#include "Intern.hpp"

#include "service.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudDataSet_HPP

#define	INCLUDE_CloudDataSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//

class CCloudDataSet : public CCodedHost
{
public:
	// Constructor
	CCloudDataSet(void);

	// Destructor
	~CCloudDataSet(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Attributes
	BOOL IsEnabled(void) const;
	BOOL UseHistory(void) const;
	UINT GetPeriod(void) const;
	BOOL IsTriggered(void);

	// Operations
	void Tick(void);

	// Json Formatting
	virtual BOOL GetJson(CMqttJsonData *pRep, CMqttJsonData *pDes, CString Ident, UINT64 uTime, UINT uRoot, UINT uCode, UINT uMode) = 0;

	// List Formatting
	virtual UINT GetCount(void) = 0;
	virtual UINT GetProps(void) = 0;
	virtual BOOL GetData(UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, UINT uMode) = 0;
	virtual BOOL GetProp(UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type) = 0;
	virtual BOOL SetData(UINT uIndex, DWORD Data, UINT Type) = 0;

	// Operations
	virtual void Init(void);
	virtual BOOL Fixup(void);
	virtual void ResetHistory(void);
	virtual void DataWasSent(void);

	// Data Members
	UINT		m_Mode;
	UINT		m_History;
	UINT		m_Scan;
	UINT		m_Force;
	CCodedItem    * m_pReq;
	CCodedItem    * m_pAck;
	CString         m_Suffix;

	// Modes
	enum
	{
		modeDelta = 0,
		modeIfAny = 1,
		modeForce = 2,
		modeBirth = 3,
	};

protected:
	// Data Members
	BOOL m_fAck;
	UINT m_Timer;

	// Implementation
	void SetAck(BOOL fAck);
	void LoadTimer(void);
};

// End of File

#endif
