/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ASSEMBLY.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** I/O data storage and handling
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef ASSEMBLY_H
#define ASSEMBLY_H


#define ASSEMBLY_SIZE		0xFFFF		/* Size of each input and output I/O data tables */

#define ASSEMBLY_CLASS			  4	    /* Assembly class identifier */
#define ASSEMBLY_REVISION		  2	    /* Assembly class revision */

extern UINT8  inputAssembly[/*ASSEMBLY_SIZE*/];	   /* Preallocated space to store all input I/O data */
extern UINT8  outputAssembly[/*ASSEMBLY_SIZE*/];   /* Preallocated space to store all output I/O data */

#define ASSEMBLY_CLASS_REVISION         0x01   /* Revision                            */
#define ASSEMBLY_CLASS_MAX_INSTANCE     0x02   /* Max instance                        */

#define ASSEMBLY_INSTANCE_DATA          0x03   /* Data (as bytes)   		                */
#define ASSEMBLY_INSTANCE_SIZE          0x04   /* Size of data (in bytes)		            */

#define ASSEMBLY_INPUT_INSTANCE			1   /* Indicate the input assembly table when processing request for the Assembly object */
#define ASSEMBLY_OUTPUT_INSTANCE		2   /* Indicate the output assembly table when processing request for the Assembly object */

extern void assemblyInit();
extern void assemblyResetConnectionSpace( INT32 nConnection );

extern INT32 assemblyAssignInputOffsetForConnPoint( INT32 nConnection, INT32 nConnPoint );
extern INT32 assemblyAssignOutputOffsetForConnPoint( INT32 nConnection, INT32 nConnPoint );

extern INT32 assemblyGetInputData( UINT8* pData, INT32 nOffset, INT32 nSize ); 
extern INT32 assemblySetInputData( UINT8* pData, INT32 nOffset, INT32 nSize ); 
extern INT32 assemblyGetOutputData( UINT8* pData, INT32 nOffset, INT32 nSize ); 
extern INT32 assemblySetOutputData( UINT8* pData, INT32 nOffset, INT32 nSize ); 

extern INT32 assemblyGetConnectionInputData( INT32 nConnection, UINT8* pData, INT32 nSize );
extern INT32 assemblySetConnectionInputData( INT32 nConnection, UINT8* pData, INT32 nSize );
extern INT32 assemblyGetConnectionOutputData( INT32 nConnection, UINT8* pData, INT32 nSize );
extern INT32 assemblySetConnectionOutputData( INT32 nConnection, UINT8* pData, INT32 nSize );

extern void assemblyParseClassInstanceRequest( INT32 nRequest );
extern void assemblyParseClassRequest( INT32 nRequest );
extern void assemblyParseInstanceRequest( INT32 nRequest );
extern void assemblySendClassAttrSingle( INT32 nRequest );
extern void assemblySendInstanceAttrSingle( INT32 nRequest );
extern void assemblySetInstanceAttrSingle( INT32 nRequest );
extern void assemblySendMember( INT32 nRequest );
extern void assemblySetMember( INT32 nRequest );


#endif /* #ifndef ASSEMBLY_H */
