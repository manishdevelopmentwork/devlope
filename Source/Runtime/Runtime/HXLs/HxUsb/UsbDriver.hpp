
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDriver_HPP

#define	INCLUDE_UsbDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Base Driver
//

class CUsbDriver 
{
	public:
		// Constructor
		CUsbDriver(void);

		// Destructor
		virtual ~CUsbDriver(void);

	protected:
		// Debug Mask
		enum
		{
			debugNone  = 0,
			debugErr   = Bit(0),
			debugWarn  = Bit(1),
			debugInfo  = Bit(2),
			debugCmds  = Bit(3),
			debugData  = Bit(4),
			debugState = Bit(5),
			debugApp   = Bit(16),
			debugAll   = 0xFFFFFFFF,
			};

		// Data
		DWORD  m_Debug;
		PCTXT  m_pName;
		ULONG  m_uRefs;

		// Debugging
		void Trace(UINT uMask, PCTXT pFormat, ...);
	};

// End of File

#endif
