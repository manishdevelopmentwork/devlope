
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LibC Implementation
//

clink void _aeon_longjmp(int volatile *, int)
{
	// TODO -- We should implement these and copy into the hosts.

	// They are only used by FreeType, and even then they should
	// not happen in the general course things. Rather, they will
	// get fired if we get an invalid font file.
	}

clink int _aeon_setjmp(int volatile *)
{
	// TODO -- We should implement these and copy into the hosts.

	return 0;
	}

clink int getpid(void)
{
	HTHREAD hThread = GetCurrentThread();
	
	return hThread ? hThread->GetIdent() : 1;
	}

clink int raise(int)
{	
	AfxAssert(FALSE);

	return 0;
	}

clink void abort(void)
{
	AfxAssert(FALSE);

	for(;;);
	}

clink void _exit(int)
{
	AfxAssert(FALSE);

	for(;;);
	}

clink struct passwd * getpwuid(uid_t uid)
{
	return NULL;
	}

clink struct passwd * getpwnam(char const *name)
{
	return NULL;
	}

clink uid_t getuid(void)
{
	return 0;
	}

// End of File
