
#include "intern.hpp"

#include "DAMix4AnalogOutput.hpp"

#include "DAMix4AnalogOutputWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/daao8props.h"

#include "import/manticore/daao8dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4AnalogOutput, CCommsItem);

// Property List

CCommsList const CDAMix4AnalogOutput::m_CommsList[] = {

	{ 1, "Data1",		PROPID_DATA1,	usageWriteBoth,  IDS_NAME_DATA1	},
	{ 1, "Data2",		PROPID_DATA2,	usageWriteBoth,  IDS_NAME_DATA2	},

	{ 2, "Alarm1",		PROPID_ALARM1,	usageRead,	 IDS_NAME_A1	},
	{ 2, "Alarm2",		PROPID_ALARM2,	usageRead,	 IDS_NAME_A2	},

};

// Constructor

CDAMix4AnalogOutput::CDAMix4AnalogOutput(void)
{
	m_Data1 = 0;
	m_Data2 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAMix4AnalogOutput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Data");

		case 2:	return CString(L"Alarms");
	}

	return CCommsItem::GetGroupName(Group);
}

// Property Filter

BOOL CDAMix4AnalogOutput::IncludeProp(WORD PropID)
{
	if( !m_InitData ) {

		switch( PropID ) {

			case PROPID_DATA1:
			case PROPID_DATA2:

				return FALSE;
		}
	}

	return TRUE;
}

// Meta Data Creation

void CDAMix4AnalogOutput::AddMetaData(void)
{
	Meta_AddInteger(Data1);
	Meta_AddInteger(Data2);

	Meta_AddInteger(InitData);

	CCommsItem::AddMetaData();
}

// End of File
