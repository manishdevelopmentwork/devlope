
#include "Intern.hpp"

#include "ProgramPrototype.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"
#include "ProgramParameter.hpp"
#include "ProgramParameterList.hpp"
#include "ProgramPrototypePage.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Program Prototype
//

// Dynamic Class

AfxImplementDynamicClass(CProgramPrototype, CUIItem);

// Constructor

CProgramPrototype::CProgramPrototype(void)
{
	m_Type        = typeVoid;

	m_pParams     = New CProgramParameterList;

	m_pParamList  = NULL;

	m_uParamCount = 0;
	}

// Destructor

CProgramPrototype::~CProgramPrototype(void)
{
	if( m_pParamList ) {

		delete [] m_pParamList;

		m_pParamList = NULL;

		m_uParamCount = 0;
		}
	}

// UI Management

CViewWnd * CProgramPrototype::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;

		pList->Append(New CUIStdPage(AfxPointerClass(this)));

		pList->Append(New CProgramPrototypePage(this));

		CUIViewWnd *pView = New CUIItemViewWnd(pList);

		pView->SetBorder(6);

		return pView;
		}

	return NULL;
	}

// UI Update

void CProgramPrototype::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			CProgramItem *pProgram = (CProgramItem *) GetParent();

			if( pProgram->m_BkGnd ) {

				pHost->EnableUI(this, L"Type", FALSE);
				}
			}
		}
	}

// Persistance

void CProgramPrototype::PostLoad(void)
{
	CUIItem::PostLoad();

	UpdateParamList();
	}

// Operations

void CProgramPrototype::UpdateParamList(void)
{
	if( m_pParamList ) {

		delete [] m_pParamList;

		m_pParamList = NULL;

		m_uParamCount = 0;
		}

	UINT c = m_pParams->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CProgramParameter *pParam = m_pParams->GetItem(n);

		if( pParam->m_Type != typeVoid ) {

			m_uParamCount ++;
			}
		}

	if( m_uParamCount ) {

		m_pParamList = new CFuncParam [m_uParamCount];

		UINT c = m_pParams->GetItemCount();

		for( UINT p = 0, n = 0; n < c; n++ ) {

			CProgramParameter *pParam = m_pParams->GetItem(n);

			if( pParam->m_Type != typeVoid ) {

				m_pParamList[p].m_Name = pParam->m_Name;

				m_pParamList[p].m_Type = pParam->m_Type;

				p++;
				}
			}
		}
	}

// Attributes

CString CProgramPrototype::Format(void)
{
	CProgramItem *pProgram = (CProgramItem *) GetParent();

	CString Text;

	Text += TextFromType(m_Type);

	Text += L" ";

	Text += pProgram->m_Name;

	Text += L"(";

	UINT uCount = 0;

	UINT c = m_pParams->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CProgramParameter *pParam = m_pParams->GetItem(n);

		if( pParam->m_Type != typeVoid ) {

			if( uCount ) {

				Text += L", ";
				}

			Text += TextFromType(pParam->m_Type);

			Text += L" ";

			Text += pParam->m_Name;

			uCount ++;
			}
		}

	if( uCount == 0 ) {

		Text += TextFromType(typeVoid);
		}

	Text += L")";

	return Text;
	}

UINT CProgramPrototype::GetParameterCount(void)
{
	return m_uParamCount;
	}

CFuncParam * CProgramPrototype::GetParameterList(void)
{
	return m_pParamList;
	}

// Operations

BOOL CProgramPrototype::Parse(CString Text)
{
	Text.TrimBoth();

	if( TypeFromText(m_Type, Text.StripToken(' ')) ) {

		BOOL    fOkay = TRUE;
	
		CString Name  = Text.StripToken('(');

		Text.Remove(')');

		CStringArray List;

		Text.Tokenize(List, ',');

		for( UINT n = 0; n < m_pParams->GetItemCount(); n ++ ) {

			CProgramParameter *pParam = m_pParams->GetItem(n);

			if( n < List.GetCount() ) {

				CString Name = List[n];

				Name.TrimBoth();

				if( TypeFromText(pParam->m_Type, Name.StripToken(' ')) ) {

					pParam->m_Name = Name;
					}
				else {
					pParam->m_Type = typeVoid;

					pParam->m_Name = CPrintf(L"Param%u", 1+n);

					fOkay = FALSE;
					}
				}
			else {
				pParam->m_Type = typeVoid;

				pParam->m_Name = CPrintf(L"Param%u", 1+n);
				}
			}

		return fOkay;
		}

	return FALSE;
	}

// Meta Data

void CProgramPrototype::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);
	Meta_AddCollect(Params);

	Meta_SetName((IDS_PROGRAM_PROTOTYPE));
	}

// Implementation

CString CProgramPrototype::TextFromType(UINT Type)
{
	PCTXT pNames[] = {

		L"void",
		L"int",
		L"float",
		L"cstring"

		};

	return Type < elements(pNames) ? pNames[Type] : L"???";
	}

BOOL CProgramPrototype::TypeFromText(UINT &Type, CString Text)
{
	PCTXT pNames[] = {

		L"void",
		L"int",
		L"float",
		L"cstring"

		};

	for( UINT n = 0; n < elements(pNames); n ++ ) {

		if( !Text.CompareC(pNames[n]) ) {

			Type = n;

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
