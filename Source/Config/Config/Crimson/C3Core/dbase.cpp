
#include "intern.hpp"

#include "secure.hpp"

#include <ws2def.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Database Object
//

// Libraries

#pragma comment(lib, "Ws2_32.lib")

// Static Data

CGuid CDatabase::m_InstGuid;

// Runtime Class

AfxImplementRuntimeClass(CDatabase, CMetaItem);

// Constructor

CDatabase::CDatabase(void)
{
	if( m_InstGuid.IsEmpty() ) {

		m_InstGuid.CreateUnique();
	}

	Construct();
}

CDatabase::CDatabase(CString Model)
{
	m_Model = Model;

	m_Human = Model;

	C3OemStrings(m_Human);

	if( m_InstGuid.IsEmpty() ) {

		m_InstGuid.CreateUnique();
	}

	Construct();

	LoadDLL(Model);

	CLASS Class = AliasClass(Model + L"Item");

	m_pSystem   = AfxNewObject(CSystemItem, Class);

	m_pSystem->SetParent(this);

	m_Map.Insert(HANDLE_SYSTEM, m_pSystem);
}

// Destructor

CDatabase::~CDatabase(void)
{
	delete m_pImage;

	CleanUp();

	FreeDLL();
}

// System Item

CSystemItem * CDatabase::GetSystemItem(void) const
{
	return m_pSystem;
}

// Attributes

CFilename CDatabase::GetFilename(void) const
{
	return m_Filename;
}

CString CDatabase::GetModelName(void) const
{
	return m_Model;
}

BOOL CDatabase::HasImage(void) const
{
	return m_Image;
}

BOOL CDatabase::IsReadOnly(void) const
{
	return m_fReadOnly;
}

BOOL CDatabase::IsPrivate(void) const
{
	return (m_Locked == 3);
}

BOOL CDatabase::IsDownloadOnly(void) const
{
	return (m_Locked == 4);
}

BOOL CDatabase::CanSaveCopy(void) const
{
	return (!m_fReadOnly || !m_Locked) && !IsDownloadOnly();
}

BOOL CDatabase::IsDirty(void) const
{
	return m_fDirty;
}

BOOL CDatabase::IsMucky(void) const
{
	return m_fMucky;
}

CString CDatabase::GetUniqueSig(void) const
{
	return CString(m_InstGuid) + L'/' + CString(m_Guid);
}

REFGUID CDatabase::GetGuid(void) const
{
	return m_Guid;
}

CString CDatabase::GetConfig(void) const
{
	return m_Config;
}

BOOL CDatabase::HasFlag(CString Name) const
{
	return !m_Flags.Failed(m_Flags.Find(Name));
}

CATLIST CDatabase::GetCatList(void) const
{
	return m_Cats;
}

BOOL CDatabase::GetRecomp(void) const
{
	return m_Recomp;
}

BOOL CDatabase::GetCircle(void) const
{
	return m_Circle;
}

BOOL CDatabase::GetPending(void) const
{
	return m_Pending;
}

CString CDatabase::GetLockedName(void) const
{
	if( IsPrivate() ) {

		return CString(IDS_PRIVATE);
	}

	if( IsReadOnly() ) {

		return CString(IDS_READ_ONLY);
	}

	if( IsDownloadOnly() ) {

		return CString(IDS_DOWNLOAD_ONLY);
	}

	return L"";
}

UINT CDatabase::GetSoftwareGroup(void) const
{
	return m_pSystem->GetSoftwareGroup();
}

// Operations

BOOL CDatabase::LoadFile(CFilename const &Name, BOOL fRead)
{
	return LoadFile(Name, L"", fRead);
}

BOOL CDatabase::LoadFile(CFilename const &Name, CString Force, BOOL fRead)
{
	return LoadFile(Name, Force, L"", fRead);
}

BOOL CDatabase::LoadFile(CFilename const &Name, CString Force, CString Model, BOOL fRead)
{
	CTextStreamMemory Stream;

	if( Stream.LoadFromFile(Name) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			CString Code = Tree.GetName();

			if( Code == L"C3Data" ) {

				Tree.GetObject();

				afxThread->SetWaitMode(TRUE);

				Tree.SetFile(Name);

				Load(Tree, Force);

				afxThread->SetWaitMode(FALSE);

				Tree.EndObject();

				if( m_pSystem ) {

					if( !m_pSystem->IsKindOf(AfxRuntimeClass(CSystemItem)) ) {

						throw (int *) 0;
					}

					if( !Model.IsEmpty() ) {

						m_pSystem->SetModel(Model);
					}

					if( !m_Password.IsEmpty() && !IsPrivate() && !IsDownloadOnly() ) {

						HideSplash();

						if( !CheckPassword(fRead) ) {

							PostLoad();

							m_Map.Insert(1, m_pSystem);

							return FALSE;
						}
					}

					if( Force.IsEmpty() ) {

						if( m_pSystem->NeedNewSoftware() ) {

							HideSplash();

							CString Text;

							Text += CString(IDS_THIS_DATABASE_WAS);

							Text += L"\n\n";

							Text += CString(IDS_EDITING_IT_WITH);

							Text += L"\n\n";

							Text += CString(IDS_FILE_WILL_BE_OPEN);

							if( C3OemFeature(L"Update", TRUE) ) {

								Text += L"\n\n";

								Text += CString(IDS_PLEASE_CHECK_FOR);
							}

							afxMainWnd->Information(Text);

							afxMainWnd->UpdateWindow();

							fRead = TRUE;
						}
						else {
							if( Tree.GetSkipFlag() ) {

								HideSplash();

								CString Text;

								Text += CString(IDS_THIS_DATABASE_WAS);

								Text += L"\n\n";

								Text += CString(IDS_SOME_ITEMS_IN);

								Text += L"\n\n";

								Text += CString(IDS_SAVING_FILE_WILL);

								if( C3OemFeature(L"Update", TRUE) ) {

									Text += L"\n\n";

									Text += CString(IDS_PLEASE_CHECK_FOR);
								}

								afxMainWnd->Information(Text);

								afxMainWnd->UpdateWindow();
							}
						}
					}
				}
				else {
					if( m_Locked ) {

						PostLoad();

						m_Map.Insert(1, m_pSystem);

						return FALSE;
					}
				}

				m_Filename = Name;

				PostLoad();

				m_Map.Insert(1, m_pSystem);

				m_fReadOnly = fRead || CheckReadOnly(Name);

				m_fDirty    = FALSE;

				m_fMucky    = FALSE;

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CDatabase::SaveFile(CFilename const &Name)
{
	if( !m_Filename.IsEmpty() ) {

		if( m_Filename == Name ) {

			if( m_fReadOnly ) {

				return FALSE;
			}
		}
		else {
			CString Text;

			Text += CString(IDS_DO_YOU_WANT_TO);

			Text += L"\n\n";

			Text += CString(IDS_NEW_IDENT_1);

			Text += CString(IDS_NEW_IDENT_2);

			Text += CString(IDS_NEW_IDENT_3);

			if( afxMainWnd->NoYes(Text) == IDYES ) {

				m_Guid.CreateUnique();
			}
		}
	}

	if( SaveCopy(Name) ) {

		m_Filename = Name;

		m_fDirty   = FALSE;

		PostSave();

		return TRUE;
	}

	return FALSE;
}

BOOL CDatabase::SaveCopy(CFilename const &Name)
{
	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		CTreeFile Tree;

		if( Tree.OpenSave(Stream) ) {

			Tree.SetFile(Name);

			afxThread->SetWaitMode(TRUE);

			Tree.PutObject(L"C3Data");

			Save(Tree);

			Tree.EndObject();

			Tree.Close();

			UINT uMode = (m_Locked > 0) ? saveSecure : saveCompress;

			if( Stream.SaveSuccessful() && Stream.SaveToFile(Name, uMode) ) {

				afxThread->SetWaitMode(FALSE);

				return TRUE;
			}

			afxThread->SetWaitMode(FALSE);
		}
	}

	return FALSE;
}

BOOL CDatabase::SaveAuto(CFilename const &Name)
{
	m_Source = m_Filename;

	if( SaveCopy(Name) ) {

		m_Source.Empty();

		m_fMucky = FALSE;

		return TRUE;
	}

	m_Source.Empty();

	return FALSE;
}

BOOL CDatabase::SaveImage(CFilename const &Name)
{
	FILE *pFile = _wfopen(Name, L"wb");

	if( pFile ) {

		DWORD dwPos = ftell(pFile);

		for( ;;) {

			m_fNew      = FALSE;

			INDEX Index = GetHeadHandle();

			do {
				CInitData InitData;

				InitData.SetAlign(TRUE);

				CItem *pItem = GetHandleItem(Index);

				pItem->MakeInitData(InitData);

				UINT uItem  = GetHandleValue(Index);

				UINT uClass = 0x1234;

				UINT uSize  = InitData.GetCount();

				if( uItem ) {

					InitData.Compress();
				}

				UINT   uComp = InitData.GetCount();

				PCBYTE pData = InitData.GetPointer();

				fwrite(&uItem, sizeof(uItem), 1, pFile);

				fwrite(&uClass, sizeof(uClass), 1, pFile);

				fwrite(&uSize, sizeof(uSize), 1, pFile);

				fwrite(&uComp, sizeof(uComp), 1, pFile);

				fwrite(pData, sizeof(BYTE), uComp, pFile);

			} while( GetNextHandle(Index) );

			UINT uItem = 0;

			fwrite(&uItem, sizeof(uItem), 1, pFile);

			if( HasNewHandles() ) {

				fseek(pFile, dwPos, SEEK_SET);

				continue;
			}

			break;
		}

		fclose(pFile);

		return TRUE;
	}

	return FALSE;
}

void CDatabase::SetConfig(CString Config)
{
	m_Config = Config;
}

void CDatabase::ClearFilename(void)
{
	m_Filename.Empty();
}

BOOL CDatabase::RecoverFilename(void)
{
	m_Filename = m_Source;

	if( !m_Filename.IsEmpty() ) {

		CString Name = m_Filename.GetBareName();

		CString Type = m_Filename.GetType();

		Name = CString(IDS_RECOVERED_COPY_OF) + Name;

		m_Filename.ChangeName(Name);

		m_Filename.ChangeType(Type);

		return TRUE;
	}

	return FALSE;
}

void CDatabase::SetImage(BOOL fImage)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Core");

	Key.MoveTo(L"Database");

	Key.SetValue(L"Image", DWORD(fImage));

	m_Image = fImage;
}

void CDatabase::SetImageDefault(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Core");

	Key.MoveTo(L"Database");

	m_Image = Key.GetValue(L"Image", C3OemFeature(L"IncludeImage", TRUE) ? UINT(1) : UINT(0));
}

void CDatabase::ClearDirty(void)
{
	m_fDirty = FALSE;
}

void CDatabase::SetDirty(void)
{
	m_fDirty = TRUE;

	m_fMucky = TRUE;
}

void CDatabase::ClearReadOnly(void)
{
	m_fReadOnly = FALSE;
}

void CDatabase::SetHumanName(CString Human)
{
	if( Human.GetLength() ) {

		C3OemStrings(Human);

		m_Human = Human;
	}
}

void CDatabase::AddFlag(CString Name)
{
	m_Flags.Insert(Name);
}

void CDatabase::RemFlag(CString Name)
{
	m_Flags.Remove(Name);
}

void CDatabase::ResetCategories(void)
{
	m_Cats.Empty();
}

void CDatabase::AddNavCategory(ENTITY Name, CString Tag, DWORD Image1, DWORD Image2, CString Res)
{
	CCategory Cat;

	Cat.m_pDbase = this;
	Cat.m_Name   = Name;
	Cat.m_Tag    = Tag;
	Cat.m_Res    = Res;
	Cat.m_Image1 = Image1;
	Cat.m_Image2 = Image2;

	m_Cats.Append(Cat);
}

void CDatabase::AddResCategory(ENTITY Name, CString Tag, DWORD Image1, DWORD Image2)
{
	CCategory Cat;

	Cat.m_pDbase = this;
	Cat.m_Name   = Name;
	Cat.m_Tag    = Tag;
	Cat.m_Image1 = Image1;
	Cat.m_Image2 = Image2;

	m_Cats.Append(Cat);
}

void CDatabase::AddResCategory(ENTITY Name, CString Tag, DWORD Image1, DWORD Image2, CDatabase *pDbase)
{
	CCategory Cat;

	Cat.m_pDbase = pDbase;
	Cat.m_Name   = Name;
	Cat.m_Tag    = Tag;
	Cat.m_Image1 = Image1;
	Cat.m_Image2 = Image2;

	m_Cats.Append(Cat);
}

void CDatabase::AddCategory(CCategory const &Cat)
{
	m_Cats.Append(Cat);
}

void CDatabase::SetRecomp(BOOL fRecomp)
{
	if( !fRecomp || !m_fBlock ) {

		m_Recomp = fRecomp;

		SetDirty();
	}
}

void CDatabase::SetCircle(BOOL fCircle)
{
	if( !fCircle || !m_fBlock ) {

		m_Circle = fCircle;

		SetDirty();
	}
}

void CDatabase::SetPending(BOOL fPending)
{
	if( !fPending || !m_fBlock ) {

		m_Pending = fPending;

		SetDirty();
	}
}

void CDatabase::BlockErrors(BOOL fBlock)
{
	m_fBlock = fBlock;
}

BOOL CDatabase::EditSecurity(void)
{
	if( IsPrivate() || IsDownloadOnly() ) {

		BOOL fRead;

		if( !CheckPassword(fRead) ) {

			return FALSE;
		}
	}

	CSecurityDialog Dlg(m_Locked,
			    m_Password,
			    m_Email
	);

	if( Dlg.Execute() ) {

		m_Locked   = Dlg.GetLocked();

		m_Password = Dlg.GetPassword();

		m_Email    = Dlg.GetEmail();

		SetDirty();

		return TRUE;
	}

	return FALSE;
}

// Class Aliasing

BOOL CDatabase::AliasModel(CString &Model, CString &Value)
{
	if( Model == L"Test" ) {

		Model = L"G310V2";

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"DSPSX" ) {

		Model = Model + L"Q";

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"DSPGT" ) {

		Model = Model + L"V";

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model ==L"MC2SX" ) {

		Model = Model + L"Q";

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"MC2GT" ) {

		Model = Model + L"V";

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"GCQ" ||Model == L"GCV" || Model == L"GCW" ) {

		Model.Insert(2, L'E');

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"C04" || Model == L"C07" || Model == L"C10" ) {

		Model.Insert(1, L'O');

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"DA10" ) {

		Model.Insert(4, L'D');

		Value = Model + L"Item";

		return TRUE;
	}

	if( Model == L"DA30WQ" || Model == L"DA30WV" || Model == L"DA30WX" ) {

		Model.Insert(4, L'D');

		Value = Model + L"Item";

		return TRUE;
	}

	return FALSE;
}

CLASS CDatabase::AliasClass(CString const &Name)
{
	if( Name == L"ProjectItem" ) {

		return NULL;
	}

	return AfxNamedClass(CEntity(L"C" + Name));
}

// Item Handles

UINT CDatabase::AllocHandle(CItem *pItem)
{
	for( INT nItem = m_nAlloc;; nItem++ ) {

		if( m_Map.Failed(m_Map.FindName(nItem)) ) {

			m_Map.Insert(nItem, pItem);

			m_nAlloc = nItem + 1;

			m_fNew   = TRUE;

			return UINT(nItem);
		}
	}

	return 0;
}

BOOL CDatabase::RegisterHandle(UINT uItem, CItem *pItem)
{
	INT nItem = INT(uItem);

	if( !pItem ) {

		m_Map.Remove(nItem);

		MakeMin(m_nAlloc, nItem);

		return TRUE;
	}

	if( nItem != HANDLE_NONE ) {

		if( m_Map.Insert(nItem, pItem) ) {

			return TRUE;
		}
	}

	return FALSE;
}

INDEX CDatabase::GetHeadHandle(void)
{
	m_fNew = FALSE;

	return m_Map.GetHead();
}

BOOL CDatabase::GetNextHandle(INDEX &Index)
{
	return m_Map.GetNext(Index);
}

UINT CDatabase::GetHandleValue(INDEX Index)
{
	return m_Map.GetName(Index);
}

CItem * CDatabase::GetHandleItem(INDEX Index)
{
	return m_Map.GetData(Index);
}

CItem * CDatabase::GetHandleItem(UINT uItem)
{
	INT   nItem = INT(uItem);

	INDEX Index = m_Map.FindName(nItem);

	if( !m_Map.Failed(Index) ) {

		return GetHandleItem(Index);
	}

	return NULL;
}

BOOL CDatabase::HasNewHandles(void)
{
	return m_fNew;
}

// Fixed Names

UINT CDatabase::AllocFixedInt(void)
{
	return m_uFixAlloc++;
}

CString CDatabase::AllocFixedString(void)
{
	return CPrintf(L"%8.8X", m_uFixAlloc++);
}

void CDatabase::CheckFixed(UINT &uFixed)
{
	if( !uFixed ) {

		uFixed = AllocFixedInt();

		return;
	}

	MakeMax(m_uFixAlloc, uFixed + 1);
}

// Item Naming

CString CDatabase::GetTitleName(void) const
{
	return m_Human;
}

CString CDatabase::GetHumanName(void) const
{
	return L"";
}

CString CDatabase::GetFixedName(void) const
{
	return CString();
}

// Persistance

void CDatabase::Init(void)
{
	CMetaItem::Init();

	m_fDirty = FALSE;

	m_fMucky = FALSE;
}

void CDatabase::Load(CTreeFile &Tree, CString Force)
{
	BOOL fForce = !Force.IsEmpty();

	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			if( pMeta->GetTag() == L"Config" ) {

				m_Config = Tree.GetValueAsString();

				continue;
			}

			if( pMeta->GetTag() == L"System" ) {

				Tree.GetObject();

				Tree.GetName();

				CString Value, Model;

				if( fForce ) {

					Value = Force + L"Item";

					Model = Force;
				}
				else {
					Value = Tree.GetValueAsString();

					Model = Value.Left(Value.GetLength() - 4);
				}

				AliasModel(Model, Value);

				m_Model = Model;

				m_Human = Model;

				C3OemStrings(m_Human);

				if( LoadDLL(Model) ) {

					CLASS Class = AliasClass(Value);

					if( Class ) {

						CItem *pItem = AfxNewObject(CItem, Class);

						pMeta->GetObject(this) = pItem;

						pItem->SetParent(this);

						pItem->Load(Tree);
					}
				}

				Tree.EndObject();

				continue;
			}

			LoadProp(Tree, pMeta);

			continue;
		}
	}
}

void CDatabase::Save(CTreeFile &Tree)
{
	m_Build = CPrintf(L"%d", C3_BUILD);

	CMetaItem::Save(Tree);
}

// Property Filters

BOOL CDatabase::SaveProp(CString const &Tag) const
{
	if( Tag == L"Source" ) {

		return !m_Source.IsEmpty();
	}

	return CMetaItem::SaveProp(Tag);
}

// UI Management

CViewWnd * CDatabase::CreateView(UINT uType)
{
	return m_pSystem->CreateView(uType);
}

// Download Support

BOOL CDatabase::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	m_pSystem->MakeInitData(Init);

	return TRUE;
}

// Download Config

CString CDatabase::GetDownloadConfig(void)
{
	CString Config = L"Default";

	if( GetSystemItem() ) {

		Config = GetSystemItem()->GetDownloadConfig();
	}

	return Config;
}

// Meta Data Creation

void CDatabase::AddMetaData(void)
{
	Meta_AddGuid(Guid);
	Meta_AddInteger(Version);
	Meta_AddString(Config);
	Meta_AddString(Build);
	Meta_AddString(Source);
	Meta_AddInteger(Recomp);
	Meta_AddInteger(Circle);
	Meta_AddInteger(Pending);
	Meta_AddInteger(Image);
	Meta_AddInteger(Locked);
	Meta_AddString(Password);
	Meta_AddString(Email);
	Meta_AddVirtual(System);
}

// Implementation

void CDatabase::Construct(void)
{
	m_Version   = 3100;

	m_pSystem   = NULL;

	m_Recomp    = FALSE;

	m_Circle    = FALSE;

	m_Pending   = FALSE;

	m_Locked    = 0;

	m_fBlock    = FALSE;

	m_Config    = C3OemCompany();

	m_pDbase    = this;

	m_fReadOnly = FALSE;

	m_fDirty    = FALSE;

	m_fMucky    = FALSE;

	m_nAlloc    = 2;

	m_Fixed     = 0;

	m_uFixAlloc = 1;

	m_fNew      = FALSE;

	m_pImage    = New CFileImage;

	m_Guid.CreateUnique();

	m_pImage->SetParent(this);

	m_Map.Insert(HANDLE_IMAGE, m_pImage);

	SetImageDefault();
}

BOOL CDatabase::CheckReadOnly(CFilename const &Name)
{
	if( GetFileAttributes(Name) & FILE_ATTRIBUTE_READONLY ) {

		HideSplash();

		afxMainWnd->Information(CString(IDS_FILE_READ_ONLY_1) +
					CString(IDS_FILE_READ_ONLY_2)
		);

		return TRUE;
	}

	return FALSE;
}

BOOL CDatabase::LoadDLL(CString Model)
{
	return TRUE;
}

void CDatabase::FreeDLL(void)
{
}

BOOL CDatabase::HideSplash(void)
{
	HWND hWnd = FindWindow(L"AfxWndClass", L"CrimsonSplash");

	if( hWnd ) {

		CWnd &Wnd = CWnd::FromHandle(hWnd);

		Wnd.DestroyWindow(TRUE);

		return TRUE;
	}

	return FALSE;
}

BOOL CDatabase::CheckPassword(BOOL &fRead)
{
	CPasswordDialog Dlg(m_Locked == 1);

	if( Dlg.Execute() ) {

		CString Pass = Dlg.GetData();

		if( Pass.IsEmpty() ) {

			fRead = TRUE;

			return TRUE;
		}

		if( !Pass.CompareC(m_Password) ) {

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
