#include "intern.hpp"

#include "mitaudpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi A UDP Master Driver
//

// Instantiator

INSTANTIATE(CMitAQUDPMaster);

// Constructor

CMitAQUDPMaster::CMitAQUDPMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CMitAQUDPMaster::~CMitAQUDPMaster(void)
{
	}

BOOL CMitAQUDPMaster::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}
	
	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		m_pCtx->m_pSock->SetOption(OPT_RECV_QUEUE, sizeof(m_bRxBuff));

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

// End of File
