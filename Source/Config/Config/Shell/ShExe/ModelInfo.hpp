
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Web Registration Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ModelInfo_HPP

#define INCLUDE_ModelInfo_HPP

//////////////////////////////////////////////////////////////////////////
//
// Model Information Library
//

class CModelInfo
{
	public:
		// Library Management
		static void LoadLib(void);
		static void FreeLib(void);

		// Operations
		static BOOL CheckModel(CString &Model);
		static BOOL SelectModel(UINT uMode, CString &Model);

	protected:
		// Static Data
		static HMODULE m_hLib;
	};

// End of File

#endif
