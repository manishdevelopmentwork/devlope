#include "intern.hpp"

#include "s7mpie.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S7 PLC Extended DB TCP Master Driver
//

// Instantiator

INSTANTIATE(CS7ExtTcpMasterDriver);

// Constructor

CS7ExtTcpMasterDriver::CS7ExtTcpMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pCtx = NULL;

	m_uKeep = 0;   
	}

// Destructor

CS7ExtTcpMasterDriver::~CS7ExtTcpMasterDriver(void)
{

	}

CCODE MCALL CS7ExtTcpMasterDriver::Ping(void)
{
	DWORD IP;

	if( !m_pCtx->m_fAux ) {

		IP = m_pCtx->m_IP1;
		}
	else
		IP = m_pCtx->m_IP2;

	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP1, m_pCtx->m_uTime2) < NOTHING ) {

		return CCODE_SUCCESS;
		}

	if( m_pCtx->m_IP2 ) {

		m_pCtx->m_fAux = !m_pCtx->m_fAux;
		}

	return CCODE_ERROR; 
	}

void CS7ExtTcpMasterDriver::AddParams(BYTE bService, UINT uType, UINT uTable, UINT uAddr, UINT uExtra, UINT uCount, AREF Addr)
{
	BYTE  bType  = FindType(uType, uTable);

	WORD  wBlock = 0;

	if( uTable == SPACE_DATA ) {

		wBlock = DATA_BLOCK(uExtra, Addr.a.m_Table) + m_pCtx->m_BlkOff;
		}

	BYTE  bArea  = FindArea(uTable);

	DWORD dwAddr = FindAddr(uType, uTable, uAddr);

	m_bParamHead = m_uPtr;

	UINT uBlocks = 1;

	AddByte(bService);		// SERVICE_ID

	AddByte(LOBYTE(uBlocks));	// NO_VAR

	for( UINT i = 0; i < uBlocks; i++ ) {

		AddByte(0x12);		// VAR_SPC
		AddByte(0x0A);		// VADDR_LG
		AddByte(0x10);		// SYNTAX_ID

		AddByte(bType);		// Type
		AddWord(uCount);	// Elements
		AddWord(wBlock);	// Block
		AddByte(bArea);		// Area

		AddByte(LOBYTE(HIWORD(dwAddr)));
		AddByte(HIBYTE(LOWORD(dwAddr)));
		AddByte(LOBYTE(LOWORD(dwAddr)));

		dwAddr++;
		}

	m_bParamTail = m_uPtr;
	}

BYTE CS7ExtTcpMasterDriver::FindType(UINT uType, UINT uTable)
{
	switch( uTable ) {

		case SPACE_TIMER:		return ID_T;
		case SPACE_COUNTER:		return ID_C;
		}

	switch( uType ) {

		case addrBitAsBit:	return TC_BOOL;
		case addrByteAsByte:	return TC_BYTE;
		case addrByteAsWord:	return TC_WORD;
		case addrByteAsLong:	return TC_DWORD;
		case addrByteAsReal:	return TC_DWORD;
		}

	return 0;
	}

BYTE CS7ExtTcpMasterDriver::FindArea(UINT uTable)
{
	switch( uTable ) {

		case SPACE_DATA:	return ID_DB;
		case SPACE_OUTPUT:	return ID_QB;
		case SPACE_INPUT:	return ID_IB;
		case SPACE_FLAG:	return ID_MB;
		case SPACE_TIMER:	return ID_T;
		case SPACE_COUNTER:	return ID_C;
		}

	return 0;
	}

UINT CS7ExtTcpMasterDriver::FindBits(UINT uType, UINT uTable)
{
	switch( uTable ) {

		case SPACE_TIMER:	return 8 * 2;
		case SPACE_COUNTER:	return 8 * 2;
		}

	switch( uType ) {

		case addrBitAsBit:	return 1;

		case addrByteAsByte:	return 8 * 1;
		case addrByteAsWord:	return 8 * 2;
		case addrByteAsLong:	return 8 * 4;
		case addrByteAsReal:	return 8 * 4;
		}

	return 0;
	}

void CS7ExtTcpMasterDriver::AddPadding(UINT uTable)
{
	}

BOOL  CS7ExtTcpMasterDriver::IsTimer(UINT uTable)
{
	return uTable == SPACE_TIMER;
	}

BOOL  CS7ExtTcpMasterDriver::IsCounter(UINT uTable)
{
	return uTable == SPACE_COUNTER;
	}

BOOL CS7ExtTcpMasterDriver::EatWrite(UINT uTable)
{
	switch( uTable ) {

		case SPACE_INPUT:
				
			return TRUE;
		}

	return FALSE;
	}

UINT CS7ExtTcpMasterDriver::GetTable(UINT uTable)
{
	return uTable & 0x7;
	}

UINT CS7ExtTcpMasterDriver::GetMaxBits(void)
{
	return 1600;
	}

// End of File
