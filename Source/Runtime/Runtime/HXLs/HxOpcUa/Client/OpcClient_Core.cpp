
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Instantiator

static IUnknown * Create_OpcUaClient(PCTXT pName)
{
	return New COpcClient;
}

// Registration

global void Register_OpcUaClient(void)
{
	piob->RegisterInstantiator("opcua.opcua-client", Create_OpcUaClient);
}

global void Revoke_OpcUaClient(void)
{
	piob->RevokeInstantiator("opcua.opcua-client");
}

// Constructor

COpcClient::COpcClient(void)
{
	StdSetRef();

	m_uDebug = 0;

	InitSecurity();
}

// Destructor

COpcClient::~COpcClient(void)
{
}

// IUnknown

HRESULT COpcClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IOpcUaClient);

	StdQueryInterface(IOpcUaClient);

	return E_NOINTERFACE;
}

ULONG COpcClient::AddRef(void)
{
	StdAddRef();
}

ULONG COpcClient::Release(void)
{
	StdRelease();
}

// IOpcUaClient

void COpcClient::OpcFree(void)
{
	delete this;
}

void COpcClient::OpcLoad(COpcUaClientConfig const &Config)
{
	// TODO -- Add timeouts? !!!

	m_uDebug = (Config.m_uSize > offset(COpcUaClientConfig, m_uDebug)) ? Config.m_uDebug : 0;
}

bool COpcClient::OpcInit(void)
{
	BaseInit();

	return true;
}

bool COpcClient::OpcStop(void)
{
	BaseStop();

	return true;
}

bool COpcClient::OpcTerm(void)
{
	while( !m_Devices.IsEmpty() ) {

		CloseDevice(m_Devices.GetHead());
	}

	BaseTerm();

	return true;
}

UINT COpcClient::OpcOpenDevice(PCTXT pHost, PCTXT pUser, PCTXT pPass, CStringArray const &Nodes)
{
	INDEX Index = OpenDevice(pHost, pUser, pPass);

	if( Index ) {

		CDevice *pDevice = m_Devices[Index];

		for( UINT n = 0; n < Nodes.GetCount(); n++ ) {

			COpcNodeId Node;

			if( Node.Decode(Nodes[n]) ) {

				pDevice->m_Nodes.Append(Node);

				pDevice->m_Types.Append(0);
			}
			else {
				pDevice->m_Nodes.Append(COpcNodeId(0, 0));

				pDevice->m_Types.Append(0);
			}
		}

		return UINT(Index);
	}

	return 0;
}

bool COpcClient::OpcCloseDevice(UINT uDevice)
{
	if( uDevice ) {

		CloseDevice(INDEX(uDevice));

		return true;
	}

	return false;
}

bool COpcClient::OpcPingDevice(UINT uDevice)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for( ;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				return true;
			}

			if( !fBusy ) {

				Sleep(250);

				return false;
			}
		}
	}

	return false;
}

bool COpcClient::OpcBrowseDevice(UINT uDevice, CStringArray &List)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for( ;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				if( Browse(pDevice, List) ) {

					List.Sort();

					return true;
				}

				return false;
			}

			if( !fBusy ) {

				Sleep(250);

				return false;
			}
		}
	}

	return false;
}

bool COpcClient::OpcReadData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD pData)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for( ;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				return ReadValues(pDevice, pList, uList, pType, pData);
			}

			if( !fBusy ) {

				Sleep(250);

				return false;
			}
		}
	}

	return false;
}

bool COpcClient::OpcReadArray(UINT uDevice, UINT uNode, UINT uType, UINT uCount, PDWORD pData)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for( ;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				return ReadArray(pDevice, uNode, uType, uCount, pData);
			}

			if( !fBusy ) {

				Sleep(250);

				return false;
			}
		}
	}

	return false;
}

bool COpcClient::OpcWriteData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		BOOL     fRead   = FALSE;

		for( ;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				if( !HasTypes(pDevice, pList, uList) ) {

					if( !fRead ) {

						CAutoArray<UINT>  pReadType(uList);

						CAutoArray<DWORD> pReadData(uList);

						if( ReadValues(pDevice, pList, uList, pReadType, pReadData) ) {

							fRead = TRUE;

							continue;
						}
					}

					return false;
				}

				return WriteValues(pDevice, pList, uList, pType, pData);
			}

			if( !fBusy ) {

				return false;
			}
		}
	}

	return false;
}

bool COpcClient::OpcWriteArray(UINT uDevice, UINT uNode, UINT uType, UINT uStart, UINT uCount, PCDWORD pData)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		BOOL     fRead   = FALSE;

		for( ;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				if( !HasType(pDevice, uNode) ) {

					if( !fRead ) {

						CAutoArray<DWORD> pReadData(uCount);

						if( ReadArray(pDevice, uNode, uType, uCount, pReadData) ) {

							fRead = TRUE;

							continue;
						}
					}

					return false;
				}

				return WriteArray(pDevice, uNode, uType, uStart, uCount, pData);
			}

			if( !fBusy ) {

				return false;
			}
		}
	}

	return false;
}

// Implementation

void COpcClient::InitSecurity(void)
{
	m_Certificate.Length = -1;
	m_Certificate.Data   = NULL;

	m_PrivateKey.Type          = OpcUa_Crypto_KeyType_Invalid;
	m_PrivateKey.Key.Length    = 0;
	m_PrivateKey.Key.Data      = PBYTE("");
	m_PrivateKey.fpClearHandle = NULL;

	m_PkiConfig.PkiType                           = OpcUa_NO_PKI;
	m_PkiConfig.CertificateTrustListLocation      = NULL;
	m_PkiConfig.CertificateRevocationListLocation = NULL;
	m_PkiConfig.CertificateUntrustedListLocation  = NULL;
	m_PkiConfig.Flags                             = 0;
	m_PkiConfig.Override                          = NULL;
}

bool COpcClient::HasType(CDevice *pDevice, UINT uNode)
{
	if( !pDevice->m_Types[uNode] ) {

		return false;
	}

	return true;
}

bool COpcClient::HasTypes(CDevice *pDevice, PCUINT pList, UINT uList)
{
	for( UINT n = 0; n < uList; n++ ) {

		UINT uNode = pList[n];

		if( !pDevice->m_Types[uNode] ) {

			return false;
		}
	}

	return true;
}

void COpcClient::FreeDiagnosticInfo(INT NoOfDiagnosticInfos, OpcUa_DiagnosticInfo *pDiagnosticInfos)
{
	{
		for( INT n = 0; n < NoOfDiagnosticInfos; n++ ) {

			OpcUa_DiagnosticInfo *pInfo = pDiagnosticInfos + n;

			OpcUa_DiagnosticInfo_Clear(pInfo);
		}
	}

	OpcUa_Free(pDiagnosticInfos);
}

CString COpcClient::Format(C3INT r)
{
	return CPrintf("%d", r);
}

CString COpcClient::Format(C3REAL r)
{
	char s[32];

	PTXT w = s;

	if( r < 0 ) {

		r    = -r;

		*w++ = '-';
	}

	gcvt(r, 5, w);

	char *p = strchr(w, 'e');

	if( p ) {

		p[0] = 'E';

		if( p[2] == '0' ) {

			if( p[3] == '0' ) {

				p[2] = p[4];
				p[3] = 0;
			}
			else {
				p[2] = p[3];
				p[3] = p[4];
				p[4] = 0;
			}
		}

		if( *--p == '.' ) {

			memmove(p, p+1, strlen(p));
		}
	}
	else {
		int n = strlen(s);

		if( n ) {

			if( s[n-1] == '.' ) {

				s[n-1] = 0;
			}
		}
	}

	return s;
}

// Debugging

void COpcClient::AfxTrace(PCTXT pText, ...)
{
	if( m_uDebug >= 1 ) {

		va_list pArgs;

		va_start(pArgs, pText);

		::AfxTraceArgs(pText, pArgs);

		va_end(pArgs);
	}
}

// End of File
