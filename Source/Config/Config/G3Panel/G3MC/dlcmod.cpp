
#include "intern.hpp"

#include "legacy.h"

#include "dlcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Module
//

// Dynamic Class

AfxImplementDynamicClass(CDLCModule, CGenericModule);

// Constructor

CDLCModule::CDLCModule(void)
{
	m_pMapper = New CDLCMapper;

	m_pLoop1  = New CPIDLoop(1);

	m_pLoop2  = New CPIDLoop(1);

	m_Ident   = ID_CSPID2;

	m_FirmID  = FIRM_PID2;

	m_Model   = "CSPID2";
	
	m_Conv.Insert(L"Graphite", L"CGMPID2Module");

	m_Conv.Insert(L"Manticore", L"CDAPID2Module");
	}

// UI Management

CViewWnd * CDLCModule::CreateMainView(void)
{
	return New CDLCMainWnd;
	}

// Comms Object Access

UINT CDLCModule::GetObjectCount(void)
{
	return 3;
	}

BOOL CDLCModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
		       	Data.ID    = OBJ_LOOP_1;

			Data.Name  = CString(IDS_MODULE_LOOP1);	
			
			Data.pItem = m_pLoop1;	

			return TRUE;
			
		case 1:
			Data.ID    = OBJ_LOOP_2;
			
			Data.Name  = CString(IDS_MODULE_LOOP2);
			
			Data.pItem = m_pLoop2;

			return TRUE;

		case 2:
			Data.ID    = OBJ_MAPPER;

			Data.Name  = CString(IDS_MODULE_OUTPUTS);
			
			Data.pItem = m_pMapper;

			return TRUE;

		}

	return FALSE;
	}

// Conversion

BOOL CDLCModule::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pLoop1 ->Convert(pValue->GetChild(L"Loop1"));

		m_pLoop2 ->Convert(pValue->GetChild(L"Loop2"));

		m_pMapper->Convert(pValue->GetChild(L"Mapper"));

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CDLCModule::AddMetaData(void)
{
	Meta_AddObject(Mapper);
	Meta_AddObject(Loop1 );	
	Meta_AddObject(Loop2 );	

	CGenericModule::AddMetaData();
	}

// Download Support

void CDLCModule::MakeConfigData(CInitData &Init)		
{
	MakeConfigData(Init, m_pLoop1);
	
	MakeConfigData(Init, m_pLoop2);
	}

void CDLCModule::MakeConfigData(CInitData &Init, CPIDLoop *m_pLoop)
{		
	if( m_pLoop->m_InputType == INPUT_RTD || m_pLoop->m_InputType == INPUT_TC ) {

		Init.AddWord(WORD(m_pLoop->m_TempUnits));

		Init.AddWord(0);

		Init.AddWord(0);

		Init.AddWord(0);
		}
	else {
		Init.AddWord(0xFFFF);

		Init.AddWord(WORD(m_pLoop->m_ProcMin));

		Init.AddWord(WORD(m_pLoop->m_ProcMax));

		Init.AddWord(0);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDLCMainWnd, CProxyViewWnd);

// Constructor

CDLCMainWnd::CDLCMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overribables

void CDLCMainWnd::OnAttach(void)
{
	m_pItem = (CDLCModule *) CProxyViewWnd::m_pItem;

	AddPIDPages(m_pItem->m_pLoop1, L"L1");
	
	AddPIDPages(m_pItem->m_pLoop2, L"L2");
	
	AddMapPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation			   	

void CDLCMainWnd::AddPIDPages(CPIDLoop *pLoop, CString Loop)  
{
	for( UINT n = 0; n < pLoop->GetPageCount(); n++ ) {

		CString    Name  = Loop + L" " + pLoop->GetPageName(n);

		CViewWnd * pPage = pLoop->CreatePage(n);

		m_pMult->AddView(Name , pPage);

		pPage->Attach(pLoop);
		}
	}

void CDLCMainWnd::AddMapPages(void)
{
	CDLCMapper *pMapper = m_pItem->m_pMapper;

	for( UINT n =0; n < pMapper->GetPageCount(); n++ ) {			   

		CString    Name  = pMapper->GetPageName(n);

		CViewWnd * pPage = pMapper->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pMapper);
		}
	}

// End of File
