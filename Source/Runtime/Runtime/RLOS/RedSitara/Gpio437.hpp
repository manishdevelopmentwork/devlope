
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Gpio437_HPP
	
#define	INCLUDE_AM437_Gpio437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 GPIO Module
//

class CGpio437 : public IGpio, public IEventSink
{
	public:
		// Constructor
		CGpio437(UINT iIndex);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IGpio
		void METHOD SetDirection(UINT n, BOOL fOutput);
		void METHOD SetState(UINT n, BOOL fState);
		BOOL METHOD GetState(UINT n);
		void METHOD EnableEvents(void);
		void METHOD SetIntHandler(UINT n, UINT uType, IEventSink *pSink, UINT uParam);
		void METHOD SetIntEnable(UINT n, BOOL fEnable);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);
	
	protected:
		// Registers
		enum
		{
			regREV		= 0x0000 / sizeof(DWORD),
			regCFG		= 0x0010 / sizeof(DWORD),
			regEOI		= 0x0020 / sizeof(DWORD),
			regIRQSTS0RAW	= 0x0024 / sizeof(DWORD),
			regIRQSTS1RAW	= 0x0028 / sizeof(DWORD),
			regIRQSTS0	= 0x002C / sizeof(DWORD),
			regIRQSTS1	= 0x0030 / sizeof(DWORD),
			regIRQSTSSET0	= 0x0034 / sizeof(DWORD),
			regIRQSTSSET1	= 0x0038 / sizeof(DWORD),
			regIRQSTSCLR0	= 0x003C / sizeof(DWORD),
			regIRQSTSCLR1	= 0x0040 / sizeof(DWORD),
			regIRQWAKEN0	= 0x0044 / sizeof(DWORD),
			regIRQWAKEN1	= 0x0048 / sizeof(DWORD),
			regSTS		= 0x0114 / sizeof(DWORD),
			regCTRL		= 0x0130 / sizeof(DWORD),
			regOE		= 0x0134 / sizeof(DWORD),
			regDATAIN	= 0x0138 / sizeof(DWORD),
			regDATAOUT	= 0x013C / sizeof(DWORD),
			regLEVEL0	= 0x0140 / sizeof(DWORD),
			regLEVEL1	= 0x0144 / sizeof(DWORD),
			regRISING	= 0x0148 / sizeof(DWORD),
			regFALLING	= 0x014C / sizeof(DWORD),
			regDEBOUNCE	= 0x0150 / sizeof(DWORD),
			regDEBOUNCETIME	= 0x0154 / sizeof(DWORD),
			regCLRDATA	= 0x0190 / sizeof(DWORD),
			regSETDATA	= 0x0194 / sizeof(DWORD)
			};

		// Sink Record
		struct CSink
		{
			IEventSink * m_pSink;
			UINT	     m_uParam;
			};
					
		// Data Members
		ULONG     m_uRefs;
		PVDWORD	  m_pBase;
		UINT      m_iIndex;
		UINT      m_uLine;
		CSink     m_Sink[32];

		// Implementation
		DWORD FindBase(UINT iIndex) const;
		UINT  FindLine(UINT iIndex) const;
	};

// End of File

#endif
