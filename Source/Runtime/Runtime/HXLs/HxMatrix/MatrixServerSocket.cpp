
#include "Intern.hpp"

#include "MatrixServerSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Matrix Server Socket
//

// Constructor

CMatrixServerSocket::CMatrixServerSocket(CMatrixSsl *pSsl, ISocket *pSock, sslKeys_t *pKeys) : CMatrixSocket(pSsl, pSock, pKeys)
{
	}

// Destructor

CMatrixServerSocket::~CMatrixServerSocket(void)
{
	}

// ISocket Methods

HRM CMatrixServerSocket::Listen(WORD Loc)
{
	if( UseExisting() ) {

		return S_OK;
		}

	if( m_pSock->Listen(Loc) == S_OK ) {

		m_uState = stateOpening;

		return S_OK;
		}

	return E_FAIL;
	}

// Overridables

void CMatrixServerSocket::InitSession(void)
{
	matrixSslNewServerSession( &m_pSession,
				    m_pKeys,
				    NULL,
				    m_pOptions
				    );

	PumpRecvData();
	}

// End of File
