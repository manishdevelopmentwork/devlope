
#include "intern.hpp"

#include "bacmstp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACnet Master Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CBACNetMSTP);

// Constructor

CBACNetMSTP::CBACNetMSTP(void)
{
	m_Ident     = DRIVER_ID;

	m_bThisDrop = 10;

	m_bLastDrop = 127;

	m_fOptim1   = FALSE;

	m_fOptim2   = FALSE;

	m_fHwDelay  = FALSE;
	}

// Destructor

CBACNetMSTP::~CBACNetMSTP(void)
{
	}

// Configuration

void MCALL CBACNetMSTP::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bThisDrop = GetByte(pData);

		m_bLastDrop = GetByte(pData);

		m_fOptim1   = GetByte(pData);

		m_fOptim2   = GetByte(pData);

		m_fTxFast   = GetByte(pData) ? TRUE : FALSE;

		m_fHwDelay  = GetByte(pData);
		}
	}
	
void MCALL CBACNetMSTP::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);

	Config.m_uFlags |= flagFastRx;
	}
	
// Management

void MCALL CBACNetMSTP::Attach(IPortObject *pPort)
{
	m_pHandler = new CBACNetHandler( m_bThisDrop,
					 m_bLastDrop,
					 m_fOptim1,
					 m_fOptim2,
					 m_fTxFast,
					 m_fHwDelay
					 );

	pPort->Bind(m_pHandler);
	}

void MCALL CBACNetMSTP::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CBACNetMSTP::Open(void)
{
	}

// Device

CCODE MCALL CBACNetMSTP::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx            = new CContext;

			m_pBase           = m_pCtx;

			m_pCtx->m_fMaster = GetByte(pData);

			m_pCtx->m_fBroke  = GetByte(pData);

			m_pCtx->m_uTime1  = GetWord(pData);

			LoadConfig(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CBACNetMSTP::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pBase->m_pObj;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBACNetMSTP::Ping(void)
{
	if( m_pHandler->IsOnline() ) {

		if( m_pCtx->m_fFound ) {

			return CCODE_SUCCESS;
			}

		if( CBACNetBase::Ping() == CCODE_SUCCESS ) {

			m_pCtx->m_bMac   = m_bRxMac;

			m_pCtx->m_fFound = TRUE;

			Show("ping", "found object id");

			return CCODE_SUCCESS;
			}
		}

	return CCODE_ERROR;
	}

// Transport Hooks

BOOL CBACNetMSTP::SendFrame(BOOL fThis, BOOL fReply)
{
	if( m_pHandler->IsOnline() ) {

		if( !fThis || m_pBase->m_fFound ) {

			AddNetworkHeader(fThis, fReply);

			Dump("tx", m_pTxBuff);

			PBYTE pData = m_pTxBuff->GetData();

			UINT  uSize = m_pTxBuff->GetSize();

			BYTE  bDrop = fThis ? m_pCtx->m_bMac : 0xFF;

			if( m_pHandler->AppSend(bDrop, pData, uSize, fReply) ) {

				m_pTxBuff->Release();

				m_pTxBuff = NULL;

				return TRUE;
				}
			else
				Show("send-frame", "send timeout");
			}
		else
			Show("send-frame", "unknown mac");
		}
	else
		Show("send-frame", "not online");

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBACNetMSTP::RecvFrame(void)
{
	if( m_pHandler->IsOnline() ) {

		m_pRxBuff = CreateBuffer(600, TRUE);

		if( m_pRxBuff ) {

			m_pRxBuff->AddTail(600);

			PBYTE pData = m_pRxBuff->GetData();

			UINT  uSize = m_pRxBuff->GetSize();

			UINT  uRecv = m_pHandler->AppRecv(m_bRxMac, pData, uSize, m_pCtx->m_uTime1);

			if( uRecv ) {

				m_pRxBuff->StripTail(uSize - uRecv);

				if( StripNetworkHeader() ) {

					Dump("rx", m_pRxBuff);

					return TRUE;
					}
				}
			else
				Show("recv-frame", "empty frame");

			RecvDone();
			}
		else
			Show("recv-frame", "can't alloc buffer");
		}
	else
		Show("recv-frame", "not online");

	return FALSE;
	}

// End of File
