
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CheckPCap_HPP

#define INCLUDE_CheckPCap_HPP

//////////////////////////////////////////////////////////////////////////
//
// WinPCap Checker
//

class CCheckPCap
{
public:
	// Operations
	static BOOL Check(BOOL fInstall);
};

// End of File

#endif
