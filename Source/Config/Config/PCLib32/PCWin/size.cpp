
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Size Object
//

// Standard Constructors

CSize::CSize(void)
{
	cx = 0;
	cy = 0;
	}

CSize::CSize(SIZE const &Src)
{
	cx = Src.cx;
	cy = Src.cy;
	}

CSize::CSize(int sx, int sy)
{
	cx = sx;
	cy = sy;
	}

// Explicit Constructors

CSize::CSize(CPoint const &Src)
{
	cx = Src.x;
	cy = Src.y;
	}

CSize::CSize(DWORD dwPacked)
{
	cx = LOWORD(dwPacked);
	cy = HIWORD(dwPacked);
	}

CSize::CSize(int nMetric)
{
	cx = GetSystemMetrics(nMetric + 0);
	cy = GetSystemMetrics(nMetric + 1);
	}

// Assignment Operators

CSize & CSize::operator = (SIZE const &Src)
{
	cx = Src.cx;
	cy = Src.cy;

	return ThisObject;
	}

CSize & CSize::operator = (CPoint const &Src)
{
	cx = Src.x;
	cy = Src.y;

	return ThisObject;
	}

// Conversions

CSize::operator SIZE * (void)
{
	return this;
	}

CSize::operator DWORD (void) const
{
	return MAKELONG(cx, cy);
	}

// Attributes

CSize CSize::GetSign(void) const
{
	return CSize(Sgn(cx), Sgn(cy));
	}
		
// Operations

void CSize::MakeSquare(void)
{
	CSize Sign = Normalize();
	
	if( cx < cy )
		cy = cx;
	else
		cx = cy;
		
	Denormalize(Sign);
	}

CSize CSize::Normalize(void)
{
	CSize Sign = GetSign();
	
	operator *= (Sign);
	
	return Sign;
	}

void CSize::Denormalize(CSize Sign)
{
	operator *= (Sign);
	}

// Compraison Operators

BOOL CSize::operator == (CSize const &Arg) const
{
	return BOOL(cx == Arg.cx && cy == Arg.cy);
	}

BOOL CSize::operator != (CSize const &Arg) const
{
	return BOOL(cx != Arg.cx || cy != Arg.cy);
	}

// Offset Operators

CSize const & CSize::operator ++ (void)
{
	cx++;
	cy++;
	
	return ThisObject;
	}

CSize CSize::operator ++ (int nDummy)
{
	CSize Prev = ThisObject;
	
	cx++;
	cy++;
	
	return Prev;
	}

CSize const & CSize::operator -- (void)
{
	cx--;
	cy--;
	
	return ThisObject;
	}
		
CSize CSize::operator -- (int nDummy)
{
	CSize Prev = ThisObject;
	
	cx--;
	cy--;
	
	return Prev;
	}
		
// Offset in Place

CSize const & CSize::operator += (int nStep)
{
	cx += nStep;
	cy += nStep;
	
	return ThisObject;
	}

CSize const & CSize::operator += (CSize const &Step)
{
	cx += Step.cx;
	cy += Step.cy;
	
	return ThisObject;
	}

CSize const & CSize::operator -= (int nStep)
{
	cx -= nStep;
	cy -= nStep;
	
	return ThisObject;
	}

CSize const & CSize::operator -= (CSize const &Step)
{
	cx -= Step.cx;
	cy -= Step.cy;
	
	return ThisObject;
	}

// Offset via Friends

CSize operator + (CSize const &a, int nStep)
{
	CSize Result;

	Result.cx = a.cx + nStep;
	Result.cy = a.cy + nStep;

	return Result;
	}

CSize operator + (CSize const &a, CSize const &b)
{
	CSize Result;

	Result.cx = a.cx + b.cx;
	Result.cy = a.cy + b.cy;

	return Result;
	}

CSize operator - (CSize const &a, int nStep)
{
	CSize Result;

	Result.cx = a.cx - nStep;
	Result.cy = a.cy - nStep;

	return Result;
	}

CSize operator - (CSize const &a, CSize const &b)
{
	CSize Result;

	Result.cx = a.cx - b.cx;
	Result.cy = a.cy - b.cy;

	return Result;
	}

// Scaling in Place

CSize const & CSize::operator *= (int nScale)
{
	cx *= nScale;
	cy *= nScale;
	
	return ThisObject;
	}

CSize const & CSize::operator *= (CSize const &Scale)
{
	cx *= Scale.cx;
	cy *= Scale.cy;
	
	return ThisObject;
	}

CSize const & CSize::operator /= (int nScale)
{
	cx /= nScale;
	cy /= nScale;
	
	return ThisObject;
	}

CSize const & CSize::operator /= (CSize const &Scale)
{
	cx /= Scale.cx;
	cy /= Scale.cy;
	
	return ThisObject;
	}

// Scaling via Friends

CSize operator * (CSize const &a, int nScale)
{
	CSize Result;

	Result.cx = a.cx * nScale;
	Result.cy = a.cy * nScale;

	return Result;
	}

CSize operator * (int nScale, CSize const &b)
{
	CSize Result;

	Result.cx = nScale * b.cx;
	Result.cy = nScale * b.cy;

	return Result;
	}

CSize operator * (CSize const &a, CSize const &b)
{
	CSize Result;

	Result.cx = a.cx * b.cx;
	Result.cy = a.cy * b.cy;

	return Result;
	}

CSize operator / (CSize const &a, int nScale)
{
	CSize Result;

	Result.cx = a.cx / nScale;
	Result.cy = a.cy / nScale;

	return Result;
	}

CSize operator / (CSize const &a, CSize const &b)
{
	CSize Result;

	Result.cx = a.cx / b.cx;
	Result.cy = a.cy / b.cy;

	return Result;
	}

// Zeroing

void AfxSetZero(CSize &s)
{
	s.cx = s.cy = 0;
	}

// End of File
