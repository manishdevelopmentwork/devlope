
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SmtpClient_HPP

#define	INCLUDE_SmtpClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MsgTransport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class  CJsonConfig;

class  CSmtpSession;

struct CSmtpConfig;

struct CStdClientConfig;

//////////////////////////////////////////////////////////////////////////
//
// SMTP Client Service
//

class CSmtpClient : public CMsgTransport, public IClientProcess
{
public:
	// Constructor
	CSmtpClient(CJsonConfig *pJson);

	// Destructor
	~CSmtpClient(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// IMsgTransport
	BOOL METHOD CanAcceptAddress(CString const &Addr);

protected:
	// Data Members
	ULONG			     m_uRefs;
	CSmtpConfig		   * m_pSmtpConfig;
	CStdClientConfig	   * m_pLinkConfig;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
};

// End of File

#endif
