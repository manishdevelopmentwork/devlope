
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Dnp3_HPP

#define	INCLUDE_Dnp3_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "MasterChannel.hpp"

#include "SlaveChannel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Structures
//

struct LOCK {

	void   * m_pLock;
	IMutex * m_pMutex;
};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Object
//

class CDnp3 : public IDnp
{
	public:
		// Constructor
		CDnp3(void);

		// Destructor
		~CDnp3(void);

		// Item Location
		static CDnp3 * Locate(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnp
		IDnpChannelConfig * METHOD GetConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO);
		BOOL		    METHOD Init(void);
		void		    METHOD Service(void);
		IDnpChannel *	    METHOD OpenChannel(IDataHandler *pHandler, WORD wSrc, BOOL fMaster);
		IDnpChannel *	    METHOD OpenChannel(IDnpChannelConfig *pSocket, WORD wSrc, BOOL fMaster);
		BOOL		    METHOD CloseChannel(void *pVoid, WORD wSrc, BOOL fMaster);
		IDnpSession *	    METHOD OpenSession(IDnpChannel *pChannel, WORD wDest, WORD wTO, DWORD dwLink, void *pCfg);
		BOOL		    METHOD CloseSession(IDnpSession *pSession);
		void		    METHOD SetColdStart(void);
		void		    METHOD SetWarmStart(void);
		void		    METHOD InitLock(void *pLock);
		void		    METHOD Lock(void *pLock);
		void		    METHOD Free(void *pLock);
		void		    METHOD DeleteLock(void *pLock);

	protected:
		// Static Data
		static CDnp3 * m_pThis;

		// Data
		ULONG		m_uRefs;
		LOCK		m_pLock[128];

		// Data Members
		BOOL		m_fInit;
		BYTE		m_bChan;
		CChannel **	m_pChannels;

		// Implementation
		CChannel * MakeChannel(IDataHandler *pHand, WORD wSrc, BOOL fMaster);
		CChannel * MakeChannel(IDnpChannelConfig *pConfig, WORD wSrc, BOOL fMaster);
		BOOL	   RecordChannel(CChannel *pChan);
		CChannel * FindChannel(void *pVoid, WORD wSrc, BOOL fMaster);

		// Helpers
		void   InitLocks(void);
		LOCK * FindLock(void * pLock);
};

// End of File

#endif
