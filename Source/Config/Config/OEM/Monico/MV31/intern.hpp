
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Proxy Application
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Header Control
//

#define	UNICODE		TRUE

#define	NO_STRICT	TRUE

#define	_WIN32_WINNT	0x0501

#define	WINVER		0x0501

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <windows.h>

#include <level4.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "kernel32.lib")
#pragma	comment(lib, "user32.lib")
#pragma	comment(lib, "gdi32.lib")
#pragma	comment(lib, "ole32.lib")
#pragma	comment(lib, "oleaut32.lib")
#pragma	comment(lib, "uuid.lib")
#pragma	comment(lib, "advapi32.lib")
#pragma comment(lib, "version.lib")

// End of File

#endif
