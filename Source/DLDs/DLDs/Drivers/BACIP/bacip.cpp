
#include "intern.hpp"

#include "bacip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CBACNetIP);

// Constructor

CBACNetIP::CBACNetIP(void)
{
	m_Ident  = DRIVER_ID;

	m_pSock  = NULL;

	m_pExtra = NULL;
}

// Destructor

CBACNetIP::~CBACNetIP(void)
{
}

// Configuration

void MCALL CBACNetIP::Load(LPCBYTE pData)
{
}

// Management

void MCALL CBACNetIP::Attach(IPortObject *pPort)
{
	m_pSock = CreateSocket(IP_UDP);

	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
}

void MCALL CBACNetIP::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
	}

	if( m_pExtra ) {

		m_pExtra->Release();

		m_pExtra = NULL;
	}
}

void MCALL CBACNetIP::Open(void)
{
	if( m_pSock ) {

		m_pSock->SetOption(OPT_RECV_QUEUE, 12);

		m_pSock->SetOption(OPT_ADDRESS, 1);

		m_pSock->Listen(0xBAC0);
	}
}

// Device

CCODE MCALL CBACNetIP::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx            = new CContext;

			m_pBase           = m_pCtx;

			m_pCtx->m_fBroke  = FALSE;

			m_pCtx->m_uTime1  = GetWord(pData);

			m_pCtx->m_fSubNet = TRUE;

			m_pCtx->m_uRoute  = NOTHING;

			LoadConfig(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
}

CCODE MCALL CBACNetIP::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete[] m_pBase->m_pObj;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CBACNetIP::Ping(void)
{
	if( m_pSock ) {

		if( m_pCtx->m_fFound ) {

			return CCODE_SUCCESS;
		}

		if( CBACNetBase::Ping() == CCODE_SUCCESS ) {

			m_pCtx->m_Mac    = m_RxMac;

			m_pCtx->m_fFound = TRUE;

			return CCODE_SUCCESS;
		}
	}

	m_pCtx->m_fSubNet = !m_pCtx->m_fSubNet;

	return CCODE_ERROR;
}

// Implemetation

BOOL CBACNetIP::GetBroadcast(DWORD &Broadcast)
{
	if( m_pCtx->m_fSubNet ) {

		if( !m_pCtx->m_fFound ) {

			return m_pExtra->EnumBroadcast(m_pCtx->m_uRoute, Broadcast);
		}

		IPADDR Addr = m_pCtx->m_Mac.m_IP;

		DWORD IP    = MAKELONG(MAKEWORD(Addr.m_b4, Addr.m_b3), MAKEWORD(Addr.m_b2, Addr.m_b1));

		return m_pExtra->GetBroadcast(IP, Broadcast);
	}

	return FALSE;
}

// Transport Hooks

BOOL CBACNetIP::SendFrame(BOOL fThis, BOOL fReply)
{
	if( !fThis || m_pCtx->m_fFound ) {

		AddNetworkHeader(fThis, fReply);

		AddTransportHeader(fThis);

		Dump("tx", m_pTxBuff);

		for( UINT n = 0; n < 5; n++ ) {

			if( m_pSock->Send(m_pTxBuff) == S_OK ) {

				m_pTxBuff = NULL;

				return TRUE;
			}

			Sleep(5);
		}
	}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
}

BOOL CBACNetIP::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime1);

	while( GetTimer() ) {

		m_pSock->Recv(m_pRxBuff);

		if( m_pRxBuff ) {

			if( StripTransportHeader() ) {

				if( StripNetworkHeader() ) {

					Dump("rx", m_pRxBuff);

					return TRUE;
				}
			}

			RecvDone();
		}

		Sleep(5);
	}

	return FALSE;
}

// Transport Header

void CBACNetIP::AddTransportHeader(BOOL fThis)
{
	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 10;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	if( fThis ) {

		pData[0] = PBYTE(&m_pCtx->m_Mac.m_IP)[0];
		pData[1] = PBYTE(&m_pCtx->m_Mac.m_IP)[1];
		pData[2] = PBYTE(&m_pCtx->m_Mac.m_IP)[2];
		pData[3] = PBYTE(&m_pCtx->m_Mac.m_IP)[3];

		pData[4] = HIBYTE(m_pCtx->m_Mac.m_Port);
		pData[5] = LOBYTE(m_pCtx->m_Mac.m_Port);
	}
	else {
		DWORD Broadcast;

		if( GetBroadcast(Broadcast) ) {

			pData[0] = PBYTE(&Broadcast)[0];
			pData[1] = PBYTE(&Broadcast)[1];
			pData[2] = PBYTE(&Broadcast)[2];
			pData[3] = PBYTE(&Broadcast)[3];
		}
		else {
			pData[0] = 0xFF;
			pData[1] = 0xFF;
			pData[2] = 0xFF;
			pData[3] = 0xFF;
		}

		pData[4] = 0xBA;
		pData[5] = 0xC0;
	}

	pData[6] = 0x81;

	pData[7] = fThis ? 0x0A : 0x0B;

	pData[8] = HIBYTE(uData + 4);

	pData[9] = LOBYTE(uData + 4);
}

BOOL CBACNetIP::StripTransportHeader(void)
{
	PBYTE pData = m_pRxBuff->GetData();

	if( pData[6] == 0x81 ) {

		if( pData[7] == 0x0A || pData[7] == 0x0B ) {

			((PBYTE) &m_RxMac.m_IP)[0] = pData[0];
			((PBYTE) &m_RxMac.m_IP)[1] = pData[1];
			((PBYTE) &m_RxMac.m_IP)[2] = pData[2];
			((PBYTE) &m_RxMac.m_IP)[3] = pData[3];

			m_RxMac.m_Port = MAKEWORD(pData[5], pData[4]);

			m_pRxBuff->StripHead(10);

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
