
#include "Intern.hpp"

#include "UITextCommsPort.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "CommsManager.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Communications Port
//

// Dynamic Class

AfxImplementDynamicClass(CUITextCommsPort, CUITextEnum);

// Constructor

CUITextCommsPort::CUITextCommsPort(void)
{
	}

// Overridables

void CUITextCommsPort::OnBind(void)
{
	CUITextEnum::OnBind();

	AddData();
	}

// Implementation

void CUITextCommsPort::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextCommsPort::AddData(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CCommsManager *pComms = pSystem->m_pComms;

	CCommsPortList *pPorts;

	for( UINT s = 0; pComms->GetPortList(pPorts, s); s ++ ) {

		for( INDEX i = pPorts->GetHead(); !pPorts->Failed(i); pPorts->GetNext(i) ) {

			CCommsPort *pPort = pPorts->GetItem(i);

			BOOL fOkay = pPort->m_Binding == bindStdSerial ||
				     pPort->m_Binding == bindRawSerial ||
				     pPort->m_Binding == bindEthernet  ;;

			if( fOkay ) {

				CString Name = pPort->GetLabel();

				UINT   uCode = pPort->m_Number;

				AddData(uCode, Name);
				}
			}
		}
	}

// End of File
