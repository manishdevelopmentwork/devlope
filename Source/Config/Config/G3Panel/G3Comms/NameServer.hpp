
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NameServer_HPP

#define INCLUDE_NameServer_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsManager;
class CCommsSystem;
class CDataLogList;
class CProgramList;
class CTagList;

//////////////////////////////////////////////////////////////////////////
//
// Name Server
//

class DLLAPI CNameServer : public CObject, public INameServer
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CNameServer(CCommsSystem *pSystem);

		// Destructor
		~CNameServer(void);

		// Initialization
		virtual void LoadLibrary(void);

		// Operations
		void ResetServer(void);
		void RebuildDirect(BOOL fBuild);
		void SetFunc (IFuncServer *pFunc);
		void SetIdent(IIdentServer *pIdent);
		void SetRoot(CString Root);

		// IBase Methods
		UINT Release(void);

		// INameServer Methods
		BOOL FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NameIdent(WORD ID, CString &Name);
		BOOL FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type);
		BOOL NameProp(WORD ID, WORD PropID, CString &Name);
		BOOL FindDirect(CError *pError, CString Name, DWORD &ID, CTypeDef &Type);
		BOOL NameDirect(DWORD ID, CString &Name);
		BOOL FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount);
		BOOL NameFunction(WORD ID, CString &Name);
		BOOL EditFunction(WORD &ID, CTypeDef &Type);
		BOOL GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type);
		BOOL FindClass(CError *pError, CString Name, UINT &Type);
		BOOL NameClass(UINT Type, CString &Name);
		BOOL SaveClass(UINT Type);

		// Standard Tag Properties
		static BOOL FindTagProp(CString Name, UINT Data, WORD &PropID, CTypeDef &Type);
		static BOOL NameTagProp(WORD PropID, CString &Name);

		// Function Category
		BOOL FindCategory(WORD ID, CString &Cat);

	protected:
		// Standard Tag Properties
		static PCTXT m_pTagProps[];

		// Type Definitions
		typedef CMap <WORD, CString> CCatMap;

		// Data Members
		CCommsSystem  * m_pSystem;
		CCommsManager * m_pComms;
		CTagList      * m_pTags;
		CProgramList  * m_pProgs;
		CDataLogList  * m_pLogs;
		IFuncLibrary  * m_pFuncLib;
		IIdentLibrary * m_pIdentLib;
		IFuncServer   * m_pFunc;
		IIdentServer  * m_pIdent;
		CString         m_Root;
		UINT            m_uLen;
		BOOL	        m_fBuild;
		CCatMap         m_CatMap;

		// Initialization
		void AddFuncsCamera(CError &Error);
		void AddFuncsCommsData(CError &Error);
		void AddFuncsCommsDevice(CError &Error);
		void AddFuncsCommsStatus(CError &Error);
		void AddFuncsCommsPort(CError &Error);
		void AddFuncsCAN(CError &Error);
		void AddFuncsDrive(CError &Error);
		void AddFuncsEvents(CError &Error);
		void AddFuncsFile(CError &Error);
		void AddFuncsFTP(CError &Error);
		void AddFuncsLoggging(CError &Error);
		void AddFuncsMail(CError &Error);
		void AddFuncsMath(CError &Error);
		void AddFuncsNetworkPort(CError &Error);
		void AddFuncsRemote(CError &Error);
		void AddFuncsStrings(CError &Error);
		void AddFuncsSystem(CError &Error);
		void AddFuncsTagData(CError &Error);
		void AddFuncsTime(CError &Error);
		void AddFuncsUI(CError &Error);
		void AddFuncsWeb(CError &Error);
		void AddFuncsSQL(CError &Error);
		void AddFuncsHoneywell(CError &Error);
		void AddFuncsControl(CError &Error);
		void AddVarsAlarms(CError &Error);
		void AddVarsSystem(CError &Error);
		void AddVarsTime(CError &Error);
		void AddFunc(CError &Error, CString Cat, WORD ID, CString Func);

		// Implementation
		BOOL FindStd(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL FindTag(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL FindOne(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL FindPort(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL FindDevice(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL FindLog(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NameStd(WORD ID, CString &Name);
		BOOL NameTag(WORD ID, CString &Name);
		BOOL NamePort(WORD ID, CString &Name);
		BOOL NameDevice(WORD ID, CString &Name);
		BOOL NameLog(WORD ID, CString &Name);
		BOOL UnsafeCommitsAllowed(void);
	};

// End of File

#endif
