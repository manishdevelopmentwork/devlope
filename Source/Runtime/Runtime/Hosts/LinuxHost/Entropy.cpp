
#include "Intern.hpp"

#include "Entropy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

// Instantiator

IDevice * Create_Entropy(void)
{
	return New CEntropy;
}

// Constructor

CEntropy::CEntropy(void)
{
	StdSetRef();

	m_fd1   = 0;

	m_fd2   = 0;

	m_nb    = 0;

	m_out   = false;

	m_pLock = NULL;
}

// Destructor

CEntropy::~CEntropy(void)
{
	if( m_fd1 > 0 ) {

		_close(m_fd1);

		_close(m_fd2);
	}

	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CEntropy::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(IEntropy);

	return E_NOINTERFACE;
}

ULONG CEntropy::AddRef(void)
{
	StdAddRef();
}

ULONG CEntropy::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CEntropy::Open(void)
{
	if( (m_fd1 = _open("/dev/random", O_RDONLY, 0)) > 0 ) {

		if( (m_fd2 = _open("/dev/urandom", O_RDONLY, 0)) > 0 ) {

			m_pLock = Create_Qutex();

			return TRUE;
		}

		_close(m_fd1);

		m_fd1 = 0;
	}

	return FALSE;
}

// IEntropy

BOOL CEntropy::GetEntropy(PBYTE pData, UINT uData)
{
	m_pLock->Wait(FOREVER);

	while( uData ) {

		int  fd = m_out ? m_fd2 : m_fd1;

		UINT nr = _read_wait(fd, pData, uData, 0);

		if( !m_out && nr < uData ) {

			AfxTrace("entropy: used %u bytes and switching to urandom\n", m_nb + nr);

			m_out = true;
		}

		pData += nr;

		uData -= nr;

		m_nb  += nr;
	}

	m_pLock->Free();

	return TRUE;
}

// End of File
