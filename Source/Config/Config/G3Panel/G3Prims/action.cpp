
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action Page
//

class CPrimActionPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimActionPage(CPrimAction *pAction, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimAction * m_pAction;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimActionPage, CUIStdPage);

// Constructor

CPrimActionPage::CPrimActionPage(CPrimAction *pAction, CString Title, UINT uPage)
{
	m_pAction = pAction;

	m_Title   = Title;

	m_Class   = AfxRuntimeClass(CPrimAction);

	m_uPage   = uPage;
	}

// Operations

BOOL CPrimActionPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CItem *pPrim = m_pAction->GetParent();

	if( pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

		CPrimData *pData = ((CPrimWithText *) pPrim)->m_pDataItem;

		if( pData && pData->m_Entry ) {

			pView->StartGroup(CString(IDS_ACTION_MODE), 1);

			pView->AddNarrative(CString(IDS_ACTIONS_CANNOT_BE));

			pView->EndGroup(TRUE);

			return TRUE;
			}
		}

	CUIStdPage::LoadIntoView(pView, m_pAction);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action
//

// Dynamic Class

AfxImplementDynamicClass(CPrimAction, CCodedHost);

// Constructor

CPrimAction::CPrimAction(void)
{
	m_Mode	    = 0;
	m_Protect   = 0;
	m_Local     = FALSE;
	m_pEnable   = NULL;
	m_pPage	    = NULL;
	m_Popup     = FALSE;
	m_Button    = 0;
	m_Delay     = 0;
	m_Limit     = 0;
	m_pBitData  = NULL;
	m_pNumData  = NULL;
	m_pNumValue = NULL;
	m_pNumLimit = NULL;
	m_pPress    = NULL;
	m_pRepeat   = NULL;
	m_pRelease  = NULL;

	m_fTouch    = TRUE;
	}

// Destructor

CPrimAction::~CPrimAction(void)
{
	}

// UI Creation

BOOL CPrimAction::OnLoadPages(CUIPageList *pList)
{	
	return LoadPage(pList, CString(IDS_ACTION));
	}

// UI Update

void CPrimAction::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			if( m_fTouch ) {

				LimitEnum(pHost, L"Mode", m_Mode, UINT(~(1<<7)));
				}

			DoEnables(pHost);
			}
		
		if( Tag == "Mode" || Tag == "Button" ) {

			DoEnables(pHost);
			}

		if( Tag == "NumData" ) {
	
			UpdateNumTypes(pHost);

			DoEnables(pHost);
			}

		if( Tag == "Page" ) {

			if( !m_pPage || !m_pPage->GetObjectRef() ) {

				if( m_Popup ) {

					m_Popup = 0;

					pHost->UpdateUI(this, "Popup");
					}
				}

			DoEnables(pHost);
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimAction::IsNull(void) const
{
	switch( m_Mode ) {

		case 0:
			return TRUE;

		case 1:
			return !m_pPress && !m_pRepeat && !m_pRelease;

		case 2:
			return !m_pPage;

		case 3:
			return !m_pBitData;

		case 4:
			return !m_pNumData;

		case 5:
			return !m_pNumData;

		case 6:
			return m_Tune.IsEmpty();
		}

	return FALSE;
	}

BOOL CPrimAction::IsBroken(void) const
{
	switch( m_Mode ) {

		case 1:
			if( m_pPress && m_pPress->IsBroken() ) {

				return TRUE;
				}

			if( m_pRepeat && m_pRepeat->IsBroken() ) {

				return TRUE;
				}
			
			if( m_pRelease && m_pRelease->IsBroken() ) {

				return TRUE;
				}
			
			return FALSE;

		case 2:
			if( m_pPage && m_pPage->IsBroken() ) {

				return TRUE;
				}

			return FALSE;

		case 3:
			if( m_pBitData && m_pBitData->IsBroken() ) {

				return TRUE;
				}

			return FALSE;

		case 4:
			if( m_pNumData && m_pNumData->IsBroken() ) {

				return TRUE;
				}

			if( m_pNumValue && m_pNumValue->IsBroken() ) {

				return TRUE;
				}

			return FALSE;

		case 5:
			if( m_pNumData && m_pNumData->IsBroken() ) {

				return TRUE;
				}

			if( m_pNumValue && m_pNumValue->IsBroken() ) {

				return TRUE;
				}

			if( m_pNumLimit && m_pNumLimit->IsBroken() ) {

				return TRUE;
				}

			return FALSE;
		}

	return FALSE;
	}

CString CPrimAction::Describe(void) const
{
	return L"";
	}

// Operations

void CPrimAction::Validate(BOOL fExpand)
{
	if( m_pNumData ) {

		m_pNumData->Recompile(fExpand);

		UpdateNumTypes(NULL);
		}
	}

void CPrimAction::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	if( m_pNumData ) {

		if( m_pNumData->IsTagRef() ) {

			UINT uTag = m_pNumData->GetTagIndex();

			if( !Tags.Failed(Tags.Find(uTag)) ) {

				Validate(TRUE);

				Done.Insert(m_pNumData);
				}
			}
		}
	}

BOOL CPrimAction::LoadPage(CUIPageList *pList, CString Title)
{
	CPrimActionPage *pPage1 = New CPrimActionPage(this, Title,  1);

	pList->Append(pPage1);
	
	return TRUE;
	}

BOOL CPrimAction::StepAddress(void)
{
	BOOL f1 = FALSE;

	BOOL f2 = FALSE;

	BOOL f3 = FALSE;

	switch( m_Mode ) {

		case 3:
			f1 = StepCoded(m_pBitData);

			break;

		case 4:
			f1 = StepCoded(m_pNumData);

			f2 = StepCoded(m_pNumValue);

			f3 = StepCoded(m_pNumLimit);

			break;

		case 5:
			f1 = StepCoded(m_pNumData);

			f2 = StepCoded(m_pNumValue);

			f3 = StepCoded(m_pNumLimit);

			break;
		}

	return f1 || f2 || f3;
	}

// Type Access

BOOL CPrimAction::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Press" || Tag == "Repeat" || Tag == "Release" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive | flagDispatch;

		return TRUE;
		}

	if( Tag == "SubPos" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Enable" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Page" ) {

		Type.m_Type  = typePage;

		Type.m_Flags = flagConstant;

		return TRUE;
		}

	if( Tag == "BitValue" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	if( Tag == "NumData" ) {

		Type.m_Type  = typeNumeric;

		Type.m_Flags = flagInherent | flagWritable;

		return TRUE;
		}

	if( Tag == "NumValue" || Tag == "NumLimit" ) {

		if( m_pNumData ) {

			Type.m_Type  = m_pNumData->GetType();

			Type.m_Flags = 0;

			return TRUE;
			}

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Persistance

void CPrimAction::PostLoad(void)
{
	CCodedHost::PostLoad();

	if( m_Mode == 0 ) {

		if( m_pPress || m_pRepeat || m_pRelease ) {

			m_Mode = 1;
			}
		}

	if( m_Mode == 6 ) {
		
		if( m_Tune.IsEmpty() ) {

			m_Tune = "Dixie:d=16,o=5,b=125:g,e,c,p,c,p,c,d,e,f,g,p,g,p,8g,8e,"
				 "a,p,a,p,8a,p,g,8a,p,g,a,b,c6,d6,4e6,8p,c6,g,4c6,8p,g,e,"
				 "4g,8p,d,e,4c";
			}
		}
	}

// Download Support

BOOL CPrimAction::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddWord(WORD(IsMomentary() ? m_Delay : 0));

	Init.AddByte(BYTE(m_Protect));

	Init.AddByte(BYTE(m_Local));

	Init.AddItem(itemVirtual, m_pEnable);

	if( m_Mode == 1 ) {

		Init.AddItem(itemVirtual, m_pPress);
		Init.AddItem(itemVirtual, m_pRepeat);
		Init.AddItem(itemVirtual, m_pRelease);

		return TRUE;
		}

	if( m_Mode == 2 ) {

		if( m_pPage ) {

			UINT hItem = m_pPage->GetObjectRef();

			if( hItem ) {

				CDispPage *pPage = (CDispPage *) GetDatabase()->GetHandleItem(hItem);

				if( pPage && pPage->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

					if( m_fTouch ) {

						Init.AddByte(0);
						Init.AddByte(0);
						}

					CString Func;

					switch( m_Popup ) {

						case 1:
							Func = "ShowPopup";
							break;

						case 2:
							Func = "ShowNested";
							break;

						case 3:
							Func = "ShowMenu";
							break;

						default:
							Func = "GotoPage";
							break;
						}

					BuildCode(Init, Func + L"(" + pPage->m_Name + L")");

					if( !m_fTouch ) {

						Init.AddByte(0);
						Init.AddByte(0);
						}

					return TRUE;
					}
				}
			else {
				CString Text = m_pPage->GetSource(FALSE);

				if( m_fTouch ) {

					Init.AddByte(0);
					Init.AddByte(0);
					}

				if( Text == L"//n" ) {

					BuildCode(Init, L"GotoNext()");
					}
				else
					BuildCode(Init, L"GotoPrevious()");

				if( !m_fTouch ) {

					Init.AddByte(0);
					Init.AddByte(0);
					}

				return TRUE;
				}
			}
		}

	if( m_Mode == 3 ) {

		if( m_pBitData ) {

			CString Data = L"(" + m_pBitData->GetSource(TRUE) + L")";

			if( m_Button == 0 ) {

				BuildCode (Init, Data + L":=!" + Data);

				Init.AddByte(0);
				Init.AddByte(0);
				}

			if( m_Button == 1 ) {

				// NOTE -- Not support right now, as I am not sure
				// where to cache the old states given that we could
				// be a keyboard action...

				BuildCode(Init, L"_LatchValue_(" + Data + L", 1)");

				Init.AddByte(0);

				BuildCode(Init, L"_LatchValue_(" + Data + L", 0)");
				}

			if( m_Button == 2 ) {

				BuildCode(Init, Data + L":=1");

				Init.AddByte(0);

				BuildCode(Init, Data + L":=0");
				}

			if( m_Button == 3 ) {

				BuildCode(Init, Data + L":=0");

				Init.AddByte(0);

				BuildCode(Init, Data + L":=1");
				}

			if( m_Button == 4 ) {

				BuildCode(Init, Data + L":=1");

				Init.AddByte(0);
				Init.AddByte(0);
				}

			if( m_Button == 5 ) {

				BuildCode(Init, Data + L":=0");

				Init.AddByte(0);
				Init.AddByte(0);
				}

			return TRUE;
			}
		}

	if( m_Mode == 4 ) {

		if( m_pNumData ) {

			CString Data  = L"(" + m_pNumData ->GetSource(TRUE) + L")";

			CString Value = L"1";
			
			if( m_pNumValue ) {

				Value = L"(" + m_pNumValue->GetSource(TRUE) + L")";
				}

			BuildCode(Init, Data + L":=" + Value);

			Init.AddByte(0);

			Init.AddByte(0);

			return TRUE;
			}
		}

	if( m_Mode == 5 ) {

		if( m_pNumData ) {

			CString Data  = L"(" + m_pNumData ->GetSource(TRUE) + L")";

			CString Value = L"1";
			
			if( m_pNumValue ) {
				
				Value = L"(" + m_pNumValue->GetSource(TRUE) + L")";
				}

			Init.AddByte(0);

			if( m_pNumLimit ) {

				CString Limit = L"(" + m_pNumLimit->GetSource(TRUE) + L")";

				CString Func  = m_Limit ? L"Max" : L"Min";

				CString Op    = m_Limit ? L"-"   : L"+";

				BuildCode( Init, CPrintf( L"Set(%s,%s(%s,%s%s%s))",
							  Data,
							  Func,
							  Limit,
							  Data,
							  Op,
							  Value));
				}
			else {
				if( m_Limit ) {

					BuildCode( Init, CPrintf( L"Set(%s, %s-%s)",
								  Data,
								  Data,
								  Value
								  ));
					}
				else {
					BuildCode( Init, CPrintf( L"Set(%s, %s+%s)",
								  Data,
								  Data,
								  Value
								  ));
					}
				}

			Init.AddByte(0);

			return TRUE;
			}
		}

	if( m_Mode == 6 ) {

		BuildCode(Init, L"PlayRTTTL(\"" + m_Tune + L"\")");

		Init.AddByte(0);
		Init.AddByte(0);

		return TRUE;
		}

	if( m_Mode == 7 ) {

		BuildCode(Init, L"Nop()");

		Init.AddByte(0);
		Init.AddByte(0);

		return TRUE;
		}

        if( m_Mode == 8 ) {

		Init.AddByte(0);
		Init.AddByte(0);

		BuildCode(Init, L"UserLogOn()");

		return TRUE;
		}

        if( m_Mode == 9 ) {

		Init.AddByte(0);
		Init.AddByte(0);

		BuildCode(Init, L"UserLogOff()");

		return TRUE;
		}

        if( m_Mode == 10 ) {

		Init.AddByte(0);
		Init.AddByte(0);

		BuildCode(Init, L"HidePopup()");

		return TRUE;
		}

        if( m_Mode == 11 ) {

		Init.AddByte(0);
		Init.AddByte(0);

		BuildCode(Init, L"HideAllPopups()");

		return TRUE;
		}

        if( m_Mode == 12 ) {

		BuildCode(Init, L"StopRTTTL()");

		Init.AddByte(0);
		Init.AddByte(0);

		return TRUE;
		}

	Init.AddByte(0);
	Init.AddByte(0);
	Init.AddByte(0);

	return TRUE;
	}

// Property Save Filter

BOOL CPrimAction::SaveProp(CString const &Tag) const
{
	if( Tag == "Press" || Tag == "Repeat" || Tag == "Release" ) {

		return m_Mode == 1;
		}

	if( Tag == "Page" || Tag == "Popup" ) {

		return m_Mode == 2;
		}

	if( Tag == "Button" || Tag == "BitData" || Tag == "Delay" ) {

		return m_Mode == 3;
		}

	if( Tag == "NumData" || Tag == "NumValue" ) {

		return m_Mode == 4 || m_Mode == 5;
		}

	if( Tag == "NumLimit" || Tag == "Limit" ) {

		return m_Mode == 5;
		}

	if( Tag == "Tune" ) {

		return m_Mode == 6;
		}

	return CMetaItem::SaveProp(Tag);
	}

// Meta Data Creation

void CPrimAction::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Mode);
	Meta_AddVirtual(Page);
	Meta_AddInteger(Popup);
	Meta_AddInteger(Button);
	Meta_AddInteger(Delay);
	Meta_AddInteger(Limit);
	Meta_AddVirtual(BitData);
	Meta_AddVirtual(NumData);
	Meta_AddVirtual(NumValue);
	Meta_AddVirtual(NumLimit);
	Meta_AddVirtual(Press);
	Meta_AddVirtual(Repeat);
	Meta_AddVirtual(Release);
	Meta_AddVirtual(Enable);
	Meta_AddInteger(Protect);
	Meta_AddInteger(Local);
	Meta_AddString (Tune);

	Meta_SetName((IDS_ACTION));
	}

// Type Updates

void CPrimAction::UpdateNumTypes(IUIHost *pHost)
{
	UpdateType(pHost, L"NumValue", m_pNumValue, TRUE);

	UpdateType(pHost, L"NumLimit", m_pNumLimit, TRUE);
	}

// Implementation

BOOL CPrimAction::IsMomentary(void)
{
	return m_Button == 2 || m_Button == 3;
	}

void CPrimAction::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Protect",  m_Mode >= 1);
	
	pHost->EnableUI(this, "Enable",   m_Mode >= 1);
	
	pHost->EnableUI(this, "Local",    m_Mode >= 1);

	pHost->EnableUI(this, "Popup",    m_Mode == 2 && m_pPage && m_pPage->GetObjectRef());

	pHost->EnableUI(this, "Delay",    IsMomentary());

	pHost->ShowUI  (this, "Press",    m_Mode == 1);
	
	pHost->ShowUI  (this, "Repeat",   m_Mode == 1);
	
	pHost->ShowUI  (this, "Release",  m_Mode == 1);

	pHost->ShowUI  (this, "Page",     m_Mode == 2);
	
	pHost->ShowUI  (this, "Popup",    m_Mode == 2);

	pHost->ShowUI  (this, "Button",   m_Mode == 3);

	pHost->ShowUI  (this, "Delay",    m_Mode == 3);
	
	pHost->ShowUI  (this, "BitData",  m_Mode == 3);

	pHost->ShowUI  (this, "NumData",  m_Mode == 4 || m_Mode == 5);
	
	pHost->ShowUI  (this, "NumValue", m_Mode == 4 || m_Mode == 5);

	pHost->ShowUI  (this, "NumLimit", m_Mode == 5);
	
	pHost->ShowUI  (this, "Limit",    m_Mode == 5);

	pHost->ShowUI  (this, "Tune",     m_Mode == 6);
	}

void CPrimAction::BuildCode(CInitData &Init, PCTXT pCode)
{
	// LATER -- It would be better to cache the code
	// rather that rebuilding it on every download.

	CCodedItem *pItem = New CCodedItem;

	pItem->SetParent(this);

	pItem->Init();

	CTypeDef Type;

	Type.m_Type  = typeVoid;

	Type.m_Flags = flagActive;

	pItem->SetReqType(Type);

	if( pItem->Compile(pCode) ) {

		Init.AddItem(itemVirtual, pItem);
		}
	else
		Init.AddByte(0);

	pItem->Kill();

	delete pItem;
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Tune List
//

class CUITextTune : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextTune(void);

	protected:
		// Typedefs
		typedef CMap <CString, UINT> CStringDict;

		// Data Members
		CStringArray m_Tunes;
		CStringDict  m_Dict;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnEnumValues(CStringArray &List);

		// Implementation
		void LoadTunes(void);
		void LoadDixie(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// UI Element -- Tune List
//

// Dynamic Class

AfxImplementDynamicClass(CUITextTune, CUITextElement);

// Constructor

CUITextTune::CUITextTune(void)
{
	m_uFlags = textEnum;

	LoadTunes();
	}

// Overridables

void CUITextTune::OnBind(void)
{
	CUITextElement::OnBind();

	m_uWidth = 20;

	m_uLimit = 200;
	}

CString CUITextTune::OnGetAsText(void)
{
	CString Tune = m_pData->ReadString(m_pItem);

	if( Tune.IsEmpty() ) {

		return IDS_PUI_NONE;
		}

	return Tune.Left(Tune.Find(L':'));
	}

UINT CUITextTune::OnSetAsText(CError &Error, CString Text)
{
	CString Tune;

	INDEX   nPos = m_Dict.FindName(Text);

	if( !m_Dict.Failed(nPos) ) {

		Tune = m_Tunes[m_Dict.GetData(nPos)];
		}

	if( m_pData->ReadString(m_pItem) == Tune ) {

		return saveSame;
		}

	m_pData->WriteString(m_pItem, Tune);

	return saveChange;
	}

BOOL CUITextTune::OnEnumValues(CStringArray &List)
{
	List.Append(CString(IDS_PUI_NONE));

	for( INDEX n = m_Dict.GetHead(); !m_Dict.Failed(n); m_Dict.GetNext(n) ) {

		List.Append(m_Dict.GetName(n));
		}

	return TRUE;
	}

// Implementation

void CUITextTune::LoadTunes(void)
{
	CFilename Path  = afxModule->GetFilename().WithName(L"tunes.txt");

	FILE    * pFile = _wfopen(Path, L"rb");

	if( pFile ) {

		UINT uSize = 8192;

		PSTR pData = new char [ uSize ];

		while( !feof(pFile) ) {

			pData[0] = 0;

			if( fgets(pData, uSize, pFile) && pData[0] ) {

				CString Tune(pData);

				Tune.TrimBoth();

				UINT    uPos = m_Tunes.Append(Tune);

				m_Dict.Insert(Tune.Left(Tune.Find(L':')), uPos);

				continue;
				}
			}

		delete [] pData;

		fclose(pFile);
		}

	LoadDixie();
	}

void CUITextTune::LoadDixie(void)
{
	CString Tune = L"Dixie:d=16,o=5,b=125:g,e,c,p,c,p,c,d,e,f,g,p,g,p,8g,8e,"
		       L"a,p,a,p,8a,p,g,8a,p,g,a,b,c6,d6,4e6,8p,c6,g,4c6,8p,g,e,"
		       L"4g,8p,d,e,4c";

	UINT    uPos = m_Tunes.Append(Tune);

	m_Dict.Insert(Tune.Left(Tune.Find(L':')), uPos);
	}

// End of File
