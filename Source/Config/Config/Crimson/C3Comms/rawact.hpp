
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RAWTCPA_HPP
	
#define	INCLUDE_RAWTCPA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Driver Options
//

class CRawTCPActiveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRawTCPActiveDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		CCodedItem *m_pService;
		UINT	    m_IP;
		UINT	    m_Port;
		UINT	    m_Timeout;
		UINT	    m_Keep;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Driver
//

class CRawTCPActiveDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CRawTCPActiveDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
