
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DiagOutput_HPP
	
#define	INCLUDE_DiagOutput_HPP

//////////////////////////////////////////////////////////////////////////
//
// Aeon Compatability
//

#if !defined(AEON_ENVIRONMENT) && !defined(METHOD)

#define METHOD 

#define StdSetRef(x)

typedef unsigned long ULONG;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics Output
//

class CDiagOutput : public IDiagOutput
{
	public:
		// Constructor
		CDiagOutput(IDiagConsole *pConsole, PCTXT pName);

		// IUnknown
		#if defined(AEON_ENVIRONMENT)
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		#endif

		// IDiagOutput
		void METHOD Error(PCTXT pText, ...);
		void METHOD Print(PCTXT pText, ...);
		void METHOD VPrint(PCTXT pText, va_list pArgs);
		void METHOD Dump(PCVOID pData, UINT uCount);
		void METHOD Dump(PCVOID pData, UINT uCount, UINT uBase);
		void METHOD AddTable(UINT uCols);
		void METHOD EndTable(void);
		void METHOD SetColumn(UINT uCol, PCTXT pName, PCTXT pForm);
		void METHOD AddHead(void);
		void METHOD AddRule(char cData);
		void METHOD AddRow(void);
		void METHOD EndRow(void);
		void METHOD SetData(UINT uCol, ...);
		void METHOD AddPropList(void);
		void METHOD EndPropList(void);
		void METHOD AddProp(PCTXT pName, PCTXT pForm, ...);
		void METHOD Finished(void);

	protected:
		// States
		enum
		{
			stateNormal = 0,
			};

		// Table Column
		struct CCol
		{
			PTXT	m_pHead;
			PTXT	m_pForm;
			bool	m_fRight;
			UINT	m_uWide;
			};

		// Table Row
		struct CRow
		{
			UINT   m_uType;
			char   m_cRule;
			PTXT * m_pData;
			};

		// Data Members
		ULONG	       m_uRefs;
		IDiagConsole * m_pConsole;
		bool	       m_fPrint;
		bool	       m_fTable;
		PCTXT	       m_pName;
		UINT	       m_State;
		UINT	       m_uCols;
		UINT	       m_uRows;
		UINT	       m_uSize;
		CCol	     * m_pCol;
		CRow	     * m_pRow;

		// Implementation
		void Write(PCTXT pText);
		void Pad(PCTXT pText, UINT uWide, bool fRight);
		bool IsRight(PCTXT pForm);
		void CheckRows(void);
	};

// End of File

#endif
