
#include "intern.hpp"

#include "DAPID1Module.hpp"

#include "DAPID1MainWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

// Dynamic Class

AfxImplementDynamicClass(CDAPID1Module, CSLCModule);

// Constructor

CDAPID1Module::CDAPID1Module(void)
{
	m_FirmID = FIRM_DAPID1;

	m_Ident  = LOBYTE(ID_DAPID1_RA);

	m_Model  = "PID1";

	m_Power  = 33;

	m_Conv.Insert(L"Legacy", L"CSLCModule");

	m_Conv.Insert(CString(IDS_MODULE_LOOP), CString(IDS_MODULE_LOOP1));
	}

// UI Management

CViewWnd * CDAPID1Module::CreateMainView(void)
{
	return New CDAPID1MainWnd;
	}

// Download Support

void CDAPID1Module::MakeConfigData(CInitData &Init)
{
	Init.AddByte(m_FirmID);
	
	CSLCModule::MakeConfigData(Init);
	}

// Implementation

void CDAPID1Module::AddMetaData(void)
{
	Meta_AddInteger(Ident);

	CSLCModule::AddMetaData();
	}

// End of File
