
#include "Intern.hpp"

#include "G3TunnelService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Rack Tunnel Service
//

// Instantiator

global ILinkService * Create_TunnelService(ICrimsonPxe *pPxe)
{
	return New CG3TunnelService(pPxe);
}

// Constructor

CG3TunnelService::CG3TunnelService(ICrimsonPxe *pPxe)
{
	m_pPxe = pPxe;

	StdSetRef();
}

// Destructor

CG3TunnelService::~CG3TunnelService(void)
{
}

// IUnknown

HRESULT CG3TunnelService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3TunnelService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3TunnelService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3TunnelService::Timeout(void)
{
}

UINT CG3TunnelService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servTunnel ) {

		if( Req.GetOpcode() == tunnelTunnel ) {

			ICrimsonApp *pApp;

			if( m_pPxe->LockApp(pApp) ) {

				UINT   uPtr  = 0;

				BYTE   bDrop = Req.ReadByte(uPtr);

				Req.ReadByte(uPtr);

				PCBYTE pData = Req.ReadData(uPtr, 64);

				BYTE   bData[64];

				memcpy(bData, pData, 64);

				if( pApp->RackTunnel(bDrop, bData) ) {

					Rep.StartFrame(servTunnel, tunnelTunnel);

					Rep.AddByte(bDrop);

					Rep.AddByte(1);

					Rep.AddData(bData, 64);

					m_pPxe->FreeApp();

					return procOkay;
				}

				Rep.StartFrame(servTunnel, tunnelTunnel);

				Rep.AddByte(bDrop);

				Rep.AddByte(0);

				m_pPxe->FreeApp();

				return procOkay;
			}
		}
	}

	return procError;
}

void CG3TunnelService::EndLink(CG3LinkFrame &Req)
{
}

// End of File
