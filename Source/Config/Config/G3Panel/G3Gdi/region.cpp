
#include "intern.hpp"

#include "gdi2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GDI Version 2.0
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Region Implementation
//

class CStdRegion : public IRegion
{
	public:
		// Constructor
		CStdRegion(void);

		// Destructor
		virtual ~CStdRegion(void);

		// Management
		UINT Release(void);

		// Attributes
		BOOL IsEmpty(void);
		BOOL GetRect(R2 &Rect);

		// Operations
		void Clear     (void);
		void AddRect   (R2 const &Rect);
		void AddEllipse(R2 const &Rect);

		// Hit Testing
		BOOL HitTest(P2 const &Point);
		BOOL HitTest(R2 const &Rect);

		// Filling
		void Fill(IGDI *pGDI);

		// Diagnostics
		void Show(IGDI *pGDI);

	protected:
		// Data Members
		R2   m_Rect[16];
		UINT m_uCount;

		// Implementation
		UINT GetDistance(R2 const &R1, R2 const &R2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Region Implementation
//

// Instantiator

IRegion * Create_StdRegion(void)
{
	return new CStdRegion;
	}

// Constructor

CStdRegion::CStdRegion(void)
{
	Clear();
	}

// Destructor

CStdRegion::~CStdRegion(void)
{
	}

// Management

UINT CStdRegion::Release(void)
{
	delete this;

	return 0;
	}

// Attributes

BOOL CStdRegion::IsEmpty(void)
{
	return !m_uCount;
	}

BOOL CStdRegion::GetRect(R2 &Rect)
{
	SetRectEmpty(Rect);

	if( m_uCount ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			CombineRects(Rect, Rect, m_Rect[n]);
			}

		return !IsRectEmpty(Rect);
		}

	return FALSE;
	}

// Operations

void CStdRegion::Clear(void)
{
	m_uCount = 0;
	}

void CStdRegion::AddRect(R2 const &Rect)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( RectInRect(m_Rect[n], Rect) ) {

			return;
			}

		if( RectInRect(Rect, m_Rect[n]) ) {

			m_Rect[n++] = Rect;

			if( n < m_uCount ) {

				for( UINT s = n; s < m_uCount; s++ ) {

					if( RectInRect(Rect, m_Rect[s]) ) {

						continue;
						}

					if( n - s ) {

						m_Rect[n] = m_Rect[s];
						}

					n++;
					}

				m_uCount = n;
				}

			return;
			}
		}

	if( m_uCount == elements(m_Rect) ) {

		UINT m = 65535;

		UINT i = 0;

		for( UINT n = 0; n < m_uCount; n++ ) {

			UINT d = GetDistance(m_Rect[n], Rect);

			if( d < m ) {

				m = d;

				i = n;
				}
			}

		CombineRects(m_Rect[i], m_Rect[i], Rect);

		return;
		}

	m_Rect[m_uCount++] = Rect;
	}

void CStdRegion::AddEllipse(R2 const &Rect)
{
	AddRect(Rect);
	}

// Hit Testing

BOOL CStdRegion::HitTest(P2 const &Point)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( PtInRect(m_Rect[n], Point) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CStdRegion::HitTest(R2 const &Rect)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( RectsIntersect(m_Rect[n], Rect) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Filling

void CStdRegion::Fill(IGDI *pGDI)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		pGDI->FillRect( m_Rect[n].x1,
				m_Rect[n].y1,
				m_Rect[n].x2,
				m_Rect[n].y2
				);
		}
	}

// Diagnostics

void CStdRegion::Show(IGDI *pGDI)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		pGDI->DrawRect( m_Rect[n].x1-1,
				m_Rect[n].y1-1,
				m_Rect[n].x2+1,
				m_Rect[n].y2+1
				);
		}
	}

// Implementation

UINT CStdRegion::GetDistance(R2 const &R1, R2 const &R2)
{
	// LATER -- This function provides a score which
	// is used to decide which rectangle to combine
	// with the new rectangle when we have used all
	// the available slots. It seems to work, but I
	// am not sure it is the optimum solution.

	P2 c1, c2;

	c1.x = (R1.x1 + R1.x2) / 2;
	c1.y = (R1.y1 + R1.y2) / 2;

	c2.x = (R2.x1 + R2.x2) / 2;
	c2.y = (R2.y1 + R2.y2) / 2;

	int dx = (c1.x > c2.x) ? (c1.x - c2.x) : (c2.x - c1.x);
	int dy = (c1.y > c2.y) ? (c1.y - c2.y) : (c2.y - c1.y);

	int si = RectsIntersect(R1, R2) ? 2 : 1;

	return (dx + dy) / si;
	}

// End of File
