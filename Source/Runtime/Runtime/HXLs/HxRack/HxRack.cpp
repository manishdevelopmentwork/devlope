
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxRack(void)
{
	piob->RegisterSingleton("usb.rack", 0, Create_Expansion());
	}

void Revoke_HxRack(void)
{
	piob->RevokeSingleton("usb.rack", 0);
	}

// End of File
