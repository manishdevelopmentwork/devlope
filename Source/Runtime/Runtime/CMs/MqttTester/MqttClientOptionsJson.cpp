
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// JSON MQTT Client Options
//

// Constructor

CMqttClientOptionsJson::CMqttClientOptionsJson(void)
{
	m_Root = 0;

	m_Code = 0;
	}

// Initialization

void CMqttClientOptionsJson::Load(PCBYTE &pData)
{
	CMqttClientOptionsCrimson::Load(pData);

	m_Root = GetByte(pData);

	m_Code = GetByte(pData);
	}

// End of File
