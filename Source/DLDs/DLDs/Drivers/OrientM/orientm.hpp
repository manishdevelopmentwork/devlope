
//////////////////////////////////////////////////////////////////////////
//
// Oriental Motor Driver
//

#define	PBSZ	9	// Size of Transmit/Receive String Buffer

// Selection ID's
enum {
	C00D	=	0x00,
	C01R	=	0x01,
	C02R	=	0x02,
	C03R	=	0x03,
	C04O	=	0x04,
	C05O	=	0x05,
	C06O	=	0x06,
	C07O	=	0x07,
	C08H	=	0x08,
	C09C	=	0x09,
	C0AC	=	0x0A,
	C0BG	=	0x0B,
	C0CG	=	0x0C,
	C0DC	=	0x0D,
	C0EC	=	0x0E,
//	C0FS	=	0x0F,	// Table
//	C10P	=	0x10,	// Table
//	C11O	=	0x11,	// Table
	C12H	=	0x12,
	C13S	=	0x13,
	C14O	=	0x14,
	C15C	=	0x15,
	C16C	=	0x16,
	C17C	=	0x17,
	C18C	=	0x18,
	C19C	=	0x19,
	C1AL	=	0x1A,
	C1BH	=	0x1B,
	C1CS	=	0x1C,
	C1DS	=	0x1D,
	C1EA	=	0x1E,
	C1FC	=	0x1F,
	C20C	=	0x20,
	C21R	=	0x21,
	C22E	=	0x22,
	C23P	=	0x23,
	C24P	=	0x24,
	C25C	=	0x25,
	C26C	=	0x26,
	C27C	=	0x27,
	C28C	=	0x28,
	C29C	=	0x29,
	C2AC	=	0x2A,
	C2BC	=	0x2B,
	C2CC	=	0x2C,
	C2DC	=	0x2D,
	C2EC	=	0x2E,
	C2FC	=	0x2F,
	C30C	=	0x30,
	C31C	=	0x31,
	C32R	=	0x32,
	C33C	=	0x33,
	C34C	=	0x34,
	C35C	=	0x35,
	C36D	=	0x36,
	C37D	=	0x37,
	C38C	=	0x38,
	C39I	=	0x39,
	C3AC	=	0x3A,
	C3BC	=	0x3B,
	C3CS	=	0x3C,
//	C3DC	=	0x3D,	// Table
//	C3EA	=	0x3E,	// Table
//	C3FW	=	0x3F,	// Table
	C40O	=	0x40,
	C41S	=	0x41,
	C42C	=	0x42,
	C43M	=	0x43,
	C44M	=	0x44,
	C45C	=	0x45,
	C46C	=	0x46,
	C47C	=	0x47,
	C48B	=	0x48,
	C49B	=	0x49,
	C4AB	=	0x4A,
	C4BE	=	0x4B,
	C4CE	=	0x4C,
	C4DC	=	0x4D,
	C4EC	=	0x4E,
	C4FC	=	0x4F,
	C50C	=	0x50,
	C51C	=	0x51,
	C52C	=	0x52,
	C53C	=	0x53,
	C54C	=	0x54,
	C55C	=	0x55,
	C56C	=	0x56,
	C57C	=	0x57,
	C58C	=	0x58,
	C59C	=	0x59,
	C5AC	=	0x5A,
	C5BC	=	0x5B,
	C5CC	=	0x5C,
	C5DC	=	0x5D,
	C5EC	=	0x5E,
	C5FC	=	0x5F,
	C60C	=	0x60,
	C61C	=	0x61,
	C62C	=	0x62,
	C63C	=	0x63,
	C64C	=	0x64,
	C65C	=	0x65,
	C66C	=	0x66,
	C67C	=	0x67,
	C68C	=	0x68,
	C69C	=	0x69,
	C6AC	=	0x6A,
	C6BC	=	0x6B,
	C6CC	=	0x6C,
	C6DC	=	0x6D,
	C6EC	=	0x6E,
	C6FC	=	0x6F,
	C70C	=	0x70,
	C71C	=	0x71,
	C72C	=	0x72,
	C73C	=	0x73,
	C74C	=	0x74,
	C75C	=	0x75,
	C76C	=	0x76,
	C77C	=	0x77,
	C78C	=	0x78,
	C79C	=	0x79,
	C7AC	=	0x7A,
	C7BC	=	0x7B,
	C7CC	=	0x7C,
	C7DC	=	0x7D,
	C7EC	=	0x7E,
	C7FC	=	0x7F,
	C80S	=	0x80,
	C81C	=	0x81,
//	C82A	=	0x82,	// Table
//	C83D	=	0x83,	// Table
	C84C	=	0x84,
	C85C	=	0x85,
	C86C	=	0x86,
	C87C	=	0x87,
//	C88O	=	0x88,	// Table
	C89C	=	0x89,
	C8AC	=	0x8A,
	C8BC	=	0x8B,
	C8CC	=	0x8C,
	C8DA	=	0x8D,
	C8EC	=	0x8E,
	C8FC	=	0x8F,
	C90J	=	0x90,
	C91J	=	0x91,
	C92J	=	0x92,
	C93S	=	0x93,
	C94A	=	0x94,
	C95O	=	0x95,
	C96C	=	0x96,
	C97C	=	0x97,
	C98H	=	0x98,
	C99S	=	0x99,
	C9AI	=	0x9A,
	C9BM	=	0x9B,
	C9CD	=	0x9C,
	C9DC	=	0x9D,
	C9EC	=	0x9E,
	C9FC	=	0x9F,
//	CA0P	=	0xA0,	// Table
	CA1C	=	0xA1,
	CA2E	=	0xA2,
	CA3E	=	0xA3,
	CA4P	=	0xA4,
	CA5A	=	0xA5,
	CA6A	=	0xA6,
	CA7C	=	0xA7,
	CA8P	=	0xA8,
	CA9S	=	0xA9,
	CAAS	=	0xAA,
	CABT	=	0xAB,
	CACB	=	0xAC,
	CADC	=	0xAD,
	CAEC	=	0xAE,
	CAFC	=	0xAF,
	CB0C	=	0xB0,
	CB1O	=	0xB1,
	CB2O	=	0xB2,
	CB3C	=	0xB3,
	CB4C	=	0xB4,
	CB5C	=	0xB5,
	CB6C	=	0xB6,
	CB7S	=	0xB7,
	CB8P	=	0xB8,
	CB9N	=	0xB9,
	CBAH	=	0xBA,
	CBBS	=	0xBB,
	CBCS	=	0xBC,
	CBDS	=	0xBD,
	CBEC	=	0xCE,
	CBFC	=	0xBF,
	CC0C	=	0xC0,
	CC1C	=	0xC1,
	CC2C	=	0xC2,
	CC3C	=	0xC3,
	CC4C	=	0xC4,
	CC5C	=	0xC5,
	CC6C	=	0xC6,
	CC7C	=	0xC7,
	CC8C	=	0xC8,
	CC9C	=	0xC9,
	CCAD	=	0xCA,
	CCBD	=	0xCB,
	CCCC	=	0xCC,
	CCDC	=	0xCD,
	CCEC	=	0xCE,
	CCFC	=	0xCF,
	UADD	=	0xFD, // CUAD Opcode
	GENU	=	0xFE, // CGEN Opcode
	DOPC	=	0xFF, // DRCT Opcode
	};

// Table commands
#define	FIRSTTABLE	100
#define	FIRSTPARAM	101
#define	LASTTABLE	112

enum {
	DRCT	=	FIRSTTABLE,
	CGEN	=	101,
	C0FS	=	102,
	C10P	=	103,
	C11O	=	104,
	C3DC	=	105,
	C3EA	=	106,
	C3FW	=	107,
	C82A	=	108,
	C83D	=	109,
	C88O	=	110,
	CA0P	=	111,
	CUAD	=	LASTTABLE,
	};

// Frame definition
// FHEAD = Header
// 7 = 0
// 6 = 0 = Slave, 1 = Master
// 5 = 0 = Individual Send, 1 = Simultaneous Send
// 4-0   = Address 00 - 1F

// FACTION = Action Entry
// 7 = 0
// 6 = 0 = Write, 1 = Read
// 5 = 0 = No Action, 1 = STOP
// 4 = 0
// 3 = 0 = No Action, 1 = HOME
// 2 = 0 = Reverse - Decel Stop, 1 = Operation
// 1 = 0 = Forward - Decel Stop, 1 = Operation
// 0 = 0 = No Action, 1 = Start Operation

// FMODE = Mode/NACK
// 0-3F  = Specify Data Number, or 0
// FF    = NACK on receive

// FCMD  = Command Byte

// FDATALL = Data 1=Intel order
// Data 2
// Data 3
// FDATAHH = Data 4

// FCRC = CRC
// Exclusive Or of bits

// Frame Defines
#define	FHEAD	0
#define	FACTION	1
#define	FMODE	2
#define	FCMD	3
#define	FDATALL	4
#define	FDATAHH	7
#define	FCRC	8

// Header defines
#define	HMASTER	0x40
#define	HSIMUL	0x20
#define	HADDR	0x1F

// Direct Action defines
#define	AEREAD	0x40
#define	AESTOP	0x20
#define	AEHOME	0x08
#define	AEREVO	0x04
#define	AEFWDO	0x02
#define	AESTART	0x01

#define	DRCTGO	(AESTOP | AEHOME | AESTART)
#define	FWDREV	(AEREVO | AEFWDO)
#define DIRMASK	(DRCTGO | FWDREV)

#define	GBLADDR(x)	(x > 31)

#define	MAXDATANO	0x3F

class COrientalMotorDriver : public CMasterDriver
{
	public:
		// Constructor
		COrientalMotorDriver(void);

		// Destructor
		~COrientalMotorDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);
		
		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

		// User Function
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bActiveDrop;
			BYTE	m_bPingCount;
			DWORD	m_dDropInterval;
			DWORD	m_dDropStart;
			UINT	m_uActiveTO;
			};

		// Data Members
		CContext *m_pCtx;

		// Item Selected
		BYTE	m_bOp;

		// Others
		BOOL	m_fTable;
		BOOL	m_fGenNoPar;

		BYTE	m_bTx[PBSZ];
		BYTE	m_bRx[PBSZ];
		BYTE	m_bFwdRev;
		BYTE	m_bDNum;
		BYTE	m_bGenOpc;
		BYTE	m_bGenPar;

		UINT	m_uPtr;
		UINT	m_uWErrCt;
		UINT	m_uNoFrameSent;

		DWORD	m_t4000;
		DWORD	m_t1000;

		// Frame Building
		void	BuildFrame(BYTE bAction, UINT uOffset, DWORD dData);
		BYTE	SelectMode(UINT uOffset, DWORD dData);
		BYTE	SelectCommand(void);
		BOOL	IsDirect(void);
		BOOL	IsSelDataNumber(void);
		BOOL	IsReqDataNumber(void);
		BOOL	IsGenu(void);
		BOOL	IsUserAddr(void);
		BOOL	HasZeroData(void);
		void	AddByte(BYTE bData);
		void	EndFrame(void);

		// Transport Layer
		BOOL	Transact(void);
		BOOL	CheckReply(BOOL fIsWrite);
		
		// Read Handlers
		CCODE	SendRead(AREF Addr, PDWORD pData);
		CCODE	DoNoOp(void);

		// Write Handlers
		CCODE	SendWrite(AREF Addr, PDWORD pData);

		// Helpers
		void	SetSelection(AREF Addr);
		BYTE	GetOMOpcode(AREF Addr);
		BYTE	SetGeneric(AREF Addr);
		BOOL	NoReadTransmit(PDWORD pData);
		BOOL	IsWriteCommand(void);
		BOOL	NoWriteTransmit(DWORD dData);
		BOOL	IsReadOnly(void);
		void	PutNum(DWORD dValue);
		
		// Port Access
		void	Put(void);
		BOOL	GetReply(void);
		UINT	Get(UINT uTime);

	};

// End of File
