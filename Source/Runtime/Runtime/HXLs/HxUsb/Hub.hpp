
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHub_HPP

#define	INCLUDE_UsbHub_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Request Types
//

enum 
{
	reqClearTT		=   8,
	reqResetTT		=   9,
	reqGetTTState		=  10,
	reqStopTT		=  11,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Descriptor Types
//

enum
{
	descHub			=  41,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Features
//

enum
{
	selHubLocalPower	=   0,
	selHubOverCurrent	=   1,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Port Features
//

enum
{
	selPortConnection	=   0,
	selPortEnable		=   1,
	selPortSuspend		=   2,
	selPortOverCurrnt	=   3,
	selPortReset		=   4,
	selPortPower		=   8,
	selPortLowSpeed		=   9,
	selCPortConnection	=  16,
	selCPortEnable		=  17,
	selCPortSuspend		=  18,
	selCPortOverCurrnt	=  19,
	selCPortReset 		=  20,
	selPortTest		=  21,
	selPortIndicator	=  22,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Recipients
//

enum
{
	recHub			=   0,
	recPort			=   3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Status Bits
//

enum
{
	statPowerSource		= Bit( 0),
	statOverCurrent		= Bit( 1),
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Port Status Bits
//

enum
{
	portConnect		= Bit( 0),
	portEnable		= Bit( 1),
	portSuspend		= Bit( 2),
	portOverCurrent		= Bit( 3),
	portReset		= Bit( 4),
	portPower		= Bit( 8),
	portLowSpeed		= Bit( 9),
	portHighSpeed		= Bit(10),
	portTestMode		= Bit(11),
	portLeds		= Bit(12),
	};

// End of File

#endif
