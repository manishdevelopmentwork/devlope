
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Common Dialog Base Class
//

// Runtime Class

AfxImplementRuntimeClass(CCommonDialog, CDialog);

// Message Map

AfxMessageMap(CCommonDialog, CDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_WINDOWPOSCHANGING)
	
	AfxMessageEnd(CCommonDialog)
	};
	
// Message Handlers

BOOL CCommonDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_fHide = TRUE;

	AfxCallDefProc();

	m_fHide = FALSE;

	PlaceDialog();

	ShowWindow(SW_SHOW);

	return FALSE;
	}

BOOL CCommonDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	AfxRouteToDefProc();

	return TRUE;
	}

void CCommonDialog::OnWindowPosChanging(WINDOWPOS &Pos)
{
	if( m_fHide ) {

		Pos.flags &= ~SWP_SHOWWINDOW;

		return;
		}
	}

// End of File
