
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Executive_HPP

#define INCLUDE_Executive_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/MosExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Object
//

class CExecutive : public CMosExecutive, public IWaitMultiple
{
	public:
		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IExecutive
		IThread * METHOD GetCurrentThread(void);
		UINT      METHOD GetCurrentIndex(void);
		void      METHOD Sleep(UINT uTime);
		BOOL      METHOD ForceSleep(UINT uTime);
		UINT      METHOD GetTickCount(void);

		// IWaitMultiple
		UINT METHOD WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime);
		UINT METHOD WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime);
		UINT METHOD WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime);

	protected:
		// Overridables
		void AllocThreadObject(CMosExecThread * &pThread);
	};

// End of File

#endif
