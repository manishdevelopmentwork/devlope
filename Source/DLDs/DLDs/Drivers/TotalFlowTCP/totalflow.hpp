
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "totalbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow TCP/IP Driver
//

class CTotalFlowTcpMasterDriver : public CTotalFlowMasterDriver
{
	public:
		// Constructor
		CTotalFlowTcpMasterDriver(void);

		// Destructor
		~CTotalFlowTcpMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Config
		struct CCtx : public CTotalFlowMasterDriver::CCtx
		{
			DWORD     m_IP;
			UINT      m_uPort;
			char      m_fKeep;
			BOOL      m_fPing;
			UINT      m_uTime1;
			UINT      m_uTime3;
			ISocket * m_pSock;
			UINT      m_uLast;
			};

		// Current Device
		CCtx * m_pCtx;
		UINT   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Transport
		BOOL CheckLink(void);
		void AbortLink(void);
		BOOL Send(CBuffer *pBuff);
		UINT Recv(UINT uTime);
	};

// End of File
