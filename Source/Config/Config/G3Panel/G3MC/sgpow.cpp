
#include "intern.hpp"

#include "legacy.h"

#include "sgloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Strain Gage Power Mapping
//

// Runtime Class

AfxImplementRuntimeClass(CUISGPower, CUIControl);

// Constructor

CUISGPower::CUISGPower(CSGLoop *pLoop)
{
	m_pLoop   = pLoop;

	m_pLayout = NULL;

	m_pCtrl   = New CSGPowerWnd(pLoop);
	}

// Destructor

CUISGPower::~CUISGPower(void)
{
	}

// Operations

void CUISGPower::Update(void)
{
	m_pCtrl->Update();
	}

void CUISGPower::Exclude(CWnd &Wnd, CDC &DC)
{
	m_pCtrl->Exclude(Wnd, DC);
	}

// Core Overidable

void CUISGPower::OnLayout(CLayFormation *pForm)
{
	m_pLayout = New CLayFormPad( CRect(4, 4, 4, 4), horzNone | vertNone );

	pForm->AddItem(m_pLayout);
	}

void CUISGPower::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pCtrl->Create( L"",
			 WS_CHILD,
			 m_pLayout->GetChildRect(),
			 Wnd,
			 uID++,
			 NULL
			 );

	m_pCtrl->SetFont(afxFont(Dialog));

	AddControl((CCtrlWnd *) m_pCtrl);
	}

void CUISGPower::OnMove(void)
{
	m_pCtrl->MoveWindow(m_pLayout->GetChildRect(), TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Power Window
//

// Runtime Class

AfxImplementRuntimeClass(CSGPowerWnd, CWnd);

// Constructor

CSGPowerWnd::CSGPowerWnd(CSGLoop *pLoop)
{
	m_pLoop = pLoop;
	}

// Destructor

CSGPowerWnd::~CSGPowerWnd(void)
{
	}

// Operations

void CSGPowerWnd::Update(void)
{
	Invalidate(TRUE);
	}

void CSGPowerWnd::Exclude(CWnd &Wnd, CDC &DC)
{
	if( IsWindowVisible() ) {

		CRect Rect = GetClientRect();

		ClientToScreen(Rect);

		Wnd.ScreenToClient(Rect);

		DC.ExcludeClipRect(Rect);
		}
	}

// Message Map

AfxMessageMap(CSGPowerWnd, CWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)

	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CSGPowerWnd)
	};

// Message Handlers

BOOL CSGPowerWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CSGPowerWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	CSize Size = Rect.GetSize();

	CRect Work = CRect(Size);

	CBitmap   Bitmap(DC, Size);

	CMemoryDC WorkDC(DC);

	WorkDC.Select(Bitmap);

	WorkDC.FillRect(Work, afxBrush(BLACK));

	Draw(WorkDC, Work);

	DC.BitBlt(Rect, WorkDC, CPoint(), SRCCOPY);

	WorkDC.Deselect();
	}

// Implementation

void CSGPowerWnd::Draw(CDC &DC, CRect Rect)
{
	int ih  = m_pLoop->m_PowerDead / +2 - m_pLoop->m_PowerOffset;

	int dyh = m_pLoop->m_PowerRevGain;

	int dxh = 10000;

	int ic  = m_pLoop->m_PowerDead / -2 - m_pLoop->m_PowerOffset;

	int dyc = m_pLoop->m_PowerDirGain;

	int dxc = 10000;

	///////////////////////////////

	CPoint P2H(ih + MulDiv(m_pLoop->m_RevLimitLo, dxh, dyh), m_pLoop->m_RevLimitLo);

	CPoint P3H(ih + MulDiv(m_pLoop->m_RevLimitHi, dxh, dyh), m_pLoop->m_RevLimitHi);

	CPoint P1H(Min(P2H.x, -10000), P2H.y);

	CPoint P4H(Max(P3H.x, +10000), P3H.y);

	///////////////////////////////

	CPoint P2C(ic - MulDiv(m_pLoop->m_DirLimitLo, dxc, dyc), m_pLoop->m_DirLimitLo);

	CPoint P3C(ic - MulDiv(m_pLoop->m_DirLimitHi, dxc, dyc), m_pLoop->m_DirLimitHi);

	CPoint P1C(Max(P2C.x, +10000), P2C.y);

	CPoint P4C(Min(P3C.x, -10000), P3C.y);

	///////////////////////////////

	int xMin = Min(Min(P1H.x, P4C.x) - 1000, -15000);

	int yMin = -1000;

	int xMax = Max(Max(P4H.x, P1C.x) + 1000, +15000);

	int yMax = 11000;

	xMin = ((xMin - 999) / 1000) * 1000;

	xMax = ((xMax + 999) / 1000) * 1000;

	P1H.x = xMin;

	P4H.x = xMax;
	
	P1C.x = xMax;

	P4C.x = xMin;

	///////////////////////////////

	DC.Save();

	DC.SetMapMode(MM_ANISOTROPIC);

	DC.SetWindowExt(CSize(xMax - xMin + 1000, yMin - yMax - 1500) / 10);

	DC.SetWindowOrg(CPoint(xMin - 500, yMax + 500) / 10);

	DC.SetViewportExt(Rect.GetSize() - CSize(1, 1));

	DC.SetViewportOrg(Rect.GetTopLeft());

	CPen Pen0(CColor(0x80, 0x80, 0x80));

	CPen Pen1(CColor(0xFF, 0xFF, 0xFF));
	
	CPen Pen2(CColor(0x00, 0xFF, 0x00));
	
	CPen Pen3(CColor(0xFF, 0x00, 0x00));
	
	CPen Pen4(CColor(0x00, 0xFF, 0xFF));

	for( int x = xMin; x <= xMax; x += 1000 ) {

		switch( x ) {

			case 0:
				DC.Select(Pen1);
				break;

			case -10000:
			case +10000:
				DC.Select(Pen2);
				break;

			default:
				DC.Select(Pen0);
				break;
			}

		DC.MoveTo(CPoint(x, yMin) / 10);
		DC.LineTo(CPoint(x, yMax) / 10);

		DC.Deselect();
		}

	for( int y = yMin; y <= yMax; y += 1000 ) {

		switch( y ) {

			case 0:
				DC.Select(Pen1);
				break;

			case -10000:
			case +10000:
				DC.Select(Pen2);
				break;

			default:
				DC.Select(Pen0);
				break;
			}

		DC.MoveTo(CPoint(xMin, y) / 10);
		DC.LineTo(CPoint(xMax, y) / 10);

		DC.Deselect();
		}

	if( m_pLoop->m_DirLimitLo < m_pLoop->m_DirLimitHi ) {

		DC.Select(Pen4);

		DC.MoveTo(P1C / 10);
		DC.LineTo(P2C / 10);
		DC.LineTo(P3C / 10);
		DC.LineTo(P4C / 10);

		DC.Deselect();
		}

	if( m_pLoop->m_RevLimitLo < m_pLoop->m_RevLimitHi ) {

		DC.Select(Pen3);

		DC.MoveTo(P1H / 10);
		DC.LineTo(P2H / 10);
		DC.LineTo(P3H / 10);
		DC.LineTo(P4H / 10);

		DC.Deselect();
		}

	CFont Font(L"Tahoma", CSize(30, 50), FALSE);

	DC.Select(Font);

	DC.SetBkMode(TRANSPARENT);

	DC.SetTextColor(afxColor(WHITE));

	DC.DrawText(L"-100", CRect(-1200, -120,  -800, -200), DT_CENTER);

	DC.DrawText(L"0",    CRect( -200, -120,  +200, -200), DT_CENTER);

	DC.DrawText(L"+100", CRect( +800, -120, +1200, -200), DT_CENTER);

	DC.Deselect();

	DC.Restore();
	}

// End of File
