
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_LangManager_HPP

#define INCLUDE_LangManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangList;

//////////////////////////////////////////////////////////////////////////
//
// Language Manager
//

class DLLAPI CLangManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLangManager(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Current Language
		UINT    GetCurrentSlot(void) const;
		UINT    GetCurrentCode(void) const;
		UINT    GetEffectiveCode(void) const;
		UINT    GetNumericCode(void) const;
		WCHAR   GetDecPointChar(void) const;
		WCHAR   GetNumGroupChar(void) const;
		BOOL	IsGroupBoundary(UINT p) const;
		CString GetMonthName(UINT m) const;
		UINT	GetTimeFormat(void) const;
		UINT    GetDateFormat(void) const;
		WCHAR	GetDateSepChar(void) const;

		// Attributes
		UINT    GetSlotCount(void) const;
		UINT    GetUsedCount(void) const;
		BOOL    IsCodeUsed(UINT uCode) const;
		BOOL    IsSlotUsed(UINT uSlot) const;
		UINT    EnumLanguages(CArray <UINT> &List) const;
		UINT    GetSystemCode(void) const;
		UINT    GetCodeFromSlot(UINT uSlot) const;
		UINT    GetCurrentFlag(void) const;
		UINT    GetFlagFromSlot(UINT uSlot) const;
		UINT    GetFlagFromCode(UINT uCode) const;
		UINT    GetFlagFromAbbr(CString Abbr) const;
		CString GetCurrentName(void) const;
		CString GetNameFromSlot(UINT uSlot) const;
		CString GetNameFromCode(UINT uCode) const;
		CString GetAbbrFromSlot(UINT uSlot) const;
		CString GetAbbrFromCode(UINT uCode) const;
		CString GetKeybFromCode(UINT uCode) const;
		CString GetAppID(void) const;

		// Operations
		BOOL InvokeDialog(CWnd &Wnd);
		void DrawFlagFromSlot(CDC &DC, CRect Rect, UINT uSlot);
		void DrawFlagFromCode(CDC &DC, CRect Rect, UINT uCode);
		void DrawFlagFromAbbr(CDC &DC, CRect Rect, CString Abbr);
		void DrawFlag(CDC &DC, CRect Rect, UINT uFlag);
		BOOL SetKeyboard(void);
		BOOL SetKeyboard(UINT uSlot);
		void ClearKeyboard(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT        m_Handle;
		UINT	    m_Lang;
		UINT        m_System;
		UINT	    m_Global;
		UINT	    m_LocSep;
		UINT	    m_LexMode;
		UINT	    m_Server;
		CString	    m_AppID;
		CLangList * m_pLangs;

	protected:
		// Data Members
		CBitmap m_Bitmap;
		CString m_SystemKeyb;
		UINT    m_SystemCode;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void FindSystemInfo(void);
	};

// End of File

#endif
