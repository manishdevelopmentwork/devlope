
#include "intern.hpp"

#include "g3master.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-97 Paradigm Controls Limited
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Crimson Authentication
//

// Constructor

CCrimsonAuth::CCrimsonAuth(void)
{
	}

// Attributes

BOOL CCrimsonAuth::ReadChallenge(CByteArray &Device, CByteArray &Challenge)
{
	if( m_Rep.GetOpcode() == opReply ) {

		PCBYTE pData = m_Rep.GetData();

		BYTE   nd    = pData[0];

		BYTE   nc    = pData[1];

		if( nd && nc ) {

			Device.Empty();

			Challenge.Empty();

			Device.Append(pData + 2, nd);

			Challenge.Append(pData + 2 + nd, nc);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCrimsonAuth::CheckResponse(void)
{
	if( m_Rep.GetOpcode() == opReply ) {

		PCBYTE pData = m_Rep.GetData();

		return pData[0] ? TRUE : FALSE;
	}

	if( m_Rep.GetOpcode() == opNak ) {

		return TRUE;
	}

	return FALSE;
}

// Operations

BOOL CCrimsonAuth::GetChallenge(void)
{
	m_Req.StartFrame(servAuth, authGetChallenge);

	return SyncTransact(opReply, FALSE, TRUE);
}

BOOL CCrimsonAuth::SendResponse(CByteArray const &User, CByteArray const &Response)
{
	m_Req.StartFrame(servAuth, authSendResponse);

	m_Req.AddByte(BYTE(User.GetCount()));

	m_Req.AddByte(BYTE(Response.GetCount()));

	m_Req.AddBulk(User.GetPointer(), User.GetCount());

	m_Req.AddBulk(Response.GetPointer(), Response.GetCount());

	return SyncTransact(opReply, FALSE, FALSE);
}

// End of File
