
#include "Intern.hpp"

#include "EspWiFi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../ATLib/ModemChannel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ESP Wi-Fi Interface
//

// Constructor

CEspWiFi::CEspWiFi(string const &face, string const &root) : CGenericModem(face, root)
{
	m_scan = m_root + "/scan";

	ResetStatus();

	ClearStatus();
}

// Destructor

CEspWiFi::~CEspWiFi(void)
{
}

// Overridables

bool CEspWiFi::OnConfigure(void)
{
	return FindSled();
}

int CEspWiFi::OnExecute(void)
{
	int wait = 250;

	int code = CModemChannel::codeOkay;

	switch( m_state ) {

		case stateNotPresent:
		{
			if( GetMonotonic() >= m_timer + 30 ) {

				AfxTrace("hardware not responding\n");

				ResetHardware();

				m_timer = GetMonotonic();

				return 0;
			}

			if( m_cmd->IsPresent() ) {

				m_cmd->SetTrace(true);

				if( m_cmd->Command("") == CModemChannel::codeOkay ) {

					AfxTrace("found channels for %s\n", m_face.c_str());

					SetLinkState(true);

					m_state = stateCheckVersion;

					m_first = false;

					return 0;
				}
			}
		}
		break;

		case stateCheckVersion:
		{
			if( CheckVersion(code) ) {

				m_state = stateFindMac;
			}
		}
		break;

		case stateFindMac:
		{
			if( GetMacAddress(code) ) {

				ScanNetworks();

				m_state = stateInitialize;
			}
			else {
				if( ++m_count < 10 ) {

					return 2000;
				}

				ResetHardware();
			}
		}
		break;

		case stateInitialize:
		{
			if( GetConfig("mode", 0) == 0 ) {

				if( InitStation(code) ) {

					m_state = stateConnecting;
				}
			}
			else {
				if( InitAccessPoint(code) ) {

					m_state = stateListening;

					m_ctime = GetMonotonic();
				}
			}
		}
		break;

		case stateConnecting:
		{
			if( IsConnected(code) ) {

				m_state = stateConnected;

				m_ctime = GetMonotonic();
			}
			else {
				if( ++m_count >= 10 ) {

					ScanNetworks();

					m_count = 0;
				}
			}

			wait = 1000;
		}
		break;

		case stateConnected:
		{
			if( IsConnected(code) ) {

				m_cmd->SetTrace(false);
			}
			else {
				m_state = stateConnecting;

				m_cmd->SetTrace(true);
			}

			wait = 1000;
		}
		break;

		case stateListening:
		{
			if( Okay(code, "") ) {

				m_cmd->SetTrace(false);

				wait = 2000;
			}
		}
		break;
	}

	if( code == CModemChannel::codeOkay ) {

		return wait;
	}

	if( code == CModemChannel::codeFailed ) {

		m_state = stateNotPresent;

		ResetStatus();

		return 1000;
	}

	m_state = stateInitialize;

	ResetStatus();

	return 1000;
}

void CEspWiFi::OnNewState(void)
{
	if( m_state == stateConnected || m_state == stateListening ) {

		RunLinkStart(true);
	}

	if( m_prev == stateConnected || m_prev == stateListening ) {

		RunLinkStart(false);
	}

	WriteStatus();
}

void CEspWiFi::OnStopping(void)
{
	if( m_state >= stateConnecting ) {

		int code;

		DisableWiFi(code);
	}

	if( m_state == stateConnected || m_state == stateListening ) {

		RunLinkStart(false);
	}

	ClearStatus();
}

bool CEspWiFi::OnCommand(string const &cmd)
{
	if( cmd == "reset" || cmd == "failed" ) {

		ResetHardware();

		return true;
	}

	if( cmd == "scan" ) {

		ScanNetworks();

		return true;
	}

	return false;
}

// Device Commands

bool CEspWiFi::CheckVersion(int &code)
{
	ifstream stm("/opt/crimson/bin/firmware/wifi-bt-sled.version");

	if( stm.good() ) {

		string latest;

		stm >> latest;

		if( latest.size() ) {

			AfxTrace("available firmware is %s\n", latest.c_str());

			vector<string> lines;

			if( Okay(code, lines, "+GMR?") ) {

				if( lines.size() >= 2 ) {

					m_version = lines[1];

					AfxTrace("installed firmware is %s\n", m_version.c_str());

					if( latest != m_version ) {

						AfxTrace("performing update\n");

						CPrintf cmd("/opt/crimson/scripts/c3-update-wifi %u", m_sled);

						if( !System(cmd) ) {

							AfxTrace("update failed\n");
						}
						else {
							AfxTrace("update succeeded\n");
						}

						code = CModemChannel::codeFailed;

						return false;
					}

					return true;
				}
			}
		}

		AfxTrace("skipping update check\n");

		return true;
	}

	return false;
}

bool CEspWiFi::GetMacAddress(int &code)
{
	vector<string> lines;

	if( Okay(code, lines, "+CWMAC?") ) {

		return true;
	}

	return false;
}

bool CEspWiFi::DisableWiFi(int &code)
{
	Okay(code, "+CWQAP");

	Okay(code, "+CWMODE=0");

	return true;
}

bool CEspWiFi::InitStation(int &code)
{
	if( Okay(code, "+CWQAP") ) {

		if( Okay(code, "+CWMODE=1") ) {

			string ssid = GetConfig("ssid", "network");

			string psk  = GetConfig("psk", "");

			if( Okay(code, "+CWCSTA=%s,%s", ssid.c_str(), psk.c_str()) ) {

				if( Okay(code, "+CWJAP") ) {

					return true;
				}
			}
		}
	}

	return false;
}

bool CEspWiFi::InitAccessPoint(int &code)
{
	if( Okay(code, "+CWQAP") ) {

		if( Okay(code, "+CWMODE=2") ) {

			string ssid = GetConfig("ssid", "network");

			string psk  = GetConfig("psk", "");

			int    chan = GetConfig("channel", 7);

			int    cast = GetConfig("broadcast", 1);

			int    code = GetConfig("encrypt", 3);

			if( Okay(code, "+CWCAP=%s,%s,%u,%u,4,%u,100", ssid.c_str(), psk.c_str(), chan, code, cast) ) {

				if( Okay(code, "+CWJAP") ) {

					m_network = ssid;

					m_channel = chan;

					m_signal  = 100;

					return true;
				}
			}

			return true;
		}
	}

	return false;
}

bool CEspWiFi::IsConnected(int &code)
{
	vector<string> lines;

	if( Okay(code, lines, "+CWJAP?") ) {

		if( lines.size() >= 2 ) {

			vector<string> r;

			if( ParseReply(r, "+CWJAP:" + lines[1], 4) ) {

				m_network = r[0];
				m_apmac   = r[1];
				m_channel = atoi(r[2].c_str());
				m_signal  = ScaleSignal(atoi(r[3].c_str()));

				WriteStatus();

				return true;
			}
		}
	}

	if( code == CModemChannel::codeError ) {

		if( lines.size() >= 2 ) {

			vector<int> r;

			if( ParseReply(r, lines[1], 1) ) {

				// What else here???!!!

				if( r[0] == 9 ) {

					code = CModemChannel::codeOkay;

					return false;
				}
			}
		}
	}

	return false;
}

bool CEspWiFi::ScanNetworks(void)
{
	int code;

	vector<string> lines;

	if( Okay(code, lines, "+CWLAP") ) {

		map<string, int> aps;

		for( size_t n = 1; n < lines.size(); n++ ) {

			vector<string> r;

			if( ParseReply(r, ":" + lines[n], 6) ) {

				auto i = aps.find(r[1]);

				int  s = (i == aps.end()) ? -99 : i->second;

				int  t = atoi(r[2].c_str());

				aps[r[1]] = max(s, t);
			}
		}

		string json;

		if( aps.size() ) {

			multimap<int, string> sorted;

			for( auto const &ap : aps ) {

				sorted.insert(make_pair(ap.second, ap.first));
			}

			auto i = sorted.rbegin();

			for( UINT n = 0; i != sorted.rend() && n < 8; i++, n++ ) {

				if( n ) {

					json += ",";
				}

				json += "{";

				json += "\"ssid\":\"" + i->second + "\",";

				json += "\"signal\":" + CPrintf("%u", ScaleSignal(i->first));

				json += "}";
			}
		}

		if( true ) {

			json = "{\"scan\":[" + json + "]}";

			string   tmp(m_scan + ".tmp");

			ofstream stm(tmp);

			if( stm.good() ) {

				stm << json;

				stm.close();

				unlink(m_scan.c_str());

				rename(tmp.c_str(), m_scan.c_str());

				printf("%u networks found\n", aps.size());

				return true;
			}
		}
	}

	return false;
}

// Implementation

bool CEspWiFi::FindSled(void)
{
	if( m_face.size() == 7 ) {

		if( m_face.substr(0, 6) == "wlan0s" ) {

			m_sled = atoi(m_face.substr(6).c_str());

			m_dev1 = CPrintf("/dev/xrcontrol_%u", m_sled);

			m_cmd  = unique_ptr<CModemChannel>(new CModemChannel(m_dev1));

			m_cmd->SetEcho(false);

			return true;
		}
	}

	return false;
}

string CEspWiFi::GetStateName(void)
{
	switch( m_state ) {

		case stateCheckVersion:
			return "Checking Firmware";

		case stateFindMac:
			return "Waiting for MAC";

		case stateInitialize:
			return "Configuring";

		case stateConnecting:
			return "Connecting";

		case stateConnected:
			return "Connected";

		case stateListening:
			return "Listening";
	}

	return CInterface::GetStateName();
}

void CEspWiFi::ClearStatus(void)
{
	CInterface::ClearStatus();

	unlink(m_scan.c_str());
}

void CEspWiFi::ResetStatus(void)
{
	m_apmac.empty();

	m_network.empty();

	m_signal  = 0;

	m_channel = 0;

	m_ctime   = 0;
}

bool CEspWiFi::WriteStatus(void)
{
	string   tmp(m_status + ".tmp");

	ofstream stm(tmp);

	if( stm.good() ) {

		bool online = (m_state == stateConnected);

		stm << "{\n";

		stm << "\t\"state\": \"" << GetStateName().c_str() << "\",\n";

		stm << "\t\"version\": \"" << m_version.c_str() << "\",\n";

		switch( m_state ) {

			case stateConnecting:
			{
				stm << "\t\"mode\": 0,\n";

				stm << "\t\"online\": false,\n";
			}
			break;

			case stateConnected:
			{
				stm << "\t\"mode\": 0,\n";

				stm << "\t\"online\": true,\n";

				stm << "\t\"network\": \"" << m_network << "\",\n";

				stm << "\t\"apmac\": \"" << m_apmac << "\",\n";

				stm << "\t\"channel\": " << m_channel << ",\n";

				stm << "\t\"signal\": " << m_signal  << ",\n";

				stm << "\t\"ctime\": " << m_ctime << ",\n";
			}
			break;

			case stateListening:
			{
				stm << "\t\"mode\": 1,\n";

				stm << "\t\"online\": true,\n";

				stm << "\t\"network\": \"" << m_network << "\",\n";

				stm << "\t\"channel\": " << m_channel << ",\n";

				stm << "\t\"signal\": " << m_signal  << ",\n";

				stm << "\t\"ctime\": " << m_ctime << ",\n";
			}
			break;
		}

		AddTraffic(stm);

		stm << "\t\"valid\": true\n";

		stm << "}\n";

		stm.close();

		unlink(m_status.c_str());

		rename(tmp.c_str(), m_status.c_str());

		return true;
	}

	return false;
}

int CEspWiFi::ScaleSignal(int s)
{
	if( s >= -35 ) {

		return 31;
	}

	if( s <= -95 ) {

		return 0;
	}

	return (31 * (s + 95)) / (95 - 35);
}

// End of File
