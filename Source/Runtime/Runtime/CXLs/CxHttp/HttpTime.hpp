
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HTTPTIME_HPP

#define	INCLUDE_HTTPTIME_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <time.h>

//////////////////////////////////////////////////////////////////////////
//
// HTTP Formatted Time
//

class DLLAPI CHttpTime
{
	public:
		// Operations
		static CString Format(time_t t);
		static CString Format(struct timeval const *pt);
		static CString Format(UINT uMode, time_t t);
		static CString Format(UINT uMode, struct timeval const *pt);
		static time_t  Parse (CString r);

	protected:
		// Implementation
		static UINT mtoi(PCTXT p);
	};

// End of File

#endif
