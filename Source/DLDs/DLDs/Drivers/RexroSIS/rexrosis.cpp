
#include "intern.hpp"

#include "rexrosis.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CRexrothSISDriver);

// Constructor

CRexrothSISDriver::CRexrothSISDriver(void)
{
	m_Ident	= DRIVER_ID;
}

// Destructor

CRexrothSISDriver::~CRexrothSISDriver(void)
{
}

// Configuration

void MCALL CRexrothSISDriver::Load(LPCBYTE pData)
{
}

void MCALL CRexrothSISDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CRexrothSISDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CRexrothSISDriver::Open(void)
{
}

// Device

CCODE MCALL CRexrothSISDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bSDrop = GetByte(pData);

			m_pCtx->m_bRDrop = GetByte(pData);

			m_pCtx->m_bModel = MODELECO /* GetByte(pData) */;

			m_pCtx->m_ErrorCode = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CRexrothSISDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CRexrothSISDriver::Ping(void)
{
	DWORD Data[1];
	CAddress A;

	A.a.m_Table = PCUR;
	A.a.m_Extra = 0x0;
	A.a.m_Offset= 0xB; // S0 Table, Parameter 11

	return Read(A, Data, 1);
}

CCODE MCALL CRexrothSISDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case PCUR:
		case PMIN:
		case PMAX:
		case PATT:
		case PUNIT:

			if( ReadParameter(Addr, pData, &uCount) ) return uCount;
			break;

		case LISTP:
		case LISTS:
		case LISTA:
		case LISTC:

			if( ReadList(Addr, pData, &uCount) ) return uCount;
			break;

		case SP_ERR:

			*pData = m_pCtx->m_ErrorCode;
			return 1;
	}

	m_pCtx->m_ErrorCode |= (DWORD(MakeParamAddress(Addr)) << 16);

	return CCODE_ERROR;
}

CCODE MCALL CRexrothSISDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case PCUR:
			if( WriteParameter(Addr, pData, &uCount) ) return uCount;
			break;

		case LISTP:
		case LISTS:
		case LISTA:
		case LISTC:
			if( WriteList(Addr, pData, &uCount) ) return uCount;
			break;

		case SP_ERR:
			m_pCtx->m_ErrorCode = *pData;
			return 1;

		case PMIN:
		case PMAX:
		case PATT:
		case PUNIT:
			return 1;
	}

	m_pCtx->m_ErrorCode |= (DWORD(MakeParamAddress(Addr)) << 16);

	return CCODE_ERROR;
}

// Implementation

BOOL CRexrothSISDriver::ReadParameter(AREF Addr, PDWORD pData, UINT * pCount)
{
	CAddress A;

	A.a.m_Table  = Addr.a.m_Table;
	A.a.m_Type   = Addr.a.m_Type;
	A.a.m_Extra  = 0;

	DWORD dAttribute;

	DWORD dData;

	UINT i;

	BYTE bType = HIBYTE(Addr.a.m_Offset);

	UINT uCt = *pCount;

	MakeMin(uCt, 8);

	for( i = 0; i < uCt; i++ ) {

		A.a.m_Offset = MakeParamAddress(Addr) + i;

		if( GetParameterAttribute(A, &dAttribute, bType) ) {

			if( Addr.a.m_Table == PATT ) pData[i] = dAttribute;

			else {
				if( !SendParameterReadReq(A, &dData, &dAttribute, bType) ) return FALSE;

				if( Addr.a.m_Table == PUNIT ) pData[i] = dData;

				else {
					UINT uDP = GetDecimalPositions(A, dAttribute, bType);

					pData[i] = FormatRcvData(dAttribute, dData, uDP);
				}
			}
		}

		else return FALSE;
	}

	*pCount = uCt;

	return TRUE;
}

BOOL CRexrothSISDriver::SendParameterReadReq(CAddress Addr, PDWORD pData, PDWORD pAttribute, BYTE bType)
{
	DWORD dResp;

	ConfigureHeaders(Addr.a.m_Table, GetParServiceCode(ISREAD));

	AddParameter(Addr.a.m_Offset, bType);

	if( Transact() ) {

		if( GetResponse(&dResp, *pAttribute) ) {

			*pData = dResp;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CRexrothSISDriver::WriteParameter(AREF Addr, PDWORD pData, UINT * pCount)
{
	CAddress A;

	A.a.m_Table  = Addr.a.m_Table;
	A.a.m_Type   = Addr.a.m_Type;
	A.a.m_Extra  = 0;

	DWORD dAttribute = 0;

	UINT i;

	BYTE bType = HIBYTE(Addr.a.m_Offset);

	UINT uCt = *pCount;

	MakeMin(uCt, 8);

	for( i = 0; i < uCt; i++ ) {

		A.a.m_Offset = MakeParamAddress(Addr) + i;

		if( GetParameterAttribute(A, &dAttribute, bType) ) {

			m_uWriteDP = GetDecimalPositions(A, dAttribute, bType);

			ConfigureHeaders(A.a.m_Table, GetParServiceCode(ISWRITE));

			AddParameter(A.a.m_Offset, bType);

			AddWriteData(pData[i], dAttribute);

			if( !Transact() || !GetResponse(pData, 0) ) return FALSE;
		}

		else return FALSE;
	}

	*pCount = uCt;

	return TRUE;
}

BOOL CRexrothSISDriver::ReadList(AREF Addr, PDWORD pData, UINT * pCount)
{
	CAddress A;

	A.a.m_Offset = MakeParamAddress(Addr);
	A.a.m_Table  = Addr.a.m_Table;
	A.a.m_Type   = Addr.a.m_Type;
	A.a.m_Extra  = 0;

	UINT uListType = GetListType(Addr.a.m_Table);

	DWORD dAttribute;

	if( !GetParameterAttribute(A, &dAttribute, uListType) ) return FALSE;

	UINT uDP = GetDecimalPositions(Addr, dAttribute, uListType);

	UINT uCount = *pCount;

	MakeMin(uCount, 8);

	ConfigureHeaders(A.a.m_Table, GetListServiceCode(ISREAD));

	AddParameter(A.a.m_Offset, uListType);

	AddData(LOBYTE(Addr.a.m_Offset) * 2, 0x1000);

	(dAttribute & ATT_SZMASK) == ATT_SZ4 ? AddData(uCount*2, 0x1000) : AddData(uCount, 0x1000);

	if( Transact() ) {

		if( GetListResponse(pData, uCount, dAttribute, uDP) ) {

			*pCount = uCount;

			return TRUE;
		}

		else { // if end of list reached, indicate but don't crash

			if( m_pCtx->m_ErrorCode && m_pCtx->m_ErrorCode < uCount ) {

				m_pCtx->m_ErrorCode |= (DWORD(MakeParamAddress(Addr)) << 16);

				*pCount = uCount;

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CRexrothSISDriver::WriteList(AREF Addr, PDWORD pData, UINT * pCount)
{
	CAddress A;

	A.a.m_Offset = MakeParamAddress(Addr);
	A.a.m_Type   = Addr.a.m_Type;
	A.a.m_Extra  = 0;
	A.a.m_Table  = Addr.a.m_Table;

	DWORD dMin;
	DWORD dMax;
	DWORD dIntData;

	DWORD dAttribute;
	DWORD dSaveAttribute;

	UINT uCount = *pCount;
	UINT uByteCount;
	UINT i;
	UINT j;

	BYTE bb[32];

	PBYTE pb = &(bb[0]);

	MakeMin(uCount, 8);

	UINT uListType = GetListType(Addr.a.m_Table);

	if( !GetParameterAttribute(A, &dAttribute, uListType) )	return FALSE;

	dSaveAttribute = dAttribute;

	m_uWriteDP = GetDecimalPositions(A, dAttribute, uListType);

	uByteCount = AssembleListData(pData, pb, uCount, dAttribute);

	A.a.m_Table = PMIN;

	if( !SendParameterReadReq(A, &dMin, &dAttribute, uListType) )	return FALSE;

	A.a.m_Table = PMAX;

	if( !SendParameterReadReq(A, &dMax, &dAttribute, uListType) )	return FALSE;

	for( i = 0; i < uCount; i++ ) {

		switch( dSaveAttribute & ATT_TYPEMASK ) {

			case ATT_UDEC:
			case ATT_BIN:
			case ATT_HEX:

				dIntData = 0;

				for( j = 0; j < 4; j++ ) {

					dIntData = (dIntData<<8) + pb[(i*4)+(3-j)];
				}

				if( dIntData < dMin || dIntData > dMax ) {

					m_pCtx->m_ErrorCode = i+(LOBYTE(Addr.a.m_Offset));

					return FALSE;
				}
				break;

			case ATT_SDEC:
			case ATT_REAL:

				dMin = FormatRcvData(dAttribute, dMin, m_uWriteDP);
				dMax = FormatRcvData(dAttribute, dMax, m_uWriteDP);

				if( I2R(pData[i]) < I2R(dMin) || I2R(pData[i]) > I2R(dMax) ) {

					m_pCtx->m_ErrorCode = i+(LOBYTE(Addr.a.m_Offset));

					return FALSE;
				}
				break;
		}
	}

	A.a.m_Table  = Addr.a.m_Table;

	dAttribute = dSaveAttribute;

	if( GetParameterAttribute(A, &dAttribute, uListType) ) {

		ConfigureHeaders(A.a.m_Table, GetListServiceCode(ISWRITE));

		AddParameter(A.a.m_Offset, uListType);

		AddData(LOBYTE(Addr.a.m_Offset) * 2, 0x1000); // list item start number

		AddData(uByteCount/2, 0x1000);

		for( i = 0; i < uByteCount; i++ ) {

			AddByte(pb[i]);
		}

		if( Transact() ) {

			*pCount = uCount;

			return TRUE;
		}
	}

	return FALSE;
}

// Frame Building

void CRexrothSISDriver::ConfigureHeaders(UINT uTable, BYTE bOp)
{
	StartFrame(bOp);

	AddUserHeader(uTable);
}

void CRexrothSISDriver::AddUserHeader(UINT uTable)
{
	BYTE b = 0;

	m_fReturnAsLong = FALSE;

	switch( uTable ) {

		case LISTP:
		case LISTS:
		case LISTA:
		case LISTC:
		case PCUR:	b = DBECUR; break;

		case PMIN:	b = DBEMIN; break;

		case PMAX:	b = DBEMAX; break;

		case PATT:
			b = DBEATT;
			m_fReturnAsLong = TRUE;
			break;

		case PUNIT:
			b = DBEUNIT;
			m_fReturnAsLong = TRUE;
			break;
	}

	AddByte(b | TFINAL);

	AddByte(m_pCtx->m_bRDrop);
}

void CRexrothSISDriver::StartFrame(BYTE bOp)
{
	m_bTx[0] = STX;

	m_bTx[1] = 0; // Checksum = 0-sum of characters

	m_bTx[2] = 0; // DatL count

	m_bTx[3] = 0; // DatW count

	m_uPtr = 4;

	AddByte(0); // no status(7-5),command(4),running(3),sub-addr's(2-0)

	AddByte(bOp);

	AddByte(m_pCtx->m_bSDrop);

	AddByte(m_pCtx->m_bRDrop);
}

void CRexrothSISDriver::EndFrame(void)
{
	BYTE bCheck = 0;

	for( UINT i = 0; i < m_uPtr; i++ ) {

		bCheck -= m_bTx[i];
	}

	m_bTx[1] = bCheck;
}

void CRexrothSISDriver::AddParameter(UINT Address, BYTE bType)
{
	// bType = 00TT TSSS - Bit 0 goes in high bit of parameter word
	AddByte(bType >> 4); // bits 1 and 2 of Type S=0, P=1, A=0x10, C=0x20

	AddData((UINT(bType & 0xF) << 12) + (Address & 0xFFF), 0x1000);
}

void CRexrothSISDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	if( m_uPtr > 8 ) {

		m_bTx[2]++;
		m_bTx[3]++;
	}
}

void CRexrothSISDriver::AddData(DWORD dData, DWORD dMask)
{
	while( dMask ) {

		AddByte(dData % 256);

		dData >>= 8;

		dMask >>= 8;
	}
}

void CRexrothSISDriver::AddWriteData(DWORD dData, DWORD dAttribute)
{
	DWORD d = (DWORD) (*((float *) &dData));

	UINT uDP = m_uWriteDP;

	switch( dAttribute & ATT_TYPEMASK ) {

		case ATT_REAL:

			AddData(dData, 0x10000000);
			break;

		case ATT_BIN:
		case ATT_HEX:

			AddDataPerSize(d, dAttribute);
			break;

		case ATT_UDEC:
		case ATT_SDEC:

			double f;

			f = *(C3REAL*) &dData;

			while( uDP ) {

				f *= 10.0;

				uDP--;
			}

			AddDataPerSize((DWORD) f, dAttribute);

			break;

		case ATT_ASC:
		case ATT_IDN:

			AddData(d, 0x10000000);
			break;
	}
}

void CRexrothSISDriver::AddDataPerSize(DWORD dData, DWORD dAttribute)
{
	switch( dAttribute & ATT_SZMASK ) {

		case ATT_SZ2:
			AddData(dData, 0x1000);
			break;

		case ATT_SZ4:
			AddData(dData, 0x10000000);
			break;

		case ATT_SZVAR1:
		case ATT_SZVAR2:
		case ATT_SZVAR4:
			AddData(dData, 0x10000000);
			break;
	}
}

UINT CRexrothSISDriver::AssembleListData(PDWORD pData, PBYTE pb, UINT uCount, DWORD dAttribute)
{
	DWORD d;

	UINT uByteCount = 0;

	BOOL fSize4 = ((dAttribute & ATT_SZMASK) >> ATT_SZP) == ATT_SZFOUR;

	UINT uDP;

	for( UINT i = 0; i < uCount; i++ ) {

		switch( dAttribute & ATT_TYPEMASK ) {

			case ATT_REAL:

				d = pData[i];

				fSize4  = TRUE;

				break;

			case ATT_UDEC:
			case ATT_SDEC:

				double f;

				uDP  = m_uWriteDP;

				f = *(C3REAL*)&(pData[i]);

				while( uDP ) {

					f *= 10.0;

					uDP--;
				}

				d = (DWORD) f;

				break;

			default:
				d = (DWORD) (*((float *)&(pData[i])));
				break;
		}

		pb[uByteCount++] = LOBYTE(LOWORD(d));

		pb[uByteCount++] = HIBYTE(LOWORD(d));

		if( fSize4 ) {

			pb[uByteCount++] = LOBYTE(HIWORD(d));

			pb[uByteCount++] = HIBYTE(HIWORD(d));
		}
	}

	return uByteCount;
}

// Data Transfer

BOOL CRexrothSISDriver::Transact(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();

	return GetReply();
}

BOOL CRexrothSISDriver::GetReply(void)
{
	UINT uTimer;
	UINT uData;
	UINT uState  = 0;
	UINT uSize   = sizeof(m_bRx) - 1;
	BYTE bCheck  = 0;
	BOOL fStatus = FALSE;

	BYTE bData;

	m_uRcvPtr = 0;

	SetTimer(500);

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
		}

		bData = LOBYTE(uData);

		bCheck += bData;

		if( uState >= 8 ) uSize--;

//**/		AfxTrace1("<%2.2x>", bData );

		switch( uState ) {

			case 0: // StZ
				if( bData == STX ) {

					bCheck = STX;

					uState = 1;

					SetTimer(500);
				}
				break;

			case 1: // Check byte
				uState = 2;
				break;

			case 2: // DatL
				uSize  = bData;

				uState = uSize <= sizeof(m_bRx) ? 3 : 20;

				break;

			case 3: // DatW
				m_uReturnListCount = bData;

				uState = bData == uSize ? 4 : 20;

				break;

			case 4: // Cntrl
				uState = 5;

				break;

			case 5: // Service
				uState = bData == m_bTx[5] ? 6 : 20;

				break;

			case 6: // AdrS
				uState = bData == m_bTx[7] ? 7 : 20;

				break;

			case 7: // AdrE
				if( bData != m_bTx[6] ) {

					uState = 20;
				}

				else {
					if( !uSize ) return CheckCheck(bCheck);

					else uState = 8;
				}
				break;

			case 8: // User Header Start - Status

				fStatus = bData;

				if( !uSize ) return CheckCheck(bCheck);

				uState = 9;

				break;

			case 9: // Control Byte
				if( bData != m_bTx[8] ) {

					uState = 20;
				}

				else {
					if( !uSize ) return CheckCheck(bCheck);

					else uState = 10;
				}

				break;

			case 10: // Device Address
				if( bData != m_pCtx->m_bRDrop ) {

					uState = 20;
				}

				else {
					if( !uSize ) return CheckCheck(bCheck);

					else {
						m_uRcvPtr = 0;
						uState = 11;
					}
				}

				break;

			case 11: // Finally, some data
				if( m_uRcvPtr < (sizeof(m_bRx) - 1) ) m_bRx[m_uRcvPtr++] = bData;

				if( !uSize ) {

					if( !fStatus ) return CheckCheck(bCheck);

					m_pCtx->m_ErrorCode = GetRxBufferData(2, 0);

					return FALSE;
				}

				break;

			case 20:
				if( !uSize ) {

					m_pCtx->m_ErrorCode = 0x9999;

					return FALSE;
				}

				break;
		}
	}

	return FALSE;
}

BOOL CRexrothSISDriver::CheckCheck(BYTE bCheck)
{
	if( bCheck ) m_pCtx->m_ErrorCode = 0x9999;

	return !bCheck;
}

// Response Handlers

BOOL CRexrothSISDriver::GetResponse(PDWORD pData, DWORD dAttribute)
{
	UINT uCount = 4;

	switch( dAttribute & ATT_SZMASK ) {

		case ATT_SZ2:	uCount = 2;	break;
		case ATT_SZ4:	uCount = 4;	break;
		case ATT_SZVAR1:
		case ATT_SZVAR2:
		case ATT_SZVAR4:
			uCount = min(4, m_uRcvPtr);
			break;
	}

	pData[0] = GetRxBufferData(uCount, 0);

	return TRUE;
}

BOOL CRexrothSISDriver::GetListResponse(PDWORD pData, UINT uCount, DWORD dAttribute, UINT uDP)
{
	UINT i;
	UINT uBufferPosition = 0;
	UINT uReturnedCount;

	UINT uByteCount = (dAttribute & ATT_SZMASK) == ATT_SZ4 ? 4 : 2;

	uReturnedCount  = (m_uReturnListCount - 3) / uByteCount;

	DWORD d;

	for( i = 0; i < uCount; i++, uBufferPosition += uByteCount ) {

		d = GetRxBufferData(uByteCount, uBufferPosition);

		pData[i] = i < uReturnedCount ? FormatRcvData(dAttribute, d, uDP) : 0;
	}

	if( uCount > uReturnedCount ) {

		m_pCtx->m_ErrorCode = uCount - uReturnedCount;

		return FALSE;
	}

	return TRUE;
}

DWORD CRexrothSISDriver::GetRxBufferData(UINT uCount, UINT uPos)
{
	DWORD d = 0;

	UINT uEnd = uPos + uCount - 1;

	for( UINT i = 0; i < uCount; i++ ) {

		d <<= 8;

		d += m_bRx[uEnd - i];
	}

	return d;
}

DWORD CRexrothSISDriver::FormatRcvData(DWORD dAttribute, DWORD dData, UINT uDP)
{
	UINT uType = dAttribute & ATT_TYPEMASK;

	BOOL fSize2 = (dAttribute & ATT_SZMASK) == ATT_SZ2;
	BOOL fNeg   = FALSE;

	if( m_fReturnAsLong || (uType == ATT_REAL) ) return dData;

	if( uType == ATT_SDEC ) {

		if( fSize2 && (dData & 0x8000) ) dData |= 0xFFFF0000;

		if( dData & 0x80000000 ) {

			fNeg = TRUE;

			dData = -dData;
		}
	}

	C3REAL f;

	f = (C3REAL) dData;

	if( fNeg ) f = -f;

	if( uType == ATT_UDEC || uType == ATT_SDEC ) {

		while( uDP-- ) f /= 10.0;
	}

	return *((DWORD *) &f);
}

// Port Access

void CRexrothSISDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	m_pData->Write(PCBYTE(m_bTx), m_uPtr, FOREVER);
}

UINT CRexrothSISDriver::Get(UINT uTimer)
{
	return m_pData->Read(uTimer);
}

// Helpers

UINT CRexrothSISDriver::GetListType(UINT uTable)
{
	switch( uTable ) {

		case LISTS:	return TYPESV;
		case LISTA:	return TYPEAV;
		case LISTC:	return TYPECV;
	}

	return TYPEPV;
}

UINT CRexrothSISDriver::MakeParamAddress(AREF Addr)
{
	UINT uHighIDN = Addr.a.m_Extra << 8;

	return uHighIDN + (IsList(Addr.a.m_Table) ? HIBYTE(Addr.a.m_Offset) : LOBYTE(Addr.a.m_Offset));
}

BOOL CRexrothSISDriver::GetParameterAttribute(CAddress Addr, PDWORD pAttribute, BYTE bType)
{
	CAddress A;

	A.a.m_Offset = Addr.a.m_Offset;
	A.a.m_Table  = PATT;
	A.a.m_Extra  = Addr.a.m_Extra;
	A.a.m_Type   = Addr.a.m_Type;

	DWORD dData;

	DWORD dAttribute = ATT_HEX | ATT_SZ4;

	if( SendParameterReadReq(A, &dData, &dAttribute, bType) ) {

		*pAttribute = dData;

		return TRUE;
	}

	return FALSE;
}

UINT CRexrothSISDriver::GetParServiceCode(BOOL fIsRead)
{
	if( m_pCtx->m_bModel == MODELECO ) return fIsRead ? ECOPARREAD : ECOPARWRITE;

	return fIsRead ? SYNPARREAD : SYNPARWRITE;
}

UINT CRexrothSISDriver::GetListServiceCode(BOOL fIsRead)
{
	if( m_pCtx->m_bModel == MODELECO ) return fIsRead ? ECOLISTREAD : ECOLISTWRITE;

	return fIsRead ? SYNLISTREAD : SYNLISTWRITE;
}

BOOL CRexrothSISDriver::IsList(UINT uTable)
{
	return !((uTable == SP_ERR) || (uTable < LISTP));
}

// Determine number of decimal point locations for non-real decimal values

UINT CRexrothSISDriver::GetDecimalPositions(CAddress Addr, DWORD dAttribute, BYTE bType)
{
	return min(9, (dAttribute & ATT_DPMASK) >> ATT_DPP);
}

// End of File
