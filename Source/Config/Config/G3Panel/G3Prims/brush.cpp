
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Brush
//

// Dynamic Class

AfxImplementDynamicClass(CPrimBrush, CCodedHost);

// Static Data

BOOL  CPrimBrush::m_ShadeMode = 0;

COLOR CPrimBrush::m_ShadeCol1 = 0;

COLOR CPrimBrush::m_ShadeCol2 = 0;

// Constructor

CPrimBrush::CPrimBrush(void)
{
	m_Pattern = brushFore;

	m_pColor1 = New CPrimColor;

	m_pColor2 = New CPrimColor;
	}

// UI Managament

void CPrimBrush::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "Pattern" ) {

			DoEnables(pHost);
			}

		if( Tag == "Color1" ) {

			pHost->UpdateUI(this, "Pattern");
			}

		if( Tag == "Color2" ) {

			pHost->UpdateUI(this, "Pattern");
			}

		if( !Tag.IsEmpty() ) {

			CPrim *pPrim = (CPrim *) GetParent();

			pPrim->OnUIChange(pHost, pItem, L"PrimProp");
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimBrush::IsNull(void) const
{
	return m_Pattern == brushNull;
	}

// Operations

void CPrimBrush::Set(COLOR Color)
{
	m_Pattern = brushFore;

	m_pColor1->Set(Color);

	m_pColor2->Set(GetRGB(31,31,31));
	}

// Drawing

void CPrimBrush::FillRect(IGDI *pGDI, R2 Rect)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_pColor1->GetColor());

			pGDI->SetBrushBack (m_pColor2->GetColor());

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillRect(PassRect(Rect));
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle / 2;

			m_ShadeCol1 = m_pColor1->GetColor();

			m_ShadeCol2 = m_pColor2->GetColor();
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeRect(PassRect(Rect), pShader);
			}
		}
	}

void CPrimBrush::FillEllipse(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_pColor1->GetColor());

			pGDI->SetBrushBack (m_pColor2->GetColor());

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillEllipse(PassRect(Rect), uType);
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle / 2;

			m_ShadeCol1 = m_pColor1->GetColor();

			m_ShadeCol2 = m_pColor2->GetColor();
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeEllipse(PassRect(Rect), uType, pShader);
			}
		}
	}

void CPrimBrush::FillWedge(IGDI *pGDI, R2 Rect, UINT uType)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_pColor1->GetColor());

			pGDI->SetBrushBack (m_pColor2->GetColor());

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillWedge(PassRect(Rect), uType);
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle / 2;

			m_ShadeCol1 = m_pColor1->GetColor();

			m_ShadeCol2 = m_pColor2->GetColor();
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadeWedge(PassRect(Rect), uType, pShader);
			}
		}
	}

void CPrimBrush::FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound)
{
	if( !IsNull() ) {

		if( m_Pattern < 16 ) {

			pGDI->SetBrushFore (m_pColor1->GetColor());

			pGDI->SetBrushBack (m_pColor2->GetColor());

			pGDI->SetBrushStyle(m_Pattern);

			pGDI->FillPolygon(pList, uCount, dwRound);
			}
		else {
			UINT uStyle = m_Pattern - 16;

			m_ShadeMode = uStyle /2;

			m_ShadeCol1 = m_pColor1->GetColor();

			m_ShadeCol2 = m_pColor2->GetColor();
			
			PSHADER pShader = (uStyle % 2) ? Shader1 : Shader2;

			pGDI->SetBrushStyle(brushFore);

			pGDI->ShadePolygon(pList, uCount, dwRound, pShader);
			}
		}
	}

// Persistance

void CPrimBrush::Init(void)
{
	CUIItem::Init();

	m_pColor1->Set(GetRGB(12,12,24));

	m_pColor2->Set(GetRGB(31,31,31));
	}

// Download Support

BOOL CPrimBrush::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddByte(BYTE(m_Pattern));

	Init.AddItem(itemSimple, m_pColor1);

	Init.AddItem(itemSimple, m_pColor2);

	return TRUE;
	}

// Color Mixing

COLOR CPrimBrush::Mix16(COLOR a, COLOR b, int p, int c)
{
	if( c ) {

		int ra = GetRED  (a);
		int ga = GetGREEN(a);
		int ba = GetBLUE (a);

		int rb = GetRED  (b);
		int gb = GetGREEN(b);
		int bb = GetBLUE (b);

		int rc = ra + ((rb - ra) * p / c);
		int gc = ga + ((gb - ga) * p / c);
		int bc = ba + ((bb - ba) * p / c);

		return GetRGB(rc, gc, bc);
		}

	return a;
	}

DWORD CPrimBrush::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
	}

// Shaders

BOOL CPrimBrush::Shader1(IGDI *pGDI, int p, int c)
{
	static BOOL  f32 = FALSE;

	static COLOR q16 = 0;

	static DWORD q32 = 0;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k32 - q32 ) {

				pGDI->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(m_ShadeCol1, m_ShadeCol2, p, c);

			if( k16 - q16 ) {

				pGDI->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGDI->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	return m_ShadeMode;
	}

BOOL CPrimBrush::Shader2(IGDI *pGDI, int p, int c)
{
	static BOOL  f32 = FALSE;

	static COLOR q16 = 0;

	static DWORD q32 = 0;

	if( c ) {

		if( f32 ) {

			DWORD k32 = Mix32(m_ShadeCol2, m_ShadeCol1, abs(c/2-p), c/2);

			if( k32 - q32 ) {

				pGDI->SetBrushFore(k32);

				q32 = k32;

				return TRUE;
				}
			}
		else {
			COLOR k16 = Mix16(m_ShadeCol2, m_ShadeCol1, abs(c/2-p), c/2);

			if( k16 - q16 ) {

				pGDI->SetBrushFore(k16);

				q16 = k16;

				return TRUE;
				}
			}

		return FALSE;
		}

	f32 = (pGDI->GetColorFormat() == colorRGB888);

	q16 = 0xFFFF;

	q32 = 0x00000000;

	return m_ShadeMode;
	}

// Meta Data

void CPrimBrush::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Pattern);
	Meta_AddObject (Color1);
	Meta_AddObject (Color2);

	Meta_SetName((IDS_BRUSH));
	}

// Implementation

void CPrimBrush::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Color1", m_Pattern > 0);

	pHost->EnableUI(this, "Color2", m_Pattern > 2);
	}

// End of File
