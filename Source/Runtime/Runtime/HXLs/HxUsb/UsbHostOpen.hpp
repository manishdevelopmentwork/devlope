
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostOpen_HPP

#define	INCLUDE_UsbHostOpen_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostHardwareDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Open Host Controller Interface
//

class CUsbHostOpen : public CUsbHostHardwareDriver
{
	public:
		// Constructor
		CUsbHostOpen(void);
	};

// End of File

#endif
