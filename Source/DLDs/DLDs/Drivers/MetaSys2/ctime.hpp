
//////////////////////////////////////////////////////////////////////////
//
// Time Support
//

extern DWORD Time(UINT h, UINT m, UINT s);
extern DWORD Date(UINT y, UINT m, UINT d);
extern void  GetWholeDate(DWORD t, UINT *p);
extern void  GetWholeTime(DWORD t, UINT *p);
extern UINT  GetYear(DWORD t);
extern UINT  GetMonth(DWORD t);
extern UINT  GetDate(DWORD t);
extern UINT  GetDays(DWORD t);
extern UINT  GetWeeks(DWORD t);
extern UINT  GetDay(DWORD t);
extern UINT  GetWeek(DWORD t);
extern UINT  GetWeekYear(DWORD t);
extern UINT  GetHours(DWORD t);
extern UINT  GetHour(DWORD t);
extern UINT  GetMin(DWORD t);
extern UINT  GetSec(DWORD t);
extern UINT  GetMonthDays(UINT y, UINT m);

// End of File
