
#include "Intern.hpp"

#include "UsbSerialFtdi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial Ftdi Port
//

// Instantiator

IPortObject * Create_UsbSerialFtdi(IUsbHostFuncDriver *pDriver)
{
	CUsbSerialFtdi *p = New CUsbSerialFtdi(pDriver);

	p->Open();

	return p;
	}

// Constructor

CUsbSerialFtdi::CUsbSerialFtdi(IUsbHostFuncDriver *pDriver)
{
	m_pDriver  = (IUsbHostFtdi *) pDriver;

	m_pHandler = NULL;

	m_pSwitch  = NULL;

	m_fOpen	   = false;

	m_fClosing = false;

	m_pTimer   = CreateTimer();
	}

// Destructor

CUsbSerialFtdi::~CUsbSerialFtdi(void)
{
	m_pTimer->Release();
	}

// IDevice

BOOL METHOD CUsbSerialFtdi::Open(void)
{
	BindDriver(m_pDriver);

	m_pTimer->SetPeriod(5);

	m_pTimer->SetHook(this, 0);

	return TRUE;
	}

// IPortObject

void METHOD CUsbSerialFtdi::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
	}

void METHOD CUsbSerialFtdi::Bind(IPortSwitch *pSwitch)
{
	m_pSwitch = pSwitch;
	}

UINT METHOD CUsbSerialFtdi::GetPhysicalMask(void)
{
	return m_pSwitch->GetMask(0);
}

BOOL METHOD CUsbSerialFtdi::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		m_Config = Config;

		if( InitUart() && InitBaudRate() && InitFormat() && InitPhysical() ) {

			m_fOpen    = true;

			m_fClosing = false;

			m_pHandler->OnOpen(m_Config);

			m_pTimer->Enable(true);

			m_uSendState = stateIdle;

			m_uRecvState = stateIdle;

			StartRecv();

			return true;
			}
		}

	return false;
	}

void METHOD CUsbSerialFtdi::Close(void)
{	
	if( m_fOpen ) {

		m_fClosing = true;

		WaitIdle();

		m_pDriver->SetRemoveLock(false);
		
		m_pTimer->Enable(false);

		m_pHandler->OnClose();

		m_fOpen = false;
		}
	}

void METHOD CUsbSerialFtdi::Send(BYTE bData)
{
	m_uTxCount = 0;
	
	m_bTxData[m_uTxCount++] = bData;

	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( m_uSendState == stateIdle ) {

		Hal_LowerIrql(uSave);

		for(;;) {
		
			if( m_pHandler->OnTxData(m_bTxData[m_uTxCount]) ) {

				if( ++m_uTxCount < sizeof(m_bTxData) ) {

					continue;
					}
				}
			
			break;
			}
			
		StartSend();

		return;
		}

	Hal_LowerIrql(uSave);
	}

void METHOD CUsbSerialFtdi::SetOutput(UINT uOutput, BOOL fOn)
{
	m_pDriver->ModemCtrl(modemRTS, fOn);
	}

BOOL METHOD CUsbSerialFtdi::GetInput(UINT uInput)
{
	return m_fCts;
	}

// IUsbHostFuncEvents

void METHOD CUsbSerialFtdi::OnData(void)
{
	if( m_fOpen ) {

		OnSend();

		OnRecv();
		}
	}

// IEventSink

void CUsbSerialFtdi::OnEvent(UINT uLine, UINT uParam)
{
	if( m_fOpen ) {

		m_pHandler->OnTimer();
		}
	}

// Implementation

bool CUsbSerialFtdi::InitUart(void)
{
	if( m_pDriver->SetRemoveLock(true) ) {

		m_pDriver->Reset(true, true);

		m_pDriver->SetFlowCtrl(0, 0, 0);

		if( m_Config.m_uFlags & flagFastRx ) {

			m_pDriver->SetLatTimer(8);
			}
		else {
			m_pDriver->SetLatTimer(16);
			}

		return true;
		}

	return false;
	}

bool CUsbSerialFtdi::InitBaudRate(void)
{
	return m_pDriver->SetBaudRate(m_Config.m_uBaudRate);
	}

bool CUsbSerialFtdi::InitFormat(void)
{
	return m_pDriver->SetData( m_Config.m_uDataBits, 
				   m_Config.m_uParity, 
				   m_Config.m_uStopBits, 
				   false
				   );
	}

bool CUsbSerialFtdi::InitPhysical(void)
{
	switch( m_Config.m_uPhysical ) {
		
		case physicalRS232:

			m_pSwitch->SetPhysical(0, false);

			m_pSwitch->SetFull(0, true);

			if( m_Config.m_uFlags & flagHonorCTS ) {

				m_pDriver->SetFlowCtrl(flowRTS, false, false);
				}

			return true;
		
		case physicalRS422Master:

			m_pSwitch->SetPhysical(0, true);

			m_pSwitch->SetFull(0, true);

			return true;

		case physicalRS422Slave:

			m_pSwitch->SetPhysical(0, true);

			m_pSwitch->SetFull(0, true);

			return true;

		case physicalRS485:

			m_pSwitch->SetPhysical(0, true);

			m_pSwitch->SetFull(0, false);

			return true;
		}

	return false;
	}

void CUsbSerialFtdi::OnRecv(void)
{
	for(;;) {
		
		if( m_uRecvState == stateRecv ) {

			StartRecv();

			return;
			}

		if( m_uRecvState == stateWait ) {

			m_uRxCount = m_pDriver->WaitRecv(0);

			if( m_uRxCount == NOTHING ) {

				return;
				}

			if( !m_fClosing ) {

				m_fCts = m_bRxData[0] & Bit(4);

				if( m_bRxData[1] & Bit(1) ) {

					AfxTrace("Rx Overflow\n");
					}

				if( m_uRxCount > 2 ) {

					for( UINT i = 2; i < m_uRxCount; i ++ ) {

						m_pHandler->OnRxData(m_bRxData[i]);
						}

					m_pHandler->OnRxDone();
					}

				m_uRecvState = stateRecv;

				continue;
				}

			m_uRecvState = stateIdle;

			return;
			}

		break;
		}
	}

void CUsbSerialFtdi::OnSend(void)
{
	if( m_uSendState == stateWait ) {

		if( m_pDriver->WaitSend(0) == NOTHING ) {

			return;
			}

		for(;;) {
		
			if( m_pHandler->OnTxData(m_bTxData[m_uTxCount]) ) {

				if( ++m_uTxCount < sizeof(m_bTxData) ) {

					continue;
					}
				}
			
			break;
			}

		if( !m_uTxCount ) {

			m_uSendState = stateIdle;

			m_pHandler->OnTxDone();
			}
		else {
			StartSend();
			}
		
		return;
		}
	}

void CUsbSerialFtdi::StartRecv(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	m_uRxCount = 0;

	if( m_pDriver->Recv(m_bRxData, sizeof(m_bRxData), true) != NOTHING ) {

		m_uRecvState = stateWait;
		}
	else {
		m_uRecvState = stateFail;
		}

	Hal_LowerIrql(uSave);
	}

void CUsbSerialFtdi::StartSend(void)
{
	UINT uSave = Hal_RaiseIrql(IRQL_MAXIMUM);

	if( m_pDriver->Send(m_bTxData, m_uTxCount, true) != NOTHING ) {

		m_uSendState = stateWait;
		}
	else {
		m_uSendState = stateFail;
		}

	m_uTxCount = 0;

	Hal_LowerIrql(uSave);
	}

void CUsbSerialFtdi::WaitIdle(void)
{
	while( m_uSendState == stateWait || m_uRecvState == stateWait ) {

		Sleep(10);
		}
	}

// End of File
