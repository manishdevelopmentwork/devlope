
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

/*
LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "ti500.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "ti500.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "ti500.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "ti500.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
*/

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CTI500v2MasterDeviceOptions
//

CTI500v2MasterDeviceOptionsUIList RCDATA
BEGIN
	"Block,Use Block Read/Write Operations,,CUIDropDown,No|Yes,"
	"Block operations allow better data transfer to occur.  Please note "
	"that the target device must support NITP word task codes 7F and 5A as  "
	"well as discrete task codes 6B and 59.  "
	"\0"
	
	"\0"
END

CTI500v2MasterDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Protocol Options,Block\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CTI500MasterTCPDeviceOptions
//

CTI500MasterTCPDeviceOptionsUIList RCDATA
BEGIN
	"IP,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the target device."
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|60000,"
	"Indicate the TCP port number on which this protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want this driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"Block,Use Block Read/Write Operations,,CUIDropDown,No|Yes,"
	"Block operations allow better data transfer to occur.  Please note "
	"that the target device must support NITP task codes 9D and 9E.  "
	"\0"

	"\0"
END

CTI500MasterTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,IP,Port\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2,Block\0"
	"\0"
END


// End of File
