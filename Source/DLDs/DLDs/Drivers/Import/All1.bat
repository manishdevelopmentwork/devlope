@echo off
call import.bat ABL5k
call import.bat ABLgxEip
call import.bat ABLgxMs
call import.bat ABUltra3
call import.bat AcrmgTCP
call import.bat ACTechSS
call import.bat ActssUDP
call import.bat Adam4000
call import.bat Adenus
call import.bat AirNet
call import.bat AlphaT
call import.bat AlsSnp
call import.bat AlsSrtp
call import.bat Animatic
call import.bat ApplMot
call import.bat BAC8023
call import.bat BAC8023s
call import.bat BACIP
call import.bat BACIPs
call import.bat BACMSTP
call import.bat BACMSTPs
call import.bat BalMint
call import.bat BanCam
call import.bat BannerMB
call import.bat Bbsapser
call import.bat Bbsaptcp
call import.bat BbsSerS
call import.bat BbsTCPS
call import.bat Beckplus
call import.bat BFD
call import.bat BhoffTCP
call import.bat BoschSIS
call import.bat C2Opc
call import.bat CATLink
call import.bat CATLink2
call import.bat CATLink4
call import.bat CATOld
call import.bat CATTest
call import.bat CiaSdoM
call import.bat COMLIMst
call import.bat COMLISlv
call import.bat Contrexr
call import.bat Contrext
call import.bat Cruisair
call import.bat CTC2xxx
call import.bat Cti2572
call import.bat CtiCampM
call import.bat CtiNitp
call import.bat CtiNitpE
call import.bat CTQuantm
call import.bat DevNetS
call import.bat Df1Meip
call import.bat Df1Menc
call import.bat Df1Ms
call import.bat Df1Mtcp
call import.bat Df1Ss
call import.bat Df1Stcp
call import.bat DH485
call import.bat DriAir
call import.bat DupMod
call import.bat Dvlt6000
call import.bat EatonELC
call import.bat EI3504
call import.bat EI3504Et
call import.bat EIepower
call import.bat EIM8tcp
call import.bat EIMini8
call import.bat Elmo
call import.bat EMCOFP93
call import.bat Emfxser
call import.bat EmRoc
call import.bat EmsnPTCP
call import.bat EmsonEP
call import.bat EmsonEPP
call import.bat EmsonER3
call import.bat EmsonMC
call import.bat EmsonSPP
call import.bat EmsonTCP
call import.bat EnetipS
call import.bat EnMbTcpM
call import.bat Euro590
call import.bat Euro590O
call import.bat Euro631
call import.bat Euro635
call import.bat Euro690
call import.bat EuroBSyn
call import.bat EzM
call import.bat EzTcpM
call import.bat Fam3M
call import.bat Fam3TcpM
call import.bat FatekPLC
call import.bat FatekUDP
call import.bat Festo
call import.bat FinsUdpM
call import.bat FlowM
call import.bat G80JkM
call import.bat G80TcpM
call import.bat Galilser
rem call import.bat GalilTCP
call import.bat GandLCE
call import.bat GEDNC2
call import.bat Gem80M
call import.bat Gem80S
call import.bat Generic
call import.bat HBMAED
call import.bat HBMFIT
call import.bat HitachiH
call import.bat HLink
call import.bat Honey620
call import.bat HoneyUDC
call import.bat IaiRobo
call import.bat IaiXsel
call import.bat IDDconM
call import.bat Idec3
call import.bat IfmAsi
call import.bat IMOK7
call import.bat IMOLoad
call import.bat IMOLoadg
call import.bat IMSMDrve
call import.bat IndraCLC
call import.bat Ircon
call import.bat Julabo
call import.bat KEbcUdpM
call import.bat KEBDIN2
call import.bat KEBSlave
call import.bat KEBTcpM
call import.bat KeltecPS
call import.bat KeyKV
call import.bat KingBus
call import.bat KollM600
call import.bat KoyoD
call import.bat KoyoK
call import.bat KoyoUdpM
call import.bat LaetusA
call import.bat Lecom2
call import.bat M2MSer
call import.bat M2MTCP
call import.bat MagMLAN
call import.bat MagmlTCP
call import.bat MasterK
call import.bat MatFP
call import.bat MatFP2
call import.bat MatFPDat
call import.bat MatFPTcp
call import.bat MbTcpM
call import.bat MbTcpS
call import.bat Melsec
call import.bat MELServo
call import.bat Mentor2
call import.bat MetaN2
call import.bat MetaSys
call import.bat MetaSys2
call import.bat MetaSys3
call import.bat MFx2TcpM
call import.bat MicrMSer
call import.bat MicrMTCP
call import.bat MicroDCI
call import.bat MitATcpM
call import.bat MitAUdpM
call import.bat MitFx
call import.bat MitQTcpM
call import.bat MLinkA
call import.bat MLinkB
call import.bat Modbus
call import.bat ModDevS
call import.bat ModDevSS
