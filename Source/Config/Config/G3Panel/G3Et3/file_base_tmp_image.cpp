
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "/base0/tmp/image"
//

// Constructor

CFileDataBaseTmpImage::CFileDataBaseTmpImage(void) : CFileData("/base0/tmp/image")
{
	m_uSize = 0;

	m_pData = NULL;
	}
 
CFileDataBaseTmpImage::CFileDataBaseTmpImage(CInitData &Data) : CFileData("/base0/tmp/image")
{
	Create(Data);
	}

// Destructor

CFileDataBaseTmpImage::~CFileDataBaseTmpImage(void)
{
	if( m_pData ) {

		delete [] m_pData;
		
		m_pData = NULL;

		m_uSize = 0;
		}
	}

// Operations

void CFileDataBaseTmpImage::Create(CInitData &Data)
{
	Create(Data.GetCount());		

	memcpy(m_pData, Data.GetPointer(), m_uSize);
	}

void CFileDataBaseTmpImage::Create(UINT uSize)
{
	AfxAssert(m_pData == NULL);

	m_uSize = uSize;

	m_pData = New BYTE [ m_uSize ];
	}

// End of File
