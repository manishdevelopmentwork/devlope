
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Data Object Construction

BOOL CPageEditorWnd::MakeDataObject(IDataObject * &pData, BOOL fRich, BOOL fFixup)
{
	CDataObject *pMake = New CDataObject;

	if( HasSelect() ) {

		if( fRich ) {

			if( HasLoneSelect() ) {

				CPrim *pPrim = GetLoneSelect();

				CStringArray Props;

				CUIGetSet().GetProps(Props, pPrim);

				CString Text;

				Text.Build(Props, '\r');

				pMake->AddText(m_cfSpec, Text);
				}
			}

		AddPrimsToDataObject(pMake, m_Pos, fRich);
		}

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CPageEditorWnd::AddPrimsToDataObject(CDataObject *pData, CPoint Pos, BOOL fRich)
{
	CTextStreamMemory Stream;

	if( Stream.OpenSave() ) {

		if( AddPrimsToStream(Stream, m_Pos, fRich) ) {

			pData->AddStream(m_cfData, Stream);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::AddPrimsToStream(ITextStream &Stream, CPoint Pos, BOOL fRich)
{
	CString F1   = m_pItem->GetDatabase()->GetUniqueSig();

	CString F2   = m_pItem->GetFixedName();

	CString F3   = CPrintf(L"%d", Pos.x);

	CString F4   = CPrintf(L"%d", Pos.y);

	CString Head = CPrintf(L"%s|%s|%s|%s\r\n", F1, F2, F3, F4);

	Stream.PutLine(Head);

	CTreeFile Tree;

	if( Tree.OpenSave(Stream) ) {

		AddPrimsToTreeFile(Tree, fRich);

		Tree.Close();

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::AddPrimsToTreeFile(CTreeFile &Tree, BOOL fRich)
{
	CPrimRefList Refs;

	for( UINT n = 0; n < m_SelList.GetCount(); n++ ) {

		INDEX   Index = m_SelList[n];

		CPrim * pItem = m_pWorkList->GetItem(Index);

		CString Class = pItem->GetClassName();

		Tree.PutObject(Class.Mid(1));

		pItem->PreCopy();

		pItem->GetRefs(Refs);

		if( !fRich ) {

			if( m_pWorkList->GetNext(Index) ) {
				
				if( Index != m_SelList[n+1] ) {

					CPrim * pBefore = m_pWorkList->GetItem(Index);

					CString Fixed   = pBefore->GetFixedName();

					Tree.PutValue(L"LIST-ORDER", Fixed);
					}
				}
			}

		pItem->Save(Tree);

		Tree.EndObject();
		}

	if( fRich ) {

		m_pSystem->SaveRefs(Tree, Refs);
		}
	}

// Data Object Testing

BOOL CPageEditorWnd::CanAcceptDataObject(IDataObject *pData, UINT &uType)
{
	if( CanAcceptPrims(pData, uType) ) {
		
		return TRUE;
		}

	if( CanAcceptOther(pData, uType) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanAcceptOther(IDataObject *pData, UINT &uType)
{
	CDatabase  * pDbase   = m_pItem->GetDatabase();

	CUISystem  * pSystem  = (CUISystem *) pDbase->GetSystemItem();

	CUIManager * pManager = pSystem->m_pUI;

	if( pManager->CanAcceptDataObject(pData) ) {

		uType = dropOther;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanAcceptPrims(IDataObject *pData, UINT &uType)
{
	FORMATETC Fmt = { m_cfData, NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			if( CanAcceptPrims(Stream, uType) ) {

				Stream.Close();

				ReleaseStgMedium(&Med);

				return TRUE;
				}
			}

		Stream.Close();

		ReleaseStgMedium(&Med);
		}

	return FALSE;
	}

BOOL CPageEditorWnd::CanAcceptPrims(ITextStream &Stream, UINT &uType)
{
	BOOL   fAccept = FALSE;

	CPoint Pos     = CPoint(0, 0);

	if( GetStreamInfo(Stream, uType, Pos) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			while( !Tree.IsEndOfData() ) {

				CString Name = Tree.GetName();

				if( !Name.IsEmpty() ) {

					if( Name.StartsWith(L"REF-") ) {

						break;
						}
					else {
						CLASS Class = AfxNamedClass(PCTXT(L"C" + Name));

						if( !Class->IsKindOf(AfxRuntimeClass(CPrim)) ) {

							fAccept = FALSE;

							break;
							}

						Tree.GetObject();

						while( !Tree.IsEndOfData() ) {

							Tree.GetName();
							}

						Tree.EndObject();

						fAccept = TRUE;
						}
					}
				}
			}
		}

	return fAccept;
	}

BOOL CPageEditorWnd::GetStreamInfo(ITextStream &Stream, UINT &uType, CPoint &Pos)
{
	TCHAR sText[256] = { 0 };

	if( Stream.GetLine(sText, elements(sText)) ) {

		CString Text = sText;

		if( Text.StartsWith(L"TreeFile") ) {

			Pos.x = 0;

			Pos.y = 0;

			uType = dropCreate;

			Stream.Rewind();

			return TRUE;
			}
		else {
			CStringArray List;

			Text.Tokenize(List, '|');

			if( List.GetCount() > 1 ) {

				Pos.x = watoi(List[2]);

				Pos.y = watoi(List[3]);

				if( List[0] == m_pItem->GetDatabase()->GetUniqueSig() ) { 

					if( List[1] == m_pItem->GetFixedName() ) {

						uType = dropSamePage;

						return TRUE;
						}

					uType = dropSameDatabase;

					return TRUE;
					}

				if( List[0] == L"CREATE" ) {

					uType = dropCreate;

					return TRUE;
					}

				uType = dropDifferent;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Data Object Acceptance

BOOL CPageEditorWnd::AcceptDataObject(IDataObject *pData, UINT uMode)
{
	if( AcceptPrims(pData, uMode) ) {

		CSize TheSize = m_AcceptRect.GetSize();

		CSize MaxSize = m_WorkRect.GetSize();

		if( TheSize.cx > MaxSize.cx || TheSize.cy > MaxSize.cy ) {

			if( MaxSize.cy > MaxSize.cx ) {

				TheSize.cy = MaxSize.cx * TheSize.cy / TheSize.cx;

				TheSize.cx = MaxSize.cx;
				}
			else {
				TheSize.cx = MaxSize.cy * TheSize.cx / TheSize.cy;

				TheSize.cy = MaxSize.cy;
				}

			CPoint Pos  = m_AcceptRect.GetTopLeft();

			CRect  Rect = CRect(Pos, TheSize);

			SizeAcceptList(m_AcceptRect, Rect);

			m_AcceptRect = Rect;
			}

		return TRUE;
		}

	if( AcceptOther(pData, uMode) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPageEditorWnd::AcceptOther(IDataObject *pData, UINT uMode)
{
	CDatabase  * pDbase   = m_pItem->GetDatabase();

	CUISystem  * pSystem  = (CUISystem *) pDbase->GetSystemItem();

	CUIManager * pManager = pSystem->m_pUI;

	m_pAcceptList = New CPrimList;

	m_pAcceptList->SetParent(m_pItem);

	m_pAcceptList->Init();

	pManager->AcceptDataObject(pData, m_pAcceptList, m_WorkRect.GetSize());

	CPoint Pos, Org;

	BOOL   fInit = TRUE;

	INDEX  Index = m_pAcceptList->GetHead();

	while( !m_pAcceptList->Failed(Index) ) {

		CPrim *pPrim = m_pAcceptList->GetItem(Index);

		CRect  Rect  = pPrim->GetRect();

		if( fInit ) {

			m_AcceptRect = Rect;

			Pos   = Rect.GetTopLeft();

			Org   = Pos;

			fInit = FALSE;
			}
		else {
			CSize Size = Rect.GetSize();
			
			CRect Move = CRect(Pos, Size);

			if( Move.bottom - Org.y > m_DispSize.cy ) {

				while( !m_pAcceptList->Failed(Index) ) {

					INDEX Kill = Index;

					m_pAcceptList->GetNext(Index);

					CPrim *pPrim = m_pAcceptList->RemoveItem(Kill);

					pPrim->Kill();

					delete pPrim;
					}

				break;
				}

			pPrim->SetRect(Move);

			m_AcceptRect |= Move;
			}

		Pos.y += Rect.cy();

		Pos.y += 2;

		m_pAcceptList->GetNext(Index);
		}

	m_AcceptPos  = m_AcceptRect.GetSize();

	m_AcceptPos /= 2;

	return TRUE;
	}

BOOL CPageEditorWnd::AcceptPrims(IDataObject *pData, UINT uMode)
{
	FORMATETC Fmt = { m_cfData, NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CTextStreamMemory Stream;

		if( Stream.OpenLoad(Med.hGlobal) ) {

			UINT uType;

			if( GetStreamInfo(Stream, uType, m_AcceptPos) ) {

				SetAdopter();

				if( AcceptPrims(Stream, uType, uMode) ) {

					ClearAdopter();

					ReleaseStgMedium(&Med);

					return TRUE;
					}

				ClearAdopter();
				}
			}

		ReleaseStgMedium(&Med);
		}

	return FALSE;
	}

BOOL CPageEditorWnd::AcceptPrims(ITextStream &Stream, UINT uType, UINT uMode)
{
	CTreeFile Tree;

	if( Tree.OpenLoad(Stream) ) {

		// LATER -- We block error as we don't want to give a message
		// if the drop of an item that contains broken references is
		// aborted. For items being dragged within widgets, we probably
		// ought to try and provide the right name and data servers to
		// the accept list so that they continue to be drawn correctly
		// when we are repositioning them...

		CDatabase *pDbase = m_pItem->GetDatabase();

		BOOL       fRefs  = (uType == dropDifferent || uType == dropCreate);

		pDbase->BlockErrors(TRUE);

		m_pAcceptList = New CPrimList;

		m_pAcceptList->SetParent(m_pItem);

		m_pAcceptList->Init();

		m_AcceptRect.Set(0, 0, 0, 0);

		CPrimRefMap Refs;

		while( !Tree.IsEndOfData() ) {

			CString Name = Tree.GetName();

			if( !Name.IsEmpty() ) {

				if( !m_pSystem->ReadRef(Tree, Name, Refs, fRefs, uMode == pasteLegacy) ) {

					CLASS  Class = AfxNamedClass(PCTXT(L"C" + Name));

					CPrim *pItem = AfxNewObject(CPrim, Class);

					Tree.GetObject();

					pItem->SetParent(m_pAcceptList);

					if( uMode == pasteDrop && Tree.IsName(L"LIST-ORDER") ) {

						Tree.GetName();

						CString Fixed = Tree.GetValueAsString();

						m_AcceptOrder.Append(Fixed);
						}
					else
						m_AcceptOrder.Append(L"");

					pItem->Load(Tree);

					m_pAcceptList->AppendItem(pItem);

					if( uMode == pasteDrop ) {

						pItem->PostLoad();
						}
					else {
						pItem->PostPaste();

						pItem->PostLoad();
						}

					m_AcceptRect |= pItem->GetNormRect();

					Tree.EndObject();
					}
				}
			}

		if( Refs.GetCount() ) {

			// LATER -- We could keep the reference map as a member and
			// then delete any created images or fonts if the accepted
			// data isn't actually put into the database. This is not
			// a big deal for now as image files are added, not image
			// views, but how is this going to work for fonts?

			FixAcceptList(Refs);
			}

		pDbase->BlockErrors(FALSE);

		return TRUE;
		}

	return FALSE;
	}

// Accepted Data Operations

void CPageEditorWnd::AcceptSelection(void)
{
	IDataObject *pData = NULL;

	MakeDataObject(pData, FALSE, FALSE);

	AcceptPrims(pData, FALSE);

	pData->Release();
	}

void CPageEditorWnd::FixAcceptList(CPrimRefMap &Refs)
{
	for( UINT p = 0; p < 2; p++ ) {

		INDEX n = m_pAcceptList->GetHead();

		while( !m_pAcceptList->Failed(n) ) {

			CPrim *pPrim = m_pAcceptList->GetItem(n);

			m_pSystem->FixRefs(pPrim, Refs, p);

			m_pAcceptList->GetNext(n);
			}
		}
	}

void CPageEditorWnd::MoveAcceptList(CRect OldRect, CRect NewRect)
{
	INDEX n = m_pAcceptList->GetHead();

	while( !m_pAcceptList->Failed(n) ) {

		CPrim *pPrim = m_pAcceptList->GetItem(n);

		pPrim->SetRect(OldRect, NewRect);

		m_pAcceptList->GetNext(n);
		}
	}

void CPageEditorWnd::SizeAcceptList(CRect OldRect, CRect NewRect)
{
	INDEX n = m_pAcceptList->GetHead();

	if( m_pAcceptList->GetItemCount() == 1 ) {

		CPrim *pPrim = m_pAcceptList->GetItem(n);

		if( pPrim->IsLine() ) {

			pPrim->SetRect(NewRect);

			return;
			}
		}

	while( !m_pAcceptList->Failed(n) ) {

		CPrim *pPrim = m_pAcceptList->GetItem(n);

		pPrim->SetRect(OldRect, NewRect);

		m_pAcceptList->GetNext(n);
		}
	}

void CPageEditorWnd::CopyAcceptList(BOOL fOrder)
{
	ClearSelect(FALSE);

	INDEX Before = NULL;

	UINT  uCount = m_pAcceptList->GetItemCount();

	while( uCount-- ) {

		INDEX  Index = m_pAcceptList->GetTail();

		CPrim *pPrim = m_pAcceptList->RemoveItem(Index);

		CRect  Rect  = pPrim->GetRect();

		CRect  Move  = Rect;

		if( ClipMoveRect(Move) ) {

			pPrim->SetRect(Rect, Move);
			}

		if( fOrder ) {

			CString Fixed = m_AcceptOrder[uCount];

			if( !Fixed.IsEmpty() ) {

				INDEX Find = m_pWorkList->FindFixedName(Fixed);

				if( Find ) {

					Before = Find;
					}
				}
			}		
	
		Before = m_pWorkList->InsertItem(pPrim, Before);

		AddSelect(Before, TRUE);
		}

	AddSelect(NULL, FALSE);
	}

void CPageEditorWnd::FreeAcceptList(void)
{
	m_pAcceptList->Kill();

	delete m_pAcceptList;

	m_pAcceptList = NULL;

	m_AcceptOrder.Empty();
	}

// Adopter Control

void CPageEditorWnd::SetAdopter(void)
{
	if( m_pWorkSet ) {

		if( m_pWorkSet->IsWidget() ) {

			CPrim::m_pAdopter = (CPrimWidget *) m_pWorkSet;
			}
		}
	}

void CPageEditorWnd::ClearAdopter(void)
{
	CPrim::m_pAdopter = NULL;
	}

// End of File
