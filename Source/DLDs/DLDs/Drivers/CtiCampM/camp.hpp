//////////////////////////////////////////////////////////////////////////
//
// CTI CAMP Master Base Driver
//

class CCampMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CCampMasterDriver(void);

		// Destructor
		~CCampMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Context
		struct CBaseCtx
		{	
			UINT m_Error;
			};

	protected:
		// Data Members
		CBaseCtx * m_pBase;

		// Data Members
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		LPCTXT	   m_pHex;
		WORD	   m_wBCC;
		BYTE	   m_ID;
		
			
		// Implementation
				
		void Begin(void);
		void AddType(BOOL fWrite);
		void AddErrChar(void);
		void AddID(void);
		void AddClass(UINT uTable, UINT uExtra);
		void AddOffset(UINT uOffset);
		void AddCount(UINT uCount);
		void End(void);
		void AddByte(BYTE bByte);
		void AddAsciiChar(BYTE bByte);
		void AddAsciiByte(BYTE bByte);
		void AddAsciiWord(WORD wWord);
		void AddAsciiLong(DWORD dwLong);
		BYTE FromAscii(BYTE bByte);
		void BuffFromAscii(void);
		BOOL CheckFrame(void);
		UINT GetRecvCount(void);
		void GetWords(PDWORD pData, UINT uCount, UINT uData);
		void GetLongs(PDWORD pData, UINT uCount, UINT uData);

		// Helpers
		BOOL  IsLong(UINT uType);
		BOOL  IsReadOnly(UINT uTable);
		BOOL  IsError(UINT uTable);
		DWORD GetData(PBYTE pByte, UINT uCount);
		DWORD GetError(AREF Addr);
		UINT  GetOffset(UINT uOffset, UINT uTable);
		UINT  GetCount(UINT uCount, UINT uTable, UINT uType);
		UINT  FindDataOffset(UINT uOffset, UINT uTable);
		BOOL  NeedRead(UINT uTable);

		// Transport Layer
		virtual BOOL Transact(void);
	
	};

// End of File
