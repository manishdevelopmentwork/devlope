
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementString_HPP

#define INCLUDE_DevConElementString_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementEditBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration String UI Element
//

class CDevConElementString : public CDevConElementEditBox
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementString(void);

	// Destructor
	~CDevConElementString(void);

	// Operations
	void    CreateControls(CWnd &Wnd, UINT &id);
	void    ParseConfig(CJsonData *pSchema, CJsonData *pField);
	CString FormatData(CString const &Data);

protected:
	// Data Members
	CString m_Type;
	CString m_Default;

	// Overridables
	BOOL OnCheckData(CString &Data);
};

// End of File

#endif
