
#include "intern.hpp"

#include "bcode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Code Generator
//

// Constructor

CCodeGenerator::CCodeGenerator(CParseTree &Tree) : m_Tree(Tree)
{
	}

// Preparation

void CCodeGenerator::Prepare(UINT uDepth, UINT uLocal, CCompileIn const &In)
{
	m_Code.Empty();

	for( UINT n = 0; n < In.m_uParam; n++ ) {

		if( In.m_pParam[n].m_Type == typeString ) {

			m_LocString.Append(BYTE(n));
			}
		}

	EmitByte(0);

	EmitByte(BYTE(2 + uDepth));

	EmitByte(BYTE(uLocal));

	EmitByte(BYTE(In.m_uParam));

	m_fOptim = (In.m_Optim >= 2);

	m_fDebug = (In.m_Debug != 0);

	m_wDebug = WORD(In.m_Debug);

	if( m_fDebug ) {

		InitDebugData(uLocal, In);
		}

	m_uInit  = m_Code.GetCount();

	m_pLoop  = NULL;

	m_uPos   = m_Tree.GetCount() - 1;

	m_uLabel = 0;

	m_uExit  = 0;
	}

// Operations

BOOL CCodeGenerator::GenerateCode(CCompileOut &Out, BOOL fFunc)
{
	AfxAssert(m_Code.GetCount() == 4);

	m_pError = &Out.m_Error;

	try {
		BYTE t = BYTE(Out.m_Type.m_Type);

		m_Type = Out.m_Type;

		if( m_fDebug ) {

			EmitByte(bcDebugInfo);

			EmitByte(0);

			EmitByte(0);
			}

		if( fFunc ) {
	
			if( m_Type.m_Flags & flagDispatch ) {

				t = BYTE(t | 0x20);
				}

			m_Code.SetAt(0, t);

			WalkStatement();

			ExitFunction();
			}
		else {
			if( m_Type.m_Flags & flagConstant ) {

				t = BYTE(t | 0x40);
				}

			if( m_Type.m_Flags & (flagWritable | flagArray) ) {

				t = BYTE(t | 0x80);

				m_Code.SetAt(0, t);

				WalkExpression(typeLValue);
				}
			else {
				m_Code.SetAt(0, t);

				WalkExpression(m_Type.m_Type);
				}
			}

		EmitCode(bcNull);

		if( m_fDebug ) {

			FixJumps();

			AddDebugData();
			}
		else {
			if( m_fOptim ) {

				Optimize();
				}

			FixJumps();
			}

		return TRUE;
		}

	catch(CUserException const &) {
	
		m_Code.Empty();

		return FALSE;
		}
	}

// Byte Code Access

CByteArray const & CCodeGenerator::GetCode(void) const
{
	return m_Code;
	}

// Statement Walking

void CCodeGenerator::WalkStatement(void)
{
	CParseNode const &Node = m_Tree[m_uPos];

	if( Node.m_Group == groupKeyword ) {

		switch( Node.m_Code ) {

			case tokenVoid:
			
				WalkExprStatement();
				
				return;

			case tokenIf:
				
				WalkIfStatement();
				
				return;

			case tokenWhile:
				
				WalkWhileStatement();
				
				return;

			case tokenDo:
				
				WalkDoStatement();
				
				return;

			case tokenFor:
				
				WalkForStatement();
				
				return;

			case tokenBreak:
				
				WalkBreakStatement();
				
				return;

			case tokenContinue:
				
				WalkContinueStatement();
				
				return;

			case tokenSwitch:
				
				WalkSwitchStatement();
				
				return;

			case tokenReturn:
				
				WalkReturnStatement();
				
				return;

			case tokenInt:
				
				WalkAllocLocal(typeInteger);
				
				return;

			case tokenFloat:
				
				WalkAllocLocal(typeReal);
				
				return;

			case tokenString:
				
				WalkAllocLocal(typeString);
				
				return;
			}
		}

	if( Node.m_Group == groupSeparator ) {

		if( Node.m_Code == tokenBraceOpen ) {

			WalkCompStatement();

			return;
			}
		}

	if( Node.m_Group == groupOperator ) {

		if( Node.m_Code == tokenNull ) {

			WalkNullStatement();

			return;
			}
		}

	if( Node.m_Group == groupEndOfText ) {

		WalkNullStatement();

		return;
		}

	m_pError->Set(CFormat(IDS_CODE_BAD_SNODE, Node.Describe()));

	ThrowError();
	}

void CCodeGenerator::WalkNullStatement(void)
{
	m_uPos--;
	}

void CCodeGenerator::WalkExprStatement(void)
{
	m_uPos--;

	WalkExpression(typeVoid);
	}

void CCodeGenerator::WalkCompStatement(void)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	for( UINT n = 0; n < Node.m_uOrder; n++ ) {

		WalkStatement();
		}
	}

void CCodeGenerator::WalkIfStatement(void)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	if( Node.m_uOrder == 2 ) {

		WalkExpression(typeInteger);
		
		if( !LoopCheckBranch() ) {

			UINT x1 = EmitFwdBranch(TRUE);

			WalkStatement();
			
			EmitLabel(x1);
			}
		}
	else {
		WalkExpression(typeInteger);
		
		if( !LoopCheckBranch() ) {

			UINT x1 = EmitFwdBranch(TRUE);

			WalkStatement();

			UINT x2 = EmitFwdJump();
		
			EmitLabel(x1);

			WalkStatement();

			EmitLabel(x2);
			}
		else
			WalkStatement();
		}
	}

void CCodeGenerator::WalkWhileStatement(void)
{
	m_uPos--;

	UINT x1 = EmitLabel();

	PVOID p = LoopCreate(0, x1);

	WalkExpression(typeInteger);

	UINT x2 = EmitFwdBranch(TRUE);

	WalkStatement();

	EmitJump(x1);

	EmitLabel(x2);

	LoopFixup(loopBreak);

	LoopDelete(p);
	}
		
void CCodeGenerator::WalkDoStatement(void)
{
	m_uPos--;

	UINT x1 = EmitLabel();

	PVOID p = LoopCreate(0, 0);

	WalkStatement();

	LoopFixup(loopContinue);

	WalkExpression(typeInteger);

	EmitBranch(x1, FALSE);

	LoopFixup(loopBreak);

	LoopDelete(p);
	}

void CCodeGenerator::WalkForStatement(void)
{
	m_uPos--;

	WalkExpression(typeVoid);

	UINT x1 = EmitLabel();

	UINT x2 = 0;

	PVOID p = LoopCreate(0, 0);

	if( !IsNullExpression() ) {

		WalkExpression(typeInteger);

		x2 = EmitFwdBranch(TRUE);
		}
	else {
		SkipNode();

		x2 = 0;
		}

	UINT p1 = m_uPos;

	SkipNode();

	WalkStatement();

	UINT p2 = m_uPos;

	LoopFixup(loopContinue);

	m_uPos = p1;

	WalkExpression(typeVoid);

	m_uPos = p2;

	EmitJump(x1);

	EmitLabel(x2);

	LoopFixup(loopBreak);

	LoopDelete(p);
	}
		
void CCodeGenerator::WalkBreakStatement(void)
{
	m_uPos--;

	LoopEmit(loopBreak, FALSE);
	}

void CCodeGenerator::WalkContinueStatement(void)
{
	m_uPos--;

	LoopEmit(loopContinue, FALSE);
	}

void CCodeGenerator::WalkSwitchStatement(void)
{
	// REV3 -- Implement a lookup opcode to enable better
	// performance when we're working with large statements.

	CParseNode const &Node = m_Tree[m_uPos--];

	PVOID p = LoopCreate(0, 0);

	UINT  n = Node.m_uOrder - 1;

	UINT  x = 0;

	UINT  y = 0;

	WalkExpression(typeInteger);

	while( n-- ) {

		CParseNode const &Node = m_Tree[m_uPos];

		if( Node.m_Group == groupKeyword ) {

			if( Node.m_Code == tokenCase ) {

				m_uPos--;

				EmitLabel(x);

				if( Node.m_uOrder > 1 ) {

					int z = 0;

					for( UINT n = 0; n < Node.m_uOrder; n++ ) {

						EmitCode(bcDupTop);

						WalkConstant(typeInteger);

						EmitCode(bcEqual);

						if( z ) {

							EmitBranch(z, FALSE);

							continue;
							}

						z = EmitFwdBranch(FALSE);
						}

					x = EmitFwdJump();

					EmitLabel(z);
					}
				else {
					EmitCode(bcDupTop);

					WalkConstant(typeInteger);

					EmitCode(bcEqual);

					x = EmitFwdBranch(TRUE);
					}

				EmitLabel(y);

				continue;
				}

			if( Node.m_Code == tokenDefault ) {

				m_uPos--;

				EmitLabel(x);

				x = 0;

				continue;
				}

			if( Node.m_Code == tokenVoid ) {

				WalkExprStatement();

				y = EmitFwdJump();

				continue;
				}
			}

		if( Node.m_Group == groupSeparator ) {

			if( Node.m_Code == tokenBraceOpen ) {

				WalkCompStatement();

				y = EmitFwdJump();

				continue;
				}
			}

		WalkStatement();

		y = EmitFwdJump();
		}
	
	EmitLabel(x);

	EmitLabel(y);

	LoopFixup(loopBreak);

	EmitCode(bcPop);

	LoopDelete(p);
	}

void CCodeGenerator::WalkReturnStatement(void)
{
	m_uPos--;

	if( m_Type.m_Type ) {

		WalkExpression(m_Type.m_Type);
		}

	if( m_LocString.GetCount() ) {

		if( !m_uExit ) {

			m_uExit = EmitFwdJump();
			}
		else
			EmitJump(m_uExit);

		return;
		}

	EmitCode(bcReturn);
	}

void CCodeGenerator::WalkAllocLocal(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	if( Node.m_uOrder > 1 ) {

		for( UINT n = 0; n < Node.m_uOrder; n++ ) {

			WalkAllocLocal(Type);
			}
		}
	else {
		BYTE bSlot = BYTE(Node.m_Type - typeObject);

		if( Node.m_uOrder ) {

			WalkExpression(Type);
			}
		else
			EmitNullValue(Type);

		if( m_fDebug ) {

			CFuncParam &Param = (CFuncParam &) m_DebugParam[bSlot];

			Param.m_Name = Node.m_Const.GetStringValue();

			Param.m_Type = Type;
			}

		if( Type == typeString ) {

			EmitCode(bcPutLocal);

			EmitCode(bSlot);

			m_LocString.Append(bSlot);
			}
		else {
			if( bSlot < 6 ) {

				BYTE bCode = BYTE(bcPutLocal0 + bSlot);

				EmitCode(bCode);
				}
			else {
				EmitCode(bcPutLocal);

				EmitCode(bSlot);
				}
			}
		}
	}

void CCodeGenerator::FreeLocals(UINT uStart)
{
	UINT uCount = m_LocString.GetCount();

	for( UINT n = uStart; n < uCount; n++ ) {

		EmitCode(bcGetLocal);

		EmitByte(m_LocString[n]);

		EmitCode(bcPopString);
		}

	m_LocString.Remove(uStart, uCount - uStart);
	}

void CCodeGenerator::ExitFunction(void)
{
	EmitNullValue(m_Type.m_Type);

	EmitLabel(m_uExit);

	FreeLocals(0);
	}

// Expression Walking

void CCodeGenerator::WalkExpression(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos];

	if( Node.m_Group == groupConstant ) {

		WalkConstant(Type);

		return;
		}

	if( Node.m_Group == groupIdentifier ) {

		switch( Node.m_Code ) {

			case usageFunction:
				
				WalkFunction(Type);
				
				return;

			case usageVariable:
				
				WalkVariable(Type);
				
				return;

			case usageDirectRef:
				
				WalkFullRef(Type);
				
				return;

			case usageFullRef:
				
				WalkFullRef(Type);
				
				return;

			case usageLocal:
				
				WalkVariable(Type);
				
				return;

			case usageProperty:

				WalkProperty(Type);

				return;
			}
		}

	if( Node.m_Group == groupKeyword ) {

		switch( Node.m_Code ) {

			case tokenInt:
				
				WalkCast(Type, typeInteger);
				
				return;

			case tokenFloat:
				
				WalkCast(Type, typeReal);
				
				return;
			
			case tokenVoid:
				
				WalkCast(Type, typeVoid);
				
				return;
			}
		}

	if( Node.m_Group == groupSeparator ) {

		switch( Node.m_Code ) {

			case tokenIndexOpen:
				
				WalkIndexArray(Type, FALSE);
				
				return;
			}
		}

	if( Node.m_Group == groupOperator ) {

		switch( Node.m_Code ) {

			case tokenNull:
				
				WalkNullExpression(Type);
				
				return;

			case tokenIndexExtended:
				
				WalkIndexArray(Type, TRUE);
				
				return;

			case tokenIndexString:
				
				WalkIndexString(Type);
				
				return;

			case tokenBitSelect:
				
				WalkBitSelect(Type);
				
				return;

			case tokenBitSelectLong:
				
				WalkBitSelect(Type);
				
				return;

			case tokenGetProperty:

				WalkGetProperty(Type);

				return;

			case tokenLogicalTest:
				
				WalkLogicalTest(Type);
				
				return;

			case tokenLogicalAnd:
				
				WalkLogical(Type, TRUE);
				
				return;

			case tokenLogicalOr:
				
				WalkLogical(Type, FALSE);
				
				return;

			case tokenComma:
				
				WalkSequence(Type);
				
				return;

			case tokenQuestion:
				
				WalkTernary(Type);
				
				return;

			case tokenCondition:
				
				WalkCondition(Type);
				
				return;
			}

		if( Node.IsSimpleAssign() ) {

			WalkSimpleAssign(Type);

			return;
			}

		if( Node.IsComplexAssign() ) {

			WalkComplexAssign(Type);

			return;
			}

		if( Node.IsPrefixOperator() ) {

			WalkPrefix(Type);

			return;
			}

		if( Node.IsPostfixOperator() ) {

			WalkPostfix(Type);

			return;
			}

		WalkOperator(Type);

		return;
		}

	m_pError->Set(CFormat(IDS_CODE_BAD_ENODE, Node.Describe()));

	ThrowError();
	}

void CCodeGenerator::WalkNullExpression(UINT Type)
{
	m_uPos--;

	if( Type != typeVoid ) {

		EmitByte(1);

		EmitConversion(typeLogical, Type);
		}
	}

void CCodeGenerator::WalkFunction(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	UINT  uArgs = Node.m_uOrder;

	PBYTE bArgs = PBYTE(alloca(uArgs));

	for( UINT n = 0; n < uArgs; n++ ) {

		CParseNode const &Arg = m_Tree[m_uPos--];

		WalkExpression(Arg.m_Type);

		bArgs[n] = BYTE(Arg.m_Type);
		}

	if( m_wDebug & 8 ) {

		UINT uMask = 0x18000;

		UINT uData = 0x10000;

		if( m_wDebug & 16 ) {

			uMask &= ~0x10000;
			uData &= ~0x10000;
			}

		if( m_wDebug & 32 ) {

			uMask &= ~0x08000;
			uData &= ~0x08000;
			}

		if( (Node.m_Const.GetIntegerValue() & uMask) == uData) {

			EmitCode(bcDebugFunc);

			EmitCode(BYTE(uArgs));

			for( UINT n = 0; n < uArgs; n++ ) {

				EmitByte(bArgs[uArgs - 1 - n]);
				}
			}
		else {
			EmitCode(bcFunction);

			EmitCode(BYTE(uArgs));
			}
		}
	else {
		EmitCode(bcFunction);

		EmitCode(BYTE(uArgs));
		}

	EmitName(Node);

	if( Node.m_Type == typeVoid ) {

		EmitConversion(typeInteger, Type);

		return;
		}

	EmitConversion(Node.m_Type, Type);
	}

void CCodeGenerator::WalkVariable(UINT Type)
{
	if( Type == typeLValue ) {

		CParseNode const &Node = m_Tree[m_uPos--];

		EmitLoadAddress(Node);
		}
	else {
		CParseNode const &Node = m_Tree[m_uPos--];

		if( Type != typeVoid ) {

			EmitLoadValue(Node);

			EmitConversion(Node.m_Type, Type);
			}
		}
	}

void CCodeGenerator::WalkFullRef(UINT Type)
{
	if( Type == typeLValue ) {

		CParseNode const &Node = m_Tree[m_uPos--];
		
		EmitConstant(Node.m_Const, typeInteger);
		}
	else {
		CParseNode const &Node = m_Tree[m_uPos--];

		if( Type != typeVoid ) {
			
			EmitConstant(Node.m_Const, typeInteger);

			EmitLoadIndirect(Node.m_Type);

			EmitConversion(Node.m_Type, Type);
			}
		}
	}

void CCodeGenerator::WalkProperty(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	if( Type != typeVoid ) {
		
		EmitConstant(Node.m_Const, Type);
		}
	}

void CCodeGenerator::WalkConstant(UINT Type)
{
	if( Type != typeVoid ) {

		CParseNode const &Node = m_Tree[m_uPos--];

		EmitConstant(Node.m_Const, Type);
		}
	}

void CCodeGenerator::WalkCast(UINT Type, UINT Cast)
{
	m_uPos--;

	if( Type != typeVoid ) {

		WalkExpression(Cast);

		EmitConversion(Cast, Type);
		}
	else
		WalkExpression(typeVoid);
	}

void CCodeGenerator::WalkSimpleAssign(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	CParseNode const &Dest = m_Tree[m_uPos];

	if( Dest.IsCode(tokenBitSelectLong) ) {

		CParseNode const &Dest = m_Tree[--m_uPos];

		if( !CanSaveValue(Dest) ) {

			WalkExpression(typeLValue);

			EmitDupTop(typeInteger);

			EmitLoadIndirect(typeInteger);

			WalkExpression(typeInteger);

			WalkExpression(typeInteger);

			EmitCode(bcBitChange);

			EmitDup3rd(Type);

			EmitSaveIndirect(typeInteger);
			}
		else {
			WalkExpression(typeInteger);

			WalkExpression(typeInteger);

			WalkExpression(typeInteger);

			EmitCode(bcBitChange);

			EmitDupTop(Type);

			EmitSaveValue(Dest);
			}
		}
	else {
		if( !CanSaveValue(Dest) ) {

			WalkExpression(typeLValue);

			WalkExpression(Node.m_Type);

			EmitDup3rd(Type);

			EmitSaveIndirect(Node.m_Type);
			}
		else {
			m_uPos--;

			WalkExpression(Node.m_Type);

			EmitDupTop(Type);

			EmitSaveValue(Dest);
			}
		}

	EmitValueConversion(Node.m_Type, Type);
	}

void CCodeGenerator::WalkComplexAssign(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	CParseNode const &Dest = m_Tree[m_uPos];

	if( !CanSaveValue(Dest) ) {

		UINT Code = GetEquivalent(Node.m_Code);

		WalkExpression(typeLValue);

		EmitCode(bcDupTop);

		EmitLoadIndirect(Node.m_Type);

		WalkExpression(GetRightType(Node));

		EmitOperator(Code, Node.m_Type);

		EmitDup3rd(Type);

		EmitSaveIndirect(Node.m_Type);
		}
	else {
		UINT Code = GetEquivalent(Node.m_Code);

		m_uPos--;

		EmitLoadValue(Dest);

		WalkExpression(GetRightType(Node));

		EmitOperator(Code, Node.m_Type);

		EmitDupTop(Type);

		EmitSaveValue(Dest);
		}

	EmitValueConversion(Node.m_Type, Type);
	}

void CCodeGenerator::WalkPrefix(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	CParseNode const &Dest = m_Tree[m_uPos];

	if( !CanSaveValue(Dest) ) {

		WalkExpression(typeLValue);

		EmitCode(bcDupTop);

		EmitLoadIndirect(Node.m_Type);

		EmitOperator(Node.m_Code, Node.m_Type);

		EmitDup3rd(Type);

		EmitSaveIndirect(Node.m_Type);
		}
	else {
		WalkExpression(Node.m_Type);

		EmitOperator(Node.m_Code, Node.m_Type);

		EmitDupTop(Type);

		EmitSaveValue(Dest);
		}

	EmitValueConversion(Node.m_Type, Type);
	}

void CCodeGenerator::WalkPostfix(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	CParseNode const &Dest = m_Tree[m_uPos];

	if( !CanSaveValue(Dest) ) {

		WalkExpression(typeLValue);

		EmitCode(bcDupTop);

		EmitLoadIndirect(Node.m_Type);

		EmitDup3rd(Type);

		EmitOperator(Node.m_Code, Node.m_Type);

		EmitSaveIndirect(Node.m_Type);
		}
	else {
		WalkExpression(Node.m_Type);

		EmitDupTop(Type);

		EmitOperator(Node.m_Code, Node.m_Type);

		EmitSaveValue(Dest);
		}

	EmitValueConversion(Node.m_Type, Type);
	}

void CCodeGenerator::WalkIndexArray(UINT Type, BOOL fExtend)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	BYTE             bCode = BYTE(fExtend ? bcIndexExtend : bcIndexArray);

	if( Type == typeLValue ) {

		WalkExpression(typeLValue);

		WalkExpression(typeInteger);

		EmitCode(bCode);
		}
	else {
		if( Type != typeVoid ) {

			WalkExpression(typeLValue);

			WalkExpression(typeInteger);

			EmitCode(bCode);

			EmitLoadIndirect(Node.m_Type);

			EmitConversion(Node.m_Type, Type);
			}
		else {
			SkipNode();

			WalkExpression(typeVoid);
			}
		}
	}

void CCodeGenerator::WalkIndexString(UINT Type)
{
	m_uPos--;

	if( Type != typeVoid ) {

		WalkExpression(typeString);

		WalkExpression(typeInteger);

		EmitCode(bcIndexString);
		}
	else {
		WalkExpression(typeVoid);

		WalkExpression(typeVoid);
		}
	}

void CCodeGenerator::WalkBitSelect(UINT Type)
{
	m_uPos--;

	if( Type == typeLValue ) {

		WalkExpression(typeLValue);

		WalkExpression(typeInteger);

		EmitCode(bcIndexBit);
		}
	else {
		if( Type != typeVoid ) {

			CParseNode const &Test = m_Tree[m_uPos];

			if( Test.m_Group == groupConstant ) {

				CheckBit(Test.m_Const.ForceInteger());
				}

			WalkExpression(typeInteger);

			WalkExpression(typeInteger);

			EmitCode(bcBitSelect);

			EmitConversion(typeLogical, Type);
			}
		else {
			WalkExpression(typeVoid);

			WalkExpression(typeVoid);
			}
		}
	}

void CCodeGenerator::WalkGetProperty(UINT Type)
{
	m_uPos--;

	if( Type != typeVoid ) {

		WalkExpression(typeLValue);

		WalkExpression(typeInteger);

		EmitLoadProperty(Type);
		}
	else {
		WalkExpression(typeVoid);

		WalkExpression(typeVoid);
		}
	}

void CCodeGenerator::WalkLogicalTest(UINT Type)
{
	m_uPos--;

	WalkExpression(typeLogical);

	EmitConversion(typeLogical, Type);
	}

void CCodeGenerator::WalkLogical(UINT Type, BOOL fTest)
{
	m_uPos--;

	WalkExpression(typeLogical);

	EmitCode(bcDupTop);

	UINT x1 = EmitFwdBranch(fTest);

	EmitCode(bcPop);

	WalkExpression(typeLogical);

	EmitLabel(x1);

	EmitConversion(typeLogical, Type);
	}

void CCodeGenerator::WalkSequence(UINT Type)
{
	m_uPos--;

	WalkExpression(typeVoid);

	WalkExpression(Type);
	}

void CCodeGenerator::WalkCondition(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	WalkExpression(typeInteger);

	EmitDupTop(typeInteger);

	UINT x = EmitFwdBranch(TRUE);

	WalkExpression(typeVoid);

	EmitConstant(1, Node.m_Type);

	EmitLabel(x);
	}

void CCodeGenerator::WalkTernary(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	WalkExpression(typeInteger);

	UINT x1 = EmitFwdBranch(FALSE);

	UINT p1 = m_uPos;

	SkipNode();

	WalkExpression(Node.m_Type);

	UINT x2 = EmitFwdJump();

	UINT p2 = m_uPos;

	EmitLabel(x1);

	m_uPos = p1;

	WalkExpression(Node.m_Type);

	m_uPos = p2;

	EmitLabel(x2);

	EmitConversion(Node.m_Type, Type);
	}

void CCodeGenerator::WalkOperator(UINT Type)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	if( Type != typeVoid ) {

		for( UINT n = 0; n < Node.m_uOrder; n++ ) {

			WalkExpression(Node.m_Type);
			}

		if( Node.IsLogicalOperator() ) {

			EmitOperator(Node.m_Code, Node.m_Type);

			EmitConversion(typeLogical, Type);
			}
		else {
			EmitOperator(Node.m_Code, Node.m_Type);

			EmitConversion(Node.m_Type, Type);
			}
		}
	else {
		for( UINT n = 0; n < Node.m_uOrder; n++ ) {
		
			WalkExpression(typeVoid);
			}
		}
	}

// Code Generation

void CCodeGenerator::EmitValueConversion(UINT From, UINT Type)
{
	if( Type != typeVoid ) {

		EmitConversion(From, Type);
		}
	}

void CCodeGenerator::EmitNullValue(UINT Type)
{
	switch( Type ) {

		case typeInteger:

			EmitConstant(C3INT(0), typeInteger);
			
			break;

		case typeReal:

			EmitConstant(C3REAL(0), typeReal);
			
			break;

		case typeString:

			EmitConstant(L"", typeString);
			
			break;
		}
	}

void CCodeGenerator::EmitConstant(CLexConst const &Const, UINT Type)
{
	if( Type == typeLogical ) {

		EmitBool(Const.ForceLogical());

		return;
		}

	if( Const.IsType(typeInteger) ) {

		C3INT Value = Const.GetIntegerValue();

		if( HIWORD(Value) ) {

			if( Type == typeReal ) {

				if( (Value & 0xFFFFFF00) == 0xFFFFFF00 ) {

					EmitConstant(-Value, typeReal);

					EmitOperator(tokenUnaryMinus, typeReal);
					}
				else {
					C3REAL Real = C3REAL(Value);

					EmitConstant(Real, Type);
					}
				}
			else {
				if( (Value & 0xFFFF0000) == 0xFFFF0000 ) {

					EmitConstant(-Value, typeInteger);

					EmitOperator(tokenUnaryMinus, typeInteger);
					}
				else {
					EmitCode(bcPushInteger4);

					EmitLong(Value);
					}
				}
			}
		else {
			if( HIBYTE(Value) ) {

				EmitCode(bcPushInteger2);

				EmitWord(LOWORD(Value));
				}
			else {
				if( Value < bcNull ) {

					EmitByte(LOBYTE(Value));
					}
				else {
					EmitCode(bcPushInteger1);

					EmitByte(LOBYTE(Value));
					}
				}

			EmitConversion(typeInteger, Type);
			}

		return;
		}

	if( Const.IsType(typeReal) ) {

		C3REAL Value = Const.GetRealValue();

		C3INT  Fixed = C3INT(Value);

		if( Type == typeInteger ) {

			EmitConstant(Fixed, Type);

			return;
			}

		if( Fixed >= 0 && Fixed < 256 && Value == Fixed ) {

			EmitConstant(Fixed, Type);

			return;
			}

		EmitCode(bcPushReal);

		EmitByte(((BYTE *) &Value)[3]);
		EmitByte(((BYTE *) &Value)[2]);
		EmitByte(((BYTE *) &Value)[1]);
		EmitByte(((BYTE *) &Value)[0]);

		EmitConversion(typeReal, Type);

		return;
		}

	if( Const.IsType(typeString) ) {

		CString Value = Const.GetStringValue();

		BOOL    fWide = FALSE;

		for( UINT n = 0; Value[n]; n++ ) {

			if( HIBYTE(Value[n]) ) {

				fWide = TRUE;

				break;
				}
			}

		if( !fWide ) {

			EmitCode(bcPushString);

			EmitWord(WORD(Value.GetLength()+1));

			for( UINT n = 0; Value[n]; n++ ) {

				EmitByte(BYTE(Value[n]));
				}

			EmitByte(0);
			}
		else {
			EmitCode(bcPushWideString);

			EmitWord(WORD(Value.GetLength()+1));

			WORD w;

			for( UINT n = 0; (w = Value[n]); n++ ) {

				EmitByte(HIBYTE(w));

				EmitByte(LOBYTE(w));
				}

			EmitByte(0);

			EmitByte(0);
			}
		
		return;
		}

	ThrowInternal();
	}

void CCodeGenerator::EmitLoadAddress(CParseNode const &Node)
{
	if( Node.m_Code == usageVariable ) {

		EmitCode(bcLoadAddr);
		
		EmitName(Node);

		return;
		}

	ThrowInternal();
	}

void CCodeGenerator::EmitLoadValue(CParseNode const &Node)
{
	if( Node.m_Code == usageVariable ) {

		if( Node.m_Type >= typeObject ) {

			EmitCode(bcLoadAddr);

			EmitName(Node);

			return;
			}

		switch( Node.m_Type ) {

			case typeInteger:	EmitCode(bcGetInteger);		break;
			case typeReal:		EmitCode(bcGetReal);		break;
			case typeString:	EmitCode(bcGetString);		break;

			default:		ThrowInternal();

			}

		EmitName(Node);

		return;
		}

	if( Node.m_Code == usageLocal ) {

		if( Node.m_Type == typeString ) {

			EmitCode(bcGetLocalString);

			EmitLocalName(Node);
			}
		else {
			BYTE bSlot = BYTE(Node.m_Const.GetIntegerValue());

			if( bSlot < 6 ) {

				BYTE bCode = BYTE(bcGetLocal0 + bSlot);

				EmitCode(bCode);
				}
			else {
				EmitCode(bcGetLocal);
				
				EmitByte(bSlot);
				}
			}

		return;
		}

	ThrowInternal();
	}

void CCodeGenerator::EmitSaveValue(CParseNode const &Node)
{
	if( Node.m_Code == usageVariable ) {

		switch( Node.m_Type ) {

			case typeInteger:	EmitCode(bcPutInteger);		break;
			case typeReal:		EmitCode(bcPutReal);		break;
			case typeString:	EmitCode(bcPutString);		break;

			default:		ThrowInternal();
			}

		EmitName(Node);

		return;
		}

	if( Node.m_Code == usageLocal ) {

		if( Node.m_Type == typeString ) {

			EmitCode(bcPutLocalString);

			EmitLocalName(Node);
			}
		else {
			BYTE bSlot = BYTE(Node.m_Const.GetIntegerValue());

			if( bSlot < 6 ) {

				BYTE bCode = BYTE(bcPutLocal0 + bSlot);

				EmitCode(bCode);
				}
			else {
				EmitCode(bcPutLocal);
				
				EmitByte(bSlot);
				}
			}

		return;
		}

	ThrowInternal();
	}

void CCodeGenerator::EmitLoadProperty(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcLoadPropInteger);	break;
		case typeReal:		EmitCode(bcLoadPropReal);	break;
		case typeString:	EmitCode(bcLoadPropString);	break;

		default:		ThrowInternal();
		}
	}

void CCodeGenerator::EmitLoadIndirect(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcLoadInteger);	break;
		case typeReal:		EmitCode(bcLoadReal);		break;
		case typeString:	EmitCode(bcLoadString);		break;

		default:		ThrowInternal();
		}
	}

void CCodeGenerator::EmitSaveIndirect(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcSaveInteger);	break;
		case typeReal:		EmitCode(bcSaveReal);		break;
		case typeString:	EmitCode(bcSaveString);		break;

		default:		ThrowInternal();
		}
	}

void CCodeGenerator::EmitOperator(UINT Code, UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitIntegerOperator(Code);	break;
		case typeReal:		EmitRealOperator(Code);		break;
		case typeString:	EmitStringOperator(Code);	break;

		default:		ThrowInternal();
		}
	}

void CCodeGenerator::EmitIntegerOperator(UINT Code)
{
	switch( Code ) {

		case tokenLogicalNot:		EmitCode(bcLogicalNot);		break;
		case tokenBitwiseNot:		EmitCode(bcBitwiseNot);		break;
		case tokenUnaryMinus:		EmitCode(bcUnaryMinus);		break;
		case tokenUnaryPlus:		/*NOP*/				break;
		case tokenPostIncrement:	EmitCode(bcIncrement);		break;
		case tokenPostDecrement:	EmitCode(bcDecrement);		break;
		case tokenPreIncrement:		EmitCode(bcIncrement);		break;
		case tokenPreDecrement:		EmitCode(bcDecrement);		break;
		case tokenBitSelect:		EmitCode(bcBitSelect);		break;
		case tokenMultiply:		EmitCode(bcMultiply);		break;
		case tokenDivide:		EmitCode(bcDivide);		break;
		case tokenRemainder:		EmitCode(bcRemainder);		break;
		case tokenAdd:			EmitCode(bcAdd);		break;
		case tokenSubtract:		EmitCode(bcSubtract);		break;
		case tokenLeftShift:		EmitCode(bcLeftShift);		break;
		case tokenRightShift:		EmitCode(bcRightShift);		break;
		case tokenLessThan:		EmitCode(bcLessThan);		break;
		case tokenLessOr:		EmitCode(bcLessOr);		break;
		case tokenGreaterThan:		EmitCode(bcGreaterThan);	break;
		case tokenGreaterOr:		EmitCode(bcGreaterOr);		break;
		case tokenEqual:		EmitCode(bcEqual);		break;
		case tokenNotEqual:		EmitCode(bcNotEqual);		break;
		case tokenBitwiseAnd:		EmitCode(bcBitwiseAnd);		break;
		case tokenBitwiseXor:		EmitCode(bcBitwiseXor);		break;
		case tokenBitwiseOr:		EmitCode(bcBitwiseOr);		break;

		default:			ThrowInternal();
		}
	}

void CCodeGenerator::EmitRealOperator(UINT Code)
{
	switch( Code ) {

		case tokenLogicalNot:		EmitCode(bcRealLogicalNot);	break;
		case tokenUnaryMinus:		EmitCode(bcRealUnaryMinus);	break;
		case tokenUnaryPlus:		/*NOP*/				break;
		case tokenPostIncrement:	EmitCode(bcRealIncrement);	break;
		case tokenPostDecrement:	EmitCode(bcRealDecrement);	break;
		case tokenPreIncrement:		EmitCode(bcRealIncrement);	break;
		case tokenPreDecrement:		EmitCode(bcRealDecrement);	break;
		case tokenMultiply:		EmitCode(bcRealMultiply);	break;
		case tokenDivide:		EmitCode(bcRealDivide);		break;
		case tokenAdd:			EmitCode(bcRealAdd);		break;
		case tokenSubtract:		EmitCode(bcRealSubtract);	break;
		case tokenLessThan:		EmitCode(bcRealLessThan);	break;
		case tokenLessOr:		EmitCode(bcRealLessOr);		break;
		case tokenGreaterThan:		EmitCode(bcRealGreaterThan);	break;
		case tokenGreaterOr:		EmitCode(bcRealGreaterOr);	break;
		case tokenEqual:		EmitCode(bcRealEqual);		break;
		case tokenNotEqual:		EmitCode(bcRealNotEqual);	break;

		default:			ThrowInternal();
		}
	}

void CCodeGenerator::EmitStringOperator(UINT Code)
{
	switch( Code ) {

		case tokenAdd:			EmitCode(bcStringAdd);		break;
		case tokenBitwiseAnd:		EmitCode(bcStringAddChar);	break;
		case tokenLessThan:		EmitCode(bcStringLessThan);	break;
		case tokenLessOr:		EmitCode(bcStringLessOr);	break;
		case tokenGreaterThan:		EmitCode(bcStringGreaterThan);	break;
		case tokenGreaterOr:		EmitCode(bcStringGreaterOr);	break;
		case tokenEqual:		EmitCode(bcStringEqual);	break;
		case tokenNotEqual:		EmitCode(bcStringNotEqual);	break;

		default:			ThrowInternal();
		}
	}

void CCodeGenerator::EmitConversion(UINT From, UINT Type)
{
	if( Type == From ) {

		return;
		}

	switch( From ) {

		case typeInteger:	EmitFromInteger(Type);		break;
		case typeLogical:	EmitFromLogical(Type);		break;
		case typeReal:		EmitFromReal(Type);		break;
		case typeString:	EmitFromString(Type);		break;

		default:		ThrowInternal();
		}
	}

void CCodeGenerator::EmitFromInteger(UINT Type)
{
	if( Type >= typeIndex ) {

		return;
		}

	switch( Type ) {

		case typeInteger:	/*NOP*/				break;
		case typeLogical:	EmitCode(bcTestInteger);	break;
		case typeReal:		EmitCode(bcToReal);		break;
		case typeVoid:		EmitCode(bcPop);		break;
//		case typeNumeric:	/*NOP*/				break;
		
		default:		ThrowInternal();		break;
		}
	}

void CCodeGenerator::EmitFromLogical(UINT Type)
{
	switch( Type ) {

		case typeInteger:	/*NOP*/				break;
		case typeLogical:	/*NOP*/				break;
		case typeReal:		EmitCode(bcToReal);		break;
		case typeVoid:		EmitCode(bcPop);		break;
//		case typeNumeric:	/*NOP*/				break;
		
		default:		ThrowInternal();		break;
		}
	}

void CCodeGenerator::EmitFromReal(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcToInteger);		break;
		case typeLogical:	EmitCode(bcTestReal);		break;
		case typeReal:		/*NOP*/				break;
		case typeVoid:		EmitCode(bcPop);		break;
//		case typeNumeric:	/*NOP*/				break;
		
		default:		ThrowInternal();		break;
		}
	}

void CCodeGenerator::EmitFromString(UINT Type)
{
	switch( Type ) {

		case typeVoid:		EmitCode(bcPopString);		break;
		case typeString:	/*NOP*/				break;
		
		default:		ThrowInternal();		break;
		}
	}

void CCodeGenerator::EmitDupTop(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcDupTop);		break;
		case typeReal:		EmitCode(bcDupTop);		break;
		case typeString:	EmitCode(bcDupTopString);	break;

		}
	}

void CCodeGenerator::EmitDup3rd(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcDup3rd);		break;
		case typeReal:		EmitCode(bcDup3rd);		break;
		case typeString:	EmitCode(bcDup3rdString);	break;

		}
	}

void CCodeGenerator::EmitPop(UINT Type)
{
	switch( Type ) {

		case typeInteger:	EmitCode(bcPop);		break;
		case typeReal:		EmitCode(bcPop);		break;
		case typeString:	EmitCode(bcPopString);		break;

		}
	}

// Low-Level Coding

void CCodeGenerator::EmitCode(BYTE bCode)
{
	m_Code.Append(bCode);
	}

void CCodeGenerator::EmitByte(BYTE bCode)
{
	m_Code.Append(bCode);
	}

void CCodeGenerator::EmitWord(WORD wCode)
{
	EmitByte(HIBYTE(wCode));

	EmitByte(LOBYTE(wCode));
	}

void CCodeGenerator::EmitLong(DWORD dwCode)
{
	EmitWord(HIWORD(dwCode));

	EmitWord(LOWORD(dwCode));
	}

void CCodeGenerator::EmitBool(BOOL fState)
{
	EmitByte(BYTE(fState ? 0x01 : 0x00));
	}

void CCodeGenerator::EmitLocalName(CParseNode const &Node)
{
	EmitByte(LOBYTE(Node.m_Const.GetIntegerValue()));
	}

void CCodeGenerator::EmitName(CParseNode const &Node)
{
	EmitWord(LOWORD(Node.m_Const.GetIntegerValue()));
	}

// Branch Support

UINT CCodeGenerator::EmitFwdJump(void)
{
	m_uLabel++;

	EmitJump(m_uLabel);

	return m_uLabel;
	}

UINT CCodeGenerator::EmitFwdBranch(BOOL fTest)
{
	m_uLabel++;

	EmitBranch(m_uLabel, fTest);

	return m_uLabel;
	}

void CCodeGenerator::EmitJump(UINT uLabel)
{
	EmitCode(bcBranch);

	EmitWord(WORD(uLabel));
	}

void CCodeGenerator::EmitBranch(UINT uLabel, BOOL fTest)
{
	EmitCode(BYTE(fTest ? bcBranchZero : bcBranchNonZero));

	EmitWord(WORD(uLabel));
	}

BOOL CCodeGenerator::EmitLabel(UINT uLabel)
{
	if( uLabel ) {

		EmitCode(bcLabel);

		EmitWord(WORD(uLabel));

		return TRUE;
		}

	return FALSE;
	}

UINT CCodeGenerator::EmitLabel(void)
{
	m_uLabel++;

	EmitLabel(m_uLabel);

	return m_uLabel;
	}

// Assignment Conversion

UINT CCodeGenerator::GetEquivalent(UINT Code)
{
	switch( Code ) {

		case tokenMultiplyAssign:	return tokenMultiply;
		case tokenDivideAssign:		return tokenDivide;
		case tokenRemainderAssign:	return tokenRemainder;
		case tokenAddAssign:		return tokenAdd;
		case tokenSubtractAssign:	return tokenSubtract;
		case tokenLeftShiftAssign:	return tokenLeftShift;
		case tokenRightShiftAssign:	return tokenRightShift;
		case tokenAndAssign:		return tokenBitwiseAnd;
		case tokenXorAssign:		return tokenBitwiseXor;
		case tokenOrAssign:		return tokenBitwiseOr;

		}

	return tokenNull;
	}

// Operand Characterization

BOOL CCodeGenerator::IsNullExpression(void)
{
	CParseNode const &Test = m_Tree[m_uPos];

	if( Test.m_Group == groupOperator ) {
		
		if( Test.m_Code == tokenNull ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CCodeGenerator::GetRightType(CParseNode const &Node)
{
	if( Node.m_Type == typeString ) {

		if( Node.m_Code == tokenAndAssign ) {

			return typeInteger;
			}
		}

	return Node.m_Type;
	}

BOOL CCodeGenerator::CanSaveValue(CParseNode const &Node)
{
	if( !Node.m_uOrder ) {

		if( Node.m_Group == groupIdentifier ) {
			
			if( Node.m_Code == usageDirectRef ) {

				return FALSE;
				}
			
			if( Node.m_Code == usageFullRef ) {

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Loop Support

PVOID CCodeGenerator::LoopCreate(UINT xb, UINT xc)
{
	CLoopCtx *pLast = m_pLoop;

	m_pLoop = New CLoopCtx;

	m_pLoop->m_uLabel[loopBreak   ] = xb;

	m_pLoop->m_uLabel[loopContinue] = xc;
	
	m_pLoop->m_fEmit [loopBreak   ] = xb ? TRUE : FALSE;

	m_pLoop->m_fEmit [loopContinue] = xc ? TRUE : FALSE;

	return pLast;
	}

BOOL CCodeGenerator::LoopCheckBranch(void)
{
	if( m_pLoop ) {

		CParseNode const &Node = m_Tree[m_uPos];

		if( Node.m_Group == groupKeyword ) {
			
			if( Node.m_Code == tokenBreak ) {

				LoopEmit(loopBreak, TRUE);

				m_uPos--;

				return TRUE;
				}

			if( Node.m_Code == tokenContinue ) {

				LoopEmit(loopContinue, TRUE);

				m_uPos--;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CCodeGenerator::LoopEmit(UINT uSlot, BOOL fCond)
{
	AfxAssert(m_pLoop);

	UINT x;
	
	if( (x = m_pLoop->m_uLabel[uSlot]) ) {

		if( fCond ) {

			EmitBranch(x, FALSE);
			}
		else
			EmitJump(x);
		}
	else {
		if( fCond ) {

			x = EmitFwdBranch(FALSE);
			}
		else
			x = EmitFwdJump();

		m_pLoop->m_uLabel[uSlot] = x;
		}
	}

BOOL CCodeGenerator::LoopFixup(UINT uSlot)
{
	AfxAssert(m_pLoop);

	if( m_pLoop->m_uLabel[uSlot] && !m_pLoop->m_fEmit[uSlot] ) {

		EmitLabel(m_pLoop->m_uLabel[uSlot]);

		return TRUE;
		}

	return FALSE;
	}

void CCodeGenerator::LoopDelete(PVOID pLast)
{
	AfxAssert(m_pLoop);

	delete m_pLoop;

	m_pLoop = (CLoopCtx *) pLast;
	}

// Final Passes

void CCodeGenerator::Optimize(void)
{
	ShowCode(L"Orginal");

	CCodeOptimizer *pOptim = New CCodeOptimizer(m_Code, m_uInit);

	if( !pOptim->Optimize(*m_pError) ) {

		delete pOptim;

		ThrowError();

		return;
		}

	delete pOptim;

	ShowCode(L"Optimized");
	}

void CCodeGenerator::FixJumps(void)
{
	CArray <UINT> List;

	for( UINT p = 0; p < 2; p++ ) {

		UINT uPos   = m_uInit;

		UINT uCount = m_Code.GetCount();

		while( uPos < uCount ) {

			if( p == 0 ) {

				if( m_Code[uPos] == bcLabel ) {

					UINT high  = m_Code[uPos+1];
					
					UINT low   = m_Code[uPos+2];

					UINT label = MAKEWORD(low,high);

					if( label >= List.GetCount() ) {

						List.SetCount(label + 1);
						}

					List.SetAt(label, uPos);

					m_Code.Remove(uPos, 3);

					uCount -= 3;

					continue;
					}
				}

			if( p == 1 ) {

				if( IsBranch(m_Code[uPos]) ) {

					UINT high  = m_Code[uPos+1];
					
					UINT low   = m_Code[uPos+2];

					UINT label = MAKEWORD(low,high);

					UINT uAddr = List.GetAt(label);

					UINT uJump = uAddr - (uPos + 1);

					m_Code.SetAt(uPos+1, HIBYTE(uJump));

					m_Code.SetAt(uPos+2, LOBYTE(uJump));

					uPos += 3;

					continue;
					}
				}

			uPos = GetNextOpcode(uPos);
			}
		}

	ShowCode(L"Final");
	}

// Opcode Scanning

BOOL CCodeGenerator::IsBranch(BYTE bCode)
{
	switch( bCode ) {

		case bcBranchZero:
		case bcBranchNonZero:
		case bcBranch:

			return TRUE;
		}

	return FALSE;
	}

BYTE CCodeGenerator::GetOpcodeSize(BYTE bCode)
{
	switch( bCode ) {

		case bcPutLocal:
		case bcPutLocalString:
		case bcGetLocal:
		case bcGetLocalString:
		case bcPushInteger1:

			return 1;
	
		case bcBranchZero:
		case bcBranchNonZero:
		case bcBranch:
		case bcLabel:

			return 2;

		case bcLoadAddr:
		case bcPutInteger:
		case bcPutReal:
		case bcPutString:
		case bcGetInteger:
		case bcGetReal:
		case bcGetString:
		case bcPushInteger2:
		case bcDebugInfo:

			return 2;

		case bcFunction:

			return 3;

		case bcPushInteger4:
		case bcPushReal:

			return 4;
		}

	return 0;
	}

UINT CCodeGenerator::GetNextOpcode(UINT uPos)
{
	BYTE bCode = m_Code[uPos];

	if( bCode == bcPushString ) {

		UINT uLength = MAKEWORD(m_Code[uPos+2], m_Code[uPos+1]);

		uPos += 3;

		uPos += 1 * uLength;

		return uPos;
		}

	if( bCode == bcPushWideString ) {

		UINT uLength = MAKEWORD(m_Code[uPos+2], m_Code[uPos+1]);

		uPos += 3;

		uPos += 2 * uLength;

		return uPos;
		}

	if( bCode == bcDebugFunc ) {

		UINT uArgs = m_Code[uPos+1];

		uPos += 1 + uArgs;

		uPos += 2;

		return uPos;
		}

	uPos += 1;

	uPos += GetOpcodeSize(bCode);

	return uPos;
	}

// Implementation

void CCodeGenerator::CheckBit(C3INT nBit)
{
	if( nBit < 0 || nBit > 31 ) {

		m_pError->Set(IDS_CODE_BAD_BIT);

		ThrowError();
		}
	}

void CCodeGenerator::SkipNode(void)
{
	CParseNode const &Node = m_Tree[m_uPos--];

	for( UINT n = 0; n < Node.m_uOrder; n++ ) {

		SkipNode();
		}
	}

void CCodeGenerator::ThrowInternal(void)
{
	m_pError->Set(IDS_INTERNAL_ERROR_IN);

	ThrowError();
	}

void CCodeGenerator::ThrowError(void)
{
	AfxThrowUserException();
	}

void CCodeGenerator::ShowCode(PCTXT p)
{
	#if 0

	AfxTrace(L"%-9.9s : ", p);

	for( UINT n = m_uInit; n < m_Code.GetCount(); n++ ) {

		AfxTrace(L"%2.2X ", m_Code[n]);
		}

	AfxTrace(L"\n");

	#endif
	}

// Debug Support

void CCodeGenerator::InitDebugData(UINT uLocal, CCompileIn const &In)
{
	UINT n;

	for( n = 0; n < In.m_uParam; n++ ) {

		m_DebugParam.Append(In.m_pParam[n]);
		}

	while( n++ < uLocal ) {

		CFuncParam Param;

		Param.m_Type = 0;

		m_DebugParam.Append(Param);
		}

	m_DebugScope = In.m_Scope;
	}

void CCodeGenerator::AddDebugData(void)
{
	UINT uPos = m_Code.GetCount();

	m_Code.SetAt(5, LOBYTE(WORD(uPos)));

	m_Code.SetAt(6, HIBYTE(WORD(uPos)));

	EmitByte(0xDD);
	EmitByte(0xEE);
	EmitByte(0xBB);
	EmitByte(0x00);

	EmitByte(LOBYTE(m_wDebug));
	EmitByte(HIBYTE(m_wDebug));

	CByteArray Data;

	CLongArray Refs;

	AddDebugString(Data, Refs, m_DebugScope);

	for( UINT n = 0; n < m_DebugParam.GetCount(); n++ ) {

		CFuncParam const &Param = m_DebugParam[n];

		AddDebugParam(Data, Refs, Param);
		}

	FixDebugStrings(Refs, m_Code.GetCount() - uPos);

	m_Code.Append(Data);
	}

void CCodeGenerator::AddDebugString(CByteArray &Data, CLongArray &Refs, CString const &Text)
{
	UINT uPos = Data.GetCount();

	Refs.Append(m_Code.GetCount());

	EmitByte(LOBYTE(uPos));

	EmitByte(HIBYTE(uPos));

	for( UINT n = 0; n < Text.GetLength(); n++ ) {

		Data.Append(BYTE(Text[n]));
		}

	Data.Append(0);
	}

void CCodeGenerator::AddDebugParam(CByteArray &Data, CLongArray &Refs, CFuncParam const &Param)
{
	EmitByte(BYTE(Param.m_Type));

	AddDebugString(Data, Refs, Param.m_Name);
	}

void CCodeGenerator::FixDebugStrings(CLongArray const &Refs, UINT uBase)
{
	for( UINT r = 0; r < Refs.GetCount(); r++ ) {

		PWORD pPos = PWORD(m_Code.GetPointer() + Refs[r]);

		*pPos += WORD(uBase);
		}
	}

// End of File
