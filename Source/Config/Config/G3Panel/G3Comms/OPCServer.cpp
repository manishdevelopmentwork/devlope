
#include "Intern.hpp"

#include "OPCServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC Configuration
//

// Dynamic Class

AfxImplementDynamicClass(COPCServer, CServiceItem);

// Constructor

COPCServer::COPCServer(void)
{
	m_pEnable   = NULL;
	m_pSocket   = NULL;
	m_pCount    = NULL;
	m_pRestrict = NULL;
	m_pSecAddr  = NULL;
	m_pSecMask  = NULL;
	}

// Initial Values

void COPCServer::SetInitValues(void)
{
	SetInitial(L"Enable",   m_pEnable,    0);
	SetInitial(L"Socket",   m_pSocket,  790);
	SetInitial(L"Count",    m_pCount,     2);
	SetInitial(L"Restrict", m_pRestrict,  0);
	SetInitial(L"SecAddr",  m_pSecAddr,   0);
	SetInitial(L"SecMask",  m_pSecMask,   0);
	}

// UI Update

void COPCServer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Enable" || Tag == "Restrict" ) {

		DoEnables(pHost);

		return;
		}
	}

// Type Access

BOOL COPCServer::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

BOOL COPCServer::IsEnabled(void) const
{
	return FALSE;
	}

UINT COPCServer::GetTreeImage(void) const
{
	return IDI_OPC;
	}

// Download Support

BOOL COPCServer::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(4));

	CServiceItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pEnable);
	Init.AddItem(itemVirtual, m_pSocket);
	Init.AddItem(itemVirtual, m_pCount);
	Init.AddItem(itemVirtual, m_pRestrict);
	Init.AddItem(itemVirtual, m_pSecAddr);
	Init.AddItem(itemVirtual, m_pSecMask);

	return TRUE;
	}

// Meta Data Creation

void COPCServer::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Socket);
	Meta_AddVirtual(Count);
	Meta_AddVirtual(Restrict);
	Meta_AddVirtual(SecAddr);
	Meta_AddVirtual(SecMask);

	Meta_SetName((IDS_OPC_PROXY));
	}

// Implementation

void COPCServer::DoEnables(IUIHost *pHost)
{
	BOOL fEnable   = CheckEnable(m_pEnable,   L"==", 1, TRUE);

	BOOL fRestrict = CheckEnable(m_pRestrict, L">=", 1, TRUE);

	pHost->EnableUI("Socket",   fEnable);

	pHost->EnableUI("Count",    fEnable);

	pHost->EnableUI("Restrict", fEnable);

	pHost->EnableUI("SecAddr",  fEnable && fRestrict);

	pHost->EnableUI("SecMask",  fEnable && fRestrict);
	}

// End of File
