
#include "intern.hpp"

#include "PrimRubyAnimImage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyTankFill.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Image Primitive
//

// Constructor

CPrimRubyAnimImage::CPrimRubyAnimImage(void)
{
	m_Count  = 1;

	m_pValue = NULL;

	m_pColor = NULL;

	m_pShow  = NULL;

	m_fAny   = FALSE;

	memset(m_pImage, 0, sizeof(m_pImage));
	}

// Destructor

CPrimRubyAnimImage::~CPrimRubyAnimImage(void)
{
	delete m_pValue;

	delete m_pColor;

	delete m_pShow;

	while( m_Count-- ) {

		delete m_pImage[m_Count];
		}
	}

// Initialization

void CPrimRubyAnimImage::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyAnimImage", pData);

	CPrimRubyGeom::DoLoad(pData);

	m_Count = GetByte(pData);

	for( UINT n = 0; n < m_Count; n++ ) {

		if( GetByte(pData) ) {

			m_pImage[n] = New CPrimImage;
		
			m_pImage[n]->Load(pData);

			m_fAny = TRUE;
			}
		}

	GetCoded(pData, m_pValue);

	GetCoded(pData, m_pColor);

	GetCoded(pData, m_pShow);

	m_Margin.x1 = GetByte(pData);
	m_Margin.y1 = GetByte(pData);
	m_Margin.x2 = GetByte(pData);
	m_Margin.y2 = GetByte(pData);

	FindImageRect();
	}

// Overridables

void CPrimRubyAnimImage::SetScan(UINT Code)
{
	SetItemScan(m_pValue, Code);

	SetItemScan(m_pColor, Code);

	SetItemScan(m_pShow,  Code);

	CPrimRubyGeom::SetScan(Code);
	}

void CPrimRubyAnimImage::MovePrim(int cx, int cy)
{
	CPrimRubyGeom::MovePrim(cx, cy);

	FindImageRect();
	}

void CPrimRubyAnimImage::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimRubyGeom::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;
			
			m_fChange = TRUE;
			}

		if( m_pFill->IsNull() ) {

			CPrimImage *pImage = m_pImage[Ctx.m_uImage];

			if( !pImage || pImage->IsTransparent(m_ImageRect) ) {

				if( pImage && pImage->IsAntiAliased() ) {

					(m_fChange ? Erase :Trans).Append(m_ImageRect);
					}
				else {
					if( m_fChange ) {

						Erase.Append(m_ImageRect);
						}
					}
				}
			}
		}
	}

void CPrimRubyAnimImage::DrawPrim(IGDI *pGDI)
{
	if( TRUE ) {

		m_pFill->Fill(pGDI, m_listFill, !m_fFast);

		m_pEdge->Fill(pGDI, m_listEdge, TRUE);

		m_pEdge->Trim(pGDI, m_listTrim, TRUE);
		}

	if( m_fAny ) {

		if( m_Ctx.m_fShow ) {

			CPrimImage *pImage = m_pImage[m_Ctx.m_uImage];

			if( pImage ) {

				UINT rop = m_Ctx.m_fColor ? 0 : ropDisable;

				pImage->DrawItem(pGDI, m_ImageRect, rop);
				}
			}
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCUTF pt = L"IMG";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimRubyWithText::DrawPrim(pGDI);
	}

// Implementation

void CPrimRubyAnimImage::FindImageRect(void)
{
	m_ImageRect = m_DrawRect;

	int nAdjust = m_pEdge->GetInnerWidth();

	DeflateRect(m_ImageRect, nAdjust, nAdjust);

	m_ImageRect.left   += m_Margin.left;

	m_ImageRect.right  -= m_Margin.right;
	
	m_ImageRect.top    += m_Margin.top;
	
	m_ImageRect.bottom -= m_Margin.bottom;
	}

// Context Creation

void CPrimRubyAnimImage::FindCtx(CCtx &Ctx)
{
	Ctx.m_uImage = GetItemData(m_pValue, C3INT(0)) % m_Count;

	Ctx.m_fColor = GetItemData(m_pColor, C3INT(1));
	
	Ctx.m_fShow  = GetItemData(m_pShow,  C3INT(1));
	}

// Context Check

BOOL CPrimRubyAnimImage::CCtx::operator == (CCtx const &That) const
{
	return m_uImage == That.m_uImage &&
	       m_fColor == That.m_fColor &&
	       m_fShow  == That.m_fShow  ;
	}

// End of File
