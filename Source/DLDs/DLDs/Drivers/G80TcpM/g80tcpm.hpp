#include "gem80.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom GEM80 Master TCP/IP Driver
//

class CGem80MasterTCPDriver : public CGem80MasterDriver
{
	public:
		// Constructor
		CGem80MasterTCPDriver(void);

		// Destructor
		~CGem80MasterTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		
	protected:
		// Device Context
		struct CContext	: CGem80MasterDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Implementation
		BOOL Start(void);
		
		// Transport Layer
		BOOL Send(void);
		BOOL RecvFrame(BOOL fWrite);
		BOOL Transact(BOOL fWrite);
		void AddCount(UINT &uCount, UINT uType);
		
	};

// End of File
