
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Security_HPP

#define INCLUDE_Security_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Types
//

typedef struct atcacert_def_s atcacert_def_t;

//////////////////////////////////////////////////////////////////////////
//
// Hardware Security Module
//

class CSecurity : public IDevice, public IFeatures
{
public:
	// Constructor
	CSecurity(void);

	// Destructor
	~CSecurity(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IFeatures
	UINT METHOD GetEnabledGroup(void);
	BOOL METHOD GetDeviceInfo(CSecureDeviceInfo &Info);
	UINT METHOD GetDeviceCode(PBYTE pData, UINT uData);
	BOOL METHOD InstallUnlock(PCBYTE pData, UINT uData);

protected:
	// Data Members
	ULONG		  m_uRefs;
	UINT		  m_uGroup;
	CSecureDeviceInfo m_Info;
	bool		  m_fImport;
	UINT		  m_SignerSize;
	UINT		  m_DeviceSize;
	BYTE		  m_SignerCert[4096];
	BYTE		  m_DeviceCert[4096];
	BYTE		  m_IssuerKey[64];
	BYTE		  m_SignerKey[64];
	BYTE		  m_DeviceKey[64];
	BYTE		  m_DeviceNum[16];

	// HSM Helpers
	bool OpenLibrary(void);
	bool FindIssuerKey(void);
	bool ReadSignerCert(void);
	bool ReadDeviceCert(void);
	bool Challenge(void);
	bool Verify(atcacert_def_t const *pDef, PCBYTE pCert, UINT uCert, PCBYTE key);
	bool Verify(PCBYTE key, PCBYTE msg, PCBYTE sig);
	bool MakeKey(PBYTE key, PCSTR x, PCSTR y);

	// Device Info
	bool ReadDeviceInfo(void);
	bool SaveDeviceInfo(void);
	bool ImportDeviceInfo(void);
	bool VerifyKeyFile(CStringArray const &List);
	bool ReadDeviceKey(void);

	// Implementation
	CString ReadFile(PCTXT pName, PCTXT pDef);
	CString ReadSkvs(PCTXT pKey, PCTXT pDef);
	CString ReadBoot(PCTXT pKey, PCTXT pDef);
};

// End of File

#endif
