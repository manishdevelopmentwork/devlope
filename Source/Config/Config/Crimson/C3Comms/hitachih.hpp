
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HITACHIH_HPP
	
#define	INCLUDE_HITACHIH_HPP

/////////////////////////////////////////////////////////////////////////
//
// Short Names for Types
//

#define	WAW	addrWordAsWord
#define	LAL	addrLongAsLong
#define	BAB	addrBitAsBit

/////////////////////////////////////////////////////////////////////////
//
//  Hitachi H Device Options
//

class CHitachiHDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHitachiHDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Loop;
		UINT m_Unit;
		BOOL m_fCheck;
		BOOL m_fMulti;
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal HitachiH Driver
//

class CHitachiHDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CHitachiHDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(void);
		UINT xtoin(CString Offset);
	};

// End of File

#endif
