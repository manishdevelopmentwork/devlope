
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// License Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CLicenseDialog, CStdDialog);
		
// Constructors

CLicenseDialog::CLicenseDialog(BOOL fWarn)
{
	m_fWarn = fWarn;

	SetName(fWarn ? L"WarningDlg" : L"LicenseDlg");
	}

// Message Map

AfxMessageMap(CLicenseDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchCommand(100,     OnSelect)
	AfxDispatchCommand(101,     OnSelect)

	AfxMessageEnd(CLicenseDialog)
	};

// Message Handlers

BOOL CLicenseDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	return FALSE;
	}

// Command Handlers

BOOL CLicenseDialog::OnCommandOK(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CLicenseDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CLicenseDialog::OnSelect(UINT uID)
{
	SaveData();

	return TRUE;
	}

// Implementation

void CLicenseDialog::SaveData(void)
{
	BOOL  fWarn = ((CButton &) GetDlgItem(101)).IsChecked();

	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Control");

	Reg.MoveTo(m_fWarn ? L"Control" : L"License");

	Reg.SetValue(L"Warning", UINT(!fWarn));
	}

// End of File
