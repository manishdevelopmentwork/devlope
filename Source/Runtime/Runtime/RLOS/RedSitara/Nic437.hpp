
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Nic437_HPP
	
#define	INCLUDE_AM437_Nic437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 NIC
//

class CNic437 : public INic, public IEventSink
{
	public:
		// Constructor
		CNic437(CDualEnet437 *pEnet, UINT uPort, UINT uPhy, UINT uGpio);

		// Destructor
		~CNic437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INic
		bool METHOD Open(bool fFast, bool fFull);
		bool METHOD Close(void);
		bool METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
		bool METHOD IsLinkActive(void);
		bool METHOD WaitLink(UINT uTime);
		bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
		bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Event Handlers
		void OnMiiEvent(WORD wData);
		void OnPhyEvent(WORD wData);
		void OnRecv(void);
		void OnSend(void);

	protected:
		// Buffer Descriptor
		struct CBuffDesc
		{
			DWORD		m_NextPtr;
			DWORD		m_BuffPtr;
			DWORD		m_BuffLen	: 16;
			DWORD		m_BuffOff	: 16;
			DWORD volatile	m_PacketLen	: 16;
			DWORD volatile	m_Flags		: 16;
			};

		// Ring Buffer
		struct CNicBuffer
		{
			UINT		m_iIndex;
			CBuffDesc     * m_pDesc;
			DWORD		m_dwDesc;
			PBYTE		m_pData;
			DWORD		m_dwData;
			};

		// Buffer Descriptor Flags
		enum
		{
			flagSop		= Bit(15),
			flagEop		= Bit(14),
			flagOwner	= Bit(13),
			flagEoq		= Bit(12),
			flagTeardown	= Bit(11),
			flagPassCrc	= Bit(10),
			flagLong	= Bit( 9),
			flagShort	= Bit( 8),
			flagCtrl	= Bit( 7),
			flagOverrun	= Bit( 6),
			flagErrorHi	= Bit( 5),
			flagErrorLo	= Bit( 4),
			flagToPortEn	= Bit( 4),
			flagVlan	= Bit( 3),
			flagPort	= 0,
			flagErrors	= flagLong | flagShort | flagCtrl | flagOverrun | flagErrorHi | flagErrorLo,
			};
		
		// Nic States
		enum
		{
			nicInitial,
			nicClosed,
			nicOpen,
			nicNegotiate,
			nicActive,
			};

		// Phy States
		enum
		{
			phyIdle,
			phyGetIsr,
			phyGetLink,
			};

		// Mii State
		enum
		{
			miiIdle,
			miiAppPut,
			miiAppGet,
			miiIntPut,
			miiIntGet,
			};

		// Buffer Layout
		enum
		{
			constTxLimit	= 16,
			constRxLimit	= 64,
			constBuffSize	= 1522,
			constMinPacket	= 64 - 4,
			};

		// Data
		ULONG            m_uRefs;
		IBufferManager * m_pBuffMan;
		CDualEnet437   * m_pEnet;
		IGpio          * m_pGpioPhyReset;
		IGpio          * m_pGpioPhyInt;
		UINT             m_uPhyIntLine;
		UINT		 m_uChan;
		UINT		 m_uPort;
		UINT             m_uPhy;
		UINT		 m_uPhyReset;
		MACADDR		 m_MacAddr;
		MACADDR	       * m_pMacList;
		UINT		 m_uMacList;
		UINT		 m_uFlags;
		bool		 m_fAllowFull;
		bool		 m_fAllowFast;
		bool		 m_fUsingFull;
		bool		 m_fUsingFast;
		UINT		 m_uNicState;
		UINT             m_uMiiState;
		UINT		 m_uPhyState;
		IEvent	       * m_pMiiFlag;
		IEvent         * m_pLinkOkay;
		IEvent         * m_pLinkDown;
		IMutex	       * m_pRxLock;
		IMutex	       * m_pTxLock;
		ISemaphore     * m_pRxFlag;
		ISemaphore     * m_pTxFlag;
		CBuffDesc      * m_pTxDesc;
		CBuffDesc      * m_pRxDesc;
		CNicBuffer     * m_pTxBuff;
		CNicBuffer     * m_pRxBuff;
		PBYTE	         m_pTxData;
		PBYTE		 m_pRxData;
		UINT		 m_uTxHead;
		UINT		 m_uTxTail;
		UINT		 m_uTxLast;
		UINT		 m_uRxHead;
		UINT		 m_uRxTail;
		UINT		 m_uRxLast;
		INT		 m_nPoll;
	
		// Mac Management
		void ResetState(void);
		void AddMac(PBYTE pData);
		void FreeMacList(void);
		
		// Phy Management
		void ResetPhy(bool fAssert);
		void PhyInterrupt(bool fEnable);
		void ConfigPhy(void);
		void OnLinkUp(void);
		void OnLinkDown(void);

		// Buffer Management
		void  AllocBuffers(void);
		PVOID AllocNoneCached(UINT uAlloc, UINT uAlign);
		void  ResetBuffers(void);
		void  FreeBuffers (void);
		void  StartRecv(CNicBuffer *pBuff);
		void  StartSend(CNicBuffer *pBuff);

		// Mii Managment
		WORD AppGetMii(BYTE bAddr);
		BOOL AppPutMii(BYTE bAddr, WORD wData);
		void IntGetMii(BYTE bAddr);
		void IntPutMii(BYTE bAddr, WORD wData);

		// Implementation
		void EnableEvents(void);
	};

// End of File

#endif
