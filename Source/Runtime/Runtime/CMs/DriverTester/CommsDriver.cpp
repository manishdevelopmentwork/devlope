
#include "Intern.hpp"

#include "CommsDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Driver Object
//

// Constructor

CCommsDriver::CCommsDriver(void)
{
	m_pDevice = NULL;

	m_pData   = NULL;

	m_fOpen   = FALSE;
	}

// IUnknown

HRESULT CCommsDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsDriver);

	return CDriver::QueryInterface(riid, ppObject);
	}

// ICommsDriver

void METHOD CCommsDriver::Load(LPCBYTE pData)
{
	}

void METHOD CCommsDriver::Load(LPCBYTE pData, UINT uSize)
{
	Load(pData);
	}

void METHOD CCommsDriver::CheckConfig(CSerialConfig &Config)
{
	}

void METHOD CCommsDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pData = MakeDoubleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void METHOD CCommsDriver::Detach(void)
{
	if( m_pData ) {

		m_fOpen = FALSE;

		m_pData->Release();

		m_pData = NULL;
		}
	}

void METHOD CCommsDriver::Open(void)
{
	m_fOpen = TRUE;
	}

void METHOD CCommsDriver::Close(void)
{
	m_fOpen = FALSE;
	}

CCODE METHOD CCommsDriver::DeviceOpen(ICommsDevice *pDevice)
{
	m_pDevice = pDevice;

	return CCODE_SUCCESS;
	}

CCODE METHOD CCommsDriver::DeviceClose(BOOL fPersistConnection)
{
	m_pDevice = NULL;

	return CCODE_SUCCESS;
	}

void METHOD CCommsDriver::Service(void)
{
	}

UINT METHOD CCommsDriver::DrvCtrl(UINT uFunc, PCTXT pValue)
{
	return 0;
	}

UINT METHOD CCommsDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT pValue)
{
	return 0;
	}

// Implementation

BOOL CCommsDriver::Make485(CSerialConfig &Config, BOOL fMaster)
{
	if( Config.m_uPhysMode ) {

		return Make422(Config, fMaster);
		}

	switch( Config.m_uPhysical ) {
		
		case physicalRS422Master:
		case physicalRS422Slave:
		case physicalRS485:

			Config.m_uPhysical = physicalRS485;

			return TRUE;
		}

	return FALSE;
	}

BOOL CCommsDriver::Make422(CSerialConfig &Config, BOOL fMaster)
{
	if( !Config.m_uPhysMode ) {

		return Make485(Config, fMaster);
		}

	switch( Config.m_uPhysical ) {
		
		case physicalRS422Master:
		case physicalRS422Slave:
		case physicalRS485:

			Config.m_uPhysical = fMaster ? physicalRS422Master : physicalRS422Slave;

			return TRUE;
		}

	return FALSE;
	}

// End of File
