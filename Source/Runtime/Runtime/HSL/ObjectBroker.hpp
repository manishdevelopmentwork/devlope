
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ObjectBroker_HPP

#define INCLUDE_ObjectBroker_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/1gCkE

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

class CObjectBroker : public IObjectBroker, public IDiagProvider
{
	public:
		// Constructor
		CObjectBroker(void);

		// Destructor
		~CObjectBroker(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IObjectBroker
		HRM RegisterDiagnostics(void);
		HRM RegisterSingleton(PCSTR pName, UINT uInst, IUnknown *punkObject);
		HRM RegisterInstantiator(PCSTR pName, INSTANTIATOR pfnInstantiator);
		HRM GetObject(PCSTR pName, UINT uInst, REFIID riid, void **ppObject);
		HRM NewObject(PCSTR pName, REFIID riid, void **ppObject);
		HRM RevokeSingleton(PCSTR pName, UINT uInst);
		HRM RevokeInstantiator(PCSTR pName);
		HRM RevokeGroup(PCSTR pName);
		HRM SerializeAccess(BOOL fLock);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Object Record
		struct CObject
		{
			CString      m_Name;
			UINT         m_Inst;
			IUnknown *   m_punkObject;
			INSTANTIATOR m_pfnInstantiator;
			};

		// Object Collections
		typedef CList    <CObject *>      CObjectList;
		typedef CZeroMap <CString, INDEX> CObjectDict;

		// Data Members
		ULONG       m_uRefs;
		BOOL	    m_fLock;
		IMutex    * m_pLock;
		CObjectList m_List;
		CObjectDict m_Dict;

		// Diagnostics
		BOOL DiagRegister(void);
		UINT DiagList(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagQuery(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Implementation
		void Revoke(INDEX &Index, CObject *pObject, CString Full);
		void FreeList(void);
	};

// End of File

#endif
