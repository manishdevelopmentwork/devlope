
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Database Format
//

#include "../../../../../Version/dbver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Memory Tracking
//

static BOOL xmt = FALSE;

static UINT xm1;

static UINT xm2;

//////////////////////////////////////////////////////////////////////////
//
// Application Hooks
//

extern BOOL Straton_Service(PCBYTE pIn, PBYTE pOut);

extern BOOL RackTunnel(BYTE bDrop, PBYTE pData);

//////////////////////////////////////////////////////////////////////////
//
// Platform Objects
//

global ICrimsonApp      * g_pApp      = NULL;
global ICrimsonPxe      * g_pPxe      = NULL;
global IDatabase        * g_pDbase    = NULL;
global IPersist	        * g_pPersist  = NULL;
global IEventStorage    * g_pEvStg    = NULL;
global ILicenseManager  * g_pLicense  = NULL;
global ICrimsonIdentity * g_pIdentity = NULL;
global ITlsLibrary      * g_pMatrix   = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
}

// Constructor

CAppObject::CAppObject(void)
{
	g_pApp = this;

	m_pTms = Create_Semaphore();

	StdSetRef();

	DiagRegister();

	ShimInit();

	TimeZoneInit();
}

// Destructor

CAppObject::~CAppObject(void)
{
	DiagRevoke();

	AfxRelease(m_pTms);

	FreeObjects();

	g_pApp = NULL;
}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ICrimsonApp);

	StdQueryInterface(ICrimsonApp);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
}

ULONG CAppObject::Release(void)
{
	StdRelease();
}

// ICrimsonApp

void CAppObject::BindPxe(ICrimsonPxe *pPxe)
{
	g_pPxe = pPxe;

	FindObjects();

	g_pIdentity->Init();
}

void CAppObject::SetWebLink(CString const &Link)
{
	m_Link = Link;
}

BOOL CAppObject::GetWebLink(CString &Link)
{
	Link = m_Link;

	return !m_Link.IsEmpty();
}

BOOL CAppObject::LoadConfig(CString &Error)
{
	xm1 = AfxMarkMemory();

	PCBYTE pData = PCBYTE(g_pDbase->LockItem(1));

	if( pData ) {

		WORD wForm  = MotorToHost(PWORD(pData)[0]);

		WORD wGroup = MotorToHost(PWORD(pData)[2]);

		pData += 4;

		if( wForm == DBASE_FORMAT ) {

			WhoSetRequiredGroup(wGroup);

			if( WhoGetActiveGroup() >= SW_GROUP_2 ) {

				g_pPersist->Init();

				g_pEvStg->Init();

				if( WhoGetActiveGroup() >= SW_GROUP_3A ) {

					(New CUISystem)->Load(pData);
				}
				else {
					(New CCommsSystem)->Load(pData);
				}

				g_pDbase->FreeItem(1);

				AfxMarkMemory();

				return TRUE;
			}
			else {
				g_pDbase->FreeItem(1);

				AfxMarkMemory();

				return TRUE;
			}
		}

		g_pDbase->FreeItem(1);

		Error = "Version Mismatch";

		return FALSE;
	}

	Error = "Invalid Database";

	g_pDbase->SetValid(FALSE);

	return FALSE;
}

BOOL CAppObject::InitApp(void)
{
	if( CSystemItem::m_pThis ) {

		g_pDbase->SetRunning(TRUE);

		CSystemItem::m_pThis->SystemStart();

		CreateTasks();

		xm2 = AfxMarkMemory();

		InitTasks();

		CSystemItem::m_pThis->SystemInit();

		StartTasks();
	}

	return TRUE;
}

BOOL CAppObject::HaltApp(void)
{
	if( CSystemItem::m_pThis ) {

		CSystemItem::m_pThis->SystemSave();

		g_pPersist->Commit(false);

		StopTasks();
	}

	return TRUE;
}

BOOL CAppObject::TermApp(void)
{
	if( CSystemItem::m_pThis ) {

		CSystemItem::m_pThis->SystemTerm();

		g_pPersist->Term();

		TermTasks();

		CSystemItem::m_pThis->SystemStop();

		delete CSystemItem::m_pThis;

		g_pDbase->SetRunning(FALSE);

		if( !xmt ) {

			DeleteTasks();
		}
		else {
			AfxDumpMemory(NULL, xm1);

			DeleteTasks();

			AfxDumpMemory(NULL, xm2);
		}
	}
	else {
		g_pDbase->SetRunning(FALSE);
	}

	return TRUE;
}

BOOL CAppObject::IsRunningControl(void)
{
	if( CSystemItem::m_pThis ) {

		return CSystemItem::m_pThis->IsRunningControl();
	}

	return FALSE;
}

BOOL CAppObject::GetData(DWORD &Data, DWORD Ref)
{
	if( CSystemItem::m_pThis ) {

		IDataServer *pData;

		if( CSystemItem::m_pThis->GetDataServer(pData) ) {

			pData->SetScan(Ref, scanOnce);

			if( pData->IsAvail(Ref) ) {

				Data = pData->GetData(Ref, typeVoid, getNoString);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CAppObject::StratonCall(PCBYTE pIn, PBYTE pOut)
{
	if( CSystemItem::m_pThis ) {

		return Straton_Service(pIn, pOut);
	}

	return FALSE;
}

BOOL CAppObject::RackTunnel(BYTE bDrop, PBYTE pData)
{
	if( CSystemItem::m_pThis ) {

		return ::RackTunnel(bDrop, pData);
	}
		
	return FALSE;
}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	SetThreadName(Def.m_Name);

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskInit(uID);

		return TRUE;
	}

	return TRUE;
}

INT CAppObject::TaskExec(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskExec(uID);

		return 0;
	}

	switch( Def.m_uID ) {

		case 1:
			for( ;;) {

				g_pPxe->KickTimeout(2);

				Sleep(1000);
			}
			break;
	}

	return 0;
}

void CAppObject::TaskStop(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskStop(uID);

		return;
	}
}

void CAppObject::TaskTerm(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskTerm(uID);

		return;
	}
}

// IDiagProvider

UINT CAppObject::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagStop(pOut, pCmd);

		case 2:
			return DiagStart(pOut, pCmd);

		case 3:
			return DiagCycle(pOut, pCmd);

		case 4:
			return DiagRestart(pOut, pCmd);
	}

	return 0;
}

// Diagnostics

BOOL CAppObject::DiagRegister(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		m_uProv = pDiag->RegisterProvider(this, "app");

		pDiag->RegisterCommand(m_uProv, 1, "stop");
		pDiag->RegisterCommand(m_uProv, 2, "start");
		pDiag->RegisterCommand(m_uProv, 3, "cycle");
		pDiag->RegisterCommand(m_uProv, 4, "restart");

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

BOOL CAppObject::DiagRevoke(void)
{
	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		pDiag->RevokeProvider(m_uProv);

		pDiag->Release();

		return TRUE;
	}

	return FALSE;
}

UINT CAppObject::DiagStop(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( !pCmd->FromConsole() ) {

			pOut->Error("must be run from console");

			return 2;
		}

		g_pPxe->SystemStop();

		pOut->Print("system stopped\n");

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CAppObject::DiagStart(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( !pCmd->FromConsole() ) {

			pOut->Error("must be run from console");

			return 2;
		}

		g_pPxe->SystemStart();

		pOut->Print("system started\n");

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CAppObject::DiagCycle(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( !pCmd->FromConsole() ) {

			pOut->Error("must be run from console");

			return 2;
		}

		g_pPxe->SystemStop();

		g_pPxe->SystemStart();

		pOut->Print("system cycled\n");

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CAppObject::DiagRestart(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		if( !pCmd->FromConsole() ) {

			pOut->Error("must be run from console");

			return 2;
		}

		g_pPxe->RestartSystem(0, 55);

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

// Task Management

void CAppObject::GetTaskList(CTaskList &List)
{
	CTaskDef Task;

	Task.m_Name   = "Watchdog";
	Task.m_pEntry = NULL;
	Task.m_uID    = 1;
	Task.m_uCount = 1;
	Task.m_uLevel = 100;
	Task.m_uStack = 0;

	List.Append(Task);

	CSystemItem::m_pThis->GetTaskList(m_List);
}

void CAppObject::CreateTasks(void)
{
	AfxTrace("=== CreateThreads\n");

	m_List.Empty();

	m_Task.Empty();

	GetTaskList(m_List);

	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CTaskDef const &Task = m_List[n];

		for( UINT i = 0; i < Task.m_uCount; i++ ) {

			UINT    uCode = n * 10 + i;

			HTHREAD hTask = CreateClientThread(this, uCode, Task.m_uLevel + i, m_pTms);

			m_Task.Append(hTask);
		}
	}

	WaitTransition();
}

void CAppObject::InitTasks(void)
{
	AfxTrace("=== InitThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		AdvanceThread(m_Task[n]);
	}

	WaitTransition();
}

void CAppObject::StartTasks(void)
{
	AfxTrace("=== StartThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		AdvanceThread(m_Task[n]);
	}

	WaitTransition();
}

void CAppObject::StopTasks(void)
{
	AfxTrace("=== StopThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		DestroyThread(m_Task[n]);
	}

	WaitTransition();
}

void CAppObject::TermTasks(void)
{
	AfxTrace("=== TermThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		AdvanceThread(m_Task[n]);
	}

	WaitTransition();
}

void CAppObject::DeleteTasks(void)
{
	AfxTrace("=== DeleteThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		WaitThread(m_Task[n], FOREVER);

		m_Task[n]->Release();
	}

	m_Task.Empty();

	m_List.Empty();

	AfxTrace("=== Done\n");
}

BOOL CAppObject::WaitTransition(void)
{
	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_pTms->Wait(30000) ) {

			continue;
		}

		// TODO -- This is actually fatal !!!

		AfxTrace("=== Transition Timeout!\n");

		return FALSE;
	}

	AfxTrace("=== Transition Complete (%u)\n", c);

	return TRUE;
}

// Implementation

void CAppObject::FindObjects(void)
{
	AfxGetObject("c3.identity", 0, ICrimsonIdentity, g_pIdentity);

	AfxGetObject("c3.eventstorage", 0, IEventStorage, g_pEvStg);

	AfxGetObject("c3.persist", 0, IPersist, g_pPersist);

	AfxGetObject("c3.database", 0, IDatabase, g_pDbase);

	AfxGetObject("tls", 0, ITlsLibrary, g_pMatrix);
}

void CAppObject::FreeObjects(void)
{
	AfxRelease(g_pMatrix);

	AfxRelease(g_pDbase);

	AfxRelease(g_pPersist);

	AfxRelease(g_pEvStg);

	AfxRelease(g_pIdentity);
}

// End of File
