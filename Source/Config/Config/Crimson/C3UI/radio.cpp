
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Radio Button Group
//

// Runtime Class

AfxImplementRuntimeClass(CUIRadioBase, CUIElement);

// Constructor

CUIRadioBase::CUIRadioBase(void)
{
	m_uFrom       = 0;
	
	m_uTo         = 0;

	m_pTextLayout = NULL;

	m_pTextCtrl   = New CStatic;
	}

// Destructor

CUIRadioBase::~CUIRadioBase(void)
{
	}

// Core Overridables

void CUIRadioBase::OnBind(void)
{
	CUIElement::OnBind();

	m_pText->EnumValues(m_DataText);

	m_Label = GetLabel() + L':';
	}

void CUIRadioBase::OnLayout(CLayFormation *pForm)
{
	m_pMainLayout = MakeLayout();

	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	UINT uCount = m_DataText.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CLayItemText *pData = New CLayItemText(m_DataText[n] + L"XXX", 1, 1);

		CLayFormPad  *pForm = New CLayFormPad (pData, horzLeft | vertCenter);

		m_pMainLayout->AddItem(pForm);

		AddData(pData);
		}

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertTop));

	pForm->AddItem(m_pMainLayout);
	}

void CUIRadioBase::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_uFrom = uID;
	
	m_uTo   = m_uFrom;
	
	UINT uCount = m_DataLayout.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CLayItemText *pDataLayout = m_DataLayout.GetAt(n);

		CButton *pCtrl   = New CButton;

		DWORD   dwStyle  = BS_AUTORADIOBUTTON;

		if( n == 0 ) {

			dwStyle |= WS_GROUP;

			dwStyle |= WS_TABSTOP;
			}

		pCtrl->Create(	m_DataText[n],
				dwStyle,
				pDataLayout->GetRect(),
				Wnd,
				uID++
				);

		pCtrl->SetFont(afxFont(Dialog));

		AddControl(pCtrl, BN_CLICKED);

		AddCtrl(pCtrl);

		m_uTo++;
		}
	
	m_pTextCtrl->Create( m_Label,
			     WS_GROUP,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);
	}

void CUIRadioBase::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	UINT uCount = m_DataLayout.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {
	
		CButton      *pDataCtrl   = m_DataCtrl.GetAt(n);

		CLayItemText *pDataLayout = m_DataLayout.GetAt(n);

		pDataCtrl->MoveWindow(pDataLayout->GetRect(), TRUE);
		}
	}

// Notification Handlers

BOOL CUIRadioBase::OnNotify(UINT uID, UINT uCode)
{
	if( uID >= m_uFrom && uID < m_uTo) {

		if( uCode == BN_CLICKED ) {

			switch( SaveUI(FALSE) ) {

				case saveChange:

					return TRUE;

				case saveSame:

					return FALSE;
				}

			AfxAssert(FALSE);
			}
		}

	return FALSE;
	}

// Data Overridables

void CUIRadioBase::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	UINT c = m_DataText.GetCount();

	for( UINT n = 0; n < c; n++  ) {

		CButton *pDataCtrl = m_DataCtrl[n];

		BOOL     fCheck    = (Data == m_DataText[n]);

		pDataCtrl->SetCheck(fCheck);
		}
	}

UINT CUIRadioBase::OnSave(BOOL fUI)
{
	UINT c = m_DataText.GetCount();

	for( UINT n = 0; n < c; n++  ) {

		CButton *pDataCtrl = m_DataCtrl[n];

		if( pDataCtrl->IsChecked() ) {

			return StdSave(fUI, m_DataText[n]);
			}
		}

	if( m_fMulti ) {

		return saveSame;
		}

	return saveError;
	}

// Implementation

void CUIRadioBase::AddCtrl(CButton *pCtrl)
{
	m_DataCtrl.Append(pCtrl);
	}

void CUIRadioBase::AddData(CLayItemText *pData)
{
	m_DataLayout.Append(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Radio Button Column
//

// Dynamic Class

AfxImplementDynamicClass(CUIRadioCol, CUIRadioBase);

// Constructor

CUIRadioCol::CUIRadioCol(void)
{
	}

// Layout Creation

CLayFormation * CUIRadioCol::MakeLayout(void)
{
	return New CLayFormCol;
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Radio Button Row
//

// Dynamic Class

AfxImplementDynamicClass(CUIRadioRow, CUIRadioBase);

// Constructor

CUIRadioRow::CUIRadioRow(void)
{
	}

// Layout Creation

CLayFormation * CUIRadioRow::MakeLayout(void)
{
	return New CLayFormRow;
	}

// End of File
