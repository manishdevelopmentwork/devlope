
//////////////////////////////////////////////////////////////////////////
//
// DevicetNet Slave
//

enum {
	assyDataSend	= 100,
	assyDataRecv	= 101,
	assyBitSend	= 102,
	assyBitRecv	= 103,
	assyPollSend	= 104,
	assyPollRecv	= 105,
	assyMultiSend	= 106,
	assyMultiRecv	= 107,
	};

//////////////////////////////////////////////////////////////////////////
//
// DevicetNet Slave
//

class CDeviceNetSlave : public CMasterDriver
{
	public:
		// Constructor
		CDeviceNetSlave(void);

		// Destructor
		~CDeviceNetSlave(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersistConnection);

		// Entry Points
		DEFMETH(void)  Service(void);
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data
		IDeviceNetHelper * m_pDevNet;
		BYTE		   m_bMac;
		BOOL		   m_fBit;
		BOOL		   m_fPoll;
		BOOL		   m_fData;
		WORD               m_wBitRecv;
		WORD		   m_wBitSend;
		WORD		   m_wPollRecv;
		WORD		   m_wPollSend;
		WORD		   m_wDataRecv;
		WORD		   m_wDataSend;
		PBYTE		   m_pBitRecv;
		PBYTE		   m_pBitSend;
		PBYTE		   m_pPollRecv;
		PBYTE		   m_pPollSend;
		PBYTE		   m_pDataRecv;
		PBYTE		   m_pDataSend;
		BOOL		   m_fIntel;
		BOOL		   m_fDBit;
		BOOL		   m_fDPoll;
		BOOL		   m_fDData;
		
		// Buffer
		void AllocBuffers(void);
		void FreeBuffers(void);
		void AllocBuffer(PBYTE &pData, UINT uSize);
		void FreeBuffer(PBYTE &pData);

		// Implementation
		PBYTE FindLocale(UINT uTable, UINT& uSize);
		void  DoWrites(void);
		void  DoReads(void);
	};

// End of File
