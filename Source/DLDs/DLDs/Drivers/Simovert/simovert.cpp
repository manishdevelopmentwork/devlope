
#include "intern.hpp"

#include "simovert.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert P via USS Driver
//

// Instantiator

INSTANTIATE(CSimovertDriver);

// Constructor

CSimovertDriver::CSimovertDriver(void)
{
	m_Ident   = DRIVER_ID;

	m_fResend = FALSE;
	}

// Destructor

CSimovertDriver::~CSimovertDriver(void)
{
	}

// Entry Points

CCODE MCALL CSimovertDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	BYTE bType [] = { addrWordAsWord, addrLongAsLong };

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = m_pCtx->m_Ping;
	Addr.a.m_Type   = bType[0];
	Addr.a.m_Extra  = 0;

	m_bRx[0] = 0;

	for( UINT u = 0; u < 10; u++ ) {

		if( !COMMS_SUCCESS( Read(Addr, Data, 1) ) ) {

			Addr.a.m_Type = bType[1];

			if( !COMMS_SUCCESS( Read(Addr, Data, 1 ) ) ) { 

				Addr.a.m_Offset = m_pCtx->m_Ping + u + 1;
				Addr.a.m_Type   = bType[0];

				continue;
				}
			}

		return CCODE_SUCCESS;
		}

	if( m_bRx[0] == STX ) { // received something at some point

		Addr.a.m_Offset = m_pCtx->m_Ping;

		Read(Addr, Data, 1); // try re-synchronize
		}

	return CCODE_ERROR;
	}

// Configuration

void MCALL CSimovertDriver::Load(LPCBYTE pData)
{
	}

void MCALL CSimovertDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}

// Management

void MCALL CSimovertDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSimovertDriver::Open(void)
{

	}

// Device

CCODE MCALL CSimovertDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop	= GetByte(pData);
			m_pCtx->m_wSTW	= 0;
			m_pCtx->m_wHSW	= 0;
			m_pCtx->m_wZSW	= 0;
			m_pCtx->m_wHIW	= 0;
			m_pCtx->m_PZD	= GetByte(pData);
			m_pCtx->m_PKW	= GetByte(pData);
			m_pCtx->m_Model	= GetByte(pData);
			m_pCtx->m_Ping	= m_pCtx->m_Model == MODELREG ? 1 : 0;
			m_pCtx->m_uWriteError = 0;

			memset(m_pCtx->m_wPZD, 0, sizeof(m_pCtx->m_wPZD));

			memset(m_pCtx->m_OUT, 0, elements(m_pCtx->m_OUT) * sizeof(DWORD));

			memset(m_pCtx->m_IN,  0, elements(m_pCtx->m_IN ) * sizeof(DWORD));

			m_pCtx->m_fDIR = FALSE;
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CSimovertDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CSimovertDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nT=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == SP_OUT ) {

		MarkLongs(FALSE, Addr.a.m_Offset, Addr.a.m_Type, uCount);

		return uCount;
		}
	
	if( Addr.a.m_Table == SP_IN ) {

		MarkLongs(TRUE, Addr.a.m_Offset, Addr.a.m_Type, uCount);

		for( UINT u = 0; u < uCount; u++ ) {

			pData[u] = m_pCtx->m_IN[Addr.a.m_Offset - 1 + u];
			}

		return uCount;
		}

	MakeMin(uCount, 1);

	if ( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Offset == FIELD_ZSW ) {

			pData[0] = MAKELONG(m_pCtx->m_wZSW, 0);

//**/			AfxTrace1(" - ZSW %8.8lx ", pData[0]);
			}

		if( Addr.a.m_Offset == FIELD_HIW ) {

			pData[0] = MAKELONG(m_pCtx->m_wHIW, 0);

//**/			AfxTrace1(" - HIW %8.8lx ", pData[0]);
			}

		return uCount;
		}

	if( Addr.a.m_Table == SP_PZ ) { // get cached process data value

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = m_pCtx->m_wPZD[Addr.a.m_Offset + n];
			}

		return uCount;
		}

	if( m_pCtx->m_Model == MODELREG && !Addr.a.m_Offset ) {

		return 1;
		}

	if ( m_pCtx->m_bDrop == CMD_BROADCAST ) {

//**/		AfxTrace0(" - BROADCAST ");

		return uCount;
		}

	m_fIsArray = CheckArray(Addr.a.m_Table);

	BYTE bLen  = 6;

	BOOL fLong = IsLong(Addr.a.m_Type);

	StartFrame(bLen + m_pCtx->m_PZD * 2 + (fLong ? 4 : 2));

	UINT uPKE = FindBasePNU(Addr);

	uPKE |= (m_fIsArray ? CMD_R_ARRAY : CMD_READ) << 12;

	AddWord(uPKE);			// PKE

	UINT uIND = FindExtPNU(Addr);

	AddWord(uIND);			// IND

	if( IsLong(Addr.a.m_Type) ) {

		AddDWord(0x0);		// VALUE
		}
	else {
		AddWord(0x0);		// VALUE
		}

	if( m_pCtx->m_PZD ) {

		AddWord(0x0);		// STW
		}

	if( m_pCtx->m_PZD >= 2 ) {

		AddWord(0x0);		// HSW

		AddPZDCache();
		}

	TxFrame();

	if( RxFrame() && m_bLen >= bLen ) {

		switch( CheckReply(Addr) ) {

			case 1:
//**/				AfxTrace1("RE-SEND %x ", m_fResend ? 0xFFFFFFFF : 0);

				if( !m_fResend ) {

					m_fResend = TRUE;

					if( Read(Addr, pData, 1) == 1 ) {

						m_fResend = FALSE;

						return 1;
						}
					}

				m_fResend = FALSE;

				return CCODE_ERROR | CCODE_NO_DATA;

			case 2:
				pData[0] = IsLong(Addr.a.m_Type) ? ReadDWord() : ReadWord();

				if( IsDirectional() ) {

					ReadIN();
					}
				else {

					ReadPZD();
					}

				m_fResend = FALSE;

				return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSimovertDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nW***** T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == SP_IN ) {

		MarkLongs(TRUE, Addr.a.m_Offset, Addr.a.m_Type, uCount);

		return uCount;
		}
	
	if( Addr.a.m_Table == SP_OUT ) {

		MarkLongs(FALSE, Addr.a.m_Offset, Addr.a.m_Type, uCount);

		for( UINT u = 0; u < uCount; u++ ) {

			m_pCtx->m_OUT[Addr.a.m_Offset - 1 + u] = pData[u];

			if( Addr.a.m_Offset - 1 + u == 0 ) {

				m_pCtx->m_wHSW = pData[u];
				}
			}

		return uCount;
		}

	if ( Addr.a.m_Table == addrNamed ) {

//**/		AfxTrace0(" - Command ");

		return WriteCommand(Addr, pData, uCount);
		}

	if( m_pCtx->m_Model == MODELREG && !Addr.a.m_Offset ) {

		return 1;
		}

	if( Addr.a.m_Table == SP_PZ ) {

		for( UINT n = 0; n < uCount; n++ ) {

			m_pCtx->m_wPZD[Addr.a.m_Offset + n] = LOWORD(pData[n]);
			}

		return uCount;
		}

	m_fIsArray = CheckArray(Addr.a.m_Table);

	MakeMin(uCount, 1);

	BYTE bLen = 6;

	BOOL fLong = IsLong(Addr.a.m_Type);

	for( UINT uTry = 0; uTry < 2; uTry++ ) {

		StartFrame(bLen + m_pCtx->m_PZD * 2 + (fLong ? 4 : 2));

		UINT uPKE = FindBasePNU(Addr);

		uPKE |= (GetWriteCmd(Addr.a.m_Extra, fLong && !uTry) << 12);

		AddWord(uPKE);				// PKE

		UINT uIND = FindExtPNU(Addr);

		AddWord(uIND);				// IND

		if( fLong ) {

			AddDWord(pData[0]);		// VAL
			}
		else{	
			AddWord(LOWORD(pData[0]));	// VAL
			}

		if( m_pCtx->m_PZD ) {

			AddWord(0x0);			// STW
			}

		if( m_pCtx->m_PZD >= 2 ) {

			AddWord(FNC_NONE);		// HSW

			AddPZDCache();
			}

		TxFrame();

		if ( m_pCtx->m_bDrop == CMD_BROADCAST ) {

			while( m_pData->Read(0) < NOTHING );

			m_pCtx->m_uWriteError = 0;

			return uCount;
			}

		if( RxFrame() && m_bLen >= bLen ) {

			uPKE = ReadWord();

			uPKE = (uPKE & 0xF000) >> 12;

			BOOL fOk = FALSE;

			if( !m_fIsArray ) {

				fOk = uPKE == REPLY_WORD   || uPKE == REPLY_DWORD;
				}

			else {
				fOk = uPKE == REPLY_ARRAY4 || uPKE == REPLY_ARRAY5;
				}

			if( fOk ) {

				m_pCtx->m_uWriteError = 0;

				return uCount;
				}
			}

		if( m_pCtx->m_uWriteError++ >= 3 ) {

			m_pCtx->m_uWriteError = 0;

			return 1;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSimovertDriver::WriteCommand(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 1);

	if( Addr.a.m_Offset == FIELD_STW ) {

		m_pCtx->m_wSTW = LOWORD(pData[0]);

		m_pCtx->m_OUT[0] = MAKELONG(m_pCtx->m_wSTW, 0);

//**/		AfxTrace1(" - STW %x ", m_pCtx->m_wSTW);

		return uCount;
		}

	if( Addr.a.m_Offset == FIELD_HSW ) {

		m_pCtx->m_wHSW = LOWORD(pData[0]);

		m_pCtx->m_OUT[1] = MAKELONG(m_pCtx->m_wHSW, 0);

//**/		AfxTrace1(" - HSW %x ", m_pCtx->m_wHSW);

		return uCount;
		}

	if( Addr.a.m_Offset == FIELD_ZSW ) {

		pData[0] = MAKELONG(m_pCtx->m_wZSW, 0);

//**/		AfxTrace1(" - ZSW %x ", pData[0]);

		return uCount;
		}

	if( Addr.a.m_Offset == FIELD_HIW ) {

		pData[0] = MAKELONG(m_pCtx->m_wHIW, 0);

//**/		AfxTrace1(" - HIW %x ", pData[0]);

		return uCount;
		}

	if( pData[0] == 0 ) {

		return uCount;
		}

	BYTE bLen = 2;

	if( m_pCtx->m_PKW < PKW_VAR_NO ) {

		bLen += 6;
		}

	StartFrame(bLen + m_pCtx->m_PZD * 2);

	UINT uPZD = m_pCtx->m_PZD;

	if( m_pCtx->m_PKW < PKW_VAR_NO ) {

		AddWord(0x0);				// PKE

		AddWord(0x0);				// IND

		AddWord(0x0);				// VAL

		if( uPZD ) {

			AddWord(m_pCtx->m_wSTW);	// STW
			}
		}

	if( uPZD ) {

		if( IsDirectional() ) {

			AddOUTCache();
			}

		else if( uPZD >= 2 ) {

			AddWord(m_pCtx->m_wHSW);	// HSW

			AddPZDCache();
			}
		}

	TxFrame();

	if ( m_pCtx->m_bDrop == CMD_BROADCAST ) {

//**/		AfxTrace0(" - BROADCAST ");

		while( m_pData->Read(0) < NOTHING );

		return uCount;
		}

	if( RxFrame() && m_bLen >= bLen - 2 ) {

		UINT uPKE = ReadWord();

		uPKE = (uPKE & 0xF000) >> 12;

		m_Ptr += 2;

		if( uPKE == REPLY_NONE ) {

			if( IsDirectional() ) {

				ReadIN();
				}
			else {
				ReadPZD();
				}
			
			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Frame Building

void CSimovertDriver::StartFrame(BYTE bLen)
{
	m_Ptr = 0;

	AddByte(bLen);

	AddDrop();
	}

void CSimovertDriver::AddByte(BYTE bData)
{
	UINT uSize = sizeof(m_bTx);

	if( m_Ptr < uSize ) {

		m_bTx[m_Ptr ++] = bData;
		}
	}

void CSimovertDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CSimovertDriver::AddDWord(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CSimovertDriver::AddDrop(void)
{
	AddByte(m_pCtx->m_bDrop);
	}

void CSimovertDriver::AddPZDCache(void)
{
	for( UINT n = 3; n <= m_pCtx->m_PZD; n++ ) {

		AddWord(m_pCtx->m_wPZD[n]);
		}
	}

void CSimovertDriver::AddOUTCache(void)
{
	UINT uEnd = elements(m_pCtx->m_OUT) - 1;
	
	for( UINT u = 0; u < m_pCtx->m_PZD; u++ ) {

		if( (m_pCtx->m_OUT[uEnd] >> u) & 0x1 ) {

			AddDWord(m_pCtx->m_OUT[u]);

			u++;
			
			continue;
			}

		AddWord(m_pCtx->m_OUT[u]);
		}
	}

BYTE CSimovertDriver::ReadByte(void)
{
	return m_bRx[m_Ptr ++];
	}

WORD CSimovertDriver::ReadWord(void)
{
	BYTE bHi = ReadByte();

	BYTE bLo = ReadByte();

	return MAKEWORD(bLo, bHi);
	}

DWORD CSimovertDriver::ReadDWord(void)
{
	WORD wHi = ReadWord();

	WORD wLo = ReadWord();

	return MAKELONG(wLo, wHi);
	}

void CSimovertDriver::ReadPZD(void)
{
	if( m_Ptr + 2 <= m_bLen ) {

		m_pCtx->m_wZSW = ReadWord();

		if( m_Ptr + 2 <= m_bLen ) {

			m_pCtx->m_wHIW = ReadWord();
			}

		for( UINT n = 3; n <= m_pCtx->m_PZD; n++ ) {

			if( m_Ptr + 2 <= m_bLen ) {

				m_pCtx->m_wPZD[n] = ReadWord();
				}

			else return;
			}
		}
	}

void CSimovertDriver::ReadIN(void)
{
	UINT uEnd = elements(m_pCtx->m_IN) - 1;
	
	if( m_Ptr + 2 <= m_bLen ) {

		if( m_pCtx->m_IN[uEnd] & 0x1 ) {

			m_pCtx->m_IN[0] = ReadDWord();

			m_pCtx->m_wZSW = HIWORD(m_pCtx->m_IN[0]);
			}
		else {
			m_pCtx->m_wZSW = ReadWord();

			m_pCtx->m_IN[0]= MAKELONG(m_pCtx->m_wZSW, 0);
			}		

		if( m_Ptr + 2 <= m_bLen ) {

			if( m_pCtx->m_IN[uEnd] & 0x1 ) {

				m_pCtx->m_IN[1] = ReadDWord();

				m_pCtx->m_wHIW  = HIWORD(m_pCtx->m_IN[1]);
				}
			else {
				m_pCtx->m_wHIW = ReadWord();

				m_pCtx->m_IN[1]= MAKELONG(m_pCtx->m_wHIW, 0);
				}
			}

		for( UINT u = 2; u < m_pCtx->m_PZD; u++ ) {

			BOOL fLong  = (m_pCtx->m_IN[uEnd] >> u) & 0x1;
			
			UINT uBytes = fLong ? sizeof(DWORD) : sizeof(WORD);
		
			if( m_Ptr + uBytes <= m_bLen ) {

				if( fLong ) {

					m_pCtx->m_IN[u] = ReadDWord();

					u++;

					continue;
					}

				m_pCtx->m_IN[u] = ReadWord();
				}
			else			
				break;
			}
		}
	}

// Transport Layer

BOOL CSimovertDriver::TxFrame(void)
{
	ClearCheck();

	m_pData->ClearRx();

	Sleep(10);

	m_pData->Write(STX, FOREVER);

	AddToCheck(STX);

	for( UINT n = 0; n < m_Ptr; n ++ ) {

		BYTE b = m_bTx[n];

		m_pData->Write(b, FOREVER);

		AddToCheck(b);
		}

	SendCheck();

	return TRUE;
	}

BOOL CSimovertDriver::RxFrame(void)
{
	UINT uState = 0;

	BYTE bCheck;

	BYTE bDrop;

	UINT uTimer = 0;

	UINT uWord = 0;

	UINT uSize = sizeof(m_bRx);

	SetTimer(1000);

	while( (uTimer = GetTimer()) ) {

		if ( ( uWord = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		switch( uState ) {

			case 0:
				if( uWord == STX ) {

					ClearCheck();

					AddToCheck(STX);

					uState = 1;
					}
				break;

			case 1:
				AddToCheck(BYTE(uWord));

				m_bLen = BYTE(uWord) - 2;

				uState = 2;

				break;

			case 2:
				AddToCheck(BYTE(uWord));

				bDrop = BYTE(uWord);

				if( bDrop == m_pCtx->m_bDrop ) {

					m_Ptr  = 0;

					uState = 3;

					break;
					}

				return FALSE;

			case 3:
				AddToCheck(BYTE(uWord));

				m_bRx[m_Ptr ++] = uWord;

				if( m_Ptr == uSize ) {

					return FALSE;
					}

				if( m_Ptr == m_bLen ) {

					uState = 4;

					m_Ptr  = 0;
					}
				break;

			case 4:
				bCheck = uWord;

				return bCheck == m_bBcc;
			}
		}

	return FALSE;
	}

// Checksum Handler

void CSimovertDriver::ClearCheck(void)
{
	m_bBcc = 0;
	}

void CSimovertDriver::AddToCheck(BYTE b)
{
	m_bBcc ^= b;
	}

void CSimovertDriver::SendCheck(void)
{
	m_pData->Write(m_bBcc, FOREVER);

//**/	AfxTrace1("[%2.2x]", m_bBcc);
	}

// Helpers

UINT CSimovertDriver::FindBasePNU(AREF Addr)
{
	return ParseAddr(Addr) % EXTSTART;
	}

UINT CSimovertDriver::FindExtPNU(AREF Addr)
{
	UINT uAddr = ParseAddr(Addr);

	if( m_pCtx->m_Model == MODELREG ) {

		return GetInxIndex(Addr.a.m_Offset) + (uAddr > EXTSTART - 1 ? EXTREGPNU : 0);
		}

	UINT uPage = uAddr / EXTSTART;

	BOOL fLite = uPage & 0x1;

	uPage = uPage >> 1;

	uPage |= fLite << 3;

	return uPage;
	}

BOOL CSimovertDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:

			if( m_pCtx->m_PKW > PKW_FIXED3 ) {

				return TRUE;
				}

			break;
		}

	return FALSE;
	}

UINT CSimovertDriver::FindReplyID(UINT uNum, UINT uIND)
{
	if( m_pCtx->m_Model == MODELREG ) {

		if( uIND & EXTREGPNU ) {

			uNum += EXTSTART;
			}

		return uNum;
		}

	uIND     = (uIND & 0xF000) >> 12;

	BYTE Ext = ( (uIND & 0x8) ? 1 : 0 ) | (uIND << 1);

	return uNum + (uIND & 0xF) * EXTSTART;
	}

BOOL CSimovertDriver::CheckArray(UINT uTable)
{
	return m_pCtx->m_Model == MODELREG && uTable >= SP_I0 && uTable <= SP_L2;
	}

UINT CSimovertDriver::CheckReply(AREF Addr)
{
	UINT uPKE   = ReadWord();

	UINT uIND   = ReadWord();

	UINT uBase  = ParseAddr(Addr);

	UINT uCode  = (uPKE & 0xF000) >> 12;

	if( !CheckReplyCode(uCode) ) return 0;

	if( FindReplyID(uPKE & 0x7FF, uIND) != uBase )	return 1;

	if( !CheckReplyType(uCode, IsLong(Addr.a.m_Type)) ) return 1;

	return CheckIndex(Addr.a.m_Offset, uIND) ? 2 : 1;
	}

BOOL CSimovertDriver::CheckReplyCode(UINT uPKE)
{
	switch( uPKE ) {

		case REPLY_WORD:
		case REPLY_DWORD:
		case REPLY_ARRAY4:
		case REPLY_ARRAY5:

			return TRUE;
		}

	return FALSE;
	}

BOOL CSimovertDriver::CheckReplyType(UINT uPKE, BOOL fLong)
{
	BYTE b              = fLong ? 1 : 0;

	if( m_fIsArray ) b += 2;

	switch( b ) {

		case 1: return uPKE == REPLY_DWORD || uPKE == REPLY_WORD;
		case 2: return uPKE == REPLY_ARRAY4;
		case 3: return uPKE == REPLY_ARRAY5;
		}

	return uPKE == REPLY_WORD;
	}

BOOL CSimovertDriver::CheckIndex(UINT uOffset, UINT uIND)
{
	return !m_fIsArray || (GetInxIndex(uOffset) == GetInxIndex(uIND));
	}

UINT CSimovertDriver::GetWriteCmd(UINT uExtra, BOOL fLong)
{
	BYTE b              = uExtra & 1;
	if( fLong      ) b += 2;
	if( m_fIsArray ) b += 4;

	switch( b ) {

		case 1: return CMD_EEPROM;
		case 2: return CMD_WRITE_LONG;
		case 3: return CMD_EEPROM_LONG;
		case 4: return CMD_W_ARRAY_16;
		case 5: return CMD_W_A2EEP_16;
		case 6: return CMD_W_ARRAY_32;
		case 7: return CMD_W_A2EEP_32;
		}

	return CMD_WRITE;
	}

UINT CSimovertDriver::ParseAddr(AREF Addr)
{
	if( m_fIsArray ) {

		UINT uRtn = Addr.a.m_Offset >> 7;

		if( Addr.a.m_Extra & 2 ) uRtn += 500;

		switch( Addr.a.m_Table ) {

			case SP_I1:
			case SP_L1:
				return uRtn + 1000;

			case SP_I2:
			case SP_L2:
				return uRtn + 2000;
			}

		return uRtn;
		}

	return Addr.a.m_Offset;
	}

UINT CSimovertDriver::GetInxIndex(UINT uOffset)
{
	return uOffset & 0x3F;
	}

BOOL CSimovertDriver::IsDirectional(void)
{
	return m_pCtx->m_fDIR;
	} 

void CSimovertDriver::MarkLongs(BOOL fIn, UINT uOffset, UINT uType, UINT uCount)
{
	m_pCtx->m_fDIR = TRUE;

	UINT uEnd = elements(m_pCtx->m_IN) - 1;

	if( fIn ) {

		for( UINT x = 0; x < uCount; x++ ) {

			if( uType == addrWordAsLong ) {

				m_pCtx->m_IN[uEnd] |= (1 << (uOffset - 1 + x));
				}
			}
				
		return;
		}

	uEnd = elements(m_pCtx->m_OUT) - 1;

	for( UINT y = 0; y < uCount; y++ ) {

		if( uType == addrWordAsLong ) {

			m_pCtx->m_OUT[uEnd] |= (1 << (uOffset - 1 + y));
			}
		}
      	}

// End of File
