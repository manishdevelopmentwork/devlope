
#include "Intern.hpp"

#include "CryptoCipherRc2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RC2 Symmetric Cipher
//

// Instantiator

static IUnknown * Create_CryptoCipherRc2(PCTXT pName)
{
	return New CCryptoCipherRc2;
	}

// Registration

global void Register_CryptoCipherRc2(void)
{
	piob->RegisterInstantiator("crypto.cipher-rc2", Create_CryptoCipherRc2);
	}

// Constructor

CCryptoCipherRc2::CCryptoCipherRc2(void)
{
	}

// ICryptoHash

CString CCryptoCipherRc2::GetName(void)
{
	return "rc2";
	}

void CCryptoCipherRc2::Initialize(PCBYTE pPass, UINT uPass)
{
	BYTE bVec[8];

	BYTE bKey[12];

	MakeBytes(bVec, sizeof(bVec), pPass, uPass);

	MakeBytes(bKey, sizeof(bKey), pPass, uPass);

	psRc2Init(&m_Ctx, bVec, bKey, sizeof(bKey));
	}

void CCryptoCipherRc2::Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psRc2Encrypt(&m_Ctx, pIn, pOut, uSize);
	}

void CCryptoCipherRc2::Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psRc2Decrypt(&m_Ctx, pIn, pOut, uSize);
	}

void CCryptoCipherRc2::Finalize(void)
{
	memset(&m_Ctx, 0, sizeof(m_Ctx));
	}

// End of File
