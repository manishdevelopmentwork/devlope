
#include "ml1tcpt.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP/IP Tester Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CML1Tester::CML1Tester(void)
{
	m_pExtra = NULL;
	}	

// Destructor

CML1Tester::~CML1Tester(void)
{
	m_pExtra->Release();
	}

// Configuration

void MCALL CML1Tester::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Server	= 0;

		m_AuthInterval	= GetLong(pData) * 1000;
		}

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// User Access

UINT MCALL CML1Tester::DrvCtrl(UINT uFunc, PCTXT Value)
{
	UINT uValue = ATOI(Value);

	switch( uFunc ) {

		case 1:	return m_Server;

		case 2: return m_Unit;

		case 3: return m_Authority;

		case 4: m_Server     = uValue & 0xFF;
			m_fPush      = TRUE;
			m_fAuthentic = FALSE;
			return 1;

		case 5: if( uValue <= 0xFFFFFF - 101 ) {

				m_Unit       = uValue;
				m_fPush      = TRUE;
				m_fAuthentic = FALSE;
				return 1;
				}

			return 0;

		case 6: m_Authority  = uValue;
			m_fPush	     = TRUE;
			m_fAuthentic = FALSE;
			return 1;
		}

	return 0;
	}

UINT MCALL CML1Tester::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CML1Dev * pDev = (CML1Dev *) pContext;

	if( pDev ) {

		UINT uValue = ATOI(Value);

		switch( uFunc ) {

			case 1:	return pDev->m_Server;

			case 2: return pDev->m_Unit;

			case 3: return pDev->m_Authority;

			case 4: pDev->m_Server     = uValue & 0xFF;
				pDev->m_fPush      = TRUE;	
				pDev->m_fOnline    = FALSE;
				pDev->m_fAuthentic = FALSE;
				return 1;

			case 5: if( uValue <= 0xFFFFFF - 101 ) {

					pDev->m_Unit	   = uValue;
					pDev->m_fPush	   = TRUE;
					pDev->m_fOnline    = FALSE;
					pDev->m_fAuthentic = FALSE;
					return 1;
					}

				return 0;

			case 6: pDev->m_Authority  = uValue;
				pDev->m_fPush	   = TRUE;
				pDev->m_fOnline	   = FALSE;
				pDev->m_fAuthentic = FALSE;
				return 1;
			}
		}

	return 0;
	}

// Entry Points

void MCALL CML1Tester::Service(void)
{
	// Server Servicing

	Recv();

	Listen();

	CML1Dev * pDev = m_pHead;
	
	while( pDev ) {

		if( (IsAuthenticated(pDev) || IsBroadcast(pDev->m_Server)) && pDev->m_fOnline ) {

			CML1Blk * pBlock = pDev->m_pHead;

			while( pBlock ) {

				BOOL fSend = pBlock->Execute(m_pExtra);

				for( UINT o = opRead; o < opWrite; o++ ) {

					if( pBlock->Request(o, fSend) ) {

						SendBlockRequest(pDev, pBlock, o);

						pBlock->SetNext(o);

						fSend = FALSE;
						}
					}

				Listen();

				ForceSleep(100);

				pBlock = pBlock->m_pNext;
				}
			}

		pDev = pDev->m_pNext;
		}
	}

CCODE MCALL CML1Tester::Ping(void)
{
	if( GetMacId() ) {

		if( IsAuthenticated(m_pDev) ) {

			m_pDev->m_fOnline = COMMS_SUCCESS(CMasterDriver::Ping());

			return m_pDev->m_fOnline ? CCODE_SUCCESS : CCODE_ERROR;
			}

		PshAuthentication(m_pDev);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CML1Tester::Read(AREF Address, PDWORD pData, UINT uCount)
{
	if( IsBroadcast(m_pDev->m_Server) ) {

		return CCODE_SUCCESS | CCODE_NO_DATA;
		}

	if( !IsAuthenticated(m_pDev) || !m_pDev->m_fOnline ) {

		return CCODE_ERROR;
		}

	CAddress Addr;

	Addr.m_Ref = Address.m_Ref - 1;

	CML1Blk * pBlock = CML1Base::FindBlock(m_pDev, Addr);

	if( pBlock ) {

		pBlock->Mark(opRead);

		UINT uReturn = pBlock->GetData(Addr, pData, uCount);

		if( pBlock->IsTimedOut(opRead, modeReply) ) {

			if( pBlock->IsExhausted(opRead) ) {

				return CCODE_ERROR;
				}
			}

		GetEntitySize(Addr, uReturn);

		MakeMaxCount(Addr, uReturn);

		if( uReturn < uCount ) {

			pBlock->SetNext(opWrite);
			}

		return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CML1Tester::Write(AREF Address, PDWORD pData, UINT uCount)
{
	if( (!IsAuthenticated(m_pDev) && !IsBroadcast(m_pDev->m_Server)) || !m_pDev->m_fOnline ) {

		return CCODE_ERROR;
		}

	CAddress Addr;

	Addr.m_Ref = Address.m_Ref - 1;

	BOOL fSpecial = IsSpecial(Addr);

	if( fSpecial ) {

		// It was decided to use Authentication messages when changing special registers...

		PWORD pWord = PWORD(pData);

		for( UINT u = Addr.a.m_Offset - SR_MIN, c = 0; c < uCount; c++, u++, pWord++ ) {

			if( IsLong(Addr) ) {
				
				SetSr(m_pDev, u, &PWORD(pData)[c]);
				
				pWord++;

				continue;
				}

			pWord++;

			SetSr(m_pDev, u, pWord);
			}

		if( FrcAuthentication(m_pDev, FindSpecialWriteCount(Addr.a.m_Offset - SR_MIN) ) ) {

			return 1;
			}

		// Instead of direct writes...

		/*

		if( IsLong(Addr) ) {

			if( DoLongWrite(m_pDev, Addr, PBYTE(pData), 1 ) ) {

				return 1;
				}

			return CCODE_ERROR;
			}

		PBYTE pBytes = PBYTE(alloca(uCount * 2));

		for( UINT u = 0; u < uCount; u++ ) {

			PWORD(pBytes + 0)[u] = LOWORD(pData[u]);
			}
		
		if( DoWordWrite(m_pDev, Addr, PBYTE(pBytes), 1) ) {

			return 1;
			}*/

		return CCODE_ERROR;
		}

	CML1Blk * pBlock = CML1Base::FindBlock(m_pDev, Addr);

	if( pBlock ) {

		pBlock->Mark(opWrite);

		UINT uReturn = pBlock->SetData(Addr, pData, uCount);

		if( pBlock->SendOnChange() ) {

			UINT Offset = 0;

			UINT Count  = uReturn;

			if( pBlock->FindOpParams(Addr, Offset, Count) ) {

				pBlock->ClrPend(opWrite);
				
				if( DoBlockWrite(m_pDev, pBlock, Offset, Count) ) {

					pBlock->SetTxTime(modeReqst);

					if( IsBroadcast(m_pDev->m_Server) ) {

						UINT uSent = Count;

						while( uSent < pBlock->GetSize() ) {

							UINT uNext = pBlock->GetNext(Offset + uSent, opWrite); 

							if( uNext ) {

								Count = pBlock->GetSize() - uSent;

								if( DoBlockWrite(m_pDev, pBlock, uNext, Count ) ) {

									uSent += Count;

									Sleep(100);
									}
								}
							}
						
						pBlock->MarkComplete(opWrite, modeReply);

						return uSent;
						}

					GetEntitySize(Addr, uReturn);

					MakeMaxCount(Addr, uReturn);

					if( uReturn < uCount ) {

						pBlock->SetNext(opWrite);
						}

					return uCount;
					}
				}
			}

		if( pBlock->IsTimedOut(opWrite, modeReply) ) {

			if( pBlock->IsExhausted(opWrite) ) {

				return CCODE_ERROR;
				}
			}

		return uReturn;
		}
	
	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL CML1Tester::SendBlockRequest(CML1Dev * pDev, CML1Blk * pBlock, BYTE bOp)
{
	if( pBlock ) {

		if( bOp == opRead ) {

			if( DoBlockRead(pDev, pBlock) ) {

				pBlock->SetRxTime(modeReqst);

				return TRUE;
				}

			return FALSE;
			}

		if( bOp == opWrite ) {

			if( DoBlockWrite(pDev, pBlock) ) {

				pBlock->SetTxTime(modeReqst);

				if( IsBroadcast(m_pDev->m_Server) ) {
					
					pBlock->MarkComplete(opWrite, modeReply);
					}			

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CML1Tester::DoBlockRead(CML1Dev * pDev, CML1Blk * pBlock)
{
	BOOL fOk = FALSE;

	ShowDebug("\nDo CML1Blk Read ");

	if( pBlock ) {

		AREF Addr  = pBlock->GetAddr();

		UINT uSize = pBlock->GetSize();

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	fOk = DoBitRead (pDev, Addr, uSize);	break;
			case addrWordAsWord:	fOk = DoWordRead(pDev, Addr, uSize);	break;
			case addrWordAsLong:
			case addrWordAsReal:	fOk = DoLongRead(pDev, Addr, uSize);	break;
			}
		}

	if( fOk ) {

		pBlock->IncPend(opRead);
		}
		
	return fOk;
	}

BOOL CML1Tester::DoBlockWrite(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset, UINT uCount)
{
	BOOL fOk = FALSE;

	if( pBlock ) {

		CAddress Addr;
		
		Addr.m_Ref = pBlock->GetAddr().m_Ref;

		UINT uSize = pBlock->GetSize();

		if( uCount ) {

			MakeMin(uSize, uCount);
			}

		Addr.a.m_Offset += uOffset;
		
		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	fOk = DoBitWrite (pDev, Addr, pBlock->m_pTx + uOffset, uSize);	break;

			case addrWordAsWord:	uOffset *= sizeof(WORD);

						fOk = DoWordWrite(pDev, Addr, pBlock->m_pTx + uOffset, uSize);	break;

			case addrWordAsLong:
			case addrWordAsReal:	uOffset *= sizeof(WORD);

						fOk = DoLongWrite(pDev, Addr, pBlock->m_pTx + uOffset, uSize);	break;
				
			}		
		}

	if( fOk ) {

		pBlock->IncPend(opWrite);
		}
	
	return fOk;
	}

// Read Handlers

BOOL CML1Tester::DoWordRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileRead(pDev, Addr, uCount);
		}

	UINT uSent = 0;
	
	while( uSent < uCount ) {

		switch( Addr.a.m_Table ) {
	
			case spaceHR:
			
				StartFrame(pDev, 0x03);
			
				break;
			
			case spaceAI:
					
				StartFrame(pDev, 0x04);
			
				break;
			
			default:
			
				return FALSE;
			}

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddWord(Addr.a.m_Offset + uSent);
	
		AddWord(uSend);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CML1Tester::DoLongRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileRead(pDev, Addr, uCount);
		}

	UINT uSent = 0;

	while( uSent < uCount ) {

		switch( Addr.a.m_Table ) {
	
			case spaceHR:
			
				StartFrame(pDev, 0x03);
			
				break;
			
			case spaceAI:
					
				StartFrame(pDev, 0x04);
			
				break;
			
			default:
				return FALSE;
			}

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddWord(Addr.a.m_Offset + uSent * 2);
	
		AddWord(uSend * 2);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	} 

BOOL CML1Tester::DoBitRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	UINT uSent = 0;
	
	while( uSent < uCount ) {

		switch( Addr.a.m_Table ) {

			case spaceDC:
			
				StartFrame(pDev, 0x01);
			
				break;
			
			case spaceDI:
			
				StartFrame(pDev, 0x02);
			
				break;

			default:
				return FALSE;
			}

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddWord(Addr.a.m_Offset + uSent);
	
		AddWord(uSend);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CML1Tester::DoFileRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	UINT uSent = 0;

	UINT uMult = IsLong(Addr) ? 2 : 1;

	UINT uFile = CML1Base::GetFileNumber(Addr);

	UINT uRec  = CML1Base::GetFileRecord(Addr);
	
	while( uSent < uCount ) {

		StartFrame(pDev, 0x14);
		
		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);

		AddByte(0x9);	

		AddByte(0x6);	

		AddWord(uFile);

		AddWord(uRec + uSent * uMult);

		AddWord(uSend * uMult);

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}

	return TRUE;
	}

// Write Handlers

BOOL CML1Tester::DoWordWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileWrite(pDev, Addr, pData, uCount);
		}

	if( Addr.a.m_Table == spaceHR || Addr.a.m_Table == spaceSR ) {

		if( uCount == 1 ) {
			
			StartFrame(pDev, 6);
			
			AddWord(Addr.a.m_Offset);
			
			AddWord(PWORD(pData)[0]);
			}
		else {
			UINT uSent = 0;

			while( uSent < uCount ) {

				UINT uSend = uCount - uSent;

				MakeMaxCount(Addr, uSend);
			
				StartFrame(pDev, 16);
			
				AddWord(Addr.a.m_Offset + uSent);
			
				AddWord(uSend);
			
				AddByte(uSend * 2);
				
				for( UINT n = uSent; n < uSent + uSend; n++ ) {
				
					AddWord(PWORD(pData)[n]);
					}

				if( Send() ) {

					return TRUE;

					//uSent += uSend;

					//Sleep(100);

					//continue;
					}

				return FALSE;
				}

			return TRUE;
			}

		return Send();
		}

	return FALSE;
	}

BOOL CML1Tester::DoLongWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileWrite(pDev, Addr, pData, uCount);
		}

	if( Addr.a.m_Table == spaceHR || Addr.a.m_Table == spaceSR ) {

		ShowDebug("\nWrite LONG");

		UINT uSent = 0;

		while( uSent < uCount ) {

			UINT uSend = uCount - uSent;

			MakeMaxCount(Addr, uSend);

			StartFrame(pDev, 16);
		
			AddWord(Addr.a.m_Offset + uSent * 2);
		
			AddWord(uSend * 2);
		
			AddByte(uSend * 4);

			for( UINT n = uSent; n < uSent + uSend; n++ ) {

				AddLong(PDWORD(pData)[n]);
				}

			if( Send() ) {

				return TRUE;

				//uSent += uSend;

				//Sleep(100);

				//continue;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CML1Tester::DoBitWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( Addr.a.m_Table == spaceDC ) {

		if( uCount == 1 ) {
			
			StartFrame(pDev, 5);
			
			AddWord(Addr.a.m_Offset);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			UINT uSent = 0;

			while( uSent < uCount ) {

				UINT uSend = uCount - uSent;

				MakeMaxCount(Addr, uSend);
			
				StartFrame(pDev, 15);
			
				AddWord(Addr.a.m_Offset + uSent);
			
				AddWord(uSend);
			
				AddByte((uSend + 7) / 8);
				
				UINT b = 0;

				BYTE m = 1;

				for( UINT n = uSent; n < uSent + uSend; n++ ) {

					if( pData[n] ) {
					
						b |= m;
						}

					if( !(m <<= 1) ) {

						AddByte(b);

						b = 0;

						m = 1;
						}
					}

				if( m > 1 ) {

					AddByte(b);
					}

				if( Send() ) {

					return TRUE;

					//uSent += uSend;

					//Sleep(100);

					//continue;
					}

				return FALSE;
				}

			return TRUE;
			}
			
		return Send();
		}

	return FALSE;
	}

BOOL CML1Tester::DoFileWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	UINT uSent = 0;

	BOOL fLong = IsLong(Addr);

	UINT uMult = fLong ? 2 : 1;

	UINT uFile = CML1Base::GetFileNumber(Addr);

	UINT uRec  = CML1Base::GetFileRecord(Addr);

	while( uSent < uCount ) {

		UINT uSend = uCount - uSent;

		MakeMaxCount(Addr, uSend);
			
		StartFrame(pDev, 0x15);

		AddByte(10 + uSend * 2 * uMult);

		AddByte(6);

		AddWord(uFile);

		AddWord(uRec + uSent * uMult);
			
		AddWord(uSend * uMult);
			
		AddByte(uSend * uMult * 2);
				
		for( UINT n = uSent; n < uSent + uSend; n++ ) {
				
			fLong ? AddLong(PDWORD(pData)[n]) : AddWord(PWORD(pData)[n]);
			}

		if( Send() ) {

			return TRUE;

			//uSent += uSend;

			//Sleep(100);

			//continue;
			}

		return FALSE;
		}
	
	return TRUE;
	}

// Authentication Support

BOOL CML1Tester::GetMacId(void)
{
	if( IsMacNULL() ) {

		return m_pExtra->GetMacId(0, m_Mac.m_Addr);
		}

	return TRUE;
	}

BOOL CML1Tester::IsAuthenticated(CML1Dev * pDev)
{
	if( pDev ) {

		return pDev->m_fAuthentic && pDev->m_Unit > 0 && !IsMacNULL(pDev) ? TRUE : FALSE;
		}

	return m_fAuthentic && m_Unit > 0 && !IsMacNULL() ? TRUE : FALSE;
	}

BOOL CML1Tester::IsAuthPollTimedOut(CML1Dev * pDev)
{
	if( pDev ) {

		return IsTimedOut(pDev->m_uAuthPoll, m_AuthInterval);
		}

	return IsTimedOut(m_uAuthPoll, m_AuthInterval);
	}

void CML1Tester::SetAuthPoll(CML1Dev * pDev)
{
	if( pDev ) {

		pDev->m_uAuthPoll = GetTickCount();

		return;
		}

	m_uAuthPoll = GetTickCount();
	}

BOOL CML1Tester::PshAuthentication(CML1Dev * pDev)
{
	ShowDebug("\nPush Authentication %u %u", GetTickCount(), pDev ? pDev->m_Server : m_Server); 

	if( pDev && IsAuthPollTimedOut(pDev) ) {

		m_uTxPtr = 0;

		AddByte(authPush);

		AddByte(pDev->m_Server);

		AddMac(pDev);

		AddByte(LOBYTE(HIWORD(pDev->m_Unit)));

		AddWord(LOWORD(pDev->m_Unit));

		AddLong(pDev->m_Authority);
		
		AddSpecial(pDev, GetSpecialExtra());

		AddKey(m_pTx);

		if( Send() ) {

			SetAuthPoll(pDev);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CML1Tester::FrcAuthentication(CML1Dev * pDev, UINT uCount)
{
	ShowDebug("\nForce Authentication %u", uCount); 

	if( pDev ) {

		m_uTxPtr = 0;

		AddByte(authPush);

		AddByte(pDev->m_Server);

		AddMac(pDev);

		AddByte(LOBYTE(HIWORD(pDev->m_Unit)));

		AddWord(LOWORD(pDev->m_Unit));

		AddLong(pDev->m_Authority);
		
		AddSpecial(pDev, uCount);

		AddKey(m_pTx);

		if( Send() ) {

			SetAuthPoll(pDev);

			return TRUE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP Driver
//

// Instantiator

INSTANTIATE(CML1TcpTester);

// Constructor

CML1TcpTester::CML1TcpTester(void)
{
	m_Ident	  = DRIVER_ID;	

	m_fTrack  = TRUE;

	m_uKeep	  = 0;

	m_fClient = FALSE;
	}

// Destructor

CML1TcpTester::~CML1TcpTester(void)
{
	}

// Device

CCODE MCALL CML1TcpTester::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pTcp = (CML1Tcp *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pTcp  = new CML1Tcp;

			m_pDev = m_pTcp;
		
			m_pTcp->m_PingReg    = GetWord(pData);

			m_pTcp->m_TO	     = GetWord(pData);

			m_pTcp->m_Retry      = GetByte(pData);

			m_pTcp->m_Blocks     = GetWord(pData);

			InitDev();

			// Test Target (Client) MAC ID
			m_pDev->m_Mac.m_Addr[1] = 0x05;
			m_pDev->m_Mac.m_Addr[2] = 0xE4;
			m_pDev->m_Mac.m_Addr[3] = 0x01;
			m_pDev->m_Mac.m_Addr[4] = 0x10;
			m_pDev->m_Mac.m_Addr[5] = 0x10;

			m_pDev->m_Unit   = 0x123456;

			m_pDev->m_Server = 1;

			// At present, ML1 is a point to point protocol with the Server ID residing on the 
			// device level, likewise, the Unit ID resides on the driver level.  This 
			// implementation (Server ID and UINT ID residing on both levels) is maintained
			// in event that ML1 evolves to a multi drop protocol!

			// Set driver level for current point to point communications!

			m_Unit   = m_pDev->m_Unit;

			m_Server = m_pDev->m_Server;

			//////////////////////////////////////////////////////////////

			pData = LoadBlocks(m_pExtra, pData);

			pData		  = LoadBlocks(m_pExtra, pData);

			m_pTcp->m_bIpSel  = GetByte(pData);

			m_pTcp->m_IP1     = GetAddr(pData);

			m_pTcp->m_pAddr   = GetString(pData);
			
			m_pTcp->m_uPort	  = GetWord(pData);

			m_pTcp->m_fKeep	  = GetByte(pData);

			m_pTcp->m_fPing	  = GetByte(pData);

			m_pTcp->m_uTime1  = GetLong(pData);

			m_pTcp->m_uTime2  = GetWord(pData);

			m_pTcp->m_uTime3  = GetWord(pData);

			m_pTcp->m_IP2	  = GetAddr(pData);

			m_pTcp->m_fClose  = FALSE;

			m_pTcp->m_fRemote = FALSE;

			InitTcp();

			InitSpecial();

			AfxListAppend(m_pHead, m_pTail, m_pTcp, m_pNext, m_pPrev);

			pDevice->SetContext(m_pTcp);

			for(UINT s = 0; s < m_pTcp->m_bSockets; s++ ) {

				OpenListener(&m_pTcp->m_pListen[s]);
				}

			return CCODE_SUCCESS;
			}
		
		return CCODE_ERROR | CCODE_HARD;
		}

	m_pDev = m_pTcp;

	return CCODE_SUCCESS;
	}

CCODE MCALL CML1TcpTester::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pTcp->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(m_pTcp, FALSE);
			}
		}
	else {
		CloseSocket(m_pTcp, FALSE);

		for( UINT s = 0; s < m_pTcp->m_bSockets; s++ ) {

			CloseListener(&m_pTcp->m_pListen[s]);
			}

		delete m_pTcp->m_pClient;

		m_pTcp->m_pClient = NULL;

		delete [] m_pTcp->m_pListen;

		m_pTcp->m_pListen = NULL;

		RemoveBlocks();

		AfxListRemove(m_pHead, m_pTail, m_pTcp, m_pNext, m_pPrev);

		m_pTcp = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CML1Tester::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CML1TcpTester::Ping(void)
{
	if( m_pTcp->m_fPing ) {
		
		DWORD IP;

		if( !m_pTcp->m_fAux ) {

			IP = m_pTcp->m_IP1;
			}
		else
			IP = m_pTcp->m_IP2;
		
		if( CheckIP(IP, m_pTcp->m_uTime2) == NOTHING ) {
			
			if( m_pTcp->m_IP2 ) {

				m_pTcp->m_fAux = !m_pTcp->m_fAux;
				}

			return CCODE_ERROR; 
			}
		}

	return CML1Tester::Ping();
	}

void CML1TcpTester::InitSpecial(void)
{
	CML1Blk * pBlk = CML1Base::FindSpecial(m_pDev);

	if( pBlk ) {

		for( UINT u = SR_MIN; u < SR_MAX; u++ ) {

			switch( u - SR_MIN ) {

				case srIP1:	SetIP(m_pTcp, pBlk, m_pTcp->m_IP1);		u++;	break;
				
				case srPort:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_uPort);		break;

				case srICMP:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_fPing);		break;

				case srTime1:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_uTime1);		break;

				case srTime2:	pBlk->SetRxWord(u - SR_MIN, m_pTcp->m_uTime2);		break;
				}
			}
		}

	CML1Tester::InitSpecial();
	}

void CML1TcpTester::SetSr(CML1Dev * pDev, UINT& uOffset, PWORD pData)
{
	if( IsSrBase(uOffset) ) {

		return CML1Base::SetSr(pDev, uOffset, pData);
		}

	CML1Tcp * pTcp = (CML1Tcp *)pDev;

	if( pTcp ) {

		CML1Blk * pBlock = FindSpecial(pTcp);

		if( pBlock ) {

			if( IsSrLong(uOffset) ) {

				if( uOffset == srIP1 ) {

					DWORD Data = PU4(pData)[0];

					if( pTcp->m_IP1 != Data ) {

						if( SetIP(pTcp, pBlock, Data + 10) ) {

							//CloseSocket(pTcp, FALSE);

							//CloseListener(pTcp->m_pListen);
							}
						}
					}

				uOffset++;

				return;
				}

			WORD x = SHORT(MotorToHost(PU2(pData)[0]));

			pBlock->SetRxWord(uOffset, x);

			if( uOffset == srPort ) {

				if( pTcp->m_uPort  !=  x ) {

					pTcp->m_uPort = pData[0];

					//CloseSocket(pTcp, FALSE);

					//CloseListener(pTcp->m_pListen);
					}

				return;
				}
	
			if( uOffset == srICMP ) {

				pTcp->m_fPing = x;

				return;
				}

			if( uOffset == srTime1 ) {
					
				pTcp->m_uTime1 = x;

				return;
				}

			if( uOffset == srTime2 ) {

				pTcp->m_uTime2 = x;

				return;
				}
			}
		}
	}

void CML1TcpTester::AddSpecial(CML1Dev * pDev, BYTE bCount)
{
	CML1Base::AddSpecial(pDev, bCount);

	CML1Blk * pBlk = FindSpecial(pDev);

	CML1Tcp * pTcp = (CML1Tcp *)pDev;

	if( pBlk && pTcp ) {

		BYTE bBase = CML1Base::GetSpecialExtra();

		if( bCount > bBase ) {

			bCount -= bBase;

			for( UINT u = srIP1, c = 0; c < bCount; u++, c++ ) {

				switch( u ) {

					case srIP1:	AddLong(MotorToHost(pTcp->m_IP1) + 10);	u++;		break;
				
					case srPort:	AddWord(pTcp->m_uPort);			break;

					case srICMP:	AddWord(pTcp->m_fPing);			break;

					case srTime1:	AddWord(pTcp->m_uTime1);		break;

					case srTime2:	AddWord(pTcp->m_uTime2);		break;
					}
				}
			}
		}
	}

BYTE CML1TcpTester::GetSpecialExtra(void)
{
	return 5 + CML1Base::GetSpecialExtra();
	}

BYTE CML1TcpTester::FindSpecialWriteCount(UINT uOffset)
{
	BYTE bCount = CML1Base::FindSpecialWriteCount(uOffset);

	switch( uOffset ) {

		case srIP1:	bCount = CML1Base::GetSpecialExtra() + 1;	break;
				
		case srPort:	bCount = CML1Base::GetSpecialExtra() + 2;	break;

		case srICMP:	bCount = CML1Base::GetSpecialExtra() + 3;	break;

		case srTime1:	bCount = CML1Base::GetSpecialExtra() + 4;	break;
			
		case srTime2:	bCount = CML1Base::GetSpecialExtra() + 5;	break;
		}

	return bCount;
	}

// Socket Management

BOOL CML1TcpTester::CheckSocket(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen)
{
	if( fListen ) {

		return CheckListener(pSock);
		}

	return CheckSocket(pCtx);
	}


// Client Socket Management

BOOL CML1TcpTester::CheckSocket(CML1Tcp * pCtx)
{
	if( pCtx ) {

		if( pCtx->m_pClient->m_pSocket ) {

			if( !pCtx->m_fDirty ) {

				UINT Phase;

				pCtx->m_pClient->m_pSocket->GetPhase(Phase);

				if( Phase == PHASE_ERROR ) {

					CloseSocket(pCtx, TRUE);

					return FALSE;
					}

				if( Phase == PHASE_CLOSING ) {

					CloseSocket(pCtx, FALSE);

					return FALSE;
					}

				return TRUE;
				}

			CloseSocket(pCtx, FALSE);

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CML1TcpTester::OpenSocket(CML1Tcp * pCtx)
{
	if( CheckSocket(pCtx) ) {

		return TRUE;
		}

	if( pCtx->m_fDirty ) {

		pCtx->m_fDirty = FALSE;

		pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - pCtx->m_uLast;

		UINT tt = ToTicks(pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (pCtx->m_pClient->m_pSocket = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		if( !pCtx->m_fAux ) {

			IP   = (IPADDR const &) pCtx->m_IP1;

			Port = WORD(pCtx->m_uPort);
			}
		else {
			IP   = (IPADDR const &) pCtx->m_IP2;

			Port = WORD(pCtx->m_uPort);
			}

		if( pCtx->m_pClient->m_pSocket->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(pCtx->m_uTime2);

			while( GetTimer() ) {

				UINT Phase;

				pCtx->m_pClient->m_pSocket->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					if( ML1_TCP_TEST ) {

						AfxTrace("\nTarget IP is %2.2x%2.2x%2.2x%2.2x %u %8.8x %8.8x", IP.m_b1,IP.m_b2,IP.m_b3,IP.m_b4, Port, pCtx->m_IP1, pCtx->m_IP2);

						pCtx->m_pClient->m_pSocket->GetRemote(IP, Port);

						AfxTrace("\nOpen rem %2.2x%2.2x%2.2x%2.2x %u", IP.m_b1,IP.m_b2,IP.m_b3,IP.m_b4, Port);

						pCtx->m_pClient->m_pSocket->GetLocal(IP, Port);

						AfxTrace(" loc %2.2x%2.2x%2.2x%2.2x %u", IP.m_b1,IP.m_b2,IP.m_b3,IP.m_b4, Port);
						}

					pCtx->m_pClient->m_uIdle = GetTickCount();

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					pCtx->m_pClient->m_pSocket->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( pCtx->m_IP2 ) {

				pCtx->m_fAux = !pCtx->m_fAux;
				}

			CloseSocket(pCtx, TRUE);

			return FALSE;
			}

		if( pCtx->m_IP2 ) {

			pCtx->m_fAux = !pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CML1TcpTester::CloseSocket(CML1Tcp * pCtx, BOOL fAbort)
{
	if( pCtx->m_pClient->m_pSocket ) {

		if( fAbort )
			pCtx->m_pClient->m_pSocket->Abort();
		else
			pCtx->m_pClient->m_pSocket->Close();

		pCtx->m_pClient->m_pSocket->Release();

		pCtx->m_pClient->m_pSocket = NULL;

		pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

void CML1TcpTester::CheckIdle(CML1Tcp * pCtx)
{
	if( IsTimedOut(pCtx->m_pClient->m_uIdle, pCtx->m_uTime1) ) {

		CloseSocket(pCtx, FALSE);
		}
	}

// Server Socket Management

BOOL CML1TcpTester::CheckListener(SOCKET * pSock)
{
	UINT Phase;

	if( pSock ) {

		if( pSock->m_pSocket ) {

			pSock->m_pSocket->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				return TRUE;
				}

			if( Phase == PHASE_ERROR ) {

				CloseListener(pSock);

				return OpenListener(pSock);
				}

			if( Phase == PHASE_CLOSING ) {

				pSock->m_pSocket->Close();

				return FALSE;
				}

			return FALSE;
			}

		OpenListener(pSock);
		}

	return FALSE;
	}

BOOL CML1TcpTester::OpenListener(SOCKET * pSock)
{
	if( pSock ) {
		
		if( !pSock->m_pSocket ) { 

			pSock->m_pSocket = CreateSocket(IP_TCP);
			}
				
		if( pSock->m_pSocket ) {

			if( pSock->m_pSocket->Listen(m_pTcp->m_uPort) == S_OK ) {

				pSock->m_uIdle = GetTickCount();

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

void CML1TcpTester::CloseListener(SOCKET * pSock)
{
	if( pSock && pSock->m_pSocket) {

		pSock->m_pSocket->Close();

		pSock->m_pSocket->Release();

		pSock->m_pSocket = NULL;
		}
	}

void CML1TcpTester::CheckSilent(CML1Tcp * pCtx, SOCKET * pSock)
{
	if( IsTimedOut(pSock->m_uIdle, pCtx->m_uTime1) ) {

		CloseListener(pSock);

		OpenListener(pSock);
		}
	}

// Implementation

void CML1TcpTester::InitTcp(void)
{
	if( m_pTcp ) {

		m_pTcp->m_uLast	   = GetTickCount();

		m_pTcp->m_fAux	   = FALSE;

		m_pTcp->m_fDirty   = FALSE;

		m_pTcp->m_bSockets = 1;
		
		m_pTcp->m_pClient  = new SOCKET;

		m_pTcp->m_pListen  = new SOCKET[m_pTcp->m_bSockets];

		m_pTcp->m_pClient->m_pSocket = NULL;

		m_pTcp->m_pListen->m_pSocket = NULL;
		}
	}

BOOL CML1TcpTester::TxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fRespond)
{
	if( pSock ) {
		
		if( fRespond && !CheckSocket(pCtx, pSock, fRespond) ) {

			return FALSE;
			}

		if( !IsAuthentication(m_pDev, m_pTx) && !IsAuthenticationReq(m_pTx) ) {

			m_pTx[5] = BYTE(m_uTxPtr - 6);
			}

		UINT uSize   = m_uTxPtr;

		if( pSock->m_pSocket->Send(m_pTx, uSize) == S_OK ) {

			if( uSize == m_uTxPtr ) {

				if( ML1_TCP_TEST ) {

					AfxTrace("\nTx %u : ", m_uTxPtr);

					for( UINT u = 0; u < uSize; u++ ) {

						AfxTrace("%2.2x ", m_pTx[u]);
						}
					}
				
				return TRUE;
				}
			}
		}
			
	return FALSE;
	}

BOOL CML1TcpTester::RxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen)
{
	if( pSock ) {

		SetTimer(pCtx->m_uTime2);

		UINT uPtr = 0;

		UINT uSize = 0;

		BOOL fRx   = FALSE;

		BOOL fContinue = FALSE;

		if( ML1_TCP_TEST ) {

			AfxTrace("\nRx %u : ", pCtx->m_uTime2);
			}

		do {
			uSize = sizeof(m_pRx) - uPtr;

			pSock->m_pSocket->Recv(m_pRx + uPtr, uSize);

			if( uSize || uPtr >= 6 + 6 ) {

				if( ML1_TCP_TEST ) {

					for( UINT u = uPtr; u < uPtr + uSize; u++ ) {

						AfxTrace("%2.2x ", m_pRx[u]);
						}
					}
					
				uPtr += uSize;
				
				if ( uPtr >= 7 ) {

					 UINT uTotal = FindRxSize();

					if( uPtr >= uTotal ) {

						for( UINT u = 0; u < uTotal; u++ ) {

							pSock->m_pBuff[u] = m_pRx[u];
							}
						
						fRx = CML1Server::HandleFrame(pSock->m_pBuff);

						if( uPtr -= uTotal ) {

							for( UINT n = 0; n < uPtr; n++ ) {

								m_pRx[n] = m_pRx[uTotal++];
								}

							fContinue = TRUE;

							continue;
							}

						return fRx;
						}
					}

				fContinue = FALSE;
				
				continue;
				}

			if( !fListen && !CheckSocket(pCtx, pSock, fListen) ) {

				return FALSE;
				}
			
			Sleep(10);
			
			} while (fContinue || (!fListen && GetTimer()));
		}
	
	return FALSE;
	}

// Transport 

BOOL CML1TcpTester::Send(void)
{
	CML1Tcp * pCtx = (CML1Tcp *)FindContext();

	if( pCtx ) {

		if( OpenSocket(pCtx) ) {

			BOOL fTx = TxData(pCtx, pCtx->m_pClient, FALSE);

			if( fTx ) {

				pCtx->m_pClient->m_uIdle = GetTickCount();
				}

			return fTx;
			}
		}

	return FALSE;
	}

BOOL CML1TcpTester::Recv(void)
{
	BOOL fRx = FALSE;

	CML1Tcp * pDev = (CML1Tcp *)m_pHead;

	while( pDev ) {

		if( CheckSocket(pDev) ) {

			if( RxData(pDev, pDev->m_pClient, FALSE) ) {

				pDev->m_pClient->m_uIdle = GetTickCount();

				fRx = TRUE;

				continue;
				}

			if( !IsAuthentication(m_pDev, m_pRx) ) {

				CheckIdle(pDev);
				}
			}

		pDev = (CML1Tcp *)pDev->m_pNext;
		}
		
	return fRx;
	}

BOOL CML1TcpTester::Listen(void)
{
	CML1Tcp * pDev = (CML1Tcp *)m_pHead;

	while( pDev ) {

		for( UINT s = 0; s < pDev->m_bSockets; s++ ) {

			if( CheckListener(&pDev->m_pListen[s]) ) {

				if( RxData(pDev, &pDev->m_pListen[s], TRUE) ) {

					pDev->m_pListen[s].m_uIdle = GetTickCount();

					return TRUE;
					}

				if( !IsAuthentication(m_pDev, m_pTx) ) {

					CheckSilent(pDev, &pDev->m_pListen[s]);
					}
				}
			}

		pDev = (CML1Tcp *)pDev->m_pNext;
		}
			
	return FALSE;
	}

BOOL CML1TcpTester::Respond(PBYTE pBuff)
{
	CML1Tcp * pCtx = (CML1Tcp *)DoListen(GetID(pBuff), GetUnitID(pBuff));

	if( pCtx ) {

		if( OpenSocket(pCtx) ) {

			return TxData(pCtx, pCtx->m_pClient, FALSE);
			}
		}

	return FALSE;
	}

BOOL CML1TcpTester::Respond(CML1Dev * pDev)
{
	CML1Tcp * pCtx = (CML1Tcp *) pDev;

	if( pCtx ) {

		for( UINT s = 0; s < m_pTcp->m_bSockets; s++ ) {

			if( TxData(pCtx, &pCtx->m_pListen[s], TRUE) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Transport Helpers

UINT CML1TcpTester::FindRxSize(void)
{
	if( IsAuthentication(m_pDev, m_pRx) || IsAuthenticationReq(m_pRx) ) {

		switch( m_pRx[0] ) {

			case authPush:	return 18 + GetSpecialBytes(GetSpecialCount(m_pRx));

			case authAck:	return 18 + GetSpecialBytes(GetSpecialCount(m_pRx));

			case authPoll:	return  7;

			case authResp:	return 18 + GetSpecialBytes(GetSpecialCount(m_pRx));
			}
		}
	
	return m_pRx[5] + 6;
	}

PVOID CML1TcpTester::FindContext(void)
{
	return m_pDev;
	}

// Helpers

BOOL CML1TcpTester::SetIP(CML1Tcp * pTcp, CML1Blk * pBlock, DWORD dwData)
{
	if( pTcp && pBlock ) {

		DWORD dwIP = MotorToHost(dwData);

		//////////////////////////////////////////

		// TESTER HACK !!!

		dwIP += 10;

		//////////////////////////////////////////

		pBlock->SetRxWord(srIP1 + 0, HIWORD(dwIP));

		pBlock->SetRxWord(srIP1 + 1, LOWORD(dwIP));

		pTcp->m_IP1 = dwData;

		return TRUE;
		}

	return FALSE;
	}

// End of File
