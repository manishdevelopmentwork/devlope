
#include "intern.hpp"

#include "GenericSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Model Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic System Item 
//

// Base Class

#define CBaseClass CUISystem

// Dynamic Class

AfxImplementDynamicClass(CGenericSystemItem, CBaseClass);

// Constructor

CGenericSystemItem::CGenericSystemItem(void) : CBaseClass(FALSE)
{
	AfxTouch(AfxRuntimeClass(CControlProject));

	m_pFramer = NULL;
}

// Download Config

CString CGenericSystemItem::GetDownloadConfig(void) const
{
	return L"Common";
}

// Model Mapping

CString CGenericSystemItem::GetModelList(void) const
{
	return m_pDevCon->m_pApply->GetModelList();
}

CString CGenericSystemItem::GetModelInfo(CString Model) const
{
	return m_pDevCon->m_pApply->GetModelInfo(Model);
}

// DLD Selection

CString CGenericSystemItem::GetDldFolder(void) const
{
	return m_pDevCon->m_pApply->GetProcessor();
}

// Conversion

void CGenericSystemItem::PostConvert(void)
{
	CBaseClass::PostConvert();

	WalkModuleTypes(FALSE);

	m_pDevCon->m_pHConfig->ImportFromJson();

	m_pFramer->Release();

	m_pFramer = NULL;

	CheckModel();
}

// Persistance

void CGenericSystemItem::Init(void)
{
	CBaseClass::Init();

	CheckModel();
}

void CGenericSystemItem::PostLoad(void)
{
	AdjustAlphaModels();

	CBaseClass::PostLoad();

	CheckModel();
}

// System Data

void CGenericSystemItem::AddNavCats(void)
{
	CDatabase *pDbase = GetDatabase();

	UINT       uGroup = GetSoftwareGroup();

	CBaseClass::AddNavCatHead();

	CBaseClass::AddNavCatTail();

	if( uGroup >= SW_GROUP_4 ) {

		pDbase->AddNavCategory(IDS_CONTROL,
				       L"Control/Project",
				       0x21000010,
				       0x20000010,
				       L"Control/Project|Control/Project/Funcs|Tags|Comms"
		);
	}

	if( HasExpansion() ) {

		if( m_pDevCon->m_Model.StartsWith(L"da") ) {

			pDbase->AddNavCategory(IDS_MODULES_IO,
					       L"Comms/Rack",
					       0x2100000E,
					       0x2000000E,
					       L"Comms"
			);
		}
		else {
			pDbase->AddNavCategory(IDS_MODULES_IO,
					       L"Comms/Rack",
					       0x2100000E,
					       0x2000000E,
					       L"Comms"
			);
		}
	}
}

void CGenericSystemItem::AddResCats(void)
{
	CDatabase *pDbase = GetDatabase();

	CBaseClass::AddResCatHead();

	pDbase->AddResCategory(IDS_CONTROL,
			       L"Control/Project",
			       0x21000010,
			       0x20000010
	);

	pDbase->AddResCategory(IDS_BLOCKS,
			       L"Control/Project/Funcs",
			       0x21000013,
			       0x20000013
	);

	CBaseClass::AddResCatTail();
}

// Database Flags

void CGenericSystemItem::CheckModel(void)
{
	CStringArray Model;

	m_pDevCon->m_Model.Tokenize(Model, '|');

	UINT uPart = m_pDevCon->m_Model.StartsWith(L"Generic") ? 1 : 0;

	switch( Model[uPart][0] ) {

		case 'g':
		{
			m_pFramer = Create_GraphiteFramer();

			m_pDbase->AddFlag(L"IconLeds");

			m_SoftKeys = 3;
		}
		break;

		case 'c':
		{
			if( Model[uPart][1] == 'o' ) {

				m_pFramer = Create_ColoradoFramer();
			}
			else {
				m_pFramer = Create_CanyonFramer();
			}

			m_pDbase->AddFlag(L"IconLedsOnly");

			m_pDbase->AddFlag(L"IconLeds");

			m_SoftKeys = 0;
		}
		break;

		case 'd':
		{
			m_pFramer  = Create_CanyonFramer();

			m_pDbase->RemFlag(L"IconLedsOnly");

			m_pDbase->RemFlag(L"IconLeds");

			m_SoftKeys = 0;
		}
		break;
	}
}

// System Hooks

void CGenericSystemItem::ChangeModel(void)
{
	WalkModuleTypes(TRUE);
}

void CGenericSystemItem::ApplyHardware(BOOL fInit)
{
	if( fInit ) {

		ApplyPorts(TRUE);

		ApplyModules(TRUE);

		m_Level = 3;
	}
	else {
		if( m_Level < 3 ) {

			ImportExpansion();

			m_pDevCon->FinalizeImport();

			ImportConfig();

			m_Level = 3;
		}

		ApplyPorts(FALSE);

		ApplyModules(FALSE);
	}

	m_DispSize = m_pDevCon->GetDisplaySize();

	m_SoftKeys = 3;
}

// New Schema Support

void CGenericSystemItem::ApplyPorts(BOOL fInit)
{
	CMap<CString, HGLOBAL> Save;

	if( !fInit ) {

		SavePortListPorts(Save, m_pComms->m_pPorts, L"");

		SaveCardListPorts(Save, m_pComms->m_pExpansion->m_pRack, L"B");

		SaveRackListPorts(Save, m_pComms->m_pExpansion->m_pTethered->m_pRacks, L"T");
	}

	if( TRUE ) {

		CStringArray List;

		m_pDevCon->GetPorts(List);

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CStringArray Fields;

			List[n].Tokenize(Fields, ',');

			if( Fields.GetCount() == 6 ) {

				CCommsPort *pPort = CCommsPort::CreatePort(char(Fields[4][0]));

				if( pPort ) {

					m_pComms->m_pPorts->AppendItem(pPort);

					if( !fInit ) {

						CString t = Fields[3];

						INDEX   n = Save.FindName(t);

						if( !Save.Failed(n) ) {

							pPort->LoadSnapshot(Save[n]);

							GlobalFree(Save[n]);

							Save.Remove(n);
						}
					}

					if( Fields[3][0] == 'S' ) {

						CCommsPortSerial *pSerial = (CCommsPortSerial *) pPort;

						switch( Fields[4][1] ) {

							case 'P':
								pSerial->m_Phys = ((1<<physicalRS232) | (1<<physicalProgram));
								break;

							case '2':
								pSerial->m_Phys = (1<<physicalRS232);
								break;

							case '4':
								pSerial->m_Phys = ((1<<physicalRS485) | (1<<physicalRS422));
								break;
						}
					}

					pPort->m_Name     = Fields[0];

					pPort->m_PortTag  = Fields[3];

					pPort->m_Where    = Fields[5];

					pPort->m_PortPhys = watoi(Fields[1]);

					pPort->m_PortLog  = watoi(Fields[2]);
				}
			}
		}
	}

	for( INDEX n = Save.GetHead(); !Save.Failed(n); Save.GetNext(n) ) {

		GlobalFree(Save[n]);
	}
}

void CGenericSystemItem::ApplyModules(BOOL fInit)
{
	CMap<CString, HGLOBAL> Save;

	if( fInit ) {

		if( !m_pComms->m_pRack->m_pPorts->GetItemCount() ) {

			CNewCommsPortRack *pPort = New CNewCommsPortRack;

			m_pComms->m_pRack->m_pPorts->AppendItem(pPort);
		}
	}
	else {
		CCommsPort *pPort = m_pComms->m_pRack->m_pPorts->GetItem(0U);

		if( pPort ) {

			if( pPort->IsKindOf(AfxRuntimeClass(CNewCommsPortRack)) ) {

				CNewCommsPortRack *pRackPort = (CNewCommsPortRack *) pPort;

				SaveNewRackModules(Save, pPort->m_pDevices);

				pRackPort->DeleteModuleDevices();
			}
			else {
				if( UseRedBus() ) {

					SaveRedBusModules(Save, (CCommsPortRack *) pPort);
				}
				else {
					SaveOldRackModules(Save, (CCommsPortRack *) pPort);
				}

				m_pComms->m_pRack->m_pPorts->DeleteAllItems(TRUE);

				CNewCommsPortRack *pPort = New CNewCommsPortRack;

				m_pComms->m_pRack->m_pPorts->AppendItem(pPort);
			}
		}
	}

	if( m_pComms->m_pRack->m_pPorts->GetItemCount() ) {

		CNewCommsPortRack *pPort = (CNewCommsPortRack *) m_pComms->m_pRack->m_pPorts->GetItem(0U);

		CStringArray List;

		m_pDevCon->GetModules(List);

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CStringArray Fields;

			List[n].Tokenize(Fields, ',');

			if( Fields.GetCount() == 4 ) {

				CNewCommsDeviceRack *pDev = New CNewCommsDeviceRack;

				pPort->m_pDevices->AppendItem(pDev);

				pDev->m_Name.Empty();

				pDev->m_Number = 0;

				if( !fInit ) {

					INDEX n = Save.FindName(Fields[1]);

					if( !Save.Failed(n) ) {

						pDev->LoadSnapshot(Save[n]);

						GlobalFree(Save[n]);

						Save.Remove(n);
					}
				}

				switch( watoi(Fields[2]) ) {

					case 301:
						pDev->SetClass(AfxNamedClass(L"CSLCModule"));
						break;

					case 302:
						pDev->SetClass(AfxNamedClass(L"CDLCModule"));
						break;

					case 303:
						pDev->SetClass(AfxNamedClass(L"CTC8Module"));
						break;

					case 304:
						pDev->SetClass(AfxNamedClass(L"CII8LModule"));
						break;

					case 305:
						pDev->SetClass(AfxNamedClass(L"CIV8LModule"));
						break;

					case 306:
						pDev->SetClass(AfxNamedClass(L"CDIO14Module"));
						break;

					case 307:
						pDev->SetClass(AfxNamedClass(L"CRTD6Module"));
						break;

					case 310:
						pDev->SetClass(AfxNamedClass(L"CII8Module"));
						break;

					case 311:
						pDev->SetClass(AfxNamedClass(L"CIV8Module"));
						break;

					case 312:
						pDev->SetClass(AfxNamedClass(L"CSGModule"));
						break;

					case 313:
						pDev->SetClass(AfxNamedClass(L"COut4Module"));
						break;

					case 314:
						pDev->SetClass(AfxNamedClass(L"CTC8ISOModule"));
						break;

					case 200:
						pDev->SetClass(AfxNamedClass(L"CGMDIO14Module"));
						break;

					case 201:
						pDev->SetClass(AfxNamedClass(L"CGMINI8Module"));
						break;

					case 202:
						pDev->SetClass(AfxNamedClass(L"CGMINV8Module"));
						break;

					case 203:
						pDev->SetClass(AfxNamedClass(L"CGMOUT4Module"));
						break;

					case 204:
						pDev->SetClass(AfxNamedClass(L"CGMPID1Module"));
						break;

					case 205:
						pDev->SetClass(AfxNamedClass(L"CGMPID2Module"));
						break;

					case 206:
						pDev->SetClass(AfxNamedClass(L"CGMRTD6Module"));
						break;

					case 207:
						pDev->SetClass(AfxNamedClass(L"CGMTC8Module"));
						break;

					case 208:
						pDev->SetClass(AfxNamedClass(L"CGMUIN4Module"));
						break;

					case 209:
						pDev->SetClass(AfxNamedClass(L"CGMSGModule"));
						break;

					case 210:
						pDev->SetClass(AfxNamedClass(L"CDADIDOModule"));
						break;

					case 211:
						pDev->SetClass(AfxNamedClass(L"CDAUIN6Module"));
						break;

					case 212:
						pDev->SetClass(AfxNamedClass(L"CDAAO8Module"));
						break;

					case 213:
						pDev->SetClass(AfxNamedClass(L"CDAPID1Module"));
						break;

					case 214:
						pDev->SetClass(AfxNamedClass(L"CDAPID2Module"));
						break;

					case 215:
						pDev->SetClass(AfxNamedClass(L"CDARO8Module"));
						break;

					case 216:
						pDev->SetClass(AfxNamedClass(L"CDAMix4Module"));
						break;

					case 217:
						pDev->SetClass(AfxNamedClass(L"CDAMix2Module"));
						break;
				}

				pDev->m_SlotNum = watoi(Fields[0]);

				pDev->m_SlotTag = Fields[1];

				pDev->m_Where   = Fields[3];
			}
		}

		for( UINT n = 0; n < pPort->m_pDevices->GetIndexCount(); n++ ) {

			CCommsDevice *pDevice = pPort->m_pDevices->GetItem(n);

			if( pDevice ) {

				if( pDevice->m_Name.IsEmpty() ) {

					pDevice->CreateNameAndNumber();
				}
			}
		}
	}
}

BOOL CGenericSystemItem::SavePortListPorts(CMap<CString, HGLOBAL> &Save, CCommsPortList *pList, PCTXT pTag)
{
	if( pList ) {

		UINT uPorts = pList->GetIndexCount();

		for( UINT uPort = 0; uPort < uPorts; uPort++ ) {

			CCommsPort *pPort = pList->GetItem(uPort);

			if( pPort ) {

				if( pPort->m_PortTag.IsEmpty() ) {

					char cTag = pPort->GetPortTag();

					if( cTag ) {

						if( *pTag ) {

							pPort->m_PortTag.Printf(L"%c%s", cTag, pTag);
						}
						else {
							pPort->m_PortTag.Printf(L"%c%u", cTag, uPort);
						}
					}
				}

				if( !pPort->m_PortTag.IsEmpty() ) {

					HGLOBAL hData = pPort->TakeSnapshot();

					Save.Insert(pPort->m_PortTag, hData);
				}
			}
		}

		pList->DeleteAllItems(TRUE);

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::SaveCardListPorts(CMap<CString, HGLOBAL> &Save, COptionCardList *pCards, PCTXT pTag)
{
	if( pCards ) {

		UINT uCards = pCards->GetIndexCount();

		for( UINT uCard = 0; uCard < uCards; uCard++ ) {

			COptionCardItem *pCard = pCards->GetItem(uCard);

			SavePortListPorts(Save, pCard->m_pPorts, CPrintf("%s%u", pTag, uCard));
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::SaveRackListPorts(CMap<CString, HGLOBAL> &Save, COptionCardRackList *pRacks, PCTXT pTag)
{
	if( pRacks ) {

		UINT uRacks = pRacks->GetIndexCount();

		for( UINT uRack = 0; uRack < uRacks; uRack++ ) {

			COptionCardRackItem *pRack = pRacks->GetItem(uRack);

			SaveCardListPorts(Save, pRack->m_pSlots, CPrintf("%s%u", pTag, 1+uRack));
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::SaveNewRackModules(CMap<CString, HGLOBAL> &Save, CCommsDeviceList *pList)
{
	if( pList ) {

		UINT uDevices = pList->GetIndexCount();

		for( UINT uDevice = 0; uDevice < uDevices; uDevice++ ) {

			CCommsDevice *pDevice = pList->GetItem(uDevice);

			if( pDevice ) {

				if( pDevice->IsKindOf(AfxRuntimeClass(CNewCommsDeviceRack)) ) {

					CNewCommsDeviceRack *pRackDevice = (CNewCommsDeviceRack *) pDevice;

					if( !pRackDevice->m_SlotTag.IsEmpty() ) {

						HGLOBAL hData = pRackDevice->TakeSnapshot();

						Save.Insert(pRackDevice->m_SlotTag, hData);
					}
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::SaveOldRackModules(CMap<CString, HGLOBAL> &Save, CCommsPortRack *pPort)
{
	if( pPort ) {

		CCommsSlotList *pSlots = pPort->m_pSlots;

		UINT uRack = NOTHING;

		UINT uSlot = 0;

		for( UINT n = 0; n < pSlots->GetIndexCount(); n++ ) {

			CCommsPortSlot *pSlot = pSlots->GetItem(n);

			if( pSlot->m_SlotNum < NOTHING ) {

				if( uRack != pSlot->m_Rack ) {

					uRack = pSlot->m_Rack;

					uSlot = 0;
				}

				if( pSlot->m_Dev < NOTHING ) {

					UINT uType = pSlot->GetJsonType();

					if( uType ) {

						CCommsDeviceRack *pDevice = pSlot->GetDevice();

						if( pDevice ) {

							CString Tag;

							if( uRack == NOTHING ) {

								Tag.Printf(L"B%u-%u", uSlot, uType);
							}
							else {
								Tag.Printf(L"T%u%u-%u", uRack, uSlot, uType);
							}

							HGLOBAL hData = pDevice->TakeSnapshot(L"CNewCommsDeviceRack");

							Save.Insert(Tag, hData);
						}
					}
				}

				uSlot++;
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::SaveRedBusModules(CMap<CString, HGLOBAL> &Save, CCommsPortRack *pPort)
{
	if( pPort ) {

		CCommsDeviceList *pDevs = pPort->m_pDevices;

		UINT		  uSlot = NOTHING;

		for( INDEX i = pDevs->GetHead(); !pDevs->Failed(i); pDevs->GetNext(i) ) {

			if( uSlot < NOTHING ) {

				CNewCommsDeviceRack *pDevice = (CNewCommsDeviceRack *) pDevs->GetItem(i);

				UINT uType = pDevice->GetJsonType();

				if( uType ) {

					CString Tag;

					Tag.Printf(L"B%u-%u", uSlot, uType);

					HGLOBAL hData = pDevice->TakeSnapshot(L"CNewCommsDeviceRack");

					Save.Insert(Tag, hData);
				}
			}

			uSlot++;
		}
	}

	return TRUE;
}

BOOL CGenericSystemItem::ImportCardListPorts(CJsonData *pModules, COptionCardList *pCards)
{
	if( pCards ) {

		UINT uCards = pCards->GetIndexCount();

		for( UINT uCard = 0; uCard < uCards; uCard++ ) {

			COptionCardItem *pCard = pCards->GetItem(uCard);

			UINT uType = 0;

			switch( pCard->m_Type ) {

				case typeModem:
					uType = 100;
					break;

				case typeCAN:
					uType = 101;
					break;

				case typeJ1939:
					uType = 102;
					break;

				case typeDeviceNet:
					uType = 103;
					break;

				case typeProfibus:
					uType = 104;
					break;
			}

			CJsonData *pItem;

			pModules->AddChild(FALSE, pItem);

			pItem->AddValue(L"order", CPrintf("%u", uCard));

			pItem->AddValue(L"type", CPrintf("%u", uType));
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::ImportRackListPorts(CJsonData *pExp, COptionCardRackList *pRacks)
{
	if( pRacks ) {

		UINT uRacks = pRacks->GetIndexCount();

		pExp->AddValue(L"tcount", CPrintf(L"%u", uRacks));

		if( uRacks ) {

			CJsonData *pTethered = NULL;

			pExp->AddChild(L"tethered", FALSE, pTethered);

			for( UINT uRack = 0; uRack < uRacks; uRack++ ) {

				COptionCardRackItem *pRack = pRacks->GetItem(uRack);

				CJsonData *pModules = NULL;

				pTethered->AddChild(CPrintf(L"rack%u.modules", 1+uRack), TRUE, pModules);

				ImportCardListPorts(pModules, pRack->m_pSlots);
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::ImportRackModules(CJsonData *pModules, CCommsSlotList *pSlots, UINT uRack)
{
	if( pSlots ) {

		UINT uCard = 0;

		for( UINT n = 0; n < pSlots->GetIndexCount(); n++ ) {

			CCommsPortSlot *pSlot = pSlots->GetItem(n);

			if( pSlot->m_Rack == uRack ) {

				if( pSlot->m_SlotNum < NOTHING ) {

					if( pSlot->m_Dev < NOTHING ) {

						UINT uType = pSlot->GetJsonType();

						if( uType ) {

							CJsonData *pItem = pModules->GetChild(uCard);

							pItem->AddValue(L"type", CPrintf("%u", uType));
						}
					}

					uCard++;
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::ImportModules(CJsonData *pExpand, CCommsSlotList *pSlots)
{
	if( pSlots ) {

		CJsonData *pTethered = pExpand->GetChild(L"tethered");

		if( pTethered ) {

			UINT uRacks = pTethered->GetCount();

			for( UINT uRack = 0; uRack < uRacks; uRack++ ) {

				CJsonData *pModules = pTethered->GetChild(CPrintf(L"rack%u.modules", 1+uRack));

				ImportRackModules(pModules, pSlots, 1+uRack);
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CGenericSystemItem::ImportRedBusModules(CJsonData *pModules, CCommsDeviceList *pDevs)
{
	UINT uSlot = NOTHING;

	for( INDEX i = pDevs->GetHead(); !pDevs->Failed(i); pDevs->GetNext(i) ) {

		if( uSlot < NOTHING ) {

			CNewCommsDeviceRack *pDevice = (CNewCommsDeviceRack *) pDevs->GetItem(i);

			UINT uType = pDevice->GetJsonType();

			if( uType ) {

				CJsonData *pItem = pModules->GetChild(uSlot);

				if( !pItem ) {

					for( UINT n = pModules->GetCount(); n <= uSlot; n++ ) {

						pModules->AddChild(FALSE, pItem);

						pItem->AddValue(L"order", CPrintf(L"%u", n), jsonString);
					}
				}

				pItem->AddValue(L"type", CPrintf("%u", uType));
			}
		}

		uSlot++;
	}

	return TRUE;
}

void CGenericSystemItem::ImportExpansion(void)
{
	CJsonData *pConfig  = m_pDevCon->m_pHConfig->GetConfig();

	CJsonData *pExpand  = NULL;

	CJsonData *pModules = NULL;

	pConfig->AddChild(L"exp", FALSE, pExpand);

	pExpand->AddChild(L"base.modules", TRUE, pModules);

	if( m_pComms->m_pExpansion->m_pRack ) {

		ImportCardListPorts(pModules, m_pComms->m_pExpansion->m_pRack);

		ImportRackListPorts(pExpand, m_pComms->m_pExpansion->m_pTethered->m_pRacks);
	}

	if( m_pComms->m_pRack ) {

		if( m_pComms->m_pRack->m_pPorts->GetItemCount() ) {

			CCommsPort *pPort = m_pComms->m_pRack->m_pPorts->GetItem(0U);

			if( pPort->IsKindOf(AfxRuntimeClass(CCommsPortRack)) ) {

				CCommsPortRack *pRackPort = (CCommsPortRack *) pPort;

				CCommsSlotList *pSlots    = pRackPort->m_pSlots;

				if( UseRedBus() ) {

					ImportRedBusModules(pModules, pRackPort->m_pDevices);
				}
				else {
					ImportRackModules(pModules, pSlots, NOTHING);

					ImportModules(pExpand, pSlots);
				}
			}
		}
	}
}

BOOL CGenericSystemItem::AdjustAlphaModels(void)
{
	CString Model = GetModel();

	if( Model.StartsWith(L"da50x") || Model.StartsWith(L"da70x") ) {

		CString Subst = Model.Left(4);

		Subst += L'|';

		Subst += Model.Mid(5, 2);

		UINT  uGroup = m_pDevCon->GetSoftwareGroup();

		CSize Size   = m_pDevCon->GetDisplaySize();

		Subst += CPrintf(L"|%ux%u|g=%u", Size.cx, Size.cy, uGroup);

		SetModel(Subst);

		return TRUE;
	}

	if( Model.StartsWith(L"Generic") ) {

		// This option is only used during import. It is used to represent
		// a model that we do not have in C32, and it will immediately be
		// converted into a supported device. To make the process work, we
		// need to enhance the model spec to include the display resolution.

		INDEX      hPage  = m_pUI->m_pPages->GetHead();

		CDispPage *pPage  = (CDispPage *) (hPage ? m_pUI->m_pPages->GetItem(hPage) : NULL);

		CString    Subst  = Model;

		CString    Sleds  = L"";

		UINT       uHint  = 70;

		UINT       uGroup = SW_GROUP_3C;

		if( Model.StartsWith(L"Generic|DA50") ) {

			uHint = 50;
		}

		if( Model.StartsWith(L"Generic|DA50") || Model.StartsWith(L"Generic|DA70") ) {

			COptionCardList *pCards = m_pComms->m_pExpansion->m_pRack;

			if( pCards ) {

				UINT uSleds = pCards->GetIndexCount();

				for( UINT uSled = 0; uSled < uSleds; uSled++ ) {

					COptionCardItem *pCard = pCards->GetItem(uSled);

					UINT		 uType = 0;

					switch( pCard->m_Type ) {

						case typeModem:
							uType = 100;
							break;

						case typeWiFi:
							uType = 102;
							break;

						case typeSerial232:
							uType = 103;
							break;

						case typeSerial485:
							uType = 104;
							break;

						case typeSerialMix:
							uType = 105;
							break;
					}

					if( uType ) {

						Sleds += CPrintf(L",s%u=%u", uSled, uType);
					}
				}
			}
		}

		if( pPage ) {

			if( m_pControl->HasControl() ) {

				uGroup = SW_GROUP_4;
			}

			Subst += CPrintf(L"|%ux%u|g=%u,h=%u", pPage->m_Size.cx, pPage->m_Size.cy, uGroup, uHint);

			Subst += Sleds;

			m_pDevCon->SetSoftwareGroup(uGroup);

			SetModel(Subst);

			return TRUE;
		}
		else {
			uGroup = SW_GROUP_2;

			Subst += CPrintf(L"|%ux%u|g=%u,h=%u", 800, 480, uGroup, uHint);

			Subst += Sleds;

			m_pDevCon->SetSoftwareGroup(uGroup);

			SetModel(Subst);

			return TRUE;
		}
	}

	return FALSE;
}

void CGenericSystemItem::WalkModuleTypes(BOOL fCopy)
{
	CJsonData *pConfig = m_pDevCon->m_pHConfig->GetConfig();

	CJsonData *pRlos   = NULL;

	CJsonData *pLinux  = NULL;

	pConfig->AddChild(L"exp.base.modules", TRUE, pRlos);

	pConfig->AddChild(L"modules.mlist", TRUE, pLinux);

	if( pRlos ) {

		UINT c = pRlos->GetCount();

		for( UINT m = 0; m < c; m++ ) {

			CJsonData *pFrom = pRlos->GetChild(m);

			UINT       uType = watoi(pFrom->GetValue(L"type", L"0"));

			if( uType ) {

				if( fCopy ) {

					if( pLinux ) {

						CJsonData *pDest = pLinux->GetChild(m);

						if( !pDest ) {

							for( UINT n = pLinux->GetCount(); n <= m; n++ ) {

								pLinux->AddChild(FALSE, pDest);

								pDest->AddValue(L"order", CPrintf(L"%u", n), jsonString);
							}
						}

						pDest->AddValue(L"type", CPrintf(L"%u", uType), jsonString);
					}
				}
				else {
					switch( uType ) {

						case 210: uType = 200; break;
						case 211: uType = 0;   break;
						case 212: uType = 0;   break;
						case 213: uType = 204; break;
						case 214: uType = 205; break;
						case 301: uType = 204; break;
						case 302: uType = 205; break;
						case 303: uType = 207; break;
						case 304: uType = 201; break;
						case 305: uType = 202; break;
						case 306: uType = 200; break;
						case 307: uType = 206; break;
						case 310: uType = 0;   break;
						case 311: uType = 0;   break;
						case 312: uType = 209; break;
						case 313: uType = 212; break;
						case 314: uType = 207; break;
					}

					pFrom->AddValue(L"type", CPrintf(L"%u", uType), jsonString);
				}
			}
		}
	}

	if( pLinux ) {

		UINT c = pLinux->GetCount();

		for( UINT m = 0; m < c; m++ ) {

			CJsonData *pFrom = pLinux->GetChild(m);

			UINT       uType = watoi(pFrom->GetValue(L"type", L"0"));

			if( uType ) {

				if( fCopy ) {

					if( pRlos ) {

						CJsonData *pDest = pRlos->GetChild(m);

						if( !pDest ) {

							for( UINT n = pRlos->GetCount(); n <= m; n++ ) {

								pRlos->AddChild(FALSE, pDest);

								pDest->AddValue(L"order", CPrintf(L"%u", n), jsonString);
							}
						}

						pDest->AddValue(L"type", CPrintf(L"%u", uType), jsonString);
					}
				}
				else {
					switch( uType ) {

						case 200: uType = 210; break;
						case 201: uType = 0;   break;
						case 202: uType = 0;   break;
						case 203: uType = 212; break;
						case 204: uType = 213; break;
						case 205: uType = 214; break;
						case 206: uType = 0;   break;
						case 207: uType = 0;   break;
						case 208: uType = 211; break;
						case 209: uType = 0;   break;
						case 301: uType = 213; break;
						case 302: uType = 214; break;
						case 303: uType = 0;   break;
						case 304: uType = 0;   break;
						case 305: uType = 0;   break;
						case 306: uType = 0;   break;
						case 307: uType = 0;   break;
						case 308: uType = 0;   break;
						case 309: uType = 0;   break;
						case 310: uType = 0;   break;
						case 311: uType = 0;   break;
						case 312: uType = 0;   break;
						case 313: uType = 212; break;
						case 314: uType = 0;   break;
					}

					pFrom->AddValue(L"type", CPrintf(L"%u", uType), jsonString);
				}
			}
		}
	}
}

BOOL CGenericSystemItem::UseRedBus(void)
{
	CString Model = GetModel();

	if( Model.StartsWith(L"GENERIC|") ) {

		CStringArray List;

		GetModel().Tokenize(List, L'|');

		if( List.GetCount() >= 2 ) {

			CString From = List[1];

			if( From.StartsWith(L"DSP") ) {

				return TRUE;
			}

			if( From.StartsWith(L"MC") ) {

				return TRUE;
			}

			if( From.StartsWith(L"VS") ) {

				return TRUE;
			}

			if( From.StartsWith(L"DA") ) {

				return TRUE;
			}
		}

		return FALSE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

// Meta Data

void CGenericSystemItem::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddInteger(Level);
}

// End of File
