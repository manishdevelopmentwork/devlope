
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CustomDataCache_HPP
	
#define	INCLUDE_CustomDataCache_HPP

//////////////////////////////////////////////////////////////////////////
//
// Custom Data Cache
//

class CCustomDataCache
{
	public:
		// Constructor
		CCustomDataCache(void);

		// Destructor
		~CCustomDataCache(void);

		// Operations
		BOOL LoadFromFile(CString const &Root, CUnicode const &File);

		// Attributes
		UINT  GetChanCount(void);
		DWORD GetPitchTime(void);
		DWORD GetStartTime(void);
		DWORD GetFinalTime(void);

		// Data Access
		BOOL   IsEmpty(void);
		UINT   GetInitSlot(void);
		BOOL   GetNextSlot(UINT &uSlot);
		DWORD  GetSlotTime(UINT uSlot);
		DWORD  GetSlotData(UINT uSlot, UINT uChan);
		PDWORD GetSlotData(UINT uSlot);

		// Log Access
		BOOL   HasCoverage(DWORD dwTime);
		DWORD  GetInitTime(void);

		// Format Access
		DWORD GetChanMin(UINT uChan);
		DWORD GetChanMax(UINT uChan);

	protected:
		// Format
		struct CFormat {

			DWORD m_dwMin;
			DWORD m_dwMax;
			DWORD m_dwDPs;
			};

		// Data Members
		UINT         m_uChans;
		UINT	     m_uBuff;
		UINT	     m_uHead;
		UINT	     m_uTail;
		DWORD        m_dwStart;
		DWORD        m_dwPitch;
		DWORD        m_uCount;
		CFormat    * m_pForm;
		DWORD      * m_pData;

		// Filing System
		IFilingSystem * m_pFileSys;
		IFile         * m_pFile;

		// Filing System Helpers
		BOOL FindFileSystem(char cDrive);
		void FreeFileSystem(void);

		// Implementation
		void Clean(void);
		void AllocBuffer(void);
		void Append(PDWORD pData);
		BOOL LoadFile(void);
		BOOL LoadForm(void);
		BOOL LoadData(void);

		// Development
		void ShowForm(void);
		void ShowData(void);
	};

// End of File

#endif
