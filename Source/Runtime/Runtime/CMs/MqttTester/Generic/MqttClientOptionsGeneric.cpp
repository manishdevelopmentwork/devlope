
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

// Constructor

CMqttClientOptionsGeneric::CMqttClientOptionsGeneric(void)
{
	}

// Initialization

void CMqttClientOptionsGeneric::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	m_PubTopic = UniConvert(GetWide(pData));

	m_SubTopic = UniConvert(GetWide(pData));

	m_NoDollar = GetByte(pData);

	SetDefaults();
	}

// Implementation

void CMqttClientOptionsGeneric::SetDefaults(void)
{
	if( m_PubTopic.IsEmpty() ) {

		m_PubTopic = "$crimson/generic/" + m_ClientId + "/pub";

		if( m_NoDollar ) {

			m_PubTopic.Delete(0, 1);
			}
		}

	if( m_SubTopic.IsEmpty() ) {

		m_SubTopic = "$crimson/generic/" + m_ClientId + "/sub";

		if( m_NoDollar ) {

			m_SubTopic.Delete(0, 1);
			}
		}
	}

// End of File
