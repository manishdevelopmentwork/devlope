
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyPoint_HPP
	
#define	INCLUDE_RubyPoint_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyVector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyMatrix;

//////////////////////////////////////////////////////////////////////////
//
// Point Object
//

class DLLAPI CRubyPoint : public P2R
{
	public:
		// Constructors
		CRubyPoint(void);
		CRubyPoint(P2 const &p);
		CRubyPoint(P2R const &p);
		CRubyPoint(CRubyPoint const &p);
		CRubyPoint(number x, number y);
		CRubyPoint(R2 const &r, int c);

		// Operations
		void Transform(CRubyMatrix const &m);
		void Inverse  (CRubyMatrix const &m);

		// Operators
		CRubyPoint  operator +  (CRubyVector const &v) const;
		CRubyPoint  operator +  (CRubyPoint  const &p) const;
		CRubyPoint  operator -  (CRubyVector const &v) const;
		CRubyVector operator -  (CRubyPoint  const &p) const;
		bool        operator == (CRubyPoint  const &p) const;
		bool        operator != (CRubyPoint  const &p) const;
		CRubyPoint  operator *  (number k) const;
		CRubyPoint  operator /  (number k) const;

		// In-Place Modification
		CRubyPoint const & operator += (CRubyVector const &v);
		CRubyPoint const & operator -= (CRubyVector const &v);

		// Debugging
		void Trace(PCTXT pName) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

inline CRubyPoint::CRubyPoint(void)
{
	}

inline CRubyPoint::CRubyPoint(P2 const &p)
{
	m_x = p.x;

	m_y = p.y;
	}

inline CRubyPoint::CRubyPoint(P2R const &p)
{
	m_x = p.m_x;

	m_y = p.m_y;
	}

inline CRubyPoint::CRubyPoint(CRubyPoint const &p)
{
	m_x = p.m_x;

	m_y = p.m_y;
	}

inline CRubyPoint::CRubyPoint(number x, number y)
{
	m_x = x;
	
	m_y = y;
	}

inline CRubyPoint::CRubyPoint(R2 const &r, int c)
{
	switch( c ) {

		case 1:
			m_x = r.x1;
			m_y = r.y1;
			break;

		case 2:
			m_x = r.x2;
			m_y = r.y2;
			break;

		case 3:
			m_x = r.x2 - 1;
			m_y = r.y2 - 1;
			break;

		case 4:
			m_x = number(r.x2) - number(0.25);
			m_y = number(r.y2) - number(0.25);
			break;

		default:
			m_x = 0;
			m_y = 0;
			break;
		}
	}

// Operators

inline CRubyPoint CRubyPoint::operator + (CRubyVector const &v) const
{
	CRubyPoint p;

	p.m_x = m_x + v.m_x;

	p.m_y = m_y + v.m_y;

	return p;
	}

inline CRubyPoint CRubyPoint::operator + (CRubyPoint const &p) const
{
	CRubyPoint q;

	q.m_x = m_x + p.m_x;

	q.m_y = m_y + p.m_y;

	return q;
	}

inline CRubyPoint CRubyPoint::operator - (CRubyVector const &v) const
{
	CRubyPoint p;

	p.m_x = m_x - v.m_x;

	p.m_y = m_y - v.m_y;

	return p;
	}

inline CRubyVector CRubyPoint::operator - (CRubyPoint const &p) const
{
	CRubyVector v;

	v.m_x = m_x - p.m_x;

	v.m_y = m_y - p.m_y;

	return v;
	}

inline bool CRubyPoint::operator == (CRubyPoint const &p) const
{
	return num_equal(m_x, p.m_x) && num_equal(m_y, p.m_y);
	}

inline bool CRubyPoint::operator != (CRubyPoint const &p) const
{
	return ! operator == (p);
	}

inline CRubyPoint CRubyPoint::operator * (number k) const
{
	CRubyPoint p;

	p.m_x = m_x * k;

	p.m_y = m_y * k;

	return p;
	}

inline CRubyPoint CRubyPoint::operator / (number k) const
{
	CRubyPoint p;

	p.m_x = m_x / k;

	p.m_y = m_y / k;

	return p;
	}

// In-Place Modification

inline CRubyPoint const & CRubyPoint::operator += (CRubyVector const &v)
{
	m_x += v.m_x;

	m_y += v.m_y;

	return ThisObject;
	}

inline CRubyPoint const & CRubyPoint::operator -= (CRubyVector const &v)
{
	m_x -= v.m_x;

	m_y -= v.m_y;

	return ThisObject;
	}

// End of File

#endif
