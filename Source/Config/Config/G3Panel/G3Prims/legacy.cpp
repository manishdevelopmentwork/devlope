
#include "intern.hpp"

#include "legacy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Figure
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyFigure, CPrim);

// Constructor

CPrimLegacyFigure::CPrimLegacyFigure(void)
{
	m_pEdge = New CPrimPen;

	m_pBack = New CPrimBrush;
	}

// UI Creation

BOOL CPrimLegacyFigure::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	LoadTailPages(pList);
	
	return TRUE;
	}

// Overridables

void CPrimLegacyFigure::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;
	
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);
	}

void CPrimLegacyFigure::SetInitState(void)
{
	CPrim::SetInitState();

	m_pEdge->m_Corner = 1;
	
	m_pBack->Set(GetRGB( 8, 8, 8));
	
	m_pEdge->Set(GetRGB(31,31,31));	
	}

// Download Support

BOOL CPrimLegacyFigure::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pEdge);

	Init.AddItem(itemSimple, m_pBack);
	
	return TRUE;
	}

// Meta Data

void CPrimLegacyFigure::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddObject (Edge);
	Meta_AddObject (Back);

	Meta_SetName((IDS_LEGACY_FIGURE));
	}

// Implementation

void CPrimLegacyFigure::LoadHeadPages(CUIPageList *pList)
{
	pList->Append( New CUIStdPage( CString(IDS_OPTIONS),
				       AfxPointerClass(this),
				       1
				       ));
	}

void CPrimLegacyFigure::LoadTailPages(CUIPageList *pList)
{
	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       2
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       3
				       ));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Shadow
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyShadow, CPrimLegacyFigure);

// Constructor

CPrimLegacyShadow::CPrimLegacyShadow(void)
{
	m_uType  = 0x2F;
	
	m_Style  = 0;

	m_Border = 1;
	}

// UI Creation

BOOL CPrimLegacyShadow::OnLoadPages(CUIPageList *pList)
{
	return CPrim::OnLoadPages(pList);
	}

// UI Update

void CPrimLegacyShadow::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Style" ) {

		DoEnables(pHost);
		}

	CPrimLegacyFigure::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimLegacyShadow::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	if( m_Style == 0 ) {

		R2 Back = Rect;

		int s =	2 * m_pEdge->m_Width;

		MakeMax(s, 2);

		Back.x1 += s;
		Back.y1 += s;

		Rect.x2 -= s;
		Rect.y2 -= s;

		m_pEdge->DrawRect(pGDI, Back, uMode);

		m_pBack->FillRect(pGDI, Rect);

		m_pEdge->DrawRect(pGDI, Rect, uMode);
		}
	else {
		COLOR Shadow = GetRGB(15,15,15);

		COLOR Hilite = GetRGB(31,31,31);
		
		P2 h[6];
		
		P2 s[6];

		FindPoints(h, s, Rect);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Shadow);
		
		pGDI->FillPolygon((m_Style == 2) ? h : s, 6, 0);

		pGDI->SetBrushFore(Hilite);

		pGDI->FillPolygon((m_Style == 2) ? s : h, 6, 0);
		
		m_pBack->FillRect(pGDI, Rect);
		}
	}

// Download Support

BOOL CPrimLegacyShadow::MakeInitData(CInitData &Init)
{
	CPrimLegacyFigure::MakeInitData(Init);

	Init.AddByte(BYTE(m_Style));

	Init.AddByte(BYTE(m_Border));

	return TRUE;
	}

// Meta Data

void CPrimLegacyShadow::AddMetaData(void)
{
	CPrimLegacyFigure::AddMetaData();

	Meta_AddInteger(Style);
	Meta_AddInteger(Border);

	Meta_SetName((IDS_LEGACY_SHADOW));
	}

// Implementation

void CPrimLegacyShadow::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(m_pEdge, L"Color", m_Style == 0);
	}

void CPrimLegacyShadow::FindPoints(P2 *t, P2 *b, R2 &r)
{
	R2 Rect1 = r;

	R2 Rect2 = r;

	DeflateRect(Rect2, m_Border - 1, m_Border - 1);

	Rect1.x2--;
	Rect1.y2--;
	Rect2.x2--;
	Rect2.y2--;

	t[0].x = Rect1.x1; t[0].y = Rect1.y2;
	t[1].x = Rect1.x1; t[1].y = Rect1.y1;
	t[2].x = Rect1.x2; t[2].y = Rect1.y1;
	t[3].x = Rect2.x2; t[3].y = Rect2.y1;
	t[4].x = Rect2.x1; t[4].y = Rect2.y1;
	t[5].x = Rect2.x1; t[5].y = Rect2.y2;

	b[0].x = Rect1.x1; b[0].y = Rect1.y2;
	b[1].x = Rect1.x2; b[1].y = Rect1.y2;
	b[2].x = Rect1.x2; b[2].y = Rect1.y1;
	b[3].x = Rect2.x2; b[3].y = Rect2.y1;
	b[4].x = Rect2.x2; b[4].y = Rect2.y2;
	b[5].x = Rect2.x1; b[5].y = Rect2.y2;

	Rect2.x1 += 1;
	Rect2.y1 += 1;

	r = Rect2;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Scale
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyScale, CPrim);

// Constructor

CPrimLegacyScale::CPrimLegacyScale(void)
{
	m_pLine     = New CPrimColor;

	m_pBack     = New CPrimBrush;

	m_Style     = 0;
	
	m_Orient    = 0;

	m_Major     = 5;

	m_Minor     = 2;

	m_Interval  = 1;

	m_pLimitMin = NULL;
	
	m_pLimitMax = NULL;
	}

// UI Update

void CPrimLegacyScale::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Style" ) {

		if( m_Style == 0 ) {

			KillCoded(pHost, L"LimitMin", m_pLimitMin);

			KillCoded(pHost, L"LimitMax", m_pLimitMax);
			}
		else {
			InitCoded(pHost, L"LimitMin", m_pLimitMin, L"0");

			InitCoded(pHost, L"LimitMax", m_pLimitMax, L"100");
			}

		DoEnables(pHost);
		}

	CPrim::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimLegacyScale::SetInitState(void)
{
	CPrim::SetInitState();
	
	m_pLine->Set(GetRGB(31,31,31));

	m_pBack->Set(GetRGB( 8, 8, 8));
	}

// Download Support

BOOL CPrimLegacyScale::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pLine);

	Init.AddItem(itemSimple, m_pBack);
	
	Init.AddByte(BYTE(m_Orient));
	
	Init.AddByte(BYTE(m_Major));
	
	Init.AddByte(BYTE(m_Minor));

	Init.AddByte(BYTE(m_Style));

	if( m_Style ) {
	
		Init.AddItem(itemVirtual, m_pLimitMin);

		Init.AddItem(itemVirtual, m_pLimitMax);
		
		Init.AddByte(BYTE(m_Interval));
		}
	
	return TRUE;
	}

// Meta Data

void CPrimLegacyScale::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddObject (Line);
	Meta_AddObject (Back);
	Meta_AddInteger(Style);
	Meta_AddInteger(Orient);
	Meta_AddInteger(Major);
	Meta_AddInteger(Minor);
	Meta_AddVirtual(LimitMin);
	Meta_AddVirtual(LimitMax);
	Meta_AddInteger(Interval);

	Meta_SetName((IDS_SIMPLE_SCALE));
	}

// Implementation

void CPrimLegacyScale::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("LimitMin",    m_Style == 1);

	pHost->EnableUI("LimitMax",    m_Style == 1);

	pHost->EnableUI("Interval",    m_Style == 1);

	pHost->EnableUI("Major",       m_Style == 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Vertical Scale
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyVertScale, CPrimLegacyScale);

// Constructor

CPrimLegacyVertScale::CPrimLegacyVertScale(void)
{
	m_uType = 0x22;
	}

// Overridables

void CPrimLegacyVertScale::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);

	pGDI->SetBrushFore(m_pLine->GetColor());
	
	int xp = Rect.x1;

	int yp = Rect.y1;

	int cx = Rect.x2 - Rect.x1;

	int cy = Rect.y2 - Rect.y1;

	if( m_Orient ) {

		pGDI->FillRect(xp + cx - 1, yp, xp + cx, yp + cy);
		}
	else
		pGDI->FillRect(xp, yp, xp + 1, yp + cy);
	
	int xMajor = cx;
	
	int xMinor = max(cx / 2, 2);
	
	int   nMax = m_pLimitMax ? m_pLimitMax->ExecVal() : 100;

	int   nMin = m_pLimitMin ? m_pLimitMin->ExecVal() : 0;

	int nMajor = m_Style ? ((nMax - nMin) / m_Interval) : m_Major;

	int nMinor = m_Minor;

	MakeMax(nMajor, 1);

	int nDiv = nMajor * nMinor;

	for( int nPos = 0; nPos <= nDiv; nPos++ ) {

		int yd = yp + MulDivRound(cy - 1, nPos, nDiv);

		int xs = (nPos % nMinor) ? xMinor : xMajor;

		if( m_Orient ) {

			pGDI->FillRect(xp + cx - xs, yd, xp + cx, yd + 1);
			}
		else
			pGDI->FillRect(xp, yd, xp + xs, yd + 1);
		}
	}

void CPrimLegacyVertScale::SetInitState(void)
{
	CPrimLegacyScale::SetInitState();

	SetInitSize(49, 161);
	}

// Meta Data

void CPrimLegacyVertScale::AddMetaData(void)
{
	CPrimLegacyScale::AddMetaData();

	Meta_SetName((IDS_SIMPLE_VERT_SCALE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Horizontal Scale
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyHorzScale, CPrimLegacyScale);

// Constructor

CPrimLegacyHorzScale::CPrimLegacyHorzScale(void)
{
	m_uType = 0x23;
	}

// Overridables

void CPrimLegacyHorzScale::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);

	pGDI->SetBrushFore(m_pLine->GetColor());
	
	int xp = Rect.x1;

	int yp = Rect.y1;

	int cx = Rect.x2 - Rect.x1;

	int cy = Rect.y2 - Rect.y1;

	int yMajor = cy;
	
	int yMinor = max(cy / 2, 2);

	if( m_Orient ) {

		pGDI->FillRect(xp, yp + cy - 1, xp + cx, yp + cy);
		}
	else
		pGDI->FillRect(xp, yp, xp + cx, yp + 1);

	int nMax = m_pLimitMax ? m_pLimitMax->ExecVal() : 100;

	int nMin = m_pLimitMin ? m_pLimitMin->ExecVal() : 0;

	int nMajor = m_Style ? ((nMax - nMin) / m_Interval) : m_Major;

	int nMinor = m_Minor;

	MakeMax(nMajor, 1);

	int nDiv = nMajor * nMinor;

	for( int nPos = 0; nPos <= nDiv; nPos++ ) {

		int xd = xp + MulDivRound(cx - 1, nPos, nDiv);

		int ys = (nPos % nMinor) ? yMinor : yMajor;

		if( m_Orient ) {

			pGDI->FillRect(xd, yp + cy - ys, xd + 1, yp + cy);
			}
		else
			pGDI->FillRect(xd, yp, xd + 1, yp + ys);
		}
	}

void CPrimLegacyHorzScale::SetInitState(void)
{
	CPrimLegacyScale::SetInitState();

	SetInitSize(161, 49);
	}

// Meta Data

void CPrimLegacyHorzScale::AddMetaData(void)
{
	CPrimLegacyScale::AddMetaData();

	Meta_SetName((IDS_SIMPLE_HORZ_SCALE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy CF Image
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyCfImage, CPrimLegacyFigure);

// Constructor

CPrimLegacyCfImage::CPrimLegacyCfImage(void)
{
	m_uType  = 0x27;
	
	m_pValue = NULL;
	
	m_pShow  = NULL;

	m_pData  = NULL;
	}

// Destructor

CPrimLegacyCfImage::~CPrimLegacyCfImage(void)
{
	FreePreview();
	}

// Overridables

void CPrimLegacyCfImage::Draw(IGDI *pGDI, UINT uMode)
{
	CPrimLegacyFigure::Draw(pGDI, uMode);

	if( IsSupported() ) {

		if( LoadPreview() ) {

			int xp = (m_DrawRect.x1 + m_DrawRect.x2 - m_cx) / 2;

			int yp = (m_DrawRect.y1 + m_DrawRect.y2 - m_cy) / 2;

			if( xp >= m_DrawRect.x1 && yp >= m_DrawRect.y1 ) {

				pGDI->BitBlt(xp, yp, m_cx, m_cy, m_s, m_pFrom, m_rop);
				}
			else {
				CString Text(IDS_CF_IMAGE_TOO_SMALL);

				ShowMessage(pGDI, Text);
				}
			}
		}
	else {
		DrawNotSupported(pGDI);
		}
	}

void CPrimLegacyCfImage::SetInitState(void)
{
	CPrimLegacyFigure::SetInitState();

	SetInitSize(320, 240);
	}

BOOL CPrimLegacyCfImage::IsSupported(void)
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3A;
	}

// Type Access

BOOL CPrimLegacyCfImage::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Value" ) {
		
		Type.m_Type  = typeInteger;

		Type.m_Flags = flagInherent;

		return TRUE;
		}

	if( Tag == L"Show" ) {
		
		Type.m_Type  = typeLogical;

		Type.m_Flags = flagInherent;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Download Support

BOOL CPrimLegacyCfImage::MakeInitData(CInitData &Init)
{
	CPrimLegacyFigure::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);
	
	Init.AddItem(itemVirtual, m_pShow);

	return TRUE;
	}

// Meta Data

void CPrimLegacyCfImage::AddMetaData(void)
{
	CPrimLegacyFigure::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddVirtual(Show);

	Meta_SetName((IDS_CF_IMAGE));
	}

// Implementation

BOOL CPrimLegacyCfImage::LoadPreview(void)
{
	// REV3 -- Cache this information to avoid reload.

	if( m_pValue ) {

		UINT	       uImage   = m_pValue->ExecVal();

		CUISystem     *pSystem  = (CUISystem *) m_pDbase->GetSystemItem();

		CImageManager *pManager = pSystem->m_pUI->m_pImages;

		CFilename      Path     = pManager->m_CfPath;

		Path += CPrintf(L"\\pic%3.3u.g3p", uImage);

		HANDLE hFile = Path.OpenReadSeq();

		if( hFile != INVALID_HANDLE_VALUE ) {

			UINT   uSize = GetFileSize(hFile, NULL);

			PBYTE  pData = New BYTE [ uSize ];

			DWORD  uRead = 0;

			ReadFile(hFile, pData, uSize, &uRead, NULL);

			if( uRead == uSize ) {

				if( !strncmp(PCSTR(pData), "G3GMP", 5) ) {

					if( LoadColorRGB888(pData) ) {

						CloseHandle(hFile);

						return TRUE;
						}
					}
				}				

			delete [] pData;

			CloseHandle(hFile);
			}
		}

	return FALSE;
	}

void CPrimLegacyCfImage::FreePreview(void)
{
	delete [] m_pData;

	m_pData = NULL;
	}

BOOL CPrimLegacyCfImage::LoadColorRGB888(PBYTE pData)
{
	FreePreview();

	m_s     = PWORD(pData+6)[0] / 2;

	m_cx    = PWORD(pData+6)[1];
	
	m_cy    = PWORD(pData+6)[2];

	m_rop   = ropSRCCOPY | ((pData[5] & 2) ? ropTrans : 0);

	BOOL fPad = ((m_s % 4) != 0);

	m_s = (m_s + 3) & ~3;

	m_pData = pData;

	m_pFrom = pData + 12;

	UINT uSize = m_s * m_cy;

	UINT uData = m_cx * m_cy;

	PDWORD pWork = PDWORD(m_pFrom);

	PWORD  pDest = new WORD[ uSize ];

	int x = 0, y = 0;

	for( UINT n = 0, m = 0; n < uData; n++, m++ ) {

		if( ++x == m_cx ) {

			x = 0;

			y++;

			if( fPad ) {

				pDest[m++] = 0;			

				continue;
				}
			}

		if( m_rop & ropTrans ) {

			if( !(pWork[n] & 0xFF000000) ) {

				pDest[m] = GetRGB(0x1F, 0x00, 0x1F);

				continue;
				}
			}
		
		BYTE r = BYTE((pWork[n] & 0x000000FF) >>  3);
		BYTE g = BYTE((pWork[n] & 0x0000FF00) >> 11);
		BYTE b = BYTE((pWork[n] & 0x00FF0000) >> 19);

		pDest[m] = GetRGB(r, g, b);
		}

	delete [] m_pData;

	m_pData  = PBYTE(pDest);

	m_pFrom  = m_pData;

	// cppcheck-suppress memleak

	return TRUE;
	}

void CPrimLegacyCfImage::DrawNotSupported(IGDI *pGDI)
{
	if( !IsSupported() ) {

		R2 Work = m_DrawRect;

		CString Text = IDS_NOT_SUPPORTED;

		pGDI->SelectFont(fontHei16);

		int cx = pGDI->GetTextWidth(Text);

		int cy = pGDI->GetTextHeight(Text);
		
		int px = (Work.x1 + Work.x2 - cx) / 2;
		
		int py = (Work.y1 + Work.y2 - cy) / 2;

		pGDI->SetTextFore(GetRGB(255,0,0));

		pGDI->SetTextBack(GetRGB(8,8,8));

		pGDI->TextOut(px, py, Text);
		}
	}

void CPrimLegacyCfImage::ShowMessage(IGDI *pGDI, PCTXT pMsg)
{
	pGDI->SelectFont(fontHei16);

	int yt = (m_DrawRect.y1 + m_DrawRect.y2 - pGDI->GetTextHeight(pMsg)) / 2;

	int xt = (m_DrawRect.x1 + m_DrawRect.x2 - pGDI->GetTextWidth(pMsg) ) / 2;

	pGDI->SetTextFore(GetRGB(255,0,0));

	pGDI->SetTextBack(GetRGB(8,8,8));

	pGDI->TextOut(xt, yt, pMsg);
	}

// End of File
