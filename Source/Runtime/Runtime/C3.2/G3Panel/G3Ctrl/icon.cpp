
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Icon Button Control
//

class CIconButton : public CStdButton
{
	public:
		// Constructor
		CIconButton(void);

		// Destructor
		~CIconButton(void);

		// Messages
		UINT OnMessage(UINT uCode, UINT uParam);

	protected:
		// Overridables
		void OnDraw    (IGdi *pGDI);
		void OnValidate(void);

		// Icon Types
		void DrawUpArrow   (IGdi *pGDI, int x1, int y1, int x2, int y2);
		void DrawDownArrow (IGdi *pGDI, int x1, int y1, int x2, int y2);
		void DrawLeftArrow (IGdi *pGDI, int x1, int y1, int x2, int y2);
		void DrawRightArrow(IGdi *pGDI, int x1, int y1, int x2, int y2);
		void DrawBackSpace (IGdi *pGDI, int x1, int y1, int x2, int y2);
		void DrawEnter     (IGdi *pGDI, int x1, int y1, int x2, int y2);
		void DrawExit      (IGdi *pGDI, int x1, int y1, int x2, int y2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Icon Button Control
//

// Instantiator

IButton * Create_IconButton(void)
{
	return New CIconButton;
	}

// Constructor

CIconButton::CIconButton(void)
{
	SetText(L"R");
	}

// Destructor

CIconButton::~CIconButton(void)
{
	}

// Messages

UINT CIconButton::OnMessage(UINT uCode, UINT uParam)
{
	UINT uMessage = LOWORD(uCode);

	BOOL fLocal   = HIWORD(uCode);

	if( uMessage == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uMessage == msgTouchDown ) {

		m_fPress = TRUE;

		m_fDirty = TRUE;

		if( !(m_uFlags & bsLatch) ) {

			SendNotify(uCode, 0);
			}

		return TRUE;
		}

	if( m_fPress ) {

		if( uMessage == msgTouchRepeat ) {

			if( !(m_uFlags & bsLatch) ) {

				SendNotify(uCode, 0);

				return TRUE;
				}
	
			return FALSE;
			}

		if( uMessage == msgTouchUp ) {

			m_fPress = FALSE;

			m_fDirty = TRUE;

			if( m_uFlags & bsLatch ) {

				m_uState = !m_uState;

				SendNotify(0, m_uState);
				}
			else
				SendNotify(uCode, 0);

			return TRUE;
			}
		}

	return FALSE;
	}

// Overridables

void CIconButton::OnDraw(IGdi *pGDI)
{
	pGDI->ResetAll();

	R2 Rect = m_Rect;

	if( m_uFlags & bsThick ) {

		pGDI->SetPenWidth(3);

		DeflateRect(Rect, 1, 1);
		}

	pGDI->SetPenFore  (m_fEnable ? m_colBorder : m_colDisabled);

	pGDI->ShadeRounded(PassRect(Rect), 8, GetShader(m_uState));
	
	pGDI->DrawRounded (PassRect(Rect), 8);

	pGDI->SetBrushFore(m_fEnable ? m_colText : m_colDisabled);

	pGDI->SetPenFore  (m_fEnable ? m_colText : m_colDisabled);

	int bs = 2 + (Rect.x2 - Rect.x1) / 8;

	int x1 = Rect.x1 + bs;
	int y1 = Rect.y1 + bs;
	int x2 = Rect.x2 - bs;
	int y2 = Rect.y2 - bs;

	if( FALSE && islower(m_sText[0]) ) {

		x1 += 2;
		y1 += 2;
		x2 -= 2;
		y2 -= 2;
		}

	switch( toupper(m_sText[0]) ) {

		case 'U': DrawUpArrow   (pGDI, x1, y1, x2, y2); break;
		case 'D': DrawDownArrow (pGDI, x1, y1, x2, y2); break;
		case 'L': DrawLeftArrow (pGDI, x1, y1, x2, y2); break;
		case 'R': DrawRightArrow(pGDI, x1, y1, x2, y2); break;
		case 'B': DrawBackSpace (pGDI, x1, y1, x2, y2); break;
		case 'E': DrawEnter     (pGDI, x1, y1, x2, y2); break;
		case 'X': DrawExit      (pGDI, x1, y1, x2, y2); break;
		}
	}

void CIconButton::OnValidate(void)
{
	m_uState = m_uState ? 1 : 0;
	}

// Icon Types

void CIconButton::DrawUpArrow(IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	P2 p[3];

	int xc = (x1+x2)/ 2;

	p[0].x = x1; p[0].y = y2;
	p[1].x = xc; p[1].y = y1;
	p[2].x = x2; p[2].y = y2;

	pGDI->FillPolygon(p, 3, 0);
	}

void CIconButton::DrawDownArrow(IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	P2 p[3];

	int xc = (x1+x2)/ 2;

	p[0].x = x1; p[0].y = y1;
	p[1].x = xc; p[1].y = y2;
	p[2].x = x2; p[2].y = y1;

	pGDI->FillPolygon(p, 3, 0);
	}

void CIconButton::DrawLeftArrow (IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	P2 p[3];

	int yc = (y1+y2)/2;

	p[0].x = x2; p[0].y = y1;
	p[1].x = x1; p[1].y = yc;
	p[2].x = x2; p[2].y = y2;

	pGDI->FillPolygon(p, 3, 0);
	}

void CIconButton::DrawRightArrow(IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	P2 p[3];

	int yc = (y1+y2)/2;

	p[0].x = x1; p[0].y = y1;
	p[1].x = x2; p[1].y = yc;
	p[2].x = x1; p[2].y = y2;

	pGDI->FillPolygon(p, 3, 0);
	}

void CIconButton::DrawEnter(IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	int w = (m_uFlags & bsBold) ? 7 : 3;

	int s = w / 2;

	int a = w + 1;

	pGDI->SetPenWidth(w);
			   
	int cx = (x2-x1-w)/2 - 0;
	int cy = (y2-y1-w)/4 - 1;
			   
	int xc = (x1+x2-0)/2 - 1;
	int yc = (y1+y2-a)/2 - 0;

	pGDI->SetPenCaps(capsEnd);

	pGDI->DrawLine  (xc-cx, yc+cy, xc+cx, yc+cy);

	pGDI->SetPenCaps(capsNone);

	pGDI->DrawLine  (xc+cx, yc+cy, xc+cx, yc-cy);

	pGDI->FillWedge (xc-cx-s, yc+cy-a, xc-cx-s+1+a, yc+cy+0+1, etQuad4); 

	pGDI->FillWedge (xc-cx-s, yc+cy+0, xc-cx-s+1+a, yc+cy+a+1, etQuad1); 
	}

void CIconButton::DrawBackSpace(IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	int w = (m_uFlags & bsBold) ? 7 : 3;

	int s = w - 1;

	int a = w + 1;

	pGDI->SetPenWidth(w);

	int cx = (x2-x1-w)/2 - 6;
	int cy = (y2-y1-w)/2 - 0;

	int xc = (x1+x2)/2 - 0;
	int yc = (y1+y2)/2 - 0;

	pGDI->SetPenCaps(capsNone);

	pGDI->DrawLine  (xc-cx, yc, xc+cx, yc);

	pGDI->FillWedge (xc-cx-s, yc-a, xc-cx-s+2+2*a, yc+0+1, etQuad4); 

	pGDI->FillWedge (xc-cx-s, yc+0, xc-cx-s+2+2*a, yc+a+1, etQuad1); 
	}

void CIconButton::DrawExit(IGdi *pGDI, int x1, int y1, int x2, int y2)
{
	int w = (m_uFlags & bsBold) ? 7 : 3;

	pGDI->SetPenWidth(w);

	int cx = (x2-x1-w)/2 - 1;
	int cy = (y2-y1-w)/2 - 1;

	int dd = min(cx,cy)+1;

	int yc = (y1+y2)/2;
	int xc = (x1+x2)/2;

	pGDI->SetPenCaps (capsNone);

	pGDI->DrawLine   (xc-dd, yc-dd, xc+dd+1, yc+dd+1);

	pGDI->DrawLine   (xc-dd, yc+dd, xc+dd+1, yc-dd-1);
	}

// End of File
