
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SymFactLibrary_HPP

#define INCLUDE_SymFactLibrary_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSymFactCategory;

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Library
//

class CSymFactLibrary
{
	public:
		// Constructor
		CSymFactLibrary(void);

		// Destructor
		~CSymFactLibrary(void);

		// Indexing
		CSymFactCategory * const operator [] (UINT n) const;

		// Attributes
		UINT GetCatCount(void) const;

	protected:
		// Data Members
		CSymFactCategory * m_pCats;
		UINT               m_uCount;

		// Operations
		void LoadLibrary(void);
		void LoadHelper(void);
	};

// End of File

#endif
