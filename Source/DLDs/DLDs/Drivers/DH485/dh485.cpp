
#include "intern.hpp"

#include "dh485.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../Df1Shared/df1m.cpp"

//////////////////////////////////////////////////////////////////////////
//
// DH485 Driver
//

// Instantiator

INSTANTIATE(CDH485Driver);

// Constructor

CDH485Driver::CDH485Driver(void)
{
	m_Ident = DRIVER_ID;

	m_bThis = 3;

	m_bLast = 31;

	m_fMake = FALSE;

	m_pCtx  = NULL;
}

// Destructor

CDH485Driver::~CDH485Driver(void)
{
}

// Configuration

void MCALL CDH485Driver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bThis = GetByte(pData);

		m_bLast = GetByte(pData);

		m_fMake = GetByte(pData);

		MakeMax(m_bLast, m_bThis);
	}
}

void MCALL CDH485Driver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uFlags |= flagFastRx;

	Make485(Config, TRUE);
}

// Management

void MCALL CDH485Driver::Attach(IPortObject *pPort)
{
	m_pHandler = new CDH485Handler(m_pHelper, m_bThis, m_bLast, m_fMake);

	pPort->Bind(m_pHandler);
}

void MCALL CDH485Driver::Detach(void)
{
	m_pHandler->Release();
}

void MCALL CDH485Driver::Open(void)
{
}

// Device

CCODE MCALL CDH485Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_uHeader = headByte;

			m_pCtx->m_bDest   = 1;

			m_pCtx->m_bDevice = GetByte(pData) + 1;

			m_pCtx->m_bDrop   = GetByte(pData);

			m_pCtx->m_wTrans = WORD(RAND(m_pCtx->m_bDrop + 1));

			m_pCtx->m_uCache  = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
}

CCODE MCALL CDH485Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Transport Layer

BOOL CDH485Driver::CheckLink(void)
{
	if( m_pHandler->HadToken() ) {

		if( m_pHandler->IsActive(m_pCtx->m_bDrop) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CDH485Driver::Transact(void)
{
	DF1HEADBYTE &TxHead = (DF1HEADBYTE &) m_bTxBuff[0];

	DF1HEADBYTE &RxHead = (DF1HEADBYTE &) m_bRxBuff[0];

	TxHead.bCount = BYTE(m_uPtr - 3);

	if( m_pHandler->PutAppData(m_pCtx->m_bDrop, m_bTxBuff, m_uPtr) ) {

		UINT uTime = 5000;

		UINT uSize = 0;

		for( ;;) {

			uSize = sizeof(m_bRxBuff);

			uSize = m_pHandler->GetAppData(m_bRxBuff, uSize, uTime);

			if( uSize > 4 && !RxHead.bStatus ) {

				if( RxHead.wTrans == TxHead.wTrans ) {

					return TRUE;
				}

				uTime = 1000;

				continue;
			}

			return FALSE;
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// DH485 Data Handler
//

// Event Codes

#define	EVENT_START	1
#define	EVENT_FRAME	2
#define	EVENT_TIMEOUT	3
#define	EVENT_ERROR	4

// Operation Codes

#define	OP_PASS		0x00
#define	OP_INVITE	0x01
#define	OP_RESET	0x02
#define	OP_ACK		0x18
#define	OP_BUSY		0x1A
#define	OP_DATA1	0x88
#define	OP_DATA2	0x08

// MAC Status Codes

#define	MS_VIRGIN	0x00
#define	MS_RESET	0x01
#define	MS_IDLE		0x02
#define	MS_WAIT_ACK	0x03
#define	MS_WAIT_ACCEPT	0x04
#define	MS_CHECK	0x05
#define	MS_DUPLICATE	0x06

// Constructor

CDH485Handler::CDH485Handler(IHelper *pHelper, BYTE bThis, BYTE bLast, BOOL fMake)
{
	StdSetRef();

	m_pHelper     = pHelper;

	m_bThisDrop   = bThis;

	m_bLastDrop   = bLast;

	m_fMake       = fMake;

	m_pPort       = NULL;

	m_bInvite     = 0;

	m_uRxState    = 0;

	m_fToggle     = 0;

	m_fSendInvite = FALSE;

	m_fSeenInvite = FALSE;

	m_uToken      = 0;

	m_uTxAppCount = 0;

	m_pRxAppFlag  = NULL;

	CreateSyncObject();

	ResetMac();

	CalibDelay();
}

// Destructor

CDH485Handler::~CDH485Handler(void)
{
	m_pRxAppFlag->Release();
}

// Operations

BOOL CDH485Handler::PutAppData(BYTE bDrop, PCBYTE pData, UINT uCount)
{
	if( m_dwActive & (1L << bDrop) ) {

		m_uTxAppCount = 0;

		m_fWaitReply  = FALSE;

		memcpy(m_bTxApp, pData, uCount);

		m_pRxAppFlag->Clear();

		m_uRxAppCount = 0;

		m_bTxAppDrop  = bDrop;

		m_uTxAppCount = uCount;

		return TRUE;
	}

	return FALSE;
}

UINT CDH485Handler::GetAppData(PBYTE pData, UINT uCount, UINT uWait)
{
	if( m_pRxAppFlag->Wait(uWait) ) {

		UINT uData = min(uCount, m_uRxAppCount);

		if( pData ) {

			memcpy(pData, m_bRxApp, uData);
		}

		m_uRxAppCount = 0;

		return uData;
	}

	return 0;
}

// Attributes

DWORD CDH485Handler::GetActiveList(void) const
{
	return m_dwActive | (1L << m_bThisDrop);
}

BOOL CDH485Handler::IsActive(BYTE bDrop) const
{
	return !!(m_dwActive & (1L << bDrop));
}

BOOL CDH485Handler::HadToken(void) const
{
	return m_uToken >= 4;
}

// IUnknown

HRESULT CDH485Handler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG CDH485Handler::AddRef(void)
{
	StdAddRef();
}

ULONG CDH485Handler::Release(void)
{
	StdRelease();
}

// Binding

void MCALL CDH485Handler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
}

// Event Handlers

void MCALL CDH485Handler::OnOpen(CSerialConfig const &Config)
{
}

void MCALL CDH485Handler::OnClose(void)
{
}

void MCALL CDH485Handler::OnTimer(void)
{
	if( /*!m_fTxEnable && */m_uTimer ) {

		if( !--m_uTimer ) {

			Signal(EVENT_TIMEOUT);
		}
	}

	if( m_uTxHack ) {

		if( !--m_uTxHack ) {

			Signal(EVENT_TIMEOUT);
		}
	}
}

BOOL MCALL CDH485Handler::OnTxData(BYTE &bData)
{
	if( m_fTxEnable ) {

		if( m_uTxScan < m_uTxPtr ) {

			bData = m_bTxBuff[m_uTxScan++];

			return TRUE;
		}

		m_fTxEnable = FALSE;

		m_uTxHack   = 0;

		m_uRxState  = 0;
	}

	return FALSE;
}

void MCALL CDH485Handler::OnTxDone(void)
{
}

void MCALL CDH485Handler::OnRxData(BYTE bData)
{
	RxByte(bData);
}

void MCALL CDH485Handler::OnRxDone(void)
{
}

// Frame Assembly

void CDH485Handler::StartFrame(BYTE bDest, BYTE bCode)
{
	m_uTxPtr = 0;

	m_bTxBuff[m_uTxPtr++] = DLE;

	m_bTxBuff[m_uTxPtr++] = STX;

	m_CRC.Clear();

	AddByte(bDest | 0x80);

	AddByte(bCode);

	AddByte(m_bThisDrop | 0x80);
}

void CDH485Handler::AddByte(BYTE bData)
{
	m_bTxBuff[m_uTxPtr++] = bData;

	if( bData == DLE ) {

		m_bTxBuff[m_uTxPtr++] = bData;
	}

	m_CRC.Add(bData);
}

void CDH485Handler::SendFrame(void)
{
	TxDelay();

	m_CRC.Add(ETX);

	m_bTxBuff[m_uTxPtr++] = DLE;

	m_bTxBuff[m_uTxPtr++] = ETX;

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	m_fTxEnable = TRUE;

	m_uTxHack   = ToTicks(250);

	m_uTxScan   = 0;

	m_pPort->Send(PAD);
}

// Receive Machine

void CDH485Handler::RxByte(BYTE bData)
{
	switch( m_uRxState ) {

		case 0:
			if( bData == DLE ) {

				m_uRxState = 1;
			}
			break;

		case 1:
			if( bData == STX ) {

				m_CRC.Clear();

				m_uRxPtr   = 0;

				m_uRxState = 2;

				Signal(EVENT_START);
			}
			else {
				m_uRxState = 0;
			}

			break;

		case 2:
			if( bData == DLE ) {

				m_uRxState = 3;
			}
			else {
				m_bRxBuff[m_uRxPtr++] = bData;

				if( m_uRxPtr >= sizeof(m_bRxBuff) ) {

					m_uRxState = 0;

					return;
				}

				m_CRC.Add(bData);
			}
			break;

		case 3:
			switch( bData ) {

				case ETX:
					m_CRC.Add(ETX);

					m_uRxState = 4;

					break;

				case DLE:
					m_bRxBuff[m_uRxPtr++] = DLE;

					if( m_uRxPtr >= sizeof(m_bRxBuff) ) {

						m_uRxState = 0;

						return;
					}

					m_CRC.Add(DLE);

					m_uRxState = 2;

					break;

				case STX:
					m_CRC.Clear();

					m_uRxPtr   = 0;

					m_uRxState = 2;

					Signal(EVENT_START);

					break;

				default:
					m_uRxState = 0;

					break;
			}
			break;

		case 4:
			if( bData == DLE ) {

				m_uRxState = 5;
			}
			else {
				m_wRxCheck = bData;

				m_uRxState = 6;
			}

			break;

		case 5:
			if( bData == DLE ) {

				m_wRxCheck = bData;

				m_uRxState = 6;
			}
			else {
				m_uRxState = 0;
			}

			break;

		case 6:
			if( bData == DLE ) {

				m_uRxState = 7;
			}
			else {
				m_wRxCheck = MAKEWORD(m_wRxCheck, bData);

				CheckFrame();

				m_uRxState = 0;
			}
			break;

		case 7:
			if( bData == DLE ) {

				m_wRxCheck = MAKEWORD(m_wRxCheck, bData);

				CheckFrame();

				m_uRxState = 0;
			}
			else {
				m_uRxState = 0;
			}

			break;
	}
}

void CDH485Handler::CheckFrame(void)
{
	if( m_wRxCheck == m_CRC.GetValue() ) {

		BYTE bFrom = (m_bRxBuff[2] & 0x7F);

		if( bFrom == m_bThisDrop ) {

			m_uMacState = MS_DUPLICATE;

			return;
		}

		m_dwWork |= (1L << bFrom);

		Signal(EVENT_FRAME);

		return;
	}

	Signal(EVENT_ERROR);
}

// Event Handlers

void CDH485Handler::Signal(UINT uEvent)
{
	switch( m_uMacState ) {

		case MS_VIRGIN:
			HandleVirgin(uEvent);
			break;

		case MS_RESET:
			HandleReset(uEvent);
			break;

		case MS_IDLE:
			HandleIdle(uEvent);
			break;

		case MS_WAIT_ACK:
			HandleWaitAck(uEvent);
			break;

		case MS_WAIT_ACCEPT:
			HandleWaitAccept(uEvent);
			break;

		case MS_CHECK:
			HandleCheck(uEvent);
			break;
	}
}

void CDH485Handler::HandleVirgin(UINT uEvent)
{
	if( uEvent == EVENT_FRAME ) {

		if( MatchDrop() ) {

			switch( m_bRxBuff[1] ) {

				case OP_INVITE:
					AcceptInvite();
					return;
			}
		}

		ResetMac();
	}

	if( uEvent == EVENT_TIMEOUT ) {

		if( m_fMake ) {

			SendReset();

			return;
		}

		return;
	}
}

void CDH485Handler::HandleReset(UINT uEvent)
{
	if( uEvent == EVENT_FRAME ) {

		ResetMac();

		return;
	}

	if( uEvent == EVENT_TIMEOUT ) {

		m_bNextDrop   = m_bThisDrop;

		m_fSeenInvite = FALSE;

		m_fSendInvite = FALSE;

		DoInvite();
	}
}

void CDH485Handler::HandleIdle(UINT uEvent)
{
	if( uEvent == EVENT_FRAME ) {

		if( MatchDrop() ) {

			switch( m_bRxBuff[1] ) {

				case OP_INVITE:
					AcceptInvite();
					break;

				case OP_PASS:
					TakeToken();
					break;

				case OP_DATA1:
				case OP_DATA2:
					TakeData();
					break;
			}
		}
		else {
			switch( m_bRxBuff[1] ) {

				case OP_INVITE:
					SeenInvite();
					break;
			}

			GoIdle();
		}

		return;
	}

	if( uEvent == EVENT_TIMEOUT ) {

		ResetMac();

		return;
	}
}

void CDH485Handler::HandleWaitAck(UINT uEvent)
{
	if( uEvent == EVENT_FRAME ) {

		if( MatchDrop() ) {

			if( m_bRxBuff[1] == OP_ACK ) {

				WaitAppReply();

				return;
			}

			m_uRxAppCount = 1;

			m_bRxApp[0]   = NAK;

			m_pRxAppFlag->Set();

			switch( m_bRxBuff[1] ) {

				case OP_INVITE:

					AcceptInvite();
					return;

				case OP_BUSY:

					UseToken();
					return;
			}
		}

		Invalid();
	}

	if( uEvent == EVENT_TIMEOUT ) {

		m_uRxAppCount = 1;

		m_bRxApp[0]   = NAK;

		m_pRxAppFlag->Set();

		UseToken();
	}
}

void CDH485Handler::HandleWaitAccept(UINT uEvent)
{
	if( uEvent == EVENT_FRAME ) {

		if( MatchDrop() ) {

			switch( m_bRxBuff[1] ) {

				case OP_ACK:
					NewSuccessor();
					return;

				case OP_INVITE:
					AcceptInvite();
					return;
			}
		}

		Invalid();
	}

	if( uEvent == EVENT_TIMEOUT ) {

		if( m_bNextDrop == m_bThisDrop ) {

			if( DoInvite() == FALSE ) {

				ResetMac();
			}
		}
		else {
			UseToken();
		}
	}
}

void CDH485Handler::HandleCheck(UINT uEvent)
{
	if( uEvent == EVENT_FRAME ) {

		GoIdle();

		Signal(uEvent);

		return;
	}

	if( uEvent == EVENT_TIMEOUT ) {

		if( m_uPassRetry ) {

			SendPass();

			m_uPassRetry--;

			return;
		}

		m_dwActive &= ~(1L << m_bNextDrop);

		m_dwWork   &= ~(1L << m_bNextDrop);

		if( m_dwActive ) {

			BYTE bCount = m_bLastDrop + 1;

			for( ;;) {

				m_bNextDrop = (m_bNextDrop + 1) % bCount;

				if( m_dwActive & (1L << m_bNextDrop) ) {

					break;
				}
			}

			if( m_bNextDrop == m_bThisDrop ) {

				ResetMac();
			}
			else {
				DoPass(3);
			}
		}
		else {
			ResetMac();
		}

		return;
	}
}

// Token Usage

void CDH485Handler::TakeToken(void)
{
	m_uToken      = min(100, m_uToken + 1);

	m_dwActive    = m_dwWork;

	m_dwWork      = 0;

	m_uTokenState = 0;

	UseToken();
}

void CDH485Handler::UseToken(void)
{
	BOOL fUsed = FALSE;

	do {
		switch( m_uTokenState++ ) {

			case 0:
				fUsed = DoSend();
				break;

			case 1:
				fUsed = DoInvite();
				break;

			default:
				fUsed = DoPass(3);
				break;
		}

	} while( !fUsed );
}

BOOL CDH485Handler::DoSend(void)
{
	if( !m_fWaitReply && m_uTxAppCount ) {

		SendData();

		return TRUE;
	}

	return FALSE;
}

BOOL CDH485Handler::DoInvite(void)
{
	if( TRUE || !m_fSendInvite ) {

		if( !m_fSeenInvite ) {

			m_fSendInvite = TRUE;
		}
		else {
			return FALSE;
		}
	}

	BYTE bWrap = (m_bLastDrop + 1);

	BYTE bDrop = (m_bThisDrop + 1 + m_bInvite++) % bWrap;

	if( bDrop == m_bNextDrop ) {

		m_fSendInvite = FALSE;

		m_bInvite     = 0;

		return FALSE;
	}

	SendInvite(bDrop);

	return TRUE;
}

BOOL CDH485Handler::DoPass(UINT uCount)
{
	m_fSeenInvite = FALSE;

	m_uPassRetry  = uCount;

	SendPass();

	return TRUE;
}

// Event Support

void CDH485Handler::Invalid(void)
{
	ResetMac();
}

void CDH485Handler::ResetMac(void)
{
	m_uMacState  = MS_VIRGIN;

	m_uTimer     = ToTicks(200 + 100 * m_bThisDrop);

	m_fTxEnable  = FALSE;

	m_uTxHack    = 0;

	m_fWaitReply = FALSE;

	m_dwActive   = 0;

	m_dwWork     = 0;

	m_pRxAppFlag->Clear();
}

void CDH485Handler::GoIdle(void)
{
	m_uMacState = MS_IDLE;

	SetTimeout(ToTicks(200));
}

void CDH485Handler::SendReset(void)
{
	StartFrame(m_bThisDrop, OP_RESET);

	SendFrame();

	m_uMacState = MS_RESET;

	m_uTimer    = ToTicks(20);
}

void CDH485Handler::SendAck(void)
{
	BYTE bDest = (m_bRxBuff[2] & 0x7F);

	StartFrame(bDest, OP_ACK);

	SendFrame();
}

void CDH485Handler::AcceptInvite(void)
{
	m_bNextDrop = (m_bRxBuff[3] & 0x7F);

	SendAck();

	m_fSendInvite = FALSE;

	m_fSeenInvite = FALSE;

	GoIdle();
}

void CDH485Handler::SendInvite(BYTE bDrop)
{
	StartFrame(bDrop, OP_INVITE);

	AddByte(m_bNextDrop | 0x80);

	SendFrame();

	m_uMacState = MS_WAIT_ACCEPT;

	SetTimeout(ToTicks(15));
}

void CDH485Handler::SendData(void)
{
	BYTE bStart = m_fToggle ? OP_DATA1 : OP_DATA2;

	StartFrame(m_bTxAppDrop, bStart);

	for( UINT n = 0; n < m_uTxAppCount; n++ ) {

		BYTE bData = m_bTxApp[n];

		AddByte(bData);
	}

	SendFrame();

	m_uMacState = MS_WAIT_ACK;

	SetTimeout(ToTicks(50));

	m_fToggle   = !m_fToggle;
}

void CDH485Handler::SendPass(void)
{
	StartFrame(m_bNextDrop, OP_PASS);

	SendFrame();

	m_uMacState = MS_CHECK;

	SetTimeout(ToTicks(50));
}

void CDH485Handler::NewSuccessor(void)
{
	m_bNextDrop = (m_bRxBuff[2] & 0x7F);

	m_bInvite   = 0;

	DoPass(1);
}

void CDH485Handler::SeenInvite(void)
{
	m_fSendInvite = FALSE;

	m_fSeenInvite = TRUE;
}

void CDH485Handler::WaitAppReply(void)
{
	m_uTxAppCount = 0;

	m_fWaitReply  = TRUE;

	UseToken();
}

void CDH485Handler::TakeData(void)
{
	if( m_fWaitReply ) {

		memcpy(m_bRxApp, m_bRxBuff + 3, m_uRxPtr - 3);

		m_fWaitReply  = FALSE;

		m_uRxAppCount = m_uRxPtr - 3;

		m_pRxAppFlag->Set();
	}

	SendAck();
}

// General Support

BOOL CDH485Handler::MatchDrop(void)
{
	return (m_bRxBuff[0] & 0x7F) == m_bThisDrop;
}

void CDH485Handler::SetTimeout(UINT uTime)
{
	m_uTimer = uTime;
}

void CDH485Handler::ClearTimeout(void)
{
	m_uTimer = 0;
}

void CDH485Handler::CreateSyncObject(void)
{
	m_pRxAppFlag = Create_AutoEvent();
}

// Transmit Timing

PCTXT CDH485Handler::WhoGetName(void)
{
	IExtraHelper *pExtra = NULL;

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &pExtra);

	PCTXT pName = pExtra->WhoGetName(TRUE);

	pExtra->Release();

	return pName;
}

void CDH485Handler::CalibDelay(void)
{
	#if defined(AEON_PLAT_LINUX)

	m_uTxDelay = 0;

	#else

	Critical(TRUE);

	UINT uTime = GetTickCount();

	while( uTime == GetTickCount() ) m_uTxDelay += 0;

	uTime += 1;

	while( uTime == GetTickCount() ) m_uTxDelay += 1;

	Critical(FALSE);

	BOOL fG315   = !stricmp(WhoGetName(), "G315");

	UINT uFactor = fG315 ? 2 : ToTime(1);

	m_uTxDelay   = m_uTxDelay / uFactor;

	#endif
}

BOOL CDH485Handler::TxDelay(void)
{
	if( m_uTxDelay ) {

		UINT uCount = 2 * m_uTxDelay;

		while( uCount -= 1 ) GetTickCount();

		return TRUE;
	}

	return FALSE;
}

UINT CDH485Handler::ToTicks(UINT t)
{
	return t ? ((t < 5) ? 1 : (t / 5)) : 0;
}

// End of File
