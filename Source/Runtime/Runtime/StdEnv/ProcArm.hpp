
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ProcArm_HPP

#define INCLUDE_ProcArm_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Flags
//

#define AEON_PROC_ARM

#define AEON_LITTLE_ENDIAN

#define _E_LITTLE

//////////////////////////////////////////////////////////////////////////
//
// Naked Functions
//

#if !defined(__INTELLISENSE__) && defined(AEON_COMP_GCC)

#define NAKED __attribute__((naked))

#else

#define NAKED

#endif

//////////////////////////////////////////////////////////////////////////
//
// Nop and Breakpoint Macros
//

#if !defined(__INTELLISENSE__)

#define ProcNop()	ASM(	"nop\n\t"		\
				)			\

#define	ProcTrap(n)	ASM(	"svc %0"		\
				:			\
				: "i"(n)		\
				)			\

#define ProcBreak()	ASM(	"svc #0x0100\n\t"	\
				)			\

#else

#define ProcNop()   ((void) 0)

#define ProcTrap(n) ((void) n)

#define ProcBreak() ((void) 0)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Byte Ordering Helpers
//

#define	MotorToHost(x)	HostToMotor(x)

#define	IntelToHost(x)	HostToIntel(x)

//////////////////////////////////////////////////////////////////////////
//
// Motorola Byte Ordering
//

STRONG_INLINE WORD HostToMotor(WORD Data)
{
	#if !defined(__INTELLISENSE__) && defined(__GNUC__)

	ASM(	"mov r0, %0, lsl#8\n\t"
		"mov %0, %0, lsr#8\n\t"
		"and  r0, r0, #0xFF00\n\t"
		"and  %0, %0, #0x00FF\n\t"
		"orr %0, %0, r0"
		: "+r"(Data)
		: "r" (Data)
		: "r0"
		);

	#endif

	return Data;
	}

STRONG_INLINE DWORD HostToMotor(DWORD Data)
{
	#if !defined(__INTELLISENSE__) && defined(__GNUC__)

	ASM(	"eor r0, %0, %0, ror#16\n\t"
		"mov r0, r0, lsr#8\n\t"
		"bic r0, r0, #0xFF00\n\t"
		"eor %0, r0, %0, ror#8"
		: "+r"(Data)
		: "r" (Data)
		: "r0"
		);

	#endif

	return Data;
	}

STRONG_INLINE LONG HostToMotor(LONG Data)
{
	#if !defined(__INTELLISENSE__) && defined(__GNUC__)

	ASM(	"eor r0, %0, %0, ror#16\n\t"
		"mov r0, r0, lsr#8\n\t"
		"bic r0, r0, #0xFF00\n\t"
		"eor %0, r0, %0, ror#8"
		: "+r"(Data)
		: "r" (Data)
		: "r0"
		);

	#endif

	return Data;
	}

STRONG_INLINE INT64 HostToMotor(INT64 Data)
{
	DWORD lo = DWORD((Data >>  0) & 0xFFFFFFFF);

	DWORD hi = DWORD((Data >> 32) & 0xFFFFFFFF);

	return INT64(MotorToHost(hi)) | (INT64(MotorToHost(lo)) << 32);
	}

//////////////////////////////////////////////////////////////////////////
//
// Intel Byte Ordering
//

STRONG_INLINE WORD HostToIntel(WORD Data)
{
	return Data;
	}

STRONG_INLINE DWORD HostToIntel(DWORD Data)
{
	return Data;
	}

STRONG_INLINE LONG HostToIntel(LONG Data)
{
	return Data;
	}

STRONG_INLINE INT64 HostToIntel(INT64 Data)
{
	return Data;
	}

///////////////////////////////////////////////////////////////////////
//								
// Network Byte Ordering
//

STRONG_INLINE WORD HostToNet(WORD const Data)
{
	return HostToMotor(Data);
	}

STRONG_INLINE DWORD HostToNet(DWORD const Data)
{
	return HostToMotor(Data);
	}

STRONG_INLINE WORD NetToHost(WORD const Data)
{
	return MotorToHost(Data);
	}

STRONG_INLINE DWORD NetToHost(DWORD const Data)
{
	return MotorToHost(Data);
	}

// End of File

#endif
