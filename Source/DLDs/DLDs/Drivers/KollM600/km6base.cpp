#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "km6base.hpp"

// Command Definitions

//struct FAR KOLLMORGEN600CmdDef {
//	UINT	uID;			// Config Table/Offset
//	char	cCmd[MAXCOMMAND];	// Text string
//	UINT	uRW;			// Read/Write Status
//	BOOL	fTbl;			// Table Opcode
//	};

KOLLMORGEN600CmdDef CODE_SEG CKollmorgen600BaseDriver::CL[] = {

	{ ACC,		"ACC",		RW, FALSE }, // "Acceleration", @@WW
	{ ACCR,		"ACCR",		RW, FALSE }, // "Acceleration - Home/Jog", @@WW
	{ ACCUNIT,	"ACCUNIT",	RW, FALSE }, // "Type of acceleration command", @@LL
	{ ACTFAULT,	"ACTFAULT",	RW, FALSE }, // "Active Fault Mode", @@BB
	{ ACTIVE,	"ACTIVE",	RO, FALSE }, //R "Output stage active/inhibited", @@BB
	{ ACTRS232,	"ACTRS232",	RW, FALSE }, // "Activate RS232 Watchdog", @@BB
	{ ADDR,		"ADDR",		RW, FALSE }, // "Multidrop Address", @@BB
	{ ADDRFB,	"ADDRFB",	RW, FALSE }, // "Fieldbus Address - Drive 400 Slave", @@WW
	{ AENA,		"AENA",		RW, FALSE }, // "Auto-Enable", @@BB
	{ ALIAS,	"ALIAS",	RW, FALSE }, // "Drive Name String", @@LL
	{ AN10TX,	"AN10TX",	RW, FALSE }, // "Additional Torque Tuning Value", @@WW
	{ AN11NR,	"AN11NR",	RW, FALSE }, // "Max. # of change-able INxTRIG", @@BB
	{ AN11RANGE,	"AN11RANGE",	RW, FALSE }, // "Range of the analog change of INxTRIG", @@LL
	{ ANTRIG,	"AN_TRIG",	RW, TRUE  }, //Table "Analog Output Scaling", @@LL
	{ ANCNFG,	"ANCNFG",	RW, FALSE }, // "Analog Input Configuration", @@BB
	{ ANDB,		"ANDB",		RW, FALSE }, // "Dead Band of Analog Velocity Input", @@RR1
	{ ANIN,		"ANIN",		RO, TRUE  }, //Table "Analog Input Voltage", @@LL
	{ ANOFF,	"ANOFF",	RW, TRUE  }, //Table "Analog Input Offset", @@WW
	{ ANOUT,	"ANOUT",	RW, TRUE  }, //Table "Analog Output Configuration", @@BB
	{ ANZERO,	"ANZERO",	WC, TRUE  }, //Table W "Analog Input Zero", @@BIT
	{ AUTOHOME,	"AUTOHOME",	RW, FALSE }, // "Automatic Homing", @@BB
	{ AVZ,		"AVZ",		RW, TRUE  }, //Table "Analog Output Filter Time Constant", @@RR1
	{ CALCRK,	"CALCRK",	WO, FALSE }, //W "Calculate Resolver Parameters", @@BIT
	{ CBAUD,	"CBAUD",	RW, FALSE }, // "CAN Bus Baud Rate", @@WW
	{ CLRFAULT,	"CLRFAULT",	WC, FALSE }, //W "Clear Drive Fault", @@BIT
	{ CLRHR,	"CLRHR",	WC, FALSE }, //W "Bit 5 of STAT is cleared", @@BIT
	{ CLRORDER,	"CLRORDER",	WO, FALSE }, //W "Delete a Motion Task", @@WW
	{ CLRWARN,	"CLRWARN",	RW, FALSE }, // "Warning Mode", @@BB
	{ COLDSTART,	"COLDSTART",	WC, FALSE }, //W "Drive Reset", @@BIT
	{ DAOFFSET,	"DAOFFSET",	RW, TRUE  }, //Table "Analog Output Offset", @@WW
	{ DEC,		"DEC",		RW, FALSE }, // "Deceleration", @@WW
	{ DECDIS,	"DECDIS",	RW, FALSE }, // "Deceleration - Disable Output", @@WW
	{ DECR,		"DECR",		RW, FALSE }, // "Deceleration Ramp - Home/Jog", @@WW
	{ DECSTOP,	"DECSTOP",	RW, FALSE }, // "Fast Stop Ramp", @@WW
	{ DEVICE,	"DEVICE",	RO, TRUE  }, //Table "Device ID Numeric Data", @@LL
	{ DICONT,	"DICONT",	RO, FALSE }, //R "Continuous Current", @@RR1
	{ DIPEAK,	"DIPEAK",	RO, FALSE }, //R "Peak Rated Current", @@RR1
	{ DIR,		"DIR",		RW, FALSE }, // "Count Direction", @@BB
	{ DIS,		"DIS",		WC, FALSE }, //W "Disable", @@BIT
	{ DREF,		"DREF",		RW, FALSE }, // "Homing Direction", @@BB
	{ DRVSTAT,	"DRVSTAT",	RO, FALSE }, // "Internal Status", @@LL
	{ EN,		"EN",		WC, FALSE }, // "Enable", @@BIT
	{ ENCIN,	"ENCIN",	RW, FALSE }, // "Encoder Pulse Input", @@LL
	{ ENCLINES,	"ENCLINES",	RW, FALSE }, // "SinCos Encoder Resolution", @@WW
	{ ENCMODE,	"ENCMODE",	RW, FALSE }, // "Encoder Emulation", @@BB
	{ ENCOUT,	"ENCOUT",	RW, FALSE }, // "Encoder Emulation Resolution", @@LL
	{ ENZERO,	"ENCZERO",	RW, FALSE }, // "Zero Pulse Offset", @@WW
	{ ERRCODE,	"ERRCODE",	RO, TRUE  }, //Table "Fault Message String (79 Chars.)", @@LL
	{ ERRCODEA,	"ERRCODE *",	RO, FALSE }, // "Output Error Register", @@LL
	{ EXTMUL,	"EXTMUL",	RW, FALSE }, // "External Encoder Multiplier", @@WW
	{ EXTWD,	"EXTWD",	RW, FALSE }, // "External Fieldbus Watchdog", @@LL
	{ ERSP,		"ERSP",		RW, TRUE  }, //Table "Response Error String (79 Chars.)", @LL
	{ FBTYPE,	"FBTYPE",	RW, FALSE }, // "Encoder/Resolver Selection", @@BB
	{ FLASH,	"FLASH",	RW, FALSE }, // "Send Data to External Flash", @@BB
	{ FLTCNT,	"FLTCNT *",	RO, TRUE  }, //R Table33 "Fault Frequency", @@LL
	{ FLTHIST,	"FLTHIST *",	RO, TRUE  }, //R Table20 "Fault History", @@LL
	{ FLUXM,	"FLUXM",	RW, FALSE }, // "Rated Flux", @@RR
	{ GEARI,	"GEARI",	RW, FALSE }, // "Gearing Input Factor", @@WW
	{ GEARMODE,	"GEARMODE",	RW, FALSE }, // "Secondary Position Source", @@BB
	{ GEARO,	"GEARO",	RW, FALSE }, // "Gearing Output Factor", @@WW
	{ GP,		"GP",		RW, FALSE }, // "Proportional Gain - Position Loop", @@RR1
	{ GPFBT,	"GPFBT",	RW, FALSE }, // "Feed Forward - Actual Current", @@RR1
	{ GPFFV,	"GPFFV",	RW, FALSE }, // "Feed Forward - Velocity", @@RR1
	{ GPTN,		"GPTN",		RW, FALSE }, // "Integral - Position Loop", @@RR1
	{ GPV,		"GPV",		RW, FALSE }, // "Proportional Gain - Velocity Controller", @@RR1
	{ GV,		"GV",		RW, FALSE }, // "Proportional Gain - Velocity Loop", @@RR1
	{ GVFBT,	"GVFBT",	RW, FALSE }, // "First Order TC - Velocity Loop", @@RR1
	{ GVFILT,	"GVFILT",	RW, FALSE }, // "% Output Filtered - Velocity Loop", @@BB
	{ GVFR,		"GVFR",		RW, FALSE }, // "Feed Forward - Actual Velocity", @@RR1
	{ GVT2,		"GVT2",		RW, FALSE }, // "Second TC - Velocity Loop", @@RR1
	{ GVTN,		"GVTN",		RW, FALSE }, // "Integral - Velocity Loop", @@RR1
	{ HVER,		"HVER",		RO, TRUE  }, //R Table13 "Hardware Version String", @@LL
	{ ICURR,	"I",		RO, FALSE }, //R "Current", @@RR
	{ I2T,		"I2T",		RO, FALSE }, //R "RMS Current Loading %", @@LL
	{ I2TLIM,	"I2TLIM",	RW, FALSE }, // "I2T Warning %", @@BB
	{ ICMD,		"ICMD",		RO, FALSE }, //R "Current Command", @@RR
	{ ICONT,	"ICONT",	RW, FALSE }, // "Rated Current", @@RR
	{ ID,		"ID",		RO, FALSE }, //R "D-Component of Current Monitor", @@RR
	{ IMAX,		"IMAX",		RO, FALSE }, //R "Current Limit for Drive/Motor", @@RR
	{ INAD,		"INAD",		RO, TRUE  }, //R "A/D Channels Input counts", @@ LL
	{ INS,		"IN",		RO, TRUE  }, //R Table4 "Digital Input Status", @@BB
	{ INMODE,	"IN_MODE",	RW, TRUE  }, //Table4 "Digital Input Function", @@BB
	{ INTRIG,	"IN_TRIG",	RW, TRUE  }, //Table4 "INMODE Trigger Data", @@LL
	{ INPOS,	"INPOS",	RO, FALSE }, //R "In-Position Status", @@BB
	{ INPT,		"INPT",		RW, FALSE }, // "In-Position Delay", @@WW
	{ IPEAK,	"IPEAK",	RW, FALSE }, // "Peak Current - Application", @@RR1
	{ IQ,		"IQ",		RO, FALSE }, //R "Q-Component of Current Monitor", @@RR
	{ ISCALE,	"ISCALE",	RW, TRUE  }, //Table2 "Analog Current Scaling", @@RR1
	{ KILL,		"K",		WC, FALSE }, //W "Kill", @@BIT
	{ KC,		"KC",		RW, FALSE }, // "I-Controller Prediction Current", @@RR1
	{ KEYLOCK,	"KEYLOCK",	RW, FALSE }, // "Lock the Push Buttons", @@BB
	{ KTN,		"KTN",		RW, FALSE }, // "Integral - Current Controller", @@RR1
	{ LIND,		"L",		RW, FALSE }, // "Stator Inductance", @@RR1
	{ LATCH,	"LATCH",	RO, TRUE  }, //R Table2 "Latched 32/16-Bit Position (DRVSTAT)", @@LL
	{ LATCHX,	"LATCHX",	RO, TRUE  }, //R Table2 "Latched 32/16-Bit Position (TRJSTAT)", @@LL
	{ LTCH16,	"LATCH1P16",	RW, TRUE  }, //  Table2 "16 bit Position @ INx rising",@@LL)); // NOV 08
	{ LTCH32,	"LATCH1P32",	RW, TRUE  }, //  Table2 "32 bit Position @ INx rising",@@LL)); // NOV 08
	{ LED,		"LED",		RW, TRUE  }, //Table 3 "LED Display", @@BB
	{ LEDSTAT,	"LEDSTAT",	RW, FALSE }, // "Display Page", @@WW
	{ LOAD,		"LOAD",		WC, FALSE }, //W "Load Parameters from EEPROM", @@BIT
	{ MAXTEMPE,	"MAXTEMPE",	RW, FALSE }, // "Switch off - Ambient �C", @@WW
	{ MAXTEMPH,	"MAXTEMPH",	RW, FALSE }, // "Switch off - Heat Sink �C", @@WW
	{ MAXTEMPM,	"MAXTEMPM",	RW, FALSE }, // "Switch off - Motor (Ohms)", @@RR1
	{ MBRAKE,	"MBRAKE",	RW, FALSE }, // "Motor Holding Brake Select", @@BB
	{ MDBCNT,	"MDBCNT",	RO, FALSE }, //R "Number of Motor Data Sets", @@BB
	{ MDBGET,	"MDBGET",	RO, FALSE }, //R "Get Actual Motor Data Set String", @@LL
	{ MDBSET,	"MDBSET",	RW, FALSE }, // "Set Actual Motor Data Set", @@WW
	{ MH,		"MH",		WC, FALSE }, //W "Start Homing", @@BIT
	{ MICONT,	"MICONT",	RW, FALSE }, // "Motor Continuous Current Rating", @@RR1
	{ MIPEAK,	"MIPEAK",	RW, FALSE }, // "Motor Peak Current Rating", @@RR1
	{ MJOG,		"MJOG",		WC, FALSE }, //W "Start Jog Mode", @@BIT
	{ MKT,		"MKT",		RW, FALSE }, // "Motor KT", @@RR1
	{ MLGC,		"MLGC",		RW, FALSE }, // "Adaptive Gain Q-rated - Current Loop", @@RR1
	{ MLGD,		"MLGD",		RW, FALSE }, // "Adaptive Gain D - Current Loop", @@RR1
	{ MLGP,		"MLGP",		RW, FALSE }, // "Adaptive Gain Q-peak - Current Loop", @@RR1
	{ MLGQ,		"MLGQ",		RW, FALSE }, // "Adaptive Gain Absolute - Current Loop", @@RR1
	{ MNAME,	"MNAME",	RW, TRUE  }, //Table3 "Motor Name String", @@LL
	{ MNUMBER,	"MNUMBER",	RW, FALSE }, // "Motor Number", @@WW
	{ MONITOR,	"MONITOR",	RO, TRUE  }, //Table2 "Monitor Output Voltage", @@WW
	{ MOVE,		"MOVE",		RW, FALSE }, // "Start Motion Task", @@WW
	{ MPHASE,	"MPHASE",	RW, FALSE }, // "Motor Phase, Feedback Offset", @@WW
	{ MPOLES,	"MPOLES",	RW, FALSE }, // "Number of Motor Poles", @@BB
	{ MRD,		"MRD",		WC, FALSE }, //W "Homing to Resolver Zero, Mode 5", @@BIT
	{ MRESBW,	"MRESBW",	RW, FALSE }, // "Resolver Bandwidth", @@WW
	{ MRESPOLES,	"MRESPOLES",	RW, FALSE }, // "Number of Resolver Poles", @@BB
	{ MSPEED,	"MSPEED",	RW, FALSE }, // "Maximum Rated Motor Velocity", @@RR1
	{ MTANGLP,	"MTANGLP",	RW, FALSE }, // "Current Lead", @@WW
	{ MTMUX,	"MTMUX",	RW, FALSE }, // "Presetting for Motion Task"
	{ MTMUXR,	"MTMUX",	WO, TRUE  }, //Table 13 "MTMUX Write Response String"
	{ MUNIT,	"MUNIT",	RW, FALSE }, // "Units for Velocity Parameters", @@BB
	{ MVANGLB,	"MVANGLB",	RW, FALSE }, // "Velocity Lead (Start Phi)", @@LL
	{ MVANGLF,	"MVANGLF",	RW, FALSE }, // "Velocity Lead (Limit Phi)", @@WW
	{ MVANGLP,	"MVANGLP",	RW, FALSE }, // "Velocity Lead (Commutation Angle)", @@WW
	{ NONBTB,	"NONBTB",	RW, FALSE }, // "Mains-BTB Check On/Off", @@BB
	{ NREF,		"NREF",		RW, FALSE }, // "Homing Mode", @@BB
	{ O_ACC,	"O_ACC",	RW, TRUE  }, //Table2 "Acceleration Time - Motion Task 0", @@WW
	{ O_C,		"O_C",		RW, FALSE }, // "Control Variable - Motion Task 0", @@WW
	{ O_DEC,	"O_DEC",	RW, TRUE  }, //Table2 "Deceleration Time - Motion Task 0", @@WW
	{ O_FN,		"O_FN",		RW, FALSE }, // "Next Task Number - Motion Task 0", @@WW
	{ O_FT,		"O_FT",		RW, FALSE }, // "Delay before Next Motion Task", @@WW
	{ O_P,		"O_P",		RW, FALSE }, // "Target Position - Motion Task 0", @@LL
	{ O_V,		"O_V",		RW, FALSE }, // "Target Speed - Motion Task 0", @@LL
	{ OUTS,		"O",		RW, TRUE  }, //Table3 "Digital Output Status", @@BB
	{ OCOPYQ,	"OCOPY",	WO, FALSE }, // "Execute OCOPY. <data> = Quantity", @@BB
	{ OCOPYS,	"OCOPY",	WO, FALSE }, // "...OCOPY Source Task Number", @@BB
	{ OCOPYD,	"OCOPY",	WO, FALSE }, // "...OCOPY Destination Task Number", @@BB
	{ OCOPYR,	"OCOPY",	WO, TRUE  }, //Table13 "OCOPY Write Response String"
	{ OMODE,	"O_MODE",	RW, TRUE  }, //Table3 "Digital Output Function", @@BB
	{ PASSCM,	"PASS",		RW, FALSE }, // "Parameter Change Password" NOV 2008 S300
	{ OTRIG,	"O_TRIG",	RW, TRUE  }, //Table3 "OMODE Trigger Data", @@LL
	{ OPMODE,	"OPMODE",	RW, FALSE }, // "Operating Mode", @@BB
	{ OPTION,	"OPTION",	RO, FALSE }, //R "Option Slot ID", @@WW
	{ PBAL,		"PBAL",		RO, FALSE }, // "Regen Power - Actual", @@LL
	{ PBALMAX,	"PBALMAX",	RW, FALSE }, // "Regen Power - Maximum", @@LL
	{ PBALRES,	"PBALRES",	RW, FALSE }, // "Regen Resistor - Select", @@BB
	{ PBAUD,	"PBAUD",	RO, FALSE }, // "Profibus Baud Rate", @@RR1
	{ PE,		"PE",		RO, FALSE }, //R "Following Error - Actual", @@LL
	{ PEINPOS,	"PEINPOS",	RW, FALSE }, // "In-Position Window", @@LL
	{ PEMAX,	"PEMAX",	RW, FALSE }, // "Following Error - Maximum", @@LL
	{ PFB,		"PFB",		RO, FALSE }, // "Actual Position from Feedback", @@LL
	{ PFB0,		"PFB0",		RO, FALSE }, //R "Position from External Encoder", @@LL
	{ PGEARI,	"PGEARI",	RW, FALSE }, // "Position Resolution - Numerator", @@LL
	{ PGEARO,	"PGEARO",	RW, FALSE }, // "Position Resolution - Denominator", @@LL
	{ PMODE,	"PMODE",	RW, FALSE }, // "Line Phase Error Mode", @@BB
	{ POSCNFG,	"POSCNFG",	RW, FALSE }, // "Axes Type", @@BB
	{ PRD,		"PRD",		RO, FALSE }, //R "20-bit Position Feedback", @@LL
	{ PSTATE,	"PSTATE",	RO, FALSE }, //R "Profibus State String", @@LL
	{ PTMIN,	"PTMIN",	RW, FALSE }, // "Min. Acceleration for Motion Tasks", @@WW
	{ PUNIT,	"PUNIT",	RW, FALSE }, // "Position Resolution", @@LL
	{ PV,		"PV",		RO, FALSE }, //R "Actual Velocity - Position Loop", @@LL
	{ PVMAX,	"PVMAX",	RW, FALSE }, // "Max. Velocity - Position Loop", @@LL
	{ PVMAXN,	"PVMAXN",	RW, FALSE }, // "Max. Neg Velocity - Position Loop", @@LL
	{ PVMAXP,	"PVMAXP",	RW, FALSE }, // "Maximum Positive Velocity" NOV 2008 S300
	{ READY,	"READY",	RO, FALSE }, //R "Software Enable Status", @@BB
	{ REFIP,	"REFIP",	RW, FALSE }, // "Peak Rated Current for Homing 7", @@RR1
	{ REFPOS,	"REFPOS",	RO, FALSE }, // "Reference Switch Position", @@LL
	{ REMOTE,	"REMOTE",	RO, FALSE }, //R "Hardware Enable Status", @@BB
	{ RESPHASE,	"RESPHASE",	RW, FALSE }, // "Resolver Phase", @@WW
	{ RK,		"RK",		RW, FALSE }, // "Resolver Sine Gain Adjust", @@WW
	{ ROFFS,	"ROFFS",	RW, FALSE }, // "Reference Offset", @@LL
	{ ROFFSA,	"ROFFSABS",	RW, FALSE }, // "Offset to Encoder Position" NOV 2008 S300
	{ RS232T,	"RS232T",	RW, FALSE }, // "RS232 Watchdog Time", @@
	{ RSTVAR,	"RSTVAR",	WC, FALSE }, //W "Restore Variables to Default", @@BIT
	{ S_STOP,	"S",		WC, FALSE }, //W "Stop Motor and Disable Drive", @@BIT
	{ SAVE,		"SAVE",		WC, FALSE }, //W "Save Data in EEPROM", @@BIT
	{ SCANX,	"SCANX",	WC, FALSE }, //W "Restart Communications", @@BIT
	{ SERIALNO,	"SERIALNO",	RO, FALSE }, //R "Drive Serial Number", @@LL
	{ SETREF,	"SETREF",	WC, FALSE }, //W "Set Reference Point", @@BIT
	{ SLOTIO,	"SLOTIO",	RO, FALSE }, //R "I/O States - Expansion Card", @@LL
	{ SSIGRAY,	"SSIGRAY",	RW, FALSE }, // "SSI Code Select", @@BB
	{ SSIINV,	"SSIINV",	RW, FALSE }, // "SSI Clock", @@BB
	{ SSIMODE,	"SSIMODE",	RW, FALSE }, // "SSI Mode", @@BB
	{ SSIOUT,	"SSIOUT",	RW, FALSE }, // "SSI Baud Rate", @@BB
	{ STAT,		"STAT",		RO, FALSE }, //R "Drive Status Word", @@WW
	{ STATCODE,	"STATCODE *",	RO, FALSE }, //R "Status Variable Warnings", @@LL
	{ STATIO,	"STATIO",	RO, TRUE  }, //R Table8 "I/O Status", @@BB
	{ STATUS,	"STATUS",	RO, TRUE  }, //R Table5 "Detailed Amplifier Status", @@WW
	{ STOP,		"STOP",		WC, FALSE }, //W "Stop Motion Task", @@BIT
	{ SWCNFG,	"SWCNFG",	RW, FALSE }, // "Position Register 1...4 Configuration", @@WW
	{ SWCNFG2,	"SWCNFG2",	RW, FALSE }, // "Position Register 0 & 5 Configuration", @@WW
	{ SWE,		"SWE",		RW, TRUE  }, //Table 6 "Position Register Data", @@LL
	{ SWEN,		"SWE_N",	RW, TRUE  }, //Table 6 "Cam Position Register Data", @@LL
	{ TCURR,	"T",		RW, FALSE }, // "Digital Current Command", @@RR1
	{ TEMPE,	"TEMPE",	RO, FALSE }, //R "Ambient Temperature", @@LL
	{ TEMPH,	"TEMPH",	RO, FALSE }, //R "Heat Sink Temperature", @@LL
	{ TEMPM,	"TEMPM",	RO, FALSE }, //R "Motor Temperature", @@LL
	{ TRJSTAT,	"TRJSTAT",	RO, FALSE }, //R "Status 2", @@LL
	{ TRUN,		"TRUNS",	RO, FALSE }, //R "Run-Time Counter String", @@LL
	{ UID,		"UID",		RW, FALSE }, // "User ID", @@WW
	{ VEL,		"V",		RO, FALSE }, //R "Actual Velocity", @@RR
	{ VBUS,		"VBUS",		RO, FALSE }, //R "DC-bus Voltage", @@LL
	{ VBUSBAL,	"VBUSBAL",	RW, FALSE }, // "Maximum Line Voltage", @@WW
	{ VBUSMAX,	"VBUSMAX",	RO, FALSE }, // "Maximum DC-bus Voltage", @@LL
	{ VBUSMIN,	"VBUSMIN",	RW, FALSE }, // "Minimum DC-bus Voltage", @@LL
	{ VCMD,		"VCMD",		RO, FALSE }, //R "Internal Velocity RPM", @@RR
	{ VER,		"VER *",	RO, TRUE  }, //R "Firmware Version String", @@LL
	{ VJOG,		"VJOG",		RW, FALSE }, // "Jog Mode Speed", @@LL
	{ VLIM,		"VLIM",		RW, FALSE }, // "Maximum Velocity", @@RR1
	{ VMAX,		"VMAX",		RO, FALSE }, //R "Maximum System Speed", @@RR
	{ VMIX,		"VMIX",		RW, FALSE }, // "Velocity Mix", @@RR1
	{ VMUL,		"VMUL",		RW, FALSE }, // "Velocity Scale Factor", @@LL
	{ VOSPD,	"VOSPD",	RW, FALSE }, // "Overspeed", @@RR1
	{ VREF,		"VREF",		RW, FALSE }, // "Homing Speed", @@LL
	{ VSCALE,	"VSCALE",	RW, TRUE  }, //Table2 "Velocity Scaling - Analog Input", @@WW
	{ WMASK,	"WMASK",	RW, FALSE }, // "Warning as Fault Mask", @@LL
	{ USRDEF,	"USER",		WO, TRUE  }, //Table10 "User Defined Command String", @@LL
	{ PROMPT,	"PROMPT ",	WO, TRUE }, // Prompt
	};

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen Driver
//

// Constructor

CKollmorgen600BaseDriver::CKollmorgen600BaseDriver(void)
{
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_pTx       = m_bTx;

	m_pRx       = m_bRx;
	}

// Destructor

CKollmorgen600BaseDriver::~CKollmorgen600BaseDriver(void)
{
	}

// Configuration

void MCALL CKollmorgen600BaseDriver::Load(LPCBYTE pData)
{
	}

void MCALL CKollmorgen600BaseDriver::CheckConfig(CSerialConfig &Config)
{
	}

// Management

void MCALL CKollmorgen600BaseDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CKollmorgen600BaseDriver::Open(void)
{
	m_pCL = (KOLLMORGEN600CmdDef FAR * )CL;
	}

// Device

CCODE MCALL CKollmorgen600BaseDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			memset( m_pCtx->m_UserText, 0, sizeof(m_pCtx->m_UserText) );
			memset( m_pCtx->m_MTMUX, 0, sizeof(m_pCtx->m_MTMUX) );
			memset( m_pCtx->m_OCOPY, 0, sizeof(m_pCtx->m_OCOPY) );

			m_pCtx->m_OCOPYD = 0;
			m_pCtx->m_OCOPYS = 0;

			memset( m_pCtx->m_RspErr, 0, MAXUSRBYTES+1);

			m_pCtx->m_RspErr[0] = 0x4E6f0000;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKollmorgen600BaseDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKollmorgen600BaseDriver::Ping(void)
{
	for( UINT i = 0; i < 2; i++ ) {

		switch( DoPrompt(1 - i) ) {

			case LF:
			case CR:
			case 'b': // "booting..."
			case '>': // "--->"

				return CCODE_ERROR;

			case '-': // "--->"
				if( i ) return CCODE_ERROR;
				break;

			case 'P':
				if( i ) {

					CAddress Addr;
					DWORD Data[1];

					Addr.a.m_Table  = AN;
					Addr.a.m_Offset = ADDR;
					Addr.a.m_Type   = BB;
					Addr.a.m_Extra  = 0;

					return (Read(Addr, Data, 1) == 1) ? 1 : CCODE_ERROR;
					}
				break;

			default:
				break;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CKollmorgen600BaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetpItem(Addr) ) return CCODE_NO_DATA | CCODE_NO_RETRY;

	UINT uCt = IsStringItem() ? uCount : 1;

	if( NoReadTransmit(Addr.a.m_Type, pData, uCt) ) return uCt;

	return Read(Addr, pData, &uCt) ? uCt : CCODE_ERROR;
	}

CCODE MCALL CKollmorgen600BaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nWRITE *** T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( !SetpItem(Addr) ) return CCODE_NO_DATA | CCODE_NO_RETRY;

	if( NoWriteTransmit(pData) ) return uCount;

	if( m_pItem->uID == USRDEF && pData[0] == 0 ) return CCODE_NO_DATA | CCODE_NO_RETRY;

	return Write(Addr, pData, &uCount) ? uCount : CCODE_ERROR;
	}

// Execute Read/Write

BOOL CKollmorgen600BaseDriver::Read(AREF Addr, PDWORD pData, UINT * pCount)
{
	StartFrame( &m_uPtr );

	AddCommand(Addr.a.m_Offset);

	EndFrame();

	Send( m_pTx, m_uPtr );

	BYTE bTerminator = IsStringItem() ? 0 : CR;

	if( GetReply( m_pRx, MAXRXBUFFER, bTerminator ) ) {

		if( DoRespErr() || ((*m_bRx == 7) && (m_pItem->uID != ERRCODE)) ) {

			return CCODE_ERROR;
			}

		if( (*m_bTx == *m_bRx) && !IsStringItem() ) {

			if( !CheckPrompt() ) {

				return CCODE_ERROR;
				}
			}

		switch( m_pItem->uID ) {

			case STATUS:	GetStatusResponse(pData, Addr.a.m_Offset, *pCount);	break;

			case STATIO:	GetIOStatusResponse(pData, Addr.a.m_Offset, *pCount);	break;

			case FLTHIST:
			case FLTCNT:
			case INAD:	GetListResponse(pData, Addr.a.m_Offset, *pCount);	break;

			default:	GetResponse(pData, Addr.a.m_Type, pCount);		break;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CKollmorgen600BaseDriver::Write(AREF Addr, PDWORD pData, UINT * pCount)
{
	StartFrame( &m_uPtr );

	AddCommand(Addr.a.m_Offset);

	if( m_pItem->uRW != WC ) {

		if( Addr.a.m_Table != USRDEF ) {

			AddByte(' ');

			if( m_pItem->uID != OCOPYQ ) AddData(pData, Addr.a.m_Type, *pCount);

			else AddOCOPYData(LOBYTE(*pData));
			}

		else MakeUserText(pData, *pCount);
		}

	EndFrame();

	Send( m_pTx, m_uPtr );

	PDWORD pString;

	UINT uMaxStringCt;
	UINT uMaxStringSz;

	switch( m_pItem->uID ) {

		case OCOPYQ:
			pString      = m_pCtx->m_OCOPY;
			uMaxStringCt = MAXOCOPYDEF;
			uMaxStringSz = MAXOCOPY+1;
			break;

		case MTMUX:
			pString      = m_pCtx->m_MTMUX;
			uMaxStringCt = MAXMTMUXDEF;
			uMaxStringSz = MAXMTMUX+1;
			break;

		case USRDEF:
			pString      = m_pCtx->m_UserText;
			uMaxStringCt = MAXUSRDEF;
			uMaxStringSz = MAXUSRBYTES+1;
			break;

		default:
			return TRUE;
		}

	if( GetReply( m_pRx, MAXRXBUFFER, CR ) ) {

		memset(pString, 0, uMaxStringSz);

		GetStringResponse( pString, uMaxStringCt );
		}

	return TRUE;
	}

// Frame Building

void CKollmorgen600BaseDriver::AddCommand(UINT uOffset)
{
	char c[12] = {0};

	strcpy( c, m_pItem->cCmd );

	UINT uLen = strlen( (const char *)c );

	char b    = m_pHex[uOffset % 10];

	if( m_pItem->fTbl ) {

		switch( m_pItem->uID ) {

			case DEVICE:
			case ERRCODE:
			case FLTCNT:
			case FLTHIST:
			case HVER:
			case LTCH16:
			case LTCH32:
			case MNAME:
			case MTMUX:
			case OCOPYQ:
			case STATIO:
			case STATUS:
			case VER:
				break;

			case OUTS:
			case OMODE:
			case OTRIG:
				if( uOffset > 9 ) {

					c[1] = m_pHex[uOffset / 10];
					c[2] = m_pHex[uOffset % 10];
					}

				else	c[1] = b;
				break;

			case ANTRIG:
				c[2] = b;
				break;

			case INS:
			case INMODE:
			case INTRIG:
				if( uOffset > 9 ) {

					c[2] = m_pHex[uOffset / 10];
					c[3] = m_pHex[uOffset % 10];
					}

				else	c[2] = b;
				break;

			case SWEN:
				c[3] = b;
				break;

			case LATCH:
			case LATCHX:
				c[uLen]   = uOffset ? '3' : '1';
				c[uLen+1] = uOffset ? '2' : '6';
				break;

			case USRDEF:
				return;

			default:
				c[uLen]   = b;
				break;
			}
		}

	AddText(c);
	}

void CKollmorgen600BaseDriver::EndFrame(void)
{
	AddByte( LF );
	AddByte( CR );
	m_pData->ClearRx();
	}

void CKollmorgen600BaseDriver::AddByte(BYTE bData)
{
	if( m_uPtr < MAXTXBUFFER ) m_bTx[m_uPtr++] = bData;
	}

void CKollmorgen600BaseDriver::AddByteAsc(BYTE bData)
{
	if( bData > 99 ) AddByte(m_pHex[bData/100]);
	if( bData >  9 ) AddByte(m_pHex[(bData/10) % 10]);
	AddByte(m_pHex[bData % 10] );
	}

void CKollmorgen600BaseDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	AddByte(LOBYTE(wData));
	}

void CKollmorgen600BaseDriver::AddWordAsc(WORD wData)
{
	char c[6] = {0};

	SPrintf( c, "%d", wData );

	AddText(c);
	}

void CKollmorgen600BaseDriver::AddLong(DWORD dData)
{
	AddWord(HIWORD(dData));
	AddWord(LOWORD(dData));
	}

void CKollmorgen600BaseDriver::AddLongAsc(DWORD dData)
{
	char c[12] = {0};

	SPrintf( c, "%ld", dData );

	AddText(c);
	}

void CKollmorgen600BaseDriver::AddText(PCTXT pChar)
{
	for( UINT i = 0; i < strlen(pChar); i++ ) AddByte(pChar[i]);
	}

void CKollmorgen600BaseDriver::AddData(PDWORD pData, UINT uType, UINT uCount)
{
	DWORD dData = *pData;

	switch( uType ) {

		case BB:
			AddByteAsc(LOBYTE(LOWORD(dData)));
			return;

		case WW:
			if( IsSignedItem() && (dData & 0x80000000) ) {

				AddByte('-');

				dData = -dData;
				}

			AddWordAsc(LOWORD(dData));
			return;

		case LL:
			MakeLongData(pData, uCount);
			return;

		case RR:
			MakeRealData(dData, uCount);
			return;
		}
	}

void CKollmorgen600BaseDriver::AddOCOPYData(BYTE bData)
{
	char c[20] = {0};

	BYTE s = m_pCtx->m_OCOPYS;
	BYTE d = m_pCtx->m_OCOPYD;

	if( bData == 1 ) SPrintf( c, "%d %d", s, d );

	else SPrintf( c, "%d - %d %d", s, s + bData - 1, d );

	AddText(c);
	}

void CKollmorgen600BaseDriver::MakeLongData(PDWORD pData, UINT uCount)
{
	char c[51] = {0};

	DWORD dData = *pData;

	switch( m_pItem->uID ) {

		case ALIAS:
			MakeText(c, pData, min(2, uCount));
			break;

		case MNAME:
			MakeText(c, pData, min(3, uCount));
			break;

		default:
			if( IsSignedItem() && (dData & 0x80000000) ) {

				AddByte('-');

				dData = -dData;
				}

			AddLongAsc(dData);
			return;
		}

	AddText(PCTXT(c));
	}

void CKollmorgen600BaseDriver::MakeUserText(PDWORD pData, UINT uCount)
{
	char c[MAXUSRDEF * sizeof(DWORD)] = {0};

	MakeText(c, pData, uCount);

	AddText(PCTXT(c));
	}

void CKollmorgen600BaseDriver::MakeText(char *c, PDWORD pData, UINT uCount)
{
	BYTE b;

	UINT i;
	UINT k;
	UINT p = 0;

	DWORD d;

	for( i = 0; i < uCount; i++ ) {

		d = pData[i];

		k = 32;

		while( k ) {

			k -= 8;

			if( !(b = (d >> k)) ) return;

			c[p++] = b;
			}
		}
	}

void CKollmorgen600BaseDriver::MakeRealData(DWORD dData, UINT uCount)
{
	char cR[12] = {0};

	if( dData == 0x80000000 ) dData = 0;

	SPrintf( cR, "%f", PassFloat(dData) );

	AddText( PCTXT(cR) );
	}

void CKollmorgen600BaseDriver::GetResponse(PDWORD pData, UINT uType, UINT *pCount)
{
	if( IsStringItem() ) {

		GetStringResponse(pData, *pCount);

		return;
		}

	UINT uPos = 0;

	DWORD dData = *pData;

	switch( FindRxData(&uPos) ) {

		case 1:
			dData = GetValue( &uPos, uType == RR );
			break;

		case 2:
			*pData = GetHexValue(&uPos);
			return;

		case 0:
		default:

			return;
		}

	switch( uType ) {

		case BT:
			*pData = BOOL(dData) ? 1 : 0;
			break;

		case BB:
			*pData = DWORD(LOBYTE(LOWORD(dData)));
			break;

		case WW:
			*pData = IsSignedItem() && (dData & 0x80000000) ? dData : LOWORD(dData);
			break;

		case LL:
		case RR:
			*pData = dData;
			break;
		}
	}

void CKollmorgen600BaseDriver::GetStringResponse(PDWORD pData, UINT uCount)
{
	if( DoRespErr() ) {

		return;
		}

	UINT p = 0;

	BOOL fDone    = FALSE;

	BYTE bEndChar = m_pItem->uID == ERRCODE ? 0 : CR;

	for( UINT i = 0; i < min(uCount, sizeof(m_bRx)/sizeof(DWORD)); i++ ) {

		DWORD d = 0;

		UINT  u = 32;

		while( u ) {

			BYTE b = m_bRx[p++];

			fDone |= BOOL(!b) || (b == bEndChar);

			if( !fDone && b < ' ' ) b = '_'; // replace cntrl chars with '_'

			if( fDone ) b = 0;

			d <<= 8;

			d += b;

			u -= 8;
			}

		pData[i] = d;
		}
	}

void CKollmorgen600BaseDriver::GetStatusResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	UINT  uPos = 0;

	for( UINT i = 0; i < uOffset; i++ ) DWORD dData = GetHexValue(&uPos); // Get first position

	for( i = 0; i < uCount; i++ ) pData[i] = GetHexValue(&uPos);
	}

void CKollmorgen600BaseDriver::GetIOStatusResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = BOOL( m_bRx[(i + uOffset - 1) * 2] == '1' );
		}
	}

void CKollmorgen600BaseDriver::GetListResponse(PDWORD pData, UINT uOffset, UINT uCount)
{
	UINT uPos = 0;
	UINT i;

	if( uOffset ) {

		for( i = 0; i < uOffset; i++ ) {

			if( !FindNextValue( &uPos ) ) return;
			}
		}

	for( i = 0; i < uCount; i++ ) {

		pData[i] = GetValue( &uPos, FALSE );

		if( !FindNextValue( &uPos ) ) return;
		}
	}

// Find Command Item

BOOL CKollmorgen600BaseDriver::SetpItem(AREF Addr)
{
	KOLLMORGEN600CmdDef * p = m_pCL;

	UINT uID = Addr.a.m_Table;

	if( uID == AN ) uID = Addr.a.m_Offset;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == p->uID ) {

			m_pItem    = p;

			return TRUE;
			}

		p++;
		}

	return FALSE;
	}

// Helpers

DWORD	CKollmorgen600BaseDriver::GetValue( UINT * pPos, BOOL fIsReal )
{
	char * p    = (char *)&m_bRx[*pPos];

	DWORD dData = fIsReal ? ATOF(p) : ATOI(p);

	return dData;
	}

DWORD	CKollmorgen600BaseDriver::GetHexValue(UINT * pPos)
{
	DWORD dData = 0;

	UINT i      = 0;
	UINT uPos   = *pPos;

	BYTE b;

	while( i < 8 ) {

		if( GetHex( m_bRx[uPos++], &b ) ) {

			dData <<= 4;
			dData  += b;
			i++;
			}

		else break;
		}

	while( (m_bRx[uPos] != CR) && !GetHex(m_bRx[uPos], &b) ) {

		uPos++;
		}

	*pPos = uPos;

	return dData;
	}

BOOL	CKollmorgen600BaseDriver::NoReadTransmit(UINT uType, PDWORD pData, UINT uCount)
{
	UINT i;

	switch( m_pItem->uID ) {

		case USRDEF:

			for( i = 0; i < min(uCount, MAXUSRDEF); i++ ) {

				pData[i] = m_pCtx->m_UserText[i];
				}

			break;

		case OCOPYS:
			*pData = DWORD(m_pCtx->m_OCOPYS);
			break;

		case OCOPYD:
			*pData = DWORD(m_pCtx->m_OCOPYD);
			break;

		case OCOPYQ:
			*pData = 0;
			break;

		case OCOPYR:

			for( i = 0; i < min(uCount, MAXOCOPYDEF); i++ ) {

				pData[i] = m_pCtx->m_OCOPY[i];
				}

			break;

		case MTMUXR:

			for( i = 0; i < min(uCount, MAXMTMUXDEF); i++ ) {

				pData[i] = m_pCtx->m_MTMUX[i];
				}

			break;

		case ERSP:
			for( i = 0; i < min(uCount, ((MAXUSRBYTES+1)/4)); i++ ) {

				pData[i] = m_pCtx->m_RspErr[i];
				}

			return TRUE;

		default:
			*pData = uType == BT ? 0 : RTNZERO;
			break;
		}

	return m_pItem->uRW >= WO;
	}

BOOL CKollmorgen600BaseDriver::NoWriteTransmit(PDWORD pData)
{
	switch( m_pItem->uID ) {

		case OCOPYS:	m_pCtx->m_OCOPYS = LOBYTE(*pData);	return TRUE;

		case OCOPYD:	m_pCtx->m_OCOPYD = LOBYTE(*pData);	return TRUE;

		case OCOPYQ:	return !BOOL(LOBYTE(*pData));

		case OCOPYR:
			if( *pData ) memset(m_pCtx->m_OCOPY, 0, MAXOCOPY+1);
			return TRUE;

		case MTMUXR:
			if( *pData ) memset(m_pCtx->m_MTMUX, 0, MAXMTMUX+1);
			return TRUE;

		case ERSP:
			if( *pData ) memset(m_pCtx->m_RspErr, 0, MAXUSRBYTES+1);
			return TRUE;
		}

	return m_pItem->uRW == RO;
	}

UINT CKollmorgen600BaseDriver::FindRxData(UINT * pPos)
{
	UINT i = *pPos;

	if( m_bRx[i] == 'H' ) {

		*pPos = i + 1;

		return 2;
		}

	if( CheckIfNumeric(i) ) {

		*pPos = i;

		return 1;
		}

	while( m_bRx[i] && m_bRx[i] != CR && i < sizeof(m_bRx) ) {

		BYTE b = m_bRx[i];

		if( b == 'H' ) {

			*pPos = i + 1;

			return 2;
			}

		if( CheckIfNumeric(i) ) {

			*pPos = i;

			return 1;
			}

		i++;
		}

	return 0;
	}

BOOL CKollmorgen600BaseDriver::CheckIfNumeric(UINT uPos)
{
	return IsNumeric( m_bRx[uPos] );
	}

BOOL CKollmorgen600BaseDriver::IsNumeric(BYTE b)
{
	if( b >= '0' && b <= '9' ) return TRUE;

	return b == '+' || b == '-';
	}

BOOL CKollmorgen600BaseDriver::GetHex(BYTE b, PBYTE r)
{
	*r = 0xFF;

	if( (b >= '0') && (b <= '9') ) *r = b - '0';

	else if( (b >= 'A') && (b <= 'F') ) *r = b - '7';

	else if( (b >= 'a') && (b <= 'f') ) *r = b - 'W';

	return *r != 0xFF;
	}

BOOL CKollmorgen600BaseDriver::IsStringItem(void)
{
	switch( m_pItem->uID ) {

		case ALIAS:
		case DEVICE:
		case ERRCODE:
		case ERSP:
		case HVER:
		case MDBGET:
		case MDBSET:
		case MTMUXR:
		case MNAME:
		case OCOPYR:
		case PSTATE:
		case VER:
		case USRDEF:
			return TRUE;
		}

	return FALSE;
	}

BOOL CKollmorgen600BaseDriver::IsSignedItem(void)
{
	switch( m_pItem->uID ) {

		case AN11RANGE:
		case ANOFF:
		case EXTMUL:
		case GEARO:
		case MONITOR:
		case O_C:
		case PEMAX:
		case PFB:
		case PGEARI:
		case PGEARO:
		case PV:
		case RESPHASE:
		case ROFFS:
		case ROFFSA:
		case TCURR:
		case TEMPE:
		case TEMPH:
		case SWE:
		case SWEN:
			return TRUE;
		}

	return FALSE;
	}

BOOL CKollmorgen600BaseDriver::FindNextValue(UINT *pPos)
{
	UINT uPos = *pPos; // start position of previous value

	for( UINT i = 1; i < 10; i++ ) {

		UINT u = i + uPos;

		if( m_bRx[u] == CR ) return FALSE;

		if( !IsNumeric(m_bRx[u]) ) { // 1st separator

			if( IsNumeric( m_bRx[u + 1] ) ) {

				*pPos = u + 1;
				return TRUE;
				}
			}
		}

	return FALSE;
	}

BYTE CKollmorgen600BaseDriver::DoPrompt(UINT uPrompt)
{
	DWORD    Data[1];
	CAddress Addr;

	Addr.a.m_Table  = AN;
	Addr.a.m_Offset = PROMPT;
	Addr.a.m_Type   = BB;

	SetpItem(Addr);

	StartFrame( &m_uPtr );
	AddCommand(uPrompt);
	EndFrame();

	m_bRx[0] = 0;

	Send( m_pTx, m_uPtr );

	GetReply( m_pRx, MAXRXBUFFER, CR );

	return m_pRx[0];
	}

BOOL CKollmorgen600BaseDriver::DoRespErr(void)
{
	if( m_pItem->uID == ERRCODE || m_bRx[0] != 7 ) {

		return FALSE;
		}

	PU4 pdD    = PU4(&m_bRx[1]);
	PU4 pdB    = m_pCtx->m_RspErr;

	BOOL fDone = FALSE;

	for( UINT i = 0; i < (MAXUSRBYTES+1)/4; i++, pdD++, pdB++ ) {

		*pdB = !fDone ? *pdD : 0;

		for( UINT j = 0; j < 4; j++ ) {

			if( m_bRx[(i*4)+j+1] < ' ' ) {

				fDone = TRUE;

				break;
				}
			}
		}

	return TRUE;
	}

BOOL CKollmorgen600BaseDriver::CheckPrompt(void)
{
	UINT uEcho = strlen((const char *)m_pItem->cCmd);

	if( !strncmp((const char *)m_bRx, (const char *)m_pItem->cCmd, uEcho) ) {

		if( m_pItem->fTbl ) {

			UINT i = 0;

			while( i < 3 ) {

				BYTE b = m_bRx[i + uEcho];

				if( b == '+' || b == '-' || b == ' ' ) {

					uEcho += i;

					break;
					}

				else i++;
				}
			}

		memcpy(m_bRx, &m_bRx[uEcho], sizeof(m_bRx) - uEcho);

		return TRUE;
		}

	else {
		return FALSE;
		}
	}

// Transport Specific

// Start Frame

void CKollmorgen600BaseDriver::StartFrame(UINT * pCount)
{
	}

// Transport Layer

void CKollmorgen600BaseDriver::Send(PCBYTE pTx, UINT uCount)
{
	}

BOOL CKollmorgen600BaseDriver::GetReply(PBYTE pRx, UINT uMax, BYTE bTerminator)
{
	return TRUE;
	}

// End of File
