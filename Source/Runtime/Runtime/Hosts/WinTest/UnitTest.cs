﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;

namespace WinTest
{
	[TestClass]
	public class UnitTest
	{
		[TestMethod]
		public void WinTest()
		{
			string WinHost = FindWinHost();

			if( WinHost.Length > 0 ) {

				int Code = 0;

				try {
					Process proc = new Process();

					proc.StartInfo.UseShellExecute = false;

					proc.StartInfo.CreateNoWindow  = true;

					proc.StartInfo.RedirectStandardOutput = true;

					proc.StartInfo.RedirectStandardError  = true;

					proc.StartInfo.FileName  = WinHost;

					proc.StartInfo.Arguments = "-test";

					proc.Start();

					proc.WaitForExit();

					Code = proc.ExitCode;
					}

				catch {
					Assert.Fail("Failed to execute WinHost");
					}

				if( Code == 100 ) {

					return;
					}

				Assert.Fail("WinHost returned " + Code.ToString());
				}
			else {
				Assert.Fail("Failed to locate WinHost");
				}
			}

		private string GetConfig()
		{
			#if DEBUG
			
			return "Debug";

			#else

			return "Release";
			
			#endif
			}

		private string FindWinHost()
		{
			string ThisPath = AppDomain.CurrentDomain.BaseDirectory;

			string WinHost1 = NormalizeFile(ThisPath + "\\WinHost.exe");
			
			string WinHost2 = NormalizeFile(ThisPath + "\\..\\..\\..\\..\\Bin\\Runtime\\Win32\\" + GetConfig() + "\\WinHost.exe");

			string WinHost3 = NormalizeFile(ThisPath + "\\..\\..\\..\\Binaries\\Win32\\" + GetConfig() + "\\WinHost.exe");

			if( File.Exists(WinHost1) ) {

				return WinHost1;
				}

			if( File.Exists(WinHost2) ) {

				return WinHost2;
				}

			if( File.Exists(WinHost3) ) {

				return WinHost3;
				}

			return "";
			}
		
		private string NormalizeFile(string File)
		{
			if( File.IndexOf('/') >= 0 ) {

				File = File.Replace("/", "\\");
				}

			for(;;) {

				int nFind = File.IndexOf("\\..\\");

				if( nFind >= 0 ) {

					int nPrev = File.LastIndexOf("\\", nFind
- 1);

					if( nPrev >= 0 ) {

						File = File.Remove(nPrev, nFind
- nPrev + 3);

						continue;
						}
					}

				break;
				}

			for(;;) {

				int nFind = File.IndexOf("\\.\\");

				if( nFind >= 0 ) {

					File = File.Remove(nFind, 2);

					continue;
					}

				break;
				}

			return File;
			}
		}
	}
