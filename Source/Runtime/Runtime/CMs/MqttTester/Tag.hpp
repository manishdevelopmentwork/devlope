
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Tag_HPP

#define INCLUDE_Tag_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Tag
//

class CTag : public CCodedHost
{
	public:
		// Destructor
		virtual ~CTag(void);

		// Property Helpers
		static UINT  GetPropCount(void);
		static DWORD GetDefProp(WORD ID, UINT Type);
		static PCTXT GetPropName(WORD ID);
		static UINT  GetPropType(WORD ID, UINT Type);

		// Limit Access
		DWORD GetMinValue(UINT uPos, UINT Type);
		DWORD GetMaxValue(UINT uPos, UINT Type);

		// Service Access
		DWORD    GetColorPair(UINT uPos);
		CUnicode GetLabel(UINT uPos);
		CUnicode GetAsText(UINT uPos, DWORD Data, UINT Type, UINT Flags);
		CUnicode GetAsText(UINT uPos, UINT Flags);
		BOOL     SetAsText(UINT uPos, CString const &Text);
		CString  GetStates(void);

		// Attributes
		virtual UINT GetDataType(void) const;
		virtual BOOL IsArray(void) const;
		virtual UINT GetExtent(void) const;
		virtual BOOL CanWrite(void) const;

		// Evaluation
		virtual BOOL  IsAvail(CDataRef const &Ref);
		virtual BOOL  IsAvail(CDataRef const &Ref, UINT Flags);
		virtual BOOL  SetScan(CDataRef const &Ref, UINT Code);
		virtual DWORD GetData(CDataRef const &Ref, UINT Type, UINT Flags);
		virtual BOOL  IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		virtual BOOL  SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags);
		virtual DWORD GetProp(CDataRef const &Ref, WORD ID, UINT Type);

		// Deadband
		virtual void InitPrevious(DWORD &Prev);
		virtual BOOL HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev);
		virtual void KillPrevious(DWORD &Prev);

		// Data Members
		CUnicode m_Name;
		CUnicode m_Desc;

	protected:
		// Property Access
		DWORD FindAsText(CDataRef const &Ref, UINT Type, DWORD Data);
		DWORD FindAsText(CDataRef const &Ref);
		DWORD FindLabel(CDataRef const &Ref);
		DWORD FindFore(CDataRef const &Ref);
		DWORD FindBack(CDataRef const &Ref);
	};

// End of File

#endif
