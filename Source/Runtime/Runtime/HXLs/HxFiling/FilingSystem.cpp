
#include "Intern.hpp"

#include "FilingSystem.hpp"

#include "TaskLocalStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Filing System
//

// Constructor 

CFilingSystem::CFilingSystem(UINT iDisk, UINT iVolume)
{
	StdSetRef();

	m_pMutex   = Create_Mutex();

	m_pDiskMan = NULL;

	m_pVolMan  = NULL;

	AfxGetObject("volman",  0, IVolumeManager, m_pVolMan);

	AfxGetObject("diskman", 0, IDiskManager,   m_pDiskMan);
		     
	m_pCache   = m_pDiskMan->GetDiskCache(iDisk);
		     
	m_pBlock   = m_pDiskMan->GetDisk(iDisk);
		     
	m_iHost    = m_pDiskMan->RegisterHost(iDisk);
		     
	m_iDisk    = iDisk;

	m_uIdent   = NOTHING;

	m_iVolume  = iVolume;

	m_fActive  = false;
		     
	m_uFirst   = 0;

	m_nRef     = 1;

	m_uSector  = m_pBlock->GetSectorSize();
		     
	m_uSize    = NOTHING;

	m_Debug	   = debugWarn | debugErr;

	m_cDrive   = 0;

	InitHandles();
	
	Debug(debugInfo, "Created");
	}

// Destructor

CFilingSystem::~CFilingSystem(void)
{
	Debug(debugInfo, "Destroyed");

	KillHandles();

	m_pDiskMan->UnregisterHost(m_iDisk, m_iHost);

	m_pMutex->Release();
	}

// IUnknown

HRESULT CFilingSystem::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IFilingSystem);

	StdQueryInterface(IFilingSystem);

	return E_NOINTERFACE;
	}

ULONG CFilingSystem::AddRef(void)
{
	Debug(debugDebug, "AddRef  : Task=%2.2d, Ref=%d", GetThreadIndex(), m_nRef+1);

	StdAddRef();
	}

ULONG CFilingSystem::Release(void)
{
	Debug(debugDebug, "Release : Task=%2.2d, Ref=%d", GetThreadIndex(), m_nRef-1);

	StdRelease();
	}

// Attributes

UINT CFilingSystem::GetIdent(void)
{
	return m_uIdent;
	}

UINT CFilingSystem::GetIndex(void)
{
	return m_iVolume;
	}

UINT CFilingSystem::GetDisk(void)
{
	return m_iDisk;
	}

CHAR CFilingSystem::GetDrive(void)
{
	return m_cDrive;
	}

INT64 CFilingSystem::GetSize(void)
{
	return 0;
	}

INT64 CFilingSystem::GetFree(void)
{
	return 0;
	}

INT64 CFilingSystem::GetUsed(void)
{
	return GetSize() - GetFree();
	}

// Methods

BOOL CFilingSystem::Init(void)
{
	Debug(debugInfo, "Formating...");

	m_fActive = true;
	
	return true;
	}

BOOL CFilingSystem::Start(void)
{
	Debug(debugInfo, "Starting...");

	m_cDrive  = m_pVolMan->GetDriveLetter(m_iVolume);

	m_fActive = true;
	
	return true;
	}

BOOL CFilingSystem::Stop(void)
{
	m_fActive = false;

	m_iVolume = NOTHING;
	
	Debug(debugInfo, "Stopped");

	return true;
	}

BOOL CFilingSystem::IsMounted(void)
{
	return m_fActive && m_iVolume != NOTHING;
	}

BOOL CFilingSystem::WaitLock(UINT uWait)
{
	Debug(debugApi, "WaitLock");

	return DiskWaitWriteLock();
	}

BOOL CFilingSystem::FreeLock(void)
{
	Debug(debugApi, "FreeLock");

	return DiskFreeWriteLock();
	}

// Enumeration

BOOL CFilingSystem::GetRoot(FSIndex &Index)
{
	return false;
	}

BOOL CFilingSystem::GetFirst(FSIndex &Index)
{
	return FindTaskDir(Index);
	}

BOOL CFilingSystem::GetNext(FSIndex &Index)
{
	return false;
	}

BOOL CFilingSystem::GetName(FSIndex const &Index, CFilename &Name)
{
	return false;
	}

BOOL CFilingSystem::GetPath(FSIndex const &Index, CFilename &Name)
{
	return false;
	}

time_t CFilingSystem::GetUnixTime(FSIndex const &Index)
{
	return 0;
	}

UINT CFilingSystem::GetSize(FSIndex const &Index)
{
	return 0;
	}

BOOL CFilingSystem::IsRoot(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::IsValid(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::IsDirectory(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::IsFile(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::IsVolume(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::IsSpecial(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::GetArchive(FSIndex const &Index)
{
	return false;
	}

// Directory

BOOL CFilingSystem::LocateDir(CFilename const &Name, FSIndex &Index)
{
	Debug(debugFile, "LocateDir(Name='%s')", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return LocateDir(Name, Dir, Index);
	}

BOOL CFilingSystem::LocateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	return false;
	}

BOOL CFilingSystem::CreateDir(CFilename const &Name)
{
	Debug(debugFile, "CreateDir(Name='%s')", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return CreateDir(Name, Dir);
	}

BOOL CFilingSystem::CreateDir(CFilename const &Name, FSIndex const &Dir)
{
	Debug(debugFile, "CreateDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	FSIndex Index;

	return CreateDir(Name, Dir, Index);
	}

BOOL CFilingSystem::CreateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	return false;
	}

BOOL CFilingSystem::ChangeDir(CFilename const &Name)
{
	Debug(debugFile, "ChangeDir(Name='%s')", PCTXT(Name));

	FSIndex Index;

	return MoveDir(Name, Index) && SaveTaskDir(Index);
	}

BOOL CFilingSystem::ChangeDir(CFilename const &Name, FSIndex const &Dir)
{
	Debug(debugFile, "ChangeDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	FSIndex Index;

	return MoveDir(Name, Dir, Index) && SaveTaskDir(Index);
	}

BOOL CFilingSystem::ChangeDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "ChangeDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	return MoveDir(Name, Dir, Index) && SaveTaskDir(Index);
	}

BOOL CFilingSystem::ChangeDir(FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "ChangeDir(Dir=0x%8.8X, Entry=0x%8.8X)", Dir.m_iCluster, Dir.m_iIndex);

	return MoveDir(Dir, Index) && SaveTaskDir(Index);
	}

BOOL CFilingSystem::MoveDir(CFilename const &Name, FSIndex &Index)
{
	Debug(debugFile, "MoveDir(Name='%s')", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return MoveDir(Name, Dir, Index);
	}

BOOL CFilingSystem::MoveDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	Debug(debugFile, "MoveDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	DiskWaitReadLock();

	if( LocateDir(Name, Dir, Index) ) {

		if( MoveDir(Index, Index) ) {

			DiskFreeReadLock();

			return true;
			}
		}

	DiskFreeReadLock();

	Debug(debugWarn, "MoveDir(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);

	return false;
	}

BOOL CFilingSystem::MoveDir(FSIndex const &Dir, FSIndex &Index)
{
	return false;
	}

UINT CFilingSystem::ScanDir(FSIndex *&pList, UINT uFlags)
{
	Debug(debugApi, "ScanDir(Flags=0x%8.8X)", uFlags);

	FSIndex Dir;

	FindTaskDir(Dir);

	return ScanDir(Dir, pList, uFlags);
	}

UINT CFilingSystem::ScanDir(CFilename const &Name, FSIndex *&pList, UINT uFlags)
{
	Debug(debugApi, "ScanDir(Name='%s', Flags=0x%8.8X)", PCTXT(Name), uFlags);

	FSIndex Index;

	DiskWaitReadLock();

	if( LocateDir(Name, Index) ) {
		
		if( MoveDir(Index, Index) ) {

			UINT uCount = ScanDir(Index, pList, uFlags);

			DiskFreeReadLock();

			return uCount;
			}
		}

	DiskFreeReadLock();

	return 0;
	}

UINT CFilingSystem::ScanDir(FSIndex const &Dir, FSIndex *&pList, UINT uFlags)
{
	return 0;
	}

BOOL CFilingSystem::RenameDir(CFilename const &From, CFilename const &To)
{
	Debug(debugFile, "RenameDir(Name='%s' to '%s')", PCTXT(From), PCTXT(To));

	DiskWaitWriteLock();

	FSIndex Dir, Index;

	FindTaskDir(Dir);

	if( LocateDir(From, Dir, Index) ) {

		if( RenameDir(To, Index) ) {

			DiskFreeWriteLock();

			return true;
			}
		}

	DiskFreeWriteLock();

	Debug(debugWarn, "RenameDir(Name='%s' to '%s') - FAILED", PCTXT(From), PCTXT(To));

	return false;
	}

BOOL CFilingSystem::RenameDir(CFilename const &Name, FSIndex &Index)
{
	return false;
	}

BOOL CFilingSystem::EmptyDir(CFilename const &Name)
{
	Debug(debugFile, "EmptyDir(Name='%s'", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return EmptyDir(Name, Dir);
	}

BOOL CFilingSystem::EmptyDir(CFilename const &Name, FSIndex const &Dir)
{
	Debug(debugFile, "EmptyDir(Name='%s', Dir=0x%8.8X)", PCTXT(Name), Dir.m_iCluster);

	DiskWaitWriteLock();

	FSIndex Index;

	if( LocateDir(Name, Dir, Index) ) {

		if( EmptyDir(Index) ) {

			DiskFreeWriteLock();

			return true;
			}
		}

	DiskFreeWriteLock();

	Debug(debugWarn, "EmptyDir(Name='%s', Dir=0x%8.8X) - FAILED", PCTXT(Name), Dir.m_iCluster);

	return false;
	}

BOOL CFilingSystem::EmptyDir(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::RemoveDir(CFilename const &Name)
{
	Debug(debugFile, "RemoveDir(Name='%s')", PCTXT(Name));

	DiskWaitWriteLock();

	FSIndex Dir, Index;

	FindTaskDir(Dir);

	if( LocateDir(Name, Dir, Index) ) {

		if( EmptyDir(Index) && RemoveDir(Index) ) {

			DiskFreeWriteLock();

			return true;
			}
		}

	DiskFreeWriteLock();

	Debug(debugWarn, "RemoveDir(Name='%s') - FAILED", PCTXT(Name));

	return false;
	}

BOOL CFilingSystem::RemoveDir(FSIndex const &Index)
{
	return false;
	}

UINT CFilingSystem::ParentDir(FSIndex const &Index)
{
	return NOTHING;
	}

BOOL CFilingSystem::CurrentDir(FSIndex &Index)
{
	return FindTaskDir(Index);
	};

// File

BOOL CFilingSystem::LocateFile(CFilename const &Name, FSIndex &Index)
{
	Debug(debugFile, "LocateFile(Name='%s')", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return LocateFile(Name, Dir, Index);
	}

BOOL CFilingSystem::LocateFile(CFilename const &Name, FSIndex const &Dir, FSIndex &Index)
{
	return false;
	}

HFILE CFilingSystem::CreateFile(CFilename const &Name)
{
	Debug(debugFile, "CreateFile(Name='%s')", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return CreateFile(Name, Dir);
	}

HFILE CFilingSystem::CreateFile(CFilename const &Name, FSIndex const &Dir)
{
	return NULL;
	}

HFILE CFilingSystem::OpenFile(CFilename const &Name, UINT Mode)
{
	Debug(debugFile, "OpenFile(Name='%s', Mode=0x%4.4X)", PCTXT(Name), Mode);

	FSIndex Dir;

	FindTaskDir(Dir);

	return OpenFile(Name, Dir, Mode);
	}

HFILE CFilingSystem::OpenFile(CFilename const &Name, FSIndex const &Dir, UINT Mode)
{
	return NULL;
	}

HFILE CFilingSystem::OpenFile(FSIndex const &Index, UINT Mode)
{
	return NULL;
	}

BOOL CFilingSystem::CloseFile(HFILE hFile)
{
	return false;
	}

BOOL CFilingSystem::DeleteFile(CFilename const &Name)
{
	Debug(debugFile, "DeleteFile(Name='%s')", PCTXT(Name));

	FSIndex Dir;

	FindTaskDir(Dir);

	return DeleteFile(Name, Dir);
	}

BOOL CFilingSystem::DeleteFile(CFilename const &Name, FSIndex const &Dir)
{
	return false;
	}

BOOL CFilingSystem::DeleteFile(FSIndex const &Index)
{
	return false;
	}

BOOL CFilingSystem::DeleteFile(HFILE hFile)
{
	return false;
	}

BOOL CFilingSystem::RenameFile(CFilename const &From, CFilename const &To)
{
	Debug(debugFile, "RenameFile(Name='%s' to '%s')", PCTXT(From), PCTXT(To));

	HFILE hFile = OpenFile(From, fileWrite);

	if( hFile ) {

		if( RenameFile(hFile, To) ) {

			CloseFile(hFile);

			return true;
			}

		CloseFile(hFile);
		}

	Debug(debugWarn, "RenameFile(Name='%s' to '%s') - FAILED", PCTXT(From), PCTXT(To));

	return false;
	}

BOOL CFilingSystem::RenameFile(HFILE hFile, CFilename const &Name)
{
	return false;
	}

BOOL CFilingSystem::FileSetLength(HFILE hFile, UINT uSize)
{
	return false;
	}

UINT CFilingSystem::FileGetLength(HFILE hFile)
{
	return 0;
	}

BOOL CFilingSystem::FileSetUnixTime(HFILE hFile, time_t time)
{
	return false;
	}

time_t CFilingSystem::FileGetUnixTime(HFILE hFile)
{
	return 0;
	}

BOOL CFilingSystem::FileGetIndex(HFILE hFile, FSIndex &Index)
{
	return false;
	}

BOOL CFilingSystem::FileAllocData(HFILE hFile, UINT uSize)
{
	return false;
	}

UINT CFilingSystem::FileWriteData(HFILE hFile, PCBYTE pData, UINT uOffset, UINT uCount)
{
	return NOTHING;
	}

UINT CFilingSystem::FileReadData(HFILE hFile, PBYTE pData, UINT uOffset, UINT uCount)
{
	return NOTHING;
	}

// Synchronisation

BOOL CFilingSystem::WaitReadLock(FSIndex const &Index)
{
	HFILE hFile = OpenHandle(Index);

	if( hFile != NULL ) {

		if( !WaitReadLock(hFile) ) {

			CloseHandle(hFile);

			return false;
			}
		
		return true;
		}

	return false;
	}

BOOL CFilingSystem::WaitWriteLock(FSIndex const &Index)
{
	HFILE hFile = OpenHandle(Index);

	if( hFile != NULL ) {

		if( !WaitWriteLock(hFile) ) {

			CloseHandle(hFile);

			return false;
			}
		
		return true;
		}

	return false;
	}

BOOL CFilingSystem::FreeReadLock(FSIndex const &Index)
{
	HFILE hFile = FindHandle(Index);

	if( hFile != NULL ) {

		if( FreeReadLock(hFile) ) {

			CloseHandle(hFile);
		
			return true;
			}
		
		CloseHandle(hFile);
		}

	return false;
	}

BOOL CFilingSystem::FreeWriteLock(FSIndex const &Index)
{
	HFILE hFile = FindHandle(Index);

	if( hFile != NULL ) {

		if( FreeWriteLock(hFile) ) {

			CloseHandle(hFile);
		
			return true;
			}
		
		CloseHandle(hFile);
		}

	return false;
	}

// Debugging

UINT CFilingSystem::DebugSetMask(UINT Mask)
{
	UINT uDebug = m_Debug;

	m_Debug = Mask;

	return uDebug;
	}

// Task Data

bool CFilingSystem::FindTaskDir(FSIndex &Index)
{
	return LoadTaskDir(Index) || GetRoot(Index);
	}

bool CFilingSystem::LoadTaskDir(FSIndex &Index)
{
	IThread *pThread = GetCurrentThread();

	if( pThread ) {

		CTaskDirectory *pThreadData = (CTaskDirectory *) pThread->GetData(TLS_FILE_DIR);

		if( pThreadData ) {

			Index.m_iCluster = pThreadData->m_Index.m_iCluster;

			Index.m_iIndex   = pThreadData->m_Index.m_iIndex;

			return true;
			}
		}

	return false;
	}

bool CFilingSystem::SaveTaskDir(FSIndex const &Index)
{
	IThread *pThread = GetCurrentThread();

	if( pThread ) {

		CTaskDirectory *pThreadData = (CTaskDirectory *) pThread->GetData(TLS_FILE_DIR);

		if( !pThreadData ) {

			pThreadData = New CTaskDirectory;

			if( !pThread->SetData(pThreadData) ) {

				delete pThreadData;

				return false;
				}
			}
		
		pThreadData->m_Index.m_iCluster = Index.m_iCluster;

		pThreadData->m_Index.m_iIndex   = Index.m_iIndex;

		return true;
		}

	return false;
	}

// Device Access

PBYTE CFilingSystem::LockSector(UINT uSector, BOOL fSkipRead)
{
	if( m_fActive && uSector < m_uSize ) {

		PBYTE pData = m_pCache->LockSector(m_uFirst + uSector, fSkipRead);
		
		if( !pData ) {

			Debug(debugWarn, "LockSector(Sector=0x%8.8X) - FAILED", uSector);

			return NULL;
			}

		return pData;
		}

	return NULL;
	}

BOOL CFilingSystem::CommitSector(UINT uSector)
{
	if( m_fActive && uSector < m_uSize ) {

		if( !m_pCache->CommitSector(m_uFirst + uSector) ) {

			Debug(debugWarn, "CommitSector(Sector=0x%8.8X) - FAILED", uSector);

			return false;
			}

		return true;
		}

	return false;
	}

BOOL CFilingSystem::UnlockSector(UINT uSector, BOOL fDirty)
{
	if( m_fActive && uSector < m_uSize ) {

		if( !m_pCache->UnlockSector(m_uFirst + uSector, fDirty) ) {

			Debug(debugWarn, "UnlockSector(Sector=0x%8.8X) - FAILED", uSector);

			return false;
			}

		return true;
		}

	return false;
	}

BOOL CFilingSystem::ReadSector(UINT uSector, PBYTE pData)
{
	return ReadSector(uSector, pData, 0, m_uSector);
	}

BOOL CFilingSystem::WriteSector(UINT uSector, PCBYTE pData)
{
	return WriteSector(uSector, pData, 0, m_uSector);
	}

BOOL CFilingSystem::ZeroSector(UINT uSector)
{
	return ZeroSector(uSector, 0, m_uSector);
	}

BOOL CFilingSystem::ReadSector(UINT uSector, PBYTE pData, UINT uCount)
{
	return ReadSector(uSector, pData, 0, uCount);
	}

BOOL CFilingSystem::WriteSector(UINT uSector, PCBYTE pData, UINT uCount)
{
	return WriteSector(uSector, pData, 0, uCount);
	}

BOOL CFilingSystem::ZeroSector(UINT uSector, UINT uCount)
{
	return ZeroSector(uSector, 0, uCount);
	}

BOOL CFilingSystem::ReadSector(CDataSector &Sector, PBYTE pData, UINT uCount)
{
	return ReadSector(Sector.m_uSector, pData, Sector.m_uOffset, uCount);
	}

BOOL CFilingSystem::WriteSector(CDataSector &Sector, PCBYTE pData, UINT uCount)
{
	return WriteSector(Sector.m_uSector, pData, Sector.m_uOffset, uCount);
	}

BOOL CFilingSystem::ZeroSector(CDataSector &Sector, UINT uCount)
{
	return ZeroSector(Sector.m_uSector, Sector.m_uOffset, uCount);
	}

BOOL CFilingSystem::ReadSector(UINT uSector, PBYTE pData, UINT uOffset, UINT uCount)
{
	Debug(debugDev, "ReadSector(Sector=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", uSector, uOffset, uCount);

	PBYTE pSectorData = LockSector(uSector, FALSE);

	if( pSectorData ) {

		memcpy(pData, pSectorData + uOffset, uCount);
		
		UnlockSector(uSector, false);
		
		return true;
		}

	Debug(debugWarn, "ReadSector(Sector=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X) - FAILED", uSector, uOffset, uCount);

	return false;
	}

BOOL CFilingSystem::WriteSector(UINT uSector, PCBYTE pData, UINT uOffset, UINT uCount)
{
	Debug(debugDev, "WriteSector(Sector=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", uSector, uOffset, uCount);

	BOOL  fSkipRead   = (uOffset == 0 && uCount == m_uSector);

	PBYTE pSectorData = LockSector(uSector, fSkipRead);

	if( pSectorData ) {

		if( !fSkipRead && !memcmp(pSectorData + uOffset, pData, uCount) ) {
		
			UnlockSector(uSector, false);
		
			return true;
			}

		memcpy(pSectorData + uOffset, pData, uCount);
		
		UnlockSector(uSector, true);
		
		return true;
		}

	Debug(debugWarn, "WriteSector(Sector=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X) - FAILED", uSector, uOffset, uCount);

	return false;
	}

BOOL CFilingSystem::ZeroSector(UINT uSector, UINT uOffset, UINT uCount)
{
	Debug(debugDev, "ZeroSector(Sector=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X)", uSector, uOffset, uCount);

	BOOL  fSkipRead   = (uOffset == 0 && uCount == m_uSector && FALSE);

	PBYTE pSectorData = LockSector(uSector, fSkipRead);

	if( pSectorData ) {

		for( UINT n = 0; n < uCount; n++ ) {

			if( fSkipRead || pSectorData[uOffset + n] ) {

				memset(pSectorData + uOffset + n, 0x00, uCount - n);
		
				UnlockSector(uSector, true);
		
				return true;
				}
			}
		
		UnlockSector(uSector, false);
		
		return true;
		}

	Debug(debugWarn, "ZeroSector(Sector=0x%8.8X, Offset=0x%8.8X, Count=0x%8.8X) - FAILED", uSector, uOffset, uCount);

	return false;
	}

// Disk Access

BOOL CFilingSystem::DiskWaitLock(UINT uLock)
{
	if( uLock & diskLockWrite ) {

		if( !DiskWaitWriteLock() ) {
			
			return false;
			}
		}

	if( uLock & diskLockRead ) {

		if( !DiskWaitReadLock() ) {
			
			return false;
			}
		}
	
	return true;
	}

BOOL CFilingSystem::DiskFreeLock(UINT uLock)
{
	if( uLock & diskLockWrite ) {

		DiskFreeWriteLock();
		}

	if( uLock & diskLockRead ) {

		DiskFreeReadLock();
		}

	return true;
	}

BOOL CFilingSystem::DiskWaitReadLock(void)
{
	Debug(debugSync, "DiskWaitReadLock(Host=%d, Disk=%d)", m_iHost, m_iDisk);

	if( !m_pDiskMan->WaitDiskLock(m_iHost, m_iDisk, diskLockRead, FOREVER) ) {

		Debug(debugWarn, "DiskWaitReadLock(Host=%d, Disk=%d) - FAILED", m_iHost, m_iDisk);

		return false;
		}

	return true;
	}

BOOL CFilingSystem::DiskWaitWriteLock(void)
{
	Debug(debugSync, "DiskWaitWriteLock(Host=%d, Disk=%d)", m_iHost, m_iDisk);

	if( !m_pDiskMan->WaitDiskLock(m_iHost, m_iDisk, diskLockWrite, FOREVER) ) {

		Debug(debugWarn, "DiskWaitWriteLock(Host=%d, Disk=%d) - FAILED", m_iHost, m_iDisk);

		return false;
		}

	return true;
	}

BOOL CFilingSystem::DiskFreeReadLock(void)
{
	Debug(debugSync, "DiskFreeReadLock(Host=%d, Disk=%d)", m_iHost, m_iDisk);

	if( !m_pDiskMan->FreeDiskLock(m_iHost, m_iDisk, diskLockRead) ) {

		Debug(debugWarn, "DiskFreeReadLock(Host=%d, Disk=%d) - FAILED", m_iHost, m_iDisk);

		return false;
		}

	return true;
	}

BOOL CFilingSystem::DiskFreeWriteLock(void)
{
	Debug(debugSync, "DiskFreeWriteLock(Host=%d, Disk=%d)", m_iHost, m_iDisk);

	if( !m_pDiskMan->FreeDiskLock(m_iHost, m_iDisk, diskLockWrite) ) {

		Debug(debugWarn, "DiskFreeWriteLock(Host=%d, Disk=%d) - FAILED", m_iHost, m_iDisk);

		return false;
		}

	return true;
	}

// Synchronisation

BOOL CFilingSystem::WaitReadLock(HFILE hFile)
{
	Debug(debugSync, "WaitReadLock(File=0x%8.8X)", hFile);

	if( hFile != NULL ) {

		CHandle *pFile = (CHandle *) hFile;

		for(;;) {

			pFile->m_pMutex->Wait(FOREVER);

			if( pFile->m_uOwner == GetThreadIndex() ) {

				pFile->m_nLockRead ++;
			
				pFile->m_pMutex->Free();

				return true;
				}

			pFile->m_pMutex->Free();
			
			UINT uObject = WaitMultiple(pFile->m_pSemaphore, pFile->m_pRead, FOREVER);

			pFile->m_pMutex->Wait(FOREVER);

			if( uObject == 1 ) {

				pFile->m_pRead->Set();
				}

			if( uObject == 2 ) {

				if( !pFile->m_nLockRead ) {

					pFile->m_pMutex->Free();

					continue;
					}
				}

			pFile->m_nLockRead ++;
			
			pFile->m_pMutex->Free();

			Debug(debugSync, "WaitReadLock(File=0x%8.8X) - Locked", hFile);

			return true;
			}
		}

	Debug(debugWarn, "WaitReadLock(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

BOOL CFilingSystem::WaitWriteLock(HFILE hFile)
{
	Debug(debugSync, "WaitWriteLock(File=0x%8.8X)", hFile);

	if( hFile != NULL ) {

		CHandle *pFile = (CHandle *) hFile;

		pFile->m_pMutex->Wait(FOREVER);

		if( pFile->m_uOwner == GetThreadIndex() ) {

			pFile->m_nLockWrite ++;

			pFile->m_pMutex->Free();

			return true;
			}

		pFile->m_pMutex->Free();

		pFile->m_pSemaphore->Wait(FOREVER);

		pFile->m_pMutex->Wait(FOREVER);

		pFile->m_uOwner = GetThreadIndex();

		pFile->m_nLockWrite ++;

		pFile->m_pMutex->Free();

		Debug(debugSync, "WaitWriteLock(File=0x%8.8X) - Locked", hFile);

		return true;
		}

	Debug(debugWarn, "WaitWriteLock(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

BOOL CFilingSystem::FreeReadLock(HFILE hFile)
{
	Debug(debugSync, "FreeReadLock(File=0x%8.8X)", hFile);

	if( hFile != NULL ) {

		CHandle *pFile = (CHandle *) hFile;

		pFile->m_pMutex->Wait(FOREVER);

		if( !--pFile->m_nLockRead ) {

			pFile->m_pRead->Clear();

			if( !pFile->m_nLockWrite ) {

				pFile->m_uOwner = NOTHING;

				pFile->m_pSemaphore->Signal(1);
				}
			}

		pFile->m_pMutex->Free();
		
		return true;
		}

	Debug(debugErr, "FreeReadLock(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

BOOL CFilingSystem::FreeWriteLock(HFILE hFile)
{
	Debug(debugSync, "FreeWriteLock(File=0x%8.8X)", hFile);

	if( hFile != NULL ) {

		CHandle *pFile = (CHandle *) hFile;

		pFile->m_pMutex->Wait(FOREVER);

		if( !--pFile->m_nLockWrite ) {

			pFile->m_uOwner = NOTHING;
			
			pFile->m_pSemaphore->Signal(1);
			}

		pFile->m_pMutex->Free();

		return true;
		}

	Debug(debugErr, "FreeWriteLock(File=0x%8.8X) - FAILED", hFile);

	return false;
	}

// File Handles

void CFilingSystem::InitHandles(void)
{
	memset(m_Handle, 0x00, sizeof(m_Handle));

	for( UINT i = 0; i < constMaxHandles; i ++ ) {

		CHandle &File = m_Handle[i];

		File.m_uOwner     = NOTHING;

		File.m_pRead      = Create_ManualEvent();

		File.m_pSemaphore = Create_Semaphore();

		File.m_pMutex     = Create_Mutex();

		File.m_pSemaphore->Signal(1);
		}
	}

void CFilingSystem::KillHandles(void)
{
	for( UINT i = 0; i < constMaxHandles; i ++ ) {

		CHandle &File = m_Handle[i];

		File.m_pRead->Release();
			
		File.m_pSemaphore->Release();

		File.m_pMutex->Release();
		}
	}

HFILE CFilingSystem::OpenHandle(HFILE hFile)
{
	if( hFile != NULL ) {

		CHandle *pFile = (CHandle *) hFile;

		AtomicIncrement(&pFile->m_nRef);
		
		return hFile;
		}

	Debug(debugErr, "OpenHandle(0x%8.8X) - FAILED : Handle Invalid", hFile);

	return NULL;
	}

HFILE CFilingSystem::OpenHandle(UINT iCluster)
{
	FSIndex Index;

	Index.m_iCluster = iCluster;

	Index.m_iIndex   = NOTHING;

	return OpenHandle(Index);
	}

HFILE CFilingSystem::OpenHandle(FSIndex const &Index)
{
	Debug(debugHandle, "OpenHandle(Cluster=0x%8.8X, Index=0x%8.8X)", Index.m_iCluster, Index.m_iIndex);
	
	m_pMutex->Wait(FOREVER);

	HFILE hFile = FindHandle(Index);

	if( hFile != NULL ) {
		
		hFile = OpenHandle(hFile);
		}
	else { 
		hFile = AllocHandle(Index);
		}

	m_pMutex->Free();

	Debug(debugHandle, "OpenHandle(Cluster=0x%8.8X, Index=0x%8.8X) = 0x%8.8X", Index.m_iCluster, Index.m_iIndex, hFile);
	
	return hFile;
	}

HFILE CFilingSystem::FindHandle(FSIndex const &Index)
{
	m_pMutex->Wait(FOREVER);

	for( UINT i = 0; i < constMaxHandles; i ++ ) {

		CHandle &File = m_Handle[i];

		if( !File.m_nRef ) {

			continue;
			}

		if( File.m_Index.m_iCluster != Index.m_iCluster ) {

			continue;
			}

		if( File.m_Index.m_iIndex != Index.m_iIndex ) {

			continue;
			}

		m_pMutex->Free();
	
		return HFILE(&File);
		}

	m_pMutex->Free();
	
	return NULL;
	}

HFILE CFilingSystem::AllocHandle(FSIndex const &Index)
{
	m_pMutex->Wait(FOREVER);

	for( UINT i = 0; i < constMaxHandles; i ++ ) {

		CHandle &File = m_Handle[i];

		if( !File.m_nRef ) {

			HFILE hFile = HFILE(&File);

			File.m_nRef		= 1;

			File.m_nLockRead	= 0;

			File.m_nLockWrite	= 0;
			
			File.m_Index.m_iCluster = Index.m_iCluster;
			
			File.m_Index.m_iIndex   = Index.m_iIndex;

			OnInitHandle(hFile);
		
			m_pMutex->Free();

			return hFile;
			}
		}

	Debug(debugWarn, "AllocHandle(...) - FAILED : No Free Slots Available", Index.m_iCluster, Index.m_iIndex);

	m_pMutex->Free();

	return NULL;
	}

void CFilingSystem::CloseHandle(HFILE hFile)
{
	Debug(debugHandle, "CloseHandle(File=0x%8.8X)", hFile);
	
	if( hFile != NULL ) {

		CHandle *pFile = (CHandle *) hFile;

		AfxAssert( pFile->m_nRef );

		OnCloseHandle(hFile);

		AtomicDecrement(&pFile->m_nRef);

		return;
		}

	Debug(debugErr, "CloseHandle(0x%8.8X) - FAILED : Handle Invalid", hFile);
	}

void CFilingSystem::OnInitHandle(HFILE hFile)
{
	}

void CFilingSystem::OnCloseHandle(HFILE hFile)
{
	}

// Sort Function

void CFilingSystem::SortList(FSIndex *pList, UINT uCount, UINT uFlags)
{
	if( pList && uCount ) {

		CSort *pSort = New CSort[uCount];

		for( UINT i = 0; i < uCount; i ++ ) {

			pSort[i].m_iIndex = pList[i].m_iIndex;

			pSort[i].m_time   = GetUnixTime(pList[i]); 
			}

		if( uFlags & scanOrder ) {

			qsort(pSort, uCount, sizeof(CSort), SortFunc1);
			}

		if( uFlags & scanReverse ) {

			qsort(pSort, uCount, sizeof(CSort), SortFunc2);
			}

		for( UINT i = 0; i < uCount; i ++ ) {

			pList[i].m_iIndex = pSort[i].m_iIndex;
			}

		delete [] pSort;
		}
	}

int CFilingSystem::SortFunc1(const void *p1, const void *p2)
{
	CSort *e1 = (CSort *) p1;

	CSort *e2 = (CSort *) p2;

	if( e1->m_time > e2->m_time ) return -1;

	if( e1->m_time < e2->m_time ) return +1;

	return 0;
	}

int CFilingSystem::SortFunc2(const void *p1, const void *p2)
{
	CSort *e1 = (CSort *) p1;

	CSort *e2 = (CSort *) p2;

	if( e1->m_time < e2->m_time ) return -1;

	if( e1->m_time > e2->m_time ) return +1;

	return 0;
	}

// Diagnostics

void CFilingSystem::Debug(UINT Mask, PCTXT pFormat, ...) const
{
	#if defined(_DEBUG)
	
	if( Mask & m_Debug ) {

		va_list pArgs;

		va_start(pArgs, pFormat);

		char sText[256];

		vsprintf(sText, pFormat, pArgs);

	//	DebugLock? !!!

	//	Critical(true);

		AfxTrace("File System(%c) : %-8.8s : ", m_cDrive ? m_cDrive : '0', GetCurrentThread()->GetName());

		AfxTrace(sText);

		AfxTrace("\n");

	//	Critical(false);
		}
	
	#endif
	}

// End of File
