
#include "intern.hpp"

#include "bacips.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IP Slave
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CBACNetIPSlave);

// Constructor

CBACNetIPSlave::CBACNetIPSlave(void)
{
	m_Ident  = DRIVER_ID;

	m_pSock  = NULL;

	m_pExtra = NULL;
}

// Destructor

CBACNetIPSlave::~CBACNetIPSlave(void)
{
}

// Configuration

void MCALL CBACNetIPSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_pCtx  = new CContext;

		m_pBase = m_pCtx;

		LoadConfig(pData);
	}
}

// Management

void MCALL CBACNetIPSlave::Attach(IPortObject *pPort)
{
	m_pSock = CreateSocket(IP_UDP);

	FindTrace();

	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
}

void MCALL CBACNetIPSlave::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
	}

	if( m_pExtra ) {

		m_pExtra->Release();

		m_pExtra = NULL;
	}
}

void MCALL CBACNetIPSlave::Open(void)
{
	if( m_pSock ) {

		m_pSock->SetOption(OPT_RECV_QUEUE, 12);

		m_pSock->SetOption(OPT_ADDRESS, 2);

		m_pSock->Listen(m_pCtx->m_uPort);
	}

	CBACNetSlave::Open();
}

// User Access

UINT MCALL CBACNetIPSlave::DrvCtrl(UINT uFunc, PCTXT Value)
{
	UINT uValue = ATOI(Value);

	switch( uFunc ) {

		case 2:	// Set Device ID

			if( uValue <= 4194303 ) {

				FreeObjectList();

				m_pCtx->m_Device = uValue;

				BuildObjectList();

				return OnSendIAm() ? 1 : 0;
			}

			break;

		case 3:	// Get Device ID

			return m_pCtx->m_Device;

	}

	return 0;
}

// Transport Hooks

BOOL CBACNetIPSlave::SendFrame(BOOL fThis, BOOL fReply)
{
	Dump("tx", m_pTxBuff);

	AddNetworkHeader(fThis, fReply);

	AddTransportHeader(fThis);

	for( UINT n = 0; n < 5; n++ ) {

		if( m_pSock->Send(m_pTxBuff) == S_OK ) {

			m_pTxBuff = NULL;

			return TRUE;
		}

		Sleep(10);
	}

	return FALSE;
}

BOOL CBACNetIPSlave::RecvFrame(void)
{
	SetTimer(5000);

	while( GetTimer() ) {

		m_pSock->Recv(m_pRxBuff);

		if( m_pRxBuff ) {

			if( StripTransportHeader() ) {

				if( StripNetworkHeader() ) {

					Dump("rx", m_pRxBuff);

					return TRUE;
				}
			}

			RecvDone();
		}

		Sleep(10);
	}

	return FALSE;
}

// Transport Header

void CBACNetIPSlave::AddTransportHeader(BOOL fThis)
{
	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 10;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	if( fThis ) {

		pData[0] = PBYTE(&m_RxMac.m_IP)[0];
		pData[1] = PBYTE(&m_RxMac.m_IP)[1];
		pData[2] = PBYTE(&m_RxMac.m_IP)[2];
		pData[3] = PBYTE(&m_RxMac.m_IP)[3];

		pData[4] = HIBYTE(m_RxMac.m_Port);
		pData[5] = LOBYTE(m_RxMac.m_Port);
	}
	else {
		BYTE tx0 = PBYTE(&m_TxMac.m_IP)[0];
		BYTE tx1 = PBYTE(&m_TxMac.m_IP)[1];
		BYTE tx2 = PBYTE(&m_TxMac.m_IP)[2];
		BYTE tx3 = PBYTE(&m_TxMac.m_IP)[3];

		DWORD IP = MAKELONG(MAKEWORD(tx0, tx1), MAKEWORD(tx2, tx3));

		if( m_pExtra->IsBroadcast(IntelToHost(IP)) ) {

			pData[0] = tx0;
			pData[1] = tx1;
			pData[2] = tx2;
			pData[3] = tx3;
		}
		else {
			pData[0] = 0xFF;
			pData[1] = 0xFF;
			pData[2] = 0xFF;
			pData[3] = 0xFF;
		}

		pData[4] = HIBYTE(m_pCtx->m_uPort);
		pData[5] = LOBYTE(m_pCtx->m_uPort);
	}

	pData[6] = 0x81;

	pData[7] = fThis ? 0x0A : 0x0B;

	pData[8] = HIBYTE(uData + 4);

	pData[9] = LOBYTE(uData + 4);
}

BOOL CBACNetIPSlave::StripTransportHeader(void)
{
	if( !m_pRxBuff->GetSize() ) {

		return FALSE;
	}

	PBYTE pData = m_pRxBuff->GetData();

	if( pData[12] == 0x81 ) {

		if( pData[13] == 0x04 || pData[13] == 0x0A || pData[13] == 0x0B ) {

			((PBYTE) &m_RxMac.m_IP)[0] = pData[0];
			((PBYTE) &m_RxMac.m_IP)[1] = pData[1];
			((PBYTE) &m_RxMac.m_IP)[2] = pData[2];
			((PBYTE) &m_RxMac.m_IP)[3] = pData[3];

			m_RxMac.m_Port = MAKEWORD(pData[0x05], pData[0x04]);

			((PBYTE) &m_TxMac.m_IP)[0] = pData[6];
			((PBYTE) &m_TxMac.m_IP)[1] = pData[7];
			((PBYTE) &m_TxMac.m_IP)[2] = pData[8];
			((PBYTE) &m_TxMac.m_IP)[3] = pData[9];

			m_TxMac.m_Port = MAKEWORD(pData[0x0B], pData[0x0A]);

			m_pRxBuff->StripHead(16);

			if( pData[13] == 0x04 ) {

				m_pRxBuff->StripHead(6);
			}

			return TRUE;
		}
	}

	return FALSE;

}

// End of File
