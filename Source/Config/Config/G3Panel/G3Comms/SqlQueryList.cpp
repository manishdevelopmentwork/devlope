
#include "Intern.hpp"

#include "SqlQueryList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SqlQuery.hpp"

/////////////////////////////////////////////////////////////////////////
//
// SQL Query List
//

// Dynamic Class

AfxImplementDynamicClass(CSqlQueryList, CNamedList);

// Constructor

CSqlQueryList::CSqlQueryList(void)
{
	m_Class = AfxRuntimeClass(CSqlQuery);
	}

// Item Access

CSqlQuery * CSqlQueryList::GetItem(INDEX Index) const
{
	return (CSqlQuery *) CNamedList::GetItem(Index);
	}

CSqlQuery * CSqlQueryList::GetItem(UINT uPos) const
{
	return (CSqlQuery *) CNamedList::GetItem(uPos);
	}

// Download Support

BOOL CSqlQueryList::MakeInitData(CInitData &Init)
{
	CNamedList::MakeInitData(Init);

	return TRUE;
	}

// End of File
