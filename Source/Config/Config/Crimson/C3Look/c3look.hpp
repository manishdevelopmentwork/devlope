
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3LOOK_HPP
	
#define	INCLUDE_C3LOOK_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3core.hpp>

#include <shlobj.h>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3look.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3LOOK

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3look.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Item View Flags
//

enum
{
	viewNone    = 0,
	viewEdge    = 1,
	viewCache   = 2,
	viewCaption = 4,
	viewMerge   = 8,
	};

//////////////////////////////////////////////////////////////////////////
//
// Item Update Types
//

enum
{
	updateNone     = 0,
	updateContents = 1,
	updateChildren = 2,
	updateRename   = 3,
	updateProps    = 4,
	updateLocked   = 5,
	updateValue    = 6,
	};

//////////////////////////////////////////////////////////////////////////
//
// Item Update Interface
//

interface IUpdate : public IUnknown
{
	virtual HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStdCmd;
class CCmdItemData;
class CCmdItem;
class CCmdGlobalItem;
class CCmdSubItem;

//////////////////////////////////////////////////////////////////////////
//
// Standard Command
//

class DLLAPI CStdCmd : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Encoding
		CString GetFixedName(CItem *pItem);
		CString GetFixedPath(CItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Root Item Change Command
//

class DLLAPI CCmdItemData : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CCmdItemData(void);

		// Destructor
		~CCmdItemData(void);

		// Attributes
		BOOL IsNull(void) const;

		// Data Members
		HGLOBAL m_hPrev;
		HGLOBAL m_hData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Simple Item Change Command
//

class DLLAPI CCmdItem : public CCmdItemData
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CCmdItem(void);
		CCmdItem(CString Menu, CItem *pItem, HGLOBAL hPrev);

		// Operations
		void Exec(CItem *pItem);
		void Undo(CItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Global Item Change Command
//

class DLLAPI CCmdGlobalItem : public CCmdItemData
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CCmdGlobalItem(CItem *pItem, HGLOBAL hPrev);

		// Operations
		void Exec(CItem *pItem);
		void Undo(CItem *pItem);

		// Data Members
		CString m_Edit;

	protected:
		// Implementation
		CItem * FindEditItem(CMetaItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Sub-Item Change Command
//

class DLLAPI CCmdSubItem : public CCmdItemData
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CCmdSubItem(CString Tag, CLASS Class);
		CCmdSubItem(CString Tag, HGLOBAL hPrev, HGLOBAL hData);

		// Attributes
		CItem * GetItem(CMetaItem *pRoot) const;

		// Operations
		BOOL Exec(CMetaItem *pRoot);
		BOOL Undo(CMetaItem *pRoot);

		// Data Members
		CString m_Tag;
		CLASS   m_Class;
		BOOL    m_fPend;
		BOOL    m_fPrev;

	protected:
		// Implementation
		CItem * & FindObject(CMetaItem *pRoot, CString Name) const;
		CItem * & FindObject(CMetaItem *pRoot) const;
		CItem *   FindParent(CMetaItem *pRoot) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSystemWnd;
class CSysProxy;
class CBlockerWnd;
class CResultsWnd;
class CWatchWnd;
class CNavPaneWnd;
class CResPaneWnd;
class CItemViewWnd;
class CCatViewWnd;

//////////////////////////////////////////////////////////////////////////
//
// System View Window
//

class DLLAPI CSystemWnd : public CTripleViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSystemWnd(void);

		// Destructor
		~CSystemWnd(void);

		// Attributes
		CViewWnd * GetCatView  (CLASS Class) const;
		CViewWnd * GetItemView (void) const;
		CCmd     * GetLastCmd  (void) const;
		CDialog  * GetSemiModal(void) const;
		BOOL       IsNavBusy   (void) const;

		// Navigation
		void SetNavCheckpoint(void);
		void SetViewedItem(CItem *pItem);
		void SetNavCategory(CItem *pItem);
		void SetResCategory(CItem *pItem);

		// Dialog Boxes
		UINT ExecSemiModal(CDialog &Dlg, CString Cat);
		UINT ExecSemiModal(CDialog &Dlg);

		// Pane Management
		BOOL HidePaneOnDrop(UINT uPane);
		void LockResPane(BOOL fLock);
		BOOL CanSyncPanes(void);
		void SyncPanes(void);
		void AssumeViewerActive(void);

		// Command Management
		void ExecCmd(CViewWnd *pView, CCmd *pCmd);
		void SaveCmd(CViewWnd *pView, CCmd *pCmd);
		BOOL KillLastCmd(void);
		BOOL KillUndoList(void);

		// Global Find
		BOOL SetFindList(CString Head, CStringArray const &List, BOOL fJump);
		void SetFindPos (UINT uPos);

		// Update Management
		UINT RegisterForUpdates(IUpdate *pUpdate);
		void UnregisterForUpdates(UINT uToken);
		void ItemUpdated(UINT uToken, CItem *pItem, UINT uType);

	protected:
		// Typedefs
		typedef CList <CCmd *   > CCmdList;
		typedef CList <CString  > CStringList;
		typedef CList <IUpdate *> CUpdateList;

		// Commands
		class CCmdNav;

		// Data Members
		CAccelerator    m_Accel1;
		CAccelerator    m_Accel2;
		CDatabase     * m_pDbase;
		CNavPaneWnd   * m_pNavPane;
		CResPaneWnd   * m_pResPane;
		CCatViewWnd   * m_pCatView;
		CItemViewWnd  * m_pViewer;
		CStringList	m_StackBack;
		CStringList	m_StackFore;
		CCmdList	m_StackUndo;
		CCmdList	m_StackRedo;
		CString         m_Init;
		BOOL	        m_fBusy;
		BOOL	        m_fUsed;
		BOOL		m_fLockRes;
		CDialog       * m_pSemi;
		CBlockerWnd   * m_pBlock;
		CUpdateList     m_Updates;
		CString		m_FindHead;
		CStringArray    m_FindList;
		UINT		m_uFindPos;
		CResultsWnd   * m_pResults;
		CWnd          * m_pWatchView;
		CWatchWnd     * m_pWatchWnd;

		// Overribables
		void    OnAttach(void);
		void    OnDetach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		void OnActivateApp(BOOL fActive, DWORD dwThread);

		// Edit Commands
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL OnEditJumpTo(UINT uPos);

		// View Commands
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Go Commands
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		void OnGoBack(void);
		void OnGoForward(void);
		BOOL StepBack(void);
		BOOL StepForward(void);

		// Edit Commands
		BOOL OnEditUndoControl(UINT uID, CCmdSource &Src);
		BOOL OnEditBackControl(UINT uID, CCmdSource &Src);
		BOOL OnEditRedoControl(UINT uID, CCmdSource &Src);
		BOOL OnEditUndoCommand(UINT uID);
		BOOL OnEditBackCommand(UINT uID);
		BOOL OnEditRedoCommand(UINT uID);
		void Enable(CCmdSource &Src, CCmdList &List, UINT uVerb, CString Key);
		BOOL Action(CCmdList &List1, CCmdList &List2, UINT uID);
		void Action(CCmd *pCmd, UINT uID);
		void Action(CViewWnd *pView, CCmd *pCmd, UINT uID);

		// Overridables
		void OnUpdateLayout(void);

		// Implementation
		UINT FindView(CWnd *pWnd);
		BOOL IsSameInit(CString const &Nav1, CString const &Nav2);
		BOOL EncodeNav(CString &Full, CString const &Init);
		BOOL DecodeNav(CString &Full);
		BOOL StepHistory(CStringList &List1, CStringList &List2);
		BOOL StoreHistory(CStringList &List, CString Nav);
		BOOL DoNavigate(CString Nav);
		void FreeList(CCmdList &List);
		void UpdateResPane(void);
		BOOL ForceResPane(void);
		void UpdateBlocker(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// System Window Proxy
//

class DLLAPI CSysProxy : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSysProxy(void);

		// Destructor
		~CSysProxy(void);

		// Binding
		void Bind(void);
		void Bind(CViewWnd *pView);
		void Bind(CSystemWnd *pSystem);

		// Attributes
		CViewWnd * GetCatView  (CLASS Class) const;
		CViewWnd * GetItemView (void) const;
		CCmd     * GetLastCmd  (void) const;
		CString    GetNavPos   (void) const;
		BOOL       InTwinMode  (void) const;
		BOOL       IsSemiModal (void) const;
		CDialog  * GetSemiModal(void) const;
		BOOL       IsNavBusy   (void) const;

		// Navigation
		BOOL Navigate(CString Nav);
		void SetNavCheckpoint(void);
		void SetViewedItem(CItem *pItem);
		void SetNavCategory(CItem *pItem);
		void SetResCategory(CItem *pItem);

		// Dialog Boxes		
		UINT ExecSemiModal(CDialog &Dlg, CString Cat);
		UINT ExecSemiModal(CDialog &Dlg);

		// Pane Management
		void SetTwinMode(BOOL fTwin);
		void LockResPane(BOOL fLock);
		BOOL HidePaneOnDrop(UINT uPane);
		BOOL CanSyncPanes(void);
		void SyncPanes(void);
		BOOL FlipToItemView(BOOL fNow);
		BOOL FlipToNavPane(BOOL fNow);
		BOOL FlipToResPane(BOOL fNow);
		void SendPaneCommand(UINT uPane, UINT uID);
		void SendPaneCommand(UINT uID);
		void AssumeViewerActive(void);

		// Command Management
		void ExecCmd(CCmd *pCmd);
		void SaveCmd(CCmd *pCmd);
		BOOL KillLastCmd(void);
		BOOL KillUndoList(void);

		// Global Find
		BOOL SetFindList(CString Head, CStringArray const &List, BOOL fJump);

		// Update Management
		void RegisterForUpdates(IUpdate *pUpdate);
		BOOL UnregisterForUpdates(void);
		void ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		CViewWnd   * m_pView;
		CSystemWnd * m_pSystem;
		UINT         m_uUpdate;
	};

//////////////////////////////////////////////////////////////////////////
//
// Blocker Window
//

class DLLAPI CBlockerWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CBlockerWnd(CWnd *pParent, CDialog *pDlg, CRect Rect);

		// Operations
		void Create(void);
		void SetDialog(CDialog *pDlg);

	protected:
		// Data Members
		CWnd    * m_pParent;
		CDialog * m_pDlg;
		CRect     m_Rect;
		CBitmap   m_Bitmap;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
	};

//////////////////////////////////////////////////////////////////////////
//
// Results Window
//

class DLLAPI CResultsWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CResultsWnd(CSystemWnd *pSystem);

		// Attributes
		BOOL IsShown(void) const;

		// Operations
		void Create(CRect Rect);
		void Show(BOOL fShow);
		void Load(CString const &Head, CStringArray const &List);
		BOOL Select(UINT uPos);
		BOOL Deselect(void);
		void Empty(void);

	protected:
		// Data Members
		CSystemWnd       * m_pSystem;
		CTreeView        * m_pTree;
		CArray <HTREEITEM> m_Index;
		BOOL		   m_fValid;
		BOOL		   m_fActive;
		BOOL		   m_fShow;
		UINT		   m_uPos;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnClose(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnActivate(UINT uMethod, BOOL fMinimize, CWnd &Wnd);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		void OnActivateApp(BOOL fActive, DWORD dwThread);
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);
		void OnKeyDown(UINT uCode, DWORD Flags);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeReturn(UINT uID, NMHDR &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);

		// Tree Items
		HTREEITEM GetFirst(void);
		HTREEITEM GetLast(void);

		// Implementation
		void DoLoad(CStringArray const &List);
		BOOL LoadConfig(CRect &Rect);
		void SaveConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CItemViewWnd;
class CItemDialog;
class CViewDialog;
class CCatViewWnd;

//////////////////////////////////////////////////////////////////////////
//
// Proxy View Window
//

class DLLAPI CItemViewWnd : public CViewWnd, public IUpdate
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CItemViewWnd(UINT uType, UINT uFlags);

		// Destructor
		~CItemViewWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// Attributes
		CViewWnd * GetView(void) const;

		// Operations
		void Refresh(void);
		void Purge(void);
		void SetFlag(UINT uFlag, BOOL fSet);
		void SetPreNav(CString PreNav);
		void EndPreNav(BOOL fExec);

	protected:
		// Cache Type
		typedef CZeroMap <PCVOID, CViewWnd *> CCache;

		// Data Members
		UINT	      m_uType;
		UINT	      m_uFlags;
		CSysProxy     m_System;
		CString       m_PreNav;
		CCache        m_Cache;
		CToolbarWnd * m_pTool;
		CViewWnd    * m_pView;
		BOOL          m_fNull;
		CItem       * m_pParent;
		CLASS         m_Class;

		// Overribables
		void    OnAttach(void);
		BOOL    OnPreDetach(void);
		void    OnDetach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Interface Control
		void OnUpdateInterface(void);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnCancelMode(void);
		void OnSetCurrent(BOOL fCurrent);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnSize(UINT uType, CSize Size);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnGoingIdle(void);
		void OnUpdateUI(void);

		// Command Handlers
		BOOL OnViewRefresh(UINT uID);

		// Implementation
		BOOL  CreateView(void);
		CRect GetViewRect(void);
		CLASS GetViewClass(CWnd *pView);
		void  HideView(CViewWnd *pView);
		BOOL  KillView(CViewWnd *pView);
		void  ClearToolbar(void);
		BOOL  UpdateToolbar(void);
		void  ShowInfo(void);
		BOOL  IsItemPrivate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Item Dialog Window
//

class DLLAPI CItemDialog : public CDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CItemDialog(CItem *pItem);
		CItemDialog(CItem *pItem, CString Caption);
		CItemDialog(CItem *pItem, CString Caption, UINT uView);

		// Creation
		BOOL Create(CWnd &Parent);

		// Attributes
		BOOL IsReadOnly(void) const;

		// Operations
		void Resize(void);
		void ForceReadOnly(void);

	protected:
		// Data Members
		CItem	   * m_pItem;
		UINT         m_uView;
		BOOL	     m_fRead;
		CViewWnd   * m_pView;
		CSize	     m_Button;
		CRect        m_Border;
		int          m_xExtra;
		int          m_yLimit;
		CButton	   * m_pButton1;
		CButton	   * m_pButton2;
		HGLOBAL	     m_hData;

		// Class Definition
		PCTXT GetDefaultClassName(void) const;
		BOOL  GetClassDetails(WNDCLASSEX &Class) const;

		// Translation Hook
		BOOL Translate(MSG &Msg);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCancel(UINT uID);

		// Message Handlers
		void OnPostCreate(void);
		void OnClose(void);
		void OnCancelMode(void);
		void OnSetFocus(CWnd &Wnd);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		void OnSize(UINT uCode, CSize Size);
		void OnGetMinMaxInfo(MINMAXINFO &Info);

		// View Menu
		BOOL OnViewRefresh(UINT uID);

		// Implementation
		void  Construct(void);
		BOOL  UseLightBack(void);
		void  EditSystemMenu(void);
		void  FindLayout(void);
		void  CreateView(void);
		void  CreateButtons(void);
		void  FindBestSize(void);
		CRect GetViewRect(void);
		void  MoveButtons(void);
		void  SaveData(void);
		void  LoadData(void);
		void  FreeData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// View Dialog Window
//

class DLLAPI CViewDialog : public CDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CViewDialog(void);
		CViewDialog(CString Caption, CViewWnd *pView);

		// Dialog Operations
		BOOL Create(CWnd &Parent);

	protected:
		// Data Members
		CViewWnd   * m_pView;
		CSize	     m_Border;
		CSize	     m_Button;
		CButton	   * m_pButton1;

		// Class Definition
		PCTXT GetDefaultClassName(void) const;
		BOOL  GetClassDetails(WNDCLASSEX &Class) const;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Message Handlers
		void OnPostCreate(void);
		void OnClose(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSetFocus(CWnd &Wnd);
		void OnSize(UINT uCode, CSize Size);
		void OnGetMinMaxInfo(MINMAXINFO &Info);

		// Implemenataion
		void  FindLayout(void);
		void  CreateView(void);
		void  CreateButtons(void);
		void  FindBestSize(void);
		CRect GetViewRect(void);
		void  MoveButtons(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Category View Window
//

class DLLAPI CCatViewWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCatViewWnd(void);

		// Destructor
		~CCatViewWnd(void);

		// Attributes
		CViewWnd * GetView(void) const;
		CViewWnd * GetView(CLASS Class) const;

	protected:
		// Cache Type
		typedef CZeroMap <CItem *, CViewWnd *> CCache;

		// Data Members
		CViewWnd * m_pView;
		CCache     m_Cache;

		// Overribables
		void OnAttach(void);
		BOOL OnPreDetach(void);
		void OnDetach(void);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Implementation
		BOOL ForwardToAll(MSG const &Message);
		BOOL ForwardToActive(MSG const &Message);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGhostBar;
class CGhostShadowWnd;

//////////////////////////////////////////////////////////////////////////
//
// Ghost Bar Window
//

class DLLAPI CGhostBar : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGhostBar(void);

		// Destructor
		~CGhostBar(void);

		// Operations
		BOOL AddBar(CMenu &Menu);
		BOOL Create(CWnd &Wnd, CPoint const &Pos);
		void PollGadgets(void);
		BOOL SetAlpha(CPoint const &Pos);
		void SetAlpha(BYTE bAlpha);

	protected:
		// Typedefs
		typedef CArray <CToolbarWnd *> CBarList;

		// Data Members
		CBarList          m_Bars;
		CGhostShadowWnd * m_pShadow;
		CWnd            * m_pWnd;
		int	          m_nMax;
		int	          m_nHit;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnDestroy(void);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnActivate(UINT uMethod, BOOL fMinimize, CWnd &Wnd);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnPaint(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ghost Bar Shadow
//

class DLLAPI CGhostShadowWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGhostShadowWnd(void);

		// Destructor
		~CGhostShadowWnd(void);

		// Operations
		void Create  (CWnd &Wnd);
		void SetAlpha(BYTE bAlpha);

	protected:
		// Data Members
		int         m_nUmbra;
		int         m_nPenumbra;
		int         m_nTotal;
		CRect       m_Rect;
		CPoint      m_Pos;
		CSize       m_Size;
		CMemoryDC * m_pDC;
		CBitmap   * m_pBitmap;
		PBYTE       m_pData;

		// Implementation
		void CreateImage(void);
		void DeleteImage(void);
		void Draw(void);
	};
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPaneWnd;
class CCatPaneWnd;
class CResPaneWnd;
class CNavPaneWnd;

//////////////////////////////////////////////////////////////////////////
//
// Pane Window
//

class DLLAPI CPaneWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CPaneWnd(void);

		// Attributes
		CViewWnd * GetView(void) const;

	protected:
		// Data Members
		CString	      m_Title;
		UINT	      m_ShowID;
		CViewWnd    * m_pView;
		CToolbarWnd * m_pTB1;
		CToolbarWnd * m_pTB2;
		CToolbarWnd * m_pTB3;
		CRect         m_TBRect1;
		CRect         m_TBRect2;
		CRect         m_TBRect3;
		BOOL	      m_fShow2;
		BOOL	      m_fShow3;
		BOOL	      m_fValid;
		UINT	      m_uCapture;

		// Interface Control
		virtual void OnUpdateInterface(void);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnCancelMode(void);
		void OnGoingIdle(void);
		void OnSetFocus(CWnd &Wnd);
		void OnSetCurrent(BOOL fCurrent);
		void OnUpdateUI(void);
		void OnKeyDown(UINT uCode, DWORD dwData);

		// Command Handlers
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Implementation
		void CalcPositions(void);
		void ClearToolbars(void);
		void UpdateToolbars(void);
		void UpdateLayout(void);

		// Overridable
		virtual CRect GetClientRect(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Category Pane Window
//

class DLLAPI CCatPaneWnd : public CPaneWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CCatPaneWnd(void);

		// Attributes
		CString GetCat(void) const;

		// Operations
		void ShowCat(CString  Cat);
		void SwapCat(CString &Cat);

	protected:
		// Static Data
		static UINT timerCheck;

		// Layout Constants
		static int const sizeCategory;
		static int const sizeToolbar;
		static int const sizeSplit;

		// Data Members
		CString      m_Prompt;
		CString      m_KeyName;
		UINT	     m_uBase;
		UINT	     m_Accel;
		CAccelerator m_Accel1;
		CAccelerator m_Accel2;
		CCatList     m_Cats;
		CCursor      m_Cursor1;
		CCursor      m_Cursor2;
		CRect	     m_Split;
		UINT	     m_uCount;
		UINT	     m_uShow;
		UINT	     m_uSelect;
		UINT	     m_uHover;
		BOOL	     m_fPress;
		int          m_nTrackOrg;
		int          m_nTrackPos; 
		int          m_nTrackMin;
		int          m_nTrackMax; 

		// Message Map
		AfxDeclareMessageMap();

		// Accelerator
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		void OnPaint(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnSetCurrent(BOOL fCurrent);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Command Handlers
		BOOL OnGoGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);

		// Overridables
		CRect GetClientRect(void);

		// Drawing Helpers
		void DrawSplit(CDC &DC);
		void DrawCategory(CDC &DC, CRect Draw, UINT uCat);
		void ShowDragBar(CDC &DC);
	
		// Item Location
		CMetaItem * FindCatItem(UINT uCat);

		// Category Selection
		virtual void OnSetCategory(CItem *pItem);

		// Implementation
		UINT GetCaptureMode(void);
		UINT GetCaptureItem(void);
		BOOL Select(CString Nav);
		void Select(UINT uSelect);
		BOOL AttachItemView(void);
		void InvalidateCat(UINT uCat);
		UINT CheckHover(CPoint Pos);
		UINT CalcShowCount(void);
		void FindConfig(CRegKey &Key);
		void LoadConfig(void);
		void SaveConfig(void);
		BOOL UpdateAll(void);
		BOOL FindSplit(void);
		BOOL CheckSplit(void);
		int  GetSplitLimit(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Resource Pane Window
//

class DLLAPI CResPaneWnd : public CCatPaneWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CResPaneWnd(void);

		// Attributes
		BOOL HasCat(CString Cat) const;

		// Operations
		BOOL ShowNavCats(CNavPaneWnd *pNav);
		void SetItem(CString Path);

	protected:
		// Data Members
		CTree <CString>        m_Res;
		CMap  <CLASS, CString> m_Last;

		// Overridables
		void OnAttach(void);
		BOOL OnPreDetach(void);
		void OnDetach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnViewRefresh(UINT uID);

		// Category Selection
		void OnSetCategory(CItem *pItem);

		// Implementation
		CString FindLastRes(void);
		void    SaveLastRes(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Navigation Pane Window
//

class DLLAPI CNavPaneWnd : public CCatPaneWnd, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CNavPaneWnd(void);

		// Attributes
		CItem * GetNavItem(void) const;
		CString GetResList(void) const;

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		
		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Static Data
		static UINT m_timerSelect;

		// Data Members
		CSysProxy   m_System;
		CDropHelper m_DropHelp;
		UINT        m_uDrop;

		// Overridables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Command Handlers
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		BOOL OnViewRefresh(UINT uID);

		// Category Selection
		void OnSetCategory(CItem *pItem);

		// Implementation
		void SetDrop(CPoint Pos);
		void SetDrop(UINT uDrop);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStdTreeWnd;
class CDotTreeWnd;
class CResTreeWnd;
class CNavTreeWnd;
class CEmptyPromptWnd;

//////////////////////////////////////////////////////////////////////////
//
// Standard Tree Window
//
	
class DLLAPI CStdTreeWnd : public CViewWnd, public IDropSource, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CStdTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropSource
		HRESULT METHOD QueryContinueDrag(BOOL fEscape, DWORD dwKeys);
		HRESULT METHOD GiveFeedback(DWORD dwEffect);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Object Maps
		typedef CZeroMap <CString, HTREEITEM> CNameMap;

		// Static Data
		static UINT m_timerDouble;

		// Data Members
		BOOL	        m_fInitRoot;
		CSysProxy	m_System;
		CDropHelper	m_DropHelp;
		CCursor		m_LinkCursor;
		CCursor		m_MoveCursor;
		CCursor		m_CopyCursor;
		UINT	        m_Accel;
		CAccelerator    m_Accel1;
		CAccelerator    m_Accel2;
		CImageList	m_Images;
		CMulTreeView  * m_pTree;
		DWORD		m_dwStyle;
		HTREEITEM	m_hRoot;
		HTREEITEM	m_hSelect;
		CMetaItem     * m_pItem;
		CMetaItem     * m_pSelect;
		CMetaItem     * m_pNamed;
		CString         m_SelPath;
		BOOL		m_fMulti;
		BOOL            m_fLoading;

		// Drag Context
		CPoint		m_DragPos;
		CSize		m_DragOffset;
		CSize		m_DragSize;
		CBitmap		m_DragImage;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);
		void OnTreeReturn(UINT uID, NMHDR &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		void OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeExpanding(UINT uID, NMTREEVIEW &Info);
		void OnTreeContextMenu(UINT uID, NMTREEVIEW &Info);
		void OnTreeChangeMulti(UINT uID, NMTREEVIEW &Info);
		void OnTreeOfferFocus(UINT uID, NMTREEVIEW &Info);

		// Data Object Construction
		virtual BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
		virtual BOOL MakeDataObject(IDataObject * &pData);

		// Drag Support
		BOOL DragItem(void);

		// Drag Hooks
		virtual void  FindDragMetrics(void);
		virtual void  MakeDragImage(void);
		virtual DWORD FindDragAllowed(void);
		virtual void  DragComplete(DWORD dwEffect);

		// Tree Loading
		virtual void LoadImageList(void);
		virtual void LoadTree(void);
		virtual void SortTree(void);

		// Item Hooks
		virtual BOOL    IncludeItem(CMetaItem *pItem);
		virtual void    NewItemSelected(void);
		virtual BOOL    GetItemMenu(CString &Name, BOOL fHit);
		virtual void    AddExtraCommands(CMenu &Menu);
		virtual UINT    GetRootImage(void);
		virtual UINT    GetItemImage(CMetaItem *pItem);
		virtual CString GetItemText(CMetaItem *pItem);
		virtual BOOL    HasFolderImage(HTREEITEM hItem);
		virtual void    KillItem(HTREEITEM hItem, BOOL fRemove);
		virtual void    OnItemDeleted(CItem *pItem, BOOL fExec);
		virtual void    OnItemRenamed(CItem *pItem);

		// Selection Hooks
		virtual CString SaveSelState(void);
		virtual void    LoadSelState(CString State);

		// Item Access
		CMetaItem * GetItemPtr(HTREEITEM hItem);
		CMetaItem * GetItemOpt(HTREEITEM hItem);

		// Implementation
		BOOL IsParent(void);
		BOOL IsParent(HTREEITEM hItem);
		void SetFolderImage(HTREEITEM hItem, UINT uAction);
		BOOL ExpandItem(HTREEITEM hItem, UINT uAction);
		void ExpandItem(HTREEITEM hItem);
		void ToggleItem(HTREEITEM hItem);
		void SelectRoot(void);
		BOOL SelectInit(void);
		void SetRedraw(BOOL fRedraw);
		void RefreshTree(void);
		void KillTree(HTREEITEM hRoot);
		void KillTree(HTREEITEM hRoot, BOOL fRemove);
		void KillData(HTREEITEM hRoot, BOOL fRemove);
		BOOL IsReadOnly(void);
		BOOL IsPrivate(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Tree Window for Items with Dotted Names
//
	
class DLLAPI CDotTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CDotTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		CString		 m_List;
		CLASS		 m_Folder;
		CLASS		 m_Class;
		CNameMap	 m_MapNames;
		CNameMap	 m_MapFixed;
		CItemIndexList * m_pList;

		// Overridables
		void OnAttach(void);

		// Tree Loading
		void LoadImageList(void);
		void LoadTree(void);
		void LoadTree(HTREEITEM hRoot, INDEX &Index, UINT uThis);
		void LoadRoot(void);
		void LoadRootItem(CTreeViewItem &Root);
		void LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem);

		// Item Hooks
		BOOL IncludeItem(CMetaItem *pItem);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL HasFolderImage(HTREEITEM hItem);
		void KillItem(HTREEITEM hRoot, BOOL fRemove);

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Name Parsing
		CString GetName(CString Path);
		CString GetRoot(CString Path, BOOL fDot);
		CString GetRoot(CItem *pItem, BOOL fDot);
		CString GetRoot(HTREEITEM hItem, BOOL fDot);
		CString GetRoot(BOOL fDot);

		// Implementation
		BOOL IsFolder(void);
		BOOL IsFolder(HTREEITEM hItem);
		void AddToMaps(CMetaItem *pItem, HTREEITEM hItem);
		void RemoveFromMaps(CMetaItem *pItem);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Resource Tree Window
//
	
class DLLAPI CResTreeWnd : public CDotTreeWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CResTreeWnd(CString List, CLASS Folder, CLASS Class);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// Overridables
		BOOL OnNavigate(CString const &Nav);

	protected:
		// Data Members
		UINT m_cfIdent;
		BOOL m_fJumpTo;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		
		// Notification Handlers
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);

		// Command Handlers
		BOOL OnResGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnResControl(UINT uID, CCmdSource &Src);
		BOOL OnResCommand(UINT uID);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData);
		BOOL AddIdentifier(CDataObject *pData);
		
		// Drag Hooks
		DWORD FindDragAllowed(void);

		// Item Hooks
		void NewItemSelected(void);
		BOOL GetItemMenu(CString &Name, BOOL fHit);

	};		

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window
//

class DLLAPI CNavTreeWnd : public CDotTreeWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CNavTreeWnd(CString List, CLASS Folder, CLASS Class);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		
		// Editing Actions
		class CCmdCreate;

	private:
		// Sort Context
		static CNavTreeWnd * m_pSort;
		static int           m_nSort;

	protected:
		// Tree Modes
		enum
		{
			modeSelect,
			modePick
			};

		// Drop Codes
		enum
		{
			dropNone,
			dropAccept,
			dropOther
			};

		// Accept Context
		struct CAcceptCtx
		{
			CString        m_Root;
			CString        m_Prev;
			BOOL           m_fPaste;
			BOOL	       m_fCheck;
			BOOL	       m_fUndel;
			CStringArray * m_pNames;
			};

		// Editing Actions
		class CCmdRename;
		class CCmdDelete;
		class CCmdPaste;
		class CCmdLock;
		class CCmdOrder;
		class CCmdPrivate;

		// Static Data
		static UINT    m_timerSelect;
		static UINT    m_timerScroll;
		static CItem * m_pKilled;

		// Data Members
		CString m_Empty;
		CWnd *  m_pEmpty;
		UINT	m_cfData;
		BOOL	m_fInitSel;
		BOOL	m_fLocked;
		UINT    m_uMode;
		UINT    m_uPick;
		CString m_LastFind;

		// Drop Context
		UINT	  m_uDrop;
		BOOL	  m_fDropLocal;
		BOOL	  m_fDropMove;
		HTREEITEM m_hDropRoot;
		HTREEITEM m_hDropPrev;

		// Overridables
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnSize(UINT uCode, CSize Size);

		// Notification Handlers
		void OnTreePickItem(UINT uID, NMTREEVIEW &Info);
		BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
		void OnTreeReturn(UINT uID, NMHDR &Info);
		void OnTreeDblClk(UINT uID, NMHDR &Info);
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);

		// Command Handlers
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Go Menu
		BOOL OnGoNext(BOOL fSiblings);
		BOOL OnGoPrev(BOOL fSiblings);
		BOOL StepAway(void);

		// Item Menu : Create
		void OnItemCreateFolder(void);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);

		// Item Menu : Rename
		BOOL OnItemRename(CString Name);
		void OnExecRename(CCmdRename *pCmd);
		void OnUndoRename(CCmdRename *pCmd);
		void RenameItem(CString Name, CString Prev);
		BOOL RenameFrom(HTREEITEM hRoot, CString const &Name, CString const &Prev);
		
		// Item Menu : Delete
		void OnItemDelete(UINT uVerb, BOOL fMove, BOOL fWarn);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);
		void PerformDelete(BOOL fMove);
		void DeleteFromHere(HTREEITEM hItem, BOOL fMove);

		// Item Menu : Sort
		void OnItemSort(int (*pfnSort)(PCVOID, PCVOID));
		void OnExecOrder(CCmdOrder *pCmd);
		void OnUndoOrder(CCmdOrder *pCmd);

		// Item Menu : Find
		void OnItemFind(void);

		// Item Menu : Private
		void OnItemPrivate(void);

		// Edit Menu : Cut and Copy
		void OnEditCut(void);
		void OnEditCopy(void);

		// Edit Menu : Paste
		BOOL CanEditPaste(void);
		void OnEditPaste(void);
		void OnEditPaste(IDataObject *pData);
		void OnEditPaste(CItem *pItem, HTREEITEM hRoot, HTREEITEM hPrev, IDataObject *pData);
		void OnExecPaste(CCmdPaste *pCmd);
		void OnExecPasteStream(CCmdPaste *pCmd);
		void OnUndoPaste(CCmdPaste *pCmd);

		// Data Object Construction
		virtual BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);
		virtual BOOL AddItemToStream(ITextStream &Stream, HTREEITEM hItem);
		virtual void AddItemToTreeFile(CTreeFile &Tree, HTREEITEM hItem);

		// Data Object Acceptance
		virtual BOOL CanAcceptDataObject(IDataObject *pData, BOOL &fLocal);
		virtual BOOL CanAcceptStream(IDataObject *pData, BOOL &fLocal);
		virtual BOOL CanAcceptStream(ITextStream &Stream, BOOL &fLocal);
		virtual BOOL AcceptDataObject(IDataObject *pData, CAcceptCtx const &Ctx);
		virtual BOOL AcceptStream(IDataObject *pData, CAcceptCtx const &Ctx);
		virtual BOOL AcceptStream(ITextStream &Stream, CAcceptCtx const &Ctx, BOOL fLocal);
		virtual BOOL GetStreamInfo(ITextStream &Stream, BOOL &fLocal);

		// Data Object Hooks
		virtual void OnAcceptInit(CTreeFile &Tree);
		virtual BOOL OnAcceptName(CTreeFile &Tree, BOOL fLocal, CString Name);
		virtual void OnAcceptDone(CTreeFile &Tree, HTREEITEM hItem);

		// Drag Hooks
		DWORD FindDragAllowed(void);

		// Drop Support
		void DropTrack(CPoint Pos, BOOL fMove);
		BOOL DropDone(IDataObject *pData);
		void DropDebug(void);
		void ShowDrop(BOOL fShow);
		BOOL ShowDropVert(HTREEITEM hItem);
		BOOL IsDropMove(DWORD dwKeys, DWORD *pEffect);
		BOOL IsValidDrop(void);
		BOOL IsExpandDrop(void);
		BOOL IsFolderDrop(void);

		// Item Addition
		void      AddToList(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem);
		HTREEITEM AddToTree(HTREEITEM hRoot, HTREEITEM hPrev, CMetaItem *pItem);

		// Item Hooks
		void NewItemSelected(void);
		BOOL GetItemMenu(CString &Name, BOOL fHit);

		// Lock Support
		virtual BOOL IsItemLocked(HTREEITEM hItem);

		// Update Hook
		virtual void OnListUpdated(void);

		// Implementation
		void NormalizeList(void);
		BOOL SetMode(UINT uMode);
		BOOL WarnMultiple(UINT uVerb);
		BOOL MarkMulti(CString Item, CString Menu, BOOL fMark);
		void MakeUnique(CString &Name);
		void WalkToLast(HTREEITEM &hItem);
		void LockUpdate(BOOL fLock);
		void ListUpdated(void);
		void LockItem(BOOL fLock);
		void HideItem(BOOL fHide);
		void SetViewedItem(BOOL fCheck);
		BOOL IsSelLocked(void);
		BOOL IsRootSelected(void);
		void CheckInitSel(void);
		void GetEmptyRect(CRect &Rect);
		BOOL IsItemPrivate(HTREEITEM hItem);
		
		// Sort Function
		static int SortByName(PCVOID p1, PCVOID p2);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Create Command
//

class DLLAPI CNavTreeWnd::CCmdCreate : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdCreate(CItem *pParent, CString Name, CLASS Class);
		CCmdCreate(CItem *pParent, CMetaItem *pItem);

		// Destructor
		~CCmdCreate(void);

		// Data Members
		CString m_Name;
		CLASS   m_Class;
		HGLOBAL m_hData;
		CString m_Make;
		CString m_Fixed;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Rename Command
//

class DLLAPI CNavTreeWnd::CCmdRename : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdRename(CMetaItem *pItem, CString Name);

		// Data Members
		CString m_Prev;
		CString m_Name;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Delete Command
//

class DLLAPI CNavTreeWnd::CCmdDelete : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdDelete( CString       Menu,
			    CString       Item,
			    CItem       * pRoot,
			    CItem       * pPrev,
			    IDataObject * pData,
			    BOOL          fMove
			    );

		// Destructor
		~CCmdDelete(void);

		// Data Members
		CString       m_Root;
		CString       m_Prev;
		IDataObject * m_pData;
		BOOL          m_fMove;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Paste Command
//

class DLLAPI CNavTreeWnd::CCmdPaste : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPaste( CItem       * pItem,
			   CItem       * pRoot,
			   CItem       * pPrev,
			   IDataObject * pData,
			   BOOL          fMove
			   );

		// Destructor
		~CCmdPaste(void);

		// Data Members
		CString       m_Root;
		CString       m_Prev;
		IDataObject * m_pData;
		BOOL          m_fInit;
		BOOL	      m_fMove;
		CStringArray  m_Names;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Lock Command
//

class DLLAPI CNavTreeWnd::CCmdLock : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdLock(CMetaItem *pItem, BOOL fLock);

		// Data Members
		BOOL m_fPrev;
		BOOL m_fLock;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Order Command
//

class DLLAPI CNavTreeWnd::CCmdOrder : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdOrder(CMetaItem *pItem);

		// Data Members
		CArray <UINT> m_Old;
		CArray <UINT> m_New;
	};

//////////////////////////////////////////////////////////////////////////
//
// Navigation Tree Window -- Private Command
//

class DLLAPI CNavTreeWnd::CCmdPrivate : public CStdCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdPrivate(CMetaItem *pItem, BOOL fHide);

		// Data Members
		BOOL m_fPrev;
		BOOL m_fHide;
	};

//////////////////////////////////////////////////////////////////////////
//
// Empty Tree Prompt Window
//

class DLLAPI CEmptyPromptWnd : public CWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEmptyPromptWnd(void);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnSetFocus(CWnd &Wnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Private View Window
//

class DLLAPI CPrivateViewWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrivateViewWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
	};

// End of File

#endif
