
#include "Intern.hpp"

#include "KeyboardItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Keyboard Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CKeyboardItem, CUSBPortItem);

// Constructor

CKeyboardItem::CKeyboardItem(void)
{
	m_Enable = 1;	// Enable for Old Databases.

	m_Layout = 0;
	}

// UI Update

void CKeyboardItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == L"Enable" ) {

			pHost->EnableUI(L"Layout", m_Enable);
			}
		}

	CUSBPortItem::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

UINT CKeyboardItem::GetTreeImage(void) const
{
	return IDI_KEYBOARD;
	}

UINT CKeyboardItem::GetType(void) const
{
	return typeKeyboard;
	}

// Persistance

void CKeyboardItem::Init(void)
{
	// Disable by Default.

	m_Enable = 0;

	CUSBPortItem::Init();
	}

// Download Support

BOOL CKeyboardItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Enable));
	Init.AddWord(WORD(m_Layout));

	return TRUE;
	}

// Meta Data Creation

void CKeyboardItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddInteger(Layout);

	Meta_SetName(IDS_KEYBOARD);
	}

// End of File
