
#include "intern.hpp"

#include "catpyro.hpp"
#define _AvgCylTempAddr	0x15E	// Get Low Channel AvgCylTemp
#define _pyroBaseAddr	0x1AA // Get ALL Low Channel Data


//////////////////////////////////////////////////////////////////////////
//
// CatPyro Master Driver
//

// Instantiator

INSTANTIATE(CCatPyroMasterDriver);

// Constructor

CCatPyroMasterDriver::CCatPyroMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CCatPyroMasterDriver::~CCatPyroMasterDriver(void)
{
	}

// Configuration

void MCALL CCatPyroMasterDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Drop = GetByte(pData);

		Debug("CCatPyroMasterDriver::Load -- Drop is %2.2X\n", m_Drop);
		}
	}
	
void MCALL CCatPyroMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Debug("CCatPyroMasterDriver::CheckConfig\n");
	}
	
// Management

void MCALL CCatPyroMasterDriver::Attach(IPortObject *pPort)
{
	Debug("CCatPyroMasterDriver::Bind\n");

	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CCatPyroMasterDriver::Open(void)
{
	Debug("CCatPyroMasterDriver::Open\n");
	}

// Device

CCODE MCALL CCatPyroMasterDriver::DeviceOpen(IDevice *pDevice)
{
	Debug("CCatPyroMasterDriver::DeviceOpen\n");
	return CMasterDriver::DeviceOpen(pDevice);
	}

CCODE MCALL CCatPyroMasterDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CCatPyroMasterDriver::Ping(void)
{
	Debug("CCatPyroMasterDriver::Ping\n");
	return CCODE_SUCCESS;
	}

void MCALL CCatPyroMasterDriver::Service(void)
{	
//	Debug("CCatPyroMasterDriver::Service\n");
	}

CCODE MCALL CCatPyroMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
// Debug("CCatPyroMasterDriver::Read type=%u addr=%c%u count=%u\n", Addr.a.m_Type, Addr.a.m_Table, Addr.a.m_Offset, uCount);
	if( Addr.a.m_Table == 'D' ) {

		if( Addr.a.m_Type == addrLongAsLong ) {

			SendReadRequest(Addr.a.m_Offset);

			if( AcceptReadReply(pData, Addr.a.m_Offset) ) {

				*pData = tLookup(pData);
//** DEBUG
//DWORD T1 =(*pData*1.8)+32;
//Debug("CCatPyroMasterDriver::T1-LOOKUP HEX[0x%4.4x] DegC[%u] DegF[%u]\n",*pData,*pData,T1);
				return 1;
			}

			return CCODE_ERROR;
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CCatPyroMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//Debug("CCatPyroMasterDriver::Write type=%u addr=%c%u count=%u\n", Addr.a.m_Type, Addr.a.m_Table, Addr.a.m_Offset, uCount);
	// NOTE -- We don't support writes but it's easier to just say
	// that they worked than risk causing comms errors because of
	// one badly configured tag in the database.

	return uCount;
	}

// Implementation


void CCatPyroMasterDriver::SendReadRequest(UINT uAddr)
{
	CRC16 crc;

	WORD  wAddr = _pyroBaseAddr + (2 * uAddr);

	if(uAddr == 25)	// D25 is Avg Cylinder Temp.  Pyro Address is 0x5E
		wAddr = _AvgCylTempAddr;

//Debug("CCatPyroMasterDriver::SendReadRequest wAddr=%2.4X uAddr=%u\n", wAddr,uAddr);

	m_bSend[ 0] = STX;
	m_bSend[ 1] = BYTE(m_Drop);
	m_bSend[ 2] = 0x60;
	m_bSend[ 3] = 0x52;
	m_bSend[ 4] = HIBYTE(wAddr);
	m_bSend[ 5] = LOBYTE(wAddr);
	m_bSend[ 6] = 0;
	m_bSend[ 7] = 0;

	crc.Clear();

	for( UINT n = 1; n < 8; n++ ) {

		crc.Add(m_bSend[n]);
		}

	m_bSend[ 8] = LOBYTE(crc.GetValue());
	m_bSend[ 9] = HIBYTE(crc.GetValue());
	m_bSend[10] = ETX;

	m_pData->ClearRx();

	m_pData->Write(m_bSend, elements(m_bSend), NOTHING);
}


BOOL CCatPyroMasterDriver::AcceptReadReply(PDWORD pData, UINT uAddr)
{
	CRC16 crc;

	WORD  wAddr  = _pyroBaseAddr + (2 * uAddr);
	if(uAddr == 25)	{ // D25 is Avg Cylinder Temp.  Pyro Address is 0x5E
		wAddr = _AvgCylTempAddr;
	}

	UINT  uState = 0;

	UINT  uCount = 0;

	WORD  wCRC   = 0;

	SetTimer(250);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			return FALSE;
			}

		switch( uState ) {

			case 0:
				if( uData == STX ) {

					crc.Clear();

					uCount = 0;

					uState = 1;
					}
				break;

			case 1:
				m_bRecv[uCount++] = BYTE(uData);

				crc.Add(BYTE(uData));

				if( uCount == elements(m_bRecv) ) {

/*
Debug("Recv<-");
for(int idx = 0; idx < uCount; idx++)
	Debug("[%2.2X]", m_bRecv[idx]);
Debug("\n");
*/


					uState = 2;
					}
				break;

			case 2:
				wCRC   = uData;

				uState = 3;

				break;

			case 3:
				wCRC   = MAKEWORD(wCRC, uData);

				uState = 4;

				break;

			case 4:
				if( uData == ETX ) {

					if( crc.GetValue() == wCRC ) {

						if( MAKEWORD(m_bRecv[4], m_bRecv[3]) == wAddr ) {

							*pData = MAKEWORD(m_bRecv[6], m_bRecv[5]);

							return TRUE;
							}
						else
							Debug("Bad Addr\n");
						}
					else
						Debug("Bad CRC %4.4X %4.4X\n", crc.GetValue(), wCRC);
					}
				else
					Debug("Bad ETX\n");

				return FALSE;
			}
		}

	AfxTrace("Timeout\n");

	return FALSE;
	}

DWORD CCatPyroMasterDriver::tLookup(PDWORD pData)
{
	DWORD T1 = *pData;
//Debug("CCatPyroMasterDriver::T1-RAW HEX[0x%4.4x] DEC[%ul]\n", T1,T1);
	if(T1<39) return(0);
	if(T1<114) return(1);
	if(T1<189) return(2);
	if(T1<264) return(3);
	if(T1<339) return(4);
	if(T1<415) return(5);
	if(T1<490) return(6);
	if(T1<565) return(7);
	if(T1<640) return(8);
	if(T1<715) return(9);
	if(T1<790) return(10);
	if(T1<866) return(11);
	if(T1<941) return(12);
	if(T1<1016) return(13);
	if(T1<1091) return(14);
	if(T1<1167) return(15);
	if(T1<1242) return(16);
	if(T1<1317) return(17);
	if(T1<1392) return(18);
	if(T1<1467) return(19);
	if(T1<1542) return(20);
	if(T1<1617) return(21);
	if(T1<1693) return(22);
	if(T1<1768) return(23);
	if(T1<1843) return(24);
	if(T1<1919) return(25);
	if(T1<1994) return(26);
	if(T1<2069) return(27);
	if(T1<2144) return(28);
	if(T1<2219) return(29);
	if(T1<2294) return(30);
	if(T1<2369) return(31);
	if(T1<2444) return(32);
	if(T1<2520) return(33);
	if(T1<2595) return(34);
	if(T1<2671) return(35);
	if(T1<2746) return(36);
	if(T1<2821) return(37);
	if(T1<2896) return(38);
	if(T1<2971) return(39);
	if(T1<3046) return(40);
	if(T1<3121) return(41);
	if(T1<3196) return(42);
	if(T1<3271) return(43);
	if(T1<3348) return(44);
	if(T1<3423) return(45);
	if(T1<3498) return(46);
	if(T1<3573) return(47);
	if(T1<3648) return(48);
	if(T1<3723) return(49);
	if(T1<3798) return(50);
	if(T1<3873) return(51);
	if(T1<3948) return(52);
	if(T1<4023) return(53);
	if(T1<4099) return(54);
	if(T1<4176) return(55);
	if(T1<4253) return(56);
	if(T1<4330) return(57);
	if(T1<4407) return(58);
	if(T1<4484) return(59);
	if(T1<4560) return(60);
	if(T1<4637) return(61);
	if(T1<4714) return(62);
	if(T1<4791) return(63);
	if(T1<4869) return(64);
	if(T1<4945) return(65);
	if(T1<5022) return(66);
	if(T1<5099) return(67);
	if(T1<5176) return(68);
	if(T1<5253) return(69);
	if(T1<5329) return(70);
	if(T1<5406) return(71);
	if(T1<5483) return(72);
	if(T1<5560) return(73);
	if(T1<5638) return(74);
	if(T1<5715) return(75);
	if(T1<5791) return(76);
	if(T1<5868) return(77);
	if(T1<5945) return(78);
	if(T1<6022) return(79);
	if(T1<6099) return(80);
	if(T1<6175) return(81);
	if(T1<6252) return(82);
	if(T1<6329) return(83);
	if(T1<6407) return(84);
	if(T1<6484) return(85);
	if(T1<6560) return(86);
	if(T1<6637) return(87);
	if(T1<6714) return(88);
	if(T1<6791) return(89);
	if(T1<6868) return(90);
	if(T1<6945) return(91);
	if(T1<7021) return(92);
	if(T1<7098) return(93);
	if(T1<7176) return(94);
	if(T1<7253) return(95);
	if(T1<7330) return(96);
	if(T1<7406) return(97);
	if(T1<7483) return(98);
	if(T1<7560) return(99);
	if(T1<7637) return(100);
	if(T1<7714) return(101);
	if(T1<7790) return(102);
	if(T1<7867) return(103);
	if(T1<7945) return(104);
	if(T1<8022) return(105);
	if(T1<8099) return(106);
	if(T1<8176) return(107);
	if(T1<8251) return(108);
	if(T1<8326) return(109);
	if(T1<8402) return(110);
	if(T1<8477) return(111);
	if(T1<8552) return(112);
	if(T1<8628) return(113);
	if(T1<8704) return(114);
	if(T1<8779) return(115);
	if(T1<8854) return(116);
	if(T1<8930) return(117);
	if(T1<9005) return(118);
	if(T1<9080) return(119);
	if(T1<9156) return(120);
	if(T1<9231) return(121);
	if(T1<9306) return(122);
	if(T1<9382) return(123);
	if(T1<9458) return(124);
	if(T1<9533) return(125);
	if(T1<9608) return(126);
	if(T1<9684) return(127);
	if(T1<9759) return(128);
	if(T1<9834) return(129);
	if(T1<9910) return(130);
	if(T1<9985) return(131);
	if(T1<10060) return(132);
	if(T1<10136) return(133);
	if(T1<10212) return(134);
	if(T1<10287) return(135);
	if(T1<10362) return(136);
	if(T1<10438) return(137);
	if(T1<10513) return(138);
	if(T1<10588) return(139);
	if(T1<10663) return(140);
	if(T1<10739) return(141);
	if(T1<10814) return(142);
	if(T1<10890) return(143);
	if(T1<10966) return(144);
	if(T1<11041) return(145);
	if(T1<11116) return(146);
	if(T1<11191) return(147);
	if(T1<11267) return(148);
	if(T1<11342) return(149);
	if(T1<11417) return(150);
	if(T1<11493) return(151);
	if(T1<11568) return(152);
	if(T1<11644) return(153);
	if(T1<11719) return(154);
	if(T1<11795) return(155);
	if(T1<11870) return(156);
	if(T1<11945) return(157);
	if(T1<12021) return(158);
	if(T1<12096) return(159);
	if(T1<12171) return(160);
	if(T1<12246) return(161);
	if(T1<12321) return(162);
	if(T1<12396) return(163);
	if(T1<12470) return(164);
	if(T1<12544) return(165);
	if(T1<12619) return(166);
	if(T1<12693) return(167);
	if(T1<12767) return(168);
	if(T1<12841) return(169);
	if(T1<12915) return(170);
	if(T1<12989) return(171);
	if(T1<13063) return(172);
	if(T1<13138) return(173);
	if(T1<13212) return(174);
	if(T1<13286) return(175);
	if(T1<13360) return(176);
	if(T1<13434) return(177);
	if(T1<13509) return(178);
	if(T1<13583) return(179);
	if(T1<13657) return(180);
	if(T1<13731) return(181);
	if(T1<13805) return(182);
	if(T1<13880) return(183);
	if(T1<13954) return(184);
	if(T1<14028) return(185);
	if(T1<14102) return(186);
	if(T1<14176) return(187);
	if(T1<14250) return(188);
	if(T1<14324) return(189);
	if(T1<14399) return(190);
	if(T1<14473) return(191);
	if(T1<14547) return(192);
	if(T1<14622) return(193);
	if(T1<14696) return(194);
	if(T1<14770) return(195);
	if(T1<14844) return(196);
	if(T1<14918) return(197);
	if(T1<14992) return(198);
	if(T1<15066) return(199);
	if(T1<15140) return(200);
	if(T1<15214) return(201);
	if(T1<15289) return(202);
	if(T1<15364) return(203);
	if(T1<15438) return(204);
	if(T1<15512) return(205);
	if(T1<15586) return(206);
	if(T1<15660) return(207);
	if(T1<15734) return(208);
	if(T1<15808) return(209);
	if(T1<15882) return(210);
	if(T1<15956) return(211);
	if(T1<16030) return(212);
	if(T1<16105) return(213);
	if(T1<16180) return(214);
	if(T1<16254) return(215);
	if(T1<16328) return(216);
	if(T1<16402) return(217);
	if(T1<16477) return(218);
	if(T1<16553) return(219);
	if(T1<16628) return(220);
	if(T1<16703) return(221);
	if(T1<16779) return(222);
	if(T1<16855) return(223);
	if(T1<16930) return(224);
	if(T1<17005) return(225);
	if(T1<17080) return(226);
	if(T1<17156) return(227);
	if(T1<17231) return(228);
	if(T1<17306) return(229);
	if(T1<17381) return(230);
	if(T1<17457) return(231);
	if(T1<17533) return(232);
	if(T1<17608) return(233);
	if(T1<17683) return(234);
	if(T1<17759) return(235);
	if(T1<17834) return(236);
	if(T1<17909) return(237);
	if(T1<17984) return(238);
	if(T1<18060) return(239);
	if(T1<18135) return(240);
	if(T1<18210) return(241);
	if(T1<18286) return(242);
	if(T1<18362) return(243);
	if(T1<18437) return(244);
	if(T1<18512) return(245);
	if(T1<18587) return(246);
	if(T1<18663) return(247);
	if(T1<18738) return(248);
	if(T1<18813) return(249);
	if(T1<18888) return(250);
	if(T1<18964) return(251);
	if(T1<19040) return(252);
	if(T1<19115) return(253);
	if(T1<19190) return(254);
	if(T1<19266) return(255);
	if(T1<19341) return(256);
	if(T1<19416) return(257);
	if(T1<19491) return(258);
	if(T1<19567) return(259);
	if(T1<19642) return(260);
	if(T1<19717) return(261);
	if(T1<19793) return(262);
	if(T1<19869) return(263);
	if(T1<19944) return(264);
	if(T1<20019) return(265);
	if(T1<20094) return(266);
	if(T1<20170) return(267);
	if(T1<20245) return(268);
	if(T1<20320) return(269);
	if(T1<20395) return(270);
	if(T1<20471) return(271);
	if(T1<20548) return(272);
	if(T1<20625) return(273);
	if(T1<20702) return(274);
	if(T1<20779) return(275);
	if(T1<20855) return(276);
	if(T1<20932) return(277);
	if(T1<21009) return(278);
	if(T1<21086) return(279);
	if(T1<21163) return(280);
	if(T1<21239) return(281);
	if(T1<21317) return(282);
	if(T1<21394) return(283);
	if(T1<21471) return(284);
	if(T1<21548) return(285);
	if(T1<21624) return(286);
	if(T1<21701) return(287);
	if(T1<21778) return(288);
	if(T1<21855) return(289);
	if(T1<21932) return(290);
	if(T1<22008) return(291);
	if(T1<22086) return(292);
	if(T1<22163) return(293);
	if(T1<22240) return(294);
	if(T1<22317) return(295);
	if(T1<22393) return(296);
	if(T1<22470) return(297);
	if(T1<22547) return(298);
	if(T1<22624) return(299);
	if(T1<22701) return(300);
	if(T1<22778) return(301);
	if(T1<22855) return(302);
	if(T1<22932) return(303);
	if(T1<23009) return(304);
	if(T1<23086) return(305);
	if(T1<23162) return(306);
	if(T1<23239) return(307);
	if(T1<23316) return(308);
	if(T1<23393) return(309);
	if(T1<23469) return(310);
	if(T1<23547) return(311);
	if(T1<23624) return(312);
	if(T1<23701) return(313);
	if(T1<23778) return(314);
	if(T1<23854) return(315);
	if(T1<23931) return(316);
	if(T1<24008) return(317);
	if(T1<24085) return(318);
	if(T1<24162) return(319);
	if(T1<24238) return(320);
	if(T1<24316) return(321);
	if(T1<24393) return(322);
	if(T1<24470) return(323);
	if(T1<24547) return(324);
	if(T1<24624) return(325);
	if(T1<24702) return(326);
	if(T1<24779) return(327);
	if(T1<24857) return(328);
	if(T1<24935) return(329);
	if(T1<25012) return(330);
	if(T1<25091) return(331);
	if(T1<25169) return(332);
	if(T1<25246) return(333);
	if(T1<25324) return(334);
	if(T1<25402) return(335);
	if(T1<25479) return(336);
	if(T1<25557) return(337);
	if(T1<25635) return(338);
	if(T1<25712) return(339);
	if(T1<25790) return(340);
	if(T1<25869) return(341);
	if(T1<25946) return(342);
	if(T1<26024) return(343);
	if(T1<26102) return(344);
	if(T1<26179) return(345);
	if(T1<26257) return(346);
	if(T1<26335) return(347);
	if(T1<26412) return(348);
	if(T1<26490) return(349);
	if(T1<26568) return(350);
	if(T1<26646) return(351);
	if(T1<26724) return(352);
	if(T1<26802) return(353);
	if(T1<26879) return(354);
	if(T1<26957) return(355);
	if(T1<27035) return(356);
	if(T1<27112) return(357);
	if(T1<27190) return(358);
	if(T1<27267) return(359);
	if(T1<27345) return(360);
	if(T1<27424) return(361);
	if(T1<27501) return(362);
	if(T1<27579) return(363);
	if(T1<27657) return(364);
	if(T1<27734) return(365);
	if(T1<27812) return(366);
	if(T1<27890) return(367);
	if(T1<27967) return(368);
	if(T1<28045) return(369);
	if(T1<28123) return(370);
	if(T1<28201) return(371);
	if(T1<28279) return(372);
	if(T1<28357) return(373);
	if(T1<28434) return(374);
	if(T1<28512) return(375);
	if(T1<28590) return(376);
	if(T1<28667) return(377);
	if(T1<28746) return(378);
	if(T1<28824) return(379);
	if(T1<28902) return(380);
	if(T1<28981) return(381);
	if(T1<29060) return(382);
	if(T1<29138) return(383);
	if(T1<29216) return(384);
	if(T1<29295) return(385);
	if(T1<29373) return(386);
	if(T1<29451) return(387);
	if(T1<29529) return(388);
	if(T1<29608) return(389);
	if(T1<29687) return(390);
	if(T1<29765) return(391);
	if(T1<29843) return(392);
	if(T1<29922) return(393);
	if(T1<30000) return(394);
	if(T1<30078) return(395);
	if(T1<30156) return(396);
	if(T1<30235) return(397);
	if(T1<30313) return(398);
	if(T1<30391) return(399);
	if(T1<30470) return(400);
	if(T1<30549) return(401);
	if(T1<30627) return(402);
	if(T1<30705) return(403);
	if(T1<30784) return(404);
	if(T1<30862) return(405);
	if(T1<30940) return(406);
	if(T1<31018) return(407);
	if(T1<31097) return(408);
	if(T1<31175) return(409);
	if(T1<31254) return(410);
	if(T1<31332) return(411);
	if(T1<31411) return(412);
	if(T1<31489) return(413);
	if(T1<31567) return(414);
	if(T1<31645) return(415);
	if(T1<31724) return(416);
	if(T1<31802) return(417);
	if(T1<31880) return(418);
	if(T1<31958) return(419);
	if(T1<32038) return(420);
	if(T1<32116) return(421);
	if(T1<32194) return(422);
	if(T1<32273) return(423);
	if(T1<32351) return(424);
	if(T1<32429) return(425);
	if(T1<32507) return(426);
	if(T1<32586) return(427);
	if(T1<32664) return(428);
	if(T1<32742) return(429);
	if(T1<32822) return(430);
	if(T1<32900) return(431);
	if(T1<32979) return(432);
	if(T1<33058) return(433);
	if(T1<33137) return(434);
	if(T1<33215) return(435);
	if(T1<33294) return(436);
	if(T1<33373) return(437);
	if(T1<33452) return(438);
	if(T1<33530) return(439);
	if(T1<33610) return(440);
	if(T1<33689) return(441);
	if(T1<33767) return(442);
	if(T1<33846) return(443);
	if(T1<33925) return(444);
	if(T1<34004) return(445);
	if(T1<34082) return(446);
	if(T1<34161) return(447);
	if(T1<34240) return(448);
	if(T1<34319) return(449);
	if(T1<34398) return(450);
	if(T1<34477) return(451);
	if(T1<34556) return(452);
	if(T1<34635) return(453);
	if(T1<34713) return(454);
	if(T1<34792) return(455);
	if(T1<34871) return(456);
	if(T1<34949) return(457);
	if(T1<35028) return(458);
	if(T1<35107) return(459);
	if(T1<35187) return(460);
	if(T1<35265) return(461);
	if(T1<35344) return(462);
	if(T1<35423) return(463);
	if(T1<35502) return(464);
	if(T1<35580) return(465);
	if(T1<35659) return(466);
	if(T1<35738) return(467);
	if(T1<35816) return(468);
	if(T1<35896) return(469);
	if(T1<35975) return(470);
	if(T1<36054) return(471);
	if(T1<36132) return(472);
	if(T1<36211) return(473);
	if(T1<36290) return(474);
	if(T1<36369) return(475);
	if(T1<36447) return(476);
	if(T1<36526) return(477);
	if(T1<36605) return(478);
	if(T1<36685) return(479);
	if(T1<36763) return(480);
	if(T1<36842) return(481);
	if(T1<36921) return(482);
	if(T1<37000) return(483);
	if(T1<37079) return(484);
	if(T1<37158) return(485);
	if(T1<37237) return(486);
	if(T1<37316) return(487);
	if(T1<37395) return(488);
	if(T1<37475) return(489);
	if(T1<37554) return(490);
	if(T1<37633) return(491);
	if(T1<37712) return(492);
	if(T1<37791) return(493);
	if(T1<37870) return(494);
	if(T1<37949) return(495);
	if(T1<38028) return(496);
	if(T1<38107) return(497);
	if(T1<38186) return(498);
	if(T1<38266) return(499);
	if(T1<38345) return(500);
	if(T1<38424) return(501);
	if(T1<38503) return(502);
	if(T1<38582) return(503);
	if(T1<38661) return(504);
	if(T1<38740) return(505);
	if(T1<38819) return(506);
	if(T1<38898) return(507);
	if(T1<38977) return(508);
	if(T1<39057) return(509);
	if(T1<39136) return(510);
	if(T1<39215) return(511);
	if(T1<39294) return(512);
	if(T1<39373) return(513);
	if(T1<39452) return(514);
	if(T1<39531) return(515);
	if(T1<39610) return(516);
	if(T1<39689) return(517);
	if(T1<39768) return(518);
	if(T1<39848) return(519);
	if(T1<39927) return(520);
	if(T1<40006) return(521);
	if(T1<40085) return(522);
	if(T1<40164) return(523);
	if(T1<40243) return(524);
	if(T1<40322) return(525);
	if(T1<40401) return(526);
	if(T1<40480) return(527);
	if(T1<40559) return(528);
	if(T1<40639) return(529);
	if(T1<40718) return(530);
	if(T1<40797) return(531);
	if(T1<40876) return(532);
	if(T1<40955) return(533);
	if(T1<41034) return(534);
	if(T1<41113) return(535);
	if(T1<41192) return(536);
	if(T1<41271) return(537);
	if(T1<41350) return(538);
	if(T1<41430) return(539);
	if(T1<41509) return(540);
	if(T1<41588) return(541);
	if(T1<41667) return(542);
	if(T1<41746) return(543);
	if(T1<41825) return(544);
	if(T1<41904) return(545);
	if(T1<41983) return(546);
	if(T1<42062) return(547);
	if(T1<42141) return(548);
	if(T1<42221) return(549);
	if(T1<42300) return(550);
	if(T1<42379) return(551);
	if(T1<42458) return(552);
	if(T1<42537) return(553);
	if(T1<42616) return(554);
	if(T1<42695) return(555);
	if(T1<42774) return(556);
	if(T1<42853) return(557);
	if(T1<42933) return(558);
	if(T1<43012) return(559);
	if(T1<43091) return(560);
	if(T1<43170) return(561);
	if(T1<43249) return(562);
	if(T1<43328) return(563);
	if(T1<43407) return(564);
	if(T1<43486) return(565);
	if(T1<43565) return(566);
	if(T1<43644) return(567);
	if(T1<43724) return(568);
	if(T1<43803) return(569);
	if(T1<43882) return(570);
	if(T1<43961) return(571);
	if(T1<44040) return(572);
	if(T1<44119) return(573);
	if(T1<44198) return(574);
	if(T1<44277) return(575);
	if(T1<44356) return(576);
	if(T1<44435) return(577);
	if(T1<44515) return(578);
	if(T1<44594) return(579);
	if(T1<44673) return(580);
	if(T1<44752) return(581);
	if(T1<44831) return(582);
	if(T1<44910) return(583);
	if(T1<44989) return(584);
	if(T1<45068) return(585);
	if(T1<45147) return(586);
	if(T1<45225) return(587);
	if(T1<45305) return(588);
	if(T1<45384) return(589);
	if(T1<45463) return(590);
	if(T1<45542) return(591);
	if(T1<45620) return(592);
	if(T1<45699) return(593);
	if(T1<45778) return(594);
	if(T1<45857) return(595);
	if(T1<45935) return(596);
	if(T1<46014) return(597);
	if(T1<46094) return(598);
	if(T1<46173) return(599);
	if(T1<46252) return(600);
	if(T1<46330) return(601);
	if(T1<46409) return(602);
	if(T1<46488) return(603);
	if(T1<46567) return(604);
	if(T1<46646) return(605);
	if(T1<46724) return(606);
	if(T1<46803) return(607);
	if(T1<46883) return(608);
	if(T1<46962) return(609);
	if(T1<47041) return(610);
	if(T1<47119) return(611);
	if(T1<47198) return(612);
	if(T1<47277) return(613);
	if(T1<47356) return(614);
	if(T1<47434) return(615);
	if(T1<47513) return(616);
	if(T1<47592) return(617);
	if(T1<47672) return(618);
	if(T1<47751) return(619);
	if(T1<47829) return(620);
	if(T1<47908) return(621);
	if(T1<47987) return(622);
	if(T1<48066) return(623);
	if(T1<48145) return(624);
	if(T1<48223) return(625);
	if(T1<48302) return(626);
	if(T1<48381) return(627);
	if(T1<48461) return(628);
	if(T1<48540) return(629);
	if(T1<48618) return(630);
	if(T1<48697) return(631);
	if(T1<48776) return(632);
	if(T1<48855) return(633);
	if(T1<48934) return(634);
	if(T1<49012) return(635);
	if(T1<49091) return(636);
	if(T1<49170) return(637);
	if(T1<49249) return(638);
	if(T1<49327) return(639);
	if(T1<49406) return(640);
	if(T1<49484) return(641);
	if(T1<49562) return(642);
	if(T1<49640) return(643);
	if(T1<49718) return(644);
	if(T1<49797) return(645);
	if(T1<49875) return(646);
	if(T1<49954) return(647);
	if(T1<50032) return(648);
	if(T1<50111) return(649);
	if(T1<50189) return(650);
	if(T1<50267) return(651);
	if(T1<50345) return(652);
	if(T1<50424) return(653);
	if(T1<50502) return(654);
	if(T1<50580) return(655);
	if(T1<50658) return(656);
	if(T1<50738) return(657);
	if(T1<50816) return(658);
	if(T1<50894) return(659);
	if(T1<50972) return(660);
	if(T1<51051) return(661);
	if(T1<51129) return(662);
	if(T1<51207) return(663);
	if(T1<51285) return(664);
	if(T1<51364) return(665);
	if(T1<51442) return(666);
	if(T1<51521) return(667);
	if(T1<51599) return(668);
	if(T1<51678) return(669);
	if(T1<51756) return(670);
	if(T1<51834) return(671);
	if(T1<51912) return(672);
	if(T1<51991) return(673);
	if(T1<52069) return(674);
	if(T1<52147) return(675);
	if(T1<52225) return(676);
	if(T1<52305) return(677);
	if(T1<52383) return(678);
	if(T1<52461) return(679);
	if(T1<52539) return(680);
	if(T1<52617) return(681);
	if(T1<52696) return(682);
	if(T1<52774) return(683);
	if(T1<52852) return(684);
	if(T1<52930) return(685);
	if(T1<53009) return(686);
	if(T1<53088) return(687);
	if(T1<53166) return(688);
	if(T1<53244) return(689);
	if(T1<53322) return(690);
	if(T1<53399) return(691);
	if(T1<53477) return(692);
	if(T1<53554) return(693);
	if(T1<53632) return(694);
	if(T1<53709) return(695);
	if(T1<53787) return(696);
	if(T1<53865) return(697);
	if(T1<53943) return(698);
	if(T1<54020) return(699);
	if(T1<54098) return(700);
	if(T1<54175) return(701);
	if(T1<54253) return(702);
	if(T1<54330) return(703);
	if(T1<54408) return(704);
	if(T1<54485) return(705);
	if(T1<54563) return(706);
	if(T1<54641) return(707);
	if(T1<54718) return(708);
	if(T1<54796) return(709);
	if(T1<54873) return(710);
	if(T1<54951) return(711);
	if(T1<55028) return(712);
	if(T1<55106) return(713);
	if(T1<55183) return(714);
	if(T1<55261) return(715);
	if(T1<55338) return(716);
	if(T1<55417) return(717);
	if(T1<55494) return(718);
	if(T1<55572) return(719);
	if(T1<55649) return(720);
	if(T1<55727) return(721);
	if(T1<55804) return(722);
	if(T1<55882) return(723);
	if(T1<55959) return(724);
	if(T1<56037) return(725);
	if(T1<56115) return(726);
	if(T1<56192) return(727);
	if(T1<56270) return(728);
	if(T1<56347) return(729);
	if(T1<56425) return(730);
	if(T1<56502) return(731);
	if(T1<56580) return(732);
	if(T1<56657) return(733);
	if(T1<56735) return(734);
	if(T1<56812) return(735);
	if(T1<56891) return(736);
	if(T1<56968) return(737);
	if(T1<57046) return(738);
	if(T1<57123) return(739);
	if(T1<57201) return(740);
	if(T1<57278) return(741);
	if(T1<57355) return(742);
	if(T1<57432) return(743);
	if(T1<57508) return(744);
	if(T1<57585) return(745);
	if(T1<57662) return(746);
	if(T1<57739) return(747);
	if(T1<57815) return(748);
	if(T1<57892) return(749);
	if(T1<57968) return(750);
	if(T1<58045) return(751);
	if(T1<58121) return(752);
	if(T1<58198) return(753);
	if(T1<58274) return(754);
	if(T1<58351) return(755);
	if(T1<58428) return(756);
	if(T1<58505) return(757);
	if(T1<58581) return(758);
	if(T1<58658) return(759);
	if(T1<58734) return(760);
	if(T1<58811) return(761);
	if(T1<58887) return(762);
	if(T1<58964) return(763);
	if(T1<59040) return(764);
	if(T1<59117) return(765);
	if(T1<59194) return(766);
	if(T1<59271) return(767);
	if(T1<59347) return(768);
	if(T1<59424) return(769);
	if(T1<59500) return(770);
	if(T1<59577) return(771);
	if(T1<59653) return(772);
	if(T1<59730) return(773);
	if(T1<59806) return(774);
	if(T1<59883) return(775);
	if(T1<59960) return(776);
	if(T1<60037) return(777);
	if(T1<60113) return(778);
	if(T1<60190) return(779);
	if(T1<60266) return(780);
	if(T1<60343) return(781);
	if(T1<60419) return(782);
	if(T1<60496) return(783);
	if(T1<60572) return(784);
	if(T1<60649) return(785);
	if(T1<60726) return(786);
	if(T1<60803) return(787);
	if(T1<60879) return(788);
	if(T1<60956) return(789);
	if(T1<61032) return(790);
	if(T1<61109) return(791);
	if(T1<61185) return(792);
	if(T1<61262) return(793);
	if(T1<61338) return(794);
	if(T1<61415) return(795);
	if(T1<61492) return(796);
	if(T1<61567) return(797);
	if(T1<61643) return(798);
	if(T1<61718) return(799);
	if(T1<61794) return(800);
	if(T1<61869) return(801);
	if(T1<61945) return(802);
	if(T1<62020) return(803);
	if(T1<62096) return(804);
	if(T1<62171) return(805);
	if(T1<62248) return(806);
	if(T1<62323) return(807);
	if(T1<62399) return(808);
	if(T1<62475) return(809);
	if(T1<62550) return(810);
	if(T1<62626) return(811);
	if(T1<62701) return(812);
	if(T1<62777) return(813);
	if(T1<62852) return(814);
	if(T1<62929) return(815);
	if(T1<63004) return(816);
	if(T1<63080) return(817);
	if(T1<63155) return(818);
	if(T1<63231) return(819);
	if(T1<63307) return(820);
	if(T1<63382) return(821);
	if(T1<63458) return(822);
	if(T1<63533) return(823);
	if(T1<63609) return(824);
	if(T1<63685) return(825);
	if(T1<63761) return(826);
	if(T1<63836) return(827);
	if(T1<63912) return(828);
	if(T1<63987) return(829);
	if(T1<64063) return(830);
	if(T1<64138) return(831);
	if(T1<64214) return(832);
	if(T1<64290) return(833);
	if(T1<64365) return(834);
	if(T1<64442) return(835);
	if(T1<64517) return(836);
	if(T1<64593) return(837);
	if(T1<64668) return(838);
	if(T1<64744) return(839);
	if(T1<64819) return(840);
	if(T1<64895) return(841);
	if(T1<64970) return(842);
	if(T1<65046) return(843);
	if(T1<65122) return(844);
	if(T1<65198) return(845);
	if(T1<65274) return(846);
	if(T1<65349) return(847);
	if(T1<65425) return(848);
	if(T1<65500) return(849);
	return(850);
}

void CCatPyroMasterDriver::Debug(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	AfxTrace(pText, pArgs);

	va_end(pArgs);
	}

// CAT 192-9001 PYROMETER DOCUMENTATION
/*
Communications Paramaters: All communications are at 9600 baud, 8 Data bits, No 
Parity, 1 Stop bits. (9600 8N1) All communications are 11 byte binary packets

Packet Description ( hexadecimal ): 02 DA CA CMD AA AA DD DD CRC CRC 03

02 = Start of packet. This indicates to the scanner that a packet has begun. The 
scanner will now wait for the other 10 bytes for a message. If the other 10 
bytes are not recieved within 3 seconds, the scanner will reset the serial 
receive buffer and await another 02.

DA = Destination address. Those scanners that are fully functional ( with 
thermocouple scanning capability ) are known as masters ( primary protection 
device ) and will always have an address of 4D ( ascii M ). This tells the 
scanner that this message is intened for it.

CA = Caller address. This signifies where the message is coming from. Hex 00 -
09 & 53 are reserved for future use. Hex 54 is used by PyroTerm terminal 
program. For custom applications, any other address up to FF can be used. The 
master, when replying, will put this address in the destination location and 
will put its own address in the caller address.

CMD = Command. The command set is: Hex.	Ascii	Description

		52	R	Read memory
		57	W	Write memory
		5A	Z	Reload EEProm into Ram
		58	X	Restore Defaults and Factory Calibration
		43	C	Scanner Reset
		42	B	Clear Max.
		41	A	Clear View Alarms

AA AA = Memory address. The Ram addresses are from 01 00 to 01 FF. The EEProm
addresses are from 00 00 to 00 FF. These are the only valid address ranges. 
Deviation from these ranges may have undesirable results.

DD DD = Data. Data sent to or returned from the scanner depending on the 
command. See the command descriptions further in this document

CRC CRC = CRC16 calculation. For verification of the packet. Mandatory to test 
the validity of the message. The CRC calculation starts at the DA and ends 
on the second CRC byte. See the CRC descrition further in this document.

03 = End of packet. Indicates to the scanner that this is a complete message 
packet to be processed.

Command Description ( Useage ):

R) The read command message would look similar to 02 DA CA 52 AA AA XX XX CRC CRC 
03 DA is the destination address of the unit to be read CA is the address of the 
calling application 52 is the read command AA AA is the address of the desired 
data XX XX is don't care. The Data space in this message is unimportant to the 
read operation CRC CRC is the CRC16 calculation

The return message will look like: 02 CA DA 52 AA AA DD DD CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. Notice 
that the XX XX is now DD DD. The scanner is returning the desired data.

W) The write command message would look similar to 02 DA CA 57 AA AA DD DD CRC 
CRC 03 DA is the destination address of the unit to be read CA is the address of 
the calling application 57 is the write command AA AA is the address of the 
desired data DD DD is the data to be written CRC CRC is the CRC16 calculation

The return message will look like: 02 DA CA 57 AA AA DD DD CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. The 
message returned signifies that DD DD was successfully written to AA AA.

Z) The refresh command message would look similar to 02 DA CA 5A XX XX XX XX CRC 
CRC 03 DA is the destination address of the unit to be read CA is the address of 
the calling application 5A is the refresh command XX XX is don't care XX XX is 
don't care. CRC CRC is the CRC16 calculation

The return message will look like: 02 DA CA 5A  XX XX XX XX CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. The 
message returned signifies that Z was executed

X) The restore command message would look similar to 02 DA CA 58 XX XX XX XX CRC 
CRC 03 DA is the destination address of the unit to be read CA is the address of 
the calling application 58 is the restore command XX XX is don't care XX XX is 
don't care. CRC CRC is the CRC16 calculation

The return message will look like: 02 DA CA 58  XX XX XX XX CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. The 
message returned signifies that X was executed

C) The reset command message would look similar to 02 DA CA 43 XX XX XX XX CRC 
CRC 03 DA is the destination address of the unit to be read CA is the address of 
the calling application 43 is the reset command XX XX is don't care XX XX is 
don't care. CRC CRC is the CRC16 calculation

The return message will look like: 02 DA CA 43  XX XX XX XX CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. The 
message returned signifies that C was executed

B) The clear max command message would look similar to 02 DA CA 42 XX XX XX XX 
CRC CRC 03 DA is the destination address of the unit to be read CA is the 
address of the calling application 42 is the clear max command XX XX is don't 
care XX XX is don't care. CRC CRC is the CRC16 calculation

The return message will look like: 02 DA CA 42  XX XX XX XX CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. The 
message returned signifies that B was executed

A) The clear alarm command message would look similar to 02 DA CA 41 XX XX XX XX 
CRC CRC 03 DA is the destination address of the unit to be read CA is the 
address of the calling application 41 is the clear alarm command XX XX is don't 
care XX XX is don't care. CRC CRC is the CRC16 calculation

The return message will look like: 02 DA CA 41  XX XX XX XX CRC CRC 03 Notice 
that the DA & CA are reversed. The scanner is now replying to the sender. The 
message returned signifies that A was executed

Communications Error: A Communications error will cause the scanner to return a 
single 15 ( NAK ) character. The scanner will return a NAK on the following: 
Invalid CRC Invalid Command

Cyclic Redundancy Codes ( CRC ): As defined in Ross Williams' "A PAINLESS GUIDE 
TO CRC ERROR DETECTION  ALGORITHMS", this is the standardized CRC-16 algorithm.

Given a string x = "123456789" the CRC and Check are as follows:

	Name   : "CRC-16"
	Width  : 16
	Poly   : 8005
	Init   : 0000
	RefIn  : True 
	RefOut : True
	XorOut : 0000	
	Check  : BB3D

	
Example of a correct packet to read from EEProm:

	[02][4d][41][52][00][af][00][00][1b][f8][03]

---------------------------------------------------------------------------------
CAT PYROMETER 192-9001 RAM DATA MAP - ONLY UTILIZE LOW ADDRESSES
	
0X15E	CYLAVGL
0X15F	CYLAVGH
0X1AA	AMBIENTL
0X1AB	AMBIENTH
0X1AC	CHAN1L
0X1AD	CHAN1H
0X1AE	CHAN2L
0X1AF	CHAN2H
0X1B0	CHAN3L
0X1B1	CHAN3H
0X1B2	CHAN4L
0X1B3	CHAN4H
0X1B4	CHAN5L
0X1B5	CHAN5H
0X1B6	CHAN6L
0X1B7	CHAN6H
0X1B8	CHAN7L
0X1B9	CHAN7H
0X1BA	CHAN8L
0X1BB	CHAN8H
0X1BC	CHAN9L
0X1BD	CHAN9H
0X1BE	CHAN10L
0X1BF	CHAN10H
0X1C0	CHAN11L
0X1C1	CHAN11H
0X1C2	CHAN12L
0X1C3	CHAN12H
0X1C4	CHAN13L
0X1C5	CHAN13H
0X1C6	CHAN14L
0X1C7	CHAN14H
0X1C8	CHAN15L
0X1C9	CHAN15H
0X1CA	CHAN16L
0X1CB	CHAN16H
0X1CC	CHAN17L
0X1CD	CHAN17H
0X1CE	CHAN18L
0X1CF	CHAN18H
0X1D0	CHAN19L
0X1D1	CHAN19H
0X1D2	CHAN20L
0X1D3	CHAN20H
0X1D4	CHAN21L
0X1D5	CHAN21H
0X1D6	CHAN22L
0X1D7	CHAN22H
0X1D8	CHAN23L
0X1D9	CHAN23H
0X1DA	CHAN24L
0X1DB	CHAN24H
*/

// End of File

