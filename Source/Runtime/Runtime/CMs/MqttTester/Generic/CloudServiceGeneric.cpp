
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientGeneric.hpp"

#include "MqttClientOptionsGeneric.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Service
//

// Instantiator

IService * Create_CloudServiceGeneric(void)
{
	return New CCloudServiceGeneric;
	}

// Constructor

CCloudServiceGeneric::CCloudServiceGeneric(void)
{
	m_Name = "GENERIC";
	}

// Initialization

void CCloudServiceGeneric::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceGeneric", pData);

	CCloudServiceCrimson::Load(pData);

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		if( m_pSet[n]->m_Array == 0 ) {

			m_pSet[n]->AddRewrite('[', '-');

			m_pSet[n]->AddRewrite(']', 0);
			}

		if( m_pSet[n]->m_Tree == 0 ) {

			m_pSet[n]->AddRewrite('.', '-');
			}

		m_pSet[n]->m_Array |= 2;
		}

	CMqttClientOptionsGeneric *pOpts = New CMqttClientOptionsGeneric;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory();

	m_pClient = New CMqttClientGeneric(this, *pOpts);

	m_pOpts->m_DiskPath = "C:\\MQTT\\GENERIC";
	}

// Service ID

UINT CCloudServiceGeneric::GetID(void)
{
	return 13;
	}

// End of File
