
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CTestDriverOptions, CUIItem);

// Constructor

CTestDriverOptions::CTestDriverOptions(void)
{
	m_Delay = 0;
	}

// Download Support

BOOL CTestDriverOptions::MakeInitData(CInitData &Init)
{
	Init.AddLong(m_Delay);

	return TRUE;
	}

// Meta Data Creation

void CTestDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Delay);
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CTestDeviceOptions, CUIItem);

// Constructor

CTestDeviceOptions::CTestDeviceOptions(void)
{
	m_Drop = 0;
	}

// Download Support

BOOL CTestDeviceOptions::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_Drop));

	return FALSE;
	}

// Meta Data Creation

void CTestDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Driver
//

// Instantiator

ICommsDriver *	Create_TestDriver(void)
{
	return New CTestDriver;
	}

// Constructor

CTestDriver::CTestDriver(void)
{
	}

// IBase Methods

void CTestDriver::Release(void)
{
	delete this;
	}

// Driver Data

WORD CTestDriver::GetID(void)
{
	return 0x1234;
	}

UINT CTestDriver::GetType(void)
{
	return driverMaster;
	}

CString	CTestDriver::GetString(UINT ID)
{
	switch( ID ) {

		case stringManufacturer:	return "Test Drivers";
		case stringDriverName:		return "Mike's FX Series";
		case stringVersion:		return "1.0";
		case stringShortName:		return "Mike's Mits FX";

		}

	return "";
	}

// Binding Control

UINT CTestDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTestDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CTestDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CTestDriverOptions);
	}

// Configuration

CLASS CTestDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTestDeviceOptions);
	}

// Address Management

BOOL CTestDriver::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	if( Text[0] == 'D' || Text[0] == 'd' ) {

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = tatoi(PCTXT(Text)+1);
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return TRUE;
		}

	if( Text[0] == 'M' || Text[0] == 'm' ) {

		Addr.a.m_Table  = 2;
		Addr.a.m_Offset = tatoi(PCTXT(Text)+1);
		Addr.a.m_Type   = addrBitAsWord;
		Addr.a.m_Extra  = 0;

		return TRUE;
		}

	if( Text[0] == 'X' || Text[0] == 'x' ) {

		Addr.a.m_Table  = 3;
		Addr.a.m_Offset = tatoi(PCTXT(Text)+1);
		Addr.a.m_Type   = addrBitAsBit;
		Addr.a.m_Extra  = 0;

		return TRUE;
		}

	if( Text[0] == 'Y' || Text[0] == 'y' ) {

		Addr.a.m_Table  = 4;
		Addr.a.m_Offset = tatoi(PCTXT(Text)+1);
		Addr.a.m_Type   = addrBitAsBit;
		Addr.a.m_Extra  = 0;

		return TRUE;
		}

	Error.Set( "The PLC register is not valid.",
		   0
		   );

	return FALSE;
	}

BOOL CTestDriver::ExpandAddress(CString &Text, CAddress const &Addr)
{
	if( Addr.a.m_Table == 1 ) {

		Text.Printf("D%u", Addr.a.m_Offset);

		return TRUE;
		}

	if( Addr.a.m_Table == 2 ) {

		Text.Printf("M%4.4u", Addr.a.m_Offset);

		return TRUE;
		}

	if( Addr.a.m_Table == 3 ) {

		Text.Printf("X%2.2X", Addr.a.m_Offset);

		return TRUE;
		}

	if( Addr.a.m_Table == 4 ) {

		Text.Printf("Y%2.2X", Addr.a.m_Offset);

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestDriver::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	UINT uCode = CWnd::FromHandle(hWnd).YesNo("Return an address?");

	if( uCode == IDYES ) {

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = 100;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;
		}
	else {
		Addr.a.m_Table  = 0;
		Addr.a.m_Offset = 0;
		Addr.a.m_Type   = 0;
		Addr.a.m_Extra  = 0;
		}

	return TRUE;
	}

BOOL CTestDriver::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		switch( uItem ) {

			case 0:
				Data.m_Name           = "D Registers";
				Data.m_Addr.a.m_Type  = addrWordAsWord;
				Data.m_Addr.a.m_Table = 1;
				Data.m_fPart          = TRUE;
				return TRUE;

			case 1:
				Data.m_Name           = "M Registers";
				Data.m_Addr.a.m_Type  = addrBitAsWord;
				Data.m_Addr.a.m_Table = 2;
				Data.m_fPart          = TRUE;
				return TRUE;

			case 2:
				Data.m_Name           = "X Registers";
				Data.m_Addr.a.m_Type  = addrBitAsBit;
				Data.m_Addr.a.m_Table = 3;
				Data.m_fPart          = TRUE;
				return TRUE;

			case 3:
				Data.m_Name           = "Y Registers";
				Data.m_Addr.a.m_Type  = addrBitAsBit;
				Data.m_Addr.a.m_Table = 4;
				Data.m_fPart          = TRUE;
				return TRUE;
			}
		}

	return FALSE;
	}

// End of File
