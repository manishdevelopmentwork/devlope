
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LEGACY_HPP
	
#define	INCLUDE_LEGACY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyFigure;
class CPrimLegacyShadow;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Figure
//

class CPrimLegacyFigure : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyFigure(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimPen   * m_pEdge;
		CPrimBrush * m_pBack;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void LoadHeadPages(CUIPageList *pList);
		void LoadTailPages(CUIPageList *pList);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Shadow
//

class CPrimLegacyShadow : public CPrimLegacyFigure
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyShadow(void);
		
		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Style;

	protected:
		// Data Members
		UINT m_Border;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void FindPoints(P2 *t, P2 *b, R2 &r);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyScale;
class CPrimLegacyVertScale;
class CPrimLegacyHorzScale;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Scale
//

class CPrimLegacyScale : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyScale(void);
		
		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		
		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimColor   * m_pLine;
		CPrimBrush   * m_pBack;
		UINT	       m_Style;
		UINT	       m_Orient;
		UINT	       m_Major;
		UINT	       m_Minor;
		UINT	       m_Interval;
		CCodedItem   * m_pLimitMin;
		CCodedItem   * m_pLimitMax;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementaiton
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Vertical Scale
//

class CPrimLegacyVertScale : public CPrimLegacyScale
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyVertScale(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Simple Horizontal Scale
//

class CPrimLegacyHorzScale : public CPrimLegacyScale
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyHorzScale(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyCFImage;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy CF Image
//

class CPrimLegacyCfImage : public CPrimLegacyFigure
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyCfImage(void);
		
		// Destructor
		~CPrimLegacyCfImage(void);
		
		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		BOOL IsSupported(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Data Members
		CCodedItem * m_pValue;
		CCodedItem * m_pShow;

	protected:
		// Data Members
		int   m_cx;
		int   m_cy;
		int   m_s;
		PBYTE m_pData;
		PBYTE m_pFrom;
		int   m_rop;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL LoadPreview(void);
		void FreePreview(void);
		BOOL LoadColorRGB888(PBYTE pData);
		void DrawNotSupported(IGDI * pGDI);
		void ShowMessage(IGDI *pGDI, PCTXT pMsg);
	};

// End of File

#endif
