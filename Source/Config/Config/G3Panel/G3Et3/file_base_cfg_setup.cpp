
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File Data - "base0/cfg/setup"
//

// Constructor

CFileDataBaseCfgSetup::CFileDataBaseCfgSetup(void) : CFileData("/base0/cfg/setup")
{	
	m_pData	= m_Data;

	m_uSize = elements(m_Data);

	for( UINT n = 0; n < elements(m_Timebases); n ++ ) {

		CTimebase &Time = m_Timebases[n];

		Time.SetBase(0x0020 + n * sizeof(DWORD));

		Time.SetFile(this);
		}

	for( UINT n = 0; n < elements(m_AIs); n ++ ) {

		CAnalogInput &AI = m_AIs[n];

		AI.SetBase(0x0100 + n);

		AI.SetFile(this);
		}

	for( UINT n = 0; n < elements(m_AOs); n ++ ) {

		CAnalogOutput &AO = m_AOs[n];

		AO.SetBase(0x0120 + n);

		AO.SetFile(this);
		}

	for( UINT n = 0; n < elements(m_DIs); n ++ ) {

		CDiscreteInput &DI = m_DIs[n];

		DI.SetBase(0x0140 + n);

		DI.SetFile(this);
		}

	for( UINT n = 0; n < elements(m_DOs); n ++ ) {

		CDiscreteOutput &DO = m_DOs[n];

		DO.SetBase(0x0160 + n);

		DO.SetFile(this);
		}
	}

// Development

void CFileDataBaseCfgSetup::Dump(void)
{
	AfxTrace(L"===============\n");

	AfxTrace(L"Properties for file %s\n", CString(GetFile()));

	AfxTrace(L" station number\t\t%d\n",		GetStationNumber());
	AfxTrace(L" WD op enable\t\t%4.4X\n",		GetWatchdogOutputEnables());
	AfxTrace(L" op io timeout\t\t%4.4X\n",		GetOutputIOTimeout());
	AfxTrace(L" io actions flags\t%d\n",		GetIOTimeoutActions());
	AfxTrace(L" modbus io force\t%d\n",		GetModbusDiscreteIOForce());
	AfxTrace(L" analog ip filter\t%2.2X\n",		GetAnalogInputFiltering());
	AfxTrace(L" temp reporting\t\t%d\n",		GetTemperatureReporting());
	AfxTrace(L" base nw jumper\t%d\n",		GetNetworkConfigJumper());
	AfxTrace(L" base srce/sink jumper\t%d\n",	GetBaseSourceSinkJumper());

	AfxTrace(L" discrete ip filter\t%8.8X\n",	GetDiscreteInputFiltering());

	if( TRUE ) {

		PCBYTE pJumper = GetModuleSourceSinkJumper();

		AfxTrace( L" module srce/sink jumper\t%2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n", 
			  pJumper[0],
			  pJumper[1],
			  pJumper[2],
			  pJumper[3],
			  pJumper[4],
			  pJumper[5],
			  pJumper[6],
			  pJumper[7]
			 );
		}

	AfxTrace(L" tpo interval\t\t\t%d\n",		GetTPOInterval());
	AfxTrace(L" tpo period\t\t\t%d\n",		GetTPOPeriod());
	AfxTrace(L" tpo minimum\t\t%d\n",		GetTPOMinimum());
	AfxTrace(L" HB register\t\t\t%d\n",		GetHBDiscreteRegister());
	AfxTrace(L" HB boot timeout\t\t%d\n",		GetHBBootTimeout());
	AfxTrace(L" HB timeout\t\t%d\n",		GetHBTimeout());

	if( TRUE ) {

		AfxTrace(L" timebases..\n");		

		for( UINT n = 0; n < elements(m_Timebases); n ++ ) {
		
			CFileDataBaseCfgSetup::CTimebase &Time = m_Timebases[n];

			if( n && !(n % 8) ) {
				
				AfxTrace(L"\n");
				}
			
			AfxTrace(L" %u", Time.GetTime());
			}

		AfxTrace(L"\n");
		}

	if( TRUE ) {

		AfxTrace(L" discrete input..\n");		

		for( UINT n = 0; n < elements(m_DIs); n ++ ) {
		
			CFileDataBaseCfgSetup::CDiscreteInput &DI = m_DIs[n];

			if( n && !(n % 8) ) {
				
				AfxTrace(L"\n");
				}
			
			AfxTrace(L" %2.2X", DI.GetConfig());
			}

		AfxTrace(L"\n");
		}

	if( TRUE ) {

		AfxTrace(L" discrete output..\n");

		for( UINT n = 0; n < elements(m_DOs); n ++ ) {
		
			CFileDataBaseCfgSetup::CDiscreteOutput &DO = m_DOs[n];

			if( n && !(n % 8) ) {
				
				AfxTrace(L"\n");
				}
			
			AfxTrace(L" %2.2X", DO.GetConfig());
			}

		AfxTrace(L"\n");
		}

	if( TRUE ) {

		AfxTrace(L" analog input range..\n");		

		for( UINT n = 0; n < elements(m_AIs); n ++ ) {
		
			CFileDataBaseCfgSetup::CAnalogInput &AI = m_AIs[n];

			if( n && !(n % 8) ) {
				
				AfxTrace(L"\n");
				}
			
			AfxTrace(L" %2.2X", AI.GetRange());
			}

		AfxTrace(L"\n");
		}

	AfxTrace(L" station name\t%s\n",	GetStationName());

	AfxTrace(L" di count\t\t%d\n", GetDICount());
	AfxTrace(L" do count\t\t%d\n", GetDOCount());
	AfxTrace(L" ai count\t\t%d\n", GetAICount());
	AfxTrace(L" ao count\t\t%d\n", GetAOCount());

	AfxTrace(L"===============\n");
	}

// Attributes

UINT CFileDataBaseCfgSetup::GetStationNumber(void)
{
	UINT uPtr = 0x0010;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetWatchdogOutputEnables(void)
{
	UINT uPtr = 0x0012;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetOutputIOTimeout(void)
{
	UINT uPtr = 0x0014;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetIOTimeoutActions(void)
{
	UINT uPtr = 0x0016;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetModbusDiscreteIOForce(void)
{
	UINT uPtr = 0x0017;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetAnalogInputFiltering(void)
{
	UINT uPtr = 0x0018;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetTemperatureReporting(void)
{
	UINT uPtr = 0x0019;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetNetworkConfigJumper(void)
{
	UINT uPtr = 0x001A;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetBaseSourceSinkJumper(void)
{
	UINT uPtr = 0x001B;

	return GetByte(uPtr);
	}

PCBYTE CFileDataBaseCfgSetup::GetTimerCounterTimebases(void)
{
	UINT uPtr = 0x0020;

	return GetData(uPtr, 128);
	}

UINT CFileDataBaseCfgSetup::GetDiscreteInputFiltering(void)
{
	UINT uPtr = 0x00A0;

	return GetLong(uPtr);
	}

PCBYTE CFileDataBaseCfgSetup::GetModuleSourceSinkJumper(void)
{
	UINT uPtr = 0x00A4;

	return GetData(uPtr, 8);
	}

UINT CFileDataBaseCfgSetup::GetTPOInterval(void)
{
	UINT uPtr = 0x00AC;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetTPOPeriod(void)
{
	UINT uPtr = 0x00AE;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetTPOMinimum(void)
{
	UINT uPtr = 0x00AF;

	return GetByte(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetHBDiscreteRegister(void)
{
	UINT uPtr = 0x00B0;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetHBBootTimeout(void)
{
	UINT uPtr = 0x00B2;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetHBTimeout(void)
{
	UINT uPtr = 0x00B4;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetHBManualResetRegister(void)
{
	UINT uPtr = 0x00B6;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetHBFeatureEnables(void)
{
	UINT uPtr = 0x00B8;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetCounterResetEnables(void)
{
	UINT uPtr = 0x00C0;

	return GetLong(uPtr);
	}

CString CFileDataBaseCfgSetup::GetStationName(void)
{	
	UINT uPtr = 0x0180;

	PCSTR pTagName = PCSTR(GetData(uPtr, 64));

	return CString(PCSTR(pTagName));
	}

UINT CFileDataBaseCfgSetup::GetDICount(void)
{
	UINT uPtr = 0x01C0;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetDOCount(void)
{
	UINT uPtr = 0x01C2;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetAICount(void)
{
	UINT uPtr = 0x01C4;

	return GetWord(uPtr);
	}

UINT CFileDataBaseCfgSetup::GetAOCount(void)
{
	UINT uPtr = 0x01C6;

	return GetWord(uPtr);
	}

// Operations

void CFileDataBaseCfgSetup::SetStationNumber(UINT uStation)
{	
	UINT uPtr = 0x0010;

	PutWord(uPtr, WORD(uStation));
	}

void CFileDataBaseCfgSetup::SetWatchdogOutputEnables(UINT uEnables)
{
	UINT uPtr = 0x0012;

	PutWord(uPtr, WORD(uEnables));
	}

void CFileDataBaseCfgSetup::SetOutputIOTimeout(UINT uEnables)
{
	UINT uPtr = 0x0014;

	PutWord(uPtr, WORD(uEnables));
	}

void CFileDataBaseCfgSetup::SetIOTimeoutActions(UINT uFlags)
{
	UINT uPtr = 0x0016;

	PutByte(uPtr, BYTE(uFlags));
	}

void CFileDataBaseCfgSetup::SetAnalogInputFiltering(UINT uData)
{
	UINT uPtr = 0x0018;

	PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgSetup::SetTemperatureReporting(UINT uData)
{
	UINT uPtr = 0x0019;

	PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgSetup::SetDiscreteInputFiltering(UINT uData)
{
	UINT uPtr = 0x00A0;

	PutLong(uPtr, DWORD(uData));
	}

void CFileDataBaseCfgSetup::SetModuleSourceSinkJumper(PCBYTE pData)
{
	UINT uPtr = 0x00A4;

	PutData(uPtr, pData, 8);
	}

void CFileDataBaseCfgSetup::SetTPOInterval(UINT uData)
{
	UINT uPtr = 0x00AC;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgSetup::SetTPOPeriod(UINT uData)
{
	UINT uPtr = 0x00AE;

	PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgSetup::SetTPOMinimum(UINT uData)
{
	UINT uPtr = 0x00AF;

	PutByte(uPtr, BYTE(uData));
	}

void CFileDataBaseCfgSetup::SetHBDiscreteRegister(UINT uRegister)
{
	UINT uPtr = 0x00B0;

	PutWord(uPtr, WORD(uRegister));
	}

void CFileDataBaseCfgSetup::SetHBBootTimeout(UINT uData)
{
	UINT uPtr = 0x00B2;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgSetup::SetHBTimeout(UINT uData)
{
	UINT uPtr = 0x00B4;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgSetup::SetHBManualResetRegister(UINT uData)
{
	UINT uPtr = 0x00B6;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgSetup::SetHBFeatureEnables(UINT uData)
{
	UINT uPtr = 0x00B8;

	PutWord(uPtr, WORD(uData));
	}

void CFileDataBaseCfgSetup::SetCounterResetEnables(UINT uData)
{
	UINT uPtr = 0x00C0;

	PutLong(uPtr, DWORD(uData));
	}

void CFileDataBaseCfgSetup::SetStationName(CString Name)
{	
	CAnsiString Work(Name);

	PCBYTE pWork = PCBYTE(PCSTR(Work));

	UINT   uSize = Work.GetLength();

	//
	BYTE bData[64];

	memset(bData, 0, sizeof(bData));

	memcpy(bData, pWork, uSize);

	UINT uPtr = 0x0180;

	PutData(uPtr, bData, sizeof(bData));
	}

// Timebases

// Initialization

void CFileDataBaseCfgSetup::CTimebase::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

void CFileDataBaseCfgSetup::CTimebase::SetFile(IFileData *pData)
{
	m_pData = pData;
	}
			
// Attributes

DWORD CFileDataBaseCfgSetup::CTimebase::GetTime(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetLong(uPtr);
	}

// Operations

void CFileDataBaseCfgSetup::CTimebase::SetTime(UINT uData)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutLong(uPtr, DWORD(uData));
	}

// Analog Inputs

// Initialization

void CFileDataBaseCfgSetup::CAnalogInput::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

void CFileDataBaseCfgSetup::CAnalogInput::SetFile(IFileData *pData)
{
	m_pData = pData;
	}
			
// Attributes

BYTE CFileDataBaseCfgSetup::CAnalogInput::GetRange(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgSetup::CAnalogInput::SetRange(UINT uData)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

// Analog Outputs

// Initialization

void CFileDataBaseCfgSetup::CAnalogOutput::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

void CFileDataBaseCfgSetup::CAnalogOutput::SetFile(IFileData *pData)
{
	m_pData = pData;
	}
			
// Attributes

BYTE CFileDataBaseCfgSetup::CAnalogOutput::GetRange(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgSetup::CAnalogOutput::SetRange(UINT uData)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

// Discrete Inputs

// Initialization

void CFileDataBaseCfgSetup::CDiscreteInput::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

void CFileDataBaseCfgSetup::CDiscreteInput::SetFile(IFileData *pData)
{
	m_pData = pData;
	}

// Attributes

UINT CFileDataBaseCfgSetup::CDiscreteInput::GetConfig(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgSetup::CDiscreteInput::SetConfig(UINT uData)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

// Discrete Outputs

// Initialization

void CFileDataBaseCfgSetup::CDiscreteOutput::SetBase(UINT uBase)
{
	m_uBase = uBase;
	}

void CFileDataBaseCfgSetup::CDiscreteOutput::SetFile(IFileData *pData)
{
	m_pData = pData;
	}

// Attributes

UINT CFileDataBaseCfgSetup::CDiscreteOutput::GetConfig(void)
{
	UINT uPtr = m_uBase + 0x0000;

	return m_pData->GetByte(uPtr);
	}

// Operations

void CFileDataBaseCfgSetup::CDiscreteOutput::SetConfig(UINT uData)
{
	UINT uPtr = m_uBase + 0x0000;

	m_pData->PutByte(uPtr, BYTE(uData));
	}

// End of File
