
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Pen
//

// Dynamic Class

AfxImplementDynamicClass(CPrimPen, CCodedHost);

// Constructor

CPrimPen::CPrimPen(void)
{
	m_Width  = 1;

	m_Corner = 0;

	m_pColor = New CPrimColor;
	}

// UI Managament

void CPrimPen::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			if( GetParent()->IsKindOf(AfxRuntimeClass(CPrimLine)) ) {

				LimitEnum(pHost, L"Width", m_Width, 0xFE);
				}

			DoEnables(pHost);
			}

		if( Tag == "Width" ) {

			DoEnables(pHost);
			}

		if( !Tag.IsEmpty() ) {

			CPrim *pPrim = (CPrim *) GetParent();

			pPrim->OnUIChange(pHost, pItem, L"PrimProp");
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimPen::IsNull(void) const
{
	return m_Width == 0;
	}

COLOR CPrimPen::GetColor(void) const
{
	return m_pColor->GetColor();
	}

int CPrimPen::GetAdjust(void) const
{
	return m_Width ? int((m_Width - 1) / 2) : 0;
	}

int CPrimPen::GetWidth(void) const
{
	return int(m_Width);
	}

// Operations

void CPrimPen::Set(COLOR Color)
{
	m_pColor->Set(Color);
	}

BOOL CPrimPen::AdjustRect(R2 &Rect)
{
	if( m_Width > 1 ) {

		int n = GetAdjust();

		DeflateRect(Rect, n, n);

		return TRUE;
		}

	return FALSE;
	}

void CPrimPen::Configure(IGDI *pGDI)
{
	pGDI->SetPenStyle(penFore);

	pGDI->SetPenCaps (capsBoth);

	pGDI->SetPenFore (m_pColor->GetColor());

	pGDI->SetPenWidth(m_Width);
	}

// Drawing

void CPrimPen::DrawRect(IGDI *pGDI, R2 Rect, UINT uMode)
{
	if( m_Width > 0 ) {

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_pColor->GetColor());

		pGDI->SetPenWidth((uMode == drawThumb) ? 1 : m_Width);

		pGDI->SetPenCaps(m_Corner ? capsNone : capsStart);

		pGDI->DrawRect(PassRect(Rect));
		}
	}

void CPrimPen::DrawEllipse(IGDI *pGDI, R2 Rect, UINT uType, UINT uMode)
{
	if( m_Width > 0 ) {

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_pColor->GetColor());

		pGDI->SetPenWidth((uMode == drawThumb) ? 1 : m_Width);

		pGDI->SetPenCaps (capsStart);

		pGDI->DrawEllipse(PassRect(Rect), uType);
		}
	}

void CPrimPen::DrawWedge(IGDI *pGDI, R2 Rect, UINT uType, UINT uMode)
{
	if( m_Width > 0 ) {

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_pColor->GetColor());

		pGDI->SetPenWidth((uMode == drawThumb) ? 1 : m_Width);

		pGDI->SetPenCaps (capsStart);

		pGDI->DrawWedge(PassRect(Rect), uType);
		}
	}

void CPrimPen::DrawPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound, UINT uMode)
{
	// OPTIM -- If this edge is part of a polygon that is going
	// to be filled with a horizontal shader, we don't get 100%
	// pixel coherence as the edge will be drawn vertically. To
	// fix it, we're going to have to have an argument to draw
	// polygon to indicate which way to draw it, and we're going
	// to have to have our owner tell us about what kind of fill
	// is being used. A better fix would be to mod the gdi code
	// to achieve coherence, but that's hard!!!

	if( m_Width > 0 ) {

		AfxAssert(!m_Corner);

		pGDI->SetPenStyle(penFore);

		pGDI->SetPenFore (m_pColor->GetColor());

		pGDI->SetPenWidth((uMode == drawThumb) ? 1 : m_Width);

		pGDI->SetPenCaps (capsStart);

		pGDI->DrawPolygon(pList, uCount, dwRound);
		}
	}

// Persistance

void CPrimPen::Init(void)
{
	CUIItem::Init();

	m_pColor->Set(GetRGB(31,31,31));
	}

// Download Support

BOOL CPrimPen::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Width));

	Init.AddByte(BYTE(m_Corner));

	Init.AddItem(itemSimple, m_pColor);

	return TRUE;
	}

// Meta Data

void CPrimPen::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Width);
	Meta_AddInteger(Corner);
	Meta_AddObject (Color);

	Meta_SetName((IDS_PEN));
	}

// Implementation

void CPrimPen::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Color",  m_Width > 0);

	pHost->EnableUI(this, "Corner", m_Width > 1);
	}

// End of File
