
#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Modem Manager
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Short Message Object
//

class CShortMessage
{
public:
	// Constructors
	CShortMessage(void);

	// Destructor
	~CShortMessage(void);

	// Attributes
	UINT  GetIndex(void) const;
	PCTXT GetNumber(void) const;
	PCTXT GetMessage(void) const;

	// Operations
	void SetIndex(UINT uIndex);
	void SetNumber(PCTXT pNumber);
	void SetMessage(PCTXT pMessage);
	void AppendSpace(void);
	void ParsePDU(PCBYTE pData);
	UINT BuildPDU(PBYTE pData);

	// Debugging
	void Dump(void);

protected:
	// Lookup Table
	static BYTE const m_bMap[];

	// Data Members
	UINT m_uIndex;
	PTXT m_pNumber;
	PTXT m_pMessage;
	bool m_fUnicode;

	// PDU Parsing
	void ParseServiceCenter(PCBYTE &pData);
	void ParseMessageType(PCBYTE &pData);
	void ParseNumber(PCBYTE &pData);
	void ParseEncoding(PCBYTE &pData);
	void ParseTimeStamp(PCBYTE &pData);
	void ParseMessage(PCBYTE &pData);

	// PDU Building
	void BuildServiceCenter(PBYTE &pData);
	void BuildMessageType(PBYTE &pData);
	void BuildNumber(PBYTE &pData);
	void BuildEncoding(PBYTE &pData);
	void BuildTimeStamp(PBYTE &pData);
	void BuildMessage(PBYTE &pData);

	// Implementation
	BYTE Encode(char cData);
	void FreeData(void);
	BYTE GetUnicodeCount(PCTXT p);
	bool HasUnicode(PCTXT p);
};

// End of File
