
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Object Database Identifier
//

class CUITextObjectHandle : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextObjectHandle(void);

	protected:
		// Data
		CControlObject *m_pObject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Object Database Identifier
//

// Dynamic Class

AfxImplementDynamicClass(CUITextObjectHandle, CUITextInteger);

// Constructor

CUITextObjectHandle::CUITextObjectHandle(void)
{
	m_uFlags &= ~textEdit;

	m_uFlags |= textRead;	
	}

// Overridables

void CUITextObjectHandle::OnBind(void)
{
	CUITextInteger::OnBind();

	m_pObject = (CControlObject *) m_pItem;
	}

void CUITextObjectHandle::OnRebind(void)
{
	CUITextInteger::OnRebind();

	m_pObject = (CControlObject *) m_pItem;
	}

CString CUITextObjectHandle::OnGetAsText(void)
{
	return CPrintf(L"%d", m_pObject->GetHandle());
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Object Database Identifier
//

class CUITextObjectName : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextObjectName(void);

	protected:
		// Data
		CControlObject *m_pObject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Object Database Identifier
//

// Dynamic Class

AfxImplementDynamicClass(CUITextObjectName, CUITextString);

// Constructor

CUITextObjectName::CUITextObjectName(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

void CUITextObjectName::OnBind(void)
{
	CUITextString::OnBind();

	m_pObject = (CControlObject *) m_pItem;
	}

void CUITextObjectName::OnRebind(void)
{
	CUITextString::OnRebind();

	m_pObject = (CControlObject *) m_pItem;
	}

CString CUITextObjectName::OnGetAsText(void)
{
	return m_pObject->m_Name;
	}

// End of File

