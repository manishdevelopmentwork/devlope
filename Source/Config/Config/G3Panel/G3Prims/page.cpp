
#include "intern.hpp"

#include "PrimRubyTextBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Page Folder
//

// Dynamic Class

AfxImplementDynamicClass(CDispFolder, CFolderItem);

// Constructor

CDispFolder::CDispFolder(void)
{
}

// Meta Data

void CDispFolder::AddMetaData(void)
{
	CFolderItem::AddMetaData();

	Meta_SetName(IDS_DISPLAY_FOLDER);
}

//////////////////////////////////////////////////////////////////////////
//
// Page Item
//

// Dynamic Class

AfxImplementDynamicClass(CDispPage, CMetaItem);

// Conversion Options

static bool keepAspect = true;

// Constructor

CDispPage::CDispPage(void)
{
	m_Handle  = HANDLE_NONE;

	m_pList   = New CPrimList;

	m_pEvents = New CEventMap;

	m_pProps  = New CDispPageProps;

	m_Size    = CSize(640, 480);

	m_Private = 0;
}

// UI Creation

CViewWnd * CDispPage::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		CLASS Class = AfxNamedClass(L"CPageEditorWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	return NULL;
}

// Operations

void CDispPage::Validate(BOOL fExpand)
{
	m_pList->Validate(fExpand);

	m_pEvents->Validate(fExpand);
}

void CDispPage::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	m_pList->TagCheck(Done, Tags);

	m_pEvents->TagCheck(Done, Tags);
}

void CDispPage::GetRefs(CPrimRefList &Refs)
{
	m_pList->GetRefs(Refs);
}

void CDispPage::EditRef(UINT uOld, UINT uNew)
{
	m_pList->EditRef(uOld, uNew);
}

void CDispPage::PostConvert(void)
{
	m_pDbase->AddFlag(L"PrimConvert");

	CUISystem * pSystem = (CUISystem *) m_pDbase->GetSystemItem();

	double      nRatio;

	if( ConvertSize(pSystem->m_DispSize, nRatio) ) {

		ConvertFonts(nRatio);
	}

	m_pEvents->PostConvert();

	m_pList->PostConvert();

	m_pDbase->RemFlag(L"PrimConvert");
}

void CDispPage::AddHelloWorld(void)
{
	CUISystem        * pSystem = (CUISystem *) m_pDbase->GetSystemItem();

	CPrimRubyTextBox * pBox    = New CPrimRubyTextBox;

	m_Size = pSystem->m_DispSize;

	m_pList->AppendItem(pBox);

	pBox->m_pTextItem->m_Font = fontHei24;

	pBox->Set(CString(IDS_HELLO_WORLD), CSize(200, 20));

	CRect Rect = pBox->GetRect();

	Rect += CPoint((m_Size - Rect.GetSize()) / 2);

	pBox->SetRect(Rect);
}

// Property Access

DWORD CDispPage::GetProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case ppName:

			return DWORD(wstrdup(m_Name));

		case ppLabel:

			return FindLabel();

		case ppDescription:

			return FindDesc();
	}

	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
	}

	return 0;
}

// Item Naming

CString CDispPage::GetHumanName(void) const
{
	return m_Name;
}

// Persistance

void CDispPage::Init(void)
{
	CMetaItem::Init();

	CDatabase *pDbase  = GetDatabase();

	CUISystem *pSystem = (CUISystem *) pDbase->GetSystemItem();

	m_Size = pSystem->m_DispSize;
}

// Download Support

BOOL CDispPage::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_DISP_PAGE);

	CMetaItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Size.cx));
	Init.AddWord(WORD(m_Size.cy));

	Init.AddText(m_Name);

	Init.AddItem(itemSimple, m_pProps);
	Init.AddItem(itemSimple, m_pEvents);
	Init.AddItem(itemSimple, m_pList);

	return TRUE;
}

// Meta Data

void CDispPage::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddString(Name);
	Meta_AddPoint(Size);
	Meta_AddCollect(List);
	Meta_AddCollect(Events);
	Meta_AddObject(Props);
	Meta_AddInteger(Private);

	Meta_SetName((IDS_DISPLAY_PAGE_1));
}

// Implementation

DWORD CDispPage::FindLabel(void)
{
	if( m_pProps->m_pLabel ) {

		return DWORD(wstrdup(m_pProps->m_pLabel->GetText()));
	}

	return DWORD(wstrdup(m_Name));
}

DWORD CDispPage::FindDesc(void)
{
	return DWORD(wstrdup(m_pProps->m_Desc));
}

// Conversion Helpers

BOOL CDispPage::ConvertSize(CSize NewSize, double &nRatio)
{
	if( NewSize != m_Size ) {

		CRect  NewRect;

		double xRatio  = double(NewSize.cx) / double(m_Size.cx);

		double yRatio  = double(NewSize.cy) / double(m_Size.cy);

		if( keepAspect && m_pDbase->HasFlag(L"ForceIntegral") ) {

			nRatio = min(int(xRatio+0.0001), int(yRatio+0.0001));

			if( !nRatio ) {

				nRatio = min(xRatio, yRatio);
			}
		}
		else
			nRatio = min(xRatio, yRatio);

		if( keepAspect ) {

			CSize s = CSize(int(m_Size.cx * nRatio),
					int(m_Size.cy * nRatio)
			);

			NewRect = CRect(s) + CPoint(NewSize - s) / 2;
		}
		else
			NewRect = CRect(NewSize);

		if( NewRect.GetSize() == m_Size ) {

			MoveToFit(NewRect, m_Size);
		}
		else
			SizeToFit(NewRect, m_Size);

		m_Size = NewSize;

		return TRUE;
	}

	return FALSE;
}

BOOL CDispPage::ConvertFonts(double nRatio)
{
	CPrimRefList Refs;

	GetRefs(Refs);

	if( !Refs.IsEmpty() ) {

		CUISystem    * pSystem = (CUISystem *) m_pDbase->GetSystemItem();

		CFontManager * pFonts  = pSystem->m_pUI->m_pFonts;

		BOOL	       fEdit   = FALSE;

		INDEX	       Index   = Refs.GetHead();

		while( !Refs.Failed(Index) ) {

			UINT r = Refs[Index];

			if( r & refFont ) {

				UINT uFont = (r & ~refFont);

				UINT uRepl = uFont;

				if( pFonts->IsCustom(uFont) ) {

					CFontItem *pFont = pFonts->GetFont(uFont);

					CString    Face  = pFont->m_Face;

					UINT       uSize = int(pFont->m_Size * nRatio);

					BOOL       fBold = pFont->m_Bold;

					uRepl = pFonts->Create(Face, uSize, fBold);
				}
				else {
					if( pSystem->HasFont(uFont) ) {

						// TODO -- This doesn't handle shrinking fonts!!!!

						int  xNeed = int(nRatio * pFonts->GetWidth(uFont, L"X"));

						int  yNeed = int(nRatio * pFonts->GetHeight(uFont));

						for( ;;) {

							UINT uNext = pFonts->GetLarger(uRepl);

							if( uNext < NOTHING ) {

								int xNext = pFonts->GetWidth(uNext, L"X");

								int yNext = pFonts->GetHeight(uNext);

								if( xNext <= xNeed && yNext <= yNeed ) {

									uRepl = uNext;

									continue;
								}
							}

							break;
						}
					}
					else {
						uRepl = pFonts->GetSmaller(uFont);

						if( uRepl == NOTHING ) {

							uRepl = fontHei16;
						}
					}
				}

				if( uRepl != uFont ) {

					EditRef(r, uRepl);

					fEdit = TRUE;
				}
			}

			Refs.GetNext(Index);
		}

		if( fEdit ) {

			pFonts->UpdateUsed();

			pFonts->PurgeUnused();

			return TRUE;
		}
	}

	return FALSE;
}

void CDispPage::SizeToFit(CRect NewRect, CRect OldRect)
{
	INDEX Index   = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		pPrim->SetRect(OldRect, NewRect);

		m_pList->GetNext(Index);
	}
}

void CDispPage::MoveToFit(CRect NewRect, CRect OldRect)
{
	INDEX Index   = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		pPrim->SetRect(OldRect, NewRect);

		m_pList->GetNext(Index);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Page List
//

// Runtime Class

AfxImplementRuntimeClass(CPageList, CNamedList);

// Constructor

CPageList::CPageList(BOOL fScratch)
{
	m_fScratch = fScratch;

	m_Assume   = AfxRuntimeClass(CDispPage);
}

// Item Access

CMetaItem * CPageList::GetItem(INDEX Index) const
{
	return (CMetaItem *) CNamedList::GetItem(Index);
}

CMetaItem * CPageList::GetItem(UINT uPos) const
{
	return (CMetaItem *) CNamedList::GetItem(uPos);
}

CDispPage * CPageList::GetInit(void) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CDispPage *pPage = (CDispPage *) GetItem(n);

		if( pPage->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			return pPage;
		}

		GetNext(n);
	}

	return NULL;
}

// Operations

void CPageList::Validate(BOOL fExpand)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CMetaItem *pItem = GetItem(Index);

		if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CDispPage *pPage = (CDispPage *) pItem;

			pPage->Validate(fExpand);
		}

		GetNext(Index);
	}
}

void CPageList::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CMetaItem *pItem = GetItem(Index);

		if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CDispPage *pPage = (CDispPage *) pItem;

			pPage->TagCheck(Done, Tags);
		}

		GetNext(Index);
	}
}

void CPageList::ScanFonts(CFontManager *pFonts)
{
	CPrimRefList Refs;

	GetRefs(Refs);

	pFonts->MarkUsed(Refs);
}

void CPageList::ScanImages(CImageManager *pImages)
{
	CPrimRefList Refs;

	GetRefs(Refs);

	pImages->MarkUsed(Refs);
}

void CPageList::GetRefs(CPrimRefList &Refs)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CMetaItem *pItem = GetItem(Index);

		if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CDispPage *pPage = (CDispPage *) pItem;

			pPage->m_pList->GetRefs(Refs);
		}

		GetNext(Index);
	}
}

void CPageList::EditRef(UINT uOld, UINT uNew)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CMetaItem *pItem = GetItem(Index);

		if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CDispPage *pPage = (CDispPage *) pItem;

			pPage->m_pList->EditRef(uOld, uNew);
		}

		GetNext(Index);
	}
}

void CPageList::PostConvert(void)
{
	CUISystem   * pSystem = (CUISystem *) m_pDbase->GetSystemItem();

	CSize         NewSize = pSystem->m_DispSize;

	IGdiWindows * pWin    = Create_GdiWindowsA888(NewSize.cx, NewSize.cy);

	IGdi        * pGdi    = pWin->GetGdi();

	INDEX         Index   = GetHead();

	while( !Failed(Index) ) {

		CMetaItem *pItem = GetItem(Index);

		if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

			CDispPage *pPage = (CDispPage *) pItem;

			pPage->PostConvert();

			pPage->m_pList->Draw(pGdi, 0);
		}

		GetNext(Index);
	}

	pGdi->Release();
}

void CPageList::PostInit(void)
{
	CMetaItem *pItem = GetItem(GetHead());

	if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

		CDispPage *pPage = (CDispPage *) pItem;

		pPage->AddHelloWorld();
	}
}

// Persistance

void CPageList::Init(void)
{
	CNamedList::Init();

	CDispPage *pPage = New CDispPage;

	AppendItem(pPage);

	if( m_fScratch ) {

		pPage->SetName(L"Scratchpad");

		pPage->m_Size = CSize(640, 480);

		return;
	}

	pPage->SetName(L"Page1");
}

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Display Page Selection
//

class CUITextDispPage : public CUITextCoded
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUITextDispPage(void);

protected:
	// Data Members
	CString m_Was;

	// Overridables
	void    OnBind(void);
	CString OnGetAsText(void);
	UINT    OnSetAsText(CError &Error, CString Text);
	BOOL    OnEnumValues(CStringArray &List);
	BOOL    OnExpand(CWnd &Wnd);

	// Implementation
	BOOL CheckWas(void);
	UINT OnNewPage(CError &Error);
};

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Display Page Selection
//

// Dynamic Class

AfxImplementDynamicClass(CUITextDispPage, CUITextCoded);

// Constructor

CUITextDispPage::CUITextDispPage(void)
{
	m_uFlags |= textEnum | textRefresh | textExpand;

	m_Verb    = CString(IDS_NEW);
}

// Overridables

void CUITextDispPage::OnBind(void)
{
	CUITextCoded::OnBind();

	CheckWas();
}

CString CUITextDispPage::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.IsEmpty() ) {

		return IDS_PUI_NONE;
	}

	if( Text == L"//p" ) {

		return IDS_PREVIOUS_PAGE;
	}

	if( Text == L"//n" ) {

		return IDS_NEXT_PAGE;
	}

	return Text;
}

UINT CUITextDispPage::OnSetAsText(CError &Error, CString Text)
{
	if( Text == CString(IDS_PUI_NONE) ) {

		return CUITextCoded::OnSetAsText(Error, L"");
	}

	if( Text == CString(IDS_PREVIOUS_PAGE) ) {

		return CUITextCoded::OnSetAsText(Error, L"//p");
	}

	if( Text == CString(IDS_NEXT_PAGE) ) {

		return CUITextCoded::OnSetAsText(Error, L"//n");
	}

	return CUITextCoded::OnSetAsText(Error, Text);
}

BOOL CUITextDispPage::OnEnumValues(CStringArray &List)
{
	if( !m_Was.IsEmpty() ) {

		List.Append(m_Was);
	}

	if( TRUE ) {

		List.Append(CString(IDS_PUI_NONE));
	}

	if( m_UIData.m_Format.IsEmpty() ) {

		List.Append(CString(IDS_PREVIOUS_PAGE));

		List.Append(CString(IDS_NEXT_PAGE));
	}

	if( TRUE ) {

		CUISystem *pSystem = (CUISystem *) m_pItem->GetDatabase()->GetSystemItem();

		CPageList *pList   = pSystem->m_pUI->m_pPages;

		CDispPage *pHost   = (CDispPage *) m_pItem->HasParent(AfxRuntimeClass(CDispPage));

		for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

			CMetaItem *pItem = pList->GetItem(n);

			if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

				CDispPage *pPage = (CDispPage *) pItem;

				if( pHost && pPage == pHost ) {

					continue;
				}

				List.Append(pPage->GetName());
			}
		}
	}

	return TRUE;
}

BOOL CUITextDispPage::OnExpand(CWnd &Wnd)
{
	CError Error(TRUE);

	if( OnNewPage(Error) == saveError ) {

		return FALSE;
	}

	return TRUE;
}

// Implementation

BOOL CUITextDispPage::CheckWas(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.Left(4) == L"WAS " ) {

		m_Was = Text;

		return TRUE;
	}

	return FALSE;
}

UINT CUITextDispPage::OnNewPage(CError &Error)
{
	CUISystem  *pSystem = (CUISystem *) m_pItem->GetDatabase()->GetSystemItem();

	CUIManager *pUI     = pSystem->m_pUI;

	CPageList  *pList   = pUI->m_pPages;

	CItemCreateDialog Dlg(pList, typePage);

	if( Dlg.Execute() ) {

		CString Name = Dlg.GetName();

		if( !pSystem->m_pUI->CreatePage(Name) ) {

			CDispPage *pHost = (CDispPage *) m_pItem->HasParent(AfxRuntimeClass(CDispPage));

			CDispPage *pFind = (CDispPage *) pList->FindByName(Name);

			if( pFind ) {

				if( pHost == pFind ) {

					CWnd::GetActiveWindow().Error(CString(IDS_CANNOT_REFERENCE));

					return saveError;
				}
			}
			else {
				CWnd::GetActiveWindow().Error(CString(IDS_PAGE_COULD_NOT_BE));

				return saveError;
			}
		}

		return CUITextCoded::OnSetAsText(Error, Name);
	}

	return saveSame;
}

// End of File
