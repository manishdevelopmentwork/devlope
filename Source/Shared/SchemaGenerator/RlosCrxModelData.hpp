
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosCrxModelData_HPP

#define INCLUDE_RlosCrxModelData_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RlosBaseModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS CR Series Model Data
//

class CRlosCrxModelData : public CRlosBaseModelData
{
public:
	// Constructors
	CRlosCrxModelData(void);
	CRlosCrxModelData(CString const &Model);

	// IPxeModel
	void    METHOD MakeAppObjects(void);
	void    METHOD MakePxeObjects(void);
	BOOL    METHOD GetDispList(CArray<DWORD> &List);
	UINT    METHOD GetObjCount(char cTag);
	PCDWORD METHOD GetUsbPaths(char cTag);

protected:
	// Function Pointers
	BOOL(CRlosCrxModelData::*m_pfnGetDispList)(CArray<DWORD> &List);
	UINT(CRlosCrxModelData::*m_pfnGetObjCount)(char cTag);
	PCDWORD(CRlosCrxModelData::*m_pfnGetUsbPaths)(char cTag);

	// Implementation
	void BindModel(void);

	// Model Data
	BOOL    CR104_GetDispList(CArray<DWORD> &List);
	BOOL    CR107_GetDispList(CArray<DWORD> &List);
	BOOL    CR110_GetDispList(CArray<DWORD> &List);
	UINT    CR1XX_GetObjCount(char cTag);
	PCDWORD CR1XX_GetUsbPaths(char cTag);
	BOOL    CR304_GetDispList(CArray<DWORD> &List);
	UINT    CR304_GetObjCount(char cTag);
	BOOL    CR307_GetDispList(CArray<DWORD> &List);
	BOOL    CR310_GetDispList(CArray<DWORD> &List);
	BOOL    CR315_GetDispList(CArray<DWORD> &List);
	UINT    CR3XX_GetObjCount(char cTag);
	PCDWORD CR3XX_GetUsbPaths(char cTag);
};

// End of File

#endif
