
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Generic Toolbar Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CGadget, CObject);
		
// Constructor

CGadget::CGadget(void)
{
	m_pWnd   = NULL;
	m_uID    = 0;
	m_uFlags = 0;
	m_uProps = 0;
	m_fShow  = TRUE;
	}
		
// Attributes

UINT CGadget::GetID(void) const
{
	return m_uID;
	}

PCTXT CGadget::GetText(void) const
{
	return m_Text;
	}

UINT CGadget::GetFlags(void) const
{
	return m_uFlags;
	}

UINT CGadget::GetProps(void) const
{
	return m_uProps;
	}

BOOL CGadget::IsVisible(void) const
{
	return m_fShow;
	}

CSize CGadget::GetSize(void) const
{
	return m_Size;
	}

CRect CGadget::GetRect(void) const
{
	return m_Rect;
	}
		
// Operations

void CGadget::SetWindow(CWnd &Wnd)
{
	m_pWnd = &Wnd;
	}

void CGadget::SetID(UINT uID)
{
	m_uID = uID;
	}

void CGadget::SetText(PCTXT pText)
{
	m_Text = pText;
	
	OnSetText();
	}

void CGadget::SetFlags(UINT uFlags)
{
	m_uFlags = uFlags;
	
	OnSetFlags();
	}

void CGadget::SetRight(CGadget *pSource)
{
	m_uProps = ((m_uProps & ~tbgRight) | pSource->GetProps());
	}

void CGadget::SetPosition(CPoint const &Pos)
{
	m_Rect = CRect(Pos, m_Size);
	
	OnSetPosition();
	}

void CGadget::ShowGadget(BOOL fShow)
{
	m_fShow = fShow;
	
	OnShowGadget();
	}

void CGadget::SetHeight(int nHeight)
{
	m_Size.cy = nHeight;
	}

void CGadget::SetWidth(int nWidth)
{
	m_Size.cx = nWidth;
	
	m_Rect    = CRect(m_Rect.GetTopLeft(), m_Size);
	
	OnSetPosition();
	}
		
// Flag Helpers

BOOL CGadget::TestFlag(UINT uMask) const
{
	return !!(m_uFlags & uMask);
	}

void CGadget::AdjustFlag(UINT uMask, BOOL fState)
{
	fState ? SetFlag(uMask) : ClearFlag(uMask);
	}

void CGadget::SetFlag(UINT uMask)
{
	if( !(m_uFlags & uMask) ) {
	
		m_uFlags |= uMask;
		
		OnSetFlags();
		}
	}

void CGadget::ClearFlag(UINT uMask)
{
	if( m_uFlags & uMask ) {
	
		m_uFlags &= ~uMask;
		
		OnSetFlags();
		}
	}

// Overridables

void CGadget::OnCreate(CDC &DC)
{
	}

void CGadget::OnSetPosition(void)
{
	}

void CGadget::OnShowGadget(void)
{
	}

void CGadget::OnPaint(CDC &DC)
{
	DC.FillRect(m_Rect, afxBrush(RED));
	}

BOOL CGadget::OnMouseMenu(CPoint const &Pos)
{
	return FALSE;
	}

BOOL CGadget::OnMouseDown(CPoint const &Pos)
{
	return FALSE;
	}

BOOL CGadget::OnMouseDblClk(CPoint const &Pos)
{
	return OnMouseDown(Pos);
	}

void CGadget::OnMouseMove(CPoint const &Pos, BOOL fHit)
{
	}

void CGadget::OnMouseUp(CPoint const &Pos, BOOL fHit)
{
	}

BOOL CGadget::OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip)
{
	return FALSE;
	}

void CGadget::OnSetText(void)
{
	}

void CGadget::OnSetFlags(void)
{
	}

void CGadget::OnIdle(CWnd *pPrivate)
{
	}

// Implementation

void CGadget::Update(void)
{
	m_pWnd->Invalidate(m_Rect, FALSE);
	}

// End of File
