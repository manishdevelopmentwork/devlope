
#include "intern.hpp"

#include "PrimRubyStar6.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 6-Pointed Star Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyStar6, CPrimRubyStar)

// Constructor

CPrimRubyStar6::CPrimRubyStar6(void)
{
	m_nSides = 12;

	m_AltRad = 40;

	m_Rotate = 0;
	}

// Meta Data

void CPrimRubyStar6::AddMetaData(void)
{
	CPrimRubyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_7));
	}

// End of File
