
#include "Intern.hpp"

#include "NativeRawSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Socket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw Socket Object
//

// Instantiator

static IUnknown * Create_RawSocket(PCTXT pName)
{
	return (IUnknown *) New CNativeRawSocket;
}

// Registration

global void Register_NativeRawSocket(void)
{
	piob->RegisterInstantiator("net.sock-raw", Create_RawSocket);
}

global void Revoke_NativeRawSocket(void)
{
	piob->RevokeInstantiator("net.sock-raw");
}

// Constructor

CNativeRawSocket::CNativeRawSocket(void)
{
	m_uAddress = 0;
}

// Destructor

CNativeRawSocket::~CNativeRawSocket(void)
{
	Abort();
}

// ISocket

HRESULT CNativeRawSocket::Listen(IPADDR const &IP, WORD Loc)
{
	if( m_uState == stateInit ) {

		CNativeRawSocket::FindInterfaces();

		CIpAddr const &Ip = (CIpAddr const &) IP;

		for( UINT n = 0; n < m_Faces.GetCount(); n++ ) {

			CFace const *pFace = m_Faces[n];

			bool         fHit  = false;

			bool         fDone = false;

			if( Ip.IsEmpty() || Ip.IsBroadcast() || Ip.IsMulticast() ) {

				fHit = true;
			}
			else {
				for( UINT i = 0; i < pFace->m_IpAddrs.GetCount(); i++ ) {

					if( pFace->m_IpAddrs[i] == Ip ) {

						fHit  = true;

						fDone = true;

						break;
					}
				}
			}

			if( fHit ) {

				if( (m_hSocket = _socket(AF_PACKET, SOCK_RAW, HostToNet(Loc))) > 0 ) {

					SetAdapterAndProtocol(pFace->m_uIndex, Loc);

					if( Ip.IsMulticast() ) {

						SetMulticast(pFace->m_uIndex, Ip, pFace->m_Adapter);
					}
					else
						SetPromiscuous(pFace->m_uIndex, pFace->m_Adapter);

					SetNonBlocking();

					m_Addrs.Append(pFace->m_MacAddr);

					m_Socks.Append(m_hSocket);
				}

				if( fDone ) {

					break;
				}
			}
		}
	}

	if( m_Socks.GetCount() ) {

		m_uState = stateOpen;

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CNativeRawSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	return E_FAIL;
}

HRESULT CNativeRawSocket::Recv(PBYTE pData, UINT &uSize)
{
	UINT uSave = uSize;

	for( UINT h = 0; h < m_Socks.GetCount(); h++ ) {

		m_hSocket = m_Socks[h];

		if( CBaseSocket::Recv(pData, uSize) == S_OK ) {

			if( m_uAddress & 1 ) {

				// Override the destination address with that of
				// the adapter on which the packet was recieved.

				if( uSize >= 6 ) {

					memcpy(pData, m_Addrs[h].m_Addr, 6);
				}
			}

			AddSeenMac(pData, uSize, h);

			return S_OK;
		}

		uSize = uSave;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CNativeRawSocket::Send(PBYTE pData, UINT &uSize)
{
	if( uSize >= 12 ) {

		UINT uMask = GetSendMask(pData);

		for( UINT h = 0; h < m_Socks.GetCount(); h++ ) {

			if( uMask & (1<<h) ) {

				m_hSocket = m_Socks[h];

				memcpy(pData + 6, m_Faces[h]->m_MacAddr.m_Addr, 6);

				UINT uSend = uSize;

				if( CBaseSocket::Send(pData, uSend) == E_FAIL ) {

					return E_FAIL;
				}

				if( uSend != uSize ) {

					return E_FAIL;
				}
			}
		}

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CNativeRawSocket::Recv(CBuffer * &pBuff)
{
	for( UINT h = 0; h < m_Socks.GetCount(); h++ ) {

		m_hSocket = m_Socks[h];

		if( CBaseSocket::Recv(pBuff) == S_OK ) {

			PBYTE pData = pBuff->GetData();

			UINT  uSize = pBuff->GetSize();

			if( m_uAddress & 1 ) {

				// Override the destination address with that of
				// the adapter on which the packet was recieved.

				if( uSize >= 6 ) {

					memcpy(pData, m_Addrs[h].m_Addr, 6);
				}
			}

			AddSeenMac(pData, uSize, h);

			return S_OK;
		}
	}

	return E_FAIL;
}

HRESULT CNativeRawSocket::Send(CBuffer *pBuff)
{
	PBYTE pData = pBuff->GetData();

	UINT  uSize = pBuff->GetSize();

	if( uSize >= 12 ) {

		UINT uMask = GetSendMask(pData);

		for( UINT h = 0; h < m_Socks.GetCount(); h++ ) {

			if( uMask & (1<<h) ) {

				m_hSocket = m_Socks[h];

				memcpy(pData + 6, m_Faces[h]->m_MacAddr.m_Addr, 6);

				CAutoBuffer pCopy(pBuff->MakeCopy());

				if( CBaseSocket::Send(pCopy) == S_OK ) {

					pCopy.TakeOver();
				}
			}
		}

		pBuff->Release();

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CNativeRawSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	return E_FAIL;
}

HRESULT CNativeRawSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	return E_FAIL;
}

HRESULT CNativeRawSocket::SetOption(UINT uOption, UINT uValue)
{
	switch( uOption ) {

		case OPT_ADDRESS:

			m_uAddress = uValue;

			return S_OK;
	}

	return CBaseSocket::SetOption(uOption, uValue);
}

HRESULT CNativeRawSocket::Close(void)
{
	if( m_uState < stateError ) {

		for( UINT h = 0; h < m_Socks.GetCount(); h++ ) {

			m_hSocket = m_Socks[h];

			CloseHandle();
		}

		m_Socks.Empty();

		m_Addrs.Empty();

		m_uAddress = 0;

		return S_OK;
	}

	return E_FAIL;
}

// Implementation

void CNativeRawSocket::FindInterfaces(void)
{
	m_Faces.Empty();

	int fd = _socket(AF_INET, SOCK_DGRAM, 0);

	struct ifconf conf;

	conf.ifc_len           = 0;

	conf.ifc_ifcu.ifcu_buf = NULL;

	_ioctl(fd, SIOCGIFCONF, &conf);

	if( conf.ifc_len ) {

		conf.ifc_ifcu.ifcu_buf = malloc(conf.ifc_len);

		_ioctl(fd, SIOCGIFCONF, &conf);

		UINT c = conf.ifc_len / sizeof(struct ifreq);

		for( UINT n = 0; n < c; n++ ) {

			struct ifreq *ifr = conf.ifc_ifcu.ifcu_req + n;

			if( ifr->ifr_ifru.ifru_addr.sa_family == AF_INET ) {

				CIpAddr Ip = (DWORD &) ifr->ifr_ifru.ifru_addr.sa_data[2];

				if( !Ip.IsLoopback() ) {

					bool hit = false;

					for( UINT f = 0; f < m_Faces.GetCount(); f++ ) {

						CFace *pFace = m_Faces[f];

						if( pFace->m_Adapter == ifr->ifr_ifrn.ifrn_name ) {

							pFace->m_IpAddrs.Append(Ip);

							hit = true;

							break;
						}
					}

					if( !hit ) {

						CFace *pFace     = New CFace;

						pFace->m_Adapter = ifr->ifr_ifrn.ifrn_name;

						pFace->m_IpAddrs.Append(Ip);

						_ioctl(fd, SIOCGIFINDEX, ifr);

						pFace->m_uIndex  = ifr->ifr_ifru.ifru_ivalue;

						_ioctl(fd, SIOCGIFHWADDR, ifr);

						pFace->m_MacAddr = PBYTE(ifr->ifr_ifru.ifru_addr.sa_data);

						m_Faces.Append(pFace);
					}
				}
			}
		}

		free(conf.ifc_ifcu.ifcu_buf);
	}

	_close(fd);
}

void CNativeRawSocket::SetAdapterAndProtocol(UINT uIndex, WORD Protocol)
{
	struct sockaddr_ll addr;

	memset(&addr, 0, sizeof(addr));

	addr.sll_family   = AF_PACKET;

	addr.sll_protocol = HostToNet(Protocol);

	addr.sll_ifindex  = uIndex;

	_bind(m_hSocket, (struct sockaddr *) &addr, sizeof(addr));
}

void CNativeRawSocket::SetPromiscuous(UINT uIndex, PCTXT pName)
{
	struct packet_mreq mreq;

	memset(&mreq, 0, sizeof(mreq));

	mreq.mr_ifindex = uIndex;

	mreq.mr_type    = PACKET_MR_PROMISC;

	mreq.mr_alen    = 6;

	_setsockopt(m_hSocket, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
}

void CNativeRawSocket::SetMulticast(UINT uIndex, CIpAddr const &Ip, PCTXT pName)
{
	CMacAddr mac;

	mac.MakeMulticast(Ip);

	struct packet_mreq mreq;

	memset(&mreq, 0, sizeof(mreq));

	mreq.mr_ifindex = uIndex;

	mreq.mr_type    = PACKET_MR_MULTICAST;

	mreq.mr_alen    = 6;

	memcpy(mreq.mr_address, mac.m_Addr, 6);

	_setsockopt(m_hSocket, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
}

void CNativeRawSocket::AddSeenMac(PCBYTE pData, UINT uSize, UINT uFace)
{
	if( uSize >= 12 ) {

		CMacAddr const &Mac = (CMacAddr const &) (pData + 6);

		INDEX          Find = m_Seen.FindName(Mac);

		if( !m_Seen.Failed(Find) ) {

			m_Seen.SetData(Find, m_Seen.GetData(Find) | (1<<uFace));

			return;
		}

		m_Seen.Insert(Mac, (1<<uFace));
	}
}

UINT CNativeRawSocket::GetSendMask(PCBYTE pData)
{
	CMacAddr const &Mac = (CMacAddr const &) pData;

	INDEX          Find = m_Seen.FindName(Mac);

	return m_Seen.Failed(Find) ? NOTHING : m_Seen[Mac];
}

// End of File
