
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DiagOutputHtml_HPP
	
#define	INCLUDE_DiagOutputHtml_HPP

//////////////////////////////////////////////////////////////////////////
//
// Aeon Compatability
//

#if !defined(AEON_ENVIRONMENT) && !defined(METHOD)

#define METHOD 

#define StdSetRef(x)

typedef unsigned long ULONG;

#endif

//////////////////////////////////////////////////////////////////////////
//
// HTML Diagnostics Output
//

class CDiagOutputHtml : public IDiagOutput
{
	public:
		// Constructor
		CDiagOutputHtml(IDiagConsole *pConsole, PCTXT pName);

		// Destructor
		~CDiagOutputHtml(void);

		// IUnknown
		#if defined(AEON_ENVIRONMENT)
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		#endif

		// IDiagOutput
		void METHOD Error(PCTXT pText, ...);
		void METHOD Print(PCTXT pText, ...);
		void METHOD VPrint(PCTXT pText, va_list pArgs);
		void METHOD Dump(PCVOID pData, UINT uCount);
		void METHOD Dump(PCVOID pData, UINT uCount, UINT uBase);
		void METHOD AddTable(UINT uCols);
		void METHOD EndTable(void);
		void METHOD SetColumn(UINT uCol, PCTXT pName, PCTXT pForm);
		void METHOD AddHead(void);
		void METHOD AddRule(char cData);
		void METHOD AddRow(void);
		void METHOD EndRow(void);
		void METHOD SetData(UINT uCol, ...);
		void METHOD AddPropList(void);
		void METHOD EndPropList(void);
		void METHOD AddProp(PCTXT pName, PCTXT pForm, ...);
		void METHOD Finished(void);

	protected:
		// States
		enum
		{
			stateNormal = 0,
			};

		// Table Column
		struct CCol
		{
			PTXT	m_pHead;
			PTXT	m_pForm;
			bool	m_fRight;
			UINT	m_uWide;
			};

		// Table Row
		struct CRow
		{
			UINT   m_uType;
			char   m_cRule;
			PTXT * m_pData;
			};

		// Data Members
		ULONG	       m_uRefs;
		IDiagConsole * m_pConsole;
		PCTXT	       m_pName;
		bool	       m_fPrint;
		UINT	       m_State;
		UINT	       m_uCols;
		UINT	       m_uRows;
		CCol	     * m_pCol;
		CRow	     * m_pRow;

		// Implementation
		void IntPrint(PCTXT pText, ...);
		void IntVPrint(PCTXT pText, va_list pArgs);
		void Write(PCTXT pText);
		void Pad(PCTXT pText, UINT uWide, bool fRight);
		bool IsRight(PCTXT pForm);
	};

// End of File

#endif
