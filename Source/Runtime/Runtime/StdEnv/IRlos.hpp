
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Arm Hardware Drivers
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef	INCLUDE_IRlos_HPP
	
#define	INCLUDE_IRlos_HPP

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Macros
//

#define interface struct

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface IDiagOutput;
interface IEventSink;

//////////////////////////////////////////////////////////////////////////
//
// Family ? -- RLOS Core
//

interface IExecHook;
interface IPic;
interface IMmu;
interface IHal;

//////////////////////////////////////////////////////////////////////////
//
// Executive Hook
//

interface IExecHook
{
	// Methods
	virtual void ExecTick(void)		  = 0;
	virtual bool HasPendingDispatch(void)     = 0;
	virtual void DispatchThread(DWORD Frame)  = 0;
	virtual void AccumThreadTime(bool fTimer) = 0;
	virtual void AccumInterruptTime(void)     = 0;
	virtual bool SuspendThread(void)	  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Interrupt Controller
//

interface IPic
{
	// Methods
	virtual void OnInterrupt(UINT uSource)					= 0;
	virtual bool SetLinePriority(UINT uLine, UINT uPriority)		= 0;
	virtual bool SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam) = 0;
	virtual bool EnableLine(UINT uLine, bool fEnable)			= 0;
	virtual bool EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable)	= 0;
	virtual void DebugRegister(void)                                        = 0;
	virtual void DebugRevoke(void)	                                        = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Memory Management Unit
//

interface IMmu
{
	// Memory Management
	virtual bool  IsVirtualValid(DWORD dwAddr, bool fWrite)     = 0;
	virtual DWORD PhysicalToVirtual(DWORD dwAddr, bool fCached) = 0;
	virtual DWORD VirtualToPhysical(DWORD dwAddr)		    = 0;
	virtual void  DebugRegister(void)                           = 0;
	virtual void  DebugRevoke(void)	                            = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer
//

interface IHal
{
	// Executive Hooks
	virtual void SetExecHook(IExecHook *pExec) = 0;
	virtual void SetExecRunning(void)          = 0;

	// Power Management
	virtual void EnterIdleState(void) = 0;
	virtual void LeaveIdleState(void) = 0;
	virtual void RestartSystem(void)  = 0;
	virtual void ShutdownSystem(void) = 0;

	// Idle Mode
	virtual void WaitForInterrupt(void) = 0;

	// Timer Support
	virtual UINT GetTimerResolution(void)  = 0;
	virtual UINT TimeToTicks(UINT uTime)   = 0;
	virtual UINT TicksToTime(UINT uTicks)  = 0;
	virtual UINT GetTickCount(void)        = 0;
	virtual UINT GetTickFraction(void)     = 0;
	virtual void SpinDelay(UINT uTime)     = 0;
	virtual void SpinDelayFast(UINT uTime) = 0;

	// Debug Support
	virtual void DebugReset(void)       = 0;
	virtual UINT DebugRead(UINT uDelay) = 0;
	virtual void DebugOut(char cData)   = 0;
	virtual void DebugWait(void)	    = 0;
	virtual void DebugStop(void)	    = 0;
	virtual void DebugKick(void)	    = 0;
	virtual void DebugRegister(void)    = 0;
	virtual void DebugRevoke(void)	    = 0;

	// Memory Management
	virtual bool  IsVirtualValid(DWORD dwAddr, bool fWrite)     = 0;
	virtual DWORD PhysicalToVirtual(DWORD dwAddr, bool fCached) = 0;
	virtual DWORD VirtualToPhysical(DWORD dwAddr)		    = 0;
	virtual DWORD GetNonCachedAlias(DWORD dwAddr)		    = 0;
	virtual DWORD GetCachedAlias(DWORD dwAddr)		    = 0;

	// Interrupt Controller
	virtual bool SetLinePriority(UINT uLine, UINT uPriority)		= 0;
	virtual bool SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam) = 0;
	virtual bool EnableLine(UINT uLine, bool fEnable)			= 0;
	virtual bool EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable)	= 0;

	// Task Context
	virtual void * CreateContext(DWORD Stack, DWORD Entry, DWORD Param) = 0;
	virtual void   DeleteContext(void *pCtx)			    = 0;
	virtual void   SwitchContext(void *pSave, void *pLoad, DWORD Frame) = 0;
	virtual bool   ContextUsesFp(void *pCtx)			    = 0;

	// Task Diagnostics
	virtual DWORD GetActiveSp(void)				       = 0;
	virtual DWORD GetSavedSp (void *pCtx)			       = 0;
	virtual DWORD GetSavedPc (void *pCtx)			       = 0;
	virtual void  GetFuncName(char *pName, UINT uName, void *pCtx) = 0;
	virtual void  StackTrace (IDiagOutput *pOut, void *pCtx)       = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// HAL Functions
//

extern UINT Hal_GetIrql(void);
extern UINT Hal_RaiseIrql(UINT IRQL);
extern bool Hal_LowerIrql(UINT IRQL);
extern bool Hal_Critical(bool fEnter);

// End of File

#endif
