
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DataLogger_HPP

#define INCLUDE_DataLogger_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDataLogInfo;

class CDataLogList;

//////////////////////////////////////////////////////////////////////////
//
// Data Logger
//

class CDataLogger : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDataLogger(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Loading
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		BOOL CreateLog(CString Name);
		void PostConvert(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT           m_Handle;
		UINT           m_EnableBatch;
		UINT	       m_EnableSets;
		UINT           m_BatchCount;
		UINT	       m_BatchDrive;
		UINT           m_CSVEncode;
		CDataLogList * m_pLogs;
		CDataLogInfo * m_pInfo;

	protected:
		// Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Creation Helpers
		BOOL CreateLog(CString *pFull, CString Name);
		BOOL CreateLog(CSysProxy &System, CString *pFull, CString Name, UINT Type);
		BOOL CreateLog(CMetaItem * &pItem, UINT Type);
	};

// End of File

#endif
