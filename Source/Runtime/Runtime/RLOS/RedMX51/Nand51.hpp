
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_INand51_HPP
	
#define	INCLUDE_INand51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Objects
//

class CCcm51; 

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Nand Flash Manager
//

class CNand51 : public INandMemory, public IEventSink
{	
	public:
		// Constructor
		CNand51(CCcm51 *pCcm);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// INandMemory
		BOOL METHOD GetGeometry(CNandGeometry &Geom);
		BOOL METHOD GetUniqueId(UINT uChip, PBYTE pData);
		BOOL METHOD IsBlockBad(UINT uChip, UINT uBlock);
		BOOL METHOD MarkBlockBad(UINT uChip, UINT uBlock);
		BOOL METHOD EraseBlock(UINT uChip, UINT uBlock);
		BOOL METHOD WriteSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PCBYTE pData);
		BOOL METHOD ReadSector(UINT uChip, UINT uBlock, UINT uPage, UINT uSect, PBYTE pData);
		BOOL METHOD WritePage(UINT uChip, UINT uBlock, UINT uPage, PCBYTE pData);
		BOOL METHOD ReadPage(UINT uChip, UINT uBlock, UINT uPage, PBYTE pData);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Geometry
		struct CGeometry
		{
			UINT	m_uPageSize;
			UINT	m_uBlockSize;
			UINT	m_uPlaneSize;
			UINT	m_uDeviceSize;
			UINT	m_uSpare;
			UINT    m_uChips;
			};

		// IP Registers
		enum
		{
			regWriteProt	= 0x3000 / sizeof(DWORD),
			regAddrUnlock	= 0x3004 / sizeof(DWORD),
			regConfig2	= 0x3024 / sizeof(DWORD),
			regConfig3	= 0x3028 / sizeof(DWORD),
			regControl	= 0x302C / sizeof(DWORD),
			regError	= 0x3030 / sizeof(DWORD),
			regDelay	= 0x3034 / sizeof(DWORD),
			};
		
		// AXI Registers
		enum
		{
			axiBuffer	= 0x0000 / sizeof(DWORD),
			axiSpare	= 0x1000 / sizeof(DWORD),
			axiCommand	= 0x1E00 / sizeof(DWORD),
			axiAddress0	= 0x1E04 / sizeof(DWORD),
			axiAddress8	= 0x1E24 / sizeof(DWORD),
			axiConfig	= 0x1E34 / sizeof(DWORD),
			axiEccStatus	= 0x1E38 / sizeof(DWORD),
			axiStatusSum	= 0x1E3C / sizeof(DWORD),
			axiLaunch	= 0x1E40 / sizeof(DWORD),
			};

		// Command Bits
		enum
		{
			cmdAutoStatus	= Bit(12),
			cmdAutoCopy1	= Bit(11),
			cmdAutoCopy0	= Bit(10),
			cmdAutoErase	= Bit( 9),
			cmdAutoRead	= Bit( 7),
			cmdAutoProg	= Bit( 6),
			cmdDataOutStat	= Bit( 5),
			cmdDataOutId	= Bit( 4),
			cmdDataOutPage	= Bit( 3),
			cmdDataIn	= Bit( 2),
			cmdAddrIn	= Bit( 1),
			cmdCmdIn	= Bit( 0),
			};

		// Data Members
		PVDWORD	   m_pBase;
		PVDWORD	   m_pBuff;
		ULONG	   m_uRefs;
		CGeometry  m_Geom;
		UINT	   m_uLine;
		UINT       m_uClock;
		IEvent   * m_pDone;
		IMutex   * m_pLock;

		// Implementation
		void Lock(void);
		void Free(void);

		void InitController(void);
		void LockRegisters(bool fLock);
		void EnableEvents(void);
		void DisableEvents(void);

		// Commands
		bool AutoReadPage(UINT uBlock, UINT uPage);
		bool AutoProgPage(UINT uBlock, UINT uPage);
		bool AutoEraseBlock(UINT uBlock);
		bool AtomicReadId(UINT uChip);
		bool AtomicReadPage(UINT uChip, UINT uBlock, UINT uPage);
		bool AtomicProgPage(UINT uChip, UINT uBlock, UINT uPage);
		bool AtomicEraseBlock(UINT uChip, UINT uBlock);
		bool AtomicReadStatus(UINT uChip);
		bool Reset(UINT uChip);
		bool WaitDone(UINT uChip, UINT uWait);
		void WaitReady(void);
		
		// Atomic 
		bool AtomicCmd(UINT uChip, BYTE bCmd, UINT uWait);
		bool AtomicAddr(UINT uChip, DWORD dwAddr);
		bool AtomicDataIn(UINT uChip);
		bool AtomicDataOut(UINT uChip);
		bool AtomicDataId(UINT uChip);
		bool AtomicDataStatus(UINT uChip);
	};

// End of File

#endif
