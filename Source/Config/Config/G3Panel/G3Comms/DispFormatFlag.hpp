
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatFlag_HPP

#define INCLUDE_DispFormatFlag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispFormat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Display Format
//

class DLLAPI CDispFormatFlag : public CDispFormat
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatFlag(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Formatting
		CString Format(DWORD Data, UINT Type, UINT Flags);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedText * m_pOff;
		CCodedText * m_pOn;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		UINT GetLimit(void);
	};

// End of File

#endif
