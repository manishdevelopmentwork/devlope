
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Session Address Rewrite
//

// Data

var session;

// Code

function doSession(onLoaded) {

	session = getParam("session");

	if (!session) {

		session = "null";
	}

	if (typeof doCustom === "function") {

		doCustom();
	}

	if (typeof onLoaded === "function") {

		onLoaded();
	}

	$("a").each(function (n, e) { e.href = reWrite(e.href); });

	$("form").each(function (n, e) { e.action = reWrite(e.action); });

	$("img").each(function (n, e) { e.src = reWrite(e.src); });
}

function doReturn() {

	setTimeout(doJump, 2000);
}

function doJump() {

	location.href = getParam("back") + "?session=" + session;
}

function reWrite(href) {

	if (href && href != "#") {

		if (href.substring(0, 11) != "javascript:") {

			var args = href.indexOf('?');

			if (args < 0) {

				href += "?session=" + session;
			}
			else {

				var left = href.substring(0, args + 1);

				var rest = href.substring(args + 1, href.length);

				if (rest.substring(0, 8) == "session=") {

					var last = rest.indexOf('&');

					if (last < 0) {

						return left + "session=" + session;
					}
					else {

						rest = rest.substring(last + 1, rest.length);
					}
				}

				return left + "session=" + session + "&" + rest;
			}
		}
	}

	return href;
}

function cacheBreaker() {

	return "&nc=" + (new Date()).getTime();
}

function getParam(name) {

	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");

	var u = location.href;

	var s = "[\\?&]" + name + "=([^&#]*)";

	var r = new RegExp(s);

	var k = r.exec(u);

	return k == null ? null : k[1];
}

function Queue() {

	var a = [], b = 0;

	this.getLength = function () { return a.length - b; };

	this.isEmpty = function () { return a.length == 0; };

	this.enqueue = function (b) { a.push(b); };

	this.dequeue = function () { if (a.length > 0) { var c = a[b]; 2 * ++b >= a.length && (a = a.slice(b), b = 0); return c; } };

	this.peek = function () { return a.length > 0 ? a[b] : void 0; }
}

function check401(code) {

	if (code == 401) {

		var uri = window.location + "";

		var len = 2;

		len += window.location.protocol.length;

		len += window.location.host.length;

		uri = uri.substring(len, uri.length);

		window.location = "/logon.htm?uri=" + uri;

		return true;
	}

	return false;
}

function setUrlArg(name, value) {

	var url = location.protocol + '//' + location.host + location.pathname;

	url += "?" + name + "=" + value;

	url += "&session=" + session;

	window.history.pushState("", "", url);
}

// End of File
