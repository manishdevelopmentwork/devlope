
#include "Intern.hpp"

#include "AutoEvent.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Auto Event Object
//

// Instantiator

AfxStdInstantiator(IEvent, AutoEvent);

// Registration

global void Register_AutoEvent(void)
{
	piob->RegisterInstantiator("exec.event-a", Create_AutoEvent);
	}

// Constructor

CAutoEvent::CAutoEvent(void)
{
	m_hSync = win32::CreateEvent(NULL, FALSE, FALSE, NULL);
	}

// IUnknown

HRESULT CAutoEvent::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IEvent);

	return CWaitable::QueryInterface(riid, ppObject);
	}

ULONG CAutoEvent::AddRef(void)
{
	return CWaitable::AddRef();
	}

ULONG CAutoEvent::Release(void)
{
	return CWaitable::Release();
	}

// IWaitable

PVOID CAutoEvent::GetWaitable(void)
{
	return CWaitable::GetWaitable();
	}

BOOL CAutoEvent::Wait(UINT uWait)
{
	return CWaitable::Wait(uWait, TRUE);
	}

BOOL CAutoEvent::HasRequest(void)
{
	return CWaitable::HasRequest();
	}

// IEvent

void CAutoEvent::Set(void)
{
	win32::SetEvent(m_hSync);
	}

void CAutoEvent::Clear(void)
{
	win32::ResetEvent(m_hSync);
	}

// End of File
