
#include "intern.hpp"

#include "koyok.hpp"

//////////////////////////////////////////////////////////////////////////
//
// KoyoKSequence Driver
//

// Instantiator

INSTANTIATE(CKoyoKSequenceDriver);

// Constructor

CKoyoKSequenceDriver::CKoyoKSequenceDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uSend = 0;
	}

// Destructor

CKoyoKSequenceDriver::~CKoyoKSequenceDriver(void)
{
	}

// Configuration

void MCALL CKoyoKSequenceDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CKoyoKSequenceDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CKoyoKSequenceDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CKoyoKSequenceDriver::Open(void)
{
	}

// Device

CCODE MCALL CKoyoKSequenceDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_PingEnable = GetByte(pData);

			m_pCtx->m_PingReg = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKoyoKSequenceDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKoyoKSequenceDriver::Ping(void)
{
	if( m_pCtx->m_PingEnable ) {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = m_pCtx->m_PingReg;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKoyoKSequenceDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	UINT uAddr = 0;

	switch( Addr.a.m_Table ) {
	
		case 1:		uAddr = 0x0000; bType = '1';	break;	/* Data Registers	*/
		case 2:		uAddr = 0x0000; bType = '1';	break;	/* Current Time		*/
		case 3:		uAddr = 0x0200; bType = '1';	break;	/* Current Count	*/
		case 4:		uAddr = 0x4100; bType = '2';	break;	/* I - X		*/
		case 5:		uAddr = 0x4000; bType = '2';	break;	/* I - GX		*/
		case 6:		uAddr = 0x4280; bType = '2';	break;	/* I - Special Relay	*/
		case 7:		uAddr = 0x4140; bType = '3';	break;	/* O - Y		*/
		case 8:		uAddr = 0x4180; bType = '3';	break;	/* O - C		*/
		case 9:		uAddr = 0x4200; bType = '3';	break;	/* O - Stage Bits	*/
		case 10:	uAddr = 0x4240; bType = '3';	break;	/* O - TMR Status Bits	*/
		case 11:	uAddr = 0x4260; bType = '3';	break;	/* O - CTR Status Bits	*/
		case 12:	uAddr = 0x4080; bType = '3';	break;  /* O - GY		*/

		default:	return CCODE_ERROR | CCODE_HARD;

		}

	if( bType == '1' ) {

		uAddr += Addr.a.m_Offset;

		uCount = min(uCount, FindCount(Addr.a.m_Type));
		
		}
	else {
		uAddr += Addr.a.m_Offset / 16;

		uCount = min(uCount, 64);

		}

	if( Read(uAddr, pData, uCount, Addr.a.m_Type) ) {
		
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CKoyoKSequenceDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bType = 0;

	UINT uAddr = 0;

	switch( Addr.a.m_Table ) {
	
		case 1:		uAddr = 0x0000; bType = '1';	break;	/* Data Registers	*/
		case 2:		uAddr = 0x0000; bType = '1';	break;	/* Current Time		*/
		case 3:		uAddr = 0x0200; bType = '1';	break;	/* Current Count	*/
		case 4:		uAddr = 0x4100; bType = '2';	break;	/* I - X		*/
		case 5:		uAddr = 0x4000; bType = '2';	break;	/* I - GX		*/
		case 6:		uAddr = 0x4280; bType = '2';	break;	/* I - Special Relay	*/
		case 7:		uAddr = 0x4140; bType = '3';	break;	/* O - Y		*/
		case 8:		uAddr = 0x4180; bType = '3';	break;	/* O - C		*/
		case 9:		uAddr = 0x4200; bType = '3';	break;	/* O - Stage Bits	*/
		case 10:	uAddr = 0x4240; bType = '3';	break;	/* O - TMR Status Bits	*/
		case 11:	uAddr = 0x4260; bType = '3';	break;	/* O - CTR Status Bits	*/
		case 12:	uAddr = 0x4080; bType = '3';	break;  /* O - GY		*/

		default:	return CCODE_ERROR | CCODE_HARD;

		}

	if( bType == '1' ) {

		uAddr += Addr.a.m_Offset;

		uCount = min(uCount, FindCount(Addr.a.m_Type));

		}
	else {
		uAddr += Addr.a.m_Offset / 16;

		uCount = min(uCount, 64);

		}

	if( Write(uAddr, pData, uCount, Addr.a.m_Type) ) {
		
		return uCount;
		}

	return CCODE_ERROR;
	}

// Port Access

void CKoyoKSequenceDriver::Put(BYTE b)
{
	m_bTxSend[m_uSend++] = b;
	}

void CKoyoKSequenceDriver::Put(void)
{
	m_pData->Write(m_bTxSend, m_uSend, FOREVER);

	m_uSend = 0;
	}

UINT CKoyoKSequenceDriver::Get(void)
{
	return m_pData->Read(GetTimer());
	}

// Frame Building

void CKoyoKSequenceDriver::StartHeader(void)
{
	m_uPtr = 0;
	}

void CKoyoKSequenceDriver::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}

void CKoyoKSequenceDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CKoyoKSequenceDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CKoyoKSequenceDriver::AddData(PDWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i++ ) {

		m_bTxBuff[m_uPtr++] = LOBYTE(*pData);

		m_bTxBuff[m_uPtr++] = HIBYTE(*pData);

		pData++;
		}
	}

void CKoyoKSequenceDriver::AddLongData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		AddLong(pData[u]);
		}
	}

// Opcode Handlers

BOOL CKoyoKSequenceDriver::Read(UINT uAddr, PDWORD pData, UINT uCount, UINT uType)
{
	StartHeader();

	AddByte(0x40);

	AddByte(0x01);

	AddByte(HIBYTE(uAddr));

	AddByte(LOBYTE(uAddr));

	BOOL fLong = IsLong(uType);
	
	AddByte(fLong ? uCount * 2 : uCount);

	if( Transact(TRUE) ) {

		UINT uPos = fLong ? sizeof(DWORD) : sizeof(WORD);

		if( m_uPtr == uCount * uPos + 3 ) {

			if( m_bRxBuff[0] == 0xC0 ) {
	
				if( m_bRxBuff[1] == HIBYTE(uAddr) ) {
					
					if( m_bRxBuff[2] == LOBYTE(uAddr) ) {

						if( fLong ) {

							return GetLong(pData, uCount);
							}

						BYTE b1;
						BYTE b2;

						UINT uPos = 3;

						for ( UINT i = 0; i < uCount; i++) {

							b1 = m_bRxBuff[uPos++];

							b2 = m_bRxBuff[uPos++];

							*pData++ = MAKEWORD(b1, b2);

							}

						return TRUE;
						}
					}
				}
			}
		}
		
	return FALSE;
	}

BOOL CKoyoKSequenceDriver::Write(UINT uAddr, PDWORD pData, UINT uCount, UINT uType)
{
	StartHeader();

	AddByte(0x46);

	AddByte(0x01);

	AddByte(HIBYTE(uAddr));

	AddByte(LOBYTE(uAddr));

	BOOL fLong = IsLong(uType);
	
	AddByte(fLong ? uCount * 2 : uCount);

	fLong ? AddLongData(pData, uCount) : AddData(pData, uCount);

	if( Transact(TRUE) ) {

		if( m_uPtr == 2 ) {

			if( m_bRxBuff[0] == 0xC6 && m_bRxBuff[1] == 0x88 ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Transport Layer

BOOL CKoyoKSequenceDriver::Transact(BOOL fEOT)
{
	m_pData->ClearRx();
	
	TxEnq();
	
	if( RxAckEnq() ) {
		
		BOOL fResult = TxHeader() && RxData() && (!fEOT || RxEOT());
	
		TxEOT();

		return fResult;
		}

	TxEOT();

	return FALSE;
	}

BOOL CKoyoKSequenceDriver::TxHeader(void)
{
	TxHeaderData();

	switch( RxAck() ) {

		case ACK:
			return TRUE;
		
		case NAK:
			return FALSE;
		
		case EOT:
			return FALSE;
		}
	
	return FALSE;
	}

void CKoyoKSequenceDriver::TxHeaderData(void)
{
	Put(SOH);
	
	ClearCheck();
	
	for( UINT n = 0; n < m_uPtr; n ++ ) {

		BYTE b = m_bTxBuff[n];

		Put(b);

		AddToCheck(b);
		}

	Put(ETB);

	Put(m_bCheck);

	Put();
	}
	
BOOL CKoyoKSequenceDriver::RxData(void)
{
	UINT uState = 0;

	UINT uLen   = 0;

	UINT uData  = 0;

	SetTimer(TIMEOUT);
	
	while( GetTimer() ) {

		if( (uData = Get()) == NOTHING ) {
			
			continue;
			}
			
		switch( uState ) {

			case 0 : 
				if( uData == STX ) {

					m_uPtr = 0;
					
					ClearCheck();

					uState++;
					}
				break;
			
			case 1: 
				uLen = uData;

				AddToCheck(BYTE(uData));
				
				uState++;

				break;

			case 2:
				uLen = uLen + (uData << 8);

				AddToCheck(BYTE(uData));
				
				uState++;

				break;

			case 3:
				m_bRxBuff[m_uPtr++] = uData;
					
				if( m_uPtr < sizeof(m_bRxBuff) ) {
							
					AddToCheck(BYTE(uData));

					if( --uLen == 0 )  {

						uState++;
						}

					break;
					}

				return FALSE;
				
			case 4:
				if( uData == ETX ) {

					uState++;

					break;
					}

				return FALSE;

			case 5:
				if( uData == m_bCheck ) {

					TxAck();
					
					return TRUE;
					}
				
				return FALSE;
			}
		}
	
	return FALSE;
	}

// Checksum Handler

void CKoyoKSequenceDriver::ClearCheck(void)
{
	m_bCheck = 0;
	}

void CKoyoKSequenceDriver::AddToCheck(BYTE b)
{
	m_bCheck ^= b;
	}

// Data Link Layer

void CKoyoKSequenceDriver::TxAck(void)
{	
	Put(ACK);

	Put();
	}

void CKoyoKSequenceDriver::TxNak(void)
{	
	Put(NAK);

	Put();
	}

void CKoyoKSequenceDriver::TxEOT(void)
{	
	Put(EOT);

	Put();
	}

void CKoyoKSequenceDriver::TxEnq(void)
{
	Put(0x4B);

	Put((m_pCtx->m_bDrop) + 0x20);

	Put(ENQ);

	Put();
	}

BOOL CKoyoKSequenceDriver::RxEOT(void)
{
	UINT uData = 0;
	
	SetTimer(TIMEOUT);

	while( GetTimer() ) {

		if( (uData = Get()) == NOTHING ) {
			
			continue;
			}

		if( uData == EOT ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

UINT CKoyoKSequenceDriver::RxAck(void)
{
	SetTimer(TIMEOUT);

	UINT uData = 0;
	
	while( GetTimer() ) {

		if( (uData = Get()) == NOTHING ) {
			
			continue;
			}

		switch( uData ) {

			case ACK: 
			case NAK:
			case EOT:
				return uData;
			}
		}
		
	return NOTHING;
	}

BOOL CKoyoKSequenceDriver::RxAckEnq(void)
{
	UINT uState = 0;

	UINT uData  = 0;

	SetTimer(TIMEOUT);
	
	while( GetTimer() ) {

		if( (uData = Get()) == NOTHING ) {
			
			continue;
			}
			
		switch( uState ) {

			case 0:
				if( uData == 0x4B ) {

					uState++;
					}
				break;

			case 1:
				if( uData == UINT((m_pCtx->m_bDrop) + 0x20) ) {

					uState++;
					}
				break;
		
			case 2:
				if( uData == ACK ) {

					return TRUE;
					}
				break;
			}		
		}
	
	return FALSE;
	}

// Helpers

BOOL CKoyoKSequenceDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CKoyoKSequenceDriver::GetLong(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		DWORD x = PU4(m_bRxBuff + 3)[u];
			
		pData[u] = IntelToHost(x);

		}

	return TRUE;
	}

UINT CKoyoKSequenceDriver::FindCount(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:

			return 32;
		}

	return 64;
	}

// End of File
