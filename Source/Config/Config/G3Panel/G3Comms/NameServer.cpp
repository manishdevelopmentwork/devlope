
#include "Intern.hpp"

#include "NameServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsManager.hpp"
#include "CommsPort.hpp"
#include "CommsSystem.hpp"
#include "DataLog.hpp"
#include "DataLogger.hpp"
#include "DataLogList.hpp"
#include "DataTag.hpp"
#include "ItemCreateDialog.hpp"
#include "ProgramItem.hpp"
#include "ProgramList.hpp"
#include "ProgramManager.hpp"
#include "Tag.hpp"
#include "TagList.hpp"
#include "TagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Name Server
//

// Runtime Class

AfxImplementRuntimeClass(CNameServer, CObject);

// Standard Tag Properties

PCTXT CNameServer::m_pTagProps[] = {

	L"AsText",   //  1
	L"Label",    //  2
	L"Desc",     //  3
	L"Prefix",   //  4
	L"Units",    //  5
	L"SP",	     //  6
	L"Min",	     //  7
	L"Max",	     //  8
	L"Fore",     //  9
	L"Back",     // 10
	L"Name",     // 11
	L"Index",    // 12
	L"Alarms",   // 13
	L"TextOff",  // 14
	L"TextOn",   // 15
	L"States",   // 16
	L"Deadband", // 17

};

// Constructor

CNameServer::CNameServer(CCommsSystem *pSystem)
{
	m_pSystem   = pSystem;
	
	m_pComms    = m_pSystem->m_pComms;
	
	m_pTags     = m_pSystem->m_pTags->m_pTags;
	
	m_pProgs    = m_pSystem->m_pPrograms->m_pPrograms;
	
	m_pLogs     = m_pSystem->m_pLog->m_pLogs;
	
	m_fBuild    = FALSE;
	
	m_uLen      = 0;
	
	m_pFuncLib  = NULL;
	
	m_pIdentLib = NULL;
	
	m_pIdent    = NULL;
	
	m_pFunc     = NULL;
}

// Destructor

CNameServer::~CNameServer(void)
{
	m_pFuncLib->Release();

	m_pIdentLib->Release();
}

// Initialization

void CNameServer::LoadLibrary(void)
{
	if( m_pFuncLib ) {

		m_pFuncLib->Release();

		m_pIdentLib->Release();
	}

	if( TRUE ) {

		C3MakeFuncLib(m_pFuncLib);

		C3MakeIdentLib(m_pIdentLib);

		CError Error(FALSE);

		UINT uGroup = m_pSystem->GetDatabase()->GetSoftwareGroup();

		if( uGroup >= SW_GROUP_2 ) {

			if( uGroup >= SW_GROUP_3B ) {

				AddFuncsCamera(Error);

				AddFuncsDrive(Error);

				AddFuncsFile(Error);

				AddFuncsFTP(Error);

				AddFuncsLoggging(Error);

				AddFuncsMail(Error);

				AddFuncsRemote(Error);

				AddFuncsWeb(Error);

				AddFuncsSQL(Error);
			}

			AddFuncsCommsData(Error);

			AddFuncsCommsDevice(Error);

			AddFuncsEvents(Error);

			AddFuncsNetworkPort(Error);

			AddFuncsSystem(Error);

			AddFuncsTagData(Error);

			AddFuncsUI(Error);

			if( C3OemFeature(L"OemSD", FALSE) ) {

				AddFuncsHoneywell(Error);
			}

			AddVarsAlarms(Error);
		}

		AddFuncsCommsPort(Error);

		AddFuncsCommsStatus(Error);

		AddFuncsMath(Error);

		AddFuncsStrings(Error);

		AddFuncsTime(Error);

		AddFuncsNetworkPort(Error);

		AddFuncsSystem(Error);

		AddFuncsUI(Error);

		AddVarsSystem(Error);

		AddVarsTime(Error);

		if( uGroup >= SW_GROUP_3A ) {

			AddFuncsCAN(Error);
		}

		if( uGroup >= SW_GROUP_4 ) {

			AddFuncsControl(Error);
		}
	}
}

// Operations

void CNameServer::ResetServer(void)
{
	m_Root.Empty();

	m_uLen   = 0;

	m_pIdent = NULL;

	m_pFunc  = NULL;
}

void CNameServer::RebuildDirect(BOOL fBuild)
{
	m_fBuild = fBuild;
}

void CNameServer::SetFunc(IFuncServer *pFunc)
{
	m_pFunc = pFunc;
}

void CNameServer::SetIdent(IIdentServer *pIdent)
{
	m_pIdent = pIdent;
}

void CNameServer::SetRoot(CString Root)
{
	m_Root = Root + L".";

	m_uLen = m_Root.GetLength();
}

// IBase Methods

UINT CNameServer::Release(void)
{
	delete this;

	return 0;
}

// INameServer Methods

BOOL CNameServer::FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( wstrlen(Name) == 7 ) {

		if( !wstrncmp(Name, L"__T", 3) ) {

			UINT  uPos = wcstoul(PCTXT(Name) + 3, NULL, 16);

			CTag *pTag = m_pTags->GetItem(uPos);

			if( pTag ) {

				if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

					CDataRef &Ref = (CDataRef &) ID;

					Ref.t.m_IsTag = 1;

					Ref.t.m_Index = uPos;

					Type.m_Type   = pTag->GetDataType();

					Type.m_Flags  = pTag->GetTypeFlags();

					return TRUE;
				}
			}
		}
	}

	if( m_pIdent ) {

		if( m_pIdent->FindIdent(pError, Name, ID, Type) ) {

			return TRUE;
		}
	}

	if( FindStd(pError, Name, ID, Type) ) {

		return TRUE;
	}

	if( FindDevice(pError, Name, ID, Type) ) {

		return TRUE;
	}

	if( FindPort(pError, Name, ID, Type) ) {

		return TRUE;
	}

	if( FindLog(pError, Name, ID, Type) ) {

		return TRUE;
	}

	return FindTag(pError, Name, ID, Type);
}

BOOL CNameServer::NameIdent(WORD ID, CString &Name)
{
	if( m_pIdent ) {

		if( m_pIdent->NameIdent(ID, Name) ) {

			return TRUE;
		}
	}

	if( NameStd(ID, Name) ) {

		return TRUE;
	}

	if( NameDevice(ID, Name) ) {

		return TRUE;
	}

	if( NamePort(ID, Name) ) {

		return TRUE;
	}

	if( NameLog(ID, Name) ) {

		return TRUE;
	}

	return NameTag(ID, Name);
}

BOOL CNameServer::FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( m_pIdent ) {

		if( m_pIdent->FindProp(pError, Name, ID, PropID, Type) ) {

			return TRUE;
		}
	}

	if( Ref.t.m_IsTag ) {

		CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

		if( pTag ) {

			if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

				UINT Data = pTag->GetDataType();

				if( FindTagProp(Name, Data, PropID, Type) ) {

					return TRUE;
				}

				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::NameProp(WORD ID, WORD PropID, CString &Name)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( m_pIdent ) {

		if( m_pIdent->NameProp(ID, PropID, Name) ) {

			return TRUE;
		}
	}

	if( Ref.t.m_IsTag ) {

		CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

		if( pTag ) {

			if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

				if( NameTagProp(PropID, Name) ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::FindDirect(CError *pError, CString Name, DWORD &ID, CTypeDef &Type)
{
	if( wstrlen(Name) == 14 ) {

		if( !wstrncmp(Name, L"__D", 3) ) {

			UINT uDev = wcstoul(PCTXT(Name) + 3, NULL, 16);

			UINT uAdr = wcstoul(PCTXT(Name) + 6, NULL, 16);

			CCommsDevice *pDevice = m_pComms->FindDevice(uDev);

			if( pDevice ) {

				ICommsDriver *pDriver = pDevice->GetDriver();

				if( pDriver ) {

					CAddress const &Addr = (CAddress const &) uAdr;

					CString         Text = L"";

					if( pDriver->ExpandAddress(Text, pDevice->m_pConfig, Addr) ) {

						Name.Printf(L"%s.%s", pDevice->m_Name, Text);
					}
				}
			}
		}
	}

	return m_pComms->FindDirect(pError, Name, ID, Type);
}

BOOL CNameServer::NameDirect(DWORD ID, CString &Name)
{
	return !m_fBuild && m_pComms->NameDirect(ID, Name);
}

BOOL CNameServer::FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount)
{
	UINT uPos;

	if( m_pFunc ) {

		if( m_pFunc->FindFunction(pError, Name, ID, Type, uCount) ) {

			return TRUE;
		}
	}

	if( (uPos = m_pProgs->FindNamePos(Name)) < NOTHING ) {

		CMetaItem *pItem = m_pProgs->GetItem(uPos);

		if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

			CProgramItem *pProgram = (CProgramItem *) pItem;

			ID = WORD(0x8000 | pProgram->m_Handle);

			pProgram->GetTypeData(L"Code", Type);

			uCount = pProgram->GetParameterCount();

			return TRUE;
		}
	}

	return m_pFuncLib->FindFunction(pError, Name, ID, Type, uCount);
}

BOOL CNameServer::NameFunction(WORD ID, CString &Name)
{
	if( m_pFunc ) {

		if( m_pFunc->NameFunction(ID, Name) ) {

			return TRUE;
		}
	}

	if( ID & 0x8000 ) {

		UINT   hItem = (ID & 0x7FFF);

		CItem *pItem = m_pSystem->GetDatabase()->GetHandleItem(hItem);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

				CProgramItem *pProgram = (CProgramItem *) pItem;

				Name = pProgram->m_Name;

				return TRUE;
			}
		}

		return FALSE;
	}

	if( ID >= 0x7F80 ) {

		return FALSE;
	}

	return m_pFuncLib->NameFunction(ID, Name);
}

BOOL CNameServer::EditFunction(WORD &ID, CTypeDef &Type)
{
	if( ID & 0x8000 ) {

		return FALSE;
	}

	if( ID >= 0x7F80 ) {

		return FALSE;
	}

	return m_pFuncLib->EditFunction(ID, Type);
}

BOOL CNameServer::GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type)
{
	if( ID & 0x8000 ) {

		UINT   hItem = (ID & 0x7FFF);

		CItem *pItem = m_pSystem->GetDatabase()->GetHandleItem(hItem);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

				CProgramItem *pProgram = (CProgramItem *) pItem;

				if( uParam < pProgram->GetParameterCount() ) {

					CFuncParam const *pList = pProgram->GetParameterList();

					CFuncParam const &Param = pList[uParam];

					Name        = Param.m_Name;

					Type.m_Type = Param.m_Type;

					Type.m_Flags = 0;

					return TRUE;
				}

				return FALSE;
			}
		}
	}

	if( ID >= 0x7F80 ) {

		return FALSE;
	}

	return m_pFuncLib->GetFuncParam(ID, uParam, Name, Type);
}

BOOL CNameServer::FindClass(CError *pError, CString Name, UINT &Type)
{
	if( Name == L"CPort" ) {

		Type = typePort;

		return TRUE;
	}

	if( Name == L"CDevice" ) {

		Type = typeDevice;

		return TRUE;
	}

	if( Name == L"CLog" ) {

		Type = typeLog;

		return TRUE;
	}

	return FALSE;
}

BOOL CNameServer::NameClass(UINT Type, CString &Name)
{
	switch( Type ) {

		case typeFolder:

			Name = CString(IDS_FOLDER_2);

			return TRUE;

		case typePort:

			Name = CString(IDS_COMMS_PORT_2);

			return TRUE;

		case typeDevice:

			Name = CString(IDS_COMMS_DEVICE_2);

			return TRUE;

		case typeLog:

			Name = CString(IDS_DATA_LOG_2);

			return TRUE;
	}

	return FALSE;
}

BOOL CNameServer::SaveClass(UINT Type)
{
	switch( Type ) {

		case typeFolder:

			return FALSE;
	}

	return TRUE;
}

// Standard Tag Properties

BOOL CNameServer::FindTagProp(CString Name, UINT Data, WORD &PropID, CTypeDef &Type)
{
	for( UINT n = 1; n <= elements(m_pTagProps); n++ ) {

		if( Name == m_pTagProps[n-1] ) {

			if( n >= 1 && n <= 5 ) {

				Type.m_Type  = typeString;

				Type.m_Flags = 0;
			}

			if( n >= 6 && n <= 8 || n == 17 ) {

				Type.m_Type  = Data;

				Type.m_Flags = flagTagRef;
			}

			if( n >= 9 && n <= 10 ) {

				Type.m_Type  = typeInteger;

				Type.m_Flags = 0;
			}

			if( n >= 11 && n <= 11 ) {

				Type.m_Type  = typeString;

				Type.m_Flags = 0;
			}

			if( n >= 12 && n <= 13 || n == 16 ) {

				Type.m_Type  = typeInteger;

				Type.m_Flags = 0;
			}

			PropID = WORD(n);

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNameServer::NameTagProp(WORD PropID, CString &Name)
{
	if( --PropID < elements(m_pTagProps) ) {

		Name = m_pTagProps[PropID];

		return TRUE;
	}

	return FALSE;
}

// Function Category

BOOL CNameServer::FindCategory(WORD ID, CString &Cat)
{
	if( m_CatMap.FindName(ID) ) {

		Cat = m_CatMap[ID];

		return TRUE;
	}

	return FALSE;
}

// Initialization

void CNameServer::AddFuncsCamera(CError &Error)
{
	AddFunc(Error, CString(IDS_BANNER_CAMERA), 0x209E, L"int GetCameraData(class CPort Port, int Camera, int Param) const");
//	AddFunc(Error, CString(IDS_BANNER_CAMERA), 0x209F, L"int GetStreamData(class CPort Port, int Param) const");
	AddFunc(Error, CString(IDS_BANNER_CAMERA), 0x20A0, L"int SaveCameraSetup(class CPort Port, int Camera, int Index, cstring File)");
	AddFunc(Error, CString(IDS_BANNER_CAMERA), 0x20A1, L"int LoadCameraSetup(class CPort Port, int Camera, int Index, cstring File)");
	AddFunc(Error, CString(IDS_BANNER_CAMERA), 0x20A2, L"int UseCameraSetup(class CPort Port, int Camera, int Index)");
}

void CNameServer::AddFuncsCommsData(CError &Error)
{
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x203B, L"void Fill(numeric [] Data, numeric Data, int Count)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x203D, L"void Copy(numeric [] Dest, numeric [const] Src, int Count)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x2050, L"void ReadData(numeric [const] Data, int Count)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x205E, L"void ReadData(cstring [const] Data, int Count)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x2074, L"void Set(int   &Dest, int   Data)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x2075, L"void Set(float &Dest, float Data)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x207F, L"int WaitData(numeric [const] Data, int Count, int Time)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x2081, L"int WriteAll(void)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x20A8, L"int IsWriteQueueEmpty(class CDevice Dev) const");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x20A9, L"void EmptyWriteQueue(class CDevice Dev)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x20B1, L"void ForceCopy(numeric [] Dest, numeric [const] Src, int Count)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x20B3, L"void Force(int   &Dest, int   Data)");
	AddFunc(Error, CString(IDS_COMMS_DATA), 0x20B4, L"void Force(float &Dest, float Data)");
}

void CNameServer::AddFuncsCommsDevice(CError &Error)
{
	AddFunc(Error, CString(IDS_COMMS_DEVICE), 0x2064, L"void EnableDevice(class CDevice Device)");
	AddFunc(Error, CString(IDS_COMMS_DEVICE), 0x2065, L"void DisableDevice(class CDevice Device)");
	AddFunc(Error, CString(IDS_COMMS_DEVICE), 0x2068, L"void ControlDevice(class CDevice Device, int Enable)");
	AddFunc(Error, CString(IDS_COMMS_DEVICE), 0x207B, L"int DevCtrl(class CDevice Device, int Function, cstring Value)");
	AddFunc(Error, CString(IDS_COMMS_PORT),   0x207A, L"int DrvCtrl(class CPort Port, int Function, cstring Value)");
}

void CNameServer::AddFuncsCommsStatus(CError &Error)
{
	AddFunc(Error, CString(IDS_COMMS_DEVICE), 0x2069, L"int IsDeviceOnline(class CDevice Device) const");
	AddFunc(Error, CString(IDS_COMMS_DEVICE), 0x20D7, L"int GetDeviceStatus(class CDevice Device) const");
}

void CNameServer::AddFuncsCommsPort(CError &Error)
{
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x200C, L"void PortWrite(class CPort Port, int Data)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x200D, L"void PortPrint(class CPort Port, cstring Data)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x200E, L"int  PortRead(class CPort Port, int Time)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x2014, L"cstring PortInput(class CPort Port, int Start, int End, int Timeout, int Length)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x2056, L"void PortClose(class CPort Port)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x20A6, L"void PortSetRTS(class CPort Port, int State)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x20A7, L"int PortGetCTS(class CPort Port) const");

//	AddFunc(Error, CString(IDS_COMMS_PORT),	0x2087, L"void SetPortConfig(class CPort Port, int Param, int Value)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x2090, L"int GetPortConfig(class CPort Port, int Param) const");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x2139, L"void PortPrintEx(class CPort Port, cstring Data)");
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x213E, L"void PortSendData(class CPort Port, int [] Data, int Count)");
}

void CNameServer::AddFuncsCAN(CError &Error)
{
	AddFunc(Error, CString(IDS_CAN_PORT), 0x212E, L"int RxCANInit(class CPort Port, int ID, int DLC)");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x212F, L"int TxCANInit(class CPort Port, int ID, int DLC)");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x2130, L"int RxCAN(class CPort Port, int [] Data, int ID)");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x2131, L"int TxCAN(class CPort Port, int [] Data, int ID)");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x2132, L"int InitRxCANMailBox(class CPort Port, int MailBox, int IdMask, int IdFilter, int DLC)");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x2133, L"int InitTxCANMailBox(class CPort Port, int MailBox, int Id, int DLC)");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x2134, L"cstring RxCANMail(class CPort Port) const");
	AddFunc(Error, CString(IDS_CAN_PORT), 0x2135, L"int TxCANMail(class CPort Port, int Mailbox, numeric [const] Data)");
}

void CNameServer::AddFuncsDrive(CError &Error)
{
	AddFunc(Error, CString(IDS_DRIVE), 0x2041, L"int FormatCompactFlash(void)");
	AddFunc(Error, CString(IDS_DRIVE), 0x2053, L"int CompactFlashStatus(void) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x2054, L"void CompactFlashEject(void)");
	AddFunc(Error, CString(IDS_DRIVE), 0x2077, L"int CreateDirectory(cstring Name)");
	AddFunc(Error, CString(IDS_DRIVE), 0x207E, L"int DeleteDirectory(cstring Name)");
	AddFunc(Error, CString(IDS_DRIVE), 0x208D, L"int GetDiskFreeBytes(int Drive) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x208E, L"int GetDiskFreePercent(int Drive) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x208F, L"int GetDiskSizeBytes(int Drive) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x20B6, L"int CopyFiles(cstring Source, cstring Target, int Flags)");
	AddFunc(Error, CString(IDS_DRIVE), 0x20B7, L"int MoveFiles(cstring Source, cstring Target, int Flags)");
	AddFunc(Error, CString(IDS_DRIVE), 0x20B8, L"int GetDriveStatus(int Drive) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x20B9, L"void EjectDrive(int Drive)");
	AddFunc(Error, CString(IDS_DRIVE), 0x20BA, L"void FormatDrive(int Drive)");
	AddFunc(Error, CString(IDS_DRIVE), 0x20C3, L"cstring GetAutoCopyStatusText(void) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x20C4, L"int GetAutoCopyStatusCode(void) const");
	AddFunc(Error, CString(IDS_DRIVE), 0x20C6, L"int KillDirectory(cstring Name)");
}

void CNameServer::AddFuncsEvents(CError &Error)
{
	AddFunc(Error, CString(IDS_EVENTS), 0x205D, L"void ClearEvents(void)");
	AddFunc(Error, CString(IDS_EVENTS), 0x2094, L"void AlarmAccept(int Alarm)");
	AddFunc(Error, CString(IDS_EVENTS), 0x2095, L"void AlarmAcceptAll(void)");
	AddFunc(Error, CString(IDS_EVENTS), 0x20EB, L"void AlarmAcceptTag(int Tag, int Index, int Event)");
	AddFunc(Error, CString(IDS_EVENTS), 0x20EC, L"void AlarmAcceptEx(int Source, int Method, int Code)");
	AddFunc(Error, CString(IDS_EVENTS), 0x20C8, L"int GetLastEventTime(int All) const");
	AddFunc(Error, CString(IDS_EVENTS), 0x20C9, L"cstring GetLastEventText(int All) const");
	AddFunc(Error, CString(IDS_EVENTS), 0x20CA, L"cstring GetLastEventType(int All) const");
	AddFunc(Error, CString(IDS_EVENTS), 0x2100, L"int GetAlarmTag(int Tag) const");
}

void CNameServer::AddFuncsFile(CError &Error)
{
	AddFunc(Error, CString(IDS_FILE), 0x206A, L"cstring FindFileFirst(cstring Name)");
	AddFunc(Error, CString(IDS_FILE), 0x206B, L"cstring FindFileNext(void)");
	AddFunc(Error, CString(IDS_FILE), 0x206C, L"int OpenFile(cstring Name, int Mode)");
	AddFunc(Error, CString(IDS_FILE), 0x206D, L"void CloseFile(int File)");
	AddFunc(Error, CString(IDS_FILE), 0x206E, L"cstring ReadFileLine(int File)");
	AddFunc(Error, CString(IDS_FILE), 0x2072, L"int WriteFileLine(int File, cstring Text)");
	AddFunc(Error, CString(IDS_FILE), 0x2073, L"int CreateFile(cstring Name)");
	AddFunc(Error, CString(IDS_FILE), 0x2076, L"int DeleteFile(int File)");
	AddFunc(Error, CString(IDS_FILE), 0x2082, L"int WriteFile(int File, cstring Text)");
	AddFunc(Error, CString(IDS_FILE), 0x2083, L"cstring ReadFile(int File, int Chars)");
	AddFunc(Error, CString(IDS_FILE), 0x2091, L"int RenameFile(int File, cstring Name)");
	AddFunc(Error, CString(IDS_FILE), 0x20AB, L"int PutFileByte(int File, int Data)");
	AddFunc(Error, CString(IDS_FILE), 0x20AC, L"int GetFileByte(int File)");
	AddFunc(Error, CString(IDS_FILE), 0x20AD, L"int PutFileData(int File, int [] Data, int Length)");
	AddFunc(Error, CString(IDS_FILE), 0x20AE, L"int GetFileData(int File, int [] Data, int Length)");
	AddFunc(Error, CString(IDS_FILE), 0x20AF, L"int FileTell(int File) const");
	AddFunc(Error, CString(IDS_FILE), 0x20B0, L"int FileSeek(int File, int Pos)");
}

void CNameServer::AddFuncsFTP(CError &Error)
{
	AddFunc(Error, CString(IDS_FTP), 0x2096, L"int FtpPutFile(int Server, cstring Loc, cstring Rem, int Delete)");
	AddFunc(Error, CString(IDS_FTP), 0x2097, L"int FtpGetFile(int Server, cstring Loc, cstring Rem, int Delete)");
}

void CNameServer::AddFuncsLoggging(CError &Error)
{
	if( C3OemFeature(L"OemSD", FALSE) ) {

		AddFunc(Error, CString(IDS_LOGGING), 0x20E0, L"void NewBatchEx(int Slot, cstring Name)");
		AddFunc(Error, CString(IDS_LOGGING), 0x20E1, L"void EndBatchEx(int Slot)");
		AddFunc(Error, CString(IDS_LOGGING), 0x20E2, L"cstring GetBatchEx(int Slot) const");
		AddFunc(Error, CString(IDS_LOGGING), 0x20F1, L"int IsBatchNameValidEx(int Set, cstring Name)");
	}

	AddFunc(Error, CString(IDS_LOGGING), 0x2084, L"void NewBatch(cstring Name)");
	AddFunc(Error, CString(IDS_LOGGING), 0x2085, L"void EndBatch(void)");
	AddFunc(Error, CString(IDS_LOGGING), 0x2086, L"cstring GetBatch(void) const");
	AddFunc(Error, CString(IDS_LOGGING), 0x20F0, L"int IsBatchNameValid(cstring Name)");
	AddFunc(Error, CString(IDS_LOGGING), 0x20F5, L"void LogBatchComment(int Set, cstring Text)");
	AddFunc(Error, CString(IDS_LOGGING), 0x20F6, L"void LogBatchHeader(int Set, cstring Text)");
	AddFunc(Error, CString(IDS_LOGGING), 0x2093, L"void LogSave(void)");
	AddFunc(Error, CString(IDS_LOGGING), 0x20C5, L"void LogComment(class CLog Log, cstring Text)");
	AddFunc(Error, CString(IDS_LOGGING), 0x20C7, L"int IsLoggingActive(void) const");
	AddFunc(Error, CString(IDS_LOGGING), 0x20EF, L"void LogHeader(class CLog Log, cstring Text)");
}

void CNameServer::AddFuncsMail(CError &Error)
{
	AddFunc(Error, CString(IDS_MAIL), 0x205F, L"void SendMail(int Rcpt, cstring Subject, cstring Body)");
	AddFunc(Error, CString(IDS_MAIL), 0x208C, L"void SendFile(int Rcpt, cstring File)");
	AddFunc(Error, CString(IDS_MAIL), 0x20FA, L"void SendFileEx(int Rcpt, cstring File, cstring Subject, int Flag)");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x20F9, L"int ResolveDNS(cstring Name) const");
	AddFunc(Error, CString(IDS_MAIL), 0x205F, L"void SendMail(int Rcpt, cstring Subject, cstring Body)");
	AddFunc(Error, CString(IDS_MAIL), 0x2148, L"void SendMailTo(cstring Rcpt, cstring Subject, cstring Body)");
	AddFunc(Error, CString(IDS_MAIL), 0x2149, L"void SendFileTo(cstring Rcpt, cstring File)");
	AddFunc(Error, CString(IDS_MAIL), 0x214A, L"void SendMailToAck(cstring Rcpt, cstring Subject, cstring Body, int &Done)");
	AddFunc(Error, CString(IDS_MAIL), 0x214B, L"void SendFileToAck(cstring Rcpt, cstring File, int &Done)");
}

void CNameServer::AddFuncsMath(CError &Error)
{
	AddFunc(Error, CString(IDS_MATH), 0x1000, L"numeric Min(numeric a, numeric b) const");
	AddFunc(Error, CString(IDS_MATH), 0x1002, L"numeric Max(numeric a, numeric b) const");
	AddFunc(Error, CString(IDS_MATH), 0x1004, L"numeric Abs(numeric a) const");
	AddFunc(Error, CString(IDS_MATH), 0x1006, L"numeric Sgn(numeric a) const");
	AddFunc(Error, CString(IDS_MATH), 0x2006, L"float Pi(void) const");
	AddFunc(Error, CString(IDS_MATH), 0x2007, L"float Deg2Rad(float theta) const");
	AddFunc(Error, CString(IDS_MATH), 0x2008, L"float Rad2Deg(float theta) const");
	AddFunc(Error, CString(IDS_MATH), 0x2009, L"float cos(float theta) const");
	AddFunc(Error, CString(IDS_MATH), 0x200A, L"float sin(float theta) const");
	AddFunc(Error, CString(IDS_MATH), 0x200B, L"float tan(float theta) const");
	AddFunc(Error, CString(IDS_MATH), 0x2015, L"float Sqrt(numeric Data) const");
	AddFunc(Error, CString(IDS_MATH), 0x2017, L"numeric Power(numeric b, numeric e) const");
	AddFunc(Error, CString(IDS_MATH), 0x2030, L"int Random(int Range) const");
	AddFunc(Error, CString(IDS_MATH), 0x2033, L"numeric Sum(numeric [const] Data, int Count) const");
	AddFunc(Error, CString(IDS_MATH), 0x2035, L"float Mean(numeric [const] Data, int Count) const");
	AddFunc(Error, CString(IDS_MATH), 0x2037, L"float StdDev(numeric [const] Data, int Count) const");
	AddFunc(Error, CString(IDS_MATH), 0x2039, L"float PopDev(numeric [const] Data, int Count) const");
	AddFunc(Error, CString(IDS_MATH), 0x203F, L"float MakeFloat(int Data) const");
	AddFunc(Error, CString(IDS_MATH), 0x2040, L"int MakeInt(float Data) const");
	AddFunc(Error, CString(IDS_MATH), 0x2044, L"float acos(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x2045, L"float asin(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x2046, L"float atan(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x2047, L"float atan2(float a, float b) const");
	AddFunc(Error, CString(IDS_MATH), 0x2048, L"float log(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x2049, L"float exp(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x204A, L"float log10(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x204B, L"float exp10(float a) const");
	AddFunc(Error, CString(IDS_MATH), 0x2062, L"int MulDiv(int a, int b, int c) const");
	AddFunc(Error, CString(IDS_MATH), 0x2063, L"int Scale(int Data, int r1, int r2, int e1, int e2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2101, L"int MulU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2102, L"void MulR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x2105, L"void AddR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x2106, L"void SubR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x2107, L"void DivR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x2108, L"void PowR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x2109, L"void MaxR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x210A, L"void MinR64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x210B, L"void AbsR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x210C, L"void SqrtR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x210D, L"void sinR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x210E, L"void cosR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x210F, L"void tanR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2110, L"void asinR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2111, L"void acosR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2112, L"void atanR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2113, L"void atan2R64(int [] Result, int [] Tag1, int [] Tag2)");
	AddFunc(Error, CString(IDS_MATH), 0x2114, L"void expR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2115, L"void exp10R64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2116, L"void logR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2117, L"void log10R64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2118, L"int AddU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2119, L"int SubU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x211A, L"int DivU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x211B, L"int CompU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x211C, L"int MaxU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x211D, L"int MinU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x211E, L"int RShU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x211F, L"int ModU32(int Tag1, int Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2120, L"int GreaterR64(int [const] Tag1, int [const] Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2121, L"int LessR64(int [const] Tag1, int [const] Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2122, L"int GreaterEqR64(int [const] Tag1, int [const] Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2123, L"int LessEqR64(int [const] Tag1, int [const] Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2124, L"int EqualR64(int [const] Tag1, int [const] Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2125, L"int NotEqualR64(int [const] Tag1, int [const] Tag2) const");
	AddFunc(Error, CString(IDS_MATH), 0x2126, L"void MinusR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2127, L"void IncR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2128, L"void DecR64(int [] Result, int [] Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x2129, L"void IntToR64(int [] Result, int Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x212A, L"void RealToR64(int [] Result, float Tag1)");
	AddFunc(Error, CString(IDS_MATH), 0x212B, L"int R64ToInt(int [const] Tag1) const");
	AddFunc(Error, CString(IDS_MATH), 0x212C, L"float R64ToReal(int [const] Tag1) const");
}

void CNameServer::AddFuncsNetworkPort(CError &Error)
{
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x206F, L"cstring GetNetId(int Port) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x2079, L"int TextToAddr(cstring Addr) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x2070, L"cstring GetNetIp(int Port) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x2071, L"cstring GetInterfaceStatus(int Face) const");

	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x215A, L"cstring GetModemProperty(int n, cstring Prop) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x215B, L"cstring GetWifiProperty(int n, cstring Prop) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x215C, L"float GetLocationProperty(int n, cstring Prop) const");

//	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x2078, L"void SetNetConfig(class CPort Port, int Addr, int Mask, int Gate)");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x207C, L"cstring GetNetMask(int Port) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x207D, L"cstring GetNetGate(int Port) const");
	AddFunc(Error, CString(IDS_NETWORK_PORT), 0x213D, L"int NetworkPing(int IPAddr, int Timeout) const");
}

void CNameServer::AddFuncsRemote(CError &Error)
{
	AddFunc(Error, CString(IDS_COMMS_PORT), 0x20BB, L"int IsPortRemote(class CPort Port) const");
}

void CNameServer::AddFuncsStrings(CError &Error)
{
	AddFunc(Error, CString(IDS_STRINGS), 0x200F, L"cstring Left(cstring Text, int Count) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2010, L"cstring Right(cstring Text, int Count) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2011, L"cstring Mid(cstring Text, int Pos, int Count) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2012, L"int TextToInt(cstring Text, int Radix) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2013, L"cstring IntToText(int Data, int Radix, int Digits) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x202F, L"int Len(cstring Text) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x204C, L"float TextToFloat(cstring Text) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x204D, L"int Find(cstring Text, int Char, int Skip) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2057, L"cstring DataToText(int [const] Data, int Limit) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2058, L"cstring Strip(cstring Text, int Target) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2066, L"cstring DecToText(numeric Data, int Signed, int Before, int After, int Leading, int Group) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x20D8, L"cstring AsText(numeric n) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2103, L"cstring AsTextR64(int [const] Data) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2138, L"cstring AsTextR64WithFormat(cstring Format, int [const] Data) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x2104, L"void TextToR64(cstring Input, int [] Output)");
	AddFunc(Error, CString(IDS_STRINGS), 0x214C, L"cstring AsTextL64(int [const] Data, int Radix, int Digits) const");
	AddFunc(Error, CString(IDS_STRINGS), 0x214D, L"void TextToL64(Cstring Input, int [] Output, int Radix)");
}

void CNameServer::AddFuncsSystem(CError &Error)
{
	AddFunc(Error, CString(IDS_SYSTEM), 0x20DB, L"cstring GetModelName(int g) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2147, L"cstring GetSerialNumber(void) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20D0, L"int GetVersionInfo(int p) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20D1, L"cstring GetRestartInfo(int n) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20D2, L"cstring GetRestartCode(int n) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20D3, L"cstring GetRestartText(int n) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20D4, L"int GetRestartTime(int n) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20E3, L"int SaveConfigFile(cstring File)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20EA, L"int IsBatteryLow(void) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20F8, L"int EnumOptionCard(int s) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20FB, L"void MountCompactFlash(int m)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x20FC, L"void EnableBatteryCheck(int f)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2137, L"void SetIconLed(int n, int s)");

	AddFunc(Error, CString(IDS_SYSTEM), 0x214E, L"void DebugPrint(cstring s)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x214F, L"void DebugStrackTrace(void)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2150, L"void DebugDumpLocals(void)");

	AddFunc(Error, CString(IDS_SYSTEM), 0x2152, L"cstring GetPersonalityString(cstring name) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2153, L"int GetPersonalityInt(cstring name) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2154, L"float GetPersonalityFloat(cstring name) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2155, L"int GetPersonalityIp(cstring name) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2156, L"void SetPersonalityString(cstring name, cstring data)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2157, L"void SetPersonalityInt(cstring name, int data)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2158, L"void SetPersonalityFloat(cstring name, float data)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2159, L"void SetPersonalityIp(cstring name, int data)");
	AddFunc(Error, CString(IDS_SYSTEM), 0x2160, L"void CommitPersonality(int reboot)");

	AddFunc(Error, CString(IDS_SYSTEM), 0x215E, L"int  GetSystemIo(cstring Prop) const");
	AddFunc(Error, CString(IDS_SYSTEM), 0x215F, L"void SetSystemIo(cstring Prop, int data)");
}

void CNameServer::AddFuncsTagData(CError &Error)
{
	AddFunc(Error, CString(IDS_TAG_DATA), 0x2098, L"int GetIntTag(int Index) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x2099, L"float GetRealTag(int Index) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x209A, L"cstring GetStringTag(int Index) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x209B, L"cstring GetFormattedTag(int Index) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x209C, L"cstring GetTagLabel(int Index) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x209D, L"int FindTagIndex(cstring Label) const");

//	AddFunc(Error, CString(IDS_TAG_DATA), 0x20A3, L"int GetAlarmTag(int Index) const");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x20A4, L"void SetIntTag(int Index, int Data)");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x20A5, L"void SetRealTag(int Index, float Data)");
	AddFunc(Error, CString(IDS_TAG_DATA), 0x20B5, L"void SetStringTag(int Index, cstring Data)");
}

void CNameServer::AddFuncsTime(CError &Error)
{
	AddFunc(Error, CString(IDS_TIME), 0x2020, L"int Date(int y, int m, int d) const");
	AddFunc(Error, CString(IDS_TIME), 0x2021, L"int Time(int h, int m, int s) const");
	AddFunc(Error, CString(IDS_TIME), 0x2022, L"int GetYear(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2023, L"int GetMonth(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2024, L"int GetDate(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2025, L"int GetDays(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2026, L"int GetWeeks(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2027, L"int GetDay(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2028, L"int GetWeek(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x2029, L"int GetWeekYear(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x202A, L"int GetHour(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x202B, L"int GetMin(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x202C, L"int GetSec(int t) const");
	AddFunc(Error, CString(IDS_TIME), 0x202D, L"int GetMonthDays(int y, int m) const");
	AddFunc(Error, CString(IDS_TIME), 0x202E, L"int GetNow(void) const");
	AddFunc(Error, CString(IDS_TIME), 0x205A, L"int GetNowTime(void) const");
	AddFunc(Error, CString(IDS_TIME), 0x205B, L"int GetNowDate(void) const");
	AddFunc(Error, CString(IDS_TIME), 0x205C, L"int SetNow(int Time)");
}

void CNameServer::AddFuncsUI(CError &Error)
{
	AddFunc(Error, CString(IDS_UI), 0x2005, L"void Sleep(int Time)");
	AddFunc(Error, CString(IDS_UI), 0x2059, L"void StopSystem(void)");
	AddFunc(Error, CString(IDS_UI), 0x2004, L"void Nop(void)");
	AddFunc(Error, CString(IDS_UI), 0x2088, L"void CommitAndReset(void)");
	AddFunc(Error, CString(IDS_UI), 0x2151, L"void StopRTTTL(void)");

	if( UnsafeCommitsAllowed() ) {

		AddFunc(Error, CString(IDS_UI), 0x212D, L"void CommitAndVoidWarranty(void)");
	}
}

void CNameServer::AddFuncsWeb(CError &Error)
{
	AddFunc(Error, CString(IDS_WEB), 0x20FD, L"int     GetWebParamInt(cstring Name) const");
	AddFunc(Error, CString(IDS_WEB), 0x20FE, L"int     GetWebParamHex(cstring Name) const");
	AddFunc(Error, CString(IDS_WEB), 0x20FF, L"cstring GetWebParamStr(cstring Name) const");
	AddFunc(Error, CString(IDS_WEB), 0x213F, L"cstring GetWebUser(int n) const");
	AddFunc(Error, CString(IDS_WEB), 0x2140, L"void    ClearWebUsers(void)");
}

void CNameServer::AddFuncsSQL(CError &Error)
{
	AddFunc(Error, CString(IDS_SQL), 0x2136, L"int     ForceSQLSync(void)");
	AddFunc(Error, CString(IDS_SQL), 0x213A, L"int     IsSQLSyncRunning(void) const");
	AddFunc(Error, CString(IDS_SQL), 0x213B, L"int     GetLastSQLSyncStatus(void) const");
	AddFunc(Error, CString(IDS_SQL), 0x213C, L"int     GetLastSQLSyncTime(int Type) const");
	AddFunc(Error, CString(IDS_SQL), 0x2142, L"void    RunQuery(cstring Name)");
	AddFunc(Error, CString(IDS_SQL), 0x2143, L"void    RunAllQueries(void)");
	AddFunc(Error, CString(IDS_SQL), 0x2144, L"int     GetSQLConnectionStatus(void) const");
	AddFunc(Error, CString(IDS_SQL), 0x2145, L"int     GetQueryTime(cstring Name) const");
	AddFunc(Error, CString(IDS_SQL), 0x2146, L"int     GetQueryStatus(cstring Name) const");
}

void CNameServer::AddFuncsHoneywell(CError &Error)
{
	AddFunc(Error, CString(IDS_HONEYWELL), 0x20ED, L"void    HonFindProf(int TimeUnits, int RampType, int [] Ramp, float [] Time, float [] Data, float [] AuxD, int [] XPos, float [] YPos, float [] APos, float [] BPos, int &CountX, int &CountA, int &Limit)");
	AddFunc(Error, CString(IDS_HONEYWELL), 0x20EE, L"int     HonFindTime(int TimeUnits, int RampType, int [] Ramp, float [] Time, float [] Data, float Segment, float Remain) const");
	AddFunc(Error, CString(IDS_HONEYWELL), 0x20F2, L"cstring HonGetLogParamS(int n, int i) const");
	AddFunc(Error, CString(IDS_HONEYWELL), 0x20F3, L"int     HonGetLogParamI(int n, int i) const");
	AddFunc(Error, CString(IDS_HONEYWELL), 0x20F4, L"void    HonSetLogParamI(int n, int i, int v)");
	AddFunc(Error, CString(IDS_HONEYWELL), 0x20F7, L"int     HonGetLogParamAvail(void) const");
}

void CNameServer::AddFuncsControl(CError &Error)
{
	AddFunc(Error, CString(IDS_COMMS_OPT_DONGLE), 0x2141, L"cstring GetLicenseState(int type) const");
}

void CNameServer::AddVarsAlarms(CError &Error)
{
	m_pIdentLib->AddIdent(Error, this, 0x7C07, L"const int ActiveAlarms");
	m_pIdentLib->AddIdent(Error, this, 0x7C0C, L"const int UnacceptedAlarms");
	m_pIdentLib->AddIdent(Error, this, 0x7C0F, L"const int UnacceptedAndAutoAlarms");
}

void CNameServer::AddVarsSystem(CError &Error)
{
	m_pIdentLib->AddIdent(Error, this, 0x7C02, L"const float Pi");
	m_pIdentLib->AddIdent(Error, this, 0x7C05, L"const int CommsError");
}

void CNameServer::AddVarsTime(CError &Error)
{
	m_pIdentLib->AddIdent(Error, this, 0x7C08, L"int UseDST");
	m_pIdentLib->AddIdent(Error, this, 0x7C09, L"int TimeZone");
	m_pIdentLib->AddIdent(Error, this, 0x7C0A, L"int TimeZoneMins");
	m_pIdentLib->AddIdent(Error, this, 0x7C0D, L"int TimeNow");
}

void CNameServer::AddFunc(CError &Error, CString Cat, WORD ID, CString Func)
{
	m_CatMap.Insert(ID, Cat);

	m_pFuncLib->AddFunc(Error, this, ID, Func);
}

// Implementation

BOOL CNameServer::FindStd(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( m_pIdentLib ) {

		if( m_pIdentLib->FindIdent(pError, Name, ID, Type) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNameServer::FindTag(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( m_uLen ) {

		CString Full = m_Root + Name;

		if( FindOne(pError, Full, ID, Type) ) {

			return TRUE;
		}
	}

	if( FindOne(pError, Name, ID, Type) ) {

		return TRUE;
	}

	if( pError->AllowUI() ) {

		if( Type.m_Type < typeObject ) {

			UINT Size = 0;

			if( Type.m_Flags & flagArray ) {

				Size = 16;
			}

			if( !pError->GetFlag(L"AutoCreate") ) {

				CItemCreateDialog Dlg(Name,
						      Type.m_Type,
						      Size
				);

				switch( Dlg.Execute() ) {

					case 0:
						pError->SetFlag(L"Shown");

						return FALSE;

					case 2:
						pError->SetFlag(L"AutoCreate");

						break;
				}

				Type.m_Type = Dlg.GetType();
			}
			else {
				if( Type.m_Type == typeVoid ) {

					UINT Last   = CItemCreateDialog::GetLastType();

					Type.m_Type = Last;
				}
			}

			if( m_pSystem->m_pTags->CreateTag(Name, Type.m_Type, Size) ) {

				if( FindOne(pError, Name, ID, Type) ) {

					return TRUE;
				}
			}
			else {
				CWnd &Wnd = CWnd::GetActiveWindow();

				Wnd.Error(CString(IDS_TAG_COULD_NOT_BE));

				pError->SetFlag(L"Shown");
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::FindOne(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	CDataRef &Ref = (CDataRef &) ID;

	UINT     uPos = 0;

	if( (uPos = m_pTags->FindNamePos(Name)) < NOTHING ) {

		CTag *pTag = m_pTags->GetItem(uPos);

		if( pTag->IsKindOf(AfxRuntimeClass(CDataTag)) ) {

			Ref.t.m_IsTag = 1;

			Ref.t.m_Index = uPos;

			Type.m_Type   = pTag->GetDataType();

			Type.m_Flags  = pTag->GetTypeFlags();
		}
		else {
			Ref.t.m_IsTag = 1;

			Ref.t.m_Index = uPos;

			Type.m_Type   = typeFolder;

			Type.m_Flags  = 0;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CNameServer::FindPort(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( Type.m_Type == typePort ) {

		CCommsPort *pPort = m_pComms->FindPort(Name);

		if( pPort ) {

			CDataRef &Ref = (CDataRef &) ID;

			Ref.t.m_IsTag = 0;

			Ref.t.m_Index = MAKEWORD(pPort->m_Number, typePort);

			Type.m_Type   = typePort;

			Type.m_Flags  = flagConstant;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNameServer::FindDevice(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( Type.m_Type == typeDevice ) {

		CCommsDevice *pDevice = m_pComms->FindDevice(Name);

		if( pDevice && pDevice->GetDriver() ) {

			UINT uType = pDevice->GetDriver()->GetType();

			if( uType == driverMaster || uType == driverHoneywell ) {

				CDataRef &Ref = (CDataRef &) ID;

				Ref.t.m_IsTag = 0;

				Ref.t.m_Index = MAKEWORD(pDevice->m_Number, typeDevice);

				Type.m_Type   = typeDevice;

				Type.m_Flags  = flagConstant;

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::FindLog(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( Type.m_Type == typeLog ) {

		CDataLog *pLog = (CDataLog *) m_pLogs->FindByName(Name);

		if( pLog ) {

			UINT     uLog = pLog->GetIndex();

			CDataRef &Ref = (CDataRef &) ID;

			Ref.t.m_IsTag = 0;

			Ref.t.m_Index = MAKEWORD(uLog, typeLog);

			Type.m_Type   = typeLog;

			Type.m_Flags  = flagConstant;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNameServer::NameStd(WORD ID, CString &Name)
{
	if( m_pIdentLib ) {

		if( m_pIdentLib->NameIdent(ID, Name) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CNameServer::NameTag(WORD ID, CString &Name)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_IsTag ) {

		if( m_pIdentLib->NameIdent(WORD(ID & 0x7FFF), Name) ) {

			return TRUE;
		}
		else {
			CTag *pTag = m_pTags->GetItem(Ref.t.m_Index);

			if( pTag ) {

				Name = pTag->m_Name;

				if( m_uLen ) {

					if( Name.Left(m_uLen) == m_Root ) {

						Name = Name.Mid(m_uLen);
					}
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::NamePort(WORD ID, CString &Name)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.t.m_IsTag ) {

		UINT uData = Ref.t.m_Index;

		if( uData & 0x4000 ) {

			UINT uType = HIBYTE(uData);

			UINT uItem = LOBYTE(uData);

			if( uType == typePort ) {

				CCommsPort *pPort = m_pComms->FindPort(uItem);

				if( pPort ) {

					Name = pPort->GetShortName();

					return TRUE;
				}

				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::NameDevice(WORD ID, CString &Name)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.t.m_IsTag ) {

		UINT uData = Ref.t.m_Index;

		if( uData & 0x4000 ) {

			UINT uType = HIBYTE(uData);

			UINT uItem = LOBYTE(uData);

			if( uType == typeDevice ) {

				CCommsDevice *pDevice = m_pComms->FindDevice(uItem);

				if( pDevice ) {

					Name = pDevice->m_Name;

					return TRUE;
				}

				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::NameLog(WORD ID, CString &Name)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( !Ref.t.m_IsTag ) {

		UINT uData = Ref.t.m_Index;

		if( uData & 0x4000 ) {

			UINT uType = HIBYTE(uData);

			UINT uItem = LOBYTE(uData);

			if( uType == typeLog ) {

				CDataLog *pLog = m_pLogs->GetItem(uItem);

				if( pLog ) {

					Name = pLog->m_Name;

					return TRUE;
				}

				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CNameServer::UnsafeCommitsAllowed(void)
{
	return FALSE;
}

// End of File
