
#include "Intern.hpp"

#include "AppObjectClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "OpcIds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObjectClient(PCSTR pName)
{
	return (IClientProcess *) New CAppObjectClient;
	}

// Constructor

CAppObjectClient::CAppObjectClient(void)
{
	StdSetRef();
	}

// Destructor

CAppObjectClient::~CAppObjectClient(void)
{
	}

// IUnknown

HRESULT CAppObjectClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CAppObjectClient::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObjectClient::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObjectClient::TaskInit(UINT uTask)
{
	SetThreadName("OpcUaClient");

	AfxNewObject("opcua-client", IOpcUaClient, m_pClient);

	COpcUaClientConfig Config;

	Config.m_uSize  = sizeof(Config);
	Config.m_uDebug = 1;

	m_pClient->OpcLoad(Config);

	return m_pClient->OpcInit();
	}

INT CAppObjectClient::TaskExec(UINT uTask)
{
	AfxTrace("TaskExec!\n");

	CStringArray Nodes;

	Nodes.Append("ns=2;i=0");

	UINT uDevice;

	for(;;) {
	
		if( (uDevice = m_pClient->OpcOpenDevice("opc.tcp://192.168.1.10:4096", "", "", Nodes)) ) {

			CStringArray List;

			if( m_pClient->OpcBrowseDevice(uDevice, List) ) {

				AfxTrace("\n%u Nodes:\n\n", List.GetCount());

				for( UINT n = 0; n < List.GetCount(); n++ ) {

					AfxTrace("  %s\n", PCTXT(List[n]));
					}

				AfxTrace("\n\n");
				}

			for(;;) {

				UINT  id  [1] = { 0 };
		
				UINT  type[1] = { 1 };

				DWORD data[1] = { 0 };

				m_pClient->OpcReadData (uDevice, id, elements(id), type, data);

				AfxTrace("Data is %u\n", data[0]);

				data[0]++;

				m_pClient->OpcWriteData(uDevice, id, elements(id), type, data);

				if( rand() % 1000 == 500 ) {

					break;
					}
				}

			m_pClient->OpcCloseDevice(uDevice);
			}
		}

	return 0;
	}

void CAppObjectClient::TaskStop(UINT uTask)
{
	m_pClient->OpcStop();
	}

void CAppObjectClient::TaskTerm(UINT uTask)
{
	m_pClient->OpcTerm();

	m_pClient->Release();
	}

// End of File
