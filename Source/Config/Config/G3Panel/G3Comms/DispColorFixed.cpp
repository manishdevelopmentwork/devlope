
#include "Intern.hpp"

#include "DispColorFixed.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fixed Display Color
//

// Dynamic Class

AfxImplementDynamicClass(CDispColorFixed, CDispColor);

// Constructor

CDispColorFixed::CDispColorFixed(void)
{
	m_uType = 1;

	m_Color = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));
	}

// Color Access

DWORD CDispColorFixed::GetColorPair(DWORD Data, UINT Type)
{
	return m_Color;
	}

// Download Support

BOOL CDispColorFixed::MakeInitData(CInitData &Init)
{
	CDispColor::MakeInitData(Init);

	Init.AddLong(m_Color);

	return TRUE;
	}

// Meta Data Creation

void CDispColorFixed::AddMetaData(void)
{
	CDispColor::AddMetaData();

	Meta_AddInteger(Color);

	Meta_SetName((IDS_FIXED_COLOR_PAIR));
	}

// End of File
