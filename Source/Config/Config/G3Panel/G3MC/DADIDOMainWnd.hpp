
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOMainWnd_HPP

#define INCLUDE_DADIDOMainWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDADIDOModule;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO Module Window
//

class CDADIDOMainWnd : public CProxyViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDADIDOMainWnd(void);

protected:
	// Data Members
	CMultiViewWnd * m_pMult;
	CDADIDOModule * m_pItem;

	// Overridables
	void OnAttach(void);

	// Implementation
	void AddIdentPage(void);
	void AddDIPages(void);
	void AddDOPages(void);
};

// End of File

#endif
