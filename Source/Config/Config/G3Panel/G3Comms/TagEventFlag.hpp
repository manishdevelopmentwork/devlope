
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagEventFlag_HPP

#define INCLUDE_TagEventFlag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TagEvent.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Event
//

class DLLNOT CTagEventFlag : public CTagEvent
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagEventFlag(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL NeedSetpoint(void) const;
		BOOL HasSetpoint(void) const;
		BOOL NoLevelMode(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
