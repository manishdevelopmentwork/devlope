
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HTTPSTREAMWRITEFILEMD5_HPP

#define	INCLUDE_HTTPSTREAMWRITEFILEMD5_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpStreamWriteFile.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Http File Writable Stream with MD5 Hash
//

class DLLAPI CHttpStreamWriteFileMd5 : public CHttpStreamWriteFile, public IHttpStreamHash
{
	public:
		// Constructor
		CHttpStreamWriteFileMd5(CString const &Name, FILE *pFile);
		CHttpStreamWriteFileMd5(FILE *pFile);

		// Destructor
		~CHttpStreamWriteFileMd5(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IHttpStreamHash
		BOOL GetHash(CString &Hash);

		// IHttpStreamWrite
		BOOL Write(PCTXT pData, UINT uData);
		void Finalize(void);
		void Abort(void);

	protected:
		// Data Members
		ICryptoHash * m_pHash;
	};
	
// End of File

#endif
