
#include "Intern.hpp"

#include "Qutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Quick Mutex Object
//

// Instantiators

IUnknown * CExecutive::Create_Qutex(PCTXT pName)
{
	return New CQutex(m_pThis);
	}

// Constructor

CQutex::CQutex(CExecutive *pExec) : CMutex(pExec)
{
	}

// IWaitable

BOOL CQutex::Wait(UINT uWait)
{
	if( CExecutive::m_pThread ) {

		CExecutive::m_pThread->Guard(TRUE);
		}

	if( CMutex::Wait(uWait) ) {

		return TRUE;
		}

	if( CExecutive::m_pThread ) {

		CExecutive::m_pThread->Guard(FALSE);
		}

	return FALSE;
	}

// IMutex

void CQutex::Free(void)
{
	CMutex::Free();

	if( CExecutive::m_pThread ) {

		CExecutive::m_pThread->Guard(FALSE);
		}
	}

// End of File
