
#include "Intern.hpp"

#include "UsbHostOpen.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Open Host Controller Interface
//

// Constructor

CUsbHostOpen::CUsbHostOpen(void)
{
	m_pName	 = "Open Host Driver";
	}

// End of File
