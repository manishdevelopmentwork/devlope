
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Standard Tree Window
//

// Runtime Class

AfxImplementRuntimeClass(CStdTreeWnd, CViewWnd);

// Static Data

UINT CStdTreeWnd::m_timerDouble = AllocTimerID();

// Constructor

CStdTreeWnd::CStdTreeWnd(void)
{
	m_pTree   = New CMulTreeView;

	m_dwStyle = TVS_NOTOOLTIPS
		  | TVS_TRACKSELECT
		  | TVS_FULLROWSELECT
	          | TVS_HASBUTTONS;

	m_fInitRoot  = FALSE;

	m_fLoading   = FALSE;

	m_hSelect    = NULL;

	m_pSelect    = NULL;

	m_pNamed     = NULL;

	m_fMulti     = FALSE;

	m_LinkCursor.Create(L"LinkCursor");

	m_MoveCursor.Create(L"MoveCursor");

	m_CopyCursor.Create(L"CopyCursor");
	}

// IUnknown

HRESULT CStdTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropSource ) {

			*ppObject = (IDropSource *) this;

			return S_OK;
			}

		if( iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CStdTreeWnd::AddRef(void)
{
	return 1;
	}

ULONG CStdTreeWnd::Release(void)
{
	return 1;
	}

// IDropSource

HRESULT CStdTreeWnd::QueryContinueDrag(BOOL fEscape, DWORD dwKeys)
{
	if( !fEscape ) {

		if( dwKeys & MK_RBUTTON ) {

			return DRAGDROP_S_CANCEL;
			}

		if( dwKeys & MK_LBUTTON ) {

			return S_OK;
			}

		return DRAGDROP_S_DROP;
		}

	return DRAGDROP_S_CANCEL;
	}

HRESULT CStdTreeWnd::GiveFeedback(DWORD dwEffect)
{
	if( dwEffect == DROPEFFECT_LINK ) {

		SetCursor(m_LinkCursor);
		
		return S_OK;
		}

	if( dwEffect == DROPEFFECT_MOVE ) {

		SetCursor(m_MoveCursor);
		
		return S_OK;
		}

	if( dwEffect == DROPEFFECT_COPY ) {

		SetCursor(m_CopyCursor);
		
		return S_OK;
		}

	SetCursor(LoadCursor(NULL, IDC_NO));

	return S_OK;
	}

// IDropTarget

HRESULT CStdTreeWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CStdTreeWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CStdTreeWnd::DragLeave(void)
{
	m_DropHelp.DragLeave();

	return S_OK;
	}

HRESULT CStdTreeWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Overridables

void CStdTreeWnd::OnAttach(void)
{
	m_pItem = (CMetaItem *) CViewWnd::m_pItem;
	}

// Message Map

AfxMessageMap(CStdTreeWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_MOUSEACTIVATE)

	AfxDispatchNotify(100, TVN_SELCHANGED,     OnTreeSelChanged )
	AfxDispatchNotify(100, TVN_KEYDOWN,        OnTreeKeyDown    )
	AfxDispatchNotify(100, NM_CUSTOMDRAW,      OnTreeCustomDraw )
	AfxDispatchNotify(100, NM_RETURN,          OnTreeReturn     )
	AfxDispatchNotify(100, NM_DBLCLK,          OnTreeDblClk     )
	AfxDispatchNotify(100, TVN_BEGINDRAG,      OnTreeBeginDrag  )
	AfxDispatchNotify(100, TVN_ITEMEXPANDING,  OnTreeExpanding  )
	AfxDispatchNotify(100, TVN_CONTEXTMENU,    OnTreeContextMenu)
	AfxDispatchNotify(100, TVN_CHANGEMULTI,    OnTreeChangeMulti)
	AfxDispatchNotify(100, TVN_OFFERFOCUS,     OnTreeOfferFocus )

	AfxMessageEnd(CStdTreeWnd)
	};

// Accelerator

BOOL CStdTreeWnd::OnAccelerator(MSG &Msg)
{
	if( afxMainWnd->IsActive() ) {
		
		if( IsCurrent() ) {
		
			if( !m_pTree->InEditMode() ) {
			
				if( m_Accel2.Translate(Msg) ) {

					return TRUE;
					}
				}
			}
		}

	if( m_Accel1.Translate(Msg) ) {
		
		return TRUE;
		}

	return FALSE;
	}

// Message Handlers

void CStdTreeWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	m_pTree->Create(m_dwStyle, GetClientRect() - 1, ThisObject, 100);
	
	m_pTree->SetFont(afxFont(Dialog));

	LoadImageList();

	m_pTree->SetImageList(TVSIL_NORMAL, m_Images);

	m_pTree->SetImageList(TVSIL_STATE,  m_Images);

	m_fLoading = TRUE;

	LoadTree();

	m_fLoading = FALSE;

	SelectInit();

	m_DropHelp.Bind(m_hWnd, this);

	m_pTree->ShowWindow(SW_SHOW);
	}

void CStdTreeWnd::OnSize(UINT uCode, CSize Size)
{
	m_pTree->MoveWindow(GetClientRect() - 1, TRUE);
	}

void CStdTreeWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.FrameRect(Rect, afxBrush(WHITE));
	}

void CStdTreeWnd::OnSetFocus(CWnd &Wnd)
{
	m_pTree->SetFocus();
	}

UINT CStdTreeWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	return AfxCallDefProc();
	}

// Notification Handlers

void CStdTreeWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			m_hSelect = Info.itemNew.hItem;

			m_pSelect = (CMetaItem *) Info.itemNew.lParam;

			if( m_pSelect && m_pSelect->IsKindOf(AfxRuntimeClass(CMetaItem)) && m_pSelect->HasName() ) {

				m_pNamed = m_pSelect;
				}
			else
				m_pNamed = NULL;

			m_SelPath = m_pSelect ? m_pSelect->GetFixedPath() : L"";

			m_fMulti  = m_pTree->HasMultiple();

			NewItemSelected();
			}
		}
	}

BOOL CStdTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_F2:

			if( m_dwStyle & TVS_EDITLABELS ) {

				CCmdSourceData Source;

				Source.PrepareSource();

				RouteControl(IDM_ITEM_RENAME, Source);

				if( !(Source.GetFlags() & MF_DISABLED) ) {

					m_pTree->ClearExcept();

					m_pTree->EditLabel(m_hSelect);
					}
				}

			return TRUE;

		case VK_TAB:

			m_System.FlipToItemView(TRUE);

			return TRUE;			

		case VK_ESCAPE:

			afxMainWnd->PostMessage(WM_CANCELMODE);

			return TRUE;
		}

	return FALSE;
	}

UINT CStdTreeWnd::OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
		}

	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		NMTVCUSTOMDRAW &Extra = (NMTVCUSTOMDRAW &) Info;

		HTREEITEM       hItem = HTREEITEM(Info.dwItemSpec);

		if( hItem ) {
		
			UINT uTest = m_pTree->GetItemState(hItem, NOTHING);

			if( uTest & TVIS_DROPHILITED ) {

				Extra.clrTextBk = afxColor(Orange2);

				Extra.clrText   = afxColor(BLACK);

				SelectObject(Info.hdc, m_pTree->GetFont());

				return CDRF_NEWFONT;
				}
			}

		if( Info.uItemState & CDIS_HOT ) {

			if( Info.uItemState & CDIS_SELECTED ) {

				Extra.clrTextBk = afxColor(Orange2);
				}
			else
				Extra.clrTextBk = afxColor(Orange3);

			Extra.clrText = afxColor(BLACK);

			SelectObject(Info.hdc, m_pTree->GetFont());

			return CDRF_NEWFONT;
			}
		}

	return 0;
	}

void CStdTreeWnd::OnTreeReturn(UINT uID, NMHDR &Info)
{
	if( m_pTree->GetChild(m_hSelect) ) {

		ToggleItem(m_hSelect);

		return;
		}
	}

void CStdTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( m_pTree->GetChild(m_hSelect) ) {

			ToggleItem(m_hSelect);

			return;
			}
		}
	}

void CStdTreeWnd::OnTreeBeginDrag(UINT uID, NMTREEVIEW &Info)
{
	if( !m_pTree->GetMultiple() ) {

		HTREEITEM hItem = Info.itemNew.hItem;

		m_pTree->SelectItem(hItem);

		afxMainWnd->PostMessage(WM_CANCELMODE);

		DragItem();
		}
	else {
		afxMainWnd->PostMessage(WM_CANCELMODE);

		if( DragItem() ) {

			m_pTree->ClearExcept();
			}
		}
	}

BOOL CStdTreeWnd::OnTreeExpanding(UINT uID, NMTREEVIEW &Info)
{
	if( Info.itemNew.hItem == m_hRoot ) {

		if( Info.action == TVE_COLLAPSE ) {

			return TRUE;
			}

		return FALSE;
		}

	SetFolderImage(Info.itemNew.hItem, Info.action);

	return FALSE;
	}

void CStdTreeWnd::OnTreeContextMenu(UINT uID, NMTREEVIEW &Info)
{
	CString Name;

	if( Info.itemNew.hItem ) {

		GetItemMenu(Name, TRUE);
		}
	else
		GetItemMenu(Name, FALSE);

	if( !Name.IsEmpty() ) {

		CMenu   Load = CMenu(PCTXT(Name));

		CMenu & Menu = Load.GetSubMenu(0);

		Menu.SendInitMessage();

		Menu.DeleteDisabled();

		AddExtraCommands(Menu);

		if( Menu.GetMenuItemCount() ) {

			m_pTree->ClientToScreen(Info.ptDrag);

			Menu.MakeOwnerDraw(FALSE);

			Menu.TrackPopupMenu( TPM_LEFTALIGN,
					     Info.ptDrag,
					     afxMainWnd->GetHandle()
					     );

			Menu.FreeOwnerDraw();
			}
		}
	}

void CStdTreeWnd::OnTreeChangeMulti(UINT uID, NMTREEVIEW &Info)
{
	m_fMulti = m_pTree->HasMultiple();

	NewItemSelected();
	}

void CStdTreeWnd::OnTreeOfferFocus(UINT uID, NMTREEVIEW &Info)
{
	m_pTree->SetFocus();
	}

// Data Object Construction

BOOL CStdTreeWnd::MakeDataObject(IDataObject * &pData, BOOL fRich)
{
	return MakeDataObject(pData);
	}

BOOL CStdTreeWnd::MakeDataObject(IDataObject * &pData)
{
	return FALSE;
	}

// Drag Support

BOOL CStdTreeWnd::DragItem(void)
{
	IDataObject *pData = NULL;

	if( MakeDataObject(pData, TRUE) ) {

		m_System.LockResPane(TRUE);

		FindDragMetrics();

		MakeDragImage();

		CDragHelper *pHelp = New CDragHelper;

		pHelp->AddImage(pData, m_DragImage, m_DragSize, m_DragOffset);

		delete pHelp;

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, 0);

		m_pTree->UpdateWindow();

		DWORD dwAllow  = FindDragAllowed();

		DWORD dwResult = DROPEFFECT_NONE;

		DoDragDrop(pData, this, dwAllow, &dwResult);

		DragComplete(dwResult);

		m_pTree->SetWindowStyle(TVS_TRACKSELECT, m_dwStyle);

		pData->Release();

		m_System.LockResPane(FALSE);

		return TRUE;
		}

	m_System.LockResPane(FALSE);

	return FALSE;
	}

// Drag Hooks

void CStdTreeWnd::FindDragMetrics(void)
{
	CRect Rect = m_pTree->GetItemRect(m_hSelect, TRUE);

	m_pTree->ClientToScreen(Rect);

	Rect.left    = Rect.left - 16;

	m_DragPos    = GetCursorPos();

	m_DragSize   = Rect.GetSize();

	m_DragOffset = m_DragPos - Rect.GetTopLeft();

	MakeMin(m_DragOffset.cx, Rect.cx());

	MakeMin(m_DragOffset.cy, Rect.cy());

	MakeMax(m_DragOffset.cx, 0);

	MakeMax(m_DragOffset.cy, 0);
	}

void CStdTreeWnd::MakeDragImage(void)
{
	CRect Rect = m_DragSize;

	m_DragImage.Create(CClientDC(NULL), m_DragSize);

	CMemoryDC DC;

	DC.Select(m_DragImage);

	DC.FillRect(Rect, afxBrush(MAGENTA));

	// NOTE -- Don't use black of Vista gets confused!

	DC.SetTextColor(RGB(1,1,1));

	DC.Select(afxFont(DialogNA));

	DC.SetBkMode(TRANSPARENT);

	CString Text   = m_pTree->GetItemText (m_hSelect);

	UINT    uImage = m_pTree->GetItemImage(m_hSelect);

	CSize   Size   = DC.GetTextExtent(Text);

	CPoint  Origin = CPoint(16, 0);

	CSize   Adjust = Rect.GetSize() - Size - CSize(16, 0);

	DC.TextOut(Origin + Adjust / 2, Text);

	m_Images.Draw(uImage, DC, 0, 0, ILD_NORMAL);

	DC.Deselect();
	
	DC.Deselect();
	}

DWORD CStdTreeWnd::FindDragAllowed(void)
{
	return DROPEFFECT_COPY;
	}

void CStdTreeWnd::DragComplete(DWORD dwEffect)
{
	}

// Tree Loading

void CStdTreeWnd::LoadImageList(void)
{
	}

void CStdTreeWnd::LoadTree(void)
{
	}

void CStdTreeWnd::SortTree(void)
{
	}

// Item Hooks

BOOL CStdTreeWnd::IncludeItem(CMetaItem *pItem)
{
	return TRUE;
	}

void CStdTreeWnd::NewItemSelected(void)
{
	}

UINT CStdTreeWnd::GetRootImage(void)
{
	return 0;
	}

UINT CStdTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return 0;
	}

CString CStdTreeWnd::GetItemText(CMetaItem *pItem)
{
	return pItem->GetName();
	}

BOOL CStdTreeWnd::HasFolderImage(HTREEITEM hItem)
{
	return FALSE;
	}

BOOL CStdTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	return FALSE;
	}

void CStdTreeWnd::AddExtraCommands(CMenu &Menu)
{
	}

void CStdTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	}

void CStdTreeWnd::OnItemDeleted(CItem *pItem, BOOL fExec)
{
	}

void CStdTreeWnd::OnItemRenamed(CItem *pItem)
{
	if( pItem == m_pSelect ) {

		m_System.ItemUpdated(m_pSelect, updateRename);
		}
	}

// Selection Hooks

CString CStdTreeWnd::SaveSelState(void)
{
	return L"";
	}

void CStdTreeWnd::LoadSelState(CString State)
{
	SelectRoot();
	}

// Item Access

CMetaItem * CStdTreeWnd::GetItemPtr(HTREEITEM hItem)
{
	CMetaItem *pItem = GetItemOpt(hItem);

	AfxAssert(pItem);

	return pItem;
	}

CMetaItem * CStdTreeWnd::GetItemOpt(HTREEITEM hItem)
{
	if( hItem ) {

		return (CMetaItem *) m_pTree->GetItemParam(hItem);
		}

	return NULL;
	}

// Implementation

BOOL CStdTreeWnd::IsParent(void)
{
	return IsParent(m_hSelect);
	}

BOOL CStdTreeWnd::IsParent(HTREEITEM hItem)
{
	return m_pTree->GetChild(hItem) ? TRUE : FALSE;
	}

void CStdTreeWnd::SetFolderImage(HTREEITEM hItem, UINT uAction)
{
	if( HasFolderImage(hItem) ) {

		CTreeViewItem Node(hItem, TVIF_IMAGE);

		m_pTree->GetItem(Node);

		UINT uImage = Node.GetImage();

		UINT uValue = (uAction == TVE_EXPAND) ? 1 : 0;

		uImage = ((uImage & ~1) | uValue);

		Node.SetImages(uImage);

		m_pTree->SetItem(Node);
		}
	}

BOOL CStdTreeWnd::ExpandItem(HTREEITEM hItem, UINT uAction)
{
	if( IsParent(hItem) ) {

		SetFolderImage(hItem, uAction);

		m_pTree->Expand(hItem, uAction);

		return TRUE;
		}

	return FALSE;
	}

void CStdTreeWnd::ExpandItem(HTREEITEM hItem)
{
	if( !m_pTree->IsExpanded(hItem) ) {

		ExpandItem(hItem, TVE_EXPAND);
		}
	}

void CStdTreeWnd::ToggleItem(HTREEITEM hItem)
{
	if( hItem != m_hRoot ) {

		UINT uAction = m_pTree->IsExpanded(hItem) ? TVE_COLLAPSE : TVE_EXPAND;

		ExpandItem(hItem, uAction);
		}
	}

void CStdTreeWnd::SelectRoot(void)
{
	m_pTree->SelectItem(NULL);

	m_pTree->SelectItem(m_hRoot);

	ExpandItem(m_hRoot);
	}

BOOL CStdTreeWnd::SelectInit(void)
{
	if( !m_System.IsNavBusy() ) {

		SelectRoot();

		if( !m_fInitRoot ) {

			HTREEITEM hItem = m_pTree->GetNextNode(m_hSelect, TRUE);

			if( hItem ) {

				m_pTree->SelectItem(hItem);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CStdTreeWnd::SetRedraw(BOOL fRedraw)
{
	if( fRedraw ) {

		m_pTree->SetRedraw(TRUE);

		m_pTree->Invalidate(FALSE);

		return;
		}

	m_pTree->SetRedraw(FALSE);
	}

void CStdTreeWnd::RefreshTree(void)
{
	CString ss = SaveSelState();

	m_fLoading = TRUE;

	SetRedraw(FALSE);

	KillTree(m_hRoot);

	LoadTree();

	m_fLoading = FALSE;

	LoadSelState(ss);

	SetRedraw(TRUE);
	}

void CStdTreeWnd::KillTree(HTREEITEM hRoot)
{
	if( hRoot == m_hRoot ) {

		KillTree(hRoot, FALSE);

		KillItem(hRoot, TRUE);

		return;
		}

	KillTree(hRoot, TRUE);
	}

void CStdTreeWnd::KillTree(HTREEITEM hRoot, BOOL fRemove)
{
	KillData(hRoot, fRemove);
		
	m_pTree->DeleteItem(hRoot);
	}

void CStdTreeWnd::KillData(HTREEITEM hRoot, BOOL fRemove)
{
	HTREEITEM hItem;

	if( hItem = m_pTree->GetChild(hRoot) ) {

		for(;;) {

			HTREEITEM hNext = m_pTree->GetNext(hItem);

			KillData(hItem, fRemove);

			if( hNext == NULL ) {

				break;
				}

			hItem = hNext;
			}
		}

	KillItem(hRoot, fRemove);
	}

BOOL CStdTreeWnd::IsReadOnly(void)
{
	return m_pItem->GetDatabase()->IsReadOnly();
	}

BOOL CStdTreeWnd::IsPrivate(void)
{
	return m_pItem->GetDatabase()->IsPrivate();
	}

// End of File
