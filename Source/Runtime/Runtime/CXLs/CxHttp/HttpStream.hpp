
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HTTPSTREAM_HPP

#define	INCLUDE_HTTPSTREAM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Http Writable Stream Interface
//

interface DLLAPI IHttpStreamWrite : public IUnknown
{
	AfxDeclareIID(100, 1);

	virtual BOOL Write(PCTXT pData, UINT uData) = 0;
	virtual void Finalize(void)		    = 0;
	virtual void Abort(void)		    = 0;
	};
	
interface DLLAPI IHttpStreamHash : public IUnknown
{
	AfxDeclareIID(100, 2);

	virtual BOOL GetHash(CString &Hash) = 0;
};

// End of File

#endif
