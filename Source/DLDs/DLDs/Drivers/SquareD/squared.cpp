
#include "intern.hpp"

#include "squared.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Idec3 Driver
//

// Instantiator

INSTANTIATE(CSquareDDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CSquareDDriver::CSquareDDriver(void)
{
	m_Ident		= DRIVER_ID;

	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_bLastResponse	= NAK;

	m_bSeq		= 0;

	m_fTxOdd	= FALSE;

	m_fDataPad	= TRUE;

	m_uDataState	= 0;
	}

// Destructor

CSquareDDriver::~CSquareDDriver(void)
{
	}

// Configuration

void MCALL CSquareDDriver::Load(LPCBYTE pData)
{
	if ( GetWord( pData ) == 0x1234 ) {

		m_bSource = GetByte(pData);

		cSrce[0]  = m_pHex[m_bSource >>  4];
		cSrce[1]  = m_pHex[m_bSource & 0xF];
		cSrce[2]  = 0;
		}
	}

void MCALL CSquareDDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}

// Management

void MCALL CSquareDDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CSquareDDriver::Open(void)
{
	}

// Device

CCODE MCALL CSquareDDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->dDrops = GetLong(pData);

			BYTE b1			= m_pCtx->dDrops % 200;
			BYTE b2			= (m_pCtx->dDrops / 1000) % 200;

			m_pCtx->m_fHas3		= m_pCtx->dDrops >= 1000;
			m_pCtx->m_fHasRoute	= b1 || m_bSource;

			memset( m_pCtx->cDst1, 0, 3 );
			memset( m_pCtx->cDst2, 0, 3 );

			if( m_pCtx->m_fHasRoute ) {

				m_pCtx->cDst1[0] = m_pHex[b1 >>  4];
				m_pCtx->cDst1[1] = m_pHex[b1 & 0xF];

				m_pCtx->cDst2[0] = m_pHex[b2 >>  4];
				m_pCtx->cDst2[1] = m_pHex[b2 & 0xF];
				}

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CSquareDDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSquareDDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_INTG;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CSquareDDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Offset ) {

//**/		AfxTrace3("\r\nRead %d %d---%8.8lx ", Addr.a.m_Offset, uCount, m_pCtx->dDrops); Sleep(100); // slow down for debug

		MakeMin( uCount, 32 );

		PREADREQ pRequest = PREADREQ(m_bReq);

		pRequest->bOpcode = 0;
		pRequest->bSeq	  = m_bSeq++;
		pRequest->wStart  = HostToMotor((WORD)(2 * (Addr.a.m_Offset - 1)));

		BOOL f16 = Addr.a.m_Table == SPACE_INTG;

		UINT uCt = f16 ? uCount - 1 : 1;

		pRequest->wCount  = HostToMotor((WORD)uCt);

		m_bRx[0] = 0;

		BOOL fOkay = TxNetworkPacket(PBYTE(pRequest), sizeof(READREQ));

		if( !fOkay && !m_bRx[0] ) return CCODE_ERROR;

		if( fOkay == FALSE || fOkay == BUSY ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

		if( !RxNetworkPacket() ) return CCODE_ERROR;

		PREADREP pReply = PREADREP(m_bRx);

		if( pReply->bOpcode != 0x86 ) return CCODE_ERROR;

		if( pReply->bSeq != pRequest->bSeq ) return CCODE_ERROR;

		if( pReply->wStart != pRequest->wStart ) return CCODE_ERROR;

		if( f16 ) {

			for( UINT i = 0; i < uCount; i++ ) {

				pData[i] = MotorToHost(pReply->wData[i]);
				}

			return uCount;
			}

		else {
			UINT uL  = MotorToHost(pReply->wData[0]);
			UINT uH  = MotorToHost(pReply->wData[1]);
			pData[0] = (DWORD(uH) << 16) + uL;

			return 1;
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CSquareDDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Offset ) {

//**/		AfxTrace3("\r\n********** WRITE %d %d---%8.8lx ", Addr.a.m_Offset, uCount, m_pCtx->dDrops);

		MakeMin( uCount, 32 );

		PWRITEREQ pRequest = PWRITEREQ(m_bReq);

		pRequest->bOpcode = 2;
		pRequest->bSeq	  = m_bSeq++;
		pRequest->wStart  = HostToMotor((WORD)(2 * (Addr.a.m_Offset - 1)));

		BOOL f16 = Addr.a.m_Table == SPACE_INTG;

		UINT i   = 2;

		UINT uCt = f16 ? uCount : uCount * 2;

		if( f16 ) {

			for( i = 0; i < uCount; i++ ) pRequest->wData[i] = HostToMotor(LOWORD(pData[i]));
			}

		else {
			pRequest->wData[0] = HostToMotor(LOWORD(pData[0]));
			pRequest->wData[1] = HostToMotor(HIWORD(pData[0]));
			}

		pRequest->wData[i] = 0xFFFF;

		BOOL fOkay = TxNetworkPacket(PBYTE(pRequest), sizeof(WRITEREQ) + uCt * sizeof(WORD));

		if( fOkay == FALSE || fOkay == BUSY ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

		if( !RxNetworkPacket() ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

		PWRITEREP pReply = PWRITEREP(m_bRx);

		if( pReply->bOpcode != 0x80 ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

		if( pReply->bSeq != pRequest->bSeq ) return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;

		return f16 ? uCount : 1;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Device Access
BOOL	CSquareDDriver::TxNetworkPacket(PBYTE pData, UINT uCount)
{
	m_fTxOdd = !m_fTxOdd;

	for( UINT uTx = 0; uTx < SEND_COUNT; uTx++ ) {

		TxDataLinkPacket(pData, uCount);

		Send();

		for( UINT uEnq = 0; uEnq < ENQ_COUNT; ) {

			SetTimer(REPLY_TIMEOUT);

			while( GetTimer() ) {

				SYM Sym;

				if( !RxDataLinkSymbol(&Sym) )
					continue;

				if( Sym.fControl ) {

					if( m_fTxOdd && Sym.bValue == ODD ) return TRUE;

					if( !m_fTxOdd && Sym.bValue == EVN ) return TRUE;

					if( Sym.bValue == SYN ) return BUSY;

					if( Sym.bValue == NAK ) {

						uEnq = ENQ_COUNT;
						break;
						}
					}
				}

			if( uEnq++ < ENQ_COUNT - 1 ) {
				Put(DLE);
				Put(ENQ);
				}
			}
		}

	return FALSE;
	}

BOOL CSquareDDriver::RxNetworkPacket(void)
{
	WORD wPtr    = 0;

	BOOL fRxOdd  = FALSE;

	BYTE bCheck  = 0;

	WORD wNAKs   = 0;

	WORD wState  = 0;

	SYM Sym;

	PBYTE pData  = m_bRx;

	memset( &Sym, 0, sizeof(Sym) );

	SetTimer(FRAME_TIMEOUT);

	while( GetTimer() ) {

		SYM Sym;

		if( !RxDataLinkSymbol(&Sym) )
			continue;

		if( !Sym.fControl ) {

			bCheck += Sym.bValue;

			if( Sym.bValue == DLE )
				bCheck += DLE;
			}
		else {
			bCheck += DLE;
			bCheck += Sym.bValue;
			}

		switch( wState ) {

			case 0:
				if( Sym.fControl && Sym.bValue == SOH ) {
					wState = 1;
					bCheck = 0;
					}
				break;

			case 1:
				if( Sym.fControl )
					wState = 0;
				else {
					switch( Sym.bValue ) {
						case ODD:
							fRxOdd = TRUE;
							wState = 2;
							break;
						case EVN:
							fRxOdd = FALSE;
							wState = 2;
							break;
						default:
							wState = 0;
							break;
						}
					}
				break;

			case 2:
				if( Sym.fControl ) {
					if( Sym.bValue == STX ) {
						wPtr   = 0;
						wState = 3;
						}
					else
						wState = 0;
					}
				break;

			case 3:
				if( Sym.fControl ) {

					if( Sym.bValue == ETX ) {

						if( !BYTE(bCheck + Sym.bCheck) ) {

							TxAck(fRxOdd);

							return TRUE;
							}
						else {
							if( wNAKs++ < NAK_COUNT ) {

								SetTimer(FRAME_TIMEOUT);

								TxNak();

								wState = 0;
								}
							else {
								TxAck(fRxOdd);

								return FALSE;
								}
							}
						}
					}
				else {
					if( wPtr < sizeof(m_bRx) )
						pData[wPtr++] = Sym.bValue;
					else {
						TxNak();

						wState = 0;
						}
					}
				break;
			}
		}

	return FALSE;
	}

void	CSquareDDriver::TxNak(void)
{
	Put(DLE);

	Put(m_bLastResponse = NAK);
	}

void	CSquareDDriver::TxAck(BOOL fOdd)
{
	Put(DLE);

	Put( fOdd ? m_bLastResponse = ODD : m_bLastResponse = EVN );
	}

void	CSquareDDriver::TxDataLinkPacket(PBYTE pData, UINT uCount)
{
	StartFrame();

	AddDLEplusByte(SOH);

	m_bCheck = 0;

	AddByte( m_fTxOdd ? ODD : EVN );

	if( m_pCtx->m_fHasRoute ) {

		AddText(cSrce);
	
		AddText(m_pCtx->cDst1);

		if( m_pCtx->m_fHas3 ) {

			AddText(m_pCtx->cDst2);
			}
		}

	AddDLEplusByte( STX );

	for( UINT i = 0; i < uCount; i++ ) {

		if( pData[i] == DLE ) AddByte(DLE);

		AddByte(pData[i]);
		}

	AddDLEplusByte(ETX);
	}

BOOL	CSquareDDriver::RxDataLinkSymbol(PSYM pSymbol)
{
	WORD uByte;

	if( (uByte = Get(TIMEOUT)) == LOWORD(NOTHING) ) return FALSE;

//**/	if( LOBYTE(uByte) == DLE ) AfxTrace0("\r\n"); AfxTrace1("<%2.2x>", LOBYTE(uByte) );

	if( uByte == PAD && m_fDataPad ) return FALSE;

	switch( m_uDataState ) {

		case 0:
			if( uByte == DLE ) {
				m_uDataState = 1;
				break;
				}

			pSymbol->fControl = FALSE;
			pSymbol->bValue   = BYTE(uByte);

			m_uDataState = 0;

			return TRUE;

		case 1:
			if( uByte == ETX ) {
				m_uDataState = 2;
				break;
				}

			if( uByte == DLE ) {

				pSymbol->fControl = FALSE;
				pSymbol->bValue	  = DLE;

				m_uDataState = 0;

				return TRUE;
				}

			switch( uByte ) {

				case ENQ:
					m_uDataState = 0;
					break;

				case SOH:
					m_fDataPad = FALSE;
					break;

				case STX:
				case ODD:
				case EVN:
				case NAK:
				case SYN:
					break;

				default:
					m_uDataState = 0;
					break;
				}

			if( m_uDataState ) {

				pSymbol->fControl = TRUE;
				pSymbol->bValue   = BYTE(uByte);

				m_uDataState = 0;

				return TRUE;
				}
			break;

		case 2:
			pSymbol->fControl = TRUE;
			pSymbol->bValue	  = ETX;
			pSymbol->bCheck   = uByte;

			m_uDataState = 0;
			m_fDataPad   = TRUE;

			return TRUE;
		}

	return FALSE;
	}

// Frame Building
void	CSquareDDriver::StartFrame(void)
{
	m_uPtr = 0;

	m_bCheck = 0;
	}

void	CSquareDDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck -= bData;
	}

void	CSquareDDriver::AddText(PCTXT Text)
{
	UINT u = 0;

	while( Text[u] ) AddByte( Text[u++] );
	}

void	CSquareDDriver::AddDLEplusByte(BYTE bData)
{
	AddByte( DLE );
	AddByte( bData );
	}

// Transport
void	CSquareDDriver::Send(void)
{
	Sleep(20);

	AddByte(m_bCheck);

	m_pData->ClearRx();

//**/	AfxTrace0("\r\nS "); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

// Port Access
void	CSquareDDriver::Put(BYTE b)
{
//**/	if( b == DLE ) AfxTrace0("\r\nP "); AfxTrace1("[%2.2x]", b );

	m_pData->Write( b, FOREVER );
	}

WORD	CSquareDDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

// End of File
