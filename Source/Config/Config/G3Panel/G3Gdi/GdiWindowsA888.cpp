
#include "Intern.hpp"

#include "GdiWindowsA888.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Software Gdi Driver for Windows hDC
//

// Instantiator

IGdiWindows * Create_GdiWindowsA888(int cx, int cy)
{
	return New CGdiWindowsA888(cx, cy);
	}

// Constructor

CGdiWindowsA888::CGdiWindowsA888(int cx, int cy) : CGdiSoftA888(cx, cy, 1.0, NULL)
{
	BITMAPINFO bmi;

	ZeroMemory(&bmi, sizeof(bmi));

	bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth       = +cx;
	bmi.bmiHeader.biHeight      = -cy;
	bmi.bmiHeader.biPlanes      = 1;
	bmi.bmiHeader.biBitCount    = 32;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biSizeImage   = cx * cy * 4;

	m_hDraw = CreateCompatibleDC(NULL);

	m_hBits = CreateDIBSection(m_hDraw, &bmi, DIB_RGB_COLORS, (void **) &m_pData, NULL, 0);

	AfxAssume(m_hBits);

	m_hOld  = SelectObject(m_hDraw, m_hBits);

	ZeroMemory(m_pData, cx * cy * 4);
	}

// Destructor

CGdiWindowsA888::~CGdiWindowsA888(void)
{
	SelectObject(m_hDraw, m_hOld);

	DeleteObject(m_hBits);

	DeleteDC(m_hDraw);
	}

// IWindowsGdi Attributes

IGdi * CGdiWindowsA888::GetGdi(void)
{
	return this;
	}

HDC CGdiWindowsA888::GetDC(void)
{
	return m_hDraw;
	}

HBITMAP CGdiWindowsA888::GetBitmap(void)
{
	return m_hBits;
	}

// IGdiWindows Operations

void CGdiWindowsA888::ClearAlpha(void)
{
	ZeroMemory(m_pData, m_cx * m_cy * 4);
	}

void CGdiWindowsA888::ReleaseBitmap(void)
{
	SelectObject(m_hDraw, m_hOld);
	}

void CGdiWindowsA888::RestoreBitmap(void)
{
	m_hOld = SelectObject(m_hDraw, m_hBits);
	}

void CGdiWindowsA888::SaveImage(void)
{
	memcpy(m_pWork, m_pData, m_cx * m_cy * sizeof(DWORD));
	}

void CGdiWindowsA888::RestoreImage(void)
{
	memcpy(m_pData, m_pWork, m_cx * m_cy * sizeof(DWORD));
	}

// IGdi Rendering

UINT CGdiWindowsA888::Render(PBYTE pData, UINT uBits)
{
	memcpy(m_pWork, m_pData, m_cx * m_cy * sizeof(DWORD));

	return CGdiSoftA888::Render(pData, uBits);
	}

// End of File
