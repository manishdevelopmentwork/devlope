
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mode Selection Button
//

// Runtime Class

AfxImplementRuntimeClass(CModeButton, CButton);

// Constructor

CModeButton::CModeButton(CUIElement *pUI) : CDropButton(pUI)
{
	m_uData = 0;

	m_uSlot = 0;
	}

// Creation

BOOL CModeButton::Create(CRect const &Rect, HWND hParent, UINT uID)
{
	DWORD dwStyle = WS_TABSTOP | BS_PUSHBUTTON;

	if( afxWin2K ) {

		dwStyle |= BS_OWNERDRAW;
		}

	if( CCtrlWnd::Create(dwStyle, Rect, hParent, uID) ) {

		SetFont(afxFont(Dialog));

		SetWindowText(L" ");

		return TRUE;
		}

	return FALSE;
	}

// Attributes

CSize CModeButton::GetSize(void)
{
	return GetSize(CClientDC(NULL));
	}

CSize CModeButton::GetSize(CDC &DC)
{
	CalcSize(DC);

	return m_Size;
	}

UINT CModeButton::GetData(void) const
{
	return m_uData;
	}

// Operations

void CModeButton::ClearOptions(void)
{
	m_Opt.Empty();

	m_Map.Empty();
	}

BOOL CModeButton::AddOption(COption const &Opt)
{
	UINT uSlot = m_Opt.Append(Opt);

	if( Opt.m_uID < NOTHING ) {

		if(  m_Map.Insert(Opt.m_uID, uSlot) ) {

			return TRUE;
			}

		AfxAssert(FALSE);

		return FALSE;
		}

	return TRUE;
	}

void CModeButton::SetData(UINT uData)
{
	m_uData = uData;

	INDEX n = m_Map.FindName(uData);

	AfxAssert(!m_Map.Failed(n));

	m_uSlot = m_Map.GetData(n);

	Update();
	}

UINT CModeButton::ShowMenu(void)
{
	CMenu Menu;

	Menu.CreatePopupMenu();

	for( UINT n = 0; n < m_Opt.GetCount(); n++ ) {

		COption const &Opt = m_Opt[n];

		if( Opt.m_uID == NOTHING ) {

			Menu.AppendSeparator();

			continue;
			}

		if( Opt.m_fHidden ) {

			continue;
			}

		UINT uID = 0x1000 + n;

		if( Opt.m_Text.GetLength() == 1 ) {

			CString TempText = Opt.m_Text + L' ';

			Menu.AppendMenu(0, uID, TempText);
			}
		else
			Menu.AppendMenu(0, uID, Opt.m_Text);
		}

	Menu.MakeOwnerDraw(FALSE);

	CPoint Pos = GetWindowRect().GetBottomLeft();

	UINT   uID = Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RETURNCMD,
					  Pos,
					  *afxMainWnd
					  );

	Menu.FreeOwnerDraw();

	if( uID ) {

		COption const &Opt = m_Opt[LOBYTE(uID)];

		if( m_uData == Opt.m_uID ) {

			return NOTHING;
			}

		return Opt.m_uID;
		}

	return NOTHING;
	}

// Message Map

AfxMessageMap(CModeButton, CDropButton)
{
	AfxDispatchMessage(WM_GETDLGCODE)
	AfxDispatchMessage(WM_CHAR)
	AfxDispatchMessage(WM_ENABLE)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxDispatchNotify(0, NM_CUSTOMDRAW, OnCustomDraw)

	AfxDispatchGetInfo(0, OnGetInfo)
	AfxDispatchControl(0, OnControl)

	AfxMessageEnd(CModeButton)
	};

// Message Handlers

UINT CModeButton::OnGetDlgCode(MSG *pMsg)
{
	return DLGC_WANTCHARS;
	}

void CModeButton::OnChar(UINT uCode, DWORD dwFlags)
{
	if( isalnum(uCode) ) {

		for( UINT n = 0; n < m_Opt.GetCount(); n++ ) {

			COption const &Opt = m_Opt[n];

			if( Opt.m_uID < NOTHING ) {

				if( !Opt.m_fHidden && Opt.m_fEnable ) {

					if( toupper(Opt.m_Text[0]) == toupper(uCode) ) {

						CWnd &Parent = GetParent();
						
						Parent.SendMessage( WM_COMMAND,
								    WPARAM(MAKELONG(GetID(), Opt.m_uID)),
								    LPARAM(GetHandle())
								    );

						return;
						}
					}
				}
			}
		}
	}

void CModeButton::OnEnable(BOOL fEnable)
{
	Update();
	}

void CModeButton::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw)
{
	CRect Rect = Draw.rcItem;

	if( Draw.itemState & ODS_SELECTED ) {

		DrawEdge(Draw.hDC, Rect--, BDR_SUNKENOUTER, BF_RECT | BF_SOFT);

		DrawEdge(Draw.hDC, Rect--, BDR_SUNKENINNER, BF_RECT | BF_SOFT);
		}
	else {
		DrawEdge(Draw.hDC, Rect--, BDR_RAISEDOUTER, BF_RECT | BF_SOFT);

		DrawEdge(Draw.hDC, Rect--, BDR_RAISEDINNER, BF_RECT | BF_SOFT);
		}

	FillRect(Draw.hDC, &Rect, afxBrush(3dFace).GetHandle());

	NMCUSTOMDRAW Info;

	Info.dwDrawStage = CDDS_POSTPAINT;
	Info.hdc         = Draw.hDC;
	Info.rc          = Rect;
	Info.uItemState  = 0;

	if( Draw.itemState & ODS_FOCUS ) {

		CRect Focus = Rect - 1;

		DrawFocusRect(Draw.hDC, &Focus);
		}

	if( Draw.itemState & ODS_DISABLED ) {

		Info.uItemState |= CDIS_DISABLED;
		}

	if( Draw.itemState & ODS_HOTLIGHT ) {

		Info.uItemState |= CDIS_HOT;
		}

	if( Draw.itemState & ODS_SELECTED ) {

		Info.uItemState |= CDIS_SELECTED;
		}

	OnCustomDraw(uID, Info);
	}

// Notification Handlers

UINT CModeButton::OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYPOSTPAINT;
		}

	if( Info.dwDrawStage == CDDS_POSTPAINT ) {

		CDC DC(Info.hdc);

		DC.SetBkMode(TRANSPARENT);

		DC.Select(afxFont(Marlett2));

		CRect   Rect  = Info.rc;

		CString Text1 = L"u";

		CString Text2 = m_Opt[m_uSlot].m_Text;

		CSize   Size1 = DC.GetTextExtent(Text1);

		if( Info.uItemState & CDIS_DISABLED ) {

			DC.SetTextColor(afxColor(3dShadow));
			}
		else {
			if( Info.uItemState & CDIS_SELECTED ) {

				DC.SetTextColor(afxColor(ButtonText));
				}
			else {
				if( Info.uItemState & CDIS_HOT ) {

					DC.SetTextColor(afxColor(NavBar1));
					}
				else
					DC.SetTextColor(afxColor(Enabled));
				}
			}

		int yDrop = (Rect.cy() - Size1.cy - 2) / 2;

		DC.TextOut(Rect.left + 4, Rect.top + yDrop, Text1);

		DC.Replace(afxFont(Dialog));

		CSize Size2 = DC.GetTextExtent(Text2);

		int   xPos  = Rect.left + 6 + Size1.cx;

		int   yPos  = Rect.top  + (Rect.cy() - Size2.cy) / 2;

		if( Info.uItemState & CDIS_DISABLED ) {

			DC.SetTextColor(afxColor(3dShadow));
			}
		else {
			if( m_Opt[m_uSlot].m_fHidden ) {

				DC.SetTextColor(RGB(160,0,0));
				}
			else
				DC.SetTextColor(afxColor(BLACK));
			}

		if( m_uDrop ) {

			DC.FrameRect(Rect, afxBrush(Orange1));
			}

		DC.TextOut(xPos, yPos, Text2);

		DC.Deselect();

		DC.Detach(FALSE);
		}

	return 0;
	}

// Menu Control

BOOL CModeButton::OnControl(UINT uID, CCmdSource &Src)
{
	if( HIBYTE(uID) == 0x10 ) {

		COption const &Opt = m_Opt[LOBYTE(uID)];

		Src.EnableItem(Opt.m_fEnable);

		Src.CheckItem (Opt.m_uID == m_uData);

		return TRUE;
		}

	return FALSE;
	}

BOOL CModeButton::OnGetInfo(UINT uID, CCmdInfo &Info)
{
	if( HIBYTE(uID) == 0x10 ) {

		COption const &Opt = m_Opt[LOBYTE(uID)];

		Info.m_Image = Opt.m_Image;

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CModeButton::CalcSize(CDC &DC)
{
	DC.Select(afxFont(Dialog));

	m_Size = CSize(80, 22);

	for( UINT n = 0; n < m_Opt.GetCount(); n++ ) {

		COption const &Opt = m_Opt[n];

		if( Opt.m_Text.GetLength() ) {

			if( Opt.m_Text.Right(3) == L"..." ) {

				continue;
				}

			CSize Size = DC.GetTextExtent(Opt.m_Text);

			Size.cx += 32;

			MakeMax(m_Size.cx, Size.cx);
			}
		}

	DC.Deselect();
	}

BOOL CModeButton::Update(void)
{
	if( IsWindow() ) {

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

// End of File
