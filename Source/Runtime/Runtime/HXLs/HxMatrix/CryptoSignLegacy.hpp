
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoSignLegacy_HPP

#define INCLUDE_CryptoSignLegacy_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoSignRsa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Legacy Cryptographic Signer
//

class CCryptoSignLegacy : public CCryptoSignRsa
{
	public:
		// Constructor
		CCryptoSignLegacy(void);

		// ICryptoSign
		CString GetName(void);
		BOOL    Initialize(PCBYTE pKey, UINT uKey, CString const &Pass);
		void    Finalize(void);

	protected:
		// Key Data
		static BYTE const m_bPri1[];
		static BYTE const m_bPri2[];
		static BYTE const m_bPri3[];
		static BYTE const m_bHide[];
		
		// Implementation
		BOOL  DecodeBlob(PCBYTE pData, UINT uData, CString const &Pass);
		PBYTE DecryptKey(PCBYTE pData, UINT uData, CString const &Pass);
		void  Set(pstm_int &v, PCBYTE p, UINT n, UINT k);
		bool  Flip(PBYTE d, PCBYTE s, UINT n);
		bool  Flip(PBYTE p, UINT n);
	};

// End of File

#endif
