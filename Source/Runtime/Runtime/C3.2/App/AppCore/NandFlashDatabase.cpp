
#include "Intern.hpp"

#include "NandFlashDatabase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

////////////////////////////////////////////////////////////////////////
//
// NAND Flash Database
//

// Macros

#define	RoundUp(x) (((x) + 3) & ~3)

#define bits(x)    (sizeof(x) * 8)

// Instantiator

global IDatabase * Create_NandFlashDatabase(UINT uStart, UINT uEnd, UINT uPool)
{
	CNandBlock Start(0, uStart);

	CNandBlock End  (0, uEnd);

	return New CNandFlashDatabase(Start, End, uPool);
	}

// Constructor

CNandFlashDatabase::CNandFlashDatabase(CNandBlock const &Start, CNandBlock const &End, UINT uPool)
{
	m_pMutex     = Create_Mutex();

	m_BlockStart = Start;

	m_BlockEnd   = End;

	m_uPoolSize  = uPool * 1024;

	m_uPoolUsed  = 0;

	m_pHead      = NULL;

	m_pTail      = NULL;

	m_pState     = New CState [ dataItemCount ];
	}

// Destructor

CNandFlashDatabase::~CNandFlashDatabase(void)
{
	delete [] m_pState;
	}

// IDatabase

void CNandFlashDatabase::Init(void)
{
	CNandClient::Init();

	if( IsValid() && MountItems() ) {

		return;
		}
	
	Clear();
	}

void CNandFlashDatabase::Clear(void)
{
	EraseAll(true);

	MountItems();

	SetValid(FALSE);
	}

BOOL CNandFlashDatabase::IsValid(void)
{
	DWORD Magic = 0;

	FRAMGetData(Mem(Database), PBYTE(&Magic), sizeof(Magic));

	if( Magic == magicFRAM ) {

		return true;
		}

	return false;
	}

void CNandFlashDatabase::SetValid(BOOL fValid)
{
	DWORD Magic = fValid ? magicFRAM : 0;

	FRAMPutData(Mem(Database), PBYTE(&Magic), sizeof(Magic));
	}

void CNandFlashDatabase::SetRunning(BOOL fRun)
{
	if( !fRun ) {

		for( UINT uItem = 0; uItem < dataItemCount; uItem++ ) {

			CState &State = m_pState[uItem];

			if( State.m_fValid ) {

				if( State.m_pData ) {

					delete [] State.m_pData;

					State.m_pData = NULL;
					}

				State.m_uLock = 0;
				}
			}

		m_uPoolUsed = 0;

		m_pHead     = NULL;

		m_pTail     = NULL;
		}
	}

BOOL CNandFlashDatabase::GarbageCollect(void)
{
	// NOTE -- This is a block level garbage collector in that is only frees blocks
	// which contain nothing but obsolete pages. A more complex approach would be to
	// walk up the database a page at a time and remove those we don't need. This
	// would produce a garbage collection routine that is bound to work if there is
	// in fact sufficient space available, but since we simply redownload upon a
	// garbage collection failure, this isn't required in this application.

	if( EraseObsoleteBlocks() ) {

		CNandBlock Dest = m_BlockStart;

		CNandBlock From = m_BlockStart;

		for(;;) {

			if( FindEmptyBlock(Dest) ) {

				From = Dest;
				
				if( FindValidBlock(From) ) {

					if( CopyBlock(Dest, From) ) {

						EraseBlock(From, true);

						continue;
						}

					SetValid(FALSE);

					return false;
					}
				}

			break;
			}

		MountItems();

		return true;
		}

	return false;
	}

BOOL CNandFlashDatabase::GetVersion(PBYTE pGuid)
{
	if( m_uPropsGen < NOTHING ) {

		memcpy(pGuid, &m_Guid, 16);

		return true;
		}

	return false;
	}

DWORD CNandFlashDatabase::GetRevision(void)
{
	if( m_uPropsGen < NOTHING ) {

		DWORD dwRev = m_Revision;

		return dwRev;
		}

	return 0;
	}

BOOL CNandFlashDatabase::SetVersion(PCBYTE pData)
{
	if( m_uPropsGen == NOTHING || memcmp(&m_Guid, pData, 16) ) {

		memcpy(&m_Guid, pData, 16);

		return CommitPropsPage();
		}

	return true;
	}

BOOL CNandFlashDatabase::SetRevision(DWORD dwRev)
{
	if( m_uPropsGen == NOTHING || m_Revision != dwRev ) {

		m_Revision = dwRev;

		return CommitPropsPage();
		}

	return true;
	}

BOOL CNandFlashDatabase::CanCompress(void)
{
	return true;
	}

BOOL CNandFlashDatabase::CheckSpace(UINT uSize)
{
	UINT uHead = m_uPageSize - sizeof(CItemHeadPage);

	UINT uData = m_uPageSize - sizeof(CItemDataPage);

	UINT uUsed = 1 + (Max(uHead, uSize) - uHead + uData - 1) / uData;

	return m_uFreePages > uUsed;
	}

BOOL CNandFlashDatabase::WriteItem(CItemInfo const &Info, PCVOID pData)
{
	if( Info.m_uItem < dataItemCount ) {

		CState &  State = m_pState[Info.m_uItem];

		UINT      uGen   = State.m_uGeneration + 1;

		DWORD     CRC    = CRC32(PBYTE(pData), Info.m_uComp);

		CNandPage Head   = m_PageCurrent;

		CNandPage Page   = Head;

		bool      fHead  = true;

		UINT      uSize  = Info.m_uComp;

		PBYTE     pFrom  = PBYTE(pData);

		while( fHead || uSize ) {

			UINT uHead;

			if( fHead ) {

				PHEAD p = PHEAD(m_pPageData);

				p->Header.Page.Magic	  = magicItem;
				
				p->Header.Page.Generation = uGen;
				
				p->Header.Index		  = Info.m_uItem;	

				p->Header.Pad[0]	  = NOTHING;
				
				p->Class		  = Info.m_uClass;
				
				p->Size			  = Info.m_uSize;

				p->Comp			  = Info.m_uComp;
				
				p->CRC			  = CRC;

				uHead = sizeof(*p);
				}
			else {			
				PDATA p = PDATA(m_pPageData);

				p->Page.Magic	   = magicBody;
				
				p->Page.Generation = uGen;
				
				p->Index	   = Info.m_uItem;

				p->Pad[0]	   = NOTHING;

				uHead = sizeof(*p);
				}

			UINT uCopy = Min(uSize, m_uPageSize - uHead);

			UINT uEnd  = uHead + uCopy;

			UINT uFill = m_uPageSize - uHead - uCopy;

			memcpy(m_pPageData + uHead, pFrom, uCopy);

			memset(m_pPageData + uEnd,   0xFF, uFill);

			if( WriteWithReloc(Page) ) {

				if( fHead ) {

					Head  = Page;

					fHead = false;
					}

				uSize -= uCopy;

				pFrom += uCopy;

				continue;
				}

			return false;
			}
		
		State.m_uGeneration = uGen;

		State.m_Size        = Info.m_uSize;

		State.m_Comp	    = Info.m_uComp;

		State.m_CRC	    = CRC;

		State.m_Page        = Head;

		State.m_fValid      = true;

		return true;
		}

	return false;
	}

BOOL CNandFlashDatabase::GetItemInfo(UINT uItem, CItemInfo &Info)
{
	if( uItem < dataItemCount ) {

		CState const &State = m_pState[uItem];

		if( State.m_fValid ) {

			Info.m_uItem  = uItem;

			Info.m_uClass = State.m_Class;

			Info.m_uSize  = State.m_Size;

			Info.m_uComp  = State.m_Comp;

			Info.m_CRC    = State.m_CRC;

			return true;
			}
		}

	return false;
	}

PCVOID CNandFlashDatabase::LockItem(UINT uItem, CItemInfo &Info)
{
	if( uItem < dataItemCount ) {

		CState &State = m_pState[uItem];

		if( State.m_fValid ) {

			if( m_pMutex->Wait(FOREVER) ) {

				if( !State.m_uLock++ && !State.m_pData ) {

					CNandPage Page  = State.m_Page;

					bool      fHead = true;

					UINT      uSize = 0;

					PBYTE     pDest = NULL;

					for(;;) {

						if( ReadPage(Page) ) {

							UINT uHead = sizeof(CItemDataPage);

							if( fHead ) {

								PCHEAD p = PCHEAD(m_pPageData);

								Info.m_uItem  = uItem;
							
								Info.m_uClass = State.m_Class;
							
								Info.m_uSize  = State.m_Size;

								Info.m_uComp  = State.m_Comp;
							
								Info.m_CRC    = State.m_CRC;

								State.m_pData = New BYTE [ State.m_Comp ];

								uSize = State.m_Comp;

								pDest = State.m_pData;

								uHead = sizeof(*p);

								fHead = false;
								}

							UINT uCopy = Min(uSize, m_uPageSize - uHead);

							memcpy(pDest, m_pPageData + uHead, uCopy);

							pDest += uCopy;

							uSize -= uCopy;

							if( !uSize ) {

								if( State.m_Comp - State.m_Size ) {

									PBYTE pWork = New BYTE [ State.m_Size ];

									PBYTE pPrev = State.m_pData;

									SetTaskLimit(50, 5);

									fastlz_decompress(pPrev, State.m_Comp, pWork);
									
									SetTaskLimit( 0, 0);

									State.m_pData = pWork;

									delete [] pPrev;
									}

								AfxListAppend(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

								m_uPoolUsed += State.m_Size;

								m_pMutex->Free();

								return State.m_pData;
								}

							if( GetNextGoodPage(Page) ) {

								continue;
								}
							}

						break;
						}

					// NOTE -- Database is hosed and we cannot recover. Do not
					// clear valid but just force a reset in the hope that we'll
					// be able to get a hardware recovery via the reset.

					HostTrap(8);

					m_pMutex->Free();

					return NULL;
					}

				Info.m_uItem  = uItem;
							
				Info.m_uClass = State.m_Class;
							
				Info.m_uSize  = State.m_Size;

				Info.m_uComp  = State.m_Comp;
							
				Info.m_CRC    = State.m_CRC;

				AfxListRemove(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

				AfxListAppend(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

				m_pMutex->Free();

				return State.m_pData;
				}
			}
		}

	return NULL;
	}

PCVOID CNandFlashDatabase::LockItem(UINT uItem)
{
	CItemInfo Info;

	return LockItem(uItem, Info);
	}

void CNandFlashDatabase::PendItem(UINT uItem, BOOL fPend)
{
	}

void CNandFlashDatabase::LockPendingItems(BOOL fLock)
{
	}

void CNandFlashDatabase::FreeItem(UINT uItem)
{
	if( uItem < dataItemCount ) {

		CState &State = m_pState[uItem];

		if( State.m_fValid ) {

			m_pMutex->Wait(FOREVER);

			if( !--State.m_uLock ) {

				CheckPoolUsage();
				}

			m_pMutex->Free();
			}
		}
	}

// Item State Array

bool CNandFlashDatabase::MountItems(void)
{
	m_PageCurrent = m_BlockStart;

	m_PageProps   = m_BlockStart;

	m_uFreePages  = m_uGoodPages;

	m_uPropsGen   = NOTHING;

	m_Revision    = 0xAAAAAAAA;

	memset(&m_Guid, 0xAA, 16);

	memset(m_pState, 0, sizeof(CState) * dataItemCount);

	return FindItems();
	}

bool CNandFlashDatabase::FindItems(void)
{
	CNandPage Page;

	Page.Invalidate();

	while( GetNextGoodPage(Page) ) {

		if( ReadPage(Page) ) {

			PCPAGE pPage = PCPAGE(m_pPageData);

			if( pPage->Magic == magicEmpty ) {

				break;
				}
			
			if( pPage->Magic == magicBody ) {

				m_uFreePages--;
				
				continue;
				}

			if( pPage->Magic == magicItem ) {

				PCHEAD   pHead = PCHEAD(m_pPageData);

				CState & State = m_pState[pHead->Header.Index];

				State.m_Page        = Page;

				State.m_fValid      = true;

				State.m_uGeneration = pPage->Generation;

				State.m_uLock       = 0;

				State.m_pData       = NULL;

				State.m_Class       = pHead->Class;

				State.m_Size        = pHead->Size;

				State.m_Comp	    = pHead->Comp;

				State.m_CRC         = pHead->CRC;

				m_uFreePages--;
				
				continue;
				}

			if( pPage->Magic == magicProps ) {

				PCPROPS pProps = PCPROPS(m_pPageData);

				m_uPropsGen  = pPage->Generation;

				m_PageProps  = Page;

				m_Revision   = pProps->Revision;

				memcpy(&m_Guid, &pProps->Guid, 16);

				m_uFreePages--;
				
				continue;
				}

			// NOTE -- This means we have a block that we don't recognize
			// and therefore to be safe, we will assume that the database
			// is hosed and return a value to indicate this.

			return false;
			}

		// NOTE -- A block that won't read with error correction also
		// means that the database the hosed, so we report such and
		// let the higher levels reformat.

		return false;
		}

	m_PageCurrent = Page;

	return true;
	}

void CNandFlashDatabase::CheckPoolUsage(void)
{
	if( m_uPoolUsed > 7 * m_uPoolSize / 8 ) {

		CState *pScan = m_pHead;

		while( m_uPoolUsed > 5 * m_uPoolSize / 8 ) {

			if( pScan ) {

				CState &State = *pScan;

				if( !State.m_uLock ) {

					delete [] State.m_pData;

					State.m_pData = NULL;

					m_uPoolUsed  -= State.m_Size;

					pScan         = pScan->m_pNext;

					AfxListRemove(m_pHead, m_pTail, (&State), m_pNext, m_pPrev);

					continue;
					}

				pScan = pScan->m_pNext;

				continue;
				}

			break;
			}
		}
	}

// Property Page Support

bool CNandFlashDatabase::CommitPropsPage(void)
{
	PPROPS p = PPROPS(m_pPageData);

	memset(p, 0xFF, m_uPageSize);

	memcpy(&p->Guid, &m_Guid, 16);

	p->Revision = m_Revision;

	p->Page.Generation = ++m_uPropsGen;

	p->Page.Magic      = magicProps;

	return WriteWithReloc(m_PageProps);
	}

// Obsolete Page Support

bool CNandFlashDatabase::EraseObsoleteBlocks(void)
{
	UINT  uCount = 0;

	CNandBlock Block = m_BlockStart;

	do {
		if( IsBlockObsolete(Block) ) {

			uCount++;
			
			EraseBlock(Block, true);

			UINT uPages  = m_Geometry.m_uPages;

			m_uFreePages = m_uFreePages + uPages;
			}

		} while( GetNextGoodBlock(Block) );

	return uCount > 0;
	}

bool CNandFlashDatabase::IsBlockObsolete(CNandBlock Block)
{
	if( !IsBadBlock(Block) ) {

		CNandPage Page = Block;

		for(;;) {

			if( !IsPageObsolete(Page) ) {

				return false;
				}

			if( ++Page.m_uPage == m_Geometry.m_uPages ) {

				break;
				}
			}

		return true;
		}

	return false;
	}

bool CNandFlashDatabase::IsPageObsolete(CNandPage Page)
{
	if( ReadPage(Page) ) {
			
		PCPAGE pHead = PCPAGE(m_pPageData);

		if( pHead->Magic == magicItem || pHead->Magic == magicBody ) {

			PCHEAD       pHead  = PCHEAD(m_pPageData);

			CState const &State = m_pState[pHead->Header.Index];

			if( State.m_uGeneration > pHead->Header.Page.Generation ) {

				return true;
				}

			return false;
			}

		if( pHead->Magic == magicProps ) {

			if( m_uPropsGen > pHead->Generation ) {

				return true;
				}
			}

		return false;
		}

	return false;
	}

bool CNandFlashDatabase::IsPageEmpty(CNandPage Page)
{
	if( ReadPage(Page) ) {
			
		PCPAGE p = PCPAGE(m_pPageData);

		if( p->Magic == magicEmpty ) {

			return true;
			}
		}

	return false;
	}

bool CNandFlashDatabase::FindEmptyBlock(CNandBlock &Block)
{
	for(;;) {

		if( IsPageEmpty(Block) ) {
			
			return true;
			}

		if( GetNextGoodBlock(Block) ) {

			if( !(Block > m_PageCurrent) ) {

				continue;
				}
			}

		return false;
		}

	return false;
	}

bool CNandFlashDatabase::FindValidBlock(CNandBlock &Block)
{
	for(;;) {

		if( !IsPageEmpty(Block) ) {
			
			return true;
			}

		if( GetNextGoodBlock(Block) ) {

			if( !(Block > m_PageCurrent) ) {

				continue;
				}
			}

		return false;
		}

	return false;
	}

bool CNandFlashDatabase::CopyBlock(CNandBlock BlockDest, CNandBlock BlockFrom)
{
	if( BlockDest != BlockFrom ) {

		CNandPage From = BlockFrom;

		CNandPage Dest = BlockDest;

		while( Dest.m_uPage < m_Geometry.m_uPages ) {

			if( ReadPage(From) ) {
				
				PCPAGE pHead = PCPAGE(m_pPageData);

				if( pHead->Magic == magicEmpty ) {
					
					break;
					}

				if( !WritePage(Dest, true) ) {
				
					return false;
					}

				From.m_uPage++;

				Dest.m_uPage++;

				continue;
				}

			return false;
			}

		return true;
		}

	return false;
	}

// Overridables

bool CNandFlashDatabase::OnPageReloc(PCBYTE pData, CNandPage const &Dest)
{
	PCPAGE pPage = PCPAGE(pData);

	if( pPage->Magic == magicItem ) {

		PCHEAD pHead = PCHEAD(pPage);

		UINT   uItem = pHead->Header.Index;

		m_pState[uItem].m_Page = Dest;

		return true;
		}

	if( pPage->Magic == magicProps ) {

		m_PageProps = Dest;

		return true;
		}

	return false;
	}

// End of File
