
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_LangManagerPage_HPP

#define INCLUDE_LangManagerPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLangManager;

//////////////////////////////////////////////////////////////////////////
//
// Language Manager Page
//

class CLangManagerPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CLangManagerPage(CLangManager *pLang);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CLangManager * m_pLang;
	};

// End of File

#endif
