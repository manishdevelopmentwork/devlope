
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Consumer_HPP

#define INCLUDE_Consumer_HPP

//////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Consumer
//

class CConsumer : public IConsumer
{
	public:
		// Constructor
		CConsumer(void);

		// Destructor
		~CConsumer(void);
		
		// Binding
		void Bind(UINT uIndex, IImplicit *pImplicit);

		// Mapping 
		void SetIdent(DWORD dwInstance, DWORD dwID);
		void SetMapping(WORD wOffset, WORD wSize);

		// Attributes
		DWORD GetInstance(void) const;
		UINT  GetIndex(void) const;
		BOOL  IsFree(void) const;

		// Operations
		void Create(void);
		void Close(void);
		void SetNewData(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IConsumer
		DWORD METHOD GetIdent(void);
		UINT  METHOD GetSize(void);
		BOOL  METHOD IsActive(void);
		BOOL  METHOD HasNewData(void);
		void  METHOD ClearNewData(void);
		UINT  METHOD Recv(PBYTE pData, UINT uSize);

	protected:
		// Data
		ULONG	    m_uRefs;
		UINT        m_uIndex;
		IImplicit * m_pAssy;
		BOOL	    m_fUsed;
		DWORD       m_dwInst;
		DWORD	    m_dwIdent;
		BOOL	    m_fDirty;
		WORD	    m_wOffset;
		WORD	    m_wSize;
	};

// End of File

#endif
