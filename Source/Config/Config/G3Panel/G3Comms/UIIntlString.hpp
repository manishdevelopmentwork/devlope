
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIIntlString_HPP

#define INCLUDE_UIIntlString_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CLangManager;
class CTransEditCtrl;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- International String
//

class CUIIntlString : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIIntlString(void);

	protected:
		// Data Members
		CCommsSystem   * m_pSystem;
		CLangManager   * m_pLang;
		CString	         m_Label;
		CString          m_Verb;
		CLayItemText   * m_pTextLayout;
		CLayItemText   * m_pDataLayout;
		CLayItem       * m_pPickLayout;
		CStatic	       * m_pTextCtrl;
		CTransEditCtrl * m_pDataCtrl;
		CCtrlWnd       * m_pPickCtrl;
		UINT             m_cfCode;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Implementation
		CRect GetDataCtrlRect(void);
		UINT  SaveCurrent(BOOL fUI, CString Data);
	};

// End of File

#endif
