
//////////////////////////////////////////////////////////////////////////
//
// AC Tech Simple Servo Driver
//
#define	FCSINIT 0xFFFF
#define	FCSGOOD 0xF0B8 // FCS of frame should come to this value
#define	BROADC	   255 // Broadcast address

// Comm byte constants
#define	EMASK	0x20   // Escaped byte mask
#define	CFLAG	0x7E   // Data Start/End Flag
#define	EFLAG	0x7D   // Escape Flag
#define	BCONT	0x03   // Control Byte
#define	PROTOS	0x0801 // Specification Protocol
#define	PROTOA	0x0010 // MotionView Protocol

// Opcode offset
#define	CMD_OFFSET	0x8000

// Data Header constants
#define	DH1	3
#define	DH2	7
#define	DH3	8
#define	DH4	0
#define	DH5	0
#define	DH6	0x22
#define	DH7	1
#define	DH7W	0

// Reply States
#define	RCV_CFLAG	0
#define	RCV_NORMAL	1
#define	RCV_EFLAG	2

// Return codes for No Read Transmit
#define	RCNODATA	1
#define	RCISDATA	2

// Data Buffer Positions
#define	RSP_ADDR	 1
#define	RSP_CONT	 2
#define	RSP_SEQL	12
#define	RSP_SEQH	13
#define	RSP_FNCL	14
#define	RSP_FNCH	15
#define	RSP_SZL		16
#define	RSP_SZH		17
#define	RSP_ERR		18
#define	RSP_DATA0	19 // first byte of data in receive frame

class CACTechSSDriver : public CMasterDriver
{
	public:
		// Constructor
		CACTechSSDriver(void);

		// Destructor
		~CACTechSSDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};

		CContext *	m_pCtx;

		// Data Members
		static	WORD CODE_SEG FCSTABLE[];

		// Comm Frames
		BYTE	m_bTx[64];
		BYTE	m_bRx[128];
		BYTE	m_bTxRaw[64];

		UINT	m_uPtr;
		UINT	m_uSequence;
		BOOL	m_fSeqErr;
		PCWORD	m_pFCS;
		UINT	m_uFCS;
		UINT	m_uRcvCt;

		// Function Data Storage
		DWORD	m_dCMMDVD;
		DWORD	m_dCMMDVV;
		DWORD	m_dCMMDURD;
		DWORD	m_dCMMDURS;
		DWORD	m_dCMMPURD;
		DWORD	m_dCMMPURS;
		DWORD	m_dCMSIPP;
		DWORD	m_dCMSIPD;
		DWORD	m_dCMUNOP;

		// Response Error
		DWORD	m_dError;
		BOOL	m_fNewError;
		
		// Frame Building
		void	StartFrame(BOOL fIsSpecial);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		BOOL	AddRead(AREF Addr);
		void	AddReadOpcode(UINT uOffset);
		BOOL	AddWrite(AREF Addr, DWORD dData);
		void	AddWriteData(AREF Addr, DWORD dData);
		void	AddEEPROMWrite(AREF Addr, DWORD dData);
		void	AddLength(UINT uLength);
		void	AddZero(void);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		void	GetResponse(AREF Addr, PDWORD pData);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		UINT	ReadNoTransmit( AREF Addr, PDWORD pData );
		BOOL	WriteNoTransmit( AREF Addr, PDWORD pData );
		BOOL	CanWriteToEEPROM( UINT uTable, UINT uOffset );
		DWORD	GetRealData(void);
		void	PutRealData(PDWORD pData);
		DWORD	GetValue( UINT uPos, UINT uCount );
		BOOL	CheckResponse(void);
		void	ConvertDF(PDWORD pdF);
		void	StoreDataToTx(void);
		BOOL	TestFCSR(UINT uCount);
		void	AddFCS(UINT * pFCS, BYTE b);

	};

// Table Space ID's
#define	TSID	1
#define	TSOC	2
#define	TSVAR	3
#define	TSEEPR	4
#define	TSUNOP	5

// Working area Function Codes
#define	CMCL	30
#define	CMPC	31
#define	CMPFPG	32
#define	CMPFIG	33
#define	CMPFDG	34
#define	CMVFFG	35
#define	CMPFIL	36
#define	CMVFPG	37
#define	CMVFIG	38
#define	CMMPE	39
#define	CMMPET	40
#define	CMDIA	41
#define	CMDIAX	0x43 // EEPROM Read ID
#define	CMDCC	42	
#define	CMCLR	43
#define	CMPCR	44
#define	CMPFPGR	45
#define	CMPFIGR	46
#define	CMPFDGR	47
#define	CMVFFGR	48
#define	CMPFILR	49
#define	CMVFPGR	50
#define	CMVFIGR	51
#define	CMMPER	52
#define	CMMPETR	53

#define	CMOC	54
#define	CMEIC	55
#define	CMO	56
#define	CMAOV	57
#define	CMOCR	58
#define	CMEICR	59
#define	CMOR	60
#define	CMDIS	61
#define	CMAIV	62
#define	CMAOVR	98

#define	CMSL	63
#define	CMHL	64
#define	CMNSL	65
#define	CMPSL	66
#define	CMSLR	67
#define	CMHLR	68
#define	CMNSLR	69
#define	CMPSLR	70

#define	CMMPF	71
#define	CMMS	72
#define	CMMRMS	73
#define	CMMAV	74
#define	CMMTV	75
#define	CMMMER	76
#define	CMMMPC	77
#define	CMERR	100
#define	CMUNOP	101

#define	CMU	 1
#define	CMA	 2
#define	CMD	 3
#define	CMQD	 4
#define	CMMV	 5
#define	CMSP	 6
#define	CMIPL	 7
#define	CMV	 8
#define	CMMER	 9
#define	CMGC	10
#define	CMVAR	11
#define	CMUR	12
#define	CMAR	13
#define	CMDR	14
#define	CMQDR	15
#define	CMMVR	16
#define	CMGAP	17
#define	CMGTP	18
#define	CMGPE	19
#define	CMIPLR	20
#define	CMVR	21
#define	CMMERR	22
#define	CMGCR	23
#define	CMVARR	24
#define	CMGLR	25

#define	CMRI	78
#define	CMMCE	79
#define	CMMCD	80
#define	CMSUSP	81
#define	CMRM	82
#define	CMMP	83
#define	CMMD	84
#define	CMMDVD	185
#define	CMMDVV	285
#define	CMMDV	85
#define	CMMPI1	86
#define	CMMPI0	87
#define	CMMNI1	88
#define	CMMNI0	89
#define	CMMDURD	190
#define	CMMDURS	290
#define	CMMDUR	90
#define	CMVME	91
#define	CMVMD	92
#define	CMGME	93
#define	CMGMD	94
#define	CMS	95
#define	CMSQ	96
#define	CMMPURD	197
#define	CMMPURS	297
#define	CMMPUR	97
#define	CMSIPP	199
#define	CMSIPD	299
#define	CMSIP	99

// EEPROM Access Function Codes
#define	CMCLS	0x53 // Write
#define	CMCLX	0x54 // Read
#define	CMPCS	0x55 // Write
#define	CMPCX	0x56 // Read
#define	CMPCS	0x55 // Write
#define	CMPCX	0x56 // Read
#define	CMPFPGS	0x4B // Write
#define	CMPFPGX	0x4C // Read
#define	CMPFIGS	0x49 // Write
#define	CMPFIGX	0x4A // Read
#define	CMPFDGS	0x50 // Write
#define	CMVFFGS	0x51 // Write
#define	CMVFFGX	0x52 // Read
#define	CMPFILS	0x4D // Write
#define	CMPFILX	0x4E // Read
#define	CMVFPGS	0x47 // Write
#define	CMVFPGX	0x48 // Read
#define	CMVFIGS	0x45 // Write
#define	CMVFIGX	0x46 // Read
#define	CMMPES	0x57 // Write
#define	CMMPEX	0x58 // Read
#define	CMMPETS	0x59 // Write
#define	CMMPETX	0x5A // Read
#define	CMOCS	0x64 // Write
#define	CMSLS	0x65 // Write
#define	CMHLS	0x66 // Write

// End of File
