
#include "Intern.hpp"

#include "UITextTimeSep.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Time Separator
//

// Dynamic Class

AfxImplementDynamicClass(CUITextTimeSep, CUITextEnum);

// Constructor

CUITextTimeSep::CUITextTimeSep(void)
{
	}

// Overridables

void CUITextTimeSep::OnBind(void)
{
	CUITextEnum::OnBind();

	AddData(0,   CString(IDS_LOCALE_DEFAULT));

	AddData(':', CString(IDS_COLON));

	AddData('.', CString(IDS_PERIOD));

	AddData(',', CString(IDS_COMMA));

	AddData(' ', CString(IDS_SPACE));
	}

// End of File
