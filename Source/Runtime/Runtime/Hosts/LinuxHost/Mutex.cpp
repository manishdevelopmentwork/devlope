
#include "Intern.hpp"

#include "Mutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mutex Object
//

// Instantiator

AfxStdInstantiator(IMutex, Mutex);

// Registration

global void Register_Mutex(void)
{
	piob->RegisterInstantiator("exec.mutex", Create_Mutex);
}

// Constructor

CMutex::CMutex(void)
{
	StdSetRef();

	m_count  = 0;

	m_pOwner = NULL;
}

// Destructor

CMutex::~CMutex(void)
{
	if( m_pOwner ) {

		AfxTrace("exec: owned mutex destroyed\n");

		ListRemove();
	}
}

// IUnknown

HRESULT CMutex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IMutex);

	return E_NOINTERFACE;
}

ULONG CMutex::AddRef(void)
{
	StdAddRef();
}

ULONG CMutex::Release(void)
{
	StdRelease();
}

// IWaitable

PVOID CMutex::GetWaitable(void)
{
	return NULL;
}

BOOL CMutex::Wait(UINT uWait)
{
	int p = _gettid();

	int c;

	if( m_state == p ) {

		m_count++;

		return TRUE;
	}

	while( (c = AtomicCompAndSwap(&m_state, 0, p)) ) {

		if( !WaitForChange(c, uWait, TRUE) ) {

			return FALSE;
		}
	}

	m_pOwner = (CExecThread *) (_tls_get_data()->cthread);

	ListAppend();

	m_count = 1;

	return TRUE;
}

BOOL CMutex::HasRequest(void)
{
	return m_swait > 0;
}

// IMutex

void CMutex::Free(void)
{
	if( m_state == _gettid() ) {

		if( !--m_count ) {

			ListRemove();

			m_pOwner = NULL;

			m_state  = 0;

			WakeThreads(1);
		}

		return;
	}

	AfxAssert(FALSE);
}

// Task Teardown

void CMutex::FreeMutexList(CExecThread *pThread)
{
	if( pThread->m_pOwnedHead ) {

		AfxTrace("exec: thread %u exiting with owned mutexes\n", pThread->GetIdent());

		while( pThread->m_pOwnedHead ) {

			pThread->m_pOwnedHead->Free();
		}
	}
}

// Implementation

void CMutex::ListAppend(void)
{
	AfxListAppend(m_pOwner->m_pOwnedHead,
		      m_pOwner->m_pOwnedTail,
		      this,
		      m_pNext,
		      m_pPrev
	);
}

void CMutex::ListRemove(void)
{
	AfxListRemove(m_pOwner->m_pOwnedHead,
		      m_pOwner->m_pOwnedTail,
		      this,
		      m_pNext,
		      m_pPrev
	);
}

// End of File
