
//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CATLINK_HPP
	
#define	INCLUDE_CATLINK_HPP

//////////////////////////////////////////////////////////////////////////
//
// i386 Stubs
//

#if defined(_M_IX86)

#define	HostMaxIPL() /**/

#define	HostMinIPL() /**/

#endif

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCatLinkBase;

class CCatLinkDriver;

class CCatLinkHandler;

//////////////////////////////////////////////////////////////////////////
//
// Special PIDs
//

#define	pidSerial	0xF810

#define	pidRPM		0x0040

#define	pidVoltage	0xF464

//////////////////////////////////////////////////////////////////////////
//
// Special Drops
//

#define	dropSelf	0xAA

#define	dropBroadcast	0x08

//////////////////////////////////////////////////////////////////////////
//
// Timeouts and Counts
//

#define	timeSystemTick	1000	// One second tick for system management

#define	secsSiteSurvey	5	// How often to check site config

#define	secsStdFaults	60	// How often to poll for faults (0xFA)

#define	secsAltFaults	4	// How often to poll for faults (0x82)

#define secsHitFaults	2	// How often to poll for faults (startup)

#define	secsDeferPoll	60	// How long to defer PIDs after timeouts

#define	timePingDrop	1500	// How often to ping when no activity

#define	timeRequest	200	// How long to wait for most replies

#define	timeStdDetail	400	// How long to wait for fault replies

#define	timeAltDetail	1000	// How long to wait for fault replies

#define	tryRead		4	// How many times to try a read (4 bits)

#define	tryRevert	30	// How often to retry first preference (5 bits)

#define	tryLoops	4	// Times to try all preferences before deferal (4 bits)

#define	tryWrite	4	// Times to try a write before giving up (4 bits)

#define	tryDetail	8	// Times to read fault details before giving up

#define	faultSlots	6	// How many fault messages buffered per MID

//////////////////////////////////////////////////////////////////////////
//
// Request Types
//

enum
{
	reqRead		= 1,
	reqWrite	= 2,
	reqDiags	= 3,
	reqEvents	= 4,
	reqDiagPoll	= 5,
	reqEventPoll	= 6,
	reqDiagDetail	= 7,
	reqEventDetail	= 8
	};

//////////////////////////////////////////////////////////////////////////
//
// MID Types
//

enum
{
	midOther	= 0,
	midECM		= 1,
	midGSC		= 2,
	midVIMS		= 3,
	midTran		= 4,
	midPay		= 5,
	midDenox	= 6,
	midImplement    = 7,
	midChassis	= 8,
	};

//////////////////////////////////////////////////////////////////////////
//
// Site Types
//

enum
{
	siteUnknown	= 0,
	siteStandard	= 1,
	siteMechanical	= 2,
	siteSlave1	= 3,
	siteSlave2	= 4,
	sitePort	= 5,
	siteCenter	= 6,
	siteStarboard	= 7,
	siteG3520C	= 8,
	siteG3600	= 9,
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Shared Functions
//

class CCatLinkBase
{
	protected:
		// PID Coding
		BOOL  IsValidPID(UINT uName);
		BOOL  IsWritablePID(UINT uName);
		PCTXT GetPIDCode(UINT uName);
		BOOL  SignExtendByte(UINT uName);
		BOOL  SignExtendWord(UINT uName);
		BOOL  ReOrderWord(UINT uName);
		BOOL  ReOrderLong(UINT uName);
		BOOL  IsDataValid(UINT uName, UINT uData);

		// Data and Name Access
		UINT GetNameLen(BYTE bData);
		UINT GetDataLen(BYTE bData);
		UINT GetNameLen(UINT uName);
		UINT GetDataLen(UINT uName);
		UINT GetNameBytes(PCBYTE pName, UINT uLen);
		UINT GetDataBytes(PCBYTE pData, UINT uLen);
		void SetNameBytes(PBYTE pName, UINT uName, UINT uLen);
		void SetDataBytes(PBYTE pData, UINT uData, UINT uLen);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Handler
//

class CCatLinkHandler : public IPortHandler, public CCatLinkBase
{	
	public:
		// Constructor
		CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver, BYTE bSelf);

		// Destructor
		~CCatLinkHandler(void);

		// Operations
		void Start(void);
		void Stop(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);

	protected:
		// Data Members
		ULONG            m_uRefs;
		IHelper        * m_pHelper;
		CCatLinkDriver * m_pDriver;
		BYTE		 m_bSelf;
		IPortObject    * m_pPort;
		BYTE		 m_bRxData[128];
		BYTE		 m_bTxData[128];
		UINT		 m_uRxPtr;
		UINT		 m_uTxPtr;
		UINT		 m_uTxSize;
		BOOL		 m_fRun;
		BOOL		 m_fSync;
		BOOL		 m_fSelf;
		UINT		 m_uCatTick;
		BOOL		 m_fCatConn;
		UINT		 m_uTimeout;

		// Frame Parsing
		BOOL ParseData(BYTE bSrc, BYTE bDest, PCBYTE pData, UINT uCount);
		BOOL IsUnknownFrame(PCBYTE pData);
		BOOL IsPollSequence(PCBYTE pData);

		// Port Managament
		void ClearContention(void);
		BOOL CheckContention(void);
		void StartEdgeTimer(BOOL fSlow);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver
//

class CCatLinkDriver : public CMasterDriver, public CCatLinkBase
{
	public:
		// Constructor
		CCatLinkDriver(void);

		// Destructor
		~CCatLinkDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping   (void);
		DEFMETH(CCODE) Read   (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write  (AREF Addr, PDWORD pData, UINT uCount);

		// Handler Calls
		void Handler_SeeDrop(BYTE bDrop);
		BOOL Handler_ReportPID(void);
		void Handler_SeePID(BYTE bDrop, UINT uName, UINT uData);
		UINT Handler_GetSendData(PBYTE pData);
		void Handler_GetNextSend(void);
		void Handler_WriteDone(BYTE bSrc, UINT uName);
		void Handler_DataFrame(BYTE bSrc, UINT uName, UINT uData);
		void Handler_FaultData(BYTE bSrc, PCBYTE pData, UINT uCount);
		void Handler_SerialNum(PCTXT pSerial, BYTE bSrc);
		void Handler_SetPoll(void);
		UINT Handler_GetConnectionTime(void);

	protected:
		// Signed Char
		typedef signed char SCHAR;

		// Site Layout
		#define siteCount 8

		// Site Info
		struct CSite
		{
			UINT	m_uType;
			BYTE	m_bGSC;
			BYTE	m_bECM1;
			BYTE	m_bECM2;
			BYTE	m_bECM3;
			BYTE	m_bICM1;
			BYTE	m_bICM2;
			BYTE	m_bVIMS;
			BYTE	m_bTran;
			BYTE    m_bPay;
			BYTE	m_bDenox;
			BYTE	m_bImplement;
			BYTE    m_bChassis;
			};

		// Request
		struct CReq
		{
			WORD	m_wType;
			WORD	m_wSlot;
			UINT	m_uData;
			UINT    m_uTime;
			PBYTE	m_pData;
			CReq  * m_pNext;
			CReq  * m_pPrev;
			};

		// MID Record
		struct CMID
		{
			BYTE	m_bDrop;
			BYTE	m_bType;
			CReq  * m_pSendHead;
			CReq  * m_pSendTail;
			CReq  * m_pDoneHead;
			CReq  * m_pDoneTail;
			CReq  * m_pPendHead;
			CReq  * m_pPendTail;
			UINT    m_uPendCount;
			UINT	m_uPendLimit;
			CMID  * m_pNext;
			CMID  * m_pPrev;
			BOOL	m_fSeen;
			UINT	m_uPing;
			UINT	m_uTime;
			UINT	m_uDiagPend;
			UINT	m_uEventPend;
			BYTE	m_bFaultFlag;
			};

		// PID Record
		struct CPID
		{
			UINT	m_uName;
			BYTE	m_bSrc;
			BYTE	m_bOkay   :5;
			BYTE	m_fValid  :1;
			BYTE	m_fDisable:1;
			BYTE	m_fForce  :1;
			BYTE	m_bSend   :4;
			BYTE	m_bPref   :4;
			BYTE	m_bLoop   :4;
			BYTE	m_bFail   :4;
			DWORD	m_Data;
			DWORD	m_Write;
			};

		// Fault Data
		struct CFault
		{
			BYTE	m_bSrc;
			BYTE	m_bSubcode;
			BYTE	m_bFlags;
			BYTE	m_bRaw;
			WORD	m_wCode;
			WORD	m_wCount;
			DWORD	m_dwFirst;
			DWORD	m_dwLast;
			UINT	m_uFetch;
			};

		// Data Members
		CCatLinkHandler * m_pHandler;
		BYTE		  m_bSelf;
		BOOL		  m_fGetFaults;
		BOOL		  m_fGetDetail;
		BOOL		  m_fAltFaults;
		UINT		  m_uStdPend;
		BOOL		  m_fSeeFaults;
		UINT		  m_uTryFaults;
		BOOL		  m_fWrites;
		BOOL		  m_fForce;
		char		  m_sSerial[12];
		CPID		* m_pPID[0x4100];
		CMID		* m_pHead;
		CMID		* m_pTail;
		CMID		* m_pScan;
		BYTE		  m_bDrop[256];
		BOOL		  m_fDropNew;
		UINT		  m_uTime;
		UINT		  m_uTick;
		CSite		  m_Site;
		CFault		  m_FaultList[80];
		CFault		  m_FaultWork[80];
		BOOL		  m_fFaultNew;
		UINT		  m_uFaultHWM;
		UINT		  m_uPoll[256];
		UINT		  m_pRepPID[360];
		BYTE		  m_pRepSRC[360];
		UINT		  m_pRepVAL[360];
		BOOL		  m_fReport;
		UINT		  m_uReport;
		UINT		  m_uConnTime;
			
		// Main Scan Loop
		void Update(void);
		void UpdateSite(void);
		void UpdatePolls(void);
		void UpdateDeferred(BYTE bSrc);
		void UpdateDeferred(void);
		void UpdateFaults(void);
		void UpdateMID(CMID *pMID);
		void UpdatePID(CPID *pPID, UINT uSlot);

		// Reply Processing
		void ProcessReply(CMID *pMID, CReq *pReq);
		void ProcessReadReply(CMID *pMID, CReq *pReq);
		void ProcessWriteReply(CMID *pMID, CReq *pReq);
		void ProcessDiagsReply(CMID *pMID, CReq *pReq);
		void ProcessEventsReply(CMID *pMID, CReq *pReq);
		void ProcessDiagDetailReply(CMID *pMID, CReq *pReq);
		void ProcessEventDetailReply(CMID *pMID, CReq *pReq);

		// Site Survey
		BOOL FindSiteType(CSite &Site);
		void CreateStandardMIDs(void);
		BYTE GetMIDType(BYTE bDrop);
		UINT GetKeyPID(BYTE bType);
		BOOL IsKeyPID(UINT uName);
		BOOL IsActiveMID(BYTE bDrop);
		BYTE FindECM(void);
		BYTE FindGSC(void);
		BYTE FindICM1(void);
		BYTE FindICM2(void);
		BYTE FindVIMS(void);
		BYTE FindTran(void);
		BYTE FindPay(void);
		BYTE FindDenox(void);
		BYTE FindImplement(void);
		BYTE FindChassis(void);
		UINT CountMID(BYTE bDrop);
		UINT CountMID(BYTE bFrom, BYTE bTo);

		// MID Scan Control
		BYTE GetSource(CPID *pPID);
		BYTE GetSource(CPID *pPID, UINT uPref);
		UINT GetRetryCount(CPID *pPID);
		UINT GetRevertCount(CPID *pPID);
		BOOL TryNextMID(CPID *pPID);

		// Fault List Management
		void ClearFaults(BYTE bDrop, BOOL fDiag);
		UINT FindFault(BYTE bDrop, WORD wCode, BOOL fDiag);
		UINT AllocFault(void);

		// PID List Management
		CPID * LookupPID(UINT uName);
		UINT   PIDToSlot(UINT uName);

		// MID List Management
		CMID * CreateMID(BYTE bDrop);
		CMID * LookupMID(BYTE bDrop);

		// Request Control
		void  QueueRequest(CMID *pMID, UINT uType, UINT uSlot);
		UINT  GetRequestTimeout(BYTE bDrop, CReq *pReq);
		PBYTE AllocRequestData(CReq *pReq);
		BOOL  GetReplyFlag(CReq *pReq);
		UINT  GetPendScore(CMID *pMID, CReq *pReq);
		UINT  GetPendLimit(BYTE bDrop);
		void  SetRequestDone(BYTE bDrop, CReq *pReq, BOOL fOkay);
		void  SetRequestDone(CMID *pMID, CReq *pReq, BOOL fOkay);
		void  CheckRequestTimeouts(CMID *pMID, UINT uTime);

		// Request Location
		CReq * FindRequest(BYTE bDrop, WORD wType, WORD wSlot);
		CReq * FindRequest(CMID *pMID, WORD wType, WORD wSlot);

		// Request Buidling
		UINT BuildRequest(PBYTE pData, BYTE bDrop, UINT uType, UINT uSlot);
		UINT BuildReadRequest(PBYTE pData, BYTE bDrop, UINT uSlot);
		UINT BuildWriteRequest(PBYTE pData, BYTE bDrop, UINT uSlot);
		UINT BuildDiagPollRequest(PBYTE pData, BYTE bDrop, UINT uSlot);
		UINT BuildEventPollRequest(PBYTE pData, BYTE bDrop, UINT uSlot);
		UINT BuildDiagDetailRequest(PBYTE pData, BYTE bDrop, UINT uSlot);
		UINT BuildEventDetailRequest(PBYTE pData, BYTE bDrop, UINT uSlot);

		// Timeout Helpers
		BOOL Timeout(UINT uTime);
		BOOL Timeout(UINT uTime, UINT uNow);
		UINT SetTimeout(UINT uTime);

		// Helpers
		BOOL IsGSC(BYTE bSrc);

		// Fault Sorting
		static int SortFunc(PCVOID p1, PCVOID p2);

		// Debug
		void ShowSiteInfo(void);
		void ShowFaults(void);
		void DumpQueue(CReq * &pHead);
	};

// End of File

#endif
