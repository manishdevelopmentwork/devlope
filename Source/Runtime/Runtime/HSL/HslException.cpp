
#include "Intern.hpp"

#include <sys/reent.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Exception Index (ARM)
//

#if !defined(AEON_PLAT_WIN32) && defined(AEON_PROC_ARM)

// Markers

clink BYTE  __text_start;
clink BYTE  __text_end;
clink INT64 __exidx_start;
clink INT64 __exidx_end;

// Static Data

static IExceptionIndex * m_pExcept  = NULL;

static PVOID             m_pContext = NULL;

// Code

global void Bind_ExceptionIndex(void)
{
	AfxGetObject("exidx", 0, IExceptionIndex, m_pExcept);

	m_pContext = m_pExcept->ArmRegister(&__text_start, &__text_end, &__exidx_start, &__exidx_end);
	}

global void Free_ExceptionIndex(void)
{
	m_pExcept->ArmDeregister(m_pContext);

	m_pExcept->Release();

	m_pExcept = NULL;
	}

clink void * __cxa_allocate_exception(int s)
{
	AfxAssert(m_pExcept);

	return m_pExcept->ExAlloc(s);
	}

clink void __cxa_free_exception(void *e)
{
	AfxAssert(m_pExcept);

	m_pExcept->ExFree(e);
	}

clink UINT __gnu_Unwind_Find_exidx(UINT Addr, int *pSize)
{
	if( m_pExcept ) {
	
		UINT Base;
		
		if( (Base = m_pExcept->ArmLookup(Addr, pSize)) ) {

			return Base;
			}
		}

	*pSize = &__exidx_end - &__exidx_start;

	return UINT(&__exidx_start);
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Exception Index (X86)
//

#if !defined(AEON_PLAT_WIN32) && defined(AEON_PROC_X86)

// Headers

#include "../../LibG/eh/dwarf2.h"

#include "../../LibG/eh/unwind-dw2-fde.h"

// Macros

#if !defined(__INTELLISENSE__)

#define EH_TOP __attribute__((section(".eh_frame_top"), aligned(4)))

#define EH_END __attribute__((section(".eh_frame_end"), aligned(4)))

#define EH_EXT

#else

#define EH_TOP

#define EH_END

#define EH_EXT 1

#endif

// Markers

clink const char  __EH_TOP__[EH_EXT] EH_TOP = { };

clink const DWORD __EH_END__         EH_END = 0;

// Static Data

static IExceptionIndex * m_pExcept  = NULL;

static PVOID             m_pContext = NULL;

static struct object     m_obj      = { 0 };

// Code

global void Bind_ExceptionIndex(void)
{
	AfxGetObject("exidx", 0, IExceptionIndex, m_pExcept);

	m_pContext = m_pExcept->X86Register(__EH_TOP__, &m_obj);
	}

global void Free_ExceptionIndex(void)
{
	m_pExcept->X86Deregister(m_pContext);

	m_pExcept->Release();

	m_pExcept = NULL;
	}

clink void * __cxa_allocate_exception(int s)
{
	AfxAssert(m_pExcept);

	return m_pExcept->ExAlloc(s);
	}

clink void __cxa_free_exception(void *e)
{
	AfxAssert(m_pExcept);

	m_pExcept->ExFree(e);
	}

clink const fde * _Unwind_Find_FDE(void *pc, struct dwarf_eh_bases *bases)
{
	AfxAssert(m_pExcept);
	
	const fde * pfde = NULL;

	m_pExcept->X86Lookup(&pfde, pc, bases);

	return pfde;
	}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Exception Index (Disabled)
//

#if defined(AEON_PLAT_WIN32)

// Common Code

global void Bind_ExceptionIndex(void)
{
	}

global void Free_ExceptionIndex(void)
{
	}

#endif

// End of File
