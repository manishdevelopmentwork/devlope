
#include "intern.hpp"

#include "RubyPath.hpp"

#include <assert.h>

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPoint.hpp"

#include "RubyVertex.hpp"

#include "RubyMatrix.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Path Object
//

// Constants

#define coLinear 0.01

// Constructors

CRubyPath::CRubyPath(void)
{
	m_nCount = 0;

	m_nStart = 0;

	m_nAlloc = 0;

	m_pv     = NULL;
	}

CRubyPath::CRubyPath(CRubyPath const &That)
{
	m_nCount = That.m_nCount;

	m_nStart = That.m_nStart;

	m_nAlloc = That.m_nCount;

	m_pv	 = ArrayAlloc(m_pv, m_nAlloc);

	m_m      = That.m_m;

	ArrayCopy(m_pv, That.m_pv, m_nCount);
	}

// Destructor

CRubyPath::~CRubyPath(void)
{
	free(m_pv);
	}

// Assignment

CRubyPath const & CRubyPath::operator = (CRubyPath const &That)
{
	if( That.m_nCount > m_nAlloc ) {

		m_nAlloc = That.m_nCount;

		m_pv     = ArrayReAlloc(m_pv, m_nAlloc);
		}

	m_nCount = That.m_nCount;

	m_nStart = That.m_nStart;

	m_m      = That.m_m;

	ArrayCopy(m_pv, That.m_pv, m_nCount);

	return ThisObject;
	}

// Attributes

int CRubyPath::GetSubCount(void) const
{
	int c = GetCount();

	int q = 0;

	for( int s = 0; s < c; ) {

		s += GetBreak(s);

		q += 1;
		}

	return q;
	}

int CRubyPath::GetGdiSubCount(void) const
{
	int c = GetCount();

	int q = 0;

	for( int s = 0; s < c; ) {

		s += GetBreak(s);

		q += 1;

		if( q % 2 ) {
			
			if( m_pv[s - 1].IsHardBreak() ) {

				q += 1;
				}
			}
		}

	return q;
	}

int CRubyPath::GetBreak(int n) const
{
	if( n < m_nCount ) {

		return m_pv[n].m_count;
		}

	return 0;
	}

// Operations

bool CRubyPath::Append(CRubyPoint const &p0, bool force)
{
	if( !force ) {

		// We want to do some checks to avoid building up
		// figures containing lots of identical or co-linear
		// points, as these just slow things down and cause
		// strange artifacts in the stroking process.

		if( m_nCount > m_nStart + 0 ) {

			CRubyVertex &p1 = m_pv[m_nCount-1];

			if( p0 == p1 ) {

				// The last two points would be identical
				// so we can just skip adding this point.

				return false;
				}

			if( m_nCount > m_nStart + 1 ) {

				CRubyVertex const &p2 = m_pv[m_nCount-2];

				CRubyVector u1 = p1 - p2;
				CRubyVector u2 = p0 - p1;

				// Check dot product to ensure the vectors
				// are oriented in the same direction.

				if( u1 * u2 > 0 ) {

					number s1 = u1.GetSquare();
					number s2 = u2.GetSquare();

					number co = u1 ^ u2;

					// This is equivalent to checking the ^ of the unit
					// vectors against coLinear but avoids having to do
					// two expensive square root operations.

					if( co * co < s1 * s2 * coLinear * coLinear ) {

						// The last three points would be co-linear or
						// nearly so, so we can just overwrite the last
						// point without any loss of fidelity.

						p1 = p0;

						return true;
						}
					}
				}
			}
		}

	// Allocate more memory if needed.

	if( m_nCount == m_nAlloc ) {

		m_nAlloc = GetNewAlloc();

		m_pv     = ArrayReAlloc(m_pv, m_nAlloc);
		}

	// Store the point and increment the cycle size.

	m_pv[m_nCount] = p0;

	m_pv[m_nStart].m_count++;

	m_nCount++;

	return true;
	}

bool CRubyPath::Append(CRubyPath const &p, int s, int c)
{
	for( int n = s; n < s + c; n++ ) {

		Append(p[n], true);
		}

	AppendHardBreak();

	return true;
	}

bool CRubyPath::AppendBreak(int mask)
{
	if( mask ) {

		if( mask == CRubyVertex::flagLineBreak ) {

			// Line breaks are like hard breaks but without
			// all the checks for degenerate wrap-around, so
			// we add the break and reset the cycle start.

			m_pv[m_nCount-1].m_flags = CRubyVertex::flagHardBreak;

			m_nStart                 = m_nCount;

			return true;
			}
		else {
			// We're at the end of the cycle so we need to check
			// for identical or co-linear points around the wrap
			// point. We can't do this when we're building the
			// cycle in the Append code, so it waits until now.

			while( m_nCount > m_nStart ) {

				int s = m_nStart;

				int e = m_nCount - 1;
		
				if( e > s ) {

					// If the first and last points are identical
					// we can remove the last point and try again.

					if( m_pv[s] == m_pv[e] ) {

						m_pv[s].m_count--;

						m_nCount--;

						continue;
						}
					}

				if( e > s + 1 ) {

					// If the first and the last two points are
					// co-linear or nearly so, we can remove the
					// last point and try again.

					CRubyVertex       &p1 = m_pv[e-1];
					CRubyVertex const &p2 = m_pv[e-0];
					CRubyVertex const &p3 = m_pv[s+0];

					CRubyVector u1 = p2 - p1;
					CRubyVector u2 = p3 - p2;

					// Check dot product to ensure the vectors
					// are oriented in the same direction.

					if( u1 * u2 > 0 ) {

						number s1 = u1.GetSquare();
						number s2 = u2.GetSquare();

						number co = u1 ^ u2;

						// This is equivalent to checking the ^ of the unit
						// vectors against coLinear but avoids having to do
						// two expensive square root operations.

						if( co * co < s1 * s2 * coLinear * coLinear ) {

							m_pv[s].m_count--;

							m_nCount--;

							continue;
							}
						}
					}

				// Add the break and reset the cycle start.

				m_pv[e].m_flags |= mask;

				m_nStart         = m_nCount;

				return true;
				}
			}
		}

	return false;
	}

bool CRubyPath::AppendSoftBreak(void)
{
	return AppendBreak(CRubyVertex::flagSoftBreak);
	}

bool CRubyPath::AppendHardBreak(void)
{
	return AppendBreak(CRubyVertex::flagHardBreak);
	}

bool CRubyPath::AppendLineBreak(void)
{
	return AppendBreak(CRubyVertex::flagLineBreak);
	}

bool CRubyPath::SoftenLastBreak(void)
{
	if( m_nCount ) {
		
		if( m_nStart == m_nCount ) {

			m_pv[m_nCount - 1].m_flags &= ~CRubyVertex::flagHardBreak;

			m_pv[m_nCount - 1].m_flags |=  CRubyVertex::flagSoftBreak;

			return true;
			}
		}

	return false;
	}

void CRubyPath::Reverse(int sub)
{
	int c = GetCount();

	int q = 1;

	for( int s = 0; s < c; ) {

		int b = GetBreak(s);

		if( !sub || sub == q++ ) {

			for( int n = 0; n < b / 2; n++ ) {

				int n1 = s + n;

				int n2 = s + b - 1 - n;

				Swap(m_pv[n1].m_x, m_pv[n2].m_x);

				Swap(m_pv[n1].m_y, m_pv[n2].m_y);
				}
			}

		if( sub ) {

			break;
			}

		s += b;
		}
	}

void CRubyPath::Transform(CRubyMatrix const &m)
{
	for( int n = 0; n < m_nCount; n++ ) {

		m_pv[n].Transform(m);
		}

	m_m.Compose(m);
	}

void CRubyPath::SetMatrix(CRubyMatrix const &m)
{
	m_m = m;
	}

// Hit Testing

bool CRubyPath::HitTest(CRubyPoint const &p1, int sub) const
{
	if( m_nCount ) {

		// We check if the point is within the path, using the ray casting
		// method. We extend a line from the test point and count how many
		// times it crosses the boundaries of the figure. If it's a odd
		// number, the point is inside the shape. The ray direction does
		// not matter, so we just extend 10000 pixels to the right.

		CRubyPoint p2;

		p2.m_x = p1.m_x + 10000;

		p2.m_y = p1.m_y + 0;

		int c = GetCount();

		int k = 0;

		int q = 1;

		for( int s = 0; s < c; ) {

			int b = GetBreak(s);

			if( !sub || sub == q++ ) {

				for( int n = 0; n < b; n++ ) {

					CRubyVertex const &p3 = m_pv[s+(n+0)%b];
			
					CRubyVertex const &p4 = m_pv[s+(n+1)%b];

					if( TestIntersection(p1, p2, p3, p4) ) {

						k++;
						}
					}

				if( m_pv[s+b-1].IsBreak() ) {

					if( sub || m_pv[s+b-1].IsHardBreak() ) {

						if( k % 2 ) {

							return true;
							}

						k = 0;
						}
					}
				}

			if( sub ) {

				break;
				}

			s += b;
			}
		}

	return false;
	}

void CRubyPath::GetBoundingRect(R2R &r) const
{
	r.m_x1 = +1000000;
	r.m_y1 = +1000000;
	r.m_x2 = -1000000;
	r.m_y2 = -1000000;

	AddBoundingRect(r);
	}

void CRubyPath::AddBoundingRect(R2R &r) const
{
	int c = GetCount();

	for( int n = 0; n < c; n++ ) {

		CRubyPoint const &p = m_pv[n];

		MakeMin(r.m_x1, p.m_x);
		MakeMin(r.m_y1, p.m_y);
		MakeMax(r.m_x2, p.m_x);
		MakeMax(r.m_y2, p.m_y);
		}
	}

void CRubyPath::GetBoundingRect(R2 &r) const
{
	r.x1 = +1000000;
	r.y1 = +1000000;
	r.x2 = -1000000;
	r.y2 = -1000000;

	AddBoundingRect(r);
	}

void CRubyPath::AddBoundingRect(R2 &r) const
{
	int c = GetCount();

	for( int n = 0; n < c; n++ ) {

		CRubyPoint const &p = m_pv[n];

		MakeMin(r.x1, int(p.m_x+0.000));
		MakeMin(r.y1, int(p.m_y+0.000));
		MakeMax(r.x2, int(p.m_x+0.999));
		MakeMax(r.y2, int(p.m_y+0.999));
		}
	}

// Debugging

void CRubyPath::Trace(PCTXT pName) const
{
	AfxTrace(L"%s = \n", pName);

	int c = GetCount();

	for( int s = 0; s < c; ) {

		int b = GetBreak(s);

		AfxTrace(L"  Block of %u vertices:\n", b);

		for( int n = 0; n < b; n++ ) {

			CRubyVertex const &p = m_pv[s+n];

			WCHAR s[16];

			wsprintf(s, L"    %-3u", n);

			p.Trace(s);
			}

		if( m_pv[s + b - 1].IsHardBreak() ) {

			AfxTrace(L"    Hard\n");
			}
		else
			AfxTrace(L"    Soft\n");

		s += b;
		}

	AfxTrace(L"  End\n");
	}

// Implementation

int CRubyPath::GetNewAlloc(void)
{
	return m_nAlloc ? 2 * m_nAlloc : 64;
	}

// Intersection Test

bool CRubyPath::TestIntersection(CRubyPoint const &p1, CRubyPoint const &p2, CRubyPoint const &p3, CRubyPoint const &p4)
{
	// Solve the equations to find the constants k1 and k2 that
	// represent where p1->p2 and p3->p4 overlap. t0 will be zero
	// if they are parallel. If k1 and k2 are between 0 and 1
	// then we overlap within the line segment. Other values are
	// for other points on the underlying infinite line.

	number t0 = (p1.m_x - p2.m_x) * (p4.m_y - p3.m_y) +
		    (p3.m_x - p4.m_x) * (p1.m_y - p2.m_y) ;

	if( !!t0 ) {

		// They're not parallel so solve the rest.

		number t1 = p1.m_x * (p4.m_y - p3.m_y) +
			    p3.m_x * (p1.m_y - p4.m_y) +
			    p4.m_x * (p3.m_y - p1.m_y) ;

		number t2 = p1.m_x * (p2.m_y - p3.m_y) +
			    p2.m_x * (p3.m_y - p1.m_y) +
			    p3.m_x * (p1.m_y - p2.m_y) ;

		number k1 = t1 / t0;
	
		number k2 = t2 / t0;

		// Check to see if the lines meet within their
		// bounds rather than at some projected point.

		if( k1 >= number(0) && k1 <= number(1) ) {
			
			if( k2 >= number(0) && k2 <= number(1) ) {

				return true;
				}
			}
		}

	return false;
	}

// End of File
