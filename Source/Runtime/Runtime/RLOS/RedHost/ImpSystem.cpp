
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Interfaces
//

#include "../../StdEnv/IRtc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Location Helper
//

bool LocateStaticObject(PCTXT pName, REFIID riid, UINT uInst, void **ppObject);

//////////////////////////////////////////////////////////////////////////
//
// System Call Implementation
//

// Data

static IRtc * m_pRtc = NULL;

// Code

clink void _exit(int code)
{
	for( ;;);
}

clink int _isatty(int fd)
{
	return -1;
}

clink int gettimeofday(timeval *pt, void const *tz)
{
	if( LocateStaticObject("rtc", AfxAeonIID(IRtc), 0, (void **) &m_pRtc) ) {

		m_pRtc->GetTime(pt);

		return 0;
	}

	return -1;
}

clink int settimeofday(timeval const *pt, void const *tz)
{
	if( LocateStaticObject("rtc", AfxAeonIID(IRtc), 0, (void **) &m_pRtc) ) {

		m_pRtc->SetTime(pt);

		return 0;
	}

	return -1;
}

clink time_t getmonosecs(void)
{
	if( LocateStaticObject("rtc", AfxAeonIID(IRtc), 0, (void **) &m_pRtc) ) {

		return m_pRtc->GetMonotonic();
	}

	return 0;
}

// End of File
