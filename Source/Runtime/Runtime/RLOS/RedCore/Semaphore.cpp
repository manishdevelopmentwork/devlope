
#include "Intern.hpp"

#include "Semaphore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "ExecThread.hpp"

#include "BaseHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Implementation
//

// Instantiators

IUnknown * CExecutive::Create_Semaphore(PCTXT pName)
{
	return New CSemaphore(m_pThis, 0);
	}

// Constructor

CSemaphore::CSemaphore(CExecutive *pExec, UINT uCount) : CWaitable(pExec)
{
	StdSetRef();

	m_nInit  = uCount;

	m_nCount = uCount;
	}

// IUnknown

HRESULT CSemaphore::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(ISemaphore);

	return E_NOINTERFACE;
	}

ULONG CSemaphore::AddRef(void)
{
	StdAddRef();
	}

ULONG CSemaphore::Release(void)
{
	StdRelease();
	}

// CWaitable

bool CSemaphore::WaitInit(CExecThread *pThread, UINT uSlot, bool fWait)
{
	if( m_Wake || !m_nCount ) {

		if( !fWait ) {

			return true;
			}

		if( !AppendThread(pThread, uSlot) ) {

			return true;
			}

		if( !m_nCount ) {

			return true;
			}

		RemoveThread(pThread, uSlot);
		}

	if( fWait && pThread->IsWaker(this) ) {

		return false;
		}

	AtomicDecrement(&m_nCount);

	return false;
	}

void CSemaphore::WaitDone(CExecThread *pThread, UINT uSlot)
{
	RemoveThread(pThread, uSlot);
	}

// IWaitable

PVOID CSemaphore::GetWaitable(void)
{
	return (CWaitable *) this;
	}

BOOL CSemaphore::Wait(UINT uTime)
{
	Hal_CheckMaxIrql(uTime ? IRQL_TASK : NOTHING);

	// This provides a fast path for waits that do not need to
	// block, avoiding the need to enter the more complex wait
	// code or to raise the system IRQL.

	UINT ipr;

	HostRaiseIpr(ipr);

	if( likely(m_nCount) ) {

		m_nCount--;

		HostLowerIpr(ipr);

		return true;
		}
	else {
		HostLowerIpr(ipr);

		if( !uTime ) {

			return false;
			}
		}

	return m_pExec->InitWait(this, uTime);
	}

BOOL CSemaphore::HasRequest(void)
{
	return m_Wake;
	}

// ISemaphore

void CSemaphore::Signal(UINT uCount)
{
	UINT ipr;

	HostRaiseIpr(ipr);

	if( likely(!m_Wake) ) {

		// This providers a fast path to signal the object when
		// no-one is waiting, avoiding the need to raise the IRQL.

		m_nCount += uCount;

		HostLowerIpr(ipr);
		}
	else {
		HostLowerIpr(ipr);

		UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

		while( uCount-- ) {
		
			if( !WakeOne(this) ) {

				AtomicIncrement(&m_nCount);
				}
			}

		Hal_LowerIrql(irql);
		}
	}

bool CSemaphore::Claim(UINT uCount)
{
	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	if( m_nCount > INT(uCount) ) {

		m_nCount -= uCount;

		Hal_LowerIrql(irql);

		return true;
		}
		
	Hal_LowerIrql(irql);

	return false;
	}

void CSemaphore::ResetState(void)
{
	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	WakeAll(this);

	m_nCount = m_nInit;

	Hal_LowerIrql(irql);
	}

// End of File
