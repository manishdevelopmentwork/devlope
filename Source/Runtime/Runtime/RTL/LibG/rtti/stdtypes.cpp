
#include <typeinfo>

namespace __cxxabiv1
{
	class __fundamental_type_info : public std::type_info
	{
		public:
			explicit __fundamental_type_info(const char* __n);

			virtual ~__fundamental_type_info(void);
		};

	__fundamental_type_info::__fundamental_type_info(const char* __n) : std::type_info(__n)
	{
		}

	__fundamental_type_info::~__fundamental_type_info(void)
	{
		}
	};
