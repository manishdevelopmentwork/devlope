
#include "Intern.hpp"

#include "HttpClientAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Authentication Method
//

// Operations

void CHttpClientAuth::Reset(void)
{
	m_uState = 0;
	}

void CHttpClientAuth::SetCredentials(CString User, CString Pass)
{
	m_User = User;

	m_Pass = Pass;
	}

BOOL CHttpClientAuth::CanAccept(CString Meth, UINT &uPriority)
{
	return FALSE;
	}

BOOL CHttpClientAuth::ProcessRequest(CString Line, BOOL fFail)
{
	return FALSE;
	}

BOOL CHttpClientAuth::ProcessInfo(CString Line, CString Path, PCBYTE pBody, UINT uBody)
{
	return TRUE;
	}

CString CHttpClientAuth::GetAuthHeader(CString Verb, CString Path, CBytes Body)
{
	return "";
	}

// End of File
