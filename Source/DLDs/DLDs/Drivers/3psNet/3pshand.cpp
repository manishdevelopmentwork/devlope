
#include "3psnet.hpp"

/////////////////////////////////////////////////////////////////////////
//
// 3psNet Handler
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Constructor

C3psNetHandler::C3psNetHandler(IHelper *pHelper, C3psNetDriver *pDriver)
{
	StdSetRef();

	m_uDEVs    = 0;

	m_pDEVs	   = NULL;

	m_pTxData  = NULL;

	m_pPort    = NULL;

	m_pHelper  = pHelper;

	m_pDriver  = pDriver;

	m_bSrc	   = 0;
}

// Destructor

C3psNetHandler::~C3psNetHandler(void)
{

}

// IUnknown

HRESULT C3psNetHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG C3psNetHandler::AddRef(void)
{
	StdAddRef();
}

ULONG C3psNetHandler::Release(void)
{
	StdRelease();
}

// IPortHandler

void C3psNetHandler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
}

void C3psNetHandler::OnOpen(CSerialConfig const &Config)
{
	m_pTxData    = NULL;

	m_uRx        = 0;

	m_uTx	     = 0;
}

void C3psNetHandler::OnClose(void)
{
}

void C3psNetHandler::OnRxData(BYTE bData)
{
	if( m_uRx < sizeof(FRAME) ) {

		PBYTE pRx = PBYTE(&m_Rx);

		pRx[m_uRx] = bData;

		m_uRx++;
	}
}

void C3psNetHandler::OnRxDone(void)
{
	if( m_uRx == sizeof(FRAME) ) {

		HandleFrame();

		m_uRx = 0;
	}
}

BOOL C3psNetHandler::OnTxData(BYTE &bData)
{
	if( m_uTx > 0 ) {

		if( m_pTxData ) {

			m_uTx--;

			bData = *m_pTxData++;

			return TRUE;
		}
	}

	m_pTxData = NULL;

	return FALSE;
}

void C3psNetHandler::OnTxDone(void)
{
	m_pTxData = NULL;

	m_uTx = 0;
}

void C3psNetHandler::OnTimer(void)
{

}

// Explicit Messages

BOOL C3psNetHandler::SendMessage(BYTE bDev, WORD wID, BYTE bMsg, PDWORD pData)
{
	DEV * pDev = FindDevice(wID, bDev);

	if( pDev ) {

		if( pDev->m_bWireless ) {

			bMsg |= 0x80;
		}

		UINT uIndex;

		Start(uIndex);

		AddByte(bDev, uIndex);

		AddByte(LOBYTE(wID), uIndex);

		AddByte(HIBYTE(wID), uIndex);

		AddByte(bMsg, uIndex);

		AddData(pData, uIndex);

		m_Tx.bCount = uIndex;

		PutFrame();

		return TRUE;
	}

	return FALSE;
}

BOOL C3psNetHandler::SendMessageReq(BYTE bDev, WORD wID, BYTE bMsg)
{
	DEV * pDev = FindDevice(wID, bDev);

	if( pDev ) {

		if( pDev->m_bWireless ) {

			bMsg |= 0x80;
		}

		UINT uIndex;

		Start(uIndex);

		AddByte(bDev, uIndex);

		AddByte(LOBYTE(wID), uIndex);

		AddByte(HIBYTE(wID), uIndex);

		AddByte(bMsg, uIndex);

		pDev->m_bMessage = bMsg;

		pDev->m_bSet = 0;

		m_Tx.bCount = uIndex;

		PutFrame();

		return TRUE;
	}

	return FALSE;
}

// Data Access

void C3psNetHandler::MakeDEV(WORD wId, BYTE bType, BYTE bWireless, UINT uLive, UINT uTran)
{
	DEV * pDEV        = new DEV;

	pDEV->m_ID        = wId;

	pDEV->m_bType     = bType;

	pDEV->m_bWireless = bWireless;

	pDEV->m_uInterval = uLive;

	pDEV->m_uTransact = uTran;

	GrowDEVs(pDEV);

	delete pDEV;
}

DEV* C3psNetHandler::FindDevice(WORD wId, BYTE bType)
{
	for( UINT u = 0; u < m_uDEVs; u++ ) {

		if( wId == m_pDEVs[u].m_ID ) {

			if( bType ) {

				if( bType == m_pDEVs[u].m_bType ) {

					return &m_pDEVs[u];
				}
			}
			else {
				return &m_pDEVs[u];
			}
		}
	}

	return NULL;
}

// Public Helpers

BOOL C3psNetHandler::HasBroadcast(DEV * pDEV)
{
	return !IsTimedOut(pDEV->m_uTime, pDEV->m_uInterval);
}

BOOL C3psNetHandler::HasExplicit(DEV * pDEV)
{
	return pDEV->m_bSet ? TRUE : FALSE;
}

void C3psNetHandler::SetSource(BYTE bSrc)
{
	m_bSrc = bSrc;
}

void C3psNetHandler::Clear(DEV * pDEV)
{
	pDEV->m_uTime = 0;
}

// Implementation

void C3psNetHandler::Start(UINT &uIndex)
{
	memset(&m_Tx, 0x0, sizeof(FRAME));

	m_Tx.wID = m_bSrc;

	uIndex = 0;
}

void C3psNetHandler::AddByte(BYTE bByte, UINT &uIndex)
{
	m_Tx.bData[uIndex++] = bByte;
}

void C3psNetHandler::AddData(PDWORD pData, UINT &uIndex)
{
	AddByte(LOBYTE(LOWORD(pData[0])), uIndex);

	AddByte(HIBYTE(LOWORD(pData[0])), uIndex);

	AddByte(LOBYTE(HIWORD(pData[0])), uIndex);

	AddByte(HIBYTE(HIWORD(pData[0])), uIndex);
}

void C3psNetHandler::GrowDEVs(DEV * pDEV)
{
	DEV * pDEVList = new DEV[m_uDEVs + 1];

	UINT uSize = 0;

	for( UINT a = 0; a < m_uDEVs; a++ ) {

		pDEVList[a].m_ID	= m_pDEVs[a].m_ID;

		pDEVList[a].m_bType	= m_pDEVs[a].m_bType;

		pDEVList[a].m_bWireless = m_pDEVs[a].m_bWireless;

		pDEVList[a].m_uTime	= m_pDEVs[a].m_uTime;

		pDEVList[a].m_uInterval = m_pDEVs[a].m_uInterval;

		pDEVList[a].m_uTransact = m_pDEVs[a].m_uTransact;
	}

	pDEVList[m_uDEVs].m_ID		= pDEV->m_ID;

	pDEVList[m_uDEVs].m_bType	= pDEV->m_bType;

	pDEVList[m_uDEVs].m_bWireless	= pDEV->m_bWireless;

	pDEVList[m_uDEVs].m_uTime	= 0;

	pDEVList[m_uDEVs].m_uInterval	= pDEV->m_uInterval;

	pDEVList[m_uDEVs].m_uTransact	= pDEV->m_uTransact;

	delete[] m_pDEVs;

	m_pDEVs = pDEVList;

	m_uDEVs++;
}

BOOL C3psNetHandler::PutFrame(void)
{
	SendFrame();

	return TRUE;
}

void C3psNetHandler::SendFrame(void)
{
	m_pTxData = PCBYTE(&m_Tx);

	m_uTx     = sizeof(FRAME) - 1;

	m_pPort->Send(*m_pTxData++);
}

void C3psNetHandler::HandleFrame(void)
{
	m_RxData = m_Rx;

	DEV * pDEV = FindDevice(m_RxData.wID, m_RxData.bData[0]);

	if( pDEV ) {

		if( IsBroadcast() ) {

			SetBroadcast(pDEV);

			return;
		}

		if( IsExplicit() ) {

			SetExplicit(pDEV);

			return;
		}
	}
}

BOOL C3psNetHandler::SetBroadcast(DEV * pDEV)
{
	if( pDEV ) {

		pDEV->m_bStatus	 = m_RxData.bData[1];

		pDEV->m_bBattery = m_RxData.bData[2];

		pDEV->m_bTemp    = m_RxData.bData[3];

		DWORD x  = PU4(m_RxData.bData + 4)[0];

		pDEV->m_pData[msgBroadcast] = IntelToHost(x);

		UINT uTick = GetTickCount();

		pDEV->m_uTime = uTick ? uTick : 1;

		return TRUE;
	}

	return FALSE;
}

BOOL C3psNetHandler::SetExplicit(DEV * pDEV)
{
	if( pDEV ) {

		if( m_RxData.bData[3] == pDEV->m_bMessage ) {

			DWORD x  = PU4(m_RxData.bData + 4)[0];

			pDEV->m_pData[msgExplicit] = IntelToHost(x);

			pDEV->m_bSet = 1;

			return TRUE;
		}
	}

	return FALSE;
}

// Helpers

BOOL C3psNetHandler::IsBroadcast(void)
{
	return m_RxData.bData[0] != 0;
}

BOOL C3psNetHandler::IsExplicit(void)
{
	return m_RxData.bData[0] == 0;
}

BOOL C3psNetHandler::IsTimedOut(UINT uTime, UINT uSpan)
{
	return int(GetTickCount() - uTime - ToTicks(uSpan * 5)) >= 0;
}

// Testing / Debug

void C3psNetHandler::ShowDEVs(void)
{
	for( UINT u = 0; u < m_uDEVs; u++ ) {

		AfxTrace("DEV %u of %u... id %u type %u\n", u + 1, m_uDEVs, m_pDEVs[u].m_ID, m_pDEVs[u].m_bType);
	}
}

// End of File
