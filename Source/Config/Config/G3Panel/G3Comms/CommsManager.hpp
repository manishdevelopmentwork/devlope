
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsManager_HPP

#define INCLUDE_CommsManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsMapData;
class CCommsPort;
class CCommsPortList;
class CCommsSysBlock;
class CConnectors;
class CEthernetItem;
class CExpansionItem;
class COptionCardItem;
class CRackItem;
class CServices;
class CUSBHostItem;

//////////////////////////////////////////////////////////////////////////
//
// Communications Manager
//

class DLLAPI CCommsManager : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsManager(void);

		// Item Lookup
		CCommsPort     * FindPort  (UINT uPort)   const;
		CCommsPort     * FindPort  (CString Name) const;
		CCommsDevice   * FindDevice(UINT uDevice) const;
		CCommsDevice   * FindDevice(CString Name) const;
		CCommsSysBlock * FindBlock (UINT uBlock ) const;

		// Ports List Access
		BOOL GetPortList(CCommsPortList * &pList, UINT uIndex) const;

		// Attributes
		DWORD GetRefAddress(DWORD ID) const;
		UINT  GetRefDevice(DWORD ID) const;
		BOOL  CanStepFragment(CString Code) const;
		BOOL  IsFragmentBit(CString Code) const;
		UINT  GetMaxPortNumber(void) const;
		UINT  GetMaxDeviceNumber(void) const;
		UINT  GetMaxBlockNumber(void) const;

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		BOOL ResolveMapData(CString &Text, CCommsMapData const *pMap);
		BOOL ResolveAddress(DWORD &ID, UINT uDevice, CAddress Addr);
		BOOL ResolveAddress(DWORD &ID, UINT uDevice, CAddress Addr, INT nCount);
		BOOL FindDirect(CError *pError, CString Name, DWORD &ID, CTypeDef &Type);
		BOOL FindDirect(CError *pError, CString Name, INT nCount, DWORD &ID, CTypeDef &Type);
		BOOL NameDirect(DWORD ID, CString &Name);
		BOOL StepFragment(CString &Code, UINT uStep);
		UINT AllocPortNumber(void);
		UINT AllocDeviceNumber(void);
		UINT AllocBlockNumber(void);
		void RegisterPortNumber(UINT n);
		void RegisterDeviceNumber(UINT n);
		void RegisterBlockNumber(UINT n);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Naming
		CString GetHumanName(void) const;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT		     m_Handle;
		UINT                 m_NoComms;
		UINT		     m_MaxPort;
		UINT		     m_MaxDevice;
		UINT		     m_MaxBlock;
		UINT		     m_StrPad;
		UINT		     m_LedAlarm;
		UINT		     m_LedOrb;
		UINT		     m_LedHome;
		CCodedItem	   * m_pOnEarly;
		CCodedItem	   * m_pOnStart;
		CCodedItem	   * m_pOnApply;
		CCodedItem         * m_pOnSecond;
		COptionCardItem	   * m_pOption0;
		COptionCardItem	   * m_pOption1;
		CEthernetItem	   * m_pEthernet;
		CCommsPortList     * m_pPorts;
		CServices	   * m_pServices;
		CConnectors        * m_pConnectors;
		CUSBHostItem	   * m_pUSBHost;
		CRackItem          * m_pRack;
		CExpansionItem	   * m_pExpansion;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT FindDevice(CError *pError, CString &Name) const;
		BOOL IsCommsRef(CString const &Code) const;
		BOOL IsNamedArray(ICommsDriver *pDriver) const;
	};

// End of File

#endif
