
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextEnumMail_HPP

#define INCLUDE_UITextEnumMail_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMailManager;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Mail Recipient
//

class CUITextEnumMail : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextEnumMail(void);

	protected:
		// Data
		CMailManager *m_pMail;

		// Overridables
		void OnBind(void);
		BOOL OnEnumValues(CStringArray &List);

		// Implementation
		void FindMailManager(void);
		void AddMailContacts(void);
	};

// End of File

#endif
