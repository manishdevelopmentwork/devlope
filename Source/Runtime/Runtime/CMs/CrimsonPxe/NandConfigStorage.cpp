
#include "Intern.hpp"

#include "NandConfigStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IDatabase * Create_NandDatabase(UINT uStart, UINT uEnd, UINT uFram, UINT uBase, UINT uPool);

//////////////////////////////////////////////////////////////////////////
//
// NAND Config Storage Object
//

// Instantiator

IConfigStorage * Create_NandConfigStorage(UINT uStart, UINT uEnd)
{
	return new CNandConfigStorage(uStart, uEnd);
}

// Constructor

CNandConfigStorage::CNandConfigStorage(UINT uStart, UINT uEnd) : CBaseConfigStorage(Create_NandDatabase(uStart, uEnd, 0, 0, 1024))
{
}

// End of File
