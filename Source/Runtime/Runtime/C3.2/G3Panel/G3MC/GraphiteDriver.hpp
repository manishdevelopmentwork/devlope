
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GraphiteDriver_HPP

#define	INCLUDE_GraphiteDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Interfaces
//

#include "RackInterface.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphite Driver
//

class CGraphiteDriver : public IRackDriver
{
public:
	// Constructor
	CGraphiteDriver(IRackHandler *pHandler);

	// Destructor
	~CGraphiteDriver(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IRackDriver
	UINT RxGeneral(BYTE bDrop);
	UINT BootTxForceReset(BYTE bDrop);
	UINT BootTxCheckModel(BYTE bDrop);
	UINT BootTxCheckVersion(BYTE bDrop, PBYTE pGuid);
	UINT BootTxProgramReset(BYTE bDrop);
	UINT BootTxClearProgram(BYTE bDrop, DWORD dwSize);
	UINT BootTxProgramSize(BYTE bDrop, DWORD dwSize);
	UINT BootTxWriteProgram(BYTE bDrop, WORD wAddr, PBYTE pData, WORD wCount);
	UINT BootTxWriteProgram32(BYTE bDrop, DWORD dwAddr, PBYTE pData, WORD wCount);
	UINT BootTxWriteVerify(BYTE bDrop, DWORD dwCrc32);
	UINT BootTxWriteVersion(BYTE bDrop, PBYTE pGuid);
	UINT BootTxStartProgram(BYTE bDrop);
	UINT BootRxCheckModel(BYTE bDrop, BYTE &ModelID);
	UINT BootRxWriteVerify(BYTE bDrop, BOOL &fSame);
	UINT BootRxCheckVersion(BYTE bDrop, BOOL &fSame);
	UINT ConfigTxCheckVersion(BYTE bDrop, PBYTE pGuid);
	UINT ConfigTxClearConfig(BYTE bDrop);
	UINT ConfigTxWriteConfig(BYTE bDrop, PCBYTE &pData);
	UINT ConfigTxWriteVersion(BYTE bDrop, PBYTE pGuid);
	UINT ConfigTxStartSystem(BYTE bDrop);
	UINT ConfigTxCheckStatus(BYTE bDrop);
	UINT ConfigRxCheckVersion(BYTE bDrop, BOOL &fSame);
	UINT ConfigRxWriteConfig(BYTE bDrop);
	UINT ConfigRxCheckStatus(BYTE bDrop, BOOL &fRun);
	void DataTxStartFrame(void);
	BOOL DataTxWrite(WORD PropID, DWORD Data);
	BOOL DataTxRead(WORD PropID);
	UINT DataTxSend(BYTE bDrop);
	UINT DataRxData(BYTE bDrop, PBYTE &pData);
	BOOL DataTooFull(WORD PropID);
	UINT TunnelTx(BYTE bDrop, PBYTE pData);
	UINT TunnelRx(BYTE bDrop, PBYTE pData);
	UINT SendEnq(BYTE bDrop);
	BOOL Recycle(BYTE bDrop);

protected:
	// Service Codes
	enum
	{
		servBoot		= 0x01,
		servConfig		= 0x02,
		servData		= 0x03,
		servCalib		= 0x04,
		servTunnel		= 0x05,
	};

	// Shared
	enum
	{
		opAck			= 0x01,
		opNak			= 0x02,
		opReply			= 0x03,
	};

	// Boot Loader
	enum
	{
		bootCheckVersion	= 0x10,
		bootClearProgram	= 0x11,
		bootWriteProgram	= 0x12,
		bootWriteVersion	= 0x13,
		bootStartProgram	= 0x14,
		bootCheckHardware	= 0x15,
		bootForceReset		= 0x16,
		bootWriteMac		= 0x17,
		bootCheckModel		= 0x18,
		bootProgramSize		= 0x19,
		bootWriteProgram32	= 0x1A,
	};

	// Configuration Opcodes
	enum
	{
		configCheckVersion	= 0x10,
		configClearConfig	= 0x11,
		configWriteConfig	= 0x12,
		configWriteVersion	= 0x13,
		configStartSystem	= 0x14,
		configCheckStatus	= 0x15,
		configWriteConfig1	= 0x16,
	};

	// Data Transfer Opcodes
	enum
	{
		dataData		= 0x10,
	};

	// Calibration Opcodes
	enum
	{
		calibRequest		= 0x10,
		calibRead		= 0x11,
	};

	// Tunneling Opcodes
	enum
	{
		tunnelTunnel		= 0x10,
	};

	// Data
	ULONG           m_uRefs;
	IRackHandler  * m_pHandler;
	BYTE		m_bData[PACKET_SIZE];
	BYTE		m_bSeq;
	BYTE		m_bPtr;

	// Frame Building
	void NewPacket(BYTE bService, BYTE bOpcode);
	void AddByte(BYTE bData);
	void AddWord(WORD wData);
	void AddLong(DWORD dwData);
	void AddData(PBYTE pData, UINT uCount);

	// Transport
	UINT PutFrame(BYTE bDrop);
};

// End of File

#endif
