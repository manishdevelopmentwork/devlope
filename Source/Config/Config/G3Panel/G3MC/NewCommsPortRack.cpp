
#include "intern.hpp"

#include "NewCommsPortRack.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "NewCommsDeviceRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CNewCommsPortRack, CCommsPort);

// Constructor

CNewCommsPortRack::CNewCommsPortRack(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding  = bindRack;

	m_PortPhys = 100;

	m_PortLog  = 0;

	m_pDevices->SetItemClass(AfxRuntimeClass(CNewCommsDeviceRack));
	}

// Operations

void CNewCommsPortRack::DeleteModuleDevices(void)
{
	UINT n;

	while( (n = m_pDevices->GetItemCount()) > 1 ) {

		m_pDevices->DeleteItem(m_pDevices->GetTail());
	}
}

// UI Creation

CViewWnd * CNewCommsPortRack::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CNewRackNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return NULL;
	}

// Driver Creation

ICommsDriver * CNewCommsPortRack::CreateDriver(UINT uID)
{
	if( m_DriverID == 0xFE01 ) {

		return New CRackDriver;
		}

	return CCommsPort::CreateDriver(uID);
	}

// Item Naming

BOOL CNewCommsPortRack::IsHumanRoot(void) const
{
	return TRUE;
	}

CString CNewCommsPortRack::GetHumanName(void) const
{
	return IDS("I/O Modules");
	}

CString CNewCommsPortRack::GetItemOrdinal(void) const
{
	return L"";
	}

// Persistance

void CNewCommsPortRack::Init(void)
{
	CCommsPort::Init();

	m_DriverID = 0xFE01;

	CheckDriver();

	CNewCommsDeviceRack *pDev = New CNewCommsDeviceRack;

	m_pDevices->AppendItem(pDev);

	pDev->SetClass(AfxNamedClass(L"CMasterModule"));

	pDev->SetName (L"Master");
	}

// Conversion

void CNewCommsPortRack::PostConvert(void)
{
	INDEX n = m_pDevices->GetHead();

	while( !m_pDevices->Failed(n) ) {

		CNewCommsDeviceRack *pDev = (CNewCommsDeviceRack *) m_pDevices->GetItem(n);

		if( pDev->m_pModule->IsKindOf(AfxNamedClass(L"CMasterModule")) ) {

			m_pDevices->GetNext(n);
		}
		else {
			CString Name = pDev->m_pModule->GetClassName();

			CString Conv = pDev->m_pModule->GetClassConv();

			if( !Conv.IsEmpty() ) {

				pDev->PostConvert(Conv);

				m_pDevices->GetNext(n);

				continue;
			}

			INDEX d = n;

			m_pDevices->GetNext(n);

			m_pDevices->DeleteItem(d);
		}
	}
}

// Download Support

BOOL CNewCommsPortRack::MakeInitData(CInitData &Init)
{
	Init.AddByte(10);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CNewCommsPortRack::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_SetName((IDS_MODULES));
	}

// End of File
