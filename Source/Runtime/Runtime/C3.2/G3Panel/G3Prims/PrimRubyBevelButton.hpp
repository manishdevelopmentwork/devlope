
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBevelButton_HPP
	
#define	INCLUDE_PrimRubyBevelButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBevelBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitive
//

class CPrimRubyBevelButton : public CPrimRubyBevelBase
{
	public:
		// Constructor
		CPrimRubyBevelButton(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
	};

// End of File

#endif
