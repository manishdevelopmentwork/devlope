
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PseudoHeader_HPP

#define	INCLUDE_PseudoHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpHeader;

//////////////////////////////////////////////////////////////////////////
//
// Pseudo Header
//

#pragma pack(1)

struct PSHEADER
{
	CIpAddr	m_Src;
	CIpAddr	m_Dest;
	BYTE	m_Zero;
	BYTE	m_Protocol;
	WORD	m_Length;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Pseudo Header Wrapper
//

class CPsHeader : public PSHEADER
{
	public:
		// Constructors
		CPsHeader(void);
		CPsHeader(CIpHeader *pIp);

		// Attributes
		DWORD GetChecksum(void) const;
		WORD  GetPrimeSum(void) const;

		// Operations
		void MakeHeader(CIpHeader *pIp);
	};

// End of File

#endif
