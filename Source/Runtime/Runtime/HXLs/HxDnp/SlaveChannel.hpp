
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SlaveChannel_HPP

#define	INCLUDE_SlaveChannel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "Channel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Slave Channel Object - Port
//

class CSlaveChannel : public CChannel
{
	public:
		// Constructor
		CSlaveChannel(IDataHandler * pHandler, WORD wSrc);
		CSlaveChannel(IDnpChannelConfig * pConfig, WORD wSrc);

		// Destructor
		~CSlaveChannel(void);

		// IDnpChannel
		IDnpSession * METHOD OpenSession(WORD wDest, WORD wTO, DWORD dwLink, void * pCfg);

	protected:
		// Data Members
		cfgSSes * m_pSession[1];
};

// End of File

#endif
