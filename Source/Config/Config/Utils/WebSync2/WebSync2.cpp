
#include "Intern.hpp"

// Static Data

static	string	m_Host;

static	string	m_User;

static	string	m_Pass;

static	string	m_Dest;

static	bool	m_fVerbose = false;

static	bool	m_fForm    = false;

static	bool	m_fBatch   = false;

static	bool	m_fBanner  = true;

static	bool	m_fTls	   = false;

// Prototypes

global	int	main(int nArg, char *pArg[]);
static	void	ShowUsage(void);
static	void	ShowBanner(void);
static	void	Error(char const *pText, ...);
static	void	AfxTrace(char const *pText, ...);
static	bool	ParseCommandLine(int nArg, char *pArg[]);
static	bool	Process(void);
static	bool	FormLogon(HINTERNET hConn);
static	bool	SyncTop(HINTERNET hConn);
static	bool	SyncBatches(HINTERNET hConn);
static	bool	SyncIndex(HINTERNET hConn, string const &batch);
static	bool	SyncLog(HINTERNET hConn, string const &batch, string const &log, string const &path);
static	bool	SyncFile(HINTERNET hConn, string const &batch, string const &log, string const &path, string const &file, UINT size);
static	bool	FetchPage(HINTERNET hConn, string &Page, string const &Path);
static	string	GetHref(string const &element);
static	string	GetUrlParam(string const &url, string name);
static	bool	MakeDirectory(string const &path);

// Code

int main(int nArg, char *pArg[])
{
	if( ParseCommandLine(nArg, pArg) ) {

		Process();

		return 0;
	}

	ShowUsage();

	return 2;
}

static void ShowUsage(void)
{
	ShowBanner();

	printf("usage: WebSync2 [switches] [protocol://]host[:port]\n\n");

	printf("Available switches are:\n\n");

	printf("  -d <path>      Specify destination path\n");
	printf("  -u <name>      Specify website user name\n");
	printf("  -p <password>  Specify website password\n");
	printf("  -f             Use form logon mode\n");
	printf("  -v             Enable verbose mode\n");
	printf("  -b             Include batch logs in sync\n");

	printf("\n");

	exit(2);
}

static void ShowBanner(void)
{
	printf("Crimson Web Sync Utility Version 3.20\n");
	printf("Copyright (c) 2020 Red Lion Controls Inc\n");
	printf("\n");
}

static void Error(char const *pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	fprintf(stderr, "\nWebSync2: ");

	vfprintf(stderr, pText, pArgs);

	fprintf(stderr, "\n");

	va_end(pArgs);

	exit(1);
}

static void AfxTrace(char const *pText, ...)
{
	if( m_fVerbose ) {

		if( m_fBanner ) {

			ShowBanner();
		}

		va_list pArgs;

		va_start(pArgs, pText);

		vfprintf(stdout, pText, pArgs);

		va_end(pArgs);

		m_fBanner = false;
	}
}

static bool ParseCommandLine(int nArg, char *pArg[])
{
	if( nArg >= 2 ) {

		for( int n = 1; n < nArg; n++ ) {

			if( pArg[n][0] == '-' ) {

				switch( tolower(pArg[n][1]) ) {

					case 'v':
					{
						m_fVerbose = true;
					}
					break;

					case 'f':
					{
						m_fForm = true;
					}
					break;

					case 'b':
					{
						m_fBatch = true;
					}
					break;

					case 'd':
					{
						if( m_Dest.size() ) {

							Error("repeated destination");
						}

						if( ++n == nArg ) {

							Error("missing destination");
						}

						m_Dest = pArg[n];
					}
					break;

					case 'u':
					{
						if( m_User.size() ) {

							Error("repeated username");
						}

						if( ++n == nArg ) {

							Error("missing username");
						}

						m_User = pArg[n];
					}
					break;

					case 'p':
					{
						if( m_Pass.size() ) {

							Error("repeated password");
						}

						if( ++n == nArg ) {

							Error("missing password");
						}

						m_Pass = pArg[n];
					}
					break;

					default:
					{
						Error("unknown switch");
					}
					break;
				}
			}
			else {
				if( !m_Host.empty() ) {

					Error("extra characters on line");
				}

				m_Host = pArg[n];
			}
		}

		if( m_Host.empty() ) {

			Error("missing host");
		}

		if( m_Dest.empty() ) {

			m_Dest = ".";
		}

		return true;
	}

	return false;
}

static bool Process(void)
{
	HINTERNET hNet = InternetOpen("Crimson WebSync",
				      INTERNET_OPEN_TYPE_PRECONFIG,
				      NULL,
				      NULL,
				      0
	);

	if( hNet ) {

		ULONG uTime1 = 60;

		ULONG uTime2 = 0xFFFFFFFF;

		InternetSetOption(hNet,
				  INTERNET_OPTION_CONNECT_TIMEOUT,
				  &uTime1,
				  sizeof(uTime1)
		);

		InternetSetOption(hNet,
				  INTERNET_OPTION_RECEIVE_TIMEOUT,
				  &uTime2,
				  sizeof(uTime2)
		);

		InternetSetOption(hNet,
				  INTERNET_OPTION_SEND_TIMEOUT,
				  &uTime2,
				  sizeof(uTime2)
		);

		AfxTrace("Connecting to %s\n", m_Host.c_str());

		UINT   port = 80;

		string reg  = "http://";

		string sec  = "https://";

		size_t pos  = 0;

		if( m_Host.substr(0, reg.size()) == reg ) {

			m_Host.erase(0, reg.size());
		}
		else {
			if( m_Host.substr(0, sec.size()) == sec ) {

				port   = 443;

				m_fTls = true;

				m_Host.erase(0, sec.size());
			}
		}

		if( (pos = m_Host.find(':')) != string::npos ) {

			port = atoi(m_Host.substr(pos+1).c_str());

			m_Host.erase(pos);
		}

		HINTERNET hConn = InternetConnect(hNet,
						  m_Host.c_str(),
						  port,
						  m_User.c_str(),
						  m_Pass.c_str(),
						  INTERNET_SERVICE_HTTP,
						  0,
						  0
		);

		if( hConn ) {

			if( m_fForm ) {

				FormLogon(hConn);
			}

			SyncTop(hConn);

			InternetCloseHandle(hConn);
		}

		InternetCloseHandle(hNet);
	}

	return false;
}

static bool FormLogon(HINTERNET hConn)
{
	CPrintf url("logon.sub?user=%s&pass=%s", m_User.c_str(), m_Pass.c_str());

	string PageData;

	return FetchPage(hConn, PageData, url);
}

static bool SyncTop(HINTERNET hConn)
{
	AfxTrace("  Logs\n");

	SyncIndex(hConn, "");

	if( m_fBatch ) {

		SyncBatches(hConn);
	}

	if( m_fBanner ) {

		printf("Done.\n");
	}

	return true;
}

static bool SyncBatches(HINTERNET hConn)
{
	string BatchIndex;

	if( FetchPage(hConn, BatchIndex, "/logs.htm?m=1") ) {

		string hit = "<a href='/logs.htm?m=1&b=";

		size_t pos = BatchIndex.find("<div class=\"container\" role=\"main\">");

		if( pos != string::npos ) {

			for( ;;) {

				if( (pos = BatchIndex.find(hit, pos)) != string::npos ) {

					size_t end = BatchIndex.find('>', pos + hit.size());

					if( end != string::npos ) {

						string href = GetHref(BatchIndex.substr(pos, end - pos + 1));

						string name = GetUrlParam(href, "b");

						if( !name.empty() ) {

							AfxTrace("  %s\n", name.c_str());

							SyncIndex(hConn, name);
						}

						pos += hit.size();

						continue;
					}
				}

				break;
			}

			return true;
		}
	}

	return false;
}

static bool SyncIndex(HINTERNET hConn, string const &batch)
{
	string LogIndex;

	string url("/logs.htm");

	if( !batch.empty() ) {

		url += "?m=1&b=";

		url += batch;
	}

	if( FetchPage(hConn, LogIndex, url) ) {

		string hit = "<a href='/logs.htm?";

		size_t pos = LogIndex.find("<div class=\"container\" role=\"main\">");

		if( pos != string::npos ) {

			for( ;;) {

				if( (pos = LogIndex.find(hit, pos)) != string::npos ) {

					size_t end = LogIndex.find('>', pos + hit.size());

					if( end != string::npos ) {

						string href = GetHref(LogIndex.substr(pos, end - pos + 1));

						string log  = GetUrlParam(href, "l");

						string path = GetUrlParam(href, "p");

						if( !log.empty() && !path.empty() ) {

							SyncLog(hConn, batch, log, path);
						}

						pos += hit.size();

						continue;
					}
				}

				break;
			}

			return true;
		}
	}

	return false;
}

static bool SyncLog(HINTERNET hConn, string const &batch, string const &log, string const &path)
{
	AfxTrace("    %s\n", log.c_str());

	string  FileIndex;

	CPrintf url("/logs.htm?l=%s&p=%s", log.c_str(), path.c_str());

	if( !batch.empty() ) {

		url += "&m=1&b=";

		url += batch;
	}

	if( FetchPage(hConn, FileIndex, url) ) {

		string  top(batch.empty() ? "Logs" : "Batch\\" + batch);

		CPrintf pfx("\\files\\%s\\%s\\", top.c_str(), path.c_str());

		string  hit = "<a href='" + pfx;

		size_t  pos = 0;

		for( ;;) {

			if( (pos = FileIndex.find(hit, pos)) != string::npos ) {

				size_t end = FileIndex.find('>', pos + hit.size());

				if( end != string::npos ) {

					string href = GetHref(FileIndex.substr(pos, end - pos + 1));

					string file = href.substr(pfx.size());

					size_t spos = FileIndex.find("<td>", end);

					if( spos != string::npos ) {

						UINT size = atoi(FileIndex.c_str() + spos + 4);

						SyncFile(hConn, batch, log, path, file, size);

						pos += hit.size();

						continue;
					}
				}
			}

			break;
		}

		return true;
	}

	return false;
}

static bool SyncFile(HINTERNET hConn, string const &batch, string const &log, string const &path, string const &file, UINT size)
{
	AfxTrace("      %s : ", file.c_str());

	string  step(batch.empty() ? "Logs" : "Batch\\" + batch);

	CPrintf base("%s\\%s\\%s", m_Dest.c_str(), step.c_str(), path.c_str());

	string  name = base +"\\" + file;

	string  temp = name + ".tmp";

	MakeDirectory(base);

	HANDLE hRead = CreateFile(name.c_str(),
				  GENERIC_READ,
				  0,
				  NULL,
				  OPEN_EXISTING,
				  NULL,
				  NULL
	);

	if( hRead != INVALID_HANDLE_VALUE ) {

		UINT disk = GetFileSize(hRead, NULL);

		if( size <= disk ) {

			AfxTrace("No Change\n");

			CloseHandle(hRead);

			return true;
		}

		CloseHandle(hRead);
	}

	HANDLE hSave = CreateFile(temp.c_str(),
				  GENERIC_READ | GENERIC_WRITE,
				  0,
				  NULL,
				  CREATE_ALWAYS,
				  NULL,
				  NULL
	);

	if( hSave != INVALID_HANDLE_VALUE ) {

		string  top(batch.empty() ? "Logs" : "Batch/" + batch);

		CPrintf url("/files/%s/%s/%s", top.c_str(), path.c_str(), file.c_str());

		string FileData;

		if( FetchPage(hConn, FileData, url) ) {

			DWORD done = 0;

			WriteFile(hSave, FileData.data(), FileData.size(), &done, NULL);

			if( done == size ) {

				CloseHandle(hSave);

				DeleteFile(name.c_str());

				MoveFile(temp.c_str(), name.c_str());

				AfxTrace("Read %u bytes\n", done);

				return true;
			}

			CloseHandle(hSave);

			DeleteFile(temp.c_str());
		}
	}

	AfxTrace("FAILED\n");

	return false;
}

static bool FetchPage(HINTERNET hConn, string &Page, string const &Path)
{
	UINT uFlags = INTERNET_FLAG_KEEP_CONNECTION | INTERNET_FLAG_NO_CACHE_WRITE | INTERNET_FLAG_RELOAD;

	if( m_fTls ) {

		uFlags |= INTERNET_FLAG_SECURE;

		uFlags |= INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;

		uFlags |= INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

	HINTERNET hFile = HttpOpenRequest(hConn,
					  "GET",
					  Path.c_str(),
					  "HTTP/1.1",
					  "",
					  NULL,
					  uFlags,
					  0
	);

	if( hFile ) {

		BOOL fSend = HttpSendRequest(hFile,
					     NULL,
					     0,
					     NULL,
					     0
		);

		if( fSend ) {

			DWORD dwStatus = 0;

			DWORD dwSize   = sizeof(dwStatus);

			DWORD dwFlags  = HTTP_QUERY_FLAG_NUMBER | HTTP_QUERY_STATUS_CODE;

			HttpQueryInfo(hFile,
				      dwFlags,
				      &dwStatus,
				      &dwSize,
				      NULL
			);

			if( dwStatus == HTTP_STATUS_OK ) {

				Page.clear();

				for( ;;) {

					size_t from = Page.size();

					size_t more = 1024;

					DWORD  read = 0;

					Page.resize(from + more, ' ');

					if( InternetReadFile(hFile, PBYTE(Page.c_str() + from), more, PDWORD(&read)) ) {

						if( read < more ) {

							Page.resize(from + read);

							InternetCloseHandle(hFile);

							return true;
						}

						continue;
					}

					Error("failed to read %s (error 0x%8.8X)", Path.c_str(), GetLastError());
				}
			}

			Error("failed to fetch %s (status %u)", Path.c_str(), dwStatus);

			return false;
		}

		Error("failed to open %s (error 0x%8.8X)", Path.c_str(), GetLastError());
	}

	return false;
}

static string GetHref(string const &element)
{
	string hit = "href='";

	size_t pos = element.find(hit);

	if( pos != string::npos ) {

		size_t end = element.find("'", (pos += hit.size()));

		if( end != string::npos ) {

			return element.substr(pos, end - pos);
		}
	}

	return "";
}

static string GetUrlParam(string const &url, string name)
{
	size_t pos = url.find('?');

	while( pos != string::npos ) {

		size_t end  = url.find('&', (pos += 1));

		string pair = url.substr(pos, end == string::npos ? end : end - pos);

		size_t set  = pair.find('=');

		if( set == string::npos ) {

			if( pair == name ) {

				return "true";
			}
		}
		else {
			if( pair.substr(0, set) == name ) {

				return pair.substr(set+1);
			}
		}

		if( end != string::npos ) {

			pos = end;

			continue;
		}

		break;
	}

	return "";
}

static bool MakeDirectory(string const &path)
{
	string walk;

	size_t pos = 0;

	for( ;;) {

		size_t end  = path.find('\\', pos);

		string part = path.substr(pos, end == string::npos ? end : end - pos);

		walk += part;

		CreateDirectory(walk.c_str(), NULL);

		if( end != string::npos ) {

			walk += "\\";

			pos   = end + 1;

			continue;
		}

		break;
	}

	return false;
}

// End of File
