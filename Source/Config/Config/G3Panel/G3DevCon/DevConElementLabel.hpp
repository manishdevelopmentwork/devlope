

#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementLabel_HPP

#define INCLUDE_DevConElementLabel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Label UI Element
//

class CDevConElementLabel : public CDevConElement
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConElementLabel(CString const &Label, BOOL fIndent);

	// Destructor
	~CDevConElementLabel(void);

	// Operations
	void AddLayout(CLayFormation *pForm);
	void CreateControls(CWnd &Wnd, UINT &id);

protected:
	// Data Member
	CString		m_Label;
	BOOL		m_fIndent;
	CLayItem      * m_pEditLayout;
	CLayFormation * m_pMainLayout;
	CCtrlWnd      * m_pLabelCtrl;
};

// End of File

#endif
