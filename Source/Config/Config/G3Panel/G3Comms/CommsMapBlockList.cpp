
#include "Intern.hpp"

#include "CommsMapBlockList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsMapBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMapBlockList, CItemIndexList);

// Constructor

CCommsMapBlockList::CCommsMapBlockList(void)
{
	m_Class = AfxRuntimeClass(CCommsMapBlock);
	}

// Item Access

CCommsMapBlock * CCommsMapBlockList::GetItem(INDEX Index) const
{
	return (CCommsMapBlock *) CItemIndexList::GetItem(Index);
	}

CCommsMapBlock * CCommsMapBlockList::GetItem(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsMapBlock *pBlock = GetItem(n);

		if( pBlock->m_Name == Name ) {

			return pBlock;
			}

		GetNext(n);
		}

	return NULL;
	}

// Operations

BOOL CCommsMapBlockList::Validate(BOOL fExpand)
{
	BOOL fSave = FALSE;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsMapBlock *pBlock = GetItem(n);

		if( pBlock->Validate(fExpand) ) {

			fSave = TRUE;
			}
		}

	return fSave;
	}

// End of File
