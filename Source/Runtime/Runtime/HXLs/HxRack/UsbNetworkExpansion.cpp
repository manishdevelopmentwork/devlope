
#include "Intern.hpp"

#include "UsbNetworkExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbNetworkExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = New CUsbNicExpansion(pDriver);

	return p;
	}

// Constructor

CUsbNicExpansion::CUsbNicExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansion(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbNicExpansion::GetName(void)
{
	return "GMENET";
	}

UINT METHOD CUsbNicExpansion::GetClass(void)
{
	return rackClassNetwork;
	}

// End of File
