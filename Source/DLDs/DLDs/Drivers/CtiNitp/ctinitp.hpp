#include "ti500v2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CTI NITP Master Serial Driver
//

class CCtiNitpMaster : public CTi500v2Driver
{
	public:
		// Constructor
		CCtiNitpMaster(void);

		// Destructor
		~CCtiNitpMaster(void);
	};

// End of File
