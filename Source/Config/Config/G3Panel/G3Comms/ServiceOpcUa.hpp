
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceOpcUa_HPP

#define INCLUDE_ServiceOpcUa_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagSet;

//////////////////////////////////////////////////////////////////////////
//
// UPC UA Service Configuration
//

class CServiceOpcUa : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CServiceOpcUa(void);

		// Initial Values
		void SetInitValues(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Loading
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL IsEnabled(void) const;
		UINT GetTreeImage(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pEnable;
		CCodedItem * m_pServer;
		CCodedItem * m_pEndpoint;
		UINT         m_Port;
		UINT	     m_Layout;
		UINT         m_Tree;
		UINT	     m_Array;
		UINT         m_Props;
		UINT	     m_HistEnable;
		UINT	     m_HistSample;
		UINT	     m_HistQuota;
		UINT	     m_HistTime;
		UINT	     m_PubSub;
		UINT	     m_Anon;
		CCodedItem * m_pUser;
		CCodedItem * m_pPass;
		UINT         m_Write;
		UINT	     m_Debug;
		CTagSet	   * m_pSet;
		CTagSet	   * m_pSet2;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
