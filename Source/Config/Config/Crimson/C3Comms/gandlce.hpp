
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GANDLCE_HPP

#define	INCLUDE_GANDLCE_HPP

#define	AN	addrNamed
#define BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

//////////////////////////////////////////////////////////////////////////
//
// Giddings and Lewis C/E Master Driver
//

class CGandLCEDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CGandLCEDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration

		// Address Management

		// Address Helpers

		// Helpers

	protected:
		// Data

		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
