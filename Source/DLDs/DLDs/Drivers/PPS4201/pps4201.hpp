

// MACROS AND CONSTANTS

#define	FRAME_TIMEOUT	2500

#define	RETRIES		1

#define	RX_NOTHING	0
#define	RX_ERROR	1
#define	RX_FRAME	2
#define	RX_ACK		3
#define	RX_NAK		4

#define	PS_STX		0x0C
#define	PS_ACK		0x06
#define	PS_NAK		0x03

#define	PS_USER_NO	0x07

#define	PS_MARKER_RANGE_START	0x0040		// pointer to dynamic range
#define	PS_MARKER_RANGE_END	0x004F
#define	PS_MARKER_RANGE_COUNT	0x10

//////////////////////////////////////////////////////////////////////////
//
// Klockner Moeller PS4-201 Driver
//

class CPPS4201Driver : public CMasterDriver
{
	public:
		// Constructor
		CPPS4201Driver(void);

		// Destructor
		~CPPS4201Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		
		// Comms Data
		BYTE m_bTxBuff[256];
		BYTE m_bRxBuff[256];
		UINT m_uPtr;
		UINT m_uMarkerOffset;
		UINT m_uMarkerEndRange;
		BOOL m_fMarkerReadReqrd;
		
		// Implementation
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		void Clear(void);
		void AddByte(BYTE bByte);
		void AddWord(WORD wData);
		void Send(void);
		WORD Receive(void);
		BOOL MarkerOffsetRead();
	};

// End of File
