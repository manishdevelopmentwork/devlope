
#include "Intern.hpp"

#include <G3Web.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PNG Files
//

#include "c-1024-768-full-x1.png.dat"
#include "c-0320-240-full-x1.png.dat"
#include "c-0320-240-full-x2.png.dat"
#include "c-0480-272-full-x1.png.dat"
#include "c-0480-272-full-x2.png.dat"
#include "c-0640-480-full-x1.png.dat"
#include "c-0800-480-full-x1.png.dat"
#include "c-0800-600-full-x1.png.dat"
#include "c-1280-720-full-x1.png.dat"
#include "g-1024-768-full-x1.png.dat"
#include "g-1280-800-full-x1.png.dat"
#include "g-0640-480-full-x1.png.dat"
#include "g-0800-480-full-x1.png.dat"
#include "g-0800-600-full-x1.png.dat"

//////////////////////////////////////////////////////////////////////////
//
// JSON Files
//

#include "c-1024-768-full-x1.json.dat"
#include "c-0320-240-full-x1.json.dat"
#include "c-0320-240-full-x2.json.dat"
#include "c-0480-272-full-x1.json.dat"
#include "c-0480-272-full-x2.json.dat"
#include "c-0640-480-full-x1.json.dat"
#include "c-0800-480-full-x1.json.dat"
#include "c-0800-600-full-x1.json.dat"
#include "c-1280-720-full-x1.json.dat"
#include "g-1024-768-full-x1.json.dat"
#include "g-1280-800-full-x1.json.dat"
#include "g-0640-480-full-x1.json.dat"
#include "g-0800-480-full-x1.json.dat"
#include "g-0800-600-full-x1.json.dat"

//////////////////////////////////////////////////////////////////////////
//
// Index Macros
//

#define AddFile(t, m, d) { #t, "/" #t "/" #m "." #t, data_##d##_##t, size_##d##_##t }

//////////////////////////////////////////////////////////////////////////
//
// File Index
//

static CWebFileData m_Files[] = {

	AddFile(png,	c-1024-768-full-x1,	c_1024_768_full_x1),
	AddFile(png,	c-0320-240-full-x1,	c_0320_240_full_x1),
	AddFile(png,	c-0320-240-full-x2,	c_0320_240_full_x2),
	AddFile(png,	c-0480-272-full-x1,	c_0480_272_full_x1),
	AddFile(png,	c-0480-272-full-x2,	c_0480_272_full_x2),
	AddFile(png,	c-0640-480-full-x1,	c_0640_480_full_x1),
	AddFile(png,	c-0800-480-full-x1,	c_0800_480_full_x1),
	AddFile(png,	c-0800-600-full-x1,	c_0800_600_full_x1),
	AddFile(png,	c-1280-720-full-x1,	c_1280_720_full_x1),
	AddFile(png,	g-1024-768-full-x1,	g_1024_768_full_x1),
	AddFile(png,	g-1280-800-full-x1,	g_1280_800_full_x1),
	AddFile(png,	g-0640-480-full-x1,	g_0640_480_full_x1),
	AddFile(png,	g-0800-480-full-x1,	g_0800_480_full_x1),
	AddFile(png,	g-0800-600-full-x1,	g_0800_600_full_x1),

	AddFile(json,	c-1024-768-full-x1,	c_1024_768_full_x1),
	AddFile(json,	c-0320-240-full-x1,	c_0320_240_full_x1),
	AddFile(json,	c-0320-240-full-x2,	c_0320_240_full_x2),
	AddFile(json,	c-0480-272-full-x1,	c_0480_272_full_x1),
	AddFile(json,	c-0480-272-full-x2,	c_0480_272_full_x2),
	AddFile(json,	c-0640-480-full-x1,	c_0640_480_full_x1),
	AddFile(json,	c-0800-480-full-x1,	c_0800_480_full_x1),
	AddFile(json,	c-0800-600-full-x1,	c_0800_600_full_x1),
	AddFile(json,	c-1280-720-full-x1,	c_1280_720_full_x1),
	AddFile(json,	g-1024-768-full-x1,	g_1024_768_full_x1),
	AddFile(json,	g-1280-800-full-x1,	g_1280_800_full_x1),
	AddFile(json,	g-0640-480-full-x1,	g_0640_480_full_x1),
	AddFile(json,	g-0800-480-full-x1,	g_0800_480_full_x1),
	AddFile(json,	g-0800-600-full-x1,	g_0800_600_full_x1),

};

//////////////////////////////////////////////////////////////////////////
//
// JSON Copies
//

static char	    m_Name[3 * 14][32];

static char	    m_Work[3 * 14][600];

static CWebFileData m_Copy[3 * 14];

//////////////////////////////////////////////////////////////////////////
//
// File Addition
//

global void AddWebFiles(void)
{
	UINT w = 0;

	for( UINT n = 0; n < elements(m_Files); n++ ) {

		CWebFileData const &File = m_Files[n];

		if( !strcmp(File.m_pType, "json") ) {

			for( UINT c = 0; c < 3; c++ ) {

				char png[32];

				char dat[600];

				strcpy(png, strchr(File.m_pName + 1, '/') + 1);

				strchr(png, '.')[0] = 0;

				strcat(png, ".png");

				memcpy(dat, PCTXT(File.m_pData), File.m_uSize);

				dat[File.m_uSize] = 0;

				SPrintf(m_Work[w], dat, png, 8*(c+1));

				strcpy (m_Name[w], File.m_pName);

				SPrintf(strchr(m_Name[w], '.'), "-%2.2u.json", 8*(c+1));

				m_Copy[w].m_pType = File.m_pType;

				m_Copy[w].m_pName = m_Name[w];

				m_Copy[w].m_pData = PCBYTE(m_Work[w]);

				m_Copy[w].m_uSize = strlen(m_Work[w]);

				AddWebFile(m_Copy + w++);

			}
		}
		else {
			AddWebFile(&File);
		}
	}
}

global PCTXT GetWebRemoteInfo(int cx, int cy, int zoom, int bits)
{
	static char sName[64];

	char t = 'c';

	if( cy != 240 && cy != 272 ) {

		zoom = 1;
	}

	CString Name(WhoGetName(TRUE));

	Name.MakeLower();
	
	if( Name[0] == 'g' || Name.StartsWith("da5") || Name.StartsWith("da7") ) {

		switch( cx ) {

			case 1280:
			{
				if( cy != 720 ) {

					t = 'g';
				}
			}
			break;

			case 640:
			case 800:
			case 1024:
			{
				t = 'g';
			}
			break;
		}
	}

	SPrintf(sName, "/assets/json/%c-%4.4u-%3.3u-full-x%u-%2.2u.json", t, cx, cy, zoom, bits);

	return sName;
}

// End of File
