
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Header Control Item
//

// Constructors

CHeaderItem::CHeaderItem(void)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));
	}

CHeaderItem::CHeaderItem(CHeaderItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((HDITEM *) this, &That, sizeof(HDITEM));

	m_Text = That.m_Text;
	}

CHeaderItem::CHeaderItem(HDITEM const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((HDITEM *) this, &Item, sizeof(HDITEM));
	}

CHeaderItem::CHeaderItem(int cx, CString const &Text, HBITMAP hBitmap)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetWidth(cx);

	SetText(Text);

	SetBitmap(hBitmap);
	}

CHeaderItem::CHeaderItem(int cx, PCTXT pText, HBITMAP hBitmap)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetWidth(cx);

	SetText(pText);

	SetBitmap(hBitmap);
	}

CHeaderItem::CHeaderItem(int cx, CString const &Text, UINT uImage)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetWidth(cx);

	SetText(Text);

	SetImage(uImage);
	}

CHeaderItem::CHeaderItem(int cx, PCTXT pText, UINT uImage)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetWidth(cx);

	SetText(pText);

	SetImage(uImage);
	}

CHeaderItem::CHeaderItem(int cx, CString const &Text)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetWidth(cx);

	SetText(Text);
	}

CHeaderItem::CHeaderItem(int cx, PCTXT pText)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetWidth(cx);

	SetText(pText);
	}

CHeaderItem::CHeaderItem(UINT uMask)
{
	memset((HDITEM *) this, 0, sizeof(HDITEM));

	SetMask(uMask);
	}

// Assignment Operators

CHeaderItem const & CHeaderItem::operator = (CHeaderItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((HDITEM *) this, &That, sizeof(HDITEM));

	m_Text = That.m_Text;

	return ThisObject;
	}

CHeaderItem const & CHeaderItem::operator = (HDITEM const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((HDITEM *) this, &Item, sizeof(HDITEM));

	return ThisObject;
	}

// Attributes

UINT CHeaderItem::GetMask(void) const
{
	return mask;
	}

BOOL CHeaderItem::TestMask(UINT uMask) const
{
	return (mask & uMask) ? TRUE : FALSE;
	}

int CHeaderItem::GetWidth(void) const
{
	AfxAssert(mask & HDI_WIDTH);

	return cxy;
	}

int CHeaderItem::GetHeight(void) const
{
	AfxAssert(mask & HDI_HEIGHT);

	return cxy;
	}

PCTXT CHeaderItem::GetText(void) const
{
	AfxAssert(mask & HDI_TEXT);

	return pszText;
	}

HBITMAP CHeaderItem::GetBitmap(void) const
{
	AfxAssert(mask & HDI_BITMAP);

	return hbm;
	}

UINT CHeaderItem::GetFormat(void) const
{
	AfxAssert(mask & HDI_FORMAT);

	return fmt;
	}

LPARAM CHeaderItem::GetParam(void) const
{
	AfxAssert(mask & HDI_LPARAM);

	return lParam;
	}

UINT CHeaderItem::GetImage(void) const
{
	AfxAssert(mask & HDI_IMAGE);

	return iImage;
	}

UINT CHeaderItem::GetOrder(void) const
{
	AfxAssert(mask & HDI_ORDER);

	return iOrder;
	}

UINT CHeaderItem::GetFilterType(void) const
{
	AfxAssert(mask & HDI_FILTER);

	return type;
	}

LPVOID CHeaderItem::GetFilter(void) const
{
	AfxAssert(mask & HDI_FILTER);

	return pvFilter;
	}

// Operations

void CHeaderItem::SetMask(UINT uMask)
{
	mask = uMask;
	}

void CHeaderItem::SetWidth(int cx)
{
	mask |= HDI_WIDTH;

	cxy = cx;
	}

void CHeaderItem::SetHeight(int cy)
{
	mask |= HDI_HEIGHT;

	cxy = cy;
	}

void CHeaderItem::SetText(PCTXT pText)
{
	mask |= HDI_TEXT;

	m_Text     = pText;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CHeaderItem::SetText(CString const &Text)
{
	mask |= HDI_TEXT;

	m_Text     = Text;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CHeaderItem::SetTextBuffer(UINT uCount)
{
	mask |= HDI_TEXT;

	m_Text     = CString(' ', uCount);

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CHeaderItem::SetBitmap(HBITMAP hBitmap)
{
	mask |= HDI_BITMAP;

	hbm = hBitmap;
	}

void CHeaderItem::SetFormat(UINT uFormat)
{
	mask |= HDI_FORMAT;

	fmt = uFormat;
	}

void CHeaderItem::SetParam(LPARAM uParam)
{
	mask |= HDI_LPARAM;

	lParam = uParam;
	}

void CHeaderItem::SetImage(UINT uImage)
{
	mask |= HDI_IMAGE;

	iImage = uImage;
	}

void CHeaderItem::SetOrder(UINT uOrder)
{
	mask |= HDI_ORDER;

	iOrder = uOrder;
	}

void CHeaderItem::SetFilter(UINT uType, LPVOID pFilter)
{
	mask |= HDI_FILTER;

	type     = uType;

	pvFilter = pFilter;
	}

// End of File
