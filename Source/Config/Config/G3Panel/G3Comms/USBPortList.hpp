
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_USBPortList_HPP

#define INCLUDE_USBPortList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUSBPortItem;

//////////////////////////////////////////////////////////////////////////
//
// USB Port List
//

class DLLNOT CUSBPortList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUSBPortList(void);

		// Destructor
		~CUSBPortList(void);

		// Item Location
		CUSBPortItem * GetItem(INDEX Index) const;
		CUSBPortItem * GetItem(UINT  uPos ) const;

		// Item Search
		CUSBPortItem * FindItem(UINT uType);
	};

// End of File

#endif
