
#include "Intern.hpp"

#include "UsbStringDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb String Descriptor
//

// Constructor

CUsbStringDesc::CUsbStringDesc(void)
{
	Init();
	}

// Endianess

void CUsbStringDesc::HostToUsb(void)
{
	for( UINT i = 0; i < GetCount(); i ++ ) {

		m_wString[i] = HostToIntel(m_wString[i]);
		}
	}

void CUsbStringDesc::UsbToHost(void)
{
	for( UINT i = 0; i < GetCount(); i ++ ) {

		m_wString[i] = IntelToHost(m_wString[i]);
		}
	}

// Attributes

BOOL CUsbStringDesc::IsValid(void) const
{
	return 	m_bType == descString;
	}

BOOL CUsbStringDesc::IsNull(void) const
{
	return m_bLength == sizeof(UsbDesc);
	}

UINT CUsbStringDesc::GetCount(void) const
{
	return (m_bLength - sizeof(UsbDesc)) / sizeof(WORD);
	}

// Operations

void CUsbStringDesc::Init(void)
{
	memset(this, 0, sizeof(UsbStringDesc));

	m_bType   = descString;

	m_bLength = sizeof(UsbDesc);
	}

void CUsbStringDesc::Set(PCTXT pText)
{
	UINT uCount = strlen(pText);

	MakeMin(uCount, elements(m_wString));

	m_bLength = BYTE(sizeof(UsbDesc) + uCount * sizeof(WORD));
	
	for( UINT i = 0; i < uCount; i ++ ) {

		m_wString[i] = pText[i];
		}
	}

void CUsbStringDesc::Set(WORD wData)
{
	m_bLength    = sizeof(UsbDesc) + sizeof(wData);	

	m_wString[0] = wData;
	}

void CUsbStringDesc::Set(PWORD pwData, UINT uCount)
{
	MakeMin(uCount, elements(m_wString));

	UINT uSize = uCount * sizeof(WORD);

	m_bLength  = BYTE(sizeof(UsbDesc) + uSize);
	
	memcpy(m_wString, pwData, uSize);
	}

void CUsbStringDesc::Get(PTXT pText, UINT n) const
{
	MakeMin(n, GetCount());

	for( UINT i = 0; i < n; i ++ ) {

		pText[i] = (char) m_wString[i];
		}
	
	pText[n] = NULL;
	}

// Debug

void CUsbStringDesc::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nUsb String Descriptor\n");

	AfxTrace("Type         = %d\n",      m_bType);
	AfxTrace("Len          = %d\n",      m_bLength);
	AfxTrace("Text         = ");

	for( UINT i = 0; i < GetCount(); i ++ ) {

		AfxTrace("%c", (char) m_wString[i]);
		}
	
	AfxTrace("\n");

	#endif
	}

// End of File
