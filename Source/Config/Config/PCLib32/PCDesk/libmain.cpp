
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		afxModule = New CModule(modCoreLibrary);

		if( !afxModule->InitLib(hThis) ) {

			AfxTrace(L"ERROR: Failed to load PCDesk\n");

			delete afxModule;

			afxModule = NULL;

			return FALSE;
			}

		CButtonBitmap *pCore16 = New CButtonBitmap( CBitmap(L"ToolCore16"),
							    CSize(16, 16),
							    67
							    );

		CButtonBitmap *pCore24 = New CButtonBitmap( CBitmap(L"ToolCore24"),
							    CSize(24, 24),
							    67
							    );

		afxButton->AppendBitmap(0x1000, pCore16);

		afxButton->AppendBitmap(0x1100, pCore24);

		return TRUE;
		}

	if( uReason == DLL_PROCESS_DETACH ) {
	
		afxButton->RemoveBitmap(0x1000);

		afxButton->RemoveBitmap(0x1100);

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
