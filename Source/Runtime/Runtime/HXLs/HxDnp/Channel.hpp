
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Channel_HPP

#define	INCLUDE_Channel_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "ChannelConfig.hpp"

#include "MasterSession.hpp"

#include "SlaveSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Channel Structure
//

struct Channel {

	dnpChan 	m_Chan;
	cfgChan 	m_Cfg;
	cfgPort 	m_Port;
	cfgLink 	m_Link;
	cfgPhys 	m_Phys;
	void	*	m_pIO;
	cfgStrt 	m_Strt;
};

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern ISocket * Create_DnpUdpSocket(IDnpChannelConfig * pConfig);

//////////////////////////////////////////////////////////////////////////
//
// Channel Object - Port
//

class CChannel : public IDnpChannel
{
	public:
		// Constructor
		CChannel(IDataHandler * pHandler, WORD wSrc);
		CChannel(IDnpChannelConfig * pConfig, WORD wSrc);

		// Destructor
		~CChannel(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnpChannel
		BOOL			METHOD Open(void);
		BOOL			METHOD Close(void);
		IDnpSession *		METHOD OpenSession(WORD wDest, WORD wTO, DWORD dwLink, void * pCfg);
		BOOL			METHOD CloseSession(WORD wDest);
		BOOL			METHOD CloseSessions(void);
		BOOL			METHOD EnableSessions(BOOL fEnable);
		WORD			METHOD GetSource(void);
		IDnpChannelConfig *	METHOD GetConfig(void);
		void *			METHOD GetChannel(void);
		void			METHOD Service(void);
		void			METHOD Share(BOOL fOpen, UINT uBackOff = 0);

		// Operations
		BOOL MatchChannel(void * pIO, WORD wSrc, BOOL fMaster);
		void EnableChannel(BOOL fEnable);
		WORD RxData(dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst);
		BOOL TxData(dnpUCHAR *pBuff, dnpWORD Bytes);
		BOOL TxUDP(dnpUCHAR Port, dnpUCHAR *pBuff, dnpWORD Bytes);
		WORD GetTxReady(void);
		BOOL IsMaster(void);
		void SetFail(void);

	protected:
		// Data
		ULONG	        m_uRefs;
		
		// Data Members
		WORD		m_wSrc;
		Channel  *	m_pConfig;
		dnpChan	 *	m_pChannel;
		dnpAPP *	m_pApp;
		cfgTarg 	m_Target;
		BYTE		m_bSess;
		CSession **	m_pSessions;
		BOOL		m_fMaster;
		BOOL		m_fEnable;
		ISocket *	m_pSock[portIP];
		UINT		m_uLast[portIP];
		IMutex *	m_pEthernet;
		IMutex *	m_pSerial;

		// Implementation
		void InitChannel(WORD wSrc);
		BOOL InitConfig(void);
		BOOL RecordSession(CSession * pSession);
		WORD RxSerial(dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst);
		BOOL TxSerial(dnpUCHAR *pBuff, dnpWORD Bytes);
		WORD RxEthernet(dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst);
		BOOL TxEthernet(dnpUCHAR *pBuff, dnpWORD Bytes, BYTE bEnum = portTCP);

		// Socket Management
		ISocket * CreateSocket(BYTE bEnum);
		BOOL	  CheckSocket(BYTE bEnum);
		BOOL	  OpenSocket(BYTE bEnum, BOOL fRx = FALSE);
		BOOL	  OpenTCP();
		BOOL	  OpenUDP();
		void	  CloseSocket(BOOL fAbort, BYTE bEnum);
		void	  SetNetwork(void);
		BOOL	  IsActive(BYTE bEnum);
		BOOL	  IsNetwork(void);
		BOOL	  IsTCP(BYTE bEnum);
		BOOL	  IsUDP(BYTE bEnum);
		BOOL	  IsOnlyTCP(void);
		BOOL      IsOnlyUDP(void);
		BOOL	  CheckRemote(BYTE bEnum);
		void	  StripHeader(BYTE bEnum, dnpUCHAR *pBuff, UINT& Bytes);
};

// End of File

#endif
