
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Hdlc_HPP

#define	INCLUDE_Hdlc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppLayer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

class CModem;

//////////////////////////////////////////////////////////////////////////
//
// HDLC ASCII Codes
//

#define	CR		0x0D
#define	XON		0x11
#define	XOFF		0x13
#define	HDLC_ESC	0x7D
#define	HDLC_SYN	0x7E

//////////////////////////////////////////////////////////////////////////
//
// PPP HDLC Encapsulator
//

class CHdlc : public CPppLayer
{
	public:
		// Constructor
		CHdlc(CPpp *pPpp, CModem *pModem);

		// Destructor
		~CHdlc(void);

		// Management
		void Open(void);
		void Close(void);
		void LowerLayerUp(void);
		void LowerLayerDown(void);
		void OnTime(void);

		// Operations
		void SetOptions(UINT uCharMap, BOOL fProtComp, BOOL fAddrComp);
		void PutFrame(WORD wProtocol, CBuffer *pBuff);
		void OnRecvByte(BYTE bData);

	protected:
		// Static Data
		static WORD const m_wTable[];

		// Data Members
		CModem * m_pModem;
		IMutex * m_pMutex;
		UINT     m_uCharMap;
		BOOL     m_fProtComp;
		BOOL     m_fAddrComp;
		BOOL	 m_fActive;
		BOOL     m_fSync;
		BOOL     m_fEscape;
		BYTE     m_bTxBuff[3500];
		UINT	 m_uTxPtr;
		BYTE     m_bRxBuff[2000];
		UINT     m_uRxPtr;

		// Frame Buidling
		void InitFrame(void);
		void SendFrame(void);
		void AddByte(BYTE bData);
		void AddByte(WORD wProtocol, BYTE bData);

		// Implementation
		void PutFrame(WORD wProtocol, PCBYTE pData, UINT uSize);
		void RecvFrame(void);
		void ResetRecv(void);
		WORD CalcFCS(PCBYTE pData, UINT uSize);
		BOOL CheckNoCarrier(void);
	};

// End of File

#endif
