
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Warning Control
//

#pragma warning(disable: 4671)

#pragma warning(disable: 4673)

//////////////////////////////////////////////////////////////////////////
//
// Exception Base Class
//

// Constructors

CException::CException(void)
{
	m_pFile = "";

	m_uLine = 0;
	}

CException::CException(CException const &That)
{
	m_pFile = That.m_pFile;

	m_uLine = That.m_uLine;
	}

// Attributes

PCSTR CException::GetFile(void) const
{
	return m_pFile;
	}

UINT CException::GetLine(void) const
{
	return m_uLine;
	}

// Operations

void CException::SetContext(PCSTR pFile, UINT uLine)
{
	m_pFile = pFile;

	m_uLine = uLine;
	}

//////////////////////////////////////////////////////////////////////////
//
// Exception Throwing Functions
//

void ThrowException(PCSTR pFile, UINT uLine)
{
	AfxTrace(L"THROW: CException at line %u of %hs\n", uLine, pFile);

	CException Ex;

	Ex.SetContext(pFile, uLine);

	throw Ex;
	}

void ThrowMemoryException(PCSTR pFile, UINT uLine)
{
	AfxTrace(L"THROW: CMemoryException at line %u of %hs\n", uLine, pFile);

	CMemoryException Ex;

	Ex.SetContext(pFile, uLine);

	throw Ex;
	}

void ThrowResourceException(PCSTR pFile, UINT uLine)
{
	AfxTrace(L"THROW: CResourceException at line %u of %hs\n", uLine, pFile);

	CResourceException Ex;

	Ex.SetContext(pFile, uLine);

	throw Ex;
	}

void ThrowSupportException(PCSTR pFile, UINT uLine)
{
	AfxTrace(L"THROW: CSupportException at line %u of %hs\n", uLine, pFile);

	CSupportException Ex;

	Ex.SetContext(pFile, uLine);

	throw Ex;
	}

void ThrowSystemException(PCSTR pFile, UINT uLine, UINT uCode)
{
	AfxTrace(L"THROW: CSystemException at line %u of %hs\n", uLine, pFile);

	CSystemException Ex;

	Ex.SetContext(pFile, uLine);

	Ex.m_uCode = uCode;

	throw Ex;
	}

void ThrowUserException(PCSTR pFile, UINT uLine)
{
	AfxTrace(L"THROW: CUserException at line %u of %hs\n", uLine, pFile);

	CUserException Ex;

	Ex.SetContext(pFile, uLine);

	throw Ex;
	}

//////////////////////////////////////////////////////////////////////////
//
// Exception Type Records
//

AfxImplementRuntimeClass(CException, CObject);

AfxImplementRuntimeClass(CMemoryException, CException);

AfxImplementRuntimeClass(CResourceException, CException);

AfxImplementRuntimeClass(CSupportException, CException);

AfxImplementRuntimeClass(CSystemException, CException);

AfxImplementRuntimeClass(CUserException, CException);

// End of File
