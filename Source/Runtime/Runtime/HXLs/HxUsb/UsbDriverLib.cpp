
#include "Intern.hpp"  

#include "UsbDriverLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Frameworks
//

#include "Hid.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IUsbHostFuncDriver * Create_HubDriver(void);
extern IUsbHostFuncDriver * Create_StorageDriver(void);
extern IUsbHostFuncDriver * Create_FtdiDriver(void);
extern IUsbHostFuncDriver * Create_AcmDriver(void);
extern IUsbHostFuncDriver * Create_EcmDriver(void);
extern IUsbHostFuncDriver * Create_NcmDriver(void);
extern IUsbHostFuncDriver * Create_SmcNicDriver(void);
extern IUsbHostFuncDriver * Create_LoopbackDriver(void);
extern IUsbHostFuncDriver * Create_RackDriver(void);
extern IUsbHostFuncDriver * Create_HidDriver(void);
extern IUsbHostFuncDriver * Create_CanDriver(void);
extern IUsbHostFuncDriver * Create_ProfibusDriver(void);
extern IUsbHostFuncDriver * Create_SerialDriver(void);
extern IUsbHostFuncDriver * Create_ModuleBootDriver(void);
extern IUsbHostFuncDriver * Create_DevNetDriver(void);
extern IUsbHostFuncDriver * Create_DongleDriver(void);
extern IUsbHostFuncDriver * Create_ModuleDriver(void);

//////////////////////////////////////////////////////////////////////////
//
// Usb Driver Library
//

// Classification

bool CUsbDriverLib::GetClassOverride(UINT uVendor, UINT uProduct) const
{
	return false;
	}

bool CUsbDriverLib::GetClassFilter(UINT uVendor, UINT uProduct) const
{
	return false;
	}
// Creation

IUsbHostFuncDriver * CUsbDriverLib::CreateDriver(UINT uClass, UINT uSubClass, UINT uProtocol) const
{
	switch( uClass ) {

		case devHub:
			
			return Create_HubDriver();

		case devComms:

			if( uSubClass == 0x00 && uProtocol == 0x00 ) {

				return Create_AcmDriver();
				}

			if( uSubClass == 0x02 && uProtocol == 0x01 ) {

				return Create_AcmDriver();
				}

			if( uSubClass == 0x06 && uProtocol == 0x00 ) {

				return Create_EcmDriver();
				}

			if( uSubClass == 0x0D && uProtocol == 0x00 ) {

				return Create_NcmDriver();
				}

			break;

		case devStorage:

			if( uSubClass == 0x06 && uProtocol == 0x50 ) {

				return Create_StorageDriver();
				}
			
			break;

		case devHid:

			if( uSubClass == classBoot || uSubClass == classNone ) {

				return Create_HidDriver();
				}

			break;
		}

	return NULL;
	}

IUsbHostFuncDriver * CUsbDriverLib::CreateDriver(UINT uVendor, UINT uProduct, UINT uClass, UINT uSubClass, UINT uProtocol) const
{
	#if defined(_XDEBUG)	

	AfxTrace("DriverLib::CreateDriver("
		 "Vendor=0x%4.4X, Product=0x%4.4X, "
		 "Class=0x%2.2X, Sub=0x%2.2X, Proto=0x%2.2X)\n", 
		 uVendor, uProduct, 
		 uClass, uSubClass, uProtocol
		 );

	#endif

	switch( uVendor ) {

		case vidRedLion:

			if( uClass == devVendor ) {

				switch( uSubClass ) {

					case subclassBoot:

						return Create_ModuleBootDriver();

					case subclassDongle:

						return Create_DongleDriver();

					case subclassDriver:

						return Create_ModuleDriver();

					case subclassRack:

						return Create_RackDriver();

					case subclassComms:

						switch( uProtocol ) {

							case protoSerial:

								return Create_SerialDriver();

							case protoCan:

								return Create_CanDriver();

							case protoDevNet:

								return Create_DevNetDriver();

							case protoProfibus:

								return Create_ProfibusDriver();

							case protoJ1939:

								return Create_CanDriver();

							case protoCdl:

								return Create_SerialDriver();
									
							case protoDnp3:

								break;
							}

						break;
					}
				}

			switch( uProduct ) {

				case pidTest:

					return Create_LoopbackDriver();

				case pidSerial232485:
				case pidSerial232:
				case pidSerial485:
									
					return Create_FtdiDriver();

				case pidPid1:
				case pidPid2:
				case pidUin4:
				case pidOut4:
				case pidDio14:
				case pidTc8:
				case pidIni8:
				case pidInv8:
				case pidRtd6:
				case pidSg1:

					return Create_RackDriver();

				case pidCan:
				case pidJ1939:

					return Create_CanDriver();

				case pidDevNet:

					return Create_DevNetDriver();

				case pidProfibus:

					return Create_ProfibusDriver();

				case pidSerial:

					return Create_SerialDriver();

				case pidDevNetBoot:
				case pidCanBoot:
				case pidProfibusBoot:
				case pidJ1939Boot:
				case pidSerialBoot:
				case pidCanCdlBoot:

					return Create_ModuleBootDriver();

				case pidDnp3Dongle:

					return Create_DongleDriver();
				}
			
			break;

		case vidFtdi:

			switch( uProduct ) {
				
				case pidFtdi2232:
				case pidFtdi4232:

					return Create_FtdiDriver();
				}
			
			break;

		case vidSmc:

			switch( uProduct ) {

				case pidSmcLan9512:
				
					return Create_SmcNicDriver();
				}
			
			break;
		}

	return NULL;
	}

// End of File
