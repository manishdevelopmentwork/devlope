
#include "intern.hpp"

#include "legacy.h"

#include "dio01mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8in 6out Simple Digital Module
//

// Dynamic Class

AfxImplementDynamicClass(CDIO14Module, CGenericModule);

// Constructor

CDIO14Module::CDIO14Module(void)
{
	m_pInput = New CDIO14ModCfg;

	m_Ident  = ID_CSDIO14;

	m_FirmID = FIRM_DIO14;

	m_Model  = "CSDIO14";

	m_Conv.Insert(L"Graphite", L"CGMDIO14Module");

	m_Conv.Insert(L"Manticore", L"CDADIDOModule");
	}

// UI Management

CViewWnd * CDIO14Module::CreateMainView(void)
{
	return New CDIO14MainWnd;
	}

// Comms Object Access

UINT CDIO14Module::GetObjectCount(void)
{
	return 1;
	}

BOOL CDIO14Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = "Variables";
			
			Data.pItem = m_pInput;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CDIO14Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pInput->Convert(pValue->GetChild(L"Config"));

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CDIO14Module::AddMetaData(void)
{
	CGenericModule::AddMetaData();

	Meta_AddObject(Input);
	}

// Download Support

void CDIO14Module::MakeObjectData(CInitData &Init)
{
	WORD ID = 1;
	
	WORD PropID;
	
	m_pInput->MakeASCIIEquations();
	
	m_pInput->MakeBinaryEquations();
		
	//--------------------------------------------------------
		
	PropID = PROPID_EQN_COUNT;
		
	PropID |= (ID << 11);

	PropID |= (CMD_WRITE << 8);

	Init.AddWord(PropID);

	Init.AddWord(WORD(m_pInput->m_NumEquations));

	//--------------------------------------------------------

	PropID = PROPID_EQN_ELEMENT;
		
	PropID |= (ID << 11);

	PropID |= (CMD_WRITE << 8);
	
	for( UINT i = 0; i < m_pInput->m_NumBinElements; i++ ) {

		Init.AddWord(PropID);

		Init.AddWord(m_pInput->m_BinEq[i]);
		}

	//--------------------------------------------------------

	PropID = PROPID_EQN_DONE;
		
	PropID |= (ID << 11);

	PropID |= (CMD_WRITE << 8);

	Init.AddWord(PropID);

	Init.AddWord(0);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8in 6out Simple Digital Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDIO14MainWnd, CProxyViewWnd);

// Constructor

CDIO14MainWnd::CDIO14MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CDIO14MainWnd::OnAttach(void)
{
	m_pItem = (CDIO14Module *) CProxyViewWnd::m_pItem;

	AddDIO14Pages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CDIO14MainWnd::AddDIO14Pages(void)
{
	CDIO14ModCfg *pInput = m_pItem->m_pInput;

	for( UINT n = 0; n < pInput->GetPageCount(); n++ ) {

		CViewWnd *pPage = pInput->CreatePage(n);

		m_pMult->AddView(pInput->GetPageName(n), pPage);

		pPage->Attach(pInput);
		}
	}

// End of File
