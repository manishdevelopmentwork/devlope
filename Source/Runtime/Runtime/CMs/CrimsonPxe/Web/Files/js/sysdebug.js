
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Scrolling Debug Console
//

// Data

var xhr;

var input = false;

var init = 1;

var cmd = "";

var last = "";

// Code

function pageMain() {

	xhr = new XMLHttpRequest();

	// TODO -- Implement all the richness of the serial console, including
	// the history buffer and an AJAX call for command and file completion.

	// TODO -- Consider a mode where we don't trace debug trace output and
	// can therefore avoid constant polling. This might be useful for low
	// bandwidth or metered cellular connections.

	if (getParam("m") == "2") {

		$("#command").keyup(

			function (e) {

				if (e.keyCode == 13) {

					var c = $(this).val();

					if (c == ".") {

						c = last;
					}

					if (c == "clear") {

						$("#debug-text").html("");
					}
					else {
						last = c;
						cmd = c;
					}

					$(this).val('');
				}
			}
		);

		$("#command").css("display", "").val("help").focus();

		input = true;
	}
	else {

		$(".debug").css("height", "calc(100vh - 250px)");

		input = false;
	}

	$("#debug-text").css("display", "");

	$("#lock-box").change(function () { onClick($(this).prop("checked")); });

	sendRequest();
}

function sendRequest() {

	var url = "/ajax/sysdebug-update.ajax?cmd=" + encodeURIComponent(cmd) + "&init=" + init.toString();

	cmd = "";

	xhr.onload = dataLoaded;

	xhr.onerror = dataFailed;

	xhr.open("GET", url, true);

	xhr.send(null);
}

function dataLoaded() {

	if (xhr.status == 200) {

		var jdiv = $("#debug-text");

		var ddiv = jdiv[0];

		if (init) {

			jdiv.html("");
		}

		if (xhr.responseText != "") {

			var list = xhr.responseText.split('\n');

			for (var n = 0; n < list.length - 1; n++) {

				while (ddiv.childElementCount >= 1000) {

					ddiv.removeChild(ddiv.firstChild);
				}

				if (list[n] == "") {

					jdiv.append("<p>&nbsp;</p>");
				}
				else {

					jdiv.append("<p>" + list[n] + "</p>");
				}
			}

			ddiv.scrollTop = ddiv.scrollHeight;
		}

		init = 0;

		setTimeout(sendRequest, 100);

		return;
	}

	if (check401(xhr.status)) {

		return;
	}

	dataFailed();
}

function dataFailed() {

	$("#debug-text").html("<p style='color: #F00'>NO CONNECTION TO DEVICE</p>");

	init = 1;

	setTimeout(sendRequest, 1000);
}

function onClick(check) {

	var snap = $("#debug-snap");

	var text = $("#debug-text");

	var dcmd = $("#command");

	if (check) {

		if (input) {

			dcmd.val("");

			dcmd.prop("disabled", true);
		}

		snap.html(text.html());

		snap.css("display", "");

		snap[0].scrollTop = text[0].scrollHeight;

		text.css("display", "none");
	}
	else {

		if (input) {

			dcmd.prop("disabled", false);

			dcmd.focus();
		}

		text.css("display", "");

		snap.css("display", "none");

		snap.html("");
	}
}

// End of File
