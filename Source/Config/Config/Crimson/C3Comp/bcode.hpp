
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BCODE_HPP

#define	INCLUDE_BCODE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Byte Code Values
//

enum ByteCode
{
	bcNull			= 0x80,

	// Conversions

	bcToInteger		= 0x81,
	bcToReal		= 0x82,
	bcTestInteger		= 0x83,
	bcTestReal		= 0x84,

	// Stack Helpers

	bcPop			= 0x85,
	bcPopString		= 0x86,
	bcDupTop		= 0x87,
	bcDupTopString		= 0x88,
	bcDup3rd		= 0x89,
	bcDup3rdString		= 0x8A,
	bcSwap			= 0x8B,

	// Functions
	
	bcFunction		= 0x8C,
	bcReturn		= 0x8D,

	// Branches

	bcBranchZero		= 0x8E,
	bcBranchNonZero		= 0x8F,
	bcBranch		= 0x90,

	// Referencing

	bcLoadAddr		= 0x91,
	bcIndexString		= 0x92,
	bcIndexArray		= 0x93,
	bcIndexExtend		= 0x94,
	bcIndexBit		= 0x95,

	// Save Local
	
	bcPutLocal		= 0x96,
	bcPutLocalString	= 0x97,
	bcPutLocal0		= 0x98,
	bcPutLocal1		= 0x99,
	bcPutLocal2		= 0x9A,
	bcPutLocal3		= 0x9B,
	bcPutLocal4		= 0x9C,
	bcPutLocal5		= 0x9D,

	// Load Local
	
	bcGetLocal		= 0x9E,
	bcGetLocalString	= 0x9F,
	bcGetLocal0		= 0xA0,
	bcGetLocal1		= 0xA1,
	bcGetLocal2		= 0xA2,
	bcGetLocal3		= 0xA3,
	bcGetLocal4		= 0xA4,
	bcGetLocal5		= 0xA5,

	// Save Direct

	bcPutInteger		= 0xA6,
	bcPutReal		= 0xA7,
	bcPutString		= 0xA8,

	// Load Direct

	bcGetInteger		= 0xA9,
	bcGetReal		= 0xAA,
	bcGetString		= 0xAB,

	// Save Indirect

	bcSaveInteger		= 0xAC,
	bcSaveReal		= 0xAD,
	bcSaveString		= 0xAE,

	// Load Indirect

	bcLoadInteger		= 0xAF,
	bcLoadReal		= 0xB0,
	bcLoadString		= 0xB1,

	// Load Property
	
	bcLoadPropInteger	= 0xE6,
	bcLoadPropReal		= 0xE7,
	bcLoadPropString	= 0xE8,

	// Constants

	bcPushInteger1		= 0xB2,
	bcPushInteger2		= 0xB3,
	bcPushInteger4		= 0xB4,
	bcPushReal		= 0xB5,
	bcPushString		= 0xB6,
	bcPushWideString	= 0xB7,

	// Integer Operators

	bcLogicalNot		= 0xB8,
	bcBitwiseNot		= 0xB9,
	bcUnaryMinus		= 0xBA,
	bcIncrement		= 0xBB,
	bcDecrement		= 0xBC,
	bcBitSelect		= 0xBD,
	bcMultiply		= 0xBE,
	bcDivide		= 0xBF,
	bcRemainder		= 0xC0,
	bcAdd			= 0xC1,
	bcSubtract		= 0xC2,
	bcLeftShift		= 0xC3,
	bcRightShift		= 0xC4,
	bcLessThan		= 0xC5,
	bcLessOr		= 0xC6,
	bcGreaterThan		= 0xC7,
	bcGreaterOr		= 0xC8,
	bcEqual			= 0xC9,
	bcNotEqual		= 0xCA,
	bcBitwiseAnd		= 0xCB,
	bcBitwiseXor		= 0xCC,
	bcBitwiseOr		= 0xCD,
	bcBitChange		= 0xCE,

	// Real Operators

	bcRealLogicalNot	= 0xCF,
	bcRealUnaryMinus	= 0xD0,
	bcRealIncrement		= 0xD1,
	bcRealDecrement		= 0xD2,
	bcRealMultiply		= 0xD3,
	bcRealDivide		= 0xD4,
	bcRealAdd		= 0xD5,
	bcRealSubtract		= 0xD6,
	bcRealLessThan		= 0xD7,
	bcRealLessOr		= 0xD8,
	bcRealGreaterThan	= 0xD9,
	bcRealGreaterOr		= 0xDA,
	bcRealEqual		= 0xDB,
	bcRealNotEqual		= 0xDC,

	// String Operators

	bcStringAdd		= 0xDD,
	bcStringAddChar		= 0xDE,
	bcStringLessThan	= 0xDF,
	bcStringLessOr		= 0xE0,
	bcStringGreaterThan	= 0xE1,
	bcStringGreaterOr	= 0xE2,
	bcStringEqual		= 0xE3,
	bcStringNotEqual	= 0xE4,

	// Label Marker

	bcLabel			= 0xE5,

	// Debug Codes

	bcDebugInfo		= 0xE9,
	bcDebugFunc		= 0xEA,

	// End Marker

	bcLastCode		= 0xEB,

	};

// End of File

#endif
