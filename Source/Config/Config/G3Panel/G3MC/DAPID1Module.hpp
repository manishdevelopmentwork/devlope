
#include "Intern.hpp"

#include "slcmod.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DAPID1Module_HPP

#define INCLUDE_DAPID1Module_HPP

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

class CDAPID1Module : public CSLCModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDAPID1Module(void);

		// UI Management
		CViewWnd * CreateMainView(void);

	protected:
		// Download Support
		void MakeConfigData(CInitData &Init);

		// Implementation
		void AddMetaData(void);
	};

// End of File

#endif

