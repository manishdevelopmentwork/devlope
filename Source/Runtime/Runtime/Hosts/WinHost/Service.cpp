
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Console Hooks
//

extern void TermConsole(void);

//////////////////////////////////////////////////////////////////////////
//
// Service Support
//

// Static Data

static win32::HANDLE m_hStop;

static win32::HANDLE m_hDone;

// Prototypes

global	BOOL		ServiceHook(int &nCode);
static	void	WINAPI	ServiceProc(DWORD nArg, PTXT *pArg);
static	DWORD	WINAPI	HandlerProc(DWORD dwControl, DWORD dwEvent, LPVOID pEvent, PVOID pContext);

// Code

global BOOL ServiceHook(int &nCode)
{
	if( g_Config.m_fService ) {

		win32::SERVICE_TABLE_ENTRYA Table[] = { 

			{ PTXT(PCTXT(g_Config.m_Service)), ServiceProc },
			{ NULL,			           NULL        },

			};

		m_hStop = win32::CreateEvent(NULL, FALSE, FALSE, NULL);

		m_hDone = win32::CreateEvent(NULL, FALSE, FALSE, NULL);

		AfxTrace("serv: starting dispatcher\n");

		if( !win32::StartServiceCtrlDispatcherA(Table) ) {

			AfxTrace("serv: failed to start dispatcher\n");
			}
		else {
			AfxTrace("serv: returned from dispatcher\n");

			nCode = 0;
			}

		win32::DeleteObject(m_hStop);

		win32::DeleteObject(m_hDone);
		
		TermConsole();

		return TRUE;
		}

	return FALSE;
	}

static void WINAPI ServiceProc(DWORD nArg, PTXT *pArg)
{
	// TODO -- This is running on a thread created
	// by the service dispatcher, and not on a real
	// Aeon thread. So whose context are we in??? I
	// wonder if we should have a hidden API to bind
	// a new Aeon thread for situations like this?

	win32::SERVICE_STATUS_HANDLE hStatus = win32::RegisterServiceCtrlHandlerExA( g_Config.m_Service,
										     HandlerProc,
										     NULL
										     );

	win32::SERVICE_STATUS Status;

	Status.dwServiceType             = SERVICE_WIN32_OWN_PROCESS;
	Status.dwCurrentState            = SERVICE_START_PENDING;
	Status.dwControlsAccepted        = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP;
	Status.dwWin32ExitCode           = 0;
	Status.dwServiceSpecificExitCode = 0;
	Status.dwCheckPoint              = 0;
	Status.dwWaitHint                = 0;

	win32::SetServiceStatus(hStatus, &Status);

	AfxTrace("serv: starting app\n");

	IClientProcess *pClient = NULL;

	// TODO -- Should we use a richer object for services? We
	// could provide a callback interface to log events etc.

	if( AfxNewObject("app", IClientProcess, pClient) == S_OK ) {

		HTHREAD hThread = CreateClientThread(pClient, 0, 200);

		Status.dwCurrentState = SERVICE_RUNNING;

		win32::SetServiceStatus(hStatus, &Status);

		AfxTrace("serv: app started\n");

		win32::WaitForSingleObject(m_hStop, INFINITE);

		AfxTrace("serv: stopping app\n");

		Status.dwCurrentState = SERVICE_STOP_PENDING;

		win32::SetServiceStatus(hStatus, &Status);

		DestroyThread(hThread);

		pClient->Release();

		AfxTrace("serv: app stopped\n");
		}
	else {
		Status.dwWin32ExitCode           = 1;

		Status.dwServiceSpecificExitCode = 1;

		AfxTrace("serv: failed to start\n");
		}

	Status.dwCurrentState = SERVICE_STOPPED;

	win32::SetServiceStatus(hStatus, &Status);

	win32::SetEvent(m_hDone);
	}

static DWORD WINAPI HandlerProc(DWORD dwControl, DWORD dwEvent, LPVOID pEvent, PVOID pContext)
{
	if( dwControl == SERVICE_CONTROL_STOP || dwControl == SERVICE_CONTROL_SHUTDOWN ) {

		AfxTrace("serv: stop request recieved\n");

		win32::SetEvent(m_hStop);

		win32::WaitForSingleObject(m_hDone, INFINITE);

		AfxTrace("serv: stop request completed\n");

		return NO_ERROR;
		}

	if( dwControl == SERVICE_CONTROL_INTERROGATE ) {

		return NO_ERROR;
		}

	return ERROR_CALL_NOT_IMPLEMENTED;
	}

// End of File
