
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FatDirName_HPP

#define	INCLUDE_FatDirName_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Reference Objects
//

class CFatDirEntry;

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Name
//

class CFatDirName
{
	public:
		// Constructors
		CFatDirName(void);
		CFatDirName(PCTXT pText);
		CFatDirName(CFatDirName const &That);

		// Attributes
		UINT GetLength(void) const;
		BOOL IsShort(void) const;
		BOOL IsLong(void) const;

		// Operations
		void  Clear(void);
		PCTXT MakeBasis(UINT uTail);
		UINT  GetEntryCount(void) const;
		BOOL  Export(UINT iEntry, CFatDirEntry &Entry);
		BOOL  Import(CFatDirEntry const &Entry);

		// Comparison Operators
		BOOL operator == (CFatDirName const &That) const;
		BOOL operator == (PCTXT pText) const;

		// Conversion Operators
		operator PTXT (void) const;

	protected:
		// Constants
		enum
		{	
			constSizeLong	= 13,
			constSizeShort	= 11,
			constMaxSize	= 255
			};

		// Data
		CHAR m_sText[constMaxSize];
		CHAR m_sBasis[13];
		UINT m_uSize;
		BYTE m_bCheck;
		BOOL m_fShort;

		// Implementation
		UINT Count(CHAR c) const;
		UINT FindExtension(void) const;
		UINT GetStripped(PTXT pText, UINT uCount) const;
		UINT GetExtension(PTXT pText) const;
		UINT GetTailLength(UINT uTail) const;
	};

// End of File

#endif

