
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Blocker Window
//

// Runtime Class

AfxImplementRuntimeClass(CBlockerWnd, CWnd);

// Constructor

CBlockerWnd::CBlockerWnd(CWnd *pParent, CDialog *pDlg, CRect Rect)
{
	m_pParent = pParent;

	m_pDlg    = pDlg;

	m_Rect    = Rect;

	CClientDC RefDC(pParent->GetHandle());

	CMemoryDC MemDC(RefDC);

	m_Bitmap.Create(RefDC, m_Rect.GetSize());

	MemDC.Select(m_Bitmap);

	UINT uFlags = PRF_CHILDREN | PRF_CLIENT | PRF_CHECKVISIBLE | PRF_ERASEBKGND;

	m_pParent->SendMessage( WM_PRINT,
				WPARAM(MemDC.GetHandle()),
				LPARAM(uFlags)
				);

	MemDC.Deselect();
	}

// Operations

void CBlockerWnd::Create(void)
{
	CWnd::Create( WS_CHILD | WS_CLIPSIBLINGS | WS_VISIBLE,
		      m_Rect,
		      m_pParent->GetHandle(),
		      0,
		      NULL
		      );
	}

void CBlockerWnd::SetDialog(CDialog *pDlg)
{
	m_pDlg = pDlg;
	}

// Message Map

AfxMessageMap(CBlockerWnd, CWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_MOUSEACTIVATE)

	AfxMessageEnd(CBlockerWnd)
	};

// Message Handlers

BOOL CBlockerWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}
	
void CBlockerWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.BitBlt(Rect, m_Bitmap, CPoint(0, 0), SRCCOPY);
	}

UINT CBlockerWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	m_pDlg->ForceActive();

	return UINT(MA_NOACTIVATEANDEAT);
	}

// End of File
