
#include "../../HSL/AeonHsl.hpp"

#include "../../RLOS/RedMX51/Models.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Model Detection
//

// Externals

clink BYTE _base[];

// Top of Memory

global unsigned long _top = 0x40000000;

// Code

UINT AeonFindModel(void)
{
	WORD id = *PWORD(_base + 0x12);

	switch( id ) {

		case 1: return MODEL_GRAPHITE_07;
		case 2: return MODEL_GRAPHITE_09;
		case 3: return MODEL_GRAPHITE_10V;
		case 4: return MODEL_GRAPHITE_10S;
		case 5: return MODEL_GRAPHITE_12;
		case 6: return MODEL_GRAPHITE_15;
		}

	return MODEL_GRAPHITE_09;
	}

// End of File
