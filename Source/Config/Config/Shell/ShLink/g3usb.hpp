
//////////////////////////////////////////////////////////////////////////
//
// WDM USB Driver
//
// Copyright (c) 1993-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3USB_HPP

#define	INCLUDE_G3USB_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include <initguid.h>

//////////////////////////////////////////////////////////////////////////
//
// Interface GUIDs
//

// {A028DE24-1A67-4AD5-B8CF-19544A08215E}

DEFINE_GUID( GUID_CLASS_IG3USB, 
	     0xA028DE24, 0x1A67, 0x4AD5, 0xB8, 0xCF, 0x19, 0x54, 0x4A, 0x08, 0x21, 0x5E
	     );

// End of File

#endif
