
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "StdEnv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Filing System Access
//

#include "../../../StdEnv/IFileSystem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IFileManager	* Create_FileManager(void);
extern IDiskManager	* Create_DiskManager(UINT uPriority);
extern IVolumeManager	* Create_VolumeManager(void);
extern IBlockCache	* Create_BlockCache(IBlockDevice *pDev, UINT uPolicy);
extern IFileCopier	* Create_FileCopier(void);
extern IFile		* Create_File(IFilingSystem *pSystem);

//////////////////////////////////////////////////////////////////////////
//
// Task Local Storage 
//

#define TLS_FILE_DIR	100
#define TLS_FILE_VOL	101
#define TLS_FILE_PATH	102
#define TLS_FILE_DRIVE	103
#define TLS_FILE_REF	104

// End of File

#endif
