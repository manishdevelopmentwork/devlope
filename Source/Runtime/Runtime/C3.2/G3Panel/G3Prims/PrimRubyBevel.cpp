
#include "intern.hpp"

#include "PrimRubyBevel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitives
//

// Constructor

CPrimRubyBevel::CPrimRubyBevel(void)
{
	}

// Initialization

void CPrimRubyBevel::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyBevel", pData);

	CPrimRubyBevelBase::Load(pData);
	}

// End of File
