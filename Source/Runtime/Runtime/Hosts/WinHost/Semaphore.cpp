
#include "Intern.hpp"

#include "Semaphore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Object
//

// Instantiator

static IUnknown * Create_Semaphore(PCTXT pName)
{
	return (ISemaphore *) New CSemaphore;
	}

// Registration

global void Register_Semaphore(void)
{
	piob->RegisterInstantiator("exec.semaphore", Create_Semaphore);
	}

// Constructor

CSemaphore::CSemaphore(void)
{
	m_hSync = win32::CreateSemaphore(NULL, 0, INT_MAX, NULL);
	}

// IUnknown

HRESULT CSemaphore::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ISemaphore);

	return CWaitable::QueryInterface(riid, ppObject);
	}

ULONG CSemaphore::AddRef(void)
{
	return CWaitable::AddRef();
	}

ULONG CSemaphore::Release(void)
{
	return CWaitable::Release();
	}

// IWaitable

PVOID CSemaphore::GetWaitable(void)
{
	return CWaitable::GetWaitable();
	}

BOOL CSemaphore::Wait(UINT uWait)
{
	return CWaitable::Wait(uWait, TRUE);
	}

BOOL CSemaphore::HasRequest(void)
{
	return CWaitable::HasRequest();
	}

// ISemaphore

void CSemaphore::Signal(UINT uCount)
{
	win32::ReleaseSemaphore(m_hSync, uCount, NULL);
	}

// End of File
