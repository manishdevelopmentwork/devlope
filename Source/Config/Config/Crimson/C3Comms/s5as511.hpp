
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_S5AS511_HPP
	
#define	INCLUDE_S5AS511_HPP

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 AS511 Base Driver
//

#include "s5base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Driver Options
//

class CS5AS511DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CS5AS511DriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Cache;
		UINT m_Mode;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Master Driver
//

class CS5AS511MasterDriver : public CS5AS511BaseDriver
{
	public:
		// Constructor
		CS5AS511MasterDriver(BOOL fV2);

		// Configuration
		CLASS GetDriverConfig(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
