//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DO_MORE_HPP

#define	INCLUDE_DO_MORE_HPP

// Data Table Definitions

#define	TBL_ST		1
#define	TBL_DST		2
#define	TBL_SDT		4
#define	TBL_X		5
#define	TBL_Y		6
#define	TBL_WX		7
#define	TBL_WY		8
#define	TBL_C		9
#define	TBL_V		10
#define	TBL_N		11
#define	TBL_D		12
#define	TBL_R		13
#define	TBL_T		14
#define	TBL_CT		15
#define	TBL_SS		16
#define	TBL_SL		17
#define	TBL_UDT		18
#define	TBL_PL		19
#define	TBL_DLX		20
#define	TBL_DLY		21
#define	TBL_DLC		22
#define	TBL_DLV		23
#define	TBL_MI		24
#define	TBL_MC		25
#define	TBL_MIR		26
#define	TBL_MHR		27
#define	TBL_LASTMSG	28
#define	TBL_LASTERR	29
#define	TBL_HEAP	240

#define	END_MARKER	L"<END>"

#define MAX_STRING	256

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Serial Device Options
//

class CDoMoreSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDoMoreSerialDeviceOptions(void);

		// Destructor
		~CDoMoreSerialDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Properties
		UINT	m_Drop;
		UINT	m_Ping;		
		UINT	m_PingEnable;
		UINT	m_Timeout;
		CString	m_Password;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Structured Type Field Entry
//

struct CDoMoreField
{
	PCUTF	m_pName;
	UINT	m_uType;
	UINT	m_uMajOffset;
	UINT	m_uMinOffset;
	};

//////////////////////////////////////////////////////////////////////////
//
// Structured Type Descriptions
//
struct CDoMoreStruct
{
	PCUTF		m_pName;
	UINT		m_uCount;
	CDoMoreField	m_Fields[32];
	};

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Serial Driver
//

class CDoMoreDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CDoMoreDriver(void);

		// Destructor
		~CDoMoreDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Type Helpers
		BOOL IsStructuredType(UINT uTable);
		BOOL IsString(UINT uTable);
		BOOL FindField(CString const &TypeName, CString const &FieldName, CDoMoreField &Field);
		CString GetTypeName(UINT uTable);
		CString GetFieldName(CString const &TypeName, UINT uBitOffset, UINT uDWORD, UINT uType);

		static CDoMoreStruct m_PredefinedStructs[];

	protected:

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Device Options
//

class CDoMoreUDPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDoMoreUDPDeviceOptions(void);

		// Destructor
		~CDoMoreUDPDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Properties
		UINT	m_Port;
		UINT	m_IP;
		UINT	m_Keep;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;
		UINT	m_Ping;
		UINT	m_PingEnable;
		UINT	m_Timeout;
		CString	m_Password;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Driver
//

class CDoMoreUDPDriver : public CDoMoreDriver
{
	public:
		// Constructor
		CDoMoreUDPDriver(void);

		// Destructor
		~CDoMoreUDPDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Driver
//

class CDoMoreAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDoMoreAddrDialog(CDoMoreDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Data members
		CDoMoreDriver * m_pDoMoreDrv;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnSelChange(UINT uID, CWnd &Wnd);
		BOOL OnOkay(UINT uID);

		// Implementation
		void DoEnables(void);
		void LoadCombo(void);
		void LoadType(CComboBox &Combo, CString TypeName);
		void ShowAddress(CAddress Addr);
		void ShowDetails(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Pre-defined Structured Types
//

CDoMoreStruct CDoMoreDriver::m_PredefinedStructs[] =
{ 
	{ L"TIMER", 5, {
		{ L"Acc",	addrLongAsLong,	1, 0 },
		{ L"Done",	addrBitAsBit,	0, 0 },
		{ L"Zero",	addrBitAsBit,	0, 1 },
		{ L"Timing",	addrBitAsBit,	0, 2 },
		{ L"Reset",	addrBitAsBit,	0, 3 },
		{ END_MARKER,	0,		0, 0 }
	}},

	{ L"COUNTER", 5, {
		{ L"Acc",	addrLongAsLong,	1, 0 },
		{ L"Done",	addrBitAsBit,	0, 0 },
		{ L"Zero",	addrBitAsBit,	0, 1 },
		{ L"Reset",	addrBitAsBit,	0, 2 },
		{ L"DnDone",	addrBitAsBit,	0, 3 },
		{ END_MARKER,	0,		0, 0 }
	}},

	{ L"DATETIME", 9, {
		{ L"Date",	addrLongAsLong,	0,  0 },
		{ L"Year",	addrWordAsWord,	0, 16 },
		{ L"Month",	addrByteAsByte,	0,  8 },
		{ L"Day",	addrByteAsByte,	0,  0 },
		{ L"Time",	addrLongAsLong,	1,  0 },
		{ L"DayOfWeek",	addrByteAsByte,	1, 24 },
		{ L"Hour",	addrByteAsByte,	1, 16 },
		{ L"Minute",	addrByteAsByte,	1,  8 },
		{ L"Second",	addrByteAsByte,	1,  0 },
		{ END_MARKER,	0,		0,  0 }
	}}
};

// End of File

#endif
