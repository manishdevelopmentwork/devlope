#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "actssudp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AC Tech Simple Servo UDP Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CACTechSSUDPDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CACTechSSUDPDriver::CACTechSSUDPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
}

// Destructor

CACTechSSUDPDriver::~CACTechSSUDPDriver(void)
{
}

// Configuration

void MCALL CACTechSSUDPDriver::Load(LPCBYTE pData)
{
}

// Management

void MCALL CACTechSSUDPDriver::Attach(IPortObject *pPort)
{
}

void MCALL CACTechSSUDPDriver::Open(void)
{
}

// Device

CCODE MCALL CACTechSSUDPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CACTechSSUDPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
		}
	}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CACTechSSUDPDriver::Ping(void)
{
	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
		}
	}

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = CMMS;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	if( Read(Addr, Data, 1) == 1 ) {

		return 1;
	}

	CloseSocket(TRUE);

	return CCODE_ERROR;
}

CCODE MCALL CACTechSSUDPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( UINT r = ReadNoTransmit(Addr, pData) ) {

		return r == 1 ? CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY : 1;
	}

	if( OpenSocket() ) {

		if( AddRead(Addr) && Transact() && !SetErrorWord() ) {

			GetResponse(Addr, pData);

			return 1;
		}
	}

	return CCODE_ERROR;
}

CCODE MCALL CACTechSSUDPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( WriteNoTransmit(Addr, pData) ) {

		return 1;
	}

//**/	AfxTrace0("\r\n** WRITE **");

	if( OpenSocket() ) {

		AddWrite(Addr, *pData);

		if( !Transact() ) {

			return CCODE_ERROR;
		}

		SetErrorWord();

		return 1;
	}

	return CCODE_ERROR;
}

// PRIVATE METHODS

// Socket Management

BOOL CACTechSSUDPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
		}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CACTechSSUDPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
	}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
		}
	}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
				}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
				}

				if( Phase == PHASE_ERROR ) {

					break;
				}

				Sleep(10);
			}

			CloseSocket(TRUE);

			return FALSE;
		}

	}

	return FALSE;
}

void CACTechSSUDPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort ) {

			m_pCtx->m_pSock->Abort();
		}

		else {
			m_pCtx->m_pSock->Close();
		}

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
	}
}

// Frame Building

void CACTechSSUDPDriver::StartFrame(void)
{
	m_uPtr   = 0;

	// Data Header
	AddByte(0x0A);
	AddByte(0x0E);
	AddByte(0x20);
	AddByte(0x00);
	AddByte(0x11);
	AddByte(0x00);
	AddByte(HIBYTE(HIWORD(m_pCtx->m_IP)));
	AddByte(LOBYTE(HIWORD(m_pCtx->m_IP)));
	AddByte(HIBYTE(LOWORD(m_pCtx->m_IP)));
	AddByte(LOBYTE(LOWORD(m_pCtx->m_IP)));
	AddByte(HIBYTE(m_pCtx->m_uPort));
	AddByte(LOBYTE(m_pCtx->m_uPort));
	AddByte(0x27);
	AddByte(0x01);

	m_uSequence++;

	AddByte(LOBYTE(m_uSequence));
	AddByte(HIBYTE(m_uSequence));
}

void CACTechSSUDPDriver::EndFrame(void)
{
	AddByte(0);
}

void CACTechSSUDPDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
}

void CACTechSSUDPDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));
	AddByte(HIBYTE(wData));
}

void CACTechSSUDPDriver::AddLong(DWORD dData)
{
	AddWord(LOWORD(dData));
	AddWord(HIWORD(dData));
}

BOOL CACTechSSUDPDriver::AddRead(AREF Addr)
{
	StartFrame();

	switch( Addr.a.m_Table ) {

		case TSID: // Drive Identification (not available via ethernet)
			AddWord(CMDIAX);
			break;

		case TSOC: // Output Configuration
			AddWord(CMOCR | CMD_OFFSET);
			AddWord(1);
			AddByte(LOBYTE(Addr.a.m_Offset));
			return TRUE;

		case TSVAR: // Indexer variable
			AddWord(CMVARR | CMD_OFFSET);
			AddWord(1);
			AddByte(LOBYTE(Addr.a.m_Offset));
			break;

		case TSUNOP:
			AddWord(LOWORD(m_dCMUNOP) & 0x00FF);
			UINT uC;
			UINT i;
			uC = LOBYTE(HIWORD(m_dCMUNOP));
			if( uC ) {
				AddWord(uC);
				AddByte(HIBYTE(LOWORD(m_dCMUNOP)) & 0xF);
				UINT i;
				for( i = 1; i < uC; i++ ) {
					AddByte(0);
				}
			}
			return TRUE;

		case addrNamed:

			AddReadOpcode(Addr.a.m_Offset);

			break;

		default:
			return FALSE;
	}

	AddByte(0); // read has 1 byte of 0 added to data

	return TRUE;
}

void  CACTechSSUDPDriver::AddReadOpcode(UINT uOffset)
{
// Offsets for some items are programmed as Write function codes, need to be adjusted for reads
	UINT u = 0;

	switch( uOffset ) {

		case	CMCL:	u = CMCLR;	break;
		case	CMPC:	u = CMPCR;	break;
		case	CMPFPG:	u = CMPFPGR;	break;
		case	CMPFIG:	u = CMPFIGR;	break;
		case	CMPFDG:	u = CMPFDGR;	break;
		case	CMVFFG:	u = CMVFFGR;	break;
		case	CMPFIL:	u = CMPFILR;	break;
		case	CMVFPG:	u = CMVFPGR;	break;
		case	CMVFIG:	u = CMVFIGR;	break;
		case	CMMPE:	u = CMMPER;	break;
		case	CMMPET:	u = CMMPETR;	break;

		case	CMEIC:	u = CMEICR;	break;
		case	CMO:	u = CMOR;	break;
		case	CMAOV:	u = CMAOVR;	break;

		case	CMSL:	u = CMSLR;	break;
		case	CMHL:	u = CMHLR;	break;
		case	CMNSL:	u = CMNSLR;	break;
		case	CMPSL:	u = CMPSLR;	break;

		case	CMU:	u = CMUR;	break;
		case	CMA:	u = CMAR;	break;
		case	CMD:	u = CMDR;	break;
		case	CMQD:	u = CMQDR;	break;
		case	CMMV:	u = CMMVR;	break;
		case	CMIPL:	u = CMIPLR;	break;
		case	CMV:	u = CMVR;	break;
		case	CMMER:	u = CMMERR;	break;
		case	CMGC:	u = CMGCR;	break;

		default:	u = uOffset;	break;
	}

	AddWord(u | CMD_OFFSET);
}

BOOL CACTechSSUDPDriver::AddWrite(AREF Addr, DWORD dData)
{
	if( Addr.a.m_Table == TSEEPR ) {

		if( !CanWriteToEEPROM(Addr.a.m_Table, Addr.a.m_Offset) ) {

			return FALSE;
		}

		AddEEPROMWrite(Addr, dData);
	}

	else {
		StartFrame();

		AddWriteData(Addr, dData);
	}

	return TRUE;
}

void CACTechSSUDPDriver::AddWriteData(AREF Addr, DWORD dData)
{
	UINT uCommand = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case addrNamed:

			AddWord(uCommand | CMD_OFFSET);

			switch( uCommand ) {

				case CMCL:
				case CMPC:
				case CMPFIG:
				case CMVFFG:
				case CMAOV:
				case CMNSL:
				case CMPSL:
				case CMU:
				case CMA:
				case CMD:
				case CMQD:
				case CMMV:
				case CMSP:
				case CMIPL:
				case CMV:
				case CMGC:
				case CMMP:
				case CMMD:
					AddLength(8);
					PutRealData(&dData);
					break;

				case CMPFPG:
				case CMPFDG:
				case CMVFPG:
				case CMPFIL:
				case CMVFIG:
				case CMMER:
					AddLength(4);
					AddLong(dData);
					break;

				case CMMPET:
				case CMMPE:
					AddLength(2);
					AddWord(LOWORD(dData));
					break;

				case CMSL:
				case CMHL:
				case CMEIC:
				case CMO:
				case CMMPI1:
				case CMMPI0:
				case CMMNI1:
				case CMMNI0:
					AddLength(1);
					AddByte(LOBYTE(dData));
					break;

				case CMRI:
				case CMMCE:
				case CMMCD:
				case CMSUSP:
				case CMRM:
				case CMVME:
				case CMVMD:
				case CMGME:
				case CMGMD:
				case CMS:
				case CMSQ:
					AddLength(0);
					break;

				case CMMDV:
					AddLength(16);
					PutRealData(&m_dCMMDVD);
					PutRealData(&m_dCMMDVV);
					break;

				case CMMDUR:
					AddLength(16);
					PutRealData(&m_dCMMDURD);
					PutRealData(&m_dCMMDURS);
					break;

				case CMMPUR:
					AddLength(16);
					PutRealData(&m_dCMMPURD);
					PutRealData(&m_dCMMPURS);
					break;

				case CMSIP:
					AddLength(16);
					PutRealData(&m_dCMSIPP);
					PutRealData(&m_dCMSIPD);
					break;
			}

			break;

		case TSOC:
			AddWord(CMOC | CMD_OFFSET);
			AddLength(2);
			AddByte(LOBYTE(Addr.a.m_Offset));
			AddByte(LOBYTE(dData));
			break;

		case TSVAR:
			AddWord(CMVAR | CMD_OFFSET);
			AddLength(9);
			AddByte(LOBYTE(Addr.a.m_Offset));
			PutRealData(&dData);
			break;
	}
}

void CACTechSSUDPDriver::AddEEPROMWrite(AREF Addr, DWORD dData)
{
	StartFrame();

	if( Addr.a.m_Table == TSOC ) {

		AddWord(CMOCS);
		AddLength(2);
		AddByte(LOBYTE(Addr.a.m_Offset));
		AddByte(LOBYTE(dData));

		return;
	}

	UINT uCmd = 0;
	UINT uLen = 0;

	switch( Addr.a.m_Offset ) {

		case CMPFPG:
			uCmd = CMPFPGS;
			uLen = 4;
			break;

		case CMPFDG:
			uCmd = CMPFDGS;
			uLen = 4;
			break;

		case CMVFPG:
			uCmd = CMVFPGS;
			uLen = 4;
			break;

		case CMPFIL:
			uCmd = CMPFILS;
			uLen = 4;
			break;

		case CMVFIG:
			uCmd = CMVFIGS;
			uLen = 4;
			break;

		case CMCL:
			uCmd = CMCLS;
			uLen = 8;
			break;

		case CMPC:
			uCmd = CMPCS;
			uLen = 8;
			break;

		case CMPFIG:
			uCmd = CMPFIGS;
			uLen = 8;
			break;

		case CMVFFG:
			uCmd = CMVFFGS;
			uLen = 8;
			break;

		case CMMPET:
			uCmd = CMMPETS;
			uLen = 2;
			break;

		case CMMPE:
			uCmd = CMMPES;
			uLen = 2;
			break;

		case CMSL:
			uCmd = CMSLS;
			uLen = 1;
			break;

		case CMHL:
			uCmd = CMHLS;
			uLen = 1;
			break;
	}

	AddWord(uCmd);

	AddLength(uLen);

	switch( uLen ) {

		case 1:
			AddByte(LOBYTE(dData));
			break;

		case 2:
			AddWord(LOWORD(dData));
			break;

		case 4:
			AddLong(dData);
			break;

		case 8:
			PutRealData(&dData);
			break;
	}
}

void CACTechSSUDPDriver::AddLength(UINT uLength)
{
	AddWord(uLength);
}

void CACTechSSUDPDriver::AddZero(void)
{
	AddLong(0);
	AddLong(0);
}

// Transport Layer

BOOL CACTechSSUDPDriver::Transact(void)
{
	if( Send() ) {

		return GetReply();
	}

	return FALSE;
}

BOOL CACTechSSUDPDriver::Send(void)
{
	EndFrame();

	UINT uSize = m_uPtr;

//**/	AfxTrace0("\r\n");
//**/	for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );
//**/	AfxTrace0("\r\n");

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CACTechSSUDPDriver::GetReply(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

//**/			AfxTrace0("\r\n");
//**/			for( UINT k = 0; k < uPtr; k++ ) AfxTrace1("<%2.2x>", m_bRx[k] );

			if( uPtr >= DATA0 + 1 + UINT(m_bRx[RCNTL]) ) {

//**/				AfxTrace0("\r\n");				

				return CheckFrame();
			}

			continue;
		}

		if( !CheckSocket() ) {

			return FALSE;
		}

		Sleep(10);
	}

	return FALSE;
}

void CACTechSSUDPDriver::GetResponse(AREF Addr, PDWORD pData)
{
	switch( Addr.a.m_Type ) {

		case addrByteAsByte:

			*pData = GetValue(DATA0, 1);
			break;

		case addrWordAsWord:

			*pData = GetValue(DATA0, 2);
			break;

		case addrLongAsLong:

			UINT uPos;

			uPos = Addr.a.m_Table != TSID ? DATA0 : DATA0 + (4*Addr.a.m_Offset);

			*pData = GetValue(uPos, 4);

			if( Addr.a.m_Table == TSID ) {

				UINT i;

				DWORD d;

				d  = (*pData & 0xFF000000) >> 24;
				d += (*pData & 0x00FF0000) >> 8;
				d += (*pData & 0x0000FF00) << 8;
				d += (*pData & 0x000000FF) << 24;

				*pData = d;
			}

			break;

		case addrRealAsReal:

			*pData = GetRealData();
			break;
	}
}

// Helpers

UINT CACTechSSUDPDriver::ReadNoTransmit(AREF Addr, PDWORD pData)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case CMSP:
			case CMRI:
			case CMMCE:
			case CMMCD:
			case CMSUSP:
			case CMRM:
			case CMMP:
			case CMMD:
			case CMMPI1:
			case CMMPI0:
			case CMMNI1:
			case CMMNI0:
			case CMVME:
			case CMVMD:
			case CMGME:
			case CMGMD:
			case CMS:
			case CMSQ:
			case CMSIP:
				return RCNODATA;

			case CMMDV:
			case CMMDUR:
			case CMMPUR:
				*pData = 0;
				return RCISDATA;

			case CMMDVD:
				*pData = m_dCMMDVD;
				return RCISDATA;

			case CMMDVV:
				*pData = m_dCMMDVV;
				return RCISDATA;

			case CMMDURD:
				*pData = m_dCMMDURD;
				return RCISDATA;

			case CMMDURS:
				*pData = m_dCMMDURS;
				return RCISDATA;

			case CMMPURD:
				*pData = m_dCMMPURD;
				return RCISDATA;

			case CMMPURS:
				*pData = m_dCMMPURS;
				return RCISDATA;

			case CMSIPP:
				*pData = m_dCMSIPP;
				return RCISDATA;

			case CMSIPD:
				*pData = m_dCMSIPD;
				return RCISDATA;

			case CMERR:
				*pData = m_dError;
				return RCISDATA;

			case CMUNOP:
				*pData = m_dCMUNOP;
				return RCISDATA;

			case CMDIAX:
				*pData = 0x204E2F41; // " N/A"
				return RCISDATA;
		}
	}

	if( Addr.a.m_Table == TSEEPR ) {

		*pData = 0;
		return RCISDATA;
	}

	if( Addr.a.m_Table == TSID ) {

		*pData = 0x204E2F41; // " N/A"
		return RCISDATA;
	}

	return 0;
}

BOOL CACTechSSUDPDriver::WriteNoTransmit(AREF Addr, PDWORD pData)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case CMDIS:
			case CMAIV:
			case CMGAP:
			case CMGTP:
			case CMGPE:
			case CMGLR:
			case CMMPF:
			case CMMS:
			case CMMRMS:
			case CMMAV:
			case CMMTV:
			case CMMMER:
			case CMMMPC:
				return TRUE;

			case CMMDVD:
				m_dCMMDVD = *pData;
				return TRUE;

			case CMMDVV:
				m_dCMMDVV = *pData;
				return TRUE;

			case CMMDURD:
				m_dCMMDURD = *pData;
				return TRUE;

			case CMMDURS:
				m_dCMMDURS = *pData;
				return TRUE;

			case CMMPURD:
				m_dCMMPURD = *pData;
				return TRUE;

			case CMMPURS:
				m_dCMMPURS = *pData;
				return TRUE;

			case CMSIPP:
				m_dCMSIPP = *pData;
				return TRUE;

			case CMSIPD:
				m_dCMSIPD = *pData;
				return TRUE;

			case CMERR:
				m_dError = 0;
				return TRUE;

			case CMUNOP:
				m_dCMUNOP = *pData;
				return TRUE;
		}
	}

	if( Addr.a.m_Table == TSEEPR ) {

		return !CanWriteToEEPROM(Addr.a.m_Table, Addr.a.m_Offset);
	}

	return Addr.a.m_Table == TSID; // can't write Drive Information
}

BOOL  CACTechSSUDPDriver::CanWriteToEEPROM(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case addrNamed:
		case TSEEPR:

			switch( uOffset ) {

				case CMCL:
				case CMPC:
				case CMPFPG:
				case CMPFDG:
				case CMVFPG:
				case CMPFIL:
				case CMVFIG:
				case CMVFFG:
				case CMPFIG:
				case CMMPE:
				case CMMPET:
				case CMHL:
				case CMSL:
				case CMOC:
					return TRUE;

				default:
					return FALSE;
			}

		case TSOC:
			return TRUE;
	}

	return FALSE;
}

DWORD CACTechSSUDPDriver::GetRealData(void)
{
	DWORD dF = GetValue(DATA0, 4);
	DWORD dI = GetValue(DATA0 + 4, 4);

	BOOL fNeg = FALSE;

	if( dI & 0x80000000 ) {

		fNeg = TRUE;

		dI  += 1;

		dF  ^= 0xFFFFFFFF;
	}

	char s1[32];
	char s2[16];

	memset(s1, 0, sizeof(s1));
	memset(s2, 0, sizeof(s2));

	char * ssI = &s1[0];
	char * ssF = &s2[0];

	SPrintf(ssI, "%d", dI);

	s1[strlen(ssI)] = '.';

	ConvertDF(&dF); // scale 0-0x100000000 to 0-100000000

	SPrintf(ssF, "%8.8d", dF);

	memcpy(&s1[strlen(ssI)], ssF, 8); // ss2 = "iiiiiiii.ffffffff"

	DWORD dwReal = ATOF((const char *) &s1[0]);

	if( fNeg ) {

		dwReal |= 0x80000000;
	}

	return dwReal;
}

void CACTechSSUDPDriver::PutRealData(PDWORD pData)
{
	if( *pData == 0 || *pData == 0x80000000 ) {

		AddZero();

		return;
	}

	DWORD dData;
	DWORD dDataI;

	char s[36]  = { 0 };
	char sI[10] = { 0 };
	char sF[10] = { 0 };

	char * ss  = &s[0];
	char * ssI = &sI[0];
	char * ssF = &sF[0];

	UINT uPos   = 0;

	BOOL fDone  = FALSE;
	BOOL fNeg   = FALSE;

	if( *pData & 0x80000000 ) {

		fNeg = TRUE;

		*pData &= 0x7FFFFFFF;
	}

	SPrintf(ss, "%f", PassFloat(*pData));

	if( !strlen(ss) ) {

		AddZero();

		return;
	}

	while( ss[uPos] == '+' || ss[uPos] == ' ' || ss[uPos] == '0' ) {

		uPos++;

		if( uPos >= strlen(ss) ) {

			AddZero();
			return;
		}
	}

	if( uPos ) {

		memcpy(ss, &(ss[uPos]), min(strlen(ss), sizeof(s)));
	}

	uPos = 0;

	while( !fDone ) {

		if( ss[uPos] == '.' ) {

			fDone = TRUE;

			if( uPos ) {

				memcpy(ssI, ss, uPos);
			}

			else { // char 0 == '.'
				sI[0] = '0';
				sI[1] = 0;
			}

			memcpy(ssF, &(ss[uPos+1]), min(strlen(ss), 9));
		}

		else {
			if( uPos >= strlen(ss) ) {

				sF[0] = '0';
				sF[1] = 0;

				fDone = TRUE;
			}
		}

		++uPos;
	}

	if( !strcmp(ssF, "0") ) {

		AddLong(0);

		dData = ATOI(ssI);

		AddLong(fNeg ? -dData : dData);

		return;
	}

// desired result is the decimal fraction * (2^32).
// e.g. 0.5 should result in 0x80000000

	DWORD dDiv = 625; // = 10000/16

	UINT uDigit;

	dData = 0;

	for( uPos = 0; uPos < strlen(ssF); uPos++ ) {

		uDigit = ssF[uPos] - '0';

		switch( uPos ) { // first 3 digits would overflow

			case 0:
				switch( uDigit ) {

					case 0:	dData = 0; break;
					case 1: dData = 0x19999999;	break; // 2^32 / 10
					case 2: dData = 0x33333333;	break;
					case 3: dData = 0x4CCCCCCC;	break;
					case 4: dData = 0x66666666;	break;
					case 5: dData = 0x80000000;	break;
					case 6: dData = 0x99999999;	break;
					case 7: dData = 0xB3333333;	break;
					case 8: dData = 0xCCCCCCCC;	break;
					case 9: dData = 0xE6666666;	break;
						break;
				}
				break;

			case 1:
				switch( uDigit ) {

					case 0:				break;
					case 1: dData += 0x28F5C28;	break; // 2^32 / 100
					case 2: dData += 0x51EB851;	break;
					case 3: dData += 0x7AE147A;	break;
					case 4: dData += 0xA3D70A3;	break;
					case 5: dData += 0xCCCCCCC;	break;
					case 6: dData += 0xF5C28F5;	break;
					case 7: dData += 0x11EB851E;	break;
					case 8: dData += 0x147AE147;	break;
					case 9: dData += 0x170A3D70;	break;
						break;
				}
				break;

			case 2:
				switch( uDigit ) {

					case 0:				break;
					case 1: dData += 0x418937;	break; // 2^32 / 1000
					case 2: dData += 0x83126E;	break;
					case 3: dData += 0xC49BA5;	break;
					case 4: dData += 0x10624DD;	break;
					case 5: dData += 0x147AE14;	break;
					case 6: dData += 0x189374B;	break;
					case 7: dData += 0x1CAC083;	break;
					case 8: dData += 0x20C49BA;	break;
					case 9: dData += 0x24DD2F1;	break;
						break;
				}
				break;

			default:
				dData += uDigit * 0x10000000 / dDiv;

				dDiv *= 10;
		}
	}

	dDataI = ATOI(ssI);

	if( fNeg ) {

		dDataI = -dDataI - 1;

		dData  ^= 0xFFFFFFFF;
	}

	AddLong(dData);
	AddLong(dDataI);
}

DWORD CACTechSSUDPDriver::GetValue(UINT uPos, UINT uCount)
{
	DWORD dData = 0;

	for( UINT i = 1; i <= uCount; i++ ) {

		dData <<= 8;

		dData += m_bRx[uPos + uCount - i];
	}

	return dData;
}

void CACTechSSUDPDriver::ConvertDF(PDWORD pdF)
{
// Scale data 0 - 0x100000000 to 0 - decimal 100000000 ).  G3 resolution is .00000001.
// Each entering digit is multiplied by 10000000 * 1/16.
// Then divided by 16^n (n = digit position) and added to previous 64 bit result
// example: 0x80000000 in -> 50000000 out ( 0x80000000/0x100000000 = .5 )
// example: 0xAAAAAAAA in -> 66666666 out ( 0xAAAAAAAA/0x100000000 = .666...)

	#define K1 6250000 // = 1/16 shifted up so that the decimal digit is digit*10^8

	DWORD dF = *pdF;

	if( dF > 0xFFFFFFCF ) {

		*pdF = 99999999;

		return;
	}

	if( dF < 21 ) { // 42.949.. -> .00000001

		*pdF = 0;

		return;
	}

	DWORD dRH   = 0; // high dword of sum of decimal values
	DWORD dRL   = 0; // low dword of sum of decimal values
	DWORD dR1   = 0; // digit * K1
	DWORD dR2   = 0; // (digit * K1) / (16^n)
	DWORD dMask = 0;

	for( UINT uShift = 0; uShift <= 28; uShift += 4 ) {

		dR1 = ((dF >> (28 - uShift)) & 0xF) * K1;

		dRH += dR1 >> uShift;

		dR2 = (dR1 & dMask) << (32 - uShift);

		if( dR2 + dRL < dRL ) {

			dRH++;
		}

		dRL   += dR2;

		dMask <<= 4;

		dMask |= 0xF;
	}

	*pdF = dRH;
}

BOOL  CACTechSSUDPDriver::CheckFrame(void)
{
	for( UINT i = 1; i < RIP; i++ ) {

		if( m_bRx[i] != m_bTx[i] ) {

			return FALSE;
		}
	}

	for( i = RPRTC; i < RCNTL; i++ ) {

		if( m_bRx[i] != m_bTx[i] ) {

			return FALSE;
		}
	}

	if( m_bRx[RCNTH] ) {

		return FALSE; // Frame too big
	}

	return TRUE;
}

BOOL  CACTechSSUDPDriver::SetErrorWord(void)
{
	if( m_bRx[RSTAT] ) {

		m_dError = DWORD(m_bRx[RSTAT]) + (DWORD(m_bRx[RFNCL]) << 16);

		return TRUE;
	}

	return FALSE;
}

// End of File
