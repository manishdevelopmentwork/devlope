
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudSparkplug_HPP

#define INCLUDE_ServiceCloudSparkplug_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sparkplug Cloud Client Configuration
//

class CServiceCloudSparkplug : public CServiceCloudBase
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudSparkplug(void);

	// Initial Values
	void SetInitValues(void);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Persistance
	void Init(void);
	void PostLoad(void);

	// Property Save Filter
	BOOL SaveProp(CString Tag) const;

	// Conversion
	void PostConvert(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Item Properties
	UINT	     m_Reboot;
	CString	     m_AuthFile;
	CCodedItem * m_pGroupId;
	CCodedItem * m_pNodeId;
	CCodedItem * m_pTagsFolder;
	CCodedItem * m_pPriAppId;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	void InitOptions(void);
};

// End of File

#endif
