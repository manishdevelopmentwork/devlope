
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../Model/OpcDataModel.hpp"

#include "../Model/OpcNode.hpp"

#include "../Model/OpcReference.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Continuation

struct CContPoint
{
	UINT			uMagic1;
	OpcUa_BrowseDescription NodeToBrowse;
	UINT			uStart;
	UINT			nMaxRefs;
	UINT			uMagic2;
};

// Services

OpcUa_StatusCode COpcServer::Browse(OpcUa_Endpoint                 hEndpoint,
				    OpcUa_Handle                   hContext,
				    const OpcUa_RequestHeader *    pRequestHeader,
				    const OpcUa_ViewDescription *  pView,
				    OpcUa_UInt32                   nRequestedMaxReferencesPerNode,
				    OpcUa_Int32                    nNoOfNodesToBrowse,
				    OpcUa_BrowseDescription *	    pNodesToBrowse,
				    OpcUa_ResponseHeader *         pResponseHeader,
				    OpcUa_Int32 *                  pNoOfResults,
				    OpcUa_BrowseResult **          pResults,
				    OpcUa_Int32 *                  pNoOfDiagnosticInfos,
				    OpcUa_DiagnosticInfo **        pDiagnosticInfos
)
{
	AfxTrace("opc: browse\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		if( !nNoOfNodesToBrowse ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);

			return OpcUa_BadNothingToDo;
		}

		int nMaxRefs  = nRequestedMaxReferencesPerNode ? Min(nRequestedMaxReferencesPerNode, m_nMaxRefs) : m_nMaxRefs;

		*pResults     = OpcAlloc(nNoOfNodesToBrowse, OpcUa_BrowseResult);

		*pNoOfResults = nNoOfNodesToBrowse;

		m_pModel->Lock();

		for( int m = 0; m < nNoOfNodesToBrowse; m++ ) {

			OpcUa_BrowseDescription *pDesc   = pNodesToBrowse + m;

			OpcUa_BrowseResult      *pResult = *pResults      + m;

			COpcNodeId               NodeId  = COpcNodeId(pDesc->NodeId);

			COpcNode                *pNode   = m_pModel->FindNode(NodeId, true);

			OpcUa_BrowseResult_Initialize(pResult);

			if( pNode ) {

				AfxTrace("    %2u) %s (%s)\n",
					 m,
					 PCTXT(pNode->GetId().GetAsText()),
					 PCTXT(pNode->GetBrowseName())
				);

				pResult->StatusCode = BrowseNode(Session, pResult, pDesc, pNode, 0, nMaxRefs);
			}
			else {
				AfxTrace("    %2u) %s (Unknown)\n",
					 m,
					 PCTXT(NodeId.GetAsText())
				);

				pResult->StatusCode = OpcUa_BadNodeIdUnknown;
			}
		}

		m_pModel->Free();

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
	}

	return pResponseHeader->ServiceResult;
}

OpcUa_StatusCode COpcServer::BrowseNext(OpcUa_Endpoint              hEndpoint,
					OpcUa_Handle                hContext,
					const OpcUa_RequestHeader * pRequestHeader,
					OpcUa_Boolean               bReleaseContinuationPoints,
					OpcUa_Int32                 nNoOfContinuationPoints,
					const OpcUa_ByteString *    pContinuationPoints,
					OpcUa_ResponseHeader *      pResponseHeader,
					OpcUa_Int32 *               pNoOfResults,
					OpcUa_BrowseResult **       pResults,
					OpcUa_Int32 *               pNoOfDiagnosticInfos,
					OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	AfxTrace("opc: browsenext\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	UINT n;

	if( (n = FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, false)) < NOTHING ) {

		CSession &Session = m_Session[n];

		if( !nNoOfContinuationPoints ) {

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_BadNothingToDo);

			return OpcUa_BadNothingToDo;
		}

		if( bReleaseContinuationPoints ) {

			AfxTrace("     release\n");

			FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

			return OpcUa_Good;
		}

		*pResults     = OpcAlloc(nNoOfContinuationPoints, OpcUa_BrowseResult);

		*pNoOfResults = nNoOfContinuationPoints;

		m_pModel->Lock();

		for( int m = 0; m < nNoOfContinuationPoints; m++ ) {

			OpcUa_BrowseResult *pResult = *pResults + m;

			CContPoint         *pCont   = (CContPoint *) pContinuationPoints[m].Data;

			if( pCont->uMagic1 == UINT(this) && pCont->uMagic2 == UINT(m_StartTime.dwLowDateTime) ) {

				OpcUa_BrowseDescription *pDesc   = &pCont->NodeToBrowse;

				COpcNodeId               NodeId  = COpcNodeId(pDesc->NodeId);

				COpcNode                *pNode   = m_pModel->FindNode(NodeId, true);

				OpcUa_BrowseResult_Initialize(pResult);

				if( pNode ) {

					AfxTrace("    %2u) %s (%s) from %u\n",
						 m,
						 PCTXT(pNode->GetId().GetAsText()),
						 PCTXT(pNode->GetBrowseName()),
						 pCont->uStart
					);

					pResult->StatusCode = BrowseNode(Session, pResult, pDesc, pNode, pCont->uStart, pCont->nMaxRefs);
				}
				else
					pResult->StatusCode = OpcUa_BadNodeIdUnknown;
			}
			else
				pResult->StatusCode = OpcUa_BadContinuationPointInvalid;
		}

		m_pModel->Free();

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
	}

	return OpcUa_Bad;
}

OpcUa_StatusCode COpcServer::TranslateBrowsePathsToNodeIds(OpcUa_Endpoint              hEndpoint,
							   OpcUa_Handle                hContext,
							   const OpcUa_RequestHeader * pRequestHeader,
							   OpcUa_Int32                 nNoOfBrowsePaths,
							   const OpcUa_BrowsePath *    pBrowsePaths,
							   OpcUa_ResponseHeader *      pResponseHeader,
							   OpcUa_Int32 *               pNoOfResults,
							   OpcUa_BrowsePathResult **   pResults,
							   OpcUa_Int32 *               pNoOfDiagnosticInfos,
							   OpcUa_DiagnosticInfo **     pDiagnosticInfos
)
{
	AfxTrace("opc: translatebrowsepathstonodeids\n");

	*pNoOfDiagnosticInfos = 0;

	*pDiagnosticInfos     = OpcUa_Null;

	if( FindSession(hEndpoint, hContext, pRequestHeader, pResponseHeader, true) < NOTHING ) {

		*pResults     = OpcAlloc(nNoOfBrowsePaths, OpcUa_BrowsePathResult);

		*pNoOfResults = nNoOfBrowsePaths;

		m_pModel->Lock();

		for( int m = 0; m < nNoOfBrowsePaths; m++ ) {

			OpcUa_BrowsePathResult *pResult = *pResults + m;

			OpcUa_BrowsePath const *pPath   = pBrowsePaths + m;

			COpcNodeId              NodeId  = COpcNodeId(pPath->StartingNode);

			COpcNode               *pNode   = m_pModel->FindNode(NodeId, true);

			// TODO -- We are only performing exact matches here based on
			// both the reference type and the name. The spec supports a
			// broader operation that can return multiple targets etc. This
			// works for now as it's all we need to test Historical Access.

			for( int e = 0; e < pPath->RelativePath.NoOfElements; e++ ) {

				OpcUa_RelativePathElement const &Element = pPath->RelativePath.Elements[e];

				COpcNodeId                       RefId   = COpcNodeId(Element.ReferenceTypeId);

				COpcNode                        *pFound  = NULL;

				UINT c = pNode->GetReferenceCount();

				for( UINT r = 0; r < c; r++ ) {

					COpcReference const &Ref = pNode->GetReference(r);

					if( Ref.IsInverse() == (Element.IsInverse ? true : false) ) {

						if( Ref.GetTypeId() == RefId ) {

							COpcNodeId TargId  = Ref.GetTargetId();

							COpcNode * pTarget = m_pModel->FindNode(TargId, true);

							if( pTarget->GetBrowseName() == Element.TargetName.Name.strContent ) {

								pFound = pTarget;

								break;
							}
						}
					}
				}

				if( !(pNode = pFound) ) {

					break;
				}
			}

			if( pNode ) {

				AfxTrace("    %2u) %s (%s)\n",
					 m,
					 PCTXT(pNode->GetId().GetAsText()),
					 PCTXT(pNode->GetBrowseName())
				);

				pResult->NoOfTargets = 1;

				pResult->Targets     = OpcAlloc(1, OpcUa_BrowsePathTarget);

				OpcUa_BrowsePathTarget &Target = pResult->Targets[0];

				OpcUa_ExpandedNodeId   &NodeId = Target.TargetId;

				Target.RemainingPathIndex = 0;

				OpcUa_String_AttachCopy(&NodeId.NamespaceUri, "");

				NodeId.ServerIndex  = 0;

				NodeId.NodeId       = pNode->GetId();

				pResult->StatusCode = OpcUa_Good;
			}
			else {
				pResult->NoOfTargets = 0;

				pResult->Targets     = NULL;

				pResult->StatusCode  = OpcUa_BadNoMatch;
			}
		}

		m_pModel->Free();

		FillResponse(pResponseHeader, pRequestHeader, OpcUa_Good);

		return OpcUa_Good;
	}

	return OpcUa_Bad;
}

// Browse Helpers

UINT COpcServer::BrowseNode(CSession &Session, OpcUa_BrowseResult *pResult, OpcUa_BrowseDescription *pDesc, COpcNode *pNode, UINT uStart, UINT uLimit)
{
	// TODO -- Refactor to match Read and Write...

	UINT uRefs;

	if( (uRefs = pNode->GetReferenceCount()) ) {

		UINT uFind = 0;

		for( UINT n = uStart; n < uRefs; n++ ) {

			COpcReference const &Ref      = pNode->GetReference(n);

			COpcNode            *pTarget  = m_pModel->FindNode(Ref.GetTargetId(), false);

			COpcNodeId           DescType = COpcNodeId(pDesc->ReferenceTypeId);

			if( Session.m_fAuthed || !pTarget->IsPrivate() ) {

				if( IsChildNode(DescType, Ref.GetTypeId(), pDesc->IncludeSubtypes ? true : false) ) {

					if( CheckMask(pDesc->NodeClassMask, pNode->GetClass()) ) {

						if( CheckDirection(Ref, pDesc->BrowseDirection) ) {

							if( uFind < uLimit ) {

								pResult->References               = OpcReAlloc(pResult->References, uFind + 1, OpcUa_ReferenceDescription);

								OpcUa_ReferenceDescription *pSlot = pResult->References + uFind;

								OpcUa_ReferenceDescription_Initialize(pSlot);

								AfxTrace("        %s %s %s\n",
									 PCTXT(Ref.GetTypeId().GetAsText()),
									 Ref.IsInverse() ? "<-" : "->",
									 PCTXT(Ref.GetTargetId().GetAsText())
								);

								if( CheckMask(pDesc->ResultMask, OpcUa_BrowseResultMask_ReferenceTypeId) ) {

									pSlot->ReferenceTypeId = Ref.GetTypeId();
								}

								if( CheckMask(pDesc->ResultMask, OpcUa_BrowseResultMask_IsForward) ) {

									pSlot->IsForward = !Ref.IsInverse();
								}

								if( true ) {

									pSlot->NodeId.NodeId      = pTarget->GetId();

									pSlot->NodeId.ServerIndex = 0;
								}

								if( CheckMask(pDesc->ResultMask, OpcUa_BrowseResultMask_BrowseName) ) {

									OpcUa_String_AttachCopy(&pSlot->BrowseName.Name, PTXT(PCTXT(pTarget->GetBrowseName())));

									pSlot->BrowseName.NamespaceIndex = pTarget->GetId().GetNamespace();
								}

								if( CheckMask(pDesc->ResultMask, OpcUa_BrowseResultMask_DisplayName) ) {

									OpcUa_String_AttachCopy(&pSlot->DisplayName.Text, PTXT(PCTXT(pTarget->GetDisplayName())));

									OpcUa_String_AttachCopy(&pSlot->DisplayName.Locale, "");
								}

								if( CheckMask(pDesc->ResultMask, OpcUa_BrowseResultMask_NodeClass) ) {

									pSlot->NodeClass = (OpcUa_NodeClass) pTarget->GetClass();
								}

								if( CheckMask(pDesc->ResultMask, OpcUa_BrowseResultMask_TypeDefinition) ) {

									if( pTarget->IsClass(OpcUa_NodeClass_Object) || pTarget->IsClass(OpcUa_NodeClass_Variable) ) {

										UINT uRefs = pTarget->GetReferenceCount();

										for( UINT n = 0; n < uRefs; n++ ) {

											COpcReference const &Ref = pTarget->GetReference(n);

											if( Ref.GetTypeId().GetNumericValue() == OpcUaId_HasTypeDefinition ) {

												pSlot->TypeDefinition.NodeId      = Ref.GetTargetId();

												pSlot->TypeDefinition.ServerIndex = 0;
											}
										}
									}
								}

								uFind++;
							}
							else {
								CContPoint *pCont   = OpcAlloc(1, CContPoint);

								pCont->uMagic1      = UINT(this);

								pCont->NodeToBrowse = *pDesc;

								pCont->uStart       = n;

								pCont->nMaxRefs     = uLimit;

								pCont->uMagic2      = UINT(m_StartTime.dwLowDateTime);

								pResult->ContinuationPoint.Data   = PBYTE(pCont);

								pResult->ContinuationPoint.Length = sizeof(CContPoint);

								break;
							}
						}
					}
				}
			}
		}

		if( uFind ) {

			AfxTrace("        found %u\n", uFind);

			pResult->NoOfReferences = uFind;

			return OpcUa_Good;
		}
	}

	pResult->NoOfReferences = 0;

	pResult->References     = OpcReAlloc(pResult->References, 1, OpcUa_ReferenceDescription);

	return OpcUa_Good;
}

bool COpcServer::IsChildNode(COpcNodeId const &DescType, COpcNodeId const &RefType, bool fIncSub)
{
	if( DescType.IsType(OpcUa_IdType_Numeric) && DescType.GetNumericValue() == 0 ) {

		return true;
	}

	if( DescType == RefType ) {

		return true;
	}

	if( fIncSub ) {

		COpcNode *pDesc = m_pModel->FindNode(DescType, true);

		if( pDesc ) {

			UINT uRefs = pDesc->GetReferenceCount();

			for( UINT n = 0; n < uRefs; n++ ) {

				COpcReference const &Ref    = pDesc->GetReference(n);

				COpcNodeId    const &Target = Ref.GetTargetId();

				if( Target == RefType ) {

					return true;
				}

				if( !Ref.IsInverse() ) {

					COpcNode *pNode = m_pModel->FindNode(Target, true);

					if( pNode ) {

						if( pNode->GetReferenceCount() ) {

							if( IsChildNode(Target, RefType, true) ) {

								return true;
							}
						}
					}
				}
			}
		}
	}

	return false;
}

bool COpcServer::CheckMask(DWORD dwMask, DWORD dwAttr)
{
	return !dwMask || (dwAttr & dwMask);
}

bool COpcServer::CheckDirection(COpcReference const &Ref, OpcUa_BrowseDirection BrowseDir)
{
	switch( BrowseDir ) {

		case OpcUa_BrowseDirection_Both:

			return true;

		case OpcUa_BrowseDirection_Forward:

			return !Ref.IsInverse();

		case OpcUa_BrowseDirection_Inverse:

			return Ref.IsInverse();

		case OpcUa_BrowseDirection_Invalid:

			return false;
	}

	return false;
}

// End of File
