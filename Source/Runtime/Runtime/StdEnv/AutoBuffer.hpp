
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoBuffer_HPP

#define	INCLUDE_AutoBuffer_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Instantiated Classes
//

#include "Buffer.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Buffer
//

class DLLAPI CAutoBuffer
{
public:
	// Constructors

	STRONG_INLINE CAutoBuffer(void)
	{
		m_pBuff = NULL;
	}

	STRONG_INLINE CAutoBuffer(CBuffer *pBuff)
	{
		m_pBuff = pBuff;
	}

	STRONG_INLINE CAutoBuffer(UINT uSize)
	{
		m_pBuff = BuffAllocate(uSize);
	}

	// Destructor

	STRONG_INLINE ~CAutoBuffer(void)
	{
		AfxRelease(m_pBuff);
	}

	// Operations

	STRONG_INLINE CBuffer * TakeOver(void)
	{
		AfxAssert(m_pBuff);

		CBuffer *pBuff = m_pBuff;

		m_pBuff        = NULL;

		return pBuff;
	}

	// Conversion

	STRONG_INLINE operator bool(void) const
	{
		return m_pBuff ? true : false;
	}

	STRONG_INLINE operator CBuffer * (void) const
	{
		return m_pBuff;
	}

	STRONG_INLINE operator CBuffer * & (void)
	{
		return m_pBuff;
	}

	// Operators

	STRONG_INLINE bool operator ! (void) const
	{
		return m_pBuff ? false : true;
	}

	STRONG_INLINE CBuffer * operator -> (void) const
	{
		return m_pBuff;
	}

protected:
	// Data Members
	CBuffer * m_pBuff;

private:
	// No Assign or Copy

	void operator = (CAutoBuffer const &That) const {};

	CAutoBuffer(CAutoBuffer const &That) {};
};

// End of File

#endif
