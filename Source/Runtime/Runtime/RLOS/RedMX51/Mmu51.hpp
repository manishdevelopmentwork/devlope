
#include "Intern.hpp"

/////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Mmu51_HPP
	
#define	INCLUDE_Mmu51_HPP

//////////////////////////////////////////////////////////////////////////
//
// MMU Register Access
//

#if !defined(__INTELLISENSE__)

#define	HostGetTable(v)		ASM(	"mrc p15, 0, %0, c2, c0, 0"	\
					: "=r"(b)			\
					)				\

#define	HostGetSCTR(v)		ASM(	"mrc p15, 0, %0, c1, c0, 0"	\
					: "=r"(v)			\
					)				\

#define	HostGetPRRR(v)		ASM(	"mrc p15, 0, %0, c10, c2, 0"	\
					: "=r"(v)			\
					)				\

#define	HostGetNMRR(v)		ASM(	"mrc p15, 0, %0, c10, c2, 1"	\
					: "=r"(v)			\
					)				\

#define HostFlushTLBs()		ASM(	"mov r1,  #0\n\t"		\
					"mcr p15, 0, r1, c8, c7, 0"	\
					: : : "r1"			\
					)				\

#else

#define	HostGetTable(r)		((void) (v=0))

#define	HostGetSCTR(v)		((void) (v=0))

#define	HostGetPRRR(v)		((void) (v=0))

#define	HostGetNMRR(v)		((void) (v=0))

#define HostFlushTLBs()		((void) 0)	

#endif

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Memory Management Unit
//

class CMmu51 : public IMmu
{
	public:
		// Constructor
		CMmu51(void);

		// IMmu
		bool  IsVirtualValid(DWORD dwAddr, bool fWrite);
		DWORD PhysicalToVirtual(DWORD dwAddr, bool fCached);
		DWORD VirtualToPhysical(DWORD dwAddr);
		void  DebugRegister(void);
		void  DebugRevoke(void);

	protected:
		// MMU Descriptor
		union DESC
		{
			struct {
				DWORD	PXN:1;	  // 0
				DWORD	T0:1;	  // 1
				DWORD	B:1;	  // 2
				DWORD	C:1;	  // 3
				DWORD	XN:1;	  // 4
				DWORD	Domain:4; // 5-8
				DWORD	ID:1;	  // 9
				DWORD	AP:2;	  // 10-11
				DWORD	TEX:3;	  // 12-14
				DWORD	AP2:1;	  // 15
				DWORD	S:1;	  // 16
				DWORD	nG:1;	  // 17
				DWORD	T1:1;	  // 18
				DWORD	NS:1;	  // 19
				DWORD	Base:12;  // 20
				};
			
			DWORD d;
			};

		// Data Members
		DESC * m_pDesc;
		UINT   m_uStep;
		DWORD  m_dwMask;

		// Implementation
		bool IsSection(DESC const &d);
		bool IsCached(DESC const &d);
		bool FindGranularity(void);
		UINT Gcd64(UINT64 a, UINT64 b);
		UINT Gcd32(UINT a, UINT b);
		void BlockUnused(void);
		void SetTypes(void);
		void BlockRange(DWORD a, DWORD b);
		void SetRangeType(DWORD a, DWORD b, UINT t);
		void ShowAll(void);
		void ShowAddress(DWORD a);
		void ShowTypes(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Implementation

STRONG_INLINE bool CMmu51::IsSection(DESC const &d)
{
	return !d.T1 && d.T0 && !d.PXN;
	}

STRONG_INLINE bool CMmu51::IsCached(DESC const &d)
{
	if( d.TEX & 4 ) {

		return (d.TEX & 3) || d.C || d.B;
		}

	return d.C || d.B;
	}

// End of File

#endif
