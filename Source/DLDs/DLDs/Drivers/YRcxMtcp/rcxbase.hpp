#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yamaha RCX Series Structure
//

struct FAR Cmd {

		UINT	m_ID;
		char	m_Text[9];
		UINT	m_Form;
		UINT	m_Resp;
		UINT	m_Acc;
		UINT    m_Radix;
		};

const char cAxis [] = { 'X', 'Y', 'Z', 'R', 'A', 'B' };

const char cDir  [] = { '+', '-' };

struct POINTS {

	UINT	m_Index;
	DWORD	m_Point[6];
	BYTE	m_Flag;
	};
	

//////////////////////////////////////////////////////////////////////////
//
// Yamaha RCX Series Enumerations
//

enum
{
	formCmd		= 1,
	formCmdArg1	= 2,
	formCmdArg2	= 3,
	formInternal	= 4,
	formDevice	= 5,
	formVar		= 6,
	formTerminal	= 7,
	formStatus	= 8,
	formDate	= 9,
	formTime	= 10,
	formValidate    = 11,
	formFunc	= 12,
	formServ	= 13,
	formMove	= 14,
     	};

enum
{
	respOK	 = 1,
	respVal  = 2,
	respNone = 3,
	respTerm = 4,
	respWait = 5,
	respMult = 6,
	respText = 7,
	respSync = 8,
	respSpec = 9,
	respAbort= 10,
	};

enum
{
	accW	= 1,
	accR	= 2,
	accRW	= 3,
	};

enum
{
	radixOct = 8,
	radixDec = 10,
	radixHex = 16,
	};

//////////////////////////////////////////////////////////////////////////
//
// Yamaha RCX Series Base Driver
//

class CYamahaRcxMbaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CYamahaRcxMbaseDriver(void);

		// Destructor
		~CYamahaRcxMbaseDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Context
		struct CBaseCtx
		{	
			UINT m_uBrkLn;
			UINT m_uBrkPt;
			char m_Error[256];
			char m_Term[80];
			BYTE m_Busy;
			BOOL m_fInit;
			
			POINTS * m_Table;
			UINT     m_uPoints;

			BOOL m_fLogOff;
			};

		static Cmd CODE_SEG m_Cmd[];
		Cmd FAR * m_pCmd;
		Cmd FAR * m_pItem;

	protected:
		// Data Members
		CBaseCtx * m_pBase;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uTxPtr;
		UINT	   m_uRxPtr;
		LPCTXT	   m_pHex;

		// Help
		IExtraHelper	 * m_pExtra;
	
		// Implementation
		void FindCmd(UINT uID);
		UINT FindID(AREF Addr);
		void Begin(void);
		void End(void);
		void Construct(AREF Addr, BOOL fWrite);
		void AddArgs(AREF Addr, PDWORD pData, UINT uCount);
		void AddByte(BYTE bByte);
		void AddValue(UINT uValue);
		void AddReal(UINT uValue);
		void AddText(LPCTXT pText);
		BOOL GetData(PDWORD pData, UINT uCount, UINT uType, UINT uOffset);
		BOOL GetMulti(PDWORD pData, UINT uCount, UINT uType, UINT uOffset);
		void GetValue(PDWORD pData);
		void GetReal(PDWORD pData);
		void FindFormat(void);
		BYTE FromAscii(BYTE bByte);
		BOOL SendData(UINT uType, PDWORD pData, UINT uCount);

		// Helpers
	 	BOOL IsLive(PDWORD pData);
		BOOL CanWrite(UINT uOffset);
		BOOL IsZeroAllowed(void);
		BOOL SetInternal(DWORD dwData);
		BOOL GetInternal(PDWORD pData, UINT uCount);
		void SetError(void);
		BOOL IsMin(void);
		BOOL IsDigit(BYTE bByte);
		BOOL IsStatus(void);
		BOOL IsAbort(void);
		BOOL HandleBusy(void);

		// Point Data Support
		POINTS * FindTable(UINT uIndex);
		UINT	 FindPointTableOffset(AREF Addr);
		BOOL	 SendPointTableCmd(AREF Addr);
					
		// Transport Layer
		virtual BOOL Transact(void);
		virtual BOOL RecvFrame(BOOL fLogin);
		virtual BOOL CheckFrame(void);
	};

// End of File
