
#include "intern.hpp"

#include "fam3m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yokogawa FA-M3 PLC Master Driver
//

// Instantiator

INSTANTIATE(CFam3MasterDriver);

// Constructor

CFam3MasterDriver::CFam3MasterDriver(void)
{
	
	}

// Destructor

CFam3MasterDriver::~CFam3MasterDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pCtx = NULL;
	}

// Configuration

void MCALL CFam3MasterDriver::Load(LPCBYTE pData)
{

	}
	
// Management

void MCALL CFam3MasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CFam3MasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CFam3MasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_uCpu 	= GetByte(pData);

			m_pCtx->m_uMode = GetByte(pData);

			m_pCtx->m_uStation = GetByte(pData);

			m_pCtx->m_uWait = GetByte(pData);

			m_pCtx->m_fCheck = GetByte(pData) ? TRUE : FALSE;

			m_pCtx->m_fTerm = GetByte(pData) ? TRUE : FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CFam3MasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CFam3MasterDriver::Ping(void)
{
	return CFam3BaseMasterDriver::Ping();
	}

// Implementation

/*** NOTE:  Serial Modules do  not support binary format at this time, 
	    therefore binary code in this module remains untested.
***/

BOOL CFam3MasterDriver::Transact(UINT uCmd)
{      
	if( SendFrame() && RecvFrame() ) {
		
		return CheckFrame(uCmd);
		}

	return FALSE;
	}

BOOL CFam3MasterDriver::SendFrame(void)
{
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER); 

	return TRUE; 
	}

// Overidables

BOOL CFam3MasterDriver::CheckFrame(UINT uCmd)
{
	if( m_pCtx->m_uMode ) {

		if( UINT(m_bRxBuff[1] - 0x30) == m_pCtx->m_uCpu ) {
			
			return ( m_bRxBuff[2] == 'O' && m_bRxBuff[3] =='K' );
			}

		return FALSE;
		}

	return ( (m_bRxBuff[0] == (uCmd + 0x80) ) && m_bRxBuff[1] == 0 );
	}

BOOL CFam3MasterDriver::RecvFrame(void)
{
	if( m_pCtx->m_uMode ) {

		return RecvAsciiFrame();
		}

	UINT uTimer = 0;

	UINT uData = 0;

	WORD wCount = 0;

	UINT uPtr = 0;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		m_bRxBuff[uPtr++] = uData;

		if( uPtr == 3 ) {

			wCount = BYTE(uData) << 16;
			}

		else if( uPtr == 4) {

			wCount |= uData;
			}

		else if ( uPtr > 2 ) {

			if( uPtr >= UINT(wCount + 4) ) {
			
				return TRUE;
				}
			}

		Sleep(10);
		}
	
	return FALSE;
	}

BOOL CFam3MasterDriver::RecvAsciiFrame(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	WORD wCount = 0;

	UINT uPtr = 0;

	BOOL fETX = FALSE;

	BOOL fSTX = FALSE;

	m_bCheck = 0;

	SetTimer(2000);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( !fSTX ) {

			if( uData == STX ) {

				fSTX = TRUE;
				 
				continue;
				}
			}
		
		if( fETX ) {

			return (uData == CR);
			}

		if( uData == ETX ) {

			if( m_pCtx->m_fCheck ) {

				m_bCheck -= m_bRxBuff[uPtr - 4];

				m_bCheck -= m_bRxBuff[uPtr - 3];

				if( (FromAscii(m_bRxBuff[uPtr - 4]) != (m_bCheck & 0xF0) >> 4) || 
				    (FromAscii(m_bRxBuff[uPtr - 3]) != (m_bCheck & 0x0F)) ) {

					return FALSE;				
					}
				}

			if( m_pCtx->m_fTerm ) {
				
				fETX = TRUE;

				continue;
				}

			return TRUE;
			}

		if( uPtr > 2 ) {

			m_bRxBuff[uPtr - 2] = uData;
			}

		if( uData != STX ) {

			m_bCheck += uData;
			}

		uPtr++;
				
		Sleep(10);
		}
		
	return FALSE;
	}

BOOL CFam3MasterDriver::Start(void)
{
	if( CFam3BaseMasterDriver::Start() ) {

		AddByte(STX);

		m_bCheck = 0;
		
		return TRUE;
		}

	return FALSE;
	}

void CFam3MasterDriver::AddPreamble(UINT uType, BOOL fWrite)
{
       	if( m_pCtx->m_uMode ) {

		AddDec(m_pCtx->m_uStation, 10);

		return;
		}
	
	switch( uType ) {

		case addrBitAsBit:
		case addrBitAsByte:
		case addrBitAsWord:

			AddByte(fWrite ? CMD_BW : CMD_BR);

			break;

		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			AddByte(fWrite ? CMD_WW : CMD_WR);

			break;
		}
	}

void CFam3MasterDriver::AddWaitTime(void)
{
	AddHex(m_pCtx->m_uWait, 0x1);
	}

void CFam3MasterDriver::AddTerm(void)
{
	if( m_pCtx->m_fCheck ) {
		
		AddHex(m_bCheck, 0x10);
		}

	AddByte(ETX);
	
	if( m_pCtx->m_fTerm ) {

		AddByte(CR);
		}
	}

void CFam3MasterDriver::GetCount(UINT &uCount, UINT uType)
{
	switch(uType) {

		case addrBitAsBit:

			MakeMin(uCount, 64);
			break;

		case addrBitAsByte:
			
			break;

		case addrBitAsWord:
		case addrWordAsWord:

			MakeMin(uCount, 32);
			break;

		case addrWordAsLong:
		case addrWordAsReal:
	
			MakeMin(uCount, 16);
			break;
		}
	}

BYTE CFam3MasterDriver::FromAscii(BYTE bByte)
{
	if( bByte >= '0' && bByte <= '9' )

		return bByte - '0';

	return	bByte - '@' + 9;
	}

// End of File
