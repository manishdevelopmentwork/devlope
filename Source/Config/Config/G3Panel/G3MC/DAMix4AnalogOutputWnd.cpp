
#include "intern.hpp"

#include "DAMix4AnalogOutputWnd.hpp"

#include "DAMix4AnalogOutput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix4AnalogOutputWnd, CUIViewWnd);

// Overibables

void CDAMix4AnalogOutputWnd::OnAttach(void)
{
	m_pItem   = (CDAMix4AnalogOutput *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("daao8_out"));

	CUIViewWnd::OnAttach();
}

// UI Update

void CDAMix4AnalogOutputWnd::OnUICreate(void)
{
	StartPage(1);

	AddOutputs();

	EndPage(FALSE);
}

// Implementation

void CDAMix4AnalogOutputWnd::AddOutputs(void)
{
	StartGroup(IDS("Initialization"), 1);

	AddUI(m_pItem, L"root", L"InitData");

	for( UINT n = 0; n < 3; n++ ) {

		AddUI(m_pItem, L"root", CPrintf("Data%u", n + 1));
	}

	EndGroup(TRUE);
}

// End of File
