
#include "intern.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Coded Item
//

// Constructor

CCodedItem::CCodedItem(void)
{
	m_uRefs = 0;

	m_pRefs = NULL;

	m_uCode = 0;

	m_pCode = NULL;
	}

// Destructor

CCodedItem::~CCodedItem(void)
{
	delete m_pRefs;

	delete m_pCode;
	}

// Initialization

void CCodedItem::Load(PCBYTE &pData)
{
	ValidateLoad("CCodedItem", pData);

	if( (m_uRefs = GetWord(pData)) ) {

		m_pRefs = New DWORD [ m_uRefs ];

		for( UINT n = 0; n < m_uRefs; n++ ) {

			m_pRefs[n] = GetLong(pData);
			}
		}

	if( (m_uCode = GetWord(pData)) ) { 

		m_pCode = New BYTE [ m_uCode ];

		memcpy(m_pCode, pData, m_uCode);

		pData += m_uCode;
		}
	}

// Server Access

IDataServer * CCodedItem::GetDataServer(void) const
{
	return CCommsSystem::m_pThis->GetDataServer();
	}

// Attributes

BOOL CCodedItem::IsEmpty(void) const
{
	return m_uCode == 0;
	}

BOOL CCodedItem::IsWritable(void) const
{
	if( m_pCode ) {
		
		if( m_pCode[0] & 0x80 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCodedItem::IsConst(void) const
{
	if( m_pCode ) {
		
		if( m_pCode[0] & 0x40 ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CCodedItem::GetRefCount(void) const
{
	return m_uRefs;
	}

UINT CCodedItem::GetType(void) const
{
	if( m_pCode ) {
		
		return m_pCode[0] & 0x0F;
		}

	return typeInteger;
	}

UINT CCodedItem::GetTagIndex(void) const
{
	CDataRef const &Ref = GetRef(0);

	return Ref.t.m_Index;
	}

// Reference Access

CDataRef const & CCodedItem::GetRef(UINT uRef) const
{
	return (CDataRef const &) m_pRefs[uRef];
	}

// Operations

BOOL CCodedItem::SetValue(DWORD Data, UINT Type, UINT Flags)
{
	if( m_pCode ) {

		IDataServer *pData = GetDataServer();
	
		DWORD Ref = C3ExecuteCode(m_pCode, pData, NULL);

		if( Type == typeInteger ) {
			
			if( GetType() == typeReal ) {

				Data = R2I(C3REAL(C3INT(Data)));

				Type = typeReal;
				}
			}
		
		if( Type == typeReal ) {
			
			if( GetType() == typeInteger ) {

				Data = C3INT(I2R(Data));

				Type = typeInteger;
				}
			}
		
		if( pData->SetData(Ref, Type, Flags, Data) ) {

			return TRUE;
			}
		}

	if( Type == typeString ) {

		Free(PUTF(Data));
		}

	return FALSE;
	}

// Execution

BOOL CCodedItem::IsAvail(void)
{
	if( m_uRefs ) {

		IDataServer *pData = GetDataServer();
	
		for( UINT n = 0; n < m_uRefs; n++ ) {

			if( !pData->IsAvail(m_pRefs[n]) ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CCodedItem::SetScan(UINT Code)
{
	if( ThisIsNull(this) ) {

		AfxTrace("*** NULL!\n");

		return TRUE;
		}

	if( m_uRefs ) {

		IDataServer *pData = GetDataServer();
	
		for( UINT n = 0; n < m_uRefs; n++ ) {

			pData->SetScan(m_pRefs[n], Code);
			}
		}

	return TRUE;
	}

DWORD CCodedItem::ExecVal(void)
{
	return Execute(GetType());
	}

DWORD CCodedItem::ExecVal(PDWORD pParam)
{
	return Execute(GetType(), pParam);
	}

DWORD CCodedItem::Execute(UINT Type)
{
	return Execute(Type, NULL);
	}

DWORD CCodedItem::Execute(UINT Type, PDWORD pParam)
{
	if( m_pCode ) {

		if( Type == typeLValue ) {

			// REV3 -- Is this a problem if we try to find the index of
			// an arrayed tag reference, and yet the tag isn't writable,
			// such that we have compiled it as a non-LValue?

			if( !IsWritable() ) {

				return 0;
				}
			}

		if( m_pCode[0] & 0x20 ) {

			CProgramItem * pProg    = New CProgramItem;

			pProg->m_fTemp          = TRUE;

			pProg->m_pCode          = New CCodedItem;

			pProg->m_pCode->m_pCode = New BYTE [ m_uCode ];

			pProg->m_pCode->m_uCode = m_uCode;

			memcpy(pProg->m_pCode->m_pCode, m_pCode, m_uCode);

			pProg->m_pCode->m_pCode[0] &= ~0x20;

			CCommsSystem::m_pThis->m_pPrograms->Dispatch(pProg, pParam, 3);
			}
		else {
			IDataServer *pData = GetDataServer();
	
			DWORD        Data  = C3ExecuteCode(m_pCode, pData, pParam);

			if( IsWritable() ) {

				if( Type < typeLValue ) {

					Data = pData->GetData(Data, GetType(), getNone);
					}
				}

			if( Type == typeInteger ) {
				
				if( GetType() == typeReal ) {

					Data = C3INT(I2R(Data));
					}
				}
			
			if( Type == typeReal ) {
				
				if( GetType() == typeInteger ) {

					Data = R2I(C3REAL(C3INT(Data)));
					}
				}

			return Data;
			}
		}

	return GetNull(Type);
	}

DWORD CCodedItem::GetNull(UINT Type)
{
	if( Type == typeString ) {

		return DWORD(wstrdup(L""));
		}

	if( Type == typeInteger ) {

		return 0;
		}

	return R2I(0);
	}

DWORD CCodedItem::GetNull(void)
{
	return GetNull(GetType());
	}

//////////////////////////////////////////////////////////////////////////
//
// Coded Text
//

// Constants

static WCHAR const cSep = 0xB6;

// Attributes

CUnicode CCodedText::GetText(void)
{
	CLangManager *pLang = CCommsSystem::m_pThis->m_pLang;

	UINT          uLang = pLang->GetCurrentSlot();

	return GetText(uLang, L"", NULL);
	}

CUnicode CCodedText::GetText(UINT uLang)
{
	return GetText(uLang, L"", NULL);
	}

CUnicode CCodedText::GetText(PCUTF pDefault)
{
	CLangManager *pLang = CCommsSystem::m_pThis->m_pLang;

	UINT          uLang = pLang->GetCurrentSlot();

	return GetText(uLang, pDefault, NULL);
	}

CUnicode CCodedText::GetText(UINT uLang, PCUTF pDefault)
{
	return GetText(uLang, pDefault, NULL);
	}

CUnicode CCodedText::GetText(PCUTF pDefault, PDWORD pParam)
{
	CLangManager *pLang = CCommsSystem::m_pThis->m_pLang;

	UINT          uLang = pLang->GetCurrentSlot();

	return GetText(uLang, pDefault, pParam);
	}

CUnicode CCodedText::GetText(UINT uLang, PCUTF pDefault, PDWORD pParam)
{
	if( ThisNotNull(this) ) {

		if( IsAvail() ) {

			DWORD Data = Execute(typeString, pParam);

			if( Data ) {

				PUTF p = PUTF(Data);

				if( !IsConst() ) {

					CUnicode t = p;

					Free(p);

					return t;
					}

				if( uLang ) {

					UINT f = NOTHING;

					UINT i = 0;

					for(;;) {

						for(;;) {

							if( p[i] == 0x00 ) {

								break;
								}

							if( p[i] == cSep ) {

								if( f == NOTHING ) {

									f = i;
									}

								break;
								}

							i++;
							}

						if( p[i++] ) {

							if( !--uLang ) {

								if( p[i] && p[i] != cSep ) {

									PUTF s = wstrchr(p + i, cSep);

									if( s ) {

										*s = 0;
										}

									CUnicode t = p + i;

									Free(p);

									return t;
									}

								break;
								}

							continue;
							}
						
						break;
						}

					if( f < NOTHING ) {

						p[f] = 0;
						}

					CUnicode t = p;

					Free(p);

					return t;
					}
				else {
					PUTF s = wstrchr(p, cSep);

					if( s ) {

						*s = 0;
						}

					CUnicode t = p;

					Free(p);

					return t;
					}
				}
			}
		}

	return pDefault;
	}

// End of File
