@echo off

if exist %2..\..\Snapshot\copy.me goto found

exit 0

:found

if #%1# == #clean#   goto done

if #%1# == #build#   set _options=/C /D

if #%1# == #rebuild# set _options=/C

mkdir %2..\..\Include 2> nul
mkdir %2..\..\Lib\%4  2> nul
mkdir %2..\..\Bin\%4  2> nul

xcopy %_options% /Y %2..\..\Snapshot\Include      %2..\..\Include
xcopy %_options% /Y %2..\..\Snapshot\Lib\%4       %2..\..\Lib\%4
xcopy %_options% /Y %2..\..\Snapshot\Bin\%4\*.dll %2..\..\Bin\%4
xcopy %_options% /Y %2..\..\Snapshot\Bin\%4\*.exe %2..\..\Bin\%4
xcopy %_options% /Y %2..\..\Snapshot\Bin\%4\*.*   %3

set _options=

:done
