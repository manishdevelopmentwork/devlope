
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ModemHspa2_HPP

#define	INCLUDE_ModemHspa2_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ModemGprs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Celluar HSPA+ Connection v2
//

class CModemCellHspa2 : public CModemCellGprs
{
	public:
		// Constructor
		CModemCellHspa2(CPpp *pPpp, CConfigPpp const &Config);

		// Destructor
		~CModemCellHspa2(void);

		// Overridables
		BOOL Init(void);
		BOOL Kick(void);
		BOOL Idle(void);
		BOOL Poll(void);
		BOOL Connect(BOOL &fOkay);
		BOOL HangUp(void);

	protected:
		// Implementation
		BOOL DropLink(void);
		BOOL FactoryReset(void);
		BOOL PulseDTR(void);
		UINT SendAttention(void);
		void SendLinkTerminate(void);
	};

// End of File

#endif
