
#include "intern.hpp"

#include "wpmii.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CStiebelWpmIISerialDriver
//

// Instantiator

ICommsDriver * Create_StiebelWpmIISerialDriver(void)
{
	return New CStiebelWpmIISerialDriver;
	}

// Constructor

CStiebelWpmIISerialDriver::CStiebelWpmIISerialDriver(void)
{
	m_wID		= 0x408D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Stiebel Eltron";
	
	m_DriverName	= "WPMII";
	
	m_Version	= "1.00";
	
	m_ShortName	= "WPMII";

	AddSpaces();
	}

// Destructor

CStiebelWpmIISerialDriver::~CStiebelWpmIISerialDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CStiebelWpmIISerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CStiebelWpmIISerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CStiebelWpmIISerialDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Implementation

void CStiebelWpmIISerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(	    1,  "MNT",	  "Minute",		10,	   1,  20, addrWordAsWord));
	AddSpace(New CSpace(	    2,  "STU",	  "Stunde",		10,	   1,  20, addrWordAsWord)); // Hour
	AddSpace(New CSpace(	    3,  "TAG",	  "Tag",		10,	   1,  20, addrWordAsWord)); // Day
	AddSpace(New CSpace(	    4,  "MON",	  "Monat",		10,	   1,  20, addrWordAsWord)); // Month
	AddSpace(New CSpace(	    5,  "JAH",	  "Jahr",		10,	   1,  20, addrWordAsWord)); // Year
	AddSpace(New CSpace(	    6,  "IWS",	  "IWS",		10,	   1,  20, addrWordAsWord));
	AddSpace(New CSpace(	    7,  "FEH",	  "Fehlernummer",	10,	   1,  20, addrWordAsWord)); // Error Number

	
	AddSpace(New CSpace(addrNamed,  "PRO",	  "Programmschalter",				10,	 274,	0, addrWordAsWord)); // Program Switch
	AddSpace(New CSpace(addrNamed,  "AUS",	  "Aussentemperatur",				10,	  12,	0, addrWordAsWord)); // Outside Temperature
	AddSpace(New CSpace(addrNamed,  "WRI",	  "Warmwasseristtemperatur",			10,	  14,	0, addrWordAsWord)); // DHW Temperature
	AddSpace(New CSpace(addrNamed,  "WSI",	  "Warmwassersolltemperatut",			10,	   3,	0, addrWordAsWord)); // Set DHW Temperature
	AddSpace(New CSpace(addrNamed,  "MRI",	  "Mischer-Isttemperatur",			10,	  15,	0, addrWordAsWord)); // Mixer Temperature
	AddSpace(New CSpace(addrNamed,  "MRS",	  "Mischer-Solltemperatur",			10,	   4,	0, addrWordAsWord)); // Set Mixer Temperature
	AddSpace(New CSpace(addrNamed,  "RI1",	  "Raum-Isttemperatur 1",			10,	  17,	0, addrWordAsWord)); // Room Temperature 1
	AddSpace(New CSpace(addrNamed,  "RI2",	  "Raum-Isttemperatur 2",			10,( 17| M5),	0, addrWordAsWord)); // Room Temperature 2
	AddSpace(New CSpace(addrNamed,  "RS1",	  "Raumsolltemperatur 1 FE7",			10,	  18,	0, addrWordAsWord)); // Room Temperature 1 FE7
	AddSpace(New CSpace(addrNamed,  "RS2",	  "Raumsolltemperatur 2 FE7",			10,( 18| M5),	0, addrWordAsWord)); // Room Temperature 2 FE7
       	
	AddSpace(New CSpace(addrNamed,  "RFI",	  "Rücklaufisttemperatur",			10,	  22,	0, addrWordAsWord)); // Return Temperature
	AddSpace(New CSpace(addrNamed,  "RFS",	  "Rücklaufsolltemperatur",			10,	 471,	0, addrWordAsWord)); // Set Return Temperature
	AddSpace(New CSpace(addrNamed,  "FES",	  "Festwerttemperatur",				10,	 448,	0, addrWordAsWord)); // Set Fixed Temperature
	AddSpace(New CSpace(addrNamed,  "PUF",	  "Puffersolltemperatur",			10,	 469,	0, addrWordAsWord)); // Set Buffer Temperature
	AddSpace(New CSpace(addrNamed,  "VOR",	  "Vorlaufisttemperatur WP",			10,	 470,	0, addrWordAsWord)); // Flow Temperature
	AddSpace(New CSpace(addrNamed,  "MAX",	  "max. WP-Vorlauftemp. bei Heizbetrieb",	10,	 488,	0, addrWordAsWord)); // Set Flow Temperature HTG
	AddSpace(New CSpace(addrNamed,  "QUE",	  "Quellenisttemperatur",			10,	 468,	0, addrWordAsWord)); // Source Temperature
	AddSpace(New CSpace(addrNamed,  "MIN",	  "minimale Quellentemperatur",			10,	 432,	0, addrWordAsWord)); // Min. Source Temperature

	AddSpace(New CSpace(addrNamed,  "STN",	  "Statusanzeige (Agregatzustände) - normal",	10,	 374,	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "STA",	  "Statusanzeige (Agregatzustände)",		10,	 412,	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "HE1",	  "Heizkurve 1",				10,	 270,	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "HE2",	  "Heizkurve 2",				10,(270| M5),	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "WAT",	  "Warmwassersolltemperatur Tag",		10,	  19,	0, addrWordAsWord)); // Set Day DHW Temperature
	AddSpace(New CSpace(addrNamed,  "WAN",	  "Warmwassersolltemperatur Nacht",		10,	2566,	0, addrWordAsWord)); // Set Night DHW Temperature
	AddSpace(New CSpace(addrNamed,  "R1T",	  "Raumsolltemperatur 1 (Tagwert)",		10,	   5,	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "R2T",	  "Raumsolltemperatur 2 (Tagwert)",		10,(  5| M5),	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "R1N",	  "Raumsolltemperatur 1 (Nachtwert)",		10,	   8,	0, addrWordAsWord));
   	AddSpace(New CSpace(addrNamed,  "R2N",	  "Raumsolltemperatur 2 (Nachtwert)",		10,(  8| M5),	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "RST",	  "RESET WP",					10,	 251,	0, addrWordAsWord));
	AddSpace(New CSpace(addrNamed,  "TFE",	  "temporäre Fehler",				10,	1640,	0, addrWordAsWord)); // Temporary Error
	AddSpace(New CSpace(addrNamed,  "SFE",	  "statische Fehler",				10,	1641,	0, addrWordAsWord)); // Static Error
	
	}

// End of File