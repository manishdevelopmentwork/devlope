
#include "intern.hpp"

#include "squared.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader(); 

//////////////////////////////////////////////////////////////////////////
//
// SquareD Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSquareDDriverOptions, CUIItem);

// Constructor

CSquareDDriverOptions::CSquareDDriverOptions(void)
{
	m_Source = 0;
	}

// Download Support

BOOL CSquareDDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(LOBYTE(m_Source));

	return TRUE;
	}

// Meta Data Creation

void CSquareDDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);
	}


//////////////////////////////////////////////////////////////////////////
//
// SquareD Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CSquareDDeviceOptions, CUIItem);

// Constructor

CSquareDDeviceOptions::CSquareDDeviceOptions(void)
{
	m_Drop = 0;
	}

// UI Management

void CSquareDDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CSquareDDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CSquareDDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// SquareD Master
//

// Instantiator

ICommsDriver *	Create_SquareDDriver(void)
{
	return New CSquareDDriver;
	}

// Constructor

CSquareDDriver::CSquareDDriver(void)
{
	m_wID		= 0x3305;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SquareD";
	
	m_DriverName	= "SYMAX";
	
	m_Version	= "1.10";
	
	m_ShortName	= "SquareD SYMAX";

	AddSpaces();
	}

// Binding Control

UINT CSquareDDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSquareDDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CSquareDDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSquareDDriverOptions);
	}


CLASS CSquareDDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CSquareDDeviceOptions);
	}

BOOL CSquareDDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		if( Addr.a.m_Table != 3 || Addr.a.m_Offset & 1 ) return TRUE;

		CString s;

		UINT u = Addr.a.m_Offset / 10;

		s.Printf("%d1, %d3, %d5, %d7, %d9", u, u, u, u, u);
	
		Error.Set( s,
			   0
			   );
		}
	
	return FALSE;
	}

// Implementation

void CSquareDDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "S", "Word Data Registers",	10, 1, 9999, addrWordAsWord));
	AddSpace(New CSpace(2, "L", "Long Data Registers",	10, 1, 9999, addrLongAsLong));
	AddSpace(New CSpace(3, "R", "Real Data Registers",	10, 1, 9999, addrRealAsReal));
	}

// End of File
