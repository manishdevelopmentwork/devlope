
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Wraper
//

// Constructors

COleResult::COleResult(void)
{
	m_h = S_OK;
	}

COleResult::COleResult(COleResult const &That)
{
	m_h = That.m_h;
	}

COleResult::COleResult(HRESULT h)
{
	m_h = h;
	}

// Assignment Operators

COleResult const & COleResult::operator = (COleResult const &That)
{
	m_h = That.m_h;

	return *this;
	}

COleResult const & COleResult::operator = (HRESULT h)
{
	m_h = h;

	return *this;
	}

// Conversions

COleResult::operator HRESULT (void) const
{
	return m_h;
	}

// Attributes

BOOL COleResult::Succeeded(void) const
{
	return SUCCEEDED(m_h);
	}

BOOL COleResult::Failed(void) const
{
	return FAILED(m_h);
	}

DWORD COleResult::GetSeverity(void) const
{
	return HRESULT_SEVERITY(m_h);
	}

DWORD COleResult::GetFacility(void) const
{
	return HRESULT_FACILITY(m_h);
	}

CString COleResult::GetName(void) const
{
	return DescribeName();
	}

// Description

CString COleResult::Describe(void) const
{
	CString Text;

	Text.Printf( L"Severity is %s. Facility is %s. Code is %s.",
		     DescribeSeverity(),
		     DescribeFacility(),
		     DescribeName()
		     );

	return Text;
	}

CString COleResult::DescribeSeverity(void) const
{
	switch( GetSeverity() ) {
	
		case SEVERITY_SUCCESS:
			return L"Success";

		case SEVERITY_ERROR:
			return L"Error";
		}

	return L"Unknown";
	}

CString COleResult::DescribeFacility(void) const
{
	switch( GetFacility() ) {
	
		case FACILITY_CONTROL:
			return L"Control";

		case FACILITY_DISPATCH:
			return L"Dispatch";
		
		case FACILITY_NULL:
			return L"General";
		
		case FACILITY_ITF:
			return L"Interface";
		
		case FACILITY_RPC:
			return L"RPC";
		
		case FACILITY_STORAGE:
			return L"Storage";
		
		case FACILITY_WINDOWS:
			return L"Windows";
		
		case FACILITY_WIN32:
			return L"Win32";
		
		case FACILITY_SSPI:
			return L"SSPI";
		
		case FACILITY_SETUPAPI:
			return L"Setup API";
		
		case FACILITY_MSMQ:
			return L"Message Queue";
		
		case FACILITY_CERT:
			return L"Certification";
		}

	return L"Unknown";
	}

CString COleResult::DescribeName(void) const
{
	if( !m_Maps.IsEmpty() ) {

		CString Text;

		BOOL fMatch = FALSE;

		int  nCount = m_Maps.GetCount();

		for( int n = 0; n < nCount; n++ ) {

			COleResultMap *pMap = m_Maps[n];

			INDEX i = pMap->FindName(m_h);

			if( !pMap->Failed(i) ) {

				CString const &Name = pMap->GetData(i);

				if( fMatch ) {

					Text += L" or ";
					}

				Text   += Name;

				fMatch  = TRUE;
				}
			}

		if( fMatch )
			return Text;
		}

	return CPrintf(L"%8.8lX", m_h);
	}

// Code Registration

void COleResult::InsertCode(HRESULT h, PCTXT p)
{
	int nCount = m_Maps.GetCount();

	for( int n = 0; n <= nCount; n++ ) {

		if( n == nCount ) {

			COleResultMap *pMap = New COleResultMap;

			pMap->Insert(h, p);

			m_Maps.Append(pMap);

			break;
			}

		if( m_Maps[n]->Insert(h, p) )
			break;
		}
	}

void COleResult::DeleteCode(HRESULT h)
{
	int nCount = m_Maps.GetCount();

	for( int n = 0; n < nCount; n++ ) {

		COleResultMap *pMap = m_Maps[n];

		pMap->Remove(h);

		if( pMap->IsEmpty() ) {

			delete pMap;

			m_Maps.Remove(n--);

			nCount--;
			}
		}

	if( !nCount )
		m_Maps.Empty();
	}

//////////////////////////////////////////////////////////////////////////
//
// OLE Result Code Lookup
//

// Constructor

COleLookup::COleLookup(HRESULT h, PCTXT p) : m_h(h)
{
	COleResult::InsertCode(h, p);
	}

// Destructor

COleLookup::~COleLookup(void)
{
	COleResult::DeleteCode(m_h);
	}

//////////////////////////////////////////////////////////////////////////
//
// Object with OLE Result
//

// Constructor

CHasResult::CHasResult(void)
{
	}

// Status Check

BOOL CHasResult::IsOkay(void) const
{
	return m_Result.Succeeded();
	}

// Result Access

COleResult const & CHasResult::GetResult(void) const
{
	return m_Result;
	}

// Implementation

BOOL CHasResult::Check(HRESULT h) const
{
	m_Result = h;

	if( m_Result.Failed() ) {

		AfxTrace(L"RESULT: %s\n", m_Result.DescribeName());

		return FALSE;
		}

	return TRUE;
	}

void CHasResult::CheckAndThrow(HRESULT h) const
{
	m_Result = h;
	
	if( IsOkay() ) return;

	ThrowResult();
	}

void CHasResult::ThrowResult(void) const
{
	AfxThrowOleException(m_Result);
	}

// End of File
