
#include "intern.hpp"

#include "DAMix2DigitalInputConfig.hpp"

#include "DAMix2DigitalInputConfigWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Input Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2DigitalInputConfig, CCommsItem);

// Property List

CCommsList const CDAMix2DigitalInputConfig::m_CommsList[] = {

	{ 0, "Mode1",      PROPID_MODE1,      usageWriteInit,  IDS_NAME_CT1	},
	{ 0, "Mode2",      PROPID_MODE2,      usageWriteInit,  IDS_NAME_CT2	},
	{ 0, "Mode3",      PROPID_MODE3,      usageWriteInit,  IDS_NAME_CT3	},
	{ 0, "Mode4",      PROPID_MODE4,      usageWriteInit,  IDS_NAME_CT4	},
	{ 0, "Mode5",      PROPID_MODE5,      usageWriteInit,  IDS_NAME_CT5	},
	{ 0, "Mode6",      PROPID_MODE6,      usageWriteInit,  IDS_NAME_CT6	},
	{ 0, "Mode7",      PROPID_MODE7,      usageWriteInit,  IDS_NAME_CT7	},
	{ 0, "Mode8",      PROPID_MODE8,      usageWriteInit,  IDS_NAME_CT8	},

	{ 1, "Pull1",	PROPID_PULLMODE1,     usageWriteInit,  IDS_NAME_PULL1	},
	{ 1, "Pull2",	PROPID_PULLMODE2,     usageWriteInit,  IDS_NAME_PULL2	},
	{ 1, "Pull3",	PROPID_PULLMODE3,     usageWriteInit,  IDS_NAME_PULL3	},
	{ 1, "Pull4",	PROPID_PULLMODE4,     usageWriteInit,  IDS_NAME_PULL4	},
	{ 1, "Pull5",	PROPID_PULLMODE5,     usageWriteInit,  IDS_NAME_PULL5	},
	{ 1, "Pull6",	PROPID_PULLMODE6,     usageWriteInit,  IDS_NAME_PULL6	},
	{ 1, "Pull7",	PROPID_PULLMODE7,     usageWriteInit,  IDS_NAME_PULL7	},
	{ 1, "Pull8",	PROPID_PULLMODE8,     usageWriteInit,  IDS_NAME_PULL8	},

};

// Constructor

CDAMix2DigitalInputConfig::CDAMix2DigitalInputConfig(void)
{
	m_Mode1      = 0;
	m_Mode2      = 0;
	m_Mode3      = 0;
	m_Mode4      = 0;
	m_Mode5      = 0;
	m_Mode6      = 0;
	m_Mode7      = 0;
	m_Mode8      = 0;

	m_Pull1      = 0;
	m_Pull2      = 0;
	m_Pull3      = 0;
	m_Pull4      = 0;
	m_Pull5      = 0;
	m_Pull6      = 0;
	m_Pull7      = 0;
	m_Pull8      = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// View Pages

UINT CDAMix2DigitalInputConfig::GetPageCount(void)
{
	return 1;
}

CString CDAMix2DigitalInputConfig::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(L"Digital Inputs");
	}

	return L"";
}

CViewWnd * CDAMix2DigitalInputConfig::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDAMix2DigitalInputConfigWnd;
	}

	return NULL;
}

// Conversion

BOOL CDAMix2DigitalInputConfig::Convert(CPropValue *pValue)
{
	if( pValue ) {

		for( UINT n = 0; n < 8; n++ ) {

			if( ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"InputMode%d", n+1)) ) {

				// Legacy

				ConvertMode(n);

				continue;
			}

			if( ImportNumber(pValue, CPrintf(L"Mode%d", n+1), CPrintf(L"InpMode%d", n+1)) ) {

				// Graphite

				continue;
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Meta Data Creation

void CDAMix2DigitalInputConfig::AddMetaData(void)
{
	Meta_AddInteger(Mode1);
	Meta_AddInteger(Mode2);
	Meta_AddInteger(Mode3);
	Meta_AddInteger(Mode4);
	Meta_AddInteger(Mode5);
	Meta_AddInteger(Mode6);
	Meta_AddInteger(Mode7);
	Meta_AddInteger(Mode8);

	Meta_AddInteger(Pull1);
	Meta_AddInteger(Pull2);
	Meta_AddInteger(Pull3);
	Meta_AddInteger(Pull4);
	Meta_AddInteger(Pull5);
	Meta_AddInteger(Pull6);
	Meta_AddInteger(Pull7);
	Meta_AddInteger(Pull8);

	CCommsItem::AddMetaData();
}

// Implementation

void CDAMix2DigitalInputConfig::ConvertMode(UINT uIndex)
{
	CMetaList       *pList = FindMetaList();

	CMetaData const *pData = pList->FindData(CPrintf(L"Mode%u", uIndex+1));

	UINT             uType = pData->GetType();

	if( uType == metaInteger ) {

		UINT uPrev   = pData->ReadInteger(this);

		UINT uData[] = { 2, 1, 0 };

		pData->WriteInteger(this, uData[uPrev]);

		return;
	}

	AfxAssert(FALSE);
}

// End of File
