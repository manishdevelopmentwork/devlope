
//////////////////////////////////////////////////////////////////////////
//
// KingBus Constants
//
//

#define	SPACE_S	 0x01
#define	SPACE_X	 0x02
#define SPACE_L	 0x03
#define SPACE_U	 0x04
#define SPACE_UW 0x05
#define KING_MAX 256

//////////////////////////////////////////////////////////////////////////
//
// KingBus ASCII Communications Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

class CKingBusASCIIDriver : public CMasterDriver
{
	public:
		// Constructor
		CKingBusASCIIDriver(void);

		// Destructor
		~CKingBusASCIIDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		struct CPollAddr
		{
			BYTE		m_bPoll;
			UINT		m_uSpaceS;
			UINT		m_uSpaceL;
			UINT		m_uSpaceX;
			UINT		m_uSpaceU;
			UINT		m_uTime;
			BYTE		m_bRetry;
		};

		// Members
		BYTE		m_bRx[32];
		BYTE		m_bTx[32];
		UINT		m_uPtr;
		WORD		m_wCheck;
		UINT		m_uTimeout;
		LPCTXT		m_pHex;
		CPollAddr *	m_pPoll;
		CPollAddr **	m_pList;
		UINT		m_uPollMin;
		UINT		m_uPollMax;
		UINT		m_uTimeMin;
		UINT		m_uTimeMax;
		BOOL		m_fInit;

		// Implementation
		UINT  GetSize(UINT uTable);
		UINT  GetOffset(UINT uTable);
		BOOL  IsMarker(UINT uPos);
		void  ResetRetry(void);
		BOOL  Retry(void);
		void  Register(UINT uPoll);
		BOOL  DoPoll(void);
		void  SaveData(PDWORD pData);
		void  GetSavedData(UINT uTable, UINT uType, PDWORD pData);

		// Frame Building
		void  Start(void);
		void  AddByte(BYTE bByte);
		void  AddSpace(void);
		void  AddTerm(void);
		void  AddPollingAddress(UINT uAddr);
		void  AddLong(UINT uTable, PDWORD pData);
		void  AddReal(UINT uTable, PDWORD pData);
		void  AddWrite(UINT uTable, UINT uType, PDWORD pData);

		// Transport Layer
		BOOL  Transact(void);
		BOOL  Tx(void);
		BOOL  Rx(void);
		BOOL  CheckFrame(void);

		// Helpers
		BOOL  IsASCII(BYTE bData);
		BYTE  FromASCII(BYTE bData);
		void  GetRead(UINT uTable, UINT uType, PDWORD pData);
		DWORD GetReal(PBYTE pData, UINT uSize);
		DWORD GetLong(PBYTE pData, UINT uSize, UINT uTable);
		DWORD GetString(PBYTE pData, UINT uSize);
		UINT  GetReadTable(UINT uTable);
		UINT  GetDecimalPosition(UINT uTable);
};

// End of File
