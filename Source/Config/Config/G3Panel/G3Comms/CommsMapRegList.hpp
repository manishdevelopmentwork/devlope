
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMapRegList_HPP

#define INCLUDE_CommsMapRegList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsMapBlock;
class CCommsMapReg;

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping Register List
//

class DLLNOT CCommsMapRegList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsMapRegList(void);

		// Item Location
		CCommsMapBlock * GetParentBlock(void) const;
		CCommsDevice   * GetParentDevice(void) const;

		// Item Access
		CCommsMapReg * GetItem(INDEX Index) const;
		CCommsMapReg * GetItem(UINT   uPos) const;

		// Operations
		BOOL Validate(BOOL fExpand);
		void SetCount(UINT uCount);
	};

// End of File

#endif
