
#include "Intern.hpp"

#include "UsbCdlExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Expansion System
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb CDL Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbCdlExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbCdlExpansion(pDriver);

	return p;
	}

// Constructor

CUsbCdlExpansion::CUsbCdlExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbCdlExpansion::GetName(void)
{
	return "GMCDL";
	}

UINT METHOD CUsbCdlExpansion::GetClass(void)
{
	return rackClassComms;
	}

UINT METHOD CUsbCdlExpansion::GetPower(void)
{
	return 12; // TODO
	}

BOOL METHOD CUsbCdlExpansion::HasBootLoader(void)
{
	return TRUE;
	}

IDevice * METHOD CUsbCdlExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbCdl(pDriver);
	}

// IExpansionSerial

UINT METHOD CUsbCdlExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD METHOD CUsbCdlExpansion::GetPortMask(void)
{
	return Bit(physicalRS485) | Bit(physicalRS422Master) | Bit(physicalRS422Slave);
	}

UINT METHOD CUsbCdlExpansion::GetPortType(UINT iPort)
{
	return rackCatLink;
	}

// End of File
