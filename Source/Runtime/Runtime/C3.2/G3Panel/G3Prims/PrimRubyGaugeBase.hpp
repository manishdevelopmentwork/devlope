
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeBase_HPP
	
#define	INCLUDE_PrimRubyGaugeBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRuby.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Base Gauge Primitive
//

class CPrimRubyGaugeBase : public CPrimRuby
{
	public:
		// Constructor
		CPrimRubyGaugeBase(void);

		// Destructor
		~CPrimRubyGaugeBase(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);

		// Drawing
		void TestScaleEtc(void);
		void DrawScaleEtc(IGDI *pGDI);

		// Data Members
		CCodedItem * m_pValue;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimColor * m_pMajorColor;
		CPrimColor * m_pMinorColor;
		UINT	     m_Major;
		UINT	     m_Minor;
		UINT	     m_PointMode;
		CPrimColor * m_pPointColor;
		UINT	     m_BandShow1;
		CPrimColor * m_pBandColor1;
		CCodedItem * m_pBandMin1;
		CCodedItem * m_pBandMax1;
		UINT	     m_BandShow2;
		CPrimColor * m_pBandColor2;
		CCodedItem * m_pBandMin2;
		CCodedItem * m_pBandMax2;
		UINT	     m_BugShow1;
		CPrimColor * m_pBugColor1;
		CCodedItem * m_pBugValue1;
		UINT	     m_BugShow2;
		CPrimColor * m_pBugColor2;
		CCodedItem * m_pBugValue2;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			number m_Min;
			number m_Max;
			number m_BandMin1;
			number m_BandMax1;
			number m_BandMin2;
			number m_BandMax2;
			number m_BugValue1;
			number m_BugValue2;
			COLOR  m_MajorColor;
			COLOR  m_MinorColor;
			COLOR  m_BandColor1;
			COLOR  m_BandColor2;
			COLOR  m_BugColor1;
			COLOR  m_BugColor2;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;
		UINT m_uChange;

		// Data Members
		CRubyDrawPlus m_d;
		CRubyMatrix   m_m1;
		CRubyMatrix   m_m2;
		number	      m_scale;
		CRubyPoint    m_pointCenter;
		CRubyPath     m_pathMajor;
		CRubyPath     m_pathMinor;
		CRubyPath     m_pathBand1;
		CRubyPath     m_pathBand2;
		CRubyPath     m_pathBug1;
		CRubyPath     m_pathBug2;
		CRubyGdiList  m_listMajor;
		CRubyGdiList  m_listMinor;
		CRubyGdiList  m_listBand1;
		CRubyGdiList  m_listBand2;
		CRubyGdiList  m_listBug1;
		CRubyGdiList  m_listBug2;

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);

		// Scale Building
		virtual void MakeScaleEtc(void);

		// Scaling
		number GetValue(CCodedItem *pValue, C3REAL Default);

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Shading
		static BOOL  ShaderDark  (IGdi *pGdi, int p, int c);
		static BOOL  ShaderChrome(IGdi *pGdi, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);
	};

// End of File

#endif
