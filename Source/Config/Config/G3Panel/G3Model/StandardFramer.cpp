
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Operator Panel Framer
//

class CFramer : public IFramer
{
	public:
		// Constructor
		CFramer(void);

		// Management
		void Release(void);

		// Attributes
		int    GetTotalKeys(void) const;
		CRect  GetFrameRect(void) const;
		CSize  GetTotalSize(void) const;
		HRGN   GetClipRegion(int nScale) const;
		CRect  GetKeyRect(int nKey) const;
		UINT   GetKeyCode(int nKey) const;
		CColor GetKeyColor(UINT uCode) const;

		// Operations
		void FindLayout(CSize Size, int nKeys);
		void DrawFrame(CDC &DC, BOOL fWarn, BOOL fSim);
		void DrawKeyBorder(CDC &DC, CRect Rect);
		void DrawKeyText(CDC &DC, CRect Rect, CString Text);
		void DrawKeyIcon(CDC &DC, CRect Rect, UINT uCode);

	protected:
		// Coloring
		CBrush m_Panel;
		CBrush m_Middle;
		CBrush m_Inner;
		CBrush m_Strip;
		CPen   m_Border;

		// Layout Data
		int     m_nKeys;
		int	m_nScale;
		int	m_nPad;
		CSize	m_DispSize;
		CRect	m_FrameRect;
		CSize   m_FrameSize;
		CSize   m_TotalSize;
		CFont   m_FontKeys;
		CFont   m_FontLogo;
		CFont   m_FontFunc;
		CFont   m_FontText;
		CFont	m_FontWarn;
		CRect   m_FramePanel;
		CRect   m_FrameMiddle;
		CRect   m_FrameInner;
		CRect	m_FrameStrip;
		CRect	m_FrameKeys;
		CPoint	m_FrameLogo;

		// Implementation
		void MakeTools(void);
		void FindLayoutKeys(void);
		void FindLayoutNone(void);
		void FindScale(void);
		int  FrameScale(int n) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Operator Panel Framer
//

// Instantiator

IFramer * Create_StdFramer(void)
{
	return New CFramer;
	}

// Constructor

CFramer::CFramer(void)
{
	MakeTools();
	}

// Management

void CFramer::Release(void)
{
	delete this;
	}

// Attributes

int CFramer::GetTotalKeys(void) const
{
	return m_nKeys;
	}

CRect CFramer::GetFrameRect(void) const
{
	return m_FrameRect;
	}

CSize CFramer::GetTotalSize(void) const
{
	return m_TotalSize;
	}

HRGN CFramer::GetClipRegion(int nScale) const
{
	return CreateRoundRectRgn( 0+MulDiv(nScale, m_FramePanel.left  , 100),
				   0+MulDiv(nScale, m_FramePanel.top   , 100),
				   1+MulDiv(nScale, m_FramePanel.right , 100),
				   1+MulDiv(nScale, m_FramePanel.bottom, 100),
				   0+MulDiv(nScale, FrameScale(3)      , 100),
				   0+MulDiv(nScale, FrameScale(3)      , 100)
				   );
	}

CRect CFramer::GetKeyRect(int nKey) const
{
	CRect Key;

	if( nKey == m_nKeys - 1 ) {

		Key.left   = m_FrameKeys.left;

		Key.right  = m_FrameKeys.right;

		Key.top    = m_FrameKeys.bottom + FrameScale(4);

		Key.bottom = Key.top		+ Key.cx();
		}

	if( nKey <= m_nKeys - 2 ) {

		Key.left   = m_FrameKeys.left;

		Key.right  = m_FrameKeys.right;

		Key.top    = m_FrameKeys.top + ((m_FrameKeys.cy() - Key.cx()) * nKey / (m_nKeys - 2));

		Key.bottom = Key.top         + Key.cx();
		}

	return Key;
	}

UINT CFramer::GetKeyCode(int nKey) const
{
	if( nKey == m_nKeys - 1 ) {

		return COPS_VK_MENU;
		}

	if( nKey <= m_nKeys - 2 ) {

		return COPS_VK_SOFT1 + nKey;
		}

	return 0;
	}

CColor CFramer::GetKeyColor(UINT uCode) const
{
	return afxColor(BLACK);
	}

// Operations

void CFramer::FindLayout(CSize Size, int nKeys)
{
	m_DispSize  = Size;

	m_nKeys     = nKeys ? nKeys + 1 : 0;

	FindScale();

	m_FrameRect = CRect( FrameScale(m_nKeys ? 10 : 6) + m_nPad,
			     FrameScale(m_nKeys ?  8 : 7) + m_nPad,
			     FrameScale(m_nKeys ?  3 : 6) + m_nPad,
			     FrameScale(m_nKeys ? 10 : 7) + m_nPad
			     );

	m_FrameSize.cx = m_FrameRect.left + m_FrameRect.right;
	
	m_FrameSize.cy = m_FrameRect.top  + m_FrameRect.bottom;

	m_TotalSize    = m_DispSize + m_FrameSize;

	if( m_nKeys ) {

		FindLayoutKeys();
		}
	else
		FindLayoutNone();

	UINT n = 6 * CString(IDS_NOTE_EDITING_IS).GetLength() / 11;

	MakeMax(n, 30);

	m_FontKeys.Create(L"rlc31",   CSize(0, FrameScale(4)      ), FALSE);

	m_FontText.Create(L"Arial",   CSize(0, FrameScale(5)  /  4), FALSE);

	m_FontFunc.Create(L"Arial",   CSize(0, FrameScale(5)  /  2),  TRUE);

	m_FontLogo.Create(L"rlc31",   CSize(0, FrameScale(4)      ), FALSE);

	m_FontWarn.Create(afxDlgFont, CSize(0, m_TotalSize.cy /  n), FALSE);
	}

void CFramer::DrawFrame(CDC &DC, BOOL fWarn, BOOL fSim)
{
	DC.Select(m_Border);

	DC.Select(m_Panel);

	DC.RoundRect(m_FramePanel,  FrameScale(3));

	DC.Deselect();

	DC.Deselect();

	////////

	DC.Select(CGdiObject(NULL_PEN));

	DC.Select(m_Middle);

	DC.RoundRect(m_FrameMiddle, FrameScale(2));

	DC.Replace(m_Inner);

	DC.RoundRect(m_FrameInner,  FrameScale(2));

	DC.Replace(m_Strip);

	DC.RoundRect(m_FrameStrip,  FrameScale(2));

	DC.Deselect();

	DC.Deselect();

	if( fWarn ) {

		DC.Select(m_FontWarn);

		DC.SetBkMode(TRANSPARENT);

		DC.SetTextColor(afxColor(WHITE));

		CString Text = IDS_NOTE_EDITING_IS;

		CRect   Rect = m_FramePanel;

		if( m_nKeys ) {

			Rect.top = Rect.bottom - FrameScale(10);
			}
		else 
			Rect.bottom = Rect.top + FrameScale(6);

		CSize   Size = DC.GetTextExtent(Text);

		CPoint  Pos  = Rect.GetTopLeft() + (Rect.GetSize() - Size) / 2;

		DC.TextOut(Pos, Text);

		DC.Deselect();
		}

	if( C3OemFeature(L"RedLion", FALSE) ) {

		DC.Select(m_FontLogo);

		DC.SetBkMode(TRANSPARENT);

		DC.SetTextColor(afxColor(WHITE));

		DC.TextOut(m_FrameLogo, L"\x049");

		DC.SetTextColor(afxColor(RED));

		DC.TextOut(m_FrameLogo, L"\x069");

		DC.Deselect();
		}
	}

void CFramer::DrawKeyBorder(CDC &DC, CRect Rect)
{
	DC.RoundRect(Rect, FrameScale(3) / 2);
	}

void CFramer::DrawKeyText(CDC &DC, CRect Rect, CString Text)
{
	if( Text.GetLength() == 2 ) {

		DC.Select(m_FontFunc);
		}
	else
		DC.Select(m_FontText);

	DC.DrawText(Text, Rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	DC.Deselect();
	}

void CFramer::DrawKeyIcon(CDC &DC, CRect Rect, UINT uCode)
{
	DC.Select(m_FontKeys);

	DC.DrawText(L"m", Rect, DT_SINGLELINE | DT_CENTER | DT_VCENTER);

	DC.Deselect();
	}

// Implementation

void CFramer::MakeTools(void)
{
	CColor Panel = C3OemGetColor(L"PanelOuter", CColor(0,0,0));

	m_Panel .Create(Panel);

	m_Middle.Create(C3OemGetColor(L"PanelMiddle", CColor(128,128,128)));
	
	m_Inner .Create(C3OemGetColor(L"PanelInner",  CColor(128,128,128)));

	m_Strip .Create(C3OemGetColor(L"PanelStrip",  CColor(128,128,128)));

	m_Border.Create(0, 2, Panel ? afxColor(BLACK) : afxColor(WHITE));
	}

void CFramer::FindLayoutKeys(void)
{
	m_FramePanel  = CRect(m_TotalSize) - m_nPad;

	m_FrameMiddle = m_FramePanel;

	m_FrameMiddle.left   += FrameScale(1);

	m_FrameMiddle.right  -= FrameScale(1);

	m_FrameMiddle.top    += FrameScale(6);

	m_FrameMiddle.bottom -= FrameScale(8);

	m_FrameInner = m_FrameMiddle;

	m_FrameInner.left   += FrameScale(6);

	m_FrameInner.right  -= FrameScale(1);

	m_FrameInner.top    += FrameScale(1);

	m_FrameInner.bottom -= FrameScale(1);

	m_FrameStrip = m_FrameMiddle;

	m_FrameStrip.left   += FrameScale(1);

	m_FrameStrip.right   = m_FrameStrip.left + FrameScale(6);

	m_FrameStrip.top    += FrameScale(2);

	m_FrameStrip.bottom -= FrameScale(2);

	m_FrameKeys = m_FrameStrip;

	int xSize   = FrameScale(4);

	m_FrameKeys.left   += FrameScale(1);

	m_FrameKeys.right   = m_FrameKeys.left + xSize;

	m_FrameKeys.top    += FrameScale(1);

	m_FrameKeys.bottom -= FrameScale(1);

	m_FrameLogo = m_FramePanel.GetTopLeft();

	m_FrameLogo.x += FrameScale(3);

	m_FrameLogo.y += FrameScale(1);
	}

void CFramer::FindLayoutNone(void)
{
	m_FramePanel  = CRect(m_TotalSize) - m_nPad;

	m_FrameMiddle = m_FramePanel;

	m_FrameMiddle.left   += FrameScale(5);

	m_FrameMiddle.right  -= FrameScale(5);

	m_FrameMiddle.top    += FrameScale(6);

	m_FrameMiddle.bottom -= FrameScale(6);

	m_FrameInner = m_FrameMiddle;

	m_FrameInner.left   += FrameScale(1);

	m_FrameInner.right  -= FrameScale(1);

	m_FrameInner.top    += FrameScale(1);

	m_FrameInner.bottom -= FrameScale(1);

	m_FrameLogo = m_FramePanel.GetBottomRight();

	m_FrameLogo.x -= FrameScale(16);

	m_FrameLogo.y -= FrameScale(5);
	}

void CFramer::FindScale(void)
{
	switch( m_DispSize.cx ) {

		case 320:
			m_nScale = 8;
			break;

		case 640:
			m_nScale = 12;
			break;

		case 800:
			m_nScale = 15;
			break;

		case 1024:
			m_nScale = 15;
			break;

		default:
			m_nScale = 12;
			break;
		}
	
	m_nPad= 4;
	}

int CFramer::FrameScale(int n) const
{
	return n * m_nScale;
	}

// End of File
