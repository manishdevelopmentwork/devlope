
//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC Serial Driver
// 

#include "intern.hpp"

#include "DoMoreSerial.hpp"

#include "MxAppProtocol.hpp"

// Instantiator

INSTANTIATE(CDoMoreSerialDriver);

// Constructor

CDoMoreSerialDriver::CDoMoreSerialDriver(void)
{
	m_Ident	= DRIVER_ID;

	m_pCtx	= NULL;

	m_uPtr	= 0;
	}

// Destructor

CDoMoreSerialDriver::~CDoMoreSerialDriver(void)
{
	}

// Configuration

void CDoMoreSerialDriver::Load(LPCBYTE pData)
{
	}

void CDoMoreSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}

// Management

void CDoMoreSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void CDoMoreSerialDriver::Open(void)
{
	}

// Device

CCODE CDoMoreSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData)== 0x1234 ) {			

			m_pCtx = new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);
			m_pCtx->m_uPingReg	= GetLong(pData);			
			m_pCtx->m_fUsePing	= GetByte(pData);
			m_pCtx->m_uTimeout	= GetLong(pData);
			m_pCtx->m_pPassword	= GetString(pData);

			m_pCtx->m_wSession	= 0;
			m_pCtx->m_fSessionOpen	= FALSE;

			m_pBaseCtx = m_pCtx;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBaseCtx = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE CDoMoreSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		Free(m_pCtx->m_pPassword);

		delete m_pCtx;
		
		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Port Access

void CDoMoreSerialDriver::Put(BYTE b)
{
	m_pData->Write(b, FOREVER);
	}

UINT CDoMoreSerialDriver::Get(UINT uTimeout)
{
	return m_pData->Read(uTimeout);
	}

// Frame Building

void CDoMoreSerialDriver::StartHeader(void)
{
	m_uPtr = 0;

	AddByte(0x4D);

	AddByte(0x58);

	AddByte(m_pCtx->m_bDrop);

	AddByte(0x6D);
	}

BOOL CDoMoreSerialDriver::StartRequest(CMxAppRequest &Req)
{
	StartHeader();

	PBYTE pBuff = new BYTE[ BUFF_SIZE ];

	memset(pBuff, 0, BUFF_SIZE);

	WORD wLen = Req.Encode(pBuff, BUFF_SIZE);

	if( wLen > 0 ) {

		AddWord(wLen);

		AddData(pBuff, wLen);

		if( Transact() ) {

			delete [] pBuff;

			return TRUE;
			}
		}

	delete [] pBuff;

	return FALSE;
	}

// Transport Layer

BOOL CDoMoreSerialDriver::Transact(void)
{
	if( PutFrame() ) {

		return GetFrame();
		}

	return FALSE;
	}

BOOL CDoMoreSerialDriver::PutFrame(void)
{
	WORD wCheck = GetChecksum(m_bTxBuff + 6, m_uPtr - 6);

	AddWord(wCheck);

	m_pData->ClearRx();

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CDoMoreSerialDriver::GetFrame(void)
{
	UINT uData  = 0;

	m_uPtr      = 0;

	UINT uState = 0;

	UINT uLen   = 0;

	WORD wCheck = 0;

	SetTimer(m_pCtx->m_uTimeout);

	while( GetTimer() ) {

		if( (uData = Get(GetTimer())) == NOTHING ) {

			continue;
			}

		switch( uState ) {

			case 0:
				if( uData == 0x4D ) {

					uState++;
					}

				break;
			case 1:
				if( uData == 0x58 ) {

					uState++;

					ClearChecksum();
					}

				break;

			case 2:
				if( uData == m_pCtx->m_bDrop ) {

					uState++;
					}

				break;

			case 3:
				if( uData == 0x72 ) {

					uState++;
					}

				break;

			case 4:
				uLen = uData;

				uState++;

				break;

			case 5:
				uLen = uLen + (uData << 8);

				uState++;

				break;

			case 6:
				if( m_uPtr < sizeof(m_bRxBuff)) {

					m_bRxBuff[m_uPtr++] = uData;

					AddToChecksum(uData);

					if( --uLen == 0 ) {

						uState++;
						}
					break;
					}

				return FALSE;

			case 7:
				wCheck = uData;

				uState++;

				break;

			case 8:
				wCheck = wCheck + (uData << 8);

				if( wCheck == m_wCheck ) {

					return TRUE;
					}

				return FALSE;
			}
		}

	return FALSE;
	}

// Checksum

WORD CDoMoreSerialDriver::GetChecksum(PBYTE pData, UINT uLen)
{
	WORD wSum = 0;

	for( UINT n = 0; n < uLen; n++ ) {

		wSum += pData[n];
		}

	return wSum;
	}

void CDoMoreSerialDriver::AddToChecksum(BYTE bData)
{
	m_wCheck += bData;
	}

void CDoMoreSerialDriver::ClearChecksum(void)
{
	m_wCheck = 0;
	}

// End of File
