
//////////////////////////////////////////////////////////////////////////
//
// IAI Robocylinder Driver
//

#define IAIROBO_ID	0x3393

struct FAR IairoboCmdDef {
	char	Com[3];
	UINT	MinimumDeviceType;
	UINT	Table;
	};

class CIairoboDriver : public CMasterDriver
{
	public:
		// Constructor
		CIairoboDriver(void);

		// Destructor
		~CIairoboDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bHomePosition;
			DWORD	m_dVel;
			DWORD	m_dAcc;
			DWORD	m_dABPosition;
			DWORD	m_dAddrAlloc;
			DWORD	m_dAAResp;
			DWORD	m_dData;
			DWORD	m_dDataResp;
			DWORD	m_dBAPosition;
			DWORD	m_dBAPosResp;
			DWORD	m_dOK;
			DWORD	m_dVelmm;
			DWORD	m_dAccmm;
			DWORD	m_dThisDeviceType;
			DWORD	m_dXA[4];
			};
		CContext *	m_pCtx;

		// Data Members
		BOOL	m_fIs9600;

		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		UINT	m_uPtr;
		UINT	m_bCheck;
		UINT	m_uPingCount;
		UINT	m_uWriteErr;

		DWORD	m_dStatus;
		DWORD 	m_dAlarm;
		DWORD	m_dInput;
		DWORD	m_dOutput;

		//
		// List access
		static IairoboCmdDef CODE_SEG CL[];
		IairoboCmdDef	FAR * m_pCL;
		IairoboCmdDef	FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;
	
		// Opcode Handler
		void	PutPointWrite(void);
		void	PutAllPointData(void);
		void	AddQ(BYTE  bPoint);
		void	AddT(DWORD dData);
		void	AddW(DWORD dData);
		void	AddV(BYTE  bPoint);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor);
		void	AddCommand(DWORD dData, BOOL fIsWrite);
		void	PutText(LPCTXT pCmd);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	CheckReply(void);
		BOOL	GetResponse(PDWORD pData, UINT uStep);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	NoReadTransmit(PDWORD pData);
		BOOL	NoWriteTransmit(DWORD dData);
		BOOL	ValidType(void);
		void	SetpItem(UINT uID);
		void	GetStatus(void);
		BOOL	SetAddressForWMemory(UINT uAddress);
		DWORD	GetGeneric(PBYTE p, UINT uCt);
		void	MakeCheckSum(PBYTE pSrc, PBYTE pDest );
		void	Pad(UINT uCt);
		void	ResetComms(void);

	};

// Model dependent defs
#define	MODELADDR	0x6800
#define	RCPTYPE		0x70
#define	RCSTYPE		0xA8
#define RCP2TYPE	0xAD

// Point Addresses
#define	POSADD	0x400
#define	VELADD	0x404
#define	ACCADD	0x405
#define	LIMADD	0x407

#define	RWMEM	1
#define	STINQ	2
#define	STATUS	3
#define	ALARMS	4
#define	INPUTS	5
#define	OUTPUT	6
#define	MOVABS	7
#define	STOP	8
#define	INCR	9
#define	HOME	10
#define	SERVO	11
#define	MOVPOS	12
#define	VELC	13
#define	ACCC	14
#define	VELACC	15
#define	ABPOSC	16
#define	PTPOSA	17
#define	ADDRC	18
#define	ADDRS	19
#define	ADDRR	20
#define	DATAC	21
#define	DATAS	22
#define	DATAR	23
#define	BAPOSC	24
#define	PTPOSB	25
#define	BAPOSR	26
#define	PTWRT	27
#define	PTOK	28
#define	PBAND	29
#define	ALMR	30
#define	MOVMA	31
#define	MOVMI	32
#define	FSPDC	33
#define	FACCC	34
#define	FSEND	35
#define	RAMPS	36
#define	HOMEPOS	37
#define	XWP	38
#define	XWV	39
#define	XWA	40
#define	XWM	41
#define	XA	42

// End of File
