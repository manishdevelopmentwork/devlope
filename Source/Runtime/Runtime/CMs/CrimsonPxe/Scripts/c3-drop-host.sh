#!/bin/sh

# c3-drop-host <interface>
#
# Remove the special entry from the local and global hosts files that
# was added to provide a reference to the IP address of this interface.

con1="/etc/hosts"

con2="/tmp/crimson/dns/hosts"

temp="/tmp/crimson/dns/hosts.temp"

face="$1"

name="c3-${face}-addr"

(
	flock -x 9

	for conf in $con1 $con2
	do
		cat $conf | grep -v $name > $temp
		
		cp $temp $conf
	done

) 9> /var/lock/c3-hosts
