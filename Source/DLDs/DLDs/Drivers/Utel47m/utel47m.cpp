
#include "intern.hpp"

#include "utel47m.hpp"
	
//////////////////////////////////////////////////////////////////////////
//
// Unitelwaym Driver
//

// *** This driver is the version that was active for Config Ver 187,
// which worked with the TSX47, while subsequent versions did not.

// Instantiator

INSTANTIATE(CUnitel47mDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CUnitel47mDriver::CUnitel47mDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CUnitel47mDriver::~CUnitel47mDriver(void)
{
	}

// Configuration

void MCALL CUnitel47mDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CUnitel47mDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CUnitel47mDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CUnitel47mDriver::Open(void)
{
	}

// Device

CCODE MCALL CUnitel47mDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Drop = GetWord(pData);

			m_pCtx->m_Category = GetWord(pData) == 0 ? 7 : 6;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CUnitel47mDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CUnitel47mDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SBIT;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrBitAsBit;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CUnitel47mDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nREAD -- T = %d, O = %d, C = %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			if( BitRead( Addr, pData, &uCount ) )
				return uCount;
			break;

		case addrByteAsByte:

			if( ByteRead( Addr, pData, &uCount ) )
				return uCount;
			break;

		case addrWordAsWord:

			if( WordRead( Addr, pData, &uCount ) )
				return uCount;
			break;

		case addrLongAsLong:
		case addrRealAsReal:

			if( LongRead( Addr, pData, &uCount ) )
				return uCount;
			break;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CUnitel47mDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n**** WRITE T = %d O = %d C = %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == CWORD || Addr.a.m_Table == CLONG || Addr.a.m_Table == CREAL )
		return uCount;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			if( BitWrite( Addr, pData, &uCount ) )
				return uCount;
			break;

		case addrByteAsByte:

			if( ByteWrite( Addr, pData, &uCount ) )
				return uCount;
			break;

		case addrWordAsWord:

			if( WordWrite( Addr, pData, &uCount ) )
				return uCount;
			break;

		case addrLongAsLong:
		case addrRealAsReal:

			if( LongWrite( Addr, pData, &uCount ) )
				return uCount;
			break;
		}

	return CCODE_ERROR;
	}

// Opcode Handlers

BOOL CUnitel47mDriver::BitRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	BYTE bOp = Addr.a.m_Table == MBIT ? OPRDI : OPRDS;

	PutShortHeader( Addr.a.m_Offset, bOp );

	UINT uStart = Addr.a.m_Offset % 8;

	UINT uCount = *pCount;

	MakeMin( uCount, 8 - uStart );

	if( Transact(FALSE) ) {
	
		if( m_bRx[6] == 0x30 + bOp ) {

			for( UINT i = 0; i < uCount; i++ ) {

				pData[i] = ( m_bRx[7] & (1 << (uStart+i)) ) ? 1 : 0;
				}

			*pCount = uCount;
			
			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CUnitel47mDriver::ByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	BYTE bOp = Addr.a.m_Table == MBYTE ? OPRDI : OPRDS;

	UINT uCount = *pCount;

	MakeMin( uCount, 8 );

	for( UINT i = 0; i < uCount; i++ ) {

		PutShortHeader( (Addr.a.m_Offset + i) * 8, bOp );

		if( !Transact(FALSE) || m_bRx[6] != ( 0x30 + bOp ) ) {

			return FALSE;
			}

		pData[i] = m_bRx[7];
		}

	*pCount = uCount;
		
	return TRUE;
	}

BOOL CUnitel47mDriver::WordRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	DWORD dResult = 0;
	UINT i = 0;
	UINT j = 0;

	BYTE bSeg;

	BYTE bType;

	UINT uOffset = Addr.a.m_Offset;

	UINT uCount = *pCount;

	MakeMin( uCount, 4 );

	switch( Addr.a.m_Table ) {

		case MWORD:
			bSeg  = MEMSEG;
			bType = MEMWORD;
			break;

		case SWORD:
			bSeg  = SYSSEG;
			bType = SYSWORD;
			break;

		case CWORD:
			bSeg  = CONSEG;
			bType = CONWORD;
			break;

		default:
			return FALSE;
		}

	PutWordsHeader( uOffset, uCount, OPREAD, bSeg, bType );

	if( Transact(FALSE) ) {
	
		if( m_bRx[6] == 0x66 ) {

			for( i = 0, j = 8; i < uCount; i++, j+=2 ) {

				dResult = GetData( j, 2 );

				if( dResult & 0x8000 ) {
					
					dResult |= 0xFFFF0000;
					}

				pData[i] = dResult;
				}

			*pCount = uCount;
			
			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CUnitel47mDriver::LongRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	DWORD dResult = 0;
	UINT i = 0;
	UINT j = 0;

	BYTE bSeg;

	BYTE bType;

	UINT uOffset = Addr.a.m_Offset;

	UINT uCount = *pCount;

	MakeMin( uCount, 1 );

	switch( Addr.a.m_Table ) {

		case MLONG:
		case MREAL:
			bSeg  = MEMSEG;
			bType = MEMLONG;
			break;

		case SLONG:
			bSeg  = SYSSEG;
			bType = SYSWORD; // Longs did not work with test PLC
			break;

		case CLONG:
		case CREAL:
			bSeg  = CONSEG;
			bType = CONLONG;
			break;

		default:
			return FALSE;
		}

	PutWordsHeader( uOffset, bSeg == SYSSEG ? uCount * 2 : uCount, OPREAD, bSeg, bType );

	if( Transact(Addr.a.m_Table == MREAL || Addr.a.m_Table == CREAL) ) {
	
		if( m_bRx[6] == 0x66 ) {

			for( i = 0, j = 8; i < uCount; i++, j+=4 ) {

				dResult = GetData( j, 4 );

				pData[i] = dResult;
				}

			*pCount = uCount;
			
			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CUnitel47mDriver::BitWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	PutShortHeader( Addr.a.m_Offset, Addr.a.m_Table == MBIT ? OPWRI : OPWRS );

	AddByte(pData[0] & 1 ? 1 : 0 );
	
	if( Transact(FALSE) ) {
	
		if( m_bRx[6] == 0xFE ) {

			*pCount = 1;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUnitel47mDriver::ByteWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	for( UINT i = 0; i < 8; i++ ) {

		PutShortHeader( (Addr.a.m_Offset * 8) + i, Addr.a.m_Table == MBYTE ? OPWRI : OPWRS );

		AddByte( pData[0] & ( 1 << i ) ? 1 : 0 );
	
		if( !Transact(FALSE) || m_bRx[6] != 0xFE ) {

			return FALSE;
			}
		}

	*pCount = 1;

	return TRUE;
	}

BOOL CUnitel47mDriver::WordWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	BYTE bSeg;

	BYTE bType;

	UINT uOffset = Addr.a.m_Offset;

	UINT uCount = *pCount;

	MakeMin( uCount, 4 );

	switch( Addr.a.m_Table ) {

		case MWORD:
			bSeg  = MEMSEG;
			bType = MEMWORD;
			break;

		case SWORD:
			bSeg  = SYSSEG;
			bType = SYSWORD;
			break;

		case CWORD:
			bSeg  = CONSEG;
			bType = CONWORD;
			break;

		default:
			return FALSE;
		}

	PutWordsHeader( uOffset, uCount, OPWRITE, bSeg, bType );
	
	for( UINT i = 0; i < uCount; i++ ) {

		AddData(LOWORD(pData[i]) );
		}
	
	if( Transact(FALSE) ) {
	
		if( m_bRx[6] == 0xFE ) {

			*pCount = uCount;

			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CUnitel47mDriver::LongWrite(AREF Addr, PDWORD pData, UINT * pCount)
{
	BYTE bSeg;

	BYTE bType;

	UINT uOffset = Addr.a.m_Offset;

	UINT uCount = *pCount;

	MakeMin( uCount, 2 );

	switch( Addr.a.m_Table ) {

		case MLONG:
		case MREAL:
			bSeg  = MEMSEG;
			bType = MEMLONG;
			break;

		case SLONG:
			bSeg  = SYSSEG;
			bType = SYSWORD; // Longs did not work with test PLC
			break;

		case CLONG:
			bSeg  = CONSEG;
			bType = CONLONG;
			break;

		default:
			return FALSE;
		}

	PutWordsHeader( uOffset, bSeg == SYSSEG ? uCount * 2 : uCount, OPWRITE, bSeg, bType );
	
	for( UINT i = 0; i < uCount; i++ ) {

		AddData(LOWORD(pData[i]) );
		AddData(HIWORD(pData[i]) );
		}
	
	if( Transact(FALSE) ) {
	
		if( m_bRx[6] == 0xFE ) {

			*pCount = uCount;

			return TRUE;
			}
		}
		
	return FALSE;
	}

// Frame Building

void CUnitel47mDriver::PutShortHeader(UINT uOffset, BYTE bOp)
{
	Clear(m_pCtx->m_Drop);

	AddByte(bOp);
	AddByte(m_pCtx->m_Category);

	AddData( uOffset );
	}

void CUnitel47mDriver::PutWordsHeader(UINT uOffset, UINT uCount, BYTE bOp, BYTE bSeg, BYTE bType)
{
	Clear(m_pCtx->m_Drop);

	AddByte(bOp);
	AddByte(m_pCtx->m_Category);
	AddByte(bSeg);
	AddByte(bType);

	AddData( uOffset );

	AddData( uCount );
	}

void CUnitel47mDriver::AddByte(BYTE bData)
{
	if( bData == DLE )
		m_bTx[m_uPtr++] = DLE;

	m_bTx[m_uPtr++] = bData;

	m_bTx[3]++;
	}

void CUnitel47mDriver::AddData(UINT uData)
{
	AddByte( LOBYTE(uData) );
	AddByte( HIBYTE(uData) );
	}

// Transport Layer

BOOL CUnitel47mDriver::Transact(BOOL fWait)
{
	return Send(fWait);
	}

BOOL CUnitel47mDriver::Send(BOOL fWait)
{
	m_pData->ClearRx();	// clear Rx buffer anew

	UINT uState = 0;

	UINT uCount = 0;

	for(;;) {
		
		switch( uState ) {
		
			case 0:
				SendPoll(m_pCtx->m_Drop);

				switch( Receive(TYPE_POLL) ) {
				
					case DLL_EOT:
						uState = 1;
						uCount = 0;
						break;
						
					case DLL_FRAME:

						if( uCount++ < RETRIES )
							Put(ACK);
						else
							return FALSE;
						break;
						
					case DLL_ERROR:

						if( uCount++ < RETRIES )
							Put(NAK);
						else
							return FALSE;
						break;
						
					default:
						return FALSE;
					}
				break;
				
			case 1:
				SendFrame();

				switch( Receive(TYPE_REPLY) ) {
				
					case DLL_NAK:

						if( uCount++ < RETRIES ) {
							uState = 1;
							}

						else {
							return FALSE;
							}
						break;
						
					case DLL_ACK:
						uState = 2;
						uCount = 0;
						break;
						
					default:
						return FALSE;
					}
				break;
				
			case 2:
				SendPoll(m_pCtx->m_Drop);

				switch( Receive(TYPE_POLL) ) {
				
					case DLL_EOT:

						if( uCount++ < RETRIES )
							Sleep(NET_POLL_GAP);

						else
							return FALSE;
						break;
					
					case DLL_ERROR:
						if( uCount++ < RETRIES )
							Put(NAK);
						else
							return FALSE;
						break;

					case DLL_FRAME:

//**/						AfxTrace0("\r\n");

						if( fWait ) Sleep(80); // Wait on Reals for PLC to Clear

						Put(ACK);

//**/						AfxTrace0("\r\n");

						return TRUE;
						
					default:
						return FALSE;
					}
				break;
			}
		}
	}

void CUnitel47mDriver::SendFrame(void)
{
	UINT uScan;
	UINT uCheck = 0;

//**/	AfxTrace0("\r\n");

	for( uScan = 0; uScan < m_uPtr; uScan++ ) {

		if( uScan == 3 && m_bTx[3] == DLE ) {
		
			uCheck += DLE;
			
			Put(DLE);
			}
				
		uCheck += m_bTx[uScan];
		
		Put(m_bTx[uScan]);
		}

	Put(LOBYTE(uCheck));
	}

void CUnitel47mDriver::SendPoll(BYTE bStation)
{
//**/	AfxTrace0("\r\nPOL ");

	Put(DLE);

	Put(ENQ);

	Put(bStation);
	}

// Port Access

void CUnitel47mDriver::Put(BYTE b)
{
//**/	AfxTrace1("[%2.2x]", b);

	m_pData->Write(b, FOREVER);
	}

UINT CUnitel47mDriver::Get(void)
{
	return m_pData->Read( TIMEOUT );
	}

// Helpers

UINT CUnitel47mDriver::Receive(UINT uType)
{
	BOOL fTimeout = TRUE;

	UINT uLength = 0;
	UINT uCheck = 0;
	UINT uPtr = 0;

	UINT uPause = 0;

	UINT uState = 0;

//**/	AfxTrace0("\r\n");

	switch( uType ) {
	
		case TYPE_REPLY:
			SetTimer( DLL_REPLY_TIMEOUT );
			break;

		case TYPE_POLL:
			SetTimer( DLL_POLL_TIMEOUT );
			break;
			
		default:
			fTimeout = FALSE;
			break;
		}
	
	while( !fTimeout || GetTimer() ) {

		UINT uByte = Get();
	
		if( LOWORD(uByte) == LOWORD(NOTHING) ) {
			
			Sleep(DLL_PAUSE);
			 
			if( ++uPause == DLL_GAP_TIMEOUT ) {
				uPause = 0;
				uState = 0;
				}

			continue;
			}
		else
			uPause = 0;

//**/		AfxTrace1("<%2.2x>", uByte);

		switch( uState ) {
		
			case 0:
				switch( uByte ) {

					case DLE:
						uCheck = 0;
						uState = 1;
						break;
						
					case ACK:
						if( uType == TYPE_REPLY )
							return DLL_ACK;
						break;
						
					case NAK:
						if( uType == TYPE_REPLY )
							return DLL_NAK;
						break;
						
					case EOT:
						if( uType == TYPE_POLL )
							return DLL_EOT;
						break;
					}
				break;
				
			case 1:
				switch( uByte ) {
				
					case ENQ:
						uState = 2;
						break;
						
					case STX:
						uState = 3;
						break;
						
					default:
						uState = 0;
						break;
					}
				break;
			
			case 2:
				if( uType == TYPE_POLL )
					uState = 0;
				else
					return DLL_ENQ;
				break;
				
			case 3:
				uState = 4;
				uPtr   = 0;
				break;
				
			case 4:
				if( uByte == DLE )
					uState = 5;
				else {
					uLength = uByte;
					uState  = 6;
					}
				break;
			
			case 5:
			 	if( uByte == DLE ) {
					uLength = uByte;
					uState  = 6;
					}
				else
					uState = 0;
				break;
				
			case 6:
				if( uByte == DLE )
					uState = 7;
				else {
					m_bRx[uPtr++] = uByte;
					
					if( uPtr == uLength )
						uState = 8;
					}
				break;
				
			case 7:
				if( uByte == DLE ) {

					m_bRx[uPtr++] = uByte;
					
					if( uPtr == uLength )
						uState = 8;
					else
						uState = 6;
					}
				else
					uState = 0;
				break;
				
			case 8:
				if( uType != TYPE_REPLY ) {

					if( uByte == LOBYTE(uCheck) )
						return DLL_FRAME;

					else
						return DLL_ERROR;
					}
				else
					uState = 0;
				break;
			}

		uCheck = uCheck + uByte;
		}
		
	return DLL_NULL;
	}

void CUnitel47mDriver::Clear(BYTE bStation)
{
	m_bTx[0] = DLE;
	m_bTx[1] = STX;
	m_bTx[2] = bStation;
	m_bTx[3] = 0;
	
	m_uPtr = 4;
	
	AddByte(0x20);

	AddByte(0x00);
	AddByte(0xFE);
	AddByte(0x00);
	AddByte(0xFE);
	AddByte(0x00);
	}

DWORD CUnitel47mDriver::GetData( UINT uPos, UINT uByteCount )
{
	DWORD dResult = 0;

	switch( uByteCount ) {

		case 4:
			dResult += ( DWORD(m_bRx[uPos+3]) << 24 );
		case 3:
			dResult += ( DWORD(m_bRx[uPos+2]) << 16 );
		case 2:
			dResult += ( DWORD(m_bRx[uPos+1]) << 8 );
		case 1:
		default:
			dResult += DWORD(m_bRx[uPos]);

			break;
		}

	return dResult;
	}

// End of File
