12
Chapter 2 Standard Functions	5
Abs(value)	6
AbsR64(result, tag)	7
acos(value)	8
acosR64(result, tag)	9
AddR64(result, tag1, tag2)	10
AddU32 (tag1, tag2)	11
AlarmAccept(alarm)	12
AlarmAcceptAll()	13
AlarmAcceptEx(source, method, code)	14
AlarmAcceptTag(tag, index, event)	15
asin(value)	16
asinR64(result, tag)	17
AsText(n)	18
AsTextL64(data, radix, count)	19
AsTextR64(data)	20
AsTextR64WithFormat(format, data)	21
atan(value)	22
atan2(a, b)	23
atanR64(result, tag)	24
atan2R64(result, a, b)	25
Beep(freq, period)	26
CanGotoNext()	27
CanGotoPrevious()	28
ClearEvents()	29
CloseFile(file)	30
ColBlend(data, min, max, col1, col2)	31
ColFlash(freq, col1, col2)	32
ColGetBlue(col)	33
ColGetGreen(col)	34
ColGetRed(col)	35
ColGetRGB(r,g,b)	36
ColPick2(pick, col1, col2)	37
ColPick4(data1, data2, col1, col2, col3, col4)	38
ColSelFlash(enable, freq, col1, col2, col3)	39
CommitAndReset()	40
CommitPersonality(reset)	41
CompactFlashEject()	42
CompactFlashStatus()	43
CompU32(tag1, tag2)	44
ControlDevice(device, enable)	45
Copy(dest, src, count)	46
CopyFiles(source, target, flags)	47
cos(theta)	48
cosR64(result, tag)	49
CreateDirectory(name)	50
CreateFile(name)	51
DataToText(data, limit)	52
Date(y, m, d)	53
DebugDumpLocals()	54
DebugPrint(text)	55
DebugStackTrace()	56
DecR64(result, tag)	57
DecToText(data, signed, before, after, leading, group)	58
Deg2Rad(theta)	59
DeleteDirectory(name)	60
DeleteFile(file)	61
DevCtrl(device, function, data)	62
DisableDevice(device)	63
DispOff()	64
DispOn()	65
DivR64(result, tag1, tag2)	66
DivU32(tag1, tag2)	67
DrvCtrl(port, function, data)	68
EjectDrive(drive)	69
EmptyWriteQueue (dev)	70
EnableBatteryCheck(disable)	71
EnableDevice(device)	72
EndBatch()	73
EndModal(code)	74
EnumOptionCard(s)	75
EqualR64(a, b)	76
exp(value)	77
exp10(value)	78
exp10R64(result, tag)	79
expR64(result, tag)	80
FileSeek(file, pos)	81
FileTell(file)	82
Fill(element, data, count)	83
Find(string, char, skip)	84
FindFileFirst(dir)	85
FindFileNext()	86
FindTagIndex(label)	87
Flash(freq)	88
Force(dest, data)	89
ForceCopy(dest, src, count)	90
ForceSQLSync()	91
FormatCompactFlash()	92
FormatDrive(drive)	93
FtpGetFile(server, loc, rem, delete)	94
FtpPutFile(server, loc, rem, delete)	95
GetAlarmTag(index)	96
GetAutoCopyStatusCode()	97
GetAutoCopyStatusText()	98
GetBatch()	99
GetCameraData(port, camera, param)	100
GetCurrentUserName()	101
GetCurrentUserRealName()	102
GetCurrentUserRights()	103
GetDate(time)	104
GetDay(time)	104
GetDays(time)	104
GetHour(time)	104
GetMin(time)	104
GetMonth(time)	104
GetSec(time)	104
GetWeek(time)	104
GetWeeks(time)	104
GetWeekYear(time)	104
GetYear(time)	104
GetDeviceStatus(device)	105
GetDiskFreeBytes(drive)	106
GetDiskFreePercent(drive)	107
GetDiskSizeBytes(drive)	108
GetDriveStatus(drive)	109
GetFileByte(file)	110
GetFileData(file, data, length)	111
GetFormattedTag(index)	112
GetInterfaceStatus(port)	113
GetIntTag(index)	114
GetLanguage()	115
GetLastEventText(all)	116
GetLastEventTime(all)	117
GetLastEventType(all)	118
GetLastSQLSyncStatus()	119
GetLastSQLSyncTime(Request)	120
GetLicenseState (id)	121
GetLocationPropery(index, property)	122
GetModelName(code)	123
GetModemProperty(index, property)	124
GetMonthDays(y, m)	125
GetNetGate(port)	126
GetNetId(port)	127
GetNetIp(port)	128
GetNetMask(port)	129
GetNow()	130
GetNowDate()	131
GetNowTime()	132
GetPersonalityFloat (name)	133
GetPersonalityInt (name)	134
GetPersonalityIp (name)	135
GetPersonalityString (name)	136
GetPortConfig(port, param)	137
GetQueryStatus(Query)	138
GetQueryTime(Query)	139
GetRealTag(index)	140
GetRestartCode(n)	141
GetRestartInfo(n)	142
GetRestartText(n)	143
GetRestartTime(n)	144
GetSQLConnectionStatus()	145
GetStringTag(index)	146
GetTagLabel(index)	147
GetUpDownData(data, limit)	148
GetUpDownStep(data, limit)	149
GetVersionInfo(code)	150
GetWebParamHex(param)	151
GetWebParamInt(param)	152
GetWebParamStr	153
GetWifiProperty(index, property)	154
GotoNext()	155
GotoPage(name)	156
GotoPrevious()	157
GreaterEqR64(a, b)	158
GreaterR64(a, b)	159
HasAccess (rights)	160
HasAllAccess(rights)	161
HideAllPopups()	162
HidePopup()	163
IntR64(result, tag)	164
IntToR64(result, n)	165
IntToText(data, radix, count)	166
IsBatchNameValid(name)	167
IsBatteryLow()	168
IsDeviceOnline(device)	169
IsLoggingActive()	170
IsPortRemote(port)	171
IsSQLSyncRunning()	172
IsWriteQueueEmpty(dev)	173
KillDirectory(name)	174
Left(string, count)	175
Len(string)	176
LessEqR64(a, b)	177
LessR64(a, b)	178
LoadCameraSetup(port, camera, index, file)	179
LoadSecurityDatabase(mode, file)	180
Log(value)	181
Log10(value)	182
Log10R64(result, tag)	183
LogBatchComment(set, text)	184
LogBatchHeader(set, text)	185
LogComment(log, text)	186
LogHeader(log, text)	187
logR64(result, tag)	188
LogSave()	189
MakeFloat(value)	190
MakeInt(value)	191
Max(a, b)	192
MaxR64(result, tag1, tag2)	193
MaxU32(tag1, tag2)	194
Mean(element, count)	195
Mid(string, pos, count)	196
Min(a, b)	197
MinR64(result, tag1, tag2)	198
MinU32(tag1, tag2)	199
MinusR64(result, tag)	200
ModU32(tag1, tag2)	201
MountCompactFlash(enable)	202
MoveFiles(source, target, flags)	203
MulDiv(a, b, c)	204
MulR64(result, tag1, tag2)	205
MulU32(tag1, tag2)	206
MuteSiren()	207
NetworkPing(address, timeout)	208
NewBatch(name)	209
Nop()	210
NotEqualR64(a, b)	211
OpenFile(name, mode)	212
Pi()	213
PlayRTTTL(tune)	214
PopDev(element, count)	215
PortClose(port)	216
PortGetCTS(port)	217
PortInput(port, start, end, timeout, length)	218
PortPrint(port, string)	219
PortPrintEx(port, string)	220
PortRead(port, period)	221
PortSendData(port, data, count)	222
PortSetRTS(port, state)	223
PortWrite(port, data)	224
PostKey(code, transition)	225
Power(value, power)	226
PowR64(result, value, power)	227
PrintScreenToFile(path, name, res)	228
PutFileByte(file, data)	229
PutFileData(file, data, length)	230
R64ToInt(x)	231
R64ToReal(x)	232
Rad2Deg(theta)	233
Random(range)	234
ReadData(data, count)	235
ReadFile(file, chars)	236
ReadFileLine(file)	237
RealToR64(result, n)	238
RenameFile(handle, name)	239
ResolveDNS(name)	240
Right(string, count)	241
RShU32(tag1, tag2)	242
RunAllQueries()	243
RunQuery(query)	244
RxCAN(port, data, id)	245
RxCANInit(port, id, dlc)	246
SaveCameraSetup(port, camera, index, file)	247
SaveConfigFile(file)	248
SaveSecurityDatabase(mode, file)	249
Scale(data, r1, r2, e1, e2)	250
SendFile(rcpt, file)	251
SendFileEx(rcpt, file, subject, flag)	252
SendFileTo(rcpt, file)	253
SendFileToAck(rcpt, file, ack)	254
SendMail(rcpt, subject, body)	255
SendMailTo(rcpt, sub, msg)	256
SendMailToAck(rcp, sub, msg, ack)	257
Set(tag, value)	258
SetIconLed(id, state)	259
SetIntTag(index, value)	260
SetLanguage(code)	261
SetNow(time)	262
SetPersonalityFloat(name, data)	263
SetPersonalityInt(name, data)	264
SetPersonalityIp(name, data)	265
SetPersonalityString(name,data)	266
SetRealTag(index, value)	267
SetStringTag(index, data)	268
Sgn(value)	269
ShowMenu(name)	270
ShowModal(name)	271
ShowNested(name)	272
ShowPopup(name)	273
sin(theta)	274
sinR64(result, tag)	275
SirenOn()	276
Sleep(period)	277
Sqrt(value)	278
SqrtR64(result, tag)	279
StdDev(element, count)	280
StopRTTTL ()	281
StopSystem()	282
Strip(text, target)	283
SubR64(result, tag1, tag2)	284
SubU32(tag1, tag2)	285
Sum(element, count)	286
tan(theta)	287
tanR64(result, tag)	288
TestAccess(rights, prompt)	289
TextToAddr(addr)	290
TextToFloat(string)	291
TextToInt(string, radix)	292
TextToL64(input, output, radix)	293
TextToR64(input, output)	294
Time(h, m, s)	295
TxCAN(port, data, id)	296
TxCANInit(port, id, dlc)	297
UseCameraSetup(port, camera, index)	298
UserLogOff()	299
UserLogOn()	300
WaitData(data, count, time)	301
WriteAll()	302
WriteFile(file, text)	303
WriteFileLine(file, text)	304
Chapter 3 System Variables 305
ActiveAlarms	306
CommsError	307
DispBrightness	308
DispContrast	309
DispCount	310
DispUpdates	311
IconBrightness	312
IsPressed 313
IsSirenOn	314
Pi	315
TimeNow	316
TimeZone	317
TimeZoneMins	318
Unaccepted Alarms	319
UnacceptedAndAutoAlarms	320
UseDST	321