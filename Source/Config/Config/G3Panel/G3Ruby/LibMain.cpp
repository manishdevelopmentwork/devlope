
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Ruby\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Ruby\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
