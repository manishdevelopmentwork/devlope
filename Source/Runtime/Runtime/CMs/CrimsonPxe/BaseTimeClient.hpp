
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_BaseTimeClient_HPP

#define	INCLUDE_BaseTimeClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Basic Time Client
//

class CBaseTimeClient : public IClientProcess
{
public:
	// Constructor
	CBaseTimeClient(CJsonConfig *pJson);

	// Destructor
	~CBaseTimeClient(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG   m_uRefs;
	CString m_Name;
	CString m_Diag;
	UINT    m_uMode;
	UINT    m_uPeriod;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
	BOOL SyncTime(CTimeSourceInfo const &Info);

	// Overridables
	virtual BOOL PerformSync(void) = 0;
};

// End of File

#endif
