
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_StdHeader_HPP

#define INCLUDE_StdHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <ctype.h>
#include <unistd.h>
#include <wctype.h>
#include <wchar.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/time.h>

//////////////////////////////////////////////////////////////////////////
//
// What Else Goes in Here?
//

// Min and Max Templates?
// Bit Macro?
// Any Other Templates?

//////////////////////////////////////////////////////////////////////////
//
// File Header Macro
//

#define	AfxFileHeader()	/*OBSOLETE*/

//////////////////////////////////////////////////////////////////////////
//
// Namespace Macros
//

#define	AfxNamespaceDefine(n)		namespace n { }

#define	AfxNamespaceBegin(n)		namespace n {

#define AfxNamespaceEnd(n)		}

#define AfxNamespaceUse(n)		using namespace n

#define AfxNamespaceAlias(n, a)		namespace a = n

//////////////////////////////////////////////////////////////////////////
//
// Placement New
//

inline void * operator new (size_t, void *p)
{
	return p;
	}

//////////////////////////////////////////////////////////////////////////
//
// General Constants
//

#define FALSE		0
#define TRUE		1
#define	NOTHING		UINT(-1)
#define	EMPTY		UINT(-1)
#define	FOREVER		UINT(-1)

//////////////////////////////////////////////////////////////////////////
//
// General Macros
//

#undef  offset

#define global		/**/
#define ThisObject	(*this)
#define	elements(x)	(sizeof(x) / sizeof(*(x)))
#define	offset(t, x)	UINT(&(((t *) 0)->x))
#define	clink		extern "C"
#define	BEGIN		do {
#define	END		} while(0)
#define	AfxTouch(x)	((void) (x))
#define DLLAPI		global
#define	Bit(x)		(1<<(x))

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#define	MakeMin(a, b)	BEGIN { if( (b) < (a) ) (a) = (b); } END

#define	MakeMax(a, b)	BEGIN { if( (b) > (a) ) (a) = (b); } END

//////////////////////////////////////////////////////////////////////////
//
// Linked List Macros
//

#define	AfxListAppend(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->p = t;				\
	o->n = NULL;				\
	t ? (t->n = o) : (h = o);		\
	t = o;					\
	} END					\

#define	AfxListInsert(h, t, o, n, p, b)		\
						\
	BEGIN {					\
	o->p = b ? b->p : t;			\
	o->n = b;				\
	o->p ? (o->p->n = o) : (h = o);		\
	o->n ? (o->n->p = o) : (t = o);		\
	} END					\

#define	AfxListRemove(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->n ? (o->n->p = o->p) : (t = o->p);	\
	o->p ? (o->p->n = o->n) : (h = o->n);	\
	} END					\

//////////////////////////////////////////////////////////////////////////
//
// Platform Specific Headers
//

#if defined(_WIN32) && !defined(_M_LINUX)

#include "PlatWin32.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Typedef Helpers
//

#define	AfxDefinePtrs(name, type)		\
						\
	typedef type          * P##name;	\
	typedef type const    * PC##name;	\
	typedef type volatile * PV##name;	\
	typedef type	      * LP##name;	\
	typedef type const    * LPC##name;	\
	typedef type volatile * LPV##name;	\

#define	AfxDefineType(name, type)		\
						\
	typedef type name;			\
						\
	AfxDefinePtrs(name, type)		\

//////////////////////////////////////////////////////////////////////////
//
// Shared Type Definitions
//

#if !defined(AEON_NEED_WIN32)

AfxDefineType(CHAR,  char);
AfxDefineType(INT,   int);
AfxDefineType(SHORT, short);
AfxDefineType(LONG,  long);
AfxDefineType(VOID,  void);
AfxDefineType(BYTE,  unsigned char);
AfxDefineType(UINT,  unsigned int);
AfxDefineType(WORD,  unsigned short);
AfxDefineType(DWORD, unsigned long);
AfxDefineType(ULONG, unsigned long);
AfxDefineType(BOOL,  int);
AfxDefinePtrs(STR,   char);

#endif

AfxDefinePtrs(TXT,    char);
AfxDefineType(WCHAR,  wchar_t);
AfxDefinePtrs(UTF,    wchar_t);
AfxDefineType(TCHAR,  char);
AfxDefineType(INT32,  int);
AfxDefineType(SINT32, signed int);
AfxDefineType(UINT32, unsigned int);

//////////////////////////////////////////////////////////////////////////
//
// Quick Argument
//

#define  QUICKARG struct tagQUICK &

#define	 QUICK	  ((struct tagQUICK &) * ((struct tagQuick *) 0))

//////////////////////////////////////////////////////////////////////////
//
// COM Method Types
//

#define HRM HRESULT METHOD

#define ULM ULONG   METHOD

//////////////////////////////////////////////////////////////////////////
//
// Splitter Macros
//

#if !defined(_WIN32) || !defined(AEON_NEED_WIN32)

#define LOWORD(l) ((WORD)(((DWORD)(l)) & 0xFFFF))

#define HIWORD(l) ((WORD)((((DWORD)(l)) >> 16) & 0xFFFF))

#define LOBYTE(w) ((BYTE)(((DWORD)(w)) & 0xFF))

#define HIBYTE(w) ((BYTE)((((DWORD)(w)) >> 8) & 0xFF))

#endif

//////////////////////////////////////////////////////////////////////////
//
// Combination Macros
//

#if !defined(_WIN32) || !defined(AEON_NEED_WIN32)

#define MAKEWORD(l, h) ((WORD)(((BYTE)(l)) | (((WORD )((BYTE)(h))) <<  8)))

#define MAKELONG(l, h) ((LONG)(((WORD)(l)) | (((DWORD)((WORD)(h))) << 16)))

#endif

//////////////////////////////////////////////////////////////////////////
//
// Compiler Specific Headers
//

#if defined(_MSC_VER) && !defined(__GNUC__)

#include "CompMsvc.hpp"

#endif

#if defined(__GNUC__)

#include "CompGcc.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Processor Specific Headers
//

#if (defined(__i386__) &&  defined(__GNUC__)) || (defined(_M_IX86 ) && !defined(__GNUC__))

#include "ProcX86.hpp"

#endif

#if defined(_M_AM437) || defined(_M_IMX51) || defined(_M_ARM)

#include "ProcArm.hpp"

#endif

#if defined(_M_AM437)

#include "ProcAM437.hpp"

#endif

#if defined(_M_IMX51)

#include "ProciMX51.hpp"

#endif

#if defined(_M_ARM926) && defined(_M_LINUX)

#include "ProcArm926.hpp"

#endif

#if defined(_M_CORTEX) && defined(_M_LINUX)

#include "ProcCortex.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Platform Specific Headers
//

#if defined(_M_LINUX)

#include "PlatLinux.hpp"

#endif

#if defined(_RLOS)

#include "PlatRLOS.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Support
//

#if !defined(AEON_COM_DEFINED)

#include "StdCom.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#if !defined(min)

#define min(a, b) Min(a, b)

#define max(a, b) Max(a, b)

#endif

//////////////////////////////////////////////////////////////////////////
//
// COM Interface Support
//

#define AfxDeclareIID(t, n)	static WORD const m_iit = t; \
				static WORD const m_iin = n  \

#define	AfxAeonIID(i)		CGuid(i::m_iit, i::m_iin)

#define	AfxIsInterface(r, i)	((CGuid const &) r).IsAeon(i::m_iit, i::m_iin)

#define	AfxIsUnknown(r)		((CGuid const &) r).IsUnknown()

//////////////////////////////////////////////////////////////////////////
//
// COM Helper Macros
//

#define StdQueryUnknown(i)	*ppObject = NULL;			\
				if( AfxIsUnknown(riid) ) {		\
				AtomicIncrement(&m_uRefs);		\
				*ppObject = (i *) this;			\
				return S_OK;				\
				}					\

#define StdQueryInterface(i)	if( AfxIsInterface(riid, i) ) {		\
				AtomicIncrement(&m_uRefs);		\
				*ppObject = (i *) this;			\
				return S_OK;				\
				}					\

#define	StdSetRef()		m_uRefs = 1;				\

#define	StdAddRef()		return AtomicIncrement(&m_uRefs);	\

#define	StdRelease()		if( !AtomicDecrement(&m_uRefs) ) {	\
				delete this;				\
				return 0;				\
				}					\
				return m_uRefs;				\

#define AfxAddRef(p)		BEGIN {					\
				if( (p) ) (p)->AddRef();		\
				} END					\

#define AfxRelease(p)		BEGIN {					\
				if( (p) ) (p)->Release();		\
				} END					\

//////////////////////////////////////////////////////////////////////////
//
// Array Helpers
//

template <typename type> void ArrayZero(type *p, UINT n)
{
	#if defined(_M_ARM)

	if( sizeof(type) == 4 ) {

		PDWORD pp = PDWORD(p);

		if( n >= 8 ) {

			int nq = n / 8;

			ASM(	"	mov	r0,  #0		\n"
				"	mov	r1,  #0		\n"
				"	mov	r2,  #0		\n"
				"	mov	r3,  #0		\n"
				"	mov	r4,  #0		\n"
				"	mov	r5,  #0		\n"
				"	mov	r6,  #0		\n"
				"	mov	r7,  #0		\n"
				"1:	stmia	%0!, {r0-r7}	\n"
				"	subs	%1,  #1		\n"
				"	bne	1b		\n"
				: "+r"(pp)
				: "r" (nq)
				: "r0", "r1", "r2", "r3",
				  "r4", "r5", "r6", "r7"
				);

			n = n % 8;
			}

		while( n-- ) {

			*pp++ = 0;
			}

		return;
		}

	#endif

	memset(p, 0, sizeof(type) * n);
	}

template <typename type> void ArrayCopy(type *d, type const *s, UINT n)
{
	// I think the new assembler memcpy is faster...

	#if 0 && defined(_M_ARM)

	if( sizeof(type) == 4 ) {

		PDWORD pd = PDWORD(d);

		PDWORD ps = PDWORD(s);

		if( n >= 8 ) {

			int nq = n / 8;

			ASM(	"1:	ldmia	%0!, {r0-r7}	\n"
				"	stmia	%1!, {r0-r7}	\n"
				"	subs	%2,  #1		\n"
				"	bne	1b		\n"
				: "+r"(ps), "+r"(pd)
				: "r" (nq)
				: "r0", "r1", "r2", "r3",
				  "r4", "r5", "r6", "r7"
				);

			n = n % 8;
			}

		while( n-- ) {

			*pd++ = *ps++;
			}

		return;
		}

	#endif

	memcpy(d, s, sizeof(type) * n);
	}

template <typename type> type * ArrayAlloc(type const *d, UINT n)
{
	return (type *) malloc(sizeof(type) * n);
	}

template <typename type> type * ArrayReAlloc(type *d, UINT n)
{
	return (type *) realloc(d, sizeof(type) * n);
	}

// End of File

#endif
