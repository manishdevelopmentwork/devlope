
#include "intern.hpp"

#include "imoload.hpp"

//////////////////////////////////////////////////////////////////////////
//
// IMOKGEN Driver
//

// Instantiator

INSTANTIATE(CIMOLoaderPortDriver);

// Constructor

CIMOLoaderPortDriver::CIMOLoaderPortDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_ErrorCode	= 0;
	}

// Destructor

CIMOLoaderPortDriver::~CIMOLoaderPortDriver(void)
{
	}

// Configuration

void MCALL CIMOLoaderPortDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIMOLoaderPortDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CIMOLoaderPortDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIMOLoaderPortDriver::Open(void)
{	
	}

// Device

CCODE MCALL CIMOLoaderPortDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	PCBYTE pData = pDevice->GetConfig();

	if( GetWord(pData) == 0x1234 ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR | CCODE_HARD; 
	}

CCODE MCALL CIMOLoaderPortDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIMOLoaderPortDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = OPD;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIMOLoaderPortDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{

	if( Addr.a.m_Table == ERRCODE ) {
		*pData = m_ErrorCode;
		return 1;
		}

	UINT uReturnCount;

	uReturnCount = GetMaxCount( Addr, uCount );

	ConfigureRead( Addr, uReturnCount );
			
	if( Transact() ) {

		return GetResponse( pData, Addr, uReturnCount );
		}

	m_ErrorCode = (DWORD(Addr.a.m_Offset) << 16) + 1;

	return CCODE_ERROR;
	}

CCODE MCALL CIMOLoaderPortDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{

	if( Addr.a.m_Table == ERRCODE ) {

		m_ErrorCode = *pData;

		return 1;
		}

	UINT uReturnCount;

	uReturnCount = GetMaxCount( Addr, uCount );

	ConfigureWrite( Addr, uReturnCount, pData );

	AddWriteData( Addr, pData, uReturnCount );

	if( Transact() ) {

		return uReturnCount;
		}

	return 1;
	}

// Frame Building

void CIMOLoaderPortDriver::ConfigureRead( AREF Addr, UINT uCount )
{
	StartFrame( 'r' ); // STX "rM"

	PutAddress( Addr );

	PutCount( Addr, uCount );
 	}

void CIMOLoaderPortDriver::ConfigureWrite( AREF Addr, UINT uCount, PDWORD pData )
{
	switch ( Addr.a.m_Type ) {

		case addrBitAsBit:

			StartFrame( pData[0] ? 'o' : 'n' );

			PutAddress( Addr );

			AddData( 0, 0x10 ); // dummy byte

			break;

		case addrWordAsWord:

			StartFrame( 'w' );

			PutAddress( Addr );

			PutCount( Addr, uCount );

			break;
		}
 	}

void CIMOLoaderPortDriver::StartFrame( BYTE bOpcode )
{
	m_bCheck = 0;

	m_bTx[0] = 2;

	m_uPtr = 1;

	AddByte( bOpcode );

	AddByte( 'M' );
	}

void CIMOLoaderPortDriver::EndFrame( void )
{
	AddData( m_bCheck, 0x10 );

	AddByte(3);
	}

void CIMOLoaderPortDriver::PutAddress( AREF Addr )
{
	UINT uOffset = 0;

	switch( Addr.a.m_Table ) {

		case OPM:
			uOffset = 2*(Addr.a.m_Offset>>4) + (AM0<<8);
			break;

		case OPP:
			uOffset = 2*(Addr.a.m_Offset>>4) + (AP0<<8);
			break;

		case OPK:
			uOffset = 2*(Addr.a.m_Offset>>4) + (AK0<<8);
			break;

		case OPL:
			uOffset = 2*(Addr.a.m_Offset>>4) + (AL0<<8);
			break;

		case OPF:
			uOffset = 2*(Addr.a.m_Offset>>4) + (AF0<<8);
			break;

		case OPD:
			uOffset = Addr.a.m_Offset*2 + (AD0<<8);
			break;

		case OPTV:
			uOffset = Addr.a.m_Offset*2 + (ATV0<<8);
			break;

		case OPTB:
			uOffset = (Addr.a.m_Offset/8) + (ATB0<<8);
			break;

		case OPCV:
			uOffset = Addr.a.m_Offset*2 + (ACV0<<8);
			break;

		case OPCB:
			uOffset = (Addr.a.m_Offset/8) + (ACB0<<8);
			break;
		}

	AddData( SwapLoWordBytes(uOffset), 0x1000 );
	}

void CIMOLoaderPortDriver::PutCount( AREF Addr, UINT uCount )
{
	m_uSendCount = Addr.a.m_Type == addrBitAsBit ? ( uCount + 7 + (Addr.a.m_Offset%16) ) / 8 : uCount*2;

	AddData( m_uSendCount, 0x1000 ); //nnnn
	}

void CIMOLoaderPortDriver::AddWriteData(AREF Addr, PDWORD pData, UINT uCount )
{
	UINT i;
	UINT uMask = 1;
	UINT uData = 0;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			if( Addr.a.m_Table < HIOPS ) {

				uMask = ( 1 << (Addr.a.m_Offset % 16) );

				uData = ( m_bTx[1] == 'o' ) ? uMask : uMask ^ 0xFFFF;

				AddData( SwapLoWordBytes(LOWORD(uData)), 0x1000 );
				}

			else {

				AddData( m_bTx[1] == 'o' ? 0x0100 : 0xFEFF, 0x1000 );
				}
			return;

		case addrWordAsWord:

			for( i = 0; i < uCount; i++ ) {

				AddData( SwapLoWordBytes( LOWORD(pData[i]) ), 0x1000 );
				}

			break;
		}
	} 

// Data Transfer
	
BOOL CIMOLoaderPortDriver::Transact(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
				
	return GetReply();
	}
	
BOOL CIMOLoaderPortDriver::GetReply(void)
{	
	UINT uPtr   = 0;

	UINT uState = 0;

	BYTE bCheck = 0;

	SetTimer(1000);

	while( GetTimer() ) {

		WORD wData = Get();
		
		if( wData == LOWORD(NOTHING) ) {
		
			Sleep(20);
			
			continue;
			}

		if( uPtr >= sizeof( m_bRx ) ) {

			return FALSE;
			}

		BYTE bData = LOBYTE(wData);

		m_bRx[uPtr++] = bData;

		switch (uState) {

			case 0:
				if( bData == 6 ) {

					uPtr = 0;

					uState = 1;
					}
				break;

			case 1:
				if( bData == 4 ) {

					bCheck = 0;

					for( UINT i = 0; i < uPtr-3; i++ ) {

						bCheck += m_bRx[i];
						}

					if(	m_pHex[bCheck/16] == m_bRx[uPtr-3] &&
						m_pHex[bCheck%16] == m_bRx[uPtr-2]
						) {

						m_uLastByte = uPtr-4;

						return TRUE;
						}
					}
				break;

			default:
				break;
			}
		}
		
	return FALSE;
	}

// Response Handlers

CCODE CIMOLoaderPortDriver::GetResponse( PDWORD pData, AREF Addr, UINT uReturnCount )
{
	if ( m_bRx[0] != 'r' ) return CCODE_ERROR;

	switch ( Addr.a.m_Type ) {

		case addrBitAsBit:

			GetBitResponse( pData, Addr, &uReturnCount );
			break;

		case addrWordAsWord:

			GetWordResponse( pData, Addr, &uReturnCount );
			break;
		}

	return uReturnCount;
	}

BOOL CIMOLoaderPortDriver::GetBitResponse(PDWORD pData, AREF Addr, UINT * pReturnCount)
{
	UINT uPtr = 1;
	UINT uData = 0;
	BYTE uMask = 1;

	uData = GetHex( uPtr, 2 );

	if( (Addr.a.m_Offset % 16) > 7 ) { // Desired data starts in 2nd byte

		uPtr += 2;

		uData = GetHex( uPtr, 2 );
		}

	uMask <<= (Addr.a.m_Offset % 8);

	for( UINT i = 0; i < *pReturnCount; i++ ) {

		if( uPtr > m_uLastByte ) { // ran out of data

			*pReturnCount = i;

			return TRUE;
			}

		pData[i] = DWORD( (uData & uMask) ? TRUE : FALSE);

		if( !(uMask <<= 1) ) {

			uPtr += 2;

			uData = GetHex( uPtr, 2 );

			uMask = 1;
			}
		}

	return TRUE;
	}

BOOL CIMOLoaderPortDriver::GetWordResponse(PDWORD pData, AREF Addr, UINT * pReturnCount)
{
	UINT uData = 0;
	UINT uPtr = 1;

	for( UINT i = 0; i < *pReturnCount; i++ ) {

		if( uPtr > m_uLastByte ) { // ran out of data

			*pReturnCount = i;

			return TRUE;
			}

		uData = SwapLoWordBytes( GetHex( uPtr, 4 ) );

		pData[i] = MAKELONG( uData, uData & 0x8000 ? 0xFFFF : 0 );

		uPtr += 4;
		}

	return TRUE;
	}

// Port Access

void CIMOLoaderPortDriver::Put(void)
{
	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT CIMOLoaderPortDriver::Get( void )
{
	UINT uData;

	uData = m_pData->Read(100);

	return uData;
	}

// Helpers

void CIMOLoaderPortDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck += bData;
	}
	
void CIMOLoaderPortDriver::AddData(UINT uData, UINT uMask)
{
	while( uMask ) {
	
		AddByte(m_pHex[(uData / uMask) % 16]);
		
		uMask >>= 4;
		}
	}

UINT CIMOLoaderPortDriver::GetHex(UINT p, UINT n)
{
	WORD d = 0;
	
	while( n-- ) {
	
		char c = char(m_bRx[p++]);

		if( c >= 'A' && c <= 'F' )
			d = (16 * d) + (c - '7');

		if( c >= 'a' && c <= 'f' )
			d = (16 * d) + (c - 'W');

		if( c >= '0' && c <= '9' )
			d = (16 * d) + (c - '0');
		}

	return d;
	}

UINT CIMOLoaderPortDriver::GetMaxCount( AREF Addr, UINT uCount )
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			MakeMin( uCount, UINT(16-(Addr.a.m_Offset%16)) );
			break;

		case addrWordAsWord:
			MakeMin( uCount, 8 );
			break;
		}

	return uCount;
	}

UINT CIMOLoaderPortDriver::SwapLoWordBytes( UINT uData )
{
	UINT u = UINT( HIBYTE(LOWORD(uData) ) );

	return ( u | ( LOBYTE(LOWORD(uData) ) << 8 ) );
	}

// End of File
