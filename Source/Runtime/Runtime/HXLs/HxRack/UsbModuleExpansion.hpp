
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbModuleExpansion_HPP

#define	INCLUDE_UsbModuleExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Rack/Module Expansion Object
//

class CUsbModuleExpansion : public CUsbExpansion
{
	public:
		// Constructor
		CUsbModuleExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT METHOD GetName(void);
		UINT  METHOD GetClass(void);
		UINT  METHOD GetPower(void);
	};

// End of File

#endif


