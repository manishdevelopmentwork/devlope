
#include "Intern.hpp"

#include "UniPtr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/_YKgE

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Unicode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

// Constructors

CUniPtr::CUniPtr(CUnicode const &That)
{
	AfxAssert(FALSE);

	m_pText = NULL;
	}

UINT CUniPtr::GetHashValue(void) const
{
	UINT uHash = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {
	
		uHash = (uHash << 5) + uHash;
		
		uHash = uHash + m_pText[n];
		}
		
	return uHash;
	}

// Read-Only Access

WCHAR CUniPtr::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : WCHAR(0);
	}

// Array Conversion

UINT CUniPtr::GetCharCodes(CWordArray &Code)
{
	UINT uLen = GetLength();

	Code.Empty();

	Code.Expand(uLen);

	for( UINT n = 0; n < uLen; n++ ) {

		Code.Append(m_pText[n]);
		}

	return uLen;
	}

// Comparison Functions

int CUniPtr::GetScore(PCUTF pText) const
{
	if( wstricmp(m_pText, pText) ) {
	
		UINT n = wstrlen(pText);
		
		if( wstrnicmp(m_pText, pText, n) ) {

			return 0;
			}
			
		return n;
		}
		
	return 10000;
	}

// Comparison Helper

int AfxCompare(CUniPtr const &a, CUniPtr const &b)
{
	return wstricmp(a.m_pText, b.m_pText);
	}

// Substring Extraction

CUnicode CUniPtr::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);

	return CUnicode(m_pText, uCount);
	}

CUnicode CUniPtr::Right(UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uCount, uLen);

	PCUTF pSrc = m_pText + uLen - uCount;

	return CUnicode(pSrc, uCount);
	}

CUnicode CUniPtr::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	MakeMin(uCount, uLen - uPos);

	PCUTF pSrc = m_pText + uPos;

	return CUnicode(pSrc, uCount);
	}

CUnicode CUniPtr::Mid(UINT uPos) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	PCUTF pSrc = m_pText + uPos;

	return CUnicode(pSrc, uLen - uPos);
	}

// Case Conversion

CUnicode CUniPtr::ToUpper(void) const
{
	CUnicode Result(ThisObject);

	Result.MakeUpper();

	return Result;
	}

CUnicode CUniPtr::ToLower(void) const
{
	CUnicode Result(ThisObject);

	Result.MakeLower();

	return Result;
	}

// Searching

UINT CUniPtr::Find(WCHAR cChar) const
{
	AfxAssert(cChar);
	
	PUTF pFind = wstrchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}

UINT CUniPtr::Find(WCHAR cChar, WCHAR cQuote) const
{
	AfxAssert(cChar);

	AfxAssert(cQuote);

	int s = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		WCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cQuote ) {

			s = !s;
			}
		}
	
	return NOTHING;
	}

UINT CUniPtr::Find(WCHAR cChar, WCHAR cOpen, WCHAR cClose) const
{
	AfxAssert(cChar);

	AfxAssert(cOpen);

	AfxAssert(cClose);

	int s = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		WCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cOpen ) {

			s++;
			}

		if( c == cClose ) {

			s--;
			}
		}
	
	return NOTHING;
	}

UINT CUniPtr::Find(WCHAR cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());
	
	PUTF pFind = wstrchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CUniPtr::Find(WCHAR cChar, UINT uPos, WCHAR cQuote) const
{
	AfxAssert(cChar);

	AfxAssert(cQuote);

	AfxAssert(uPos <= GetLength());

	int s = 0;

	for( UINT n = uPos; m_pText[n]; n++ ) {

		WCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cQuote ) {

			s = !s;
			}
		}
	
	return NOTHING;
	}

UINT CUniPtr::Find(WCHAR cChar, UINT uPos, WCHAR cOpen, WCHAR cClose) const
{
	AfxAssert(cChar);

	AfxAssert(cOpen);

	AfxAssert(cClose);

	AfxAssert(uPos <= GetLength());

	int s = 0;

	for( UINT n = uPos; m_pText[n]; n++ ) {

		WCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cOpen ) {

			s++;
			}

		if( c == cClose ) {

			s--;
			}
		}
	
	return NOTHING;
	}

UINT CUniPtr::FindRev(WCHAR cChar) const
{
	AfxAssert(cChar);

	PUTF pFind = wstrrchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CUniPtr::FindRev(WCHAR cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());

	PUTF pFind = wstrrchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CUniPtr::Find(PCUTF pText) const
{
	AfxAssertStringPtr(pText);
			
	PUTF pFind = wstrstr(m_pText, pText);
		
	return pFind ? pFind - PUTF(m_pText) : NOTHING;
	}
	
UINT CUniPtr::Find(PCUTF pText, UINT uPos) const
{
	AfxAssertStringPtr(pText);
			
	AfxAssert(uPos <= GetLength());

	PUTF pFind = wstrstr(m_pText + uPos, pText);
		
	return pFind ? pFind - PUTF(m_pText) : NOTHING;
	}
	
UINT CUniPtr::FindNot(PCUTF pList) const
{
	AfxAssertStringPtr(pList);

	UINT uFind = wstrspn(m_pText, pList);
	
	return uFind;
	}

UINT CUniPtr::FindNot(PCUTF pList, UINT uPos) const
{
	AfxAssertStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = wstrspn(m_pText + uPos, pList);
	
	return uFind ? uFind + uPos : 0;
	}

UINT CUniPtr::FindOne(PCUTF pList) const
{
	AfxAssertStringPtr(pList);

	UINT uFind = wstrcspn(m_pText, pList);
	
	return m_pText[uFind] ? uFind : NOTHING;
	}

UINT CUniPtr::FindOne(PCUTF pList, UINT uPos) const
{
	AfxAssertStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = uPos + wstrcspn(m_pText + uPos, pList);
	
	return m_pText[uFind] ? uFind : NOTHING;
}

// Searching

UINT CUniPtr::Search(PCUTF pText, UINT uMethod) const
{
	return Search(pText, 0, uMethod);
	}

UINT CUniPtr::Search(PCUTF pText, UINT uFrom, UINT uMethod) const
{
	UINT uFind;

	switch( LOBYTE(uMethod) ) {

		case searchIsEqualTo:

			if( uMethod & searchMatchCase ) {

				if( !wstrcmp(pText, m_pText) ) {

					return 0;
					}

				return NOTHING;
				}

			if( !wstricmp(pText, m_pText) ) {

				return 0;
				}

			return NOTHING;

		case searchStartsWith:

			if( uMethod & searchMatchCase ) {

				if( !wstrncmp(pText, m_pText, wstrlen(pText)) ) {

					return 0;
					}

				return NOTHING;
				}

			if( !wstrnicmp(pText, m_pText, wstrlen(pText)) ) {

				return 0;
				}

			return NOTHING;

		case searchContains:

			uFind = wstrlen(pText);

			for(;;) {

				PCUTF pFind;

				if( uMethod & searchMatchCase ) {

					pFind = wstrstr (m_pText + uFrom, pText);
					}
				else
					pFind = wstristr(m_pText + uFrom, pText);

				if( pFind ) {

					if( uMethod & searchWholeWords ) {

						if( pFind > m_pText + uFrom && wisalnum(pFind[-1]) ) {

							uFrom = pFind - m_pText + uFind;

							continue;
							}

						if( pFind[uFind] && wisalnum(pFind[uFind]) ) {

							uFrom = pFind - m_pText + uFind;

							continue;
							}
						}

					return pFind - m_pText;
					}

				return NOTHING;
				}

			break;
		}

	return NOTHING;
	}

// Partials

BOOL CUniPtr::StartsWith(PCUTF pText) const
{
	UINT c1 = wstrlen(m_pText);

	UINT c2 = wstrlen(pText);

	if( c1 >= c2 ) {

		return !wstrnicmp(m_pText, pText, c2);
		}

	return FALSE;
	}

BOOL CUniPtr::EndsWith(PCUTF pText) const
{
	UINT c1 = wstrlen(m_pText);

	UINT c2 = wstrlen(pText);

	if( c1 >= c2 ) {

		return !wstrnicmp(m_pText + c1 - c2, pText, c2);
		}

	return FALSE;
	}

// Counting

UINT CUniPtr::Count(WCHAR cChar) const
{
	UINT c = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cChar ) {

			c++;
			}
		}
		
	return c;
	}

// Diagnostics

void CUniPtr::AssertValid(void) const
{
	AfxAssertReadPtr(this, sizeof(ThisObject));
	
	AfxAssertStringPtr(m_pText);
	}

// End of File
