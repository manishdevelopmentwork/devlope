
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortRack, CCommsPort);

// Constructor

CCommsPortRack::CCommsPortRack(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceRackList))
{
	m_Binding  = bindRack;

	m_PortPhys = 3;

	m_PortLog  = 0;

	m_pSlots   = New CCommsSlotList;

	m_pDevices->SetItemClass(AfxRuntimeClass(CCommsDeviceRack));

	m_pSlots  ->SetItemClass(AfxRuntimeClass(CCommsPortSlot));
	}

// Operations

void CCommsPortRack::UpgradeSlots(void)
{
	for( INDEX Index = m_pSlots->GetHead(); !m_pSlots->Failed(Index); m_pSlots->GetNext(Index) ) {
 
		m_pSlots->GetItem(Index)->Upgrade();
		}

	MakeSlots();	
	}

void CCommsPortRack::MakeSlots(void)
{
	m_Order.Empty();

	CExpansionItem *pExpansion = ((CCommsManager *) GetParent(3))->m_pExpansion;

	MakeSlots(pExpansion->m_pRack);

	MakeSlots(pExpansion->m_pCascaded);

	MakeSlots(pExpansion->m_pTethered);
	}

void CCommsPortRack::RemapSlots(BOOL fForce)
{
	CExpansionItem *pExpansion = ((CCommsManager *) GetParent(3))->m_pExpansion;

	if( fForce || m_pSlots->GetItemCount() != (pExpansion->GetSlotCount() + pExpansion->GetRackCount()) ) {

		MakeSlots();

		INDEX Index = m_pSlots->GetHead(); 
		
		while( !m_pSlots->Failed(Index) ) {

			CCommsPortSlot *pSlot = m_pSlots->GetItem(Index);

			if( m_Order.Find(pSlot->GetIndex()) == NOTHING ) {

				m_pSlots->GetNext(Index);

				m_pSlots->DeleteItem(pSlot);

				continue;
				}

			m_pSlots->GetNext(Index);
			}
				
		m_pSlots->SetOrder(m_Order);
		}
	}

// UI Creation

CViewWnd * CCommsPortRack::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		if( TRUE ) {

			CLASS Class = AfxNamedClass(L"CFixedRackNavTreeWnd");

			return AfxNewObject(CViewWnd, Class);
			}
		else {
			CLASS Class = AfxNamedClass(L"CRackNavTreeWnd");

			return AfxNewObject(CViewWnd, Class);
			}
		}

	return NULL;
	}

// Driver Creation

ICommsDriver * CCommsPortRack::CreateDriver(UINT uID)
{
	if( m_DriverID == 0xFE01 ) {

		return New CRackDriver;
		}

	return CCommsPort::CreateDriver(uID);
	}

// Item Naming

BOOL CCommsPortRack::IsHumanRoot(void) const
{
	return TRUE;
	}

CString CCommsPortRack::GetHumanName(void) const
{
	if( GetDatabase()->GetSystemItem()->GetModel().StartsWith(L"da") ) {

		return CString(IDS_MODULES);
		}
	else {
		return CString(IDS_IO_MODULES);
		}
	}

CString CCommsPortRack::GetItemOrdinal(void) const
{
	return L"";
	}

// Persistance

void CCommsPortRack::Init(void)
{
	CCommsPort::Init();

	m_DriverID = 0xFE01;

	CheckDriver();

	CCommsDeviceRack *pDev = New CCommsDeviceRack;

	m_pDevices->AppendItem(pDev);

	pDev->SetClass(AfxNamedClass(L"CMasterModule"));

	pDev->SetName (L"Master");
	}

void CCommsPortRack::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if( Name == L"Slots" || Name == L"TetherSlots" ) {

			Tree.GetCollect();

			m_pSlots->SetParent(this);

			m_pSlots->Load(Tree);

			Tree.EndCollect();

			continue;
			}

		if( Name == L"Devices" || Name == L"TetherDevices" ) {

			Tree.GetCollect();

			m_pDevices->SetParent(this);

			m_pDevices->Load(Tree);

			Tree.EndCollect();

			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

void CCommsPortRack::PostLoad(void)
{
	CCommsPort::PostLoad();

	INDEX i = m_pDevices->GetHead();

	UINT  n = 0;

	while( !m_pDevices->Failed(i) ) {

		CCommsDeviceRack *pDev = (CCommsDeviceRack *) m_pDevices->GetItem(i);

		if( !pDev->m_pModule->IsKindOf(AfxNamedClass(L"CMasterModule")) ) {

			if( !pDev->m_pModule->m_Drop ) {

				CCommsPortSlot *pSlot = m_pSlots->GetItem(n-1);

				if( pSlot ) {

					if( !pSlot->GetRack() ) {

						pSlot->SetDevice(pDev);
						}
					}
				}
			}
			
		m_pDevices->GetNext(i);

		n++;
		}
	}

void CCommsPortRack::Save(CTreeFile &File)
{
	CCommsPort::Save(File);

	if( TRUE ) {

		UINT uPad = File.GetPadding();

		File.PutCollect(L"Slots");

		m_pSlots->SaveFixed(File);

		File.EndCollect();

		File.SetPadding(uPad);
		}

	if( TRUE ) {

		UINT uPad = File.GetPadding();

		File.PutCollect(L"TetherSlots");

		m_pSlots->SaveTether(File);

		File.EndCollect();

		File.SetPadding(uPad);
		}

	if( TRUE ) {

		UINT uPad = File.GetPadding();

		File.PutCollect(L"Devices");

		((CCommsDeviceRackList *) m_pDevices)->SaveFixed(File);

		File.EndCollect();

		File.SetPadding(uPad);
		}

	if( TRUE ) {

		UINT uPad = File.GetPadding();

		File.PutCollect(L"TetherDevices");

		((CCommsDeviceRackList *) m_pDevices)->SaveTether(File);

		File.EndCollect();

		File.SetPadding(uPad);
		}
	}

// Conversion

void CCommsPortRack::PostConvert(void)
{
	MigrateSlots();

	CStringArray List(L"CMasterModule");

	LoadModules(List);

	UINT uCount = 0;

	INDEX n = m_pDevices->GetHead();

	while( !m_pDevices->Failed(n) ) {

		CCommsDeviceRack *pDev  = (CCommsDeviceRack *) m_pDevices->GetItem(n);

		CString           Name  = pDev->m_pModule->GetClassName();

		CString           Conv  = pDev->m_pModule->GetClassConv();

		CCommsPortSlot   *pSlot = m_pSlots->FindItem(pDev);  

		if( pDev->m_pModule->IsKindOf(AfxNamedClass(L"CMasterModule")) ) {

			m_pDevices->GetNext(n);

			continue;
			}

		if( pSlot ) {

			if( List.Find(Name) < NOTHING ) {

				m_pDevices->GetNext(n);
			
				continue;
				}

			if( List.Find(Conv) < NOTHING ) {

				pDev->PostConvert(Conv);
			
				m_pDevices->GetNext(n);

				uCount++;

				continue;
				}

			if( pSlot ) {

				pSlot->SetDevice(NULL);
				}
			}

		INDEX d = n;

		m_pDevices->GetNext(n);

		m_pDevices->DeleteItem(d);
		}
	}

// Download Support

BOOL CCommsPortRack::MakeInitData(CInitData &Init)
{
	Init.AddByte(10);

	CCommsPort::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCommsPortRack::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_AddCollect(Slots);

	Meta_SetName((IDS_MODULES));
	}

// Implementation

void CCommsPortRack::MakeSlots(COptionCardList *pItem)
{
	for( INDEX n = pItem->GetHead(); !pItem->Failed(n); pItem->GetNext(n) ) {

		COptionCardItem *pCard = pItem->GetItem(n);
		
		MakeSlots(pCard);
		}
	}

void CCommsPortRack::MakeSlots(COptionCardRackItem *pItem)
{
	CCommsPortSlot *pSlot = m_pSlots->FindRack(pItem->m_Number);

	if( !pSlot ) {

		pSlot = New CCommsPortSlot;

		m_pSlots->AppendItem(pSlot);
		}

	pSlot->SetRack(pItem);
			
	m_Order.Append(pSlot->GetIndex());

	MakeSlots(pItem->m_pSlots);

	for( INDEX i = pItem->m_pRacks->GetHead(); !pItem->m_pRacks->Failed(i); pItem->m_pRacks->GetNext(i) ) {
		
		COptionCardRackItem *pRack = pItem->m_pRacks->GetItem(i);

		MakeSlots(pRack);
		}			
	}

void CCommsPortRack::MakeSlots(COptionCardItem *pItem)
{
	CCommsPortSlot *pSlot = m_pSlots->FindSlot(pItem->m_Number);

	if( !pSlot ) {
		
		pSlot = New CCommsPortSlot;

		m_pSlots->AppendItem(pSlot);
		}

	pSlot->SetPort(pItem);

	m_Order.Append(pSlot->GetIndex());
	}

void CCommsPortRack::MigrateSlots(void)
{
	CArray <HGLOBAL> Data;

	UINT uSlots = m_pSlots->GetIndexCount();

	BOOL fAdjust = FALSE;

	for( UINT uSlot = 0; uSlot < uSlots; uSlot++  ) {

		if( GetDatabase()->HasFlag(L"USBRack") ) {

			CCommsPortSlot *pSlot = m_pSlots->GetItem(uSlot);

			if( pSlot ) {

				if( !fAdjust && pSlot->GetRack() ) {

					fAdjust = TRUE;

					AdjustSlotData(Data, uSlot);
					}

				HGLOBAL hData = pSlot->TakeSnapshot();

				Data.Append(hData);
				}

			continue;
			}

		if( GetDatabase()->HasFlag(L"HasRack") ) {
			
			CCommsPortSlot *pSlot = m_pSlots->GetItem(uSlot);

			if( pSlot ) {

				if( pSlot->m_Rack == NOTHING ) {

					HGLOBAL hData = pSlot->TakeSnapshot();

					Data.Append(hData);
					}
				}

			AfxTouch(pSlot);
			}
		}

	m_pSlots->DeleteAllItems(TRUE);
	
	MakeSlots();

	uSlots = m_pSlots->GetIndexCount();

	for( UINT uSlot = 0; uSlot < uSlots; uSlot++  ){

		CCommsPortSlot *pSlot = m_pSlots->GetItem(uSlot);

		if( pSlot ) {

			HGLOBAL hData = NULL;

			if( Data.GetCount() ) {

				hData = Data[0];

				Data.Remove(0);
				}
			
			if( hData ) {

				//UINT Slot = pSlot->m_Slot;

				pSlot->LoadSnapshot(hData);

				//pSlot->m_Slot = Slot;

				pSlot->PostConvert();
												
				GlobalFree(hData);
				}
			}
		}

	while( Data.GetCount() ) {

		GlobalFree(Data[0]);

		Data.Remove(0);
		}
	}

void CCommsPortRack::AdjustSlotData(CArray <HGLOBAL> &Data, UINT uSlot)
{
	CExpansionItem *pExpansion = ((CCommsManager *) GetParent(3))->m_pExpansion;

	INT nAdjust = pExpansion->m_pRack->GetItemCount() - uSlot;

	BOOL fRem   = nAdjust < 0;

	for( INT n = nAdjust; n != 0; fRem ? n++ : n-- ) {

		fRem ? Data.Remove(uSlot) : Data.Insert(uSlot, NULL);
		}
	}

void CCommsPortRack::LoadModules(CStringArray &List, CModel *pType, UINT uCount)
{
	for( UINT n = 0; n < uCount; n++ ) {		

		CString Class;

		CString Name  = pType[n].m_pName;

		if( C3OemFeature(Name, TRUE) ) {

			C3OemStrings(Name);

			Class = CPrintf(L"C%sModule", pType[n].m_pClass);

			List.Append(Class);
			}
		}
	}

void CCommsPortRack::LoadModules(CStringArray &List)
{
	static CModel Modules[] = {

	{	L"GMDIO14",	L"GMDIO14"	},
	{	L"GMINI8",	L"GMINI8"	},
	{	L"GMINV8",	L"GMINV8"	},
	{	L"GMOUT4",	L"GMOUT4"	},
	{	L"GMPID1",	L"GMPID1"	},
	{	L"GMPID2",	L"GMPID2"	},
	{	L"GMRTD6",	L"GMRTD6"	},
	{	L"GMTC8",	L"GMTC8"	},
	{	L"GMUIN4",	L"GMUIN4"	},
	{	L"GMSG1",	L"GMSG"		},

	};
		
	LoadModules(List, Modules, elements(Modules));
	}

// Property Filters

BOOL CCommsPortRack::SaveProp(CString const &Tag) const
{
	if( Tag == L"Slots" ) {
		
		return FALSE;
		}

	if( Tag == L"Devices" ) {
		
		return FALSE;
		}

	return CCommsPort::SaveProp(Tag);
	}

// End of File
