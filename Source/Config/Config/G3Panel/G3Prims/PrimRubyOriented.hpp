
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyOriented_HPP
	
#define	INCLUDE_PrimRubyOriented_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Oriented Primitive
//

class CPrimRubyOriented : public CPrimRubyGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyOriented(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Data Members
		UINT m_Orient;
		UINT m_Reflect;
		UINT m_Rotate;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);
		void MakeFigure(R2 const &Rect);

		// Path Generation
		virtual void MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
