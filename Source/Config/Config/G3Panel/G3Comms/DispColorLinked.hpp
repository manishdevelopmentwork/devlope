
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorLinked_HPP

#define INCLUDE_DispColorLinked_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispColor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linked Display Color
//

class DLLNOT CDispColorLinked : public CDispColor
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispColorLinked(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Color Access
		DWORD GetColorPair(DWORD Data, UINT Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem *m_pLink;

	protected:
		// Data Members
		CDispColor *m_pColor;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL TestColor(void);
		BOOL FindColor(void);
	};

// End of File

#endif
