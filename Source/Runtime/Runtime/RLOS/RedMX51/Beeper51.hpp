
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_iMX51_Beeper51_HPP
	
#define	INCLUDE_iMX51_Beeper51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPwm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Beeper
//

class CBeeper51 : public IBeeper, public IEventSink
{
	public:
		// Constructor
		CBeeper51(CPwm51 *pPwm);

		// Destructor
		~CBeeper51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IBeeper
		void METHOD Beep(UINT uBeep);
		void METHOD Beep(UINT uFreq, UINT uTime);
		void METHOD BeepOff(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Static Data
		static UINT m_Table[];

		// Data Members
		ULONG        m_uRefs;
		CPwm51     * m_pPwm;
		ITimer     * m_pTimer;
		UINT	     m_uTime;
	};

// End of File

#endif
