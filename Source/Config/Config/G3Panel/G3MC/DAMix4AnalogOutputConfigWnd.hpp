
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4AnalogOutputConfigWnd_HPP

#define INCLUDE_DAMix4AnalogOutputConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMix4AnalogOutputConfig;

class CDAMix4Module;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Output Config Window
//

class CDAMix4AnalogOutputConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix4AnalogOutputConfigWnd(UINT uPage);

protected:
	// Data Members
	CDAMix4AnalogOutputConfig * m_pItem;
	UINT		            m_uPage;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Implementation
	void AddOutputs(void);

	// Data Access
	UINT GetInteger(CMetaItem *pItem, CString Tag);
	void PutInteger(CMetaItem *pItem, CString Tag, UINT Data);

	// Enabling
	void DoEnables(void);
};

// End of File

#endif
