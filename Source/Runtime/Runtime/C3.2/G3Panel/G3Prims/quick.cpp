
#include "intern.hpp"

#include "quick.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Quick Plot
//

// Constructor

CPrimQuickPlot::CPrimQuickPlot(void)
{
	m_uTag   = 0xFFFF;
	
	m_pMin   = NULL;
	
	m_pMax   = NULL;
	
	m_Align  = 0;
	
	m_Pad    = 0;

	m_pPen   = New CPrimColor(naPen);

	m_pQuick = NULL;
	}

// Destructor

CPrimQuickPlot::~CPrimQuickPlot(void)
{
	delete m_pMin;
	
	delete m_pMax;

	delete m_pPen;
	}

// Initialization

void CPrimQuickPlot::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimQuickPlot", pData);

	CPrimLegacyFigure::Load(pData);

	m_uTag = GetWord(pData) & 0x7FFF;

	GetCoded(pData, m_pMin);

	GetCoded(pData, m_pMax);

	m_Align = GetByte(pData);

	m_Pad   = GetByte(pData);

	m_pPen->Load(pData);

	FindQuick();
	}

// Overridables

void CPrimQuickPlot::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimLegacyFigure::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;
			
			m_fChange = TRUE;			
			}

		if( m_pBack->IsNull() ) {

			if( m_fChange ) {

				Erase.Append(m_DrawRect);
				}
			}
		}
	}

void CPrimQuickPlot::DrawPrim(IGDI *pGDI)
{
	CPrimLegacyFigure::DrawPrim(pGDI);

	if( m_pQuick ) {

		CTagManager *pTags = CCommsSystem::m_pThis->m_pTags;

		pTags->LockQuickPlots();

		UINT uLimit = m_pQuick->GetPointLimit();

		UINT uCount = m_pQuick->GetPointCount();

		if( uCount && uLimit ) {

			C3REAL *pData = New C3REAL [ uCount ];

			m_pQuick->GetData(pData);

			pTags ->FreeQuickPlots();

			pGDI  ->SetPenFore(m_Ctx.m_Pen);

			R2 Rect = m_DrawRect;
	
			if( m_pEdge->m_Width ) {

				m_pEdge->AdjustRect(Rect);

				DeflateRect(Rect, 2, 2);
				}

			if( m_Pad ) {

				Rect.left   += 1;

				Rect.right  -= 2;

				Rect.top    += 1;

				Rect.bottom -= 2;
				}

			int cx = Rect.x2 - Rect.x1;

			int cy = Rect.y2 - Rect.y1;

			int px = -1;

			int np =  m_Align ? uLimit - uCount : 0;

			for( UINT n = 0; n < uCount; n++ ) {

				if( !IsNAN(pData[n]) ) {

					int x1 = Rect.x1 + (cx - 1) * np / (uLimit - 1);

					if( x1 > px ) {

						int y1 = Rect.y2 - 1 - Scale(pData[n], cy - 1);

						if( px >= 0 ) {

							pGDI->LineTo(x1, y1);
							}
						else
							pGDI->MoveTo(x1, y1);

						px = x1;
						}
					}
				else
					px = -1;

				np++;
				}

			delete [] pData;

			return;
			}

		pTags->FreeQuickPlots();
		}
	}

// Context Check

BOOL CPrimQuickPlot::CCtx::operator == (CCtx const &That) const
{
	return m_fAvail == That.m_fAvail &&
	       m_Seq    == That.m_Seq    &&
	       m_Min    == That.m_Min    &&
	       m_Max    == That.m_Max    &&
	       m_Pen    == That.m_Pen    ;;
	}

// Context Creation

void CPrimQuickPlot::FindCtx(CCtx &Ctx)
{
	if( m_pQuick ) {

		Ctx.m_fAvail = (IsItemAvail(m_pMin) && IsItemAvail(m_pMax));

		Ctx.m_Seq    = m_pQuick->GetSequence();

		Ctx.m_Min    = GetItemData(m_pMin, C3REAL(  0.0));

		Ctx.m_Max    = GetItemData(m_pMax, C3REAL(100.0));

		Ctx.m_Pen    = m_pPen->GetColor();

		return;
		}

	memset(&Ctx, 0, sizeof(Ctx));
	}

// Implementation

BOOL CPrimQuickPlot::FindQuick(void)
{
	CTagManager *pTags = CCommsSystem::m_pThis->m_pTags;

	CTag        *pTag  = pTags->m_pTags->GetItem(m_uTag);

	if( pTag ) {

		pTag->GetQuickPlot(m_pQuick);

		return TRUE;
		}

	return FALSE;
	}

int CPrimQuickPlot::Scale(C3REAL Data, int nSize)
{
	MakeMax(Data, m_Ctx.m_Min);

	MakeMin(Data, m_Ctx.m_Max);

	Data -= m_Ctx.m_Min;

	Data /= m_Ctx.m_Max - m_Ctx.m_Min;

	Data *= nSize;

	return int(Data);
	}

// End of File
