#!/bin/bash
#
# chkconfig: 2345 00 88
# description: syslog
#

. /etc/init.d/functions

RETVAL=0

case "$1" in
  
	start)
		opts=""

		if [ -e /vap/opt/crimson/config/syslog/vars ]
		then
			source /vap/opt/crimson/config/syslog/vars
		fi

  		echo -n "Starting system logger:"
		daemon syslogd -S -m 0 -s 1024 -b 1 -L $opts
		echo

  		echo -n "Starting kernel logger:"
		daemon klogd -c 1
		echo

  		RETVAL=0
  		;;
  
	stop)
		echo -n "Stopping kernel logger:"
		killproc klogd
		echo

		echo -n "Stopping system logger:"
		killproc syslogd
		echo

  		RETVAL=0
		;;
	
  	restart|reload)
		RESTART=1
		$0 stop
		$0 start
		RETVAL=$?
    	;;
    
	*)
		echo $"Usage: $0 {start|stop|restart}"
		exit 1

esac

exit $RETVAL
