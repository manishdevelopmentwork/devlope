
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConResWnd_HPP

#define INCLUDE_DevConResWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConPart;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Resource Window
//

class CDevConResWnd : public CStdTreeWnd, public IUpdate
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructors
	CDevConResWnd(void);

	// IUnknown
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
	ULONG   METHOD AddRef(void);
	ULONG   METHOD Release(void);

	// IUpdate
	HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

protected:
	// Value Types
	enum
	{
		valInteger = 1,
		valFloat   = 2,
		valString  = 3,
		valIp      = 4
	};

	// Data Members
	UINT          m_cfPerson;
	UINT	      m_cfCode[4];
	CDevConPart * m_pPerson;
	CJsonData   * m_pKeys;

	// Overridables
	void OnAttach(void);

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnPostCreate(void);
	void OnDestroy(void);
	void OnLoadTool(UINT uCode, CMenu &Menu);

	// Notification Handlers
	BOOL OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info);
	void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);
	void OnTreeDblClk(UINT uID, NMHDR &Info);

	// Data Object Construction
	BOOL MakeDataObject(IDataObject * &pData);

	// Drag Hooks
	DWORD FindDragAllowed(void);

	// Tree Loading
	void LoadTree(void);
	void LoadRoot(void);

	// Selection Hooks
	CString SaveSelState(void);
	void    LoadSelState(CString State);
};

// End of File

#endif
