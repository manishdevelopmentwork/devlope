
#include "Intern.hpp"

#include "ArpFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// ARP Frame Wrapper Class
//

// Operations

void CARPFrame::MakeReq(MACREF MacSrc, IPREF IpSrc, IPREF IpDest)
{
	m_HardwareType = HW_ETHERNET;

	m_ProtocolType = ET_IP;

	m_HardwareLen  = sizeof(MACADDR);

	m_ProtocolLen  = sizeof(IPADDR);

	m_Opcode       = ARP_REQUEST;

	m_IpSrc        = IpSrc;

	m_IpDest       = IpDest;

	m_MacSrc       = MacSrc;

	m_MacDest.MakeEmpty();
	}

void CARPFrame::MakeRep(MACREF MacSrc)
{
	CIpAddr IpSrc  = m_IpSrc;

	m_Opcode       = ARP_REPLY;

	m_IpSrc        = m_IpDest;

	m_IpDest       = IpSrc;

	m_MacDest      = m_MacSrc;
	
	m_MacSrc       = MacSrc;
	}

// End of File
