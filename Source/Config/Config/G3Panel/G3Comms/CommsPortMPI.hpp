
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortMPI_HPP

#define INCLUDE_CommsPortMPI_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MPI Comms Port
//

class DLLNOT CCommsPortMPI : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortMPI(void);

		// Attributes
		UINT GetPageType(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_ThisDrop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
