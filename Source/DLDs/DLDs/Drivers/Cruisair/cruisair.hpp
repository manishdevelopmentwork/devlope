
//////////////////////////////////////////////////////////////////////////
//
// Cruisair Driver
//

#define CRUISAIR_ID 0x4027

//////////////////////////////////////////////////////////////////////////
//
// Cruisair Driver
//

class CCruisairDriver : public CMasterDriver
{
	public:
		// Constructor
		CCruisairDriver(void);

		// Destructor
		~CCruisairDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT	m_uChiller;
			DWORD	m_dPing;
			};

		CContext *m_pCtx;

		// Data Members
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		UINT	m_uPtr;
		UINT	m_uTimeout;
		UINT	m_uChiller;
		UINT	m_uDisconnect;

		// Opcode Handlers
		void	Read(UINT uOpcode);
		void	Write(UINT uOpcode, DWORD dData);

		// Frame Building
		void	StartFrame(BYTE bOp);
		void	AddByte(BYTE bData);
		void	AddText(char * sText);

		// Transport Layer
		BOOL	Transact(BOOL fIsWakeup);
		void	Send(void);
		BOOL	GetReply(BOOL fIsWakeup);
		BOOL	CheckReply(void);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTimer);

		// Helpers
		BOOL	ModeCheck(void);
		BOOL	WakeUp(void);
		UINT	GetParameterNumber(UINT uTable, UINT uOffset, UINT uExtra);
		void	SetChillerNumber(UINT uTable, UINT uOffset, UINT uExtra);
		BOOL	ReadNoTransmit(UINT uTable, UINT uOffset, PDWORD pData);
		BOOL	IsWriteable(UINT uParam);
		void	GetBitData(AREF Addr, PDWORD pData);
	};

#define	AN	addrNamed
#define	SPH	addrNamed - 1	// Section Title

#define	BB	addrBitAsBit
#define	LL	addrLongAsLong

#define	MINWP	49	// minimum writeable parameter number
#define	MAXWP	85	// maximum writeable parameter number
#define	MINWC	256	// minimum writeable control number
#define	MAXWC	284	// maximum writeable control number

#define	RTNZEROS	409600000 // number ending in 4 hex 0's & 5 decimal 0's

#define	CGLOBAL	0
#define	ISLOCAL	2

// Selection ID's

// Chiller Status
#define	CSN	  0	// Current Stage number
#define	LFS	  1	// Loop Water flow switch
#define	SVN	  3	// Software Version #90-xx
#define	SIT	 16	// Common Seawater Inlet Temp COMMON
#define	SOT	 17	// Seawater Outlet Temp
#define	LST	 18	// Loop Supply Temp COMMON
#define	LRT	 19	// Loop Return Temp COMMON
#define	LFT	 20	// Loop Freeze Temp
#define	RLP	 21	// R22 Low Pressure
#define	RHP	 22	// R22 High Pressure
#define	LPR	 23	// Loop Pressure COMMON
#define	SPR	 24	// Seawater Pressure COMMON
#define	CCU	 25	// Compressor Current
#define	SPC	 26	// Seawater Pump Current
#define	LPC	 27	// Loop Pump Current
#define	ACV	 28	// AC Voltage
#define	CRT	 44	// Compressor run time
#define	RVT	 45	// Reversing Valve run time
#define	SPT	 46	// Seawater Pump run time
#define	LPT	 47	// Loop Pump run time
#define	HRT	 48	// Heater run time
#define	DLYT	153	// Delay Timer
#define	IMT	154	// Immersion Heater Temperature
#define	MODE	 86	// Operating Mode
#define	FDB	 87	// Fault Display Bits
#define	TGR	266	// Toggle Rotate On/Off
#define	ISN	267	// Increment Stage Number
#define	DSN	268	// Decrement Stage Number
#define	TGC	269	// Toggle Chiller On/Off
#define	C1LG	272	// Clear All Logs
#define	C1FH	274	// Clear Fault History
#define	C1IO	276	// Clear I/O Limits
#define	C1HH	280	// Clear Heater Hours
#define	C1CH	282	// Clear Compressor Hours

// Parameters
#define	LFLT	 49	// Loop Freeze Limit Temp
#define	LFLD	 50	// Loop Freeze Limit Differential
#define	LHLT	 51	// Loop Hi Limit Temp
#define	LHLD	 52	// Loop Hi Limit Differential
#define	SFLT	 53	// Seawater Freeze Limit Temp
#define	SFLD	 54	// Seawater Freeze Limit Differential
#define	SPL	 55	// Suction pressure Limit
#define	SPLD	 56	// Suction pressure Limit Differential
#define	DPL	 57	// Discharge pressure Limit
#define	DPLD	 58	// Discharge pressure Limit Differential
#define	HSP	 59	// Heating Setpoint x6
#define	HDF	 60	// Heating Differential x6
#define	CSP	 61	// Cooling Setpoint x6
#define	CDF	 62	// Cooling Differential x6
#define	IHLT	 83	// Immer Heater Temp Limit
#define	CRD	 84	// Compressor restart delay
#define	ERD	 85	// Error retry delay
#define	NET	 29	// Network Data
#define	PGEN	237	// Generic Item

// Faults
#define	FCN	 34	// Number of faults
#define	ACL	 35	// Low AC voltage fault count
#define	FSW	 36	// Flow Switch fault count
#define	HDS	 37	// High Discharge fault count
#define	LLT	 38	// Loop Low Temp fault count
#define	LHT	 39	// Loop High Temp fault count
#define	SLT	 40	// Seawater Low Temp fault count
#define	EEC	 41	// Eprom Error count
#define	LOP	 42	// Loop Out Probe fault count
#define	LSP	 43	// Loop Supply Probe fault count
#define	FAB	 90	// Active Bits
#define	FCT	128	// Total Fault Count
#define	FCAC	129	// AC Voltage Fault Count
#define	FCFS	130	// Flow Switch Fault Count
#define	FCLF	131	// Low Freon Suction Fault Count
#define	FCHF	132	// High Freon Discharge Fault Count
#define	FCLL	133	// Loop Low Temp Error Fault Count
#define	FCLH	134	// Loop High Temp Error Fault Count
#define	FCSL	135	// Seawater Low Temp Error Fault Count
#define	FCEE	136	// Eprom Error Fault Count
#define	FCLO	137	// Loop Out Probe Error Fault Count
#define	FCLS	138	// Loop Supply Probe Error Fault Count
#define	FCSO	139	// Seawater Out Probe Error Fault Count
#define	FCSI	140	// Seawater In Probe Error Fault Count
#define	FCLR	141	// Loop Return Probe Error Fault Count
#define	FCNC	142	// Network Connection Error Fault Count
#define	FCLD	143	// Loop Temp Differential Error Fault Count
#define	FCSD	144	// Seawater Temp Differential Error Fault Count
#define	FCIH	145	// Immr. Heater High Temp Error Fault Count
#define	FCIP	146	// Immr. Heater Probe Error Fault Count
#define	FADD	158	// Additional Fault Bits

// IO Log
#define	SPN	 93	// Min Suction Pressure Logged
#define	DPX	 94	// Max Discharge Pressure Logged
#define	CCX	 95	// Max Compressor current logged
#define	SWPN	 96	// Min Seawater Pressure logged
#define	LPN	 97	// Min Loopwater pressure logged
#define	SPCX	 98	// Max Seawater Pump Current logged
#define	LPCX	 99	// Max Loop Pump Current logged
#define	ACLN	100	// Min AC voltage logged
#define	ACLX	101	// Max AC voltage logged
#define	RLTN	102	// Min Return Loop Temp Logged
#define	RLTX	103	// Max Return Loop Temp Logged
#define	LSTN	104	// Min Loop Supply Temp Logged
#define	LSTX	105	// Max Loop Supply Temp Logged
#define	SITN	106	// Min Seawater In Temp Logged
#define	SITX	107	// Max Seawater In Temp Logged
#define	SOTN	108	// Min Seawater Out Temp Logged
#define	SOTX	109	// Max Seawater Out Temp Logged
#define	LOTN	110	// Min Loop Out Temp Logged
#define	LOTX	111	// Max Loop Out Temp Logged
#define	IHTN	112	// Min Immer. Heater Temp logged
#define	IHTX	113	// Max Immer. Heater Temp logged

// Output Status
//#define	TODO5	220
//#define	CPOF	TODO5	// Compressor on/off
//#define	RVOF	TODO5+1	// Reversing Valve on/off
//#define	HTOF	TODO5+2	// Heater on/off
//#define	LPOF	TODO5+3	// Loop Water Pump on/off
//#define	SPOF	TODO5+4	// Sea Water Pump on/off
//#define	TODO5E	TODO5+4

// Control
#define	SOFF	256	// Select Off
#define	COOL	257	// Select Cool
#define	HEAT	258	// Select Heat
#define	BOTH	259	// Select Both
#define	RESH	260	// Select Resistance Heat
#define	FAHR	261	// Select Farenheit
#define	CENT	262	// Select Centigrade
#define	HILD	263	// Select Hi Load Limits
#define	LOLD	264	// Select Lo Load Limits
#define	CINIT	270	// Initialize
#define	RDEF	271	// Restore Defaults
#define	BUZZ	265	// Buzzer On/Off
#define	CALL	273	// Clear All Logs, all Units COMMON
#define	CAFH	275	// Clear All Fault History
#define	CAIO	277	// Clear All I/O Limits
#define	CLPH	278	// Clear Loop Pump Hours
#define	CSPH	279	// Clear Seawater Pump Hours
#define	CAHH	281	// Clear All Heater Hours
#define	CACH	283	// Clear All Compressor Hours
#define	CDFT	284	// Clear Displayed Fault

// End of File
