
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

#include "loader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	LOAD_MAGIC	0x93

//////////////////////////////////////////////////////////////////////////
//
// Loader Counter Object
//

// Constructor

CLoadCounter::CLoadCounter(void)
{
	Load();
	}

// Assignment Operator

CLoadCounter const & CLoadCounter::operator = (BYTE bData)
{
	m_bData[0] = LOAD_MAGIC;

	m_bData[1] &= ~0x0F;

	m_bData[1] |= bData;
		
	Save();

	return *this;
	}

// Arithmetic Operators

CLoadCounter const & CLoadCounter::operator += (BYTE bData)
{
	if( m_bData[0] == LOAD_MAGIC ) {

		BYTE bCount = m_bData[1] & 0x0F;

		bCount += bData;		

		*this = bCount;
		}
	else {
		*this = 1;
		}
		
	return *this;
	}

CLoadCounter const & CLoadCounter::operator -= (BYTE bData)
{
	if( m_bData[0] == LOAD_MAGIC )	{

		BYTE bCount = m_bData[1] & 0x0F;

		if( bCount ) {

			bCount -= bData;
			
			*this = bCount;
			}		
		}
	else {
		*this = 0;
		}

	return *this;
	}

// Comparison

BOOL CLoadCounter::operator >= (BYTE bData) const
{
	if( m_bData[0] == LOAD_MAGIC ) {

		return GetData() >= bData;
		}

	return FALSE;
	}

// Operations

void CLoadCounter::SetFlag(BOOL fState)
{
	if( m_bData[0] == LOAD_MAGIC ) {

		if( fState ) {
		
			m_bData[1] |= 0xF0;
			}
		else {
			m_bData[1] &= ~0xF0;
			}

		Save();
		}
	else {
		m_bData[1] = fState ? 0xF0 : 0x00;

		*this = 0;
		}
	}

// Attributes

BYTE CLoadCounter::GetData(void) const
{
	if( m_bData[0] == LOAD_MAGIC )	{

		return m_bData[1] & 0x0F;		
		}

	return 0;
	}

BOOL CLoadCounter::GetFlag(void)
{
	if( m_bData[0] == LOAD_MAGIC ) {

		return (m_bData[1] & 0xF0) == 0xF0;
		}

	return FALSE;
	}

// Implementation

void CLoadCounter::Save(void)
{
	if( m_bData[0] == LOAD_MAGIC ) {

		FRAMPutData(Mem(Loading), m_bData, 2);		
		}
	}

void CLoadCounter::Load(void)
{
	memset(m_bData, 0, sizeof(m_bData));

	FRAMGetData(Mem(Loading), m_bData, 2);
	}

// End of File
