
#include "Intern.hpp"

#include "UsbHubDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Descriptor
//

// Constructor

CUsbHubDesc::CUsbHubDesc(void)
{
	Init();
	}

// Endianess

void CUsbHubDesc::HostToUsb(void)
{
	}

void CUsbHubDesc::UsbToHost(void)
{
	}

// Attributes

BOOL CUsbHubDesc::IsValid(void) const
{
	return m_bType == descHub;
	}

// Init

void CUsbHubDesc::Init(void)
{
	memset(this, 0, sizeof(UsbHubDesc));
	
	m_bType	= descHub;
	}

// Debug

void CUsbHubDesc::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nHub Descriptor\n");

	AfxTrace("Type          = %d\n",    m_bType);
	AfxTrace("Len           = %d\n",    m_bLength);
	AfxTrace("Ports         = %d\n",    m_bPorts);
	AfxTrace("Power Switch  = 0x%X\n",  m_wPowerSwitch);
	AfxTrace("Compound      = 0x%X\n",  m_wCompound);
	AfxTrace("Protect       = 0x%X\n",  m_wProctect);
	AfxTrace("Think Time    = 0x%X\n",  m_wThinkTime);
	AfxTrace("Port LEDs     = 0x%X\n",  m_wPortLeds);
	AfxTrace("PwrOn2PwrGood = %2.2X\n", m_bPwrOn2PwrGood);
	AfxTrace("Current       = %2.2X\n", m_bCurrent);

	#endif
	}

// End of File
