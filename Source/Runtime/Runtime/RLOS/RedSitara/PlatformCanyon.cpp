
#include "Intern.hpp"

#include "PlatformCanyon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Ctrl437.hpp"

#include "Dss437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice    * Create_RtcAbmc(void);
extern IDevice    * Create_Mr25Hxx(UINT uChan);
extern IDevice    * Create_Leds(ILeds *pLeds);
extern IDevice    * Create_InputSwitch(IInputSwitch *pSwitch);
extern IDevice    * Create_Identity(UINT uAddr);
extern IDevice    * Create_SdHost437(CClock437 *pClock);
extern IUsbDriver * Create_UsbFunc437(UINT iIndex);
extern IUsbDriver * Create_UsbHost437(UINT iIndex);
extern IUsbDriver * Create_UsbHostExtensible(void);
extern IUsbDriver * Create_UsbHostExtensibleController(void);

//////////////////////////////////////////////////////////////////////////
//
// Prototypes
//

extern bool GetDssConfig(CDss437Config &Config, UINT Display);

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Canyon HMI
//

// Constructor

CPlatformCanyon::CPlatformCanyon(UINT uModel) : CPlatformSitara(TRUE)
{
	m_uModel = uModel;

	m_opp    = oppOPP100;
}

// Destructor

CPlatformCanyon::~CPlatformCanyon(void)
{
	piob->RevokeGroup("dev.");

	delete m_pDss;
}

// IUnknown

HRESULT CPlatformCanyon::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IPortSwitch);

	StdQueryInterface(IInputSwitch);

	StdQueryInterface(ILeds);

	StdQueryInterface(IUsbSystem);

	StdQueryInterface(IUsbSystemPortMapper);

	return CPlatformSitara::QueryInterface(riid, ppObject);
}

ULONG CPlatformCanyon::AddRef(void)
{
	return CPlatformSitara::AddRef();
}

ULONG CPlatformCanyon::Release(void)
{
	return CPlatformSitara::Release();
}

// IDevice

BOOL CPlatformCanyon::Open(void)
{
	if( CPlatformSitara::Open() ) {

		m_pDss = New CDss437(m_pClock);

		piob->RegisterSingleton("dev.rtc", 0, Create_RtcAbmc());

		piob->RegisterSingleton("dev.leds", 0, Create_Leds(this));

		piob->RegisterSingleton("dev.input-s", 0, Create_InputSwitch(this));

		piob->RegisterSingleton("dev.fram", 0, Create_Mr25Hxx(0));

		piob->RegisterSingleton("dev.ident", 0, Create_Identity(addrIdentity));

		piob->RegisterSingleton("dev.sdhost", 0, Create_SdHost437(m_pClock));

		piob->RegisterSingleton("dev.usbctrl-d", 0, Create_UsbFunc437(0));

		piob->RegisterSingleton("dev.usbctrl-h", 0, Create_UsbHost437(1));

		piob->RegisterSingleton("dev.usbctrl-x", 0, Create_UsbHostExtensible());

		piob->RegisterSingleton("dev.usbctrl-c", 0, Create_UsbHostExtensibleController());

		InitUsb();

		RegisterRedCommon();

		InitRack();

		return TRUE;
	}

	return FALSE;
}

// IPlatform

PCTXT CPlatformCanyon::GetModel(void)
{
	switch( m_uModel ) {

		case MODEL_CANYON_04: return "ca04";
		case MODEL_CANYON_07: return "ca07";
		case MODEL_CANYON_07EQ: return "ca07eq";
		case MODEL_CANYON_10: return "ca10";
		case MODEL_CANYON_10EV: return "ca10ev";
		case MODEL_CANYON_15: return "ca15";
		case MODEL_MANTICORE_30: return "da30";
	}

	return CPlatformSitara::GetModel();
}

PCTXT CPlatformCanyon::GetVariant(void)
{
	return GetModel();
}

UINT CPlatformCanyon::GetFeatures(void)
{
	return	1 * rfGraphiteModules |
		0 * rfSerialModules   |
		1 * rfDisplay;
}

// IPortSwitch

UINT METHOD CPlatformCanyon::GetCount(UINT uUnit)
{
	switch( uUnit ) {

		case 0:	return 1;

		case 1:	return 1;

		case 2:	return 1;

		case 3:	return 1;
	}

	return 0;
}

UINT METHOD CPlatformCanyon::GetMask(UINT uUnit)
{
	switch( uUnit ) {

		case 0:	return (1 << physicalRS232);

		case 1:	return (1 << physicalRS232);

		case 2:	return (1 << physicalRS485)       |
			       (1 << physicalRS422Master) |
			       (1 << physicalRS422Slave);

		case 3:	return (1 << physicalRS485)       |
			       (1 << physicalRS422Master) |
			       (1 << physicalRS422Slave);
	}

	return physicalNone;
}

UINT METHOD CPlatformCanyon::GetType(UINT uUnit, UINT uLog)
{
	switch( uUnit ) {

		case 0:
			return physicalRS232;

		case 1:
			return physicalRS232;

		case 2:
			return physicalRS485;

		case 3:
			return physicalRS485;
	}

	return physicalNone;
}

BOOL METHOD CPlatformCanyon::EnablePort(UINT uUnit, BOOL fEnable)
{
	return false;
}

BOOL METHOD CPlatformCanyon::SetPhysical(UINT uUnit, BOOL fRS485)
{
	return false;
}

BOOL METHOD CPlatformCanyon::SetFull(UINT uUnit, BOOL fFull)
{
	return false;
}

BOOL METHOD CPlatformCanyon::SetMode(UINT uUnit, BOOL fAuto)
{
	return false;
}

// ILeds

void METHOD CPlatformCanyon::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 1 ) {

		switch( iLed ) {

			case 0: m_pGpio[4]->SetState(28, uState != stateOff); break;
			case 1: m_pGpio[4]->SetState(29, uState != stateOff); break;
			case 2: m_pGpio[4]->SetState(26, uState != stateOff); break;
			case 3: m_pGpio[4]->SetState(27, uState != stateOff); break;
		}
	}
}

void METHOD CPlatformCanyon::SetLedLevel(UINT uPercent, bool fPersist)
{
}

UINT METHOD CPlatformCanyon::GetLedLevel(void)
{
	return 100;
}

// IInputSwitch

UINT METHOD CPlatformCanyon::GetSwitches(void)
{
	return 0xFFFFFFFF;
}

UINT METHOD CPlatformCanyon::GetSwitch(UINT uSwitch)
{
	return 1;
}

// IUsbSystem

void METHOD CPlatformCanyon::OnDeviceArrival(IUsbHostFuncDriver *pDriver)
{
	IUsbDevice *pDev = NULL;

	pDriver->GetDevice(pDev);

	UsbTreePath const &Path = pDev->GetTreePath();

	if( GetPortType(Path) == typeSystem ) {

		if( pDriver->GetClass() == devHub ) {

			IUsbHubDriver *pHub = (IUsbHubDriver *) pDriver;

			UsbHubLedMap Map = {

				{ MAKEWORD(0,1), MAKEWORD(2,1), 0, 0, 0, 0, 0, 0 },
				{ MAKEWORD(1,1), MAKEWORD(3,1), 0, 0, 0, 0, 0, 0 },
			};

			pHub->SetLedMap(Map);

			return;
		}
	}
}

void METHOD CPlatformCanyon::OnDeviceRemoval(IUsbHostFuncDriver *pDriver)
{
}

// IUsbSystemPortMapper

UINT METHOD CPlatformCanyon::GetPortCount(void)
{
	return 1;
}

UINT METHOD CPlatformCanyon::GetPortType(UsbTreePath const &Path)
{
	if( Path.a.dwTier == 1 ) {

		return typeSystem;
	}

	return typeUnknown;
}

UINT METHOD CPlatformCanyon::GetPortReset(UsbTreePath const &Path)
{
	if( Path.a.dwTier == 1 && Path.a.dwPort1 == 1 ) {

		return MAKEWORD(13, 5);
	}

	return NOTHING;
}

// Inititialisation

void CPlatformCanyon::InitClocks(void)
{
	CPlatformSitara::InitClocks();

	m_pClock->SetClockMode(CClock437::clockMMC0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSB0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSB1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSBPHY0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUSBPHY1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockDSS, CClock437::modeSwWakup);
}

void CPlatformCanyon::InitMpu(void)
{
	CDss437Config Config;

	GetDssConfig(Config, m_uModel);

	UINT o = m_pCtrl->GetRefFreq();

	UINT n = 40;

	UINT q = 4;

	UINT k = Min(180000000 / Config.m_uFrequency, 4u);

	UINT f = o / (n * q * k);

	UINT m = (Config.m_uFrequency + f / 2) / f;

	while( m > 2047 ) {

		m >>= 1;

		n <<= 1;
	}

	m_pClock->SetDispFreq(m, n - 1, q);

	switch( Config.m_uFrequency / 10000000 ) {

		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			SetMpuBandwidth(300);
			break;

		case 5:
			SetMpuBandwidth(100);
			break;

		case 6:
			SetMpuBandwidth(70);
			break;

		case 7:
			SetMpuBandwidth(50);
			break;

		default:
			SetMpuBandwidth(40);
			break;
	}

	SetMpuOpp(m_opp);
}

void CPlatformCanyon::InitMux(void)
{
	CPlatformSitara::InitMux();

	DWORD const muxSd[] = {

		CCtrl437::pinMMC0CMD,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0CLK,   CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT0,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT1,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT2,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMMC0DAT3,  CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA2, CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
	};

	DWORD const muxUsb[] = {

		CCtrl437::pinGPIO5_13,  CCtrl437::muxMode7 | CCtrl437::padRxActive,
		CCtrl437::pinGPMCAD10,  CCtrl437::muxMode9 | CCtrl437::padRxActive | CCtrl437::padPullNo,
		CCtrl437::pinGPMCAD9,   CCtrl437::muxMode9 | CCtrl437::padRxActive | CCtrl437::padPullNo,
		CCtrl437::pinCAM0DATA4, CCtrl437::muxMode7 | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA5, CCtrl437::muxMode7 | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA6, CCtrl437::muxMode7 | CCtrl437::padRxActive,
		CCtrl437::pinCAM0DATA7, CCtrl437::muxMode7 | CCtrl437::padRxActive,
	};

	DWORD const muxDss[] = {

		CCtrl437::pinDSSDATA0,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA1,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA2,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA3,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA4,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA5,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA6,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA7,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA8,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA9,    CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA10,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA11,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA12,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA13,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA14,   CCtrl437::muxMode0,
		CCtrl437::pinDSSDATA15,   CCtrl437::muxMode0,
		CCtrl437::pinDSSPCLK,     CCtrl437::muxMode0,
		CCtrl437::pinDSSACBIASEN, CCtrl437::muxMode0,
		CCtrl437::pinDSSHSYNC,    CCtrl437::muxMode0,
		CCtrl437::pinDSSVSYNC,    CCtrl437::muxMode0,
		CCtrl437::pinGPMCAD15,    CCtrl437::muxMode1,
		CCtrl437::pinGPMCAD14,    CCtrl437::muxMode1,
		CCtrl437::pinGPMCAD13,    CCtrl437::muxMode1,
		CCtrl437::pinGPMCAD12,    CCtrl437::muxMode1,
		CCtrl437::pinCAM0WEN,     CCtrl437::muxMode2,
		CCtrl437::pinCAM0FIELD,   CCtrl437::muxMode2,
		CCtrl437::pinCAM0VD,      CCtrl437::muxMode2,
		CCtrl437::pinGPMCAD8,     CCtrl437::muxMode1,
	};

	m_pCtrl->SetMux(muxSd,  elements(muxSd));

	m_pCtrl->SetMux(muxUsb, elements(muxUsb));

	m_pCtrl->SetMux(muxDss, elements(muxDss));
}

void CPlatformCanyon::InitGpio(void)
{
	CPlatformSitara::InitGpio();

	m_pGpio[1]->SetDirection(19, true);
	m_pGpio[1]->SetDirection(22, true);
	m_pGpio[1]->SetDirection(23, true);
	m_pGpio[4]->SetDirection(24, false);
	m_pGpio[4]->SetDirection(26, true);
	m_pGpio[4]->SetDirection(27, true);
	m_pGpio[4]->SetDirection(28, true);
	m_pGpio[4]->SetDirection(29, true);
	m_pGpio[5]->SetDirection(4,  true);
	m_pGpio[5]->SetDirection(13, true);
	m_pGpio[5]->SetDirection(24, false);
	m_pGpio[5]->SetDirection(25, false);

	m_pGpio[1]->SetState(22, true);
	m_pGpio[1]->SetState(23, true);
	m_pGpio[4]->SetState(26, false);
	m_pGpio[4]->SetState(27, false);
	m_pGpio[4]->SetState(28, false);
	m_pGpio[4]->SetState(29, false);
	m_pGpio[5]->SetState(13, false);
}

void CPlatformCanyon::InitMisc(void)
{
	CPlatformSitara::InitMisc();

	m_pCtrl->SetMIIMode(0, CCtrl437::modeGMII);

	m_pCtrl->SetMIIMode(1, CCtrl437::modeGMII);

	m_pCtrl->SetPhyPower(0, true);

	m_pCtrl->SetPhyPower(1, true);
}

void CPlatformCanyon::InitUarts(void)
{
	for( UINT i = 0; i < 4; i++ ) {

		IPortObject *pPort = NULL;

		AfxGetObject("uart", i, IPortObject, pPort);

		if( pPort ) {

			pPort->Bind(this);

			pPort->Release();
		}
	}
}

void CPlatformCanyon::InitUsb(void)
{
	IUsbDriver *p0, *p1, *p2;

	piob->GetObject("usbctrl-h", 0, AfxAeonIID(IUsbDriver), (void **) &p0);

	piob->GetObject("usbctrl-x", 0, AfxAeonIID(IUsbDriver), (void **) &p1);

	piob->GetObject("usbctrl-c", 0, AfxAeonIID(IUsbDriver), (void **) &p2);

	if( p0 && p1 && p2 ) {

		p0->Bind(p1);

		p1->Bind(p2);
	}

	AfxRelease(p0);

	AfxRelease(p1);

	AfxRelease(p2);
}

void CPlatformCanyon::InitRack(void)
{
	IUsbHostStack    *pStack;

	IExpansionSystem *pExpSys;

	IUsbSystem       *pExpUsb;

	AfxGetObject("usb.host", 0, IUsbHostStack, pStack);

	AfxGetObject("usb.rack", 0, IExpansionSystem, pExpSys);

	AfxGetObject("usb.rack", 0, IUsbSystem, pExpUsb);

	if( pStack ) {

		pStack->Init();

		pStack->Attach((IUsbSystem *) this);

		pStack->Attach(pExpUsb);
	}

	AfxRelease(pExpSys);

	AfxRelease(pStack);

	AfxRelease(pExpUsb);

	m_pGpio[5]->SetState(13, false);

	Sleep(5);

	m_pGpio[5]->SetState(13, true);
}

// End of File
