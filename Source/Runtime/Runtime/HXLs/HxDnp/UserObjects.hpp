
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UserObjects_HPP

#define	INCLUDE_UserObjects_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "UserTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Objects
//

class CUserObjects
{
	public:
		// Constructor
		CUserObjects(void);

		// Destructor
		~CUserObjects(void);

		// Data Access
		CUserTable * Find(BYTE bObject);
		CUserData  * Find(BYTE bObject, UINT uIndex);
		DWORD	     GetIIbits(void);
		void	     SetIIbit(BYTE bBit, BOOL fState);
		void	     SetUnsolicitMsgMask(BYTE bMask);
		BYTE	     GetUnsolicitMsgMask(void);
		WORD	     GetFeedback(void);
		void	     SetFeedback(WORD wFeedback);
		void	     SetMode(BYTE bOject, BYTE bMode);
		BYTE	     GetMode(BYTE bOject);
		
	protected:

		// Data Members
		CUserTable * m_pObjects[MAX_OBJ];
		DWORD	     m_pPending[MAX_OBJ];
		BOOL	     m_pDoPoll [MAX_OBJ];
		BYTE	     m_bMode   [MAX_OBJ];
		BYTE	     m_bPoll;
		DWORD	     m_IIbits;
		BYTE	     m_bUnsolicitMsgMask;
		WORD	     m_Feedback;
		ITimeZone *  m_pZone;

	};

// End of File

#endif
