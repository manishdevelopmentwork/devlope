
#include "intern.hpp"

#include "imgdlg.hpp"

#include "rle8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Image Options
//

// Constructor

CImageOpts::CImageOpts(void)
{
	m_Keep   = 0;

	m_Rotate = 0;

	m_Scale  = 1000;

	m_Flip   = 0;

	m_dx     = 0;

	m_dy     = 0;
}

// Comparison

BOOL CImageOpts::IsEntireSame(CImageOpts const &That) const
{
	return m_Keep   == That.m_Keep   &&
		m_Size   == That.m_Size   &&
		m_Rotate == That.m_Rotate &&
		m_Scale  == That.m_Scale	 &&
		m_Flip   == That.m_Flip   &&
		m_dx     == That.m_dx     &&
		m_dy     == That.m_dy;
}

BOOL CImageOpts::IsFormatSame(CImageOpts const &That) const
{
	return m_Rotate == That.m_Rotate &&
		m_Scale  == That.m_Scale	 &&
		m_Flip   == That.m_Flip   &&
		m_dx     == That.m_dx     &&
		m_dy     == That.m_dy;
}

// Comparison Operators

int CImageOpts::operator == (CImageOpts const &That) const
{
	return IsEntireSame(That);
}

// Operations

void CImageOpts::SetAspect(CRect &Rect, CSize Aspect) const
{
	if( m_Keep ) {

		Rect.MatchAspect(Aspect);
	}
}

void CImageOpts::SetTransform(CDC &DC, CRect const &Rect) const
{
	if( !IsFormatSame(CImageOpts()) ) {

		XFORM x1, x2, x3, x4, x5, x6;

		int dx = m_dx * Rect.cx() / 1000;

		int dy = m_dy * Rect.cy() / 1000;

		CPoint c = Rect.GetCentre();

		MakeTrans(x1, -c.x, -c.y);

		MakeRotate(x2, m_Rotate);

		MakeScale(x3, m_Scale);

		MakeTrans(x4, dx, dy);

		MakeFlip(x5, m_Flip);

		MakeTrans(x6, +c.x, +c.y);

		SetGraphicsMode(DC, GM_ADVANCED);

		ModifyWorldTransform(DC, &x6, MWT_LEFTMULTIPLY);

		ModifyWorldTransform(DC, &x5, MWT_LEFTMULTIPLY);

		ModifyWorldTransform(DC, &x4, MWT_LEFTMULTIPLY);

		ModifyWorldTransform(DC, &x3, MWT_LEFTMULTIPLY);

		ModifyWorldTransform(DC, &x2, MWT_LEFTMULTIPLY);

		ModifyWorldTransform(DC, &x1, MWT_LEFTMULTIPLY);
	}
}

void CImageOpts::ClearTransform(CDC &DC) const
{
	if( !IsFormatSame(CImageOpts()) ) {

		ModifyWorldTransform(DC, NULL, MWT_IDENTITY);

		SetGraphicsMode(DC, GM_COMPATIBLE);
	}
}

// Implementation

void CImageOpts::MakeRotate(XFORM &x, INT a)
{
	FLOAT t = FLOAT(a / 180.0 * 3.1415926);

	x.eM11 = FLOAT(+cos(t));
	x.eM12 = FLOAT(-sin(t));
	x.eM21 = FLOAT(+sin(t));
	x.eM22 = FLOAT(+cos(t));

	x.eDx = 0;
	x.eDy = 0;
}

void CImageOpts::MakeScale(XFORM &x, UINT s)
{
	FLOAT f = FLOAT(s / 1000.0);

	x.eM11 = f;
	x.eM12 = 0;
	x.eM21 = 0;
	x.eM22 = f;

	x.eDx = 0;
	x.eDy = 0;
}

void CImageOpts::MakeFlip(XFORM &x, UINT f)
{
	x.eM11 = FLOAT((f & 1) ? -1 : +1);
	x.eM12 = 0;
	x.eM21 = 0;
	x.eM22 = FLOAT((f & 2) ? -1 : +1);

	x.eDx = 0;
	x.eDy = 0;
}

void CImageOpts::MakeTrans(XFORM &x, int dx, int dy)
{
	x.eM11 = 1;
	x.eM12 = 0;
	x.eM21 = 0;
	x.eM22 = 1;

	x.eDx = FLOAT(dx);
	x.eDy = FLOAT(dy);
}

//////////////////////////////////////////////////////////////////////////
//
// Image Manager
//

// Dynamic Class

AfxImplementDynamicClass(CImageManager, CUIItem);

// Constructor

CImageManager::CImageManager(void)
{
	m_pFiles   = New CImageFileList;

	m_pViews   = New CImageViewList;

	m_SymLevel = NOTHING;

	m_Include  = 0;

	m_RelMode  = 1;

	FindRelBase();

	FindCfPath();

	ClearCache();
}

// Destructor

CImageManager::~CImageManager(void)
{
}

// UI Managament

void CImageManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			pHost->EnableUI(this, L"RelBase", m_RelMode == 1);
		}

		if( Tag == L"RelMode" ) {

			pHost->EnableUI(this, L"RelBase", m_RelMode == 1);

			if( m_RelMode == 2 ) {

				if( m_pDbase->GetFilename().IsEmpty() ) {

					CString Text = CString(IDS_NO_DBASE_PATH_1) +
						CString(IDS_NO_DBASE_PATH_2) +
						CString(IDS_NO_DBASE_PATH_3);

					afxMainWnd->Error(Text);

					m_RelMode = 1;

					pHost->UpdateUI(Tag);

					return;
				}
			}

			UpdateRelative();
		}

		if( Tag == L"RelBase" ) {

			if( !UpdateRelative() ) {

				CString Text = CString(IDS_IMAGES_MISSING_1) +
					CString(IDS_IMAGES_MISSING_2);

				afxMainWnd->Error(Text);
			}

			SaveRelBase();
		}

		if( Tag == L"CfPath" ) {

			SaveCfPath();
		}

		if( Tag == L"SymLevel" ) {

			Sym_PreferNew(m_SymLevel);

			m_pFiles->SetSymLevel();

			m_pViews->DeleteAllItems(TRUE);

			ClearCache();
		}
	}

	CUIItem::OnUIChange(pHost, pItem, Tag);
}

// View Location

UINT CImageManager::FindView(UINT uImage, CImageOpts const &Opts)
{
	return m_pViews->FindView(uImage, Opts);
}

// Data Testing

BOOL CImageManager::CanAcceptDataObject(IDataObject *pData)
{
	CImageFileItem *pFile = New CImageFileItem;

	if( pFile->CanAcceptDataObject(pData) ) {

		delete pFile;

		return TRUE;
	}

	delete pFile;

	return FALSE;
}

// Data Acceptance

UINT CImageManager::LoadFromDataObject(IDataObject *pData)
{
	CImageFileItem *pFile = New CImageFileItem;

	pFile->SetParent(m_pFiles);

	pFile->Init();

	if( pFile->LoadFromDataObject(pData) ) {

		UINT n = FindImageFile(pFile->GetCRC32());

		if( n == NOTHING ) {

			if( m_pFiles->AppendItem(pFile) ) {

				return pFile->GetIndex();
			}
		}

		pFile->Kill();

		delete pFile;

		return n;
	}

	pFile->Kill();

	delete pFile;

	return NOTHING;
}

UINT CImageManager::LoadFromFile(CFilename File, BOOL fKeep)
{
	CImageFileItem *pFile = New CImageFileItem;

	pFile->SetParent(m_pFiles);

	pFile->Init();

	if( pFile->LoadFromFile(File, fKeep) ) {

		UINT n = FindImageFile(pFile->GetCRC32());

		if( n == NOTHING ) {

			if( m_pFiles->AppendItem(pFile) ) {

				return pFile->GetIndex();
			}
		}

		pFile->Kill();

		delete pFile;

		return n;
	}

	pFile->Kill();

	delete pFile;

	return NOTHING;
}

UINT CImageManager::LoadFromTreeFile(CTreeFile &Tree, BOOL fFixup)
{
	CImageFileItem *pFile = New CImageFileItem;

	pFile->SetParent(m_pFiles);

	if( pFile->LoadFromTreeFile(Tree, fFixup) ) {

		UINT n = FindImageFile(pFile->GetCRC32());

		if( n == NOTHING ) {

			if( m_pFiles->AppendItem(pFile) ) {

				return pFile->GetIndex();
			}
		}

		pFile->Kill();

		delete pFile;

		return n;
	}

	pFile->Kill();

	delete pFile;

	return NOTHING;
}

// Attributes

CString CImageManager::GetRelBase(void) const
{
	return m_RelBase;
}

CString CImageManager::GetFilename(UINT uImage) const
{
	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			return pFile->GetFilename();
		}
	}

	return L"";
}

UINT CImageManager::FindImageFile(DWORD CRC) const
{
	INDEX n = m_pFiles->GetHead();

	while( !m_pFiles->Failed(n) ) {

		CImageFileItem *pFile = m_pFiles->GetItem(n);

		if( pFile && pFile->GetCRC32() == CRC ) {

			return pFile->GetIndex();
		}

		m_pFiles->GetNext(n);
	}

	return NOTHING;
}

CSize CImageManager::GetImageNativeSize(UINT uImage) const
{
	CSize Size;

	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			Size = pFile->GetNativeSize();
		}
	}

	return Size;
}

CSize CImageManager::GetImageMaxSize(UINT uImage) const
{
	CSize Size;

	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			Size = pFile->GetMaxSize();
		}
	}

	return Size;
}

CSize CImageManager::GetImageMinSize(UINT uImage) const
{
	CSize Size;

	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			Size = pFile->GetMinSize();
		}
	}

	return Size;
}

BOOL CImageManager::IsTexture(UINT uImage) const
{
	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			return pFile->IsTexture();
		}
	}

	return FALSE;
}

BOOL CImageManager::IsMetaFile(UINT uImage) const
{
	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			return pFile->IsMetaFile();
		}
	}

	return FALSE;
}

BOOL CImageManager::IsXaml(UINT uImage) const
{
	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			return pFile->IsXaml();
		}
	}

	return FALSE;
}

// Operations

BOOL CImageManager::UpdateRelative(void)
{
	afxThread->SetWaitMode(TRUE);

	INDEX n = m_pFiles->GetHead();

	BOOL  f = TRUE;

	while( !m_pFiles->Failed(n) ) {

		CImageFileItem *pFile = m_pFiles->GetItem(n);

		if( pFile ) {

			if( !pFile->UpdateRelative() ) {

				f = FALSE;
			}
		}

		m_pFiles->GetNext(n);
	}

	afxThread->SetWaitMode(FALSE);

	return f;
}

void CImageManager::ClearCache(void)
{
	for( UINT n = 0; n < elements(m_Cache); n++ ) {

		m_Cache[n].m_Data.Empty();

		m_Cache[n].m_fUsed = FALSE;
	}
}

void CImageManager::InvokeDialog(CViewWnd &Wnd)
{
	if( m_pFiles->GetItemCount() ) {

		CSysProxy Proxy;

		Proxy.Bind(&Wnd);

		CImageManagerDialog Dialog(this);

		Proxy.ExecSemiModal(Dialog);

		return;
	}

	CString Text = IDS_IMAGE_DATABASE_IS;

	Wnd.Error(Text);
}

void CImageManager::UpdateUsed(void)
{
	m_pFiles->MarkAsUnused();

	CUIManager *pUI = (CUIManager *) GetParent();

	pUI->m_pPages->ScanImages(this);
}

BOOL CImageManager::MarkUsed(CPrimRefList const &Refs)
{
	INDEX Index = Refs.GetHead();

	while( !Refs.Failed(Index) ) {

		UINT r = Refs[Index];

		if( r & refImage ) {

			UINT uImage = (r & ~refImage);

			MarkUsed(uImage);
		}

		Refs.GetNext(Index);
	}

	return TRUE;
}

BOOL CImageManager::MarkUsed(UINT uImage)
{
	if( uImage < NOTHING ) {

		CImageFileItem *pFile = m_pFiles->GetItem(uImage);

		if( pFile ) {

			pFile->m_Used = 1;

			return TRUE;
		}
	}

	return FALSE;
}

// Persistance

void CImageManager::Init(void)
{
	CMetaItem::Init();

	m_SymLevel = 1;

	Sym_PreferNew(m_SymLevel);
}

void CImageManager::PostLoad(void)
{
	if( m_SymLevel == NOTHING ) {

		// This is a C3.0 database so we need
		// to fix the symbol type references and
		// select old-style symbol preference.

		m_pFiles->FixTypes();

		m_SymLevel = 0;
	}

	CMetaItem::PostLoad();

	Sym_PreferNew(m_SymLevel);
}

// Property Save Filter

BOOL CImageManager::SaveProp(CString const &Tag) const
{
	if( Tag == L"RelBase" ) {

		return FALSE;
	}

	return CUIItem::SaveProp(Tag);
}
// Meta Data Creation

void CImageManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(SymLevel);
	Meta_AddInteger(Include);
	Meta_AddInteger(RelMode);
	Meta_AddString(RelBase);
	Meta_AddString(CfPath);
	Meta_AddCollect(Files);
	Meta_AddCollect(Views);
}

// Base Path Helpers

BOOL CImageManager::FindRelBase(void)
{
	CModule * pApp = afxModule->GetApp();

	CRegKey   Reg  = pApp->GetUserRegKey();

	CString   Def  = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Images");

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Images");

	m_RelBase = Reg.GetValue(L"Root", Def);

	if( m_RelBase.Right(1) == "\\" ) {

		UINT uLen = m_RelBase.GetLength();

		m_RelBase = m_RelBase.Left(uLen - 1);

		return TRUE;
	}

	return FALSE;
}

void CImageManager::SaveRelBase(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Images");

	Reg.SetValue(L"Root", m_RelBase);
}

BOOL CImageManager::FindCfPath(void)
{
	CModule * pApp = afxModule->GetApp();

	CRegKey   Reg  = pApp->GetUserRegKey();

	CString   Def  = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"CF");

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"CF");

	m_CfPath = Reg.GetValue(L"Root", Def);

	if( m_CfPath.Right(1) == "\\" ) {

		UINT uLen = m_CfPath.GetLength();

		m_CfPath  = m_CfPath.Left(uLen - 1);

		return TRUE;
	}

	return FALSE;
}

void CImageManager::SaveCfPath(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"CF");

	Reg.SetValue(L"Root", m_CfPath);
}

//////////////////////////////////////////////////////////////////////////
//
// Image File List
//

// Dynamic Class

AfxImplementDynamicClass(CImageFileList, CItemIndexList);

// Constructor

CImageFileList::CImageFileList(void)
{
	m_Class = AfxRuntimeClass(CImageFileItem);
}

// Destructor

CImageFileList::~CImageFileList(void)
{
}

// Operations

void CImageFileList::DeleteUnused(void)
{
	// TODO -- Get rid.
}

void CImageFileList::MarkAsUnused(void)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->m_Used = 0;

		GetNext(Index);
	}
}

void CImageFileList::SetSymLevel(void)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->SetSymLevel();

		GetNext(Index);
	}
}

void CImageFileList::FixTypes(void)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		GetItem(Index)->FixType();

		GetNext(Index);
	}
}

// Item Location

CImageFileItem * CImageFileList::GetItem(INDEX Index) const
{
	return (CImageFileItem *) CItemIndexList::GetItem(Index);
}

CImageFileItem * CImageFileList::GetItem(UINT uPos) const
{
	return (CImageFileItem *) CItemIndexList::GetItem(uPos);
}

//////////////////////////////////////////////////////////////////////////
//
// Image View List
//

// Dynamic Class

AfxImplementDynamicClass(CImageViewList, CItemIndexList);

// Constructor

CImageViewList::CImageViewList(void)
{
	m_Class = AfxRuntimeClass(CImageViewItem);
}

// Destructor

CImageViewList::~CImageViewList(void)
{
}

// Operations

UINT CImageViewList::FindView(UINT uImage, CImageOpts const &Opts)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CImageViewItem *pView = GetItem(Index);

		if( pView->m_Image == uImage ) {

			if( pView->m_Opts == Opts ) {

				return pView->m_Handle;
			}
		}

		GetNext(Index);
	}

	CImageViewItem *pView = New CImageViewItem;

	AppendItem(pView);

	pView->m_Image = uImage;

	pView->m_Opts  = Opts;

	pView->Render();

	return pView->m_Handle;
}

// Item Location

CImageViewItem * CImageViewList::GetItem(INDEX Index) const
{
	return (CImageViewItem *) CItemIndexList::GetItem(Index);
}

CImageViewItem * CImageViewList::GetItem(UINT uPos) const
{
	return (CImageViewItem *) CItemIndexList::GetItem(uPos);
}

//////////////////////////////////////////////////////////////////////////
//
// Image File Item
//

// Dynamic Class

AfxImplementDynamicClass(CImageFileItem, CMetaItem);

// Clipboard Format

UINT CImageFileItem::m_cfFile = RegisterClipboardFormat(L"FileNameW");

UINT CImageFileItem::m_cfSym  = RegisterClipboardFormat(L"C31Symbol");

UINT CImageFileItem::m_cfEMF  = RegisterClipboardFormat(L"C31MetaFile");

UINT CImageFileItem::m_cfBMP  = RegisterClipboardFormat(L"C31Bitmap");

UINT CImageFileItem::m_cfTEX  = RegisterClipboardFormat(L"C31Texture");

// Constructor

CImageFileItem::CImageFileItem(void)
{
	m_Used   = 0;

	m_Type   = 0;

	m_fDirty = TRUE;

	m_pCache = NULL;

	m_nCache = 0;
}

// Destructor

CImageFileItem::~CImageFileItem(void)
{
}

// Data Testing

BOOL CImageFileItem::CanAcceptDataObject(IDataObject *pData)
{
	if( CanAcceptFile(pData) ) {

		return TRUE;
	}

	if( CanAcceptEMF(pData) ) {

		return TRUE;
	}

	if( CanAcceptBMP(pData) || CanAcceptTEX(pData) ) {

		return TRUE;
	}

	return FALSE;
}

// Data Acceptance

BOOL CImageFileItem::LoadFromDataObject(IDataObject *pData)
{
	if( AcceptFile(pData) ) {

		return TRUE;
	}

	if( AcceptEMF(pData) ) {

		return TRUE;
	}

	if( AcceptBMP(pData) || AcceptTEX(pData) ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::LoadFromFile(CFilename File, BOOL fKeep)
{
	if( IsSymFile(File) ) {

		if( DoLoad(File) ) {

			m_File = File;

			FindSizes();
		}

		return TRUE;
	}

	if( FindType(File.GetType()) ) {

		if( DoLoad(File) ) {

			if( fKeep ) {

				m_File = StripFile(File);
			}

			FindSizes();

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CImageFileItem::LoadFromTreeFile(CTreeFile &Tree, BOOL fFixup)
{
	Tree.GetObject();

	Load(Tree);

	Tree.EndObject();

	if( fFixup ) {

		FixType();
	}

	PostPaste();

	PostLoad();

	return TRUE;
}

// Attributes

BOOL CImageFileItem::IsTexture(void) const
{
	return m_Type == typeTEX;
}

BOOL CImageFileItem::IsMetaFile(void) const
{
	return m_Type == typeEMF || m_Type == typeWMF;
}

BOOL CImageFileItem::IsXaml(void) const
{
	return m_Type == typeXAML;
}

BOOL CImageFileItem::IsOnDisk(void) const
{
	if( !m_File.IsEmpty() ) {

		if( IsSymFile(m_File) ) {

			return TRUE;
		}

		if( CFilename(GetFilename()).Exists() ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CImageFileItem::IsValid(void) const
{
	return m_Data.GetCount() > 0;
}

DWORD CImageFileItem::GetCRC32(void) const
{
	if( m_fDirty ) {

		UINT   uCount = m_Data.GetCount();

		PCBYTE pData  = m_Data.GetPointer();

		m_CRC         = CRC32(pData, uCount);

		m_fDirty      = FALSE;
	}

	return m_CRC;
}

CSize CImageFileItem::GetNativeSize(void) const
{
	return m_Nat;
}

CSize CImageFileItem::GetMinSize(void) const
{
	return m_Min;
}

CSize CImageFileItem::GetMaxSize(void) const
{
	return m_Max;
}

CString CImageFileItem::GetFilename(void) const
{
	if( m_File.IsEmpty() || IsSymFile(m_File) ) {

		return L"";
	}

	return CString(BuildFile(m_File));
}

CString CImageFileItem::GetSourceType(void) const
{
	if( m_File.IsEmpty() ) {

		return IDS_INTERNAL;
	}

	if( IsSymFile(m_File) ) {

		return IDS_SYMBOL;
	}

	if( m_File.Left(2) == L".\\" ) {

		return IDS_RELATIVE;
	}

	return IDS_FIXED;
}

CString CImageFileItem::GetSourceName(void) const
{
	if( m_File.IsEmpty() ) {

		return IDS_UNTITLED;
	}

	if( IsSymFile(m_File) ) {

		CStringArray List;

		m_File.Mid(4).Tokenize(List, ',');

		UINT  uCat     = wcstoul(List[0], NULL, 10);

		DWORD dwHandle = wcstoul(List[1], NULL, 10);

		UINT  uSlot    = Sym_FindByHandle(uCat, dwHandle);

		if( uSlot < NOTHING ) {

			return Sym_GetDesc(uCat, uSlot);
		}

		return IDS_UNKNOWN;
	}

	if( m_File.Left(2) == L".\\" ) {

		return m_File.Mid(2);
	}

	return m_File;
}

CString CImageFileItem::GetTypeName(void) const
{
	if( m_Type == typeGDIP ) {

		CString Type = SuggestExt();

		if( !Type.IsEmpty() ) {

			Type.MakeUpper();

			return Type;
		}
	}

	switch( m_Type ) {

		case typeBMP:

			return IDS_BITMAP_2;

		case typeTEX:

			return IDS_TEXTURE;

		case typeGDIP:

			return IDS_GDI;

		case typeEMF:
		case typeWMF:

			return IDS_METAFILE_2;

		case typeXAML:

			return CString(IDS_XAML);
	}

	return IDS_UNKNOWN;
}

CString CImageFileItem::SuggestExt(void) const
{
	if( m_Type == typeGDIP ) {

		if( !m_File.IsEmpty() ) {

			CString Ext = CFilename(m_File).GetType();

			Ext.MakeLower();

			return Ext;
		}
		else {
			PCSTR pData = PCSTR(m_Data.GetPointer());

			if( pData ) {

				if( !strncmp(pData+0, "GIF8", 4) ) {

					return L"gif";
				}

				if( !strncmp(pData+0, "\xFF\xD8\xFF", 3) ) {

					return L"jpeg";
				}

				if( !strncmp(pData+1, "PNG", 3) ) {

					return L"png";
				}

				if( !strncmp(pData+0, "I I", 3) ) {

					return L"tiff";
				}

				if( !strncmp(pData+0, "II*", 3) ) {

					return L"tiff";
				}
			}
		}
	}

	switch( m_Type ) {

		case typeBMP:
		case typeTEX:

			return L"bmp";

		case typeEMF:

			return L"emf";

		case typeWMF:

			return L"wmf";

		case typeXAML:

			return L"xaml";
	}

	return L"";
}

// Operations

BOOL CImageFileItem::Preview(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	if( m_Data.GetCount() ) {

		if( m_Type == typeBMP || m_Type == typeTEX ) {

			PreviewBMP(DC, Pos, Opts);

			return TRUE;
		}

		if( m_Type == typeGDIP ) {

			PreviewGDIP(DC, Pos, Opts);

			return TRUE;
		}

		if( m_Type == typeEMF ) {

			PreviewEMF(DC, Pos, Opts);

			return TRUE;
		}

		if( m_Type == typeXAML ) {

			PreviewXAML(DC, Pos, Opts);

			return TRUE;
		}
	}

	return FALSE;
}

CSize CImageFileItem::Render(CByteArray &Data, CImageOpts const &Opts, UINT uBits)
{
	CSize Size;

	if( m_Data.GetCount() ) {

		if( SearchLocalCache(Data, Size, Opts, uBits) ) {

			return Size;
		}

		if( SearchViewList(Data, Size, Opts, uBits) ) {

			return Size;
		}

		if( m_Type == typeBMP || m_Type == typeTEX ) {

			Size = RenderBMP(Data, Opts, uBits);
		}

		if( m_Type == typeGDIP ) {

			Size = RenderGDIP(Data, Opts, uBits);
		}

		if( m_Type == typeWMF ) {

			ConvertWMF();
		}

		if( m_Type == typeEMF ) {

			Size = RenderEMF(Data, Opts, uBits);
		}

		if( m_Type == typeXAML ) {

			Size = RenderXAML(Data, Opts, uBits);
		}

		if( Size.cx && Size.cy ) {

			UINT t = NOTHING;

			UINT f = NOTHING;

			UINT n;

			for( n = 0; n < m_nCache; n++ ) {

				if( !m_pCache[n].m_fUsed ) {

					break;
				}

				if( m_pCache[n].m_uHit < t ) {

					t = m_pCache[n].m_uHit;

					f = n;
				}
			}

			if( n == m_nCache ) {

				n = f;
			}

			m_pCache[n].m_fUsed = TRUE;

			m_pCache[n].m_CRC   = GetCRC32();

			m_pCache[n].m_Opts  = Opts;

			m_pCache[n].m_uBits = uBits;

			m_pCache[n].m_uHit  = GetTickCount();

			m_pCache[n].m_Data.Empty();

			m_pCache[n].m_Data.Append(Data);
		}
	}

	return Size;
}

void CImageFileItem::PutHandle(CInitData &Init, CImageOpts const &Opts)
{
	CImageManager  *pImages = (CImageManager *) GetParent(2);

	CImageViewList *pViews  = pImages->m_pViews;

	Init.AddWord(WORD(pViews->FindView(GetIndex(), Opts)));
}

BOOL CImageFileItem::UpdateRelative(void)
{
	if( !m_File.IsEmpty() ) {

		if( !IsSymFile(m_File) ) {

			CImageManager *pImages = (CImageManager *) GetParent(2);

			if( pImages->m_RelMode ) {

				if( m_File.Left(2) != L".\\" ) {

					m_File = StripFile(m_File);
				}
			}
			else {
				if( m_File.Left(2) == L".\\" ) {

					m_File = m_File.Mid(1);

					m_File = pImages->GetRelBase() + m_File;
				}
				else
					return TRUE;
			}

			if( m_Type == typeEMF ) {

				if( m_File.Right(3) == L"WMF" ) {

					m_Type = typeWMF;
				}
			}

			if( m_Data.GetCount() ) {

				m_Data.Empty();
			}

			if( DoLoad(BuildFile(m_File)) ) {

				FindSizes();

				return TRUE;
			}

			return FALSE;
		}
	}

	return TRUE;
}

void CImageFileItem::SetSymLevel(void)
{
	if( IsSymFile(m_File) ) {

		CStringArray List;

		m_File.Mid(4).Tokenize(List, ',');

		if( List.GetCount() == 3 ) {

			UINT    uCat     = wcstoul(List[0], NULL, 10);

			DWORD   dwHandle = wcstoul(List[1], NULL, 10);

			UINT    uSlot    = Sym_FindByHandle(uCat, dwHandle);

			UINT    uType    = Sym_GetType(uCat, uSlot);

			CPrintf File   = CPrintf(L"SYM:%u,%u,%u", uCat, dwHandle, uType);

			if( DoLoad(File) ) {

				m_File = File;
			}
		}
	}
}

void CImageFileItem::FixType(void)
{
	if( IsSymFile(m_File) ) {

		CStringArray List;

		m_File.Mid(4).Tokenize(List, ',');

		if( List.GetCount() == 3 ) {

			UINT  uCat     = wcstoul(List[0], NULL, 10);

			DWORD dwHandle = wcstoul(List[1], NULL, 10);

			UINT  uType    = wcstoul(List[2], NULL, 10);

			switch( uType ) {

				case 0:
					uType = typeBMP;
					break;

				case 1:
					uType = typeTEX;
					break;

				default:
					uType = typeEMF;
					break;
			}

			m_File.Printf(L"SYM:%u,%u,%u", uCat, dwHandle, uType);
		}
	}
}

// Persistance

void CImageFileItem::Init(void)
{
	CMetaItem::Init();

	FindCache();
}

void CImageFileItem::PostLoad(void)
{
	// TODO -- If we have a SYM: reference and it doesn't contain
	// enough commas, it must be from a previous version so we can
	// make an attempt to convert it to the new library. We could
	// also use something other than SYM: for 3.1 so that we can
	// clearly spot old vs. new symbol referneces!!!!

	FindCache();

	if( !m_File.IsEmpty() ) {

		if( m_Type == typeEMF ) {

			if( m_File.Right(3) == L"wmf" ) {

				m_Type = typeWMF;
			}
		}

		if( DoLoad(BuildFile(m_File)) ) {

			FindSizes();
		}
	}

	if( !m_Nat.cx || !m_Nat.cy ) {

		m_Nat.cx = m_Max.cx;

		m_Nat.cy = m_Max.cy;
	}

	CMetaItem::PostLoad();
}

// Property Save Filter

BOOL CImageFileItem::SaveProp(CString const &Tag) const
{
	if( Tag == L"Data" || Tag == L"Min" || Tag == L"Max" || Tag == L"Nat" ) {

		if( !m_File.IsEmpty() ) {

			if( IsSymFile(m_File) ) {

				return FALSE;
			}

			CImageManager *pImages = (CImageManager *) GetParent(2);

			return pImages->m_Include;
		}

		return TRUE;
	}

	if( Tag == L"File" ) {

		return m_File.IsEmpty() ? FALSE : TRUE;
	}

	return CMetaItem::SaveProp(Tag);
}

// Meta Data Creation

void CImageFileItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Used);
	Meta_AddString(File);
	Meta_AddInteger(Type);
	Meta_AddPoint(Min);
	Meta_AddPoint(Max);
	Meta_AddPoint(Nat);
	Meta_AddBlob(Data);
}

// Data Testing Helpers

BOOL CImageFileItem::CanAcceptEMF(IDataObject *pData)
{
	FORMATETC Fm1 = { WORD(m_cfEMF),  NULL, 1, -1, TYMED_ENHMF };

	FORMATETC Fm2 = { CF_ENHMETAFILE, NULL, 1, -1, TYMED_ENHMF };

	if( pData->QueryGetData(&Fm1) == S_OK ) {

		return TRUE;
	}

	if( pData->QueryGetData(&Fm2) == S_OK ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::CanAcceptBMP(IDataObject *pData)
{
	FORMATETC Fm1 = { WORD(m_cfBMP), NULL, 1, -1, TYMED_HGLOBAL };

	FORMATETC Fm2 = { CF_DIB,        NULL, 1, -1, TYMED_HGLOBAL };

	if( pData->QueryGetData(&Fm1) == S_OK ) {

		return TRUE;
	}

	if( pData->QueryGetData(&Fm2) == S_OK ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::CanAcceptTEX(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfTEX), NULL, 1, -1, TYMED_HGLOBAL };

	return pData->QueryGetData(&Fmt) == S_OK;
}

BOOL CImageFileItem::CanAcceptFile(IDataObject *pData)
{
	FORMATETC Fm1 = { WORD(m_cfSym), NULL, 1, -1, TYMED_HGLOBAL };

	FORMATETC Fm2 = { WORD(m_cfFile), NULL, 1, -1, TYMED_HGLOBAL };

	FORMATETC Fm3 = { WORD(CF_HDROP), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->QueryGetData(&Fm1) == S_OK ) {

		return TRUE;
	}

	if( pData->GetData(&Fm2, &Med) == S_OK ) {

		HGLOBAL  hName = Med.hGlobal;

		CString  Name  = LPOLESTR(GlobalLock(hName));

		GlobalUnlock(hName);

		ReleaseStgMedium(&Med);

		return FindType(CFilename(Name).GetType());
	}

	if( pData->GetData(&Fm3, &Med) == S_OK ) {

		HGLOBAL    hDrop = Med.hGlobal;

		DROPFILES *pDrop = (DROPFILES *) GlobalLock(hDrop);

		CFilename   Name = GetDropName(pDrop);

		GlobalUnlock(hDrop);

		ReleaseStgMedium(&Med);

		return FindType(CFilename(Name).GetType());
	}

	return FALSE;
}

// Data Acceptance Helpers

BOOL CImageFileItem::AcceptEMF(IDataObject *pData)
{
	FORMATETC Fm1 = { WORD(m_cfEMF),  NULL, 1, -1, TYMED_ENHMF };

	FORMATETC Fm2 = { CF_ENHMETAFILE, NULL, 1, -1, TYMED_ENHMF };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fm1, &Med) == S_OK || pData->GetData(&Fm2, &Med) == S_OK ) {

		HENHMETAFILE hFile = Med.hEnhMetaFile;

		UINT         uSize = GetEnhMetaFileBits(hFile, 0, NULL);

		PBYTE        pData = GrowData(uSize);

		GetEnhMetaFileBits(hFile, uSize, pData);

		ReleaseStgMedium(&Med);

		m_Type = typeEMF;

		FindSizes();

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::AcceptBMP(IDataObject *pData)
{
	FORMATETC Fm1 = { WORD(m_cfBMP), NULL, 1, -1, TYMED_HGLOBAL };

	FORMATETC Fm2 = { CF_DIB,        NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fm1, &Med) == S_OK || pData->GetData(&Fm2, &Med) == S_OK ) {

		HGLOBAL hBitmap = Med.hGlobal;

		PCBYTE  pBitmap = PCBYTE(GlobalLock(hBitmap));

		UINT    uSize   = GlobalSize(hBitmap);

		PBYTE   pData   = GrowData(uSize);

		memcpy(pData, pBitmap, uSize);

		GlobalUnlock(hBitmap);

		ReleaseStgMedium(&Med);

		m_Type = typeBMP;

		ReduceBitmap();

		FindSizes();

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::AcceptTEX(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfTEX), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		HGLOBAL hBitmap = Med.hGlobal;

		PCBYTE  pBitmap = PCBYTE(GlobalLock(hBitmap));

		UINT    uSize   = GlobalSize(hBitmap);

		PBYTE   pData   = GrowData(uSize);

		memcpy(pData, pBitmap, uSize);

		GlobalUnlock(hBitmap);

		ReleaseStgMedium(&Med);

		m_Type = typeTEX;

		ReduceBitmap();

		FindSizes();

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::AcceptFile(IDataObject *pData)
{
	FORMATETC Fm1 = { WORD(m_cfSym), NULL, 1, -1, TYMED_HGLOBAL };

	FORMATETC Fm2 = { WORD(m_cfFile), NULL, 1, -1, TYMED_HGLOBAL };

	FORMATETC Fm3 = { WORD(CF_HDROP), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fm1, &Med) == S_OK || pData->GetData(&Fm2, &Med) == S_OK ) {

		HGLOBAL  hName = Med.hGlobal;

		CString  Name  = LPOLESTR(GlobalLock(hName));

		GlobalUnlock(hName);

		ReleaseStgMedium(&Med);

		return LoadFromFile(Name, TRUE);
	}

	if( pData->GetData(&Fm3, &Med) == S_OK ) {

		HGLOBAL    hDrop = Med.hGlobal;

		DROPFILES *pDrop = (DROPFILES *) GlobalLock(hDrop);

		CFilename   Name = GetDropName(pDrop);

		GlobalUnlock(hDrop);

		ReleaseStgMedium(&Med);

		return LoadFromFile(Name, FALSE);
	}

	return FALSE;
}

// Image Expansion

BOOL CImageFileItem::ExpandBMP(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	BITMAPINFO       *pInfo = (BITMAPINFO *) m_Data.GetPointer();

	BITMAPINFOHEADER *pHead = &pInfo->bmiHeader;

	PCBYTE            pData = PCBYTE(pInfo) + pHead->biSize + 4 * GetColorCount(pHead);

	HBITMAP           hData = CreateDIBitmap(DC,
						 pHead,
						 CBM_INIT,
						 pData,
						 pInfo,
						 DIB_RGB_COLORS
	);

	CRect Rect = CRect(Pos, Opts.m_Size);

	Opts.SetAspect(Rect, m_Nat);

	Opts.SetTransform(DC, Rect);

	if( Opts.m_Size.cx >= m_Nat.cx || Opts.m_Size.cy >= m_Nat.cy ) {

		CMemoryDC Src;

		Src.Select(CBitmap::FromHandle(hData));

		DC.SetStretchBltMode(COLORONCOLOR);

		DC.SetTextColor(afxColor(WHITE));

		DC.SetBkColor(afxColor(BLACK));

		CSize Size(abs(pHead->biWidth), abs(pHead->biHeight));

		DC.StretchBlt(Rect, Src, CRect(Size), SRCCOPY);

		Src.Deselect();
	}
	else {
		GpBitmap *pImage = NULL;

		GdipCreateBitmapFromHBITMAP(hData,
					    NULL,
					    &pImage
		);

		GpGraphics *pGraph = NULL;

		GdipCreateFromHDC(DC.GetHandle(), &pGraph);

		GdipDrawImageRectRectI(pGraph,
				       pImage,
				       Rect.left,
				       Rect.top,
				       Rect.cx(),
				       Rect.cy(),
				       0,
				       0,
				       abs(pHead->biWidth),
				       abs(pHead->biHeight),
				       UnitPixel,
				       NULL,
				       NULL,
				       NULL
		);

		GdipDeleteGraphics(pGraph);

		GdipDisposeImage(pImage);
	}

	Opts.ClearTransform(DC);

	DeleteObject(hData);

	return TRUE;
}

BOOL CImageFileItem::ExpandGDIP(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	UINT    uSize = m_Data.GetCount();

	HGLOBAL hCopy = GlobalAlloc(GHND, uSize);

	AfxAssume(hCopy);

	PBYTE   pCopy = PBYTE(GlobalLock(hCopy));

	memcpy(pCopy, m_Data.GetPointer(), uSize);

	GlobalUnlock(hCopy);

	IStream *pStream = NULL;

	GpImage *pImage  = NULL;

	CreateStreamOnHGlobal(hCopy, TRUE, &pStream);

	GdipLoadImageFromStream(pStream, &pImage);

	CRect Rect = CRect(Pos, Opts.m_Size);

	Opts.SetAspect(Rect, m_Nat);

	Opts.SetTransform(DC, Rect);

	if( pImage ) {

		GpGraphics *pGraph = NULL;

		GdipCreateFromHDC(DC.GetHandle(), &pGraph);

		GdipDrawImageRectI(pGraph,
				   pImage,
				   Rect.left,
				   Rect.top,
				   Rect.cx(),
				   Rect.cy()
		);

		GdipDeleteGraphics(pGraph);

		GdipDisposeImage(pImage);
	}
	else {
		DC.FillRect(Rect, afxBrush(BLACK));

		DC.FrameRect(Rect, afxBrush(WHITE));

		DC.Select(afxPen(WHITE));

		DC.MoveTo(Rect.GetTopLeft());

		DC.LineTo(Rect.GetBottomRight());

		DC.MoveTo(Rect.GetTopRight());

		DC.LineTo(Rect.GetBottomLeft());

		DC.Deselect();
	}

	Opts.ClearTransform(DC);

	pStream->Release();

	return FALSE;
}

BOOL CImageFileItem::ExpandEMF(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	UINT         uSize = m_Data.GetCount();

	PCBYTE       pData = m_Data.GetPointer();

	HENHMETAFILE hMeta = SetEnhMetaFileBits(uSize, pData);

	CRect Rect = CRect(Pos, Opts.m_Size - CSize(1, 1));

	Opts.SetAspect(Rect, m_Nat);

	Opts.SetTransform(DC, Rect);

	PlayEnhMetaFile(DC, hMeta, Rect);

	Opts.ClearTransform(DC);

	DeleteEnhMetaFile(hMeta);

	return TRUE;
}

BOOL CImageFileItem::ExpandXAML(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	if( IsSymFile(m_File) ) {

		CStringArray List;

		m_File.Mid(4).Tokenize(List, ',');

		UINT  uCat     = wcstoul(List[0], NULL, 10);
		DWORD dwHandle = wcstoul(List[1], NULL, 10);
	//	UINT  uType    = wcstoul(List[2], NULL, 10);
		UINT  uMode    = wcstoul(List[3], NULL, 10);
		DWORD Fill     = wcstoul(List[4], NULL, 16);
		DWORD Back     = wcstoul(List[5], NULL, 16);

		UINT  uSlot    = Sym_FindByHandle(uCat, dwHandle);

		if( Sym_LoadPng(uCat, uSlot, uMode, Fill, Back, Opts.m_Size, Opts.m_Keep, m_Data) ) {

			return ExpandGDIP(DC, Pos, Opts);
		}
	}

	CRect   Rect = CRect(Pos, Opts.m_Size);

	CSize   Size = Opts.m_Size;

	CString Text = CPrintf(L"(%u, %u)", Size.cx, Size.cy);

	DC.Select(afxFont(Dialog));

	DC.RoundRect(Rect, 18);

	CSize Extent = DC.GetTextExtent(Text);

	int px = (Size.cx - Extent.cx) / 2;

	int py = (Size.cy - Extent.cy) / 2;

	DC.TextOut(px, py, Text);

	DC.Deselect();

	return TRUE;
}

// Image Preview

void CImageFileItem::PreviewBMP(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	ExpandBMP(DC, Pos, Opts);
}

void CImageFileItem::PreviewGDIP(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	ExpandGDIP(DC, Pos, Opts);
}

void CImageFileItem::PreviewEMF(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	ExpandEMF(DC, Pos, Opts);
}

void CImageFileItem::PreviewXAML(CDC &DC, CPoint Pos, CImageOpts const &Opts)
{
	ExpandXAML(DC, Pos, Opts);
}

// Image Rendering

CSize CImageFileItem::RenderBMP(CByteArray &Data, CImageOpts const &Opts, UINT uBits)
{
	CSize  Size = Opts.m_Size;

	CPoint Pos  = CPoint(0, 0);

/*	if( uBits == 32 ) {

		HDC     hDC;
		PVOID   pBits;
		HBITMAP hBits;
		HGDIOBJ hOld;

		// NOTE -- BMP rendering is not alpha-aware.

		CreateRenderingBitmap(hDC, pBits, hBits, hOld, Size, FALSE);

		ExpandBMP(CDC::FromHandle(hDC), Pos, Opts);

		RenderUsingBitmap32(Data, pBits, Size, FALSE);

		DeleteRenderingBitmap(hDC, hBits, hOld);

		return Size;
		}

*/	CClientDC DispDC(NULL);

CMemoryDC WorkDC(DispDC);

CBitmap   Bitmap(DispDC, Size);

WorkDC.Select(Bitmap);

WorkDC.FillRect(Size, afxBrush(MAGENTA));

ExpandBMP(WorkDC, Pos, Opts);

WorkDC.Deselect();

return RenderViaBitmap(Data, Opts.m_Size, uBits, WorkDC, Bitmap);
}

CSize CImageFileItem::RenderGDIP(CByteArray &Data, CImageOpts const &Opts, UINT uBits)
{
	CSize  Size = Opts.m_Size;

	CPoint Pos  = CPoint(0, 0);

	if( uBits == 32 ) {

		HDC     hDC;
		PVOID   pBits;
		HBITMAP hBits;
		HGDIOBJ hOld;

		// NOTE -- GDI+ rendering is always alpha-aware.

		CreateRenderingBitmap(hDC, pBits, hBits, hOld, Size, TRUE);

		ExpandGDIP(CDC::FromHandle(hDC), Pos, Opts);

		RenderUsingBitmap32(Data, pBits, Size, TRUE);

		DeleteRenderingBitmap(hDC, hBits, hOld);

		return Size;
	}

	CClientDC DispDC(NULL);

	CMemoryDC WorkDC(DispDC);

	CBitmap   Bitmap(DispDC, Size);

	WorkDC.Select(Bitmap);

	WorkDC.FillRect(Size, afxBrush(MAGENTA));

	ExpandGDIP(WorkDC, Pos, Opts);

	WorkDC.Deselect();

	return RenderViaBitmap(Data, Opts.m_Size, uBits, WorkDC, Bitmap);
}

CSize CImageFileItem::RenderEMF(CByteArray &Data, CImageOpts const &Opts, UINT uBits)
{
	CSize  Size = Opts.m_Size;

	CPoint Pos  = CPoint(0, 0);

	if( uBits == 32 ) {

		HDC     hDC;
		PVOID   pBits;
		HBITMAP hBits;
		HGDIOBJ hOld;

		// NOTE -- EMF rendering is not alpha-aware.

		CreateRenderingBitmap(hDC, pBits, hBits, hOld, Size, FALSE);

		ExpandEMF(CDC::FromHandle(hDC), Pos, Opts);

		RenderUsingBitmap32(Data, pBits, Size, FALSE);

		DeleteRenderingBitmap(hDC, hBits, hOld);

		return Size;
	}

	CClientDC DispDC(NULL);

	CMemoryDC WorkDC(DispDC);

	CBitmap   Bitmap(DispDC, Size);

	WorkDC.Select(Bitmap);

	WorkDC.FillRect(Size, afxBrush(MAGENTA));

	ExpandEMF(WorkDC, Pos, Opts);

	WorkDC.Deselect();

	return RenderViaBitmap(Data, Opts.m_Size, uBits, WorkDC, Bitmap);
}

CSize CImageFileItem::RenderXAML(CByteArray &Data, CImageOpts const &Opts, UINT uBits)
{
	CSize  Size = Opts.m_Size;

	CPoint Pos  = CPoint(0, 0);

	if( uBits == 32 ) {

		HDC     hDC;
		PVOID   pBits;
		HBITMAP hBits;
		HGDIOBJ hOld;

		// NOTE -- GDI+ rendering is always alpha-aware.

		CreateRenderingBitmap(hDC, pBits, hBits, hOld, Size, TRUE);

		ExpandXAML(CDC::FromHandle(hDC), Pos, Opts);

		RenderUsingBitmap32(Data, pBits, Size, TRUE);

		DeleteRenderingBitmap(hDC, hBits, hOld);

		return Size;
	}

	CClientDC DispDC(NULL);

	CMemoryDC WorkDC(DispDC);

	CBitmap   Bitmap(DispDC, Size);

	WorkDC.Select(Bitmap);

	WorkDC.FillRect(Size, afxBrush(MAGENTA));

	ExpandXAML(WorkDC, Pos, Opts);

	WorkDC.Deselect();

	return RenderViaBitmap(Data, Opts.m_Size, uBits, WorkDC, Bitmap);
}

void CImageFileItem::CreateRenderingBitmap(HDC &hDC, PVOID &pBits, HBITMAP &hBits, HBITMAP &hOld, CSize Size, BOOL fAlpha)
{
	// NOTE -- The fAlpha parameter indicates if the drawing done into this
	// bitmap will by functions that are alpha aware. If it is true, we load
	// the bitmap with zeros and expect the drawing process to set the alpha
	// data correctly. If not, we load with 255s and use this pattern to spot
	// pixels that are to be left transparent, while also adding the alpha.

	BITMAPINFO bmi;

	ZeroMemory(&bmi, sizeof(bmi));

	UINT cb = 4 * Size.cx * Size.cy;

	bmi.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth       = +Size.cx;
	bmi.bmiHeader.biHeight      = -Size.cy;
	bmi.bmiHeader.biPlanes      = 1;
	bmi.bmiHeader.biBitCount    = 32;
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biSizeImage   = cb;

	hDC   = CreateCompatibleDC(NULL);

	pBits = NULL;

	hBits = CreateDIBSection(hDC, &bmi, DIB_RGB_COLORS, &pBits, NULL, 0);

	hOld  = SelectObject(hDC, hBits);

	memset(pBits, fAlpha ? 0 : 255, cb);
}

void CImageFileItem::RenderUsingBitmap16(CByteArray &Data, PVOID pBits, CSize Size, BOOL fAlpha)
{
	// TODO -- This would use the same trick as below but to render
	// in sixteen bit form. It would still be more efficient than
	// the conversion method we're using in RenderViaBitmap. Ideally
	// we would render into the final downloadable bitmap format, but
	// that would need the GDI to handle the formats correctly.
}

void CImageFileItem::RenderUsingBitmap32(CByteArray &Data, PVOID pBits, CSize Size, BOOL fAlpha)
{
	UINT cq  = Size.cx * Size.cy;

	UINT cb  = 4 * cq;

	UINT rop = ropSRCCOPY;

	if( fAlpha ) {

		// NOTE -- For alpha enabled bitmaps, we scan the data looking for
		// transparent pixels with zero alpha, and for any intermediate pixels
		// with non-zero but non-maximum alpha. We use this to set the correct
		// ROP code to let the runtime know how to display the bitmap.

		PDWORD p = PDWORD(pBits);

		for( UINT n = 0; n < cq; n++ ) {

			BYTE a = *p++ >> 24;

			if( !a ) {

				rop |= ropTrans;
			}
			else {
				if( BYTE(a+1) ) {

					rop &= ~ropTrans;

					rop |=  ropBlend;

					break;
				}
			}
		}
	}
	else {
		// NOTE -- For non-alpha enabled bitmaps, we can looking for pixels
		// left at all 255s, and indicate that these are transparent. For any
		// other pixels, we add the correct alpha value to the pixel.

		PDWORD p = PDWORD(pBits);

		for( UINT n = 0; n < cq; n++ ) {

			if( !(1 + *p) ) {

				*p   = 0;

				rop |= ropTrans;
			}
			else
				*p  |= (255 << 24);

			p++;
		}
	}

	Data.Empty();

	Data.Expand(8 + cb);

	Data.Append(LOBYTE(rop));
	Data.Append(HIBYTE(rop));

	Data.Append(LOBYTE(4 * Size.cx));
	Data.Append(HIBYTE(4 * Size.cx));

	Data.Append(LOBYTE(Size.cx));
	Data.Append(HIBYTE(Size.cx));

	Data.Append(LOBYTE(Size.cy));
	Data.Append(HIBYTE(Size.cy));

	Data.Append(PBYTE(pBits), cb);
}

void CImageFileItem::DeleteRenderingBitmap(HDC hDC, HBITMAP hBits, HBITMAP hOld)
{
	SelectObject(hDC, hOld);

	DeleteObject(hBits);

	DeleteDC(hDC);
}

// Rendering Helper

CSize CImageFileItem::RenderViaBitmap(CByteArray &Data, CSize Req, UINT uBits, CDC &DC, CBitmap &Map)
{
	UINT        uInfo = sizeof(BITMAPINFO);

	BITMAPINFO *pInfo = (BITMAPINFO *) new BYTE[uInfo];

	memset(pInfo, 0, uInfo);

	pInfo->bmiHeader.biSize		 = sizeof(pInfo->bmiHeader);
	pInfo->bmiHeader.biWidth	 = +Req.cx;
	pInfo->bmiHeader.biHeight	 = -Req.cy;
	pInfo->bmiHeader.biPlanes	 = 1;
	pInfo->bmiHeader.biBitCount	 = WORD(uBits);
	pInfo->bmiHeader.biCompression	 = BI_RGB;
	pInfo->bmiHeader.biSizeImage	 = 0;
	pInfo->bmiHeader.biXPelsPerMeter = 0;
	pInfo->bmiHeader.biYPelsPerMeter = 0;
	pInfo->bmiHeader.biClrUsed	 = 0;
	pInfo->bmiHeader.biClrImportant	 = 0;

	UINT  uStep = uBits / 8;

	UINT  uSpan = Round(Req.cx, 32 / uBits);

	UINT  uSize = uSpan * Req.cy;

	UINT  uData = uSize * uStep;

	PBYTE pData = new BYTE[uData];

	GetDIBits(DC, Map, 0, Req.cy, pData, pInfo, DIB_RGB_COLORS);

	UINT rop = ropSRCCOPY;

	if( uBits == 16 ) {

		// NOTE -- This data is incorrectly formatted and does not
		// have the transparent pixels mapped correctly. We will fix
		// all this stuff when making the download image.

		COLOR trn = GetRGB(0x1F, 0x00, 0x1F);

		PWORD pic = PWORD(pData);

		for( UINT n = 0; n < uSize; n++ ) {

			if( pic[n] == trn ) {

				rop |= ropTrans;

				break;
			}
		}
	}
	else {
		DWORD  trn = RGB(0xFF, 0x00, 0xFF);

		PDWORD pic = PDWORD(pData);

		for( UINT n = 0; n < uSize; n++ ) {

			if( (pic[n] & 0xFFFFFF) == trn ) {

				pic[n]  = 0x00FFFFFF;

				rop    |= ropTrans;
			}
			else
				pic[n] |= 0xFF000000;
		}
	}

	Data.Empty();

	Data.Expand(8 + uData);

	Data.Append(LOBYTE(rop));
	Data.Append(HIBYTE(rop));

	Data.Append(LOBYTE(uSpan * uStep));
	Data.Append(HIBYTE(uSpan * uStep));

	Data.Append(LOBYTE(Req.cx));
	Data.Append(HIBYTE(Req.cx));

	Data.Append(LOBYTE(Req.cy));
	Data.Append(HIBYTE(Req.cy));

	Data.Append(PBYTE(pData), uData);

	delete[] pData;

	delete[] pInfo;

	return Req;
}

// Cache Helpers

BOOL CImageFileItem::SearchLocalCache(CByteArray &Data, CSize &Size, CImageOpts const &Opts, UINT uBits)
{
	DWORD CRC = GetCRC32();

	for( UINT n = 0; n < m_nCache; n++ ) {

		if( m_pCache[n].m_fUsed ) {

			if( m_pCache[n].m_CRC == CRC ) {

				if( m_pCache[n].m_Opts == Opts ) {

					if( m_pCache[n].m_uBits == uBits ) {

						Data.Append(m_pCache[n].m_Data);

						Size.cx = PWORD(Data.GetPointer())[2];

						Size.cy = PWORD(Data.GetPointer())[3];

						m_pCache[n].m_uHit = GetTickCount();

						return TRUE;
					}
				}
			}
		}
	}

	return FALSE;
}

BOOL CImageFileItem::SearchViewList(CByteArray &Data, CSize &Size, CImageOpts const &Opts, UINT uBits)
{
	if( uBits == 16 ) {

		CImageViewList *pList = ((CImageManager *) GetParent(2))->m_pViews;

		INDEX Index = pList->GetHead();

		while( !pList->Failed(Index) ) {

			CImageViewItem *pView = pList->GetItem(Index);

			if( pView->m_Data.GetCount() ) {

				if( pView->m_Image == GetIndex() ) {

					if( pView->m_Opts == Opts ) {

						Data.Append(pView->m_Data);

						Size.cx = PWORD(Data.GetPointer())[2];

						Size.cy = PWORD(Data.GetPointer())[3];

						return TRUE;
					}
				}
			}

			pList->GetNext(Index);
		}
	}

	return FALSE;
}

// Relative Path Handling

CFilename CImageFileItem::StripFile(CFilename File) const
{
	CImageManager *pImages = (CImageManager *) GetParent(2);

	if( pImages->m_RelMode ) {

		CString Base = pImages->GetRelBase();

		UINT    uLen = Base.GetLength();

		if( File.Left(uLen) == Base ) {

			File = File.Mid(uLen);

			File = L"." + File;

			return File;
		}
	}

	return File;
}

CFilename CImageFileItem::BuildFile(CFilename File) const
{
	CImageManager *pImages = (CImageManager *) GetParent(2);

	if( pImages->m_RelMode ) {

		if( File.Left(2) == L".\\" ) {

			File = File.Mid(1);

			File = pImages->m_RelBase + File;

			return File;
		}
	}

	return File;
}

// Drop Name Extraction

CFilename CImageFileItem::GetDropName(DROPFILES *pDrop)
{
	if( pDrop->fWide ) {

		PCTXT pName = PCTXT(PBYTE(pDrop) + pDrop->pFiles);

		return pName;
	}
	else {
		CFilename Name;

		PCSTR pName = PCSTR(PBYTE(pDrop) + pDrop->pFiles);

		while( *pName ) {

			Name += *pName++;
		}

		return Name;
	}
}

// Implementation

void CImageFileItem::FindCache(void)
{
	CImageManager *pImages = (CImageManager *) GetParent(2);

	m_pCache = pImages->m_Cache;

	m_nCache = elements(pImages->m_Cache);
}

BOOL CImageFileItem::FindSizes(void)
{
	if( m_Type == typeBMP || m_Type == typeTEX ) {

		BITMAPINFOHEADER *pHead = (BITMAPINFOHEADER *) m_Data.GetPointer();

		m_Max.cx = abs(pHead->biWidth);

		m_Max.cy = abs(pHead->biHeight);

		m_Min.cx = (m_Max.cx + 7) / 8;

		m_Min.cy = (m_Max.cy + 7) / 8;

		m_Nat.cx = m_Max.cx;

		m_Nat.cy = m_Max.cy;

		return TRUE;
	}

	if( m_Type == typeGDIP ) {

		UINT    uSize = m_Data.GetCount();

		HGLOBAL hCopy = GlobalAlloc(GHND, uSize);

		AfxAssume(hCopy);

		PBYTE   pCopy = PBYTE(GlobalLock(hCopy));

		memcpy(pCopy, m_Data.GetPointer(), uSize);

		GlobalUnlock(hCopy);

		IStream    *pStream = NULL;

		GpImage    *pImage  = NULL;

		CreateStreamOnHGlobal(hCopy, TRUE, &pStream);

		GdipLoadImageFromStream(pStream, &pImage);

		if( pImage ) {

			GdipGetImageWidth(pImage, PUINT(&m_Max.cx));

			GdipGetImageHeight(pImage, PUINT(&m_Max.cy));

			GdipDisposeImage(pImage);
		}
		else {
			m_Max.cx = 65;

			m_Max.cy = 65;
		}

		pStream->Release();

		m_Min.cx = 32;

		m_Min.cy = 32;

		m_Nat.cx = m_Max.cx;

		m_Nat.cy = m_Max.cy;

		return TRUE;
	}

	if( m_Type == typeEMF ) {

		UINT          uSize = m_Data.GetCount();

		PCBYTE        pData = m_Data.GetPointer();

		HENHMETAFILE  hMeta = SetEnhMetaFileBits(uSize, pData);

		ENHMETAHEADER Head;

		GetEnhMetaFileHeader(hMeta, sizeof(Head), &Head);

		DeleteEnhMetaFile(hMeta);

		m_Max.cx = 1024;

		m_Max.cy = 768;

		m_Min.cx = 8;

		m_Min.cy = 8;

		int cx = Head.rclBounds.right  - Head.rclBounds.left;

		int cy = Head.rclBounds.bottom - Head.rclBounds.top;

		if( cx > cy ) {

			m_Nat.cx = 65;

			m_Nat.cy = 65 * cy / cx;
		}
		else {
			m_Nat.cy = 65;

			m_Nat.cx = 65 * cx / cy;
		}

		return TRUE;
	}

	if( m_Type == typeXAML ) {

		UINT    uSize = m_Data.GetCount();

		HGLOBAL hCopy = GlobalAlloc(GHND, uSize);

		AfxAssume(hCopy);

		PBYTE   pCopy = PBYTE(GlobalLock(hCopy));

		memcpy(pCopy, m_Data.GetPointer(), uSize);

		GlobalUnlock(hCopy);

		IStream    *pStream = NULL;

		GpImage    *pImage  = NULL;

		CreateStreamOnHGlobal(hCopy, TRUE, &pStream);

		GdipLoadImageFromStream(pStream, &pImage);

		if( pImage ) {

			GdipGetImageWidth(pImage, PUINT(&m_Max.cx));

			GdipGetImageHeight(pImage, PUINT(&m_Max.cy));

			GdipDisposeImage(pImage);
		}
		else {
			m_Max.cx = 65;

			m_Max.cy = 65;
		}

		pStream->Release();

		m_Min.cx = 32;

		m_Min.cy = 32;

		m_Nat.cx = m_Max.cx;

		m_Nat.cy = m_Max.cy;

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::DoLoad(CFilename File)
{
	if( IsSymFile(File) ) {

		CStringArray List;

		File.Mid(4).Tokenize(List, ',');

		UINT  uCat     = wcstoul(List[0], NULL, 10);
		DWORD dwHandle = wcstoul(List[1], NULL, 10);
		UINT  uType    = wcstoul(List[2], NULL, 10);
		UINT  uMode    = wcstoul(List[3], NULL, 10);
		DWORD Fill     = wcstoul(List[4], NULL, 16);
		DWORD Back     = wcstoul(List[5], NULL, 16);

		UINT  uSlot    = Sym_FindByHandle(uCat, dwHandle);

		if( uType == typeXAML ) {

			if( uSlot < NOTHING ) {

				// TODO -- We should at this point load the XAML or BAML into m_Data
				// so that it can be rendered later using the Expand call. But since
				// we can't do that right now just load the whole image so that we
				// can at least work out the sizing.

				CSize Size(200, 200);

				if( Sym_LoadPng(uCat, uSlot, uMode, Fill, Back, Size, TRUE, m_Data) ) {

					m_Type = typeXAML;

					return TRUE;
				}
			}

			return FALSE;
		}
		else {
			if( uSlot < NOTHING ) {

				if( uType == typeTEX || uType == typeBMP ) {

					Sym_LoadData(uCat, uSlot, m_Data);

					m_Type = uType;

					return TRUE;
				}

				if( uType == typeEMF ) {

					HENHMETAFILE hMeta = Sym_LoadMetaFile(uCat, uSlot);

					UINT         uSize = GetEnhMetaFileBits(hMeta, 0, NULL);

					PBYTE        pData = GrowData(uSize);

					GetEnhMetaFileBits(hMeta, uSize, pData);

					DeleteEnhMetaFile(hMeta);

					m_Type = typeEMF;

					return TRUE;
				}
			}
		}

		return FALSE;
	}
	else {
		HANDLE hFile = File.OpenReadSeq();

		if( hFile != INVALID_HANDLE_VALUE ) {

			UINT uBase = FindBase(hFile);

			if( uBase < NOTHING ) {

				SetFilePointer(hFile, uBase, NULL, FILE_BEGIN);

				DWORD uSize = GetFileSize(hFile, NULL) - uBase;

				PBYTE pData = GrowData(uSize);

				SetFilePointer(hFile, uBase, NULL, FILE_BEGIN);

				ReadFile(hFile, pData, uSize, &uSize, NULL);

				CloseHandle(hFile);

				ConvertWMF();

				return TRUE;
			}

			CloseHandle(hFile);
		}
	}

	return FALSE;
}

BOOL CImageFileItem::FindType(CString Type)
{
	if( Type == L"bmp" ) { m_Type = typeBMP;  return TRUE; }
	if( Type == L"jpg" ) { m_Type = typeGDIP; return TRUE; }
	if( Type == L"jpeg" ) { m_Type = typeGDIP; return TRUE; }
	if( Type == L"gif" ) { m_Type = typeGDIP; return TRUE; }
	if( Type == L"png" ) { m_Type = typeGDIP; return TRUE; }
	if( Type == L"tif" ) { m_Type = typeGDIP; return TRUE; }
	if( Type == L"tiff" ) { m_Type = typeGDIP; return TRUE; }
	if( Type == L"emf" ) { m_Type = typeEMF;  return TRUE; }
	if( Type == L"wmf" ) { m_Type = typeWMF;  return TRUE; }

	return FALSE;
}

PBYTE CImageFileItem::GrowData(UINT uSize)
{
	m_Data.SetCount(uSize);

	m_fDirty = TRUE;

	return PBYTE(m_Data.GetPointer());
}

UINT CImageFileItem::FindBase(HANDLE hFile)
{
	if( m_Type == typeBMP ) {

		BITMAPFILEHEADER Head;

		PBYTE pData = PBYTE(&Head);

		DWORD uSize = sizeof(Head);

		ReadFile(hFile, pData, uSize, &uSize, NULL);

		if( Head.bfType == 'MB' ) {

			return sizeof(Head);
		}

		return NOTHING;
	}

	if( m_Type == typeGDIP ) {

		return 0;
	}

	if( m_Type == typeEMF ) {

		return 0;
	}

	if( m_Type == typeWMF ) {

		return 22;
	}

	return NOTHING;
}

BOOL CImageFileItem::ConvertWMF(void)
{
	if( m_Type == typeWMF ) {

		HENHMETAFILE hFile = SetWinMetaFileBits(m_Data.GetCount(),
							m_Data.GetPointer(),
							NULL,
							NULL
		);

		UINT  uSize = GetEnhMetaFileBits(hFile, 0, NULL);

		PBYTE pData = GrowData(uSize);

		GetEnhMetaFileBits(hFile, uSize, pData);

		DeleteEnhMetaFile(hFile);

		m_Type = typeEMF;

		return TRUE;
	}

	return FALSE;
}

BOOL CImageFileItem::ReduceBitmap(void)
{
	if( m_Type == typeBMP || m_Type == typeTEX ) {

		BITMAPINFOHEADER *pHead = (BITMAPINFOHEADER *) m_Data.GetPointer();

		if( pHead->biBitCount >= 16 ) {

			// OPTIM -- Reduce image to 16 bit to avoid too much
			// data being stored in the database file. We do not
			// bother if we have the source BMP file available.

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

UINT CImageFileItem::GetColorCount(BITMAPINFOHEADER *pHead)
{
	switch( pHead->biBitCount ) {

		case 24:
			return 0;

		case 32:
			return 0;
	}

	if( pHead->biClrUsed ) {

		return pHead->biClrUsed;
	}

	return 1 << pHead->biBitCount;
}

BOOL CImageFileItem::IsSymFile(CString const &File) const
{
	return File.StartsWith(L"SYM:");
}

UINT CImageFileItem::Round(UINT n, UINT f) const
{
	return f * ((n + f - 1) / f);
}

//////////////////////////////////////////////////////////////////////////
//
// Image View Item
//

// Dynamic Class

AfxImplementDynamicClass(CImageViewItem, CMetaItem);

// Constructor

CImageViewItem::CImageViewItem(void)
{
	m_Handle = HANDLE_NONE;

	m_Image  = NOTHING;
}

// Destructor

CImageViewItem::~CImageViewItem(void)
{
}

// Download Support

BOOL CImageViewItem::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_IMAGE);

	CMetaItem::MakeInitData(Init);

	UINT   uSize = m_Data.GetCount();

	PCBYTE pData = m_Data.GetPointer();

	if( uSize ) {

		Init.AddWord(1);

		Init.AddWord(WORD(m_Size.cx));

		Init.AddWord(WORD(m_Size.cy));

		Init.AddByte(BYTE(m_fTile));

		Init.AddByte(BYTE(0));

		Init.AddData(pData, uSize);

		return TRUE;
	}

	Init.AddWord(0);

	return TRUE;
}

// Operations

BOOL CImageViewItem::Render(void)
{
	CImageManager  *pImages = (CImageManager *) GetParent(2);

	CImageFileItem *pFile   = pImages->m_pFiles->GetItem(m_Image);

	if( pFile ) {

		if( pFile->IsValid() ) {

			m_fTile = pFile->IsTexture();

			m_Size  = pFile->Render(m_Data, m_Opts, 32);

			return TRUE;
		}
	}

	return FALSE;
}

// Persistance

void CImageViewItem::PostLoad(void)
{
	if( m_Handle == HANDLE_BROKE ) {

		m_Handle = HANDLE_NONE;
	}

	CMetaItem::PostLoad();

	Render();
}

// Property Save Filter

BOOL CImageViewItem::SaveProp(CString const &Tag) const
{
	if( Tag == L"Data" ) {

		return FALSE;
	}

	return CMetaItem::SaveProp(Tag);
}

// Meta Data Creation

void CImageViewItem::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);

	Meta_AddInteger(Image);

	Meta_Add(L"Size", m_Opts.m_Size, metaPoint);
	Meta_Add(L"Keep", m_Opts.m_Keep, metaInteger);
	Meta_Add(L"Rotate", m_Opts.m_Rotate, metaInteger);
	Meta_Add(L"Scale", m_Opts.m_Scale, metaInteger);
	Meta_Add(L"Flip", m_Opts.m_Flip, metaInteger);
	Meta_Add(L"TranX", m_Opts.m_dx, metaInteger);
	Meta_Add(L"TranY", m_Opts.m_dy, metaInteger);

	Meta_AddBlob(Data);
}

// End of File
