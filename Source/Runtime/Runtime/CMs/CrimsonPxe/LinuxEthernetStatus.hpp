
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxEthernetStatus_HPP

#define INCLUDE_LinuxEthernetStatus_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linux Ethernet Status
//

class CLinuxEthernetStatus :
	public IEthernetStatus
{
public:
	// Constructor
	CLinuxEthernetStatus(PCTXT pName, UINT uInst);

	// Destructor
	~CLinuxEthernetStatus(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IEthernetStatus
	BOOL METHOD GetEthernetStatus(CEthernetStatusInfo &Info);

protected:
	// Data Members
	ULONG    m_uRefs;
	CString  m_Name;
	UINT     m_uInst;

	// Implementation
	CString ReadStatus(CString Name);
};

// End of File

#endif
