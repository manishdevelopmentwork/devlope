
#include "intern.hpp"

#include "gmin8.hpp"

#include "uiinpproc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Imported Data
//

#include "import\graphite\in8dbase.h"

#include "import\graphite\in8props.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteIN8Config, CCommsItem);

// Property List

CCommsList const CGraphiteIN8Config::m_CommsList[] = {

	{ 1, "PV1",		PROPID_PV1,			usageRead,	IDS_NAME_PV1	},
	{ 1, "PV2",		PROPID_PV2,			usageRead,	IDS_NAME_PV2	},
	{ 1, "PV3",		PROPID_PV3,			usageRead,	IDS_NAME_PV3	},
	{ 1, "PV4",		PROPID_PV4,			usageRead,	IDS_NAME_PV4	},
	{ 1, "PV5",		PROPID_PV5,			usageRead,	IDS_NAME_PV5	},
	{ 1, "PV6",		PROPID_PV6,			usageRead,	IDS_NAME_PV6	},
	{ 1, "PV7",		PROPID_PV7,			usageRead,	IDS_NAME_PV7	},
	{ 1, "PV8",		PROPID_PV8,			usageRead,	IDS_NAME_PV8	},

	{ 1, "InputAlarm1",	PROPID_INPUT_ALARM1,		usageRead,	IDS_NAME_IA1	},
	{ 1, "InputAlarm2",	PROPID_INPUT_ALARM2,		usageRead,	IDS_NAME_IA2	},
	{ 1, "InputAlarm3",	PROPID_INPUT_ALARM3,		usageRead,	IDS_NAME_IA3	},
	{ 1, "InputAlarm4",	PROPID_INPUT_ALARM4,		usageRead,	IDS_NAME_IA4	},
	{ 1, "InputAlarm5",	PROPID_INPUT_ALARM5,		usageRead,	IDS_NAME_IA5	},
	{ 1, "InputAlarm6",	PROPID_INPUT_ALARM6,		usageRead,	IDS_NAME_IA6	},
	{ 1, "InputAlarm7",	PROPID_INPUT_ALARM7,		usageRead,	IDS_NAME_IA7	},
	{ 1, "InputAlarm8",	PROPID_INPUT_ALARM8,		usageRead,	IDS_NAME_IA8	},

	{ 2, "InputFilter",	PROPID_INPUT_FILTER,		usageWriteBoth,	IDS_NAME_IF	},

	{ 2, "ProcMin1",	PROPID_PROC_MIN1,		usageWriteBoth,	IDS_NAME_PMI1	},
	{ 2, "ProcMin2",	PROPID_PROC_MIN2,		usageWriteBoth,	IDS_NAME_PMI2	},
	{ 2, "ProcMin3",	PROPID_PROC_MIN3,		usageWriteBoth,	IDS_NAME_PMI3	},
	{ 2, "ProcMin4",	PROPID_PROC_MIN4,		usageWriteBoth,	IDS_NAME_PMI4	},
	{ 2, "ProcMin5",	PROPID_PROC_MIN5,		usageWriteBoth,	IDS_NAME_PMI5	},
	{ 2, "ProcMin6",	PROPID_PROC_MIN6,		usageWriteBoth,	IDS_NAME_PMI6	},
	{ 2, "ProcMin7",	PROPID_PROC_MIN7,		usageWriteBoth,	IDS_NAME_PMI7	},
	{ 2, "ProcMin8",	PROPID_PROC_MIN8,		usageWriteBoth,	IDS_NAME_PMI8	},

	{ 2, "ProcMax1",	PROPID_PROC_MAX1,		usageWriteBoth,	IDS_NAME_PMA1	},
	{ 2, "ProcMax2",	PROPID_PROC_MAX2,		usageWriteBoth,	IDS_NAME_PMA2	},
	{ 2, "ProcMax3",	PROPID_PROC_MAX3,		usageWriteBoth,	IDS_NAME_PMA3	},
	{ 2, "ProcMax4",	PROPID_PROC_MAX4,		usageWriteBoth,	IDS_NAME_PMA4	},
	{ 2, "ProcMax5",	PROPID_PROC_MAX5,		usageWriteBoth,	IDS_NAME_PMA5	},
	{ 2, "ProcMax6",	PROPID_PROC_MAX6,		usageWriteBoth,	IDS_NAME_PMA6	},
	{ 2, "ProcMax7",	PROPID_PROC_MAX7,		usageWriteBoth,	IDS_NAME_PMA7	},
	{ 2, "ProcMax8",	PROPID_PROC_MAX8,		usageWriteBoth,	IDS_NAME_PMA8	},

	{ 0, "PIRange",		PROPID_PI_RANGE,		usageWriteInit,	IDS_NAME_PIR	},
	{ 0, "PVRange",		PROPID_PV_RANGE,		usageWriteInit,	IDS_NAME_PVR	},

	{ 0, "ChanEnable1",	PROPID_CHAN_ENABLE1,		usageWriteInit,	IDS_NAME_CE1	},
	{ 0, "ChanEnable2",	PROPID_CHAN_ENABLE2,		usageWriteInit,	IDS_NAME_CE2	},
	{ 0, "ChanEnable3",	PROPID_CHAN_ENABLE3,		usageWriteInit,	IDS_NAME_CE3	},
	{ 0, "ChanEnable4",	PROPID_CHAN_ENABLE4,		usageWriteInit,	IDS_NAME_CE4	},
	{ 0, "ChanEnable5",	PROPID_CHAN_ENABLE5,		usageWriteInit,	IDS_NAME_CE5	},
	{ 0, "ChanEnable6",	PROPID_CHAN_ENABLE6,		usageWriteInit,	IDS_NAME_CE6	},
	{ 0, "ChanEnable7",	PROPID_CHAN_ENABLE7,		usageWriteInit,	IDS_NAME_CE7	},
	{ 0, "ChanEnable8",	PROPID_CHAN_ENABLE8,		usageWriteInit,	IDS_NAME_CE8	},

	{ 0, "SquareRt1",	PROPID_SQUARE_RT1,		usageWriteInit,	IDS_NAME_SQRT1	},
	{ 0, "SquareRt2",	PROPID_SQUARE_RT2,		usageWriteInit,	IDS_NAME_SQRT2	},
	{ 0, "SquareRt3",	PROPID_SQUARE_RT3,		usageWriteInit,	IDS_NAME_SQRT3	},
	{ 0, "SquareRt4",	PROPID_SQUARE_RT4,		usageWriteInit,	IDS_NAME_SQRT4	},
	{ 0, "SquareRt5",	PROPID_SQUARE_RT5,		usageWriteInit,	IDS_NAME_SQRT5	},
	{ 0, "SquareRt6",	PROPID_SQUARE_RT6,		usageWriteInit,	IDS_NAME_SQRT6	},
	{ 0, "SquareRt7",	PROPID_SQUARE_RT7,		usageWriteInit,	IDS_NAME_SQRT7	},
	{ 0, "SquareRt8",	PROPID_SQUARE_RT8,		usageWriteInit,	IDS_NAME_SQRT8	},

	};

// Constructor

CGraphiteIN8Config::CGraphiteIN8Config(void)
{
	m_PIRange	= 2;
	m_PVRange	= 1;

	m_ProcDP1	= 2;
	m_ProcDP2	= 2;
	m_ProcDP3	= 2;
	m_ProcDP4	= 2;
	m_ProcDP5	= 2;
	m_ProcDP6	= 2;
	m_ProcDP7	= 2;
	m_ProcDP8	= 2;

	m_InputFilter	= 20;

	m_ProcMin1	= 0;
	m_ProcMin2	= 0;
	m_ProcMin3	= 0;
	m_ProcMin4	= 0;
	m_ProcMin5	= 0;
	m_ProcMin6	= 0;
	m_ProcMin7	= 0;
	m_ProcMin8	= 0;

	m_ProcMax1	= 10000;
	m_ProcMax2	= 10000;
	m_ProcMax3	= 10000;
	m_ProcMax4	= 10000;
	m_ProcMax5	= 10000;
	m_ProcMax6	= 10000;
	m_ProcMax7	= 10000;
	m_ProcMax8	= 10000;

	m_SquareRt1	= FALSE;
	m_SquareRt2	= FALSE;
	m_SquareRt3	= FALSE;
	m_SquareRt4	= FALSE;
	m_SquareRt5	= FALSE;
	m_SquareRt6	= FALSE;
	m_SquareRt7	= FALSE;
	m_SquareRt8	= FALSE;

	m_ChanEnable1	= TRUE;
	m_ChanEnable2	= TRUE;
	m_ChanEnable3	= TRUE;
	m_ChanEnable4	= TRUE;
	m_ChanEnable5	= TRUE;
	m_ChanEnable6	= TRUE;
	m_ChanEnable7	= TRUE;
	m_ChanEnable8	= TRUE;

	m_uCommsCount = elements(m_CommsList);
		
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CGraphiteIN8Config::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_STATUS);

		case 2:	return CString(IDS_MODULE_CTRL);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CGraphiteIN8Config::GetPageCount(void)
{
	return 0;
	}

CString CGraphiteIN8Config::GetPageName(UINT n)
{
	return L"";
	}

CViewWnd * CGraphiteIN8Config::CreatePage(UINT n)
{
	return NULL;
	}

// Conversion

BOOL CGraphiteIN8Config::Convert(CPropValue *pValue)
{
	if( pValue ) {

		ImportNumber(pValue, L"PIRange");

		ImportNumber(pValue, L"PVRange");
		
		ImportNumber(pValue, L"InputFilter");

		for( UINT n = 0; n < 8; n ++ ) {
			
			ImportNumber(pValue, CPrintf(L"ProcDP%d",     n+1), CPrintf(L"ProcessDP%d",  n+1));

			ImportNumber(pValue, CPrintf(L"ProcMin%d",    n+1), CPrintf(L"ProcessMin%d", n+1));
			
			ImportNumber(pValue, CPrintf(L"ProcMax%d",    n+1), CPrintf(L"ProcessMax%d", n+1));
			
			ImportNumber(pValue, CPrintf(L"SquareRt%d",   n+1), CPrintf(L"SquareRt%d",   n+1));
			
			ImportNumber(pValue, CPrintf(L"ChanEnable%d", n+1), CPrintf(L"ChanEnable%d", n+1));
			}		
		
		return TRUE;
		}

	return FALSE;
	}

// Property Filter

BOOL CGraphiteIN8Config::IncludeProp(WORD PropID)
{
	return TRUE;
	}

// Implementation

void CGraphiteIN8Config::AddMetaData(void)
{
	Meta_AddInteger(PIRange);
	Meta_AddInteger(PVRange);

	Meta_AddInteger(ProcDP1);
	Meta_AddInteger(ProcDP2);
	Meta_AddInteger(ProcDP3);
	Meta_AddInteger(ProcDP4);
	Meta_AddInteger(ProcDP5);
	Meta_AddInteger(ProcDP6);
	Meta_AddInteger(ProcDP7);
	Meta_AddInteger(ProcDP8);

	Meta_AddInteger(InputFilter);

	Meta_AddInteger(ProcMin1);
	Meta_AddInteger(ProcMin2);
	Meta_AddInteger(ProcMin3);
	Meta_AddInteger(ProcMin4);
	Meta_AddInteger(ProcMin5);
	Meta_AddInteger(ProcMin6);
	Meta_AddInteger(ProcMin7);
	Meta_AddInteger(ProcMin8);

	Meta_AddInteger(ProcMax1);
	Meta_AddInteger(ProcMax2);
	Meta_AddInteger(ProcMax3);
	Meta_AddInteger(ProcMax4);
	Meta_AddInteger(ProcMax5);
	Meta_AddInteger(ProcMax6);
	Meta_AddInteger(ProcMax7);
	Meta_AddInteger(ProcMax8);

	Meta_AddInteger(SquareRt1);
	Meta_AddInteger(SquareRt2);
	Meta_AddInteger(SquareRt3);
	Meta_AddInteger(SquareRt4);
	Meta_AddInteger(SquareRt5);
	Meta_AddInteger(SquareRt6);
	Meta_AddInteger(SquareRt7);
	Meta_AddInteger(SquareRt8);

	Meta_AddInteger(ChanEnable1);
	Meta_AddInteger(ChanEnable2);
	Meta_AddInteger(ChanEnable3);
	Meta_AddInteger(ChanEnable4);
	Meta_AddInteger(ChanEnable5);
	Meta_AddInteger(ChanEnable6);
	Meta_AddInteger(ChanEnable7);
	Meta_AddInteger(ChanEnable8);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteINI8Config, CGraphiteIN8Config);

// Constructor

CGraphiteINI8Config::CGraphiteINI8Config(void)
{
	}

// View Pages

UINT CGraphiteINI8Config::GetPageCount(void)
{
	return 1;
	}

CString CGraphiteINI8Config::GetPageName(UINT n)
{
	return CString(IDS_MODULE_CONFIG);
	}

CViewWnd * CGraphiteINI8Config::CreatePage(UINT n)
{
	return New CGraphiteINI8ConfigWnd;
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteINV8Config, CGraphiteIN8Config);

// Constructor

CGraphiteINV8Config::CGraphiteINV8Config(void)
{
	}

// View Pages

UINT CGraphiteINV8Config::GetPageCount(void)
{
	return 1;
	}

CString CGraphiteINV8Config::GetPageName(UINT n)
{
	return CString(IDS_MODULE_CONFIG);
	}

CViewWnd * CGraphiteINV8Config::CreatePage(UINT n)
{
	return New CGraphiteINV8ConfigWnd;
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CGraphiteINI8ConfigWnd, CGraphiteIN8ConfigWnd);

// UI Update

void CGraphiteINI8ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddInputs();

	EndPage(TRUE);
	}

// Implementation

void CGraphiteINI8ConfigWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_GEN), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));
	AddUI(m_pItem, TEXT("root"), TEXT("PIRange")); //

	EndGroup(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CGraphiteINV8ConfigWnd, CGraphiteIN8ConfigWnd);

// UI Update

void CGraphiteINV8ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddGeneral();

	AddInputs();

	EndPage(TRUE);
	}

// Implementation

void CGraphiteINV8ConfigWnd::AddGeneral(void)
{
	StartGroup(CString(IDS_MODULE_GEN), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("InputFilter"));
	AddUI(m_pItem, TEXT("root"), TEXT("PVRange")); //

	EndGroup(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CGraphiteIN8ConfigWnd, CUIViewWnd);

// Overidables

void CGraphiteIN8ConfigWnd::OnAttach(void)
{
	m_pItem   = (CGraphiteIN8Config *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("gmin8"));

	CUIViewWnd::OnAttach();
	}

void CGraphiteIN8ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		for( UINT n = 0; n < 8; n ++ ) {
			
			UpdateUI(CPrintf("ProcessDP%d", n+1));
			}

		DoEnables();
		}

	if( Tag == "PVRange" && m_pItem->m_PVRange == 2 ) {

		m_pItem->m_SquareRt1 = FALSE;
		m_pItem->m_SquareRt2 = FALSE;
		m_pItem->m_SquareRt3 = FALSE;
		m_pItem->m_SquareRt4 = FALSE;
		m_pItem->m_SquareRt5 = FALSE;
		m_pItem->m_SquareRt6 = FALSE;
		m_pItem->m_SquareRt7 = FALSE;
		m_pItem->m_SquareRt8 = FALSE;

		for( UINT n = 0; n < 8; n ++ ) {

			UpdateUI(CPrintf("SquareRt1%d", n+1));
			}
		}

	DoEnables();

	CUIInputProcess::CheckUpdate(m_pItem, Tag);
	}

void CGraphiteIN8ConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 5);

	AddColHead(L"");
	AddColHead(L"");
	AddColHead(L"");
	AddColHead(L"");
	AddColHead(L"");

	AddColHeadExtraRow();

	AddColHead(CString(IDS_MODULE_ENABLED));
	AddColHead(CString(IDS_MODULE_DECS));
	AddColHead(CPrintf(IDS_MODULE_PV_AT, L"0%"));
	AddColHead(CPrintf(IDS_MODULE_PV_AT, L"100%"));
	AddColHead(CString(IDS_MODULE_SR));

	for( UINT n = 0; n < 8; n++ ) {
	
		AddRowHead(CPrintf(CString(IDS_MODULE_CHANNEL), n+1));

		AddUI(m_pItem, TEXT("root"), CPrintf("ChanEnable%u", n+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("ProcDP%u",     n+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("ProcMin%u",    n+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("ProcMax%u",    n+1));
		AddUI(m_pItem, TEXT("root"), CPrintf("SquareRt%u",   n+1));
		}

	EndTable();
	}

// Enabling

void CGraphiteIN8ConfigWnd::DoEnables(void)
{
	if( m_pItem->m_PVRange == 2 ) {

		UINT n;

		for( n = 0; n < 8; n ++ ) {

			EnableUI(CPrintf("SquareRt%d", n+1),    FALSE);
			}
		}
	else {
		}
	}

// End of File
