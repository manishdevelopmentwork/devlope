/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** TCP/IP.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** TCP/IP object 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************/


#ifndef TCPIP_H
#define TCPIP_H

#define READ_CONFIG_FROM_REGISTRY


#define TCPIP_CLASS						0xF5			/* TCP/IP class identifier */
#define TCPIP_CLASS_REVISION			1				/* TCP/IP class revision */

/* Class and instance attribute numbers */
#define TCPIP_CLASS_ATTR_REVISION              1
#define TCPIP_CLASS_ATTR_MAX_INSTANCE          2
#define TCPIP_CLASS_ATTR_NUM_INSTANCES         3
#define TCPIP_CLASS_ATTR_OPT_ATTR_LIST         4
#define TCPIP_CLASS_ATTR_OPT_SVC_LIST          5
#define TCPIP_CLASS_ATTR_MAX_CLASS_ATTR        6
#define TCPIP_CLASS_ATTR_MAX_INST_ATTR         7

#define TCPIP_INST_ATTR_INTERFACE_STATUS	   1
#define TCPIP_INST_ATTR_CONFIG_CAPABILITY      2
#define TCPIP_INST_ATTR_CONFIG_CONTROL         3
#define TCPIP_INST_ATTR_LINK_PATH              4
#define TCPIP_INST_ATTR_INTERFACE_CONFIG       5
#define TCPIP_INST_ATTR_HOST_NAME              6

/* Attribute data lengths */
#define TCPIP_MAX_DOMAIN_LENGTH       48
#define TCPIP_MAX_HOST_LENGTH         64

/* Status attribute bit-map */
#define TCPIP_STATUS_NOT_CONFIGURED        0x00000000
#define TCPIP_STATUS_VALID_CONFIGURATION   0x00000001

/* Capability attribute bit-map */
#define TCPIP_CAPABILITY_BOOTP             0x00000001
#define TCPIP_CAPABILITY_DNS               0x00000002
#define TCPIP_CAPABILITY_DHCP              0x00000004
#define TCPIP_CAPABILITY_DHCP_DNS          0x00000008
#define TCPIP_CAPABILITY_SETTABLE_CONFIG   0x00000010

/* Control attribute bit-map */
#define TCPIP_CONTROL_CONFIG_BITMASK       0x00000007
#define TCPIP_CONTROL_USE_STORED_CONFIG    0x00000000
#define TCPIP_CONTROL_BOOTP                0x00000001
#define TCPIP_CONTROL_DHCP                 0x00000002
#define TCPIP_CONTROL_DNS_ENABLE           0x00000010

#define TCPIP_MAX_LINK_PATH_LENGTH		   12


/* Class attribute structure */
typedef struct tagTCPIP_CLASS_ATTR
{
   UINT16  iRevision;
   UINT16  iMaxInstance;
   UINT16  iNumInstances;
}
TCPIP_CLASS_ATTR;

#define TCPIP_CLASS_ATTR_SIZE	6


/* Instance attribute structure */
typedef struct tagTCPIP_INTERFACE_CONFIG
{
   UINT32   lIpAddr;
   UINT32   lSubnetMask;
   UINT32   lGatewayAddr;
   UINT32   lNameServer;
   UINT32   lNameServer2;
   UINT8	szDomainName[TCPIP_MAX_DOMAIN_LENGTH+1];   
} 
TCPIP_INTERFACE_CONFIG;

#define TCPIP_INTERFACE_CONFIG_SIZE		20

typedef struct tagTCPIP_INTERFACE_INSTANCE_ATTRIBUTES
{
	UINT32	lInterfaceStatus;
	UINT32	lConfigCapability;
	UINT32	lConfigControl;
	UINT16	iLinkObjPathSize;
	UINT8	LinkObjPath[TCPIP_MAX_LINK_PATH_LENGTH];
	TCPIP_INTERFACE_CONFIG	InterfaceConfig;
	UINT8	szHostName[TCPIP_MAX_HOST_LENGTH+1];    
	
}
TCPIP_INTERFACE_INSTANCE_ATTRIBUTES;

extern void tcpipInit();
extern void tcpipReadConfigFromRegistry();
extern void tcpipParseClassInstanceRequest( INT32 nRequest );
extern void tcpipParseClassRequest( INT32 nRequest );
extern void tcpipSendClassAttrAll( INT32 nRequest );
extern void tcpipSendClassAttrSingle( INT32 nRequest );
extern void tcpipParseInstanceRequest( INT32 nRequest );
extern void tcpipSendInstanceAttrAll( INT32 nRequest );
extern void tcpipSendInstanceAttrSingle( INT32 nRequest );
extern void tcpipSetInstanceAttrAll( INT32 nRequest );
extern void tcpipSetInstanceAttrSingle( INT32 nRequest );

#endif /* #ifndef TCPIP_H */


