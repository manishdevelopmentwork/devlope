
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagTriggerNumeric_HPP

#define INCLUDE_TagTriggerNumeric_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TagTrigger.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Numeric Tag Trigger
//

class DLLNOT CTagTriggerNumeric : public CTagTrigger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagTriggerNumeric(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		BOOL NeedSetpoint(void) const;
		BOOL HasSetpoint(void) const;

		// Operations
		void UpdateTypes(BOOL fComp);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pValue;
		CCodedItem * m_pHyst;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
