
#include "dnp3sers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Serial Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CDnp3SerialSlave::CDnp3SerialSlave(void)
{
	m_Ident = DRIVER_ID;
}

// Destructor

CDnp3SerialSlave::~CDnp3SerialSlave(void)
{
}

// Management

void MCALL CDnp3SerialSlave::Attach(IPortObject *pPort)
{
	CDnp3Base::Attach(pPort);

	m_pData = MakeDoubleDataHandler();

	m_pData->SetTxSize(2048);

	pPort->Bind(m_pData);
}

// Config

void MCALL CDnp3SerialSlave::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
}

// Device

CCODE MCALL CDnp3SerialSlave::DeviceOpen(IDevice *pDevice)
{
	CDnp3Base::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Dest   = GetWord(pData);

			m_pCtx->m_TO     = GetWord(pData);

			m_pCtx->m_Link	 = GetLong(pData);

			m_pCtx->m_eMask  = GetByte(pData);

			m_pCtx->m_pEvent = NULL;

			GetEventConfig(pData);

			m_pCtx->m_bAiCalc  = GetByte(pData);

			m_pCtx->m_pSession = NULL;

			m_pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CDnp3SerialSlave::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CloseSession();

		delete[] m_pCtx->m_pEvent;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CDnp3Base::DeviceClose(fPersist);
}

// Channels

void CDnp3SerialSlave::OpenChannel(void)
{
	if( m_pDnp ) {

		if( !m_pChannel ) {

			if( (m_pChannel = m_pDnp->OpenChannel(m_pData, m_Source, FALSE)) ) {

				m_fOpen = TRUE;
			}
		}
	}
}

BOOL CDnp3SerialSlave::CloseChannel(void)
{
	if( CDnp3Base::CloseChannel() ) {

		if( m_pDnp->CloseChannel(m_pData, m_Source, FALSE) ) {

			m_pChannel = NULL;

			m_fOpen    = FALSE;
		}
	}

	return !m_fOpen;
}

// End of File

