
#include "intern.hpp"

#include "rawcan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw CAN Port Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CRawCANDriver::CRawCANDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_pPort     = NULL;

	m_uRxIndex  = 0;

	m_uTxIndex  = 0;

	m_pTxPDU    = NULL;

	m_pRxPDU    = NULL;

	m_RxMax	    = 0;

	m_TxMax	    = 0;

	m_RxMailMax = 0;

	m_TxMailMax = 0;
	}

// Destructor

CRawCANDriver::~CRawCANDriver(void)
{
	}

// Identification

WORD MCALL CRawCANDriver::GetCategory(void)
{
	return DC_COMMS_RAWPORT;
	}

// Management

void MCALL CRawCANDriver::Attach(IPortObject *pPort)
{
	if( pPort ) {

		m_pPort = pPort;

		m_pHandler = new CCAN29bitIdRawHandler(m_pHelper);

		pPort->Bind(m_pHandler);
		}
	}

void MCALL CRawCANDriver::Detach(void)
{
	m_pHandler->Release();

	if( m_pTxPDU ) {

		delete m_pTxPDU;

		m_pTxPDU = NULL;
		}

	if( m_pRxPDU ) {

		delete m_pRxPDU;

		m_pRxPDU = NULL;
		}
	}

// Configuration

void MCALL CRawCANDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		SkipUpdate(pData);

		m_RxMax = GetWord(pData);

		m_TxMax = GetWord(pData);

		m_RxMailMax = GetWord(pData);

		m_TxMailMax = GetWord(pData);
		}
	}

void MCALL CRawCANDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uFlags |= flagPrivate;

	Config.m_uFlags |= flagExtID; 
	}

// Entry Points

BOOL MCALL CRawCANDriver::MakePDU(UINT uID, UINT DLC, BOOL fExt, BOOL fRx)
{
	CPDU * pPDU   = fRx ? m_pRxPDU : m_pTxPDU;

	UINT * pIndex = fRx ? &m_uRxIndex : &m_uTxIndex;

	UINT uIndex = *pIndex;

	UINT uMax = fRx ? m_RxMax : m_TxMax;

	if( uIndex < uMax ) {

		if( !pPDU ) {

			pPDU = new CPDU[uMax];

			memset(pPDU, 0xFF, sizeof(CPDU) * uMax);

			if( fRx ) {

				m_pRxPDU = pPDU;
				}
			else {
				m_pTxPDU = pPDU;
				}
			}

		for( UINT u = 0; u < uIndex; u++ ) {

			if( pPDU[u].m_uID == uID ) {

				uIndex = u;

				break;
				}
			}		

		ID id;

		id.m_Ref = uID;

		if( m_pHandler ) {

			m_pHandler->Suspend(TRUE);

			m_pHandler->MakePDU(&id, DLC, 0, 0, 0, 0);

			m_pHandler->Suspend(FALSE);

			//m_pHandler->ShowPDUs();

			pPDU[uIndex].m_uID  = uID;

			pPDU[uIndex].m_bDLC = DLC;

			if( uIndex == *pIndex ) {

				(*pIndex)++;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL MCALL CRawCANDriver::RxPDU(UINT uID, PDWORD pData)
{
	UINT uIndex = FindID(uID, TRUE);

	if( m_pHandler && uIndex < m_RxMax ) {

		ID id;

		id.m_Ref = uID;

		PDU * pPDU = m_pHandler->FindPDU(&id, dirRx);

		if( pPDU ) {

			if( m_pHandler->HasData(pPDU) ) {

				DWORD pRecv[2];

				memset(PBYTE(pRecv), 0, 8);

				Critical(TRUE);

				memcpy(PBYTE(pRecv), pPDU->m_pData, pPDU->m_uBytes);

				Critical(FALSE);

				pData[0] = MotorToHost(pRecv[0]);
				
				if( pPDU->m_uBytes > 4 ) {

					pData[1] = MotorToHost(pRecv[1]);
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL MCALL CRawCANDriver::TxPDU(UINT uID, PDWORD pData)
{
	UINT uIndex = FindID(uID, FALSE);

	if( m_pHandler && uIndex < m_TxMax ) {

		ID id;

		id.m_Ref = uID;

		PDU * pPDU = m_pHandler->FindPDU(&id, dirTx);

		if( pPDU ) {

			DWORD pSend[2];

			for( UINT u = 0; u < elements(pSend); u++ ) {

				pSend[u] = MotorToHost(pData[u]);
				}

			memcpy(pPDU->m_pData, PBYTE(pSend), pPDU->m_uBytes);

			m_pHandler->SendPDU(pPDU, 0, transDriver);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL MCALL CRawCANDriver::MakeRxMailBox(UINT uBox, UINT uMask, UINT uFilter, UINT uDLC)
{
	return FALSE;
	}

BOOL MCALL CRawCANDriver::MakeTxMailBox(UINT uBox, UINT uId, UINT uDLC)
{
	return FALSE;
	}

BOOL MCALL CRawCANDriver::RxMail(PTXT Mail)
{
	return FALSE;
	}

BOOL MCALL CRawCANDriver::TxMailBox(UINT uBox, PDWORD pData)
{
	return FALSE;
	}

void MCALL CRawCANDriver::Service(void)
{
	while( !m_pHandler->HasUpdate() ) {

		ForceSleep(10);
		}
	}

// Data Access

UINT MCALL CRawCANDriver::GetDLC(UINT uID, BOOL fRx)
{
	UINT uIndex = FindID(uID, fRx);

	if( uIndex < NOTHING ) {

		CPDU * pPDU  = fRx ? m_pRxPDU : m_pTxPDU;

		if( pPDU ) {

			return pPDU[uIndex].m_bDLC;
			}
		}

	return NOTHING;
	}

UINT MCALL CRawCANDriver::GetMailDLC(UINT uBox)
{
	return 0;
	}

// Implementation

BOOL CRawCANDriver::SkipUpdate(LPCBYTE &pData)
{
	if( GetByte(pData) == 0x01 ) {
		       
		if( GetWord(pData) == 0x1234 ) {

			UINT uCount = GetWord(pData);

			pData += 4 * uCount;

			Item_Align4(pData);

			uCount = GetWord(pData);
				
			pData += 1 * uCount;
			
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CRawCANDriver::IsTimedOut(UINT uTime)
{
	return int(GetTickCount() - uTime - m_uTimeout) >= 0;
	}

UINT CRawCANDriver::FindID(DWORD dwID, BOOL fRx)
{
	CPDU * pPDU  = fRx ? m_pRxPDU : m_pTxPDU;

	UINT uIndex = fRx ? m_uRxIndex : m_uTxIndex;

	UINT uMax = fRx ? m_RxMax : m_TxMax;

	if( uIndex - 1 < uMax ) {
		
		for( UINT i = 0; i < uIndex; i++ ) {

			if( pPDU[i].m_uID == dwID ) {

				return i;
				}
			}
		}
		
	return NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit Identifier Handler
//

// Imports

#include "..\raw29id\r29id.cpp"

// Constructor

CCAN29bitIdRawHandler::CCAN29bitIdRawHandler(IHelper *pHelper) : CCAN29bitIdEntryRawHandler(pHelper)
{
	}

// IPortHandler

void CCAN29bitIdRawHandler::OnOpen(CSerialConfig const &Config) 
{	
	CCAN29bitIdEntryRawHandler::OnOpen(Config);

	m_pRxMail = NULL;

	m_pTxMail = NULL;

	m_uRxIndex = 0;

	m_uTxIndex = 0;

	m_fSuspend = TRUE;
	}	

void CCAN29bitIdRawHandler::OnClose(void)
{
	if( m_pRxMail ) {

		delete m_pRxMail;

		m_pRxMail = NULL;
		}

	if( m_pTxMail ) {

		delete m_pTxMail;

		m_pTxMail = NULL;
		}
	}

// Driver Access

BOOL CCAN29bitIdRawHandler::SendMail(UINT uBox, PDWORD pData)
{
	for( UINT u = 0; u < m_uTxIndex; u++ ) {

		if( m_pTxMail[u].m_Box == uBox ) {

			memcpy(m_pTxMail[u].m_bData, PBYTE(pData), m_pTxMail[u].m_bDLC);

			PDU Mail;

			Mail.m_ID.m_Ref = m_pTxMail[u].m_ID;

			Mail.m_uBytes = m_pTxMail[u].m_bDLC;

			Mail.m_pData  = PBYTE(alloca(m_pTxMail[u].m_bDLC));

			memcpy(Mail.m_pData, m_pTxMail[u].m_bData, Mail.m_uBytes);

			if( SendPDU(&Mail, FALSE) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CCAN29bitIdRawHandler::MakeRxMail(UINT uBox, UINT uMask, UINT uFilter, BYTE bDLC, UINT uMax)
{
	if( !m_pRxMail ) {

		Suspend(TRUE);

		m_pRxMail = new CRxMAIL[uMax];

		memset(m_pRxMail, 0xFF, sizeof(CRxMAIL) * uMax);

		Suspend(FALSE);

		m_uRxIndex = 0;
		}

	if( m_uRxIndex < uMax ) {

		UINT uIndex = m_uRxIndex;

		for( UINT i = 0; i < m_uRxIndex; i++ ) {

			if( m_pRxMail[i].m_Box == uBox ) {

				uIndex = i;

				Suspend(TRUE);

				break;
				}
			}

		m_pRxMail[uIndex].m_Box = uBox;

		m_pRxMail[uIndex].m_Mask = uMask;

		m_pRxMail[uIndex].m_Filter = uFilter;

		m_pRxMail[uIndex].m_bDLC = bDLC;

		m_pRxMail[uIndex].m_fDirty = FALSE;

		if( uIndex == m_uRxIndex ) {

			m_uRxIndex++;
			
			qsort(m_pRxMail, m_uRxIndex, sizeof(CRxMAIL), SortFunc);
			}

		Suspend(FALSE);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CCAN29bitIdRawHandler::MakeTxMail(UINT uBox, UINT uID, BYTE bDLC, UINT uMax)
{
	if( !m_pTxMail ) {

		Suspend(TRUE);

		m_pTxMail = new CTxMAIL[uMax];

		memset(m_pTxMail, 0xFF, sizeof(CTxMAIL) * uMax);

		m_uTxIndex = 0;

		Suspend(FALSE);
		}

	if( m_uTxIndex < uMax  ) {

		UINT uIndex = m_uTxIndex;

		for( UINT u = 0; u < m_uTxIndex; u++ ) {

			if( m_pTxMail[u].m_Box == uBox ) {

				uIndex = u;

				break;
				}
			}

		m_pTxMail[uIndex].m_Box = uBox;

		m_pTxMail[uIndex].m_ID = uID;

		m_pTxMail[uIndex].m_bDLC = bDLC;

		if( uIndex == m_uTxIndex ) {

			m_uTxIndex++;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCAN29bitIdRawHandler::GetRxMail(PTXT Mail)
{
	if( m_pRxMail ) {

		for( UINT u = 0; u < m_uRxIndex; u++ ) {

			if( m_pRxMail[u].m_fDirty ) {

				SPrintf(Mail, "%8.8x", m_pRxMail[u].m_ID);

				for( UINT i = 0; i < m_pRxMail[u].m_bDLC; i++ ) {

					char DLC[3] = {0};

					SPrintf(DLC, "%2.2x", m_pRxMail[u].m_bData[i]);

					strcat(PSTR(Mail), DLC);
					}

				m_pRxMail[u].m_fDirty = FALSE;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

UINT CCAN29bitIdRawHandler::GetMailDLC(UINT uBox)
{
	if( m_pTxMail ) {

		for( UINT u = 0; u < m_uTxIndex; u++ ) {

			if( m_pTxMail[u].m_Box == uBox ) {

				return m_pTxMail[u].m_bDLC;
				}
			}
		}
	
	return NOTHING;
	}

// Implementation

void CCAN29bitIdRawHandler::HandleFrame(void)
{
	memcpy(&m_RxData, &m_Rx, sizeof(FRAME_EXT));

	PDU * pPDU = FindPDU();

	if( pPDU ) {

		HandleGlobalRequest(pPDU, FALSE);
		}

	if( m_pRxMail ) {

		ID id;

		id.m_Ref = m_RxData.m_ID;

		for( UINT u = 0; u < m_uRxIndex; u++ ) {

			if( (id.m_Ref & m_pRxMail[u].m_Mask) == m_pRxMail[u].m_Filter ) {

				m_pRxMail[u].m_ID = id.m_Ref;

				memcpy(m_pRxMail[u].m_bData, m_RxData.m_bData, m_pRxMail[u].m_bDLC);

				m_pRxMail[u].m_fDirty = TRUE;
				
				m_fUpdate = TRUE;
				}
			}
		}
	}

// Mail Sorting

int CCAN29bitIdRawHandler::SortFunc(PCVOID p1, PCVOID p2)
{
	CRxMAIL *f1 = (CRxMAIL *) p1;

	CRxMAIL *f2 = (CRxMAIL *) p2;

	if( f1->m_Box < f2->m_Box ) return -1;
	
	if( f1->m_Box > f2->m_Box ) return +1;

	return 0;
	}

// Helpers

BOOL CCAN29bitIdRawHandler::IsThisNode(void)
{
	if( m_pRxMail ) {

		return TRUE;
		}
	
	return CCAN29bitIdEntryRawHandler::IsThisNode();
	}

// End of File
