
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

// Development

void CControlCallProgram::AddTest(void)
{
	m_pLocals->AddTest();

	m_pParams->AddTest();
	}

void CGroupPrograms::AddTest(void)
{
	CControlProject *pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));

	DWORD dwProject = pProject->GetHandle();

	for( UINT n = 0; n < 4; n ++ ) {

		if( n == 0 ) {

			CString Name = Program_FindNextName( dwProject, 
							     CControlProgram::langFBD, 
							     CControlProgram::sectEnd, 
							     0, 
							     L"Program%1!u!"
							     );
			//

			CControlProgram *pItem = New CControlMainProgram;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(Name);

			pItem->m_Section  = CControlProgram::sectEnd;

			pItem->m_Language = CControlProgram::langFBD;
			
			/*pItem->OnCreate();*/

			// Development
			
			pItem->AddTest();
			
			continue;
			}

		if( n == 1 ) {

			CString Name = Program_FindNextName( dwProject, 
							     CControlProgram::langLD, 
							     CControlProgram::sectEnd, 
							     0, 
							     L"Program%1!u!"
							     );

			//
			CControlProgram *pItem = New CControlMainProgram;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(Name);

			pItem->m_Section  = CControlProgram::sectEnd;

			pItem->m_Language = CControlProgram::langLD;
			
			/*pItem->OnCreate();*/

			// Development
			
			pItem->AddTest();
			
			continue;
			}

		if( n == 2 ) {

			CString Name = Program_FindNextName( dwProject, 
							     CControlProgram::langSFC, 
							     CControlProgram::sectSFCMain, 
							     0, 
							     L"Program%1!u!"
							     );

			//
			CControlProgram *pItem = New CControlSfcMainProgram;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(Name);

			pItem->m_Section  = CControlProgram::sectSFCMain;

			pItem->m_Language = CControlProgram::langSFC;
			
			/*pItem->OnCreate();*/

			// Development
			
			pItem->AddTest();
			
			continue;
			}

		if( n == 3 ) {

			CString Name = Program_FindNextName( dwProject, 
							     CControlProgram::langST, 
							     CControlProgram::sectEnd, 
							     0, 
							     L"Program%1!u!"
							     );

			//
			CControlProgram *pItem = New CControlCallProgram;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(CPrintf(L"Program%d", n+1));

			pItem->m_Section  = CControlProgram::sectEnd;

			pItem->m_Language = CControlProgram::langST;
			
			/*pItem->OnCreate();*/

			// Development
			
			pItem->AddTest();
			
			continue;
			}
		}
	}

// Development

void CGroupChildPrograms::AddTest(void)
{
	CControlSfcProgram *pParent = (CControlSfcProgram *) GetParent(AfxRuntimeClass(CControlSfcProgram));

	DWORD dwProject = pParent->GetProject();

	DWORD  dwParent = pParent->GetHandle();

	for( UINT n = 0; n < 4; n ++ ) {

		if( TRUE || n == 0 ) {

			CString Name = Program_FindNextName( dwProject, 
							     CControlProgram::langSFC, 
							     CControlProgram::sectSFCChild, 
							     dwParent, 
							     L"Child%1!u!"
							     );
			//

			CControlProgram *pItem = New CControlSfcChildProgram;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(Name);

			pItem->m_Section  = CControlProgram::sectSFCChild;

			pItem->m_Language = CControlProgram::langSFC;

			pItem->m_Parent   = dwParent;
			
			/*pItem->OnCreate();*/

			// Development
			
			pItem->AddTest();
			
			continue;
			}
		}
	}

// Development

void CGlobalVariables::AddTest(void)
{
/*	CControlProject *pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));

	DWORD dwProject = pProject->GetHandle();

	for( UINT n = 0; n < 4; n ++ ) {

		CString    Name = Variable_FindNextName(dwProject, L"Variable%1!u!");

		if( n == 3 ) {

			DWORD    dwType = afxDatabase->FindType(dwProject, L"DWORD");

			DWORD   dwGroup = afxDatabase->FindGroup( dwProject, L"(Retain)");

			CControlVariable *pItem = New CControlGlobalVariable;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(Name);

			pItem->m_Persist = 1;

			pItem->m_GroupIdent = dwGroup;

			pItem->m_TypeIdent  = dwType;

			pItem->OnCreate();

			continue;
			}

		if( TRUE ) {

			DWORD    dwType = afxDatabase->FindType(dwProject, L"BOOL");

			DWORD   dwGroup = afxDatabase->FindGroup( dwProject, L"(Global)");

			CControlVariable *pItem = New CControlGlobalVariable;

			m_pObjects->AppendItem(pItem);

			pItem->SetName(Name);

			pItem->m_Persist    = 0;

			pItem->m_GroupIdent = dwGroup;

			pItem->m_TypeIdent  = dwType;

			pItem->OnCreate();
			}
		}
*/	}

// Development

void CLocalVariables::AddTest(void)
{
/*	CControlProgram *pProgram = (CControlProgram *) GetParent(AfxRuntimeClass(CControlProgram));

	DWORD dwProject = pProgram->GetProject();

	CString Program = pProgram->GetName();

	DWORD   dwGroup = afxDatabase->FindGroup( dwProject, Program);

	DWORD    dwType = afxDatabase->FindType(dwProject, L"BOOL");

	for( UINT n = 0; n < 4; n ++ ) {

		CString    Name = Variable_FindNextName(dwProject, L"Local%1!u!");

		//

		CControlVariable *pItem = New CControlLocalVariable;

		m_pObjects->AppendItem(pItem);

		pItem->SetName(Name);

		pItem->m_GroupIdent = dwGroup;

		pItem->m_TypeIdent  = dwType;

		pItem->OnCreate();
		}
*/	}

// Development

void CInOutVariables::AddTest(void)
{
/*	DWORD dwProject = m_pProgram->GetProject();

	CString    Name = m_pProgram->GetName();

	DWORD   dwGroup = afxDatabase->FindGroup( dwProject, Name);

	DWORD    dwType = afxDatabase->FindType(dwProject, L"BOOL");

	for( UINT n = 0; n < 4; n ++ ) {

		CString   Name1 = Variable_FindNextName(dwProject, L"Input%1!u!");

		CString   Name2 = Variable_FindNextName(dwProject, L"Output%1!u!");

		//
		
		CControlVariable *pItem = New CControlInOutVariable;

		m_pObjects->AppendItem(pItem);

		pItem->SetName(n < 3 ? Name1 : Name2);

		pItem->m_GroupIdent = dwGroup;

		pItem->m_TypeIdent  = dwType;

		pItem->OnCreate();
		}
*/	}

// End of File
