
#include "Intern.hpp"

#include "DhcpOption.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// DHCP Option Wrapper Class
//

// Creation

void CDhcpOption::Create(BYTE Type)
{
	m_Type = Type;

	m_Len  = 0;
	}

void CDhcpOption::Create(BYTE Type, BYTE bData)
{
	m_Type = Type;

	m_Len  = 0;

	AddByte(bData);
	}

void CDhcpOption::Create(BYTE Type, PCTXT pText)
{
	m_Type = Type;

	m_Len  = 0;

	AddText(pText);
	}

void CDhcpOption::Create(BYTE Type, IPREF Ip)
{
	m_Type = Type;

	m_Len  = 0;

	AddAddr(Ip);
	}

// Attributes

BOOL CDhcpOption::IsEnd(void) const
{
	return m_Type == dhcpEND;
	}

BOOL CDhcpOption::IsPad(void) const
{
	return m_Type == dhcpPAD;
	}

BYTE CDhcpOption::GetByte(UINT &Ptr) const
{
	return m_Data[Ptr++];
	}

WORD CDhcpOption::GetWord(UINT &Ptr) const
{
	BYTE bHi = GetByte(Ptr);

	BYTE bLo = GetByte(Ptr);

	return MAKEWORD(bLo, bHi);
	}

DWORD CDhcpOption::GetLong(UINT &Ptr) const
{
	WORD wHi = GetWord(Ptr);

	WORD wLo = GetWord(Ptr);

	return MAKELONG(wLo, wHi);
	}

PCTXT CDhcpOption::GetText(UINT &Ptr) const
{
	PCTXT Data = PCTXT(m_Data + Ptr);

	UINT  Size = strlen(Data) + 1;

	Ptr += Size;

	return (Size > 64) ? "" : Data;
	}

IPADDR CDhcpOption::GetAddr(UINT &Ptr) const
{
	IPADDR Ip;

	Ip.m_b1 = GetByte(Ptr);
	Ip.m_b2 = GetByte(Ptr);
	Ip.m_b3 = GetByte(Ptr);
	Ip.m_b4 = GetByte(Ptr);

	return Ip;
	}

UINT CDhcpOption::GetSize(void) const
{
	return (IsEnd() || IsPad()) ? 1 : 2 + m_Len;
	}

// Operations

void CDhcpOption::AddByte(BYTE bData)
{
	m_Data[m_Len++] = bData;
	}

void CDhcpOption::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CDhcpOption::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CDhcpOption::AddText(PCTXT pText)
{
	PTXT pTarg = PTXT(m_Data + m_Len);

	strcpy(pTarg, pText);

	m_Len += BYTE(strlen(pText) + 1);
	}

void CDhcpOption::AddAddr(IPREF Ip)
{
	AddByte(Ip.m_b1);
	AddByte(Ip.m_b2);
	AddByte(Ip.m_b3);
	AddByte(Ip.m_b4);
	}

void CDhcpOption::AddHex(PCBYTE pData, UINT uCount)
{
	while( uCount-- ) AddHexByte(*(pData++));
	}

void CDhcpOption::AddHexByte(BYTE bData)
{
	AddHexChar(bData / 16);

	AddHexChar(bData % 16);
	}

void CDhcpOption::AddHexChar(BYTE bData)
{
	AddByte(BYTE("0123456789abcdef"[bData]));
	}

// End of File
