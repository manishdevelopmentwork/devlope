
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagFlagPage_HPP

#define INCLUDE_TagFlagPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTagFlag;

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Page
//

class CTagFlagPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTagFlagPage(CTagFlag *pTag, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CTagFlag * m_pTag;

		// Implementation
		BOOL LoadLimits(IUICreate *pView);
		BOOL LoadFormat(IUICreate *pView);
		BOOL LoadColor(IUICreate *pView);
		BOOL LoadSecurity(IUICreate *pView);
	};

// End of File

#endif
