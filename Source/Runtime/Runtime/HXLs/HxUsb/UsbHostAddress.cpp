
#include "Intern.hpp"

#include "UsbHostAddress.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Address Allocator
//

// Constructor

CUsbHostAddress::CUsbHostAddress(void)
{
	m_pLock    = Create_Mutex();

	m_fDefLock = false;
	}

// Destructor

CUsbHostAddress::~CUsbHostAddress(void)
{
	m_pLock->Release();
	}

// Operations

void CUsbHostAddress::Init(void)
{
	memset(m_Address, 0, sizeof(m_Address));
	
	m_fDefLock = false;
	}

UINT CUsbHostAddress::Allocate(void)
{
	m_pLock->Wait(FOREVER);

	for( UINT i = 0; i < elements(m_Address); i ++ ) {

		DWORD &d = m_Address[i];

		if( d != 0xFFFFFFFF ) { 

			DWORD m = 1;
			
			for( UINT n = 0; n < 32; n ++ ) {

				if( !(d & m ) ) {

					d |= m;

					m_pLock->Free();

					return i * 32 + n + 1;
					}

				m <<= 1;
				}
			}
		}
	
	m_pLock->Free();
	
	return NOTHING;
	}

void CUsbHostAddress::Free(UINT uAddr)
{
	if( --uAddr < elements(m_Address) * 32 ) {  	

		DWORD &d = m_Address[ uAddr / 32 ];

		DWORD  m = 1 << (uAddr % 32);

		m_pLock->Wait(FOREVER);	

		d &= ~m;

		m_pLock->Free();
		}
	}

bool CUsbHostAddress::LockDefault(void)
{
	Hal_Critical(true);

	if( !m_fDefLock ) {

		m_fDefLock = true;

		Hal_Critical(false);

		return true;
		}

	Hal_Critical(false);

	return false;
	}

bool CUsbHostAddress::FreeDefault(void)
{
	Hal_Critical(true);

	if( m_fDefLock ) {

		m_fDefLock = false;

		Hal_Critical(false);

		return true;
		}

	Hal_Critical(false);

	return false;
	}

// End of File
