/*
 * Platform definitions
 *
 * Copyright 2013 Red Lion Controls, Inc.
 *
 * All Rights Reserved
 *
 *
 * The definitions in this file support cross-platform used of Linux-based
 * code (open source or in-house) in Crimson DLDs.  By including it in
 * such modules, they typically compile and behave correctly in GCC (for 
 * Crimson devices like HMIs and controllers) and in MSVC (for the 
 * Windows-based emulator/simulator).
 */

#ifndef RL_PLATFORM_H
#define RL_PLATFORM_H

// ====================================================================
// In GCC, __func__ is the name of the function that the current line is
// in.  This can be used to log/trace like:
//
//    printf("Entering %s\n", __func__);
//
// Alas, this is not standardized.  Microsoft C uses "__FUNCTION__".
//
// This code is adapted from http://stackoverflow.com/questions/2281970
#if defined(_MSC_VER)
#define __func__ __FUNCTION__
#endif

// ====================================================================
// Mapping protocol data units onto structures requires that those
// structures be packed.  Sadly, GCC and MSVC do this very differently.
//
// http://stackoverflow.com/questions/1537964 provides a couple solutions.  
// Unlike using a different name (__func__ vs. __FUNCTION__), the 
// semantics of packing are different in the two environments so it is
// necessary to modify the original code to be portable via the PACK macro.
//
// Use like:
//    PACK(
//    typedef struct {
//        uint8_t aField;
//        uint16_t anotherField;
//    } ) theTypeName_t

#if defined(SWIG)
#define PACK(...) __VA_ARGS__
#elif defined(_MSC_VER)
#define PACK(__Decl__) __pragma(pack(push, 1)) __Decl__ __pragma(pack(pop))
#else // GCC
#define PACK(__Decl__) __Decl__ __attribute__ ((packed))
#endif

#endif // RL_PLATFORM_H
