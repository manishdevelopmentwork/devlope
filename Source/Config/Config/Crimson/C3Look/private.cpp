
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Private View Window
//

// Runtime Class

AfxImplementRuntimeClass(CPrivateViewWnd, CViewWnd);

// Constructor

CPrivateViewWnd::CPrivateViewWnd(void)
{
	}

// Message Map

AfxMessageMap(CPrivateViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)

	AfxMessageEnd(CPrivateViewWnd)
	};

// Message Handlers

void CPrivateViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	DC.Select(afxFont(Large));

	DC.SetBkMode(TRANSPARENT);

	DC.FillRect(Rect, afxColor(TabFace));

	CPoint Mid = Rect.GetCenter();

	CString Hidden = CString(IDS_PRIVATE_ACCESS_2);

	CSize Size = DC.GetTextExtent(Hidden);

	int xp = Mid.x - Size.cx / 2;

	int yp = Mid.y - Size.cy / 2;

	DC.TextOut(xp, yp, Hidden);

	DC.Deselect();
	}

void CPrivateViewWnd::OnSize(UINT uCode, CSize Size)
{
	Invalidate(FALSE);
	}

// End of File
