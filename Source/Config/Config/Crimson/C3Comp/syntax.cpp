
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Syntax Color Span
//

DLLAPI BOOL C3SyntaxColors(CArray <CColorSpan> &List, BOOL &fComment, PCTXT pLine, UINT uSize)
{
	CError Error(FALSE);

	CSyntaxParser *pSyntax = New CSyntaxParser;

	if( pSyntax->ParseLine(Error, fComment, pLine, uSize) ) {

		BOOL fResult = pSyntax->BuildColorList(List);

		delete pSyntax;

		return fResult;
		}

	delete pSyntax;

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Syntax Token Analysis
//

DLLAPI BOOL C3SyntaxTokens(CArray <CTokenSpan> &List, BOOL &fComment, PCTXT pLine, UINT uSize)
{
	CError Error(FALSE);

	CSyntaxParser *pSyntax = New CSyntaxParser;

	if( pSyntax->ParseLine(Error, fComment, pLine, uSize) ) {

		pSyntax->BuildTokenList(List);

		delete pSyntax;

		return TRUE;
		}

	delete pSyntax;

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Syntax Analyser
//

// Constructor

CSyntaxParser::CSyntaxParser(void)
{
	}

// Operations

BOOL CSyntaxParser::ParseLine(CError &Error, BOOL &fComment, PCTXT pText, UINT uSize)
{
	// REV3 -- We don't color direct references very well.

	if( *pText ) {

		m_pError = &Error;
		
		m_Lex.Attach(pText, uSize);

		m_Lex.PartialMode(fComment);

		for(;;) {

			GetToken();

			if( m_Token.IsGroup(groupEndOfText) ) {

				fComment = m_Lex.GetCommentState();
				
				return TRUE;
				}
			else {
				CTokenCtx Token;

				Token.m_Group  = m_Token.m_Group;

				Token.m_Code   = m_Token.m_Code;

				Token.m_Type   = m_Token.GetConst().GetType();

				Token.m_uCount = m_Token.GetEnd() - m_Token.GetStart();
				
				m_List.Append(Token);

				if( Token.m_Code == tokenUnknown ) {

					fComment = m_Lex.GetCommentState();

					return TRUE;
					}
				}
			}

		fComment = m_Lex.GetCommentState();
		}

	return FALSE;
	}

// List Building

BOOL CSyntaxParser::BuildColorList(CArray <CColorSpan> &List)
{
	CColorSpan Span;

	Span.m_Type   = 0;

	Span.m_uCount = 0;

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CTokenCtx const &Token = m_List[n];

		if( n > 0 ) {

			if( Token.m_Group == groupWhiteSpace ) {

				Span.m_uCount += Token.m_uCount;
				
				continue;
				}

			if( Span.m_Type == ToColorSpan(Token) ) {

				Span.m_uCount += Token.m_uCount;
				
				continue;
				}

			List.Append(Span);
			}

		Span.m_Type   = ToColorSpan(Token);

		Span.m_uCount = Token.m_uCount;
		}

	if( Span.m_uCount ) {

		List.Append(Span);
		}

	return Span.m_uCount > 0;
	}

BOOL CSyntaxParser::BuildTokenList(CArray <CTokenSpan> &List)
{
	// LATER -- Split comments into words and whitespace.

	CTokenSpan Span;

	for( UINT n = 0; n < m_List.GetCount(); n ++ ) {

		CTokenCtx const &Token = (CTokenCtx &) m_List[n];

		Span.m_Type   = ToColorSpan(Token);

		Span.m_uCount = Token.m_uCount;

		Span.m_Code   = Token.m_Code;

		List.Append(Span);
		}

	return TRUE;
	}

// Overridables

BOOL CSyntaxParser::FilterToken(void)
{
	return TRUE;
	}

// Implementation

UINT CSyntaxParser::ToColorSpan(CTokenCtx const &Token)
{
	switch( Token.m_Group ) {

		case groupWhiteSpace:

			switch( Token.m_Code ) {
				
				case tokenUnknown:

					return spanDefault;
				}
			
			return spanWhiteSpace;

		case groupConstant:

			switch( Token.m_Type ) {

				case typeInteger:
				case typeReal:
		
					return spanNumber;

				case typeString:

					return spanString;
				}
		
			return spanDefault;

		case groupKeyword:
			
			switch( Token.m_Code ) {
				
				case tokenWasComment:

					return spanWas;
				}
			
			return spanKeyword;

		case groupIdentifier:
			
			return spanIdentifier;

		case groupSeparator:
			
			return spanSeparator;

		case groupOperator:	
			
			switch( Token.m_Code ) {

				case tokenBlockComment:
				case tokenLineComment:

					return spanComment;
				}

			return spanOperator;

		case groupGarbage:
			
			return spanDefault;
		}

	return spanDefault;
	}

// End of File
