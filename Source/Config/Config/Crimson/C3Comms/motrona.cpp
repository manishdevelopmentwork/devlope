
#include "intern.hpp"

#include "motrona.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Motrona via LECOM Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMotronaDeviceOptions, CUIItem);

// Constructor

CMotronaDeviceOptions::CMotronaDeviceOptions(void)
{
	m_Drop = 11;

	m_Addr = 0;
	}

// UI Managament

void CMotronaDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Addr" ) {

		pWnd->EnableUI("Addr", FALSE);
		}
	}



// Download Support

BOOL CMotronaDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Addr));
       
	return TRUE;
	}

// Meta Data Creation

void CMotronaDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Addr);
	}


//////////////////////////////////////////////////////////////////////////
//
// Motrona Space Wrapper Class
//

// Constructors

CSpaceMotrona::CSpaceMotrona(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t)
{
	m_uTable	= uTable;
	m_Prefix	= p;
	m_Caption	= c;
	m_uRadix	= r;
	m_uMinimum	= n;
	m_uMaximum	= x;
	m_uType		= t;
	m_uSpan		= t;

	FindAlignment();

	FindWidth();
	}

// Matching

BOOL CSpaceMotrona::MatchSpace(CAddress const &Addr)
{	
	return (m_uTable == (Addr.a.m_Table & 0xF));
	}


// Limits

void CSpaceMotrona::GetMinimum(CAddress &Addr)
{
	CSpace::GetMinimum(Addr);
	}

void CSpaceMotrona::GetMaximum(CAddress &Addr)
{
	CSpace::GetMaximum(Addr);

	Addr.a.m_Extra  =  0xF;
	Addr.a.m_Table |= (0xF << 4);
	}

//////////////////////////////////////////////////////////////////////////
//
// Motrona via LECOM Driver
//

// Instantiator

ICommsDriver *	Create_MotronaSerialMasterDriver(void)
{
	return New CMotronaDriver;
	}

// Constructor

CMotronaDriver::CMotronaDriver(void)
{
	m_wID		= 0x4058;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Motrona";
	
	m_DriverName	= "LECOM Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Motrona";

	AddSpaces(NULL);
	}

// Binding Control

UINT CMotronaDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMotronaDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CMotronaDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMotronaDeviceOptions);
	}

// Address Management

BOOL CMotronaDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	DeleteAllSpaces();

	AddSpaces(pConfig);

	CMotronaAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CMotronaDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CMotronaDeviceOptions * pOptions = (CMotronaDeviceOptions *)pConfig;
  
	CString Type = StripType(pSpace, Text);

	if( !Text.IsEmpty() && pOptions && pSpace ) {

		Text = Text.ToUpper();

		if( pOptions->m_Addr == 0 ) {
			
			if( Text.GetLength() <= 2 ) {

				if( Text.GetAt(0) <= 'F' && Text.GetAt(1) <= 'F' ) {

					Addr.a.m_Type	= pSpace->TypeFromModifier(Type);

					Addr.a.m_Table	= pSpace->m_uTable;
	
					Addr.a.m_Extra	= 0;

					Addr.a.m_Offset = (Text.GetAt(0) << 8) | Text.GetAt(1);
									
					return TRUE;
					}

				Error.Set( "Invalid characters entered.", 0);

				return FALSE;
				}
		
			Error.Set( "Too many characters entered.", 0);

			return FALSE;
			}
	
		UINT uSub  = 0;

		/*UINT uFind = Text.Find('|');

		if( uFind < NOTHING ) {

			uSub = strtol(Text.Mid(uFind + 1), NULL, 16);

			if( uSub > 0xFF ) {

				Error.Set( "Sub code must be less than 0xFF.", 0);

				return FALSE;
				}
		       
			Text = Text.Mid(0, uFind);
			}*/

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			Addr.a.m_Extra  = uSub & 0x0F;

			Addr.a.m_Table |= uSub & 0xF0;

			return TRUE;
			}
		}


	return FALSE;
	}

BOOL CMotronaDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CAddress Address;

	Address.m_Ref = Addr.m_Ref;

	Address.a.m_Table = Addr.a.m_Table & 0xF;
	
	CSpace *pSpace = GetSpace(Address);

	if( pSpace ) {

		CMotronaDeviceOptions *	pOptions = (CMotronaDeviceOptions *) pConfig;

		UINT uOffset = Addr.a.m_Offset;

		if( pOptions ) {
			
			if( pOptions->m_Addr == 0 ) {
		
				Text.Printf("%s%c%c", pSpace->m_Prefix, 
						      HIBYTE(uOffset), 
						      LOBYTE(uOffset));
			
				return TRUE;
				}

			Text.Printf("%s%4.4X|%2.2X", pSpace->m_Prefix, 
					             Addr.a.m_Offset, 
						     Addr.a.m_Extra | (Addr.a.m_Table & 0xF0));

			return TRUE;
			}
		}


	return FALSE;
	}

// Implementation

void CMotronaDriver::AddSpaces(CItem *pConfig)
{
	CMotronaDeviceOptions * pOptions = (CMotronaDeviceOptions *) pConfig;
	
	UINT uMax = pOptions && pOptions->m_Addr ? 0xFFFF : 0x4646;

	UINT uMin = pOptions && pOptions->m_Addr ? 0x0000 : 0x2C2C;
	
//	AddSpace(New CSpaceMotrona(1, "C",  "Fixed Point Decimal Register Code",	16, uMin, uMax, addrLongAsLong));
	AddSpace(New CSpaceMotrona(2, "I",  "Integer Register Code",			16, uMin, uMax, addrLongAsLong));
//	AddSpace(New CSpaceMotrona(3, "H",  "Hexadecimal Register Code",		16, uMin, uMax, addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Motrona Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CMotronaAddrDialog, CStdAddrDialog);
		
// Constructor

CMotronaAddrDialog::CMotronaAddrDialog(CMotronaDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = "MotronaElementDlg";
	}

// Overridables

void CMotronaAddrDialog::SetAddressText(CString Text)
{
	CMotronaDeviceOptions * pOptions = (CMotronaDeviceOptions *)m_pConfig;

	if( pOptions ) {

		GetDlgItem(2002).EnableWindow(m_pSpace ? TRUE : FALSE);

		if( pOptions->m_Addr == 0 ) {

			GetDlgItem(2002).SetWindowText(Text.Mid(0,2));

			GetDlgItem(2004).ShowWindow(SW_HIDE);

			GetDlgItem(2005).ShowWindow(SW_HIDE);

			return;
			}

		GetDlgItem(2002).SetWindowText(Text.Mid(0,4));

		GetDlgItem(2005).SetWindowText(Text.Mid(5,2));

		return;
		}

	GetDlgItem(2002).EnableWindow(FALSE);
	}

CString CMotronaAddrDialog::GetAddressText(void)
{
	CString Text = "";
	
	CMotronaDeviceOptions * pOptions = (CMotronaDeviceOptions *)m_pConfig;

	if( pOptions ) {

		Text += GetDlgItem(2002).GetWindowText();

		/*if( pOptions->m_Addr ) {

			Text += "|";

			Text += GetDlgItem(2005).GetWindowText();
			}*/
		}

	return Text;
	}

// End of File
