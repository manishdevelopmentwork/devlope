
//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner
//

// Device ID's
enum {
	LLS57	= 0,
	COCAM	= 1,
	};

// Space Definitions
enum {
	SOM =  1,	// Set Operating Mode (Page 9)
	LRS =  2,	// Read Result Status Byte (Page 11)
	LRD =  3,	// Read Result Data String (Page 11)
	LES =  4,	// Ask wrong reads Status Byte (Page 11)
	LED =  5,	// Ask wrong reads Data String (Page 11)
	LXC =  6,	// Clear wrong read buffer (Page 11)
	SRG =  7,	// Start/Stop Reading Gate command (Page 13)
	MCF =  8,	// Matchcode Configuration (Page 39)
	MC1 =  9,	// Matchcode 1 (Page 39)
	MC2 = 10,	// Matchcode 2 (Page 39)
	CCF = 11,	// Code Configuration (Page 17)
	CCS = 12,	// Specific Symbology Parameter String (Page 17)
	CCL = 13,	// Code Length String (Page 17)
	DCF = 14,	// Device Configuration (Page 25)
	OPC = 15,	// Operational Counters (Page 64)
	HOC = 16,	// Host Output Configuration (Page 47)
	TFE = 17,	// Host Conf. - Splitter String Contents"
	TFH = 18,	// Host Conf. - Header String"
	TFL = 19,	// Host Conf. - Code Length List String"
	TFM = 20,	// Host Conf. - Format Mask String"
	TFS = 21,	// Host Conf. - Separator String"
	TFT = 22,	// Host Conf. - Terminator String"
	PE1 = 23,	// Percentage Evaluation String 1 (Page 59)
	PE2 = 24,	// Percentage Evaluation String 2 (Page 59)
	PE3 = 25,	// Percentage Evaluation String 3 (Page 59)
	PE4 = 26,	// Percentage Evaluation String 4 (Page 59)
	PEX = 27,	// Execute Percentage Eval Read   (Page 59)
	SOC = 28,	// Switching Output Configuration (Page 33/35)
	DST = 29,	// Device Self Test (Page 65)
	EEP = 30,	// RAM / EEPROM Transfer (Page 58)

	USR  = 96,	// Send USRC
	USRC = 97,	// User Defined String Command
	USRR = 98,	// User Defined String Response
	SCK  = 99,	// Use SICK Protocol
	};

// Response Types
enum {
	NORSP	= 0,	// None
	RACK	= 1,	// Ack/Nak
	RBYT	= 2,	// Byte
	RWRD	= 3,	// Word
	RLNG	= 4,	// Long
	RSTR	= 5,	// String
	RCAC	= 6,	// Cached
	RVAR	= 7,	// Varied
	RSPC	= 8,	// Special String
	ROPC	= 9,	// Operational Counters
	};

enum {
	STRITEM	= 0,	// String
	DECITEM	= 1,	// Decimal
	CHITEMU = 2,	// Upper case alpha
	CHITEML	= 3,	// Lower case alpha
	HEXITEM = 4,	// Hex digits
	MIXITEM = 5,	// may be alpha or numeric
	};

#define	MAXMCFSZ	13	// max 50 characters
#define	MAXCCFCSZ	 2	// max 4 chars
#define	MAXCCFLSZ	 5
#define	MAXUSRSZ	20

// char based maxes
#define	MAXLRDSZ	50
#define	MAXLEDSZ	50

#define	STARTDELIM	TRUE
#define	ENDDELIM	FALSE

#define	HOCESIZE	10
#define	HOCHSIZE	80
#define	HOCLSIZE	32
#define	HOCMSIZE	108
#define	HOCSSIZE	36

#define	CHCT2DWORD(x)	((x / sizeof(DWORD)) + 1)

// Command Definitions

struct FAR LAETUSCmdDef {
	UINT	uID;	// Table number
	char	b[5];	// Transmit string
	UINT	uOff;	// Use "uOffset" number of bytes
	UINT	uResp;	// Response Type
	};

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Driver
//

class CLaetusSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CLaetusSerialDriver(void);

		// Destructor
		~CLaetusSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT	m_bDrop;
			UINT	m_bUnit;
			
			BOOL	m_fSICK;

			// Internal Status cache
			BYTE	m_bAckStatus[20];
			BYTE	m_bSOMStatus[7];

			// Internal String Array cache
			DWORD	m_dMCFC[MAXMCFSZ+1];
			DWORD	m_dMCFD[MAXMCFSZ+1];
			BYTE	m_bCodeType;
			DWORD	m_dCCFC[2];
			DWORD	m_dCCFL[5];
			DWORD	m_dTFE[3];
			DWORD	m_dTFH[20];
			DWORD	m_dTFL[8];
			DWORD	m_dTFM[27];
			DWORD	m_dTFS[9];
			DWORD	m_dTFT[20];
			BYTE	m_bPE1[80];
			BYTE	m_bPE2[32];
			BYTE	m_bPE3[80];
			BYTE	m_bPE4[80];
			DWORD	m_dPEX;
			BYTE	m_bLRD[MAXLRDSZ];
			BYTE	m_bLED[MAXLEDSZ];
			DWORD	m_dUSRC[MAXUSRSZ];
			DWORD	m_dUSRR[MAXUSRSZ];
			};

		// Data Members
		CContext *	m_pCtx;

		LPCTXT		m_pHex;
		LPCTXT		m_pList;
		LPCTXT		m_pMCFL;
		LPCTXT		m_pMCFC;
		LPCTXT		m_pCCFL;
		LPCTXT		m_pCCFC;
		LPCTXT		m_pDCFL;
		LPCTXT		m_pDCFC;
		LPCTXT		m_pHOCL;
		LPCTXT		m_pHOCC;
		LPCTXT		m_pSOCL;
		LPCTXT		m_pSOCC;

		BYTE	m_bTx[256];
		BYTE	m_bRx[256];

		UINT	m_uPtr;
		UINT	m_uChecksum;
		UINT	m_uWriteErr;
		UINT	m_uTerminator;
		UINT	m_uLRS1;
		UINT	m_uLRS2;
		UINT	m_uLES1;
		UINT	m_uLES2;
		UINT	m_uLCtr;

		BOOL	m_fIsLLS;
		BOOL	m_fInPing;
		BOOL	m_fIsCCF;

		// Command Definition Table
		static LAETUSCmdDef CODE_SEG LaetusCL[];
		LAETUSCmdDef FAR * m_pLCL;
		LAETUSCmdDef FAR * m_pLItem;

		// Implementation
		BOOL	DoSetMode(UINT uTable);
		BOOL	SetMode(UINT uMode);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddHex (BYTE bData);
		void	AddText(PCTXT pData);
		
		// Transport Layer
		void	PutFrame(void);
		BOOL	GetFrame(void);
		BOOL	Transact(void);
		BOOL	CheckRsp(void);

		// Port Access
		UINT	Get(UINT uTime);

		// Read Handlers
		CCODE	DoRead(UINT uOffset, PDWORD pData, UINT uCount);
		CCODE	GetReadResponse(UINT uOffset, PDWORD pData, UINT uCount);
		UINT	GetMultiData (UINT uOffset, PDWORD pData, UINT uCount);
		void	ProcessMulti (PDWORD pData, UINT uOffset, UINT uCount, PBYTE pLst, PDWORD *pStrDest);
		UINT	FindDestPos  (PBYTE  pLst,  BYTE b);
		UINT	GetMCFItem   (UINT uOffset, PDWORD pData, UINT uCount);
		UINT	GetCCFItem   (UINT uOffset, PDWORD pData, UINT uCount);
		UINT	GetDCFItem   (UINT uOffset, PDWORD pData, UINT uCount);
		UINT	GetHOCItem   (UINT uOffset, PDWORD pData, UINT uCount);
		UINT	GetSOCItem   (UINT uOffset, PDWORD pData, UINT uCount);
		UINT	GetOPCItem   (UINT uOffset, PDWORD pData, UINT uCount);
		DWORD	GetMultiDWD  (PDWORD pData, UINT uSize, BYTE bType, UINT *pPos);
		void	SavePEXString(void);
		BYTE	SaveReadString(PBYTE pData);

		// Write Handlers
		CCODE	DoWrite(PDWORD pData, UINT uOffset, UINT uCount);
		void	AddWriteData(PDWORD pData, UINT uOffset, UINT uCount);
		void	AddMultiData(PDWORD pData, UINT uOffset, UINT uCount, PBYTE pStr, PDWORD *pSrcStr);
		void	AddMCFData  (PDWORD pData, UINT uOffset, UINT uCount);
		BOOL	AddRSTRData (PDWORD pData, UINT uCount,  UINT uMax, BYTE bOp, BOOL fMC);
		void	AddCCFData  (PDWORD pData, UINT uOffset, UINT uCount);
		void	AddDCFData  (PDWORD pData, UINT uOffset, UINT uCount);
		void	AddHOCData  (PDWORD pData, UINT uOffset, UINT uCount);
		void	AddSOCData  (PDWORD pData, UINT uOffset, UINT uCount);
		UINT	GetItemInfo (BYTE bItem, PBYTE pType);
		UINT	MCFItemInfo (BYTE bItem, PBYTE pType);
		UINT	CCFItemInfo (BYTE bItem, PBYTE pType);
		UINT	DCFItemInfo (BYTE bItem, PBYTE pType);
		UINT	HOCItemInfo (BYTE bItem, PBYTE pType);
		UINT	SOCItemInfo (BYTE bItem, PBYTE pType);
		UINT	FindNextCR(UINT uStartPos);

		// String and character handling
		UINT	FindCharMatch(BYTE bList, BOOL fIgnoreCCFString);
		void	SkipSpaceChars(UINT *pPos);
		UINT	SaveStringData(PBYTE pData, UINT uPos, UINT uMax, BYTE b0, BYTE b1);
		BOOL	StringHasSize(BYTE b);
		UINT	GetStringLength(BYTE b, UINT *pPos);
		UINT	GetLength(UINT *pPos);
		UINT	FindEndOfString(UINT uPos);
		UINT	FindDouble0(UINT uPos, UINT uMax);
		BOOL	StopChar(UINT uPos);
		void	HandleSpecial(PDWORD pData, UINT uCount, BOOL fIsRead);
		void	GetCachedData(PDWORD pData, PDWORD pCache, UINT uCount);
		void	StringToCache(PDWORD pData, PDWORD pCache, UINT uCount, UINT uMax);
		BOOL	IsStringItem (UINT uTable);

		// Helpers
		LAETUSCmdDef * SetLaetusCommand(UINT uTable);
		BOOL	NoReadTransmit (AREF Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, PDWORD dData, UINT uCount);
		BOOL	IsClearString(DWORD dData);
		void	PutDecimal(DWORD dValue, UINT uSize);
		void	PutHex(DWORD dValue, UINT uSize);
		BYTE	GetTxDelimiter(BOOL fStart);
		BYTE	GetRxDelimiter(BOOL fStart);
		UINT	GetEndPos(void);
		BYTE	GetSick(BOOL fStart);
		BYTE	GetWell(BOOL fTx, BOOL fStart);
		WORD	FromHex(BYTE bData);
		DWORD	GetDecRespValue(UINT *pPos, UINT uSize);
		DWORD	GetChrRespValue(UINT *pPos, UINT uSize, BYTE bLo);
		DWORD	GetHexRespValue(UINT *pPos, UINT uSize);
		DWORD	GetMixRespValue(UINT *pPos, UINT uSize);
		BYTE	PickNextChar (UINT *pPos);
		BYTE	GetNextRespChar(UINT *pPos);
		DWORD	GetNextRespValue(UINT *pPos, UINT uCount);
		void	SaveAckStatus(UINT uOffset);
		UINT	ToggleL(BOOL fIsLRS);
	};

/*
LAETUSCmdDef CODE_SEG CLaetusSerialDriver::LaetusCL[] = {

	{SOM,	"1",	1,	RACK  },
	{LRS,	"LR",   0,	RBYT  },
	{LRD,	"LR",	0,	RSTR  },
	{LES,	"LE",	0,	RBYT  },
	{LED,	"LE",	0,	RSPC  },
	{LXC,	"LX",	0,	RACK  },
	{SRG,	"21",	0,	NORSP },
	{MCF,	"3CV",	0,	RVAR  },
	{MC1,	"3CV",	0,	RSTR  },
	{MC2,	"3CV",	0,	RSTR  },
	{CCF,	"3CO",	0,	RVAR  },
	{CCS,	"3CO",	0,	RSTR  },
	{CCL,	"3CO",	0,	RSTR  },
	{DCF,	"3LK",	0,	RVAR  },
	{OPC,	"4CC",	4,	RLNG  },
	{PE1,	"2TT",	0,	RSTR  },
	{PE2,	"2TT",	0,	RSTR  },
	{PE3,	"2TT",	0,	RSTR  },
	{PE4,	"2TT",	0,	RSTR  },
	{SOC,	"3EX",	0,	RLNG  },
	{DST,	"15",	3,	RBYT  },
	{EEP,	"EEW",	0,	RACK  },

	{USC,	"ZZ",	0,	RSPC  },
	{USR,	"ZZ",	0,	RSPC  },
	{SCK,	"ZZ",	0,	RSPC  },
*/

// End of File
