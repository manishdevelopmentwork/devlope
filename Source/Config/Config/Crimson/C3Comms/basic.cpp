
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Basic Comms Driver
//

// Constructor

CBasicCommsDriver::CBasicCommsDriver(void)
{
	m_wID		= 0;

	m_uType		= 0;
	
	m_Manufacturer	= L"<manufacturer>";
	
	m_DriverName	= L"<name>";
	
	m_Version	= L"<version>";
	
	m_ShortName	= L"<shortname>";

	m_DevRoot	= L"PLC";

	m_fSingle	= FALSE;

	m_fNoMap	= FALSE;

	m_fCrimson3     = FALSE;
	}

// Destructor

CBasicCommsDriver::~CBasicCommsDriver(void)
{
	}

// IBase Methods

UINT CBasicCommsDriver::Release(void)
{
	delete this;

	return 0;
	}

// Driver Data

WORD CBasicCommsDriver::GetID(void)
{
	return m_wID;
	}

UINT CBasicCommsDriver::GetType(void)
{
	return m_uType;
	}

CString	CBasicCommsDriver::GetString(UINT ID)
{
	switch( ID ) {

		case stringManufacturer: return m_Manufacturer;
		case stringDriverName:	 return m_DriverName;
		case stringVersion:	 return m_Version;
		case stringShortName:	 return m_ShortName;
		case stringDevRoot:	 return m_DevRoot;
		}

	return L"";
	}

UINT CBasicCommsDriver::GetFlags(void)
{
	UINT uFlags = 0;

	if( m_fSingle   ) uFlags |= dflagOneDevice;

	if( m_fNoMap    ) uFlags |= dflagNoMapping;

	if( m_fCrimson3 ) uFlags |= dflagCrimson3;

	return uFlags;
	}

// Binding Control

UINT CBasicCommsDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CBasicCommsDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CBasicCommsDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CBasicCommsDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Tag Import

BOOL CBasicCommsDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	return FALSE;
	}

// Address Management

BOOL CBasicCommsDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return FALSE;
	}

BOOL CBasicCommsDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem * pDrvCfg)
{
	return ParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CBasicCommsDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}

BOOL CBasicCommsDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem * pDrvCfg)
{
	return ExpandAddress(Text, pConfig, Addr);
	}

BOOL CBasicCommsDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return FALSE;
	}

BOOL CBasicCommsDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem *pDrvCfg)
{
	return SelectAddress(hWnd, Addr, pConfig, fPart);
	}

BOOL CBasicCommsDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

BOOL CBasicCommsDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}

BOOL CBasicCommsDriver::IsAddrNamed(CItem *pConfig, CAddress const &Addr)
{
	return Addr.a.m_Table >= addrNamed;
	}

void CBasicCommsDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	
	}

void CBasicCommsDriver::NotifyInit(CItem * pConfig)
{
	}

// End of File
