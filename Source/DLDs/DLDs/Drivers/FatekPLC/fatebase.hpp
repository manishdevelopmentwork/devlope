
//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Driver
//

#ifndef	FATEKBASEINC
#define	FATEKBASEINC

// Fatek Spaces
enum {
	SP_XS	= 1,  // Input Bits - On/Off
	SP_YS	= 2,  // Output Bits - On/Off
	SP_MS	= 3,  // Internal Bits - On/Off
	SP_SS	= 4,  // Step Bits - On/Off
	SP_TS	= 5,  // Timer Bits - On/Off
	SP_CS	= 6,  // Counter Bits - On/Off
	SP_TR	= 7,  // Timer Registers
	SP_CR	= 8,  // Counter Registers
	SP_H	= 9,  // H Registers
	SP_D	= 10, // D Registers
	SP_ST	= 11, // Simple System Status
	SP_RS	= 12, // Run/Stop request
	SP_XE	= 13, // Input Bits - Enable/Disable
	SP_YE	= 14, // Output Bits - Enable/Disable
	SP_ME	= 15, // Internal Bits - Enable/Disable
	SP_SE	= 16, // Step Bits - Enable/Disable
	SP_TE	= 17, // Timer Bits - Enable/Disable
	SP_CE	= 18, // Counter Bits - Enable/Disable
	SP_ERR	= 20  // Latest Comm Error
	};

enum {
// * = not implemented
	RSSTAT	= 0x40,	// simple read status
	RUNSTOP	= 0x41, // run/stop plc
	WONEB	= 0x42, // write one bit
	RENBLB	= 0x43, // read bit enables
	RDISCB	= 0x44, // read bits
	WCONTB	= 0x45, // *write contiguous bits
	RCONTR	= 0x46, // read contiguous registers
	WCONTR	= 0x47, // write contiguous registers
	RMIXDR	= 0x48, // *read mixed set of registers
	WMIXDR	= 0x49, // *write mixed set of registers
	LOOPBK	= 0x4E, // loop back test
	RDSTAT	= 0x53  // *read detailed status
	};

#define	BB	addrBitAsBit
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

enum {
	BITDIS	= '1',
	BITENB	= '2',
	BITSET	= '3',
	BITRST	= '4'
	};

#define	ISBITSTAT	1
#define	ISBITENBL	2

#define	RTNZERO	409600000 // 5 decimal, 4 hex 0's

class CFatekPLCBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CFatekPLCBaseDriver(void);

		// Destructor
		~CFatekPLCBaseDriver(void);
		
		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	public:
		// Device Data
		struct CBaseCtx
		{
			BYTE	Drop[2];
			BOOL	m_fGlobal;
			DWORD	m_dLatestError;
			};

	protected:

		CBaseCtx * m_pBase;

		// Data Members
		BYTE	m_bTx[96];
		BYTE	m_bRx[96];
		UINT	m_uPtr;
		UINT	m_uWriteError;

		// Hex Lookup
		LPCTXT	m_pHex;

		CCODE	TestLoopBack(void);
		
		// Frame Building
		void	AddRead( AREF Addr, UINT uCount);
		void	AddWrite(AREF Addr, UINT uCount, PDWORD pData);
		BOOL	BuildFrame(AREF Addr, UINT uCount, BOOL fIsWrite, BYTE bWriteData);
		void	StartFrame(void);
		void	AddReqCode(UINT uTable, BOOL fIsBits, BOOL fIsWrite);
		void	AddLetters(UINT uTable, UINT uType);
		void	AddBitsLetter(UINT uTable);
		void	AddRegsLetter(UINT uTable, UINT uType);
		void	AddStart(UINT uTable, UINT uOffset);
		void	AddHex(BYTE bData);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, BOOL fHex, DWORD dFactor);
		void	EndFrame(void);

		// Frame Building Helpers
		BYTE	GetBitType(UINT uTable);
		BOOL	IsBits(UINT uTable);
		BOOL	IsCmds(UINT uTable);
		BOOL	IsHD(UINT uTable);
		BOOL	IsLong(UINT uType);
		
		// Transport Layer
		virtual	BOOL Transact(void);

		// Reply Handling
		BOOL	CheckReply(UINT uLen);
		void	GetResponse(PDWORD pData, UINT uCount, UINT uSize);
		
		// Port Access

		// Helpers
		BOOL	ReadNoTransmit (AREF Addr, PDWORD pData);
		BOOL	WriteNoTransmit(AREF Addr, PDWORD pData);
		BYTE	MakeChecksum(PBYTE pBuff, UINT uLen);
		DWORD	GetGeneric(UINT uPos, UINT uCount);
		BYTE	xtoi(BYTE b);
	};

// End of File

#endif
