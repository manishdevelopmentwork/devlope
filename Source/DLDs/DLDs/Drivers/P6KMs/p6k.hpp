#ifndef	INCLUDE_P6K_HPP
	
#define	INCLUDE_P6K_HPP  

//////////////////////////////////////////////////////////////////////////
//
// Parker Compumotor 6K Base Driver
//

struct FAR Cmd {

		UINT	m_ID;
		char	m_Text[8];
		UINT	m_Form;
		UINT	m_Response;
		UINT	m_Access;
		};

class CParker6KBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CParker6KBaseDriver(void);

		// Destructor
		~CParker6KBaseDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
	   
	
		// Device Context
		struct CBaseCtx
		{
			BYTE m_bDrop;
			UINT m_uInitTime;
			char m_Arg1[9];
			BYTE m_bDevice;
			};

	protected:

		CBaseCtx * m_pBase;

		BYTE m_bRxBuff[300];
		BYTE m_bTxBuff[300];
		UINT m_uPtr;
		BYTE m_bSum;
		LPCTXT m_pHex;
					
		static Cmd CODE_SEG m_Cmd[];
		Cmd FAR * m_pCmd;
		Cmd FAR * m_pItem;

		// Implementation
		BOOL PutReadCommand(UINT uOffset, UINT uType);
		BOOL GetRead(UINT uOffset, UINT uType, PDWORD pData, UINT uCount);

		BOOL PutWriteCommand(UINT uOffset, UINT uType, PDWORD pData, BOOL fGlobal);
		void PutVARCommand(UINT uOffset);
  
		void ProcessResponse(UINT uType);

		BOOL GetArg(PDWORD pData, UINT uCount);
		BOOL SetArg(PDWORD pData, UINT uCount);
		
	virtual	void Start(void);
	virtual	void End(void);

		void PutText(LPCTXT pCmd);
		void PutText(LPCTXT pCmd, UINT uLimit);
		void PutChar(char cChar);
		void PutGeneric(DWORD dwData, UINT wBase, DWORD dwFactor);
		
		void	PutReal(PDWORD pData);
		void	PutInteger(PDWORD pData);
		void	PutBinary(PDWORD pData);
		void    PutRBinary(PDWORD pData, UINT uType);
		void    PutRBinaryBitAsByte(PDWORD pData, UINT uBit, BOOL fGlobal);
		void	PutBinaryLong(PDWORD pData);
		void	PutPrefix(UINT uAddr, BOOL fGlobal);

	       	DWORD	GetReal(void);
		DWORD	GetInteger(void);
		void    GetString(PDWORD pData, UINT uCount);
		DWORD	GetBinary(UINT uBit);
		DWORD	GetBinary(void);
		void	FindCmd(UINT uIndex);
		UINT    FindIndex(UINT uTable, UINT uOffset);
	};

/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define FRAME_TIMEOUT	2000

#define FORM_NONE	0
#define FORM_AXIS	1
#define FORM_INT	2
#define FORM_BIN	4
#define FORM_REAL	8
#define FORM_BRK	16
#define FORM_X		32
#define FORM_E		64
#define FORM_IO		128
#define FORM_VAR	256
#define FORM_BLOC	512
#define	FORM_ARG	1024
#define FORM_STR	2048

#define RESP_DPEQ	0
#define RESP_DP		1
#define RESP_EQ		2
#define RESP_NODP	3
#define RESP_BITS	4
#define RESP_RBITS	5
#define RESP_NONE	6

#define ACC_R		1
#define ACC_W		2
#define ACC_RW		3

#define SEL_A		1
#define SEL_AA		2
#define SEL_AD		3
#define SEL_ADA		4
#define SEL_ADDR	5
//// EXPANSION
#define SEL_CMDDIR	7
#define SEL_COMEXC	8
#define SEL_COMEXL	9
#define SEL_COMEXR	10
#define SEL_COMEXS	11
#define SEL_D		12
#define SEL_DACLIM	13
//// EXPANSION
#define SEL_DRES	23
#define SEL_DRFEN	24
#define SEL_DRFLVL	25
#define SEL_DRIVE	26
//// EXPANSION
#define SEL_EFAIL	31
#define SEL_ENCCNT	32
#define	SEL_ENCPOL	33
#define SEL_ENCSEND	34
//// EXPANSION
#define SEL_ERES	39
//// EXPANSION
#define SEL_ESDB	44
#define SEL_ESK		45
#define SEL_ESTALL	46
#define SEL_FFILT	47
#define SEL_FGADV	48
#define SEL_FMAXA	49
#define SEL_FMAXV	50
#define SEL_FMCLEN	51
#define SEL_FMCNEW	52
#define SEL_FMCP	53
#define SEL_FOLEN	54
//// EXPANSION
#define SEL_FOLMD	56
#define SEL_FOLRD	57
#define SEL_FOLRN	58
#define SEL_FPPEN	59
#define SEL_FSHFC	60
#define SEL_FSHFD	61
#define SEL_FVMACC	62
#define SEL_FVMFRQ	63
#define SEL_GO		64
#define SEL_GOL		65
#define SEL_HALT	66
#define SEL_HOM		67
#define SEL_HOMA	68
#define SEL_HOMAA	69
#define SEL_HOMAD	70
#define SEL_HOMADA	71
#define SEL_HOMBAC	72
#define SEL_HOMDF	73
#define SEL_HOMDG	74
#define SEL_HOMV	75
#define SEL_HOMVF	76
#define SEL_HOMZ	77
//// EXPANSION
#define SEL_INDUSE	79
//// EXPANSION
#define SEL_JOG		84
#define SEL_JOGA	85
#define SEL_JOGAA	86
#define SEL_JOGAD	87
#define SEL_JOGADA	88
#define SEL_JOGVH	89
#define SEL_JOGVL	90
#define SEL_JOY		91
#define SEL_JOYA	92
#define SEL_JOYAA	93
#define SEL_JOYAD	94
#define SEL_JOYADA	95
//// EXPANSION
#define SEL_JOYVH	100
#define SEL_JOYVL	101
//// EXPANSION
#define SEL_LH		104
#define SEL_LHAD	105
#define SEL_LHADA	106
//// EXPANSION
#define SEL_LS		109
#define SEL_LSAD	110
#define SEL_LSADA	111
#define SEL_LSNEG	112
#define SEL_LSPOS	113
#define SEL_MA		114
#define SEL_MC		115
#define SEL_MEPOL	116
#define SEL_MESND	117
//// EXPANSION
#define SEL_OUT		127
//// EXPANSION
#define SEL_PA		130
#define SEL_PAA		131
#define SEL_PAD		132
#define SEL_PADA	133
//// EXPANSION
#define SEL_PV		153
//// EXPANSION
#define SEL_RE		155
//// EXPANSION
#define SEL_REGLOD	157
#define SEL_REGSS	158
#define SEL_S		159
#define SEL_SCALE	160
#define SEL_SCLA	161
#define SEL_SCLD	162
#define SEL_SCLMAS	163
#define SEL_SCLV	164
#define SEL_SFB		165
#define SEL_SGAF	166
#define SEL_SGI		167
#define SEL_SGILIM	168
#define SEL_SGP		169
#define SEL_SGV		170
#define SEL_SGVF	171
#define SEL_SINAMP	172
#define SEL_SINANG	173
#define SEL_SINGO	174
#define SEL_SMPER	175
#define SEL_SOFFS	176
#define SEL_STRGTD	177
#define SEL_STRGTE	178
#define SEL_STRGTT	179
#define SEL_STRGTV	180
//// EXPANSION
#define SEL_TAS		183
#define SEL_TASX	184
#define	SEL_TDAC	185
//// EXPANSION
#define SEL_TFB		187
#define SEL_TFS		188
#define SEL_TIN		189
#define SEL_TINO	190
#define SEL_TLIM	191
#define SEL_TOUT	192
#define SEL_TPC		193
#define SEL_TPCME	194
#define SEL_TPE		195
#define SEL_TPER	196
#define SEL_TPM		197
#define SEL_TPMAS	198
#define SEL_TPME	199
#define SEL_TPSHF	200
#define SEL_TPSLV	201
#define SEL_TRGLOT	202
#define SEL_TSS		203
#define SEL_TSTLT	204
//// EXPANSION
#define SEL_TTASK	206
#define SEL_TTIM	207
#define SEL_TUS		208
#define SEL_TVEL	209
#define SEL_TVELA	210
#define SEL_TVMAS	211
#define SEL_V		212
#define SEL_VAR		213
#define SEL_VARB	214
#define SEL_VARCLR	215
#define SEL_VARI	216
#define SEL_VARS	217
#define SEL_RUNARG	218
#define SEL_RUN		219

#define DEV_6K		0
#define DEV_GEM6K	1

// End of File

#endif

