
#include "Intern.hpp"

#include "Printf.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Initialization
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

// Constants

#define	lockFile	"/var/run/LedManager.pid"

#define pipeFile	"/tmp/crimson/leds/LedManager.pipe"

#define	pi		3.1415926

// Sequence Segment

struct CSeg
{
	CSeg(DWORD rgb, int in, int hold)
	{
		m_rgb  = rgb;
		m_in   = in;
		m_hold = hold;
	}

	DWORD	m_rgb;
	int	m_in;
	int	m_hold;
};

// Static Data

static	bool		m_stop  = false;

static	int		m_pd    = -1;

static	string		m_pipe  = pipeFile;

static	bool		m_debug = false;

static	bool		m_fore  = false;

static	int		m_mode  = 0;

static	DWORD		m_last  = 0xFFFFFF;

static	DWORD		m_save  = 0xFFFFFF;

static	vector<CSeg>	m_seq;

static	vector<CSeg>	m_next;

static	vector<int>	m_ease;

// Prototypes

global	int	main(int nArg, char const *pArg[]);
static	void	Error(char const *pText, ...);
static	void	AfxTrace(char const *p, ...);
static	void	CheckParent(void);
static	bool	GetProcStatusEntry(string &data, string const &proc, string const &name);
static	bool	MakeLockFile(void);
static	bool	TestLockFile(void);
static	void	KillLockFile(void);
static	void	OnTerm(int sig);
static	void	OnPipe(int sig);
static	void	MainLoop(void);
static	void	MakeEaseTable(void);
static	void	SetRgb(DWORD rgb);
static	double	Ease(double t, double d);
static	bool	OpenPipe(void);
static	size_t	Tokenize(vector<string> &list, string const &text, char const *seps);
static	bool	ParseSequence(string const &text);
static	void	SetPowerMode(int mode);

// Code

int main(int nArg, char const *pArg[])
{
	CheckParent();

	openlog("c3-led", 0, LOG_USER);

	signal(SIGTERM, OnTerm);

	signal(SIGPIPE, OnPipe);

	if( nArg == 1 ) {

		if( !m_debug && !m_fore ) {

			AfxTrace("starting up and forking\n");

			if( fork() ) {

				return 0;
			}
		}

		if( TestLockFile() ) {

			MakeLockFile();

			nice(-15);

			MainLoop();

			KillLockFile();

			return 0;
		}

		Error("already running");
	}

	Error("syntax error");
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	if( m_debug ) {

		printf("LedManager: ");

		vprintf(p, v);

		printf("\n");
	}
	else {
		vsyslog(LOG_ERR, p, v);
	}

	va_end(v);

	exit(1);
}

static void AfxTrace(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	if( m_debug ) {

		vprintf(p, v);
	}
	else {
		vsyslog(LOG_NOTICE, p, v);
	}

	va_end(v);
}

static void CheckParent(void)
{
	string ppid;

	if( GetProcStatusEntry(ppid, "self", "PPid") ) {

		string name;

		if( GetProcStatusEntry(name, ppid, "Name") ) {

			if( name == "gdbserver" || name == "gdb" ) {

				m_debug = true;
			}

			if( name == "InitC32" ) {

				m_fore = true;
			}
		}
	}
}

static bool GetProcStatusEntry(string &data, string const &proc, string const &name)
{
	ifstream stm(CPrintf("/proc/%s/status", proc.c_str()));

	if( stm.good() ) {

		while( !stm.eof() ) {

			string line;

			getline(stm, line);

			size_t n = line.find(':');

			if( n != string::npos ) {

				if( line.substr(0, n) == name ) {

					int d = line.find_first_not_of(" \t", n + 1);

					if( d != string::npos ) {

						int e = line.find_last_not_of(" \t\r\n");

						data = line.substr(d, e - d + 1);

						return true;
					}
				}
			}
		}
	}

	return false;
}

static bool MakeLockFile(void)
{
	if( !m_fore ) {

		ofstream stm(lockFile);

		if( stm.good() ) {

			stm << getpid();

			return true;
		}

		return false;
	}

	return true;
}

static bool TestLockFile(void)
{
	if( !m_fore ) {

		ifstream stm(lockFile);

		if( stm.good() ) {

			pid_t pid;

			stm >> pid;

			// Kill with signal of zero is a test for the
			// existance of a valid process of that pid...

			int r;

			if( (r = kill(pid, 0)) == 0 ) {

				return false;
			}

			stm.close();

			KillLockFile();
		}
	}

	return true;
}

static void KillLockFile(void)
{
	if( !m_fore ) {

		unlink(lockFile);
	}
}

static void OnTerm(int sig)
{
	m_stop = true;
}

static void OnPipe(int sig)
{
}

static void MainLoop(void)
{
	SetRgb(0);

	MakeEaseTable();

	OpenPipe();

	m_seq.emplace_back(0x202020, 1000, 1000);

	m_seq.emplace_back(0x808080, 400, 0);

	int   step  = 0;

	int   state = 0;

	int   timer = 0;

	DWORD rgb   = 0;

	while( !m_stop ) {

		CSeg const &seg = m_seq[step];

		switch( state ) {

			case 0:
			{
				if( seg.m_in ) {

					timer = 0;

					state = 1;
				}
				else {
					SetRgb(rgb = seg.m_rgb);

					timer = 0;

					state = 2;
				}
			}
			break;

			case 1:
			{
				if( timer > seg.m_in ) {

					rgb   = seg.m_rgb;

					timer = 0;

					state = 2;
				}
				else {
					if( rgb != seg.m_rgb ) {

						DWORD c1 = rgb;

						DWORD c2 = seg.m_rgb;

						DWORD c3 = 0;

						int   es = timer * 1024 / seg.m_in;

						int   ez = m_ease[es];

						for( int c = 0; c < 3; c++ ) {

							int v1 = (c1 & 255);

							int v2 = (c2 & 255);

							int v3 = v1 + (((v2 - v1) * ez) >> 8);

							c1 >>= 8;

							c2 >>= 8;

							c3  |= (v3 << (c*8));
						}

						SetRgb(c3);
					}

					usleep(5 * 1000);

					timer += 5;
				}
			}
			break;

			case 2:
			{
				if( timer >= seg.m_hold ) {

					step = (step + 1) % m_seq.size();

					if( m_next.size() ) {

						int match = 0;

						for( int s = 0; s < m_next.size(); s++ ) {

							if( m_next[s].m_rgb == m_seq[step].m_rgb ) {

								match = s;

								break;
							}
						}

						m_seq = m_next;

						step  = match;

						m_next.clear();
					}

					state = 0;
				}
				else {
					usleep(50 * 1000);

					timer += 50;
				}
			}
			break;
		}

		char data[4096];

		int n = read(m_pd, data, sizeof(data));

		if( n > 0 ) {

			if( data[n-1] == '\n' ) {

				string s(data, data + n - 1);

				size_t r = s.rfind('\n');

				if( r != s.npos ) {

					s.erase(0, r + 1);
				}

				ParseSequence(s);
			}
		}
	}

	close(m_pd);
}

static void MakeEaseTable(void)
{
	for( int n = 0; n <= 1024; n++ ) {

		m_ease.push_back(int(256 * Ease(n, 1024) + 0.5));
	}
}

static void SetRgb(DWORD rgb)
{
	m_save = rgb;

	switch( m_mode ) {

		case 1:
		{
			rgb = 0;
		}
		break;

		case 2:
		{
			rgb = ((rgb>>1) & 0x7F7F7F);
		}
		break;

		case 3:
		{
			rgb = ((rgb>>2) & 0x3F3F3F);
		}
		break;

		case 4:
		{
			rgb = ((rgb>>3) & 0x1F1F1F);
		}
		break;
	}

	static char const *path[] = {

		"/sys/class/leds/halo:blue/brightness",
		"/sys/class/leds/halo:green/brightness",
		"/sys/class/leds/halo:red/brightness"
	};

	DWORD was;

	was    = m_last;

	m_last = rgb;

	for( int c = 0; c < 3; c++ ) {

		if( (was & 0xFF) ^ (rgb & 0xFF) ) {

			int fd = open(path[c], O_WRONLY);

			if( fd >= 0 ) {

				char s[3];

				int  cb = (rgb & 0xFF);

				s[0] = '0' + ((cb / 100) % 10);
				s[1] = '0' + ((cb / 10) % 10);
				s[2] = '0' + ((cb / 1) % 10);

				write(fd, s, 3);

				close(fd);
			}
		}

		was >>= 8;

		rgb >>= 8;
	}
}

static double Ease(double t, double d)
{
	return -0.5 * (cos(pi*t / d) - 1);
}

static bool OpenPipe(void)
{
	string path = m_pipe.substr(0, m_pipe.rfind('/'));

	system(("mkdir -p " + path).c_str());

	mkfifo(m_pipe.c_str(), 0666);

	if( (m_pd = open(m_pipe.c_str(), O_RDWR | O_NONBLOCK)) >= 0 ) {

		return true;
	}

	return false;
}

static size_t Tokenize(vector<string> &list, string const &text, char const *seps)
{
	list.clear();

	size_t p = 0;

	for( ;;) {

		size_t a = text.find_first_not_of(seps, p);

		if( a < text.npos ) {

			size_t b = text.find_first_of(seps, a);

			if( b < text.npos ) {

				list.push_back(text.substr(a, b-a));

				p = b;

				continue;
			}

			list.push_back(text.substr(a));
		}

		break;
	}

	return list.size();
}

static bool ParseSequence(string const &text)
{
	m_next.clear();

	vector<string> segs;

	if( Tokenize(segs, text, ",") ) {

		for( auto const &seg : segs ) {

			vector<string> args;

			Tokenize(args, seg, "-");

			if( args[0] == "POWERSAVE" ) {

				SetPowerMode(atoi(args[1].c_str()));
			}
			else {
				DWORD rgb  = (args.size() > 0) ? strtoul(args[0].c_str(), NULL, 16) : 0;

				int   in   = (args.size() > 1) ? strtoul(args[1].c_str(), NULL, 10) : 0;

				int   hold = (args.size() > 2) ? strtoul(args[2].c_str(), NULL, 10) : 0;

				m_next.emplace_back(rgb, in, hold);
			}
		}

		return true;
	}

	return false;
}

void SetPowerMode(int mode)
{
	if( m_mode != mode ) {

		m_mode = mode;

		SetRgb(m_save);
	}
}

// End of File
