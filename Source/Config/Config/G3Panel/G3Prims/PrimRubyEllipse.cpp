
#include "intern.hpp"

#include "PrimRubyEllipse.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Ellipse Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyEllipse, CPrimRubyGeom);

// Constructor

CPrimRubyEllipse::CPrimRubyEllipse(void)
{
	}

// Meta Data

void CPrimRubyEllipse::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_SetName((IDS_ELLIPSE_2));
	}

// Path Management

void CPrimRubyEllipse::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyDraw::Ellipse( m_pathFill,
			    CRubyPoint(Rect, 1),
			    CRubyPoint(Rect, 4)
			    );

	CPrimRubyGeom::MakePaths();
	}

// End of File
