@echo off

rem *** Switch to the project directory.

cd /d "%~1"

rem *** Clean up any left-over folders.

if exist Bin\     rmdir /s /q Bin\
if exist Build\   rmdir /s /q Build\
if exist Include\ rmdir /s /q Include\
if exist Tools\   rmdir /s /q Tools\

rem *** Copy all the output files into Bin

xcopy /s "%~3..\..\*.lib" Bin\
xcopy /s "%~3..\..\*.pdb" Bin\
xcopy /s "%~3..\..\*.out" Bin\
xcopy /s "%~3..\..\*.exe" Bin\

rem *** Get rid of the test host files.

erase Bin\Win32\Debug\WinTest.*

erase Bin\Win32\Release\WinTest.*

rem *** Remove any client module files.

for /D %%g in (../source/cms/*) do (
  if exist Bin\Win32\Debug\%%g.* (
    erase Bin\Win32\Debug\%%g.*
    )
  if exist Bin\Win32\Release\%%g.* (
    erase Bin\Win32\Release\%%g.*
    )
  )

rem *** Remove the Crimson libraries.

for /D %%g in (Bin\*) do (
  erase %%g\Debug\AppCore.*
  erase %%g\Debug\C3*.*
  erase %%g\Debug\G3*.*
  erase %%g\Release\AppCore.*
  erase %%g\Release\C3*.*
  erase %%g\Release\G3*.*
  )

rem *** Rename the lower case pdbs files.

for %%g in (Bin\Win32\Debug\*.lib) do (
  if exist "%%~dpng.pdb" (
    ren "%%~dpng.pdb" %%~ng.pdb
    )
  )

for %%g in (Bin\Win32\Debug\*.exe) do (
  if exist "%%~dpng.pdb" (
    ren "%%~dpng.pdb" %%~ng.pdb
    )
  )

for %%g in (Bin\Win32\Release\*.lib) do (
  if exist "%%~dpng.pdb" (
    ren "%%~dpng.pdb" %%~ng.pdb
    )
  )

for %%g in (Bin\Win32\Release\*.exe) do (
  if exist "%%~dpng.pdb" (
    ren "%%~dpng.pdb" %%~ng.pdb
    )
  )

rem *** Copy the build files into Build.

xcopy /s ..\Source\Build Build\

rem *** Copy the required headers into Include.

xcopy /s ..\Source\RTL\LibC\Include Include\

xcopy    ..\Include Include\

rem *** Remove the Crimson headers.

erase Include\G3*.*

erase Include\C3*.*

rem *** Copy the tool executables into Tools.

xcopy ..\Tools\fixup.exe    Tools\

xcopy ..\Tools\exportvs.exe Tools\

rem *** Create and export the SDK archive.

attrib -r *.zip

erase     *.zip

7z a sdk.zip *.* -r -x!sdk*.* -x!??port.bat -x!Build.vcxproj*

copy sdk.zip "%~3..\..\sdk.zip"

rem *** Clean up the work folders.

rmdir /s /q Bin\
rmdir /s /q Build\
rmdir /s /q Include\
rmdir /s /q Tools\

rem *** Done.
