
//////////////////////////////////////////////////////////////////////////
//
// WDM G3 USB User Mode Driver
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3USBDRV_HPP

#define	INCLUDE_G3USBDRV_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <setupapi.h>

#include "g3usb.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "setupapi.lib")

//////////////////////////////////////////////////////////////////////////
//								
// Notification Types
//

typedef PVOID        HDEVNOTIFY;

typedef HDEVNOTIFY * PHDEVNOTIFY;

//////////////////////////////////////////////////////////////////////////
//								
// Notification Macros
//

#define RegisterDeviceNotify(x)		CUSBDriver::RegEvents(x)

#define UnregisterDeviceNotify(x)	CUSBDriver::UnregEvents(x)

//////////////////////////////////////////////////////////////////////////
//								
// Notification Structures
//

struct DEV_BROADCAST_HDR 
{
	DWORD	dbch_size; 
	DWORD	dbch_devicetype; 
	DWORD	dbch_reserved; 
	
	};

struct DEV_BROADCAST_DEVICEINTERFACE
{
	DWORD	dbcc_size;
	DWORD	dbcc_devicetype;
	DWORD	dbcc_reserved;
	GUID	dbcc_classguid;
	char	dbcc_name[1];
	
	};

//////////////////////////////////////////////////////////////////////////
//								
// Notification Flags
//

#define DEVICE_NOTIFY_WINDOW_HANDLE     0x00000000

//////////////////////////////////////////////////////////////////////////
//								
// Notification Device Types
//

#define DBT_DEVTYP_DEVICEINTERFACE      0x00000005

//////////////////////////////////////////////////////////////////////////
//								
// Notification Events
//

#define DBT_DEVICEARRIVAL               0x8000
#define DBT_DEVICEQUERYREMOVE           0x8001
#define DBT_DEVICEQUERYREMOVEFAILED     0x8002
#define DBT_DEVICEREMOVEPENDING         0x8003
#define DBT_DEVICEREMOVECOMPLETE        0x8004
#define DBT_DEVICETYPESPECIFIC          0x8005

//////////////////////////////////////////////////////////////////////////
//
// Device Object
//

class CDevice
{
	public:
		UINT    m_uVend;
		UINT	m_uProd;
		CString m_Serial;
		CString m_Path;
	};

//////////////////////////////////////////////////////////////////////////
//
// Search Wildcards
//

#define PRODUCT_ANY	0

#define	SERIAL_ANY	TEXT("*")

//////////////////////////////////////////////////////////////////////////
//
// Custom Types
//

typedef CArray <CDevice *> CDeviceList; 

//////////////////////////////////////////////////////////////////////////
//
// USB User Mode Driver
//

class CUSBDriver
{
	public:
		// Constructor
		CUSBDriver(void);

		// Destructor
		~CUSBDriver(void);

		// Attributes
		BOOL    IsValid(void) const;
		UINT    GetVendor(void) const;
		UINT    GetProduct(void) const;
		CString GetSN(void) const;

		// Operations
		BOOL Open(void);
		BOOL Open(UINT uProduct);
		BOOL Open(UINT uProduct, UINT uMask);
		BOOL Open(CString Serial);
		BOOL Open(UINT uProduct, UINT uMask, CString Serial);
		BOOL Send(PBYTE pData, UINT &uSize);
		BOOL Recv(PBYTE pData, UINT &uSize, UINT uTimeout = INFINITE);
		void CancelIO(void);
		void Close(void);
		void Refresh(void);

		// Device Enumerations
		CDeviceList const & GetDevList(void);
		UINT                GetDevCount(void);

		// Device Notifications
		static HDEVNOTIFY RegEvents(HWND hWnd);
		static void       UnregEvents(HDEVNOTIFY hNotify);

	protected:
		// Data Members
		HANDLE	     m_hHandle;
		OVERLAPPED   m_TxDataOS;
		OVERLAPPED   m_RxDataOS;
		BOOL         m_fRxPend;
		CDeviceList  m_DevList;
		CDevice    * m_pDevice;

		// Init
		void Init(void);
		
		// Enumeration
		BOOL FindDevList(void);
		void ClearDevList(void);
		void AppendDevList(PCTXT pDevPath);
		
		// Sync Objects
		void OpenOverlapped(void);
		void CloseOverlapped(void);
	};

// End of File

#endif
