
#include "Intern.hpp"

#include "UsbSerialCdcExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Serial CDC Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbSerialCdcExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbSerialCdcExpansion(pDriver);

	return p;
	}

// Constructor

CUsbSerialCdcExpansion::CUsbSerialCdcExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT CUsbSerialCdcExpansion::GetName(void)
{
	return "UART";
	}

UINT CUsbSerialCdcExpansion::GetPower(void)
{
	return 100; // TODO 
	}

IDevice * CUsbSerialCdcExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbSerialCdc(pDriver);
	}

// IExpansionSerial

UINT CUsbSerialCdcExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD CUsbSerialCdcExpansion::GetPortMask(void)
{
	return Bit(physicalRS232);
	}

UINT CUsbSerialCdcExpansion::GetPortType(UINT iPort)
{
	return rackSerial;
	}

// End of File
