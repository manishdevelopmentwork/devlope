
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosCrxConfigApplicator_HPP

#define INCLUDE_RlosCrxConfigApplicator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RlosBaseConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS CR Series Configuration Applicator
//

class CRlosCrxConfigApplicator : public CRlosBaseConfigApplicator
{
public:
	// Constructors
	CRlosCrxConfigApplicator(CString Model, IPxeModel *pModel);

	// IConfigApplicator
	CString METHOD GetDisplayName(void);
	CString METHOD GetProcessor(void);
	CString METHOD GetModelList(void);
	CString METHOD GetModelInfo(CString Model);
	CString METHOD GetEmulatorModel(CSize DispSize);
	BOOL    METHOD GetPorts(CStringArray &List, CJsonData *pHard);

protected:
	// Implementation
	CString MakeFull(CString const &Prefix, CString const &Base);
};

// End of File

#endif
