
#include "Intern.hpp"

#include "G3LinkFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Link Frame
//

// Constructor

CG3LinkFrame::CG3LinkFrame(void)
{
	m_pData = NULL;

	m_pBulk = NULL;
}

// Destructor

CG3LinkFrame::~CG3LinkFrame(void)
{
	FreeData();

	FreeBulk();
}

// Frame Creation

void CG3LinkFrame::StartFrame(BYTE bService, BYTE bOpcode)
{
	FreeData();

	FreeBulk();

	m_bService   = bService;

	m_bOpcode    = bOpcode;

	m_bFlags     = 0;

	m_pData      = NULL;

	m_pBulk	     = NULL;

	m_uDataAlloc = 0;

	m_uBulkAlloc = 0;

	m_uDataCount = 0;

	m_uBulkCount = 0;

	m_fSlow      = FALSE;
}

// Operations

void CG3LinkFrame::MarkAsync(void)
{
	m_bFlags |= flagsAsync;
}

void CG3LinkFrame::MarkVerySlow(void)
{
	m_fSlow = TRUE;
}

void CG3LinkFrame::SetFlags(BYTE bFlags)
{
	m_bFlags = bFlags;
}

// Simple Data

void CG3LinkFrame::AddByte(BYTE bData)
{
	ExpandData(sizeof(BYTE));

	m_pData[m_uDataCount++] = bData;
}

void CG3LinkFrame::AddWord(WORD wData)
{
	ExpandData(sizeof(WORD));

	m_pData[m_uDataCount++] = HIBYTE(wData);
	m_pData[m_uDataCount++] = LOBYTE(wData);
}

void CG3LinkFrame::AddLong(LONG lData)
{
	ExpandData(sizeof(LONG));

	m_pData[m_uDataCount++] = HIBYTE(HIWORD(lData));
	m_pData[m_uDataCount++] = LOBYTE(HIWORD(lData));
	m_pData[m_uDataCount++] = HIBYTE(LOWORD(lData));
	m_pData[m_uDataCount++] = LOBYTE(LOWORD(lData));
}

void CG3LinkFrame::AddGuid(GUID Guid)
{
	AddData(PCBYTE(&Guid), sizeof(Guid));
}

// Larger Data

void CG3LinkFrame::AddData(PCBYTE pData, UINT uCount)
{
	if( uCount ) {

		ExpandData(uCount);

		memcpy(m_pData + m_uDataCount, pData, uCount);

		m_uDataCount += uCount;
	}
}

void CG3LinkFrame::AddBulk(PCBYTE pData, UINT uCount)
{
	if( uCount ) {

		ExpandBulk(uCount);

		memcpy(m_pBulk + m_uBulkCount, pData, uCount);

		m_uBulkCount += uCount;
	}
}

// Data Reads

PCBYTE CG3LinkFrame::ReadData(UINT &uPtr, UINT uCount) const
{
	PCBYTE pData = m_pData + uPtr;

	uPtr += uCount;

	return pData;
}

GUID & CG3LinkFrame::ReadGuid(UINT &uPtr) const
{
	return (GUID &) (ReadData(uPtr, 16)[0]);
}

BYTE CG3LinkFrame::ReadByte(UINT &uPtr) const
{
	return m_pData[uPtr++];
}

WORD CG3LinkFrame::ReadWord(UINT &uPtr) const
{
	BYTE hi = ReadByte(uPtr);

	BYTE lo = ReadByte(uPtr);

	return MAKEWORD(lo, hi);
}

DWORD CG3LinkFrame::ReadLong(UINT &uPtr) const
{
	WORD hi = ReadWord(uPtr);

	WORD lo = ReadWord(uPtr);

	return MAKELONG(lo, hi);
}

// Attributes

BYTE CG3LinkFrame::GetService(void) const
{
	return m_bService;
}

BYTE CG3LinkFrame::GetOpcode(void) const
{
	return m_bOpcode;
}

BYTE CG3LinkFrame::GetFlags(void) const
{
	return m_bFlags;
}

BOOL CG3LinkFrame::IsAsync(void) const
{
	return (m_bFlags & flagsAsync) ? TRUE : FALSE;
}

BOOL CG3LinkFrame::IsVerySlow(void) const
{
	return m_fSlow;
}

UINT CG3LinkFrame::GetDataSize(void) const
{
	return m_uDataCount;
}

UINT CG3LinkFrame::GetBulkSize(void) const
{
	return m_uBulkCount;
}

PBYTE CG3LinkFrame::GetData(void) const
{
	return m_pData;
}

PBYTE CG3LinkFrame::GetBulk(void) const
{
	return m_pBulk;
}

BYTE CG3LinkFrame::GetAt(UINT uPos)
{
	if( m_uDataCount >= uPos ) {

		return m_pData[uPos];
	}
	else {
		return 0;
	}
}

// Implementation

void CG3LinkFrame::FreeData(void)
{
	if( m_pData ) {

		delete m_pData;

		m_pData = NULL;
	}
}

void CG3LinkFrame::FreeBulk(void)
{
	if( m_pBulk ) {

		delete m_pBulk;

		m_pBulk = NULL;
	}
}

void CG3LinkFrame::ExpandData(UINT uExtra)
{
	if( m_uDataCount + uExtra > m_uDataAlloc ) {

		UINT uAlloc = m_uDataAlloc + max(uExtra, 256);

		PBYTE pData = New BYTE[uAlloc];

		if( m_pData ) {

			memcpy(pData, m_pData, m_uDataAlloc);

			delete m_pData;
		}

		m_uDataAlloc = uAlloc;

		m_pData      = pData;
	}
}

void CG3LinkFrame::ExpandBulk(UINT uExtra)
{
	if( m_uBulkCount + uExtra > m_uBulkAlloc ) {

		UINT uAlloc = m_uBulkAlloc + max(uExtra, 256);

		PBYTE pBulk = New BYTE[uAlloc];

		if( m_pBulk ) {

			memcpy(pBulk, m_pBulk, m_uBulkAlloc);

			delete m_pBulk;
		}

		m_uBulkAlloc = uAlloc;

		m_pBulk      = pBulk;
	}
}

// End of File
