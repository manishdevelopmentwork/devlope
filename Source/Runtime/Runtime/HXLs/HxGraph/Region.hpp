
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Region_HPP
	
#define	INCLUDE_Region_HPP

//////////////////////////////////////////////////////////////////////////
//
// Region Implementation
//

class CRegion : public IRegion
{
	public:
		// Constructor
		CRegion(void);

		// Destructor
		~CRegion(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Attributes
		BOOL IsEmpty(void);
		BOOL GetRect(R2 &Rect);

		// Operations
		void Clear     (void);
		void AddRect   (R2 const &Rect);
		void AddEllipse(R2 const &Rect);

		// Hit Testing
		BOOL HitTest(P2 const &Point);
		BOOL HitTest(R2 const &Rect);

		// Filling
		void Fill(IGdi *pGdi);

		// Diagnostics
		void Show(IGdi *pGdi);
		void Dump(void);

	protected:
		// Data Members
		ULONG m_uRefs;
		R2    m_Rect[16];
		UINT  m_uCount;

		// Implementation
		UINT GetDistance(R2 const &R1, R2 const &R2);
	};

// End of File

#endif
