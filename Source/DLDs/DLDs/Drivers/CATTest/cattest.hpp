
//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CATTEST_HPP
	
#define	INCLUDE_CATTEST_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCatLinkDriver;
class CCatLinkHandler;

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	IsSelf(x)	((x) == m_bSelf)

#define	IsSafe(x)	((x) == m_bSelf || (x) == 0xAA)

//////////////////////////////////////////////////////////////////////////
//
// Timeous
//

#define	timeDelay	5000	// Startup delay to listen for engine
#define	timeRetry	100	// Retry period if send queue is full
#define	timePeriodic	1000	// Frequency at which to send RPM data

//////////////////////////////////////////////////////////////////////////
//
// States
//

enum
{
	stateIdle     = 0,
	stateDelay    = 1,
	stateListen   = 2,
	stateRetry    = 3,
	statePeriodic = 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver
//

class CCatLinkDriver : public CMasterDriver
{
	public:
		// Constructor
		CCatLinkDriver(void);

		// Destructor
		~CCatLinkDriver(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping   (void);
		DEFMETH(CCODE) Read   (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write  (AREF Addr, PDWORD pData, UINT uCount);

		// Handler Calls
		void SeePoll(BYTE bSrc, UINT uName);
		void SeeFault(WORD wCode);
		UINT GetSend(PBYTE pData);
		BOOL GetNext(void);

	protected:
		// Data Item
		struct CDataItem
		{
			WORD	m_wState;
			BYTE	m_bPoll;
			BYTE	m_bFrom;
			UINT	m_uTime;
			DWORD	m_Data;
			};

		// Data Members
		BYTE		  m_bSelf;
		BYTE		  m_bDummy;
		CCatLinkHandler * m_pHandler;
		CDataItem	  m_Data [0x4080];
		CDataItem *	  m_pSend[128];
		UINT		  m_uScan;
		UINT		  m_uHead;
		UINT		  m_uTail;

		// State Machine
		void UpdateItems(void);
		void UpdateItem(UINT uName, CDataItem *pItem);
		void SetTime(CDataItem *pItem, UINT uTime);
		BOOL TimeOut(CDataItem *pItem);
		BOOL QueueSend(CDataItem *pItem);

		// Data Item Access
		CDataItem * GetItem(UINT uName);

		// Item Name Helpers
		BOOL IsValidName(UINT uName);
		UINT GetName    (UINT uIndex);

		// Framing Helpers
		UINT GetNameLen(UINT uName);
		UINT GetDataLen(UINT uName);
		void SetName(PBYTE pName, UINT uName, UINT uLen);
		void SetData(PBYTE pData, UINT uData, UINT uLen);
	};

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Handler
//

class CCatLinkHandler : public IPortHandler
{	
	public:
		// Constructor
		CCatLinkHandler(IHelper *pHelper, CCatLinkDriver *pDriver, BYTE bSelf);

		// Destructor
		~CCatLinkHandler(void);

		// Operations
		void Start(void);
		void Stop(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortHandler
		void METHOD Bind(IPortObject *pPort);
		void METHOD OnRxData(BYTE bData);
		void METHOD OnRxDone(void);
		BOOL METHOD OnTxData(BYTE &bData);
		void METHOD OnTxDone(void);
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);
		void METHOD OnTimer(void);

	protected:
		// Data Members
		ULONG            m_uRefs;
		IHelper        * m_pHelper;
		CCatLinkDriver * m_pDriver;
		BYTE		 m_bSelf;
		IPortObject    * m_pPort;
		BYTE		 m_bRxData[128];
		BYTE		 m_bTxData[128];
		UINT		 m_uRxPtr;
		UINT		 m_uTxPtr;
		UINT		 m_uTxSize;
		BOOL		 m_fRun;
		BOOL		 m_fSync;
		BOOL		 m_fSelf;

		// Implementation
		void Process(BYTE bSrc, BYTE bDest, PBYTE pData, UINT uCount);
		UINT GetName(PBYTE pName, UINT uLen);
		void ClearContention(void);
		BOOL CheckContention(void);
		void StartEdgeTimer(BOOL fSlow);
	};

// End of File

#endif
