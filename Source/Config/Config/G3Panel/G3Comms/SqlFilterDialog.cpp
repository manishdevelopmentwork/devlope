
#include "Intern.hpp"

#include "SqlFilterDialog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlFilter.hpp"
#include "SqlFilterList.hpp"
#include "SqlFilterItemDialog.hpp"
#include "SqlQuery.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CSqlFilterDialog, CStdDialog);

// Message Map

AfxMessageMap(CSqlFilterDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay        )
	AfxDispatchCommand(3001, OnCreateFilter)
	AfxDispatchCommand(3002, OnEditFilter  )
	AfxDispatchCommand(3003, OnDelFilter   )

	AfxDispatchNotify(2001, NM_DBLCLK,       OnDblClk     )
	AfxDispatchNotify(2001, LVN_ITEMCHANGED, OnItemChanged)

	AfxMessageEnd(CSqlFilterDialog)
	};

// Constructor

CSqlFilterDialog::CSqlFilterDialog(CSqlQuery *pQuery)
{
	m_pQuery = pQuery;

	m_pList  = pQuery->m_pFilters;

	SetName(L"SqlFilterDlg");
	}

// Message Handlers

BOOL CSqlFilterDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListView &List = (CListView &) GetDlgItem(2001);

	DWORD dwStyle   = LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES;

	List.SetExtendedListViewStyle(dwStyle, dwStyle);

	CRect Rect = List.GetWindowRect();

	int cx = Rect.cx() / 9;

	CListViewColumn Column;

	Column.SetText(CString(IDS("Binding")));

	Column.SetWidth(cx);

	List.InsertColumn(0, Column);

	Column.SetText(CString(IDS_COLUMN));

	Column.SetWidth(3 * cx);

	List.InsertColumn(1, Column);

	Column.SetText(CString(IDS("Operator")));

	Column.SetWidth(2 * cx);

	List.InsertColumn(2, Column);

	Column.SetText(CString(IDS_FILTER));

	Column.SetWidth(3 * cx);

	List.InsertColumn(3, Column);

	LoadFilters();

	DoEnables();

	return TRUE;
	}

BOOL CSqlFilterDialog::OnOkay(UINT uId)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CSqlFilterDialog::OnCreateFilter(UINT uID)
{
	CSqlFilter * pFilter = m_pQuery->MakeFilter();
	
	CSqlFilterItemDialog Dlg(pFilter, L"Create Condition");

	if( Dlg.Execute() == IDOK ) {

		LoadFilters();

		return TRUE;
		}

	m_pQuery->DeleteFilter(pFilter);

	return FALSE;
	}

BOOL CSqlFilterDialog::OnDelFilter(UINT uID)
{
	DeleteFilter();

	LoadFilters();

	return TRUE;
	}

BOOL CSqlFilterDialog::OnEditFilter(UINT uID)
{	
	EditFilter();

	return TRUE;
	}

void CSqlFilterDialog::OnItemChanged(UINT uID, NMLISTVIEW &Info)
{
	DoEnables();
	}

void CSqlFilterDialog::OnDblClk(UINT uId, NMHDR &HDR)
{
	EditFilter();
	}

// Implementation

void CSqlFilterDialog::LoadFilters(void)
{
	CListView &List = (CListView &) GetDlgItem(2001);

	List.SetRedraw(FALSE);

	List.DeleteAllItems();

	UINT n = 0;

	for( INDEX i = m_pList->GetHead(); !m_pList->Failed(i); m_pList->GetNext(i) ) {

		CSqlFilter *pFilter = m_pList->GetItem(i);

		if( pFilter ) {

			CSqlColumn *pColumn = m_pQuery->GetColumn(pFilter->m_ColName);

			CString Name = pColumn ? pColumn->m_SqlName : IDS("<ERR>");

			CString Bind = (n == 0) ? L"" : pFilter->GetBinding();

			List.InsertItem(CListViewItem(n, 0, Bind, 0, LPARAM(pFilter)));

			List.SetItem   (CListViewItem(n, 1, Name, 0));

			List.SetItem   (CListViewItem(n, 2, pFilter->GetOperator(), 0));

			if( pFilter->NeedsOperand() )

				List.SetItem(CListViewItem(n, 3, pFilter->m_pValue->GetSource(TRUE), 0));
			else
				List.SetItem(CListViewItem(n, 3, L"", 0));
			}

		n++;
		}

	List.SetRedraw(TRUE);
	}

BOOL CSqlFilterDialog::EditFilter(void)
{
	CListView &List = (CListView &) GetDlgItem(2001);

	UINT uPos = List.GetSelection();

	CSqlFilter *pFilter = m_pQuery->m_pFilters->GetItem(uPos);

	if( pFilter ) {

		CSqlFilterItemDialog Dlg(pFilter, L"Edit Condition");

		pFilter->m_Column = m_pQuery->GetColumnPos(pFilter->m_ColName);

		HGLOBAL hPrev = pFilter->TakeSnapshot();

		CError Error;

		if( Dlg.Execute() == IDOK ) {

			LoadFilters();

			GlobalFree(hPrev);

			return TRUE;
			}

		Error.Show(ThisObject);

		pFilter->LoadSnapshot(hPrev);

		GlobalFree(hPrev);
		}

	LoadFilters();

	return FALSE;
	}

BOOL CSqlFilterDialog::DeleteFilter(void)
{
	CListView &List = (CListView &) GetDlgItem(2001);

	UINT uPos = List.GetSelection();

	CListViewItem Item = List.GetItem(uPos, 0);

	CSqlFilter *pFilter = (CSqlFilter *) Item.GetParam();

	if( pFilter ) {

		if( m_pQuery->DeleteFilter(pFilter) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CSqlFilterDialog::DoEnables(void)
{
	CListView &List = (CListView &) GetDlgItem(2001);
	
	GetDlgItem(3002).EnableWindow(List.GetSelectedCount() > 0);	

	GetDlgItem(3003).EnableWindow(List.GetSelectedCount() > 0);	
	}

// End of File
