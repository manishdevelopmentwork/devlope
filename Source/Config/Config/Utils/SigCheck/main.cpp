
//////////////////////////////////////////////////////////////////////////
//
// C2 Log Signature Check Utility
//
// Copyright (c) 1996-2006 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

// Static Data

static	int		m_nArg	   = 0;

static	PTXT *		m_pArg	   = NULL;

static	BOOL		m_fBanner  = FALSE;

static	PTXT		m_pName	   = NULL;

static	FILE *		m_pFile    = NULL;

static	UINT		m_uLine	   = 0;

static	PCTXT		m_pError   = "invalid file";

static	HCRYPTKEY	m_hKey     = NULL;

static	UINT		m_uBits    = 1024;

static	CMSCrypto	m_Crypto;

// Prototypes

global	void	main(int inArg, char *pArg[]);
static	void	ParseLine(void);
static	void	AppBanner(void);
static	void	ShowHelp(void);
static	void	Error(PCTXT pText, ...);
static	BOOL	Verify(void);
static	void	ReadSig(PTXT pData, KEY_SIG &Sig);
static	BYTE	FromHex(char c);

// Code

global	void	main(int nArg, char *pArg[])
{
	m_nArg = nArg;

	m_pArg = pArg;

	if( nArg == 2 )  {

		ParseLine();

		if( !Verify() ) {

			printf("%s(%u) : error : %s\n", m_pName, m_uLine, m_pError);
		
			exit(1);
			}

		printf("%s : okay : strength is %u\n", m_pName, m_uBits);

		exit(0);
		}
		
	ShowHelp();
	}	
	
static	void	AppBanner(void)
{
	if( !m_fBanner ) {

		printf("File Signature Verification  Version 1.20\n");
		
		printf("Copyright (c) 1996-2006 Red Lion Controls\n\n");

		m_fBanner = TRUE;
		}
	}

static	void	ParseLine(void)
{
	if( --m_nArg ) {

		m_pName = *++m_pArg;
		}

	if( !m_pName ) {
		
		Error("filename must be specified");
		}

	if( !(m_pFile = fopen(m_pName, "rb")) ) {

		Error("cannot open file");
		}
	}	

static	void	Error(PCTXT pText, ...)
{
	AppBanner();

	printf("sigcheck : ");
	
	vprintf(pText, PTXT(&pText+1));
	
	printf("\n");

	exit(2);
	}

static	void	ShowHelp(void)
{
	AppBanner();
	
	printf("usage: sigcheck <file>\n");
	
	printf("\n");
	
	exit(3);
	}

static	BOOL	Verify(void)
{
	if( m_Crypto.Open() ) {

		KEY_SIG Sig = { 0, { 0 } };

		char sLine[1024];

		BYTE bData[32768];

		UINT uPtr      = 0;

		BOOL fOkay     = TRUE;

		UINT uLastMono = 0;

		UINT uLastMAC  = 0;

		while( !feof(m_pFile) ) {

			UINT uMax   = sizeof(sLine) - 1;

			sLine[0]    = 0;

			sLine[uMax] = 0;

			fgets(sLine, uMax, m_pFile);

			UINT uLen = strlen(sLine);

			if( uLen == uMax - 1 ) {

				m_uLine  = m_uLine + 1;

				m_pError = "line too long";

				return FALSE;
				}

			if( uLen ) {

				if( ++m_uLine > 1 ) {

					PTXT pComma = strrchr(sLine, ',');

					if( pComma ) {

						BYTE bType = BYTE(strtoul(pComma +  1, NULL, 16));

						UINT uMono = UINT(strtoul(pComma +  4, NULL, 16));

						UINT uMAC  = UINT(strtoul(pComma + 13, NULL, 16));

						if( m_uLine > 2 && uMono <= uLastMono ) {

							m_pError = "non-monotonic sequence numbers";

							return FALSE;
							}

						if( m_uLine > 2 && uMAC  != uLastMAC  ) {

							m_pError = "non-matching MAC IDs";

							return FALSE;
							}

						uLastMono = uMono;

						uLastMAC  = uMAC;

						if( bType == 0x00 ) {

							if( uPtr + uLen > sizeof(bData) ) {

								m_pError = "too long without signature";

								return FALSE;
								}

							memcpy(bData + uPtr, sLine, uLen);

							uPtr += uLen;

							fOkay = FALSE;

							continue;
							}

						if( bType == 0x01 ) {

							UINT uCopy = pComma - sLine;

							PTXT pSig  = pComma + 20;

							memcpy(bData + uPtr, sLine, uCopy);

							uPtr += uCopy;

							ReadSig(pSig, Sig);

							if( !m_hKey ) {

								m_pError = "unknown key length";

								return FALSE;
								}

							if( m_Crypto.SignTest(bData, uPtr, m_hKey, Sig) ) {

								uPtr  = uLen - uCopy - 1;

								fOkay = TRUE;

								memmove(bData, pComma + 1, uPtr);

								continue;
								}

							m_pError = "bad signature";

							return FALSE;
							}

						m_pError = "unknown sig block type";

						return FALSE;
						}

					m_pError = "missing sig block";

					return FALSE;
					}
				else {
					memcpy(bData + uPtr, sLine, uLen);

					uPtr += uLen;
					}
				}
			}

		if( !fOkay ) {

			m_pError = "unsigned residue";

			return FALSE;
			}

		return TRUE;
		}

	Error("can't open cryptographic provider");

	return FALSE;
	}

static	void	ReadSig(PTXT pData, KEY_SIG &Sig)
{
	UINT n = 0;

	while( isxdigit(pData[0]) && isxdigit(pData[1]) ) {

		BYTE n1 = FromHex(pData[0]);
		
		BYTE n2 = FromHex(pData[1]);

		Sig.bData[n++] = ((n1<<4)|n2);

		pData += 2;
		}

	Sig.uLen = n;

	m_hKey   = m_Crypto.GetKeyByLength(n * 8);

	m_uBits  = min(m_uBits, n * 8);
	}

static	BYTE	FromHex(char c)
{
	if( c >= '0' && c <= '9' ) return c - '0' + 0x00;
	
	if( c >= 'A' && c <= 'F' ) return c - 'A' + 0x0A;
	
	if( c >= 'a' && c <= 'f' ) return c - 'a' + 0x0A;

	return 0;
	}

// End of File
