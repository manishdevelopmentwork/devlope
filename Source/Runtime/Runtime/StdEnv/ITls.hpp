
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ITls_HPP

#define INCLUDE_ITls_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 9 -- TLS/SSL
//

interface ITlsLibrary;
interface ITlsClientContext;
interface ITlsServerContext;
interface ICertManager;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

interface ISocket;

//////////////////////////////////////////////////////////////////////////
//
// TLS Certificate Checks
//

enum
{
	tlsCheckNone  = 0, // Check Nothing
	tlsCheckChain = 1, // Check Only Chain Validity
	tlsCheckName  = 2, // Check Name Too
	tlsCheckDate  = 3, // Check Date Too
	};

//////////////////////////////////////////////////////////////////////////
//
// TLS Library Interface
//

interface ITlsLibrary : public IUnknown
{
	// LINK

	AfxDeclareIID(9, 1);

	virtual BOOL METHOD LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)     = 0;
	virtual BOOL METHOD CreateClientContext(ITlsClientContext * &pCtx) = 0;
	virtual BOOL METHOD CreateServerContext(ITlsServerContext * &pCtx) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// TLS Client Context
//

interface ITlsClientContext : public IUnknown
{
	// LINK

	AfxDeclareIID(9, 2);

	virtual BOOL	  LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)                                      = 0;
	virtual BOOL      LoadClientCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass) = 0;
	virtual ISocket * CreateSocket(ISocket *pSock, PCTXT pName, UINT uCheck)                          = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// TLS Server Context
//

interface ITlsServerContext : public IUnknown
{
	// LINK

	AfxDeclareIID(9, 3);

	virtual BOOL	  LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)                                      = 0;
	virtual BOOL      LoadServerCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass) = 0;
	virtual ISocket * CreateSocket(ISocket *pSock)				                          = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Certificate Manager
//

interface ICertManager : public IUnknown
{
	// LINK

	AfxDeclareIID(9, 4);

	virtual BOOL METHOD GetTrustedRoots(CByteArray &Certs)                                             = 0;
	virtual BOOL METHOD GetIdentityCert(UINT uSlot, CByteArray &Cert, CByteArray &Priv, CString &Pass) = 0;
	virtual BOOL METHOD GetTrustedCert(UINT uSlot, CByteArray &Cert)                                   = 0;
};

// End of File

#endif
