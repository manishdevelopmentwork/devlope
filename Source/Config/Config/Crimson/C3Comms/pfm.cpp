
#include "intern.hpp"

#include "pfm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Plant Floor Marquee Driver
//
//

// Instantiator

ICommsDriver *	Create_PFMDriver(void)
{
	return New CPFMDriver;
	}

// Constructor

CPFMDriver::CPFMDriver(void)
{
	m_wID		= 0x4037;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "Red Lion";
	
	m_DriverName	= "Plant Floor Marquee";
	
	m_Version	= "1.00";
	
	m_ShortName	= "PFM";

	m_fSingle	= TRUE;
	}

// Binding Control

UINT CPFMDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CPFMDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// End of File
