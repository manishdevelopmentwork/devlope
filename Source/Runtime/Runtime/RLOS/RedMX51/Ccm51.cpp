
#include "Intern.hpp"

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Dpll51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Clock Controller
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CCcm51::CCcm51(CDpll51 **ppDpll)
{
	m_pBase         = PVDWORD(ADDR_CCM);

	m_ppDpll        = ppDpll;

	Reg(LowPwrCtrl) = 0x00000018;

	Reg(ClkSwitch)  = 0x00000000;

	Reg(CtrlDiv)    = Bit(17) | Bit(16);
	}

// Attributes

UINT CCcm51::GetSrc(UINT uClk) const
{
	switch( uClk ) {

		case clkPll1:
		case clkPll2:
		case clkPll3:
		case clkLpArm:
			return uClk;

		case clkOsc:
			return 0;
		
		case clkArmRoot:
			return GetSrc(GetArmSrc());
			
		case clkAxia:
			return GetSrc(GetAxxSrc());
			
		case clkAxib:
			return GetSrc(GetAxxSrc());

		case clkEmi:
			return GetSrc(GetEmiSrc());

		case clkAhb:
			return GetSrc(GetAxxSrc());

		case clkIpg:
			return GetSrc(clkAhb);
	
		case clkPer:
			return GetSrc(GetPerSrc());

		case clkGpu2:
			return GetSrc(GetGpu2Src());

		case clkArmAxi:
			return GetSrc(GetAxiSrc());

		case clkIpuHsp:
			return GetSrc(GetIpuSrc());

		case clkGpu:
			return GetSrc(GetGpuSrc());

		case clkVpu:
			return GetSrc(GetVpuSrc());

		case clkDdr:
			return GetSrc(GetDdrSrc());

		case clkSdhc1:
			return GetSrc(GetSdh1Src());

		case clkSdhc2:
			return GetSrc(GetSdh2Src());

		case clkUart:
			return GetSrc(GetUartSrc());

		case clkUsboh3:
			return GetSrc(GetUsbSrc());

		case clkUsbPhy:
			return GetSrc(GetPhySrc());

		case clkEcspi:
			return GetSrc(GetSpiSrc());

		case clkEnfc:
			return GetSrc(clkEmi);

		case clkSync:
		case clkSdhc3:
		case clkSdhc4:
		case clkSsi1:
		case clkSsi3:
		case clkSsi2:
		case clkSsiExt1:
		case clkSsiExt2:
		case clkTv:
		case clkDi1:
		case clkVpuR:
		case clkSpdif0:
		case clkSpdif1:
		case clkSim:
		case clkFiri:
		case clkHsI2c:
		case clkSsi:
		case clkMclk1:
		case clkMclk2:
		case clkWrck:
		case clkLpsr:
		case clkPgc:

			// Not Supported

			HostTrap(PANIC_DEVICE_CCM);

			break;
		}
	
	return 0;
	}

UINT CCcm51::GetFreq(UINT uClk) const
{
	switch( uClk ) {

		case clkPll1:
			return m_ppDpll[0]->Get();

		case clkPll2:
			return m_ppDpll[1]->Get();

		case clkPll3:
			return m_ppDpll[2]->Get();
			
		case clkLpArm:
			return 0;
		
		case clkArmRoot:
			return GetFreq(GetArmSrc()) / GetArmDiv();
			
		case clkAxia:
			return GetFreq(GetAxxSrc()) / GetAxiaDiv();
			
		case clkAxib:
			return GetFreq(GetAxxSrc()) / GetAxibDiv();

		case clkEmi:
			return GetFreq(GetEmiSrc()) / GetEmiDiv();

		case clkAhb:
			return GetFreq(GetAxxSrc()) / GetAhbDiv();

		case clkIpg:
			return GetFreq(clkAhb) / GetIpgDiv();
	
		case clkPer:
			return GetFreq(GetPerSrc()) / GetPerDiv();

		case clkGpu2:
			return GetFreq(GetGpu2Src());

		case clkArmAxi:
			return GetFreq(GetAxiSrc());

		case clkIpuHsp:
			return GetFreq(GetIpuSrc());

		case clkGpu:
			return GetFreq(GetGpuSrc());

		case clkVpu:
			return GetFreq(GetVpuSrc());

		case clkDdr:
			return GetFreq(GetDdrSrc()) / GetDdrDiv();

		case clkSdhc1:
			return GetFreq(GetSdh1Src()) / GetSdh1Div();

		case clkSdhc2:
			return GetFreq(GetSdh2Src()) / GetSdh2Div();

		case clkUart:
			return GetFreq(GetUartSrc()) / GetUartDiv();

		case clkUsboh3:
			return GetFreq(GetUsbSrc()) / GetUsbDiv();

		case clkUsbPhy:
			return GetFreq(GetPhySrc()) / GetPhyDiv();

		case clkEcspi:
			return GetFreq(GetSpiSrc()) / GetSpiDiv();

		case clkOsc:
			return 26000000;

		case clkEnfc:
			return GetFreq(clkEmi) / GetEnfcDiv();

		case clkSync:
		case clkSdhc3:
		case clkSdhc4:
		case clkSsi1:
		case clkSsi3:
		case clkSsi2:
		case clkSsiExt1:
		case clkSsiExt2:
		case clkTv:
		case clkDi1:
		case clkVpuR:
		case clkSpdif0:
		case clkSpdif1:
		case clkSim:
		case clkFiri:
		case clkHsI2c:
		case clkSsi:
		case clkMclk1:
		case clkMclk2:
		case clkWrck:
		case clkLpsr:
		case clkPgc:

			// Not Supported

			HostTrap(PANIC_DEVICE_CCM);

			break;
		}
	
	return 0;
	}

void CCcm51::SetFpm(UINT uMode)
{
	if( uMode == fpmOff ) {

		Reg(Ctrl) |= Bit(12);

		while( !(Reg(Status) & Bit(5)) );

		Reg(Ctrl) &= ~Bit(8);
		}
	else {
		if( uMode == fpm512 ) {

			Reg(Ctrl) &= ~Bit(11);
			}

		if( uMode == fpm1024 ) {

			Reg(Ctrl) |= Bit(11);
			}
		
		Reg(Ctrl) |= Bit(8);
		}
	}

void CCcm51::EnableAmps(BOOL fEnable)
{
	if( fEnable ) {

		Reg(Ctrl) |=  (Bit(10) | Bit(9));
		}
	else {
		Reg(Ctrl) &= ~(Bit(10) | Bit(9));
		}
	}

bool CCcm51::Set(UINT uClk, UINT uSrc)
{
	return Set(uClk, uSrc, 0, 0);
	}

bool CCcm51::Set(UINT uClk, UINT uSrc, UINT uPod)
{
	return Set(uClk, uSrc, 0, uPod);
	}

bool CCcm51::Set(UINT uClk, UINT uSrc, UINT uPre, UINT uPod)
{
	switch( uClk ) {

		case clkPll1:
		case clkPll2:
		case clkPll3:
		case clkLpArm:
			
			return false;

		case clkArmRoot:

			SetArmSrc(uSrc);

			SetArmDiv(uPod);

			return true;

		case clkAxia:

			SetAxxSrc(uSrc);

			SetAxiaDiv(uPod);

			return true;

		case clkAxib:

			SetAxxSrc(uSrc);

			SetAxibDiv(uPod);

			return true;

		case clkAhb:

			SetAxxSrc(uSrc);

			SetAhbDiv(uPod);

			return true;

		case clkIpg:

			SetAxxSrc(uSrc);

			SetIpgDiv(uPod);

			return true;

		case clkEmi:

			SetEmiSrc(uSrc);

			SetEmiDiv(uPod);

			return true;

		case clkPer:

			SetPerSrc(uSrc);

			SetPerDiv(1, uPre, uPod);

			return true;
			
		case clkGpu2:
			
			SetGpu2Src(uSrc);

			return true;

		case clkArmAxi:
			
			SetAxiSrc(uSrc);

			return true;

		case clkIpuHsp:
			
			SetIpuSrc(uSrc);

			return true;

		case clkGpu:

			SetGpuSrc(uSrc);

			return true;

		case clkVpu:
			
			SetVpuSrc(uSrc);

			return true;

		case clkDdr:

			SetDdrSrc(uSrc);

			SetDdrDiv(uPod);

			return true;

		case clkSdhc1:
			
			SetSdh1Src(uSrc);

			SetSdh1Div(uPre, uPod);

			return true;

		case clkSdhc2:
			
			SetSdh2Src(uSrc);

			SetSdh2Div(uPre, uPod);

			return true;

		case clkUart:
			
			SetUartSrc(uSrc);

			SetUartDiv(uPre, uPod);

			return true;

		case clkUsboh3:

			SetUsbSrc(uSrc);

			SetUsbDiv(uPre, uPod);

			return true;

		case clkUsbPhy:

			SetPhySrc(uSrc);

			SetPhyDiv(uPre, uPod);

			return true;

		case clkEcspi:
			
			SetSpiSrc(uSrc);

			SetSpiDiv(uPre, uPod);

			return true;

		case clkOsc:

			return false;

		case clkEnfc:

			SetEnfcDiv(uPod);

			return true;

		case clkSync:
		case clkSdhc3:
		case clkSdhc4:
		case clkSsi1:
		case clkSsi3:
		case clkSsi2:
		case clkSsiExt1:
		case clkSsiExt2:
		case clkTv:
		case clkDi1:
		case clkVpuR:
		case clkSpdif0:
		case clkSpdif1:
		case clkSim:
		case clkFiri:
		case clkHsI2c:
		case clkSsi:
		case clkMclk1:
		case clkMclk2:
		case clkWrck:
		case clkLpsr:
		case clkPgc:

			// Not Supported

			HostTrap(PANIC_DEVICE_CCM);

			break;
		}
	
	return false;
	}

void CCcm51::SetGating(bool fOn)
{
	Reg(ClkGate0)   = fOn ? 0x3F0F000F : 0xFFFFFFFF;

	Reg(ClkGate1)   = fOn ? 0x00000000 : 0xFFFFFFFF;

	Reg(ClkGate2)   = fOn ? 0x00000000 : 0xFFFFFFFF;

	Reg(ClkGate3)   = fOn ? 0x00000000 : 0xFFFFFFFF;

	Reg(ClkGate4)   = fOn ? 0x0000FFC0 : 0xFFFFFFFF;

	Reg(ClkGate5)   = fOn ? 0x00FFC000 : 0xFFFFFFFF;

	Reg(ClkGate6)   = fOn ? 0x00000000 : 0x0000FFFF;
	}

void CCcm51::SetArmPod(UINT uPod)
{
	Reg(DVFSCtrl) |= Bit(2);

	while( Reg(DivHandshake) & Bit(16) );

	SetArmDiv(uPod);
	}

// Eval Board

void CCcm51::SetTestOutput(void)
{
	enum 
	{
		clkArm	=  0,
		clkPLL1	=  1,
		clkPLL2	=  2,
		clkPLL3	=  3,
		clkEmi	=  4,
		clkEnfc	=  6,
		clkDi	=  8,
		clkAhb	= 11,
		clkIpg	= 12,
		clkPer	= 13, 
		clkSync	= 14
		};
	
	Reg(ClkOpSrc) = Bit(7) | (7 << 4) | clkPLL3;
	}

// Sources

UINT CCcm51::GetArmSrc(void) const
{
	if( Reg(ClkSwitch) & Bit(2) ) {

		switch( GetReg(regClkSwitch, 7, 2) ) {

			case 2: return clkPll2;
			case 3: return clkPll3;
			}
		}
	
	return clkPll1;
	}

UINT CCcm51::GetAxxSrc(void) const
{
	if( Reg(BusClkDiv) & Bit(25) ) {

		switch( GetReg(regBusClkMux, 12, 2) ) {

			case 0: return clkPll1;
			case 1: return clkPll3;
			case 2: return clkLpArm;
			}

		return 0;
		}
	
	return clkPll2;
	}

UINT CCcm51::GetEmiSrc(void) const
{
	if( Reg(BusClkDiv) & Bit(26) ) {

		return clkAhb;
		}

	return GetAxxSrc();
	}

UINT CCcm51::GetPerSrc(void) const
{
	if( Reg(BusClkMux) & Bit(0) ) {

		return clkIpg;
		}

	if( Reg(BusClkMux) & Bit(1) ) {
			
		return clkLpArm;
		}

	return GetAxxSrc();
	}

UINT CCcm51::GetGpu2Src(void) const
{
	return GetReg(regBusClkMux, 16, 2) + 6;
	}

UINT CCcm51::GetAxiSrc(void) const
{
	return GetReg(regBusClkMux, 8, 2) + 6;
	}

UINT CCcm51::GetIpuSrc(void) const
{
	return GetReg(regBusClkMux, 6, 2) + 6;
	}

UINT CCcm51::GetGpuSrc(void) const
{
	return GetReg(regBusClkMux, 4, 2) + 6;
	}

UINT CCcm51::GetVpuSrc(void) const
{
	return GetReg(regBusClkMux, 14, 2) + 6;
	}

UINT CCcm51::GetDdrSrc(void) const
{
	if( Reg(BusClkDiv) & Bit(30) )  {

		return clkPll1;
		}

	return GetReg(regBusClkMux, 10, 2) + 6;
	}

UINT CCcm51::GetSdh1Src(void) const
{
	return GetReg(regSerClkMux1, 20, 2) + 1;
	}

UINT CCcm51::GetSdh2Src(void) const
{
	return GetReg(regSerClkMux1, 16, 2) + 1;
	}

UINT CCcm51::GetUartSrc(void) const
{
	return GetReg(regSerClkMux1, 24, 2) + 1;
	}

UINT CCcm51::GetUsbSrc(void) const
{
	return GetReg(regSerClkMux1, 22, 2) + 1;
	}

UINT CCcm51::GetPhySrc(void) const
{
	return (Reg(SerClkMux1) & Bit(26)) ? clkPll3 : clkOsc;
	}

UINT CCcm51::GetSpiSrc(void) const
{
	return GetReg(regSerClkMux1, 4, 2) + 1;
	}

void CCcm51::SetArmSrc(UINT uSrc)
{
	Reg(ClkSwitch) &= ~Bit(2);

	if( uSrc == clkPll2 || uSrc == clkPll3 ) {

		SetReg(regClkSwitch, 7, 2, uSrc);
				
		Reg(ClkSwitch) |= Bit(2);
		}
	else {
		Reg(ClkSwitch) = 0;
		}
	}

void CCcm51::SetAxxSrc(UINT uSrc)
{
	if( uSrc != clkPll2 ) {

		Reg(BusClkDiv) |= Bit(25);
		
		switch( uSrc ) {

			case clkPll1:  uSrc = 0; break;
			case clkPll3:  uSrc = 1; break;
			case clkLpArm: uSrc = 2; break;
			}

		SetReg(regBusClkMux, 12, 2, uSrc);
		}
	else {
		Reg(BusClkDiv) &= ~Bit(25);
		}
	}

void CCcm51::SetEmiSrc(UINT uSrc)
{
	if( uSrc != clkAhb ) {

		Reg(BusClkDiv) &= ~Bit(26);

		SetAxxSrc(uSrc);
		}
	else {
		Reg(BusClkDiv) |= Bit(26);
		}
	}

void CCcm51::SetPerSrc(UINT uSrc)
{
	if( uSrc == clkIpg ) {
		
		Reg(BusClkMux) |= Bit(0);
		}
	else {
		Reg(BusClkMux) &= ~Bit(0);

		if( uSrc == clkLpArm ) {

			Reg(BusClkMux) |= Bit(1);
			}
		else {
			Reg(BusClkMux) &= ~Bit(1);
			
			SetAxxSrc(uSrc);
			}
		}
	}

void CCcm51::SetGpu2Src(UINT uSrc)
{
	SetReg(regBusClkMux, 16, 2, uSrc-6);
	}

void CCcm51::SetAxiSrc(UINT uSrc)
{
	SetReg(regBusClkMux, 8, 2, uSrc-6);
	}

void CCcm51::SetIpuSrc(UINT uSrc)
{
	SetReg(regBusClkMux, 6, 2, uSrc-6);
	}

void CCcm51::SetGpuSrc(UINT uSrc)
{
	SetReg(regBusClkMux, 4, 2, uSrc-6);
	}

void CCcm51::SetVpuSrc(UINT uSrc)
{
	SetReg(regBusClkMux, 14, 2, uSrc-6);
	}

void CCcm51::SetSdh1Src(UINT uSrc)
{	
	SetReg(regSerClkMux1, 20, 2, uSrc-1);
	}

void CCcm51::SetSdh2Src(UINT uSrc)
{
	SetReg(regSerClkMux1, 16, 2, uSrc-1);
	}

void CCcm51::SetUartSrc(UINT uSrc)
{
	SetReg(regSerClkMux1, 24, 2, uSrc-1);
	}

void CCcm51::SetUsbSrc(UINT uSrc)
{
	SetReg(regSerClkMux1, 22, 2, uSrc-1);
	}

void CCcm51::SetPhySrc(UINT uSrc)
{
	SetReg(regSerClkMux1, 26, 1, uSrc == clkOsc ? 0 : 1);
	}

void CCcm51::SetSpiSrc(UINT uSrc)
{
	SetReg(regSerClkMux1, 4, 2, uSrc-1);
	}

void CCcm51::SetDdrSrc(UINT uSrc)
{
	if( uSrc != clkPll1 ) {

		SetReg(regBusClkMux, 10, 2, uSrc-6);

		Reg(BusClkDiv) &= ~Bit(30);
		}
	else {
		Reg(BusClkDiv) |= Bit(30);
		}

	while( Reg(DivHandshake) & Bit(8) );
	}

// Dividers

UINT CCcm51::GetArmDiv(void) const
{
	return Reg(ArmClkRoot) + 1;
	}

UINT CCcm51::GetAxiaDiv(void) const 
{
	return GetReg(regBusClkDiv, 16, 3) + 1;
	}

UINT CCcm51::GetAxibDiv(void) const 
{
	return GetReg(regBusClkDiv, 19, 3) + 1;
	}

UINT CCcm51::GetAhbDiv(void) const 
{
	return GetReg(regBusClkDiv, 10, 3) + 1;
	}

UINT CCcm51::GetEmiDiv(void) const
{
	return GetReg(regBusClkDiv, 22, 3) + 1;
	}

UINT CCcm51::GetIpgDiv(void) const
{
	return GetReg(regBusClkDiv, 8, 2) + 1;
	}

UINT CCcm51::GetPerDiv(void) const
{
	if( GetPerSrc() != clkIpg ) {

		UINT uPre1 = GetReg(regBusClkDiv, 6, 2) + 1;

		UINT uPre2 = GetReg(regBusClkDiv, 3, 3) + 1;

		UINT uPod  = GetReg(regBusClkDiv, 0, 3) + 1;

		return uPre1 * uPre2 * uPod;
		}

	return 1;
	}

UINT CCcm51::GetDdrDiv(void) const
{
	if( GetDdrSrc() == clkPll1 ) {	

		return GetReg(regBusClkDiv, 27, 3) + 1;
		}

	return 1;
	}

UINT CCcm51::GetSdh1Div(void) const
{
	UINT uPre = GetReg(regSerClkDiv1, 16, 3) + 1;

	UINT uPod = GetReg(regSerClkDiv1, 11, 3) + 1;

	return uPre * uPod;
	}

UINT CCcm51::GetSdh2Div(void) const
{
	UINT uPre = GetReg(regSerClkDiv1, 22, 3) + 1;

	UINT uPod = GetReg(regSerClkDiv1, 19, 3) + 1;

	return uPre * uPod;
	}

UINT CCcm51::GetUartDiv(void) const
{
	UINT uPre = GetReg(regSerClkDiv1, 3, 3) + 1;

	UINT uPod = GetReg(regSerClkDiv1, 0, 3) + 1;

	return uPre * uPod;
	}

UINT CCcm51::GetUsbDiv(void) const
{
	UINT uPre = GetReg(regSerClkDiv1, 8, 3) + 1;

	UINT uPod = GetReg(regSerClkDiv1, 6, 2) + 1;

	return uPre * uPod;
	}

UINT CCcm51::GetPhyDiv(void) const
{
	if( Reg(SerClkMux1) & Bit(26) ) {

		UINT uPre = GetReg(regDIClkDiv, 3, 3) + 1;

		UINT uPod = GetReg(regDIClkDiv, 0, 2) + 1;

		return uPre * uPod;
		}

	return 1;
	}

UINT CCcm51::GetSpiDiv(void) const
{
	UINT uPre = GetReg(regSerClkDiv2, 25, 3) + 1;

	UINT uPod = GetReg(regSerClkDiv2, 19, 6) + 1;

	return uPre * uPod;
	}

UINT CCcm51::GetEnfcDiv(void) const
{
	return GetReg(regBusClkDiv, 13, 3) + 1;
	}

void CCcm51::SetArmDiv(UINT uDiv)
{
	Reg(ArmClkRoot) = uDiv - 1;
	}

void CCcm51::SetAxiaDiv(UINT uDiv)
{
	SetReg(regBusClkDiv, 16, 3, uDiv-1);

	while( Reg(DivHandshake) & Bit(0) );
	}

void CCcm51::SetAxibDiv(UINT uDiv)
{
	SetReg(regBusClkDiv, 19, 3, uDiv-1);

	while( Reg(DivHandshake) & Bit(1) );
	}

void CCcm51::SetAhbDiv(UINT uDiv)
{
	SetReg(regBusClkDiv, 10, 3, uDiv-1);

	while( Reg(DivHandshake) & Bit(3) );
	}

void CCcm51::SetEmiDiv(UINT uDiv) 
{
	SetReg(regBusClkDiv, 22, 3, uDiv-1);

	while( Reg(DivHandshake) & Bit(2) );
	}

void CCcm51::SetIpgDiv(UINT uDiv)
{
	SetReg(regBusClkDiv, 8, 2, uDiv-1);
	}

void CCcm51::SetPerDiv(UINT uPre1, UINT uPre2, UINT uPod)
{
	SetReg(regBusClkDiv, 6, 2, uPre1-1, 3, 3, uPre2-1);

	SetReg(regBusClkDiv, 0, 3, uPod-1);
	}

void CCcm51::SetDdrDiv(UINT uDiv)
{
	SetReg(regBusClkDiv, 27, 3, uDiv-1);

	while( Reg(DivHandshake) & Bit(7) );
	}

void CCcm51::SetSdh1Div(UINT uPre, UINT uPod)
{
	SetReg(regSerClkDiv1, 16, 3, uPre-1, 11, 3, uPod-1);
	}

void CCcm51::SetSdh2Div(UINT uPre, UINT uPod)
{
	SetReg(regSerClkDiv1, 22, 3, uPre-1, 19, 3, uPod-1);
	}

void CCcm51::SetUartDiv(UINT uPre, UINT uPod)
{
	SetReg(regSerClkDiv1, 3, 3, uPre-1, 0, 3, uPod-1);
	}

void CCcm51::SetUsbDiv (UINT uPre, UINT uPod)
{
	SetReg(regSerClkDiv1, 8, 3, uPre-1, 6, 2, uPod-1);
	}

void CCcm51::SetPhyDiv (UINT uPre, UINT uPod)
{
	SetReg(regDIClkDiv, 3, 3, uPre-1, 0, 3, uPod-1);
	}

void CCcm51::SetSpiDiv(UINT uPre, UINT uPod)
{
	SetReg(regSerClkDiv2, 25, 3, uPre-1, 19, 6, uPod-1);
	}

void CCcm51::SetEnfcDiv(UINT uDiv)
{
	SetReg(regBusClkDiv, 13, 3, uDiv-1);

	while( Reg(DivHandshake) & Bit(4) );
	}

// Implementation

DWORD CCcm51::GetReg(UINT uReg, UINT nBit, UINT nBits) const
{
	DWORD d = m_pBase[ uReg ];

	DWORD m = (1 << nBits) - 1;

	return (d >> nBit) & m;
	}

void CCcm51::SetReg(UINT uReg, UINT nBit, UINT nBits, DWORD d)
{
	DWORD r = m_pBase[uReg];

	DWORD m = (1 << nBits) - 1;

	d &=  m;
	
	r &= ~(m << nBit);

	r |=  (d << nBit);

	m_pBase[uReg] = r;
	}

void CCcm51::SetReg(UINT uReg, UINT nBit0, UINT nBits0, DWORD d0, UINT nBit1, UINT nBits1, DWORD d1)
{
	DWORD r = m_pBase[uReg];

	DWORD m0 = (1 << nBits0) - 1;

	DWORD m1 = (1 << nBits1) - 1;

	d0 &= m0;

	d1 &= m1;

	r  &= ~((m0 << nBit0) | (m1 << nBit1));

	r  |=  ((d0 << nBit0) | (d1 << nBit1));

	m_pBase[uReg] = r;
	}

// End of File
