
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_INic_HPP

#define INCLUDE_INic_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 7 -- Network Interface
//

interface INic;

//////////////////////////////////////////////////////////////////////////
//
// NIC Driver Diagnostics
//

struct NICDIAG
{
	UINT	m_TxCount;
	UINT	m_TxFail;
	UINT	m_TxDisc;
	UINT	m_RxCount;
	UINT	m_RxOver;
	UINT	m_RxDisc;
};

//////////////////////////////////////////////////////////////////////////
//
// NIC Driver Capabilities
//

enum
{
	nicFilterIp  = Bit(0),
	nicCheckSums = Bit(1),
	nicAddSums   = Bit(2),
};

//////////////////////////////////////////////////////////////////////////
//
// NIC Driver Flags
//

enum
{
	nicOnlyIp  = Bit(0),
};

//////////////////////////////////////////////////////////////////////////
//
// NIC Packet Flags
//

enum
{
	packetTypeValid = Bit(0),
	packetTypeIp    = Bit(1),
	packetTypeUdp   = Bit(2),
	packetTypeTcp   = Bit(3),
	packetSumsValid = Bit(4),
	packetAddSums	= Bit(5)
};

////////////////////////////////////////////////////////////////////////
//
// NIC Driver Interface
//

interface INic : public IDevice
{
	// LINK

	AfxDeclareIID(7, 3);

	virtual bool METHOD Open(bool fFast, bool fFull)                   = 0;
	virtual bool METHOD Close(void)                                    = 0;
	virtual bool METHOD InitMac(MACADDR const &Addr)                   = 0;
	virtual void METHOD ReadMac(MACADDR &Addr)                         = 0;
	virtual UINT METHOD GetCapabilities(void)                          = 0;
	virtual void METHOD SetFlags(UINT uFlags)                          = 0;
	virtual bool METHOD SetMulticast(MACADDR const *pList, UINT uList) = 0;
	virtual bool METHOD IsLinkActive(void)                             = 0;
	virtual bool METHOD WaitLink(UINT uTime)                           = 0;
	virtual bool METHOD SendData(CBuffer *  pBuff, UINT uTime)         = 0;
	virtual bool METHOD ReadData(CBuffer * &pBuff, UINT uTime)         = 0;
	virtual void METHOD GetCounters(NICDIAG &Diag)                     = 0;
	virtual void METHOD ResetCounters(void)                            = 0;
};

// End of File

#endif
