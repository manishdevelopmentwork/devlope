
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGradButton_HPP
	
#define	INCLUDE_PrimRubyGradButton_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyPenEdge;

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graduated Button Primitive
//

class CPrimRubyGradButton : public CPrimRubyWithText
{
	public:
		// Constructor
		CPrimRubyGradButton(void);

		// Destructor
		~CPrimRubyGradButton(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void MovePrim(int cx, int cy);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);

		// Data Members
		CPrimColor       * m_pColor1;
		CPrimColor       * m_pColor2;
		CPrimRubyPenEdge * m_pEdge;

	protected:
		// Data Members
		BOOL	     m_fFast;
		CRubyGdiList m_listFill;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listTrim;

		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR m_Color1;
			COLOR m_Color2;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
