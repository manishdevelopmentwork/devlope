
#include "Intern.hpp"

#include "G3ConfigService.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Configuration Service
//

// Instantiator

global ILinkService * Create_ConfigService(ICrimsonPxe *pPxe)
{
	return New CG3ConfigService(pPxe);
}

// Constructor

CG3ConfigService::CG3ConfigService(ICrimsonPxe *pPxe)
{
	m_pPxe    = pPxe;

	m_pDbase  = NULL;

	m_pConfig = NULL;

	m_pData   = NULL;

	AfxGetObject("c3.config-storage", 0, IConfigStorage, m_pConfig);

	AfxGetObject("c3.database", 0, IDatabase, m_pDbase);

	m_pDbase->Init(); // Should be done by pxe!!!

	StdSetRef();
}

// Destructor

CG3ConfigService::~CG3ConfigService(void)
{
	AfxRelease(m_pDbase);

	AfxRelease(m_pConfig);
}

// IUnknown

HRESULT CG3ConfigService::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ILinkService);

	StdQueryInterface(ILinkService);

	return E_NOINTERFACE;
}

ULONG CG3ConfigService::AddRef(void)
{
	StdAddRef();
}

ULONG CG3ConfigService::Release(void)
{
	StdRelease();
}

// ILinkService

void CG3ConfigService::Timeout(void)
{
}

UINT CG3ConfigService::Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans)
{
	if( Req.GetService() == servConfig ) {

		switch( Req.GetOpcode() ) {

			case configReadIP:
				return ReadIPConfig(Req, Rep);

			case configCheckVersion:
				return CheckVersion(Req, Rep);

			case configClearConfig:
				return ClearConfig(Req, Rep);

			case configClearGarbage:
				return ClearGarbage(Req, Rep);

			case configCheckItem:
				return CheckItem(Req, Rep);

			case configWriteItem:
				return WriteItem(Req, Rep);

			case configWriteItemEx:
				return WriteItemEx(Req, Rep);

			case configWriteData:
				return WriteData(Req, Rep);

			case configWriteVersion:
				return WriteVersion(Req, Rep);

			case configHaltSystem:
				return HaltSystem(Req, Rep);

			case configStartSystem:
				return StartSystem(Req, Rep);

			case configWriteTime:
				return WriteTime(Req, Rep);

			case configReadItem:
				return ReadItem(Req, Rep);

			case configReadData:
				return ReadData(Req, Rep);

			case configFlashMount:
				return FlashMount(Req, Rep);

			case configFlashDismount:
				return FlashDismount(Req, Rep);

			case configFlashVerify:
				return FlashVerify(Req, Rep);

			case configFlashFormat:
				return FlashFormat(Req, Rep);

			case configCheckCompression:
				return CheckCompresion(Req, Rep);

			case configCheckControl:
				return CheckControl(Req, Rep);

			case configCheckEditFlags:
				return CheckEditFlags(Req, Rep);

			case configClearEditFlags:
				return ClearEditFlags(Req, Rep);

			case configIdentify:
				return Identify(Req, Rep);
		}
	}

	return procError;
}

void CG3ConfigService::EndLink(CG3LinkFrame &Req)
{
}

// Implementation

UINT CG3ConfigService::ReadIPConfig(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	#if 0

	UINT uPtr  = 0;

	UINT uPort = UINT(Req.ReadByte(uPtr));

	CIpAddr  IP;

	CIpAddr  Mask;

	CMacAddr Mac;

	IRouter *pRouter = g_pRouter;

	if( pRouter && pRouter->GetConfig(uPort, IP, Mask, Mac) ) {

		CString Addr = IP.GetAsText();

		Rep.StartFrame(servConfig, opReply);

		Rep.AddData(PBYTE(PCTXT(Addr)), Addr.GetLength()+1);

		return procOkay;
	}

	#endif

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::CheckVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	if( m_pDbase->IsValid() ) {

		UINT   uPtr  = 0;

		PCBYTE pData = Req.ReadData(uPtr, 16);

		BYTE   bGuid[16];

		m_pDbase->GetVersion(bGuid);

		if( !memcmp(bGuid, pData, 16) ) {

			Rep.StartFrame(servConfig, opReply);

			Rep.AddByte(1);

			return procOkay;
		}
	}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::ClearConfig(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	m_pDbase->Clear();

	AfxGetAutoObject(pZone, "dev.tz", 0, ITimeZone);

	if( pZone ) {

		pZone->Lock(NOTHING);
	}

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
}

UINT CG3ConfigService::ClearGarbage(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	if( m_pDbase->GarbageCollect() ) {

		Rep.StartFrame(servConfig, opReply);

		Rep.AddByte(1);

		return procOkay;
	}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::CheckItem(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	UINT  uPtr   = 0;

	UINT  uItem  = UINT(INT(SHORT(Req.ReadWord(uPtr))));

	UINT  uClass = Req.ReadWord(uPtr);

	CItemInfo Info;

	if( m_pDbase->GetItemInfo(uItem, Info) ) {

		DWORD CRC = Req.ReadLong(uPtr);

		if( Info.m_uClass == uClass || Info.m_uClass == 0 ) {

			if( Info.m_CRC == CRC ) {

				Rep.StartFrame(servConfig, opReply);

				Rep.AddByte(1);

				return procOkay;
			}
		}
	}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::WriteItem(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	UINT  uPtr   = 0;

	UINT  uItem  = UINT(INT(SHORT(Req.ReadWord(uPtr))));

	UINT  uClass = Req.ReadWord(uPtr);

	UINT  uSize  = Req.ReadLong(uPtr);

	if( m_pDbase->CheckSpace(uSize) ) {

		InitItem(uItem, uClass, uSize, uSize);

		Rep.StartFrame(servConfig, opReply);

		Rep.AddByte(1);

		return procOkay;
	}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::WriteItemEx(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	UINT  uPtr   = 0;

	UINT  uItem  = UINT(INT(SHORT(Req.ReadWord(uPtr))));

	UINT  uClass = Req.ReadWord(uPtr);

	UINT  uSize  = Req.ReadLong(uPtr);

	UINT  uComp  = Req.ReadLong(uPtr);

	if( m_pDbase->CheckSpace(uComp) ) {

		InitItem(uItem, uClass, uSize, uComp);

		Rep.StartFrame(servConfig, opReply);

		Rep.AddByte(1);

		return procOkay;
	}

	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::WriteData(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	if( m_pData ) {

		UINT  uPtr   = 0;

		DWORD dwAddr = Req.ReadLong(uPtr);

		WORD  wCount = Req.ReadWord(uPtr);

		PBYTE pData  = PBYTE(Req.ReadData(uPtr, wCount));

		if( dwAddr + wCount <= m_Info.m_uComp ) {

			memcpy(m_pData + dwAddr, pData, wCount);

			if( dwAddr + wCount == m_Info.m_uComp ) {

				if( !WriteItem() ) {

					Rep.StartFrame(servConfig, opReply);

					Rep.AddByte(0);

					return procOkay;
				}
			}

			Rep.StartFrame(servConfig, opReply);

			Rep.AddByte(1);

			return procOkay;
		}
	}

	return procError;
}

UINT CG3ConfigService::WriteVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	UINT   uPtr  = 0;

	PCBYTE pData = Req.ReadData(uPtr, 16);

	ImageDisable('a');

	m_pDbase->SetRunning(FALSE);

	m_pDbase->SetVersion(pData);

	m_pDbase->SetRevision(0);

	m_pDbase->SetValid(TRUE);

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
}

UINT CG3ConfigService::HaltSystem(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	UINT uPtr     = 0;

	UINT uTimeout = Req.ReadLong(uPtr);

	BOOL fStopped = m_pPxe->SystemStop();

	Rep.StartFrame(servConfig, opReply);

	if( fStopped ) {

		m_pDbase->SetValid(FALSE);

		Rep.AddByte(1);
	}
	else
		Rep.AddByte(0);

	AfxTouch(uTimeout);

	return procOkay;
}

UINT CG3ConfigService::StartSystem(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	m_pDbase->SetValid(TRUE);

	m_pPxe->SystemStart();

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
}

UINT CG3ConfigService::WriteTime(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	struct tm tm = { 0 };

	UINT   uPtr  = 0;

	PCBYTE pTime = Req.ReadData(uPtr, 9);

	tm.tm_sec  = pTime[0];
	tm.tm_min  = pTime[1];
	tm.tm_hour = pTime[2];
	tm.tm_mday = pTime[3];
	tm.tm_mon  = pTime[4] - 1;
	tm.tm_year = pTime[5] + 100;

	WORD wZone = MAKEWORD(pTime[7], pTime[6]);

	INT  nMins = (int(wZone) - 12 * 60) + (pTime[8] ? 60 : 0);

	struct timeval tv;

	tv.tv_sec  = timegm(&tm) - 60 * nMins;

	tv.tv_usec = 0;

	settimeofday(&tv, NULL);

	Rep.StartFrame(servConfig, opAck);

	return procOkay;
}

UINT CG3ConfigService::ReadItem(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	FreeItem();

	UINT uPtr  = 0;

	UINT uItem = UINT(INT(SHORT(Req.ReadWord(uPtr))));

	Rep.StartFrame(servConfig, opReply);

	if( IsDevCon(uItem) ) {

		if( GetConfig(GetTag(uItem), m_Cache) ) {

			Rep.AddLong(m_Cache.GetLength());

			Rep.AddLong(m_Cache.GetLength());

			return procOkay;
		}
	}
	else {
		CItemInfo Info;

		if( m_pDbase->GetItemInfo(uItem, Info) ) {

			Rep.AddLong(Info.m_uComp);

			Rep.AddLong(Info.m_uSize);

			return procOkay;
		}
	}

	Rep.AddLong(0);

	return procOkay;
}

UINT CG3ConfigService::ReadData(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	UINT uPtr    = 0;

	UINT uItem   = UINT(INT(SHORT(Req.ReadWord(uPtr))));

	UINT uAddr   = Req.ReadLong(uPtr);

	UINT uCount  = Req.ReadWord(uPtr);

	if( IsDevCon(uItem) ) {

		if( uAddr + uCount <= m_Cache.GetLength() ) {

			Rep.StartFrame(servConfig, opReply);

			Rep.AddData(PCBYTE(PCSTR(m_Cache)) + uAddr, uCount);

			return procOkay;
		}
	}
	else {
		CItemInfo Info;

		PCBYTE pItem = PCBYTE(m_pDbase->LockItem(uItem, Info));

		if( pItem ) {

			if( uAddr + uCount <= Info.m_uComp ) {

				Rep.StartFrame(servConfig, opReply);

				Rep.AddData(pItem + uAddr, uCount);

				m_pDbase->FreeItem(uItem);

				return procOkay;
			}

			m_pDbase->FreeItem(uItem);
		}
	}

	return procError;
}

UINT CG3ConfigService::FlashMount(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);

	return procOkay;
}

UINT CG3ConfigService::FlashDismount(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);

	return procOkay;
}

UINT CG3ConfigService::FlashVerify(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);

	return procOkay;
}

UINT CG3ConfigService::FlashFormat(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);

	return procOkay;
}

UINT CG3ConfigService::CheckCompresion(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	Rep.AddByte(1);

	return procOkay;
}

UINT CG3ConfigService::CheckControl(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	ICrimsonApp *pApp = NULL;

	if( m_pPxe->LockApp(pApp) ) {

		if( pApp->IsRunningControl() ) {

			Rep.AddByte(1);

			m_pPxe->FreeApp();

			return procOkay;
		}

		m_pPxe->FreeApp();
	}

	Rep.AddByte(0);

	return procOkay;
}

UINT CG3ConfigService::CheckEditFlags(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	BYTE  bData = 0;

	PCSTR pTags = "hpsu";

	for( UINT n = 0; pTags[n]; n++ ) {

		if( m_pConfig->IsEdited(pTags[n]) ) {

			CString Text;

			m_pConfig->GetConfig(pTags[n], Text);

			PCBYTE pData = PCBYTE(PCSTR(Text));

			UINT   uData = Text.GetLength();

			Rep.AddLong(crc(pData, uData));

			bData |= (1 << n);
		}
		else {
			Rep.AddLong(0);
		}
	}

	Rep.AddByte(bData);

	return procOkay;
}

UINT CG3ConfigService::ClearEditFlags(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	UINT  uPtr  = 0;

	BYTE  bData = Req.ReadByte(uPtr);

	PCSTR pTags = "hpsu";

	for( UINT n = 0; pTags[n]; n++ ) {

		if( bData & (1 << n) ) {

			m_pConfig->SetEdited(pTags[n], false);
		}
	}

	Rep.AddByte(bData);

	return procOkay;
}

UINT CG3ConfigService::Identify(CG3LinkFrame &Req, CG3LinkFrame &Rep)
{
	Rep.StartFrame(servConfig, opReply);

	AfxGetAutoObject(pIdent, "identifier", 0, IDeviceIdentifier);

	if( pIdent ) {

		UINT uPtr  = 0;

		BYTE bCode = Req.ReadByte(uPtr);

		CAutoBuffer pSend(256);

		pIdent->RunOperation(bCode, TRUE, pSend);

		Rep.AddByte(BYTE(pSend->GetSize()));

		Rep.AddData(pSend->GetData(), pSend->GetSize());
	}
	else {
		Rep.AddByte(0);
	}

	return procOkay;
}

// Item Helpers

void CG3ConfigService::FreeItem(void)
{
	if( m_pData ) {

		delete m_pData;

		m_pData = NULL;
	}
}

void CG3ConfigService::InitItem(UINT uItem, UINT uClass, UINT uSize, UINT uComp)
{
	m_Info.m_uItem  = uItem;

	m_Info.m_uClass = uClass;

	m_Info.m_uSize  = uSize;

	m_Info.m_uComp  = uComp;

	m_pData         = New BYTE[uComp];
}

BOOL CG3ConfigService::WriteItem(void)
{
	if( m_pData ) {

		if( m_pDbase->WriteItem(m_Info, m_pData) ) {

			FreeItem();

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CG3ConfigService::IsDevCon(UINT uItem)
{
	return (uItem & 0xFFFFFF00) == 0xFFFFFF00;
}

BOOL CG3ConfigService::GetConfig(char cTag, CString &Text)
{
	if( m_pConfig->GetConfig(cTag, Text) ) {

		if( cTag == 's' ) {

			CString Hard;

			if( m_pConfig->GetConfig('h', Hard) ) {

				Text += '\xFF';

				Text += Hard;

				return TRUE;
			}

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

char CG3ConfigService::GetTag(UINT uItem)
{
	switch( LOWORD(uItem) ) {

		case 0xFF01: return 'h';
		case 0xFF02: return 'p';
		case 0xFF03: return 's';
		case 0xFF04: return 'u';
	}

	return 0;
}

// End of File
