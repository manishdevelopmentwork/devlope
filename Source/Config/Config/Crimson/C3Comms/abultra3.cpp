
#include "intern.hpp"

#include "abultra3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ABUltra3000 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABUltra3DeviceOptions, CUIItem);

// Constructor

CABUltra3DeviceOptions::CABUltra3DeviceOptions(void)
{
	m_Drop		= 0;
	m_Broadcast     = 0;
	}

// Download Support

BOOL CABUltra3DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Broadcast));

	return TRUE;
	}

// Meta Data Creation

void CABUltra3DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Broadcast);
	}

//////////////////////////////////////////////////////////////////////////
//
// ABUltra3000 Driver
//

// Instantiator

ICommsDriver *	Create_ABUltra3Driver(void)
{
	return New CABUltra3Driver;
	}

// Constructor

CABUltra3Driver::CABUltra3Driver(void)
{
	m_wID		= 0x3380;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Ultra 3000";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Ultra 3000";

	AddSpaces();

	C3_PASSED();
	}

// Binding Control

UINT	CABUltra3Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CABUltra3Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CABUltra3Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABUltra3DeviceOptions);
	}

// Address Management

BOOL CABUltra3Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CABUltra3AddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CABUltra3Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		CString sTable = "";

		BOOL fIsNamed  = pSpace->m_uTable == addrNamed;

		CString sErr   = pSpace->m_Prefix + "(n)";

		UINT uOffset   = fIsNamed ? pSpace->m_uMinimum : 0;

		UINT uFunction = 0;

		Text.MakeUpper();

		UINT uFind1    = Text.Find('(');

		UINT uFind2    = Text.FindRev(')');

		if( (uFind1 < uFind2 - 1) && (uFind2 < NOTHING) ) {

			TCHAR f = Text[uFind1+1];

			if( f >= '0' && f <= '9' ) {

				uFunction = f - '0';
				}

			else {
				if( f == 'A' || f == 'B' ) {

					uFunction = f - '7';
					}
				}

			sErr = pSpace->m_Prefix + sTable;

			if( !fIsNamed ) {

				sTable = Text.Left(uFind1);

				uOffset = tatoi(sTable);

				if( uOffset > pSpace->m_uMaximum ) {

					sErr.Printf("%s 0 - %d",
						pSpace->m_Prefix,
						pSpace->m_uMaximum
						);

					Error.Set( sErr,
						0
						);

					return FALSE;
					}
				}

			if( Validate( pSpace, uFunction, &sErr ) ) {

				Addr.a.m_Offset = uOffset;
				Addr.a.m_Type   = LL;
				Addr.a.m_Table  = fIsNamed ? addrNamed : pSpace->m_uTable;
				Addr.a.m_Extra  = uFunction;

				return TRUE;
				}
			}

		Error.Set( sErr,
			0
			);

		return FALSE;
		}

	return FALSE;
	}

BOOL CABUltra3Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable != addrNamed ) {

		Text.Printf( "%s%d(%1.1X)",
			pSpace->m_Prefix,
			Addr.a.m_Offset,
			Addr.a.m_Extra
			);

		return TRUE;
		}

	CString s = pSpace->m_Prefix;

	if( pSpace->m_uMinimum >= SPGENERAL && pSpace->m_uMinimum <= SPMOTORMODEL ) {

		s = pSpace->m_Caption;
		}
				
	Text.Printf( "%s(%1.1X)",
		s,
		Addr.a.m_Extra
		);

	return TRUE;
	}

// Implementation	

void CABUltra3Driver::AddSpaces(void)
{
	AddSpace(New CSpace(AN, " ",	"GENERAL...",				10, SPGENERAL,  FZ, LL));
	AddSpace(New CSpace(AN, "C000",	"   Product Type",			10, 0x000,  FA, LL));
	AddSpace(New CSpace(AN, "C001",	"   Power Up Status",			10, 0x001,  FA, LL));
	AddSpace(New CSpace(AN, "C002",	"   Firmware Version",			10, 0x002,  FA, LL));
	AddSpace(New CSpace(AN, "C179",	"   Development Firmware Version",	10, 0x179,  FA, LL));
	AddSpace(New CSpace(AN, "C003",	"   Boot Firmware Version",		10, 0x003,  FA, LL));
	AddSpace(New CSpace(AN, "C004",	"   Factory Defaults->User Parameters",	10, 0x004,  FB, LL));
	AddSpace(New CSpace(AN, "C016",	"   Position Window Size",		10, 0x016,  FH, LL));
	AddSpace(New CSpace(AN, "C017",	"   Position Window Time",		10, 0x017,  FH, LL));
	AddSpace(New CSpace(AN, "C022",	"   Zero Speed Limit",			10, 0x022,  FH, LL));
	AddSpace(New CSpace(AN, "C023",	"   Speed Window Size",			10, 0x023,  FH, LL));
	AddSpace(New CSpace(AN, "C025",	"   Up to Speed Limit",			10, 0x025,  FH, LL));
	AddSpace(New CSpace(AN, "C02F",	"   Forward Current Limit",		10, 0x02F,  FH, LL));
	AddSpace(New CSpace(AN, "C030",	"   Reverse Current Limit",		10, 0x030,  FH, LL));
	AddSpace(New CSpace(AN, "C043",	"   Override Mode",			10, 0x043,  FH, LL));
	AddSpace(New CSpace(AN, "C05A",	"   Operation Mode",			10, 0x05A,  FH, LL));
	AddSpace(New CSpace(AN, "C061",	"   Host Control Mode",			10, 0x061,  FD, LL));
	AddSpace(New CSpace(AN, "C06A",	"   Reset Drive",			10, 0x06A,  FB, LL));
	AddSpace(New CSpace(AN, "C06B",	"   Drive Enable/Disable",		10, 0x06B,  FD, LL));
	AddSpace(New CSpace(AN, "C06C",	"   Setpoint Current",			10, 0x06C,  FD, LL));
	AddSpace(New CSpace(AN, "C06D",	"   Setpoint Velocity",			10, 0x06D,  FD, LL));
	AddSpace(New CSpace(AN, "C06E",	"   Setpoint Acceleration",		10, 0x06E,  FD, LL));
	AddSpace(New CSpace(AN, "C06F",	"   Reset Faults",			10, 0x06F,  FB, LL));
	AddSpace(New CSpace(01, "C15D",	"   Position Comparator",		10, 0,       1, LL));
	AddSpace(New CSpace(02, "C15E",	"   Position Comparator Polarity",	10, 0,       1, LL));
	AddSpace(New CSpace(AN, "C193",	"   Machine Cycle Size",		10, 0x193,  FH, LL));
	AddSpace(New CSpace(AN, "C194",	"   Position Rollover Enable/Disable",	10, 0x194,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"COMMUNICATION...",			10, SPCOMMS,  FZ, LL));
	AddSpace(New CSpace(AN, "C052",	"   Drive Address",			10, 0x052,  FE, LL));
	AddSpace(New CSpace(AN, "C053",	"   Broadcast Address",			10, 0x053,  FF, LL));
//	AddSpace(New CSpace(AN, "C10A",	"   Reset Serial Port",			10, 0x10A,  FB, LL));

	AddSpace(New CSpace(AN, " ",	"ANALOG OPERATING MODE...",		10, SPANALOGMODE,  FZ, LL));
	AddSpace(New CSpace(AN, "C028",	"   Analog Position Scale",		10, 0x028,  FH, LL));
	AddSpace(New CSpace(AN, "C029",	"   Analog Position Offset",		10, 0x029,  FH, LL));
	AddSpace(New CSpace(AN, "C118",	"   Analog Velocity Scale",		10, 0x118,  FH, LL));
	AddSpace(New CSpace(AN, "C047",	"   Analog Velocity Offset",		10, 0x047,  FH, LL));
	AddSpace(New CSpace(AN, "C119",	"   Analog Current Scale",		10, 0x119,  FH, LL));
	AddSpace(New CSpace(AN, "C049",	"   Analog Current Offset",		10, 0x049,  FH, LL));
	AddSpace(New CSpace(AN, "C05D",	"   Analog Acceleration Limit",		10, 0x05D,  FH, LL));
	AddSpace(New CSpace(AN, "C05E",	"   Analog Deceleration Limit",		10, 0x05E,  FH, LL));
	AddSpace(New CSpace(AN, "C0A8",	"   Analog Acceleration Enable/Disable",10, 0x0A8,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"PRESET OPERATING MODE...",		10, SPPRESETMODE,  FZ, LL));
	AddSpace(New CSpace( 3, "C05B",	"   Preset Velocity",			10, 0,       7, LL));
	AddSpace(New CSpace( 4, "C05C",	"   Preset Current",			10, 0,       7, LL));
	AddSpace(New CSpace(AN, "C05F",	"   Preset Velocity Acceleration Limit",10, 0x05F,  FH, LL));
	AddSpace(New CSpace(AN, "C060",	"   Preset Velocity Deceleration Limit",10, 0x060,  FH, LL));
	AddSpace(New CSpace(AN, "C0A9",	"   Velocity Accel/Decel Limit Enable/Disable",10, 0x0A9,  FH, LL));
	AddSpace(New CSpace( 5, "C00D",	"   Preset Position",			10, 0,       7, LL));
	AddSpace(New CSpace( 6, "C00E",	"   Preset Position Velocity",		10, 0,       7, LL));
	AddSpace(New CSpace( 7, "C02B",	"   Preset Position Acceleration",	10, 0,       7, LL));
	AddSpace(New CSpace( 8, "C093",	"   Preset Position Deceleration",	10, 0,       7, LL));

	AddSpace(New CSpace(AN, " ",	"FOLLOWER OPERATING MODE...",		10, SPFOLLOWMODE,  FZ, LL));
	AddSpace(New CSpace( 9, "C01B",	"   Master Gear Ratio",			10, 0,       7, LL));
	AddSpace(New CSpace(10, "C01A",	"   Motor Gear Ratio",			10, 0,       7, LL));
	AddSpace(New CSpace(AN, "C0AB",	"   Rotation Direction",		10, 0x0AB,  FH, LL));
	AddSpace(New CSpace(AN, "C01D",	"   Slew Limit",			10, 0x01D,  FH, LL));
	AddSpace(New CSpace(AN, "C01E",	"   Slew Limit Enable/Disable",		10, 0x01E,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"INDEXING OPERATING MODE...",		10, SPINDEXMODE,  FZ, LL));
	AddSpace(New CSpace(AN, "C058",	"   Auto-start Indexing",		10, 0x058,  FH, LL));
	AddSpace(New CSpace(AN, "C070",	"   Start Index",			10, 0x070,  FB, LL));
	AddSpace(New CSpace(AN, "C0B8",	"   Host Index",			10, 0x0B8,  FD, LL));
	AddSpace(New CSpace(11, "C0AE",	"   Index Type",			10, 0,    0x3F, LL));
	AddSpace(New CSpace(12, "C0AF",	"   Index Distance/Position",		10, 0,    0x3F, LL));
	AddSpace(New CSpace(13, "C0B0",	"   Index Registration Distance",	10, 0,    0x3F, LL));
	AddSpace(New CSpace(14, "C0B1",	"   Index Velocity",			10, 0,    0x3F, LL));
	AddSpace(New CSpace(15, "C0B2",	"   Index Acceleration",		10, 0,    0x3F, LL));
	AddSpace(New CSpace(16, "C0B3",	"   Index Deceleration",		10, 0,    0x3F, LL));
	AddSpace(New CSpace(17, "C0B4",	"   Index Dwell",			10, 0,    0x3F, LL));
	AddSpace(New CSpace(18, "C0B5",	"   Index Count",			10, 0,    0x3F, LL));
	AddSpace(New CSpace(19, "C0B6",	"   Index Termination",			10, 0,    0x3F, LL));
	AddSpace(New CSpace(20, "C0B7",	"   Index Pointer",			10, 0,    0x3F, LL));
	AddSpace(New CSpace(AN, "C0B9",	"   Index Abort Deceleration",		10, 0x0B9,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"OVERTRAVEL...",			10, SPOVERTRAVEL,  FZ, LL));
	AddSpace(New CSpace(AN, "C0D8",	"   Positive Soft Position Limit",	10, 0x0D8,  FH, LL));
	AddSpace(New CSpace(AN, "C0D9",	"   Negative Soft Position Limit",	10, 0x0D9,  FH, LL));
	AddSpace(New CSpace(AN, "C0DA",	"   Positive Deceleration Distance",	10, 0x0DA,  FH, LL));
	AddSpace(New CSpace(AN, "C0DB",	"   Negative Deceleration Distance",	10, 0x0DB,  FH, LL));
	AddSpace(New CSpace(AN, "C0DD",	"   Soft Travel Enable/Disable",	10, 0x0DD,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"HOMING...",				10, SPHOMING,  FZ, LL));
	AddSpace(New CSpace(AN, "C071",	"   Define Home",				10, 0x071,  FB, LL));
	AddSpace(New CSpace(AN, "C0C4",	"   Auto-start Homing",			10, 0x0C4,  FH, LL));
	AddSpace(New CSpace(AN, "C0C5",	"   Homing Type",			10, 0x0C5,  FH, LL));
	AddSpace(New CSpace(AN, "C0C6",	"   Homing Velocity",			10, 0x0C6,  FH, LL));
	AddSpace(New CSpace(AN, "C0C7",	"   Homing Acceleration",		10, 0x0C7,  FH, LL));
	AddSpace(New CSpace(AN, "C0C8",	"   Offset Move Distance",		10, 0x0C8,  FH, LL));
	AddSpace(New CSpace(AN, "C0C9",	"   Home Position",			10, 0x0C9,  FH, LL));
	AddSpace(New CSpace(AN, "C0CD",	"   Start Homing",			10, 0x0CD,  FB, LL));
	AddSpace(New CSpace(AN, "C0CA",	"   Homing Backoff Enable/Disable",	10, 0x0CA,  FH, LL));
	AddSpace(New CSpace(AN, "C0CB",	"   Home Sensor Polarity",		10, 0x0CB,  FH, LL));
	AddSpace(New CSpace(AN, "C0CC",	"   Homing Creep Velocity",		10, 0x0CC,  FH, LL));
	AddSpace(New CSpace(AN, "C0D7",	"   Homing Abort Deceleration",		10, 0x0D7,  FH, LL));
	AddSpace(New CSpace(AN, "C15F",	"   Home Current",			10, 0x15F,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"MOTOR...",				10, SPMOTOR,  FZ, LL));
	AddSpace(New CSpace(AN, "C037",	"   Encoder Lines/Revolution",		10, 0x037,  FH, LL));
	AddSpace(New CSpace(AN, "C038",	"   Maximum Rotary Speed",		10, 0x038,  FH, LL));
	AddSpace(New CSpace(AN, "C039",	"   Intermittent Current",		10, 0x039,  FH, LL));
	AddSpace(New CSpace(AN, "C03A",	"   Continuous Current",		10, 0x03A,  FH, LL));
	AddSpace(New CSpace(AN, "C03B",	"   Torque Constant",			10, 0x03B,  FH, LL));
	AddSpace(New CSpace(AN, "C03C",	"   Inertia",				10, 0x03C,  FH, LL));
	AddSpace(New CSpace(AN, "C03D",	"   Resistance",			10, 0x03D,  FH, LL));
	AddSpace(New CSpace(AN, "C03E",	"   Inductance",			10, 0x03E,  FH, LL));
	AddSpace(New CSpace(AN, "C03F",	"   Thermostat",			10, 0x03F,  FH, LL));
	AddSpace(New CSpace(AN, "C040",	"   Commutation Type",			10, 0x040,  FH, LL));
	AddSpace(New CSpace(AN, "C041",	"   Poles/Revolution",			10, 0x041,  FH, LL));
	AddSpace(New CSpace(AN, "C042",	"   Hall Offset",			10, 0x042,  FH, LL));
	AddSpace(New CSpace(AN, "C0AA",	"   Motor Thermal Protection Enable",	10, 0x0AA,  FH, LL));
	AddSpace(New CSpace(AN, "C095",	"   Standard Motor Flag",		10, 0x095,  FH, LL));
	AddSpace(New CSpace(AN, "C0DE",	"   Motor Type",			10, 0x0DE,  FH, LL));
	AddSpace(New CSpace(AN, "C0DF",	"   Encoder Type",			10, 0x0DF,  FH, LL));
	AddSpace(New CSpace(AN, "C0E0",	"   Startup Commutation",		10, 0x0E0,  FH, LL));
	AddSpace(New CSpace(AN, "C0E1",	"   Encoder Lines/Motor",		10, 0x0E1,  FH, LL));
	AddSpace(New CSpace(AN, "C0E2",	"   Force Constant",			10, 0x0E2,  FH, LL));
	AddSpace(New CSpace(AN, "C0E3",	"   Electrical Cycle Length",		10, 0x0E3,  FH, LL));
	AddSpace(New CSpace(AN, "C0E4",	"   Motor Mass",			10, 0x0E4,  FH, LL));
	AddSpace(New CSpace(AN, "C11F",	"   Total Moving Mass",			10, 0x11F,  FH, LL));
	AddSpace(New CSpace(21, "C0E5",	"   Flux Saturation",			10, 0,       7, LL));
	AddSpace(New CSpace(AN, "C0E6",	"   Maximum Linear Speed",		10, 0x0E6,  FH, LL));
	AddSpace(New CSpace(AN, "C0E8",	"   Thermal Resistance-Winding to Encoder",10, 0x0E8,  FH, LL));
	AddSpace(New CSpace(AN, "C0E7",	"   Thermal Resistance-Winding to Ambient",10, 0x0E7,  FH, LL));
	AddSpace(New CSpace(AN, "C0EA",	"   Thermal Capacitance-Winding to Encoder",10, 0x0EA,  FH, LL));
	AddSpace(New CSpace(AN, "C0E9",	"   Thermal Capacitance-Winding to Ambient",10, 0x0E9,  FH, LL));
	AddSpace(New CSpace(AN, "C0EC",	"   Rated Voltage",			10, 0x0EC,  FH, LL));
	AddSpace(New CSpace(AN, "C10D",	"   Integral Limits",			10, 0x10D,  FH, LL));
	AddSpace(New CSpace(AN, "C159",	"   Automatic Motor Identification",	10, 0x159,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"TUNING...",				10, SPTUNING,  FZ, LL));
	AddSpace(New CSpace(AN, "C011",	"   Position Loop Kp Gain",		10, 0x011,  FH, LL));
	AddSpace(New CSpace(AN, "C012",	"   Position Loop Ki Gain",		10, 0x012,  FH, LL));
	AddSpace(New CSpace(AN, "C013",	"   Position Loop Kd Gain",		10, 0x013,  FH, LL));
	AddSpace(New CSpace(AN, "C014",	"   Position Loop Kff Gain",		10, 0x014,  FH, LL));
	AddSpace(New CSpace(AN, "C015",	"   Position Loop Izone",		10, 0x015,  FH, LL));
	AddSpace(New CSpace(AN, "C01F",	"   Velocity Loop P Gain",		10, 0x01F,  FH, LL));
	AddSpace(New CSpace(AN, "C020",	"   Velocity Loop I Gain",		10, 0x020,  FH, LL));
	AddSpace(New CSpace(AN, "C021",	"   Velocity Loop D Gain",		10, 0x021,  FH, LL));
	AddSpace(New CSpace(AN, "C02D",	"   Low Pass Filter Bandwidth",		10, 0x02D,  FH, LL));
	AddSpace(New CSpace(AN, "C02E",	"   Low Pass Filter Enable/Disable",	10, 0x02E,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"ENCODER...",				10, SPENCODER,  FZ, LL));
	AddSpace(New CSpace(AN, "C02C",	"   Position Feedback Source",		10, 0x02C,  FH, LL));
	AddSpace(New CSpace(AN, "C101",	"   Encoder Ratio Motor",		10, 0x101,  FH, LL));
	AddSpace(New CSpace(AN, "C102",	"   Encoder Ratio Load",		10, 0x102,  FH, LL));
	AddSpace(New CSpace(AN, "C103",	"   Load Encoder Type",			10, 0x103,  FH, LL));
	AddSpace(New CSpace(AN, "C100",	"   Load Encoder Lines Per Revolution",	10, 0x100,  FH, LL));
	AddSpace(New CSpace(AN, "C0FF",	"   Load Encoder Lines Per Meter",	10, 0x0FF,  FH, LL));
	AddSpace(New CSpace(AN, "C0ED",	"   Motor Encoder Interpolation",	10, 0x0ED,  FH, LL));
	AddSpace(New CSpace(AN, "C0EE",	"   Encoder Output Type",		10, 0x0EE,  FH, LL));
	AddSpace(New CSpace(AN, "C0EF",	"   Maximum Encoder Output Frequency",	10, 0x0EF,  FH, LL));
	AddSpace(New CSpace(AN, "C059",	"   Encoder Output Divider",		10, 0x059,  FH, LL));
	AddSpace(New CSpace(AN, "C01C",	"   Marker Output Gating",		10, 0x01C,  FH, LL));

	AddSpace(New CSpace(AN, " ",	"DIGITAL I/O...",			10, SPDIGITALIO,  FZ, LL));
	AddSpace(New CSpace(AN, "C044",	"   Output Override",			10, 0x044,  FC, LL));
	AddSpace(New CSpace(AN, "C045",	"   Brake Active Delay",		10, 0x045,  FH, LL));
	AddSpace(New CSpace(AN, "C046",	"   Brake Inactive Delay",		10, 0x046,  FH, LL));
	AddSpace(New CSpace(22, "C069",	"   Input Functions",			10, 0,       7, LL));
	AddSpace(New CSpace(23, "C0CE",	"   Output Functions",			10, 0,       4, LL));

	AddSpace(New CSpace(AN, " ",	"ANALOG I/O...",			10, SPANALOGIO,  FZ, LL));
	AddSpace(New CSpace(AN, "C04B",	"   Output Configuration",		10, 0x04B,  FH, LL));
	AddSpace(New CSpace(AN, "C04C",	"   Output Offset",			10, 0x04C,  FH, LL));
	AddSpace(New CSpace(AN, "C04D",	"   Output Scale",			10, 0x04D,  FH, LL));
	AddSpace(New CSpace(AN, "C04E",	"   Output Override Enable/Disable",	10, 0x04E,  FD, LL));
	AddSpace(New CSpace(AN, "C04F",	"   Output Override Value",		10, 0x04F,  FD, LL));

	AddSpace(New CSpace(AN, " ",	"MONITOR...",				10, SPMONITORING,  FZ, LL));
	AddSpace(New CSpace(AN, "C073",	"   Drive Status",			10, 0x073,  FA, LL));
	AddSpace(New CSpace(AN, "C075",	"   Run Status",			10, 0x075,  FA, LL));
	AddSpace(New CSpace(AN, "C076",	"   Digital Input Status",		10, 0x076,  FA, LL));
	AddSpace(New CSpace(AN, "C077",	"   Digital Output Status",		10, 0x077,  FA, LL));
	AddSpace(New CSpace(AN, "C094",	"   Encoder Status",			10, 0x094,  FA, LL));
	AddSpace(New CSpace(AN, "C18F",	"   Encoder Temperature",		10, 0x18F,  FA, LL));
	AddSpace(New CSpace(AN, "C0AC",	"   Index Number",			10, 0x0AC,  FA, LL));
	AddSpace(New CSpace(AN, "C0AD",	"   Monitor Index Count",		10, 0x0AD,  FA, LL));
	AddSpace(New CSpace(AN, "C079",	"   Reset Peaks",			10, 0x079,  FB, LL));
	AddSpace(New CSpace(AN, "C07A",	"   Analog Command Input",		10, 0x07A,  FA, LL));
	AddSpace(New CSpace(AN, "C07C",	"   Analog Current Limit Input",	10, 0x07C,  FA, LL));
	AddSpace(New CSpace(AN, "C07D",	"   Analog Output",			10, 0x07D,  FA, LL));
	AddSpace(New CSpace(AN, "C07E",	"   Motor Position",			10, 0x07E,  FA, LL));
	AddSpace(New CSpace(AN, "C07F",	"   Auxiliary Encoder Position",	10, 0x07F,  FA, LL));
	AddSpace(New CSpace(AN, "C080",	"   Position Command",			10, 0x080,  FA, LL));
	AddSpace(New CSpace(AN, "C081",	"   Position Error",			10, 0x081,  FA, LL));
	AddSpace(New CSpace(AN, "C082",	"   Positive Peak Position Error",	10, 0x082,  FA, LL));
	AddSpace(New CSpace(AN, "C083",	"   Negative Peak Position Error",	10, 0x083,  FA, LL));
	AddSpace(New CSpace(AN, "C084",	"   Velocity Command",			10, 0x084,  FA, LL));
	AddSpace(New CSpace(AN, "C085",	"   Velocity Motor",			10, 0x085,  FA, LL));
	AddSpace(New CSpace(AN, "C086",	"   Velocity Error",			10, 0x086,  FA, LL));
	AddSpace(New CSpace(AN, "C087",	"   Current Command",			10, 0x087,  FA, LL));
	AddSpace(New CSpace(AN, "C088",	"   Average Current",			10, 0x088,  FA, LL));
	AddSpace(New CSpace(AN, "C089",	"   Positive Peak Current Command",	10, 0x089,  FA, LL));
	AddSpace(New CSpace(AN, "C08A",	"   Negative Peak Current Command",	10, 0x08A,  FA, LL));
	AddSpace(New CSpace(AN, "C08B",	"   Bus Voltage",			10, 0x08B,  FA, LL));
	AddSpace(New CSpace(AN, "C08D",	"   Current Feedback",			10, 0x08D,  FA, LL));
	AddSpace(New CSpace(AN, "C08E",	"   U-Phase Current",			10, 0x08E,  FA, LL));
	AddSpace(New CSpace(AN, "C08F",	"   W-Phase Current",			10, 0x08F,  FA, LL));
	AddSpace(New CSpace(AN, "C092",	"   Motor Temperature",			10, 0x092,  FA, LL));
	AddSpace(New CSpace(AN, "C108",	"   Drive Temperature",			10, 0x108,  FA, LL));
	AddSpace(New CSpace(AN, "C024",	"   Operating Mode",			10, 0x024,  FA, LL));

	AddSpace(New CSpace(AN, " ",	"FAULT...",				10, SPFAULT,  FZ, LL));
	AddSpace(New CSpace(AN, "C018",	"   Position Error Limit",		10, 0x018,  FH, LL));
	AddSpace(New CSpace(AN, "C019",	"   Position Error Time",		10, 0x019,  FH, LL));
	AddSpace(New CSpace(AN, "C10E",	"   Overspeed Limit",			10, 0x10E,  FH, LL));
	AddSpace(New CSpace(AN, "C10F",	"   Velocity Error Limit",		10, 0x10F,  FH, LL));
	AddSpace(New CSpace(AN, "C027",	"   Velocity Error Time",		10, 0x027,  FH, LL));
	AddSpace(New CSpace(AN, "C031",	"   User Current Limit",		10, 0x031,  FH, LL));
	AddSpace(New CSpace(AN, "C074",	"   Fault Status",			10, 0x074,  FA, LL));
	AddSpace(New CSpace(AN, "C19E",	"   Extended Fault Status",		10, 0x19E,  FA, LL));
	AddSpace(New CSpace(AN, "CEXC",	"   Latest Exception Code",		10, 0x300,  FJ, LL));
	AddSpace(New CSpace(AN, "CEXP",	"   Parameter of Latest Exception",	10, 0x301,  FJ, LL));

	AddSpace(New CSpace(AN, " ",	"DRIVE NAME...",			10, SPDRIVENAME,  FZ, LL));
	AddSpace(New CSpace(AN, "C601",	"   Drive Name - Characters   1 -  4",	10, 0x601,  FX, LL));
	AddSpace(New CSpace(AN, "C602",	"   Drive Name - Characters   5 -  8",	10, 0x602,  FX, LL));
	AddSpace(New CSpace(AN, "C603",	"   Drive Name - Characters   9 - 12",	10, 0x603,  FX, LL));
	AddSpace(New CSpace(AN, "C604",	"   Drive Name - Characters  13 - 16",	10, 0x604,  FX, LL));
	AddSpace(New CSpace(AN, "C605",	"   Drive Name - Characters  17 - 20",	10, 0x605,  FX, LL));
	AddSpace(New CSpace(AN, "C606",	"   Drive Name - Characters  21 - 24",	10, 0x606,  FX, LL));
	AddSpace(New CSpace(AN, "C607",	"   Drive Name - Characters  25 - 28",	10, 0x607,  FX, LL));
	AddSpace(New CSpace(AN, "C608",	"   Drive Name - Characters  29 - 32",	10, 0x608,  FX, LL));
	AddSpace(New CSpace(AN, "C006",	"   Send Stored Text to Drive Name",	10, 0x600,  FW, LL));

	AddSpace(New CSpace(AN, " ",	"MOTOR MODEL...",			10, SPMOTORMODEL,  FZ, LL));
	AddSpace(New CSpace(AN, "CEB1",	"   Motor Model - Characters  1 -  4",	10, 0xEB1,  FY, LL));
	AddSpace(New CSpace(AN, "CEB2",	"   Motor Model - Characters  5 -  8",	10, 0xEB2,  FY, LL));
	AddSpace(New CSpace(AN, "CEB3",	"   Motor Model - Characters  9 - 12",	10, 0xEB3,  FY, LL));
	AddSpace(New CSpace(AN, "CEB4",	"   Motor Model - Characters 13 - 16",	10, 0xEB4,  FY, LL));
	AddSpace(New CSpace(AN, "CEB5",	"   Motor Model - Characters 17 - 20",	10, 0xEB5,  FY, LL));
	AddSpace(New CSpace(AN, "CEB6",	"   Motor Model - Characters 21 - 24",	10, 0xEB6,  FY, LL));
	AddSpace(New CSpace(AN, "CEB7",	"   Motor Model - Characters 25 - 28",	10, 0xEB7,  FY, LL));
	AddSpace(New CSpace(AN, "CEB8",	"   Motor Model - Characters 29 - 32",	10, 0xEB8,  FY, LL));
	AddSpace(New CSpace(AN, "C0EB",	"   Send Stored Text to Motor Model",	10, 0xEB0,  FW, LL));
	}

// Helpers
BOOL CABUltra3Driver::Validate(CSpace *pSpace, UINT uFnc, CString * pErr)
{
	UINT uRange;

	switch( pSpace->m_uTable ) {

		case addrNamed:
			uRange = pSpace->m_uMaximum;
			break;

		case 22:
		case 23:
			uRange = FG;
			break;

		default:
			uRange = FI;
			break;
		}

	switch( uRange ) {

		case FA:
		case FJ:
			if( uFnc ) {

				*pErr += "(0)";

				return FALSE;
				}

			return TRUE;

		case FB:
		case FW:
			if( uFnc != 1 ) {

				*pErr += "(1)";

				return FALSE;
				}

			return TRUE;

		case FC:
			if( uFnc > 1 ) {

				*pErr += "(<01>)";

				return FALSE;
				}

			return TRUE;

		case FD:
			if( (uFnc > 1 && uFnc < 8) || uFnc > 9 ) {

				*pErr += "(<0189>)";

				return FALSE;
				}

			return TRUE;

		case FE:
			if( uFnc == 1 || uFnc == 4 || uFnc == 5 || uFnc >= 10 ) {

				*pErr += "(<0236789>)";

				return FALSE;
				}

			return TRUE;

		case FF:
			if( uFnc == 1 || uFnc == 4 || uFnc == 5 || uFnc == 7 || uFnc >= 10 ) {

				*pErr += "(<023689>)";

				return FALSE;
				}

			return TRUE;

		case FG:
			if( uFnc == 8 || uFnc == 9 ) {

				*pErr += "(<01234567AB>)";

				return FALSE;
				}

			return TRUE;

		case FH:
			if( uFnc >= 10 ) {

				*pErr += "(<0123456789>)";

				return FALSE;
				}

			return TRUE;

		case FI:
			return TRUE;

		case FX:
			if( uFnc > 7 ) {

				*pErr += "(<01234567>)";

				return FALSE;
				}

			return TRUE;

		case FY:
			if( uFnc == 4 || uFnc == 5 || uFnc > 7 ) {

				*pErr += "(<012367>)";

				return FALSE;
				}

			return TRUE;
		}

	*pErr = IDS_NAMED_ADDR_INVALID;

	return FALSE;
	}

CSpace * CABUltra3Driver::GetSpace(CAddress const &Addr)
{ // In Driver
	CSpaceList & List = CStdCommsDriver::GetSpaceList();

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			if( MatchSpace(pSpace, Addr) ) {

				return pSpace;
				}

			List.GetNext(n);
			}
		}

	return NULL;
	}

BOOL CABUltra3Driver::MatchSpace(CSpace * pSpace, CAddress const &Addr)
{
	UINT uTable = pSpace->m_uTable;

	if( uTable == Addr.a.m_Table ) {

		if( uTable == addrNamed ) {

			if( Addr.a.m_Offset == pSpace->m_uMinimum ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// ABUltra3000 Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CABUltra3AddrDialog, CStdAddrDialog);
		
// Constructor

CABUltra3AddrDialog::CABUltra3AddrDialog(CABUltra3Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pDriver	= &Driver;

	m_pABUDriver	= &Driver;
	
	m_pAddr		= &Addr;

	m_fPart		= fPart;

	m_pSpace	= NULL;

	SetName(fPart ? TEXT("ABUltra3PartAddressDlg") : TEXT("ABUltra3AddressDlg"));

	if( m_uAllowSpace < SPGENERAL || m_uAllowSpace > SPMOTORMODEL ) {

		m_uAllowSpace = SPMOTORMODEL;
		}
	}

// Message Map

AfxMessageMap(CABUltra3AddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CABUltra3AddrDialog)
	};

// Message Handlers

BOOL CABUltra3AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	m_pSpace = m_pABUDriver->GetSpace(*m_pAddr);
	
	if( !m_fPart ) {

		SetCaption();

		if( m_pSpace ) {

			SetAllow();
			}

		LoadList();

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			ShowAddress(Addr);

			return FALSE;
			}

		EnableWindow(0);

		return TRUE;
		}
	else {
		SetAllow();

		ShowAddress(Addr);

		return FALSE;
		}
	}

// Notification Handlers

void CABUltra3AddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CABUltra3AddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		BOOL fNamed = m_pSpace->m_uTable == addrNamed;

		EnableIndexInput();

		if( m_pSpace != pSpace ) {

			CAddress Addr;

			CString Text;

			SetAllow();

			SetRadioOptions();

			m_pSpace->GetMinimum(Addr);

			if( IsHeader(m_pSpace->m_uMinimum) ) {

				GetDlgItem(2001).SetWindowText("");
				GetDlgItem(2017).SetWindowText(m_pSpace->m_Caption);
				}

			else {
				GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
				GetDlgItem(2017).SetWindowText("");

				if( !fNamed ) {

					GetDlgItem(2002).SetWindowText("0");
					}
				}

			SetDescription2016(m_pSpace->m_uMinimum);

			Addr.a.m_Type   = LL;

			Addr.a.m_Extra  = GetRadioButton();

			Addr.a.m_Offset = fNamed ? m_pSpace->m_uMinimum : 0;

			Addr.a.m_Table  = fNamed ? addrNamed : m_pSpace->m_uTable;
			}
		}
	else {
		m_pSpace = NULL;

		EnableIndexInput();

		SetDescription2016( SPNONE );

		SetRadioOptions();

		ClearRadioButtons();
		}
	}

BOOL CABUltra3AddrDialog::OnOkay(UINT uID)
{
	CString Hex = "0123456789AB";

	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		if( m_pSpace->m_uTable != addrNamed ) {

			Text += GetDlgItem(2002).GetWindowText();
			}

		else {

			if( IsHeader(m_pSpace->m_uMinimum) ) {

				m_uAllowSpace = m_pSpace->m_uMinimum;

				LoadList();

				return TRUE;
				}
			}

		Text += "(";

		Text += Hex[GetRadioButton()];

		Text += ")";

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( m_pSpace->m_uTable == addrNamed ? 2004 + GetRadioButton() : 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overridables

void CABUltra3AddrDialog::ShowAddress(CAddress Addr)
{
	EnableIndexInput();

	SetRadioOptions();

	SetRadioButton(Addr.a.m_Extra);

	switch( Addr.a.m_Table ) {

		case addrNamed:

			SetDescription2016(m_pSpace->m_uMinimum);
			SetDlgFocus( 2004 + GetRadioButton() );
			break;

		default:
			GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
			GetDlgItem(2002).SetWindowText(m_pSpace->GetValueAsText(Addr.a.m_Offset));
			GetDlgItem(2016).SetWindowText(m_pSpace->m_Caption);
			GetDlgItem(2017).SetWindowText(" ");
			SetDlgFocus(2002);
			break;			
		}
	}

// Helpers

UINT CABUltra3AddrDialog::GetRadioButton(void)
{
	UINT uResult = 0;

	if( !IsHeader(m_pSpace->m_uMinimum) ) {

		for( UINT i = 2004; i <= 2015; i++ ) {

			CButton &Button = (CButton &) GetDlgItem(i);

			if( Button.IsChecked() ) {

				uResult = i - 2004;
				}
			}

		SetRadioButton( uResult );
		}

	else {
		ClearRadioButtons();
		}

	return uResult;
	}

void CABUltra3AddrDialog::SetRadioButton(UINT uFunction)
{
	if( !IsHeader(m_pSpace->m_uMinimum) ) {

		ValidateSetting( &uFunction );

		UINT uRange = GetFunctionCodeList();

		for( UINT i = 0; i <= 11; i++ ) {

			switch( uRange ) {

				case FC:
				case FD:
				case FG:
				case FH:
				case FI:
					if( i == 1 && uFunction == 1 ) {

						PutRadioButton( 2004, TRUE );
						}

					else {
						PutRadioButton( 2004 + i, i == uFunction );
						}

					break;

				default:
					PutRadioButton( 2004 + i, i == uFunction );

					break;
				}
			}
		}

	else {
		ClearRadioButtons();
		}
	}

void CABUltra3AddrDialog::PutRadioButton(UINT uID, BOOL fYes)
{
	CButton &Check = (CButton &) GetDlgItem(uID);

	Check.SetCheck( fYes );
	}

void CABUltra3AddrDialog::ClearRadioButtons(void)
{
	for( UINT i = 2004; i <= 2015; i++ ) {

		CButton &Check = (CButton &) GetDlgItem(i);

		Check.SetCheck(FALSE);
		}
	}

void CABUltra3AddrDialog::SetRadioOptions(void)
{
	UINT uSpecifier = GetFunctionCodeList();	

	BOOL fReadWrite = TRUE;

	switch( uSpecifier ) {

		case FA:
			EnableWindow( 1 );
			SetRadioButton(0);
			fReadWrite = FALSE;
			break;

		case FB:
		case FW:
			EnableWindow( 2 );
			SetRadioButton(1);

			if( uSpecifier == FW ) {

				GetDlgItem(2005).SetWindowText("1-Send Stored Text");

				return;
				}

			fReadWrite = FALSE;
			break;

		case FC:
			EnableWindow( 1 );
			break;

		case FD:
			EnableWindow( 0x301 );
			break;

		case FE:
			EnableWindow( 0x3CD );
			fReadWrite = FALSE;
			break;

		case FF:
			EnableWindow( 0x34D );
			fReadWrite = FALSE;
			break;

		case FG:
			EnableWindow( 0xCFD );
			break;

		case FH:
			EnableWindow( 0x3FD );
			break;

		case FI:
			EnableWindow( 0xFFD );
			break;

		case FJ:
			EnableWindow( 1 );
			GetDlgItem(2004).SetWindowText("INTERNAL-R/W Exception Data");
			return;

		case FX:
			EnableWindow( 0xFF );
			GetDlgItem(2004).SetWindowText("0-Read Working Value");
			GetDlgItem(2005).SetWindowText("INTERNAL-Read/Write Stored Text");
			return;

		case FY:
			EnableWindow( 0xCF );
			GetDlgItem(2004).SetWindowText("0-Read Working Value");
			GetDlgItem(2005).SetWindowText("INTERNAL-Read/Write Stored Text");
			return;

		case FZ:
			EnableWindow( 0 );
			fReadWrite = FALSE;
			break;
		}

	GetDlgItem(2004).SetWindowText(fReadWrite ? "0,1-Read/Write Working Value" : "0-Read Working Value");
	GetDlgItem(2005).SetWindowText("1-Write Working Value");
	}

void CABUltra3AddrDialog::EnableWindow(UINT uMask)
{
	for( UINT i = 2004; i <= 2015; i++ ) {

		if( !(uMask &  1) ) {

			CButton &Check = (CButton &) GetDlgItem(i);

			Check.SetCheck(FALSE);
			}

		GetDlgItem(i).EnableWindow( uMask & 1 );

		uMask >>= 1;
		}
	}

void CABUltra3AddrDialog::ValidateSetting(UINT * pFunction)
{
	UINT uFnc   = *pFunction;

	UINT uRange = GetFunctionCodeList();

	switch( uRange ) {

		case FA:
			uFnc = 0;
			break;

		case FB:
		case FW:
			uFnc = 1;
			break;

		case FC:
			uFnc = uFnc < 3 ? uFnc : 0;
			break;

		case FD:
			uFnc = ( (uFnc > 1 && uFnc < 8) || uFnc > 9 ) ? 0 : uFnc;
			break;

		case FE:
			uFnc = ( uFnc != 1 && uFnc != 4 && uFnc != 5 && uFnc < 10 ) ? uFnc : 0;
			break;

		case FF:
			uFnc = ( uFnc != 1 && uFnc != 4 && uFnc != 5 && uFnc != 7 && uFnc < 10 ) ? uFnc : 0;
			break;

		case FG:
			uFnc = ( uFnc != 8 && uFnc != 9 ) ? uFnc : 0;
			break;

		case FH:
			uFnc = uFnc < 10 ? uFnc : 0;
			break;

		case FI:
			break;

		case FJ:
			uFnc = 0;
			break;

		case FX:
			uFnc = uFnc <= 7 ? uFnc : 0;
			break;

		case FY:
			uFnc = ( uFnc != 4 && uFnc != 5 && uFnc <= 7 ) ? uFnc : 0;
			break;
		}

	*pFunction = uFnc;
	}

BOOL CABUltra3AddrDialog::AllowSpace(CSpace *pSpace)
{
	return SelectList( pSpace->m_uTable == addrNamed ? pSpace->m_uMinimum : pSpace->m_uTable + 0x800, m_uAllowSpace );
	}

BOOL CABUltra3AddrDialog::SelectList(UINT uCommand, UINT uAllow)
{
	if( IsHeader(uCommand) ) {

		return TRUE;
		}

	switch( uAllow ) {

		case SPGENERAL:

			switch( uCommand ) {

				case 0x000:
				case 0x001:
				case 0x002:
				case 0x179:
				case 0x003:
				case 0x004:
				case 0x016:
				case 0x017:
				case 0x022:
				case 0x023:
				case 0x025:
				case 0x02F:
				case 0x030:
				case 0x043:
				case 0x05A:
				case 0x061:
				case 0x06A:
				case 0x06B:
				case 0x06C:
				case 0x06D:
				case 0x06E:
				case 0x06F:
				case 0x193:
				case 0x194:
				case 0x801:
				case 0x802:
					return TRUE;
				}

			return FALSE;

		case SPCOMMS:

			switch( uCommand ) {

				case 0x052:
				case 0x053:
				case 0x10A:
					return TRUE;
				}

			return FALSE;

		case SPANALOGMODE:

			switch( uCommand ) {

				case 0x028:
				case 0x029:
				case 0x118:
				case 0x047:
				case 0x119:
				case 0x049:
				case 0x05D:
				case 0x05E:
				case 0x0A8:
					return TRUE;
				}

			return FALSE;

		case SPPRESETMODE:

			switch( uCommand ) {

				case 0x05F:
				case 0x060:
				case 0x0A9:
				case 0x803:
				case 0x804:
				case 0x805:
				case 0x806:
				case 0x807:
				case 0x808:
					return TRUE;
				}

			return FALSE;

		case SPFOLLOWMODE:

			switch( uCommand ) {

				case 0x0AB:
				case 0x01D:
				case 0x01E:
				case 0x809:
				case 0x80A:
					return TRUE;
				}

			return FALSE;

		case SPINDEXMODE:

			switch( uCommand ) {

				case 0x058:
				case 0x070:
				case 0x0B8:
				case 0x0B9:
				case 0x80B:
				case 0x80C:
				case 0x80D:
				case 0x80E:
				case 0x80F:
				case 0x810:
				case 0x811:
				case 0x812:
				case 0x813:
				case 0x814:
					return TRUE;
				}

			return FALSE;

		case SPOVERTRAVEL:

			switch( uCommand ) {

				case 0x0D8:
				case 0x0D9:
				case 0x0DA:
				case 0x0DB:
				case 0x0DD:
					return TRUE;
				}

			return FALSE;

		case SPHOMING:

			switch( uCommand ) {

				case 0x071:
				case 0x0C4:
				case 0x0C5:
				case 0x0C6:
				case 0x0C7:
				case 0x0C8:
				case 0x0C9:
				case 0x0CD:
				case 0x0CA:
				case 0x0CB:
				case 0x0CC:
				case 0x0D7:
				case 0x15F:
					return TRUE;
				}

			return FALSE;

		case SPMOTOR:

			switch( uCommand ) {

				case 0x037:
				case 0x038:
				case 0x039:
				case 0x03A:
				case 0x03B:
				case 0x03C:
				case 0x03D:
				case 0x03E:
				case 0x03F:
				case 0x040:
				case 0x041:
				case 0x042:
				case 0x0AA:
				case 0x095:
				case 0x0DE:
				case 0x0DF:
				case 0x0E0:
				case 0x0E1:
				case 0x0E2:
				case 0x0E3:
				case 0x0E4:
				case 0x11F:
				case 0x0E6:
				case 0x0E8:
				case 0x0E7:
				case 0x0EA:
				case 0x0E9:
				case 0x0EC:
				case 0x10D:
				case 0x159:
				case 0x815:
					return TRUE;
				}

			return FALSE;

		case SPTUNING:

			switch( uCommand ) {

				case 0x011:
				case 0x012:
				case 0x013:
				case 0x014:
				case 0x015:
				case 0x01F:
				case 0x020:
				case 0x021:
				case 0x02D:
				case 0x02E:
					return TRUE;
				}

			return FALSE;

		case SPENCODER:

			switch( uCommand ) {

				case 0x02C:
				case 0x101:
				case 0x102:
				case 0x103:
				case 0x100:
				case 0x0FF:
				case 0x0ED:
				case 0x0EE:
				case 0x0EF:
				case 0x059:
				case 0x01C:
					return TRUE;
				}

			return FALSE;

		case SPDIGITALIO:

			switch( uCommand ) {

				case 0x044:
				case 0x045:
				case 0x046:
				case 0x816:
				case 0x817:
					return TRUE;
				}

			return FALSE;

		case SPANALOGIO:

			switch( uCommand ) {

				case 0x04B:
				case 0x04C:
				case 0x04D:
				case 0x04E:
				case 0x04F:
					return TRUE;
				}

			return FALSE;

		case SPMONITORING:

			switch( uCommand ) {

				case 0x073:
				case 0x075:
				case 0x076:
				case 0x077:
				case 0x094:
				case 0x18F:
				case 0x0AC:
				case 0x0AD:
				case 0x079:
				case 0x07A:
				case 0x07C:
				case 0x07D:
				case 0x07E:
				case 0x07F:
				case 0x080:
				case 0x081:
				case 0x082:
				case 0x083:
				case 0x084:
				case 0x085:
				case 0x086:
				case 0x087:
				case 0x088:
				case 0x089:
				case 0x08A:
				case 0x08B:
				case 0x08D:
				case 0x08E:
				case 0x08F:
				case 0x092:
				case 0x108:
				case 0x024:
					return TRUE;
				}

			return FALSE;

		case SPFAULT:

			switch( uCommand ) {

				case 0x018:
				case 0x019:
				case 0x10E:
				case 0x10F:
				case 0x027:
				case 0x031:
				case 0x074:
				case 0x19E:
				case 0x300:
				case 0x301:
					return TRUE;
				}

			return FALSE;

		case SPDRIVENAME:

			return uCommand >= 0x600 && uCommand <= 0x608;

		case SPMOTORMODEL:

			return uCommand >= 0xEB0 && uCommand <= 0xEB8;
		}

	return FALSE;
	}

void CABUltra3AddrDialog::SetAllow()
{
	UINT uCommand = m_pSpace->m_uTable == addrNamed ? m_pSpace->m_uMinimum : m_pSpace->m_uTable + 0x800;

	for( UINT i = SPGENERAL; i <= SPMOTORMODEL; i++ ) {

		if( SelectList(uCommand, i) ) {

			m_uAllowSpace = i;

			return;
			}
		}
	}

void CABUltra3AddrDialog::SetDescription2016(UINT uCommand)
{
	switch( IsHeader(uCommand) ) {

		case 1:
			GetDlgItem(2001).SetWindowText(" ");
			GetDlgItem(2002).SetWindowText(" ");
			GetDlgItem(2016).SetWindowText("MENU HEADER");
			GetDlgItem(2017).SetWindowText(m_pSpace ? m_pSpace->m_Caption : " ");
			break;

		case 2:
			GetDlgItem(2001).SetWindowText(" ");
			GetDlgItem(2002).SetWindowText(" ");
			GetDlgItem(2016).SetWindowText(" ");
			GetDlgItem(2017).SetWindowText(" ");
			break;

		case 0:
		default:
			GetDlgItem(2001).SetWindowText(m_pSpace ? m_pSpace->m_Prefix  : " ");
			GetDlgItem(2016).SetWindowText(m_pSpace ? m_pSpace->m_Caption : " ");
			GetDlgItem(2017).SetWindowText(" ");
			break;
		}
	}

UINT CABUltra3AddrDialog::IsHeader(UINT uCommand)
{
	if( uCommand >= SPGENERAL && uCommand <= SPMOTORMODEL ) {

		return 1;
		}

	if( uCommand > SPMOTORMODEL ) {

		return 2;
		}

	return 0;
	}

UINT CABUltra3AddrDialog::GetFunctionCodeList(void)
{
	if( m_pSpace ) {

		switch( m_pSpace->m_uTable ) {

			case addrNamed: return m_pSpace->m_uMaximum;

			case 22:
			case 23:
				return FG;

			default:
				return FI;
			}
		}

	return FZ;
	}

BOOL CABUltra3AddrDialog::EnableIndexInput(void)
{
	BOOL f = m_pSpace && m_pSpace->m_uTable != addrNamed;

	GetDlgItem(2002).EnableWindow( f );
	GetDlgItem(2018).EnableWindow( f );
	GetDlgItem(2019).EnableWindow( f );

	if( !f ) {

		GetDlgItem(2002).SetWindowText(" ");
		GetDlgItem(2019).SetWindowText(" ");
		}

	else {
		GetDlgItem(2019).SetWindowText( "< 0 - " + m_pSpace->GetValueAsText(m_pSpace->m_uMaximum) + " >");
		}

	return f;
	}

// End of File
