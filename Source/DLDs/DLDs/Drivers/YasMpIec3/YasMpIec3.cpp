
#include "intern.hpp"

#include "YasMpIec3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP 3000 Series IEC TCP/IP Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CYaskawaMpIec3Master);

// Constructor

CYaskawaMpIec3Master::CYaskawaMpIec3Master(void)
{
	m_Ident = DRIVER_ID;
	}

// Device

CCODE MCALL CYaskawaMpIec3Master::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtxMp3 = (CCtxMp3 *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtxMp3 = new CCtxMp3;

			m_pCtx	  = m_pCtxMp3;

			m_pCtxMp3->m_IP		   = GetAddr(pData);
			m_pCtxMp3->m_uPort	   = GetWord(pData);
			m_pCtxMp3->m_bUnit	   = GetByte(pData);
			m_pCtxMp3->m_fKeep	   = GetByte(pData);
			m_pCtxMp3->m_uTime1	   = GetWord(pData);
			m_pCtxMp3->m_uTime2	   = GetWord(pData);
			m_pCtxMp3->m_uTime3	   = GetWord(pData);
			m_pCtxMp3->m_ExtendedWrite = GetByte(pData);
			m_pCtxMp3->m_Endian	   = GetByte(pData);
			
			m_pCtxMp3->m_IXHead	   = GetWord(pData);
			m_pCtxMp3->m_QXHead	   = GetWord(pData);
			m_pCtxMp3->m_IBHead	   = GetWord(pData);
			m_pCtxMp3->m_QBHead	   = GetWord(pData);
			m_pCtxMp3->m_QHead	   = GetWord(pData);
			m_pCtxMp3->m_Model	   = GetByte(pData);

			m_pCtxMp3->m_wTrans	   = 0;
			m_pCtxMp3->m_pSock	   = NULL;
			m_pCtxMp3->m_uLast	   = GetTickCount();
			m_pCtxMp3->m_fDirty	   = FALSE;

			m_pCtxMp3->m_fQHigh	   = m_pCtx->m_QHead > m_pCtx->m_IBHead;

			m_pCtxMp3->m_MemWordSwap   = GetByte(pData) ? 0 : 1;

			pDevice->SetContext(m_pCtxMp3);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pCtxMp3;

	return CCODE_SUCCESS;
	}

// Entry Points

CCODE MCALL CYaskawaMpIec3Master::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address;
	
	Address.m_Ref = GetRef(Addr);

	CCODE cc = CYaskawaMPIECMaster::Read(Address, pData, uCount);

	if( COMMS_SUCCESS(cc) ) {

		SwapMemWords(Address, pData, cc);
		}

	return cc;
	}

CCODE MCALL CYaskawaMpIec3Master::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address;
	
	Address.m_Ref = GetRef(Addr);

	SwapMemWords(Address, pData, uCount);

	return CYaskawaMPIECMaster::Write(Address, pData, uCount);
	}

// Implementation

DWORD CYaskawaMpIec3Master::GetRef(AREF Addr)
{
	if( IsModbus(Addr.a.m_Table) ) {

		return Addr.m_Ref - 1;
		}

	return Addr.m_Ref;
	}

void CYaskawaMpIec3Master::SwapMemWords(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtxMp3->m_MemWordSwap && IsMemReg(Addr.a.m_Table) && !Is64Bit(Addr.a.m_Table) ) {

		if( Addr.a.m_Type > addrWordAsWord ) {

			for( UINT u = 0; u < uCount; u++ ) {

				pData[u] = SwapWords(pData[u]);
				}
			}
		}
	}

DWORD CYaskawaMpIec3Master::SwapWords(DWORD dwData)
{
	WORD wLo = LOWORD(dwData);

	WORD wHi = HIWORD(dwData);

	return MAKELONG(wHi, wLo);	
	}

void  CYaskawaMpIec3Master::Swap64Bit(PDWORD pData)
{
	DWORD Swap[2];

	Swap[0]  = SwapWords(pData[0]);

	Swap[1]  = SwapWords(pData[1]);

	pData[0] = Swap[1];

	pData[1] = Swap[0];
	}

BOOL CYaskawaMpIec3Master::IsMemReg(UINT uTable)
{
	switch( uTable ) {

		case SP_4:
		case SP_9:
		case SP_10:
		case SP_MB:
		case SP_M32:
		case SP_M64:

			return TRUE;
		}
	
	return FALSE;
	}

BOOL CYaskawaMpIec3Master::IsModbus(UINT uTable)
{
	switch( uTable ) {

		case SP_0:
		case SP_1:
		case SP_3:
		case SP_4:
		case SP_5:
		case SP_6:
		case SP_7:
		case SP_8:
		case SP_9:
		case SP_10:

			return TRUE;
		}

	return FALSE;
	}

// Overridables

UINT CYaskawaMpIec3Master::GetModbusSpace(CAddress Addr)
{
	switch( Addr.a.m_Table ) {

		case SP_9:
		case SP_10:
		case SP_MB:
		case SP_M32:
		case SP_M64:	
			
			return SP_4;
		}

	return CYaskawaMPIECMaster::GetModbusSpace(Addr);
	}

BOOL CYaskawaMpIec3Master::Is64Bit(UINT uTable)
{
	switch( uTable ) {

		case SP_10:
		case SP_M64:
			
			return TRUE;
		}

	return CYaskawaMPIECMaster::Is64Bit(uTable);
	}

DWORD CYaskawaMpIec3Master::Convert64To32(PDWORD pData, UINT uTable)
{	
	if( m_pCtxMp3->m_MemWordSwap && IsMemReg(uTable) ) {
	
		Swap64Bit(pData);
		}

	return CYaskawaMPIECMaster::Convert64To32(pData, uTable);
	}

void CYaskawaMpIec3Master::Convert32To64(DWORD d, PDWORD pData, UINT uTable)
{
	CYaskawaMPIECMaster::Convert32To64(d, pData, uTable);

	if( m_pCtxMp3->m_MemWordSwap && IsMemReg(uTable) ) {

		Swap64Bit(pData);
		}
	}

// End of File
