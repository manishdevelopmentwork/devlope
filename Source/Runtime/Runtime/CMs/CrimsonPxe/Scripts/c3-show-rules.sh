#!/bin/sh

# c3-show-rules <interface>
#
# List the firewall rules associated with this interface.

cat /vap/opt/crimson/config/firewall/init | grep "\b$1\b"
