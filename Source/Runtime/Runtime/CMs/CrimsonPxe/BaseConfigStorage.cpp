
#include "Intern.hpp"

#include "BaseConfigStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Config Storage Object
//

// Constructor

CBaseConfigStorage::CBaseConfigStorage(IDatabase *pDbase)
{
	m_pDbase  = pDbase;

	m_pSchema = NULL;

	m_pDbase->Init();

	AfxGetObject("c3.schema-generator", 0, ISchemaGenerator, m_pSchema);

	ReadStoredData();

	StdSetRef();
}

// Destructor

CBaseConfigStorage::~CBaseConfigStorage(void)
{
	AfxRelease(m_pSchema);

	AfxRelease(m_pDbase);
}

// IUnknown

HRESULT CBaseConfigStorage::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IConfigStorage);

	StdQueryInterface(IConfigStorage);

	return E_NOINTERFACE;
}

ULONG CBaseConfigStorage::AddRef(void)
{
	StdAddRef();
}

ULONG CBaseConfigStorage::Release(void)
{
	StdRelease();
}

// IConfigStorage

bool CBaseConfigStorage::AddUpdateSink(IConfigUpdate *pUpdate)
{
	m_Updates.Append(pUpdate);

	return true;
}

bool CBaseConfigStorage::SetConfig(char cTag, CString const &Text, bool fEdit)
{
	UINT uItem = GetItem(cTag);

	if( uItem < NOTHING ) {

		CString Work = Text;

		if( Work.IsEmpty() ) {

			m_pSchema->GetDefault(cTag, Work);
		}

		if( m_Config[uItem].CompareC(Work) ) {

			if( WriteStoredData(uItem, Work, fEdit, true) ) {

				m_Config[uItem] = Work;

				m_Edited[uItem] = fEdit;

				for( UINT n = 0; n < m_Updates.GetCount(); n++ ) {

					m_Updates[n]->OnConfigUpdate(cTag);
				}

				m_pDbase->SetValid(TRUE);

				return true;
			}
		}
	}

	return false;
}

bool CBaseConfigStorage::SetEdited(char cTag, bool fEdit)
{
	UINT uItem = GetItem(cTag);

	if( uItem < NOTHING ) {

		if( m_Edited[uItem] != fEdit ) {

			if( WriteStoredData(uItem, m_Config[uItem], fEdit, true) ) {

				m_Edited[uItem] = fEdit;

				m_pDbase->SetValid(TRUE);

				return true;
			}

			return false;
		}

		return true;
	}

	return false;
}

bool CBaseConfigStorage::GetConfig(char cTag, CString &Text)
{
	UINT uItem = GetItem(cTag);

	if( uItem < NOTHING ) {

		Text = m_Config[uItem];

		return true;
	}

	AfxAssert(FALSE);

	return false;
}

bool CBaseConfigStorage::IsEdited(char cTag)
{
	UINT uItem = GetItem(cTag);

	if( uItem < NOTHING ) {

		return m_Edited[uItem];
	}

	AfxAssert(FALSE);

	return false;
}

bool CBaseConfigStorage::ClearData(void)
{
	m_pDbase->Clear();

	return true;
}

// Implementation

bool CBaseConfigStorage::ReadStoredData(void)
{
	bool fOkay = false;

	if( m_pDbase->IsValid() ) {

		fOkay = true;

		for( UINT uItem = 0; uItem < elements(m_Config); uItem++ ) {

			CItemInfo Info;

			if( m_pDbase->GetItemInfo(uItem, Info) ) {

				if( Info.m_uClass == 200 + uItem ) {

					bool   fEdit = false;

					UINT   uSize = Info.m_uSize;

					PCBYTE pData = PCBYTE(m_pDbase->LockItem(uItem));

					switch( *pData ) {

						case 0xFE:
						{
							fEdit = false;
							pData++;
							uSize--;
						}
						break;

						case 0xFF:
						{
							fEdit = true;
							pData++;
							uSize--;
						}
						break;
					}

					CString Text(PCSTR(pData), uSize);

					m_pDbase->FreeItem(uItem);

					m_Config[uItem] = Text;

					m_Edited[uItem] = fEdit;

					continue;
				}
			}

			fOkay = false;
		}
	}

	if( !fOkay ) {

		m_pDbase->Clear();

		for( UINT uItem = 0; uItem < elements(m_Config); uItem++ ) {

			char cTag = GetTag(uItem);

			m_pSchema->GetDefault(cTag, m_Config[uItem]);

			m_Edited[uItem] = false;
		}

		WriteStoredData();

		m_pDbase->SetValid(TRUE);
	}

	return true;
}

bool CBaseConfigStorage::WriteStoredData(void)
{
	for( UINT uItem = 0; uItem < elements(m_Config); uItem++ ) {

		if( !WriteStoredData(uItem, m_Config[uItem], m_Edited[uItem], false) ) {

			return true;
		}
	}

	return false;
}

bool CBaseConfigStorage::WriteStoredData(UINT uItem, CString const &Text, bool fEdit, bool fTidy)
{
	UINT uSize = 1 + Text.GetLength();

	CAutoArray<BYTE> Data(uSize);

	Data[0] = fEdit ? 0xFF : 0xFE;

	memcpy(PBYTE(Data) + 1, PCSTR(Text), uSize - 1);

	CItemInfo Info;

	Info.m_uItem  = uItem;
	Info.m_uClass = 200 + uItem;
	Info.m_uSize  = uSize;
	Info.m_uComp  = uSize;
	Info.m_CRC    = 0;

	if( m_pDbase->WriteItem(Info, Data) ) {

		return true;
	}

	if( fTidy ) {

		if( m_pDbase->GarbageCollect() ) {

			if( m_pDbase->WriteItem(Info, Data) ) {

				return true;
			}
		}

		m_pDbase->Clear();

		WriteStoredData();

		m_pDbase->SetValid(TRUE);

		return true;
	}

	return false;
}

char CBaseConfigStorage::GetTag(UINT uItem)
{
	switch( uItem ) {

		case 0: return 'h';
		case 1: return 'p';
		case 2: return 's';
		case 3: return 'u';
	}

	AfxAssert(FALSE);

	return 0;
}

UINT CBaseConfigStorage::GetItem(char cTag)
{
	switch( cTag ) {

		case 'h': return 0;
		case 'p': return 1;
		case 's': return 2;
		case 'u': return 3;
	}

	AfxAssert(FALSE);

	return NOTHING;
}

// End of File
