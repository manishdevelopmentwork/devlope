
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "galilser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Base Class
//

#include "../YUnivSMC/yunivsmc.cpp"

//////////////////////////////////////////////////////////////////////////
//
// Galil Serial Driver : YaskawaUnivSMC Driver
//

// Instantiator

INSTANTIATE(CGalilSerialDriver);

// Constructor

CGalilSerialDriver::CGalilSerialDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// End of File
