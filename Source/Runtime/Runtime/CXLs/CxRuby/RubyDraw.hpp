
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_Ruby_Draw_HPP
	
#define	INCLUDE_Ruby_Draw_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CRubyPoint;
class CRubyVector;
class CRubyPath;

//////////////////////////////////////////////////////////////////////////
//
// Drawing Object
//

class DLLAPI CRubyDraw
{
	public:
		// Drawing Methods
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r, number scale);
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, number scale);
		static void ArcSeg   (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r, number scale);
		static void ArcSeg   (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, number scale);
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, number t1, number t2, CRubyVector const &r, number scale);
		static void Bezier   (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyPoint const &p3, CRubyPoint const &p4);
		static void Ellipse  (CRubyPath &output, CRubyPoint const &cp, CRubyVector const &r, number scale);
		static void Circle   (CRubyPath &output, CRubyPoint const &cp, number r, number scale);
		static void Rectangle(CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2);
		static void Trimmed  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int style, int skip);
		static void Trimmed  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int style, int skip);
		static void Line     (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2);

		// Drawing Variants
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r);
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r);
		static void ArcSeg   (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, number r);
		static void ArcSeg   (CRubyPath &output, CRubyPoint const &cp, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r);
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, number t1, number t2, number r);
		static void Arc      (CRubyPath &output, CRubyPoint const &cp, number t1, number t2, CRubyVector const &r);
		static void Ellipse  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2);
		static void Ellipse  (CRubyPath &output, CRubyPoint const &cp, CRubyVector const &r);
		static void Circle   (CRubyPath &output, CRubyPoint const &cp, number r);
		static void Beveled  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int skip);
		static void Beveled  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int skip);
		static void Rounded  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int skip);
		static void Rounded  (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int skip);
		static void Filleted (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, number r, int skip);
		static void Filleted (CRubyPath &output, CRubyPoint const &p1, CRubyPoint const &p2, CRubyVector const &r, int skip);

	protected:
		// Implementation
		static int GetThetaStep(CRubyVector const &r);
	};

// End of File

#endif