
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SECURE_HPP

#define INCLUDE_SECURE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPasswordDialog;
class CSecurityDialog;

//////////////////////////////////////////////////////////////////////////
//
// Password Entry Dialog
//

class CPasswordDialog : public CStdDialog
{
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CPasswordDialog(BOOL fRead);
		
		// Attributes
		CString GetData(void) const;
		
	protected:
		// Data Members
		BOOL    m_fRead;
		CString m_Data;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnReadOnly(UINT uID);

		// Notifcation Handlers
		void OnEditChange(UINT uID, CWnd &Wnd);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Security Dialog
//

class CSecurityDialog : public CStdDialog
{
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CSecurityDialog(UINT Locked, CString Password, CString Email);
		
		// Attributes
		UINT    GetLocked(void) const;
		CString GetPassword(void) const;
		CString GetEmail(void) const;
		
	protected:
		// Data Members
		UINT    m_Locked;
		CString m_Password;
		CString m_Email;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);

		// Notification Handlers
		void OnSelChange(UINT uID, CWnd &Wnd);

		// Implementation
		void DoEnables(void);
	};	

// End of File

#endif
