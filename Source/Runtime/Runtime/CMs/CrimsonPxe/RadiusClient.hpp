
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RadiusClient_HPP

#define	INCLUDE_RadiusClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

class CRadiusFrame;

//////////////////////////////////////////////////////////////////////////
//
// Radius Client Service
//

class CRadiusClient
{
public:
	// Results
	enum
	{
		accessAllowed = 1,
		accessDenied  = 2,
		accessFailed  = 3
	};

	// Constructor
	CRadiusClient(CJsonConfig *pJson);

	// Destructor
	~CRadiusClient(void);

	// Operations
	UINT GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info);

protected:
	// Data Members
	CJsonConfig * m_pJson;

	// Implementation
	UINT CheckUser(UINT n, CString const &User, CAuthInfo const &Auth, CUserInfo &Info);
	bool DecodeAccessTag(CString const &tag, CUserInfo &Info);
};

// End of File

#endif
