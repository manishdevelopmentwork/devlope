
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LibG Global Data
//

extern "C"
{
	int __dso_handle = 0;
}

//////////////////////////////////////////////////////////////////////////
//
// LibG Implementation
//

clink void __cxa_pure_virtual(void)
{
	// TODO -- This is actually a fatal exit for this
	// thread and maybe more as the stack is left in
	// a horrible state... !!!

	HostBreak();
}

clink void __cxa_throw_bad_array_new_length(void)
{
	HostBreak();
}

clink void __cxa_bad_typeid(void)
{
	HostBreak();
}

clink void __cxa_bad_cast(void)
{
	HostBreak();
}

clink void __aeabi_atexit(void)
{
}

namespace __gnu_cxx
{
	void __verbose_terminate_handler()
	{
		::abort();
	}
}

namespace std
{
	void abort(void)
	{
		::abort();
	}
}

// End of File
