
#include "Intern.hpp"

#include "IntlStringList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IntlString.hpp"

//////////////////////////////////////////////////////////////////////////
//
// International String List
//

// Dynamic Class

AfxImplementDynamicClass(CIntlStringList, CItemList);

// Constructor

CIntlStringList::CIntlStringList(void)
{
	m_Class = AfxRuntimeClass(CIntlString);
	}

// Item Access

CIntlString * CIntlStringList::GetItem(UINT uPos)
{
	return (CIntlString *) CItemList::GetItem(uPos);
	}

CIntlString * CIntlStringList::GetItem(INDEX Index)
{
	return (CIntlString *) CItemList::GetItem(Index);
	}

// End of File
