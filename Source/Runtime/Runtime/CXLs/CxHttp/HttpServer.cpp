
#include "Intern.hpp"

#include "HttpServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServerManager.hpp"

#include "HttpServerConnection.hpp"

#include "HttpServerRequest.hpp"

#include "HttpServerSession.hpp"

#include "HttpServerBasicAuth.hpp"

#include "HttpServerDigestAuth.hpp"

#include "HttpServerNtlmAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Options
//

// Constructor

CHttpServerOptions::CHttpServerOptions(void)
{
	m_uAuthMethod   = methodForm;

	m_uHttpMethod   = httpBasic;

	m_Realm	        = "realm";

	m_uHttpRedirect = 80;

	m_uSockCount    = 10;

	#ifdef _DEBUG

	m_uIdleTimeout = 1 * 20 * 1000;

	m_uInitTimeout = 1 * 10 * 1000;

	m_uSessTimeout = 1 * 30 * 1000;

	#else

	m_uIdleTimeout = 1 * 60 * 1000;

	m_uInitTimeout = 1 * 30 * 1000;

	m_uSessTimeout = 5 * 60 * 1000;

	#endif
}

// Initialization

void CHttpServerOptions::Load(PCBYTE &pData)
{
	CHttpServerConnectionOptions::Load(pData);

	m_uAuthMethod   = GetByte(pData);

	m_uHttpMethod   = GetByte(pData);

	m_Realm	        = UniConvert(GetWide(pData));

	m_uHttpRedirect = GetByte(pData) ? 80 : 0;

	m_uSockCount    = GetByte(pData);

	m_uIdleTimeout  = GetWord(pData) * 1000;

	m_uInitTimeout  = GetWord(pData) * 1000;

	m_uSessTimeout  = GetWord(pData) * 1000 * 60;
}

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server
//

// Constructor

CHttpServer::CHttpServer(CHttpServerManager *pManager, CHttpServerOptions const &Opts) : m_Opts(Opts)
{
	StdSetRef();

	m_pManager = pManager;

	if( Opts.m_uAuthMethod == methodHttp ) {

		m_pMethod[0] = !(Opts.m_uHttpMethod & httpBasic) ? NULL : New CHttpServerBasicAuth(m_Opts.m_Realm, m_Opts.m_uSockCount * 2);

		m_pMethod[1] = !(Opts.m_uHttpMethod & httpDigest) ? NULL : New CHttpServerDigestAuth(m_Opts.m_Realm, m_Opts.m_uSockCount * 4);

		m_pMethod[2] = !(Opts.m_uHttpMethod & httpNTLM) ? NULL : New CHttpServerNtlmAuth(m_Opts.m_Realm, m_Opts.m_uSockCount * 4);
	}
	else {
		m_pMethod[0] = NULL;

		m_pMethod[1] = NULL;

		m_pMethod[2] = NULL;
	}

	m_pInfo = New CConnectionInfo[m_Opts.m_uSockCount];

	m_pHead = NULL;

	m_pTail	= NULL;

	m_uPoll = 0;

	m_SessionKey = "Session";

	AddDiagCmds();
}

// Destructor

CHttpServer::~CHttpServer(void)
{
	for( UINT m = 0; m < elements(m_pMethod); m++ ) {

		if( m_pMethod[m] ) {

			delete m_pMethod[m];
		}
	}

	delete[] m_pInfo;
}

// Attributes

BOOL CHttpServer::IsIdle(void) const
{
	if( !m_pHead ) {

		for( UINT n = 0; n < m_Opts.m_uSockCount; n++ ) {

			CConnectionInfo &Info  = m_pInfo[n];

			if( Info.m_pCon && Info.m_pCon->GetSockState() == PHASE_OPEN ) {

				return FALSE;
			}
		}

		return TRUE;
	}

	return FALSE;
}

UINT CHttpServer::GetSockCount(void) const
{
	return m_Opts.m_uSockCount;
}

// Operations

BOOL CHttpServer::Service(void)
{
	BOOL fBusy = FALSE;

	for( UINT n = 0; n < m_Opts.m_uSockCount; n++ ) {

		CConnectionInfo    &Info  = m_pInfo[n];

		CHttpServerRequest *pReq  = NULL;

		CHttpServerSession *pSess = NULL;

		CString		    User;

		if( !Info.m_pCon ) {

			GuardThread(TRUE);

			UINT uPort = 0;

			BOOL fTls  = FALSE;

			if( UseForRedirect(n, uPort, fTls) ) {

				Info.m_pCon   = m_pManager->CreateRedirect(this, m_Opts, uPort, fTls);

				Info.m_pDefer = NULL;

				Info.m_pCon->BindAuth(m_pMethod, elements(m_pMethod));

				Info.m_fRedirect = TRUE;
			}
			else {
				Info.m_pCon   = m_pManager->CreateConnection(this, m_Opts);

				Info.m_pDefer = NULL;

				Info.m_pCon->BindAuth(m_pMethod, elements(m_pMethod));

				Info.m_fRedirect = FALSE;
			}

			GuardThread(FALSE);

			Info.n = n;
		}

		if( Info.m_pDefer ) {

			Info.m_pCon->Idle();

			if( Info.m_pCon->GetSockState() == PHASE_OPEN ) {

				if( GetTickCount() - Info.m_uDefer >= 500 ) {

					FindSession(pSess, Info, Info.m_pDefer);

					ServePage(pSess, Info, Info.m_pDefer);

					if( Info.m_pDefer->GetStatus() == 999 ) {

						Info.m_uDefer = GetTickCount();

						continue;
					}

					Info.m_uTime = GetTickCount();

					Info.m_uReqs++;

					Info.m_pCon->Send();

					Info.m_pDefer = NULL;
				}

				continue;
			}

			Info.m_pCon->KillRequest();

			Info.m_pDefer = NULL;
		}

		switch( Info.m_pCon->Recv(pReq) ) {

			case httpRecvNone:
			{
				Info.m_uTime = 0;

				Info.m_uReqs = 0;
			}
			break;

			case httpRecvIdle:
			{
				if( !Info.m_uTime ) {

					Info.m_uTime = GetTickCount();
				}

				if( GetTickCount() - Info.m_uTime > ToTicks(Info.m_uReqs ? m_Opts.m_uIdleTimeout : m_Opts.m_uInitTimeout) ) {

					Info.m_pCon->Close();

					Info.m_uTime  = 0;
				}
			}
			break;

			case httpRecvMore:
			{
				Info.m_uTime = GetTickCount();

				fBusy        = TRUE;
			}
			break;

			case httpRecvDone:
			{
				if( Info.m_fRedirect ) {

					Info.m_pCon->ClearKeepAlive();

					CString Full = pReq->GetRequestHeader("Host");

					CString Host = Full.Left(Full.FindRev(':'));

					CPrintf Port = CPrintf((m_Opts.m_uPort == 443) ? "" : ":%u", m_Opts.m_uPort);

					CString Path = pReq->GetFullPath();

					pReq->AddReplyHeader("Location", "https://" + Host + Port + Path);

					pReq->SetStatus(302);

					Info.m_uReqs++;

					Info.m_pCon->Send();
				}
				else {
					FindSession(pSess, Info, pReq);

					if( false ) {

						AfxTrace("Sess [%p:%s] : %p : %s %s\n",
							 pSess,
							 pSess->GetOpaquePtr(),
							 Info.m_pCon,
							 pReq->GetVerb().data(),
							 pReq->GetFullPath().data()
						);
					}

					switch( Info.m_pCon->GetAuthStatus() ) {

						case CHttpServerConnection::authNew:
						{
							User = Info.m_pCon->GetUser();

							pSess->SetUser(User);

							pSess->SetReal(FindReal(User));
						}

						// Fall Thru

						case CHttpServerConnection::authSame:
						{
							if( m_Opts.m_uAuthMethod == methodHttp ) {

								if( !SkipAuth(pReq) ) {

									if( !Info.m_pCon->CheckPass(FindPass(pSess->GetUser())) ) {

										Info.m_pCon->Send401();

										break;
									}
								}
							}

							ServePage(pSess, Info, pReq);

							if( pReq->GetStatus() == 999 ) {

								Info.m_pDefer = pReq;

								Info.m_uDefer = GetTickCount();
							}
							else {
								Info.m_uReqs++;

								Info.m_pCon->Send();
							}
						}
						break;

						case CHttpServerConnection::authStale:
						{
							Info.m_pCon->Send401();
						}
						break;

						case CHttpServerConnection::authFail:
						{
							Info.m_pCon->Send401();
						}
						break;
					}
				}

				Info.m_uTime = GetTickCount();

				fBusy        = TRUE;
			}
			break;
		}
	}

	if( ++m_uPoll > 100 ) {

		DiscardOldSessions();

		m_uPoll = 0;
	}

	return fBusy;
}

BOOL CHttpServer::AcceptPost(IHttpStreamWrite * &pStm, CHttpServerConnection *pCon, CHttpServerRequest *pReq, PCTXT pPath)
{
	return CatchPost(pStm, pCon, pReq, pPath);
}

void CHttpServer::Disconnect(void)
{
	for( UINT n = 0; n < m_Opts.m_uSockCount; n++ ) {

		Disconnect(n);
	}
}

void CHttpServer::Disconnect(UINT uSock)
{
	if( uSock < m_Opts.m_uSockCount ) {

		CConnectionInfo &Info = m_pInfo[uSock];

		if( Info.m_pCon ) {

			Info.m_pCon->Abort();

			delete Info.m_pCon;

			Info.m_pCon = NULL;
		}
	}
}

void CHttpServer::Term(void)
{
	Disconnect();

	DiscardAllSessions();
}

// IUnknown

HRESULT CHttpServer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
}

ULONG CHttpServer::AddRef(void)
{
	StdAddRef();
}

ULONG CHttpServer::Release(void)
{
	StdRelease();
}

// IDiagProvider

UINT CHttpServer::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagConnections(pOut, pCmd);

		case 2:
			return DiagSessions(pOut, pCmd);
	}

	return 0;
}

// Overridables

CString CHttpServer::FindPass(CString User)
{
	return "password3";
}

CString CHttpServer::FindReal(CString User)
{
	return "User Name";
}

BOOL CHttpServer::SkipAuth(CHttpServerRequest *pReq)
{
	return FALSE;
}

void CHttpServer::ServePage(CHttpServerSession *pSess, CConnectionInfo &Info, CHttpServerRequest *pReq)
{
	if( pReq->GetVerb() == "GET" ) {

		CString Name  = AdjustPath(pReq->GetPath());

		#if 0

		HANDLE  hFile = CreateFile("HTML" + Name,
					   GENERIC_READ,
					   FILE_SHARE_READ,
					   NULL,
					   OPEN_EXISTING,
					   0,
					   NULL
		);

		if( hFile != INVALID_HANDLE_VALUE ) {

			DWORD dwSize = GetFileSize(hFile, NULL);

			DWORD dwRead = 0;

			CByteArray Bytes;

			Bytes.SetCount(dwSize);

			ReadFile(hFile,
				 PBYTE(Bytes.GetPointer()),
				 dwSize,
				 &dwRead,
				 NULL
			);

			CloseHandle(hFile);

			if( Name.Right(5) == ".html" ) {

				pReq->SetContentType("text/html");
			}
			else
				pReq->SetContentType("images/png");

			pReq->SetStatus(200);

			pReq->SetReplyBody(Bytes);

			return;
		}

		#endif

		pReq->SetStatus(404);

		return;
	}

	pReq->SetStatus(405);
}

BOOL CHttpServer::CatchPost(IHttpStreamWrite * &pStm, CHttpServerConnection *pCon, CHttpServerRequest *pReq, PCTXT pPath)
{
	return FALSE;
}

void CHttpServer::MakeSession(CHttpServerSession * &pSess)
{
	pSess = New CHttpServerSession(this);
}

void CHttpServer::KillSession(CHttpServerSession *pSess)
{
	delete pSess;
}

BOOL CHttpServer::UseForRedirect(UINT n, UINT &uPort, BOOL &fTls)
{
	if( m_Opts.m_fTls ) {

		if( m_Opts.m_uHttpRedirect ) {

			if( n == 0 ) {

				uPort = m_Opts.m_uHttpRedirect;

				fTls  = FALSE;

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Implementation

void CHttpServer::FindSession(CHttpServerSession * &pSess, CConnectionInfo &Info, CHttpServerRequest *pReq)
{
	FindSession(pSess, Info.m_pCon, pReq);
}

void CHttpServer::FindSession(CHttpServerSession * &pSess, CHttpServerConnection *pCon, CHttpServerRequest *pReq)
{
	if( !pCon->GetSession(pSess) ) {

		if( false ) {

			AfxTrace("Conn %p has no session\n", pCon);
		}

		for( UINT n = 0;; n++ ) {

			CString Cookie = pReq->GetRequestHeader("Cookie", n);

			if( Cookie.GetLength() ) {

				while( Cookie.GetLength() ) {

					CString Name = Cookie.StripToken('=');

					CString Data = Cookie.StripToken(';', '\"');

					Name.TrimBoth();

					Data.TrimBoth();

					if( Data[0] == '"' ) {

						Data = Data.Mid(1, Data.GetLength() - 2);
					}

					if( Name == m_SessionKey ) {

						PTXT p = NULL;

						pSess  = (CHttpServerSession *) strtoul(PCTXT(Data), &p, 16);

						if( !m_Tree.Failed(m_Tree.Find(pSess)) ) {

							if( *p == '-' ) {

								if( pSess->CheckOpaque(p+1) ) {

									if( false ) {

										AfxTrace("Conn %p bound to existing [%p:%s]\n",
											 pCon,
											 pSess, pSess->GetOpaquePtr()
										);
									}

									pCon->BindSession(pSess);

									return;
								}
							}
						}

						if( false ) {

							AfxTrace("Conn %p requsted bad session [%p]\n", pSess);
						}
					}

					Cookie.TrimBoth();
				}

				continue;
			}

			break;
		}

		MakeSession(pSess);

		AfxListAppend(m_pHead, m_pTail, pSess, m_pNext, m_pPrev);

		m_Tree.Insert(pSess);

		pReq->SetCookieHeader(CPrintf("%s=\"%8.8x-%s\"; HttpOnly; Path=/",
					      PCTXT(m_SessionKey),
					      pSess,
					      PCTXT(pSess->GetOpaque())
		));

		if( false ) {

			AfxTrace("Conn %p bound to new [%p:%s]\n",
				 pCon,
				 pSess,
				 pSess->GetOpaquePtr()
			);
		}

		pCon->BindSession(pSess);
	}
}

void CHttpServer::EndSession(CHttpServerSession *pFind, UINT uActive)
{
	for( UINT n = 0; n < m_Opts.m_uSockCount; n++ ) {

		CConnectionInfo     &Info = m_pInfo[n];

		CHttpServerSession *pSess = NULL;

		if( Info.m_pCon ) {

			Info.m_pCon->GetSession(pSess);

			if( pSess == pFind ) {

				if( Info.n == uActive ) {

					Info.m_pCon->ClearSessionAuth();

					Info.m_pCon->ClearKeepAlive();
				}
				else
					Info.m_pCon->Close();
			}
		}
	}
}

void CHttpServer::DiscardAllSessions(void)
{
	CHttpServerSession *pSess = m_pHead;

	while( pSess ) {

		CHttpServerSession *pNext = pSess->m_pNext;

		AfxListRemove(m_pHead, m_pTail, pSess, m_pNext, m_pPrev);

		KillSession(pSess);

		pSess = pNext;
	}
}

void CHttpServer::DiscardOldSessions(void)
{
	CHttpServerSession *pSess = m_pHead;

	while( pSess ) {

		CHttpServerSession *pNext = pSess->m_pNext;

		if( pSess->IsDiscardable(m_Opts.m_uSessTimeout) ) {

			m_Tree.Remove(pSess);

			AfxListRemove(m_pHead, m_pTail, pSess, m_pNext, m_pPrev);

			KillSession(pSess);
		}
		else {
			pSess->TickleSession();
		}

		pSess = pNext;
	}
}

CString CHttpServer::AdjustPath(CString Path)
{
	if( Path == "/" ) {

		Path = "/default.html";
	}

	if( isdigit(Path.Right(1)[0]) ) {

		UINT n = Path.GetLength();

		Path   = Path.Left(n - 1);
	}

	Path.Replace('/', '\\');

	return Path;
}

// Diagnostics

bool CHttpServer::AddDiagCmds(void)
{
/*	TODO -- Add revoke method!!!

	IDiagManager *pDiag = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, pDiag);

	if( pDiag ) {

		UINT uProv = pDiag->RegisterProvider(this, "w3s");

		pDiag->RegisterCommand(uProv, 1, "connections");

		pDiag->RegisterCommand(uProv, 2, "sessions");

		return true;
		}

*/	return false;
}

UINT CHttpServer::DiagConnections(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddTable(7);

		pOut->SetColumn(0, "Index", "%u");
		pOut->SetColumn(1, "State", "%u");
		pOut->SetColumn(2, "Peer", "%s");
		pOut->SetColumn(3, "Auth", "%u");
		pOut->SetColumn(4, "Session", "%8.8X");
		pOut->SetColumn(5, "Reqs", "%u");
		pOut->SetColumn(6, "Age", "%u");

		pOut->AddHead();

		pOut->AddRule('-');

		for( UINT n = 0; n < m_Opts.m_uSockCount; n++ ) {

			CConnectionInfo &Info = m_pInfo[n];

			pOut->AddRow();

			CHttpServerSession *pSess = NULL;

			Info.m_pCon->GetSession(pSess);

			pOut->SetData(0, n);
			pOut->SetData(1, PCTXT(Info.m_pCon->GetSockState()));
			pOut->SetData(2, PCTXT(Info.m_pCon->GetSockPeer()));
			pOut->SetData(3, Info.m_pCon->GetAuthStatus());
			pOut->SetData(4, pSess);
			pOut->SetData(5, Info.m_uReqs);
			pOut->SetData(6, Info.m_uTime ? ToTime(GetTickCount() - Info.m_uTime) / 1000 : 0);

			pOut->EndRow();
		}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

UINT CHttpServer::DiagSessions(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		CHttpServerSession *pSess = m_pHead;

		if( pSess ) {

			pOut->AddTable(pSess->GetDiagColCount());

			pSess->GetDiagCols(pOut);

			pOut->AddHead();

			pOut->AddRule('-');

			while( pSess ) {

				pOut->AddRow();

				pSess->GetDiagInfo(pOut);

				pOut->EndRow();

				pSess = pSess->m_pNext;
			}

			pOut->AddRule('-');

			pOut->EndTable();

			return 0;
		}

		pOut->Print("No Sessions\n");

		return 0;
	}

	pOut->Error(NULL);

	return 1;
}

// End of File
