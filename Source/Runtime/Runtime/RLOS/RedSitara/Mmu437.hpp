
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Mmu437_HPP
	
#define	INCLUDE_AM437_Mmu437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Memory Management Unit
//

class CMmu437 : public IMmu
{
	public:
		// Constructor
		CMmu437(void);

		// IMmu
		bool  IsVirtualValid(DWORD dwAddr, bool fWrite);
		DWORD PhysicalToVirtual(DWORD dwAddr, bool fCached);
		DWORD VirtualToPhysical(DWORD dwAddr);
		void  DebugRegister(void);
		void  DebugRevoke(void);

	protected:
		// Data Members
		PDWORD m_pTable;
		UINT   m_uStep;
		DWORD  m_dwMask;

		// Implementation
		bool IsEntryValid(DWORD dwEntry);
		bool IsEntryCached(DWORD dwEntry);
		bool FindGranularity(void);
		UINT Gcd64(UINT64 a, UINT64 b);
		UINT Gcd32(UINT a, UINT b);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Implementation

STRONG_INLINE bool CMmu437::IsEntryValid(DWORD dwEntry)
{
	return (dwEntry & 0x00080003) == 0x00000002;
	}

STRONG_INLINE bool CMmu437::IsEntryCached(DWORD dwEntry)
{
	if( dwEntry & (1 << 14) ) {

		return (dwEntry & 0x0000300C) ? true : false;
		}

	return (dwEntry & 0x0000000C) ? true : false;
	}

// End of File

#endif
