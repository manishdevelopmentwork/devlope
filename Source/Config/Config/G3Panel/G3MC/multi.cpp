
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Module Multi View Window
//

// Runtime Class

AfxImplementRuntimeClass(CModuleMultiViewWnd, CMultiViewWnd);

// Constructor

CModuleMultiViewWnd::CModuleMultiViewWnd(void)
{
	m_fRecycle = TRUE;

	m_nMargin  = 4;

	SetTabStyle(0);
	}

// Overridables

CString CModuleMultiViewWnd::OnGetNavPos(void)
{
	CString Item = m_Views[m_uFocus]->GetNavPos();

	CString Left = Item.StripToken(':');

	CString Page = CPrintf(L"%u", m_uFocus);

	CString Nav  = Left + L":" + Page + L":" + Item;

	return Nav;
	}

BOOL CModuleMultiViewWnd::OnNavigate(CString const &Nav)
{
	UINT uSep = Nav.Count(':');

	if( uSep == 2 ) {

		CString Item = Nav;

		CString Left = Item.StripToken(':');

		CString Page = Item.StripToken(':');

		CString Rest = Left + L":" + Item;

		if( !Page.IsEmpty() ) {

			UINT uPage = watoi(Page);

			if( uPage < m_Views.GetCount() ) {

				SelectFocus(uPage);

				if( m_Views[uPage]->Navigate(Rest) ) {

					if( !IsCurrent() ) {

						SetFocus();
						}

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( uSep == 1 ) {

		for( UINT uPage = 0; uPage < m_Views.GetCount(); uPage++ ) {

			CreateView(uPage);

			if( m_Views[uPage]->Navigate(Nav) ) {

				SelectFocus(uPage);

				if( !IsCurrent() ) {

					SetFocus();
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CModuleMultiViewWnd::OnExec(CCmd *pCmd)
{
	m_Views[m_uFocus]->ExecCmd(pCmd);
	}

void CModuleMultiViewWnd::OnUndo(CCmd *pCmd)
{
	m_Views[m_uFocus]->UndoCmd(pCmd);
	}

// End of File
