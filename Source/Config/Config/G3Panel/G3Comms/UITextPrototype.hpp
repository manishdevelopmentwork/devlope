
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextPrototype_HPP

#define INCLUDE_UITextPrototype_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramPrototype;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Program Prototype
//

class CUITextPrototype : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextPrototype(void);

	protected:
		// Data Members
		CProgramPrototype * m_pPrototype;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnExpand(CWnd &Wnd);

		// Implementation
		void FindPrototype(void);
	};

// End of File

#endif
