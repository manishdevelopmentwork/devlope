
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Resource Window
//

class CEt3DeviceResTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructors
		CEt3DeviceResTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		CEt3CommsManager * m_pComms;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnPostCreate(void);

		// Notification Handlers
		void OnTreeSelChanged(UINT uID, NMTREEVIEW &Info);

		// Tree Loading
		void LoadTree(void);
		void LoadImageList(void);		

		// Selection Hooks
		CString SaveSelState(void);
		void    LoadSelState(CString State);

		// Implementation
		void LoadRoot(void);
		void LoadPorts  (CEt3CommsPortList *pPorts);
		void LoadDevices(CEt3CommsDeviceList *pDevices);
		void LoadDevice (HTREEITEM hRoot, CEt3CommsDevice *pDevice);
		void LoadBlocks (HTREEITEM hRoot, CTransferBlockList *pBlocks);
		void LoadBlock  (HTREEITEM hRoot, CTransferBlockItem *pBlock);
	};

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CEt3DeviceResTreeWnd, CStdTreeWnd);

// Constructors

CEt3DeviceResTreeWnd::CEt3DeviceResTreeWnd(void)
{
	m_dwStyle = m_dwStyle | TVS_HASBUTTONS;

	m_Accel1.Create(L"ResTreeAccel");
	}

// IUnknown

HRESULT CEt3DeviceResTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
	}

ULONG CEt3DeviceResTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
	}

ULONG CEt3DeviceResTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
	}

// IUpdate

HRESULT CEt3DeviceResTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsDeviceList)) ) {

			RefreshTree();
			}

		if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsPort)) ) {

			RefreshTree();
			}

		if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

			RefreshTree();
			}
		}

	if( pItem->IsKindOf(AfxRuntimeClass(CEt3CommsDevice)) ) {

		// LATER -- Could be a little more clever here...

		if( uType == updateRename ) {

			RefreshTree();
			}
		}

	return S_OK;
	}

// Overridables

void CEt3DeviceResTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pComms    = (CEt3CommsManager *) m_pItem;
	}

// Message Map

AfxMessageMap(CEt3DeviceResTreeWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)

	AfxDispatchNotify(100, TVN_SELCHANGED, OnTreeSelChanged)

	AfxMessageEnd(CEt3DeviceResTreeWnd)
	};

// Message Handlers

void CEt3DeviceResTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
	}

// Notification Handlers

void CEt3DeviceResTreeWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			if( Info.action == TVC_BYMOUSE ) {
				
				SetFocus();
				}

			m_hSelect = Info.itemNew.hItem;

			m_pSelect = NULL;

			NewItemSelected();
			}
		}
	}

// Tree Loading

void CEt3DeviceResTreeWnd::LoadTree(void)
{
	LoadRoot();

	CEt3CommsPortList *pPorts;

	for( UINT s = 0; m_pComms->GetPortList(pPorts, s); s++ ) {

		LoadPorts(pPorts);
		}

	ExpandItem(m_hRoot);

	m_pTree->SelectItem(m_hRoot);
	}

void CEt3DeviceResTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"Et3Tree16"), afxColor(MAGENTA));
	}

// Selection Hooks

CString CEt3DeviceResTreeWnd::SaveSelState(void)
{
	// LATER -- Is there a way to implement this?

	return L"";
	}

void CEt3DeviceResTreeWnd::LoadSelState(CString State)
{
	SelectRoot();
	}

// Implementation

void CEt3DeviceResTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;
	
	Node.SetText(CString(IDS_DEVICES));

	Node.SetParam(NULL);

	Node.SetImages(IDI_COMMS);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);
	}

void CEt3DeviceResTreeWnd::LoadPorts(CEt3CommsPortList *pPorts)
{
	INDEX n = pPorts->GetHead();

	while( !pPorts->Failed(n) ) {

		CEt3CommsPort       *pPort = pPorts->GetItem(n);

		CEt3CommsDeviceList *pDevs = pPort->m_pDevices;

		LoadDevices(pDevs);

		pPorts->GetNext(n);
		}
	}

void CEt3DeviceResTreeWnd::LoadDevices(CEt3CommsDeviceList *pDevices)
{
	INDEX n = pDevices->GetHead();

	while( !pDevices->Failed(n) ) {

		CEt3CommsDevice *pDevice = pDevices->GetItem(n);

		LoadDevice(m_hRoot, pDevice);

		pDevices->GetNext(n);
		}
	}

void CEt3DeviceResTreeWnd::LoadDevice(HTREEITEM hRoot, CEt3CommsDevice *pDevice)
{
	if( TRUE ) {

		CTreeViewItem Node;

		Node.SetText(pDevice->m_Name);

		Node.SetParam(NULL);

		Node.SetImage(pDevice->GetTreeImage());

		HTREEITEM hNode = m_pTree->InsertItem(hRoot, NULL, Node);

		afxThread->SetWaitMode(TRUE);

		LoadBlocks(hNode, pDevice->m_pBlocks);

		afxThread->SetWaitMode(FALSE);
		}
	}

void CEt3DeviceResTreeWnd::LoadBlocks(HTREEITEM hRoot, CTransferBlockList *pBlocks)
{
	INDEX n = pBlocks->GetHead();

	while( !pBlocks->Failed(n) ) {

		CTransferBlockItem *pBlock = pBlocks->GetItem(n);

		LoadBlock(hRoot, pBlock);

		pBlocks->GetNext(n);
		}
	}

void CEt3DeviceResTreeWnd::LoadBlock(HTREEITEM hRoot, CTransferBlockItem *pBlock)
{
	if( TRUE ) {

		CTreeViewItem Node;

		Node.SetText(pBlock->m_Name);

		Node.SetParam(NULL);

		Node.SetImage(pBlock->GetTreeImage());		

		m_pTree->InsertItem(hRoot, NULL, Node);
		}
	}

// End of File
