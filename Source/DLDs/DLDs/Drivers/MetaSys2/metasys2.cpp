#include "intern.hpp"

#include "metasys2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver
//

// Instantiator

INSTANTIATE(CMetaSysN2SystemDriver2);

// Constructor

CMetaSysN2SystemDriver2::CMetaSysN2SystemDriver2(void)
{
	m_Ident = DRIVER_ID;
       	}

void CMetaSysN2SystemDriver2::HandleWriteSingle(UINT uRegion)
{
	UINT uLook = FindLookup(uRegion);

	UINT uPtr = 4;

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = GetChar2(uPtr);

	CAddress Addr;
	
	Addr.a.m_Table  = Table[uLook];

	Addr.a.m_Type   = Type[uLook];

	if( uLook == 2 || uLook == 3 ) {

		Addr.a.m_Type = addrBitAsBit;
		}

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = uObj;

	PDWORD pData = new DWORD[1];

	if( !SetData(Addr, uLook, uAtr, pData, 1, uPtr) ) {

		// Always send a response !!
		}
	
	delete pData;

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver2::HandleReadSingle(UINT uRegion)
{
	UINT uLook = FindLookup(uRegion);

	UINT uPtr = 4;

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = GetChar2(uPtr);

	FindAttribute(uLook, uAtr);

	CAddress Addr;
	
	Addr.a.m_Table  = Table[uLook];

	Addr.a.m_Type   = Type[uLook];

	if( uLook == 2 || uLook == 3 ) {

		Addr.a.m_Type = addrBitAsBit;
		}

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = uObj;

	UINT uCount = FindCount(uLook, uAtr);
		
	PDWORD pData = new DWORD[uCount];

	memset(PBYTE(pData), 0, uCount * sizeof(DWORD));
	
	if( COMMS_SUCCESS(Read(Addr, pData, uCount)) ) {

		// Always send a response !!
		}

	SendData(Addr.a.m_Type, uLook, uAtr, pData, uCount);
	
	delete pData;
	}

void CMetaSysN2SystemDriver2::HandleWriteMulti(void)
{
	UINT uPtr = 4;

	UINT uRegion = GetChar2(uPtr);
       
	UINT uLook = FindLookup(uRegion);

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = 1;
	
	CAddress Addr;
	
	Addr.a.m_Table  = Table[uLook];

	Addr.a.m_Type   = Type[uLook];

	if( uLook == 2 || uLook == 3 ) {

		Addr.a.m_Type = addrBitAsBit;
		}

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = uObj;

	UINT uCount = Attrib[uLook];

	PDWORD pData = new DWORD[uCount];

	if( !SetData(Addr, uLook, uAtr, pData, uCount, uPtr) ) {

		// Always send a response !!
		}

	delete pData;

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysN2SystemDriver2::HandleReadMulti(void)
{
	UINT uPtr = 4;
	
	UINT uRegion = GetChar2(uPtr);

	UINT uLook = FindLookup(uRegion);

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = 1;
	
	CAddress Addr;

	Addr.a.m_Table	= Table[uLook];

	Addr.a.m_Type   = Type[uLook];
	
	if( uLook == 2 || uLook == 3 ) {

		Addr.a.m_Type = addrBitAsBit;
		}

	Addr.a.m_Extra  = 0;

	Addr.a.m_Offset = uObj;

	UINT uCount = Attrib[uLook];
	
	PDWORD pData = new DWORD[uCount];

	memset(PBYTE(pData), 0, uCount * sizeof(DWORD));

	UINT uSuccess = 0;
	
	if( (uSuccess = Read(Addr, pData, uCount)) ) {

		// Always send a response !!

		}
	
	SendData(Addr.a.m_Type, uLook, uAtr, pData, uCount);
	
	delete pData;
	}

BOOL CMetaSysN2SystemDriver2::SetAnalogs(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook)
{	
	// Current Value is read only

	return TRUE;
	}

BOOL CMetaSysN2SystemDriver2::SetBinarys(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos, UINT uLook)
{
	// Current Value is read only

	return TRUE;
	}

BOOL CMetaSysN2SystemDriver2::SetInternals(CAddress Addr, UINT uAtr, PDWORD pData, UINT uCount, UINT uPos)
{
	UINT uBytes = m_Ptr - 2 - 7;
	
	switch( uBytes ) {

		case 2:	pData[0] = GetChar2(uPos);	break;
		case 4: pData[0] = GetChar4(uPos);	break;
		case 8: pData[0] = GetChar8(uPos);	break;
		}

	return Set(Addr, pData, 1);
	}

void CMetaSysN2SystemDriver2::SendAnalogs(UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr <= 2 ) {

			PutChar2(0);

			continue;
			}

		if( uAtr == 3 ) {

			PutChar8(pData[0]);
			}
		else {
			PutChar8(0);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver2::SendBinaryInputs(UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr < 2 ) {

			PutChar2(0);
			}
		
		else if( uAtr == 2 ) {

			PutChar2(pData[0]);
			}
		
		else if( uAtr == 3 ) {

			PutChar4(0);
			}
		else {		   
			PutChar8(0);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver2::SendBinaryOutputs(UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr < 2 ) {

			PutChar2(0);
			}
		
		else if( uAtr == 2 ) {

			PutChar2(pData[0]);
			}
		else {		
			PutChar4(0);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysN2SystemDriver2::SendInternals(UINT uType, UINT uAtr, PDWORD pData, UINT uCount)
{
	Start('A');

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr == 1 ) {

			PutChar2(0);

			continue;
			}
		   
		switch( uType ) {

			case addrByteAsByte: PutChar2(pData[0]);	break;
			case addrWordAsWord: PutChar4(pData[0]);	break;
			case addrLongAsLong: PutChar8(pData[0]);	break;
			case addrRealAsReal: PutChar8(pData[0]);	break;
			}
		}
		
	AddCheck();

	Tx();
	}

// End of File

