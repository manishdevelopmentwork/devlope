#include "intern.hpp"

#include "mitqtcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q TCP Master Driver
//

// Instantiator

INSTANTIATE(CMitQTCPMaster);

// Constructor

CMitQTCPMaster::CMitQTCPMaster(void)
{
	m_Ident = DRIVER_ID;
	
	}

// Destructor

CMitQTCPMaster::~CMitQTCPMaster(void)
{
	}

// Implementation

BOOL CMitQTCPMaster::RecvFrame(UINT uTotal)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	uTotal = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 8 ) {

				if( (m_bRxBuff[0] == 0xD0) && 
				    (m_bRxBuff[2] == m_bTxBuff[2]) &&	
				    (m_bRxBuff[3] == m_bTxBuff[3]) ) {

					uTotal = m_bRxBuff[7];

					if ( uPtr >= uTotal + 9 ) {

						memcpy(m_bRxBuff, m_bRxBuff + 9, uTotal);

						return TRUE;
						}
					}
				else {	
					return FALSE;
					}
				}
					
			if( uPtr -= uTotal ) {

				for( UINT n = 0; n < uPtr; n++ ) {
				
					m_bRxBuff[n] = m_bRxBuff[uTotal++];
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CMitQTCPMaster::CheckFrame(void)
{
	if( (m_bRxBuff[0] == 0x00) && (m_bRxBuff[1] == 0x00) ) {
	
		return TRUE;
		}

	return FALSE;
       	}

BOOL CMitQTCPMaster::Start(void)
{
	if( CMitTCPMasterDriver::Start() ) {

		AddWord(0x0050);

		return TRUE;
		}

	return FALSE;
	}

void CMitQTCPMaster::AddID(void)
{
	AddByte(m_pCtx->m_bNet);

	AddByte(m_pCtx->m_bPlc);
	}

void CMitQTCPMaster::AddCommand(UINT uType, UINT uCommand, UINT uCount)
{
	AddCpu();

	AddByte(0x00);

	UINT uSize = 12;
	
	if( uCommand == CMD_WRITE ) {

		switch( uType ) {

			case addrBitAsBit:

				uSize += uCount / 2 + uCount % 2; 
				break;

			case addrBitAsWord:
			case addrWordAsWord:

				uSize += uCount * 2;
				break;

			case addrWordAsLong:
			case addrWordAsReal:

				uSize += uCount * 4;
				break;
			}
		}

	AddWord(uSize);	   

	AddWord(m_pCtx->m_uMonitor);

	AddWord(uCommand);  

	AddWord(GetSubCommand(uType));
	}

void CMitQTCPMaster::AddAddress(AREF Addr)
{
	UINT uOffset = Addr.a.m_Offset + (65536 * Addr.a.m_Extra);

	AddOffset(uOffset);
	
	AddBinaryPrefix(Addr.a.m_Table);

	}

void CMitQTCPMaster::AddOffset(UINT uOffset)
{
	if( m_pBase->m_fCode ) {

		AddHex(uOffset, 0x10000000);

		return;
		} 
	
	AddWord(LOWORD(uOffset));

	AddByte(LOBYTE(HIWORD(uOffset)));
	}

WORD CMitQTCPMaster::GetCommand(UINT uType, BOOL fWrite)
{
	if( fWrite ) {

		return CMD_WRITE;
		}

	return CMD_READ;
	}

// End of File
