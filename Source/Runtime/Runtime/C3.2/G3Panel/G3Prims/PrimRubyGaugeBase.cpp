
#include "intern.hpp"

#include "PrimRubyGaugeBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Base Gauge Primitive
//

// Constructor

CPrimRubyGaugeBase::CPrimRubyGaugeBase(void)
{
	m_pointCenter.m_x = 100;
	m_pointCenter.m_y = 100;
	m_pValue          = NULL;
	m_pMin            = NULL;
	m_pMax            = NULL;
	m_pMajorColor     = New CPrimColor(naFeature);
	m_pMinorColor     = New CPrimColor(naFeature);
	m_Major           = 4;
	m_Minor           = 5;
	m_PointMode       = 0;
	m_pPointColor     = New CPrimColor(naFeature);
	m_BandShow1       = 0;
	m_pBandColor1     = New CPrimColor(naFeature);
	m_pBandMin1       = NULL;
	m_pBandMax1       = NULL;
	m_BandShow2       = 0;
	m_pBandColor2     = New CPrimColor(naFeature);
	m_pBandMin2       = NULL;
	m_pBandMax2       = NULL;
	m_BugShow1        = 0;
	m_pBugColor1      = New CPrimColor(naFeature);
	m_pBugValue1      = NULL;
	m_BugShow2        = 0;
	m_pBugColor2      = New CPrimColor(naFeature);
	m_pBugValue2      = NULL;
	m_uChange	  = 4;
	}

// Destructor

CPrimRubyGaugeBase::~CPrimRubyGaugeBase(void)
{
	delete m_pValue;
	delete m_pMin;
	delete m_pMax;
	delete m_pMajorColor;
	delete m_pMinorColor;
	delete m_pPointColor;
	delete m_pBandColor1;
	delete m_pBandMin1;
	delete m_pBandMax1;
	delete m_pBandColor2;
	delete m_pBandMin2;
	delete m_pBandMax2;
	delete m_pBugColor1;
	delete m_pBugValue1;
	delete m_pBugColor2;
	delete m_pBugValue2;
	}

// Initialization

void CPrimRubyGaugeBase::Load(PCBYTE &pData)
{
	CPrimRuby::Load(pData);

	GetCoded(pData, m_pValue);
	GetCoded(pData, m_pMin);
	GetCoded(pData, m_pMax);
	
	m_pMajorColor->Load(pData);
	m_pMinorColor->Load(pData);
	
	m_Major     = GetByte(pData);
	m_Minor     = GetByte(pData);
	m_PointMode = GetByte(pData);

	m_pPointColor->Load(pData);

	if( (m_BandShow1 = GetByte(pData)) ) {

		m_pBandColor1->Load(pData);

		GetCoded(pData, m_pBandMin1);
		GetCoded(pData, m_pBandMax1);
		}

	if( (m_BandShow2 = GetByte(pData)) ) {

		m_pBandColor2->Load(pData);

		GetCoded(pData, m_pBandMin2);
		GetCoded(pData, m_pBandMax2);
		}

	if( (m_BugShow1 = GetByte(pData)) ) {

		m_pBugColor1->Load(pData);

		GetCoded(pData, m_pBugValue1);
		}

	if( (m_BugShow2 = GetByte(pData)) ) {

		m_pBugColor2->Load(pData);

		GetCoded(pData, m_pBugValue2);
		}

	LoadMatrix(pData, m_m1);

	LoadNumber(pData, m_scale);

	LoadPoint (pData, m_pointCenter);

	m_m2 = m_m1;
	}

// Overridables

void CPrimRubyGaugeBase::SetScan(UINT Code)
{
	SetItemScan(m_pValue,     Code);
	SetItemScan(m_pMin,       Code);
	SetItemScan(m_pMax,       Code);
	SetItemScan(m_pBandMin1,  Code);
	SetItemScan(m_pBandMax1,  Code);
	SetItemScan(m_pBandMin2,  Code);
	SetItemScan(m_pBandMax2,  Code);
	SetItemScan(m_pBugValue1, Code);
	SetItemScan(m_pBugValue2, Code);
	
	m_pMajorColor->SetScan(Code);
	m_pMinorColor->SetScan(Code);
	m_pPointColor->SetScan(Code);
	m_pBandColor1->SetScan(Code);
	m_pBandColor2->SetScan(Code);
	m_pBugColor1 ->SetScan(Code);
	m_pBugColor2 ->SetScan(Code);
	}

void CPrimRubyGaugeBase::MovePrim(int cx, int cy)
{
	m_m1.AddTranslation(cx, cy);

	CPrimRuby::MovePrim(cx, cy);
	}

// Drawing

void CPrimRubyGaugeBase::TestScaleEtc(void)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_fChange = TRUE;

		MakeMax(m_uChange, 3);

		m_Ctx = Ctx;
		}
	}

void CPrimRubyGaugeBase::DrawScaleEtc(IGDI *pGDI)
{
	CRubyGdiLink gdi(pGDI);

	InitPaths();

	MakePaths();

	MakeLists();

	gdi.OutputSolid(m_listBand1, m_Ctx.m_BandColor1, 0);
	gdi.OutputSolid(m_listBand2, m_Ctx.m_BandColor2, 0);
	gdi.OutputSolid(m_listMajor, m_Ctx.m_MajorColor, 0);
	gdi.OutputSolid(m_listMinor, m_Ctx.m_MinorColor, 0);
	gdi.OutputSolid(m_listBug1,  m_Ctx.m_BugColor1,  0);
	gdi.OutputSolid(m_listBug2,  m_Ctx.m_BugColor2,  0);
	}

// Path Management

void CPrimRubyGaugeBase::InitPaths(void)
{
	m_pathMajor.Empty();
	m_pathMinor.Empty();
	m_pathBand1.Empty();
	m_pathBand2.Empty();
	m_pathBug1 .Empty();
	m_pathBug2 .Empty();
	}

void CPrimRubyGaugeBase::MakePaths(void)
{	
	MakeScaleEtc();
	}

void CPrimRubyGaugeBase::MakeLists(void)
{
	m_pathMajor.Transform(m_m2);
	m_pathMinor.Transform(m_m2);
	m_pathBand1.Transform(m_m2);
	m_pathBand2.Transform(m_m2);
	m_pathBug1 .Transform(m_m2);
	m_pathBug2 .Transform(m_m2);

	m_listMajor.Load(m_pathMajor,  true);
	m_listMinor.Load(m_pathMinor,  true);
	m_listBand1.Load(m_pathBand1,  true);
	m_listBand2.Load(m_pathBand2,  true);
	m_listBug1 .Load(m_pathBug1,   true);
	m_listBug2 .Load(m_pathBug2,   true);
	}

// Scale Building

void CPrimRubyGaugeBase::MakeScaleEtc(void)
{
	}

// Scaling

number CPrimRubyGaugeBase::GetValue(CCodedItem *pValue, C3REAL Default)
{
	if( pValue ) {
		
		return I2R(pValue->Execute(typeReal));
		}

	return Default;
	}

// Context Creation

void CPrimRubyGaugeBase::FindCtx(CCtx &Ctx)
{
	Ctx.m_Min        = GetValue(m_pMin,        0);
	Ctx.m_Max        = GetValue(m_pMax,      100);
	Ctx.m_BandMin1   = GetValue(m_pBandMin1,  75);
	Ctx.m_BandMax1   = GetValue(m_pBandMax1, 100);
	Ctx.m_BandMin2   = GetValue(m_pBandMin2,  75);
	Ctx.m_BandMax2   = GetValue(m_pBandMax2, 100);
	Ctx.m_BugValue1  = GetValue(m_pBugValue1, 50);
	Ctx.m_BugValue2  = GetValue(m_pBugValue2, 50);

	Ctx.m_MajorColor = m_pMajorColor->GetColor();
	Ctx.m_MinorColor = m_pMinorColor->GetColor();
	Ctx.m_BandColor1 = m_pBandColor1->GetColor();
	Ctx.m_BandColor2 = m_pBandColor2->GetColor();
	Ctx.m_BugColor1  = m_pBugColor1 ->GetColor();
	Ctx.m_BugColor2  = m_pBugColor2 ->GetColor();
	}

// Context Check

BOOL CPrimRubyGaugeBase::CCtx::operator == (CCtx const &That) const
{
	// TODO -- Avoid de minimis changes?

	return m_Min        == That.m_Min        &&
	       m_Max        == That.m_Max        &&
	       m_BandMin1   == That.m_BandMin1   &&
	       m_BandMax1   == That.m_BandMax1   &&
	       m_BandMin2   == That.m_BandMin2   &&
	       m_BandMax2   == That.m_BandMax2   &&
	       m_BugValue1  == That.m_BugValue1  &&
	       m_BugValue2  == That.m_BugValue2  &&
	       m_MajorColor == That.m_MajorColor &&
	       m_MinorColor == That.m_MinorColor &&
	       m_BandColor1 == That.m_BandColor1 &&
	       m_BandColor2 == That.m_BandColor2 &&
	       m_BugColor1  == That.m_BugColor1  &&
	       m_BugColor2  == That.m_BugColor2  ;
	}

// Shaders

BOOL CPrimRubyGaugeBase::ShaderDark(IGdi *pGdi, int p, int c)
{
	static DWORD q = 0;

	if( c ) {

		COLOR a = GetRGB(0,0,0);

		COLOR b = GetRGB(20,20,20);

		DWORD k = Mix32(a, b, p, c);

		if( k - q ) {

			pGdi->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = 0;

	return FALSE;
	}

BOOL CPrimRubyGaugeBase::ShaderChrome(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0, 250, 300, 400, 500, 900, 1000 };

	static int col[] = { 0,  10,   2,  31,  29,  16,   31 };

	static DWORD q  = 0;

	static UINT  n  = 0;
	
	if( c ) {

		int pc = (c - p) * 1000 / c;

		if( n ) {
			
			if( pc <= pos[n] ) {
			
				n--;
				}
			}

		COLOR a = GetRGB(col[n+0], col[n+0], col[n+0]);

		COLOR b = GetRGB(col[n+1], col[n+1], col[n+1]);

		DWORD k = Mix32(a, b, pc - pos[n], pos[n+1] - pos[n]);

		if( k - q ) {

			pGdi->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = 0;

	n = elements(pos) - 1;

	return FALSE;
	}

DWORD CPrimRubyGaugeBase::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	ra = (ra << 3) | (3 & (ra >> 2));
	ga = (ga << 3) | (3 & (ga >> 2));
	ba = (ba << 3) | (3 & (ba >> 2));

	if( c && p ) {

		int rb = GetRED  (b);
		int gb = GetGREEN(b);
		int bb = GetBLUE (b);

		rb = (rb << 3) | (3 & (rb >> 2));
		gb = (gb << 3) | (3 & (gb >> 2));
		bb = (bb << 3) | (3 & (bb >> 2));

		if( p < c ) {

			int rc = ra + ((rb - ra) * p / c);
			int gc = ga + ((gb - ga) * p / c);
			int bc = ba + ((bb - ba) * p / c);

			return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
			}

		return MAKELONG(MAKEWORD(bb, gb), MAKEWORD(rb, 0xFF));
		}

	return MAKELONG(MAKEWORD(ba, ga), MAKEWORD(ra, 0xFF));
	}

// End of File
