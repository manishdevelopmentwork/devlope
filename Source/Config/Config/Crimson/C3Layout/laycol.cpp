
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Column
//

// Constructor

CLayFormCol::CLayFormCol(void)
{
	}

// Overridables

void CLayFormCol::OnPrepare(CDC &DC)
{
	ClearData();

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		pItem->Prepare(DC);

		CSize Min = pItem->GetMinSize();

		CSize Max = pItem->GetMaxSize();

		m_MinSize.cx = Max(m_MinSize.cx, Min.cx);

		m_MinSize.cy = m_MinSize.cy + Min.cy;

		m_MaxSize.cx = Max(m_MaxSize.cx, Max.cx);

		m_MaxSize.cy = m_MaxSize.cy + Max.cy;
		}
	}

void CLayFormCol::OnSetRect(void)
{
	int nSpace = m_Rect.GetHeight();

	SetMins(nSpace);
	
	while( DealOut(nSpace) );

	CPoint Pos = m_Rect.GetTopLeft();

	CSize Size = m_Rect.GetSize();

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		Size.cy = m_Size[n];

		pItem->SetRect(CRect(Pos, Size));

		Pos.y += m_Size[n];
		}
	}

// Implementation

void CLayFormCol::ClearData(void)
{
	m_MinSize = CSize();

	m_MaxSize = CSize();

	m_Size.Empty();
	}

void CLayFormCol::SetMins(int &nSpace)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		int nMin = pItem->GetMinSize().cy;

		m_Size.Append(nMin);

		nSpace -= nMin;
		}
	}

BOOL CLayFormCol::DealOut(int &nSpace)
{
	int nEdit = 0;

	if( nSpace > 0 ) {

		int nItem = 0;

		int nEach = 0;

		for( UINT p = 0; p < 2; p++ ) {

			for( UINT n = 0; n < m_List.GetCount(); n++ ) {

				CLayItem *pItem = m_List[n];

				int nSize = m_Size[n];

				int nMax  = pItem->GetMaxSize().cy;

				if( nSize < nMax ) {

					if( p == 0 ) {

						nItem++;
						}
					else {
						int nDeal = Min(nEach, nMax - nSize);

						nSize  += nDeal;

						nSpace -= nDeal;

						m_Size.SetAt(n, nSize);

						nEdit++;
						}
					}
				}

			if( p == 0 ) {

				if( nItem && (nEach = nSpace / nItem) ) {

					continue;
					}

				break;
				}
			}
		}

	return nEdit ? TRUE : FALSE;
	}

// End of File
