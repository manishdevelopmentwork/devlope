
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SSDFIRE_HPP
	
#define	INCLUDE_SSDFIRE_HPP

//////////////////////////////////////////////////////////////////////////
//
// SSD Drives via FireWire
//

class CSSDFireDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSSDFireDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
