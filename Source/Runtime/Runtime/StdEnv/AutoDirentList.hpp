
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoDirentList_HPP

#define	INCLUDE_AutoDirentList_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Directory Entry List
//

class DLLAPI CAutoDirentList
{
public:
	// Constructors

	STRONG_INLINE CAutoDirentList(void)
	{
		m_pList = NULL;

		m_uSize = 0;
	}

	// Destructor

	STRONG_INLINE ~CAutoDirentList(void)
	{
		Empty();
	}

	// Attributes

	STRONG_INLINE UINT GetCount(void) const
	{
		return m_pList ? m_uSize : 0;
	}

	// Operations

	STRONG_INLINE void Empty(void)
	{
		if( m_pList ) {

			for( UINT n = 0; n < m_uSize; n++ ) {

				free(m_pList[n]);
			}

			free(m_pList);

			m_pList = NULL;
		}
	}

	STRONG_INLINE void SetCount(UINT uSize)
	{
		m_uSize = uSize;
	}

	STRONG_INLINE bool Scan(char const *name, int (*selector)(struct dirent const *), int (*compare)(struct dirent const **, struct dirent const **))
	{
		AfxAssert(!m_pList);

		m_uSize = ScanDir(name, selector, compare);

		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE bool ScanFiles(char const *name, int (*compare)(struct dirent const **, struct dirent const **))
	{
		AfxAssert(!m_pList);

		m_uSize = ScanDir(name, SelectFiles, compare);

		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE bool ScanDirs(char const *name, int (*compare)(struct dirent const **, struct dirent const **))
	{
		AfxAssert(!m_pList);

		m_uSize = ScanDir(name, SelectDirs, compare);

		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE bool Scan(char const *name)
	{
		AfxAssert(!m_pList);

		m_uSize = ScanDir(name, NULL, StdSort);

		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE bool ScanFiles(char const *name)
	{
		AfxAssert(!m_pList);

		m_uSize = ScanDir(name, SelectFiles, StdSort);

		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE bool ScanByTimeFwd(char const *name)
	{
		AfxAssert(!m_pList);

		if( (m_uSize = ScanDir(name, NULL, NULL)) && m_pList ) {

			SortByTimeFwd(name);

			return true;
		}

		return false;
	}

	STRONG_INLINE bool ScanByTimeRev(char const *name)
	{
		AfxAssert(!m_pList);

		if( (m_uSize = ScanDir(name, NULL, NULL)) && m_pList ) {

			SortByTimeRev(name);

			return true;
		}

		return false;
	}

	STRONG_INLINE bool ScanFilesByTimeFwd(char const *name)
	{
		AfxAssert(!m_pList);

		if( (m_uSize = ScanDir(name, SelectFiles, NULL)) && m_pList ) {

			SortByTimeFwd(name);

			return true;
		}

		return false;
	}

	STRONG_INLINE bool ScanFilesByTimeRev(char const *name)
	{
		AfxAssert(!m_pList);

		if( (m_uSize = ScanDir(name, SelectFiles, NULL)) && m_pList ) {

			SortByTimeRev(name);

			return true;
		}

		return false;
	}

	STRONG_INLINE bool ScanDirsByTimeFwd(char const *name)
	{
		AfxAssert(!m_pList);

		if( (m_uSize = ScanDir(name, SelectDirs, NULL)) && m_pList ) {

			SortByTimeFwd(name);

			return true;
		}

		return false;
	}

	STRONG_INLINE bool ScanDirsByTimeRev(char const *name)
	{
		AfxAssert(!m_pList);

		if( (m_uSize = ScanDir(name, SelectDirs, NULL)) && m_pList ) {

			SortByTimeRev(name);

			return true;
		}

		return false;
	}

	STRONG_INLINE bool ScanDirs(char const *name)
	{
		AfxAssert(!m_pList);

		m_uSize = ScanDir(name, SelectDirs, StdSort);

		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE bool SortByTimeFwd(char const *name)
	{
		return SortByTime(name, FwdTimeSort);
	}

	STRONG_INLINE bool SortByTimeRev(char const *name)
	{
		return SortByTime(name, RevTimeSort);
	}

	bool AddFileTimes(char const *name);

	bool SortByTime(char const *name, int (*compare)(struct dirent const **, struct dirent const **));

	int  stat(char const *name, UINT n, struct stat *s);

	// Conversion

	STRONG_INLINE operator bool(void) const
	{
		return (m_pList && m_uSize) ? true : false;
	}

	STRONG_INLINE operator dirent ** & (void)
	{
		return m_pList;
	}

	// Operators

	STRONG_INLINE bool operator ! (void) const
	{
		return (m_pList && m_uSize) ? false : true;
	}

	// Sorting

	static int StdSort(struct dirent const **p1, struct dirent const **p2);

	static int RevSort(struct dirent const **p1, struct dirent const **p2);

	static int FwdTimeSort(struct dirent const **p1, struct dirent const **p2);

	static int RevTimeSort(struct dirent const **p1, struct dirent const **p2);

	// Selection

	static int SelectFiles(struct dirent const *p);

	static int SelectDirs(struct dirent const *p);

protected:
	// Data Members
	dirent ** m_pList;
	UINT      m_uSize;

	// Implementation

	STRONG_INLINE UINT ScanDir(char const *name, int (*selector)(struct dirent const *), int (*compare)(struct dirent const **, struct dirent const **))
	{
		int n;

		if( (n = scandir(name, &m_pList, selector, compare)) < 0 ) {

			return 0;
		}

		return UINT(n);
	}

private:
	// No Assign or Copy

	void operator = (CAutoDirentList const &That) const {};

	CAutoDirentList(CAutoDirentList const &That) {};
};

// End of File

#endif
