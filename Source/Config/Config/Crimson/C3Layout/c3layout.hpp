
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3LAYOUT_HPP
	
#define	INCLUDE_C3LAYOUT_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcwin.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "c3layout.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_C3LAYOUT

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "c3layout.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CLayItem;
class CLayFormation;
class CLayFormCol;
class CLayFormRow;
class CLayFormGrid;
class CLayFormEqual;
class CLayFormPad;
class CLayItemText;
class CLayItemSize;

//////////////////////////////////////////////////////////////////////////
//
// Layout Flags
//

enum LayoutFlags
{
	horzNone	= 0x000,
	horzLeft	= 0x001,
	horzCenter	= 0x002,
	horzRight	= 0x004,
	horzTest	= 0x00F,

	vertNone	= 0x000,
	vertTop		= 0x010,
	vertCenter	= 0x020,
	vertBottom	= 0x040,
	vertTest	= 0x0F0,

	horzGrow	= 0x100,
	vertGrow	= 0x200,

	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Item
//

class DLLAPI CLayItem
{
	public:
		// Constructors
		CLayItem(void);
		CLayItem(BOOL fEmpty);

		// Destructor
		virtual ~CLayItem(void);

		// Attributes
		CSize GetMinSize(void) const;
		CSize GetMaxSize(void) const;
		CRect GetRect(void) const;
		
		// Operations
		void SetRect(CRect const &Rect);
		void Prepare(CDC &DC);
		void SetOffset(CSize const &Offset);

	protected:
		// Data Members
		CSize  m_MinSize;
		CSize  m_MaxSize;
		CRect  m_Rect;
		CSize  m_Offset;

		// Overrdiables
		virtual void OnPrepare(CDC &DC);
		virtual void OnSetRect(void);
		virtual void OnSetOffset(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation Base Class
//

class DLLAPI CLayFormation : public CLayItem
{
	public:
		// Constructor
		CLayFormation(void);

		// Destructor
		~CLayFormation(void);

		// Operations
		void AddItem(CLayItem *pItem);

	protected:
		// Data Members
		CArray <CLayItem *> m_List;

		// Overrdiables
		void OnSetOffset(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Overlay
//

class DLLAPI CLayFormOverlay : public CLayFormation
{
	public:
		// Constructor
		CLayFormOverlay(void);

	protected:
		// Overridables
		void OnPrepare(CDC &DC);
		void OnSetRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Column
//

class DLLAPI CLayFormCol : public CLayFormation
{
	public:
		// Constructor
		CLayFormCol(void);

	protected:
		// Size Information
		CArray <int> m_Size;

		// Overridables
		void OnPrepare(CDC &DC);
		void OnSetRect(void);

		// Implementation
		void ClearData(void);
		void SetMins(int &nSpace);
		BOOL DealOut(int &nSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Row
//

class DLLAPI CLayFormRow : public CLayFormation
{
	public:
		// Constructor
		CLayFormRow(void);

	protected:
		// Size Information
		CArray <int> m_Size;

		// Overridables
		void OnPrepare(CDC &DC);
		void OnSetRect(void);

		// Implementation
		void ClearData(void);
		void SetMins(int &nSpace);
		BOOL DealOut(int &nSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Grid
//

class DLLAPI CLayFormGrid : public CLayFormation
{
	public:
		// Constructor
		CLayFormGrid(UINT uCols);

	protected:
		// Data Members
		UINT m_uRows;
		UINT m_uCols;

		// Size Information
		CArray <int> m_ColSize;
		CArray <int> m_RowSize;

		// Overridables
		void OnPrepare(CDC &DC);
		void OnSetRect(void);

		// Item Location
		CLayItem * GetItem(UINT uRow, UINT uCol);

		// Size Helpers
		int  GetColMinSize(UINT uCol);
		int  GetColMaxSize(UINT uCol);
		int  GetRowMinSize(UINT uRow);
		int  GetRowMaxSize(UINT uRow);

		// Implementation
		void ClearData(void);
		void FindRows(void);
		void RowSetMins(int &nSpace);
		BOOL RowDealOut(int &nSpace);
		void ColSetMins(int &nSpace);
		BOOL ColDealOut(int &nSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Equal Columns Grid
//

class DLLAPI CLayFormEqual : public CLayFormation
{
	public:
		// Constructor
		CLayFormEqual(UINT uCols);

	protected:
		// Data Members
		UINT m_uRows;
		UINT m_uCols;

		// Size Information
		CArray <int> m_RowSize;
		int	     m_ColSize[2];

		// Overridables
		void OnPrepare(CDC &DC);
		void OnSetRect(void);

		// Item Location
		CLayItem * GetItem(UINT uRow, UINT uCol);

		// Size Helpers
		int  GetColMinSize(UINT uCol);
		int  GetColMaxSize(UINT uCol);
		int  GetRowMinSize(UINT uRow);
		int  GetRowMaxSize(UINT uRow);

		// Implementation
		void ClearData(void);
		void FindRows(void);
		void RowSetMins(int &nSpace);
		BOOL RowDealOut(int &nSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation -- Padder
//

class DLLAPI CLayFormPad : public CLayFormation
{
	public:
		// Constructors
		CLayFormPad(CLayItem *pItem, UINT Flags);
		CLayFormPad(CLayItem *pItem, CRect const &Pad, UINT Flags);
		CLayFormPad(CLayItem *pItem, int nPad, UINT Flags);
		CLayFormPad(CRect const &Pad, UINT Flags);
		CLayFormPad(int nPad, UINT Flags);

		// Attributes
		CRect GetChildRect(void) const;

	protected:
		// Data Members
		UINT  m_Flags;
		CRect m_Pad;
		CRect m_Child;

		// Overridables
		void OnPrepare(CDC &DC);
		void OnSetRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Item for Text
//

class DLLAPI CLayItemText : public CLayItem
{
	public:
		// Constructors
		CLayItemText(PCTXT pText);
		CLayItemText(PCTXT pText, int nExtra, int nGrow);
		CLayItemText(PCTXT pText, CSize Extra, CSize Grow);
		CLayItemText(UINT uCount);
		CLayItemText(UINT uCount, int nExtra, int nGrow);
		CLayItemText(UINT uCount, CSize Extra, CSize Grow);

	protected:
		// Data Mebers
		CString m_Text;
		CSize   m_Extra;
		CSize	m_Grow;

		// Overidables
		void OnPrepare(CDC &DC);
	};

//////////////////////////////////////////////////////////////////////////
//
// Layout Item for Fixed Size
//

class DLLAPI CLayItemSize : public CLayItem
{
	public:
		// Constructors
		CLayItemSize(CSize Size);
		CLayItemSize(CSize Size, int nExtra, int nGrow);
		CLayItemSize(CSize Size, CSize Extra, CSize Grow);

	protected:
		// Data Mebers
		CSize m_Size;
		CSize m_Extra;
		CSize m_Grow;

		// Overidables
		void OnPrepare(CDC &DC);
	};

// End of File

#endif
