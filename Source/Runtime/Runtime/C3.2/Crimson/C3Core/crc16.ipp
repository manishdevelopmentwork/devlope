
//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2001 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// 16-bit CRC Generator
//

// Constructor

inline CRC16::CRC16(void)
{
	m_CRC = 0;
	}
	
// Attributes

inline WORD CRC16::GetValue(void) const
{
	return m_CRC;
	}
	
// Operations

inline void CRC16::Clear(void)
{
	m_CRC = 0x0000;
	}
	
inline void CRC16::Preset(void)
{
	m_CRC = 0xFFFF;
	}

inline void CRC16::Add(BYTE b)
{
	m_CRC = WORD((m_CRC >> 8) ^ m_Table[b ^ (m_CRC & 0xFF)]);
	}
			
// End of File
