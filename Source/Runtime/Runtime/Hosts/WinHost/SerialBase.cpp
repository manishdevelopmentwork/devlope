
#include "Intern.hpp"

#include "SerialBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serial Base
//

// Constructor

CSerialBase::CSerialBase(CString Name)
{
	StdSetRef();

	m_Name     = Name;

	m_fOpen    = FALSE;

	m_hPort    = NULL;

	m_pHandler = NULL;

	InitObjects();
}

// Destructor

CSerialBase::~CSerialBase(void)
{
	Close();

	TermObjects();
}

// IUnknown

HRESULT CSerialBase::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortObject);

	StdQueryInterface(IPortObject);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CSerialBase::AddRef(void)
{
	StdAddRef();
}

ULONG CSerialBase::Release(void)
{
	StdRelease();
}

// IDevice

BOOL METHOD CSerialBase::Open(void)
{
	return TRUE;
}

// IPortObject

void METHOD CSerialBase::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
}

void METHOD CSerialBase::Bind(IPortSwitch  *pSwitch)
{
}

UINT METHOD CSerialBase::GetPhysicalMask(void)
{
	return Bit(physicalRS232);
}

BOOL METHOD CSerialBase::Open(CSerialConfig const &Config)
{
	if( !m_fOpen ) {

		m_Config = Config;

		if( PortOpen() ) {

			InitOverlapped();

			InitThread();

			m_pHandler->AddRef();

			m_pHandler->OnOpen(Config);

			m_fOpen = TRUE;

			return TRUE;
		}
	}

	return FALSE;
}

void METHOD CSerialBase::Close(void)
{
	if( m_fOpen ) {

		WaitDone();

		TermThread();

		PortClose();

		m_pHandler->OnClose();

		m_pHandler->Release();

		m_fOpen = FALSE;
	}
}

void METHOD CSerialBase::Send(BYTE bData)
{
	WaitDone();

	StartSend(bData);
}

void METHOD CSerialBase::SetBreak(BOOL fBreak)
{
	if( fBreak ) {

		win32::SetCommBreak(m_hPort);
	}
	else
		win32::ClearCommBreak(m_hPort);
}

void METHOD CSerialBase::EnableInterrupts(BOOL fEnable)
{
	if( fEnable ) {

		LeaveGuarded();
	}
	else
		EnterGuarded();
}

void METHOD CSerialBase::SetOutput(UINT uOutput, BOOL fOn)
{
}

BOOL METHOD CSerialBase::GetInput(UINT uInput)
{
	return FALSE;
}

DWORD METHOD CSerialBase::GetHandle(void)
{
	return DWORD(m_hPort);
}

// IClientProcess

BOOL CSerialBase::TaskInit(UINT uTask)
{
	SetThreadName("Serial");

	return TRUE;
}

INT CSerialBase::TaskExec(UINT uTask)
{
	for(;;) {

		if( OnConn() ) {

			HANDLE hObjects[4] = { m_hTimer, m_hSend, m_hRecv, m_hTerm };

			StartRecv();

			for( ;;) {

				UINT uObject = win32::WaitForMultipleObjectsEx( elements(hObjects),
									        hObjects,
									        FALSE,
									        INFINITE,
									        TRUE
										);

				if( uObject == WAIT_OBJECT_0 ) {

					if( !OnTest() ) {

						break;
					}
				}

				EnterGuarded();

				switch( uObject ) {

					case WAIT_OBJECT_0 + 0:

						OnTimer();

						break;

					case WAIT_OBJECT_0 + 1:

						OnSend();

						break;

					case WAIT_OBJECT_0 + 2:

						OnRecv();

						break;

					case WAIT_OBJECT_0 + 3:

						OnTerm();

						LeaveGuarded();

						return 0;
				}

				LeaveGuarded();
			}

			win32::CancelIo(m_hPort);

			OnBreak();

			// TODO -- Why do we need to spin?

			for( PortClose(); !PortOpen(); Sleep(10) );
		}
	}

	return 0;
}

void CSerialBase::TaskStop(UINT uTask)
{
}

void CSerialBase::TaskTerm(UINT uTask)
{
}

// Handlers

BOOL CSerialBase::OnConn(void)
{
	return TRUE;
}

BOOL CSerialBase::OnTest(void)
{
	return TRUE;
}

void CSerialBase::OnTerm(void)
{
	win32::CancelIo(m_hPort);
}

void CSerialBase::OnTimer(void)
{
	m_pHandler->OnTimer();
}

void CSerialBase::OnRecv(void)
{
	if( m_fRecv ) {

		DWORD dwCount;

		if( win32::GetOverlappedResult(m_hPort, &m_oRecv, &dwCount, TRUE) ) {

			for( UINT i = 0; i < dwCount; i++ ) {

				m_pHandler->OnRxData(m_bRecv[i]);
			}

			m_uRecv += dwCount;
		}

		m_fRecv = FALSE;
	}

	DWORD dwSpace = elements(m_bRecv);

	DWORD dwCount = 0;

	if( win32::ReadFile(m_hPort, m_bRecv, dwSpace, &dwCount, &m_oRecv) ) {

		for( UINT i = 0; i < dwCount; i++ ) {

			m_pHandler->OnRxData(m_bRecv[i]);
		}

		m_uRecv += dwCount;

		return;
	}

	if( win32::GetLastError() == ERROR_IO_PENDING ) {

		m_fRecv = TRUE;

		if( m_uRecv ) {

			m_pHandler->OnRxDone();

			m_uRecv = 0;
		}

		return;
	}

	if( OnError() ) {

		return;
	}

	OnDone();
}

void CSerialBase::OnSend(void)
{
	UINT uCount = 1;

	if( m_fSend ) {

		DWORD dwCount;

		win32::GetOverlappedResult(m_hPort, &m_oSend, &dwCount, TRUE);

		m_fSend  = FALSE;

		m_uSend += dwCount;

		uCount   = 0;
	}

	while( !m_fDone ) {

		if( uCount < elements(m_bSend) ) {

			BYTE &bData = m_bSend[uCount];

			if( m_pHandler->OnTxData(bData) ) {

				uCount++;
			}
			else {
				m_fDone = TRUE;

				break;
			}
		}
	}

	if( uCount ) {

		DWORD dwCount = 0;

		if( win32::WriteFile(m_hPort, m_bSend, uCount, &dwCount, &m_oSend) ) {

			m_uSend += dwCount;

			uCount   = 0;
		}
		else {
			if( win32::GetLastError() == ERROR_IO_PENDING ) {

				m_fSend = TRUE;

				return;
			}
		}
	}

	if( m_fDone && !uCount ) {

		win32::ResetEvent(m_hSend);

		if( m_uSend > 1 ) {

			m_pHandler->OnTxDone();
		}

		win32::SetEvent(m_hDone);
	}

	OnDone();
}

void CSerialBase::OnDone(void)
{
}

void CSerialBase::OnBreak(void)
{
}

BOOL CSerialBase::OnError(void)
{
	return FALSE;
}

// Comms

void CSerialBase::StartRecv(void)
{
	m_fRecv = FALSE;

	m_uRecv = 0;

	win32::SetEvent(m_hRecv);
}

void CSerialBase::StartSend(BYTE bData)
{
	win32::WaitForSingleObjectEx(m_hDone, INFINITE, TRUE);

	m_fSend    = FALSE;

	m_fDone    = FALSE;

	m_uSend    = 0;

	m_bSend[0] = bData;

	win32::ResetEvent(m_hDone);

	win32::SetEvent(m_hSend);
}

void CSerialBase::WaitDone(void)
{
	win32::WaitForSingleObjectEx(m_hDone, INFINITE, TRUE);
}

// Objects

void CSerialBase::InitObjects(void)
{
	m_hTimer = win32::CreateWaitableTimer(NULL, FALSE, NULL);

	m_hSend  = win32::CreateEvent(NULL, FALSE, FALSE, NULL);

	m_hRecv  = win32::CreateEvent(NULL, FALSE, FALSE, NULL);

	m_hConn  = win32::CreateEvent(NULL, FALSE, FALSE, NULL);

	m_hDone  = win32::CreateEvent(NULL, TRUE, TRUE, NULL);

	m_hTerm  = win32::CreateEvent(NULL, TRUE, FALSE, NULL);

	win32::InitializeCriticalSection(&m_cs);
}

void CSerialBase::TermObjects(void)
{
	win32::CloseHandle(m_hTimer);

	win32::CloseHandle(m_hSend);

	win32::CloseHandle(m_hRecv);

	win32::CloseHandle(m_hConn);

	win32::CloseHandle(m_hDone);

	win32::CloseHandle(m_hTerm);

	win32::DeleteCriticalSection(&m_cs);
}

void CSerialBase::InitOverlapped(void)
{
	memset(&m_oSend, 0, sizeof(OVERLAPPED));

	memset(&m_oRecv, 0, sizeof(OVERLAPPED));

	memset(&m_oConn, 0, sizeof(OVERLAPPED));

	m_oSend.hEvent = m_hSend;

	m_oRecv.hEvent = m_hRecv;

	m_oConn.hEvent = m_hConn;
}

void CSerialBase::InitThread(void)
{
	win32::LARGE_INTEGER liTime;

	liTime.QuadPart = 0LL;

	win32::SetEvent(m_hDone);

	win32::ResetEvent(m_hTerm);

	win32::SetWaitableTimer(m_hTimer, &liTime, 10, NULL, NULL, FALSE);

	m_pThread = CreateClientThread(this, 0, 0);
}

void CSerialBase::TermThread(void)
{
	win32::WaitForSingleObjectEx(m_hDone, INFINITE, TRUE);

	win32::SetEvent(m_hTerm);

	win32::CancelWaitableTimer(m_hTimer);

	m_pThread->Wait(INFINITE);

	m_pThread->Release();
}

// Interrupts

void CSerialBase::EnterGuarded(void)
{
	win32::EnterCriticalSection(&m_cs);
}

void CSerialBase::LeaveGuarded(void)
{
	win32::LeaveCriticalSection(&m_cs);
}

// Implementation

BOOL CSerialBase::PortOpen(void)
{
	return FALSE;
}

void CSerialBase::PortClose(void)
{
	win32::CloseHandle(m_hPort);
}

// End of File
