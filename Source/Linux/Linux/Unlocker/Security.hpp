
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Security_HPP

#define INCLUDE_Security_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Types
//

typedef struct atcacert_def_s atcacert_def_t;

//////////////////////////////////////////////////////////////////////////
//
// Software Groups
//

enum
{
	SW_GROUP_1  = 0,
	SW_GROUP_2  = 1,
	SW_GROUP_3A = 2,
	SW_GROUP_3B = 3,
	SW_GROUP_3C = 4,
	SW_GROUP_4  = 5
};

//////////////////////////////////////////////////////////////////////////
//
// Secure Device Information
//

#pragma pack(1)

struct CSecureDeviceInfo
{
	BYTE	m_bModel[8];
	BYTE	m_bSerial[16];
	BYTE	m_bDefPass[22];
	BYTE	m_bMac0[6];
	BYTE	m_bMac1[6];
	DWORD	m_Options;
	WORD	m_Revision;
};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Hardware Security Module
//

class CSecurity
{
public:
	// Constructor
	CSecurity(void);

	// Destructor
	~CSecurity(void);

	// IDevice
	bool Open(void);

	// IFeatures
	UINT GetEnabledGroup(void);
	bool GetDeviceInfo(CSecureDeviceInfo &Info);
	UINT GetDeviceCode(PBYTE pData, UINT uData);
	bool InstallUnlock(PCBYTE pData, UINT uData);

	// Device Information
	string GetDeviceModel(void);
	string GetSerialNumber(void);

	// Password Generator
	string MakePassword(void);

protected:
	// Data Members
	UINT		  m_uGroup;
	CSecureDeviceInfo m_Info;
	UINT		  m_SignerSize;
	UINT		  m_DeviceSize;
	BYTE		  m_SignerCert[4096];
	BYTE		  m_DeviceCert[4096];
	BYTE		  m_IssuerKey[64];
	BYTE		  m_SignerKey[64];
	BYTE		  m_DeviceKey[64];
	BYTE		  m_DeviceNum[16];

	// HSM Helpers
	bool OpenLibrary(void);
	bool FindIssuerKey(void);
	bool ReadSignerCert(void);
	bool ReadDeviceCert(void);

	// Device Info
	bool ReadDeviceInfo(void);
	bool ImportDeviceInfo(void);
	bool VerifyKeyFile(vector<string> const &List);
	bool ReadDeviceKey(void);
	bool MakeKey(PBYTE key, PCSTR x, PCSTR y);

	// Implementation
	string ReadFile(PCTXT pName, PCTXT pDef);
	string ReadSkvs(PCTXT pKey, PCTXT pDef);
	string ReadBoot(PCTXT pKey, PCTXT pDef);
	size_t Tokenize(vector<string> &list, string text, char sep);
};

// End of File

#endif
