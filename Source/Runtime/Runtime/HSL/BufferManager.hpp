
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_BufferManager_HPP
	
#define	INCLUDE_BufferManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Buffer Manager
//

class CBufferManager : public IBufferManager, public IDiagProvider
{
	public:
		// Constructor
		CBufferManager(void);

		// Destructor
		~CBufferManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IBufferManager
		CBuffer * METHOD Allocate (UINT uSize);
		bool	  METHOD MarkFree (CBuffer *pBuff);
		void	  METHOD ShowError(CBuffer *pBuff, PCTXT pText);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Buffer Object
		class CBuff : public CBuffer
		{
			public:
				// Constructor
				CBuff(CBufferManager *pManager, UINT uAlloc);

				// Data Members
				CBuff * m_pNextBuff;
				CBuff * m_pPrevBuff;
			};

		// Data Members
		ULONG    m_uRefs;
		IMutex * m_pLock;
		CBuff  * m_pHeadFree;
		CBuff  * m_pTailFree;
		CBuff  * m_pHeadUsed;
		CBuff  * m_pTailUsed;

		// Implementation
		void CreatePool(void);

		// Diagnostics
		bool AddDiagCmds(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
