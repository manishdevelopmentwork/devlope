
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// System Log Viewer
//

// Code

function pageMain() {

	initLogArea("/ajax/read-pipe.ajax?type=syslog", true);

	m_log_save = "syslog.txt";

	$("#log-read").click(function () { window.open("/ajax/logread.ajax"); });
}

// End of File
