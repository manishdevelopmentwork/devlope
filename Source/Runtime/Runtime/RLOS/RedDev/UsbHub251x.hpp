
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Shared_UsbHub251x_HPP
	
#define	INCLUDE_Shared_UsbHub251x_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub 251x Serial Eeprom
//

class CUsbHub251x : public ISerialMemory
{
	public:
		// Constructor
		CUsbHub251x(void);

		// Destructor
		~CUsbHub251x(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISerialMemory
		UINT METHOD GetSize(void);
		BOOL METHOD IsFast(void);
		BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount);
		BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount);

	protected:
		// Data Members
		ULONG   m_uRefs;
		II2c  * m_pI2c;
		BYTE    m_bChip;
		BYTE    m_bBuff[32 + 1];
	};

// End of File

#endif
