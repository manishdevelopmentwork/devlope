
#include "intern.hpp"

#include "emcofp93.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// EMCO FP-93 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEMCOFP93DeviceOptions, CUIItem);

// Constructor

CEMCOFP93DeviceOptions::CEMCOFP93DeviceOptions(void)
{
	m_Drop = 0;
	}

// UI Management

void CEMCOFP93DeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEMCOFP93DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CEMCOFP93DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// EMCO FP-93 Master
//

// Instantiator

ICommsDriver *	Create_EMCOFP93SerialDriver(void)
{
	return New CEMCOFP93SerialDriver;
	}

// Constructor

CEMCOFP93SerialDriver::CEMCOFP93SerialDriver(void)
{
	m_wID		= 0x4045;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "EMCO";
	
	m_DriverName	= "FP-93";
	
	m_Version	= "1.00";
	
	m_ShortName	= "EMCO FP-93";

	AddSpaces();
	}

// Binding Control

UINT CEMCOFP93SerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

// Configuration

CLASS CEMCOFP93SerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEMCOFP93DeviceOptions);
	}

void CEMCOFP93SerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Address Helpers

BOOL CEMCOFP93SerialDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	UINT u       = tatoi(Text);

	switch( IsStringItem(pSpace->m_uTable, u) ) {

		case 0: // not a string item
			return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);

		case 1: // selected Sxx

			Addr.a.m_Table  = pSpace->m_uTable;
			Addr.a.m_Offset = 0;
			Addr.a.m_Type   = addrLongAsLong;
			Addr.a.m_Extra  = 0;

			return TRUE;

		}

	CString sErr;

	sErr.Printf( "S%d", u );

	Error.Set( sErr, 0 );

	return FALSE;
	}

BOOL CEMCOFP93SerialDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	UINT uTable    = Addr.a.m_Table;

	UINT uFLString = IsStringItem(uTable, Addr.a.m_Offset);

	if( uTable == addrNamed || (uFLString == 1) ){

		Text.Printf( "%s", pSpace->m_Prefix );

		return TRUE;
		}

	if( uFLString == 2 ) { // this is a bad assignment

		Text.Printf( "S%d", Addr.a.m_Offset );

		return FALSE;
		}

	return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
	}

// Implementation

void CEMCOFP93SerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"F",	"Float Registers",		10, 1,	 99, addrRealAsReal));
	AddSpace(New CSpace(2,	"L",	"Long Integer Registers",	10, 1,	 99, addrLongAsLong));
	AddSpace(New CSpace(3,	"S19",	"Temperature Units <String>",	10, 0,	  4, addrLongAsLong));
	AddSpace(New CSpace(4,	"S24",	"Pressure Units <String>",	10, 0,	  4, addrLongAsLong));
	AddSpace(New CSpace(5,	"S26",	"Density Units <String>",	10, 0,	  4, addrLongAsLong));
	AddSpace(New CSpace(6,	"S38",	"Volume Flow Units <String>",	10, 0,	  4, addrLongAsLong));
	AddSpace(New CSpace(7,	"S44",	"Mass Flow Units <String>",	10, 0,	  4, addrLongAsLong));
	AddSpace(New CSpace(8,	"S49",	"Energy Flow Units <String>",	10, 0,	  4, addrLongAsLong));
	AddSpace(New CSpace(9,	"S94",	"Unit Information Block <String>",10, 0,  4, addrLongAsLong));
	AddSpace(New CSpace(	"C03",	"Analog Input #1 current, mA",		C03, addrRealAsReal));
	AddSpace(New CSpace(	"C04",	"Analog Input #2 current, mA",		C04, addrRealAsReal));
	AddSpace(New CSpace(	"C05",	"Analog Input #3 current, mA",		C05, addrRealAsReal));
	AddSpace(New CSpace(	"C06",	"Analog Input #4 current, mA",		C06, addrRealAsReal));
	AddSpace(New CSpace(	"C51",	"Analog Output current, mA",		C51, addrRealAsReal));
	AddSpace(New CSpace(	"C46",	"Avg. Energy Flow",			C46, addrRealAsReal));
	AddSpace(New CSpace(	"C41",	"Avg. Mass Flow",			C41, addrRealAsReal));
	AddSpace(New CSpace(	"C21",	"Avg. Pressure",			C21, addrRealAsReal));
	AddSpace(New CSpace(	"C11",	"Avg. Temperature #1",			C11, addrRealAsReal));
	AddSpace(New CSpace(	"C15",	"Avg. Temperature #2",			C15, addrRealAsReal));
	AddSpace(New CSpace(	"C35",	"Avg. Temp. Comp. Volume Flow",		C35, addrRealAsReal));
	AddSpace(New CSpace(	"C31",	"Avg. Volume Flow",			C31, addrRealAsReal));
	AddSpace(New CSpace(	"C50",	"Calculation Interval, secs.",		C50, addrRealAsReal));
	AddSpace(New CSpace(	"C91",	"Clear Faults and Changed Flag",	C91, addrLongAsLong));
	AddSpace(New CSpace(	"C92",	"Clear Statistical Values",		C92, addrLongAsLong));
	AddSpace(New CSpace(	"C93",	"Clear Totalizers",			C93, addrLongAsLong));
	AddSpace(New CSpace(	"C66",	"Compressibility Factor",		C66, addrRealAsReal));
	AddSpace(New CSpace(	"C25",	"Density",				C25, addrRealAsReal));
	AddSpace(New CSpace(	"C18",	"Differential Temperature ",		C18, addrRealAsReal));
	AddSpace(New CSpace(	"C45",	"Energy Flow",				C45, addrRealAsReal));
	AddSpace(New CSpace(	"C68",	"Enthalpy",				C68, addrRealAsReal));
	AddSpace(New CSpace(	"C69",	"Enthalpy (Temp. #2)",			C69, addrRealAsReal));
	AddSpace(New CSpace(	"C90",	"Fault Flags",				C90, addrLongAsLong));
	AddSpace(New CSpace(	"C08",	"Flow direction 1=Fwd, 0=Rev",		C08, addrLongAsLong));
	AddSpace(New CSpace(	"C07",	"Frequency, Hz",			C07, addrRealAsReal));
	AddSpace(New CSpace(	"C40",	"Mass Flow",				C40, addrRealAsReal));
	AddSpace(New CSpace(	"C48",	"Max. Energy Flow",			C48, addrRealAsReal));
	AddSpace(New CSpace(	"C43",	"Max. Mass Flow",			C43, addrRealAsReal));
	AddSpace(New CSpace(	"C23",	"Max. Pressure",			C23, addrRealAsReal));
	AddSpace(New CSpace(	"C13",	"Max. Temperature #1",			C13, addrRealAsReal));
	AddSpace(New CSpace(	"C17",	"Max. Temperature #2",			C17, addrRealAsReal));
	AddSpace(New CSpace(	"C37",	"Max. Temp. Comp. Volume Flow",		C37, addrRealAsReal));
	AddSpace(New CSpace(	"C33",	"Max. Volume Flow",			C33, addrRealAsReal));
	AddSpace(New CSpace(	"C47",	"Min. Energy Flow",			C47, addrRealAsReal));
	AddSpace(New CSpace(	"C42",	"Min. Mass Flow",			C42, addrRealAsReal));
	AddSpace(New CSpace(	"C22",	"Min. Pressure",			C22, addrRealAsReal));
	AddSpace(New CSpace(	"C12",	"Min. Temperature #1",			C12, addrRealAsReal));
	AddSpace(New CSpace(	"C16",	"Min. Temperature #2",			C16, addrRealAsReal));
	AddSpace(New CSpace(	"C36",	"Min. Temp. Comp. Volume Flow",		C36, addrRealAsReal));
	AddSpace(New CSpace(	"C32",	"Min. Volume Flow",			C32, addrRealAsReal));
	AddSpace(New CSpace(	"C62",	"Obscuration factor",			C62, addrRealAsReal));
	AddSpace(New CSpace(	"C61",	"Profile Factor",			C61, addrRealAsReal));
	AddSpace(New CSpace(	"C20",	"Pressure",				C20, addrRealAsReal));
	AddSpace(New CSpace(	"C65",	"Reynolds number",			C65, addrRealAsReal));
	AddSpace(New CSpace(	"C01",	"RTD #1 resistance, ohms",		C01, addrRealAsReal));
	AddSpace(New CSpace(	"C02",	"RTD #2 resistance, ohms",		C02, addrRealAsReal));
	AddSpace(New CSpace(	"C27",	"Specific Volume",			C27, addrRealAsReal));
	AddSpace(New CSpace(	"C67",	"Supercompressibility Factor",		C67, addrRealAsReal));
	AddSpace(New CSpace(	"C10",	"Temperature #1",			C10, addrRealAsReal));
	AddSpace(New CSpace(	"C14",	"Temperature #2",			C14, addrRealAsReal));
	AddSpace(New CSpace(	"C34",	"Temp. Comp. Volume Flow",		C34, addrRealAsReal));
	AddSpace(New CSpace(	"C81",	"Time, Alarm, A/D Overrange",		C81, addrLongAsLong));
	AddSpace(New CSpace(	"C76",	"Time, Alarm, Analog Output",		C76, addrLongAsLong));
	AddSpace(New CSpace(	"C77",	"Time, Alarm, Flow Input Range",	C77, addrLongAsLong));
	AddSpace(New CSpace(	"C80",	"Time, Alarm, Pressure Input Range",	C80, addrLongAsLong));
	AddSpace(New CSpace(	"C75",	"Time, Alarm, Relay Output Rate",	C75, addrLongAsLong));
	AddSpace(New CSpace(	"C78",	"Time, Alarm, Temp. Input Range",	C78, addrLongAsLong));
	AddSpace(New CSpace(	"C79",	"Time, Alarm, Temp. Input 2 Range",	C79, addrLongAsLong));
	AddSpace(New CSpace(	"C82",	"Time, Fault, Battery",			C82, addrLongAsLong));
	AddSpace(New CSpace(	"C83",	"Time, Fault, EEPROM",			C83, addrLongAsLong));
	AddSpace(New CSpace(	"C85",	"Time, Fault, RAM",			C85, addrLongAsLong));
	AddSpace(New CSpace(	"C84",	"Time, Fault, ROM",			C84, addrLongAsLong));
	AddSpace(New CSpace(	"C71",	"Time, Changed Flag Set",		C71, addrLongAsLong));
	AddSpace(New CSpace(	"C70",	"Time, Current",			C70, addrLongAsLong));
	AddSpace(New CSpace(	"C74",	"Time, Power Failure",			C74, addrLongAsLong));
	AddSpace(New CSpace(	"C72",	"Time, Statistical Values Cleared",	C72, addrLongAsLong));
	AddSpace(New CSpace(	"C73",	"Time, Totalizers Cleared",		C73, addrLongAsLong));
	AddSpace(New CSpace(	"C56",	"Totalizer #1, Assignment",		C56, addrLongAsLong));
	AddSpace(New CSpace(	"C52",	"Totalizer #1, Fwd, Non-Reset",		C52, addrLongAsLong));
	AddSpace(New CSpace(	"C53",	"Totalizer #1, Fwd, Resettable",	C53, addrLongAsLong));
	AddSpace(New CSpace(	"C57",	"Totalizer #1, Scale Factor",		C57, addrRealAsReal));
	AddSpace(New CSpace(	"C58",	"Totalizer #2, Assignment",		C58, addrLongAsLong));
	AddSpace(New CSpace(	"C54",	"Totalizer #2, Rev, Non-Rst",		C54, addrLongAsLong));
	AddSpace(New CSpace(	"C55",	"Totalizer #2, Rev, Resettable",	C55, addrLongAsLong));
	AddSpace(New CSpace(	"C59",	"Totalizer #2, Scale Factor",		C59, addrRealAsReal));
	AddSpace(New CSpace(	"C63",	"Velocity, Line",			C63, addrRealAsReal));
	AddSpace(New CSpace(	"C60",	"Velocity, Raw",			C60, addrRealAsReal));
	AddSpace(New CSpace(	"C64",	"Viscosity",				C64, addrRealAsReal));
	AddSpace(New CSpace(	"C30",	"Volume Flow",				C30, addrRealAsReal));
	}

// Helper
UINT CEMCOFP93SerialDriver::IsStringItem(UINT uID, UINT uOffset)
{
	if( uID >= 3 && uID <= 9 ) return 1;

	switch( uOffset ) {

		case 19:
		case 24:
		case 26:
		case 38:
		case 44:
		case 49:
		case 94:
			return 2;
		}

	return 0;
	}

UINT CEMCOFP93SerialDriver::FLItemToString(UINT uOffset)
{
	switch( uOffset ) {

		case C19: return 3;
		case C24: return 4;
		case C26: return 5;
		case C38: return 6;
		case C44: return 7;
		case C49: return 8;
		}

	return 9;
	}

// End of File
