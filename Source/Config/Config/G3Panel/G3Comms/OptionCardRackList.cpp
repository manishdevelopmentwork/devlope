
#include "Intern.hpp"

#include "OptionCardRackList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPortList.hpp"
#include "OptionCardItem.hpp"
#include "OptionCardRackItem.hpp"
#include "UsbTreePath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Option Card Rack Item List
//

// Dynamic Class

AfxImplementDynamicClass(COptionCardRackList, CItemIndexList);

// Constructor

COptionCardRackList::COptionCardRackList(void)
{
	m_Class = NULL;
	}

// Item Access

COptionCardRackItem * COptionCardRackList::GetItem(INDEX Index) const
{
	return (COptionCardRackItem *) CItemIndexList::GetItem(Index);
	}

COptionCardRackItem * COptionCardRackList::GetItem(UINT uSlot) const
{
	return (COptionCardRackItem *) CItemIndexList::GetItem(uSlot);
	}

// Operations

void COptionCardRackList::Validate(BOOL fExpand)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		pRack->Validate(fExpand);
		}
	}

void COptionCardRackList::ClearSysBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		pRack->ClearSysBlocks();
		}
	}

void COptionCardRackList::NotifyInit(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		pRack->NotifyInit();
		}
	}

void COptionCardRackList::CheckMapBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		pRack->CheckMapBlocks();
		}
	}

UINT COptionCardRackList::GetPowerTotal(void)
{
	UINT uTotal = 0;

	UINT uSlots = 0;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		uTotal += pRack->GetPowerTotal();

		uSlots++;
		}

	AfxTouch(uSlots);

	return uTotal;
	}

BOOL COptionCardRackList::CheckPower(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		if( !pRack->CheckPower() ) {

			return FALSE;
			}
		}

	return TRUE;
	}

UINT COptionCardRackList::GetSlotCount(void)
{
	UINT uTotal = 0;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		uTotal += pRack->GetSlotCount();
		}

	return uTotal;
	}

UINT COptionCardRackList::GetRackCount(void)
{
	UINT uTotal = 0;

	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		uTotal += 1;

		uTotal += pRack->GetRackCount();
		}

	return uTotal;
	}

BOOL COptionCardRackList::HasType(UINT uClass) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		if( pRack->HasType(uClass) ) {

			return TRUE;
			}
		}

	return FALSE;;
	}

// Ports List Access

BOOL COptionCardRackList::GetPortList(CCommsPortList * &pList, UINT uIndex) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		if( !pRack->GetPortList(pList, uIndex) ) {

			uIndex -= pRack->GetSlotCount();

			continue;
			}

		return TRUE;
		}

	return FALSE;
	}

// Rack

COptionCardRackItem * COptionCardRackList::FindRack(CUsbTreePath const &Rack) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		if( pRack && pRack->m_Slot == Rack.dw ) {

			return pRack;
			}
		}

	return NULL;
	}

COptionCardRackItem * COptionCardRackList::FindRack(UINT Rack) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		if( pRack && pRack->m_Number == Rack ) {

			return pRack;
			}
		}

	return NULL;
	}

// Slot

COptionCardItem * COptionCardRackList::FindSlot(CUsbTreePath const &Slot) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n)->FindSlot(Slot);

		if( pOption ) {

			return pOption;
			}
		}

	return NULL;
	}

COptionCardItem * COptionCardRackList::FindSlot(UINT Slot) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardItem *pOption = GetItem(n)->FindSlot(Slot);

		if( pOption ) {

			return pOption;
			}
		}

	return NULL;
	}

// Firmware

void COptionCardRackList::GetFirmwareList(CArray<UINT> &List) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		COptionCardRackItem *pRack = GetItem(n);

		pRack->GetFirmwareList(List);
		}
	}

// End of File
