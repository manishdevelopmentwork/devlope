
#include "intern.hpp"

#include "idec3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Idec3 Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CIdec3Driver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CIdec3Driver::CIdec3Driver(void)
{
	m_Ident     = DRIVER_ID;
}

// Configuration

void MCALL CIdec3Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CIdec3Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
}

// Device

CCODE MCALL CIdec3Driver::DeviceOpen(IDevice *pDevice)
{
	CIdecBaseDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CIdec3Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CIdecBaseDriver::DeviceClose(fPersist);
}

// Overridables

BOOL CIdec3Driver::Send(void)
{
	Sleep(20);

	m_pData->ClearRx();

	m_pData->Write(PCBYTE(m_bTxBuff), m_uPtr, FOREVER);

	return TRUE;
}

BYTE CIdec3Driver::GetFrame(void)
{
	BYTE bType = 0;

	UINT uState = 0;

	UINT uPtr = 0;

	UINT uData = 0;

	UINT uTimer = 0;

	SetTimer(TIMEOUT);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
		}

		switch( uState ) {

			case 0:
				switch( uData ) {

					case ACK:
					case NAK:

						bType    = uData;
						m_bCheck = uData;
						uPtr     = 0;
						uState   = 1;

						break;
				}
				break;

			case 1:
				if( uData == CR ) {

					m_bCheck ^= m_bRxBuff[uPtr - 1];

					m_bCheck ^= m_bRxBuff[uPtr - 2];

					if( m_bCheck == GetValue(uPtr - 2, 2, 16) ) {

						return bType;
					}

					return FALSE;
				}
				else {
					m_bCheck ^= uData;

					m_bRxBuff[uPtr++] = uData;

					if( uPtr >= sizeof(m_bRxBuff) ) {

						return FALSE;
					}
				}
				break;
		}
	}

	return FALSE;
}

// End of File
