
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_DiskManager_HPP

#define	INCLUDE_DiskManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Disk Manager Object
//

class CDiskManager : public IDiskManager, public IDiagProvider
{
	public:
		// Constructor
		CDiskManager(UINT uPriority);

		// Destructor
		~CDiskManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiskManager
		UINT           METHOD RegisterHost(UINT iDisk);
		void           METHOD UnregisterHost(UINT iDisk, UINT iHost);
		UINT	       METHOD RegisterDisk(IBlockDevice *pDevice);
		UINT	       METHOD FindDisk(CHAR cDrive);
		UINT           METHOD FindVolume(CHAR cDrive);
		bool	       METHOD CheckDisk(UINT iDisk);
		bool	       METHOD EjectDisk(UINT iDisk);
		bool	       METHOD FormatDisk(UINT iDisk);
		UINT           METHOD GetDiskCount(void);
		IBlockDevice * METHOD GetDisk(UINT iDisk);
		IBlockCache  * METHOD GetDiskCache(UINT iDisk);
		bool	       METHOD GetDiskReady(UINT iDisk);
		UINT	       METHOD GetDiskStatus(UINT iDisk);
		UINT           METHOD GetVolumeCount(UINT iDisk);
		UINT	       METHOD GetVolumeIndex(UINT iDisk, UINT n);
		bool           METHOD WaitDiskLock(UINT iHost, UINT iDisk, UINT uType, UINT uWait);
		bool           METHOD FreeDiskLock(UINT iHost, UINT iDisk, UINT uType);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);
	
	protected:
		// Flags
		enum
		{	
			flagPromote	= Bit(31),
			};

		// Host Structure
		struct CHost
		{
			UINT           m_iIndex;
			UINT           m_iDisk;
			UINT           m_uActive;
			UINT volatile  m_uFlags;
			INT  volatile  m_nLockRead;
			INT  volatile  m_nLockWrite;
			IEvent	     * m_pLockWrite;
			CHost        * m_pNext;
			CHost        * m_pPrev;
			};

		// Volume Structure
		struct CVolume
		{
			UINT m_iIndex;
			CHAR m_cName;
			};

		// Mapping
		struct CMap
		{
			DWORD m_Disk	:  8;
			DWORD m_Vol	:  8;
			DWORD m_Spare	: 15;
			DWORD m_Used	:  1;
			};

		// Flags
		enum Flags
		{
			flagEjectPend	= Bit(0),
			flagEjectDone	= Bit(1),
			flagFormat	= Bit(2),
			flagCheck	= Bit(3),
			flagPresent	= Bit(4),
			};

		// Disk Structure
		struct CDisk
		{
			UINT		  m_iIndex;
			UINT volatile	  m_uFlags;
			IBlockDevice    * m_pDev;
			IBlockCache	* m_pCache;
			CVolume		  m_VolList[4];
			UINT		  m_uVolCount;
			CHost		  m_HostList[8];
			UINT		  m_uHostCount;
			INT  volatile	  m_nLockCount;
			UINT volatile	  m_uLockFlags;
			IEvent		* m_pLockRead;
			ISemaphore	* m_pSemaphore;
			IMutex		* m_pMutex;
			CHost           * m_pLockHead;
			CHost           * m_pLockTail;
			};

		// Data
		ULONG	         m_uRefs;
		IMutex	       * m_pMutex;
		IThread        * m_pThread;
		IEvent         * m_pEvent;
		IVolumeManager * m_pVolMan;
		CDisk		 m_DiskList[8];
		CMap             m_DriveMap[26];
		UINT		 m_uDiskCount;

		// Task Entry
		void TaskEntry(void);

		// Init
		void Init(void);

		// Flags
		void SetFlags(CDisk &Disk, UINT uFlags);
		void ClrFlags(CDisk &Disk, UINT uFlags);

		// Volumes
		void FormatVolumes(CDisk &Disk);
		void MountVolumes(CDisk &Disk);
		void UnmountVolumes(CDisk &Disk);
		UINT MapDrive(CHAR cDisk) const;
		UINT MapVolume(CHAR cDisk) const;
		CHAR AllocMap(UINT iDisk, UINT iVol);
		void FreeMap(CHAR cName);

		// Locks
		bool WaitDiskLock(CDisk &Disk, CHost &Host, UINT uType, UINT uWait);
		bool FreeDiskLock(CDisk &Disk, CHost &Host, UINT uType);
		bool WaitWriteLock(CDisk &Disk, CHost &Host, UINT uWait);
		bool WaitReadLock(CDisk &Disk, CHost &Host, UINT uWait);
		bool FreeWriteLock(CDisk &Disk, CHost &Host);
		bool FreeReadLock(CDisk &Disk, CHost &Host);

		// Diagnostics
		BOOL AddDiagCmds(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Diagnostics
		void Debug(PCTXT pFormat, ...) const;

		// Entry Point
		static int TaskDiskManager(IThread *pThread, void *pParam, UINT uParam); 
	};

// End of File

#endif
