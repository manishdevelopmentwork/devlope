
#include "Intern.hpp"

#include "ServiceCloudJson.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Possible Classes
//

/*
#include "CloudDeviceDataSet.hpp"
#include "CloudTagSet.hpp"
#include "CodedItem.hpp"
#include "CodedText.hpp"
#include "DataServer.hpp"
#include "NameServer.hpp"
*/

//////////////////////////////////////////////////////////////////////////
//
// JSON MQTT Cloud Connector Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudJson, CServiceCloudBase);

// Constructor

CServiceCloudJson::CServiceCloudJson(void)
{
	m_Root = 0;

	m_Code = 0;
	}

// UI Update

void CServiceCloudJson::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			DoEnables(pHost);
			}
		}

	CServiceCloudBase::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CServiceCloudJson::MakeInitData(CInitData &Init)
{
	CServiceCloudBase::MakeInitData(Init);

	Init.AddByte(BYTE(m_Root));
	Init.AddByte(BYTE(m_Code));

	return TRUE;
	}

// Meta Data Creation

void CServiceCloudJson::AddMetaData(void)
{
	CServiceCloudBase::AddMetaData();

	Meta_AddInteger(Root);
	Meta_AddInteger(Code);
	}

// Implementation

void CServiceCloudJson::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Root", fEnable);

	pHost->EnableUI(this, "Code", fEnable);
	}

// End of File
