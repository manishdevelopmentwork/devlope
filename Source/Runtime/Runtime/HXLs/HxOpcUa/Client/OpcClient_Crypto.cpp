
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Crypto Support

bool COpcClient::EncryptSecret(CByteArray &Data, CString &Name, PCTXT pMethod, PCBYTE pCert, UINT uCert, CByteArray const &Secret)
{
	AfxNewAutoObject(Cipher, "crypto.cipher-rsa", ICryptoAsym);

	if( Cipher ) {

		UINT uFlags = asymKeyPublic | rsaFromCert;

		if( !strcasecmp(pMethod, "Basic128Rsa15") ) {

			Name = OpcUa_AlgorithmUri_Encryption_Rsa15;

			uFlags |= rsaPadPkcs15;
			}
		else {
			Name = OpcUa_AlgorithmUri_Encryption_RsaOaep;

			uFlags |= rsaPadOaep;
			}

		if( Cipher->Initialize(pCert, uCert, "", uFlags) ) {

			Cipher->Encrypt(Data, Secret);

			Cipher->Finalize();
		
			return true;
			}
		}

	return false;
	}

// End of File
