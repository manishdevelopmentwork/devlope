/*****************************************************************************
T5RedUti.c : Redundancy - tools
(c) COPALP 2006
*****************************************************************************/

#include "t5vm.h"
#include "t5redapi.h"

/****************************************************************************/

#define _CRC_UNKNOWN    0xffffffffL

/****************************************************************************/

typedef struct
{
    T5_BOOL  bDefined;
    T5_DWORD dwDefined;
    T5_DWORD dwSize;
    T5_PTR   pCode;
} str_T5reduti;

/****************************************************************************/

T5_PTR T5RedUti_AllocData (void)
{
    str_T5reduti *pUti;

    pUti = T5RedSys_Malloc (sizeof (str_T5reduti));

    pUti->bDefined = FALSE;
    pUti->dwDefined = _CRC_UNKNOWN;
    pUti->dwSize = 0L;
    pUti->pCode = NULL;
    return pUti;
}

void T5RedUti_FreeData (T5_PTR pData)
{
    if (pData != NULL)
        T5RedSys_Free ((str_T5reduti *)pData);
}

void T5RedUti_ResetLocalCodeCRC (T5_PTR pSrvData)
{
    str_T5reduti *pUti, **pUtiPtr;

    pUtiPtr = (str_T5reduti **)pSrvData;
    if (pUtiPtr == NULL)
        return;

    pUti = *pUtiPtr;
    if (pUti == NULL)
        return;

    pUti->bDefined = FALSE;
    pUti->dwDefined = _CRC_UNKNOWN;
    pUti->dwSize = 0L;
    pUti->pCode = NULL;
}

void T5RedUti_SetLocalCodeCRC (T5_PTR pSrvData, T5PTR_MM pMM)
{
    T5PTR_MMB pmmb;
    T5_PTBYTE pCRC;
    str_T5reduti *pUti, **pUtiPtr;

    pUtiPtr = (str_T5reduti **)pSrvData;
    if (pUtiPtr == NULL)
        return;

    pUti = *pUtiPtr;
    if (pUti == NULL)
        return;

    pmmb = &(pMM->mmbCode);
    if (pmmb->pData == NULL)
    {
        T5RedUti_ResetLocalCodeCRC (pSrvData);
        return;
    }
    
    pUti->pCode = pmmb->pData;
    pUti->dwSize = pmmb->dwSize;
    pCRC = (T5_PTBYTE)(pUti->pCode);
    pCRC += (pUti->dwSize - 4);
    T5_MEMCPY (&(pUti->dwDefined), pCRC, sizeof (T5_DWORD));
    pUti->bDefined = TRUE;
}

T5_DWORD T5RedUti_GetLocalCodeCRC (T5_PTR pSrvData, T5_PTDWORD pdwSize)
{
    T5STR_MM mm;
    T5_DWORD dwCRC, dwCodeSize;
    T5_PTBYTE pCode;
    str_T5reduti *pUti, **pUtiPtr;

    pUtiPtr = (str_T5reduti **)pSrvData;
    if (pUtiPtr == NULL)
        pUti = NULL;
    else
        pUti = *pUtiPtr;

    dwCRC = _CRC_UNKNOWN;
    dwCodeSize = 0;
    if (pUti != NULL && pUti->bDefined)
    {
        if (pdwSize)
            *pdwSize = pUti->dwSize;
        return pUti->dwDefined;
    }

    T5MM_Open (&mm, NULL);
    if (T5MM_LoadCode (&mm, NULL) == T5RET_OK
        && (pCode = (T5_PTBYTE)T5MM_LinkCode (&mm)) != NULL)
    {
        dwCodeSize = mm.mmbCode.dwSize;
        pCode += (dwCodeSize - 4);
        T5_MEMCPY (&dwCRC, pCode, sizeof (T5_DWORD));
        T5MM_UnlinkCode (&mm);
    }
    T5MM_Close (&mm);
    if (pdwSize)
        *pdwSize = dwCodeSize;
    return dwCRC;
}

T5_PTR T5RedUti_GetLocalCode (T5_PTR pSrvData, T5PTR_MMB pmmb, T5_PTDWORD pdwSize)
{
    str_T5reduti *pUti, **pUtiPtr;

    pUtiPtr = (str_T5reduti **)pSrvData;
    if (pUtiPtr == NULL)
        pUti = NULL;
    else
        pUti = *pUtiPtr;

    if (pUti != NULL && pUti->bDefined)
    {
        *pdwSize = pUti->dwSize;
        return pUti->pCode;
    }

    *pdwSize = 0;
    T5_MEMSET (pmmb, 0, sizeof (pmmb));
    pmmb->wID = T5MM_CODE;
    if (T5Memory_Load (pmmb, NULL, NULL) != T5RET_OK)
        return NULL;

    if (T5Memory_Link (pmmb, NULL) != T5RET_OK)
    {
        T5Memory_Free (pmmb, NULL);
        return NULL;
    }

    *pdwSize = pmmb->dwSize;
    return pmmb->pData;
}

void T5RedUti_ReleaseLocalCode (T5_PTR pSrvData, T5PTR_MMB pmmb)
{
    str_T5reduti *pUti, **pUtiPtr;

    pUtiPtr = (str_T5reduti **)pSrvData;
    if (pUtiPtr == NULL)
        pUti = NULL;
    else
        pUti = *pUtiPtr;

    if (pUti != NULL && pUti->bDefined)
        return;

    T5Memory_Unlink (pmmb, NULL);
    T5Memory_Free (pmmb, NULL);
}

void T5RedUti_RegisterAppCode (T5_PTR pClientData, T5_PTR pCode)
{
    T5_PTDWORD pdwHere;

    if (pClientData == NULL)
        return;

    pdwHere = (T5_PTDWORD)pClientData;
    if (pCode == NULL && *pdwHere != 0L)
    {
#ifdef T5DEF_REDBUSDRVEXT
        T5BusDrv_ClosePassive ();
#endif /*T5DEF_REDBUSDRVEXT*/
        *pdwHere = 0L;
    }
    else if (pCode != NULL && *pdwHere == 0L)
    {
#ifdef T5DEF_REDBUSDRVEXT
        T5BusDrv_OpenPassive (pCode);
#endif /*T5DEF_REDBUSDRVEXT*/
        *pdwHere = (T5_DWORD)pCode;
    }
}

T5_DWORD T5RedUti_RegisterPartnerCycle (void)
{
    return T5RedSys_GetTickCount ();
}

T5_DWORD T5RedUti_RegisterSwitch (T5_DWORD dwLastReg)
{
    T5_DWORD dwElapsed;

    dwElapsed = T5_BOUNDTIME (T5RedSys_GetTickCount () - dwLastReg);
    T5REDLOG (">> Swap time estimation = %lu ms", dwElapsed);
    return dwElapsed;
}

/* eof **********************************************************************/
