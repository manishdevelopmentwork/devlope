
#include "intern.hpp"

#include "eiMini8.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 2009-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys Mini8 TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEuroInvensysMini8TCPDeviceOptions, CUIItem);       

// Constructor

CEuroInvensysMini8TCPDeviceOptions::CEuroInvensysMini8TCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CEuroInvensysMini8TCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CEuroInvensysMini8TCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CEuroInvensysMini8TCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

// Constructor

CEuroInvensysMini8Driver::CEuroInvensysMini8Driver(void)
{
	AddSpaces();

	InitEIE();
	}

// Destructor

CEuroInvensysMini8Driver::~CEuroInvensysMini8Driver(void)
{
	DeleteAllSpaces();
	}

// Address Management

BOOL CEuroInvensysMini8Driver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEuroInvensysMini8AddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers
BOOL  CEuroInvensysMini8Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	if( !(IsNotStdModbus(pSpace->m_uTable)) ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	Addr.a.m_Table	= pSpace->m_uTable;
	Addr.a.m_Type	= pSpace->m_uType;
	Addr.a.m_Extra	= 0;

	UINT uOffset	= tatoi(Text);

	if( uOffset > pSpace->m_uMaximum ) {

		uOffset = 0;
		}

	m_pEIE->uOffset	= uOffset;

	Addr.a.m_Offset = uOffset;

	return TRUE;
	}

BOOL  CEuroInvensysMini8Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( !IsNotStdModbus(Addr.a.m_Table ) ) {

			return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
			}

		UINT uTable	= Addr.a.m_Table;
		UINT uOffset	= Addr.a.m_Offset;

		m_pEIE->uTable	= uTable;

		LoadItemInfo(uTable);

		if( uTable == SP_PGS ) {

			m_pEIE->uNPos	= uOffset % SEGMAX;
			m_pEIE->uBlock	= (uOffset / PRGMAX) + 1;
			m_pEIE->uSegm	= ((uOffset % PRGMAX) / SEGMAX) + 1;
			}

		else {
			m_pEIE->uNPos	= uOffset % m_pEIE->uQty;
			m_pEIE->uBlock	= (uOffset / m_pEIE->uQty) + 1;
			m_pEIE->uSegm	= 0;
			}

		LoadItemStrings(uTable, TRUE, uOffset);

		Text.Printf(TEXT("%s%d_%s"),

			pSpace->m_Prefix,
			uOffset,
			Get_sName()
			);

		return TRUE;
		}

	return FALSE;
	}

EIESTRINGS * CEuroInvensysMini8Driver::GetEIEPtr(void)
{
	return m_pEIE;
	}

BOOL CEuroInvensysMini8Driver::IsNotStdModbus(UINT uTable)
{
	return uTable >= SP_ACC && uTable < SP_COMT || uTable == SP_PGS;
	}

void CEuroInvensysMini8Driver::InitItemInfo(UINT uTable)
{
	LoadItemInfo(uTable);

	LoadItemStrings(uTable, TRUE, m_pEIE->uNPos);
	}

void CEuroInvensysMini8Driver::LoadItemInfo(UINT uTable)
{
	switch( uTable ) {

 		case SP_ACC: 	LoadAccessInfo();	break;
		case SP_ALA: 	LoadAlarmAInfo();	break;
		case SP_ALB: 	LoadAlarmBInfo();	break;
		case SP_ALS:	LoadAlarmSummaryInfo();	break;
		case SP_BCD: 	LoadBCDInfo();		break;
		case SP_COM: 	LoadCommsInfo();	break;
		case SP_DAL: 	LoadDigAlarmInfo();	break;
		case SP_HUM: 	LoadHumidityInfo();	break;
		case SP_INS: 	LoadInstrumentInfo();	break;
		case SP_CMC:	LoadCurrMonCnfgInfo();	break;
		case SP_CML:	LoadCurrMonLoadInfo();	break;
		case SP_CMS:	LoadCurrMonStatInfo();	break;
		case SP_IOF:	LoadFixedIOInfo();	break;
		case SP_IPM: 	LoadIPMonitorInfo();	break;
		case SP_LG2: 	LoadLgc2Info();		break;
		case SP_LG8: 	LoadLgc8Info();		break;
		case SP_LGI: 	LoadLgcIOInfo();	break;
		case SP_LIN: 	LoadLinInfo();		break;
		case SP_LDG: 	LoadLoopDiagInfo();	break;
		case SP_LMN: 	LoadLoopMainInfo();	break;
		case SP_LOP: 	LoadLoopOPInfo();	break;
		case SP_PID: 	LoadLoopPIDInfo();	break;
		case SP_SET: 	LoadLoopSetupInfo();	break;
		case SP_SP: 	LoadSPInfo();		break;
		case SP_TUN: 	LoadTuneInfo();		break;
		case SP_MAT: 	LoadMath2Info();	break;
		case SP_MOD: 	LoadModInfo();		break;
		case SP_MID: 	LoadModIDInfo();	break;
		case SP_MUL: 	LoadMultInfo();		break;
		case SP_PGM: 	LoadProgrammerDInfo();	break;
		case SP_PGS: 	LoadProgrammerSInfo();	break;
		case SP_PV: 	LoadPVInfo();		break;
		case SP_REC: 	LoadRecipeInfo();	break;
		case SP_RLY: 	LoadRelayInfo();	break;
		case SP_SWO: 	LoadSwitchoverInfo();	break;
		case SP_TIM: 	LoadTimerInfo();	break;
		case SP_TXD: 	LoadTxdrInfo();		break;
		case SP_USE: 	LoadUsrValInfo();	break;
		case SP_ZIR: 	LoadZirconiaInfo();	break;
		}
	}

CString CEuroInvensysMini8Driver::LoadItemStrings(UINT uTable, BOOL fStore, UINT uSelect)
{
	CString s = SNOTU;

	switch( uTable ) {

		case SP_ACC: 	s = LoadAccess(uSelect);	break;
		case SP_ALA: 	s = LoadAlarmA(uSelect);	break;
		case SP_ALB: 	s = LoadAlarmB(uSelect);	break;
		case SP_ALS:	s = LoadAlarmSummary(uSelect);	break;
		case SP_BCD: 	s = LoadBCD(uSelect);		break;
		case SP_COM: 	s = LoadComms(uSelect);		break;
		case SP_DAL: 	s = LoadDigAlarm(uSelect);	break;
		case SP_HUM: 	s = LoadHumidity(uSelect);	break;
		case SP_INS: 	s = LoadInstrument(uSelect);	break;
		case SP_CMC:	s = LoadCurrMonCnfg(uSelect);	break;
		case SP_CML:	s = LoadCurrMonLoad(uSelect);	break;
		case SP_CMS:	s = LoadCurrMonStat(uSelect);	break;
		case SP_IOF:	s = LoadFixedIO(uSelect);	break;
		case SP_IPM: 	s = LoadIPMonitor(uSelect);	break;
		case SP_LG2: 	s = LoadLgc2(uSelect);		break;
		case SP_LG8: 	s = LoadLgc8(uSelect);		break;
		case SP_LGI: 	s = LoadLgcIO(uSelect);		break;
		case SP_LIN: 	s = LoadLin(uSelect);		break;
		case SP_LDG: 	s = LoadLoopDiag(uSelect);	break;
		case SP_LMN: 	s = LoadLoopMain(uSelect);	break;
		case SP_LOP: 	s = LoadLoopOP(uSelect);	break;
		case SP_PID: 	s = LoadLoopPID(uSelect);	break;
		case SP_SET: 	s = LoadLoopSetup(uSelect);	break;
		case SP_SP: 	s = LoadLoopSP(uSelect);	break;
		case SP_TUN: 	s = LoadLoopTune(uSelect);	break;
		case SP_MAT: 	s = LoadMath2(uSelect);		break;
		case SP_MOD: 	s = LoadMod(uSelect);		break;
		case SP_MID: 	s = LoadModID(uSelect);		break;
		case SP_MUL: 	s = LoadMult(uSelect);		break;
		case SP_PGM: 	s = LoadProgrammerData(uSelect);	break;
		case SP_PGS: 	s = LoadProgrammerSegs(uSelect);	break;
		case SP_PV: 	s = LoadPV(uSelect);		break;
		case SP_REC: 	s = LoadRecipe(uSelect);	break;
		case SP_RLY: 	s = LoadRelay(uSelect);		break;
		case SP_SWO: 	s = LoadSwitchover(uSelect);	break;
		case SP_TIM: 	s = LoadTimer(uSelect);		break;
		case SP_TXD: 	s = LoadTxdr(uSelect);		break;
		case SP_USE: 	s = LoadUsrVal(uSelect);	break;
		case SP_ZIR: 	s = LoadZirconia(uSelect);	break;
		}

	return ParseParamString(s, fStore);
	}

BOOL CEuroInvensysMini8Driver::HasBlocks(void)
{
	return m_pEIE->uBlkCt > 1;
	}

CString CEuroInvensysMini8Driver::Get_sName(void)
{
	return m_sName.Left(m_sName.Find('@'));
	}

void CEuroInvensysMini8Driver::Set_sName(CString sName)
{
	m_sName = sName;
	}

UINT CEuroInvensysMini8Driver::GetAddrFromName(void)
{
	return tatoi(m_sName.Mid(m_sName.Find('@') + 1));
	}

// Implementation

void  CEuroInvensysMini8Driver::AddSpaces(void)
{
	AddSpace(New CSpace(SP_ACC,	"ACC",	"Access",				10,     0,     1, WW));
	AddSpace(New CSpace(SP_ALA,	"ALA",	"Alarms",				10,     0,   319, WW));
//	AddSpace(New CSpace(SP_ALB,	"ALB",	"Alarm.5 to Alarm.8",			10,     0,    43, WW));
	AddSpace(New CSpace(SP_ALS,	"ALS",	"Alarm Summary",			10,     0,    21, WW));
	AddSpace(New CSpace(SP_BCD,	"BCD",	"BCDInput",				10,     0,     1, WW));
	AddSpace(New CSpace(SP_COM,	"COM",	"Comms",				10,     0,     0, WW));
	AddSpace(New CSpace(SP_DAL,	"DAL",	"DigAlarm",				10,     0,   223, WW));
	AddSpace(New CSpace(SP_HUM,	"HUM",	"Humidity",				10,     0,     8, WW));
	AddSpace(New CSpace(SP_INS,	"INS",	"Instrument",				10,     0,     6, WW));
	AddSpace(New CSpace(SP_CMC,	"ICG",	"IO.CurrentMonitor.Config",		10,     0,    15, WW));
	AddSpace(New CSpace(SP_CML,	"ICL",	"IO.CurrentMonitor.Config.Load",	10,     0,    79, WW));
	AddSpace(New CSpace(SP_CMS,	"ICS",	"IO.CurrentMonitor.Status",		10,     0,    18, WW));
	AddSpace(New CSpace(SP_IOF,	"IOF",	"IO.FixedIO",				10,     0,     3, WW));
	AddSpace(New CSpace(SP_IPM,	"IPM",	"IPMonitor",				10,     0,     9, WW));
	AddSpace(New CSpace(SP_LG2,	"L2G",	"Lgc2",					10,     0,    71, WW));
	AddSpace(New CSpace(SP_LG8,	"L8G",	"Lgc8",					10,     0,    35, WW));
//	AddSpace(New CSpace(SP_LGI,	"LIO",	"LgcIO",				10,     0,     6, WW));
	AddSpace(New CSpace(SP_LIN,	"LIN",	"Lin16",				10,     0,    33, WW));
	AddSpace(New CSpace(SP_LDG,	"LDG",	"Loop Diag",				10,     0,   287, WW));
	AddSpace(New CSpace(SP_LMN,	"LMN",	"Loop Main",				10,     0,    95, WW));
	AddSpace(New CSpace(SP_LOP,	"LOP",	"Loop OP",				10,     0,   431, WW));
	AddSpace(New CSpace(SP_PID,	"LPI",	"Loop PID",				10,     0,   575, WW));
	AddSpace(New CSpace(SP_SET,	"LSE",	"Loop Setup",				10,     0,    95, WW));
	AddSpace(New CSpace(SP_SP,	"LSP",	"Loop SP",				10,     0,   303, WW));
	AddSpace(New CSpace(SP_TUN,	"LTU",	"Loop Tune",				10,     0,   127, WW));
	AddSpace(New CSpace(SP_MAT,	"MAT",	"Math2",				10,     0,    71, WW));
	AddSpace(New CSpace(SP_MOD,	"MOD",	"Mod",					10,     0,   223, WW));
	AddSpace(New CSpace(SP_MID,	"MID",	"ModIDs",				10,     0,     3, WW));
	AddSpace(New CSpace(SP_MUL,	"MUL",	"MultiOper",				10,     0,    47, WW));
	AddSpace(New CSpace(SP_PGM,	"PGM",	"Programmer General Data",		10,     0,  XPGM, WW));
	AddSpace(New CSpace(SP_PGS,	"PGS",	"Programmer Segment Data",		10,     0,  XPGS, WW));
//	AddSpace(New CSpace(SP_PV,	"PV",	"PV",					10,     0,     9, WW));
	AddSpace(New CSpace(SP_REC,	"REC",	"Recipe",				10,     0,     2, WW));
//	AddSpace(New CSpace(SP_RLY,	"RLY",	"Relay AA",				10,     0,     0, WW));
	AddSpace(New CSpace(SP_SWO,	"SWO",	"Switchover",				10,     0,     2, WW));
	AddSpace(New CSpace(SP_TIM,	"TIM",	"Timer",				10,     0,    11, WW));
//	AddSpace(New CSpace(SP_TXD,	"TXD",	"Txdr",					10,     0,    19, WW));
	AddSpace(New CSpace(SP_USE,	"USR",	"UsrVal",				10,     0,    31, WW));
	AddSpace(New CSpace(SP_ZIR,	"ZIR",	"Zirconia",				10,     0,    57, WW));
	AddSpace(New CSpace(SP_4,	"4",	"Holding Registers",			10,     1, 19999, WW, WR));
	}

CString CEuroInvensysMini8Driver::ParseParamString(CString sParamString, BOOL fStore)
{
	UINT uFind	= sParamString.Find('@');

	CString sName	= sParamString.Left(uFind);	// Name portion
	CString sAddr	= sParamString.Mid(uFind + 1);	// Initial Address portion

	BOOL fPrgr	= sParamString.Right(1) == L"$";	// the segment number is in front of last '.'
	BOOL fIO	= sParamString.Right(1) == L"%";	// the block number is in front of last '.'

	if( HasBlocks() ) {

		UINT uBPos0	= sName.Find('.');				// in most cases, '.' before the block number
		UINT uBPos1	= Find2ndDotFwd(sParamString) + 1;		// in most cases, start of string subsequent to block number

		UINT uSPos1	= fPrgr ? sName.FindRev('.') + 1 : 0;		// beginning of final parameter string
		UINT uSPos0	= fPrgr ? Find2ndDotRev(sParamString) : 0;	// '.' at beginning of segment # string, if any

		if( fIO ) {

			uBPos0	= uSPos0;	// IO block numbers are just before the final parameter
			}

		UINT uBlock	= m_pEIE->uBlock;
		UINT uSegmt	= m_pEIE->uSegm;

		CString Lt	= sName.Left(uBPos0);
		CString Rt	= sName.Mid(fPrgr ? uSPos1 : uBPos1);

		if( fPrgr ) {

			sName.Printf( L"%s.%d.Segment.%d.%s",

				Lt,
				uBlock,
				uSegmt,
				Rt
				);
			}

		else {
			sName.Printf(L"%s.%d.%s", Lt, uBlock, Rt);
			}
		}

	sName.Printf(L"%s@%s", sName, ParseAddr(sParamString, fStore));

	if( fStore ) {

		m_sName = sName;
		}

	return sName;
	}

CString CEuroInvensysMini8Driver::ParseAddr(CString sParamString, BOOL fStore)
{
	CString sAddr	= sParamString.Mid(sParamString.Find('@') + 1);

	UINT uAddr	= tatoi(sAddr);	// base Modbus address for selection

	if( HasBlocks() ) {	// construct Modbus Address

		if( sParamString.Right(1) == L"$" ) {

			uAddr += (m_pEIE->uSegm - 1) * SEGMAX;
			}

		UINT uInc = 0;

		ParseModifiers(sAddr, &uInc);

		if( uInc ) {

			uAddr += (m_pEIE->uBlock - 1) * uInc;
			}
		}

	if( fStore ) {

		m_pEIE->uAddr = uAddr;
		}

	sAddr.Printf("%d", uAddr);

	return sAddr;
	}

void CEuroInvensysMini8Driver::ParseModifiers(CString sParamString, UINT *pInc)
{
	UINT uFind = sParamString.Find('#');

	if( uFind < NOTHING ) {

		*pInc = tatoi(sParamString.Mid(uFind + 1));

		return;
		}

	*pInc = 0;
	}

// Helpers

UINT CEuroInvensysMini8Driver::Find2ndDotFwd(CString s)
{
	UINT uFind1 = s.Find('.');

	UINT uFind2 = (s.Mid(uFind1 + 1)).Find('.');

	return uFind1 + uFind2 + 1;
	}

UINT CEuroInvensysMini8Driver::Find2ndDotRev(CString s)
{
	UINT uFind = s.FindRev('.');

	return s.Left(uFind).FindRev('.');
	}

// Other Help
void CEuroInvensysMini8Driver::InitEIE(void)
{
	m_pEIE	= &EIESTR;

	m_pEIE->uBlock	= 1;
	m_pEIE->uNPos	= 0;
	m_pEIE->uBPos	= 0;
	m_pEIE->uOffset	= 0;
	m_pEIE->uSegm	= 1;
	m_pEIE->uAddr	= SACC1A;
	m_sName		= SACC1N;

	LoadAccessInfo();
	}

// String Loading
void CEuroInvensysMini8Driver::LoadAccessInfo(void)
{
	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 2;
	pE->uTable	= SP_ACC;
	}

void CEuroInvensysMini8Driver::LoadAlarmAInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 32;
	pE->uQty	= 10;
	pE->uTable	= SP_ALA;
	}

void CEuroInvensysMini8Driver::LoadAlarmBInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 11;
	pE->uTable	= SP_ALB;

	if( pE->uBlock < 5 ) {

		pE->uBlock = max(5, pE->uBlock + 4);
		}

	if( pE->uBlock > 8 ) {

		pE->uBlock = 8;
		}
	}

void CEuroInvensysMini8Driver::LoadAlarmSummaryInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 22;
	pE->uTable	= SP_ALS;
	}

void CEuroInvensysMini8Driver::LoadBCDInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 2;
	pE->uTable	= SP_BCD;
	}

void CEuroInvensysMini8Driver::LoadCommsInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 1;
	pE->uTable	= SP_COM;
	}

void CEuroInvensysMini8Driver::LoadDigAlarmInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 32;
	pE->uQty	= 7;
	pE->uTable	= SP_DAL;
	}

void CEuroInvensysMini8Driver::LoadHumidityInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 9;
	pE->uTable	= SP_HUM;
	}

void CEuroInvensysMini8Driver::LoadInstrumentInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 7;
	pE->uTable	= SP_INS;
	}

void CEuroInvensysMini8Driver::LoadCurrMonCnfgInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 16;
	pE->uTable	= SP_CMC;
	}

void CEuroInvensysMini8Driver::LoadCurrMonLoadInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 5;
	pE->uTable	= SP_CML;
	}

void CEuroInvensysMini8Driver::LoadCurrMonStatInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 19;
	pE->uTable	= SP_CMS;
	}

void CEuroInvensysMini8Driver::LoadFixedIOInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 4;
	pE->uTable	= SP_IOF;
	}

void CEuroInvensysMini8Driver::LoadIPMonitorInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 10;
	pE->uTable	= SP_IPM;
	}

void CEuroInvensysMini8Driver::LoadLgc2Info() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 24;
	pE->uQty	= 3;
	pE->uTable	= SP_LG2;
	}

void CEuroInvensysMini8Driver::LoadLgc8Info() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 36;
	pE->uTable	= SP_LG8;
	}

void CEuroInvensysMini8Driver::LoadLgcIOInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 7;
	pE->uTable	= SP_LGI;
	}

void CEuroInvensysMini8Driver::LoadLinInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 34;
	pE->uTable	= SP_LIN;
	}

void CEuroInvensysMini8Driver::LoadLoopDiagInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 18;
	pE->uTable	= SP_LDG;
	}

void CEuroInvensysMini8Driver::LoadLoopMainInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 6;
	pE->uTable	= SP_LMN;
	}

void CEuroInvensysMini8Driver::LoadLoopOPInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 27;
	pE->uTable	= SP_LOP;
	}

void CEuroInvensysMini8Driver::LoadLoopPIDInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 36;
	pE->uTable	= SP_PID;
	}

void CEuroInvensysMini8Driver::LoadLoopSetupInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 6;
	pE->uTable	= SP_SET;
	}

void CEuroInvensysMini8Driver::LoadSPInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 19;
	pE->uTable	= SP_SP;
	}

void CEuroInvensysMini8Driver::LoadTuneInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 16;
	pE->uQty	= 8;
	pE->uTable	= SP_TUN;
	}

void CEuroInvensysMini8Driver::LoadMath2Info() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 24;
	pE->uQty	= 3;
	pE->uTable	= SP_MAT;
	}

void CEuroInvensysMini8Driver::LoadModInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 32;
	pE->uQty	= 7;
	pE->uTable	= SP_MOD;
	}

void CEuroInvensysMini8Driver::LoadModIDInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 4;
	pE->uTable	= SP_MID;
	}

void CEuroInvensysMini8Driver::LoadMultInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 12;
	pE->uTable	= SP_MUL;
	}

void CEuroInvensysMini8Driver::LoadProgrammerDInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= PGCNT;
	pE->uQty	= PGMCNT;
	pE->uTable	= SP_PGM;
	}

void CEuroInvensysMini8Driver::LoadProgrammerSInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= PGCNT;
	pE->uQty	= SEGMAX;
	pE->uTable	= SP_PGS;
	}

void CEuroInvensysMini8Driver::LoadPVInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 10;
	pE->uTable	= SP_PV;
	}

void CEuroInvensysMini8Driver::LoadRecipeInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 3;
	pE->uTable	= SP_REC;
	}

void CEuroInvensysMini8Driver::LoadRelayInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 1;
	pE->uTable	= SP_RLY;
	}

void CEuroInvensysMini8Driver::LoadSwitchoverInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 3;
	pE->uTable	= SP_SWO;
	}

void CEuroInvensysMini8Driver::LoadTimerInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 4;
	pE->uQty	= 3;
	pE->uTable	= SP_TIM;
	}

void CEuroInvensysMini8Driver::LoadTxdrInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 2;
	pE->uQty	= 10;
	pE->uTable	= SP_TXD;
	}

void CEuroInvensysMini8Driver::LoadUsrValInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 1;
	pE->uQty	= 32;
	pE->uTable	= SP_USE;
	}

void CEuroInvensysMini8Driver::LoadZirconiaInfo() {

	EIESTRINGS *pE	= m_pEIE;

	pE->uBlkCt	= 2;
	pE->uQty	= 29;
	pE->uTable	= SP_ZIR;
	}

// String Loading
CString CEuroInvensysMini8Driver::LoadAccess(UINT uSel)
{
	switch( uSel ) {

		case 0: return SACC1;
		case 1: return SACC2;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadAlarmA(UINT uSel)
{
	switch( uSel % 10 ) {

		case 0: return SALM1;
		case 1: return SALM2;
		case 2: return SALM3;
		case 3: return SALM4;
		case 4: return SALM5;
		case 5: return SALM6;
		case 6: return SALM7;
		case 7: return SALM8;
		case 8: return SALM9;
		case 9: return SALM10;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadAlarmB(UINT uSel)
{
	switch( uSel % 11 ) {

		case 0: return SALM19;
		case 1: return SALM20;
		case 2: return SALM21;
		case 3: return SALM22;
		case 4: return SALM23;
		case 5: return SALM24;
		case 6: return SALM25;
		case 7: return SALM26;
		case 8: return SALM27;
		case 9: return SALM28;
		case 10: return SALM29;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadAlarmSummary(UINT uSel)
{
	switch( uSel ) {

		case  0:	return SALS1;
		case  1:	return SALS2;
		case  2:	return SALS3;
		case  3:	return SALS4;
		case  4:	return SALS5;
		case  5:	return SALS6;
		case  6:	return SALS7;
		case  7:	return SALS8;
		case  8:	return SALS9;
		case  9:	return SALS10;
		case 10:	return SALS11;
		case 11:	return SALS12;
		case 12:	return SALS13;
		case 13:	return SALS14;
		case 14:	return SALS15;
		case 15:	return SALS16;
		case 16:	return SALS17;
		case 17:	return SALS18;
		case 18:	return SALS19;
		case 19:	return SALS20;
		case 20:	return SALS21;
		case 21:	return SALS22;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadBCD(UINT uSel)
{
	switch( uSel ) {

		case 0: return SBCD1;
		case 1: return SBCD2;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadComms(UINT uSel)
{
	return uSel ? SCOM2 : SCOM1;
	}

CString CEuroInvensysMini8Driver::LoadDigAlarm(UINT uSel)
{
	switch( uSel % 7 ) {

		case 0: return SDAL1;
		case 1: return SDAL2;
		case 2: return SDAL3;
		case 3: return SDAL4;
		case 4: return SDAL5;
		case 5: return SDAL6;
		case 6: return SDAL7;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadHumidity(UINT uSel)
{
	switch( uSel ) {

		case 0: return SHUM1;
		case 1: return SHUM2;
		case 2: return SHUM3;
		case 3: return SHUM4;
		case 4: return SHUM5;
		case 5: return SHUM6;
		case 6: return SHUM7;
		case 7: return SHUM8;
		case 8: return SHUM9;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadInstrument(UINT uSel)
{
	switch( uSel ) {

		case 0: return SINS1;
		case 1: return SINS2;
		case 2: return SINS3;
		case 3: return SINS4;
		case 4: return SINS5;
		case 5: return SINS6;
		case 6: return SINS7;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadCurrMonCnfg(UINT uSel)
{
	switch( uSel ) {

		case  0: return SCMC1;
		case  1: return SCMC2;
		case  2: return SCMC3;
		case  3: return SCMC4;
		case  4: return SCMC5;
		case  5: return SCMC6;
		case  6: return SCMC7;
		case  7: return SCMC8;
		case  8: return SCMC9;
		case  9: return SCMC10;
		case 10: return SCMC11;
		case 11: return SCMC12;
		case 12: return SCMC13;
		case 13: return SCMC14;
		case 14: return SCMC15;
		case 15: return SCMC16;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadCurrMonLoad(UINT uSel)
{
	switch( uSel % 5) {

		case  0: return SCML1;
		case  1: return SCML2;
		case  2: return SCML3;
		case  3: return SCML4;
		case  4: return SCML5;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadCurrMonStat(UINT uSel)
{
	switch( uSel ) {

		case  0: return SCMS1;
		case  1: return SCMS2;
		case  2: return SCMS3;
		case  3: return SCMS4;
		case  4: return SCMS5;
		case  5: return SCMS6;
		case  6: return SCMS7;
		case  7: return SCMS8;
		case  8: return SCMS9;
		case  9: return SCMS10;
		case 10: return SCMS11;
		case 11: return SCMS12;
		case 12: return SCMS13;
		case 13: return SCMS14;
		case 14: return SCMS15;
		case 15: return SCMS16;
		case 16: return SCMS17;
		case 17: return SCMS18;
		case 18: return SCMS19;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadFixedIO(UINT uSel)
{
	switch( uSel ) {

		case  0: return SIOF1;
		case  1: return SIOF2;
		case  2: return SIOF3;
		case  3: return SIOF4;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadIPMonitor(UINT uSel)
{
	switch( uSel ) {

		case 0: return SIP1;
		case 1: return SIP2;
		case 2: return SIP3;
		case 3: return SIP4;
		case 4: return SIP5;
		case 5: return SIP6;
		case 6: return SIP7;
		case 7: return SIP8;
		case 8: return SIP9;
		case 9: return SIP10;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLgc2(UINT uSel)
{
	switch( uSel % 3 ) {

		case 0: return S2LG1;
		case 1: return S2LG2;
		case 2: return S2LG3;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLgc8(UINT uSel)
{
	switch( uSel ) {

		case  0: return S8LG1;
		case  1: return S8LG2;
		case  2: return S8LG3;
		case  3: return S8LG4;
		case  4: return S8LG5;
		case  5: return S8LG6;
		case  6: return S8LG7;
		case  7: return S8LG8;
		case  8: return S8LG9;
		case  9: return S8LG10;
		case 10: return S8LG11;
		case 11: return S8LG12;
		case 12: return S8LG13;
		case 13: return S8LG14;
		case 14: return S8LG15;
		case 15: return S8LG16;
		case 16: return S8LG17;
		case 17: return S8LG18;
		case 18: return S8LG19;
		case 19: return S8LG20;
		case 20: return S8LG21;
		case 21: return S8LG22;
		case 22: return S8LG23;
		case 23: return S8LG24;
		case 24: return S8LG25;
		case 25: return S8LG26;
		case 26: return S8LG27;
		case 27: return S8LG28;
		case 28: return S8LG29;
		case 29: return S8LG30;
		case 30: return S8LG31;
		case 31: return S8LG32;
		case 32: return S8LG33;
		case 33: return S8LG34;
		case 34: return S8LG35;
		case 35: return S8LG36;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLgcIO(UINT uSel)
{
	switch( uSel ) {

		case 0: return SLGI1;
		case 1: return SLGI2;
		case 2: return SLGI3;
		case 3: return SLGI4;
		case 4: return SLGI5;
		case 5: return SLGI6;
		case 6: return SLGI7;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLin(UINT uSel)
{
	switch( uSel ) {

		case  0: return SLIN1;
		case  1: return SLIN2;
		case  2: return SLIN3;
		case  3: return SLIN4;
		case  4: return SLIN5;
		case  5: return SLIN6;
		case  6: return SLIN7;
		case  7: return SLIN8;
		case  8: return SLIN9;
		case  9: return SLIN10;
		case 10: return SLIN11;
		case 11: return SLIN12;
		case 12: return SLIN13;
		case 13: return SLIN14;
		case 14: return SLIN15;
		case 15: return SLIN16;
		case 16: return SLIN17;
		case 17: return SLIN18;
		case 18: return SLIN19;
		case 19: return SLIN20;
		case 20: return SLIN21;
		case 21: return SLIN22;
		case 22: return SLIN23;
		case 23: return SLIN24;
		case 24: return SLIN25;
		case 25: return SLIN26;
		case 26: return SLIN27;
		case 27: return SLIN28;
		case 28: return SLIN29;
		case 29: return SLIN30;
		case 30: return SLIN31;
		case 31: return SLIN32;
		case 32: return SLIN33;
		case 33: return SLIN34;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopDiag(UINT uSel)
{
	switch( uSel % 18 ) {

		case  0: return SLDG1;
		case  1: return SLDG2;
		case  2: return SLDG3;
		case  3: return SLDG4;
		case  4: return SLDG5;
		case  5: return SLDG6;
		case  6: return SLDG7;
		case  7: return SLDG8;
		case  8: return SLDG9;
		case  9: return SLDG10;
		case 10: return SLDG11;
		case 11: return SLDG12;
		case 12: return SLDG13;
		case 13: return SLDG14;
		case 14: return SLDG15;
		case 15: return SLDG16;
		case 16: return SLDG17;
		case 17: return SLDG18;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopMain(UINT uSel)
{
	switch( uSel % 6 ) {

		case 0: return SLMN1;
		case 1: return SLMN2;
		case 2: return SLMN3;
		case 3: return SLMN4;
		case 4: return SLMN5;
		case 5: return SLMN6;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopOP(UINT uSel)
{
	switch( uSel % 27 ) {

		case  0: return SLOP1;
		case  1: return SLOP2;
		case  2: return SLOP3;
		case  3: return SLOP4;
		case  4: return SLOP5;
		case  5: return SLOP6;
		case  6: return SLOP7;
		case  7: return SLOP8;
		case  8: return SLOP9;
		case  9: return SLOP10;
		case 10: return SLOP11;
		case 11: return SLOP12;
		case 12: return SLOP13;
		case 13: return SLOP14;
		case 14: return SLOP15;
		case 15: return SLOP16;
		case 16: return SLOP17;
		case 17: return SLOP18;
		case 18: return SLOP19;
		case 19: return SLOP20;
		case 20: return SLOP21;
		case 21: return SLOP22;
		case 22: return SLOP23;
		case 23: return SLOP24;
		case 24: return SLOP25;
		case 25: return SLOP26;
		case 26: return SLOP27;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopPID(UINT uSel)
{
	switch( uSel % 36 ) {

		case  0: return SLPD1;
		case  1: return SLPD2;
		case  2: return SLPD3;
		case  3: return SLPD4;
		case  4: return SLPD5;
		case  5: return SLPD6;
		case  6: return SLPD7;
		case  7: return SLPD8;
		case  8: return SLPD9;
		case  9: return SLPD10;
		case 10: return SLPD11;
		case 11: return SLPD12;
		case 12: return SLPD13;
		case 13: return SLPD14;
		case 14: return SLPD15;
		case 15: return SLPD16;
		case 16: return SLPD17;
		case 17: return SLPD18;
		case 18: return SLPD19;
		case 19: return SLPD20;
		case 20: return SLPD21;
		case 21: return SLPD22;
		case 22: return SLPD23;
		case 23: return SLPD24;
		case 24: return SLPD25;
		case 25: return SLPD26;
		case 26: return SLPD27;
		case 27: return SLPD28;
		case 28: return SLPD29;
		case 29: return SLPD30;
		case 30: return SLPD31;
		case 31: return SLPD32;
		case 32: return SLPD33;
		case 33: return SLPD34;
		case 34: return SLPD35;
		case 35: return SLPD36;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopSetup(UINT uSel)
{
	switch( uSel % 6 ) {

		case 0: return SLSE1;
		case 1: return SLSE2;
		case 2: return SLSE3;
		case 3: return SLSE4;
		case 4:	return SLSE5;
		case 5:	return SLSE6;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopSP(UINT uSel)
{
	switch( uSel % 19 ) {

		case  0: return SLSP1;
		case  1: return SLSP2;
		case  2: return SLSP3;
		case  3: return SLSP4;
		case  4: return SLSP5;
		case  5: return SLSP6;
		case  6: return SLSP7;
		case  7: return SLSP8;
		case  8: return SLSP9;
		case  9: return SLSP10;
		case 10: return SLSP11;
		case 11: return SLSP12;
		case 12: return SLSP13;
		case 13: return SLSP14;
		case 14: return SLSP15;
		case 15: return SLSP16;
		case 16: return SLSP17;
		case 17: return SLSP18;
		case 18: return SLSP19;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadLoopTune(UINT uSel)
{
	switch( uSel % 8 ) {

		case 0: return SLTU1;
		case 1: return SLTU2;
		case 2: return SLTU3;
		case 3: return SLTU4;
		case 4: return SLTU5;
		case 5: return SLTU6;
		case 6: return SLTU7;
		case 7: return SLTU8;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadMath2(UINT uSel)
{
	switch( uSel % 3) {

		case 0: return SMAT1;
		case 1: return SMAT2;
		case 2: return SMAT3;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadMod(UINT uSel)
{
	switch( uSel % 7 ) {

		case 0: return SMOD1;
		case 1: return SMOD2;
		case 2: return SMOD3;
		case 3: return SMOD4;
		case 4: return SMOD5;
		case 5: return SMOD6;
		case 6: return SMOD7;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadModID(UINT uSel)
{
	switch( uSel ) {

		case 0: return SMID1;
		case 1: return SMID2;
		case 2: return SMID3;
		case 3: return SMID4;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadMult(UINT uSel)
{
	switch( uSel % 12 ) {

		case  0: return SMUL1;
		case  1: return SMUL2;
		case  2: return SMUL3;
		case  3: return SMUL4;
		case  4: return SMUL5;
		case  5: return SMUL6;
		case  6: return SMUL7;
		case  7: return SMUL8;
		case  8: return SMUL9;
		case  9: return SMUL10;
		case 10: return SMUL11;
		case 11: return SMUL12;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadProgrammerData(UINT uSel)	{

	switch( uSel % PGMCNT ) {

		case  0: return SPGM01;
		case  1: return SPGM02;
		case  2: return SPGM03;
		case  3: return SPGM04;
		case  4: return SPGM05;
		case  5: return SPGM06;
		case  6: return SPGM07;
		case  7: return SPGM08;
		case  8: return SPGM09;
		case  9: return SPGM10;
		case 10: return SPGM11;
		case 11: return SPGM12;
		case 12: return SPGM13;
		case 13: return SPGM14;
		case 14: return SPGM15;
		case 15: return SPGM16;
		case 16: return SPGM17;
		case 17: return SPGM18;
		case 18: return SPGM19;
		case 19: return SPGM20;
		case 20: return SPGM21;
		case 21: return SPGM22;
		case 22: return SPGM23;
		case 23: return SPGM24;
		case 24: return SPGM25;
		case 25: return SPGM26;
		case 26: return SPGM27;
		case 27: return SPGM28;
		case 28: return SPGM29;
		case 29: return SPGM30;
		case 30: return SPGM31;
		case 31: return SPGM32;
		case 32: return SPGM33;
		case 33: return SPGM34;
		case 34: return SPGM35;
		case 35: return SPGM36;
		case 36: return SPGM37;
		case 37: return SPGM38;
		case 38: return SPGM39;
		case 39: return SPGM40;
		case 40: return SPGM41;
		case 41: return SPGM42;
		case 42: return SPGM43;
		case 43: return SPGM44;
		case 44: return SPGM45;
		case 45: return SPGM46;
		case 46: return SPGM47;
		case 47: return SPGM48;
		case 48: return SPGM49;
		case 49: return SPGM50;
		case 50: return SPGM51;
		case 51: return SPGM52;
		case 52: return SPGM53;
		case 53: return SPGM54;
		case 54: return SPGM55;
		case 55: return SPGM56;
		case 56: return SPGM57;
		case 57: return SPGM58;
		case 58: return SPGM59;
		case 59: return SPGM60;
		case 60: return SPGM61;
		case 61: return SPGM62;
		case 62: return SPGM63;
		case 63: return SPGM64;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadProgrammerSegs(UINT uSel)
{
	uSel -= (uSel / PRGMAX) * PRGMAX;	// residue

	switch( uSel % SEGMAX ) {

		case  0: return SPGS01;
		case  1: return SPGS02;
		case  2: return SPGS03;
		case  3: return SPGS04;
		case  4: return SPGS05;
		case  5: return SPGS06;
		case  6: return SPGS07;
		case  7: return SPGS08;
		case  8: return SPGS09;
		case  9: return SPGS10;
		case 10: return SPGS11;
		case 11: return SPGS12;
		case 12: return SPGS13;
		case 13: return SPGS14;
		case 14: return SPGS15;
		case 15: return SPGS16;
		case 16: return SPGS17;
		case 17: return SPGS18;
		case 18: return SPGS19;
		case 19: return SPGS20;
		case 20: return SPGS21;
		case 21: return SPGS22;
		case 22: return SPGS23;
		case 23: return SPGS24;
		case 24: return SPGS25;
		case 25: return SPGS26;
		case 26: return SPGS27;
		case 27: return SPGS28;
		case 28: return SPGS29;
		case 29: return SPGS30;
		case 30: return SPGS31;
		case 31: return SPGS32;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadPV(UINT uSel)
{
	switch( uSel ) {

		case 0: return SPV1;
		case 1: return SPV2;
		case 2: return SPV3;
		case 3: return SPV4;
		case 4: return SPV5;
		case 5: return SPV6;
		case 6: return SPV7;
		case 7: return SPV8;
		case 8: return SPV9;
		case 9: return SPV10;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadRecipe(UINT uSel)
{
	switch( uSel ) {

		case 0: return SRCP1;
		case 1: return SRCP2;
		case 2: return SRCP3;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadRelay(UINT uSel)
{
	return SRLY1;
	}

CString CEuroInvensysMini8Driver::LoadSwitchover(UINT uSel)
{
	switch( uSel ) {

		case 0: return SSWO1;
		case 1: return SSWO2;
		case 2: return SSWO3;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadTimer(UINT uSel)
{
	switch( uSel % 3 ) {

		case 0: return STIM1;
		case 1: return STIM2;
		case 2: return STIM3;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadTxdr(UINT uSel)
{
	switch( uSel % 10 ) {

		case 0: return STXD1;
		case 1: return STXD2;
		case 2: return STXD3;
		case 3: return STXD4;
		case 4: return STXD5;
		case 5: return STXD6;
		case 6: return STXD7;
		case 7: return STXD8;
		case 8: return STXD9;
		case 9: return STXD10;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadUsrVal(UINT uSel)
{
	switch( uSel % 32 ) {

		case  0: return SUSE1;
		case  1: return SUSE2;
		case  2: return SUSE3;
		case  3: return SUSE4;
		case  4: return SUSE5;
		case  5: return SUSE6;
		case  6: return SUSE7;
		case  7: return SUSE8;
		case  8: return SUSE9;
		case  9: return SUSE10;
		case 10: return SUSE11;
		case 11: return SUSE12;
		case 12: return SUSE13;
		case 13: return SUSE14;
		case 14: return SUSE15;
		case 15: return SUSE16;
		case 16: return SUSE17;
		case 17: return SUSE18;
		case 18: return SUSE19;
		case 19: return SUSE20;
		case 20: return SUSE21;
		case 21: return SUSE22;
		case 22: return SUSE23;
		case 23: return SUSE24;
		case 24: return SUSE25;
		case 25: return SUSE26;
		case 26: return SUSE27;
		case 27: return SUSE28;
		case 28: return SUSE29;
		case 29: return SUSE30;
		case 30: return SUSE31;
		case 31: return SUSE32;
		}

	return SNOTU;
	}

CString CEuroInvensysMini8Driver::LoadZirconia(UINT uSel)
{
	switch( uSel % 29 ) {

		case  0: return SZIR1;
		case  1: return SZIR2;
		case  2: return SZIR3;
		case  3: return SZIR4;
		case  4: return SZIR5;
		case  5: return SZIR6;
		case  6: return SZIR7;
		case  7: return SZIR8;
		case  8: return SZIR9;
		case  9: return SZIR10;
		case 10: return SZIR11;
		case 11: return SZIR12;
		case 12: return SZIR13;
		case 13: return SZIR14;
		case 14: return SZIR15;
		case 15: return SZIR16;
		case 16: return SZIR17;
		case 17: return SZIR18;
		case 18: return SZIR19;
		case 19: return SZIR20;
		case 20: return SZIR21;
		case 21: return SZIR22;
		case 22: return SZIR23;
		case 23: return SZIR24;
		case 24: return SZIR25;
		case 25: return SZIR26;
		case 26: return SZIR27;
		case 27: return SZIR28;
		case 28: return SZIR29;
		}

	return SNOTU;
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Serial Comms Driver
//

// Instantiator

ICommsDriver *	Create_EuroInvensysMini8SerialDriver(void)
{
	return New CEuroInvensysMini8SerialDriver;
	}

// Destructor

CEuroInvensysMini8SerialDriver::~CEuroInvensysMini8SerialDriver(void)
{
	}

CEuroInvensysMini8SerialDriver::CEuroInvensysMini8SerialDriver(void)
{
	m_wID		= 0x4087;

	m_uType		= driverMaster;

	m_Manufacturer	= "Eurotherm";

	m_DriverName	= "Mini8 Serial";

	m_Version	= "1.00";

	m_ShortName	= "Eurotherm Mini8";
	}

// Binding Control

UINT  CEuroInvensysMini8SerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void  CEuroInvensysMini8SerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys TCP Comms Driver
//

// Instantiator

ICommsDriver *	Create_EuroInvensysMini8TCPDriver(void)
{
	return New CEuroInvensysMini8TCPDriver;
	}

CEuroInvensysMini8TCPDriver::CEuroInvensysMini8TCPDriver(void)
{
	m_wID		= 0x353D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Eurotherm";
	
	m_DriverName	= "Mini8 TCP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Eurotherm Mini8";
	}

// Destructor

CEuroInvensysMini8TCPDriver::~CEuroInvensysMini8TCPDriver(void)
{
	}

// Binding Control

UINT CEuroInvensysMini8TCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEuroInvensysMini8TCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CEuroInvensysMini8TCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CEuroInvensysMini8TCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEuroInvensysMini8TCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEuroInvensysMini8AddrDialog, CStdAddrDialog);
		
// Constructor

CEuroInvensysMini8AddrDialog::CEuroInvensysMini8AddrDialog(CEuroInvensysMini8Driver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	m_pGDrv = &Driver;

	m_pEIE	= m_pGDrv->GetEIEPtr();

	SetName(TEXT("CEuroInvensysMini8AddressDlg"));
	}

// Destructor
CEuroInvensysMini8AddrDialog::~CEuroInvensysMini8AddrDialog(void)
{
	}

// Message Map

AfxMessageMap(CEuroInvensysMini8AddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001,  LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(DNAME, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(DNUMB, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(DSEGM, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(4001,  LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CEuroInvensysMini8AddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	LoadList();

	GetDlgItem(DNAME).SetWindowText(TEXT(""));
	GetDlgItem(DNUMB).SetWindowText(TEXT(""));
	GetDlgItem(DSEGM).SetWindowText(TEXT(""));

	if( m_pSpace ) {

		UINT uTable = Addr.m_Ref ? Addr.a.m_Table : m_pSpace->m_uTable;

		BOOL f = m_pGDrv->IsNotStdModbus(uTable);

		GetDlgItem(DNAME).EnableWindow(f);
		GetDlgItem(DNUMB).EnableWindow(f);

		BOOL f1 = uTable == SP_PGS;

		GetDlgItem(DSEGM).EnableWindow(f1);
		GetDlgItem(DSEGT).EnableWindow(f1);

		ClearSelData();
		InitSelects();

		if( f ) {
			if( Addr.m_Ref ) {

				CString Text;

				m_pGDrv->InitItemInfo(uTable);

				if( m_pGDrv->DoExpandAddress(Text, NULL, Addr) ) {

					GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

					InitItemInfo(Addr.a.m_Table, FALSE);

					return FALSE;
					}
				}

			OnSpaceChange(1001, Focus);

			return FALSE;
			}
	
		if( !m_fPart ) {

			if( m_pSpace ) {

				ShowAddress(Addr);

				ShowType(Addr.a.m_Type);

				SetAddressFocus();

				return FALSE;
				}

			return TRUE;
			}

		else {
			LoadType();

			ShowAddress(Addr);

			ShowType(Addr.a.m_Type);

			if( TRUE ) {

				CString Text = m_pSpace->m_Prefix;

				Text += GetAddressText();

				Text += GetTypeText();

				CError   Error(FALSE);

				CAddress Addr;
			
				if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

					CAddress Addr;

					m_pSpace->GetMinimum(Addr);

					ShowAddress(Addr);
					}
				}

			SetAddressFocus();

			return FALSE;
			}
		}

	ClearSelData();

	return FALSE;
	}

// Notification Handlers

void CEuroInvensysMini8AddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CEuroInvensysMini8AddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		m_pEIE->fIsPrgr = m_pSpace->m_uTable == SP_PGS;

		BOOL f = FALSE;

		LoadType();

		if( m_pSpace != pSpace ) {

			f = m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable);

			if( !f ) {

				LoadType();

				m_pSpace      = NULL;

				CStdAddrDialog::OnSpaceChange(uID, Wnd);

				ShowDetails();
				}
			}

		if( f ) {

			ClearAddress();
			ClearDetails();
			ClearSelData();
			InitSelects();

			InitItemInfo(m_pSpace->m_uTable, TRUE);

			GetParamData(GetBoxPosition(DNAME));
			}

		GetDlgItem(DNAME).EnableWindow(f);
		GetDlgItem(DNUMB).EnableWindow(f);

		GetDlgItem(DSEGM).EnableWindow(m_pSpace && m_pSpace->m_uTable == SP_PGS);
		GetDlgItem(DSEGT).EnableWindow(m_pSpace && m_pSpace->m_uTable == SP_PGS);
		}

	else {
		ClearAddress();
		ClearDetails();
		ClearSelData();
		InitSelects();

		m_pSpace = NULL;
		}
	}

void CEuroInvensysMini8AddrDialog::OnComboChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable) ) {

			m_pEIE->uNPos	= GetBoxPosition(DNAME);

			m_pEIE->uBPos	= GetBoxPosition(DNUMB);

			m_pEIE->uSPos	= GetBoxPosition(DSEGM);

			m_pEIE->uBlock	= m_pEIE->uBPos + 1;

			m_pEIE->uSegm	= m_pEIE->uSPos + 1;

			m_pGDrv->InitItemInfo(m_pEIE->uTable);

			InitItemInfo(m_pEIE->uTable, FALSE);

			SetDlgFocus(uID);
			}

		else SetDlgFocus(DPADD);
		}

	else SetDlgFocus(1001);
	}

void CEuroInvensysMini8AddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnTypeChange(uID, Wnd);
	}

// Command Handlers
BOOL CEuroInvensysMini8AddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		BOOL f = m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable);

		if( f ) {

			CString s;

			s.Printf(TEXT("%d_%s"),

				m_pEIE->uOffset,
				m_pGDrv->Get_sName()
				);

			Text += s;
 			}

		else {
			Text += GetAddressText();

			Text += GetTypeText();
			}

		CError   Error(TRUE);

		CAddress Addr;

		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			if( f ) {

				Addr.a.m_Table	= m_pSpace->m_uTable;
				Addr.a.m_Offset	= m_pEIE->uOffset;
				Addr.a.m_Type	= m_pSpace->m_uType;
				Addr.a.m_Extra	= 1;
				}

			else {
				Addr.a.m_Table	= SP_4;
				Addr.a.m_Offset	= 1;
				Addr.a.m_Type	= m_pSpace->m_uType;
				Addr.a.m_Extra	= 0;
				}
			}

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overrides
BOOL CEuroInvensysMini8AddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrByteAsByte:
		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:
		case addrLongAsLong:
		case addrRealAsReal:

			return TRUE;
		}

	return FALSE;
	}

void CEuroInvensysMini8AddrDialog::ShowDetails(void)
{
	if( m_pGDrv->IsNotStdModbus(m_pSpace->m_uTable ) ) {

		ShowMini8Details();
		}

	else {
		CStdAddrDialog::ShowDetails();
		}
	}

// Selecton Handling
void CEuroInvensysMini8AddrDialog::InitItemInfo(UINT uTable, BOOL fNew)
{
	ClearSelData();

	if( fNew ) {

		m_pEIE->uTable	= uTable;

		m_pEIE->uBlock	= 1;

		m_pEIE->uNPos	= 0;

		m_pEIE->uBPos	= 0;

		m_pEIE->uSPos	= 0;

		m_pEIE->uSegm	= 1;

		m_pEIE->uPrgr	= 0;

		m_pGDrv->InitItemInfo(uTable);
		}

	m_pEIE->uNPos %= m_pEIE->uQty;

	LoadNames();

	SetBlockNumbers();

	SetSegmNumbers();

	GetParamData(m_pEIE->uNPos); 

	GetDlgItem(DNUMB).EnableWindow(TRUE);

	BOOL fSeg = uTable == SP_PGS;

	m_pEIE->fIsPrgr = fSeg;

	GetDlgItem(DSEGM).EnableWindow(fSeg);

	SetItemData();

	SetBoxPosition(DNAME, m_pEIE->uNPos);

	SetBoxPosition(DSEGM, (fSeg ? m_pEIE->uSPos : 0));

	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);
	}

void CEuroInvensysMini8AddrDialog::LoadNames(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DNAME);

	ClearBox(DNAME);

	for( UINT i = 0; i < m_pEIE->uQty; i++ ) {

		CString s = m_pGDrv->LoadItemStrings(m_pEIE->uTable, FALSE, i);

		Box.AddString(StripAddr(s));
		}
	}

void CEuroInvensysMini8AddrDialog::SetItemData(void)
{
	SetBoxPosition(DNUMB, m_pEIE->uBlock - 1);

	SetItemAddr();

	ShowMini8Details();
	}

void CEuroInvensysMini8AddrDialog::SetBoxPosition(UINT uID, UINT uPos)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CEuroInvensysMini8AddrDialog::GetBoxPosition(UINT uID)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CEuroInvensysMini8AddrDialog::SetItemAddr(void)
{
	m_pEIE->uAddr = m_pGDrv->GetAddrFromName();	// get modbus address

	CString s;

	s.Printf( "%d", m_pEIE->uAddr);

	GetDlgItem(DMODB).SetWindowText(s);

	m_pEIE->uOffset = m_pEIE->uNPos;	// name list selection

	if( m_pEIE->fIsPrgr ) {

		m_pEIE->uOffset += (m_pEIE->uBlock - 1) * PRGMAX;
		m_pEIE->uOffset += (m_pEIE->uSegm  - 1) * SEGMAX;
		}

	else {
		m_pEIE->uOffset += m_pEIE->uQty * (m_pEIE->uBlock - 1);
		}

	s.Printf( "%d", m_pEIE->uOffset);

	GetDlgItem(DPADD).SetWindowText(s);
	}

void CEuroInvensysMini8AddrDialog::SetBlockNumbers(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DNUMB);

	ClearBox(DNUMB);

	UINT uBlkCt = m_pEIE->uBlkCt;

	for( UINT i = 1; i <= uBlkCt; i++ ) {

		CString sCount;

		sCount.Printf(TEXT("%d"), i);

		Box.AddString(sCount);
		}

	GetDlgItem(DNUMB).EnableWindow(TRUE);

	Box.SetCurSel(m_pEIE->uBlock - 1);
	}

void CEuroInvensysMini8AddrDialog::SetSegmNumbers(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DSEGM);

	ClearBox(DSEGM);

	BOOL f = m_pEIE->fIsPrgr;

	if( f ) {

		for( UINT i = 1; i <= SEGCNT; i++ ) {

			CString sCount;

			sCount.Printf(TEXT("%d"), i);

			Box.AddString(sCount);
			}
		}

	GetDlgItem(DSEGM).EnableWindow(f);

	Box.SetCurSel( f ? m_pEIE->uSegm - 1 : 0 );
	}

void CEuroInvensysMini8AddrDialog::GetParamData(UINT uPos)
{
	UINT uBlock = 1;

	UINT uSelect = GetBoxPosition(DNUMB);

	if( m_pEIE->uBlkCt > 1 ) {

		uBlock = uSelect + 1;
		}

	m_pEIE->uBlock	= uBlock;
	m_pEIE->uAddr	= m_pGDrv->GetAddrFromName();
	m_pEIE->uBPos	= uSelect;
	m_pEIE->uOffset	= uPos;
	m_pEIE->uNPos	= uPos;
	m_pEIE->uSPos	= GetBoxPosition(DSEGM);
	}

// Helpers
void CEuroInvensysMini8AddrDialog::InitSelects(void)
{
	m_pEIE->uNPos = 0;
	m_pEIE->uBPos = 0;
	m_pEIE->uSPos	= 0;
	}

void CEuroInvensysMini8AddrDialog::ClearSelData(void)
{
	GetDlgItem(DPADD).SetWindowText(SNONE);
	GetDlgItem(DMODB).SetWindowText(SNONE);

	ClearBox(DNAME);
	ClearBox(DNUMB);
	}

void CEuroInvensysMini8AddrDialog::ClearBox(CComboBox & ccb)
{
	UINT uCount = ccb.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		ccb.DeleteString(0);
		}
	}

void CEuroInvensysMini8AddrDialog::ClearBox(UINT uID)
{
	ClearBox((CComboBox &)GetDlgItem(uID));
	}

void CEuroInvensysMini8AddrDialog::ShowMini8Details(void)
{
	EIESTRINGS a;

	memcpy(&a, m_pEIE, sizeof(EIESTRINGS));

	UINT uTable = m_pEIE->uTable;

	m_pEIE->uBlock	= 1;

	CString s	= L"Loop:";

	if( m_pEIE->fIsPrgr ) {

		m_pEIE->uBlock	= 1;
		m_pEIE->uSegm	= 1;

		s	= L"Prog:";
		}

	GetDlgItem(DLOOP).SetWindowTextW(s);

	s = m_pGDrv->LoadItemStrings(uTable, FALSE, m_pSpace->m_uMinimum);

	GetDlgItem(3004).SetWindowText(StripAddr(s));

	m_pEIE->uBlock	= m_pEIE->uBlkCt;

	if( m_pEIE->fIsPrgr ) {

		m_pEIE->uSegm = SEGCNT;
		}

	s = m_pGDrv->LoadItemStrings(uTable, FALSE, m_pSpace->m_uMaximum);

	GetDlgItem(3006).SetWindowText(StripAddr(s));

	GetDlgItem(3008).SetWindowText(L"Decimal");

	GetDlgItem(3002).SetWindowText(m_pSpace->GetTypeText());

	memcpy(m_pEIE, &a, sizeof(EIESTRINGS));

	m_pGDrv->LoadItemInfo(uTable);
	m_pGDrv->LoadItemStrings(uTable, TRUE, a.uOffset);
	}

CString CEuroInvensysMini8AddrDialog::StripAddr(CString sSelect)
{
	return sSelect.Left(sSelect.Find('@'));
	}

// End of File
