
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "c3source.hpp"

#include "intern.hxx"

#include "..\build.hxx"

//////////////////////////////////////////////////////////////////////////
//
// System Libraries
//

#pragma comment(lib, "shell32.lib")

//////////////////////////////////////////////////////////////////////////
//
// Editor Colors
//

#define	colorSelectBack		afxColor(Orange2)

#define	colorSelectText		afxColor(BLACK)

#define colorCaretBack		afxColor(WHITE)

#define colorEditorBack		afxColor(WHITE)

#define colorEditorMargin	afxColor(TabFace)

#define colorSyntaxComment	CColor(0, 128, 0)

#define colorSyntaxKeyword	CColor(0, 0, 192)

#define colorSyntaxNumber	CColor(110, 0, 110)

#define colorSyntaxString	CColor(0, 110, 110)

#define colorSyntaxOperator	CColor(160, 0, 0)

#define colorSyntaxIdentifier	afxColor(BLACK)

#define colorSyntaxWas		CColor(255, 0, 0)

#define colorSyntaxDefault	afxColor(BLACK)

//////////////////////////////////////////////////////////////////////////
//
// End of Line
//

#define EOL			'\r'

//////////////////////////////////////////////////////////////////////////
//
// Capture Codes
//

enum {
	captNone,
	captPend,
	captSelect,
	};

//////////////////////////////////////////////////////////////////////////
//
// Token Codes
//

enum {
	tokenBlockComment		= 0x49,
	tokenLineComment		= 0x4A,
	};

// End of File
