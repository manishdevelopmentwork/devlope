
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagTriggerFlag_HPP

#define INCLUDE_TagTriggerFlag_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TagTrigger.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Trigger
//

class DLLNOT CTagTriggerFlag : public CTagTrigger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagTriggerFlag(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL NeedSetpoint(void) const;
		BOOL HasSetpoint(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
