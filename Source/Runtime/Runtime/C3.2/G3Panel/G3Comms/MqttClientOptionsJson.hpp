
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientOptionsJson_HPP

#define	INCLUDE_MqttClientOptionsJson_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "MqttClientOptionsCrimson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// JSON MQTT Client Options
//

class CMqttClientOptionsJson : public CMqttClientOptionsCrimson
{
	public:
		// Constructor
		CMqttClientOptionsJson(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		UINT m_Root;
		UINT m_Code;
	};

// End of File

#endif
