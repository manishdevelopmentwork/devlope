
#include "intern.hpp"

#include "PrimRubyBrush.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Brush
//

// Constructor

CPrimRubyBrush::CPrimRubyBrush(void)
{
	m_Pattern = brushFore;

	m_pColor1 = New CPrimColor(naBack);

	m_pColor2 = New CPrimColor(naFeature);
	}

// Destructor

CPrimRubyBrush::~CPrimRubyBrush(void)
{
	delete m_pColor1;

	delete m_pColor2;
	}

// Initialization

void CPrimRubyBrush::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubyBrush", pData);

	DoLoad(pData);
	}

// Attributes

BOOL CPrimRubyBrush::IsNull(void) const
{
	return m_Pattern == brushNull;
	}

// Operations

void CPrimRubyBrush::SetScan(UINT Code)
{
	SetItemScan(m_pColor1, Code); 
	
	SetItemScan(m_pColor2, Code); 
	}

BOOL CPrimRubyBrush::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		return TRUE;
		}

	return FALSE;
	}

// Drawing

BOOL CPrimRubyBrush::Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( !IsNull() ) {

		CRubyGdiLink link(pGdi);

		CRubyPatternLib *pLib = CUISystem::m_pThis->m_pUI->m_pPatterns;

		PSHADER pShader = pLib->SetGdi( pGdi,
						m_Pattern,
						m_Ctx.m_Color1,
						m_Ctx.m_Color2
						);

		if( fOver )
			link.OutputShade(list, pShader, 0);
		else
			link.OutputShade(list, pShader);

		pLib->Release(pGdi, m_Pattern);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CPrimRubyBrush::DoLoad(PCBYTE &pData)
{
	m_Pattern = GetByte(pData);

	m_pColor1->Load(pData);

	m_pColor2->Load(pData);
	}

// Context Creation

void CPrimRubyBrush::FindCtx(CCtx &Ctx)
{
	Ctx.m_Color1 = m_pColor1->GetColor();

	Ctx.m_Color2 = m_pColor2->GetColor();
	}

// Context Check

BOOL CPrimRubyBrush::CCtx::operator == (CCtx const &That) const
{
	return m_Color1 == That.m_Color1 &&
	       m_Color2 == That.m_Color2 ;
	}

// End of File
