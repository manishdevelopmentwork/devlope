
#include "Intern.hpp"

#include "ModuleManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Module Manager
//

// Decompressor

clink int fastlz_decompress(const void *input, int length, void *output, int maxout);

// Client Hooks

extern bool FindClientImage(PCTXT, PCBYTE &, UINT &);

// Constructor

CModuleManager::CModuleManager(void)
{
	StdSetRef();

	piob->RegisterSingleton("host.modman", 0, this);
	}

// Destructor

CModuleManager::~CModuleManager(void)
{
	// The module manager is deleted via a C++ delete call
	// so we don't want the Release from the revocation to
	// kick off another call to the destructor!

	AddRef();

	piob->RevokeSingleton("host.modman", 0);

	FreeAllModules();
	}

// IUnknown

HRESULT CModuleManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IModuleManager);

	StdQueryInterface(IModuleManager);

	return E_NOINTERFACE;
	}

ULONG CModuleManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CModuleManager::Release(void)
{
	StdRelease();
	}

// Operations

bool CModuleManager::LoadClientModule(PCTXT pName)
{
	UINT hModule;

	return LoadClientModule(hModule, pName);
	}

bool CModuleManager::LoadClientModule(UINT &hModule, PCTXT pName)
{
	if( pName && *pName ) {

		PCBYTE pData;

		UINT   uSize;

		if( FindClientImage(pName, pData, uSize) ) {

			return LoadClientModule(hModule, pName, pData, uSize);
			}

		Error("cannot find client module");
		}

	return false;
	}

bool CModuleManager::LoadClientModule(UINT &hModule, PCTXT pName, PCBYTE pData, UINT uSize)
{
	if( pName && *pName ) {

		CModule *pModule  = New CModule;

		pModule->m_Name   = pName;

		pModule->m_pImage = pData;

		pModule->m_uImage = uSize;

		if( pModule->CopyImage() ) {

			if( pModule->IsValid() ) {

				if( pModule->Fixup() ) {

					(pModule->m_pfnMain)(piob);

					hModule = UINT(m_List.Append(pModule));

					return true;
					}
				}

			pModule->FreeImage();
			}

		delete pModule;

		return false;
		}

	return false;
	}

bool CModuleManager::FreeClientModule(UINT hModule)
{
	if( hModule ) {

		CModule *pModule = m_List[INDEX(hModule)];

		(pModule->m_pfnExit)();

		pModule->FreeImage();

		delete pModule;

		m_List.Remove(INDEX(hModule));

		return true;
		}

	return false;
	}

bool CModuleManager::FreeAllModules(void)
{
	INDEX i;

	while( !m_List.Failed(i = m_List.GetTail()) ) {

		FreeClientModule(UINT(i));
		}

	return true;
	}

// Operations

bool CModuleManager::FindModule(DWORD dwAddr, PTXT pName, DWORD &dwBase)
{
	for( INDEX n = m_List.GetHead(); !m_List.Failed(n); m_List.GetNext(n) ) {

		CModule *pModule = m_List[n];

		if( dwAddr >= pModule->m_pHead->dwBase && dwAddr <= pModule->m_pHead->dwAddrLast ) {

			strcpy(pName, pModule->m_Name);

			dwBase = pModule->m_pHead->dwBase;

			return true;
			}
		}

	return false;
	}

// Implementation

void CModuleManager::Error(PCTXT pText, ...)
{
	va_list va;

	va_start(va, pText);

	AfxTrace("ERROR: ");

	AfxTraceArgs(pText, va);

	AfxTrace("\n");
	}

// Module Operations

bool CModuleManager::CModule::CopyImage(void)
{
	UINT uLimit = 4 * m_uImage;

	m_pData     = PBYTE(malloc(uLimit));

	for(;;) {

		UINT uOutput = fastlz_decompress(m_pImage, m_uImage, m_pData, uLimit);

		if( uOutput && uOutput < uLimit ) {

			m_pData = PBYTE(realloc(m_pData, uOutput));

			m_pHead = (CHeader *) m_pData;

			return true;
			}

		uLimit  = uLimit << 1;

		m_pData = PBYTE(realloc(m_pData, uLimit));
		}
	}

bool CModuleManager::CModule::FreeImage(void)
{
	free(m_pData);

	return true;
	}

bool CModuleManager::CModule::IsValid(void)
{
	if( !strcmp(m_pHead->sMagic, "AEON CM") ) {

		if( m_pHead->wMajorVersion == 1 && m_pHead->wMinorVersion == 0 ) {

			if( m_pHead->dwFlags & 1 ) {
	
				m_dwLink  = m_pHead->dwBase;

				UINT fix1 = m_pHead->dwAddrFix2 - m_pHead->dwAddrFix1;
					
				UINT fix2 = m_pHead->dwAddrFix3 - m_pHead->dwAddrFix2;

				if( fix1 == fix2 ) {

					return true;
					}

				Error("the relocation segments do not match in size");

				return false;
				}

			Error("the image has no relocation information");

			return false;
			}

		Error("the image version is not supported");

		return false;
		}

	Error("the image does not contain an Aeon client module");

	return false;
	}

bool CModuleManager::CModule::Fixup(void)
{
	m_dwBase = DWORD(m_pData);

	m_dwLink = DWORD(m_pHead->dwBase);

	if( FixVirt() && FixCode() && FixData() && FixHead() ) {

		m_pfnMain = MAINPTR(m_pData + 0);

		m_pfnExit = EXITPTR(m_pData + 4);

		ProcSyncCaches();

		return true;
		}

	return false;
	}

// Module Implementation

bool CModuleManager::CModule::FixVirt(void)
{
	UINT   x = 0;

	UINT   s = m_pHead->dwAddrFix1 - m_pHead->dwAddrVTab;

	UINT   c = s / sizeof(DWORD);

	PDWORD p = PDWORD(m_pData + m_pHead->dwAddrVTab - m_dwLink);

	bool   u = false;

	for( UINT n = 0; n < c; n++ ) {

		// For vtable relocations, we have to watch out for
		// pointer offset values, which are negative. Since
		// we do not have the relocation records, we have to
		// apply a heuristic to figure out what really needs
		// relocating and what does not. It seems to work!

		// Note that this section can also contain RTTI if we
		// are using that option, so we have to watch out for
		// small integer values that represent type flags. We
		// skip those by nothing the zero high-order word.

		if( ((p[n] & 0xFF000000) == (m_dwLink & 0xFF000000)) && ((p[n] & 0xF0000000) ^ 0xF0000000) ) {

			if( (u || p[n] % 4 == 0) && p[n] >= m_pHead->dwAddrVTab && p[n] < m_pHead->dwAddrData ) {

				p[n] -= m_dwLink;

				p[n] += m_dwBase;

				x++;
				}
			else {
				Error("unexpected vtable entry 0x%8.8X", p[n]);

				return false;
				}
			}
		}

	return true;
	}

bool CModuleManager::CModule::FixCode(void)
{
	UINT   x = 0;

	UINT   s = m_pHead->dwAddrFix2 - m_pHead->dwAddrFix1;

	UINT   c = s / sizeof(DWORD);

	PDWORD p = PDWORD(m_pData + m_pHead->dwAddrFix1 - m_dwLink);

	PDWORD d = PDWORD(m_pData + m_pHead->dwAddrFix2 - m_dwLink);

	bool   u = true;

	for( UINT n = 0; n < c; n++ ) {

		if( p[n] > 0 ) {

			if( (u || p[n] % 4 == 0) && p[n] >= m_pHead->dwBase && p[n] <= m_pHead->dwAddrLast ) {

				if( d[n] ) {

					Error("unexpected value in fixup segment");

					return false;
					}

				p[n] -= m_dwLink;

				p[n] += m_dwBase;

				d[n]  = p[n];

				x++;
				}
			else {
				Error("unexpected code fixup 0x%8.8X", p[n]);

				return false;
				}
			}
		}

	return true;
	}

bool CModuleManager::CModule::FixData(void)
{
	UINT   x = 0;

	UINT   s = m_pHead->dwAddrText - m_pHead->dwAddrFix3;

	UINT   c = s / sizeof(DWORD);

	PDWORD p = PDWORD(m_pData + m_pHead->dwAddrFix3 - m_dwLink);

	bool   u = true;

	for( UINT n = 0; n < c; n++ ) {

		// For data relocations, we could have all sorts of
		// stuff mixed in here and we can't easily tell the
		// relocatable stuff from the rest as we don't have
		// the relocation records. We apply a heuristic to
		// figure out what really needs relocating and what
		// does not. It seems to work, especially when the
		// original file is base at a distinctive address.

		if( p[n] > 0 ) {

			if( (u || p[n] % 4 == 0) && p[n] >= m_pHead->dwAddrFix3 && p[n] <= m_pHead->dwAddrLast ) {

				p[n] -= m_dwLink;

				p[n] += m_dwBase;

				x++;
				}
			}
		}

	return true;
	}

bool CModuleManager::CModule::FixHead(void)
{
	m_dwExit = m_pHead->dwExit - m_dwLink + m_dwBase;

	if( FixJump(0, m_pHead->dwEntry) && FixJump(1, m_pHead->dwExit) ) {

		m_pHead->dwAddrLast -= m_pHead->dwBase;

		m_pHead->dwAddrLast += m_dwBase;

		m_pHead->dwBase   = m_dwBase;

		m_pHead->dwFlags &= ~1;

		return true;
		}

	return false;
	}

bool CModuleManager::CModule::FixJump(UINT n, DWORD &dwAddr)
{
	if( dwAddr ) {

		if( dwAddr - m_dwLink < 32 * 1024 * 1024 ) {

			// Jump via "B dwAddr"

			dwAddr = (0xEA000000 | (((dwAddr - m_dwLink) >> 2) - 2 - n));

			return true;
			}

		Error("entry table fixup out of range %u %8.8X %8.8X", n, dwAddr, m_dwLink);

		return false;
		}

	// Return via "BX LR"

	dwAddr = 0xE12FFF1E;

	return true;
	}

void CModuleManager::CModule::Error(PCTXT pText, ...)
{
	va_list va;

	va_start(va, pText);

	AfxTrace("ERROR: ");

	AfxTraceArgs(pText, va);

	AfxTrace("\n");
	}

// End of File
