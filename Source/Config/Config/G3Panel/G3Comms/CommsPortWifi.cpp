
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPortWifi.hpp"

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDeviceList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Wifi Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortWifi, CCommsPort);

// Constructor

CCommsPortWifi::CCommsPortWifi(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding = bindWifi;
	}

// End of File
