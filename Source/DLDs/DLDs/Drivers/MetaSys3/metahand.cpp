#include "intern.hpp"

#include "metasys3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Metasys N2 System Port Handler
//

// Constructor

CMetaSysHandler::CMetaSysHandler(IHelper *pHelper, CMetaSysN2SystemDriver3 * pDriver)
{
	StdSetRef();

	m_pHelper     = pHelper;

	m_pDriver     = pDriver;

	m_pPort       = NULL;

	m_fOpen       = FALSE;

	m_RxPtr	      = 0;

	m_TxPtr	      = 0;

	m_TxCount     = 0;

	m_pExtra      = NULL;

	m_pDev	      = NULL;

	CTEXT Hex[]   = "0123456789ABCDEF";

	m_pHex	      = Hex;
	
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	}

// Destructor

CMetaSysHandler::~CMetaSysHandler(void)
{
	m_pExtra->Release();
	}

// IUnknown

HRESULT CMetaSysHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CMetaSysHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CMetaSysHandler::Release(void)
{
	StdRelease();
	}

// Binding

void MCALL CMetaSysHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

// Event Handlers

void MCALL CMetaSysHandler::OnOpen(CSerialConfig const &Config)
{
	m_fOpen      = TRUE;

	m_fSOM	     = FALSE;
	}

void MCALL CMetaSysHandler::OnClose(void)
{
	m_fOpen = FALSE;
	}

void MCALL CMetaSysHandler::OnTimer(void)
{
	}

BOOL MCALL CMetaSysHandler::OnTxData(BYTE &bData)
{
	if( m_TxPtr < m_TxCount ) {
		
		bData = m_Tx[m_TxPtr++];

		return TRUE;
		}
	
	return FALSE;
	}

void MCALL CMetaSysHandler::OnTxDone(void)
{
	}

void MCALL CMetaSysHandler::OnRxData(BYTE bData)
{
	if( !m_fSOM ) {
		
		if( bData == '>' ) {

			m_fSOM = TRUE;

			m_Check = 0;
			}
		return;
		}

	if( bData != CR ) {

		m_Rx[m_RxPtr] = bData;

		m_RxPtr++;

		return;
		}

	if( DoProcess() && m_pDev ) {

		HandleFrame();
		}

	m_fSOM = FALSE;

	m_RxPtr = 0;
	}

void MCALL CMetaSysHandler::OnRxDone(void)
{
	}

// Driver Access

PBYTE CMetaSysHandler::GetData(BYTE bDevice, UINT uTable, UINT uOffset)
{
	CDevice * pDev = m_pDriver->FindDevice(bDevice);

	if( pDev ) {

		return &pDev->m_Table[uTable - 1].m_Data[(uOffset - 1) * 4];
		}

	return NULL;
	}

BOOL CMetaSysHandler::SetData(BYTE bDevice, UINT uTable, UINT uOffset, PBYTE pBytes, UINT uCount)
{
	CDevice * pDev = m_pDriver->FindDevice(bDevice);

	if( pDev ) {

		UINT uAdd  = (uOffset - 1) * 4;

		UINT uSize = uCount * 4;

		memcpy(pDev->m_Table[uTable - 1].m_Data + uAdd, pBytes, uSize);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CMetaSysHandler::DoProcess(void)
{	
	if( m_RxPtr ) {

		if( m_Check == 0 ) {

			for( UINT u = 0; u < m_RxPtr - 2; u++ ) {

				m_Check	+= m_Rx[u];
				}

			m_Check = m_Check % 0x100;
			}

		BOOL fCheck = m_pHex[m_Check / 0x10] == m_Rx[m_RxPtr - 2] &&
			      m_pHex[m_Check % 0x10] == m_Rx[m_RxPtr - 1];

		if( DoListen() ) {

			if( !fCheck ) {

				HandleError(0x02);
				}
		
			return fCheck;
			}

		return FALSE;
		} 

	return TRUE;
	}

BOOL CMetaSysHandler::DoListen(void)
{
	if( m_RxPtr ) {

		UINT uPtr = 0;

		BYTE bDrop = GetChar2(uPtr);

		m_pDev = m_pDriver->FindDevice(bDrop);

		return m_pDev ? TRUE : FALSE;
		}

	return FALSE;
	}

void CMetaSysHandler::Tx(void)
{
	m_TxCount = m_TxPtr;

	m_TxPtr = 1;

	TxDelay();

	m_pPort->Send(m_Tx[0]);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_TxCount; u++ ) {

		AfxTrace("%2.2x ", m_Tx[u]);
		}   */ 
	}

void CMetaSysHandler::HandleFrame(void)
{
/*	AfxTrace("\nRx %u : ", m_RxPtr);

	for( UINT u = 0; u < m_RxPtr; u++ ) {

		AfxTrace("%2.2x ", m_Rx[u]);
		}  */
	
	UINT uPtr = 2;

	UINT uOne = GetChar1(uPtr);

	if( IsIdentifyDeviceTypeCmd(uOne) ) {

		HandleIdentifyDeviceCmd();

		return;
		}
	
	if( !IsOnline() ) {

		HandleError(0x00);

		return;
		}
	
	UINT uTwo = GetChar1(uPtr);

	if( IsSynchTimeCmd(uOne, uTwo) ) {

		HandleSynchTimeCmd();

		return;
		}

	if( IsPollNoAck(uOne, uTwo) ) {

		HandlePollNoAck();

		return;
		}

	if( IsPollWithAck(uOne, uTwo) ) {

		HandlePollWithAck();

		return;
		}

	if( IsWriteSingle(uOne) ) {

		HandleWriteSingle(uTwo);

		return;
		}

	if( IsReadSingle(uOne) ) {

		HandleReadSingle(uTwo);

		return;
		}

	if( IsWriteMulti(uOne, uTwo) ) {

		HandleWriteMulti();

		return;
		}

	if( IsReadMulti(uOne, uTwo) ) {

		HandleReadMulti();

		return;
		}

	if( IsOverride(uOne, uTwo) ) {

		HandleOverride();

		return;
		}

	if( IsOverrideReleaseReq(uOne, uTwo) ) {

		HandleOverrideRelease();

		return;
		}

	if( IsUploadRequest(uOne, uTwo) ) {

		HandleUploadRequest();

		return;
		}

	if( IsUploadRecord(uOne, uTwo) ) {

		HandleUploadRecord();

		return;
		}

	if( IsUploadComplete(uOne, uTwo) ) {

		HandleUploadComplete();

		return;
		}

	if( IsDownloadRequest(uOne, uTwo) ) {

		HandleDownloadRequest();

		return;
		}

	if( IsDownloadRecord(uOne, uTwo) ) {

		HandleDownloadRecord();

		return;
		}

	if( IsDownloadComplete(uOne, uTwo) ) {

		HandleDownloadComplete();

		return;
		}

	if( IsWarmStart(uOne, uTwo) ) {

		HandleWarmStart();

		return;
		}

	if( IsStatusUpdate(uOne, uTwo) ) {

		HandleStatusUpdate();

		return;
		}

	if( IsReadMemoryCmd(uOne, uTwo) ) {

		HandleReadMemoryCmd();

		return;
		}		

	HandleError(0x01);
       	}

void CMetaSysHandler::HandleSynchTimeCmd(void)
{	
	UINT uPtr = 4;

	UINT uYear  = GetChar4(uPtr);

	UINT uMonth = GetChar2(uPtr);

	UINT uDayM  = GetChar2(uPtr);

	UINT uDayW  = GetChar2(uPtr);

	UINT uHours = GetChar2(uPtr);

	UINT uMins  = GetChar2(uPtr);

	UINT uSecs  = GetChar2(uPtr);

	DWORD t = 0;

	t += Time(uHours, uMins, uSecs);

	t += Date(uYear, uMonth, uDayM);
	
	m_pExtra->SetNow(t);

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandlePollNoAck(void)
{
	// No COS
	
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandlePollWithAck(void)
{
	// No COS
	
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandleIdentifyDeviceCmd(void)
{
	Start('A');

	PutChar2(0x10);

	AddCheck();

	Tx();
	}

void CMetaSysHandler::HandleWriteSingle(UINT uRegion)
{
	UINT uLook = FindLookup(uRegion);
	
	UINT uPtr = 4;

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = 0;

	if( Attrib[uLook] > 2 ) {

		uAtr = GetChar2(uPtr);
		}
	
	SetData(uLook, uObj, uAtr, uPtr, 1);

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandleReadSingle(UINT uRegion)
{
	UINT uLook = FindLookup(uRegion);

	UINT uPtr = 4;

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = GetChar2(uPtr);

	FindAttribute(uLook, uAtr);

	UINT uCount = FindCount(uLook, uAtr);
	
	SendData(uLook, uObj, uAtr, uCount);
	}

void CMetaSysHandler::HandleWriteMulti(void)
{
	// No multi writes possible for current values
	
	// Acknowledge only

	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandleReadMulti(void)
{
	UINT uPtr = 4;
	
	UINT uRegion = GetChar2(uPtr);

	UINT uLook = FindLookup(uRegion);

	UINT uObj = GetChar2(uPtr);

	UINT uAtr = 1;
	
	UINT uCount = Attrib[uLook];
	
	SendData(uLook, uObj, uAtr, uCount);
	}

void CMetaSysHandler::HandleOverride(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandleOverrideRelease(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void CMetaSysHandler::HandleUploadRequest(void)
{
	Start('A');

	PutChar2(0);

	AddCheck();

	Tx();
	}

void CMetaSysHandler::HandleUploadRecord(void)
{
	Start('A');

	PutChar2(0);

	PutChar4(0);

	PutChar4(0);

	AddCheck();

	Tx();
	}

void  CMetaSysHandler::HandleUploadComplete(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void  CMetaSysHandler::HandleDownloadRequest(void)
{
	Start('A');

	PutChar2(0);

	AddCheck();

	Tx();
	}

void  CMetaSysHandler::HandleDownloadRecord(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void  CMetaSysHandler::HandleDownloadComplete(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void  CMetaSysHandler::HandleWarmStart(void)
{
	Start('A');

	AddByte(CR);

	Tx();
	}

void  CMetaSysHandler::HandleStatusUpdate(void)
{
	Start('A');

	PutChar4(0);

	PutChar4(0);

	PutChar4(0);

	PutChar4(0);

	PutChar4(0);

	PutChar4(0);

	AddCheck();

	Tx();
	}

void CMetaSysHandler::HandleReadMemoryCmd(void)
{
	UINT uPos = 13;
	
	UINT uMem = GetChar2(uPos);

	MakeMin(uMem, 64);

	Start('A');

	for( UINT u = 0; u < uMem; u++ ) {

		PutChar2(0);
		}

	AddCheck();

	Tx();
	}

 
void CMetaSysHandler::HandleError(UINT uCode)
{
	Start('N');

	PutChar2(uCode);

	AddByte(CR);

	Tx();
	}

// Data Handling

void CMetaSysHandler::SetData(UINT uLook, UINT uObj, UINT uAtr, UINT uPtr, UINT uCount)
{
	switch( Table[uLook] ) {

		case 1:  
		case 2:  SetAnalogs	(uLook, uObj, uPtr);	break;
		case 3:  
		case 4:	 SetBinarys	(uLook, uObj, uPtr);	break;
		case 5:  
		case 6:
		case 7:	 SetInternals	(uLook, uObj, uPtr);	break;
	  	}
	}

void CMetaSysHandler::SetAnalogs(UINT uLook, UINT uObj, UINT uPtr)
{	
	// Current Value is read only
	}

void CMetaSysHandler::SetBinarys(UINT uLook, UINT uObj, UINT uPtr)
{
	// Current Value is read only
	}

void CMetaSysHandler::SetInternals(UINT uLook, UINT uObj, UINT uPtr)
{
	UINT uTable = Table[uLook];

	PBYTE pData = m_pDev->m_Table[uLook].m_Data + uObj * 4;

	DWORD dwData = 0;

	switch( uTable ) {

		case 7:	dwData = GetChar2(uPtr);	break;
		case 6: dwData = GetChar4(uPtr);	break;
		case 5: dwData = GetChar8(uPtr);	break;
		}
	
	memcpy(pData, PBYTE(&dwData), sizeof(DWORD));
	}

void CMetaSysHandler::SendData(UINT uLook, UINT uObj, UINT uAtr, UINT uCount)
{
	switch( Table[uLook]) {

		case 1:  
		case 2:  SendAnalogs	  (uLook, uObj, uAtr, uCount);	break;
		case 3:  SendBinaryInputs (uLook, uObj, uAtr, uCount);	break;
		case 4:	 SendBinaryOutputs(uLook, uObj,	uAtr, uCount);	break;
		case 5:  
		case 6:
		case 7:	 SendInternals	  (uLook, uObj, uAtr, uCount);	break;
		}
	}

void CMetaSysHandler::SendAnalogs(UINT uLook, UINT uObj, UINT uAtr, UINT uCount)
{
	Start('A');

	DWORD Data = PDWORD(m_pDev->m_Table[uLook].m_Data + uObj * 4)[0];

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr <= 2 ) {

			PutChar2(0);

			continue;
			}

		if( uAtr == 3 ) {

			PutChar8(Data);
			}
		else {
			PutChar8(0);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysHandler::SendBinaryInputs(UINT uLook, UINT uObj, UINT uAtr, UINT uCount)
{
	Start('A');

	DWORD Data = PDWORD(m_pDev->m_Table[uLook].m_Data + uObj * 4)[0];

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr < 2 ) {

			PutChar2(0);
			}
		
		else if( uAtr == 2 ) {

			PutChar2((Data << 6) & 0x40);
			}
		
		else if( uAtr == 3 ) {

			PutChar4(0);
			}
		else {		   
			PutChar8(0);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysHandler::SendBinaryOutputs(UINT uLook, UINT uObj, UINT uAtr, UINT uCount)
{
	Start('A');

	DWORD Data = PDWORD(m_pDev->m_Table[uLook].m_Data + uObj * 4)[0];

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr < 2 ) {

			PutChar2(0);
			}
		
		else if( uAtr == 2 ) {

			PutChar2((Data << 6) & 0x40);
			}
		else {		
			PutChar4(0);
			}
		}
		
	AddCheck();

	Tx();
	}

void CMetaSysHandler::SendInternals(UINT uLook, UINT uObj, UINT uAtr, UINT uCount)
{
	Start('A');

	DWORD Data = PDWORD(m_pDev->m_Table[uLook].m_Data + uObj * 4)[0];

	for( UINT u = 0; u < uCount; u++, uAtr++ ) {

		if( uAtr == 1 ) {

			PutChar2(0);

			continue;
			}
		   
		switch( Table[uLook] ) {

			case 7: PutChar2(Data);		break;
			case 6: PutChar4(Data);		break;
			case 5:	PutChar8(Data);		break;
			}
		}
		
	AddCheck();

	Tx();
	}

// Frame Building

void CMetaSysHandler::Start(UINT uByte)
{
	m_TxPtr = 0;

	m_Check = 0;

	AddByte(uByte);
	}

void CMetaSysHandler::AddByte(BYTE bByte)
{
	m_Tx[m_TxPtr] = bByte;
	
	m_TxPtr++;

	if( bByte == 'A' && m_TxPtr == 1 ) {

		return;
		}

	m_Check += bByte;
	}

void CMetaSysHandler::AddCheck(void)
{
	PutChar2(m_Check % 256);

	AddByte(CR);
	}

void CMetaSysHandler::PutChar1(DWORD Data)
{
	AddByte(m_pHex[Data & 0xF]);	
	}

void CMetaSysHandler::PutChar2(DWORD Data)
{
	AddByte(m_pHex[(Data & 0xFF) / 0x10]);

	AddByte(m_pHex[(Data & 0xFF) % 0x10]);
	}

void CMetaSysHandler::PutChar4(DWORD Data)
{		
	PutChar2((Data >> 8) & 0xFF);
	
	PutChar2(Data & 0xFF);
	}

void CMetaSysHandler::PutChar8(DWORD Data)
{
	PutChar4(HIWORD(Data));

	PutChar4(LOWORD(Data));
	} 

// Frame Access

UINT CMetaSysHandler::GetChar1(UINT& uPos)
{
	UINT uChar = m_Rx[uPos];

	uPos++;

	if( uChar >= '0' && uChar <= '9' ) {

		return (uChar - '0') & 0xF;
		}

	return (uChar - '@' + 9) & 0xF;
      	}

UINT CMetaSysHandler::GetChar2(UINT& uPos, BOOL fHex)
{
	UINT uChar = GetChar1(uPos);

	fHex ? uChar <<= 4 : uChar *= 10;

	uChar += GetChar1(uPos);

	return uChar;
	}

UINT CMetaSysHandler::GetChar4(UINT& uPos, BOOL fHex)
{
	UINT uHi = GetChar2(uPos, fHex);

	UINT uLo = GetChar2(uPos, fHex);
	
	return MAKEWORD(uLo, uHi);
	}

UINT CMetaSysHandler::GetChar8(UINT& uPos)
{
	UINT uHi = GetChar4(uPos);

	UINT uLo = GetChar4(uPos);
	
	return MAKELONG(uLo, uHi);
	}

// Helpers

BOOL CMetaSysHandler::IsOnline(void)
{
	return m_pDev->m_fOnline;
	}

BOOL CMetaSysHandler::IsSynchTimeCmd(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 0;
 	}

BOOL CMetaSysHandler::IsPollNoAck(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 4;
	}

BOOL CMetaSysHandler::IsPollWithAck(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 5;
	}

BOOL CMetaSysHandler::IsIdentifyDeviceTypeCmd(UINT uCmd)
{
	if( !IsOnline() ) {

		m_pDev->m_fOnline = ( uCmd == 0xF && m_RxPtr == 5 );
		}

	if( m_RxPtr == 3 || ( uCmd == 0xF && m_RxPtr == 5 ) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CMetaSysHandler::IsWriteSingle(UINT uCmd)
{
	return uCmd == 2;
	}

BOOL CMetaSysHandler::IsReadSingle(UINT uCmd)
{
	return uCmd == 1;
	}

BOOL CMetaSysHandler::IsWriteMulti(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 7;
	}

BOOL CMetaSysHandler::IsReadMulti(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 8;
	}

BOOL CMetaSysHandler::IsOverride(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 2;
	}

BOOL CMetaSysHandler::IsOverrideReleaseReq(UINT uCmd, UINT uSub)
{
	return uCmd == 7 && uSub == 3;
	}

BOOL CMetaSysHandler::IsUploadRequest(UINT uCmd, UINT uSub)
{
	return uCmd == 8 && (uSub == 0 || uSub == 1);
	}

BOOL CMetaSysHandler::IsUploadRecord(UINT uCmd, UINT uSub)
{
	return uCmd == 8 && uSub == 3;
	}

BOOL CMetaSysHandler::IsUploadComplete(UINT uCmd, UINT uSub)
{
	return uCmd == 8 && uSub == 4;
	}

BOOL CMetaSysHandler::IsDownloadRequest(UINT uCmd, UINT uSub)
{
	return uCmd == 8 && (uSub == 0 || uSub == 1);
	}

BOOL CMetaSysHandler::IsDownloadRecord(UINT uCmd, UINT uSub)
{
	return uCmd == 8 && uSub == 3;
	}

BOOL CMetaSysHandler::IsDownloadComplete(UINT uCmd, UINT uSub)
{
	return uCmd == 8 && uSub == 4;
	}

BOOL CMetaSysHandler::IsWarmStart(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 8;
	}

BOOL CMetaSysHandler::IsStatusUpdate(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 9;
	}

BOOL CMetaSysHandler::IsReadMemoryCmd(UINT uCmd, UINT uSub)
{
	return uCmd == 0 && uSub == 1;
	}

UINT CMetaSysHandler::FindLookup(UINT uRegion)
{
	for( UINT u = 0; u < elements(Table); u++ ) {

		if( uRegion >= UINT(Region[u]) && uRegion <= UINT(Region[u] + Span[u]) ) {

			return u;
			}
		}

	return NOTHING;
	}

void CMetaSysHandler::FindAttribute(UINT uLook, UINT &uAtr)
{
	if( ( Table[uLook] == 1 || Table[uLook] == 2 )  && uAtr == 3 ) {

		uAtr = 2;
		}

	if( Table[uLook] >= 5 && Table[uLook] <= 7 ) {

		uAtr = 1;
		}
	}

UINT CMetaSysHandler::FindCount(UINT uLook, UINT uAtr)
{
	if( ( Table[uLook] == 1 || Table[uLook] == 2 ) && uAtr == 2 ) {

		return 2;
		}

	if( Table[uLook] >= 5 && Table[uLook] <= 7 ) {

		return 2;
		}		   

	return 1;
	}

BOOL CMetaSysHandler::TxDelay(void)
{
	UINT uCount = 1000;

	while( uCount -= 1 ) GetTickCount();

	return TRUE;
	}

// End of File

