
#ifndef INCLUDE_DO_MORE_UDP

#define INCLUDE_DO_MORE_UDP

#include "../DoMoreSerial/DoMoreBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Automation Direct Do-More PLC UDP Driver
//

// Host Application Protocol constants

enum {
	hapACK   = 0x20,
	hapNACK  = 0x21,
	hapReply = 0x22
	};

class CDoMoreUDPDriver : public CDoMoreBaseDriver
{
	public:		
		// Constructor
		CDoMoreUDPDriver(void);

		// Destructor
		~CDoMoreUDPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		//// Entry Points
		DEFMETH(CCODE) Ping (void);
		/*DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);*/

	protected:

		struct CContext : public CBaseCtx
		{
			DWORD	m_IP;
			UINT	m_uPort;
			WORD	m_wTrans;
			BOOL	m_fKeep;
			UINT	m_uTime1;
			UINT	m_uTime2;
			UINT	m_uTime3;
			UINT	m_uLast;

			ISocket	 * m_pSock;
			};

		// Data Members
		CContext	* m_pCtx;
		UINT		  m_uKeep;
		
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL RecvCheckAck(void);
		BOOL CheckTransID(WORD wTransID);
		BOOL Transact(void);
		
		// Frame Building
		void StartHeader(void);
		BOOL StartRequest(CMxAppRequest &Req);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);		
	};

#endif

// End of File
