
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Type Library
//

// Constructors

CTypeLib::CTypeLib(void)
{
	InitFrom(NULL, FALSE);
	}

CTypeLib::CTypeLib(CTypeLib const &That)
{
	InitFrom(That.m_pTypeLib, TRUE);
	}

CTypeLib::CTypeLib(ITypeLib *pTypeLib)
{
	InitFrom(pTypeLib, TRUE);
	}

CTypeLib::CTypeLib(ITypeLib *pTypeLib, BOOL fAdd)
{
	InitFrom(pTypeLib, fAdd);
	}

// Destructors

CTypeLib::~CTypeLib(void)
{
	CloseLib();
	}

// Assignment Operators

CTypeLib const & CTypeLib::operator = (CTypeLib const &That)
{
	CloseLib();

	InitFrom(That.m_pTypeLib, TRUE);

	return *this;
	}

CTypeLib const & CTypeLib::operator = (ITypeLib *pTypeLib)
{
	CloseLib();

	InitFrom(pTypeLib, FALSE);

	return *this;
	}

// Conversion

CTypeLib::operator ITypeLib * (void) const
{
	return m_pTypeLib;
	}

CTypeLib::operator BOOL (void) const
{
	return m_pTypeLib != NULL;
	}

CTypeLib::operator ! (void) const
{
	return m_pTypeLib == NULL;
	}

// Attributes

CGuid CTypeLib::GetGuid(void)
{
	GetLibAttr();
	
	return m_pAttr->guid;
	}

LCID CTypeLib::GetLcid(void)
{
	GetLibAttr();

	return m_pAttr->lcid;
	}

WORD CTypeLib::GetFlags(void)
{
	GetLibAttr();

	return m_pAttr->wLibFlags;
	}

CString CTypeLib::GetVersion(void)
{
	GetLibAttr();

	WORD wMaj = m_pAttr->wMajorVerNum;

	WORD wMin = m_pAttr->wMinorVerNum;

	return CPrintf(L"%u.%u", Max(1u, wMaj), wMin);
	}

CString CTypeLib::GetName(void)
{
	AfxAssert(m_pTypeLib);

	BSTR bstr;

	CheckAndThrow( m_pTypeLib->GetDocumentation( -1,
						     &bstr,                
						     NULL,
						     NULL,  
						     NULL
						     ));

	return CBSTR(bstr);
	}

CString CTypeLib::GetDesc(void)
{
	AfxAssert(m_pTypeLib);

	BSTR bstr;

	CheckAndThrow( m_pTypeLib->GetDocumentation( -1,
						     NULL,                
						     &bstr,
						     NULL,  
						     NULL
						     ));

	return CBSTR(bstr);
	}

CFilename CTypeLib::GetFilename(void)
{
	return m_Filename;
	}

// Enumerators

CTypeInfo CTypeLib::EnumComponents(UINT n)
{
	return Enum(n, TKIND_COCLASS);
	}

CTypeInfo CTypeLib::EnumInterfaces(UINT n)
{
	return Enum(n, TKIND_INTERFACE);
	}

CTypeInfo CTypeLib::EnumDispatchs(UINT n)
{
	return Enum(n, TKIND_DISPATCH);
	}

// Type Info

UINT CTypeLib::GetTypeInfoCount(void)
{
	AfxAssert(m_pTypeLib);

	return m_pTypeLib->GetTypeInfoCount();
	}

TYPEKIND CTypeLib::GetTypeInfoType(UINT uIndex)
{
	AfxAssert(m_pTypeLib);
	
	TYPEKIND t;
	
	CheckAndThrow(m_pTypeLib->GetTypeInfoType(uIndex, &t)); 

	return t;
	}

CTypeInfo CTypeLib::GetTypeInfo(UINT uIndex)
{
	AfxAssert(m_pTypeLib);

	ITypeInfo *pInfo = NULL;
	
	CheckAndThrow(m_pTypeLib->GetTypeInfo(uIndex, &pInfo));

	return CTypeInfo(pInfo, FALSE);
	}

// Loading

void CTypeLib::Load(CFilename const &File)
{
	CloseLib();
	
	m_Filename = File;
	
	CheckAndThrow( LoadTypeLibEx( File, 
				      REGKIND_NONE,
				      &m_pTypeLib
				      ));
	}

void CTypeLib::Load(CFilename const &File, UINT uTypeLib)
{
	CloseLib();
	
	m_Filename = File;
	
	CString Text = File + L'\\' + CPrintf(L"%u", uTypeLib);

	CheckAndThrow( LoadTypeLibEx( Text, 
				      REGKIND_NONE,
				      &m_pTypeLib
				      ));
	}

void CTypeLib::Load(CGuid const &LibID, DWORD dwVersion)
{
	CloseLib();
	
	CheckAndThrow( LoadRegTypeLib( LibID,             
				       HIWORD(dwVersion),
				       LOWORD(dwVersion),  
				       GetUserDefaultLCID(),                 
				       &m_pTypeLib
				       ));
	
	BSTR bstr;

	CheckAndThrow( QueryPathOfRegTypeLib( LibID,          
					      HIWORD(dwVersion),
					      LOWORD(dwVersion),
					      GetUserDefaultLCID(),                 
					      &bstr
					      ));
	
	m_Filename = CBSTR(bstr);
	}

// Library Attributes

void CTypeLib::GetLibAttr(void)
{
	if( !m_pAttr ) {
	
		AfxAssert(m_pTypeLib);

		CheckAndThrow(m_pTypeLib->GetLibAttr(&m_pAttr));
		}
	}

void CTypeLib::FreeLibAttr(void)
{
	if( m_pAttr ) {

		AfxAssert(m_pTypeLib);
		
		m_pTypeLib->ReleaseTLibAttr(m_pAttr);

		m_pAttr = NULL;
		}
	}

// Enumerator

CTypeInfo CTypeLib::Enum(UINT n, TYPEKIND Type)
{
	UINT uIndex = 0;

	while( uIndex < GetTypeInfoCount() ) {

		if( GetTypeInfoType(uIndex) == Type ) {

			if( !n )
				return GetTypeInfo(uIndex);
			n--;
			}

		uIndex++;
		}

	return CTypeInfo();
	}

// Implementation

void CTypeLib::InitFrom(ITypeLib *pTypeLib, BOOL fAdd)
{
	m_pAttr = NULL;
	
	if( fAdd && pTypeLib ) {

		m_pTypeLib = pTypeLib;

		m_pTypeLib->AddRef();
		
		return;
		}

	m_pTypeLib = pTypeLib;
	}

void CTypeLib::CloseLib(void)
{
	FreeLibAttr();

	AfxRelease(m_pTypeLib);

	m_pTypeLib = NULL;
	}

// End of File
