
#include "Intern.hpp"

#include "UITranslation.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "LangManager.hpp"
#include "TransEditCtrl.hpp"
#include "TransFlagCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Translation Editor
//

// Dynamic Class

AfxImplementDynamicClass(CUITranslation, CUIControl);

// Constructor

CUITranslation::CUITranslation(void)
{
	m_dwStyle     = ES_AUTOHSCROLL;

	m_pSlotLayout = NULL;

	m_pFlagLayout = NULL;

	m_pNameLayout = NULL;

	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pSlotCtrl   = New CStatic;

	m_pFlagCtrl   = New CTransFlagCtrl;

	m_pNameCtrl   = New CStatic;

	m_pDataCtrl   = New CTransEditCtrl(this);
	}

// Core Overridables

void CUITranslation::OnBind(void)
{
	CUIElement::OnBind();

	m_pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	m_pLang   = m_pSystem->m_pLang;

	if( !m_pText->HasFlag(textEdit) ) {

		m_dwStyle |= ES_READONLY;
		}

	if( (m_uSlot = watoi(m_UIData.m_Label)) ) {

		CString Text;

		Text.Format(IDS_SAME_AS_FMT, m_pLang->GetNameFromSlot(0));

		m_pDataCtrl->SetDefault(Text);
		}

	m_Label = m_pLang->GetNameFromSlot(m_uSlot);

	m_uFlag = m_pLang->GetFlagFromSlot(m_uSlot);
	}

void CUITranslation::OnLayout(CLayFormation *pForm)
{
	UINT uSize = m_pText->GetWidth();

	m_pSlotLayout = New CLayItemText(L"XX",   1, 1);

	m_pFlagLayout = New CLayItemSize(CSize(16, 16), 0, 1);

	m_pNameLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayItemText(uSize,   2, 2);

	m_pTextLayout = New CLayFormRow;

	m_pMainLayout = New CLayFormRow;

	m_pTextLayout->AddItem(New CLayFormPad(m_pSlotLayout, horzLeft | vertCenter));

	m_pTextLayout->AddItem(New CLayFormPad(m_pFlagLayout, horzLeft | vertCenter));

	m_pTextLayout->AddItem(New CLayFormPad(m_pNameLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pDataLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pTextLayout);

	pForm->AddItem(m_pMainLayout);
	}

void CUITranslation::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pSlotCtrl->Create( CPrintf(L"%u.", m_uSlot),
			     0,
			     m_pSlotLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pFlagCtrl->Create( 0,
			     m_pFlagLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pNameCtrl->Create( m_Label,
			     0,
			     m_pNameLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP | WS_BORDER | m_dwStyle,
			     GetDataCtrlRect(),
			     Wnd,
			     uID++
			     );

	m_pSlotCtrl->SetFont(afxFont(Dialog));

	m_pFlagCtrl->SetFont(afxFont(Dialog));

	m_pNameCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pSlotCtrl);

	AddControl(m_pFlagCtrl);

	AddControl(m_pNameCtrl);

	AddControl(m_pDataCtrl, EN_KILLFOCUS);

	m_pFlagCtrl->SetFlag(m_pLang, m_uFlag);

	m_pDataCtrl->SetSlot(m_pLang, m_uSlot);

	m_pDataCtrl->LimitText(m_pText->GetLimit());
	}

void CUITranslation::OnPosition(void)
{
	m_pSlotCtrl->MoveWindow(m_pSlotLayout->GetRect(), TRUE);

	m_pFlagCtrl->MoveWindow(m_pFlagLayout->GetRect(), TRUE);

	m_pNameCtrl->MoveWindow(m_pNameLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(GetDataCtrlRect(), TRUE);
	}

BOOL CUITranslation::OnFindFocus(CWnd * &pWnd)
{
	CRange Range(TRUE);

	m_pDataCtrl->SetSel(Range);

	pWnd = m_pDataCtrl;

	return TRUE;
	}

// Data Overridables

void CUITranslation::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	if( !IsMulti(Data) ) {

		m_pDataCtrl->SetWindowText(Data);

		if( m_pDataCtrl->HasFocus() ) {

			CRange Range(TRUE);

			m_pDataCtrl->SetSel(Range);
			}
		}
	else
		m_pDataCtrl->SetWindowText(L"");

	m_pDataCtrl->SetModify(FALSE);
	}

UINT CUITranslation::OnSave(BOOL fUI)
{
	if( m_pDataCtrl->GetModify() ) {

		CString Text = GetDataCtrlText();

		return StdSave(fUI, Text);
		}

	return saveSame;
	}

// Notification Overridables

BOOL CUITranslation::OnNotify(UINT uID, NMHDR &Info)
{
	return FALSE;
	}

// Implementation

CRect CUITranslation::GetDataCtrlRect(void)
{
	CRect Rect;

	Rect = m_pDataLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

CString CUITranslation::GetDataCtrlText(void)
{
	CString Text = m_pDataCtrl->GetWindowText();

	if( m_fMulti && Text.IsEmpty() ) {

		return multiString;
		}

	return Text;
	}

// End of File
