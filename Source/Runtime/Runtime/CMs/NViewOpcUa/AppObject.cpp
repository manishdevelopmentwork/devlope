
#include "Intern.hpp"

#include "AppObject.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "OpcIds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "NViewListener.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Application Object
//

// Instantiator

clink IUnknown * Create_AppObject(PCSTR pName)
{
	return (IClientProcess *) New CAppObject;
	}

// Constructor

CAppObject::CAppObject(void)
{
	StdSetRef();

	m_pNView = New CNViewListener;
	}

// Destructor

CAppObject::~CAppObject(void)
{
	m_pNView->Release();
	}

// IUnknown

HRESULT CAppObject::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IOpcUaServerHost);

	return E_NOINTERFACE;
	}

ULONG CAppObject::AddRef(void)
{
	StdAddRef();
	}

ULONG CAppObject::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAppObject::TaskInit(UINT uTask)
{
	m_pThread = CreateClientThread(m_pNView, 0, 500);

	AfxNewObject("opcua-server", IOpcUaServer, m_pServer);

	CString Endpoint = "opc.tcp://192.168.1.138:4840";

	return m_pServer->OpcInit(this, Endpoint);
	}

INT CAppObject::TaskExec(UINT uTask)
{
	if( m_pServer->OpcExec() ) {

		for(;;) Sleep(FOREVER);
		}

	return 0;
	}

void CAppObject::TaskStop(UINT uTask)
{
	}

void CAppObject::TaskTerm(UINT uTask)
{
	m_pThread->Destroy();

	m_pServer->OpcTerm();

	m_pServer->Release();
	}

// IOpcUaServerHost

bool CAppObject::LoadModel(IOpcUaDataModel *pModel)
{
	pModel->AddObject   (1, 1, 0, OpcUaId_BaseObjectType, "NView");

	pModel->AddObject   (1, 2, 0, OpcUaId_BaseObjectType, "Switches");

	pModel->AddVariable (1, 3, 0, OpcUaId_BaseDataVariableType, "SwitchCount", 0, OpcUaId_UInt32, 0, 0, OpcUa_AccessLevels_CurrentRead, false);

	pModel->AddOrganizes(0, OpcUaId_ObjectsFolder, 1, 1);

	pModel->AddComponent(1, 1,                     1, 2);

	pModel->AddProperty (1, 1,                     1, 3);

	pModel->AddObjectType(8, 1, 0, OpcUaId_BaseObjectType, "NViewSwitchType",  false);

	pModel->AddObjectType(8, 2, 0, OpcUaId_BaseObjectType, "NViewPortType",    false);

	pModel->AddObjectType(8, 3, 0, OpcUaId_BaseObjectType, "NViewPortMibType", false);

	AddSwitchVars(pModel, 8, 1, 8, 100);

	AddPortVars  (pModel, 8, 2, 8, 200);

	AddMibVars   (pModel, 8, 3, 8, 300);

	for( UINT n = 0; n < m_pNView->GetSwitchCount(); n++ ) {

		AddSwitch(pModel, n);
		}

	return true;
	}

bool CAppObject::CheckUser(CString const &User, CString const &Pass)
{
	return true;
	}

bool CAppObject::SkipValue(UINT ns, UINT id)
{
	return false;
	}

void CAppObject::SetValueDouble(UINT ns, UINT id, UINT n, double d)
{
	}

void CAppObject::SetValueInt(UINT ns, UINT id, UINT n, int d)
{
	}

void CAppObject::SetValueInt64(UINT ns, UINT id, UINT n, INT64 d)
{
	}

void CAppObject::SetValueString(UINT ns, UINT id, UINT n, PCTXT d)
{
	}

double CAppObject::GetValueDouble(UINT ns, UINT id, UINT n)
{
	return 0;
	}

UINT CAppObject::GetValueInt(UINT ns, UINT id, UINT n)
{
	// NS=10X ID=Z      Switch X Property Z
	// NS=10X ID=10Y    Switch X Port Y
	// NS=10X ID=10Y0Z  Switch X Port Y Property Z

	if( ns == 1 && id == 3 ) {

		return m_pNView->GetSwitchCount();
		}

	if( ns >= 100 ) {

		UINT s = ns - 101;

		switch( id ) {

			case 3:
				return m_pNView->GetSwitchPorts(s);

			case 4:
				return m_pNView->IsSwitchActive(s);

			case 6:
				return m_pNView->GetSwitchMac(s).m_Addr[n];

			case 8:
				return m_pNView->GetSwitchIp(s).m_b[n];

			case 9:
				return m_pNView->GetSwitchIp(s).GetValue();

			case 10:
				return m_pNView->GetSwitchVersion(s);

			case 11:
				return m_pNView->IsRingManager(s);

			case 12:
				return m_pNView->IsRingActive(s);
			
			case 13:
				return m_pNView->GetRingState(s);

			case 16:
				return m_pNView->GetSwitchRxMac(s).m_Addr[n];
			}

		if( id >= 10000 ) {

			UINT p = id / 100 - 101;

			UINT v = id % 100;

			switch( v ) {

				case 98:
					return m_pNView->GetPortChipId(n, p);

				case 97:
					return m_pNView->GetPortPortId(n, p);

				case 96:
					return m_pNView->GetPortStatus(n, p);

				case 95:
					return m_pNView->GetPortEnabled(n, p);

				case 94:
					return m_pNView->GetPortDuplex(n, p);

				case 93:
					return m_pNView->GetPortPause(n, p);

				case 92:
					return m_pNView->GetPortSpeed(n, p);
				}

			return m_pNView->GetCounter32(s, p, v - 1);
			}
		}

	return 0;
	}

UINT64 CAppObject::GetValueInt64(UINT ns, UINT id, UINT n)
{
	// NS=10X ID=Z      Switch X Property Z
	// NS=10X ID=10Y    Switch X Port Y
	// NS=10X ID=10Y0Z  Switch X Port Y Property Z

	if( ns >= 100 ) {

		UINT s = ns - 101;

		if( id >= 10000 ) {

			UINT p = id / 100 - 101;

			UINT v = id % 100 - 1;

			if( m_pNView->IsCounter64(v) ) {

				return m_pNView->GetCounter64(s, p, v);
				}
			}
		}

	return 0;
	}

CString CAppObject::GetValueString(UINT ns, UINT id, UINT n)
{
	// NS=10X ID=Z      Switch X Property Z
	// NS=10X ID=10Y    Switch X Port Y
	// NS=10X ID=10Y0Z  Switch X Port Y Property Z

	if( ns == 0 ) {

		switch( id ) {

			case OpcUaId_Server_ServerArray:

				return "NViewServer";
					
			case OpcUaId_Server_ServerStatus_BuildInfo_ManufacturerName:
	
				return "Red Lion Controls";

			case OpcUaId_Server_ServerStatus_BuildInfo_ProductName:

				return "Aeon-Based Device";

			case OpcUaId_Server_ServerStatus_BuildInfo_ProductUri:

				return "http://www.redlion.net";

			case OpcUaId_Server_ServerStatus_BuildInfo_ApplicationUri:

				return "http://www.redlion.net/crimson";

			case OpcUaId_Server_ServerStatus_BuildInfo_SoftwareVersion:

				return "Aeon 1.000";
			}
		}

	if( ns >= 100 ) {

		UINT s = ns - 101;

		switch( id ) {

			case 1:
				return m_pNView->GetSwitchName(s);

			case 2:
				return m_pNView->GetSwitchModel(s);

			case 5:
				return m_pNView->GetSwitchMac(s).GetAsText();

			case 7:
				return m_pNView->GetSwitchIp(s).GetAsText();

			case 15:
				return m_pNView->GetSwitchRxMac(s).GetAsText();
			}

		if( id >= 10000 ) {

			UINT p = id / 100 - 101;

			UINT v = id % 100;

			switch( v ) {

				case 99:
					return m_pNView->GetPortName(s, p);
				}
			}
		}

	return "";
	}

timeval CAppObject::GetValueTime(UINT ns, UINT id, UINT n)
{
	// NS=10X ID=Z      Switch X Property Z
	// NS=10X ID=10Y    Switch X Port Y
	// NS=10X ID=10Y0Z  Switch X Port Y Property Z

	if( ns >= 100 ) {

		UINT s = ns - 101;

		if( id == 14 ) {

			return m_pNView->GetSwitchUpdate(s);
			}
		}

	timeval t = { 0 };

	return t;
	}

bool CAppObject::GetDesc(CString &Desc, UINT ns, UINT id)
{
	if( ns == 1 ) {
		
		switch( id ) {

			case 1: Desc = "An object containing all the NView data collected by this server.";		   return true;
			case 2: Desc = "An object containing objects representing the switches monitored by this server."; return true;
			case 3: Desc = "The number of switches this server is configured to monitor.";			   return true;
			}
		}

	if( ns == 2 ) {

		if( id >= 200 ) {

			Desc = "An object containing objects representing the ports configured for this switch.";

			return true;
			}

		if( id >= 100 ) {

			Desc = "An object representing the status of this switch.";

			return true;
			}
		}

	if( ns >= 100 ) {

		switch( id ) {

			case  1: Desc = "The configured name of this switch.";                                                                return true;
			case  2: Desc = "The model name transmitted by this switch.";                                                         return true;
			case  5: Desc = "The MAC address of this switch, formatted as text.";                                                 return true;
			case  7: Desc = "The IP address of this switch, formatted as text.";                                                  return true;
			case  3: Desc = "The number of port configured for this switch.";                                                     return true;
			case  4: Desc = "A flag to indicate whether messages have been received from this switch.";                           return true;
			case  6: Desc = "The MAC address of this switch, formatted as an array of values.";                                   return true;
			case  8: Desc = "The IP address of this switch, formatted as an array of values.";                                    return true;
			case  9: Desc = "The IP address of this switch, formatted as a single 32-bit value.";                                 return true;
			case 10: Desc = "The NView protocol version being transmitted by this switch.";                                       return true;
			case 11: Desc = "A flag to indicate whether this switch is the NRing mater.";                                         return true;
			case 12: Desc = "A flag to indicate whether this switch is active on the NRing.";                                     return true;
			case 13: Desc = "A value indicating the status of the switch's NRing participation.";                                 return true;
			case 14: Desc = "The time at which the last message was received from this switch.";                                  return true;
			case 15: Desc = "The MAC address of the adapter to which this switch is connected, formatted as text.";               return true;
			case 16: Desc = "The MAC address of the adapter to which this switch is connected, formatted as an array of values."; return true;
			}

		if( id >= 10000 ) {

			UINT v = id % 100;

			switch( v ) {

				case 99: Desc = "The name of this port";                                                 return true;
				case 98: Desc = "The index number of the chip handling this port.";                      return true;
				case 97: Desc = "The index number of this port within the chip that is handling it.";    return true;
				case 96: Desc = "A flag to indicate whether a device is connected to this port.";        return true;
				case 95: Desc = "A flag to indicate whether this port is enabled.";                      return true;
				case 94: Desc = "A flag to indicate whether this port is in full duplex mode.";          return true;
				case 93: Desc = "A flag to indicate whether this port is in pause mode.";                return true;
				case 92: Desc = "The speed in megabits per second at which this port is operating.";     return true;
				case 50: Desc = "An object containing the performance counters related to this switch."; return true;
				}

			Desc = "A counter tracking the performance statistics for this port.";

			return true;
			}

		if( id >= 100 ) {

			Desc = "An object representing the status of a port on this switch.";

			return true;
			}

		return false;
		}

	return false;
	}

bool CAppObject::GetSourceTimeStamp(timeval &t, UINT ns, UINT id)
{
	if( ns >= 100 ) {

		UINT s = ns - 101;

		t = m_pNView->GetSwitchUpdate(s);

		return true;
		}

	return false;
	}

bool CAppObject::HasEvents(CArray <UINT> &List, UINT ns, UINT id)
{
	return false;
	}

bool CAppObject::HasCustomHistory(UINT ns, UINT id)
{
	return false;
	}

bool CAppObject::InitHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	return true;
	}

bool CAppObject::HasChanged(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	return false;
	}

void CAppObject::KillHistory(UINT ns, UINT id, UINT uType, DWORD *pHistory)
{
	}

UINT CAppObject::GetWireType(UINT uType)
{
	return uType;
	}

// Implementation

void CAppObject::AddSwitch(IOpcUaDataModel *pModel, UINT s)
{
	// Property Naming Scheme
	//
	// NS=2   ID=10X    Switch X
	// NS=10X ID=Z      Switch X Property Z
	// NS=10X ID=10Y    Switch X Port Y
	// NS=10X ID=10Y0Z  Switch X Port Y Property Z

	UINT nsSwitch = 2;

	UINT idSwitch = 101 + s;

	UINT idPorts  = 201 + s;

	UINT idVars   = 0;

	pModel->AddObject   (nsSwitch, idSwitch, 8, 1, m_pNView->GetSwitchName(s));

	pModel->AddObject   (nsSwitch, idPorts,  0, OpcUaId_BaseObjectType, "Ports");

	pModel->AddComponent(1,        2,        nsSwitch, idSwitch);

	pModel->AddComponent(nsSwitch, idSwitch, nsSwitch, idPorts);

	UINT c = m_pNView->GetSwitchPorts(s);

	for( UINT p = 0; p < c; p++ ) {

		AddPort(pModel, nsSwitch, idSwitch, idPorts, s, p);
		}

	AddSwitchVars(pModel, nsSwitch, idSwitch, idSwitch, idVars);
	}

void CAppObject::AddSwitchVars(IOpcUaDataModel *pModel, UINT nsSwitch, UINT idSwitch, UINT nsVars, UINT idVars)
{
	pModel->AddVariable(nsVars, idVars +  1, 0, OpcUaId_BaseDataVariableType, "Name",               0, OpcUaId_String,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  2, 0, OpcUaId_BaseDataVariableType, "Model",              0, OpcUaId_String,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  3, 0, OpcUaId_BaseDataVariableType, "PortCount",          0, OpcUaId_UInt32,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  4, 0, OpcUaId_BaseDataVariableType, "IsActive",           0, OpcUaId_Boolean,  0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  5, 0, OpcUaId_BaseDataVariableType, "MacString",          0, OpcUaId_String,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  6, 0, OpcUaId_BaseDataVariableType, "MacBytes",           0, OpcUaId_Byte,     1, 6, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  7, 0, OpcUaId_BaseDataVariableType, "IpString",           0, OpcUaId_String,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  8, 0, OpcUaId_BaseDataVariableType, "IpBytes",            0, OpcUaId_Byte,     1, 4, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars +  9, 0, OpcUaId_BaseDataVariableType, "IpInteger",          0, OpcUaId_UInt32,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 10, 0, OpcUaId_BaseDataVariableType, "Version",            0, OpcUaId_UInt32,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 11, 0, OpcUaId_BaseDataVariableType, "RingIsManager",      0, OpcUaId_Boolean,  0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 12, 0, OpcUaId_BaseDataVariableType, "RingIsActiveMember", 0, OpcUaId_Boolean,  0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 13, 0, OpcUaId_BaseDataVariableType, "RingState",          0, OpcUaId_UInt32,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 14, 0, OpcUaId_BaseDataVariableType, "UpdateTime",		0, OpcUaId_DateTime, 0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 15, 0, OpcUaId_BaseDataVariableType, "RxMacString",        0, OpcUaId_String,   0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable(nsVars, idVars + 16, 0, OpcUaId_BaseDataVariableType, "RxMacBytes",         0, OpcUaId_Byte,     1, 6, OpcUa_AccessLevels_CurrentRead, false);

	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  1);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  2);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  3);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  4);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  5);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  6);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  7);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  8);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars +  9);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 10);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 11);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 12);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 13);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 14);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 15);
	pModel->AddProperty(nsSwitch, idSwitch, nsVars, idVars + 16);
	}

void CAppObject::AddPort(IOpcUaDataModel *pModel, UINT nsSwitch, UINT idSwitch, UINT idPorts, UINT s, UINT p)
{
	UINT nsPort = idSwitch;

	UINT idPort = 101 + p;

	UINT idVars = idPort * 100;

	pModel->AddObject   (nsPort, idPort, 8, 2, m_pNView->GetPortName(s, p));

	pModel->AddComponent(nsSwitch, idPorts, nsPort, idPort);

	AddPortVars(pModel, nsPort, idPort, nsPort, idVars);

	AddMibVars (pModel, nsPort, idVars + 50, nsPort, idVars);
	}

void CAppObject::AddPortVars(IOpcUaDataModel *pModel, UINT nsPort, UINT idPort, UINT nsVars, UINT idVars)
{
	pModel->AddVariable (nsVars, idVars + 99, 0, OpcUaId_BaseDataVariableType, "Name",     0, OpcUaId_String,  0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 98, 0, OpcUaId_BaseDataVariableType, "ChipId",   0, OpcUaId_Byte,    0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 97, 0, OpcUaId_BaseDataVariableType, "PortId",   0, OpcUaId_Byte,    0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 96, 0, OpcUaId_BaseDataVariableType, "Status",   0, OpcUaId_Boolean, 0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 95, 0, OpcUaId_BaseDataVariableType, "Enabled",  0, OpcUaId_Boolean, 0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 94, 0, OpcUaId_BaseDataVariableType, "Duplex",   0, OpcUaId_Boolean, 0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 93, 0, OpcUaId_BaseDataVariableType, "Pause",    0, OpcUaId_Boolean, 0, 0, OpcUa_AccessLevels_CurrentRead, false);
	pModel->AddVariable (nsVars, idVars + 92, 0, OpcUaId_BaseDataVariableType, "Speed",    0, OpcUaId_UInt32,  0, 0, OpcUa_AccessLevels_CurrentRead, false);

	pModel->AddObject   (nsVars, idVars + 50, 8, 3, "MIB");

	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 99);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 98);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 97);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 96);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 95);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 94);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 93);
	pModel->AddProperty (nsPort, idPort, nsVars, idVars + 92);

	pModel->AddComponent(nsPort, idPort, nsVars, idVars + 50);
	}

void CAppObject::AddMibVars(IOpcUaDataModel *pModel, UINT nsMib, UINT idMib, UINT nsVars, UINT idVars)
{
	UINT c = m_pNView->GetCounterCount();

	for( UINT n = 0; n < c; n++ ) {

		CString Name = m_pNView->GetCounterName(n);
		
		UINT    Type = m_pNView->IsCounter64(n) ? OpcUaId_UInt64 : OpcUaId_UInt32;

		pModel->AddVariable( nsVars,
				     idVars + 1 + n,
				     0,
				     OpcUaId_BaseDataVariableType,
				     Name,
				     0,
				     Type,
				     0,
				     0,
				     OpcUa_AccessLevels_CurrentRead,
				     false
				     );

		pModel->AddProperty(nsMib, idMib, nsVars, idVars + 1 + n);
		}
	}

// End of File
