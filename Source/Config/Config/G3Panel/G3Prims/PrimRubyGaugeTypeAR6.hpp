
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAR6_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAR6_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeAR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A6 Radial Gauge Primitive
//

class CPrimRubyGaugeTypeAR6 : public CPrimRubyGaugeTypeAR
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeAR6(void);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);

		// Implementation
		void LoadLayout(void);
	};

// End of File

#endif
