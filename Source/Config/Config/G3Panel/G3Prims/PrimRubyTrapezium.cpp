
#include "intern.hpp"

#include "PrimRubyTrapezium.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Parallelogram Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyTrapezium, CPrimRubyWithHandle);

// Constructor

CPrimRubyTrapezium::CPrimRubyTrapezium(void)
{
	m_Max1 = 5000;
	}

// Meta Data

void CPrimRubyTrapezium::AddMetaData(void)
{
	CPrimRubyWithHandle::AddMetaData();

	Meta_SetName((IDS_TRAPEZIUM));
	}

// Path Generation

void CPrimRubyTrapezium::MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2)
{
	number y1 = p1.m_y + (p2.m_y - p1.m_y) * m_Pos1 / 10000;

	number y2 = p2.m_y - (p2.m_y - p1.m_y) * m_Pos1 / 10000;

	figure.Append(p1.m_x, p1.m_y);
	
	figure.Append(p2.m_x, y1);
	
	figure.Append(p2.m_x, y2);
	
	figure.Append(p1.m_x, p2.m_y);

	figure.AppendHardBreak();
	}

// End of File
