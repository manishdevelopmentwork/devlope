//////////////////////////////////////////////////////////////////////////
//
// Unitelway App Master/Network Slave Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "unitbase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUnitelwaysDriver;
class CUnitelwaysHandler;

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Application Master / Network Slave Driver
//

class CUnitelwaysDriver : public CUnitelwayBaseDriver
{
	public:
		// Constructor
		CUnitelwaysDriver(void);

		// Destructor
		~CUnitelwaysDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Driver Data
		BYTE	m_ThisDrop;
		BYTE	m_Category;
		BOOL	m_fMaster;

		// Device Data
		struct CContext
		{
			UINT m_Drop;
			};
		CContext *	m_pCtx;

		// Data Members
		CUnitelwaysHandler * m_pHandler;
	
		// Opcode Handlers
		BOOL BitRead  (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ByteRead (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL TCByteRead(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WordRead (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL LongRead (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL BitWrite (AREF Addr, PDWORD pData, UINT * pCount);
		BOOL ByteWrite(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL WordWrite(AREF Addr, PDWORD pData, UINT * pCount);
		BOOL LongWrite(AREF Addr, PDWORD pData, UINT * pCount);

		// Transport Layer
		BOOL Transact(BOOL fWait);
		void DoCheck(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Unitelway Application Master / Network Slave Handler
//

class CUnitelwaysHandler : public IPortHandler
{
	public:
		// Constructor
		CUnitelwaysHandler(IHelper *pHelper);
		
		// Destructor
		~CUnitelwaysHandler(void);

		// Operations
		BOOL PutAppData(PCBYTE pData, UINT uCount, BYTE bTransaction);
		UINT GetAppData(PBYTE pData, UINT uCount, UINT uWait);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Binding
		void MCALL Bind(IPortObject *pPort);

		// Event Initialization
		void CreateEvents(void);

		// Event Handlers
		void MCALL OnOpen(CSerialConfig const &Config);
		void MCALL OnClose(void);
		void MCALL OnTimer(void);
		BOOL MCALL OnTxData(BYTE &bData);
		void MCALL OnTxDone(void);
		void MCALL OnRxData(BYTE bData);
		void MCALL OnRxDone(void);

		//Helpers		
		void SetDrops(BYTE bThis, BYTE bThat, BOOL fMaster);
		BOOL IsActive(void);

	protected:
		// Data
		ULONG   m_uRefs;

		// Helper
		IHelper *m_pHelper;

		// Port Object
		IPortObject *m_pPort;
		
		// Rx State Machine
		UINT	m_uRxState;
		UINT	m_uRxPtr;
		BYTE	m_bRxCheck;
		UINT	m_uRxEnd;
		BYTE	m_bHead[8];

		// Tx State Machine
		BOOL	m_fTxEnable;
		UINT	m_uTxPtr;
		UINT	m_uTxScan;
		BYTE	m_Transaction;

		// Other Context
		UINT	m_uTimer;
		UINT	m_uIsActive;
		BOOL	m_fWaitReply;
		UINT	m_uMacState;
		UINT	m_uSetTimer;

		// Application Data
		UINT	m_uTxAppCount;
		UINT	m_uRxAppCount;
		BYTE	m_bThisDrop;
		BYTE	m_bThatDrop;
		BOOL	m_fMaster;
		IEvent *m_pRxAppFlag;
		BYTE	m_bTxApp[256];
		BYTE	m_bRxApp[256];
		BOOL	m_fNotNAK;

		// Sequencing Control
		UINT	m_uResendCount;
		BYTE	m_bSourceDrop;
		BYTE	m_bThisPoll;
		BOOL	m_fIgnoreFrame;
		UINT	m_WaitAckCount;

		// Transmit Machine
		void SendData(void);
		void SendPoll(void);
		
		// Receive Machine
		void RxByte(BYTE bData);
		UINT ProcessFrame(void);
		
		// Event Handlers
		void Signal(UINT uEvent);
		void HandleIdle(UINT uEvent);
		void HandleWaitResponse(UINT uEvent);
		void HandleWaitAck(UINT uEvent);
		
		// Event Support
		void ResetMac(void);

		// V2 Delay
		UINT m_uEOTDelay;
		void SpeedCheck(void);
		void DelayDecr(UINT *pData);
		void DelayABit(void);

		// 485 - 9600 support
		BOOL m_fIgnoreCheck;

		// General Support
		void SetTimeout(UINT uTime);
		void ClearTimeout(void);
		void SendEOT(void);
		UINT CheckContinuation(BYTE bData);
		void HandleResponseData(BYTE bData);

		// Timing
		UINT ToTicks(UINT t);
	};

// End of File
