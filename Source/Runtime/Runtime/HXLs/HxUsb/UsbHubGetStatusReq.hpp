
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubGetStatusReq_HPP

#define	INCLUDE_UsbHubGetStatusReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Get Status Request
//

class CUsbHubGetStatusReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubGetStatusReq(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
