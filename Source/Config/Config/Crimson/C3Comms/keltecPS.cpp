
#include "intern.hpp"

#include "keltecPS.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Keltec Power Supply Driver
//

// Instantiator

ICommsDriver *	Create_KeltecPowerSupplyDriver(void)
{
	return New CKeltecPowerSupplyDriver;
	}

// Constructor

CKeltecPowerSupplyDriver::CKeltecPowerSupplyDriver(void)
{
	m_wID		= 0x4056;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Keltec";
	
	m_DriverName	= "Power Supply";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Keltec Power Supply";

	m_DevRoot	= "PS";

	AddSpaces();
	}

// Binding Control

UINT	CKeltecPowerSupplyDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CKeltecPowerSupplyDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

// Address Management

BOOL	CKeltecPowerSupplyDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CKeltecPowerSupplyAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Implementation

void	CKeltecPowerSupplyDriver::AddSpaces(void)
{
// Queries - Address Named
	AddSpace( New CSpace("STQ",	"Status Query (1)",		STQ	));
	AddSpace( New CSpace("OCQ",	"Output Current Query (6)",	OCQ, RR	));
	AddSpace( New CSpace("OVQ",	"Output Voltage Query (7)",	OVQ, RR	));
	AddSpace( New CSpace("OPQ",	"Output Power Query (8)",	OPQ, RR	));
	AddSpace( New CSpace("TMQ",	"Temperature Query (=)",	TMQ, RR	));
	AddSpace( New CSpace("CSQ",	"Current State Query (C)",	CSQ	));
// Commands
	AddSpace( New CSpace("ARDY",	"Ready Mode Command (4)",	ARDY	));
	AddSpace( New CSpace("ARUN",	"Run Mode Command (5)",		ARUN	));
	AddSpace( New CSpace("SLFT",	"Self Test Command (2)",	SLFT	));
	AddSpace( New CSpace("FRST",	"Fault Reset Command (9)",	FRST	));
	AddSpace( New CSpace("REBT",	"Reboot (!)",			REBT	));
	AddSpace( New CSpace("SETC",	"Compute and set checksum (*)",	SETC	));
	AddSpace( New CSpace("MTMD",	"Maintenance Mode (M)",		MTMD	));
	AddSpace( New CSpace("RELD",	"Reload (P)",			RELD	));
// Read Only Table
	AddSpace( New CSpace(ADQ,	"ADQ",	"ADC Query (A)",		10, 0, 7, YY));
// R/W values named
	AddSpace( New CSpace("SRI",	"Set/Read Current (I)",		SRI ));
	AddSpace( New CSpace("FTMO",	"Set/Read Fault Timeout (X)",	FTMO));
// R/W Table
	AddSpace( New CSpace(NVWD,	"NVWD",	"Set/Read 16 bit NV (Y)",	10, 0, 254, WW));
	AddSpace( New CSpace(NVBY,	"NVBY",	"Set/Read  8 bit NV (Y)",	10, 0, 254, YY));
// String Response
	AddSpace( New CSpace(IDQ,	"IDQ",	"ID String (0)",		10, 0,   4, LL));
// User Commands
	AddSpace( New CSpace("USR",	"Send USRC",			USR));
	AddSpace( New CSpace(USRC,	"USRC",	"   User Command String",	10, 0,   4, LL));
	AddSpace( New CSpace(USRR,	"USRR",	"   USRC Response String",	10, 0,   4, LL));
// Latest Error Code
	AddSpace( New CSpace("ERRC",	"Latest Error Code",		ERRC));
	}

//////////////////////////////////////////////////////////////////////////
//
// Keltec Power Supply Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CKeltecPowerSupplyAddrDialog, CStdAddrDialog);
		
// Constructor

CKeltecPowerSupplyAddrDialog::CKeltecPowerSupplyAddrDialog(CKeltecPowerSupplyDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	}

// Message Map

AfxMessageMap(CKeltecPowerSupplyAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CKeltecPowerSupplyAddrDialog)
	};

// Message Handlers

BOOL CKeltecPowerSupplyAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	BOOL f = CStdAddrDialog::OnInitDialog(Focus, dwData);

	if( !f && m_pSpace ) {

		CAddress &Addr = (CAddress &) *m_pAddr;

		HandleDetails(Addr.a.m_Table, Addr.a.m_Offset);
		}

	return f;
	}

// Notification Handlers

void CKeltecPowerSupplyAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CKeltecPowerSupplyAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	if( m_pSpace ) {

		HandleDetails(m_pSpace->m_uTable, m_pSpace->m_uMinimum);
		}
	}

BOOL CKeltecPowerSupplyAddrDialog::OnOkay(UINT uID)
{
	return CStdAddrDialog::OnOkay(uID);
	}

// Helpers

void CKeltecPowerSupplyAddrDialog::HandleDetails(UINT uTable, UINT uOffset)
{
	BOOL f02 = TRUE;
	BOOL f30 = TRUE;

	switch( uTable ) {

		case    0:
		case  IDQ:
		case USRC:
		case USRR:
			f02 = FALSE;
			f30 = FALSE;
			break;

		case AN:
			f02 = FALSE;

			if( !uOffset ) {

				f02      = FALSE;
				f30      = FALSE;
				}
			break;
		}

	if( !f02 ) {

		GetDlgItem(2002).SetWindowText("");
		}

	GetDlgItem(2002).EnableWindow(f02);

	if( !f30 ) {

		GetDlgItem(3004).SetWindowText("");
		GetDlgItem(3006).SetWindowText("");
		GetDlgItem(3008).SetWindowText("");
		}
	}

// End of File
