
//////////////////////////////////////////////////////////////////////////
//
// KEB DIN66019II Driver
//

#define KEBDIN2_ID 0x339B

#define	SZS12D	36
#define	DWSZS12	SZS12D/sizeof(DWORD)

class CKEBDIN2Driver : public CMasterDriver
{
	public:
		// Constructor
		CKEBDIN2Driver(void);

		// Destructor
		~CKEBDIN2Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			DWORD	m_S02[2];
			DWORD	m_S04[2];
			DWORD	m_S05[4];
			DWORD	m_S07[4];
			DWORD	m_S10[5];
			DWORD	m_S12[3];
			char	m_S12D[SZS12D];
			DWORD	m_S16[2];
			DWORD	m_S17[4];
			DWORD	m_S18[5];
			DWORD	m_S12ALoaded;
			};

		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[64];
		BYTE	m_bRx[64];
		UINT	m_uPtr;
		UINT	m_uCheck;
		UINT	m_uIID;
		UINT	m_uTable;
		DWORD	m_Error;
		DWORD	m_W48[2];
		DWORD	m_R48[3];
		DWORD	m_W49[4];
		DWORD	m_R49[5];
		BOOL	m_fS12ALoaded;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Initiate
		BOOL	InitDevice(void);
	
		// Opcode Handlers
		BOOL	Read(AREF Addr);
		UINT	WriteKEB(AREF Addr, PDWORD pData, UINT uCount);
		
		// Frame Building
		void	StartFrame(BOOL fIsWrite);
		void	EndFrame(BOOL fIsWrite);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor);
		void	AddParameter( AREF Addr, DWORD dData, BOOL fIsWrite );
		void	AddWriteData(AREF Addr, PDWORD pData, UINT uCount);
		
		// Transport Layer
		UINT	Transact(void);
		void	Send(void);
		UINT	GetReply(void);
		BOOL	GetResponse(PDWORD pData, AREF Addr, UINT uCount);
		BOOL	CheckResponse( UINT uPtr, UINT uCode );
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BYTE	GetServiceByte(void);
		UINT	ReadNoTransmit(UINT uTable, UINT uOffset, PDWORD pData, UINT uCount);
		BOOL	WriteNoTransmit(UINT uTable, UINT uOffset, PDWORD pData, UINT uCount);
		DWORD	GetData( UINT uPosition, UINT uCount );
		DWORD	GetDataValue( BYTE b );
		UINT	GetErrorControl(void);
	};

#define TXIIDR	4
#define TXIIDW	5
#define	TXSTX	3

#define RXIID	2	

#define	CFAIL	0
#define	COK	1
#define	CERROR	2

#define	RNTREAD	0
#define	RNTDATA	1
#define	RNTDONE	2
#define	RNTARR	3

#define	R2A	1
#define	R2B	2
#define W2A	3
#define W2B	4
#define	S2W	5
#define	S3A	6
#define	R4A	7
#define	R4B	8
#define	S4W	9
#define	R5A	10
#define	R5B	11
#define	R5C	12
#define	R5D	13
#define	S5W	14
#define	R16A	15
#define	S16W	16
#define	R17A	17
#define	S17W	18
#define	SERR	19
#define	W4A	20
#define	W4B	21
#define	W5A	22
#define	W5B	23
#define	W5C	24
#define	W5D	25
#define	W16A	26
#define	W17A	27
#define	CM6	28
#define	R7A	37
#define	R7B	38
#define	R7C	39
#define	R7D	40
#define	W7A	41
#define	W7B	42
#define	W7C	43
#define	W7D	44
#define	S7W	45
#define	R10A	46
#define	R10B	47
#define	R10C	48
#define	R10D	49
#define	R10E	50
#define	W10A	51
#define	W10B	52
#define	W10C	53
#define	W10D	54
#define	W10E	55
#define	S10W	56
#define	CM11	57
#define R18A	66
#define R18B	67
#define R18C	68
#define R18D	69
#define R18E	70
#define W18A	71
#define W18B	72
#define W18C	73
#define W18D	74
#define W18E	75
#define S18W	76
#define S48	30
#define W48D	77
#define R48D	29
#define S49	32
#define W49D	78
#define R49D	31

#define R12B	83
#define R12C	84
#define R12D	85
#define W12A	89
#define W12B	90
#define W12C	91
#define W12D	92
#define	S12W	96


// End of File
