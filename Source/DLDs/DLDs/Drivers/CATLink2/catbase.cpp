
#include "intern.hpp"

#include "catlink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Shared Functions
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// PID Coding

BOOL CCatLinkBase::IsValidPID(UINT uName)
{
	switch( uName ) {

		case 0x0001:
		case 0x0002:
		case 0x0003:
		case 0x0007:
		case 0x0008:
		case 0x000C:
		case 0x000D:
		case 0x000E:
		case 0x000F:
		case 0x0011:
		case 0x0015:
		case 0x0016:
		case 0x0017:
		case 0x0018:
		case 0x0040:
		case 0x0041:
		case 0x0042:
		case 0x0044:
		case 0x0045:
		case 0x0046:
		case 0x0047:
		case 0x004B:
		case 0x004D:
		case 0x004E:
		case 0x0053:
		case 0x0054:
		case 0x0055:
		case 0x0058:
		case 0x0059:
		case 0x005A:
		case 0x005B:
		case 0x005C:
		case 0x005D:
		case 0x005E:
		case 0x005F:
		case 0x0080:
		case 0x0082:
		case 0x0083:
		case 0x0084:
		case 0x0086:
		case 0x00C8:
		case 0xF001:
		case 0xF002:
		case 0xF00D:
		case 0xF00E:
		case 0xF013:
		case 0xF014:
		case 0xF016:
		case 0xF018:
		case 0xF01B:
		case 0xF021:
		case 0xF022:
		case 0xF023:
		case 0xF024:
		case 0xF025:
		case 0xF026:
		case 0xF027:
		case 0xF029:
		case 0xF02A:
		case 0xF02B:
		case 0xF02C:
		case 0xF02D:
		case 0xF031:
		case 0xF032:
		case 0xF062:
		case 0xF066:
		case 0xF074:
		case 0xF07C:
		case 0xF08F:
		case 0xF090:
		case 0xF094:
		case 0xF09C:
		case 0xF0A6:
		case 0xF0A8:
		case 0xF0A9:
		case 0xF0AA:
		case 0xF0AC:
		case 0xF0B0:
		case 0xF0B1:
		case 0xF0B2:
		case 0xF0B3:
		case 0xF0B4:
		case 0xF0B5:
		case 0xF0B6:
		case 0xF0BA:
		case 0xF0C1:
		case 0xF0C2:
		case 0xF0E8:
		case 0xF0F1:
		case 0xF0F2:
		case 0xF0FD:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF111:
		case 0xF112:
		case 0xF113:
		case 0xF115:
		case 0xF116:
		case 0xF117:
		case 0xF118:
		case 0xF119:
		case 0xF11B:
		case 0xF11C:
		case 0xF11D:
		case 0xF121:
		case 0xF122:
		case 0xF123:
		case 0xF124:
		case 0xF125:
		case 0xF127:
		case 0xF129:
		case 0xF14F:
		case 0xF153:
		case 0xF154:
		case 0xF189:
		case 0xF191:
		case 0xF192:
		case 0xF1AD:
		case 0xF1C4:
		case 0xF1D0:
		case 0xF1D1:
		case 0xF1D3:
		case 0xF1D4:
		case 0xF1D5:
		case 0xF1D6:
		case 0xF212:
		case 0xF213:
		case 0xF22A:
		case 0xF22B:
		case 0xF22C:
		case 0xF231:
		case 0xF24D:
		case 0xF24F:
		case 0xF257:
		case 0xF287:
		case 0xF28A:
		case 0xF290:
		case 0xF299:
		case 0xF2B3:
		case 0xF2C3:
		case 0xF2C6:
		case 0xF2CB:
		case 0xF2CC:
		case 0xF2CE:
		case 0xF2D4:
		case 0xF2D5:
		case 0xF2D6:
		case 0xF2D7:
		case 0xF2DD:
		case 0xF2DE:
		case 0xF2F6:
		case 0xF2F7:
		case 0xF2FA:
		case 0xF2FC:
		case 0xF2FD:
		case 0xF30B:
		case 0xF30C:
		case 0xF312:
		case 0xF313:
		case 0xF402:
		case 0xF40E:
		case 0xF410:
		case 0xF411:
		case 0xF412:
		case 0xF413:
		case 0xF415:
		case 0xF417:
		case 0xF419:
		case 0xF41C:
		case 0xF41F:
		case 0xF420:
		case 0xF421:
		case 0xF422:
		case 0xF425:
		case 0xF426:
		case 0xF427:
		case 0xF428:
		case 0xF42A:
		case 0xF42C:
		case 0xF42E:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF442:
		case 0xF443:
		case 0xF444:
		case 0xF445:
		case 0xF446:
		case 0xF447:
		case 0xF448:
		case 0xF449:
		case 0xF44A:
		case 0xF44B:
		case 0xF44C:
		case 0xF44D:
		case 0xF44E:
		case 0xF44F:
		case 0xF450:
		case 0xF451:
		case 0xF452:
		case 0xF453:
		case 0xF455:
		case 0xF456:
		case 0xF457:
		case 0xF45B:
		case 0xF45C:
		case 0xF45D:
		case 0xF45E:
		case 0xF45F:
		case 0xF460:
		case 0xF461:
		case 0xF462:
		case 0xF463:
		case 0xF464:
		case 0xF465:
		case 0xF466:
		case 0xF467:
		case 0xF468:
		case 0xF469:
		case 0xF46A:
		case 0xF46B:
		case 0xF46C:
		case 0xF46D:
		case 0xF46F:
		case 0xF477:
		case 0xF478:
		case 0xF48D:
		case 0xF48F:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4A4:
		case 0xF4A5:
		case 0xF4B2:
		case 0xF4B3:
		case 0xF4B4:
		case 0xF4B5:
		case 0xF4B6:
		case 0xF4C3:
		case 0xF4C4:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4CF:
		case 0xF4D0:
		case 0xF4D1:
		case 0xF4D2:
		case 0xF4D3:
		case 0xF4D4:
		case 0xF4D5:
		case 0xF4E1:
		case 0xF4EA:
		case 0xF4F7:
		case 0xF4F8:
		case 0xF4FE:
		case 0xF4FF:
		case 0xF508:
		case 0xF509:
		case 0xF50A:
		case 0xF50B:
		case 0xF50C:
		case 0xF50D:
		case 0xF50E:
		case 0xF50F:
		case 0xF510:
		case 0xF511:
		case 0xF512:
		case 0xF513:
		case 0xF514:
		case 0xF515:
		case 0xF516:
		case 0xF517:
		case 0xF518:
		case 0xF519:
		case 0xF51A:
		case 0xF51B:
		case 0xF51C:
		case 0xF51D:
		case 0xF51E:
		case 0xF51F:
		case 0xF520:
		case 0xF524:
		case 0xF525:
		case 0xF527:
		case 0xF53E:
		case 0xF540:
		case 0xF541:
		case 0xF542:
		case 0xF54F:
		case 0xF557:
		case 0xF55A:
		case 0xF55B:
		case 0xF55C:
		case 0xF55D:
		case 0xF55E:
		case 0xF55F:
		case 0xF560:
		case 0xF561:
		case 0xF562:
		case 0xF563:
		case 0xF564:
		case 0xF565:
		case 0xF566:
		case 0xF567:
		case 0xF568:
		case 0xF569:
		case 0xF56A:
		case 0xF56B:
		case 0xF56C:
		case 0xF56D:
		case 0xF56E:
		case 0xF56F:
		case 0xF570:
		case 0xF571:
		case 0xF572:
		case 0xF573:
		case 0xF574:
		case 0xF575:
		case 0xF577:
		case 0xF578:
		case 0xF579:
		case 0xF57B:
		case 0xF57C:
		case 0xF57E:
		case 0xF57F:
		case 0xF58B:
		case 0xF58D:
		case 0xF58E:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF597:
		case 0xF598:
		case 0xF599:
		case 0xF59A:
		case 0xF59B:
		case 0xF59C:
		case 0xF59D:
		case 0xF59E:
		case 0xF59F:
		case 0xF5A0:
		case 0xF5A1:
		case 0xF5A2:
		case 0xF5A3:
		case 0xF5A4:
		case 0xF5A5:
		case 0xF5A6:
		case 0xF5A7:
		case 0xF5A8:
		case 0xF5A9:
		case 0xF5AA:
		case 0xF5AB:
		case 0xF5B1:
		case 0xF5B7:
		case 0xF5B8:
		case 0xF5BA:
		case 0xF5BB:
		case 0xF5C4:
		case 0xF5C9:
		case 0xF5D1:
		case 0xF5D6:
		case 0xF5D8:
		case 0xF5D9:
		case 0xF5DA:
		case 0xF5DB:
		case 0xF5E0:
		case 0xF602:
		case 0xF603:
		case 0xF604:
		case 0xF605:
		case 0xF61E:
		case 0xF62B:
		case 0xF636:
		case 0xF701:
		case 0xF702:
		case 0xF703:
		case 0xF704:
		case 0xF705:
		case 0xF706:
		case 0xF707:
		case 0xF708:
		case 0xF709:
		case 0xF70A:
		case 0xF70B:
		case 0xF70C:
		case 0xF70D:
		case 0xF70E:
		case 0xF70F:
		case 0xF710:
		case 0xF711:
		case 0xF7FA:
		case 0xF7FC:
		case 0xF803:
		case 0xF806:
		case 0xF810:
		case 0xF811:
		case 0xF814:
		case 0xF81A:
		case 0xF81C:
		case 0xFC07:
		case 0xFC08:
		case 0xFC09:
		case 0xFC0D:
		case 0xFC0F:
		case 0xFC10:
		case 0xFC11:
		case 0xFC12:
		case 0xFC13:
		case 0xFC14:
		case 0xFC15:
		case 0xFC16:
		case 0xFC17:
		case 0xFC18:
		case 0xFC19:
		case 0xFC1A:
		case 0xFC1B:
		case 0xFC1C:
		case 0xFC1D:
		case 0xFC1E:
		case 0xFC1F:
		case 0xFC21:
		case 0xFC22:
		case 0xFC27:
		case 0xFC28:
		case 0xFC29:
		case 0xFC2D:
		case 0xFC2E:
		case 0xFC32:
		case 0xFC33:
		case 0xFC88:
		case 0xD00020:
		case 0xD00021:
		case 0xD00022:
		case 0xD00023:
		case 0xD00024:
		case 0xD00025:
		case 0xD00026:
		case 0xD00027:
		case 0xD00028:
		case 0xD00029:
		case 0xD0002A:
		case 0xD0002B:
		case 0xD0002C:
		case 0xD0002D:
		case 0xD0002E:
		case 0xD0002F:
		case 0xD00030:
		case 0xD00031:
		case 0xD00032:
		case 0xD00033:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:
		case 0xD00050:
		case 0xD00051:
		case 0xD00052:
		case 0xD00053:
		case 0xD000EB:
		case 0xD000EC:
		case 0xD000ED:
		case 0xD000EE:
		case 0xD000EF:
		case 0xD000F0:
		case 0xD000F1:
		case 0xD000F2:
		case 0xD000F3:
		case 0xD000F4:
		case 0xD000F5:
		case 0xD000F6:
		case 0xD000F7:
		case 0xD000F8:
		case 0xD000F9:
		case 0xD000FA:
		case 0xD000FB:
		case 0xD000FC:
		case 0xD000FD:
		case 0xD000FE:
		case 0xD00109:
		case 0xD0010A:
		case 0xD0012F:
		case 0xD00130:
		case 0xD00131:
		case 0xD00148:
		case 0xD00149:
		case 0xD0014A:
		case 0xD0014B:
		case 0xD0014C:
		case 0xD0014D:
		case 0xD0014E:
		case 0xD0014F:
		case 0xD00150:
		case 0xD00151:
		case 0xD00188:
		case 0xD00189:
		case 0xD001A1:
		case 0xD001C7:
		case 0xD00259:
		case 0xD00260:
		case 0xD00261:
		case 0xD00262:
		case 0xD0027D:
		case 0xD0027E:
		case 0xD0027F:
		case 0xD00281:
		case 0xD002AE:
		case 0xD0036F:
		case 0xD00370:
		case 0xD00371:
		case 0xD00375:
		case 0xD00377:
		case 0xD00378:
		case 0xD00379:
		case 0xD003A5:
		case 0xD003A6:
		case 0xD003E0:
		case 0xD003E1:
		case 0xD003E2:
		case 0xD003E3:
		case 0xD003E4:
		case 0xD003E5:
		case 0xD003E6:
		case 0xD003E7:
		case 0xD003E8:
		case 0xD003E9:
		case 0xD003EA:
		case 0xD003EB:
		case 0xD003EC:
		case 0xD003ED:
		case 0xD003EE:
		case 0xD003EF:
		case 0xD003F0:
		case 0xD003F1:
		case 0xD003F2:
		case 0xD003F3:
		case 0xD003F4:
		case 0xD003F5:
		case 0xD003F6:
		case 0xD003F7:
		case 0xD003F8:
		case 0xD003F9:
		case 0xD003FA:
		case 0xD003FB:
		case 0xD003FC:
		case 0xD003FD:
		case 0xD003FE:
		case 0xD003FF:
		case 0xD00400:
		case 0xD00401:
		case 0xD00402:
		case 0xD00403:
		case 0xD00404:
		case 0xD00405:
		case 0xD00406:
		case 0xD00407:
		case 0xD00418:
		case 0xD00419:
		case 0xD0041A:
		case 0xD00453:
		case 0xD00478:
		case 0xD00479:
		case 0xD0047A:
		case 0xD0047B:
		case 0xD0047C:
		case 0xD0047D:
		case 0xD0047E:
		case 0xD0047F:
		case 0xD00480:
		case 0xD00481:
		case 0xD00482:
		case 0xD00483:
		case 0xD004B5:
		case 0xD004B6:
		case 0xD004B7:
		case 0xD004DB:
		case 0xD004DC:
		case 0xD0050B:
		case 0xD0050C:
		case 0xD0050D:
		case 0xD0050E:
		case 0xD0050F:
		case 0xD00510:
		case 0xD00511:
		case 0xD00512:
		case 0xD00513:
		case 0xD00514:
		case 0xD00515:
		case 0xD00516:
		case 0xD00517:
		case 0xD00518:
		case 0xD00519:
		case 0xD0051A:
		case 0xD005B1:
		case 0xD005B2:
		case 0xD005B3:
		case 0xD005F5:
		case 0xD006BB:
		case 0xD0098D:
		case 0xD0099E:
		case 0xD0099F:
		case 0xD009A0:
		case 0xD009A1:
		case 0xD009A2:
		case 0xD009A3:
		case 0xD00AF4:
		case 0xD00AF5:
		case 0xD00AF6:
		case 0xD00B01:
		case 0xD00B18:
		case 0xD00B3D:
		case 0xD00B3E:
		case 0xD00BB3:
		case 0xD00BB7:
		case 0xD00C81:
		case 0xD00D14:
		case 0xD00D15:
		case 0xD00D16:
		case 0xD00D17:
		case 0xD00D18:
		case 0xD00D19:
		case 0xD00D3B:
		case 0xD00D5A:
		case 0xD00D61:
		case 0xD00E9D:
		case 0xD00EEC:
		case 0xD00F5D:
		case 0xD00F5E:
		case 0xD00F5F:
		case 0xD00F60:
		case 0xD00F75:
		case 0xD00FCA:
		case 0xD00FCB:
		case 0xD01075:
		case 0xD01076:
		case 0xD01078:
		case 0xD01079:
		case 0xD01092:
		case 0xD0109E:
		case 0xD0109F:
		case 0xD010F0:
		case 0xD010F1:
		case 0xD010F2:
		case 0xD010F3:
		case 0xD01124:
		case 0xD01125:
		case 0xD0112C:
		case 0xD0112D:
		case 0xD0113A:
		case 0xD0113B:
		case 0xD0113C:
		case 0xD0113D:
		case 0xD0113E:
		case 0xD0113F:
		case 0xD01140:
		case 0xD01141:
		case 0xD0114A:
		case 0xD0114B:
		case 0xD0114C:
		case 0xD0114D:
		case 0xD01152:
		case 0xD01154:
		case 0xD01153:
		case 0xD01155:
		case 0xD0115A:
		case 0xD0115C:
		case 0xD0115B:
		case 0xD0115D:
		case 0xD0115E:
		case 0xD01160:
		case 0xD0115F:
		case 0xD01161:
		case 0xD01164:
		case 0xD01165:
		case 0xD01192:
		case 0xD01209:
		case 0xD0120A:
		case 0xD0120B:
		case 0xD0120C:
		case 0xD0120D:
		case 0xD0120E:
		case 0xD0120F:
		case 0xD01255:
		case 0xD01257:
		case 0xD01258:
		case 0xD016E9:
		case 0xD016EA:
		case 0xD016EB:
		case 0xD016EC:
		case 0xD016ED:
		case 0xD016EE:
		case 0xD016EF:
		case 0xD016F0:
		case 0xD016F1:
		case 0xD016F2:
		case 0xD016F3:
		case 0xD016F4:
		case 0xD016F5:
		case 0xD016F6:
		case 0xD016F7:
		case 0xD10018:
		case 0xD1004A:
		case 0xD10050:
		case 0xD10051:
		case 0xD1005D:
		case 0xD10060:
		case 0xD10066:
		case 0xD1009A:
		case 0xD100A0:
		case 0xD100AA:
		case 0xD100AB:
		case 0xD100C5:
		case 0xD100C6:
		case 0xD100C7:
		case 0xD100C8:
		case 0xD100C9:
		case 0xD100CA:
		case 0xD100CD:
		case 0xD100CE:
		case 0xD100CF:
		case 0xD100EC:
		case 0xD10104:
		case 0xD10120:
		case 0xD1013A:
		case 0xD10167:
		case 0xD101EF:
		case 0xD101FE:
		case 0xD10230:
		case 0xD10293:
		case 0xD10986:
		case 0xD10987:
		case 0xD10D21:
		case 0xD10D22:
		case 0xD10D23:
		case 0xD10D24:
		case 0xD10D25:
		case 0xD10D26:
		case 0xD10D27:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkBase::IsWritablePID(UINT uName)
{
	switch( uName ) {
		
		case 0x000D:	// 7
		case 0x0047:	// 74
		case 0xF0B0:	// 22
		case 0xF0B1:	// 23
		case 0xF0B2:	// 24
		case 0xF0C2:	// 30
		case 0xF11C:	// 47
		case 0xF11D:	// 48
		case 0xF125:	// 53
		case 0xF213:	// 63
		case 0xF44D:	// 91
		case 0xF51A:    // 120
		case 0xF524:	// 310
		case 0xF57C:	// 352
		case 0xF5B1:	// 354
		case 0xFC2E:	// 672
		case 0xFC32:	// 693
		case 0xFC33:	// 694
		case 0xD00281:	// 383
		case 0xD00D61:	// 708
		case 0xD100A0:	// 206
		case 0xD10167:  // 209
		case 0xD00478:  // 391
		case 0xD00479:	// 392
		case 0xD0047A:	// 393
		case 0xD005B3:	// 409
		case 0xD006BB:	// 697
		case 0xD01078:  // 634
		case 0xD01079:	// 703
		case 0xD0109E:	// 704
		case 0xD0109F:	// 706


			return TRUE;
		}

	return FALSE;
	}

PCTXT CCatLinkBase::GetPIDCode(UINT uName)
{
	// ============================================================
	//
	// Return a string indicating where to get the PID. The first
	// letter defines one or more source MIDs, while the numeric
	// suffix might be used later to figure out where to start
	// according to the cylinder we are addressing. The letters
	// are as follows...
	//
	// E = ECMs
	// G = GCS
	// S = GCS, ECMs
	// I = ICMs
	// V = VIMS, ECM
	// T = Trans, VIMS
	// P = Payload Only
	//
	// ============================================================

	switch( uName ) {

		case 0x000001: return "E";
		case 0x000002: return "E";
		case 0x000003: return "E";
		case 0x000007: return "E";
		case 0x000008: return "E";
		case 0x00000C: return "E";
		case 0x00000D: return "S";
		case 0x000011: return "E";
		case 0x000016: return "E";
		case 0x00F014: return "S";
		case 0x00F016: return "E";
		case 0x00F01B: return "E";
		case 0x00F02A: return "S";
		case 0x00F02C: return "E";
		case 0x00F08F: return "S";
		case 0x00F09C: return "E";
		case 0x00F0A6: return "E";
		case 0x00F0A8: return "E";
		case 0x00F0A9: return "E";
		case 0x00F0AA: return "E";
		case 0x00F0AC: return "E";
		case 0x00F0B0: return "G";
		case 0x00F0B1: return "S";
		case 0x00F0B2: return "S";
		case 0x00F0B3: return "G";
		case 0x00F0B4: return "G";
		case 0x00F0B5: return "E";
		case 0x00F0B6: return "E";
		case 0x00F0C1: return "E";
		case 0x00F0C2: return "E";
		case 0x00F0F2: return "E";
		case 0x00F0FD: return "E";
		case 0x00F108: return "E";
		case 0x00F109: return "E";
		case 0x00F10A: return "E";
		case 0x00F10B: return "E";
		case 0x00F10C: return "E";
		case 0x00F10D: return "E";
		case 0x00F111: return "E";
		case 0x00F112: return "E";
		case 0x00F113: return "E";
		case 0x00F115: return "E";
		case 0x00F116: return "E";
		case 0x00F117: return "E";
		case 0x00F118: return "E";
		case 0x00F119: return "E";
		case 0x00F11C: return "E";
		case 0x00F11D: return "E";
		case 0x00F121: return "E";
		case 0x00F122: return "E";
		case 0x00F123: return "E";
		case 0x00F124: return "E";
		case 0x00F125: return "E";
		case 0x00F129: return "E";
		case 0x00F14F: return "E";
		case 0x00F192: return "E";
		case 0x00F2C3: return "E";
		case 0x00F127: return "E";
		case 0x00F1D3: return "G";
		case 0x00F1D4: return "G";
		case 0x00F1D5: return "G";
		case 0x00F1D6: return "G";
		case 0x00F213: return "S";
		case 0x00F24D: return "E";
		case 0x00F24F: return "E";
		case 0x00F28A: return "E";
		case 0x00F2CB: return "G";
		case 0x00F2CC: return "G";
		case 0x00F2D6: return "G";
		case 0x00F2D7: return "G";
		case 0x000040: return "S";
		case 0x000042: return "G";
		case 0x000044: return "S";
		case 0x000047: return "E";
		case 0x00004D: return "E";
		case 0x00005E: return "S";
		case 0x00F417: return "E";
		case 0x00F420: return "V";
		case 0x00F440: return "S";
		case 0x00F441: return "S";
		case 0x00F442: return "G";
		case 0x00F443: return "G";
		case 0x00F444: return "G";
		case 0x00F445: return "G";
		case 0x00F446: return "G";
		case 0x00F447: return "G";
		case 0x00F448: return "G";
		case 0x00F449: return "G";
		case 0x00F44A: return "G";
		case 0x00F44C: return "S";
		case 0x00F44D: return "G";
		case 0x00F45B: return "E";
		case 0x00F460: return "G";
		case 0x00F461: return "G";
		case 0x00F462: return "G";
		case 0x00F463: return "G";
		case 0x00F464: return "G";
		case 0x00F465: return "G";
		case 0x00F466: return "G";
		case 0x00F467: return "G";
		case 0x00F468: return "G";
		case 0x00F469: return "G";
		case 0x00F46A: return "G";
		case 0x00F46B: return "G";
		case 0x00F46C: return "G";
		case 0x00F46D: return "S";
		case 0x00F48F: return "E";
		case 0x00F4A0: return "E";
		case 0x00F4C3: return "G";
		case 0x00F4C4: return "G";
		case 0x00F4D0: return "G";
		case 0x00F4D1: return "G";
		case 0x00F4D2: return "G";
		case 0x00F50A: return "E";
		case 0x00F50B: return "S";
		case 0x00F50C: return "S";
		case 0x00F50D: return "S";
		case 0x00F510: return "E";
		case 0x00F518: return "E";
		case 0x00F51A: return "E";
		case 0x00F51E: return "E";
		case 0x00F53E: return "S";
		case 0x00F5E0: return "E";
		case 0x00F430: return "A1";
		case 0x00F431: return "A2";
		case 0x00F432: return "A3";
		case 0x00F433: return "A4";
		case 0x00F434: return "A5";
		case 0x00F435: return "A6";
		case 0x00F436: return "A7";
		case 0x00F437: return "A8";
		case 0x00F438: return "A9";
		case 0x00F439: return "A10";
		case 0x00F43A: return "A11";
		case 0x00F43B: return "A12";
		case 0x00F43C: return "A13";
		case 0x00F43D: return "A14";
		case 0x00F43E: return "A15";
		case 0x00F43F: return "A16";
		case 0x00F598: return "A17";
		case 0x00F599: return "A18";
		case 0x00F59A: return "A19";
		case 0x00F59B: return "A20";
		case 0x00F593: return "A";
		case 0x00F595: return "A";
		case 0x00F594: return "A";
		case 0x00F596: return "A";
		case 0x00F55C: return "A";
		case 0x00F55D: return "A";
		case 0x00F62B: return "I";
		case 0x00F0E8: return "E";
		case 0x00F1D0: return "E";
		case 0x00F48D: return "E";
		case 0x00F57B: return "E";
		case 0x00F57F: return "E";
		case 0x00F58E: return "E";
		case 0xD00020: return "E1";
		case 0xD00021: return "E2";
		case 0xD00022: return "E3";
		case 0xD00023: return "E4";
		case 0xD00024: return "E5";
		case 0xD00025: return "E6";
		case 0xD00026: return "E7";
		case 0xD00027: return "E8";
		case 0xD00028: return "E9";
		case 0xD00029: return "E10";
		case 0xD0002A: return "E11";
		case 0xD0002B: return "E12";
		case 0xD0002C: return "E13";
		case 0xD0002D: return "E14";
		case 0xD0002E: return "E15";
		case 0xD0002F: return "E16";
		case 0xD00030: return "E17";
		case 0xD00031: return "E18";
		case 0xD00032: return "E19";
		case 0xD00033: return "E20";
		case 0xD000EB: return "E1";
		case 0xD000EC: return "E2";
		case 0xD000ED: return "E3";
		case 0xD000EE: return "E4";
		case 0xD000EF: return "E5";
		case 0xD000F0: return "E6";
		case 0xD000F1: return "E7";
		case 0xD000F2: return "E8";
		case 0xD000F3: return "E9";
		case 0xD000F4: return "E10";
		case 0xD000F5: return "E11";
		case 0xD000F6: return "E12";
		case 0xD000F7: return "E13";
		case 0xD000F8: return "E14";
		case 0xD000F9: return "E15";
		case 0xD000FA: return "E16";
		case 0xD000FB: return "E17";
		case 0xD000FC: return "E18";
		case 0xD000FD: return "E19";
		case 0xD000FE: return "E20";
		case 0xD0027D: return "E";
		case 0xD0027E: return "E";
		case 0xD0027F: return "E";
		case 0xD10066: return "E";
		case 0xD10120: return "E";
		case 0xD10293: return "E";
		case 0xD00109: return "E";
		case 0xD002AE: return "E";
		case 0xD00453: return "E";
		case 0xD100A0: return "E";
		case 0xD10104: return "E";
		case 0xD1013A: return "E";
		case 0xD10167: return "E";
		case 0x00F597: return "I";
		case 0x00F5C9: return "E";
		case 0x00F55E: return "I1";
		case 0x00F55F: return "I2";
		case 0x00F560: return "I3";
		case 0x00F561: return "I4";
		case 0x00F562: return "I5";
		case 0x00F563: return "I6";
		case 0x00F564: return "I7";
		case 0x00F565: return "I8";
		case 0x00F566: return "I9";
		case 0x00F567: return "I10";
		case 0x00F568: return "I11";
		case 0x00F569: return "I12";
		case 0x00F56A: return "I13";
		case 0x00F56B: return "I14";
		case 0x00F56C: return "I15";
		case 0x00F56D: return "I16";
		case 0x00F56E: return "I17";
		case 0x00F56F: return "I18";
		case 0x00F570: return "I19";
		case 0x00F571: return "I20";
		case 0x00F572: return "I21";
		case 0x00F573: return "I22";
		case 0x00F574: return "I23";
		case 0x00F575: return "I24";
		case 0xD003A5: return "E";
		case 0xD003A6: return "E";
		case 0xD0012F: return "E";
		case 0x00FC07: return "E";
		case 0x00FC08: return "E";
		case 0x00FC09: return "E";
		case 0x00FC0D: return "G";
		case 0x00FC0F: return "S";
		case 0x00FC10: return "G";
		case 0x00FC11: return "G";
		case 0x00FC12: return "G";
		case 0x00FC13: return "G";
		case 0x00FC14: return "G";
		case 0x00FC15: return "G";
		case 0x00FC16: return "G";
		case 0x00FC17: return "G";
		case 0x00FC18: return "G";
		case 0x00FC19: return "G";
		case 0x00FC1A: return "G";
		case 0x00FC1B: return "G";
		case 0x00FC1C: return "G";
		case 0x00FC1D: return "G";
		case 0x00FC1E: return "G";
		case 0x00FC1F: return "G";
		case 0x00FC27: return "E";
		case 0x00FC28: return "E";
		case 0x000015: return "E";
		case 0x00F013: return "S";
		case 0x00F11B: return "E";
		case 0x00F189: return "E";
		case 0x000041: return "E";
		case 0x000046: return "E";
		case 0x00004B: return "E";
		case 0x00004E: return "E";
		case 0x000053: return "E";
		case 0x000054: return "S";
		case 0x000055: return "E";
		case 0x000058: return "E";
		case 0x00005A: return "E";
		case 0x00005B: return "E";
		case 0x00005C: return "E";
		case 0x00005F: return "E";
		case 0x00005D: return "E";
		case 0x00F40E: return "E";
		case 0x00F410: return "E";
		case 0x00F411: return "E";
		case 0x00F412: return "E";
		case 0x00F415: return "E";
		case 0x00F419: return "E";
		case 0x00F41C: return "E";
		case 0x00F41F: return "E";
		case 0x00F44B: return "G";
		case 0x00F44E: return "E";
		case 0x00F44F: return "E";
		case 0x00F4C7: return "G";
		case 0x00F4C8: return "G";
		case 0x00F4C9: return "G";
		case 0x00F4CA: return "G";
		case 0x00F4CB: return "G";
		case 0x00F4CF: return "G";
		case 0x00F508: return "E";
		case 0x00F509: return "E";
		case 0x00F50E: return "E";
		case 0x00F50F: return "E";
		case 0x00F511: return "E";
		case 0x00F512: return "E";
		case 0x00F515: return "E";
		case 0x00F517: return "E";
		case 0x00F519: return "E";
		case 0x00F51B: return "E";
		case 0x00F51C: return "E";
		case 0x00F51D: return "E";
		case 0x00F51F: return "E";
		case 0x00F520: return "E";
		case 0x00F524: return "E";
		case 0x00F525: return "E";
		case 0x00F557: return "G";
		case 0x00F701: return "I1";
		case 0x00F702: return "I2";
		case 0x00F703: return "I3";
		case 0x00F704: return "I4";
		case 0x00F705: return "I5";
		case 0x00F706: return "I6";
		case 0x00F707: return "I7";
		case 0x00F708: return "I8";
		case 0x00F709: return "I9";
		case 0x00F70A: return "I10";
		case 0x00F70B: return "I11";
		case 0x00F70C: return "I12";
		case 0x00F70D: return "I13";
		case 0x00F70E: return "I14";
		case 0x00F70F: return "I15";
		case 0x00F710: return "I16";
		case 0x00F711: return "I";
		case 0x00F55A: return "I2";
		case 0x00F55B: return "I1";
		case 0x0000C8: return "E";
		case 0x00F59C: return "I1";
		case 0x00F59D: return "I2";
		case 0x00F59E: return "I3";
		case 0x00F59F: return "I4";
		case 0x00F5A0: return "I5";
		case 0x00F5A1: return "I6";
		case 0x00F5A2: return "I7";
		case 0x00F5A3: return "I8";
		case 0x00F5A4: return "I9";
		case 0x00F5A5: return "I10";
		case 0x00F5A6: return "I11";
		case 0x00F5A7: return "I12";
		case 0x00F5A8: return "I13";
		case 0x00F5A9: return "I14";
		case 0x00F5AA: return "I15";
		case 0x00F5AB: return "I16";
		case 0x00F4A2: return "E";
		case 0x00F4EA: return "E";
		case 0x00F513: return "E";
		case 0x00F57C: return "E";
		case 0x00F57E: return "E";
		case 0x00F5B1: return "E";
		case 0x00F5BA: return "E";
		case 0x00F61E: return "E";
		case 0x00FC2D: return "E";
		case 0xD00040: return "E1";
		case 0xD00041: return "E2";
		case 0xD00042: return "E3";
		case 0xD00043: return "E4";
		case 0xD00044: return "E5";
		case 0xD00045: return "E6";
		case 0xD00046: return "E7";
		case 0xD00047: return "E8";
		case 0xD00048: return "E9";
		case 0xD00049: return "E10";
		case 0xD0004A: return "E11";
		case 0xD0004B: return "E12";
		case 0xD0004C: return "E13";
		case 0xD0004D: return "E14";
		case 0xD0004E: return "E15";
		case 0xD0004F: return "E16";
		case 0xD00050: return "E17";
		case 0xD00051: return "E18";
		case 0xD00052: return "E19";
		case 0xD00053: return "E20";
		case 0xD00130: return "E";
		case 0xD00131: return "E";
		case 0xD004DB: return "E";
		case 0xD004DC: return "E";
		case 0xD0010A: return "E";
		case 0xD00281: return "E";
		case 0xD00375: return "E";
		case 0xD00377: return "E";
		case 0xD00378: return "E";
		case 0xD00379: return "E";
		case 0xD00418: return "E";
		case 0xD00419: return "E";
		case 0xD0041A: return "E";
		case 0xD00478: return "E";
		case 0xD00479: return "E";
		case 0xD0047A: return "E";
		case 0xD0047B: return "E";
		case 0xD0047C: return "E";
		case 0xD0047D: return "E";
		case 0xD0047E: return "E";
		case 0xD0047F: return "E";
		case 0xD00480: return "E";
		case 0xD00481: return "E";
		case 0xD00482: return "E";
		case 0xD00483: return "E";
		case 0xD005B3: return "E";
		case 0x00F516: return "E";
		case 0x00FC88: return "E";
		case 0xD005B1: return "E";
		case 0xD005B2: return "E";
		case 0x00F636: return "E";
		case 0xD00BB3: return "E";
		case 0xD00371: return "E";
		case 0xD0036F: return "E";
		case 0xD00370: return "E";
		case 0xD00D15: return "E";
		case 0xD00D16: return "E";
		case 0xD00D17: return "E";
		case 0xD00D18: return "E";
		case 0x00F5DB: return "E";
		case 0xD00B18: return "E";
		case 0xD0050B: return "I";
		case 0xD0050C: return "I";
		case 0xD0050D: return "I";
		case 0xD0050E: return "I";
		case 0xD0050F: return "I";
		case 0xD00510: return "I";
		case 0xD00511: return "I";
		case 0xD00512: return "I";
		case 0xD00513: return "I";
		case 0xD00514: return "I";
		case 0xD00515: return "I";
		case 0xD00516: return "I";
		case 0xD00517: return "I";
		case 0xD00518: return "I";
		case 0xD00519: return "I";
		case 0xD0051A: return "I";
		case 0xD010F0: return "I";
		case 0xD010F1: return "I";
		case 0xD010F2: return "I";
		case 0xD010F3: return "I";
		case 0xD00BB7: return "E";
		case 0xD00F5D: return "E";
		case 0xD00F5E: return "E";
		case 0xD00F5F: return "E";
		case 0xD00F60: return "E";
		case 0xD01075: return "E";
		case 0xD01076: return "E";
		case 0x00F5BB: return "E";
		case 0x00F4D5: return "E";
		case 0xD00D19: return "E";
		case 0xD00AF4: return "E";
		case 0xD00AF5: return "E";
		case 0xD00AF6: return "E";
		case 0xD004B5: return "E";
		case 0xD00B01: return "E";
		case 0x00F42C: return "V";
		case 0x00F021: return "V";
		case 0x00F428: return "V";
		case 0x00F02D: return "V";
		case 0x00F032: return "V";
		case 0x00F02B: return "V";
		case 0x00F42A: return "V";
		case 0x00F023: return "V";
		case 0x00F024: return "V";
		case 0x00F2FD: return "V";
		case 0x00F421: return "V";
		case 0x00F42E: return "V";
		case 0x00F5D6: return "V";
		case 0x00F5D9: return "V";
		case 0x00F026: return "V";
		case 0x00F025: return "V";
		case 0x00F062: return "V";
		case 0x00F022: return "V";
		case 0x00F422: return "V";
		case 0x00F0BA: return "V";
		case 0x00F066: return "V";
		case 0x00F029: return "V";
		case 0x00F45D: return "V";
		case 0x00F45F: return "V";
		case 0x00F45C: return "V";
		case 0x00F45E: return "V";
		case 0x00F027: return "V";
		case 0x00F450: return "V";
		case 0x00F452: return "V";
		case 0x00F451: return "V";
		case 0x00F453: return "V";
		case 0x000018: return "V";
		case 0x00F5B8: return "V";
		case 0x00F477: return "V";
		case 0x00F4FE: return "V";
		case 0x00F22A: return "E";
		case 0x00F22B: return "E";
		case 0x00F22C: return "E";
		case 0x00F30B: return "E";
		case 0x00F30C: return "E";
		case 0x00F312: return "E";
		case 0x00F313: return "E";
		case 0x00F2D4: return "E";
		case 0x00F4A4: return "E";
		case 0x00F4D3: return "E";
		case 0x00F4D4: return "E";
		case 0xD00F75: return "E";
		case 0xD004B7: return "E";
		case 0x000045: return "M";
		case 0x00F478: return "T";
		case 0x00F4B3: return "T";
		case 0x00F4B4: return "T";
		case 0x00F4B5: return "T";
		case 0x00F4B6: return "T";
		case 0xD00148: return "T";
		case 0xD00149: return "T";
		case 0xD0014A: return "T";
		case 0xD0014B: return "T";
		case 0xD0014C: return "T";
		case 0xD0014D: return "T";
		case 0xD0014E: return "T";
		case 0xD0014F: return "T";
		case 0xD00150: return "T";
		case 0xD00151: return "T";
	   	case 0xD001C7: return "V";
		case 0x00F2DD: return "V";
		case 0x00F2DE: return "V";
		case 0x00F803: return "V";
		case 0xD100CF: return "V";
		case 0xD100CD: return "V";
		case 0xD100CE: return "V";
		case 0xD100C5: return "V";
		case 0xD100C6: return "V";
		case 0xD100C7: return "V";
		case 0xD100C8: return "V";
		case 0xD100C9: return "V";
		case 0xD100CA: return "V";
		case 0x00F090: return "T";
		case 0x00F074: return "T";
		case 0x00F00D: return "T";
		case 0x00F154: return "T";
		case 0x00F153: return "T";
		case 0x00F212: return "T";
		case 0x00F290: return "T";
		case 0x00F1C4: return "T";
		case 0xD10050: return "T";
		case 0x00F2F6: return "T";
		case 0x00F07C: return "T";
		case 0x00F299: return "T";
		case 0x00F018: return "T";
		case 0x00F1D1: return "T";
		case 0x00F094: return "T";
		case 0x00F5DA: return "T";
		case 0x00F605: return "T";
		case 0xD0098D: return "V";
		case 0x00F2B3: return "E";
		case 0x00F031: return "E";
		case 0xD00189: return "E";
		case 0xD00188: return "E";
		case 0x00F287: return "V";
		case 0xD10051: return "V";
		case 0xD1005D: return "V";
		case 0xD10060: return "V";
		case 0xD100AB: return "V";
		case 0xD1004A: return "V";
		case 0xD100AA: return "V";
		case 0xD101FE: return "V";
		case 0xD1009A: return "V";
		case 0x000017: return "V";
		case 0x000059: return "E";
		case 0x00F2CE: return "E";
		case 0x00F514: return "E";
		case 0xD00D14: return "E";
		case 0x00F4FF: return "P";
		case 0x00F527: return "D";
		case 0xD00EEC: return "D";
		case 0x00F54F: return "T";
		case 0x00F427: return "E";
		case 0x00F603: return "C";
		case 0x00F604: return "C";
		case 0x00F4F7: return "C";
		case 0x00F5D8: return "T";
		case 0x00FC29: return "E";
		case 0xD016E9: return "E";
		case 0xD016EA: return "E";
		case 0xD016EB: return "E";
		case 0xD016EC: return "E";
		case 0xD016ED: return "E";
		case 0xD016EE: return "E";
		case 0xD016EF: return "E";
		case 0xD016F0: return "E";
		case 0xD016F1: return "E";
		case 0xD016F2: return "E";
		case 0xD016F3: return "E";
		case 0xD016F4: return "E";
		case 0xD016F5: return "E";
		case 0xD016F6: return "E";
		case 0xD016F7: return "E";
		case 0xD10D21: return "E";
		case 0xD10D22: return "E";
		case 0xD10D23: return "E";
		case 0xD10D24: return "E";
		case 0xD10D25: return "E";
		case 0xD10D26: return "E";
		case 0xD10D27: return "E";
		case 0xD00D3B: return "E";
		case 0x00F7FA: return "E";
		case 0x00F7FC: return "E";
		case 0x00F0F1: return "E";
		case 0x00F413: return "E";
		case 0x00F540: return "E";
		case 0x00F541: return "E";
		case 0x00F542: return "E";
		case 0x00FC21: return "E";
		case 0x00FC22: return "E";
		case 0xD004B6: return "E";
		case 0x00F457: return "T";
		case 0xD00D5A: return "T";
		case 0x00F58D: return "T";
		case 0xD100EC: return "T";
		case 0xD003E0: return "E";
		case 0xD003E1: return "E";
		case 0xD003E2: return "E";
		case 0xD003E3: return "E";
		case 0xD003E4: return "E";
		case 0xD003E5: return "E";
		case 0xD003E6: return "E";
		case 0xD003E7: return "E";
		case 0xD003E8: return "E";
		case 0xD003E9: return "E";
		case 0xD003EA: return "E";
		case 0xD003EB: return "E";
		case 0xD003EC: return "E";
		case 0xD003ED: return "E";
		case 0xD003EE: return "E";
		case 0xD003EF: return "E";
		case 0xD003F0: return "E";
		case 0xD003F1: return "E";
		case 0xD003F2: return "E";
		case 0xD003F3: return "E";
		case 0xD003F4: return "E";
		case 0xD003F5: return "E";
		case 0xD003F6: return "E";
		case 0xD003F7: return "E";
		case 0xD003F8: return "E";
		case 0xD003F9: return "E";
		case 0xD003FA: return "E";
		case 0xD003FB: return "E";
		case 0xD003FC: return "E";
		case 0xD003FD: return "E";
		case 0xD003FE: return "E";
		case 0xD003FF: return "E";
		case 0xD00400: return "E";
		case 0xD00401: return "E";
		case 0xD00402: return "E";
		case 0xD00403: return "E";
		case 0xD00404: return "E";
		case 0xD00405: return "E";
		case 0xD00406: return "E";
		case 0xD00407: return "E";
		case 0xD01078: return "E";
		case 0xD01258: return "E";
		case 0x00F4A5: return "E";
		case 0xD01257: return "E";
		case 0x00F2D5: return "E";
		case 0xD001A1: return "V";
		case 0x00F4E1: return "V";
		case 0x00F5C4: return "E";
		case 0xD00B3D: return "E";
		case 0xD00B3E: return "E";
		case 0xD009A1: return "V";
		case 0xD0099E: return "V";
		case 0x00F456: return "E";
		case 0xD0099F: return "V";
		case 0xD009A0: return "V";
		case 0xD01092: return "E";
		case 0xD009A2: return "V";
		case 0xD009A3: return "V";
		case 0x00FC2E: return "E";
		case 0x00F58B: return "E";
		case 0x00F2F7: return "E";
		case 0x00F2FA: return "E";
		case 0x00F002: return "T";
		case 0xD101EF: return "C";
		case 0x00F2FC: return "E";
		case 0x00F5D1: return "E";
		case 0x00F1AD: return "V";
		case 0x00F5B7: return "E";
		case 0x00F2C6: return "P";
		case 0xD00C81: return "E";
		case 0xD10018: return "E";
		case 0x00F4F8: return "E";
		case 0x00F231: return "E";
		case 0x00F257: return "E";
		case 0xD10230: return "C";
		case 0xD00259: return "V";
		case 0xD00261: return "V";
		case 0xD00260: return "V";
		case 0xD00262: return "V";
		case 0x00FC32: return "E";
		case 0x00FC33: return "E";
		case 0x00F602: return "T";
		case 0x00F191: return "V";
		case 0xD006BB: return "V";
		case 0xD00FCA: return "V";
		case 0xD00FCB: return "V";
		case 0xD005F5: return "E";
		case 0xD01192: return "E";
		case 0xD01255: return "E";
		case 0xD01079: return "E";
		case 0xD0109E: return "E";
		case 0xD10986: return "E";
		case 0xD0109F: return "E";
		case 0xD10987: return "E";
		case 0xD00D61: return "E";
		case 0xD01209: return "E";
		case 0xD0120A: return "E";
		case 0xD0120B: return "E";
		case 0xD0120C: return "E";
		case 0xD0120D: return "E";
		case 0xD0120E: return "E";
		case 0xD0120F: return "E";
		case 0xD01124: return "E";
		case 0xD01125: return "E";
		case 0xD0112C: return "E";
		case 0xD0112D: return "E";
		case 0xD0113A: return "E";
		case 0xD0113B: return "E";
		case 0xD0113C: return "E";
		case 0xD0113D: return "E";
		case 0xD0113E: return "E";
		case 0xD0113F: return "E";
		case 0xD01140: return "E";
		case 0xD01141: return "E";
		case 0xD0114A: return "E";
		case 0xD0114B: return "E";
		case 0xD0114C: return "E";
		case 0xD0114D: return "E";
		case 0xD01152: return "E";
		case 0xD01154: return "E";
		case 0xD01153: return "E";
		case 0xD01155: return "E";
		case 0xD0115A: return "E";
		case 0xD0115C: return "E";
		case 0xD0115B: return "E";
		case 0xD0115D: return "E";
		case 0xD0115E: return "E";
		case 0xD01160: return "E";
		case 0xD0115F: return "E";
		case 0xD01161: return "E";
		case 0xD01164: return "E";
		case 0xD01165: return "E";
		case 0x00000E: return "G";
		case 0x00000F: return "E";
		case 0x00F001: return "E";
		case 0x00F402: return "G";
		case 0xD00E9D: return "E";
		case 0x00F577: return "V";
		case 0x00F578: return "V";
		case 0x00F579: return "V";
		case 0x00F46F: return "V";
		case 0x00F425: return "V";
		case 0x00F426: return "V";
		case 0x00F00E: return "E";
		case 0x00F455: return "V"; 
		case 0x00F4B2: return "T";
		}

	return "S";
	}

BOOL CCatLinkBase::SignExtendByte(UINT uName)
{
	switch( uName ) {

		case 0x000D:
		case 0xF108:
		case 0xF109:
		case 0xF10A:
		case 0xF10B:
		case 0xF10C:
		case 0xF10D:
		case 0xF11C:
		case 0xF11D:
		case 0xF1D0:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkBase::SignExtendWord(UINT uName)
{
	switch( uName ) {

		case 0x000C:
		case 0x0044:
		case 0x004D:
		case 0xF023:
		case 0xF024:
		case 0xF02B:
		case 0xF02D:
		case 0xF032:
		case 0xF2FD:
		case 0xF420:
		case 0xF421:
		case 0xF425:
		case 0xF426:
		case 0xF427:
		case 0xF428:
		case 0xF430:
		case 0xF431:
		case 0xF432:
		case 0xF433:
		case 0xF434:
		case 0xF435:
		case 0xF436:
		case 0xF437:
		case 0xF438:
		case 0xF439:
		case 0xF43A:
		case 0xF43B:
		case 0xF43C:
		case 0xF43D:
		case 0xF43E:
		case 0xF43F:
		case 0xF440:
		case 0xF441:
		case 0xF451:
		case 0xF452:
		case 0xF456:
		case 0xF45C:
		case 0xF46F:
		case 0xF4A0:
		case 0xF4A2:
		case 0xF4C7:
		case 0xF4C8:
		case 0xF4C9:
		case 0xF4CA:
		case 0xF4CB:
		case 0xF4E1:
		case 0xF509:
		case 0xF511:
		case 0xF51D:
		case 0xF527:
		case 0xF53E:
		case 0xF542:
		case 0xF54F:
		case 0xF557:
		case 0xF55C:
		case 0xF55D:
		case 0xF55E:
		case 0xF55F:
		case 0xF560:
		case 0xF561:
		case 0xF562:
		case 0xF563:
		case 0xF564:
		case 0xF565:
		case 0xF566:
		case 0xF567:
		case 0xF568:
		case 0xF569:
		case 0xF56A:
		case 0xF56B:
		case 0xF56C:
		case 0xF56D:
		case 0xF56E:
		case 0xF56F:
		case 0xF570:
		case 0xF571:
		case 0xF572:
		case 0xF573:
		case 0xF574:
		case 0xF575:
		case 0xF58B:
		case 0xF593:
		case 0xF594:
		case 0xF595:
		case 0xF596:
		case 0xF597:
		case 0xF598:
		case 0xF599:
		case 0xF59A:
		case 0xF59B:
		case 0xF5BA:
		case 0xF5BB:
		case 0xF5C9:
		case 0xF5D9:
		case 0xF5DB:
		case 0xD00040:
		case 0xD00041:
		case 0xD00042:
		case 0xD00043:
		case 0xD00044:
		case 0xD00045:
		case 0xD00046:
		case 0xD00047:
		case 0xD00048:
		case 0xD00049:
		case 0xD0004A:
		case 0xD0004B:
		case 0xD0004C:
		case 0xD0004D:
		case 0xD0004E:
		case 0xD0004F:
		case 0xD00050:
		case 0xD00051:
		case 0xD00052:
		case 0xD00053:
		case 0xD001A1:
		case 0xD00377:
		case 0xD00378:
		case 0xD003F4:
		case 0xD003F5:
		case 0xD003F6:
		case 0xD003F7:
		case 0xD003F8:
		case 0xD003F9:
		case 0xD003FA:
		case 0xD003FB:
		case 0xD003FC:
		case 0xD003FD:
		case 0xD003FE:
		case 0xD003FF:
		case 0xD00400:
		case 0xD00401:
		case 0xD00402:
		case 0xD00403:
		case 0xD00404:
		case 0xD00405:
		case 0xD00406:
		case 0xD00407: 
		case 0xD00418:
		case 0xD00419:
		case 0xD0041A:
		case 0xD00453:
		case 0xD00478:
		case 0xD00479:
		case 0xD0047A:
		case 0xD0047B:
		case 0xD0047C:
		case 0xD0047D:
		case 0xD0047E:
		case 0xD0047F:
		case 0xD00480:
		case 0xD00481:
		case 0xD00482:
		case 0xD00483:
		case 0xD004B5:
		case 0xD005B3:
		case 0xD0099F:
		case 0xD009A0:
		case 0xD009A2:
		case 0xD009A3:
		case 0xD00B3D:
		case 0xD00B3E:
		case 0xD0099E:
		case 0xD009A1:
		case 0xD01075:
		case 0xD01076:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkBase::ReOrderWord(UINT uName)
{
	switch( uName ) {

		case 0xD10066:	
		case 0xD0012F:
		case 0xD0098D:
		case 0xD00D61:
		case 0xD0109E:
		case 0xD0109F:
		case 0xD10018:
		case 0xD1004A:
		case 0xD10050:
		case 0xD10051:
		case 0xD1005D:
		case 0xD10060:
		case 0xD1009A:
		case 0xD100A0:
		case 0xD100AA:
		case 0xD100AB:
		case 0xD100C5:
		case 0xD100C6:
		case 0xD100C7:
		case 0xD100C8:
		case 0xD100C9:
		case 0xD100CA:
		case 0xD100CD:
		case 0xD100CE:
		case 0xD100CF:
		case 0xD100EC:
		case 0xD10104:
		case 0xD10120:
		case 0xD1013A:
		case 0xD10167:
		case 0xD101EF:
		case 0xD101FE:
		case 0xD10230:
		case 0xD10293:
		case 0xD10986:
		case 0xD10987:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkBase::ReOrderLong(UINT uName)
{
	return FALSE;
	}

BOOL CCatLinkBase::IsDataValid(UINT uName, UINT uData)
{
	switch( uName ) {

		case 0x00F53E:

			return (uData <= 250) ? TRUE : FALSE;
		}

	return TRUE;
	}

// Data and Name Access

UINT CCatLinkBase::GetNameLen(BYTE bData)
{
	if( bData >= 0x01 && bData <= 0x1F ) {

		return 1;
		}

	if( bData >= 0x21 && bData <= 0x5F ) {

		return 1;
		}

	if( bData >= 0xC8 && bData <= 0xC8 ) {

		return 1;
		}

	if( bData >= 0xF0 && bData <= 0xF3 ) {

		return 2;
		}

	if( bData >= 0xF4 && bData <= 0xF7 ) {

		return 2;
		}

	if( bData >= 0xFC ) {

		return 2;
		}

	if( bData >= 0xD0 && bData <= 0xD1 ) {

		return 3;
		}

	return 0;
	}

UINT CCatLinkBase::GetDataLen(BYTE bData)
{
	if( bData >= 0x01 && bData <= 0x1F ) {

		return 1;
		}

	if( bData >= 0x21 && bData <= 0x5F ) {

		return 2;
		}

	if( bData >= 0xC8 && bData <= 0xC8 ) {

		return 4;
		}

	if( bData >= 0xF0 && bData <= 0xF3 ) {

		return 1;
		}

	if( bData >= 0xF4 && bData <= 0xF7 ) {

		return 2;
		}

	if( bData >= 0xFC ) {

		return 4;
		}

	if( bData >= 0xD0 && bData <= 0xD1 ) {

		return 2;
		}

	return 0;
	}

UINT CCatLinkBase::GetNameLen(UINT uName)
{
	if( uName >= 0x000001 && uName <= 0x00001F ) {

		return 1;
		}

	if( uName >= 0x000021 && uName <= 0x00005F ) {

		return 1;
		}

	if( uName >= 0x0000C8 && uName <= 0x0000C8 ) {

		return 1;
		}

	if( uName >= 0x00F000 && uName <= 0x00F3FF ) {

		return 2;
		}

	if( uName >= 0x00F400 && uName <= 0x00F7FF ) {

		return 2;
		}

	if( uName >= 0x00FC00 && uName <= 0x00FFFF ) {

		return 2;
		}

	if( uName >= 0xD00000 && uName <= 0xD1FFFF ) {

		return 3;
		}

	return 0;
	}

UINT CCatLinkBase::GetDataLen(UINT uName)
{
	if( uName >= 0x000001 && uName <= 0x00001F ) {

		return 1;
		}

	if( uName >= 0x000021 && uName <= 0x00005F ) {

		return 2;
		}

	if( uName >= 0x0000C8 && uName <= 0x0000C8 ) {

		return 4;
		}

	if( uName >= 0x00F000 && uName <= 0x00F3FF ) {

		return 1;
		}

	if( uName >= 0x00F400 && uName <= 0x00F7FF ) {

		return 2;
		}

	if( uName >= 0x00FC00 && uName <= 0x00FFFF ) {

		return 4;
		}

	if( uName >= 0xD00000 && uName <= 0xD1FFFF ) {

		return 2;
		}

	return 0;
	}

UINT CCatLinkBase::GetNameBytes(PCBYTE pName, UINT uLen)
{
	if( uLen == 1 ) {

		return pName[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pName[1],
				 pName[0]
				 );
		}
	
	if( uLen == 3 ) {

		return MAKELONG( MAKEWORD( pName[2],
					   pName[1]
					   ),
				 MAKEWORD( pName[0],
					   0
					   )
				 );
		}

	return NOTHING;
	}

UINT CCatLinkBase::GetDataBytes(PCBYTE pData, UINT uLen)
{
	if( uLen == 1 ) {

		return pData[0];
		}

	if( uLen == 2 ) {

		return MAKEWORD( pData[0],
				 pData[1]
				 );
		}
	
	if( uLen == 4 ) {

		return MAKELONG( MAKEWORD(pData[0], pData[1]),
				 MAKEWORD(pData[2], pData[3])
				 );
		}

	return 999999;
	}

void CCatLinkBase::SetNameBytes(PBYTE pName, UINT uName, UINT uLen)
{
	if( uLen == 1 ) {

		pName[0] = LOBYTE(uName);
		}

	if( uLen == 2 ) {

		pName[0] = HIBYTE(uName);
		pName[1] = LOBYTE(uName);
		}
	
	if( uLen == 3 ) {

		pName[0] = LOBYTE(HIWORD(uName));
		pName[1] = HIBYTE(LOWORD(uName));
		pName[2] = LOBYTE(LOWORD(uName));
		}
	}

void CCatLinkBase::SetDataBytes(PBYTE pData, UINT uData, UINT uLen)
{
	if( uLen == 1 ) {

		pData[0] = LOBYTE(uData);
		}

	if( uLen == 2 ) {

		pData[0] = LOBYTE(uData);
		pData[1] = HIBYTE(uData);
		}
	
	if( uLen == 4 ) {

		pData[0] = LOBYTE(LOWORD(uData));
		pData[1] = HIBYTE(LOWORD(uData));
		pData[2] = LOBYTE(HIWORD(uData));
		pData[3] = HIBYTE(HIWORD(uData));
		}
	}

// End of File
