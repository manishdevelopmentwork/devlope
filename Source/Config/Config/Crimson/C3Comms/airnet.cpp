
#include "intern.hpp"

#include "airnet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader(); 

//////////////////////////////////////////////////////////////////////////
//
// Dometic Air Net Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CAirNetDriverOptions, CUIItem);

// Constructor

CAirNetDriverOptions::CAirNetDriverOptions(void)
{
	m_Source = 1;
	}

// Download Support

BOOL CAirNetDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Source));

	return TRUE;
	}

// Meta Data Creation

void CAirNetDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);
	}


//////////////////////////////////////////////////////////////////////////
//
// Dometic Air Net Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CAirNetDeviceOptions, CUIItem);

// Constructor

CAirNetDeviceOptions::CAirNetDeviceOptions(void)
{
	m_ID	= 10;

	m_Def	= 0;
	}

// UI Managament

void CAirNetDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "ID" ) {

		BOOL fEnable = pItem->GetDataAccess("ID")->ReadInteger(pItem) != 0;
		
		if( !fEnable ) {
			
			pItem->GetDataAccess("Def")->WriteInteger(pItem, 2);
			}
		
		else if( pItem->GetDataAccess("Def")->ReadInteger(pItem) == 2 ) {

			pItem->GetDataAccess("Def")->WriteInteger(pItem, 0);
			}
				 
		pWnd->EnableUI("Def", fEnable);

		pWnd->UpdateUI();
		}
	
	if( Tag.IsEmpty() || Tag == "Def" ) {

		BOOL fBroad  = pItem->GetDataAccess("Def")->ReadInteger(pItem) == 2;

		if( fBroad ) {

			pItem->GetDataAccess("ID")->WriteInteger(pItem, 0);
			}

		pWnd->EnableUI("Def", !fBroad);

		pWnd->UpdateUI();
		}
	}

// Download Support

BOOL CAirNetDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_ID));

	Init.AddByte(BYTE(m_Def));

	return TRUE;
	}

// Meta Data Creation

void CAirNetDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(ID);

	Meta_AddInteger(Def);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dometic AirNet CAN Bus Driver
//

// Instantiator

ICommsDriver *	Create_DometicAirNetDriver(void)
{
	return New CAirNetDriver;
	}


// Constructor

CAirNetDriver::CAirNetDriver(void)
{
	m_wID		= 0x4061;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Dometic";
	
	m_DriverName	= "AirNet CAN";
	
	m_Version	= "1.01";
	
	m_ShortName	= "AirNet";

	AddSpaces();
	}

// Binding Control

UINT CAirNetDriver::GetBinding(void)
{
	return bindCAN;
	}

void CAirNetDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 250000;

	CAN.m_Terminate = FALSE;
	}

// Configuration

CLASS CAirNetDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CAirNetDriverOptions);
	}


CLASS CAirNetDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CAirNetDeviceOptions);
	}


// Implementation

// Commented out spaces need not be implemented since Eskimo Ice Machine will not be used.

void CAirNetDriver::AddSpaces(void)     
{
	AddSpace(New CSpace(1,	"HOMW", "HVAC Op Mode\t(Write Only)",	10,  1,  1, addrByteAsByte));
	AddSpace(New CSpace(2,	"HFMW", "HVAC Fan Mode\t(Write Only)",	10,  2,  2, addrByteAsByte));
	AddSpace(New CSpace(3,	"HFSW", "HVAC Fan Speed\t(Write Only)",	10,  3,  3, addrByteAsByte));
	AddSpace(New CSpace(4,  "HSTW", "HVAC Spt Temp\t(Write Only)",	10,  4,  4, addrByteAsByte));
	AddSpace(New CSpace(5,	"SHVAC","Send HVAC Cmd\t(Write Only)",	10,  5,  5, addrBitAsBit));
	AddSpace(New CSpace(6,	"HCFR", "HVAC Config\t(Read Only)",	10,  6,  6, addrByteAsByte));
	AddSpace(New CSpace(7,	"HMR",  "HVAC Mode\t(Read Only)",	10,  7,  7, addrByteAsByte));
	AddSpace(New CSpace(8,	"HSR",  "HVAC Status\t(Read Only)",	10,  8,  8, addrByteAsByte));
	AddSpace(New CSpace(9,	"HFMR", "HVAC Fan Mode\t(Read Only)",	10,  9,  9, addrByteAsByte));
	AddSpace(New CSpace(10,	"HFSR", "HVAC Fan Speed\t(Read Only)",	10, 10, 10, addrByteAsByte));
	AddSpace(New CSpace(11, "HSTR", "HVAC Spt Temp\t(Read Only)",	10, 11, 11, addrByteAsByte));
	AddSpace(New CSpace(12,	"HAMR", "HVAC Amb Temp\t(Read Only)",	10, 12, 12, addrByteAsByte));
	AddSpace(New CSpace(13,	"HOTR", "HVAC Out Temp\t(Read Only)",	10, 13, 13, addrByteAsByte));
	AddSpace(New CSpace(14,	"HFR",  "HVAC Faults\t(Read Only)",	10, 14, 14, addrByteAsByte));
	AddSpace(New CSpace(15,	"HATR", "HVAC Add Temps\t(Read Only)",	10, 15, 15, addrByteAsByte));
	
//	AddSpace(New CSpace(16,	"ISW",  "IceM State\t(Write Only)",	10, 16, 16, addrByteAsByte));
//	AddSpace(New CSpace(17,	"ISR",  "IceM State\t(Read Only)",	10, 17, 17, addrByteAsByte));
//	AddSpace(New CSpace(18,	"IHSR", "IceM Hdwr State\t(Read Only)",	10, 18, 18, addrByteAsByte));
//	AddSpace(New CSpace(19,	"IFR",  "IceM Faults\t(Read Only)",	10, 19, 19, addrByteAsByte));
//	AddSpace(New CSpace(20,	"ILVR", "IceM Line Volts\t(Read Only)",	10, 20, 20, addrByteAsByte));
//	AddSpace(New CSpace(21,	"ICCR", "IceM Comp Curr\t(Read Only)",	10, 21, 21, addrByteAsByte));
//	AddSpace(New CSpace(22,	"IACR", "IceM Auger Curr\t(Read Only)",	10, 22, 22, addrByteAsByte));

	AddSpace(New CSpace(23,	"SNR",	"Serial Number\t(Read Only)",	10, 23, 23, addrLongAsLong));
	AddSpace(New CSpace(24,	"UIDR", "Unit ID\t(Read Only)",		10, 24, 24, addrByteAsByte));
	AddSpace(New CSpace(25,	"GIDR", "Group ID\t(Read Only)",	10, 25, 25, addrByteAsByte));
	AddSpace(New CSpace(26,	"DIDR", "Device ID\t(Read Only)",	10, 26, 26, addrWordAsWord));
	AddSpace(New CSpace(27,	"BRR",  "Boot Rev\t(Read Only)",	10, 27, 27, addrByteAsByte));
	AddSpace(New CSpace(28,	"CRR",  "Code Rev\t(Read Only)",	10, 28, 28, addrByteAsByte));

	AddSpace(New CSpace(29,	"UACW", "Unit Addr Chg\t(Write Only)",	10, 29, 29, addrByteAsByte));
	AddSpace(New CSpace(30, "GACW", "Group Addr Chg\t(Write Only)", 10, 30, 30, addrByteAsByte));
	AddSpace(New CSpace(31, "SM",	"Silent Mode\t(Write Only)",	10, 31, 31, addrBitAsBit));

//	AddSpace(New CSpace(32, "RM",   "Read Memory",			16, 0, 0xFFFF, addrByteAsByte, addrByteAsLong));
	}


// End of File
