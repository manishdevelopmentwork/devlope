
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_LangItem_HPP

#define INCLUDE_LangItem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Language Codes
//

#define	C3L_SYSTEM		0x0000
#define	C3L_GENERIC		0x0001
#define	C3L_ENGLISH		0x0100
#define	C3L_ENGLISH_US		0x0101
#define	C3L_ENGLISH_UK		0x0102
#define	C3L_ENGLISH_IN		0x0103
#define	C3L_FRENCH		0x0200
#define	C3L_FRENCH_FR		0x0201
#define	C3L_FRENCH_CA		0x0202
#define	C3L_FRENCH_BE		0x0203
#define	C3L_FRENCH_CH		0x0204
#define	C3L_SPANISH		0x0300
#define	C3L_SPANISH_ES		0x0301
#define	C3L_SPANISH_MX		0x0302
#define	C3L_GERMAN		0x0400
#define	C3L_GERMAN_DE		0x0401
#define	C3L_GERMAN_AT		0x0402
#define	C3L_GERMAN_CH		0x0403
#define	C3L_ITALIAN		0x0500
#define	C3L_ITALIAN_IT		0x0501
#define	C3L_ITALIAN_CH		0x0502
#define	C3L_FINNISH		0x0600
#define	C3L_SIMP_CHINESE	0x8101
#define	C3L_TRAD_CHINESE	0x8201
#define	C3L_RUSSIAN		0x8301
#define	C3L_KOREAN_JAMO		0x8401
#define	C3L_KOREAN_FULL		0x8402
#define C3L_JAPANESE		0x8500

//////////////////////////////////////////////////////////////////////////
//
// Language Item
//

class DLLNOT CLangItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLangItem(void);

		// Attributes
		BOOL HasLowerDiacriticals(void) const;
		BOOL HasUpperDiacriticals(void) const;

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Persistance
		void PostLoad(void);

		// Data Members
		UINT    m_Lang;
		CString m_Code;
		UINT    m_Keyb;
		UINT    m_Num;
		UINT    m_Upper;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
