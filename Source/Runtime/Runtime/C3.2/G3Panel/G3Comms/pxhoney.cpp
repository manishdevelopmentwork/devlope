
#include "intern.hpp"

#include "proxy.hpp"

#include "events.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//					
// Special Function Block Numbers
//

const int BLKNUM_MBTCP_SLVSTATUS = 38;

const int BLKNUM_MBSER_SLVSTATUS = 39;

const int SYS_BLOCK_SEQ		 = 32000;

const int SYS_BLOCK_SPS		 = 32001;

const int SYS_BLOCK_SPP		 = 32002;

const int SYS_BLOCK_RCP		 = 32003;

//////////////////////////////////////////////////////////////////////////
//					
// Time Conversion Bias
//

#define	TIME_ADJUST		852076800

//////////////////////////////////////////////////////////////////////////
//					
// Error Codes
//

enum
{
	errorCannotOpen		= -1,
	errorNoWorkspace	= -2,
	errorFileHeader		= -3,
	errorAbort		= -4,
	errorCommsFailure	= -5,
	errorBadSchema		= -6,
	errorBadMode		= -7,
	errorClearFailed        = -8,
	errorValidateFailed	= -9,
	errorBadCode		= -10,
	errorRestartRejected    = -11,
	errorRestartFailed      = -12,
	errorClearBusy          = -13,
	};

//////////////////////////////////////////////////////////////////////////
//					
// Honeywell Database Objects
//

#pragma pack(1)

struct CHeadTab
{
	WORD	Type;
	WORD	Number;
	WORD	Count;
	WORD	Size;
	DWORD	ModCount;
	BYTE	RevNum;
	BYTE	InUse;
	WORD	CRC;
	};

struct CHeadSPP
{
	WORD	wType;
	WORD	wNumber;
	WORD	wRecordCount;
	WORD	wRecordSize;
	char	sLabel[8];
	char	sDesc [16];
	char    sPriDesc [8];
	char    sPriUnits[4];
	long	lTimeStamp;
	DWORD	rGuarSoakLo;
	DWORD	rGuarSoakHi;
	DWORD	rRestartRate;
	BYTE	bJogSeg;
	BYTE	bLoopStart;
	BYTE	bLoopEnd;
	BYTE	bRepeats;
	char	sAuxDesc [8];
	char    sAuxUnits[4];
	DWORD	rFixed;
	DWORD	rDispHiLim;
	DWORD	rDispLoLim;
	BYTE	bTimeUnits;
	BYTE	bRampTime;
	BYTE	bGuardType;
	BYTE	bSpare1;
	WORD	wSpare2;
	WORD	CRC;
	};

struct CHeadSPS
{
	WORD	wType;
	WORD	wNumber;
	WORD	wRecordCount;
	WORD	wRecordSize;
	char	sLabel[8];
	char	sDesc [16];
	DWORD   rGuard[8];
	BYTE	bTimeUnits;
	BYTE	bJogStep;
	WORD	CRC;
	};

struct CHeadSEQ
{
	WORD	wType;
	WORD	wNumber;
	WORD	wRecordCount;
	WORD	wRecordSize;
	char	sLabel[8];
	char	sDesc [16];
	BYTE	bTimeUnits;
	BYTE	bJogStep;
	WORD	CRC;
	};

struct CHeadRcp
{
	WORD	wType;
	WORD	wNumber;
	WORD	wRecordCount;
	WORD	wRecordSize;
	char	sLabel[8];
	char	sDesc [16];
	};

struct CDataSPP
{
	BYTE	RampSegment;
	BYTE	GuarSoakEnable;
	WORD	Events;
	DWORD	TimeOrRate;
	DWORD	StartValue;
	DWORD	AuxValue;
	};

struct CDataSPS
{
	DWORD	SegmentTime;
	BYTE	GuarSoakType[8];
	WORD	RecycleCount;
	WORD	RecycleSegment;
	DWORD	StartValue[8];
	DWORD	AuxValue[8];
	WORD	Events;
	WORD	Padding;
	};

struct CDataSEQ
{
	BYTE	PhaseNum;
	BYTE	TimeNextStep;
	BYTE	Sig1NextStep;
	BYTE	Sig2NextStep;
	BYTE	AdvNextStep;
	BYTE	Spare[3];
	DWORD	Time;
	DWORD	Aux;
	};

struct CDataRcp
{
	WORD	VarNum;
	WORD	Dummy;
	DWORD	Value;
	};

#pragma	pack()

//////////////////////////////////////////////////////////////////////////
//
// Honeywell Proxy Object
//

#if defined(_HON)

class CProxyHoneywell : public CProxy, public IEventSource
{
	public:
		// Constructor
		CProxyHoneywell(BOOL fNet);

		// Destructor
		~CProxyHoneywell(void);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Term(UINT uID);

		// Suspension
		void Suspend(void);
		void Resume(void);

		// Control
		UINT Control(void *pContext, UINT uFunc, PCTXT pValue);

		// IEventSource
		BOOL GetEventText(CUnicode &Text, UINT Code);
		void AcceptAlarm (UINT Method, UINT Code);
		void AcceptAlarm (CActiveAlarm *pInfo);

	protected:
		// Static Data
		static WORD const m_ListSEQ[];
		static WORD const m_ListSPS[];
		static WORD const m_ListSPP[];

		// Comms States
		enum
		{
			stateOffline   = 0,
			stateWaitBeat  = 1,
			stateCheckTabs = 2,
			stateMismatch  = 3,
			stateRecipes   = 5,
			stateOnline    = 4,
			stateNoDevice  = 6,
			stateUpload    = 7,
			stateDownload  = 8,
			stateRestart   = 9,
			stateGetAlarms = 10,
			};

		// Heartbeat Flags
		enum
		{
			beatDataValid = 0x00800000,
			beatNewConfig = 0x00200000,
			beatNewRecipe = 0x00100000,
			beatNewEvents = 0x00004000,
			beatNewAlarms = 0x00002000,
			};

		// Layout Data
		struct CLayout
		{
			UINT	m_uMin;
			UINT	m_uMax;
			UINT	m_uPos;
			UINT	m_uLen;
			};

		// Dynamic Table
		struct CTab
		{
			BYTE	m_bTable;
			WORD	m_wRecord;
			BOOL	m_fValid;
			UINT	m_uAlloc;
			WORD	m_wCRC;
			UINT	m_uSize;
			UINT	m_uCount;
			UINT	m_uVersion;
			PBYTE	m_pHead;
			PBYTE	m_pData;
			};

		// Recipe Data
		struct CRecipe
		{
			BYTE	m_bTable;
			UINT	m_uCount;
			PDWORD	m_pText;
			PWORD   m_pNumb;
			};

		// SPP Data Block
		struct CSPP
		{
			WORD	m_Block;
			BOOL	m_fScan;
			BOOL    m_fValid;
			BYTE	m_bSeg[50][16];
			};

		// SPS Data Block
		struct CSPS
		{
			WORD	m_Block;
			BOOL	m_fScan;
			BOOL    m_fValid;
			BYTE	m_bSeg[50][84];
			CTab    m_Disp;
			};

		// SEQ Data Block
		struct CSEQ
		{
			WORD	m_Block;
			BOOL	m_fScan;
			BOOL    m_fValid;
			BYTE	m_bSeg[64][16];
			CTab	m_Tags;
			};

		// Bound Items
		CCommsDevice       * m_pDev;
		CCommsSysBlockList * m_pList;
		IEventConsumer     * m_pEvents;
		UINT                 m_Source;

		// Config Data
		DWORD	  m_IPAddr1;
		DWORD	  m_IPAddr2;
		BOOL	  m_fFlip;
		BOOL	  m_fQuick;
		BYTE	  m_HMI;
		BOOL	  m_fAlarms;
		UINT      m_uSlot;
		PWORD     m_pSlot;
		UINT      m_uBlob;
		PBYTE     m_pBlob;
		PCWORD    m_pCheck;
		UINT	  m_uVersion;
		WORD	  m_wNetBlock;
		BOOL	  m_fOldBeat;
		BOOL	  m_fOldAlarms;

		// Data Members
		BOOL	       m_fNet;
		UINT	       m_uState;
		UINT	       m_uFrom;
		ISocket	     * m_pSock;
		IDataHandler * m_pHand;
		BYTE	       m_bTxData[300];
		CBuffer      * m_pRxBuff;
		BYTE	     * m_pRxData;
		WORD	       m_wTrans;
		UINT	       m_uPtr;
		UINT           m_uCheck;
		PCWORD         m_pRecs;
		PCWORD         m_pPeer;
		PCWORD         m_pPPO;
		BYTE	       m_MisMode;

		// CDE Transfer
		UINT  m_uCDE;
		char  m_sCDE[128];
		UINT  m_uResult;
		UINT  m_uPhase;
		UINT  m_uProgress;
		UINT  m_uTable;
		BYTE  m_bSendID[4];
		UINT  m_uWaitTime;

		// Heartbeat Data
		DWORD m_dwBeat;

		// Time Data
		DWORD m_Time;
		DWORD m_Bias;

		// Editing Buffers
		DWORD m_WorkSPP[64 ];
		DWORD m_WorkSPS[64 ];
		DWORD m_WorkSEQ[64 ];
		DWORD m_WorkRcp[106];

		// HC Config Data
		CTab m_TabSignal;
		CTab m_TabDesc;
		CTab m_TabEvents;
		CTab m_TabAlarms;

		// Recipe Data
		BOOL    m_fRecipe;
		CRecipe m_RecRcp;
		CRecipe m_RecSpp;
		CRecipe m_RecSps;
		CRecipe m_RecSeq;

		// Function 03 Data
		UINT m_ReqCount[2];
		UINT m_ReqBlock[2][60];
		UINT m_ReqIndex[2][60];

		// Function 05 Data
		UINT   m_nSPP;
		CSPP * m_pSPP;

		// Function 07 Data
		BYTE m_FileHead[16];

		// Function 18 Data
		UINT   m_nSPS;
		CSPS * m_pSPS;

		// Function 26 Data
		UINT   m_nSEQ;
		CSEQ * m_pSEQ;

		// Alarms and Events
		UINT   m_uAlarms;
		PDWORD m_pAlarms;
		UINT   m_uAckHead;
		UINT   m_uAckTail;
		UINT   m_uAck[50];

		// Service Calls
		BOOL ServiceDriver(void);
		BOOL ServiceBlock(UINT b);
		BOOL ServiceGlobal(void);
		BOOL Invalidate(BOOL fAll);
		BOOL TakeOffline(void);

		// Configuration
		BOOL CopyConfig(void);
		void ReadParams(void);

		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddData(DWORD dwData);
		void AddData(PCBYTE pData, UINT uCount);
		void CloseFrame(void);

		// Transport Layer
		BOOL Transact(BOOL fCheck);
		BOOL CloseSocket(void);
		BOOL CheckSocket(BOOL fSwitch);
		BOOL CycleSocket(void);
		void FreeRx(void);

		// Application Layer
		void StartRead(BYTE bFunc);
		void StartWrite(BYTE bFunc);
		BOOL CheckReply(void);
		BOOL ShowError(PCTXT pForm, ...);

		// Table Access
		BOOL ReadTable(CTab &Tab, BOOL fData);
		void FreeTable(CTab &Tab);
		void WipeTable(CTab &Tab);
		BOOL ReadTable(BYTE bType, BYTE bTable, WORD wRecord, PBYTE pData, UINT uSize);
		BOOL ReadTable(BYTE bTable, WORD wRecord, PBYTE pData, UINT uSize);
		BOOL SendTable(WORD wTable, BYTE bType, WORD wRecord, PBYTE pData, UINT uSize);
		BOOL SendTable(WORD wTable, BYTE bType, WORD wRecord, IFile *pFile, UINT uSize);

		// HC Config Check
		void CheckStart(void);
		BOOL CheckTable(void);
		BOOL IsCheckDone(void);

		// Heartbeat
		BOOL Heartbeat(BOOL fCheck);
		BOOL SendHB1(void);
		BOOL SendHB2(void);

		// Driver Data
		BOOL ScanDriver(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr, BOOL fOnline);

		// HC Config Data
		void  InitConfig(void);
		BOOL  ReadConfig(void);
		BOOL  SyncAlarmHistory(void);
		void  WipeConfig(void);
		void  KillConfig(void);
		PCTXT FindSigName(WORD wSig);
		BOOL  FindSigName(PTXT pText, WORD wSig);
		BOOL  FindSigDesc(PTXT pText, WORD wSig);
		BYTE  FindSigMode(WORD wSig);

		// Recipe Data
		BOOL InitRecipes(void);
		BOOL KillRecipes(void);
		BOOL ScanRecipes(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr);
		BOOL ReadRecipes(void);
		void InitRecipe(CRecipe &Rec);
		void KillRecipe(CRecipe &Rec);
		BOOL ScanRecipe(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr, CRecipe &Rec);
		BOOL ReadRecipe(CRecipe &Rec);

		// Function Code 03
		void InitFunc03(void);
		void KillFunc03(void);
		void WipeFunc03(BOOL fNew);
		BOOL ScanFunc03(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr, PCWORD pList);
		BOOL ScanFunc03(void);
		BOOL ReadFunc03(UINT t);
		BOOL ReadFunc03(UINT b, PDWORD pData, PCWORD pProps, UINT uProps);
		BOOL ReadFunc03(BYTE bType, WORD Block, WORD Prop, DWORD &dwData);
		BOOL SendFunc03(CAddress const &Addr, DWORD dwData);
		BOOL SendFunc03(UINT b, PDWORD pData, PCWORD pProps, UINT uProps);
		BOOL SendFunc03(BYTE bType, WORD Block, WORD Prop, DWORD dwData);
		BOOL UsesFunc03(CAddress const &Addr);
		void TranslateAddress(CAddress &Addr);
		BOOL SendTime(void);

		// Function Code 05
		void   InitFunc05(void);
		void   KillFunc05(void);
		void   WipeFunc05(BOOL fNew);
		BOOL   ScanFunc05(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr);
		BOOL   ScanFunc05(void);
		BOOL   ReadFunc05(CSPP &SPP);
		BOOL   SendFunc05(CSPP &SPP);
		BOOL   UsesFunc05(CAddress const &Addr);
		DWORD  ParseSPPS(CSPP &SPP, UINT a);
		BOOL   WriteSPPS(CSPP &SPP, UINT a, DWORD d);
		CSPP * FindWorkSPP(void);
		BOOL   SendWorkSPP(WORD Block);
		BOOL   ReadWorkSPP(WORD Block);

		// Function Code 07
		void  InitFunc07(void);
		BOOL  ReadFunc07(void);
		BOOL  IsValidRecipe(PBYTE pHead);
		BOOL  IsValidHead(PCBYTE pHead);

		// Function Code 18
		void   InitFunc18(void);
		void   KillFunc18(void);
		void   WipeFunc18(BOOL fNew);
		BOOL   ScanFunc18(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr);
		BOOL   ScanFunc18(void);
		BOOL   ReadFunc18(CSPS &SPS);
		BOOL   SendFunc18(CSPS &SPS);
		BOOL   UsesFunc18(CAddress const &Addr);
		DWORD  ParseSPSS(CSPS &SPS, UINT a);
		BOOL   WriteSPSS(CSPS &SPS, UINT a, DWORD d);
		DWORD  ParseSPSD(CSPS &SPS, UINT a);
		CSPS * FindWorkSPS(void);
		BOOL   SendWorkSPS(WORD Block);
		BOOL   ReadWorkSPS(WORD Block);

		// Function Code 20
		void  InitFunc20(void);
		void  KillFunc20(void);
		void  WipeFunc20(BOOL fNew);
		BOOL  ScanFunc20(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr);
		BOOL  ScanFunc20(void);
		BOOL  ReadFunc20(WORD Block, PBYTE pData);
		BOOL  SendFunc20(WORD Block, PBYTE pData);
		BOOL  UsesFunc20(CAddress const &Addr);

		// Function Code 26
		void   InitFunc26(void);
		void   KillFunc26(void);
		void   WipeFunc26(BOOL fNew);
		BOOL   ScanFunc26(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr);
		BOOL   ScanFunc26(void);
		BOOL   ReadFunc26(CSEQ &SEQ);
		BOOL   SendFunc26(CSEQ &SEQ);
		BOOL   UsesFunc26(CAddress const &Addr);
		DWORD  ParseSEQS(CSEQ &SEQ, UINT a);
		BOOL   WriteSEQS(CSEQ &SEQ, UINT a, DWORD d);
		DWORD  ParseSEQP(CSEQ &SEQ, UINT a);
		DWORD  ParseSEQH(CSEQ &SEQ, UINT a);
		CSEQ * FindWorkSEQ(void);
		BOOL   SendWorkSEQ(WORD Block);
		BOOL   ReadWorkSEQ(WORD Block);

		// Implementation
		DWORD Extract (PCBYTE pData, CLayout const *pTable, UINT uRows, UINT a);
		WORD  GetBlock(CAddress const &Addr);
		WORD  GetProp (CAddress const &Addr);
		void  SetBlock(CAddress &Addr, WORD Block);
		void  SetProp (CAddress &Addr, WORD Prop);

		// Variable Recipes
		BOOL SendWorkRcp(WORD Block);
		BOOL ReadWorkRcp(WORD Block);

		// Recipe Headers
		void MakeHeadSEQ(CHeadSEQ &Head, WORD wCount, WORD wNumber);
		void ReadHeadSEQ(CHeadSEQ &Head);
		void MakeHeadSPS(CHeadSPS &Head, WORD wCount, WORD wNumber);
		void ReadHeadSPS(CHeadSPS &Head);
		void MakeHeadSPP(CHeadSPP &Head, WORD wCount, WORD wNumber);
		void ReadHeadSPP(CHeadSPP &Head);
		void MakeHeadRcp(CHeadRcp &Head, WORD wNumber);
		void ReadHeadRcp(CHeadRcp &Head);

		// Table Imaging
		PBYTE MakeImageSEQ(UINT &uSize, CSEQ *pSEQ, WORD wCount, WORD wNumber);
		PBYTE MakeImageSPS(UINT &uSize, CSPS *pSPS, WORD wCount, WORD wNumber);
		PBYTE MakeImageSPP(UINT &uSize, CSPP *pSPP, WORD wCount, WORD wNumber);
		PBYTE MakeImageRcp(UINT &uSize, WORD wNumber);

		// File Handling
		UINT SaveSEQ(PCTXT pName);
		UINT LoadSEQ(PCTXT pName);
		UINT SaveSPS(PCTXT pName);
		UINT LoadSPS(PCTXT pName);
		UINT SaveSPP(PCTXT pName);
		UINT LoadSPP(PCTXT pName);
		UINT SaveRcp(PCTXT pName);
		UINT LoadRcp(PCTXT pName);

		// CDE Transfer
		BOOL UploadCDE(PCTXT pName);
		BOOL DownloadCDE(PCTXT pName);
		BOOL PerformClear(void);
		BOOL SendClear(void);
		BOOL TestClear(BOOL &fDone);
		BOOL PerformValidate(void);
		BOOL SendValidate(void);
		BOOL TestValidate(void);
		BOOL RestartCDE(char cMode);
		BOOL SendRestart(BYTE bCode);
		void SetPhase(UINT uPhase);
		void SetProgress(UINT uDone, UINT uCount);
		void SetTable(WORD wTable);
		void SetResult(UINT uCode);
		UINT GetTableCount(BYTE s);
		BYTE GetTableOrder(BYTE s, UINT n);

		// Alarms and Events
		void  InitAlarmInfo(void);
		void  KillAlarmInfo(void);
		void  WipeAlarmInfo(void);
		BOOL  ScanAlarmInfo(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr);
		void  FindAlarmCount(void);
		BOOL  ReadAlarmInfo(void);
		BOOL  ReadAlarmInfo(UINT uIndex);
		BOOL  SendAlarmAccepts(void);
		BOOL  ReadVolatileAlarms(void);
		BOOL  TrackAlarm(UINT uIndex);
		BOOL  TrackAlarm(UINT uIndex, BYTE bState);
		BOOL  ReadHistory(BYTE bTab, BOOL fInit);
		BOOL  TakeAlarm(UINT uIndex, BYTE bState, DWORD dwTime);
		BOOL  TakeEvent(UINT uIndex, BYTE bState, DWORD dwTime);
		DWORD MakeAlarmCode(UINT uIndex);
		BYTE  FindAlarmMode(UINT uIndex);
		BOOL  IsAlarmManualAck(UINT uIndex);
		DWORD TimeAdjust(DWORD dwTime);
		BOOL  ReadMismatchMode(void);
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Honeywell Proxy Object
//

// Instantiator

#if !defined(_HON)

CProxy * Create_ProxyHoneywell(BOOL fNet)
{
	return NULL;
	}

#else

// Static Data

WORD const CProxyHoneywell::m_ListSEQ[] =
{
	8, 9, 11, 12, 18, 19, 20, 21
	};

WORD const CProxyHoneywell::m_ListSPS[] =
{
	28, 29, 30, 31, 32, 33, 34, 35,
	36, 37, 38, 39, 40, 41, 42, 43
	};

WORD const CProxyHoneywell::m_ListSPP[] =
{
	13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 24,
	25, 26, 27, 28, 29, 30, 31, 32, 36, 37, 38
	};

// Instantiator

CProxy * Create_ProxyHoneywell(BOOL fNet)
{
	return New CProxyHoneywell(fNet);
	}

// Constructor

CProxyHoneywell::CProxyHoneywell(BOOL fNet)
{
	m_fNet   = fNet;

	m_wTrans = 0x1111;

	m_uState = stateNoDevice;

	m_pDev   = NULL;
	}

// Destructor

CProxyHoneywell::~CProxyHoneywell(void)
{
	}

// Entry Points

void CProxyHoneywell::Init(UINT uID)
{
	if( m_pPort->m_pDevices->m_uCount ) {

		if( !m_fNet || g_pRouter ) {

			if( (m_pDev = m_pPort->m_pDevices->m_ppDevice[0]) ) {

				m_pList   = m_pDev->m_pSysBlocks;

				m_IPAddr1 = PDWORD(m_pDev->m_pConfig + 0)[0];

				m_IPAddr2 = PDWORD(m_pDev->m_pConfig + 4)[0];

				m_HMI     = MotorToHost(PDWORD(m_pDev->m_pConfig +  8)[0]);

				m_fAlarms = MotorToHost(PDWORD(m_pDev->m_pConfig + 12)[0]);

				if( CopyConfig() ) {

					ReadParams();

					memset(m_FileHead, 0, 16);

					memset(m_bSendID,  0,  4);

					m_fFlip     = FALSE;

					m_fQuick    = FALSE;

					m_uState    = stateOffline;

					m_fRecipe   = FALSE;

					m_pSock     = NULL;

					m_Time      = 0;

					m_Bias      = NOTHING;

					m_uCDE      = 0;

					m_uResult   = 0;

					m_uPhase    = 0;

					m_uProgress = 0;

					m_uTable    = 0;

					m_uWaitTime = 0;

					InitConfig();

					InitFunc03();

					InitFunc05();

					InitFunc07();

					InitFunc18();

					InitFunc20();

					InitFunc26();

					InitAlarmInfo();
					}
				else
					m_pDev = NULL;
				}
			}
		}
	}

void CProxyHoneywell::Task(UINT uID)
{
	UINT b = 0;

	UINT t = 0;

	if( !m_fNet ) {

		m_pHand = Create_DoubleDataHandler();

		if( !m_pPort->Open(m_pHand, flagFastRx) ) {

			m_uState = stateNoDevice;

			m_pDev   = NULL;
			}
		}

	for(;;) {

		switch( m_uState ) {

			case stateOffline:

				if( CheckSocket(TRUE) ) {

					m_uState = stateWaitBeat;
					}
				else
					Sleep(50);

				ServiceDriver();

				ForceSleep(50);

				break;

			case stateWaitBeat:

				if( Heartbeat(FALSE) ) {

					if( !ReadFunc07() ) { 

						CloseSocket();

						break;
						}

					if( m_dwBeat & beatDataValid ) {

						if( m_fQuick ) {

							AfxTrace("*** FAST RESTART\n");

							m_uState = stateOnline;

							t        = GetTickCount();

							b        = 0;
							}
						else {
							AfxTrace("*** SLOW RESTART\n");

							CheckStart();
							}
						}
					else
						Sleep(500);
					}

				ServiceDriver();

				ForceSleep(50);

				break;

			case stateCheckTabs:

				if( !IsCheckDone() ) {

					if( !CheckTable() ) {

						m_uState = stateMismatch;

						t        = GetTickCount();
						}
					}
				else {
					if( !ReadConfig() ) {

						CloseSocket();

						break;
						}

					InitRecipes();

					m_uState = stateRecipes;
					}

				ServiceDriver();

				ForceSleep(50);

				break;

			case stateMismatch:

				if( m_uCDE == 21 ) {

					m_uFrom   = m_uState;

					m_uState  = stateDownload;

					SetResult(0);

					break;
					}

				if( m_uCDE == 22 ) {

					m_uFrom   = m_uState;

					m_uState  = stateRestart;

					SetResult(0);

					break;
					}

				if( GetTickCount() - t >= ToTicks(m_fNet ? 500 : 2000) ) {

					if( !Heartbeat(TRUE) ) {

						break;
						}

					t = GetTickCount();
					}

				if( !ReadMismatchMode() ) {

					CloseSocket();

					break;
					}

				ServiceDriver();

				ForceSleep(50);

				break;

			case stateRecipes:

				if( !ReadRecipes() ) {

					CloseSocket();

					break;
					}

				m_uState = stateGetAlarms;

				break;

			case stateGetAlarms:

				if( !SyncAlarmHistory() ) {

					CloseSocket();

					break;
					}

				t        = GetTickCount();

				b        = 0;

				m_uState = stateOnline;

				break;

			case stateOnline:

				if( m_uCDE == 20 ) {

					m_uFrom  = m_uState;

					m_uState = stateUpload;

					SetResult(0);

					break;
					}

				if( m_uCDE == 21 ) {

					m_uFrom  = m_uState;

					m_uState = stateDownload;

					SetResult(0);

					break;
					}

				if( m_uCDE == 22 ) {

					m_uFrom  = m_uState;

					m_uState = stateRestart;

					SetResult(0);

					break;
					}

				if( GetTickCount() - t >= ToTicks(m_fNet ? 500 : 2000) ) {

					if( Heartbeat(TRUE) ) {

						if( m_dwBeat & beatNewRecipe ) {

							m_uState = stateRecipes;

							break;
							}

						t = GetTickCount();
						}

					break;
					}

				if( !SendAlarmAccepts() ) {

					CloseSocket();

					break;
					}

				if( !ServiceBlock(b) ) {

					CloseSocket();

					break;
					}

				if( ++b == m_pList->m_uCount ) {

					b = 0;

					if( !ServiceGlobal() ) {

						CloseSocket();

						break;
						}

					ForceSleep(50);
					}
				break;

			case stateUpload:

				if( !UploadCDE(m_sCDE) ) {

					m_uCDE = 0;

					CloseSocket();

					break;
					}

				m_uCDE   = 0;

				m_uState = m_uFrom;

				break;

			case stateDownload:

				if( !DownloadCDE(m_sCDE) ) {

					m_uCDE = 0;

					CloseSocket();

					break;
					}

				m_uCDE   = 0;

				m_uState = m_uFrom;

				break;

			case stateRestart:

				if( !RestartCDE(m_sCDE[0]) ) {

					m_uCDE = 0;

					TakeOffline();

					break;
					}

				m_uCDE   = 0;

				m_uState = m_uFrom;

				break;

			case stateNoDevice:

				ForceSleep(50);

				break;
			}

		if( !m_fNet ) {

			m_pPort->Poll();
			}
		}
	}

void CProxyHoneywell::Term(UINT uID)
{
	if( m_pDev ) {

		KillConfig();

		KillRecipes();

		KillFunc26();

		KillFunc18();

		KillFunc20();
		
		KillFunc05();
		
		KillFunc03();

		KillAlarmInfo();
		}

	m_pPort->Close();
	}

// Suspension

void CProxyHoneywell::Suspend(void)
{
	TakeOffline();
	}

void CProxyHoneywell::Resume(void)
{
	}

// Control

UINT CProxyHoneywell::Control(void *pContext, UINT uFunc, PCTXT pValue)
{
	switch( uFunc ) {

		case 10: return SaveSEQ(pValue);
		case 11: return LoadSEQ(pValue);
		case 12: return SaveSPS(pValue);
		case 13: return LoadSPS(pValue);
		case 14: return SaveSPP(pValue);
		case 15: return LoadSPP(pValue);
		case 16: return SaveRcp(pValue);
		case 17: return LoadRcp(pValue);
		}

	if( uFunc == 20 || uFunc == 21 || uFunc == 22 ) {

		if( !m_uCDE ) {

			strcpy(m_sCDE, pValue);

			m_uCDE = uFunc;
			}
		}

	if( uFunc == 29 ) {

		m_uCDE = 0;
		}

	return 0;
	}

// IEventSource

BOOL CProxyHoneywell::GetEventText(CUnicode &Text, UINT Code)
{
	if( m_pAlarms ) {

		Critical(TRUE);

		if( m_uState == stateOnline || m_uState == stateGetAlarms ) {

			char sText[32];

			memset(sText, 0, 32);

			UINT uSig = LOWORD(Code);

			if( FindSigDesc(sText, uSig) ) {

				Critical(FALSE);

				Text = sText;

				Text.TrimBoth();

				return TRUE;
				}
			}

		Critical(FALSE);
		}

	Text = CPrintf("SIGNAL %u", LOWORD(Code));

	return TRUE;
	}

void CProxyHoneywell::AcceptAlarm(UINT Method, UINT Code)
{
	if( m_pAlarms ) {

		Critical(TRUE);

		if( m_uState == stateOnline ) {

			UINT uNext = (m_uAckTail + 1) % elements(m_uAck);

			if( uNext != m_uAckHead ) {

				if( Method == 0 ) {

					UINT uIndex = Code;

					UINT uGroup = uIndex / 12;

					UINT uAlarm = uIndex % 12;

					m_uAck[m_uAckTail] = MAKELONG(1<<uAlarm, uGroup);

					m_uAckTail         = uNext;
					}

				if( Method == 1 ) {

					UINT uGroup = Code;

					m_uAck[m_uAckTail] = MAKELONG(0x0FFF, uGroup);

					m_uAckTail         = uNext;
					}
				}
			}

		Critical(FALSE);
		}
	}

void CProxyHoneywell::AcceptAlarm(CActiveAlarm *pInfo)
{
	if(m_pAlarms ) {

		Critical(TRUE);

		if( m_uState == stateOnline ) {

			UINT uNext = (m_uAckTail + 1) % elements(m_uAck);

			if( uNext != m_uAckHead ) {

				UINT uIndex = HIWORD(pInfo->m_Code) - 1;

				UINT uGroup = uIndex / 12;

				UINT uAlarm = uIndex % 12;

				m_uAck[m_uAckTail] = MAKELONG(1<<uAlarm, uGroup);

				m_uAckTail         = uNext;
				}
			}

		Critical(FALSE);
		}
	}

// Service Calls

BOOL CProxyHoneywell::ServiceDriver(void)
{
	for( UINT b = 0; b < m_pList->m_uCount; b++ ) {

		CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

		if( pBlock ) {

			DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

			CAddress & Addr = (CAddress &) Data;

			if( !pBlock->m_Named ) {

				if( Addr.a.m_Extra == 0 ) {

					if( Addr.a.m_Table == 100 ) {

						ScanDriver(b, pBlock, Addr, FALSE);

						return TRUE;
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CProxyHoneywell::ServiceBlock(UINT b)
{
	CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

	if( pBlock ) {

		DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

		CAddress & Addr = (CAddress &) Data;

		if( pBlock->m_Named ) {

			if( !ScanFunc03(b, pBlock, Addr, pBlock->m_pList) ) {

				return FALSE;
				}
			}
		else {
			if( ScanDriver(b, pBlock, Addr, TRUE) ) {

				return TRUE;
				}

			if( !ScanRecipes(b, pBlock, Addr) ) {

				return FALSE;
				}

			if( !ScanFunc03(b, pBlock, Addr, NULL) ) {

				return FALSE;
				}

			if( !ScanFunc05(b, pBlock, Addr) ) {

				return FALSE;
				}

			if( !ScanFunc18(b, pBlock, Addr) ) {

				return FALSE;
				}

			if( !ScanFunc20(b, pBlock, Addr) ) {

				return FALSE;
				}

			if( !ScanFunc26(b, pBlock, Addr) ) {

				return FALSE;
				}

			if( !ScanAlarmInfo(b, pBlock, Addr) ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ServiceGlobal(void)
{
	if( !ScanFunc03() ) {

		return FALSE;
		}

	if( !ScanFunc05() ) {

		return FALSE;
		}

	if( !ScanFunc18() ) {

		return FALSE;
		}

	if( !ScanFunc20() ) {

		return FALSE;
		}

	if( !ScanFunc26() ) {

		return FALSE;
		}

	m_pDev->Tickle();

	return TRUE;
	}

BOOL CProxyHoneywell::Invalidate(BOOL fAll)
{
	if( fAll ) {

		WipeConfig();
		}

	WipeFunc03(FALSE);

	WipeFunc05(FALSE);

	WipeFunc18(FALSE);
	
	WipeFunc20(FALSE);

	WipeFunc26(FALSE);

	WipeAlarmInfo();

	m_pList->MarkInvalid();

	ServiceDriver();

	return TRUE;
	}

BOOL CProxyHoneywell::TakeOffline(void)
{
	if( m_uState != stateOffline ) {

		Invalidate(TRUE);

		m_dwBeat  = 0;

		m_uState = stateOffline;

		return TRUE;
		}

	return FALSE;
	}

// Configuration

BOOL CProxyHoneywell::CopyConfig(void)
{
	UINT   uSlot = MotorToHost(PCWORD(m_pDev->m_pConfig + 16)[0]);
		     	
	PCWORD pSlot = PCWORD(m_pDev->m_pConfig + 16) + 1;

	UINT   uBlob = MotorToHost(PCWORD(pSlot + uSlot)[0]);

	PCBYTE pBlob = PCBYTE(pSlot + uSlot) + 2;

	if( uSlot && uBlob ) {

		m_uSlot = uSlot;

		m_uBlob = uBlob;

		m_pSlot = New WORD [ m_uSlot ];

		m_pBlob = New BYTE [ m_uBlob ];

		memcpy(m_pSlot, pSlot, sizeof(WORD) * m_uSlot);

		memcpy(m_pBlob, pBlob, sizeof(BYTE) * m_uBlob);

		return TRUE;
		}

	return FALSE;
	}

void CProxyHoneywell::ReadParams(void)
{
	if( MotorToHost(PCWORD(m_pBlob)[0]) == 0x5678 ) {

		m_uVersion   = MotorToHost(PCWORD(m_pBlob)[1]);

		m_fOldBeat   = (MotorToHost(PCWORD(m_pBlob)[2]) >= 1);

		m_fOldAlarms = (MotorToHost(PCWORD(m_pBlob)[2]) >= 2);

		m_wNetBlock  = MotorToHost(PCWORD(m_pBlob)[3]);

		m_pCheck     = PCWORD(m_pBlob) + 4;
		}
	else {
		m_uVersion   = 0;

		m_fOldBeat   = FALSE;

		m_fOldAlarms = FALSE;

		m_wNetBlock  = MotorToHost(PCWORD(m_pBlob)[0]);

		m_pCheck     = PCWORD(m_pBlob) + 1;
		}
	}

// Frame Building

void CProxyHoneywell::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	if( m_fNet ) {

		m_wTrans++;

		AddByte(LOBYTE(m_wTrans));
		AddByte(HIBYTE(m_wTrans));
		
		AddByte(0);
		AddByte(0);
		
		AddByte(0);
		AddByte(0);
	
		AddByte(0);
		}
	else
		AddByte(LOBYTE(m_IPAddr1));

	AddByte(bOpcode);
	
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0);
	
	AddByte(0);
	AddByte(0);
	AddByte(0);
	AddByte(0);
	}

void CProxyHoneywell::AddByte(BYTE bData)
{
	m_bTxData[m_uPtr] = bData;
		
	m_uPtr++;
	}

void CProxyHoneywell::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));
	
	AddByte(HIBYTE(wData));
	}

void CProxyHoneywell::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));
	
	AddWord(HIWORD(dwData));
	}

void CProxyHoneywell::AddData(DWORD dwData)
{
	AddByte(HIBYTE(HIWORD(dwData)));

	AddByte(LOBYTE(HIWORD(dwData)));
	
	AddByte(HIBYTE(LOWORD(dwData)));
	
	AddByte(LOBYTE(LOWORD(dwData)));
	}

void CProxyHoneywell::AddData(PCBYTE pData, UINT uCount)
{
	if( uCount ) {

		memcpy(m_bTxData + m_uPtr, pData, uCount);

		m_uPtr += uCount;
		}
	}

void CProxyHoneywell::CloseFrame(void)
{
	if( m_fNet ) {

		m_bTxData[5] = BYTE(m_uPtr - 6);

		m_bTxData[8] = BYTE(m_uPtr - 9);
		}
	else {
		m_bTxData[2] = BYTE(m_uPtr - 3);

		CRC16 crc;

		crc.Preset();

		for( UINT i = 0; i < m_uPtr; i++ ) {
		
			BYTE bData = m_bTxData[i];

			crc.Add(bData);
			}

		WORD wCRC = crc.GetValue();

		AddByte(LOBYTE(wCRC));

		AddByte(HIBYTE(wCRC));
		}
	}

// Transport Layer

BOOL CProxyHoneywell::Transact(BOOL fCheck)
{
	CloseFrame();

	if( m_fNet ) {

		for( UINT n = 0; n < 2; n++ ) {

			UINT uSend = m_uPtr;

			m_pSock->Send(m_bTxData, uSend);

			SetTimer(500);

			for(;;) {

				m_pRxBuff = NULL;

				m_pSock->Recv(m_pRxBuff);

				if( m_pRxBuff ) {

					m_pRxData = m_pRxBuff->GetData();

					if( fCheck ) {

						return CheckReply();
						}

					return TRUE;
					}

				if( !GetTimer() ) {

					break;
					}

				Sleep(5);
				}

			if( !CycleSocket() ) {

				break;
				}
			}

		ShowError("TIMEOUT");

		return FALSE;
		}
	else {
		for( UINT t = 0; t < 3; t++ ) {

			m_pHand->Write(m_bTxData, m_uPtr, NOTHING);

			m_pRxBuff = BuffAllocate(1024);

			m_pRxData = m_pRxBuff->AddTail(1024);

			UINT uPtr = 0;

			SetTimer(500);

			while( GetTimer() ) {

				UINT uData = m_pHand->Read(10);

				if( uData == NOTHING ) {

					if( uPtr ) {

						if( uPtr >= 4 ) {

							uPtr -= 2;

							CRC16 crc;

							WORD wCRC = IntelToHost(PWORD(m_pRxData + uPtr)[0]);

							crc.Preset();

							for( UINT i = 0; i < uPtr; i++ ) {

								crc.Add(m_pRxData[i]);
								}

							if( wCRC == crc.GetValue() ) {

								m_pRxData = m_pRxData - 6;

								if( fCheck ) {

									return CheckReply();
									}

								return TRUE;
								}
							}

						FreeRx();

						ShowError("BAD FRAME");

						return FALSE;
						}

					continue;
					}

				m_pRxData[uPtr++] = uData;
				}

			FreeRx();
			}

		ShowError("TIMEOUT");

		return FALSE;
		}
	}

BOOL CProxyHoneywell::CloseSocket(void)
{
	if( m_fNet ) {

		if( m_pSock ) {

			m_pSock->Abort();

			m_pSock->Release();

			m_pSock = NULL;
			}

		if( m_IPAddr2 ) {

			m_fFlip  = !m_fFlip;

			m_fQuick = TRUE;
			}

		TakeOffline();

		Sleep(50);

		return TRUE;
		}

	TakeOffline();

	Sleep(50);

	return TRUE;
	}

BOOL CProxyHoneywell::CheckSocket(BOOL fSwitch)
{
	if( m_fNet ) {

		if( m_pSock ) {

			UINT Phase;
		
			m_pSock->GetPhase(Phase);

			if( Phase == PHASE_OPEN ) {

				return TRUE;
				}

			if( Phase == PHASE_OPENING ) {

				Sleep(50);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				m_pSock->Close();
				}
			else
				m_pSock->Abort();

			m_pSock->Release();

			m_pSock = NULL;

			Sleep(50);
			}

		for( UINT n = 0; n < 4; n++ ) {

			CIPAddr IP = m_fFlip ? m_IPAddr2 : m_IPAddr1;

			BOOL    mt = fSwitch && m_fQuick;

			UINT    nt = mt ? 4 : 1;

			for( UINT t = 0; t < nt; t++ ) {

				if( !m_IPAddr2 || g_pRouter->Ping(IP, 500) < NOTHING ) {

					m_pSock = g_pRouter->CreateSocket(IP_TCP);

					m_pSock->SetOption(OPT_LOCAL, 1);

					m_pSock->Connect(IP, 502);

					Sleep(50);

					return FALSE;
					}

				AfxTrace("*** PING FAIL ON %u\n", 1 + m_fFlip);
				}

			if( !fSwitch || !m_IPAddr2 ) {

				return FALSE;
				}

			m_fFlip  = !m_fFlip;

			m_fQuick = FALSE;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::CycleSocket(void)
{
	if( m_pSock ) {

		m_pSock->Abort();

		m_pSock->Release();

		m_pSock = NULL;
		}

	if( !CheckSocket(FALSE) ) { 

		if( m_pSock ) {

			if( !CheckSocket(FALSE) ) { 

				return FALSE;
				}

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

void CProxyHoneywell::FreeRx(void)
{
	m_pRxBuff->Release();
	}

// Application Layer

void CProxyHoneywell::StartRead(BYTE bFunc)
{
	StartFrame(20);

	AddByte(0);

	AddByte(bFunc);
	}

void CProxyHoneywell::StartWrite(BYTE bFunc)
{
	StartFrame(21);

	AddByte(128);

	AddByte(bFunc);
	}

BOOL CProxyHoneywell::CheckReply(void)
{
	if( m_pRxData[16] == 0x0A ) {

		return TRUE;
		}

	ShowError("ERROR %u", m_pRxData[17]);

	FreeRx();

	return FALSE;
	}

BOOL CProxyHoneywell::ShowError(PCTXT pForm, ...)
{
	va_list pArgs;

	va_start(pArgs, pForm);

	char sText[32];

	VSPrintf(sText, pForm, pArgs);

	BYTE  bMode = m_bTxData[m_fNet ? 16 : 10];

	BYTE  bCode = m_bTxData[m_fNet ? 17 : 11];

	PCTXT pMode = (bMode & 128) ? "WRITE" : "READ";

	AfxTrace( "*** %s on ON OPCODE %u %s\n",
		  sText,
		  bCode,
		  pMode
		  );

	return TRUE;
	}

// Table Access

BOOL CProxyHoneywell::ReadTable(CTab &Tab, BOOL fData)
{
	CHeadTab Head;

	if( ReadTable(Tab.m_bTable, 0, PBYTE(&Head), sizeof(Head)) ) {

		Head.Type     = MotorToHost(Head.Type);
		Head.Number   = MotorToHost(Head.Number);
		Head.Count    = MotorToHost(Head.Count);
		Head.Size     = MotorToHost(Head.Size);
		Head.ModCount = MotorToHost(Head.ModCount);
		Head.CRC      = MotorToHost(Head.CRC);

		if( Head.Type ) {

			Tab.m_fValid = FALSE;

			return TRUE;
			}

		if( Tab.m_fValid ) {
			
			if( Tab.m_wCRC == Head.CRC ) {

				return TRUE;
				}

			Tab.m_fValid = FALSE;
			}

		if( fData ) {

			UINT uAlloc = Head.Size;

			UINT uExtra = 0;

			if( !Tab.m_wRecord ) {

				uAlloc *= Head.Count;

				uExtra  = 16;

				uAlloc += 16;
				}

			if( Tab.m_uAlloc != uAlloc ) {

				delete Tab.m_pHead;

				Tab.m_uAlloc   = uAlloc;

				Tab.m_pHead    = New BYTE [ uAlloc ];

				Tab.m_pData    = Tab.m_pHead + uExtra;

				Tab.m_uSize    = Head.Size;

				Tab.m_uVersion = Head.RevNum;
				}

			if( !Tab.m_wRecord ) {

				if( !ReadTable(Tab.m_bTable, 0xFFFF, Tab.m_pHead, uAlloc) ) {

					Tab.m_fValid = FALSE;

					return FALSE;
					}

				Tab.m_uCount = Head.Count;
				}
			else {
				if( !ReadTable(Tab.m_bTable, Tab.m_wRecord, Tab.m_pData, Tab.m_uSize) ) {

					Tab.m_fValid = FALSE;

					return FALSE;
					}

				Tab.m_uCount = 1;
				}
			}
		else {
			Tab.m_uSize    = Head.Size;

			Tab.m_uVersion = Head.RevNum;
			}

		Tab.m_wCRC   = Head.CRC;

		Tab.m_fValid = TRUE;
		
		return TRUE;
		}

	Tab.m_fValid = FALSE;

	return FALSE;
	}

void CProxyHoneywell::FreeTable(CTab &Tab)
{
	delete Tab.m_pHead;
	}

void CProxyHoneywell::WipeTable(CTab &Tab)
{
	Tab.m_fValid = FALSE;
	}

BOOL CProxyHoneywell::ReadTable(BYTE bTable, WORD wRecord, PBYTE pData, UINT uSize)
{
	return ReadTable(bTable, 0, wRecord, pData, uSize);
	}

BOOL CProxyHoneywell::ReadTable(BYTE bTable, BYTE bType, WORD wRecord, PBYTE pData, UINT uSize)
{
	UINT uPtr = 0;

	UINT uSeq = 1;

	while( uPtr < uSize ) {

		StartRead(4);

		AddWord(uSeq);

		AddByte(bType);
		
		AddByte(bTable);

		AddWord(wRecord);

		if( Transact(TRUE) ) {

			UINT uData = m_pRxData[24];

			UINT uLeft = uSize - uPtr;

			UINT uCopy = min(uData, uLeft);

			memcpy( pData     + uPtr,
				m_pRxData + 25,
				uCopy
				);

			uPtr += uCopy;

			if( (uSeq = MAKEWORD(m_pRxData[18], m_pRxData[19])) >= 0xFFFE ) {

				FreeRx();

				if( uPtr == uSize ) {

					return TRUE;
					}

				ShowError("SHORT TABLE");

				return FALSE;
				}

			FreeRx();

			uSeq++;

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::SendTable(WORD wTable, BYTE bType, WORD wRecord, PBYTE pData, UINT uSize)
{
	UINT uPtr = 0;

	UINT uMax = 224;

	UINT uSeq = (uSize <= uMax) ? 0xFFFE : 1;

	for(;;) {

		StartWrite(HIBYTE(wTable) ? 48 : 4);

		UINT uLeft = uSize - uPtr;

		UINT uCopy = min(uMax, uLeft);

		AddWord(uCopy ? uSeq : 0xFFFF);

		AddByte(bType);

		if( HIBYTE(wTable) ) {

			AddWord(wTable);

			AddWord(wRecord);

			AddByte(0);
			}
		else {
			AddByte(LOBYTE(wTable));

			AddWord(wRecord);
			}

		AddByte(uCopy);

		AddData(pData + uPtr, uCopy);

		if( Transact(TRUE) ) {

			FreeRx();

			if( uCopy && uSeq < 0xFFFE ) {

				uPtr += uCopy;

				uSeq += 1;

				continue;
				}

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::SendTable(WORD wTable, BYTE bType, WORD wRecord, CFile &File, UINT uSize)
{
	UINT  uPtr  = 0;

	UINT  uMax  = 224;

	UINT  uSeq  = (uSize <= uMax) ? 0xFFFE : 1;

	PBYTE pRead = PBYTE(alloca(uMax));

	for(;;) {

		UINT uLeft = uSize - uPtr;

		UINT uCopy = min(uMax, uLeft);

		if( !uCopy || File.Read(pRead, uCopy) ) {

			StartWrite(HIBYTE(wTable) ? 48 : 4);
	
			AddWord(uCopy ? uSeq : 0xFFFF);

			AddByte(bType);

			if( HIBYTE(wTable) ) {

				AddWord(wTable);

				AddWord(wRecord);

				AddByte(0);
				}
			else {
				AddByte(LOBYTE(wTable));

				AddWord(wRecord);
				}

			AddByte(uCopy);

			AddData(pRead, uCopy);

			if( Transact(TRUE) ) {

				FreeRx();

				if( uCopy && uSeq < 0xFFFE ) {

					uPtr += uCopy;

					uSeq += 1;

					continue;
					}

				return TRUE;
				}

			ShowError("FAILED");
			}

		return FALSE;
		}

	return TRUE;
	}

// HC Config Check

void CProxyHoneywell::CheckStart(void)
{
	m_uState = stateCheckTabs;

	m_uCheck = 0;
	}

BOOL CProxyHoneywell::CheckTable(void)
{
	PCWORD pIndex = m_pCheck;

	PCWORD pTable = pIndex + 2 * m_uCheck;

	BYTE   bTable = MotorToHost(pTable[0]);

	WORD   wCRC   = MotorToHost(pTable[1]);

	CTab Tab;

	Tab.m_bTable = bTable;

	Tab.m_fValid = FALSE;

	if( ReadTable(Tab, FALSE) ) {

		if( !bTable || Tab.m_wCRC == wCRC ) {

			m_uCheck++;

			return TRUE;
			}

		AfxTrace("*** MISMATCH T%u %4.4X %4.4X\n", bTable, wCRC, Tab.m_wCRC);

		return FALSE;
		}

	CloseSocket();

	return TRUE;
	}

BOOL CProxyHoneywell::IsCheckDone(void)
{
	PCWORD pIndex = m_pCheck;

	PCWORD pTable = pIndex + 2 * m_uCheck;

	UINT   uTable = pTable[0];

	if( uTable == 0xFFFF ) {

		m_pRecs = pTable + 1;

		m_pPeer = pTable + 5;

		m_pPPO  = m_pPeer;

		while( *m_pPPO++ != 0xFFFF );

		return TRUE;
		}

	return FALSE;
	}

// Heartbeat

BOOL CProxyHoneywell::Heartbeat(BOOL fCheck)
{
	if( SendHB1() ) {

		BOOL fSame = TRUE;
		
		if( fCheck ) {

			if( m_pAlarms ) {

				if( m_dwBeat & beatNewAlarms ) {

					if( !ReadHistory(0, FALSE) ) {

						CloseSocket();

						return FALSE;
						}

					fSame = FALSE;
					}
				
				if( m_dwBeat & beatNewEvents ) {

					if( !ReadHistory(1, FALSE) ) {

						CloseSocket();

						return FALSE;
						}

					fSame = FALSE;
					}

				if( !ReadVolatileAlarms() ) {

					CloseSocket();

					return FALSE;
					}
				}

			if( m_dwBeat & beatNewConfig ) {

				fSame = FALSE;
				}
			
			if( m_dwBeat & beatNewRecipe ) {

				fSame = FALSE;
				}
			}

		if( fSame || SendHB2() ) {

			if( fCheck ) {

				if( !(m_dwBeat & beatDataValid) ) {

					Invalidate(TRUE);

					m_uState = stateWaitBeat;

					return TRUE;
					}

				if( m_dwBeat & beatNewConfig ) {

					Invalidate(TRUE);

					CheckStart();

					return TRUE;
					}
				}

			return TRUE;
			}
		}
	
	CloseSocket();

	return FALSE;
	}

BOOL CProxyHoneywell::SendHB1(void)
{
	if( m_fNet && !m_fOldBeat ) {

		StartRead(50);

		AddByte(m_HMI);
		}
	else {
		StartRead(3);

		AddByte(1);
		
		AddByte(0);

		AddByte(2);

		AddWord(12);

		AddWord(10);
		}

	if( Transact(TRUE) ) {

		m_dwBeat = MotorToHost(*PDWORD(m_pRxData+19));

		/*AfxTrace("GET BEAT 0x%8.8X (%u)\n", m_dwBeat, GetTickCount());*/

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::SendHB2(void)
{
	if( m_fNet && !m_fOldBeat ) {

		StartWrite(50);

		AddByte(m_HMI);
		}
	else {
		StartWrite(3);

		AddByte(1);
		
		AddByte(0);

		AddByte(2);

		AddWord(12);

		AddWord(11);
		}

	AddByte(HIBYTE(HIWORD(m_dwBeat)));
	AddByte(LOBYTE(HIWORD(m_dwBeat)));
	AddByte(HIBYTE(LOWORD(m_dwBeat)));
	AddByte(LOBYTE(LOWORD(m_dwBeat)));

	if( Transact(TRUE) ) {

		/*AfxTrace("PUT BEAT 0x%8.8X (%u)\n", m_dwBeat, GetTickCount());*/

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

// Driver Data

BOOL CProxyHoneywell::ScanDriver(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr, BOOL fOnline)
{
	if( Addr.a.m_Extra == 0 ) {

		if( Addr.a.m_Table == 100 ) {

			WORD a = Addr.a.m_Offset;

			for( INT i = 0; i < pBlock->m_Size; i++ ) {

				DWORD Data = 0;

				switch( a ) {

					case  1: Data = m_uState;    break;
					case  2: Data = m_dwBeat;    break;
					case  3: Data = m_HMI;       break;
					case  4: Data = m_uResult;   break;
					case  5: Data = m_uProgress; break;
					case  6: Data = m_uWaitTime; break;
					case  7: Data = m_uPhase;    break;
					case  8: Data = m_uTable;    break;
					case  9: Data = m_fAlarms;   break;
					case 10: Data = m_MisMode;   break;
					case 20: Data = m_Time;      break;
					case 22: Data = m_Bias;	     break;
					}

				if( a >= 4 && a <= 8 ) {

					if( pBlock->ShouldWrite(i) ) {

						UINT w = pBlock->GetWriteData(i);

						switch( a ) {

							case 4: m_uResult   = w; break;
							case 5: m_uProgress = w; break;
							case 6: m_uWaitTime = w; break;
							case 7: m_uPhase    = w; break;
							case 8: m_uTable    = w; break;
							}

						pBlock->SetWriteDone(i);

						a++;

						continue;
						}
					}

				if( fOnline ) {

					if( pBlock->ShouldWrite(i) ) {

						if( a >= 10 && a <= 17 ) {

							WORD Block = WORD(pBlock->GetWriteData(i));

							if( Block ) {

								switch( a ) {

									case 10:
										SendWorkSEQ(Block);
										break;

									case 11:
										ReadWorkSEQ(Block);
										break;

									case 12:
										SendWorkSPS(Block);
										break;

									case 13:
										ReadWorkSPS(Block);
										break;

									case 14:
										SendWorkSPP(Block);
										break;

									case 15:
										ReadWorkSPP(Block);
										break;

									case 16:
										SendWorkRcp(Block);
										break;

									case 17:
										ReadWorkRcp(Block);
										break;
									}
								}
							}

						if( a == 20 ) {

							m_Time = pBlock->GetWriteData(i);
							}

						if( a == 22 ) {

							m_Bias = pBlock->GetWriteData(i);
							}

						if( a == 21 ) {

							if( pBlock->GetWriteData(i) ) {

								SendTime();
								}
							}

						pBlock->SetWriteDone(i);

						a++;

						continue;
						}
					}

				pBlock->SetCommsData(i, Data);

				a++;
				}

			return TRUE;
			}
			
		if( Addr.a.m_Table == 102 ) {

			for( INT i = 0; i < pBlock->m_Size; i++ ) {

				pBlock->SetCommsData(i, 0);
				}

			return TRUE;
			}
		
		if( fOnline ) {
			
			if( Addr.a.m_Table == 101 ) {

				WORD a = Addr.a.m_Offset;

				for( INT i = 0; i < pBlock->m_Size; i++ ) {

					DWORD Data = m_pPPO[i];

					pBlock->SetCommsData(i, Data);
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// HC Config Data

void CProxyHoneywell::InitConfig(void)
{
	memset(&m_TabSignal, 0, sizeof(m_TabSignal));

	memset(&m_TabDesc,   0, sizeof(m_TabDesc));

	memset(&m_TabEvents, 0, sizeof(m_TabEvents));

	memset(&m_TabAlarms, 0, sizeof(m_TabAlarms));

	m_TabSignal.m_bTable =  4;

	m_TabDesc  .m_bTable =  5;

	m_TabEvents.m_bTable =  6;

	m_TabAlarms.m_bTable = 16;
	}

BOOL CProxyHoneywell::ReadConfig(void)
{
	if( ReadTable(m_TabSignal, TRUE) ) {

		if( ReadTable(m_TabDesc, TRUE) ) {
	
			if( ReadTable(m_TabEvents, TRUE) ) {
	
				if( ReadTable(m_TabAlarms, TRUE) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CProxyHoneywell::SyncAlarmHistory(void)
{
	if( m_pAlarms ) {

		FindAlarmCount();

		if( !ReadHistory(0, TRUE) ) {

			return FALSE;
			}

		if( !ReadHistory(1, TRUE) ) {

			return FALSE;
			}

		if( !ReadAlarmInfo() ) {

			return FALSE;
			}
		}

	return TRUE;
	}

void CProxyHoneywell::WipeConfig(void)
{
	WipeTable(m_TabSignal);

	WipeTable(m_TabDesc);

	WipeTable(m_TabEvents);
	
	WipeTable(m_TabAlarms);
	}

void CProxyHoneywell::KillConfig(void)
{
	FreeTable(m_TabSignal);

	FreeTable(m_TabDesc);

	FreeTable(m_TabEvents);

	FreeTable(m_TabAlarms);
	}

PCTXT CProxyHoneywell::FindSigName(WORD wSig)
{
	static char sName[16];

	FindSigName(sName, wSig);

	return sName;
	}

BOOL CProxyHoneywell::FindSigName(PTXT pText, WORD wSig)
{
	if( wSig ) {

		if( m_TabSignal.m_fValid ) {

			PBYTE pSig = m_TabSignal.m_pData + m_TabSignal.m_uSize * (wSig - 1);

			if( m_TabSignal.m_uVersion >= 3 ) {

				memcpy(pText + 0, pSig, 0x10);
				}
			else {
				memcpy(pText + 0, pSig, 0x08);

				memset(pText + 8, 0x20, 0x08);
				}

			return TRUE;
			}
		}

	memset(pText + 0, 0x20, 0x10);

	return FALSE;
	}

BOOL CProxyHoneywell::FindSigDesc(PTXT pText, WORD wSig)
{
	if( wSig ) {

		if( m_TabSignal.m_fValid ) {

			if( wSig <= m_TabSignal.m_uCount ) {

				BOOL  fOld  = (m_TabSignal.m_uVersion <= 2);

				PBYTE pSig  = m_TabSignal.m_pData + m_TabSignal.m_uSize * (wSig - 1);

				WORD  wDesc = MotorToHost(*PWORD(pSig + (fOld ? 26 : 34)));

				if( wDesc ) {

					if( m_TabDesc.m_fValid ) {

						UINT uLen = m_TabDesc.m_pData[wDesc];

						if( uLen ) {

							PBYTE pFrom = m_TabDesc.m_pData + wDesc + 1;

							memcpy(pText, pFrom, uLen);

							pText[uLen] = 0;

							return TRUE;
							}
						}
					}

				if( fOld ) {

					memcpy(pText + 0, pSig, 0x08);

					memset(pText + 8, 0x20, 0x08);
					}
				else
					memcpy(pText + 0, pSig, 0x10);

				return TRUE;
				}
			}
		}

	memset(pText + 0, 0x20, 0x10);

	return FALSE;
	}

BYTE CProxyHoneywell::FindSigMode(WORD wSig)
{
	if( wSig ) {

		if( m_TabSignal.m_fValid ) {

			PBYTE pSig = m_TabSignal.m_pData + m_TabSignal.m_uSize * (wSig - 1);

			if( m_TabSignal.m_uVersion >= 3 ) {

				return pSig[32];
				}

			return pSig[24];
			}
		}

	return 0;
	}

// Recipe Data

BOOL CProxyHoneywell::InitRecipes(void)
{
	if( !m_fRecipe ) {

		m_RecRcp.m_uCount = m_pRecs[0];

		m_RecSpp.m_uCount = m_pRecs[1];
		
		m_RecSps.m_uCount = m_pRecs[2];
		
		m_RecSeq.m_uCount = m_pRecs[3];

		m_RecRcp.m_bTable = 9;

		m_RecSpp.m_bTable = 12;
		
		m_RecSps.m_bTable = 48;
		
		m_RecSeq.m_bTable = 58;

		InitRecipe(m_RecRcp);
		
		InitRecipe(m_RecSpp);
		
		InitRecipe(m_RecSps);
		
		InitRecipe(m_RecSeq);

		m_fRecipe = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::KillRecipes(void)
{
	if( m_fRecipe ) {

		KillRecipe(m_RecRcp);
		
		KillRecipe(m_RecSpp);
		
		KillRecipe(m_RecSps);
		
		KillRecipe(m_RecSeq);

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::ScanRecipes(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr)
{
	if( Addr.a.m_Extra == 0 ) {
		
		if( Addr.a.m_Table == 10 ) {

			return ScanRecipe(b, pBlock, Addr, m_RecRcp);
			}
		
		if( Addr.a.m_Table == 11 ) {

			return ScanRecipe(b, pBlock, Addr, m_RecSpp);
			}
		
		if( Addr.a.m_Table == 12 ) {

			return ScanRecipe(b, pBlock, Addr, m_RecSps);
			}
		
		if( Addr.a.m_Table == 13 ) {

			return ScanRecipe(b, pBlock, Addr, m_RecSeq);
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadRecipes(void)
{
	if( !ReadRecipe(m_RecRcp) ) {

		return FALSE;
		}

	if( !ReadRecipe(m_RecSpp) ) {

		return FALSE;
		}

	if( !ReadRecipe(m_RecSps) ) {

		return FALSE;
		}

	if( !ReadRecipe(m_RecSeq) ) {

		return FALSE;
		}

	return TRUE;
	}

void CProxyHoneywell::InitRecipe(CRecipe &Rec)
{
	Rec.m_pText = New DWORD [ 6 * Rec.m_uCount ];

	Rec.m_pNumb = New WORD  [ 1 * Rec.m_uCount ];

	memset(Rec.m_pText, 0, 6 * sizeof(DWORD) * Rec.m_uCount);

	memset(Rec.m_pNumb, 0, 1 * sizeof(WORD)  * Rec.m_uCount);
	}

void CProxyHoneywell::KillRecipe(CRecipe &Rec)
{
	delete [] Rec.m_pText;

	delete [] Rec.m_pNumb;
	}

BOOL CProxyHoneywell::ScanRecipe(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr, CRecipe &Rec)
{
	UINT a = Addr.a.m_Offset;

	if( a == 30000 ) {

		a -= 30000;

		if( pBlock->ShouldWrite(a) ) {

			DWORD Data = pBlock->GetWriteData(a);

			if( Data == 1 ) {

				if( !ReadRecipe(Rec) ) {

					return FALSE;
					}

				pBlock->SetWriteDone(a);

				pBlock->SetWriteData(a, setNone, 1001);

				return TRUE;
				}

			if( Data >= 1001 ) {

				pBlock->SetWriteDone(a);

				pBlock->SetCommsData(a, 0);

				return TRUE;
				}

			pBlock->SetWriteDone(a);
			}

		pBlock->SetCommsData(a, 0);

		return TRUE;
		}

	if( a >= 20000 ) {

		a -= 20000;

		for( INT i = 0; i < pBlock->m_Size; i++ ) {

			DWORD Data = Rec.m_pNumb[a + i];

			pBlock->SetCommsData(i, Data);
			}

		return TRUE;
		}

	for( INT i = 0; i < pBlock->m_Size; i++ ) {

		DWORD Data = Rec.m_pText[a + i];

		pBlock->SetCommsData(i, Data);
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadRecipe(CRecipe &Rec)
{
	CTab Tab;

	memset(&Tab, 0, sizeof(Tab));

	Tab.m_bTable = Rec.m_bTable;

	if( ReadTable(Tab, TRUE) ) {

		UINT uCount = min(Tab.m_uCount, Rec.m_uCount);

		UINT uPtr   = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			PCBYTE pData = Tab.m_pData + uPtr;

			if( Tab.m_bTable == 58 ) {

				// NOTE -- Seq table has only later schema.

				Tab.m_uVersion = 2;
				}

			if( Tab.m_uVersion == 1 ) {

				Rec.m_pNumb[n] = PCWORD(pData)[1];

				PDWORD pText = Rec.m_pText + 6 * n;

				memcpy(pText, pData + 8, 8);
				}
			
			if( Tab.m_uVersion == 2 ) {

				Rec.m_pNumb[n] = PCWORD(pData)[1];

				PDWORD pText = Rec.m_pText + 6 * n;

				memcpy(pText, pData + 8, 24);
				}

			uPtr += Tab.m_uSize;
			}

		FreeTable(Tab);

		return TRUE;
		}

	return FALSE;
	}

// Function Code 03

void CProxyHoneywell::InitFunc03(void)
{
	m_ReqCount[0] = 0;

	m_ReqCount[1] = 0;

	memset(m_WorkRcp, 0, sizeof(m_WorkRcp));
	}

void CProxyHoneywell::KillFunc03(void)
{
	}

void CProxyHoneywell::WipeFunc03(BOOL fNew)
{
	}

BOOL CProxyHoneywell::ScanFunc03(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr, PCWORD pList)
{
	if( UsesFunc03(Addr) ) {

		CAddress Real = Addr;

		UINT     t    = NOTHING;
		
		TranslateAddress(Real);

		switch( Real.a.m_Extra ) {

			case 1:
			case 10:
				t = 0;
				break;

			case 2:
			case 11:
				t = 1;
				break;
			}

		if( GetBlock(Real) >= SYS_BLOCK_SEQ ) {

			PDWORD pData = NULL;

			switch( GetBlock(Real) ) {

				case SYS_BLOCK_SEQ: pData = m_WorkSEQ; break;
				case SYS_BLOCK_SPS: pData = m_WorkSPS; break;
				case SYS_BLOCK_SPP: pData = m_WorkSPP; break;
				case SYS_BLOCK_RCP: pData = m_WorkRcp; break;
				}

			for( INT i = 0; i < pBlock->m_Size; i++ ) {

				CAddress Item = Addr;

				if( pList ) {

					Item.a.m_Offset = pList[i];
					}
				else
					Item.a.m_Offset = Item.a.m_Offset + i;

				if( pBlock->ShouldWrite(i) ) {

					DWORD Data = pBlock->GetWriteData(i);

					UINT  Prop = GetProp(Item);

					if( pData ) {

						pData[Prop] = Data;
						}

					pBlock->SetWriteDone(i);
					}
				else {
					if( pBlock->ShouldRead(i) ) {

						DWORD Data = 0;

						UINT  Prop = GetProp(Item);
						
						if( pData ) {
							
							Data = pData[Prop];
							}

						pBlock->SetCommsData(i, Data);
						}
					}
				}

			return TRUE;
			}

		for( INT i = 0; i < pBlock->m_Size; i++ ) {

			if( pBlock->ShouldWrite(i) ) {

				DWORD    Data = pBlock->GetWriteData(i);

				CAddress Send = Addr;

				if( pList ) {

					Send.a.m_Offset = pList[i];
					}
				else
					Send.a.m_Offset = Send.a.m_Offset + i;

				TranslateAddress(Send);

				if( !SendFunc03(Send, Data) ) {

					return FALSE;
					}

				pBlock->SetWriteDone(i);
				}
			else {
				if( pBlock->ShouldRead(i) ) {

					m_ReqBlock[t][m_ReqCount[t]] = b;

					m_ReqIndex[t][m_ReqCount[t]] = i;

					m_ReqCount[t]++;
						
					if( m_ReqCount[t] == 60 ) {

						if( !ReadFunc03(t) ) {

							return FALSE;
							}
						}
					}
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ScanFunc03(void)
{
	if( m_ReqCount[0] ) {

		if( !ReadFunc03(0) ) {

			return FALSE;
			}
		}

	if( m_ReqCount[1] ) {

		if( !ReadFunc03(1) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadFunc03(BYTE bType, WORD Block, WORD Prop, DWORD &dwData)
{
	StartRead(3);

	AddByte(1);
	
	AddByte(0);

	AddByte(bType);

	AddWord(Block);

	AddWord(Prop);

	if( Transact(TRUE) ) {

		PDWORD d = PDWORD(m_pRxData+19);

		dwData   = MotorToHost(d[0]);

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::ReadFunc03(UINT t)
{
	StartRead(3);

	AddByte(m_ReqCount[t]);
	
	AddByte(0);

	AddByte(BYTE(t+1));

	for( UINT n = 0; n < m_ReqCount[t]; n++ ) {

		UINT b = m_ReqBlock[t][n];

		UINT i = m_ReqIndex[t][n];

		CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

		if( pBlock ) {

			DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

			CAddress & Addr = (CAddress &) Data;

			if( pBlock->m_Named ) {

				Addr.a.m_Offset = pBlock->m_pList[i];
				}
			else
				Addr.a.m_Offset = Addr.a.m_Offset + i;

			TranslateAddress(Addr);

			WORD Prop  = GetProp (Addr);

			WORD Block = GetBlock(Addr);

			AddWord(Block);

			AddWord(Prop);
			}
		}

	if( Transact(TRUE) ) {

		PDWORD d = PDWORD(m_pRxData+19);

		for( UINT n = 0; n < m_ReqCount[t]; n++ ) {

			UINT b = m_ReqBlock[t][n];

			UINT i = m_ReqIndex[t][n];

			CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

			if( pBlock ) {

				if( FALSE ) {
					
					DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

					CAddress & Addr = (CAddress &) Data;

					if( pBlock->m_Named ) {

						Addr.a.m_Offset = pBlock->m_pList[i];
						}
					else
						Addr.a.m_Offset = Addr.a.m_Offset + i;

					TranslateAddress(Addr);

					WORD Prop  = GetProp (Addr);

					WORD Block = GetBlock(Addr);

					AfxTrace( "Read %u %3u.%-3u = 0x%8.8X  %-10d  %f\n",
						  1+t,
						  Block,
						  Prop,
						  d[n],
						  d[n],
						  I2R(d[n])
						  );
					}
				
				pBlock->SetCommsData(i, MotorToHost(d[n]));
				}
			}

		FreeRx();

		m_ReqCount[t] = 0;

		return TRUE;
		}

	m_ReqCount[t] = 0;

	return FALSE;
	}

BOOL CProxyHoneywell::ReadFunc03(UINT b, PDWORD pData, PCWORD pProps, UINT uProps)
{
	StartRead(3);

	AddByte(uProps);
	
	AddByte(0);

	AddByte(2);

	for( UINT n = 0; n < uProps; n++ ) {

		AddWord(b);

		AddWord(pProps[n]);
		}

	if( Transact(TRUE) ) {

		PDWORD d = PDWORD(m_pRxData+19);

		for( UINT n = 0; n < uProps; n++ ) {

			pData[pProps[n]] = MotorToHost(d[n]);
			}

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::SendFunc03(BYTE bType, WORD Block, WORD Prop, DWORD dwData)
{
	StartWrite(3);

	AddByte(1);
	
	AddByte(0);

	AddByte(bType);

	AddWord(Block);

	AddWord(Prop);

	AddData(dwData);

	if( FALSE ) {

		AfxTrace( "Send %u %3u.%-3u = 0x%8.8X  %-10d  %f\n",
			  bType,
			  Block,
			  Prop,
			  dwData,
			  dwData,
			  I2R(dwData)
			  );
		}

	if( Transact(FALSE) ) {

		if( m_pRxData[16] != 0x0A ) {

			ShowError("ERROR %u", m_pRxData[17]);
			}

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::SendFunc03(CAddress const &Addr, DWORD dwData)
{
	WORD Prop  = GetProp (Addr);

	WORD Block = GetBlock(Addr);

	BYTE bType = Addr.a.m_Extra;

	return SendFunc03(bType, Block, Prop, dwData);
	}

BOOL CProxyHoneywell::SendFunc03(UINT b, PDWORD pData, PCWORD pProps, UINT uProps)
{
	// TODO -- Write all in one frame?

	for( UINT n = 0; n < uProps; n++ ) {

		if( !SendFunc03(2, b, pProps[n], pData[pProps[n]]) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::UsesFunc03(CAddress const &Addr)
{
	switch( Addr.a.m_Extra ) {

		case 0:
			switch( Addr.a.m_Table ) {

				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
					return TRUE;
				}
			break;

		case 1:
		case 2:
			return TRUE;

		case 10:
		case 11:
			return TRUE;
		}

	return FALSE;
	}

void CProxyHoneywell::TranslateAddress(CAddress &Addr)
{
	if( Addr.a.m_Extra == 0 ) {

		WORD Type = Addr.a.m_Table;
		
		WORD Data = Addr.a.m_Offset;

		if( Type == 1 ) {

			// Peer Text

			WORD Block = Data % 200 / 4 + 4000;

			WORD Prop  = Data % 200 % 4 + 0;

			SetBlock(Addr, Block);

			SetProp (Addr, Prop);

			Addr.a.m_Extra = 1;
			}

		if( Type == 2 ) {

			// Peer Data

			WORD Block = Data % 100 + 4000;

			WORD Prop  = Data / 100;

			SetBlock(Addr, Block);

			SetProp (Addr, Prop);

			Addr.a.m_Extra = 2;
			}

		if( Type == 3 ) {

			// PPO Data

			WORD Block = Data % 200 + 3800;
			
			WORD Prop  = 0;

			switch( Data / 200 ) {

				case 1:	Prop =  8; break;
				case 2:	Prop =  9; break;
				case 3:	Prop = 20; break;
				case 4:	Prop = 21; break;
				}

			SetBlock(Addr, Block);

			SetProp (Addr, Prop);

			Addr.a.m_Extra = 1;
			}

		if( Type == 4 ) {

			// Modbus Serial

			WORD Prop = Data / 200;

			if( Prop == 1 ) {

				WORD Item = Data % 200 / 4;

				WORD Step = Data % 200 % 4;

				SetProp(Addr, Prop + 20 * Item + Step);
				}
			else {
				WORD Item = Data % 200;

				SetProp(Addr, Prop + 20 * Item);
				}

			SetBlock(Addr, 3792);

			Addr.a.m_Extra = 2;
			}

		if( Type == 5 ) {

			// Modbus TCP

			WORD Prop = Data / 200;

			if( Prop == 1 ) {

				WORD Item = Data % 200 / 4;

				WORD Step = Data % 200 % 4;

				SetProp(Addr, Prop + 20 * Item + Step);
				}
			else {
				WORD Item = Data % 200;

				SetProp(Addr, Prop + 20 * Item);
				}

			SetBlock(Addr, 3791);

			Addr.a.m_Extra = 2;
			}

		if( Type == 6 ) {

			// Host Connections

			WORD Prop = Data / 100;

			WORD Item = Data % 100 + 1;

			SetProp (Addr, Prop + 100 * Item);

			SetBlock(Addr, 3790);

			Addr.a.m_Extra = 2;
			}
		}
	}

BOOL CProxyHoneywell::SendTime(void)
{
	if( m_Bias == NOTHING ) {

		if( SendFunc03(2, 1, 72, m_Time + TIME_ADJUST) ) {

			if( SendFunc03(2, 25, 29, R2I(1)) ) {

				return TRUE;
				}
			}
		}
	else {
		if( SendFunc03(2, 1, 71, m_Time + 852076800) ) {

			if( SendFunc03(2, 25, 6, m_Bias) ) {

				if( SendFunc03(2, 25, 29, R2I(1)) ) {
			
					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Function Code 05

void CProxyHoneywell::InitFunc05(void)
{
	for( UINT p = 0; p < 2; p++ ) {

		m_nSPP = 0;

		for( UINT b = 0; b < m_pList->m_uCount; b++ ) {

			CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

			if( pBlock ) {

				DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

				CAddress & Addr = (CAddress &) Data;

				if( !GetProp(Addr) ) {
		
					if( Addr.a.m_Extra == 8 ) {

						if( p ) {

							m_pSPP[m_nSPP].m_Block = GetBlock(Addr);
							}

						m_nSPP++;
						}
					}
				}
			}

		if( p == 0 ) {

			m_pSPP = New CSPP [ m_nSPP ];

			memset(m_pSPP, 0, sizeof(CSPP) * m_nSPP);
			}

		memset(m_WorkSPP, 0, sizeof(m_WorkSPP));
		}
	}

void CProxyHoneywell::KillFunc05(void)
{
	delete [] m_pSPP;
	}

void CProxyHoneywell::WipeFunc05(BOOL fNew)
{
	for( UINT n = 0; n < m_nSPP; n++ ) {

		CSPP &SPP = m_pSPP[n];

		if( !fNew ) {

			SPP.m_fValid = FALSE;
			}
		}
	}

BOOL CProxyHoneywell::ScanFunc05(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr)
{
	if( UsesFunc05(Addr) ) {

		WORD Block = GetBlock(Addr);

		WORD Prop  = GetProp (Addr);

		for( UINT n = 0; n < m_nSPP; n++ ) {

			CSPP &SPP = m_pSPP[n];

			if( Block == SPP.m_Block ) {

				if( SPP.m_fValid ) {

					BOOL fSend = FALSE;

					UINT Write = Prop;

					for( INT i = 0; i < pBlock->m_Size; i++ ) {

						if( pBlock->ShouldWrite(i) ) {

							DWORD Data = pBlock->GetWriteData(i);

							WriteSPPS(SPP, Write, Data);

							pBlock->SetWriteDone(i);

							fSend = TRUE;
							}

						Write++;
						}

					if( fSend ) {

						if( SPP.m_Block < SYS_BLOCK_SEQ ) {

							if( !SendFunc05(SPP) ) {

								return FALSE;
								}
							}
						}
					else {
						WORD Read = Prop;

						for( INT i = 0; i < pBlock->m_Size; i++ ) {

							DWORD Data = ParseSPPS(SPP, Read);

							pBlock->SetCommsData(i, Data);

							Read++;
							}
						}
					}

				for( INT i = 0; i < pBlock->m_Size; i++ ) {

					if( pBlock->ShouldRead(i) ) {

						SPP.m_fScan = TRUE;

						break;
						}
					}

				break;
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ScanFunc05(void)
{
	for( UINT n = 0; n < m_nSPP; n++ ) {

		CSPP &SPP = m_pSPP[n];

		if( SPP.m_fScan ) {

			if( SPP.m_Block < SYS_BLOCK_SEQ ) {

				if( !ReadFunc05(SPP) ) {

					SPP.m_fValid = FALSE;

					return FALSE;
					}
				}

			SPP.m_fScan  = FALSE;

			SPP.m_fValid = TRUE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadFunc05(CSPP &SPP)
{
	UINT n = 10;

	for( UINT s = 0; s < 50; s += n ) {

		StartRead(5);

		AddByte(n);

		AddWord(SPP.m_Block);

		AddByte(BYTE(s+1));

		if( Transact(TRUE) ) {

			memcpy( SPP.m_bSeg[s],
				m_pRxData + 19,
				n * 16
				);

			FreeRx();

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::SendFunc05(CSPP &SPP)
{
	UINT n = 10;

	for( UINT s = 0; s < 50; s += n ) {

		StartWrite(5);

		AddByte(n);

		AddWord(SPP.m_Block);

		AddByte(BYTE(s+1));

		AddData(SPP.m_bSeg[s], n * 16);

		if( Transact(TRUE) ) {

			FreeRx();

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::UsesFunc05(CAddress const &Addr)
{
	return Addr.a.m_Extra == 8;
	}

DWORD CProxyHoneywell::ParseSPPS(CSPP &SPP, UINT a)
{
	UINT uSlot = a % 50;

	UINT uProp = a / 50;

	if( uProp < 21 ) {

		CDataSPP *pRec = (CDataSPP *) SPP.m_bSeg[uSlot];

		if( uProp == 0 ) {

			return pRec->RampSegment;
			}

		if( uProp == 1 ) {

			return pRec->GuarSoakEnable;
			}

		if( uProp >= 2 && uProp < 18 ) {

			DWORD m = (1 << (uProp - 2));

			return (MotorToHost(pRec->Events) & m) ? TRUE : FALSE;
			}

		if( uProp == 18 ) {

			return MotorToHost(pRec->TimeOrRate);
			}

		if( uProp == 19 ) {

			return MotorToHost(pRec->StartValue);
			}

		if( uProp == 20 ) {

			return MotorToHost(pRec->AuxValue);
			}
		}

	return 0;
	}

BOOL CProxyHoneywell::WriteSPPS(CSPP &SPP, UINT a, DWORD d)
{
	UINT uSlot = a % 50;

	UINT uProp = a / 50;

	if( uProp < 21 ) {

		CDataSPP *pRec = (CDataSPP *) SPP.m_bSeg[uSlot];

		if( uProp == 0 ) {

			pRec->RampSegment = d;

			return TRUE;
			}

		if( uProp == 1 ) {

			pRec->GuarSoakEnable = d;

			return TRUE;
			}

		if( uProp >= 2 && uProp < 18 ) {

			DWORD m = (1 << (uProp - 2));

			pRec->Events &= ~m;

			pRec->Events |= (d ? m : 0);

			return TRUE;
			}

		if( uProp == 18 ) {

			pRec->TimeOrRate = d;

			return TRUE;
			}

		if( uProp == 19 ) {

			pRec->StartValue = d;

			return TRUE;
			}

		if( uProp == 20 ) {

			pRec->AuxValue = d;

			return TRUE;
			}
		}

	return FALSE;
	}

CProxyHoneywell::CSPP * CProxyHoneywell::FindWorkSPP(void)
{
	for( UINT n = 0; n < m_nSPP; n++ ) {

		CSPP &SPP = m_pSPP[n];

		if( SPP.m_Block == SYS_BLOCK_SPP ) {

			return &SPP;
			}
		}

	return NULL;
	}

BOOL CProxyHoneywell::SendWorkSPP(WORD Block)
{
	CSPP *pSPP = FindWorkSPP();

	if( pSPP ) {

		if( Block < 30000 ) {

			pSPP->m_Block = Block;

			BOOL fOkay    = SendFunc05(*pSPP);

			pSPP->m_Block = SYS_BLOCK_SPP;

			if( fOkay ) {

				fOkay = SendFunc03( Block,
						    m_WorkSPP,
						    m_ListSPP,
						    elements(m_ListSPP)
						    );
				}

			return fOkay;
			}

		if( (Block -= 30000) ) {

			CHeadSPP Head;

			if( ReadTable(Block, 1, PBYTE(&Head), sizeof(Head)) ) {

				UINT  uSize = 0;

				PBYTE pData = MakeImageSPP(uSize, pSPP, Head.wRecordCount, Block);

				if( SendTable(Block, 1, 0xFFFF, pData, uSize) ) {

					delete [] pData;

					return TRUE;
					}

				delete [] pData;
				}
			}
		}

	return FALSE;
	}

BOOL CProxyHoneywell::ReadWorkSPP(WORD Block)
{
	CSPP *pSPP = FindWorkSPP();

	if( pSPP ) {

		if( Block < 30000 ) {

			pSPP->m_Block = Block;

			BOOL fOkay    = ReadFunc05(*pSPP);

			pSPP->m_Block = SYS_BLOCK_SPP;

			if( fOkay ) {

				fOkay = ReadFunc03( Block,
						    m_WorkSPP,
						    m_ListSPP,
						    elements(m_ListSPP)
						    );
				}

			return fOkay;
			}

		if( (Block -= 30000) ) {

			CHeadSPP Head;

			if( ReadTable(12, Block, PBYTE(&Head), sizeof(Head)) ) {

				UINT r = 1 + 50 * (Block - 1);

				for( UINT i = 0; i < 50; i++ ) {

					if( !ReadTable(13, r, pSPP->m_bSeg[i], 16) ) {

						return FALSE;
						}

					r++;
					}

				ReadHeadSPP(Head);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Function Code 07

void CProxyHoneywell::InitFunc07(void)
{
	}

BOOL CProxyHoneywell::ReadFunc07(void)
{
	StartRead(7);

	if( Transact(TRUE) ) {

		PBYTE pData = m_pRxData+18;

		memset(m_FileHead, 0, 16);

		m_FileHead[0] = pData[0];
		m_FileHead[1] = pData[1];
		m_FileHead[2] = pData[2];
		m_FileHead[3] = pData[3];
		m_FileHead[4] = 1;
		m_FileHead[5] = 0;
		m_FileHead[6] = pData[4];
		m_FileHead[7] = pData[5];

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::IsValidRecipe(PBYTE pHead)
{
	return pHead[2] >= 29;
	}

BOOL CProxyHoneywell::IsValidHead(PCBYTE pHead)
{
	return !memcmp(pHead+0, m_FileHead+0, 4) && !memcmp(pHead+6, m_FileHead+6, 2);
	}

// Function Code 18

void CProxyHoneywell::InitFunc18(void)
{
	for( UINT p = 0; p < 2; p++ ) {

		m_nSPS = 0;

		for( UINT b = 0; b < m_pList->m_uCount; b++ ) {

			CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

			if( pBlock ) {

				DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

				CAddress & Addr = (CAddress &) Data;

				if( !GetProp(Addr) ) {
	
					if( Addr.a.m_Extra == 7 ) {

						if( p ) {

							CSPS &SPS = m_pSPS[m_nSPS];

							SPS.m_Block          = GetBlock(Addr);

							SPS.m_Disp.m_bTable  = 50;

							SPS.m_Disp.m_wRecord = 1 + m_nSPS;
							}

						m_nSPS++;
						}
					}
				}
			}

		if( p == 0 ) {

			m_pSPS = New CSPS [ m_nSPS ];

			memset(m_pSPS, 0, sizeof(CSPS) * m_nSPS);
			}

		memset(m_WorkSPS, 0, sizeof(m_WorkSPS));
		}
	}

void CProxyHoneywell::KillFunc18(void)
{
	for( UINT n = 0; n < m_nSPS; n++ ) {

		FreeTable(m_pSPS[n].m_Disp);
		}

	delete [] m_pSPS;
	}

void CProxyHoneywell::WipeFunc18(BOOL fNew)
{
	for( UINT n = 0; n < m_nSPS; n++ ) {

		CSPS &SPS = m_pSPS[n];

		if( !fNew ) {

			SPS.m_fValid = FALSE;
			}

		WipeTable(SPS.m_Disp);
		}
	}

BOOL CProxyHoneywell::ScanFunc18(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr)
{
	if( UsesFunc18(Addr) ) {
						
		WORD Block = GetBlock(Addr);

		WORD Prop  = GetProp (Addr);

		for( UINT n = 0; n < m_nSPS; n++ ) {

			CSPS &SPS = m_pSPS[n];

			if( Block == SPS.m_Block ) {

				if( SPS.m_fValid ) {

					BOOL fSend = FALSE;

					UINT Write = Prop;

					if( Addr.a.m_Extra == 7 ) {

						for( INT i = 0; i < pBlock->m_Size; i++ ) {

							if( pBlock->ShouldWrite(i) ) {

								DWORD Data = pBlock->GetWriteData(i);

								WriteSPSS(SPS, Write, Data);

								pBlock->SetWriteDone(i);

								fSend = TRUE;
								}

							Write++;
							}
						}
					else {
						for( INT i = 0; i < pBlock->m_Size; i++ ) {

							if( pBlock->ShouldWrite(i) ) {

								pBlock->SetWriteDone(i);
								}
							}
						}

					if( fSend ) {

						if( SPS.m_Block < SYS_BLOCK_SEQ ) {

							if( !SendFunc18(SPS) ) {

								return FALSE;
								}
							}
						}
					else {
						if( Addr.a.m_Extra == 6 ) {

							DWORD Read = Prop;

							for( INT i = 0; i < pBlock->m_Size; i++ ) {

								DWORD Data = ParseSPSD(SPS, Read);

								pBlock->SetCommsData(i, Data);

								Read++;
								}
							}

						if( Addr.a.m_Extra == 7 ) {

							DWORD Read = Prop;

							for( INT i = 0; i < pBlock->m_Size; i++ ) {

								DWORD Data = ParseSPSS(SPS, Read);

								pBlock->SetCommsData(i, Data);

								Read++;
								}
							}
						}
					}

				for( INT i = 0; i < pBlock->m_Size; i++ ) {

					if( pBlock->ShouldRead(i) ) {

						SPS.m_fScan = TRUE;

						break;
						}
					}

				break;
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ScanFunc18(void)
{
	for( UINT n = 0; n < m_nSPS; n++ ) {

		CSPS &SPS = m_pSPS[n];

		if( SPS.m_fScan ) {

			if( SPS.m_Block < SYS_BLOCK_SEQ ) {

				if( !ReadFunc18(SPS) ) {

					SPS.m_fValid = FALSE;

					return FALSE;
					}

				if( !ReadTable(SPS.m_Disp, TRUE) ) {

					SPS.m_fValid = FALSE;

					return FALSE;
					}
				}
			
			SPS.m_fScan  = FALSE;

			SPS.m_fValid = TRUE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadFunc18(CSPS &SPS)
{
	UINT n = 2;

	for( UINT s = 0; s < 50; s += n ) {

		StartRead(18);

		AddByte(n);

		AddWord(SPS.m_Block);

		AddByte(BYTE(s+1));

		if( Transact(TRUE) ) {

			memcpy( SPS.m_bSeg[s],
				m_pRxData + 19,
				n * 84
				);

			FreeRx();

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::SendFunc18(CSPS &SPS)
{
	UINT n = 2;

	for( UINT s = 0; s < 50; s += n ) {

		StartWrite(18);

		AddByte(n);

		AddWord(SPS.m_Block);

		AddByte(BYTE(s+1));

		AddData(SPS.m_bSeg[s], n * 84);

		if( Transact(TRUE) ) {

			FreeRx();

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::UsesFunc18(CAddress const &Addr)
{
	return Addr.a.m_Extra == 6 || Addr.a.m_Extra == 7;
	}

DWORD CProxyHoneywell::ParseSPSS(CSPS &SPS, UINT a)
{
	UINT uSlot = a % 50;

	UINT uProp = a / 50;

	if( uProp < 43 ) {

		CDataSPS *pRec = (CDataSPS *) SPS.m_bSeg[uSlot];

		if( uProp == 0 ) {

			return MotorToHost(pRec->SegmentTime);
			}

		if( uProp >= 1 && uProp < 9 ) {

			return pRec->GuarSoakType[uProp - 1];
			}

		if( uProp == 9 ) {

			return MotorToHost(pRec->RecycleCount);
			}

		if( uProp == 10 ) {

			return MotorToHost(pRec->RecycleSegment);
			}

		if( uProp >= 11 && uProp < 19 ) {

			return MotorToHost(pRec->StartValue[uProp - 11]);
			}

		if( uProp >= 19 && uProp < 27 ) {

			return MotorToHost(pRec->AuxValue[uProp - 19]);
			}

		if( uProp >= 27 && uProp < 43 ) {

			DWORD m = (1 << (uProp - 27));

			return (MotorToHost(pRec->Events) & m) ? TRUE : FALSE;
			}
		}

	return 0;
	}

BOOL CProxyHoneywell::WriteSPSS(CSPS &SPS, UINT a, DWORD d)
{
	UINT uSlot = a % 50;

	UINT uProp = a / 50;

	if( uProp < 43 ) {

		CDataSPS *pRec = (CDataSPS *) SPS.m_bSeg[uSlot];

		if( uProp == 0 ) {

			pRec->SegmentTime = HostToMotor(d);

			return TRUE;
			}

		if( uProp >= 1 && uProp < 9 ) {

			pRec->GuarSoakType[uProp - 1] = HostToMotor(d);

			return TRUE;
			}

		if( uProp == 9 ) {

			pRec->RecycleCount = HostToMotor(d);

			return TRUE;
			}

		if( uProp == 10 ) {

			pRec->RecycleSegment = HostToMotor(d);

			return TRUE;
			}

		if( uProp >= 11 && uProp < 19 ) {

			pRec->StartValue[uProp - 11] = HostToMotor(d);

			return TRUE;
			}

		if( uProp >= 19 && uProp < 27 ) {

			pRec->AuxValue[uProp - 19] = HostToMotor(d);

			return TRUE;
			}

		if( uProp >= 27 && uProp < 43 ) {

			DWORD m = (1 << (uProp - 27));

			DWORD e = MotorToHost(pRec->Events);

			e &= ~m;

			e |= (d ? m : 0);

			pRec->Events = HostToMotor(e);

			return TRUE;
			}
		}

	return FALSE;
	}

DWORD CProxyHoneywell::ParseSPSD(CSPS &SPS, UINT a)
{
	if( SPS.m_Disp.m_uVersion == 1 ) {

		static CLayout const Table[] = {

			//	Min	Max	Pos	Len
			{	  0,	 16,	  0,	  8	},
			{	 16,	 32,	 64,	  4	},
			{	 32,	 40,	 96,	  1	},
			{	 40,	 56,	104,	  8	},
			{	 56,	 72,	168,	  4	},
			{	 72,	 80,	200,	  1	},
			{	 80,	 96,	208,	  8	},

			};

		PCBYTE pData = SPS.m_Disp.m_pData;

		return Extract(pData, Table, elements(Table), a);
		}

	if( SPS.m_Disp.m_uVersion == 2 ) {

		static CLayout const Table[] = {

			//	Min	Max	Pos	Len
			{	  0,	 16,	  0,	  8	},
			{	 16,	 32,	 64,	  6	},
			{	 32,	 40,	112,	  1	},
			{	 40,	 56,	120,	  8	},
			{	 56,	 72,	184,	  6	},
			{	 72,	 80,	232,	  1	},
			{	 80,	 96,	240,	  8	},

			};

		PCBYTE pData = SPS.m_Disp.m_pData;

		return Extract(pData, Table, elements(Table), a);
		}

	return 0;
	}

CProxyHoneywell::CSPS * CProxyHoneywell::FindWorkSPS(void)
{
	for( UINT n = 0; n < m_nSPS; n++ ) {

		CSPS &SPS = m_pSPS[n];

		if( SPS.m_Block == SYS_BLOCK_SPS ) {

			return &SPS;
			}
		}

	return NULL;
	}

BOOL CProxyHoneywell::SendWorkSPS(WORD Block)
{
	CSPS *pSPS = FindWorkSPS();

	if( pSPS ) {

		if( Block < 30000 ) {

			pSPS->m_Block = Block;

			BOOL fOkay    = SendFunc18(*pSPS);

			pSPS->m_Block = SYS_BLOCK_SPS;

			if( fOkay ) {

				fOkay = SendFunc03( Block,
						    m_WorkSPS,
						    m_ListSPS,
						    elements(m_ListSPS)
						    );
				}

			return fOkay;
			}

		if( (Block -= 30000) ) {

			CHeadSPS Head;

			if( ReadTable(Block, 3, PBYTE(&Head), sizeof(Head)) ) {

				UINT  uSize = 0;

				PBYTE pData = MakeImageSPS(uSize, pSPS, Head.wRecordCount, Block);

				if( SendTable(Block, 3, 0xFFFF, pData, uSize) ) {

					delete [] pData;

					return TRUE;
					}

				delete [] pData;
				}
			}

		}

	return FALSE;
	}

BOOL CProxyHoneywell::ReadWorkSPS(WORD Block)
{
	CSPS *pSPS = FindWorkSPS();

	if( pSPS ) {

		if( Block < 30000 ) {

			pSPS->m_Block = Block;

			BOOL fOkay    = ReadFunc18(*pSPS);

			pSPS->m_Block = SYS_BLOCK_SPS;

			if( fOkay ) {

				fOkay = ReadFunc03( Block,
						    m_WorkSPS,
						    m_ListSPS,
						    elements(m_ListSPS)
						    );
				}

			return fOkay;
			}

		if( (Block -= 30000) ) {

			CHeadSPS Head;

			if( ReadTable(48, Block, PBYTE(&Head), sizeof(Head)) ) {

				UINT r = 1 + 50 * (Block - 1);

				for( UINT i = 0; i < 50; i++ ) {

					if( !ReadTable(49, r, pSPS->m_bSeg[i], 84) ) {

						return FALSE;
						}

					r++;
					}

				ReadHeadSPS(Head);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Function Code 20

void CProxyHoneywell::InitFunc20(void)
{
	}

void CProxyHoneywell::KillFunc20(void)
{
	}

void CProxyHoneywell::WipeFunc20(BOOL fNew)
{
	}

BOOL CProxyHoneywell::ScanFunc20(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr)
{
	if( UsesFunc20(Addr) ) {

		WORD Block = GetBlock(Addr);

		WORD Prop  = GetProp (Addr);

		BOOL fSend = FALSE;

		if( pBlock->ShouldWrite(16) ) {

			DWORD Code = pBlock->GetWriteData(16);
			
			if( Code == 1 ) {

				BYTE Data[16];

				if( ReadFunc20(Block, Data) ) {

					for( INT i = 0; i < 16; i++ ) {

						pBlock->SetCommsData(i, Data[i]);
						}

					pBlock->SetWriteDone(16);

					return TRUE;
					}

				return FALSE;
				}

			if( Code == 2 ) {

				BYTE Data[16];

				if( ReadFunc20(Block, Data) ) {

					for( INT i = 0; i < 16; i++ ) {

						Data[i] = pBlock->GetWriteData(i);
						}

					if( SendFunc20(Block, Data) ) {

						pBlock->SetWriteDone(16);

						return TRUE;
						}
					}

				return FALSE;
				}

			pBlock->SetWriteDone(16);
			}

		for( INT i = 0; i < pBlock->m_Size; i++ ) {

			if( i == 16 ) {

				pBlock->SetCommsData(i, 0);
				}
			else {
				if( pBlock->ShouldWrite(i) ) {

					pBlock->SetWriteDone(i);
					}

				pBlock->SetCommsData(i);
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ScanFunc20(void)
{
	return TRUE;
	}

BOOL CProxyHoneywell::ReadFunc20(WORD Block, PBYTE pData)
{
	StartRead(20);

	AddWord(Block);

	if( Transact(TRUE) ) {

		memcpy( pData,
			m_pRxData + 18,
			16
			);

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::SendFunc20(WORD Block, PBYTE pData)
{
	StartWrite(20);

	AddWord(Block);

	for( UINT n = 0; n < 16; n++ ) {

		AddByte(pData[n]);
		}

	if( Transact(FALSE) ) {

		if( m_pRxData[16] != 0x0A ) {

			ShowError("ERROR %u", m_pRxData[17]);
			}

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::UsesFunc20(CAddress const &Addr)
{
	return Addr.a.m_Extra == 9;
	}

// Function Code 26

void CProxyHoneywell::InitFunc26(void)
{
	for( UINT p = 0; p < 2; p++ ) {

		m_nSEQ = 0;

		for( UINT b = 0; b < m_pList->m_uCount; b++ ) {

			CCommsSysBlock *pBlock = m_pList->m_ppBlock[b];

			if( pBlock ) {

				DWORD      Data = MAKELONG(pBlock->m_Index, pBlock->m_Space);

				CAddress & Addr = (CAddress &) Data;

				if( !GetProp(Addr) ) {
	
					if( Addr.a.m_Extra == 5 ) {

						if( p ) {

							CSEQ &SEQ = m_pSEQ[m_nSEQ];

							SEQ.m_Block          = GetBlock(Addr);

							SEQ.m_Tags.m_bTable  = 60;

							SEQ.m_Tags.m_wRecord = 1 + m_nSEQ;
							}

						m_nSEQ++;
						}
					}
				}
			}

		if( p == 0 ) {

			m_pSEQ = New CSEQ [ m_nSEQ ];

			memset(m_pSEQ, 0, sizeof(CSEQ) * m_nSEQ);
			}

		memset(m_WorkSEQ, 0, sizeof(m_WorkSEQ));
		}
	}

void CProxyHoneywell::KillFunc26(void)
{
	for( UINT n = 0; n < m_nSEQ; n++ ) {

		FreeTable(m_pSEQ[n].m_Tags);
		}

	delete [] m_pSEQ;
	}

void CProxyHoneywell::WipeFunc26(BOOL fNew)
{
	for( UINT n = 0; n < m_nSEQ; n++ ) {

		CSEQ &SEQ = m_pSEQ[n];

		if( !fNew ) {

			SEQ.m_fValid = FALSE;
			}

		WipeTable(SEQ.m_Tags);
		}
	}

BOOL CProxyHoneywell::ScanFunc26(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr)
{
	if( UsesFunc26(Addr) ) {
						
		WORD Block = GetBlock(Addr);

		WORD Prop  = GetProp (Addr);

		for( UINT n = 0; n < m_nSEQ; n++ ) {

			CSEQ &SEQ = m_pSEQ[n];

			if( Block == SEQ.m_Block ) {

				if( SEQ.m_fValid ) {

					BOOL fSend = FALSE;

					UINT Write = Prop;

					if( Addr.a.m_Extra == 5 ) {

						for( INT i = 0; i < pBlock->m_Size; i++ ) {

							if( pBlock->ShouldWrite(i) ) {

								DWORD Data = pBlock->GetWriteData(i);

								WriteSEQS(SEQ, Write, Data);

								pBlock->SetWriteDone(i);

								fSend = TRUE;
								}

							Write++;
							}
						}
					else {
						for( INT i = 0; i < pBlock->m_Size; i++ ) {

							if( pBlock->ShouldWrite(i) ) {

								pBlock->SetWriteDone(i);
								}
							}
						}

					if( fSend ) {

						if( SEQ.m_Block < SYS_BLOCK_SEQ ) {

							if( !SendFunc26(SEQ) ) {

								return FALSE;
								}
							}
						}
					else {
						if( Addr.a.m_Extra == 3 ) {

							WORD Read = Prop;

							for( INT i = 0; i < pBlock->m_Size; i++ ) {

								DWORD Data = ParseSEQH(SEQ, Read);

								pBlock->SetCommsData(i, Data);

								Read++;
								}
							}

						if( Addr.a.m_Extra == 4 ) {

							WORD Read = Prop;

							for( INT i = 0; i < pBlock->m_Size; i++ ) {

								DWORD Data = ParseSEQP(SEQ, Read);

								pBlock->SetCommsData(i, Data);

								Read++;
								}
							}

						if( Addr.a.m_Extra == 5 ) {

							WORD Read = Prop;

							for( INT i = 0; i < pBlock->m_Size; i++ ) {

								DWORD Data = ParseSEQS(SEQ, Read);

								pBlock->SetCommsData(i, Data);

								Read++;
								}
							}
						}
					}

				for( INT i = 0; i < pBlock->m_Size; i++ ) {

					if( pBlock->ShouldRead(i) ) {

						SEQ.m_fScan = TRUE;

						break;
						}
					}

				break;
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ScanFunc26(void)
{
	for( UINT n = 0; n < m_nSEQ; n++ ) {

		CSEQ &SEQ = m_pSEQ[n];

		if( SEQ.m_fScan ) {

			if( SEQ.m_Block < SYS_BLOCK_SEQ ) {

				if( !ReadFunc26(SEQ) ) {

					SEQ.m_fValid = FALSE;

					return FALSE;
					}

				if( !ReadTable(SEQ.m_Tags, TRUE) ) {

					SEQ.m_fValid = FALSE;

					return FALSE;
					}
				}

			SEQ.m_fScan  = FALSE;

			SEQ.m_fValid = TRUE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadFunc26(CSEQ &SEQ)
{
	UINT n = 8;

	for( UINT s = 0; s < 64; s += n ) {

		StartRead(26);

		AddByte(n);

		AddWord(SEQ.m_Block);

		AddByte(BYTE(s+1));

		if( Transact(TRUE) ) {

			memcpy( SEQ.m_bSeg[s],
				m_pRxData + 19,
				n * 16
				);

			FreeRx();

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::SendFunc26(CSEQ &SEQ)
{
	if( SEQ.m_Block < SYS_BLOCK_SEQ ) {

		UINT n = 1;

		for( UINT s = 0; s < 64; s += n ) {

			StartWrite(26);

			AddByte(n);

			AddWord(SEQ.m_Block);

			AddByte(BYTE(s+1));

			AddData(SEQ.m_bSeg[s], n * 16);

			if( Transact(TRUE) ) {

				FreeRx();

				continue;
				}

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::UsesFunc26(CAddress const &Addr)
{
	return Addr.a.m_Extra == 3 || Addr.a.m_Extra == 4 || Addr.a.m_Extra == 5;
	}

DWORD CProxyHoneywell::ParseSEQS(CSEQ &SEQ, UINT a)
{
	UINT uSlot = a % 64;

	UINT uProp = a / 64;

	if( uProp < 7 ) {

		CDataSEQ *pRec = (CDataSEQ *) SEQ.m_bSeg[uSlot];

		if( uProp == 0 ) {

			return pRec->PhaseNum;
			}

		if( uProp == 1 ) {

			return pRec->TimeNextStep;
			}

		if( uProp == 2 ) {

			return pRec->Sig1NextStep;
			}

		if( uProp == 3 ) {

			return pRec->Sig2NextStep;
			}

		if( uProp == 4 ) {

			return pRec->AdvNextStep;
			}

		if( uProp == 5 ) {

			return MotorToHost(pRec->Time);
			}

		if( uProp == 6 ) {

			return MotorToHost(pRec->Aux);
			}
		}

	return 0;
	}

BOOL CProxyHoneywell::WriteSEQS(CSEQ &SEQ, UINT a, DWORD d)
{
	UINT uSlot = a % 64;

	UINT uProp = a / 64;

	if( uProp < 7 ) {

		CDataSEQ *pRec = (CDataSEQ *) SEQ.m_bSeg[uSlot];

		if( uProp == 0 ) {

			pRec->PhaseNum = d;

			return TRUE;
			}

		if( uProp == 1 ) {

			pRec->TimeNextStep = d;

			return TRUE;
			}

		if( uProp == 2 ) {

			pRec->Sig1NextStep = d;

			return TRUE;
			}

		if( uProp == 3 ) {

			pRec->Sig2NextStep = d;

			return TRUE;
			}

		if( uProp == 4 ) {

			pRec->AdvNextStep = d;

			return TRUE;
			}

		if( uProp == 5 ) {

			pRec->Time = HostToMotor(d);

			return TRUE;
			}

		if( uProp == 6 ) {

			pRec->Aux = HostToMotor(d);

			return TRUE;
			}
		}

	return FALSE;
	}

DWORD CProxyHoneywell::ParseSEQP(CSEQ &SEQ, UINT a)
{
	if( SEQ.m_Tags.m_uVersion == 1 ) {

		static CLayout const Table[] = {

			//	 Min	 Max	 Pos	Len
			{	   0,	 150,	  26,	 12	},
			{	 150,	 950,    754,	800	},
			{	 950,	1150,	 854,	200	},
			{	1150,	1350,	 954,	200	},

			};

		PCBYTE pData = SEQ.m_Tags.m_pData;

		return Extract(pData, Table, elements(Table), a);
		}

	if( SEQ.m_Tags.m_uVersion == 2 ) {

		static CLayout const Table[] = {

			//	 Min	 Max	 Pos	Len
			{	   0,	 150,	  34,	 12	},
			{	 150,	 950,    762,	800	},
			{	 950,	1150,	 862,	200	},
			{	1150,	1350,	 962,	200	},

			};

		PCBYTE pData = SEQ.m_Tags.m_pData;

		return Extract(pData, Table, elements(Table), a);
		}

	return 0;
	}

DWORD CProxyHoneywell::ParseSEQH(CSEQ &SEQ, UINT a)
{
	if( SEQ.m_Tags.m_uVersion == 1 ) {

		static CLayout const Table[] = {

			//	Min	Max	 Pos	Len
			{	  0,	 32,	 626,	  8	},
			{	 32,	 34,    1054,	  8	},
			{	 34,	 36,	1062,	  4	},
			{	 36,	 37,	1066,	  1	},

			};

		PCBYTE pData = SEQ.m_Tags.m_pData;

		return Extract(pData, Table, elements(Table), a);
		}

	if( SEQ.m_Tags.m_uVersion == 2 ) {

		static CLayout const Table[] = {

			//	Min	Max	 Pos	Len
			{	  0,	 32,	 634,	  8	},
			{	 32,	 34,    1062,	  8	},
			{	 34,	 36,	1070,	  6	},
			{	 36,	 37,	1076,	  1	},

			};

		PCBYTE pData = SEQ.m_Tags.m_pData;

		return Extract(pData, Table, elements(Table), a);
		}

	return 0;
	}

CProxyHoneywell::CSEQ * CProxyHoneywell::FindWorkSEQ(void)
{
	for( UINT n = 0; n < m_nSEQ; n++ ) {

		CSEQ &SEQ = m_pSEQ[n];

		if( SEQ.m_Block == SYS_BLOCK_SEQ ) {

			return &SEQ;
			}
		}

	return NULL;
	}

BOOL CProxyHoneywell::SendWorkSEQ(WORD Block)
{
	CSEQ *pSEQ = FindWorkSEQ();

	if( pSEQ ) {

		if( Block < 30000 ) {

			pSEQ->m_Block = Block;

			BOOL fOkay    = SendFunc26(*pSEQ);

			pSEQ->m_Block = SYS_BLOCK_SEQ;

			if( fOkay ) {

				fOkay = SendFunc03( Block,
						    m_WorkSEQ,
						    m_ListSEQ,
						    elements(m_ListSEQ)
						    );
				}

			return fOkay;
			}

		if( (Block -= 30000) ) {

			CHeadSEQ Head;

			if( ReadTable(Block, 4, PBYTE(&Head), sizeof(Head)) ) {

				UINT  uSize = 0;

				PBYTE pData = MakeImageSEQ(uSize, pSEQ, Head.wRecordCount, Block);

				if( SendTable(Block, 4, 0xFFFF, pData, uSize) ) {

					delete [] pData;

					return TRUE;
					}

				delete [] pData;
				}
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadWorkSEQ(WORD Block)
{
	CSEQ *pSEQ = FindWorkSEQ();

	if( pSEQ ) {

		if( Block < 30000 ) {

			pSEQ->m_Block = Block;

			BOOL fOkay    = ReadFunc26(*pSEQ);

			pSEQ->m_Block = SYS_BLOCK_SEQ;

			if( fOkay ) {

				fOkay = ReadFunc03( Block,
						    m_WorkSEQ,
						    m_ListSEQ,
						    elements(m_ListSEQ)
						    );
				}

			return fOkay;
			}

		if( (Block -= 30000) ) {

			CHeadSEQ Head;

			if( ReadTable(58, Block, PBYTE(&Head), sizeof(Head)) ) {

				UINT r = 1 + 64 * (Block - 1);

				for( UINT i = 0; i < 64; i++ ) {

					if( !ReadTable(59, r, pSEQ->m_bSeg[i], 16) ) {

						return FALSE;
						}

					r++;
					}

				ReadHeadSEQ(Head);

				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

// Implementation

DWORD CProxyHoneywell::Extract(PCBYTE pData, CLayout const *pTable, UINT uRows, UINT a)
{
	for( UINT i = 0; i < uRows; i++ ) {

		CLayout const &Row = pTable[i];

		if( a >= Row.m_uMin && a < Row.m_uMax ) {

			pData += Row.m_uPos;

			a     -= Row.m_uMin;

			if( Row.m_uLen == 1 ) {

				UINT d = 1;

				pData += Row.m_uLen * (a / d);

				return *pData;
				}

			if( Row.m_uLen == 4 ) {

				UINT d = 2;

				pData += Row.m_uLen * (a / d);

				if( a % 2 ) {

					return 0;
					}

				return MotorToHost(*PDWORD(pData+0));
				}

			if( Row.m_uLen == 6 ) {

				UINT d = 2;

				pData += Row.m_uLen * (a / d);

				if( a % 2 ) {

					return MotorToHost(*PWORD(pData+4)) << 16;
					}

				return MotorToHost(*PDWORD(pData+0));
				}

			if( Row.m_uLen <= 16 ) {

				UINT d = Row.m_uLen / 4;

				pData += Row.m_uLen * (a / d);

				return MotorToHost(*PDWORD(pData + 4 * (a % d)));
				}

			if( Row.m_uLen == 200 ) {

				UINT d = 4;

				pData += 2 * (a / d);

				WORD  wData = *PWORD(pData);

				PCTXT pText = FindSigName(wData);

				return MotorToHost(*PDWORD(pText + 4 * (a % d)));
				}

			if( Row.m_uLen == 800 ) {

				UINT d = 50;

				pData += 2 * (a % d);

				WORD wData = MotorToHost(*PWORD(pData));

				WORD wMask = (1 << (a / d));

				return (wData & wMask) ? TRUE : FALSE;
				}
			}
		}

	return 0;
	}

WORD CProxyHoneywell::GetBlock(CAddress const &Addr)
{
	WORD s = WORD(Addr.a.m_Table << 4 | Addr.a.m_Offset >> 12);

	if( s >= 4000 ) {

		return MotorToHost(m_pPeer[s - 4000]);
		}

	if( s >= 3800 ) {

		return MotorToHost(m_pPPO[s - 3800]);
		}

	if( s == 3790 ) {

		return m_wNetBlock;
		}
	
	if( s == 3791 ) {

		return BLKNUM_MBTCP_SLVSTATUS;
		}

	if( s == 3792 ) {

		return BLKNUM_MBSER_SLVSTATUS;
		}

	return IntelToHost(m_pSlot[s]);
	}

WORD CProxyHoneywell::GetProp(CAddress const &Addr)
{
	return WORD(Addr.a.m_Offset & 4095);
	}

void CProxyHoneywell::SetBlock(CAddress &Addr, WORD Block)
{
	Addr.a.m_Table   = BYTE(Block >> 4);

	Addr.a.m_Offset &= 4095;

	Addr.a.m_Offset |= (Block & 15) << 12;
	}

void CProxyHoneywell::SetProp(CAddress &Addr, WORD Prop)
{
	Addr.a.m_Offset &= ~4095;

	Addr.a.m_Offset |= Prop;
	}

// Variable Recipes

BOOL CProxyHoneywell::SendWorkRcp(WORD Block)
{
	if( (Block -= 30000) ) {

		UINT  uSize = 0;

		PBYTE pData = MakeImageRcp(uSize, Block);

		if( SendTable(Block, 2, 0xFFFF, pData, uSize) ) {

			delete [] pData;

			return TRUE;
			}

		delete [] pData;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::ReadWorkRcp(WORD Block)
{
	if( (Block -= 30000) ) {

		CHeadRcp Head;

		if( ReadTable(9, Block, PBYTE(&Head), sizeof(Head)) ) {

			UINT r = 1 + 50 * (Block - 1);

			for( UINT i = 0; i < 50; i++ ) {

				CDataRcp Data;

				if( m_fOldAlarms ) {

					if( ReadTable(Block, 2, i+1, PBYTE(&Data), sizeof(Data)) ) {

						return FALSE;
						}
					}
				else {
					if( !ReadTable(11, r, PBYTE(&Data), sizeof(Data)) ) {

						return FALSE;
						}
					}

				m_WorkRcp[0*50+i] = Data.VarNum;
				
				m_WorkRcp[1*50+i] = Data.Value;

				r++;
				}

			ReadHeadRcp(Head);

			return TRUE;
			}
		}

	return FALSE;
	}

// Recipe Headers

void CProxyHoneywell::MakeHeadSEQ(CHeadSEQ &Head, WORD wCount, WORD wNumber)
{
	Head.wType        = 4;
	Head.wNumber      = wNumber;
	Head.wRecordCount = Max(wCount, wNumber);
	Head.wRecordSize  = 16;
	
	memcpy(Head.sLabel, m_WorkSEQ +  8, 8);
	memcpy(Head.sDesc,  m_WorkSEQ + 18, 16);

	Head.bTimeUnits   = BYTE(I2R(m_WorkSEQ[12]));
	Head.bJogStep     = BYTE(I2R(m_WorkSEQ[11]));
	}

void CProxyHoneywell::ReadHeadSEQ(CHeadSEQ &Head)
{
	memcpy(m_WorkSEQ +  8, Head.sLabel, 8);
	memcpy(m_WorkSEQ + 18, Head.sDesc,  16);

	m_WorkSEQ[12] = R2I(Head.bTimeUnits);
	m_WorkSEQ[11] = R2I(Head.bJogStep);
	}

void CProxyHoneywell::MakeHeadSPS(CHeadSPS &Head, WORD wCount, WORD wNumber)
{
	Head.wType        = 3;
	Head.wNumber      = wNumber;
	Head.wRecordCount = Max(wCount, wNumber);
	Head.wRecordSize  = 84;
	
	memcpy(Head.sLabel, m_WorkSPS + 28, 8);
	memcpy(Head.sDesc,  m_WorkSPS + 30, 16);

	Head.bTimeUnits   = BYTE(I2R(m_WorkSPS[43]));
	Head.bJogStep     = BYTE(I2R(m_WorkSPS[42]));

	Head.rGuard[0]    = m_WorkSPS[34];
	Head.rGuard[1]    = m_WorkSPS[35];
	Head.rGuard[2]    = m_WorkSPS[36];
	Head.rGuard[3]    = m_WorkSPS[37];
	Head.rGuard[4]    = m_WorkSPS[38];
	Head.rGuard[5]    = m_WorkSPS[39];
	Head.rGuard[6]    = m_WorkSPS[40];
	Head.rGuard[7]    = m_WorkSPS[41];
	}

void CProxyHoneywell::ReadHeadSPS(CHeadSPS &Head)
{
	memcpy(m_WorkSPS + 28, Head.sLabel, 8);
	memcpy(m_WorkSPS + 30, Head.sDesc,  16);

	m_WorkSPS[43] = R2I(Head.bTimeUnits);
	m_WorkSPS[42] = R2I(Head.bJogStep);

	m_WorkSPS[34] = Head.rGuard[0];
	m_WorkSPS[35] = Head.rGuard[1];
	m_WorkSPS[36] = Head.rGuard[2];
	m_WorkSPS[37] = Head.rGuard[3];
	m_WorkSPS[38] = Head.rGuard[4];
	m_WorkSPS[39] = Head.rGuard[5];
	m_WorkSPS[40] = Head.rGuard[6];
	m_WorkSPS[41] = Head.rGuard[7];
	}

void CProxyHoneywell::MakeHeadSPP(CHeadSPP &Head, WORD wCount, WORD wNumber)
{
	Head.wType        = 1;
	Head.wNumber      = wNumber;
	Head.wRecordCount = Max(wCount, wNumber);
	Head.wRecordSize  = 16;

	Head.lTimeStamp   = 0;
	Head.rGuarSoakLo  = m_WorkSPP[23];
	Head.rGuarSoakHi  = m_WorkSPP[24];
	Head.rRestartRate = m_WorkSPP[25];
	Head.bJogSeg      = BYTE(I2R(m_WorkSPP[26]));
	Head.bLoopStart   = BYTE(I2R(m_WorkSPP[27]));
	Head.bLoopEnd     = BYTE(I2R(m_WorkSPP[28]));
	Head.bRepeats     = BYTE(I2R(m_WorkSPP[29]));
	Head.rFixed       = 0;
	Head.rDispHiLim   = 0;
	Head.rDispLoLim   = 0;
	Head.bTimeUnits   = BYTE(I2R(m_WorkSPP[36]));
	Head.bRampTime    = BYTE(I2R(m_WorkSPP[37]));
	Head.bGuardType   = BYTE(I2R(m_WorkSPP[38]));
	Head.bSpare1      = 0;
	Head.wSpare2      = 0;
	
	memcpy(Head.sLabel,    m_WorkSPP + 13, 8);
	memcpy(Head.sDesc,     m_WorkSPP + 15, 16);
	memcpy(Head.sPriDesc,  m_WorkSPP + 19, 8);
	memcpy(Head.sPriUnits, m_WorkSPP + 21, 4);
	memcpy(Head.sAuxDesc,  m_WorkSPP + 30, 8);
	memcpy(Head.sAuxUnits, m_WorkSPP + 32, 4);
	}

void CProxyHoneywell::ReadHeadSPP(CHeadSPP &Head)
{
	m_WorkSPP[23] = Head.rGuarSoakLo;
	m_WorkSPP[24] = Head.rGuarSoakHi;
	m_WorkSPP[25] = Head.rRestartRate;
	m_WorkSPP[26] = R2I(Head.bJogSeg);
	m_WorkSPP[27] = R2I(Head.bLoopStart);
	m_WorkSPP[28] = R2I(Head.bLoopEnd);
	m_WorkSPP[29] = R2I(Head.bRepeats);
	m_WorkSPP[36] = R2I(Head.bTimeUnits);
	m_WorkSPP[37] = R2I(Head.bRampTime);
	m_WorkSPP[38] = R2I(Head.bGuardType);
	
	memcpy(m_WorkSPP + 13, Head.sLabel,    8);
	memcpy(m_WorkSPP + 15, Head.sDesc,     16);
	memcpy(m_WorkSPP + 19, Head.sPriDesc,  8);
	memcpy(m_WorkSPP + 21, Head.sPriUnits, 4);
	memcpy(m_WorkSPP + 30, Head.sAuxDesc,  8);
	memcpy(m_WorkSPP + 32, Head.sAuxUnits, 4);
	}

void CProxyHoneywell::MakeHeadRcp(CHeadRcp &Head, WORD wNumber)
{
	for( UINT i = 50; i; i-- ) {
			
		if( m_WorkRcp[i-1] ) {

			break;
			}
		}

	Head.wType        = 2;
	Head.wNumber      = wNumber;
	Head.wRecordCount = i;
	Head.wRecordSize  = 8;

	memcpy(Head.sLabel, m_WorkRcp + 100,  8);
	memcpy(Head.sDesc,  m_WorkRcp + 102, 16);
	}

void CProxyHoneywell::ReadHeadRcp(CHeadRcp &Head)
{
	memcpy(m_WorkRcp + 100, Head.sLabel,  8);
	memcpy(m_WorkRcp + 102, Head.sDesc,  16);
	}

// Table Imaging

PBYTE CProxyHoneywell::MakeImageSEQ(UINT &uSize, CSEQ *pSEQ, WORD wCount, WORD wNumber)
{
	UINT      uSegs = 64 * 16;

	UINT      uNeed = sizeof(CHeadSEQ) + uSegs;

	PBYTE     pData = New BYTE [ uNeed ];

	CHeadSEQ *pHead = (CHeadSEQ *) pData;

	PBYTE     pSegs = PBYTE(pHead + 1);

	MakeHeadSEQ(*pHead, wCount, wNumber);

	memcpy     (pSegs, pSEQ->m_bSeg, uSegs);

	uSize = uNeed;

	return pData;
	}

PBYTE CProxyHoneywell::MakeImageSPS(UINT &uSize, CSPS *pSPS, WORD wCount, WORD wNumber)
{
	UINT      uSegs = 50 * 84;

	UINT      uNeed = sizeof(CHeadSPS) + uSegs;

	PBYTE     pData = New BYTE [ uNeed ];

	CHeadSPS *pHead = (CHeadSPS *) pData;

	PBYTE     pSegs = PBYTE(pHead + 1);

	MakeHeadSPS(*pHead, wCount, wNumber);

	memcpy     (pSegs, pSPS->m_bSeg, uSegs);

	uSize = uNeed;

	return pData;
	}

PBYTE CProxyHoneywell::MakeImageSPP(UINT &uSize, CSPP *pSPP, WORD wCount, WORD wNumber)
{
	UINT      uSegs = 50 * 16;

	UINT      uNeed = sizeof(CHeadSPP) + uSegs;

	PBYTE     pData = New BYTE [ uNeed ];

	CHeadSPP *pHead = (CHeadSPP *) pData;

	PBYTE     pSegs = PBYTE(pHead + 1);

	MakeHeadSPP(*pHead, wCount, wNumber);

	memcpy     (pSegs, pSPP->m_bSeg, uSegs);

	uSize = uNeed;

	return pData;
	}

PBYTE CProxyHoneywell::MakeImageRcp(UINT &uSize, WORD wNumber)
{
	UINT      uSegs = 50 * 8;

	UINT      uNeed = sizeof(CHeadRcp) + uSegs;

	PBYTE     pData = New BYTE [ uNeed ];

	CHeadRcp *pHead = (CHeadRcp *) pData;

	PBYTE     pSegs = PBYTE(pHead + 1);

	MakeHeadRcp(*pHead, wNumber);

	for( UINT i = 0; i < 50; i++ ) {
			
		CDataRcp Data;

		Data.VarNum = m_WorkRcp[0*50+i];

		Data.Dummy  = 0;

		Data.Value  = m_WorkRcp[1*50+i];

		memcpy(pSegs + i * 8, &Data, 8);
		}

	uSize = uNeed;

	return pData;
	}

// File Handling

UINT CProxyHoneywell::SaveSEQ(PCTXT pName)
{
	CSEQ *pSEQ = FindWorkSEQ();

	if( pSEQ ) {

		CFile File;

		if( File.Open(pName, TRUE) ) {

			File.Delete();

			File.Close();
			}

		if( File.Create(pName) ) {

			File.Write(m_FileHead, sizeof(m_FileHead));

			UINT  uSize = 0;

			PBYTE pData = MakeImageSEQ(uSize, pSEQ, 1, 1);

			File.Write(pData, uSize);

			delete [] pData;

			File.Close();

			return 1;
			}

		return errorCannotOpen;
		}

	return errorNoWorkspace;
	}

UINT CProxyHoneywell::LoadSEQ(PCTXT pName)
{
	CSEQ *pSEQ = FindWorkSEQ();

	if( pSEQ ) {

		CFile File;

		if( File.Open(pName, FALSE) ) {

			BYTE FileHead[16];

			File.Read(FileHead, sizeof(FileHead));

			if( IsValidRecipe(FileHead) ) {

				CHeadSEQ Head;

				memset(pSEQ->m_bSeg, 0, sizeof(pSEQ->m_bSeg));

				File.Read(PBYTE(&Head), sizeof(Head));

				File.Read(PBYTE(pSEQ->m_bSeg), sizeof(pSEQ->m_bSeg));

				ReadHeadSEQ(Head);

				File.Close();

				return 1;
				}

			File.Close();

			return errorFileHeader;
			}

		return errorCannotOpen;
		}

	return errorNoWorkspace;
	}

UINT CProxyHoneywell::SaveSPS(PCTXT pName)
{
	CSPS *pSPS = FindWorkSPS();

	if( pSPS ) {

		CFile File;

		if( File.Open(pName, TRUE) ) {

			File.Delete();

			File.Close();
			}

		if( File.Create(pName) ) {

			File.Write(m_FileHead, sizeof(m_FileHead));

			UINT  uSize = 0;

			PBYTE pData = MakeImageSPS(uSize, pSPS, 1, 1);

			File.Write(pData, uSize);

			delete [] pData;

			return 1;
			}

		return errorCannotOpen;
		}

	return errorNoWorkspace;
	}

UINT CProxyHoneywell::LoadSPS(PCTXT pName)
{
	CSPS *pSPS = FindWorkSPS();

	if( pSPS ) {

		CFile File;

		if( File.Open(pName, FALSE) ) {

			BYTE FileHead[16];

			File.Read(FileHead, sizeof(FileHead));

			if( IsValidRecipe(FileHead) ) {

				CHeadSPS Head;

				memset(pSPS->m_bSeg, 0, sizeof(pSPS->m_bSeg));

				File.Read(PBYTE(&Head), sizeof(Head));

				File.Read(PBYTE(pSPS->m_bSeg), sizeof(pSPS->m_bSeg));

				ReadHeadSPS(Head);

				File.Close();

				return 1;
				}

			File.Close();

			return errorFileHeader;
			}

		return errorCannotOpen;
		}

	return errorNoWorkspace;
	}

UINT CProxyHoneywell::SaveSPP(PCTXT pName)
{
	CSPP *pSPP = FindWorkSPP();

	if( pSPP ) {

		CFile File;

		if( File.Open(pName, TRUE) ) {

			File.Delete();

			File.Close();
			}

		if( File.Create(pName) ) {

			File.Write(m_FileHead, sizeof(m_FileHead));

			UINT  uSize = 0;

			PBYTE pData = MakeImageSPP(uSize, pSPP, 1, 1);

			File.Write(pData, uSize);

			delete [] pData;

			return 1;
			}

		return errorCannotOpen;
		}

	return errorNoWorkspace;
	}

UINT CProxyHoneywell::LoadSPP(PCTXT pName)
{
	CSPP *pSPP = FindWorkSPP();

	if( pSPP ) {

		CFile File;

		if( File.Open(pName, FALSE) ) {

			BYTE FileHead[16];

			File.Read(FileHead, sizeof(FileHead));

			if( IsValidRecipe(FileHead) ) {

				CHeadSPP Head;

				memset(pSPP->m_bSeg, 0, sizeof(pSPP->m_bSeg));

				File.Read(PBYTE(&Head), sizeof(Head));

				File.Read(PBYTE(pSPP->m_bSeg), sizeof(pSPP->m_bSeg));
				
				ReadHeadSPP(Head);

				File.Close();

				return 1;
				}
			
			File.Close();

			return errorFileHeader;
			}

		return errorCannotOpen;
		}

	return errorNoWorkspace;
	}

UINT CProxyHoneywell::SaveRcp(PCTXT pName)
{
	CFile File;

	if( File.Open(pName, TRUE) ) {

		File.Delete();

		File.Close();
		}

	if( File.Create(pName) ) {

		File.Write(m_FileHead, sizeof(m_FileHead));

		UINT  uSize = 0;

		PBYTE pData = MakeImageRcp(uSize, 1);

		File.Write(pData, uSize);

		delete [] pData;

		File.Close();

		return 1;
		}

	return errorCannotOpen;
	}

UINT CProxyHoneywell::LoadRcp(PCTXT pName)
{
	CFile File;

	if( File.Open(pName, FALSE) ) {

		BYTE FileHead[16];

		File.Read(FileHead, sizeof(FileHead));

		if( IsValidRecipe(FileHead) ) {

			CHeadRcp Head;

			memset(m_WorkRcp, 0, sizeof(m_WorkRcp));

			File.Read(PBYTE(&Head), sizeof(Head));

			for( UINT i = 0; i < 50; i++ ) {
					
				CDataRcp Data;

				if( File.Read(PBYTE(&Data), sizeof(Data)) ) {

					m_WorkRcp[0*50+i] = Data.VarNum;
						
					m_WorkRcp[1*50+i] = Data.Value;
					}
				}

			ReadHeadRcp(Head);

			File.Close();

			return 1;
			}

		File.Close();

		return errorFileHeader;
		}

	return errorCannotOpen;
	}

// CDE Transfer

BOOL CProxyHoneywell::UploadCDE(PCTXT pName)
{
	CFile File;

	if( File.Open(pName, TRUE) ) {

		File.Delete();

		File.Close();
		}

	if( File.Create(pName) ) {

		if( Invalidate(FALSE) ) {

			if( m_uFrom == stateOnline ) {

				m_uFrom = stateGetAlarms;
				}
			}

		File.Write(m_FileHead, sizeof(m_FileHead));

		BYTE n = 0;

		BYTE s = m_FileHead[2];

		UINT c = GetTableCount(s);

		if( c ) {

			for( UINT i = 0; i < c; i++ ) {

				CTab Tab;

				memset(&Tab, 0, sizeof(Tab));

				Tab.m_bTable = GetTableOrder(s, i);

				SetTable(Tab.m_bTable);

				if( !m_uCDE ) {

					File.Delete();

					SetResult(errorAbort);

					return TRUE;
					}

				if( ReadTable(Tab, TRUE) ) {

					if( Tab.m_fValid ) {

						File.Write(Tab.m_pHead, Tab.m_uAlloc);

						n++;
						}

					FreeTable(Tab);

					SetProgress(i, c);

					continue;
					}

				File.Delete();

				SetResult(errorCommsFailure);

				return FALSE;
				}

			File.Seek (4, fsmBegin);

			File.Write(&n, 1);

			File.Close();

			SetProgress(i, c);

			SetResult(1);

			return TRUE;
			}

		File.Delete();

		SetResult(errorBadSchema);

		return FALSE;
		}

	SetResult(errorCannotOpen);

	return TRUE;
	}

BOOL CProxyHoneywell::DownloadCDE(PCTXT pName)
{
	CFile File;

	if( File.Open(pName, FALSE) ) {

		BYTE FileHead[16];

		File.Read(FileHead, sizeof(FileHead));

		if( IsValidHead(FileHead) ) {

			DWORD State;

			if( ReadFunc03(2, 1, 22, State) ) {

				if( State == 0 || State == 3 || State == 4 ) {

					SetResult(errorBadMode);

					return TRUE;
					}

				if( !PerformClear() ) {

					return FALSE;
					}

				if( Invalidate(FALSE) ) {

					if( m_uFrom == stateOnline ) {

						m_uFrom = stateGetAlarms;
						}
					}

				SetPhase(3);

				UINT c = FileHead[4];

				for( UINT t = 0; t < c; t++ ) {

					CHeadTab Head;

					if( !m_uCDE ) {

						SetResult(errorAbort);

						return TRUE;
						}

					if( File.Read(PBYTE(&Head), sizeof(Head)) ) {

						WORD  wTable = MotorToHost(Head.Number);

						UINT  uCount = MotorToHost(Head.Count);

						UINT  uSize  = MotorToHost(Head.Size);

						UINT  uAlloc = uCount * uSize + 16;

						SetTable(wTable);

						File.Seek(-16, fsmCurrent);

						if( !SendTable(wTable, 0, 0xFFFF, File, uAlloc) ) {

							SetResult(errorCommsFailure);

							return FALSE;
							}

						SetProgress(t, c);

						continue;
						}

					break;
					}

				SetProgress(t, c);

				return PerformValidate();
				}

			SetResult(errorCommsFailure);

			return FALSE;
			}

		SetResult(errorFileHeader);

		return TRUE;
		}

	SetResult(errorCannotOpen);

	return TRUE;
	}

BOOL CProxyHoneywell::PerformClear(void)
{
	SetPhase(2);

	if( SendClear() ) {

		for( UINT n = 0; n < 120; n++ ) {

			BOOL fDone;

			if( TestClear(fDone) ) {

				if( fDone ) {

					return TRUE;
					}
				}

			if( !m_uCDE ) {

				SetResult(errorAbort);

				return TRUE;
				}

			Sleep(500);
			}

		SetResult(errorClearFailed);

		return FALSE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::SendClear(void)
{
	StartWrite(8);

	if( Transact(FALSE) ) {

		if( m_pRxData[16] == 0x0A ) {

			FreeRx();

			return TRUE;
			}

		SetResult(errorClearBusy);

		FreeRx();

		return FALSE;
		}

	SetResult(errorCommsFailure);

	return FALSE;
	}

BOOL CProxyHoneywell::TestClear(BOOL &fDone)
{
	StartRead(8);

	if( Transact(TRUE) ) {

		if( m_pRxData[16] == 0x0A ) {

			fDone = !m_pRxData[18];

			FreeRx();

			return TRUE;
			}
		}

	fDone = FALSE;

	return FALSE;
	}

BOOL CProxyHoneywell::PerformValidate(void)
{
	SetPhase(4);

	if( SendValidate() ) {

		for( UINT n = 0; n < 120; n++ ) {

			if( TestValidate() ) {

				return TRUE;
				}

			if( !m_uCDE ) {

				SetResult(errorAbort);

				return TRUE;
				}

			Sleep(1000);
			}

		SetResult(errorValidateFailed);

		return FALSE;
		}

	SetResult(errorCommsFailure);

	return FALSE;
	}

BOOL CProxyHoneywell::SendValidate(void)
{
	StartWrite(24);

	if( Transact(TRUE) ) {

		memcpy(m_bSendID, m_pRxData+18, 4);

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::TestValidate(void)
{
	StartRead(24);

	AddByte(0);

	if( Transact(TRUE) ) {

		if( m_pRxData[18] ) {

			if( m_pRxData[18] == 4 ) {

				m_uWaitTime = 10 * m_pRxData[19] + m_pRxData[20];
				}
			else
				m_uWaitTime = 0;

			SetResult(m_pRxData[18]);

			FreeRx();

			return TRUE;
			}

		FreeRx();
		}

	return FALSE;
	}

BOOL CProxyHoneywell::RestartCDE(char cMode)
{
	BYTE bCode = 0;

	switch( toupper(cMode) ) {

		case 'H':
			bCode = 1;
			break;

		case 'C':
			bCode = 2;
			break;

		case 'P':
			bCode = 3;
			break;
		}

	if( bCode ) {

		if( SendRestart(bCode) ) {

			if( bCode < 3 ) {

				for( UINT n = 0; n < 90; n++ ) {

					if( n >= 5 ) {

						DWORD State;

						if( ReadFunc03(2, 1, 22, State) ) {

							if( State == 2 || State == 4 ) {
								
								SetResult(1);

								return FALSE;
								}
							}
						}

					SetProgress(n, 90);

					Sleep(1000);
					}

				SetResult(errorRestartFailed);

				return FALSE;
				}

			SetResult(1);

			return FALSE;
			}

		return TRUE;
		}
	
	SetResult(errorBadCode);

	return TRUE;
	}

BOOL CProxyHoneywell::SendRestart(BYTE bCode)
{
	StartWrite(25);

	AddByte(bCode);

	AddByte(m_bSendID[0]);
	
	AddByte(m_bSendID[1]);
	
	AddByte(m_bSendID[2]);
	
	AddByte(m_bSendID[3]);

	if( Transact(FALSE) ) {

		if( m_pRxData[16] == 0x0A ) {

			if( !m_pRxData[18] ) {

				FreeRx();

				return TRUE;
				}

			SetResult(errorRestartRejected);

			FreeRx();

			return FALSE;
			}

		SetResult(errorRestartFailed);

		FreeRx();

		return FALSE;
		}

	SetResult(errorCommsFailure);

	return FALSE;
	}

void CProxyHoneywell::SetPhase(UINT uPhase)
{
	m_uPhase = uPhase;

	ServiceDriver();
	}

void CProxyHoneywell::SetProgress(UINT uDone, UINT uCount)
{
	m_uProgress = 100 * uDone / uCount;

	ServiceDriver();
	}

void CProxyHoneywell::SetTable(WORD wTable)
{
	m_uTable = wTable;

	ServiceDriver();
	}

void CProxyHoneywell::SetResult(UINT uCode)
{
	m_uResult = uCode;

	if( INT(uCode) >= 0 ) {

		m_uProgress = uCode ? 100 : 0;

		m_uPhase    = uCode ? 100 : 1;

		m_uTable    = 0;
		}

	ServiceDriver();
	}

UINT CProxyHoneywell::GetTableCount(BYTE s)
{
	if( s == 30 ) {

		return 66;
		}

	if( s == 32 ) {

		return 72;
		}

	return 0;
	}

BYTE CProxyHoneywell::GetTableOrder(BYTE s, UINT n)
{
	if( s == 30 ) {

		static BYTE const b[] = 
		{
			#include "hons30.hpp"
			};

		return b[n];
		}

	if( s == 32 ) {

		static BYTE const b[] = 
		{
			#include "hons32.hpp"
			};

		return b[n];
		}

	return 0;
	}

// Alarms and Events

void CProxyHoneywell::InitAlarmInfo(void)
{
	if( m_fOldAlarms && m_fNet ) {

		m_pAlarms  = NULL;

		m_pEvents  = NULL;

		m_Source   = 0;
		}
	else {
		m_pAlarms  = New DWORD [ 5 * 360 ];

		m_pEvents  = CCommsSystem::m_pThis->m_pEvents;

		m_Source   = m_pEvents->Register(this, "HC");

		memset(m_pAlarms, 0x00, 5 * 4 * 360);
		}

	m_uAlarms  = 0;

	m_uAckHead = 0;

	m_uAckTail = 0;
	}

void CProxyHoneywell::KillAlarmInfo(void)
{
	if( m_pAlarms ) {

		delete [] m_pAlarms;
		}
	}

void CProxyHoneywell::WipeAlarmInfo(void)
{
	if( m_pAlarms ) {

		if( m_fAlarms ) {

			for( UINT uIndex = 0; uIndex < m_uAlarms; uIndex++ ) {

				if( m_pAlarms[uIndex] ) {

					UINT           Code  = MakeAlarmCode(uIndex);

					CActiveAlarm * pInfo = m_pEvents->FindAlarm(m_Source, Code);

					if( pInfo ) {

						m_pEvents->EditAlarm( pInfo,
								      alarmIdle
								      );
						}
					}
				}
			}

		memset(m_pAlarms, 0, 4 * 5 * 360);
		}
	}

BOOL CProxyHoneywell::ScanAlarmInfo(UINT b, CCommsSysBlock *pBlock, CAddress const &Addr)
{
	if( Addr.a.m_Extra == 0 ) {
		
		if( Addr.a.m_Table == 20 ) {

			// REV3 -- Ought to be a way to map large
			// amouns of data like this without having
			// to worry about the request flags.

			WORD p = Addr.a.m_Offset;

			for( INT i = 0; i < pBlock->m_Size; i++ ) {

				if( pBlock->ShouldRead(i) ) {

					if( m_pAlarms ) {

						pBlock->SetCommsData(i, m_pAlarms[p]);
						}
					else
						pBlock->SetCommsData(i, 0);
					}

				p++;
				}
			}
		}

	return TRUE;
	}

void CProxyHoneywell::FindAlarmCount(void)
{
	for( UINT uGroup = m_TabAlarms.m_uCount; uGroup; uGroup-- ) {

		for( UINT uAlarm = 12; uAlarm; uAlarm-- ) {

			PWORD pLook = PWORD(m_TabAlarms.m_pData + 72 * (uGroup-1) + 24);

			if( pLook[uAlarm-1] ) {

				m_uAlarms = 12 * (uGroup-1) + uAlarm;

				uGroup    = 1;

				uAlarm    = 1;
				}
			}
		}
	}

BOOL CProxyHoneywell::ReadAlarmInfo(void)
{
	for( UINT uAlarm = 0; uAlarm < m_uAlarms; uAlarm++ ) {

		if( !ReadAlarmInfo(uAlarm) ) {

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadAlarmInfo(UINT uIndex)
{
	if( uIndex < m_uAlarms ) {

		StartRead(9);

		if( m_fOldAlarms ) {

			AddByte(BYTE(uIndex+1));
			}
		else {
			AddByte(HIBYTE(uIndex+1));

			AddByte(LOBYTE(uIndex+1));
			}

		if( Transact(FALSE) ) {

			if( m_pRxData[16] == 0x0A ) {

				UINT uPos = m_fOldAlarms ? 20 : 21;

				TrackAlarm(uIndex, m_pRxData[uPos]);

				m_pAlarms[1 * 360 + uIndex] = IsAlarmManualAck(uIndex);

				m_pAlarms[2 * 360 + uIndex] = TimeAdjust(MotorToHost(PDWORD(m_pRxData+uPos+1)[0]));
				
				m_pAlarms[3 * 360 + uIndex] = TimeAdjust(MotorToHost(PDWORD(m_pRxData+uPos+1)[1]));

				m_pAlarms[4 * 360 + uIndex] = 0;
				}
			else {
				TrackAlarm(uIndex, 0);

				m_pAlarms[1 * 360 + uIndex] = 0;
				
				m_pAlarms[2 * 360 + uIndex] = 0;
				
				m_pAlarms[3 * 360 + uIndex] = 0;

				m_pAlarms[4 * 360 + uIndex] = 0;
				}

			FreeRx();

			DWORD &Data = m_pAlarms[4 * 360 + uIndex];

			UINT  uBase = m_fOldAlarms ? 240 : 360;

			return ReadFunc03(2, 6, uBase + uIndex, Data);
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CProxyHoneywell::SendAlarmAccepts(void)
{
	while( m_uAckHead - m_uAckTail ) {

		UINT Code   = m_uAck[m_uAckHead];

		UINT uGroup = HIWORD(Code);

		UINT uMask  = LOWORD(Code);

		m_uAckHead  = (m_uAckHead + 1) % elements(m_uAck);

		if( uMask ) {

			StartWrite(6);

			AddByte(0);
			
			AddWord(1 + uGroup);

			AddWord(uMask);

			if( Transact(FALSE) ) {

				if( m_pRxData[16] == 0x0A ) {

					FreeRx();

					for( UINT n = 0; n < 12; n++ ) {

						if( uMask & (1<<n) ) {

							UINT uIndex = 12 * uGroup + n;

							BYTE bState = m_pAlarms[uIndex];

							if( bState == 3 || bState == 4 || bState == 7 ) {

								UINT Code = MakeAlarmCode(uIndex);

								m_pEvents->LogEvent( 0,
										     m_Source,
										     eventAccept,
										     Code
										     );

								if( !TrackAlarm(uIndex) ) {

									return FALSE;
									}
								}
							}
						}

					continue;
					}

				FreeRx();

				continue;
				}

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CProxyHoneywell::ReadVolatileAlarms(void)
{
	PUINT pList = New UINT [ 60 ];

	UINT  uList = 0;

	for( UINT uIndex = 0; uIndex < m_uAlarms; uIndex++  ) {

		if( m_pAlarms[uIndex] > 1 ) {

			pList[uList++] = uIndex;
			}

		if( uList == 60 || uIndex == m_uAlarms - 1 ) {

			if( uList ) {

				StartRead(3);

				AddByte(uList);
				
				AddByte(0);

				AddByte(2);

				for( UINT n = 0; n < uList; n++ ) {

					AddWord(6);

					AddWord(pList[n]);
					}

				if( Transact(TRUE) ) {

					PDWORD d = PDWORD(m_pRxData+19);

					for( UINT n = 0; n < uList; n++ ) {

						UINT uIndex = pList[n];

						BYTE bState = MotorToHost(d[n]);

						TrackAlarm(uIndex, bState);
						}

					FreeRx();

					uList = 0;

					continue;
					}

				delete [] pList;

				return FALSE;
				}
			}
		}

	delete [] pList;

	return TRUE;
	}

BOOL CProxyHoneywell::TrackAlarm(UINT uIndex)
{
	StartRead(3);

	AddByte(1);
	
	AddByte(0);

	AddByte(2);

	AddWord(6);

	AddWord(uIndex);

	if( Transact(TRUE) ) {

		BYTE bState = MotorToHost(*PDWORD(m_pRxData+19));

		TrackAlarm(uIndex, bState);

		FreeRx();

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::TrackAlarm(UINT uIndex, BYTE bState)
{
	if( m_pAlarms[uIndex] ^ bState ) {

		if( m_fAlarms ) {

			UINT           Code  = MakeAlarmCode(uIndex);

			CActiveAlarm * pInfo = m_pEvents->FindAlarm(m_Source, Code);

			if( !pInfo ) {

				if( bState ) {

					pInfo = m_pEvents->MakeAlarm(m_Source, Code);

					if( pInfo ) {

						pInfo->m_Time = CTimeSync::m_pThis->GetLogTime();

						if( bState == 3 || bState == 7 ) {

							m_pEvents->EditAlarm( pInfo,
									      alarmActive
									      );
							}
						}
					}
				}

			if( pInfo ) {

				if( bState == 0 ) {

					m_pEvents->EditAlarm( pInfo,
							      alarmIdle
							      );
					}

				if( bState == 1 ) {

					m_pEvents->EditAlarm( pInfo,
							      alarmAccepted
							      );
					}

				if( bState == 4 ) {

					m_pEvents->EditAlarm( pInfo,
							      alarmWaitAccept
							      );
					}
				}
			}

		m_pAlarms[uIndex] = bState;

		return TRUE;
		}

	return FALSE;
	}

BOOL CProxyHoneywell::ReadHistory(BYTE bTab, BOOL fInit)
{
	PBYTE pNew = New BYTE [ m_uAlarms ];

	UINT  uSeq = 1;

	memset(pNew, 0, m_uAlarms);

	for(;;) {

		if( m_fNet && m_FileHead[6] > 3 ) {

			StartRead(51);

			AddWord(uSeq);

			AddByte(bTab);

			AddByte(m_HMI);
			}
		else {
			StartRead(10);

			AddWord(uSeq);

			AddByte(bTab);
			}

		if( Transact(TRUE) ) {

			UINT  uCopy = m_pRxBuff->GetSize();

			PBYTE pCopy = New BYTE [ uCopy ];

			memcpy(pCopy, m_pRxData, uCopy);
			
			FreeRx();

			BYTE  bLen = pCopy[24];

			PBYTE pRec = pCopy+25;

			UINT  uLen = m_fOldAlarms ? 6 : 7;

			while( bLen >= uLen ) {

				BYTE  bState = pRec[0];

				UINT  uIndex = 0;

				DWORD dwTime = 0;

				if( m_fOldAlarms ) {

					uIndex = pRec[1] - 1;

					dwTime = MotorToHost(PDWORD(pRec + 2)[0]);
					}
				else {
					uIndex = MAKEWORD(pRec[2], pRec[1]) - 1;

					dwTime = MotorToHost(PDWORD(pRec + 3)[0]);
					}

				if( bTab == 0 ) {

					if( uIndex < m_uAlarms ) {

						TakeAlarm(uIndex, bState, TimeAdjust(dwTime));

						pNew[uIndex] = TRUE;
						}
					}

				if( bTab == 1 ) {

					TakeEvent(uIndex, bState, TimeAdjust(dwTime));
					}

				pRec += uLen;

				bLen -= uLen;
				}

			if( MAKEWORD(pCopy[18], pCopy[19]) < 0xFFFE ) {

				delete [] pCopy;

				uSeq += 1;

				continue;
				}

			delete [] pCopy;

			break;
			}
		
		delete [] pNew;

		return FALSE;
		}

	if( m_fNet && m_FileHead[6] > 3 ) {

		StartWrite(51);

		AddByte(bTab);
		
		AddByte(m_HMI);
		}
	else {
		StartWrite(10);

		AddByte(bTab);
		}

	if( Transact(TRUE) ) {

		FreeRx();

		if( !fInit ) {

			if( bTab == 0 ) {

				for( UINT n = 0; n < m_uAlarms; n++ ) {

					if( pNew[n] ) {

						if( !ReadAlarmInfo(n) ) {

							delete [] pNew;

							return FALSE;
							}
						}
					}
				}
			}

		delete [] pNew;

		return TRUE;
		}
	
	delete [] pNew;

	return FALSE;
	}

BOOL CProxyHoneywell::TakeAlarm(UINT uIndex, BYTE bState, DWORD dwTime)
{
	UINT Code  = MakeAlarmCode(uIndex);

	BYTE bMode = FindAlarmMode(uIndex);

	if( bState == !(bMode & 0x10) ) {

		if( m_fAlarms ) {

			CActiveAlarm *pInfo = m_pEvents->FindAlarm(m_Source, Code);

			if( !pInfo ) {
				
				pInfo = m_pEvents->MakeAlarm(m_Source, Code);

				if( pInfo ) {

					pInfo->m_Time = 5*dwTime;

					m_pEvents->EditAlarm( pInfo,
							      alarmActive
							      );
					}
				}
			}

		m_pEvents->LogEvent( 5*dwTime,
				     m_Source,
				     eventAlarm,
				     Code
				     );
		}
	else {
		if( m_fAlarms ) {

			TrackAlarm(uIndex);
			}

		m_pEvents->LogEvent( 5*dwTime,
				     m_Source,
				     eventClear,
				     Code
				     );
		}

	return TRUE;
	}

BOOL CProxyHoneywell::TakeEvent(UINT uIndex, BYTE bState, DWORD dwTime)
{
	PWORD pLook = PWORD(m_TabEvents.m_pData + 24);

	UINT  uSig  = MotorToHost(pLook[uIndex]);

	BYTE  bMode = FindSigMode(uSig);

	if( bState == !(bMode & 0x10) ) {

		m_pEvents->LogEvent( 5*dwTime,
				     m_Source,
				     eventEvent,
				     uSig
				     );

		return TRUE;
		}

	return FALSE;
	}

DWORD CProxyHoneywell::MakeAlarmCode(UINT uIndex)
{
	UINT  uGroup = uIndex / 12;

	UINT  uAlarm = uIndex % 12;

	PWORD pLook  = PWORD(m_TabAlarms.m_pData + 72 * uGroup + 24);

	UINT  uSig   = MotorToHost(pLook[uAlarm]);

	return MAKELONG(uSig, 1 + uIndex);
	}

BYTE CProxyHoneywell::FindAlarmMode(UINT uIndex)
{
	UINT  uGroup = uIndex / 12;

	UINT  uAlarm = uIndex % 12;

	PWORD pLook  = PWORD(m_TabAlarms.m_pData + 72 * uGroup + 24);

	UINT  uSig   = MotorToHost(pLook[uAlarm]);

	return FindSigMode(uSig);
	}

BOOL CProxyHoneywell::IsAlarmManualAck(UINT uIndex)
{
	return !(FindAlarmMode(uIndex) & 0x20);
	}

DWORD CProxyHoneywell::TimeAdjust(DWORD dwTime)
{
	return dwTime ? dwTime - TIME_ADJUST : 0;
	}

BOOL CProxyHoneywell::ReadMismatchMode(void)
{
	DWORD dwMode;

	if( ReadFunc03(2, 1, 22, dwMode) ) {

		m_MisMode = dwMode & 0xFF;
		
		return TRUE;
		}

	return FALSE;
	}

#endif

// End of File
