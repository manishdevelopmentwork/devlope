
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHidReport_HPP

#define	INCLUDE_UsbHidReport_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Framework
//

#include "Hid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hid Report
//

class CUsbHidReport
{
	public:
		// Constructor
		CUsbHidReport(void);
		CUsbHidReport(PCBYTE pData, UINT uSize);
		
		// Operations
		void Attach(PCBYTE pData, UINT uSize);

		// Enumerators
		UINT GetFirst(void) const;
		bool GetNext(UINT &iIndex) const;

		// Operators
		HidItem const * operator[] (UINT iIndex) const;

		// Failure Checks
		bool Failed(UINT iIndex) const;
		
		// Debug
		void Debug(void);

	private:
		// Data
		PCBYTE m_pData;
		UINT   m_uSize;
	};

// End of File

#endif
