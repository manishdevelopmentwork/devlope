
#include "Intern.hpp"

#include "PseudoHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpHeader.hpp"

////////////////////////////////////////////////////////////////////////
//
// Pseudo Header Wrapper Class
//

// Constructors

CPsHeader::CPsHeader(void)
{
	}

CPsHeader::CPsHeader(CIpHeader *pIp)
{
	MakeHeader(pIp);
	}

// Attributes

DWORD CPsHeader::GetChecksum(void) const
{
	return BasicSum(PBYTE(this), sizeof(CPsHeader));
	}

WORD  CPsHeader::GetPrimeSum(void) const
{
	return Checksum(PBYTE(this), sizeof(CPsHeader));
	}

// Operations

void CPsHeader::MakeHeader(CIpHeader *pIp)
{
	m_Src	   = pIp->m_IpSrc;

	m_Dest	   = pIp->m_IpDest;

	m_Zero	   = 0;

	m_Protocol = pIp->m_Protocol;

	m_Length   = HostToNet(pIp->GetDataLength());
	}

// End of File
