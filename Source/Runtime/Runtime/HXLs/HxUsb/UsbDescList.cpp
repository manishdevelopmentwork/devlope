
#include "Intern.hpp"

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

#include "UsbDeviceDesc.hpp"

#include "UsbConfigDesc.hpp"

#include "UsbEndpointDesc.hpp"

#include "UsbInterfaceDesc.hpp"

#include "UsbDeviceQual.hpp"

#include "UsbHubDesc.hpp"

#include "UsbHidDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Descriptor
//

// Constructor

CUsbDescList::CUsbDescList(void)
{
	m_pData  = NULL;

	m_uSize  = 0;

	m_fOwner = false;
	}

// Destructor

CUsbDescList::~CUsbDescList(void)
{
	Detach();	
	}

// Endianess

void CUsbDescList::HostToUsb(void)
{
	for( UINT i = 0; i < m_uSize; ) {

		UsbDesc &Desc = (UsbDesc &) m_pData[i];

		switch( Desc.m_bType ) {

			case descDevice:
				
				((CUsbDeviceDesc &) Desc).HostToUsb();
				
				break;

			case descConfig:

				((CUsbConfigDesc &) Desc).HostToUsb();

				break;

			case descInterface:

				((CUsbInterfaceDesc &) Desc).HostToUsb();

				break;

			case descEndpoint:

				((CUsbEndpointDesc &) Desc).HostToUsb();

				break;

			case descDeviceQual:

				((CUsbDeviceQual &) Desc).HostToUsb();

				break;
			}

		i += Desc.m_bLength;
		}
	}

void CUsbDescList::UsbToHost(void)
{
	for( UINT i = 0; i < m_uSize; ) {

		UsbDesc &Desc = (UsbDesc &) m_pData[i];

		switch( Desc.m_bType ) {

			case descDevice:
				
				((CUsbDeviceDesc &) Desc).UsbToHost();
				
				break;

			case descConfig:

				((CUsbConfigDesc &) Desc).UsbToHost();

				break;

			case descInterface:

				((CUsbInterfaceDesc &) Desc).UsbToHost();

				break;

			case descEndpoint:

				((CUsbEndpointDesc &) Desc).UsbToHost();

				break;

			case descDeviceQual:

				((CUsbDeviceQual &) Desc).UsbToHost();

				break;
			}

		i += Desc.m_bLength;
		}
	}

// Attributes

BOOL CUsbDescList::IsValid(void) const
{
	for( UINT i = 0; i < m_uSize; ) {

		UsbDesc &Desc = (UsbDesc &) m_pData[i];

		switch( Desc.m_bType ) {

			case descDevice:
				
				if( !((CUsbDeviceDesc &) Desc).IsValid() ) {

					return false;
					}
				break;

			case descConfig:

				if( !((CUsbConfigDesc &) Desc).IsValid() ) {

					return false;
					}
				break;

			case descInterface:

				if( !((CUsbInterfaceDesc &) Desc).IsValid() ) {

					return false;
					}
				break;

			case descEndpoint:

				if( !((CUsbEndpointDesc &) Desc).IsValid() ) {

					return false;
					}
				break;

			case descDeviceQual:

				if( !((CUsbDeviceQual &) Desc).IsValid() ) {

					return false;
					}
				break;

			case descHid:

				// Accept as valid to avoid debug messages.

				break;

			default: 
				
				/*AfxTrace("WARNING - Unknown Descriptor Type 0x%2.2x\n", Desc.m_bType);*/

				break;
			}

		i += Desc.m_bLength;
		}
	
	return true;
	}

// Operations

void CUsbDescList::Attach(PBYTE pData, UINT uSize)
{
	Detach();

	m_pData  = pData;

	m_uSize  = uSize;

	m_fOwner = false;
	}

void CUsbDescList::Take(PBYTE pData, UINT uSize)
{
	Detach();

	m_pData  = pData;

	m_uSize  = uSize;

	m_fOwner = true;
	}

void CUsbDescList::Detach(void)
{
	if( m_pData ) {	

		if( m_fOwner ) {
		
			free(m_pData);
			}
		
		m_pData = NULL;

		m_uSize = 0;
		}
	}

UINT CUsbDescList::GetIndexStart(void) const
{
	return 0;
	}

UINT CUsbDescList::FindIndex(PCBYTE pData) const
{
	return pData - m_pData;
	}

BOOL CUsbDescList::IsIndexValid(UINT iIndex) const
{
	return iIndex < m_uSize;
	}

// Enumerations

UsbDesc * CUsbDescList::Enum(UINT uType, UINT& iIndex) const
{
	while( iIndex < m_uSize ) {

		UsbDesc &Desc = (UsbDesc &) m_pData[iIndex];

		iIndex += Desc.m_bLength;

		if( Desc.m_bType == uType ) {

			return &Desc;
			}
		}

	return NULL;
	}

CUsbConfigDesc * CUsbDescList::EnumConfig(UINT &iIndex) const
{
	return (CUsbConfigDesc *) Enum(descConfig, iIndex);
	}

CUsbInterfaceDesc * CUsbDescList::EnumInterface(UINT &iIndex) const
{
	return (CUsbInterfaceDesc *) Enum(descInterface, iIndex);
	}

CUsbInterfaceDesc * CUsbDescList::EnumInterface(UINT &iIndex, UINT uAltSetting) const
{
	for(;;) {
	
		CUsbInterfaceDesc *p = (CUsbInterfaceDesc *) Enum(descInterface, iIndex);

		if( p ) {

			if( p->m_bAltSetting == uAltSetting ) {

				return p;
				}

			continue;
			}

		break;
		}
	
	return NULL;
	}

CUsbEndpointDesc * CUsbDescList::EnumEndpoint(UINT &iIndex) const
{
	return (CUsbEndpointDesc *) Enum(descEndpoint, iIndex);
	}

CUsbConfigDesc * CUsbDescList::FindConfig(UINT &iIndex) const
{
	iIndex = GetIndexStart();

	return EnumConfig(iIndex);
	}

CUsbInterfaceDesc * CUsbDescList::FindInterface(UINT &iIndex, UINT iIdent) const
{
	iIndex = GetIndexStart();

	for(;;) {

		CUsbInterfaceDesc *p = EnumInterface(iIndex);

		if( p ) {

			if( p->m_bThis == iIdent ) {

				return p;
				}

			continue;
			}

		break;
		}

	return NULL;
	}

CUsbInterfaceDesc * CUsbDescList::FindInterface(UINT &iIndex, UINT iIdent, UINT iAltSetting) const
{
	iIndex = GetIndexStart();

	for(;;) {

		CUsbInterfaceDesc *p = EnumInterface(iIndex, iAltSetting);

		if( p ) {

			if( p->m_bThis == iIdent ) {

				return p;
				}

			continue;
			}

		break;
		}

	return NULL;
	}

// Debug

void CUsbDescList::Debug(void)
{
	for( UINT i = 0; i < m_uSize; ) {

		UsbDesc &Desc = (UsbDesc &) m_pData[i];

		switch( Desc.m_bType ) {

			case descDevice:
				
				((CUsbDeviceDesc &) Desc).Debug();
				
				break;

			case descConfig:

				((CUsbConfigDesc &) Desc).Debug();

				break;

			case descInterface:

				((CUsbInterfaceDesc &) Desc).Debug();

				break;

			case descEndpoint:

				((CUsbEndpointDesc &) Desc).Debug();

				break;

			case descDeviceQual:

				((CUsbDeviceQual &) Desc).Debug();

				break;

			case descHid:

				// Accept as valid to avoid debug messages.

				break;

			default:

			//	AfxTrace("WARNING - Unknown Descriptor Type 0x%2.2x\n", Desc.m_bType);

			//	AfxDump(m_pData + i, Desc.m_bLength);

				break;
			}

		i += Desc.m_bLength;
		}
	}

// End of File
