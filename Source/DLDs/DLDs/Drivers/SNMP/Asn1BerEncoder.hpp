
//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

#ifndef INCLUDE_SNMP_Asn1BerEncoder_HPP

#define INCLUDE_SNMP_Asn1BerEncoder_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Asn1Base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class COid;

//////////////////////////////////////////////////////////////////////////
//
// ASN.1 BER Encoder
//

class CAsn1BerEncoder : public CAsn1
{
	public:
		// Constructor
		CAsn1BerEncoder(void);

		// Destructor
		~CAsn1BerEncoder(void);

		// Attributes
		bool   IsNull(void) const;
		UINT   GetSize(void) const;
		PCBYTE GetData(void) const;

		// Operations
		void Rewind(UINT uPos);

		// Writing
		UINT AddSequence(void);
		UINT AddConstructed(BYTE bTag);
		void AddEnd(UINT uPos);
		void AddInteger(UINT uData);
		void AddFloat(DWORD Data);
		void AddNetAddress(UINT uData);
		void AddCounter(UINT uData);
		void AddTimeTicks(UINT uData);
		void AddInteger(BYTE bTag, UINT uData);
		void AddOctString(PCSTR pText);
		void AddOctString(PCBYTE pData, UINT uSize);
		bool AddObjectID(COid &Oid);
		void AddVarBindList(COid *pOid);
		void AddNull(void);
		void AddEndOfView(void);
		void AddNoSuchObject(void);

	protected:
		// Data Members
		BYTE m_bData[2048];
		UINT m_uPtr;
	};

// End of File

#endif
