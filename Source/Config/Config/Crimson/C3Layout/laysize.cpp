
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Item for Fixed Size
//

// Constructor

CLayItemSize::CLayItemSize(CSize Size)
{
	m_Size     = Size;

	m_Extra.cx = 4;

	m_Extra.cy = 4;

	m_Grow.cx  = 1;

	m_Grow.cy  = 1;
	}

CLayItemSize::CLayItemSize(CSize Size, int nExtra, int nGrow)
{
	m_Size     = Size;

	m_Extra.cx = nExtra;

	m_Extra.cy = nExtra;

	m_Grow.cx  = nGrow;

	m_Grow.cy  = 1;
	}

CLayItemSize::CLayItemSize(CSize Size, CSize Extra, CSize Grow)
{
	m_Size  = Size;

	m_Extra = Extra;

	m_Grow  = Grow;
	}

// Overidables

void CLayItemSize::OnPrepare(CDC &DC)
{
	m_MinSize  = m_Size + 2 * m_Extra;

	m_MaxSize = m_MinSize * m_Grow;
	}

// End of File
