
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxUsb(void)
{
	piob->RegisterSingleton("usb.func", 0, Create_UsbFuncStack(40000));

	piob->RegisterSingleton("usb.host", 0, Create_UsbHostStack(41000));
	}

void Revoke_HxUsb(void)
{
	piob->RevokeGroup("usb.");
	}

// End of File
