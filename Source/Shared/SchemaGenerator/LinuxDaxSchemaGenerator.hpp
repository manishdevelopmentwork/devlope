
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_LinuxDaxSchemaGenerator_HPP

#define INCLUDE_LinuxDaxSchemaGenerator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SchemaGenerator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DAx Schema Generator
//

class CLinuxDaxSchemaGenerator : public CSchemaGenerator
{
public:
	// Constructors
	CLinuxDaxSchemaGenerator(void);
	CLinuxDaxSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel);

	// Destructor
	~CLinuxDaxSchemaGenerator(void);

protected:
	// Data Members
	IPxeModel * m_pModel;
	CString	    m_EnumFace;
	CString	    m_EnumBridge;
	CString	    m_EnumGps;
	CString	    m_EnumCell;
	CString	    m_EnumTrans;
	UINT	    m_uEth;
	UINT	    m_uVLan;
	UINT	    m_uPpp;
	UINT	    m_uWiFi;
	UINT	    m_uTuns;

	// Overridables
	bool MakeHardwareSchema(void);
	bool MakePersonalitySchema(void);
	bool MakeSystemSchema(void);
	bool MakeUserSchema(void);
	bool AdjustDefault(char cTag, CJsonData *pData);

	// Implementation
	void	    AddHardwareInterfaces(CJsonData *pSchema, CJsonData *pHard);
	void	    AddTunnelInterfaces(CJsonData *pSchema, CJsonData *pHard);
	void        AddBaseEthernetPorts(CJsonData *pList, CJsonData *pHard);
	void        ScanForEthernetSleds(CJsonData *pList, CJsonData *pHard);
	void        ScanForCellularSleds(CJsonData *pList, CJsonData *pHard);
	void        ScanForWiFiSleds(CJsonData *pList, CJsonData *pHard);
	void	    AddTunnels(CJsonData *pList, PCTXT pType, PCTXT pName, PCTXT pDev, UINT uBase, UINT uCount);
	CJsonData * AddInterface(CJsonData *pList, PCTXT pType, PCTXT pName, PCTXT pDev, UINT uByte, UINT uOpts);
	void	    AddNote(CJsonData *pItem, PCTXT pNote);
	UINT	    MakeEthByte(void);
	UINT	    MakeVLanByte(void);
	UINT	    MakeWiFiByte(void);
	void	    AddServices(CJsonData *pSchema);
	void	    AddFirewall(CJsonData *pSchema);
};

// End of File

#endif
