
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConWnd_HPP

#define INCLUDE_DevConWnd_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Referenced Classes
//

class CDevCon;

//////////////////////////////////////////////////////////////////////////
//								
// Device Configuration View Window
//

class CDevConWnd : public CUIViewWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConWnd(void);

	// Destructor
	~CDevConWnd(void);

protected:
	// Data Members
	CDevCon * m_pItem;

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Overridables
	void OnAttach(void);

	// Implementation
	BOOL OnExport(void);
	BOOL OnExtract(void);
	BOOL SaveFile(CFilename const &Name, CString const &Data);
};

// End of File

#endif
