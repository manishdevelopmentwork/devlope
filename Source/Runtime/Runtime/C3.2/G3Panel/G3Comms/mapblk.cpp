
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Serialization
//

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block List
//

// Constructor

CCommsMapBlockList::CCommsMapBlockList(void)
{
	m_uCount  = 0;

	m_ppBlock = NULL;
	}

// Destructor

CCommsMapBlockList::~CCommsMapBlockList(void)
{
	while( m_uCount-- ) {

		delete m_ppBlock[m_uCount];
		}

	delete [] m_ppBlock;
	}

// Initialization

void CCommsMapBlockList::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsMapBlockList", pData);

	if( (m_uCount = GetWord(pData)) ) { 

		m_ppBlock = New CCommsMapBlock * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CCommsMapBlock *pBlock = NULL;

			if( GetByte(pData) ) {

				pBlock = New CCommsMapBlock;
				}

			if( (m_ppBlock[n] = pBlock) ) {

				pBlock->Load(pData);
				}
			}
		}
	}

// Attributes

BOOL CCommsMapBlockList::HasError(void) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			UINT uError = m_ppBlock[n]->GetError();
			
			if( uError == errorNone || uError == errorInit ) {

				continue;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

void CCommsMapBlockList::Bind(CCommsDevice *pDevice)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->Bind(pDevice);
			}
		}
	}

void CCommsMapBlockList::SetError(UINT uError)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppBlock[n] ) {

			m_ppBlock[n]->SetError(uError);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Mapping Block
//

// Constructor

CCommsMapBlock::CCommsMapBlock(void)
{
	m_Addr.m_Ref = 0;
	m_Type       = 0;
	m_Size	     = 0;
	m_Write	     = 0;
	m_Scaled     = 0;
	m_Update     = 0;
	m_Period     = 500;
	m_Slave	     = 0;
	m_pReq       = NULL;
	m_pAck	     = NULL;
	m_pRegs	     = New CCommsMapRegList;
	}

// Destructor

CCommsMapBlock::~CCommsMapBlock(void)
{
	delete m_pReq;
	delete m_pAck;
	delete m_pRegs;
	delete m_pData;
	delete m_pInfo;
	}

// Initialization

void CCommsMapBlock::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsMapBlock", pData);

	m_Addr.m_Ref = GetLong(pData);

	m_Factor     = GetByte(pData);

	m_Write      = GetByte(pData);

	m_Size       = GetWord(pData);

	m_Scaled     = GetByte(pData);

	m_Update     = GetByte(pData);
	
	m_Period     = GetWord(pData);

	m_Slave	     = GetByte(pData);

	GetCoded(pData, m_pReq);

	GetCoded(pData, m_pAck);

	m_pRegs->Load(pData);

	m_uError  = errorInit;

	m_pData   = NULL;
	
	m_pInfo   = NULL;
	
	m_uLast	  = 0;

	m_pDevice = NULL;
	}

// Adress Access

CAddress CCommsMapBlock::GetAddress(INT nPos) const
{
	CAddress Addr;

	if( nPos < m_Size ) {

		Addr             = m_Addr;

		Addr.a.m_Offset += nPos * m_Factor;
		}

	return Addr;
	}

// Attribute

UINT CCommsMapBlock::GetError(void) const
{
	return m_uError;
	}

BOOL CCommsMapBlock::IsActive(void) const
{
	if( m_pReq ) {

		if( m_pReq->Execute(typeInteger) ) {

			if( m_pAck ) {

				if( m_pAck->Execute(typeInteger) ) {

					return FALSE;
					}
				}
			}
		else {
			if( m_pAck ) {

				if( m_pAck->Execute(typeInteger) ) {

					m_pAck->SetValue(0, typeInteger, setDirect);
					}
				}

			return FALSE;
			}
		}

	if( m_Update == 2 ) {

		if( GetTickCount() - m_uLast >= ToTicks(m_Period) ) {

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

DWORD CCommsMapBlock::GetData(INT nPos) const
{
	return m_pData[nPos];
	}

BOOL CCommsMapBlock::ShouldRead(INT nPos) const
{
	CInfo const &Info = m_pInfo[nPos];

	return !Info.m_fLocal;
	}

BOOL CCommsMapBlock::CouldRead(INT nPos) const
{
	return TRUE;
	}

BOOL CCommsMapBlock::ShouldWrite(INT nPos) const
{
	CInfo const &Info = m_pInfo[nPos];

	return Info.m_fDirty;
	}

UINT CCommsMapBlock::GetFlags(void) const
{
	switch( m_Scaled ) {

		case 0:
			return getCommsData;

		case 1:
			return getManipulated;

		case 2:
			return getScaled;
		}

	return getScaled;
	}

// Operations

void CCommsMapBlock::Bind(CCommsDevice *pDevice)
{
	m_pDevice = pDevice;
	}

BOOL CCommsMapBlock::UpdateWriteData(void)
{
	InitWriteData();

	UINT Flags = GetFlags();

	for( INT nPos = 0; nPos < m_Size; nPos++ ) {

		CCommsMapReg *pReg = m_pRegs->m_ppReg[nPos];

		if( pReg->IsMapped() ) {

			if( pReg->IsAvail() ) {

				CInfo &Info = m_pInfo[nPos];

				DWORD  Data = pReg->GetValue(Flags);

				if( m_Update || !Info.m_fValid || m_pData[nPos] != Data ) {

					if( !Info.m_fDirty ) {
						
						Info.m_fDirty = TRUE;

						m_uDirty++;
						}

					m_pData[nPos] = Data;
						
					Info.m_fValid = TRUE;
					}
				}
			}
		}

	return m_uDirty > 0;
	}

BOOL CCommsMapBlock::UpdateReadData(void)
{
	InitReadData();

	IDataServer *pData = CCommsSystem::m_pThis->GetDataServer();

	UINT         Flags = GetFlags();

	for( INT nPos = 0; nPos < m_Size; nPos++ ) {

		CCommsMapReg *pReg = m_pRegs->m_ppReg[nPos];

		if( pReg->IsMapped() ) {

			DWORD Ref = m_pData[nPos];

			if( Ref ) {

				if( pData->IsAvail(Ref) ) {

					DWORD Data = pData->GetData(Ref, typeVoid, getNone);

					pReg->SetValue(Data, Flags | setDirect);

					m_pInfo[nPos].m_fLocal = TRUE;

					continue;
					}
				}

			m_pInfo[nPos].m_fLocal = FALSE;

			continue;
			}

		m_pInfo[nPos].m_fLocal = TRUE;
		}

	return TRUE;
	}

void CCommsMapBlock::MarkDone(void)
{
	if( m_pAck ) {

		m_pAck->SetValue(1, typeInteger, setDirect);
		}

	m_uLast = GetTickCount();
	}

void CCommsMapBlock::SetError(UINT uError)
{
	if( m_uError != errorHard ) {

		if( m_uError != uError ) {

			if( m_Write == 0 || m_Slave == 1 ) {

				if( uError   == errorNone ) {

					m_pRegs->SetScan(scanTrue);
					}

				if( m_uError == errorNone ) {

					m_pRegs->SetScan(scanFalse);
					}
				}

			m_uError = uError;
			}
		}
	}

BOOL CCommsMapBlock::SetCommsData(INT nPos, DWORD Data)
{
	if( ShouldRead(nPos) ) {

		CCommsMapReg *pReg = m_pRegs->m_ppReg[nPos];

		UINT         Flags = GetFlags();

		return pReg->SetValue(Data, Flags | setDirect);
		}

	return TRUE;
	}

PDWORD CCommsMapBlock::GetWriteData(INT nPos)
{
	return m_pData + nPos;
	}

void CCommsMapBlock::SetWriteDone(INT nPos)
{
	CInfo &Info = m_pInfo[nPos];

	if( Info.m_fDirty ) {

		Info.m_fDirty = FALSE;

		m_uDirty--;
		}
	}

// Implementation

BOOL CCommsMapBlock::InitData(void)
{
	if( !m_pData ) {

		m_pData = New DWORD [ m_Size ];

		m_pInfo = New CInfo [ m_Size ];

		memset(m_pData, 0, m_Size * sizeof(DWORD));

		memset(m_pInfo, 0, m_Size * sizeof(CInfo));

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsMapBlock::InitWriteData(void)
{
	if( InitData() ) {

		m_uDirty = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCommsMapBlock::InitReadData(void)
{
	if( InitData() ) {

		CCommsManager *pComms = CCommsSystem::m_pThis->m_pComms;

		for( INT nPos = 0; nPos < m_Size; nPos++ ) {

			CCommsMapReg *pReg = m_pRegs->m_ppReg[nPos];

			if( pReg->IsMapped() ) {

				CAddress Addr = GetAddress(nPos);

				DWORD    Ref  = 0;
		
				if( pComms->FindAddress(Addr, (CDataRef &) Ref, m_pDevice) ) {

					m_pData[nPos] = Ref;
					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
