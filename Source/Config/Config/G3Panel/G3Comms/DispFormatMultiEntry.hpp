
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatMultiEntry_HPP

#define INCLUDE_DispFormatMultiEntry_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Entry
//

class DLLAPI CDispFormatMultiEntry : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatMultiEntry(void);

		// Operations
		void Set(CString Data, CString Text);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Data Matching
		BOOL MatchData(DWORD Data, UINT Type, CString &Text);

		// Data Testing
		int CompareData(DWORD Data, UINT uType);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		void UpdateTypes(BOOL fComp);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pData;
		CCodedText * m_pText;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
