
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMetaSysHandler;
class CMetaSysN2SystemDriver3;

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "ctime.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Info
//

static BYTE const Table[]  = { 1, 2, 3, 4, 5, 6, 7 };

static BYTE const Region[] = { 1, 3, 2, 4, 5, 6, 7 };

static BYTE const Span[]   = { 0, 0, 0, 0, 0, 0, 0 };

static BYTE const Attrib[] = {14, 5, 4, 7, 2, 2, 2 };

//////////////////////////////////////////////////////////////////////////
//
// Structures
//

struct CTable
{
	BYTE m_Data[256 * 4];
	};

struct CDevice
{
	BYTE   m_bDrop;
	BOOL   m_fOnline;
	CTable m_Table[elements(Table)];
	};

//////////////////////////////////////////////////////////////////////////
//
// MetaSys N2 System Port Handler - Multi Device - Current Data Only
//

class CMetaSysHandler : public IPortHandler
{
	public:
		// Constructor
		CMetaSysHandler(IHelper *pHelper, CMetaSysN2SystemDriver3 * pDriver);

		// Destructor
		~CMetaSysHandler(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Binding
		void MCALL Bind(IPortObject *pPort);

		// Event Handlers
		void MCALL OnOpen(CSerialConfig const &Config);
		void MCALL OnClose(void);
		void MCALL OnTimer(void);
		BOOL MCALL OnTxData(BYTE &bData);
		void MCALL OnTxDone(void);
		void MCALL OnRxData(BYTE bData);
		void MCALL OnRxDone(void);

		// Driver Access
		PBYTE GetData(BYTE bDevice, UINT uTable, UINT uOffset);
		BOOL  SetData(BYTE bDevice, UINT uTable, UINT uOffset, PBYTE pBytes, UINT uCount);

	protected:

		// Help

		IHelper          * m_pHelper;
		IExtraHelper	 * m_pExtra;

		// Port

		IPortObject      * m_pPort;
		BOOL               m_fOpen;

		
		
		// Members
		ULONG		   m_uRefs;
		BOOL		   m_fSOM;
		BYTE		   m_Rx[256];
		BYTE		   m_Tx[256];
		UINT		   m_RxPtr;
		UINT		   m_TxPtr;
		UINT		   m_TxCount;
		BYTE		   m_Check;
		LPCTXT		   m_pHex;
		CDevice	*	   m_pDev;
	
		CMetaSysN2SystemDriver3 * m_pDriver;
		
		// Implementation
		BOOL	  DoProcess(void);
		BOOL	  DoListen(void);
		void	  Tx(void);
		void	  HandleFrame(void);
		void	  HandleSynchTimeCmd(void);
		void	  HandlePollNoAck(void);
		void	  HandlePollWithAck(void);
		void	  HandleIdentifyDeviceCmd(void);
		void	  HandleWriteSingle(UINT uRegion);
		void	  HandleReadSingle(UINT uRegion);
		void	  HandleWriteMulti(void);
		void	  HandleReadMulti(void);
		void	  HandleOverride(void);
		void	  HandleOverrideRelease(void);
		void	  HandleUploadRequest(void);
		void	  HandleUploadRecord(void);
		void	  HandleUploadComplete(void);
		void	  HandleDownloadRequest(void);
		void	  HandleDownloadRecord(void);
		void	  HandleDownloadComplete(void);
		void	  HandleWarmStart(void);
		void	  HandleStatusUpdate(void);
		void	  HandleReadMemoryCmd(void);
		void	  HandleError(UINT uCode);

		// Data Handling
	       	void SetData(UINT uLook, UINT uObj, UINT uAtr, UINT uPtr, UINT uCount);
		void SetAnalogs(UINT uLook, UINT uObj, UINT uPtr);
		void SetBinarys(UINT uLook, UINT uObj, UINT uPtr);
		void SetInternals(UINT uLook, UINT uObj, UINT uPtr);
		void SendData(UINT uLook, UINT uObj, UINT uAtr, UINT uCount);
		void SendAnalogs(UINT uLook, UINT uObj, UINT uAtr, UINT uCount);
		void SendBinaryInputs(UINT uLook, UINT uObj, UINT uAtr, UINT uCount);
		void SendBinaryOutputs(UINT uLook, UINT uObj, UINT uAtr, UINT uCount);
		void SendInternals(UINT uLook, UINT uObj, UINT uAtr, UINT uCount);
	       		
	  
		// Frame Building
		void Start(UINT uByte);
		void AddByte(BYTE bByte);
		void AddCheck(void);

		void PutChar1(DWORD Data);
		void PutChar2(DWORD Data);
		void PutChar4(DWORD Data);
		void PutChar8(DWORD Data); 

		// Frame Access
		UINT GetChar1(UINT& uPos);
		UINT GetChar2(UINT& uPos,  BOOL fHex = TRUE);
		UINT GetChar4(UINT& uPos,  BOOL fHex = TRUE);
		UINT GetChar8(UINT& uPos);

		// Helpers
		BOOL IsOnline(void);
		BOOL IsSynchTimeCmd(UINT uCmd, UINT uSub);
		BOOL IsPollNoAck(UINT uCmd, UINT uSub);
		BOOL IsPollWithAck(UINT uCmd, UINT uSub);
		BOOL IsIdentifyDeviceTypeCmd(UINT uCmd);
	 	BOOL IsWriteSingle(UINT uCmd);
		BOOL IsReadSingle(UINT uCmd);
		BOOL IsWriteMulti(UINT uCmd, UINT uSub);
		BOOL IsReadMulti(UINT uCmd, UINT uSub);
		BOOL IsOverride(UINT uCmd, UINT uSub);
		BOOL IsOverrideReleaseReq(UINT uCmd, UINT uSub);
		BOOL IsUploadRequest(UINT uCmd, UINT uSub);
		BOOL IsUploadRecord(UINT uCmd, UINT uSub);
		BOOL IsUploadComplete(UINT uCmd, UINT uSub);
		BOOL IsDownloadRequest(UINT uCmd, UINT uSub);
		BOOL IsDownloadRecord(UINT uCmd, UINT uSub);
		BOOL IsDownloadComplete(UINT uCmd, UINT uSub);
		BOOL IsWarmStart(UINT uCmd, UINT uSub);
		BOOL IsStatusUpdate(UINT uCmd, UINT uSub);
		BOOL IsReadMemoryCmd(UINT uCmd, UINT uSub);
		UINT FindLookup(UINT uRegion);
		void FindAttribute(UINT uLook, UINT& uAtr);
		UINT FindCount(UINT uLook, UINT uAtr);
		BOOL TxDelay(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Johnson Controls MetaSys N2 System Slave Driver - Multi Device - Current Values Only
//
//

class CMetaSysN2SystemDriver3 : public CMasterDriver
{
	public:
		// Constructor
		CMetaSysN2SystemDriver3(void);

		// Destructor
		~CMetaSysN2SystemDriver3(void);

		// Config
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)Write(AREF Addr, PDWORD pData, UINT uCount);

		// Handler Access
		CDevice * FindDevice(BYTE bDevice);

			    
	protected:

		struct CContext
		{
			BYTE           m_bDrop;
			UINT	       m_fOnline;
			};

		// Data Members

		CContext	* m_pCtx;
		CDevice *	  m_pList[255];
		UINT		  m_uDevs;
		

		// Handler

		CMetaSysHandler * m_pHandler;

		// Implementation
		void CreateDevice(BYTE bDevice);
		
	};

// End of File

