
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FATEKPLC_HPP
#define	INCLUDE_FATEKPLC_HPP

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC - Device Options
//

class CFatekPLCDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFatekPLCDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		
	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Helpers
	};

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC UDP Master Device Options
//

class CFatekPLCUDPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFatekPLCUDPDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Driver
//

// Fatek Spaces
enum {
	SP_XS	= 1,
	SP_YS	= 2,
	SP_MS	= 3,
	SP_SS	= 4,
	SP_TS	= 5,
	SP_CS	= 6,
	SP_TR	= 7,
	SP_CR	= 8,
	SP_H	= 9,
	SP_D	= 10,
	SP_ST	= 11,
	SP_RS	= 12,
	SP_XE	= 13,
	SP_YE	= 14,
	SP_ME	= 15,
	SP_SE	= 16,
	SP_TE	= 17,
	SP_CE	= 18,
	SP_ERR	= 20
	};

class CFatekPLCSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CFatekPLCSerialDriver(void);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

class CFatekPLCUDPDriver : public CFatekPLCSerialDriver
{
	public:
		// Constructor
		CFatekPLCUDPDriver(void);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Address Selection
//

class CFatekPLCAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CFatekPLCAddrDialog(CFatekPLCSerialDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Data Members

		// Overridables
		BOOL	AllowType(UINT uType);

		// Helpers

	};
        
// End of File

#endif
