
#include "Intern.hpp"  

#include "UsbHidMapper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbHidReport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hid Mouse Mapper
//

// Constructor

CUsbHidMapper::CUsbHidMapper(void)
{
	m_Debug	      = debugWarn;

	m_pHostDriver = NULL;

	m_pSendBuff   = NULL;

	m_pRecvBuff   = NULL;

	m_uSendSize   = 0;

	m_uRecvSize   = 0;

	m_fPolling    = false;
	}

// Destructor

CUsbHidMapper::~CUsbHidMapper(void)
{
	FreeBuffers();
	}

// IUnknown

HRESULT CUsbHidMapper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHidMapper);

	StdQueryInterface(IUsbHidMapper);

	return E_NOINTERFACE;
	}

ULONG CUsbHidMapper::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbHidMapper::Release(void)
{
	StdRelease();
	}

// IUsbHidMapper

void CUsbHidMapper::Bind(IUsbHostHid *pHost)
{
	m_pHostDriver = pHost;
	}

BOOL CUsbHidMapper::SetReport(PCBYTE pReport, UINT uSize)
{
	return FALSE;
	}

UINT CUsbHidMapper::GetUsagePage(void)
{
	return NOTHING;
	}

UINT CUsbHidMapper::GetUsageType(void)
{
	return NOTHING;
	}

UINT CUsbHidMapper::GetRecvSize(UINT uId)
{
	return (m_uRecvSize + 7) / 8;
	}

UINT CUsbHidMapper::GetSendSize(UINT uId)
{
	return (m_uSendSize + 7 ) / 8;
	}

void CUsbHidMapper::SetConfig(PCBYTE pConfig, UINT uSize)
{
	}

void CUsbHidMapper::Poll(void)
{
	}

// Helpers

bool CUsbHidMapper::RecvReport(void)
{
	if( m_fPolling ) {

		UINT uCount = m_pHostDriver->WaitReport(0);

		if( uCount != NOTHING ) {
		
			m_fPolling = false;

			return uCount && uCount <= GetRecvSize(0);
			}
		
		return false;
		}
		
	if( m_pHostDriver->RecvReport(m_pRecvBuff, GetRecvSize(0)) ) {

		m_fPolling = true;
		}
			
	return false;
	}

bool CUsbHidMapper::SendReport(void)
{
	return m_pHostDriver->SetReport(reportOutput, 0, m_pSendBuff, GetSendSize(0));
	}

// Parsers

void CUsbHidMapper::ParseReportDesc(PCBYTE pData, UINT uSize)
{
	Trace(debugInfo, "ParseReportDesc");

	CContext Ctx = { 0 };
	
	CUsbHidReport Rep;
	
	Rep.Attach(pData, uSize);

	for( UINT i = Rep.GetFirst(); !Rep.Failed(i); Rep.GetNext(i) ) {

		HidShort const &Item = * (HidShort const *) Rep[i];

		switch( Item.m_bType ) {

			case itemMain:

				ParseMain(Item, Ctx);
				
				break;

			case itemGlobal:

				ParseGlobal(Item, Ctx);

				break;

			case itemLocal:

				ParseLocal(Item, Ctx);

				break;
			}
		}

	Trace(debugInfo, "Report Size : Send(%d), Recv(%d)", GetSendSize(0), GetRecvSize(0));
	}

void CUsbHidMapper::ParseMain(HidShort const &Item, CContext &Ctx)
{
	switch( Item.m_bTag ) {
	
		case mainCollection:

			Trace(debugInfo, "Collection");

			m_uSendSize = 0;

			m_uRecvSize = 0;

			break;

		case mainInput:

			Trace(debugInfo, "Input(Size=%d, Count=%d)", Ctx.m_uRepSize, Ctx.m_uRepCount);

			m_uRecvSize += (Ctx.m_uRepSize * Ctx.m_uRepCount);

			break;

		case mainOutput:

			Trace(debugInfo, "Output(Size=%d, Count=%d)", Ctx.m_uRepSize, Ctx.m_uRepCount);

			m_uSendSize += (Ctx.m_uRepSize * Ctx.m_uRepCount);

			break;
		}	
	}

void CUsbHidMapper::ParseGlobal(HidShort const &Item, CContext &Ctx)
{
	switch( Item.m_bTag ) {
	
		case globalUsagePage:
			
			Trace(debugInfo, "UsagePage(%d)", Item.m_bData[0]);

			Ctx.m_uUsagePage = Item.m_bData[0];
			
			break;
		
		case globalRepSize:

			Trace(debugInfo, "Report Size(%d)", Item.m_bData[0]);

			Ctx.m_uRepSize = Item.m_bData[0];
			
			break;
		
		case globalRepCount:

			Trace(debugInfo, "Report Count(%d)", Item.m_bData[0]);

			Ctx.m_uRepCount = Item.m_bData[0];
			
			break;
		}
	}

void CUsbHidMapper::ParseLocal(HidShort const &Item, CContext &Ctx)
{
	switch( Item.m_bTag ) {
	
		case localUsage:

			Trace(debugInfo, "Usage(%d)", Item.m_bData[0]);
		
			Ctx.m_uUsage = Item.m_bData[0];

			break;
		}
	}
		
// Buffers

void CUsbHidMapper::InitBuffers(void)
{
	FreeBuffers();

	m_pSendBuff = New BYTE [ GetSendSize(0) ];

	m_pRecvBuff = New BYTE [ GetRecvSize(0) ];

	memset(m_pSendBuff, 0, GetSendSize(0)); 

	memset(m_pRecvBuff, 0, GetRecvSize(0)); 
	}

void CUsbHidMapper::FreeBuffers(void)
{
	if( m_pSendBuff ) {

		delete [] m_pSendBuff;

		m_pSendBuff = NULL;
		}
	
	if( m_pRecvBuff ) {

		delete [] m_pRecvBuff;

		m_pRecvBuff = NULL;
		}
	}

// End of File
