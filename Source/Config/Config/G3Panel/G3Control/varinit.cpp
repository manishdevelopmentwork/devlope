
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitial, CUIItem);

// Class Enumeration

CLASS CVariableInitial::GetClass(CStratonDataTypeDescriptor const &Type, BOOL fArray)
{
	if( Type.IsBasic() && !Type.Is64Bit() ) {

		CString Class;

		Class += L"CVariableInitial";
		
		Class += fArray ? L"Array" : L"Single";

		if(      Type.IsBool() )    { Class += L"Boolean"; }

		else if( Type.IsString() )  { Class += L"String";  }

		else if( Type.IsTime() )    { Class += L"Time";	   }

		else if( Type.IsNumeric() ) { Class += L"Number";  }

		else if( Type.IsReal() )    { Class += L"Real";	   }

		else                        { return NULL;         }

		return AfxNamedClass(Class);
		}

	return NULL;
	}

// Constructor

CVariableInitial::CVariableInitial(void)
{
	m_pFormat = NULL;
	}

// Destructor

CVariableInitial::~CVariableInitial(void)
{
	delete m_pFormat;
	}

// Operations

void CVariableInitial::Preserve(CVariableInitial *pOld)
{
	}

BOOL CVariableInitial::CheckRange(void)
{
	return FALSE;
	}

// Formatter

CVariableInitialFormatter & CVariableInitial::GetFormat(void)
{
	return *m_pFormat;
	}

// UI Creation

BOOL CVariableInitial::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIStdPage(CString(IDS_INITIAL_VALUE),      AfxPointerClass(this), 1));

	return FALSE;
	}

// Persistance

void CVariableInitial::Init(void)
{		
	CUIItem::Init();
	
	FindVariable();

	DWORD   dwProject = m_pVariable->GetProject();

	DWORD dwTypeIdent = m_pVariable->m_TypeIdent;

	CStratonDataTypeDescriptor Type(dwProject, dwTypeIdent);

	m_Type = Type.m_Name;
	}

void CVariableInitial::PostLoad(void)
{		
	CUIItem::PostLoad();
	
	FindVariable();
	}

// Meta Data Creation

void CVariableInitial::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Type);

	Meta_SetName((IDS_INITIAL_VALUE));
	}

// Implementation

void CVariableInitial::FindVariable(void)
{
	m_pVariable = (CControlVariable *) GetParent(AfxRuntimeClass(CControlVariable));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Single
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialSingle, CVariableInitial);

// Constructor

CVariableInitialSingle::CVariableInitialSingle(void)
{
	}

// UI Update

void CVariableInitialSingle::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		BOOL fPersist = m_pVariable->m_Persist == 1;
		
		pHost->EnableUI(L"Data", !fPersist);
		}

	if( Tag == L"Data" ) {

		BOOL fPersist = m_pVariable->m_Persist == 1;
		
		pHost->EnableUI(L"Data", !fPersist);
		}

	CVariableInitial::OnUIChange(pHost, pItem, Tag);
	}

// Operations

void CVariableInitialSingle::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArray)) ) {

		m_Data = ((CVariableInitialArray *) pOld)->m_Data[0];

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingle)) ) {

		m_Data = ((CVariableInitialSingle *) pOld)->m_Data;

		Commit();

		return;
		}
	}

// Persistance

void CVariableInitialSingle::Init(void)
{		
	CVariableInitial::Init();

	Commit();
	}

void CVariableInitialSingle::PostLoad(void)
{		
	CVariableInitial::PostLoad();

	Commit();
	}

// Meta Data Creation

void CVariableInitialSingle::AddMetaData(void)
{
	CVariableInitial::AddMetaData();

	Meta_AddString(Data);	
	}

// Implementation

void CVariableInitialSingle::Commit(void)
{
	CVariableInitialFormatter &Fmt = GetFormat();

	DWORD  dwProject = m_pVariable->GetProject();
	
	DWORD dwVariable = m_pVariable->GetHandle();

	DWORD    dwGroup = m_pVariable->GetGroup(dwProject);

	afxDatabase->SetVarInitValue( dwProject, 
				      dwGroup, 
				      dwVariable, 
				      Fmt.ToStraton(m_Data)
				      );
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Single Time
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialSingleTime, CVariableInitialSingle);

// Constructor

CVariableInitialSingleTime::CVariableInitialSingleTime(void)
{
	m_Data    = L"0";

	m_pFormat = New CVariableInitialFormatterTime;
	}

// Operations

void CVariableInitialSingleTime::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleString)) ) {

		CVariableInitialSingleString *pPrev = (CVariableInitialSingleString *) pOld;

		if( pPrev->m_Data.IsEmpty() ) {
			
			m_Data = L"0";
			}
		else {
			m_Data = CPrintf(L"%u", watoi(pPrev->m_Data));
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleReal)) ) {

		CString Real = ((CVariableInitialSingleReal *) pOld)->m_Data;

		UINT p1 = Real.Find(L'.');

		m_Data = Real.Left(p1);
		
		Commit();
		
		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleBoolean)) ) {
		
		m_Data = L"0";

		Commit();

		return;
		}

	CVariableInitialSingle::Preserve(pOld);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Single Boolean
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialSingleBoolean, CVariableInitialSingle);

// Constructor

CVariableInitialSingleBoolean::CVariableInitialSingleBoolean(void)
{
	m_Data    = L"FALSE";

	m_pFormat = New CVariableInitialFormatter;
	}

// Operations

void CVariableInitialSingleBoolean::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleString)) ) {

		CVariableInitialSingleString *pPrev = (CVariableInitialSingleString *) pOld;

		if( pPrev->m_Data.IsEmpty() ) {
			
			m_Data = L"FALSE";
			}
		else {
			CStringArray List(L"FALSE", L"TRUE");

			if( List.Find(pPrev->m_Data) < NOTHING ) {
				
				m_Data = pPrev->m_Data;
				}
			else {
				m_Data = List[watoi(pPrev->m_Data) ? 1 : 0];
				}
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleReal)) ) {

		CVariableInitialSingleReal *pPrev = (CVariableInitialSingleReal *) pOld;

		m_Data = watof(pPrev->m_Data) > 0.0 ? L"TRUE" : L"FALSE";

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleNumber)) ) {

		CVariableInitialSingleNumber *pPrev = (CVariableInitialSingleNumber *) pOld;

		m_Data = watoi(pPrev->m_Data) > 0 ? L"TRUE" : L"FALSE";

		Commit();

		return;
		}

	CVariableInitialSingle::Preserve(pOld);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Single String
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialSingleString, CVariableInitialSingle);

// Constructor

CVariableInitialSingleString::CVariableInitialSingleString(void)
{
	m_pFormat = New CVariableInitialFormatterString;
	}

// Operations

void CVariableInitialSingleString::Preserve(CVariableInitial *pOld)
{
	CVariableInitialSingle::Preserve(pOld);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Single Number
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialSingleNumber, CVariableInitialSingle);

// Constructor

CVariableInitialSingleNumber::CVariableInitialSingleNumber(void)
{
	m_Data    = L"0";

	m_pFormat = New CVariableInitialFormatterNumber;
	}

// Operations

void CVariableInitialSingleNumber::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleString)) ) {

		CVariableInitialSingleString *pPrev = (CVariableInitialSingleString *) pOld;

		if( pPrev->m_Data.IsEmpty() ) {
			
			m_Data = L"0";
			}
		else {
			m_Data = CPrintf(L"%u", watoi(pPrev->m_Data));
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleBoolean)) ) {

		CVariableInitialSingleBoolean *pPrev = (CVariableInitialSingleBoolean *) pOld;

		m_Data = pPrev->m_Data == L"FALSE" ? L"0" : L"1";

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleReal)) ) {

		CVariableInitialSingleReal *pPrev = (CVariableInitialSingleReal *) pOld;

		CString Real = pPrev->m_Data;

		m_Data = Real.Left(Real.Find(L'.'));

		CheckRange();

		Commit();

		return;
		}

	CVariableInitialSingle::Preserve(pOld);
	}

BOOL CVariableInitialSingleNumber::CheckRange(void)
{
	DWORD   dwProject = m_pVariable->GetProject();

	DWORD dwTypeIdent = m_pVariable->m_TypeIdent;

	CStratonDataTypeDescriptor Type(dwProject, dwTypeIdent);

	INT64 nMin;

	INT64 nMax;

	if( Type.GetRange(nMin, nMax) ) {
		
		INT64 nVal =  watoi64(m_Data);

		if( nVal < nMin ) {

			m_Data = CPrintf(L"%ld", nMin);

			return TRUE;
			}

		if( nVal > nMax ) {

			m_Data = CPrintf(L"%ld", nMax);

			return TRUE;
			}
		}
	
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Single Real
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialSingleReal, CVariableInitialSingle);

// Constructor

CVariableInitialSingleReal::CVariableInitialSingleReal(void)
{
	m_Data    = L"0.0";

	m_pFormat = New CVariableInitialFormatterReal;
	}

// Operations

void CVariableInitialSingleReal::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleString)) ) {

		m_Data = L"0.0";

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingleBoolean)) ) {

		CVariableInitialSingleBoolean *pPrev = (CVariableInitialSingleBoolean *) pOld;

		m_Data = pPrev->m_Data == L"FALSE" ? L"0.0" : L"1.0";

		Commit();

		return;
		}

	CVariableInitialSingle::Preserve(pOld);
	}

/////////////////////////////////////////////////////////////////////////
//
// Variable Initial Value Array Page
//

class CVariableInitialArrayPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CVariableInitialArrayPage(CVariableInitial *pInitial, CString Title, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CVariableInitial * m_pInitial;
	};

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Page
//

// Runtime Class

AfxImplementRuntimeClass(CVariableInitialArrayPage, CUIStdPage);

// Constructor

CVariableInitialArrayPage::CVariableInitialArrayPage(CVariableInitial *pInitial, CString Title, UINT uPage)
{
	m_pInitial  = pInitial;

	m_uPage     = uPage;
	}

// Operations

BOOL CVariableInitialArrayPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CVariableInitialFormatter &Fmt = m_pInitial->GetFormat();

	pView->StartTable(L"Intial Values", 1);

	pView->AddColHead(CString(IDS_VALUE));

	UINT c = m_pInitial->m_pVariable->m_Extent;

	for( UINT n = 0; n < c ; n ++ ) {

		pView->AddRowHead(CPrintf(L"%u", n));

		CUIData UIData;

		UIData.m_Tag       = CPrintf(L"Data%2.2u", n);

		UIData.m_Label     = CPrintf(L"Element %u", n);

		UIData.m_ClassText = Fmt.GetUIClassText();

		UIData.m_ClassUI   = Fmt.GetUIClassUI();

		UIData.m_Format    = Fmt.GetUIFormat();

		UIData.m_Tip       = CString(IDS_INDICATES_INITIAL_2);

		pView->AddUI(pItem, L"root", &UIData);
		}	

	pView->EndTable();	

	pView->NoRecycle();

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialArray, CVariableInitial);

// Constructor

CVariableInitialArray::CVariableInitialArray(void)
{
	}

// Operations

CString CVariableInitialArray::ToStraton(void)
{
	// Structure

	struct CEntry {

		CString	m_Elem;
		UINT	m_uCount;
		};

	CVariableInitialFormatter &Fmt = GetFormat();

	CArray  <CEntry> List;

	UINT c = m_pVariable->m_Extent;
	
	for( UINT n = 0; n < c; n ++ ) {

		CString Elem = Fmt.ToStraton(m_Data[n]);

		AfxAssert(!Elem.IsEmpty());

		if( List.IsEmpty() ) {

			CEntry Entry;

			Entry.m_Elem   = Elem;

			Entry.m_uCount = 0;
			
			List.Append(Entry);
			}

		UINT   uLast = List.GetCount() - 1;

		CEntry &Last = (CEntry &) List[uLast];

		if( Last.m_Elem == Elem ) {

			Last.m_uCount ++;
			}
		else {
			CEntry Entry;

			Entry.m_Elem   = Elem;

			Entry.m_uCount = 1;

			List.Append(Entry);
			}
		}

	CString Text;

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CEntry const &Entry = List[n];

		if( !Text.IsEmpty() ) {
			
			Text += L",";
			}

		if( Entry.m_uCount > 1 ) {

			Text += CPrintf(L"%u", Entry.m_uCount);

			Text += L"(";

			Text += Entry.m_Elem;

			Text += L")";
			}
		else {
			Text += Entry.m_Elem;
			}
		}

	return Text;
	}

void CVariableInitialArray::ToCrimson(CString Text)
{
	UINT uCount = 0;

	CStringArray List;

	CString Work;

	for( UINT n = 0; n < Text.GetLength(); n ++ ) {

		// TODO -- improve this parser

		TCHAR c = Text[n];

		if( c == L'(' ) {

			uCount = watoi(Work);

			Work.Empty();

			continue;
			}

		if( c == L')' ) {

			while( uCount-- ) {

				List.Append(Work);
				}

			Work.Empty();

			n ++; //

			continue;
			}

		if( c == L',' ) {
			
			List.Append(Work);

			Work.Empty();

			continue;
			}

		Work += c;
		}

	if( !Work.IsEmpty() ) {

		List.Append(Work);
		}

	CVariableInitialFormatter &Fmt = GetFormat();

	UINT c = List.GetCount();
	
	for( UINT n = 0; n < c; n ++ ) {		

		CString Elem = Fmt.ToCrimson(List[n]);		

		//AfxAssert(!Elem.IsEmpty());
		
		m_Data[n] = Elem;
		}	
	}

// Operations

void CVariableInitialArray::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialSingle)) ) {

		m_Data[0] = ((CVariableInitialSingle *) pOld)->m_Data;

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArray)) ) {

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			m_Data[n] = ((CVariableInitialArray *) pOld)->m_Data[n];
			}

		Commit();

		return;
		}
	}

// UI Creation

BOOL CVariableInitialArray::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CVariableInitialArrayPage(this, CString(IDS_INITIAL_VALUE), 1));

	return FALSE;
	}

// Meta Data Creation

void CVariableInitialArray::AddMetaData(void)
{
	CVariableInitial::AddMetaData();

	for( UINT n = 0; n < elements(m_Data); n ++ ) {

		Meta_Add(CPrintf(L"Data%2.2u", n), m_Data[n], metaString);
		}
	}

// Implementation

void CVariableInitialArray::Commit(void)
{
	DWORD  dwProject = m_pVariable->GetProject();
	
	DWORD dwVariable = m_pVariable->GetHandle();

	DWORD    dwGroup = m_pVariable->GetGroup(dwProject);

	afxDatabase->SetVarInitValue( dwProject, 
				      dwGroup, 
				      dwVariable, 
				      ToStraton()
				      );	
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialArrayBoolean, CVariableInitialArray);

// Constructor

CVariableInitialArrayBoolean::CVariableInitialArrayBoolean(void)
{
	for( UINT n = 0; n < elements(m_Data); n ++ ) {

		m_Data[n] = L"FALSE";
		}

	m_pFormat = New CVariableInitialFormatterBoolean;
	}

// Operations

void CVariableInitialArrayBoolean::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayString)) ) {

		CVariableInitialArrayString *pPrev = (CVariableInitialArrayString *) pOld;

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			if( pPrev->m_Data[n].IsEmpty() ) {
			
				m_Data[n] = L"FALSE";
				}
			else {
				CStringArray List(L"FALSE", L"TRUE");

				if( List.Find(pPrev->m_Data[n]) < NOTHING ) {
				
					m_Data[n] = pPrev->m_Data[n];
					}
				else {
					m_Data[n] = List[watoi(pPrev->m_Data[n]) ? 1 : 0];
					}
				}
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayReal)) ) {

		CVariableInitialArrayReal *pPrev = (CVariableInitialArrayReal *) pOld;

		for( UINT n = 0; n < elements(m_Data); n++ ) {

			m_Data[n] = watof(pPrev->m_Data[n]) > 0.0 ? L"TRUE" : L"FALSE";
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayNumber)) ) {

		CVariableInitialArrayNumber *pPrev = (CVariableInitialArrayNumber *) pOld;

		for( UINT n = 0; n < elements(m_Data); n++ ) {

			m_Data[n] = watoi(pPrev->m_Data[n]) > 0 ? L"TRUE" : L"FALSE";
			}

		Commit();

		return;
		}

	CVariableInitialArray::Preserve(pOld);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialArrayString, CVariableInitialArray);

// Constructor

CVariableInitialArrayString::CVariableInitialArrayString(void)
{
	m_pFormat = New CVariableInitialFormatterString;
	}

// Operations

void CVariableInitialArrayString::Preserve(CVariableInitial *pOld)
{
	CVariableInitialArray::Preserve(pOld);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialArrayTime, CVariableInitialArray);

// Constructor

CVariableInitialArrayTime::CVariableInitialArrayTime(void)
{
	for( UINT n = 0; n < elements(m_Data); n ++ ) {

		m_Data[n] = L"0";
		}

	m_pFormat = New CVariableInitialFormatterTime;
	}

// Operations

void CVariableInitialArrayTime::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayString)) ) {

		CVariableInitialArrayString *pPrev = (CVariableInitialArrayString *) pOld;

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			if( pPrev->m_Data[n].IsEmpty() ) {
			
				m_Data[n] = L"0";
				}
			else {
				m_Data[n] = CPrintf(L"%u", watoi(pPrev->m_Data[n]));
				}
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayReal)) ) {

		CVariableInitialArrayReal *pPrev = (CVariableInitialArrayReal *) pOld;

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			UINT p1 = m_Data[n].Find(L'.');

			pPrev->m_Data[n] = m_Data[n].Left(p1);
		
			Commit();
			}
		
		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayBoolean)) ) {

		CVariableInitialArrayBoolean *pPrev = (CVariableInitialArrayBoolean *) pOld;

		for( UINT n = 0; n < elements(m_Data); n ++ ) {
		
			m_Data[n] = (pPrev->m_Data[n] == L"FALSE") ? L"0" : L"1";
			}

		Commit();

		return;
		}

	CVariableInitialArray::Preserve(pOld);
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialArrayNumber, CVariableInitialArray);

// Constructor

CVariableInitialArrayNumber::CVariableInitialArrayNumber(void)
{
	for( UINT n = 0; n < elements(m_Data); n ++ ) {

		m_Data[n] = L"0";
		}

	m_pFormat = New CVariableInitialFormatterNumber;
	}

// Operations

void CVariableInitialArrayNumber::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayString)) ) {

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			m_Data[n] = L"0";
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayBoolean)) ) {

		CVariableInitialArrayBoolean *pPrev = (CVariableInitialArrayBoolean *) pOld;

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			m_Data[n] = pPrev->m_Data[n] == L"FALSE" ? L"0" : L"1";;
			}

		Commit();

		return;
		}

	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayReal)) ) {

		CVariableInitialArrayReal *pPrev = (CVariableInitialArrayReal *) pOld;

		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			CString Real = pPrev->m_Data[n];

			m_Data[n] = Real.Left(Real.Find(L'.'));			
			}

		CheckRange();
			
		Commit();

		return;
		}

	CVariableInitialArray::Preserve(pOld);
	}

BOOL CVariableInitialArrayNumber::CheckRange(void)
{
	DWORD   dwProject = m_pVariable->GetProject();

	DWORD dwTypeIdent = m_pVariable->m_TypeIdent;

	CStratonDataTypeDescriptor Type(dwProject, dwTypeIdent);

	INT64 nMin;

	INT64 nMax;

	if( Type.GetRange(nMin, nMax) ) {
		
		for( UINT n = 0; n < elements(m_Data); n ++ ) {

			INT64 nVal =  watoi64(m_Data[n]);

			if( nVal < nMin ) {

				m_Data[n] = CPrintf(L"%ld", nMin);

				continue;
				}

			if( nVal > nMax ) {

				m_Data[n] = CPrintf(L"%ld", nMax);

				continue;
				}
			}
		}
	
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Variable Initial Value Array
//

// Dynamic Class

AfxImplementDynamicClass(CVariableInitialArrayReal, CVariableInitialArray);

// Constructor

CVariableInitialArrayReal::CVariableInitialArrayReal(void)
{
	for( UINT n = 0; n < elements(m_Data); n ++ ) {

		m_Data[n] = L"0.0";
		}

	m_pFormat = New CVariableInitialFormatterReal;
	}

// Operations

void CVariableInitialArrayReal::Preserve(CVariableInitial *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CVariableInitialArrayBoolean)) ) {

		CVariableInitialArrayBoolean *pPrev = (CVariableInitialArrayBoolean *) pOld;

		for( UINT n = 0; n < elements(m_Data); n++ ) {

			m_Data[n] = (pPrev->m_Data[n] == L"FALSE") ? L"0.0" : L"1.0";
			}

		Commit();

		return;
		}

	CVariableInitialArray::Preserve(pOld);
	}

// End of File

