
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2001 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Navigation Pane Window
//

// Runtime Class

AfxImplementRuntimeClass(CNavPaneWnd, CPaneWnd);

// Static Data

UINT CNavPaneWnd::timerCheck = AllocTimerID();

// Layout Constants

int const CNavPaneWnd::sizeCategory = 33;

int const CNavPaneWnd::sizeToolbar  = 26;

int const CNavPaneWnd::sizeSplit    = 5;

// Constructor

CNavPaneWnd::CNavPaneWnd(void)
{
	m_Title    = IDS_NAVIGATION_PANE;

	m_ShowID   = IDM_VIEW_SHOW_LEFT;

	m_KeyName  = L"NavPane";

	m_pView    = New CItemViewWnd(viewNavigation, viewCache);

	m_uSelect  = 0;

	m_uHover   = 0;

	m_fPress   = FALSE;

	m_uCapture = 0;

	m_Cursor1.Create(L"VSplitAdjust");

	m_Cursor2.Create(IDC_HAND);

	m_Accel1.Create(L"NavPaneAccel1");

	m_Accel2.Create(L"NavPaneAccel2");

	m_ToolMenu.Create(L"NavHeadTool1");
	}

// Overridables

void CNavPaneWnd::OnAttach(void)
{
	m_pDbase  = (CDatabase *) m_pItem;

	m_pSystem = (CMetaItem *) m_pDbase->GetSystemItem();

	m_uCats   = m_pDbase->GetCatCount();

	m_pCats   = m_pDbase->GetCatList();

	m_pView->Attach(m_pItem);

	LoadConfig();
	}

BOOL CNavPaneWnd::OnNavigate(CString Nav)
{
	CString N1 = Nav.StripToken('/');
	
	if( Select(N1) ) {

		return m_pView->Navigate(Nav);
		}

	return FALSE;
	}

void CNavPaneWnd::OnExec(CCmd *pCmd)
{
	m_pView->ExecCmd(pCmd);
	}

void CNavPaneWnd::OnUndo(CCmd *pCmd)
{
	m_pView->UndoCmd(pCmd);
	}

// Message Map

AfxMessageMap(CNavPaneWnd, CPaneWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchGetInfoType(IDM_GO, OnGoGetInfo)
	AfxDispatchControlType(IDM_GO, OnGoControl)
	AfxDispatchCommandType(IDM_GO, OnGoCommand)

	AfxMessageEnd(CNavPaneWnd)
	};

// Accelerator

BOOL CNavPaneWnd::OnAccelerator(MSG &Msg)
{
	if( IsCurrent() && m_Accel2.Translate(Msg) ) {

		return TRUE;
		}

	if( m_Accel1.Translate(Msg) ) {
		
		return TRUE;
		}

	return FALSE;
	}

// Message Handlers

void CNavPaneWnd::OnPostCreate(void)
{
	m_fShow3 = (m_uShow < m_uCats);

	CPaneWnd::OnPostCreate();

	AttachItemView();
	}

void CNavPaneWnd::OnSize(UINT uCode, CSize Size)
{
	CPaneWnd::OnSize(uCode, Size);

	m_Split = CPaneWnd::GetClientRect();

	m_Split.bottom = m_Split.bottom - m_uShow * sizeCategory;

	m_Split.top    = m_Split.bottom - sizeSplit;
	}

void CNavPaneWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = CWnd::GetClientRect();

	DC.FrameRect(Rect, afxBrush(3dShadow));

	CRect Work = CPaneWnd::GetClientRect();

	DC.SetBkMode(TRANSPARENT);

	DC.Select(afxFont(Bolder));

	for( UINT n = 0; n < m_uShow; n++ ) {

		UINT  uCat  = m_uShow - 1 - n;

		CRect Draw  = Work;

		CRect Line  = Work;

		Draw.top    = Draw.bottom - sizeCategory + 1;

		Line.bottom = Draw.top;

		Line.top    = Draw.top    - 1;

		if( DC.GetPaintRect().Overlaps(Draw) ) {

			DrawCategory(DC, Draw, uCat);
			}

		if( DC.GetPaintRect().Overlaps(Line) ) {

			DC.FillRect(Line, afxBrush(3dShadow));
			}

		Work.bottom = Line.top;
		}

	DrawSplit(DC);

	DC.Deselect();
	}

void CNavPaneWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		UINT uLast = Menu.GetMenuItemID(Menu.GetMenuItemCount() - 1);

		if( uLast < 0x80 ) {

			for( UINT n = 0; n < m_uCats; n++ ) {

				CFormat Text(IDS_TCTRLU, m_pCats[n].m_Name, 1+n);
				
				Menu.AppendMenu(0, IDM_GO_SELECT + n, Text);
				}

			Menu.AppendSeparator();

			Menu.AppendMenu(0, IDM_GO_PREV_CAT, CString(IDS_PREVIOUS));

			Menu.AppendMenu(0, IDM_GO_NEXT_CAT, CString(IDS_NEXT));

			Menu.MakeOwnerDraw(FALSE);
			}
		}
	}

void CNavPaneWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 3 ) {
		
		for( UINT n = m_uShow; n < m_uCats; n++ ) {

			PCAT pCat = m_pCats + n;

			Menu.AppendMenu( 0,
					 IDM_GO_SELECT + n,
					 CPrintf( L"%8.8X",
						  pCat->m_Image2
						  )
					 );
			}
		}
	}

void CNavPaneWnd::OnSetCurrent(BOOL fCurrent)
{
	m_pView->SetCurrent(fCurrent);
	}

BOOL CNavPaneWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( &Wnd == this && uHitTest == HTCLIENT ) {

		CRect  Rect = CWnd::GetClientRect() - 1;
	
		CPoint Pos  = GetCursorPos();
		
		ScreenToClient(Pos);
		
		if( m_Split.PtInRect(Pos) ) {
		
			SetCursor(m_Cursor1);
			
			return TRUE;
			}

		if( Rect.PtInRect(Pos) ) {
		
			SetCursor(m_Cursor2);
		
			return TRUE;
			}
		}
	
	return BOOL(AfxCallDefProc());
	}

void CNavPaneWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	afxMainWnd->SendMessage(WM_CANCELMODE);
	}

void CNavPaneWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_uCapture ) {

		UINT uHover = CheckHover(Pos);

		if( uHover ) {
				
			InvalidateCat(uHover);

			m_uCapture = uHover;
			}
		else {
			if( m_Split.PtInRect(Pos) ) {

				m_uCapture  = 200;

				m_nTrackOrg = Pos.y;

				m_nTrackPos = Pos.y;

				int nBottom = CWnd::GetClientRect().bottom - 1 - (m_Split.bottom - Pos.y);

				m_nTrackMin = nBottom - sizeCategory * m_uCats;

				m_nTrackMax = nBottom - sizeToolbar;
				}
			}

		if( m_uCapture ) {

			if( m_uCapture / 100 == 2 ) {

				CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

				ShowDragBar(DC);
				}

			SetCapture();

			KillTimer(timerCheck);

			m_fPress = TRUE;
			}
		}

	SetFocus();
	}

void CNavPaneWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( m_uCapture ) {

		if( m_uCapture / 100 == 1 ) {

			BOOL fPress = (CheckHover(Pos) == m_uCapture);

			if( m_fPress != fPress ) {

				InvalidateCat(m_uCapture);

				m_fPress = fPress;
				}
			}

		if( m_uCapture / 100 == 2 ) {

			CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

			ShowDragBar(DC);

			m_nTrackPos = Pos.y;

			MakeMin(m_nTrackPos, m_nTrackMax);

			MakeMax(m_nTrackPos, m_nTrackMin);

			ShowDragBar(DC);
			}
		}
	else {
		UINT uHover = CheckHover(Pos);

		if( m_uHover != uHover ) {

			InvalidateCat(m_uHover);

			if( (m_uHover = uHover) ) {
							
				SetTimer(timerCheck, 10);
				}
			
			InvalidateCat(m_uHover);

			return;
			}
		}
	}

void CNavPaneWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_uCapture ) {

		ReleaseCapture();

		if( m_uCapture / 100 == 1 ) {

			if( m_fPress ) {

				Select(m_uCapture % 100);
				}
			}

		if( m_uCapture / 100 == 2 ) {

			CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);

			ShowDragBar(DC);

			m_uShow  = CalcShowCount();

			m_fShow3 = (m_uShow < m_uCats);

			OnUpdateUI();

			OnSize(0, CSize());

			Invalidate(FALSE);

			SaveConfig();
			}

		m_uCapture = 0;

		m_fPress   = FALSE;

		OnMouseMove(uFlags, Pos);
		}
	}

void CNavPaneWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	if( !CheckHover(Pos) ) {

		InvalidateCat(m_uHover);

		m_uHover = 0;

		KillTimer(timerCheck);
		}
	}

// Command Handlers

BOOL CNavPaneWnd::OnGoGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_GO_SELECT && uID < IDM_GO_SELECT + m_uCats ) {

		UINT uCat = uID - IDM_GO_SELECT;

		PCAT pCat = m_pCats + uCat;

		Info.m_Prompt  = CFormat(IDS_SELECT_THE, pCat->m_Name);

		Info.m_ToolTip = pCat->m_Name;

		Info.m_Image   = pCat->m_Image2;

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavPaneWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_GO_SELECT && uID < IDM_GO_SELECT + m_uCats ) {

		UINT uCat = uID - IDM_GO_SELECT;

		if( !Src.IsKindOf(AfxRuntimeClass(CCmdSourceMenu)) ) {

			Src.CheckItem(m_uSelect == uCat);
			}

		Src.EnableItem(TRUE);

		return TRUE;
		}

	if( uID == IDM_GO_PREV_CAT ) {

		Src.EnableItem(m_uSelect > 0);

		return TRUE;
		}

	if( uID == IDM_GO_NEXT_CAT ) {

		Src.EnableItem(m_uSelect < m_uCats - 1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CNavPaneWnd::OnGoCommand(UINT uID)
{
	if( uID >= IDM_GO_SELECT && uID < IDM_GO_SELECT + m_uCats ) {

		UINT uCat = uID - IDM_GO_SELECT;

		Select(uCat);

		return TRUE;
		}

	if( uID == IDM_GO_PREV_CAT ) {

		Select(m_uSelect-1);

		return TRUE;
		}

	if( uID == IDM_GO_NEXT_CAT ) {

		Select(m_uSelect+1);

		return TRUE;
		}

	return FALSE;
	}

// Overridable

CRect CNavPaneWnd::GetClientRect(void)
{
	CRect Rect = CPaneWnd::GetClientRect();

	Rect.bottom -= m_uShow * sizeCategory;

	Rect.bottom -= sizeSplit;

	return Rect;
	}

// Drawing Helpers

void CNavPaneWnd::DrawSplit(CDC &DC)
{
	DC.GradVert(m_Split, afxColor(Blue4), afxColor(Blue2));

	for( UINT n = 0; n < 8; n++ ) {

		int xp = (m_Split.left + m_Split.right - 40) / 2 + 5 * n;

		int yp = (m_Split.top + m_Split.bottom -  1) / 2;

		CRect Rect(xp-1, yp-1, xp+1, yp+1);

		DC.FillRect(Rect+CPoint(1,1), afxBrush(3dFace));

		DC.FillRect(Rect+CPoint(0,0), afxBrush(3dDkShadow));
		}
	}

void CNavPaneWnd::DrawCategory(CDC &DC, CRect Rect, UINT uCat)
{
	CRect Draw   = Rect;

	UINT  uState = 0;

	if( m_uSelect == uCat ) {

		if( m_uHover == 100 + uCat ) {

			DC.GradVert(Draw, afxColor(NavBar1), afxColor(NavBar2));

			uState = MF_HILITE | MF_POPUP;
			}
		else
			DC.GradVert(Draw, afxColor(NavBar2), afxColor(NavBar1));
		}
	else {
		if( m_uHover == 100 + uCat ) {

			if( m_fPress ) {

				DC.GradVert(Draw, afxColor(NavBar1), afxColor(NavBar2));
				}
			else
				DC.GradVert(Draw, afxColor(NavBar4), afxColor(NavBar3));

			uState = MF_HILITE | MF_POPUP;
			}
		else
			DC.GradVert(Draw, afxColor(Blue2), afxColor(Blue3));
		}

	Draw.left  = Draw.left + 2;

	Draw.right = Draw.left + Draw.cy();

	afxButton->Draw(DC, Draw, m_pCats[uCat].m_Image1, L"", uState);

	Draw.left  = Draw.right + 6;

	Draw.right = Rect.right;

	DC.DrawText(m_pCats[uCat].m_Name, Draw, DT_SINGLELINE | DT_VCENTER);
	}

void CNavPaneWnd::ShowDragBar(CDC &DC)
{
	CRect Split  = CWnd::GetClientRect() - 1;

	UINT  uShow = CalcShowCount();

	if( uShow < m_uCats ) {

		Split.bottom -= sizeToolbar;
		}

	Split.bottom = Split.bottom - sizeCategory * uShow;

	Split.top    = Split.bottom - sizeSplit;
	
	DC.PatBlt(Split, DSTINVERT);
	}

// Implementation

BOOL CNavPaneWnd::Select(CString Nav)
{
	for( UINT n = 0; n < m_uCats; n++ ) {

		CString          Name  = m_pCats[n].m_Tag;

		CMetaData const *pMeta = m_pSystem->FindMetaData(Name);

		if( pMeta ) {

			CItem *pItem = pMeta->GetObject(m_pSystem);

			if( pItem->GetFixedName() == Nav ) {

				Select(n);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CNavPaneWnd::Select(UINT uSelect)
{
	if( m_uSelect != uSelect ) {

		InvalidateCat(100 + m_uSelect);

		m_uSelect = uSelect;

		AttachItemView();

		InvalidateCat(100 + m_uSelect);
		}
	}

void CNavPaneWnd::AttachItemView(void)
{
	CString          Name  = m_pCats[m_uSelect].m_Tag;

	CMetaData const *pMeta = m_pSystem->FindMetaData(Name);

	if( pMeta ) {

		CItem *pItem = pMeta->GetObject(m_pSystem);

		if( pItem ) {

			if( !IsCurrent() ) {
				
				m_pView->Attach(pItem);
				}
			else {
				AfxNull(CWnd).SetFocus();

				m_pView->Attach(pItem);

				SetFocus();
				}
			}
		}
	}

void CNavPaneWnd::InvalidateCat(UINT uCat)
{
	if( uCat ) {

		if( (uCat %= 100) < m_uShow ) {
		
			CRect Rect  = CPaneWnd::GetClientRect();

			Rect.top    = Rect.bottom - sizeCategory * (m_uShow - uCat);

			Rect.bottom = Rect.top    + sizeCategory;
			
			Invalidate(Rect, FALSE);
			}
		}
	}

UINT CNavPaneWnd::CheckHover(CPoint Pos)
{
	CRect Rect = GetClientRect();

	if( Pos.x >= Rect.left && Pos.x < Rect.right ) {

		Rect.bottom += sizeSplit;

		if( Pos.y >= Rect.bottom ) {

			UINT uHover = (Pos.y - Rect.bottom) / sizeCategory;

			if( uHover < m_uShow ) {

				return 100 + uHover;
				}
			}
		}

	return 0;
	}

UINT CNavPaneWnd::CalcShowCount(void)
{
	UINT uAdj = sizeCategory / 2;

	UINT uPos = m_Split.bottom + m_nTrackPos - m_nTrackOrg;
	
	UINT uGap = CWnd::GetClientRect().bottom - 1 - uPos + uAdj;

	if( uGap / sizeCategory >= m_uCats ) {

		return m_uCats;
		}
		
	uGap = uGap - sizeToolbar;

	return uGap / sizeCategory;
	}

void CNavPaneWnd::FindConfig(CRegKey &Key)
{
	Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Core");

	Key.MoveTo(m_KeyName);
	}

void CNavPaneWnd::LoadConfig(void)
{
	CRegKey Key;

	FindConfig(Key);

	m_uShow = Key.GetValue(L"Count", 100);

	MakeMin(m_uShow, m_uCats);

	SaveConfig();
	}

void CNavPaneWnd::SaveConfig(void)
{
	CRegKey Key;

	FindConfig(Key);

	UINT uShow = (m_uShow == m_uCats) ? 100 : m_uShow;

	Key.SetValue(L"Count", uShow);
	}

// End of File
