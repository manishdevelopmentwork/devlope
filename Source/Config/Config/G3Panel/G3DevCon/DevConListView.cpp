
#include "Intern.hpp"

#include "DevConListView.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration List View Control
//

// Base Class

#define CBaseClass CListView

// Runtime Class

AfxImplementRuntimeClass(CDevConListView, CBaseClass);

// Constructor

CDevConListView::CDevConListView(CDevConElement *pElem, BOOL fFixed, BOOL fFull)
{
	m_pElem  = pElem;

	m_fFixed = fFixed;

	m_fFull  = fFull;
}

// Attributes

UINT CDevConListView::HitTestControl(NMITEMACTIVATE &Info) const
{
	if( Info.iItem != NOTHING ) {

		CRect Rect = GetItemButtonRect(Info.iItem);

		if( Rect.PtInRect(Info.ptAction) ) {

			UINT uButton = (Info.ptAction.x - Rect.left) / 20;

			UINT lParam  = GetItemParam(Info.iItem);

			if( GetItemButtonEnable(lParam, uButton) ) {

				return uButton;
			}
		}
	}

	return NOTHING;
}

// Operations

void CDevConListView::InsertButtons(void)
{
	LoadImageList();

	UINT uInsert = 0;

	CHeaderCtrl &Hdr = GetHeader();

	if( Hdr.GetItemCount() ) {

		uInsert = Hdr.OrderToIndex(Hdr.GetItemCount() - 1) + 1;
	}

	UINT uWidth = 10 + 20 * (m_fFull ? 4 : 1);

	CListViewColumn Button(uInsert, LVCFMT_LEFT | LVCFMT_FIXED_WIDTH, uWidth, L"");

	InsertColumn(uInsert, Button);
}

// Message Map

AfxMessageMap(CDevConListView, CBaseClass)
{
	AfxDispatchMessage(WM_GETDLGCODE)

	AfxDispatchNotify(0, NM_CUSTOMDRAW, OnCustomDraw)
	
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CDevConListView)
};

// Message Handlers

UINT CDevConListView::OnGetDlgCode(MSG *pMsg)
{
	UINT uCode = AfxCallDefProc();

	if( pMsg ) {

		if( pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP ) {

			if( pMsg->wParam == VK_RETURN ) {

				uCode |= DLGC_WANTMESSAGE;
			}
		}
	}

	return uCode;
}

UINT CDevConListView::OnCustomDraw(UINT uID, NMCUSTOMDRAW &Info)
{
	if( Info.dwDrawStage == CDDS_PREPAINT ) {

		return CDRF_NOTIFYITEMDRAW;
	}
	
	if( Info.dwDrawStage == CDDS_ITEMPREPAINT ) {

		if( m_Images.GetImageCount() ) {

			return CDRF_NOTIFYSUBITEMDRAW;
		}
			
		if( !(Info.lItemlParam & 1) ) {

			NMLVCUSTOMDRAW &Item = (NMLVCUSTOMDRAW &) Info;

			Item.clrTextBk = afxColor(TabFace);

			return CDRF_NEWFONT;
		}

		return CDRF_DODEFAULT;
	}

	if( Info.dwDrawStage == (CDDS_ITEMPREPAINT | CDDS_SUBITEM) ) {

		NMLVCUSTOMDRAW &Item = (NMLVCUSTOMDRAW &) Info;

		if( UINT(Item.iSubItem) == GetHeader().GetItemCount() - 1 ) {

			if( !(Info.lItemlParam & 1) ) {

				CDC DC(Info.hdc);

				DC.FillRect(Info.rc, afxBrush(TabFace));

				DC.Detach(FALSE);
			}

			CRect Rect = Info.rc;

			Rect.top  += 1;

			Rect.left += 7;

			for( UINT i = 0; i < GetItemButtonCount(Info.lItemlParam); i++ ) {

				UINT iImage = GetItemButtonImage(Info.lItemlParam, i);

				if( iImage != NOTHING ) {

					m_Images.Draw(iImage, Info.hdc, Rect.GetTopLeft(), 0);

					Rect.left += 20;
				}
			}

			return CDRF_SKIPDEFAULT;
		}

		if( !(Info.lItemlParam & 1) ) {

			Item.clrTextBk = afxColor(TabFace);

			return CDRF_NEWFONT;
		}

		return CDRF_DODEFAULT;
	}

	return CDRF_DODEFAULT;
}

// Command Handlers

BOOL CDevConListView::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_PROPERTIES:

			if( GetSelection() < NOTHING ) {

				Src.EnableItem(TRUE);
			}

			return TRUE;

		case IDM_EDIT_DELETE:

			if( !m_fFixed && !m_pElem->IsReadOnly() ) {

				UINT uItem = GetSelection();

				if( uItem < NOTHING && !(GetItemParam(uItem) & 0x80000000) ) {

					Src.EnableItem(TRUE);
				}
			}

			return TRUE;

		case IDM_EDIT_INSERT:

			if( !m_fFixed && !m_pElem->IsReadOnly() ) {

				if( GetSelection() == NOTHING ) {

					Src.EnableItem(TRUE);
				}
			}

			return TRUE;
	}

	return FALSE;
}

BOOL CDevConListView::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_PROPERTIES:
		case IDM_EDIT_DELETE:
		case IDM_EDIT_INSERT:

			SendNotify(uID);

			return TRUE;
	}

	return FALSE;
}

void CDevConListView::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 128);

	m_Images.AddMasked(CBitmap(L"TableIcons16"), afxColor(MAGENTA));
}

CRect CDevConListView::GetItemButtonRect(UINT uItem) const
{
	if( GetItemButtonCount(GetItemParam(uItem)) ) {

		CHeaderCtrl &Hdr = GetHeader();

		CRect Rect = GetSubItemRect(uItem, Hdr.GetItemCount() - 1, LVIR_BOUNDS);

		Rect.left  += 5;

		Rect.right -= 5;

		return Rect;
	}

	return CRect();
}

UINT CDevConListView::GetItemButtonCount(UINT uParam) const
{
	if( m_Images.GetImageCount() ) {

		return m_fFull ? 4 : 1;
	}

	return 0;
}

BOOL CDevConListView::GetItemButtonEnable(UINT uParam, UINT uButton) const
{
	if( uButton < GetItemButtonCount(uParam) ) {

		if( uButton == 0 ) {

			return TRUE;
		}

		if( !(uParam & 0x80000000) ) {

			if( uButton == 1 ) {

				return !m_pElem->IsReadOnly();
			}

			if( uButton == 2 ) {

				UINT uLast = GetItemParam(GetItemCount() - (m_fFixed ? 1 : 2));

				return !m_pElem->IsReadOnly() && uParam != uLast;
			}

			if( uButton == 3 ) {

				UINT uFirst = GetItemParam(0);

				return !m_pElem->IsReadOnly() && uParam != uFirst;
			}
		}
	}

	return FALSE;
}

UINT CDevConListView::GetItemButtonImage(UINT uParam, UINT uButton) const
{
	return GetItemButtonEnable(uParam, uButton) ? uButton : uButton + 5;
}

// End of File
