
#include "intern.hpp"

#include "mshawk.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MicroScan Hawk Camera Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMicroScanHawkCameraDriverOptions, CUIItem);

// Constructor

CMicroScanHawkCameraDriverOptions::CMicroScanHawkCameraDriverOptions(void)
{
	
	}

// UI Managament

void CMicroScanHawkCameraDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {
	
		}
	}

// Download Support

BOOL CMicroScanHawkCameraDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CMicroScanHawkCameraDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// MicroScan Hawk Camera Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CMicroScanHawkCameraDeviceOptions, CUIItem);

// Constructor

CMicroScanHawkCameraDeviceOptions::CMicroScanHawkCameraDeviceOptions(void)
{
	m_Camera = 0;

	m_IP     = DWORD(MAKELONG(MAKEWORD(10, 0), MAKEWORD(168, 192)));

	m_Port   = 80;

	m_Time4  = 10;

	m_Scale  = 1;

	m_Width  = 640;

	m_Height = 480;

	m_Graphic = 1;

	m_Border  = 1;

	m_Failed  = 0;
	}

// UI Managament

void CMicroScanHawkCameraDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Scale" ) {

			pWnd->EnableUI("Width", m_Scale);

			pWnd->EnableUI("Height", m_Scale);
			}

		pWnd->UpdateUI();	
		}
	}

// Download Support

BOOL CMicroScanHawkCameraDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Camera));

	Init.AddLong(LONG(m_IP));
	
	Init.AddWord(WORD(m_Port));

	Init.AddWord(WORD(m_Time4));

	Init.AddByte(BYTE(m_Scale));

	Init.AddWord(WORD(m_Width));

	Init.AddWord(WORD(m_Height));

	Init.AddByte(BYTE(m_Graphic));

	Init.AddByte(BYTE(m_Border));

	Init.AddByte(BYTE(m_Failed));

	return TRUE;
	}

// Persistence

void CMicroScanHawkCameraDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if(Name == "Device") {

			m_Camera = Tree.GetValueAsInteger();

			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// Meta Data Creation

void CMicroScanHawkCameraDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Camera);

	Meta_AddInteger(IP);

	Meta_AddInteger(Port);

	Meta_AddInteger(Time4);

	Meta_AddInteger(Scale);

	Meta_AddInteger(Width);

	Meta_AddInteger(Height);

	Meta_AddInteger(Graphic);

	Meta_AddInteger(Border);

	Meta_AddInteger(Failed);
	}

//////////////////////////////////////////////////////////////////////////
//
// MicroScan Hawk Camera Driver
//

// Instantiator

ICommsDriver *	Create_MicroScanHawkCameraDriver(void)
{
	return New CMicroScanHawkCameraDriver;
	}

// Constructor

CMicroScanHawkCameraDriver::CMicroScanHawkCameraDriver(void)
{
	m_wID		= 0x40A9;

	m_uType		= driverCamera;
	
	m_Manufacturer	= "Microscan";
	
	m_DriverName	= "Vision Hawk Camera";
	
	m_Version	= "1.00";

	m_DevRoot	= "Cam";
	
	m_ShortName	= "Vision Hawk";
	}

// Configuration

CLASS CMicroScanHawkCameraDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMicroScanHawkCameraDriverOptions);
	}

CLASS CMicroScanHawkCameraDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CMicroScanHawkCameraDeviceOptions);
	}

// Binding Control

UINT CMicroScanHawkCameraDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
