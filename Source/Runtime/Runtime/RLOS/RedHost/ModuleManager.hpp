
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ModuleManager_HPP

#define INCLUDE_ModuleManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Module Manager
//

class CModuleManager : public IModuleManager
{
	public:
		// Constructor
		CModuleManager(void);

		// Destructor
		~CModuleManager(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IModuleManager
		bool METHOD LoadClientModule(PCTXT pName);
		bool METHOD LoadClientModule(UINT &hModule, PCTXT pName);
		bool METHOD LoadClientModule(UINT &hModule, PCTXT pName, PCBYTE pData, UINT uSize);
		bool METHOD FreeClientModule(UINT hModule);
		bool METHOD FreeAllModules(void);

		// Operations
		bool FindModule(DWORD dwAddr, PTXT pName, DWORD &dwBase);

	protected:
		// Modules Calls
		typedef void (*MAINPTR)(IObjectBroker *);
		typedef void (*EXITPTR)(void);
		typedef UINT (*FINDPTR)(int *pSize);

		// Client Header
		struct CHeader
		{
			DWORD	dwEntry;
			DWORD	dwExit;
			char	sMagic[10];
			WORD    wModel;
			WORD	wMajorVersion;
			WORD	wMinorVersion;
			DWORD	dwFlags;
			DWORD	dwBase;
			DWORD	dwAddrVTab;
			DWORD	dwAddrFix1;
			DWORD	dwAddrFix2;
			DWORD	dwAddrFix3;
			DWORD	dwAddrText;
			DWORD	dwAddrData;
			DWORD	dwAddrLast;
			DWORD	dwZero;
			BYTE    bGuid[16];
			};

		// Module Structure
		struct CModule
		{
			public:
				// Data Members
				CString   m_Name;
				PCBYTE	  m_pImage;
				UINT      m_uImage;
				PBYTE     m_pData;
				CHeader * m_pHead;
				DWORD     m_dwLink;
				DWORD     m_dwBase;
				MAINPTR   m_pfnMain;
				EXITPTR   m_pfnExit;
				DWORD	  m_dwExit;

				// Operations
				bool CopyImage(void);
				bool FreeImage(void);
				bool IsValid(void);
				bool Fixup(void);

			protected:
				// Implementation
				bool FixVirt(void);
				bool FixCode(void);
				bool FixData(void);
				bool FixHead(void);
				bool FixJump(UINT n, DWORD &dwAddr);
				void Error(PCTXT pText, ...);
			};

		// Module List
		typedef CList <CModule *> CModuleList;

		// Data Members
		ULONG       m_uRefs;
		CModuleList m_List;
		PBYTE	    m_pData;
		DWORD	    m_dwBase;
		DWORD	    m_dwLink;

		// Implementation
		void Error(PCTXT pText, ...);
	};

// End of File

#endif
