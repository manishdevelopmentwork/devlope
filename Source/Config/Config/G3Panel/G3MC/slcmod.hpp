
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SLCMOD_HPP

#define INCLUDE_SLCMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "slcmap.hpp"

#include "pidloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSLCModule;
class CSLCMainWnd;

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

class CSLCModule : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSLCModule(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		CSLCMapper *m_pMapper;
		CPIDLoop   *m_pLoop;

	protected:
		// Implementation
		void AddMetaData(void);

		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module Main Window
//

class CSLCMainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSLCMainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd *	m_pMult;
		CSLCModule    *	m_pItem;

		// Overribables
		void OnAttach(void);

		// Implementation
		void AddPIDPages(void);
		void AddMapPages(void);
	};

// End of File

#endif
