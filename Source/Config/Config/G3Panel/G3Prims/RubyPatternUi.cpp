
#include "intern.hpp"

#include "RubyPatternUi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Extra Height
//

static const int extraHeight = 16;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element for Ruby Pattern
//

// Dynamic Class

AfxImplementDynamicClass(CUITextRubyPattern, CUITextEnum);

// Constructor

CUITextRubyPattern::CUITextRubyPattern(void)
{
	}

// Overridables

void CUITextRubyPattern::OnBind(void)
{
	CUITextElement::OnBind();

	m_pLib = ((CUISystem *) m_pItem->GetDatabase()->GetSystemItem())->m_pUI->m_pPatterns;
	
	AddData();
	}

// Implementation

void CUITextRubyPattern::AddData(void)
{
	UINT n = m_pLib->GetCount();

	UINT s = (GetFormat() == "P") ? 1 : 0;

	UINT f = (GetFormat() == "S") ? 9 : n;

	for( UINT p = s; p < f; p++ ) {

		CEntry Enum;

		Enum.m_Data = m_pLib->EnumCode(p);

		Enum.m_Text = m_pLib->EnumName(p);

		if( !p && f<n ) Enum.m_Text = CString(IDS_ORIGINAL_COLORS);

		// cppcheck-suppress uninitStructMember

		m_Enum.Append(Enum);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element for Ruby Pattern
//

// Dynamic Class

AfxImplementDynamicClass(CUIRubyPattern, CUIControl);

// Constructor

CUIRubyPattern::CUIRubyPattern(void)
{
	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CRubyPatternComboBox(this);
	}

// Core Overridables

void CUIRubyPattern::OnBind(void)
{
	m_Label = GetLabel() + L':';

	CUIElement::OnBind();
	}

void CUIRubyPattern::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayDropDown(m_pText, TRUE, 0, 6 + extraHeight);

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem( New CLayFormPad (m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIRubyPattern::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP         |
			     WS_VSCROLL         |
			     CBS_DROPDOWNLIST   |
			     CBS_OWNERDRAWFIXED |
			     CBS_HASSTRINGS,
			     FindComboRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, CBN_SELCHANGE);

	LoadList();
	}

void CUIRubyPattern::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(FindComboRect(), TRUE);
	}

// Data Overridables

void CUIRubyPattern::OnLoad(void)
{
	CPrimBrush *pBrush = (CPrimBrush *) m_pItem;

	m_pDataCtrl->SetFore(pBrush->m_pColor1->GetColor());

	m_pDataCtrl->SetBack(pBrush->m_pColor2->GetColor());

	m_pDataCtrl->SelectStringExact(m_pText->GetAsText());
	}

UINT CUIRubyPattern::OnSave(BOOL fUI)
{
	CString Text = m_pDataCtrl->GetWindowText();

	Text.TrimLeft();

	return StdSave(fUI, Text);
	}

// Implementation

CRect CUIRubyPattern::FindComboRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.top    = Rect.top  - 1;

	Rect.bottom = Rect.top  + 8 * Rect.cy();

	return Rect;
	}

void CUIRubyPattern::LoadList(void)
{
	m_pDataCtrl->LoadList();
	}

//////////////////////////////////////////////////////////////////////////
//
// Ruby Pattern Combo Box
//

// Dynamic Class

AfxImplementRuntimeClass(CRubyPatternComboBox, CDropComboBox);

// Constructor

CRubyPatternComboBox::CRubyPatternComboBox(CUIElement *pUI) : CDropComboBox(pUI)
{
	}

// Operations

void CRubyPatternComboBox::LoadList(void)
{
	m_pLib = ((CUISystem *) m_pUI->GetItem()->GetDatabase()->GetSystemItem())->m_pUI->m_pPatterns;

	SetRedraw(FALSE);

	SendMessage(CB_SETMINVISIBLE, 1);

	UINT n = m_pLib->GetCount();

	UINT s = (m_pUI->GetFormat() == "P") ? 1 : 0;

	UINT f = (m_pUI->GetFormat() == "S") ? 9 : n;

	for( UINT p = s; p < f; p++ ) {

		DWORD   Code = m_pLib->EnumCode(p);
		
		CString Name = m_pLib->EnumName(p);

		if( !p && f<n ) Name = CString(IDS_ORIGINAL_COLORS);

		AddString(Name, Code);
		}

	SendMessage(CB_SETMINVISIBLE, 8);

	SetRedraw(TRUE);
	}

void CRubyPatternComboBox::SetFore(COLOR Fore)
{
	m_Fore = Fore;

	Invalidate(TRUE);
	}

void CRubyPatternComboBox::SetBack(COLOR Back)
{
	m_Back = Back;

	Invalidate(TRUE);
	}

// Message Map

AfxMessageMap(CRubyPatternComboBox, CDropComboBox)
{
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxMessageEnd(CRubyPatternComboBox)
	};

// Message Handlers

void CRubyPatternComboBox::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	Item.itemHeight = DC.GetTextExtent(L"ABC").cy + 2 + extraHeight;

	DC.Deselect();
	}

void CRubyPatternComboBox::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
	CDC   DC(Item.hDC);

	CRect Rect = Item.rcItem;

	CRect Fill = Rect;

	Fill.right = Rect.left += Fill.cy();

	Fill--;

	if( Item.itemState & ODS_DISABLED ) {

		DC.SetTextColor(afxColor(3dShadow));

		DC.SetBkColor(afxColor(3dFace));

		DC.FillRect(Fill, afxBrush(3dShadow));
		}
	else {
		DC.SetBkMode(OPAQUE);

		DC.FrameRect(Fill, afxBrush(WHITE));

		m_pLib->Draw(DC, Fill, Item.itemData, m_Fore, m_Back);

		if( Item.itemState & ODS_SELECTED ) {

			DC.SetTextColor(afxColor(SelectText));

			DC.SetBkColor(afxColor(SelectBack));
			}
		else {
			DC.SetTextColor(afxColor(WindowText));

			DC.SetBkColor(afxColor(WindowBack));
			}
		}

	if( Item.itemID < NOTHING ) {

		CString Text = GetLBText(Item.itemID);

		CSize   Size = DC.GetTextExtent(Text);

		CPoint  Pos  = CPoint(8, 0);

		Pos.y += (Rect.cy() - Size.cy) / 2;

		DC.ExtTextOut( Rect.GetTopLeft() + Pos,
			       ETO_OPAQUE | ETO_CLIPPED,
			       Rect, Text
			       );
		}

	if( Item.itemState & ODS_FOCUS ) {

		DC.DrawFocusRect(Rect);
		}
	}

// End of File
