
#include "intern.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Structures

struct CIOInfo
{
	T5PTR_CODEIO	m_pDef;
	T5_DWORD	m_Data;
	};

// Data

static DWORD m_dwLast = 0;

// Prototypes

// Code

clink T5_DWORD iosGetMemorySizing(T5_PTR pDef)
{
	while( *((T5_PTWORD) pDef) ) {

		T5PTR_CODEIO pIO = (T5PTR_CODEIO) pDef;

		AfxTrace("size\t%d\n",	  pIO->wSizeOf);
		AfxTrace("key\t%d\n",	  pIO->wKey);
		AfxTrace("VA\t%d\n",	  pIO->wVA);
		AfxTrace("slot\t%d\n",	  pIO->wSlot);
		AfxTrace("subslot\t%d\n", pIO->wSubSlot);
		AfxTrace("chans\t%d\n",   pIO->wNbChan);

		AfxTrace("\n");

		T5_PTR pParams = (T5_PTR) ((T5_PTBYTE) pDef + sizeof(T5STR_CODEIO));

		pDef = (T5_PTR) ((T5_PTBYTE) pDef + pIO->wSizeOf);
		}

	return 0;
	}

clink T5_RET iosOpen(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
	Create_ProfileManager(pDB);

	m_dwLast = GetTickCount();

	g_pPM->SetData();

	return T5RET_OK;
	}

clink T5_RET iosCanHotRestart(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
	return T5RET_HOTUNSUP;
	}

clink T5_RET iosHotRestart(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
	return T5RET_HOTUNSUP;
	}

clink T5_RET iosClose(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
	if( g_pPM ) {

		delete g_pPM;

		g_pPM = NULL;
		}

	return T5RET_OK;
	}

clink T5_RET iosExchange(T5_PTR pDef, T5PTR_DB pDB, T5_PTR pMem)
{
	return T5RET_OK;
	}

// End of File
