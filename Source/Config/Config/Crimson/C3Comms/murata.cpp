
#include "intern.hpp"

#include "murata.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();


//////////////////////////////////////////////////////////////////////////
//
// Murata Mitsubishi MELSEC 4 Serial Slave Driver
//

// Instantiator

ICommsDriver *	Create_MurMelsec4SlaveDriver(void)
{
	return New CMurMelsec4SlaveDriver;
	}

// Constructor

CMurMelsec4SlaveDriver::CMurMelsec4SlaveDriver(void)
{
	m_wID		= 0x4082;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Mitsubishi Electric";
	
	m_DriverName	= "MELSEC 4 Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "MELSEC 4";
  
	AddSpaces();
	}

// Binding Control

UINT CMurMelsec4SlaveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CMurMelsec4SlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}


// Configuration

CLASS CMurMelsec4SlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMurMelsec4SlaveDriverOptions);
	}

CLASS CMurMelsec4SlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}


// Implementation

void CMurMelsec4SlaveDriver::AddSpaces(void)
{
	AddSpace(New CSpace('D', "D",  "Data Register",	 10,  0,  9999, addrWordAsWord));
	}


//////////////////////////////////////////////////////////////////////////
//
// CMurMelsec4SlaveDriverOptions
//

// Dynamic Class

AfxImplementDynamicClass(CMurMelsec4SlaveDriverOptions, CUIItem);
				   
// Constructor

CMurMelsec4SlaveDriverOptions::CMurMelsec4SlaveDriverOptions(void)
{
	m_Drop		= 0;

	m_Melsec	= 1;

	m_PC		= 0xFF;
	}

// Download Support

BOOL CMurMelsec4SlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Melsec));
	Init.AddByte(BYTE(m_PC));
	
	return TRUE;
	}

// Meta Data Creation

void CMurMelsec4SlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Melsec);
	Meta_AddInteger(PC);
	}



// End of File
