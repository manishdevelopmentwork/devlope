
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Rutex_HPP

#define INCLUDE_Rutex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CExecThread;

//////////////////////////////////////////////////////////////////////////
//
// Really Quick Mutex Object
//

class CRutex : public IMutex
{
	public:
		// Constructor
		CRutex(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IMutex
		void METHOD Free(void);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
