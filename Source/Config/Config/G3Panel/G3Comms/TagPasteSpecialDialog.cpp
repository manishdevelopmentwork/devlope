
#include "Intern.hpp"

#include "TagPasteSpecialDialog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Paste Special Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CTagPasteSpecialDialog, CStdDialog);

// Constructors

CTagPasteSpecialDialog::CTagPasteSpecialDialog(void)
{
	SetName(L"TagPasteSpecialDlg");
	}

// Attributes

UINT CTagPasteSpecialDialog::GetMode(void) const
{
	return m_uMode;
	}

// Message Map

AfxMessageMap(CTagPasteSpecialDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CTagPasteSpecialDialog)
	};

// Message Handlers

BOOL CTagPasteSpecialDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetRadioGroup(1100, 1107, 7);

	return TRUE;
	}

// Command Handlers

BOOL CTagPasteSpecialDialog::OnCommandOK(UINT uID)
{
	m_uMode = GetRadioGroup(1100, 1107);

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CTagPasteSpecialDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

// End of File
