
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c  1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ChannelConfig_HPP

#define	INCLUDE_ChannelConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "DnpStack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Channel Config Object
//

class CChannelConfig : public IDnpChannelConfig
{
	public:
		// Constructor
		CChannelConfig(IPADDR IP, IPADDR IP2, WORD wTcp, WORD wUdp, WORD wTO);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDnpChannelConfig 
		IPADDR METHOD GetIP(void);
		WORD   METHOD GetTcpPort(void);
		WORD   METHOD GetUdpPort(void);
		WORD   METHOD GetConnectionTimeout(void);
		void   METHOD SetFail(void);
			
	protected:
		// Data Members
		ULONG	m_uRefs;
		IPADDR	m_IP;
		IPADDR  m_IP2;
		WORD	m_Tcp;
		WORD	m_Udp;
		WORD	m_TO;
		BOOL    m_fFail;
	};

// End of File

#endif
