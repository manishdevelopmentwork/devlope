
#include "Intern.hpp"

#include "DeviceResTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsDeviceList.hpp"
#include "CommsManager.hpp"
#include "CommsMapData.hpp"
#include "CommsPort.hpp"
#include "CommsPortList.hpp"
#include "OptionCardItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CDeviceResTreeWnd, CStdTreeWnd);

// Constructors

CDeviceResTreeWnd::CDeviceResTreeWnd(void)
{
	m_dwStyle = m_dwStyle | TVS_HASBUTTONS;

	m_cfCode  = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfPart  = RegisterClipboardFormat(L"C3.1 Partial Address");

	m_hRack   = NULL;

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CommsTreeIcon16"), afxColor(MAGENTA));

	m_Accel1.Create(L"ResTreeAccel");
}

// IUnknown

HRESULT CDeviceResTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
}

ULONG CDeviceResTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
}

ULONG CDeviceResTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
}

// IUpdate

HRESULT CDeviceResTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		if( pItem->IsKindOf(AfxRuntimeClass(COptionCardItem)) ) {

			RefreshTree();
		}

		if( pItem->IsKindOf(AfxRuntimeClass(CCommsPort)) ) {

			RefreshTree();
		}

		if( pItem->IsKindOf(AfxRuntimeClass(CCommsDeviceList)) ) {

			RefreshTree();
		}
	}

	if( pItem->IsKindOf(AfxRuntimeClass(CCommsDevice)) ) {

		// LATER -- Could be a little more clever here...

		if( uType == updateRename ) {

			RefreshTree();
		}
	}

	return S_OK;
}

// Overridables

void CDeviceResTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	m_pComms = (CCommsManager *) m_pItem;
}

// Message Map

AfxMessageMap(CDeviceResTreeWnd, CStdTreeWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchNotify(100, TVN_KEYDOWN, OnTreeKeyDown)
	AfxDispatchNotify(100, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(100, NM_RETURN, OnTreeDblClk)
	AfxDispatchNotify(100, NM_DBLCLK, OnTreeDblClk)

	AfxMessageEnd(CDeviceResTreeWnd)
};

// Message Handlers

void CDeviceResTreeWnd::OnPostCreate(void)
{
	CStdTreeWnd::OnPostCreate();

	m_System.RegisterForUpdates(this);
}

void CDeviceResTreeWnd::OnDestroy(void)
{
	KillTree(m_hRoot);
}

void CDeviceResTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ResTreeTool"));
	}
}

// Notification Handlers

BOOL CDeviceResTreeWnd::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	switch( Info.wVKey ) {

		case VK_TAB:

			m_System.FlipToNavPane(TRUE);

			return TRUE;
	}

	return CStdTreeWnd::OnTreeKeyDown(uID, Info);
}

void CDeviceResTreeWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			if( Info.action == TVC_BYMOUSE ) {

				SetFocus();
			}

			m_hSelect = Info.itemNew.hItem;

			m_pSelect = NULL;

			NewItemSelected();
		}
	}
}

void CDeviceResTreeWnd::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	if( m_pTree->IsMouseInSelection() ) {

		if( !m_pTree->GetChild(m_hSelect) ) {

			IDataObject *pData = NULL;

			if( MakeDataObject(pData) ) {

				OleSetClipboard(pData);

				OleFlushClipboard();

				pData->Release();

				m_System.SendPaneCommand(IDM_EDIT_PASTE);

				m_System.FlipToItemView(FALSE);
			}

			return;
		}

		ToggleItem(m_hSelect);
	}
}

// Data Object Construction

BOOL CDeviceResTreeWnd::MakeDataObject(IDataObject * &pData)
{
	CCommsMapData *pMap = (CCommsMapData *) m_pTree->GetItemParam(m_hSelect);

	if( pMap ) {

		CDataObject *pMake = New CDataObject;

		if( pMap->m_fPart ) {

			CString Text;

			Text  = m_pItem->GetDatabase()->GetUniqueSig();

			Text += L'|';

			Text += CPrintf(L"%8.8X", pMap);

			pMake->AddText(m_cfPart, Text);
		}
		else {
			CString Text;

			if( m_pComms->ResolveMapData(Text, pMap) ) {

				pMake->AddText(m_cfCode, Text);
			}
		}

		if( !pMake->IsEmpty() ) {

			pData = pMake;

			return TRUE;
		}

		delete pMake;

		pData = NULL;
	}

	return FALSE;
}

// Tree Loading

void CDeviceResTreeWnd::LoadTree(void)
{
	LoadRoot();

	CCommsPortList *pPorts;

	for( UINT s = 0; m_pComms->GetPortList(pPorts, s); s++ ) {

		LoadPorts(pPorts);
	}

	LoadNull(m_hRoot);

	ExpandItem(m_hRoot);

	m_pTree->SelectItem(m_hRoot);
}

void CDeviceResTreeWnd::LoadRoot(void)
{
	CTreeViewItem Node;

	Node.SetText(CString(IDS_DEVICES));

	Node.SetParam(NULL);

	Node.SetImages(IDI_COMMS);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Node);

	m_hRack = NULL;
}

void CDeviceResTreeWnd::LoadRack(void)
{
	CTreeViewItem Node;

	Node.SetText(CString(IDS_MODULES));

	Node.SetParam(NULL);

	Node.SetImages(IDI_RACK_GRAPHITE);

	m_hRack = m_pTree->InsertItem(m_hRoot, NULL, Node);
}

void CDeviceResTreeWnd::LoadNull(HTREEITEM hRoot)
{
	CCommsMapData *pData = New CCommsMapData;

	pData->m_pDevice     = NULL;

	pData->m_Addr.m_Ref  = 0;

	pData->m_fPart       = FALSE;

	CTreeViewItem Node;

	Node.SetText(CString(IDS_COMMS_MAP_NONE));

	Node.SetParam(LPARAM(pData));

	Node.SetImages(IDI_NOTHING);

	m_pTree->InsertItem(hRoot, NULL, Node);
}

void CDeviceResTreeWnd::LoadPorts(CCommsPortList *pPorts)
{
	for( INDEX n = pPorts->GetHead(); !pPorts->Failed(n); pPorts->GetNext(n) ) {

		CCommsPort   *pPort   = pPorts->GetItem(n);

		ICommsDriver *pDriver = pPort->GetDriver();

		if( pDriver ) {

			if( pDriver->GetType() == driverMaster ) {

				CCommsDeviceList *pDevs = pPort->m_pDevices;

				for( INDEX m = pDevs->GetHead(); !pDevs->Failed(m); pDevs->GetNext(m) ) {

					CCommsDevice *pDev = pDevs->GetItem(m);

					if( pDriver->GetID() == 0xFE01 ) {

						if( !m_hRack ) {

							LoadRack();
						}

						LoadDevice(m_hRack, pDev);
					}
					else {
						LoadDevice(m_hRoot, pDev);
					}
				}
			}
		}
	}
}

void CDeviceResTreeWnd::LoadDevice(HTREEITEM hRoot, CCommsDevice *pDevice)
{
	ICommsDriver * pDriver = pDevice->GetDriver();

	if( !(pDriver->GetFlags() & dflagNoMapping) ) {

		CTreeViewItem Node;

		Node.SetText(pDevice->m_Name);

		Node.SetParam(NULL);

		if( pDriver->GetID() == 0xFE01 ) {

			if( m_pTree->GetChild(hRoot) ) {

				if( m_pComms->GetDatabase()->GetSystemItem()->GetModel().StartsWith(L"da") ) {

					Node.SetImages(IDI_MODULE_MANTICORE);
				}
				else {
					Node.SetImages(IDI_MODULE_GRAPHITE);
				}
			}
			else {
				if( m_pComms->GetDatabase()->GetSystemItem()->GetModel().StartsWith(L"da") ) {

					Node.SetImages(IDI_MASTER_MANTICORE);
				}
				else {
					Node.SetImages(IDI_MASTER_GRAPHITE);
				}
			}
		}
		else
			Node.SetImages(IDI_PLC_ENABLED);

		HTREEITEM hNode = m_pTree->InsertItem(hRoot, NULL, Node);

		afxThread->SetWaitMode(TRUE);

		LoadDriver(hNode, pDevice, NULL);

		afxThread->SetWaitMode(FALSE);
	}
}

BOOL CDeviceResTreeWnd::LoadDriver(HTREEITEM hRoot, CCommsDevice *pDevice, CAddrData *pRoot)
{
	ICommsDriver *pDriver = pDevice->GetDriver();

	if( pDriver ) {

		CItem *pConfig = pDevice->GetConfig();

		UINT n;

		for( n = 0;; n++ ) {

			CAddrData AddrData;

			AddrData.m_Addr.m_Ref = 0;

			if( pDriver->ListAddress(pRoot, pConfig, n, AddrData) ) {

				CCommsMapData *pData  = New CCommsMapData;

				pData->m_pDevice = pDevice;

				pData->m_Addr    = AddrData.m_Addr;

				pData->m_fPart   = AddrData.m_fPart;

				CTreeViewItem Node;

				Node.SetText(AddrData.m_Name);

				Node.SetParam(LPARAM(pData));

				Node.SetImages(FindAddrImage(pDriver, pConfig, AddrData.m_Addr));

				HTREEITEM hNode = m_pTree->InsertItem(hRoot, NULL, Node);

				LoadDriver(hNode, pDevice, &AddrData);

				continue;
			}

			break;
		}

		return n > 0;
	}

	return FALSE;
}

// Drag Hooks

DWORD CDeviceResTreeWnd::FindDragAllowed(void)
{
	return DROPEFFECT_COPY | DROPEFFECT_LINK;
}

// Item Hooks

void CDeviceResTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	delete (CCommsMapData *) m_pTree->GetItemParam(hItem);
}

// Selection Hooks

CString CDeviceResTreeWnd::SaveSelState(void)
{
	// LATER -- Is there a way to implement this?

	return L"";
}

void CDeviceResTreeWnd::LoadSelState(CString State)
{
	SelectRoot();
}

// Implementation

UINT CDeviceResTreeWnd::FindAddrImage(ICommsDriver *pDriver, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		UINT uImage = 0;

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:

				uImage = IDI_YELLOW_FLAG;

				break;

			case addrBitAsByte:
			case addrByteAsByte:
			case addrBitAsWord:
			case addrByteAsWord:
			case addrWordAsWord:
			case addrBitAsLong:
			case addrByteAsLong:
			case addrWordAsLong:
			case addrLongAsLong:

				uImage = IDI_YELLOW_INTEGER;

				break;

			case addrBitAsReal:
			case addrByteAsReal:
			case addrWordAsReal:
			case addrLongAsReal:
			case addrRealAsReal:

				uImage = IDI_YELLOW_FLOAT;

				break;
		}

		if( pDriver->IsReadOnly(pConfig, Addr) ) {

			uImage -= IDI_YELLOW_FLAG;

			uImage += IDI_GREEN_FLAG;
		}

		return uImage;
	}

	return IDI_DOT;
}

// End of File
