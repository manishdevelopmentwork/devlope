
#include "intern.hpp"

#include "bac8023s.hpp"

//////////////////////////////////////////////////////////////////////////
//
// BACNet IEEE 802.3 Slave
//

// Instantiator

INSTANTIATE(CBACNet8023Slave);

// Constructor

CBACNet8023Slave::CBACNet8023Slave(void)
{
	m_Ident = DRIVER_ID;

	m_pSock = NULL;
	}

// Destructor

CBACNet8023Slave::~CBACNet8023Slave(void)
{
	}

// Configuration

void MCALL CBACNet8023Slave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_pCtx  = new CContext;

		m_pBase = m_pCtx;

		LoadConfig(pData);
		}
	}
	
// Management

void MCALL CBACNet8023Slave::Attach(IPortObject *pPort)
{
	m_pSock = CreateSocket(IP_RAW);
	}

void MCALL CBACNet8023Slave::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Release();

		m_pSock = NULL;
		}
	}

void MCALL CBACNet8023Slave::Open(void)
{
	if( m_pSock ) {

		m_pSock->Listen(0x82);
		}	

	CBACNetSlave::Open();
	}

// User Access

UINT MCALL CBACNet8023Slave::DrvCtrl(UINT uFunc, PCTXT Value)
{	
	UINT uValue = ATOI(Value);

	switch( uFunc ) {

		case 2:	// Set Device ID
			
			if( uValue <= 4194303 ) {

				FreeObjectList();

				m_pCtx->m_Device = uValue;

				BuildObjectList();

				return OnSendIAm() ? 1 : 0;
				}

			break;	

		case 3:	// Get Device ID

			return m_pCtx->m_Device;

		}

	return 0;
	}

// Transport Hooks

BOOL CBACNet8023Slave::SendFrame(BOOL fThis, BOOL fReply)
{
	if( m_pSock ) {

		Dump("tx", m_pTxBuff);

		AddNetworkHeader  (fThis, fReply);

		AddTransportHeader(fThis);

		for( UINT n = 0; n < 5; n++ ) {

			if( m_pSock->Send(m_pTxBuff) == S_OK ) {

				m_pTxBuff = NULL;

				return TRUE;
				}

			Sleep(10);
			}

		Show("send-frame", "can't send");
		}

	m_pTxBuff->Release();

	m_pTxBuff = NULL;

	return FALSE;
	}

BOOL CBACNet8023Slave::RecvFrame(void)
{
	if( m_pSock ) {

		SetTimer(5000);

		while( GetTimer() ) {

			m_pSock->Recv(m_pRxBuff);

			if( m_pRxBuff ) {

				if( StripTransportHeader() ) {

					if( StripNetworkHeader() ) {

						Dump("rx", m_pRxBuff);

						return TRUE;
						}
					}

				RecvDone();
				}

			Sleep(10);

			continue;
			}

		return FALSE;
		}

	Sleep(FOREVER);

	return FALSE;
	}

// Transport Header

void CBACNet8023Slave::AddTransportHeader(BOOL fThis)
{
	UINT  uData = m_pTxBuff->GetSize();

	UINT  uSize = 17;

	PBYTE pData = m_pTxBuff->AddHead(uSize);

	if( fThis ) {
	
		memcpy(pData + 0, m_RxMac.m_MAC, 6);

		memset(pData + 6, 0, 6);
		}
	else {
		memset(pData + 0, 0xFF, 6);

		memset(pData + 6, 0, 6);
		}

	pData[12] = HIBYTE(uData + 3);

	pData[13] = LOBYTE(uData + 3);

	pData[14] = 0x82;

	pData[15] = 0x82;

	pData[16] = 0x03;
	}

BOOL CBACNet8023Slave::StripTransportHeader(void)
{
	PBYTE pData = m_pRxBuff->GetData();

	if( pData[14] == 0x82 ) {
		
		if( pData[15] == 0x82 ) {

			if( pData[16] == 0x03 ) {

				memcpy(m_RxMac.m_MAC, pData + 6, 6);

				m_pRxBuff->StripHead(17);

				return TRUE;
				}
			}
		}

	Show("strip", "invalid transport header");

	return FALSE;
	}

// End of File
