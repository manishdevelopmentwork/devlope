 
#include "intern.hpp"

#include "beckplus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBeckhoffPlusTagsTCPDeviceOptions, CUIItem);

// Constructor

CBeckhoffPlusTagsTCPDeviceOptions::CBeckhoffPlusTagsTCPDeviceOptions(void)
{
	}

// Attributes

CString CBeckhoffPlusTagsTCPDeviceOptions::GetDeviceName(void)
{
	return ((CNamedItem *) GetParent())->m_Name;
	}

// Operations

BOOL CBeckhoffPlusTagsTCPDeviceOptions::CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fNew)
{
	INDEX Index;

	BOOL fUsed = FALSE;

	BOOL fNotI = IsNotIndexed(Addr.a.m_Table);

	if( fNotI ) {

		Index = m_TagsRevMap.FindName(Addr.m_Ref);

		fUsed = !m_TagsRevMap.Failed(Index);
		}

	if( !fUsed ) {

		Index = m_TagsFwdMap.FindName(Name);

		fUsed = !m_TagsFwdMap.Failed(Index);
		}

	if( fUsed ) {

		Error.Set(CString(STRUSED), 0);

		return FALSE;
		}

	if( fNew ) {

		if( !IsFixed(Addr.a.m_Table) ) {

			Addr.a.m_Offset = fNotI ? 0 : CreateIndex();
			Addr.a.m_Extra  = 0;
			}
		}
	
	m_TagsFwdMap.Insert(Name, Addr.m_Ref);
	
	m_TagsRevMap.Insert(Addr.m_Ref, Name);
	
	return TRUE;
	}

void CBeckhoffPlusTagsTCPDeviceOptions::DeleteTag(CAddress const &Addr, BOOL fKill)
{
	INDEX Index = m_TagsFwdMap.FindData(Addr.m_Ref);

	if( !m_TagsFwdMap.Failed(Index) ) {

		CString Name = m_TagsFwdMap.GetName(Index);
		
		m_TagsFwdMap.Remove(Name);

		m_TagsRevMap.Remove(Addr.m_Ref);

		if( fKill ) {

			UINT uIndex = GetIndex(Addr);
		
			m_AddrArray.Append(uIndex);
			}
		}
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::RenameTag(CString const &Name, CAddress const &Addr)
{
	CString Old;

	ExpandAddress(Old, Addr);
	
	DeleteTag(Addr, FALSE);

	CError Error(TRUE);

	if( !CreateTag(Error, Name, Addr, FALSE) ) {
		
		Error.Show(*afxMainWnd);

		CreateTag(Error, Old, Addr, FALSE);

		return FALSE;
		}

	return TRUE;
	}

CString CBeckhoffPlusTagsTCPDeviceOptions::CreateName(CAddress Addr)
{
	UINT uT        = Addr.a.m_Table;

	CString Type   = GetVarType(Addr.a.m_Type);
	CString Text   = GetDefVarText(uT);
	CString Device = GetDeviceName();

	if( IsFixed(uT) ) {

		Text.Printf("%s.%s%s%4.4X", Device, Type, Text, Addr.a.m_Offset);
		return Text;
		}

	else if( IsNotIndexed(uT) ) {

		Text.Printf("%s.%s%s", Device, Type, Text);
		return Text;
		}

	Text.Printf("%s.%s%sV%s", Device, Type, Text, "%u");

	return CreateName(Text);
	}

void CBeckhoffPlusTagsTCPDeviceOptions::ListTags(CTagDataArray &List)
{
	INDEX Index = m_TagsFwdMap.GetHead();

	while( !m_TagsFwdMap.Failed(Index) ) {

		CTagData Data;

		Data.m_Name = m_TagsFwdMap.GetName(Index);
		
		Data.m_Addr = m_TagsFwdMap.GetData(Index);

		List.Append(Data);
		
		m_TagsFwdMap.GetNext(Index);
		}
	}

// Address Management

BOOL CBeckhoffPlusTagsTCPDeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CString Name = Text;

	INDEX  Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		Addr.m_Ref = m_TagsFwdMap.GetData(Index);

		if( UsesName(Addr.a.m_Table) ) {

			for( UINT i = 0; i < m_AddrDict.GetCount(); i ++ ) {

				if( InDictionary(Addr.m_Ref) ) return TRUE;
				}

			m_AddrDict.Append(Addr.m_Ref);
			}

// Debugging Verification
		for(UINT k = 0; k < m_AddrDict.GetCount(); k ++ ) {

			UINT d = m_AddrDict[k];

			if( !d ) continue;
			}

		return TRUE;
		}

	//AfxTrace("name not in fwd map <%s>\n", Text);

	return FALSE;
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	CAddress Find = Addr;

	INDEX Index = m_TagsRevMap.FindName(Find.m_Ref);

	if( !m_TagsRevMap.Failed(Index) ) {

		Text = m_TagsRevMap.GetData(Index);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CBeckhoffPlusTagsTCPDriver Driver;
	
	CBeckhoffPlusTagsTCPDialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	Title = CPrintf("Select Variable for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			m_List.Empty();
			
			ListTags(m_List);

			if( !m_List.GetCount() ) {

				return FALSE;
				}
			}
		else {
			if( uItem >= m_List.GetCount() ) {

				return FALSE;
				}
			}
		
		CTagData &TagData = (CTagData &) m_List[uItem];

		CAddress Addr = (CAddress &) TagData.m_Addr;
			
		Data.m_Name   = TagData.m_Name;
		Data.m_Addr   = Addr;
		Data.m_fPart  = FALSE;
		Data.m_Type   = Addr.a.m_Type;

		return TRUE;
		}

	return FALSE;
	}

// UI Management

void CBeckhoffPlusTagsTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Push" ) {

			switch( m_Push ) {

				case 0:
					OnManage(pWnd);
					break;

				case 1:
					OnImport(pWnd);
					break;

				case 2:
					OnExport(pWnd);
					break;
				}

			return;
			}
		}
	}

// Persistance

void CBeckhoffPlusTagsTCPDeviceOptions::TagInit(void)
{
	CUIItem::Init();
	}

void CBeckhoffPlusTagsTCPDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Tags" ) {

			Tree.GetObject();

			LoadTags(Tree);

			Tree.EndObject();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CBeckhoffPlusTagsTCPDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);	

	if( m_TagsFwdMap.GetCount() ) {

		Tree.PutObject("Tags");

		SaveTags(Tree);

		Tree.EndObject();
		}
	}

// Download Support

void CBeckhoffPlusTagsTCPDeviceOptions::PrepareData()
{
	m_Tags.Empty();
	m_PrepAddr.Empty();

	CTagDataArray List;

	ListTags(List);

	List.Sort();

	for( UINT i = 0; i < List.GetCount(); i ++ ) {

		CTagData &Data = (CTagData &) List[i];

		if( InDictionary(Data.m_Addr) ) {

			CString Name = Data.m_Name;
			DWORD   Addr = Data.m_Addr;

			if( !StripDeviceName(&Name) ) {
			// Set Addr.a.m_Extra to not prefix Dev. Name in runtime
				Addr += 0x01000000;
				}

			m_Tags.Append(Name);
			m_PrepAddr.Append(Addr);
			}
		}
	}

// PrepareData Helper

BOOL CBeckhoffPlusTagsTCPDeviceOptions::StripDeviceName(CString *pName)
{
	CString Name = *pName;

	UINT uFind   = Name.Find('.');

	if( uFind < NOTHING ) {

		*pName = Name.Right(Name.GetLength() - uFind - 1);

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	UINT uVarCount = m_Tags.GetCount();

	if( uVarCount ) {

		TagInit();

		Init.AddWord(0xABCD);

		Init.AddText(GetDeviceName());

		Init.AddWord(WORD(uVarCount));

		for( UINT i = 0; i < uVarCount; i ++ ) {

			Init.AddLong(m_PrepAddr[i]);
			}

		UINT  uTotalSize = 0;
	
		CArray <CString> m_NameData;

		for( i = 0; i < uVarCount; i ++ ) {

			CString Name = m_Tags[i];

			uTotalSize  += Name.GetLength() + 1; // Null added to end

			m_NameData.Append(Name);
			}

		UINT uSize = m_NameData.GetCount();

		if( (uSize == uVarCount) && uTotalSize ) {

			Init.AddWord(WORD(uTotalSize));

			for( i = 0; i < uSize; i ++ ) {

				Init.AddText(m_NameData[i]);
				};

			return TRUE;
			}
		}
	
	return FALSE;
	}

// Meta Data Creation

void CBeckhoffPlusTagsTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	
	Meta_AddInteger(Push);
	}

// Property Save Filter

BOOL CBeckhoffPlusTagsTCPDeviceOptions::SaveProp(CString Tag) const
{
	if( Tag == "Push" ) {
		
		return FALSE;
		}

	return CUIItem::SaveProp(Tag);
	}

// Implementation

void CBeckhoffPlusTagsTCPDeviceOptions::OnManage(CWnd *pWnd)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	CBeckhoffPlusTagsTCPDriver Driver;
	
	CBeckhoffPlusTagsTCPDialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Variable Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

void CBeckhoffPlusTagsTCPDeviceOptions::OnImport(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath("Beckhoff Plus");

	Dlg.SetCaption(CPrintf("Import Tags for %s", GetDeviceName()));

	Dlg.SetFilter("CSV Files|*.csv");

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( !(pFile = fopen(Dlg.GetFilename(), "rt")) ) {

			CString Text = "Unable to open file for reading.";

			pWnd->Error(Text);
			}
		else {
			if( m_TagsFwdMap.GetCount() ) {
				
				CString Text = "All tags will be replaced with data from the selected file.\n\nDo you want to continue?";

				if( pWnd->YesNo(Text) == IDYES ) {

					m_TagsFwdMap.Empty();
					
					m_TagsRevMap.Empty();
				
					m_AddrArray.Empty();
					}
				else {
					fclose(pFile);
					
					return;
					}
				}

			afxThread->SetWaitMode(TRUE);

			if( Import(pFile) ) {

				SetDirty();				

				CString Text =  "You have made changes to the names that could \n"
						"invalidate references in the database.\n\n"
						"You must execute Validate All Tags \n"
						"from the Data Tags window.";

				pWnd->Information(Text);

				Dlg.SaveLastPath("Beckhoff Plus");

				m_AddrDict.Empty();
				}

			else ShowErrors(pWnd);
			
			fclose(pFile);

			afxThread->SetWaitMode(FALSE);
			}
		}
	}

void CBeckhoffPlusTagsTCPDeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath("Beckhoff Plus");

	Dlg.SetCaption(CPrintf("Export Tags for %s", GetDeviceName()));

	Dlg.SetFilter ("CSV Files|*.csv");

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), "rb") ) {
			
			fclose(pFile);

			CString Text = "The selected file already exists.\n\n"
				       "Do you want to overwrite it?";

			if( pWnd->YesNo(Text) == IDNO ) {
			
				fclose(pFile);

				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), "wt")) ) {

			CString Text = "Unable to open file for writing.";

			pWnd->Error(Text);

			return;
			}

		Export(pFile);

		fclose(pFile);

		Dlg.SaveLastPath("Beckhoff Plus");
		}
	}

CString CBeckhoffPlusTagsTCPDeviceOptions::CreateName(PCTXT pFormat)
{
	for( UINT n = 1;; n ++ ) {

		CPrintf Name(pFormat, n);

		INDEX Index = m_TagsFwdMap.FindName(Name);

		if( m_TagsFwdMap.Failed(Index) ) {
			
			return Name;
			}
		}
	}

UINT CBeckhoffPlusTagsTCPDeviceOptions::CreateIndex(void)
{
	if( m_AddrArray.GetCount() ) {

		UINT n = m_AddrArray[0];

		m_AddrArray.Remove(0);

		return n;
		}

	CTagDataArray List;

	ListTags(List);

	List.Sort();

	UINT uIndex = 0;

	UINT uCount = List.GetCount();

	for( UINT i = 0; i < uCount; i ++ ) {

		CTagData &Data = (CTagData &) List[i];

		if( uIndex != GetIndex((CAddress &) Data.m_Addr) ) {
			
			break;
			}

		uIndex ++;
		}

	return uIndex;
	}

CString CBeckhoffPlusTagsTCPDeviceOptions::GetVarType(UINT uType)
{
	switch( uType ) {

		case addrByteAsByte:	return "b";
		case addrWordAsWord:	return "w";
		case addrLongAsLong:	return "d";
		case addrRealAsReal:	return "r";
		}

	return "";
	}

CString CBeckhoffPlusTagsTCPDeviceOptions::GetDefVarText(UINT uTable)
{
	switch( uTable ) {

		case SPFMEM:	return "FM";
		case SPFI:	return "FI";
		case SPFO:	return "FO";
		case SPINP:	return "I";
		case SPOUT:	return "O";
		case SPRAS:	return "RAS";
		case SPRDS:	return "RDS";
		case SPWC:	return "WC";
		case SPWCA:	return "WCA";
		case SPWCD:	return "WCD";
		}

	return "M";
	}

void CBeckhoffPlusTagsTCPDeviceOptions::SaveTags(CTreeFile &Tree)
{
	CTagDataArray List;
	
	ListTags(List);

	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		Tree.PutValue("Name", List[n].m_Name);

		Tree.PutValue("Data", List[n].m_Addr);
		}
	}

void CBeckhoffPlusTagsTCPDeviceOptions::LoadTags(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Name" ) {
			
			CString Name = Tree.GetValueAsString();

			Tree.GetName();

			DWORD Data = Tree.GetValueAsInteger();

			m_TagsFwdMap.Insert(Name, Data);

			m_TagsRevMap.Insert(Data, Name);
			
			continue;
			}
		}
	}

CString CBeckhoffPlusTagsTCPDeviceOptions::GetTagName(CString const &Text)
{
	return Text;
	}

UINT CBeckhoffPlusTagsTCPDeviceOptions::GetIndex(CAddress const &Addr)
{
	return Addr.a.m_Offset;
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::IsNotIndexed(UINT uTable)
{
	switch( uTable ) {

		case SPRAS:
		case SPRDS:
		case SPWC:
		case SPWCA:
		case SPWCD:	return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::IsFixed(UINT uTable)
{
	switch( uTable ) {

		case SPFMEM:
		case SPFI:
		case SPFO:	return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::UsesName(UINT uTable)
{
	return !(IsNotIndexed(uTable) || IsFixed(uTable));
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::InDictionary(DWORD m_Ref)
{
	for( UINT i = 0; i < m_AddrDict.GetCount(); i ++ ) {

		if( m_Ref == m_AddrDict[i] ) return TRUE;
		}

	return FALSE;
	}

// Import/Export Support

void CBeckhoffPlusTagsTCPDeviceOptions::Export(FILE *pFile)
{
	CTagDataArray List;

	ListTags(List);

	List.Sort();

	fprintf(pFile, "%s%s\n", STRHDR, GetDeviceName());

	fprintf(pFile, "NAME,TABLE,INDEX,TYPE\n");

	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		CString  Name = List[n].m_Name;

		CAddress Addr = (CAddress &) List[n].m_Addr;

		CString Type;

		ExpandType(Type, Addr);

		fprintf(pFile, "%s,%d,%d,%s\n", 
				Name,
				Addr.a.m_Table,
				Addr.a.m_Offset,
				Type
				);
		}
	}

BOOL CBeckhoffPlusTagsTCPDeviceOptions::Import(FILE *pFile)
{
	m_Errors.Empty();

	DWORD dwPos = ftell(pFile);

	UINT uLine = 0;

	fseek(pFile, dwPos, SEEK_SET);

	while( !feof(pFile) ) {

		char sLine[256] = {0};

		fgets(sLine, sizeof(sLine), pFile);

		if( !uLine ) {

			sLine[strlen(sLine)-1] = 0;

			CString ID  = CString(sLine, strlen(sLine));

			CString Hdr = STRHDR;

			UINT hlen   = Hdr.GetLength();

			if( ID.Left(hlen) != STRHDR ) {

				return FALSE;
				}

			fgets(sLine, sizeof(sLine), pFile); // "NAME,TABLE,INDEX,TYPE"

			fgets(sLine, sizeof(sLine), pFile); // First Entry

			uLine = 2;
			}

		if( sLine[0] ) {

			uLine ++;

			CStringArray List;

			sLine[strlen(sLine)-1] = 0;

			CString(sLine).Tokenize(List, ',');

			if( List.GetCount() == 4 ) {
					     
				CString Name  = List[0];

				CString Table = List[1];

				CString Index = List[2];

				CString Type = List[3];

				Name.Remove('"');
				Table.Remove('"');
				Index.Remove('"');
				Type.Remove('"');

				if(	Name.IsEmpty()  ||
					Table.IsEmpty()	||
					Index.IsEmpty() ||
					Type.IsEmpty()
					) {

					m_Errors.Append(uLine);

					continue;
					}
					
				CAddress Addr;

				Addr.a.m_Offset = tatoi(Index);
				Addr.a.m_Extra  = 0;
				Addr.a.m_Table  = tatoi(Table);

				ParseType(Addr, Type);

				if( !CreateTag(CError(FALSE), Name, Addr, TRUE) ) {
						
					if( m_Errors.Find(uLine) == NOTHING ) {

						m_Errors.Append(uLine);
						}
					}
				}
			}
		}

	return m_Errors.GetCount() == 0;
	}

void CBeckhoffPlusTagsTCPDeviceOptions::ParseType(CAddress &Addr, CString Text)
{
	UINT uType = addrWordAsWord;

	Text.MakeUpper();

	switch( Text[0] ) {

		case 'L':
			uType = addrLongAsLong;
			break;

		case 'R':
			uType = addrRealAsReal;
			break;

		case 'B':
			uType = Text[1] == 'Y' ? addrByteAsByte : addrBitAsBit;
			break;
		}

	Addr.a.m_Type = uType;
	}

void CBeckhoffPlusTagsTCPDeviceOptions::ExpandType(CString &Text, CAddress const &Addr)
{
	switch( Addr.a.m_Table ) {

		case SPRAS:
		case SPRDS:
		case SPWCA:
		case SPWCD: Text = "WORD";	return;

		case SPWC:  Text = "BIT";	return;

		case SPFMEM:
		case SPFI:
		case SPFO:
		case SPMEM:
		case SPINP:
		case SPOUT:
			switch( Addr.a.m_Type ) {

				case addrBitAsBit:	Text = "BIT"; return;
				case addrByteAsByte:	Text = "BYTE"; return;
				case addrWordAsWord:	Text = "WORD"; return;
				case addrRealAsReal:	Text = "REAL"; return;
				default:		Text = "LONG"; return;
				}
			return;
		}

	Text = "";
	}

void CBeckhoffPlusTagsTCPDeviceOptions::ShowErrors(CWnd *pWnd)
{
	if( m_Errors.GetCount() ) {
		
		afxThread->SetWaitMode(TRUE);
		
		CString Lines;

		UINT uErrMax = 101;
		
		for( UINT i = 0; i < m_Errors.GetCount(); ) {

			if( i )
				Lines += ", ";
			
			Lines += CPrintf("%d", m_Errors[i]);

			if( ++ i >= uErrMax ) {

				CString Text = "...\nFile contains more than %d errors";
				
				Lines += CPrintf(Text, uErrMax);

				break;
				}
			}

		afxThread->SetWaitMode(FALSE);

		CString Text = "The following lines in the file contained errors :\n\n%s."
			       "\n\nThe lines without errors were still imported.";

		pWnd->Error(CPrintf(Text, Lines));
		}
	else {
		CString Text = "The operation completed without error.";

		pWnd->Information(Text);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Plus TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBeckhoffPlusTCPDeviceOptions, CBeckhoffPlusTagsTCPDeviceOptions);

// Constructor

CBeckhoffPlusTCPDeviceOptions::CBeckhoffPlusTCPDeviceOptions(void)
{
	m_AMSPort	= 800;

	m_Addr		= DWORD(MAKELONG(MAKEWORD(20, 21), MAKEWORD(16, 172) ));

	m_Socket	= 48898;

	m_Keep		= TRUE;

	m_Time1		= 5000;

	m_Time2		= 2500;

	m_Time3		= 200;
	}

// UI Management

void CBeckhoffPlusTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}

	CBeckhoffPlusTagsTCPDeviceOptions::OnUIChange(pWnd, pItem, Tag);
	}

// Download Support

BOOL CBeckhoffPlusTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_AMSPort));
	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	CBeckhoffPlusTagsTCPDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBeckhoffPlusTCPDeviceOptions::AddMetaData(void)
{
	CBeckhoffPlusTagsTCPDeviceOptions::AddMetaData();

	Meta_AddInteger(AMSPort);
	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

// End of File
