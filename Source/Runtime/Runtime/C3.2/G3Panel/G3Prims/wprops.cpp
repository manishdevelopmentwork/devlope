
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property Data
//

// Constructor

CPrimWidgetPropData::CPrimWidgetPropData(void)
{
	m_pList = New CPrimWidgetPropList;
	}

// Destructor

CPrimWidgetPropData::~CPrimWidgetPropData(void)
{
	delete m_pList;
	}

// Initialization

void CPrimWidgetPropData::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimWidgetPropData", pData);

	m_pList->Load(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property List
//

// Constructor

CPrimWidgetPropList::CPrimWidgetPropList(void)
{
	m_uCount = 0;

	m_ppProp = NULL;
	}

// Destructor

CPrimWidgetPropList::~CPrimWidgetPropList(void)
{
	while( m_uCount-- ) {

		delete m_ppProp[m_uCount];
		}

	delete m_ppProp;
	}

// Initialization

void CPrimWidgetPropList::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimWidgetPropList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppProp = New CPrimWidgetProp * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CPrimWidgetProp *pProp = NULL;

			if( GetByte(pData) ) {

				pProp = New CPrimWidgetProp;
				}

			if( (m_ppProp[n] = pProp) ) {

				pProp->Load(pData);
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property
//

// Constructor

CPrimWidgetProp::CPrimWidgetProp(void)
{
	m_Type   = 1;

	m_Flags  = 0;

	m_pValue = NULL;

	m_Data   = 0;
	}

// Destructor

CPrimWidgetProp::~CPrimWidgetProp(void)
{
	if( m_Type == 2 ) {

		if( m_Data ) {

			Free(PUTF(m_Data));
			}
		}

	delete m_pValue;
	}

// Initialization

void CPrimWidgetProp::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimWidgetProp", pData);

	m_Type  = GetByte(pData);

	m_Flags = GetByte(pData);

	GetCoded(pData, m_pValue);
	}

// Attributes

BOOL CPrimWidgetProp::HasLocal(void) const
{
	if( m_Flags & 2 ) {

		if( m_Flags & 1 ) {

			return FALSE;
			}

		if( m_Flags & 4 ) {

			return FALSE;
			}

		if( m_Flags & 8 ) {

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

DWORD CPrimWidgetProp::GetData(UINT Type) const
{
	if( Type == typeString ) {

		if( m_Type == 2 ) {

			if( m_Data ) {

				return DWORD(wstrdup(PUTF(m_Data)));
				}
			}

		return DWORD(wstrdup(L""));
		}

	if( Type == typeInteger && m_Type == 1 ) {

		return C3INT(I2R(m_Data));
		}

	if( Type == typeReal    && m_Type == 0 ) {

		return R2I(C3REAL(C3INT(m_Data)));
		}

	return m_Data;
	}

// Operations

BOOL CPrimWidgetProp::SetData(DWORD Data, UINT Type)
{
	if( Type == typeString ) {

		if( m_Type == 2 ) {

			if( m_Data ) {

				Free(PUTF(m_Data));
				}

			m_Data = DWORD(wstrdup(PUTF(Data)));
			}

		return TRUE;
		}

	if( Type == typeInteger && m_Type == 1 ) {

		Data = R2I(C3REAL(C3INT(Data)));
		}

	if( Type == typeReal    && m_Type == 0 ) {

		Data = C3INT(I2R(Data));
		}

	m_Data = Data;

	return TRUE;
	}

// End of File
