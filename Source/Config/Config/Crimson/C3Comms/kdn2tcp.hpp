#include "s7tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_KDN2TCP_HPP
	
#define	INCLUDE_KDN2TCP_HPP

//////////////////////////////////////////////////////////////////////////
//
// KEB Master Defs
//

#define ETD	0x1 << 2
#define STD	0x2 << 2
#define LMT	0x3 << 2
#define DEF	0x4 << 2
#define CHR	0x5 << 2
#define DSP	0x6 << 2
#define NAM	0x7 << 2
#define PLI	0x8 << 2
#define PD1    0x11 << 2
#define PD2    0x12 << 2
#define PD3    0x13 << 2

#define ERR	0xF << 4


//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 Space Wrapper
//

class CSpaceKEBDN2 : public CSpace
{
	public:
		// Constructors
		CSpaceKEBDN2(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s);

		// Limits
		void GetMaximum(CAddress &Addr);
	};



//////////////////////////////////////////////////////////////////////////
//
// KEB DIN2 Master TCP/IP Device Options
//

class CKEBDin2MasterTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CKEBDin2MasterTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_IP;
		UINT m_Port;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 TCP/IP Master Driver
//

class CKEBDin2MasterTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CKEBDin2MasterTCPDriver(void);

		// Destructor
		~CKEBDin2MasterTCPDriver(void);
		
		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Access
		BOOL IsSet(UINT uTable);
		BOOL IsCmd(UINT uTable);


	
		
	protected:
		// Implementation
		void AddSpaces(void);

		


	};

//////////////////////////////////////////////////////////////////////////
//
// KEB DIN 2 Driver Address Selection
//

class CKEBDn2Dialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CKEBDn2Dialog(CKEBDin2MasterTCPDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};





// End of File

#endif
