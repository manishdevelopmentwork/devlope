
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Setup Boot Strapper
//
// Copyright (c) 1993-2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#define  _CRT_SECURE_NO_WARNINGS

#include <windows.h>

#include <commctrl.h>

#include <shellapi.h>

#pragma	comment(lib, "comctl32.lib")

// End of File
