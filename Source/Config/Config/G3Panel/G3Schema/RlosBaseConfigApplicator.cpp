
#include "Intern.hpp"

#include "RlosBaseConfigApplicator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

DLLAPI IConfigApplicator * Create_RlosDaxConfigApplicator(CString Model, IPxeModel *pModel);

DLLAPI IConfigApplicator * Create_RlosGraphiteConfigApplicator(CString Model, IPxeModel *pModel);

DLLAPI IConfigApplicator * Create_RlosCrxConfigApplicator(CString Model, IPxeModel *pModel);

//////////////////////////////////////////////////////////////////////////
//
// RLOS Configuration Applicator
//

// Instantiator

DLLAPI IConfigApplicator * Create_RlosConfigApplicator(CString Model, IPxeModel *pModel)
{
	if( Model.StartsWith(T("da")) ) {

		return Create_RlosDaxConfigApplicator(Model, pModel);
	}

	if( Model.StartsWith(T("g")) ) {

		return Create_RlosGraphiteConfigApplicator(Model, pModel);
	}

	if( Model.StartsWith(T("co")) || Model.StartsWith(T("ca")) ) {

		return Create_RlosCrxConfigApplicator(Model, pModel);
	}

	return NULL;
}

// Constructor

CRlosBaseConfigApplicator::CRlosBaseConfigApplicator(CString Model, IPxeModel *pModel)
{
	m_Model  = Model;

	m_pModel = pModel;

	m_pModel->AddRef();

	StdSetRef();
}

// Destructor

CRlosBaseConfigApplicator::~CRlosBaseConfigApplicator(void)
{
	AfxRelease(m_pModel);
}

// IUnknown

HRESULT CRlosBaseConfigApplicator::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IConfigApplicator);

	StdQueryInterface(IConfigApplicator);

	return E_NOINTERFACE;
}

ULONG CRlosBaseConfigApplicator::AddRef(void)
{
	StdAddRef();
}

ULONG CRlosBaseConfigApplicator::Release(void)
{
	StdRelease();
}

// IConfigApplicator

BOOL CRlosBaseConfigApplicator::GetPorts(CStringArray &List, CJsonData *pHard)
{
	UINT uFrom = List.GetCount();

	ScanForModules(List, pHard, 101, uFrom, L"C", L"CAN Port");

	ScanForModules(List, pHard, 102, uFrom, L"J", L"J1939 Port");

	ScanForModules(List, pHard, 103, uFrom, L"D", L"DeviceNet Port");

	ScanForModules(List, pHard, 104, uFrom, L"P", L"Profibus Port");

	return TRUE;
}

BOOL CRlosBaseConfigApplicator::GetModules(CStringArray &List, CJsonData *pHard)
{
	UINT nb = m_pModel->GetObjCount('b');

	UINT nf = tatoi(pHard->GetValue("exp.fcount", "0"));

	UINT nt = tatoi(pHard->GetValue("exp.tcount", "0"));

	UINT mn = 0;

	for( UINT n = 0; n < nb + 3 * nf + 3 * nt; n++ ) {

		CJsonData *pSlot = NULL;

		CString    Where;

		CString    Tag;

		if( !pSlot && n < nb ) {

			pSlot = pHard->GetChild(CPrintf("exp.base.modules.%4.4X", n));

			Where.Printf("Expansion Slot %u", 1 + n);

			Tag = "B";
		}

		if( !pSlot && n < nb + 3 * nf ) {

			int b = nb;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.fixed.rack%u.modules.%4.4X", r, s));

			Where.Printf("Fixed Rack %u Slot %u", r, 1 + s);

			Tag.Printf("F%u", r);
		}

		if( !pSlot && n < nb + 3 * nf + 3 * nt ) {

			int b = nb + 3 * nf;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.tethered.rack%u.modules.%4.4X", r, s));

			Where.Printf("Tethered Rack %u Slot %u", r, 1 + s);

			Tag.Printf("T%u", r);
		}

		if( pSlot ) {

			UINT uType = UINT(tatoi(pSlot->GetValue("type", "0")));

			if( uType >= 200 ) {

				CString Info;

				Info += CPrintf("%u,", mn++);

				Info += CPrintf("%s%s-%u,", Tag, pSlot->GetValue("order", "0"), uType);

				Info += CPrintf("%u,", uType);

				Info += Where;

				List.Append(Info);
			}
		}
	}

	return TRUE;
}

BOOL CRlosBaseConfigApplicator::HasModules(CJsonData *pHard)
{
	UINT nb = m_pModel->GetObjCount('b');

	UINT nf = tatoi(pHard->GetValue("exp.fcount", "0"));

	UINT nt = tatoi(pHard->GetValue("exp.tcount", "0"));

	for( UINT n = 0; n < nb + 3 * nf + 3 * nt; n++ ) {

		CJsonData *pSlot = NULL;

		if( !pSlot && n < nb ) {

			pSlot = pHard->GetChild(CPrintf("exp.base.modules.%4.4X", n));
		}

		if( !pSlot && n < nb + 3 * nf ) {

			int b = nb;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.fixed.rack%u.modules.%4.4X", r, s));
		}

		if( !pSlot && n < nb + 3 * nf + 3 * nt ) {

			int b = nb + 3 * nf;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.tethered.rack%u.modules.%4.4X", r, s));
		}

		if( pSlot ) {

			UINT uType = UINT(tatoi(pSlot->GetValue("type", "0")));

			if( uType >= 200 ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Implementation

void CRlosBaseConfigApplicator::ScanForModules(CStringArray &List, CJsonData *pHard, UINT uType, UINT &uFrom, PCTXT pType, PCTXT pName)
{
	UINT nb = m_pModel->GetObjCount('b');

	UINT nf = tatoi(pHard->GetValue("exp.fcount", "0"));

	UINT nt = tatoi(pHard->GetValue("exp.tcount", "0"));

	UINT pn = 1;

	for( UINT n = 0; n < nb + 3 * nf + 3 * nt; n++ ) {

		CJsonData *pSlot = NULL;

		CString    Where;

		CString    Tag;

		if( !pSlot && n < nb ) {

			pSlot = pHard->GetChild(CPrintf("exp.base.modules.%4.4X", n));

			Where.Printf("Expansion Slot %u", 1 + n);

			Tag = "B";
		}

		if( !pSlot && n < nb + 3 * nf ) {

			int b = nb;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.fixed.rack%u.modules.%4.4X", r, s));

			Where.Printf("Fixed Rack %u Slot %u", r, 1 + s);

			Tag.Printf("F%u", r);
		}

		if( !pSlot && n < nb + 3 * nf + 3 * nt ) {

			int b = nb + 3 * nf;

			int r = 1 + (n - b) / 3;

			int s = (n - b) % 3;

			pSlot = pHard->GetChild(CPrintf("exp.tethered.rack%u.modules.%4.4X", r, s));

			Where.Printf("Tethered Rack %u Slot %u", r, 1 + s);

			Tag.Printf("T%u", r);
		}

		if( pSlot ) {

			if( UINT(tatoi(pSlot->GetValue("type", "0"))) == uType ) {

				CString Info;

				Info += CPrintf("%s %u,", pName, pn);

				Info += CPrintf("%u,0,", uFrom);

				Info += CPrintf("%s%s%s,", pType, Tag, pSlot->GetValue("order", "0"));

				Info += CPrintf("%s,", pType);

				Info += Where;

				List.Append(Info);

				uFrom++;

				pn++;
			}
		}
	}
}

// End of File
