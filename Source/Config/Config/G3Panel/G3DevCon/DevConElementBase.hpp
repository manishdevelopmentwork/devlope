
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementBase_HPP

#define INCLUDE_DevConElementBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElement.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConModeButton;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Base UI Element
//

class CDevConElementBase : public CDevConElement
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementBase(void);

	// Destructor
	~CDevConElementBase(void);

	// Operations
	void    CreateControls(CWnd &Wnd, UINT &id);
	void    ShowControls(BOOL fShow);
	UINT    OnNotify(UINT uID, UINT uNotify, CWnd &Wnd);
	BOOL    CanAcceptData(IDataObject *pData, DWORD &dwEffect);
	BOOL    AcceptData(IDataObject *pData);
	void    SetData(CString const &Data);
	CString GetData(void);

protected:
	// Static Data
	static UINT m_cfPerson;

	// Data Member
	CLayFormation	  * m_pFullLayout;
	CLayFormation	  * m_pMainLayout;
	CLayItem	  * m_pDataLayout;
	CLayItem	  * m_pModeLayout;
	CDevConModeButton * m_pModeCtrl;
	CComboBox	  * m_pKeyCtrl;
	CString		    m_Data;

	// Overridables
	virtual void OnSetData(void);
	virtual BOOL OnCheckData(CString &Data);
	virtual BOOL AllowPersonality(void);

	// Implementation
	void LoadOptions(void);
	void AddToMain(CLayItem *pItem, UINT Flags);
	void AddToFull(CLayItem *pItem, UINT Flags);
	void FinalizeLayout(CLayFormation *pForm);
	void Select(CComboBox *pCombo, CString const &Text);
	void LoadData(void);
	void ShowData(BOOL fShow);
};

// End of File

#endif
