
#include "Intern.hpp"

#include "Fat16FileSystem.hpp"

#include "FatDirEntry.hpp"

#include "FatDirName.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat16 File System
//

// Instantiator

IFilingSystem * Create_Fat16FileSystem(UINT iDisk, UINT iVolume, UINT uBoot)
{
	return New CFat16FileSystem(iDisk, iVolume, uBoot);
	}

// Constructor 

CFat16FileSystem::CFat16FileSystem(UINT iDisk, UINT iVolume, UINT uBoot) : CFatFileSystem(iDisk, iVolume, uBoot)
{
	}

// Destructor

CFat16FileSystem::~CFat16FileSystem(void)
{	
	}

// Attributes

INT64 CFat16FileSystem::GetSize(void)
{
	return m_Boot.GetDataSize();
	}

INT64 CFat16FileSystem::GetFree(void)
{
	return INT64(FindFreeCount()) * INT64(m_Boot.GetClusterSize());
	}

// Methods

BOOL CFat16FileSystem::Init(void)
{
	if( CFilingSystem::Init() ) {

		ReadBoot();

		InitFat();

		InitRoot();

		return true;
		}

	return false;
	}

BOOL CFat16FileSystem::Start(void)
{
	if( CFilingSystem::Start() ) {
	
		ReadBoot();

		Debug(debugInfo, "Started");

		return true;
		}
	
	return false;
	}

// Enumeration

BOOL CFat16FileSystem::GetRoot(FSIndex &Index)
{
	MakeDataIndex(Index);

	Index.m_iCluster = rootEntry;

	return true;
	}

BOOL CFat16FileSystem::IsRoot(FSIndex const &Index)
{
	return Index.m_iCluster == rootEntry;
	}

// Layout

UINT CFat16FileSystem::GetFirstSector(UINT uCluster) const
{
	return m_Boot.GetFirstSector(uCluster);
	}

// Implementation

BOOL CFat16FileSystem::ReadBoot(void)
{
	if( ReadSector(0, PBYTE(&m_Boot), sizeof(m_Boot)) ) { 

		m_Boot.LocalToHost();

		m_Boot.Dump();

		if( m_Boot.IsValid() ) {

			m_uIdent       = m_Boot.m_dwVolId;
				       
			m_uSize        = m_Boot.GetTotalSectors();
				       
			m_uFat0        = m_Boot.GetFirstFatSector(0);
				       
			m_uFat1        = m_Boot.GetFirstFatSector(1);
				       
			m_uData        = m_Boot.GetFirstDataSector(); 
				       
			m_uRoot        = m_Boot.GetFirstRootDirSector();

			m_uRootSize    = m_Boot.m_BiosParamBlock.m_wRootSize;

			m_uClusterSize = m_Boot.GetClusterSize();

			m_uSectorSize  = m_Boot.m_BiosParamBlock.m_wSectorSize;

			return true;
			}
		}

	return false;
	}

BOOL CFat16FileSystem::InitFat(void)
{
	if( m_Boot.IsValid() ) {
	
		UINT uTotal = m_Boot.GetFatSectors() * m_Boot.GetBiosParamBlock().m_bFatCount;

		UINT uFirst = m_Boot.GetFirstFatSector(0);

		for( UINT n = 0; n < uTotal; n ++ ) {

			ZeroSector(uFirst + n);
			}

		if( !WriteFatEntry(0, entryReserved) ) {

			return false;
			}

		if( !WriteFatEntry(1, entryEoc) ) {

			return false;
			}
		
		if( !WriteFatEntry(2, entryEoc) ) {

			return false;
			}

		return true;
		}

	return false;
	}

BOOL CFat16FileSystem::InitRoot(void)
{
	// TODO

	return false;
	}

// Flags

BOOL CFat16FileSystem::IsVolumeClean(void)
{
	return (ReadFatEntry(1) & entryVolClean) == entryVolClean;
	}

BOOL CFat16FileSystem::IsVolumeDirty(void)
{
	return (ReadFatEntry(1) & entryVolClean) != entryVolClean;
	}

BOOL CFat16FileSystem::HasHardError(void)
{
	return (ReadFatEntry(1) & entryHardError) != entryHardError;
	}

BOOL CFat16FileSystem::SetVolumeClean(void)
{
	return WriteFatEntry(1, WORD(ReadFatEntry(1) | entryVolClean));
	}

BOOL CFat16FileSystem::SetVolumeDirty(void)
{
	return WriteFatEntry(1, WORD(ReadFatEntry(1) & ~entryVolClean));
	}

BOOL CFat16FileSystem::SetHardError(void)
{
	return WriteFatEntry(1, WORD(ReadFatEntry(1) & ~entryHardError));
	}

// Data Allocation

UINT CFat16FileSystem::AllocData(UINT uAlloc, BOOL fZero)
{
	UINT uRequired = (uAlloc + m_uClusterSize - 1) / m_uClusterSize;

	UINT uThis     = entryInvalid;

	UINT uNext     = entryInvalid;

	UINT uCluster  = entryInvalid;

	m_pMutex->Wait(FOREVER);

	while( uRequired ) {

		uNext = FindFree();

		if( uNext != entryInvalid ) {

			if( uThis != entryInvalid ) {

				if( !WriteFatEntry(uThis, WORD(uNext)) ) {

					break;
					}
				}

			if( WriteFatEntry(uNext, entryEoc) ) {

				uRequired --;
					
				uThis = uNext;

				if( uCluster == entryInvalid ) {
					
					uCluster = uNext;
					}

				continue;
				}
			}
			
		break;
		}
			
	if( uRequired ) {

		FreeData(uCluster);

		uCluster = entryInvalid;
		}

	m_pMutex->Free();

	if( uCluster == entryInvalid ) {

		Debug(debugWarn, "AllocData(Alloc=0x%8.8X) - FAILED", uAlloc);
		
		return entryInvalid;
		}

	if( fZero ) {

		ZeroData(uCluster, 0, m_uClusterSize);
		}
	
	return uCluster;
	}

UINT CFat16FileSystem::ReAllocData(UINT uCluster, UINT uAlloc, BOOL fZero)
{
	UINT uRequired  = (uAlloc + m_uClusterSize - 1) / m_uClusterSize;

	UINT uAllocated = FindCount(uCluster);

	if( uAllocated != entryInvalid ) {

		if( uRequired == uAllocated ) {

			return uCluster;
			}

		if( uRequired > uAllocated ) {

			UINT uLink = AllocData((uRequired - uAllocated) * m_Boot.GetClusterSize(), fZero);

			if( uLink != entryInvalid ) {

				UINT uLast = FindLast(uCluster);

				if( WriteFatEntry(uLast, WORD(uLink)) ) {

					return uCluster;
					}
				}

			Debug(debugWarn, "ReAllocData(Cluster=0x%8.8X, Alloc=0x%8.8X) - FAILED", uCluster, uAlloc);
						
			return entryInvalid;
			}

		if( uRequired < uAllocated ) {

			UINT uLast = FindNext(uCluster, uRequired - 1);

			UINT uFree = FindNext(uLast);

			if( WriteFatEntry(uLast, entryEoc) ) {

				FreeData(uFree);

				return uCluster;
				}

			Debug(debugWarn, "ReAllocData(Cluster=0x%8.8X, Alloc=0x%8.8X) - FAILED", uCluster, uAlloc);
						
			return entryInvalid;
			}
		}

	Debug(debugErr, "ReAllocData(Cluster=0x%8.8X, Alloc=0x%8.8X) - FAILED : Invalid Param", uCluster, uAlloc);

	return entryInvalid;
	}

BOOL CFat16FileSystem::FreeData(UINT uCluster)
{
	if( uCluster < 2 || uCluster > m_Boot.GetDataClusters() ) {

		Debug(debugErr, "FreeData(Cluster=0x%8.8X) - FAILED : Invalid Param", uCluster);

		return false;
		}

	while( uCluster != entryEoc ) {

		UINT uNext = FindNext(uCluster);

		if( uNext != entryInvalid ) {

			m_pMutex->Wait(FOREVER);

			if( WriteFatEntry(uCluster, entryFree) ) {

				uCluster = uNext;

				m_pMutex->Free();

				continue;
				}
			
			m_pMutex->Free();
			}
		
		Debug(debugWarn, "FreeData(Cluster=0x%8.8X) - FAILED", uCluster);

		return false;
		}

	return true;
	}

// Chain

UINT CFat16FileSystem::FindNext(UINT uCluster)
{
	uCluster = ReadFatEntry(uCluster);

	switch( uCluster ) {

		case entryReserved:
		case entryBad:
		case entryFree:

			return entryInvalid;
		}

	return uCluster;
	}

UINT CFat16FileSystem::FindNext(UINT uCluster, UINT uCount)
{
	while( uCount -- ) {

		uCluster = ReadFatEntry(uCluster);

		switch( uCluster ) {

			case entryEoc: 
			case entryReserved:
			case entryBad:
			case entryFree:

				return entryInvalid;
			}
		}

	return uCluster;
	}

UINT CFat16FileSystem::FindLast(UINT uCluster)
{
	for(;;) {

		UINT uNext = ReadFatEntry(uCluster);

		switch( uNext ) {

			case entryEoc: 

				return uCluster;

			case entryReserved:
			case entryBad:
			case entryFree:

				return entryInvalid;
			}

		uCluster = uNext;
		}
	}

UINT CFat16FileSystem::FindCount(UINT uCluster)
{
	for( UINT n = 0; ; n++ ) {

		uCluster = ReadFatEntry(uCluster);

		switch( uCluster ) {

			case entryEoc: 

				return n + 1;

			case entryReserved:
			case entryBad:
			case entryFree:

				return entryInvalid;
			}
		}

	return 0;
	}

UINT CFat16FileSystem::FindFree(void)
{
	UINT uFatClusters = m_Boot.GetDataClusters();

	UINT uFatSectors  = m_Boot.GetFatSectors();

	UINT uPerSector   = m_Boot.GetFatPerSector();

	UINT uFatSector   = m_uFat0;

	for( UINT nSector = 0; nSector < uFatSectors; nSector ++ ) {
		
		PWORD pEntry = PWORD(LockSector(uFatSector, FALSE));

		if( pEntry ) {

			UINT uThis = Min(uPerSector, uFatClusters);

			for( UINT nCluster = 0; nCluster < uThis; nCluster ++ ) {

				if( IntelToHost(pEntry[nCluster]) == entryFree ) {

					UnlockSector(uFatSector, false);

					return nSector * uPerSector + nCluster;
					}
				}

			uFatClusters -= uThis;

			UnlockSector(uFatSector++, false);
			
			continue;
			}
		
		break;
		}

	Debug(debugWarn, "FindFree() - FAILED");

	return entryInvalid;
	}

UINT CFat16FileSystem::FindFreeCount(void)
{
	UINT uFatClusters = m_Boot.GetDataClusters();

	UINT uFatSectors  = m_Boot.GetFatSectors();

	UINT uPerSector   = m_Boot.GetFatPerSector();

	UINT uFatSector   = m_uFat0;

	UINT uFreeCount   = 0;

	for( UINT nSector = 0; nSector < uFatSectors; nSector ++ ) {
		
		PWORD pEntry = PWORD(LockSector(uFatSector, FALSE));

		if( pEntry ) {

			UINT uThis = Min(uPerSector, uFatClusters);

			for( UINT nCluster = 0; nCluster < uThis; nCluster ++ ) {

				if( IntelToHost(pEntry[nCluster]) == entryFree ) {

					uFreeCount ++;
					}
				}

			uFatClusters -= uThis;

			UnlockSector(uFatSector++, false);
			
			continue;
			}
		
		break;
		}

	return WORD(uFreeCount);
	}

// Fat Entry

UINT CFat16FileSystem::ReadFatEntry(UINT uCluster)
{
	return ReadFatEntry(0, uCluster);
	}

BOOL CFat16FileSystem::WriteFatEntry(UINT uCluster, WORD wEntry)
{
	return WriteFatEntry(0, uCluster, wEntry) && WriteFatEntry(1, uCluster, wEntry);
	}

UINT CFat16FileSystem::ReadFatEntry(UINT uFat, UINT uCluster)
{
	Debug(debugData, "ReadFATEntry(Fat=%d, Cluster=0x%8.8X)", uFat, uCluster);

	CDataSector Sector;

	if( MapFatEntry(uFat, uCluster, Sector) ) {

		WORD wData;

		if( ReadSector(Sector, PBYTE(&wData), sizeof(WORD)) ) {

			return IntelToHost(WORD(wData));
			}
		}

	Debug(debugData, "ReadFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X) - FAILED", uFat, uCluster);

	return entryInvalid;
	}

BOOL CFat16FileSystem::WriteFatEntry(UINT uFat, UINT uCluster, WORD wEntry)
{
	Debug(debugData, "WriteFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X)", uFat, uCluster, wEntry);

	CDataSector Sector;

	if( MapFatEntry(uFat, uCluster, Sector) ) {

		wEntry = HostToIntel(WORD(wEntry));

		return WriteSector(Sector, PBYTE(&wEntry), sizeof(WORD));
		}

	Debug(debugWarn, "WriteFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X) - FAILED", uFat, uCluster, wEntry);

	return false;
	}

BOOL CFat16FileSystem::MapFatEntry(UINT uFat, UINT uCluster, CDataSector &Ptr) const
{
	if( uFat < m_Boot.m_BiosParamBlock.m_bFatCount ) {

		if( uCluster < m_Boot.GetDataClusters() ) {

			UINT uFatSect = uFat ? m_uFat1 : m_uFat0;

			UINT uDataPtr = uCluster * sizeof(WORD);
			
			Ptr.m_uSector = uDataPtr / m_uSectorSize + uFatSect;

			Ptr.m_uOffset = uDataPtr % m_uSectorSize;

			return true;
			}
		}

	return false;
	}

// End of File
