
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DEVCAP_HPP
	
#define	INCLUDE_DEVCAP_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDeviceCaps;

//////////////////////////////////////////////////////////////////////////
//
// Device abstraction types for device capabilities
//

// DEVICE CLASS CONSTANTS (PLATFORM TYPES)

const int DEVCLASS_UNDEFINED  = 0;
const int DEVCLASS_UMC800     = 1;
const int DEVCLASS_HC900      = 2;

// DEVICE TYPE CONSTANTS (PRODUCT TYPES)

const int DEVTYPE_UMC800      = 0;
const int DEVTYPE_USF820      = 1;
const int DEVTYPE_HC950       = 2;
const int DEVTYPE_HC930       = 3;
const int DEVTYPE_HC970R      = 4;
const int DEVTYPE_HC970       = 5;
const int DEVTYPE_HC920       = 6;
const int DEVTYPE_HC921       = 7;
const int DEVTYPE_HC975       = 8;
const int DEVTYPE_HC950S      = 9;
const int DEVTYPE_HC930S      = 10;
const int DEVTYPE_HC970S      = 11;
const int DEVTYPE_HC975S      = 12;

// DEVICE REVISION CONSTANTS

const int REV_1p0x            = 0;  //   Rev 1.0x [Feature Set 0]
const int REV_1p1x            = 1;  //   Rev 1.1x [Feature Set 1]
const int REV_2p0x            = 2;  //   Rev 2.0x [Feature Set 2]  (Modbus Master/Slave support)
const int REV_2p1x            = 3;  //   Rev 2.1x [Feature Set 3]  (Extended Hot Start & Save/Menu Inhibit)
                                    // & Rev 2.3x [Feature Set 3]  (New C50/C30 on NetSilicon '9750 Processor)
const int REV_1p0xC70R        = 4;  //   Rev 1.0x [Feature Set 4]  (HC900 C70R for Redundancy)
const int REV_2p4x            = 5;  //   Rev 2.4x [Feature Set 5]  (High Density IO and new PFQ Module Support)
const int REV_2p4xC70R        = 6;  //   Rev 2.4x [Feature Set 6]  (C70R High Density IO and new PFQ Module Support)
                                    //   Rev 3.0x *** THERE ARE NO REVISION 3.x CONFIGURATIONS ***
const int REV_4p0x            = 7;  //   Rev 4.0x [Feature Set 7]  (New Database - all HC900 Controllers)
const int REV_4p1x            = 8;  //   Rev 4.1x [Feature Set 8]  (New Function Blocks, DST and NTP Server Support)
const int REV_4p2x            = 9;  //   Rev 4.2x [Feature Set 9]  (Support for Profibus Gateway device)
const int REV_4p3x            = 10; //   Rev 4.3x [Feature Set 10] (High Density AO Blocks, New Wireless and UDC Modbus blocks)
const int REV_4p4x            = 11; //   Rev 4.4x [Feature Set 11] (Trend, Trend Point, and Psychrometric Blocks)

const int REV_6p0x            = 12; //   Rev 6.0x [Feature Set 12] (Seattle Project)
const int REV_6p1x            = 13; //   Rev 6.1x [Feature Set 13] (Support for new cde file type)
const int REV_6p2x            = 14; //   Rev 6.2x [Feature Set 14]
const int REV_6p3x            = 15; //	 Rev 6.3x [Feature Set 15]
const int REV_6p4x            = 17; //	 Rev 6.4x [Feature Set 17]
const int REV_6p5x            = 18; //	 Rev 6.5x [Feature Set 18]
const int REV_6p7x            = 19; //	 Rev 6.7x [Feature Set 19]

const int REV_7p0x            = 20; //	 Rev 7.0x [Feature Set 20] (IO redundancy funciton block)

// DOUBLE VALUES FOR DEVICE CAPABILITIES

enum EDeviceCapDbl
{
	DEVCAPDBL_FBLK_MIN_CYC_TIME,
	DEVCAPDBL_PROC_TIME_PCT,
	DEVCAPDBL_FBLK_MAX_PCT,
	DEVCAPDBL_FBLK_CYC53_3,
	DEVCAPDBL_FBLK_CYC53_66_7,
	DEVCAPDBL_FBLK_CYC53_106_7,
	DEVCAPDBL_FBLK_CYC53_133_3,
	DEVCAPDBL_FBLK_CYC53_266_7,
	DEVCAPDBL_FBLK_CYC53_533_3,
	DEVCAPDBL_FIO_MAX_SCAN_TIME,
	DEVCAPDBL_IO_LIMITED_FBLK_CYC,
	DEVCAPDBL_BLK_MIN_CYC_TIME,
	DEVCAPDBL_BLK_CYC_TIME_INCR,
	DEVCAPDBL_FBLK_CYC_TIME_INCR
	};

// INTEGER VALUES FOR DEVICE CAPABILITIES

enum EDeviceCapInt
{
	DEVCAPINT_MAX_RESERVED_BLOCKS,
	DEVCAPINT_MAX_RACKS,
	DEVCAPINT_MAX_SERIAL_MODBUS_SLAVES,
	DEVCAPINT_BLOCK_VERSION_PID,
	DEVCAPINT_BLOCK_VERSION_CARBON,
	DEVCAPINT_WIRE_GRAPHICS_RECORD_FACTOR,
	DEVCAPINT_SIZE_AE_PACKET	
	};

// FEATURES FOR DEVICE CAPABILITIES

enum EDeviceCapFeature
{
	FEATURE_USER_MB_REG,				// User Modbus Register Map
	FEATURE_OI_SECURITY,				// Honeywell Operator Interface Security Menu Supported
	FEATURE_TWO_ETHERNET_PORTS,			// CPU has two ethernet ports (E1 and E2)
	FEATURE_REDUNDANCY,					// CPU is a redundancy CPU (C70R only)
	FEATURE_TEN_HOST_CONNECTIONS,		// supports upto 10 host connections
	FEATURE_MODBUS_TCP_SLAVES,			// supports Modbus/TCP Slave Devices
	FEATURE_ACCUTUNE3,					// support for Accutune 3 (otherwise it uses Accutune 2 only)
	FEATURE_MODBUS_SERIAL_SLAVES_32,	// support for 32 modbus serial slave devices; otherwise 16
	FEATURE_MANUAL_FLASH_BURN,			// support for Manual Flash Memory burn on HC900 CPU
	FEATURE_SUPPORT_30_ALARM_GROUPS,	// support for 30 alarm groups, otherwise 20
	FEATURE_ENHANCED_TIME_CLOCK,		// support DST and TimeZones in HC900
	FEATURE_UNLIMITED_RECIPES,			// support unlimited recipe files (use recipe allocation table to find recipe quantities
	FEATURE_LOW_SERIAL_BAUD_RATES,		// support 1200, 2400, 4800 serial baud rates
	FEATURE_HIGH_115K_SERIAL_BAUD_RATE,	// support of Advanced high speed 115.2K baud rate with FC Timeouts
	FEATURE_GENERIC_TAG_TABLE,			// uses the Generic Tag Table to store tags for AGA, PB, FSS, etc
	FEATURE_PPO_TAG_TABLE,				// PPO Tag Table is built by HCD (earlier was built by CPU)
	FEATURE_CALENDAR_TAG_TABLE,			// uses the Calendar Block Tag Table
	FEATURE_TWO_BYTE_ALARM_NUM,			// Alarm/Event Packet when reading history has >255 alarms, so alarm # is stored as a word
	FEATURE_VER2_SERIAL_NET_BLOCKS,		// introduced in C70R Release 1
	FEATURE_SPP_TAG_TABLE			// R670 Setpoint Type support
	};

//////////////////////////////////////////////////////////////////////////
//					
// CInstrumentInfo defines the basic attributes of a
// device such as its platform type and product type.
//

class CInstrumentInfo
{
	public:
		// Constructors
		CInstrumentInfo();
		CInstrumentInfo(int nPlatformType, int nProductType, int nDbSchema, int nFeatureSet);

		// Equality Operator
		BOOL operator == (const CInstrumentInfo &InstInfo) const;

		// Data Members
		int m_nPlatformType;
		int m_nProductType;
		int m_nDbSchema;
		int m_nFeatureSet;
	};

//////////////////////////////////////////////////////////////////////////
//					
// CDeviceRev defines the attributes of a device revision
// such as its schema number and feature set.
//

class CDeviceRev
{
	public:
		// Constructors
		CDeviceRev();
		CDeviceRev(CString DeviceRev, int nSchemaNumber, int nFeatureSet);

		// Data Members
		CString m_DeviceRev;  // Device rev as string (e.g., "1.0x")
		int m_nSchemaNumber;  // Schema number of revision
		int m_nFeatureSet;    // Feature set of revision (e.g., REV_1p0x)
	};

//////////////////////////////////////////////////////////////////////////
//					
// CDeviceType defines the identification attributes of a
// device type such as its platform type and product type,
// its name (e.g., "HC900-C50") and the revisions that it
// supports (schema numbers and feature sets).
//

class CDeviceType
{
	public:
		// Constructors
		CDeviceType();
		CDeviceType(int nPlatformType, int nProductType, CString DeviceName);

		// Destructor
		~CDeviceType();

		// Attributes
		int GetDeviceFeatureSet(int nIndex);
		CString GetDeviceName();
		CString GetDeviceNameRev(int nIndex);
		CString GetDeviceRev(int nIndex);
		CString GetDeviceRev(const CInstrumentInfo &InstInfo);
		int GetDeviceRevCount();
		CInstrumentInfo* GetInstInfo(int nIndex);
		int HasInstInfo(const CInstrumentInfo &InstInfo);

		// Operations
		void AddDeviceRev(CDeviceRev *pDeviceRev);
		CDeviceCaps* CreateDeviceCapsObject(const CInstrumentInfo &InstInfo);

		// Data Members
		int m_nPlatformType;			// Platform type (e.g., DEVCLASS_HC900)
		int m_nProductType;				// Product type (e.g., DEVTYPE_HC950)

	protected:
		// Data Members
		CString m_DeviceName;			// Name of device (e.g., "HC900-C50")
		CArray <CDeviceRev*> m_RevArray;// Array of device revisions supported
	};

//////////////////////////////////////////////////////////////////////////
//					
// CDeviceTypeInfo defines all supported device types
// (e.g., an HC900-C30, an HC900-C50, an HC900-C70)
// and their revisions.

class CDeviceTypeInfo
{
	public:
		// Constructor
		CDeviceTypeInfo();

		// Destructor
		~CDeviceTypeInfo();

		// Attributes
		CDeviceType* GetDeviceType(const CInstrumentInfo &InstInfo);

	protected:
		// Operations
		void AddDeviceType(CDeviceType *pDeviceType);

		// Data Members
		CArray <CDeviceType*> m_DeviceTypes;

		// Implementation
		void MakeHC900_30(void);
		void MakeHC900_30S(void);
		void MakeHC900_50(void);
		void MakeHC900_50S(void);
		void MakeHC900_70(void);
		void MakeHC900_70S(void);
		void MakeHC900_70R(void);
		void MakeHC900_75(void);
		void MakeHC900_75S(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// CDeviceCaps defines the capabilities of all revisions
// of a device type. It is an abstract base class.
// 
// USAGE:
//
//   To test if a device type/revision has a feature:
//   Pass in a member from enum EDeviceCapFeature:
//   GetDeviceCaps().HasFeature(FEATURE_USER_MB_REG);
//
//   To get the value of an integer attribute in a device type/revision:
//   Pass in a member from enum EDeviceCapInt:
//   GetDeviceCaps().GetValue(DEVCAPINT_MAX_RESERVED_BLOCKS);
//
//   To get the value of a double attribute in a device type/revision:
//   Pass in a member from enum EDeviceCapDbl:
//   GetDeviceCaps().GetValue(DEVCAPDBL_FBLK_MIN_CYC_TIME);

class CDeviceCaps
{
	public:
		// Constructors
		CDeviceCaps();
		CDeviceCaps(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo);

		// Attributes
		CString GetDeviceNameRev();
		CString GetDeviceRev();

		// Get the integer value of the specified key on the device.
		int GetValue(EDeviceCapInt key);

		// Get the double value of the specified key on the device.
		double GetValue(EDeviceCapDbl key);

		// Check if the device has the specified feature.
		BOOL HasFeature(EDeviceCapFeature key);

	protected:
		// Operations
		void AddFeature(EDeviceCapFeature key, BOOL value);
		void AddValue(EDeviceCapInt key, int value);
		void AddValue(EDeviceCapDbl key, double value);

	public:
		// Data Members
		CDeviceType *m_pDeviceType;	// Device type information for this device.
		CInstrumentInfo m_InstInfo;	// Platform type, product type, schema number, feature set.
		int m_nPlatformType;		// Extracted platformType from m_InstInfo
		int m_nProductType;			// Extracted productType from m_InstInfo
		int m_nDbSchema;			// Extracted dbSchema from m_InstInfo
		int m_nFeatureSet;			// Extracted featureSet from m_InstInfo

	protected:
		// Data Members
		CMap <EDeviceCapInt, int> m_IntValues;			// Map of integer values that this device has defined for itself.
		CMap <EDeviceCapDbl, double> m_DblValues;		// Map of double values that this device has defined for itself.
		CMap <EDeviceCapFeature, BOOL> m_FeatureValues;	// Map of features that this device has defined for itself.
	};

//////////////////////////////////////////////////////////////////////////
//
// CHc930 defines the capabilities of all revisions
// of device type HC900-C30.

class CHc930 : public CDeviceCaps
{
	public:
		// Constructor
		CHc930(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo);
	};

//////////////////////////////////////////////////////////////////////////
//
// CHc950 defines the capabilities of all revisions
// of device type HC900-C50.

class CHc950 : public CDeviceCaps
{
	public:
		// Constructor
		CHc950(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo);
	};

//////////////////////////////////////////////////////////////////////////
//
// CHc970 defines the capabilities of all revisions
// of device type HC900-C70.

class CHc970 : public CDeviceCaps
{
	public:
		// Constructor
		CHc970(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo);
	};

//////////////////////////////////////////////////////////////////////////
//
// CHc930 defines the capabilities of all revisions
// of device type HC900-C70R.

class CHc970R : public CDeviceCaps
{
	public:
		// Constructor
		CHc970R(CDeviceType *pDeviceType, const CInstrumentInfo &InstInfo);
	};

// End of File

#endif
