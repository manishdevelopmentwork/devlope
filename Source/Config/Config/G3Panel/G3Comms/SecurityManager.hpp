
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SecurityManager_HPP

#define INCLUDE_SecurityManager_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUserList;

//////////////////////////////////////////////////////////////////////////
//
// User Manager
//

class DLLNOT CSecurityManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSecurityManager(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT        m_Handle;
		CUserList * m_pUsers;
		UINT	    m_DefMapped;
		UINT	    m_DefLocal;
		UINT	    m_DefPage;
		UINT	    m_LogMapped;
		UINT	    m_LogLocal;
		UINT	    m_LogEnable;
		UINT	    m_FileLimit;
		UINT	    m_FileCount;
		UINT	    m_WithBatch;
		UINT        m_SignLogs;
		UINT        m_Drive;
		UINT	    m_UserTime;
		UINT	    m_UserClear;
		UINT	    m_CheckTime;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
