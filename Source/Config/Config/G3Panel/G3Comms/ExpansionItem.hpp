
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ExpansionItem_HPP

#define INCLUDE_ExpansionItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCascadedItem;
class CCommsPortList;
class COptionCardItem;
class COptionCardList;
class COptionCardRackItem;
class CTetheredItem;
union CUsbTreePath;

//////////////////////////////////////////////////////////////////////////
//
// Expansion System
//

class DLLAPI CExpansionItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CExpansionItem(void);

		// Attributes
		UINT    GetTreeImage(void) const;
		CString GetTreeLabel(void) const;

		// Operations
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		void Validate(BOOL fExpand);
		UINT GetPowerBudget(void);
		UINT GetPowerTotal(void);
		UINT GetPowerUsage(void);
		BOOL CheckPower(void);
		BOOL CheckPower(CString &Warning);
		UINT GetSlotCount(void);
		UINT GetRackCount(void);
		UINT AllocSlotNumber(void);
		UINT AllocRackNumber(void);

		// Ports List Access
		BOOL GetPortList(CCommsPortList * &pList, UINT uIndex) const;

		// Slot
		COptionCardItem * FindSlot(CUsbTreePath const &Slot) const;
		COptionCardItem * FindSlot(UINT uSlot) const;

		// Rack
		COptionCardRackItem * FindRack(CUsbTreePath const &Rack) const;
		COptionCardRackItem * FindRack(UINT uRack) const;

		// Firmware
		void GetFirmwareList(CArray<UINT> &List) const;

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void PostLoad(void);

		// Conversion
		void PostConvert(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		COptionCardList * m_pRack;
		CCascadedItem   * m_pCascaded;
		CTetheredItem   * m_pTethered;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void UpgradeRack(void);
	};

// End of File

#endif
