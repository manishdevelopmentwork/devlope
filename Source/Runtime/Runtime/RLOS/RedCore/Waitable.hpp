
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Waitable_HPP
	
#define	INCLUDE_Waitable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Executive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CExecThread;

//////////////////////////////////////////////////////////////////////////
//
// Executive Event
//

class CWaitable
{
	public:
		// Constuctor
		CWaitable(CExecutive *pExec);

		// Overridables
		virtual bool WaitInit(CExecThread *pThread, UINT uSlot, bool fWait) = 0;
		virtual void WaitDone(CExecThread *pThread, UINT uSlot)             = 0;

	protected:
		// Data Members
		CExecutive *    m_pExec;
		CExecThreadLink m_Head;
		CExecThreadLink m_Tail;
		CExecThreadLink m_Wake;

		// Implementation
		bool WakeOne(IWaitable *pThis);
		void WakeAll(IWaitable *pThis);
		bool AppendThread(CExecThread *pThread, UINT uSlot);
		bool RemoveThread(CExecThread *pThread, UINT uSlot);
	};

// End of File

#endif
