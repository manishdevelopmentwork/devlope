
#include "intern.hpp"

#include "calib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 MC2 Calibration Link Support
//
// Copyright (c) 1993-2017 Red Lion Controls
// 
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Calibration Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCalibDlg, CStdDialog);

// Entry Point

global void CalibDialog(CWnd &Wnd)
{
	CCalibDlg Dlg(FALSE);

	Dlg.Execute(Wnd);
}

// Constructor

CCalibDlg::CCalibDlg(BOOL fLinkOptions)
{
	m_pComms       = New CCommsThread;

	m_fAutoClose   = FALSE;

	m_fBusy        = FALSE;

	m_fRead        = FALSE;

	m_wLinOut      = 0;

	m_wCommit      = 0;

	m_uCmdsLoaded  = 0;

	m_uChansLoaded = 0;

	m_fGraphite    = TRUE;

	m_fLinkOptions = fLinkOptions;

	SetName(L"CalibDlg");
}

// Destructors

CCalibDlg::~CCalibDlg(void)
{
	delete m_pComms;
}

// Message Map

AfxMessageMap(CCalibDlg, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PREDESTROY)

	AfxDispatchNotify(101, CBN_SELCHANGE, OnCmdSelChange)
	AfxDispatchNotify(102, CBN_SELCHANGE, OnCmdSelChange)
	AfxDispatchNotify(103, CBN_SELCHANGE, OnCmdSelChange)

	AfxDispatchCommand(300, OnCommandSend)
	AfxDispatchCommand(301, OnCommandRead)
	AfxDispatchCommand(302, OnCommandLink)

	AfxDispatchCommand(IDCANCEL, OnCommandDone)

	AfxDispatchCommand(IDDONE, OnCommsDone)
	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDCRED, OnCommsCred)

	AfxMessageEnd(CCalibDlg)
};

// Message Handlers

BOOL CCalibDlg::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_pComms->Bind(this);

	m_pComms->Create();

	LoadConfig();

	SetCaption();

	LoadModules();

	LoadModel();

	LoadChannels(GetModel(), 1);

	LoadCommands(GetModel(), 5);

	DoEnables();

	GetDlgItem(302).ShowWindow(m_fLinkOptions);

	return TRUE;
}

void CCalibDlg::OnPreDestroy(void)
{
	SaveConfig();
}

// Command Handlers

BOOL CCalibDlg::OnCommandSend(UINT uID)
{
	if( !m_fBusy ) {

		if( GetCode() < CALIBLINDONE ) {

			if( Abort(CString(IDS_SEND_CAL_MODIFY)) ) {

				return TRUE;
			}
		}

		if( GetCode() == CALIBSAVE ) {

			if( Abort(CString(IDS_SEND_CAL_SAVE)) ) {

				return TRUE;
			}
		}

		WORD wData;

		if( IsColdJunc(GetCode()) ) {

			double Data = GetFloat();

			Data += 273.2;

			Data *= 100;

			wData = WORD(Data + 0.5);
		}
		else
			wData = GetWord();

		m_bSlot = BYTE(GetSlot());

		m_bCode = GetCode();

		EncodeChannel(m_bCode);

		m_pComms->CalibTxReq(m_bSlot, m_bCode, wData);

		m_fRead = FALSE;

		m_fBusy = TRUE;
	}
	else
		MessageBeep(0);

	return TRUE;
}

BOOL CCalibDlg::OnCommandRead(UINT uID)
{
	if( !m_fBusy ) {

		if( IsColdJunc(GetCode()) ) {

			UINT uModel = GetModel();

			if( !IsTC8(uModel) && !IsTC8Iso(uModel) ) {

				if( Abort(CString(IDS_SEND_CAL_INPUT)) ) {

					return TRUE;
				}
			}
		}

		m_bSlot = BYTE(GetSlot());

		m_bCode = GetCode();

		EncodeChannel(m_bCode);

		m_pComms->CalibTxRead(m_bSlot, m_bCode);

		m_fRead = TRUE;

		m_fBusy = TRUE;
	}
	else
		MessageBeep(0);

	return TRUE;
}

BOOL CCalibDlg::OnCommandDone(UINT uID)
{
	if( m_wCommit || m_wLinOut ) {

		CString Commit;

		CString LinOut;

		for( BYTE b = 0; b < 16; b++ ) {

			if( m_wCommit & (1 << b) ) {

				if( !Commit.IsEmpty() )
					Commit += L", ";

				Commit += CPrintf(CString(IDS_SEND_MODULE), 1 + b);
			}

			if( m_wLinOut & (1 << b) ) {

				if( !LinOut.IsEmpty() )
					LinOut += L", ";

				LinOut += CPrintf(CString(IDS_SEND_MODULE), 1 + b);
			}
		}

		CString Text;

		if( m_wLinOut ) {

			if( LinOut.Find(L",") < NOTHING ) {

				Text += CString(IDS_SEND_MODULE_LINOUT);

				Text += L"\n\n   " + LinOut;

				Text += CString(IDS_SEND_OP_BAD);
			}
			else {
				Text += LinOut + CString(IDS_SEND_LINOUT);

				Text += CString(IDS_SEND_OP_BAD);
			}
		}

		if( m_wCommit ) {

			if( Commit.Find(L",") < NOTHING ) {

				Text += CString(IDS_SEND_MODULE_CAL);

				Text += L"\n\n   " + Commit;

				Text += CString(IDS_SEND_CAL_BAD);
			}
			else {
				Text += Commit + CString(IDS_SEND_CAL);

				Text += CString(IDS_SEND_CAL_BAD);
			}
		}

		Text += L"\n\n";

		Text += CString(IDS_SEND_EXIT_NOW);

		if( NoYes(Text) == IDNO ) {

			return TRUE;
		}
	}

	m_pComms->Terminate(INFINITE);

	EndDialog(FALSE);

	return TRUE;
}

BOOL CCalibDlg::OnCommsDone(UINT uID)
{
	if( m_fBusy ) {

		DecodeChannel(m_bCode);

		if( m_fRead ) {

			WORD wData = 0;

			if( m_pComms->CalibRxRead(wData) ) {

				CString Text;

				if( IsColdJunc(m_bCode) ) {

					double Data = wData;

					Data /= 100;

					Data -= 273.15;

					Text.Printf(L"%.2f", Data);
				}
				else
					Text.Printf(L"%u", wData);

				GetDlgItem(104).SetWindowText(Text);
			}
			else {
				CString Text = CString(IDS_SEND_CAL_READ_FAIL);

				Error(Text);

				DoEnables();
			}
		}
		else {
			if( !m_pComms->CalibRxReq() ) {

				CString Text = CString(IDS_SEND_CAL_REQ_FAIL);

				Error(Text);
			}
			else {
				if( m_bCode > CALIBNULL && m_bCode < CALIBLIN0V ) {

					m_wCommit |=  (1 << m_bSlot);
				}

				if( m_bCode >= CALIBLIN0V && m_bCode <= CALIBLIN20MA ) {

					m_wLinOut |=  (1 << m_bSlot);
				}

				if( m_bCode == CALIBSAVE ) {

					m_wCommit &= ~(1 << m_bSlot);
				}

				if( m_bCode == CALIBLINDONE ) {

					m_wLinOut &= ~(1 << m_bSlot);
				}
			}
		}

		m_fBusy = FALSE;
	}

	return TRUE;
}

BOOL CCalibDlg::OnCommsFail(UINT uID)
{
	CString Text;

	Text += CString(IDS_SEND_CAL_TRANS_FAIL);

	Text += L"\n\n";

	Text += m_pComms->GetErrorText();

	Error(Text);

	DoEnables();

	return TRUE;
}

BOOL CCalibDlg::OnCommsCred(UINT uID)
{
	m_pComms->AskForCredentials();

	return TRUE;
}

BOOL CCalibDlg::OnCommandLink(UINT uID)
{
	if( uID == 302 ) {

		m_pComms->Terminate(INFINITE);

		m_pComms->Close();

		m_pComms->Detach();

		delete m_pComms;

		m_pComms = NULL;

		Link_SetOptions(FALSE);

		SetWindowText(m_Caption);

		m_pComms = New CCommsThread;

		m_pComms->Bind(this);

		m_pComms->Create();
	}

	return TRUE;
}

// Notification Handlers

void CCalibDlg::OnCmdSelChange(UINT uID, CWnd &Wnd)
{
	UINT uModel = GetModel();

	BYTE bCode  = GetCode();

	if( IsUIn4(uModel) ) {

		if( bCode == CALIBSAVE ) {

			CComboBox &Channel = (CComboBox &) GetDlgItem(102);

			Channel.SelectData(1);
		}
	}
	else {
		if( bCode == CALIBCJ || (bCode >= CALIBLIN0V && bCode <= CALIBSAVE) ) {

			CComboBox &Channel = (CComboBox &) GetDlgItem(102);

			Channel.SelectData(1);
		}
	}

	LoadChannels(uModel, GetChannel());

	LoadCommands(uModel, bCode);

	DoEnables();

	GetDlgItem(104).SetWindowText(L"");
}

// Implementation

void CCalibDlg::SetCaption(void)
{
	CString Series = m_fGraphite ? L"Graphite Series" : L"Controller Series";

	m_Caption = CPrintf(GetWindowText(), Series);

	SetWindowText(m_Caption);
}

void CCalibDlg::LoadModules(void)
{
	if( m_fGraphite ) {

		CComboBox &Box = (CComboBox &) GetDlgItem(100);

		for( UINT n = 0; n < 8; n++ ) {

			Box.AddString(CPrintf(IDS_SEND_MODULE, 1 + n), n);
		}

		Box.SelectData(m_uModule);
	}
	else {
		CComboBox &Box = (CComboBox &) GetDlgItem(100);

		for( UINT n = 0; n < 16; n++ ) {

			Box.AddString(CPrintf(IDS_SEND_MODULE, 1 + n), n);
		}

		Box.SelectData(m_uModule);
	}
}

void CCalibDlg::LoadModel(void)
{
	if( m_fGraphite ) {

		CComboBox &Model = (CComboBox &) GetDlgItem(101);

		Model.AddString(L"GMINI8", ID_GMINI8);
		Model.AddString(L"GMINV8", ID_GMINV8);
		Model.AddString(L"GMOUT4", ID_GMOUT4);
		Model.AddString(L"GMPID1", ID_GMPID1);
		Model.AddString(L"GMPID2", ID_GMPID2);
		Model.AddString(L"GMRTD6", ID_GMRTD6);
		Model.AddString(L"GMTC8", ID_GMTC8);
		Model.AddString(L"GMUIN4", ID_GMUIN4);
		Model.AddString(L"GMSG1", ID_GMSG1);

		Model.SelectData(m_uModel);
	}
	else {
		CComboBox &Model = (CComboBox &) GetDlgItem(101);

		Model.AddString(L"CSINI8", ID_CSINI8);
		Model.AddString(L"CSINI8L", ID_CSINI8L);
		Model.AddString(L"CSINV8", ID_CSINV8);
		Model.AddString(L"CSINV8L", ID_CSINV8L);
		Model.AddString(L"CSOUT4", ID_CSOUT4);
		Model.AddString(L"CSPID1", ID_CSPID1);
		Model.AddString(L"CSPID2", ID_CSPID2);
		Model.AddString(L"CSRTD6", ID_CSRTD6);
		Model.AddString(L"CSSG1", ID_CSSG);
		Model.AddString(L"CSTC8", ID_CSTC8);
		Model.AddString(L"CSTC8ISO", ID_CSTC8ISO);

		Model.SelectData(m_uModel);
	}
}

void CCalibDlg::LoadChannels(UINT uModel, BYTE bSelect)
{
	if( m_uChansLoaded == uModel ) {

		return;
	}

	m_uChansLoaded = uModel;

	CComboBox &Channel = (CComboBox &) GetDlgItem(102);

	Channel.ResetContent();

	BYTE bChanQty = 0;

	if( IsPid1(uModel) ) {

		bChanQty = 1;

		bSelect = 1;
	}

	if( IsPid2(uModel) || IsSG(uModel) ) {

		bChanQty = 2;

		if( !(bSelect == 1 || bSelect == 2) )

			bSelect = 1;
	}

	if( IsOut4(uModel) ) {

		bChanQty = 4;

		if( !(bSelect >= 1 && bSelect <= 4) )

			bSelect = 1;
	}

	if( IsRtd6(uModel) ) {

		bChanQty = 6;

		if( bSelect < 1 || bSelect > 6 )

			bSelect = 1;
	}

	if( IsTC8(uModel)    ||
	   IsTC8Iso(uModel) ||
	   IsInI8(uModel)   ||
	   IsInI8L(uModel)  ||
	   IsInV8(uModel)   ||
	   IsInV8L(uModel) ) {

		bChanQty = 8;

		if( !(bSelect >= 1 && bSelect <= 8) )

			bSelect = 1;
	}

	if( IsUIn4(uModel) ) {

		bChanQty = 4;

		bSelect  = 1;
	}

	if( bChanQty >= 1 ) {

		Channel.AddString(CString(IDS_SEND_CAL_1), 1);
	}

	if( bChanQty >= 2 ) {

		Channel.AddString(CString(IDS_SEND_CAL_2), 2);
	}

	if( bChanQty >= 4 ) {

		Channel.AddString(CString(IDS_SEND_CAL_3), 3);
		Channel.AddString(CString(IDS_SEND_CAL_4), 4);
	}

	if( bChanQty >= 6 ) {

		Channel.AddString(CString(IDS_SEND_CAL_5), 5);
		Channel.AddString(CString(IDS_SEND_CAL_6), 6);
	}

	if( bChanQty >= 8 ) {

		Channel.AddString(CString(IDS_SEND_CAL_7), 7);
		Channel.AddString(CString(IDS_SEND_CAL_8), 8);
	}

	Channel.SelectData(bSelect);
}

void CCalibDlg::LoadCommands(UINT uModel, BYTE bSelect)
{
	if( m_uCmdsLoaded == uModel ) {

		return;
	}

	m_uCmdsLoaded = uModel;

	CComboBox &Code = (CComboBox &) GetDlgItem(103);

	Code.ResetContent();

	if( IsPid1(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -10), 3);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 56), 4);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 0), 5);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 10), 6);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 0), 7);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 4), 8);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 20), 9);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 0), 11);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 300), 13);
		Code.AddString(CPrintf(IDS_SEND_CAL_HCM_DC, 0, 0), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_HCM_DC, 89, 9), 2);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_V, 0), CALIBLIN0V);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_V, 10), CALIBLIN10V);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_MA, 0), CALIBLIN0MA);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_MA, 4), CALIBLIN4MA);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_MA, 20), CALIBLIN20MA);
		Code.AddString(CString(IDS_SEND_CAL_OUT_DONE), CALIBLINDONE);
		Code.AddString(CString(IDS_SEND_CAL_CJ_COMP), CALIBCJ);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( bSelect ==  0 || bSelect == 12 ||
		   bSelect == 14 || bSelect >= 22 )

			bSelect = 5;
	}

	if( IsPid2(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -10), 3);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 56), 4);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 0), 5);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 10), 6);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 0), 7);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 4), 8);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 20), 9);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 0), 11);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 300), 13);
		Code.AddString(CPrintf(IDS_SEND_CAL_HCM_DC, 0, 0), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_HCM_DC, 89, 9), 2);
		Code.AddString(CString(IDS_SEND_CAL_CJ_COMP), CALIBCJ);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( !((bSelect >=  1 && bSelect <= 11) ||
		      bSelect == 13 || bSelect == CALIBSAVE) )

			bSelect = 5;
	}

	if( IsRtd6(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 0), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 300), 3);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( !(bSelect == 1 || bSelect == 3 || bSelect == CALIBSAVE) )

			bSelect = 1;
	}

	if( IsTC8(uModel) || IsTC8Iso(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -10), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 56), 2);
		Code.AddString(CString(IDS_SEND_CAL_CJ_COMP), CALIBCJ);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( !(bSelect == 1       || bSelect == 2 ||
		      bSelect == CALIBCJ || bSelect == CALIBSAVE) )

			bSelect = 1;
	}

	if( IsInI8(uModel) || IsInI8L(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 0), 5);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 4), 6);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 20), 7);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( !(bSelect == 5 || bSelect == 6 ||
		      bSelect == 7 || bSelect == CALIBSAVE) )

			bSelect = 5;
	}

	if( IsInV8(uModel) || IsInV8L(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, -10), 3);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 10), 4);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( !(bSelect == 3 || bSelect == 4 ||
		      bSelect == CALIBSAVE) )

			bSelect = 3;
	}

	if( IsSG(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -20), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 20), 2);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -33), 3);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 33), 4);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -200), 5);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 200), 6);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_V, 0), CALIBLIN0V);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_V, 10), CALIBLIN10V);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_MA, 0), CALIBLIN0MA);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_MA, 4), CALIBLIN4MA);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT_MA, 20), CALIBLIN20MA);
		Code.AddString(CString(IDS_SEND_CAL_OUT_DONE), CALIBLINDONE);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		if( !((bSelect >= 1 && bSelect <= 6) ||
			(bSelect >= CALIBLIN0V && bSelect <= CALIBSAVE)) )

			bSelect = 1;
	}

	if( IsOut4(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0-5V", L"0V"), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0-5V", L"5V"), 2);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0-10V", L"0V"), 3);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0-10V", L"10V"), 4);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"+/-10V", L"-10V"), 5);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"+/-10V", L"+10V"), 6);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0/4-20mA", L"0mA"), 7);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0/4-20mA", L"4mA"), 8);
		Code.AddString(CPrintf(IDS_SEND_CAL_OUT4, L"0/4-20mA", L"20mA"), 9);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		bSelect = 1;
	}

	if( IsUIn4(uModel) ) {

		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, -10), 1);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_MV, 56), 2);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 0), 3);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PV, 10), 4);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 0), 5);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 4), 6);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_PC, 20), 7);
		Code.AddString(CString(IDS_SEND_CAL_CJ_COMP), 8);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 0), 10);
		Code.AddString(CPrintf(IDS_SEND_CAL_INP_RTD, 300), 12);
		Code.AddString(CString(IDS_SEND_CAL_EEPROM), CALIBSAVE);

		bSelect = 1;
	}

	Code.SelectData(bSelect);
}

void CCalibDlg::DoEnables(void)
{
	UINT uModel = GetModel();

	BYTE bCode  = BYTE(GetCode());

	BOOL fModel;

	if( IsUIn4(uModel) ) {

		fModel = TRUE;
	}
	else {
		fModel = !IsPid1(uModel) && bCode != CALIBCJ;
	}

	if( bCode >= CALIBLIN0V && bCode <= CALIBSAVE ) {

		fModel = FALSE;
	}

	BOOL fParam;

	if( IsOut4(uModel) ) {

		fParam = (bCode < CALIBSAVE);
	}
	else {
		if( IsUIn4(uModel) ) {

			fParam = IsColdJunc(bCode);
		}
		else {
			fParam = ((bCode >= CALIBLIN0V && bCode <= CALIBLIN20MA) || bCode == CALIBCJ);
		}
	}

	BOOL fRead  = (bCode < CALIBLINDONE);

	GetDlgItem(102).EnableWindow(fModel);

	GetDlgItem(202).EnableWindow(fModel);

	GetDlgItem(104).EnableWindow(fParam);

	GetDlgItem(204).EnableWindow(fParam);

	GetDlgItem(301).EnableWindow(fRead);
}

// Implementation

BOOL CCalibDlg::IsPid1(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMPID1 : uModel == ID_CSPID1;
}

BOOL CCalibDlg::IsPid2(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMPID2 : uModel == ID_CSPID2;
}

BOOL CCalibDlg::IsSG(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMSG1 : uModel == ID_CSSG;
}

BOOL CCalibDlg::IsOut4(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMOUT4 : uModel == ID_CSOUT4;
}

BOOL CCalibDlg::IsRtd6(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMRTD6 : uModel == ID_CSRTD6;
}

BOOL CCalibDlg::IsTC8(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMTC8 : uModel == ID_CSTC8;
}

BOOL CCalibDlg::IsTC8Iso(UINT uModel)
{
	return m_fGraphite ? FALSE : uModel == ID_CSTC8ISO;
}

BOOL CCalibDlg::IsInI8(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMINI8 : uModel == ID_CSINI8;
}

BOOL CCalibDlg::IsInI8L(UINT uModel)
{
	return m_fGraphite ? FALSE : uModel == ID_CSINI8L;
}

BOOL CCalibDlg::IsInV8(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMINV8 : uModel == ID_CSINV8;
}

BOOL CCalibDlg::IsInV8L(UINT uModel)
{
	return m_fGraphite ? FALSE : uModel == ID_CSINV8L;
}

BOOL CCalibDlg::IsUIn4(UINT uModel)
{
	return m_fGraphite ? uModel == ID_GMUIN4 : FALSE;
}

BOOL CCalibDlg::IsColdJunc(UINT uCode)
{
	return IsUIn4(GetModel()) ? uCode == 8 : uCode == CALIBCJ;
}

void CCalibDlg::EncodeChannel(BYTE &bCode)
{
	if( m_bCode != CALIBSAVE ) {

		UINT uChan = GetChannel();

		if( uChan > 1 ) {

			UINT uModel = GetModel();

			if( IsUIn4(uModel) ) {

				bCode |= (uChan - 1) << 6;
			}
			else {
				UINT uAdjust = m_bCode + (uChan * 10) + 10;

				bCode = BYTE(uAdjust);
			}
		}
	}
}

void CCalibDlg::DecodeChannel(BYTE &bCode)
{
	BYTE bChannel = GetChannel();

	if( bChannel > 1 ) {

		if( IsUIn4(GetModel()) ) {

			bCode &= 0x3F;
		}
		else {
			UINT uAdjust = bCode - (bChannel * 10) - 10;

			bCode = BYTE(uAdjust);
		}
	}
}

UINT CCalibDlg::GetSlot(void)
{
	CComboBox const &Box    = (CComboBox &) GetDlgItem(100);

	return Box.GetCurSelData();
}

UINT CCalibDlg::GetModule(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(100);

	return Box.GetCurSelData();
}

UINT CCalibDlg::GetModel(void)
{
	CComboBox const &Box = (CComboBox &) GetDlgItem(101);

	return Box.GetCurSelData();
}

BYTE CCalibDlg::GetChannel(void)
{
	CComboBox const &Box    = (CComboBox &) GetDlgItem(102);

	return BYTE(Box.GetCurSelData());
}

BYTE CCalibDlg::GetCode(void)
{
	CComboBox const &Box    = (CComboBox &) GetDlgItem(103);

	return BYTE(Box.GetCurSelData());
}

double CCalibDlg::GetFloat(void)
{
	return watof(GetDlgItem(104).GetWindowText());
}

WORD CCalibDlg::GetWord(void)
{
	return WORD(watoi(GetDlgItem(104).GetWindowText()));
}

void CCalibDlg::LoadConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"Calibration");

	Reg.MoveTo(m_fGraphite ? L"Graphite" : L"Legacy");

	m_uModel = Reg.GetValue(L"Model", m_fGraphite ? ID_GMINI8 : ID_CSINI8);

	m_uModule = Reg.GetValue(L"Module", UINT(0));
}

void CCalibDlg::SaveConfig(void)
{
	CRegKey Reg  = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"Calibration");

	Reg.MoveTo(m_fGraphite ? L"Graphite" : L"Legacy");

	Reg.SetValue(L"Model", UINT(GetModel()));

	Reg.SetValue(L"Module", UINT(GetModule()));
}

BOOL CCalibDlg::Abort(CString Msg)
{
	CString Text;

	Text += Msg;

	Text += L"\n\n";

	Text += CString(IDS_PROCEED_SURE);

	if( NoYes(Text) == IDNO ) {

		return TRUE;
	}

	return FALSE;
}

// End of File

