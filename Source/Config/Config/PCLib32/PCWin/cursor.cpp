
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cursor Object
//

// Dynamic Class

AfxImplementDynamicClass(CCursor, CUserObject);

// Constructors

CCursor::CCursor(void)
{
	}
	
CCursor::CCursor(CCursor const &That)
{
	AfxAssert(FALSE);
	}

CCursor::CCursor(ENTITY ID, int xSize, int ySize)
{
	CheckException(Create(ID, xSize, ySize));

	SetExtern(ID.IsStock());
	}

CCursor::CCursor(ENTITY ID, CSize const &Size)
{
	CheckException(Create(ID, Size));

	SetExtern(ID.IsStock());
	}

CCursor::CCursor(ENTITY ID)
{
	CheckException(Create(ID));

	SetExtern(ID.IsStock());
	}

// Destructor

CCursor::~CCursor(void)
{       
	Detach(TRUE);
	}

// Creation

BOOL CCursor::Create(ENTITY ID, int xSize, int ySize)
{
	Detach(TRUE);

	m_Size = CSize(xSize, ySize);

	HANDLE hObject = CModule::LoadCursor(ID);

	return Attach(hObject);
	}

BOOL CCursor::Create(ENTITY ID, CSize const &Size)
{
	Detach(TRUE);

	m_Size = Size;

	HANDLE hObject = CModule::LoadCursor(ID);

	return Attach(hObject);
	}

BOOL CCursor::Create(ENTITY ID)
{
	Detach(TRUE);

	HANDLE hObject = CModule::LoadCursor(ID);

	return Attach(hObject);
	}

// Attributes

CSize CCursor::GetSize(void) const
{
	return m_Size;
	}
	
// Handle Lookup

CCursor & CCursor::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CCursor NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CCursor &) CHandle::FromHandle(hObject, NS_HCURSOR, Class);
	}

// Handle Space

WORD CCursor::GetHandleSpace(void) const
{
	return NS_HCURSOR;
	}

// Destruction

void CCursor::DestroyObject(void)
{
	DestroyCursor(m_hObject);
	}

// End of File
