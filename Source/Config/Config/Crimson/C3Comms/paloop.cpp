
#include "intern.hpp"

#include "paloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Motion Controller Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CParkerAcroloopDeviceOptions, CUIItem);

// Constructor

CParkerAcroloopDeviceOptions::CParkerAcroloopDeviceOptions(void)
{
	m_Device = 0;
	}

// Download Support	     

BOOL CParkerAcroloopDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
		
	Init.AddByte(BYTE(m_Device));
			
	return TRUE;
	}

// Meta Data Creation

void CParkerAcroloopDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);
	} 


//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Motion Controller
//

// Instantiator

ICommsDriver *	Create_ParkerAcroloopDriver(void)
{
	return New CParkerAcroloopDriver;
	}

// Constructor

CParkerAcroloopDriver::CParkerAcroloopDriver(void)
{
	m_wID		= 0x3374;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Parker";
	
	m_DriverName	= "Acroloop Motion Controller";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Acroloop";

	AddSpaces();
	}

// Configuration

CLASS CParkerAcroloopDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CParkerAcroloopDeviceOptions);
	}


// Binding Control

UINT CParkerAcroloopDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CParkerAcroloopDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void CParkerAcroloopDriver::AddSpaces(void)
{
	AddSpace(New CSpaceAcro(1,	"PU",	"Global User Variable",		10, 0,  4095, addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpaceAcro(2,	"P",	"Global System Parameter",	10,4096,49152,addrLongAsLong, addrLongAsReal));
	AddSpace(New CSpaceAcro(3,	"BF",	"Global Bit Flag (Write Only)",	10, 0,  16660,addrBitAsBit,   addrBitAsBit));
	}

// Address Helpers

BOOL CParkerAcroloopDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

			Addr.a.m_Table |= addrNamed;

			return TRUE;
			}
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
		   0
		   );

	return FALSE;
	}

BOOL CParkerAcroloopDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CAddress Address = Addr;

	Address.a.m_Table = Addr.a.m_Table & 0x0F;

	if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Address) ) {

		return TRUE;
		}
	
	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CPAMasterTCPDeviceOptions, CParker6KTCPDeviceOptions);       

// Constructor

CPAMasterTCPDeviceOptions::CPAMasterTCPDeviceOptions(void)
{
	m_Socket = 5006;
	
	m_TimeWD = 10;
	
	m_TickWD = 3;

	m_Device = 0;
	}

// Download Support	     

BOOL CPAMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CParker6KTCPDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPAMasterTCPDeviceOptions::AddMetaData(void)
{
	CParker6KTCPDeviceOptions::AddMetaData();
	}


//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Master TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_PAMasterTCPDriver(void)
{
	return New CPAMasterTCPDriver;
	}

// Constructor

CPAMasterTCPDriver::CPAMasterTCPDriver(void)
{
	m_wID		= 0x4040;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Parker";
	
	m_DriverName	= "Acroloop TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Acroloop TCP/IP Master";
	}

// Binding Control

UINT CPAMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CPAMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CPAMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CPAMasterTCPDeviceOptions);
	}

// End of File


//////////////////////////////////////////////////////////////////////////
//
// Acroloop Space Wrapper
//

// Constructor

CSpaceAcro::CSpaceAcro(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s) : CSpace(uTable, p, c, r, n, x, t, s)
{
	
	}

// Matching

BOOL CSpaceAcro::MatchSpace(CAddress const &Addr)
{
	return m_uTable == (Addr.a.m_Table & 0x0F);
	}


// End of File
