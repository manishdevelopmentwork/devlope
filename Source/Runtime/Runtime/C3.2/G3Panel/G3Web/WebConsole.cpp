
#include "Intern.hpp"

#include "WebConsole.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Enhanced Web Server
//

// Constructor

CWebConsole::CWebConsole(void)
{
	m_pMutex = Create_Mutex();

	m_pDiag  = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	m_uCon   = m_pDiag->RegisterConsole(this);

	StdSetRef();

	m_fEnable = FALSE;
	}

// Destructor

CWebConsole::~CWebConsole(void)
{
	m_pDiag->RevokeConsole(m_uCon);

	m_pDiag->Release();

	m_pMutex->Release();
	}

// Operations

void CWebConsole::Enable(BOOL fEnable)
{
	m_pMutex->Wait(FOREVER);

	if( !(m_fEnable = fEnable) ) {

		m_Pend.Empty();

		m_List.Empty();
		}

	m_pMutex->Free();
	}

void CWebConsole::Read(CString &Text)
{
	m_pMutex->Wait(FOREVER);

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		Text += m_List[n];

		Text += '\n';
		}

	m_List.Empty();

	m_pMutex->Free();
	}

void CWebConsole::Exec(CString const &Cmd)
{
	m_pMutex->Wait(FOREVER);

	if( !m_Pend.IsEmpty() ) {

		Write("\n");
		}

	if( Cmd != "auto-hello" ) {

		m_List.Append("<span style='color: #FF0'>" + Cmd + "</span>");

		m_List.Append("");

		m_pMutex->Free();
	
		m_pDiag->RunCommand(this, Cmd);

		m_pMutex->Wait(FOREVER);
		}
	else {
		m_Color = "#0FF";

		m_pMutex->Free();

		m_pDiag->RunCommand(this, "hello");

		m_pMutex->Wait(FOREVER);

		m_Color.Empty();
		}

	m_List.Append("");

	m_pMutex->Free();
	}
	
// IUnknown

#if defined(AEON_ENVIRONMENT)

HRESULT CWebConsole::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagConsole);

	StdQueryInterface(IDiagConsole);

	return E_NOINTERFACE;
	}

ULONG CWebConsole::AddRef(void)
{
	StdAddRef();
	}

ULONG CWebConsole::Release(void)
{
	StdRelease();
	}

#endif

// IDiagConsole

void CWebConsole::Write(PCTXT pText)
{
	if( m_fEnable ) {

		m_pMutex->Wait(FOREVER);

		while( *pText ) {

			PCTXT pFind = strchr(pText, '\n');

			if( pFind ) {

				CString Add = m_Pend + CString(pText, pFind - pText);

				if( m_List.GetCount() > 1000 ) {

					m_List.Remove(0);
					}

				Add.Replace("&", "&amp;");

				Add.Replace(" ", "&nbsp;");

				Add.Replace("<", "&lt;");

				Add.Replace(">", "&gt;");

				if( m_Color.IsEmpty() ) {

					if( Add.StartsWith("error:&nbsp") ) {

						Add.Insert(0, "<span style='color: #F00'>");

						Add.Append("</span>");
						}
					}
				else {
					Add.Insert(0, "<span style='color: " + m_Color + "'>");

					Add.Append("</span>");
					}

				m_List.Append(Add);

				m_Pend.Empty();

				pText = pFind + 1;

				continue;
				}

			m_Pend += pText;

			break;
			}

		m_pMutex->Free();
		}
	}

// End of File
