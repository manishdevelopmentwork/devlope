 
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Tree View Item
//

// Constructors

CTabItem::CTabItem(void)
{
	memset((TCITEM *) this, 0, sizeof(TCITEM));
	}

CTabItem::CTabItem(CTabItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((TCITEM *) this, &That, sizeof(TCITEM));

	m_Text = That.m_Text;
	}

CTabItem::CTabItem(TCITEM const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((TCITEM *) this, &Item, sizeof(TCITEM));
	}

CTabItem::CTabItem(PCTXT pText, UINT uImage, LPARAM lParam)
{
	memset((TCITEM *) this, 0, sizeof(TCITEM));

	SetText(pText);

	SetImage(uImage);

	SetParam(lParam);
	}

CTabItem::CTabItem(PCTXT pText, UINT uImage)
{
	memset((TCITEM *) this, 0, sizeof(TCITEM));

	SetText(pText);

	SetImage(uImage);
	}

CTabItem::CTabItem(PCTXT pText)
{
	memset((TCITEM *) this, 0, sizeof(TCITEM));

	SetText(pText);
	}

CTabItem::CTabItem(UINT uMask)
{
	memset((TCITEM *) this, 0, sizeof(TCITEM));

	SetMask(uMask);
	}

// Assignment Operators

CTabItem const & CTabItem::operator = (CTabItem const &That)
{
	AfxValidateReadPtr(&That, sizeof(That));

	memcpy((TCITEM *) this, &That, sizeof(TCITEM));

	m_Text = That.m_Text;

	return ThisObject;
	}

CTabItem const & CTabItem::operator = (TCITEM const &Item)
{
	AfxValidateReadPtr(&Item, sizeof(Item));

	memcpy((TCITEM *) this, &Item, sizeof(TCITEM));

	return ThisObject;
	}

// Attributes

UINT CTabItem::GetMask(void) const
{
	return mask;
	}

BOOL CTabItem::TestMask(UINT uMask) const
{
	return (mask & uMask) ? TRUE : FALSE;
	}

UINT CTabItem::GetState(void) const
{
	AfxAssert(mask & TCIF_STATE);

	return dwState;
	}

UINT CTabItem::GetStateMask(void) const
{
	AfxAssert(mask & TCIF_STATE);

	return dwStateMask;
	}

PCTXT CTabItem::GetText(void) const
{
	AfxAssert(mask & TCIF_TEXT);

	return pszText;
	}

UINT CTabItem::GetImage(void) const
{
	AfxAssert(mask & TCIF_IMAGE);

	return iImage;
	}

LPARAM CTabItem::GetParam(void) const
{
	AfxAssert(mask & TCIF_PARAM);

	return lParam;
	}

// Operations

void CTabItem::SetMask(UINT uMask)
{
	mask = uMask;
	}

void CTabItem::SetState(UINT uState)
{
	mask |= TCIF_STATE;

	dwState = uState;
	}

void CTabItem::SetStateMask(UINT uMask)
{
	mask |= TCIF_STATE;

	dwStateMask = uMask;
	}

void CTabItem::SetImage(UINT uImage)
{
	mask |= TCIF_IMAGE;

	iImage = uImage;
	}

void CTabItem::SetText(PCTXT pText)
{
	mask |= TCIF_TEXT;

	m_Text     = pText;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CTabItem::SetText(CString const &Text)
{
	mask |= TCIF_TEXT;

	m_Text     = Text;

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CTabItem::SetTextBuffer(UINT uCount)
{
	mask |= TCIF_TEXT;

	m_Text     = CString(' ', uCount);

	pszText    = PTXT(PCTXT(m_Text));

	cchTextMax = m_Text.GetLength();
	}

void CTabItem::SetParam(LPARAM uParam)
{
	mask |= TCIF_PARAM;

	lParam = uParam;
	}

// End of File
