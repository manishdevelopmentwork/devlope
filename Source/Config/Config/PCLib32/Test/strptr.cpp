
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

// Constructors

CStrPtr::CStrPtr(CStrPtr const &That)
{
	That.AssertValid();
	
	m_pText = That.m_pText;
	}
	
CStrPtr::CStrPtr(CString const &That)
{
	AfxAssert(FALSE);
	}

CStrPtr::CStrPtr(PCTXT pText)
{
	AfxValidateStringPtr(pText);
	
	m_pText = (PTXT) pText;
	}

// Conversion

/*CStrPtr::operator PCTXT (void) const
{
	return m_pText;
	}*/

// Attributes

BOOL CStrPtr::IsEmpty(void) const
{
	return !*m_pText;
	}
	
BOOL CStrPtr::operator ! (void) const
{
	return !*m_pText;
	}

UINT CStrPtr::GetLength(void) const
{
	return wstrlen(m_pText);
	}

UINT CStrPtr::GetHashValue(void) const
{
	UINT uHash = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {
	
		uHash = (uHash << 5) + uHash;
		
		uHash = uHash + m_pText[n];
		}
		
	return uHash;
	}

// Read-Only Access

TCHAR CStrPtr::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : TCHAR(0);
	}

// Array Conversion

UINT CStrPtr::GetCharCodes(CWordArray &Code)
{
	UINT uLen = GetLength();

	Code.Empty();

	Code.Expand(uLen);

	for( UINT n = 0; n < uLen; n++ ) {

		Code.Append(m_pText[n]);
		}

	return uLen;
	}

UINT CStrPtr::GetTypeInfo1(CWordArray &Type)
{
	return GetTypeInfo(Type, CT_CTYPE1);
	}

UINT CStrPtr::GetTypeInfo2(CWordArray &Type)
{
	return GetTypeInfo(Type, CT_CTYPE2);
	}

UINT CStrPtr::GetTypeInfo3(CWordArray &Type)
{
	return GetTypeInfo(Type, CT_CTYPE3);
	}

UINT CStrPtr::GetTypeInfo(CWordArray &Type, UINT uType)
{
	UINT uLen = GetLength();

	if( uLen ) {

		Type.Empty();
	
		Type.SetCount(uLen);

		GetStringTypeEx( LOCALE_USER_DEFAULT,
				 uType,
				 m_pText,
				 uLen,
				 PWORD(Type.GetPointer())
				 );
		}

	return uLen;
	}

// Comparison Functions

int CStrPtr::CompareC(PCTXT pText) const
{
	AfxValidateStringPtr(pText);
	
	return wstrcmp(m_pText, pText);
	}

int CStrPtr::CompareN(PCTXT pText) const
{
	AfxValidateStringPtr(pText);
	
	return wstricmp(m_pText, pText);
	}

int CStrPtr::CompareC(PCTXT pText, UINT uCount) const
{
	AfxValidateStringPtr(pText);
	
	return wstrncmp(m_pText, pText, uCount);
	}

int CStrPtr::CompareN(PCTXT pText, UINT uCount) const
{
	AfxValidateStringPtr(pText);
	
	return wstrnicmp(m_pText, pText, uCount);
	}      

int CStrPtr::GetScore(PCTXT pText) const
{
	if( wstricmp(m_pText, pText) ) {
	
		UINT n = wstrlen(pText);
		
		if( wstrnicmp(m_pText, pText, n) ) {

			return 0;
			}
			
		return n;
		}
		
	return 10000;
	}

// Legacy Comparison

BOOL CStrPtr::operator == (PCSTR pText) const
{
	return operator == (PCTXT(CString(pText)));
	}

BOOL CStrPtr::operator != (PCSTR pText) const
{
	return operator != (PCTXT(CString(pText)));
	}

// Comparison Helper

int AfxCompare(CStrPtr const &a, CStrPtr const &b)
{
	return wstricmp(a.m_pText, b.m_pText);
	}

// Comparison Operators

BOOL CStrPtr::operator == (PCTXT pText) const
{
	return CompareN(pText) == 0;
	}

BOOL CStrPtr::operator != (PCTXT pText) const
{
	return CompareN(pText) != 0;
	}

BOOL CStrPtr::operator < (PCTXT pText) const
{
	return CompareN(pText) < 0;
	}

BOOL CStrPtr::operator > (PCTXT pText) const
{
	return CompareN(pText) > 0;
	}

BOOL CStrPtr::operator <= (PCTXT pText) const
{
	return CompareN(pText) <= 0;
	}

BOOL CStrPtr::operator >= (PCTXT pText) const
{
	return CompareN(pText) >= 0;
	}

// Substring Extraction

CString CStrPtr::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);

	return CString(m_pText, uCount);
	}

CString CStrPtr::Right(UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uCount, uLen);

	PCTXT pSrc = m_pText + uLen - uCount;

	return CString(pSrc, uCount);
	}

CString CStrPtr::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	MakeMin(uCount, uLen - uPos);

	PCTXT pSrc = m_pText + uPos;

	return CString(pSrc, uCount);
	}

CString CStrPtr::Mid(UINT uPos) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	PCTXT pSrc = m_pText + uPos;

	return CString(pSrc, uLen - uPos);
	}

// Case Conversion

CString CStrPtr::ToUpper(void) const
{
	CString Result(ThisObject);

	Result.MakeUpper();

	return Result;
	}

CString CStrPtr::ToLower(void) const
{
	CString Result(ThisObject);

	Result.MakeLower();

	return Result;
	}

// Searching

UINT CStrPtr::Find(TCHAR cChar) const
{
	AfxAssert(cChar);
	
	PTXT pFind = wstrchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, TCHAR cQuote) const
{
	AfxAssert(cChar);

	AfxAssert(cQuote);

	int s = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cQuote ) {

			s = !s;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, TCHAR cOpen, TCHAR cClose) const
{
	AfxAssert(cChar);

	AfxAssert(cOpen);

	AfxAssert(cClose);

	int s = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cOpen ) {

			s++;
			}

		if( c == cClose ) {

			s--;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());
	
	PTXT pFind = wstrchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::Find(TCHAR cChar, UINT uPos, TCHAR cQuote) const
{
	AfxAssert(cChar);

	AfxAssert(cQuote);

	AfxAssert(uPos <= GetLength());

	int s = 0;

	for( UINT n = uPos; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cQuote ) {

			s = !s;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, UINT uPos, TCHAR cOpen, TCHAR cClose) const
{
	AfxAssert(cChar);

	AfxAssert(cOpen);

	AfxAssert(cClose);

	AfxAssert(uPos <= GetLength());

	int s = 0;

	for( UINT n = uPos; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cOpen ) {

			s++;
			}

		if( c == cClose ) {

			s--;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::FindRev(TCHAR cChar) const
{
	AfxAssert(cChar);

	PTXT pFind = wstrrchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::FindRev(TCHAR cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());

	PTXT pFind = wstrrchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::Find(PCTXT pText) const
{
        AfxValidateStringPtr(pText);
			
	PTXT pFind = wstrstr(m_pText, pText);
		
	return pFind ? pFind - PTXT(m_pText) : NOTHING;
	}
	
UINT CStrPtr::Find(PCTXT pText, UINT uPos) const
{
        AfxValidateStringPtr(pText);
			
	AfxAssert(uPos <= GetLength());

	PTXT pFind = wstrstr(m_pText + uPos, pText);
		
	return pFind ? pFind - PTXT(m_pText) : NOTHING;
	}
	
UINT CStrPtr::FindNot(PCTXT pList) const
{
	AfxValidateStringPtr(pList);

	UINT uFind = wstrspn(m_pText, pList);
	
	return uFind;
	}

UINT CStrPtr::FindNot(PCTXT pList, UINT uPos) const
{
	AfxValidateStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = wstrspn(m_pText + uPos, pList);
	
	return uFind ? uFind + uPos : 0;
	}

UINT CStrPtr::FindOne(PCTXT pList) const
{
	AfxValidateStringPtr(pList);

	UINT uFind = wstrcspn(m_pText, pList);
	
	return uFind;
	}

UINT CStrPtr::FindOne(PCTXT pList, UINT uPos) const
{
	AfxValidateStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = wstrcspn(m_pText + uPos, pList);
	
	return uFind ? uFind + uPos : 0;
	}

// Searching

UINT CStrPtr::Search(PCTXT pText, UINT uMethod) const
{
	return Search(pText, 0, uMethod);
	}

UINT CStrPtr::Search(PCTXT pText, UINT uFrom, UINT uMethod) const
{
	UINT uFind;

	switch( LOBYTE(uMethod) ) {

		case searchIsEqualTo:

			if( uMethod & searchMatchCase ) {

				if( !wstrcmp(pText, m_pText) ) {

					return 0;
					}

				return NOTHING;
				}

			if( !wstricmp(pText, m_pText) ) {

				return 0;
				}

			return NOTHING;

		case searchStartsWith:

			if( uMethod & searchMatchCase ) {

				if( !wstrncmp(pText, m_pText, wstrlen(pText)) ) {

					return 0;
					}

				return NOTHING;
				}

			if( !wstrnicmp(pText, m_pText, wstrlen(pText)) ) {

				return 0;
				}

			return NOTHING;

		case searchContains:

			uFind = wstrlen(pText);

			for(;;) {

				PCTXT pFind;

				if( uMethod & searchMatchCase ) {

					pFind = wstrstr (m_pText + uFrom, pText);
					}
				else
					pFind = wstristr(m_pText + uFrom, pText);

				if( pFind ) {

					if( uMethod & searchWholeWords ) {

						if( pFind > m_pText + uFrom && isalnum(pFind[-1]) ) {

							uFrom = pFind - m_pText + uFind;

							continue;
							}

						if( pFind[uFind] && isalnum(pFind[uFind]) ) {

							uFrom = pFind - m_pText + uFind;

							continue;
							}
						}

					return pFind - m_pText;
					}

				return NOTHING;
				}

			break;
		}

	return NOTHING;
	}

// Partials

BOOL CStrPtr::StartsWith(PCTXT pText) const
{
	UINT c1 = wstrlen(m_pText);

	UINT c2 = wstrlen(pText);

	if( c1 >= c2 ) {

		return !wstrnicmp(m_pText, pText, c2);
		}

	return FALSE;
	}

BOOL CStrPtr::EndsWith(PCTXT pText) const
{
	UINT c1 = wstrlen(m_pText);

	UINT c2 = wstrlen(pText);

	if( c1 >= c2 ) {

		return !wstrnicmp(m_pText + c1 - c2, pText, c2);
		}

	return FALSE;
	}

// Counting

UINT CStrPtr::Count(TCHAR cChar) const
{
	UINT c = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cChar ) {

			c++;
			}
		}
		
	return c;
	}

// Diagnostics

void CStrPtr::AssertValid(void) const
{
	AfxValidateReadPtr(this, sizeof(ThisObject));
	
	AfxValidateStringPtr(m_pText);
	}

// End of File
