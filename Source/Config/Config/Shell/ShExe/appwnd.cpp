
#include "intern.hpp"

#include "update.hpp"

#include "register.hpp"

#include "ModelInfo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application Window
//

// Runtime Class

AfxImplementRuntimeClass(CCrimsonWnd, CMainWnd);

// Constructor

CCrimsonWnd::CCrimsonWnd(void)
{
	m_IconL.Create(L"C3App", 32, 32);

	m_IconS.Create(L"C3App", 16, 16);

	m_Accel.Create(L"MainMenu");

	m_uCommand   = cmdNone;

	m_fOnline    = C3OemFeature(L"OnlineMode", FALSE);

	m_WndClass   = CPrintf(L"c3shell_wnd_%8.8X", GetCurrentProcessId());

	m_pView      = New CCrimsonView(this);

	m_fEatUpdate = FALSE;

	m_fShowSB    = TRUE;

	SetView(m_pView);

	ShowEdge(FALSE);
}

// Destructor

CCrimsonWnd::~CCrimsonWnd(void)
{
}

// Initialization

BOOL CCrimsonWnd::Initialize(void)
{
	if( ParseCommandLine() ) {

		if( m_uCommand == cmdNone ) {

			if( m_LoadFile.GetLength() ) {

				m_LoadFile.MakeLong();

				if( OpenFile(m_LoadFile, FALSE) ) {

					return TRUE;
				}

				return FALSE;
			}

			CheckRecover();
		}

		if( m_uCommand == cmdSend || m_uCommand == cmdUpdate ) {

			if( m_LoadFile.GetLength() ) {

				SendCommand(m_uCommand == cmdUpdate);

				return FALSE;
			}
		}

		return TRUE;
	}

	return FALSE;
}

void CCrimsonWnd::ShowWindow(void)
{
	if( m_ShowZoom && m_uInst == 1 ) {

		CWnd::ShowWindow(SW_MAXIMIZE);
	}
	else
		CWnd::ShowWindow(SW_SHOWNOACTIVATE);

	SetFocus();
}

// Attributes

CDatabase * CCrimsonWnd::GetDatabase()
{
	return m_pView->GetDatabase();
}

// Operations

BOOL CCrimsonWnd::AddRecent(CFilename const &Name)
{
	if( !m_fOnline ) {

		SHAddToRecentDocs(SHARD_PATHW, Name);

		m_Recent.AddFile(Name);

		return TRUE;
	}

	return FALSE;
}

void CCrimsonWnd::UpdateTitleBar(void)
{
	CString Title;

	Title += m_pView->GetCaption();

	Title += L" - ";

	Title += m_pView->GetDisplayName();

	Title += L" - ";

	Title += CString(IDS_APP_CAPTION);

	Title += m_Unreg;

	CString Locked = m_pView->GetDatabase()->GetLockedName();

	if( !Locked.IsEmpty() ) {

		Title += L" - ";

		Title += L"(";

		Title += Locked;

		Title += L")";
	}

	SetWindowText(Title);
}

BOOL CCrimsonWnd::CheckReg(void)
{
	if( CString(C3_BRANCH).Find(L"Gold") == NOTHING ) {

		m_Unreg = IDS(" - BETA SOFTWARE");

		UpdateTitleBar();

		return FALSE;
	}
	else {
		CRegKey Reg = afxModule->GetUserRegKey();

		Reg.MoveTo(L"Registration");

		if( !Reg.GetValue(L"Status", UINT(0)) ) {

			m_Unreg = IDS_UNREGISTERED;

			UpdateTitleBar();

			return FALSE;
		}

		return TRUE;
	}
}

// Interface Control

void CCrimsonWnd::OnUpdateInterface(void)
{
	LRESULT lResult = 0;

	CMenu Tool;

	CMenu Menu;

	Tool.CreateMenu();

	Menu.CreateMenu();

	CMenu HeadTool(L"HeadTool");

	CMenu HeadMenu(L"HeadMenu");

	AddExtraCmds(HeadMenu.GetSubMenu(0));

	Tool.AppendMenu(HeadTool);

	Menu.AppendMenu(HeadMenu);

	MSG MsgTool = { NULL, WM_LOADTOOL, 0, LPARAM(&Tool) };

	MSG MsgMenu = { NULL, WM_LOADMENU, 0, LPARAM(&Menu) };

	MSG MsgShow = { NULL, WM_SHOWUI,   1, 0 };

	RouteMessage(MsgTool, lResult);

	RouteMessage(MsgMenu, lResult);

	RouteMessage(MsgShow, lResult);

	Tool.AppendMenu(CMenu(L"TailTool"));

	Menu.AppendMenu(CMenu(L"TailMenu"));

	m_pTB->AddFromMenu(Tool, 0);

	AdjustMenu(Menu);

	Menu.MakeOwnerDraw(TRUE);

	SetMenu(Menu);
}

void CCrimsonWnd::OnCreateStatusBar(void)
{
	m_pSB->AddGadget(New CHardwareGadget(this, 204));

	m_pSB->AddGadget(New CErrorsGadget(this, 200));

	m_pSB->AddGadget(New CCircleGadget(this, 201));

	m_pSB->AddGadget(New CTextGadget(203, CString(IDS_TRANSLATE), textNormal));

	m_pSB->AddGadget(New CTextGadget(202, CString(IDS_READ), textNormal));
}

void CCrimsonWnd::OnUpdateStatusBar(void)
{
	if( m_pView->IsKindOf(AfxRuntimeClass(CSystemWnd)) ) {

		CSystemWnd *pSysWnd = (CSystemWnd *) m_pView;

		if( pSysWnd->GetSemiModal() ) {

			return;
		}
	}

	m_pSB->GetGadget(200)->AdjustFlag(MF_DISABLED, !GetDatabase()->GetRecomp());

	m_pSB->GetGadget(201)->AdjustFlag(MF_DISABLED, !GetDatabase()->GetCircle());

	m_pSB->GetGadget(202)->AdjustFlag(MF_DISABLED, !GetDatabase()->IsReadOnly());

	m_pSB->GetGadget(203)->AdjustFlag(MF_DISABLED, !GetDatabase()->GetPending());

	m_pSB->GetGadget(204)->AdjustFlag(MF_DISABLED, !GetDatabase()->GetSystemItem()->HasHardware());
}

// Message Procedure

LRESULT CCrimsonWnd::DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	return CMainWnd::DefProc(uMessage, wParam, lParam);
}

// Class Definition

PCTXT CCrimsonWnd::GetDefaultClassName(void) const
{
	return m_WndClass;
}

BOOL CCrimsonWnd::GetClassDetails(WNDCLASSEX &Class) const
{
	if( CWnd::GetClassDetails(Class) ) {

		Class.hIcon   = m_IconL.GetHandle();

		Class.hIconSm = m_IconS.GetHandle();

		return TRUE;
	}

	return FALSE;
}

// Message Map

AfxMessageMap(CCrimsonWnd, CMainWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_QUERYENDSESSION)
	AfxDispatchMessage(WM_GETMINMAXINFO)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_MOVE)
	AfxDispatchMessage(WM_ENABLE)

	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxDispatchGetInfoType(IDM_FILE, OnFileGetInfo)
	AfxDispatchControlType(IDM_FILE, OnFileControl)
	AfxDispatchCommandType(IDM_FILE, OnFileExecute)
	AfxDispatchControlType(IDM_LINK, OnLinkControl)
	AfxDispatchCommandType(IDM_LINK, OnLinkExecute)
	AfxDispatchGetInfoType(IDM_HELP, OnHelpGetInfo)
	AfxDispatchControlType(IDM_HELP, OnHelpControl)
	AfxDispatchCommandType(IDM_HELP, OnHelpExecute)
	AfxDispatchControlType(IDM_WARN, OnWarnControl)
	AfxDispatchCommandType(IDM_WARN, OnWarnExecute)

	AfxMessageEnd(CCrimsonWnd)
};

// Accelerators

BOOL CCrimsonWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
}

// Message Handlers

void CCrimsonWnd::OnPostCreate(void)
{
	LoadModel();

	LoadDisambig();

	CDatabase *pDbase = NULL;

	InitToDefault(pDbase);

	pDbase->ClearDirty();

	m_pView->Attach(pDbase);

	m_pView->SetNewFile();

	CMainWnd::OnPostCreate();

	if( m_fOnline ) {

		while( m_Recent.GetCount() ) {

			CString File = m_Recent.GetFile(0);

			m_Recent.RemoveFile(File);
		}
	}

	m_uInst = GetCrimsonCount();

	FindWindowPos();

	PromptNew();
}

void CCrimsonWnd::OnPreDestroy(void)
{
	SaveWindowPos();
}

void CCrimsonWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		m_pView->SendMessage(m_MsgCtx.Msg.message,
				     m_MsgCtx.Msg.wParam,
				     m_MsgCtx.Msg.lParam
		);
	}

	CMainWnd::OnInitPopup(Menu, nIndex, fSystem);
}

void CCrimsonWnd::OnClose(void)
{
	if( m_pView->CheckSave() ) {

		DestroyWindow(FALSE);
	}
}

BOOL CCrimsonWnd::OnQueryEndSession(void)
{
	if( m_MsgCtx.Msg.lParam & 1 ) {

		// LATER -- Save file to temporary and then
		// register for restart, reloading the file
		// and resetting our filename context etc.

		return TRUE;
	}

	if( m_pView->CheckSave() ) {

		return TRUE;
	}

	return FALSE;
}

void CCrimsonWnd::OnGetMinMaxInfo(MINMAXINFO &Info)
{
	Info.ptMinTrackSize.x = 300;

	Info.ptMinTrackSize.y = 240;
}

void CCrimsonWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED ) {

		m_SaveZoom = TRUE;
	}

	if( uCode == SIZE_RESTORED ) {

		m_SaveRect = GetWindowRect();

		m_SaveZoom = FALSE;
	}

	CMainWnd::OnSize(uCode, Size);
}

void CCrimsonWnd::OnMove(CPoint Pos)
{
	if( !IsZoomed() ) {

		if( !IsIconic() ) {

			m_SaveRect = GetWindowRect();
		}
	}

	CMainWnd::OnMove(Pos);
}

void CCrimsonWnd::OnEnable(BOOL fEnable)
{
	afxMainWnd->RouteCommand(fEnable ? IDM_GLOBAL_ENABLE : IDM_GLOBAL_DISABLE, 0);
}

// Command Handlers

BOOL CCrimsonWnd::OnViewRefresh(UINT uID)
{
	return FALSE;
}

BOOL CCrimsonWnd::OnFileGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_FILE_EXTRA && uID < IDM_FILE_RECENT ) {

		Info.m_Prompt = L"";

		return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::OnFileControl(UINT uID, CCmdSource &Src)
{
	if( uID >= IDM_FILE_RECENT && uID < IDM_FILE_RECENT + 10 ) {

		if( !m_fOnline ) {

			RouteControl(IDM_FILE_OPEN, Src);
		}

		return TRUE;
	}

	if( uID >= IDM_FILE_EXTRA && uID < IDM_FILE_RECENT ) {

		CDatabase   *pDbase  = GetDatabase();

		CSystemItem *pSystem = pDbase->GetSystemItem();

		UINT         uCmd    = uID - IDM_FILE_EXTRA;

		Src.EnableItem(pSystem->GetExtraState(uCmd));

		return TRUE;
	}

	switch( uID ) {

		case IDM_FILE_NEW:

			Src.EnableItem(!m_fOnline);

			break;

		case IDM_FILE_OPEN:

			Src.EnableItem(!m_fOnline);

			break;

		case IDM_FILE_CONVERT:

			Src.EnableItem(TRUE);

			break;

		case IDM_FILE_UPDATE:

			Src.EnableItem(TRUE);

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::OnFileExecute(UINT uID)
{
	if( uID >= IDM_FILE_RECENT && uID < IDM_FILE_RECENT + 10 ) {

		CString File = m_Recent.GetFileFromID(uID);

		OpenFile(File, FALSE);

		return TRUE;
	}

	if( uID >= IDM_FILE_EXTRA && uID < IDM_FILE_RECENT ) {

		CDatabase   *pDbase  = GetDatabase();

		CSystemItem *pSystem = pDbase->GetSystemItem();

		UINT         uCmd    = uID - IDM_FILE_EXTRA;

		pSystem->RunExtraCmd(ThisObject, uCmd);

		return TRUE;
	}

	switch( uID ) {

		case IDM_FILE_NEW:

			OnFileNew();

			break;

		case IDM_FILE_OPEN:

			OnFileOpen();

			break;

		case IDM_FILE_CONVERT:

			OnFileConvert();

			break;

		case IDM_FILE_UPDATE:

			OnFileUpdate();

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::OnLinkControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_LINK_UPLOAD:
		case IDM_LINK_ONLINE:

			Src.EnableItem(CanLinkUpload());

			return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::OnLinkExecute(UINT uID)
{
	switch( uID ) {

		case IDM_LINK_UPLOAD:

			OnLinkUpload();

			return TRUE;

		case IDM_LINK_ONLINE:

			return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::OnHelpGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{ IDM_HELP_BUTTON,   MAKELONG(0x000D, 0x4000),

	};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
		}
	}

	return FALSE;
}

BOOL CCrimsonWnd::OnHelpControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_HELP_CONTENTS:

			Src.EnableItem(HasContents());

			break;

		case IDM_HELP_REFERENCE:

			Src.EnableItem(HasReference());

			break;

		case IDM_HELP_UPDATE:

			Src.EnableItem(C3OemFeature(L"Update", TRUE));

			break;

		case IDM_HELP_SUPPORT:

			Src.EnableItem(C3OemFeature(L"Support", TRUE));

			break;

		case IDM_HELP_NOTES:

			Src.EnableItem(C3OemFeature(L"BuildNotes", TRUE));

			break;

		case IDM_HELP_REGISTER:

			Src.EnableItem(C3OemFeature(L"Registration", TRUE));

			break;

		case IDM_HELP_ABOUT:

			Src.EnableItem(TRUE);

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::OnHelpExecute(UINT uID)
{
	switch( uID ) {

		case IDM_HELP_CONTENTS:

			return OnHelpContents();

		case IDM_HELP_REFERENCE:

			return OnHelpReference();

		case IDM_HELP_SUPPORT:

			return OnHelpSupport();

		case IDM_HELP_NOTES:

			return OnHelpNotes();

		case IDM_HELP_UPDATE:

			return OnHelpUpdate();

		case IDM_HELP_REGISTER:

			if( CString(C3_BRANCH).Find(L"Gold") < NOTHING ) {

				if( m_Unreg.IsEmpty() ) {

					CString Text = IDS_REG_AMEND;

					if( NoYes(Text) == IDNO ) {

						return TRUE;
					}
				}

				if( CRegisterDialog().Check(ThisObject, TRUE) ) {

					m_Unreg.Empty();

					UpdateTitleBar();
				}
			}

			return TRUE;

		case IDM_HELP_ABOUT:

			return OnHelpAbout();

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::OnWarnControl(UINT uID, CCmdSource &Src)
{
	CDatabase   *pDbase  = GetDatabase();

	CSystemItem *pSystem = pDbase->GetSystemItem();

	switch( uID ) {

		case IDM_WARN_SHOW_ERROR:

			Src.EnableItem(pSystem->HasBroken());

			break;

		case IDM_WARN_SHOW_CIRCLE:

			Src.EnableItem(pSystem->HasCircular());

			break;

		case IDM_WARN_RECOMPILE:
		case IDM_WARN_REBLOCK_COMMS:
		case IDM_WARN_REMAP_PERSIST:

			Src.EnableItem(!pDbase->IsReadOnly());

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::OnWarnExecute(UINT uID)
{
	CDatabase   *pDbase  = GetDatabase();

	CSystemItem *pSystem = pDbase->GetSystemItem();

	switch( uID ) {

		case IDM_WARN_SHOW_ERROR:

			pSystem->FindBroken();

			break;

		case IDM_WARN_SHOW_CIRCLE:

			pSystem->FindCircular();

			break;

		case IDM_WARN_RECOMPILE:

			pSystem->Rebuild(0);

			break;

		case IDM_WARN_REBLOCK_COMMS:

			pSystem->Rebuild(2);

			break;

		default:
			return FALSE;
	}

	return TRUE;
}

// Core File Commands

BOOL CCrimsonWnd::OnFileNew(void)
{
	if( !m_pView || m_pView->CheckSave() ) {

		if( CModelInfo::SelectModel(0, m_Model) ) {

			NewModel();

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CCrimsonWnd::OnFileOpen(void)
{
	COpenFileDialog Dialog;

	CModule * pApp = afxModule->GetApp();

	CFilename Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Databases");

	CString   Type = L"Prime";

	Dialog.SetFilter(CString(IDS_LOAD_FILTER));

	Dialog.AllowReadOnly();

	Dialog.LoadLastPath(Type, Path);

	if( Dialog.Execute(ThisObject) ) {

		UpdateWindow();

		OpenFile(Dialog.GetFilename(), Dialog.IsReadOnly());

		Dialog.SaveLastPath(Type);

		return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::OnFileConvert(void)
{
	if( !m_pView->IsDirty() && !m_pView->HasFilename() ) {

		return OnFileNew();
	}
	else {
		CSystemItem *pSystem = GetDatabase()->GetSystemItem();

		CString Model = pSystem->GetModelSpec();

		CString Check = Model;

		if( CModelInfo::SelectModel(1, Model) ) {

			if( Model != Check ) {

				if( m_pView->KillUndoList() ) {

					pSystem->SetModelSpec(Model);

					SaveModel();

					m_pView->SetCaption();

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// File Menu Update

void CCrimsonWnd::AddExtraCmds(CMenu &Menu)
{
	UINT uCount = Menu.GetMenuItemCount();

	for( UINT p = 0; p < uCount; p++ ) {

		if( Menu.GetMenuItemID(p) == NOTHING ) {

			CDatabase   *pDbase  = GetDatabase();

			CSystemItem *pSystem = pDbase->GetSystemItem();

			UINT         uExtra  = pSystem->GetExtraCount();

			if( uExtra ) {

				CMenu &Sub = Menu.GetSubMenu(p);

				Sub.DeleteMenu(0, MF_BYPOSITION);

				for( UINT n = 0; n < uExtra; n++ ) {

					CString Text = pSystem->GetExtraText(n);

					if( Text == L"-" ) {

						Sub.AppendSeparator();
					}
					else {
						UINT    uID  = IDM_FILE_EXTRA + n;

						Sub.AppendMenu(0, uID, Text);
					}
				}
			}
			else {
				Menu.DeleteMenu(p, MF_BYPOSITION);

				Menu.DeleteMenu(p, MF_BYPOSITION);
			}

			break;
		}
	}
}

BOOL CCrimsonWnd::OnFileUpdate(void)
{
	if( !m_fEatUpdate ) {

		CDatabase   *pDbase  = m_pView->GetDatabase();

		CSystemItem *pSystem = pDbase->GetSystemItem();

		BOOL         fHidden = pDbase->IsDownloadOnly();

		pSystem->UpdateHardware();

		m_pView->Detach();

		m_pView = fHidden ? New CHiddenView(this, CString(IDS_DOWNLOAD_MODE)) : New CCrimsonView(this);

		m_pView->Attach(pDbase);

		SetView(m_pView);

		pSystem->ShowHardware();

		DoUpdateUI();
	}

	m_fEatUpdate = FALSE;

	return TRUE;
}

// Link Commands

BOOL CCrimsonWnd::CanLinkUpload(void)
{
	#if 0

	if( IsEtherTrak3() ) {

		return Et3Link_CanUpload();
	}

	#endif

	return TRUE;
}

void CCrimsonWnd::OnLinkUpload(void)
{
	if( m_pView->CheckSave() ) {

		m_pView->ClearDirty();

		CFilename File;

		if( LinkUpload(File) ) {

			if( !File.IsEmpty() ) {

				// TODO -- We ought to be able to extract both C3.1
				// and C3.2 databases here, and maybe even older...

				if( File.GetType() == CString(IDS_CD32).Mid(1) ) {

					OpenFile(File, FALSE, FALSE);

					AddRecent(File);
				}
			}
		}
	}
}

// Help Commands

BOOL CCrimsonWnd::OnHelpContents(void)
{
	UINT uIndex = 0;

	#if 0

	if( IsEtherTrak3() ) {

		uIndex = 2;
	}

	#endif

	UpdateWindow();

	CPdfHelpSystem pdf(uIndex);

	pdf.ShowHelp();

	return TRUE;
}

BOOL CCrimsonWnd::OnHelpReference(void)
{
	UpdateWindow();

	CPdfHelpSystem pdf(1);

	pdf.ShowHelp();

	return TRUE;
}

BOOL CCrimsonWnd::OnHelpSupport(void)
{
	CSupportDialog Dlg(C3OemFeature(L"Support", FALSE));

	Dlg.Execute(ThisObject);

	return TRUE;
}

BOOL CCrimsonWnd::OnHelpNotes(void)
{
	UpdateWindow();

	CString Web = L"https://www.redlion.net/node/39558";

	C3OemStrings(Web);

	ShellExecute(ThisObject,
		     L"open",
		     Web,
		     NULL,
		     NULL,
		     SW_SHOWNORMAL
	);

	return TRUE;
}

BOOL CCrimsonWnd::OnHelpUpdate(void)
{
	if( CString(C3_BRANCH).Find(L"Gold") < NOTHING ) {

		if( m_pView->CheckSave() ) {

			CUpdateDialog Dlg(FALSE);

			Dlg.Execute(ThisObject);
		}
	}

	return TRUE;
}

BOOL CCrimsonWnd::OnHelpAbout(void)
{
	CRegKey Key     = afxModule->GetApp()->GetMachineRegKey();

	CString Version = Key.GetValue(L"Version", L"3.2");

	CString Branch  = CString(C3_BRANCH);

	Branch.Delete(0, Branch.Find('-') + 1);

	Branch.Delete(Branch.Find('-'), NOTHING);

	C3OemStrings(Version);

	CPrintf Text(IDS_APP_BUILD,
		     Version,
		     Branch,
		     C3_BUILD,
		     C3_HOTFIX,
		     L"\x00A9",
		     2020,
		     L"Red Lion Controls Inc"
	);

	if( C3OemFeature(L"PoweredBy", FALSE) ) {

		Text += L"\n\n\n\n\n\n\n\n\n@@@";
	}

	CAboutBox Box(Text);

	Box.ChangeIcon(L"C3App");

	Box.Execute(ThisObject);

	return TRUE;
}

// Model Management

BOOL CCrimsonWnd::LoadModel(void)
{
	CRegKey Reg = afxModule->GetUserRegKey();

	Reg.MoveTo(L"Models");

	m_Model = Reg.GetValue(L"Current", L"");

	CModelInfo::CheckModel(m_Model);

	return TRUE;
}

void CCrimsonWnd::SaveModel(void)
{
	m_Model     = m_pView->GetFullModel();

	CRegKey Reg = afxModule->GetUserRegKey();

	Reg.MoveTo(L"Models");

	if( m_Model.Count(L'|') == 4 ) {

		CString Model = m_Model.Left(m_Model.FindRev('|'));

		Reg.SetValue(L"Current", Model);

		return;
	}

	Reg.SetValue(L"Current", m_Model);
}

void CCrimsonWnd::NewModel(void)
{
	CDatabase *pDbase = NULL;

	InitToDefault(pDbase);

	m_pView = New CCrimsonView(this);

	m_pView->Attach(pDbase);

	SaveModel();

	m_pView->SetNewFile();

	SetView(m_pView);

	DoUpdateUI();

	PromptNew();
}

// Implementation

void CCrimsonWnd::LoadDisambig(void)
{
	CRegKey Reg = afxModule->GetUserRegKey();

	Reg.MoveTo(L"C3Look\\NavTree");

	CNamedList::m_Disambig = Reg.GetValue(L"DupSuffix", L"_");

	Reg.SetValue(L"DupSuffix", CNamedList::m_Disambig);
}

BOOL CCrimsonWnd::AdjustMenu(CMenu &Menu)
{
	UINT  uCount = Menu.GetMenuItemCount();

	UINT  uKill  = 0;

	BOOL  fBeta = (CString(C3_BRANCH).Find(L"Gold") == NOTHING) ? TRUE : FALSE;

	CMenu &Help  = Menu.GetSubMenu(uCount - 1);

	if( !C3OemFeature(L"Update", TRUE) || fBeta ) {

		Help.DeleteMenu(IDM_HELP_UPDATE, MF_BYCOMMAND);

		uKill++;
	}

	if( !C3OemFeature(L"Registration", TRUE) || fBeta ) {

		Help.DeleteMenu(IDM_HELP_REGISTER, MF_BYCOMMAND);

		uKill++;
	}

	if( !C3OemFeature(L"Support", TRUE) ) {

		Help.DeleteMenu(IDM_HELP_SUPPORT, MF_BYCOMMAND);

		uKill++;
	}

	if( !C3OemFeature(L"BuildNotes", TRUE) ) {

		Help.DeleteMenu(IDM_HELP_NOTES, MF_BYCOMMAND);

		uKill++;
	}

	if( uKill == 4 ) {

		UINT uPos = Help.GetMenuItemCount() - 2;

		Help.DeleteMenu(uPos, MF_BYPOSITION);
	}

	C3OemAdjustMenu(Menu);

	return TRUE;
}

BOOL CCrimsonWnd::Navigate(CString Nav)
{
	if( !Nav.IsEmpty() ) {

		CSysProxy Proxy;

		Proxy.Bind(m_pView);

		Proxy.Navigate(Nav);

		Proxy.FlipToItemView(TRUE);

		return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::OpenFile(CFilename const &Name, BOOL fRead)
{
	return OpenFile(Name, fRead, FALSE);
}

BOOL CCrimsonWnd::OpenFile(CFilename const &Name, BOOL fRead, BOOL fAuto)
{
	if( !IsFileOpen(Name) && m_pView->CheckSave() ) {

		UpdateWindow();

		CString Force;

		BOOL    fIntegral = FALSE;

		BOOL    fSame     = TRUE;

		if( CheckImportFrom3x(Name, Force, fIntegral, fSame) ) {

			CDatabase *pDbase = New CDatabase;

			afxThread->SetWaitMode(TRUE);

			afxThread->SetStatusText(CString(IDS_OPENING_FILE));

			try {
				if( pDbase->LoadFile(Name, L"GenericSystem", Force, fRead) ) {

					CString Test = C3OemCompany();

					if( pDbase->GetConfig() == Test ) {

						CSystemItem *pSystem = pDbase->GetSystemItem();

						if( pSystem ) {

							BOOL fHidden = pDbase->IsDownloadOnly();

							if( !Force.IsEmpty() ) {

								CString Model = pSystem->GetModelSpec();

								if( CModelInfo::SelectModel(2, Model) ) {

									pSystem->SetModelSpec(Model);

									if( fIntegral ) {

										pDbase->AddFlag(L"ForceIntegral");
									}

									pSystem->UpdateHardware();

									m_fEatUpdate = TRUE;
								}
								else {
									HideSplash();

									afxThread->SetStatusText(L"");

									afxThread->SetWaitMode(FALSE);

									delete pDbase;

									return FALSE;
								}

								pSystem->Rebuild(2);
							}

							if( pDbase->HasFlag(L"Recomp") ) {

								pSystem->Rebuild(3);

								pDbase->RemFlag(L"Recomp");
							}

							if( fHidden ) {

								m_pView = New CHiddenView(this, CString(IDS_DOWNLOAD_MODE));
							}
							else {
								m_pView = New CCrimsonView(this);
							}

							m_pView->Attach(pDbase);

							if( !fAuto ) {

								if( !Is30Filename(Name) && !Is31Filename(Name) ) {

									AddRecent(Name);
								}

								SaveModel();

								m_pView->SetCaption();
							}

							SetView(m_pView);

							DoUpdateUI();

							PromptNew();

							afxThread->SetStatusText(L"");

							afxThread->SetWaitMode(FALSE);

							return TRUE;
						}
						else {
							HideSplash();

							afxThread->SetStatusText(L"");

							afxThread->SetWaitMode(FALSE);

							CString Text;

							Text += CString(IDS_DATABASE_WAS);

							Text += CString(IDS_CHECK_FOR_UPDATED);

							Error(Text);

							delete pDbase;

							return FALSE;
						}
					}
				}
			}

			catch( int * )
			{
				OutputDebugString(L"ZDI Catch\n");

				ExitProcess(0);
			}

			m_Recent.RemoveFile(Name);

			HideSplash();

			afxThread->SetStatusText(L"");

			afxThread->SetWaitMode(FALSE);

			Error(IDS_LOAD_ERROR);

			delete pDbase;
		}
	}

	return FALSE;
}

BOOL CCrimsonWnd::IsFileOpen(CFilename const &Name)
{
	if( m_pView->GetFilename() == Name ) {

		if( m_pView->IsDirty() ) {

			CPrintf Text(IDS_FILE_IS_OPEN, Name.GetBareName());

			if( NoYes(Text) == IDYES ) {

				m_pView->ClearDirty();

				return FALSE;
			}
		}
		else
			return FALSE;

		return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::CheckImportFrom3x(CFilename const &Name, CString &Force, BOOL &fIntegral, BOOL &fSame)
{
	CString Model;

	if( Is30File(Name, Model) || Is31File(Name, Model) ) {

		if( Model.StartsWith(L"ET") ) {

			Error(IDS("Import of EtherTrak databases is not supported."));

			return FALSE;
		}

		Force = L"Generic|" + Model;

		fSame = FALSE;

		return TRUE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::Is30File(CFilename const &Name, CString &Model)
{
	if( Is30Filename(Name) ) {

		return Is3xFile(Name, Model, 0, 3100);
	}

	return FALSE;
}

BOOL CCrimsonWnd::Is31File(CFilename const &Name, CString &Model)
{
	if( Is31Filename(Name) ) {

		return Is3xFile(Name, Model, 3100, 3200);
	}

	return FALSE;
}

BOOL CCrimsonWnd::Is3xFile(CFilename const &Name, CString &Model, UINT uMin, UINT uMax)
{
	CTextStreamMemory Stream;

	if( Stream.LoadFromFile(Name) ) {

		CTreeFile Tree;

		if( Tree.OpenLoad(Stream) ) {

			CString Code = Tree.GetName();

			if( Code == L"C3Data" ) {

				Tree.GetObject();

				while( !Tree.IsEndOfData() ) {

					CString const &Name = Tree.GetName();

					if( Name == L"Version" ) {

						UINT uVersion = Tree.GetValueAsInteger();

						if( uVersion < uMin || uVersion >= uMax ) {

							Tree.Abort();

							return FALSE;
						}

						continue;
					}

					if( Name == L"System" ) {

						Tree.GetObject();

						Tree.GetName();

						Model = Tree.GetValueAsString();

						Model = Model.Left(Model.GetLength() - 4);

						Tree.Abort();

						return TRUE;
					}
				}

				Tree.EndObject();
			}
		}
	}

	return FALSE;
}

BOOL CCrimsonWnd::Is30Filename(CFilename const &Name)
{
	return Name.GetType() == CString(IDS_CD3).Mid(1);
}

BOOL CCrimsonWnd::Is31Filename(CFilename const &Name)
{
	return Name.GetType() == CString(IDS_CD31).Mid(1);
}

void CCrimsonWnd::FindWindowPos(void)
{
	CRect       Work;

	ICONMETRICS Icon;

	Icon.cbSize = sizeof(Icon);

	SystemParametersInfo(SPI_GETWORKAREA, 0, &Work, 0);

	SystemParametersInfo(SPI_GETICONMETRICS, 0, &Icon, 0);

	Work.left += Icon.iHorzSpacing;

	Work      -= 20;

	CSize   Need = CSize(1100, 800);

	CSize   Size = Work.GetSize();

	CRegKey Key  = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"Window");

	m_ShowZoom        = Key.GetValue(L"Zoom", DWORD(Size.cx < Need.cx || Size.cy < Need.cy));

	m_ShowRect.left   = Key.GetValue(L"Left", DWORD(Work.left));

	m_ShowRect.top    = Key.GetValue(L"Top", DWORD(Work.top));

	m_ShowRect.right  = Key.GetValue(L"Right", DWORD(Work.right));

	m_ShowRect.bottom = Key.GetValue(L"Bottom", DWORD(Work.bottom));

	m_ShowRect -= 32 * (m_uInst - 1);

	MoveWindow(m_ShowRect, FALSE);
}

BOOL CCrimsonWnd::SaveWindowPos(void)
{
	if( m_uInst == 1 ) {

		CRegKey Key = afxModule->GetApp()->GetUserRegKey();

		Key.MoveTo(L"Window");

		Key.SetValue(L"Zoom", DWORD(m_SaveZoom));

		if( !m_SaveRect.IsEmpty() ) {

			if( m_SaveRect.cx() < GetSystemMetrics(SM_CXSCREEN) ) {

				if( m_SaveRect.cy() < GetSystemMetrics(SM_CYSCREEN) ) {

					Key.SetValue(L"Left", DWORD(m_SaveRect.left));

					Key.SetValue(L"Top", DWORD(m_SaveRect.top));

					Key.SetValue(L"Right", DWORD(m_SaveRect.right));

					Key.SetValue(L"Bottom", DWORD(m_SaveRect.bottom));
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::ParseCommandLine(void)
{
	CString Error;

	UINT	uCount = afxModule->GetArgumentCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CString Arg = afxModule->GetArgument(n);

		if( Arg[0] == '-' || Arg[0] == '/' ) {

			if( Arg.Mid(1) == L"lang" ) {

				n++;

				continue;
			}

			if( Arg.Mid(1) == L"beta" ) {

				continue;
			}

			if( Arg.Mid(1) == L"send" ) {

				m_uCommand = cmdSend;

				continue;
			}

			if( Arg.Mid(1) == L"update" ) {

				m_uCommand = cmdUpdate;

				continue;
			}

			if( Arg.Mid(1) == L"user" ) {

				if( ++n < uCount ) {

					m_UserName = afxModule->GetArgument(n);

					continue;
				}
			}

			if( Arg.Mid(1) == L"pass" ) {

				if( ++n < uCount ) {

					m_Password = afxModule->GetArgument(n);

					continue;
				}
			}

			if( Arg.Mid(1, 3) == L"com" || Arg.Mid(1, 3) == L"usb" || Arg.Mid(1, 3) == L"tcp" ) {

				m_Method = Arg.Mid(1);

				if( m_Method == L"tcp" ) {

					if( ++n < uCount ) {

						CString Arg = afxModule->GetArgument(n);

						m_Method += L" ";

						m_Method += Arg;
					}
				}

				continue;
			}

			Error = IDS_CMD_BAD_SWITCH;

			break;
		}

		if( m_LoadFile.GetLength() ) {

			Error = IDS_CMD_EX_FILE;

			break;
		}

		m_LoadFile = Arg;
	}

	if( Error.IsEmpty() ) {

		if( m_uCommand && m_LoadFile.IsEmpty() ) {

			Error = IDS_CMD_NO_FILE;
		}
	}

	if( Error.GetLength() ) {

		HideSplash();

		CWnd::Error(Error);

		return FALSE;
	}

	return TRUE;
}

void CCrimsonWnd::HideSplash(void)
{
	((CCrimsonApp *) afxThread)->HideSplash();
}

BOOL CCrimsonWnd::PromptNew(void)
{
	if( C3OemModels().Count(',') ) {

		CString Name(m_pView->GetDisplayName());

		CPrintf Text(IDS_FILE_TARGET_SELECT, Name);

		afxThread->SetStatusText(Text);

		return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonWnd::InitToDefault(CDatabase * &pDbase)
{
	// LATER -- Give OEM chance to set default database.

	CString Default = L"";

	if( !Default.IsEmpty() ) {

		CFilename File = afxModule->GetFilename();

		File.ChangeName(Default);

		pDbase = New CDatabase;

		if( pDbase->LoadFile(File, FALSE) ) {

			pDbase->ClearFilename();

			return TRUE;
		}

		delete pDbase;
	}

	pDbase = New CDatabase(L"GenericSystem");

	pDbase->GetSystemItem()->SetModel(m_Model);

	pDbase->Init();

	return FALSE;
}

BOOL CCrimsonWnd::HasCalibrate(void)
{
	return FALSE;
}

UINT CCrimsonWnd::GetCrimsonCount(void)
{
	PCTXT pFind  = L"c3shell_wnd";

	UINT  uFind  = wstrlen(pFind);

	UINT  uCount = 0;

	HWND  hWnd   = ::GetNextWindow(GetDesktopWindow(), GW_CHILD);

	while( hWnd ) {

		WCHAR sClass[256];

		RealGetWindowClass(hWnd, sClass, sizeof(sClass));

		if( !wstrncmp(sClass, pFind, uFind) ) {

			uCount++;
		}

		hWnd = ::GetNextWindow(hWnd, GW_HWNDNEXT);
	}

	return uCount;
}

BOOL CCrimsonWnd::SendCommand(BOOL fUpdate)
{
	WCHAR sPath[MAX_PATH];

	GetTempPath(MAX_PATH, sPath);

	wstrcat(sPath, L"c3send.txt");

	DeleteFile(sPath);

	if( OpenFile(m_LoadFile, TRUE) ) {

		HideSplash();

		if( fUpdate ? LinkUpdate(GetDatabase()) : LinkSend(GetDatabase()) ) {

			FILE *pFile = _wfopen(sPath, L"w");

			fprintf(pFile, "0 OK\r\n");

			fclose(pFile);

			return TRUE;
		}

		FILE *pFile = _wfopen(sPath, L"w");

		fprintf(pFile, "1 FAIL SEND\r\n");

		fclose(pFile);

		return FALSE;
	}

	FILE *pFile = _wfopen(sPath, L"w");

	fprintf(pFile, "2 FAIL OPEN\r\n");

	fclose(pFile);

	return FALSE;
}

BOOL CCrimsonWnd::CheckRecover(void)
{
	WIN32_FIND_DATA Data;

	CModule * pApp  = afxModule->GetApp();

	CFilename Path  = pApp->GetFolder(CSIDL_LOCAL_APPDATA, L"AutoSave");

	HANDLE    hFind = FindFirstFile(Path + L"*.tmp", &Data);

	BOOL	  fOkay = FALSE;

	if( hFind != INVALID_HANDLE_VALUE ) {

		do {
			CFilename Name = Path;

			Name += Data.cFileName;

			if( !Name.WithType(L"mrk").Exists() ) {

				CString Text;

				Text += CString(IDS_AUTOSAVE_FILE);

				Text += CString(IDS_NN);

				Text += CString(IDS_DO_YOU_WANT_TO);

				HideSplash();

				if( YesNo(Text) == IDYES ) {

					if( OpenFile(Name, FALSE, TRUE) ) {

						CDatabase *pDbase = GetDatabase();

						if( pDbase->RecoverFilename() ) {

							m_pView->SetCaption();
						}
						else
							m_pView->SetNewFile();

						Text = CString(IDS_FILE_WAS_2);

						Information(Text);

						fOkay = TRUE;
					}
				}

				DeleteFile(Name);

				FindClose(hFind);

				break;
			}

		} while( FindNextFile(hFind, &Data) );
	}

	return fOkay;
}

BOOL CCrimsonWnd::WarnConvert(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"Convert");

	BOOL fConvert = Reg.GetValue(L"Warning", UINT(1));

	return !fConvert || CConvertDialog().Execute(ThisObject);
}

BOOL CCrimsonWnd::HasContents(void)
{
	CDatabase   *pDbase  = GetDatabase();

	if( pDbase->GetSystemItem()->GetDownloadConfig() == L"EtherTrak3" ) {

		return TRUE;
	}

	return TRUE;
}

BOOL CCrimsonWnd::HasReference(void)
{
	CDatabase   *pDbase  = GetDatabase();

	if( pDbase->GetSystemItem()->GetDownloadConfig() == L"EtherTrak3" ) {

		return FALSE;
	}

	return C3OemFeature(L"FuncHelp", TRUE);
}

// Link Hooks

BOOL CCrimsonWnd::LinkSend(CDatabase *pDbase)
{
	#if 0

	if( IsEtherTrak3() ) {

		return Et3Link_Send(pDbase);
	}

	#endif

	return Link_Send(pDbase, m_Method, m_UserName, m_Password);
}

BOOL CCrimsonWnd::LinkUpdate(CDatabase *pDbase)
{
	#if 0

	if( IsEtherTrak3() ) {

		return Et3Link_Update(pDbase);
	}

	#endif

	return Link_Update(pDbase, m_Method, m_UserName, m_Password);
}

BOOL CCrimsonWnd::LinkUpload(CFilename &File)
{
	#if 0

	if( IsEtherTrak3() ) {

		return Et3Link_Upload(pDbase, File);
	}

	#endif

	return Link_Upload(File);
}

// End of File
