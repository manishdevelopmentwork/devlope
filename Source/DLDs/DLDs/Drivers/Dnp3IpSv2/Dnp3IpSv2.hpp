#ifndef	INCLUDE_Dnp3IpSv2_HPP
	
#define	INCLUDE_Dnp3IpSv2_HPP

#include "Dnp3IpS.hpp"

#include "Dnp3S.hpp"

#include "Dnp3Base.hpp"

/////////////////////////////////////////////////////////////////////////
//
//  DNP3 TCP Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

class CDnp3IpSlaveV2 : public CDnp3IpSlave
{
	public:
		// Constructor
		CDnp3IpSlaveV2(void);
	};

#endif

// End of File

