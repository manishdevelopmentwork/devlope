
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Uart437_HPP

#define	INCLUDE_AM437_Uart437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;
class CPru437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 UART
//

class CUart437 : public IPortObject, public IEventSink
{
public:
	// Constructor
	CUart437(UINT iIndex, CClock437 *pClock, CPru437 *pPru);

	// Destructor
	~CUart437(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void  METHOD Bind(IPortHandler *pHandler);
	void  METHOD Bind(IPortSwitch  *pSwitch);
	UINT  METHOD GetPhysicalMask(void);
	BOOL  METHOD Open(CSerialConfig const &Config);
	void  METHOD Close(void);
	void  METHOD Send(BYTE bData);
	void  METHOD SetBreak(BOOL fBreak);
	void  METHOD EnableInterrupts(BOOL fEnable);
	void  METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL  METHOD GetInput(UINT uInput);
	DWORD METHOD GetHandle(void);

	// IEventSink
	void OnEvent(UINT uLine, UINT uParam);

protected:
	// Registers
	enum
	{
		regTHR		= 0x0000 / sizeof(WORD),
		regRHR		= 0x0000 / sizeof(WORD),
		regDLL		= 0x0000 / sizeof(WORD),
		regIER		= 0x0004 / sizeof(WORD),
		regDLH		= 0x0004 / sizeof(WORD),
		regEFR		= 0x0008 / sizeof(WORD),
		regFCR		= 0x0008 / sizeof(WORD),
		regIIR		= 0x0008 / sizeof(WORD),
		regLCR		= 0x000C / sizeof(WORD),
		regMCR		= 0x0010 / sizeof(WORD),
		regXON1		= 0x0010 / sizeof(WORD),
		regXON2		= 0x0014 / sizeof(WORD),
		regLSR		= 0x0014 / sizeof(WORD),
		regTCR		= 0x0018 / sizeof(WORD),
		regMSR		= 0x0018 / sizeof(WORD),
		regXOFF1	= 0x0018 / sizeof(WORD),
		regSPR		= 0x001C / sizeof(WORD),
		regTLR		= 0x001C / sizeof(WORD),
		regXOFF2	= 0x001C / sizeof(WORD),
		regMDR1		= 0x0020 / sizeof(WORD),
		regMDR2		= 0x0024 / sizeof(WORD),
		regTXFLL	= 0x0028 / sizeof(WORD),
		regSFLSR	= 0x0028 / sizeof(WORD),
		regRESUME	= 0x002C / sizeof(WORD),
		regTXFLH	= 0x002C / sizeof(WORD),
		regRXFLL	= 0x0030 / sizeof(WORD),
		regSFREGL	= 0x0030 / sizeof(WORD),
		regSFREGH	= 0x0034 / sizeof(WORD),
		regRXFLH	= 0x0034 / sizeof(WORD),
		regBLR		= 0x0038 / sizeof(WORD),
		regUASR		= 0x0038 / sizeof(WORD),
		regACREG	= 0x003C / sizeof(WORD),
		regSCR		= 0x0040 / sizeof(WORD),
		regSSR		= 0x0044 / sizeof(WORD),
		regEBLR		= 0x0048 / sizeof(WORD),
		regMVR		= 0x0050 / sizeof(WORD),
		regSYSC		= 0x0054 / sizeof(WORD),
		regSYSS		= 0x0058 / sizeof(WORD),
		regWER		= 0x005C / sizeof(WORD),
		regCFPS		= 0x0060 / sizeof(WORD),
		regRXFIFO	= 0x0064 / sizeof(WORD),
		regTXFIFO	= 0x0068 / sizeof(WORD),
		regIER2		= 0x006C / sizeof(WORD),
		regISR2		= 0x0070 / sizeof(WORD),
		regFREQ		= 0x0074 / sizeof(WORD),
		regMDR3		= 0x0080 / sizeof(WORD),
		regTXDMA	= 0x0084 / sizeof(WORD),
	};

	// States
	enum
	{
		stateClosed	= 0,
		stateOpen	= 1,
	};

	// Interrupt Enables
	enum
	{
		intCts		= Bit(7),
		intRts		= Bit(6),
		intXOff		= Bit(5),
		intSleep	= Bit(4),
		intModem	= Bit(3),
		intLine		= Bit(2),
		intTx		= Bit(1),
		intRx		= Bit(0),
	};

	// Interrupt Types
	enum
	{
		typeModem	= 0x00,
		typeTxFifo	= 0x01,
		typeRxFifo	= 0x02,
		typeLineErr	= 0x03,
		typeRxTimeout	= 0x06,
		typeXOff	= 0x08,
		typeLine	= 0x10,
	};

	// Status  
	enum
	{
		statFifoError	= Bit(7),
		statTxDone	= Bit(6),
		statTxFifo	= Bit(5),
		statRxBreak	= Bit(4),
		statRxFrame	= Bit(3),
		statRxParity	= Bit(2),
		statRxOverrun	= Bit(1),
		statRxFifo	= Bit(0),
	};

	// Control Bits
	enum
	{
		bitAutoCts	= Bit(7),
		bitRxLevel1Bit	= Bit(7),
		bitAutoRts	= Bit(6),
		bitBreakEn	= Bit(6),
		bitCts		= Bit(4),
		bitTxEmptyCtrl	= Bit(3),
		bitTxFifoClear	= Bit(2),
		bitRxFifoClear	= Bit(1),
		bitAssertRts	= Bit(1),
	};

	// Data Members
	PVWORD          m_pBase;
	ULONG           m_uRefs;
	UINT		m_uLine;
	UINT		m_uState;
	UINT		m_uClock;
	IPortHandler  * m_pHandler;
	CSerialConfig	m_Config;
	IPortSwitch   * m_pSwitch;
	ITimer        * m_pTimer;
	UINT            m_uUnit;
	UINT            m_uSave;
	BYTE		m_bMask;
	CPru437       * m_pPru;
	UINT            m_uPru;

	// Implementation
	void Reset(void);
	bool InitUart(void);
	bool InitBaudRate(void);
	bool InitFlags(void);
	bool InitFormat(void);
	WORD GetParityInit(void);
	WORD GetStopBitsInit(void);
	WORD GetDataBitsInit(void);
	BYTE GetDataBitsMask(void);
	WORD GetPhysical(void);
	void EnablePort(void);
	void DisablePort(void);
	void EnableEvents(void);
	void DisableEvents(void);
	bool CanWait(void);
	bool WaitDone(void);
	void EnablePru(void);
	void DisablePru(void);
	UINT FindBase(UINT iIndex) const;
	UINT FindLine(UINT iIndex) const;
};

// End of File

#endif
