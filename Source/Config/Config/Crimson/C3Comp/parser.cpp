
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Basic Parser
//

// Constructor

CBasicParser::CBasicParser(void)
{
	}
		
// Token Position

CRange CBasicParser::GetTokenPos(void) const
{
	return m_Token.GetPos();
	}

UINT CBasicParser::GetTokenStart(void) const
{
	return m_Token.GetStart();
	}

UINT CBasicParser::GetTokenEnd(void) const
{
	return m_Token.GetEnd();
	}

UINT CBasicParser::GetTokenLine(void) const
{
	return m_Token.GetLine();
	}
		
// Overridables

BOOL CBasicParser::FilterToken(void)
{
	return !m_Token.IsGroup(groupWhiteSpace);
	}
	
// Implementation

void CBasicParser::GetToken(void)
{
	do {
		m_Before = m_Token;

		if( !m_Lex.GetToken(*m_pError, m_Token) ) {
		
			ThrowError();
			}

		if( m_Token.IsGroup(groupEndOfText) ) {

			return;
			}
			
		} while( !FilterToken() );
	}

void CBasicParser::ThrowError(CLexToken const &Token)
{
	m_pError->SetRange(Token.GetPos());

	AfxThrowUserException();
	}

void CBasicParser::ThrowError(void)
{
	m_pError->SetRange(m_Token.GetPos());

	AfxThrowUserException();
	}

void CBasicParser::Expected(ENTITY ID)
{
	Expected(CString(ID));
	}

void CBasicParser::Expected(PCTXT pText)
{
	if( !pText ) {

		m_pError->Set(CFormat(IDS_COMP_EXPECT_1, m_Token.Describe()));

		ThrowError();
		}

	m_pError->Set(CFormat(IDS_COMP_EXPECT_2, m_Token.Describe(), pText));

	ThrowError();
	}
	
void CBasicParser::MatchCode(UINT Code)
{
	if( m_Token.m_Code != Code ) {

		CString Token = CLexToken(Code).Describe();
				
		Expected(Token);
		}
	
	GetToken();
	}

// End of File
