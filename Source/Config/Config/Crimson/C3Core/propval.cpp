
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Property Value
//

// Static Data

UINT CPropValue::m_uDepth = 0;

// Constructor

CPropValue::CPropValue(void)
{
	m_fUsed = FALSE;
	}

// Destructor

CPropValue::~CPropValue(void)
{
	INDEX n = m_Props.GetHead();

	while( !m_Props.Failed(n) ) {

		delete m_Props.GetData(n);

		m_Props.GetNext(n);
		}

	m_Props.Empty();
	}

// Child Access

CPropValue * CPropValue::GetChild(CString Name) const
{
	if( this ) {

		CString Prop = Name.StripToken(L'/');

		INDEX   Find = m_Props.FindName(Prop);

		if( !m_Props.Failed(Find) ) {

			if( Name.IsEmpty() ) {

				return m_Props.GetData(Find);
				}

			return m_Props.GetData(Find)->GetChild(Name);
			}

		return NULL;
		}

	return NULL;
	}

// Attributes

BOOL CPropValue::IsList(void) const
{
	return m_fList;
	}

BOOL CPropValue::IsComplex(void) const
{
	return !m_Props.IsEmpty();
	}

BOOL CPropValue::IsString(void) const
{
	return !IsComplex() && m_Value[0] == '\"';
	}

BOOL CPropValue::IsNumber(void) const
{
	return !IsComplex() && !IsBlob() && !IsString();
	}

BOOL CPropValue::IsBlob(void) const
{
	return !IsComplex() && m_Value[0] == '0' && m_Value[1] == 'x';
	}

CString CPropValue::GetString(void) const
{
	if( m_Value.Find(0xA7) < NOTHING ) {

		CStringArray List;

		CString      Text;

		m_Value.Tokenize(List, 0xA7);

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			if( List[n][0] == '\"' ) {

				Text += List[n].Mid(1, List[n].GetLength() - 2);

				continue;
				}

			Text += List[n];
			}

		return Text;
		}

	if( m_Value[0] == '\"' ) {

		return m_Value.Mid(1, m_Value.GetLength() - 2);
		}

	return m_Value;
	}

UINT CPropValue::GetNumber(void) const
{
	return watoi(m_Value);
	}

UINT CPropValue::GetBlob(CByteArray &Blob) const
{
	PCTXT t = m_Value;

	while( *t ) {

		if( *t++ == 0xA7 ) {

			for( UINT p = 2; t[p]; p += 2 ) {

				if( t[p] == 0xA7 ) {

					p += 1;

					continue;
					}

				BYTE uHi = FromHex(t[p+0]);

				BYTE uLo = FromHex(t[p+1]);

				Blob.Append(BYTE(uLo+16*uHi));
				}

			return Blob.GetCount();
			}
		}

	return 0;
	}

// Operations

BOOL CPropValue::Rename(CString From, CString Name)
{
	INDEX n = m_Props.FindName(From);

	if( !m_Props.Failed(n) ) {

		CPropValue *pValue = m_Props.GetData(n);

		m_Props.Remove(n);

		if( m_Props.Insert(Name, pValue) ) {

			return TRUE;
			}

		delete pValue;
		}

	return FALSE;
	}

void CPropValue::Print(CString Name, UINT uDepth)
{
	if( TRUE ) {

		AfxTrace(L"%*s%s", m_uDepth, L"", Name);

		if( !m_Value.IsEmpty() ) {

			if( m_Value.GetLength() > 256 ) {

				AfxTrace(L" = BIG");
				}
			else
				AfxTrace(L" = %s", m_Value);
			}

		AfxTrace(L"\n");
		}

	if( uDepth ) {

		m_uDepth += 2;

		INDEX n = m_Props.GetHead();

		while( !m_Props.Failed(n) ) {

			m_Props.GetData(n)->Print(m_Props.GetName(n), uDepth-1);

			m_Props.GetNext(n);
			}

		m_uDepth -= 2;
		}
	}

// Implementation

BYTE CPropValue::FromHex(TCHAR cData) const
{
	if( cData >= '0' && cData <= '9' ) {
		
		return BYTE(cData - '0' + 0x00);
		}

	if( cData >= 'A' && cData <= 'F' ) {
		
		return BYTE(cData - 'A' + 0x0A);
		}
	
	if( cData >= 'a' && cData <= 'f' ) {
		
		return BYTE(cData - 'a' + 0x0A);
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Property Loader
//

// Static Data

WCHAR CPropLoader::m_sLine[8192];

// Constructor

CPropLoader::CPropLoader(void)
{
	m_Root.m_pLink = NULL;

	m_Root.m_fList = FALSE;
	}

// Destructor

CPropLoader::~CPropLoader(void)
{
	}

// Operations

BOOL CPropLoader::LoadFromFile(CFilename const &Name)
{
	CTextStreamMemory Stm;

	Stm.SetPrev();

	if( Stm.LoadFromFile(Name) ) {

		CPropValue *pRoot = &m_Root;

		CString     Path  = L"";

		UINT        Time  = 0;

		while( ReadLine(&Stm) ) {

			if( GetTickCount() > Time + 100 ) {

				UINT pc = UINT((INT64(100) * Stm.GetRead()) / Stm.GetSize());

				CPrintf Text( CString(IDS_PARSING_CRIMSON),
					      pc
					      );

				afxThread->SetStatusText(Text);

				Time = GetTickCount();
				}

			if( IsValue() ) {

				if( m_Name ==  L"." ) {

					if( !m_Last.IsEmpty() ) {

						INDEX n = pRoot->m_Props.FindName(m_Last);

						if( !pRoot->m_Props.Failed(n) ) {

							CPropValue *pValue = pRoot->m_Props.GetData(n);

							pValue->m_Value += 0xA7;

							pValue->m_Value += m_Data;

							continue;
							}
						}

					return FALSE;
					}
				else {
					if( ForceSkip(m_Name) ) {

						continue;
						}

					if( ForceEnum(Path, m_Name) ) {

						// REV3 -- Do we need this any more now we
						// try to handle it automatically below?

						// NOTE -- Module import replies on the
						// sequential enumeration of properties.

						UINT uSlot = pRoot->m_Props.GetCount();

						m_Name += CPrintf("%4.4u", uSlot);
						}
					else {
						INDEX Index = pRoot->m_Props.FindName(m_Name);

						if( !pRoot->m_Props.Failed(Index) ) {

							CString     First = m_Name + L"0000";

							CPropValue *pKeep = pRoot->m_Props.GetData(Index);

							UINT        uSlot = pRoot->m_Props.GetCount();

							pRoot->m_Props.Insert(First, pKeep);

							pRoot->m_Props.Remove(Index);

							m_Name += CPrintf("%4.4u", uSlot);
							}
						}

					CPropValue *pValue = New CPropValue;

					pValue->m_pLink    = pRoot;

					pValue->m_Value    = m_Data;

					pValue->m_fList    = FALSE;

					if( pRoot->m_Props.Insert(m_Name, pValue) ) {

						m_Last = m_Name;

						continue;
						}

					return FALSE;
					}
				}

			if( IsOpen() ) {

				if( ForceList(Path, m_Name) ) {

					m_Sep = L"[";
					}

				if( ForceItem(Path, m_Name) ) {

					m_Sep = L"{";
					}

				CPropValue *pValue = New CPropValue;

				pValue->m_pLink    = pRoot;

				pValue->m_fList    = IsList();

				if( pRoot->m_fList ) {

					CPropValue *pClass = New CPropValue;

					pValue->m_Value    = m_Name;

					UINT uCount        = pRoot->m_Props.GetCount();

					pClass->m_pLink    = pValue;

					pClass->m_Value    = m_Name;

					pValue->m_Props.Insert(L"*", pClass);

					m_Name.Printf(L"C%4.4u", uCount);
					}

				if( pRoot->m_Props.Insert(m_Name, pValue) ) {

					pRoot = pValue;

					Path += L'/';

					Path += m_Name;

					m_Last.Empty();

					continue;
					}

				delete pValue;

				return FALSE;
				}

			if( IsClose() ) {

				if( (pRoot = pRoot->m_pLink) ) {

					Path = Path.Left(Path.FindRev('/'));

					continue;
					}

				return FALSE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CPropLoader::ReadLine(ITextStream *pStm)
{
	if( pStm->GetLine(m_sLine, elements(m_sLine)) ) {

		UINT  uState  = 0;

		int   nPos[6] = { 0, 0, 0, 0, 0, 0 };

		TCHAR cData   = 0;

		int n;

		for( n = 0; cData = m_sLine[n]; n++ ) {

			switch( uState ) {

				case 0:
				case 2:
				case 4:
					if( !isspace(cData) ) {

						nPos[uState++] = n;
						}
					break;

				case 1:
				case 3:
					if( isspace(cData) ) {

						nPos[uState++] = n;
						}
					break;
				}
			}

		if( nPos[2] ) {

			if( nPos[4] ) {

				while( isspace(m_sLine[--n]) );

				nPos[5] = n + 1;
				}

			m_Name.QuickInit(m_sLine + nPos[0], nPos[1] - nPos[0]);
			
			m_Sep .QuickInit(m_sLine + nPos[2], nPos[3] - nPos[2]);

			m_Data.QuickInit(m_sLine + nPos[4], nPos[5] - nPos[4]);
			}
		else {
			m_Name.QuickInit(L"", 0);

			m_Sep .QuickInit(m_sLine + nPos[0], nPos[1] - nPos[0]);

			m_Data.QuickInit(L"", 0);
			}

		return TRUE;
		}

	m_Name.QuickInit(L"", 0);

	m_Sep .QuickInit(L"", 0);
	
	m_Data.QuickInit(L"", 0);

	return FALSE;
	}

BOOL CPropLoader::IsValue(void)
{
	return m_Sep == L"=";
	}

BOOL CPropLoader::IsOpen(void)
{
	return m_Sep == L"{" || m_Sep == L"[";
	}

BOOL CPropLoader::IsClose(void)
{
	return m_Sep == L"}" || m_Sep == L"]";
	}

BOOL CPropLoader::IsList(void)
{
	return m_Sep == L"[";
	}

BOOL CPropLoader::ForceList(CString const &Path, CString const &Name)
{
	if( Name == L"Tags" ) {

		if( Path == L"/TreeFile/C2Data/System/Comms/Tags" ) {

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CPropLoader::ForceItem(CString const &Path, CString const &Name)
{
	if( Name == L"Progs" ) {

		if( Path == L"/TreeFile/C2Data/System" ) {

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CPropLoader::ForceEnum(CString const &Path, CString const &Name)
{
	if( Name == L"Tag" ) {

		if( Path.StartsWith(L"/TreeFile/C2Data/System/Web/Pages") ) {

			return TRUE;
			}

		if( Path.StartsWith(L"/TreeFile/C2Data/System/Logger/Logs") ) {

			return TRUE;
			}

		if( Path.StartsWith(L"/TreeFile/C2Data/System/UI/Pages") ) {

			if( Path.EndsWith(L"/Key") ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	if( Name == L"Color" ) {

		if( Path.StartsWith(L"/TreeFile/C2Data/System/UI/Pages") ) {

			if( Path.EndsWith(L"/Key") ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	if( Name == L"Prop" ) {

		if( Path.StartsWith(L"/TreeFile/C2Data/System/Rack/ModCol") ) {
			
			return TRUE;
			}
		}

	if( Name == L"WireEnd" ) {

		if( Path.StartsWith(L"/TreeFile/C2Data/System/Rack/ModCol") ) {
			
			return TRUE;
			}
		}

	if( Name == L"PinWire" ) {

		if( Path.StartsWith(L"/TreeFile/C2Data/System/Rack/ModCol") ) {
			
			return TRUE;
			}
		}

	if( Name == L"Name" || Name == L"Data" ) {

		if( Path.StartsWith(L"/TreeFile/C2Data/System/Comms/Devices") ) {

			if( Path.EndsWith(L"/Tags") ) {
			
				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CPropLoader::ForceSkip(CString const &Name)
{
	if( Name == L"Ref" ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
