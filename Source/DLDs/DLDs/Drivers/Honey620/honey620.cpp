
#include "intern.hpp"

#include "honey620.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Driver
//

// Instantiator

INSTANTIATE(CHoneywellIPC620Driver);

// Constructor

CHoneywellIPC620Driver::CHoneywellIPC620Driver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CHoneywellIPC620Driver::~CHoneywellIPC620Driver(void)
{
	}

// Configuration

void MCALL CHoneywellIPC620Driver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_wLTConnection = GetWord(pData);
		}
	}
	
void MCALL CHoneywellIPC620Driver::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CHoneywellIPC620Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CHoneywellIPC620Driver::Open(void)
{
	}

// Device

CCODE MCALL CHoneywellIPC620Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CHoneywellIPC620Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CHoneywellIPC620Driver::Ping(void)
{
	m_uTime = ToTicks(COMMDELAY); // Limit update rate

	if( m_wLTConnection ) {

		return SendStatusRequest();
		}

	m_uNext = GetTickCount(); // execute ping immediately

	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACED;
	Addr.a.m_Offset = 4096;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return DoWordRead(Addr, Data, 1);
	}

CCODE MCALL CHoneywellIPC620Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	while( !TimeToExecuteComms() );

	switch( Addr.a.m_Table ) {

		case SPACED:
		case SPACEI:

			return DoWordRead(Addr, pData, uCount);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CHoneywellIPC620Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	while( !TimeToExecuteComms() );

	switch( Addr.a.m_Table ) {

		case SPACED:
		case SPACEI:

			return DoWordWrite(Addr, pData, uCount);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Port Access

UINT CHoneywellIPC620Driver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CHoneywellIPC620Driver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	if( m_wLTConnection ) {

		AddByte(STX);

		AddByte(bOpcode);

		AddByte( bOpcode == 0x10 ? 5 : 2);

		return;
		}
	
	AddByte(SOH);

	AddByte(m_pCtx->m_bDrop);

	AddByte(0x01);

	AddByte(ETB);

	AddByte(STX);

	AddByte(bOpcode);
	}

void CHoneywellIPC620Driver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CHoneywellIPC620Driver::AddWord(UINT uData)
{
	AddByte(HIBYTE(uData));

	AddByte(LOBYTE(uData));
	}
		
// Transport Layer

BOOL CHoneywellIPC620Driver::GetReply(void)
{
	BYTE bCheck = 0;
	UINT uState = m_wLTConnection ? LTSTATE0 : 0;
	UINT uTimer = m_wLTConnection ? 1000 : 500;
	UINT uPtr   = 0;
	UINT uLen   = 0;

	UINT uByte;

	SetTimer(uTimer);

//**/	AfxTrace0("\r\n");
	
	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}

//**/		AfxTrace1("<%2.2x>", uByte);

		if( uState < 9 || uState > LTSTATE0 ) {

			bCheck += uByte;
			}

		if( uState >= LTSTATE0 ) {

			SetTimer(20);
			}
		
		switch( uState ) {
		
			case 0:
				if( uByte == SOH ) {
					bCheck = 0;
					uState = 1;
					}
				break;
				
			case 1:
				if( uByte == m_bTx[1] )
					uState = 2;
				else
					return FALSE;
				break;
				
			case 2:
				if( uByte == 0x81 )
					uState = 3;
				else
					return FALSE;
				break;
				
			case 3:
				if( uByte == ETB )
					uState = 4;
				else
					return FALSE;
				break;

			case 4:
				if( uByte == STX )
					uState = 5;
				else
					return FALSE;
				break;
				
			case 5:
				if( uByte == 0x00 )
					uState = 6;
				else
					return FALSE;
				break;
				
			case 6:
				uLen = (uByte << 8);
				uState = 7;
				break;
			
			case 7:
				uLen   = (uLen << 8) + uByte;
				uPtr   = 0;
				uState = uLen ? 8 : 9;
				break;
			
			case 8:
				m_bRx[uPtr++] = uByte;

				if( uPtr == uLen )
					uState = 9;
				break;
			
			case 9:
				bCheck -= uByte;
				uState = 10;
				break;
				
			case 10:
				return uByte == ETX && !bCheck;

			case LTSTATE0:

				if( uByte == STX ) {

					bCheck = 0;

					uState = LTSTATE1;
					}

				break;

			case LTSTATE1:
				uState = uByte == ACK ? LTSTATE2 : LTSTATE0;
				break;

			case LTSTATE2:
				uState = LTSTATE3;
				break;

			case LTSTATE3:
				uLen   = uByte + 1;
				uState = LTSTATE4;
				break;

			case LTSTATE4:

				switch( --uLen ) {

					case 0:
						return !bCheck;

					case 1:
						m_bRx[1] = uByte;
						break;

					case 2:
						m_bRx[0] = uByte;
						break;

					default:
						break;
					}
				break;
			}

		if( uPtr >= sizeof(m_bRx) ) {

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CHoneywellIPC620Driver::Transact(void)
{
	BYTE bCheck = 0;

	for( UINT i = 1; i < m_uPtr; i++ ) {

		bCheck += m_bTx[i];
		}

	if( m_wLTConnection ) {

		m_bTx[m_uPtr++] = 0x100 - bCheck;
		}

	else {
		m_bTx[m_uPtr++] = bCheck;

		m_bTx[m_uPtr++] = ETX;
		}

	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for(UINT d = 0; d < m_uPtr; d++ ) AfxTrace1("[%2.2x]", m_bTx[d] );

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return GetReply();
	}

// Read Handlers

CCODE CHoneywellIPC620Driver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_wLTConnection ) {

		StartFrame( 0xE );

		AddWord( Addr.a.m_Offset );

		uCount = 1;
		}

	else {

		StartFrame( Addr.a.m_Table == SPACED ? 4 : 2 );
		
		AddWord(4);

		MakeMin( uCount, 8 );
	
		AddWord(uCount);

		AddWord(Addr.a.m_Offset);
		}
	
	if( Transact() ) {

		if( m_wLTConnection ) {

			pData[0] = (m_bRx[0] << 8) + m_bRx[1];

			return 1;
			}
		
		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = m_bRx[2*n] + ( m_bRx[1 + 2*n] << 8 );
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CHoneywellIPC620Driver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_wLTConnection ) {

		StartFrame( 0x10);

		AddWord( Addr.a.m_Offset );

		AddByte(0);

		AddWord( LOWORD(pData[0]) );

		uCount = 1;
		}

	else {
		StartFrame( Addr.a.m_Table == SPACED ? 0x10 : 0xE );

		MakeMin( uCount, 8 );

		AddWord(4 + 2 * uCount);

		AddWord(uCount);

		AddWord(Addr.a.m_Offset);

		for( UINT i = 0; i < uCount; i++ ) {

			AddByte( LOBYTE( LOWORD(pData[i]) ) );
			AddByte( HIBYTE( LOWORD(pData[i]) ) );
			}
		}

	if( Transact() ) {
			
		return uCount;
		}

	return CCODE_ERROR;
	}

// Control Update Timing
BOOL  CHoneywellIPC620Driver::TimeToExecuteComms(void)
{
	UINT uTick = GetTickCount();

	if( uTick >= m_uNext || m_uNext > uTick + m_uTime ) {

		m_uNext = uTick + m_uTime;

		return TRUE;
		}

	return FALSE;
	}

CCODE CHoneywellIPC620Driver::SendStatusRequest(void)
{
	m_uPtr = 0;
	AddByte(STX);
	AddByte(4);
	AddByte(3);
	AddByte(8);
	AddByte(0xFF);
	AddByte(4);

	return Transact() ? 1 : CCODE_ERROR;
	}

// End of File
