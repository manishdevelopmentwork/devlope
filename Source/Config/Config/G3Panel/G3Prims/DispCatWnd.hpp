
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispCatWnd_HPP

#define INCLUDE_DispCatWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CScratchPadWnd;
class CPageEditorWnd;

//////////////////////////////////////////////////////////////////////////
//
// Display Page Category Window
//

class DLLAPI CDispCatWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispCatWnd(void);

		// Destructor
		~CDispCatWnd(void);

		// Attributes
		BOOL             IsScratchPadActive(void) const;
		CScratchPadWnd * GetScratchPadWnd(void) const;
		CPageEditorWnd * GetScratchPadEditor(void) const;
		CPageEditorWnd * GetMainEditor(void) const;

		// Operations
		void SetMainEditor(CViewWnd *pMain);
		void PageSelected(BOOL fPage);
		void NavTreeActive(CRect const &Rect);
		void NavToScratch(void);

	protected:
		// Data Members
		CAccelerator     m_Accel;
		CSysProxy        m_System;
		CUIManager     * m_pManager;
		CScratchPadWnd * m_pScratch;
		CViewWnd       * m_pMain;
		BOOL		 m_fActive;
		BOOL             m_fCurrent;
		BOOL	         m_fPageSel;
		BOOL	         m_fScratch;
		BOOL		 m_fEnable;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnSetCurrent(BOOL fCurrent);
		void OnActivateApp(BOOL fActive, DWORD dwThread);
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnGlobalCommand(UINT uID);
		BOOL OnViewGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Implementation
		void ShowOrHideScratchPad(void);
		void ReadConfig(void);
		void SaveConfig(void);
	};

// End of File

#endif
