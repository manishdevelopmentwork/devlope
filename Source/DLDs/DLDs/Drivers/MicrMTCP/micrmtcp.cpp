
#include "intern.hpp"

#include "micrmtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Micromod TCP Driver - Encapsulated Serial Frames
//

// Instantiator

INSTANTIATE(CMicromodTCPDriver);

// Constructor

CMicromodTCPDriver::CMicromodTCPDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	m_pTx		= m_bTx;

	m_pRx		= m_bRx;

	m_uTimeout	= 1000;

	m_dRDBO		= 0;

	m_bNAKResp	= 0;

	m_uKeep		= 0;

	m_bSeq		= 1;

	m_fTCPTypeIsSet	= FALSE;
	}

// Destructor

CMicromodTCPDriver::~CMicromodTCPDriver(void)
{
	if( m_pCtx ) {

		if( m_pCtx->m_wArraySize ) {

			delete m_pCtx->m_pLSPArray;
			delete m_pCtx->m_pOFFArray;
			delete m_pCtx->m_pSZEArray;
			delete m_pCtx->m_pBITArray;
			}
		}
	}

// Configuration

void MCALL CMicromodTCPDriver::Load(LPCBYTE pData)
{

	}
	
// Management

void MCALL CMicromodTCPDriver::Attach(IPortObject *pPort)
{

	}

void MCALL CMicromodTCPDriver::Open(void)
{

	}

// Device

CCODE MCALL CMicromodTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			m_pCtx->m_bDrop		= GetByte(pData);
			m_pCtx->m_bDataBaseType	= GetByte(pData) << 6;
			m_pCtx->m_uUnlock       = GetLong(pData);

			m_pCtx->m_wArraySize	= GetWord(pData);

			m_pCtx->m_uACKSHUTPosition = 0xFFFF;

			ClearPackFG();

			if( (BOOL)m_pCtx->m_wArraySize ) { // entry count + list terminator

				AddArrays(pData);
				}

			m_pCtx->m_uAddMBTCP	= 0;

			m_pCtx->m_wTrans	= 1;
			m_pCtx->m_uWriteErr	= 0;
			m_pCtx->m_dLatestErr	= 0;

			// Generic Command Cache
			memset(m_pCtx->m_bHead, 0,  4);
			memset(m_pCtx->m_dCDAT, 0, GENSIZE * 4);
			memset(m_pCtx->m_dRDAT, 0, GENSIZE * 4);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMicromodTCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CloseSocket(FALSE);

		if( m_pCtx->m_wArraySize ) {

			delete m_pCtx->m_pLSPArray;
			delete m_pCtx->m_pOFFArray;
			delete m_pCtx->m_pSZEArray;
			delete m_pCtx->m_pBITArray;
			}

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	else {
		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMicromodTCPDriver::Ping(void)
{
//**/	AfxTrace3("\r\nPing Drop=%d Err=%d AddMBTCP=%x\r\n", m_pCtx->m_bDrop, m_pCtx->m_dLatestErr, m_pCtx->m_uAddMBTCP);

	if( OpenSocket() ) {

		if( m_pCtx->m_bDrop ) {
		
			DWORD    Data[1];

			CAddress Addr;

			if( !m_pCtx->m_uAddMBTCP ) {

				Addr.a.m_Table	= SP_HOLD;

				Addr.a.m_Offset	= 1;

				Addr.a.m_Extra	= 0;

				Addr.a.m_Type	= addrWordAsWord;

				m_bRx[0] = 0;
				m_bRx[1] = 0;

				if( m_fTCPTypeIsSet ) {

					if( Read(Addr, Data, 1) == 1 ) {

						return 1;
						}

					else {
						m_fTCPTypeIsSet = FALSE;
						}
					}

				for( UINT i = 0; i < 2; i++ ) {

					m_pCtx->m_uAddMBTCP = BOOL(i) ? 6 : 0;

					m_bRx[0] = 0;
					m_bRx[1] = 0;

					if( Read(Addr, Data, 1) == 1 ) {

						m_fTCPTypeIsSet = TRUE;

						return 1;
						}

					else {
						m_fTCPTypeIsSet = (BOOL)m_bRx[0] || (BOOL)m_bRx[1];

						if( m_fTCPTypeIsSet ) {

							break;
							}
						}
					}
				}

			if( IsValid65Access(0) ) {

				Addr.a.m_Table	= SP_65;
				Addr.a.m_Offset	= 0;
				Addr.a.m_Extra	= 0;
				Addr.a.m_Type	= addrLongAsLong;
				}

			if( m_fTCPTypeIsSet ) { // did receive a response using a particular m_uAddMBTCP setting

				return Read(Addr, Data, 1) == 1 ? 1 : CCODE_ERROR;
				}

			m_bRx[0] = 0;
			m_bRx[1] = 0;

			for( UINT i = 0; i < 2; i++ ) {

				m_pCtx->m_uAddMBTCP = (BOOL)i ? 6 : 0;

				if( Read(Addr, Data, 1) == 1 ) {

					m_fTCPTypeIsSet = TRUE;

					return 1;
					}

				m_fTCPTypeIsSet = (BOOL)m_bRx[0] || (BOOL)m_bRx[1];
				}

			return CCODE_ERROR;
			}

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CMicromodTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		return uCount;
		}

	if( !OpenSocket() ) return CCODE_ERROR;

//**/	Sleep(100); // slow debug
//**/	if( Addr.a.m_Table < SP_ERR ) {
//**/		AfxTrace2("\r\nRead T=%d O=%d ", Addr.a.m_Table, Addr.a.m_Offset);
//**/		AfxTrace3("t=%x, C=%d TCP=%x ", Addr.a.m_Type, uCount, m_pCtx->m_uAddMBTCP);
//**/		}

	if( IsFuncCode65Item(Addr.a.m_Table) ) {

		if( IsValid65Access(Addr.a.m_Offset) ) { // has a valid database

			if( NoReadTransmit(Addr, &uCount) ) { // if Address IF_141 - return 0 when read

				*pData = 0;

				return 1;
				}

			m_uRWType = F65READ;

			MakeMin(uCount, 20);

			return Handle65(Addr, pData, uCount, TRUE);
			}
		}

	m_uRWType = MDBREAD;

	SetModbusData(Addr);

	CCODE cc = CCODE_ERROR;

	switch( m_uTable ) {

		case SP_HOLD:

			if( m_uType == addrWordAsWord ) {

				MakeMin(uCount, 16);

				cc = DoWordRead(pData, uCount);
				}

			else {
				MakeMin(uCount, 8);

				cc = DoLongRead(pData, uCount);
				}
			break;

		case SP_ANALOG:

			if( m_uType == addrWordAsWord ) {

				MakeMin(uCount, 16);

				cc = DoWordRead(pData, uCount);
				}

			else {
				MakeMin(uCount, 8);

				cc = DoLongRead(pData, uCount);
				}
			break;

		case SP_HOLD32:

			MakeMin(uCount, 8);
			
			cc = DoLongRead(pData, uCount);
			break;

		case SP_ANALOG32:

			MakeMin(uCount, 8);
			
			cc = DoLongRead(pData, uCount);
			break;

		case SP_OUTPUT:

			MakeMin(uCount, 16);
			
			cc = DoBitRead(pData, uCount);
			break;

		case SP_INPUT:

			MakeMin(uCount, 16);
			
			cc = DoBitRead(pData, uCount);
			break;

		case SP_ERR:

			*pData = m_pCtx->m_dLatestErr;

			return 1;

		case SP_NAK:

			*pData = m_bNAKResp;

			return 1;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	return cc;
	}

CCODE MCALL CMicromodTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\n*** WRITE *** T=%d O=%d E=%x ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Extra);
//**/	AfxTrace2("t=%x, C=%d ", Addr.a.m_Type, uCount);

	if( !OpenSocket() ) return CCODE_ERROR;

	if( IsFuncCode65Item(Addr.a.m_Table) ) {

		m_uRWType = F65WRITE;

		if( IsValid65Access(Addr.a.m_Offset) ) {

			if( !Is65Command(Addr.a.m_Table) ) {

				if( IsAckShutdown(Addr.a.m_Offset) ) {

					if( *pData == 1 ) {

						SendAckShutdown();
						}

					return 1;
					}
				}

			return Handle65(Addr, pData, 1, FALSE);
			}

		return 1;
		}

	m_uRWType = MDBWRITE;

	SetModbusData(Addr);

	switch( m_uTable ) {

		case SP_HOLD:

			if( m_uType == addrWordAsWord ) {

				MakeMin(uCount, 16);

				return DoWordWrite(pData, uCount);
				}

			MakeMin(uCount, 8);

			return DoLongWrite(pData, uCount);

		case SP_HOLD32:

			MakeMin(uCount, 8);
			
			return DoLongWrite(pData, uCount);

		case SP_OUTPUT:

			MakeMin(uCount, 16);
			
			return DoBitWrite (pData, uCount);

		case SP_ERR:

			m_pCtx->m_dLatestErr = 0;

			return 1;

		case SP_NAK:

			m_bNAKResp = 0;

			return 1;

		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Frame Building

void CMicromodTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	if( m_pCtx->m_uAddMBTCP ) {

		AddWord(++m_pCtx->m_wTrans);

		AddWord(0);

		AddByte(0);

		AddByte(0);
		}

	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CMicromodTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < BUFFSIZE ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CMicromodTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CMicromodTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer

BOOL CMicromodTCPDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {

		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}
			
		if( GetFrame() ) {

			if( m_pRx[0] == m_pTx[0] ) {

				if( m_pCtx->m_uAddMBTCP ) {

					memcpy(m_bRx, m_bRx + 6, m_bRx[5]);
					}

				if( fIgnore ) {

					return TRUE;
					}

				if( m_pRx[1] & 0x80 ) {

					m_bNAKResp = m_pRx[2];
		
					return FALSE;
					}

				return TRUE;
				}
			}
		}

	CloseSocket(TRUE);
		
	return FALSE;
	}

BOOL CMicromodTCPDriver::PutFrame(void)
{
	if( !m_pCtx->m_uAddMBTCP ) {

		m_CRC.Preset();

		for( UINT i = 0; i < m_uPtr; i++ ) {
	
			BYTE bData = m_pTx[i];

			m_CRC.Add(bData);
			}

		WORD wCRC = m_CRC.GetValue();

		AddByte(LOBYTE(wCRC));

		AddByte(HIBYTE(wCRC));
		}

	else {
		m_bTx[5] = m_uPtr - 6;
		}

	return SendFrame();
	}

BOOL CMicromodTCPDriver::GetFrame(void)
{
	return RecvFrame();
	}

// Read Handlers

CCODE CMicromodTCPDriver::DoWordRead(PDWORD pData, UINT uCount)
{
	switch( m_uTable ) {
	
		case SP_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SP_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(m_uOffset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			WORD x   = PU2(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CMicromodTCPDriver::DoLongRead(PDWORD pData, UINT uCount)
{
	UINT uTable = m_uTable;

	switch( uTable ) {
	
		case SP_HOLD32:
		case SP_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SP_ANALOG32:
		case SP_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(m_uOffset - 1);
	
	AddWord(uTable == SP_HOLD32 ? uCount : uCount * 2);
	
	if( Transact(FALSE) ) {

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_pRx + 3)[n];
			
			pData[n] = (DWORD)MotorToHost(x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CMicromodTCPDriver::DoBitRead(PDWORD pData, UINT uCount)
{
	switch( m_uTable ) {
			
		case SP_OUTPUT:
			
			StartFrame(0x01);
			
			break;
			
		case SP_INPUT:
			
			StartFrame(0x02);
			
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(m_uOffset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CMicromodTCPDriver::DoWordWrite(PDWORD pData, UINT uCount)
{
	UINT uOffset  = m_uOffset - 1;

	if( m_uTable == SP_HOLD ) {

		if( uCount == 1 ) {
			
			StartFrame(6);
			
			AddWord(uOffset);
			
			AddWord(LOWORD(pData[0]));
			}
		else {
			StartFrame(16);
			
			AddWord(uOffset);
			
			AddWord(uCount);
			
			AddByte(uCount * 2);
			
			for( UINT n = 0; n < uCount; n++ ) {
				
				AddWord(LOWORD(pData[n]));
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMicromodTCPDriver::DoLongWrite(PDWORD pData, UINT uCount)
{
	UINT uTable = m_uTable;
		
	if( uTable == SP_HOLD32 || uTable == SP_HOLD ) {

		StartFrame(16);
		
		AddWord(m_uOffset - 1);
		
		AddWord(uTable == SP_HOLD32 ? uCount : uCount * 2);
		
		AddByte(uCount * 4);
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(pData[n]);
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CMicromodTCPDriver::DoBitWrite(PDWORD pData, UINT uCount)
{
	UINT uOffset  = m_uOffset - 1;

	if( m_uTable == SP_OUTPUT ) {

		if( uCount == 1 ) {
			
			StartFrame(5);
			
			AddWord(uOffset);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(uOffset);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Function Code 65 Handlers

CCODE CMicromodTCPDriver::Handle65(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsRead)
{
	UINT uTable = Addr.a.m_Table;

	switch( uTable ) {

		case SP_RDB:
			if( !fIsRead ) {

				return uCount;
				}

			MakeMin(uCount, 10);

			m_uPtr = SetRDBCommand(Addr, pData, uCount);
			break;

		case SP_RDBO:
			if( fIsRead ) *pData = m_dRDBO;

			else m_dRDBO = *pData;

			return 1;

		default:
			fIsRead ? ReadList(Addr, uCount) : WriteItems(Addr, *pData);
			break;
		}

	memset(m_bRx, 0, 4);

	if( Transact(FALSE) ) {

		if( m_bRx[0] == m_pCtx->m_bDrop && m_bRx[1] == 0x41 ) {

			if( fIsRead ) {

				return Get65Data(Addr, pData, uCount);
				}

			m_dWriteErr = 0;

			return 1;
			}
		}

	if( !fIsRead ) {

		if( IsDuplicateError() ) {

			if( HandleDuplicateSeqNum(Addr, *pData) ) {

				m_dWriteErr = 0;

				return 1;
				}
			}

		if( (m_bRx[1] & 0x80) || (++m_dWriteErr >= 3) ) {

			m_dWriteErr = 0;

			return 1;
			}
		}

	return (m_bRx[1] & 0x80) ? CCODE_ERROR | CCODE_NO_RETRY : CCODE_ERROR;
	}

void CMicromodTCPDriver::StartFrame65(void)
{
	m_uPtr = 0;

	if( m_pCtx->m_uAddMBTCP ) {

		AddWord(++m_pCtx->m_wTrans);

		AddWord(0);

		AddByte(0);

		AddByte(0);
		}

	AddByte(m_pCtx->m_bDrop);

	AddByte(0x41);

	AddByte(3);
	}

UINT CMicromodTCPDriver::SetRDBCommand(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame65();

	AddByte(QMFCRDB);

	AddByte((Addr.a.m_Offset >> 4) & 0xC0); // database to access

	AddByte(5);		// total 

	Put65Data(m_dRDBO, 4);	// address to access

	AddByte(uCount * 4);	// number of bytes to read

	return 11 + m_pCtx->m_uAddMBTCP; // frame size
	}

void CMicromodTCPDriver::ReadList(AREF Addr, UINT uCount)
{
	StartFrame65();

	AddByte(QMFCMFG);

	AddByte(0);

	UINT uFirst	= Addr.a.m_Offset;

	UINT uRecvSize	= GetDataSize(uFirst, &uCount);

	AddByte(6);	// size for sections

	m_pCtx->m_uPackFG[0] = 1;

	m_pCtx->m_uPackFG[1] = uFirst;

	AddByte(1);	// Set Pack - required to read written data correctly

	AddByte(1);	// read sections

	AddWord(m_pCtx->m_pOFFArray[uFirst]);	// starting offset

	AddWord(LOWORD(uRecvSize));	// byte count to read
	}

void CMicromodTCPDriver::WriteItems(AREF Addr, DWORD dData)
{
	StartFrame65();

	AddByte(QMFCSWA);

	AddByte(0);

	UINT uItem = Addr.a.m_Offset;

	UINT uSize = m_pCtx->m_pSZEArray[uItem];

	AddByte(6 + uSize);

	AddLong(m_pCtx->m_pLSPArray[uItem]);

	AddByte(0);

	AddByte((m_pCtx->m_bDrop << 4) + m_bSeq);

	m_bSeq = (m_bSeq + 5) % 16;

	switch( uSize ) {

		case 1:
			AddByte(LOBYTE(dData));
			break;

		case 2:
			AddWord(LOWORD(dData));
			break;

		case 3:
			AddByte(LOBYTE(HIWORD(dData)));
			AddWord(LOWORD(dData));
			break;

		default:
			AddLong(dData);

			while( uSize > 4 ) {

				AddByte(0);

				uSize--;
				}
			break;
		}
	}

void CMicromodTCPDriver::Put65Data(DWORD dData, UINT uCount)
{
	switch( uCount ) {

		case 4: AddByte(HIBYTE(HIWORD(dData)));
		case 3: AddByte(LOBYTE(HIWORD(dData)));
		case 2: AddByte(HIBYTE(LOWORD(dData)));
		case 1: AddByte(LOBYTE(LOWORD(dData)));
		}
	}

BOOL CMicromodTCPDriver::HandleDuplicateSeqNum(AREF Addr, DWORD dData)
{
	for( UINT i = 0; i < 5; i++ ) {

		m_bSeq++;

		if( DoDuplicateWrite(Addr, dData) ) {

			return TRUE;
			}

		if( !IsDuplicateError() ) {

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CMicromodTCPDriver::DoDuplicateWrite(AREF Addr, DWORD dData)
{
	WriteItems(Addr, dData);

	if( Transact(FALSE) ) {

		if( m_bRx[1] == EXTMOD ) return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodTCPDriver::IsDuplicateError(void)
{
	return (m_bRx[1] == (EXTMOD | 0x80)) && (m_bRx[2] == 0x71);
	}

// Special Command - uses Write_Attribute instead of Seq_Write_Attribute

void CMicromodTCPDriver::SendAckShutdown(void)
{
	StartFrame65();

	// Write Attribute command
	AddByte(QMFATW);
	// Qualifier
	AddByte(0);
	// Byte count following
	AddByte(5);
	// LSP of IF_141(1)
	AddByte(0);
	AddByte(0x40);
	AddByte(0);
	AddByte(141);
	// Data
	AddByte(1);

	Transact(TRUE); // ignore response
	}

// Receive Handling

CCODE CMicromodTCPDriver::Get65Data(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SP_RDB:	return GetRDBData(pData, uCount);
		}

	return GetAttributeData(Addr, pData, uCount);
	}

CCODE CMicromodTCPDriver::GetAttributeData(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bRcvSz  = m_bRx[RX_COUNT];	// number of received data bytes

	PBYTE pRcvD  = &m_bRx[RX_DATA];

	if( pRcvD[0] == 1 ) { // first item is in error

		SetADError(Addr, 0);

		return 1;	// try next
		}

	UINT uAdd = Addr.a.m_Offset;

	UINT uEnd = pRcvD[0];	// if !0, valid data up to this point

	if( !(BOOL)uEnd ) {

		uEnd = bRcvSz;	// all are valid data
		}

	uEnd--;			// adjust for error byte

	pRcvD++;		// first data byte

	UINT uItemsDone	= 0;

	UINT uByteTotal	= 0;

	for( UINT i = 0; i < uCount; i++ ) {

		UINT uBit = m_pCtx->m_pBITArray[uAdd + i];	// Check if bit

		if( uBit < 0xFF ) { // might be multiple bits in this byte

			UINT uThisO = m_pCtx->m_pOFFArray[uAdd + i];

			BYTE b = pRcvD[uByteTotal++];

			UINT uMask = 1 << uBit;

			while( i < uCount ) {

				if( LOBYTE(uMask) ) {

					pData[i] = (DWORD)((uMask & b) ? 0xFFFFFFFF : 0);

					uItemsDone++;

					if( uThisO == m_pCtx->m_pOFFArray[uAdd + i + 1] ) {

						i++;

						uMask <<= 1;
						}

					else uMask = 0;
					}

				else break;
				}
			}

		else {

			UINT uItemSize	= m_pCtx->m_pSZEArray[uAdd + i];

			UINT uPos	= uByteTotal + RX_DATA + 1;

			switch( uItemSize ) {

				case 1:
					pData[i] = pRcvD[uByteTotal];
					break;

				case 2:
					pData[i] = GetDataWORD(uPos);
					break;

				case 3:
					pData[i] = GetDataDWORD(uPos);
					pData[i] >>= 8;
					break;

				case 4:
				default:
					pData[i] = GetDataDWORD(uPos);
					break;
				}

			uByteTotal += uItemSize;

			uItemsDone++;
			}

		if( uByteTotal >= uEnd ) {	// hit the next error or end of buffer

			return uItemsDone;
			}
		}

	return CCODE_ERROR;
	}

void CMicromodTCPDriver::SetADError(AREF Addr, UINT uPos)
{
	m_pCtx->m_dLatestErr = m_pCtx->m_pLSPArray[Addr.a.m_Offset + uPos]; // store latest error LSP
	}

CCODE CMicromodTCPDriver::GetRDBData(PDWORD pData, UINT uCount)
{
	for( UINT i = 0; i < uCount; i++ ) {

		pData[i] = GetDataDWORD(4 + (4 * i));
		}

	return uCount;
	}

DWORD CMicromodTCPDriver::GetDataDWORD(UINT uPos)
{
	return (DWORD)MotorToHost(*PU4(&m_bRx[uPos]));
	}

DWORD CMicromodTCPDriver::GetDataDWORD(PBYTE pData)
{
	return (DWORD)MotorToHost(*PU4(pData));
	}

WORD CMicromodTCPDriver::GetDataWORD(UINT uPos)
{
	return (WORD)MotorToHost(*PU2(&m_bRx[uPos]));
	}

// Helpers

void CMicromodTCPDriver::SetModbusData(AREF Addr)
{
	m_uTable  = Addr.a.m_Table;
	m_uOffset = Addr.a.m_Offset;
	m_uExtra  = Addr.a.m_Extra;
	m_uType   = Addr.a.m_Type;
	}

BOOL CMicromodTCPDriver::IsFuncCode65Item(UINT uTable)
{
	switch( uTable ) {

		case SP_65:
		case SP_RDB:
		case SP_RDBO:

			return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodTCPDriver::Is65Command(UINT uTable)
{
	switch( uTable ) {

		case SP_RDB:
		case SP_RDBO:
			return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodTCPDriver::NoReadTransmit(AREF Addr, UINT *pCount)
{
	UINT uACK = m_pCtx->m_uACKSHUTPosition;

	UINT uAdd = Addr.a.m_Offset;

	if( uAdd != uACK ) {

		if( uAdd < uACK && uAdd + *pCount > uACK ) {

			*pCount = uACK - uAdd;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CMicromodTCPDriver::IsAckShutdown(UINT uOffset)
{
	return m_pCtx->m_pLSPArray[uOffset] == ACKSHUTLSP;
	}

BOOL CMicromodTCPDriver::IsValid65Access(UINT uItem)
{
	return m_pCtx->m_wArraySize > 1 && m_pCtx->m_wArraySize > uItem;
	}

// Array Help

UINT CMicromodTCPDriver::GetTableNum(UINT uArrayItem)
{
	DWORD dLSP = m_pCtx->m_pLSPArray[uArrayItem];

	return (dLSP & LSP_BLK_MASK) >> LSP_BLK_POS; 
	}

UINT CMicromodTCPDriver::GetParamNum(UINT uArrayItem)
{
	DWORD dLSP = m_pCtx->m_pLSPArray[uArrayItem];

	return dLSP & LSP_PAR_MASK;
	}

UINT CMicromodTCPDriver::GetDataSize(UINT uStart, UINT *pCount)
{
	if( (BOOL)m_pCtx->m_uPackFG[0] ) {		// controller saved the entire block that is being read in parts

		if( uStart != m_pCtx->m_uPackFG[1] ) {	// system is not requesting the next packed item, clear pack flag

			ClearPackFG();
			}
		}

	PWORD pOFF	= m_pCtx->m_pOFFArray;
	PWORD pSZE	= m_pCtx->m_pSZEArray;
	PWORD pBIT	= m_pCtx->m_pBITArray;

	UINT uMax	= sizeof(m_bRx) - 16;	// leave room for one more after uMax byte count is reached

	UINT uTotalOFF	= 0;

	UINT uCnt	= *pCount;

	UINT uLast	= min(uStart + uCnt, (UINT)m_pCtx->m_wArraySize - 1);	// don't examine terminator

	UINT uItem	= uStart;

	while( uItem < uLast ) {

		UINT uSize = pSZE[uItem];

		UINT uOff0 = pOFF[uItem];

		if( uTotalOFF < uMax ) {			// response will fit in rcv buffer

			if( pBIT[uItem] < 0xFF ) {		// handle multiple bits in one offset

				UINT uFirst = uItem;

				while( uItem < uLast ) {

					uItem++;

					if( uOff0 != pOFF[uItem] ) { // next item has different offset

						break;
						}
					}

				uTotalOFF++;
				}

			else {
				if( uOff0 + uSize == pOFF[uItem+1] ) {	// next offset is contiguous to this one

					uTotalOFF += uSize;

					uItem++;
					}

				else {					// next offset is not contiguous, stop this read here
					*pCount = uItem - uStart + 1;

					ClearPackFG();			// next read will not be of packed item

					return uTotalOFF + uSize;
					}
				}
			}

		else {
			m_pCtx->m_uPackFG[0]++;		// expect to get packed data next

			m_pCtx->m_uPackFG[1] = uItem;	// next item to be called in packed list

			break;
			}
		}

	*pCount = uItem - uStart;

	return uTotalOFF;
	}

void CMicromodTCPDriver::ClearPackFG(void)
{
	memset(m_pCtx->m_uPackFG, 0, sizeof(m_pCtx->m_uPackFG));
	}

// Array Loading

void CMicromodTCPDriver::AddArrays(LPCBYTE &pData)
{
	UINT uSize	= m_pCtx->m_wArraySize;

	m_pCtx->m_pLSPArray	= new DWORD [uSize];
	m_pCtx->m_pOFFArray	= new WORD  [uSize];
	m_pCtx->m_pSZEArray	= new WORD  [uSize];
	m_pCtx->m_pBITArray	= new WORD  [uSize];

	BOOL fNoAckShutdown	= TRUE;

	for( UINT i = 0; i < uSize; i++ ) {

		DWORD d = GetLong(pData);

		m_pCtx->m_pLSPArray[i] = d;

		if( fNoAckShutdown ) {

			if( IsAckShutdown(i) ) {

				m_pCtx->m_uACKSHUTPosition = i;

				fNoAckShutdown = FALSE;
				}
			}

		if( d == 0xFFFFFFFF ) break;
		}

	for( i = 0; i < uSize; i++ ) {

		WORD w = GetWord(pData);

		m_pCtx->m_pOFFArray[i] = w;

		if( w == 0xFFFF ) break;
		}

	for( i = 0; i < uSize; i++ ) {

		WORD w = GetWord(pData);

		m_pCtx->m_pSZEArray[i] = w;

		if( w == 0xFFFF ) break;
		}

	for( i = 0; i < uSize; i++ ) {

		WORD w = GetWord(pData);

		m_pCtx->m_pBITArray[i] = w;

		if( w == 0xFFFF ) break;
		}
	}

// Socket Management

BOOL CMicromodTCPDriver::CheckSocket(void)
{
	if( (BOOL)m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMicromodTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CMicromodTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CMicromodTCPDriver::SendFrame(void)
{
	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMicromodTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uEnd  = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			AfxTrace0("\r\n"); for( UINT k = 0; k < uSize; k++ ) AfxTrace1("<%2.2x>", m_bRx[k]);

			uPtr += uSize;

			if( m_pCtx->m_uAddMBTCP ) {

				if( uPtr >= 8 ) {

					UINT uTotal = 6 + m_bRx[5];

					if( uPtr >= uTotal ) {

						return TRUE;
						}
					}
				}

			else {
				if( !uEnd ) {

					switch( m_uRWType ) {

						case F65READ:
						case F65WRITE:

							if( uPtr >= 4 ) uEnd = 6 + m_bRx[3];
							break;

						case MDBREAD:

							if( uPtr >= 3 ) uEnd = 5 + m_bRx[2];
							break;

						case MDBWRITE:

							uEnd = 8;
							break;
						}
					}

				if( uEnd ) {

					if( uPtr >= uEnd ) {

						if( m_bRx[0] == m_bTx[0] && m_bRx[1] == m_bTx[1] ) {

							m_CRC.Preset();
				
							PBYTE p = m_bRx;
				
							for( UINT i = 0; i < uEnd - 2; i++ ) {

								m_CRC.Add(*(p++));
								}
							
							WORD c1 = IntelToHost(PWORD(p)[0]);
				
							WORD c2 = m_CRC.GetValue();

							return c1 == c2;
							}

						return FALSE;
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

// End of File
