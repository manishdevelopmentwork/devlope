
#include "Intern.hpp"

#include "ProgramCodeItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Code
//

// Dynamic Class

AfxImplementDynamicClass(CProgramCodeItem, CCodedItem);

// Constructor

CProgramCodeItem::CProgramCodeItem(void)
{
	m_ReqType = typeVoid;

	m_Error   = FALSE;
	}

// Destructor

CProgramCodeItem::~CProgramCodeItem(void)
{
	}

// Attributes

BOOL CProgramCodeItem::HasError(void) const
{
	return m_Error || IsBroken();
	}

CRange CProgramCodeItem::GetErrorPos(void) const
{
	return CRange(m_From, m_To);
	}

CString CProgramCodeItem::GetAsText(void) const
{
	if( !m_Cache.IsEmpty() ) {

		PCWORD pData = m_Cache.GetPointer();

		UINT   uSize = m_Cache.GetCount();

		return CString(PCTXT(pData), uSize);
		}

	return GetSource(TRUE);
	}

// Operations

void CProgramCodeItem::SetAsText(CString Text)
{
	m_Object.Empty();

	m_Source.Empty();

	m_Cache.Empty();

	PCWORD pData = PCWORD(PCTXT(Text));

	UINT   uSize = Text.GetLength();

	m_Cache.Append(pData, uSize);

	m_Error = FALSE;
	}

void CProgramCodeItem::ClearCache(void)
{
	m_Cache.Empty();

	m_Error = FALSE;
	}

// Overridables

void CProgramCodeItem::LoadParams(CCompileIn &In, BOOL &fFunc)
{
	CProgramItem *pItem = (CProgramItem *) GetParent();

	In.m_Scope  = pItem->m_Name;

	In.m_Debug  = pItem->GetDebugFlags();

	In.m_uParam = GetParamCount();

	In.m_pParam = GetParamList ();

	fFunc       = TRUE;
	}

void CProgramCodeItem::KillParams(CCompileIn &In)
{
	}

// Property Save Filter

BOOL CProgramCodeItem::SaveProp(CString const &Tag) const
{
	if( !m_Error ) {

		if( Tag == L"Error" ) {

			return FALSE;
			}

		if( Tag == L"From" ) {

			return FALSE;
			}

		if( Tag == L"To" ) {

			return FALSE;
			}
		}

	return CCodedItem::SaveProp(Tag);
	}

// Meta Data

void CProgramCodeItem::AddMetaData(void)
{
	CCodedItem::AddMetaData();

	Meta_AddWide   (Cache);
	Meta_AddInteger(Error);
	Meta_AddInteger(From);
	Meta_AddInteger(To);

	Meta_SetName((IDS_PROGRAM_CODE));
	}

// Recompile Hook

BOOL CProgramCodeItem::OnRecompile(CError &Error, BOOL fExpand)
{
	if( !Error.AllowUI() ) {

		if( !fExpand ) {

			CProgramItem *pProgram = (CProgramItem *) GetParent();

			if( pProgram->StoreInFile() ) {

				if( pProgram->Translate(Error, FALSE) ) {

					return TRUE;
					}

				m_pDbase->SetRecomp();

				return FALSE;
				}
			}
		}

	return CCodedItem::OnRecompile(Error, fExpand);
	}

// Implementation

UINT CProgramCodeItem::GetParamCount(void)
{
	CProgramItem *pProgram = (CProgramItem *) GetParent();

	return pProgram->GetParameterCount();
	}

CFuncParam * CProgramCodeItem::GetParamList()
{
	CProgramItem *pProgram = (CProgramItem *) GetParent();

	return pProgram->GetParameterList();
	}

// End of File
