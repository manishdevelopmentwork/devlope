
#include "intern.hpp"

#include <crtdbg.h>

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Debug Only Code
//

#ifdef _DEBUG

//////////////////////////////////////////////////////////////////////////
//
// Memory Helpers
//

static void DumpClient(void *p, size_t n)
{
	int t = _CrtReportBlockType(p);

	if( HIWORD(t) ) {

		AfxTrace(L" %s\n", ((CObject *) p)->GetClassName());
		}
	else
		AfxTrace(L" Raw Data\n");

	AfxDump(p, n);

	AfxTrace(L"\n");
	}

static BOOL IsFourBytes(void const *pData, BYTE bCheck)
{
	for( int n = 0; n < sizeof(pData); n++ ) {

		if( ((BYTE *) &pData)[n] == bCheck ) {

			continue;
			}

		return FALSE;
		}

	return TRUE;
	}

static BOOL IsValidPointer(void const *pData)
{
	if( pData == NULL ) {

		AfxTrace(L"ERROR: Attempting to use NULL memory pointer\n");

		return FALSE;
		}

	if( IsFourBytes(pData, 0xCD) ) {

		AfxTrace(L"ERROR: Attempting to use pointer from empty block\n");

		return FALSE;
		}

	if( IsFourBytes(pData, 0xDD) ) {

		AfxTrace(L"ERROR: Attempting to use pointer from released block\n");

		return FALSE;
		}

	if( IsBadWritePtr(PVOID(pData), 1) ) {

		AfxTrace(L"ERROR: Attempting to use invalid memory pointer\n");

		return FALSE;
		}
	
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Memory Management Functions
//

void * AfxMalloc(UINT uSize)
{
	return AfxMalloc(uSize, NULL, 0, FALSE);
	}

void AfxFree(void *pData)
{
	AfxFree(pData, FALSE);
	}

void * AfxMalloc(UINT uSize, PCSTR pFile, UINT uLine, BOOL fObject)
{
	return _malloc_dbg(uSize, MAKELONG(_CLIENT_BLOCK, fObject), pFile, uLine);
	}

void AfxFree(void *pData, BOOL fObject)
{
	if( !pData ) return;

	AfxAssert(IsValidPointer(pData));

	_free_dbg(pData, MAKELONG(_CLIENT_BLOCK, fObject));
	}

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Context
//

static _CrtMemState m_States[128];

static UINT         m_uState = 0;

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Functions
//

BOOL AfxCheckBlock(void *pData)
{
	return _CrtIsMemoryBlock(pData, 1, NULL, NULL, NULL);
	}

BOOL AfxCheckMemory(void)
{
	CCriticalGuard Guard;

	return _CrtCheckMemory();
	}

void AfxDumpBlock(void const *pData)
{
	if( IsValidPointer(pData) ) {

		int   nSize = PINT(pData)[-4];

		long  Index = 0;

		char *pFile = NULL;

		int   nLine = 0;

		if( _CrtIsMemoryBlock(pData, nSize, &Index, &pFile, &nLine) ) {

			int nType = _CrtReportBlockType(pData);

			AfxTrace(L"BLOCK at %p : %u bytes : ", pData, nSize);

			if( LOWORD(nType) == _CLIENT_BLOCK ) {

				if( HIWORD(nType) ) {

					AfxTrace(L"Class %s : ", ((CObject *) pData)->GetClassName());
					}
				else
					AfxTrace(L"Raw Data : ");
				}

			if( pFile ) {

				if( IsBadStringPtrA(pFile, 10) ) {

					AfxTrace(L"Line %u of [Unloaded]\n\n", nLine);
					}
				else
					AfxTrace(L"Line %u of %hs\n\n", nLine, pFile);
				}
			else
				AfxTrace(L"\n\n");

			AfxDump(pData, nSize);
			}
		}
	}

void AfxBreakOnAlloc(UINT uIndex)
{
	_CrtSetBreakAlloc(uIndex);
	}

UINT AfxMarkMemory(void)
{
	m_uState = (m_uState + 1) % elements(m_States);

	_CrtMemCheckpoint(&m_States[m_uState]);

	return m_uState;
	}

BOOL AfxDumpMemory(UINT uSince, PCSTR pFile)
{
	if( _CrtCheckMemory() ) {

		_CrtMemState state, delta;

		_CrtMemCheckpoint(&state);

		if( _CrtMemDifference(&delta, &m_States[uSince], &state) ) {

			if( delta.lCounts[_NORMAL_BLOCK] || delta.lCounts[_CLIENT_BLOCK] ) {

				_CrtSetDumpClient(DumpClient);

				_CrtMemDumpAllObjectsSince(&m_States[uSince]);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void AfxDumpMemoryLeaks(void)
{
	_CrtSetDumpClient(DumpClient);

	AfxAssert(!_CrtDumpMemoryLeaks());
	}

// End of File

#endif
