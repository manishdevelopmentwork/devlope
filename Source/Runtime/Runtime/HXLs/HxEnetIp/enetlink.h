/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ENETLINK.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Ethernet Link object 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************/


#ifndef ENETLINK_H
#define ENETLINK_H


#define ENETLINK_CLASS					0xF6			/* Ethernet Link class identifier */
#define ENETLINK_CLASS_REVISION			1				/* Ethernet Link class revision */

/* Class and instance attribute numbers */
#define ENETLINK_CLASS_ATTR_REVISION              1
#define ENETLINK_CLASS_ATTR_MAX_INSTANCE          2
#define ENETLINK_CLASS_ATTR_NUM_INSTANCES         3

#define ENETLINK_INST_ATTR_INTERFACE_SPEED		  1
#define ENETLINK_INST_ATTR_INTERFACE_FLAGS		  2
#define ENETLINK_INST_ATTR_PHYSICAL_ADDRESS       3

#define ENETLINK_GET_AND_CLEAR      0x4C		/* Additional service code */

#define MAC_ADDR_LENGTH				6			/* Size of the MAC Id in bytes */	

#define ENETLINK_FLAG_LINK_ACTIVE		0x00000001	/* Bit 0 indicates whether link is active */

#define EL_FLAG_DUPLEX_MODE				0x00000002	/* Bit 1 indicates whether it is duplex mode */

/* Class attribute structure */
typedef struct tagENETLINK_CLASS_ATTR
{
   UINT16  iRevision;
   UINT16  iMaxInstance;
   UINT16  iNumInstances;
}
ENETLINK_CLASS_ATTR;

#define ENETLINK_CLASS_ATTR_SIZE	6

extern UINT32   glInterfaceSpeed;
extern UINT32   glInterfaceFlags;
extern UINT8    gmacAddr[MAC_ADDR_LENGTH];


extern void enetlinkInit();
extern void enetlinkGetMacID();
extern void enetlinkParseClassInstanceRequest( INT32 nRequest );
extern void enetlinkParseClassRequest( INT32 nRequest );
extern void enetlinkSendClassAttrAll( INT32 nRequest );
extern void enetlinkSendClassAttrSingle( INT32 nRequest );
extern void enetlinkParseInstanceRequest( INT32 nRequest );
extern void enetlinkSendInstanceAttrAll( INT32 nRequest );
extern void enetlinkSendInstanceAttrSingle( INT32 nRequest );

#endif /* #ifndef ENETLINK_H */

