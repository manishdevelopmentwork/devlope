
#include "Intern.hpp"

#include "Peer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// VPN Interface
//

// Constructor

CPeer::CPeer(string const &face, string const &root) : CInterface(face, root)
{
	m_status = m_root + "/peer";

	ClearStatus();
}

// Destructor

CPeer::~CPeer(void)
{
}

// Overridables

bool CPeer::OnConfigure(void)
{
	m_mode = GetConfig("mode", 0);

	m_peer = GetConfig("peer", "");

	return true;
}

int CPeer::OnExecute(void)
{
	switch( m_state ) {

		case stateNotPresent:
		{
			m_state = stateWaitPeer;

			return 500;
		}
		break;

		case stateWaitPeer:
		{
			if( CheckPeer(false) ) {

				m_state = statePeerOkay;

				return 4000;
			}

			return 2000;
		}
		break;

		case statePeerOkay:
		{
			if( !CheckPeer(true) ) {

				m_state = stateWaitPeer;

				return 1000;
			}

			return 2000;
		}
		break;
	}

	m_state = stateNotPresent;

	m_ctime = 0;

	return 250;
}

void CPeer::OnNewState(void)
{
	if( m_state == statePeerOkay ) {

		RunLinkStart(true);
	}

	if( m_prev == statePeerOkay ) {

		RunLinkStart(false);
	}

	WriteStatus();
}

void CPeer::OnStopping(void)
{
	if( m_state == statePeerOkay ) {

		RunLinkStart(false);

		m_ctime = 0;
	}

	ClearStatus();
}

bool CPeer::OnCommand(string const &cmd)
{
	// TODO -- Should we accept a "failed" command here?

	return false;
}

// Implementation

bool CPeer::CheckPeer(bool good)
{
	if( !m_peer.empty() ) {

		switch( m_mode ) {

			case 0:
			{
				return true;
			}
			break;

			case 1:
			{
				global bool IsReachable(char const *name);

				return IsReachable(m_peer.c_str());
			}
			break;

			case 2:
			{
				UINT c = 0;

				for( UINT n = 0; n < 3; n++ ) {

					if( system(CPrintf("ping -c 1 -W 2 %s", m_peer.c_str())) == 0 ) {

						c++;
					}
				}

				return c >= (good ? 1 : 2);
			}

			case 3:
			{
				// TODO -- This designed to work with a central server
				// that mediates VPN connections. It will send our name
				// and if we get a signal back, it will open the link.

				return true;
			}
		}
	}

	return true;
}

string CPeer::GetStateName(void)
{
	switch( m_state ) {

		case stateWaitPeer:
			return "Waiting for Peer";

		case statePeerOkay:
			return "Peer Accessible";
	}

	return CInterface::GetStateName();
}

bool CPeer::WriteStatus(void)
{
	string   tmp(m_status + ".tmp");

	ofstream stm(tmp);

	if( stm.good() ) {

		stm << "{\n";

		stm << "\t\"state\": \"" << GetStateName().c_str() << "\",\n";

		stm << "\t\"ctime\": " << m_ctime << "\n";

		stm << "}\n";

		stm.close();

		unlink(m_status.c_str());

		rename(tmp.c_str(), m_status.c_str());

		return true;
	}

	return false;
}

// End of File
