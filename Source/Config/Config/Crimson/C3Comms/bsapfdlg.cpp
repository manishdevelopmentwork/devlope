
#include "intern.hpp"

#include "bsapfull.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBSAPFULLMasterTagsDialog, CStdAddrDialog);

// Constructor

CBSAPFULLMasterTagsDialog::CBSAPFULLMasterTagsDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_pViewWnd	= ((CBSAPFULLTagsDeviceOptions	*)(pConfig))->GetViewWnd();

	m_fSelect	= TRUE;

	m_fUpdate	= FALSE;
	
	m_Images.Create(L"CommsManager", 16, 0, IMAGE_BITMAP, 0);

	SetName(fPart ? L"PartBSAPFULLDlg" : L"FullBSAPFULLDlg");
	}

// Destructor

CBSAPFULLMasterTagsDialog::~CBSAPFULLMasterTagsDialog(void)
{
//	afxThread->SetStatusText("");
	}

// Initialisation

void CBSAPFULLMasterTagsDialog::SetCaption(CString const &Text)
{
	m_Caption = Text;
	}

void CBSAPFULLMasterTagsDialog::SetSelect(BOOL fSelect)
{
	m_fSelect = fSelect;
	}

// Message Map

AfxMessageMap(CBSAPFULLMasterTagsDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown   )

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(PBCREATE, OnCreateTag)
	AfxDispatchCommand(PBREFRSH, OnUpdateAddress)
	AfxDispatchCommand(PBBOOL, OnCreateBOOLAck)
	AfxDispatchCommand(PBREAL, OnCreateREALAck)
	AfxDispatchNotify(CBTYPE, CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(CBLEVEL, CBN_SELCHANGE, OnComboChange)

	AfxMessageEnd(CBSAPFULLMasterTagsDialog)
	};

// Message Handlers

BOOL CBSAPFULLMasterTagsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(m_Caption);

	GetDlgItem(IDCANCEL).EnableWindow(m_fSelect);

	GetDlgItem(IDOK).SetWindowText(m_fSelect ? L"OK" : L"Close");

	if( !m_fPart ) {

		LoadCBAll();

		LoadNames();
		}

	DSetArrayStatus();

	ShowAddress(*m_pAddr);

	return FALSE;
	}

// Notification Handlers

void CBSAPFULLMasterTagsDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_hSelect  = Info.itemNew.hItem;

	m_dwSelect = Info.itemNew.lParam;

	CAddress Addr   = *m_pAddr;

	if( m_dwSelect == Addr.m_Ref ) {
		
		Addr = *m_pAddr;
		}
	else
		Addr = (CAddress &) m_dwSelect;

	if( IsNRTTable(Addr.a.m_Table) && !m_fSelect ) {

		ShowAddress(Addr);

		CString s;

		s.Printf(L"%d", Addr.a.m_Offset);

		GetDlgItem(DINDEX).SetWindowTextW(s);

		return;
		}

	ClearHolding();

	UINT uTypeSel = GetCBType();

	BOOL fNRT     = FALSE;

	if( Addr.m_Ref ) {

		uTypeSel = Addr.a.m_Type;

		fNRT     = IsNRTTable(Addr.a.m_Table);
		}

	SetCBType(uTypeSel, fNRT, IsStrTable(Addr.a.m_Table));

	ShowAddress(Addr);
	}

BOOL CBSAPFULLMasterTagsDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	if( Info.wVKey == VK_DELETE ) {
		
		if( IsDown(VK_SHIFT) ) {

			return DeleteTag();
			}
		}

	return FALSE;
	}

void CBSAPFULLMasterTagsDialog::OnComboChange(UINT uID, CWnd &Wnd) {

	UINT n = GetBoxPosition(uID);

	switch( uID ) {

		case CBLEVEL:

			UINT uMLevel;

			uMLevel = DGetMLevel();

			if( n <= uMLevel ) {

				SetBoxPosition(CBLEVEL, uMLevel + 1);
				}
			break;
		}

	DoDlgEnables();
	}

// Command Handlers

BOOL CBSAPFULLMasterTagsDialog::OnOkay(UINT uID)
{
	if( m_fSelect ) {
	
		CAddress Addr;

		CError Error(TRUE);

		BOOL fOk = FALSE;

		Addr.m_Ref   = m_dwSelect;

		UINT uSelect = GetSelect(Addr.a.m_Table, Addr.a.m_Offset);

		if( !m_fPart ) {

			if( uSelect >= DGetArrayCount() ) {

				m_pAddr->m_Ref = 0;

				EndDialog(TRUE);

				return TRUE;
				}
			}

		else {
			Addr = *m_pAddr;
			}

		CString Text;

		if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ExpandAddress(Text, Addr) ) {

			fOk = m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text);
			}

		if( fOk ) {

			*m_pAddr = Addr;

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
			
			EndDialog(TRUE);
			
			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus(2002);

		return TRUE;
		}

	EndDialog(TRUE);
	
	return TRUE;
	}

BOOL CBSAPFULLMasterTagsDialog::OnCreateTag(UINT uID)
{
	CAddress Addr;

	ClearHolding();

	Addr.m_Ref = MakeAddrFromSelects(NOTHING);

	if( Addr.a.m_Offset == 0xFFFF ) {

		return TRUE;
		}

	CString Name;

	BOOL fOk  = FALSE;

	CError Error(TRUE);

	Name = GetDlgItem(ETNAME).GetWindowTextW();

	CString PostFix;

	PostFix.Printf( L"#L%1.1dD%2.2X#", GetLevel(), GetDeviceAddr());

	if( Name == L"None" || Name.Left(4) == L"zTMS" || Name.Left(4) == L"zNRT" ) {

		Name = ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateName(Addr, PostFix);
		}

	else {
		UINT uLen = Name.GetLength();

		if( uLen > 7 && Name[uLen-1] == '#' ) {

			Name = Name.Left(uLen - 7);

			uLen = Name.GetLength();

			if( uLen > 5 ) {

				Name = Name.Left(uLen - 1);
				}
			}
		}

	CStringDialog Dlg(L"Create Tag", L"Tag Name", Name);

	if( !IsDown(VK_SHIFT) ) {
		
		if( Dlg.Execute(ThisObject) ) {

			Name    = Dlg.GetData();

			Name.Printf( L"%s%s", Name, PostFix );
			}
		else
			return TRUE;
		}

	if( Name.Left(4) == L"zTMS" || Name.Left(4) == L"zNRT" )  {

		return TRUE;	// disallow manual NRT/TMS tag names
		}

	fOk = ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, Name, Addr, FALSE, FALSE);

	if( fOk ) {

		CError Error(FALSE);

		if( AddNewTag(Error, Addr, Name) ) {

			GetDlgItem(ETNAME).SetWindowTextW(Name);

			UpdateTagList(Addr, Name);
			}

		else {
			AfxAssert(FALSE);

			return FALSE;
			}
		}
	else {
		Error.Show(ThisObject);
		}

	ShowAddress(Addr);

	return TRUE;
	}

void CBSAPFULLMasterTagsDialog::UpdateTagList(CAddress Addr, CString Name) {

	CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);
			
	LoadTag(Tree, Name, Addr);
			
	Tree.Expand(m_hRoot, TVE_EXPAND);
			
	Tree.SetFocus();
			
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
	}

BOOL CBSAPFULLMasterTagsDialog::OnCreateBOOLAck(UINT uID) {

	return AddAckSpace(TRUE);
	}

BOOL CBSAPFULLMasterTagsDialog::OnCreateREALAck(UINT uID) {
	
	return AddAckSpace(FALSE);
	}

BOOL CBSAPFULLMasterTagsDialog::AddAckSpace(BOOL fBool) {

	CAddress Addr;

	Addr.a.m_Table  = fBool ? SPBOOLACK : SPREALACK;
	Addr.a.m_Offset = 0xFFFE;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	CString s = fBool ? L"BOOL_ALARM_ACK" : L"REAL_ALARM_ACK";

	CError Error;

	if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, s, Addr, FALSE, TRUE) ) {	// see if tag exists

		((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, s, Addr, FALSE, FALSE);	// add new tag

		if( AddNewTag(Error, Addr, s) ) {

			UpdateTagList(Addr, s);
			}

		else {
			AfxAssert(FALSE);

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CBSAPFULLMasterTagsDialog::AddNewTag(CError Error, CAddress Addr, CString Name ) {

	BOOL fParse	= ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ParseAddress(Error, Addr, Name);

	UINT uTable	= Addr.a.m_Table;

	if( fParse ) {

		BOOL fAck = Addr.a.m_Table == SPBOOLACK || Addr.a.m_Table == SPREALACK;

		DSetName  (Name);
		DSetOffs  (Addr.a.m_Offset);
		DSetLNum  (0);

		UINT uLev = fAck ? GetLevel() : uTable % 10;
		UINT uDev = GetDeviceAddr();
		DSetLevel((DWORD)uLev);
		DSetDevAdd((DWORD)uDev);
		DSetTable((DWORD)Addr.a.m_Table);
		DSetGlobal(fAck ? 0 : (DWORD)GetGlobal());
		DSetDevNum(fAck ? 0 : MakeDevNum(uLev, uDev));
		DSetType(Addr.a.m_Type);
		DSetMSD(0);

		DAppendItem(Addr.m_Ref);

		if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ParseAddress(Error, Addr, Name) ) {

			GetDlgItem(ETNAME).SetWindowTextW(Name);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBSAPFULLMasterTagsDialog::OnUpdateAddress(UINT uID)
{
	CString sWork = TEXT("You may be making a change to the configuration of a tag, such as\n")
			TEXT("a device number change, that may cause errors in the database\n")
			TEXT("If you continue, use \"File\\Utilities\\Recompile Database\" to check for errors\n\n")
			TEXT("Do you wish to continue?");

	if( m_pViewWnd->YesNo(sWork) == IDNO ) {

		return TRUE;
		}

	ClearHolding();

	UINT uSize = DGetArrayCount();

	if( !uSize ) return TRUE;

	if( m_dwSelect ) {

		CAddress Addr	= (CAddress &)m_dwSelect;

		UINT uIndex	= DGetIndx((UINT)Addr.m_Ref);

		CString sName	= DGetName(uIndex);			// Array value

		UINT dType	= GetCBType();				// Display value

		if( IsNRTTable(Addr.a.m_Table ) ) {

			CString s = sName.Left(4);

			if( s == L"zTMS" || s == L"zNRT" ) {

				return FALSE;
				}

			else { // allow replacement of NRT/TMS error
				Addr.a.m_Table = dType == addrBitAsBit ? SPBIT0 : SPREAL0;
				}
			}

		if( Addr.a.m_Table == SPBOOLACK || Addr.a.m_Table == SPREALACK ) return TRUE;

		CString dName	= GetDlgItem(ETNAME).GetWindowTextW();	// Display value

		if( !dName[0] || dName[0] == '*' ) return FALSE;

		UINT uLevel	= DGetLevel(uIndex);			// Array value
		UINT dLevel	= GetBoxPosition(CBLEVEL);		// Display value

		UINT uDevAd	= DGetDevAdd(uIndex);			// Array value
		UINT dDevAd	= (UINT)GetDeviceAddr();		// Display value

		UINT uType	= DGetType(uIndex);			// Array value

		UINT uTable	= Addr.a.m_Table;
		UINT dTable	= (uTable / 10 * 10) + uLevel;

		UINT dOffset	= Addr.a.m_Offset;			// current offset value

		UINT uGlobal	= DGetGlobalAddr(FALSE);		// Array value
		UINT dGlobal	= GetGlobal();

		if(	sName == dName && uLevel == dLevel && uDevAd == dDevAd &&
			uType == dType && uTable == dTable && uGlobal == dGlobal
			) {
				return TRUE;				// no change
			}

		if( !dName[0] || dName.Left(4) == L"None" ) {

			dName = sName;
			}

		UINT uFind = sName.Find(L"#L");

		if( uFind == NOTHING ) return TRUE;	// invalid stored format

		uFind = dName.Find(L"#L");

		CString PostFix;
		PostFix.Printf( L"#L%1.1dD%2.2X#", dLevel, dDevAd);

		dName = (uFind < NOTHING ? dName.Left(uFind) : dName) + PostFix;

		if( sName != dName ) {	// Name string has changed

			UINT x = 0;

			while( x < uSize ) {

				if( DGetName(x) == dName ) {		// duplicated name

					uFind = dName.Find(L"#L");

					dName = dName.Left(uFind) + L"_1" + dName.Mid(uFind);

					x = 0;
					}

				x++;
				}			
			}

		if( uTable != dTable || uType != dType ) {	// change of config dword

			dOffset = FindNextUnusedOffset(dTable);
			}

		DSetName(dName);
		DSetOffs(dOffset);
		DSetMSD(0);
		DSetLNum(0);
		DSetType(dType);
		DSetLevel(dLevel);
		DSetDevAdd(dDevAd);
		DSetGlobal(dGlobal);
		DSetTable(dTable);

		DFreezeIndex(uIndex);

		Addr.a.m_Offset = dOffset;
		Addr.a.m_Table	= dTable;
		Addr.a.m_Type	= dType;
		Addr.a.m_Extra	= DSetDevNum(MakeDevNum(uLevel, dDevAd));

		if( Addr.a.m_Table == SPBOOLACK || Addr.a.m_Table == SPREALACK ) Addr.a.m_Extra = 0;

		DReplace(Addr.m_Ref);

		*m_pAddr = Addr;

		((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
		
		CTreeView &Tree = (CTreeView &)GetDlgItem(1001);

		HTREEITEM h = Tree.GetRoot();

		Tree.DeleteChildren(h);

		Tree.DeleteItem(h);

		LoadNames();

		CError Error;

		((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, dName, Addr, FALSE, FALSE);

		LoadTag(Tree, dName, Addr);

		((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();

		ShowAddress(Addr);
		}

	return TRUE;
	}

// Tree Loading

void CBSAPFULLMasterTagsDialog::LoadNames(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_EDITLABELS
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SendMessage(TVM_SETUNICODEFORMAT, 1);
	
	LoadNames(Tree);

	Tree.SetFocus();
	}

void CBSAPFULLMasterTagsDialog::LoadNames(CTreeView &Tree)
{
	LoadRoot(Tree);
	
	LoadTags(Tree);

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CBSAPFULLMasterTagsDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->GetDeviceName());

	Node.SetParam(FALSE);

	Node.SetImages(3);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CBSAPFULLMasterTagsDialog::LoadTags(CTreeView &Tree)
{
	CTagDataEArray List;
	
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ListTags(List);

	m_fLoad = TRUE;

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CTagDataE const &Data = List[n];

		LoadTag(Tree, Data.m_Name, (CAddress &) Data.m_Addr);
		}

	m_fLoad = FALSE;
	}

void CBSAPFULLMasterTagsDialog::LoadTag(CTreeView &Tree, CString Name, CAddress Addr)
{
	if( Name[0] == '*' ) return;

	CTreeViewItem Node;

	Node.SetText(Name);

	Node.SetParam(Addr.m_Ref);

	Node.SetImages(0x34);

	HTREEITEM hNode = Tree.InsertItem(m_hRoot, NULL, Node);

	if( m_fLoad ) {

		CAddress Test = *m_pAddr;

		if( Addr.m_Ref == Test.m_Ref ) {	

			Tree.SelectItem(hNode, TVGN_CARET);
			}
		}
	else
		Tree.SelectItem(hNode, TVGN_CARET);
	}

// Implementation

void CBSAPFULLMasterTagsDialog::ShowAddress(CAddress Addr)
{
	DoDlgEnables();

	UINT uCnt = DGetArrayCount();

	if( (BOOL)uCnt ) {

		if( Addr.m_Ref ) {

			UINT uIndex  = DGetIndx(Addr.m_Ref);

			if( uIndex == NOTHING ) {

				return;
				}

			UINT uTable = Addr.a.m_Table;

			ShowIndx(Addr.a.m_Offset);

			BOOL fNRT = IsNRTTable(uTable);

			ShowCBType(uIndex, fNRT, IsStrTable(uTable));

			ShowName(uIndex);

			ShowMSD(uIndex);

			ShowLNum(uIndex);

			ShowLevel(uIndex);

			ShowDevAdd(uIndex);

			ShowGlobal(uIndex);

			DoDlgEnables();

			return;
			}
		}

	GetDlgItem(ETNAME).SetWindowText(L"None");

	GetDlgItem(DINDEX).SetWindowTextW(L"--");

	GetDlgItem(MSDVAL).SetWindowTextW(L"0");

	GetDlgItem(LISTNM).SetWindowTextW(L"0");

	GetDlgItem(PBREFRSH).EnableWindow(FALSE);

	CString s;

	BYTE b = LOBYTE(DGetLocalAddr());

	if( b > 127 || !b ) b = 1;

	s.Printf( L"%d", b);

	GetDlgItem(ETSLVNUM).SetWindowTextW(s);

	WORD w = LOWORD(DGetGlobalAddr(TRUE));

	s.Printf( L"%d", w);

	GetDlgItem(ETSLVGNM).SetWindowTextW(s);

	SetBoxPosition(CBLEVEL, DGetMLevel() + 1);

	DoDlgEnables();
	}

void CBSAPFULLMasterTagsDialog::ShowName(UINT uIndex)
	{
	CString s = DGetName(uIndex);

	GetDlgItem(ETNAME).SetWindowTextW(s);
	}

void CBSAPFULLMasterTagsDialog::ShowIndx(UINT uIndex)
	{
	CString s;

	s.Printf( L"%d", uIndex );

	GetDlgItem(DINDEX).SetWindowTextW(s);
	}

void CBSAPFULLMasterTagsDialog::ShowCBType(UINT uIndex, BOOL fNRT, BOOL fStr)
{
	SetCBType(fNRT ? addrLongAsLong : (UINT)(DGetType(uIndex)), fNRT, fStr);
	}

void CBSAPFULLMasterTagsDialog::SetCBType(UINT uVal, BOOL fNRT, BOOL fStr)
{
	switch( uVal ) {

		case addrBitAsBit:	uVal = 1; break;
		case addrLongAsLong:	uVal = fNRT ? 2 : fStr ? 6 : 5; break;
		case addrRealAsReal:	uVal = 0; break;
		case addrByteAsByte:	uVal = 3; break;
		case addrWordAsWord:	uVal = 4; break;
		}

	SetBoxPosition(CBTYPE, uVal);
	}

void CBSAPFULLMasterTagsDialog::ShowMSD(UINT uIndex)
	{
	ShowPVal(0, MSDVAL);
	}

void CBSAPFULLMasterTagsDialog::ShowLNum(UINT uIndex)
	{
	ShowPVal(0, LISTNM);
	}

void CBSAPFULLMasterTagsDialog::ShowLevel(UINT uIndex)
	{
	UINT uLevel = DGetLevel(uIndex);

	SetBoxPosition(CBLEVEL, uLevel);
	}

void CBSAPFULLMasterTagsDialog::ShowDevAdd(UINT uIndex) {

	ShowPVal(DGetDevAdd(uIndex), ETSLVNUM);
	}

void CBSAPFULLMasterTagsDialog::ShowGlobal(UINT uIndex) {

	ShowPVal(DGetGlobalAddr(FALSE), FALSE);
	}

void CBSAPFULLMasterTagsDialog::ShowPVal(DWORD dVal, UINT uID)
	{
	CString s;

	s.Printf(L"%d", dVal);

	GetDlgItem(uID).SetWindowTextW(s);
	}

BOOL CBSAPFULLMasterTagsDialog::DeleteTag(void)
{
	BOOL fOk = FALSE;

	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		if( Addr.a.m_Table == SPBOOLACK || Addr.a.m_Table == SPREALACK) return FALSE;

		if( IsNRTTable(Addr.a.m_Table) ) {

			UINT uIndex = GetSelect(Addr.a.m_Table, Addr.a.m_Offset);

			CString s   = DGetName(uIndex);

			s = s.Left(4);

			if( s == L"zNRT" || s == L"zTMS" ) {

				return FALSE;
				}
			}

		CString  Name;

		((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ExpandAddress(Name, Addr);

		if( !m_fUpdate ) {

			CString Text  = CPrintf("Do you really want to delete %s ?", Name);

			m_fUpdate = NoYes(Text) == IDYES;
			}

		if( m_fUpdate ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			Tree.SetRedraw(FALSE);

			HTREEITEM hPrev = m_hSelect;

			Tree.MoveSelection();

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->DeleteTag(Addr, TRUE);

			Tree.DeleteItem(hPrev);

			Tree.SetRedraw(TRUE);

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();

			fOk = TRUE;
			}
		}

	return fOk;
	}

BOOL CBSAPFULLMasterTagsDialog::RenameTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString Name;

		if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ExpandAddress(Name, Addr) ) {

			CString Title   = L"Rename Variable";
			
			CString Group   = L"Variable Name";

			CString TagName = Name;

			CStringDialog Dlg(Title, Group, TagName);

			if( Dlg.Execute(ThisObject) ) {

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);
				
				CString    Name = Dlg.GetData();			

				if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->RenameTag(Name, Addr) ) {

					Tree.SetRedraw(FALSE);

					HTREEITEM hPrev = m_hSelect;

					Tree.MoveSelection();

					Tree.DeleteItem(hPrev);

					LoadTag(Tree, Name, Addr);

					Tree.SetRedraw(TRUE);

					((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
					
					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

BOOL CBSAPFULLMasterTagsDialog::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

DWORD CBSAPFULLMasterTagsDialog::MakeAddrFromSelects(UINT uOffset)
{
	CAddress Addr;

	UINT uType	= GetBoxPosition(CBTYPE);

	if( uType == 2 || uType == 12 ) return 0xFFFF;	// can't create NRT Tags from dropdown

	UINT uLevel	= GetBoxPosition(CBLEVEL);

	UINT uDev	= GetDeviceAddr();

	DWORD dDev	= DSetDevNum(0x10000 + MakeDevNum(uLevel, uDev));	// get/set device number in address list

	if( dDev == NOTHING ) {

		return NOTHING;		// device address table is full
		}

	Addr.a.m_Extra	= LOBYTE(LOWORD(dDev));

	UINT dAdj	= 1;

	switch( uType ) {

		case 0:
		case 10:
			Addr.a.m_Table	= uType == 10 ? SPREALINT : SPREAL0;
			Addr.a.m_Type	= addrRealAsReal;
			break;

		case 1:
		case 11:
			Addr.a.m_Table	= uType == 11 ? SPBITINT : SPBIT0;
			Addr.a.m_Type	= addrBitAsBit;
			break;

		case 2:
		case 12:
			return 0xFFFF;	// can't create NRT Tags

		case 3:
		case 13:
			Addr.a.m_Table	= uType == 13 ? SPBYTEINT : SPBYTE0;
			Addr.a.m_Type	= addrByteAsByte;
			break;

		case 4:
		case 14:
			Addr.a.m_Table	= uType == 14 ? SPWORDINT : SPWORD0;
			Addr.a.m_Type	= addrWordAsWord;
			break;

		case 5:
		case 15:
			Addr.a.m_Table	= uType == 15 ? SPDWORDINT : SPDWORD0;
			Addr.a.m_Type	= addrLongAsLong;
			break;

		case 6:
		case 16:
			Addr.a.m_Table	= uType == 16 ? SPSTRINGINT : SPSTRING0;
			Addr.a.m_Type	= addrLongAsLong;
			dAdj		= MAXDWORDLEN;
			break;
		}

	Addr.a.m_Table += uLevel;

	if( uOffset == NOTHING ) {	// creating tag

		Addr.a.m_Offset = FindNextUnusedOffset(Addr.a.m_Table);
		}

	return Addr.m_Ref;
	}

UINT CBSAPFULLMasterTagsDialog::GetSelect(UINT uTable, UINT uOffset) {

	return IsNRTTable(uTable) ? GetNRTFromAddr(uOffset) : GetSelectFromAddr(uTable, uOffset);
	}

UINT CBSAPFULLMasterTagsDialog::GetSelectFromAddr(UINT uTable, UINT uOffset) {

	UINT uMax = DGetArrayCount();

	if( uMax ) {

		for( UINT n = 0; n < uMax; n++ ) {

			if( OffsetUsed(uTable, uOffset, n) ) {

				return n;
				}
			}
		}

	return NOTHING;
	}

UINT CBSAPFULLMasterTagsDialog::FindNextUnusedOffset(UINT uTable) {

	BOOL fStr = IsStrTable(uTable);

	UINT uAdj = fStr ? MAXDWORDLEN : 1;

	UINT uOff = fStr ? 0 : 1;

	if( IsRealTable(uTable) || IsBitTable(uTable) ) { 

		uOff += (uTable * 1000);
		}

	if( IsIntTable(uTable) ) {

		uOff += (uTable * 500);
		}

	UINT n    = 0;

	UINT uMax = DGetArrayCount();

	while( n < uMax ) {

		if( OffsetUsed(uTable, uOff, n) ) {

			n = 0;

			uOff += uAdj;

			continue;
			}

		n++;
		}

	return uOff;
	}

BOOL CBSAPFULLMasterTagsDialog::OffsetUsed(UINT uTable, UINT uOffset, UINT uSelect) {

	return uTable == DGetTable(uSelect) && uOffset == DGetOffs(uSelect);
	}

UINT CBSAPFULLMasterTagsDialog::GetNRTFromAddr(UINT uOffset) {

	BYTE bAddr = GetDeviceAddr();

	UINT n = DGetArrayCount();

	BOOL fNew = uOffset == NOTHING;

	if( fNew && !bAddr ) {	// NRT 0 invalid

		return NOTHING;		
		}

	UINT uMax  = 0;

	while( n ) {	// find max offset or check entire list for duplicate

		UINT x     = n-1;

		UINT uOffs = DGetOffs(x);

		if( fNew ) {

			if( uOffs & 0x8000 ) {

				uMax = Max( uMax, LOBYTE(uOffs));
				}
			}

		else {
			if( uOffset == uOffs ) {

				return x;
				}
			}

		n--;
		}

	return fNew ? 0x8000 | (WORD)(bAddr << 8) | (uMax + 1) : 1;	// always start with 1
	}

BOOL CBSAPFULLMasterTagsDialog::IsNRTTable(UINT uTable) {

	return uTable >= SPNRT0 && uTable <= SPNRT6;
	}

BOOL CBSAPFULLMasterTagsDialog::IsBitTable(UINT uTable)
{
	return uTable >= SPBIT0 && uTable <= SPBIT6;
	}

BOOL CBSAPFULLMasterTagsDialog::IsByteTable(UINT uTable)
{
	return uTable >= SPBYTE0 && uTable <= SPBYTE6;
	}

BOOL CBSAPFULLMasterTagsDialog::IsWordTable(UINT uTable)
{
	return uTable >= SPWORD0 && uTable <= SPWORD6;
	}

BOOL CBSAPFULLMasterTagsDialog::IsDwordTable(UINT uTable)
{
	return uTable >= SPDWORD0 && uTable <= SPDWORD6;
	}

BOOL CBSAPFULLMasterTagsDialog::IsIntTable(UINT uTable)
{
	return IsByteTable(uTable) || IsWordTable(uTable) || IsDwordTable(uTable);
	}

BOOL CBSAPFULLMasterTagsDialog::IsRealTable(UINT uTable)
{
	return uTable >= SPREAL0 && uTable <= SPREAL6;
	}

BOOL CBSAPFULLMasterTagsDialog::IsStrTable(UINT uTable)
{
	return uTable >= SPSTRING0 && uTable <= SPSTRING6;
	}

// Dialog Box Data Handling
void CBSAPFULLMasterTagsDialog::LoadCBAll(void)
{
	LoadCBType();
	LoadCBLevel();
	LoadCBArray();
	}

void CBSAPFULLMasterTagsDialog::LoadCBType(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(CBTYPE);

	ClearBox(CBTYPE);

	Box.AddString(L"REAL");
	Box.AddString(L"BIT");
	Box.AddString(L"NRT");
	Box.AddString(L"BYTE");
	Box.AddString(L"WORD");
	Box.AddString(L"DWORD");
	Box.AddString(L"STRING");

	SetBoxPosition(CBTYPE, 0);
	}

void CBSAPFULLMasterTagsDialog::LoadCBLevel(void) {

	CComboBox & Box	= (CComboBox &)GetDlgItem(CBLEVEL);

	ClearBox(CBLEVEL);

	CString s;

	for( UINT i = 0; i < 7; i++ ) {

		s.Printf( L"%d", i );

		Box.AddString(s);
		}

	SetBoxPosition(CBLEVEL, 0);
	}

void CBSAPFULLMasterTagsDialog::LoadCBArray(void) {

	CComboBox & Box	= (CComboBox &)GetDlgItem(CBCOLUMN);

	ClearBox(CBCOLUMN);

	Box.AddString(L"--");

	SetBoxPosition(CBCOLUMN, 0);
	}

void CBSAPFULLMasterTagsDialog::SetBoxPosition(UINT uID, UINT uPos)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CBSAPFULLMasterTagsDialog::GetBoxPosition(UINT uID)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CBSAPFULLMasterTagsDialog::ClearBox(CComboBox & ccb)
{
	UINT uCount = ccb.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		ccb.DeleteString(0);
		}
	}

void CBSAPFULLMasterTagsDialog::ClearBox(UINT uID)
{
	ClearBox((CComboBox &)GetDlgItem(uID));
	}

UINT CBSAPFULLMasterTagsDialog::GetCBPos(void)
{
	return GetBoxPosition(CBTYPE);
	}

UINT CBSAPFULLMasterTagsDialog::GetCBType(void)
{	
	switch( GetBoxPosition(CBTYPE) ) {

		case 1: return addrBitAsBit;
		case 2: return addrLongAsLong;
		case 3: return addrByteAsByte;
		case 4: return addrWordAsWord;
		case 5: return addrLongAsLong;
		case 6: return addrLongAsLong;
		}

	return addrRealAsReal;
	}

UINT CBSAPFULLMasterTagsDialog::GetETMSD(void)
	{
	return tatoi(GetDlgItem(MSDVAL).GetWindowTextW());
	}

UINT CBSAPFULLMasterTagsDialog::GetETLNum(void)
	{
	return min(63, tatoi(GetDlgItem(LISTNM).GetWindowText()));
	}

UINT CBSAPFULLMasterTagsDialog::GetETIndex(void)
	{
	CString s = GetDlgItem(DINDEX).GetWindowTextW();

	if( s.Left(3) == L"Dev" ) {

		UINT uFind = s.Find('_');

		if( uFind < NOTHING ) {

			return 0x8000 + tatoi(s.Mid(uFind + 1));
			}
		}

	return tatoi(s);
	}

BYTE CBSAPFULLMasterTagsDialog::GetDeviceAddr(void) {

	return LOBYTE(tatoi(GetDlgItem(ETSLVNUM).GetWindowTextW()));
	}

WORD CBSAPFULLMasterTagsDialog::GetGlobal(void) {

	return LOWORD(tatoi(GetDlgItem(ETSLVGNM).GetWindowTextW()));
	}

BYTE CBSAPFULLMasterTagsDialog::GetLevel(void) {

	return LOBYTE(tatoi(GetDlgItem(CBLEVEL).GetWindowTextW()));
	}

DWORD CBSAPFULLMasterTagsDialog::GetArrayValue(void) {

	return ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayValue;
	}

void CBSAPFULLMasterTagsDialog::DoDlgEnables(void) {

	BOOL fEnable	= !m_fSelect;
	BOOL fNRT	= GetBoxPosition(CBTYPE) == 2;

	GetDlgItem(DINDEX).EnableWindow(TRUE);
	GetDlgItem(INDEXTXT).EnableWindow(TRUE);

	GetDlgItem(CBTYPE).EnableWindow(fEnable);
	GetDlgItem(PBBOOL).EnableWindow(fEnable);
	GetDlgItem(PBREAL).EnableWindow(fEnable);

	GetDlgItem(ETNAME).EnableWindow(fEnable && !fNRT);
	GetDlgItem(PBCREATE).EnableWindow(fEnable && !fNRT);
	GetDlgItem(PBREFRSH).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVTXT).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVNUM).EnableWindow(fEnable && !fNRT);
	GetDlgItem(CBLEVEL).EnableWindow(fEnable && !fNRT);
	GetDlgItem(LEVELTXT).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVGTX).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVGNM).EnableWindow(fEnable && !fNRT);


	GetDlgItem(LISTNM).EnableWindow(FALSE);
	GetDlgItem(LNUMTXT).EnableWindow(FALSE);
	GetDlgItem(LISTPOS).EnableWindow(FALSE);
	GetDlgItem(LISTPVAL).EnableWindow(FALSE);
	GetDlgItem(MSDVAL).EnableWindow(FALSE);
	GetDlgItem(MSDTXT).EnableWindow(FALSE);
	GetDlgItem(ARRAYTXT).EnableWindow(FALSE);
	GetDlgItem(ETARRAY).EnableWindow(FALSE);
	GetDlgItem(CBCOLUMN).EnableWindow(FALSE);
	}

WORD CBSAPFULLMasterTagsDialog::MakeDevNum(UINT uLevel, UINT uDevice) {

	return (WORD)(LOBYTE(uLevel) << 8) + LOBYTE(uDevice);
	}

// Device Config Array Access
DWORD CBSAPFULLMasterTagsDialog::AccessArrayVal(DWORD dSel, DWORD dVal)
	{
	SetArrayItems(dSel, dVal, L"");

	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->AccessArrayValue();

	return ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayValue;
	}

CString CBSAPFULLMasterTagsDialog::AccessArrayStr(DWORD dSel, DWORD dVal, CString sStr)
	{
	SetArrayItems(dSel, dVal, sStr);

	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->AccessArrayValue();

	return ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayString;
	}

void CBSAPFULLMasterTagsDialog::SetArrayItems(DWORD dSel, DWORD dVal, CString sStr)
	{
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArraySelect = dSel;
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayValue  = dVal;
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayString = sStr;
	}

void CBSAPFULLMasterTagsDialog::DSetItem(DWORD dVal)
	{
	AccessArrayVal(WARRITM, dVal);
	}

void CBSAPFULLMasterTagsDialog::DAppendItem(DWORD dVal)
	{
	AccessArrayVal(FAPPEND, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetName(CString sStr)
	{
	AccessArrayStr(WARRNAM, 0, sStr);
	}

void CBSAPFULLMasterTagsDialog::DSetMSD(DWORD dVal)
	{
	AccessArrayVal(WARRMSD, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetType(DWORD dVal)
	{
	AccessArrayVal(WARRTYP, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetLNum(DWORD dVal)
	{
	AccessArrayVal(WARRLNM, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetOffs(DWORD dVal)
	{
	AccessArrayVal(WARROFF, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetLevel(DWORD dVal) {

	AccessArrayVal(WARRLVL, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetDevAdd(DWORD dVal) {

	AccessArrayVal(WARRDVA, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetGlobal(DWORD dVal) {

	AccessArrayVal(WARRGLB, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetTable(DWORD dVal) {

	AccessArrayVal(WARRTAB, dVal);
	}

void CBSAPFULLMasterTagsDialog::DSetArrayStatus(void) {

	GetDlgItem(ETARRAY).SetWindowTextW(L"--");

	SetBoxPosition(CBCOLUMN, 0);

	AccessArrayVal(WCASTAT, 0);
	}

void CBSAPFULLMasterTagsDialog::DReplace(UINT uItem)
	{
	AccessArrayVal(FREPLCE, uItem);
	}

void CBSAPFULLMasterTagsDialog::ClearHolding(void)
	{
	AccessArrayVal(FCLEAR, 0);
	}

void CBSAPFULLMasterTagsDialog::DFreezeIndex(UINT uIndex) {

	AccessArrayVal(WFREEZE, uIndex);
	}

DWORD CBSAPFULLMasterTagsDialog::DSetDevNum(DWORD dValue) {	// returns the index into the DevNumList

	return (DWORD)(AccessArrayVal(WARRDNM, (DWORD)dValue) % 15);
	}

CString CBSAPFULLMasterTagsDialog::DGetName(UINT uItem)
	{
	return AccessArrayStr(RARRNAM, uItem, L""); 
	}

DWORD CBSAPFULLMasterTagsDialog::DGetIndx(UINT uItem)
	{
	return AccessArrayVal(RARRITM, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetMSD(UINT uItem)
	{
	return AccessArrayVal(RARRMSD, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetType(UINT uItem)
	{
	return AccessArrayVal(RARRTYP, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetLNum(UINT uItem)
	{
	return AccessArrayVal(RARRLNM, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetOffs(UINT uItem)
	{
	return AccessArrayVal(RARROFF, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetLevel(UINT uItem)
	{
	return AccessArrayVal(RARRLVL, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetDevAdd(UINT uItem)
	{
	return AccessArrayVal(RARRDVA, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetTable(UINT uItem)
	{
	return AccessArrayVal(RARRTAB, uItem);
	}

DWORD CBSAPFULLMasterTagsDialog::DGetLocalAddr(void) {

	AccessArrayVal(GETALOC, 0);

	return LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLMasterTagsDialog::DGetGlobalAddr(BOOL fOfDevice) {

	AccessArrayVal(fOfDevice ? RARRDGL : RARRGLB, 0);

	return LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLMasterTagsDialog::DGetDevNumExtra(UINT uIndex) {

	UINT uLev = DGetLevel(uIndex);
	UINT uDev = DGetDevAdd(uIndex);

	return DSetDevNum(MakeDevNum(uLev, uDev));
	}

DWORD CBSAPFULLMasterTagsDialog::DGetDevNumFromExtra(UINT uExtra) {

	return AccessArrayVal(RARRDNM, (DWORD)(uExtra % 15));
	}

DWORD CBSAPFULLMasterTagsDialog::DGetMLevel(void) {

	AccessArrayVal(RARRMLV, 0);

	return (DWORD)LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLMasterTagsDialog::DGetPath(void) {

	AccessArrayVal(RARRPAT, 0);

	return (DWORD)LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLMasterTagsDialog::DGetArrayCount(void)
	{
	return AccessArrayVal(RARRCNT, 0);
	}

DWORD CBSAPFULLMasterTagsDialog::DFindUnUsedMSD(void)
	{
	return AccessArrayVal(FNXTMSD, 0);
	}

DWORD CBSAPFULLMasterTagsDialog::DCheckMSD(DWORD dItem)
	{
	return AccessArrayVal(CHKMSD, dItem);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBSAPFULLSlaveTagsDialog, CStdAddrDialog);

// Constructor

CBSAPFULLSlaveTagsDialog::CBSAPFULLSlaveTagsDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fSelect	= TRUE;

	m_fUpdate	= FALSE;
	
	m_Images.Create(L"CommsManager", 16, 0, IMAGE_BITMAP, 0);

	SetName(fPart ? L"PartBSAPFULLDlg" : L"FullBSAPFULLDlg");
	}

// Destructor

CBSAPFULLSlaveTagsDialog::~CBSAPFULLSlaveTagsDialog(void)
{
//	afxThread->SetStatusText("");
	}

// Initialisation

void CBSAPFULLSlaveTagsDialog::SetCaption(CString const &Text)
{
	m_Caption = Text;
	}

void CBSAPFULLSlaveTagsDialog::SetSelect(BOOL fSelect)
{
	m_fSelect = fSelect;
	}

// Message Map

AfxMessageMap(CBSAPFULLSlaveTagsDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(1001, TVN_KEYDOWN,    OnTreeKeyDown   )

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(PBCREATE, OnCreateTag)
	AfxDispatchCommand(PBREFRSH, OnUpdateAddress)
	AfxDispatchNotify(CBTYPE,   CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(CBLEVEL,  CBN_SELCHANGE, OnComboChange)
	AfxDispatchNotify(CBCOLUMN, CBN_SELCHANGE, OnComboChange)

	AfxMessageEnd(CBSAPFULLSlaveTagsDialog)
	};

// Message Handlers

BOOL CBSAPFULLSlaveTagsDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetWindowText(m_Caption);

	GetDlgItem(IDCANCEL).EnableWindow(m_fSelect);

	GetDlgItem(IDOK).SetWindowText(m_fSelect ? L"OK" : L"Close");

	if( !m_fPart ) {

		LoadCBAll();

		LoadNames();
		}

	CAddress A = *m_pAddr;

	ShowArrayStatus((BOOL)DGetArrayCount() && (BOOL)A.m_Ref ? DGetIndx((UINT)A.m_Ref) : NOTHING);

	ShowAddress(*m_pAddr);

	return FALSE;
	}

// Notification Handlers

void CBSAPFULLSlaveTagsDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_hSelect  = Info.itemNew.hItem;

	m_dwSelect = Info.itemNew.lParam;

	CAddress Addr   = *m_pAddr;

	if( m_dwSelect == Addr.m_Ref ) {
		
		Addr = *m_pAddr;
		}
	else
		Addr = (CAddress &) m_dwSelect;

	if( IsNRTTable(Addr.a.m_Table) && !m_fSelect ) {

		ShowAddress(Addr);

		CString s;

		s.Printf(L"%d", Addr.a.m_Offset);

		GetDlgItem(DINDEX).SetWindowTextW(s);

		return;
		}

	ClearHolding();

	UINT uTypeSel = GetCBType();

	BOOL fNRT     = FALSE;

	UINT u = NOTHING;

	if( Addr.m_Ref ) {

		uTypeSel = Addr.a.m_Type;

		fNRT     = IsNRTTable(Addr.a.m_Table);

		u = (BOOL)DGetArrayCount() ? DGetIndx((UINT)Addr.m_Ref) : 0;
		}

	ShowArrayStatus(u);

	SetCBType(uTypeSel, fNRT);

	ShowAddress(Addr);
	}

BOOL CBSAPFULLSlaveTagsDialog::OnTreeKeyDown(UINT uID, NMTVKEYDOWN &Info)
{
	if( Info.wVKey == VK_DELETE ) {
		
		if( IsDown(VK_SHIFT) ) {

			return DeleteTag();
			}
		}

	return FALSE;
	}

void CBSAPFULLSlaveTagsDialog::OnComboChange(UINT uID, CWnd &Wnd) {

	UINT n = GetBoxPosition(uID);

	CString s;

	switch( uID ) {

		case CBCOLUMN:

			if( (BOOL)n ) {		// selected # of columns

				SetDlgFocus(ETARRAY);
				}

			break;

		case CBLEVEL:

			UINT uMLevel;

			uMLevel = DGetMLevel();

			if( n <= uMLevel ) {

				SetBoxPosition(CBLEVEL, uMLevel + 1);
				}
			break;

		case CBTYPE:
			GetDlgItem(MSDVAL).SetWindowTextW(L"1");
			GetDlgItem(LISTNM).SetWindowTextW(L"0");
			GetDlgItem(LISTPVAL).SetWindowTextW(L"0");
			GetDlgItem(ETARRAY).SetWindowTextW("0");
			GetDlgItem(ETARRAY).EnableWindow(FALSE);
			GetDlgItem(ARRAYTXT).EnableWindow(FALSE);
			SetBoxPosition(CBCOLUMN, 0);
			break;
		}

	DoDlgEnables();
	}

// Command Handlers

BOOL CBSAPFULLSlaveTagsDialog::OnOkay(UINT uID)
{
	if( m_fSelect ) {
	
		CAddress Addr;

		CError Error(TRUE);

		BOOL fOk = FALSE;

		Addr.m_Ref   = m_dwSelect;

		UINT uSelect = GetSelect(Addr.a.m_Table, Addr.a.m_Offset);

		if( !m_fPart ) {

			if( uSelect >= DGetArrayCount() ) {

				m_pAddr->m_Ref = 0;

				EndDialog(TRUE);

				return TRUE;
				}
			}

		else {
			Addr = *m_pAddr;
			}

		CString Text;

		if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ExpandAddress(Text, Addr) ) {

			fOk = m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text);
			}

		if( fOk ) {

			*m_pAddr = Addr;

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
			
			EndDialog(TRUE);
			
			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus(2002);

		return TRUE;
		}

	EndDialog(TRUE);
	
	return TRUE;
	}

BOOL CBSAPFULLSlaveTagsDialog::OnCreateTag(UINT uID)
{
	CAddress Addr;

	ClearHolding();

	UINT uMSD  = GetETMSD();

	if( !(uMSD && uMSD < 0x1000) ) {

		CError Error;

		Error.Set( "1 - 4095", 0);

		return FALSE;
		}

	Addr.m_Ref = MakeAddrFromSelects(NOTHING);

	if( Addr.a.m_Offset == 0xFFFF ) {

		return TRUE;
		}

	CString Name = GetDlgItem(ETNAME).GetWindowTextW();

	BOOL fOk  = FALSE;

	CError Error(TRUE);

	CString PostFix;

	PostFix.Printf( L"#L%1.1dD%2.2X#", GetLevel(), GetDeviceAddr());

	if( Name == L"None" || Name.Left(4) == L"zTMS" || Name.Left(4) == L"zNRT" ) {

		Name = ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateName(Addr, PostFix);
		}

	else {
		UINT uLen = Name.GetLength();

		if( uLen > 7 && Name[uLen-1] == '#' ) {

			Name = Name.Left(uLen - 7);

			uLen = Name.GetLength();

			if( uLen > 5 ) {

				Name = Name.Left(uLen - 1);
				}
			}
		}

	CStringDialog Dlg(L"Create Tag", L"Tag Name", Name);

	if( !IsDown(VK_SHIFT) ) {
		
		if( Dlg.Execute(ThisObject) ) {

			Name = Dlg.GetData();

			Name.Printf( L"%s%s", Name, PostFix );
			}
		else
			return TRUE;
		}

	if( Name.Left(4) == L"zTMS" || Name.Left(4) == L"zNRT" ) {

		return TRUE;	// disallow manual NRT/TMS tag names
		}

	fOk = ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, Name, Addr, FALSE, FALSE);

	if( fOk ) {

		CError Error(FALSE);

		if( AddNewTag(Error, Addr, Name) ) {

			CTreeView  &Tree = (CTreeView &) GetDlgItem(1001);
			
			LoadTag(Tree, Name, Addr);
			
			Tree.Expand(m_hRoot, TVE_EXPAND);
			
			Tree.SetFocus();
			
			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
			}

		else {
			AfxAssert(FALSE);

			return FALSE;
			}
		}
	else {
		Error.Show(ThisObject);
		}

	ShowAddress(Addr);

	return TRUE;
	}

BOOL CBSAPFULLSlaveTagsDialog::AddNewTag(CError Error, CAddress Addr, CString Name ) {

	BOOL fParse  = ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ParseAddress(Error, Addr, Name);

	UINT uTable  = Addr.a.m_Table;

	BOOL fNotNRT = uTable < SPNRT0 || uTable > SPNRT6;

	if( fParse ^ !fNotNRT ) {

		DSetName(Name);
		DSetOffs(Addr.a.m_Offset);
		DSetLNum (fNotNRT ? (DWORD)GetETNandP() : 0);
		UINT uLev = uTable % 10;
		UINT uDev = GetDeviceAddr();
		DSetLevel((DWORD)uLev);
		DSetDevAdd((DWORD)uDev);
		DSetTable((DWORD)Addr.a.m_Table);
		DSetGlobal((DWORD)GetGlobal());
		DSetDevNum(MakeDevNum(uLev, uDev));
		DSetArrayStatus(MakeAStatus());

		DSetType(Addr.a.m_Type);

		DWORD dOff = 0;

		if( fNotNRT ) {

			dOff = (DWORD)GetETMSD();

			if( DCheckMSD(dOff) == NOTHING ) {

				dOff = DFindUnUsedMSD();

				if( dOff == NOTHING ) {

					dOff = 0xFFFF;
					}
				}
			}

		DSetMSD(dOff);

		SetBoxPosition(CBCOLUMN, 0);
		GetDlgItem(ETARRAY).SetWindowTextW(L"0");

		DAppendItem(Addr.m_Ref);

		if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ParseAddress(Error, Addr, Name) ) {

			GetDlgItem(ETNAME).SetWindowTextW(Name);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBSAPFULLSlaveTagsDialog::OnUpdateAddress(UINT uID)
{
	ClearHolding();

	UINT uSize = DGetArrayCount();

	if( !uSize ) return TRUE;

	BOOL fNew = FALSE;

	if( m_dwSelect ) {

		CAddress Addr	= (CAddress &)m_dwSelect;

		UINT uTable	= Addr.a.m_Table;

		UINT uIndex	= DGetIndx((UINT)Addr.m_Ref);

		CString sName	= DGetName(uIndex);			// Array value

		if( !sName[0] || sName[0] == '*' ) return FALSE;

		DWORD dType	= GetCBType();				// Display value

		if( IsNRTTable(Addr.a.m_Table ) ) {

			CString s = sName.Left(4);

			if( s == L"zTMS" || s == L"zNRT" ) {
			
				return FALSE;
				}

			else { // allow replacement of NRT/TMS error
				Addr.a.m_Table = dType == addrBitAsBit ? SPBIT0 : SPREAL0;
				}
			}

		UINT uType = GetBoxPosition(CBTYPE);

		switch( uType ) {

			case 0:
				uTable	= SPREAL0;
				break;

			case 1:
				uTable	= SPBIT0;
				break;

			case 3:
				uTable	= SPBYTE0;
				break;

			case 4:
				uTable	= SPWORD0;
				break;

			case 5:
				uTable	= SPDWORD0;
				break;

			case 6:
				uTable	= SPSTRING0;
				break;
			}
				
		UINT uLevel	= GetBoxPosition(CBLEVEL);
		UINT uDAddr	= (UINT)GetDeviceAddr();
		UINT uExtra	= DSetDevNum(0x10000 + MakeDevNum(uLevel, uDAddr));	// temporary Extra

		UINT uTableSel	= (uTable / 10 * 10) + uLevel;
		UINT uOffset	= DGetOffs(uIndex);

		CAddress A;

		A.a.m_Table	= uTableSel;
		A.a.m_Extra	= uExtra;
		A.a.m_Type	= dType;
		A.a.m_Offset	= uOffset;

		if( HIWORD(Addr.m_Ref) != HIWORD(A.m_Ref) ) {	// need to check offset value

			CError Error;

			UINT k = 0;

			while( k < DGetArrayCount() ) {

				if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, L"Do Not Care", A, FALSE, TRUE) ) {	// address does not exist

					uOffset = A.a.m_Offset;
					break;
					}

				A.a.m_Offset++;

				k++;
				}
			}

		Addr.a.m_Table  = uTableSel;
		Addr.a.m_Offset = uOffset;
		Addr.a.m_Type	= dType;
		Addr.a.m_Extra	= uExtra;

		CString PostFix;

		PostFix.Printf( L"#L%1.1dD%2.2X#", uLevel, uDAddr);

		CString sName1	= GetDlgItem(ETNAME).GetWindowTextW();

		if( !sName1[0] || sName1 == L"None" ) {

			sName1 = sName;
			}

		CString s	= sName.Mid(1);		// names can start with "#L"
		UINT uFind	= s.Find(L"#L") + 1;

		s		= sName1.Mid(1);
		UINT uFind1	= s.Find(L"#L") + 1;

		if( sName1 == sName ) {

			s.Printf( L"%s%s", sName.Left(uFind), PostFix);

			if( s != sName ) {

				sName1 = s;

				fNew   = TRUE;
				}
			}

		else {
			if( uFind1 == NOTHING ) {

				s.Printf( L"%s", sName1 );
				}

			else {
				s.Printf( L"%s", sName1.Left(uFind1) );
				}

			sName1.Printf( L"%s%s", s, PostFix );

			fNew = TRUE;
			}

		if( fNew ) {

			for( UINT i = 0; i < uSize; i++ ) {

				if( sName1 == DGetName(i) ) {

					return TRUE;
					}
				}

			sName = sName1;
			}

		DSetName(sName);
		DSetOffs(Addr.a.m_Offset);
		DSetMSD(GetETMSD());
		DSetLNum(GetETNandP());
		DSetType(Addr.a.m_Type);
		DSetLevel(uLevel);
		DSetDevAdd(uDAddr);
		DSetTable(Addr.a.m_Table);
		DSetGlobal(GetGlobal());
		DSetArrayStatus(MakeAStatus());

		DSetDevNum(MakeDevNum(uLevel, uDAddr));

		DFreezeIndex(uIndex);

		DReplace(Addr.m_Ref);

		fNew = TRUE;

		*m_pAddr = Addr;
		
		if( fNew ) {

			CTreeView &Tree = (CTreeView &)GetDlgItem(1001);

			HTREEITEM h = Tree.GetRoot();

			Tree.DeleteChildren(h);

			Tree.DeleteItem(h);

			LoadNames();

			LoadTag(Tree, sName, Addr);

			CError Error;

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->CreateTag(Error, sName, Addr, FALSE, FALSE);

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
			}

		ShowAddress(Addr);
		}

	return TRUE;
	}

// Tree Loading

void CBSAPFULLSlaveTagsDialog::LoadNames(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_NOHSCROLL
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_EDITLABELS
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.SetImageList(TVSIL_NORMAL, m_Images);

	Tree.SendMessage(TVM_SETUNICODEFORMAT, 1);
	
	LoadNames(Tree);

	Tree.SetFocus();
	}

void CBSAPFULLSlaveTagsDialog::LoadNames(CTreeView &Tree)
{
	LoadRoot(Tree);
	
	LoadTags(Tree);

	Tree.Expand(m_hRoot, TVE_EXPAND);
	}

void CBSAPFULLSlaveTagsDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->GetDeviceName());

	Node.SetParam(FALSE);

	Node.SetImages(3);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CBSAPFULLSlaveTagsDialog::LoadTags(CTreeView &Tree)
{
	CTagDataEArray List;
	
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ListTags(List);

	m_fLoad = TRUE;

	for( UINT n = 0; n < List.GetCount(); n ++ ) {

		CTagDataE const &Data = List[n];

		LoadTag(Tree, Data.m_Name, (CAddress &) Data.m_Addr);
		}

	m_fLoad = FALSE;
	}

void CBSAPFULLSlaveTagsDialog::LoadTag(CTreeView &Tree, CString Name, CAddress Addr)
{
	CTreeViewItem Node;

	Node.SetText(Name);

	Node.SetParam(Addr.m_Ref);

	Node.SetImages(0x34);

	HTREEITEM hNode = Tree.InsertItem(m_hRoot, NULL, Node);

	if( m_fLoad ) {

		CAddress Test = *m_pAddr;

		if( Addr.m_Ref == Test.m_Ref ) {	

			Tree.SelectItem(hNode, TVGN_CARET);
			}
		}
	else
		Tree.SelectItem(hNode, TVGN_CARET);
	}

// Implementation

void CBSAPFULLSlaveTagsDialog::ShowAddress(CAddress Addr)
{
	DoDlgEnables();

	UINT uCnt = DGetArrayCount();

	if( (BOOL)uCnt ) {

		if( Addr.m_Ref ) {

			UINT uTable = Addr.a.m_Table;

			UINT uIndex  = GetSelect(uTable, Addr.a.m_Offset);

			if( uIndex == NOTHING ) {

				return;
				}

			ShowIndx(uIndex);

			BOOL fNRT = IsNRTTable(uTable);

			ShowCBType(uIndex, fNRT);

			ShowName(uIndex);

			ShowMSD(uIndex);

			if( fNRT ) {

				GetDlgItem(LISTNM).SetWindowTextW(L"0");
				GetDlgItem(LISTPVAL).SetWindowTextW(L"0");
				GetDlgItem(ETARRAY).SetWindowTextW(L"0");
				GetDlgItem(ETSLVGNM).SetWindowTextW(L"0");
				SetBoxPosition(CBCOLUMN, 0);
				}

			else {
				ShowLNum(uIndex);

				ShowLevel(uIndex);

				ShowDevAdd(uIndex);

				ShowGlobal(uIndex);

				ShowArrayStatus(uIndex);
				}

			DoDlgEnables();

			return;
			}
		}

	GetDlgItem(ETNAME).SetWindowText(L"None");

	GetDlgItem(DINDEX).SetWindowTextW(L"--");

	GetDlgItem(MSDVAL).SetWindowTextW(L"1");

	GetDlgItem(LISTNM).SetWindowTextW(L"0");

	GetDlgItem(LISTPVAL).SetWindowTextW(L"0");

	GetDlgItem(PBREFRSH).EnableWindow(FALSE);

	CString s;

	BYTE b = LOBYTE(DGetLocalAddr());

	if( b > 127 ) b = 1;

	s.Printf( L"%d", b);

	GetDlgItem(ETSLVNUM).SetWindowTextW(s);

	WORD w = b * 100;

	s.Printf( L"%d", w);

	GetDlgItem(ETSLVGNM).SetWindowTextW(s);

	SetBoxPosition(CBLEVEL, 1);

	DoDlgEnables();
	}

void CBSAPFULLSlaveTagsDialog::ShowName(UINT uIndex)
	{
	CString s = DGetName(uIndex);

	GetDlgItem(ETNAME).SetWindowTextW(s);
	}

void CBSAPFULLSlaveTagsDialog::ShowIndx(UINT uIndex)
	{
	CString s;

	s.Printf(L"%d", uIndex);

	GetDlgItem(DINDEX).SetWindowTextW(s);
	}

void CBSAPFULLSlaveTagsDialog::ShowCBType(UINT uIndex, BOOL fNRT)
{
	SetCBType(fNRT ? addrLongAsLong : (UINT)(DGetType(uIndex)), fNRT);
	}

void CBSAPFULLSlaveTagsDialog::SetCBType(UINT uVal, BOOL fNRT)
{
	switch( uVal ) {

		case addrBitAsBit:	uVal = 1; break;
		case addrLongAsLong:	uVal = fNRT ? 2 : 5; break;
		case addrRealAsReal:	uVal = 0; break;
		case addrByteAsByte:	uVal = 3; break;
		case addrWordAsWord:	uVal = 4; break;
		}

	SetBoxPosition(CBTYPE, uVal);
	}

void CBSAPFULLSlaveTagsDialog::ShowMSD(UINT uIndex)
	{
	ShowPVal(DGetMSD((DWORD)uIndex), MSDVAL);
	}

void CBSAPFULLSlaveTagsDialog::ShowLNum(UINT uIndex)
	{
	WORD w = LOWORD(DGetLNLP(uIndex));

	ShowPVal((DWORD)HIBYTE(w), LISTNM);
	ShowPVal((DWORD)LOBYTE(w), LISTPVAL);
	}

void CBSAPFULLSlaveTagsDialog::ShowLevel(UINT uIndex)
	{
	UINT uLevel = DGetLevel(uIndex);

	SetBoxPosition(CBLEVEL, uLevel);
	}

void CBSAPFULLSlaveTagsDialog::ShowDevAdd(UINT uIndex) {

	ShowPVal(DGetDevAdd(uIndex), ETSLVNUM);
	}

void CBSAPFULLSlaveTagsDialog::ShowGlobal(UINT uIndex) {

	ShowPVal(DGetGlobalAddr(TRUE, uIndex), ETSLVGNM);
	}

void CBSAPFULLSlaveTagsDialog::ShowArrayStatus(UINT uIndex) {

	UINT lst	= 0;

	UINT col	= 0;

	WORD msd	= 0xFFFF;

	UINT min	= 0xFFFF;

	if( uIndex != NOTHING ) {

		UINT inx = 0;

		UINT sze = DGetArrayCount();

		WORD w	= LOWORD(DGetArrayStatus(uIndex));

		lst	= LOBYTE(w);

		if( (BOOL)lst ) {	// item has a list number

			col	= HIBYTE(w);

			msd	= LOWORD(DGetMSD(uIndex));

			while( inx < sze ) {

				UINT m = DGetMSD(inx);

				UINT l = LOBYTE(LOWORD(DGetArrayStatus(inx)));

				if( l == lst ) {

					min = min(min, m);
					}

				inx++;
				}
			}
		}

	SetBoxPosition(CBCOLUMN, min == msd ? col : 0);

	CString s;

	s.Printf( L"%d", lst);

	GetDlgItem(ETARRAY).SetWindowTextW(s);
	}

void CBSAPFULLSlaveTagsDialog::ShowPVal(DWORD dVal, UINT uID)
	{
	CString s;

	s.Printf(L"%d", dVal);

	GetDlgItem(uID).SetWindowTextW(s);
	}

BOOL CBSAPFULLSlaveTagsDialog::DeleteTag(void)
{
	BOOL fOk = FALSE;

	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		if( IsNRTTable(Addr.a.m_Table) ) {

			UINT uIndex = GetSelect(Addr.a.m_Table, Addr.a.m_Offset);

			CString s   = DGetName(uIndex);

			s = s.Left(4);

			if( s == L"zNRT" || s == L"zTMS" ) {

				return FALSE;
				}
			}

		CString  Name;

		((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ExpandAddress(Name, Addr);

		if( !m_fUpdate ) {

			CString Text  = CPrintf("Do you really want to delete %s ?", Name);

			m_fUpdate = NoYes(Text) == IDYES;
			}

		if( m_fUpdate ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			Tree.SetRedraw(FALSE);

			HTREEITEM hPrev = m_hSelect;

			Tree.MoveSelection();

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->DeleteTag(Addr, TRUE);

			Tree.DeleteItem(hPrev);

			Tree.SetRedraw(TRUE);

			((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();

			fOk = TRUE;

			m_fUpdate = FALSE;
			}
		}

	return fOk;
	}

BOOL CBSAPFULLSlaveTagsDialog::RenameTag(void)
{
	if( m_dwSelect ) {

		CAddress Addr = (CAddress &) m_dwSelect;

		CString Name;

		if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->ExpandAddress(Name, Addr) ) {

			CString Title   = L"Rename Variable";
			
			CString Group   = L"Variable Name";

			CString TagName = Name;

			CStringDialog Dlg(Title, Group, TagName);

			if( Dlg.Execute(ThisObject) ) {

				CTreeView &Tree = (CTreeView &) GetDlgItem(1001);
				
				CString    Name = Dlg.GetData();			

				if( ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->RenameTag(Name, Addr) ) {

					Tree.SetRedraw(FALSE);

					HTREEITEM hPrev = m_hSelect;

					Tree.MoveSelection();

					Tree.DeleteItem(hPrev);

					LoadTag(Tree, Name, Addr);

					Tree.SetRedraw(TRUE);

					((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->SetDirty();
					
					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Implementation

BOOL CBSAPFULLSlaveTagsDialog::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000);
	}

DWORD CBSAPFULLSlaveTagsDialog::MakeAddrFromSelects(UINT uOffset)
{
	CAddress Addr;

	UINT uType  = GetBoxPosition(CBTYPE);

	if( uType == 2 || uType == 12 ) return 0xFFFF;

	UINT uLevel = GetBoxPosition(CBLEVEL);

	UINT uDev   = GetDeviceAddr();

	DWORD dDev  = DSetDevNum(0x10000 + MakeDevNum(uLevel, uDev));	// get/set temporary device number in address list

	if( dDev == NOTHING ) {

		return NOTHING;		// device address table is full
		}

	Addr.a.m_Extra	= dDev % 15;

	UINT dAdj  = 1;

	switch( uType ) {

		case 0:
		case 10:
			Addr.a.m_Table	= uType == 10 ? SPREALINT : SPREAL0;
			Addr.a.m_Type	= addrRealAsReal;
			break;
		case 1:
		case 11:
			Addr.a.m_Table	= uType == 11 ? SPBITINT : SPBIT0;
			Addr.a.m_Type	= addrBitAsBit;
			break;

		case 2:
		case 12:
			Addr.a.m_Table	= SPNRT0;
			Addr.a.m_Type	= addrLongAsLong;
			break;

		case 3:
		case 13:
			Addr.a.m_Table	= uType == 13 ? SPBYTEINT : SPBYTE0;
			Addr.a.m_Type	= addrByteAsByte;
			break;

		case 4:
		case 14:
			Addr.a.m_Table	= uType == 14 ? SPWORDINT :SPWORD0;
			Addr.a.m_Type	= addrWordAsWord;
			break;

		case 5:
		case 15:
			Addr.a.m_Table	= uType == 15 ? SPDWORDINT :SPDWORD0;
			Addr.a.m_Type	= addrLongAsLong;
			break;

		case 6:
		case 16:
			Addr.a.m_Table	= uType == 16 ? SPSTRINGINT : SPSTRING0;
			Addr.a.m_Type	= addrLongAsLong;
			dAdj		= MAXDWORDLEN;
			break;
		}

	Addr.a.m_Table += uLevel;

	if( uOffset == NOTHING ) {	// creating tag

		UINT uETMSD = GetETMSD();

		UINT uMSD   = FindAvailableMSD(uETMSD, Addr);

		if( uMSD != uETMSD ) {	// User entered MSD exists

			CString Text  = CPrintf("MSD is used.  Use %d?", uMSD & 0xFFF);

			if( NoYes(Text) == IDNO ) {

				return NOTHING;
				}
			}

		CString sx;

		sx.Printf( L"%d", uMSD );

		GetDlgItem(MSDVAL).SetWindowTextW(sx);

		uOffset = uMSD;
		}

	Addr.a.m_Offset = uOffset;					// unique offset

	return Addr.m_Ref;
	}

UINT CBSAPFULLSlaveTagsDialog::FindAvailableMSD(UINT uMSD, CAddress Addr) {

	UINT u = DGetArrayCount();

	if( !(BOOL)u ) {

		return max(uMSD, 1);
		}

	PWORD pList = new WORD [u + 1];

	memset(pList, 0xFF, sizeof(WORD) * (u + 1));

	UINT i	= 0;

	while( i < u ) {	// copy and sort current MSD values

		if( IsNRTTable(DGetTable(i)) ) {

			pList[i] = 0;
			}

		else {
			pList[i] = (WORD)DGetMSD(i);
			}

		i++;
		}

	i = 0;

	while( i < u ) {

		WORD w = pList[i];

		WORD y = pList[i+1];

		if( w > y ) {

			pList[i]   = y;
			pList[i+1] = w;
			i = 0;
			continue;
			}

		i++;
		}

	i = 0;

	UINT uExtra = Addr.a.m_Extra;

	while( i < u ) {

		if( !IsNRTTable(DGetTable(i)) ) {

			UINT uDev    = DGetDevAdd(i);
			UINT uLev    = DGetLevel(i);

			UINT uDevNum = DSetDevNum(MakeDevNum(uLev, uDev));	// get this device number index

			UINT uSel    = pList[i];

			if( uMSD == uSel && uExtra == uDevNum ) {

				if( uMSD < pList[i + 1] ) {

					uMSD = uSel + 1;
					}
				}
			}

		i++;
		}

	delete [] pList;

	return uMSD;
	}

UINT CBSAPFULLSlaveTagsDialog::GetSelect(UINT uTable, UINT uOffset) {

	return IsNRTTable(uTable) ? GetNRTFromAddr(uOffset) : GetSelectFromAddr(uTable, uOffset);
	}

UINT CBSAPFULLSlaveTagsDialog::GetSelectFromAddr(UINT uTable, UINT uOffset) {

	UINT uMax = DGetArrayCount();

	if( uMax ) {

		for( UINT n = 0; n < uMax; n++ ) {

			if( uTable == DGetTable(n) ) {

				if( uOffset == DGetOffs(n) ) {

					return (BOOL)(uOffset % 64) ? n : n >> 6;
					}
				}
			}
		}

	return NOTHING;
	}

UINT CBSAPFULLSlaveTagsDialog::GetNRTFromAddr(UINT uOffset) {

	BYTE bAddr = GetDeviceAddr();

	UINT n = DGetArrayCount();

	BOOL fNew = uOffset == NOTHING;

	if( fNew && !bAddr ) {	// NRT 0 invalid

		return NOTHING;		
		}

	UINT uMax  = 0;

	while( n ) {	// find max offset or check entire list for duplicate

		UINT x     = n-1;

		UINT uOffs = DGetOffs(x);

		if( fNew ) {

			if( uOffs & 0x8000 ) {

				uMax = Max( uMax, LOBYTE(uOffs));
				}
			}

		else {
			if( uOffset == uOffs ) {

				return x;
				}
			}

		n--;
		}

	return fNew ? 0x8000 | (WORD)(bAddr << 8) | (uMax + 1) : 1;	// always start with 1
	}

BOOL CBSAPFULLSlaveTagsDialog::IsNRTTable(UINT uTable) {

	return uTable >= SPNRT0 && uTable <= SPNRT6;
	}

BOOL CBSAPFULLSlaveTagsDialog::IsBitTable(UINT uTable) {

	return uTable >= SPBIT0 && uTable <= SPBIT6;
	}

BOOL CBSAPFULLSlaveTagsDialog::IsRealTable(UINT uTable) {

	return uTable >= SPREAL0 && uTable <= SPREAL6;
	}

BOOL CBSAPFULLSlaveTagsDialog::IsStrTable(UINT uTable) {

	return uTable >= SPSTRING0 && uTable <= SPSTRING6;
	}

// Dialog Box Data Handling
void CBSAPFULLSlaveTagsDialog::LoadCBAll(void)
{
	LoadCBType();
	LoadCBLevel();
	LoadCBArray();
	}

void CBSAPFULLSlaveTagsDialog::LoadCBType(void)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(CBTYPE);

	ClearBox(CBTYPE);

	Box.AddString(L"REAL");
	Box.AddString(L"BIT");
	Box.AddString(L"NRT");
	Box.AddString(L"BYTE");
	Box.AddString(L"WORD");
	Box.AddString(L"DWORD");
//	Box.AddString(L"STRING");

	SetBoxPosition(CBTYPE, 0);
	}

void CBSAPFULLSlaveTagsDialog::LoadCBLevel(void) {

	CComboBox & Box	= (CComboBox &)GetDlgItem(CBLEVEL);

	ClearBox(CBLEVEL);

	CString s;

	for( UINT i = 0; i < 7; i++ ) {

		s.Printf( L"%d", i );

		Box.AddString(s);
		}

	SetBoxPosition(CBLEVEL, 0);
	}

void CBSAPFULLSlaveTagsDialog::LoadCBArray(void) {

	CComboBox & Box	= (CComboBox &)GetDlgItem(CBCOLUMN);

	ClearBox(CBCOLUMN);

	CString s;

	Box.AddString( L"Not item 1 of an array" );

	Box.AddString( L"Array Has 1 Column" );

	for( UINT i = 2; i < 20; i++ ) {

		s.Printf( L"Array Has %d Columns", i );

		Box.AddString(s);
		}

	SetBoxPosition(CBCOLUMN, 0);
	}

void CBSAPFULLSlaveTagsDialog::SetBoxPosition(UINT uID, UINT uPos)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CBSAPFULLSlaveTagsDialog::GetBoxPosition(UINT uID)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CBSAPFULLSlaveTagsDialog::ClearBox(CComboBox & ccb)
{
	UINT uCount = ccb.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		ccb.DeleteString(0);
		}
	}

void CBSAPFULLSlaveTagsDialog::ClearBox(UINT uID)
{
	ClearBox((CComboBox &)GetDlgItem(uID));
	}

UINT CBSAPFULLSlaveTagsDialog::GetCBType(void)
{	
	switch( GetBoxPosition(CBTYPE) ) {

		case 1: return addrBitAsBit;
		case 2: return addrLongAsLong;
		case 3: return addrByteAsByte;
		case 4: return addrWordAsWord;
		case 5: return addrLongAsLong;
		}

	return addrRealAsReal;
	}

UINT CBSAPFULLSlaveTagsDialog::GetETMSD(void)
	{
	return tatoi(GetDlgItem(MSDVAL).GetWindowTextW());
	}

UINT CBSAPFULLSlaveTagsDialog::GetETLNum(void)
	{
	return tatoi(GetDlgItem(LISTNM).GetWindowText());
	}

UINT CBSAPFULLSlaveTagsDialog::GetETLPos(void)
	{
	return tatoi(GetDlgItem(LISTPVAL).GetWindowText());
	}

UINT CBSAPFULLSlaveTagsDialog::GetETNandP(void)
	{
	return (GetETLNum() << 8) | GetETLPos();
	}

UINT CBSAPFULLSlaveTagsDialog::GetETIndex(void)
	{
	CString s = GetDlgItem(DINDEX).GetWindowTextW();

	if( s.Left(3) == L"Dev" ) {

		UINT uFind = s.Find('_');

		if( uFind < NOTHING ) {

			return 0x8000 + tatoi(s.Mid(uFind + 1));
			}
		}

	return tatoi(s);
	}

BYTE CBSAPFULLSlaveTagsDialog::GetDeviceAddr(void) {

	return LOBYTE(tatoi(GetDlgItem(ETSLVNUM).GetWindowTextW()));
	}

WORD CBSAPFULLSlaveTagsDialog::GetGlobal(void) {

	return LOWORD(tatoi(GetDlgItem(ETSLVGNM).GetWindowTextW()));
	}

UINT CBSAPFULLSlaveTagsDialog::GetArrayStatus(void) {

	return GetBoxPosition(CBCOLUMN);
	}

UINT CBSAPFULLSlaveTagsDialog::GetArrayData(void) {

	return tatoi(GetDlgItem(ETARRAY).GetWindowTextW());
	}

BYTE CBSAPFULLSlaveTagsDialog::GetLevel(void) {

	return LOBYTE(tatoi(GetDlgItem(CBLEVEL).GetWindowTextW()));
	}

DWORD CBSAPFULLSlaveTagsDialog::GetArrayValue(void) {

	return ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayValue;
	}

void CBSAPFULLSlaveTagsDialog::DoDlgEnables(void) {

	BOOL fEnable	= !m_fSelect;
	BOOL fNRT	= GetBoxPosition(CBTYPE) == 2;

	GetDlgItem(DINDEX).EnableWindow(TRUE);
	GetDlgItem(INDEXTXT).EnableWindow(TRUE);
	GetDlgItem(CBTYPE).EnableWindow(fEnable);

	GetDlgItem(PBCREATE).EnableWindow(fEnable && !fNRT);
	GetDlgItem(PBREFRSH).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVTXT).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVNUM).EnableWindow(fEnable && !fNRT);
	GetDlgItem(CBLEVEL).EnableWindow(fEnable && !fNRT);
	GetDlgItem(LEVELTXT).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVGTX).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ETSLVGNM).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ARRAYTXT).EnableWindow(fEnable && !fNRT);

	GetDlgItem(CBCOLUMN).EnableWindow(fEnable && !fNRT);

	BOOL fArray = (BOOL)tatoi(GetDlgItem(ETARRAY).GetWindowTextW());
	if( (fEnable && !fArray) || fNRT ) GetDlgItem(ETARRAY).SetWindowTextW("0");
	GetDlgItem(ETARRAY).EnableWindow(fEnable && !fNRT);
	GetDlgItem(ARRAYTXT).EnableWindow(fEnable && !fNRT);

	GetDlgItem(PBBOOL).EnableWindow(FALSE);
	GetDlgItem(PBREAL).EnableWindow(FALSE);

	fEnable &= !fNRT;

	GetDlgItem(ETNAME).EnableWindow(fEnable);
	GetDlgItem(LISTNM).EnableWindow(fEnable);
	GetDlgItem(LNUMTXT).EnableWindow(fEnable);
	GetDlgItem(LISTPOS).EnableWindow(fEnable);
	GetDlgItem(LISTPVAL).EnableWindow(fEnable);
	GetDlgItem(MSDVAL).EnableWindow(fEnable);
	GetDlgItem(MSDTXT).EnableWindow(fEnable);
	}

WORD CBSAPFULLSlaveTagsDialog::MakeDevNum(UINT uLevel, UINT uDevice) {

	return (WORD)(LOBYTE(uLevel) << 8) + LOBYTE(uDevice);
	}

UINT CBSAPFULLSlaveTagsDialog::MakeAStatus(void) {

	UINT  lst = GetArrayData();				// get dialog list number

	if( !(BOOL)lst ) {

		return 0;					// invalid list number
		}

	DWORD sze = DGetArrayCount();

	DWORD inx = 0;

	WORD  min = 0xFFFF;

	while( inx < sze ) {

		WORD w = LOWORD(DGetArrayStatus(inx));

		if( LOBYTE(w) == lst ) {			// is this list number

			min = min( min, LOWORD(DGetMSD(inx)) );
			}

		inx++;
		}

	WORD  a = LOWORD(GetETMSD());

	WORD hi = a == min || min == 0xFFFF ? LOWORD(GetBoxPosition(CBCOLUMN)) << 8 : 0;	// lowest MSD-keep column ct

	return hi | lst;
	}

// Device Config Array Access
DWORD CBSAPFULLSlaveTagsDialog::AccessArrayVal(DWORD dSel, DWORD dVal)
{
	SetArrayItems(dSel, dVal, L"");

	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->AccessArrayValue();

	return ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayValue;
	}

CString CBSAPFULLSlaveTagsDialog::AccessArrayStr(DWORD dSel, DWORD dVal, CString sStr)
{
	SetArrayItems(dSel, dVal, sStr);

	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->AccessArrayValue();

	return ((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayString;
	}

void CBSAPFULLSlaveTagsDialog::SetArrayItems(DWORD dSel, DWORD dVal, CString sStr)
{
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArraySelect = dSel;
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayValue  = dVal;
	((CBSAPFULLTagsDeviceOptions *)(m_pConfig))->m_ArrayString = sStr;
	}

void CBSAPFULLSlaveTagsDialog::DSetItem(DWORD dVal)
{
	AccessArrayVal(WARRITM, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DAppendItem(DWORD dVal)
{
	AccessArrayVal(FAPPEND, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetName(CString sStr)
{
	AccessArrayStr(WARRNAM, 0, sStr);
	}

void CBSAPFULLSlaveTagsDialog::DSetMSD(DWORD dVal)
{
	AccessArrayVal(WARRMSD, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetType(DWORD dVal)
{
	AccessArrayVal(WARRTYP, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetLNLP(DWORD dVal)
{
	AccessArrayVal(WARRLNA, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetLNum(DWORD dVal)
{
	AccessArrayVal(WARRLNA, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetOffs(DWORD dVal)
{
	AccessArrayVal(WARROFF, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetLevel(DWORD dVal)
{
	AccessArrayVal(WARRLVL, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetDevAdd(DWORD dVal)
{
	AccessArrayVal(WARRDVA, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetGlobal(DWORD dVal)
{
	AccessArrayVal(WARRGLB, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetDevGlobal(DWORD dVal)
{
	AccessArrayVal(WARRDGL, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetTable(DWORD dVal)
{
	AccessArrayVal(WARRTAB, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DSetArrayStatus(DWORD dVal)
{
	AccessArrayVal(WARRAST, dVal);
	}

void CBSAPFULLSlaveTagsDialog::DReplace(UINT uItem)
{
	AccessArrayVal(FREPLCE, uItem);
	}

void CBSAPFULLSlaveTagsDialog::ClearHolding(void)
{
	AccessArrayVal(FCLEAR, 0);
	}

void CBSAPFULLSlaveTagsDialog::DFreezeIndex(UINT uIndex)
{
	AccessArrayVal(WFREEZE, uIndex);
	}

DWORD CBSAPFULLSlaveTagsDialog::DSetDevNum(DWORD dValue)
{
	return (DWORD)(AccessArrayVal(WARRDNM, (DWORD)dValue) % 15);
	}

CString CBSAPFULLSlaveTagsDialog::DGetName(UINT uItem)
{
	return AccessArrayStr(RARRNAM, uItem, L""); 
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetIndx(UINT uItem)
{
	return AccessArrayVal(RARRITM, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetMSD(UINT uItem)
{
	return AccessArrayVal(RARRMSD, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetType(UINT uItem)
{
	return AccessArrayVal(RARRTYP, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetLNLP(UINT uItem)
{
	return AccessArrayVal(RARRLNA, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetLNum(UINT uItem)
{
	return AccessArrayVal(RARRLNM, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetLPos(UINT uItem)
{
	return AccessArrayVal(RARRLPS, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetOffs(UINT uItem)
{
	return AccessArrayVal(RARROFF, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetLevel(UINT uItem)
{
	return AccessArrayVal(RARRLVL, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetDevAdd(UINT uItem)
{
	return AccessArrayVal(RARRDVA, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetTable(UINT uItem)
{
	return AccessArrayVal(RARRTAB, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetLocalAddr(void)
{
	AccessArrayVal(GETALOC, 0);

	return (DWORD)LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetGlobalAddr(BOOL fOfDevice, UINT uItem)
{
	AccessArrayVal(RARRGLB, uItem);

	return (DWORD)LOWORD(GetArrayValue());
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetArrayStatus(UINT uItem)
{
	return AccessArrayVal(RARRAST, uItem);
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetDevNumExtra(UINT uIndex)
{
	UINT uDev = DGetDevAdd(uIndex);
	UINT uLev = DGetLevel(uIndex);

	return DSetDevNum(MakeDevNum(uLev, uDev));
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetDevNumFromExtra(UINT uExtra)
{
	return AccessArrayVal(RARRDNM, (DWORD)(uExtra & 0xF));
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetMLevel(void)
{
	AccessArrayVal(RARRMLV, 0);

	return (DWORD)LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetPath(void)
{
	AccessArrayVal(RARRPAT, 0);

	return (DWORD)LOBYTE(GetArrayValue());
	}

DWORD CBSAPFULLSlaveTagsDialog::DGetArrayCount(void)
{
	return AccessArrayVal(RARRCNT, 0);
	}

DWORD CBSAPFULLSlaveTagsDialog::DFindUnUsedMSD(void)
{
	return AccessArrayVal(FNXTMSD, 0);
	}

DWORD CBSAPFULLSlaveTagsDialog::DCheckMSD(DWORD dItem)
{
	return AccessArrayVal(CHKMSD, dItem);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Selection Dialog
		
AfxImplementRuntimeClass(CBSAPFULLTagsUDPDialog, CBSAPFULLMasterTagsDialog);

// Constructor

CBSAPFULLTagsUDPDialog::CBSAPFULLTagsUDPDialog(CBSAPFULLTagsUDPDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CBSAPFULLMasterTagsDialog(Driver, Addr, pConfig, fPart)
{
	CBSAPFULLMasterTagsDialog(Driver, Addr, pConfig, fPart);
	}

// Destructor

CBSAPFULLTagsUDPDialog::~CBSAPFULLTagsUDPDialog(void)
{
	afxThread->SetStatusText(L"");
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBSAPFULLTagsSerialDialog, CBSAPFULLMasterTagsDialog);
		
// Constructor

CBSAPFULLTagsSerialDialog::CBSAPFULLTagsSerialDialog(CBSAPFULLTagsSerialDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CBSAPFULLMasterTagsDialog(Driver, Addr, pConfig, fPart)
{
	CBSAPFULLMasterTagsDialog(Driver, Addr, pConfig, fPart);
	}

// Destructor

CBSAPFULLTagsSerialDialog::~CBSAPFULLTagsSerialDialog(void)
{
	afxThread->SetStatusText(L"");
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP UDP Selection Dialog
		
AfxImplementRuntimeClass(CBSAPFULLTagsUDPSDialog, CBSAPFULLSlaveTagsDialog);

// Constructor

CBSAPFULLTagsUDPSDialog::CBSAPFULLTagsUDPSDialog(CBSAPFULLTagsUDPSDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CBSAPFULLSlaveTagsDialog(Driver, Addr, pConfig, fPart)
{
	CBSAPFULLSlaveTagsDialog(Driver, Addr, pConfig, fPart);
	}

// Destructor

CBSAPFULLTagsUDPSDialog::~CBSAPFULLTagsUDPSDialog(void)
{
	afxThread->SetStatusText(L"");
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBSAPFULLTagsSerialSDialog, CBSAPFULLSlaveTagsDialog);
		
// Constructor

CBSAPFULLTagsSerialSDialog::CBSAPFULLTagsSerialSDialog(CBSAPFULLTagsSerialSDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CBSAPFULLSlaveTagsDialog(Driver, Addr, pConfig, fPart)
{
	CBSAPFULLSlaveTagsDialog(Driver, Addr, pConfig, fPart);
	}

// Destructor

CBSAPFULLTagsSerialSDialog::~CBSAPFULLTagsSerialSDialog(void)
{
	afxThread->SetStatusText(L"");
	}

// End of File
