
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Standard Toolbox
//

// Runtime Class

AfxImplementRuntimeClass(CStdTools, CObject);

// Static Data

CStdTools * CStdTools::m_pThis = NULL;

// Object Location

CStdTools * CStdTools::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CStdTools::CStdTools(void)
{
	AfxAssert(m_pThis == NULL);

	m_hWnd   = NULL;

	m_dpi    = GetDPI();

	m_fWin2K = GetWin2K();

	m_fVista = GetVista();

	m_fBlend = GetCanBlend();

	m_fAero  = FALSE;

	memset(m_Force, 0, sizeof(m_Force));

	LoadThemeLib();

	DoUpdate(TRUE);

	MakeFonts();	

	m_pThis = this;
	}

// Destructor

CStdTools::~CStdTools(void)
{
	AfxAssert(m_pThis == this);

	m_pThis = NULL;

	FreeThemeLib();
	}

// Attributes

int CStdTools::AdjustDPI(int n) const
{
	return n * m_dpi / 96;
	}

BOOL CStdTools::Win2K(void) const
{
	return m_fWin2K;
	}

BOOL CStdTools::Vista(void) const
{
	return m_fVista;
	}

BOOL CStdTools::CanBlend(void) const
{
	return m_fBlend;
	}

BOOL CStdTools::UsingAero(void) const
{
	return m_fAero;
	}

CString CStdTools::DlgFont(void) const
{
	// REV3 -- Option to use Segoe UI all the time?

	return m_fVista ? IDS_FONT_PROP_2 : IDS_FONT_PROP_1;
	}

// Operations

void CStdTools::Update(void)
{
	DoUpdate(FALSE);
	}

// cppcheck-suppress passedByValue

BOOL CStdTools::SetColor(int nPos, CColor Color)
{
	if( m_Color[nPos] != Color ) {

		m_Force[nPos] = 1;

		m_Color[nPos] = Color;

		return TRUE;
		}

	return FALSE;
	}

void CStdTools::SetMainWnd(void)
{
	m_hWnd = afxMainWnd->GetHandle();

	DoUpdate(FALSE);
	}

// Theme Access

BOOL CStdTools::HasTheme(void) const
{
	return m_fTheme;
	}

HANDLE CStdTools::OpenTheme(CString const &Class)
{
	if( m_fTheme && m_hWnd ) {

		if( m_Class != Class ) {

			if( m_hTheme ) {

				(*m_pfnClose)(m_hTheme);
				}

			m_hTheme = (*m_pfnOpen)(m_hWnd, Class);

			m_Class  = Class;
			}

		return m_hTheme;
		}

	return NULL;
	}

CColor CStdTools::GetThemeColor(CString const &Class, int nPart, int nState, int nProp)
{
	OpenTheme(Class);

	return GetThemeColor(nPart, nState, nProp);
	}

CColor CStdTools::GetThemeColor(int nPart, int nState, int nProp)
{
	CColor Col;

	if( m_hTheme ) {

		(*m_pfnGetCol)( m_hTheme,
				nPart,
				nState,
				nProp,
				PDWORD(&Col)
				);
		}

	return Col;
	}

void CStdTools::CloseTheme(void)
{
	if( m_hTheme ) {

		(*m_pfnClose)(m_hTheme);

		m_hTheme = NULL;

		m_Class.Empty();
		}
	}

// Implementation

void CStdTools::DoUpdate(BOOL fSafe)
{
	if( !fSafe ) {

		if( m_hLib ) {

			if( (m_fTheme = (*m_pfnCheck)()) ) {

				// NOTE -- Sanity check to avoid a situation where a broken
				// theme gives us a black-on-black display. We have seen this
				// on a customer's machine that was running Windows XP.

				CColor c1 = GetThemeColor(L"Tab", 9, 1, 3821);
				
				CColor c2 = CColor(COLOR_WINDOWTEXT);

				CColor c3 = CColor(COLOR_BTNTEXT);

				if( c1 == c2 || c1 == c3 ) {

					m_fTheme = FALSE;
					}
				}
			}
		}

	for( int nPos = 0; nPos < elements(m_Color); nPos++ ) {

		CColor Color;
		
		if( m_Force[nPos] ) {
			
			Color = m_Color[nPos];
			}
		else {
			Color = FindColor(nPos);

			m_Color[nPos] = Color;
			}
		
		m_Brush[nPos].Create(Color);

		m_Pen  [nPos].Create(Color);
		}

	m_fAero = GetAero();

	CloseTheme();
	}

CColor CStdTools::FindColor(int nPos)
{
	switch( nPos ) {

		case colBackground:	return CColor(COLOR_BACKGROUND);
		case colAppWorkspace:	return CColor(COLOR_APPWORKSPACE);
		case colWindowText:	return CColor(COLOR_WINDOWTEXT);
		case colWindowBack:	return CColor(COLOR_WINDOW);
		case colSelectText:	return CColor(COLOR_HIGHLIGHTTEXT);
		case colSelectBack:	return CColor(COLOR_HIGHLIGHT);
		case colButtonText:	return CColor(COLOR_BTNTEXT);
		case col3dFace:		return CColor(COLOR_3DFACE);
		case col3dShadow:	return CColor(COLOR_3DSHADOW);
		case col3dHighlight:	return CColor(COLOR_3DHIGHLIGHT);
		case col3dDkShadow:	return CColor(COLOR_3DDKSHADOW);
		case col3dLight:	return CColor(COLOR_3DLIGHT);
		case colInfoText:	return CColor(COLOR_INFOTEXT);
		case colInfoBack:	return CColor(COLOR_INFOBK);
		case colMASK:		return CColor(0xFF, 0x00, 0xFF);
		case colWHITE:		return CColor(0xFF, 0xFF, 0xFF);
		case colBLACK:		return CColor(0x00, 0x00, 0x00);
		case colRED:		return CColor(0xFF, 0x00, 0x00);
		case colGREEN:		return CColor(0x00, 0xFF, 0x00);
		case colBLUE:		return CColor(0x00, 0x00, 0xFF);
		case colYELLOW:		return CColor(0xFF, 0xFF, 0x00);
		case colCYAN:		return CColor(0x00, 0xFF, 0xFF);
		case colMAGENTA:	return CColor(0xFF, 0x00, 0xFF);
		case colTabGroup:	return CColor(COLOR_WINDOWTEXT);
		case colMenuFace:	return CColor(0xFF, 0xFF, 0xFF);
		case colMenuText:	return CColor(0x00, 0x00, 0x00);
		case colOrange1:	return CColor(255, 145,  78);
		case colOrange2:	return CColor(255, 200, 145);
		case colOrange3:	return CColor(255, 240, 200);
		case colNavBar1:	return CColor(240, 150,  20);
		case colNavBar2:	return CColor(250, 230, 150);
		case colNavBar3:	return CColor(250, 200,  90);
		case colNavBar4:	return CColor(255, 255, 220);
		}

	if( !m_fTheme || !m_hWnd ) {

		switch( nPos ) {
			
			case colTabFace:	return CColor(0xF8, 0xF8, 0xF8);
			case colTabLock:	return CColor(0xF2, 0xF2, 0xF2);
			
			case colDisabled:	return CColor(200, 200, 185);
			case colEnabled:	return CColor(127, 157, 185);
			
			case colBlue1:		return CColor(244, 244, 250);
			case colBlue2:		return CColor(216, 216, 230);
			case colBlue3:		return CColor(150, 150, 180);
			case colBlue4:		return CColor(110, 110, 140);
			
			case colMenuBar:	return CColor(COLOR_3DFACE);
			}

		AfxAssert(FALSE);

		return 0;
		}

	switch( nPos ) {

		case colTabFace:	return GetThemeColor(L"Tab",          9, 1, 3821);
		case colTabLock:	return GetThemeColor(L"Button",       1, 1, 3821);
		
		case colEnabled:	return GetThemeColor(L"ListView",     1, 1, 3801);
		case colDisabled:	return GetThemeColor(L"ListView",     1, 1, 3806);

		// LATER -- These just don't look as good. Perhaps we
		// could use our colors for silver mode in XP, and use
		// the OS colors in other situations. Or just mix our
		// colors from a couple of colors from the OS palette?

//		case colBlue1:		return GetThemeColor(L"ExplorerBar",  5, 1, 3802);
//		case colBlue2:		return GetThemeColor(L"ExplorerBar",  1, 1, 3810);
//		case colBlue3:		return GetThemeColor(L"ExplorerBar",  1, 1, 3811);
//		case colBlue4:		return GetThemeColor(L"ExplorerBar", 10, 1, 3821);
		
		case colBlue1:		return CColor(244, 244, 250);
		case colBlue2:		return CColor(216, 216, 230);
		case colBlue3:		return CColor(150, 150, 180);
		case colBlue4:		return CColor(110, 110, 140);

		case colMenuBar:	return CColor(COLOR_MENUBAR);
		}

	AfxAssert(FALSE);

	return 0;
	}

void CStdTools::MakeFonts(void)
{
	CClientDC DC(NULL);

	CString FontP(DlgFont());

	CString FontF(IDS_FONT_FIXED);

	CString FontM(IDS_FONT_MARLETT);

	UINT uNorm = 0;

	UINT uJags = CFont::NoSmooth;

	UINT uBold = CFont::Bold;

	UINT uLean = CFont::Italic;

	int  nSize = 8;

	m_Font[fontDialog  ].Create(DC, FontP, nSize,	  uNorm,   0);
	m_Font[fontDialogNA].Create(DC, FontP, nSize,	  uJags,   0);
	m_Font[fontRotate  ].Create(DC, FontP, nSize,	  uNorm, 900);
	m_Font[fontStatus  ].Create(DC, FontP, nSize,	  uNorm,   0);
	m_Font[fontLarge   ].Create(DC, FontP, nSize + 2, uNorm,   0);
	m_Font[fontToolTip ].Create(DC, FontP, nSize,	  uNorm,   0);
	m_Font[fontToolFix ].Create(DC, FontF, nSize,	  uNorm,   0);
	m_Font[fontFixed   ].Create(DC, FontF, 9,         uNorm,   0);
	m_Font[fontBolder  ].Create(DC, FontP, nSize,	  uBold,   0);
	m_Font[fontItalic  ].Create(DC, FontP, nSize,	  uLean,   0);
	m_Font[fontMarlett1].Create(DC, FontM, 8,	  uNorm,   0);
	m_Font[fontMarlett2].Create(DC, FontM, 12,	  uNorm,   0);
	}

BOOL CStdTools::GetWin2K(void)
{
	OSVERSIONINFO Ver;

	Ver.dwOSVersionInfoSize = sizeof(Ver);

	GetVersionEx(&Ver);

	if( Ver.dwMajorVersion == 5 ) {
		
		if( !Ver.dwMinorVersion ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CStdTools::GetVista(void)
{
	OSVERSIONINFO Ver;

	Ver.dwOSVersionInfoSize = sizeof(Ver);

	GetVersionEx(&Ver);

	if( Ver.dwMajorVersion >= 6 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CStdTools::GetCanBlend(void)
{
	if( !GetSystemMetrics(SM_REMOTESESSION) ) {
	
		OSVERSIONINFO Ver;

		Ver.dwOSVersionInfoSize = sizeof(Ver);

		GetVersionEx(&Ver);

		if( Ver.dwMajorVersion >= 6 ) {

			return TRUE;
			}

		if( GetDeviceCaps(CClientDC(NULL), SHADEBLENDCAPS) ) {

			return TRUE;
			}
		}
		
	return FALSE;
	}

BOOL CStdTools::GetAero(void)
{
	OSVERSIONINFO Ver;

	Ver.dwOSVersionInfoSize = sizeof(Ver);

	GetVersionEx(&Ver);

	if( Ver.dwMajorVersion >= 6 ) {

		HANDLE hLib = LoadLibrary(L"dwmapi.dll");

		if( hLib ) {

			PISAERO pfnProc = PISAERO(GetProcAddress(hLib, "DwmIsCompositionEnabled"));

			if( pfnProc ) {

				BOOL fEnable = FALSE;

				(*pfnProc)(&fEnable);

				return fEnable;
				}
			}
		}

	return FALSE;
	}

int CStdTools::GetDPI(void)
{
	CClientDC DC(NULL);

	return GetDeviceCaps(DC, LOGPIXELSX);
	}

BOOL CStdTools::LoadThemeLib(void)
{
	m_fTheme = FALSE;

	m_hTheme = NULL;
	
	m_hLib   = LoadLibrary(L"uxtheme.dll");

	if( m_hLib ) {

		m_pfnCheck  = PCHECK (GetProcAddress(m_hLib, "IsThemeActive"));

		m_pfnOpen   = POPEN  (GetProcAddress(m_hLib, "OpenThemeData"));

		m_pfnGetCol = PGETCOL(GetProcAddress(m_hLib, "GetThemeColor"));
		
		m_pfnClose  = PCLOSE (GetProcAddress(m_hLib, "CloseThemeData"));

		return TRUE;
		}

	return FALSE;
	}

BOOL CStdTools::FreeThemeLib(void)
{
	if( m_hLib ) {

		FreeLibrary(m_hLib);

		return TRUE;
		}

	return FALSE;
	}

// End of File
