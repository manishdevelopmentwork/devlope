
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IpuIdmac51_HPP
	
#define	INCLUDE_IpuIdmac51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpu51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit DMA Control Module
//

class CIpuImageDma51
{	
	public:
		// Constructor
		CIpuImageDma51(CIpu51 *pIpu);

		// Interface
		void SetChanEnable(UINT uChan, bool fSet);
		void SetChanSepAlpha(UINT uChan, bool fSet);
		void SetChanAltSepAlpha(UINT uChan, bool fSet);
		void SetChanPriority(UINT uChan, bool fSet);
		void SetChanWatermark(UINT uChan, bool fSet);
		void SetChanLock(UINT uChan, bool fSet);
		void SetChanAltAddr(UINT uChan, UINT uAddr);
		UINT GetChanAltAddr(UINT uChan);
		void SetChanBandMode(UINT uChan, bool fSet);
		void SetCroll(UINT nSet, UINT x, UINT y);
		bool GetChanBusy(UINT uChan);
		
	protected:
		// Regisgters
		enum
		{
			regConfig	= 0x0000 / sizeof(DWORD),
			regChanEnable	= 0x0004 / sizeof(DWORD),
			regSepAlpha	= 0x000C / sizeof(DWORD),
			regAltSepAlpha	= 0x0010 / sizeof(DWORD),
			regChanPriority = 0x0014 / sizeof(DWORD),
			regWmEnable	= 0x001C / sizeof(DWORD),
			regLockEnable	= 0x0024 / sizeof(DWORD),
			regSubAddr	= 0x002C / sizeof(DWORD),
			regBmEnable	= 0x0040 / sizeof(DWORD),
			regScrollCoord	= 0x0048 / sizeof(DWORD),
			regChanBusy	= 0x0100 / sizeof(DWORD),
			};

		// Data
		PVDWORD   m_pBase;
		CIpu51  * m_pIpu;

		// Implementation
		void SetChanAltAddr(DWORD volatile &Reg, DWORD dwData, UINT uShift);
		UINT GetChanAltAddr(DWORD volatile &Reg, UINT uShift);
		void SetBit(DWORD Reg, UINT uBit, BOOL fSet);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Operations

INLINE void CIpuImageDma51::SetBit(DWORD r, UINT n, BOOL f)
{
	if( f ) {

		AtomicBitSet(&r, n);
		}
	else {
		AtomicBitClr(&r, n);
		}
	}

// End of File

#endif
