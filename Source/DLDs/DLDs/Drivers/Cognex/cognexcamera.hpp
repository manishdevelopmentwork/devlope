
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCognexCamera;

#pragma pack(2)

struct HEADER_BM
{
	CHAR	Magic[2];
	DWORD	Size;
	DWORD	Reserved;
	DWORD	Offset;
	BYTE	pad[4];
	DWORD	cx;
	DWORD	cy;
	BYTE	pad2[2];
	WORD	Bits;
	};

#pragma pack()

class CCognexCamera : public CCameraDriver
{
	public:
		// Constructor
		CCognexCamera();

		// Destructor
		~CCognexCamera();

		DEFMETH(void)	Load(LPCBYTE pData);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE)	Ping(void);
		DEFMETH(CCODE)	ReadImage(PBYTE &pData);
		DEFMETH(void)	SwapImage(PBYTE pData);
		DEFMETH(void)	KillImage(void);
		DEFMETH(PCBYTE)	GetData(UINT uDevice);
		DEFMETH(UINT)	GetInfo(UINT uDevice, UINT uParam);

		DEFMETH(BOOL)	SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL)	LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		DEFMETH(BOOL)	UseSetup (PVOID pContext, UINT uIndex);
		
	protected:

		struct CContext
		{
			CContext * m_pNext;
			CContext * m_pPrev;
			UINT	   m_uDevice;
			DWORD	   m_IP;
			UINT	   m_uPort;
			BOOL	   m_fKeep;
			BOOL	   m_fPing;
			UINT	   m_uTime1;
			UINT	   m_uTime2;
			UINT	   m_uTime3;
			UINT	   m_uTime4;
			UINT	   m_uLast3;
			UINT	   m_uLast4;
			PBYTE	   m_pData;
			UINT	   m_uInfo[4];
			ISocket  * m_pSock;
			UINT	   m_uImgSize;
			UINT	   m_uFrame;
			BOOL	   m_fSession;
			BOOL	   m_fCmdSent;
			PTXT	   m_pUser;
			PTXT	   m_pPass;
			char	   m_Cmd[32];
			};

		// Data Members
		CContext	* m_pCtx;
		CContext	* m_pHead;
		CContext	* m_pTail;
		UINT		m_uType;
		UINT		m_uKeep;

		// Display Help
		IDisplayHelper	* m_pDisp;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Implementation
		BOOL GetImage(PBYTE &pData);
		BOOL Read(PBYTE pData, UINT uCount);
		BOOL Send(ISocket *pSock, PCTXT pText, PTXT pArgs);
		BOOL Send(ISocket *pSock, PCBYTE pText, UINT uSize);
		BOOL ReadHeader(PBYTE pEncHead, HEADER_BM &Header);

		// Helpers
		void DecodeAscii(PBYTE Src, PBYTE Dst, UINT SrcSize);
		WORD FindCRC16(WORD CRC, PBYTE Src, UINT Size);
		BOOL Login(CContext *pCtx);
		BOOL SendCommand(CContext *pCtx);
		BOOL ValidateBitmap(PBYTE pHead, PBYTE pEnc, UINT uSize, WORD CRC);
		void EndSession(CContext *pCtx);
		PBYTE CreateDummy(HEADER_BM &Header);
		PBYTE CreateBitmap(HEADER_BM &Header, PBYTE pImg);
	};

// End of File
