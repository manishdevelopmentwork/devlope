
#include "Intern.hpp"

#include "HttpBase64.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Base-64 Endcoder
//

// Attributes

UINT CHttpBase64::GetEncodeSize(UINT uSize)
{
	return ((uSize + 2) / 3) * 4;
}

UINT CHttpBase64::GetDecodeSize(PCSTR pText)
{
	UINT s = strlen(pText);

	UINT n = ((s + 3) / 4) * 3;

	if( s > 1 && pText[s-1] == '=' ) n--;

	if( s > 2 && pText[s-2] == '=' ) n--;

	return n;
}

// Operations

BOOL CHttpBase64::Encode(PSTR pBuff, PCBYTE pData, UINT uSize, BOOL fBreak)
{
	if( pData ) {

		PSTR   p2 = pBuff;

		PCBYTE p1 = pData;

		UINT   bc = 0;

		while( bc < uSize ) {

			UINT nb = min(3, uSize - bc);

			BYTE b0 = (nb >= 1) ? *p1++ : 0;

			BYTE b1 = (nb >= 2) ? *p1++ : 0;

			BYTE b2 = (nb >= 3) ? *p1++ : 0;

			BYTE c1 = (b0 >> 2);

			BYTE c2 = ((b0 & 3) << 4) | (b1 >> 4);

			*p2++ = ByteEncode(c1);

			*p2++ = ByteEncode(c2);

			if( nb >= 2 ) {

				BYTE c3 = ((b1 & 15) << 2) | (b2 >> 6);

				*p2++ = ByteEncode(c3);
			}

			if( nb >= 3 ) {

				BYTE c4 = (b2 & 0x3F);

				*p2++ = ByteEncode(c4);
			}

			while( nb < 3 ) {

				*p2++ = '=';

				nb++;
			}

			bc += nb;

			if( fBreak ) {

				if( bc % 57 == 0 || bc >= uSize ) {

					*p2++ = 0x0D;

					*p2++ = 0x0A;
				}
			}
		}

		*p2++ = 0;

		return TRUE;
	}

	return FALSE;
}

PSTR CHttpBase64::Encode(PCBYTE pData, UINT uSize, BOOL fBreak)
{
	UINT uEncode = GetEncodeSize(uSize);

	PSTR pEncode = new char[uEncode + 1];

	Encode(pEncode, pData, uSize, fBreak);

	return pEncode;
}

BOOL CHttpBase64::Decode(PBYTE pBuff, PCSTR pText, UINT uText)
{
	if( pText ) {

		PBYTE  p2 = pBuff;

		PCSTR  p1 = pText;

		UINT   es = uText;

		UINT   bc = 0;

		while( isspace(*p1) ) {

			p1++;

			es--;
		}

		while( isspace(p1[es-1]) ) {

			es--;
		}

		while( bc < es ) {

			UINT nb = min(4, es - bc);

			BYTE c1 = (nb >= 1) ? *p1++ : '=';

			BYTE c2 = (nb >= 2) ? *p1++ : '=';

			BYTE c3 = (nb >= 3) ? *p1++ : '=';

			BYTE c4 = (nb >= 4) ? *p1++ : '=';

			BYTE b1 = ByteDecode(c1);

			BYTE b2 = ByteDecode(c2);

			BYTE b3 = ByteDecode(c3);

			BYTE b4 = ByteDecode(c4);

			if( TRUE ) {

				*p2++ = ((b1 << 2) | (b2 >> 4));
			}

			if( c3 != '=' ) {

				*p2++ = ((b2 & 0x0F) << 4) | (b3 >> 2);
			}

			if( c4 != '=' ) {

				*p2++ = ((b3 & 0x03) << 6) | b4;
			}

			bc += nb;
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CHttpBase64::Decode(PBYTE pBuff, PCSTR pText)
{
	return Decode(pBuff, pText, strlen(pText));
}

PBYTE CHttpBase64::Decode(PCSTR pText, UINT uExtra)
{
	UINT  uSize = GetDecodeSize(pText);

	PBYTE pData = new BYTE[uSize + uExtra];

	Decode(pData, pText);

	return pData;
}

// Implementation

BOOL CHttpBase64::IsBase64(BYTE cData)
{
	if( cData >= 'A' && cData <= 'Z' ) return TRUE;

	if( cData >= 'a' && cData <= 'z' ) return TRUE;

	if( cData >= '0' && cData <= '9' ) return TRUE;

	if( cData == '+' ) return TRUE;

	if( cData == '/' ) return TRUE;

	if( cData == '=' ) return TRUE;

	return FALSE;
}

BYTE CHttpBase64::ByteEncode(BYTE bData)
{
	if( bData <  26 ) return 'A' + bData;

	if( bData <  52 ) return 'a' + (bData - 26);

	if( bData <  62 ) return '0' + (bData - 52);

	if( bData == 62 ) return '+';

	return '/';
}

BYTE CHttpBase64::ByteDecode(BYTE cData)
{
	if( cData >= 'A' && cData <= 'Z' ) return cData - 'A';

	if( cData >= 'a' && cData <= 'z' ) return cData - 'a' + 26;

	if( cData >= '0' && cData <= '9' ) return cData - '0' + 52;

	if( cData == '+' ) return 62;

	return 63;
}

// End of File
