
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Colorado CO07 Model Data
//

static BYTE imageCO07[] = {

	#include "co07-x1.png.dat"
	0
	};

global CHostModel modelCO07 = {

	"",
	"CO07",
	"CR1000-07",
	"CR1000-07000",
	rfColorado,
	1,
	988,
	758,
	94,
	124,
	800,
	480,
	0,
	NULL,
	sizeof(imageCO07)-1,
	imageCO07
	};

// End of File
