
#include "Intern.hpp"

#include "Ecspi51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IoMux51.hpp"

#include "Ccm51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Enhanced Configurable Serial Peripheral Interface
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Instantiator

IDevice * Create_SpiInterface51(CIoMux51 *pMux, CCcm51 *pCcm)
{
	IDevice *pDevice = (IDevice *)(ISpi *) New CEcspi51(pMux, pCcm);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CEcspi51::CEcspi51(CIoMux51 *pMux, CCcm51 *pCcm)
{
	StdSetRef();

	m_pEvent   = Create_AutoEvent();

	m_pMutex   = Create_Qutex();

	m_pBase    = PVDWORD(ADDR_ECSPI1);

	m_uClock   = pCcm->GetFreq(CCcm51::clkEcspi);

	m_uLine    = INT_CSPI1;

	m_pReq     = NULL;

	m_uHead    = 0;

	m_uTail    = 0;

	m_pIoMux   = pMux;

	m_uMux[0]  = CIoMux51::MUX_CSPI1_SS0;

	m_uMux[1]  = CIoMux51::MUX_CSPI1_SS1;

	m_uMuxSel  = 0;

	m_uMuxGpio = 3;
	}

// Destructor

CEcspi51::~CEcspi51(void)
{
	m_pEvent->Release();

	m_pMutex->Release();
	}

// IUnknown

HRESULT CEcspi51::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISpi);

	return E_NOINTERFACE;
	}

ULONG CEcspi51::AddRef(void)
{
	StdAddRef();
	}

ULONG CEcspi51::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CEcspi51::Open(void)
{
	InitController();

	InitEvents();

	memset(m_Config, 0, sizeof(m_Config));

	return TRUE;
	}

// ISpi

BOOL METHOD CEcspi51::Init(UINT uChan, CSpiConfig const &Config)
{
	if( uChan < elements(m_Config) ) {

		Reg(Config) &= 0x00FFFFFF;

		Reg(Config) &= ~(0x00111111 << uChan);

		if( Config.m_fClockIdle ) {

			Reg(Config) |= (1 << (20 + uChan));
			}

		if( !Config.m_fDataIdle ) {

			Reg(Config) |= (1 << (16 + uChan));
			}

		if( Config.m_uSelect ) {

			Reg(Config) |= (1 << (12 + uChan));
			}
		
		if( Config.m_fBurst ) {

			Reg(Config) |= (1 << (8 + uChan));
			}
		
		if( Config.m_uClockPol ) {

			Reg(Config) |= (1 << (4 + uChan));
			}
		
		if( Config.m_uClockPha ) {

			Reg(Config) |= (1 << (0 + uChan));
			}
		
		FindDivider(uChan, Config.m_uFreq);

		return true;
		}

	return false;
	}

BOOL METHOD CEcspi51::Send(UINT uChan, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData)
{
	CRequest Req;

	Req.m_uChan = uChan;
	Req.m_pHead = pHead;
	Req.m_uHead = uHead;
	Req.m_pData = PBYTE(pData);
	Req.m_uData = uData;
	Req.m_fSend = true;

	return Request(&Req);
	}

BOOL METHOD CEcspi51::Recv(UINT uChan, PCBYTE pHead, UINT uHead, PBYTE pData, UINT uData)
{
	CRequest Req;

	Req.m_uChan = uChan;
	Req.m_pHead = pHead;
	Req.m_uHead = uHead;
	Req.m_pData = pData;
	Req.m_uData = uData;
	Req.m_fSend = false;

	return Request(&Req);
	}

// IEventSink

void CEcspi51::OnEvent(UINT uLine, UINT uParam)
{
	if( Reg(Stat) & intTransfer ) {

		Reg(Stat) = intTransfer;

		if( m_pReq->m_fSend ) {

			ReadFifo(NULL, 0, m_pReq->m_uHead + m_pReq->m_uData);
			}
		else {
			ReadFifo(m_pReq->m_pData, m_pReq->m_uData, m_pReq->m_uHead);
			}

		CommsDone(m_pReq);

		EnableSelect(m_pReq->m_uChan, false);

		if( m_uHead == m_uTail ) {

			m_pReq = NULL;
			}
		else {
			m_pReq  = m_pList[m_uHead];

			m_uHead = (m_uHead + 1) % elements(m_pList);

			StartComms();
			}
		}
	}

// Implementation

void CEcspi51::InitController(void)
{
	Reg(Control) = 0x00000000;

	Reg(Control) = 0x000000F1;

	Reg(Stat)    = intTransfer;

	Reg(Int)     = intTransfer;
	}

void CEcspi51::InitEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);

	phal->EnableLine(m_uLine, true);
	}

bool CEcspi51::Request(CRequest *pReq)
{
	if( pReq->m_uChan < elements(m_Config) ) {
		
		UINT uLen = ((pReq->m_uHead + pReq->m_uData) * 8) - 1;
	
		if( uLen < 2048 ) {

			InitWait(pReq);

			UINT uSave;

			phal->EnableLineViaIrql(m_uLine, uSave, false);

			if( m_pReq == NULL ) {

				m_pReq = pReq;

				StartComms();

				phal->EnableLineViaIrql(m_uLine, uSave, true);
				}
			else {
				UINT uNext = (m_uTail + 1) % elements(m_pList);

				if( uNext == m_uHead ) {

					AfxTrace("CEcspi: Queue Full\n");

					phal->EnableLineViaIrql(m_uLine, uSave, true);

					TermWait(pReq);

					return false;
					}

				m_pList[m_uTail] = pReq;

				m_uTail		 = uNext;

				phal->EnableLineViaIrql(m_uLine, uSave, true);
				}

			WaitDone(pReq);
			
			return true;
			}
		}
	
	return false;
	}

void CEcspi51::StartComms(void)
{
	CConfig &Cfg = m_Config[ m_pReq->m_uChan ];

	UINT    uLen = ((m_pReq->m_uHead + m_pReq->m_uData) * 8) - 1;

	Reg(Control) = (uLen		<< 20) |
		       (m_pReq->m_uChan << 18) |
		       (Cfg.m_uPre	<< 12) | 
		       (Cfg.m_uPod	<<  8) |
		       (0x0F		<<  4) |
   		       (0x01		<<  0) ;

	Reg(Dma)     = 0;

	Reg(Period)  = 0;

	FlushFifo();
		
	WriteFifo( m_pReq->m_pHead, 
		   m_pReq->m_uHead, 
		   m_pReq->m_fSend ? m_pReq->m_pData : NULL, 
		   m_pReq->m_uData
		   );

	EnableSelect(m_pReq->m_uChan, true);

	Reg(Control) |= Bit(2);
	}

void CEcspi51::InitWait(CRequest *pReq)
{
	bool fWait = true;

	if( Hal_GetIrql() >= IRQL_DISPATCH || !GetCurrentThread() || GetThreadIndex() <= 1 ) {

		fWait = false;
		}

	if( fWait ) {

		m_pMutex->Wait(FOREVER);

		pReq->m_fPoll = false;
		}
	else {
		pReq->m_fPoll = true;

		pReq->m_fDone = false;
		}
	}

void CEcspi51::TermWait(CRequest *pReq)
{
	if( !pReq->m_fPoll ) {

		m_pMutex->Free();

		CheckThreadCancellation();
		}
	}

void CEcspi51::WaitDone(CRequest *pReq)
{
	if( pReq->m_fPoll ) {

		bool volatile &fDone = pReq->m_fDone;

		while( !fDone );
		}
	else {
		m_pEvent->Wait(FOREVER);

		m_pMutex->Free();

		CheckThreadCancellation();
		}
	}

void CEcspi51::CommsDone(CRequest *pReq)
{
	if( pReq->m_fPoll ) {

		pReq->m_fDone = true;
		}
	else {
		m_pEvent->Set();
		}
	}

void CEcspi51::WriteFifo(PCBYTE pData1, UINT uData1, PCBYTE pData2, UINT uData2)
{
	DWORD  dwData = 0;
		
	PCBYTE pData  = pData1;

	UINT   uData  = uData1;

	UINT   uMod   = (uData1 + uData2) % sizeof(DWORD);
		
	UINT   uByte  = uMod ? sizeof(DWORD) - uMod : 0;

	for( UINT nPass = 0; nPass < 2; nPass ++ ) {

		while( uData ) {
			
			if( uByte == 0 && uData >= sizeof(DWORD) ) {

				Reg(TxData) = (pData ? HostToMotor(PDWORD(pData)[0]) : 0);

				uData -= sizeof(DWORD);

				pData += sizeof(DWORD);
				
				continue;
				}

			dwData <<= 8;

			dwData  |= (pData ? *pData++ : 0x00);

			uData --;

			if( ++uByte == sizeof(DWORD) ) {

				Reg(TxData) = dwData;

				uByte       = 0;
				}
			}

		if( !nPass ) {

			pData = pData2;

			uData = uData2;
			}
		}
	}

void CEcspi51::ReadFifo(PBYTE pData, UINT uData, UINT uSkip)
{
	UINT uFrame = (uSkip + uData);

	UINT uTotal = (uFrame + sizeof(DWORD) - 1) / sizeof(DWORD);

	UINT uStart = uTotal * sizeof(DWORD) - uData;

	while( (Reg(Test) >> 8) & 0x7F ) {

		DWORD dwFifo = Reg(RxData);

		if( uStart >= sizeof(DWORD) ) {

			uStart -= sizeof(DWORD);
				
			continue;
			}

		DWORD dwData = MotorToHost(dwFifo);

		if( uStart % sizeof(DWORD) ) {				

			PBYTE pBuff = PBYTE(&dwData) + uStart;

			while( uStart % sizeof(DWORD) ) {
					
				*pData ++ = *pBuff ++;

				uStart ++;
				}
				
			uStart = 0;
				
			continue;
			}

		*PDWORD(pData) = dwData;

		pData += sizeof(DWORD);
		}
	}

void CEcspi51::FlushFifo(void)
{
	while( (Reg(Test) >> 8) & 0x7F ) {

		DWORD d = Reg(RxData);
		}
	}

bool CEcspi51::FindDivider(UINT uChan, UINT uFreq)
{
	UINT uErr = NOTHING;

	for( UINT uPod = 0; uPod < 16; uPod ++ ) {

		for( UINT uPre = 0; uPre < 16; uPre ++ ) {

			UINT uClock = m_uClock / ((uPre + 1) * (DWORD(1) << uPod));

			UINT uDelta = uClock > uFreq ? uClock - uFreq : uFreq - uClock;

			if( uDelta < uErr ) {

				uErr = uDelta;

				m_Config[uChan].m_uPod = uPod;

				m_Config[uChan].m_uPre = uPre;
				}
			}
		}

	return uErr != NOTHING;
	}

void CEcspi51::EnableSelect(UINT uChan, bool fEnable)
{
	// Erratum Workaround

	if( Reg(Config) & (1 << (12 + uChan)) ) {

		m_pIoMux->Set(m_uMux[uChan], fEnable ? m_uMuxSel : m_uMuxGpio);
		}
	}

// End of File
