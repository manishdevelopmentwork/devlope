
#include "Intern.hpp"

#include <G3Web.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PNG
//

static BYTE const full_x1_png[] = {
	#include "full-x1.png.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// JSON Template
//

static BYTE const full_x1_json[] = {
	#include "full-x1.json.dat"
	0
	};

//////////////////////////////////////////////////////////////////////////
//
// JSON Versions
//

static char m_Json[3][600];

//////////////////////////////////////////////////////////////////////////
//
// File Table
//

static CWebFileData m_Files[4] = {

	{ "png",  "/png/full-x1.png", full_x1_png, sizeof(full_x1_png) },
	
	{ "json", "/json/full-x1-08.json", NULL, 0 },
	{ "json", "/json/full-x1-16.json", NULL, 0 },
	{ "json", "/json/full-x1-24.json", NULL, 0 },

	};

//////////////////////////////////////////////////////////////////////////
//
// File Addition
//

global	void	AddWebFiles(void)
{
	for( UINT n = 0; n < elements(m_Files); n++ ) {

		if( n > 0 ) {

			m_Files[n].m_pData = PBYTE(m_Json[n-1]);

			SPrintf(m_Json[n-1], PCTXT(full_x1_json), 8 * n);

			m_Files[n].m_uSize = strlen(m_Json[n-1]) + 1;
			}

		AddWebFile(m_Files + n);
		}
	}

global	PCTXT	GetWebRemoteInfo(int cx, int cy, int zoom, int bits)
{
	static char sName[64];

	zoom = 1;

	SPrintf(sName, "/assets/json/full-x%u-%2.2u.json", zoom, bits);

	return sName;
	}

// End of File
