
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Indexed List of Items with Names
//

// Runtime Class

AfxImplementRuntimeClass(CNamedList, CItemIndexList);

// Static Data

CString CNamedList::m_Disambig = L"_";

// Constructor

CNamedList::CNamedList(void)
{
	}

CNamedList::CNamedList(CLASS Class) : CItemIndexList(Class)
{
	}

// Destructor

CNamedList::~CNamedList(void)
{
	}

// Attributes

INDEX CNamedList::IndexFromName(CString Name) const
{
	INDEX Index = m_Names.FindName(Name);

	if( m_Names.Failed(Index) ) {

		return NULL;
		}

	return m_Names.GetData(Index);
	}

CItem * CNamedList::FindByName(CString Name) const
{
	INDEX Index = m_Names.FindName(Name);

	if( m_Names.Failed(Index) ) {

		return NULL;
		}

	return m_List[m_Names.GetData(Index)];
	}

// Operations

void CNamedList::MakeUnique(CString &Name)
{
	if( FindByName(Name) ) {

		UINT c = Name.GetLength();

		UINT d = m_Disambig.GetLength();

		UINT n = 0;

		while( isdigit(Name[c - 1 - n]) ) {

			n++;
			}

		if( Name.Mid(c - n - d, d) == m_Disambig ) {
			
			Name = Name.Left(c - n - d);
			}

		CString r = Name + m_Disambig;

		UINT    v = 0;

		for(;;) { 

			Name = r + CPrintf(L"%u", ++v);
			
			if( !FindByName(Name) ) {

				break;
				}
			}
		}
	}

BOOL CNamedList::ItemRenamed(CItem *pItem)
{
	INDEX Index = FindItemIndex(pItem);

	if( !m_List.Failed(Index) ) {

		ItemRenamed(Index);

		return TRUE;
		}

	return FALSE;
	}

void CNamedList::ItemRenamed(INDEX Index)
{
	CMetaItem *pMeta = (CMetaItem *) m_List[Index];

	CString    Name  = pMeta->GetName();

	INDEX      Find  = m_Names.FindData(Index);

	if( !m_Names.Failed(Find) ) {

		m_Names.Remove(Find);
		}

	if( !Name.IsEmpty() ) {

		m_Names.Insert(Name, Index);
		}
	}

void CNamedList::RemakeIndex(void)
{
	m_Names.Empty();

	MakeIndex();
	}

// Persistance

void CNamedList::PostLoad(void)
{
	CItemIndexList::PostLoad();

	MakeIndex();
	}

// Index Management

void CNamedList::AllocIndex(CItem *pItem, INDEX Index)
{
	CMetaItem *pMeta = (CMetaItem *) pItem;

	CString    Name  = pMeta->GetName();

	if( Name.GetLength() ) {

		m_Names.Insert(Name, Index);
		}

	CItemIndexList::AllocIndex(pItem, Index);
	}

void CNamedList::FreeIndex(CItem *pItem, INDEX Index)
{
	CMetaItem *pMeta = (CMetaItem *) pItem;

	if( pMeta->HasName() ) {

		CString Name = pMeta->GetName();

		m_Names.Remove(Name);
		}

	CItemIndexList::FreeIndex(pItem, Index);
	}

// Implementation

BOOL CNamedList::MakeIndex(void)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CMetaItem *pMeta = (CMetaItem *) m_List[Index];

		CString   Name   = pMeta->GetName();

		if( Name.GetLength() ) {

			m_Names.Insert(Name, Index);
			}

		m_List.GetNext(Index);
		}

	return TRUE;
	}

// End of File
