
#include "Intern.hpp"

#include "Rm25Cxx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Adesto Rm25Cxx SPI EEPROM
//

// Instantiator

IDevice * Create_Rm25Cxx(UINT uChan)
{
	CRm25Cxx *pDevice = New CRm25Cxx(uChan);

	pDevice->Open();

	return pDevice;
	}

// Constructor

CRm25Cxx::CRm25Cxx(UINT uChan)
{
	StdSetRef();

	m_uChan = uChan;

	AfxGetObject("spi", 0, ISpi, m_pSpi); 
	}

// Destructor

CRm25Cxx::~CRm25Cxx(void)
{
	m_pSpi->Release();
	}

// IUnknown

HRESULT CRm25Cxx::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ISerialMemory);

	return E_NOINTERFACE;
	}

ULONG CRm25Cxx::AddRef(void)
{
	StdAddRef();
	}

ULONG CRm25Cxx::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CRm25Cxx::Open(void)
{
	CSpiConfig Config;

	Config.m_uFreq      = 1600000;
	Config.m_fClockIdle = false;
	Config.m_fDataIdle  = false;
	Config.m_uSelect    = 0;
	Config.m_uClockPol  = 0;
	Config.m_uClockPha  = 0;
	Config.m_fBurst	    = false;
	
	m_pSpi->Init(m_uChan, Config);
	
	return TRUE;
	}

// ISerialMemory

UINT METHOD CRm25Cxx::GetSize(void)
{
	return 8192;
	}

BOOL METHOD CRm25Cxx::IsFast(void)
{
	return FALSE;
	}

BOOL METHOD CRm25Cxx::GetData(UINT uAddr, PBYTE pData, UINT uCount)
{
	while( uCount ) {

		UINT uMax  = 32;

		UINT uLump = Min(uMax, uCount);

		BYTE bCmd[3];

		bCmd[0] = 0x03;

		bCmd[1] = HIBYTE(WORD(uAddr));

		bCmd[2] = LOBYTE(WORD(uAddr));

		if( m_pSpi->Recv(m_uChan, bCmd, sizeof(bCmd), pData, uLump) ) {

			uCount -= uLump;

			uAddr  += uLump;

			pData  += uLump;

			continue;
			}

		return FALSE;
		}
	
	return TRUE;
	}

BOOL METHOD CRm25Cxx::PutData(UINT uAddr, PCBYTE pData, UINT uCount)
{
	while( uCount ) {

		if( WriteEnable(true) ) {
					
			UINT uMax  = 32;

			BYTE bAddr = LOBYTE(uAddr) & 0x1F;

			UINT uLump = Min(uCount, uMax - bAddr);

			BYTE bCmd[3];

			bCmd[0] = 0x02;

			bCmd[1] = HIBYTE(WORD(uAddr));

			bCmd[2] = LOBYTE(WORD(uAddr));
			
			if( m_pSpi->Send(m_uChan, bCmd, sizeof(bCmd), pData, uLump) ) {

				uCount -= uLump;

				uAddr  += uLump;

				pData  += uLump;

				while( !IsWriteDone() ) {
					
					Sleep(5);
					}

				continue;
				}

			return FALSE;
			}

		return FALSE;
		}

	return TRUE;
	}

// Implementation

bool CRm25Cxx::WriteEnable(bool fEnable)
{	
	BYTE bCmd = fEnable ? 0x06 : 0x04; 

	return m_pSpi->Send(m_uChan, &bCmd, sizeof(bCmd), NULL, 0);
	}

bool CRm25Cxx::IsWriteDone(void)
{
	BYTE bStatus = 0;

	if( ReadStatusReg(bStatus) ) {

		return (bStatus & Bit(0)) == 0;
		}

	return false;
	}

bool CRm25Cxx::ReadStatusReg(BYTE &Reg)
{
	BYTE bCmd    = 0x05;

	BYTE bStatus = 0;

	if( m_pSpi->Recv(m_uChan, &bCmd, sizeof(bCmd), &bStatus, sizeof(bStatus)) ) {

		Reg = bStatus;

		return true;
		}

	return false;
	}

// End of File
