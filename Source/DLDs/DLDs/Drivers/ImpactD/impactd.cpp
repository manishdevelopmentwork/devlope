
#include "intern.hpp"

#include "impactd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Impact Data Driver
//

// Timeouts

static UINT const timeSendTimeout  = 5000;

static UINT const timeSendDelay	   = 10;

static UINT const timeRecvDelay	   = 10;

static UINT const timeBuffDelay    = 100;

// Instantiator

INSTANTIATE(CImpactData);

// Constructor

CImpactData::CImpactData(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;

	m_pHelper = NULL;
	}

// Srcructor

CImpactData::~CImpactData(void)
{
	}

// Configuration

void MCALL CImpactData::Load(LPCBYTE pData)
{
	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);
	
	if ( GetWord( pData ) == 0x1234 ) {
		

		}
	}
	
// Management

void MCALL CImpactData::Attach(IPortObject *pPort)
{

	}

void MCALL CImpactData::Open(void)
{
	}

// Device

CCODE MCALL CImpactData::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {
			
			m_pCtx = new CContext;

			m_pCtx->m_IP	 = GetAddr(pData);
			m_pCtx->m_uPort	 = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);

			m_pCtx->m_uLast  = GetTickCount();
			m_pCtx->m_pSock  = NULL;

			m_pCtx->m_pNoteBook = NULL;

			m_pCtx->m_uNotes = GetWord(pData);

			if(m_pCtx->m_uNotes > 0 ) {

				m_pCtx->m_pNoteBook = new ImpAddr[m_pCtx->m_uNotes];
				}

			for( UINT u = 0; u < m_pCtx->m_uNotes; u++ ) {

				m_pCtx->m_pNoteBook[u].m_Type = GetByte(pData);

				m_pCtx->m_pNoteBook[u].m_Refs = GetLong(pData);

				PTXT pText = GetString(pData);

				strcpy(m_pCtx->m_pNoteBook[u].m_Text, pText);
				}

			memset(m_pCtx->m_GenError, 0, sizeof(m_pCtx->m_GenError));

			memset(m_pCtx->m_SynError, 0, sizeof(m_pCtx->m_SynError));

			memset(m_pCtx->m_DataError, 0, sizeof(m_pCtx->m_DataError));

			//ShowNotes();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CImpactData::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {  
		CloseSocket(FALSE);

		delete m_pCtx->m_pNoteBook;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CImpactData::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CImpactData::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( ReadError(Addr, pData, uCount) ) {

		return uCount;
		}

	if( !CanRead(Addr) ) {

		return 1;
		}

	if( OpenSocket() ) {

		if( IsCameraOnline(Addr, uCount) ) {

			if( Transact(FALSE) ) {

				return GetReadCmd(Addr, pData, uCount);
				}

			ReportGeneralError(Addr.m_Ref);
			}

		if( HTTPRead(Addr, uCount) ) {

			if( Transact() ) {

				return GetReadData(Addr, pData, uCount);
				}

			ReportGeneralError(Addr.m_Ref);
			}

		CloseSocket(FALSE);
		}
		
	return CCODE_ERROR;
	}

CCODE MCALL CImpactData::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( RunTaskOrTool(Addr, uCount, pData) ) {

			if( Transact() ) {

				return RunSuccess(uCount, Addr.m_Ref);
				}
			}

		else if( TriggerCamera(Addr, uCount, pData) ) {

			if( !uCount ) {

				return 1;
				}

			if( Transact() ) {

				return TriggerSuccess(uCount, Addr.m_Ref);
				}
			}

		else if( SetCameraOnline(Addr,  uCount, pData) ) {

			if( !uCount ) {

				return 1;
				}

			if( Transact(FALSE) ) {

				return OnlineSuccess(uCount, Addr.m_Ref);
				}
			}

		else if( SetCameraOffline(Addr,  uCount, pData) ) {

			if( !uCount ) {

				return 1;
				}

			if( Transact(FALSE) ) {

				return OfflineSuccess(uCount, Addr.m_Ref);
				}
			}

		else if( Abort(Addr,  uCount, pData) ) {

			if( !uCount ) {

				return 1;
				}

			if( Transact(FALSE) ) {

				return AbortSuccess(uCount, Addr.m_Ref);
				}
			}

		else if( HTTPWrite(Addr, uCount, pData) ) {

			if( Transact() ) {

				return WriteSuccess(uCount, Addr.m_Ref);
				}
			}

		CloseSocket(FALSE);
		}	

	return CCODE_ERROR;
	}


// Socket Management

BOOL CImpactData::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {
			
					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CImpactData::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock  = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CImpactData::RecvFrame(BOOL fXML)
{
	BOOL  fOk   = FALSE;

	UINT  uPos  = 0;
	
	UINT uBytes = 48 + 200 * 32;
				
	PBYTE pWork = new BYTE[uBytes];

	memset(pWork, 0, uBytes);

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		UINT n = uBytes - uPos;
				
		if( m_pCtx->m_pSock->Recv(pWork + uPos, n) == S_OK ) {

			if( !fOk ) {
						
				for( UINT o = 0; o < n; o++ ) {

					if( fOk ) {

						if( CheckRecv(HEAD, MotorToHost(PU4(pWork + o)[0])) ) {

							if( CheckRecv(TAIL, MotorToHost(PU4(pWork + uPos + n - 7)[0])) ) {

								UINT uBegin = Find(MARK, pWork, uPos + n);

								if( uBegin ) {

									m_uRx = uPos + n - uBegin;

									memcpy(m_bRxBuff, pWork + uBegin, m_uRx);

									delete pWork;

									return TRUE;
									}

								delete pWork;

								return FALSE;
								}
								
							uPos += n;

							continue;
							}

						if( CheckRecv(FAIL, MotorToHost(PU4(pWork + o)[0])) ) {

							// Non-XML general error !

							m_uRx = n - o;

							memcpy(m_bRxBuff, pWork + uPos + o, m_uRx);

							delete pWork;
						
							return FALSE;
							}

						if( !fXML ) {

							// Non-XML reply expected !

							PBYTE pText = NULL;

							for( UINT u = n; u > 0; u-- ) {

								if( pWork[u] == '.' ) {

									pText = PBYTE(pWork + u + 5);

									m_uRx = n - u;

									memcpy(m_bRxBuff, pText, m_uRx);

									delete pWork;
						
									return TRUE;
									}
								}

							// Non-XML general error !

							m_uRx = n - o;

							memcpy(m_bRxBuff, pWork + uPos + o, m_uRx);

							delete pWork;

							return FALSE;							
							}
						}
						
					if(pWork[o] == 'O' && pWork[o+1] == 'K') {

						fOk = TRUE;
						}
					}
				}
							
			else {	
				if( uPos == 0 ) {

					if( !CheckRecv(HEAD, MotorToHost(PU4(pWork + 1)[0])) ) {

						// copy anyway for general error reporting !

						m_uRx = n;

						memcpy(m_bRxBuff, pWork, m_uRx);

						delete pWork;
						
						return FALSE;
						}
					}

				if( CheckRecv(TAIL, MotorToHost(PU4(pWork + uPos + n - 7)[0])) ) {

					UINT uBegin = Find(MARK, pWork, uPos + n);

					if( uBegin ) {

						m_uRx = uPos + n - uBegin;

						memcpy(m_bRxBuff, pWork + uBegin, m_uRx);

						delete pWork;

						return TRUE;
						}

					delete pWork;

					return FALSE;
					}
								
				uPos += n;

				continue;
				}
			}
		
		if( !CheckSocket() ) {

			break;
			}

		Sleep(timeRecvDelay);
		}

	delete pWork;

	return FALSE;
	}

BOOL CImpactData::CheckRecv(DWORD dwCheck, DWORD dwGot)
{
	return dwCheck == dwGot;
	}

UINT CImpactData::Find(DWORD dwFind, PBYTE pData, UINT uBytes)
{
	BOOL fFound = FALSE;

	for( UINT u = 0; u < uBytes; u++ ) {

		if( fFound ) {

			if( pData[u] == '<' ) {

				return u;
				}

			continue;
			}

		DWORD dwGot = MotorToHost(PU4(pData + u)[0]);

		if( dwGot == dwFind ) {

			fFound = TRUE;
			}
		}

	return 0;
	}

BOOL CImpactData::Transact(BOOL fXML)
{
	if( Send(m_pCtx->m_pSock, m_pCtx->m_Http, PTXT(&m_pCtx->m_Http + 1)) ) {

		if( RecvFrame(fXML) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CImpactData::Send(ISocket *pSock, PCTXT pText, PTXT pArgs)
{
	UINT uSize = strlen(pText);

	PTXT pWork = new char [uSize + 256];

	SPrintf(pWork, pText, pArgs);

	//AfxTrace("\nTX : %s", pWork);

	if( Send(pSock, PCBYTE(pWork), strlen(pWork)) ) {

		delete pWork;

		return TRUE;
		}

	delete pWork;

	return FALSE;
	}

BOOL CImpactData::Send(ISocket *pSock, PCBYTE pText, UINT uSize)
{
	UINT uLimit = 1280;

	while( uSize ) {

		CBuffer *pBuff = CreateBuffer(uLimit, TRUE);

		if( pBuff ) {
			
			PBYTE pData = pBuff->GetData();

			UINT  uCopy = min(uLimit, uSize);

			memcpy(pData, pText, uCopy);

			pBuff->AddTail(uCopy);  

			SetTimer(timeSendTimeout);

			while( pSock->Send(pBuff) == E_FAIL ) {

				UINT Phase;

				pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN && GetTimer() ) {

					Sleep(timeSendDelay);

					continue;
					}

				BuffRelease(pBuff);
				
				return FALSE;
				}

			pText += uCopy;

			uSize -= uCopy;
			}
		else {  
			Sleep(timeBuffDelay);

			return FALSE;
			}
		}

	return TRUE;
	}

// Implementation

BOOL CImpactData::HTTPRead(AREF Addr, UINT uCount)
{
	// NOTE: In order to properly support multiple runs as implemented below, deletion of database mappings must be handed down to the driver configuration.

	UINT Count = 1;

	memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

	strcpy(m_pCtx->m_Http, "GET /getData/");

	if( FindText(Addr.m_Ref, Count, Addr.a.m_Table) ) {

		strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::IsCameraOnline(AREF Addr, UINT uCount)
{
	if( FindNoteType(Addr.m_Ref) == tIsOnline ) {

		// NOTE: In order to properly support multiple runs as implemented below, deletion of database mappings must be handed down to the driver configuration.

		UINT Count = 1;

		memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

		strcpy(m_pCtx->m_Http, "GET /IsOnline/");

		strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::HTTPWrite(AREF Addr, UINT& uCount, PDWORD pData)
{
	// NOTE: In order to properly support multiple runs as implemented below, deletion of database mappings must be handed down to the driver configuration.

	BOOL fString = Addr.a.m_Table >= 0x20;

	MakeMin(uCount, fString ? 16 : 1);
	
	memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

	strcpy(m_pCtx->m_Http, "GET /setData/");

	PTXT Type = "        ";

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	Type = "Boolean";					break;
		case addrRealAsReal:	Type = "real";						break;
		case addrLongAsLong:	Type = ( fString ) ? PTXT("string") : PTXT("integer");	break;
		}

	UINT uSingle = 1;

	for( UINT u = 0; u < uCount; u++ ) {

		if( FindText(Addr.m_Ref + u, uSingle, Addr.a.m_Table) ) {

			char pValue[128];

			if( Addr.a.m_Type == addrBitAsBit ) {

				SPrintf(pValue, "=(%s)%s", Type, pData[u] ? "true" : "false");
				}

			else if( Addr.a.m_Type == addrRealAsReal ) {

				SPrintf(pValue, "=(%s)%f", Type, pData[u]);
				}

			else if( fString ) {

				char pEnc[108];

				Encode(PCTXT(pData), pEnc);

				SPrintf(pValue, "=(%s)%s", Type, pEnc);
				}
			else {
				SPrintf(pValue, "=(%s)%u", Type, pData[u]);
				}

			strcat(m_pCtx->m_Http, pValue);

			if ( fString ) {

				u += 16;
				}

			if( u < uCount - 1 ) {

				strcat(m_pCtx->m_Http, ",");
				}

			continue;
			}

		MakeMin(uCount, u);

		break;
		}

	if( uCount ) {

		strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

		return TRUE;
		}

	
	return FALSE;
	}

BOOL CImpactData::RunTaskOrTool(AREF Addr, UINT& uCount, PDWORD pData)
{
	// NOTE: In order to properly support multiple runs as implemented below, deletion of database mappings must be handed down to the driver configuration.

	if( FindNoteType(Addr.m_Ref) == tRun ) {

		MakeMin(uCount, 1);

		memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

		strcpy(m_pCtx->m_Http, "GET /run/");

		BOOL fComma = FALSE;

		for( UINT u = 0; u < uCount; u++ ) {

			if( FindNoteType(Addr.m_Ref + u) == tRun ) {

				if( fComma ) {
				 
					strcat(m_pCtx->m_Http, ",");
					}
				
				if( FindText(Addr.m_Ref + u, 1, Addr.a.m_Table) ) {

					fComma = TRUE;

					continue;
					}
				}

			MakeMin(uCount, u);

			break;
			}

		if( uCount ) {

			strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CImpactData::TriggerCamera(AREF Addr, UINT& uCount, PDWORD pData)
{
	if( FindNoteType(Addr.m_Ref) == tTrig ) {

		MakeMin(uCount, 1);

		memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

		strcpy(m_pCtx->m_Http, "GET /trigger/");

		if( pData[0] ) {

			if( Addr.a.m_Type == addrLongAsLong ) {

				char pValue[32];

				SPrintf(pValue, "id=%u", pData[0]);

				strcat(m_pCtx->m_Http, pValue);	
				}
			
			strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

			return TRUE;
			}

		uCount = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::SetCameraOnline(AREF Addr, UINT& uCount, PDWORD pData)
{
	if( FindNoteType(Addr.m_Ref) == tOnline ) {

		MakeMin(uCount, 1);

		memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

		strcpy(m_pCtx->m_Http, "GET /Online/");

		if( pData[0] ) {

			strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

			return TRUE;
			}

		uCount = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::SetCameraOffline(AREF Addr, UINT& uCount, PDWORD pData)
{
	if( FindNoteType(Addr.m_Ref) == tOffline ) {

		MakeMin(uCount, 1);

		memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

		strcpy(m_pCtx->m_Http, "GET /Offline/");

		if( pData[0] ) {

			strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

			return TRUE;
			}

		uCount = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::Abort(AREF Addr, UINT& uCount, PDWORD pData)
{
	if( FindNoteType(Addr.m_Ref) == tAbort ) {

		MakeMin(uCount, 1);

		memset(m_pCtx->m_Http, '\0', sizeof(m_pCtx->m_Http));

		strcpy(m_pCtx->m_Http, "GET /Abort/");

		if( pData[0] ) {

			strcat(m_pCtx->m_Http, " HTTP/1.1\r\nAccept: application/xaml+xml\r\nAccept-Language: en-us\r\nConnection: keep-alive\r\n\r\n");

			return TRUE;
			}

		uCount = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::FindText(DWORD dwAddr, UINT uCount, UINT uTable)
{
	for( UINT u = 0; u < uCount; u++, dwAddr++ ) {

		UINT uType = FindNoteType(dwAddr);

		if( uType < NOTHING ) {

			if( u ) {

				strcat(m_pCtx->m_Http, ",");
				}

			if( uType == tObject ) {

				PTXT pText = "Vision%%20System.";

				strcat(m_pCtx->m_Http, pText);
				}

			strcat(m_pCtx->m_Http, FindNoteText(dwAddr));

			if( IsString(uTable) ) {

				dwAddr += 16;
				}

			continue;
			}
		
		break;
		}

	return uCount ? TRUE : FALSE;
	}

PCTXT CImpactData::FindNoteText(DWORD dwAddr)
{
	for( UINT u = 0; u < m_pCtx->m_uNotes; u++ ) {

		if( m_pCtx->m_pNoteBook[u].m_Refs == dwAddr ) {

			return m_pCtx->m_pNoteBook[u].m_Text;
			}
		}

	return PTXT("");
	}

UINT CImpactData::FindNoteType(DWORD dwAddr)
{
	for( UINT u = 0; u < m_pCtx->m_uNotes; u++ ) {

		if( m_pCtx->m_pNoteBook[u].m_Refs == dwAddr ) {

			return m_pCtx->m_pNoteBook[u].m_Type;
			}
		}

	return NOTHING;
	}

UINT CImpactData::FindNoteIndex(DWORD dwAddr)
{
	for( UINT u = 0; u < m_pCtx->m_uNotes; u++ ) {

		if( m_pCtx->m_pNoteBook[u].m_Refs == dwAddr ) {

			return u;
			}
		}

	return NOTHING;
	}

UINT CImpactData::FindValueField(UINT uPos)
{
	for( UINT u = uPos; u < m_uRx; u++ )  {

		if( !memcmp(m_bRxBuff + u, "<val>", 5) ) {

			return u + 5;
			}
		}

	return NOTHING;
	}

UINT CImpactData::FindBooleanField(PCTXT Text)
{
	PTXT pBool = FindField(Text);

	PTXT pFormat = "<Boolean portname=\"%s\">";

	char pValue[128];

	SPrintf(pValue, pFormat, pBool);

	for( UINT u = 0; u < m_uRx; u++ ) {

		if( !memcmp(m_bRxBuff + u, pValue, strlen(pValue)) ) {

			return u + strlen(pValue);		
			}
		}

	return 0;
	}

UINT CImpactData::FindStringField(PCTXT Text)
{
	PTXT pStr = FindField(Text);

	PTXT pFormat = "<String portname=\"%s\">";

	char pValue[128];

	SPrintf(pValue, pFormat, pStr);

	for( UINT u = 0; u < m_uRx; u++ ) {

		if( !memcmp(m_bRxBuff + u, pValue, strlen(pValue)) ) {

			return u + strlen(pValue);		
			}
		}

	return 0;
	}

BOOL CImpactData::RecvReal(UINT uPos)
{
	for( UINT u = uPos; u > 0; u-- ) {

		if( m_bRxBuff[u] == '<' ) {

			return FALSE;
			}

		if( !memcmp(m_bRxBuff + u, "Real", 4) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CImpactData::Decode(PCTXT pText, PTXT pField)
{ 
	PTXT pStr [] = { "%%20", "%%3D", "%%2C" };

	PTXT pRep [] = { " "   , "="   , ","    };

	UINT uChars  = strlen(pText);

	PTXT pFind = PTXT(pText);

	UINT uMark = 0;

	UINT uIndex = 0;

	while( *pFind != ':' && *pFind != '\0' && uChars ) {

		if( *pFind == '.' ) {

			uMark = uIndex;
			}

		uIndex++;

		pFind++;

		uChars--;
		}

	if( uChars == 0 && uMark != uIndex ) {

		pFind = PTXT(pText + uMark);

		uChars = uIndex - uMark;
		}

	pFind++;

	strcpy(pField, pFind);

	if( uChars && pField ) {

		for( UINT u = 0; u < elements(pStr); u++ ) {

			uChars = strlen(pField);

			pFind  = pField;

			while( *pFind != '\0' ) {

				if( !memcmp(pFind, pStr[u], 4) ) {

					memset(pFind, pRep[u][0], 1);
					
					memcpy(pFind + 1, pFind + 4, uChars - 3);

					memset(pFind + uChars - 3, '\0', 1);
					}

				pFind++;

				uChars--;
				}
			}
		}
	
	return TRUE;
	}

BOOL CImpactData::Encode(PCTXT pText, PTXT pEncode)
{
	PTXT pRep [] = { "%%20", "%%3D", "%%2C" };

	PTXT pStr [] = { " "   , "="   , ","    };

	UINT uLimit = 64;

	UINT uChar = 0;

	PCTXT pFind  = pText;

	while( *pFind != '\0' && uChar < uLimit ) { 

		BOOL fRep = FALSE;

		DWORD Test = PDWORD(pFind)[0];

		if( Test == 0x20202020 ) {

			pEncode[uChar] = '\0';

			return TRUE;
			}

		for( UINT u = 0; u < elements(pStr); u++ ) {

			if( *pFind == *pStr[u] ) {

				memcpy(PBYTE(pEncode + uChar), pRep[u], strlen(pRep[u]));

				uChar += strlen(pRep[u]);

				uLimit += (strlen(pRep[u]) - 1);

				fRep = TRUE;

				break;
				}
			}

		if( !fRep ) {

			pEncode[uChar] = *pFind;

			uChar++;
			}

		pFind++;
		}

	pEncode[uChar] = '\0';

	return TRUE;
	}

BOOL CImpactData::CanRead(AREF Addr)
{
	UINT uNote = FindNoteType(Addr.m_Ref);

	return (( uNote <= tTask ) || ( uNote == tUser ) || (uNote == tIsOnline));
	}

BOOL CImpactData::ReadError(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uNote = FindNoteType(Addr.m_Ref);

	if( uNote == tGen ) {

		MakeMin(uCount, sizeof(m_pCtx->m_GenError) / 4); 

		memcpy(pData, m_pCtx->m_GenError, uCount * 4);

		return TRUE;
		}

	if( uNote == tSyn ) {

		MakeMin(uCount, sizeof(m_pCtx->m_SynError) / 4); 

		memcpy(pData, m_pCtx->m_SynError, uCount * 4);

		return TRUE;
		}

	if( uNote == tData ) {

		MakeMin(uCount, sizeof(m_pCtx->m_DataError) / 4); 

		memcpy(pData, m_pCtx->m_DataError, uCount * 4);

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::CheckStrEnd(PDWORD pData)
{
	PBYTE pByte = PBYTE(pData);
	
	for( UINT u = 0; u < sizeof(DWORD); u++, pByte++ ) {

		if( *pByte == '<' ) {

			if( *(pByte + 1) == '/' ) {

				*pByte = '\0';

				return TRUE;
				}

			if( *(pByte - 1) == '/' ) {

				*pByte = '\0';

				return TRUE;
				}

			if( u == sizeof(DWORD) - 1 ) {

				*pByte = '\0';

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CImpactData::IsBoolean(UINT uType)
{
	return (uType == addrBitAsBit);
	}

BOOL CImpactData::IsString(UINT uTable)
{
	return (uTable >= 0x20);
	}

BOOL CImpactData::IsLong(UINT uType, UINT uTable)
{
	if( !IsString(uTable) ) {

		return ( (uType == addrLongAsLong) || (uType == addrRealAsReal) );
		}
		
	return FALSE;
	}

BOOL CImpactData::HandleBoolean(PCTXT Text, DWORD& Data)
{
	UINT uBit = FindBooleanField(Text);

	if( uBit ) {

		Data = !memcmp(PCTXT(m_bRxBuff + uBit), "true", 4) ? 1 : 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::HandleString(PCTXT Text, PDWORD pData, UINT& Index, UINT uCount)
{
	UINT uStr = FindStringField(Text);

	if( uStr ) {

		UINT uChars = min((uCount - Index) * 4, 68);

		UINT u = Index;

		for( UINT s = 0; s < uChars; s+=4, u++ ) { 

			DWORD x   = PU4(m_bRxBuff + uStr + s)[0];

			pData[u]  = MotorToHost(x);

			if( CheckStrEnd(&pData[u]) ) {

				break;
				}
			}

		Index += 16;

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CImpactData::HandleLong(UINT uType, PCTXT Text, DWORD& Data)
{
	PTXT pField = FindField(Text);

	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, pField, strlen(pField)) ) {

			//BOOL fReal = RecvReal(n);

			UINT uPtr = FindValueField(n);

			if( uPtr < NOTHING ) {

				if( uType == addrLongAsLong ) {

					Data = ATOI(PTXT(m_bRxBuff + uPtr));

					return TRUE;
					}

				Data = ATOF(PTXT(m_bRxBuff + uPtr));

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

PTXT CImpactData::FindField(PCTXT Text)
{
	PTXT pTest  = PTXT(Text);

	PTXT pField = PTXT(Text);

	while( *pTest ) {

		if( *pTest == '.' ) {

			strcpy(pField, PCTXT(pTest + 1));
			}

		pTest++;
		}

	return pField;	
	}

CCODE CImpactData::GetReadData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT u;

	for( u = 0; u < uCount; u++ ) {

		PCTXT Note = FindNoteText(Addr.m_Ref + u);

		UINT uChar = strlen(Note);

		PTXT Text = new char[uChar];

		if( Decode(Note, Text) && Text ) {

			if( IsBoolean(Addr.a.m_Type) ) {

				// NOTE: In order to properly support multiple runs as implemented below, deletion of database mappings must be handed down to the driver configuration.

				MakeMin(uCount, 1);

				if( HandleBoolean(Text, pData[u]) ) {

					delete Text;

					continue;
					}

				delete Text;

				return u;
				}

			if( IsString(Addr.a.m_Table) ) {

				if( HandleString(Text, pData, u, uCount) ) {

					delete Text;

					continue;
					}

				delete Text;

				return u;
				}

			if( HandleLong(Addr.a.m_Type, Text, pData[u]) ) {

				// NOTE: In order to properly support multiple runs as implemented below, deletion of database mappings must be handed down to the driver configuration.

				MakeMin(uCount, 1);

				delete Text;

				continue;
				}
			}

		delete Text;

		return u;
		}

	return u;
	}

CCODE CImpactData::GetReadCmd(AREF Addr, PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = !memcmp(m_bRxBuff, "True", 4) ? 1 : 0;
		}

	return uCount;
	}

CCODE CImpactData::WriteSuccess(UINT uCount, DWORD dwAddr)
{
	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, "<SetDataError>", 14) ) {

			if( !memcmp(m_bRxBuff + n + 14, "Ok", 2) ) {

				return uCount;
				}

			ReportDataError(dwAddr);
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CImpactData::RunSuccess(UINT uCount, DWORD dwAddr)
{
	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, "<SyntaxError>", 13) ) {

			if( !memcmp(m_bRxBuff + n + 13, "Ok", 2) ) {

				return uCount;
				}

			ReportSyntaxError(dwAddr);
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CImpactData::TriggerSuccess(UINT uCount, DWORD dwAddr)
{
	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, "<Error>", 7) ) {

			if( !memcmp(m_bRxBuff + n + 7, "Ok", 2) ) {

				return uCount;
				}

			ReportGeneralError(dwAddr);	
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CImpactData::OnlineSuccess(UINT uCount, DWORD dwAddr)
{
	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, "System Online!", 14) ) {

			return uCount;
			}

		ReportGeneralError(dwAddr);		
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CImpactData::OfflineSuccess(UINT uCount, DWORD dwAddr)
{
	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, "System Offline!", 15) ) {

			return uCount;
			}

		ReportGeneralError(dwAddr);		
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CImpactData::AbortSuccess(UINT uCount, DWORD dwAddr)
{
	for( UINT n = 0; n < m_uRx; n++ ) {

		if( !memcmp(m_bRxBuff + n, "Abort!", 6) ) {

			return uCount;
			}

		ReportGeneralError(dwAddr);		
		}

	return CCODE_ERROR | CCODE_HARD;
	}

BOOL CImpactData::ReportGeneralError(DWORD dwAddr)
{
	if( m_bRxBuff[0] != '<' ) {

		memset(m_pCtx->m_GenError, 0, sizeof(m_pCtx->m_GenError));

		UINT uCount = min(m_uRx, sizeof(m_pCtx->m_GenError));

		for( UINT u = 0; u < uCount; u+=4 ) {

			DWORD x = PU4(m_bRxBuff + u)[0];

			x = MotorToHost(x);

			memcpy(PBYTE(&m_pCtx->m_GenError[u]), PBYTE(&x), 4);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CImpactData::ReportSyntaxError(DWORD dwAddr)
{
	UINT uMark = 0;

	//UINT uIndex = FindNoteIndex(dwAddr);

	for( UINT s = 0; s < m_uRx; s++ ) {

		if( uMark ) {

			if( m_bRxBuff[s] == '<' ) {

				m_bRxBuff[s] = '\0';

				memset(m_pCtx->m_SynError, 0, sizeof(m_pCtx->m_SynError));

				for( UINT u = 0; u < s - uMark; u+=4 ) {

					DWORD x = PU4(m_bRxBuff + u + uMark)[0];

					x = MotorToHost(x);

					memcpy(PBYTE(&m_pCtx->m_SynError[u]), PBYTE(&x), 4);
					}

				return TRUE;
				}
			}


		if( !memcmp(m_bRxBuff + s, "<SyntaxError>", 13) ) {

			s +=12;

			uMark = s + 1;
			}
		}

	return FALSE;
	}

BOOL CImpactData::ReportDataError(DWORD dwAddr)
{
	UINT uMark = 0;

	//UINT uIndex = FindNoteIndex(dwAddr);

	for( UINT d = 0; d < m_uRx; d++ ) {

		if( uMark ) {

			if( m_bRxBuff[d] == '<' ) {

				m_bRxBuff[d] = '\0';

				memset(m_pCtx->m_DataError, 0, sizeof(m_pCtx->m_DataError));

				for( UINT u = 0; u < d - uMark; u+=4 ) {

					DWORD x = PU4(m_bRxBuff + u + uMark)[0];

					x = MotorToHost(x);

					memcpy(PBYTE(&m_pCtx->m_DataError[u]), PBYTE(&x), 4);
					}

				return TRUE;
				}
			}

		if( !memcmp(m_bRxBuff + d, "<SetDataError>", 14) ) {

			d +=12;

			uMark = d + 1;
			}
		}
	
	return FALSE;
	}

// Helpers 

void CImpactData::ShowNotes(void)
{
	AfxTrace("\nNotes %u", m_pCtx->m_uNotes);

	for( UINT u = 0; u < m_pCtx->m_uNotes; u++ ) {	

		AfxTrace("\nAddr %8.8x, Text %s Type %u", m_pCtx->m_pNoteBook[u].m_Refs, m_pCtx->m_pNoteBook[u].m_Text, m_pCtx->m_pNoteBook[u].m_Type);
		}
	}



// End of File
