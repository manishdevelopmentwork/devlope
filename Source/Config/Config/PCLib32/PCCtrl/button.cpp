
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Button Control
//

// Dynamic Class

AfxImplementDynamicClass(CButton, CCtrlWnd);

// Constructor

CButton::CButton(void)
{
	}

// Attributes

BOOL CButton::IsChecked(void) const
{
	return SendMessageConst(BM_GETCHECK) == BST_CHECKED;
	}

UINT CButton::GetState(void) const
{
	return UINT(SendMessageConst(BM_GETSTATE));
	}

HANDLE CButton::GetImage(UINT uType) const
{
	return HANDLE(SendMessageConst(BM_GETIMAGE, WPARAM(uType)));
	}
		
// Operations

void CButton::Click(void)
{
	SendMessage(BM_CLICK);
	}

void CButton::SetCheck(BOOL fCheck)
{
	SendMessage(BM_SETCHECK, fCheck ? BST_CHECKED : BST_UNCHECKED);
	}

void CButton::SetState(BOOL fState)
{
	SendMessage(BM_SETSTATE, WPARAM(fState));
	}

void CButton::SetImage(UINT uType, HANDLE hImage)
{
	SendMessage(BM_SETIMAGE, WPARAM(uType), LPARAM(hImage));
	}

void CButton::SetImage(CBitmap &Bitmap)
{
	SetImage(IMAGE_BITMAP, Bitmap.GetHandle());
	}

void CButton::SetImage(CIcon &Icon)
{
	SetImage(IMAGE_ICON, Icon.GetHandle());
	}

// Handle Lookup

CButton & CButton::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CButton NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CButton &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CButton::GetDefaultClassName(void) const
{
	return L"BUTTON";
	}

// Message Map

AfxMessageMap(CButton, CCtrlWnd)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)

	AfxMessageEnd(CButton)
	};

// Message Handlers

UINT CButton::OnCreate(CREATESTRUCT &Create)
{
	m_fDraw = DrawSelf();

	return AfxCallDefProc();
	}

BOOL CButton::OnEraseBkGnd(CDC &DC)
{
	if( m_fDraw ) {

		return TRUE;
		}
	
	return AfxCallDefProc();
	}

void CButton::OnPaint(void)
{
	if( m_fDraw ) {

		CPaintDC DC(ThisObject);

		DC.Select(afxFont(Dialog));

		CRect Text = GetClientRect();

		CRect Edge = Text;

		Text.left += DC.GetTextExtent(L"X").cx * 1;

		Edge.top  += DC.GetTextExtent(L"X").cy / 2;

		DC.Select(afxPen(3dShadow));

		DC.Select(CGdiObject(NULL_BRUSH));

		DC.RoundRect(Edge, 6);

		DC.Deselect();

		DC.Deselect();

		if( GetWindowTextLength() ) {

			if( afxWin2K ) {

				DC.SetBkColor(afxColor(3dFace));
				}
			else
				DC.SetBkColor(afxColor(TabFace));

			DC.SetTextColor(afxColor(TabGroup));

			DC.DrawText(L" " + GetWindowText() + L" ", Text, 0);
			}

		DC.Deselect();

		return;
		}
	
	AfxCallDefProc();
	}

void CButton::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	DWORD dwStyle = (GetWindowStyle() & BS_TYPEMASK);

	if( dwStyle == BS_RADIOBUTTON || dwStyle == BS_AUTORADIOBUTTON ) {

		CWnd &Parent = GetParent();

		CWnd &Button = Parent.GetDlgItem(IDOK);

		if( Button.IsWindow() && !Button.IsEnabled() ) {

			MessageBeep(0);

			return;
			}

		Parent.PostMessage(WM_COMMAND, IDOK);

		return;
		}

	SendMessage(WM_LBUTTONDOWN, uCode, LPARAM(Pos));
	}

// Implementation

BOOL CButton::DrawSelf(void)
{
	if( (GetWindowStyle() & BS_TYPEMASK) == BS_GROUPBOX ) {

		CLASS Class = AfxObjectClass(GetParent());

		while( Class ) {

			if( wstrstr(Class->GetClassName(), L"Tab") ) {

				return TRUE;
				}

			Class = Class->GetBaseClass();
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
