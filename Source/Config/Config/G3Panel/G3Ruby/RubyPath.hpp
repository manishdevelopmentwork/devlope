
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyPath_HPP
	
#define	INCLUDE_RubyPath_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyMatrix.hpp"

#include "RubyVertex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Path Object
//

class DLLAPI CRubyPath
{
	public:
		// Constructors
		CRubyPath(void);
		CRubyPath(CRubyPath const &That);
		
		// Destructor
		~CRubyPath(void);

		// Assignment
		CRubyPath const & operator = (CRubyPath const &That);

		// Indexing
		CRubyVertex const & operator [] (int n) const;

		// Matrix Access
		CRubyMatrix const & GetMatrix(void) const;

		// Attributes
		bool IsEmpty(void) const;
		int  GetCount(void) const;
		int  GetSubCount(void) const;
		int  GetGdiSubCount(void) const;
		int  GetBreak(int n) const;

		// Operations
		void Empty(void);
		bool Append(number x, number y);
		bool Append(CRubyPoint const &p);
		bool Append(CRubyPoint const &p, bool force);
		bool Append(CRubyPath const &p, int s, int c);
		bool AppendBreak(int mask);
		bool AppendSoftBreak(void);
		bool AppendHardBreak(void);
		bool AppendLineBreak(void);
		bool SoftenLastBreak(void);
		void Reverse(int sub);
		void Transform(CRubyMatrix const &m);
		void SetMatrix(CRubyMatrix const &m);

		// Hit Testing
		bool HitTest(CRubyPoint const &p, int sub) const;
		void GetBoundingRect(R2R &r) const;
		void AddBoundingRect(R2R &r) const;
		void GetBoundingRect(R2 &r) const;
		void AddBoundingRect(R2 &r) const;

		// Debugging
		void Trace(PCTXT pName) const;

	protected:
		// Data Members
		int           m_nCount;
		int	      m_nStart;
		int           m_nAlloc;
		CRubyVertex * m_pv;
		CRubyMatrix   m_m;

		// Implementation
		int GetNewAlloc(void);

		// Intersection Test
		static bool TestIntersection(CRubyPoint const &p1, CRubyPoint const &p2, CRubyPoint const &p3, CRubyPoint const &p4);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Indexing

inline CRubyVertex const & CRubyPath::operator [] (int n) const
{
	return m_pv[n];
	}

// Matrix Access

inline CRubyMatrix const & CRubyPath::GetMatrix(void) const
{
	return m_m;
	}

// Attributes

inline int CRubyPath::GetCount(void) const
{
	return m_nCount;
	}

// Operations

inline bool CRubyPath::IsEmpty(void) const
{
	return !m_nCount;
	}

inline void CRubyPath::Empty(void)
{
	m_nCount = 0;

	m_nStart = 0;
	}

inline bool CRubyPath::Append(number x, number y)
{
	return Append(CRubyPoint(x, y), false);
	}

inline bool CRubyPath::Append(CRubyPoint const &p)
{
	return Append(p, false);
	}

// End of File

#endif
