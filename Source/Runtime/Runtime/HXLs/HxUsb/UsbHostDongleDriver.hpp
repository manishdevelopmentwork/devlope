
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostDongleDriver_HPP

#define	INCLUDE_UsbHostDongleDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncBulkDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Dongle Driver
//

class CUsbHostDongleDriver : public CUsbHostFuncBulkDriver, public IUsbHostDongle
{
	public:
		// Constructor
		CUsbHostDongleDriver(void);

		// Destructor
		~CUsbHostDongleDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostDongle
		BOOL SetLed(UINT uState);
		BOOL Challenge(void);
	
	protected:
		// Commands
		enum 
		{
			cmdSetLed = 0x01,
			};

		// Data
		BYTE m_bHash[16];
		BYTE m_bNonce[16];
		CHAR m_sText[64];

		// MD5 Helpers
		void MakeHash(PCBYTE pData, UINT uSize);
		void MakeHash(PCBYTE pData, UINT uSize, PBYTE pHash);

		// Implementation
		void MakeNonce(void);
		void MakeText1(void);
		void MakeText2(void);
	};

// End of File

#endif
