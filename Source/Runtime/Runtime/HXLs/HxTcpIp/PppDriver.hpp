
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PppDriver_HPP

#define	INCLUDE_PppDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Clases
//

#include "PcapFilterIp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDynDns;

class CPpp;

//////////////////////////////////////////////////////////////////////////
//
// PPP Packet Driver
//

class CPppDriver : public IPacketDriver,
		   public IClientProcess,
		   public IPacketCapture
{
	public:
		// Constructor
		CPppDriver(CConfigPpp const &Config);

		// Destructor
		~CPppDriver(void);

		// Operations
		void LinkActive(BOOL fActive, IPREF DNS);

		// Capture
		void Capture(WORD wProtocol, CBuffer *pBuff, BOOL fSend);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		
		// IPacketDriver
		bool    Bind(IRouter *pRouter);
		bool    Bind(IPacketSink *pSink, UINT uFace);
		CString GetDeviceName(void);
		CString GetStatus(void);
		bool    SetIpAddress(IPREF Ip, IPREF Mask);
		bool	SetIpGateway(IPREF Gate);
		bool    GetMacAddress(MACADDR &Addr);
		bool    GetDhcpOption(BYTE bType, UINT &uValue);
		bool    GetSockOption(UINT uCode, UINT &uValue);
		bool    SetMulticast(IPADDR *pList, UINT uCount);
		bool    Open(void);
		bool    Close(void);
		void    Poll(void);
		bool    Send(IPREF Ip, CBuffer *pBuff);

		// IPacketCapture
		BOOL  METHOD IsCaptureRunning(void);
		PCSTR METHOD GetCaptureFilter(void);
		UINT  METHOD GetCaptureSize(void);
		BOOL  METHOD CopyCapture(PBYTE pData, UINT uSize);
		BOOL  METHOD StartCapture(PCSTR pFilter);
		void  METHOD StopCapture(void);
		void  METHOD KillCapture(void);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

	protected:
		// Data Members
		ULONG	      m_uRefs;
		CConfigPpp    m_Config;
		BOOL          m_fServer;
		CDynDns     * m_pDynDns;
		IRouter     * m_pRouter;
		IPacketSink * m_pSink;
		UINT          m_uFace;
		CPpp        * m_pPpp;
		IEvent	    * m_pDone;
		HTHREAD       m_hThread;
		CIpAddr       m_Dns;
		bool	      m_fCapture;
		CPcapFilterIp m_CaptFilter;
		CPcapBuffer   m_CaptBuffer;
	};

// End of File

#endif
