
#include "Intern.hpp"

#include "Service.hpp"

#include "Matrix.hpp"

#include "MqttClientCrimson.hpp"

#include <sys/time.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudServiceCrimson.hpp"

#include "MqttClientOptionsCrimson.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//

// Constructor

CMqttClientCrimson::CMqttClientCrimson(CCloudServiceCrimson *pService, CMqttClientOptionsCrimson &Opts) : CMqttQueuedClient(Opts), m_Opts(Opts)
{
	m_pService = pService;

	m_uTick    = 0;
}

// Operations

BOOL CMqttClientCrimson::Open(void)
{
	return CMqttQueuedClient::Open();
}

BOOL CMqttClientCrimson::Poll(UINT uID)
{
	if( uID == 1 ) {

		struct timeval tv;

		gettimeofday(&tv, NULL);

		UINT64 k = 1000;

		UINT64 t = tv.tv_sec * k + tv.tv_usec / k;

		if( m_uTick != tv.tv_sec ) {

			for( UINT n = 0; n < m_PubList.GetCount(); n++ ) {

				CPub           &Pub = (CPub &) m_PubList[n];

				CCloudDataSet *pSet = m_pService->GetDataSet(Pub.m_uTopic);

				pSet->Tick();
			}

			m_uTick = tv.tv_sec;
		}

		for( UINT n = 0; n < m_PubList.GetCount(); n++ ) {

			CPub &Pub = (CPub &) m_PubList[n];

			if( Pub.m_uPeriod ) {

				UINT64 uSlot = t     / Pub.m_uPeriod;

				UINT64 uTime = uSlot * Pub.m_uPeriod;

				if( uTime > Pub.m_uLast ) {

					m_pLock->Wait(FOREVER);

					if( (m_Opts.m_uBuffer && Pub.m_fHistory) || (IsLive() && !Pub.m_fPend) ) {

						UINT64        uMark = Pub.m_uLast ? uTime : t;

						BOOL          fTemp = Pub.m_uLast ? FALSE : TRUE;

						UINT          uMode = m_Opts.m_Mode;

						CMqttMessage *pMsg  = NULL;

						if( GetPubMessage(Pub.m_uTopic, uMark, fTemp, uMode, pMsg) ) {

							pMsg->SetCode(n);

							if( QueueMessage(pMsg) ) {

								if( Pub.m_fHistory ) {

									CCloudDataSet *pSet = m_pService->GetDataSet(Pub.m_uTopic);

									pSet->DataWasSent();
								}
								else
									Pub.m_fPend = TRUE;
							}
							else
								delete pMsg;
						}
					}

					m_pLock->Free();

					Pub.m_uLast = uTime;
				}
			}
		}

		return FALSE;
	}

	return CMqttQueuedClient::Poll(uID);
}

// Client Hooks

void CMqttClientCrimson::OnClientPhaseChange(void)
{
	if( m_uPhase == phaseInitial ) {

		UINT c = m_pService->GetSetCount();

		for( UINT s = 0; s < c; s++ ) {

			CCloudDataSet *pSet = m_pService->GetDataSet(s);

			if( pSet->IsEnabled() ) {

				AddToPubList(s, pSet->UseHistory(), pSet->GetPeriod());
			}
		}

		m_pService->SetStatus(0);
	}

	if( m_uPhase == phaseConnecting ) {

		m_pService->SetStatus(1);
	}

	if( m_uPhase == phaseLive ) {

		if( m_Opts.m_Reconn == 0 ) {

			UINT c = m_pService->GetSetCount();

			for( UINT s = 0; s < c; s++ ) {

				CCloudDataSet *pSet = m_pService->GetDataSet(s);

				pSet->ResetHistory();
			}

			for( UINT n = 0; n < m_PubList.GetCount(); n++ ) {

				CPub &Pub   = (CPub &) m_PubList[n];

				Pub.m_uLast = 0;

				Pub.m_fPend = FALSE;
			}
		}
		else {
			for( UINT n = 0; n < m_PubList.GetCount(); n++ ) {

				CPub &Pub   = (CPub &) m_PubList[n];

				Pub.m_fPend = FALSE;
			}
		}
	}

	if( m_uPhase == phaseDropped ) {

		m_pService->SetStatus(0);
	}

	CMqttQueuedClient::OnClientPhaseChange();
}

BOOL CMqttClientCrimson::OnClientDataSent(CMqttMessage const *pMsg)
{
	if( pMsg->m_uCode < m_PubList.GetCount() ) {

		CPub &Pub = (CPub &) m_PubList[pMsg->m_uCode];

		if( Pub.m_uTopic < pubExtended ) {

			if( !Pub.m_fHistory ) {

				CCloudDataSet *pSet = m_pService->GetDataSet(Pub.m_uTopic);

				pSet->DataWasSent();

				Pub.m_fPend = FALSE;
			}

			return CMqttQueuedClient::OnClientDataSent(pMsg);
		}
	}

	return TRUE;
}

// Message Hook

BOOL CMqttClientCrimson::OnMakeHistoric(CMqttMessage *pMsg)
{
	CPub const &Pub = m_PubList[pMsg->m_uCode];

	if( Pub.m_fHistory ) {

		return CMqttQueuedClient::OnMakeHistoric(pMsg);
	}

	return FALSE;
}

// Publish Hook

BOOL CMqttClientCrimson::GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg)
{
	return FALSE;
}

// Implementation

void CMqttClientCrimson::AddToPubList(UINT uTopic, BOOL fHistory, UINT uPeriod)
{
	CPub Pub;

	Pub.m_uTopic   = uTopic;
	Pub.m_fHistory = fHistory;
	Pub.m_uPeriod  = uPeriod;
	Pub.m_uLast    = m_uMilli;
	Pub.m_fPend    = FALSE;

	m_PubList.Append(Pub);
}

UINT CMqttClientCrimson::GetTopicCode(UINT uTopic)
{
	for( UINT c = 0; c < m_PubList.GetCount(); c++ ) {

		if( m_PubList[c].m_uTopic == uTopic ) {

			return c;
		}
	}

	return NOTHING;
}

BOOL CMqttClientCrimson::ForceUpdate(UINT uCode)
{
	if( uCode < m_PubList.GetCount() ) {

		CPub &Pub   = (CPub &) m_PubList[uCode];

		Pub.m_uLast = 0;

		return TRUE;
	}

	return FALSE;
}

BOOL CMqttClientCrimson::HasWrites(void)
{
	UINT c = m_pService->GetSetCount();

	for( UINT n = 1; n < c; n++ ) {

		CCloudTagSet *pSet = (CCloudTagSet *) m_pService->GetDataSet(n);

		if( pSet->HasWrites() ) {

			return TRUE;
		}
	}

	return FALSE;
}

void CMqttClientCrimson::SubstTopic(CString &Topic)
{
	// TODO -- Add %c for ClientId, %d for device name etc.
}

// End of File
