#include "intern.hpp"

#include "ethip2a.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Base Dialog
//

// Constructor

CEIP2aDialogBase::CEIP2aDialogBase(CEIP2aMasterDriver &Driver, CItem * pConfig)
{
	m_pDriver = &Driver;

	m_pConfig = (CEIP2aMasterDeviceOptions *) pConfig;

	m_hRoot   = NULL;
	}

void CEIP2aDialogBase::LoadTree(CTreeView &Tree, CString const &RootName, CEIPL5KList *pTags, CArray<CEIPL5KList *> const &Programs)
{
	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.DeleteItem(TVI_ROOT);
	
	LoadRoot(Tree, RootName);

	LoadTags(Tree, L"Controller Tags", pTags);

	LoadProgramTags(Tree, Programs);

	Tree.Expand(m_hRoot, TVE_EXPAND);

	Tree.SetFocus();
	}

void CEIP2aDialogBase::LoadRoot(CTreeView &Tree, CString const &RootName)
{
	CTreeViewItem Node;

	Node.SetText(RootName);

	Node.SetParam(NULL);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CEIP2aDialogBase::LoadProgramTags(CTreeView &Tree, CArray<CEIPL5KList *> Programs)
{
	for( UINT n = 0; n < Programs.GetCount(); n++ ) {

		if( Programs[n]->GetItemCount() > 0 ) {

			CTreeViewItem Root;

			Root.SetText(Programs[n]->m_Name);

			Root.SetParam(NULL);

			HTREEITEM hRoot = Tree.InsertItem(m_hRoot, NULL, Root);

			LoadTags(Tree, hRoot, Programs[n]);
			}
		}
	}

void CEIP2aDialogBase::LoadTags(CTreeView &Tree, CString Root, CEIPL5KList *pTags)
{
	CTreeViewItem Node;

	Node.SetText(Root);

	HTREEITEM hRoot = Tree.InsertItem(m_hRoot, NULL, Node);

	LoadTags(Tree, hRoot, pTags);
	}

void CEIP2aDialogBase::LoadTags(CTreeView &Tree, HTREEITEM hRoot, CEIPL5KList *pTags)
{
	for( UINT n = 0; n < pTags->GetItemCount(); n++ ) {
		
		CEIPL5KEntry *pTag = (CEIPL5KEntry *) pTags->GetItem(n);

		CString Name = pTag->m_Name;

		CString Type = pTag->m_Type;

		if( pTag->m_fStruct ) {

			CTreeViewItem Node;

			Node.SetText(Name);

			Node.SetParam(DWORD(pTag));

			HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);
			
			LoadTags(Tree, hNode, pTag->m_pMembers);

			continue;		
			}

		LoadTag(Tree, hRoot, pTag);
		}
	}

void CEIP2aDialogBase::LoadTag(CTreeView &Tree, HTREEITEM hRoot, CEIPL5KEntry *pTag)
{
	if( !pTag->m_fStruct ) {

		CTreeViewItem Node;

		Node.SetText(pTag->m_Name);

		Node.SetParam(DWORD(pTag));

		HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);

		AfxTouch(hNode);
		}
	}

// UI Management

void CEIP2aDialogBase::ShowDetails(UINT uMin, UINT uMax, UINT uType, UINT uAlias, UINT uAtom, CEIPL5KEntry const *pEntry)
{
	CString Text;

	switch( pEntry->m_uType ) {

		case addrBitAsBit:
			Text = L"Bit as Bit";
			break;

		case addrByteAsByte:
			Text = L"Byte as Byte";
			break;

		case addrWordAsWord:
			Text = L"Word as Word";
			break;

		case addrLongAsLong:
			Text = L"Long as Long";
			break;

		case addrRealAsReal:
			Text = L"Real as Real";
			break;

		default:
			Text = "";
			break;
		}

	GetDlgItem(uAtom).SetWindowText(PCTXT(Text));

	GetDlgItem(uType).SetWindowText(pEntry->m_Type);

	if( pEntry->m_fAlias ) {

		GetDlgItem(uAlias).SetWindowText(pEntry->m_Alias);
		}
	else {
		GetDlgItem(uAlias).SetWindowText(L"");
		}

	CEIPTagInfo *pTag = m_pConfig->FindTag(pEntry->m_Name);

	if( pTag && pTag->m_Dims.GetCount() > 0 ) {

		// Determine minimum and maximum dimensions

		CString MinText(pEntry->m_Name);

		CString MaxText(pEntry->m_Name);

		CLongArray MinDims, MaxDims, CurDims;

		for( UINT u = 0; u < pTag->m_Dims.GetCount(); u++ ) {

			MinDims.Append(0);

			MaxDims.Append(pTag->m_Dims[u] - 1);
			}

		m_pConfig->ExpandDimsText(MinText, MinDims);

		m_pConfig->ExpandDimsText(MaxText, MaxDims);

		GetDlgItem(uMin).SetWindowText(MinText);

		GetDlgItem(uMax).SetWindowText(MaxText);
		}
	else {
		GetDlgItem(uMin).SetWindowText("");

		GetDlgItem(uMax).SetWindowText("");
		}
	}

void CEIP2aDialogBase::PopulateListBox(CListBox &ListBox, CNameIndex &Names)
{
	INDEX Idx = Names.GetHead();

	while( !Names.Failed(Idx) ) {

		CString Name = Names.GetName(Idx);

		ListBox.AddString(Name);

		Names.GetNext(Idx);
		}
	}

// Tag Management

BOOL CEIP2aDialogBase::AddTagToDevice(CString Text, UINT uType)
{
	CError Error;

	if( !m_pConfig->AddTagToDevice(Error, Text, uType) ) {

		Error.Show(ThisObject);

		return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Management Dialog
//

AfxImplementRuntimeClass(CEIP2aMasterDialog, CEIP2aDialogBase);

// Constructor

CEIP2aMasterDialog::CEIP2aMasterDialog(CEIP2aMasterDriver &Driver, CItem * pConfig)
	: CEIP2aDialogBase(Driver, pConfig)
{
	SetName(TEXT("EIPMasterManageDlg"));
	}

// Destructor

CEIP2aMasterDialog::~CEIP2aMasterDialog(void)
{

	}

// Message Map

AfxMessageMap(CEIP2aMasterDialog, CEIP2aDialogBase)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(2001, OnAddTag)
	AfxDispatchCommand(2002, OnDelTag)
	AfxDispatchCommand(1003, OnImport)
	AfxDispatchCommand(2003, OnAddL5K)

	AfxDispatchNotify(1001, NM_DBLCLK, OnTreeDblClk)
	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(3001, LBN_SELCHANGE, OnListSelChanged)

	AfxMessageEnd(CEIP2aMasterDialog)
	};

// Message Handlers

BOOL CEIP2aMasterDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CString Text;

	SetDlgFocus(1002);

	CListBox &List = (CListBox &) GetDlgItem(3001);

	CComboBox &Combo = (CComboBox &) GetDlgItem(1004);

	Combo.AddString(L"Word as Word", addrWordAsWord);
	Combo.AddString(L"Real as Real", addrRealAsReal);
	Combo.AddString(L"Bit as Bit",   addrBitAsBit  );
	Combo.AddString(L"Long as Long", addrLongAsLong);
	Combo.AddString(L"Byte as Byte", addrByteAsByte);

	Combo.SetCurSel(0);

	if( m_pConfig->m_fL5KInit ) {		

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		CString Name = m_pConfig->m_Controller.m_Name;

		CEIPL5KList *pTags = m_pConfig->m_pL5KTags;

		LoadTree(Tree, Name, pTags, m_pConfig->m_pPrograms);
		}

	PopulateListBox(List, m_pConfig->m_Names);

	return TRUE;
	}

BOOL CEIP2aMasterDialog::OnAddTag(UINT uId)
{
	CListBox &List = (CListBox &) GetDlgItem(3001);

	CComboBox &Combo = (CComboBox &) GetDlgItem(1004);

	List.SetRedraw(FALSE);

	List.ResetContent();

	UINT uType = Combo.GetItemData(Combo.GetCurSel());

	CString Text;

	Text += GetDlgItem(1002).GetWindowText();

	if( !Text.IsEmpty() ) {

		if( !m_pConfig->TagExists(Text) ) {

			AddTagToDevice(Text, uType);
			}
		}

	INDEX Idx = m_pConfig->m_Names.GetHead();

	while( !m_pConfig->m_Names.Failed(Idx) ) {

		List.AddString(m_pConfig->m_Names.GetName(Idx));

		m_pConfig->m_Names.GetNext(Idx);
		}

	List.SetRedraw(TRUE);

	List.Invalidate(FALSE);

	return TRUE;
	}

BOOL  CEIP2aMasterDialog::OnDelTag(UINT uId)
{
	CListBox &List = (CListBox &) GetDlgItem(3001);

	List.SetRedraw(FALSE);

	CString Text;

	UINT uSel = List.GetCurSel();

	Text += List.GetText(uSel);

	m_pConfig->DeleteTag(Text);

	List.ResetContent();

	INDEX Idx = m_pConfig->m_Names.GetHead();

	while( !m_pConfig->m_Names.Failed(Idx) ) {

		List.AddString(m_pConfig->m_Names.GetName(Idx));

		m_pConfig->m_Names.GetNext(Idx);
		}

	if( uSel < List.GetCount() )

		List.SetCurSel(uSel);
	else
		List.SetCurSel(List.GetCount() - 1);

	List.SetRedraw(TRUE);

	List.Invalidate(FALSE);

	return TRUE;
	}

BOOL CEIP2aMasterDialog::OnImport(UINT uID)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(L"EIP Tags");

	Dlg.SetFilter(L"RSLogix 5000 Import/Export Files (*.l5k)|*.l5k");

	if( Dlg.Execute(ThisObject) ) {

		afxThread->SetWaitMode(TRUE);

		CFilename Filename = Dlg.GetFilename();
				
		if( m_pConfig->LoadFromFile(Filename) ) {
			
			Dlg.SaveLastPath(L"EIP Tags");

			m_Filename = Filename;

			BOOL fSetName = TRUE;

			if( !m_pConfig->GetSavedFilename().IsEmpty() ) {

				CString Message =	L"Would you like to use this file when loading the database? "
							L"This will replace the L5K file path that is currently set.";							

				fSetName = (YesNo(Message) == IDYES);
				}

			if( fSetName ) {

				m_pConfig->SetSavedFilename(m_Filename);
				}
			}

		afxThread->SetWaitMode(FALSE);

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		CString Name = m_pConfig->m_Controller.m_Name;

		CEIPL5KList *pTags = m_pConfig->m_pL5KTags;

		LoadTree(Tree, Name, pTags, m_pConfig->m_pPrograms);
		}

	return TRUE;
	}

BOOL CEIP2aMasterDialog::OnAddL5K(UINT uID)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	CListBox &List = (CListBox &) GetDlgItem(3001);

	List.SetRedraw(FALSE);

	List.ResetContent();

	CTreeViewItem Item = Tree.GetItem(Tree.GetSelection());

	// Check if the child is a leaf
	// --> this means the child represents an atomic tag.

	if( Item.GetChildren() == 0 ) {

		CEIPL5KEntry *pTag = (CEIPL5KEntry *) Item.GetParam();

		if( pTag ) {

			CString Text;
		
			if( pTag->m_fProgram ) {

				Text.Printf("Program:%s.", pTag->m_Scope);
				}

			Text += pTag->m_Name;

			if( !m_pConfig->TagExists(Text) ) {

				AddTagToDevice(Text, pTag->m_uType);
				}
			else {
				CError Error(TRUE);

				Error.Set(L"This tag has already been added to the available address list.");

				Error.Show(ThisObject);
				}
			}
		}

	INDEX Idx = m_pConfig->m_Names.GetHead();
	
	while( !m_pConfig->m_Names.Failed(Idx) ) {

		List.AddString(m_pConfig->m_Names.GetName(Idx));

		m_pConfig->m_Names.GetNext(Idx);
		}

	List.SetRedraw(TRUE);

	List.Invalidate(FALSE);

	return TRUE;
	}

void CEIP2aMasterDialog::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	if( Tree.IsMouseInSelection() ) {

		OnAddL5K(uID);
		}
	}

void CEIP2aMasterDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	CTreeView   & Tree = (CTreeView &) GetDlgItem(1001);

	CTreeViewItem Item = Tree.GetItem(Info.itemNew.hItem);

	CEIPL5KEntry *pEntry = (CEIPL5KEntry *) Item.GetParam();

	if( pEntry ) {

		ShowDetails(pEntry);
		}
	}

void CEIP2aMasterDialog::OnListSelChanged(UINT uID, CWnd &Wnd)
{
	CListBox &List = (CListBox &) GetDlgItem(3001);

	UINT uIndex = List.GetCurSel();

	// Temporary entry for display

	CEIPL5KEntry Entry;

	CString Name = List.GetText(uIndex);

	Entry.m_Name = Name;

	CEIPTagInfo *pTagInfo = m_pConfig->FindTag(Entry.m_Name);

	if( pTagInfo ) {

		Entry.m_uType = pTagInfo->m_Type;
		}

	ShowDetails(&Entry);
	}

void CEIP2aMasterDialog::ShowDetails(CEIPL5KEntry const *pEntry)
{
	UINT uMin	= 2007;
	UINT uMax	= 2009;
	UINT uType	= 2005;
	UINT uAlias	= 2013;
	UINT uAtom	= 2011;

	CEIP2aDialogBase::ShowDetails(uMin, uMax, uType, uAlias, uAtom, pEntry);
	}

BOOL CEIP2aMasterDialog::OnOkay(UINT uId)
{
	GetParent().PostMessage(WM_COMMAND, IDM_COMMS_BUILD_BLOCK);

	GetParent().PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

	EndDialog(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Address Selection Dialog
//

AfxImplementRuntimeClass(CEIP2aSelectionDialog, CEIP2aDialogBase);

// Constructor

CEIP2aSelectionDialog::CEIP2aSelectionDialog(CEIP2aMasterDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart, BOOL fSelect)
	: CEIP2aDialogBase(Driver, pConfig)
{
	m_pAddr		= &Addr;
	
	m_fPart		= fPart;

	m_fSelect	= fSelect;

	m_uLastFocus	= NOTHING;

	SetName(fPart ? TEXT("PartEIPMasterDlg") : TEXT("FullEIPMasterDlg"));
	}

// Destructor

CEIP2aSelectionDialog::~CEIP2aSelectionDialog(void)
{
	}

AfxMessageMap(CEIP2aSelectionDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(3001, LBN_DBLCLK, OnDblClk)
	AfxDispatchNotify(1001, NM_DBLCLK, OnTreeDblClk)	
	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)
	AfxDispatchNotify(3001, LBN_SELCHANGE, OnListSelChanged)

	AfxMessageEnd(CEIP2aSelectionDialog)
	};

// Message Handlers

BOOL CEIP2aSelectionDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( !m_fPart ) {

		if( m_pConfig->m_fL5KInit ) {

			CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

			CString Name = m_pConfig->m_Controller.m_Name;

			CEIPL5KList *pTags = m_pConfig->m_pL5KTags;

			LoadTree(Tree, Name, pTags, m_pConfig->m_pPrograms);
			}

		CListBox &ListBox = (CListBox &) GetDlgItem(3001);

		PopulateListBox(ListBox, m_pConfig->m_Names);

		DoEnables();
		}

	if( m_pAddr ) {

		ShowAddress();
		}

	m_uLastFocus = NOTHING;

	return TRUE;
	}

BOOL CEIP2aSelectionDialog::OnOkay(UINT uId)
{
	CString Text = GetDlgItem(4004).GetWindowText();

	if( Text.IsEmpty() ) {

		EndDialog(TRUE);

		return TRUE;
		}

	BOOL fMapped = FALSE; 
	
	if( m_uLastFocus == 1001 ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		fMapped = MapFromTree(Tree, Text);
		}
	else {		
		fMapped = MapTag(Text);
		}

	if( fMapped ) {

		EndDialog(fMapped);
		}

	return fMapped;
	}

void CEIP2aSelectionDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	CListBox &List = (CListBox &) GetDlgItem(3001);

	CString Text = List.GetText(List.GetCurSel());

	if( MapTag(Text) ) {

		EndDialog(TRUE);
		}
	}

void CEIP2aSelectionDialog::OnTreeDblClk(UINT uID, NMHDR &Info)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	if( Tree.IsMouseInSelection() ) {

		CString Text = GetDlgItem(4004).GetWindowText();

		if( MapFromTree(Tree, Text) ) {

			EndDialog(TRUE);
			}
		}
	}

void CEIP2aSelectionDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_uLastFocus = 1001;

	UpdateSelectedText();
	}
	
void CEIP2aSelectionDialog::OnListSelChanged(UINT uID, CWnd &Wnd)
{
	m_uLastFocus = 3001;

	UpdateSelectedText();
	}

// UI Handlers
		
void CEIP2aSelectionDialog::ShowDetails(CEIPL5KEntry const *pEntry)
{
	UINT uMin	= 2007;
	UINT uMax	= 2009;
	UINT uType	= 2005;
	UINT uAlias	= 2013;
	UINT uAtom	= 2011;

	CEIP2aDialogBase::ShowDetails(uMin, uMax, uType, uAlias, uAtom, pEntry);
	}

void CEIP2aSelectionDialog::ShowAddress(void)
{
	CEIPTagInfo *pTag = NULL;

	UINT uSlot = m_pAddr->a.m_Table < addrNamed ? m_pAddr->a.m_Table : m_pAddr->a.m_Offset;

	pTag = m_pConfig->FindTag(uSlot);

	if( pTag ) {

		// Dummy entry to display

		CEIPL5KEntry Entry;

		Entry.m_Name = pTag->m_Name;

		Entry.m_uType = pTag->m_Type;

		CLongArray CurDims;

		UINT uIndex = m_pAddr ? m_pAddr->a.m_Offset : 0;

		m_pConfig->ExpandDimsIndex(uIndex, pTag->m_Dims, CurDims);

		UINT uCount = CurDims.GetCount();

		if( uCount > 2 ) GetDlgItem(4003).SetWindowText(CPrintf("%d", CurDims[2]));
		if( uCount > 1 ) GetDlgItem(4002).SetWindowText(CPrintf("%d", CurDims[1]));
		if( uCount > 0 ) GetDlgItem(4001).SetWindowText(CPrintf("%d", CurDims[0]));			

		ShowDetails(&Entry);

		DoEnables(Entry.m_Name);

		GetDlgItem(4004).SetWindowText(Entry.m_Name);

		if( !m_fPart ) {

			CListBox &ListBox = (CListBox &) GetDlgItem(3001);

			UINT uFind = ListBox.FindString(0, pTag->m_Name);

			if( uFind < NOTHING ) {

				ListBox.SetCurSel(uFind);
			
				ListBox.SetFocus();
				}
			}
		}
	}

void CEIP2aSelectionDialog::DoEnables(void)
{
	GetDlgItem(4001).EnableWindow(FALSE);
	GetDlgItem(4002).EnableWindow(FALSE);
	GetDlgItem(4003).EnableWindow(FALSE);
	}
		
void CEIP2aSelectionDialog::DoEnables(CString const &TagName)
{
	DoEnables();

	CEIPTagInfo *pTag = m_pConfig->FindTag(TagName);

	if( pTag ) {

		EnableOffsets(pTag->m_Dims);

		InitOffsets(pTag->m_Dims);
		}
	}

void CEIP2aSelectionDialog::EnableOffsets(CLongArray const &Dims)
{
	UINT uCount = Dims.GetCount();

	switch( uCount ) {

		case 3:
			GetDlgItem(4003).EnableWindow(TRUE);
		case 2:
			GetDlgItem(4002).EnableWindow(TRUE);
		case 1:
			GetDlgItem(4001).EnableWindow(TRUE);
		}
	}

void CEIP2aSelectionDialog::InitOffsets(CLongArray const &Dims)
{
	UINT uCount = Dims.GetCount();

	switch( uCount ) {

		case 3:
			if( GetDlgItem(4003).GetWindowText().IsEmpty() ) {

				GetDlgItem(4003).SetWindowText("0");
				}
		case 2:
			if( GetDlgItem(4002).GetWindowText().IsEmpty() ) {

				GetDlgItem(4002).SetWindowText("0");
				}
		case 1:
			if( GetDlgItem(4001).GetWindowText().IsEmpty() ) {

				GetDlgItem(4001).SetWindowText("0");
				}
		}
	}

void CEIP2aSelectionDialog::UpdateSelectedText(void)
{
	CEIPL5KEntry Entry;
	
	if( m_uLastFocus == 1001 ) {

		CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

		GetSelectedEntry(Entry, Tree, TRUE);

		CLongArray Dims;

		m_pConfig->ParseDims(Dims, Entry.m_Type);

		DoEnables(Entry.m_Name);

		EnableOffsets(Dims);

		InitOffsets(Dims);
		}

	if( m_uLastFocus == 3001 ) {

		CListBox &ListBox = (CListBox &) GetDlgItem(3001);

		GetSelectedEntry(Entry, ListBox);

		DoEnables(Entry.m_Name);
		}

	ShowDetails(&Entry);

	GetDlgItem(4004).SetWindowText(Entry.m_Name);
	}

// Address Management

void CEIP2aSelectionDialog::GetSelectedEntry(CEIPL5KEntry &Entry, CTreeView const &Tree, BOOL fTrim)
{
	CTreeViewItem Item = Tree.GetItem(Tree.GetSelection());

	if( Item.GetChildren() == 0 ) {

		CEIPL5KEntry *pEntry = (CEIPL5KEntry *) Item.GetParam();

		if( pEntry ) {

			CString Text;
		
			if( pEntry->m_fProgram ) {

				Text.Printf("Program:%s.", pEntry->m_Scope);
				}

			Text += pEntry->m_Name;

			Entry.m_Name	= Text;
			Entry.m_Type	= pEntry->m_Type;
			Entry.m_Alias	= pEntry->m_Alias;
			Entry.m_Scope	= pEntry->m_Scope;
			Entry.m_uType	= pEntry->m_uType;

			if( fTrim ) {

				UINT uDot = Text.FindRev('.');

				UINT uFind = Text.FindRev('[');

				if( uFind < NOTHING ) {

					if( uDot == NOTHING || uDot < NOTHING && uFind > uDot ) {

						Entry.m_Name = Text.Mid(0, uFind);
						}		
					}
				}
			}
		}
	}

void CEIP2aSelectionDialog::GetSelectedEntry(CEIPL5KEntry &Entry, CListBox const &ListBox)
{
	CString Name = ListBox.GetText(ListBox.GetCurSel());

	CEIPTagInfo *pTagInfo = m_pConfig->FindTag(Name);

	if( pTagInfo ) {

		Entry.m_Name = Name;

		Entry.m_uType = pTagInfo->m_Type;
		}
	}

BOOL CEIP2aSelectionDialog::MapTag(CString Text)
{
	UINT x, y, z;

	CEIPTagInfo *pTag = m_pConfig->FindTag(Text);

	if( pTag ) {

		x = wcstoul(GetDlgItem(4001).GetWindowText(), NULL, 10);
		y = wcstoul(GetDlgItem(4002).GetWindowText(), NULL, 10);
		z = wcstoul(GetDlgItem(4003).GetWindowText(), NULL, 10);
		
		UINT uCount = pTag->m_Dims.GetCount();

		switch( uCount ) {

			case 1:
				Text += CPrintf("[%d]", x);
				break;
			case 2:
				Text += CPrintf("[%d,%d]", x, y);
				break;
			case 3:
				Text += CPrintf("[%d,%d,%d]", x, y, z);
				break;
			}
		}

	CAddress Addr;

	Addr.m_Ref = 0;

	return DoMapTag(Text, Addr);
	}

BOOL CEIP2aSelectionDialog::MapFromTree(CTreeView &Tree, CString Text)
{
	CEIPL5KEntry Entry;

	GetSelectedEntry(Entry, Tree, FALSE);

	if( !m_pConfig->TagExists(Entry.m_Name) ) {

		if( !AddTagToDevice(Entry.m_Name, Entry.m_uType) ) {

			return FALSE;
			}
		}

	return MapTag(Text);
	}

BOOL CEIP2aSelectionDialog::DoMapTag(CString Text, CAddress Addr)
{
	CError Error;

	if( !Text.IsEmpty() ) {

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			return TRUE;
			}
		else {
			Error.Show(ThisObject);

			return FALSE;
			}
		}

	return FALSE;
	}

// End of File
