
#include "intern.hpp"

#include "PrimRubyPenLine.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Line Pen
//

// Constructor

CPrimRubyPenLine::CPrimRubyPenLine(void)
{
	m_End1 = CRubyStroker::endFlat;

	m_End2 = CRubyStroker::endFlat;
	}

// Initialization

void CPrimRubyPenLine::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubPenEdge", pData);

	CPrimRubyPenBase::Load(pData);

	m_End1 = GetByte(pData);
	m_End2 = GetByte(pData);
	}

// Stroking

BOOL CPrimRubyPenLine::Stroke(CRubyPath &output, CRubyPath const &figure)
{
	if( !IsNull() ) {

		CRubyStroker s;

		s.SetEdgeMode  ((CRubyStroker::EdgeMode ) m_Edge);

		s.SetJoinStyle ((CRubyStroker::JoinStyle) m_Join);

		if( m_End1 == m_End2 && m_End1 < 100 ) {

			s.SetEndStyle((CRubyStroker::EndStyle) m_End1);
			}
		else {
			s.SetEndStyle  (CRubyStroker::endArrow);

			s.SetArrowStyle(m_End1, m_End2);
			}

		s.StrokeOpen(output, figure, 0, m_Width);

		return TRUE;
		}

	return FALSE;
	}

// End of File
