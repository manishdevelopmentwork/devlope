
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "events.hpp"

#include "service.hpp"

#include "web.hpp"

#include "datalog.hpp"

#include "secure.hpp"

#include "lang.hpp"

#include "sql.hpp"

#include "web.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Straton Control Instance Pointer
//

IStratonControl	* g_pControl = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Communications System Item
//

// Instance Pointer

CCommsSystem * CCommsSystem::m_pThis = NULL;

// Constructor

CCommsSystem::CCommsSystem(void)
{
	m_pThis       = this;

	m_pComms      = New CCommsManager;

	m_pTags       = New CTagManager;

	m_pEvents     = New CEventManager;

	m_pAlarms     = New CAlarmManager;

	m_pPrograms   = New CProgramManager;

	m_pLog        = New CDataLogger;

	m_pSecure     = New CSecurityManager;

	m_pLang	      = New CLangManager;

	m_pWeb        = WhoHasFeature(rfWebServer) ? Create_WebServer() : NULL;

	m_pSql        = NULL;

	m_pControl    = NULL;

	m_pDataServer = NULL;

	m_pUsedServer = NULL;

	m_pGDI        = NULL;
}

// Destructor

CCommsSystem::~CCommsSystem(void)
{
	m_pThis = NULL;

	delete m_pComms;

	delete m_pTags;

	delete m_pEvents;

	delete m_pAlarms;

	delete m_pPrograms;

	delete m_pLog;

	delete m_pSecure;

	delete m_pLang;

	delete m_pWeb;

	delete m_pSql;

	AfxRelease(m_pControl);

	m_pUsedServer->Release();
}

// Initialization

void CCommsSystem::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsSystem", pData);

	DoLoad(pData);

	m_pDataServer = New CDataServer(this);

	m_pUsedServer = m_pDataServer;
}

// System Calls

void CCommsSystem::SystemStart(void)
{
	m_pSecure->Init();

	if( m_pComms->m_pOnEarly ) {

		m_pPrograms->SetEarly(TRUE);

		m_pComms->m_pOnEarly->Execute(typeVoid);

		m_pPrograms->SetEarly(FALSE);
	}
}

void CCommsSystem::SystemStop(void)
{
	m_pSecure->Term();
}

void CCommsSystem::SystemSave(void)
{
	m_pLog->ForceSave();

	m_pLog->WaitSave();
}

void CCommsSystem::SystemInit(void)
{
}

void CCommsSystem::SystemTerm(void)
{
	m_pTags->m_pTags->Purge();
}

void CCommsSystem::GetTaskList(CTaskList &List)
{
	m_pComms->GetTaskList(List);

	m_pTags->GetTaskList(List);

	m_pPrograms->GetTaskList(List);

	m_pLog->GetTaskList(List);

	m_pSecure->GetTaskList(List);

	if( m_pWeb ) {

		m_pWeb->GetTaskList(List);
	}

	if( m_pSql ) {

		m_pSql->GetTaskList(List);
	}

	if( WhoGetActiveGroup() >= SW_GROUP_4 ) {

		g_pControl->GetTaskList(List);
	}
}

UINT CCommsSystem::GetCommsStatus(void)
{
	return m_pComms->GetCommsStatus();
}

BOOL CCommsSystem::GetDataServer(IDataServer * &pData)
{
	pData = m_pDataServer;

	return pData ? TRUE : FALSE;
}

BOOL CCommsSystem::IsRunningControl(void)
{
	return g_pControl ? g_pControl->IsRunning() : FALSE;
}

// Server Access

IDataServer * CCommsSystem::GetDataServer(void) const
{
	return m_pUsedServer;
}

// GDI Access

IGDI * CCommsSystem::GetGDI(void) const
{
	return m_pGDI;
}

// Implementation

void CCommsSystem::DoLoad(PCBYTE &pData)
{
	m_Build   = GetWord(pData);

	m_hComms  = GetItem(pData, m_pComms, IDC_COMMS);

	m_hTags   = GetItem(pData, m_pTags, IDC_TAG_MANAGER);

	m_hProgs  = GetItem(pData, m_pPrograms, IDC_PROG_MANAGER);

	m_hLog    = GetItem(pData, m_pLog, IDC_LOGGER);

	m_hSecure = GetItem(pData, m_pSecure, IDC_SECURITY);

	m_hLang   = GetItem(pData, m_pLang, IDC_LANGUAGE);

	if( WhoHasFeature(rfWebServer) ) {

		m_hWeb = GetItem(pData, m_pWeb, IDC_WEB_SERVER);
	}
	else {
		m_hWeb = NULL;

		pData += sizeof(WORD);
	}

	LoadSql(pData);

	LoadControl(pData);

	m_pEvents->Load();
}

BOOL CCommsSystem::LoadControl(PCBYTE &pData)
{
	UINT   hCtrl = GetWord(pData);

	PCBYTE pInit = PCBYTE(g_pDbase->LockItem(hCtrl));

	if( pInit ) {

		if( GetWord(pInit) == IDC_CONTROL ) {

			m_pControl = Create_StratonControl();

			if( m_pControl ) {

				m_pControl->LoadControl(pInit);
			}
		}

		g_pDbase->FreeItem(hCtrl);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsSystem::LoadSql(PCBYTE &pData)
{
	UINT hSql = GetWord(pData);

	if( WhoGetActiveGroup() >= SW_GROUP_3B ) {

		if( WhoHasFeature(rfSqlQuery) ) {

			PCBYTE pInit = PCBYTE(g_pDbase->LockItem(hSql));

			if( pInit ) {

				if( GetWord(pInit) == IDC_SQL_MANAGER ) {

					m_pSql = New CSqlManager;

					if( m_pSql ) {

						m_pSql->LoadSqlManager(pInit);
					}
				}

				g_pDbase->FreeItem(hSql);

				return TRUE;
			}
		}
	}

	return FALSE;
}

// End of File
