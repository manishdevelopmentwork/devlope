
#include "Intern.hpp"

#include "UsbModuleBoot.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Module Boot Loader
//

// Instantiator

IUsbHostFuncEvents * Create_UsbModuleBoot(IUsbHostFuncDriver *pDriver, IExpansionFirmware *pFirmware)
{
	CUsbModuleBoot *p = New CUsbModuleBoot(pDriver, pFirmware);

	return p;
	}

// Constructor

CUsbModuleBoot::CUsbModuleBoot(IUsbHostFuncDriver *pDriver, IExpansionFirmware *pFirmware)
{
	StdSetRef();

	m_pDriver = (IUsbHostModuleBoot *) pDriver;

	m_pFirm   = pFirmware;

	m_uState  = stateSendCheck;

	m_uDelay  = 0;

	m_fLock   = false;

	m_pDriver->AddRef();

	m_pDriver->Bind(this);
	}

// Destructor

CUsbModuleBoot::~CUsbModuleBoot(void)
{
	SetFirmwareLock(false);

	m_pDriver->Bind(NULL);

	m_pDriver->Release();
	}

// IUnknown

HRESULT METHOD CUsbModuleBoot::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbHostFuncEvents);

	StdQueryInterface(IUsbHostFuncEvents);

	return E_NOINTERFACE;
	}

ULONG METHOD CUsbModuleBoot::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CUsbModuleBoot::Release(void)
{
	StdRelease();
	}

// IUsbHostFuncEvents

void METHOD CUsbModuleBoot::OnPoll(UINT uLapsed)
{
	if( m_uDelay ) {

		m_uDelay--;

		return;
		}

	switch( m_uState ) {

		case stateSendCheck:

			OnSendCheck();

			break;

		case stateWaitCheck:

			OnWaitCheck();

			break;

		case stateSendClear:

			OnSendClear();

			break;

		case stateWaitClear:

			OnWaitClear();

			break;

		case stateSendProg:

			OnSendProg();

			break;

		case stateWaitProg:

			OnWaitProg();

			break;

		case stateSendGuid:

			OnSendGuid();

			break;

		case stateWaitGuid:

			OnWaitGuid();

			break;

		case stateSendRun:

			OnSendRun();

			break;

		case stateError:

			OnError();

			break;
		}
	}

void METHOD CUsbModuleBoot::OnData(void)
{
	}

void METHOD CUsbModuleBoot::OnTerm(void)
{
	delete this;
	}

// State Handlers

void CUsbModuleBoot::OnSendCheck(void)
{
	if( SetFirmwareLock(true) ) {

		m_pGuid = m_pFirm->GetFirmwareGuid(m_pDriver->GetProduct());
	
		if( m_pGuid ) {

			/*AfxTrace("CUsbModuleBoot::OnCheck\n");*/

			m_pDriver->CheckVersion(m_pGuid, 16);

			m_uState = stateWaitCheck;
			
			return;
			}

		m_uDelay = 100;
		
		SetFirmwareLock(false);

		return;
		}

	m_uDelay = 10;
	}

void CUsbModuleBoot::OnWaitCheck(void)
{
	/*AfxTrace("CUsbModuleBoot::OnWaitCheck\n");*/

	CheckWait(stateSendRun, stateSendClear, stateError);
	}

void CUsbModuleBoot::OnSendClear(void)
{
	/*AfxTrace("CUsbModuleBoot::OnSendClear\n");*/

	m_uSize = m_pFirm->GetFirmwareSize(m_pDriver->GetProduct()); 

	m_pDriver->ClearProgram(m_uSize);

	m_uState = stateWaitClear;
	}

void CUsbModuleBoot::OnWaitClear(void)
{
	/*AfxTrace("CUsbModuleBoot::OnWaitClear\n");*/

	if( CheckWait(stateSendProg, stateError, stateError) ) {

		m_uAddr = 0;

		m_pData = m_pFirm->GetFirmwareData(m_pDriver->GetProduct());
		}
	}

void CUsbModuleBoot::OnSendProg(void)
{
	/*AfxTrace("CUsbModuleBoot::OnSendProg\n");*/

	if( m_uAddr < m_uSize ) {

		UINT uCount = m_uSize - m_uAddr;

		MakeMin(uCount, 512);

		m_pDriver->WriteProgram(m_uAddr, m_pData, uCount);

		m_uAddr += uCount;

		m_pData += uCount;

		m_uState = stateWaitProg;
		}
	else {
		m_uState = stateSendGuid;
		}
	}

void CUsbModuleBoot::OnWaitProg(void)
{
	/*AfxTrace("CUsbModuleBoot::OnWaitProg\n");*/

	CheckWait(stateSendProg, stateError, stateError);
	}

void CUsbModuleBoot::OnSendGuid(void)
{
	/*AfxTrace("CUsbModuleBoot::OnSendGuid\n");*/

	m_pDriver->WriteVersion(m_pGuid, 16);

	m_uState = stateWaitGuid;
	}

void CUsbModuleBoot::OnWaitGuid(void)
{
	/*AfxTrace("CUsbModuleBoot::OnWaitGuid\n");*/

	CheckWait(stateSendRun, stateError, stateError);
	}

void CUsbModuleBoot::OnSendRun(void)
{
	/*AfxTrace("CUsbModuleBoot::OnSendRun\n");*/

	m_pDriver->StartProgram();

	while( m_uState != stateError ) {

		if( CheckWait(stateIdle, stateError, stateError) ) {

			SetFirmwareLock(false);

			return;
			}

		Sleep(5);
		}
	}

void CUsbModuleBoot::OnError(void)
{
	/*AfxTrace("CUsbModuleBoot::OnError\n");*/

	SetFirmwareLock(false);

	m_uState = stateSendCheck;
	}

// Implementation

bool CUsbModuleBoot::CheckWait(UINT uPass, UINT uFail, UINT uError)
{
	switch( m_pDriver->WaitComplete() ) {

		case respPass:

			m_uState = uPass;

			return true;

		case respFalse:

			m_uState = uFail;

			return true;

		case respFail:

			m_uState = uError;

			return false;

		case respBlock:

			return false;
		}

	return false;
	}

bool CUsbModuleBoot::SetFirmwareLock(bool fSet) 
{
	if( fSet != m_fLock ) {

		if( m_pFirm->SetFirmwareLock(fSet) ) {

			m_fLock = fSet;
			}
		}

	return fSet == m_fLock;
	}

// End of File
