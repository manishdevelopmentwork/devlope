
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPenBase_HPP
	
#define	INCLUDE_PrimRubyPenBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyBrush.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

class DLLAPI CPrimRubyPenBase : public CPrimRubyBrush
{
	public:
		// Constructor
		CPrimRubyPenBase(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsNull(void) const;
		int  GetWidth(void) const;
		int  GetInnerWidth(void) const;
		int  GetOuterWidth(void) const;

		// Stroking
		BOOL StrokeExact(CRubyPath &output, CRubyPath const &figure);
		BOOL StrokeEdge (CRubyPath &output, CRubyPath const &figure);
		BOOL StrokeOpen (CRubyPath &output, CRubyPath const &figure);

		// Drawing
		BOOL Fill(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Item Properties
		UINT m_Width;
		UINT m_Edge;
		UINT m_Join;
	};

// End of File

#endif
