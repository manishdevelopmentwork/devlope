
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group Database Identifier
//

class CUITextGroupHandle : public CUITextInteger
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextGroupHandle(void);

	protected:
		// Data
		CGroupVariables *m_pGroup;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group Database Identifier
//

// Dynamic Class

AfxImplementDynamicClass(CUITextGroupHandle, CUITextInteger);

// Constructor

CUITextGroupHandle::CUITextGroupHandle(void)
{
	m_uFlags &= ~textEdit;

	m_uFlags |= textRead;	
	}

// Overridables

void CUITextGroupHandle::OnBind(void)
{
	CUITextInteger::OnBind();

	m_pGroup = (CGroupVariables *) m_pItem;	
	}

void CUITextGroupHandle::OnRebind(void)
{
	CUITextInteger::OnRebind();

	m_pGroup = (CGroupVariables *) m_pItem;
	}

CString CUITextGroupHandle::OnGetAsText(void)
{
	return L"<unknown>";
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Group Database Identifier
//

class CUITextGroupName : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextGroupName(void);

	protected:
		// Data
		CGroupVariables *m_pGroup;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Control Object Database Identifier
//

// Dynamic Class

AfxImplementDynamicClass(CUITextGroupName, CUITextString);

// Constructor

CUITextGroupName::CUITextGroupName(void)
{
	m_uFlags &= ~textEdit;
	}

// Overridables

void CUITextGroupName::OnBind(void)
{
	CUITextString::OnBind();

	m_pGroup = (CGroupVariables *) m_pItem;
	}

void CUITextGroupName::OnRebind(void)
{
	CUITextString::OnRebind();

	m_pGroup = (CGroupVariables *) m_pItem;
	}

CString CUITextGroupName::OnGetAsText(void)
{
	return m_pGroup->m_Name;
	}

// End of File
