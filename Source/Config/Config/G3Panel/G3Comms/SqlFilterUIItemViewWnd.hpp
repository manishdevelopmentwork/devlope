
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlQuery;
class CSqlFilter;

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter Item View Window
//

class CSqlFilterUIItemViewWnd : public CUIItemViewWnd
{
	public:
		// Constructor
		CSqlFilterUIItemViewWnd(CSqlQuery *m_pQuery, CUIPageList *pList, BOOL fScroll = TRUE);

		// UI Management
		void OnUIChange(CItem *pItem, CString Tag);

	protected:
		// Data Members
		CSqlQuery * m_pQuery;

		// Implementation
		void DoEnables(CItem *pItem);
	};

// End of File
