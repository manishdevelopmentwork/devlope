
//////////////////////////////////////////////////////////////////////////
//
// Alfa Laval COMLI Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Alfa Laval COMLI Master Driver Data Spaces
//

#define	SP_ANALOG	0x01
#define	SP_DATAWORDS	0x02
#define	SP_BITBYTES	0x03
#define	SP_BITBITS	0x04
#define	SP_FLOAT	0x05
#define	SP_TIMER	0x06
#define	SP_DATER	0x07
#define	SP_DATEW	0x08
#define	SP_EVENTC	0x09
#define	SP_EVENTG	0x0A

// Read Opcodes
#define	OP_REGR		'2'
#define	OP_BITR		'4'
#define	OP_CONTSTAT	'6'
#define	OP_ANALOGR	'9'
#define	OP_DATER	'I'
#define	OP_FLOATR	'O'
#define	OP_TIMERR	'S'
#define	OP_REGREXT	'<'
#define	OP_EVENTR	']'

// Write Opcodes
#define	OP_REGW		'0'
#define	OP_BITW		'3'
#define	OP_ANALOGW	'A'
#define	OP_DATEW	'J'
#define	OP_FLOATW	'P'
#define	OP_TIMERW	'T'
#define	OP_REGWEXT	'='
#define	OP_EVENTW	'['

#define	K_EXTREG	3072

//////////////////////////////////////////////////////////////////////////
//
// Alfa Laval COMLI Master Driver
//

class CCOMLIMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CCOMLIMasterDriver(void);

		// Destructor
		~CCOMLIMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE  m_bDrop;
			BYTE  m_bStamp;
			BYTE  m_bDate[7];
			DWORD m_dEvent[54];
			BOOL  m_fAscii;
			};

		// Data Members
		CContext *m_pCtx;
		LPCTXT	  m_pHex;

		BYTE	  m_bTx[256];
		BYTE	  m_bRx[256];
		BYTE	  m_bCheck;

		UINT	  m_uPtr;
		UINT	  m_uWriteErr;
		BOOL	  m_fEnhanced;

		// Implementation

		// Startup
		CCODE	GetControllerStatus(void);

		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddDataByte(BYTE bData);
		void	AddDataWord(WORD wData);
		void	AddDataLong(DWORD dData);
		void	AddID(UINT uVal, UINT uQty);
		void	EndFrame(void);

		// Transport Layer
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	Transact(void);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Read Handlers
		CCODE	DoRead(AREF Addr, PDWORD pData, UINT uCount);
		
		// Write Handlers
		CCODE	DoWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		void	KnockStamp(void);
		UINT	AdjCountForAscii(UINT uStartCt);
		DWORD	GetResult(UINT uItem, UINT uSize);
		WORD	SwapWordBits(UINT uData);
		BYTE	SwapByteBits(BYTE bData);
		UINT	SwapBytes(UINT uData);
		void	GetDateFromCache(UINT uItem, UINT uCount, PDWORD pData);
		void	PutDateIntoCache(UINT uItem, UINT uCount, PDWORD pData);
		void	GetDate(UINT uItem, UINT uCount, PDWORD pData);
		DWORD	GetDateValue(UINT uPos);
		void	PutDate(void);
		void	GetEventFromCache(UINT uItem, UINT uCount, PDWORD pData);
		void	PutEventIntoCache(void);
		DWORD	GetEventDec(PBYTE *ppR);
		BYTE	HexBytesToByte(PBYTE *ppR);
		BYTE	xtob(BYTE b);

		BOOL	Resp(void);
		void	ClearCheck(void);
		void	AddToCheck(BYTE bData);
		UINT	GetWordData(PDWORD pData, UINT uCount);
		UINT	GetByteData(PDWORD pData, UINT uCount);
		UINT	GetBitsData(PDWORD pData, UINT uCount);
		CCODE	ReadBitBytes(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	ReadDataWords(AREF Addr, PDWORD pData, UINT uCount);
	};

// End of File
