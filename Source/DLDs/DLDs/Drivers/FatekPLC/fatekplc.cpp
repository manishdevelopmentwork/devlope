
#include "intern.hpp"

#include "fatekplc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Serial Driver
//

// Instantiator

INSTANTIATE(CFatekPLCSerialDriver);

// Constructor

CFatekPLCSerialDriver::CFatekPLCSerialDriver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Destructor

CFatekPLCSerialDriver::~CFatekPLCSerialDriver(void)
{
	}

// Configuration

void MCALL CFatekPLCSerialDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CFatekPLCSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CFatekPLCSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CFatekPLCSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CFatekPLCSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pBase = m_pCtx;

			BYTE b = GetByte(pData);

			m_pCtx->Drop[0]   = m_pHex[b/16];
			m_pCtx->Drop[1]   = m_pHex[b%16];

			m_pCtx->m_fGlobal = !b;

			m_pCtx->m_dLatestError = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CFatekPLCSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Transport Layer

BOOL CFatekPLCSerialDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CFatekPLCSerialDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CFatekPLCSerialDriver::GetReply(void)
{
	if( m_pCtx->m_fGlobal ) return TRUE;

	UINT uState = 0;

	UINT uCount = 0;

	UINT uTimer = 1000;

	UINT uData;

	SetTimer(uTimer);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
			}

		BYTE bData      = LOBYTE(uData);

		m_bRx[uCount++] = bData;

//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				uState = bData == STX ? 1 : 0;
				break;

			case 1:
				uState = bData == m_pCtx->Drop[0] ? 2 : 0;
				break;

			case 2:
				uState = bData == m_pCtx->Drop[1] ? 3 : 0;
				break;

			case 3:
				uState = bData == m_bTx[3] ? 4 : 0;
				break;

			case 4:
				uState = bData == m_bTx[4] ? 5 : 0;
				break;

			case 5:
				if( bData == '0' ) uState = 6;

				else {
					uState = 10;

					m_pCtx->m_dLatestError  = m_bTx[3] << 24;
					m_pCtx->m_dLatestError += m_bTx[4] << 16;
					m_pCtx->m_dLatestError += bData;
					}
				break;

			case 6:
				if( bData == ETX ) {

					return CheckReply(uCount);
					}
				break;

			case 10:
				if( bData == ETX ) {

					return FALSE;
					}
				break;
			}

		if( uCount >= sizeof(m_bRx) ) {

			return FALSE;
			}
		}	
 
	return FALSE;
	}

// Port Access

void CFatekPLCSerialDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT CFatekPLCSerialDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
