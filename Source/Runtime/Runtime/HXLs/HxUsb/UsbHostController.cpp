
#include "Intern.hpp"

#include "UsbHostController.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Controller Driver
//

// Instantiator

IUsbHostControllerDriver * Create_UsbHostController(void)
{
	CUsbHostController *p = New CUsbHostController;

	return p;
	}

// Constructor

CUsbHostController::CUsbHostController(void)
{
	m_pName	    = "Host Controller Driver";

	m_pEnhanced = NULL;

	m_pPorts    = NULL;

	m_uPorts    = 0;

	memset(m_pCompanion, 0, sizeof(m_pCompanion));
	}

// IUsbDriver

BOOL CUsbHostController::Init(void)
{
	if( m_pEnhanced ) {

		MakePorts();

		if( !m_pEnhanced->Init() ) {

			return false;
			}

		for( UINT i = 0; i < elements(m_pCompanion); i ++ ) {
	
			if( m_pCompanion[i] && !m_pCompanion[i]->Init() ) {

				return false;
				}
			}

		return true;
		}
	
	return false;
	}

BOOL CUsbHostController::Start(void)
{
	if( m_pEnhanced ) {

		InitPorts();

		if( !m_pEnhanced->Start() ) {

			return false;
			}
	
		for( UINT i = 0; i < elements(m_pCompanion); i ++ ) {
	
			if( m_pCompanion[i] && !m_pCompanion[i]->Start() ) {

				return false;
				}
			}

		return true;
		}
	
	return false;
	}

BOOL CUsbHostController::Stop(void)
{
	if( m_pEnhanced ) {

		if( !m_pEnhanced->Stop() ) {

			return false;
			}
	
		for( UINT i = 0; i < elements(m_pCompanion); i ++ ) {
	
			if( m_pCompanion[i] && !m_pCompanion[i]->Stop() ) {

				return false;
				}
			}

		return true;
		}
	
	return false;
	}
		
// IUsbHostControllerDriver

void CUsbHostController::Poll(UINT uLapsed)
{
	PollPorts(uLapsed);
	}

void CUsbHostController::EnableEvents(BOOL fEnable)
{
	if( m_pEnhanced ) {

		m_pEnhanced->EnableEvents(fEnable);
		}
	}

BOOL CUsbHostController::ResetPort(UINT uPort)
{
	if( uPort < m_uPorts ) {
		
		CPort &Port = m_pPorts[uPort];

		Port.m_uFlags |= flagReqReset;

		return TRUE;
		}

	return FALSE;
	}

// Host Interface Drivers

IUsbHostInterfaceDriver * CUsbHostController::GetInterface(UINT iHost)
{
	return iHost == 0 ? GetEnhanced() : GetCompanion(iHost - 1);
	}

IUsbHostEnhancedDriver * CUsbHostController::GetEnhanced(void)
{
	return m_pEnhanced;
	}

IUsbHostInterfaceDriver * CUsbHostController::GetCompanion(UINT i)
{
	return i < elements(m_pCompanion) ? m_pCompanion[i] : NULL;
	}

// IUsbEvents

void CUsbHostController::OnBind(IUsbDriver *pDriver)
{
	IUsbHostInterfaceDriver *pHost = (IUsbHostInterfaceDriver *) pDriver;
		
	switch( pHost->GetType() ) {

		case usbEhci:

			if( !m_pEnhanced ) {

				m_pEnhanced = (IUsbHostEnhancedDriver *) pDriver;

				m_pEnhanced->SetIndex(0);
				}

			break;

		case usbOhci:
		case usbUhci:

			for( UINT i = 0; i < elements(m_pCompanion); i ++ ) {

				if( !m_pCompanion[i] ) {

					m_pCompanion[i] = pHost;

					m_pCompanion[i]->SetIndex(i+1);
					
					break;
					}
				}

			break;
		}
	}
	
// IUsbHostInterfaceEvents

void CUsbHostController::OnPortConnect(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostController::OnPortConnect(iPort=%d)\n", Path.a.dwPort);*/

	if( Path.a.dwPort < m_uPorts ) {

		CPort &Port = m_pPorts[Path.a.dwPort];
		
		if( Port.m_uState == stateDetached ) {

			Port.m_pHost  = GetInterface(Path.a.dwPort);
			
			Port.m_uState = stateAttached;

			Port.m_uTimer = 100;
			}
		}
	}

void CUsbHostController::OnPortRemoval(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostController::OnPortRemoval(iPort=%d)\n", Path.a.dwPort);*/

	if( Path.a.dwPort < m_uPorts ) {

		CPort &Port = m_pPorts[Path.a.dwPort];
		
		if( Port.m_uState != stateDetached ) {

			Port.m_uState = stateDetached;

			Port.m_pHost  = m_pEnhanced;

			OnDeviceRemoval(Path);
			}
		}
	}

void CUsbHostController::OnPortCurrent(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostController::OnPortCurrent(iPort=%d)\n", Path.a.dwPort);*/

	CPort &Port = m_pPorts[Path.a.dwPort];

	if( Port.m_uState != stateOverCurrent ) {

		Port.m_pHost->SetPortPower (Path.a.dwPort, false);

		Port.m_pHost->SetPortEnable(Path.a.dwPort, false);
		
		Port.m_uState = stateOverCurrent;

		Port.m_uTimer = 10000;
		}
	}

// Ports

void CUsbHostController::MakePorts(void)
{
	m_uPorts = FindPorts();

	m_pPorts = New CPort[ m_uPorts ];

	memset(m_pPorts, 0, m_uPorts * sizeof(CPort));
	}

void CUsbHostController::InitPorts(void)
{
	for( UINT i = 0; i < m_uPorts; i ++ ) {

		m_pPorts[i].m_uState = stateDetached;

		m_pPorts[i].m_pHost  = m_pEnhanced;
		}
	}

void CUsbHostController::KillPorts(void)
{
	if( m_pPorts ) {

		free(m_pPorts);

		m_pPorts = NULL;

		m_uPorts = 0;
		}
	}

UINT CUsbHostController::FindPorts(void)
{
	return m_pEnhanced->GetPortCount();
	}

void CUsbHostController::PollPorts(UINT uLapsed)
{
	UsbPortPath Path = { { 0 } };

	Path.a.dwCtrl = m_iIndex;

	for( UINT i = 0; i < m_uPorts; i ++ ) {

		CPort &Port = m_pPorts[i];

		switch( Port.m_uState ) {

			case stateDetached:

				break;

			case stateAttached:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_uState = statePowered;
					}
				
				break;
				
			case statePowered:

				Port.m_pHost->SetPortEnable(i, false);

				Port.m_pHost->SetPortReset (i, true);

				Port.m_uState = stateReset;

				Port.m_uTimer = 10;

				break;
				
			case stateReset:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_pHost->SetPortReset(i, false);
	
					Port.m_uState = stateCheckSpeed;
					}
				
				break;

			case stateCheckSpeed:

				if( !Port.m_pHost->GetPortReset(i) ) {

					if( Port.m_pHost == m_pEnhanced ) {

						if( !Port.m_pHost->GetPortEnabled(i) ) {

							m_pEnhanced->SetPortOwner(i, true);

							Port.m_pHost  = NULL;

							Port.m_uState = stateDetached;

							break;
							}
					
						Port.m_uSpeed = usbSpeedHigh;
						}
					else {
						Port.m_uSpeed = Port.m_pHost->GetPortSpeed(i);
						}

					Port.m_uState = stateResetDelay;

					Port.m_uTimer = 10;
					}

				break;
				
			case stateResetDelay:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_uState = stateConnected;

					Path.a.dwPort = i;
				
					Path.a.dwHost = Port.m_pHost->GetIndex();

					OnDeviceArrival(Path);
					}
				
				break;

			case stateConnected:

				if( Port.m_uFlags & flagReqReset ) {

					Port.m_uFlags &= ~flagReqReset;

					Path.a.dwPort = i;
				
					Path.a.dwHost = Port.m_pHost->GetIndex();

					OnPortRemoval(Path);

					Port.m_pHost->SetPortPower(i, false);

					Port.m_uState = stateRestart;

					Port.m_uTimer = 1000;
					}
				
				break;

			case stateRestart:
			case stateOverCurrent:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_uState = stateDetached;

					Port.m_pHost->SetPortPower(i, true);
					}
				
				break;
			}
		}
	}

// End of File
