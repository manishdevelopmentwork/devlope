
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CipClient_HPP

#define	INCLUDE_CipClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CipCommon.hpp"

#include "CipPath.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCipConnect;
class CCipPath;

//////////////////////////////////////////////////////////////////////////
//
// CIP Client
//

class CCipClient : public CCipCommon
{
	public:
		// Constructor
		CCipClient(CCipConnect *pConnect);

		// Destructor
		~CCipClient(void);

		// Attributes
		bool IsLayerUp(void) const;

		// Operations
		bool Open(void);
		UINT ReadTag (PCTXT pName, PDWORD pData, UINT uCount, CCipPath const &TargetPath);
		UINT WriteTag(PCTXT pName, PDWORD pData, UINT uCount, CCipPath const &TargetPath);
		bool Close(void);

		UINT CreateWatchList(void);
		bool ConfigureWatchList(CCipPath const &Path, UINT uInst, UINT uSize);
		bool DeleteWatchList(UINT uInst);

		bool GetTagInfo(PCTXT pName, CCipPath &Path, UINT &uSize, UINT &uType, UINT &uExtra);
		bool GetWatchListData(PBYTE pData, UINT uInst, UINT uSize);
		void AddTagToKeep(PCTXT pName);
		void AllocKeepTags(UINT uKeep);

	protected:

		struct CTag
		{
			UINT     m_uIndex;
			char  *	 m_pName;
			WORD	 m_ProgHandle;
			UINT	 m_uType;
			UINT	 m_uSize;
			WORD	 m_wOffset;
			WORD	 m_wExtra;
			CCipPath m_Path;
			bool	 m_fInit;
			};

		struct CStructType
		{
			UINT	m_uType;
			WORD	m_wMembers;
			WORD	m_wSize;
			PBYTE	m_pStruct;
			};

		// Data Members
		CCipConnect   * m_pConnect;
		bool	        m_fLayerUp;
		CTag	      *	m_pTags;
		CTag	      * m_pStructTags;
		UINT		m_uStructTags;
		UINT		m_uTags;
		UINT		m_uAlloc;
		UINT		m_uStructAlloc;
		PTXT	      * m_ppNames;
		UINT	        m_uKeepNames;
		UINT		m_uKeepAlloc;
		bool		m_fSort;
		CStructType   * m_pCached;
		UINT		m_uCache;
		UINT		m_uCacheAlloc;

		// Layer Transition
		bool LayerUp(void);
		bool LayerDown(void);
		void ClearData(void);

		// Tag Management
		void AddTag(CTag const &Tag);
		bool ShouldKeepTag(CTag const &Tag);
		bool ShouldKeepStruct(CTag const &Tag);
		void AddStruct(CTag const &Tag);
		void FreeTags(CTag *&pTags, UINT &uAlloc, UINT &uCount);
		void FreeKeepTags(void);
		void SortTags(void);

		// Sort/Search Comparison Functions
		static int CompFunc(void const *p1, void const *p2);
		static int CompNames(void const *p1, void const *p2);
		static int CompStruct(void const *p1, void const *p2);

		// Tag Metadata
		bool FindAvailableTags(void);
		bool FindTags(PCTXT pScope);
		WORD FindProgramHandle(PCTXT pName);
		bool ReadStructMetaData(CTag &Tag);
		bool FindStructInfo(CTag const &Tag, CStructType &Struct);
		void FindMemberStrings(CTag const &Tag, CStructType const &Struct, char ** &ppNames);
		bool GetCachedStruct(UINT uType, CStructType &Struct);
		void AddCachedStruct(CStructType const &UDT);
		void ClearStructCache(void);
		bool GetStructSizes(CTag const &Tag, WORD &wSize, WORD &wMembers);
		UINT GetAtomicSize(UINT uType);
		void SetBitOffset(CTag &Tag);
	};

// End of File

#endif
