/* ========================================================================
 * Copyright (c) 2005-2018 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

/******************************************************************************************************/
/** @file platform indepented implementation of the socketmanager/socket architecture                 */
/******************************************************************************************************/

/* System Headers */
#include <StdEnv.hpp>

/* File Header */
AfxFileHeader();

/* UA platform definitions */
#include <opcua_p_internal.h>

/* UA platform definitions */
#include <opcua_datetime.h>

#include <opcua_p_semaphore.h>
#include <opcua_p_thread.h>
#include <opcua_p_mutex.h>
#include <opcua_p_utilities.h>
#include <opcua_p_memory.h>

/* own headers */
#include <opcua_p_aeon_socket_manager.h>

/* Aeon Socket Manager */
#include "AeonSocketManager.hpp"

/*============================================================================
 * Create a new socket manager or initialize the global one (OpcUa_Null first).
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_SocketManager_Create( OpcUa_SocketManager*    a_pSocketManager,
                                                                   OpcUa_UInt32            a_nSockets,
                                                                   OpcUa_UInt32            a_nFlags)
{
    CAeonSocketManager *pManager = New CAeonSocketManager(a_nSockets, a_nFlags);

    if( pManager->Open() )
    {
	*a_pSocketManager = (OpcUa_SocketManager) pManager;

        return OpcUa_Good;
    }

    delete pManager;

    return OpcUa_Bad;
}

/*============================================================================
 * Delete SocketManager Type (closes all sockets)
 *===========================================================================*/
clink OpcUa_Void OPCUA_DLLCALL OpcUa_P_SocketManager_Delete(OpcUa_SocketManager* a_pSocketManager)
{
    CAeonSocketManager *pManager = (CAeonSocketManager *) *a_pSocketManager;

    pManager->Close();

    pManager->Release();

    *a_pSocketManager = NULL;
}

/*============================================================================
 * Create a server socket
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_SocketManager_CreateServer( OpcUa_SocketManager         a_pSocketManager,
                                                                         OpcUa_StringA               a_sAddress,
                                                                         OpcUa_Boolean               a_bListenOnAllInterfaces,
                                                                         OpcUa_Socket_EventCallback  a_pfnSocketCallBack,
                                                                         OpcUa_Void*                 a_pCallbackData,
                                                                         OpcUa_Socket*               a_pSocket)
{
    CAeonSocketManager *pManager = (CAeonSocketManager *) a_pSocketManager;
    
    return pManager->CreateServer(a_sAddress, a_bListenOnAllInterfaces, a_pfnSocketCallBack, a_pCallbackData, a_pSocket);
}

/*============================================================================
 * Create a client socket
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_SocketManager_CreateClient( OpcUa_SocketManager         a_pSocketManager,
                                                                         OpcUa_StringA               a_sRemoteAddress,
                                                                         OpcUa_UInt16                a_uLocalPort,
                                                                         OpcUa_Socket_EventCallback  a_pfnSocketCallBack,
                                                                         OpcUa_Void*                 a_pCallbackData,
                                                                         OpcUa_Socket*               a_pSocket)
{
    CAeonSocketManager *pManager = (CAeonSocketManager *) a_pSocketManager;
    
    return pManager->CreateClient(a_sRemoteAddress, a_uLocalPort, a_pfnSocketCallBack, a_pCallbackData, a_pSocket);
}

/*============================================================================
 * Call the main serve loop for a certain socket list.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_SocketManager_ServeLoop( OpcUa_SocketManager a_pSocketManager,
                                                                      OpcUa_UInt32        a_msecTimeout,
                                                                      OpcUa_Boolean       a_bRunOnce)
{
    // Not used in multithreaded mode?

    AfxAssert(FALSE);

    return OpcUa_Good;
}

/*============================================================================
 * Signal a certain event on a socket.
 *===========================================================================*/
clink OpcUa_StatusCode OPCUA_DLLCALL OpcUa_P_SocketManager_SignalEvent( OpcUa_SocketManager pSocketManager,
                                                                        OpcUa_UInt32        uEvent,
                                                                        OpcUa_Boolean       bAllManagers)
{
    // Not used in multithreaded mode?

    AfxAssert(FALSE);

    return OpcUa_Good;
}
