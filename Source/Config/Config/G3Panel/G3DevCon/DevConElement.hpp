
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElement_HPP

#define INCLUDE_DevConElement_HPP

//////////////////////////////////////////////////////////////////////////
//
// Actions
//

enum
{
	actionNone,
	actionChange,
	actionLayout
};

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration UI Element
//

class CDevConElement : public CObject
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElement(void);

	// Destructor
	~CDevConElement(void);

	// Creation
	static CDevConElement * Create(CString const &Type);

	// Operations
	virtual BOOL    IsReadOnly(void);
	virtual void	BindItem(CItem *pItem);
	virtual void    SetPersonality(CJsonData *pPerson);
	virtual void    ParseConfig(CJsonData *pSchema, CJsonData *pField);
	virtual void    AddLayout(CLayFormation *pForm);
	virtual void    CreateControls(CWnd &Wnd, UINT &id);
	virtual void    DestroyControls(void);
	virtual void    ShowControls(BOOL fShow);
	virtual void	EnableControls(BOOL fEnable);
	virtual BOOL	FindFocus(CWnd * &pWnd);
	virtual UINT    OnNotify(UINT uID, UINT uNotify, CWnd &Wnd);
	virtual UINT    OnNotify(UINT uID, NMHDR &Info);
	virtual BOOL    CanAcceptData(IDataObject *pData, DWORD &dwEffect);
	virtual BOOL    AcceptData(IDataObject *pData);
	virtual CString FormatData(CString const &Data);
	virtual void    SetData(CString const &Data);
	virtual CString GetData(void);
	virtual CString GetDefault(void);
	virtual void	ScrollData(UINT uCode);

protected:
	// Typedefs
	typedef CArray<CCtrlWnd *> CCtrlList;

	// Data Members
	CCtrlList   m_Controls;
	BOOL        m_fShow;
	CItem	  * m_pItem;
	CJsonData * m_pPerson;

	// Overridables
	virtual BOOL IsControlEnabled(CCtrlWnd *pCtrl);

	// Implementation
	void AddControl(CCtrlWnd *pCtrl);
};

// End of File

#endif
