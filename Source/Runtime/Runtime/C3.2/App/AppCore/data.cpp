
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

#include "g3slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Access Service
//

class CDataAccess : public ILinkService
{
	public:
		// Constructor
		SLOW CDataAccess(void);

		// ILinkService
		void SLOW Timeout(void);
		UINT SLOW Process(CLinkFrame &Req, CLinkFrame &Rep);
		void SLOW EndLink(CLinkFrame &Req);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Access Loader Service
//

global	ILinkService *	Create_DataService(void)
{
	return New CDataAccess;
	}

// Constructor

CDataAccess::CDataAccess(void)
{
	}

// ILinkService

void CDataAccess::Timeout(void)
{
	}

UINT CDataAccess::Process(CLinkFrame &Req, CLinkFrame &Rep)
{
	if( Req.GetService() == servData ) {

		if( Req.GetOpcode() == dataListRead ) {

			if( CSystemItem::m_pThis ) {

				IDataServer *pData = NULL;

				CSystemItem::m_pThis->GetDataServer(pData);

				if( pData ) {

					UINT uPtr   = 0;

					UINT uCount = Req.ReadWord(uPtr);

					Rep.StartFrame(servData, dataListRead);

					Rep.AddWord   (WORD(uCount));

					for( UINT n = 0; n < uCount; n++ ) {

						DWORD Ref = Req.ReadLong(uPtr);

						pData->SetScan(Ref, scanOnce);

						if( pData->IsAvail(Ref) ) {

							DWORD Data = pData->GetData(Ref, typeVoid, getNoString);

							Rep.AddByte(1);

							Rep.AddLong(Data);
							}
						else
							Rep.AddByte(0);
						}

					Sleep(50);

					return procOkay;
					}
				}
			
			UINT uPtr   = 0;

			UINT uCount = Req.ReadWord(uPtr);

			Rep.StartFrame(servData, dataListRead);

			Rep.AddWord   (WORD(uCount));

			for( UINT n = 0; n < uCount; n++ ) {

				Rep.AddByte(0);
				}

			Sleep(50);

			return procOkay;
			}
		}

	return procError;
	}

void CDataAccess::EndLink(CLinkFrame &Req)
{
	}

// End of File
