
#include "modbus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via Modbus Driver
//

class CSchModDriver : public CModbusDriver
{
	public:
		// Constructor
		CSchModDriver(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
	
	protected:

		// Device Data
		struct CSchntext :CModbusDriver::CContext
		{
			BYTE m_bByteR;
			BYTE m_bByteL;
			};

		// Data Members
		CSchntext * m_pSCtx;

		// Handlers
		CCODE ReadSchneider(AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteSchneider(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Long Manipulation
		DWORD OrderBytes(DWORD dwValue, UINT uType);
		DWORD SwapWords(DWORD dwWord);
		DWORD SwapBytes(DWORD dwWord);
	};

// End of file

