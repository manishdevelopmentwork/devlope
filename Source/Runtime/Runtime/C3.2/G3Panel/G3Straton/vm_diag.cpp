
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Virtual Machine
//

// IDiagProvider

UINT CVirtualMachine::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case diagStatus: return DiagStatus(pOut, pCmd);
		case diagStart:	 return DiagStart (pOut, pCmd);
		case diagStop:	 return DiagStop  (pOut, pCmd);
		case diagVars:	 return DiagVars  (pOut, pCmd);
		}

	return 0;
	}

// Diagnostics

BOOL CVirtualMachine::AddDiagCmds(void)
{
	#if defined(_DEBUG)	

	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);
	
	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "vm");

		m_pDiag->RegisterCommand(m_uProv, CVirtualMachine::diagStatus, "status");
		m_pDiag->RegisterCommand(m_uProv, CVirtualMachine::diagStart,  "start" );
		m_pDiag->RegisterCommand(m_uProv, CVirtualMachine::diagStop,   "stop"  );
		m_pDiag->RegisterCommand(m_uProv, CVirtualMachine::diagVars,   "vars"  );

		return TRUE;
		}

	#endif	

	return FALSE;
	}

// Commands

UINT CVirtualMachine::DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		pOut->AddProp("Status",  "%u", m_uState);

		pOut->EndPropList();

		return 0;
		}

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CVirtualMachine::DiagStart(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	if( pCmd->GetArgCount() == 1 ) {

		if( !strcmp(pCmd->GetArg(0), "cold") ) {

			SetState(vmOpen);

			return 0;
			}
		}

	SetState(vmOpen);

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CVirtualMachine::DiagStop(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	#if defined(_DEBUG)	

	SetState(vmKill);

	return 0;

	#endif

	pOut->Error(NULL);

	return 1;
	}

UINT CVirtualMachine::DiagVars(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	pOut->Error(NULL);

	return 1;
	}

// End of File
