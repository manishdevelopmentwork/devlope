
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CdcNtbParam_HPP

#define	INCLUDE_CdcNtbParam_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cdc Framework
//

#include "Cdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// NTB Param Block
//

class CCdcNtbParam : public CdcNtbParam
{
	public:
		// Constructor
		CCdcNtbParam(void);

		// Endianess
		void HostToCdc(void);
		void CdcToHost(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
