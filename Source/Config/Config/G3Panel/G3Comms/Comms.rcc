
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Bitmaps
//

CommsTreeIcon16 BITMAP "Images/tree16.bmp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Tree Toolbar
//

CommsTreeTool MENU
BEGIN
	POPUP    "40000010|New"
	BEGIN
	MENUITEM "Comms Device",		IDM_COMMS_ADD_DEVICE
	MENUITEM "Gateway Block",		IDM_COMMS_ADD_BLOCK
	MENUITEM "Network Port",		IDM_COMMS_ADD_PORT
	MENUITEM "Virtual Port",		IDM_COMMS_ADD_VIRTUAL
	END
	MENUITEM SEPARATOR
	MENUITEM "10000008",			IDM_ITEM_DELETE
END

//////////////////////////////////////////////////////////////////////////
//
// Comms Tree Context Menu
//

CommsTreeCtxMenu MENU
BEGIN
	POPUP "CCM"
	BEGIN
		MENUITEM "Expand",		IDM_ITEM_EXPAND
		MENUITEM SEPARATOR
		MENUITEM "Expand Bits",		IDM_COMMS_MAP_EXPAND
		MENUITEM "Collapse Bits",	IDM_COMMS_MAP_COLLAPSE
		MENUITEM SEPARATOR
		MENUITEM "Add Device",		IDM_COMMS_ADD_DEVICE
		MENUITEM "Add Block",		IDM_COMMS_ADD_BLOCK
		MENUITEM SEPARATOR
		MENUITEM "Add Network Port",	IDM_COMMS_ADD_PORT
		MENUITEM "Add Virtual Port",	IDM_COMMS_ADD_VIRTUAL
//		MENUITEM SEPARATOR
//		MENUITEM "Add Expansion Rack",	IDM_COMMS_ADD_RACK
		MENUITEM SEPARATOR
		MENUITEM "Sync Panes",		IDM_ITEM_SYNC
		MENUITEM SEPARATOR
		MENUITEM "Cut",			IDM_EDIT_CUT
		MENUITEM "Copy",		IDM_EDIT_COPY
		MENUITEM "Paste",		IDM_EDIT_PASTE
		MENUITEM "Delete",		IDM_EDIT_DELETE
		MENUITEM SEPARATOR
		MENUITEM "Find Usage",		IDM_ITEM_USAGE
		MENUITEM SEPARATOR
		MENUITEM "Rename",		IDM_ITEM_RENAME
		MENUITEM SEPARATOR
		MENUITEM "Watch Block",		IDM_COMMS_WATCH_BLOCK
	END
END

//////////////////////////////////////////////////////////////////////////
//
// Item Schemas
//

CCommsManager_Schema RCDATA
BEGIN
	L"NoComms,Communications,,Enum/DropDown,Enabled|Disabled,"
	L"Change this setting to Disabled to prevent all communications "
	L"by Crimson and to allow testing without the presence of remote "
	L"communications devices."
	L"\0"

	L"StrPad,String Padding,,Enum/DropDown,Space|NUL,"
	L"Select the character that should be used to pad string tags "
	L"when they are written to a remote device. This character will "
	L"be used to extend the string from its actual size to its maximum."
	L"\0"

	L"OnEarly,On Early,,Coded/Action,|None,"
	L"Define an action to be executed before any services start."
	L"\0"

	L"OnStart,On Startup,,Coded/Action,|None,"
	L"Define an action to be executed at startup."
	L"\0"

	L"OnApply,On Apply,,Coded/Action,|None,"
	L"Define an action to be executed to apply a custom personality."
	L"\0"

	L"OnSecond,On Tick,,Coded/Action,|None,"
	L"Define an action to be executed each second."
	L"\0"

	L"LedAlarm,Alarm LED,,Enum/DropDown,System|User Configured,"
	L"Specify whether the Alarm Icon LED should follow the system "
	L"behavior or configurable by the user."
	L"\0"

	L"LedOrb,Orb LED,,Enum/DropDown,System|User Configured,"
	L"Specify whether the Orb Icon LED should follow the system "
	L"behavior or configurable by the user."
	L"\0"

	L"LedHome,Home LED,,Enum/DropDown,System|User Configured,"
	L"Specify whether the Home Icon LED should follow the system "
	L"behavior or configurable by the user."
	L"\0"

	L"\0"
END

COptionCardItem_Schema RCDATA
BEGIN
	L"Type,Option Card,,OptionCard/Pick,,"
	L"Select the type of option card installed in the Crimson device."
	L"\0"

	L"\0"
END

CTetheredItem_Schema RCDATA
BEGIN
	L"Port,Tethered Port,,Enum/DropDown,USB Host Port A|USB Host Port B,"
	L"Select the USB port used for the tethered rack connection."
	L"\0"

	L"\0"
END

CCommsPort_Schema RCDATA
BEGIN
	L"DriverID,Driver,,CommsDriver/Pick,,"
	L"Select the communications driver for this port. You will be offered a list of "
	L"options based on the type of port you are configuring. After the driver has been "
	L"selected, a device will be created. If the driver supports multi-drop operation, "
	L"you will have the option of creating additional devices."
	L"\0"

	L"\0"
END

CCommsPortSerial_Schema RCDATA
BEGIN
	L"ShareEnable,Share Port,,ExprEnum/ExprDropEdit,No|Yes,"
	L"Select whether or not you want to allow remote access to this port using "
	L"a virtual serial port driver. This facility can be used to provide remote "
	L"PLC programming, or to otherwise access a device from a PC. Note that comms "
	L"via the port will be suspended while remote access is taking place."
	L"\0"

	L"SharePort,TCP Port,,ExprInteger/ExprEditBox,|0||0|9999,"
	L"Select the TCP/IP port number to be used for remote access. Leaving this value at "
	L"zero will select a port number of 4000 plus the logical port index. Take care not "
	L"to allocate the same TCP/IP port number to more than one serial port."
	L"\0"

	L"Baud,Baud Rate,,ExprEnumInt/ExprDropEdit,300|187500|300|600|1200|2400|4800|9600|19200|38400|57600|115200,"
	L"Select the speed at which the port should communicate. The protocol driver will "
	L"have already loaded in a suitable setting, but you should check to ensure that the "
	L"target device is configured to use the same speed."
	L"\0"

	L"Data,Data Bits,,ExprEnum/ExprDropEdit,Eight|Seven,"
	L"Select the number of data bits in each byte. The protocol driver will "
	L"have already loaded in a suitable setting, and it will be rare for you to have to "
	L"make adjustments to the default."
	L"\0"

	L"Stop,Stop Bits,,ExprEnum/ExprDropEdit,One|Two,"
	L"Select the number of stop bits in each byte. The protocol driver will "
	L"have already loaded in a suitable setting, and it will be rare for you to have to "
	L"make adjustments to the default."
	L"\0"

	L"Parity,Parity,,ExprEnum/ExprDropEdit,None|Odd|Even,"
	L"Select the type of parity to be appended to each byte. The protocol driver will "
	L"have already loaded in a suitable setting, and it will be rare for you to have to "
	L"make adjustments to the default."
	L"\0"

	L"Mode,Port Mode,,ExprEnum/ExprDropEdit,2-Wire RS485|4-Wire RS485 or RS422,"
	L"Specify the mode in which the port is to operate."
	L"\0"

	L"\0"
END

CCommsPortVirtual_Schema RCDATA
BEGIN
	L"Mode,Mode,,ExprEnum/ExprDropEdit,Active|Passive,"
	L"Select the mode that the virtual port will use. Active will attempt "
	L"to open a connection to the speicified remote device. Passive will "
	L"listen on the specified TCP port for incoming connections. "
	L"\0"

	L"IP,IP Address,,ExprIPAddress/ExprEditBox,,"
	L"Specify the IP Address that the virtual port will use. "
	L"\0"

	L"Port,TCP Port,,ExprInteger/ExprEditBox,|0||0|65535,"
	L"Specify the TCP port for the virtual port to use. "
	L"\0"

	L"LinkOpt,Idle Link Options,,ExprEnum/ExprDropEdit,Default|Keep-Alive|Timeout,"
	L"Specify the behavior to use when the passive virtual port does not receive "
	L"a packet for a period of time. The default behavior can leave the connection "
	L"half-open if the remote device goes offline, but has no additional overhead. "
	L"Keep-Alive will use TCP keep-alives to determine if the connection has been "
	L"lost. Timeout will abort the connection after there is no activity for the "
	L"configured time period. "
	L"\0"

	L"Timeout,Inactivity Timeout,,ExprInteger/ExprEditBox,|0||0|65535|ms,"
	L"Specify the timeout value to be used when the link is idle. After the "
	L"timeout period passes with no activity, the TCP connection will be closed "
	L"\0"

	L"\0"
END

CCommsPortCAN_Schema RCDATA
BEGIN
	L"Baud,Baud Rate,,EnumInt/DropEdit,10000|1000000|10000|20000|50000|125000|250000|500000|800000|1000000,"
	L"Select the speed at which the port should communicate. The protocol driver will "
	L"have already loaded in a suitable setting, but you should check to ensure that the "
	L"target device is configured to use the same speed."
	L"\0"

	L"\0"
END

CCommsPortJ1939_Schema RCDATA
BEGIN
	L"Baud,Baud Rate,,EnumInt/DropEdit,10000|250000|10000|20000|50000|125000|250000,"
	L"Select the speed at which the port should communicate. The protocol driver will "
	L"have already loaded in a suitable setting, but you should check to ensure that the "
	L"target device is configured to use the same speed."
	L"\0"

	L"\0"
END

CCommsPortDevNet_Schema RCDATA
BEGIN
	L"Baud,Baud Rate,,EnumInt/DropEdit,125000|500000|125000|250000|500000,"
	L"Select the speed at which the port should communicate. The protocol driver will "
	L"have already loaded in a suitable setting, but you should check to ensure that the "
	L"target device is configured to use the same speed."
	L"\0"

	L"\0"
END

CCommsPortMPI_Schema RCDATA
BEGIN
	L"ThisDrop,Station Address,,Integer/EditBox,|0||7|127,"
	L"Select the drop number for the MPI port."
	L"\0"

	L"\0"
END

CEthernetItem_Schema RCDATA
BEGIN
	L"Routing,IP Routing,,Enum/DropDown,Disabled|Enabled,"
	L"Select whether or not you wish to enable IP routing. If routing "
	L"is enabled, packets will be routed between the panel's various IP "
	L"interfaces. For example, enabling routing will allow a client who "
	L"connects via a PPP modem interface to access not only the operator "
	L"panel itself, but also any other devices connected to the Ethernet "
	L"port."
	L"\0"

	L"Routes,Edit Routing Table,,CUIMoreButton,4,"
	L"Edit the static routes provided to the TCP/IP stack."
	L"\0"

	L"Download,IP Download,,Enum/DropDown,Disabled|Enabled,"
	L"Select whether or not you want to be able to update the panel's firmware "
	L"and configuration via a TCP/IP connection."
	L"\0"

	L"DownPort,Listen Port Number,,Integer/EditBox,,"
	L"Enter the TCP port number on which the target device will listen for download "
	L"operations initiated by the Crimson 3 software from the PC. Note that the "
	L"default value WILL normally be suitable for your application."
	L"\0"

	L"SendPort,Send Port Number,,Integer/EditBox,,"
	L"Enter the TCP port number that the PC will use to download to the "
	L"target device. This will typically be the same as the listen port"
	L"\0"

	L"UseSendPort,Download to Different Port,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this setting to have the PC connect to a different TCP port than the target device "
	L"will be listening on. This is useful when the target device is on a LAN "
	L"behind a router or firewall that requires port forwarding, but most applications "
	L"will not need to modify this setting. "
	L"\0"

	L"DownMode,Selection Mode,,Enum/DropDown,Manual|Auto Ethernet 1|Auto Ethernet 2|Auto Local Name,"
	L"Select the method to be used by this PC to decide which IP address "
	L"to use when downloading to the device. Manual mode allows you to enter "
	L"an IP address here, while the Auto Ethernet modes get the IP address "
	L"from the configuration of the respective port. These modes will not "
	L"work with DHCP. Auto Local Name uses the local name of the unit, and will "
	L"only operate if local name resolution is enabled."
	L"\0"

	L"DownIP,Remote Address,,IPAddress/IPAddress,,"
	L"Enter the address to by used by this PC to download to the device."
	L"\0"

	L"EnableTls,Protocol Support,,Enum/DropDown,Disabled|Enabled,"
	L""
	L"\0"

	L"RootSource,Certificate Source,,Enum/DropDown,Do Not Provide|Read from DER Encoded File|Read from Base64 Encoded File|Obtain from Windows,"
	L""
	L"\0"

	L"RootFile,Certificate File,,File/Pick,,"
	L""
	L"\0"

	L"ZeroEnable,Responders,,Enum/DropDown,Disabled|LLMNR Only|mDNS Only|LLMNR and mDNS,"
	L"Select which local name resolution protocols you wish to enable. For best "
	L"performance, enable all available protocols to give clients more options as "
	L"to how to locate your device."
	L"\0"

	L"ZeroName,Local Name,,String/EditBox,20|Per MAC Address||.local,"
	L"Enter the name by which you wish to refer to this unit. You may use the name followed by "
	L"the .local prefix from any PC or other compatible device on the same subnet as this device. "
	L"For example, entering 'crimson' will allow you to access this device's webserver simply "
	L"by typing 'crimson.local' into your brower's address bar."
	L"\0"

	L"\0"
END

CEthernetRoutes_Schema RCDATA
BEGIN
	L"Use0,Enable,,OnOff/Check,,Enable or disable this routing entry.\0"
	L"Use1,Enable,,OnOff/Check,,Enable or disable this routing entry.\0"
	L"Use2,Enable,,OnOff/Check,,Enable or disable this routing entry.\0"
	L"Use3,Enable,,OnOff/Check,,Enable or disable this routing entry.\0"
	L"Use4,Enable,,OnOff/Check,,Enable or disable this routing entry.\0"
	L"Use5,Enable,,OnOff/Check,,Enable or disable this routing entry.\0"

	L"Dest0,,,IPAddress/IPAddress,,Enter the IP address to be matched by this route.\0"
	L"Dest1,,,IPAddress/IPAddress,,Enter the IP address to be matched by this route.\0"
	L"Dest2,,,IPAddress/IPAddress,,Enter the IP address to be matched by this route.\0"
	L"Dest3,,,IPAddress/IPAddress,,Enter the IP address to be matched by this route.\0"
	L"Dest4,,,IPAddress/IPAddress,,Enter the IP address to be matched by this route.\0"
	L"Dest5,,,IPAddress/IPAddress,,Enter the IP address to be matched by this route.\0"

	L"Mask0,,,IPAddress/IPAddress,,Enter the mask to be used when matching this route.\0"
	L"Mask1,,,IPAddress/IPAddress,,Enter the mask to be used when matching this route.\0"
	L"Mask2,,,IPAddress/IPAddress,,Enter the mask to be used when matching this route.\0"
	L"Mask3,,,IPAddress/IPAddress,,Enter the mask to be used when matching this route.\0"
	L"Mask4,,,IPAddress/IPAddress,,Enter the mask to be used when matching this route.\0"
	L"Mask5,,,IPAddress/IPAddress,,Enter the mask to be used when matching this route.\0"

	L"Gate0,,,IPAddress/IPAddress,,Enter the gateway that is the target for this route.\0"
	L"Gate1,,,IPAddress/IPAddress,,Enter the gateway that is the target for this route.\0"
	L"Gate2,,,IPAddress/IPAddress,,Enter the gateway that is the target for this route.\0"
	L"Gate3,,,IPAddress/IPAddress,,Enter the gateway that is the target for this route.\0"
	L"Gate4,,,IPAddress/IPAddress,,Enter the gateway that is the target for this route.\0"
	L"Gate5,,,IPAddress/IPAddress,,Enter the gateway that is the target for this route.\0"

	L"Metric0,,,Integer/EditBox,|0||1|32,Enter the metric for this route. Lower metrics are used first.\0"
	L"Metric1,,,Integer/EditBox,|0||1|32,Enter the metric for this route. Lower metrics are used first.\0"
	L"Metric2,,,Integer/EditBox,|0||1|32,Enter the metric for this route. Lower metrics are used first.\0"
	L"Metric3,,,Integer/EditBox,|0||1|32,Enter the metric for this route. Lower metrics are used first.\0"
	L"Metric4,,,Integer/EditBox,|0||1|32,Enter the metric for this route. Lower metrics are used first.\0"
	L"Metric5,,,Integer/EditBox,|0||1|32,Enter the metric for this route. Lower metrics are used first.\0"

	L"\0"
END

CEthernetFace_Schema RCDATA
BEGIN
	L"Mode,Port Mode,,ExprEnum/ExprDropEdit,Disabled|Configured via DHCP or APIPA|Manual Configuration|IEEE 802.3 Only,"
	L"Select whether or not the Ethernet port should be enabled, and define how it "
	L"should obtain its configuration. If DHCP mode is selected and a DHCP server "
	L"cannot be found, APIPA will be used to allocate a private IP address from "
	L"the 169.254.0.0/16 0 network until a DHCP server becomes available. If you "
	L"select IEEE 802.3 only operation, the port will be enabled, but it will not "
	L"be allocated an IP address. Only protocols based on IEEE 802.3 will be supported, "
	L"and any protocols that use TCP or UDP will not be able to function. Note that "
	L"IEEE 802.3 is supported whenever the port is enabled -- the specific IEEE mode "
	L"is only used if you wish to disable the IP-based protocols."
	L"\0"

	L"Address,IP Address,,ExprIPAddress/ExprEditBox,,"
	L"Enter the Crimson device's IP address. You will normally obtain this setting "
	L"from your network administrator. Note that the default value WILL NOT most "
	L"likely be suitable for your application."
	L"\0"

	L"NetMask,Network Mask,,ExprIPAddress/ExprEditBox,,"
	L"Enter the mask used to detect whether a given IP address is on the Crimson device's network."
	L"You will normally obtain this setting from your network administrator. Note that "
	L"the default value WILL NOT most likely be suitable for your application."
	L"\0"

	L"Gateway,Gateway,,ExprIPAddress/ExprEditBox,,"
	L"Enter the station to which non-local packets will be sent for routing."
	L"You will normally obtain this setting from your network administrator. Note that "
	L"the default value WILL NOT most likely be suitable for your application."
	L"\0"

	L"Metric,Metric,,ExprInteger/ExprEditBox,|0||1|32,"
	L"Enter the metric for this port. Lower metrics are used first "
	L"when several routing options exist for the same target address."
	L"\0"

	L"DNSMode,DNS Mode,,ExprEnum/ExprDropEdit,Disabled|Automatic|Manual,"
	L"Indicate whether DNS servers are provided by this connection, and whether "
	L"their addresses will be provided automatically during the DHCP negotiation, "
	L"or manually using the fields below."
	L"\0"

	L"DNS1,Name Server 1,,ExprIPAddress/ExprEditBox,,"
	L"Specify the IP address of the primary DNS server for this connection."
	L"\0"

	L"DNS2,Name Server 2,,ExprIPAddress/ExprEditBox,,"
	L"Specify the IP address of the secondary DNS server for this connection."
	L"\0"

	L"EnableFull,Full Duplex,,ExprEnum/ExprDropEdit,Disabled|Enabled,"
	L"Enabling this feature instructs the Ethernet driver to attempt to "
	L"negotiate a full-duplex connection, thereby eliminating collisions "
	L"and speeding-up data transfer. Even if this box is checked, a "
	L"full-duplex link will not be established if the Crimson device is "
	L"connected to a half-duplex hub."
	L"\0"

	L"Enable100,High Speed,,ExprEnum/ExprDropEdit,Disabled|Enabled,"
	L"Enabling this feature instructs the Ethernet driver to attempt to "
	L"negotiate a 100Mbps link, thereby speeding up data transfer. Even "
	L"if this box is checked, a high-speed link will not be established "
	L"if the Crimson device is connected to a 10Mbps hub."
	L"\0"

	L"SendMSS,For Send,,ExprInteger/ExprEditBox,|0||64|2048,"
	L"Enter the maximum segment size to be used for transmission on this port."
	L"\0"

	L"RecvMSS,For Receive,,ExprInteger/ExprEditBox,|0||64|2048,"
	L"Enter the maximum segment size to be advertised for reception on this port."
	L"\0"

	L"\0"
END

CCommsDevice_Schema RCDATA
BEGIN
	L"Enable,Enable Device,,ExprEnum/ExprDropEdit,No|Yes,"
	L"Clear this box to disable a given device for testing. This can be used "
	L"to avoid start-up delays and various other issues when commissioning a "
	L"system that does not yet have all its external devices connected."
	L"\0"

	L"Transact,Transactional Writes,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to ensure that writes are transmitted to the remote "
	L"device in the order they occur, and that intermediate values are written "
	L"in the correct sequence. This feature ensures that, for example, push "
	L"buttons write the ON and OFF values, even if the OFF transitions occurs "
	L"before the ON value has been written."
	L"\0"

	L"Split,Favor UI Writes,,Enum/DropDown,No|Yes,"
	L"Enable thie feature to give writes initiated by the user precedence over other "
	L"writes. This feature can be used to make the user interface more responsive "
	L"on systems with heavy communication activity. However, it should be used with "
	L"caution, as the sequence order of writes can no longer be guaranteed."
	L"\0"

	L"Preempt,Preempt Other Devices,,Enum/DropDown,No|Yes,"
	L"Enable this feature to allow writes to this device to preempt communications "
	L"activity with other devices. This will allow writes to occur more quickly at the "
	L"cost of less predictable read performance. For TCP/IP drivers that are using "
	L"shared sockets, this feature may result in slower overall performance as sockets "
	L"are opened and closed to switch between devices."
	L"\0"

	L"Delay,Comms Delay,,Integer/EditBox,|0|ms|0|60000,",
	L"Select the delay to be inserted between each access to this device. This "
	L"value should normally be left at zero, but can be increased to reduce "
	L"comms loading on devices that cannot keep up with the Crimson device."
	L"\0"

	L"Spanning,Spanning Reads,,Enum/DropDown,Disabled|Enabled,"
	L"Enable this feature to allow the system to read large blocks of data "
	L"that span the required registers but that might also include registers "
	L"that are not being accessed. Disable this feature to ensure that only "
	L"the requested registers are read from the remote device."
	L"\0"

	L"\0"
END

CCommsMapBlock_Schema RCDATA
BEGIN
	L"Addr,Start Address,,MapAddress/Pick,,"
	L"Select the address at which this mapping block will begin. Once the block "
	L"size has also been configured, you will be able to select various data "
	L"items to be written to or read from the block. These items may be tags "
	L"from within Crimson, or registers from other devices. For a master device, "
	L"Crimson will read or write data as required. For a slave device, it will "
	L"process read and writes, either writing to the appropriate destinations, "
	L"or replying with the selected data."
	L"\0"

	L"Size,Block Size,,Integer/EditBox,0|0||0|1024,"
	L"Select the number of registers in the mapping block."
	L"\0"

	L"Write,Direction,,MapDirection/DropDown,,"
	L"Select the direction of data transfer. For master devices, this controls "
	L"the type of operation that Crimson will perform. For slave devices, it "
	L"indicates whether or not the Crimson will accept write operations. Slave "
	L"read operations are accepted regardless of this setting."
	L"\0"

	L"Scaled,Tag Data,,Enum/DropDown,Use Raw Values|Use Manipulated Values|Use Scaled Values,"
	L""
	L"\0"

	L"Update,Update Policy,,Enum/DropDown,Automatic|Continuous|Timed,"
	L""
	L"\0"

	L"Period,Update Period,,Integer/EditBox,0|0|ms|50|30000,"
	L""
	L"\0"

	L"Slave,Comms Scan,,Enum/DropDown,Always|On Demand,"
	L""
	L"\0"

	L"Req,Request,,Coded/Expression,|Always,"
	L""
	L"\0"

	L"Ack,Acknowledge,,Coded/LValue,|None,"
	L""
	L"\0"

	L"\0"
END

CMemoryStickItem_Schema RCDATA
BEGIN
	L"Enable,Enabled,,Enum/DropDown,No|Yes,"
	L"Select whether or not the auto synchronisation features should be enabled. These "
	L"services will operate when a memory stick is inserted."
	L"\0"

	L"Update,Updates,,Enum/DropDown,Disabled|Enabled,"
	L"Select whether or not you want to be able to update the panel's firmware and "
	L"configuration via the memory stick. This feature requires a Memory Card "
	L"to be present in the system." 
	L"\0"

	L"Config,Database,,ExprString/ExprEditBox,31,"
	L"Specify the name of the configuration database file contained on the memory stick."
	L"\0"
	
	L"Folder,Location,,ExprString/ExprEditBox,31|Root Folder,"
	L"Specify the folder on the memory stick containing the update files."
	L"\0"

	L"Reboot,Restart,,Enum/DropDown,Manual|Auto,"
	L"To compelete the udpate process the panel must be restarted. Select Auto to have the panel "
	L"automatically restart after the transfer completes, otherwise select Manual to restart the "
	L"panel at a later time."
	L"\0"

	L"Mode1,Transfer,,Enum/DropDown,Disabled|Copy Files|Move Files|Copy Files and Folders|Move Files and Folders,"
	L"Select the type of transfer to execute when a memory stick is inserted."
	L"\0"

	L"Dir1,Direction,,Enum/DropDown,Memory Card To Memory Stick|Memory Stick to Memory Card,"
	L"Select the direction of the transfer."
	L"\0"

	L"Source1,Source,,String/EditBox,31|Root Folder,"
	L"Specify the file or folder to be transferred."
	L"\0"

	L"Target1,Destination,,String/EditBox,31|Root Folder,"
	L"Specify the name of the new file or folder."
	L"\0"

	L"All1,Update,,Enum/DropDown,New and Modified Files Only|All Files,"
	L"Select the criteria for determining if a file should be tranferred."
	L"\0"

	L"Mode2,Transfer,,Enum/DropDown,Disabled|Copy Files|Move Files|Copy Files and Folders|Move Files and Folders,"
	L"Select the type of transfer to execute when a memory stick is inserted."
	L"\0"

	L"Dir2,Direction,,Enum/DropDown,Memory Card To Memory Stick|Memory Stick to Memory Card,"
	L"Select the direction of the transfer."
	L"\0"

	L"Source2,Source,,String/EditBox,31|Root Folder,"
	L"Specify the file or folder to be transferred."
	L"\0"

	L"Target2,Destination,,String/EditBox,31|Root Folder,"
	L"Specify the name of the new file or folder."
	L"\0"

	L"All2,Update,,Enum/DropDown,New and Modified Files Only|All Files,"
	L"Select the criteria for determining if a file should be tranferred."
	L"\0"

	L"\0"
END

CKeyboardItem_Schema RCDATA
BEGIN
	L"Enable,Enabled,,Enum/DropDown,No|Yes,"
	L"Indicate whether the keyboard driver should be enabled."
	L"\0"

	L"Layout,Keyboard Layout,,Enum/DropDown,United States|United Kingdom,"
	L""
	L"\0"

	L"\0"
END

CMouseItem_Schema RCDATA
BEGIN
	L"Enable,Enabled,,Enum/DropDown,No|Yes,"
	L"Indicate whether the mouse driver should be enabled."
	L"\0"

	L"Rate,Divide Movement By,,Integer/EditBox,|0|%|25|400,"
	L"Indicate the factor by which to divide mouse movement."
	L"\0"

	L"Hide,Hide Pointer After,,Integer/EditBox,|0|secs|0|240,"
	L"Indicater the number of seconds of no mouse activity "
	L"after which the pointer should be hidden. A value of "
	L"zero disables this feature."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Page Schemas
//

CCommsManager_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Global Options,NoComms,StrPad\0"
	L"\0"
END

CCommsManager_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Global Options,NoComms,StrPad\0"
	L"G:1,root,Global Actions,OnEarly,OnStart,OnApply,OnSecond\0"
	L"\0"
END

CCommsManager_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Icon Leds,LedAlarm,LedOrb,LedHome\0"
	L"\0"
END

COptionCardItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Card Selection,Type\0"
	L"\0"
END

CTetheredItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Selection,Port\0"
	L"\0"
END

CCommsPort_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Driver Selection,DriverID\0"
	L"\0"
END

CCommsPortSerial_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Baud,Data,Stop,Parity,Mode\0"
	L"G:2,root,Port Sharing,ShareEnable,SharePort\0"
	L"\0"
END

CCommsPortSerial_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Baud,Data,Stop,Parity\0"
	L"G:2,root,Port Sharing,ShareEnable,SharePort\0"
	L"\0"
END

CCommsPortSerial_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Baud,Data,Stop,Parity,Mode\0"
	L"\0"
END

CCommsPortSerial_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Baud,Data,Stop,Parity\0"
	L"\0"
END

CCommsPortSerial_Page5 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Serial Settings,Baud\0"
	L"\0"
END

CCommsPortVirtual_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Mode,IP,Port,LinkOpt,Timeout\0"
	L"\0"
END

CCommsPortCAN_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Baud\0"
	L"\0"
END

CCommsPortDevNet_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,Baud\0"
	L"\0"
END

CCommsPortMPI_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Port Settings,ThisDrop\0"
	L"\0"
END

CEthernetItem_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Routing Mode,Routing\0"
	L"G:1,root,Static Routes,Routes\0"
	L"\0"
END

CEthernetItem_Page2 RCDATA
BEGIN
	L"P:1\0"
//	L"G:1,root,Remote Update,Download,DownPort\0"
	L"G:1,root,Unit Addressing,DownMode,DownIP,SendPort\0"
	L"\0"
END

CEthernetItem_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Face0,Port Settings,Mode,Address,NetMask,Gateway\0"
	L"G:1,Face0,DNS Settings,DNSMode,DNS1,DNS2\0"
	L"G:1,Face0,Physical Layer,EnableFull,Enable100\0"
	L"G:1,Face0,Maximum Segment Size,SendMSS,RecvMSS\0"
	L"\0"
END

CEthernetItem_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Face1,Port Settings,Mode,Address,NetMask,Gateway\0"
	L"G:1,Face1,DNS Settings,DNSMode,DNS1,DNS2\0"
	L"G:1,Face1,Physical Layer,EnableFull,Enable100\0"
	L"G:1,Face1,Maximum Segment Size,SendMSS,RecvMSS\0"
	L"\0"
END

CEthernetItem_Page5 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Settings,EnableTls\0"
	L"G:1,root,Trusted Roots,RootSource,RootFile\0"
	L"\0"
END

CEthernetItem_Page6 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Settings,ZeroName\0"
	L"G:1,root,Advanced,ZeroEnable\0"
	L"\0"
END

CEthernetRoutes_Page RCDATA
BEGIN
	L"P:1\0"
	L"T:4,Static Routes,,Destination,Mask,Gateway\0"
	L"R:root,1,Use0,Dest0,Mask0,Gate0\0"
	L"R:root,2,Use1,Dest1,Mask1,Gate1\0"
	L"R:root,3,Use2,Dest2,Mask2,Gate2\0"
	L"R:root,4,Use3,Dest3,Mask3,Gate3\0"
	L"R:root,5,Use4,Dest4,Mask4,Gate4\0"
	L"R:root,6,Use5,Dest5,Mask5,Gate5\0"
	L"R:done\0"
	L"\0"
END

CCommsDevice_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Device Settings,Enable\0"
	L"\0"
END

CCommsDevice_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Advanced Settings,Spanning,Transact,Preempt,Split,Delay\0"
	L"\0"
END

CCommsMapBlock_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Block Settings,Addr,Size,Write,Scaled,Update,Period\0"
	L"G:1,root,Block Control,Req,Ack\0"
	L"\0"
END

CMemoryStickItem_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Settings,Enable\0"
	L"G:1,root,Firmware and Configuraton Updates,Update,Config,Folder,Reboot\0"

	L"\0"
END

CMemoryStickItem_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,File Synchronization 1,Mode1,Dir1,All1,Source1,Target1\0"
	L"G:1,root,File Synchronization 2,Mode2,Dir2,All2,Source2,Target2\0"

	L"\0"
END

CKeyboardItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Driver Settings,Enable\0"
	L"G:1,root,Keyboard Selection,Layout\0"

	L"\0"
END

CMouseItem_Page RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Driver Settings,Enable\0"
	L"G:1,root,Pointer Behavior,Rate,Hide\0"

	L"\0"
END

// End of File
