
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LECOM2_HPP
	
#define	INCLUDE_LECOM2_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CLecom2Driver;
class CLecom2Dialog;

//////////////////////////////////////////////////////////////////////////
//
// File and Address Ranges
//

#define	LECOM_EMPTY		0

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Device Options
//

class CLecom2MasterSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLecom2MasterSerialDeviceOptions(void);
				
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
		UINT m_Ping;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Space Wrapper Class
//

class CSpaceLecom2 : public CSpace
{
	public:
		// Constructors
		CSpaceLecom2(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT SubMin, UINT SubMax);

		// Public Data
		UINT m_uSubMin;
		UINT m_uSubMax;

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Driver
//

class CLecom2MasterSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CLecom2MasterSerialDriver(void);

		// Destructor
		~CLecom2MasterSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
	
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Lecom2 Address Selection
//

class CLecom2Dialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CLecom2Dialog(CLecom2MasterSerialDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressFocus(void);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

// End of File

#endif
