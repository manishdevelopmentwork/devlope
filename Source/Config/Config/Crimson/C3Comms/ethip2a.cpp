
#include "intern.hpp"

#include "ethip2a.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#define END_MARKER L"<end>"

#define	MAX_SLOTS 65536

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Driver
//

// Instantiator

ICommsDriver *	Create_EIP2aMasterDriver(void)
{
	return New CEIP2aMasterDriver;
	}

// Constructor

CEIP2aMasterDriver::CEIP2aMasterDriver(void)
{
	m_wID		= 0x40B0;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Native Tags via L5K file Enhanced";
	
	m_Version	= "2.01";
	
	m_ShortName	= "AB L5K Tags";
	}

// Binding Control

UINT CEIP2aMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CEIP2aMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount  = 1;

	Ether.m_UDPCount  = 0;
	}

// Configuration

CLASS CEIP2aMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEIP2aMasterDriverOptions);
	}

CLASS CEIP2aMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEIP2aMasterDeviceOptions);
	}

// Address Management

BOOL CEIP2aMasterDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CEIP2aMasterDeviceOptions *pDev = (CEIP2aMasterDeviceOptions *) pConfig;

	CString Search(Text);

	UINT k = Search.FindRev('.');
	UINT p = Search.FindRev('[');

	CLongArray List;

	// Check if the last element is an array
	if( p < NOTHING && k == NOTHING || k < NOTHING && p > k ) {

		Search = Text.Left(p);

		pDev->ParseDims(List, Text);
		}

	CEIPTagInfo *pTag = pDev->FindTag(Search);

	if( pTag ) {

		Addr = pTag->m_Addr;

		if( Addr.a.m_Table < addrNamed ) {

			UINT uFactor = 1;
				
			UINT uOffset = 0;
			
			if( CheckDims(pTag->m_Dims, List) ) {

				// Calculate offset for up to 3 dimensions
				for( int i = pTag->m_Dims.GetCount() - 1; i >= 0; i-- ) {

					uOffset += List[i] * uFactor;
				
					uFactor *= pTag->m_Dims[i];
					}

				Addr.a.m_Offset = uOffset;
				}
			else {
				Error.Set(CString(IDS_ARRAY_DIMENSIONS));

				return FALSE;
				}
			}

		return TRUE;
		}

	Error.Set(L"The tag does not exist in the available address list for this device.");

	return FALSE;
	}

BOOL CEIP2aMasterDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CEIP2aMasterDeviceOptions *pDev = (CEIP2aMasterDeviceOptions *) pConfig;

	CEIPTagInfo *pTag = NULL;

	if( Addr.a.m_Table >= addrNamed ) {
	
		pTag = pDev->FindTag(Addr.a.m_Offset);

		if( pTag ) {

			Text = pTag->m_Name;

			return TRUE;
			}
		}
	else {
		pTag = pDev->FindTag(Addr.a.m_Table);

		if( pTag ) {

			Text = pTag->m_Name;

			pDev->ExpandDims(Text, pTag->m_Dims, Addr.a.m_Offset);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEIP2aMasterDriver::CheckDims(CLongArray const &MaxDims, CLongArray const &Dims)
{
	if( MaxDims.GetCount() == Dims.GetCount() ) {

		for( UINT n = 0; n < MaxDims.GetCount(); n++ ) {

			if( Dims[n] >= MaxDims[n] || Dims[n] < 0 ){

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CEIP2aMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CEIP2aSelectionDialog Dlg(*this, Addr, pConfig, fPart, TRUE);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CEIP2aMasterDriver::IsAddrNamed(CItem *pConfig, CAddress const &Addr)
{
	return Addr.a.m_Table >= addrNamed;
	}

BOOL CEIP2aMasterDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CEIP2aMasterDeviceOptions *) pConfig)->DoListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// L5K Tag List
//

CEIPL5KList::CEIPL5KList(void)
{
	m_fProgram = FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// L5K Tag Entry
//

CEIPL5KEntry::CEIPL5KEntry(void)
{
	m_fStruct  = FALSE;

	m_fProgram = FALSE;

	m_fAlias   = FALSE;

	m_Scope	= "";

	m_Type	= "";

	m_Name	= "";

	m_Alias = "";

	m_pMembers = NULL;

	m_uType = NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CEIP2aMasterDriverOptions, CUIItem);

// Constructor

CEIP2aMasterDriverOptions::CEIP2aMasterDriverOptions(void)
{
	}

// UI Managament

void CEIP2aMasterDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support	     

BOOL CEIP2aMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CEIP2aMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEIP2aMasterDeviceOptions, CUIItem);

// Constructor

CEIP2aMasterDeviceOptions::CEIP2aMasterDeviceOptions(void)
{
	m_NamedAlloc	= 0;

	m_TableAlloc	= 1;

	m_Limit		= 0;

	m_IPAddr	= DWORD(MAKELONG(MAKEWORD(51, 15), MAKEWORD(168, 192)));

	m_Port		= 44818;

	m_fL5KInit	= FALSE;

	m_pL5KTags	= New CEIPL5KList;

	m_LoadFile	= 1;

	m_Routing	= 1;
	}

// Destructor

CEIP2aMasterDeviceOptions::~CEIP2aMasterDeviceOptions(void)
{
	Kill();

	KillL5KTags();
	}

// Attributes

BOOL CEIP2aMasterDeviceOptions::IsListFull(void) const
{
	return m_Tags.GetCount() == MAX_SLOTS;
	}

// Tag Lookup

CEIPTagInfo * CEIP2aMasterDeviceOptions::FindTag(CString Name) const
{
	INDEX i = m_Names.FindName(Name);

	if( !m_Names.Failed(i) ) {

		INDEX t = m_Names.GetData(i);

		return m_Tags[t];
		}

	return NULL;
	}

CEIPTagInfo * CEIP2aMasterDeviceOptions::FindTag(UINT Slot) const
{
	INDEX i = 0;

	if( Slot < addrNamed ) {

		i = m_TableSlots.FindName(Slot);
		}
	else
		i = m_NamedSlots.FindName(Slot);

	if( !m_Names.Failed(i) ) {

		INDEX t = m_Names.GetData(i);

		return m_Tags[t];
		}

	return NULL;
	}

BOOL  CEIP2aMasterDeviceOptions::TagExists(CString Name)
{
	UINT k = Name.FindRev('.');
	UINT p = Name.FindRev('[');

	CLongArray List;

	// Check if the last element is an array
	if( p < NOTHING && k == NOTHING || k < NOTHING && p > k ) {

		Name = Name.Left(p);
		}

	if( FindTag(Name) ) {

		return TRUE;
		}

	return FALSE;
	}

// Tag Addition

CEIPTagInfo * CEIP2aMasterDeviceOptions::AddTag(CString Name, UINT Type, BOOL fNamed)
{
	INDEX i = m_Names.FindName(Name);

	if( m_Names.Failed(i) ) {

		CEIPTagInfo *pTag = New CEIPTagInfo;

		pTag->m_Name = Name;

		i = m_Tags.Append(pTag);

		UINT uSlot = NOTHING;

		if( fNamed ) {

			if( (uSlot = FindNextSlot(m_NamedSlots, addrNamed, MAX_SLOTS)) < NOTHING ) {
			
				pTag->m_Slot = uSlot;

				m_NamedSlots.Insert(pTag->m_Slot, i);
				}
			else
				return NULL;
			}

		else {
			if( (uSlot = FindNextSlot(m_TableSlots, 0, addrNamed)) < NOTHING ) {

				pTag->m_Slot = uSlot;

				m_TableSlots.Insert(pTag->m_Slot, i);
				}
			else
				return NULL;
			}

		pTag->m_Type = Type;

		m_Names.Insert(pTag->m_Name, i);

		MakeMax(m_Limit, pTag->m_Slot + 1);

		GetDatabase()->SetDirty();

		return pTag;
		}

	return NULL;
	}

BOOL CEIP2aMasterDeviceOptions::AddTagToDevice(CError &Error, CString Text, UINT uType)
{
	if( Text.IsEmpty() ) {

		return FALSE;
		}

	CString BaseName = Text;

	UINT uFind = 0;

	if( (uFind = Text.FindRev(L'.')) < NOTHING) {

		BaseName = Text.Mid(uFind + 1);
		}

	CEIPTagInfo * pTag = NULL;

	CAddress Addr;

	memset(&Addr, 0, sizeof(Addr));

	if( BaseName.FindRev('[') < NOTHING ) {

		CString Name(Text);

		UINT k = Name.FindRev('.');
		UINT p = Name.FindRev('[');

		// Check if the last element is an array
		if( p < NOTHING && k == NOTHING || k < NOTHING && p > k ) {

			Name = Text.Left(p);
			}

		if( (pTag = AddTag(Name, uType, FALSE)) ) {

			Addr.a.m_Table  = pTag->m_Slot;
			Addr.a.m_Extra  = 0;
			Addr.a.m_Offset = 0;
			Addr.a.m_Type   = pTag->m_Type;

			pTag->m_Addr = Addr;

			ParseDims(pTag->m_Dims, BaseName);

			return TRUE;
			}
		else {
			Error.Set(L"Maximum number of table tags reached for this device.");

			return FALSE;
			}
		}
	else {
		if( (pTag = AddTag(Text, uType, TRUE)) ) {

			Addr.a.m_Table  = addrNamed;
			Addr.a.m_Extra  = 0;
			Addr.a.m_Offset = pTag->m_Slot;
			Addr.a.m_Type   = pTag->m_Type;

			pTag->m_Addr = Addr;

			return TRUE;
			}
		else {
			Error.Set(L"Maximum number of named tags reached for this device.");

			return FALSE;
			}
		}

	return FALSE;
	}

// Tag Deletion

BOOL CEIP2aMasterDeviceOptions::DeleteTag(CString Name)
{
	CEIPTagInfo *pTag;

	INDEX i = m_Names.FindName(Name);

	if( (pTag = FindTag(Name)) ) {

		INDEX t = m_Names.GetData(i);

		m_Tags.Remove(t);

		m_Names.Remove(pTag->m_Name);

		if( pTag->m_Slot < addrNamed ) {

			m_TableSlots.Remove(pTag->m_Slot);
			}
		else
			m_NamedSlots.Remove(pTag->m_Slot);

		delete pTag;

		CSystemItem *pItem = GetDatabase()->GetSystemItem();

		pItem->Rebuild(1);

		return TRUE;
		}

	return FALSE;
	}


void CEIP2aMasterDeviceOptions::ClearTags(void)
{	
	for( INDEX n = m_Tags.GetHead(); !m_Tags.Failed(n); m_Tags.GetNext(n) ) {

		CEIPTagInfo *pTag = m_Tags[n];

		delete pTag;
		}

	m_Names.Empty();

	m_NamedSlots.Empty();

	m_TableSlots.Empty();

	m_Tags.Empty();

	// We've cleared out all the tags, so we need to recompile
	// the database.
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	pSystem->Rebuild(1);
	}

BOOL CEIP2aMasterDeviceOptions::IsAtomic(CString const& Type)
{
	return GetAtomicType(Type) < NOTHING;
	}

UINT CEIP2aMasterDeviceOptions::GetAtomicType(CString const &Type)
{
	CString Text;

	UINT uFind = Type.Find('[');

	if( uFind < NOTHING ) {

		Text = Type.Left(uFind);
		}
	else {
		Text = Type;
		}

	if( Text == "BIT" )
		return addrBitAsBit;

	else if( Text == "BOOL" )
		return addrBitAsBit;

	else if( Text == "SINT" )
		return addrByteAsByte;

	else if( Text == "INT" )
		return addrWordAsWord;

	else if( Text == "DINT" )
		return addrLongAsLong;

	else if( Text == "REAL" )
		return addrRealAsReal;

	return NOTHING;
	}

BOOL CEIP2aMasterDeviceOptions::LoadFromFile(CFilename Filename)
{
	if( m_fL5KInit ) {

		KillL5KTags();

		m_fL5KInit = FALSE;

		m_pL5KTags = New CEIPL5KList;
		}

	CTextStreamMemory Stream;

	Stream.SetPrev();

	if( Stream.LoadFromFile(Filename) ) {

		CABL5kFile File;

		if( File.OpenLoad(Stream) ) {

			CABL5kController const &Logix = File.m_Logix;

			m_Controller = Logix;

			BuildL5KTags(m_pL5KTags, m_Controller.m_Tags, "");

			for( UINT u = 0; u < m_Controller.m_Programs.GetCount(); u++ ) {

				CEIPL5KList *pProgram = New CEIPL5KList;

				pProgram->m_Name = m_Controller.m_Programs[u].m_Name;

				pProgram->m_fProgram = TRUE;

				m_pPrograms.Append(pProgram);

				BuildL5KTags(pProgram, m_Controller.m_Programs[u].m_List, "");
				}

			m_fL5KInit = TRUE;

			Stream.Close();

			afxThread->SetStatusText(L"");

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEIP2aMasterDeviceOptions::SetSavedFilename(CString const &Name)
{
	m_Path = Name;

	return TRUE;
	}

CFilename CEIP2aMasterDeviceOptions::GetSavedFilename(void)
{
	return m_Path;
	}

// Persistance

void CEIP2aMasterDeviceOptions::Init(void)
{
	CUIItem::Init();
	}

void CEIP2aMasterDeviceOptions::PostLoad(void)
{
	MakeNameIndex();

	MakeSlotIndex();

	CUIItem::PostLoad();
	}

void CEIP2aMasterDeviceOptions::Load(CTreeFile &Tree)
{	
	CEIPTagInfo *pTag = NULL;

	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString const &Name = Tree.GetName();

		if( Name.StartsWith(L"Tag") ) {

			if( Name == L"TagName" ) {

				pTag = New CEIPTagInfo;

				pTag->m_Name = Tree.GetValueAsString();

				pTag->m_Slot = NOTHING;

				pTag->m_Type = addrReserved;

				pTag->m_Addr.m_Ref = 0;

				m_Tags.Append(pTag);
				}
			else {
				if( pTag ) {

					if( Name == L"TagSlot" ) {

						pTag->m_Slot = Tree.GetValueAsInteger();

						continue;
						}

					if( Name == L"TagType" ) {

						pTag->m_Type = Tree.GetValueAsInteger();

						continue;
						}

					if( Name == L"TagAddr" ) {

						pTag->m_Addr.m_Ref = Tree.GetValueAsInteger();

						continue;
						}

					if( Name == L"TagDimX" ) {

						pTag->m_Dims.Append(Tree.GetValueAsInteger());

						continue;
						}

					if( Name == L"TagDimY" ) {

						pTag->m_Dims.Append(Tree.GetValueAsInteger());

						continue;
						}

					if( Name == L"TagDimZ" ) {

						pTag->m_Dims.Append(Tree.GetValueAsInteger());

						continue;
						}
					}
				}
			}
		else {
			CMetaData const *pMeta = m_pList->FindData(Name);

			if( pMeta ) {

				LoadProp(Tree, pMeta);

				continue;
				}
			}
		}

	if( m_LoadFile ) {

		CFilename Filename(m_Path);

		LoadFromFile(Filename);
		}

	RegisterHandle();
	}

void CEIP2aMasterDeviceOptions::Save(CTreeFile &Tree)
{
	for( INDEX n = m_Tags.GetHead(); !m_Tags.Failed(n); m_Tags.GetNext(n) ) {

		CEIPTagInfo *pTag = m_Tags[n];

		Tree.PutValue(L"TagName", pTag->m_Name);

		Tree.PutValue(L"TagSlot", pTag->m_Slot);

		Tree.PutValue(L"TagType", pTag->m_Type);

		Tree.PutValue(L"TagAddr", pTag->m_Addr.m_Ref);

		switch( pTag->m_Dims.GetCount() ) {

			case 1:
				Tree.PutValue(L"TagDimX", pTag->m_Dims[0]);
				break;
			case 2:
				Tree.PutValue(L"TagDimX", pTag->m_Dims[0]);
				Tree.PutValue(L"TagDimY", pTag->m_Dims[1]);
				break;
			case 3:
				Tree.PutValue(L"TagDimX", pTag->m_Dims[0]);
				Tree.PutValue(L"TagDimY", pTag->m_Dims[1]);
				Tree.PutValue(L"TagDimZ", pTag->m_Dims[2]);
				break;
			}
		}

	CUIItem::Save(Tree);
	}

void CEIP2aMasterDeviceOptions::Kill(void)
{
	for( INDEX n = m_Tags.GetHead(); !m_Tags.Failed(n); m_Tags.GetNext(n) ) {

		CEIPTagInfo *pTag = m_Tags[n];

		delete pTag;
		}

	m_Tags.Empty();
	
	m_Names.Empty();

	m_NamedSlots.Empty();

	m_TableSlots.Empty();

	CUIItem::Kill();
	}

// UI Managament

void CEIP2aMasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "Push" ) {

		CEIP2aMasterDriver Driver;

		CEIP2aMasterDialog Dlg(Driver, this);

		Dlg.Execute();

		return;
		}

	if( Tag.IsEmpty() || Tag == "Routing" ) {

		pHost->EnableUI("Route", m_Routing ? TRUE : FALSE);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support	     

BOOL CEIP2aMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(UINT(m_IPAddr));

	Init.AddWord(WORD(m_Port));

	Init.AddText(m_Routing ? m_Route : L"NULL");

	Init.AddWord(WORD(m_Limit));

	// NOTE -- This builds a list of strings in slot order, adding empty
	// strings if a given slot isn't used for some reason. The first version
	// of the driver doesn't leave gaps in the slot allocation, but once
	// we start to allow the deletion of tags, we shall need to handle this.

	UINT Slot = 0;

	for( INDEX n = m_TableSlots.GetHead(); !m_TableSlots.Failed(n); m_TableSlots.GetNext(n) ) {

		INDEX i = m_TableSlots[n];

		MakeTagInit(Init, i, Slot);

		Slot++;
		}

	for( INDEX n = m_NamedSlots.GetHead(); !m_NamedSlots.Failed(n); m_NamedSlots.GetNext(n) ) {

		INDEX i = m_NamedSlots[n];

		MakeTagInit(Init, i, Slot);

		Slot++;
		}

	while( Slot < m_Limit ) {

		Init.AddByte(0);

		Slot++;
		}

	return TRUE;
	}

void CEIP2aMasterDeviceOptions::MakeTagInit(CInitData &Init, INDEX i, UINT &Slot)
{
	CEIPTagInfo *pTag = m_Tags [i];

	while( Slot < pTag->m_Slot ) {

		Init.AddByte(0);

		Slot++;
		}

	UINT  s = pTag->m_Name.GetLength();

	PCTXT p = pTag->m_Name;

	Init.AddByte(BYTE(s));

	for( UINT n = 0; n < s; n++ ) {

		Init.AddByte(BYTE(p[n]));
		}

	// Add the dimensions
	UINT uDims = pTag->m_Dims.GetCount();

	Init.AddByte(BYTE(uDims));

	if( uDims > 0 ) {
			
		switch( uDims ) {

			case 1:
				Init.AddWord(WORD(pTag->m_Dims.GetAt(0)));

				break;
			case 2:
				Init.AddWord(WORD(pTag->m_Dims.GetAt(0)));
				Init.AddWord(WORD(pTag->m_Dims.GetAt(1)));

				break;
			case 3:
				Init.AddWord(WORD(pTag->m_Dims.GetAt(0)));
				Init.AddWord(WORD(pTag->m_Dims.GetAt(1)));
				Init.AddWord(WORD(pTag->m_Dims.GetAt(2)));

				break;
			}
		}
	}

// Meta Data Creation

void CEIP2aMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IPAddr);
	Meta_AddInteger(Port);
	Meta_AddInteger(Push);
	Meta_AddInteger(Routing);
	Meta_AddString(Route);
	Meta_AddInteger(LoadFile);
	Meta_AddString(Path);
	}

// Implementation

void CEIP2aMasterDeviceOptions::MakeNameIndex(void)
{
	for( INDEX n = m_Tags.GetHead(); !m_Tags.Failed(n); m_Tags.GetNext(n) ) {

		CEIPTagInfo *pTag = m_Tags[n];

		m_Names.Insert(pTag->m_Name, n);
		}
	}

void CEIP2aMasterDeviceOptions::MakeSlotIndex(void)
{
	for( INDEX n = m_Tags.GetHead(); !m_Tags.Failed(n); m_Tags.GetNext(n) ) {

		CEIPTagInfo *pTag = m_Tags[n];

		if( pTag->m_Slot < NOTHING ) {

			if( pTag->m_Slot < addrNamed ) {

				m_TableSlots.Insert(pTag->m_Slot, n);
				}
			else
				m_NamedSlots.Insert(pTag->m_Slot, n);

			MakeMax(m_Limit, pTag->m_Slot + 1);
			}
		}
	}

BOOL CEIP2aMasterDeviceOptions::FindLowestSlot(CSlotIndex const &Slots, UINT uStart, UINT uMax)
{
	//if( !IsListFull() ) {

	//	// This implementation isn't very memory efficient,
	//	// but it should work.

	//	UINT uList = 65636;

	//	bool * pUsed = New bool[ uList ];

	//	memset(pUsed, 0, uList);

	//	for( INDEX n = m_Slots.GetHead(); !m_Slots.Failed(n); m_Slots.GetNext(n) ) {

	//		UINT s = m_Slots.GetName(n);
	//		
	//		pUsed[s] = true;
	//		}

	//	UINT u;

	//	for( u = 0; u < addrNamed; u++ ) {

	//		if( !pUsed[u] ) {

	//			m_TableAlloc = u;

	//			break;
	//			}
	//		}

	//	for( u = addrNamed; u < uList; u++ ) {

	//		if( !pUsed[u] ) {

	//			m_NamedAlloc = u;

	//			break;
	//			}
	//		}

	//	delete [] pUsed;

	//	return TRUE;
	//	}

	return FALSE;
	}

UINT CEIP2aMasterDeviceOptions::FindNextSlot(CSlotIndex const &Slots, UINT uStart, UINT uMax)
{
	UINT uNext = uStart;

	if( Slots.GetCount() < uMax ) {

		for(;;) {

			uNext = (uNext + 1) % uMax;

			if( Slots.Failed(Slots.FindName(uNext)) ) {

				return uNext;
				}
			}
		}

	return NOTHING;
	}

BOOL CEIP2aMasterDeviceOptions::DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {

			m_ListIndex = m_Tags.GetHead();
			}
		else {
			m_Tags.GetNext(m_ListIndex);
			}

		if( !m_Names.Failed(m_ListIndex) ) {

			CEIPTagInfo *pTag = m_Tags[m_ListIndex];

			BOOL fPart = pTag->m_Addr.a.m_Table < addrNamed;

			Data.m_Name		= pTag->m_Name;
			Data.m_fPart		= fPart;
			Data.m_Addr		= pTag->m_Addr;

			return TRUE;
			}
		}

	return FALSE;
	}

// L5K Support

void CEIP2aMasterDeviceOptions::BuildL5KTags(CEIPL5KList *pList, CArray<CABL5kDesc> const &Tags, CString Base)
{
	CArray<CABL5kDesc> AliasTags;

	for( UINT n = 0; n < Tags.GetCount(); n++ ) {

		CABL5kDesc Tag = Tags.GetAt(n);

		if( !Tag.IsAlias() ) {

			CEIPL5KEntry *pEntry = New CEIPL5KEntry;

			pEntry->m_Type = Tag.m_Type;

			if( pList->m_fProgram ) {

				pEntry->m_Scope = pList->m_Name;

				pEntry->m_fProgram = TRUE;
				}

			pEntry->m_Name = Base + Tag.m_Name;

			UINT p = 0;

			if( !IsAtomic(pEntry->m_Type) ) {

				if( (p = Base.Find(L'.')) < NOTHING ) {

					if( (p = Tag.m_Name.Find(L'[')) < NOTHING ) {

						// Remove array subscripts from the tag name
						pEntry->m_Name = Base + Tag.m_Name.Left(p);

						// Add the subscript to the type instead
						pEntry->m_Type += Tag.m_Name.Mid(p);
						}
					}
			
				// Resolve the members of the structured type.
				BuildStructTypes(pEntry, pList, Base);
				}

			else {
				if( (p = pEntry->m_Type.FindRev('[')) < NOTHING && IsAtomic(pEntry->m_Type) ) {

					pEntry->m_Name += pEntry->m_Type.Mid(p);
					}

				pEntry->m_uType = GetAtomicType(pEntry->m_Type);

				pList->AppendItem(pEntry);
				}
			}
		else {
			AliasTags.Append(Tag);
			}
		}

	for( UINT n = 0; n < AliasTags.GetCount(); n++ ) {

		CABL5kDesc Tag = AliasTags.GetAt(n);

		if( Tag.IsAlias() ) {

			CEIPL5KEntry *pEntry = New CEIPL5KEntry;

			if( pList->m_fProgram ) {

				pEntry->m_Scope = pList->m_Name;

				pEntry->m_fProgram = TRUE;
				}

			pEntry->m_Name = Tag.m_Name;

			pEntry->m_Type = Tag.m_Type;

			pEntry->m_fAlias = TRUE;

			pEntry->m_Alias = Tag.m_Type;

			// Resolve the alias
			FindAliasType(pEntry);

			if( !IsAtomic(pEntry->m_Type) ) {

				BuildStructTypes(pEntry, pList, Base);
				}
			else {
				pList->AppendItem(pEntry);
				}
			}
		}
	}

void CEIP2aMasterDeviceOptions::KillL5KTags(void)
{
	// Delete all of the program scoped tags
	for( UINT u = 0; u < m_pPrograms.GetCount(); u++ ) {

		CEIPL5KList *pList = m_pPrograms.GetAt(u);

		DoKillL5KTags(pList);
		}

	m_pPrograms.Empty();

	// Delete the controller scoped tags
	DoKillL5KTags(m_pL5KTags);
	}

void CEIP2aMasterDeviceOptions::DoKillL5KTags(CEIPL5KList *pList)
{
	// Walk the tag list, deleting any nested structured types
	// as we go.
	for( UINT u = 0; u < pList->GetItemCount(); u++ ) {

		CEIPL5KEntry *pTag = (CEIPL5KEntry *) pList->GetItem(u);

		if( !IsAtomic(pTag->m_Type) ) {

			DoKillL5KTags(pTag->m_pMembers);
			}
		}

	pList->Kill();

	delete pList;
	}

BOOL CEIP2aMasterDeviceOptions::ParseDims(CLongArray &Dims, CString const &Type)
{
	UINT k = Type.FindRev('.');

	UINT p = Type.FindRev('[');

	if( p < NOTHING || k < NOTHING && p > k ) {

		CStringArray DimArray;

		CString Text = Type.Mid(p);

		Text.Remove('[');

		Text.Remove(']');

		Text.Tokenize(DimArray, L",");

		for( UINT n = 0; n < DimArray.GetCount(); n++ ) {

			UINT uDim = wcstoul(DimArray[n], NULL, 10);

			Dims.Append(uDim);
			}

		return TRUE;
		}

	return FALSE;
	}

void CEIP2aMasterDeviceOptions::ExpandDims(CString &Text, CLongArray const &Dims, UINT uIndex)
{
	UINT uCount = Dims.GetCount();

	if( uCount ) {

		Text += L"[";

		switch( uCount - 1 ) {

			case 0:
				Text += CPrintf(L"%d", uIndex % Dims[0]);
				break;

			case 1:
				Text += CPrintf(L"%d", uIndex / Dims[1]);

				Text += L",";

				Text += CPrintf(L"%d", uIndex % Dims[1]);

				break;

			case 2:
				Text += CPrintf(L"%d", uIndex / (Dims[1] * Dims[2]));

				uIndex = uIndex % (Dims[1] * Dims[2]);

				Text += L",";

				Text += CPrintf(L"%d", uIndex / Dims[2]);

				Text += L",";

				Text += CPrintf(L"%d", uIndex % Dims[2]);

				break;
			}

		Text += L"]";
		}
	}

void CEIP2aMasterDeviceOptions::ExpandDimsText(CString &Text, CLongArray const &Dims)
{
	switch( Dims.GetCount() ) {

		case 1:
			Text += CPrintf("[%d]", Dims[0]);
			break;
		case 2:
			Text += CPrintf("[%d,%d]", Dims[0], Dims[1]);
			break;
		case 3:
			Text += CPrintf("[%d,%d,%d]", Dims[0], Dims[1], Dims[2]);
			break;

		}
	}

void CEIP2aMasterDeviceOptions::ExpandDimsIndex(UINT uIndex, CLongArray const &InDims, CLongArray &OutDims)
{
	UINT uCount = InDims.GetCount();

	UINT x, y, z;
	
	x = y = z = NOTHING;

	if( uCount ) {

		switch( uCount - 1 ) {

			case 0:
				x = uIndex % InDims[0];
				break;

			case 1:
				x = uIndex / InDims[1];

				y = uIndex % InDims[1];

				break;

			case 2:
				x = uIndex / (InDims[1] * InDims[2]);

				uIndex = uIndex % (InDims[1] * InDims[2]);

				y = uIndex / InDims[2];

				z = uIndex % InDims[2];

				break;
			}
		}

	if( x < NOTHING ) OutDims.Append(x);
	if( y < NOTHING ) OutDims.Append(y);
	if( z < NOTHING ) OutDims.Append(z);
	}

void CEIP2aMasterDeviceOptions::ExpandStructDims(CArray<CABL5kDesc> &ArrMembers, CLongArray const &Dims, CEIPL5KEntry *pEntry, CString const &BaseType)
{
	// Expand all of the permutations for a multidimensional array
	// of structured types.

	CString BaseName = pEntry->m_Name;

	CString Name;

	UINT uCount = Dims.GetCount();

	if( uCount == 1 ) {

		for( UINT i = 0; i < Dims[0]; i++ ) {

			Name = CPrintf("%s[%d]", pEntry->m_Name, i);

			CABL5kDesc Desc(Name, BaseType);

			ArrMembers.Append(Desc);
			}
		}

	else if( uCount == 2 ) {

		for( UINT i = 0; i < Dims[0]; i++ ) {

			for( UINT j = 0; j < Dims[1]; j++ ) {

				Name = CPrintf("%s[%d,%d]", pEntry->m_Name, i, j);

				CABL5kDesc Desc(Name, BaseType);

				ArrMembers.Append(Desc);
				}
			}
		}

	else if( uCount == 3 ) {

		for( UINT i = 0; i < Dims[0]; i++ ) {

			for( UINT j = 0; j < Dims[1]; j++ ) {

				for( UINT k = 0; k < Dims[2]; k++ ) {

					Name = CPrintf("%s[%d,%d,%d]", pEntry->m_Name, i, j, k);

					CABL5kDesc Desc(Name, BaseType);

					ArrMembers.Append(Desc);
					}
				}
			}
		}
	}

void CEIP2aMasterDeviceOptions::FindAliasType(CEIPL5KEntry *pEntry)
{
	CArray<CABL5kDesc> Tags = m_Controller.m_Tags;
	
	CString Text = pEntry->m_Type;

	UINT p = pEntry->m_Type.FindRev(L'[');

	UINT uDot = pEntry->m_Type.FindRev(L'.');

	// We start at -1 so we can search the controller scoped tags first.
	for( INT n = -1; n < (INT) m_Controller.m_Programs.GetCount(); n++ ) {

		UINT uCount = Tags.GetCount();

		// Ignore array subscripts
		if( (uDot == NOTHING && p < NOTHING) || (p < NOTHING && p > uDot) ) {

			Text = pEntry->m_Type.Left(p);
			}

		for( UINT u = 0; u < uCount; u++ ) {

			CABL5kDesc Tag = Tags.GetAt(u);

			if( Text == Tag.m_Name ) {

				if( Tag.IsAlias() ) {

					pEntry->m_Type = Tag.m_Type;
				
					FindAliasType(pEntry);

					return;
					}

				CString Type(Tag.m_Type);

				if( (p = Tag.m_Type.Find('[')) < NOTHING ) {
			
					Type = Tag.m_Type.Left(p);
					}

				pEntry->m_Type = Type;

				pEntry->m_uType = GetAtomicType(Type);

				return;
				}
			}

		Tags = m_Controller.m_Programs[n + 1].m_List;
		}

	if( uDot < NOTHING ) {

		if( FindStructAlias(pEntry, m_pL5KTags, 1) ) {

			return;
			}

		for( UINT n = 0; n < m_pPrograms.GetCount(); n++ ) {

			if( FindStructAlias(pEntry, m_pPrograms.GetAt(n), 1) ) {

				return;
				}
			}
		}

	// We couldn't resolve the alias for some reason,
	// so we'll just call it a DINT.
	pEntry->m_Type = L"DINT";

	pEntry->m_uType = addrLongAsLong;
	}

BOOL CEIP2aMasterDeviceOptions::FindStructAlias(CEIPL5KEntry *pEntry, CEIPL5KList *pList, UINT uDepth)
{
	CString Type = pEntry->m_Type;

	UINT uDot = Type.FindRev(L'.');

	CString Atom = Type.Mid(uDot + 1);

	BOOL fBitAlias = TRUE;

	for( UINT k = 0; k < Atom.GetLength(); k++ ) {

		if( !isdigit(Atom[k]) ) {

			fBitAlias = FALSE;

			break;
			}
		}

	if( fBitAlias ) {

		pEntry->m_Type = "BIT";

		pEntry->m_uType = addrBitAsBit;

		return TRUE;
		}

	for( UINT u = 0; u < pList->GetItemCount(); u++ ) {

		CEIPL5KEntry *pTag = (CEIPL5KEntry *) pList->GetItem(u);

		UINT uPos = 0;

		for( UINT uSearch = 0; uSearch < uDepth; uSearch++  ) {

			uPos = Type.Find(L'.', uPos + 1);
			}

		CString Start = Type.Left(uPos);
		
		if( pTag->m_fStruct ) {

			BOOL fStructArray = FALSE;

			CString Stripped;

			UINT uBracket = Start.Find(L'[');

			if( uBracket < NOTHING ) {

				Stripped = Start.Left(uBracket);
				}

			if( Start == pTag->m_Name || Stripped == pTag->m_Name ) {

				for( UINT n = 0; n < pTag->m_pMembers->GetItemCount(); n++ ) {

					CEIPL5KEntry *pMember = (CEIPL5KEntry *) pTag->m_pMembers->GetItem(n);

					UINT uDot2 = pMember->m_Name.FindRev(L'.');

					CString MemberAtom = pMember->m_Name.Mid(uDot2 + 1);

					UINT uMemBrack = MemberAtom.FindRev(L'[');

					if( uMemBrack < NOTHING ) {

						if( uDepth == 1 ) {

							fStructArray = TRUE;
							}

						MemberAtom = MemberAtom.Left(uMemBrack);

						UINT uAtomBrack = Atom.FindRev(L'[');
							
						if( uAtomBrack < NOTHING ) {

							Atom = Atom.Left(uAtomBrack);
							}
						}

					if( Atom == MemberAtom ) {

						UINT uTypeBrack = pMember->m_Type.FindRev(L'[');

						pEntry->m_Type = uTypeBrack < NOTHING ? pMember->m_Type.Left(uTypeBrack) : pMember->m_Type;

						pEntry->m_uType = pMember->m_uType;

						return TRUE;
						}
					}

				return FindStructAlias(pEntry, pTag->m_pMembers, fStructArray ? uDepth : ++uDepth);
				}
			}
		}

	return FALSE;
	}

void CEIP2aMasterDeviceOptions::BuildStructTypes(CEIPL5KEntry *pEntry, CEIPL5KList *pList, CString Base)
{
	pEntry->m_fStruct = TRUE;

	pEntry->m_pMembers = New CEIPL5KList;

	pEntry->m_pMembers->m_fProgram = pList->m_fProgram;

	pEntry->m_pMembers->m_Name = pList->m_Name;

	UINT p = pEntry->m_Type.Find(L'[');

	CLongArray Dims;

	ParseDims(Dims, pEntry->m_Type);

	UINT uCount = Dims.GetCount();

	// Ignore array brackets for type lookup
	CString BaseType = p < NOTHING ? pEntry->m_Type.Left(p) : pEntry->m_Type;

	// Search user-defined types
	for( UINT u = 0; u < m_Controller.m_UDTs.GetCount(); u++ ) {

		if( BaseType == m_Controller.m_UDTs[u].m_Name ) {

			if( uCount ) {

				CArray<CABL5kDesc> ArrMembers;

				ExpandStructDims(ArrMembers, Dims, pEntry, BaseType);

				pList->AppendItem(pEntry);

				Base = L"";

				BuildL5KTags(pEntry->m_pMembers, ArrMembers, Base);

				return;
				}

			else {
				Base = CPrintf("%s.", pEntry->m_Name);

				pList->AppendItem(pEntry);

				BuildL5KTags(pEntry->m_pMembers, m_Controller.m_UDTs[u].m_List, Base);

				return;
				}
			}
		}

	// Search pre-defined types
	for( UINT n = 0; n < CABL5kTypes::GetPDTCount(); n++ ) {

		CABL5kTypes::CStructEntry PDT = CABL5kTypes::m_PredefinedList[n];

		if( BaseType == PDT.m_pName ) {

			CArray<CABL5kDesc> MemberTags;

			for( UINT k = 0; PDT.m_Elements[k].m_pName; k++ ) {
						
				if( wstrcmp(PDT.m_Elements[k].m_pName, END_MARKER) ) {

					CABL5kDesc Member(PDT.m_Elements[k].m_pName, PDT.m_Elements[k].m_pType);

					MemberTags.Append(Member);
					}
				}

			if( uCount ) {

				CArray<CABL5kDesc> ArrMembers;

				ExpandStructDims(ArrMembers, Dims, pEntry, BaseType);

				pList->AppendItem(pEntry);

				Base = L"";

				BuildL5KTags(pEntry->m_pMembers, ArrMembers, Base);

				return;
				}
			else {
				Base.Printf("%s.", pEntry->m_Name);

				pList->AppendItem(pEntry);

				BuildL5KTags(pEntry->m_pMembers, MemberTags, Base);

				return;
				}
			}
		}

	// Search add-on instructions
	for( UINT t = 0; t < m_Controller.m_AOIs.GetCount(); t++ ) {

		if( BaseType == m_Controller.m_AOIs[t].m_Name ) {

			if( uCount ) {

				CArray<CABL5kDesc> ArrMembers;

				ExpandStructDims(ArrMembers, Dims, pEntry, BaseType);

				pList->AppendItem(pEntry);

				Base = L"";

				BuildL5KTags(pEntry->m_pMembers, ArrMembers, Base);

				return;
				}
			else {
				Base.Printf("%s.", pEntry->m_Name);

				pList->AppendItem(pEntry);

				BuildL5KTags(pEntry->m_pMembers, m_Controller.m_AOIs[t].m_List, Base);

				return;
				}
			}
		}
	}

// End of File
