
#include "intern.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Data

extern IStratonRetain * g_pRetain;

// Code

clink T5_BOOL retainCanLoad(T5_DWORD dwCrc, T5_PTR pArgs)
{
	return g_pRetain->CanLoad(dwCrc);
	}

clink T5_RET retainLoad(T5PTR_DBRETAIN pRetain, T5_PTR pArgs)
{
	if( g_pRetain->Load(pRetain) ) {
		
		return T5RET_OK;
		}

	return T5RET_ERROR;
	}

clink T5_BOOL retainCanStore(T5PTR_DBRETAIN pRetain, T5_PTR pArgs)
{
	if( g_pRetain->CanSave(pRetain) ) {
		
		return T5RET_OK;
		}

	return T5RET_ERROR;
	}

clink void retainShutDown(T5PTR_DBRETAIN pRetain, T5_PTR pArgs)
{
	}

clink void retainCycleExchange (T5PTR_DBRETAIN pRetain)
{
	g_pRetain->Save(pRetain);
	}

// End of File
