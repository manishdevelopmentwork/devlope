#ifndef	INCLUDE_ML1SERV_HPP
	
#define	INCLUDE_ML1SERV_HPP

#include "ml1base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Server Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CML1Server : public CML1Base
{
	public:
		CML1Server(void);

		// Destructor
		~CML1Server(void);

		// Access
		BOOL HandleFrame(PBYTE pBuff);

	protected:

		// Implementation
		BOOL	  HandleAuthentication(PBYTE pBuff);
		BOOL	  HandleRequest(CML1Dev * pDev, PBYTE pBuff);
		BOOL	  HandleResponse(CML1Dev * pDev, PBYTE pBuff);
		PVOID	  DoListen(BYTE bId, DWORD dwUnit);
		BOOL	  SendBlockRequest(CML1Dev * pDev, CML1Blk * pBlock, BYTE bOp, UINT uOffset = 0);
		BOOL	  DoBlockRead(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset);
		BOOL	  DoBlockWrite(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset, UINT& uCount);
		BOOL	  IsRequest(CML1Dev * pDev, PBYTE pBuff);
		CML1Blk * FindBlock(CML1Dev * pDev, UINT uSpace, UINT uOffset, UINT uFile = 0, PBYTE pBuff = NULL);
		CML1Blk * ExtendRemoteRequest(CML1Dev * pDev, CML1Blk * pBlock, CAddress &Address, UINT &uIndex);
		BOOL	  CheckRemoteRequest(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset, UINT uCount);
		BOOL	  TestBlock(CML1Blk * pBlock, UINT uSpace, UINT uOffset, UINT uFile = 0);
		WORD      GetTrans(PBYTE pBuff);
		DWORD	  GetUnitID(PBYTE pBuff, BOOL fAuth = FALSE);
		BYTE	  GetID(PBYTE pBuff, BOOL fAuth = FALSE);
		BYTE	  GetFC(PBYTE pBuff, BOOL fAuth = FALSE);
		UINT	  GetOffset(PBYTE pBuff);
		UINT	  GetFileNumber(PBYTE pBuff);
		UINT	  GetFileRecord(PBYTE pBuff);
		UINT	  GetFileBytes(PBYTE pBuff);
		UINT	  GetCount(PBYTE pBuff, BOOL fFile = FALSE);
		UINT	  GetBytes(PBYTE pBuff);
		PBYTE	  GetData(PBYTE pBuff);
		WORD	  GetKey(PBYTE pBuff);
		BYTE	  GetMacHead(PBYTE pBuff);
		DWORD	  GetAuthority(PBYTE pBuff);
		BOOL	  GetCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  GetWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  GetFile (CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  SetSpec (CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  SetCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  SetWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  SetFile (CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  CopyCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  CopyWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  CopyFile (CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  MarkSpec (CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  MarkCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  MarkWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  MarkFile (CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff);
		BOOL	  TakeException(CML1Dev * pDev, BYTE bCode, PBYTE pBuff);
		BOOL	  HandleLoopBack(CML1Dev * pDev, PBYTE pBuff);

		// Read Handlers
		BOOL DoWordRead(CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoLongRead(CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoBitRead (CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoFileRead(CML1Dev * pDev, AREF Addr, UINT uCount);

		// Write Handlers
		BOOL DoWordWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoLongWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoBitWrite (CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoFileWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);

		// Authentication Support
		BOOL AckAuthentication(PBYTE pBuff);
		BOOL RspAuthentication(PBYTE pBuff);
		BOOL MarkPshAuthentication(PBYTE pBuff);
		BOOL MarkReqAuthentication(PBYTE pBuff);
		BOOL SetAuthentication(CML1Dev * pDev, PBYTE pBuff);
		BOOL IsAuthentication(CML1Dev * pDev, PBYTE pBuff);
		BOOL IsAuthenticationReq(PBYTE pBuff);
		BOOL IsMacValid(CML1Dev * pDev, PBYTE pBuff);
		BOOL PassKey(PBYTE pBuff);
		void GetMac(CML1Dev * pDev, PBYTE pBuff);
		BOOL DoAckAuthentication(CML1Dev * pDev, PBYTE pBuff);
		BOOL DoRspAuthentication(CML1Dev * pDev, PBYTE pBuff);
		BOOL DoPreAuthentication(CML1Dev * pDev);
	virtual	BOOL DoPostAuthentication(CML1Dev * pDev);
	};

#endif

// End of File
