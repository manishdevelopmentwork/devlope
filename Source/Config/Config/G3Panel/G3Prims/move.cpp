
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Movement Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimMove, CPrimSet);

// Constructor

CPrimMove::CPrimMove(void)
{
	}

// Operations

void CPrimMove::SetRect(CRect const &NewRect)
{
	CPrimSet::SetRect(NewRect);
	}

// Overridables

void CPrimMove::SetRect(CRect const &OldRect, CRect const &NewRect)
{
	if( m_pDbase->HasFlag(L"PrimConvert") ) {

		INDEX Index = m_pList->GetHead();

		while( !m_pList->Failed(Index) ) {

			m_pList->GetItem(Index)->SetRect(OldRect, NewRect);

			m_pList->GetNext(Index);
			}
		}
	else
		m_pList->SizePrims(OldRect, NewRect);

	CPrimSet::SetRect(OldRect, NewRect);
	}

// Meta Data

void CPrimMove::AddMetaData(void)
{
	CPrimSet::AddMetaData();

	Meta_SetName((IDS_MOVEMENT));
	}

//////////////////////////////////////////////////////////////////////////
//
// 2D Movement Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimMove2D, CPrimMove);

// Constructor

CPrimMove2D::CPrimMove2D(void)
{
	m_pPosX = NULL;
	
	m_pMinX = NULL;

	m_pMaxX = NULL;

	m_pPosY = NULL;
	
	m_pMinY = NULL;

	m_pMaxY = NULL;

	m_uType = 0x30;
	}

// UI Overridables

void CPrimMove2D::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == L"PosX" || Tag == L"PosY" ) {

			DoEnables(pHost);
			}
		}

	CPrimMove::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimMove2D::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.StartsWith(L"Pos") || Tag.StartsWith(L"Min") || Tag.StartsWith(L"Max") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimMove::GetTypeData(Tag, Type);
	}

// Download Support

BOOL CPrimMove2D::MakeInitData(CInitData &Init)
{
	CPrimMove::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pPosX);
	Init.AddItem(itemVirtual, m_pMinX);
	Init.AddItem(itemVirtual, m_pMaxX);
	Init.AddItem(itemVirtual, m_pPosY);
	Init.AddItem(itemVirtual, m_pMinY);
	Init.AddItem(itemVirtual, m_pMaxY);

	return TRUE;
	}

// Meta Data

void CPrimMove2D::AddMetaData(void)
{
	CPrimMove::AddMetaData();

	Meta_AddVirtual(PosX);
	Meta_AddVirtual(MaxX);
	Meta_AddVirtual(MinX);
	Meta_AddVirtual(PosY);
	Meta_AddVirtual(MaxY);
	Meta_AddVirtual(MinY);

	Meta_SetName((IDS_D_MOVEMENT));
	}

// Implementation

void CPrimMove2D::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"MinX", !!m_pPosX);

	pHost->EnableUI(this, L"MaxX", !!m_pPosX);
	
	pHost->EnableUI(this, L"MinY", !!m_pPosY);
	
	pHost->EnableUI(this, L"MaxY", !!m_pPosY);
	}

//////////////////////////////////////////////////////////////////////////
//
// Polar Movement Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimMovePolar, CPrimMove);

// Constructor

CPrimMovePolar::CPrimMovePolar(void)
{
	m_pPosR = NULL;
	
	m_pMinR = NULL;

	m_pMaxR = NULL;

	m_pPosT = NULL;
	
	m_pMinT = NULL;

	m_pMaxT = NULL;

	m_uType = 0x31;
	}

// UI Overridables

void CPrimMovePolar::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == L"PosT" || Tag == L"PosR" ) {

			DoEnables(pHost);
			}
		}

	CPrimMove::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimMovePolar::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.StartsWith(L"Pos") || Tag.StartsWith(L"Min") || Tag.StartsWith(L"Max") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimMove::GetTypeData(Tag, Type);
	}

// Overridables

void CPrimMovePolar::Draw(IGDI *pGDI, UINT uMode)
{
	if( uMode == drawSet ) {

		CRect Used = GetUsedRect();

		R2    Rect = m_DrawRect;

		int   cx   = Used.cx() / 2;

		int   cy   = Used.cy() / 2;

		DeflateRect(Rect, cx, cy);

		pGDI->SelectPen(penFore);

		pGDI->SetPenFore(GetRGB(31,31,31));

		pGDI->DrawEllipse(PassRect(Rect), etWhole);
		}

	CPrimSet::Draw(pGDI, uMode);
	}

// Download Support

BOOL CPrimMovePolar::MakeInitData(CInitData &Init)
{
	CPrimMove::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pPosT);
	Init.AddItem(itemVirtual, m_pMinT);
	Init.AddItem(itemVirtual, m_pMaxT);
	Init.AddItem(itemVirtual, m_pPosR);
	Init.AddItem(itemVirtual, m_pMinR);
	Init.AddItem(itemVirtual, m_pMaxR);

	return TRUE;
	}

// Meta Data

void CPrimMovePolar::AddMetaData(void)
{
	CPrimMove::AddMetaData();

	Meta_AddVirtual(PosT);
	Meta_AddVirtual(MaxT);
	Meta_AddVirtual(MinT);
	Meta_AddVirtual(PosR);
	Meta_AddVirtual(MaxR);
	Meta_AddVirtual(MinR);

	Meta_SetName((IDS_POLAR_MOVEMENT));
	}

// Implementation

void CPrimMovePolar::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"MinT", !!m_pPosT);

	pHost->EnableUI(this, L"MaxT", !!m_pPosT);
	
	pHost->EnableUI(this, L"MinR", !!m_pPosR);
	
	pHost->EnableUI(this, L"MaxR", !!m_pPosR);
	}

// End of File
