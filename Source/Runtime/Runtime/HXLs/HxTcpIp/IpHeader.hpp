
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IpHeader_HPP

#define	INCLUDE_IpHeader_HPP

//////////////////////////////////////////////////////////////////////////
//
// IP Header
//

#pragma pack(1)

#if defined(AEON_LITTLE_ENDIAN)

struct IPHEADER
{
	BYTE		m_HdrLen :4;
	BYTE		m_Version:4;
	BYTE		m_Type;
	WORD		m_TotalLen;
	WORD		m_Id;
	WORD		m_Fragment;
	BYTE		m_TTL;
	BYTE		m_Protocol;
	WORD		m_Checksum;
	CIpAddr		m_IpSrc;
	CIpAddr		m_IpDest;
	};

#else

struct IPHEADER
{
	BYTE		m_Version:4;
	BYTE		m_HdrLen :4;
	BYTE		m_Type;
	WORD		m_TotalLen;
	WORD		m_Id;
	WORD		m_Fragment;
	BYTE		m_TTL;
	BYTE		m_Protocol;
	WORD		m_Checksum;
	CIpAddr		m_IpSrc;
	CIpAddr		m_IpDest;
	};

#endif

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// IP Header Wrapper
//

class CIpHeader : public IPHEADER
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Attributes
		WORD GetDataLength(void) const;
		WORD GetOptsLength(void) const;
		BOOL TestChecksum(void) const;

		// Operations
		void SetHeader(IPREF IpSrc, IPREF IpDest, WORD Prot, UINT uSize);
		void AddChecksum(void);

	private:
		// Constructor
		CIpHeader(void);

	protected:
		// Static Data
		static WORD m_NextId;

		// Implementation
		WORD CalcChecksum(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE void CIpHeader::NetToHost(void)
{
	m_TotalLen = ::NetToHost(m_TotalLen);

	m_Id       = ::NetToHost(m_Id);
	
	m_Fragment = ::NetToHost(m_Fragment);
	}

STRONG_INLINE void CIpHeader::HostToNet(void)
{
	m_TotalLen = ::HostToNet(m_TotalLen);

	m_Id       = ::HostToNet(m_Id);
	
	m_Fragment = ::HostToNet(m_Fragment);
	}

// End of File

#endif
