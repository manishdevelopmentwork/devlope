//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_EMCORIOLISDEVICES_HPP

#define INCLUDE_EMCORIOLISDEVICES_HPP

#define IS_READ_ONLY 1
#define IS_NAMED     1
#define IS_STRING    1

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Device Models
//

enum Models {

	CDMAnalog	= 0,
	Model2700Analog,			
	MVD9739,			
	Model1500LFT2,			
	Model1700Analog,			
	Model2500,			
	Series3000MVD,			
	Series3000NOC4Wire,		
	MVDDirectConnectEnhanced,	
	MVDDirectConnectStandard,	
	Model5700Config,			
	RFT9739,				
	FDMAnalog,			
	HFVMAnalog,			
	SGMAnalog,			
	GDMAnalog,		
	FillingMassTransmitter,		
	Model1500Filling,		
	FVMAnalog,			
	Series3000NOC9Wire,		

	MAX_MODELS,
	};

enum Channels {
	
	holdingRegisters = 1,
	inputRegisters	 = 2,
	outputCoils	 = 3,
	inputCoils	 = 4,
	Coils		 = 5,
	Registers	 = 6,

	MAX_CHANNELS,
	};

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Device Register
//

struct CEMRegister
{
	UINT	m_uTable;
	UINT	m_uType;
	UINT	m_uMin;
	UINT	m_uMax;
	BOOL    m_fIsString;
	BOOL    m_fReadOnly;
	CString m_ExpandPrefix;
	CString m_Caption;
	
	BOOL operator ==(CEMRegister const &That);
	void Empty(void);
	};

typedef CArray<CEMRegister> CRegArray;

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Devices 
//

class CEMCoriolisDevices 
{
	public:

		// Constructor
		CEMCoriolisDevices(void);

		// Destructor
		~CEMCoriolisDevices(void);

		// Data Access
		CString GetPrefixText(UINT uChannel);
		CString GetCaptionText(UINT uChannel);
		CString GetNewCaption(UINT uChannel);
		CString GetNewPrefix(UINT uChannel);
		UINT    GetCurrentTable(UINT uChannel);

	protected:
		
		// Parse Registers
		CRegArray LoadRegisters(CEMRegister Register[], UINT uLength);

		// Generic Register
		CRegArray LoadGenericRegister(UINT uChannel);

		// Load Models;
		CRegArray LoadCDMAnalog(UINT uChannel);
		CRegArray LoadModel2700Analog(UINT uChannel);			
		CRegArray LoadMVD9739(UINT uChannel);				
		CRegArray LoadModel1500LFT2(UINT uChannel);			
		CRegArray LoadModel1700Analog(UINT uChannel);			
		CRegArray LoadModel2500(UINT uChannel);			
		CRegArray LoadSeries3000MVD(UINT uChannel);			
		CRegArray LoadSeries3000NOC4Wire(UINT uChannel);			
		CRegArray LoadMVDDirectConnectEnhanced(UINT uChannel);	
		CRegArray LoadMVDDirectConnectStandard(UINT uChannel);	
		CRegArray LoadModel5700Config(UINT uChannel);			
		CRegArray LoadRFT9739(UINT uChannel);				
		CRegArray LoadFDMAnalog(UINT uChannel);			
		CRegArray LoadHFVMAnalog(UINT uChannel);			
		CRegArray LoadSGMAnalog(UINT uChannel);			
		CRegArray LoadGDMAnalog(UINT uChannel);	
		CRegArray LoadFillingMassTransmitter(UINT uChannel);
		CRegArray LoadModel1500Filling(UINT uChannel);
		CRegArray LoadFVMAnalog(UINT uChannel);	
		CRegArray LoadSeries3000NOC9Wire(UINT uChannel);

		// Properties
		CRegArray m_Registers;
		CString   m_Prefix[MAX_CHANNELS];
		CString   m_Caption[MAX_CHANNELS];
	};

#endif