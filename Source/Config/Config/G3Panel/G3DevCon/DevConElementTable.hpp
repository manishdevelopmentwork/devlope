
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementTable_HPP

#define INCLUDE_DevConElementTable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConElement;
class CDevConListView;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Table UI Element
//

class CDevConElementTable : public CDevConElementBase
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementTable(void);

	// Destructor
	~CDevConElementTable(void);

	// Operations
	void AddLayout(CLayFormation *pForm);
	void CreateControls(CWnd &Wnd, UINT &id);
	void ParseConfig(CJsonData *pSchema, CJsonData *pTable);
	UINT OnNotify(UINT uID, UINT uNotify, CWnd &Wnd);
	UINT OnNotify(UINT uID, NMHDR &Info);

protected:
	// Column
	struct CColumn
	{
		CJsonData      * m_pField;
		UINT	         m_uSub;
		CString	         m_Label;
		int	         m_xSize;
		CString		 m_Type;
		CDevConElement * m_pElem;
	};

	// Typedefs
	typedef CArray<CColumn> CColList;

	// Data Member
	CColList          m_Cols;
	BOOL	          m_fFixed;
	BOOL              m_fFull;
	CDevConListView * m_pListCtrl;
	CJsonData       * m_pTable;
	CJsonData       * m_pFields;
	CJsonData       * m_pSchema;
	CJsonData       * m_pRoot;
	CJsonData       * m_pData;

	// Overridables
	void OnSetData(void);
	void LoadData(void);
	BOOL AllowPersonality(void);

	// Implementation
	void SetRow(UINT uRow, CJsonData *pRow);
	UINT ClearRow(UINT uRow);
	UINT DeleteRow(UINT uRow);
	UINT InsertRow(void);
	UINT EditRow(UINT uRow, CString const &Verb);
	UINT MoveRowUp(UINT uRow);
	UINT MoveRowDn(UINT uRow);
	void Renumber(void);
	void AddEmptyRow(void);
	void DelEmptyRow(void);
	int  GetWidth(CWnd &Wnd, CString const &Text);
};

// End of File

#endif
