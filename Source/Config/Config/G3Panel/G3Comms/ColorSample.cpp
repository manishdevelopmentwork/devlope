
#include "Intern.hpp"

#include "ColorSample.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text Color Sample
//

// Dynamic Class

AfxImplementRuntimeClass(CColorSample, CStatic);

// Constructor

CColorSample::CColorSample(void)
{
	m_Pair = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));

	m_Fore = afxColor(WHITE);

	m_Back = afxColor(BLACK);
	}

// Operations

void CColorSample::SetColor(DWORD Pair)
{
	m_Pair = Pair;

	m_Fore = C3GetWinColor(LOWORD(Pair));

	m_Back = C3GetWinColor(HIWORD(Pair));

	Invalidate(FALSE);
	}

// Message Map

AfxMessageMap(CColorSample, CStatic)
{
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ENABLE)

	AfxMessageEnd(CColorSample)
	};

// Message Handlers

BOOL CColorSample::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CColorSample::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( IsWindowEnabled() ) {

		CRect Rect = GetClientRect();

		DC.FrameRect(Rect--, afxBrush(3dFace));

		DC.FillRect (Rect--, CBrush(m_Back));

		DC.SetBkColor  (m_Back);

		DC.SetTextColor(m_Fore);

		DC.Select(afxFont(Bolder));

		CString Text = GetWindowText();

		CSize   Size = DC.GetTextExtent(Text);

		DC.TextOut(Rect.GetTopLeft() + (Rect.GetSize() - Size) / 2, Text);

		DC.Deselect();

		return;
		}

	CRect Rect = GetClientRect();

	DC.FillRect(Rect, afxBrush(TabFace));
	}

void CColorSample::OnEnable(BOOL fEnable)
{
	Invalidate(FALSE);
	}

// End of File
