
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <sys/reent.h>

//////////////////////////////////////////////////////////////////////////
//
// RTL Support Implementation
//

// Externals

clink int    _tls_force;

clink struct _reent _impure_tls * _thread_impure_ptr;

clink void   __sinit(struct _reent *);

// Code

global void Init_RtlSupport(void)
{
	__sinit(_thread_impure_ptr);

	_tls_force = 1;
	}

global void Term_RtlSupport(void)
{
	}

// End of File
