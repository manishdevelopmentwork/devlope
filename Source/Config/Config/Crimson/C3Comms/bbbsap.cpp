
#include "intern.hpp"

#include "bbbsap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Slave TCP/IP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPTCPSDriverOptions, CUIItem);

// Constructor

CBBBSAPTCPSDriverOptions::CBBBSAPTCPSDriverOptions(void)
{
	m_Socket   = 1234;

	m_Count    = 2;
	}

// UI Management

void CBBBSAPTCPSDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CBBBSAPTCPSDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Socket));
	Init.AddWord(WORD(m_Count));

	return TRUE;
	}

// Meta Data Creation

void CBBBSAPTCPSDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Socket);
	Meta_AddInteger(Count);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Slave Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBBBSAPSerialSDriverOptions, CUIItem);

// Constructor

CBBBSAPSerialSDriverOptions::CBBBSAPSerialSDriverOptions(void)
{
	m_Drop   = 1;
	}

// UI Management

void CBBBSAPSerialSDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CBBBSAPSerialSDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CBBBSAPSerialSDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Tags BASE Driver
//

// Constructor

CBBBSAPTagsBaseDriver::CBBBSAPTagsBaseDriver(void)
{
	m_Manufacturer	= "Bristol Babcock BSAP";
	
	m_DevRoot       = "Dev";
	}

// Address Management

BOOL CBBBSAPTagsBaseDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{	
	return ((CBBBSAPTagsDeviceOptions *) pConfig)->ParseAddress(Error, Addr, Text);
	}

BOOL CBBBSAPTagsBaseDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CBBBSAPTagsDeviceOptions *) pConfig)->ExpandAddress(Text, Addr);
	}

BOOL CBBBSAPTagsBaseDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CBBBSAPTagsDeviceOptions *) pConfig)->SelectAddress(hWnd, Addr, fPart);
	}

BOOL CBBBSAPTagsBaseDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CBBBSAPTagsDeviceOptions *) pConfig)->ListAddress(pRoot, uItem, Data);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP Driver
//

// Instantiator

ICommsDriver *	Create_BBBSAPTCPDriver(void)
{
	return New CBBBSAPTCPDriver;
	}

// Constructor

CBBBSAPTCPDriver::CBBBSAPTCPDriver(void)
{
	m_wID		= 0x3526;

	m_uType		= driverMaster;
		
	m_DriverName    = "UDP Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "BSAP Master";
	}

// Binding Control

UINT CBBBSAPTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBBBSAPTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

//CLASS CBBBSAPTCPDriver::GetDriverConfig(void)
//{
//	return AfxRuntimeClass(CBBBSAPDriverOptions);
//	}

// Configuration

CLASS CBBBSAPTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBBBSAPTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Driver
//

// Instantiator

ICommsDriver *	Create_BBBSAPSerialDriver(void)
{
	return New CBBBSAPSerialDriver;
	}

// Constructor

CBBBSAPSerialDriver::CBBBSAPSerialDriver(void)
{
	m_wID		= 0x4053;

	m_uType		= driverMaster;
		
	m_DriverName	= "Serial Master";
	
	m_Version	= "1.01";
	
	m_ShortName	= "BSAP Master";
	}

// Binding Control

UINT	CBBBSAPSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CBBBSAPSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

//CLASS CBBBSAPSerialDriver::GetDriverConfig(void)
//{
//	return AfxRuntimeClass(CBBBSAPDriverOptions);
//	}

// Configuration

CLASS CBBBSAPSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBBBSAPSerialDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP TCP Slave Driver
//

// Instantiator

ICommsDriver *	Create_BBBSAPTCPSDriver(void)
{
	return New CBBBSAPTCPSDriver;
	}

// Constructor

CBBBSAPTCPSDriver::CBBBSAPTCPSDriver(void)
{
	m_wID		= 0x340E;

	m_uType		= driverSlave;
		
	m_DriverName	= "UDP Slave";
	
	m_Version	= "1.01";
	
	m_ShortName	= "BSAP Slave";
	}

// Binding Control

UINT CBBBSAPTCPSDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBBBSAPTCPSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CBBBSAPTCPSDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBBBSAPTCPSDriverOptions);
	}

// Configuration

CLASS CBBBSAPTCPSDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBBBSAPTCPSDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Slave Driver
//

// Instantiator

ICommsDriver *	Create_BBBSAPSerialSDriver(void)
{
	return New CBBBSAPSerialSDriver;
	}

// Constructor

CBBBSAPSerialSDriver::CBBBSAPSerialSDriver(void)
{
	m_wID		= 0x340F;

	m_uType		= driverSlave;
		
	m_DriverName	= "Serial Slave";
	
	m_Version	= "1.01";
	
	m_ShortName	= "BSAP Slave";
	}

// Binding Control

UINT	CBBBSAPSerialSDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CBBBSAPSerialSDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CBBBSAPSerialSDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBBBSAPSerialSDriverOptions);
	}

// Configuration

CLASS CBBBSAPSerialSDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBBBSAPSerialSDeviceOptions);
	}

// End of File
