
#include "intern.hpp"

#include "PrimRubyStar5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 5-Pointed Star Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyStar5, CPrimRubyStar)

// Constructor

CPrimRubyStar5::CPrimRubyStar5(void)
{
	m_nSides = 10;

	m_AltRad = 35;

	m_Rotate = +900;
	}

// Meta Data

void CPrimRubyStar5::AddMetaData(void)
{
	CPrimRubyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR_6));
	}

// End of File
