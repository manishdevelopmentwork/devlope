
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortList_HPP

#define INCLUDE_CommsPortList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsPort;
class CCommsSysBlock;

//////////////////////////////////////////////////////////////////////////
//
// Comms Port List
//

class DLLAPI CCommsPortList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortList(void);
		CCommsPortList(UINT uLimit);

		// Item Access
		CCommsPort * GetItem(INDEX Index) const;
		CCommsPort * GetItem(UINT  uPort) const;

		// Item Lookup
		CCommsPort     * FindPort  (UINT uPort  ) const;
		CCommsPort     * FindPort  (CString Name) const;
		CCommsDevice   * FindDevice(UINT uDevice) const;
		CCommsDevice   * FindDevice(CString Name) const;
		CCommsSysBlock * FindBlock (UINT uBlock ) const;

		// Attributes
		BOOL    HasRoom(void) const;
		CString MakeUnique(CString Format) const;

		// Operations
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
		void CheckConflicts(void);

		// Conversion
		void PostConvert();

	protected:
		// Data Members
		UINT m_uLimit;
	};

// End of File

#endif
