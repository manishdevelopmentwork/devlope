//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DNP3_HPP
	
#define	INCLUDE_DNP3_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDnpSpace;

//////////////////////////////////////////////////////////////////////////
//
// Enumerations
//

enum PropText {

	textCurrent,
	textFlags,
	textTimeStamp,
	textClass,
	textFreeze,
	textControl,
	textOnTime,
	textOffTime,
	textObject,
	textPoint
	};

enum EventDef {

	eventBI,
	eventDBI,
	eventBO,
	eventC,
	eventFC,
	eventAI,
	eventAO,
	eventTotal,
	};

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

CString m_Label[] = { IDS("Binary Input"),
		      IDS("Double Binary Input"),
		      IDS("Binary Output"),
		      IDS("Counter"),
		      IDS("Frozen Counter"),
		      IDS("Analog Input"),
		      IDS("Analog Output")};

///////////////////////////////////////////////////////////////////////////
//
// DNP3 Event Configuration
//

class CDnp3EventConfig : public CUIItem
{
	public :
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Construtor
		CDnp3EventConfig(void);
		CDnp3EventConfig(UINT uMode, UINT uLimit);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		void GetModeData (UINT uItem, CUIData &Data);
		void GetLimitData(UINT uItem, CUIData &Data);

		// Data Members
		UINT m_Mode;
		UINT m_Limit;
		
	protected:
		
		// Meta Data Creation
		void AddMetaData(void);	
	};

///////////////////////////////////////////////////////////////////////////
//
// DNP3 Event Config List
//

class CDnp3EventConfigList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3EventConfigList(void);

		// Item Access
		CDnp3EventConfig * GetItem(UINT  uPos ) const;
	};

///////////////////////////////////////////////////////////////////////////
//
// DNP3 Driver Options
//

class CDnp3DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3DriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Source;
		
				
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Device Options
//

class CDnp3DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3DeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Dest;
		UINT m_TO;
		UINT m_Link;
						
	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Master Device Options
//

class CDnp3MasterDeviceOptions : public CDnp3DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3MasterDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Poll;
		UINT m_Class0;
		UINT m_Class1;
		UINT m_Class2;
		UINT m_Class3;
								
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Slave Device Options
//

class CDnp3SlaveDeviceOptions : public CDnp3DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3SlaveDeviceOptions(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Unsol;
		UINT m_Class1;
		UINT m_Class2;
		UINT m_Class3;
		UINT m_Retain;
		UINT m_AiCalc;

		// Event Config
		CDnp3EventConfigList * m_pEvents;
					
	protected:

		// Implementation
		void InitEvents(void);
		void AddEvent(UINT uMode, UINT uLimit);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Slave Device Options UI Page
//

class CDnp3SlaveDeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDnp3SlaveDeviceOptionsUIPage(CDnp3SlaveDeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CDnp3SlaveDeviceOptions * m_pOption;
	}; 


//////////////////////////////////////////////////////////////////////////
//
// DNP3 TCP Master Device Options
//

class CDnp3TcpMasterDeviceOptions : public CDnp3MasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3TcpMasterDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_UdpPort;
		UINT m_Time1;
		UINT m_Keep;
		UINT m_Time3;
		UINT m_Prot;
			
	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 TCP Slave Device Options
//

class CDnp3TcpSlaveDeviceOptions : public CDnp3SlaveDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDnp3TcpSlaveDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Addr;
		UINT m_Addr2;
		UINT m_Socket;
		UINT m_UdpPort;	
		UINT m_Time1;
		UINT m_Prot;
				
	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// DNP3 Base Driver
//

class CDnp3BaseDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CDnp3BaseDriver(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Data Access
		BOOL IsBinaryCommand(UINT uTable);
		BOOL IsFeedback(UINT uTable);
		
		// Prop Lookup
		CString GetPropAsText(UINT uProp, CSpace * pSpace);
		UINT	GetPropAsUint(CString Prop, CSpace * pSpace);

	protected:
		
		// Implementation
		void AddSpaces(void);

		// Text Lookup
		CString GetPropText(UINT uEnum);

		// Helpers
		BOOL IsDouble(CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Base Driver
//

class CDnp3SerialDriver : public CDnp3BaseDriver
{
	public:
		// Constructor
		CDnp3SerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Master Driver
//

class CDnp3MasterSerialDriver : public CDnp3SerialDriver
{
	public:
		// Constructor
		CDnp3MasterSerialDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Slave Driver
//

class CDnp3SlaveSerialDriver : public CDnp3SerialDriver
{
	public:
		// Constructor
		CDnp3SlaveSerialDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Serial Slave Driver v2
//

class CDnp3SlaveSerialv2Driver : public CDnp3SlaveSerialDriver
{
	public:
		// Constructor
		CDnp3SlaveSerialv2Driver(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Base Driver
//

class CDnp3TcpDriver : public CDnp3BaseDriver
{
	public:
		// Constructor
		CDnp3TcpDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Master Driver
//

class CDnp3MasterTcpDriver : public CDnp3TcpDriver
{
	public:
		// Constructor
		CDnp3MasterTcpDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Slave Driver
//

class CDnp3SlaveTcpDriver : public CDnp3TcpDriver
{
	public:
		// Constructor
		CDnp3SlaveTcpDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dnp3 Tcp Slave Driver v2
//

class CDnp3SlaveTcpv2Driver : public CDnp3SlaveTcpDriver
{
	public:
		// Constructor
		CDnp3SlaveTcpv2Driver(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Address Selection Dialog
//

class CDnp3AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDnp3AddrDialog(CDnp3BaseDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message and Notification Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnSpaceChange(UINT uID, CWnd &Wnd);
		void OnPropChange(UINT uId, CWnd &Wnd);
				
		// Overridables
		BOOL	AllowType(UINT uType);
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowDetails(void);

		// Helpers
		CDnpSpace * GetCurSpace(void);
		
	};

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Space Wrapper
//

class CDnpSpace : public CSpace
{
	public:
		// Constructors
		CDnpSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT em);

		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
		
		// Type Handling
		UINT GetWholeType(CString Type);

	protected:
		// Data Members
		UINT m_ExtraMax;
		
	};


// End of File

#endif
