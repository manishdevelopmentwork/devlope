
#include "intern.hpp"

#include "r29id.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw/CAN Sub Elements
//

// Dynamic Class

AfxImplementDynamicClass(CSUB, CMetaItem);

// Constructor

CSUB::CSUB(void)
{
	m_Size   = 0;
   	}

CSUB::CSUB(UINT uSize)
{
	m_Size   = uSize;
	}

// Destructor

CSUB::~CSUB(void)
{
	
	} 

// Download Support

BOOL CSUB::MakeInitData(CInitData &Init)
{
	Init.AddByte(BYTE(m_Size));

	return TRUE;
	}

// Meta Data Creation

void CSUB::AddMetaData(void)	
{
	CMetaItem::AddMetaData();
  
	Meta_AddInteger(Size);

	Meta_SetName(L"Sub Element");
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw / CAN Sub Element List Class
//

// Dynamic Class

AfxImplementDynamicClass(CSUBList, CItemList);

// Constructor

CSUBList::CSUBList(void)
{	
	}

// Destructor

CSUBList::~CSUBList(void)
{	
	DeleteAllItems(TRUE);
	}

// Item Access

CSUB * CSUBList::GetItem(INDEX Index) const
{
	return (CSUB *) CItemList::GetItem(Index);
	}

CSUB * CSUBList::GetItem(UINT uPos) const
{
	return (CSUB *) CItemList::GetItem(uPos);
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit Identifier
//

// Dynamic Class

AfxImplementDynamicClass(CID29, CMetaItem);

// Constructor

CID29::CID29(void)
{
	m_Number     = 0;

	// Send request is a remnant of the J1939 PGN, 
	// left intact for future use should the need arise.

	m_SendReq    = 0;

	m_Title      = "";

	m_pSUBs      = New CSUBList;

	m_RepRate    = 1000;

	m_Addr.m_Ref = 0;

	m_Active     = 0;

	m_Enhanced   = 0;
	}

CID29::CID29(CID29 * pID)
{
	if( pID ) {

		m_Number     = pID->m_Number;

		// Send request is a remnant of the J1939 PGN, 
		// left intact for future use should the need arise.

		m_SendReq    = pID->m_SendReq;

		m_Title      = pID->m_Title;

		m_RepRate    = pID->m_RepRate;

		m_Addr.m_Ref = pID->m_Addr.m_Ref;

		m_Active     = pID->m_Active;

		m_Enhanced   = pID->m_Enhanced;

		m_pSUBs      = New CSUBList;

		UINT uCount  = pID->GetSUBs()->GetItemCount();
		
		for( UINT u = 0; u < uCount; u++ ) {
			
			CSUB * pNew = pID->GetSUBs()->GetItem(u);

			UINT uSize  = pNew->m_Size;

			CSUB * pSUB = New CSUB(uSize);

			m_pSUBs->AppendItem(pSUB);
			}
		} 
	}
 
// Destructor
 
CID29::~CID29(void)
{
	m_pSUBs->DeleteAllItems(TRUE);
	} 

// Download Support

BOOL CID29::MakeInitData(CInitData &Init)
{
	Init.AddLong(LONG(m_Number));

	Init.AddLong(LONG(m_Addr.m_Ref));

	// Send request is a remnant of the J1939 PGN, 
	// left intact for future use should the need arise.

	Init.AddByte(BYTE(m_SendReq));

	m_pSUBs->MakeInitData(Init);

	Init.AddLong(LONG(m_RepRate));

	Init.AddByte(BYTE(m_Active));

	Init.AddByte(BYTE(m_Enhanced));
			
	return TRUE;
	}

// Meta Data Creation

void CID29::AddMetaData(void)	
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Number);

	Meta_AddInteger(Addr.m_Ref);

	// Send request is a remnant of the J1939 PGN, 
	// left intact for future use should the need arise.
  
	Meta_AddInteger(SendReq);

	Meta_AddCollect(SUBs);

	Meta_AddInteger(RepRate);

	Meta_AddString(Title);

	Meta_AddInteger(Active);

	Meta_AddInteger(Enhanced);

	Meta_SetName(L"29-bit ID");
	}

// Data Access

void CID29::SetAddrRef(DWORD AddrRef)
{
	m_Addr.m_Ref    = AddrRef;	

	m_Addr.a.m_Type = addrLongAsLong;
	}

BOOL CID29::IsDirty(BOOL fReq, UINT uRepRate)
{
	return fReq != FALSE || uRepRate != m_RepRate;
	}

// SUB List Management

BOOL CID29::GrowSUBList(UINT uCount)
{
	if( m_pSUBs ) {

		UINT uIndex = m_pSUBs->GetItemCount();

		if( uIndex > uCount ) {

			INDEX i = m_pSUBs->GetTail();

			for( UINT u = uIndex; u > uCount; i = m_pSUBs->GetTail(), u-- ) {

				CSUB * pSUB = m_pSUBs->GetItem(i);

				if( pSUB ) {

					m_pSUBs->RemoveItem(pSUB);

					delete pSUB;
					}
				}
			}

		else {  
	       		while( uIndex < uCount ) {

				UINT uSize  = type1bit;

				CSUB * pSUB = New CSUB(uSize);
				
				if( pSUB ) {
				
					m_pSUBs->AppendItem(pSUB);
					}

				uIndex++;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

CSUBList * CID29::GetSUBs(void)
{
	return m_pSUBs;
	}

//////////////////////////////////////////////////////////////////////////
//
// 29-bit Identifier List Class
//

// Dynamic Class

AfxImplementDynamicClass(CID29List, CItemList);

// Constructor

CID29List::CID29List(void)
{	
	}

// Destructor

CID29List::~CID29List(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CID29 * CID29List::GetItem(INDEX Index) const
{
	return (CID29 *) CItemList::GetItem(Index);
	}

CID29 * CID29List::GetItem(UINT uPos) const
{
	return (CID29 *) CItemList::GetItem(uPos);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Raw Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCAN29bitIdEntryRawDriverOptions, CUIItem);

// Constructor

CCAN29bitIdEntryRawDriverOptions::CCAN29bitIdEntryRawDriverOptions(void)
{
	}

void CCAN29bitIdEntryRawDriverOptions::AddMetaData(void)
{ 	
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Raw Device Options UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CCAN29bitIdEntryRawDeviceOptionsUIPage, CUIStdPage);

// Constructor

CCAN29bitIdEntryRawDeviceOptionsUIPage::CCAN29bitIdEntryRawDeviceOptionsUIPage(CCAN29bitIdEntryRawDeviceOptions * pOption) 
{
	m_pOption = pOption;
	}

// Operations

BOOL CCAN29bitIdEntryRawDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{	
	pView->StartGroup(L"CAN 29-bit Identifier Messages", 1);

	pView->AddButton(L"Edit Messages", L"", L"ButtonManageMessages");
	
	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Raw Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCAN29bitIdEntryRawDeviceOptions, CUIItem);

// Constructor

CCAN29bitIdEntryRawDeviceOptions::CCAN29bitIdEntryRawDeviceOptions(void)
{
	m_pIDs     = New CID29List;   

	m_Last     = 0xFF00;

	m_BackOff  = 0;
	}

// Destructor

CCAN29bitIdEntryRawDeviceOptions::~CCAN29bitIdEntryRawDeviceOptions(void)
{
	}

// UI Loading

BOOL CCAN29bitIdEntryRawDeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	CCAN29bitIdEntryRawDeviceOptionsUIPage * pPage = New CCAN29bitIdEntryRawDeviceOptionsUIPage(this);

	pList->Append(pPage);
	
	return TRUE;			   
	}

// UI Managament

void CCAN29bitIdEntryRawDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == L"ButtonManageMessages" ) {

			HGLOBAL hPrev = m_pIDs->TakeSnapshot();

			CSystemItem *pSystem = GetDatabase()->GetSystemItem();

			CId29MessDialog Dlg(this, pSystem);
	
			if( Dlg.Execute(*afxMainWnd) ) {
				
				HGLOBAL        hData = m_pIDs->TakeSnapshot();

				CCmdSubItem *  pCmd  = New CCmdSubItem( L"Config/IDs",
									hPrev,
									hData
									);
				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else {
					pHost->SaveExtraCmd(pCmd);

					SetDirty();
					}
				}
			else
				GlobalFree(hPrev);
			}
 		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Data Access

UINT CCAN29bitIdEntryRawDeviceOptions::GetNext(void)
{
	m_Last += 0x100;

	return m_Last;
	}

CID29 * CCAN29bitIdEntryRawDeviceOptions::GetID(UINT uNumber, UINT uMatch)
{     
	UINT uCount = m_pIDs->GetItemCount();

	UINT uFound = 0;

	for( UINT u = 0; u < uCount; u++ ) {

		if( uNumber == m_pIDs->GetItem(u)->m_Number ) {

			if( uFound == uMatch ) {

				return m_pIDs->GetItem(u);
				}

			uFound++;
			}
		}
	
	return NULL;
	}

CID29 * CCAN29bitIdEntryRawDeviceOptions::GetID(CString Text)
{
	if( !Text.IsEmpty() ) {

		UINT uFind = Text.Find('\t');

		if( uFind < NOTHING ) {

			UINT uNum = tstrtoul(Text.Mid(0, uFind), NULL, 16);

			return GetID(uNum);
			}
		}

	return NULL;
	}

CID29 * CCAN29bitIdEntryRawDeviceOptions::FindID(CString Text)
{
	UINT uCount = m_pIDs->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		if( Text == m_pIDs->GetItem(u)->m_Title ) {

			return m_pIDs->GetItem(u);
			}
		}
	
	return NULL;
	}


// Download Support

BOOL CCAN29bitIdEntryRawDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_BackOff));

	m_pIDs->MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CCAN29bitIdEntryRawDeviceOptions::AddMetaData(void)	
{
	CUIItem::AddMetaData();

	Meta_AddInteger(BackOff);

	Meta_AddCollect(IDs);

	Meta_AddInteger(Last);
	
	Meta_SetName(L"Generic 29-bit ID Device Options");
	}

// Implementation

void CCAN29bitIdEntryRawDeviceOptions::InitIDs(void)
{
	if( m_pIDs ) {

		for( INDEX i = m_pIDs->GetHead(); !m_pIDs->Failed(i); m_pIDs->GetNext(i)) {

			CID29 * pItem = m_pIDs->GetItem(i);

			if( pItem ) {

				pItem->m_Addr.m_Ref = 0;

				pItem->m_Active	    = 0;

				m_Last              = 0xFF00;
				}
			}
		}
	}

CID29 * CCAN29bitIdEntryRawDeviceOptions::FindListID(CString Text)
{
	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		CString Entry = Text.Mid(0, uFind);

		CID29 * pID   = FindID(Entry);

		if( !pID ) {

			UINT uNum = tstrtoul(Entry, NULL, 16);

			pID = GetID(uNum);
			}

		return pID;
		}

	return NULL;
	}

// Debugging

void CCAN29bitIdEntryRawDeviceOptions::ShowIDs(void)
{
	for( INDEX i = m_pIDs->GetHead(); !m_pIDs->Failed(i); m_pIDs->GetNext(i) ) {

		CID29 * pID = m_pIDs->GetItem(i);

		AfxTrace(L"\nID %8.8X %s %8.8x", pID->m_Number, pID->m_Title, pID->m_Addr.m_Ref);

		CSUBList * pSUBs = pID->GetSUBs();

		UINT uSUBs = pSUBs->GetItemCount();

		AfxTrace(L" SUBs %u - ", uSUBs);

		for( UINT u = 0; u < uSUBs; u++ ) {

			AfxTrace(L"%u ", pSUBs->GetItem(u)->m_Size);
			}		
		} 
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Entry Raw Driver
//

// Instantiator

ICommsDriver * Create_CAN29bitIdEntryRawDriver(void)
{
	return New CCAN29bitIdEntryRawDriver;
	}

// Constructor			        

CCAN29bitIdEntryRawDriver::CCAN29bitIdEntryRawDriver(void)
{
	m_wID		= 0x40AC;

	m_uType		= driverSlave;

	m_Manufacturer	= "CAN";
	
	m_DriverName	= "Generic 29-bit Identifier";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Gen 29-bit";

	m_DevRoot	= "DEV";

	m_fSingle       = TRUE;

	m_fExpand	= FALSE;
	}

// Destructor

CCAN29bitIdEntryRawDriver::~CCAN29bitIdEntryRawDriver(void)
{
	}

// Driver Data

UINT CCAN29bitIdEntryRawDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagRemotable | dflagRpcMaster;
	}
	
// Binding Control

UINT CCAN29bitIdEntryRawDriver::GetBinding(void)
{
	return bindCAN;
	}

void CCAN29bitIdEntryRawDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 250000;

	CAN.m_Terminate = FALSE;
	}

// Configuration

CLASS CCAN29bitIdEntryRawDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCAN29bitIdEntryRawDriverOptions);
	}

CLASS CCAN29bitIdEntryRawDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCAN29bitIdEntryRawDeviceOptions);
	}

// Notifications

void CCAN29bitIdEntryRawDriver::NotifyInit(CItem * pConfig)
{	
	CCAN29bitIdEntryRawDeviceOptions * pOptions = (CCAN29bitIdEntryRawDeviceOptions *) pConfig;

	if( pOptions ) {

		pOptions->InitIDs();
		}
	}
 
// Address Management

BOOL CCAN29bitIdEntryRawDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCAN29bitIdEntryRawDeviceOptions * pOpt = (CCAN29bitIdEntryRawDeviceOptions *)pConfig;

	CID29List * pList = pOpt->m_pIDs;

	if( pList ) {

		if( pList->GetItemCount() ) {

			CId29MessSelDialog Dlg(*this, Addr, pConfig, fPart);

			return Dlg.Execute(*afxMainWnd);
			}

		afxMainWnd->Information(CString(IDS_IDENTIFIERS_MUST));
		}

	return FALSE;
	}

BOOL CCAN29bitIdEntryRawDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return FALSE;
	}
 
// Address Helpers

BOOL CCAN29bitIdEntryRawDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{	
	CCAN29bitIdEntryRawDeviceOptions * pOpts = (CCAN29bitIdEntryRawDeviceOptions *) pConfig;

	if( pOpts ) {

		CID29 * pID = pOpts->FindListID(Text);

		if( pID ) {

			if( pID->m_Addr.m_Ref == 0 ) {

				pID->SetAddrRef(pOpts->GetNext());
				}

			Addr.m_Ref = pID->m_Addr.m_Ref;

			UINT uFind = Text.Find('*');

			if( uFind < NOTHING ) {

				Text  = Text.Mid(uFind + 1);

				// Send request is a remnant of the J1939 PGN, 
				// left intact for future use should the need arise.

				if( Text.GetAt(0) == 'R' ) {

					pID->m_SendReq = TRUE;

					Text = Text.Mid(1);
					}
				else {
					pID->m_SendReq = FALSE;
					}

				pID->m_RepRate = tstrtoul(Text, NULL, 10);
				}

			pID->m_Active = 1;

			pID->GetDatabase()->SetDirty();

			return TRUE;
			}
		}

	Error.Set("Invalid Identifier", 0);

	return FALSE; 
	}

BOOL CCAN29bitIdEntryRawDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{	
	CCAN29bitIdEntryRawDeviceOptions * pOpts = (CCAN29bitIdEntryRawDeviceOptions *) pConfig;

	if( pOpts ) {

		CID29List * pList = pOpts->m_pIDs;

		if( pList ) {

			UINT uCount = pList->GetItemCount();

			for( UINT u = 0; u < uCount; u++ ) {

				CID29 * pID = pList->GetItem(u);

				if( pID ) {

					if( pID->m_Addr.m_Ref == (Addr.m_Ref & 0xFFFFFF00) ) {

						UINT uSUB = (Addr.m_Ref & 0xFF);

						CSUBList * pList = pID->GetSUBs();

						if( pList ) {

							if( uSUB < pList->GetItemCount() ) {

								Text = CPrintf("%s:%u.%u-bit(s)", pID->m_Title,
												  uSUB + 1,
												  pList->GetItem(uSUB)->m_Size);

								return TRUE;
								}

							Text = L"?.?";

							return TRUE;
							}
						}
					}
				}
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw CAN Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CRawCANDriverOptions, CUIItem);

// Constructor

CRawCANDriverOptions::CRawCANDriverOptions(void)
{
	m_pService = NULL;

	m_RxPDU    = 32;

	m_TxPDU    = 32;

	m_RxMail   = 32;

	m_TxMail   = 32;
	}

// UI Managament

void CRawCANDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "RxMail" ) {

		pWnd->ShowUI("RxMail", FALSE);
		}

	if( Tag.IsEmpty() || Tag == "TxMail" ) {

		pWnd->ShowUI("TxMail", FALSE);
		}
	}


// Download Support

BOOL CRawCANDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pService);

	Init.AddWord(WORD(m_RxPDU));

	Init.AddWord(WORD(m_TxPDU));

	Init.AddWord(WORD(m_RxMail));

	Init.AddWord(WORD(m_TxMail));

	return TRUE;
	}

// Meta Data Creation

void CRawCANDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Service);

	Meta_AddInteger(RxPDU);

	Meta_AddInteger(TxPDU);

	Meta_AddInteger(RxMail);

	Meta_AddInteger(TxMail);

	Meta_SetName(IDS_DRIVER_OPTIONS);
	}


//////////////////////////////////////////////////////////////////////////
//
// CAN Raw Driver
//

// Instantiator

ICommsDriver * Create_CANRawDriver(void)
{
	return New CCANRawDriver;
	}

// Constructor			        

CCANRawDriver::CCANRawDriver(void)
{
	m_wID		= 0x40AE;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Raw 29-bit CAN Port";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Raw CAN";

	m_fSingle	= TRUE;
	}

// Destructor

CCANRawDriver::~CCANRawDriver(void)
{
	}

// Binding Control

UINT CCANRawDriver::GetBinding(void)
{
	return bindCAN;
	}

void CCANRawDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 250000;

	CAN.m_Terminate = FALSE;
	}

// Configuration

CLASS CCANRawDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CRawCANDriverOptions);
	}


/////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Raw Message Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CId29MessSelDialog, CStdDialog);
		
// Constructor

CId29MessSelDialog::CId29MessSelDialog(CCAN29bitIdEntryRawDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;
	
	SetName(L"Id29SelDlg");
 	}

// Message Map

AfxMessageMap(CId29MessSelDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange) 
	
	AfxMessageEnd(CId29MessSelDialog)
	};

// Message Handlers

BOOL CId29MessSelDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadIDs();

	LoadOptions();

	LoadRepRate();

	return TRUE;
	}

// Notification Handlers

void CId29MessSelDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == 1001 ) {

		SetOptions();

		SetRepRate();
		}
	}


// Command Handlers

BOOL CId29MessSelDialog::OnOkay(UINT uID)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uSel = Box.GetCurSel();

	if( uSel > 0 ) {

		CError   Error(TRUE);
		
		CAddress Addr;

		CString Text = Box.GetText(uSel);

		Text.Replace('\t', ':');

		CButton &Button = (CButton &) GetDlgItem(2002);

		Text += "*";

		// Send request is a remnant of the J1939 PGN, 
		// left intact for future use should the need arise.

		if( Button.IsChecked() ) {

			Text += "R";
			}

		CEditCtrl &RepRate = (CEditCtrl &) GetDlgItem(2003);

		Text += CPrintf("%5.5u", tstrtol(RepRate.GetWindowText(), NULL, 10));

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;			
			}	   
		}

	m_pAddr->m_Ref  = 0;
	
	EndDialog(TRUE);

	return TRUE;				    
	}

// Implementation

void CId29MessSelDialog::LoadIDs(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	int nTab[] = { 40, 110 };

	Box.SetTabStops(elements(nTab), nTab);
	
	Box.AddString(CString(IDS_IDTNO_SELECTION));

	CCAN29bitIdEntryRawDeviceOptions * pOpt = (CCAN29bitIdEntryRawDeviceOptions *) m_pConfig;

	if( pOpt ) {

		CID29List * pIDs = pOpt->m_pIDs;

		if( pIDs ) {

			UINT uCount = pIDs->GetItemCount();

			CStringArray List;

			CString Sel = "";

			for( UINT u = 0; u < uCount; u++ ) {

				CID29 * pID = pIDs->GetItem(u);

				CString ID = CPrintf("%8.8X", pID->m_Number);

				ID += "\t";

				ID += pID->m_Title;

				List.Append(ID);

				if( m_pAddr->m_Ref && pID->m_Addr.m_Ref == (m_pAddr->m_Ref & 0xFFFFFF00) ) {

					Sel = ID;
					}
				}

			List.Sort();

			Box.AddStrings(List);

			for( UINT n = 0; n < uCount; n++ ) {

				if( Sel == List.GetAt(n) ) {

					Box.SetCurSel(n + 1);

					return;
					}
				}

			Box.SetCurSel(0);
			}
		}
	}

void CId29MessSelDialog::LoadOptions(void)
{
	SetOptions();
	}

void CId29MessSelDialog::LoadRepRate(void)
{
	SetRepRate();
	}	

void CId29MessSelDialog::SetOptions(void)
{
	if( m_pDriver ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);
	
		CButton &Button = (CButton &) GetDlgItem(2002);

		Button.SetCheck(FALSE);

		CCAN29bitIdEntryRawDeviceOptions * pOptions = (CCAN29bitIdEntryRawDeviceOptions *) m_pConfig;

		if( pOptions ) {

			UINT uSel   = Box.GetCurSel();

			CString Sel = Box.GetText(uSel);

			CID29 * pID = pOptions->GetID(Sel);

			if( pID ) {

				// Send request is a remnant of the J1939 PGN, 
				// left intact for future use should the need arise.

				Button.SetCheck(pID->m_SendReq);
				}

			Button.EnableWindow(pID ? TRUE : FALSE);
			}
		}
	}

void CId29MessSelDialog::SetRepRate(void)
{
	if( m_pDriver ) {

		CEditCtrl &Ctrl = (CEditCtrl &) GetDlgItem(2003);

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		CCAN29bitIdEntryRawDeviceOptions * pOptions = (CCAN29bitIdEntryRawDeviceOptions *) m_pConfig;

		if( pOptions ) {

			UINT uSel   = Box.GetCurSel();

			CString Sel = Box.GetText(uSel);

			CID29 * pID = pOptions->GetID(Sel);

			if( pID ) {

				UINT uRepRate = pID->m_RepRate;

				CString RepRate = "%u";

				RepRate.Printf(RepRate, uRepRate);
		
				Ctrl.SetWindowText(RepRate);
				}

			Ctrl.EnableWindow(pID ? TRUE : FALSE);
			}
		}
	} 


//////////////////////////////////////////////////////////////////////////
//
// CAN 29-bit Identifier Raw Message Configuration Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CId29MessDialog, CStdDialog);
		
// Constructor

CId29MessDialog::CId29MessDialog(CCAN29bitIdEntryRawDeviceOptions * pOptions, CSystemItem *pSystem)
{
	memset(m_pSUBs, 0, sizeof(m_pSUBs));

	m_pOptions = pOptions;

	m_fRebuild = FALSE;
	
	m_fChange  = FALSE;

	m_pSystem  = pSystem;

	m_pIDs     = (CID29List *) m_pOptions->MakeFromItem(m_pOptions, m_pOptions->m_pIDs);

	SetName(L"Id29MessDlg");
	}

// Message Map

AfxMessageMap(CId29MessDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_COMMAND)
	
	AfxDispatchCommand(CP_ADD, OnCreateEdit)
	AfxDispatchCommand(CP_DEL, OnRemove)
	AfxDispatchCommand(IDOK,   OnOkay)
		
	AfxDispatchNotify(1001,		LBN_SELCHANGE,	OnIDChange )
	AfxDispatchNotify(CP_NUM,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_TITLE,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPNs,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN1,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN2,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN3,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN4,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN5,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN6,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN7,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_SPN8,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_PAGE,	EN_KILLFOCUS,	OnEditChange)
	AfxDispatchNotify(CP_ENH,	CBN_SELCHANGE,	OnEditChange)
	
	AfxMessageEnd(CId29MessDialog)
	};
 
// Message Handlers

BOOL CId29MessDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadIDList();

	InitAll();
	
	return TRUE;
	}

// Notification Handlers

void CId29MessDialog::OnIDChange(UINT uID, CWnd &Wnd)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uIndex   = Box.GetCurSelData();

	if( uIndex == NOTHING ) {

		InitAll();
		
		return;
		}

	CID29 * pID = m_pIDs->GetItem(uIndex);

	SetID(pID);
		
	GetDlgItem(CP_PAGE).SetWindowText("1");

	SetEnhanced(pID->m_Enhanced);

	SetSUBs(TRUE);

	DoEnables();
	}

void CId29MessDialog::OnDiagChange(UINT uID, CWnd &Wnd)
{
	CComboBox &Box = (CComboBox &)GetDlgItem(CP_DIAG);

	UINT uSel = Box.GetCurSel();

	if( uSel ) {

		GetDlgItem(CP_SPNs).SetWindowText("8");

		CString Text = uSel == 2 ? "32" : "8";

		for( UINT u = CP_SPN1; u <= CP_SPN8; u+=2 ) {

			GetDlgItem(u).SetWindowText(Text);

			OnEditChange(u, Wnd);
			}
		}

	DoEnables();
	}

void CId29MessDialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	if( !GetDlgItem(uID).IsWindowVisible() ) {

		return;
		}

	if( uID == CP_ENH || uID == CP_TITLE ) {

		DoEnables();

		return;
		}

	UINT uHi = 0;

	UINT uLo = 0;

	BOOL fSUBs = FALSE;

	BOOL fPage = FALSE;

	if( uID == CP_NUM ) {

		uHi = 0x1FFFFFFF;
		}
	
	if( uID == CP_SPNs ) {

		uHi = 64;
		}

	if( (uID >= CP_SPN1) && (uID <= CP_SPN8) && (uID % 2 == 0) ) {

		uHi = 32;

		fSUBs = TRUE;
		}

	if( uID == CP_PAGE ) {

		uHi = 8;

		uLo = 1;

		fPage = TRUE;
		}

	if( uHi > 0 ) {

 		if( uID != CP_NUM && !IsInteger(uID) ) {

			Error(CPrintf(CString(IDS_VALID_VALUES_ARE_2), uLo, uHi));

			GetDlgItem(uID).SetFocus();

			return;
			}

		if( uID == CP_NUM && !IsHex(uID) ) {

			Error(CPrintf(CString(IDS_VALID_VALUES_ARE_3), uLo, uHi));

			GetDlgItem(uID).SetFocus();

			return;
			}

		UINT uRadix = uID == CP_NUM ? 16 : 10;

		UINT uCheck = tstrtol(GetDlgItem(uID).GetWindowText(), NULL, uRadix);

		if( uCheck > uHi ) {

			CString Text = uID == CP_NUM ? "%8.8X" : "%u";

			GetDlgItem(uID).SetWindowText(CPrintf(Text, uHi));
			}

		if( uCheck < uLo ) {

			CString Text = uID == CP_NUM ? "%8.8X" : "%u";

			GetDlgItem(uID).SetWindowText(CPrintf(Text, uLo));
			}

		if( fSUBs ) {

			UINT uPage  = tstrtol(GetDlgItem(CP_PAGE).GetWindowText(), NULL, 10);

			if( uPage > 0 ) {

				uPage -= 1;
				}

			UINT uIndex = uPage * 8 + (uID - CP_SPN1) / 2;

			BYTE bSize  = BYTE(tstrtol(GetDlgItem(uID).GetWindowText(), NULL, 10));

			m_pSUBs[uIndex] = bSize;
			}
		
		if( fPage ) {
		
			SetSUBs(FALSE);
			}

		DoEnables();
		}
	}

// Command Handlers

BOOL CId29MessDialog::OnCreateEdit(UINT uID)
{
	CID29 * pID = CreateID();

	if( pID ) {
		
		MakeID(pID);

		LoadIDList();

		InitAll();
					
		return TRUE;
		}

	return FALSE;
	}

BOOL CId29MessDialog::OnRemove(UINT uID)
{
	RemoveID();

	LoadIDList();

	InitAll();
	
	return TRUE;
	}

BOOL CId29MessDialog::OnOkay(UINT uID)
{
	if( m_pOptions->m_pIDs ) {

		delete m_pOptions->m_pIDs;
		}

	m_pOptions->m_pIDs = (CID29List *) m_pOptions->MakeFromItem(m_pOptions, m_pIDs);

	if( m_fRebuild ) {

		m_pSystem->Rebuild(1);
		}

	if( m_pIDs ) {

		delete m_pIDs;
		}

	EndDialog(TRUE);

	return TRUE;
	}

// Message Handlers

BOOL CId29MessDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl)
{
	if( uID == IDCANCEL ) {

		if( m_pIDs ) {

			delete m_pIDs;
			}			
		}
	
	return CStdDialog::OnCommand(uID, uNotify, Ctrl);
	}

// Implementation

void CId29MessDialog::LoadIDList(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	Box.ResetContent();

	int nTab[] = { 40, 110 };

	Box.SetTabStops(elements(nTab), nTab);

	CID29List * pList = m_pIDs;

	Box.AddString(CString(IDS_FORMAT_2), NOTHING);

	for( UINT u = 0; u < pList->GetItemCount(); u++ ) {

		CID29 * pID = pList->GetItem(u);

		if( pID ) {

			CString ID;

			ID.Printf("%8.8X\t", pID->m_Number);

			ID += GetTitle(pID);

			Box.AddString(ID, u);

			continue;
			}

		break;
		}

	Box.SetCurSel(0);		
	}

void CId29MessDialog::InitAll(void)
{
	GetDlgItem(CP_NUM).SetWindowText("");

	GetDlgItem(CP_TITLE).SetWindowText("");

	GetDlgItem(CP_PRE).SetWindowText("");

	GetDlgItem(CP_SPNs).SetWindowText("1");

	GetDlgItem(CP_PAGE).SetWindowText("1");

	SetEnhanced(0);

	SetSUBs(TRUE);

	GetDlgItem(CP_SPN1).SetWindowText("0");

	GetDlgItem(CP_SPN2).SetWindowText("0");

	GetDlgItem(CP_SPN3).SetWindowText("0");

	GetDlgItem(CP_SPN4).SetWindowText("0");

	GetDlgItem(CP_SPN5).SetWindowText("0");

	GetDlgItem(CP_SPN6).SetWindowText("0");

	GetDlgItem(CP_SPN7).SetWindowText("0");

	GetDlgItem(CP_SPN8).SetWindowText("0");

	DoEnables();
	}

void CId29MessDialog::SetSUBs(BOOL fInit)
{	
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	CID29 * pID   = NULL;

	UINT uIndex   = Box.GetCurSelData();

	if( uIndex < NOTHING ) {
		
		pID = m_pIDs->GetItem(uIndex);
		}
	
	UINT uPage = tstrtol(GetDlgItem(CP_PAGE).GetWindowText(), NULL, 10) - 1;

	if( uPage == NOTHING ) {

		uPage = 0;
		}

	CString Text;

	for( UINT a = 3001, b = 0; a < 3017; a+=2, b++ ) {

		CString String = GetDlgItem(a).GetWindowText();

		UINT uLen = String.Find(' ');

		if( uLen < NOTHING ) {

			String = String.Mid(0, uLen);
		
			Text = CPrintf(L" %2.2u:", uPage * 8 + b + 1 );

			Text = String + Text;
			}

		GetDlgItem(a).SetWindowText(Text);
		}

	if( fInit ) {

		memset(m_pSUBs, 0, sizeof(m_pSUBs));

		if( pID ) {

			UINT uSUBs = pID->m_pSUBs->GetItemCount();

			for( UINT u = 0; u < uSUBs; u++ ) {

				m_pSUBs[u] = BYTE(pID->GetSUBs()->GetItem(u)->m_Size);
				}
			}
		}

	for( UINT x = CP_SPN1, y = 0; x <= CP_SPN8; x+=2, y++ ) {

		Text.Printf("%u", m_pSUBs[y + uPage * 8] & 0x7F);

		if( pID ) {

			CSUB * pSUB = pID->GetSUBs()->GetItem(y + uPage * 8);

			if( !pSUB ) {

				Text = "0";
				}
			}
		
		GetDlgItem(x).SetWindowText(Text);
		}  
	 }

void CId29MessDialog::SetEnhanced(UINT uSel)
{
	CComboBox &Enh = (CComboBox &) GetDlgItem(CP_ENH);

	if( Enh.GetCount() == 0 ) {

		Enh.AddString(CString(IDS_DRIVER_NONE));

		Enh.AddString("Rapid Fire Message");
		}

	Enh.SetCurSel(uSel);
	}

CID29 * CId29MessDialog::CreateID(void)
{
	CError Error;

	CString Text = GetDlgItem(CP_NUM).GetWindowText();

	if( !IsIdValid(Error, Text) ) {

		Error.Show(ThisObject);

		return NULL;
		}

	UINT uID    = tstrtol(Text, NULL, 16);

	if( !IsIdValid(Error, uID) ) {

		Error.Show(ThisObject);
				
		return NULL;
		}

	CID29 * pID = NewID();

	if( pID ) {

		pID->m_pSUBs->SetDatabase(m_pOptions->GetDatabase());

		if( !SetID(Error, pID, uID) ) {

			delete pID;
	
			Error.Show(ThisObject);

			return NULL;
			}
		}

	return pID;
	}

BOOL CId29MessDialog::MakeID(CID29 * pIdent)
{	
	m_fChange = TRUE;

	if( IsEdit() ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		UINT uIndex   = Box.GetCurSelData();

		if( uIndex < NOTHING ) {

			CID29 * pID   = m_pIDs->GetItem(uIndex);
		
			if( pID ) {

				pIdent->m_Addr.m_Ref = pID->m_Addr.m_Ref;

				pIdent->m_Active     = pID->m_Active;

				m_pIDs->InsertItem(pIdent, pID);

				m_pIDs->RemoveItem(pID);

				delete pID;
				}
			}

		return FALSE;
		} 

	m_pIDs->AppendItem(pIdent); 

	return TRUE;
	}

BOOL CId29MessDialog::RemoveID(void)
{
	if( IsEdit() ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		UINT uIndex   = Box.GetCurSelData();

		if( uIndex < NOTHING ) {

			CID29 * pID   = m_pIDs->GetItem(uIndex);
		
			if( pID ) {
		
				if( pID->m_Active ) {

					CString Text = CString(IDS_YOU_ARE_ABOUT_TO_3);

					if( YesNo(Text) == IDNO ) {

						return FALSE;
						}

					m_fRebuild = TRUE;
					}

				m_fChange = TRUE;

				m_pIDs->RemoveItem(pID);

				delete pID;

				return TRUE;
				}
			}
		} 

	return FALSE;
	}

// Overridables

CID29 * CId29MessDialog::NewID(void)
{
	return New CID29;
	}

BOOL CId29MessDialog::SetID(CError &Error, CID29 * pID, UINT uID)
{
	if( pID ) {

		CString Title   = GetDlgItem(CP_TITLE).GetWindowText();

		if( !IsTitleValid(Error, Title) ) {

			return FALSE;
			}

		UINT uSUBs      = tstrtol(GetDlgItem(CP_SPNs).GetWindowText(), NULL, 10);

		pID->m_Number   = uID;

		pID->m_Title    = Title;

		CComboBox &Enh  = (CComboBox &) GetDlgItem(CP_ENH);

		pID->m_Enhanced = BYTE(Enh.GetCurSel());

		pID->GrowSUBList(uSUBs);

		UINT uSize = 0;

		for( UINT u = 0; u < uSUBs; u++ ) {

			uSize += m_pSUBs[u];

			if( !IsSubElementValid(Error, uSize) ) {

				return FALSE;
				}

			pID->GetSUBs()->GetItem(u)->m_Size = m_pSUBs[u];
			}

		return TRUE;
		}

	return FALSE;
	}

void CId29MessDialog::SetID(CID29 * pID)
{
	if( pID ) {

		GetDlgItem(CP_NUM).SetWindowText(CPrintf("%8.8X", pID->m_Number));

		GetDlgItem(CP_TITLE).SetWindowText(GetTitle(pID));

		GetDlgItem(CP_SPNs).SetWindowText(CPrintf("%u", pID->GetSUBs()->GetItemCount()));
		}
	}

void CId29MessDialog::DoEnables(void)
{
	BOOL fEnable = !GetDlgItem(CP_NUM  ).GetWindowText().IsEmpty() && 
	               !GetDlgItem(CP_TITLE).GetWindowText().IsEmpty();

	UINT uSUBs = tstrtol(GetDlgItem(CP_SPNs).GetWindowText(), NULL, 10);

	UINT uPage = tstrtol(GetDlgItem(CP_PAGE).GetWindowText(), NULL, 10) - 1;

	GetDlgItem(CP_SPNs).EnableWindow(fEnable);

	UINT uSize = 8;

	for( UINT x = CP_SPN1, y = 1; x <= CP_SPN8; x+=2, y++ ) {

		GetDlgItem(x).EnableWindow(fEnable ? uSUBs >= y + uSize * uPage : FALSE);
		} 

	GetDlgItem(CP_PAGE).EnableWindow(fEnable ? uSUBs > uSize : FALSE);

	CListBox &List = (CListBox &) GetDlgItem(1001);

	GetDlgItem(CP_ADD).EnableWindow(fEnable);

	GetDlgItem(CP_DEL).EnableWindow(List.GetCurSel());

	GetDlgItem(IDOK).EnableWindow(m_fChange);

	GetDlgItem(CP_ENH).EnableWindow(fEnable);
	}

// Helpers

BOOL CId29MessDialog::IsInteger(UINT uID)
{
	CString Text = GetDlgItem(uID).GetWindowText();

	for( UINT u = 0; u < Text.GetLength(); u++ ) {

		TCHAR Char = Text.GetAt(u);

		if( Char < '0' || Char > '9' ) {

			return FALSE;
			}
		}
	
	return TRUE;
	}

BOOL CId29MessDialog::IsHex(UINT uID)
{
	CString Text = GetDlgItem(uID).GetWindowText();

	Text = Text.ToUpper();

	for( UINT u = 0; u < Text.GetLength(); u++ ) {

		TCHAR Char = Text.GetAt(u);

		if( ( Char >= '0' && Char <= '9' ) || (Char >= 'A' && Char <= 'F') ) {

			continue;
			}

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CId29MessDialog::IsEdit(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	return Box.GetCurSelData() < NOTHING;
	}

CString CId29MessDialog::GetTitle(CID29 * pID)
{
	CString Title = pID->m_Title;

	UINT uFind    = Title.Find('\t');

	while( uFind < NOTHING ) {

		Title.Remove('\t');

		uFind = Title.Find('\t');
		}

	return Title;
	}

CID29 * CId29MessDialog::FindID(CString Text)
{
	UINT uCount = m_pIDs->GetItemCount();

	for( UINT u = 0; u < uCount; u++ ) {

		if( Text == m_pIDs->GetItem(u)->m_Title ) {

			return m_pIDs->GetItem(u);
			}
		}
	
	return NULL;
	}

BOOL CId29MessDialog::IsIdValid(CError &Error, UINT uID)
{
	if( uID > 0x1FFFFFFF ) {

		Error.Set(CPrintf(CString(IDS_IDENTIFIER_FMT_IS), GetDlgItem(CP_NUM).GetWindowText()));
	
		return FALSE;
		}

	return TRUE;
	}

BOOL CId29MessDialog::IsIdValid(CError &Error, CString ID)
{
	if( ID.IsEmpty() ) {

		Error.Set(CPrintf(CString(IDS_PLEASE_ENTER)));
	
		return FALSE;
		}

	return TRUE;
	}

BOOL CId29MessDialog::IsSubElementValid(CError &Error, UINT uSize)
{
	if( uSize > 64 ) {

		Error.Set(CPrintf(CString(IDS_SUBELEMENT_SIZES)));

		return FALSE;
		}

	return TRUE;
	}

BOOL CId29MessDialog::IsTitleValid(CError &Error, CString Title)
{
	CID29 * pID   = FindID(Title);

	CListBox &Box = (CListBox &) GetDlgItem(1001);

	UINT uIndex   = Box.GetCurSelData();

	if( Title.IsEmpty() || ( pID && pID != m_pIDs->GetItem(uIndex) ) ) {

		Error.Set(CPrintf(CString(IDS_UNIQUE)));

		return FALSE;
		}

	return TRUE;
	}

// End of file
