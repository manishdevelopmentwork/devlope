
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientDigestAuth_HPP

#define	INCLUDE_HttpClientDigestAuth_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpClientAuth.hpp"

#include "HttpClientDigest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Authentication Method
//

class DLLAPI CHttpClientDigestAuth : public CHttpClientAuth
{
	public:
		// Constructor
		CHttpClientDigestAuth(void);

		// Operations
		BOOL	CanAccept(CString Meth, UINT &uPriority);
		BOOL    ProcessRequest(CString Line, BOOL fFail);
		BOOL    ProcessInfo(CString Line, CString Path, PCBYTE pBody, UINT uBody);
		CString GetAuthHeader(CString Verb, CString Path, CBytes Body);

	protected:
		// Data Members
		CHttpClientDigest m_d;
	};

// End of File

#endif
