
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DeviceIo_HPP

#define INCLUDE_DeviceIo_HPP

//////////////////////////////////////////////////////////////////////////
//
// Device I/O
//

class CDeviceIo : public IDeviceIo
{
public:
	// Constructor
	CDeviceIo(void);

	// Destructor
	~CDeviceIo(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IDeviceIo
	bool METHOD GetDigitalIn(void);
	UINT METHOD GetAnalogIn(void);
	UINT METHOD GetPsuVoltage(void);
	void METHOD SetDigitalOut(bool fState);
	bool METHOD GetDigitalOut(void);

protected:
	// Data Members
	ULONG    m_uRefs;
	IMutex * m_pLock;

	// Implemenation
	bool    Write(PCSTR pFile, PCSTR pData);
	CString Read(PCSTR pFile);
	UINT    Scale(UINT counts);
};

// End of File

#endif
