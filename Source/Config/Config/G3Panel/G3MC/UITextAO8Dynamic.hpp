
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UITextAO8Dynamic_HPP

#define INCLUDE_UITextAO8Dynamic_HPP

#include "DAAO8OutputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Analog Output Module Dynamic Value
//

class CUITextAO8Dynamic : public CUITextInteger
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CUITextAO8Dynamic(void);

	// Destructor
	~CUITextAO8Dynamic(void);

protected:
	// Data Members
	CDAAO8OutputConfig * m_pConfig;
	CString	             m_Type;

	// Core Overidables
	void OnBind(void);

	// Implementation
	void GetConfig(void);
	void FindPlaces(void);
	void FindUnits(void);
	void FindRanges(void);
	void SetMinMax(UINT OutType);
	void CheckFlags(void);

	// Friends
	friend class CUIAO8Dynamic;
};

// End of File

#endif
