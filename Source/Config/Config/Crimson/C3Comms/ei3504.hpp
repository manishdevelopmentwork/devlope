
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EUROINVENSYS3504_HPP
	
#define	INCLUDE_EUROINVENSYS3504_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CEuroInvensys3504Driver;
class CEuroInvensys3504SerialDriver;
class CEuroInvensys3504TCPDeviceOptions;
class CEuroInvensys3504TCPDriver;
class CEuroInvensys3504AddrDialog;

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	WL	addrWordAsLong
#define	WR	addrWordAsReal
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Register Table Spaces
#define	SP_0	 1	// Modbus Digital Outputs  00000
#define	SP_1	 2	// Modbus Digital Inputs   10000
#define	SP_3	 3	// Modbus Read-Only Words  30000
#define	SP_4	 4	// Modbus Read/Write Words 40000
// The following are Modbus Holding Registers
#define	SP_COMT	99	// Indirect Register Access
#define	SP_ACC	10	// Access
#define	SP_ALA	11	// Alarm.1 - Alarm.4
#define	SP_ALB	12	// Alarm.5 - Alarm.8
#define	SP_ALS	13	// Alarm Summary
#define	SP_BCD	14	// BCDInput
#define	SP_COM	15	// Comms
#define	SP_DAL	16	// DigAlarm
#define	SP_HUM	17	// Humidity
#define	SP_INS	18	// Instrument
#define	SP_IPM	19	// IPMonitor
#define	SP_LG2	20	// Lgc2
#define	SP_LG8	21	// Lgc8
#define	SP_LGI	22	// LgcIO
#define	SP_LIN	23	// Lin16
#define	SP_LDG	24	// Loop Diag
#define	SP_LMN	25	// Loop Main
#define	SP_LOP	26	// Loop OP
#define	SP_PID	27	// Loop PID
#define	SP_SET	28	// Loop Setup
#define	SP_SP	29	// Loop SP
#define	SP_TUN	30	// Loop Tune
#define	SP_MAT	31	// Math2
#define	SP_MOD	32	// Mod
#define	SP_MID	33	// ModIDs
#define	SP_MUL	34	// MultiOper
#define	SP_PGM	35	// Programmer
#define	SP_PV	36	// PV
#define	SP_REC	37	// Recipe
#define	SP_RLY	38	// RelayAA
#define	SP_SWO	39	// SwitchOver
#define	SP_TIM	40	// Timer
#define	SP_TXD	41	// Txdr
#define	SP_USE	42	// UsrVal
#define	SP_ZIR	43	// Zirconia

// Added May 2011
#define	SP_PGS	48	// Prgr Segments
// End Space definitions

#define	PGCNT	 2	// 8 Programmers

// Programmer size
#define	PGMCNT	64	// 64 data items per Programmer General Data
#define	XPGM	((PGCNT * PGMCNT) - 1)

// Prgr sizes
#define	SEGCNT	50	// 16 segments are allocated per Programmer
#define	SEGMAX	32	// 32 modbus addresses are allocated per segment
#define	PRGMAX	1600	// 1600 modbus addresses are allocated per programmer
#define	SEGSZ	(SEGCNT * SEGMAX)	// current number of selections per programmer
#define	XPGS	(((PGCNT - 1) * PRGMAX) + SEGSZ - 1)	// maximum number for selections

// Dialog box numbers
#define	DPADD	2002
#define	DNAME	2021
#define	DLOOP	2030
#define	DNUMB	2031
#define	DMODB	2033
#define	DSEGT	2034
#define	DSEGM	2035

struct EIESTRINGS
{
	UINT	uQty;		// Number of strings entered for selection
	UINT	uBlkCt;		// Number of blocks for selection
	UINT	uBlock;		// Selected block
	UINT	uBPos;		// Position of Block # in List
	UINT	uAddr;		// ID
	UINT	uNPos;		// Position of Name in List
	UINT	uSPos;		// Position of Segment in List
	UINT	uTable;		// Table number selected
	UINT	uOffset;	// Addr.a.m_Offset value
	UINT	uPrgr;		// Programmer Selection
	UINT	uSegm;		// Segment for Programmer
	BOOL	fIsPrgr;	// Prgr - has 2 numeric entries
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm/Invensys 3504 TCP/IP Master Driver Options
//

class CEuroInvensys3504TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEuroInvensys3504TCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Invensys EuroInvensys Comms Driver
//

class CEuroInvensys3504Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CEuroInvensys3504Driver(void);

		// Destructor
		~CEuroInvensys3504Driver(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		EIESTRINGS * GetEIEPtr(void);

		BOOL	IsNotStdModbus(UINT uTable);

		void	InitItemInfo   (UINT uTable);
		void	LoadItemInfo   (UINT uTable);
		CString	LoadItemStrings(UINT uTable, BOOL fStore, UINT uSelect);

		BOOL	HasBlocks(void);

		CString	Get_sName(void);
		void	Set_sName(CString sName);
		UINT	GetAddrFromName(void);

	protected:

		// Data
		EIESTRINGS	EIESTR;
		EIESTRINGS	*m_pEIE;

		CString m_sName;	// Selected string

		// Implementation
		void	AddSpaces(void);

		CString	ParseParamString(CString sParamString, BOOL fStore);
		CString	ParseAddr(CString sParamString, BOOL fStore);
		void	ParseModifiers(CString sParamString, UINT *pAdd, UINT *pInc);

		// Helpers
		UINT	Find2ndDotFwd(CString s);

		// Other Help
		void	InitEIE(void);

		// String Info Loading
 		void	LoadAccessInfo(void);
		void	LoadAlarmAInfo(void);
		void	LoadAlarmBInfo(void);
		void	LoadAlarmSummaryInfo(void);
		void	LoadBCDInfo(void);
		void	LoadCommsInfo(void);
		void	LoadDigAlarmInfo(void);
		void	LoadHumidityInfo(void);
		void	LoadInstrumentInfo(void);
		void	LoadIPMonitorInfo(void);
		void	LoadLgc2Info(void);
		void	LoadLgc8Info(void);
		void	LoadLgcIOInfo(void);
		void	LoadLinInfo(void);
		void	LoadLoopDiagInfo(void);
		void	LoadLoopMainInfo(void);
		void	LoadLoopOPInfo(void);
		void	LoadLoopPIDInfo(void);
		void	LoadLoopSetupInfo(void);
		void	LoadSPInfo(void);
		void	LoadTuneInfo(void);
		void	LoadMath2Info(void);
		void	LoadModInfo(void);
		void	LoadModIDInfo(void);
		void	LoadMultInfo(void);
		void	LoadProgrammerDInfo(void);
		void	LoadProgrammerSInfo(void);
		void	LoadPVInfo(void);
		void	LoadRecipeInfo(void);
		void	LoadRelayInfo(void);
		void	LoadSwitchoverInfo(void);
		void	LoadTimerInfo(void);
		void	LoadTxdrInfo(void);
		void	LoadUsrValInfo(void);
		void	LoadZirconiaInfo(void);

		// String Loading
 		CString	LoadAccess(UINT uSel);
		CString	LoadAlarmA(UINT uSel);
		CString	LoadAlarmB(UINT uSel);
		CString	LoadAlarmSummary(UINT uSel);
		CString	LoadBCD(UINT uSel);
		CString	LoadComms(UINT uSel);
		CString	LoadDigAlarm(UINT uSel);
		CString	LoadHumidity(UINT uSel);
		CString	LoadInstrument(UINT uSel);
		CString	LoadIPMonitor(UINT uSel);
		CString	LoadLgc2(UINT uSel);
		CString	LoadLgc8(UINT uSel);
		CString	LoadLgcIO(UINT uSel);
		CString	LoadLin(UINT uSel);
		CString	LoadLoopDiag(UINT uSel);
		CString	LoadLoopMain(UINT uSel);
		CString	LoadLoopOP(UINT uSel);
		CString	LoadLoopPID(UINT uSel);
		CString	LoadLoopSetup(UINT uSel);
		CString	LoadLoopSP(UINT uSel);
		CString	LoadLoopTune(UINT uSel);
		CString	LoadMath2(UINT uSel);
		CString	LoadMod(UINT uSel);
		CString	LoadModID(UINT uSel);
		CString	LoadMult(UINT uSel);
		CString	LoadProgrammerData(UINT uSel);
		CString	LoadProgrammerSegs(UINT uSel);
		CString	LoadPV(UINT uSel);
		CString	LoadRecipe(UINT uSel);
		CString	LoadRelay(UINT uSel);
		CString	LoadSwitchover(UINT uSel);
		CString	LoadTimer(UINT uSel);
		CString	LoadTxdr(UINT uSel);
		CString	LoadUsrVal(UINT uSel);
		CString	LoadZirconia(UINT uSel);
	};

class CEuroInvensys3504SerialDriver : public CEuroInvensys3504Driver
{
	public:
		// Constructor
		CEuroInvensys3504SerialDriver(void);

		// Destructor
		~CEuroInvensys3504SerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);
	};

class CEuroInvensys3504TCPDriver : public CEuroInvensys3504Driver
{
	public:
		// Constructor
		CEuroInvensys3504TCPDriver(void);

		// Destructor
		~CEuroInvensys3504TCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Invensys EuroInvensys Selection Dialog
//

class CEuroInvensys3504AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEuroInvensys3504AddrDialog(CEuroInvensys3504Driver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

		// Destructor
		~CEuroInvensys3504AddrDialog(void);
		                
	protected:
		CEuroInvensys3504Driver * m_pGDrv;

		EIESTRINGS	*m_pEIE;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk     (UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		void	OnComboChange(UINT uID, CWnd &Wnd);
		void	OnTypeChange (UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overrides
		BOOL	AllowType(UINT uType);
		void	ShowDetails(void);

		// Selection Handling
		void	InitItemInfo(UINT uTable, BOOL fNew);
		void	LoadNames(void);
		void	SetItemData(void);
		void	SetBoxPosition(UINT uID, UINT uPos);
		UINT	GetBoxPosition(UINT uID);
		void	SetItemAddr (void);
		void	SetNameInfo(UINT uTable);
		void	SetBlockNumbers(void);
		void	SetSegmNumbers(void);
		void	GetParamData(UINT uPos);
		UINT	ChkALBBlkInit(void);
		UINT	ChkALBBlkUp(UINT uBlk);
		UINT	ChkALBBlkDown(void);
		UINT	GetAddrFromText(void);

		// Helpers
		void	InitSelects(void);
		void	ClearSelData(void);
		void	ClearBox(CComboBox & ccb);
		void	ClearBox(UINT uID);
		void	Show3504Details(void);
		CString	StripAddr(CString sSelect);
	};

// String Format:
// {
//	Parameter Name
//	(Number of bits in parameter)
//	if( String 0 ) {
//		number of blocks-
//		if( number of blocks > 1 ) {
//			[block 0 starting address]
//			=increment between blocks
//			}
//		else {
//			[block address]
//	else {
//		offset from start of block
//		}
//	}

#define	SNOTU	L"Not Defined"
#define	SNONE	L""

// Format
// A - Section Name
// B - Block Number
// C - Parameter Name
// D - ID
// E - Increment to ID in next block
// F - ID's in subsequent blocks

// Access Strings (10)
// Format A.C_D
// Simplify initialization
#define	SACC1N	L"Access.ConfPasscode"
#define	SACC1A	515
#define	SACC1	L"Access.ConfPasscode@515"
#define	SACC2	L"Access.CustomerID@629"
#define	SACC3	L"Access.Goto@147"
#define	SACC4	L"Access.IM@199"
#define	SACC5	L"Access.Keylock@279"
#define	SACC6	L"Access.L2Passcode@514"
#define	SACC7	L"Access.L3Passcode@554"

// Alarm Strings A 1-4 (11) 
// Format A.B.C@D#E + special case - A.B.C.D(<ID2,ID3|ID4>)
#define	SALM1	L"Alarm.1.Ack@10250#16"
#define	SALM2	L"Alarm.1.Block_1@544#1"
#define	SALM3	L"Alarm.1.Block_2@10246#16"
#define	SALM4	L"Alarm.1.Delay_1@221#1"
#define	SALM5	L"Alarm.1.Delay_2@10248#16"
#define	SALM6	L"Alarm.1.Hysteresis_1@47(68,69|71)"
#define	SALM7	L"Alarm.1.Hysteresis_2@10242#16"
#define	SALM8	L"Alarm.1.Inhibit@10247#16"
#define	SALM9	L"Alarm.1.Latch_1@540#1"
#define	SALM10	L"Alarm.1.Latch_2@10244#16"
#define	SALM11	L"Alarm.1.Out_1@294#1"
#define	SALM12	L"Alarm.1.Out_29@10249#16"
#define	SALM13	L"Alarm.1.Priority@10245#16"
#define	SALM14	L"Alarm.1.Reference@10243#16"
#define	SALM15	L"Alarm.1.Threshold_1@13(14,81|82)"
#define	SALM16	L"Alarm.1.Threshold_2@10241#16"
#define	SALM17	L"Alarm.1.Type_1@536#1"
#define	SALM18	L"Alarm.1.Type_2@10240#16"

// Alarm Strings B 5-8 (12)
// Format A.B.C@D#E
#define	SALM19	L"Alarm.5.Ack@10314#16"
#define	SALM20	L"Alarm.5.Block@10310#16"
#define	SALM21	L"Alarm.5.Delay@10312#16"
#define	SALM22	L"Alarm.5.Hysteresis@10306#16"
#define	SALM23	L"Alarm.5.Inhibit@10311#16"
#define	SALM24	L"Alarm.5.Latch@10308#16"
#define	SALM25	L"Alarm.5.Out@10313#16"
#define	SALM26	L"Alarm.5.Priority@10309#16"
#define	SALM27	L"Alarm.5.Reference@10307#16"
#define	SALM28	L"Alarm.5.Threshold@10305#16"
#define	SALM29	L"Alarm.5.Type@10304#16"

// Alarm Summary (13)
// Format A.C@D
#define	SALS1	L"AlmSummary.AnAlarmByte@10176"
#define	SALS2	L"AlmSummary.AnyAlarm_261@261"
#define	SALS3	L"AlmSummary.AnyAlarm_10213@10213"
#define	SALS4	L"AlmSummary.DigAlarmByte@10188"
#define	SALS5	L"AlmSummary.GlobalAck_274@274"
#define	SALS6	L"AlmSummary.GlobalAck_10214@10214"
#define	SALS7	L"AlmSummary.NewAlarm_260@260"
#define	SALS8	L"AlmSummary.NewAlarm_10212@10212"
#define	SALS9	L"AlmSummary.SBrkAlarm@10200"

// BCD Strings
// Format A.C@D (14)
#define	SBCD1	L"BCDInput.1.BCDVal@96"
#define	SBCD2	L"BCDInput.2.BCDVal@105"

// Comms Strings (15)
// Format A.C@D
#define	SCOM1	L"Comms.Address@131"
#define	SCOM2	L"Comms.ProgNum@8192"
#define	SCOM3	L"Comms.Wait@523"

// DigAlarm 1-8 (16)
// Format A.B.C@D#E
#define	SDAL1	L"DigAlarm.1.Ack@11274#16"
#define	SDAL2	L"DigAlarm.1.Block@11270#16"
#define	SDAL3	L"DigAlarm.1.Delay@11272#16"
#define	SDAL4	L"DigAlarm.1.Inhibit@11271#16"
#define	SDAL5	L"DigAlarm.1.Latch@11268#16"
#define	SDAL6	L"DigAlarm.1.Out@11273#16"
#define	SDAL7	L"DigAlarm.1.Priority@11269#16"
#define	SDAL8	L"DigAlarm.1.Type@11264#16"


// Humidity Strings (17)
// Format A.C@D
#define	SHUM1	L"Humidity.DewPoint@13317"
#define	SHUM2	L"Humidity.DryTemp@13318"
#define	SHUM3	L"Humidity.Pressure@13313"
#define	SHUM4	L"Humidity.PsychroConst@13315"
#define	SHUM5	L"Humidity.RelHumid@13316"
#define	SHUM6	L"Humidity.Resolution@13320"
#define	SHUM7	L"Humidity.SBrk@13314"
#define	SHUM8	L"Humidity.WetOffset@13312"
#define	SHUM9	L"Humidity.WetTemp@13319"

// Instrument Strings (18)
// Format A.C@D
#define	SINS1	L"Instrument.Diagnostics.ErrCount@73"
#define	SINS2	L"Instrument.Diagnostics.MaxConTicks@201"
#define	SINS3	L"Instrument.Display.HomePage@106"
#define	SINS4	L"Instrument.Display.Units@516"
#define	SINS5	L"Instrument.InstInfo.CompanyID@121"
#define	SINS6	L"Instrument.InstInfo.InstType@122"
#define	SINS7	L"Instrument.InstInfo.Version@107"

// IP Monitor Strings (19)
// Format A.C@D
#define	SIP1	L"IPMonitor.1.Max_133@133"
#define	SIP2	L"IPMonitor.1.Max_4915@4915"
#define	SIP3	L"IPMonitor.1.Min_134@134"
#define	SIP4	L"IPMonitor.1.Min_4916@4916"
#define	SIP5	L"IPMonitor.1.Reset_140@140"
#define	SIP6	L"IPMonitor.1.Reset_4919@4919"
#define	SIP7	L"IPMonitor.1.Threshold_138@138"
#define	SIP8	L"IPMonitor.1.Threshold_4917@4917"
#define	SIP9	L"IPMonitor.1.TimeAbove_139@139"
#define	SIP10	L"IPMonitor.1.TimeAbove_4918@4918"
#define	SIP11	L"IPMonitor.2.Max@4920"
#define	SIP12	L"IPMonitor.2.Min@4921"
#define	SIP13	L"IPMonitor.2.Reset@4924"
#define	SIP14	L"IPMonitor.2.Threshold@4922"
#define	SIP15	L"IPMonitor.2.TimeAbove@4923"

// LGC2 Strings 1-24 (20)
// Format A.B.C@D#E
#define	S2LG1	L"Lgc2.1.In1@4822#3"
#define	S2LG2	L"Lgc2.1.In2@4823#3"
#define	S2LG3	L"Lgc2.1.Out@4824#3"

// LGC8 Strings 1-2 (21)
// Format A.B.C@D#E
#define	S8LG1	L"Lgc8.1.In1@4894#9"
#define	S8LG2	L"Lgc8.1.In2@4895#9"
#define	S8LG3	L"Lgc8.1.In3@4896#9"
#define	S8LG4	L"Lgc8.1.In4@4897#9"
#define	S8LG5	L"Lgc8.1.In5@4898#9"
#define	S8LG6	L"Lgc8.1.In6@4899#9"
#define	S8LG7	L"Lgc8.1.In7@4900#9"
#define	S8LG8	L"Lgc8.1.In8@4901#9"
#define	S8LG9	L"Lgc8.1.Out@4902#9"

// LgcIO Strings (22)
// Format A.C@D
#define	SLGI1	L"LgcIO.LA.Backlash@124"
#define	SLGI2	L"LgcIO.LA.Inertia@123"
#define	SLGI3	L"LgcIO.LA.MinOnTime_45@45"
#define	SLGI4	L"LgcIO.LA.MinOnTime_54@54"
#define	SLGI5	L"LgcIO.LA.PV@361"
#define	SLGI6	L"LgcIO.LB.MinOnTime@89"
#define	SLGI7	L"LgcIO.LB.PV@362"

// Lin16 Strings (23)
// Format A.C@D
#define	SLIN1	L"Lin16.In@618"
#define	SLIN2	L"Lin16.In1@602"
#define	SLIN3	L"Lin16.In2@603"
#define	SLIN4	L"Lin16.In3@604"
#define	SLIN5	L"Lin16.In4@605"
#define	SLIN6	L"Lin16.In5@606"
#define	SLIN7	L"Lin16.In6@607"
#define	SLIN8	L"Lin16.In7@608"
#define	SLIN9	L"Lin16.In8@609"
#define	SLIN10	L"Lin16.In9@610"
#define	SLIN11	L"Lin16.In10@611"
#define	SLIN12	L"Lin16.In11@612"
#define	SLIN13	L"Lin16.In12@613"
#define	SLIN14	L"Lin16.In13@614"
#define	SLIN15	L"Lin16.In14@615"
#define	SLIN16	L"Lin16.InHighLimit@616"
#define	SLIN17	L"Lin16.InLowLimit@601"
#define	SLIN18	L"Lin16.Out@619"
#define	SLIN19	L"Lin16.Out1@622"
#define	SLIN20	L"Lin16.Out2@623"
#define	SLIN21	L"Lin16.Out3@624"
#define	SLIN22	L"Lin16.Out4@625"
#define	SLIN23	L"Lin16.Out5@626"
#define	SLIN24	L"Lin16.Out6@627"
#define	SLIN25	L"Lin16.Out7@628"
#define	SLIN26	L"Lin16.Out8@630"
#define	SLIN27	L"Lin16.Out9@631"
#define	SLIN28	L"Lin16.Out10@632"
#define	SLIN29	L"Lin16.Out11@633"
#define	SLIN30	L"Lin16.Out12@634"
#define	SLIN31	L"Lin16.Out13@635"
#define	SLIN32	L"Lin16.Out14@636"
#define	SLIN33	L"Lin16.OutHighLimit@637"
#define	SLIN34	L"Lin16.OutLowLimit@621"

// LOOP Diag 1-2 (24)
// Format A.B.C@D#E
#define	SLDG1	L"Loop.1.Diag.DerivativeOutContrib@116#1024"
#define	SLDG2	L"Loop.1.Diag.Error@39#1024"
#define	SLDG3	L"Loop.1.Diag.IntegralOutContrib@55#1024"
#define	SLDG4	L"Loop.1.Diag.LoopBreakAlarm@263#1024"
#define	SLDG5	L"Loop.1.Diag.PropOutContrib@214#1024"
#define	SLDG6	L"Loop.1.Diag.SBrk@258#1024"

// LOOP Main 1-2 (25)
// Format A.B.C@D#E
#define	SLMN1	L"Loop.1.Main.ActiveOut@4#1024"
#define	SLMN2	L"Loop.1.Main.AutoMan@273#1024"
#define	SLMN3	L"Loop.1.Main.Inhibit@268#1024"
#define	SLMN4	L"Loop.1.Main.PV_1@1#1024"
#define	SLMN5	L"Loop.1.Main.PV_2@289#1024"
#define	SLMN6	L"Loop.1.Main.TargetSP@2#1024"
#define	SLMN7	L"Loop.1.Main.WorkingSP@5#1024"

// LOOP OP 1-2 (26)
// Format A.B.C@D#E
#define	SLOP1	L"Loop.1.OP.Ch1OnOffHysteresis@86#1024"
#define	SLOP2	L"Loop.1.OP.Ch1Out@85#1024"
#define	SLOP3	L"Loop.1.OP.Ch1PotBreak@350#1024"
#define	SLOP4	L"Loop.1.OP.Ch1PotPosition_1@53#1024"
#define	SLOP5	L"Loop.1.OP.Ch1PotPosition_2@317#1024"
#define	SLOP6	L"Loop.1.OP.Ch1TravelTime@21#1024"
#define	SLOP7	L"Loop.1.OP.Ch2Deadband@16#1024"
#define	SLOP8	L"Loop.1.OP.Ch2OnOffHysteresis@88#1024"
#define	SLOP9	L"Loop.1.OP.Ch2Out@126#1024"
#define	SLOP10	L"Loop.1.OP.Ch2PotPosition@318#1024"
#define	SLOP11	L"Loop.1.OP.Ch2TravelTime@319#1024"
#define	SLOP12	L"Loop.1.OP.CoolType@524#1024"
#define	SLOP13	L"Loop.1.OP.EnablePowerFeedforward@565#1024"
#define	SLOP14	L"Loop.1.OP.FeedForwardGain@97#1024"
#define	SLOP15	L"Loop.1.OP.FeedForwardOffset@98#1024"
#define	SLOP16	L"Loop.1.OP.FeedForwardTrimLimit@99#1024"
#define	SLOP17	L"Loop.1.OP.FeedForwardType@532#1024"
#define	SLOP18	L"Loop.1.OP.FeedForwardVal@209#1024"
#define	SLOP19	L"Loop.1.OP.ManualMode@556#1024"
#define	SLOP20	L"Loop.1.OP.ManualOutVal_1@3#1024"
#define	SLOP21	L"Loop.1.OP.ManualOutVal_2@84#1024"
#define	SLOP22	L"Loop.1.OP.OutputHighLimit@30#1024"
#define	SLOP23	L"Loop.1.OP.OutputLowLimit@31#1024"
#define	SLOP24	L"Loop.1.OP.PotCalibrate_1@46#1024"
#define	SLOP25	L"Loop.1.OP.PotCalibrate_2@210#1024"
#define	SLOP26	L"Loop.1.OP.Rate@37#1024"
#define	SLOP27	L"Loop.1.OP.SafeOutVal@34#1024"
#define	SLOP28	L"Loop.1.OP.SensorBreakMode@553#1024"
#define	SLOP29	L"Loop.1.OP.TrackEnable@127#1024"
#define	SLOP30	L"Loop.1.OP.TrackOutVal@128#1024"

// LOOP PID 1-2 (27)
// Format A.B.C@D#E
#define	SLPD1	L"Loop.1.PID.ActiveSet_1@72#1024"
#define	SLPD2	L"Loop.1.PID.ActiveSet_2@185#1024"
#define	SLPD3	L"Loop.1.PID.Boundary1-2@153#1024"
#define	SLPD4	L"Loop.1.PID.Boundary2-3@152#1024"
#define	SLPD5	L"Loop.1.PID.CutbackHigh@18#1024"
#define	SLPD6	L"Loop.1.PID.CutbackHigh2@118#1024"
#define	SLPD7	L"Loop.1.PID.CutbackLow@17#1024"
#define	SLPD8	L"Loop.1.PID.CutbackLow2@117#1024"
#define	SLPD9	L"Loop.1.PID.DerivativeTime@9#1024"
#define	SLPD10	L"Loop.1.PID.DerivativeTime2@51#1024"
#define	SLPD11	L"Loop.1.PID.DerivativeTime3@183#1024"
#define	SLPD12	L"Loop.1.PID.IntegralTime@8#1024"
#define	SLPD13	L"Loop.1.PID.IntegralTime2@49#1024"
#define	SLPD14	L"Loop.1.PID.IntegralTime3@181#1024"
#define	SLPD15	L"Loop.1.PID.LoopBreakTime@83#1024"
#define	SLPD16	L"Loop.1.PID.ManualReset@28#1024"
#define	SLPD17	L"Loop.1.PID.ManualReset2@50#1024"
#define	SLPD18	L"Loop.1.PID.ManualReset3@182#1024"
#define	SLPD19	L"Loop.1.PID.ProportionalBand@6#1024"
#define	SLPD20	L"Loop.1.PID.ProportionalBand2@48#1024"
#define	SLPD21	L"Loop.1.PID.ProportionalBand3@180#1024"
#define	SLPD22	L"Loop.1.PID.RelCh2Gain@19#1024"
#define	SLPD23	L"Loop.1.PID.RelCh2Gain2@52#1024"
#define	SLPD24	L"Loop.1.PID.RelCh2Gain3@184#1024"

// LOOP Setup 1-2 (28)
// Format A.B.C@D#E
#define	SLSE1	L"Loop.1.Setup.CH1ControlType@512#1024"
#define	SLSE2	L"Loop.1.Setup.CH2ControlType@513#1024"
#define	SLSE3	L"Loop.1.Setup.ControlAction@7#1024"
#define	SLSE4	L"Loop.1.Setup.DerivativeType@550#1024"

// LOOP SP 1-2 (29)
// Format A.B.C@D#E
#define	SLSP1	L"Loop.1.SP.AltSP@485#1024"
#define	SLSP2	L"Loop.1.SP.AltSPSelect@276#1024"
#define	SLSP3	L"Loop.1.SP.ManualTrack@527#1024"
#define	SLSP4	L"Loop.1.SP.RangeHigh@12#1024"
#define	SLSP5	L"Loop.1.SP.RangeLow@11#1024"
#define	SLSP6	L"Loop.1.SP.Rate@35#1024"
#define	SLSP7	L"Loop.1.SP.RateDisable@78#1024"
#define	SLSP8	L"Loop.1.SP.RateDone@277#1024"
#define	SLSP9	L"Loop.1.SP.SP1@24#1024"
#define	SLSP10	L"Loop.1.SP.SP2@25#1024"
#define	SLSP11	L"Loop.1.SP.SPHighLimit_1@111#1024"
#define	SLSP12	L"Loop.1.SP.SPHighLimit_2@155#1024"
#define	SLSP13	L"Loop.1.SP.SPLowLimit_1@112#1024"
#define	SLSP14	L"Loop.1.SP.SPLowLimit_2@156#1024"
#define	SLSP15	L"Loop.1.SP.SPSelect@15#1024"
#define	SLSP16	L"Loop.1.SP.SPTrack_1@526#1024"
#define	SLSP17	L"Loop.1.SP.SPTrack_2@528#1024"
#define	SLSP18	L"Loop.1.SP.SPTrim_1@27#1024"
#define	SLSP19	L"Loop.1.SP.SPTrim_2@486#1024"
#define	SLSP20	L"Loop.1.SP.SPTrimHighLimit@66#1024"
#define	SLSP21	L"Loop.1.SP.SPTrimLowLimit@67#1024"

// LOOP Tune 1-2
// Format A.B.C@D#E  (30)
#define	SLTU1	L"Loop.1.Tune.AutotuneEnable@270#1024"
#define	SLTU2	L"Loop.1.Tune.Stage@269#1024"

// MATH2 Strings 1-24  (31)
// Format A.B.C@D#E
#define	SMAT1	L"Math2.1.In1@4750#3"
#define	SMAT2	L"Math2.1.In2@4751#3"
#define	SMAT3	L"Math2.1.Out@4752#3"

// Mod Strings  (32)
// Format A.C@D
#define	SMOD1	L"Mod.1.A.PV@364"
#define	SMOD2	L"Mod.1.B.PV@365"
#define	SMOD3	L"Mod.1.C.PV@366"
#define	SMOD4	L"Mod.2.A.PV@367"
#define	SMOD5	L"Mod.2.B.PV@368"
#define	SMOD6	L"Mod.2.C.PV@369"
#define	SMOD7	L"Mod.3.A.CJCTemp@216"
#define	SMOD8	L"Mod.3.A.Emissivity@104"
#define	SMOD9	L"Mod.3.A.FilterTimeConstant@103"
#define	SMOD10	L"Mod.3.A.MeasuredVal@208"
#define	SMOD11	L"Mod.3.A.Offset@142"
#define	SMOD12	L"Mod.3.A.PV_290@290"
#define	SMOD13	L"Mod.3.A.PV_370@370"
#define	SMOD14	L"Mod.3.B.PV@371"
#define	SMOD15	L"Mod.3.C.PV@372"
#define	SMOD16	L"Mod.4.A.PV@373"
#define	SMOD17	L"Mod.4.B.PV@374"
#define	SMOD18	L"Mod.4.C.PV@375"
#define	SMOD19	L"Mod.5.A.PV@376"
#define	SMOD20	L"Mod.5.B.PV@377"
#define	SMOD21	L"Mod.5.C.PV@378"
#define	SMOD22	L"Mod.6.A.PV@379"
#define	SMOD23	L"Mod.6.B.PV@380"
#define	SMOD24	L"Mod.6.C.PV@381"

// ModIDs  (33)
// Format A.C@D
#define	SMID1	L"ModIDs.Mod1Ident@12707"
#define	SMID2	L"ModIDs.Mod2Ident@12771"
#define	SMID3	L"ModIDs.Mod3Ident@12835"
#define	SMID4	L"ModIDs.Mod4Ident@12899"
#define	SMID5	L"ModIDs.Mod5Ident@12963"
#define	SMID6	L"ModIDs.Mod6Ident@13027"

// MultiOper Strings 1-2  (34)
// Format A.B.C@D#E
#define	SMUL1	L"MultiOper.1.AverageOut@5017#12"
#define	SMUL2	L"MultiOper.1.In1@5006#12"
#define	SMUL3	L"MultiOper.1.In2@5007#12"
#define	SMUL4	L"MultiOper.1.In3@5008#12"
#define	SMUL5	L"MultiOper.1.In4@5009#12"
#define	SMUL6	L"MultiOper.1.In5@5010#12"
#define	SMUL7	L"MultiOper.1.In6@5011#12"
#define	SMUL8	L"MultiOper.1.In7@5012#12"
#define	SMUL9	L"MultiOper.1.In8@5013#12"
#define	SMUL10	L"MultiOper.1.MaxOut@5015#12"
#define	SMUL11	L"MultiOper.1.MinOut@5016#12"
#define	SMUL12	L"MultiOper.1.SumOut@5014#12"

// Programmer Data Strings  1-2 (35)
// Format A.B.C@D#E
// added May 2011
#define	SPGM01	L"Programmer.1.CommsProgramNumber@5184#64"
#define	SPGM02	L"Programmer.1.ProgramHoldbackVal@5185#64"
#define	SPGM03	L"Programmer.1.ProgramRampUnits@5186#64"
#define	SPGM04	L"Programmer.1.ProgramDwellUnits@5187#64"
#define	SPGM05	L"Programmer.1.ProgramCycles@5188#64"
#define	SPGM06	L"Programmer.1.PowerFailAct@5189#64"
#define	SPGM07	L"Programmer.1.Servo@5190#64"
#define	SPGM08	L"Programmer.1.SyncMode@5191#64"
#define	SPGM09	L"Programmer.1.ResetEventOuts@5192#64"
#define	SPGM10	L"Programmer.1.CurProg@5193#64"
#define	SPGM11	L"Programmer.1.CurSeg@5194#64"
#define	SPGM12	L"Programmer.1.ProgStatus@5195#64"
#define	SPGM13	L"Programmer.1.PSP@5196#64"
#define	SPGM14	L"Programmer.1.CyclesLeft@5197#64"
#define	SPGM15	L"Programmer.1.CurSegType@5198#64"
#define	SPGM16	L"Programmer.1.SegTarget@5199#64"
#define	SPGM17	L"Programmer.1.SegRate@5200#64"
#define	SPGM18	L"Programmer.1.ProgTimeLeft@5201#64"
#define	SPGM19	L"Programmer.1.PVIn@5202#64"
#define	SPGM20	L"Programmer.1.SPIn@5203#64"
#define	SPGM21	L"Programmer.1.EventOuts@5204#64"
#define	SPGM22	L"Programmer.1.SegTimeLeft@5205#64"
#define	SPGM23	L"Programmer.1.EndOfSeg@5206#64"
#define	SPGM24	L"Programmer.1.SyncIn@5207#64"
#define	SPGM25	L"Programmer.1.FastRun@5208#64"
#define	SPGM26	L"Programmer.1.AdvSeg@5209#64"
#define	SPGM27	L"Programmer.1.SkipSeg@5210#64"
#define	SPGM28	L"Programmer.1.Ch2RampUnits@5211#64"
#define	SPGM29	L"Programmer.1.Ch2DwellUnits@5212#64"
#define	SPGM30	L"Programmer.1.PVStart@5213#64"
#define	SPGM31	L"Programmer.1.Ch2PVStart@5214#64"
#define	SPGM32	L"Programmer.1.Ch2HoldbackVal@5215#64"
#define	SPGM33	L"Programmer.1.Ch1HoldbackVal@5216#64"
#define	SPGM34	L"Programmer.1.Ch1RampUnits@5217#64"
#define	SPGM35	L"Programmer.1.PrgIn1@5218#64"
#define	SPGM36	L"Programmer.1.PrgIn2@5219#64"
#define	SPGM37	L"Programmer.1.PVWaitIP@5220#64"
#define	SPGM38	L"Programmer.1.ProgError@5221#64"
#define	SPGM39	L"Programmer.1.PVEventOP@5222#64"
#define	SPGM40	L"Programmer.1.GoBackCyclesLeft@5223#64"
#define	SPGM41	L"Programmer.1.DelayTime@5224#64"
#define	SPGM42	L"Programmer.1.ProgReset@5225#64"
#define	SPGM43	L"Programmer.1.ProgRun@5226#64"
#define	SPGM44	L"Programmer.1.ProgHold@5227#64"
#define	SPGM45	L"Programmer.1.ProgRunHold@5228#64"
#define	SPGM46	L"Programmer.1.ProgRunReset@5229#64"
#define	SPGM47	L"Programmer.1.Offset46@5230#64"
#define	SPGM48	L"Programmer.1.Offset47@5231#64"
#define	SPGM49	L"Programmer.1.Offset48@5232#64"
#define	SPGM50	L"Programmer.1.Offset49@5233#64"
#define	SPGM51	L"Programmer.1.Offset50@5234#64"
#define	SPGM52	L"Programmer.1.Offset51@5235#64"
#define	SPGM53	L"Programmer.1.Offset52@5236#64"
#define	SPGM54	L"Programmer.1.Offset53@5237#64"
#define	SPGM55	L"Programmer.1.Offset54@5238#64"
#define	SPGM56	L"Programmer.1.Offset55@5239#64"
#define	SPGM57	L"Programmer.1.Offset56@5240#64"
#define	SPGM58	L"Programmer.1.Offset57@5241#64"
#define	SPGM59	L"Programmer.1.Offset58@5242#64"
#define	SPGM60	L"Programmer.1.Offset59@5243#64"
#define	SPGM61	L"Programmer.1.Offset60@5244#64"
#define	SPGM62	L"Programmer.1.Offset61@5245#64"
#define	SPGM63	L"Programmer.1.Offset62@5246#64"
#define	SPGM64	L"Programmer.1.Offset63@5247#64"

// Programmer Segment Strings P 1-8 S 1-16 (48) $ means segment number needed
#define	SPGS01	L"Prgr.1.Segment.1.Type@5376#1600$"
#define	SPGS02	L"Prgr.1.Segment.1.Holdback@5377#1600$"
#define	SPGS03	L"Prgr.1.Segment.1.CallProgNum@5378#1600$"
#define	SPGS04	L"Prgr.1.Segment.1.Cycles@5379#1600$"
#define	SPGS05	L"Prgr.1.Segment.1.Duration@5380#1600$"
#define	SPGS06	L"Prgr.1.Segment.1.RampRate@5381#1600$"
#define	SPGS07	L"Prgr.1.Segment.1.TargetSP@5382#1600$"
#define	SPGS08	L"Prgr.1.Segment.1.EndAction@5383#1600$"
#define	SPGS09	L"Prgr.1.Segment.1.EventOutputs@5384#1600$"
#define	SPGS10	L"Prgr.1.Segment.1.WaitFor@5385#1600$"
#define	SPGS11	L"Prgr.1.Segment.1.SyncToCh2Seg@5386#1600$"
#define	SPGS12	L"Prgr.1.Segment.1.GobackSeg@5387#1600$"
#define	SPGS13	L"Prgr.1.Segment.1.GobackCycles@5388#1600$"
#define	SPGS14	L"Prgr.1.Segment.1.PVEvent@5389#1600$"
#define	SPGS15	L"Prgr.1.Segment.1.PVThreshold@5390#1600$"
#define	SPGS16	L"Prgr.1.Segment.1.UserVal@5391#1600$"
#define	SPGS17	L"Prgr.1.Segment.1.GsoakType@5392#1600$"
#define	SPGS18	L"Prgr.1.Segment.1.GsoakVal@5393#1600$"
#define	SPGS19	L"Prgr.1.Segment.1.TimeEvent@5394#1600$"
#define	SPGS20	L"Prgr.1.Segment.1.OnTime@5395#1600$"
#define	SPGS21	L"Prgr.1.Segment.1.OffTime@5396#1600$"
#define	SPGS22	L"Prgr.1.Segment.1.PIDSet@5397#1600$"
#define	SPGS23	L"Prgr.1.Segment.1.PVWait@5398#1600$"
#define	SPGS24	L"Prgr.1.Segment.1.WaitVal@5399#1600$"
#define	SPGS25	L"Prgr.1.Segment.1.Offset24@5400#1600$"
#define	SPGS26	L"Prgr.1.Segment.1.Offset25@5401#1600$"
#define	SPGS27	L"Prgr.1.Segment.1.Offset26@5402#1600$"
#define	SPGS28	L"Prgr.1.Segment.1.Offset27@5403#1600$"
#define	SPGS29	L"Prgr.1.Segment.1.Offset28@5404#1600$"
#define	SPGS30	L"Prgr.1.Segment.1.Offset29@5405#1600$"
#define	SPGS31	L"Prgr.1.Segment.1.Offset30@5406#1600$"
#define	SPGS32	L"Prgr.1.Segment.1.Offset31@5407#1600$"

// PV Strings  (36)
// Format A.C@D
#define	SPV1	L"PV.CalState@534"
#define	SPV2	L"PV.CJCTemp@215"
#define	SPV3	L"PV.Emissivity@38"
#define	SPV4	L"PV.FilterTimeConstant@101"
#define	SPV5	L"PV.MeasuredVal@202"
#define	SPV6	L"PV.Offset@141"
#define	SPV7	L"PV.PV@360"
#define	SPV8	L"PV.RangeHigh@548"
#define	SPV9	L"PV.RangeLow@549"
#define	SPV10	L"PV.SBrkType@578"

// Recipe Strings  (37)
// Format A.C@D
#define	SRCP1	L"Recipe.LastDataset@315"
#define	SRCP2	L"Recipe.LoadingStatus@316"
#define	SRCP3	L"Recipe.RecipeSelect@313"

// RelayAA Strings  (38)
// Format A.C@D
#define	SRLY1	L"RlyAA.PV@363"

// Switchover Strings  (39)
// Format A.C@D
#define	SSWO1	L"SwitchOver.SelectIn_288@288"
#define	SSWO2	L"SwitchOver.SelectIn_4927@4927"
#define	SSWO3	L"SwitchOver.SwitchHigh_286@286"
#define	SSWO4	L"SwitchOver.SwitchHigh_4925@4925"
#define	SSWO5	L"SwitchOver.SwitchLow_287@287"
#define	SSWO6	L"SwitchOver.SwitchLow_4926@4926"

// Timer Strings 1-4  (40)
// Format A.B.C@D#E
#define	STIM1	L"Timer.1.ElapsedTime@4995#3"
#define	STIM2	L"Timer.1.Out@4996#3"
#define	STIM3	L"Timer.1.Time@4994#3"

// Txdr Strings 1-2  (41)
// Format A.B.C@D#E
#define	STXD1	L"Txdr.1.CalAdjust_1@237#8"
#define	STXD2	L"Txdr.1.CalAdjust_2@238#8"
#define	STXD3	L"Txdr.1.InHigh@233#8"
#define	STXD4	L"Txdr.1.InLow@232#8"
#define	STXD5	L"Txdr.1.ScaleHigh@235#8"
#define	STXD6	L"Txdr.1.ScaleLow@234#8"
#define	STXD7	L"Txdr.1.StartCal@226#2"
#define	STXD8	L"Txdr.1.StartHighCal@231#8"
#define	STXD9	L"Txdr.1.StartTare@225#2"
#define	STXD10	L"Txdr.1.TareValue@236#8"


// UsrVal Strings 1-16  (42)
#define	SUSE1	L"UsrVal.1.Val@4962#1"

// Zirconia Strings  (43)
// Format A.C@D
#define	SZIR1	L"Zirconia.1.CarbonPot@13256"
#define	SZIR2	L"Zirconia.1.CleanFreq@13251"
#define	SZIR3	L"Zirconia.1.CleanProbe@13248"
#define	SZIR4	L"Zirconia.1.CleanState@13268"
#define	SZIR5	L"Zirconia.1.CleanTime@13252"
#define	SZIR6	L"Zirconia.1.CleanValve@13263"
#define	SZIR7	L"Zirconia.1.DewPoint@13274"
#define	SZIR8	L"Zirconia.1.GasRef@13254"
#define	SZIR9	L"Zirconia.1.MaxRcovTime@13253"
#define	SZIR10	L"Zirconia.1.MinCalTemp@13270"
#define	SZIR11	L"Zirconia.1.MinRcovTime@13255"
#define	SZIR12	L"Zirconia.1.Oxygen@13261"
#define	SZIR13	L"Zirconia.1.OxygenExp@13260"
#define	SZIR14	L"Zirconia.1.ProbeFault@13271"
#define	SZIR15	L"Zirconia.1.ProbeInput@13259"
#define	SZIR16	L"Zirconia.1.ProbeOffset@13250"
#define	SZIR17	L"Zirconia.1.ProbeStatus@13262"
#define	SZIR18	L"Zirconia.1.ProbeType@13258"
#define	SZIR19	L"Zirconia.1.ProcFactor@13275"
#define	SZIR20	L"Zirconia.1.PVFrozen@13272"
#define	SZIR21	L"Zirconia.1.RemGasEn@13257"
#define	SZIR22	L"Zirconia.1.RemGasRef@13267"
#define	SZIR23	L"Zirconia.1.Resolution@13273"
#define	SZIR24	L"Zirconia.1.SootAlm@13264"
#define	SZIR25	L"Zirconia.1.TempInput@13269"
#define	SZIR26	L"Zirconia.1.TempOffset@13266"
#define	SZIR27	L"Zirconia.1.Time2Clean@13249"
#define	SZIR28	L"Zirconia.1.Tolerence@13276"
#define	SZIR29	L"Zirconia.1.WrkGas@13265"

// End of File
#endif
