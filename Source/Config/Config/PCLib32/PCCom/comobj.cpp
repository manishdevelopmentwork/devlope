
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Component Object
//

// Dynamic Class

AfxImplementDynamicClass(CComObject, CObject);

// Constructor

CComObject::CComObject(void)
{
	m_uRefCount  = 1;

	m_punkOuter  = NULL;

	m_pAggIntMap = NULL;

	m_pAggObjMap = NULL;
	}

// Destructor

CComObject::~CComObject(void)
{
	AfxAssert(!m_uRefCount);

	m_uRefCount = 1;
	
	EmptyAggregateMap();
	}

// Interface Map Creation

void CComObject::AddInterfaces(void)
{
	}

// Unknown Access

IUnknown * CComObject::GetUnknown(void)
{
	return (IUnknown *) this;
	}

IUnknown * CComObject::GetImplicit(void)
{
	return (IUnknown *) (IImplicit *) this;
	}

// IUnknown Methods

HRESULT CComObject::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		try {
			*ppObject = NULL;

			if( m_punkOuter ) {

				HRESULT h = m_punkOuter->QueryInterface(iid, ppObject);

				return h;
				}

			return ImplicitQuery(iid, ppObject);
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

ULONG CComObject::AddRef(void)
{
	if( m_punkOuter ) {

		ULONG n = m_punkOuter->AddRef();

		return n;
		}

	return ImplicitAddRef();
	}

ULONG CComObject::Release(void)
{
	if( m_punkOuter ) {

		ULONG n = m_punkOuter->Release();

		return n;
		}

	return ImplicitRelease();
	}

// IImplicit Methods

HRESULT CComObject::ImplicitQuery(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		try {
			*ppObject = NULL;

			if( iid == IID_IUnknown ) {

				*ppObject = GetImplicit();

				InterlockedIncrement(&m_uRefCount);

				return S_OK;
				}
			else {
				BuildInterfaceMap();

				CInterfaceMap &IntMap = AfxThisClass()->GetInterfaceMap();

				INDEX i = IntMap.FindName(iid);

				if( !IntMap.Failed(i) ) {

					UINT uStep = IntMap[i];

					*ppObject = ((BYTE *) this) + uStep;

					((IUnknown *) *ppObject)->AddRef();

					return S_OK;
					}
				}

			if( m_pAggIntMap ) {

				CAggregateIntMap &AggMap = *m_pAggIntMap;

				INDEX i = AggMap.FindName(iid);

				if( !AggMap.Failed(i) ) {

					IUnknown *pObject = AggMap[i];

					if( SUCCEEDED(pObject->QueryInterface(iid, ppObject)) ) {

						return S_OK;
						}
					}
				}

			return E_NOINTERFACE;
			}

		AfxStdCatch();
		}

	return E_POINTER;
	}

ULONG CComObject::ImplicitAddRef(void)
{
	InterlockedIncrement(&m_uRefCount);

	return m_uRefCount;
	}

ULONG CComObject::ImplicitRelease(void)
{
	if( !InterlockedDecrement(&m_uRefCount) ) {

		AfxThisClass()->ObjectDeleted();
	
		delete this;

		return 0;
		}

	return m_uRefCount;
	}

// Creation

BOOL CComObject::CheckLicense(void)
{
	return TRUE;
	}

BOOL CComObject::Create(void)
{
	return TRUE;
	}

// Operations

void CComObject::SetOuter(IUnknown *punkOuter)
{
	AfxAssert(!m_punkOuter);

	m_punkOuter = punkOuter;
	}

// Threading

CCriticalSection * CComObject::GetGuard(void)
{
	return NULL;	
	}

void CComObject::EnterGuarded(void)
{
	CCriticalSection *p = GetGuard();

	if( p ) p->Enter();
	}

void CComObject::LeaveGuarded(void)
{
	CCriticalSection *p = GetGuard();

	if( p ) p->Leave();
	}

// Registration

void CComObject::BuildRegInfo(CComRegInfo &Info)
{
	CString ClassName = AfxThisClass()->GetClassName() + 1;

	CString Company   = AfxThisClass()->GetModule()->GetRegCompany();

	CString Stripped  = Company;

	Stripped.StripAll();

	if( !Info.m_Version ) Info.m_Version = 1;

	Info.m_Readable.Printf(L"%s %s Object", Company, ClassName);

	Info.m_ProgID.Printf  (L"%s.%s.%u", Stripped, ClassName, Info.m_Version);

	Info.m_VerInd.Printf  (L"%s.%s", Stripped, ClassName);
	}

void CComObject::BuildRegKeys(CKeyMap &Map)
{
	CComRegInfo Info;

	BuildRegInfo(Info);

	Map.Insert(L"", Info.m_Readable);

	Map.Insert(L"VersionIndependentProgID\\", Info.m_VerInd);

	Map.Insert(L"ProgID\\", Info.m_ProgID);

	if( AfxThisClass()->GetModule()->IsServer() ) {
	
		Map.Insert(L"InprocServer32\\", AfxThisClass()->GetModule()->GetFilename());
		
		Map.Insert(L"InprocServer32\\ThreadingModel", L"Both");

		return;
		}

	Map.Insert(L"LocalServer32\\", AfxThisClass()->GetModule()->GetFilename());

	Map.Insert(L"AppID", GetClassGuid());
	}

BOOL CComObject::EnumCategories(UINT i, CComCategory &Cat)
{
	return FALSE;
	}

BOOL CComObject::CustomRegistration(void)
{
	return TRUE;
	}

// Connection Support

DWORD CComObject::AttachConnection(REFIID iid, IUnknown *pSource, IUnknown *pSink)
{
	AfxAssert(pSource);

	DWORD Cookie = 0;

	IConnectionPointContainer *pContain = NULL;

	pSource->QueryInterface(IID_IConnectionPointContainer, (void **) &pContain);
		
	if( pContain ) {

		IConnectionPoint *pConPoint = NULL;

		pContain->FindConnectionPoint(iid, &pConPoint);

		if( pConPoint ) {

			if( !pSink )
				pSink = GetUnknown();

			pConPoint->Advise(pSink, &Cookie);

			pConPoint->Release();
			}

		pContain->Release();
		}

	return Cookie;
	}

BOOL CComObject::DetachConnection(REFIID iid, IUnknown *pSource, DWORD Cookie)
{
	AfxAssert(pSource);

	AfxAssert(Cookie);

	HRESULT h = E_UNEXPECTED;

	IConnectionPointContainer *pContain = NULL;

	pSource->QueryInterface(IID_IConnectionPointContainer, (void **) &pContain);
		
	if( pContain ) {

		IConnectionPoint *pConPoint = NULL;

		pContain->FindConnectionPoint(iid, &pConPoint);

		if( pConPoint ) {

			h = pConPoint->Unadvise(Cookie);

			pConPoint->Release();
			}

		pContain->Release();
		}

	return SUCCEEDED(h);
	}

// Interface and Aggregation

void CComObject::AddInterface(REFIID iid, IUnknown *pObject)
{
	AfxAssert(pObject);

	CInterfaceMap &IntMap = AfxThisClass()->GetInterfaceMap();

	UINT uStep = ((BYTE *) pObject) - ((BYTE *) this);

	IntMap.Insert(iid, uStep);
	}

BOOL CComObject::AddAggregate(REFIID iid, IUnknown *pObject)
{
	AfxAssert(pObject);

	AllocAggregateMap();

	if( m_pAggIntMap->Insert(iid, pObject) ) {

		pObject->AddRef();

		return TRUE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CComObject::AddAggregate(REFIID iid, CGuid const &Guid)
{
	if( m_pAggObjMap ) { 

		CAggregateObjMap &AggMap = *m_pAggObjMap;

		INDEX i = AggMap.FindName(Guid);

		if( !AggMap.Failed(i) ) {
			
			IUnknown *pObject = AggMap[i];
			
			AddAggregate(iid, pObject);

			pObject->Release();

			return TRUE;
			}
		}

	IUnknown *pObject = NULL;

	CoCreateInstance( Guid,
			  GetUnknown(),
			  CLSCTX_SERVER,
			  IID_IUnknown,
			  (void **) &pObject
			  );
	
	if( pObject ) {

		AddAggregate(iid, pObject);
	
		m_pAggObjMap->Insert(Guid, pObject);

		pObject->Release();
		
		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CComObject::BuildInterfaceMap(void)
{
	CCriticalGuard Guard;

	ICLASS c = AfxThisClass();

	UINT   n = AfxAllocAppData(L"AfxInterfaceMap");

	if( !c->GetAppData(n) ) {

		AddInterfaces();

		c->SetAppData(n, NULL);

		return TRUE;
		}

	return FALSE;
	}

BOOL CComObject::AllocAggregateMap(void)
{
	if( !m_pAggIntMap && !m_pAggObjMap ) {

		m_pAggIntMap = New CAggregateIntMap;

		m_pAggObjMap = New CAggregateObjMap;

		return TRUE;
		}

	return FALSE;
	}

void CComObject::EmptyAggregateMap(void)
{
	if( m_pAggObjMap ) {

		CAggregateObjMap &AggMap = *m_pAggObjMap;

		INDEX i = AggMap.GetHead();
		
		while( !AggMap.Failed(i) ) {

			IUnknown *pObject = AggMap[i];

			pObject->Release();
			
			AggMap.GetNext(i);
			}

		AggMap.Empty();

		delete m_pAggObjMap;

		m_pAggObjMap = NULL;
		}

	if( m_pAggIntMap ) {

		m_pAggIntMap->Empty();

		delete m_pAggIntMap;

		m_pAggIntMap = NULL;
		}
	}

// End of File
