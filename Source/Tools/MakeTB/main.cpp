
#include "intern.hpp"

// Static Data

static	FILE *	m_pList = NULL;

static	int	m_nSize = 24;

static	int	m_xSize = 0;

static	int	m_ySize = 0;

static	HBITMAP	m_hBit  = NULL;

static	HBITMAP	m_hOld  = NULL;

static	HDC	m_hRef	= NULL;

static	HDC	m_hDC   = NULL;

static	PCSTR	m_pDir1 = "j:\\tfs\\glyFX\\Library\\";

static	PCSTR	m_pDir2 = "j:\\tfs\\c3.1\\dev\\config\\source\\icons\\";

// Prototypes

global	void	main(int nArg, char *pArg[]);
static	BOOL	LoadBitmap(int xs, int ys, int yo, char *pBase, char *pMod);
static	BOOL	LoadBitmap(int xs, int ys, int yo, BYTE *pData);
static	void	SaveBitmap(PCSTR pName);

// Code

global	void	main(int nArg, char *pArg[])
{
	if( nArg != 4 ) {

		printf("usage: maketb <script> <size> <output>\n");

		exit(1);
		}

	m_nSize = atoi (pArg[2]);

	m_pList = fopen(pArg[1], "rt");

	if( m_pList ) {

		char sSize[32];

		sprintf(sSize, "\\%ux%u\\", m_nSize, m_nSize);

		for( UINT n = 0, p = 0; p < 2; p++ ) {

			fseek(m_pList, 0, SEEK_SET);

			if( p ) {

				m_xSize = m_nSize * n;

				m_ySize = m_nSize * 3;

				RECT Rect;
				
				m_hBit = CreateBitmap(m_xSize, m_ySize, 1, 32, NULL);

				m_hRef = GetDC(NULL);

				m_hDC  = CreateCompatibleDC(m_hRef);

				m_hOld = SelectObject(m_hDC, m_hBit);

				Rect.left   = 0;
				Rect.top    = 0;
				Rect.right  = m_xSize;
				Rect.bottom = m_ySize;

				HBRUSH hBrush = CreateSolidBrush(RGB(255,0,255));

				FillRect(m_hDC, &Rect, hBrush);
				
				DeleteObject(hBrush);
				}

			for( n = 0; !feof(m_pList); n++ ) {
			
				char sLine[256] = {0};

				fgets(sLine, sizeof(sLine), m_pList);

				if( sLine[0] ) {

					if( p ) {

						char *pComma1 = strchr(sLine,     ',');

						char *pComma2 = strchr(pComma1+1, ',');

						*pComma1 = 0;

						*pComma2 = 0;

						UINT t;

						for( t = 0; t < 2; t++ ) {
						
							char sBase[256] = {0};

							strcat(sBase, t ? m_pDir2 : m_pDir1);

							strcat(sBase, sLine);

							strcat(sBase, "\\bmp");

							strcat(sBase, sSize);

							strcat(sBase, pComma1 + 1);

							int yo = atoi(pComma2 + 1);
							
							if( LoadBitmap(n, 0, yo, sBase, "") ) {

								LoadBitmap(n, 1, yo, sBase, "_d");
								
								LoadBitmap(n, 2, yo, sBase, "_h");

								break;
								}
							}

						if( t == 2 ) {

							printf("maketb: can't find %s\n", pComma1 + 1);

							exit(2);
							}
						}

					continue;
					}

				break;
				}
			}

		SelectObject(m_hDC, m_hOld);

		SaveBitmap(pArg[3]);

		DeleteDC(m_hDC);

		DeleteObject(m_hBit);

		ReleaseDC(NULL, m_hRef);

		fclose(m_pList);

		exit(0);
		}

	printf("maketb: can't open %s\n", pArg[1]);

	exit(4);
	}

static	BOOL	LoadBitmap(int xs, int ys, int yo, char *pBase, char *pMod)
{
	char sFile[256];

	char sSize[32];

	for( UINT t = 0; t < 2; t++ ) {

		sprintf(sSize, "%s%u",  t ? "" : "_", m_nSize);

		strcpy(sFile, pBase);

		strcat(sFile, sSize);

		strcat(sFile, pMod);

		strcat(sFile, ".bmp");

		FILE *pFile = fopen(sFile, "rb");

		if( pFile ) {

			fseek(pFile, 0, SEEK_END);

			UINT  uSize = ftell(pFile);

			BYTE *pData = new BYTE [ uSize ];

			fseek(pFile, 0, SEEK_SET);

			fread(pData, uSize, 1, pFile);

			fclose(pFile);

			if( LoadBitmap(xs, ys, yo, pData) ) {

				delete [] pData;

				return TRUE;
				}

			printf("maketb: can't parse %s\n", sFile);

			exit(3);
			}
		}

	return FALSE;
	}

static	BOOL	LoadBitmap(int xs, int ys, int yo, BYTE *pData)
{
	BITMAPFILEHEADER *pHead = (BITMAPFILEHEADER *) pData;

	BITMAPINFO       *pInfo = (BITMAPINFO       *) (pHead+1);

	BYTE             *pBits = pData + pHead->bfOffBits;

	if( pInfo->bmiHeader.biBitCount == 8 ) {

		for( int y = m_nSize - 1; y >= 0; y-- ) {

			for( int x = 0; x < m_nSize ; x++ ) {

				BYTE p = *pBits++;

				BYTE b = pInfo->bmiColors[p].rgbBlue;
				BYTE g = pInfo->bmiColors[p].rgbGreen;
				BYTE r = pInfo->bmiColors[p].rgbRed;

				if( r == 0xFF && b == 0xFF && g == 0x00 ) {

					continue;
					}

				SetPixel( m_hDC,
					  m_nSize * xs + x,
					  m_nSize * ys + y + yo,
					  RGB(r,g,b)
					  );
				}
			}

		return TRUE;
		}

	if( pInfo->bmiHeader.biBitCount == 24 ) {

		for( int y = m_nSize - 1; y >= 0; y-- ) {

			for( int x = 0; x < m_nSize ; x++ ) {

				BYTE b = *pBits++;
				BYTE g = *pBits++;
				BYTE r = *pBits++;

				if( r == 0xFF && b == 0xFF && g == 0x00 ) {

					continue;
					}

				SetPixel( m_hDC,
					  m_nSize * xs + x,
					  m_nSize * ys + y + yo,
					  RGB(r,g,b)
					  );
				}
			}
		return TRUE;
		}

	if( pInfo->bmiHeader.biBitCount == 32 ) {

		for( int y = m_nSize - 1; y >= 0; y-- ) {

			for( int x = 0; x < m_nSize ; x++ ) {

				BYTE b = *pBits++;
				BYTE g = *pBits++;
				BYTE r = *pBits++;
				BYTE a = *pBits++;

				if( r == 0xFF && b == 0xFF && g == 0x00 ) {

					continue;
					}

				SetPixel( m_hDC,
					  m_nSize * xs + x,
					  m_nSize * ys + y + yo,
					  RGB(r,g,b)
					  );
				}
			}
		
		return TRUE;
		}

	return FALSE;
	}

static	void	SaveBitmap(PCSTR pName)
{
	UINT             uSize = m_xSize * m_ySize * 4;

	BYTE             *pData = new BYTE [ uSize ];

	BITMAPINFO       *pInfo = new BITMAPINFO;

	BITMAPFILEHEADER *pHead = new BITMAPFILEHEADER;

	////////

	memset(pHead, 0, sizeof(BITMAPFILEHEADER));

	pHead->bfType    = 'MB';
	pHead->bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO);
	pHead->bfSize    = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFO) + uSize;

	////////

	memset(pInfo, 0, sizeof(BITMAPINFO));

	pInfo->bmiHeader.biSize		 = sizeof(BITMAPINFO);
	pInfo->bmiHeader.biWidth	 = m_xSize;
	pInfo->bmiHeader.biHeight	 = m_ySize;
	pInfo->bmiHeader.biPlanes	 = 1;
	pInfo->bmiHeader.biBitCount	 = 32;
	pInfo->bmiHeader.biCompression	 = BI_RGB;
	pInfo->bmiHeader.biSizeImage	 = 0;
	pInfo->bmiHeader.biXPelsPerMeter = 0;
	pInfo->bmiHeader.biYPelsPerMeter = 0;
	pInfo->bmiHeader.biClrUsed	 = 0;
	pInfo->bmiHeader.biClrImportant	 = 0;

	////////

	GetDIBits( m_hDC,
		   m_hBit,
		   0,
		   m_ySize,
		   pData,
		   pInfo,
		   DIB_RGB_COLORS
		   );

	////////

	FILE *pWrite = fopen(pName, "wb");

	if( pWrite ) {

		fwrite(pHead, sizeof(*pHead), 1, pWrite);

		fwrite(pInfo, sizeof(*pInfo), 1, pWrite);

		fwrite(pData, uSize, 1, pWrite);

		fclose(pWrite);

		////////

		delete pData;

		delete pHead;

		delete pInfo;
		
		return;
		}

	printf("maketb: can't open %s\n", pName);

	exit(4);
	}

// End of File
