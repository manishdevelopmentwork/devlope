
#include "Intern.hpp"

#include "DisplayDrm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <sys/ioctl.h>

#include <drm/drm.h>

#include <drm/drm_fourcc.h>

//////////////////////////////////////////////////////////////////////////
//
// Display Object using Dynamic Rendering Manager
//

// Macros

#define AllocA(t, c) ((t *) alloca(sizeof(t) * (c)))

// Instantiator

IDevice * Create_DisplayDrm(void)
{
	return (IDisplay *) New CDisplayDrm;
}

// Constructor

CDisplayDrm::CDisplayDrm(void)
{
	StdSetRef();

	m_pName = "/dev/dri/card0";

	m_uMode = modeFlipSetCrtc;

	m_pb0   = 0;

	m_pb1   = 0;
}

// Destructor

CDisplayDrm::~CDisplayDrm(void)
{
	CloseDisplay();
}

// IDevice

BOOL CDisplayDrm::Open(void)
{
	if( OpenDisplay() ) {

		m_pBuff = PDWORD(m_page ? m_pb1 : m_pb0);

		return CDisplaySoftPoint::Open();
	}

	return FALSE;
}

// IDisplay

void CDisplayDrm::Update(PCVOID pData)
{
	if( m_pb0 ) {

		if( m_uMode > modeSimple ) {

			if( m_uMode == modeFlipSync ) {

				BYTE b[128];

				int n = _read(m_fd, b, sizeof(b));

				AfxAssert(PDWORD(b)[0] == DRM_EVENT_FLIP_COMPLETE);

				AfxAssert(PDWORD(b)[1] == n);
			}

			m_page = !m_page;
		}

		if( true ) {

			LockPointer(true);

			SavePointer(PDWORD(pData));

			DrawPointer(PDWORD(pData));

			#if !defined(__INTELLISENSE__) && defined(_M_ARM)

			DWORD pd = DWORD(m_page ? m_pb1 : m_pb0);

			DWORD ps = DWORD(pData);

			DWORD nq = m_cb / 32;

			ASM("1:	ldmia	%0!, {r0-r7}	\n"
			    "	stmia	%1!, {r0-r7}	\n"
			    "	subs	%2,  #1		\n"
			    "	bne	1b		\n"
			    : "+r"(ps), "+r"(pd)
			    : "r" (nq)
			    : "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7"
			);

			#else

			memcpy(m_page ? m_pb1 : m_pb0, pData, m_cb);

			#endif

			LoadPointer(PDWORD(pData));
		}

		if( m_uMode > modeSimple ) {

			if( m_uMode == modeFlipSetCrtc ) {

				SetCrtc(m_crtc, m_page ? m_fb1 : m_fb0, m_cid, m_mode);
			}
			else {
				struct drm_mode_crtc_page_flip flip = { 0 };

				flip.crtc_id = m_crtc;

				flip.fb_id   = m_page ? m_fb1 : m_fb0;

				flip.flags   = (m_uMode == modeFlipSync) ? DRM_MODE_PAGE_FLIP_EVENT : 0;

				IoCtl(DRM_IOCTL_MODE_PAGE_FLIP, &flip);
			}

			m_pBuff = PDWORD(m_page ? m_pb1 : m_pb0);
		}

		LockPointer(false);
	}
}

// Implementation

BOOL CDisplayDrm::OpenDisplay(void)
{
	if( (m_fd = _open(m_pName, O_RDWR, 0)) > 0 ) {

		if( SetMaster() ) {

			struct drm_mode_card_res Res = { 0 };

			IoCtl(DRM_IOCTL_MODE_GETRESOURCES, &Res);

			Res.count_crtcs    = 0;

			Res.count_encoders = 0;

			Res.count_fbs      = 0;

			PUINT64 pConns       = AllocA(UINT64, Res.count_connectors);

			Res.connector_id_ptr = UINT64(DWORD(pConns));

			IoCtl(DRM_IOCTL_MODE_GETRESOURCES, &Res);

			for( int i = 0; i < Res.count_connectors; i++ ) {

				struct drm_mode_get_connector Conn = { 0 };

				Conn.connector_id = pConns[i];

				IoCtl(DRM_IOCTL_MODE_GETCONNECTOR, &Conn);

				Conn.count_props    = 0;

				Conn.count_encoders = 0;

				drm_mode_modeinfo *pModes = AllocA(drm_mode_modeinfo, Conn.count_modes);

				Conn.modes_ptr            = UINT64(DWORD(pModes));

				IoCtl(DRM_IOCTL_MODE_GETCONNECTOR, &Conn);

				m_cx = pModes[0].hdisplay;

				m_cy = pModes[0].vdisplay;

				m_cb = 4 * m_cx * m_cy;

				m_mb = ((m_cb + ((1<<12)-1)) >> 12) << 12;

				if( Conn.count_encoders && Conn.count_modes && Conn.connection ) {

					if( CreateBuffer(m_fb0, m_pb0) ) {

						if( m_uMode == modeSimple || CreateBuffer(m_fb1, m_pb1) ) {

							if( GetEncoderCrtc(m_crtc, Conn.encoder_id) ) {

								m_cid  = pConns[i];

								m_mode = pModes[0];

								if( SetCrtc(m_crtc, m_fb0, m_cid, m_mode) ) {

									m_page = 0;

									return TRUE;
								}
							}
						}
					}
				}
			}
		}
	}

	AfxTrace("display: failed to open drm device\n");

	return FALSE;
}

void CDisplayDrm::CloseDisplay(void)
{
	if( m_fd > 0 ) {

		// TODO -- Should we clean up better? Or do
		// we just rely on the DRM to do everything?

		if( m_pb0 > 0 ) {

			_munmap(m_pb0, m_mb);
		}

		if( m_pb1 > 0 ) {

			_munmap(m_pb1, m_mb);
		}

		DropMaster();

		_close(m_fd);
	}
}

BOOL CDisplayDrm::IoCtl(int n, void *p)
{
	int r;

	r = _ioctl(m_fd, n, p);

	return r >= 0;
}

BOOL CDisplayDrm::SetMaster(void)
{
	return IoCtl(DRM_IOCTL_SET_MASTER, 0);
}

BOOL CDisplayDrm::DropMaster(void)
{
	return IoCtl(DRM_IOCTL_DROP_MASTER, 0);
}

BOOL CDisplayDrm::CreateBuffer(UINT &id, PVOID &pb)
{
	struct drm_mode_create_dumb CreateDumb = { 0 };

	CreateDumb.width  = m_cx;
	CreateDumb.height = m_cy;
	CreateDumb.bpp    = 32;

	if( IoCtl(DRM_IOCTL_MODE_CREATE_DUMB, &CreateDumb) ) {

		struct drm_mode_fb_cmd CmdDumb = { 0 };

		CmdDumb.width  = CreateDumb.width;
		CmdDumb.height = CreateDumb.height;
		CmdDumb.pitch  = CreateDumb.pitch;
		CmdDumb.bpp    = 32;
		CmdDumb.depth  = 24;
		CmdDumb.handle = CreateDumb.handle;

		if( IoCtl(DRM_IOCTL_MODE_ADDFB, &CmdDumb) ) {

			struct drm_mode_map_dumb MapDumb = { 0 };

			MapDumb.handle = CreateDumb.handle;

			if( IoCtl(DRM_IOCTL_MODE_MAP_DUMB, &MapDumb) ) {

				if( (pb = _mmap(0, m_mb, PROT_READ | PROT_WRITE, MAP_SHARED, m_fd, MapDumb.offset)) ) {

					memset(pb, 0, m_cb);

					id = CmdDumb.fb_id;

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CDisplayDrm::GetEncoderCrtc(UINT &crtc, UINT enc)
{
	struct drm_mode_get_encoder Encoder = { 0 };

	Encoder.encoder_id = enc;

	if( IoCtl(DRM_IOCTL_MODE_GETENCODER, &Encoder) ) {

		crtc = Encoder.crtc_id;

		return TRUE;
	}

	return FALSE;
}

BOOL CDisplayDrm::SetCrtc(UINT crtc, UINT fb, UINT64 conn, struct drm_mode_modeinfo &mode)
{
	struct drm_mode_crtc Crtc = { 0 };

	Crtc.crtc_id = crtc;

	if( IoCtl(DRM_IOCTL_MODE_GETCRTC, &Crtc) ) {

		Crtc.fb_id              = fb;
		Crtc.set_connectors_ptr = UINT64(DWORD(&conn));
		Crtc.count_connectors   = 1;
		Crtc.mode               = mode;
		Crtc.mode_valid         = 1;

		if( IoCtl(DRM_IOCTL_MODE_SETCRTC, &Crtc) ) {

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
