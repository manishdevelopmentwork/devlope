
#include "Intern.hpp"

#include "ProgramResourceItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SystemResourceItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Programming Resource
//

// Dynamic Class

AfxImplementDynamicClass(CProgramResourceItem, CCommsSystem);

// Constructor

CProgramResourceItem::CProgramResourceItem(void) : CCommsSystem(TRUE)
{
	m_pSystem = New CSystemResourceItem;
	}

// Persistance

void CProgramResourceItem::Init(void)
{
	CCommsSystem::Init();

	m_pSystem->LoadLibrary();
	}

// Meta Data

void CProgramResourceItem::AddMetaData(void)
{
	CCommsSystem::AddMetaData();

	Meta_AddObject(System);
	}

// End of File
