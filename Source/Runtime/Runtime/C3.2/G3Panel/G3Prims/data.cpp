
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Data Block
//

// Constructor

CPrimData::CPrimData(CPrim *pPrim) : CPrimBlock(pPrim)
{
	m_pValue       = NULL;
	m_Entry        = 0;
	m_Flash        = 0;
	m_Layout       = 0;
	m_Content      = 0;
	m_TagLimits    = 0;
	m_TagLabel     = 0;
	m_TagFormat    = 0;
	m_TagColor     = 0;
	m_pEnable      = NULL;
	m_Local        = FALSE;
	m_pValidate    = NULL;
	m_pOnSetFocus  = NULL;
	m_pOnKillFocus = NULL;
	m_pOnComplete  = NULL;
	m_pOnError     = NULL;
	m_pLabel       = NULL;
	m_pLimitMin    = NULL;
	m_pLimitMax    = NULL;
	m_FormType     = 0;
	m_ColType      = 0;
	m_UseBack      = 1;
	m_pFormat      = NULL;
	m_pColor       = NULL;
	m_pTextColor   = New CPrimColor(naText);
	m_pTextShadow  = New CPrimColor(naNone);
	m_uTag	       = 0;
	m_fFocus       = FALSE;
	m_uPress       = 0;
	m_pEdit        = NULL;
	m_pKeyTitle    = NULL;
	m_pKeyStatus   = NULL;
	m_KeyNumeric   = 1;
	m_Accel        = 1;
	}

// Destructor

CPrimData::~CPrimData(void)
{
	delete m_pValue;
	delete m_pEnable;
	delete m_pValidate;
	delete m_pOnSetFocus;
	delete m_pOnKillFocus;
	delete m_pOnComplete;
	delete m_pOnError;
	delete m_pLabel;
	delete m_pLimitMin;
	delete m_pLimitMax;
	delete m_pFormat;
	delete m_pColor;
	delete m_pTextColor;
	delete m_pTextShadow;
	delete m_pEdit;
	delete m_pKeyTitle;
	delete m_pKeyStatus;
	}

// Initialization

void CPrimData::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimData", pData);

	CPrimBlock::Load(pData);

	GetCoded(pData, m_pValue);

	m_Entry   = GetByte(pData);
	m_Flash   = GetByte(pData);
	m_Layout  = GetByte(pData);
	m_Content = GetByte(pData);

	if( (m_uTag = GetWord(pData)) ) {

		m_TagLimits = GetByte(pData);
		m_TagLabel  = GetByte(pData);
		m_TagFormat = GetByte(pData);
		m_TagColor  = GetByte(pData);
		}

	if( m_Entry ) {

		GetCoded(pData, m_pEnable);
		GetCoded(pData, m_pValidate);
		GetCoded(pData, m_pOnSetFocus);
		GetCoded(pData, m_pOnKillFocus);
		GetCoded(pData, m_pOnComplete);
		GetCoded(pData, m_pOnError);
		
		m_Local = GetByte(pData);

		GetCoded(pData, m_pKeyTitle);
		GetCoded(pData, m_pKeyStatus);

		m_KeyNumeric = GetByte(pData);
		m_Accel      = GetByte(pData);
		}

	GetCoded(pData, m_pLabel);
	GetCoded(pData, m_pLimitMin);
	GetCoded(pData, m_pLimitMax);

	if( GetByte(pData) ) {

		m_pFormat = CDispFormat::MakeObject(GetByte(pData));

		if( m_pFormat ) {

			m_pFormat->Load(pData);
			}
		}

	if( GetByte(pData) ) {

		m_pColor = CDispColor::MakeObject(GetByte(pData));

		if( m_pColor ) {

			m_pColor->Load(pData);
			}
		}

	if( !m_pColor ) {
	
		m_pTextColor->Load(pData);
		
		m_pTextShadow->Load(pData);
		}

	m_UseBack = GetByte(pData);
	}

// Attributes

BOOL CPrimData::IsAvail(void) const
{
	CCodedText  *pLabel;

	CDispFormat *pFormat;
	
	CDispColor  *pColor;

	if( GetDataLabel(pLabel) ) {

		if( !pLabel->IsAvail() ) {

			return FALSE;
			}
		}

	if( GetDataFormat(pFormat) ) {

		if( !pFormat->IsAvail() ) {

			return FALSE;
			}
		}

	if( GetDataColor(pColor) ) {

		if( !pColor->IsAvail() ) {

			return FALSE;
			}
		}

	if( !IsItemAvail(m_pValue) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pEnable) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pValidate) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnSetFocus) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnKillFocus) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnComplete) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pOnError) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pLimitMin) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pLimitMax) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pTextColor) ) {

		return FALSE;
		}
	
	if( !IsItemAvail(m_pTextShadow) ) {

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CPrimData::IsTransparent(void) const
{
	if( m_Content == 1 && m_Layout == 0 ) {

		if( m_TagColor || m_pColor ) {

			if( m_UseBack ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CPrimData::IsTagRef(void) const
{
	return m_uTag ? TRUE : FALSE;
	}

BOOL CPrimData::GetTagItem(CTag * &pTag) const
{
	if( IsTagRef() ) {

		CCommsSystem *pSystem = CCommsSystem::m_pThis;

		CTagList     *pTags   = pSystem->m_pTags->m_pTags;

		if( (pTag = pTags->GetItem(m_uTag - 1)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagLabel(CCodedText * &pLabel) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pLabel ) {

			pLabel = pTag->m_pLabel;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagFormat(CDispFormat * &pFormat) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pFormat ) {

			pFormat = pTag->m_pFormat;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagColor(CDispColor * &pColor) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pColor ) {

			pColor = pTag->m_pColor;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetDataLabel(CCodedText * &pLabel) const
{
	if( (pLabel = m_pLabel) ) {

		return TRUE;
		}

	if( m_TagLabel ) {
		
		if( GetTagLabel(pLabel) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetDataFormat(CDispFormat * &pFormat) const
{
	if( (pFormat = m_pFormat) ) {

		return TRUE;
		}

	if( m_TagFormat ) {
		
		if( GetTagFormat(pFormat) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetDataColor(CDispColor * &pColor) const
{
	if( (pColor = m_pColor) ) {

		return TRUE;
		}

	if( m_TagColor ) {
			
		if( GetTagColor(pColor) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

void CPrimData::SetScan(UINT Code)
{
	CCodedText  *pLabel;

	CDispFormat *pFormat;
	
	CDispColor  *pColor;

	if( GetDataLabel(pLabel) ) {

		pLabel->SetScan(Code);
		}

	if( GetDataFormat(pFormat) ) {

		pFormat->SetScan(Code);
		}

	if( GetDataColor(pColor) ) {

		pColor->SetScan(Code);
		}

	SetItemScan(m_pValue,       Code);
	
	SetItemScan(m_pEnable,      Code);
	
	SetItemScan(m_pValidate,    Code);
	
	SetItemScan(m_pOnSetFocus,  Code);
	
	SetItemScan(m_pOnKillFocus, Code);
	
	SetItemScan(m_pOnComplete,  Code);
	
	SetItemScan(m_pOnError,     Code);
	
	SetItemScan(m_pLimitMin,    Code);
	
	SetItemScan(m_pLimitMax,    Code);
	
	SetItemScan(m_pTextColor,   Code);
	
	SetItemScan(m_pTextShadow,  Code);
	}

BOOL CPrimData::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		return TRUE;
		}

	return FALSE;
	}

void CPrimData::DrawItem(IGDI *pGDI, R2 Rect)
{
	SelectFont(pGDI);

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	switch( m_Layout ) {

		case 0:
			DrawSingle(pGDI, Rect);
			
			break;

		case 1:
			DrawMulti(pGDI, Rect);
			
			break;
		}
	}

UINT CPrimData::OnMessage(UINT uMsg, UINT uParam)
{
	switch( uMsg ) {

		case msgTakeFocus:

			return OnTakeFocus(uParam);

		case msgSetFocus:

			return OnSetFocus();

		case msgKillFocus:

			return OnKillFocus();

		case msgTouchInit:

			return OnTouchInit();

		case msgTouchDown:

			return OnTouchDown();

		case msgTouchUp:

			return OnTouchUp();

		case msgKeyDown:

			return OnKeyDown(uParam);

		case msgKeyUp:

			return OnKeyUp(uParam);

		case msgUsesKeypad:

			return m_Entry && m_pValue;

		case msgTestKeypad:

			return OnTestKeypad();

		case msgShowKeypad:

			return OnShowKeypad();

		case msgInitKeypad:

			return OnInitKeypad((IKeypad *) uParam);

		case msgDrawKeypad:

			return OnDrawKeypad((IKeypad *) uParam);

		case msgIsPressed:

			return m_uPress > 0;

		case msgBlockRemote:

			return m_Local == 1;
		}

	return 0;
	}

// Message Handlers

UINT CPrimData::OnTakeFocus(UINT uParam)
{
	if( m_pValue ) {

		if( uParam == 0 ) {

			if( IsAvail() ) {

				if( m_pEnable && !m_pEnable->ExecVal() ) {

					return FALSE;
					}

				return TRUE;
				}

			return FALSE;
			}

		if( uParam == 1 ) {

			return m_Entry ? TRUE : FALSE;
			}
		}

	return FALSE;
	}

UINT CPrimData::OnSetFocus(void)
{
	m_fFocus = TRUE;

	Execute(m_pOnSetFocus);

	return TRUE;
	}

UINT CPrimData::OnKillFocus(void)
{
	m_fFocus = FALSE;

	KillEdit();

	Execute(m_pOnKillFocus);

	return FALSE;
	}

UINT CPrimData::OnTouchInit(void)
{
	m_uPress = 1;

	return TRUE;
	}

UINT CPrimData::OnTouchDown(void)
{
	m_uPress = 2;

	return TRUE;
	}

UINT CPrimData::OnTouchUp(void)
{
	if( m_uPress ) {

		if( TRUE || m_uPress == 2 ) {

			InitEdit();
			}

		m_uPress = 0;

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimData::OnKeyDown(UINT uCode)
{
	if( m_pEdit ) {

		UINT uEdit = EditFunc(uCode);

		switch( uEdit ) {

			case editReload:

				ReadEditData();

				break;

			case editAbort:

				KillEdit();
				
				break;

			case editCommit:

				Execute(m_pOnComplete);

				KillEdit();

				break;

			case editError:

				if( !Execute(m_pOnError) ) {

					Beep(60, 200);
					}

				break;
			}

		return uEdit;
		}

	return FALSE;
	}

UINT CPrimData::OnKeyUp(UINT uCode)
{
	if( m_pEdit ) {

		if( uCode == 0x0A || uCode == 0x0B ) {

			m_pEdit->m_uAccel = m_Accel;

			m_pEdit->m_Step   = 0;

			m_pEdit->m_uCount = 0;

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CPrimData::OnTestKeypad(void)
{
	if( m_pEdit ) {

		UINT uType = m_pEdit->m_uKeypad;

		if( m_pEdit->m_Foot.GetLength() ) {

			uType |= kpsShowFoot;
			}

		uType |= kpsShowHead;

		return uType;
		}

	return 0;
	}

UINT CPrimData::OnShowKeypad(void)
{
	if( !m_pEdit && m_Entry && m_pValue ) {

		InitEdit();

		return TRUE;
		}

	return FALSE;
	}

UINT CPrimData::OnInitKeypad(IKeypad *pKeypad)
{
	CUnicode Head = m_pKeyTitle  ? m_pKeyTitle ->GetText() : L"";

	CUnicode Foot = m_pKeyStatus ? m_pKeyStatus->GetText() : L"";

	pKeypad->SetPadHead(Head.IsEmpty() ? FindLabelText() : Head);

	pKeypad->SetPadFoot(Foot.IsEmpty() ? m_pEdit->m_Foot : Foot);

	return TRUE;
	}

UINT CPrimData::OnDrawKeypad(IKeypad *pKeypad)
{
	if( m_pEdit->m_fDefault ) {

		ReadEditData();
		}
	else {
		if( m_pEdit->m_fHide ) {

			CUnicode Hide('*', m_pEdit->m_Edit.GetLength());

			pKeypad->SetPadText(Hide);

			pKeypad->SetPadCrsr(m_pEdit->m_uCursor);

			return TRUE;
			}
		}

	pKeypad->SetPadText(m_pEdit->m_Edit);

	pKeypad->SetPadCrsr(m_pEdit->m_uCursor);

	return TRUE;
	}

// Edit Support

BOOL CPrimData::InitEdit(void)
{
	if( !m_pEdit ) {

		m_pEdit = New CEditCtx;

		m_pEdit->m_pValue   = m_pValue;

		m_pEdit->m_pValid   = m_pValidate;

		m_pEdit->m_pFormat  = NULL;

		m_pEdit->m_uFlags   = GetEditFlags();
	
		m_pEdit->m_Type     = m_pValue->GetType();

		m_pEdit->m_uKeypad  = keypadNumeric;

		m_pEdit->m_uCursor  = cursorAll;

		m_pEdit->m_fDefault = TRUE;

		m_pEdit->m_fError   = FALSE;

		m_pEdit->m_fHide    = FALSE;

		m_pEdit->m_uAccel   = m_Accel;

		GetDataFormat(m_pEdit->m_pFormat);

		ReadEditData();

		if( EditFunc(0) == editAbort ) {

			KillEdit();

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::KillEdit(void)
{
	if( m_pEdit ) {

		delete m_pEdit;

		m_pEdit = NULL;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::ReadEditData(void)
{
	m_pEdit->m_Min  = GetMinValue(m_pEdit->m_Type);

	m_pEdit->m_Max  = GetMaxValue(m_pEdit->m_Type);

	m_pEdit->m_Data = m_pValue->ExecVal();

	if( m_pEdit->m_pFormat ) {
		
		m_pEdit->m_Edit = m_pEdit->m_pFormat->Format( m_pEdit->m_Data,
							      m_pEdit->m_Type,
							      fmtBare | fmtPad
							      );
		}
	else {
		m_pEdit->m_Edit = CDispFormat::GeneralFormat( m_pEdit->m_Data,
							      m_pEdit->m_Type,
							      fmtBare | fmtPad
							      );
		}

	return TRUE;
	}

UINT CPrimData::EditFunc(UINT uCode)
{
	if( m_pEdit->m_pFormat ) {

		return m_pEdit->m_pFormat->Edit(m_pEdit, uCode);
		}

	return CDispFormat::GeneralEdit(m_pEdit, uCode);
	}

// Drawing Helpers

void CPrimData::DrawSingle(IGDI *pGDI, R2 Rect)
{
	BOOL  fMove  = m_Ctx.m_uPress;

	BOOL  fFocus = m_Ctx.m_fFocus;

	BOOL  fMoveX = fMove && m_MoveDir > 1;

	BOOL  fMoveY = fMove && m_MoveDir > 0;

	int   xOrg   = Rect.x1;

	int   yOrg   = Rect.y1;

	int   xRect  = Rect.x2 - Rect.x1;

	int   yRect  = Rect.y2 - Rect.y1;

	int   yEdit  = m_Entry ? 1 : 0;

	int   ySize  = pGDI->GetTextHeight(L"X");
			
	int   yPos   = m_AlignV < 5 ? max(0, (m_AlignV * (yRect - ySize - yEdit)) / 4) + yEdit : yEdit;

	if( fMoveY ) {

		yPos += m_MoveStep;
		}

	if( m_Content == 1 ) {

		CUnicode Label = UniVisual(m_Ctx.m_Label) + L':';

		CUnicode Value = UniVisual(m_Ctx.m_Value);

		int xSize1 = pGDI->GetTextWidth(Label);

		int xSize2 = pGDI->GetTextWidth(Value);

		int xPos1  = Rect.x1 + 2;

		int xPos2  = Rect.x2 - 2 - xSize2;

		if( fMoveX ) {

			xPos1 += m_MoveStep;

			xPos2 += m_MoveStep;
			}

		if( m_Ctx.m_Shadow < 0x8000 ) {

			if( !fFocus ) {

				SetColors(pGDI, 3);

				pGDI->TextOut(xPos1 + 1, yOrg + yPos + 1, Label);

				pGDI->TextOut(xPos2 + 1, yOrg + yPos + 1, Value);
				}
			else {
				SetColors(pGDI, 3);

				pGDI->TextOut(xPos1 + 1, yOrg + yPos + 1, Label);
				}
			}

		if( TRUE ) {

			SetColors(pGDI, 1);

			if( m_Entry ) {

				pGDI->FillRect( xPos1,
						yOrg  + yPos - 1,
						xPos2,
						yOrg  + yPos
						);
				}

			pGDI->TextOut(xPos1, yOrg + yPos, Label);
			}

		if( xPos1 + xSize1 < xPos2 ) {

			pGDI->FillRect( xPos1 + xSize1,
					yOrg  + yPos - yEdit,
					xPos2,
					yOrg  + yPos + ySize
					);
			}

		if( TRUE ) {

			SetColors(pGDI, 2);

			if( m_Entry ) {

				pGDI->FillRect( xPos2,
						yOrg  + yPos - 1,
						xPos2 + xSize2,
						yOrg  + yPos
						);
				}

			pGDI->TextOut(xPos2, yOrg + yPos, Value);
			}
		}
	else {
		CUnicode Line  = UniVisual(m_Content ? m_Ctx.m_Label : m_Ctx.m_Value);

		int      xSize = pGDI->GetTextWidth(Line);

		int      xPos  = (m_AlignH * (xRect - xSize)) / 2;

		if( fMoveX ) {

			xPos += m_MoveStep;
			}

		if( m_Ctx.m_Shadow < 0x8000 ) {

			if( !fFocus ) {

				SetColors(pGDI, 3);

				pGDI->TextOut(xOrg + xPos + 1, yOrg + yPos + 1, Line);
				}
			}

		if( TRUE ) {

			SetColors(pGDI, 2);

			if( fFocus ) {

				pGDI->FillRect( xOrg + xPos,
						yOrg + yPos - 1,
						xOrg + xPos + xSize,
						yOrg + yPos
						);
				}

			pGDI->TextOut(xOrg + xPos, yOrg + yPos, Line);
			}
		}
	}

void CPrimData::DrawMulti(IGDI *pGDI, R2 Rect)
{
	CUnicode Text;

	if( m_Content == 1 ) {

		Text += UniVisual(m_Ctx.m_Label) + L':';

		Text += '|';

		Text += UniVisual(m_Ctx.m_Value);
		}
	else {
		if( m_Content ) {

			Text = UniVisual(m_Ctx.m_Label);
			}
		else
			Text = UniVisual(m_Ctx.m_Value);
		}

	m_Flow.Flow(pGDI, Text, Rect);

	CTextFlow::CFormat Fmt;

	Fmt.m_AlignH   = m_AlignH;
	
	Fmt.m_AlignV   = m_AlignV;
	
	Fmt.m_Lead     = m_Lead;
	
	Fmt.m_Color    = m_Ctx.m_Fore1;
	
	Fmt.m_Shadow   = m_Ctx.m_Shadow;
	
	Fmt.m_fMove    = m_Ctx.m_uPress;
	
	Fmt.m_MoveDir  = m_MoveDir;
	
	Fmt.m_MoveStep = m_MoveStep;

	m_Flow.Draw(pGDI, Fmt);
	}

// Color Setting

void CPrimData::SetColors(IGDI *pGDI, UINT uMode)
{
	switch( uMode ) {

		case 1:
			SetColors(pGDI, m_Ctx.m_Fore1, m_Ctx.m_Back1);
			break;

		case 2:
			SetColors(pGDI, m_Ctx.m_Fore2, m_Ctx.m_Back2);
			break;

		case 3:
			SetColors(pGDI, m_Ctx.m_Shadow, 0x8000);
			break;
		}
	}

void CPrimData::SetColors(IGDI *pGDI, COLOR Fore, COLOR Back)
{
	pGDI->SetTextFore(Fore);

	if( Back >= 0x8000 ) {

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SelectBrush(brushNull);
		}
	else {
		pGDI->SetTextBack(Back);

		pGDI->SetTextTrans(modeOpaque);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Back);
		}
	}

// Limit Access

DWORD CPrimData::GetMinValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag;

		if( GetTagItem(pTag) ) {

			DWORD     n = m_pValue->Execute(typeLValue);

			CDataRef &r = (CDataRef &) n;

			return pTag->GetMinValue(r.x.m_Array, Type);
			}
		}

	if( m_pLimitMin ) {

		return m_pLimitMin->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return 0;
	}

DWORD CPrimData::GetMaxValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag;

		if( GetTagItem(pTag) ) {

			DWORD     n = m_pValue->Execute(typeLValue);

			CDataRef &r = (CDataRef &) n;

			return pTag->GetMaxValue(r.x.m_Array, Type);
			}
		}

	if( m_pLimitMax ) {

		return m_pLimitMax->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(100);
		}

	return 100;
	}

// Text Extraction

CUnicode CPrimData::FindLabelText(void)
{
	CCodedText *pLabel;

	if( GetDataLabel(pLabel) ) {

		DWORD n = 0;

		if( m_pValue && m_TagLabel ) {

			CDataRef &Ref = (CDataRef &) n;

			n = m_pValue->Execute(typeLValue);

			n = Ref.x.m_Array;
			}

		return pLabel->GetText(L"", &n);
		}
	else {
		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			CUnicode Name = pTag->m_Name;

			UINT     uPos = Name.FindRev(L'.');

			if( uPos < NOTHING ) {

				return Name.Mid(uPos + 1);
				}

			return Name;
			}
		}

	return L"Label";
	}

CUnicode CPrimData::FindValueText(DWORD Data, UINT Type)
{
	CUnicode     Value = L"";

	WCHAR        cHair = spaceHair;

	CDispFormat *pForm = NULL;

	if( GetDataFormat(pForm) ) {

		if( pForm->IsAvail() ) {

			Value = pForm->Format(Data, Type, fmtPad);
			}
		else
			Value = pForm->Format(0, typeVoid, fmtPad);
		}
	else
		Value = CDispFormat::GeneralFormat(Data, Type, fmtPad);

	if( m_Entry ) {

		Value = cHair + Value + cHair;
		}

	return Value;
	}

UINT CPrimData::GetEditFlags(void)
{
	UINT uFlags = CUISystem::m_pThis->m_pUI->GetEditFlags();

	if( m_KeyNumeric ) {

		if( m_KeyNumeric == 1 ) {
			
			uFlags &= ~flagRampOnly;

			uFlags &= ~flagShowRamp;
			}

		if( m_KeyNumeric == 2 ) {

			uFlags &= ~flagRampOnly;

			uFlags |=  flagShowRamp;
			}

		if( m_KeyNumeric == 3 ) {

			uFlags |=  flagRampOnly;

			uFlags &= ~flagShowRamp;
			}
		}

	return uFlags;
	}

// Context Creation

void CPrimData::FindCtx(CCtx &Ctx)
{
	DWORD Data   = 0;

	UINT  Type   = typeVoid;

	if( m_pValue ) {

		if( m_pValue->IsAvail() ) {

			Data = m_pValue->ExecVal();

			Type = m_pValue->GetType();
			}
		}

	if( m_Content >= 1 ) {

		Ctx.m_Label = FindLabelText();
		}

	if( m_Content <= 1 ) {
	
		Ctx.m_Value = FindValueText(Data, Type);
		}

	CDispColor *pColor;

	if( GetDataColor(pColor) || m_TagColor ) {

		DWORD Pair;
		
		if( pColor ) {
			
			Pair = pColor->GetColorPair(Data, Type);
			}
		else
			Pair = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));

		if( m_UseBack ) {

			Ctx.m_Fore1 = LOWORD(Pair);

			Ctx.m_Back1 = HIWORD(Pair);
			}
		else {
			Ctx.m_Fore1 = LOWORD(Pair);

			Ctx.m_Back1 = 0x8000;
			}
		
		if( m_fFocus ) {

			Ctx.m_Fore2 = HIWORD(Pair);

			Ctx.m_Back2 = LOWORD(Pair);
			}

		Ctx.m_Shadow = 0x8000;
		}
	else {
		if( m_fFocus ) {

			Ctx.m_Fore1  = m_pTextColor->GetColor();

			Ctx.m_Back1  = 0x8000;

			Ctx.m_Fore2  = FindCompColor(Ctx.m_Fore1);

			Ctx.m_Back2  = Ctx.m_Fore1;

			Ctx.m_Shadow = m_pTextShadow->GetColor();
			}
		else {
			Ctx.m_Fore1  = m_pTextColor->GetColor();

			Ctx.m_Back1  = 0x8000;

			Ctx.m_Shadow = m_pTextShadow->GetColor();
			}
		}

	if( !m_fFocus ) {

		Ctx.m_Fore2 = Ctx.m_Fore1;

		Ctx.m_Back2 = Ctx.m_Back1;
		}

	if( !Type ) {

		Ctx.m_Fore2 = naText;

		Ctx.m_Back2 = naNone;
		}

	if( !m_Entry ) {

		Ctx.m_fFocus = m_fFocus;

		Ctx.m_uPress = m_pPrim->IsPressed();
		}
	else {
		Ctx.m_fFocus = m_fFocus;

		Ctx.m_uPress = m_uPress;
		}

	FreeData(Data, Type);
	}

// Implementation

BOOL CPrimData::Execute(CCodedItem *pAction)
{
	if( pAction ) {

		pAction->Execute(typeVoid);

		return TRUE;
		}

	return FALSE;
	}

COLOR CPrimData::FindCompColor(COLOR c)
{
	BYTE  r = GetRED  (c);
	BYTE  g = GetGREEN(c);
	BYTE  b = GetBLUE (c);

	BYTE  i = BYTE((14*r + 45*g + 5*b) / 64);

	if( i >= 15 ) {

		return GetRGB(0,0,0);
		}

	return GetRGB(31,31,31);
	}

// Context Check

BOOL CPrimData::CCtx::operator == (CCtx const &That) const
{
	if( !m_Label.CompareC(That.m_Label) ) {

		if( !m_Value.CompareC(That.m_Value) ) {

			return m_Fore1  == That.m_Fore1  &&
			       m_Fore2  == That.m_Fore2  &&
			       m_Back1  == That.m_Back1  &&
			       m_Back2  == That.m_Back2  &&
			       m_Shadow == That.m_Shadow &&
			       m_fFocus == That.m_fFocus &&
			       m_uPress == That.m_uPress ;
			}
		}

	return FALSE;
	}

// End of File
