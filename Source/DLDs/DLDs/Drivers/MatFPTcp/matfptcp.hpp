#include "basefp2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series TCP/IP Constants
//

#define ETLAN_NONE	0
#define ETLAN_SAME	1
#define ETLAN_OTHER	2

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP Series TCP/IP Master Driver
//

class CMatsushitaFPTcpDriver : public CMatFP2BaseDriver
{
	public:
		// Constructor
		CMatsushitaFPTcpDriver(void);

		// Destructor
		~CMatsushitaFPTcpDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CTcpContext : CMatFP2BaseDriver::CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			BYTE     m_bETLan;
			BYTE     m_bStation;
			BYTE	 m_bRs1;
			BYTE	 m_bRs2;
			BYTE	 m_bRs3;
			BYTE	 m_bRs1R;
			BYTE	 m_bRs2R;
			BYTE	 m_bRs3R;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			};

		// Data Members
		CTcpContext * m_pTcpCtx;
		UINT	      m_uKeep;
		UINT	      m_Source;
		UINT	      m_Port;
		
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Overridables
		void StartFrame(void);
		BOOL SendFrame(void);
		BOOL GetFrame(void);
		BOOL Transact(void);

		// Helpers
		UINT FindETLanOffset(void);
		void AddStationNumbers(void);
		void AddRouteNumbers(void);
		BOOL CheckRoutes(void);
	};

// End of File
