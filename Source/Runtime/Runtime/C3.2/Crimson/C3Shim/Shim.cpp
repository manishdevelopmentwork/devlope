
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Shim Initialization
//

void ShimInit(void)
{
	BeepInit();

	DispInit();

	FRAMInit();

	InputInit();

	KeyInit();

	NICInit();

	TouchInit();

	TrapInit();

	WhoInit();
	}

// End of File
