
#include "intern.hpp"

#include "mtsascii.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA App Master/Network Slave Driver
//

// Instantiator

INSTANTIATE(CMTSDDADriver);

// Constructor

CMTSDDADriver::CMTSDDADriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CMTSDDADriver::~CMTSDDADriver(void)
{
	}

// Configuration

void MCALL CMTSDDADriver::Load(LPCBYTE pData)
{

	}
	
void MCALL CMTSDDADriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CMTSDDADriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CMTSDDAHandler(m_pHelper);

	pPort->Bind(m_pHandler);
	}

void MCALL CMTSDDADriver::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CMTSDDADriver::Open(void)
{
	}

// Device

CCODE MCALL CMTSDDADriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Drop = GetWord(pData);

			m_pHandler->SetDefaultAddress(HIBYTE(m_pCtx->m_Drop));

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMTSDDADriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMTSDDADriver::Ping(void)
{
	return m_pHandler ? 1 : CCODE_ERROR;
	}

CCODE MCALL CMTSDDADriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pHandler == NULL ) return CCODE_ERROR | CCODE_HARD;

	if( Addr.a.m_Table == CSGEN ) *pData = uCount;

	m_pHandler->AccessHandler(Addr, pData, FALSE);

	Sleep(20);

	return Addr.a.m_Table != CSGEN ? 1 : uCount;
	}

CCODE MCALL CMTSDDADriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pHandler == NULL ) return CCODE_ERROR | CCODE_HARD;

	m_pHandler->AccessHandler(Addr, pData, TRUE);

	Sleep(20);

	return Addr.a.m_Table != CSGEN ? 1 : uCount;
	}

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Data Handler
//

// Constructor

CMTSDDAHandler::CMTSDDAHandler(IHelper *pHelper)
{
	StdSetRef();

	m_pHelper	= pHelper;

	m_pPort		= NULL;

	m_fTxEnable	= FALSE;

	m_fTxReady	= FALSE;

	m_fDisableTx	= FALSE;

	m_bNewAddress	= 0;

	m_bRx[POSSTX]	= 0;

	memset( m_Addr, 0, sizeof(m_Addr) );
	memset( m_Out1, 0, sizeof(m_Out1) );
	memset( m_Out2, 0, sizeof(m_Out2) );
	memset( m_TAvg, 0, sizeof(m_TAvg) );
	memset( m_TIDT, 0, sizeof(m_TIDT) );
	memset( m_SGEN, 0, sizeof(m_SGEN) );
	memset( m_Comm, 0, sizeof(m_Comm) );

	UINT i;

	for( i =  0; i < 10; i++ ) m_Hex[i] = '0' + i;
	for( i = 10; i < 16; i++ ) m_Hex[i] = '7' + i;

	m_pHex		= m_Hex;

	CreateEvents();
	}
		
// Destructor

CMTSDDAHandler::~CMTSDDAHandler(void)
{
	m_pRxAppFlag->Release();
	}

// Operations

void CMTSDDAHandler::SetDefaultAddress(BYTE bAddress)
{
	m_Addr[1] = IsValidUnitAddress(bAddress) ? bAddress : ADEF;
	}

void CMTSDDAHandler::AccessHandler(AREF Addr, PDWORD pData, BOOL fIsWrite)
{
	UINT uOffset = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case CADD: // internal read/write only
			if( fIsWrite ) {

				if( IsValidUnitAddress(*pData) && !FindAddress(*pData) ) {

					m_Addr[uOffset] = *pData;
					}
				}

			else *pData = m_Addr[uOffset];

			break;

		case COUT1:
			if( !fIsWrite ) *pData = m_Out1[uOffset];
			break;

		case COUT2:
			if( !fIsWrite ) *pData = m_Out2[uOffset];
			break;

		case CTAVG:
			if( !fIsWrite ) *pData = m_TAvg[uOffset];
			break;

		case CTIDT:
			if( !fIsWrite ) *pData = m_TIDT[uOffset];
			break;

		case CCADD:
			if( fIsWrite ) {

				if( !m_fTxReady ) SetTx(m_Addr[uOffset], LOBYTE(*pData));
				}

			else *pData = m_bNewAddress;
			break;

		case CSGEN:

			if( fIsWrite ) {

				memset(m_SGEN, 0, sizeof(m_SGEN));
				return;
				}

			PDWORD pS;

			UINT uReq;
			UINT uCount;

			pS     = PDWORD(&m_SGEN[uOffset * 4]);
			uReq   = *pData;
			uCount = 0;

			while( uReq-- ) {

				pData[uCount++] = *pS;

				pS++;
				}

			break;

		case CCOMM:
			*pData = m_Comm[uOffset];
			break;
		}
	}

// IUnknown

HRESULT CMTSDDAHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG CMTSDDAHandler::AddRef(void)
{
	StdAddRef();
	}

ULONG CMTSDDAHandler::Release(void)
{
	StdRelease();
	}

// Binding

void MCALL CMTSDDAHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
	}

// Event Initialization

void CMTSDDAHandler::CreateEvents(void)
{
	ISyncHelper *pSync = NULL;

	m_pHelper->MoreHelp(IDH_SYNC, (void **) &pSync);

	m_pRxAppFlag = pSync->CreateAutoEvent();

	pSync->Release();
	}

// Event Handlers

void MCALL CMTSDDAHandler::OnOpen(CSerialConfig const &Config)
{
	}

void MCALL CMTSDDAHandler::OnClose(void)
{
	}

void MCALL CMTSDDAHandler::OnTimer(void)
{
	}

BOOL MCALL CMTSDDAHandler::OnTxData(BYTE &bData)
{
	if( m_fTxEnable ) {

		bData = m_bTx[m_uTxScan++];

//**/		AfxTrace1("[%2.2x]", bData);

		if( m_uTxScan >= 7 ) {

			m_fTxEnable   = FALSE;

			m_bNewAddress = 0;
			}

		return TRUE;
		}

	return FALSE;
	}

void MCALL CMTSDDAHandler::OnTxDone(void)
{
	}

void MCALL CMTSDDAHandler::OnRxData(BYTE bData)
{
	RxByte(bData);
	}

void MCALL CMTSDDAHandler::OnRxDone(void)
{
	}

// Transmit Machine

void CMTSDDAHandler::SetTx(BYTE bOldAddr, BYTE bNewAddr)
{
	if( !IsValidUnitAddress(bOldAddr) || !IsValidUnitAddress(bNewAddr) ) return;

	m_bNewAddress   = bNewAddr;

	m_bTx[POSADD]	= bOldAddr;
	m_bTx[POSCMD]	= 2;
	m_bTx[2]	= SOH;
	m_bTx[3]	= m_pHex[ bNewAddr/100];
	m_bTx[4]	= m_pHex[(bNewAddr/10) % 10];
	m_bTx[5]	= m_pHex[ bNewAddr%10];
	m_bTx[6]	= EOT;

	m_fTxReady  = TRUE;

	if( !m_fDisableTx ) Send();
	}

void CMTSDDAHandler::Send(void)
{
	m_fTxReady  = FALSE;

	if( !IsValidUnitAddress(m_bTx[POSADD]) ) return;

	m_uTxScan   = 1;

	m_fTxEnable = TRUE;

//**/	AfxTrace1("\r\n[%2.2x]", m_bTx[POSADD]);

	m_pPort->Send(m_bTx[POSADD]);
	}

// Receive Machine

void CMTSDDAHandler::RxByte(BYTE bData)
{
//**/	if( IsValidUnitAddress(bData) ) AfxTrace0("\r\n"); AfxTrace1("<%2.2x>", bData);

	m_fDisableTx = TRUE;

	if( IsValidUnitAddress(bData) ) m_uRxState = 0; // reset if new address byte

	switch( m_uRxState ) {

		case 0:
			if( IsValidUnitAddress(UINT(bData)) ) {

				m_uRxPtr   = 1;

				m_bRx[POSADD]   = bData;

				m_bRx[POSSTX]   = 0;

				m_uRxState = 1;
				}

			break;

		case 1:
			m_bRx[m_uRxPtr++] = bData;

			if( bData == ETX ) {

				if( m_bRx[POSSTX] == STX ) ProcessFrame();

				m_bRx[POSSTX]   = 0;

				m_uRxState = 2;
				}

			if( m_uRxPtr >= sizeof(m_bRx) ) m_uRxPtr = 0;

			break;

		case 2: // receive checksum characters
		case 3:
		case 4:
		case 5:
			m_uRxState++;
			break;

		default:
			m_uRxState = 0;

			m_fDisableTx = FALSE;

			if( m_fTxReady ) Send();

			break;
		}
	}

BYTE CMTSDDAHandler::FindAddress(BYTE bAddress)
{
	UINT i = CFGLO;

	while( i <= CFGHI ) {

		if( bAddress == m_Addr[i] ) return i;

		i++;
		}

	return 0;
	}

void CMTSDDAHandler::ProcessFrame(void)
{
	m_pRxAppFlag->Clear();

	BYTE bAdd = FindAddress(m_bRx[POSADD]);

	if( !bAdd ) { // received address is not in the configured list

		FillGenericString();

		return;
		}

	switch( m_bRx[POSCMD] ) {

		case 10:
		case 11:
		case 12:
			StoreData( 1, &m_Out1[bAdd] );
			m_Comm[COMMO1]++;
			return;

		case 13:
		case 14:
		case 15:
			StoreData( 1, &m_Out2[bAdd] );
			m_Comm[COMMO2]++;
			return;

		case 16:
		case 17:
		case 18:
			StoreData( 1, &m_Out1[bAdd] );
			StoreData( 2, &m_Out2[bAdd] );
			m_Comm[COMMO1]++;
			m_Comm[COMMO2]++;
			return;

		case 25:
		case 26:
		case 27:
			StoreData( 1, &m_TAvg[bAdd] );
			m_Comm[COMMTA]++;
			return;

		case 28:
		case 29:
		case 30:
			StoreTemperatures( bAdd, 1 );
			m_Comm[COMMTI]++;
			return;

		case 31:
		case 32:
		case 33:
		case 37:
			StoreData( 1, &m_TAvg[bAdd] );
			m_Comm[COMMTA]++;
			StoreTemperatures( bAdd, 2 );
			m_Comm[COMMTI]++;
			return;

		case 40:
		case 41:
		case 42:
			StoreData( 1, &m_Out1[bAdd] );
			StoreData( 2, &m_TAvg[bAdd] );
			m_Comm[COMMO1]++;
			m_Comm[COMMTA]++;
			return;

		case 43:
		case 44:
		case 45:
			StoreData( 1, &m_Out1[bAdd] );
			StoreData( 2, &m_Out2[bAdd] );
			StoreData( 3, &m_TAvg[bAdd] );
			m_Comm[COMMO1]++;
			m_Comm[COMMO2]++;
			m_Comm[COMMTA]++;
			return;

		default:
			FillGenericString();
			m_Comm[COMMSG]++;
			break;
		}
	}

DWORD CMTSDDAHandler::GetData(UINT uPos)
{
	if( m_bRx[uPos] == 'E' ) { // error followed by code - change to 99<error code>

		m_bRx[2] = '9';
		m_bRx[3] = '9';
		uPos     = 2;
		}

	return ATOF((const char *)&m_bRx[uPos]);
	}

void CMTSDDAHandler::StoreTemperatures(BYTE bAdd, UINT uStart)
{
	BYTE bArrayPos = (5 * bAdd) - 4; // = 1 + ( 5 * (bAdd-1) )

	for( UINT i = 0; i < 5; i++ ) StoreData( uStart + i, &m_TIDT[bArrayPos + i] );
	}

// General Support

BOOL CMTSDDAHandler::IsValidUnitAddress(BYTE bAddress)
{
	return bAddress >= ADEF && bAddress <= AMAX;
	}

void CMTSDDAHandler::StoreData(UINT uField, PDWORD pItem)
{
	UINT uPos;

	if( FindDataPosition( uField, &uPos ) ) *pItem = GetData(uPos);
	}

BOOL CMTSDDAHandler::FindDataPosition(UINT uField, UINT * pPos)
{
	if( uField <= 1 ) {

		*pPos = POSDATA;

		return TRUE;
		}

	UINT uPos = 0;
	uField--;

	while( uPos < sizeof(m_bRx) && m_bRx[uPos] != ETX ) {

		if( m_bRx[uPos] == ':' ) {

			if( !(--uField) ) {

				*pPos = uPos + 1;

				return TRUE;
				}

			}

		uPos++;
		}

	return FALSE;
	}

void CMTSDDAHandler::FillGenericString(void)
{
	UINT SPos  = 0;
	UINT RxPos = 0;

	m_SGEN[SPos++] = '<';

	m_SGEN[SPos++] = m_pHex[ m_bRx[RxPos  ] / 16 ]; // Address (0xC0 - 0xFD)
	m_SGEN[SPos++] = m_pHex[ m_bRx[RxPos++] % 16 ];

	m_SGEN[SPos++] = m_pHex[ m_bRx[RxPos  ] / 16 ]; // Command (0x00...)
	m_SGEN[SPos++] = m_pHex[ m_bRx[RxPos++] % 16 ];

	m_SGEN[SPos++] = m_pHex[ m_bRx[RxPos  ] / 16 ]; // STX
	m_SGEN[SPos++] = m_pHex[ m_bRx[RxPos++] % 16 ];

	m_SGEN[SPos++] = '>';

	BOOL fETX = FALSE;

	while( SPos < sizeof( m_SGEN ) ) {

		BYTE b;

		b = m_bRx[RxPos++];

		fETX = fETX | (b == ETX); 

		m_SGEN[SPos++] = !fETX ? b : 0;
		}

	m_SGEN[GENSTR-1] = 0;
	}

// End of File
