
#include "intern.hpp"

#include "legacy.h"

#include "out4mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Analog Output Module
//

// Dynamic Class

AfxImplementDynamicClass(COut4Module, CGenericModule);


// Constructor

COut4Module::COut4Module(void)
{
	m_pConf  = New COut4Conf;

	m_FirmID = FIRM_OUT4;

	m_Ident  = ID_CSOUT4;

	m_Model  = "CSOUT4";

	m_Conv.Insert(L"Graphite", L"CGMOut4Module");

	m_Conv.Insert(L"Manticore", L"CDAAO8Module");
	}

// UI Management

CViewWnd * COut4Module::CreateMainView(void)
{
	return New COut4MainWnd;
	}

// Comms Object Access

UINT COut4Module::GetObjectCount(void)
{
	return 1;
	}

BOOL COut4Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;

			Data.Name  = CString(IDS_NAME_OUT);

			Data.pItem = m_pConf;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL COut4Module::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pConf->Convert(pValue->GetChild(L"Conf"));

		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void COut4Module::AddMetaData(void)
{
	Meta_AddObject(Conf);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Analog Output Module Window
//

// Runtime Class

AfxImplementRuntimeClass(COut4MainWnd, CProxyViewWnd);

// Constructor

COut4MainWnd::COut4MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void COut4MainWnd::OnAttach(void)
{
	m_pItem = (COut4Module *) CProxyViewWnd::m_pItem;

	AddConfig();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void COut4MainWnd::AddConfig(void)
{
	COut4Conf *pConf = m_pItem->m_pConf;

	for( UINT n = 0; n < pConf->GetPageCount(); n++ ) {

		CViewWnd *pPage = pConf->CreatePage(n);

		m_pMult->AddView(pConf->GetPageName(n), pPage);

		pPage->Attach(pConf);
		}
	}

// End of File
