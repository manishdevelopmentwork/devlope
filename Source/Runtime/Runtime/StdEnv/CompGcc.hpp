
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_CompGcc_HPP

#define INCLUDE_CompGcc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Compiler Flag
//

#define AEON_COMP_GCC

//////////////////////////////////////////////////////////////////////////
//
// What Else Goes In Here?
//

// Structure packing?
// Function attributes?

//////////////////////////////////////////////////////////////////////////
//
// Assembly Code
//

#define	ASM __asm volatile

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#if !defined(__INTELLISENSE__) && __GNUC__ > 4 && 0 // !!!

#define	Min(a, b) ((a) <? (b))

#define	Max(a, b) ((a) >? (b))

#else

#define	Min(a, b) ((a) < (b) ? (a) : (b))

#define	Max(a, b) ((a) > (b) ? (a) : (b))

#endif

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

AfxDefineType(INT64,  long long int);

AfxDefineType(UINT64, unsigned long long int);

AfxDefineType(SINT64, signed long long int);

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Support
//

#define METHOD

//////////////////////////////////////////////////////////////////////////
//
// Variable Argument Lists
//

#if defined(__INTELLISENSE__)

typedef char * va_list;

#define va_start(a, b)

#define va_end(a)

#define va_arg(a, t) ((t) 0)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Inline Expansion
//

#if !defined(__INTELLISENSE__)

#define	INLINE		__inline

#define	STRONG_INLINE	__attribute__ ((always_inline)) __inline

#else

#define	INLINE		__inline

#define	STRONG_INLINE	__inline

#endif

//////////////////////////////////////////////////////////////////////////
//
// Alignment
//

#if !defined(__INTELLISENSE__)

#define	ALIGN(x)	__attribute__ ((aligned(x)))

#else

#define	ALIGN(x)	/**/

#endif

//////////////////////////////////////////////////////////////////////////
//
// Likely and Unlikely Macros
//

#if !defined(__INTELLISENSE__)

#define likely(x)       __builtin_expect(bool((x)),1)

#define unlikely(x)     __builtin_expect(bool((x)),0)

#else

#define likely(x)	(x)

#define unlikely(x)	(x)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Assume Macro
//

#if !defined(__INTELLISENSE__)

#define __assume(x)	BEGIN { if (!(x)) __builtin_unreachable(); } END

#else

#define __assume(x)	((void) 0)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Dummy Assertion Macros
//

#if !defined(_DEBUG)

#define AfxAssert(x)	BEGIN { __assume(x); ((void) (0)); } END

#define	AfxVerify(x)	BEGIN { __assume(x); ((void) (x)); } END

#endif

//////////////////////////////////////////////////////////////////////////
//
// Other Built-Ins
//

#if !defined(__INTELLISENSE__)

#define BuiltInClz(n) ((n) ? __builtin_clzl(long((n))) : 32)

#else

#define BuiltInClz(n)	0

#define alloca(n)	((void *) 4)

#endif

//////////////////////////////////////////////////////////////////////////
//
// Atomic Operations
//

#if !defined(__INTELLISENSE__)

#define AtomicCompAndSwap(pv, ov, nv) __sync_val_compare_and_swap(pv, ov, nv)

#define AtomicFetchAndSub(pv, n)      __sync_fetch_and_sub(pv, n)

#define AtomicFetchAndAdd(pv, n)      __sync_fetch_and_add(pv, n)

#define AtomicIncrement(pv)	      (AtomicFetchAndAdd(pv, 1) + 1)
				      
#define AtomicDecrement(pv)	      (AtomicFetchAndSub(pv, 1) - 1)

#define AtomicBitSet(pv, n)           __sync_or_and_fetch(pv, (1 << n))

#define AtomicBitClr(pv, n)           __sync_and_and_fetch(pv, ~(1 << n))

#else

#define AtomicCompAndSwap(pv, ov, nv) (*pv)

#define AtomicFetchAndSub(pv, n)      ((*pv)++)

#define AtomicFetchAndAdd(pv, n)      ((*pv)--)

#define AtomicIncrement(pv)	      (++(*pv))
				      
#define AtomicDecrement(pv)	      (--(*pv))

#define AtomicBitSet(pv, n)	      (*pv |= (1<<n))

#define AtomicBitClr(pv, n)           (*pv &= ~(1<<n))

#endif

// End of File

#endif
