
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Tree Window for Items with Dotted Names
//

// Runtime Class

AfxImplementRuntimeClass(CDotTreeWnd, CStdTreeWnd);

// Constructor

CDotTreeWnd::CDotTreeWnd(void)
{
	}

// IUnknown

HRESULT CDotTreeWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return CStdTreeWnd::QueryInterface(iid, ppObject);
	}

ULONG CDotTreeWnd::AddRef(void)
{
	return CStdTreeWnd::AddRef();
	}

ULONG CDotTreeWnd::Release(void)
{
	return CStdTreeWnd::Release();
	}

// IUpdate

HRESULT CDotTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateRename ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			CMetaItem *pMeta = (CMetaItem *) pItem;

			m_pTree->SetItemText(hItem, GetName(pMeta->GetName()));
			}
		}

	if( uType == updateProps ) {

		HTREEITEM hItem = m_MapFixed[pItem->GetFixedPath()];

		if( hItem ) {

			if( !HasFolderImage(hItem) ) {

				CMetaItem *pMeta = GetItemPtr(hItem);

				CTreeViewItem Item(hItem, TVIF_IMAGE);

				m_pTree->GetItem(Item);

				Item.SetImages(GetItemImage(pMeta));

				m_pTree->SetItem(Item);
				}
			}

		return S_OK;
		}

	return S_OK;
	}

// Overridables

void CDotTreeWnd::OnAttach(void)
{
	CStdTreeWnd::OnAttach();

	CMetaData const *pMeta = m_pItem->FindMetaData(m_List);

	if( pMeta ) {

		CItem *pList = pMeta->GetObject(m_pItem);

		if( pList->IsKindOf(AfxRuntimeClass(CItemIndexList)) ) {

			m_pList = (CItemIndexList *) pList;

			return;
			}
		}

	AfxAssert(FALSE);
	}

// Tree Loading

void CDotTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TreeIcon16"), afxColor(MAGENTA));
	}

void CDotTreeWnd::LoadTree(void)
{
	afxThread->SetWaitMode(TRUE);

	afxThread->SetStatusText(CString(IDS_LOADING_VIEW));

	LoadRoot();

	UINT uTotal = m_pList->GetItemCount();

	UINT uCount = 0;

	for( UINT uPass = 0; uCount < uTotal; uPass++ ) {

		INDEX Index = m_pList->GetHead();

		while( !m_pList->Failed(Index) ) {

			CMetaItem *pItem = (CMetaItem *) m_pList->GetItem(Index);

			CString    Name  = pItem->GetName();
			
			if( Name.Count('.') == uPass ) {

				if( IncludeItem(pItem) ) {

					PCTXT pName = Name;

					UINT  uPos  = 0;

					if( uPass ) {

						for( UINT c = 0; pName[uPos]; uPos++ ) {

							if( pName[uPos] == L'.' ) {

								if( ++c == uPass ) {

									break;
									}
								}
							}
						}

					CString Path = Name.Left(uPos+0);

					CTreeViewItem Node;

					LoadNodeItem(Node, pItem);

					HTREEITEM hRoot = m_MapNames[Path];

					HTREEITEM hItem = m_pTree->InsertItem(hRoot, NULL, Node);

					AddToMaps(pItem, hItem);
					}

				uCount++;
				}

			m_pList->GetNext(Index);
			}
		}

	SortTree();

	afxThread->SetStatusText(L"");

	afxThread->SetWaitMode(FALSE);

	SelectRoot();
	}

void CDotTreeWnd::LoadRoot(void)
{
	CTreeViewItem Root;
	
	LoadRootItem(Root);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Root);

	m_MapNames.Insert(L"", m_hRoot);

	m_MapFixed.Insert(m_pItem->GetFixedPath(), m_hRoot);
	}

void CDotTreeWnd::LoadRootItem(CTreeViewItem &Root)
{
	Root.SetText (m_pItem->GetHumanName());

	Root.SetParam(LPARAM(m_pItem));

	UINT uImage = GetRootImage();

	Root.SetImages(uImage);
	}

void CDotTreeWnd::LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem)
{
	Node.SetText  (GetName(GetItemText(pItem)));

	Node.SetParam (LPARAM(pItem));

	UINT uImage = GetItemImage(pItem);

	Node.SetImages(uImage);
	}

// Item Hooks

BOOL CDotTreeWnd::IncludeItem(CMetaItem *pItem)
{
	return TRUE;
	}

UINT CDotTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return pItem->IsKindOf(m_Folder) ? 2 : 4;
	}

BOOL CDotTreeWnd::HasFolderImage(HTREEITEM hItem)
{
	return IsFolder(hItem);
	}

void CDotTreeWnd::KillItem(HTREEITEM hItem, BOOL fRemove)
{
	if( fRemove ) {

		if( hItem == m_hRoot ) {

			m_MapNames.Empty();

			m_MapFixed.Empty();

			return;
			}

		INDEX Index1 = m_MapFixed.FindData(hItem);

		INDEX Index2 = m_MapNames.FindData(hItem);

		AfxAssert(!m_MapFixed.Failed(Index1));

		AfxAssert(!m_MapNames.Failed(Index2));

		m_MapFixed.Remove(Index1);

		m_MapNames.Remove(Index2);
		}
	}

// Selection Hooks

CString CDotTreeWnd::SaveSelState(void)
{
	// LATER -- Encode expand state of children?

	// LATER -- Encode multiple selections?

	return m_SelPath;
	}

void CDotTreeWnd::LoadSelState(CString State)
{
	HTREEITEM hItem = m_MapFixed[State];

	if( hItem ) {

		m_hSelect = NULL;

		m_pTree->SelectItem(NULL);

		m_pTree->SelectItem(hItem);

		return;
		}

	m_hSelect = NULL;

	m_pTree->SelectItem(NULL);

	SelectRoot();
	}

// Name Parsing

CString CDotTreeWnd::GetName(CString Path)
{
	UINT uPos = Path.FindRev('.');

	return Path.Mid(uPos + 1);
	}

CString CDotTreeWnd::GetRoot(CString Path, BOOL fDot)
{
	UINT    uPos = Path.FindRev('.');

	CString Root = Path.Left(uPos + 1);

	if( !fDot ) {

		Root = Root.Left(Root.GetLength() - 1);
		}
	
	return Root;
	}

CString CDotTreeWnd::GetRoot(CItem *pItem, BOOL fDot)
{
	if( pItem->IsKindOf(m_Folder) ) {

		CMetaItem *pNamed = (CMetaItem *) pItem;

		if( fDot ) {

			return pNamed->GetName() + L'.';
			}

		return pNamed->GetName();
		}

	if( pItem->IsKindOf(m_Class) ) {

		CMetaItem *pNamed = (CMetaItem *) pItem;

		return GetRoot(pNamed->GetName(), fDot);
		}

	return L"";
	}

CString CDotTreeWnd::GetRoot(HTREEITEM hItem, BOOL fDot)
{
	return GetRoot((CItem *) m_pTree->GetItemParam(hItem), fDot);
	}

CString CDotTreeWnd::GetRoot(BOOL fDot)
{
	return GetRoot(m_pSelect, fDot);
	}

// Implementation

BOOL CDotTreeWnd::IsFolder(void)
{
	return IsFolder(m_hSelect);
	}

BOOL CDotTreeWnd::IsFolder(HTREEITEM hItem)
{
	CItem *pItem = (CItem *) m_pTree->GetItemParam(hItem);

	return pItem->IsKindOf(m_Folder);
	}

void CDotTreeWnd::AddToMaps(CMetaItem *pItem, HTREEITEM hItem)
{
	BOOL fTest1 = m_MapNames.Insert(pItem->GetName(), hItem);

	BOOL fTest2 = m_MapFixed.Insert(pItem->GetFixedPath(), hItem);

	AfxAssert(fTest1 && fTest2);
	}

void CDotTreeWnd::RemoveFromMaps(CMetaItem *pItem)
{
	m_MapNames.Remove(pItem->GetName());

	m_MapFixed.Remove(pItem->GetFixedPath());
	}

// End of File
