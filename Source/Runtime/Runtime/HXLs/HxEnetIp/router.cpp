/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ROUTER.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Message Router object sends requests to the appropriate objects for handling
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

/* Supported by Message Router Class objects */
#if defined( ET_IP_SCANNER )
UINT16	routerObjects[ROUTER_OBJECT_COUNT] = { ID_CLASS, ROUTER_CLASS, ASSEMBLY_CLASS, CONNMGR_CLASS, OUTPOINT_CLASS, CONFIG_CLASS, PORT_CLASS, TCPIP_CLASS, ENETLINK_CLASS };
#else 
UINT16	routerObjects[ROUTER_OBJECT_COUNT] = { ID_CLASS, ROUTER_CLASS, ASSEMBLY_CLASS, CONNMGR_CLASS, OUTPOINT_CLASS, PORT_CLASS, TCPIP_CLASS, ENETLINK_CLASS  };
#endif

CPF_SOCKADDR_TAG gtoTag;		/* Store Target->Originator address tag that came with the UCMM or Class3 message */
CPF_SOCKADDR_TAG gotTag;		/* Store Originator->Target address tag that came with the UCMM or Class3 message */


/*---------------------------------------------------------------------------
** routerDataTransferReceived( )
**
** Message Router parses TCP message and directs the request to the 
** appropriate objects for processing
**---------------------------------------------------------------------------
*/

void  routerDataTransferReceived( INT32 nSession )
{
   ENCAPH*          pHdr = ( ENCAPH * )gmsgBuf;
   UINT16           len, tag_len;   
   CPF_TAG          tag;
   UINT8*           pData;   
   INT32            i;   
   ENCAP_DT_HDR     dataHdr;
   UINT16           iObjectCount;
   CPF_ADR_TAG      addrTag;
   CPF_PKT_TAG      pktTag;   
   INT32            nConnection;
   INT32            nFromSession;   
//   UINT8            bGeneralStatus = 0;
//   UINT16           iExtendedStatus = 0;
//   UINT8            bClass3DataChanged = FALSE;
   BOOL             bRequestRemove = FALSE;
#ifdef ET_IP_SCANNER
   UINT16           iDataSize;
   UINT8*			pDataStart;
   INT32			j;
#endif

   memset( &gotTag,  0, CPF_SOCKADDR_TAG_SIZE );
   memset( &gtoTag,  0, CPF_SOCKADDR_TAG_SIZE );
   memset( &addrTag, 0, CPF_ADR_TAG_SIZE      );
   memset( &pktTag,  0, CPF_PKT_TAG_SIZE      );
   
   pData = &gmsgBuf[sizeof(ENCAPH)];			/* Point right outside the header */
	
   if ( pHdr->lStatus != ENCAP_E_SUCCESS )		/* Status other than 0 indicates failure */
   {
	   DumpStr1("routerDataTransferReceived Status is 0x%x and not 0", pHdr->lStatus);

	   /* Check if there is a request waiting for response for this session, and if yes, mark as failed */
	   for( i = 0; i < gnRequests; i++ )		/* See if it's a reply to our UCMM request */
	   {
	   	   if ( gRequests[i].nType == ObjectRequest && !gRequests[i].bIncoming && gRequests[i].nState == RequestWaitingForResponse )
		   {
		   	    if ( gRequests[i].lIPAddress == gSessions[nSession].sClientAddr.sin_addr.s_addr )
				{
					/* Modify the request object appropriately */					
					gRequests[i].bGeneralError = ROUTER_ERROR_ENCAP_PROTOCOL;					
					gRequests[i].iDataSize = 0; 
					requestResponseReceived( i );							
					break;
				}
		   }
	   }			

	   return;
   }
   
   len = pHdr->iLength;

   if ( len < ENCAP_DT_HDR_SIZE )				/* Data header is required */
   {
	    DumpStr0("routerDataTransferReceived invalid packet length");
	    ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
        return;
   }

   /*
   ** Grab the parameters from the data transfer header. Adjust the
   ** packet pointer and the remaining length to account for the values
   ** that were just read.
   */
   memcpy( &dataHdr, pData, ENCAP_DT_HDR_SIZE );	/* Read the data header */
   ENCAP_CVT_HS( dataHdr.iTimeout );
       
   pData += ENCAP_DT_HDR_SIZE;						/* Point beyond the data header */
   len -= ENCAP_DT_HDR_SIZE;						/* Adjust the length */

   /*
   ** Next, check for a standard object header which should precede the
   ** individual header tags.
   */
   if ( len < sizeof(UINT16) )
   {
	  DumpStr0("routerDataTransferReceived invalid length specified in the header");
	  ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
      return;
   }

   iObjectCount = UINT16_GET( pData );				/* Number of tags (objects) to follow */
      
   pData += sizeof(UINT16);							/* Point to the data beyond Object count */
   len -= sizeof(UINT16);							/* Remove the object length from the total */
   								 
   /*
   ** Parse the tags in the packet. Convert the standard tag header to
   ** host byte order. 
   */
   for ( i = 0; i < iObjectCount; i++ )
   {
      /*
      ** Copy the tag header into a place where it can be converted into
      ** host standard byte order.  Then decode the tag to find out where
      ** it belongs.  Make sure that the tag length doesn't exceed the
      ** amount of data remaining in the packet.
      */
      if ( len < CPF_TAG_SIZE )
      {
          DumpStr0("routerDataTransferReceived invalid packet length");  
		  ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
          return;
      }

      memcpy( &tag, pData, CPF_TAG_SIZE );
      ENCAP_CVT_HS( tag.iTag_type );
      ENCAP_CVT_HS( tag.iTag_length );

      tag_len = tag.iTag_length;

      /* Adjust the data length and data pointer to account for the tag header */
      len -= CPF_TAG_SIZE;
      pData += CPF_TAG_SIZE;

      /* Does the tag length specify more data than the packet contains? */
      if ( tag_len > len )
      {
         ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
         return;
      }
      	  
	  /* Switch out on the TAG types */
      switch ( tag.iTag_type )
      {
         case CPF_TAG_ADR_NULL:				/* Destination address - store in addrTag */
         case CPF_TAG_ADR_LOCAL:
         case CPF_TAG_ADR_OFFLINK:
         case CPF_TAG_ADR_CONNECTED:

            /* The length of the tag can not exceed the storage reserved for it */
            if ( tag_len > sizeof(addrTag.data) )
            {
				DumpStr0("routerDataTransferReceived The length of the tag exceeds the storage reserved for it");  
				ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
				return;
			}

            memcpy( &addrTag.sTag, &tag, CPF_TAG_SIZE );
            if ( tag_len )
            {
                memcpy( &addrTag.data.g, pData, tag_len );
                pData += tag_len;
                len -= tag_len;
            }              
            break;

         case CPF_TAG_PKT_PCCC:				/* The packet data - store in pktTag */
         case CPF_TAG_PKT_UCMM:
         case CPF_TAG_PKT_CONNECTED:
			
            memcpy( &pktTag.sTag, &tag, CPF_TAG_SIZE );
			pktTag.pPacketPtr = pData;

			pData += tag_len;
            len -= tag_len;               
            break;

         case CPF_TAG_SOCKADDR_OT:	/* Originator -> Target socket address */

            if ( tag_len != sizeof(struct sockaddr_in) )                  
            {
				DumpStr0("routerDataTransferReceived the tag length is invalid");  
				ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
				return;
			}

            memcpy( &gotTag.sTag, &tag, CPF_TAG_SIZE );
            memcpy( &gotTag.sAddr, pData, tag_len );
            pData += tag_len;
            len -= tag_len;
            break;

         case CPF_TAG_SOCKADDR_TO:	/* Target -> Originator socket address */

            if ( tag_len != sizeof(struct sockaddr_in) )                  
			{
				DumpStr0("routerDataTransferReceived the tag length is invalid");  
				ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
				return;
			}

            memcpy( &gtoTag.sTag, &tag, CPF_TAG_SIZE );
            memcpy( &gtoTag.sAddr, pData, tag_len );
            pData += tag_len;
            len -= tag_len;
            break;

         default:

		    /* Return an error on anything we don't know about */
			DumpStr0("routerDataTransferReceived the tag type is invalid");  
			ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
			return;                     
	  }
   }

   if ( len )	/* Should not be anything else in the packet */
   {
	  DumpStr0("routerDataTransferReceived unexpected extra data");  
	  ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
      return;            
   }

   if ( pktTag.sTag.iTag_length > MAX_REQUEST_DATA_SIZE )	/* Packet data should not exceed the limit */
   {
	  DumpStr0("routerDataTransferReceived packet is too long");  
      ucmmErrorReply( nSession, ENCAP_E_BAD_LENGTH );  
      return;            
   }

   /* Check if this is a UCMM or Class3 message */
   if ( ( dataHdr.lTarget == INTERFACE_HANDLE ) &&
        ( ( CPF_TAG_PKT_CONNECTED == pktTag.sTag.iTag_type ) ||
          ( CPF_TAG_PKT_UCMM == pktTag.sTag.iTag_type ) ) )
   {
       if ( ( ( pHdr->iCommand == ENCAP_CMD_SEND_RRDATA ) &&	/* This is UCMM */
              ( pktTag.sTag.iTag_type == CPF_TAG_PKT_UCMM ) &&
              ( addrTag.sTag.iTag_type == CPF_TAG_ADR_NULL ) ) 
	        ||
			  ( ( pHdr->iCommand == ENCAP_CMD_SEND_UNITDATA ) &&	/* This is Class 3 message */
			  ( pktTag.sTag.iTag_type == CPF_TAG_PKT_CONNECTED ) &&
              ( addrTag.sTag.iTag_type == CPF_TAG_ADR_CONNECTED ) ) )
	   {
		  if( pktTag.sTag.iTag_type == CPF_TAG_PKT_UCMM )
		  {				  
			  pktTag.bConnected = FALSE;			  
			  
#ifdef ET_IP_SCANNER			  
			  if ( *pktTag.pPacketPtr == (FWD_OPEN_CMD_CODE | REPLY_BIT_MASK) )	/* This is a Forward Open reply */
			  {
				  scanmgrProcessFwdOpenReply( nSession, pktTag.pPacketPtr, &gotTag, &gtoTag );		
				  bRequestRemove = TRUE;
			  }
			  else if ( *pktTag.pPacketPtr == (FWD_CLOSE_CMD_CODE | REPLY_BIT_MASK) )	/* This is a Forward Open reply */
			  {
				  scanmgrProcessFwdCloseReply( nSession, pktTag.pPacketPtr );		
				  bRequestRemove = TRUE;
			  }			  
#endif /* #ifdef ET_IP_SCANNER	*/  
			  if ( (*pktTag.pPacketPtr) & REPLY_BIT_MASK )		/* This is some reply */
			  {
				for( i = 0; i < gnRequests; i++ )		/* See if it's a reply to our UCMM request */
				{
					if ( gRequests[i].nType == ObjectRequest && !gRequests[i].bIncoming && gRequests[i].nState == RequestWaitingForResponse )
					{
						if ( gRequests[i].lIPAddress == gSessions[nSession].sClientAddr.sin_addr.s_addr && 
							 (gRequests[i].bService|REPLY_BIT_MASK) == *pktTag.pPacketPtr )
						{
							if ( bRequestRemove )
								requestRemove( i );
							else
							{
								/* Store the response in the request object */
								gRequests[i].bGeneralError = *(pktTag.pPacketPtr+2);	/* Store general status. Should be 0 in case of success */
								utilResetMemoryPoolOffset( &gRequests[i].iDataOffset, &gRequests[i].iDataSize, 
									(UINT8*)(pktTag.pPacketPtr+sizeof(REPLY_HEADER)), (UINT16)(pktTag.sTag.iTag_length-sizeof(REPLY_HEADER)) );										
								if ( gRequests[i].iDataSize && gRequests[i].iDataOffset == INVALID_MEMORY_OFFSET )
								{
									notifyEvent( NM_OUT_OF_MEMORY, 0 );
									requestRemove( i );	/* Out of memory */
									break;
								}
								requestResponseReceived( i );							
								break;
							}
						}
					}
					else if ( gRequests[i].nType == UnconnectedSendRequest && gRequests[i].nState == RequestWaitingForResponse )
					{
						if ( gRequests[i].lIPAddress == gSessions[nSession].sClientAddr.sin_addr.s_addr && 
							 ( ( (gRequests[i].bService|REPLY_BIT_MASK) == *pktTag.pPacketPtr ) ||
							   ( (UNCONNECTED_SEND_CMD_CODE|REPLY_BIT_MASK) == *pktTag.pPacketPtr ) ) )
						{
							/* If Unconnected Send was originated by the API call */ 
							if ( !gRequests[i].bIncoming )
							{
								/* Store the response in the request object */
								gRequests[i].bGeneralError = *(pktTag.pPacketPtr+2);	/* Store general status. Should be 0 in case of success */
								utilResetMemoryPoolOffset( &gRequests[i].iDataOffset, &gRequests[i].iDataSize, 
									(UINT8*)(pktTag.pPacketPtr+sizeof(REPLY_HEADER)), (UINT16)(pktTag.sTag.iTag_length-sizeof(REPLY_HEADER)) );																		
								if ( gRequests[i].iDataSize && gRequests[i].iDataOffset == INVALID_MEMORY_OFFSET )
								{
									notifyEvent( NM_OUT_OF_MEMORY, 0 );
									requestRemove( i );	/* Out of memory */
									break;
								}
							
								requestResponseReceived( i );							
								break;
							}
							else	/* If we received this Unconnected Send from somebody else and were resending it */
							{
								nFromSession = sessionFindAddressEstablished( gRequests[i].lFromIPAddress, Incoming );
								if ( nFromSession != INVALID_SESSION )
								{
									if ( socketEncapSendData( nFromSession ) > 0 )
										requestRemove(i);
									i--;
								}
								break;
							}
						}
					}
				}				
			  }
			  else		/* This is a UCMM request to us */
			    routerParseObjectRequest(nSession, &pktTag, pHdr->lContext1, pHdr->lContext2);
		  }
		  else if( pktTag.sTag.iTag_type == CPF_TAG_PKT_CONNECTED )	/* This is a Class 3 message */
		  {	
			 if ( addrTag.sTag.iTag_length == sizeof(UINT32) )
			 {
				ENCAP_CVT_HL( addrTag.data.sC.lCid );
				/* Find the Class 3 connection this message belongs to */
				for( nConnection = 0; nConnection < gnConnections; nConnection++ )
				{
					if ( gConnections[nConnection].lConnectionState == ConnectionEstablished && 
						 gConnections[nConnection].cfg.bTransportClass == Class3 &&
						 addrTag.data.sC.lCid == gConnections[nConnection].lConsumingCID )
						break;
				}

				if ( nConnection < gnConnections )	/* Class 3 connection found */
				{
					pktTag.bConnected = TRUE;		
					pktTag.nConnection = nConnection;
					pktTag.iConnSequence = UINT16_GET( pktTag.pPacketPtr );
					pktTag.sTag.iTag_length -= sizeof(UINT16);
					pktTag.pPacketPtr += sizeof(UINT16);

					/* For Class 3 request reset the last received data timestamp */
					connectionResetConsumingTicks( nConnection );
			
					if ( (*pktTag.pPacketPtr) & REPLY_BIT_MASK ) /* This is a reply to our Class 3 request */
					{
#ifdef ET_IP_SCANNER						
						if ( (gConnections[nConnection].cfg.bService|REPLY_BIT_MASK) == *pktTag.pPacketPtr )
						{
							bGeneralStatus = *(pktTag.pPacketPtr+2);
							
							iDataSize = pktTag.sTag.iTag_length - sizeof(REPLY_HEADER);
							pDataStart = pktTag.pPacketPtr + sizeof(REPLY_HEADER); 
							
							if ( bGeneralStatus )
							{
								if ( iDataSize >= sizeof(UINT16) )
								{
									iExtendedStatus = UINT16_GET( pDataStart );
								}
							}
							
							if ( ( bGeneralStatus != gConnections[i].bGeneralStatus ) ||
								 ( iExtendedStatus != gConnections[i].iExtendedStatus ) ||
								 ( iDataSize !=  gConnections[i].cfg.iInputDataSize ) )
									bClass3DataChanged = TRUE;
							else
							{
								for( j = 0; j < iDataSize; j++ )
								{
									if ( pDataStart[j] != inputAssembly[gConnections[j].cfg.iInputDataOffset+j] )
									{
										bClass3DataChanged = TRUE;
										break;
									}
								}
							}
							
							if ( !bGeneralStatus && bClass3DataChanged )
							{
								/* Store Class 3 response results in the Assembly */
								assemblySetConnectionInputData( nConnection, pDataStart, iDataSize );	/* Store data in assembly object */	
							}

							gConnections[i].bGeneralStatus = bGeneralStatus;	/* Store general status. Should be 0 in case of success */
							gConnections[i].iExtendedStatus = iExtendedStatus;	/* Store extended status. */
							gConnections[i].cfg.iInputDataSize = iDataSize;		/* Store the actual input data size */
							
							if ( bClass3DataChanged )
							{
								DumpStr0("routerDataTransferReceived NM_CONNECTION_NEW_INPUT_DATA");
								notifyEvent( NM_CONNECTION_NEW_INPUT_DATA, gConnections[nConnection].cfg.nInstance );
							}
						}				
#endif /* #ifdef ET_IP_SCANNER	*/
					}
					else	/* This is a Class 3 request directed to us */
						routerParseObjectRequest(nSession, &pktTag, pHdr->lContext1, pHdr->lContext2);	
				}
				else	/* Could not match a connection */
					ucmmSendServiceErrorResponse( nSession, *pktTag.pPacketPtr, ROUTER_ERROR_FAILURE, ROUTER_EXT_ERR_CONNECTION_NOT_FOUND );
			 }
		  }
	   }
	   else		/* Did not recognize the packet structure */
	   {
		  DumpStr0("routerDataTransferReceived unknown packet structure");  
		  ucmmErrorReply( nSession, ENCAP_E_BADDATA );  
		  return;            
	   }
   }          
}


/*---------------------------------------------------------------------------
** routerParseObjectRequest( )
**
** Parse UCMM or Class 3 request 
**---------------------------------------------------------------------------
*/

void  routerParseObjectRequest( INT32 nSession, CPF_PKT_TAG* pPacketTag, UINT32 lContext1, UINT32 lContext2  )
{	    
	EPATH			pdu;
	INT32			nPDUSize;		
	INT32			nRequest;
	UINT8           bService;
	
	bService = *pPacketTag->pPacketPtr;

	/* Parse the next part of the packet that must be Router PDU (Protocol Data Unit) */
	nPDUSize = routerParsePDU( pPacketTag->pPacketPtr, &pdu );

	/* Continue if we got the Class and Instance or a symbolic Tag */
	if ( nPDUSize < ROUTER_ERROR_BASE )		 
	{		
		if ( pdu.iFilled & EPATH_CLASS || pdu.iFilled & EPATH_INSTANCE || 
			 pdu.iFilled & EPATH_ATTRIBUTE || pdu.iFilled & EPATH_EXT_SYMBOL )
		{			
			nRequest = requestNew( ObjectRequest, TRUE, TRUE );		/* Register new incoming object request */
	
			if ( nRequest == ERROR_STATUS )						/* No more space for requests */
			{
				notifyEvent( NM_PENDING_REQUESTS_LIMIT_REACHED, 0 );
				return;		
			}

			gRequests[nRequest].lIPAddress = gSessions[nSession].sClientAddr.sin_addr.s_addr;			
									
			gRequests[nRequest].bService = bService;
			gRequests[nRequest].iClass = (pdu.iFilled & EPATH_CLASS) ? pdu.iClass : INVALID_CLASS;
			gRequests[nRequest].iInstance = (pdu.iFilled & EPATH_INSTANCE) ? pdu.iInstance : INVALID_INSTANCE;
			gRequests[nRequest].iAttribute = ( pdu.iFilled & EPATH_ATTRIBUTE ) ? pdu.bAttribute : INVALID_ATTRIBUTE;
			gRequests[nRequest].iMember = ( pdu.iFilled & EPATH_MEMBER ) ? pdu.iMember : INVALID_MEMBER;
			gRequests[nRequest].lRequestTimeoutTick = platformGetTickCount() + DEFAULT_TIMEOUT;	/* Set the request timeout tick */
			if ( pdu.iFilled & EPATH_EXT_SYMBOL )
			{
				gRequests[nRequest].iTagSize = pdu.bExtSymbolSize;
				gRequests[nRequest].iTagOffset = utilAddToMemoryPool( pdu.bExtSymbol, pdu.bExtSymbolSize );
			}
			else
			{
				gRequests[nRequest].iTagSize = 0;
				gRequests[nRequest].iTagOffset = INVALID_MEMORY_OFFSET;
			}
			gRequests[nRequest].iDataSize = (UINT16)(pPacketTag->sTag.iTag_length - PDU_HDR_SIZE - nPDUSize);
			gRequests[nRequest].iDataOffset = utilAddToMemoryPool( &pPacketTag->pPacketPtr[PDU_HDR_SIZE + nPDUSize], 
				gRequests[nRequest].iDataSize );

			if ( (gRequests[nRequest].iDataSize && gRequests[nRequest].iDataOffset == INVALID_MEMORY_OFFSET) ||
				 (gRequests[nRequest].iTagSize && gRequests[nRequest].iTagOffset == INVALID_MEMORY_OFFSET) )
			{
				notifyEvent( NM_OUT_OF_MEMORY, 0 );
				requestRemove( nRequest );	/* Out of memory */
				return;
			}

			if ( pPacketTag->bConnected )
			{
				gRequests[nRequest].iConnectionSerialNbr = gConnections[pPacketTag->nConnection].iConnectionSerialNbr;
				gRequests[nRequest].iConnSequence = pPacketTag->iConnSequence;
			}
			
			gRequests[nRequest].lContext1 = lContext1;		/* Save the senders's context to send it back with the response */  
			gRequests[nRequest].lContext2 = lContext2;  
						
			gRequests[nRequest].nState = RequestLogged;					
		}
		else	/* Request was not created yet, so just reply with an error based on the session and the service code */
		{
			ucmmSendServiceErrorResponse( nSession, bService, ROUTER_ERROR_INVALID_SEG_TYPE, 0 );
			return;
		}
	}
	else	/* Offset of the routerParsePDU() return value from the ROUTER_ERROR_BASE indicates the error found when parsing an EPATH */
		ucmmSendServiceErrorResponse( nSession, bService, (BYTE)(nPDUSize - ROUTER_ERROR_BASE), 0 );
}


/*---------------------------------------------------------------------------
** routerProcessObjectRequest( )
**
** Process UCMM or Class 3 request 
**---------------------------------------------------------------------------
*/

void routerProcessObjectRequest( INT32 nRequest )
{	    
	INT32	i, nIndex;

	if ( gRequests[nRequest].nType != ObjectRequest ) 
		return;	
	
	for ( i = 0; i < (INT32)glClientAppProcClassNbr; i++ )
	{
		if ( gpClientAppProcClasses[i] == gRequests[nRequest].iClass )
		{			
			gRequests[nRequest].nState = RequestWaitingForResponse;
			notifyEvent( NM_CLIENT_OBJECT_REQUEST_RECEIVED, gRequests[nRequest].nIndex );
			return;
		}
	}

	for ( i = 0; i < (INT32)glClientAppProcServiceNbr; i++ )
	{
		if ( gpClientAppProcServices[i] == gRequests[nRequest].bService )
		{			
			gRequests[nRequest].nState = RequestWaitingForResponse;
			notifyEvent( NM_CLIENT_OBJECT_REQUEST_RECEIVED, gRequests[nRequest].nIndex );
			return;
		}
	}

	if ( gRequests[nRequest].bService == SVC_CIP_DATA_TABLE_READ ||
		 gRequests[nRequest].bService == SVC_CIP_DATA_TABLE_WRITE )
	{
		dataTableParseRequest( nRequest );
		gRequests[nRequest].nState = RequestResponseReceived;
		return;
	}

	nIndex = gRequests[nRequest].nIndex;

	/* Direct the request to the appropriate object - this is what Message Router does */
	switch( gRequests[nRequest].iClass )
	{
		case ID_CLASS:
			idParseClassInstanceRequest( nRequest );
			break;
		case ROUTER_CLASS:
			routerParseClassInstanceRequest( nRequest );
			break;
		case ASSEMBLY_CLASS:
			assemblyParseClassInstanceRequest( nRequest );
			break;
		case CONNMGR_CLASS:
			connmgrParseClassInstanceRequest( nRequest );
			break;
		case OUTPOINT_CLASS:
			outpointParseClassInstanceRequest( nRequest );
			break;
#ifdef ET_IP_SCANNER
		case CONFIG_CLASS:
			configParseClassInstanceRequest( nRequest );
			break;
#endif /* #ifdef ET_IP_SCANNER */
		case PORT_CLASS:
			portParseClassInstanceRequest( nRequest );
			break;
		case TCPIP_CLASS:
			tcpipParseClassInstanceRequest( nRequest );
			break;
		case ENETLINK_CLASS:
			enetlinkParseClassInstanceRequest( nRequest );
			break;
		default:	/* Do not support this class yet */
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
			break;
	}	

	/* Check that we still have the request; it could have been already completely processed and removed */
	nRequest = requestGetByRequestId( nIndex );
	if ( nRequest == INVALID_REQUEST )
		return;							/* Request has been removed when processing */
	
	gRequests[nRequest].nState = RequestResponseReceived;
}

/*---------------------------------------------------------------------------
** routerParsePDU( )
**
** Parse the Message Router Protocol Data Unit
**---------------------------------------------------------------------------
*/

INT32  routerParsePDU( UINT8* pPacketPtr, EPATH* pPDU )
{
	INT32       nParsed = 0;
	INT32       nTotal;
	PDU_HDR     pdu_hdr;
	UINT8       bSegment;
	UINT8       bSegmentType;
	UINT8       bLogicalType;
	UINT8       bLogicalFormat;
	UINT8       bNetworkType;
	UINT8       bExtendedSymbol;
	UINT8       bExtendedSymbolType;
	UINT8       bExtendedSymbolSize;
	UINT8       bLinkAddressSize;
		
	pPDU->iFilled = 0;
	
	memcpy( &pdu_hdr, pPacketPtr, PDU_HDR_SIZE );	/* The first 2 bytes is PDU header */
	memset( pPDU, 0, sizeof(EPATH) );

	nTotal = pdu_hdr.bSize * 2;						/* PDU Length not including the PDU header */

	pPacketPtr += PDU_HDR_SIZE;

	if ( nTotal == 0 )
		return nParsed;

	while( nParsed < nTotal )						/* Still parsing PDU */
	{
		if ( (nParsed % 2) && (*pPacketPtr == 0) )	/* This is a pad byte */
		{
			pPacketPtr++;
			nParsed++;
			continue;
		}
		
		bSegment = *pPacketPtr;
		bSegmentType = bSegment & SEGMENT_TYPE_MASK;
		
		pPacketPtr++;
		nParsed++;

		switch( bSegmentType )						/* Segment type determine the data length and structure that follows */
		{
			case PATH_SEGMENT:
			{
				pPDU->iPort = ( bSegment & PORT_ID_MASK );
				pPDU->iFilled |= EPATH_PORT;
				
				if ( bSegment & EXTENDED_LINK_MASK )
				{
					bLinkAddressSize = pPDU->bLinkAddressSize = *pPacketPtr;
					pPacketPtr++;
					nParsed++;
				}
				else
					bLinkAddressSize = 1;

				if ( pPDU->iPort == EXTENDED_PORT_FLAG )
				{
					pPDU->iPort = UINT16_GET( pPacketPtr );
					pPacketPtr += 2;
					nParsed += 2;
				}

				if ( bLinkAddressSize > 1 )
				{
					memcpy( pPDU->bLinkAddress, pPacketPtr, pPDU->bLinkAddressSize );
					pPDU->iFilled |= EPATH_LINK_ADDRESS;
				}
				else if ( bLinkAddressSize == 1 )
				{
					pPDU->bSlot = *pPacketPtr;
					pPDU->iFilled |= EPATH_SLOT_ADDRESS;
				}

				pPacketPtr += bLinkAddressSize;
				nParsed += bLinkAddressSize;
			}
			break;

			case LOGICAL_SEGMENT:
			{
				bLogicalType = bSegment & LOGICAL_SEG_TYPE_MASK;
				bLogicalFormat = bSegment & LOGICAL_SEG_FORMAT_MASK;

				switch( bLogicalType )
				{
					case LOGICAL_SEG_CLASS:
						if ( bLogicalFormat == LOGICAL_SEG_8_BIT )	/* Class ID is 1 byte long */
						{
							pPDU->iClass = *pPacketPtr;
							pPDU->iFilled |= EPATH_CLASS;
							pPacketPtr++;
							nParsed++;
						}
						else if ( bLogicalFormat == LOGICAL_SEG_16_BIT )	/* Class ID is 2 bytes long */
						{
							/* Skip the pad byte */
							pPacketPtr++;
							nParsed++;
									
							pPDU->iClass = UINT16_GET( pPacketPtr );
							pPDU->iFilled |= EPATH_CLASS;
							pPacketPtr += 2;
							nParsed += 2;
						}
						else										/* Class ID must be either 1 or 2 bytes long */
						{
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
						}
						break;

					case LOGICAL_SEG_INSTANCE:
						if ( bLogicalFormat == LOGICAL_SEG_8_BIT )	/* Instance ID is 1 byte long */
						{
							pPDU->iInstance = *pPacketPtr;
							pPDU->iFilled |= EPATH_INSTANCE;
							pPacketPtr++;
							nParsed++;
						}
						else if ( bLogicalFormat == LOGICAL_SEG_16_BIT )	/* Instance ID is 2 bytes long */
						{
							/* Skip the pad byte */
							pPacketPtr++;
							nParsed++;

							pPDU->iInstance = UINT16_GET( pPacketPtr );
							pPDU->iFilled |= EPATH_INSTANCE;
							pPacketPtr += 2;
							nParsed += 2;
						}
						else										/* Instance ID must be either 1 or 2 bytes long */
						{
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
						}
						break;

					case LOGICAL_SEG_ATTRIBUTE:
						if ( bLogicalFormat == LOGICAL_SEG_8_BIT )	/* Attribute ID is 1 byte long */
						{
							pPDU->bAttribute = *pPacketPtr;
							pPDU->iFilled |= EPATH_ATTRIBUTE;
							pPacketPtr++;
							nParsed++;
						}
						else										/* Attribute ID must be either 1 or 2 bytes long */
						{
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
						}
						break;						

					case LOGICAL_SEG_MEMBER:
						if ( bLogicalFormat == LOGICAL_SEG_8_BIT )
						{
							pPDU->iMember = *pPacketPtr;
							pPDU->iFilled |= EPATH_MEMBER;
							pPacketPtr++;
							nParsed++;
						}
						else if ( bLogicalFormat == LOGICAL_SEG_16_BIT )
						{
							/* Skip the pad byte */
							pPacketPtr++;
							nParsed++;

							pPDU->iMember = UINT16_GET( pPacketPtr );
							pPDU->iFilled |= EPATH_MEMBER;
							pPacketPtr += 2;
							nParsed += 2;
						}
						else
						{
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
						}
						break;

					case LOGICAL_SEG_CONN_POINT:					/* Parse Originator->Target and/or Target->Originator conection points */
						if ( bLogicalFormat == LOGICAL_SEG_8_BIT )
						{
							if ( !(pPDU->iFilled & EPATH_CONSUMING_CONN_POINT) )	/* Connection point is 1 byte long */
							{
								pPDU->iConsumingConnPoint = *pPacketPtr;
								pPDU->iFilled |= EPATH_CONSUMING_CONN_POINT;
							}
							else
							{
								pPDU->iProducingConnPoint = *pPacketPtr;
								pPDU->iFilled |= EPATH_PRODUCING_CONN_POINT;
							}
							pPacketPtr++;
							nParsed++;
						}
						else if ( bLogicalFormat == LOGICAL_SEG_16_BIT )	/* Connection point is 2 bytes long */
						{
							/* Skip the pad byte */
							pPacketPtr++;
							nParsed++;
							
							if ( !(pPDU->iFilled & EPATH_CONSUMING_CONN_POINT) )
							{
								pPDU->iConsumingConnPoint = UINT16_GET(pPacketPtr);
								pPDU->iFilled |= EPATH_CONSUMING_CONN_POINT;
							}
							else
							{
								pPDU->iProducingConnPoint = UINT16_GET(pPacketPtr);
								pPDU->iFilled |= EPATH_PRODUCING_CONN_POINT;
							}
							pPacketPtr += 2;
							nParsed += 2;
						}
						else	/* Connection point must be either 1 or 2 bytes long */
						{
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
						}
						break;

					case LOGICAL_SEG_KEY:	/* Parse segment key. Values other than 0 indicate that keying is enabled */
						if ( (*pPacketPtr) == COMMON_KEY_FORMAT )
						{							
							pPacketPtr++;
							nParsed++;
							memcpy( &pPDU->deviceId, pPacketPtr, sizeof(EtIPDeviceID) );
							ENCAP_CVT_HS(pPDU->deviceId.iVendorID);
							ENCAP_CVT_HS(pPDU->deviceId.iProductCode);
							ENCAP_CVT_HS(pPDU->deviceId.iProductType);
							pPDU->iFilled |= EPATH_DEVICE_ID;
							pPacketPtr += sizeof(EtIPDeviceID);
							nParsed += sizeof(EtIPDeviceID);
						}
						else
						{
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
						}
						break;
				
						default:	/* Segment type was not recognized */
							DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
							return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
				}
			}
			break;

			case NETWORK_SEGMENT:
			{
				bNetworkType = bSegment & NET_SEGMENT_TYPE_MASK;
				
				if ( bNetworkType == PRODUCTION_INHIBIT_TYPE )
				{
					if ( !(pPDU->iFilled & EPATH_OT_INHIBIT_INTERVAL) )
					{
						pPDU->bProductionOTInhibitInterval = *pPacketPtr;
						pPDU->iFilled |= EPATH_OT_INHIBIT_INTERVAL;
					}
					else
					{
						pPDU->bProductionTOInhibitInterval = *pPacketPtr;
						pPDU->iFilled |= EPATH_TO_INHIBIT_INTERVAL;
					}
				}

				pPacketPtr++;
				nParsed++;
			}
			break;

			case SYMBOLIC_SEGMENT:
			{
				pPDU->bSymbolSize = bSegment & SYM_SEGMENT_SIZE_MASK;
				if ( pPDU->bSymbolSize == 0 )
				{
					bExtendedSymbol = *pPacketPtr;
					pPacketPtr++;
					nParsed++;

					bExtendedSymbolType = bExtendedSymbol & EXTENDED_SYM_TYPE_MASK;
					bExtendedSymbolSize = bExtendedSymbol & SYM_SEGMENT_SIZE_MASK;

					switch ( bExtendedSymbol )
					{
						case EXTENDED_SYM_TYPE_DOUBLEBYTE:
							pPDU->bSymbolSize = bExtendedSymbolSize*2;
							break;
						case EXTENDED_SYM_TYPE_TRIPLEBYTE:
							pPDU->bSymbolSize = bExtendedSymbolSize*3;
							break;
						case EXTENDED_SYM_TYPE_SPECIAL:
							switch ( bExtendedSymbolSize )
							{
								case 6:
									pPDU->bSymbolSize = sizeof(UINT8);
									break;
								case 7:
									pPDU->bSymbolSize = sizeof(UINT16);
									break;
								case 8:
									pPDU->bSymbolSize = sizeof(UINT32);
									break;
								default:
									pPDU->bSymbolSize = 0;
									break;
							}
							break;
						default:
							pPDU->bSymbolSize = 0;
							break;
					}
				}
				
				if ( pPDU->bSymbolSize )
				{
					memcpy( &pPDU->bSymbol, pPacketPtr, pPDU->bSymbolSize );
					pPDU->iFilled |= EPATH_SYMBOL;
				}
				pPacketPtr += pPDU->bSymbolSize;
				nParsed += pPDU->bSymbolSize;
			}
			break;

			case DATA_SEGMENT:
			{				
				if ( bSegment == SIMPLE_DATA_SEGMENT )	/* Simple data segment could be used to send device specific data in the Forward Open */
					pPDU->iDataSize = (*pPacketPtr)*2;
				else if ( bSegment == EXT_SYMBOL_SEGMENT )	/* Used to attach a simple ANSI string */
					pPDU->bExtSymbolSize = *pPacketPtr;
			
				pPacketPtr++;
				nParsed++;

				if ( bSegment == SIMPLE_DATA_SEGMENT )
				{
					memcpy( &pPDU->bData, pPacketPtr, pPDU->iDataSize );
					pPDU->iFilled |= EPATH_DATA;
					pPacketPtr += pPDU->iDataSize;
					nParsed += pPDU->iDataSize;
				}
				else if ( bSegment == EXT_SYMBOL_SEGMENT )
				{
					memcpy( &pPDU->bExtSymbol, pPacketPtr, pPDU->bExtSymbolSize );
					pPDU->iFilled |= EPATH_EXT_SYMBOL;
					pPacketPtr += pPDU->bExtSymbolSize;
					nParsed += pPDU->bExtSymbolSize;
				}				
			}
			break;

			default:
				DumpStr1("routerParsePDU invalid segment specifier: 0x%x", bSegment);
				return (ROUTER_ERROR_BASE|ROUTER_ERROR_INVALID_SEG_TYPE);
		}		
	}
	
	return nParsed;
}


/*---------------------------------------------------------------------------
** routerParseClassInstanceRequest( )
**
** Parse Message Router Class and Instance requests
**---------------------------------------------------------------------------
*/

void routerParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		routerParseClassRequest( nRequest );
	else
		routerParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** routerParseClassRequest( )
**
** Parse Message Router Class requests
**---------------------------------------------------------------------------
*/

void routerParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_ALL:
			routerSendClassAttrAll( nRequest );
			break;
		default:	/* Do not support any other services */
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;			
			break;
	}
}

/*---------------------------------------------------------------------------
** routerParseInstanceRequest( )
**
** Parse Message Router Instance requests
**---------------------------------------------------------------------------
*/

void routerParseInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 1 )
	{
		switch( gRequests[nRequest].bService )
		{
			case SVC_GET_ATTR_SINGLE:
				routerSendInstanceAttrSingle( nRequest );
				break;
			default:	/* Do not support any other services */
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;				
				break;
		}
	}
	else	/* Instance 1 is the only instance supported */
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
}

/*---------------------------------------------------------------------------
** routerSendClassAttrAll( )
**
** Respond to the GetAttributeAll service request for the Message Router class
**---------------------------------------------------------------------------
*/

void routerSendClassAttrAll( INT32 nRequest )
{
	ROUTER_CLASS_ATTRIBUTE routerAttr;

	routerAttr.iRevision = ROUTER_CLASS_REVISION;
	routerAttr.iOptionalAttributeNbr = 0;
	routerAttr.iOptionalServiceNbr = 0;
	routerAttr.iMaxClassAttr = 0;
	routerAttr.iMaxInstanceAttr = 0;

	utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&routerAttr, ROUTER_CLASS_ATTRIBUTE_SIZE );			
}


/*---------------------------------------------------------------------------
** routerSendInstanceAttrSingle( )
**
** Respond to the GetAttributeSingle service request to the Message Router
** instance
**---------------------------------------------------------------------------
*/

void routerSendInstanceAttrSingle( INT32 nRequest )
{
	UINT16 iVal;
	UINT8* pData;
	
	switch( gRequests[nRequest].iAttribute )
	{
		case ROUTER_ATTR_CLASS_LIST:			
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, NULL, sizeof(UINT16)*ROUTER_OBJECT_COUNT + sizeof(UINT16) );			
			if ( gRequests[nRequest].iDataOffset != INVALID_MEMORY_OFFSET )
			{
				pData = MEM_PTR(gRequests[nRequest].iDataOffset);											
				UINT16_SET( pData, ROUTER_OBJECT_COUNT );			
				pData += sizeof(UINT16);
				memcpy( pData, (char*)&routerObjects[0], sizeof(UINT16)*ROUTER_OBJECT_COUNT );			
			}
			break;
		case ROUTER_ATTR_MAX_CONNECTIONS:
			iVal = ENCAP_TO_HS(MAX_CONNECTIONS);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );			
			break;
		default:	/* Only 2 attributes supported at this time */
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}			
}

