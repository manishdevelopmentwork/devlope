
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MODIN8_HPP

#define	INCLUDE_MODIN8_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TC8, INI8, INV8 Module Configuration
//

class CIN8Module : public CModule
{
	public:
		// Constructor
		SLOW CIN8Module(void);

		// Destructor
		SLOW ~CIN8Module(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

// End of File

#endif
