
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern	BOOL	SaveFile(PCBYTE pData, UINT uSize);

//////////////////////////////////////////////////////////////////////////
//
// Control Project
//

// Constructor

CControlProject::CControlProject(void)
{
	m_pVars     = New CVariableList;
		    
	m_pExec     = New CExecuteFile;
		    
	m_pMode     = NULL;

	m_pInitHook = NULL;

	m_pExecHook = NULL;

	m_fInit     = TRUE;
	}

// Destructor

CControlProject::~CControlProject(void)
{
	delete m_pVars;

	delete m_pExec;

	delete m_pMode;

	delete m_pInitHook;

	delete m_pExecHook;
	}

// Attributes

BOOL CControlProject::HasLicense(void)
{
	return !m_fLicense || (!g_pLicense || g_pLicense->Validate(0x1012));
	}

BOOL CControlProject::HasHooks(void)
{
	return !!m_pInitHook || !!m_pExecHook;
	}

BOOL CControlProject::UseWarmStart(void)
{
	return GetItemData(m_pMode, C3INT(TRUE));
	}

BOOL CControlProject::UseFastScan(void)
{
	return m_uPoll == 2;
	}

BOOL CControlProject::IsNonEmpty(void)
{
	return m_uPoll >= 1 && HasLicense() && m_pExec->IsNonEmpty();
	}

// Operations

void CControlProject::SetScan(void)
{
	SetItemScan(m_pMode,     scanTrue);

	SetItemScan(m_pInitHook, scanTrue);

	SetItemScan(m_pExecHook, scanTrue);
	}

void CControlProject::RunHooks(void)
{
	CCodedItem *pItem = m_fInit ? m_pInitHook : m_pExecHook;

	if( !pItem ) {

		m_fInit = FALSE;
		}
	else {
		if( pItem->IsAvail() ) {

			pItem->Execute(typeVoid);

			m_fInit = FALSE;
			}
		}
	}

// Initialization

void CControlProject::Load(PCBYTE &pData)
{
	ValidateLoad("CControlProject", pData);

	m_fLicense = GetByte(pData);

	m_uPoll    = GetByte(pData);

	m_uScan    = GetWord(pData);

	GetCoded(pData, m_pInitHook);

	GetCoded(pData, m_pExecHook);

	GetCoded(pData, m_pMode);

	m_pVars->Load(pData);

	UINT hExec = GetWord(pData);
	
	if( hExec < 0xFFFF ) {

		PCBYTE pInit = PCBYTE(g_pDbase->LockItem(hExec));

		if( pInit ) {

			if( GetWord(pInit) == IDC_STRATON ) {
			
				m_pExec->Load(pInit);
				}

			g_pDbase->PendItem(hExec, TRUE);
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Executable File Item
//

// Constructor

CExecuteFile::CExecuteFile(void)
{
	m_Handle = 0;

	m_uSize  = 0;
	}

// Destructor

CExecuteFile::~CExecuteFile(void)
{
	if( m_Handle ) {

		g_pDbase->FreeItem(m_Handle);
		}
	}

// Attributes

BOOL CExecuteFile::IsNonEmpty(void)
{
	return m_uSize > 1264;
	}

// Initialization

void CExecuteFile::Load(PCBYTE &pData)
{
	ValidateLoad("CExecuteFile", pData);

	if( (m_Handle = GetWord(pData)) < 0xFFFF ) {

		m_uSize = GetLong(pData);

		if( m_uSize ) {
		
			::SaveFile(pData, m_uSize);
			}

		return;
		}

	m_Handle = 0;
	}

// End of File
