
#include "Intern.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cloud Data Set
//
// Sorting Mode

BOOL volatile CCloudTagSet::m_fSortBusy = FALSE;

UINT volatile CCloudTagSet::m_uSortName = 0;

// Constructor

CCloudTagSet::CCloudTagSet(UINT uSet)
{
	m_uSet  = uSet;

	m_Label = 0;

	m_Name  = 0;

	m_Tree  = 0;

	m_Array = 0;

	m_Props = 0;

	m_Write = 0;

	m_uTags = 0;

	m_pTags = NULL;

	m_pSrc  = afxTagList;

	m_pInfo = NULL;

	m_pLock = Create_Rutex();
}

// Destructor

CCloudTagSet::~CCloudTagSet(void)
{
	AfxRelease(m_pLock);

	delete[] m_pTags;

	delete[] m_pInfo;
}

// Initialization

void CCloudTagSet::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudTagSet", pData);

	CCloudDataSet::Load(pData);

	m_Label = GetByte(pData);

	m_Name  = GetByte(pData);

	m_Tree  = GetByte(pData);

	m_Array = GetByte(pData);

	m_Props = GetByte(pData);

	m_Write = GetByte(pData);

	LoadTagList(pData);
}

// Attributes

BOOL CCloudTagSet::HasLabel(void) const
{
	return m_Label;
}

CString CCloudTagSet::GetLabel(BOOL fLower) const
{
	return CPrintf(fLower ? "set%u" : "Set%u", 1+m_uSet);
}

BOOL CCloudTagSet::HasWrites(void) const
{
	return IsEnabled() && m_Write;
}

// Operations

void CCloudTagSet::Init(void)
{
	SortTagList();

	InitTagInfo();

	CCloudDataSet::Init();
}

void CCloudTagSet::AddRewrite(char cFind, char cRepl)
{
	CRewrite rule = { cFind, cRepl };

	m_Rewrite.Append(rule);
}

UINT CCloudTagSet::FindTag(CString Name)
{
	if( HasLabel() ) {

		if( Name.StripToken('.') != GetLabel(TRUE) ) {

			return NOTHING;
		}
	}

	if( Name.GetLength() ) {

		INDEX n = m_TagMap.FindName(Name);

		if( !m_TagMap.Failed(n) ) {

			CTagInfo &Info = m_TagMap.GetData(n)[0];

			return &Info - m_pInfo;
		}
	}

	return NOTHING;
}

BOOL CCloudTagSet::SetTagData(CString Name, CString Data)
{
	if( IsEnabled() ) {

		if( HasLabel() ) {

			if( Name.StripToken('.') != GetLabel(TRUE) ) {

				return FALSE;
			}
		}

		if( m_Props ) {

			CString Value = ".Value";

			if( !Name.EndsWith(Value) ) {

				return FALSE;
			}

			Name = Name.Left(Name.GetLength() - Value.GetLength());
		}

		if( Name.GetLength() ) {

			INDEX n = m_TagMap.FindName(Name);

			if( !m_TagMap.Failed(n) ) {

				CTagInfo &Info = m_TagMap.GetData(n)[0];

				Info.m_fWipe   = TRUE;

				if( m_Write ) {

					return Info.m_pTag->SetAsText(Info.m_Ref.t.m_Array, Data);
				}
			}
		}
	}

	return FALSE;
}

void CCloudTagSet::ResetHistory(void)
{
	for( UINT n = 0; n < m_uTags; n++ ) {

		CTagInfo &Info = m_pInfo[n];

		if( Info.m_pTag ) {

			Info.m_pTag->KillPrevious(Info.m_Prev);

			Info.m_pTag->InitPrevious(Info.m_Prev);
		}

		Info.m_fInit = TRUE;
	}

	CCloudDataSet::ResetHistory();
}

void CCloudTagSet::DataWasSent(void)
{
	for( UINT n = 0; n < m_uTags; n++ ) {

		CTagInfo &Info = m_pInfo[n];

		if( Info.m_pTag ) {

			if( Info.m_fSent ) {

				Info.m_pTag->KillPrevious(Info.m_Prev);

				Info.m_Prev  = Info.m_Data;

				Info.m_fWipe = FALSE;

				Info.m_fSent = FALSE;

				Info.m_fInit = FALSE;

				Info.m_pTag->InitPrevious(Info.m_Data);
			}
		}
	}

	CCloudDataSet::DataWasSent();
}

BOOL CCloudTagSet::GetTagNames(CStringArray &List)
{
	if( m_uTags ) {

		for( UINT n = 0; n < m_uTags; n++ ) {

			CTagInfo &Info = m_pInfo[n];

			List.Append(GetTagName(Info.m_pTag, NOTHING, m_Name));
		}

		return TRUE;
	}

	return FALSE;
}

// Json-Based Access

BOOL CCloudTagSet::GetJson(CMqttJsonData *pRep, CMqttJsonData *pDes, CString Ident, UINT64 uTime, UINT uRoot, UINT uCode, UINT uMode)
{
	BOOL    fForce     = FALSE;

	BOOL    fData      = FALSE;

	CString Current    = ".$";

	UINT    uRepDepth  = 0;

	UINT    uDesDepth  = 0;

	CArray <CString>     RepNames;

	CArray <CMqttJsonData *> RepStack;

	CArray <CMqttJsonData *> DesStack;

	RepStack.Append(pRep);

	DesStack.Append(pDes);

	if( uRoot == 0 ) {

		AddToStack(RepStack, uRepDepth, "tags");

		RepNames.Append("tags");
	}

	if( m_Label ) {

		AddToStack(RepStack, uRepDepth, GetLabel(TRUE));

		RepNames.Append(GetLabel(TRUE));
	}

	if( Ident.GetLength() ) {

		RepStack[uRepDepth]->AddValue("cid", Ident);
	}

	if( uMode == modeForce ) {

		fForce = TRUE;
	}

	if( m_Force == 0xFFFF ) {

		fForce = TRUE;
	}

	for( UINT n = 0; n == NOTHING || n < m_uTags; n++ ) {

		CTagInfo &Info = m_pInfo[n];

		if( Info.m_pTag ) {

			if( fForce ) {

				Info.m_pTag->KillPrevious(Info.m_Prev);

				Info.m_pTag->InitPrevious(Info.m_Prev);
			}

			if( Info.m_pTag->HasChanged(Info.m_Ref, Info.m_Data, Info.m_Prev) ) {

				if( uMode == modeIfAny ) {

					if( !fForce ) {

						n      = NOTHING;

						fForce = TRUE;

						continue;
					}
				}

				CString Name = Info.m_Label;

				CString Used = Name;

				CString Data = UniConvert(Info.m_pTag->GetAsText(Info.m_Ref.t.m_Array,
							  Info.m_Data,
							  Info.m_Type,
							  fmtBare | fmtANSI
				));

				if( uRoot == 0 && m_Tree ) {

					CString Norm = ".$." + Name;

					UINT    uDot = Norm.FindRev('.');

					CString Path = Norm.Left(uDot+0);

					CString Rest = Norm.Mid(uDot+1);

					if( Path != Current ) {

						// We need to change folder as the
						// paths do not match any longer.

						while( !Path.StartsWith(Current) ) {

							// The path does not start with our
							// working path, so we have to keep
							// going up a level until it does.

							uDot    = Current.FindRev('.');

							Current = Current.Left(uDot);

							RepStack.Remove(uRepDepth);

							RepNames.Remove(uRepDepth);

							if( uDesDepth == uRepDepth ) {

								DesStack.Remove(uDesDepth);

								uDesDepth--;
							}

							uRepDepth--;
						}

						if( Path != Current ) {

							// If they still don't match, we need to make
							// some New child nodes, so find the path that
							// contains the required names.

							CString Make = Path.Mid(Current.GetLength()+1);

							while( Make.GetLength() ) {

								// For each name, create the JSON
								// node and add it to our stack.

								CString    Walk = Make.StripToken('.');

								AddToStack(RepStack, uRepDepth, Walk);

								RepNames.Append(Walk);

								// Update the current path accordingly.

								Current += '.';

								Current += Walk;
							}
						}
					}

					if( m_Tree == 1 ) {

						Used = Rest;
					}
				}

				if( pDes ) {

					if( Info.m_fWipe ) {

						while( uDesDepth < uRepDepth ) {

							AddToStack(DesStack, uDesDepth, RepNames[uDesDepth]);
						}

						if( !m_Props ) {

							DesStack[uDesDepth]->AddNull(Used);
						}
						else {
							CMqttJsonData *pPut = DesStack[uRepDepth];

							CMqttJsonData *pTag = NULL;

							pPut->AddObject(Used, pTag);

							pTag->AddNull("Value");
						}
					}
				}

				if( uRoot == 2 && uTime > 0 ) {

					CMqttJsonData *pPut = RepStack[uRepDepth];

					CMqttJsonData *pTag = NULL;

					pPut->AddObject(Used, pTag);

					if( Info.m_Type == typeString ) {

						pTag->AddValue("value", Data, jsonString);
					}
					else {
						pTag->AddValue("value", Data, jsonAuto);
					}

					struct timeval tv;

					tv.tv_sec  = UINT(uTime / 1000);

					tv.tv_usec = UINT(uTime % 1000 * 1000);

					pTag->AddValue("timestamp", CPrintf("%u%3.3u", tv.tv_sec, tv.tv_usec / 1000), jsonNumber);
				}
				else {
					if( uRoot >= 1 || !m_Props ) {

						if( Info.m_Type == typeString ) {

							RepStack[uRepDepth]->AddValue(Used, Data, jsonString);
						}
						else
							RepStack[uRepDepth]->AddValue(Used, Data, jsonAuto);
					}
					else {
						CMqttJsonData *pPut = RepStack[uRepDepth];

						CMqttJsonData *pTag = NULL;

						pPut->AddObject(Used, pTag);

						if( Info.m_fInit ) {

							for( UINT p = 1; p <= 17; p++ ) {

								switch( p ) {

									// Skip these for now as we don't offer check
									// for change and they are often dynamic data.

									case tpAlarms:
									case tpAsText:
									case tpBackColor:
									case tpForeColor:
									case tpIndex:
									case tpSetPoint:

										continue;
								}

								CString PropName;

								DWORD   PropData;

								UINT    PropType;

								if( GetProp(n, p, PropName, PropData, PropType) ) {

									if( p == tpStateCount ) {

										if( m_Props == 2 ) {

										//	if( Info.m_pTag->m_pFormat && Info.m_pTag->m_pFormat->IsMulti() ) {
										//
										//		This is where we'd add the state list as an object.
										//		}
										}
										else
											continue;
									}

									// Unicode Handling?

									CUnicode Text = Info.m_pTag->GetAsText(Info.m_Ref.t.m_Array,
													       PropData,
													       PropType,
													       fmtBare | fmtANSI
									);

									pTag->AddValue(PropName, UniConvert(Text), jsonAuto);
								}
							}
						}

						if( Info.m_Type == typeString ) {

							pTag->AddValue("Value", Data, jsonString);
						}
						else {
							pTag->AddValue("Value", Data, jsonAuto);
						}
					}
				}

				Info.m_fSent = TRUE;

				fData        = TRUE;
			}
		}
	}

	return fData;
}

// List-Based Access

UINT CCloudTagSet::GetCount(void)
{
	return m_uTags;
}

UINT CCloudTagSet::GetProps(void)
{
	return m_Props ? 17 : 0;
}

BOOL CCloudTagSet::GetData(UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, UINT uMode)
{
	if( uIndex < m_uTags ) {

		CTagInfo &Info = m_pInfo[uIndex];

		if( Info.m_pTag ) {

			if( uMode == modeBirth ) {

				Name = Info.m_Label;

				Data = Info.m_pTag->GetData(Info.m_Ref, Info.m_Type, getNone);

				Type = Info.m_Type;

				Free = (Type == typeString) ? TRUE : FALSE;

				return TRUE;
			}

			if( uMode == modeForce ) {

				Info.m_pTag->KillPrevious(Info.m_Prev);

				Info.m_pTag->InitPrevious(Info.m_Prev);
			}

			if( Info.m_pTag->HasChanged(Info.m_Ref, Info.m_Data, Info.m_Prev) ) {

				Name = Info.m_Label;

				Data = Info.m_Data;

				Type = Info.m_Type;

				Free = FALSE;

				Info.m_fSent = TRUE;

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCloudTagSet::GetProp(UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type)
{
	if( m_Props ) {

		if( uIndex < m_uTags ) {

			CTagInfo &Info = m_pInfo[uIndex];

			if( Info.m_pTag ) {

				WORD id = WORD(uProp);

				Name    = Info.m_pTag->GetPropName(id);

				Type    = Info.m_pTag->GetPropType(id, Info.m_pTag->GetDataType());

				Data    = Info.m_pTag->GetProp(Info.m_Ref, id, Type);

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCloudTagSet::SetData(UINT uIndex, DWORD Data, UINT Type)
{
	if( uIndex < m_uTags ) {

		CTagInfo &Info = m_pInfo[uIndex];

		if( Info.m_pTag ) {

			if( m_Write ) {

				return Info.m_pTag->SetData(Info.m_Ref, Data, Type, setNone);
			}
		}
	}

	return TRUE;
}

// Implementation

BOOL CCloudTagSet::LoadTagList(PCBYTE &pData)
{
	if( (m_uTags = GetWord(pData)) ) {

		m_pTags = New DWORD[m_uTags];

		for( UINT n = 0; n < m_uTags; n++ ) {

			m_pTags[n] = GetLong(pData);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CCloudTagSet::InitTagInfo(void)
{
	m_pInfo = New CTagInfo[m_uTags];

	for( UINT n = 0; n < m_uTags; n++ ) {

		CTagInfo &Info = m_pInfo[n];

		Info.m_Ref  = (CDataRef &) m_pTags[n];

		Info.m_pTag = m_pSrc->GetItem(Info.m_Ref.t.m_Index);

		if( Info.m_pTag ) {

			CString Name = GetTagName(Info.m_pTag, NOTHING, m_Name);

			if( Info.m_pTag->IsArray() ) {

				UINT i = Info.m_pTag->GetExtent();

				UINT d = 1;

				while( i >= 10 ) {

					i /= 10;

					d += 1;
				}

				if( (m_Array & 1) == 0 ) {

					if( (m_Array & 2) == 0 ) {

						Name.AppendPrintf("[%*.*u]", d, d, Info.m_Ref.t.m_Array);
					}
					else {
						// Do not pad numbering!

						Name.AppendPrintf("[%u]", Info.m_Ref.t.m_Array);
					}
				}
				else {
					if( (m_Array & 2) == 0 ) {

						Name.AppendPrintf(".%*.*u", d, d, Info.m_Ref.t.m_Array);
					}
					else {
						// Do not pad numbering!

						Name.AppendPrintf(".%u", Info.m_Ref.t.m_Array);
					}
				}
			}

			for( UINT r = 0; r < m_Rewrite.GetCount(); r++ ) {

				CRewrite const &rule = m_Rewrite[r];

				if( rule.m_cRepl ) {

					Name.Replace(rule.m_cFind, rule.m_cRepl);
				}
				else
					Name.Remove(rule.m_cFind);
			}

			Info.m_pTag->SetScan(Info.m_Ref, scanTrue);

			Info.m_pTag->InitPrevious(Info.m_Prev);

			Info.m_pTag->InitPrevious(Info.m_Data);

			Info.m_Label = Name;

			Info.m_Type  = Info.m_pTag->GetDataType();

			Info.m_fWipe = FALSE;

			Info.m_fSent = FALSE;

			Info.m_fInit = TRUE;

			m_TagMap.Insert(Info.m_Label, &Info);
		}
	}

	return TRUE;
}

void CCloudTagSet::SortTagList(void)
{
	CAutoGuard Guard;

	for( ;;) {

		CAutoLock Lock(m_pLock);

		if( !m_fSortBusy ) {

			m_fSortBusy = TRUE;

			m_uSortName = m_Name;

			Lock.Free();

			qsort(m_pTags, m_uTags, sizeof(DWORD), SortTags);

			m_fSortBusy = FALSE;

			break;
		}

		Lock.Free();

		Sleep(5);
	}
}

void CCloudTagSet::AddToStack(CArray <CMqttJsonData *> &Stack, UINT &uDepth, CString const &Name)
{
	CMqttJsonData *pNode = NULL;

	Stack[uDepth]->AddChild(Name, FALSE, pNode);

	Stack.Append(pNode);

	uDepth++;
}

// Tag Name Access

CString CCloudTagSet::GetTagName(CTag *pTag, UINT uPos, UINT uName)
{
	CString Name;

	switch( uName ) {

		case 0:
			Name = UniConvert(pTag->m_Name);

			break;

		case 1:
		case 4:
			if( !(Name = UniConvert(pTag->m_Desc)).IsEmpty() ) {

				Normalize(Name, pTag->m_Name, uName == 4);
			}
			else
				Name = UniConvert(pTag->m_Name);

			break;

		case 2:
		case 5:
			if( !(Name = UniConvert(pTag->GetLabel(uPos, FALSE))).IsEmpty() ) {

				Normalize(Name, pTag->m_Name, uName == 5);
			}
			else
				Name = UniConvert(pTag->m_Name);

			break;

		case 3:
		case 6:
			if( !(Name = UniConvert(pTag->GetAlias(uPos))).IsEmpty() ) {

				Normalize(Name, pTag->m_Name, uName == 6);
			}
			else
				Name = UniConvert(pTag->m_Name);

			break;
	}

	return Name;
}

void CCloudTagSet::Normalize(CString &Name, CUnicode Base, bool fPath)
{
	Name.Replace(' ', '_');

	for( UINT n = 0; n < Name.GetLength(); n++ ) {

		if( isalnum(Name[n]) || Name[n] == '_' ) {

			continue;
		}

		Name.Delete(n--, 1);
	}

	if( fPath ) {

		CString Path = UniConvert(Base);

		UINT    uDot = Path.FindRev('.');

		if( uDot < NOTHING ) {

			Name = Path.Left(uDot+1) + Name;
		}
	}
}

// Friends

int CCloudTagSet::SortTags(void const *p1, void const *p2)
{
	CDataRef const &r1 = (CDataRef const &) *PDWORD(p1);

	CDataRef const &r2 = (CDataRef const &) *PDWORD(p2);

	if( r1.t.m_Index == r2.t.m_Index ) {

		if( r1.t.m_Array < r2.t.m_Array ) {

			return -1;
		}

		if( r1.t.m_Array > r2.t.m_Array ) {

			return +1;
		}

		return 0;
	}

	CTag *  t1 = afxTagList->GetItem(r1.t.m_Index);

	CTag *  t2 = afxTagList->GetItem(r2.t.m_Index);

	CString s1 = t1 ? GetTagName(t1, r1.t.m_Index, m_uSortName) : "";

	CString s2 = t2 ? GetTagName(t2, r2.t.m_Index, m_uSortName) : "";

	return strcmp(s1, s2);
}

// End of File
