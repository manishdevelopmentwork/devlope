
#include "Intern.hpp"

#include "Services.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ServiceOpcUa.hpp"
#include "ServiceCloudGeneric.hpp"
#include "ServiceCloudAws.hpp"
#include "ServiceCloudAzure.hpp"
#include "ServiceCloudGoogle.hpp"
#include "ServiceCloudSparkplug.hpp"
#include "ServiceCloudCumulocity.hpp"
#include "ServiceCloudUbidots.hpp"
#include "TimeSync.hpp"
#include "OPCServer.hpp"
#include "FTPServer.hpp"
#include "FileSync.hpp"
#include "MailManager.hpp"
#include "SqlSync.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Services Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CServices, CMetaItem);

// Service List

static CLASS m_List[] = {

	AfxRuntimeClass(CServiceOpcUa),
	AfxRuntimeClass(CServiceCloudGeneric),
	AfxRuntimeClass(CServiceCloudAws),
	AfxRuntimeClass(CServiceCloudAzure),
	AfxRuntimeClass(CServiceCloudGoogle),
	AfxRuntimeClass(CServiceCloudSparkplug),
//	AfxRuntimeClass(CServiceCloudCumulocity),
	AfxRuntimeClass(CServiceCloudUbidots),
	AfxRuntimeClass(CTimeSync),
	AfxRuntimeClass(COPCServer),
	AfxRuntimeClass(CFTPServer),
	AfxRuntimeClass(CFileSync),
	AfxRuntimeClass(CMailManager),
	AfxRuntimeClass(CSqlSync)

};

// Constructor

CServices::CServices(void)
{
	m_pList = New CItemIndexList;
}

// Attributes

UINT CServices::GetTreeImage(void) const
{
	return IDI_SERVICES;
}

// Persistance

void CServices::Init(void)
{
	CMetaItem::Init();

	AddServices();
}

void CServices::PostLoad(void)
{
	CMetaItem::PostLoad();

	DWORD Mask = 0;

	for( INDEX Index = m_pList->GetHead(); !m_pList->Failed(Index); m_pList->GetNext(Index) ) {

		CServiceItem *pServ = (CServiceItem *) m_pList->GetItem(Index);

		CLASS         Found = AfxPointerClass(pServ);

		for( UINT n = 0; n < elements(m_List); n++ ) {

			if( m_List[n] == Found ) {

				Mask |= (1<<n);

				break;
			}
		}
	}

	for( UINT n = 0; n < elements(m_List); n++ ) {

		if( !(Mask & (1<<n)) ) {

			CItem *pBefore = NULL;

			if( n < elements(m_List) - 1 ) {

				CLASS Before = m_List[n+1];

				for( UINT m = 0; m < elements(m_List); m++ ) {

					CServiceItem *pServ = (CServiceItem *) m_pList->GetItem(m);

					CLASS         Found = AfxPointerClass(pServ);

					if( Found == Before ) {

						pBefore = pServ;

						break;
					}
				}
			}

			CLASS Class = m_List[n];

			m_pList->InsertItem(AfxNewObject(CItem, Class), pBefore);
		}
	}
}

// Conversion

void CServices::PostConvert(void)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CServiceItem *pServ = (CServiceItem *) m_pList->GetItem(Index);

		pServ->PostConvert();

		m_pList->GetNext(Index);
	}
}

// Download Support

BOOL CServices::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	UINT uCount = 0;

	for( UINT p = 0; p < 2; p++ ) {

		INDEX Index = m_pList->GetHead();

		while( !m_pList->Failed(Index) ) {

			CServiceItem *pServ = (CServiceItem *) m_pList->GetItem(Index);

			if( pServ->IsEnabled() ) {

				if( p == 0 ) {

					uCount++;
				}

				if( p == 1 ) {

					Init.AddWord(WORD(pServ->m_Handle));
				}
			}

			m_pList->GetNext(Index);
		}

		if( p == 0 ) {

			Init.AddWord(WORD(uCount));
		}
	}

	return TRUE;
}

// Meta Data Creation

void CServices::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(List);

	Meta_SetName((IDS_SERVICES));
}

// Service Creation

void CServices::AddServices(void)
{
	for( UINT n = 0; n < elements(m_List); n++ ) {

		CLASS Class = m_List[n];

		m_pList->AppendItem(AfxNewObject(CItem, Class));
	}
}

// End of File
