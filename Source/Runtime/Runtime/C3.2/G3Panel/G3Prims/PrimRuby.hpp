
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRuby_HPP
	
#define	INCLUDE_PrimRuby_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive
//

class CPrimRuby : public CPrim
{
	public:
		// Constructor
		CPrimRuby(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		R2   GetBackRect(void);
		void MovePrim(int cx, int cy);

	protected:
		// Data Members
		R2 m_bound;

		// Implementation
		BOOL LoadList(PCBYTE &pData, CRubyGdiList &list);
		BOOL LoadNumber(PCBYTE &pData, number &n);
		BOOL LoadPoint(PCBYTE &pData, CRubyPoint &p);
		BOOL LoadVector(PCBYTE &pData, CRubyVector &v);
		BOOL LoadMatrix(PCBYTE &pData, CRubyMatrix &m);
	};

// End of File

#endif
