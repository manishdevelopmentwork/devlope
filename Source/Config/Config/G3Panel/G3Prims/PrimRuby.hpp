
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRuby_HPP
	
#define	INCLUDE_PrimRuby_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive
//

class CPrimRuby : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRuby(void);

		// Overridables
		CRect GetBoundingRect(void);
		void  SetHand(BOOL fInit);
		void  UpdateLayout(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		R2 m_bound;

		// Implementation
		BOOL AddList(CInitData &Init, CRubyGdiList const &list);
		BOOL AddNumber(CInitData &Init, number n);
		BOOL AddPoint(CInitData &Init, CRubyPoint const &p);
		BOOL AddVector(CInitData &Init, CRubyVector const &v);
		BOOL AddMatrix(CInitData &Init, CRubyMatrix const &m);

		// Path Management
		virtual void InitPaths(void);
		virtual void MakePaths(void);
		virtual void MakeLists(void);
	};

// End of File

#endif
