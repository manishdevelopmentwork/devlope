
#include "Intern.hpp"

#include "DispFormatMulti.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "DispFormatMultiEntry.hpp"
#include "DispFormatMultiList.hpp"
#include "DispFormatMultiPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatMulti, CDispFormat);

// Constructor

CDispFormatMulti::CDispFormatMulti(void)
{
	m_uType    = 6;

	m_Count    = 3;

	m_pList    = New CDispFormatMultiList;

	m_pLimit   = NULL;

	m_pDefault = NULL;

	m_Range    = 0;
	}

// UI Creation

BOOL CDispFormatMulti::OnLoadPages(CUIPageList *pList)
{
	CDispFormatMultiPage *pPage = New CDispFormatMultiPage(this);

	pList->Append(pPage);

	return TRUE;
	}

// UI Update

void CDispFormatMulti::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	if( Tag == "Count" ) {

		if( pHost->HasUndo() ) {

			if( !pHost->InReplay() ) {

				HGLOBAL hPrev = m_pList->TakeSnapshot();

				m_pList->SetItemCount(m_Count);

				HGLOBAL hData = m_pList->TakeSnapshot();

				CCmd *  pCmd  = New CCmdSubItem( L"Format/List",
								 hPrev,
								 hData
								 );

				pHost->SaveExtraCmd(pCmd);

				pHost->RemakeUI();
				}
			}
		else {
			m_pList->SetItemCount(m_Count);

			pHost->RemakeUI();
			}
		}

	if( Tag == "ButtonExport" ) {

		DoExport();
		}

	if( Tag == "ButtonImport" ) {

		if( pHost->HasUndo() ) {

			HGLOBAL hPrev = m_pList->TakeSnapshot();

			if( DoImport() ) {

				HGLOBAL     hData = m_pList->TakeSnapshot();

				CCmdSubItem *pCmd = New CCmdSubItem( L"Format/List",
								     hPrev,
								     hData
								     );

				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else {
					pHost->SaveExtraCmd(pCmd);

					pHost->RemakeUI();
					}
				}
			else
				GlobalFree(hPrev);
			}
		else {
			if( DoImport() ) {

				pHost->RemakeUI();
				}
			}
		}

	if( Tag == "SUBITEM" ) {

		pHost->RemakeUI();
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatMulti::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Default" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Limit" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Formatting

CString CDispFormatMulti::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeInteger || Type == typeReal ) {

		CString d = m_pDefault->GetText();

		UINT    c = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

			if( pItem ) {

				if( pItem->MatchData(Data, Type, d) ) {

					return d;
					}
				}
			}

		if( d.IsEmpty() ) {

			d = GeneralFormat(Data, Type, Flags);

			d = L'(' + d + L')';
			}

		return d;
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormatMulti::GetMin(UINT Type)
{
	if( Type == typeInteger || Type == typeReal ) {

		DWORD dwMin = 0;

		BOOL fFirst = TRUE;

		UINT c      = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

			if( pItem && pItem->m_pData ) {

				if( fFirst ) {

					dwMin  = pItem->m_pData->ExecVal();

					fFirst = FALSE;
					}
				else {
					if( pItem->CompareData(dwMin, Type) < 0 ) {

						dwMin = pItem->m_pData->ExecVal();
						}
					}
				}
			}

		return dwMin;
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatMulti::GetMax(UINT Type)
{
	if( Type == typeInteger || Type == typeReal ) {

		DWORD dwMax = 0;

		BOOL fFirst = TRUE;

		UINT      c = GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

			if( pItem && pItem->m_pData ) {

				if( fFirst ) {

					dwMax  = pItem->m_pData->ExecVal();

					fFirst = FALSE;
					}
				else {
					if( pItem->CompareData(dwMax, Type) > 0 ) {

						dwMax = pItem->m_pData->ExecVal();
						}
					}
				}
			}

		return dwMax;
		}

	return CDispFormat::GetMax(Type);
	}

// Operations

void CDispFormatMulti::UpdateTypes(BOOL fComp)
{
	UINT c = m_pList->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CDispFormatMultiEntry *pItem = m_pList->GetItem(n);

		pItem->UpdateTypes(fComp);
		}
	}

// Persistance

void CDispFormatMulti::Init(void)
{
	CDispFormat::Init();

	m_pList->SetItemCount(m_Count);

	m_pList->GetItem(0U)->Set(L"1", L"\"S1\"");

	m_pList->GetItem(1U)->Set(L"2", L"\"S2\"");

	m_pList->GetItem(2U)->Set(L"3", L"\"S3\"");
	}

// Download Support

BOOL CDispFormatMulti::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pLimit);

	Init.AddItem(itemVirtual, m_pDefault);

	Init.AddByte(BYTE(m_Range));

	Init.AddItem(itemSimple, m_pList);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatMulti::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddCollect(List);
	Meta_AddInteger(Count);
	Meta_AddVirtual(Limit);
	Meta_AddInteger(Range);
	Meta_AddVirtual(Default);

	Meta_SetName((IDS_MULTISTATE_FORMAT));
	}

// Implementation

BOOL CDispFormatMulti::DoExport(void)
{
	// TODO -- Use stream and support ansi format, too.

	CSaveFileDialog Dialog;

	Dialog.LoadLastPath(L"Format");

	Dialog.SetCaption(CString(IDS_EXPORT_FORMAT));

	Dialog.SetFilter (CString(IDS_TEXT_FILES_TXTTXT));

	if( Dialog.ExecAndCheck() ) {

		FILE *pFile = _wfopen(Dialog.GetFilename(), L"wb");

		if( pFile ) {

			afxThread->SetWaitMode(TRUE);

			fwprintf(pFile, L"\xFEFF");

			fwprintf(pFile, L"Data\tText\r\n");

			UINT uCount = m_pList->GetItemCount();

			for( UINT n = 0; n < uCount; n++ ) {

				CDispFormatMultiEntry *pEntry = m_pList->GetItem(n);

				CString Data, Text;

				if( pEntry->m_pData ) {

					Data = pEntry->m_pData->GetSource(TRUE);
					}

				if( pEntry->m_pText ) {

					Text = pEntry->m_pText->GetSource(TRUE);
					}

				// cppcheck-suppress invalidPrintfArgType_s

				fwprintf(pFile, L"%s\t%s\r\n", PCTXT(Data), PCTXT(Text));
				}

			fclose(pFile);

			afxThread->SetWaitMode(FALSE);

			return TRUE;
			}

		afxMainWnd->Error(CString(IDS_UNABLE_TO_WRITE));
		}

	return FALSE;
	}

BOOL CDispFormatMulti::DoImport(void)
{

	// TODO -- Use stream and support ansi format, too.

	COpenFileDialog Dialog;

	Dialog.LoadLastPath(L"Format");

	Dialog.SetCaption(CString(IDS_IMPORT_FORMAT));

	Dialog.SetFilter (CString(IDS_TEXT_FILES_TXTTXT));

	if( Dialog.Execute() ) {

		FILE *pFile = _wfopen(Dialog.GetFilename(), L"rb");

		if( pFile ) {

			afxThread->SetWaitMode(TRUE);

			if( !(getc(pFile) == 0xFF && getc(pFile) == 0xFE) ) {

				fseek(pFile, 0, SEEK_SET);
				}

			CStringArray Data, Text;

			UINT n;

			for( n = 0;; n++ ) {

				WCHAR sLine[1024] = {0};

				fgetws(sLine, sizeof(sLine), pFile);

				if( sLine[0] ) {

					if( n > 0 ) {

						CStringArray List;

						CString Line(sLine, wstrlen(sLine)-2);

						Line.Tokenize(List, L'\t');

						Data.Append(List[0]);

						Text.Append(List[1]);
						}
					else {
						if( wstrcmp(sLine, L"Data\tText\r\n") ) {

							break;
							}
						}

					continue;
					}

				break;
				}

			afxThread->SetWaitMode(FALSE);

			fclose(pFile);

			if( !n ) {

				CWnd::GetActiveWindow().Error(CString(IDS_INVALID_HEADER));

				return FALSE;
				}

			m_Count = n - 1;

			m_pList->SetItemCount(m_Count);

			if( m_Count ) {

				for( UINT n = 0; n < m_Count; n++ ) {

					CDispFormatMultiEntry *pEntry = m_pList->GetItem(n);

					pEntry->Set(Data[n], Text[n]);
					}
				}

			return TRUE;
			}

		afxMainWnd->Error(CString(IDS_UNABLE_TO_READ));
		}

	return FALSE;
	}

void CDispFormatMulti::DoEnables(IUIHost *pHost)
{
	}

UINT CDispFormatMulti::GetCount(void)
{
	UINT uCount = m_pList->GetItemCount();

	if( m_pLimit ) {

		UINT uLimit = m_pLimit->ExecVal();

		return max(1, min(uLimit, uCount));
		}

	return uCount;
	}

// End of File
