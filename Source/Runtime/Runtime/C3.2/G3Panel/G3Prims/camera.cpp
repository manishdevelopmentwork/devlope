
#include "intern.hpp"

#include "uitask.hpp"

#include "camera.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Camera
//

// Constructor

CPrimCamera::CPrimCamera(void)
{
	m_pCamera   = NULL;

	m_fInit     = FALSE;

	ClearCtx(m_Ctx);
	}

// Initialization

void CPrimCamera::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimCamera", pData);

	CPrimGeom::Load(pData);

	m_uPort      = GetByte(pData);

	m_uDevice    = GetWord(pData);

	m_uScale     = GetByte(pData);
	}

// Overridables

void CPrimCamera::SetScan(UINT Code)
{
	CPrimGeom::SetScan(Code);
	}

void CPrimCamera::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( WhoHasFeature(rfCamera) ) {

		if( !m_fInit ) {

			CCommsManager      *pComms = CCommsSystem::m_pThis->m_pComms;

			CCommsDevice      *pDevice = pComms->FindDevice(m_uDevice);

			IProxyCameraList *pCameras = pComms->m_pCameras;

			if( pCameras ) {

				m_pCamera = pCameras->FindProxy(m_uPort);
				}

			m_fInit = TRUE;
			}

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;
		
			m_fChange = TRUE;
			}
		}

	CPrimGeom::DrawPrep(pGDI, Erase, Trans);
	}

void CPrimCamera::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	CPrim::DrawExec(pGDI, pDirty);
	}

void CPrimCamera::DrawPrim(IGDI *pGDI)
{
	R2 Rect = m_DrawRect;

	if( WhoHasFeature(rfCamera) ) {

		if( m_pCamera ) {
		
			DrawBase(pGDI, Rect);

			m_pCamera->ClaimData();

			PCBYTE pData = m_pCamera->GetData(m_uDevice);

			if( pData ) {

				BITMAP_RLC   *pBitmap = (BITMAP_RLC *) pData;

				int      cx = pBitmap->Width;

				int      cy = pBitmap->Height;

				int      nb = pBitmap->Stride;
				
				int      k  = 1;

				int      z  = 0;

				switch( m_uScale ) {

					case 1: k = 2; z = ropZoom2X; break;
					case 2: k = 3; z = ropZoom3X; break;
					case 3: k = 4; z = ropZoom4X; break;
					}			

				int px = (Rect.x2 + Rect.x1 - k * cx) / 2;

				int py = (Rect.y2 + Rect.y1 - k * cy) / 2;

				if( m_uScale >= 4 ) {

					UINT uWidth  = Rect.x2 - Rect.x1;

					UINT uHeight = Rect.y2 - Rect.y1;
					
					float scaleX = float(uWidth ) / cx;

					float scaleY = float(uHeight) / cy;

					float factor = min(scaleX, scaleY);

					UINT scx = UINT(factor * cx);

					UINT scy = UINT(factor * cy);

					UINT snb = UINT(factor * nb);

					PBYTE pScaled = New BYTE[ snb * scy ];
										
					if( m_uScale == 4 ) {

						ScaleImageNearest(PCDWORD(pBitmap->Data), cx, cy, factor, PDWORD(pScaled));
						}

					if( m_uScale == 5 ) {

						ScaleImageBilinear(PCDWORD(pBitmap->Data), cx, cy, factor, PDWORD(pScaled));
						}
										
					delete [] pData;

					px = (Rect.x2 + Rect.x1 - k * scx) / 2;

					py = (Rect.y2 + Rect.y1 - k * scy) / 2;

					pGDI->BitBlt(   px,  py,
							scx, scy,
							snb,
							pScaled,
							ropSRCCOPY | ropInvert
							);

					delete [] pScaled;

					m_pCamera->FreeData();

					DrawSeq (pGDI, Rect);

					return;
					}

				if( px >= Rect.x1 && py >= Rect.y1 ) {

					pGDI->BitBlt( px, py,
							cx, cy,
							nb,
							pBitmap->Data,
							ropSRCCOPY | ropInvert | z
							);
					}
				else {
					pGDI->SelectPen(penFore);

					pGDI->DrawLine(Rect.x1+4, Rect.y1+4, Rect.x2-4, Rect.y2-4);

					pGDI->DrawLine(Rect.x1+4, Rect.y2-4, Rect.x2-4, Rect.y1+4);
					}

				delete [] pData;
				}
		
			m_pCamera->FreeData();

			DrawSeq (pGDI, Rect);
		
			return;
			}
		}

	DrawBase(pGDI, Rect);
	}

// Implementation

void CPrimCamera::DrawBase(IGDI *pGDI, R2 &Rect)
{
	m_pEdge->AdjustRect(Rect);

	m_pFill->FillRect(pGDI, Rect);

	m_pEdge->DrawRect(pGDI, Rect);
	}

void CPrimCamera::DrawSeq(IGDI *pGDI, R2 Rect)
{
	if( FALSE ) {

		CString Text = CPrintf("Seq %d", m_Ctx.m_uFrame);

		int cx = pGDI->GetTextWidth(Text);

		int cy = pGDI->GetTextHeight(Text);

		int px = Rect.x1 + (Rect.x2 - Rect.x1 - cx) / 2;

		int py = Rect.y2 - (cy + 2);

		pGDI->ResetFont();

		SelectFont(pGDI, fontDefault);

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SetTextFore(GetRGB(0,0,0));

		pGDI->TextOut(px, py, Text);
		}
	}

// Scaling

void CPrimCamera::ScaleImageNearest(PCDWORD pSource, int iWidth, int iHeight, float Scale, PDWORD pOutput)
{
	UINT uNewWidth  = UINT(iWidth  * Scale);

	UINT uNewHeight = UINT(iHeight * Scale);

	float recip = 1.0f / Scale;

	float posY  = 0.0f;
	
	UINT uLine  = 0;

	for( UINT y = 0; y < uNewHeight; y++ ) {

		UINT yp    = UINT(posY);

		UINT uSrc  = yp * iWidth;

		float posX = 0.0f;

		for( UINT x = 0; x < uNewWidth; x++ ) {

			UINT xp = UINT(posX);
			
			pOutput[x + uLine] = pSource[xp + uSrc];

			posX += recip;
			}

		uLine += uNewWidth;

		posY += recip;
		}
	}

#define LERP(s, e, t) ((s)+((e)-(s))*(t))

#define BLERP(c00, c10, c01, c11, tx, ty) LERP(LERP((c00), (c10), (tx)), LERP((c01), (c11), (tx)), (ty))

void CPrimCamera::ScaleImageBilinear(PCDWORD pSource, int iWidth, int iHeight, float Scale, PDWORD pOutput)
{
	UINT uWidth  = UINT(iWidth  * Scale);

	UINT uHeight = UINT(iHeight * Scale);

	UINT uLine = 0;
		
	float xInc = (1.0f / (float) uWidth ) * (iWidth  - 1);
	
	for( UINT y = 0; y < uHeight; y++ ) {
		
		float gy = (y / (float) uHeight) * (iHeight - 1);

		UINT gyi = UINT(gy);

		float dy = gy - gyi;

		float gx = 0.0f;

		UINT uSrc  = iWidth *  gyi;
		UINT uNext = iWidth * (gyi + 1);

		for( UINT x = 0; x < uWidth; x++ ) {
			
			UINT gxi = UINT(gx);

			DWORD c00 = pSource[gxi     + uSrc ];
			DWORD c10 = pSource[gxi + 1 + uSrc ];
			DWORD c01 = pSource[gxi     + uNext];
			DWORD c11 = pSource[gxi + 1 + uNext];

			PBYTE p00 = PBYTE(&c00);
			PBYTE p01 = PBYTE(&c01);
			PBYTE p10 = PBYTE(&c10);
			PBYTE p11 = PBYTE(&c11);

			DWORD dwPixel;

			PBYTE pPix = PBYTE(&dwPixel);

			float dx = gx - gxi;

			pPix[0] = BYTE(BLERP(p00[0], p10[0], p01[0], p11[0], dx, dy));
			pPix[1] = BYTE(BLERP(p00[1], p10[1], p01[1], p11[1], dx, dy));
			pPix[2] = BYTE(BLERP(p00[2], p10[2], p01[2], p11[2], dx, dy));
			
			pOutput[uLine + x] = dwPixel;

			gx += xInc;
			}
		
		uLine += uWidth;
		}
	}

// Context Handling

void CPrimCamera::FindCtx(CCtx &Ctx)
{
	Ctx.m_uFrame = m_pCamera ? m_pCamera->GetInfo(m_uDevice, 1) : NOTHING;
	}

void CPrimCamera::ClearCtx(CCtx &Ctx)
{
	Ctx.m_uFrame = NOTHING;
	}

// Context Check

BOOL CPrimCamera::CCtx::operator == (CCtx const &That) const
{
	return m_uFrame   == That.m_uFrame ;
	}

// End of File
