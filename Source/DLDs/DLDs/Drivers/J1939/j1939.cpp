
#include "intern.hpp"

#include "j1939.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Debugging Help

BOOL m_fDebug = 0;

UINT m_fPrint = 0;

UINT m_uTO    = 0;

//////////////////////////////////////////////////////////////////////////
//
// J1939 Driver
//

// Constructor

CCANJ1939Driver::CCANJ1939Driver(void)
{
	m_Ident  = DRIVER_ID;

	m_fDebug = FALSE;

	m_fPrint = FALSE;
       	}

// Destructor

CCANJ1939Driver::~CCANJ1939Driver(void)
{
	}

// Configuration

void MCALL CCANJ1939Driver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSrc  = GetWord(pData) & 0xFF;

		m_bSrc2 = GetWord(pData) & 0xFF;
		}
	}
	
void MCALL CCANJ1939Driver::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bSrc | m_bSrc2;

	Config.m_uFlags |= flagPrivate;

	Config.m_uFlags |= flagExtID;
	}
	
// Management

void MCALL CCANJ1939Driver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCANJ1939Handler(m_pHelper);

	m_pHandler->SetSA(m_bSrc, m_bSrc2);

	pPort->Bind(m_pHandler);
	}

void MCALL CCANJ1939Driver::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CCANJ1939Driver::Open(void)
{	
	}

// Device

CCODE MCALL CCANJ1939Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDA = GetByte(pData);

			m_pCtx->m_uBackOff = GetLong(pData);

			m_pCtx->m_bClrDTC = GetByte(pData);

			m_pHandler->SetClearDTC(m_pCtx->m_bDA, m_pCtx->m_bClrDTC);

			UINT uPGN = GetWord(pData);

			m_pCtx->m_uPGNs = uPGN + 2;

			m_pCtx->m_pPGNs = new PGN[m_pCtx->m_uPGNs];

			ID id;

			for( UINT x = 0; x < uPGN; x++ ) {

				m_pCtx->m_pPGNs[x].m_Number = GetLong(pData);

				m_pCtx->m_pPGNs[x].m_Diag = GetByte(pData);

				m_pCtx->m_pPGNs[x].m_Priority = GetByte(pData);

				m_pCtx->m_pPGNs[x].m_SendReq = GetByte(pData);

				m_pCtx->m_pPGNs[x].m_uRequest = GetTickCount();

				UINT uSPN  = GetWord(pData);

				m_pCtx->m_pPGNs[x].m_uSPNs = uSPN;

				m_pCtx->m_pPGNs[x].m_pSPNs  = new SPN[uSPN];

				for( UINT y = 0; y < uSPN; y++ ) {

					m_pCtx->m_pPGNs[x].m_pSPNs[y].m_Size = GetByte(pData) & 0x7F;

					m_pCtx->m_pPGNs[x].m_pSPNs[y].m_Number = GetWord(pData);
				}

				m_pCtx->m_pPGNs[x].m_RepRate = GetLong(pData);

				m_pCtx->m_pPGNs[x].m_Enh     = GetByte(pData);

				m_pCtx->m_pPGNs[x].m_Dir     = GetByte(pData);
			}

			m_pCtx->m_fSrc = GetByte(pData) ? TRUE : FALSE;

			m_pHandler->SetSecondaryUsed(m_pCtx->m_fSrc);

			for( UINT y = 0; y < uPGN; y++ ) {

				FindID(id, m_pCtx->m_pPGNs[y].m_Number, m_pCtx->m_pPGNs[y].m_Priority, m_pCtx->m_fSrc);

				UINT uBytes = FindPGNByteSize(&m_pCtx->m_pPGNs[y], m_pCtx->m_pPGNs[y].m_uSPNs);

				m_pHandler->MakePDU(&id, uBytes, m_pCtx->m_pPGNs[y].m_RepRate,
						    m_pCtx->m_pPGNs[y].m_Diag,
						    m_pCtx->m_pPGNs[y].m_Enh,
						    m_pCtx->m_pPGNs[y].m_Dir,
						    m_pCtx->m_fSrc);
			}
									 
			m_pCtx->m_pPGNs[m_pCtx->m_uPGNs - 2].m_Number = CLAIM_REQ;

			FindID(id, CLAIM_REQ, 6, m_pCtx->m_fSrc);

			m_pHandler->MakePDU(&id, 0, 0, 0, 0, 0, m_pCtx->m_fSrc);

			m_pCtx->m_pPGNs[m_pCtx->m_uPGNs - 1].m_Number = CLAIM_RESP;

			FindID(id, CLAIM_RESP, 6, m_pCtx->m_fSrc);

			m_pHandler->MakePDU(&id, 0, 0, 0, 0, 0, m_pCtx->m_fSrc);
			
			m_pCtx->m_uLast = GetTickCount();
			
			pDevice->SetContext(m_pCtx);

			m_pHandler->ShowPDUs();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
 	}

CCODE MCALL CCANJ1939Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pCtx->m_pPGNs->m_pSPNs;

		delete [] m_pCtx->m_pPGNs;
		
		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CCANJ1939Driver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	if( uFunc == 1 ) {

		if( UserSendPGNRqst((CContext *) pContext, ATOI(Value)) ) {

			return TRUE;
		}
	}

	else if( uFunc == 2 ) {

		if( UserSendPGNData((CContext *) pContext, ATOI(Value)) ) {

			return TRUE;
		}
	}

	return 0;
}

// Entry Points

CCODE CCANJ1939Driver::Ping(void)
{
	return m_pHandler->ClaimAddress(m_pCtx->m_fSrc, transDriver);
	}

CCODE CCANJ1939Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	//AfxTrace("Read %4.4x %4.4x %4.4x %4.4x %4.4x\n", Addr.a.m_Table, Addr.a.m_Type, Addr.a.m_Offset, Addr.a.m_Extra, uCount);

	ID id;				      

	if( FindID(id, Addr, m_pCtx->m_fSrc) ) {

		PDU * pPDU = m_pHandler->FindPDU(&id, dirRx, m_pCtx->m_fSrc);

		BOOL fPrint = FALSE;

		if( m_fDebug && id.i.m_PF == 0xFE && id.i.m_PS == 0xCA && m_fPrint ) {

			fPrint = TRUE;

			AfxTrace("\nR : ");
			}

		if( pPDU ) {

			PGN * pPG = FindPG(pPDU);

			if( pPG ) {

				if( IsCommand(pPG) ) {

					return uCount;
					}
			       
				if( pPG->m_SendReq ) {

					if( pPDU->m_uLive && m_pHandler->IsTimedOut(pPG->m_uRequest, m_pCtx->m_uBackOff) ) {

						if( !m_pHandler->SendPGNRequest(pPG, m_pCtx->m_bDA, m_pCtx->m_uBackOff, m_pCtx->m_fSrc, transDriver) ) {


							return CCODE_ERROR;
							}
						}
					}

				if( !m_pHandler->HasData(pPDU) ) {

					return CCODE_SILENT;
					}

				if( m_pCtx->m_bDA < 255 ) {

					UINT uIndex = Addr.a.m_Offset & 0x3F;

					UINT uShift = 0;

					UINT uDead  = 0;

					for( UINT x = 0, y = 0; x < pPG->m_uSPNs; x++ ) {

						UINT uSize   = FindSPNBitSize(pPG, x);

						if( IsDead(pPG, x) ) {

							uShift += uSize;

							uDead++;

							continue;
							} 

						if( x - uDead == uIndex || ( x - uDead > uIndex && x < uIndex + uCount + uDead ) ) {

							UINT uMask = 0x1 << (uSize - 1);
				
							while( !(uMask & 0x1) ) {

								uMask |= uMask >> 1;
								}
						
							UINT uBegin   = uShift / 8;

							UINT uEnd     = (uShift + uSize) / 8;

							DWORD dwData  = 0;

							UINT i        = 0;

							UINT s	      = uShift % 8;
					
							PBYTE pBytes  = PBYTE(pPDU->m_pData + uBegin);

							dwData |= pBytes[i] >> s;

							while( uEnd > uBegin ) {

								i++;

								dwData |= (pBytes[i] << (8 * i)) >> s;

								uBegin++;
					   			}

							dwData = dwData & uMask;

							pData[y] = dwData;

							if( fPrint ) {

								AfxTrace("%u ", pData[y]);

								m_fPrint = 0;
								}
							y++;
							}
												
						uShift += uSize;
						}

					return uCount;
					} 
				}

			return CCODE_ERROR;
			}
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CCANJ1939Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	//AfxTrace("Write %4.4x %4.4x %4.4x %4.4x %4.4x %8.8x\n", Addr.a.m_Table, Addr.a.m_Type, Addr.a.m_Offset, Addr.a.m_Extra, uCount, pData[0]);

	ID id;

	if( FindID(id, Addr, m_pCtx->m_fSrc) ) {

		PDU * pPDU = m_pHandler->FindPDU(&id, dirTx, m_pCtx->m_fSrc);

		if( pPDU ) {

			PGN * pPG = FindPG(pPDU);

			if( pPG ) {

				if( IsCommand(pPG) ) {

					if( pData[0] ) {

						m_pHandler->SendPGNRequest(pPG, m_pCtx->m_bDA, m_pCtx->m_uBackOff, m_pCtx->m_fSrc, transDriver);
						}

					return uCount;
					}

				UINT uShift = 0;

				UINT uSize  = 0;

				UINT uIndex = Addr.a.m_Offset & 0x3F;

				BOOL fPass  = FALSE;

				UINT uDead  = 0;

				UINT uEnd   = 0;

				if( IsDM(pPG) ) {

					FlopBytes(pPDU, TRUE);
					}

				for( UINT x = 0, y = 0; x < pPG->m_uSPNs; x++ ) {

					DWORD Data = 0;

				 	if( IsDead(pPG, x) ) {

						uDead++;

						Data  = 0xFFFFFFFF;

						fPass = TRUE;
						}
						 					
					else if( x - uDead == uIndex || ( x - uDead > uIndex && x < uIndex + uCount + uDead ) ) {
						
						Data  = pData[y];
					
						y++;

						fPass = TRUE;
						}
				
					uSize = FindSPNBitSize(pPG, x);

					if( fPass ) {

						UINT uBegin = uShift / 8;

						uEnd        = (uShift + uSize) / 8;

						UINT uMask  = 0x1 << (uSize - 1);

						while( !(uMask & 0x1) ) {

						 	uMask |= uMask >> 1;
							}

						PBYTE pBytes = PBYTE(pPDU->m_pData + uBegin);

						UINT i = 0;

						UINT s = uShift % 8;

						while( uEnd >= uBegin ) {

							*pBytes &= ~(uMask << s) >> i;

							*pBytes |= (((Data & uMask) << s) >> i);

							pBytes++;
						
							i += 8;

							if( i >= sizeof(DWORD) * 8 ) {

								break;
								}

							uBegin++;
							}
						
						fPass = FALSE;
						}

					uShift += uSize;
					}

				if( IsDM(pPG) ) {

					FlopBytes(pPDU, FALSE);
					}

				SetDirection(pPDU, TRUE);

				if( m_pHandler->SendPDU(pPDU, FALSE, transDriver) ) {

					return uCount;
					}
				}

			return CCODE_ERROR;
			}
		}
		
	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

BOOL  CCANJ1939Driver::FindID(ID &id, UINT uID, UINT uPriority, BOOL fSrc)
{
	if( m_pCtx ) {

		id.i.m_X   = 0;
	
		id.i.m_P   = uPriority;

		id.i.m_EDP = 0;

		id.i.m_DP  = (uID & 0x10000) >> 16;

		id.i.m_PF  = (uID & 0x0FF00) >> 8;

		if( id.i.m_PF < 0xF0 ) {

			id.i.m_PS  = m_pHandler->GetSA(fSrc);
			}
		else {
			id.i.m_PS  = uID & 0xFF;
			}

		id.i.m_SA  = m_pCtx->m_bDA;
	       
		return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Driver::FindID(ID &id, AREF Addr, BOOL fSrc)
{
	if( m_pCtx ) {

		UINT uID   = Addr.a.m_Offset & 0xFF3F;

		UINT uPF   = Addr.a.m_Extra | (Addr.a.m_Offset & 0x80) >> 3;

		id.i.m_X   = 0;
	
		id.i.m_P   = 0x3;

		id.i.m_EDP = 0;

		id.i.m_DP  = (Addr.a.m_Offset & 0x40) >> 6;

		if( uPF > 0 ) {

			uID  = uID >> 8;

			uID |= (uPF - 1) * 0x100 + 0xF000;
			}

		id.i.m_PF  = (uID & 0x0FF00) >> 8;

		if( id.i.m_PF < 0xF0 ) {

			id.i.m_PS  = m_pHandler->GetSA(fSrc);
			}
		else {
			id.i.m_PS  = uID & 0xFF;
			}

		id.i.m_SA  = m_pCtx->m_bDA;

		return TRUE;
		}

	return FALSE;
	}


PGN * CCANJ1939Driver::FindPG(PDU * pPDU)
{
	if( pPDU && m_pCtx ) {

		UINT uMask = m_pHandler->GetMask();

		UINT uDA = m_pCtx->m_bDA;

		BOOL fSpecific = pPDU->m_ID.i.m_PF < 0xF0;

		for( UINT u = 0; u < m_pCtx->m_uPGNs; u++ ) {

			UINT uPGN = (m_pCtx->m_pPGNs[u].m_Number << 8) | m_pCtx->m_bDA;

			if( fSpecific ) {

				uPGN |= (m_pHandler->GetSA(m_pCtx->m_fSrc) << 8);
				}
			 
			if( ((uPGN & uMask) == (pPDU->m_ID.m_Ref & uMask)) ) {

				return &m_pCtx->m_pPGNs[u];
				}
			}
		}

	return NULL;
	}

UINT  CCANJ1939Driver::FindPGNByteSize(PGN * pPGN, UINT uCount)
{
	UINT uSize = FindPGNBitSize(pPGN, uCount);

	return ( uSize % 8 ) ? uSize / 8 + 1 : uSize / 8;
	}

UINT  CCANJ1939Driver::FindPGNBitSize(PGN * pPGN, UINT uCount)
{
	UINT uSize = 0;

	if( pPGN ) {

		for( UINT u = 0; u < uCount; u++ ) {

			uSize += FindSPNBitSize(pPGN, u);
			}
		}

	return uSize;
	}

UINT CCANJ1939Driver::FindSPNBitSize(PGN * pPGN, UINT uIndex)
{
	UINT uSize = pPGN->m_pSPNs[uIndex].m_Size; 

	uSize = GetSpecialSize(uIndex, uSize, pPGN);

	return uSize;
	}

void CCANJ1939Driver::SetDirection(PDU * pPDU, BOOL fSend)
{
	pPDU->m_fSend = fSend;
	}

UINT CCANJ1939Driver::GetSpecialSize(UINT uIndex, UINT uSize, PGN * pPGN)
{
	if( IsDM(pPGN) ) {
	
		if( uSize == 24 ) {

			return 19;
			}
		}

	return uSize;
	}

void CCANJ1939Driver::FlopBytes(PDU * pPDU, BOOL fInit)
{	
	UINT uStart = m_pHandler->GetFlopStart(pPDU);

	if( uStart < NOTHING ) {

		UINT uEnd  = min(pPDU->m_uBytes, m_pHandler->GetFlopEnd(pPDU));

		const BYTE * pMask = (fInit ? INIT_MASK : SEND_MASK);

		const BYTE * pShft = (fInit ? INIT_SHFT : SEND_SHFT);

		for( UINT u = uStart; u < uEnd; u += 3 ) {

			BYTE bLo = (pPDU->m_pData[u] & pMask[0]) >> pShft[0];

			BYTE bHi = (pPDU->m_pData[u] & pMask[1]) << pShft[1];

			pPDU->m_pData[u] = bHi | bLo;

			u++;

			bLo = (pPDU->m_pData[u] & pMask[2]) >> pShft[2];
		
			bHi = (pPDU->m_pData[u] & pMask[3]) << pShft[3];

			pPDU->m_pData[u] = bHi | bLo;
			}
		}
	}

BOOL CCANJ1939Driver::IsDM(PGN * pPGN)
{
	switch( pPGN->m_Number ) {

		case 65226:
		case 65227:
		case 65229:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Driver::IsDead(PGN * pPGN, UINT uOffset)
{
	return pPGN->m_pSPNs[uOffset].m_Number == UNSPECIFIED;
	}

BOOL CCANJ1939Driver::IsCommand(PGN * pPGN)
{
	switch( pPGN->m_Number ) {

		case 65228:
		case 65235:

			return TRUE;
		}

	return FALSE;
	}

// User Access Methods

BOOL CCANJ1939Driver::UserSendPGNRqst(CContext * pCtx, UINT uPGN)
{
	for( UINT u = 0; u < pCtx->m_uPGNs; u++ ) {

		PGN * pPGN = &pCtx->m_pPGNs[u];

		if( pPGN->m_Number == uPGN ) {

			if( pPGN->m_Dir != dirTx ) {

				if( m_pHandler->SendPGNRequest(pPGN, pCtx->m_bDA, pCtx->m_uBackOff, pCtx->m_fSrc, transDriver) ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

BOOL CCANJ1939Driver::UserSendPGNData(CContext * pCtx, UINT uPGN)
{
	for( UINT u = 0; u < pCtx->m_uPGNs; u++ ) {

		PGN * pPGN = &pCtx->m_pPGNs[u];

		if( pPGN->m_Number == uPGN ) {

			if( pPGN->m_Dir != dirRx ) {

				ID id;

				if( FindID(id, pPGN->m_Number, pPGN->m_Priority, pCtx->m_fSrc) ) {

					PDU * pPDU = m_pHandler->FindPDU(&id, pPGN->m_Dir, pCtx->m_fSrc);

					if( pPDU ) {

						if( m_pHandler->SendPDU(pPDU, FALSE, transDriver) ) {

							return TRUE;
						}
					}
				}
			}
		}
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// J1939 Handler
//

// Constructor

CCANJ1939Handler::CCANJ1939Handler(IHelper *pHelper)
{
	StdSetRef();

	m_uPDUs      = 0;

	m_pTxData    = NULL;

	m_pPort      = NULL;

	m_uTx	     = 0;
	
	m_uRx        = 0;

	m_uQueue     = 0;
	
	m_uSend      = 0;

	for( UINT x = 0; x < DEVICES; x++ ) {

		m_pTransfer[x]  = NULL;

		m_pBroadcast[x] = NULL;
		}

	m_fSet     = TRUE;

	m_uTimer   = 0;

	m_Diag	   = 0;

	memset(m_Diag1, 0xFF, DEVICES);

	m_fSuspend = FALSE;

	m_fUpdate  = FALSE;

	m_pHelper  = pHelper;

	m_pExtra   = NULL;

	m_pHelper->MoreHelp(IDH_EXTRA, (void **) &m_pExtra);

	memset(m_bClrDTC, 0, DEVICES);

	for( UINT s = 0; s < elements(m_fClaim); s++ ) {

		m_fClaim[s]		= FALSE;

		m_fClaimSent[s]	= FALSE;

		m_uClaim[s]		= 0;

		m_Name[s]		= NULL;
		}

	m_fSecondary = FALSE;
	}

// Destructor

CCANJ1939Handler::~CCANJ1939Handler(void)
{
	}

// IUnknown

HRESULT METHOD CCANJ1939Handler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
	}

ULONG METHOD CCANJ1939Handler::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CCANJ1939Handler::Release(void)
{
	StdRelease();
	}

// IPortHandler

void METHOD CCANJ1939Handler::Bind(IPortObject * pPort)
{
	m_pPort = pPort;
	}

void METHOD CCANJ1939Handler::OnOpen(CSerialConfig const &Config)
{
	m_pTxData    = NULL;

	m_uTx	     = 0;
	
	m_uRx        = 0;

	m_uQueue     = 0;
	
	m_uSend      = 0;

	m_nTxDisable = 0;

	m_fTimer     = TRUE;

	memset(m_Diag1, 0xFF, DEVICES);

	memset(PBYTE(m_Diag2), 0xFF, DEVICES * sizeof(DWORD));

	for( UINT s = 0; s < elements(m_fClaim); s++ ) {

		m_fClaim[s] = FALSE;
		}
	}

void METHOD CCANJ1939Handler::OnClose(void)
{
	}

void METHOD CCANJ1939Handler::OnRxData(BYTE bData)
{
	if( m_uRx < sizeof(FRAME_EXT) ) {

		PBYTE pRx = PBYTE(&m_Rx);

		pRx[m_uRx] = bData;

		m_uRx++;
		}
	}

void METHOD CCANJ1939Handler::OnRxDone(void)
{
	if( m_uRx == sizeof(FRAME_EXT) ) {

		if( !m_fSuspend ) {

			RecvFrame();
			}

		m_uRx = 0;
		}
	}

BOOL METHOD CCANJ1939Handler::OnTxData(BYTE &bData)
{
	if( m_uTx > 0 ) {

		if( m_pTxData ) {

			m_uTx--;

			bData = *m_pTxData++;

			return TRUE;
			}
		}

	Increment(m_uSend);

	m_pTxData = NULL;

	return FALSE;
	}

void METHOD CCANJ1939Handler::OnTxDone(void)
{
	if( !m_pTxData && !m_nTxDisable ) { 

		if( m_uSend != m_uQueue ) {

			SendFrame();
			}
		}
	}

void METHOD CCANJ1939Handler::OnTimer(void)
{
	for( UINT s = 0; s < elements(m_fClaim); s++ ) {

		if( !s || m_fSecondary ) {

			if( !m_fClaim[s] && !m_fClaimSent[s] ) {

				ClaimAddress(s, transTimer);
				}

			if( !m_fClaim[s] && m_fClaimSent[s] ) {

				if( GetTickCount() >= m_uClaim[s] + ToTicks(1250) ) {

					m_fClaim[s]     = TRUE;

					m_fClaimSent[s] = FALSE;
					}
				}
			}
		}

	for( UINT x = 0; x < m_uPDUs; x++ ) {

		PDU * pPDU = &m_pPDUs[x];

		if( pPDU ) {

			if( pPDU->m_pTransfer ) {
				
				if( pPDU->m_bState[TRANS_SEND] != STATE_BEGIN ) {  

					if( IsTimedOut(pPDU->m_uTime[TRANS_SEND], FindTimeout(pPDU->m_bState[TRANS_SEND])) ) {

						if( m_fDebug ) {

							m_uTO++;
						
							AfxTrace("\nTX MPTO state %2.2x pdu %8.8x", pPDU->m_bState[TRANS_SEND], pPDU->m_ID.m_Ref);
							}

						if( !Retry(pPDU->m_bState[TRANS_SEND]) ) {

							if( SetState(pPDU, TRANS_SEND, STATE_RESET, transTimer) ) {

								if( IsDestinationSpecific(pPDU->m_ID) ) {

									ConnAbort(pPDU, ABORT_TIMEOUT, transTimer);
									}
								}
							}
						else {
							if( SetState(pPDU, TRANS_SEND, STATE_RETRY, transTimer) ) {
							
								SendPDU(pPDU, TRUE, transTimer);
								}
							}
						}

					if( (IsDestinationSpecific(pPDU->m_ID) && pPDU->m_fCTS[TRANS_SEND]) || !IsDestinationSpecific(pPDU->m_ID) ) {

						if( IsTimedOut(pPDU->m_uTime[TRANS_SEND], 50) ) {

							TransferData(pPDU, 0, pPDU->m_bSequence[TRANS_SEND], transTimer);
							}
						}
					}

				if( pPDU->m_bState[TRANS_RECV] != STATE_BEGIN ) {

					if( IsTimedOut(pPDU->m_uTime[TRANS_RECV], FindTimeout(pPDU->m_bState[TRANS_RECV])) ) {

						if( m_fDebug ) {

							m_uTO++;

							AfxTrace("\nRX MPTO state %2.2x pdu %8.8x", pPDU->m_bState[TRANS_RECV], pPDU->m_ID.m_Ref);
							}

						if( SetState(pPDU, TRANS_RECV, STATE_RESET, transTimer) ) {

							BYTE bSrc = pPDU->m_ID.m_Ref & 0xFF;

							if( m_pTransfer[bSrc] == pPDU ) {

								ConnAbort(pPDU, ABORT_TIMEOUT, transTimer);

								m_pTransfer[bSrc] = NULL;
								}

							if( m_pBroadcast[bSrc] == pPDU ) {

								m_pBroadcast[bSrc] = NULL;
								}
							}
						}
					}
				}

			if( pPDU->m_fSend && !IsRapidFire(pPDU) ) {
				
				if( IsTimedOut(pPDU->m_uReq, pPDU->m_uLive / 3 - 2) ) {
					
					if( SendPDU(pPDU, FALSE, transTimer) ) {

						UINT uTick = GetTickCount();
				        
						pPDU->m_uReq = uTick ? uTick : 1;
						}
					}
				}

			else if( pPDU->m_uSet > 0 && pPDU->m_uLive > 0 && !pPDU->m_bDiag ) {

				if( IsTimedOut(pPDU->m_uSet, pPDU->m_uLive) ) {

					if( m_fDebug && IsDM1(pPDU) ) {

						if( m_uTO < 3 ) {

							AfxTrace("\nLive TO : %u, %u, %u", GetTickCount(), pPDU->m_uSet, pPDU->m_uLive);
							}

						m_uTO++;
						}

					if( IsRapidFire(pPDU) ) {

						memcpy(pPDU->m_pData, pPDU->m_pEnhanced, pPDU->m_uBytes);
						}
					else {  
						ClearData(pPDU);
						}
					}
				}
			}
		}
	}

// Network Management

CCODE CCANJ1939Handler::ClaimAddress(BOOL fSrc, UINT uTrans)
{
	if( !m_fClaim[fSrc] && !m_fClaimSent[fSrc] ) {

		if( SourceAddrClaim(fSrc, uTrans) ) {

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}
 
	return CCODE_SUCCESS;
	}

// FRAME_EXT Access

BOOL CCANJ1939Handler::SendPDU(PDU * pPDU, BOOL fRetry, UINT uTrans)
{
	if( m_fClaim[pPDU->m_fSrc] && IsSendPDU(pPDU) ) {

		if( IsMultipacket(pPDU) ) {

			return SendMultipacketPDU(pPDU, fRetry, uTrans);
			} 

		Start(uTrans);

		ID id;	   

		id.m_Ref   = pPDU->m_ID.m_Ref;

		if( IsDestinationSpecific(id) ) {

			id.i.m_PS  = id.i.m_SA;
			}

		id.i.m_SA  = GetSA(pPDU->m_fSrc);
       
		AddID(id, uTrans);

		AddCtrl(pPDU, uTrans);

		AddData(pPDU, uTrans);

		return PutFrame(uTrans);
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::SendPGNRequest(PGN * pPG, BYTE bDA, UINT uBackOff, BOOL fSrc, UINT uTrans)
{
	if( m_fClaim[fSrc] ) {

		ID id;

		id.m_Ref  = (pPG->m_Number << 8) | bDA;

		UINT uPGN = pPG->m_Number;

		if( IsDestinationSpecific(id) ) {

			id.i.m_PS = GetSA(fSrc);

			uPGN     |= bDA;


		}

		PDU * pPDU = FindPDU(&id, dirRx, fSrc);

		if( pPDU ) {

			if( !IsTimedOut(pPDU->m_uSet, pPDU->m_uLive / 3 + uBackOff) ) {

				return TRUE;
			}

			if( IsMultipacket(pPDU) ) {

				if( pPDU->m_fCTS[TRANS_RECV] ) {

					return TRUE;
				}
			}

			Start(uTrans);

			FindID(id, PGN_REQ, pPG->m_Priority, fSrc);

			id.i.m_PS = bDA;

			AddID(id, uTrans);

			AddCtrl(3, uTrans);

			AddPGN(uPGN, uTrans);

			BOOL fPut = PutFrame(uTrans);

			if( fPut ) {

				pPG->m_uRequest = GetTickCount();
			}

			return fPut;
		}


	}

	return TRUE;
}


// Data Access

void CCANJ1939Handler::MakePDU(ID * pID, UINT uBytes, UINT uLive, UINT uDiag, UINT uEnh, UINT uDir, BOOL fSrc, DWORD dwRef, UINT uExt)
{
	if( pID ) {

		PDU * pPDU = new PDU;

		memset(pPDU, 0, sizeof(PDU));

		pPDU->m_ID     = *pID;

		pPDU->m_uBytes = uBytes;

		pPDU->m_uTotal = uBytes;

		pPDU->m_pData  = NULL;

		pPDU->m_uLive  = uLive;

		pPDU->m_bDiag  = uDiag;

		pPDU->m_bEnh   = uEnh;

		pPDU->m_Ref    = dwRef;

		pPDU->m_bExt   = uExt;

		pPDU->m_bDir   = uDir;

		pPDU->m_fSrc   = fSrc;
		
		GrowPDUs(pPDU);

		delete pPDU;
		}
      	}

PDU* CCANJ1939Handler::FindPDU(ID * pID, UINT uDir)
{
	if( pID ) {

		UINT uMask = GetMask();

		for( UINT u = 0; u < m_uPDUs; u++ ) {

			if( (pID->m_Ref & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask) ) {

				if( uDir == dirTx && IsSendPDU(&m_pPDUs[u]) ) {

					return &m_pPDUs[u];
				}

				if( uDir == dirRx && IsSetPDU(&m_pPDUs[u]) ) {

					return &m_pPDUs[u];
				}
			}
		}
	}

	return NULL;
}

PDU* CCANJ1939Handler::FindPDU(ID * pID, UINT uDir, BOOL fSrc)
{
	if( pID ) {

		UINT uMask = GetMask();

		for( UINT u = 0; u < m_uPDUs; u++ ) {

			if( fSrc == m_pPDUs[u].m_fSrc ) {

				if( (pID->m_Ref & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask) ) {

					if( uDir != dirRx && IsSendPDU(&m_pPDUs[u]) ) {

						return &m_pPDUs[u];
					}

					if( uDir != dirTx && IsSetPDU(&m_pPDUs[u]) ) {

						return &m_pPDUs[u];
					}
				}
			}
		}
	}

	return NULL;
}


PDU* CCANJ1939Handler::GetPDU(UINT uIndex)
{
	if( uIndex < m_uPDUs ) {

		return &m_pPDUs[uIndex];
		}

	return NULL;
	}

BYTE CCANJ1939Handler::GetSA(BOOL fSrc)
{
	return m_bSrc[fSrc];
}

void CCANJ1939Handler::SetSA(BYTE bSA, BYTE bSA2)
{
	for( UINT s = 0; s < elements(m_Name); s++ ) {

		if( !m_Name[s] ) {

			m_bSrc[s] = !s ? bSA : bSA2;

			m_Name[s] = new NAME;

			memset(PBYTE(m_Name[s]), 0, sizeof(NAME));

			m_Name[s]->a.m_Arb = 0;

			m_Name[s]->a.m_IG  = 0;	   // Global

			m_Name[s]->a.m_VSI = 0;

			m_Name[s]->a.m_VS  = 0x7F; // Not Available

			m_Name[s]->a.m_R   = 0;

			m_Name[s]->a.m_F   = 0xFF; // Not Available

			m_Name[s]->a.m_FI  = 0;

			m_Name[s]->a.m_ECUI= 0;

			m_Name[s]->b.m_MC  = 267;  // Assigned by SAE

			CMacAddr Mac;

			AfxGetAutoObject(pUtils, "ip", 0, INetUtilities);

			if( pUtils ) {

				pUtils->GetInterfaceMac(0, Mac);
				}

			m_Name[s]->b.m_ID = MotorToHost((DWORD &) Mac.m_Addr[s ? 4 : 2]);

			switch( m_bSrc[s] ) {

				case 38:
					m_Name[s]->a.m_F = 29;
					break;
				case 40:
					m_Name[s]->a.m_F = 60;
					break;
				case 251:
					m_Name[s]->a.m_F = 130;
					break;

				default:
					m_Name[s]->a.m_F = 0xFF;
			}
		}
	}
}

BOOL CCANJ1939Handler::GetSrc(ID * id)
{
	if( m_fSecondary ) {

		return id->i.m_PS == m_bSrc[1];
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HasData(PDU * pPDU)
{
	return pPDU->m_uSet;
	}

UINT CCANJ1939Handler::GetMask(void)
{
	return 0x3FFFFFF;
	}

void CCANJ1939Handler::Suspend(BOOL fSuspend)
{
	m_fSuspend = fSuspend;
	}

BOOL CCANJ1939Handler::HasUpdate(void)
{
	BOOL fUpdate = m_fUpdate;

	m_fUpdate = FALSE;

	return fUpdate;
	}

void CCANJ1939Handler::SetClearDTC(BYTE bDA, BYTE bClrDTC)
{
	m_bClrDTC[bDA] = bClrDTC;
	}

UINT CCANJ1939Handler::GetFlopStart(PDU * pPDU)
{
	UINT uPGN = (pPDU->m_ID.m_Ref & 0x00FFFF00) >> 8;

	switch( uPGN ) {

		case 65226:
		case 65227:
			
			return 4;

		case 65229:

			return 3;
		}

	return NOTHING;
	}

UINT CCANJ1939Handler::GetFlopEnd(PDU * pPDU)
{
	UINT uPGN = (pPDU->m_ID.m_Ref & 0x00FFFF00) >> 8;

	switch( uPGN ) {

		case 65226:
		case 65227:
			
			return pPDU->m_uBytes;

		case 65229:

			return 6;
		}

	return NOTHING;
	}

void CCANJ1939Handler::SetSecondaryUsed(BOOL fSrc)
{
	if( !m_fSecondary ) {

		m_fSecondary = fSrc;
	}
}


// Testing / Debug

void CCANJ1939Handler::ShowPDUs(void)
{
/*	AfxTrace("Source %u\n", m_bSrc);

	for( UINT u = 0; u < m_uPDUs; u++ ) {

		AfxTrace("PDU %u of %u at %8.8x...\n", u + 1, m_uPDUs, &m_pPDUs[u]);

		AfxTrace("PDU %8.8x has %u bytes and lives %u dir %u\n", m_pPDUs[u].m_ID, m_pPDUs[u].m_uBytes, m_pPDUs[u].m_uLive, m_pPDUs[u].m_bDir);
		}
*/	}

// Implementation

void CCANJ1939Handler::Start(UINT uTrans)
{
	memset(&m_Tx[uTrans], 0x0, sizeof(FRAME_EXT));
	}

void CCANJ1939Handler::AddByte(BYTE bByte, UINT &uPos, UINT uTrans)
{
	m_Tx[uTrans].m_bData[uPos++] = bByte;
	}

void CCANJ1939Handler::AddWord(WORD wWord, UINT &uPos, UINT uTrans)
{
	AddByte(LOBYTE(wWord), uPos, uTrans);

	AddByte(HIBYTE(wWord), uPos, uTrans);
	}

void CCANJ1939Handler::AddID(DWORD dwID, UINT &uPos, UINT uTrans)
{
	AddByte(LOBYTE(LOWORD(dwID)), uPos, uTrans);

	AddByte(HIBYTE(LOWORD(dwID)), uPos, uTrans);

	AddByte(LOBYTE(HIWORD(dwID)), uPos, uTrans);
  	}

void CCANJ1939Handler::AddID(PDU * pPDU, UINT uTrans)
{
	AddID(pPDU->m_ID, uTrans);
	}

void CCANJ1939Handler::AddID(ID id, UINT uTrans)
{
	m_Tx[uTrans].m_ID  = id.m_Ref;
	}

void CCANJ1939Handler::AddCtrl(PDU * pPDU, UINT uTrans)
{
       	AddCtrl(pPDU->m_uBytes, uTrans);
	}

void CCANJ1939Handler::AddCtrl(UINT uBytes, UINT uTrans)
{
	if( uBytes > 8 ) {

		uBytes = 8;
		}
	
	m_Tx[uTrans].m_Ctrl = uBytes | 0x80;
	}

void CCANJ1939Handler::AddPGN(UINT uPGN, UINT uTrans)
{
	UINT uPos = 0;

	AddID(uPGN, uPos, uTrans);
	}

void CCANJ1939Handler::AddData(PDU * pPDU, UINT uTrans)
{
	memcpy(m_Tx[uTrans].m_bData, pPDU->m_pData, min(pPDU->m_uBytes, UINT(m_Tx[uTrans].m_Ctrl & 0x0F)));
	}

void CCANJ1939Handler::AddName(BOOL fSrc, UINT uTrans)
{
	if( m_Name[fSrc] ) {

		DWORD dwHi = m_Name[fSrc]->Hi;
		
		DWORD dwLo = m_Name[fSrc]->Lo;

		UINT uPos = 0;

		AddWord(LOWORD(dwLo), uPos, uTrans);

		AddWord(HIWORD(dwLo), uPos, uTrans);

		AddWord(LOWORD(dwHi), uPos, uTrans);

		AddWord(HIWORD(dwHi), uPos, uTrans);
		}
	}


void CCANJ1939Handler::GrowPDUs(PDU *pPDU)
{
	UINT uOld = m_uPDUs;

	UINT uNew = uOld + 1;

	PDU *pOld = m_pPDUs;
	
	PDU *pNew = new PDU [ uNew ];

	PDU *pAdd = pNew + uOld;

	memcpy(pNew, pOld, sizeof(PDU) * uOld);

	memcpy(pAdd, pPDU, sizeof(PDU));

	UINT uSize = pPDU->m_uBytes;

	pAdd->m_pData     = new BYTE [ uSize ];

	pAdd->m_pTransfer = NULL;

	pAdd->m_uTotal = uSize;

	memset(pAdd->m_pData, 0, uSize);

	if( uSize > 8 ) {

		pAdd->m_pTransfer = new BYTE [ uSize ];

		memset(pAdd->m_pTransfer, 0, uSize);
		
		for( UINT x = 0; x < TRANS_MODE; x++ ) {

			pAdd->m_bSequence[x] = 0;

			pAdd->m_bState   [x] = STATE_BEGIN;

			pAdd->m_uTime    [x] = 0;

			pAdd->m_bLimit   [x] = 0;

			pAdd->m_fCTS     [x] = FALSE;
			}
		}

	pAdd->m_uSet   = GetTickCount();

	pAdd->m_uReq   = 0;

	pAdd->m_uLive *= 3;
		       
	pAdd->m_fSend  = FALSE;

	m_Diag |= pPDU->m_bDiag;

	pAdd->m_pEnhanced = NULL;

	if( pPDU->m_bEnh ) {

		if( IsRapidFire(pPDU) ) {

			pAdd->m_pEnhanced = new BYTE [ uSize ];
			}
		}

	m_pPDUs = pNew;

	m_uPDUs = uNew;

	delete [] pOld;
	}

BOOL CCANJ1939Handler::PutFrame(UINT uTrans)
{	
	AfxRaiseIRQL(IRQL_HARDWARE + ExpGetIPL(0, 4));

	UINT uQueue = m_uQueue;
	
	Increment(m_uQueue);
	
	if( m_uQueue != m_uSend ) {
		
		m_nTxDisable++;

		AfxLowerIRQL();
		
		memcpy(&m_Send[uQueue], &m_Tx[uTrans], sizeof(FRAME_EXT));

		AfxRaiseIRQL(IRQL_HARDWARE + ExpGetIPL(0, 4));

		if( !--m_nTxDisable && !m_pTxData ) { 
			
			SendFrame();
			}
		
		AfxLowerIRQL();
		
		return TRUE;
		}
	
	m_uQueue = uQueue;

	AfxLowerIRQL();
	
	return FALSE;
	}

void CCANJ1939Handler::SendFrame(void)
{
	m_pTxData = PCBYTE(&m_Send[m_uSend]);

	m_uTx     = sizeof(FRAME_EXT) - 1;

	m_pPort->Send(*m_pTxData++);
	}

void CCANJ1939Handler::RecvFrame(void)
{
	if( DoListen() ) {

		HandleFrame();
		}
	
	DoDiagnostics(DIAG_IDS);

	DoDiagnostics(DIAG_PGNS);
	}

void CCANJ1939Handler::HandleFrame(void)
{
	memcpy(&m_RxData, &m_Rx, sizeof(FRAME_EXT));

	if( !HandleRequest() ) {

		if( m_fClaim[0] || m_fClaim[1] ) {

			if( !HandleDataTransfer() ) {

				if( !HandleTransport() ) {

					BOOL fClaim = HandleClaimAddress();

					UINT uNext  = 0;

					PDU * pPDU  = FindPDU(uNext);

					if( pPDU ) {

						if( !HandleRapidFire(pPDU) ) {

							if( !HandleSpecificRequest(pPDU, FALSE) ) {

								if( !HandleGlobalRequest(pPDU, FALSE) ) {

									if( !fClaim ) {

										DoNak();
										}
									}
								}

							uNext++;

							if( (pPDU = FindPDU(uNext)) ) {

								HandleGlobalRequest(pPDU, FALSE);
								}
							}
						}
					else {	
						if( !fClaim ) {
							
							DoNak();
							}
						}
					}
				}
			}
		}
	}

BOOL CCANJ1939Handler::DoListen(void)
{
	return IsJ1939() && IsThisNode();
	}

BOOL CCANJ1939Handler::SendAck(PDU * pPDU)
{
	Start();

	ID id;

	FindID(id, PGN_ACK, 6, pPDU->m_fSrc);

	AddID(id);

	AddCtrl(8);

	UINT uPos = 0;

	AddByte(0, uPos);

	AddByte(0xFF, uPos);  // Group Function Value (don't care ?)	

	AddWord(0xFFFF, uPos);

	AddByte(pPDU->m_ID.i.m_SA, uPos);

	AddID((pPDU->m_ID.m_Ref & GetMask()) >> 8, uPos);
	
	return PutFrame();
	}

BOOL CCANJ1939Handler::SendNak(ID id)
{
	Start();

	ID nak;

	FindID(nak, PGN_ACK, 6, GetSrc(&id));

	AddID(nak);

	AddCtrl(8);

	UINT uPos = 0;

	AddByte(1, uPos);

	AddByte(0xFF, uPos);  // Group Function Value (don't care?)	

	AddWord(0xFFFF, uPos);

	AddByte(id.i.m_SA, uPos);

	AddID((id.m_Ref & GetMask()) >> 8, uPos);

	return PutFrame();
	}


PDU* CCANJ1939Handler::FindPDU(UINT uNext)
{
	UINT uMask = GetMask();

	ID id;

	id.m_Ref = m_RxData.m_ID;

	if( IsBroadcast(id) ) {

		uMask = 0x3FF00FF;
		}
	
	for( UINT u = 0; u < m_uPDUs; u++ ) {

		if( ((m_RxData.m_ID & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask)) ) {

			if( uNext > 0 ) {

				uNext--;

				continue;
				}

			if( IsSetPDU(&m_pPDUs[u]) ) {

				return &m_pPDUs[u];
				}
			}
		}
      
	return NULL;
	}

BOOL CCANJ1939Handler::HandleRequest(void)
{
	if( IsRequestPGN() ) {

		ID id;

		id.m_Ref = 0;

		id.m_Ref |= m_RxData.m_bData[0] << 8;

		id.m_Ref |= m_RxData.m_bData[1] << 16;

		id.m_Ref |= m_RxData.m_bData[2] << 24;

		id.m_Ref |= m_RxData.m_ID & 0xFF;

		PDU * pPDU = FindPDU(&id, dirTx);

		if( pPDU ) {

			if( IsCommandOnly(pPDU) ) {

				UINT uTo = (m_RxData.m_ID & 0xFF00) >> 8;

				BOOL fForce = uTo < 0xF0; 

				HandleNak(id, fForce);

				return TRUE;
				}  

			if( HandleSpecificRequest(pPDU, TRUE) ) {

				return TRUE;
				}

			if( HandleGlobalRequest(pPDU, TRUE) ) {
				
				return TRUE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HandleSpecificRequest(PDU * pPDU, BOOL fRTR)
{
	if( IsDestinationSpecific(pPDU->m_ID) ) {

		if( fRTR && HandleRequest(pPDU) ) {

			return TRUE;
			}

		if( HandleClaimAddressReq(pPDU) ) {

			return TRUE;
			}

		return SetPDU(pPDU);
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HandleGlobalRequest(PDU * pPDU, BOOL fRTR)
{
	if( !IsDestinationSpecific(pPDU->m_ID) ) {

		if( fRTR ) {

			return SendPDU(pPDU);
			}

		if( HandleClaimAddressReq(pPDU) ) {

			return TRUE;
			}

		return SetPDU(pPDU);
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HandleRequest(PDU * pPDU)
{
	return SendPDU(pPDU);
	}

BOOL CCANJ1939Handler::HandleClaimAddressReq(PDU * pPDU)
{
	if( IsAddrClaimedReq(pPDU) ) {
	
		SourceAddrClaim(pPDU->m_fSrc);
	       
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleClaimAddress(void)
{
	ID rx;

	rx.m_Ref = m_RxData.m_ID;

	if( IsAddrClaimed(rx) ) {

		for( UINT s = 0; s < elements(m_fClaim); s++ ) {

			if( !s || m_fSecondary ) {

				if( rx.i.m_SA == m_bSrc[s] ) {

					PBYTE Name = PBYTE(m_Name[s]);

					for( UINT u = 7; u > 0; u-- ) {

						if( Name[u] < m_RxData.m_bData[u] ) {

							SourceAddrClaim(s);

							return TRUE;
							}

						if( Name[u] > m_RxData.m_bData[u] ) {


							if( m_fClaim[s] || m_fClaimSent[s] ) {

								CannotClaimAddr(s);
								}

							return TRUE;
							}
						}

					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HandleTransport(void)
{
	if( IsTransportPGN() ) {

		ID rx;

		rx.m_Ref   = m_RxData.m_ID;

		ID id;

		id.m_Ref   = (m_RxData.m_bData[5] << 8) | (m_RxData.m_bData[6] << 16);

		if( IsDestinationSpecific(id) ) {

			id.i.m_PS  = rx.i.m_PS;
			}

		id.i.m_SA  = rx.i.m_SA;

		if( rx.i.m_PS < 0xF0 ) {

			if( IsRTS() ) {

				return HandleRTS(id);
				}

			if( IsCTS() ) {

				return HandleCTS(id);
				}

			if( IsEndOfMsg() ) {

				return HandleEOM(id);
				}

			if( IsAbort() ) {

				return HandleAbort(id);
				} 

			return TRUE;
				}

		if( IsBAM() ) {

			return HandleBAM(id);
			} 

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleRTS(ID id)
{
	PDU * pPDU = FindPDU(&id, dirRx);

	if( pPDU ) {

		ClearToSend(pPDU);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleCTS(ID id)
{
	PDU * pPDU = FindPDU(&id, dirTx);

	if( pPDU ) {

		if( m_RxData.m_bData[1] == 0 ) {

			SetState(pPDU, TRANS_SEND, STATE_HOLD);

			return TRUE;
			}
		
		SetState(pPDU, TRANS_SEND, STATE_CTS);

		TransferData(pPDU, m_RxData.m_bData[1], m_RxData.m_bData[2]);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HandleEOM(ID id)
{
	PDU * pPDU = FindPDU(&id, dirRx);

	if( pPDU ) {

		SendAck(pPDU);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleBAM(ID id)
{
	PDU * pPDU = FindPDU(&id, dirRx);

	if( pPDU ) {

		BYTE bSrc = pPDU->m_ID.m_Ref & 0xFF;

		if( m_pBroadcast[bSrc] ) {

			if( m_pBroadcast[bSrc] == pPDU ) {

				if( SetState(pPDU, TRANS_RECV, STATE_RESET) ) {

					m_pBroadcast[bSrc] = NULL;
					}
				}
			}

		if( !m_pBroadcast[bSrc] ) {

			m_pBroadcast[bSrc] = pPDU;

			m_pBroadcast[bSrc]->m_uBytes = MAKEWORD(m_RxData.m_bData[1], m_RxData.m_bData[2]);

			SetState(pPDU, TRANS_RECV, STATE_CTS);
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleDataTransfer(void)
{
	BYTE bSrc = m_RxData.m_ID & 0xFF;

	ID id;

	id.m_Ref = m_RxData.m_ID;

	BOOL fBroadcast = IsBroadcast(id);
	
	PDU * pPDU = fBroadcast ? m_pBroadcast[bSrc] : m_pTransfer[bSrc];

	if( pPDU && IsTransferPGN() ) {

		if( pPDU->m_fCTS[TRANS_RECV] ) {

			UINT uSeq  = m_RxData.m_bData[0];

			UINT uSize = 7;

			if( m_fDebug && m_uTO ) {

				AfxTrace("-&%u,%2.2x", uSeq, pPDU->m_bState[TRANS_RECV]);
				}

			if( uSeq == pPDU->m_bSequence[TRANS_RECV] ) {

				UINT uPackets = FindTotalPackets(pPDU);

				if( uPackets == uSeq && (pPDU->m_uBytes % 7) ) {

					uSize = pPDU->m_uBytes % 7;
					}

				UINT uOffset = (uSeq - 1) * 7;

				memcpy(pPDU->m_pTransfer + uOffset, m_RxData.m_bData + 1, uSize);

				if( uPackets == uSeq ) {

					memcpy(pPDU->m_pData, pPDU->m_pTransfer, pPDU->m_uBytes);

					if( !fBroadcast ) {

						EndOfMsgACK(pPDU);
						}

					FlopBytes(pPDU, pPDU->m_uBytes);

					UINT uTick = GetTickCount();

					pPDU->m_uSet = uTick ? uTick : 1;

					SetState(pPDU, TRANS_RECV, STATE_RESET);

					if( fBroadcast ) {

						PDU * pSEC = FindPDU(&pPDU->m_ID, dirRx, !pPDU->m_fSrc);

						if( pSEC ) {

							UINT uBytes = min(pSEC->m_uBytes, pPDU->m_uBytes);

							memcpy(pSEC->m_pData, pPDU->m_pTransfer, uBytes);

							FlopBytes(pSEC, uBytes);

							uTick = GetTickCount();

							pSEC->m_uSet = uTick ? uTick : 1;
							}

						m_pBroadcast[bSrc] = NULL;
						}
					else 
						m_pTransfer[bSrc]  = NULL;

					return TRUE;
					}

				SetState(pPDU, TRANS_RECV, STATE_DATA);
				}

			if( m_fDebug && m_uTO ) {

				AfxTrace("-#%u,%2.2x", uSeq, pPDU->m_bState[TRANS_RECV]);
				}
			}

		return TRUE;
		}

	if( m_fDebug && m_uTO ) {

		AfxTrace("-_%2.2x", pPDU->m_bState[TRANS_RECV]);
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::HandleAbort(ID id)
{
	PDU * pPDU = FindPDU(&id, dirTx);

	if( pPDU ) {

		SendAck(pPDU);

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleNak(ID id, BOOL fForce)
{
	if( IsDestinationSpecific(id) || fForce ) {

		return SendNak(id);  
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::HandleRapidFire(PDU * pPDU)
{
	if( IsRapidFire(pPDU) ) {

		BYTE bSeq = m_RxData.m_bData[0];

		UINT uCount = min(7, m_RxData.m_Ctrl);

		memcpy(PBYTE(pPDU->m_pEnhanced + bSeq * 7), PBYTE(m_RxData.m_bData + 1), uCount);
		
		UINT uTick = GetTickCount();

		pPDU->m_uSet = uTick ? uTick : 1;

		return TRUE;
		}

	return FALSE;
	}


BOOL CCANJ1939Handler::SetPDU(PDU * pPDU)
{
	if( m_fClaim[pPDU->m_fSrc] && IsSetPDU(pPDU) ) {

		UINT uCount = min(pPDU->m_uBytes, m_RxData.m_Ctrl);

		if( IsDM(pPDU) ) {

			while( m_RxData.m_bData[uCount - 1] == 0xFF ) {

				uCount--;
				}
			}

		/* Should we not report unavailable data ???? Are there any cases where FF is or part of valid data?

		for( UINT u = 0; u < uCount; u++ ) {

			if( m_RxData.m_bData[u] != 0xFF ) {

				pPDU->m_pData[u] = m_RxData.m_bData[u];
				}
			}*/

		memcpy(pPDU->m_pData, m_RxData.m_bData, uCount);  

		FlopBytes(pPDU,  uCount);

		UINT uTick = GetTickCount();

		pPDU->m_uSet = uTick ? uTick : 1;

		m_fUpdate = TRUE;
		}

	return m_fClaim[pPDU->m_fSrc];
	}

BOOL CCANJ1939Handler::TransferData(PDU * pPDU, UINT uLimit, UINT uSequence, UINT uTrans)
{
	SetState(pPDU, TRANS_SEND, STATE_DATA, uTrans);

	if( uSequence ) {

		pPDU->m_bSequence[TRANS_SEND] = uSequence;
		}
	
	BOOL fLimit = ((uLimit > 0) && (uLimit < 0xFF)) || pPDU->m_bLimit[TRANS_SEND];

	if( fLimit && uLimit == 0 ) {

		uLimit = pPDU->m_bLimit[TRANS_SEND];
		}

	UINT uPackets = fLimit ? uLimit : FindTotalPackets(pPDU);

	UINT uData = pPDU->m_bSequence[TRANS_SEND] - 1;

	if( uData < uPackets ) {

		Start(uTrans);

		ID id;

		FindID(id, PGN_TRANSFER, 7, pPDU->m_fSrc);

		if( IsDestinationSpecific(pPDU->m_ID) ) {

			id.i.m_PS = pPDU->m_ID.i.m_SA;  
			}
		
		AddID(id, uTrans);

		AddCtrl(pPDU, uTrans);

		UINT uPos = 0;

		AddByte(pPDU->m_bSequence[TRANS_SEND], uPos, uTrans);

		memcpy(m_Tx[uTrans].m_bData + 1, pPDU->m_pData + uData * 7, 7);

		if( uData + 1 == FindTotalPackets(pPDU) && pPDU->m_uBytes % 7 ) {

			UINT uMod = pPDU->m_uBytes % 7;

			memset(m_Tx[uTrans].m_bData + 1 + uMod, 0xFF, 7 - uMod);
			}

		if( !PutFrame(uTrans) ) {

			SetState(pPDU, TRANS_SEND, STATE_RETRY, uTrans);

			return FALSE;
			}

		if( uData + 1 == uPackets ) {

			if( fLimit ) {

				if( uLimit < uPackets ) {

					SetState(pPDU, TRANS_SEND, STATE_WAIT, uTrans);

					return TRUE;
					}
				}

			SetState(pPDU, TRANS_SEND, STATE_RESET, uTrans);

			return TRUE;
			}

		SetState(pPDU, TRANS_SEND, STATE_DATA, uTrans);

		return TRUE;
		}

	SetState(pPDU, TRANS_SEND, STATE_RESET, uTrans);

	return TRUE;
	}

void CCANJ1939Handler::DoNak(void)
{
	ID id;

	id.m_Ref = m_RxData.m_ID;
		
	if( id.i.m_PS == m_bSrc[0] || (m_fSecondary && id.i.m_PS == m_bSrc[1]) ) {

		HandleNak(id, FALSE);
		}
	}


// Network Management

BOOL CCANJ1939Handler::SourceAddrClaim(BOOL fSrc, UINT uTrans)
{
	Start(uTrans);

	ID id;

	FindID(id, CLAIM_RESP, 6, fSrc);

	AddID(id, uTrans);

	AddCtrl(8, uTrans);

	AddName(fSrc, uTrans);

	m_fClaimSent[fSrc] = PutFrame(uTrans);

	if( m_fClaimSent[fSrc] ) {

		m_uClaim[fSrc] = GetTickCount();
		}

	return m_fClaimSent[fSrc];
	}

BOOL CCANJ1939Handler::CannotClaimAddr(BOOL fSrc)
{
	ID id;

	FindID(id, CLAIM_RESP, 6, fSrc);

	id.i.m_SA = 254;

	// Random delay

	int Random = rand() % 153;

	if( CanTaskWait() ) {

		Sleep(Random);
		}

	Start();

	AddID(id);

	AddCtrl(8);

	AddName(fSrc);

	m_fClaim[fSrc]		= FALSE;

	m_fClaimSent[fSrc]	= FALSE;

	// Adjust Source Address ?

	return PutFrame();
	}

// Transport Protocol - Flow Control for multi-packeted messages

BOOL CCANJ1939Handler::SendMultipacketPDU(PDU * pPDU, BOOL fRetry, UINT uTrans)
{
	if( !IsDestinationSpecific(pPDU->m_ID) ) {

		if( pPDU->m_bSequence[TRANS_SEND] ) {

			SetState(pPDU, TRANS_SEND, STATE_RESET, uTrans);
			}

		if( BroadcastAnnounceMsg(pPDU, uTrans) ) {

			if( CanTaskWait() ) {

				Sleep(50);
				}

			return TransferData(pPDU, 0, pPDU->m_bSequence[TRANS_SEND], uTrans);
			}

		return FALSE;
		}

	return RequestToSend(pPDU, fRetry, uTrans);
	}

BOOL CCANJ1939Handler::RequestToSend(PDU * pPDU, BOOL fRetry, UINT uTrans)
{
	if( !fRetry && pPDU->m_bState[TRANS_SEND] != 0xFF ) {

		return FALSE;
		}

	Start(uTrans);

	ID id;

	FindID(id, PGN_TRANSPORT, 7, pPDU->m_fSrc);

	id.i.m_PS = pPDU->m_ID.i.m_SA;

	AddID(id, uTrans);

	AddCtrl(8, uTrans);

	UINT uPos = 0;

	AddByte(16, uPos, uTrans);

	AddWord(pPDU->m_uBytes, uPos, uTrans);

	AddByte(FindTotalPackets(pPDU), uPos, uTrans);

	AddByte(0xFF, uPos, uTrans);

	id.i.m_PF = pPDU->m_ID.i.m_PF;

	AddID((id.m_Ref & GetMask()) >> 8, uPos, uTrans);

	if( PutFrame(uTrans) ) {

		SetState(pPDU, TRANS_SEND, STATE_RTS, uTrans);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::ClearToSend(PDU * pPDU)
{
	if( pPDU->m_bState[TRANS_RECV] != STATE_BEGIN ) {

		ConnAbort(pPDU, ABORT_SESSION);

		return TRUE;
		}

	BYTE bSrc = pPDU->m_ID.i.m_SA;

	if( !m_pTransfer[bSrc] ) {

		m_pTransfer[bSrc] = pPDU;

		Start();
		
		ID id;

		FindID(id, PGN_TRANSPORT, 7, pPDU->m_fSrc);
			      
		id.i.m_PS = pPDU->m_ID.i.m_SA;

		AddID(id);
				
		AddCtrl(8);

		UINT uPos = 0;

		AddByte(17, uPos);

		AddByte(min(m_RxData.m_bData[3], FindTotalPackets(pPDU)), uPos);	

		AddByte(1, uPos);  

		AddWord(0xFFFF, uPos);

		AddID((pPDU->m_ID.m_Ref & GetMask()) >> 8, uPos);

		UINT uTotal = MAKEWORD(m_RxData.m_bData[1], m_RxData.m_bData[2]);

		MakeMin(uTotal, pPDU->m_uTotal);

		m_pTransfer[bSrc]->m_uBytes = uTotal;

		if( PutFrame() ) {
   	
			SetState(pPDU, TRANS_RECV, STATE_CTS);

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	} 

BOOL CCANJ1939Handler::EndOfMsgACK(PDU * pPDU)
{
	Start();

	ID id;

	FindID(id, PGN_TRANSPORT, 7, pPDU->m_fSrc);

	id.i.m_PS = pPDU->m_ID.i.m_SA;

	AddID(id);

	AddCtrl(8);

	UINT uPos = 0;

	AddByte(19, uPos);

	AddWord(pPDU->m_uBytes, uPos);

	AddByte(FindTotalPackets(pPDU), uPos);

	AddByte(0xFF, uPos);

	AddID((pPDU->m_ID.m_Ref & GetMask()) >> 8, uPos);

	return PutFrame();
	}

BOOL CCANJ1939Handler::ConnAbort(PDU * pPDU, UINT uReason, UINT uTrans)
{
	Start(uTrans);

	ID id;

	FindID(id, PGN_TRANSPORT, 7, pPDU->m_fSrc);

	id.i.m_PS = pPDU->m_ID.i.m_SA;

	AddID(id, uTrans);

	AddCtrl(8, uTrans);

	UINT uPos = 0;

	AddByte(255, uPos, uTrans);

	AddByte(uReason, uPos, uTrans); 

	AddWord(0xFFFF, uPos, uTrans);

	AddByte(0xFF, uPos, uTrans);

	AddID((pPDU->m_ID.m_Ref & GetMask()) >> 8, uPos, uTrans);

	return PutFrame(uTrans);
	}

BOOL CCANJ1939Handler::BroadcastAnnounceMsg(PDU * pPDU, UINT uTrans)
{
	Start(uTrans);

	ID id;

	FindID(id, PGN_TRANSPORT, 7, pPDU->m_fSrc);

	AddID(id, uTrans);

	AddCtrl(8, uTrans);

	UINT uPos = 0;

	AddByte(32, uPos, uTrans);

	AddWord(pPDU->m_uBytes, uPos, uTrans);

	AddByte(FindTotalPackets(pPDU), uPos, uTrans);

	AddByte(0xFF, uPos, uTrans);

	AddID((pPDU->m_ID.m_Ref & GetMask()) >> 8, uPos, uTrans);

	return PutFrame(uTrans);
	}

// Helpers

void CCANJ1939Handler::Increment(UINT volatile &uIndex)
{
	uIndex = (uIndex + 1) % elements(m_Send);
	}

BOOL CCANJ1939Handler::IsISO15765(void)
{
	return (m_Rx.m_ID >> 24) & 0x3;
	}

BOOL CCANJ1939Handler::IsJ1939(void)
{	
	return !((m_Rx.m_ID >> 25) & 0x1);
	}

BOOL CCANJ1939Handler::IsThisNode(void)
{
	ID id;
	
	id.m_Ref = m_Rx.m_ID;

	BOOL fSrc = GetSrc(&id);

	return (m_fClaim[fSrc] || m_fClaimSent[fSrc]) && ((id.i.m_PS == m_bSrc[fSrc]) || (id.i.m_PF >=	0xF0) || (id.i.m_PS == 0xFF));
	}

BOOL CCANJ1939Handler::IsMultipacket(PDU * pPDU)
{
	return ((pPDU->m_uBytes > 8) && !IsRapidFire(pPDU));
	}

BOOL CCANJ1939Handler::IsAddrClaimedReq(PDU * pPDU)
{
	UINT uID = pPDU->m_ID.i.m_DP << 16;

	uID |= pPDU->m_ID.i.m_PF << 8;

	if( uID == CLAIM_REQ ) {

		if( m_RxData.m_Ctrl == 3 ) {

			UINT uPGN = m_RxData.m_bData[0];

			uPGN     |= m_RxData.m_bData[1] << 8;

			uPGN	 |= m_RxData.m_bData[2] << 16;

			if( uPGN == CLAIM_RESP ) {

				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

BOOL CCANJ1939Handler::IsAddrClaimed(ID id)
{
	UINT uID = id.i.m_DP << 16;

	uID |= id.i.m_PF << 8;

	if( uID == CLAIM_RESP ) {

		return TRUE;
		}
	
	return FALSE;
	}


BOOL CCANJ1939Handler::IsDestinationSpecific(ID id)
{
	return id.i.m_PF < 0xF0;
       	}

BOOL CCANJ1939Handler::IsDestinationGlobal(ID id)
{
	return id.i.m_PF == 0xFF;
	}

BOOL CCANJ1939Handler::IsGroupExtension(ID id)
{
	return id.i.m_PF >> 4 == 0xF; 
	}

BOOL CCANJ1939Handler::IsRequestPGN(void)
{
	UINT uID = ((m_RxData.m_ID & 0x1FFFF00) >> 8);

	return (uID == UINT(PGN_REQ | m_bSrc[0])) || (m_fSecondary && (uID == UINT(PGN_REQ | m_bSrc[1])));
	}


BOOL CCANJ1939Handler::IsTransferPGN(void)
{	
	return (((m_RxData.m_ID & 0x1FF0000) >> 8) == PGN_TRANSFER);
	}

BOOL CCANJ1939Handler::IsTransportPGN(void)
{	
	return (((m_RxData.m_ID & 0x1FF0000) >> 8) == PGN_TRANSPORT);
	}

BOOL CCANJ1939Handler::IsRTS(void)
{
	return m_RxData.m_bData[0] == 16;
	}

BOOL CCANJ1939Handler::IsCTS(void)
{
	return m_RxData.m_bData[0] == 17;
	}

BOOL CCANJ1939Handler::IsBAM(void)
{
	return m_RxData.m_bData[0] == 32;
	}

BOOL CCANJ1939Handler::IsEndOfMsg(void)
{
	return m_RxData.m_bData[0] == 19;
	}

BOOL CCANJ1939Handler::IsAbort(void)
{
	return m_RxData.m_bData[0] == 255;
   	}

BOOL CCANJ1939Handler::IsDataTransfer(void)
{
	ID id;
	
	id.m_Ref = m_RxData.m_ID;

	return id.i.m_PF == HIBYTE(PGN_TRANSFER);
	}

BOOL CCANJ1939Handler::IsSendPDU(PDU * pPDU)
{
	return pPDU->m_bDir != dirRx;
	}

BOOL CCANJ1939Handler::IsSetPDU(PDU * pPDU)
{
	return pPDU->m_bDir != dirTx;
	}

BOOL CCANJ1939Handler::IsTimedOut(UINT uTime, UINT uSpan)
{
	return int(GetTickCount() - uTime - ToTicks(uSpan)) >= 0;
	}

BOOL CCANJ1939Handler::IsBroadcast(ID id)
{	
	return id.i.m_PF < 0xF0 && id.i.m_PS == 0xFF;
	}

void CCANJ1939Handler::FindID(ID &id, UINT uID, UINT uPriority, BOOL fSrc)
{
	id.i.m_X   = 0;
	
	id.i.m_P   = uPriority;

	id.i.m_EDP = 0;

	id.i.m_DP  = (uID & 0x10000) >> 16;

	id.i.m_PF  = (uID & 0x0FF00) >> 8;

	id.i.m_PS  = 0xFF;

	id.i.m_SA  = m_bSrc[fSrc];
	}

UINT CCANJ1939Handler::FindTotalPackets(PDU * pPDU)
{
	UINT uPackets = pPDU->m_uBytes / 7;

	if( pPDU->m_uBytes % 7 ) {

		uPackets++;
		}

	return uPackets;
	}

BOOL CCANJ1939Handler::SetState(PDU * pPDU, UINT uMode, UINT uState, UINT uTrans)
{
	if( uTrans == transTimer ) {
		
		if( !m_fTimer ) {

			return FALSE;
			}
		}

	m_fTimer = FALSE;

	if( uState == STATE_RESET ) {

		pPDU->m_bState[uMode] = STATE_BEGIN;

		pPDU->m_bSequence[uMode] = 0;

		pPDU->m_bLimit[uMode] = 0;

		pPDU->m_fCTS[uMode] = FALSE;
		}

	else if( uState == STATE_RETRY ) {
		
		pPDU->m_uTime[uMode] = GetTickCount();
		}
	
	else {
		pPDU->m_bSequence[uMode]++;

		pPDU->m_bState[uMode] = uState;

		pPDU->m_uTime[uMode]  = GetTickCount();

		if( uState == STATE_CTS ) {

			pPDU->m_fCTS[uMode] = TRUE;
			}
		}

	m_fTimer = TRUE;

	return TRUE;
	}

void CCANJ1939Handler::ClearData(PDU * pPDU)
{
	memset(pPDU->m_pData, 0, pPDU->m_uTotal);
	}

void CCANJ1939Handler::SetLED(UINT uLed, BOOL fState)
{
/*	if( GetTickCount() - ToTicks(200) > m_uTimer ) {	
	
		m_fSet = fState ? !m_fSet : !fState;

		m_pPort->SetOutput(uLed, m_fSet);

		m_uTimer = GetTickCount();
		}
*/	}

BOOL CCANJ1939Handler::IsDM(PDU * pPDU)
{
	UINT uPGN = (pPDU->m_ID.m_Ref & 0x00FFFF00) >> 8;
	
	switch( uPGN ) {

		case 65226:
		case 65227:
		case 65229:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::IsDM1(PDU * pPDU)
{
	UINT uPGN = (pPDU->m_ID.m_Ref & 0x00FFFF00) >> 8;
	
	switch( uPGN ) {

		case 65226:
		
			return TRUE;
		}

	return FALSE;
	}

void CCANJ1939Handler::FlopBytes(PDU * pPDU, UINT uCount)
{
	UINT uPGN = (pPDU->m_ID.m_Ref & 0x00FFFF00) >> 8;

	switch( uPGN ) {

		case 65226:
		case 65227:
			
			if( m_bClrDTC[pPDU->m_ID.i.m_SA] ) {

				memset(pPDU->m_pData + uCount, 0, pPDU->m_uTotal - uCount);
				}

			break;

		case 65229:

			break;

		default:
			return;
			}

	UINT uBegin = GetFlopStart(pPDU);

	UINT uEnd   = GetFlopEnd(pPDU);

	for( UINT u = uBegin; u < uCount && u < uEnd; u += 3 ) {

		BYTE bLo = (pPDU->m_pData[u] & 0xE0) >> 5;

		BYTE bHi = (pPDU->m_pData[u] & 0x1F);

		pPDU->m_pData[u] = (bHi << 3) | bLo;

		u++;

		bLo = (pPDU->m_pData[u] & 0x80) >> 7;
		
		bHi = pPDU->m_pData[u] & 0x7F;

		pPDU->m_pData[u] = (bHi << 1) | bLo;
		}

	if( m_fDebug && IsDM1(pPDU) ) {

		m_fPrint = 1;

		m_uTO    = 0;
		}
	}

BOOL CCANJ1939Handler::IsCommandOnly(PDU * pPDU)
{
	UINT uPGN = (pPDU->m_ID.m_Ref & 0x00FFFF00) >> 8;
	
	switch( uPGN ) {

		case 65228:
		case 65235:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCANJ1939Handler::IsRapidFire(PDU * pPDU)
{
	return (pPDU->m_bEnh == ENH_RAPIDFIRE);
	}

UINT CCANJ1939Handler::FindTimeout(BYTE bState)
{
	switch( bState ) {

		case STATE_HOLD:	return TIME_T4;
		case STATE_RTS:		return TIME_T3;		
		case STATE_CTS:		return TIME_T2;
		case STATE_DATA:	return TIME_T1;
		case STATE_WAIT:	return TIME_TR;
		
		}

	return 0;
	}
		
BOOL CCANJ1939Handler::Retry(BYTE bState)
{
	switch( bState ) {

		case STATE_RTS:	
		case STATE_WAIT:

			return TRUE;
		}

	return FALSE;
	}


// Diagnostics

void CCANJ1939Handler::DoDiagnostics(BYTE bDiag)
{
	if( bDiag == DIAG_IDS ) {

		if( m_Diag & DIAG_IDS ) {

			BYTE bID = m_Rx.m_ID & 0xFF;

			if( !memchr(m_Diag1, bID, DEVICES) ) {

				PBYTE pLoc = PBYTE(memchr(m_Diag1, 0xFF, DEVICES));

				if( pLoc ) {

					memset(pLoc, bID, 1);

					for( UINT u = 0; u < m_uPDUs; u++ ) {

						if( m_pPDUs[u].m_bDiag & DIAG_IDS ) {

							memcpy(m_pPDUs[u].m_pData, m_Diag1, m_pPDUs[u].m_uBytes);
							}
						}
					}
				}
			}
		}

	if( bDiag == DIAG_PGNS ) {

		if( m_Diag & DIAG_PGNS ) {

			DWORD dwPGN = IntelToHost(m_Rx.m_ID  & GetMask());

			for( UINT x = 0; x < DEVICES; x++ ) {

				if( m_Diag2[x] == dwPGN ) {

					return;
					}

				if( m_Diag2[x] == 0xFFFFFFFF ) {

					m_Diag2[x] = dwPGN;

					break;
					}
				}

			for( UINT y = 0; y < m_uPDUs; y++ ) {

				if( m_pPDUs[y].m_bDiag & DIAG_PGNS ) {

					memcpy(m_pPDUs[y].m_pData, PBYTE(m_Diag2), m_pPDUs[y].m_uBytes);
					}
				}
			}
		}
	}

// End of File

