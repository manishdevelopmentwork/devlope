
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Off-line Simulator
//

// Name Fettling

extern "C" BOOL K5NETDRV_SetName(PCTXT pName);

// Dynamic Class

AfxImplementDynamicClass(COfflineSimulator, CObject);

// Constructor

COfflineSimulator::COfflineSimulator(void)
{
	m_dwProject = 0;

	m_fRunning  = FALSE;
	}

// Attributes

BOOL COfflineSimulator::IsRunning(void)
{
	return m_fRunning;
	}

BOOL COfflineSimulator::CanPauseResume(void)
{
	DWORD dwCon, dwApp;	

	if( afxMiddleware->GetStatus(m_dwProject, dwCon, dwApp) ) {

		if( dwCon == K5MWSTAT_CNXOK ) {

			switch( dwApp ) {

				case K5MWSTAT_APPRUN:
				case K5MWSTAT_APPSTOP:
					break;

				default:
					return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL COfflineSimulator::CanSingleStep(void)
{
	DWORD dwCon, dwApp;	

	if( afxMiddleware->GetStatus(m_dwProject, dwCon, dwApp) ) {

		if( dwCon == K5MWSTAT_CNXOK ) {

			switch( dwApp ) {

				case K5MWSTAT_APPSTOP:
					break;

				default:
					return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL COfflineSimulator::FindProject(PCTXT pPath)
{
	DWORD dwProject;

	if( (dwProject = afxMiddleware->FindProject(pPath)) ) {

		m_dwProject = dwProject;
		
		return TRUE;
		}

	return FALSE;
	}

void COfflineSimulator::SetHexMode(BOOL fEnable)
{
	afxMiddleware->SetHexMode(m_dwProject, fEnable);
	}

BOOL COfflineSimulator::GetHexMode(void)
{
	return afxMiddleware->GetHexMode(m_dwProject);
	}

// Operations
	
BOOL COfflineSimulator::Open(CWnd &Wnd, CDatabase *pDbase, PCTXT pPath, BOOL fOnline)
{
	DWORD dwFlags = 0;

	dwFlags |= K5MW_POLLEVENTS;
	dwFlags |= K5MW_QUIETMODE;

	if( fOnline ) {

		// Let the driver know the real project name
		// so it can replace the dummy one with whatever
		// the middleware is expecting to see...

		CString Name = pPath;

		Name = Name.Mid(Name.GetLength() - 8, 7);

		K5NETDRV_SetName(Name);
		}

	if( afxMiddleware->Connect(Wnd.GetHandle(), dwFlags) ) {

		extern CDatabase * g_pDbase;

		g_pDbase  = pDbase;

		m_fOnline = fOnline;

		if( (m_dwProject = afxMiddleware->OpenProject( pPath,
							       fOnline ? L"G3STRATON.DLL" : L"K5NETSIM.DLL",
							       fOnline ? L"localhost"     : pPath,
							       1,
							       0)) ) {

			m_fRunning = TRUE;

			return TRUE;
			}
		}

	return FALSE;
	}

void COfflineSimulator::Close(void)
{
	if( m_fRunning ) {

		afxMiddleware->Disconnect();

		afxMiddleware->CloseProject(m_dwProject);

		m_fRunning = FALSE;
		}
	}

void COfflineSimulator::SetCycleTime(UINT uScan)
{
	afxMiddleware->Execute(m_dwProject, CPrintf(L"SetCycleTime:%d", uScan));
	}

void COfflineSimulator::PauseResume(void)
{
	DWORD dwCon, dwApp;	

	if( afxMiddleware->GetStatus(m_dwProject, dwCon, dwApp) ) {

		if( dwCon == K5MWSTAT_CNXOK ) {

			switch( dwApp ) {

				case K5MWSTAT_APPRUN:

					afxMiddleware->Execute(m_dwProject, L"Pause");

					break;

				case K5MWSTAT_APPSTOP:
					
					afxMiddleware->Execute(m_dwProject, L"Resume");
					
					break;
				}
			}
		}
	}

void COfflineSimulator::SingleStep(void)
{
	DWORD dwCon, dwApp;	

	if( afxMiddleware->GetStatus(m_dwProject, dwCon, dwApp) ) {

		if( dwCon == K5MWSTAT_CNXOK ) {

			switch( dwApp ) {

				case K5MWSTAT_APPSTOP:
					
					afxMiddleware->Execute(m_dwProject, L"CycleStep");
					
					break;
				}
			}
		}
	}

void COfflineSimulator::OnEvent(UINT uCode, DWORD dwIdent)
{
//	AfxTrace(L"mw: %d\t%d\n", uCode, dwIdent);

	switch( uCode ) {

		case 1:
			OnChange(dwIdent);
			break;

		case 2:
			OnStatus(dwIdent);
			break;

		case 3:
			OnTiming(dwIdent);
			break;

		case 4:
			OnProgress(dwIdent);
			break;

		case 6:
			OnPoll(dwIdent);
			break;
		
		default:
/*			AfxTrace(L"mw - Other, code %d\n", uCode);
*/			break;
		}
	}

// Event Handlers

void COfflineSimulator::OnChange(DWORD dwIdent)
{
	AfxTrace(L"mw change, ident %d\n", dwIdent);
	}

void COfflineSimulator::OnStatus(DWORD dwIdent)
{
/*	AfxTrace(L"mw status, ident %d\n", dwIdent);

	DWORD dwCon, dwApp;	

	if( afxMiddleware->GetStatus(m_dwProject, dwCon, dwApp) ) {
		
		AfxTrace(L" con %d, app %d\n", dwCon, dwApp);
		}
*/	}

void COfflineSimulator::OnTiming(DWORD dwIdent)
{
	AfxTrace(L"mw timing, ident %d\n", dwIdent);
	}

void COfflineSimulator::OnProgress(DWORD dwIdent)
{
	AfxTrace(L"mw progress, ident %d\n", dwIdent);
	}

void COfflineSimulator::OnPoll(DWORD dwIdent)
{
	CString Text;

	DWORD dwCon, dwApp;	

	if( afxMiddleware->GetStatus(m_dwProject, dwCon, dwApp) ) {

		if( dwCon == K5MWSTAT_CNXOK ) {

			Text += m_fOnline ? L"Online Debug" : L"Simulation";

			Text += L" - ";

			switch( dwApp ) {

				case K5MWSTAT_APPRUN:

					Text += L"Running";

					break;

				case K5MWSTAT_APPSTOP:

					Text += L"Paused";

					break;
				}
			}
		}

	afxThread->SetStatusText(Text);
	}

// End of File
