
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Canyon CA04 Model Data
//

static BYTE imageCA04[] = {

	#include "ca04-x1.png.dat"
	0
	};

global CHostModel modelCA04 = {

	"",
	"CA04",
	"CR3000-04",
	"CR3000-04000",
	rfCanyon,
	1,
	584,
	424,
	52,
	68,
	480,
	272,
	0,
	NULL,
	sizeof(imageCA04)-1,
	imageCA04
	};

// End of File
