
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbNetworkExpansion_HPP

#define	INCLUDE_UsbNetworkExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Expansion Object
//

class CUsbNicExpansion : public CUsbExpansion
{
	public:
		// Constructor
		CUsbNicExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT METHOD GetName(void);
		UINT  METHOD GetClass(void);
	};

// End of File

#endif


