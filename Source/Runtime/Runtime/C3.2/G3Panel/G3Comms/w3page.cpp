
#include "intern.hpp"

#include "web.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Web Page List
//

// Constructor

CWebPageList::CWebPageList(void)
{
	m_uCount = 0;

	m_ppPage = NULL;
	}

// Destructor

CWebPageList::~CWebPageList(void)
{
	while( m_uCount-- ) {

		delete m_ppPage[m_uCount];
		}

	delete [] m_ppPage;
	}

// Initialization

void CWebPageList::Load(PCBYTE &pData)
{
	ValidateLoad("CWebPageList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		m_ppPage = New CWebPage * [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CWebPage *pPage = NULL;

			if( GetByte(pData) ) {

				pPage = New CWebPage;

				pPage->Load(pData);
				}

			m_ppPage[n] = pPage;
			}
		}
	}

// Operations

void CWebPageList::PreRegister(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( m_ppPage[n] ) {

			m_ppPage[n]->PreRegister();
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Web Page Item
//

// Constructor

CWebPage::CWebPage(void)
{
	m_pSrc    = CCommsSystem::m_pThis->m_pTags->m_pTags;
	m_pTitle  = NULL;
	m_Refresh = 0;
	m_Colors  = 0;
	m_Delay   = 5;
	m_Edit    = 0;
	m_Hide    = 0;
	m_uTags   = 0;
	m_pTags   = NULL;
	m_pType   = NULL;
	}

// Destructor

CWebPage::~CWebPage(void)
{
	delete m_pTitle;

	delete [] m_pTags;

	delete [] m_pType;
	}

// Initialization

void CWebPage::Load(PCBYTE &pData)
{
	ValidateLoad("CWebPage", pData);

	GetCoded(pData, m_pTitle);

	m_Refresh = GetByte(pData);
	m_Colors  = GetByte(pData);
	m_Edit    = GetByte(pData);
	m_Hide    = GetByte(pData);
	m_Delay   = GetWord(pData);
	m_PageSec = GetLong(pData);

	LoadTagList(pData);
	}

// Attributes

CString CWebPage::GetTitle(void) const
{
	return UniConvert(m_pTitle->GetText(L"Web Page"));
	}

BOOL CWebPage::HasTag(UINT uTag) const
{
	for( UINT n = 0; n < m_uTags; n++ ) {

		CDataRef const &Ref = (CDataRef &) m_pTags[n];

		if( Ref.t.m_Index == uTag ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Operations

void CWebPage::PreRegister(void)
{
	if( !m_Delay ) {

		SetScan(scanTrue);
		}
	}

void CWebPage::SetScan(UINT Code)
{
	CDataRef Ref;

	Ref.m_Ref = 0;

	for( UINT n = 0; n < m_uTags; n++ ) {

		if( m_pType[n] ) {

			CDataRef const &Ref = (CDataRef &) m_pTags[n];

			UINT  uTag = Ref.t.m_Index;

			CTag *pTag = m_pSrc->GetItem(uTag);

			pTag->SetScan(Ref, Code);
			}
		}
	}

BOOL CWebPage::WaitAvailable(void)
{
	CDataRef Ref;

	Ref.m_Ref  = 0;

	UINT uTime = m_Delay ? (1000 * m_Delay) : 100;

	SetTimer(uTime);

	while( GetTimer() ) {

		UINT n;

		for( n = 0; n < m_uTags; n++ ) {

			if( m_pType[n] ) {

				UINT  uTag = ((CDataRef &) m_pTags[n]).t.m_Index;

				CTag *pTag = m_pSrc->GetItem(uTag);

				if( !pTag->IsAvail(Ref) ) {

					break;
					}
				}
			}

		if( n < m_uTags ) {

			Sleep(25);

			continue;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CWebPage::LoadTagList(PCBYTE &pData)
{
	if( (m_uTags = GetWord(pData)) ) {

		m_pTags = New DWORD [ m_uTags ];

		m_pType = New  BYTE [ m_uTags ];

		for( UINT n = 0; n < m_uTags; n++ ) {

			DWORD    Data = GetLong(pData);

			CDataRef &Ref = (CDataRef &) Data;

			UINT  uTag = Ref.t.m_Index;
			
			CTag *pTag = m_pSrc->GetItem(uTag);

			if( pTag ) {

				UINT uType = pTag->GetDataType();

				m_pType[n] = BYTE(uType);

				m_pTags[n] = Data;
				
				continue;
				}

			m_pType[n] = typeVoid;

			m_pTags[n] = 0xFFFF;
			}

		return TRUE;
		}

	return FALSE;
	}

// End of File
