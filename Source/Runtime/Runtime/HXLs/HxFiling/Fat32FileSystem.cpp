
#include "Intern.hpp"

#include "Fat32FileSystem.hpp"

#include "FatDirEntry.hpp"

#include "FatDirName.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat32 File System
//

// Instantiator

IFilingSystem * Create_Fat32FileSystem(UINT iDisk, UINT iVolume, UINT uBoot)
{
	return New CFat32FileSystem(iDisk, iVolume, uBoot);
	}

// Constructor 

CFat32FileSystem::CFat32FileSystem(UINT iDisk, UINT iVolume, UINT uBoot) : CFatFileSystem(iDisk, iVolume, uBoot)
{
	}

// Destructor

CFat32FileSystem::~CFat32FileSystem(void)
{	
	}

// Attributes

INT64 CFat32FileSystem::GetSize(void)
{
	return m_Boot.GetDataSize();
	}

INT64 CFat32FileSystem::GetFree(void)
{
	return INT64(m_Info.m_dwFreeCount) * INT64(m_Boot.GetClusterSize());
	}

// Methods

BOOL CFat32FileSystem::Init(void)
{
	if( CFilingSystem::Init() ) {

		ReadBoot();

		CopyBoot();

		InitFat();

		InitRoot();

		InitInfo();

		SaveInfo();

		return true;
		}

	return false;
	}

BOOL CFat32FileSystem::Start(void)
{
	if( CFilingSystem::Start() ) {
	
		ReadBoot();

		ReadInfo();

		FindInfo();

		Debug(debugInfo, "Started");

		return true;
		}
	
	return false;
	}

BOOL CFat32FileSystem::Stop(void)
{
	Debug(debugInfo, "Stopping...");

	SaveInfo();

	return CFilingSystem::Stop();
	}

// Enumeration

BOOL CFat32FileSystem::GetRoot(FSIndex &Index)
{
	MakeDataIndex(Index);

	Index.m_iCluster = m_Boot.GetRootDirCluster();

	return true;
	}

BOOL CFat32FileSystem::IsRoot(FSIndex const &Index)
{
	return Index.m_iCluster == m_Boot.GetRootDirCluster();
	}

// Layout

UINT CFat32FileSystem::GetRootCluster(void) const
{
	return m_Boot.GetRootDirCluster();
	}

UINT CFat32FileSystem::GetFirstSector(UINT uCluster) const
{
	return m_Boot.GetFirstSector(uCluster);
	}

// Implementation

BOOL CFat32FileSystem::ReadBoot(void)
{
	if( ReadSector(0, PBYTE(&m_Boot), sizeof(m_Boot)) ) { 

		m_Boot.LocalToHost();

		m_Boot.Dump();

		if( m_Boot.IsValid() ) {

			m_uIdent       = m_Boot.m_dwVolId;
				       
			m_uSize        = m_Boot.m_BiosParamBlock.m_dwTotalSectors;
				       
			m_uFat0        = m_Boot.GetFirstFatSector(0);
				       
			m_uFat1        = m_Boot.GetFirstFatSector(1);
				       
			m_uData        = m_Boot.GetFirstDataSector(); 

			m_uClusterSize = m_Boot.GetClusterSize();

			m_uSectorSize  = m_Boot.m_BiosParamBlock.m_wSectorSize;

			return true;
			}
		}

	return false;
	}

BOOL CFat32FileSystem::CopyBoot(void)
{
	if( m_Boot.IsValid() ) {

		UINT uCopy = m_Boot.m_BiosParamBlock.m_wBootSector;

		m_Boot.HostToLocal();

		if( WriteSector(uCopy, PCBYTE(&m_Boot), sizeof(m_Boot)) ) {

			m_Boot.LocalToHost();

			return true;
			}

		m_Boot.LocalToHost();
		}

	return false;
	}

BOOL CFat32FileSystem::InitFat(void)
{
	if( m_Boot.IsValid() ) {
	
		UINT uTotal = m_Boot.GetFatSectors() * m_Boot.GetBiosParamBlock().m_bFatCount;

		UINT uFirst = m_Boot.GetFirstFatSector(0);

		for( UINT n = 0; n < uTotal; n ++ ) {

			ZeroSector(uFirst + n);
			}

		if( !WriteFatEntry(0, entryReserved) ) {

			return false;
			}

		if( !WriteFatEntry(1, entryEoc) ) {

			return false;
			}
		
		if( !WriteFatEntry(2, entryEoc) ) {

			return false;
			}

		return true;
		}

	return false;
	}

BOOL CFat32FileSystem::InitRoot(void)
{
	FSIndex Index;

	GetRoot(Index);

	if( ZeroData(Index.m_iCluster, 0, m_Boot.GetClusterSize()) ) {

		CFatDirEntry Entry;

		Entry.GetShort().MakeFreeLast();

		if( WriteEntry(Index.m_iCluster, 0, Entry) ) {

			return true;
			}
		}

	return false;
	}

BOOL CFat32FileSystem::InitInfo(void)
{
	m_Info.Init();

	if( m_Boot.IsValid() ) {

		m_Info.m_dwNextFree  = m_Boot.GetRootDirCluster() + 1;

		m_Info.m_dwFreeCount = m_Boot.GetDataClusters() - m_Info.m_dwNextFree - 1;

		return true;
		}

	return false;
	}

BOOL CFat32FileSystem::ReadInfo(void)
{
	DWORD dwInfo = m_Boot.m_BiosParamBlock.m_wFSInfoSector;

	if( ReadSector(dwInfo, PBYTE(&m_Info), sizeof(m_Info)) ) {

		m_Info.LocalToHost();

		m_Info.Dump();

		return m_Info.IsValid();
		}

	return false;
	}

BOOL CFat32FileSystem::SaveInfo(void)
{
	DWORD dwInfo = m_Boot.m_BiosParamBlock.m_wFSInfoSector;

	m_Info.HostToLocal();
	
	BOOL fResult = WriteSector(dwInfo, PBYTE(&m_Info), sizeof(m_Info));

	m_Info.LocalToHost();

	return fResult;
	}

BOOL CFat32FileSystem::FindInfo(void)
{
	if( !m_Info.IsValid() ) {

		m_Info.Init();

		m_Info.m_dwFreeCount = FindFreeCount();
		
		return true;
		}

	if( m_Info.m_dwFreeCount == NOTHING ) {

		m_Info.m_dwFreeCount = FindFreeCount();
		
		return true;
		}
	
	return false;
	}

// Flags

BOOL CFat32FileSystem::IsVolumeClean(void)
{
	return (ReadFatEntry(1) & entryVolClean) == entryVolClean;
	}

BOOL CFat32FileSystem::IsVolumeDirty(void)
{
	return (ReadFatEntry(1) & entryVolClean) != entryVolClean;
	}

BOOL CFat32FileSystem::HasHardError(void)
{
	return (ReadFatEntry(1) & entryHardError) != entryHardError;
	}

BOOL CFat32FileSystem::SetVolumeClean(void)
{
	return WriteFatEntry(1, ReadFatEntry(1) | entryVolClean);
	}

BOOL CFat32FileSystem::SetVolumeDirty(void)
{
	return WriteFatEntry(1, ReadFatEntry(1) & ~entryVolClean);
	}

BOOL CFat32FileSystem::SetHardError(void)
{
	return WriteFatEntry(1, ReadFatEntry(1) & ~entryHardError);
	}

// Data Allocation

UINT CFat32FileSystem::AllocData(UINT uAlloc, BOOL fZero)
{
	UINT uRequired = (uAlloc + m_uClusterSize - 1) / m_uClusterSize;

	UINT uThis     = entryInvalid;

	UINT uNext     = entryInvalid;

	UINT uCluster  = entryInvalid;

	m_pMutex->Wait(FOREVER);

	if( uRequired < m_Info.m_dwFreeCount ) {
				
		while( uRequired ) {

			uNext = FindFree();

			if( uNext != entryInvalid ) {

				if( uThis != entryInvalid ) {

					if( !WriteFatEntry(uThis, uNext) ) {

						break;
						}
					}

				if( WriteFatEntry(uNext, entryEoc) ) {

					m_Info.m_dwFreeCount --;		

					uRequired --;
					
					uThis = uNext;

					if( uCluster == entryInvalid ) {
					
						uCluster = uNext;
						}

					continue;
					}
				}
			
			break;
			}
			
		if( uRequired ) {

			FreeData(uCluster);

			uCluster = entryInvalid;
			}
		}

	m_pMutex->Free();

	if( uCluster == entryInvalid ) {

		Debug(debugWarn, "AllocData(Alloc=0x%8.8X) - FAILED", uAlloc);
		
		return entryInvalid;
		}

	if( fZero ) {

		ZeroData(uCluster, 0, m_uClusterSize);
		}
	
	return uCluster;
	}

UINT CFat32FileSystem::ReAllocData(UINT uCluster, UINT uAlloc, BOOL fZero)
{
	UINT uRequired  = (uAlloc + m_uClusterSize - 1) / m_uClusterSize;

	UINT uAllocated = FindCount(uCluster);

	if( uAllocated != entryInvalid ) {

		if( uRequired == uAllocated ) {

			return uCluster;
			}

		if( uRequired > uAllocated ) {

			UINT uLink = AllocData((uRequired - uAllocated) * m_Boot.GetClusterSize(), fZero);

			if( uLink != entryInvalid ) {

				UINT uLast = FindLast(uCluster);

				if( WriteFatEntry(uLast, uLink) ) {

					return uCluster;
					}
				}

			Debug(debugWarn, "ReAllocData(Cluster=0x%8.8X, Alloc=0x%8.8X) - FAILED", uCluster, uAlloc);
						
			return entryInvalid;
			}

		if( uRequired < uAllocated ) {

			UINT uLast = FindNext(uCluster, uRequired - 1);

			UINT uFree = FindNext(uLast);

			if( WriteFatEntry(uLast, entryEoc) ) {

				FreeData(uFree);

				return uCluster;
				}

			Debug(debugWarn, "ReAllocData(Cluster=0x%8.8X, Alloc=0x%8.8X) - FAILED", uCluster, uAlloc);
						
			return entryInvalid;
			}
		}

	Debug(debugErr, "ReAllocData(Cluster=0x%8.8X, Alloc=0x%8.8X) - FAILED : Invalid Param", uCluster, uAlloc);

	return entryInvalid;
	}

BOOL CFat32FileSystem::FreeData(UINT uCluster)
{
	if( uCluster < 2 || uCluster > m_Boot.GetDataClusters() ) {

		Debug(debugErr, "FreeData(Cluster=0x%8.8X) - FAILED : Invalid Param", uCluster);

		return false;
		}

	if( m_Info.m_dwNextFree == entryInvalid ) {

		m_Info.m_dwNextFree = uCluster;
		}

	while( uCluster != entryEoc ) {

		UINT uNext = FindNext(uCluster);

		if( uNext != entryInvalid ) {

			m_pMutex->Wait(FOREVER);

			if( WriteFatEntry(uCluster, entryFree) ) {

				m_Info.m_dwFreeCount ++;

				uCluster = uNext;

				m_pMutex->Free();

				continue;
				}
			
			m_pMutex->Free();
			}
		
		Debug(debugWarn, "FreeData(Cluster=0x%8.8X) - FAILED", uCluster);

		return false;
		}

	return true;
	}

// Chain

UINT CFat32FileSystem::FindNext(UINT uCluster)
{
	uCluster = ReadFatEntry(uCluster);

	switch( uCluster ) {

		case entryInvalid:
		case entryReserved:
		case entryBad:
		case entryFree:

			return entryInvalid;
		}

	return uCluster;
	}

UINT CFat32FileSystem::FindNext(UINT uCluster, UINT uCount)
{
	while( uCount -- ) {

		uCluster = ReadFatEntry(uCluster);

		switch( uCluster ) {

			case entryEoc: 
			case entryInvalid:
			case entryReserved:
			case entryBad:
			case entryFree:

				return entryInvalid;
			}
		}

	return uCluster;
	}

UINT CFat32FileSystem::FindLast(UINT uCluster)
{
	for(;;) {

		UINT uNext = ReadFatEntry(uCluster);

		switch( uNext ) {

			case entryEoc: 

				return uCluster;

			case entryInvalid:
			case entryReserved:
			case entryBad:
			case entryFree:

				return entryInvalid;
			}

		uCluster = uNext;
		}
	}

UINT CFat32FileSystem::FindCount(UINT uCluster)
{
	for( UINT n = 0; ; n++ ) {

		uCluster = ReadFatEntry(uCluster);

		switch( uCluster ) {

			case entryEoc: 

				return n + 1;

			case entryInvalid:
			case entryReserved:
			case entryBad:
			case entryFree:

				return entryInvalid;
			}
		}

	return 0;
	}

UINT CFat32FileSystem::FindFree(void)
{
	if( m_Info.IsValid() ) {
		
		if( !m_Info.m_dwFreeCount ) {

			return entryInvalid;
			}

		if( m_Info.m_dwNextFree != entryInvalid ) {

			if( ReadFatEntry(m_Info.m_dwNextFree) == entryFree ) {

				UINT uFatCluster    = m_Info.m_dwNextFree;

				m_Info.m_dwNextFree = entryInvalid;

				m_Info.m_dwFreeCount--;
					
				return uFatCluster;
				}

			m_Info.m_dwNextFree = entryInvalid;
			}
		}

	UINT uFatClusters = m_Boot.GetDataClusters();

	UINT uFatSectors  = m_Boot.GetFatSectors();

	UINT uPerSector   = m_Boot.GetFatPerSector();

	UINT uFatSector   = m_uFat0;

	for( UINT nSector = 0; nSector < uFatSectors; nSector ++ ) {
		
		PDWORD pEntry = PDWORD(LockSector(uFatSector, FALSE));

		if( pEntry ) {

			UINT uThis = Min(uPerSector, uFatClusters);

			for( UINT nCluster = 0; nCluster < uThis; nCluster ++ ) {

				if( IntelToHost(pEntry[nCluster]) == entryFree ) {

					UnlockSector(uFatSector, false);

					m_Info.m_dwFreeCount--;

					return nSector * uPerSector + nCluster;
					}
				}

			uFatClusters -= uThis;

			UnlockSector(uFatSector++, false);
			
			continue;
			}
		
		break;
		}

	Debug(debugWarn, "FindFree() - FAILED");

	return entryInvalid;
	}

UINT CFat32FileSystem::FindFreeCount(void)
{
	UINT uFatClusters = m_Boot.GetDataClusters();

	UINT uFatSectors  = m_Boot.GetFatSectors();

	UINT uPerSector   = m_Boot.GetFatPerSector();

	UINT uFatSector   = m_uFat0;

	UINT uFreeCount   = 0;

	for( UINT nSector = 0; nSector < uFatSectors; nSector ++ ) {
		
		PDWORD pEntry = PDWORD(LockSector(uFatSector, FALSE));

		if( pEntry ) {

			UINT uThis = Min(uPerSector, uFatClusters);

			for( UINT nCluster = 0; nCluster < uThis; nCluster ++ ) {

				if( IntelToHost(pEntry[nCluster]) == entryFree ) {

					uFreeCount ++;
					}
				}

			uFatClusters -= uThis;

			UnlockSector(uFatSector++, false);
			
			continue;
			}
		
		break;
		}

	return uFreeCount;
	}

// Fat Entry

UINT CFat32FileSystem::ReadFatEntry(UINT uCluster)
{
	return ReadFatEntry(0, uCluster);
	}

BOOL CFat32FileSystem::WriteFatEntry(UINT uCluster, UINT uEntry)
{
	return WriteFatEntry(0, uCluster, uEntry) && WriteFatEntry(1, uCluster, uEntry);
	}

UINT CFat32FileSystem::ReadFatEntry(UINT uFat, UINT uCluster)
{
	Debug(debugData, "ReadFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X)", uFat, uCluster);

	CDataSector Sector;

	if( MapFatEntry(uFat, uCluster, Sector) ) {

		UINT uData;

		if( ReadSector(Sector, PBYTE(&uData), sizeof(DWORD)) ) {

			uData  = IntelToHost(DWORD(uData));

			uData &= entryMask; 
				
			return uData;
			}
		}

	Debug(debugData, "ReadFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X) - FAILED", uFat, uCluster);

	return entryInvalid;
	}

BOOL CFat32FileSystem::WriteFatEntry(UINT uFat, UINT uCluster, UINT uEntry)
{
	Debug(debugData, "WriteFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X)", uFat, uCluster, uEntry);

	CDataSector Sector;

	if( MapFatEntry(uFat, uCluster, Sector) ) {

		uEntry &= entryMask;
			
		uEntry  = HostToIntel(DWORD(uEntry));

		return WriteSector(Sector, PBYTE(&uEntry), sizeof(DWORD));
		}

	Debug(debugWarn, "WriteFATEntry(Fat=%d, Cluster=0x%8.8X, Entry=0x%8.8X) - FAILED", uFat, uCluster, uEntry);

	return false;
	}

BOOL CFat32FileSystem::MapFatEntry(UINT uFat, UINT uCluster, CDataSector &Ptr) const
{
	if( uFat < m_Boot.m_BiosParamBlock.m_bFatCount ) {

		if( uCluster < m_Boot.GetDataClusters() ) {

			UINT uFatSect = uFat ? m_uFat1 : m_uFat0;

			UINT uDataPtr = uCluster * sizeof(DWORD);
			
			Ptr.m_uSector = uDataPtr / m_uSectorSize + uFatSect;

			Ptr.m_uOffset = uDataPtr % m_uSectorSize;

			return true;
			}
		}

	return false;
	}

// End of File
