
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextExprEnum_HPP

#define INCLUDE_UITextExprEnum_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Enumeration
//

class CUITextExprEnum : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextExprEnum(void);

	protected:
		// Data Option
		struct CEntry
		{
			// Data Members
			CString m_Text;
			UINT    m_Data;
			};

		// Data Members
		CArray <CEntry> m_Enum;
		UINT            m_uMask;

		// Overridables
		void    OnBind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
		BOOL    OnEnumValues(CStringArray &List);

		// Implementation
		void AddData(UINT Data, CString Text);
		BOOL IsNumberConst(CString const &Text);
	};

// End of File

#endif
