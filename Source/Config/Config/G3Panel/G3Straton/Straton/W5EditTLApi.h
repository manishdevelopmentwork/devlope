#if !defined(_W5EDITTLAPI_H_)
#define _W5EDITTLAPI_H_

#define W5EDITTL_CLASSNAME		_T("W5EditTL")

/* exchange structures */
typedef struct _s_W5TLTargetMark {
    HTREEITEM hItem;
    BOOL      bAfter;
    int       iCol;
} str_W5TLTargetMark;   //used to exchange status of the target mark of a drag n drop operation

//Control styles /////////////////////////////////////////////////////////////////
#define TL_HIDEHEADER           0x0001
#define TL_DISABLEDND           0x0002
#define TL_NOTOOLTIP            0x0004
#define TL_NOHORZSCROLL         0x0008
#define TL_COMMENTWITHNAME      0x0010
#define TL_MULTISEL             0x0020
#define TL_ONESELSELECTION      0x0040
#define TL_HASLINES             0x0080
#define TL_LINESATROOT          0x0100
#define TL_HASBUTTONS           0x0200
#define TL_SHOWSELALWAYS        0x0400
#define TL_DISABLEHEADERRESIZE  0x0800
#define TL_AUTOEDIT             0x1000   //if this style is set, the standard treelist open its own 
                                         //dialog boxes to enter values
#define TL_HOTHEADER            0x2000   //indicates that header is hot paint when user can change col selected
#define TL_DISABLEHEADERVERTRESIZE  0x4000 //can not resize vertical column to zoom content
#define TL_HIDELINESEL          0x8000


//sort flags (internal) ///////////////////////////////////////////////////////
#define TLSORT_NONE         0
#define TLSORT_ALPHABETIC   1
#define TLSORT_EXEC         2

//dictionary filter flags /////////////////////////////////////////////////////
#define TLFILTER_ONLYSTRUCTURE      0x0001 //dictionary only display structures
#define TLFILTER_HIDESTRUCT         0x0002 //dictionary hides structures
#define TLFILTER_HIDEINSTANCE       0x0004 //hide the instances of FB and UDFB in dictionary
#define TLFILTER_ONLYGLOBAL         0x0008 //show only global group
#define TLFILTER_ONLYSINGLETYPE     0x0010 //show only single types in dictionary
#define TLFILTER_HIDERETAIN         0x0020 //hide the group of RETAIN variables

//constants ///////////////////////////////////////////////////////////////////
#define W5F_NOCOL               -1
#define W5F_CURRENTCOL          -2

#define TLMOVE_UP               1
#define TLMOVE_DOWN             2
#define TLMOVE_LEFT             3
#define TLMOVE_RIGHT            4
#define TLMOVE_BEGIN            5
#define TLMOVE_END              6

#define CONST_TRUE              "#TRUE#"
#define CONST_FALSE             "#FALSE#"


//constants for W5EDITTL_PRGLIST //////////////////////////////////////////////
#define TLPRG_PRGSFC            1
#define TLPRG_PRGSFCCHILD       2
#define TLPRG_PRGFBD            3
#define TLPRG_PRGLD             4
#define TLPRG_PRGST             5
#define TLPRG_PRGIL             6
#define TLPRG_UDFBFBD           7
#define TLPRG_UDFBLD            8
#define TLPRG_UDFBST            9
#define TLPRG_UDFBIL            10
#define TLPRG_SUBFBD            11
#define TLPRG_SUBLD             12
#define TLPRG_SUBST             13
#define TLPRG_SUBIL             14


//constants for W5EDITTL_MULTITREEPRJ /////////////////////////////////////////
#define TLPRGCMD_START          1
#define TLPRGCMD_STOP           2
#define TLPRGCMD_PAUSE          3
#define TLPRGCMD_RESUME         4


//constants for W5EDITTL_DEFINETREE  //////////////////////////////////////////
#define W5EDITTL_DEFINENONE     "W5EDITTL_DEFINENONE"
#define W5EDITTL_DEFINELIB      "W5EDITTL_DEFINELIB"
#define W5EDITTL_DEFINECOMM     "W5EDITTL_DEFINECOMM"
#define W5EDITTL_DEFINENGLOB    "W5EDITTL_DEFINENGLOB"
#define W5EDITTL_DEFINENLOC     "W5EDITTL_DEFINENLOC"


//constants for W5EDITTL_MULTIPRJ  ////////////////////////////////////////////
#define TLFILE_NONE             0x0000
#define TLFILE_SPY              0x0001
#define TLFILE_RECIPE           0x0002
#define TLFILE_GRAPHICS         0x0004
#define TLFILE_SIGNAL           0x0008
#define TLFILE_STRINGTABLE      0x0010
#define TLFILE_CURVE            0x0020
#define TLFILE_IEC850           0x0040

//constants for W5EDITTL_MULTIPRJ  ////////////////////////////////////////////
#define FLAGS_HMISTRING         0x0001
#define FLAGS_HMIBMP            0x0002
#define FLAGS_HMIFONT           0x0004

//constants for W5EDITTL_FRAMECLIENT //////////////////////////////////////////
#define FRAME_GOOSE             0x88B8
#define FRAME_ALL               0xFFFFFFFF

//constant for file extensions ////////////////////////////////////////////////
#define EXT_SPY         _T(".spl")
#define EXT_RCP         _T(".rcp")
#define EXT_GRA         _T(".gra")
#define EXT_STRINGTABLE _T(".stb")
#define EXT_SIGNAL      _T(".sgl")
#define EXT_CURVE       _T(".ss5")
#define EXT_IEC850      _T(".icd")

//column types ////////////////////////////////////////////////////////////////
#define NO_TYPE             0   //column type is not defined
#define TYPE_NAME           1   //database, symbol ID must be set in the item data
#define TYPE_TYPE           2   //database, symbol ID must be set in the item data
#define TYPE_DIM            3   //database, symbol ID must be set in the item data
#define TYPE_ATTR           4   //database, symbol ID must be set in the item data
#define TYPE_INIT           5   //database, symbol ID must be set in the item data
#define TYPE_ALIAS          6   //database, symbol ID must be set in the item data
#define TYPE_COMM           7   //database, symbol ID must be set in the item data
#define TYPE_VALUE          8   //middleware value
#define TYPE_SYB            9   //database, symbol ID must be set in the item data
#define TYPE_PROPS          10  //database, need N� property, symbol ID must be set in the item data
#define TYPE_CUSTOM         11  // need IDCustom (see TC_xxx defines)
#define TYPE_ADD            12  // need N� of added col (0 based!)
#define TYPE_ITEM           13  // type depends of item (see TC_xxx defines)
#define TYPE_VARUSERGROUP   14  //database, symbol ID must be set in the item data
#define TYPE_CONTENT        15  //database, symbol ID must be set in the item data (for a program display content)
#define TYPE_CELL           16  //the content of each cell has its own edit type
#define TYPE_MTSHARE        17  //database, symbol ID must be set in the item data


//NB: TYPE_CUSTOM, TYPE_ITEM ans TYPE_CELL are different: 
//TYPE_CUSTOM is used to set the entire column with one edit type
//TYPE_ITEM   is used to set only one line with one edit type (when grid is used as property grid)
//TYPE_CELL   is used to set only one cell with one edit type, the caller has to set each edit type for each cell by default all cells are TC_EDIT

#define TC_NONE         0
#define TC_EDIT         1
#define TC_COLOR        2
#define TC_FILEPATH     3
#define TC_ENUM         4
#define TC_SELECTVAR    5
#define TC_FONTNAME     6
#define TC_FONTSIZE     7
#define TC_CHECK        8
#define TC_SPIN         9
#define TC_BITMAP       10
#define TC_PROGRESS     11
#define TC_ENUMEDIT     12
#define TC_MAGNETO      13  //is a mix of flags values: see TC_MAGNETO_xxx for possible values
#define TC_GRID2        14  //used to edit 2 coulmns value in same cell
#define TC_PASSWORD     15
#define TC_BASEFILE     16  //used with mask (high word: see TLFILE_xxx for possible values)
#define TC_OEM          17  //used to edit user cell
#define TC_NOTIF        18  //used to test double click, editcell notif parent only
#define TC_GRID1        19  //used to edit a list of values in same cell
#define TC_EDITMULTI    20


//magneto constants ///////////////////////////////////////////////////////////
//0xaaaabbbb, bbbb: drawn icon and aaaa: activated icons
#define TC_MAGNETO_PREVIOUS         0x00000001      //  |<<
#define TC_MAGNETO_FASTBACKWARD     0x00000002      //  <<
#define TC_MAGNETO_BACK             0x00000004      //  <
#define TC_MAGNETO_PAUSE            0x00000008      //  ||
#define TC_MAGNETO_STOP             0x00000010      //  
#define TC_MAGNETO_PREVIOUSUP       0x00000020      //  
#define TC_MAGNETO_FASTUP           0x00000040      //
#define TC_MAGNETO_UP               0x00000080      //
#define TC_MAGNETO_DOWN             0x00000100      //
#define TC_MAGNETO_FASTDOWN         0x00000200      //
#define TC_MAGNETO_NEXTDOWN         0x00000400      //
#define TC_MAGNETO_PLAY             0x00000800      //  >
#define TC_MAGNETO_FORWARD          0x00001000      //  >>
#define TC_MAGNETO_NEXT             0x00002000      //  >>|

#define TC_MAGNETO_ON_PREVIOUS      0x00010000
#define TC_MAGNETO_ON_FASTBACKWARD  0x00020000
#define TC_MAGNETO_ON_BACK          0x00040000
#define TC_MAGNETO_ON_PAUSE         0x00080000
#define TC_MAGNETO_ON_STOP          0x00100000
#define TC_MAGNETO_ON_PREVIOUSUP    0x00200000
#define TC_MAGNETO_ON_FASTUP        0x00400000
#define TC_MAGNETO_ON_UP            0x00800000
#define TC_MAGNETO_ON_DOWN          0x01000000
#define TC_MAGNETO_ON_FASTDOWN      0x02000000
#define TC_MAGNETO_ON_NEXTDOWN      0x04000000
#define TC_MAGNETO_ON_PLAY          0x08000000
#define TC_MAGNETO_ON_FORWARD       0x10000000
#define TC_MAGNETO_ON_NEXT          0x20000000

//TC_GRID2 constants ///////////////////////////////////////////////////////////
//used to separate item in an extradata cell
#define EXTRADATA_SEP      (TCHAR)(0x03)    //separate doublons
#define EXTRADATA_SEP2     (TCHAR)(0x04)    //separate internal value and user value


#ifdef __cplusplus
extern "C" {
#endif

#ifdef W5EDITTLDLL
	/* Build the DLL */
	#define W5EDITTL_APICALL __declspec(dllexport)
#else
	#define W5EDITTL_APICALL __declspec(dllimport)
#endif

#ifdef __cplusplus
}
#endif

#endif // !defined(_W5EDITTLAPI_H_)
