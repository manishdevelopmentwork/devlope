
//////////////////////////////////////////////////////////////////////////
//
// OEM Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Main Loop
//

// Static Data

static	BOOL	m_fOem	    = FALSE;

static	char	m_sName[32] = { 0 };

static	char	m_sApp [32] = { 0 };

static	char	m_sExt [4]  = { 0 };

static	char	m_sKeys[32] = { 0 };

static	WORD	m_wBase     = 0;

// Prototypes

global	BOOL	OemInit(void);
global	BOOL	IsOem(void);
global	PCTXT	GetOemName(void);
global	void	SetOemName(PCTXT pName);
global	PCTXT	GetOemAppName(void);
global	PCTXT	GetOemImageExt(void);
global	PCTXT	GetOemKeyList(void);
global	WORD	GetOemDriverBase(void);

// Code

global	BOOL	OemInit(void)
{
	g_pIdentity->GetOemName(m_sName, sizeof(m_sName));

	g_pIdentity->GetOemApp (m_sApp,  sizeof(m_sApp) );

	switch( BYTE(m_sName[0]) ) {

		case 0x00:
		case 0xFF:
			m_fOem = FALSE;
			break;

		default:
			m_fOem = TRUE;
			break;
		}

	if( m_fOem ) {

		if( !stricmp(m_sName, "Preferred Instruments") ) {

			strcpy(m_sExt,  "oi3");

			strcpy(m_sApp,  "OIT_Edit_3.2");

			strcpy(m_sKeys, "sssssssssm");

			m_wBase = 0 * 0x0400;

			return TRUE;
			}

		if( !stricmp(m_sName, "FW Murphy") ) {

			strcpy(m_sExt,  "mvi");

			strcpy(m_sApp,  "M-VIEW DESIGNER 3.2");

			strcpy(m_sKeys, "sssssssssm");

			m_wBase = 0 * 0x0400;

			return TRUE;
			}

		if( !stricmp(m_sName, "Monico") ) {

			strcpy(m_sExt,  "mi2");

			strcpy(m_sApp,  "MonicoView 3.2");

			strcpy(m_sKeys, "sssssssssm");

			m_wBase = 0 * 0x0400;

			return TRUE;
			}
		}

	strcpy(m_sName, "Red Lion Controls");

	strcpy(m_sApp,  "Crimson 3.2");

	strcpy(m_sExt,  "ci3");

	strcpy(m_sKeys, "sssssssssm");

	m_wBase = 0 * 0x0400;

	return FALSE;
	}

global	void	SetOemName(PCTXT pName)
{
	strcpy(m_sName, pName);
	}

global	BOOL	IsOem(void)
{
	return m_fOem;
	}

global	PCTXT   GetOemName(void)
{
	return m_sName;
	}

global	PCTXT   GetOemAppName(void)
{
	return m_sApp;
	}

global	PCTXT	GetOemImageExt(void)
{
	return m_sExt;
	}

global	PCTXT	GetOemKeyList(void)
{
	return m_sKeys;
	}

global	WORD	GetOemDriverBase(void)
{
	return m_wBase;
	}

// End of File
