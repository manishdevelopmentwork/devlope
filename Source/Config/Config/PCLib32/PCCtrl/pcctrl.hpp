
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCCTRL_HPP
	
#define	INCLUDE_PCCTRL_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcwin.hpp>

#include <commctrl.h>

#include <richedit.h>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pcctrl.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "comctl32.lib")

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCCTRL

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pcctrl.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRange;
class CCtrlWnd;

//////////////////////////////////////////////////////////////////////////
//
// Notification Dispatch
//

#include "notmsg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Linear Range
//

class DLLAPI CRange
{
	public:
		// Data Members
		INT m_nFrom;
		INT m_nTo;

		// Standard Constructors
		CRange(void);
		CRange(INT nFrom, INT nTo);

		// Explicit Constructors
		explicit CRange(BOOL fAll);
		explicit CRange(DWORD dwRange);

		// Conversion
		operator DWORD(void) const;
		operator WORD (void) const;

		// Attributes
		UINT GetCount(void) const;
		BOOL IsEmpty (void) const;

		// Operations
		void Empty(void);
		void Set(BOOL fAll);
		void Set(INT nFrom, INT nTo);
		void Reverse(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Abstract Control Window
//

class DLLAPI CCtrlWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCtrlWnd(void);

		// Normal Creation
		BOOL Create(DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID);
		BOOL Create(DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID);
		BOOL Create(PCTXT pName, DWORD dwExStyle, DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID);
		BOOL Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, HWND hParent, UINT uID);

		// Common Control Management
		static BOOL IsControlClassLoaded(DWORD dwICC);
		static BOOL LoadControlClass(DWORD dwICC);

		// Handle Lookup
		static CCtrlWnd & FromHandle(HWND hWnd);

	protected:
		// Static Data
		static DWORD m_dwICC;

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Default Class Definition
		BOOL GetClassDetails(WNDCLASSEX &Class) const;

		// Implementation
		LRESULT SendNotify(UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStatic;
class CScrollBar;
class CButton;
class CListBox;
class CComboBox;
class CEditCtrl;
class CRichEditCtrl;

//////////////////////////////////////////////////////////////////////////
//
// Static Control
//

class DLLAPI CStatic : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStatic(void);

		// Attributes
		CIcon  & GetIcon(void) const;
		HANDLE   GetImage(UINT uType) const;

		// Operations
		void SetIcon(CIcon const &Icon);
		void SetImage(UINT uType, HANDLE hImage);
		void SetImage(CBitmap &Bitmap);
		void SetImage(CIcon &Icon);

		// Handle Lookup
		static CStatic & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Scroll Bar Control
//

class DLLAPI CScrollBar : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CScrollBar(void);

		// Attributes
		int  GetScrollPos(void) const;
		int  GetRangeMin(void) const;
		int  GetRangeMax(void) const;
		UINT GetPageSize(void) const;
		
		// Operations
		void SetScrollPos(int nPos, BOOL fPaint);
		void SetScrollRange(int nMin, int nMax, BOOL fPaint);
		void SetPageSize(UINT uPage, BOOL fPaint);
		void EnableArrows(BOOL fMin, BOOL fMax);

		// Handle Lookup
		static CScrollBar & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Button Control
//

class DLLAPI CButton : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CButton(void);

		// Attributes
		BOOL   IsChecked(void) const;
		UINT   GetState(void) const;
		HANDLE GetImage(UINT uType) const;
		
		// Operations
		void Click(void);
		void SetCheck(BOOL fCheck);
		void SetState(BOOL fState);
		void SetImage(UINT uType, HANDLE hImage);
		void SetImage(CBitmap &Bitmap);
		void SetImage(CIcon &Icon);

		// Handle Lookup
		static CButton & FromHandle(HWND hWnd);

	protected:
		// Data Members
		BOOL m_fDraw;

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);

		// Implementation
		BOOL DrawSelf(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// List Box Control
//

class DLLAPI CListBox : public CCtrlWnd 
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CListBox(void);

		// Attributes
		UINT    GetAnchorIndex(void) const;
		UINT    GetCaretIndex(void) const;
		UINT    GetCount(void) const;
		UINT    GetCurSel(void) const;
		DWORD   GetCurSelData(void) const;
		int     GetHorizontalExtent(void) const;
		DWORD   GetItemData(UINT uIndex) const;
		int     GetItemHeight(UINT uIndex) const;
		CRect   GetItemRect(UINT uIndex) const;
		void    GetItemRect(UINT uIndex, CRect &Rect) const;
		BOOL    GetSel(UINT uIndex) const;
		UINT    GetSelCount(void) const;
		UINT    GetSelItems(UINT uMax, UINT *pList) const;
		UINT	GetSelItems(CArray <UINT> &Array) const;
		CString GetText(UINT uIndex) const;
		UINT    GetTextLen(UINT uIndex) const;
		UINT    GetTopIndex(void) const;
		UINT    ItemFromPoint(CPoint const &Pos) const;

		// Operations
		UINT AddFile(PCTXT pFile);
		UINT AddString(PCTXT pString);
		UINT AddString(PCSTR pString);
		UINT AddString(PCTXT pString, DWORD dwData);
		UINT AddString(PCSTR pString, DWORD dwData);
		void AddStrings(CStringArray const &Array);
		void DeleteString(UINT uIndex);
		UINT Dir(UINT uAttr, PCTXT pFileSpec);
		UINT FindString(UINT uStart, PCTXT pString);
		UINT FindStringExact(UINT uStart, PCTXT pString);
		void InitStorage(UINT uCount, DWORD dwMemory);
		UINT InsertString(UINT uIndex, PCTXT pString);
		UINT InsertString(UINT uIndex, PCTXT pString, DWORD dwData);
		void InsertStrings(UINT uIndex, CStringArray const &Array);
		void ResetContent(void);
		void SelectString(UINT uIndex, PCTXT pString);
		void SelItemRange(UINT uFirst, UINT uLast, BOOL fSelect);
		void SelItemRange(CRange const &Range, BOOL fSelect);
		void SetAnchorIndex(UINT uIndex);
		void SetCaretIndex(UINT uIndex, BOOL fScroll);
		void SetColumnWidth(int nWidth);
		void SetCount(UINT uCount);
		void SetCurSel(UINT uIndex);
		BOOL SelectData(DWORD dwData);
		void SetHorizontalExtent(int nExtent);
		void SetItemData(UINT uIndex, DWORD dwData);
		void SetItemHeight(UINT uIndex, int nHeight);
		void SetSel(UINT uIndex, BOOL fSelect);
		void SetTabStops(UINT uTabs, int const *pTabs);
		void SetTabStops(CArray <int> const &Tabs);
		void SetTopIndex(UINT uIndex);

		// Handle Lookup
		static CListBox & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Combo Box Control
//

class DLLAPI CComboBox : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CComboBox(void);

		// Attributes
		UINT    GetCount(void) const;
		UINT    GetCurSel(void) const;
		DWORD   GetCurSelData(void) const;
		CRect   GetDroppedControlRect(void) const;
		void    GetDroppedControlRect(CRect &Rect) const;
		BOOL    GetDroppedState(void) const;
		int     GetDroppedWidth(void) const;
		CRange  GetEditSel(void) const;
		BOOL    GetExtendedUI(void) const;
		int     GetHorizontalExtent(void) const;
		DWORD   GetItemData(UINT uIndex) const;
		int     GetItemHeight(UINT uIndex) const;
		CString GetLBText(UINT uIndex) const;
		UINT    GetLBTextLen(UINT uIndex) const;
		UINT    GetTopIndex(void) const;
		
		// General Operations
		UINT AddString(PCTXT pString);
		UINT AddString(PCSTR pString);
		UINT AddString(PCTXT pString, DWORD dwData);
		UINT AddString(PCSTR pString, DWORD dwData);
		void AddStrings(CStringArray const &Array);
		void DeleteString(UINT uIndex);
		UINT Dir(UINT uAttr, PCTXT pFileSpec);
		UINT FindString(UINT uStart, PCTXT pString);
		UINT FindString(PCTXT pString);
		UINT FindStringExact(UINT uStart, PCTXT pString);
		UINT FindStringExact(PCTXT pString);
		void InitStorage(UINT uCount, DWORD dwMemory);
		UINT InsertString(UINT uIndex, PCTXT pString);
		UINT InsertString(UINT uIndex, PCTXT pString, DWORD dwData);
		void InsertStrings(UINT uIndex, CStringArray const &Array);
		void LimitText(UINT uLimit);
		void ResetContent(void);
		void SelectString(UINT uIndex, PCTXT pString);
		void SelectString(PCTXT pString);
		void SelectStringExact(UINT uIndex, PCTXT pString);
		void SelectStringExact(PCTXT pString);
		void SetCurSel(UINT uIndex);
		BOOL SelectData(DWORD dwData);
		void SetDroppedWidth(int nWidth);
		void SetEditSel(CRange const &Sel);
		void SetExtendedUI(BOOL fSetting);
		void SetHorizontalExtent(int nExtent);
		void SetItemData(UINT uIndex, DWORD dwData);
		void SetItemHeight(UINT uIndex, int nHeight);
		void SetTopIndex(UINT uIndex);
		void ShowDropDown(BOOL fShow);

		// Clipboard Functions
		void Clear(void);
		void Copy(void);
		void Cut(void);
		void Paste(void);

		// Handle Lookup
		static CComboBox & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);

		// Command Handlers
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Implementation
		BOOL CanPasteText(void) const;
		BOOL CanSelectAll(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Edit Control
//

class DLLAPI CEditCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEditCtrl(void);

		// Attributes
		BOOL     CanUndo(void) const;
		UINT	 CharFromPos(CPoint const &Pos) const;
		UINT     GetFirstVisibleLine(void) const;
		HANDLE   GetEditHandle(void) const;
		CString  GetLine(UINT uLine) const;
		UINT     GetLineCount(void) const;
		DWORD	 GetMargins(void) const;
		UINT	 GetMarginLeft(void) const;
		UINT	 GetMarginRight(void) const;
		BOOL     GetModify(void) const;
		TCHAR    GetPasswordChar(void) const;
		CRect    GetRect(void) const;
		void     GetRect(CRect &Rect) const;
		CRange   GetSel(void) const;
		UINT	 GetThumb(void) const;
		FARPROC  GetWordBreakProc(void) const;
		BOOL     IsReadOnly(void) const;
		UINT     LineFromChar(UINT uChar) const;
		UINT     LineIndex(UINT uLine) const;
		UINT     LineLength(UINT uChar) const;
		CPoint	 PosFromChar(UINT uChar) const;

		// General Operations
		void EmptyUndoBuffer(void);
		void FmtLines(BOOL fAddEOL);
		void LimitText(UINT uLimit);
		void LineScroll(int dx, int dy);
		void ReplaceSel(PCTXT pString);
		void SetEditHandle(HANDLE hHandle);
		void SetMargins(UINT uFlags, DWORD dwMargin);
		void SetMarginLeft(UINT uMargin);
		void SetMarginRight(UINT uMargin);
		void SetModify(BOOL fModify);
		void SetPasswordChar(TCHAR cChar);
		void SetReadOnly(BOOL fState);
		void SetRect(CRect const &Rect);
		void SetRectNP(CRect const &Rect);
		void SetSel(CRange const &Sel);
		void SetTabStops(UINT uTabs, int const *pTabs);
		void SetTabStops(CArray <int> const &Tabs);
		void SetWordBreakProc(FARPROC pfnProc);
		void Scroll(UINT uCode);
		void ScrollCaret(void);

		// Clipboard Functions		
		void Undo(void);
		void Clear(void);
		void Copy(void);
		void Cut(void);
		void Paste(void);
		
		// Handle Lookup
		static CEditCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Implementation
		BOOL CanPasteText(void) const;
		BOOL CanSelectAll(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Rich Edit Control
//

class DLLAPI CRichEditCtrl : public CEditCtrl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRichEditCtrl(void);

		// Handle Lookup
		static CRichEditCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CToolInfo;
class CToolTip;

//////////////////////////////////////////////////////////////////////////
//
// Tool Tip Info
//
		     
class DLLAPI CToolInfo : public TOOLINFO
{
	public:
		// Constructors
		CToolInfo(void);
		CToolInfo(CToolInfo const &That);
		CToolInfo(TOOLINFO const &Info);
		CToolInfo(HWND hWnd, UINT uID,   CRect const &Rect, PCTXT pText, UINT uFlags);
		CToolInfo(HWND hWnd, HWND hCtrl, CRect const &Rect, PCTXT pText, UINT uFlags);
		CToolInfo(HWND hWnd, UINT uID,   CRect const &Rect, PCTXT pText);
		CToolInfo(HWND hWnd, HWND hCtrl, CRect const &Rect, PCTXT pText);
		CToolInfo(HWND hWnd, UINT uID);
		CToolInfo(HWND hWnd, HWND hCtrl);

		// Assignment Operators
		CToolInfo const & operator = (CToolInfo const &That);
		CToolInfo const & operator = (TOOLINFO const &Info);

		// Attributes
		UINT	  GetFlags(void) const;
		BOOL	  TestFlag(UINT uFlags) const;
		CWnd &	  GetWnd(void) const;
		WPARAM	  GetID(void) const;
		CRect     GetRect(void) const;
		HINSTANCE GetInstance(void) const;
		PCTXT     GetText(void) const;
		LPARAM	  GetParam(void) const;
    
		// Operations
		void SetFlags(UINT uFlags);
		void SetWnd(HWND hWnd);
		void SetID(WPARAM uID);
		void SetID(HWND hCtrl);
		void SetID(HWND hWnd, WPARAM uID);
		void SetID(HWND hWnd, HWND hCtrl);
		void SetRect(CRect const &Rect);
		void SetInstance(HINSTANCE hInstance);
		void SetText(CString const &Text);
		void SetText(PCTXT pText);
		void SetTextBuffer(UINT uCount);
		void SetParam(LPARAM lParam);

	protected:
		// Data Members
		CString m_Text;
	};

/////////////////////////////////////////////////////////////////////////
//
// Tool Tip Control
//
		     
class DLLAPI CToolTip : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CToolTip(void);

		// Creation
		BOOL Create(HWND hParent, DWORD dwStyle);
		BOOL Create(HWND hParent, DWORD dwExStyle, DWORD dwStyle);

		// Enable Status
		static BOOL AreBalloonsEnabled(void);
		static BOOL AreBalloonsPossible(void);

		// Enable Control
		static BOOL EnableBalloons(BOOL fPrompt);

		// Attributes
		CRect     AdjustRect(BOOL fGrow, RECT const &Rect) const;
		BOOL      EnumTools(UINT uIndex, TOOLINFO &Info) const;
		CToolInfo EnumTools(UINT uIndex) const;
		BOOL	  HasCurrentTool(void) const;
		CSize	  GetBubbleSize(TOOLINFO &Info) const;
		CSize	  GetBubbleSize(HWND hWnd, UINT uID) const;
		CSize	  GetBubbleSize(HWND hWnd, HWND hCtrl) const;
		BOOL      GetCurrentTool(TOOLINFO &Info) const;
		CToolInfo GetCurrentTool(void) const;
		INT	  GetDelayTime(UINT uCode) const;
		void	  GetMargin(RECT &Rect) const;
		CRect	  GetMargin(void) const;
		INT	  GetMaxTipWidth(void) const;
		CColor	  GetTipBkColor(void) const;
		CColor	  GetTipTextColor(void) const;
		BOOL	  GetText(TOOLINFO &Info) const;
		CString   GetText(HWND hWnd, UINT uID) const;
		CString	  GetText(HWND hWnd, HWND hCtrl) const;
		UINT	  GetToolCount(void) const;
		BOOL	  GetToolInfo(TOOLINFO &Info) const;
		CToolInfo GetToolInfo(HWND hWnd, UINT uID) const;
		CToolInfo GetToolInfo(HWND hWnd, HWND hCtrl) const;
		BOOL      HitTest(TTHITTESTINFO &Test) const;
		BOOL	  HitTest(CToolInfo &Info, HWND hWnd, CPoint const &Pos) const;
		CToolInfo HitTest(HWND hWnd, CPoint const &Pos) const;
		CWnd &	  WindowFromPoint(CPoint const &Pos) const;

		// Operations
		void Activate(BOOL fActivate);
		BOOL AddTool(TOOLINFO const &Info);
		void DelTool(TOOLINFO const &Info);
		void DelTool(HWND hWnd, UINT uID);
		void DelTool(HWND hWnd, HWND hCtrl);
		void NewToolRect(TOOLINFO const &Info);
		void NewToolRect(HWND hWnd, UINT uID,   CRect const &Rect);
		void NewToolRect(HWND hWnd, HWND hCtrl, CRect const &Rect);
		void Pop(void);
		void RelayEvent(MSG &Message);
		void SetDelayTime(UINT uCode, UINT uDelay);
		void SetMargin(RECT const &Rect);
		void SetMaxTipWidth(INT nMax);
		void SetTipBkColor(CColor const &Color);
		void SetTipTextColor(CColor const &Color);
		void SetTitle(UINT Icon, PCTXT pText);
		void SetToolInfo(TOOLINFO const &Info);
		void TrackActivate(BOOL fActive, TOOLINFO const &Info);
		void TrackActivate(BOOL fActive, HWND hWnd, UINT uID);
		void TrackActivate(BOOL fActive, HWND hWnd, HWND hCtrl);
		void TrackPosition(CPoint const &Pos);
		void Update(void);
		void UpdateTipText(TOOLINFO const &Info);
		void UpdateTipText(HWND hWnd, UINT uID,   PCTXT pText);
		void UpdateTipText(HWND hWnd, HWND hCtrl, PCTXT pText);

		// Handle Lookup
		static CToolTip & FromHandle(HWND hWnd);

	protected:
		// Static Data
		static BOOL m_fAsked;

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CImageList;
class CHeaderItem;
class CHeaderControl;
class CListViewItem;
class CListViewColumn;
class CListView;
class CTreeViewItem;
class CTreeView;
class CMulTreeView;

//////////////////////////////////////////////////////////////////////////
//
// Image List
//

// cppcheck-suppress copyCtorAndEqOperator

class DLLAPI CImageList : public CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CImageList(void);
		CImageList(CImageList const &That);
		CImageList(PCTXT pName, int xSize, UINT uExtra, UINT uType, UINT uFlags);
		CImageList(CSize const &Size, UINT uFlags, UINT uInitial, UINT uExtra);
		CImageList(int xSize, int ySize, UINT uFlags, UINT uInitial, UINT uExtra);
		CImageList(UINT uFlags, UINT uInitial, UINT uExtra);
		CImageList(IStream *pStream);

		// Destructor
		~CImageList(void);

		// Creation
		BOOL Create(CImageList const &That);
		BOOL Create(PCTXT pName, int xSize, UINT uExtra, UINT uType, UINT uFlags);
		BOOL Create(CSize const &Size, UINT uFlags, UINT uInitial, UINT uExtra);
		BOOL Create(int xSize, int ySize, UINT uFlags, UINT uInitial, UINT uExtra);
		BOOL Create(UINT uFlags, UINT uInitial, UINT uExtra);
		BOOL Create(IStream *pStream);

		// Attachment
		BOOL Attach(HIMAGELIST hList);
		void Detach(BOOL fDelete);

		// Conversion
		operator HIMAGELIST (void) const;

		// Attributes
		HIMAGELIST GetHandle(void) const;
		CIcon &    ExtractIcon(UINT uImage) const;
		CColor     GetBkColor(void) const;
		CIcon &    GetIcon(UINT uImage, UINT uFlags) const;
		CSize      GetIconSize(void) const;
		UINT       GetImageCount(void) const;
		BOOL       GetImageInfo(UINT uImage, IMAGEINFO &Info) const;

		// Operations
		UINT Add(CBitmap const &Image, CBitmap const &Mask);
		UINT Add(CBitmap const &Image);
		UINT AddCursor(CCursor const &Cursor);
		UINT AddIcon(CIcon const &Icon);
		UINT AddMasked(CBitmap const &Image, CColor const &Color);
		UINT AddMasked(CBitmap const &Image);
		BOOL BeginDrag(UINT uImage, CPoint const &Pos);
		BOOL BeginDrag(UINT uImage, int xPos, int yPos);
		BOOL BeginDrag(UINT uImage);
		BOOL Copy(UINT uDest, UINT uSource, UINT uFlags);
		BOOL Draw(UINT uImage, HDC hDC, CPoint const &Pos, UINT uStyle);
		BOOL Draw(UINT uImage, HDC hDC, int xPos, int yPos, UINT uStyle);
		BOOL DrawEx(UINT uImage, HDC hDC, CPoint const &Pos, CSize const &Size, CColor const &Back, CColor const &Fore, UINT uStyle);
		BOOL DrawEx(UINT uImage, HDC hDC, CRect const &Rect, CColor const &Back, CColor const &Fore, UINT uStyle);
		BOOL DrawEx(UINT uImage, HDC hDC, int xPos, int yPos, int xSize, int ySize, CColor const &Back, CColor const &Fore, UINT uStyle);
		BOOL Remove(UINT uImage);
		BOOL RemoveAll(void);
		BOOL Replace(UINT uIndex, CBitmap const &Image, CBitmap const &Mask);
		BOOL Replace(UINT uIndex, CBitmap const &Image);
		UINT ReplaceCursor(UINT uIndex, CCursor const &Cursor);
		UINT ReplaceIcon(UINT uIndex, CIcon const &Icon);
		void SetBkColor(CColor const &Color);
		BOOL SetDragCursorImage(UINT uIndex, CPoint const &Pos);
		BOOL SetDragCursorImage(UINT uIndex, int xPos, int yPos);
		void SetExtern(BOOL fExtern);
		BOOL SetIconSize(CSize const &Size);
		BOOL SetIconSize(int xSize, int ySize);
		BOOL SetImageCount(UINT uCount);
		BOOL SetOverlayImage(UINT uImage, UINT uIndex);
		BOOL Write(IStream *pStream);

		// Drag Attributes
		static CImageList & GetDragImage(void);

		// Drag Operations
		static BOOL DragEnter(HWND hWnd, CPoint const &Pos);
		static BOOL DragEnter(HWND hWnd, int xPos, int yPos);
		static BOOL DragEnter(CPoint const &Pos);
		static BOOL DragEnter(int xPos, int yPos);
		static BOOL DragLeave(HWND hWnd);
		static BOOL DragLeave(void);
		static BOOL DragMove(CPoint const &Pos);
		static BOOL DragMove(int xPos, int yPos);
		static BOOL DragShowNoLock(BOOL fShow);
		static void EndDrag(void);

		// Handle Lookup
		static CImageList & FromHandle(HIMAGELIST hList);

	protected:
		// Data Members
		HIMAGELIST m_hList;
		BOOL       m_fExtern;

		// Implementation
		void CheckException(BOOL fCheck);
	};

//////////////////////////////////////////////////////////////////////////
//
// Header Control Item
//

class DLLAPI CHeaderItem : public HDITEM
{
	public:
		// Constructors
		CHeaderItem(void);
		CHeaderItem(CHeaderItem const &That);
		CHeaderItem(HDITEM const &Item);
		CHeaderItem(int cx, CString const &Text, HBITMAP hBitmap);
		CHeaderItem(int cx, PCTXT pText, HBITMAP hBitmap);
		CHeaderItem(int cx, CString const &Text, UINT uImage);
		CHeaderItem(int cx, PCTXT pText, UINT uImage);
		CHeaderItem(int cx, CString const &Text);
		CHeaderItem(int cx, PCTXT pText);
		CHeaderItem(UINT uMask);

		// Assignment Operators
		CHeaderItem const & operator = (CHeaderItem const &That);
		CHeaderItem const & operator = (HDITEM const &Item);

		// Attributes
		UINT	GetMask(void) const;
		BOOL	TestMask(UINT uMask) const;
		int     GetWidth(void) const;
		int     GetHeight(void) const;
		PCTXT   GetText(void) const;
		HBITMAP GetBitmap(void) const;
		UINT	GetFormat(void) const;
		LPARAM  GetParam(void) const;
		UINT	GetImage(void) const;
		UINT	GetOrder(void) const;
		UINT    GetFilterType(void) const;
		LPVOID  GetFilter(void) const;

		// Operations
		void SetMask(UINT uMask);
		void SetWidth(int cx);
		void SetHeight(int cy);
		void SetText(CString const &Text);
		void SetText(PCTXT pText);
		void SetTextBuffer(UINT uCount);
		void SetBitmap(HBITMAP hBitmap);
		void SetFormat(UINT uFormat);
		void SetParam(LPARAM lParam);
		void SetImage(UINT uImage);
		void SetOrder(UINT uOrder);
		void SetFilter(UINT uType, LPVOID pvFilter);

	protected:
		// Data Members
		CString m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// Header Control
//

class DLLAPI CHeaderCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHeaderCtrl(void);

		// Attributes
		UINT	     GetBitmapMargin(void) const;
		CImageList & GetImageList(void) const;
		BOOL	     GetItem(UINT uIndex, HDITEM &Item) const;
		CHeaderItem  GetItem(UINT uIndex) const;
		CString	     GetItemText(UINT uIndex) const;
		HBITMAP      GetItemBitmap(UINT uIndex) const;
		UINT	     GetItemImage(UINT uIndex) const;
		LPARAM	     GetItemParam(UINT uIndex) const;
		UINT	     GetItemCount(void) const;
		void	     GetItemRect(UINT uIndex, RECT &Rect) const;
		CRect	     GetItemRect(UINT uIndex) const;
		BOOL	     GetOrderArray(UINT uCount, UINT *pArray) const;
		BOOL	     GetOrderArray(CArray <UINT> &Array) const;
		UINT	     HitTest(HDHITTESTINFO &Info) const;
		UINT	     HitTestItem(CPoint const &Pos) const;
		UINT	     HitTestFlags(CPoint const &Pos) const;
		UINT	     HitTestItem(int xPos, int yPos) const;
		UINT	     HitTestFlags(int xPos, int yPos) const;
		BOOL	     Layout(HDLAYOUT &Layout) const;
		UINT	     OrderToIndex(UINT uOrder) const;

		// Operations
		BOOL	   AddItem(HDITEM const &Item);
		BOOL	   ClearFilter(UINT uIndex);
		HIMAGELIST CreateDragImage(UINT uIndex);
		BOOL	   DeleteItem(UINT uIndex);
		BOOL	   EditFilter(UINT uIndex, BOOL fDiscard);
		UINT	   InsertItem(UINT uIndex, HDITEM const &Item);
		BOOL	   SetBitmapMargin(UINT uMargin);
		BOOL	   SetFilterChangeTimeout(UINT uTimeout);
		BOOL	   SetHotDivider(UINT uIndex);
		BOOL	   SetHotDivider(CPoint const &Pos);
		BOOL	   SetHotDivider(int xPos, int yPos);
		BOOL	   SetImageList(HIMAGELIST hList);
		BOOL	   SetItem(UINT uIndex, HDITEM const &Item);
		BOOL	   SetOrderArray(UINT uCount, UINT const *pArray);
		BOOL	   SetOrderArray(CArray <UINT> const &Array);

		// Handle Lookup
		static CHeaderCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// List View Item
//

class DLLAPI CListViewItem : public LVITEM
{
	public:
		// Constructors
		CListViewItem(void);
		CListViewItem(CListViewItem const &That);
		CListViewItem(LVITEM const &Item);
		CListViewItem(UINT uItem, UINT uSub, CString const &Text, UINT uImage, LPARAM lParam);
		CListViewItem(UINT uItem, UINT uSub, PCTXT pText, UINT uImage, LPARAM lParam);
		CListViewItem(UINT uItem, CString const &Text, UINT uImage, LPARAM lParam);
		CListViewItem(UINT uItem, PCTXT pText, UINT uImage, LPARAM lParam);
		CListViewItem(UINT uItem, UINT uSub, CString const &Text, UINT uImage);
		CListViewItem(UINT uItem, UINT uSub, PCTXT pText, UINT uImage);
		CListViewItem(UINT uItem, CString const &Text, UINT uImage);
		CListViewItem(UINT uItem, PCTXT pText, UINT uImage);
		CListViewItem(UINT uMask);

		// Assignment Operators
		CListViewItem const & operator = (CListViewItem const &That);
		CListViewItem const & operator = (LVITEM const &Item);

		// Attributes
		UINT	GetMask(void) const;
		BOOL	TestMask(UINT uMask) const;
		UINT	GetItem(void) const;
		UINT	GetSubItem(void) const;
		UINT	GetState(void) const;
		UINT	GetStateMask(void) const;
		PCTXT   GetText(void) const;
		UINT	GetImage(void) const;
		LPARAM	GetParam(void) const;
		UINT	GetIndent(void) const;

		// Operations
		void SetMask(UINT uMask);
		void SetItem(UINT uItem);
		void SetItem(UINT uItem, UINT uSub);
		void SetSubItem(UINT nSub);
		void SetParam(LPARAM lParam);
		void SetState(UINT uState);
		void SetStateMask(UINT uMask);
		void SetText(CString const &Text);
		void SetText(PCTXT pText);
		void SetTextBuffer(UINT uCount);
  		void SetImage(UINT uImage);
		void SetIndent(UINT uIndent);

	protected:
		// Data Members
		CString m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// List View Column
//

class DLLAPI CListViewColumn : public LVCOLUMN
{
 	public:
		// Constructors
		CListViewColumn(void);
		CListViewColumn(CListViewColumn const &That);
		CListViewColumn(LVCOLUMN const &Column);
		CListViewColumn(UINT uSub, UINT uFormat, int cx, CString const &Text, UINT uImage);
		CListViewColumn(UINT uSub, UINT uFormat, int cx, PCTXT pText, UINT uImage);
		CListViewColumn(UINT uSub, UINT uFormat, int cx, CString const &Text);
		CListViewColumn(UINT uSub, UINT uFormat, int cx, PCTXT pText);
		CListViewColumn(UINT uMask);

		// Assignment Operators
		CListViewColumn const & operator = (CListViewColumn const &That);
		CListViewColumn const & operator = (LVCOLUMN const &Column);

		// Attributes
		UINT  GetMask(void) const;
		BOOL  TestMask(UINT uMask) const;
		UINT  GetFormat(void) const;
		int   GetWidth(void) const;
		PCTXT GetText(void) const;
		UINT  GetSubItem(void) const;
		UINT  GetImage(void) const;
		UINT  GetOrder(void) const;
	
		// Operations
		void SetMask(UINT uMask);
		void SetFormat(UINT uFormat);
		void SetWidth(int cx);
		void SetText(CString const &Text);
		void SetText(PCTXT pText);
		void SetTextBuffer(UINT uCount);
		void SetSubItem(UINT uSub);
  		void SetImage(UINT uImage);
		void SetOrder(UINT uOrder);
	
	protected:
		// Data Members
		CString m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// List View Control
//
		     
class DLLAPI CListView : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CListView(void);

		// Attributes
		CSize		ApproximateViewRect(UINT uCount, CSize Size) const;
		CSize		ApproximateViewRect(UINT uCount) const;
		UINT		FindItem(UINT uStart, LVFINDINFO &FindInfo) const;
		CColor		GetBkColor(void) const;
		BOOL		GetBkImage(LVBKIMAGE &Image) const;
		UINT		GetCallbackMask(void) const;
		BOOL		GetColumn(UINT uIndex, LVCOLUMN &Item) const;
		CListViewColumn	GetColumn(UINT uIndex) const;
		UINT		GetColumnFormat(UINT uIndex) const;
		UINT		GetColumnImage(UINT uIndex) const;
		CString		GetColumnText(UINT uIndex) const;
		int		GetColumnWidth(UINT uColumn) const;
		BOOL		GetColumnOrderArray(UINT uCount, UINT *pArray) const;
		BOOL		GetColumnOrderArray(CArray <UINT> &Array) const;
		UINT		GetCountPerPage(void) const;
		CEditCtrl &	GetEditControl(void) const;
		DWORD		GetExtendedListViewStyle(void) const;
		CHeaderCtrl &	GetHeader(void) const;
		HCURSOR		GetHotCursor(void) const;
		UINT		GetHotItem(void) const;
		UINT		GetHoverTime(void) const;
		CImageList &	GetImageList(UINT uList) const;
		CString		GetSearchString(void) const;
		BOOL		GetItem(LVITEM &Item) const;
		BOOL		GetItem(UINT uItem, UINT uSub, LVITEM &Item) const;
		BOOL		GetItem(UINT uItem, LVITEM &Item) const;
		CListViewItem	GetItem(UINT uItem, UINT uSub) const;
		CListViewItem	GetItem(UINT uItem) const;
		UINT		GetItemImage(UINT uItem, UINT uSub) const;
		UINT		GetItemImage(UINT uItem) const;
		LPARAM		GetItemParam(UINT uItem, UINT uSub) const;
		LPARAM		GetItemParam(UINT uItem) const;
		CString		GetItemText(UINT uItem, UINT uSub) const;
		CString		GetItemText(UINT uItem) const;
		UINT		GetItemCount(void) const;
		CPoint		GetItemPosition(UINT uItem) const;
		CRect		GetItemRect(UINT uItem, UINT uCode) const;
		int		GetItemSpacing(BOOL fSmall) const;
		UINT		GetItemState(UINT uItem, UINT uMask) const;
		UINT		GetNextItem(UINT uStart, UINT uFlags) const;
		UINT		GetNumberOfWorkAreas(void) const;
		CPoint		GetOrigin(void) const;
		UINT		GetSelection(void) const;
		UINT		GetSelectionList(CArray <UINT> &Array) const;
		UINT		GetSelectedCount(void) const;
		UINT		GetSelectionMark(void) const;
		int		GetStringWidth(PCTXT pString) const;
		CRect		GetSubItemRect(UINT uItem, UINT uSub, UINT uCode) const;
		CColor		GetTextBkColor(void) const;
		CColor		GetTextColor(void) const;
		UINT		GetTopIndex(void) const;
		CToolTip &	GetToolTips(void) const;
		CRect		GetViewRect(void) const;
		void		GetWorkAreas(UINT uCount, RECT *pRects) const;
		void		GetWorkAreas(CArray <CRect> &Array) const;
		UINT		HitTest(BOOL fSub, LVHITTESTINFO &Info) const;
		UINT		HitTestItem(BOOL fSub, CPoint const &Pos) const;
		UINT		HitTestSubItem(BOOL fSub, CPoint const &Pos) const;
		UINT		HitTestFlags(BOOL fSub, CPoint const &Pos) const;
		UINT		HitTestItem(BOOL fSub, int xPos, int yPos) const;
		UINT		HitTestSubItem(BOOL fSub, int xPos, int yPos) const;
		UINT		HitTestFlags(BOOL fSub, int xPos, int yPos) const;

		// Operations
		BOOL		Arrange(UINT uCode);
		CImageList &	CreateDragImage(UINT uItem);
		BOOL		DeleteAllItems(void);
		BOOL		DeleteColumn(UINT uColumn);
		BOOL		DeleteItem(UINT uItem);
		BOOL            EditLabel(UINT uItem);
		BOOL		EnsureVisible(UINT uItem, BOOL fPartial);
		UINT		InsertColumn(UINT uIndex, LVCOLUMN const &Column);
		UINT		InsertItem(LVITEM const &Item);
		BOOL		RedrawItems(UINT uFirst, UINT uLast);
		BOOL		Scroll(CSize const &Size);
		BOOL		Scroll(int xSize, int ySize);
		BOOL		SetBkColor(CColor const &Color);
		BOOL		SetBkImage(LVBKIMAGE &Image);
		BOOL		SetCallbackMask(UINT uMask);
		BOOL		SetColumn(UINT uIndex, LVCOLUMN const &Column);
		BOOL		SetColumnOrderArray(UINT uCount, UINT const *pArray);
		BOOL		SetColumnOrderArray(CArray <UINT> const &Array);
		BOOL		SetColumnWidth(UINT uIndex, int xSize);
		void		SetExtendedListViewStyle(DWORD dwMask, DWORD dwStyle);
		void		SetHotCursor(HCURSOR hCursor);
		UINT		SetHotItem(UINT uItem);
		UINT		SetHoverTime(UINT uTime);
		CSize		SetIconSpacing(CSize Size);
		void		SetImageList(UINT uList, HIMAGELIST hList);
		BOOL		SetItem(LVITEM &Item);
		void		SetItemCount(UINT uCount);
		BOOL		SetItemPosition(UINT uItem, CPoint const &Pos);
		BOOL		SetItemPosition(UINT uItem, int xPos, int yPos);
		BOOL		SetItemState(UINT uItem, UINT uMask, UINT uState);
		BOOL		SetItemText(UINT uItem, UINT uSub, PCTXT pText);
		BOOL		SetItemText(UINT uItem, PCTXT pText);
		BOOL		SetItemParam(UINT uItem, LPARAM lParam);
		BOOL		SetItemParam(UINT uItem, UINT uSub, LPARAM lParam);
		UINT		SetSelectionMark(UINT uItem);
		BOOL		SetTextBkColor(CColor const &Color);
		BOOL		SetTextColor(CColor const &Color);
		void		SetToolTips(CToolTip &Tips);
		void		SetWorkAreas(UINT uCount, RECT const *pRects);
		void		SetWorkAreas(CArray <CRect> const &Array);
		void		SetWorkArea(CRect const &Rect);
		BOOL		SortItems(FARPROC pfnCompare, LPARAM lParam);
		BOOL		SortItemsEx(FARPROC pfnCompare, LPARAM lParam);
		BOOL		Update(void);

		// Handle Lookup
		static CListView & FromHandle(HWND hWnd);

	protected:
		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tree View Item
//

class DLLAPI CTreeViewItem : public TVITEMEX
{
	public:
		// Constructors
		CTreeViewItem(void);
		CTreeViewItem(CTreeViewItem const &That);
		CTreeViewItem(TVITEMEX const &Item);
		CTreeViewItem(CString const &Text, UINT uImage);
		CTreeViewItem(PCTXT pText, UINT uImage);
		CTreeViewItem(CString const &Text, UINT uImage, UINT uState);
		CTreeViewItem(PCTXT pText, UINT uImage, UINT uState);
		CTreeViewItem(HTREEITEM hItem);
		CTreeViewItem(HTREEITEM hItem, CString const &Text);
		CTreeViewItem(HTREEITEM hItem, PCTXT pText);
		CTreeViewItem(HTREEITEM hItem, UINT uMask);
		CTreeViewItem(UINT uMask);

		// Assignment Operators
		CTreeViewItem const & operator = (CTreeViewItem const &That);
		CTreeViewItem const & operator = (TVITEMEX const &Item);

		// Attributes
		UINT	  GetMask(void) const;
		BOOL	  TestMask(UINT uMask) const;
		HTREEITEM GetHandle(void) const;
		UINT      GetState(void) const;
		UINT	  GetStateMask(void) const;
		PCTXT     GetText(void) const;
		UINT	  GetImage(void) const;
		UINT	  GetSelectedImage(void) const;
		UINT	  GetChildren(void) const;
		LPARAM	  GetParam(void) const;
		int	  GetHeight(void) const;

		// Operations
		void SetMask(UINT uMask);
		void SetHandle(HTREEITEM hItem);
		void SetState(UINT uState);
		void SetState(UINT uState, UINT uMask);
		void SetStateMask(UINT uMask);
		void SetStateImage(UINT uImage);
		void SetText(CString const &Text);
		void SetText(PCTXT pText);
		void SetTextBuffer(UINT uCount);
		void SetImage(UINT uImage);
		void SetSelectedImage(UINT uSelect);
		void SetImages(UINT uImage, UINT uSelect);
		void SetImages(UINT uImage);
		void SetChildren(UINT uChildren);
		void SetParam(LPARAM lParam);
		void SetHeight(int nHeight);

	protected:
		// Data Members
		CString m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tree View Control
//

class DLLAPI CTreeView : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTreeView(void);

		// Attributes
		CColor		GetBkColor(void) const;
		UINT		GetCount(void) const;
		CEditCtrl &	GetEditControl(void) const;
		CImageList &	GetImageList(void) const;
		int		GetIndent(void) const;
		CColor		GetInsertMarkColor(void) const;
		CString		GetSearchString(void) const;
		BOOL		GetItem(TVITEMEX &Item) const;
		CTreeViewItem	GetItem(HTREEITEM hItem) const;
		CString		GetItemText(HTREEITEM hItem) const;
		UINT		GetItemImage(HTREEITEM hItem) const;
		LPARAM		GetItemParam(HTREEITEM hItem) const;
		int		GetItemHeight(void) const;
		CRect		GetItemRect(HTREEITEM hItem, BOOL fRect) const;
		UINT		GetItemState(HTREEITEM hItem, UINT uMask) const;
		HTREEITEM	GetLastChild(HTREEITEM hItem) const;
		CColor		GetLineColor(void) const;
		HTREEITEM	GetNextItem(HTREEITEM hItem, UINT uCode) const;
		HTREEITEM	GetNextNode(HTREEITEM hItem, BOOL fSiblings) const;
		HTREEITEM	GetPrevNode(HTREEITEM hItem, BOOL fSiblings) const;
		UINT		GetScrollTime(void) const;
		CColor		GetTextColor(void) const;
		CToolTip &	GetToolTips(void) const;
		UINT		GetVisibleCount(void) const;
		HTREEITEM	HitTest(TV_HITTESTINFO &Info) const;
		HTREEITEM	HitTestItem(CPoint const &Pos) const;
		UINT		HitTestFlags(CPoint const &Pos) const;
		BOOL		IsExpanded(HTREEITEM hItem) const;
		BOOL		IsVisible(HTREEITEM hItem) const;

		// Notification Helper
		BOOL IsMouseInSelection(void) const;

		// Item Access Helpers
		HTREEITEM	GetSelection(void) const;
		HTREEITEM	GetChild(HTREEITEM hItem) const;
		HTREEITEM	GetDropHilite(void) const;
		HTREEITEM	GetFirstVisible(void) const;
		HTREEITEM	GetNext(HTREEITEM hItem) const;
		HTREEITEM	GetNextVisible(HTREEITEM hItem) const;
		HTREEITEM	GetParent(HTREEITEM hItem) const;
		HTREEITEM	GetPrevious(HTREEITEM hItem) const;
		HTREEITEM	GetPreviousVisible(HTREEITEM hItem) const;
		HTREEITEM	GetRoot(void) const;

		// Operations
		CImageList &	CreateDragImage(HTREEITEM hItem);
		BOOL		DeleteItem(HTREEITEM hItem);
		void		DeleteChildren(HTREEITEM hItem);
		BOOL		EditLabel(HTREEITEM hItem);
		BOOL		EndEditLabelNow(BOOL fCancel);
		BOOL		EnsureVisible(HTREEITEM hItem);
		BOOL		Expand(HTREEITEM hItem, UINT uAction);
		HTREEITEM	InsertItem(HTREEITEM hParent, HTREEITEM hAfter, CTreeViewItem const &Item);
		void		MoveSelection(void);
		BOOL		SelectItem(HTREEITEM hItem);
		BOOL		SelectItem(HTREEITEM hItem, UINT uAction);
		void		SetBkColor(CColor const &Color);
		void		SetImageList(UINT uList, HIMAGELIST hList);
		void		SetIndent(int nIndent);
		void		SetInsertMark(UINT uMark);
		void		SetInsertMarkColor(CColor const &Color);
		BOOL		SetItem(TVITEMEX const &Item);
		void		SetItemHeight(int nHeight);
		void		SetItemState(HTREEITEM hItem, UINT uState, UINT uMask);
		void		SetItemText(HTREEITEM hItem, CString Text);
		void		SetLineColor(CColor const &Color);
		void		SetScrollTime(UINT uTime);
		void		SetTextColor(CColor const &Color);
		void		SetToolTips(CToolTip &Tips);
		BOOL		SortChildren(HTREEITEM hItem);
		BOOL		SortChildren(HTREEITEM hItem, PFNTVCOMPARE pfnCompare, LPARAM lParam);

		// Handle Lookup
		static CTreeView & FromHandle(HWND hWnd);

	protected:
		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Multiple Select Tree View
//

class DLLAPI CMulTreeView : public CTreeView
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMulTreeView(void);

		// Attributes
		BOOL      GetMultiple(void) const;
		BOOL      HasMultiple(void) const;
		BOOL	  IsSelected(HTREEITEM hItem) const;
		HTREEITEM GetFirstSelect(void) const;
		HTREEITEM GetLastSelect(void) const;
		HTREEITEM GetNextSelect(HTREEITEM hItem) const;
		HTREEITEM GetPrevSelect(HTREEITEM hItem) const;
		BOOL	  InEditMode(void) const;

		// Operations
		void SetMultiple(BOOL fMulti);
		void SetDeferred(BOOL fDefer);
		void ClearSelection(void);
		void ClearExcept(void);
		void ClearExcept(HTREEITEM hSkip);
		BOOL SelectItem(HTREEITEM hItem);
		void SelectRange(HTREEITEM h1, HTREEITEM h2);
		BOOL SyncFirstSelection(void);
		void SetPickMode(BOOL fPick);

	protected:
		// Data Members
		UINT          m_uCount;
		BOOL          m_fMulti;
		BOOL          m_fDefer;
		DWORD         m_dwStyle;
		HTREEITEM     m_hAnchor;
		BOOL          m_fCapture;
		BOOL	      m_fDouble;
		BOOL	      m_fWaiting;
		BOOL	      m_fPick;
		CCursor       m_PickCursor;
		CCursor       m_MissCursor;
		TVHITTESTINFO m_Info;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnLButtonDown(UINT uCode, CPoint Pos);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);
		void OnMouseMove(UINT uCode, CPoint Pos);
		void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
		void OnLButtonUp(UINT uCode, CPoint Pos);
		void OnRButtonDown(UINT uCode, CPoint Pos);
		void OnRButtonDblClk(UINT uCode, CPoint Pos);
		void OnRButtonUp(UINT uCode, CPoint Pos);
		void OnKeyDown(UINT uCode, DWORD dwData);
		void OnKeyUp(UINT uCode, DWORD dwData);
		void OnChar(UINT uCode, DWORD dwData);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);

		// Implementation
		void SetCount(UINT uCount);
		void NotifyMultiple(BOOL fMulti);
		void ClearSelected(HTREEITEM hItem);
		void SetSelected(HTREEITEM hItem);
		BOOL StartDefer(CPoint Pos);
		BOOL KillDefer(void);
		BOOL OfferFocus(void);
		BOOL PickItem(HTREEITEM hItem);
		BOOL IsValidPick(HTREEITEM hItem);
		void PrepNotify(NMTREEVIEW &Notify, UINT uCode);
		BOOL SendNotify(NMTREEVIEW &Notify);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTabItem;
class CTabCtrl;

//////////////////////////////////////////////////////////////////////////
//
// Tab Control Item
//

class DLLAPI CTabItem : public TCITEM
{
	public:
		// Constructors
		CTabItem(void);
		CTabItem(CTabItem const &That);
		CTabItem(TCITEM const &Item);
		CTabItem(PCTXT pText, UINT uImage, LPARAM lParam);
		CTabItem(PCTXT pText, UINT uImage);
		CTabItem(PCTXT pText);
		CTabItem(UINT uMask);

		// Assignment Operators
		CTabItem const & operator = (CTabItem const &That);
		CTabItem const & operator = (TCITEM const &Item);

		// Attributes
		UINT	GetMask(void) const;
		BOOL	TestMask(UINT uMask) const;
		UINT    GetState(void) const;
		UINT	GetStateMask(void) const;
		PCTXT   GetText(void) const;
		UINT	GetImage(void) const;
		LPARAM	GetParam(void) const;

		// Operations
		void SetMask(UINT uMask);
		void SetState(UINT uState);
		void SetStateMask(UINT uMask);
		void SetText(CString const &Text);
		void SetText(PCTXT pText);
		void SetTextBuffer(UINT uCount);
		void SetImage(UINT uImage);
		void SetParam(LPARAM lParam);

	protected:
		// Data Members
		CString m_Text;
	};

//////////////////////////////////////////////////////////////////////////
//
// Tab Control
//

class DLLAPI CTabCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTabCtrl(void);

		// Attributes
		void	     AdjustRect(BOOL fGrow, CRect &Rect) const;
		UINT	     GetCurFocus(void) const;
		UINT	     GetCurSel(void) const;
		DWORD	     GetExtendedSyle(void) const;
		CImageList & GetImageList(void) const;
		BOOL	     GetItem(UINT uItem, TCITEM &Item) const;
		CTabItem     GetItem(UINT uItem) const;
		CString	     GetItemText(UINT uItem) const;
		INT	     GetItemImage(UINT uItem) const;
		DWORD	     GetItemState(UINT uItem) const;
		UINT	     GetItemCount(void) const;
		CRect	     GetItemRect(UINT uItem) const;
		UINT	     GetRowCount(void) const;
		CToolTip &   GetToolTips(void) const;

		// Operations
		void AddToolTip(UINT uItem, PCTXT pText);
		UINT AppendItem(TCITEM const &Item);
		void DeleteAllItems(void);
		void DeleteItem(UINT uItem);
		void DeselectAll(BOOL fExFocus);
		void HighlightItem(UINT uItem, BOOL fShow);
		UINT InsertItem(UINT uItem, TCITEM const &Item);
		void RemoveImage(UINT uImage);
		void SetCurFocus(UINT uItem);
		void SetCurSel(UINT uItem);
		void SetExtendedStyle(DWORD dwMask, DWORD dwStyle);
		void SetImageList(HIMAGELIST hList);
		void SetItem(UINT uItem, TCITEM const &Item);
		void SetItemText(UINT uItem, PCTXT pText);
		void SetItemImage(UINT uItem, UINT uImage);
		void SetItemState(UINT uItem, DWORD dwState);
		void SetItemSize(CSize const &Size);
		void SetMinTabWidth(int cx);
		void SetPadding(CSize const &Size);
		void SetToolTips(CToolTip &Tips);

		// Handle Lookup
		static CTabCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CAnimation;
class CProgressBar;
class CTrackBar;
class CSpinner;
class CMonthCtrl;
class CTimeDateCtrl;
class CIPAddrCtrl;

//////////////////////////////////////////////////////////////////////////
//
// Animation Control
//

class DLLAPI CAnimation : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CAnimation(void);

		// Operations
		BOOL Open(PCTXT pFilename);
		BOOL Close(void);
		BOOL Play(UINT uRepeat, UINT uFrom, UINT uTo);
		BOOL Play(UINT uRepeat);
		BOOL Stop(void);

		// Handle Lookup
		static CAnimation & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Progress Bar Control
//
		     
class DLLAPI CProgressBar : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgressBar(void);

		// Attributes
		UINT   GetPos(void) const;
		UINT   GetRange(BOOL fMin) const;
		CRange GetRange(void) const;
		UINT   GetRangeMin(void) const;
		UINT   GetRangeMax(void) const;

		// Operations
		void DeltaPos(int nDelta);
		void SetBarColor(CColor const &Color);
		void SetBkColor(CColor const &Color);
		void SetPos(UINT uPos);
		void SetRange(UINT uMin, UINT uMax);
		void SetRange(CRange const &Range);
		void SetStep(UINT uStep);
		void StepIt(void);

		// Handle Lookup
		static CProgressBar & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Track Bar Control
//
		     
class DLLAPI CTrackBar : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTrackBar(void);

		// Attributes
		CWnd & GetBuddy(BOOL fPos) const;
		CWnd & GetBuddyLeft(void) const;
		CWnd & GetBuddyRight(void) const;
		CRect  GetChannelRect(void) const;
		UINT   GetLineSize(void) const;
		UINT   GetNumTicks(void) const;
		UINT   GetPageSize(void) const;
		UINT   GetPos(void) const;
		UINT * GetTickPointer(void) const;
		UINT   GetRangeMax(void) const;
		UINT   GetRangeMin(void) const;
		CRange GetRange(void) const;
		UINT   GetSelEnd(void) const;
		UINT   GetSelStart(void) const;
		CRange GetSel(void) const;
		UINT   GetThumbLength(void) const;
		CRect  GetThumbRect(void) const;
		UINT   GetTick(UINT uIndex) const;
		int    GetTickPos(UINT uIndex) const;

		// Operations
		void ClearSel(BOOL fRedraw);
		void ClearTicks(BOOL fRedraw);
		void SetBuddy(BOOL fPos, HWND hWnd);
		void SetBuddyLeft(HWND hWnd);
		void SetBuddyRight(HWND hWnd);
		void SetLineSize(UINT uSize);
		void SetPageSize(UINT uSize);
		void SetPos(UINT uPos, BOOL fRedraw);
		void SetRange(UINT uMin, UINT uMax, BOOL fRedraw);
		void SetRange(CRange const &Range, BOOL fRedraw);
		void SetRangeMax(UINT uMax, BOOL fRedraw);
		void SetRangeMin(UINT uMin, BOOL fRedraw);
		void SetSel(UINT uMin, UINT uMax, BOOL fRedraw);
		void SetSel(CRange const &Range, BOOL fRedraw);
		void SetSelEnd(UINT uPos, BOOL fRedraw);
		void SetSelStart(UINT uPos, BOOL fRedraw);
		void SetThumbLength(UINT uSize, BOOL fRedraw);
		void SetTick(UINT uPos);
		void SetTickFreq(UINT uFreq);

		// Handle Lookup
		static CTrackBar & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Spinner Control
//

class DLLAPI CSpinner : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSpinner(void);

		// Attributes
		UINT   GetAccel(UINT uCount, UDACCEL *pAccel) const;
		UINT   GetAccel(CArray <UDACCEL> &Array) const;
		UINT   GetBase(void) const;
		CWnd & GetBuddy(void) const;
		INT    GetPos(void) const;
		CRange GetRange(void) const;
		INT    GetRangeMin(void) const;
		INT    GetRangeMax(void) const;

		// Operations
		BOOL SetAccel(UINT uCount, UDACCEL const *pAccel);
		BOOL SetAccel(CArray <UDACCEL> const &Array);
		UINT SetBase(UINT uBase);
		void SetBuddy(HWND hWnd);
		INT  SetPos(INT nPos);
		void SetRange(INT nMin, INT nMax);
		void SetRange(CRange const &Range);

		// Handle Lookup
		static CSpinner & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
	};

//////////////////////////////////////////////////////////////////////////
//
// Month Calendar Control
//
		     
class DLLAPI CMonthCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMonthCtrl(void);

		// Attributes
		CColor     GetColor(UINT uIndex) const;
		BOOL       GetCurSel(SYSTEMTIME &Time) const;
		SYSTEMTIME GetCurSel(void) const;
		UINT       GetFirstDayOfWeek(void) const;
		UINT       GetMaxSelCount(void) const;
		int        GetMaxTodayWidth(void) const;
		CRect      GetMinReqRect(void) const;
		int        GetMonthDelta(void) const;
		UINT       GetMonthRange(DWORD dwFlags, SYSTEMTIME *pTimeArray) const;
		DWORD      GetRange(SYSTEMTIME *pTimeArray) const;
		BOOL       GetSelRange(SYSTEMTIME *pTimeArray) const;
		BOOL       GetToday(SYSTEMTIME &Time) const;
		SYSTEMTIME GetToday(void) const;
		DWORD      HitTest(MCHITTESTINFO &Info) const;
		UINT       HitTestCode(CPoint const &Pos) const;
		UINT       HitTestTime(CPoint const &Pos, SYSTEMTIME &Time) const;
		SYSTEMTIME HitTestTime(CPoint const &Pos) const;

		// Operations
		CColor SetColor(UINT uIndex, CColor const &Color);
		BOOL   SetCurSel(SYSTEMTIME const &Time);
		BOOL   SetDayState(UINT uCount, MONTHDAYSTATE const *pState);
		BOOL   SetDayState(CArray <MONTHDAYSTATE> const &Array);
		DWORD  SetFirstDayOfWeek(UINT uDay);
		BOOL   SetMaxSelCount(UINT uCount);
		BOOL   SetMonthDelta(UINT uDelta);
		BOOL   SetRange(UINT uFlags, SYSTEMTIME const *pTimeArray);
		BOOL   SetSelRange(SYSTEMTIME const *pTimeArray);
		BOOL   SetToday(SYSTEMTIME const &Time);

		// Handle Lookup
		static CMonthCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Time Date Control
//
		     
class DLLAPI CTimeDateCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTimeDateCtrl(void);

		// Attributes
		CColor       GetMonthColor(UINT uIndex) const;
		CFont        GetMonthFont(void) const;
		CMonthCtrl & GetMonthCtrl(void) const;
		DWORD        GetRange(SYSTEMTIME *pTimeArray) const;
		BOOL         GetSystemTime(SYSTEMTIME &Time) const;
		SYSTEMTIME   GetSystemTime(void) const;

		// Operations
		BOOL   SetFormat(PCTXT pText);
		CColor SetMonthColor(UINT uIndex, CColor const &Color);
		void   SetMonthFont(CFont const &Font, BOOL fRedraw);
		BOOL   SetRange(UINT uFlags, SYSTEMTIME const *pTimeArray);
		BOOL   SetSystemTime(SYSTEMTIME const &Time);
		BOOL   ClearSystemTime(void);

		// Handle Lookup
		static CTimeDateCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// IP Address Control
//
		     
class DLLAPI CIPAddrCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIPAddrCtrl(void);

		// Attributes
		DWORD GetAddress(void) const;
		BOOL  IsBlank(void) const;

		// Operations
		void ClearAddress(void);
		void SetAddress(DWORD dwAddr);
		void SetRange(UINT uIndex, CRange const &Range);
		void SetFocus(UINT uIndex);
		void SetFocus(void);

		// Clipboard Support
		void Cut(void);
		BOOL Copy(void);
		void Paste(void);
		void Clear(void);

		// Handle Lookup
		static CIPAddrCtrl & FromHandle(HWND hWnd);

	protected:
		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Message Map
		AfxDeclareMessageMap();

		// Command Handlers
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);

		// Implementation
		BOOL CanPasteText(void) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CHotLink;

//////////////////////////////////////////////////////////////////////////
//
// Hot Link Control
//

class DLLAPI CHotLinkCtrl : public CCtrlWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHotLinkCtrl(void);

		// Destructor
		~CHotLinkCtrl(void);

	protected:
		// Static Data
		static UINT m_timerCheck;

		// Data Members
		BOOL m_fHover;
		BOOL m_fSpace;
		BOOL m_fPress;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnLButtonDown(UINT uCode, CPoint Pos);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);
		void OnLButtonUp(UINT uCode, CPoint Pos);
		void OnMouseMove(UINT uCode, CPoint Pos);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnEnable(BOOL fEnable);
		UINT OnGetDlgCode(MSG *pMsg);
		void OnKeyDown(UINT uCode, DWORD dwData);
		void OnKeyUp(UINT uCode, DWORD dwData);

		// Implementation
		CString GetLinkText(void);
		BOOL    InText(CPoint Pos);
		BOOL    InText(void);
	};

// End of File

#endif
