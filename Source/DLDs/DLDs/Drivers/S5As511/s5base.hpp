
//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Base Driver
//

#define QUIET		500

//////////////////////////////////////////////////////////////////////////
//
// Block Address Cache
//

struct CBlock
{
	WORD  m_fUsed : 1;
	WORD  m_wIdx  : 15;
	DWORD m_dwAddr;
	
	};

//////////////////////////////////////////////////////////////////////////
//
// Block Types
//

enum BlockType
{
	btDB = 1,
	btSB = 2,
	btPB = 4,
	btFB = 8,
	btDX = 12,
	btOB = 48,
	btFX = 76,

	};
		
//////////////////////////////////////////////////////////////////////////
//
// Link Symbols
//

enum Symbols
{
	symNul = 0x8000,
	symStx,		
	symEtx,
	symEot,
	symAck,
	symNak,
	symErr

	};

//////////////////////////////////////////////////////////////////////////
//
// Context
//

struct CBase 
{
	BOOL     m_fCache;
	BOOL     m_fBusy;
	WORD     m_wWidth;
	DWORD    m_dwAddrPII;
	DWORD    m_dwAddrPIQ;
	DWORD    m_dwAddrF;
	DWORD    m_dwAddrT;
	DWORD    m_dwAddrC;
	DWORD    m_dwAddrSys;
	CBlock * m_pAddrDB;
	CBlock * m_pAddrDX;
	UINT     m_uSizeDB;
	UINT     m_uSizeDX;
	WORD     m_wCPU1;
	WORD     m_wCPU2;
	BOOL     m_fAddr32;
	WORD     m_wPad;
	BOOL     m_fExtra;

	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 Base Driver
//

class CS5AS511Base : public CMasterDriver
{
	public:
		// Constructor
		CS5AS511Base(void);

		// Destructor
		~CS5AS511Base(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data
		CBase * m_pBase;
		BYTE    m_bTxBuf[2048];
		BYTE	m_bPack [512];
		BYTE	m_bRxBuf[2048];
		UINT	m_uTxPtr;
		UINT	m_uRxPtr;

		// Read/Write Handlers
		CCODE DoByteRead (DWORD dwAddr, PDWORD pData, UINT uCount, UINT uWidth);
		CCODE DoWordRead (DWORD dwAddr, PDWORD pData, UINT uCount, UINT uWidth);
		CCODE DoLongRead (DWORD dwAddr, PDWORD pData, UINT uCount, UINT uWidth);
		CCODE DoByteWrite(DWORD dwAddr, PDWORD pData, UINT uCount);
		CCODE DoWordWrite(DWORD dwAddr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(DWORD dwAddr, PDWORD pData, UINT uCount);
		
		// Address Cache
		BOOL IsCacheOk(UINT uType);
		void InitCache(UINT uCountDB, UINT uCountDX);
		BOOL FindCache(UINT uType, UINT uIndex, DWORD &dwAddr);
		void LoadCache(UINT uType);
		void LoadCache(UINT uType, UINT uWIndex, DWORD dwAddr);
		void FreeCache(UINT uType);

		// Implementation
		DWORD FindAddress(AREF Addr);
		UINT  GetWidth(UINT uSpace);
		BOOL  FindWidth(void);
		BOOL  Write(DWORD dwAddr, PBYTE pData, UINT uCount);
		BOOL  Read(DWORD dwAddr, UINT uCount);
		BOOL  ReadSystem(void);
		BOOL  ReadDir(UINT uType);
		BOOL  BlockInfo(UINT uType, UINT uBlock, DWORD &dwAddr, WORD &wLen);
		BOOL  Start(BYTE bFunc);
		BOOL  Continue(void);
		BOOL  Stop(void);
		BOOL  Terminate(void);
		void  SetBusy(void);
		void  WaitBusy(void);
		BOOL  Transact(BYTE bFunc, BOOL fWrite);

		// Transport
		void SendData(BYTE bData);
		void SendCtrl(BYTE bData);
		BOOL RecvCtrl(BYTE bData);
		BOOL Send(BYTE bData);
		BOOL Send(void);
		BOOL Recv(BYTE &bData);
		BOOL Recv(BOOL fTerm);
		UINT RecvSym(void);

		// Frame Building
		void StartFrame(void);
		void AddByte(BYTE bData);
		void AddCtrl(BYTE bData);
		void AddDataByte(BYTE bData);
		void AddDataWord(WORD wData);
		void AddDataLong(DWORD dwData);
		void AddDataBuff(PBYTE pData, UINT uLen);

		// Link Management
		virtual BOOL CheckLink(void);
		virtual void AbortLink(void);

		// Port Access
		virtual void ClearRx(void);
		virtual BOOL Send(PCBYTE pData, UINT uLen);
		virtual BOOL Recv(PBYTE  pData, UINT &uLen, BOOL fTerm);
		
		// Base Context
		void FreeBase(void);
		void InitBase(CBase *pCtx);
		void SetBase(CBase *pCtx);

		// Debug
		void Dump(PCBYTE pData, UINT uLen, BOOL fTx);
	};

// End of File
