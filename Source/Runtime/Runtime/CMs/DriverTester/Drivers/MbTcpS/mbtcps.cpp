
#include "local.hpp"

#include "mbtcps.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP Slave Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

// Instantiator

INSTANTIATE(CModbusTCPSlave);

// Constructor

CModbusTCPSlave::CModbusTCPSlave(void)
{
	m_Ident   = DRIVER_ID;

	m_uPort   = 502;

	m_uCount  = 1;

	m_fEnable = TRUE;
	}

// Destructor

CModbusTCPSlave::~CModbusTCPSlave(void)
{
	delete [] m_pSock;
	}

// Configuration

void MCALL CModbusTCPSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uPort     = GetWord(pData);

		m_uCount    = GetWord(pData);

		m_uRestrict = GetByte(pData);

		m_SecMask   = GetLong(pData);

		m_SecData   = GetLong(pData);
		
		m_fFlipLong = GetByte(pData);

		m_fFlipReal = GetByte(pData);

		m_uTimeout  = GetLong(pData);
		}

	m_pSock = New SOCKET [ m_uCount ];
	}

// Management

void MCALL CModbusTCPSlave::Attach(IPortObject *pPort)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &Sock = m_pSock[n];

		OpenSocket(n, Sock);
		}
	}

void MCALL CModbusTCPSlave::Detach(void)
{
	BOOL fHit = FALSE;

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			SOCKET &Sock = m_pSock[n];

			if( Sock.m_pSocket ) {

				if( !p ) {

					Sock.m_pSocket->Close();

					fHit = TRUE;
					}
				else {
					Sock.m_pSocket->Abort();

					Sock.m_pSocket->Release();
					
					Sock.m_pSocket = NULL;
					}
				}

			if( !p && !fHit ) {

				break;
				}

			Sleep(100);
			}
		}
	}

// Entry Point

void MCALL CModbusTCPSlave::Service(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &Sock = m_pSock[n];

		if( Sock.m_pSocket ) {

			UINT Phase;

			Sock.m_pSocket->GetPhase(Phase);

			switch( Phase ) {

				case PHASE_OPEN:

					if( !m_fEnable ) {

						CloseSocket(Sock);
						
						break;
						}

					if( !Sock.m_fBusy ) {

						if( !CheckAccess(Sock, FALSE) ) {

							Sock.m_pSocket->Close();

							break;
							}

						Sock.m_uTime = GetTickCount();

						Sock.m_fBusy = TRUE;
						}

					ReadData(Sock);

					break;

				case PHASE_CLOSING:

					Sock.m_pSocket->Close();

					break;

				case PHASE_ERROR:

					CloseSocket(Sock);

					OpenSocket(n, Sock);

					break;
				}
			}
		else {
			OpenSocket(n, Sock);
			}
		}

	ForceSleep(20);
	}

// User Access

UINT MCALL CModbusTCPSlave::DrvCtrl(UINT uFunc, PCTXT Value)
{
	switch( uFunc ) {

		case 2:
			return m_fEnable;

		case 3:
			m_fEnable = strcmp(Value, "0");

			break;
		}

	return 0;
	}

// Implementation

void CModbusTCPSlave::OpenSocket(UINT n, SOCKET &Sock)
{
	if( m_fEnable ) {

		Sock.m_pSocket = CreateSocket(IP_TCP);

		if( Sock.m_pSocket ) {

			Sock.m_pSocket->Listen(m_uPort);

			Sock.m_pData = New BYTE [ 300 ];

			Sock.m_uPtr  = 0;

			Sock.m_fBusy = FALSE;
			}
		}
	}

void CModbusTCPSlave::CloseSocket(SOCKET &Sock)
{
	if( !m_fEnable ) {

		Sock.m_pSocket->Close();
		}

	Sock.m_pSocket->Release();

	if( Sock.m_pData ) {

		delete Sock.m_pData;

		Sock.m_pData = NULL;
		}

	Sock.m_pSocket = NULL;
	}

void CModbusTCPSlave::ReadData(SOCKET &Sock)
{
	UINT uSize = 300 - Sock.m_uPtr;

	Sock.m_pSocket->Recv(Sock.m_pData + Sock.m_uPtr, uSize);

	if( uSize ) {

		Sock.m_uPtr += uSize;

		while( Sock.m_uPtr >= 8 ) {

			UINT uTotal = 6 + Sock.m_pData[5];

			if( Sock.m_uPtr >= uTotal ) {

				if( !DoFrame(Sock, Sock.m_pData, uTotal) ) {

					Sock.m_pSocket->Close();

					return;
					}

				if( Sock.m_uPtr -= uTotal ) {

					for( UINT n = 0; n < Sock.m_uPtr; n++ ) {

						Sock.m_pData[n] = Sock.m_pData[uTotal++];
						}

					continue;
					}
				}
			
			break;
			}

		Sock.m_uTime = GetTickCount();

		return;
		}

	if( UINT(GetTickCount() - Sock.m_uTime) >= ToTicks(m_uTimeout) ) {

		Sock.m_pSocket->Close();

		return;
		}
	}

BOOL CModbusTCPSlave::CheckAccess(SOCKET &Sock, BOOL fWrite)
{
	DWORD IP;

	if( Sock.m_pSocket->GetRemote((IPADDR &) IP) == S_OK ) {

		if( m_uRestrict == 0 ) {

			return TRUE;
			}

		if( m_uRestrict == 2 || fWrite == TRUE ) {

			if( (IP & m_SecMask) == (m_SecData & m_SecMask) ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CModbusTCPSlave::DoFrame(SOCKET &Sock, PBYTE pData, UINT uSize)
{
	switch( pData[7] ) {
		
		case 3:
		case 4:
			return DoStdRead (Sock, pData, uSize, pData[7]);

		case 6:
		case 16:
			return DoStdWrite(Sock, pData, uSize, pData[7]);

		case 1:
		case 2:
			return DoBitRead (Sock, pData, uSize, pData[7]);

		case 5:
		case 15:
			return DoBitWrite(Sock, pData, uSize, pData[7]);
		}

	return DoFuncXX(Sock, pData, uSize, ILLEGAL_FUNCTION);
	}

// Opcode Handlers

BOOL CModbusTCPSlave::DoStdRead(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode)
{
	UINT uAddr  = MotorToHost(PWORD(pData +  8)[0]);

	UINT uCount = MotorToHost(PWORD(pData + 10)[0]);

	if( uCount < 125 || uAddr + uCount < 0xFFFF ) {

		PDWORD pWork   = New DWORD [ uCount ];

		UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal, addrLongAsLong, addrLongAsReal };

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  = (bCode == 3) ? 1 : 2;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				if( IsLongReg(uType[t]) ) {

					Addr.a.m_Table = (bCode == 3) ? 5 : 6;
					}

				uTotal = uCount / uFact[t];

				if( !COMMS_SUCCESS(Read(Addr, pWork, uTotal)) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !COMMS_SUCCESS(Read(Addr, pWork, uTotal)) ) {

							continue;
							}
						}
					else
						continue;
					}
				 
				break;
				}
			}

		if( t < elements(uType) ) {

			UINT	 uBytes = IsLongReg(uType[t]) ? 4 : 2;

			UINT     uTail  = 6 + uBytes * uCount + 3;

			CBuffer *pBuff  = CreateBuffer(uTail, TRUE);

			if( pBuff ) {

				PBYTE pReply = pBuff->AddTail(uTail);

				*pReply++ = pData[0];
				*pReply++ = pData[1];
				*pReply++ = 0;
				*pReply++ = 0;
				*pReply++ = 0;
				*pReply++ = uBytes * uCount + 3;
				*pReply++ = pData[6];
				*pReply++ = bCode;
				*pReply++ = uBytes * uCount;

				BOOL fReal = IsReal(uType[t]);

				BOOL fFlip = fReal ? m_fFlipReal : m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( t == 0 ) {

						*pReply++ = HIBYTE(pWork[n]);
						*pReply++ = LOBYTE(pWork[n]);
						}
					else {
						if( fFlip ) {

							*pReply++ = HIBYTE(LOWORD(pWork[n]));
							*pReply++ = LOBYTE(LOWORD(pWork[n]));
							*pReply++ = HIBYTE(HIWORD(pWork[n]));
							*pReply++ = LOBYTE(HIWORD(pWork[n]));
							}
						else {
							*pReply++ = HIBYTE(HIWORD(pWork[n]));
							*pReply++ = LOBYTE(HIWORD(pWork[n]));
							*pReply++ = HIBYTE(LOWORD(pWork[n]));
							*pReply++ = LOBYTE(LOWORD(pWork[n]));
							}
						}
					}

				if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

					delete pWork;

					return TRUE;
					}

				delete pWork;

				BuffRelease(pBuff);

				return FALSE;
				}
			}

		delete pWork;
		}

	return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
	}

BOOL CModbusTCPSlave::DoStdWrite(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode)
{
	PU2   pWrite = NULL;

	UINT  uAddr  = 0;

	UINT  uCount = 0;

	if( !CheckAccess(Sock, TRUE) ) {

		return FALSE;
		}

	if( bCode == 16 ) {

		pWrite = PU2(pData + 13);

		uAddr  = MotorToHost(PWORD(pData +  8)[0]);

		uCount = MotorToHost(PWORD(pData + 10)[0]);
		}
	else {
		pWrite = PU2(pData + 10);

		uAddr  = MotorToHost(PWORD(pData +  8)[0]);

		uCount = 1;
		}

	if( uCount < 125 && uAddr + uCount < 0xFFFF ) {

		PDWORD pWork   = New DWORD [ uCount ];

		UINT   uType[] = { addrWordAsWord, addrWordAsLong, addrWordAsReal, addrLongAsLong, addrLongAsReal };

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		BOOL   fSign   = FALSE;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  =  IsLongReg(uType[t]) ? 5 : 1;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				uTotal = uCount / uFact[t];

				if( !COMMS_SUCCESS(Write(Addr, NULL, uTotal)) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !COMMS_SUCCESS(Write(Addr, NULL, uTotal)) ) {

							continue;
							}

						fSign = TRUE;
						}
					else
						continue;
					}

				BOOL fReal = IsReal(uType[t]);

				BOOL fFlip = fReal ? m_fFlipReal : m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( t == 0 ) {

						DWORD dwData = MotorToHost(pWrite[n]);

						if( fSign ) {

							Make16BitSigned(dwData);
							}
						
						pWork[n] = dwData;
						}
					else {
						WORD hi = MotorToHost(pWrite[2*n+0]);
							
						WORD lo = MotorToHost(pWrite[2*n+1]);

						if( fFlip )
							pWork[n] = MAKELONG(hi, lo);
						else
							pWork[n] = MAKELONG(lo, hi);
						}
					}

				Write(Addr, pWork, uTotal);

				break;
				}
			}

		if( t < elements(uType) ) {

			UINT     uTail = 6 + 2 + 4;
			
			CBuffer *pBuff = CreateBuffer(uTail, TRUE);

			if( pBuff ) {

				PBYTE pReply = pBuff->AddTail(uTail);

				*pReply++ = pData[0];
				*pReply++ = pData[1];
				*pReply++ = 0;
				*pReply++ = 0;
				*pReply++ = 0;
				*pReply++ = 6;
				*pReply++ = pData[6];
				*pReply++ = bCode;
				*pReply++ = pData[ 8];
				*pReply++ = pData[ 9];
				*pReply++ = pData[10];
				*pReply++ = pData[11];

				if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

					delete pWork;

					return TRUE;
					}

				delete pWork;

				BuffRelease(pBuff);

				return FALSE;
				}
			}

		delete pWork;
		}
	
	return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
	}

BOOL CModbusTCPSlave::DoBitRead(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode)
{
	UINT uAddr  = MotorToHost(PWORD(pData +  8)[0]);

	UINT uCount = MotorToHost(PWORD(pData + 10)[0]);

	if( uCount > 125 * 16 || uAddr + uCount > 0xFFFF ) {

		return FALSE;
		}

	UINT     uBytes = 1 + ( (uCount-1) / 8);

	UINT     uTail  = 6 + uBytes + 3;

	CBuffer *pBuff  = CreateBuffer(uTail, TRUE);

	if( pBuff ) {

		PBYTE pReply = pBuff->AddTail(uTail);

		*pReply++ = pData[0];
		*pReply++ = pData[1];
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = uBytes + 3;
		*pReply++ = pData[6];
		*pReply++ = bCode;

		*pReply++ = uBytes;

		PDWORD pWork = New DWORD [ uCount ];

		memset(pWork, 0, uCount * sizeof(DWORD));

		CAddress Addr;

		Addr.a.m_Table  = bCode + 2;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = uAddr + 1;
		Addr.a.m_Type   = addrBitAsBit;

		if( COMMS_SUCCESS(Read(Addr, pWork, uCount)) ) {

			if( uCount == 1 ) {

				*pReply++ = pWork[0] ? 0xFF : 0x00;
				}
			else {
				UINT uBitsDone  = 0;

				for( UINT uByte = 0; uByte < uBytes; uByte++ ) {

					UINT b = 0;

					BYTE m = 1;

					for( UINT n = 0; n < 8; n++ ) {

						if( uCount - uBitsDone ) {

							if( pWork[uBitsDone++] ) {

								b |= m;
								}

							m <<= 1;
							}
						}

					*pReply++ = b;
					}
				}
							
			delete pWork;

			if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

				return TRUE;
				}
			}
		else
			delete pWork;

		BuffRelease(pBuff);
		}

	return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
	}

BOOL CModbusTCPSlave::DoBitWrite(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode)
{
	UINT uAddr  = MotorToHost(PWORD(pData +  8)[0]);

	UINT uCount = bCode != 5 ? MotorToHost(PWORD(pData + 10)[0]) : 1;

	UINT uTail  = 6 + 2 + 4;

	if( !CheckAccess(Sock, TRUE) ) {

		return FALSE;
		}

	if( uCount > 125 * 16 || uAddr + uCount > 0xFFFF ) {

		return FALSE;
		}

	CBuffer *pBuff  = CreateBuffer(uTail, TRUE);

	if( pBuff ) {

		PBYTE pReply = pBuff->AddTail(uTail);

		*pReply++ = pData[0];
		*pReply++ = pData[1];
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 6;
		*pReply++ = pData[6];
		*pReply++ = bCode;
		*pReply++ = pData[ 8];
		*pReply++ = pData[ 9];
		*pReply++ = pData[10];
		*pReply++ = pData[11];

		PDWORD pWork = New DWORD [ uCount ];

		if( bCode != 5 ) {

			UINT b = 13;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				pWork[n] = (pData[b] & m) ? TRUE : FALSE;

				if( !(m <<= 1) ) {

					b = b + 1;

					m = 1;
					}
				}
			}
		else
			pWork[0] = pData[10] ? TRUE :FALSE;

		CAddress Addr;

		Addr.a.m_Table  = 3;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = 1 + uAddr;
		Addr.a.m_Type   = addrBitAsBit;

		Write(Addr, pWork, uCount);

		delete pWork;

		if( Sock.m_pSocket->Send(pBuff) == S_OK ) {
			
			return TRUE;
			}

		BuffRelease(pBuff);
		}
	
	return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
	}

BOOL CModbusTCPSlave::DoFuncXX(SOCKET &Sock, PBYTE pData, UINT uSize, BYTE bCode)
{
	UINT     uTail = 7 + 2;

	CBuffer *pBuff = CreateBuffer(uTail, TRUE);

	if( pBuff ) {

		PBYTE pReply = pBuff->AddTail(uTail);

		*pReply++ = pData[0];
		*pReply++ = pData[1];
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 3;
		*pReply++ = pData[6];
		*pReply++ = pData[7] | 0x80;
		*pReply++ = bCode;

		if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

			if( pData[7] >= 0x7D && pData[7] <= 0x7F ) {

				return TRUE;
				}

			if( pData[7] >= 0x01 && pData[7] <= 0x28) {

				return TRUE;
				}

			return FALSE;
			}

		BuffRelease(pBuff);
		}

	return FALSE;
	}

// Helpers

void CModbusTCPSlave::Make16BitSigned(DWORD &dwData)
{
	if( dwData & 0x8000 ) {

		dwData |= 0xFFFF0000;
		}
	}

BOOL CModbusTCPSlave::IsLongReg(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CModbusTCPSlave::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

// End of File
