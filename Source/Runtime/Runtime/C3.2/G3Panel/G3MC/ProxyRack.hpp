
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ProxyRack_HPP

#define	INCLUDE_ProxyRack_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../g3comms/proxy.hpp"

#include "../g3comms/queue.hpp"

#include "RackInterface.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CModule;

//////////////////////////////////////////////////////////////////////////
//
// Rack Proxy Object
//

class CProxyRack : public CProxy
{
public:
	// Constructor
	CProxyRack(void);

	// Destructor
	~CProxyRack(void);

	// Attributes
	BOOL HasError(void);

	// Entry Points
	void Init(UINT uTask);
	void Task(UINT uTask);
	void Term(UINT uTask);

	// Suspension
	void Suspend(void);
	void Resume(void);

	// Firmware
	BOOL   GetFirmGUID(BYTE FirmID, PBYTE pData);
	PCBYTE GetFirmData(BYTE FirmID);
	DWORD  GetFirmSize(BYTE FirmID);

	// Shared API
	BOOL RxResponse(void);
	BOOL RxGeneral(void);

	// Boot Loader API
	BOOL BootTxForceReset(void);
	BOOL BootTxCheckModel(void);
	BOOL BootTxCheckVersion(PBYTE pGuid);
	BOOL BootTxProgramReset(void);
	BOOL BootTxClearProgram(DWORD dwSize);
	BOOL BootTxProgramSize(DWORD dwSize);
	BOOL BootTxWriteProgram(WORD wAddr, PBYTE pData, WORD wCount);
	BOOL BootTxWriteProgram32(DWORD dwAddr, PBYTE pData, WORD wCount);
	BOOL BootTxWriteVerify(DWORD Crc32);
	BOOL BootTxWriteVersion(PBYTE pGuid);
	BOOL BootTxStartProgram(void);
	BOOL BootRxCheckModel(BOOL &fSame, BYTE ModelID);
	BOOL BootRxWriteVerify(BOOL &fSame);
	BOOL BootRxCheckVersion(BOOL &fSame);

	// Configuration API
	BOOL ConfigTxCheckVersion(PBYTE pGuid);
	BOOL ConfigTxClearConfig(void);
	BOOL ConfigTxWriteConfig(PCBYTE &pData);
	BOOL ConfigTxWriteVersion(PBYTE pGuid);
	BOOL ConfigTxStartSystem(void);
	BOOL ConfigTxCheckStatus(void);
	BOOL ConfigRxCheckVersion(BOOL &fSame);
	BOOL ConfigRxWriteConfig(void);
	BOOL ConfigRxCheckStatus(BOOL &fRun);

	// Data Transfer API
	void DataTxStartFrame(void);
	BOOL DataTxWrite(WORD PropID, DWORD Data);
	BOOL DataTxRead(WORD PropID);
	BOOL DataTxSend(void);
	BOOL DataRxData(PBYTE &pData);
	BOOL DataTooFull(WORD PropID);

	// Tunneling
	BOOL Tunnel(BYTE bDrop, PBYTE pData);

	// Tunneling API
	BOOL TunnelTx(PBYTE pData);
	BOOL TunnelRx(PBYTE pData);

protected:
	// Static Data
	static CProxyRack * m_pThis;

	// Comms States
	enum
	{
		stateInit,
		stateIdle,
		stateReply,
		stateError,
	};

	// Rack Handler
	IRackHandler * m_pRack;
	IRackDriver  * m_pDriver;

	// Firmware
	UINT   m_uFirmCount;
	PCBYTE m_pFirmData;

	// Master Device
	CCommsDevice * m_pMaster;
	UINT           m_uUpdate;

	// Module List
	CModule ** m_ppModule;
	UINT       m_uTotal;
	UINT	   m_uCount;

	// Data Members
	UINT      m_uState[32];
	UINT      m_uTicks[32];
	UINT	  m_uDog;
	UINT      m_uDiv;
	UINT      m_uError;
	UINT      m_uRetry;
	UINT      m_uSlow;
	UINT	  m_uMask;
	BYTE      m_bDrop;
	CModule * m_pDrop;
	UINT      m_uRecycle;
	BOOL      m_fRecycle;

	// Implementation
	void MakeModules(void);
	void InitComms(void);
	void MainLoop(void);
	void UpdateMaster(void);
	BOOL UseDrop(UINT uIndex);
	BOOL CheckCount(BOOL fWait);
	void StateInit(void);
	void StateIdle(void);
	void StateReply(void);
	void StateError(void);
	BOOL PingModule(void);
	void ModuleError(UINT uCode, PCTXT pText);
	UINT ModelLookup(BYTE bModel);

	// Port Access
	BOOL OpenPort(void);
	void ClosePort(void);

	// Transport
	UINT SendEnq(void);
	BOOL Request(UINT uMsg, BOOL fSlow);
	BOOL Response(UINT uMsg);
	void Recycle(void);

	// Debug Function
	inline void Debug(PCTXT pForm, ...);

	// Friends
	friend BOOL RackHasError(void);
	friend BOOL RackTunnel(BYTE bDrop, PBYTE pData);
};

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IRackDriver  * Create_GraphiteDriver(IRackHandler *pHandler);
extern IRackDriver  * Create_ManticoreDriver(IRackHandler *pHandler);
extern IRackHandler * Create_GraphiteHandler(UINT uCount);
extern IRackHandler * Create_ManticoreHandler(void);

// End of File

#endif

