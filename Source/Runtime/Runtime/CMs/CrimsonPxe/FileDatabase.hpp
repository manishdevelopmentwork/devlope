
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_FileDatabase_HPP

#define	INCLUDE_FileDatabase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

struct CItemInfo;

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Database
//

class CFileDatabase : public IDatabase
{
public:
	// Constructor
	CFileDatabase(CString const &Root, UINT uBase, UINT uPool);

	// Destructor
	~CFileDatabase(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDatabase
	void   METHOD Init(void);
	void   METHOD Clear(void);
	BOOL   METHOD IsValid(void);
	void   METHOD SetValid(BOOL fValid);
	void   METHOD SetRunning(BOOL fRun);
	BOOL   METHOD GarbageCollect(void);
	BOOL   METHOD GetVersion(PBYTE pGuid);
	DWORD  METHOD GetRevision(void);
	BOOL   METHOD SetVersion(PCBYTE pData);
	BOOL   METHOD SetRevision(DWORD dwRes);
	BOOL   METHOD CheckSpace(UINT uSize);
	BOOL   METHOD WriteItem(CItemInfo const &Info, PCVOID pData);
	BOOL   METHOD GetItemInfo(UINT uItem, CItemInfo &Info);
	PCVOID METHOD LockItem(UINT uItem, CItemInfo &Info);
	PCVOID METHOD LockItem(UINT uItem);
	void   METHOD PendItem(UINT uItem, BOOL fPend);
	void   METHOD LockPendingItems(BOOL fLock);
	void   METHOD FreeItem(UINT uItem);

protected:
	// Constants
	static UINT  const dataItemCount = 4096;
	static DWORD const magicItem     = 0x4D455449;
	static DWORD const magicFRAM     = 0x12190846;
	static DWORD const clearFRAM     = 0x22190846;

	// Item Headers
	struct CFileHead
	{
		DWORD		m_Class;
		DWORD		m_CRC;
		DWORD		m_Size;
		DWORD		m_Comp;
		DWORD		m_Gen;
	};


	// Item State
	struct CState
	{
		CFileHead	m_Head;
		BOOL		m_fValid;
		UINT		m_uLock;
		PBYTE		m_pData;
		CState *	m_pNext;
		CState *	m_pPrev;
	};

	// Data
	ULONG		m_uRefs;
	UINT		m_uBase;
	CString		m_Root;
	IMutex	      * m_pMutex;
	UINT		m_uFreePages;
	UINT		m_uPoolSize;
	UINT		m_uPoolUsed;
	CState	      * m_pState;
	CState	      * m_pHead;
	CState	      * m_pTail;
	BOOL		m_fProps;
	GUID		m_Guid;
	DWORD		m_Revision;
	BOOL		m_fDirty;

	// Implementation
	bool    MountItems(void);
	bool    FindItems(void);
	void    EraseAll(void);
	void    CheckPoolUsage(void);
	CString MakeFile(CString const &Special);
	CString MakeFile(UINT uItem);

	// Implementation
	BOOL  IsClear(void);
	void  SetClear(void);
	DWORD GetValid(void);
	void  PutValid(DWORD dwData);
	void  Commit(void);
};

// End of File

#endif
