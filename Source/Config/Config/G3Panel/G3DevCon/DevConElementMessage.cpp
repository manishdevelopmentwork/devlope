
#include "Intern.hpp"

#include "DevConElementMessage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Message UI Element
//

// Base Class

#define CBaseClass CDevConElement

// Runtime Class

AfxImplementRuntimeClass(CDevConElementMessage, CBaseClass);

// Constructor

CDevConElementMessage::CDevConElementMessage(CString const &Message) : m_Message(Message)
{
	m_pEditLayout = NULL;

	m_pMainLayout = NULL;

	m_pTextCtrl   = New CStatic;
}

// Destructor

CDevConElementMessage::~CDevConElementMessage(void)
{
}

// Operations

void CDevConElementMessage::AddLayout(CLayFormation *pForm)
{
	CRect Rect(24, 4, 16, 8);

	if( m_Message[0] == '!' ) {

		Rect.top += 8;
	}

	m_pEditLayout = New CLayItemText(m_Message);

	m_pMainLayout = New CLayFormPad(m_pEditLayout, Rect, horzNone | vertNone | horzGrow);

	pForm->AddItem(m_pMainLayout);
}

void CDevConElementMessage::CreateControls(CWnd &Wnd, UINT &id)
{
	CRect   Rect = m_pEditLayout->GetRect();

	CString Text = m_Message;

	if( Text[0] == '!' ) {

		Text.Delete(0, 1);
	}

	m_pTextCtrl->Create(Text,
			    WS_CHILD | SS_LEFT,
			    Rect,
			    Wnd,
			    id++
	);

	m_pTextCtrl->SetFont(afxFont(Italic));

	AddControl(m_pTextCtrl);
}

// End of File
