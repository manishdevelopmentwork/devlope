
#include "Intern.hpp"

#include "UsbFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Framework
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Function Driver
//

// Instantiator

IUsbDriver * Create_UsbFuncDriver(UINT uPriority)
{
	CUsbFuncDriver *p = New CUsbFuncDriver(uPriority);

	return p;
	}

// Constructor

CUsbFuncDriver::CUsbFuncDriver(UINT uPriority)
{
	StdSetRef();

	m_pLowerDrv   = NULL;

	m_pUpperDrv   = NULL;

	m_pConfigData = NULL;

	m_pFlag       = Create_AutoEvent();
	
	m_pLimit      = Create_Semaphore();

	m_pLock       = Create_Mutex();

	m_pThread     = CreateThread(TaskUsbFuncDriver, uPriority, this, 0);

	m_pLimit->Signal(elements(m_Urbs));

	MakeUrb();
	}

// Destructor

CUsbFuncDriver::~CUsbFuncDriver(void)
{
	AfxRelease(m_pLowerDrv);

	AfxRelease(m_pUpperDrv);

	m_pThread->Destroy();

	FreeDescriptorData();

	m_pFlag->Release();

	m_pLimit->Release();

	m_pLock->Release();

	KillUrb();
	}

// IUnknown

HRESULT CUsbFuncDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IUsbFuncDriver);

	StdQueryInterface(IUsbFuncDriver);

	StdQueryInterface(IUsbDriver);

	StdQueryInterface(IUsbFuncHardwareEvents);

	StdQueryInterface(IUsbEvents);

	return E_NOINTERFACE;
	}

ULONG CUsbFuncDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CUsbFuncDriver::Release(void)
{
	StdRelease();
	}

// IUsbDriver 

BOOL CUsbFuncDriver::Bind(IUsbEvents *pDriver)
{
	if( pDriver ) {

		AfxRelease(m_pUpperDrv);

		m_pUpperDrv = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind(this);
		
			return true;
			}
		}
	
	return false;
	}

BOOL CUsbFuncDriver::Bind(IUsbDriver *pDriver)
{
	if( pDriver ) {
		
		AfxRelease(m_pUpperDrv);

		m_pUpperDrv = NULL;

		pDriver->QueryInterface(AfxAeonIID(IUsbFuncEvents), (void **) &m_pUpperDrv);

		if( m_pUpperDrv ) {

			m_pUpperDrv->OnBind(this);
		
			return true;
			}
		}
	
	return false;
	}

BOOL CUsbFuncDriver::Init(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {

		m_uStateNow = devDetached;

		m_uStateWas = devDetached;

		InitUrb();

		BuildDescriptors();

		return m_pLowerDrv->Init();
		}

	return false;
	}

BOOL CUsbFuncDriver::Start(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {

		return m_pLowerDrv->Start();
		}

	return false;
	}

BOOL CUsbFuncDriver::Stop(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {

		return m_pLowerDrv->Stop();
		}

	return false;
	}

// IUsbFunctionDriver

UINT CUsbFuncDriver::GetState(void) 
{
	return m_uStateNow;
	}

UINT CUsbFuncDriver::GetMaxPacket(UINT iEndpt)
{
	return m_pLowerDrv->GetHiSpeedActual() ? 512 : 64;
	}

BOOL CUsbFuncDriver::SendCtrlStatus(UINT iEndpt)
{
	return SendCtrl(iEndpt, NULL, 0);
	}

BOOL CUsbFuncDriver::RecvCtrlStatus(UINT iEndpt)
{
	return RecvCtrl(iEndpt, NULL, 0);
	}

BOOL CUsbFuncDriver::SendCtrl(UINT iEndpt, PCBYTE pData, UINT uLen)
{
	UsbIor r;

	r.m_iEndpt  = iEndpt;
	r.m_uFlags  = r.flagIn;
	r.m_pData   = PBYTE(pData);
	r.m_uCount  = uLen;
	r.m_uStatus = r.statActive;
		
	return m_pLowerDrv->Transfer(r);
	}

BOOL CUsbFuncDriver::RecvCtrl(UINT iEndpt, PBYTE pData, UINT uLen)
{
	if( pData == NULL ) {

		UsbIor r;

		r.m_iEndpt  = iEndpt;
		r.m_uFlags  = 0;
		r.m_pData   = NULL;
		r.m_uCount  = 0;
		r.m_uStatus = r.statActive;
		
		return m_pLowerDrv->Transfer(r);
		}

	return Transfer(iEndpt, pData, uLen, false, FOREVER) == uLen;
	}

UINT CUsbFuncDriver::SendBulk(UINT iEndpt, PCBYTE pData, UINT uLen, UINT uTimeout)
{
	return Transfer(iEndpt, PBYTE(pData), uLen, true, uTimeout);	
	}

UINT CUsbFuncDriver::RecvBulk(UINT iEndpt, PBYTE pData, UINT uLen, UINT uTimeout)
{
	return Transfer(iEndpt, pData, uLen, false, uTimeout);	
	}

BOOL CUsbFuncDriver::SetStall(UINT iEndpt, BOOL fStall)
{
	return m_pLowerDrv->SetStall(iEndpt, fStall);
	}

BOOL CUsbFuncDriver::GetStall(UINT iEndpt)
{
	return m_pLowerDrv->GetStall(iEndpt);
	}

// IUsbEvents

void CUsbFuncDriver::OnBind(IUsbDriver *pDriver)
{
	AfxRelease(m_pLowerDrv);

	m_pLowerDrv = NULL;

	pDriver->QueryInterface(AfxAeonIID(IUsbFuncHardwareDriver), (void **) &m_pLowerDrv);
	}

void CUsbFuncDriver::OnInit(void)
{
	if( m_pUpperDrv ) {
		
		m_pUpperDrv->OnInit();
		}
	}

void CUsbFuncDriver::OnStart(void)
{
	if( m_pUpperDrv ) {

		m_pUpperDrv->OnStart();
		}
	}

void CUsbFuncDriver::OnStop(void)
{
	if( m_pUpperDrv ) {
		
		SetState(devDetached);

		m_pUpperDrv->OnStop();
		}
	}

// IUsbFuncHardwareEvents

void CUsbFuncDriver::OnConnect(void)
{
	SetState(devAttached);
	}

void CUsbFuncDriver::OnDisconnect(void)
{
	SetState(devDetached);
	}

void CUsbFuncDriver::OnReset(void)
{
	SetState(devDefault);
	}

void CUsbFuncDriver::OnSuspend(void)
{
	SetState(devSuspended);
	}

void CUsbFuncDriver::OnResume(void)
{
	SetState(m_uStateWas);
	}

BOOL CUsbFuncDriver::OnSetup(PCBYTE m_pSetup)
{
	m_Req = (CUsbDeviceReq &) m_pSetup[0];

	m_pFlag->Set();

	return true;
	}

void CUsbFuncDriver::OnTransfer(UsbIor &Urb)
{
	if( Urb.m_pDone ) {

		Urb.m_pDone->Set();
		}
	}

// Task Entry

void CUsbFuncDriver::Service(void)
{
	for(;;) {

		m_pFlag->Wait(FOREVER);

		m_Req.UsbToHost();

		m_Req.Debug();

		BOOL fHandle = false;

		switch( m_Req.m_Type ) {

			case reqStandard:

				fHandle = OnStandard();

				break;

			case reqClass:

				fHandle = OnClass();

				break;

			case reqVendor:

				fHandle = OnVendor();

				break;

			case reqOther:

				fHandle = OnOther();

				break;
			}

		if( !fHandle ) {

			m_pLowerDrv->SetStall(0, true);
			}
		}
	}

// Transfer

UINT CUsbFuncDriver::Transfer(UINT iEndpt, PBYTE pData, UINT uLen, BOOL fIn, UINT uTimeout)
{
	UINT i = AllocUrb();

	if( i < NOTHING ) {

		UsbIor &r = m_Urbs[i];

		r.m_iEndpt = iEndpt;
		
		r.m_pData  = PBYTE(pData);
				
		r.m_uCount = uLen;
		
		r.m_uFlags = r.flagNotify | (fIn ? r.flagIn : 0);

		r.m_pDone->Clear();

		if( m_pLowerDrv->Transfer(r) ) {

			if( r.m_pDone->Wait(uTimeout) ) {

				if( r.m_uStatus == r.statPassed ) {

					uLen = r.m_uCount;
				
					FreeUrb(i);
			
					return uLen;
					}

				FreeUrb(i);

				return NOTHING;
				}

			m_pLowerDrv->Abort(r);
			}

		FreeUrb(i);
		}

	return NOTHING;
	}

// Framework Event Handlers

BOOL CUsbFuncDriver::OnStandard(void)
{
	switch( m_Req.m_bRequest ) {

		case reqGetStatus:
			
			return OnGetStatus();
		
		case reqClearFeature:

			return OnClearFeature();

		case reqSetAddress:

			return OnSetAddress();

		case reqGetDesc:

			return OnGetDescriptor();

		case reqGetConfig:
			
			return OnGetConfig();

		case reqSetConfig:

			return OnSetConfig();

		case reqSetDesc:
		case reqSetFeature:
		case reqGetInterface:
		case reqSetInterface:
		case reqSynchFrame:

			return false;
		}

	return false;
	}

BOOL CUsbFuncDriver::OnClass(void)
{
	return m_pUpperDrv->OnSetupClass(m_Req);
	}

BOOL CUsbFuncDriver::OnVendor(void)
{
	return m_pUpperDrv->OnSetupVendor(m_Req);
	}

BOOL CUsbFuncDriver::OnOther(void)
{
	return m_pUpperDrv->OnSetupOther(m_Req);
	}

BOOL CUsbFuncDriver::OnGetStatus(void)
{
	WORD wData = 0;

	switch( m_Req.m_Recipient ) {

		case recDevice:

			wData = 1;
			
			break;

		case recInterface:

			wData = 0;

			break;
		
		case recEndpoint:

			wData = WORD(m_pLowerDrv->GetStall(m_Req.m_wIndex));

			break;
		
		default:
			return false;
		}

	if( SendCtrl(0, PBYTE(&wData), sizeof(wData)) ) {
		
		SendCtrlStatus(0);
		}

	return true;
	}

BOOL CUsbFuncDriver::OnClearFeature(void)
{
	if( m_Req.m_wValue == selEndpointHalt ) {

		UINT iEndpt = m_Req.m_wIndex & 0x7F;
		
		m_pLowerDrv->ResetDataToggle(iEndpt);
		
		m_pLowerDrv->SetStall(iEndpt, false);

		SendCtrlStatus(0);

		return true;
		}

	return false;
	}

BOOL CUsbFuncDriver::OnSetAddress(void)
{
	if( m_pLowerDrv->SetAddress(m_Req.m_wValue) ) {

		SendCtrlStatus(0);

		SetState(devAddress);

		return true;
		}

	return false;
	}

BOOL CUsbFuncDriver::OnGetConfig(void)
{
	BYTE bData = m_uStateNow == devConfigured ? 1 : 0;
	
	if( SendCtrl(0, &bData, sizeof(bData)) ) {
		
		RecvCtrlStatus(0);
		}

	return true;
	}

BOOL CUsbFuncDriver::OnSetConfig(void)
{
	if( m_Req.m_wValue ) {

		for( UINT i = 0; i < m_uEndpointCount; i ++ ) {
		
			CUsbEndpointDesc &e = m_Endpoints[i];

			if( e.m_bTransfer == eptBulk ) {

				m_pLowerDrv->InitBulk(e.m_bAddr, e.m_bDirIn, e.m_wMaxPacket);
				}
			}

		SendCtrlStatus(0);

		m_pLowerDrv->SetConfig(m_Req.m_wValue);

		SetState(devConfigured);
		}
	else {
		SetState(devAddress);
		
		for( UINT i = 0; i < m_uEndpointCount; i ++ ) {
		
			CUsbEndpointDesc &e = m_Endpoints[i];

			if( e.m_bTransfer == eptBulk ) {

				m_pLowerDrv->KillBulk(e.m_bAddr);
				}
			}

		m_pLowerDrv->SetConfig(0);

		SendCtrlStatus(0);
		}

	return true;
	}

BOOL CUsbFuncDriver::OnGetDescriptor(void)
{
	if( m_Req.m_Direction == dirDevToHost ) {

		BYTE bType  = HIBYTE(m_Req.m_wValue);

		BYTE bIndex = LOBYTE(m_Req.m_wValue);

		switch( bType ) {

			case descDevice:
				
				return OnGetDeviceDesc();

			case descDeviceQual:

				return OnGetDeviceQual();
			
			case descConfig:

				return OnGetConfigDesc(bIndex);

			case descString:

				return OnGetStringDesc(bIndex);
			}
		}
	
	return false;
	}

BOOL CUsbFuncDriver::OnGetDeviceDesc(void)
{
	PCBYTE pData = PBYTE(&m_Device);

	UINT   uSize = sizeof(m_Device);

	MakeMin(uSize, m_Req.m_wLength);

	CheckDescriptors();

	m_Device.HostToUsb();

	if( SendCtrl(0, pData, uSize) ) {
		
		RecvCtrlStatus(0);
		}

	m_Device.UsbToHost();

	return true;
	}

BOOL CUsbFuncDriver::OnGetDeviceQual(void)
{
	if( m_pLowerDrv->GetHiSpeedCapable() ) {

		m_Qualifier.m_bMaxPacket = BYTE(m_pLowerDrv->GetHiSpeedActual() ? 64 : 512);

		PCBYTE pData = PBYTE(&m_Qualifier);

		UINT   uSize = sizeof(m_Qualifier);

		MakeMin(uSize, m_Req.m_wLength);

		m_Qualifier.HostToUsb();

		if( SendCtrl(0, pData, uSize) ) {
			
			RecvCtrlStatus(0);
			}

		m_Qualifier.UsbToHost();

		return true;
		}

	return false;
	}

BOOL CUsbFuncDriver::OnGetConfigDesc(UINT iIndex)
{
	MakeDescriptorData();

	UINT   uSize = m_Config.m_wTotal;

	PCBYTE pData = m_pConfigData;

	MakeMin(uSize, m_Req.m_wLength);

	if( SendCtrl(0, pData, uSize) ) {
		
		RecvCtrlStatus(0);
		}

	return true;
	}

BOOL CUsbFuncDriver::OnGetStringDesc(UINT iIndex)
{
	UsbDesc *pDesc;

	if( m_pUpperDrv->OnGetString(pDesc, iIndex) ) {

		CUsbStringDesc &s = *(CUsbStringDesc *) pDesc;

		UINT   uSize = s.m_bLength;

		PCBYTE pData = PBYTE(&s);
		
		MakeMin(uSize, m_Req.m_wLength);

		s.HostToUsb();

		if( SendCtrl(0, pData, uSize) ) {
			
			RecvCtrlStatus(0);
			}

		s.UsbToHost();

		return true;
		}

	return false;
	}

// Descriptors

BOOL CUsbFuncDriver::BuildDescriptors(void)
{
	BuildDeviceDescriptor();

	BuildQualifierDescriptor();

	BuildConfigDescriptor();

	BuildInterfaceDescriptors();

	BuildEndpointDescriptors();

	return true;
	}

BOOL CUsbFuncDriver::BuildDeviceDescriptor(void)
{
	m_Device.Init();

	m_Device.m_bClass	= devReserved;
	
	m_Device.m_bSubClass	= devReserved;
	
	m_Device.m_bProtocol	= devReserved;
	
	m_Device.m_bMaxPacket	= 64;
	
	m_Device.m_bConfigCount	= 1;

	return m_pUpperDrv->OnGetDevice(m_Device);
	}

BOOL CUsbFuncDriver::BuildQualifierDescriptor(void)
{
	m_Qualifier.InitFrom(m_Device);	

	m_Qualifier.m_bConfigCount = 1;

	return m_pUpperDrv->OnGetQualifier(m_Qualifier);
	}

BOOL CUsbFuncDriver::BuildConfigDescriptor(void)
{
	m_Config.Init();	
	
	m_Config.m_bInterfaces	= 1;
	
	m_Config.m_bConfig	= 1;
	
	m_Config.m_bSelfPowered	= true;

	return m_pUpperDrv->OnGetConfig(m_Config);
	}

BOOL CUsbFuncDriver::BuildInterfaceDescriptors(void)
{
	MakeMin(m_Config.m_bInterfaces, elements(m_Interfaces));
	
	for( UINT i = 0; i < m_Config.m_bInterfaces; i ++ ) {

		CUsbInterfaceDesc &Desc = m_Interfaces[i];

		Desc.Init();

		Desc.m_bThis = BYTE(i);

		m_Config.m_wTotal += sizeof(UsbInterfaceDesc);

		m_pUpperDrv->OnGetInterface(Desc);
		}

	return true;
	}

BOOL CUsbFuncDriver::BuildEndpointDescriptors(void)
{
	m_uEndpointCount = 0;

	for( UINT i = 0; i < elements(m_Endpoints); i ++ ) {

		CUsbEndpointDesc &Desc = m_Endpoints[i];

		Desc.Init();

		Desc.m_bAddr	  = i + 1;

		Desc.m_bInterval  = 1;

		Desc.m_wMaxPacket = m_pLowerDrv->GetHiSpeedCapable() ? 512 : 64;

		if( m_pUpperDrv->OnGetEndpoint(Desc) ) {

			m_Config.m_wTotal += sizeof(UsbEndpointDesc);

			m_uEndpointCount  ++;

			continue;
			}
		
		break;
		}

	return true;
	}

void CUsbFuncDriver::MakeDescriptorData(void)
{
	if( !m_pConfigData ) {

		m_pConfigData = New BYTE [ m_Config.m_wTotal ];

		UINT uCount   = 0;

		UINT uEndpt   = 0;

		m_Config.HostToUsb();
		
		memcpy(m_pConfigData, &m_Config, sizeof(m_Config));

		m_Config.UsbToHost();

		uCount += sizeof(m_Config);

		for( UINT i = 0; i < m_Config.m_bInterfaces; i ++ ) {

			CUsbInterfaceDesc &id = m_Interfaces[i];

			id.HostToUsb();

			memcpy(m_pConfigData + uCount, &id, sizeof(id));

			id.UsbToHost();

			uCount += sizeof(id);

			for( UINT e = 0; e < id.m_bEndpoints; e++ ) {

				CUsbEndpointDesc &ed = m_Endpoints[uEndpt++];
				
				ed.HostToUsb();
				
				memcpy(m_pConfigData + uCount, &ed, sizeof(ed));

				ed.UsbToHost();

				uCount += sizeof(ed);
				}
			}
		}
	}

void CUsbFuncDriver::FreeDescriptorData(void)
{
	if( m_pConfigData ) {

		free(m_pConfigData);

		m_pConfigData = NULL;
		}
	}

void CUsbFuncDriver::CheckDescriptors(void)
{
	for( UINT i = 0; i < elements(m_Endpoints); i ++ ) {

		CUsbEndpointDesc &Desc = m_Endpoints[i];

		Desc.m_wMaxPacket = m_pLowerDrv->GetHiSpeedActual() ? 512 : 64;
		}
	}

// IO Request Blocks

void CUsbFuncDriver::MakeUrb(void)
{
	memset(m_Urbs, 0, sizeof(m_Urbs));

	for( UINT i = 0; i < elements(m_Urbs); i ++ ) {

		m_Urbs[i].m_pDone = Create_ManualEvent();
		}
	}

void CUsbFuncDriver::InitUrb(void)
{
	for( UINT i = 0; i < elements(m_Urbs); i ++ ) {

		FreeUrb(i);
		}
	}

UINT CUsbFuncDriver::AllocUrb(void)
{
	m_pLimit->Wait(FOREVER);

	for( UINT i = 0; i < elements(m_Urbs); i ++ ) {

		m_pLock->Wait(FOREVER);
		
		if( m_Urbs[i].m_uStatus == UsbIor::statInit ) {

			m_Urbs[i].m_uStatus = UsbIor::statActive;

			m_pLock->Free();

			return i;
			}

		m_pLock->Free();
		}

	return NOTHING;
	}

void CUsbFuncDriver::FreeUrb(UINT i)
{
	if( i < elements(m_Urbs) ) {

		if( m_Urbs[i].m_uStatus != UsbIor::statInit ) {
		
			m_Urbs[i].m_uStatus = UsbIor::statInit;

			m_pLimit->Signal(1);
			}
		}
	}

void CUsbFuncDriver::KillUrb(void)
{
	memset(m_Urbs, 0, sizeof(m_Urbs));

	for( UINT i = 0; i < elements(m_Urbs); i ++ ) {

		if( m_Urbs[i].m_pDone ) {
			
			m_Urbs[i].m_pDone->Release();
		
			m_Urbs[i].m_pDone = NULL;
			}
		}
	}

// State 

void CUsbFuncDriver::SetState(UINT uState)
{
	if( uState != m_uStateNow ) {

		m_uStateWas = m_uStateNow;

		m_uStateNow = uState;
		
		#if defined(_XDEBUG)
		
		AfxTrace( "OnDevState %s -> %s\n", 
			  GetStateText(m_uStateWas), 
			  GetStateText(m_uStateNow)
			  );

		#endif

		switch( m_uStateNow ) {

			case devConfigured:
			case devDetached:
			case devAttached:
			case devPowered:
			case devDefault:
			case devAddress:
			case devSuspended:
				
				break;
			}
		
		m_pUpperDrv->OnState(m_uStateNow);
		}
	}

PCTXT CUsbFuncDriver::GetStateText(UINT uState) const
{
	#if defined(_DEBUG)

	switch( uState ) {

		case devDetached:   return "DETACHED";
		case devAttached:   return "ATTACHED";
		case devPowered:    return "POWERED";
		case devDefault:    return "DEFAULT";
		case devAddress:    return "ADDRESS";
		case devConfigured: return "CONFIGURED";
		case devSuspended:  return "SUSPENDED";
		}

	#endif

	return "<Unknown>";
	}

// Debug

void CUsbFuncDriver::DumpUrbs(void) const
{
	#if defined(_DEBUG)

	AfxTrace("CUsbFuncDriver::DumpUrbs()\n");

	for( UINT i = 0; i < elements(m_Urbs); i ++ ) {

		DumpUrb(i);
		}
	
	#endif
	}

void CUsbFuncDriver::DumpUrb(UINT i) const
{
	#if defined(_DEBUG)

	AfxTrace("Urb(%2.2d)\n", i);
	
	AfxTrace("m_iEndpt   = %d\n",      m_Urbs[i].m_iEndpt);
	AfxTrace("m_uFlags   = 0x%8.8X\n", m_Urbs[i].m_uFlags);
	AfxTrace("m_pData    = 0x%8.8X\n", m_Urbs[i].m_pData);
	AfxTrace("m_uCount   = %d\n",      m_Urbs[i].m_uCount);
	AfxTrace("m_uStatus  = 0x%8.8X\n", m_Urbs[i].m_uStatus);
	
	#endif
	}

// Entry Point

int CUsbFuncDriver::TaskUsbFuncDriver(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("UsbFunc");

	((CUsbFuncDriver *) pParam)->Service();

	return 0;
	}

// End of File
