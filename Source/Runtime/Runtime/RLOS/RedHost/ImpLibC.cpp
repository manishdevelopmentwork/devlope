
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LibC Implementation
//

// Code

clink void __libc_csu_init(void)
{
	}

clink void __libc_csu_fini(void)
{
	}

clink void abort(void)
{
	HostTrap(PANIC_AEON_ABORT);

	for(;;);
	}

clink int raise(int)
{	
	HostTrap(PANIC_AEON_RAISE);

	for(;;);
	}

clink void __malloc_lock(void *)
{
	Hal_Critical(true);
	}

clink void __malloc_unlock(void *)
{
	Hal_Critical(false);
	}

clink struct passwd * getpwuid(uid_t uid)
{
	return NULL;
	}

clink uid_t getuid(void)
{
	return 0;
	}

clink struct passwd * getpwnam(char const *name)
{
	return NULL;
	}

// End of File
