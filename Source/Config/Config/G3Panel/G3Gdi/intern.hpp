
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Embedded GDI Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HPP

#define	INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "g3gdi.hpp"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Library Files
//

#pragma	comment(lib, "msimg32.lib")

//////////////////////////////////////////////////////////////////////////
//
// Array Stuff
//

template <typename type> void ArrayZero(type *p, UINT n)
{
	memset(p, 0, sizeof(type) * n);
	}

template <typename type> void ArrayCopy(type *d, type const *s, UINT n)
{
	memcpy(d, s, sizeof(type) * n);
	}

template <typename type> type * ArrayAlloc(type const *d, UINT n)
{
	return (type *) malloc(sizeof(type) * n);
	}

template <typename type> type * ArrayReAlloc(type *d, UINT n)
{
	return (type *) realloc(d, sizeof(type) * n);
	}

//////////////////////////////////////////////////////////////////////////
//
// Inline Macro
//

#define	INLINE	__inline

// End of File

#endif
