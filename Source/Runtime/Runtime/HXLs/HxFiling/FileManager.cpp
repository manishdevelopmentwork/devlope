
#include "Intern.hpp"

#include "FileManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// File Manager
//

// Instantiator

IFileManager * Create_FileManager(void)
{
	CFileManager *p = New CFileManager;

	return p;
	}

// Constructors

CFileManager::CFileManager(void)
{
	StdSetRef();

	m_pDiskMan = NULL;

	m_pVolMan  = NULL;

	m_pMutex   = Create_Mutex();

	m_pHead    = NULL;

	m_pTail    = NULL;

	AfxGetObject("volman",  0, IVolumeManager, m_pVolMan);

	AfxGetObject("diskman", 0, IDiskManager,   m_pDiskMan);
	
	piob->RegisterSingleton("fs.fileman", 0, this);
	}

// Destructor

CFileManager::~CFileManager(void)
{
	m_pMutex->Release();
	}

// IUnknown

HRESULT CFileManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IFileManager);

	StdQueryInterface(IFileManager);

	return E_NOINTERFACE;
	}

ULONG CFileManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CFileManager::Release(void)
{
	StdRelease();
	}

// IFileManager

IFilingSystem * CFileManager::FindFileSystem(void)
{
	GuardThread(true);

	IFilingSystem *pFileSys = m_pVolMan->GetFileSystem();

	if( pFileSys ) {

		Register(pFileSys);

		GuardThread(false);

		return pFileSys;
		}
	
	GuardThread(false);

	return FindFileSystem(CHAR('C'));
	}

IFilingSystem * CFileManager::FindFileSystem(CHAR cDrive)
{
	GuardThread(true);

	IFilingSystem *pFileSys = m_pVolMan->GetFileSystem(m_pDiskMan->FindVolume(cDrive));

	if( pFileSys ) {

		Register(pFileSys);

		GuardThread(false);

		return pFileSys;
		}
	
	GuardThread(false);

	return NULL;
	}

IFilingSystem * CFileManager::FindFileSystem(UINT iVolume)
{
	GuardThread(true);

	IFilingSystem *pFileSys = m_pVolMan->GetFileSystem(iVolume);

	if( pFileSys ) {

		Register(pFileSys);

		GuardThread(false);

		return pFileSys;
		}
	
	GuardThread(false);

	return NULL;
	}

IFilingSystem * CFileManager::LockFileSystem(void)
{
	IFilingSystem *pFileSys = FindFileSystem();

	if( pFileSys ) {

		if( LockFileSystem(pFileSys) ) {

			return pFileSys;
			}

		FreeFileSystem(pFileSys, false);
		}

	return NULL;
	}

IFilingSystem * CFileManager::LockFileSystem(CHAR cDrive)
{
	IFilingSystem *pFileSys = FindFileSystem(cDrive);

	if( pFileSys ) {

		if( LockFileSystem(pFileSys) ) {

			return pFileSys;
			}

		FreeFileSystem(pFileSys, false);
		}

	return NULL;
	}

IFilingSystem * CFileManager::LockFileSystem(UINT iVolume)
{
	IFilingSystem *pFileSys = FindFileSystem(iVolume);

	if( pFileSys ) {

		if( LockFileSystem(pFileSys) ) {

			return pFileSys;
			}

		FreeFileSystem(pFileSys, false);
		}

	return NULL;
	}

BOOL CFileManager::LockFileSystem(IFilingSystem *pFileSys)
{
	if( pFileSys->WaitLock(FOREVER) ) {

		if( pFileSys->IsMounted() ) {

			return true;
			}

		pFileSys->FreeLock();
		}
	
	return false;
	}

void CFileManager::FreeFileSystem(IFilingSystem *pFileSys, BOOL fLocked)
{
	if( pFileSys ) {

		if( fLocked ) {

			pFileSys->FreeLock();
			}

		GuardThread(true);

		Unregister(pFileSys);

		GuardThread(false);
		}
	}

BOOL CFileManager::RegisterTask(IFilingSystem *pFileSys)
{
	if( pFileSys ) {

		GuardThread(true);

		pFileSys->AddRef();

		Register(pFileSys);
		
		GuardThread(false);

		return true;
		}

	return false;
	}

BOOL CFileManager::UnregisterTask(IFilingSystem *pFileSys)
{
	if( pFileSys ) {

		m_pMutex->Wait(FOREVER);
		
		CRef *pRef = Find(pFileSys);

		if( pRef ) { 
	
			if( !--pRef->m_uRef ) {

				GuardThread(true);

				AfxListRemove(m_pHead, m_pTail, pRef, m_pNext, m_pPrev);

				pRef->m_pRef->Release();

				GuardThread(false);

				delete pRef;
				}
			}

		m_pMutex->Free();

		return true;
		}

	return false;
	}

void CFileManager::Purge(UINT uTask)
{
	CRef *pRef = m_pHead;

	while( pRef ) {

		if( pRef->m_uTask == uTask ) {

			CRef *pNext = pRef->m_pNext;

			AfxListRemove(m_pHead, m_pTail, pRef, m_pNext, m_pPrev);

			pRef->m_pRef->Release();

			delete pRef;

			pRef = pNext;

			continue;
			}

		pRef = pRef->m_pNext;
		}

	m_pMutex->Free();
	}

// Implementation

CFileManager::CRef * CFileManager::Find(IFilingSystem *pFileSys)
{
	m_pMutex->Wait(FOREVER);

	CRef *pFind = m_pHead;

	while( pFind ) {

		if( pFind->m_pRef == pFileSys ) {

			if( pFind->m_uTask == GetThreadIndex() ) {
	
				m_pMutex->Free();

				return pFind;
				}
			}

		pFind = pFind->m_pNext;
		}

	m_pMutex->Free();

	return NULL;
	}

void CFileManager::Register(IFilingSystem *pFileSys)
{
	m_pMutex->Wait(FOREVER);

	CRef *pRef = Find(pFileSys);

	if( !pRef ) {

		pRef = New CRef;

		pRef->m_pRef  = pFileSys;

		pRef->m_uTask = GetThreadIndex();

		pRef->m_uRef  = 1;

		AfxListAppend(m_pHead, m_pTail, pRef, m_pNext, m_pPrev);
		}
	else {
		pRef->m_uRef ++;

		pRef->m_pRef->Release();
		}
	
	m_pMutex->Free();
	}

void CFileManager::Unregister(IFilingSystem *pFileSys)
{
	m_pMutex->Wait(FOREVER);
		
	CRef *pRef = Find(pFileSys);

	if( pRef ) { 
	
		if( !--pRef->m_uRef ) {

			AfxListRemove(m_pHead, m_pTail, pRef, m_pNext, m_pPrev);

			pRef->m_pRef->Release();

			delete pRef;
			}
		}

	m_pMutex->Free();
	}

// End of File
