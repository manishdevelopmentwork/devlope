
#include "intern.hpp"

#include "catwnd.hpp"

#include "navtree.hpp"

#include <shlink.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Category Window
//

// Dynamic Class

AfxImplementDynamicClass(CControlCatWnd, CViewWnd);

// Constructors

CControlCatWnd::CControlCatWnd(void)
{
	m_fDebug   = FALSE;

	m_fOnline  = FALSE;

	m_pNavTree = NULL;

	m_Accel.Create(L"ControlCatMenu");
	}

// Destructors

CControlCatWnd::~CControlCatWnd(void)
{
	}

// Binding

void CControlCatWnd::AddProgramCtrl(CControlProgramCtrlWnd *pProgCtrl)
{
	m_ProgCtrl.Insert(pProgCtrl);
	}

void CControlCatWnd::DelProgramCtrl(CControlProgramCtrlWnd *pProgCtrl)
{
	m_ProgCtrl.Remove(pProgCtrl);
	}

void CControlCatWnd::SetControlNavTree(CControlTreeWnd *pNavTree)
{
	m_pNavTree = pNavTree;
	}

// Attributes

BOOL CControlCatWnd::IsDebug(void) const
{
	return m_fDebug;
	}

BOOL CControlCatWnd::IsOnline(void) const
{
	return m_fOnline;
	}

// Operations

void CControlCatWnd::Commit(void)
{
	for( INDEX n = m_ProgCtrl.GetHead(); !m_ProgCtrl.Failed(n); m_ProgCtrl.GetNext(n) ) {

		m_ProgCtrl[n]->Commit();
		}
	}

void CControlCatWnd::Reload(void)
{
	for( INDEX n = m_ProgCtrl.GetHead(); !m_ProgCtrl.Failed(n); m_ProgCtrl.GetNext(n) ) {

		m_ProgCtrl[n]->Reload();
		}
	}

void CControlCatWnd::DetachEditors(CItem *pItem)
{
	for( INDEX n = m_ProgCtrl.GetHead(); !m_ProgCtrl.Failed(n); m_ProgCtrl.GetNext(n) ) {

		m_ProgCtrl[n]->DetachEditor(pItem);
		}
	}

BOOL CControlCatWnd::StartDebug(BOOL fOnline)
{
	if( !m_fDebug ) {

		Commit();

		CControlCompiler Comp(m_pProject);

		if( !Comp.NeedBuild() || Comp.Build(CControlCompiler::compSilent) || Comp.Build(CControlCompiler::compErrors) ) {

			if( !fOnline || Verify(m_pItem->GetDatabase()) ) {

				CString Path = m_pProject->GetProjectPath();

				if( m_Simulate.Open(ThisObject, m_pProject->GetDatabase(), Path, fOnline) ) {

					m_Simulate.SetCycleTime(m_pProject->m_pCycle->m_Scan);

					SetViewDebug(TRUE, fOnline);

					m_fOnline = fOnline;

					m_fDebug  = TRUE;

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

BOOL CControlCatWnd::StopDebug(void)
{
	if( m_fDebug ) {

		SetViewDebug(FALSE, FALSE);

		m_Simulate.Close();

		afxThread->SetStatusText(L"");

		m_fOnline = FALSE;

		m_fDebug  = FALSE;

		return TRUE;
		}

	return FALSE;
	}

// Overridables

void CControlCatWnd::OnAttach(void)
{
	m_pProject  = (CControlProject *) m_pItem;

	m_dwProject = m_pProject->GetHandle();
	}

// Message Map

AfxMessageMap(CControlCatWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_MWEVENT)
	AfxDispatchMessage(WM_DATABASE)
	
	AfxDispatchCommandType(IDM_GLOBAL, OnGlobalCommand)

	AfxDispatchGetInfoType(IDM_CTRL, OnCtrlGetInfo)
	AfxDispatchControlType(IDM_CTRL, OnCtrlControl)
	AfxDispatchCommandType(IDM_CTRL, OnCtrlCommand)

	AfxMessageEnd(CControlCatWnd)
	};

// Accelerators

BOOL CControlCatWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CControlCatWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	afxDatabase->SetCallback(GetHandle());
	}

void CControlCatWnd::OnPreDestroy(void)
{
	StopDebug();
	}

void CControlCatWnd::OnSetCurrent(BOOL fCurrent)
{
	if( !(m_fCurrent = fCurrent) ) {

		StopDebug();
		}
	}

void CControlCatWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {
		
		Menu.AppendMenu(CMenu(L"ControlCatMenu"));
		}
	}

void CControlCatWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 0 ) {
		
		Menu.AppendMenu(CMenu(L"ControlCatTool"));
		}
	}

void CControlCatWnd::OnMiddleware(UINT uCode, DWORD dwIdent)
{
	m_Simulate.OnEvent(uCode, dwIdent);
	}

void CControlCatWnd::OnDatabase(UINT uCode, DWORD dwIdent)
{
	if( m_pNavTree ) {

		m_pNavTree->SendMessage( m_MsgCtx.Msg.message,
					 m_MsgCtx.Msg.wParam,
					 m_MsgCtx.Msg.lParam
					 );
		}

	switch( HIWORD(uCode) ) {

		case 0x0008:
			OnPropertyEvent(LOWORD(uCode), dwIdent);
			break;

		case 0x0009:
			OnCommentEvent(LOWORD(uCode), dwIdent);
			break;

		case 0x0002:
			OnProgramEvent(LOWORD(uCode), dwIdent);
			break;

		case 0x0004:
			OnVarGroupEvent(LOWORD(uCode), dwIdent);
			break;

		case 0x0005:
			OnDataTypeEvent(LOWORD(uCode), dwIdent);
			break;

		case 0x0007:
			OnVariableEvent(LOWORD(uCode), dwIdent);
			break;

		case 0x8000:
			OnExternalEvent(LOWORD(uCode), dwIdent);
			break;

		default:
			OnUnknownEvent(uCode, dwIdent);
			break;
		}
	}

// Command Handlers

BOOL CControlCatWnd::OnGlobalCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GLOBAL_COMMIT:

			Commit();

			break;

		case IDM_GLOBAL_STOP_DEBUG:

			StopDebug();

			break;

		case IDM_GLOBAL_DROP_LINK:

			StopDebug();

			break;
		}

	return FALSE;
	}

BOOL CControlCatWnd::OnCtrlGetInfo(UINT uID, CCmdInfo &Info)
{
	if( !m_fCurrent ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_CTRL_SYNTAX:

			Info.m_Image = 0x41000000;

			break;

		case IDM_CTRL_BUILD:

			Info.m_Image = 0x60000035;

			break;

		case IDM_CTRL_SIMULATE:

			Info.m_Image = 0x10000034;

			break;

		case IDM_CTRL_ONLINE:

			Info.m_Image = 0x10000039;

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CControlCatWnd::OnCtrlControl(UINT uID, CCmdSource &Src)
{
	if( !m_fCurrent ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_CTRL_SYNTAX:
			
			Src.EnableItem(!m_fDebug && m_pNavTree->CanCheckSyntax());

			break;

		case IDM_CTRL_BUILD:
			
			Src.EnableItem(!m_fDebug && (HasDirtyView() || CControlCompiler(m_pProject).NeedBuild()));
			
			break;

		case IDM_CTRL_CLEAN:

			Src.EnableItem(!m_fDebug);
			
			break;

		case IDM_CTRL_MAPPINGS:

			Src.EnableItem(TRUE);

			break;

		case IDM_CTRL_ONLINE:

			Src.CheckItem ( m_fDebug && m_fOnline);

			Src.EnableItem(!m_fDebug || m_fOnline);

			break;

		case IDM_CTRL_SIMULATE:

			Src.CheckItem ( m_fDebug && !m_fOnline);

			Src.EnableItem(!m_fDebug || !m_fOnline);

			break;

		case IDM_CTRL_STOP:

			Src.EnableItem(m_fDebug);

			break;

		case IDM_CTRL_PAUSE_RESUME:

			Src.CheckItem (m_Simulate.CanSingleStep());

			Src.EnableItem(m_Simulate.CanPauseResume());

			break;

		case IDM_CTRL_SINGLE_STEP:

			Src.EnableItem(m_Simulate.CanSingleStep());

			break;

		case IDM_CTRL_CREDENTIALS:

			Src.EnableItem(TRUE);

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CControlCatWnd::OnCtrlCommand(UINT uID)
{
	if( !m_fCurrent ) {

		return FALSE;
		}

	switch( uID ) {

		case IDM_CTRL_SYNTAX:

			Commit();

			CControlCompiler(m_pProject).Check(m_pNavTree->GetProgramHandle());

			break;

		case IDM_CTRL_BUILD:

			Commit();
			
			CControlCompiler(m_pProject).Build();
			
			break;

		case IDM_CTRL_CLEAN:

			Commit();
			
			CControlCompiler(m_pProject).Clean();
			
			break;

		case IDM_CTRL_MAPPINGS:

			Commit();

			CVariableMappingDialog(m_pProject).Execute();

			break;

		case IDM_CTRL_ONLINE:

			OnCtrlSimulate(TRUE);

			break;

		case IDM_CTRL_SIMULATE:

			OnCtrlSimulate(FALSE);

			break;

		case IDM_CTRL_STOP:

			OnCtrlSimulate(m_fOnline);

			break;

		case IDM_CTRL_PAUSE_RESUME:

			m_Simulate.PauseResume();

			break;

		case IDM_CTRL_SINGLE_STEP:

			m_Simulate.SingleStep();

			break;

		case IDM_CTRL_CREDENTIALS:

			Link_StratonAskForCredentials();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

void CControlCatWnd::OnCtrlSimulate(BOOL fOnline)
{
	if( m_fDebug ) {

		if( m_fOnline && m_Simulate.CanSingleStep() ) {

			CString Text(CString(IDS_STOPPING_ONLINE));

			if( afxMainWnd->NoYes(Text) == IDNO ) {

				return;
				}
			}

		StopDebug();

		return;
		}
	
	StartDebug(fOnline);
	}

// Program Events

void CControlCatWnd::OnProgramEvent(UINT uCode, DWORD dwIdent)
{
	switch( uCode ) {

		case 1:
			OnProgramCreated(dwIdent);
			break;

		case 3:
			OnProgramRenamed(dwIdent);
			break;

		case 4:
			OnProgramDeleted(dwIdent);
			break;

		case 10:
			OnProgramLocals(dwIdent);
			break;
		}
	}

void CControlCatWnd::OnProgramCreated(DWORD dwIdent)
{
	UpdateFuncResources(dwIdent);
	}

void CControlCatWnd::OnProgramRenamed(DWORD dwIdent)
{
	Commit();

	CStratonProgramDescriptor Desc(m_dwProject, dwIdent);

	UINT c = Straton_XRefAutoReplaceInFiles( NULL,
						 afxDatabase->GetProjectPath(m_dwProject),
						 Desc.m_Prev,
						 Desc.m_Name,
						 NULL,
						 NULL
						 );

	AfxTouch(c);

	Reload();

	UpdateFuncResources(dwIdent);
	}

void CControlCatWnd::OnProgramDeleted(DWORD dwIdent)
{
	UpdateFuncResources(0);
	}

void CControlCatWnd::OnProgramLocals(DWORD dwIdent)
{
	// LATER -- Why is this needed?

	Commit();

	afxDatabase->SaveProject(m_dwProject);
	}

// Group Events

void CControlCatWnd::OnVarGroupEvent(UINT uCode, DWORD dwIdent)
{
	switch( uCode ) {

		case 2:
			OnVarGroupRenamed(dwIdent);
		}
	}

void CControlCatWnd::OnVarGroupRenamed(DWORD dwIdent)
{
	}

// Type Events

void CControlCatWnd::OnDataTypeEvent(UINT uCode, DWORD dwIdent)
{
	switch( uCode ) {

		case 1:
			OnTypeCreated(dwIdent);
			break;

		case 3:
			OnTypeRenamed(dwIdent);
			break;

		case 4:
			OnTypeDeleted(dwIdent);
			break;

		case 7:
			OnTypeChanged(dwIdent);
			break;
		}
	}

void CControlCatWnd::OnTypeRenamed(DWORD dwIdent)
{
	}

void CControlCatWnd::OnTypeCreated(DWORD dwIdent)
{
	OnTypeChanged(dwIdent);
	}

void CControlCatWnd::OnTypeDeleted(DWORD dwIdent)
{
	}

void CControlCatWnd::OnTypeChanged(DWORD dwIdent)
{
	CStratonDataTypeDescriptor Desc(m_dwProject, dwIdent);

	if( Desc.IsUdFb() ) {

		if( FALSE ) {

			// !!!! -- What does all this do?

			DWORD      dwType = afxDatabase->GetTypeUDFB(m_dwProject, dwIdent);

			CLongArray List;

			UINT       c;

			if( (c = afxDatabase->GetTypeParams(m_dwProject, dwType, List)) ) {
			
				for( UINT n = 0; n < c; n ++ ) {

					DWORD dwParam = List[n];

					DWORD dwKind  = afxDatabase->GetKindOfObject(dwParam);

					AfxTouch(dwKind);
					}
				}
			}

		UpdateFuncResources(0);
		}
	}

// Variable Events

void CControlCatWnd::OnVariableEvent(UINT uCode, DWORD dwIdent)
{
	switch( uCode ) {

		case 1:
			OnVariableCreated(dwIdent);
			break;

		case 2:
			OnVariableRenamed(dwIdent);
			break;

		case 5:
			OnVariableChanged(dwIdent);
			break;
		}
	}

// Variable Events

void CControlCatWnd::OnVariableCreated(DWORD dwIdent)
{
	}

void CControlCatWnd::OnVariableRenamed(DWORD dwIdent)
{
	Commit();

	CStratonVariableDescriptor Desc(m_dwProject, dwIdent);

	UINT c = Straton_XRefAutoReplaceInFiles( NULL,
						 afxDatabase->GetProjectPath(m_dwProject),
						 Desc.m_Prev,
						 Desc.m_Name,
						 NULL,
						 NULL
						 );

	AfxTouch(c);

	Reload();
	}

void CControlCatWnd::OnVariableChanged(DWORD dwIdent)
{
	}

// Other Events

void CControlCatWnd::OnPropertyEvent(UINT uCode, DWORD dwIdent)
{
	}

void CControlCatWnd::OnExternalEvent(UINT uCode, DWORD dwIdent)
{
	}

void CControlCatWnd::OnCommentEvent(UINT uCode, DWORD dwIdent)
{
	}

void CControlCatWnd::OnUnknownEvent(UINT uCode, DWORD dwIdent)
{
	AfxAssert(FALSE);
	}

// Implementation

BOOL CControlCatWnd::HasDirtyView(void)
{
	for( INDEX n = m_ProgCtrl.GetHead(); !m_ProgCtrl.Failed(n); m_ProgCtrl.GetNext(n) ) {

		if( m_ProgCtrl[n]->IsDirty() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CControlCatWnd::SetViewDebug(BOOL fDebug, BOOL fOnline)
{
	for( INDEX n = m_ProgCtrl.GetHead(); !m_ProgCtrl.Failed(n); m_ProgCtrl.GetNext(n) ) {

		m_ProgCtrl[n]->SetDebug(fDebug, fOnline);
		}

	return TRUE;
	}

BOOL CControlCatWnd::UpdateFuncResources(DWORD dwIdent)
{
	if( dwIdent ) {

		CStratonProgramDescriptor Prog(m_dwProject, dwIdent);

		if( !Prog.IsCalled() ) {
			
			if( !Prog.IsUdFb() ) {

				return FALSE;
				}
			}
		}

	m_pProject->m_pFuncs->Update();

	m_System.ItemUpdated(m_pProject->m_pFuncs, updateChildren);

	return TRUE;
	}

BOOL CControlCatWnd::Verify(CDatabase *pDbase)
{
	DropLink();
	
	return Link_Verify(pDbase, TRUE);
	}

BOOL CControlCatWnd::DropLink(void)
{
	afxMainWnd->RouteCommand(IDM_GLOBAL_DROP_LINK, 0);

	return TRUE;
	}

// End of File
