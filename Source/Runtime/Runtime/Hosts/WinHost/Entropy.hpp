
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Entropy_HPP

#define INCLUDE_Entropy_HPP

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

class CEntropy : public IEntropy
{
	public:
		// Constructor
		CEntropy(void);

		// Destructor
		~CEntropy(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IEntropy
		BOOL METHOD GetEntropy(PBYTE pData, UINT uData);

	protected:
		// Data Members
		ULONG m_uRefs;
	};

// End of File

#endif
