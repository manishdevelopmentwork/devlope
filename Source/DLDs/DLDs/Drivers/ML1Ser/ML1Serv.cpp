
#include "ml1serv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Server Implementation
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//


// Constructor

CML1Server::CML1Server(void)
{
	}

// Destructor

CML1Server::~CML1Server(void)
{
	
	}

// Access

BOOL  CML1Server::HandleFrame(PBYTE pBuff)
{
	ShowDebug("\nServer Handle Frame ");

	if( HandleAuthentication(pBuff) ) {

		return TRUE;
		}

	CML1Dev * pDev = (CML1Dev *)DoListen(GetID(pBuff), GetUnitID(pBuff));

	if( pDev ) {

		if( HandleResponse(pDev, pBuff) ) {

			pDev->m_fOnline = TRUE;

			return TRUE;
			}

		return HandleRequest(pDev, pBuff);
		}
		
	return FALSE;
	}

// Implementation

BOOL  CML1Server::HandleAuthentication(PBYTE pBuff)
{
	ShowDebug("\nServer Handle Authentication");

	if( pBuff && IsAuthentication(!m_fClient ? m_pDev : NULL, pBuff) && PassKey(pBuff) ) {

		if( m_fClient ) {
			
			switch( GetFC(pBuff, TRUE) ) {

				case authPush:
					return AckAuthentication(pBuff);

				case authResp:
					return MarkReqAuthentication(pBuff);
				}

			return FALSE;
			}

		switch( GetFC(pBuff, TRUE) ) {

			case authPoll:
				return RspAuthentication(pBuff);

			case authAck:
				return MarkPshAuthentication(pBuff);
			}
		}

	return FALSE;
	}

BOOL  CML1Server::HandleRequest(CML1Dev * pDev, PBYTE pBuff)
{
	ShowDebug("\nServer Handle Request %u %2.2x ", m_fClient, GetFC(pBuff));

	if( m_fClient ) {

		switch( GetFC(pBuff) ) {

			case 0x01:
				return GetCoils(pDev, spaceDC, GetCount(pBuff), pBuff);
			
			case 0x02:
				return GetCoils(pDev, spaceDI, GetCount(pBuff), pBuff);
			
			case 0x03:
				return GetWords(pDev, spaceHR, GetCount(pBuff), pBuff);
								
			case 0x04:
				return GetWords(pDev, spaceAI, GetCount(pBuff), pBuff);

			//case 0x06:
			//	return SetSpec(pDev, spaceSR, 1, pBuff);
	
			case 0x08:
				return HandleLoopBack(pDev, pBuff);
							
			//case 0x10:
			//	return SetSpec(pDev, spaceSR, GetCount(pBuff), pBuff);

			case 0x14:
				return GetFile(pDev, spaceFR, GetCount(pBuff, TRUE), pBuff);

			default:
				return TakeException(pDev, exFunction, pBuff);
			}

		return FALSE;
		}

	switch( GetFC(pBuff) ) {

		//case 0x03:
		//	return GetWords(pDev, spaceSR, GetCount(pBuff), pBuff);

		case 0x05:
			return SetCoils(pDev, spaceDC, 1, pBuff);
							
		case 0x06:
			return SetWords(pDev, spaceHR, 1, pBuff);
		
		case 0x08:
			return HandleLoopBack(pDev, pBuff);
							
		case 0x0F:
			return SetCoils(pDev, spaceDC, GetCount(pBuff), pBuff);
			
		case 0x10:
			return SetWords(pDev, spaceHR, GetCount(pBuff), pBuff);

		case 0x15:
			return SetFile(pDev, spaceFR, GetCount(pBuff, TRUE), pBuff);

		default:
			return TakeException(pDev, exFunction, pBuff);
		}

	return FALSE;
	}

BOOL  CML1Server::HandleResponse(CML1Dev * pDev, PBYTE pBuff)
{
	ShowDebug("\nServer Handle Response %u %2.2x ", m_fClient, GetFC(pBuff));

	if( m_fClient ) {

		switch( GetFC(pBuff) ) {

			//case 0x03:
			//	return MarkSpec(pDev, spaceSR, GetCount(pBuff), pBuff);

			case 0x05:
				return MarkCoils(pDev, spaceDC, 1, pBuff);
							
			case 0x06:
				return MarkWords(pDev, spaceHR, 1, pBuff);
		
			case 0x08:
				return HandleLoopBack(pDev, pBuff);
							
			case 0x0F:
				return MarkCoils(pDev, spaceDC, GetCount(pBuff), pBuff);
			
			case 0x10:
				return MarkWords(pDev, spaceHR, GetCount(pBuff), pBuff);

			case 0x15:
				return MarkFile(pDev, spaceFR, GetCount(pBuff, TRUE), pBuff);

			case 0x01:
			case 0x02:
			case 0x03:
			case 0x04:
			case 0x14:
				break;

			default:
				return TakeException(pDev, exFunction, pBuff);
			}

		return FALSE;
		}

	switch( GetFC(pBuff) ) {

		case 0x01:
			return CopyCoils(pDev, spaceDC, GetCount(pBuff), pBuff);
			
		case 0x02:
			return CopyCoils(pDev, spaceDI, GetCount(pBuff), pBuff);
			
		case 0x03:
			return CopyWords(pDev, spaceHR, GetCount(pBuff), pBuff);
								
		case 0x04:
			return CopyWords(pDev, spaceAI, GetCount(pBuff), pBuff);
	
		case 0x08:
			return HandleLoopBack(pDev, pBuff);
							
		//case 0x10:
		//	return MarkWords(pDev, spaceSR, GetCount(pBuff), pBuff);

		case 0x14:
			return CopyFile(pDev, spaceFR, GetCount(pBuff, TRUE), pBuff);

		case 0x05:
		case 0x06:
		case 0x0F:
		case 0x10:
		case 0x15:
			break;


		default:
			return TakeException(pDev, exFunction, pBuff);
		}

	return FALSE;
	}

PVOID CML1Server::DoListen(BYTE bId, DWORD dwUnit)
{
	CML1Dev * pDev = m_pHead;

	while( pDev ) {

		if( m_fClient ) {

			if( (pDev->m_Server == bId && m_Unit == dwUnit) ) {
				
				return pDev;
				}
			}
		else {
			if( (m_Server == bId && pDev->m_Unit == dwUnit) ) {

				return pDev;
				}
			}

		pDev = pDev->m_pNext;
		}
	
	return NULL;
	}

BOOL CML1Server::SendBlockRequest(CML1Dev * pDev, CML1Blk * pBlock, BYTE bOp, UINT uOffset)
{
	if( pBlock ) {

		if( bOp == opRead ) {

			if( DoBlockRead(pDev, pBlock, uOffset) ) {

				pBlock->SetRxTime(modeReqst);

				return TRUE;
				}

			return FALSE;
			}

		if( bOp == opWrite ) {

			UINT uCount = pBlock->GetSize();

			if( DoBlockWrite(pDev, pBlock, uOffset, uCount) ) {

				pBlock->SetTxTime(modeReqst);

				if( IsBroadcast(m_pDev->m_Server) ) {

					UINT uSent = uCount;

					while( uSent < pBlock->GetSize() ) {

						UINT uNext = pBlock->GetNext(uOffset + uSent, opWrite); 

						if( uNext ) {

							uCount = pBlock->GetSize() - uSent;

							if( DoBlockWrite(pDev, pBlock, uNext, uCount ) ) {

								uSent += uCount;

								Sleep(100);
								}
							}
						}

					pBlock->MarkComplete(opWrite, modeReply);
					}			

				return TRUE;
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CML1Server::DoBlockRead(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset)
{
	BOOL fOk = FALSE;

	ShowDebug("\nDo CML1Blk Read");

	if( pBlock ) {

		CAddress Addr;

		Addr.m_Ref = pBlock->GetAddr().m_Ref;

		Addr.a.m_Offset += uOffset;

		UINT uSize = pBlock->GetSize() - uOffset;

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	fOk = DoBitRead (pDev, Addr, uSize);	break;

			case addrWordAsWord:	fOk = DoWordRead(pDev, Addr, uSize);	break;

			case addrWordAsLong:
			case addrWordAsReal:	fOk = DoLongRead(pDev, Addr, uSize);	break;
			}
		}

	if( fOk ) {

		pBlock->IncPend(opRead);
		}
		
	return fOk;
	}

BOOL CML1Server::DoBlockWrite(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset, UINT& uCount)
{
	BOOL fOk = FALSE;

	if( pBlock ) {

		CAddress Addr;
		
		Addr.m_Ref = pBlock->GetAddr().m_Ref;
		
		uCount	   = pBlock->GetSize();
		
		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	fOk = DoBitWrite (pDev, Addr, pBlock->m_pTx, uCount); break;

			case addrWordAsWord:	fOk = DoWordWrite(pDev, Addr, pBlock->m_pTx, uCount); break;

			case addrWordAsLong:
			case addrWordAsReal:	fOk = DoLongWrite(pDev, Addr, pBlock->m_pTx, uCount); break;
				
			}		
		}

	if( fOk ) {

		pBlock->IncPend(opWrite);
		}
		
	return fOk;
	}

BOOL CML1Server::IsRequest(CML1Dev * pDev, PBYTE pBuff)
{
	return FALSE;
	}

CML1Blk * CML1Server::FindBlock(CML1Dev * pDev, UINT uSpace, UINT uOffset, UINT uFile, PBYTE pBuff)
{
	if( pDev ) {

		WORD wTrans = pBuff ? GetTrans(pBuff) : 0;

		CML1Blk * pBlock = pDev->m_pHead;

		while( pBlock ) {

			for( UINT o = 0; o < opWrite; o++ ) {

				if( pBlock->IsTrans(o, wTrans) ) {

					if( TestBlock(pBlock, uSpace, uOffset, uFile) ) {

						return pBlock;
						}
					}
				}

			pBlock = pBlock->m_pNext;
			}
		}

	return NULL;
	}

CML1Blk * CML1Server::ExtendRemoteRequest(CML1Dev * pDev, CML1Blk * pBlock, CAddress &Address, UINT &uIndex)
{
	UINT Size = pBlock->GetSize();
	
	GetEntitySize(Address, Size);

	if( uIndex == Size ) {

		UINT uNext = uIndex;

		pBlock = FindBlock(pDev, Address.a.m_Table, Address.a.m_Offset + uNext);

		if( pBlock && pBlock->GetAddr().m_Ref <= Address.m_Ref + uNext ) {

			uNext += Address.a.m_Offset;

			Address.m_Ref = pBlock->GetAddr().m_Ref;

			uIndex = uNext - Address.a.m_Offset;
			}
		else
			pBlock = NULL;
		}
	
	return pBlock;
	}

BOOL CML1Server::CheckRemoteRequest(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset, UINT uCount)
{
	AREF Addr = pBlock->GetAddr();

	UINT Size = uCount;
	
	GetEntitySize(Addr, Size);

	while( uCount > uOffset - Addr.a.m_Offset + Size ) {

		pBlock = FindBlock(pDev, Addr.a.m_Table, uOffset);

		if( !pBlock ) {

			return FALSE;
			}

		AREF Addr = pBlock->GetAddr();

		uCount   -= uOffset - Addr.a.m_Offset + Size;

		uOffset  += Size;
		}

	return TRUE;
	}


BOOL CML1Server::TestBlock(CML1Blk * pBlock, UINT uSpace, UINT uOffset, UINT uFile)
{
	AREF Addr = pBlock->GetAddr();

	UINT Size = pBlock->GetSize();

	GetEntitySize(Addr, Size);

	if( Addr.a.m_Table == uSpace ) {

		UINT uMask = NOTHING;

		if( uSpace == spaceFR ) { 

			if( CML1Base::GetFileNumber(Addr) != uFile ) {

				return FALSE;
				}

			uMask = 0x3FFF;
			}

		if( (Addr.a.m_Offset & uMask) <= uOffset ) {

			if( uOffset < ((Addr.a.m_Offset + Size) & uMask) ) { 

				return TRUE;
				}
			}
		}

	return FALSE;
	}

WORD CML1Server::GetTrans(PBYTE pBuff)
{
	return MAKEWORD(pBuff[1], pBuff[0]);
	}

DWORD CML1Server::GetUnitID(PBYTE pBuff, BOOL fAuth)
{
	if( fAuth ) {

		return pBuff[8] << 16 | pBuff[9] << 8 | pBuff[10];
		}

	return pBuff[2] << 16 | pBuff[3] << 8 | pBuff[4];
	}

BYTE  CML1Server::GetID(PBYTE pBuff, BOOL fAuth)
{
	if( fAuth ) {

		return pBuff[1];
		}

	return pBuff[6 + 0];
	}

BYTE  CML1Server::GetFC(PBYTE pBuff, BOOL fAuth)
{
	if( fAuth ) {

		return pBuff[0];
		}

	return pBuff[6 + 1];
	}

UINT  CML1Server::GetOffset(PBYTE pBuff)
{
	return pBuff[6 + 2] << 8 | pBuff[6 + 3];
	}

UINT  CML1Server::GetFileNumber(PBYTE pBuff)
{
	UINT uFile = pBuff[6 + 4] << 8 | pBuff[6 + 5];

	return uFile;
	}

UINT  CML1Server::GetFileRecord(PBYTE pBuff)
{
	return pBuff[6 + 6] << 8 | pBuff[6 + 7];
	}

UINT  CML1Server::GetFileBytes(PBYTE pBuff)
{
	return pBuff[6 + 8];
	}

UINT  CML1Server::GetCount(PBYTE pBuff, BOOL fFile)
{
	if( fFile ) {

		return pBuff[6 + 8] << 8 | pBuff[6 + 9];
		}

	return pBuff[6 + 4] << 8 | pBuff[6 + 5];
	}

UINT  CML1Server::GetBytes(PBYTE pBuff)
{
	return pBuff[5];
	}

PBYTE  CML1Server::GetData(PBYTE pBuff)
{
	UINT uIndex = 13;

	switch( GetFC(pBuff) ) {

		case 5:
		case 6:

			uIndex = 10;
			break;

		case 0x14:
		case 0x15:

			uIndex = 17;
			break;
		}

	return &pBuff[uIndex];
	}

WORD CML1Server::GetKey(PBYTE pBuff)
{
	BYTE bIndex = GetSpecialBytes(GetSpecialCount(pBuff));

	return pBuff[16 + bIndex] << 8 | pBuff[17 + bIndex];
	}

BYTE CML1Server::GetMacHead(PBYTE pBuff)
{
	return GetFC(pBuff, TRUE) == authPoll ? 1 : 2;
	}

DWORD CML1Server::GetAuthority(PBYTE pBuff)
{
	return  pBuff[11] << 24 | pBuff[12] << 16 | pBuff[13] << 8 | pBuff[14];
	}

BOOL  CML1Server::GetCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a read request from a remote client.

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	ShowDebug("\nRemote Client Read Coils %u %u %u %u", GetTickCount(), uSpace, uOffset, uCount);

	if( pBlock ) {

		AREF Addr = pBlock->GetAddr();

		if( uCount == 1 ) {

			StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));
			
			AddWord(uOffset);
			
			AddWord(pBlock->GetTxByte(uOffset - Addr.a.m_Offset) ? 0xFF00 : 0x0000);
			
			return Send();
			}

		if( CheckRemoteRequest(pDev, pBlock, uOffset, uCount) ) {

			StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

			AddWord(uOffset);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			CAddress Address;

			Address.m_Ref = Addr.m_Ref;

			for( UINT n = 0, u = uOffset - Addr.a.m_Offset; n < uCount; n++, u++ ) {

				if( (pBlock = ExtendRemoteRequest(pDev, pBlock, Address, u)) ) {

					if( pBlock->GetTxByte(u) ) {
					
						b |= m;
						}

					if( !(m <<= 1) ) {

						AddByte(b);

						b = 0;

						m = 1;
						}

					continue;
					}

				break;

				// Or should we take exception ???

				//TakeException(pDev, exAddress, pBuff);
					
				//return FALSE;
				}

			if( m > 1 ) {

				AddByte(b);
				}

			return Respond(pBuff);
			}
		}

	TakeException(pDev, exAddress, pBuff);

	return FALSE;
	}

BOOL  CML1Server::GetWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a read request from a remote client.

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	ShowDebug("\nRemote Client Read Words %u offset %u", GetTickCount(), uOffset);

	if( pBlock ) {

		if( CheckRemoteRequest(pDev, pBlock, uOffset, uCount) ) {

			StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

			AddWord(uOffset);

			AddWord(uCount);

			AddByte(uCount * 2);

			CAddress Address;

			Address.m_Ref = pBlock->GetAddr().m_Ref;

			uOffset -= Address.a.m_Offset;

			if( pBlock->IsLong() ) {

				uOffset /= 2;

				uCount  /= 2;
				}

			/*if( !m_fClient && uSpace == spaceSR ) {

				memcpy(pBlock->m_pTx, pBlock->m_pRx, pBlock->m_Bytes);
				}*/

			for( UINT n = 0, u = uOffset; n < uCount; n++, u++ ) {

				if( (pBlock = ExtendRemoteRequest(pDev, pBlock, Address, u)) ) {

					if( pBlock->IsLong() ) {

						AddLong(pBlock->GetTxLong(u));
						}
					else {  
						AddWord(pBlock->GetTxWord(u));
						}

					continue;
					}

				break;

				// Or should we take exception ???

				//TakeException(pDev, exAddress, pBuff);
					
				//return FALSE;
				}

			return Respond(pBuff);
			}
		}

	TakeException(pDev, exAddress, pBuff);

	return FALSE;
	}

BOOL  CML1Server::GetFile(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a file record read request from a remote client.

	UINT uFile	 = GetFileNumber(pBuff);

	UINT uRecord	 = GetFileRecord(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uRecord, uFile, pBuff);

	ShowDebug("\nRemote Client Read File %u file %u record %u", GetTickCount(), uFile, uRecord);

	if( pBlock ) {

		if( CheckRemoteRequest(pDev, pBlock, uRecord, uCount) ) {

			StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

			AddByte(10 + uCount * 2);

			AddByte(6);

			AddWord(uFile);

			AddWord(uRecord);

			AddWord(uCount);

			AddByte(uCount * 2);

			CAddress Address;

			Address.m_Ref = pBlock->GetAddr().m_Ref;

			uRecord -= (Address.a.m_Offset & 0x3FFF);

			if( pBlock->IsLong() ) {

				uRecord /= 2;

				uCount  /= 2;
				}

			for( UINT n = 0, u = uRecord; n < uCount; n++, u++ ) {

				if( (pBlock = ExtendRemoteRequest(pDev, pBlock, Address, u)) ) {

					if( pBlock->IsLong() ) {

						AddLong(pBlock->GetTxLong(u));
						}
					else {
						AddWord(pBlock->GetTxWord(u));
						}

					continue;
					}
				
				break;

				// Or should we take exception ???

				//TakeException(pDev, exAddress, pBuff);
					
				//return FALSE;
				}

			return Respond(pBuff);
			}
		}

	TakeException(pDev, 0x14, pBuff);

	return FALSE;
	}

BOOL  CML1Server::SetSpec(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a write request from a remote client to our special registers !!!

	ShowDebug("\nRemote Client Write Special %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		if( CheckRemoteRequest(pDev, pBlock, uOffset, uCount) ) {

			PBYTE pData = GetData(pBuff);

			if( pData ) {

				if( !IsBroadcast(GetID(pBuff)) ) {

					StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

					AddWord(uOffset);

					AddWord(uCount == 1 ? PWORD(pData)[0] : uCount);

					Respond(pBuff);
					}

				uOffset -= SR_MIN;

				for( UINT u = 0; u < uCount; uOffset++, u++ ) {

					UINT uBegin = uOffset;

					SetSr(pDev, uOffset, &PWORD(pData)[u]);

					if( uBegin < uOffset ) {

						u++;
						}
					}

				return TRUE;
				}
			}
		}

	TakeException(pDev, exAddress, pBuff);
	
	return FALSE;		
	}

BOOL  CML1Server::SetCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a write request from a remote client.

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	ShowDebug("\nRemote Client Write Coils %u %u %u %u", GetTickCount(), uSpace, uOffset, uCount);

	if( pBlock ) {

		if( CheckRemoteRequest(pDev, pBlock, uOffset, uCount) ) {

			PBYTE pData = GetData(pBuff);

			if( pData ) {

				UINT b = 0;

				BYTE m = 1;

				CAddress Address;

				Address.m_Ref = pBlock->GetAddr().m_Ref;

				for( UINT n = 0, u = uOffset - Address.a.m_Offset; n < uCount; n++, u++ ) {

					if( (pBlock = ExtendRemoteRequest(pDev, pBlock, Address, u)) ) {

						if( uCount == 1 ) {

							pBlock->SetRxByte(u, pData[0] ? 1 : 0);
							}
						else {
							pBlock->SetRxByte(u, (pData[b] & m) ? 1 : 0);

							if( !(m <<= 1) ) {

								b = b + 1;

								m = 1;
								}
							}

						continue;						
						}

					break;

					// Or should we take exception ???

					//TakeException(pDev, exAddress, pBuff);
					
					//return FALSE;
					}

				if( !IsBroadcast(GetID(pBuff)) ) {

					StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

					AddWord(uOffset);

					AddWord(uCount == 1 ? (pData[0] ? 1 : 0) : uCount);

					Respond(pBuff);
					}

				return TRUE;
				}
			}
		}

	TakeException(pDev, exAddress, pBuff);

	return FALSE;
	}

BOOL  CML1Server::SetWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a write request from a remote client.

	ShowDebug("\nRemote Client Write Words %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		if( CheckRemoteRequest(pDev, pBlock, uOffset, uCount) ) {

			PBYTE pData = GetData(pBuff);

			if( pData ) {

				CAddress Address;

				Address.m_Ref = pBlock->GetAddr().m_Ref;

				for( UINT n = 0, u = uOffset - Address.a.m_Offset; n < uCount; n++, u++ ) {

					if( (pBlock = ExtendRemoteRequest(pDev, pBlock, Address, u)) ) {

						pBlock->SetRxWord(u, SHORT(MotorToHost(PU2(pData)[n])));

						continue;
						}

					break;

					// Or should we take exception ???

					//TakeException(pDev, exAddress, pBuff);
					
					//return FALSE;
					}

				if( !IsBroadcast(GetID(pBuff)) ) {

					StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

					AddWord(uOffset);

					AddWord(uCount == 1 ? PWORD(pData)[0] : uCount);

					Respond(pBuff);
					}

				return TRUE;
				}
			}
		}

	TakeException(pDev, exAddress, pBuff);
	
	return FALSE;		
	}

BOOL  CML1Server::SetFile(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a file write request from a remote client.

	UINT uFile	 = GetFileNumber(pBuff);

	UINT uRecord	 = GetFileRecord(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uRecord, uFile, pBuff);

	ShowDebug("\nRemote Client Write File %u file %u record %u", GetTickCount(), uFile, uRecord);

	if( pBlock ) {

		if( CheckRemoteRequest(pDev, pBlock, uRecord, uCount) ) {

			PBYTE pData = GetData(pBuff);

			if( pData ) {

				CAddress Address;

				Address.m_Ref = pBlock->GetAddr().m_Ref;

				for( UINT n = 0, u = uRecord - (Address.a.m_Offset & 0x3FFF); n < uCount; n++, u++ ) {

					if( (pBlock = ExtendRemoteRequest(pDev, pBlock, Address, u)) ) {

						pBlock->SetRxWord(u, SHORT(MotorToHost(PU2(pData)[n])));

						continue;
						}

					break;

					// Or should we take exception ???

					//TakeException(pDev, exAddress, pBuff);
					
					//return FALSE;
					}

				if( !IsBroadcast(GetID(pBuff)) ) {

					StartFrame(pDev, GetFC(pBuff), GetTrans(pBuff));

					AddByte(9);

					AddByte(6);

					AddWord(uFile);

					AddWord(uRecord);

					AddWord(uCount);

					Respond(pBuff);
					}

				return TRUE;
				}
			}
		}

	TakeException(pDev, 0x15, pBuff);
	
	return FALSE;		
	}

BOOL  CML1Server::CopyCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client read request.

	ShowDebug("\nLocal Client Read Coil Resp %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		AREF Addr = pBlock->GetAddr();

		uOffset  -= Addr.a.m_Offset;

		PBYTE pData = GetData(pBuff);

		if( pData ) {

			if( uCount == 1 ) {

				pBlock->SetRxByte(uOffset, pData[0] ? 1 : 0);
				}
			else {
				UINT b = 0;

				BYTE m = 1;

				for( UINT n = 0; n < uCount; n++ ) {

					pBlock->SetRxByte(uOffset + n, (pData[b] & m) ? 1 : 0);

					if( !(m <<= 1) ) {

						b = b + 1;

						m = 1;
						}
					}
				}
					
			UINT uNext = pBlock->GetNext(GetOffset(pBuff) + uCount, opRead);

			if( uNext ) {

				SendBlockRequest(pDev, pBlock, opRead, uNext);

				return TRUE;
				}

			pBlock->MarkComplete(opRead, modeReply);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL  CML1Server::CopyWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client read request.

	ShowDebug("\nLocal Client Read Word Resp %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		AREF Addr = pBlock->GetAddr();

		uOffset  -= Addr.a.m_Offset;

		uOffset  *= sizeof(WORD);

		UINT Size = pBlock->GetSize();

		GetEntitySize(Addr, Size);

		MakeMin(uCount, Size - uOffset);

		PBYTE pData = GetData(pBuff);

		if( pData ) {

			pBlock->SetRxBuff(uOffset, pData, uCount * sizeof(WORD));

			UINT uNext = pBlock->GetNext(GetOffset(pBuff) + uCount, opRead);

			if( uNext ) {

				SendBlockRequest(pDev, pBlock, opRead, uNext);

				return TRUE;
				}

			pBlock->MarkComplete(opRead, modeReply);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL  CML1Server::CopyFile(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client file read request.

	ShowDebug("\nLocal Client Read File Resp %u", GetTickCount());

	UINT uFile	 = GetFileNumber(pBuff);

	UINT uRecord	 = GetFileRecord(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uRecord, uFile, pBuff);

	if( pBlock ) {

		AREF Addr = pBlock->GetAddr();

		uRecord  -= (Addr.a.m_Offset & 0x3FFF);

		uRecord  *= sizeof(WORD);

		UINT Size = pBlock->GetSize();

		GetEntitySize(Addr, Size);

		MakeMin(uCount, Size - uRecord);

		PBYTE pData = GetData(pBuff);

		if( pData ) {

			pBlock->SetRxBuff(uRecord, pData, uCount * sizeof(WORD));

			UINT uNext = pBlock->GetNext(GetFileRecord(pBuff) + uCount, opRead);

			if( uNext ) {

				SendBlockRequest(pDev, pBlock, opRead, uNext);

				return TRUE;
				}

			pBlock->MarkComplete(opRead, modeReply);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL  CML1Server::MarkSpec(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client read special register request.

	ShowDebug("\nLocal Client Read Special Resp %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		AREF Addr = pBlock->GetAddr();

		uOffset  -= Addr.a.m_Offset;

		uOffset  *= sizeof(WORD);

		UINT Size = pBlock->GetSize();

		GetEntitySize(Addr, Size);

		MakeMin(uCount, Size - uOffset);

		PBYTE pData = GetData(pBuff);

		if( pData ) {

			for(UINT u = 0; u < uCount; u++ ) {

				SetSr(pDev, u, &PWORD(pData)[u]);
				}

			pBlock->MarkComplete(opRead, modeReply);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL  CML1Server::MarkCoils(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client write request.

	ShowDebug("\nLocal Client Write Coil ACK %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		UINT uNext = pBlock->GetNext(uOffset + uCount, opWrite);

		if( uNext ) {

			SendBlockRequest(pDev, pBlock, opWrite, uNext);

			return TRUE;
			}

		pBlock->MarkComplete(opWrite, modeReply);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL  CML1Server::MarkWords(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client write request.

	ShowDebug("\nLocal Client Write Word ACK %u", GetTickCount());

	UINT uOffset	 = GetOffset(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uOffset, 0, pBuff);

	if( pBlock ) {

		UINT uNext = pBlock->GetNext(uOffset + uCount, opWrite);

		if( uNext ) {

			SendBlockRequest(pDev, pBlock, opWrite, uNext);

			return TRUE;
			}

		pBlock->MarkComplete(opWrite, modeReply);
		
		return TRUE;
		}

	return FALSE;		
	}

BOOL  CML1Server::MarkFile(CML1Dev * pDev, UINT uSpace, UINT uCount, PBYTE pBuff)
{
	// This is a response for the local client file write request.

	ShowDebug("\nLocal Client Write File ACK %u", GetTickCount());

	UINT uFile	 = GetFileNumber(pBuff);

	UINT uRecord	 = GetFileRecord(pBuff);

	CML1Blk * pBlock = FindBlock(pDev, uSpace, uRecord, uFile, pBuff);

	if( pBlock ) {

		UINT uNext = pBlock->GetNext(uRecord + uCount, opWrite);

		if( uNext ) {

			SendBlockRequest(pDev, pBlock, opWrite, uNext);

			return TRUE;
			}

		pBlock->MarkComplete(opWrite, modeReply);

		return TRUE;
		}

	return FALSE;		
	}

BOOL CML1Server::TakeException(CML1Dev * pDev, BYTE bCode, PBYTE pBuff)
{
	if( !IsBroadcast(GetID(pBuff)) ) {

		BYTE bEx = GetFC(pBuff) | 0x80;

		StartFrame(pDev, bEx, GetTrans(pBuff));

		AddByte(bCode);

		return Respond(pBuff);
		}

	return TRUE;
	}

BOOL CML1Server::HandleLoopBack(CML1Dev * pDev, PBYTE pBuff)
{
	if( GetOffset(pBuff) ) {

		return TakeException(pDev, exFunction, pBuff);
		}

	memcpy(m_pTx, m_pRx, 6);

	m_uTxPtr = 6;

	return Send();
	}

// Read Handlers

BOOL CML1Server::DoWordRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileRead(pDev, Addr, uCount);
		}

	switch( Addr.a.m_Table ) {
	
		case spaceHR:
	//	case spaceSR:
			
			StartFrame(pDev, 0x03);
			
			break;
			
		case spaceAI:
					
			StartFrame(pDev, 0x04);
			
			break;
			
		default:
			
			return FALSE;
		}

	MakeMaxCount(Addr, uCount);

	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	return Send();
	}

BOOL CML1Server::DoLongRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileRead(pDev, Addr, uCount);
		}

	switch( Addr.a.m_Table ) {
	
		case spaceHR:
			
			StartFrame(pDev, 0x03);
			
			break;
			
		case spaceAI:
					
			StartFrame(pDev, 0x04);
			
			break;
			
		default:
			return FALSE;
		}

	MakeMaxCount(Addr, uCount);

	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount * 2);

	return Send();
	} 

BOOL CML1Server::DoBitRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case spaceDC:
			
			StartFrame(pDev, 0x01);
		
			break;
			
		case spaceDI:
			
			StartFrame(pDev, 0x02);
			
			break;

		default:
			return FALSE;
		}

	MakeMaxCount(Addr, uCount);

	AddWord(Addr.a.m_Offset);
	
	AddWord(uCount);

	return Send();
	}

BOOL CML1Server::DoFileRead(CML1Dev * pDev, AREF Addr, UINT uCount)
{
	UINT uMult = IsLong(Addr) ? 2 : 1;

	UINT uFile = CML1Base::GetFileNumber(Addr);

	UINT uRec  = CML1Base::GetFileRecord(Addr);
	
	StartFrame(pDev, 0x14);
		
	MakeMaxCount(Addr, uCount);

	AddByte(0x9);	

	AddByte(0x6);	

	AddWord(uFile);

	AddWord(uRec);

	AddWord(uCount * uMult);

	return Send();
	}

// Write Handlers

BOOL CML1Server::DoWordWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileWrite(pDev, Addr, pData, uCount);
		}

	if( Addr.a.m_Table == spaceHR ) {

		MakeMaxCount(Addr, uCount);
			
		StartFrame(pDev, 16);
			
		AddWord(Addr.a.m_Offset);
			
		AddWord(uCount);
			
		AddByte(uCount * 2);
			
		for( UINT n = 0; n < uCount; n++ ) {
				
			AddWord(PWORD(pData)[n]);
			}
		
		return Send();
		}

	return FALSE;
	}

BOOL CML1Server::DoLongWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( IsFileRecord(Addr) ) {

		return DoFileWrite(pDev, Addr, pData, uCount);
		}

	if( Addr.a.m_Table == spaceHR ) {

		MakeMaxCount(Addr, uCount);

		StartFrame(pDev, 16);
		
		AddWord(Addr.a.m_Offset);
		
		AddWord(uCount * 2);
		
		AddByte(uCount * 4);

		for( UINT n = 0; n < uCount; n++ ) {

			AddLong(PDWORD(pData)[n]);
			}

		if( Send() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CML1Server::DoBitWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	if( Addr.a.m_Table == spaceDC ) {

		MakeMaxCount(Addr, uCount);
			
		StartFrame(pDev, 15);
			
		AddWord(Addr.a.m_Offset);
			
		AddWord(uCount);
			
		AddByte((uCount + 7) / 8);
				
		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) {
					
				b |= m;
				}

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;
					
				m = 1;
				}
			}

		if( m > 1 ) {

			AddByte(b);
			}
		
		return Send();
		}

	return FALSE;
	}

BOOL CML1Server::DoFileWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount)
{
	BOOL fLong = IsLong(Addr);

	UINT uMult = fLong ? 2 : 1;

	UINT uFile = CML1Base::GetFileNumber(Addr);

	UINT uRec  = CML1Base::GetFileRecord(Addr);

	MakeMaxCount(Addr, uCount);
			
	StartFrame(pDev, 0x15);

	AddByte(10 + uCount * 2 * uMult);

	AddByte(6);

	AddWord(uFile);

	AddWord(uRec);
			
	AddWord(uCount * uMult);
			
	AddByte(uCount * uMult * 2);
				
	for( UINT n = 0; n < uCount; n++ ) {
				
		fLong ? AddLong(PDWORD(pData)[n]) : AddWord(PWORD(pData)[n]);
		}

	return Send();
	}

// Authentication Support

BOOL CML1Server::AckAuthentication(PBYTE pBuff)
{
	ShowDebug("\nServer Ack Authentication %u", GetTickCount());

	if( m_fClient ) {

		CML1Dev * pDev = m_pHead;

		while( pDev ) {

			if( IsMacValid(pDev, pBuff) ) {

				if( DoPreAuthentication(pDev) ) {

					if( SetAuthentication(pDev, pBuff) ) {

						if( DoAckAuthentication(pDev, pBuff) ) {

							return DoPostAuthentication(pDev);
							}
						}
					}
				}

			pDev = pDev->m_pNext;
			}
		}

	return FALSE;
	}

BOOL CML1Server::RspAuthentication(PBYTE pBuff)
{
	ShowDebug("\nServer Rsp Authentication %u", GetTickCount());

	if( !m_fClient ) {
		
		CML1Dev * pDev = m_pHead;

		while( pDev ) {

			if( IsMacValid(pDev, pBuff) ) {

				if( DoRspAuthentication(pDev, pBuff) ) {

					pDev->m_fAuthentic = TRUE;

					return TRUE;
					}
				}

			pDev = pDev->m_pNext;
			}
		}

	return FALSE;
	}

BOOL CML1Server::MarkPshAuthentication(PBYTE pBuff)
{
	ShowDebug("\nServer Mark Psh Authentication %u", GetTickCount());

	DWORD Unit = GetUnitID(pBuff, TRUE);

	DWORD Auth = GetAuthority(pBuff);

	if( !m_fClient ) {

		CML1Dev * pDev = m_pHead;
	
		while( pDev ) {

			if( pDev->m_Server == GetID(pBuff, TRUE) ) {

				if( IsMacValid(pDev, pBuff) ) {

					if( pDev->m_Unit == Unit ) {

						if( pDev->m_Authority == Auth ) {

							pDev->m_fAuthentic = TRUE;

							pDev->m_fPush	   = FALSE;

							return TRUE;
							}
						}
					}
				}
		
			pDev = pDev->m_pNext;
			}
		}

	return FALSE;
	}

BOOL CML1Server::MarkReqAuthentication(PBYTE pBuff)
{
	ShowDebug("\nServer Mark Req Authentication %u", GetTickCount());

	if( m_fClient ) {

		CML1Dev * pDev = m_pHead;

		while( pDev ) {

			if( IsMacValid(pDev, pBuff) ) {

				return SetAuthentication(pDev, pBuff);
				}

			pDev = pDev->m_pNext;
			}
		}

	return FALSE;
	}

BOOL CML1Server::SetAuthentication(CML1Dev * pDev, PBYTE pBuff)
{
	ShowDebug("\nSet Authentication");

	if( pDev ) {

		m_Server	  = GetID(pBuff, TRUE);

		m_Unit		  = GetUnitID(pBuff, TRUE);

		m_Authority	  = GetAuthority(pBuff);

		pDev->m_Server	  = m_Server;

		pDev->m_Unit	  = m_Unit;

		pDev->m_Authority = m_Authority;
		
		CML1Blk * pBlock  = FindSpecial(pDev);

		if( pBlock ) {

			pBlock->SetRxWord(srServer, m_Server);

			pBlock->SetRxWord(srUnit + 0, HIWORD(m_Unit));
			
			pBlock->SetRxWord(srUnit + 1, LOWORD(m_Unit));
		
			pBlock->SetRxWord(srAuth + 0, HIWORD(m_Authority));

			pBlock->SetRxWord(srAuth + 1, LOWORD(m_Authority));

			BYTE bCount  = GetSpecialCount(pBuff);

			for( UINT u = srUTC, c = 0; c < bCount; c++, u++ ) {

				SetSr(pDev, u, &PWORD(pBuff + 16)[u - srUTC]);
				}
			}		
			
		m_fAuthentic = TRUE;

		return TRUE;
		}

	return FALSE;
	}

BOOL CML1Server::IsAuthentication(CML1Dev * pDev, PBYTE pBuff)
{
	ShowDebug("\nIs Authentication ");

	BYTE bFC = GetFC(pBuff, TRUE);

	if( bFC == authPush || bFC == authAck || bFC == authPoll || bFC == authResp ) {

		return IsMacValid(pDev, pBuff);
		}

	return FALSE;
	}

BOOL CML1Server::IsMacValid(CML1Dev * pDev, PBYTE pBuff)
{
	BYTE bOff = GetMacHead(pBuff);

	if( pDev ) {

		return !memcmp(pDev->m_Mac.m_Addr, pBuff + bOff, sizeof(pDev->m_Mac.m_Addr));
		}

	return !memcmp(m_Mac.m_Addr, pBuff + bOff, sizeof(m_Mac.m_Addr));
	}

BOOL CML1Server::PassKey(PBYTE pBuff)
{
	ShowDebug("\nPass Key ");

	if( pBuff ) {

		if( IsAuthenticationReq(pBuff) ) {

			return TRUE;
			}

		m_Key.Clear();

		for( UINT k = GetKeyHead(); k < GetKeyTail(pBuff); k++ ) {

			m_Key.Add(pBuff[k]);
			}

		if( m_Key.GetValue() == GetKey(pBuff) ) {

			return TRUE;
			}		
		}

	return FALSE;
	}

void CML1Server::GetMac(CML1Dev * pDev, PBYTE pBuff)
{
	if( pDev && pBuff ) {

		UINT uMac = sizeof(pDev->m_Mac.m_Addr);

		BYTE bOff = GetMacHead(pBuff);

		CMacAddr Null;

		memset(Null.m_Addr, 0, uMac);

		if( memcmp(pBuff + bOff, Null.m_Addr, uMac) ) {

			for( UINT m = 0; m < uMac; m++ ) {

				pDev->m_Mac.m_Addr[m] = pBuff[m + bOff];
				}
			}
		}
	}

BOOL CML1Server::IsAuthenticationReq(PBYTE pBuff)
{
	return ((GetFC(pBuff, TRUE) == authPoll) && IsMacValid(!m_fClient ? m_pDev : NULL, pBuff));
	}

BOOL CML1Server::DoAckAuthentication(CML1Dev * pDev, PBYTE pBuff)
{
	if( m_fClient && pDev ) {

		m_uTxPtr = 0;

		AddByte(authAck);

		AddByte(pDev->m_Server);

		AddMac(pDev);

		AddByte(LOBYTE(HIWORD(pDev->m_Unit)));

		AddWord(LOWORD(pDev->m_Unit));

		AddLong(pDev->m_Authority);
	
		AddSpecial(pDev, GetSpecialCount(pBuff));

		AddKey(m_pTx);

		return Respond(pDev);
		}

	return TRUE;
	}

BOOL CML1Server::DoRspAuthentication(CML1Dev * pDev, PBYTE pBuff)
{
	if( !m_fClient && pDev ) {

		m_uTxPtr = 0;

		AddByte(authResp);

		AddByte(pDev->m_Server);

		AddMac(pDev);
	
		AddByte(LOBYTE(HIWORD(pDev->m_Unit)));

		AddWord(LOWORD(pDev->m_Unit));

		AddLong(pDev->m_Authority);

		AddSpecial(pDev, GetSpecialExtra());

		pDev->m_fAuthentic = TRUE;
				
		AddKey(m_pTx);

		return Respond(pDev);
		}

	return TRUE;
	}

BOOL CML1Server::DoPreAuthentication(CML1Dev * pDev)
{
	return TRUE;
	}

BOOL CML1Server::DoPostAuthentication(CML1Dev * pDev)
{
	return TRUE;
	}

// End of File
