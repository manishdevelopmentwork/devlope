
#include "intern.hpp"

#include "segment.hpp"

#include "abl5k.hpp"

#define AfxTrace	m_pDriver->Trace

//////////////////////////////////////////////////////////////////////////
//
// CIP Path
//

// Constructor

CCIPPath::CCIPPath(void)
{
	m_pHead   = NULL;

	m_pTail   = NULL;

	m_uCount  = 0;

	m_fPadded = TRUE;

	m_pDriver = NULL;
	}

CCIPPath::CCIPPath(IEIPMaster *pDriver)
{
	m_pHead   = NULL;

	m_pTail   = NULL;

	m_uCount  = 0;

	m_fPadded = TRUE;

	m_pDriver = pDriver;
	}

// Destructor

CCIPPath::~CCIPPath(void)
{
	Free();
	}

// Attributes

CSegment * CCIPPath::GetHead(void) const
{
	return m_pHead;
	}

CSegment * CCIPPath::GetTail(void) const
{
	return m_pTail;
	}

UINT CCIPPath::GetCount(void) const
{
	return m_uCount;
	}

BOOL CCIPPath::GetPadded(void) const
{
	return m_fPadded;
	}

// Management

void CCIPPath::Empty(void)
{
	Free();

	m_pHead  = NULL;

	m_pTail  = NULL;

	m_uCount = 0;
	}

CSegment * CCIPPath::Append(CSegment *pSeg)
{
	m_uCount++;

	pSeg->SetPath(this);

	AfxListAppend(m_pHead, m_pTail, pSeg, m_pNext, m_pPrev);

	return pSeg;
	}

// Encoding

UINT CCIPPath::GetLength(void) const
{
	UINT uLen = 0;

	CSegment *pSeg = GetHead();

	while( pSeg ) {

		uLen += pSeg->GetLength();
		
		pSeg  = pSeg->m_pNext;
		}

	return uLen;
	}

UINT CCIPPath::Encode(CDataBuf &Buff)
{
	CSegment *pSeg = GetHead();

	while( pSeg ) {

		pSeg->Encode(Buff);

		pSeg = pSeg->m_pNext;		
		}

	return GetLength();
	}

// Parsing

UINT CCIPPath::Parse(CBuffer *pBuff)
{
	PBYTE pData = pBuff->GetData();

	UINT  uSize = pBuff->GetSize();

	return Parse(pData, uSize);
	}

UINT CCIPPath::Parse(PBYTE pData, UINT uSize)
{
	Empty();

	UINT uLen = 0;

	while( uLen < uSize ) {
		
		CSegment *pSeg = NULL;

		switch( (pData[0] & 0xE0) >> 5 ) {	
			
			case segDataTypeE:

				pSeg = new CSegDataTypeE;
				
				break;
			
			case segDataTypeC:

				pSeg = new CSegDataTypeC;
				
				break;
			}

		if( pSeg ) {

			Append(pSeg);

			UINT uCount = pSeg->Parse(pData, uSize - uLen);

			pData += uCount;

			uLen  += uCount;
			}
		else
			break;
		}

	return uLen;
	}

// Implementation

void CCIPPath::Free(void)
{
	CSegment *pScan = m_pHead;

	while( pScan ) {

		CSegment *pNext = pScan->m_pNext;

		delete pScan;

		m_uCount --;
		
		pScan = pNext;
		}
	}

// End of File
