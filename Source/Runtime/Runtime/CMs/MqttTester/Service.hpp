
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Service_HPP

#define INCLUDE_Service_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include "CrimsonDefs.hpp"
#include "CodedHost.hpp"
#include "CodedItem.hpp"
#include "ConfigBlob.hpp"
#include "DataTag.hpp"
#include "Format.hpp"
#include "ServiceItem.hpp"
#include "Tag.hpp"
#include "TagInteger.hpp"
#include "TagList.hpp"
#include "TagNumeric.hpp"
#include "TagReal.hpp"
#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Tag List
//

extern CTagList * afxTagList;

// End of File

#endif
