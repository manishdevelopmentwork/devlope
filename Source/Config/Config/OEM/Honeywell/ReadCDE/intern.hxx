
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//
//

#define IDS_BASE                0x4000
#define IDS_WRONG_VERSION_1	0x4000
#define IDS_WRONG_VERSION_2	0x4001
#define IDS_WRONG_VERSION_3	0x4002
#define IDS_WRONG_VERSION_4	0x4003
#define IDS_WRONG_VERSION_5	0x4004

// End of File

#endif
