
#include "Intern.hpp"

#include "dlcmod.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DAPID2Module_HPP

#define INCLUDE_DAPID2Module_HPP

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

class CDAPID2Module : public CDLCModule
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDAPID2Module(void);

	// UI Management
	CViewWnd * CreateMainView(void);

protected:
	// Download Support
	void MakeConfigData(CInitData &Init);

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif

