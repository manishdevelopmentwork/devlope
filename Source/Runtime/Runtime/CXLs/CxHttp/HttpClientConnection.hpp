
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpClientConnection_HPP
	
#define	INCLUDE_HttpClientConnection_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpClientManager;

class DLLAPI CHttpClientRequest;

class DLLAPI CHttpClientAuth;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Connection Options
//

class DLLAPI CHttpClientConnectionOptions : public CHttpConnectionOptions
{
	public:
		// Constructor
		CHttpClientConnectionOptions(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Data Members
		UINT m_uCheck;
		UINT m_uConnTimeout;
		BOOL m_fCompRequest;
		BOOL m_fCompReply;
		UINT m_uVer;
	};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Connection
//

class DLLAPI CHttpClientConnection : public CHttpConnection
{
	public:
		// Constructor
		CHttpClientConnection(CHttpClientManager *pManager, CHttpClientConnectionOptions const &Opts);

		// Destructor
		~CHttpClientConnection(void);

		// Operations
		BOOL SetServer(PCTXT pHost, PCTXT pUser, PCTXT pPass);
		BOOL Transact (CHttpClientRequest *pReq);

	protected:
		// Configuration
		CHttpClientConnectionOptions const &m_Opts;

		// Data Members
		CHttpClientManager * m_pManager;
		CString		     m_Host;
		CString		     m_User;
		CString		     m_Pass;
		IPADDR		     m_IP;
		char		     m_sVer[4];
		CHttpClientAuth    * m_pMethod[3];
		CHttpClientAuth    * m_pAuth;
		CStringMap           m_Cookies;

		// Implementation
		BOOL Recv(CHttpRequest *pReq);
		void ScanCookies(CHttpClientRequest *pReq);
		void BuildRequest(CHttpClientRequest *pReq);
		BOOL AddCompression(CHttpClientRequest *pReq);
		BOOL AddCookies(void);
		void ResetAuth(void);
		BOOL ProcessAuth(CHttpClientRequest *pReq, BOOL fFail);
		BOOL CheckConnection(void);
		BOOL CheckClose(CHttpRequest *pReq);
	};

// End of File

#endif
