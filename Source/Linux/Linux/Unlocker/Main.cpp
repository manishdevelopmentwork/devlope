
#include "Intern.hpp"

#include "Security.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Crypto Experiments
//
// Copyright (c) 2019 Red Lion Controls
//
// All Rights Reserved
//

int main(int nArg, char *pArg[])
{
	CSecurity sec;

	if( sec.Open() ) {

		if( nArg == 1 ) {

			BYTE key[16];

			if( sec.GetDeviceCode(key, sizeof(key)) == sizeof(key) ) {

				for( UINT n = 0; n < sizeof(key); n++ ) {

					printf("%2.2X", key[n]);
				}

				printf("\n%s,%s\n%8.8X\n%8.8X\n",
				       sec.GetDeviceModel().c_str(),
				       sec.GetSerialNumber().c_str(),
				       0,
				       sec.GetEnabledGroup()
				       );

				for( UINT n = 0; n < 4; n++ ) {

					printf("%s\n", sec.MakePassword().c_str());
				}

				return 0;
			}
		}

		if( nArg == 2 ) {

			if( !strcmp(pArg[1], "-show") ) {

				CSecureDeviceInfo Info;

				if( sec.GetDeviceInfo(Info) ) {

					char pass[32] = { 0 };

					strncpy(pass, PCSTR(Info.m_bDefPass), 22);

					printf("%s\n", pass);

					return 0;
				}
			}
			else {
				int fd = open(pArg[1], O_RDONLY, 0);

				if( fd >= 0 ) {

					bytes data(65536, 0);

					int r = read(fd, data.data(), data.size());

					close(fd);

					if( r > 0 && r < data.size() ) {

						data.resize(r);

						if( sec.InstallUnlock(data.data(), data.size()) ) {

							printf("OK\n");

							return 0;
						}
					}
				}
			}
		}
	}

	printf("FAIL\n");

	return 1;
}

// End of File
