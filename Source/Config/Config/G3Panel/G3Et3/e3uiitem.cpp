
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Et3 UI Item
//

// Dynamic Class

AfxImplementDynamicClass(CEt3UIItem, CUIItem);

// Constructor

CEt3UIItem::CEt3UIItem(void)
{
	}

// Useful

CString CEt3UIItem::FormatIPAddr(PCBYTE pAddr)
{
	return CPrintf(L"%u.%u.%u.%u", 
			pAddr[0],
			pAddr[1],
			pAddr[2],
			pAddr[3]
			);
	}

BOOL CEt3UIItem::LimitEnum(IUIHost *pHost, CString Tag, UINT &uData, UINT uMask)
{
	CLASS        Class = AfxRuntimeClass(CUITextEnum);

	CUITextEnum *pEnum = (CUITextEnum *) pHost->FindUI(Class, this, Tag);

	if( pEnum ) {

		pEnum->SetMask(uMask);

		if( !pEnum->IsDataValid(uData) ) {

			while( uData > 0x00 && !pEnum->IsDataValid(uData) ) {

				uData--;
				}

			while( uData < 0x1F && !pEnum->IsDataValid(uData) ) {

				uData++;
				}
			}

		if( pHost ) {

			pHost->UpdateUI(this, Tag);
			}

		return TRUE;
		}

	return FALSE;
	}

// Persistence

void CEt3UIItem::Init(void)
{
	CUIItem::Init();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

void CEt3UIItem::PostLoad(void)
{
	CUIItem::PostLoad();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

// End of File

