
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MatrixSocket_HPP

#define INCLUDE_MatrixSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CMatrixSsl;

//////////////////////////////////////////////////////////////////////////
//
// Matrix Socket
//

class CMatrixSocket : public ISocket
{
	public:
		// Constructor
		CMatrixSocket(CMatrixSsl *pSsl, ISocket *pSock, sslKeys_t *pKeys);

		// Destructor
		~CMatrixSocket(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket Methods
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &IP, WORD Loc);
		HRM Connect(IPADDR const &IP, WORD Rem);
		HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &IP);
		HRM GetRemote(IPADDR &IP);
		HRM GetLocal (IPADDR &IP, WORD &Port);
		HRM GetRemote(IPADDR &IP, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

	protected:
		// Socket States
		enum
		{
			stateInit      = 0,
			stateOpening   = 1,
			stateHandshake = 2,
			stateOpen      = 3,
			stateClosing   = 4,
			stateError     = 5
			};

		// Data Members
		ULONG		  m_uRefs;
		CMatrixSsl	* m_pSsl;
		ISocket         * m_pSock;
		sslKeys_t       * m_pKeys;
		UINT		  m_uState;
		PBYTE		  m_pAppData;
		UINT		  m_uAppData;
		sslSessOpts_t   * m_pOptions;
		ssl_t		* m_pSession;
		void		* m_pStream;

		// Overridables
		virtual void InitSession(void) = 0;

		// Implementation
		void InitOptions(void);
		void InitStream(void);
		BOOL UseExisting(void);
		BOOL PumpSendData(void);
		BOOL PumpRecvData(void);
		BOOL PumpRecvDone(void);
		BOOL CheckAlert(BYTE bLevel, BYTE bDesc);
		void KillOptions(void);
		void KillStream(BOOL fReset);
		void KillSession(void);
	};

// End of File

#endif
