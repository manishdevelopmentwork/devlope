
#include "Intern.hpp"

#include "Elm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Error Location Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CElm437::CElm437(void)
{
	m_pBase = PVDWORD(ADDR_ELM);

	m_uLine = INT_ELM;

	memset(m_pHook, 0, sizeof(m_pHook));

	Init();
	}

// Attributes

UINT CElm437::GetLine(void) const
{
	return m_uLine;
	}

// Operations

void CElm437::Reset(void)
{
	Reg(SYSCFG) |= Bit(1);

	while( !(Reg(SYSSTS) & Bit(0)) );
	}

void CElm437::SetAutoGating(bool fSet)
{
	if( fSet ) {

		Reg(SYSCFG) |= Bit(0);
		}
	else {
		Reg(SYSCFG) &= ~Bit(0);
		}
	}

void CElm437::SetBchEccLevel(UINT uBits)
{
	Reg(LOCCFG) &= ~(0x3);

	Reg(LOCCFG) |= (uBits >> 3);
	}

void CElm437::SetBchBuffSize(UINT uSize)
{
	Reg(LOCCFG) &= ~(0x7FF << 16);

	Reg(LOCCFG) |=  (uSize << 16);
	}

void CElm437::SetPageMode(UINT uMode)
{
	Reg(PAGECTRL) &= ~0xFF;

	Reg(PAGECTRL) |= uMode;
	}

void CElm437::SetSyndromeFrag(UINT uPolyNum, UINT uFragId, UINT uData)
{
	DWORD volatile &Reg = m_pBase[regSYNDROME + uFragId + (uPolyNum * 0x40 / sizeof(DWORD))];

	Reg = uData;
	}

void CElm437::SetSyndromeStart(UINT uPolyNum)
{
	DWORD volatile &Reg = m_pBase[regSYNDROME + 6 + (uPolyNum * 0x40 / sizeof(DWORD))];

	Reg |= Bit(16);
	}

bool CElm437::GetErrorStatus(UINT uPolyNum)
{
	DWORD volatile &Reg = m_pBase[regLOCSTS + (uPolyNum * 0x100 / sizeof(DWORD))];

	return Reg & Bit(8);
	}

UINT CElm437::GetErrorCount(UINT uPolyNum)
{
	DWORD volatile &Reg = m_pBase[regLOCSTS + (uPolyNum * 0x100 / sizeof(DWORD))];

	return Reg & 0x1F;
	}

UINT CElm437::GetErrorAddr(UINT uPolyNum, UINT iError)
{
	DWORD volatile &Reg = m_pBase[regERROR + iError + (uPolyNum * 0x100 / sizeof(DWORD))];

	return Reg & 0x1FFF;
	}

void CElm437::SetCompHook(UINT i, IEventSink *pSink)
{
	if( i < elements(m_pHook) ) {
		
		m_pHook[i]  = pSink;

		Reg(IRQSTS) = Bit(i);
		
		Reg(IRQEN) |= Bit(i);
		}
	}

// IEventSink

void CElm437::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uLine ) {

		for(;;) { 

			UINT uStatus = Reg(IRQSTS);

			if( uStatus & 0xFF ) {

				UINT uMask = Bit(0);

				for( UINT n = 0; n < elements(m_pHook); n ++ ) {

					if( uStatus & uMask ) {

						Reg(IRQSTS) = uMask;

						m_pHook[n]->OnEvent(m_uLine, n);
						}

					uMask <<= 1;
					}

				continue;
				}

			break;
			}
		}
	}

// Implementation

void CElm437::Init(void)
{
	Reset();

	Reg(SYSCFG) = Bit(8) | (1 << 3);

	Reg(IRQEN)  = 0;

	Reg(IRQSTS) = 0x1FF;

	EnableEvents();
	}

void CElm437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);
	
	phal->EnableLine(m_uLine, true);
	}

// End of File
