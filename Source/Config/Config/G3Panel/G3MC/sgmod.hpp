
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SGMOD_HPP

#define INCLUDE_SGMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "sgmap.hpp"

#include "sgloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSGModule;
class CSGMainWnd;

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module
//

class CSGModule : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSGModule(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		CSGMapper * m_pMapper;
		CSGLoop   * m_pLoop;

	protected:
		// Implementation
		void AddMetaData(void);

		// Download Support
		void MakeConfigData(CInitData &Init);
	};

//////////////////////////////////////////////////////////////////////////
//
// Strain Gage Module Main Window
//

class CSGMainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSGMainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd *	m_pMult;
		CSGModule     *	m_pItem;

		// Overribables
		void OnAttach(void);

		// Implementation
		void AddPIDPages(void);
		void AddMapPages(void);
	};

// End of File

#endif
