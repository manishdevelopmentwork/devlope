
#include "intern.hpp"

#include "idec.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IDEC Micro 3 Series / ONC Driver
//

// Instantiator

ICommsDriver *	Create_Idec3PLCDriver(void)
{
	return New CIdec3PLCDriver;
	}

// Constructor

CIdec3PLCDriver::CIdec3PLCDriver(void)
{
	m_wID		= 0x3332;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "IDEC";
	
	m_DriverName	= "Micro 3 Series / ONC";
	
	m_Version	= "1.04";
	
	m_ShortName	= "IDEC Micro 3";

	AddSpaces();
	}

// Binding Control

void CIdec3PLCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Address Helpers

BOOL CIdec3PLCDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uTable  = pSpace->m_uTable;

	UINT    uType   = pSpace->m_uType;

	UINT    uOffset = 0;

	BOOL    fFail   = FALSE;

	UINT    uPos    = Text.Find('.');

	CString Offset  = Text.Left(uPos);

	CString Type    = StripType(pSpace, Text);

	if( IsByteAndBit(pSpace) ) {

		PTXT    pError = NULL;

		CString sLeft  = Offset.Left(Offset.GetLength()-1);
		
		CString sRight = Offset.Right(1);

		uOffset = tstrtoul(sLeft , &pError, 10);
		
		if( pError && pError[0] ) {
			
			fFail = TRUE;
			}
		else {
			uOffset *= 8;

			uOffset += tstrtoul(sRight, &pError, 8);
			
			if( pError && pError[0] ) {

				fFail = TRUE;
				}
			}
		}
	else {
		PTXT pError = NULL;

		uOffset = tstrtoul(Offset, &pError, pSpace->m_uRadix);

		if( pError && pError[0] ) {

			fFail = TRUE;
			}
		}

	if( fFail ) {
	
		Error.Set( CString(IDS_ERROR_ADDR),
			   0
			   );
		
		return FALSE;
		}

	if( pSpace->IsOutOfRange(uOffset) ) {

		Error.Set( CString(IDS_ERROR_OFFSETRANGE),
			   0
			   );

		return FALSE;
		}
		
	if( pSpace->IsBadAlignment(uOffset) ) {
	
		Error.Set( CString(IDS_ERROR_BADOFFSET),
			   0
			   );
		
		return FALSE;
		}

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uOffset;
	
	Addr.a.m_Table	= uTable;
	
	Addr.a.m_Type	= !Type.IsEmpty() ? pSpace->TypeFromModifier(Type) : uType;

	return TRUE;
	}

BOOL CIdec3PLCDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		CSpace *pSpace = GetSpace(Addr);

		if( pSpace ) {

			if( IsByteAndBit(pSpace) ) {

				UINT uOffset = Addr.m_Ref & 0xFFFF;

				Text.Printf( "%s%0*d%1.1d",
					     pSpace->m_Prefix,
					     pSpace->m_uWidth - 1,
					     uOffset/8,
					     uOffset%8
					     );
				
				return TRUE;
				}

			if( Addr.a.m_Type > WW ) {

				Text.Printf( "%s%s.%s",
					     pSpace->m_Prefix,
					     pSpace->GetValueAsText(Addr.a.m_Offset),
					     pSpace->GetTypeModifier(Addr.a.m_Type)
				     );

				return TRUE;				
				}

			Text.Printf( "%s%s",   
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText(Addr.a.m_Offset)
				     );

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

void CIdec3PLCDriver::AddSpaces(void)
{
	// NOTE -- MB must come before M, and RB before R!

	AddSpace(New CSpace('1', "D",   "Data Registers",		10,	0,	 8999,	WW, addrWordAsReal));
	AddSpace(New CSpace('X', "EDR", "Extra Data Register",		10, 10000,	55999,	WW, addrWordAsReal));
	AddSpace(New CSpace('2', "TC",  "Timer (Current Value)",	10,	0,	 1023,	WW));
	AddSpace(New CSpace('3', "TP",  "Timer (Preset Value)",		10,	0,	 1023,	WW));
	AddSpace(New CSpace('4', "CC",  "Counter (Current Value)",	10,	0,	  511,	WW));
	AddSpace(New CSpace('5', "CP",  "Counter (Preset Value)",	10,	0,	  511,	WW));
	AddSpace(New CSpace('6', "I",   "Input Bits",			10,	0,     63*8+7,	BB));
	AddSpace(New CSpace('7', "Q",   "Output Bits",			10,	0,     63*8+7,	BB));
	AddSpace(New CSpace('D', "MB",  "Ordinary Relay Bytes",		10,	0,	  799,	YY));
	AddSpace(New CSpace('8', "M",   "Ordinary Relay Bits",		10,	0,    799*8+7,  BB));
	AddSpace(New CSpace('9', "S8",  "Special Relay Bits",		10,	0,     31*8+7,	BB));
	AddSpace(New CSpace('F', "RB",  "Shift Register Bytes",		10,	0,	  248,	YY));
	AddSpace(New CSpace('A', "R",   "Shift Register Bits",		10,	0,	  255,  BB));
	AddSpace(New CSpace('B', "IB",  "Input Bytes",			10,	0,	   63,  YY));
	AddSpace(New CSpace('C', "QB",  "Output Bytes",			10,	0,	   63,	YY));
	AddSpace(New CSpace('E', "SB8", "Special Relay Bytes",		10,	0,	   31,	YY));
	//AddSpace(New CSpace('Z', "ER",  "Comm Error Count",		10,	0,	    0,  WW));
	}

// Address Checking

BOOL CIdec3PLCDriver::IsByteAndBit(CSpace * pSpace)
{
	if( pSpace ) {

		if( pSpace->m_uType == BB && pSpace->m_Prefix != "R" ) {

			return TRUE;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// IDEC MicroSmart TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIdecMicroSmartMasterTCPDeviceOptions, CUIItem);       

// Constructor

CIdecMicroSmartMasterTCPDeviceOptions::CIdecMicroSmartMasterTCPDeviceOptions(void)
{
	m_IP1   = DWORD(MAKELONG(MAKEWORD( 1, 5), MAKEWORD(  168, 192)));

	m_IP2   = DWORD(0);
	        
	m_Port  = 2101;
	        
	m_Unit  = 1;
	        
	m_Keep  = TRUE;
	        
	m_Ping  = FALSE;

	m_Time1 = 5000;

	m_Time2 = 2500;

	m_Time3 = 200;
	}

// UI Managament

void CIdecMicroSmartMasterTCPDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pHost->EnableUI("Time3", !m_Keep);
			}
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CIdecMicroSmartMasterTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_IP1));
	Init.AddLong(LONG(m_IP2));
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));
	
	return TRUE;
	}

// Meta Data Creation

void CIdecMicroSmartMasterTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(IP1);
	Meta_AddInteger(IP2);
	Meta_AddInteger(Port);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

/////////////////////////////////////////////////////////////////////////
//
// IDEC MicroSmart TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_IdecMicroSmartMasterTCPDriver(void)
{
	return New CIdecMicroSmartMasterTCPDriver;
	}

// Constructor

CIdecMicroSmartMasterTCPDriver::CIdecMicroSmartMasterTCPDriver(void)
{
	m_wID		= 0x40E1;

	m_uType		= driverMaster;
	
	m_DriverName	= "MicroSmart TCP/IP Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "IDEC MicroSmart";
	}

// Binding Control

UINT CIdecMicroSmartMasterTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CIdecMicroSmartMasterTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CIdecMicroSmartMasterTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIdecMicroSmartMasterTCPDeviceOptions);
	}


// End of File
