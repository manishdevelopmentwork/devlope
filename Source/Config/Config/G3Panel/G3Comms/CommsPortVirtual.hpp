
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortVirtual_HPP

#define INCLUDE_CommsPortVirtual_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Virtual Comms Port
//

class DLLNOT CCommsPortVirtual : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortVirtual(void);

		// Initial Values
		void SetInitValues(void);

		// UI Overridables
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);
		BOOL OnLoadPages(CUIPageList *pList);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetPageType(void) const;

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Link Options
		enum {
			linkDefault	= 0,
			linkKeepAlive	= 1,
			linkTimeout	= 2
			};

		// Item Properties
		CCodedItem * m_pMode;
		CCodedItem * m_pIP;
		CCodedItem * m_pPort;
		CCodedItem * m_pLinkOpt;
		CCodedItem * m_pTimeout;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
