
#include <windows.h>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

extern "C" int fastlz_compress(const void *input, int length, void *output);

extern "C" int fastlz_decompress(const void *input, int length, void *output, int maximum);
