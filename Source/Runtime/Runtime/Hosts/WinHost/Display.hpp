
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Display_HPP

#define INCLUDE_Display_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "GdiPlus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Use Entire Win32 API
//

using namespace win32;

//////////////////////////////////////////////////////////////////////////
//
// Display Object
//

class CDisplay : public IDisplay,
		 public ITouchScreen,
		 public ILeds
{
	public:
		// Constructor
		CDisplay(void);

		// Destructor
		~CDisplay(void);

		// Operations
		void    Bind(HWND hWnd);
		LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IDisplay
		void METHOD Claim(void);
		void METHOD Free(void);
		void METHOD GetSize(int &cx, int &cy);
		void METHOD Update(PCVOID pData);
		BOOL METHOD SetBacklight(UINT pc);
		UINT METHOD GetBacklight(void);
		BOOL METHOD EnableBacklight(BOOL fOn);
		BOOL METHOD IsBacklightEnabled(void);

		// ITouchScreen
		void   METHOD GetCellSize(int &xCell, int &yCell);
		void   METHOD GetDispCells(int &xDisp, int &yDisp);
		void   METHOD ITouchScreen::SetBeepMode(bool fBeep);
		void   METHOD SetDragMode(bool fDrag);
		PCBYTE METHOD GetMap(void);
		void   METHOD SetMap(PCBYTE pMap);
		bool   METHOD GetRaw(int &xRaw, int &yRaw);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);
		void METHOD SetLedLevel(UINT uPercent, bool fPersist);
		UINT METHOD GetLedLevel(void);

		// Instance Pointer
		static CDisplay * m_pThis;

	protected:
		// Data Members
		ULONG          m_uRefs;
		IMutex       * m_pLock;
		HWND	       m_hWnd;
		bool	       m_fShow;
		int	       m_cx;
		int	       m_cy;
		int	       m_xCells;
		int	       m_yCells;
		UINT	       m_uTimer;
		BOOL	       m_fBeep;
		HDC	       m_hDraw;
		HBITMAP	       m_hBits;
		HBITMAP	       m_hOld;
		PBYTE	       m_pData;
		HANDLE	       m_hEvent;
		IInputQueue  * m_pInput;
		IBeeper      * m_pBeeper;
		PCBYTE	       m_pMap;
		BOOL	       m_fPress;
		int	       m_xPress;
		int	       m_yPress;
		int	       m_xVal;
		int	       m_yVal;
		BOOL	       m_fLed[3];
		HBRUSH	       m_hLed[3];

		// Image Loading
		GpImage * CreateImage(void);

		// Implementation
		bool StoreEvent(UINT uType);
		bool IsActive(void);
	};

// End of File

#endif
