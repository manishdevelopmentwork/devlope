
#include "Intern.hpp"

#include "SystemLibrary.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "NameServer.hpp"
#include "SystemLibraryItem.hpp"
#include "SystemLibraryList.hpp"
#include "SystemLibraryWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Libary Resource
//

// Dynamic Class

AfxImplementDynamicClass(CSystemLibrary, CMetaItem);

// Constructor

CSystemLibrary::CSystemLibrary(void)
{
	m_pLib    = New CSystemLibraryList;

	m_pServer = NULL;
	}

// UI Creation

CViewWnd * CSystemLibrary::CreateView(UINT uType)
{
	if( uType == viewResource ) {

		return New CSystemLibraryWnd;
		}

	return CMetaItem::CreateView(uType);
	}

// Operations

void CSystemLibrary::ReadLibrary(void)
{
	while( m_pLib->GetItemCount() ) {

		INDEX Index = m_pLib->GetHead();

		m_pLib->DeleteItem(Index);
	}

	LoadVariables();

	LoadFunctions();
}

// Persistance

void CSystemLibrary::Init(void)
{
	CMetaItem::Init();

	FindNameServer();
	}

void CSystemLibrary::PostLoad(void)
{
	CMetaItem::PostLoad();

	while( m_pLib->GetItemCount() ) {

		INDEX Index = m_pLib->GetHead();

		m_pLib->DeleteItem(Index);
		}

	FindNameServer();
	}

// Meta Data

void CSystemLibrary::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddObject(Lib);

	Meta_SetName((IDS_SYSTEM));
	}

// Implementation

void CSystemLibrary::LoadVariables(void)
{
	CString Root = IDS_VARIABLES;

	AddCategory(Root);

	for( WORD wIdent = 0; wIdent < 128; wIdent++ ) {

		CString Name;

		if( m_pServer->NameIdent(WORD(0x7C00 + wIdent), Name) ) {

			WORD     ID;

			CTypeDef Type;

			if( FindIdent(Name, ID, Type) ) {

				CSystemLibraryItem * pItem = New CSystemLibraryItem;

				pItem->m_Ident = ID;

				pItem->m_Image = (Type.m_Flags & flagWritable) ? 10 : 2;

				pItem->m_Name  = CFormat(L"%1.%2", Root, Name);

				pItem->m_Code  = Name;

				m_pLib->AppendItem(pItem);
				}
			}
		}
	}

void CSystemLibrary::LoadFunctions(void)
{
	CStringTree Used;

	CString Root = IDS_FUNCTIONS;

	CString All  = CString(IDS_ALL);

	AddCategory(Root);

	AddCategory(CFormat(L"%1.%2", Root, All));

	WORD wBase[] = { 0x1000, 0x2000 };

	for( UINT n = 0; n < elements(wBase); n++ ) {

		// Load all of the functions

		for( WORD wIdent = 0; wIdent <= 0x160; wIdent++ ) {

			WORD ID = WORD(wBase[n] + wIdent);

			if( ID >= 0x2132 && ID <= 0x2135 ) {

				// Do not show functions 0x2132 - 0x2135

				continue;
				}

			UINT     uCount;

			CString  Name;

			CTypeDef Type;

			if( FindFunction(Name, ID, Type, uCount) ) {

				if( !Used.Failed(Used.Find(Name)) ) {

					continue;
					}

				if( TRUE ) {

					CSystemLibraryItem *pItem = New CSystemLibraryItem;

					m_pLib->AppendItem(pItem);

					pItem->m_Ident = ID;

					pItem->m_Image = (Type.m_Flags & flagActive) ? 10 : 2;

					pItem->m_Name  = CFormat(L"%1.%2.%3", Root, All, Name);

					pItem->MakeCode();
					}

				if( TRUE ) {

					CString Cat;

					if( m_pServer->FindCategory(ID, Cat) ) {

						AddCategory(CFormat(L"%1.%2", Root, Cat));

						CSystemLibraryItem *pItem = New CSystemLibraryItem;

						m_pLib->AppendItem(pItem);

						pItem->m_Ident = ID;

						pItem->m_Image = (Type.m_Flags & flagActive) ? 10 : 2;

						pItem->m_Name  = CFormat(L"%1.%2.%3", Root, Cat, Name);

						pItem->MakeCode();
						}
					}

				Used.Insert(Name);
				}
			}
		}
	}

void CSystemLibrary::FindNameServer(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	m_pServer             = pSystem->GetNameServer();
	}

BOOL CSystemLibrary::FindFunction(CString &Name, WORD ID, CTypeDef &Type, UINT &uCount)
{
	if( m_pServer->NameFunction(ID, Name) ) {

		CError *pError = New CError(FALSE);

		if( m_pServer->FindFunction(pError, Name, ID, Type, uCount) ) {

			delete pError;

			return TRUE;
			}

		delete pError;
		}

	return FALSE;
	}

BOOL CSystemLibrary::FindIdent(CString Name, WORD &ID, CTypeDef &Type)
{
	CError *pError = New CError(FALSE);

	if( m_pServer->FindIdent(pError, Name, ID, Type) ) {

		delete pError;

		return TRUE;
		}

	delete pError;

	return FALSE;
	}

void CSystemLibrary::AddCategory(CString Name)
{
	if( m_pLib->FindNamePos(Name) == NOTHING ) {

		CFolderItem *pFolder = New CFolderItem;

		m_pLib->AppendItem(pFolder);

		pFolder->SetName(Name);
		}
	}

// End of File
