
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DataTag_HPP

#define INCLUDE_DataTag_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Tag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Data Tag
//

class CDataTag : public CTag
{
	public:
		// Constructors
		CDataTag(void);

		// Attributes
		BOOL IsArray(void) const;
		UINT GetExtent(void) const;
		BOOL CanWrite(void) const;

		// Operations
		void Force(void);

	protected:
		// Data Members
		UINT m_Extent;

		// Implementation
		BOOL AllowWrite(CDataRef const &Ref, DWORD Data, UINT Type);
		BOOL LocalData(void);
		BOOL IsNumeric(UINT Type);
		BOOL IsInteger(UINT Type);
		BOOL IsString (UINT Type);
	};

// End of File

#endif
