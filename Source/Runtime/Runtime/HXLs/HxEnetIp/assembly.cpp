/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** ASSEMBLY.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** I/O data storage and handling
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

UINT8  inputAssembly[ASSEMBLY_SIZE];	/* Preallocated space to store all input I/O data */
UINT8  outputAssembly[ASSEMBLY_SIZE];   /* Preallocated space to store all output I/O data */

/*---------------------------------------------------------------------------
** assemblyInit( )
**
** Initialize assembly tables
**---------------------------------------------------------------------------
*/
void assemblyInit()
{
	memset( inputAssembly, 0, ASSEMBLY_SIZE );
	memset( outputAssembly, 0, ASSEMBLY_SIZE );
}

/*---------------------------------------------------------------------------
** assemblyResetConnectionSpace( )
**
** Reset connection assembly space to 0
**---------------------------------------------------------------------------
*/
void assemblyResetConnectionSpace( INT32 nConnection )
{		
	memset( &inputAssembly[gConnections[nConnection].cfg.iInputDataOffset], 0, gConnections[nConnection].cfg.iInputDataSize );
	
	/* Reset  outputAssembly only if there is no other multicast open for the same connection point */
	if ( connectionGetAnyMulticastProducer( nConnection ) == INVALID_CONNECTION )
		memset( &outputAssembly[gConnections[nConnection].cfg.iOutputDataOffset], 0, gConnections[nConnection].cfg.iOutputDataSize );
}

/*---------------------------------------------------------------------------
** assemblyGetInputOffsetForConnPoint()
**k
** For the new connection point returns the first unoccupied offset big enough 
** to accomodate a new connection. For an existing connection point return 
** the offset, but make sure that allocated space is sufficient for the new
** connection. Return INVALID_OFFSET if out of space.
**---------------------------------------------------------------------------
*/
INT32 assemblyAssignInputOffsetForConnPoint( INT32 nConnection, INT32 nConnPoint )
{
	INT32  i, j;
	
	gConnections[nConnection].cfg.iInputDataOffset = INVALID_OFFSET;
		
	for ( i = 0; i < ASSEMBLY_SIZE; i+=2 )
	{			
		/* Check if any connection occupies space starting with offset i and ending at (i+gConnections[j].cfg.iInputDataSize) */
		for( j = 0; j < gnConnections; j++ )
		{
			if ( j == nConnection )
				continue;

			/* Check if this connection occupies space starting with offset i and ending at (i+gConnections[j].cfg.iInputDataSize) */
			if ( ( gConnections[j].cfg.iInputDataOffset <= i && (gConnections[j].cfg.iInputDataOffset+gConnections[j].cfg.iInputDataSize) > i ) ||
				 ( gConnections[j].cfg.iInputDataOffset < (i+gConnections[nConnection].cfg.iInputDataSize) && (gConnections[j].cfg.iInputDataOffset+gConnections[j].cfg.iInputDataSize) >= (i+gConnections[nConnection].cfg.iInputDataSize) ) )
			{
				break;
			}
		}

		/* If no connection occipes it, we found an open slot */
		if ( j == gnConnections )
		{
			gConnections[nConnection].cfg.iInputDataOffset = (UINT16)i;
			break;
		}
	}
		
	DumpStr3("assemblyAssignInputOffsetForConnPoint for Connection %d, Connection Point %d assigned Offset 0x%X",
		nConnection, nConnPoint, gConnections[nConnection].cfg.iInputDataOffset );
	return gConnections[nConnection].cfg.iInputDataOffset;
}

/*---------------------------------------------------------------------------
** assemblyGetOutputOffsetForConnPoint()
**
** For the new connection point returns the first unoccupied offset big enough 
** to accomodate a new connection. For an existing connection point return 
** the offset, but make sure that allocated space is sufficient for the new
** connection. Return INVALID_OFFSET if out of space.
**---------------------------------------------------------------------------
*/
INT32 assemblyAssignOutputOffsetForConnPoint( INT32 nConnection, INT32 nConnPoint )
{
	INT32  i, j;
	
	gConnections[nConnection].cfg.iOutputDataOffset = INVALID_OFFSET;
		
	for ( i = 0; i < ASSEMBLY_SIZE; i+=2 )
	{			
		/* Check if any connection occupies space starting with offset i and ending at (i+gConnections[j].cfg.iOutputDataSize) */
		for( j = 0; j < gnConnections; j++ )
		{
			if ( j == nConnection )
				continue;

			if ( ( gConnections[j].cfg.iOutputDataOffset <= i && (gConnections[j].cfg.iOutputDataOffset+gConnections[j].cfg.iOutputDataSize) > i ) ||
				 ( gConnections[j].cfg.iOutputDataOffset < (i+gConnections[nConnection].cfg.iOutputDataSize) && (gConnections[j].cfg.iOutputDataOffset+gConnections[j].cfg.iOutputDataSize) >= (i+gConnections[nConnection].cfg.iOutputDataSize) ) )
			{
				break;
			}
		}

		/* If no connection occipes it, we found an open slot */
		if ( j == gnConnections )
		{
			gConnections[nConnection].cfg.iOutputDataOffset = (UINT16)i;
			break;
		}
	}
	
	DumpStr3("assemblyAssignOutputOffsetForConnPoint for Connection %d, Connection Point %d assigned Offset 0x%X",
		nConnection, nConnPoint, gConnections[nConnection].cfg.iOutputDataOffset );
	return gConnections[nConnection].cfg.iOutputDataOffset;
}

/*---------------------------------------------------------------------------
** assemblyGetInputData( )
**
** Fills the provided buffer with the input assembly data for the particular 
** offset/size combination. pData is the pointer to the buffer. nSize is the 
** buffer length. Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblyGetInputData( UINT8* pData, INT32 nOffset, INT32 nSize )
{			
	if ( ( nOffset + nSize ) > ASSEMBLY_SIZE || nOffset < 0 )
		return 0;
	
	memcpy( pData, &inputAssembly[nOffset], nSize );
		
	return nSize;
}

/*---------------------------------------------------------------------------
** assemblySetInputData( )
**
** Fills the input assembly data with teh provided data for the particular 
** offset/size combination. pData is the pointer to the buffer. nSize is the 
** buffer length. Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblySetInputData( UINT8* pData, INT32 nOffset, INT32 nSize )
{			
	if ( ( nOffset + nSize ) > ASSEMBLY_SIZE || nOffset < 0 )
		return 0;
	
	memcpy( &inputAssembly[nOffset], pData, nSize );
		
	return nSize;
}

/*---------------------------------------------------------------------------
** assemblyGetOutputData( )
**
** Fills the provided buffer from the output assembly.
** Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblyGetOutputData( UINT8* pData, INT32 nOffset, INT32 nSize )
{			
	if ( ( nOffset + nSize ) > ASSEMBLY_SIZE || nOffset < 0 )
		return 0;
	
	memcpy( pData, &outputAssembly[nOffset], nSize );
		
	return nSize;
}

/*---------------------------------------------------------------------------
** assemblySetOutputData( )
**
** Fills the output assembly from the provided buffer.
** Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblySetOutputData( UINT8* pData, INT32 nOffset, INT32 nSize )
{			
	INT32 i, nConnSize, nConnOffset;
	
	if ( ( nOffset + nSize ) > ASSEMBLY_SIZE || nOffset < 0 )
		return 0;
	
	memcpy( &outputAssembly[nOffset], pData, nSize );

	/* Bump the data sequence count up for the affected connections */
	for ( i = 0; i < gnConnections; i++ )
	{
		nConnSize = gConnections[i].cfg.iOutputDataSize;
		nConnOffset = gConnections[i].cfg.iOutputDataOffset;

		if ( ( nConnOffset <= nOffset && (nConnOffset+nConnSize) > nOffset ) || 
			 ( nConnOffset < (nOffset+nSize) && (nConnOffset+nConnSize) >= (nOffset+nSize) ) )
				gConnections[i].iOutDataSeqNbr++;
	}
	
	return nSize;
}

/*---------------------------------------------------------------------------
** assemblyGetConnectionInputData( )
**
** Fills the provided buffer with the input assembly data for the particular 
** connection. pData is the pointer to the buffer. nSize is the size of the 
** buffer. Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblyGetConnectionInputData( INT32 nConnection, UINT8* pData, INT32 nSize )
{			
	INT32 nLen = gConnections[nConnection].cfg.iInputDataSize;
	INT32 nOffset = gConnections[nConnection].cfg.iInputDataOffset;

	if ( nLen > nSize )
		nLen = nSize;

	return assemblyGetInputData( pData, nOffset, nLen );
}

/*-------------------------------------------------------------------------------
** assemblySetConnectionInputData( )
**
** Copies the data from the buffer to the input assembly data for the particular 
** connection. pData is the pointer to the buffer. nSize is the size of the 
** buffer. Returns the actual size of copied data. If data changed notify the 
** client.
**-------------------------------------------------------------------------------
*/
INT32 assemblySetConnectionInputData( INT32 nConnection, UINT8* pData, INT32 nSize )
{			
	INT32 nLen = gConnections[nConnection].cfg.iInputDataSize;
	INT32 nOffset = gConnections[nConnection].cfg.iInputDataOffset;
	
	if ( nLen > nSize )
		nLen = nSize;

	return assemblySetInputData( pData, nOffset, nLen );
}

/*---------------------------------------------------------------------------
** assemblyGetConnectionOutputData( )
**
** Copies the data from the output assembly to the provided buffer for the 
** particular connection. pData is the pointer to the buffer. nSize is the 
** size of the buffer. Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblyGetConnectionOutputData( INT32 nConnection, UINT8* pData, INT32 nSize )
{			
	INT32 nLen = gConnections[nConnection].cfg.iOutputDataSize;
	INT32 nOffset = gConnections[nConnection].cfg.iOutputDataOffset;

	if ( nLen > nSize )
		nLen = nSize;

	return assemblyGetOutputData( pData, nOffset, nLen );
}

/*---------------------------------------------------------------------------
** assemblySetConnectionOutputData( )
**
** Fills the output assembly table with the provided data for the particular 
** connection. pData is the pointer to the buffer. nSize is the size of the 
** buffer. Returns the actual size of copied data.
**---------------------------------------------------------------------------
*/
INT32 assemblySetConnectionOutputData( INT32 nConnection, UINT8* pData, INT32 nSize )
{			
	INT32 nLen = gConnections[nConnection].cfg.iOutputDataSize;
	INT32 nOffset = gConnections[nConnection].cfg.iOutputDataOffset;

	if ( nLen > nSize )
		nLen = nSize;

	gConnections[nConnection].iOutDataSeqNbr++;

	return assemblySetOutputData( pData, nOffset, nLen );
}


/*---------------------------------------------------------------------------
** assemblyParseClassInstanceRequest( )
**
** Determine if it's request for the Class or for the particular instance 
**---------------------------------------------------------------------------
*/

void assemblyParseClassInstanceRequest( INT32 nRequest )
{
	if ( gRequests[nRequest].iInstance == 0 )
		assemblyParseClassRequest( nRequest );
	else
		assemblyParseInstanceRequest( nRequest );
}

/*---------------------------------------------------------------------------
** assemblyParseClassRequest( )
**
** Respond to the class request
**---------------------------------------------------------------------------
*/

void assemblyParseClassRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_GET_ATTR_SINGLE:
			assemblySendClassAttrSingle( nRequest );
			break;
		default:		
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}
}

/*---------------------------------------------------------------------------
** assemblyParseInstanceRequest( )
**
** Respond to the instance request
**---------------------------------------------------------------------------
*/

void assemblyParseInstanceRequest( INT32 nRequest )
{
	switch( gRequests[nRequest].bService )
	{
		case SVC_SET_ATTR_SINGLE:			
			assemblySetInstanceAttrSingle( nRequest );
			break;

		case SVC_SET_MEMBER:
			assemblySetMember( nRequest );
			break;

		case SVC_GET_ATTR_SINGLE:
			assemblySendInstanceAttrSingle( nRequest );
			break;

		case SVC_GET_MEMBER:
			assemblySendMember( nRequest );
			break;

		default:
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_BAD_SERVICE;
			break;
	}	
}

/*--------------------------------------------------------------------------------
** assemblySendClassAttrSingle( )
**
** GetAttributeSingle request has been received for the Assembly object class
**--------------------------------------------------------------------------------
*/

void assemblySendClassAttrSingle( INT32 nRequest )
{
	UINT16 iMaxInstance = MAX_CONNECTIONS * 2;
	UINT16 iRevision = ASSEMBLY_REVISION;
	UINT16 iVal;

	switch( gRequests[nRequest].iAttribute )
	{
		case ASSEMBLY_CLASS_REVISION:			
			iVal = ENCAP_TO_HS(iRevision);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );	
			break;
		case ASSEMBLY_CLASS_MAX_INSTANCE:
			iVal = ENCAP_TO_HS(iMaxInstance);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );	
			break;		
		default:			
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}	
}


/*--------------------------------------------------------------------------------
** assemblySendMember( )
**
** GetMember request has been received for the Assembly object instance
**--------------------------------------------------------------------------------
*/

void assemblySendMember( INT32 nRequest )
{
	UINT8* pData;
	UINT16 iSize = sizeof(UINT16);
	UINT16 iVal;
		
	if ( gRequests[nRequest].iMember == INVALID_MEMBER )
		gRequests[nRequest].iMember = 0;

	switch( gRequests[nRequest].iAttribute )
	{
		case ASSEMBLY_INSTANCE_SIZE:
			iVal = ENCAP_TO_HS(iSize);
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );	
			break;

		case ASSEMBLY_INSTANCE_DATA:
			if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
				pData = &inputAssembly[gRequests[nRequest].iMember];
			else if ( gRequests[nRequest].iInstance == ASSEMBLY_OUTPUT_INSTANCE )
				pData = &outputAssembly[gRequests[nRequest].iMember];
			else
			{
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;						
				break;
			}
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, pData, iSize );				
			break;

		default:			
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}		
}

/*--------------------------------------------------------------------------------
** assemblySendConnectionInstanceAttrSingle( )
**
** GetAttributeSingle request has been received for the Assembly object instance
**--------------------------------------------------------------------------------
*/

void assemblySendInstanceAttrSingle( INT32 nRequest )
{	
	INT32  nConnection;
	UINT16 iSize;
	UINT16 iVal;
	UINT8  data[MAX_REQUEST_DATA_SIZE];
	UINT16 iStartOffset = 0;
	UINT16 iEndOffset = 0;
	BOOL   bFirstConnection = TRUE;

	if ( gRequests[nRequest].iInstance != ASSEMBLY_INPUT_INSTANCE && 
		 gRequests[nRequest].iInstance != ASSEMBLY_OUTPUT_INSTANCE )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;									
		return;
	}

	if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
	{
		for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
		{
			if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			{
				if ( bFirstConnection )
				{
					iStartOffset = gConnections[nConnection].cfg.iInputDataOffset;
					iEndOffset = gConnections[nConnection].cfg.iInputDataOffset+gConnections[nConnection].cfg.iInputDataSize;
					bFirstConnection = FALSE;
				}
				else
				{
					if ( gConnections[nConnection].cfg.iInputDataOffset < iStartOffset )
						iStartOffset = gConnections[nConnection].cfg.iInputDataOffset;

					if ( (gConnections[nConnection].cfg.iInputDataOffset+gConnections[nConnection].cfg.iInputDataSize) > iEndOffset )
						iEndOffset = gConnections[nConnection].cfg.iInputDataOffset+gConnections[nConnection].cfg.iInputDataSize;
				}
			}
		}
	}
	else 
	{
		for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
		{
			if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			{
				if ( bFirstConnection )
				{
					iStartOffset = gConnections[nConnection].cfg.iOutputDataOffset;
					iEndOffset = gConnections[nConnection].cfg.iOutputDataOffset+gConnections[nConnection].cfg.iOutputDataSize;
					bFirstConnection = FALSE;
				}
				else
				{
					if ( gConnections[nConnection].cfg.iOutputDataOffset < iStartOffset )
						iStartOffset = gConnections[nConnection].cfg.iOutputDataOffset;

					if ( (gConnections[nConnection].cfg.iOutputDataOffset+gConnections[nConnection].cfg.iOutputDataSize) > iEndOffset )
						iEndOffset = gConnections[nConnection].cfg.iOutputDataOffset+gConnections[nConnection].cfg.iOutputDataSize;
				}
			}
		}
	}

	iSize = iEndOffset - iStartOffset;

	switch( gRequests[nRequest].iAttribute )
	{
		case ASSEMBLY_INSTANCE_SIZE:
			iVal = ENCAP_TO_HS( iSize );
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, (UINT8*)&iVal, sizeof(UINT16) );	
			break;

		case ASSEMBLY_INSTANCE_DATA:
			if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
				assemblyGetInputData(data, iStartOffset, iSize );					
			else 
				assemblyGetOutputData(data, iStartOffset, iSize );					
			utilResetMemoryPoolOffset( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize, data, iSize );				
			break;

		default:			
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;	
	}
}


/*--------------------------------------------------------------------------------
** assemblySetMember( )
**
** SetMember request has been received for the Assembly object instance
**--------------------------------------------------------------------------------
*/

void assemblySetMember( INT32 nRequest )
{
	UINT8* pDataFrom = MEM_PTR( gRequests[nRequest].iDataOffset);	
	UINT8* pDataTo;
	INT32  nConnection;
	
	if ( gRequests[nRequest].iMember == INVALID_MEMBER )
		gRequests[nRequest].iMember = 0;

	switch( gRequests[nRequest].iAttribute )
	{
		case ASSEMBLY_INSTANCE_DATA:
			if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
			{
				pDataTo = &inputAssembly[gRequests[nRequest].iMember];
				memcpy( pDataTo, pDataFrom, gRequests[nRequest].iDataSize );
				for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
				{
					if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
					{
						if ( (gConnections[nConnection].cfg.iInputDataOffset <= gRequests[nRequest].iMember && 
							  (gConnections[nConnection].cfg.iInputDataOffset+gConnections[nConnection].cfg.iInputDataSize) > gRequests[nRequest].iMember) || 
							 (gConnections[nConnection].cfg.iInputDataOffset < (gRequests[nRequest].iMember+gRequests[nRequest].iDataSize) && 
							  (gConnections[nConnection].cfg.iInputDataOffset+gConnections[nConnection].cfg.iInputDataSize) >= (gRequests[nRequest].iMember+gRequests[nRequest].iDataSize)) )
							notifyEvent( NM_CONNECTION_NEW_INPUT_DATA, gConnections[nConnection].cfg.nInstance );	
					}
				}
			}
			else if ( gRequests[nRequest].iInstance == ASSEMBLY_OUTPUT_INSTANCE )
			{
				pDataTo = &outputAssembly[gRequests[nRequest].iMember];
				memcpy( pDataTo, pDataFrom, gRequests[nRequest].iDataSize );
				for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
				{
					if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
					{
						if ( (gConnections[nConnection].cfg.iOutputDataOffset <= gRequests[nRequest].iMember && 
							  (gConnections[nConnection].cfg.iOutputDataOffset+gConnections[nConnection].cfg.iOutputDataSize) > gRequests[nRequest].iMember) || 
							 (gConnections[nConnection].cfg.iOutputDataOffset < (gRequests[nRequest].iMember+gRequests[nRequest].iDataSize) && 
							  (gConnections[nConnection].cfg.iOutputDataOffset+gConnections[nConnection].cfg.iOutputDataSize) >= (gRequests[nRequest].iMember+gRequests[nRequest].iDataSize)) )
							notifyEvent( NM_CONNECTION_NEW_OUTPUT_DATA, gConnections[nConnection].cfg.nInstance );	
					}
				}
			}
			else
			{
				gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;									
			}				
			break;

		default:			
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;
	}		
	
	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );		
}


/*--------------------------------------------------------------------------------
** assemblySetInstanceAttrSingle( )
**
** SetAttributeSingle request has been received for the Assembly object instance
**--------------------------------------------------------------------------------
*/

void assemblySetInstanceAttrSingle( INT32 nRequest )
{
	INT32  nConnection;
	UINT8* pDataFrom = MEM_PTR( gRequests[nRequest].iDataOffset);	
	UINT16 iStartOffset = 0;
	BOOL   bFirstConnection = TRUE;

	if ( gRequests[nRequest].iInstance != ASSEMBLY_INPUT_INSTANCE && 
		 gRequests[nRequest].iInstance != ASSEMBLY_OUTPUT_INSTANCE )
	{
		gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;									
		return;
	}

	if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
	{
		for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
		{
			if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			{
				if ( bFirstConnection )
				{
					iStartOffset = gConnections[nConnection].cfg.iInputDataOffset;
					bFirstConnection = FALSE;
				}
				else
				{
					if ( gConnections[nConnection].cfg.iInputDataOffset < iStartOffset )
						iStartOffset = gConnections[nConnection].cfg.iInputDataOffset;				
				}				
			}
		}
	}
	else 
	{
		for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
		{
			if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			{
				if ( bFirstConnection )
				{
					iStartOffset = gConnections[nConnection].cfg.iOutputDataOffset;					
					bFirstConnection = FALSE;
				}
				else
				{
					if ( gConnections[nConnection].cfg.iOutputDataOffset < iStartOffset )
						iStartOffset = gConnections[nConnection].cfg.iOutputDataOffset;
				}
			}
		}
	}

	switch( gRequests[nRequest].iAttribute )
	{
		case ASSEMBLY_INSTANCE_DATA:
			if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
				assemblySetInputData(pDataFrom, iStartOffset, gRequests[nRequest].iDataSize );					
			else 
				assemblySetOutputData(pDataFrom, iStartOffset, gRequests[nRequest].iDataSize );								
			break;

		default:			
			gRequests[nRequest].bGeneralError = ROUTER_ERROR_ATTR_NOT_SUPPORTED;
			break;	
	}

	utilRemoveFromMemoryPool( &gRequests[nRequest].iDataOffset, &gRequests[nRequest].iDataSize );		

	if ( !gRequests[nRequest].bGeneralError )
	{
		if ( gRequests[nRequest].iInstance == ASSEMBLY_INPUT_INSTANCE )
		{
			for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
			{
				if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
					notifyEvent( NM_CONNECTION_NEW_INPUT_DATA, gConnections[nConnection].cfg.nInstance );								
			}
		}
		else
		{
			for ( nConnection = 0; nConnection < gnConnections; nConnection++ )
			{
				if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
					notifyEvent( NM_CONNECTION_NEW_OUTPUT_DATA, gConnections[nConnection].cfg.nInstance );								
			}
		}
	}	
}



