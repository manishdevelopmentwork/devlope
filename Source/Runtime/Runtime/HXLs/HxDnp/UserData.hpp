
//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UserData_HPP

#define	INCLUDE_UserData_HPP

//////////////////////////////////////////////////////////////////////////
//
// Headers
//

#include "DnpStack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUserTable;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define typeDouble	addrRealAsReal + 1

//////////////////////////////////////////////////////////////////////////
//
// 64-bit Support
//

union CReal64 {

	unsigned int	i[2];
	double		d;
	};

//////////////////////////////////////////////////////////////////////////
//
// User Data Object 
//

class CUserData
{
	public:

		// Constructor
		CUserData(BYTE bObject, UINT uIndex, BYTE bType, BYTE bMode, ITimeZone * pZone);

		// Destructor
		~CUserData(void);

		// List Access
		BOOL SetList(CUserTable * pList);
		CUserData * GetNext(void)  const;
		CUserData * GetPrev(void)  const;

		// Data Access
		BYTE  GetClass(void);
		BYTE  GetClassMask(void);
		BYTE  MakeClassMask(BYTE bClass);
		BYTE  GetObject(void);
		DWORD GetTimeStamp(void);
		BYTE  GetFlags(void);
		BYTE  GetEventFlags(void);
		UINT  GetIndex(void);
		BYTE  GetMode(void);
		BYTE  GetType(void);
		BOOL  IsEvent(dnpANA * pNew, dnpANA * pDeadband, BOOL fLastEvent);
								
		// System
		BOOL   IsDirty(BYTE bEnum);
		PDWORD GetData(PDWORD pData);
		PDWORD SetData(PDWORD dValue, dnpTIME * pTimeStamp = NULL);
		BOOL   GetEvent(PDWORD pData);
		void   SetEvent(PDWORD pData);
		void   SetFlag(BYTE bFlag);
		BOOL   SetClass(BYTE bClass);
		UINT   GetCtrl(void);
		UINT   GetOnTime(void);
		UINT   GetOffTime(void);
		void   SetCtrl(DWORD dwCtrl);
		void   SetOnTime(DWORD dwOn);
		void   SetOffTime(DWORD dwOff);
		PDWORD SetValue(dnpANA * pVal, PDWORD pData);
		BOOL   MatchType(BYTE bType);
		
		// DNP SCL
		void  GetBinary(dnpUCHAR *f);
		void  GetValue(dnpLONG *pVal, dnpUCHAR *f, dnpTIME * pTimeStamp = NULL);
		void  GetValue(dnpANA *pVal, dnpUCHAR *f);
		BYTE  GetVariation(BYTE bObject = 0);
		void  SetBinary(dnpUCHAR f, dnpBOOL isE = FALSE, dnpTIME * pTS = NULL);
		void  SetValue(dnpWORD Val,  dnpTIME *pTS = NULL, dnpUCHAR f = 0, dnpBOOL isE = 0);
		void  SetValue(dnpLONG Val,  dnpTIME *pTS = NULL, dnpUCHAR f = 0, dnpBOOL isE = 0);
		void  SetValue(dnpANA *pVal, dnpTIME *pTS = NULL, dnpUCHAR f = 0, dnpBOOL isE = 0);
		void  SetOperate(dnpANA *pVal, dnpTIME * pTS = NULL, dnpUCHAR f = 0, dnpBOOL isE = 0);
		void  SetOperate(dnpUCHAR f, dnpBOOL isE = FALSE, dnpTIME * pTS = NULL);

	protected:

		// Data Members
		BYTE	 m_bObject;
		UINT	 m_uIndex;
		DWORD	 m_Value[MAX_VAR];
		DWORD    m_Event[MAX_VAR];
		BYTE	 m_bStatus;
		UINT	 m_TimeStamp;
		DWORD	 m_IIN;
		BYTE	 m_Type;
		BOOL	 m_fDirty[dirtyDat];
		UINT     m_uLast;
		UINT     m_Ctrl;
		UINT     m_On;
		UINT     m_Off;
		BYTE	 m_bClass;
		BYTE     m_bDataType;
		BYTE	 m_bMode;
				
		CUserTable * m_pList;
		CUserData * m_pPrev;
		CUserData * m_pNext;
		ITimeZone * m_pZone;
		
		// Synchronization
		IMutex * m_pMutex;
		
		// Implementation
		void SetTimeStamp(dnpTIME * pTime);
		void SetFlags(dnpUCHAR flag);
		BOOL IsEvent(void);
		BOOL IsDouble(void);
		BOOL IsDoubleData(void);
		BOOL IsWordData(void);
		BOOL IsRealData(void);
		BOOL IsLongData(void);
		BOOL IsRealEvent(dnpANA * pNew, dnpANA * pDeadband, dnpANA * pCurrent);
												
		// Friends
		friend class CUserTable;

		// Debugging Help
		void PrintData(void);

	};

// End of File

#endif
