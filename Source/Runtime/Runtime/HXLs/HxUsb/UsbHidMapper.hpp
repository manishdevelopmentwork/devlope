
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHidMapper_HPP

#define	INCLUDE_UsbHidMapper_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hid Framework
//

#include "Hid.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hid Mapper
//

class CUsbHidMapper : public CUsbDriver, public IUsbHidMapper
{
	public:
		// Constructor
		CUsbHidMapper(void);

		// Destructor
		~CUsbHidMapper(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHidMapper
		void METHOD Bind(IUsbHostHid *pHost);
		BOOL METHOD SetReport(PCBYTE pReport, UINT uSize);
		UINT METHOD GetUsagePage(void);
		UINT METHOD GetUsageType(void);
		UINT METHOD GetRecvSize(UINT uId);
		UINT METHOD GetSendSize(UINT uId);
		void METHOD SetConfig(PCBYTE pConfig, UINT uSize);
		void METHOD Poll(void);

	protected:
		// Context
		struct CContext
		{
			UINT	m_uRepSize;
			UINT	m_uRepCount;
			UINT	m_uUsagePage;
			UINT	m_uUsage;
			};

		// Data
		IUsbHostHid * m_pHostDriver;
		UINT          m_uSendSize;
		UINT          m_uRecvSize;
		PBYTE         m_pRecvBuff;
		PBYTE         m_pSendBuff;
		bool	      m_fPolling;
		
		// Helpers
		bool RecvReport(void);
		bool SendReport(void);
		
		// Parsers
		void ParseReportDesc(PCBYTE pData, UINT uSize);
		void ParseMain(HidShort const &Item, CContext &Ctx);
		void ParseGlobal(HidShort const &Item, CContext &Ctx);
		void ParseLocal(HidShort const &Item, CContext &Ctx);
		
		// Buffers
		void InitBuffers(void);
		void FreeBuffers(void);
	};

// End of File

#endif
