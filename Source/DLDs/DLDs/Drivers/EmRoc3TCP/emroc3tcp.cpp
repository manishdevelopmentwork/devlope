#include "intern.hpp"

#include "emroc3tcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC 3 Communications Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

/////////////////////////////////////////////////////////////////////////
//
// Emerson ROC 3 Master TCP Driver
//

// Instantiator

INSTANTIATE(CEmRoc3MasterTCPDriver);

// Constructor

CEmRoc3MasterTCPDriver::CEmRoc3MasterTCPDriver(void)
{
	m_Ident  = DRIVER_ID; 

	m_bGroup = 2;

	m_bUnit	 = 1;

	m_TxPtr	 = 0;

	m_RxPtr  = 0;

	m_uKeep  = 0;

	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));
	}

// Destructor

CEmRoc3MasterTCPDriver::~CEmRoc3MasterTCPDriver(void)
{
	}

// Configuration

void MCALL CEmRoc3MasterTCPDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bGroup   = GetByte(pData);

		m_bUnit    = GetByte(pData);
		}
	}

	
void MCALL CEmRoc3MasterTCPDriver::CheckConfig(CSerialConfig &Config)
{	
	}
	
// Management

void MCALL CEmRoc3MasterTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEmRoc3MasterTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmRoc3MasterTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bGroup  = GetByte(pData);
			
			m_pCtx->m_bUnit	  = GetByte(pData);

			m_pCtx->m_bLast   = 0xFF;

			m_pCtx->m_uLast   = 0;
			
			m_pCtx->m_uTime   = 0;

			m_pCtx->m_uPoll   = ToTicks(GetWord(pData));

			m_pCtx->m_Pass    = GetWord(pData);

			m_pCtx->m_Use     = GetByte(pData);

			m_pCtx->m_Acc	  = GetByte(pData);

			m_pCtx->m_Inc	  = GetByte(pData);

			m_pCtx->m_Ping	  = GetLong(pData);

			m_pCtx->m_OpID    = GetString(pData);

			m_pCtx->m_Logon   = FALSE;

			m_pCtx->m_uSlots  = GetLong(pData);

			m_pCtx->m_pSlots  = new CSlot[m_pCtx->m_uSlots];

			for( UINT u = 0; u < m_pCtx->m_uSlots; u++ ) {

				CSlot * pSlot   = &m_pCtx->m_pSlots[u];

				pSlot->m_Slot   = GetLong(pData);

				pSlot->m_Target = GetLong(pData);

				pSlot->m_Extent = GetWord(pData);

				pSlot->m_Time   = 0;

				memset(pSlot->m_Data, 0, elements(pSlot->m_Data) * sizeof(DWORD));
				}

			m_pCtx->m_IP1    = GetAddr(pData);

			m_pCtx->m_IP2    = GetAddr(pData);

			m_pCtx->m_uPort  = GetWord(pData);

			m_pCtx->m_fKeep  = GetByte(pData);

			m_pCtx->m_fPing  = GetByte(pData);

			m_pCtx->m_uTime1 = GetWord(pData);

			m_pCtx->m_uTime2 = GetWord(pData);

			m_pCtx->m_uTime3 = GetWord(pData);

			m_pCtx->m_pSock  = NULL;

			m_pCtx->m_fDirty = FALSE;

			m_pCtx->m_fAux   = FALSE;

			pDevice->SetContext(m_pCtx);

			m_pBase = m_pCtx;

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmRoc3MasterTCPDriver::DeviceClose(BOOL fPersist)
{	
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx->m_pSlots;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmRoc3MasterTCPDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {
		
		DWORD IP;

		if( !m_pCtx->m_fAux ) {

			IP = m_pCtx->m_IP1;
			}
		else
			IP = m_pCtx->m_IP2;
		
		if( CheckIP(IP, m_pCtx->m_uTime2) == NOTHING ) {
			
			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			return CCODE_ERROR; 
			}
		}

	if( OpenSocket() ) {

		return CEmRoc3MasterBaseDriver::Ping();
		}

	return CCODE_ERROR;
	}

// User Access

UINT MCALL CEmRoc3MasterTCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 5 || uFunc == 9 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		if( uFunc == 5 ) {

			pCtx->m_IP1 = MotorToHost(dwValue);
			}

		if( uFunc == 9 ) {

			pCtx->m_IP2 = MotorToHost(dwValue);
			}

		pCtx->m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		} 
	
	if( uFunc == 6 )  {
		
		// Set Target Port

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			pCtx->m_uPort  = uValue;

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 7 )  { 
		
		// Set Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_bUnit  = BYTE(uValue);

			pCtx->m_fDirty = TRUE;

			return 1;
			}
		}

	if( uFunc == 8 ) {
		
		// Get Current IP Address

		return MotorToHost(pCtx->m_IP1);
		}
	
	if( uFunc == 10 ) {
		
		// Get Current IP Address
		
		return MotorToHost(pCtx->m_IP2);
		}
		
	if( uFunc == 11 ) {
		
		//Get Fallback Status

		return pCtx->m_fAux;
		}
	
	return CEmRoc3MasterBaseDriver::DevCtrl(pContext, uFunc, Value);
	}

BOOL CEmRoc3MasterTCPDriver::Send(void)
{
	if( OpenSocket() ) {

		UINT uSize = m_TxPtr;

		if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

			if( uSize == m_TxPtr ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CEmRoc3MasterTCPDriver::Recv(void)
{
	if( OpenSocket() ) {

		m_CRC.Clear();

		m_RxPtr = 0;

		SetTimer(m_pCtx->m_uTime2);

		while( GetTimer() ) {

			UINT uSize = sizeof(m_bRx) - m_RxPtr;

			if( m_pCtx->m_pSock->Recv(m_bRx + m_RxPtr, uSize) == S_OK ) {

				if( uSize ) {

					m_RxPtr += uSize;

					if( m_RxPtr >= 6 ) {

						UINT uTotal = m_bRx[5] + 8;

						if( m_RxPtr >= uTotal ) {

							if( CheckReply() ) {

								for( UINT n = 0; n < m_RxPtr; n++ ) {

									AddCheck(m_bRx[n]);
									}

								return CheckCRC();
								}

							if( m_RxPtr -= uTotal ) {

								for( UINT n = 0; n < m_RxPtr; n++ ) {

									m_bRx[n] = m_bRx[uTotal++];
									}
								}
							}
						}

					continue;
					}
				}

			if( !CheckSocket() ) {

				return FALSE;
				}

			Sleep(10);
			}
		}

	return FALSE;
	}

// Implementation

BOOL CEmRoc3MasterTCPDriver::CheckFrame(void)
{
	if( m_bRx[4] == 0xFF ) {

		// ERROR

		switch( m_bRx[6] ) {

			case 20:
			case 21:
			case 63:

				m_pBase->m_Logon = FALSE;
				break;
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CEmRoc3MasterTCPDriver::CheckReply(void)
{
	if( m_bRx[0] != m_bUnit	) {

		return FALSE;
		}

	if( m_bRx[1] != m_bGroup ) {

		return FALSE;
		}

	if( m_bRx[2] != m_pBase->m_bUnit ) {

		return FALSE;
		}

	if( m_bRx[3] != m_pBase->m_bGroup ) {

		return FALSE;
		}

	return TRUE;
	}

// Socket Management

BOOL CEmRoc3MasterTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}

			return TRUE;
			}

		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CEmRoc3MasterTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		m_pCtx->m_fAux   = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR IP;

		WORD   Port;

		if( !m_pCtx->m_fAux ) {

			IP   = (IPADDR const &) m_pCtx->m_IP1;

			Port = WORD(m_pCtx->m_uPort);
			}
		else {
			IP   = (IPADDR const &) m_pCtx->m_IP2;

			Port = WORD(m_pCtx->m_uPort);
			}

		if( m_pCtx->m_pSock->Connect(IP, Port) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			if( m_pCtx->m_IP2 ) {

				m_pCtx->m_fAux = !m_pCtx->m_fAux;
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		if( m_pCtx->m_IP2 ) {

			m_pCtx->m_fAux = !m_pCtx->m_fAux;
			}

		return FALSE;
		}

	return FALSE;
	}

void CEmRoc3MasterTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Helpers

BOOL CEmRoc3MasterTCPDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CEmRoc3MasterTCPDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CEmRoc3MasterTCPDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
