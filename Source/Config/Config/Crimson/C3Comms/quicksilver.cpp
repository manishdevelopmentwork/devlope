
#include "intern.hpp"

#include "quicksilver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CQuicksilverDriverOptions, CUIItem);

// Constructor

CQuicksilverDriverOptions::CQuicksilverDriverOptions(void)
{
	m_Group	= 255;
	}

// UI Management

void CQuicksilverDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CQuicksilverDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Group));

	return TRUE;
	}

// Meta Data Creation

void CQuicksilverDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	}

//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CQuicksilverDeviceOptions, CUIItem);

// Constructor

CQuicksilverDeviceOptions::CQuicksilverDeviceOptions(void)
{
	m_Address	= 16;
	}

// UI Management

void CQuicksilverDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CQuicksilverDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Address));

	return TRUE;
	}

// Meta Data Creation

void CQuicksilverDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Address);
	}

//////////////////////////////////////////////////////////////////////////
//
// Quicksilver Comms Driver
//

// Instantiator

ICommsDriver *	Create_QuicksilverDriver(void)
{
	return New CQuicksilverDriver;
	}

// Constructor

CQuicksilverDriver::CQuicksilverDriver(void)
{
	m_wID		= 0x3356;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Quicksilver";
	
	m_DriverName	= "Quicksilver Master";
	
	m_Version	= "1.20";
	
	m_ShortName	= "Quicksilver Master";

	AddSpaces();
	}

// Binding Control

UINT	CQuicksilverDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CQuicksilverDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 57600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CQuicksilverDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CQuicksilverDriverOptions);
	}

CLASS CQuicksilverDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CQuicksilverDeviceOptions);
	}

// Address Management

BOOL CQuicksilverDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CQuicksilverDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL	CQuicksilverDriver::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart, CItem *pDev)
{
	CQuicksilverDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));	
	}

BOOL CQuicksilverDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace ) return FALSE;

	if( pSpace->m_uTable < USRMIN || pSpace->m_uTable >= USRMAX ) {

		return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
		}

	Addr.a.m_Table  = pSpace->m_uTable;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrLongAsLong;

	return TRUE;
	}

BOOL CQuicksilverDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( !pSpace ) return FALSE;

	if( Addr.a.m_Table < USRMIN || Addr.a.m_Table >= USRMAX ) {

		return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
		}

	Text.Printf("%s", pSpace->m_Prefix);

	return TRUE;
	}

// Implementation

void	CQuicksilverDriver::AddSpaces(void)
{
	AddSpace( New CSpace(ALLHDR, "", "   SHOW ALL HEADERS...",			10,   0, 0,	LL) );
	AddSpace( New CSpace(GENHDR, "", "Commands with Parameters...",			10,   0, 0,	LL) );
	AddSpace( New CSpace(1,  "CIO",  "Configure I/O Setting",			10,   1, 7,	LL) );
	AddSpace( New CSpace(5,  "CII",  "Configure I/O Setting, Immediate",		10,   1, 7,	LL) );
	AddSpace( New CSpace(6,  "CIE",  "Configure Extended I/O",			10, 101, 116,	LL) );
	AddSpace( New CSpace(7,  "CIIE", "Configure Extended I/O, Immediate",		10, 101, 116,	LL) );
	AddSpace( New CSpace(2,  "RPB",  "READ 1 Word from Program Buffer",		10,   0, 65535,	LL) );
	AddSpace( New CSpace(3,  "REG",  "Register, Immediate (RRG/WRI)",		10,   0, 767,	LL) );
	AddSpace( New CSpace(4,  "WRP",  "Write Register - Program Type",		10,   0, 767,	LL) );
	AddSpace( New CSpace(addrNamed,  "ERR", "NAK Response",				10, QERR, 0,	LL) );

	AddSpace( New CSpace(USRHDR, "", "Custom User Commands...",			10,   0, 0,	LL) );
	AddSpace( New CSpace(28, "_CW1", "Command String 1 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(29, "_RW1", "String 1 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(30, "_CW2", "Command String 2 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(31, "_RW2", "String 2 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(32, "_CW3", "Command String 3 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(33, "_RW3", "String 3 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(34, "_CW4", "Command String 4 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(35, "_RW4", "String 4 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(36, "_CW5", "Command String 5 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(37, "_RW5", "String 5 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(38, "_CW6", "Command String 6 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(39, "_RW6", "String 6 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(40, "_CW7", "Command String 7 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(41, "_RW7", "String 7 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(42, "_CW8", "Command String 8 (Cmd Number data data...)",	10,   0, 10,	LL) );
	AddSpace( New CSpace(43, "_RW8", "String 8 Response",				10,   0, 10,	LL) );
	AddSpace( New CSpace(44, "_SEND","Send Command String N",			10,   0, 0,	LL) );
	AddSpace( New CSpace(20, "_CRA", "Continuous Read-String A (Cmd# data data...)",10,   0, 10,	LL) );
	AddSpace( New CSpace(21, "_RRA", "Read String A Response",			10,   0, 10,	LL) );
	AddSpace( New CSpace(22, "_CRB", "Continuous Read-String B (Cmd# data data...)",10,   0, 10,	LL) );
	AddSpace( New CSpace(23, "_RRB", "Read String B Response",			10,   0, 10,	LL) );
	AddSpace( New CSpace(24, "_CRC", "Continuous Read-String C (Cmd# data data...)",10,   0, 10,	LL) );
	AddSpace( New CSpace(25, "_RRC", "Read String C Response",			10,   0, 10,	LL) );
	AddSpace( New CSpace(26, "_CRD", "Continuous Read-String D (Cmd# data data...)",10,   0, 10,	LL) );
	AddSpace( New CSpace(27, "_RRD", "Read String D Response",			10,   0, 10,	LL) );

	AddSpace( New CSpace(AHDR, "", "Commands - A...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ACR", "Set Analog Continuous Read",		10,   5, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ACRC", "  ...Analog Channel Number",		10,   6, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ACRD", "  ...Data Register",			10,   7, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ADL",  "ACK Delay",				10,   8, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ADX",  "ACK Delay Extended",			10, 347, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "AHC", "Set Anti-Hunt Constants",		10,   9, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "AHCOC","  ...Out: Open->Closed",		10,  10, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "AHCCO","  ...Into: Closed->Open",		10,  11, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "AHD",  "Anti-Hunt Delay",			10,  12, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "AHM",  "Anti-Hunt Mode",			10, 276, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ARI",  "Analog Read Input",			10,  13, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ARIC", "  ...Analog Channel Number",		10,  14, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ARID", "  ...Data Register",			10,  15, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ATR",  "Add to Register",			10,  16, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ATRA", "  ...Data Register",			10,  17, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ATRD", "  ...Data To Add",			10,  18, 0,	LL) );

	AddSpace( New CSpace(CHDR, "", "Commands - C...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CER",  "Command Error Recovery",		10, 348, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CIS",  "Clear Internal Status",		10,  19, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CKS",  "Check Internal Status",		10,  20, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CKSE", "  ...Condition Enable",		10,  21, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CKSS", "  ...Condition State",			10,  22, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLCA", "Calculation (Not SilverMax)",		10, 306, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLC1", "  ...Operation",			10, 307, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLC2", "  ...Data Register",			10, 308, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLC",  "Calculation (SilverMax Only)",		10,  23, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLCO", "  ...Operation",			10,  24, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLCD", "  ...Data Register",			10,  25, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLD",  "Calculation, Extended with Data",	10, 309, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLDR", "  ...Register",			10, 310, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLDD", "  ...Data",				10, 311, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLDO", "  ...Operation",			10, 312, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLDA", "  ...Result Register",			10, 313, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLM",  "Control Loop Mode",			10, 277, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLX",  "Calculation, Extended",		10, 314, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLX1", "  ...Register 1",			10, 315, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLX2", "  ...Register 2",			10, 316, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLCO", "  ...Operation",			10, 317, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CLXA", "  ...Result Register",			10, 318, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CME",  "Clear Max Error",			10,  26, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "COB",  "Clear Output Bit",			10,  27, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CPL",  "Clear Poll",				10,  28, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTC",  "Control Constants",			10,  29, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTC1", "  ...Velocity 1 Feedback Gain",	10,  30, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTC2", "  ...Velocity 2 Feedback Gain",	10,  31, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTCV", "  ...Velocity Feedforward Gain",	10,  32, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTCB", "  ...Acceleration Feedback Gain",	10,  33, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTCF", "  ...Acceleration Feedforward Gain",	10,  34, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTCP", "  ...Proportional Gain",		10,  35, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "CTCI", "  ...Integrator Gain",			10,  36, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2T",  "Control Constants 2",			10, 351, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2T1", "  ...Velocity 1 Feedback Gain",	10, 352, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2T2", "  ...Velocity 2 Feedback Gain",	10, 353, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2TV", "  ...Velocity Feedforward Gain",	10, 354, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2TB", "  ...Acceleration 1 Feedback Gain",	10, 355, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2TC", "  ...Acceleration 2 Feedback Gain",	10, 356, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2TF", "  ...Acceleration Feedforward Gain",	10, 357, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2TP", "  ...Proportional Gain",		10, 358, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "C2TI", "  ...Integrator Gain",			10, 359, 0,	LL) );
// following is the same as the CLC command
//	AddSpace( New CSpace(addrNamed, "CTW",  "Calculation, Two Word",		10, 319, 0,	LL) );
//	AddSpace( New CSpace(addrNamed, "CTWO", "  ...Operation",			10, 320, 0,	LL) );
//	AddSpace( New CSpace(addrNamed, "CTWR", "  ...Register",			10, 321, 0,	LL) );

	AddSpace( New CSpace(DHDR, "", "Commands - D...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DEM",  "Disable Encoder Monitor",		10,  37, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DDB",  "Disable Done Bit",			10,  38, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DIF",  "Digital Input Filter",			10,  39, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DIFL", "  ...I/O Line Number",			10,  40, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DIFF", "  ...Filter Constant",			10,  41, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DIR",  "Set Direction",			10,  42, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DLC",  "Dual Loop Control",			10,  43, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DLY",  "Delay",				10,  44, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DMD",  "Disable Motor Driver",			10,  45, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "DMT",  "Disable Multi-Tasking",		10,  46, 0,	LL) );

	AddSpace( New CSpace(EHDR, "", "Commands - E...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EDH",  "Enable Done High",			10,  47, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EDL",  "Enable Done Low",			10,  48, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EEM",  "Enable Encoder Monitor",		10,  49, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EMD",  "Enable Motor Driver",			10,  50, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EMN",  "Encoder Monitor",			10, 363, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EMNM", "  ...Mode",				10, 364, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EMNI", "  ...Index State",			10, 365, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EMNR", "  ...Reserved",			10, 366, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "EMT",  "Enable Multi-Tasking",			10,  51, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "END",  "End Program",				10,  52, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ERL",  "Error Limits",				10,  53, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ERLM", "  ...Moving limit",			10,  54, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ERLH", "  ...Hold Limit",			10,  55, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ERLD", "  ...Delay to Holding",		10,  56, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ELR",  "Error Limits, Remote (ERR)",		10, 367, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ELRP", "  ...Remote Position Register",	10, 368, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ELRE", "  ...Maximum Starting Error",		10, 369, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN",  "End of Travel, Negative",		10, 370, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN1", "  ...Enable ISW",			10, 371, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN2", "  ...State ISW",			10, 372, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN3", "  ...Enable IS2",			10, 373, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN4", "  ...State IS2",			10, 374, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN5", "  ...Enable XIO",			10, 375, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETN6", "  ...State XIO",			10, 376, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP",  "End of Travel, Positive",		10, 377, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP1", "  ...Enable ISW",			10, 378, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP2", "  ...State ISW",			10, 379, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP3", "  ...Enable IS2",			10, 380, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP4", "  ...State IS2",			10, 381, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP5", "  ...Enable XIO",			10, 382, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ETP6", "  ...State XIO",			10, 383, 0,	LL) );

	AddSpace( New CSpace(FHDR, "", "Commands - F...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "FLC",  "Filter Constants",			10,  57, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "FLC1", "  ...Velocity 1 Feedback",		10,  58, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "FLC2", "  ...Velocity 2 Feedback",		10,  59, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "FLCA", "  ...Acceleration Feedback",		10,  60, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "FL2",  "Filter Constants 2",			10, 384, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "F2LD", "  ...Kd:  Damping",			10, 385, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "F2LS", "  ...Ksi: Stiffness Per Inertia",	10, 386, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "F2LA", "  ...Kaa: Anticipated Acceleration",	10, 387, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "F2LV", "  ...Velocity 2 Feedback",		10, 388, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "F2L1", "  ...Acceleration 1 Feedback",		10, 389, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "F2L2", "  ...Acceleration 2 Feedback",		10, 390, 0,	LL) );

	AddSpace( New CSpace(GHDR, "", "Commands - G...",				10,   0, 0,	LL) );	
	AddSpace( New CSpace(addrNamed, "GCL",  "Go Closed Loop",			10,  61, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "GOC",  "Gravity Offset Constant",		10,  62, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "GOP",  "Go Open Loop",				10,  63, 0,	LL) );

	AddSpace( New CSpace(HHDR, "", "Commands - H...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "HLT",  "Halt",					10, 346, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "HSM",  "Hard Stop Move",			10,  64, 0,	LL) );

	AddSpace( New CSpace(IHDR, "", "Commands - I...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IDT", 	"Identity",				10,  65, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IDTG", "  ...Group Number",			10,  66, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IDTU", "  ...Unit Number",			10,  67, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMQ",  "Interpolated Move Queue Clear",	10, 281, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMS",  "Interpolated Move Start",		10, 282, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMW",  "Interpolated Move Write Queue",	10, 391, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMWT", "  ...Time",				10, 392, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMWP", "  ...Position",			10, 393, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMWA", "  ...Acceleration",			10, 394, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "IMWV", "  ...Velocity",			10, 395, 0,	LL) );

	AddSpace( New CSpace(JHDR, "", "Commands - J...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JAN",  "Jump on Inputs Anded",			10,  68, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JANE", "  ...IOS Condition Enable",		10,  69, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JANS", "  ...IOS Condition State",		10,  70, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JANL", "  ...Program Buffer Location",		10,  71, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JMP",  "Jump",					10,  72, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JMPE", "  ...Condition Enable",		10,  73, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JMPS", "  ...Condition State",			10,  74, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JMPA", "  ...Command Buffer Address",		10,  75, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JNA",  "Jump on Inputs Nanded",		10,  76, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JNAE", "  ...IOS Condition Enable",		10,  77, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JNAS", "  ...IOS Condition State",		10,  78, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JNAL", "  ...Program Buffer Location",		10,  79, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JOI",  "Jump on Input",			10,  80, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JOIC", "  ...I/O Code Number",			10,  81, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JOIS", "  ...I/O State",			10,  82, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JOIA", "  ...Command Buffer Address",		10,  83, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JOR",  "Jump on Inputs - Ored",		10,  84, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JORE", "  ...IOS Condition Enable",		10,  85, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JORS", "  ...IOS Condition State",		10,  86, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "JORL", "  ...Program Buffer Location",		10,  87, 0,	LL) );

	AddSpace( New CSpace(KHDR, "", "Commands - K...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KDD",  "Kill Disable Driver",			10,  88, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KED",  "Kill Enable Driver",			10,  89, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMC",  "Kill Motor Conditions",		10,  90, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMCE", "  ...Condition Enable",		10,  91, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMCS", "  ...Condition State",			10,  92, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMR",  "Kill Motor Recovery",			10,  93, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX",  "Kill Motor Conditions, Extended",	10, 289, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX1", "  ...Condition Enable ISW",		10, 290, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX2", "  ...Condition State ISW",		10, 291, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX3", "  ...Condition Enable IS2",		10, 292, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX4", "  ...Condition State IS2",		10, 293, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX5", "  ...Condition Enable XIO",		10, 294, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "KMX6", "  ...Condition State XIO",		10, 295, 0,	LL) );

	AddSpace( New CSpace(LHDR, "", "Commands - L...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "LPR",  "Load Program",				10,  94, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "LPRA", "  ...NV Memory Address",		10,  95, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "LPRC", "  ...Count",				10,  96, 0,	LL) );
 	AddSpace( New CSpace(addrNamed, "LRP",  "Load and Run Program",			10,  97, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "LVP",  "Low Voltage Processor Trip",		10, 278, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "LVT",  "Low Voltage Trip",			10,  98, 0,	LL) );

	AddSpace( New CSpace(MHDR, "", "Commands - M...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAT",  "Move Absolute - Time",			10,  99, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MATP", "  ...Position",			10, 100, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MATA", "  ...Acceleration Time",		10, 101, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MATT", "  ...Total Time",			10, 102, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MATE", "  ...Stop Enable",			10, 103, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MATS", "  ...Stop State",			10, 104, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAV",  "Move Absolute - Velocity",		10, 105, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAVP", "  ...Position",			10, 106, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAVA", "  ...Acceleration",			10, 107, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAVV", "  ...Velocity",			10, 108, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAVE", "  ...Stop Enable",			10, 109, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MAVS", "  ...Stop State",			10, 110, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "MCT",  "Motor Constants",			10, 296, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT1", "  ...Constant 1",			10, 297, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT2", "  ...Constant 2",			10, 298, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT3", "  ...Constant 3",			10, 299, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT4", "  ...Constant 4",			10, 300, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT5", "  ...Constant 5",			10, 301, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT6", "  ...Constant 6",			10, 302, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT7", "  ...Constant 7",			10, 303, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MCT8", "  ...Constant 8",			10, 304, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "MDC",  "Modulo Clear",				10, 111, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MDS",  "Modulo Set",				10, 112, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MDSC", "  ...Count",				10, 113, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MDSE", "  ...Encoder Source",			10, 114, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MDSF", "  ...Output Format",			10, 115, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MDT",  "Modulo Trigger",			10, 116, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "MRT",  "Move Relative - Time",			10, 117, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRTD", "  ...Distance",			10, 118, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRTA", "  ...Acceleration Time",		10, 119, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRTT", "  ...Total Time",			10, 120, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRTE", "  ...Stop Enable",			10, 121, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRTS", "  ...Stop State",			10, 122, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRV",  "Move Relative - Velocity",		10, 123, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRVD", "  ...Distance",			10, 124, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRVA", "  ...Acceleration",			10, 125, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRVV", "  ...Velocity",			10, 126, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRVE", "  ...Stop Enable",			10, 127, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MRVS", "  ...Stop State",			10, 128, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "MTT",  "Maximum Temperature Trip",		10, 129, 0,	LL) );

	AddSpace( New CSpace(OHDR, "", "Commands - O...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "OLP",  "Open Loop Phase",			10, 130, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "OVT",  "Over Voltage Trip",			10, 131, 0,	LL) );

	AddSpace( New CSpace(PHDR, "", "Commands - P...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PAC",  "Set Phase Advance Constants",		10, 132, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PAC1A", "  ...Phase Advance-P_adv",		10, 133, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PAC2A", "  ...Phase Advance-P2_adv",		10, 134, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PACL",  "  ...Phase Advance-P_limit",		10, 135, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCG",  "Pre-Calculate Go",			10, 136, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCI",  "Program Call on Input",		10, 137, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCIL", "  ...Program Buffer Location",		10, 138, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCIE", "  ...I/O Enable",			10, 139, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCIS", "  ...I/O State",			10, 140, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCL",  "Program Call",				10, 141, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCLL", "  ...Program Buffer Location",		10, 142, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCLE", "  ...Condition Enable",		10, 143, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCLS", "  ...Condition State",			10, 144, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "PCM",  "Pre-calculated Move",			10, 145, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PCP",  "Position Compare",			10, 146, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PIM",  "Position Input Mode",			10, 147, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PIMF", "  ...Filter Constant",			10, 148, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PIME", "  ...I/O Exit Enable",			10, 149, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PIMS", "  ...I/O Exit State",			10, 150, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PLG",	"DISPLAY Lowest Group Number",		10, 151, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PLR",  "Power Low Recovery",			10, 152, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMC",  "Profile Move Continuous",		10, 153, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMCE", "  ...Stop Enable",			10, 154, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMCS", "  ...Stop State",			10, 155, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMO",  "Profile Move Override",		10, 156, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMOE", "  ...Stop Enable",			10, 157, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMOS", "  ...Stop State",			10, 158, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "PMV",  "Profile Move",				10, 159, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMVE", "  ...Stop Enable",			10, 160, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMVS", "  ...Stop State",			10, 161, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PMX",  "Profile Move Exit",			10, 162, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "POL",  "READ Polling Status Word",		10, 163, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PRI",  "Program Return on Input",		10, 164, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PRIE", "  ...I/O Enable",			10, 165, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PRIS", "  ...I/O State",			10, 166, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PRT",  "Program Return",			10, 167, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PRTE", "  ...Condition Enable",		10, 168, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PRTS", "  ...Condition State",			10, 169, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PUN",	"Access Unit Number",			10, 170, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PUP",  "Protect User Program",			10, 339, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PUPC", "  ...Lockout Code",			10, 340, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PUPA", "  ...First Memory Address",		10, 341, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PVC",  "Profile Velocity Continuous",		10, 396, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PVCM", "  ...Mode",				10, 397, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PVCR", "  ...Starting Data Register",		10, 398, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PVCE", "  ...Stop Enable",			10, 399, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PVCS", "  ...Stop State",			10, 400, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PWO",  "PWM Output",				10, 286, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PWOR", "  ...Register",			10, 287, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "PWOM", "  ...Mode",				10, 288, 0,	LL) );

	AddSpace( New CSpace(RHDR, "", "Commands - R...",				10,   0, 0,	LL) );	
	AddSpace( New CSpace(addrNamed, "RAT",  "Register Move Absolute - Time",	10, 171, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RATD", "  ...Data Register",			10, 172, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RATA", "  ...Acceleration Time",		10, 173, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RATT", "  ...Total Time",			10, 174, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RATE", "  ...Stop Enable",			10, 175, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RATS", "  ...Stop State",			10, 176, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RAV",  "Register Move Absolute - Velocity",	10, 177, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RAVD", "  ...Data Register",			10, 178, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RAVA", "  ...Acceleration",			10, 179, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RAVV", "  ...Velocity",			10, 180, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RAVE", "  ...Stop Enable",			10, 181, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RAVS", "  ...Stop State",			10, 182, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "RGG",  "Registered Electronic Gearing (REG)",	10, 401, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RGGM", "  ...Mode",				10, 402, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RGGR", "  ...Starting Data Register",		10, 403, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RGGC", "  ...Cycle Count",			10, 404, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RIO",  "READ I/O Status",			10, 183, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RIS",  "READ Internal Status Word",		10, 184, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLM",  "Register Load Multiple",		10, 185, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLMN", "  ...Number of Registers",		10, 186, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLMD", "  ...Starting Data Register",		10, 187, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLMA", "  ...NV Memory Address",		10, 188, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLN",  "Register Load from non-volatile",	10, 189, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLND", "  ...Data Register",			10, 190, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RLNA", "  ...NV Memory Address",		10, 191, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "RRT",  "Register Move Relative - Time",	10, 192, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRTD", "  ...Data Register",			10, 193, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRTA", "  ...Acceleration",			10, 194, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRTT", "  ...Total Time",			10, 195, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRTE", "  ...Stop Enable",			10, 196, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRTS", "  ...Stop State",			10, 197, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRV",  "Register Move Relative - Velocity",	10, 198, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRVD", "  ...Data Register",			10, 199, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRVA", "  ...Acceleration",			10, 200, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRVV", "  ...Velocity",			10, 201, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRVE", "  ...Stop Enable",			10, 202, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRVS", "  ...Stop State",			10, 203, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRW",  "Read Register, Write",			10, 322, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRWO", "  ...Operation",			10, 323, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRWR", "  ...Register",			10, 324, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RRWD", "  ...Data",				10, 325, 0,	LL) );

	AddSpace( New CSpace(addrNamed, "RSD",  "Registered Step and Direction",	10, 204, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSM",  "Register Store Multiple",		10, 205, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSMN", "  ...Number of Registers",		10, 206, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSMD", "  ...Starting Data Register",		10, 207, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSMA", "  ...NV Memory Address",		10, 208, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSN",  "Register Store to non-volatile",	10, 209, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSND", "  ...Data Register",			10, 210, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSNA", "  ...NV Memory Address",		10, 211, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RSP",  "Restart, Program Mode",		10, 284, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RST",  "Restart, Immediate",			10, 212, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "RUN",  "Run Program",				10, 213, 0,	LL) );

	AddSpace( New CSpace(SHDR, "", "Commands - S...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SCF",  "S-Curve Factor",			10, 214, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SEE",  "Select External Encoder",		10, 215, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SEEI", "  ...Index State",			10, 216, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SEES", "  ...Index Source",			10, 217, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SEEE", "  ...Encoder Style",			10, 218, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SEF",  "Select Encoder Filter",		10, 279, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SIF",	"RS-232 = 0, RS-485 = 1",		10, 219, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SLC",  "Single Loop Control",			10, 220, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SOB",  "Set Output Bit",			10, 221, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SPR",  "Store Program",			10, 280, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSD",  "Scaled Step and Direction",		10, 222, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSE",  "Single Step Exit",			10, 223, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSI",  "SSI Port Mode",			10, 412, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSIM", "  ...Mode",				10, 413, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSIR", "  ...Resolution",			10, 414, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSIO", "  ...Options",				10, 415, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSL",  "Soft Stop Limits",			10, 224, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "SSP",  "Single Step Program",			10, 225, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "STP",  "Stop",					10, 226, 0,	LL) );

	AddSpace( New CSpace(THDR, "", "Commands - T...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "T1F",  "Thread 1 Force",			10, 349, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "T2K",  "Thread 2 Kill Exclusions",		10, 350, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "T2S",  "Thread 2 Start",			10, 360, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "T2SA",  "  ...NV Memory Address",		10, 361, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "T2SZ",  "  ...Program Buffer Size",		10, 362, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TIM",  "Torque Input Mode",			10, 342, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TIMF", "  ...Filter Constant",			10, 343, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TIME", "  ...Stop Enable",			10, 344, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TIMS", "  ...Stop State",			10, 345, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TQL",  "Set Torque Limits",			10, 227, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TQLCH","  ...Closed Loop Holding",		10, 228, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TQLCM","  ...Closed Loop Moving",		10, 229, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TQLOH","  ...Open Loop Holding",		10, 230, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TQLOM","  ...Open Loop Moving",		10, 231, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TRU",  "Torque Ramp Up",			10, 232, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TRUF", "  ...Final Value",			10, 233, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TRUI", "  ...increment",			10, 234, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "TTP",  "Set Target to Position",		10, 235, 0,	LL) );

	AddSpace( New CSpace(VHDR, "", "Commands - V...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VIM",  "Velocity Input Mode",			10, 236, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VIMF", "  ...Filter Constant",			10, 237, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VIME", "  ...I/O Exit Enable",			10, 238, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VIMS", "  ...I/O Exit State",			10, 239, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VLL",  "Velocity Limits",			10, 409, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VLLM", "  ...Moving Limit",			10, 410, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VLLH", "  ...Holding Limit",			10, 411, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMI",  "Velocity Mode - Immediate. Mode",	10, 240, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMIA", "  ...Acceleration",			10, 241, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMIV", "  ...Velocity",			10, 242, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMIE", "  ...Stop Enable",			10, 243, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMIS", "  ...Stop State",			10, 244, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMP",  "Velocity Mode - Program Type",		10, 245, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMPA", "  ...Acceleration",			10, 246, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMPV", "  ...Velocity",			10, 247, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMPE", "  ...Stop Enable",			10, 248, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "VMPS", "  ...Stop State",			10, 249, 0,	LL) );

	AddSpace( New CSpace(WHDR, "", "Commands - W...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WBE",  "Wait on Bit Edge",			10, 250, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WBEC", "  ...Input Code",			10, 251, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WBET", "  ...Input Transition",		10, 252, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WBS",  "Wait on Bit State",			10, 253, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WBSC", "  ...Input Code",			10, 254, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WBSS", "  ...Input State",			10, 255, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WCL",  "Write Command Buffer, Long",		10, 326, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WCLR", "  ...Register",			10, 327, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WCLA", "  ...Program Buffer Address",		10, 328, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WCW",  "Write Command Buffer, Word",		10, 329, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WCWR", "  ...Register",			10, 330, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WCWA", "  ...Program Buffer Address",		10, 331, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WDL",  "Wait Delay",				10, 256, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRF",  "Write Register File",			10, 332, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRFR", "  ...Register",			10, 333, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRFD", "  ...Data",				10, 334, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRX",  "Write Register Extended",		10, 335, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRXO", "  ...Operation",			10, 336, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRXR", "  ...Register",			10, 337, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "WRXD", "  ...Data",				10, 338, 0,	LL) );

	AddSpace( New CSpace(XHDR, "", "Commands - X...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XAT",  "Ext. Register Move Abs. - Time",	10, 257, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XATD", "  ...Starting Data Register",		10, 258, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XATE", "  ...Stop Enable",			10, 259, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XATS", "  ...Stop State",			10, 260, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XAV",  "Ext. Register Move Abs. - Velocity",	10, 261, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XAVD", "  ...Starting Data Register",		10, 262, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XAVE", "  ...Stop Enable",			10, 263, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XAVS", "  ...Stop State",			10, 264, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRT",  "Ext. Register Move Rel. - Time",	10, 265, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRTD", "  ...Starting Data Register",		10, 266, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRTE", "  ...Stop Enable",			10, 267, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRTS", "  ...Stop State",			10, 268, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRV",  "Ext. Register Move Rel. - Velocity",	10, 269, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRVD", "  ...Starting Data Register",		10, 270, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRVE", "  ...Stop Enable",			10, 271, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "XRVS", "  ...Stop State",			10, 272, 0,	LL) );

	AddSpace( New CSpace(ZHDR, "", "Commands - Z...",				10,   0, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ZTG",  "Zero Target",				10, 273, 0,	LL) );
	AddSpace( New CSpace(addrNamed, "ZTP",  "Zero Target and Position",		10, 274, 0,	LL) );
	}

//////////////////////////////////////////////////////////////////////////
//
// QuickSilver Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CQuicksilverDialog, CStdAddrDialog);
		
// Constructor

CQuicksilverDialog::CQuicksilverDialog(CQuicksilverDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_fPart   = fPart;

	m_pSpace  = NULL;

	if( m_CurrentList < ALLHDR || m_CurrentList > ZHDR ) {

		m_CurrentList = ALLHDR;
		}

	SetName(fPart ? TEXT("QuicksilverPartDlg") : TEXT("QuicksilverDlg"));
	}

// Message Map

AfxMessageMap(CQuicksilverDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(4001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CQuicksilverDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;
	
	if( !m_fPart ) {

		SetCaption();

		FindSpace();

		if( m_pSpace ) {

			SetAllowSpace(m_pSpace->m_uTable);
			}

		else m_CurrentList = ALLHDR;

		LoadList();

		if( m_pSpace ) {

			if( !IsAnyHeader(m_pSpace->m_uTable) ) {

				LoadType();
				ShowAddress(Addr);
				ShowDetails();
				}

			return FALSE;
			}

		return TRUE;
		}
	else {
		FindSpace();

		if( IsAnyHeader(m_pSpace->m_uTable) ) {

			Addr.a.m_Table  = addrNamed;
			Addr.a.m_Offset = QERR;
			Addr.a.m_Type   = addrLongAsLong;
			Addr.a.m_Extra  = 0;
			*m_pAddr = Addr;
			FindSpace();
			}

		SetAllowSpace(m_pSpace->m_uTable);

		LoadType();

		ShowAddress(Addr);

		ShowDetails();

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		CError   Error(FALSE);

		CAddress Addr;
			
		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			CAddress Addr;

			m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			ShowDetails();
			}

		return FALSE;
		}
	}

// Notification Handlers

void CQuicksilverDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CQuicksilverDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		if( IsReloadList(m_pSpace->m_uTable) ) return;

		if( m_pSpace != pSpace ) {

			UINT uTable = m_pSpace->m_uTable;

			SetAllowSpace(uTable);

			if( !IsAnyHeader(uTable) ) {

				CAddress Addr;

				Addr.a.m_Type = addrLongAsLong;

				m_pSpace->GetMinimum(Addr);

				ShowAddress(Addr);
				ShowDetails();
				}
			}
		}
	else {
		m_CurrentList = ALLHDR;

		LoadList();

		m_pSpace = NULL;

		ClearType();

		ClearAddress();

		ClearDetails();
		}
	}

// Command Handlers

BOOL CQuicksilverDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		if( IsReloadList(m_pSpace->m_uTable) ) return TRUE;

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overrideables
BOOL	CQuicksilverDialog::AllowSpace(CSpace * pSpace)
{
	CSpace * p = pSpace;

	UINT uTable = p->m_uTable;

	if( IsNonUseful(p) ) return FALSE;

	if( m_fPart ) {

		if( IsAnyHeader(uTable) ) return FALSE;

		if( uTable < ALLHDR || uTable == addrNamed ) return TRUE;
		}

	if( uTable == ALLHDR ) return TRUE;

	if( m_CurrentList == ALLHDR ) return IsHeader(uTable);

	if( IsGeneralCommand(uTable, p->m_uMinimum) ) return m_CurrentList == GENHDR;

	if( IsUserCommand(uTable) ) return m_CurrentList == USRHDR;

	UINT a = m_CurrentList - AHDR + 'A';

	return (UINT)p->m_Prefix[0] == a;
	}

// Helpers
BOOL	CQuicksilverDialog::IsHeader(UINT uTable)
{
	return uTable >= GENHDR && uTable <= ZHDR;
	}

BOOL	CQuicksilverDialog::IsAnyHeader(UINT uTable)
{
	return uTable == ALLHDR || IsHeader(uTable);
	}

BOOL	CQuicksilverDialog::IsReloadList(UINT uTable)
{
	if( IsAnyHeader(uTable) ) {

		SetAllowSpace(uTable);

		LoadList();

		ClearDetails();

		return TRUE;
		}

	return FALSE;
	}

void	CQuicksilverDialog::SetAllowSpace(UINT uTable)
{
	if( IsAnyHeader(uTable) ) {

		m_CurrentList = uTable;

		return;
		}

	if( m_pSpace ) {

		if( IsGeneralCommand(uTable, m_pSpace->m_uMinimum) ) m_CurrentList = GENHDR;

		else {
			if( IsUserCommand(uTable) ) m_CurrentList = USRHDR;

			else m_CurrentList = (UINT)m_pSpace->m_Prefix[0] - 'A' + AHDR;
			}
		}
	}

BOOL	CQuicksilverDialog::IsGeneralCommand(UINT uTable, UINT uMin)
{
	return (uTable <= GENMAX) || ( (uTable == addrNamed) && (uMin == QERR) );
	}

BOOL	CQuicksilverDialog::IsUserCommand(UINT uTable)
{
	return (uTable >= USRMIN) && (uTable <= USRMAX);
	}

BOOL	CQuicksilverDialog::IsNonUseful(CSpace * pSpace)
{
	if( pSpace->m_uTable != addrNamed ) return FALSE;

	// commands found not to be useful for HMI
	// they remain in the unlikely case someone programmed them in an old database.

	switch( pSpace->m_uMinimum ) {

		case 52:
		case 68:
		case 69:
		case 70:
		case 71:
		case 72:
		case 73:
		case 74:
		case 75:
		case 76:
		case 77:
		case 78:
		case 79:
		case 80:
		case 81:
		case 82:
		case 83:
		case 84:
		case 85:
		case 86:
		case 87:
		case 164:
		case 165:
		case 166:
		case 167:
		case 168:
		case 169:
		case 250:
		case 251:
		case 252:
		case 253:
		case 254:
		case 255:
			return TRUE;
		}

	return FALSE;
	}

// End of File
