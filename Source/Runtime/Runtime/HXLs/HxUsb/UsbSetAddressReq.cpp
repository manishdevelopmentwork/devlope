
#include "Intern.hpp"

#include "UsbSetAddressReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Address Standard Device Request
//

// Constructor

CUsbSetAddressReq::CUsbSetAddressReq(WORD wAddress)
{
	Init(wAddress);
	} 

// Operations

void CUsbSetAddressReq::Init(WORD wAddress)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqSetAddress;

	m_Direction = dirHostToDev;

	m_Recipient = recDevice;

	m_wValue    = wAddress;
	}

// End of File
