
#ifndef INCLUDE_RAWCAN_HPP

#define INCLUDE_RAWCAN_HPP

// Instantiated Objects

#include "..\raw29id\r29id.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAN Structures
//

struct CPDU {
	
	UINT  m_uID;
	BOOL  m_fDirty;
	UINT  m_uTime;
	BYTE  m_bDLC;
	};

struct CRxMAIL {

	WORD m_Box;
	UINT m_ID;
	UINT m_Mask;
	UINT m_Filter;
	BYTE m_bDLC;
	BOOL m_fDirty;
	BYTE m_bData[8];
	};

struct CTxMAIL {

	WORD m_Box;
	UINT m_ID;
	BYTE m_bDLC;
	BYTE m_bData[8];
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit Identifier Handler
//

class CCAN29bitIdRawHandler : public CCAN29bitIdEntryRawHandler
{	
	public:
		// Constructor
		CCAN29bitIdRawHandler(IHelper *pHelper);

		// IPortHandler
		void METHOD OnOpen(CSerialConfig const &Config);
		void METHOD OnClose(void);

		// Driver Access
		BOOL SendMail(UINT uBox, PDWORD pData);
		BOOL MakeRxMail(UINT uBox, UINT uMask, UINT uFilter, BYTE bDLC, UINT uMax);
		BOOL MakeTxMail(UINT uBox, UINT uID, BYTE bDLC, UINT uMax);
		BOOL GetRxMail(PTXT Mail);
		UINT GetMailDLC(UINT uBox);
		
	protected:

		// Data Members
		CRxMAIL * m_pRxMail;
		CTxMAIL * m_pTxMail;
		UINT	  m_uRxIndex;
		UINT	  m_uTxIndex;
		
		// Implementation
		
		void HandleFrame(void);

		// Mail Sorting
		static int SortFunc(PCVOID p1, PCVOID p2);

		// Helpers
		BOOL IsThisNode(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Raw CAN Driver
//

class CRawCANDriver : public CCommsDriver
{
	public:
		// Constructor
		CRawCANDriver(void);

		// Destructor
		~CRawCANDriver(void);

		// Identification
		DEFMETH(WORD) GetCategory(void);

		// Management
		void MCALL Attach(IPortObject *pPort);
		void MCALL Detach(void);

		// Configuration
		void MCALL Load(LPCBYTE pData);
		void MCALL CheckConfig(CSerialConfig &Config);

		// Entry Points
		DEFMETH(BOOL) MakePDU(UINT uID, UINT DLC, BOOL fExt, BOOL fRx);
		DEFMETH(BOOL) RxPDU(UINT uID, PDWORD pData);
		DEFMETH(BOOL) TxPDU(UINT uID, PDWORD pData);

		DEFMETH(BOOL) MakeRxMailBox(UINT uBox, UINT uMask, UINT uFilter, UINT uDLC);
		DEFMETH(BOOL) MakeTxMailBox(UINT uBox, UINT uId, UINT uDLC);
		DEFMETH(BOOL) RxMail(PTXT Mail);
		DEFMETH(BOOL) TxMailBox(UINT uBox, PDWORD pData);
		DEFMETH(void) Service(void);

		// Data Access
		DEFMETH(UINT) GetDLC(UINT uID, BOOL fRx);
		DEFMETH(UINT) GetMailDLC(UINT uBox);

	protected:
		// Data Members
		CCAN29bitIdRawHandler * m_pHandler;
		IPortObject *	     m_pPort;
		
		BYTE	m_bMode;
		UINT	m_uTimeout;
		CPDU *	m_pRxPDU;
		CPDU *	m_pTxPDU;
		UINT	m_uRxIndex;
		UINT	m_uTxIndex;
		WORD	m_RxMax;
		WORD    m_TxMax;
		WORD	m_RxMailMax;
		WORD    m_TxMailMax;
		
		// Implementation
		BOOL SkipUpdate(LPCBYTE &pData);
		BOOL IsTimedOut(UINT uTime);
		UINT FindID(DWORD dwID, BOOL fRx);
	};

// End of File

#endif
