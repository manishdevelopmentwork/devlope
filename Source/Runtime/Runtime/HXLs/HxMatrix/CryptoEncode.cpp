
#include "Intern.hpp"

#include "CryptoEncode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Encoding Helper
//

// String Encoder

CString CCryptoEncode::Encode(UINT Code, PCBYTE pData, UINT uData)
{
	if( Code == hashHex ) {

		PTXT  pText = (char *) alloca(1 + 2 * uData);
	
		PCTXT pHex  = "0123456789abcdef";

		UINT  c     = 0;

		for( UINT n = 0; n < uData; n++ ) {

			pText[c++] = pHex[pData[n]/16];
		
			pText[c++] = pHex[pData[n]%16];
			}

		pText[c++] = 0;

		return pText;
		}

	if( Code == hashBase64 || Code == hashUrlEnc ) {

		PTXT pText = (char *) alloca(1 + 2 * uData);

		EncodeBase64(pText, pData, uData, FALSE);

		if( Code == hashUrlEnc ) {

			CString Text(pText);

			EncodeUrl(Text);

			return Text;
			}

		return pText;
		}

	if( Code == hashUrl64 ) {

		PTXT pText = (char *) alloca(1 + 2 * uData);

		EncodeBase64Url(pText, pData, uData);

		return pText;
		}

	AfxAssert(FALSE);

	return "";
	}

// Encoding Helpers

BYTE CCryptoEncode::EncodeBase64(BYTE bData)
{
	if( bData <  26 ) return 'A' + bData;

	if( bData <  52 ) return 'a' + (bData - 26);

	if( bData <  62 ) return '0' + (bData - 52);

	if( bData == 62 ) return '+';

	return '/';
	}

BOOL CCryptoEncode::EncodeBase64(PTXT pBuff, PCBYTE pData, UINT uSize, BOOL fBreak)
{
	if( pData ) {

		PTXT   p2 = pBuff;

		PCBYTE p1 = pData;

		UINT   bc = 0;

		while( bc < uSize ) {

			UINT nb = min(3, uSize - bc);

			BYTE b0 = (nb >= 1) ? *p1++ : 0;
				
			BYTE b1 = (nb >= 2) ? *p1++ : 0;
				
			BYTE b2 = (nb >= 3) ? *p1++ : 0;

			BYTE c1 = (b0 >> 2);

			BYTE c2 = ((b0 & 3) << 4) | (b1 >> 4);

			*p2++ = EncodeBase64(c1);
				
			*p2++ = EncodeBase64(c2);

			if( nb >= 2 ) {

				BYTE c3 = ((b1 & 15) << 2) | (b2 >> 6);

				*p2++ = EncodeBase64(c3);
				}

			if( nb >= 3 ) {

				BYTE c4 = (b2 & 0x3F);

				*p2++ = EncodeBase64(c4);
				}

			while( nb < 3 ) {

				*p2++ = '=';

				nb++;
				}

			bc += nb;

			if( fBreak ) {
					
				if( bc % 57 == 0 || bc >= uSize ) {

					*p2++ = 0x0D;
						
					*p2++ = 0x0A;
					}
				}
			}

		*p2++ = 0;

		return TRUE;
		}

	return FALSE;
	}

BYTE CCryptoEncode::EncodeBase64Url(BYTE bData)
{
	if( bData <  26 ) return 'A' + bData;

	if( bData <  52 ) return 'a' + (bData - 26);

	if( bData <  62 ) return '0' + (bData - 52);

	if( bData == 62 ) return '-';

	return '_';
	}

BOOL CCryptoEncode::EncodeBase64Url(PTXT pBuff, PCBYTE pData, UINT uSize)
{
	if( pData ) {

		PTXT   p2 = pBuff;

		PCBYTE p1 = pData;

		UINT   bc = 0;

		while( bc < uSize ) {

			UINT nb = min(3, uSize - bc);

			BYTE b0 = (nb >= 1) ? *p1++ : 0;
				
			BYTE b1 = (nb >= 2) ? *p1++ : 0;
				
			BYTE b2 = (nb >= 3) ? *p1++ : 0;

			BYTE c1 = (b0 >> 2);

			BYTE c2 = ((b0 & 3) << 4) | (b1 >> 4);

			*p2++ = EncodeBase64Url(c1);
				
			*p2++ = EncodeBase64Url(c2);

			if( nb >= 2 ) {

				BYTE c3 = ((b1 & 15) << 2) | (b2 >> 6);

				*p2++ = EncodeBase64Url(c3);
				}

			if( nb >= 3 ) {

				BYTE c4 = (b2 & 0x3F);

				*p2++ = EncodeBase64Url(c4);
				}

			bc += nb;
			}

		*p2++ = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCryptoEncode::EncodeUrl(CString &Text)
{
	if( !Text.IsEmpty() ) {

		PCTXT   pList = " %!*'();:@&=+$,/?#[]";

		PCTXT   pHex  = "0123456789ABCDEF";

		PCTXT   pText = PCTXT(Text);

		UINT    uFrom = 0;

		CString Output;

		while( pText[uFrom] ) {

			PCTXT pFrom = pText + uFrom;

			UINT  uSpan = strcspn(pFrom , pList);

			PCTXT pFind = pFrom + uSpan;

			if( *pFind ) {

				// We've found one of the encoded set.

				if( !uFrom ) {

					// Assume 25% increase in size before
					// we start the process of building
					// the output string to avoid a lot
					// of reallocation as we work.

					UINT uAlloc = 5 * Text.GetLength() / 4;

					Output.Expand(uAlloc + 1);
					}

				if( *pFind == '%' ) {

					// If it is percent, add the characters
					// before it and the matched character,
					// and then add a second percent sign.

					Output.Append(pFrom, uSpan + 1);

					Output.Append('%');
					}
				else {
					// Otherwise, add the characters before
					// it, followed by a percent sign and then
					// the hex encoding of the character.

					Output.Append(pFrom, uSpan);

					Output.Append('%');

					Output.Append(pHex[*pFind / 16]);
					
					Output.Append(pHex[*pFind % 16]);
					}

				// Skip the encoded characters and
				// the character string before it.

				uFrom += uSpan + 1;

				continue;
				}

			if( uFrom ) {

				// Nothing found, so if we're not at the
				// start of the process, add what is left
				// of the string and we are finished.

				Output.Append(pFrom);

				break;
				}

			// Nothing found and we're at the start of
			// the encoding process, so just return the
			// input string as we've nothing to do.

			return FALSE;
			}

		Text = Output;

		return TRUE;
		}

	return FALSE;
	}

// End of File
