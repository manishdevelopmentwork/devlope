
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Producer Consumer Driver Implementation
//

// Constructor

CProConDriver::CProConDriver(void)
{
	m_pSlaveHelper = NULL;
	}

// IUnknown

HRESULT CProConDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsSlave);

	return CMasterDriver::QueryInterface(riid, ppObject);
	}

ULONG CProConDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CProConDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CProConDriver::GetCategory(void)
{
	return DC_COMMS_PROCON;
	}

// ICommsMaster

CCODE CProConDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	return uCount;
	}
 
// ICommsSlave

void CProConDriver::SetHelper(ISlaveHelper *pHelper)
{
	m_pSlaveHelper = pHelper;
	}

// IO

CCODE CProConDriver::ReadSlave(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pSlaveHelper->Read(Addr, pData, uCount);
	}

CCODE CProConDriver::WriteSlave(AREF Addr, PDWORD pData, UINT uCount)
{	
	return m_pSlaveHelper->Write(Addr, pData, uCount);
	}

// End of File
