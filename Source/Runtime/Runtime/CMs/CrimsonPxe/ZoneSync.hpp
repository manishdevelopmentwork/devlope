
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ZoneSync_HPP

#define	INCLUDE_ZoneSync_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PxeIpClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Time Zone Sync
//

class CZoneSync : public IClientProcess
{
public:
	// Constructor
	CZoneSync(CJsonConfig *pJson, UINT uFaces);

	// Destructor
	~CZoneSync(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG		    m_uRefs;
	ITimeZone	  * m_pZone;
	UINT		    m_uMode;
	BOOL		    m_fMobile;
	UINT		    m_uDMode;
	UINT		    m_uCell;
	CLocationSourceInfo m_Info;
	int		    m_offset;
	BOOL		    m_dst;

	// Implementation
	void   ApplyConfig(CJsonConfig *pJson);
	double GetDistance(CLocationSourceInfo const &a, CLocationSourceInfo const &b);
	double ToRad(double deg);
};

// End of File

#endif
