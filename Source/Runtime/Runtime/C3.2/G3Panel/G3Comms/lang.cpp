
#include "intern.hpp"

#include "lang.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Language Manager
//

// String Data

PCTXT GetStringOne(void)
{
	return "3X2jCwVAqKz*!I8TjAxh";
	}

// Constructor

CLangManager::CLangManager(void)
{
	m_pLang    = NULL;

	m_pNum     = NULL;

	m_hTask    = NULL;

	m_uSlot    = NOTHING;

	m_uDefLang = C3L_ENGLISH_US;

	m_uDefNum  = C3L_ENGLISH_US;

	m_uLang    = C3L_ENGLISH_US;

	m_uNum     = C3L_ENGLISH_US;
	}

// Destructor

CLangManager::~CLangManager(void)
{
	delete [] m_pLang;

	delete [] m_pNum;
	}

// Initialization

void CLangManager::Load(PCBYTE &pData)
{
	ValidateLoad("CLangManager", pData);

	m_System = GetWord(pData);

	m_Global = GetByte(pData);

	m_LocSep = GetByte(pData);

	m_uCount = GetWord(pData);

	m_pLang  = New UINT [ m_uCount ];

	m_pNum   = New BOOL [ m_uCount ];

	for( UINT n = 0; n < m_uCount; n++ ) {

		m_pLang[n] = GetWord(pData);

		m_pNum [n] = GetWord(pData);
		}

	m_uDefLang = m_pLang[0] ? m_pLang[0] : m_System;

	m_uDefNum  = m_pNum [0] ? m_uDefLang : C3L_ENGLISH_US;

	LoadLang();
	}

// Operations

void CLangManager::RegisterUITask(void)
{
	m_hTask = GetCurrentTask();
	}

void CLangManager::SetCurrentSlot(UINT uSlot)
{
	if( uSlot < m_uCount ) {

		if( m_uSlot != uSlot ) {

			m_uLang = m_pLang[uSlot] ? m_pLang[uSlot] : m_System;

			m_uNum  = m_pNum [uSlot] ? m_uLang        : C3L_ENGLISH_US;

			if( m_uSlot == NOTHING ) {

				m_uSlot = uSlot;
				}
			else {
				m_uSlot = uSlot;

				SaveLang();
				}
			}
		}
	}

// Current Language

UINT CLangManager::GetCurrentSlot(void) const
{
	if( m_Global || GetCurrentTask() == m_hTask ) {

		return m_uSlot;
		}

	return 0;
	}

UINT CLangManager::GetEffectiveCode(void) const
{
	if( m_Global || GetCurrentTask() == m_hTask ) {

		return m_uLang;
		}

	return m_uDefLang;
	}

UINT CLangManager::GetNumericCode(void) const
{
	if( m_Global || GetCurrentTask() == m_hTask ) {

		return m_uNum;
		}

	return m_uDefNum;
	}

WCHAR CLangManager::GetDecPointChar(void) const
{
	switch( GetNumericCode() ) {

		case C3L_FRENCH_FR:
		case C3L_FRENCH_BE:
		case C3L_SPANISH_ES:
		case C3L_SPANISH_MX:
		case C3L_GERMAN_DE:
		case C3L_GERMAN_AT:
		case C3L_GERMAN_CH:
		case C3L_ITALIAN_IT:
		case C3L_ITALIAN_CH:
		case C3L_FINNISH:

			return L',';
		}

	return L'.';
	}

WCHAR CLangManager::GetNumGroupChar(void) const
{
	switch( GetNumericCode() ) {

		case C3L_FRENCH_FR:
		case C3L_FRENCH_BE:
		case C3L_FRENCH_CH:

			return spaceNarrow;

		case C3L_SPANISH_ES:
		case C3L_SPANISH_MX:
		case C3L_GERMAN_DE:
		case C3L_GERMAN_AT:
		case C3L_GERMAN_CH:
		case C3L_ITALIAN_IT:
		case C3L_ITALIAN_CH:
		case C3L_FINNISH:

			return L'.';
		}

	return L',';
	}

BOOL CLangManager::IsGroupBoundary(UINT p) const
{
	if( p ) {

		switch( GetNumericCode() ) {

			case C3L_ENGLISH_IN:

				if( p == 3 ) {

					return TRUE;
					}

				if( p >= 5 && (p - 5) % 2 == 0 ) {

					return TRUE;
					}

				return FALSE;
			}

		return p % 3 == 0;
		}

	return FALSE;
	}

CUnicode CLangManager::GetMonthName(UINT m) const
{
	UINT c1 = GetEffectiveCode();

	UINT c2 = c1 & 0xFF00;

	switch( c1 ) {

		case C3L_SPANISH_ES:

			switch( m ) {

				case  1: return L"Ene";
				case  2: return L"Feb";
				case  3: return L"Mar";
				case  4: return L"Abr";
				case  5: return L"May";
				case  6: return L"Jun";
				case  7: return L"Jul";
				case  8: return L"Agos";
				case  9: return L"Sep";
				case 10: return L"Oct";
				case 11: return L"Nov";
				case 12: return L"Dic";
				}
			break;

		case C3L_FINNISH:

			switch( m ) {

				case  1: return L"Tam";
				case  2: return L"Hel";
				case  3: return L"Maa";
				case  4: return L"Huh";
				case  5: return L"Tou";
				case  6: return L"Kes";
				case  7: return L"Hei";
				case  8: return L"Elo";
				case  9: return L"Syy";
				case 10: return L"Lok";
				case 11: return L"Mar";
				case 12: return L"Jou";
				}
			break;

		case C3L_TRAD_CHINESE:
		case C3L_SIMP_CHINESE:

			switch( m ) {

				case  1: return L"\x4E00\x6708";
				case  2: return L"\x4E8C\x6708";
				case  3: return L"\x4E09\x6708";
				case  4: return L"\x56DB\x6708";
				case  5: return L"\x4E94\x6708";
				case  6: return L"\x516D\x6708";
				case  7: return L"\x4E03\x6708";
				case  8: return L"\x516B\x6708";
				case  9: return L"\x4E5D\x6708";
				case 10: return L"\x5341\x6708";
				case 11: return L"\x5341\x4E00\x6708";
				case 12: return L"\x5341\x4E8C\x6708";
				}
			break;

		case C3L_KOREAN_FULL:

			switch( m ) {

				case  1: return L"1\xC8D4";
				case  2: return L"2\xC8D4";
				case  3: return L"3\xC8D4";
				case  4: return L"4\xC8D4";
				case  5: return L"5\xC8D4";
				case  6: return L"6\xC8D4";
				case  7: return L"7\xC8D4";
				case  8: return L"8\xC8D4";
				case  9: return L"9\xC8D4";
				case 10: return L"10\xC8D4";
				case 11: return L"11\xC8D4";
				case 12: return L"12\xC8D4";
				}
			break;

		case C3L_RUSSIAN:

			switch( m ) {

				case  1: return L"\x042F\x041D\x0412";
				case  2: return L"\x0424\x0415\x0412";
				case  3: return L"\x043C\x0435\x0441";
				case  4: return L"\x0410\x041F\x0420";
				case  5: return L"\x041C\x0410\x0419";
				case  6: return L"\x0418\x042E\x041D";
				case  7: return L"\x0418\x042E\x041B";
				case  8: return L"\x0410\x0412\x0413";
				case  9: return L"\x0421\x0415\x041D";
				case 10: return L"\x041E\x041A\x0422";
				case 11: return L"\x041D\x041E\x042F";
				case 12: return L"\x0414\x0415\x041A";
				}
			break;

		case C3L_JAPANESE:

			switch( m ) {

				case  1: return L"\x4E00\x6708";
				case  2: return L"\x4E8C\x6708";
				case  3: return L"\x4E09\x6708";
				case  4: return L"\x56DB\x6708";
				case  5: return L"\x4E94\x6708";
				case  6: return L"\x516D\x6708";
				case  7: return L"\x4E03\x6708";
				case  8: return L"\x516B\x6708";
				case  9: return L"\x4E5D\x6708";
				case 10: return L"\x5341\x6708";
				case 11: return L"\x5341\x4E00\x6708";
				case 12: return L"\x5341\x4E8C\x6708";
				}
			break;
		}

	switch( c2 ) {

		case C3L_FRENCH:

			switch( m ) {

				case  1: return L"Jan";
				case  2: return L"F\x00E9v";
				case  3: return L"Mar";
				case  4: return L"Avr";
				case  5: return L"Mai";
				case  6: return L"Juin";
				case  7: return L"Juil";
				case  8: return L"Ao\x00FB";
				case  9: return L"Sep";
				case 10: return L"Oct";
				case 11: return L"Nov";
				case 12: return L"Dec";
				}
			break;

		case C3L_GERMAN:

			switch( m ) {

				case  1: return L"Jan";
				case  2: return L"Feb";
				case  3: return L"M\x00E4r";
				case  4: return L"Apr";
				case  5: return L"Mai";
				case  6: return L"Jun";
				case  7: return L"Jul";
				case  8: return L"Aug";
				case  9: return L"Sep";
				case 10: return L"Okt";
				case 11: return L"Nov";
				case 12: return L"Dez";
				}
			break;

		case C3L_ITALIAN:

			switch( m ) {

				case  1: return L"Gen";
				case  2: return L"Feb";
				case  3: return L"Mar";
				case  4: return L"Apr";
				case  5: return L"Mag";
				case  6: return L"Giu";
				case  7: return L"Lug";
				case  8: return L"Ago";
				case  9: return L"Set";
				case 10: return L"Ott";
				case 11: return L"Nov";
				case 12: return L"Dic";
				}
			break;
		}

	switch( m ) {

		case  1: return L"Jan";
		case  2: return L"Feb";
		case  3: return L"Mar";
		case  4: return L"Apr";
		case  5: return L"May";
		case  6: return L"Jun";
		case  7: return L"Jul";
		case  8: return L"Aug";
		case  9: return L"Sep";
		case 10: return L"Oct";
		case 11: return L"Nov";
		case 12: return L"Dec";
		}

	return L"xxx";
	}

UINT CLangManager::GetTimeFormat(void) const
{
	switch( GetEffectiveCode() ) {

		case C3L_ENGLISH_US:
		case C3L_ENGLISH_IN:

			return 2;
		}

	return 1;
	}

UINT CLangManager::GetDateFormat(void) const
{
	switch( GetEffectiveCode() ) {

		case C3L_ENGLISH_US:

			return 1;

		case C3L_SIMP_CHINESE:
		case C3L_TRAD_CHINESE:
		case C3L_KOREAN_FULL:
		case C3L_KOREAN_JAMO:
		case C3L_JAPANESE:

			return 3;
		}

	return 2;
	}

WCHAR CLangManager::GetDateSepChar(void) const
{
	switch( GetEffectiveCode() ) {

		case C3L_ENGLISH_IN:
		case C3L_FRENCH_FR:
		case C3L_FRENCH_BE:
		case C3L_FRENCH_CH:
		case C3L_ITALIAN_IT:
		case C3L_ITALIAN_CH:

			return L'/';

		case C3L_JAPANESE:

			return L'.';
		}

	return L'-';
	}

WCHAR CLangManager::GetListSepChar(void) const
{
	if( m_LocSep ) {

		switch( GetEffectiveCode() ) {

			case C3L_FRENCH_FR:
			case C3L_FRENCH_BE:
			case C3L_FRENCH_CA:
			case C3L_FRENCH_CH:
			case C3L_GERMAN_DE:
			case C3L_GERMAN_AT:
			case C3L_GERMAN_CH:
			case C3L_ITALIAN_IT:
			case C3L_ITALIAN_CH:
			case C3L_FINNISH:
			case C3L_SPANISH_ES:
			case C3L_RUSSIAN:

				return L';';
			}
		}

	return L',';
	}

// Implementation

BOOL CLangManager::LoadLang(void)
{
	WORD wLang[2];

	FRAMGetData(Mem(Language), PBYTE(wLang), sizeof(wLang));

	if( wLang[0] == 0xABBA ) {

		SetCurrentSlot(wLang[1]);

		return TRUE;
		}

	SetCurrentSlot(0);

	return FALSE;
	}

BOOL CLangManager::SaveLang(void)
{
	WORD wLang[2];
	
	wLang[0] = 0xABBA;
	
	wLang[1] = m_uSlot;

	FRAMPutData(Mem(Language), PBYTE(wLang), sizeof(wLang));

	return TRUE;
	}

// End of File
