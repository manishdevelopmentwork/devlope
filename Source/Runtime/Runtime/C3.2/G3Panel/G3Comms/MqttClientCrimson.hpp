
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClientCrimson_HPP

#define	INCLUDE_MqttClientCrimson_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCloudServiceCrimson;

class CMqttClientOptionsCrimson;

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//

class CMqttClientCrimson : public CMqttQueuedClient
{
public:
	// Constructor
	CMqttClientCrimson(CCloudServiceCrimson *pService, CMqttClientOptionsCrimson &Opts);

	// Operations
	BOOL Open(void);
	BOOL Poll(UINT uID);

protected:
	// Pub Topics
	enum
	{
		pubDevice   = 0,
		pubTags	    = 1,
		pubExtended = 50
	};

	// Pub Definition
	struct CPub
	{
		UINT   m_uTopic;
		BOOL   m_fHistory;
		UINT   m_uPeriod;
		UINT64 m_uLast;
		BOOL   m_fPend;
	};

	// Data Members
	CArray <CPub>		    m_PubList;
	CCloudServiceCrimson      * m_pService;
	CMqttClientOptionsCrimson & m_Opts;
	UINT			    m_uTick;

	// Client Hooks
	void OnClientPhaseChange(void);
	BOOL OnClientDataSent(CMqttMessage const *pMsg);

	// Message Hook
	BOOL OnMakeHistoric(CMqttMessage *pMsg);

	// Publish Hook
	virtual BOOL GetPubMessage(UINT uTopic, UINT64 uTime, BOOL fTemp, UINT uMode, CMqttMessage * &pMsg);

	// Implementation
	void AddToPubList(UINT uTopic, BOOL fHistory, UINT uPeriod);
	UINT GetTopicCode(UINT uTopic);
	BOOL ForceUpdate(UINT uCode);
	BOOL HasWrites(void);
	void SubstTopic(CString &Topic);
};

// End of File

#endif
