
#include "intern.hpp"

#include "../G3Prims/ScratchPadWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Mouse Messages

void CPageEditorWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	SetFocus();

	((CMainWnd *) afxMainWnd)->ForceUI();

	UpdatePos(Pos);

	if( !m_fScratch ) {

		m_pScratch->ClickOnMain();
		}

	if( m_uMode == modeSelect ) {

		if( !m_fPosValid ) {

			if( KeyMenu() ) {

				return;
				}
			}

		DefaultStart(TRUE);

		CString Name = L"DisplayEditCtx";

		CMenu   Load = CMenu(PCTXT(Name));

		if( Load.IsValid() ) {

			KillGhost();
		
			CMenu &Menu = Load.GetSubMenu(0);

			Menu.SendInitMessage();

			Menu.DeleteDisabled();

			BuildJumpList();

			AddJumpCommands(Menu);

			BuildBuriedList();
			
			AddBuriedCommands(Menu);

			Menu.MakeOwnerDraw(FALSE);
			
			ClientToScreen(Pos);

			Menu.TrackPopupMenu( TPM_LEFTALIGN | TPM_RIGHTBUTTON,
					     Pos,
					     afxMainWnd->GetHandle()
					     );

			Menu.FreeOwnerDraw();
			}
		}

	if( m_uMode == modeZoom ) {

		if( IsDown(VK_CONTROL) ) {
			
			SetScale(m_nScale + 1, Pos);
			}
		else			
			SetScale(m_nScale - 1, Pos);

		return;
		}
	}

void CPageEditorWnd::OnMButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_fScratch ) {

		m_pScratch->ClickOnMain();
		}

	if( !m_pGhost ) {
	
		if( MakeGhost() ) {

			ShowGhost();
	
			ClientToScreen(Pos);

			if( !m_pGhost->SetAlpha(Pos) ) {

				KillGhost();
				}
			}
		}
	}

void CPageEditorWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	SetFocus();

	UpdatePos(Pos);

	if( !m_fScratch ) {

		m_pScratch->ClickOnMain();
		}

	if( m_fPosValid ) {

		if( m_uMode == modeText ) {

			if( !PPtoDP(m_SelText).PtInRect(m_ClientPos) ) {

				SetMode(modeSelect);
				}

			return;
			}

		if( m_uMode == modeGrab ) {

			GrabSetCursor();

			m_TrackPos = Pos;

			m_uCapture = captGrab;

			SetCapture();

			return;
			}

		if( m_uMode == modeZoom ) {

			if( IsDown(VK_CONTROL) ) {
				
				SetScale(m_nScale - 1, Pos);
				}
			else			
				SetScale(m_nScale + 1, Pos);

			return;
			}

		if( InPickMode() ) {

			if( PickDone() ) {

				SetMode(modeSelect);
				}

			return;
			}

		if( !DefaultStart(FALSE) ) {

			m_TrackPos = m_Pos;

			m_uCapture = captPend;

			SetCapture();
			}
		}
	}

void CPageEditorWnd::OnRButtonDblClk(UINT uFlags, CPoint Pos)
{
	UpdatePos(Pos);

	if( m_uMode == modeZoom ) {

		OnRButtonDown(uFlags, Pos);
		}
	}

void CPageEditorWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( m_uMode == modeZoom ) {

		UpdatePos(Pos);

		OnLButtonDown(uFlags, Pos);

		return;
		}

	if( m_uMode == modeSelect ) {

		if( m_pHover ) {
			
			if( HasLoneSelect() ) {
				
				if( m_pHover != GetLoneSelect() ) {

					OnLButtonDown(uFlags, Pos);

					Invalidate(FALSE);

					UpdateWindow();

					if( !HasLoneSelect() ) {

						// Shouldn't happen unless something went
						// wrong in the selecion process, but let's
						// be careful here...

						return;
						}
					}
				else
					UpdatePos(Pos);

				if( CanEditItemProps() ) {

					OnEditItemProps();
					}
				}
			else {
				OnLButtonDown(uFlags, Pos);
			
				Invalidate(FALSE);

				UpdateWindow();
				}
			}
		else {
			UpdatePos(Pos);

			if( !EditKey() ) {

				if( CanEditItemProps() ) {

					if( HasSelect() ) {

						OnEditItemProps();
						}
					}
				}
			}

		return;
		}

	UpdatePos(Pos);
	}

void CPageEditorWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_uCapture ) {

		TrackEnd(FALSE);

		m_uCapture = captNone;

		ReleaseCapture();

		UpdateImage();

		Invalidate(FALSE);

		ShowDefaultStatus();
		}

	ShowGhost();
	}

void CPageEditorWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	// LATER -- Add support for key hot tracking like we
	// just to do on C2. We will have to add the appropriate
	// hooks into frame.cpp and let it do most of the work.

	if( UpdatePos(Pos) ) {

		if( m_uMode == modeZoom ) {

			ShowDefaultStatus();
			
			return;
			}

		if( m_uMode == modeGrab ) {

			if( m_uCapture == captGrab ) {

				CSize Step = CSize(m_TrackPos - Pos);

				m_TrackPos = Pos;

				ScrollBy(Step);

				ShowDefaultStatus();
				}

			return;
			}			

		if( InPickMode() ) {

			PickUpdate();

			return;
			}

		if( m_uCapture == captPend ) {

			if( !TrackStart() ) {

				m_uCapture = captNone;

				ReleaseCapture();
				}
			else {
				KillGhost();

				TrackSetCursor();

				TrackUpdate();
				}
			}
		else {
			if( m_uCapture ) {

				ScrollIntoView(Pos, FALSE);
				}
			else
				ShowDefaultStatus();

			TrackUpdate();
			}

		CheckKeyHover();
		}
	}

BOOL CPageEditorWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( Wnd.GetHandle() == m_hWnd ) {
	
		TrackSetCursor();

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerHover ) {

		if( m_uMode == modeSelect ) {

			if( m_nHover && IsHoverable(m_nHover) ) {

				CPoint Pos = GetClientPos();

				CRect  R1  = GetClientRect();

				if( !R1.PtInRect(Pos) ) {

					m_fPosValid = FALSE;

					KillHover();

					Invalidate(FALSE);
					}
				}
			}
		}

	if( uID == m_timerGhost ) {

		if( m_pGhost ) {

			CPoint Pos = GetCursorPos();

			CRect  R1  = m_pGhost->GetWindowRect();

			CRect  R2  = GetWindowRect();

			if( !R1.PtInRect(Pos) && !R2.PtInRect(Pos) ) {

				KillGhost();
				}
			}
		}

	if( uID == m_timerKeys ) {

		CheckKeyTimer();
		}

	if( uID == m_timerScroll ) {

		CPoint Pos = GetClientPos();

		if( m_fDrop ) {

			// LATER -- Update scroll position?

			ScrollIntoView(Pos, TRUE);
			}
		else
			OnMouseMove(0, Pos);
		}
	}

// Tracking Helpers

BOOL CPageEditorWnd::TrackStart(void)
{
	if( m_uMode == modeSelect ) {

		if( HasSelect() ) {
		
			if( SizeStart() ) {

				return TRUE;
				}

			if( DragStart() ) {

				return FALSE;
				}
			}

		return SelectStart();
		}

	return FALSE;
	}

void CPageEditorWnd::TrackUpdate(void)
{
	switch( m_uCapture ) {

		case captSelect:
			SelectUpdate();
			break;

		case captSize:
			SizeUpdate();
			break;

		default:
			DefaultUpdate();
			break;
		}
	}

void CPageEditorWnd::TrackSetCursor(void)
{
	if( m_uMode == modeText ) {

		SetCursor(CCursor(IDC_ARROW));

		return;
		}

	if( m_uMode == modeGrab ) {

		GrabSetCursor();
		
		return;
		}

	if( m_uMode == modeZoom ) {

		ZoomSetCursor();
		
		return;
		}

	if( InPickMode() ) {

		PickSetCursor();
		
		return;
		}

	DefaultSetCursor();
	}

void CPageEditorWnd::TrackDraw(void)
{
	if( m_fDrop ) {

		DropDraw();
		}
	else {
		switch( m_uCapture ) {

			case captSize:
				SizeDraw();
				break;
			}
		}
	}

void CPageEditorWnd::TrackDraw(CDC &DC)
{
	if( m_fDrop ) {

		DropDraw(DC);
		}
	else {
		switch( m_uCapture ) {

			case captSelect:
				SelectDraw(DC);
				break;

			case captSize:
				SizeDraw(DC);
				break;

			default:
				DefaultDraw(DC);
				break;
			}
		}

	if( m_pWorkSet ) {

		CPrimSet *pSet    = m_pWorkSet;

		CColor    Color   = afxColor(GREEN);

		int       nOffset = 4;

		for(;;) {

			CItem *pItem = pSet->GetParent(2);

			CRect  Rect  = PPtoDP(pSet->GetRect());

			BlendItemRect(DC, Rect + nOffset, Color);

			if( pItem->IsKindOf(AfxRuntimeClass(CPrimSet)) ) {

				pSet = (CPrimSet *) pItem;

				Color.m_bColor[0] /= 2;
				Color.m_bColor[1] /= 2;
				Color.m_bColor[2] /= 2;

				nOffset += 5;

				continue;
				}

			break;
			}
		}
	}

void CPageEditorWnd::TrackEnd(BOOL fAbort)
{
	if( m_Pos == m_TrackPos ) {

		fAbort = TRUE;
		}

	switch( m_uCapture ) {

		case captSelect:

			SelectEnd(fAbort);
			
			break;

		case captSize:
			
			SizeEnd(fAbort);
			
			break;
		}

	KillTimer(m_timerScroll);

	DefaultUpdate();
	}

// Default Helpers

BOOL CPageEditorWnd::DefaultStart(BOOL fRight)
{
	// REV3 -- Should we make clicked selection occur on mouse up, so
	// that you can still draw a selection rectangle even if you are
	// working on top of a background primitive?

	if( m_uMode == modeSelect ) {

		DefaultUpdate();

		if( m_pHover ) {
			
			if( m_pHover->GetParent(2) == m_pPendSet ) {

				if( fRight ) {

					ClearHover();
					
					return FALSE;
					}

				SelectWorkList();
				}
			}

		if( m_uHandle == NOTHING ) {

			while( m_pWorkSet ) {

				if( !PPtoDP(m_WorkRect).PtInRect(m_ClientPos) ) {

					ClimbWorkList(FALSE);

					continue;
					}

				break;
				}

			if( fRight ) {
			
				if( PPtoDP(m_SelRect).PtInRect(m_ClientPos) ) {

					return FALSE;
					}
				}
			
			if( m_pHover ) {

				if( IsDown(VK_SHIFT) ) {

					if( !IsSelected(m_nHover) ) {
					
						AddSelect(m_nHover, FALSE);
						}
					else
						RemSelect(m_nHover, FALSE);
					}
				else {
					if( IsSelected(m_nHover) ) {

						if( PPtoDP(m_SelText).PtInRect(m_ClientPos) ) {

							if( IsTextZoomOkay(TRUE) ) {

								MakeBlockCmd( m_pHover,
									      CString(IDS_EDIT_TEXT)
									      );

								SetTextMode(TRUE, FALSE);
								}

							return TRUE;
							}

						return FALSE;
						}

					SetSelect(m_nHover);
					}

				return FALSE;
				}

			if( PPtoDP(m_SelRect).PtInRect(m_ClientPos) ) {

				return FALSE;
				}

			ClearSelect(FALSE);
			}
		}

	return FALSE;
	}

void CPageEditorWnd::DefaultUpdate(void)
{
	if( m_pGhost ) {

		CPoint Pos = GetCursorPos();

		if( !m_pGhost->SetAlpha(Pos) ) {

			KillGhost();
			}
		}

	if( m_fPosValid ) {
		
		if( m_uHandle == NOTHING || InPickMode() ) {

			INDEX nHit = HitTestPrim(m_Pos);

			if( m_nHover != nHit ) {

				if( m_nHover = nHit ) {

					if( !IsSelected(m_nHover) ) {

						SetTimer(m_timerHover, 50);
						}

					m_pHover = m_pWorkList->GetItem(nHit);
					}
				else {
					m_pHover = NULL;

					KillTimer(m_timerHover);
					}

				Invalidate(FALSE);
				}

			return;
			}
		}

	if( m_nHover ) {

		KillHover();
		}

	Invalidate(FALSE);
	}

void CPageEditorWnd::DefaultSetCursor(void)
{
	if( m_uMode == modeSelect ) {

		if( m_uCapture == captNone ) {

			if( m_uHandle < NOTHING ) {

				CHand const &Hand = m_Handles[m_uHandle];

				switch( Hand.m_Code ) {

					case handN:
					case handS:

						SetCursor(CCursor(IDC_SIZENS));
						
						return;

					case handE:
					case handW:
						
						SetCursor(CCursor(IDC_SIZEWE));
						
						return;

					case handNW:
					case handSE:
						
						SetCursor(CCursor(IDC_SIZENWSE));
						
						return;

					case handNE:
					case handSW:
						
						SetCursor(CCursor(IDC_SIZENESW));
						
						return;

					case handMid:

						SetCursor(CCursor(IDC_SIZEALL));

						return;

					case handLine1:
					case handLine2:
						
						SetCursor(m_LineCursor);
						
						return;

					case handItem:
						
						if( Hand.m_Hand.m_Clip.cx() == 0 ) {

							SetCursor(CCursor(IDC_SIZENS));

							return;
							}
						
						if( Hand.m_Hand.m_Clip.cy() == 0 ) {

							SetCursor(CCursor(IDC_SIZEWE));

							return;
							}

						SetCursor(CCursor(IDC_SIZEALL));

						return;
					}

				SetCursor(CCursor(IDC_ARROW));
				}
			else {
				if( PPtoDP(m_SelText).PtInRect(m_ClientPos) ) {

					SetCursor(CCursor(IDC_IBEAM));
					}
				else
					SetCursor(CCursor(IDC_ARROW));
				}
			}

		return;
		}
	}

void CPageEditorWnd::DefaultDraw(CDC &DC)
{
	if( m_uMode == modeText ) {

		BlendItemLine(DC, PPtoDP(m_SelText), afxColor(YELLOW));

		return;
		}

	if( m_uMode == modeSelect || InPickMode() ) {

		if( !m_fDrag ) {

			if( HasSelect() && HasFocus() ) {

				DrawSelect(DC);

				DrawHandles(DC);
				}

			if( m_fHover ) {

				if( m_pHover && IsHoverable(m_nHover) ) {

					CRect Rect = m_pHover->GetNormRect();

					if( m_pHover->IsLine() ) {

						Rect = PPtoDP(Rect);
					
						BlendItemLine(DC, Rect, afxColor(BLUE));
						}
					else {
						Rect = PPtoDP(Rect);

						BlendItemRect(DC, Rect, afxColor(BLUE));
						}
					}
				}
			}

		return;
		}
	}

// Selection Helpers

BOOL CPageEditorWnd::SelectStart(void)
{
	m_uCapture = captSelect;

	SelectUpdate();

	return TRUE;
	}

void CPageEditorWnd::SelectUpdate(void)
{
	m_TrackRect = CRect(m_TrackPos, m_Pos);

	m_TrackRect.Normalize();

	m_TrackRect.right  += 1;

	m_TrackRect.bottom += 1;

	Invalidate(FALSE);
	}

void CPageEditorWnd::SelectDraw(CDC &DC)
{
	if( m_TrackRect.cx() > 1 && m_TrackRect.cy() > 1 ) {

		INDEX n = m_pWorkList->GetHead();

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( !IsSelected(pPrim) ) {

				if( pPrim->IsLine() ) {

					CRect  Rect = pPrim->GetRect();

					CPoint Pos1 = Rect.GetTopLeft();

					CPoint Pos2 = Rect.GetBottomRight();

					if( m_TrackRect.PtInRect(Pos1) ) {

						if( m_TrackRect.PtInRect(Pos2) ) {

							Rect = PPtoDP(Rect);

							BlendItemLine(DC, Rect, afxColor(BLUE));
							}
						}
					}
				else {
					if( m_TrackRect.Encloses(pPrim->GetRect()) ) {

						CRect Rect = PPtoDP(pPrim->GetRect());

						BlendItemRect(DC, Rect, afxColor(BLUE));
						}
					}
				}

			m_pWorkList->GetNext(n);
			}

		DrawSelect(DC);

		CRect  Rect  = PPtoDP(m_TrackRect);

		CBrush Brush = CBrush(afxColor(WHITE), afxColor(BLACK), 128);

		DC.FrameRect(Rect, Brush);
		}
	}

void CPageEditorWnd::SelectEnd(BOOL fAbort)
{
	if( !fAbort ) {

		INDEX n = m_pWorkList->GetHead();

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( !IsSelected(pPrim) ) {

				if( pPrim->IsLine() ) {

					CRect  Rect = pPrim->GetRect();

					CPoint Pos1 = Rect.GetTopLeft();

					CPoint Pos2 = Rect.GetBottomRight();

					if( m_TrackRect.PtInRect(Pos1) ) {

						if( m_TrackRect.PtInRect(Pos2) ) {

							AddSelect(n, TRUE);
							}
						}
					}
				else {
					if( m_TrackRect.Encloses(pPrim->GetRect()) ) {

						AddSelect(n, TRUE);
						}
					}
				}

			m_pWorkList->GetNext(n);
			}

		AddSelect(NULL, FALSE);
		}
	}

// Sizing Helpers

BOOL CPageEditorWnd::SizeStart(void)
{	
	if( m_uHandle < NOTHING ) {

		CHand const &Hand = m_Handles[m_uHandle];

		if( Hand.m_Code != handMid ) {

			m_uCapture = captSize;

			FindSelectMinSize();

			AcceptSelection();

			m_TrackRect = m_AcceptRect;

			UpdateImage();

			SizeUpdate();

			return TRUE;
			}
		}

	return FALSE;
	}

void CPageEditorWnd::SizeUpdate(void)
{
	CRect        Rect = m_TrackRect;

	CHand const &Hand = m_Handles[m_uHandle];

	if( Hand.m_Code == handItem ) {

		if( SizeUpdateItem(Hand) ) {

			ShowTrackStatus();

			Invalidate(FALSE);
			}
		}
	else {
		SnapToGrid(m_Pos, 4);

		if( Hand.m_Code == handLine1 || Hand.m_Code == handLine2 ) {

			SizeUpdateLine(Hand);
			}

		if( Hand.m_Code <= handSE ) {

			SizeUpdateRect(Hand);
			}

		if( m_TrackRect != Rect ) {

			SizeAcceptList(Rect, m_TrackRect);

			ShowTrackStatus();

			Invalidate(FALSE);
			}
		}
	}

BOOL CPageEditorWnd::SizeUpdateItem(CHand const &Hand)
{
	INDEX  Index = m_pAcceptList->GetHead();

	CPrim *pPrim = m_pAcceptList->GetItem(Index);

	CRect  Rect  = pPrim->GetRect();

	CRect  Clip  = Hand.m_Hand.m_Clip;

	CPoint Pos   = m_Pos;

	switch( Hand.m_Hand.m_uRef % 100 ) {

		case 0:
		case 3:
			Pos.x = Pos.x - Rect.left;
			break;

		case 1:
		case 2:
			Pos.x = Rect.right - 1 - Pos.x;
			break;
		}

	switch( Hand.m_Hand.m_uRef % 100 ) {

		case 0:
		case 1:
			Pos.y = Pos.y - Rect.top;
			break;

		case 2:
		case 3:
			Pos.y = Rect.bottom - 1 - Pos.y;
			break;
		}

	if( IsDown(VK_CONTROL) ) {

		if( Clip.cx() && Clip.cy() ) {

			MakeMin(Pos.x, Pos.y);
			MakeMin(Pos.y, Pos.x);
			}
		}

	if( Hand.m_Hand.m_uRef / 100 == 1 ) {
		
		Pos.x = 10000 * Pos.x / Rect.cx();
		Pos.y = 10000 * Pos.y / Rect.cy();
		}

	if( Hand.m_Hand.m_uRef / 100 == 2 ) {
		
		if( Clip.cx() ) {

			Pos.x = 10000 * Pos.x / Rect.cy();
			Pos.y = 10000 * Pos.y / Rect.cy();
			}
		else {
			Pos.x = 10000 * Pos.x / Rect.cx();
			Pos.y = 10000 * Pos.y / Rect.cx();
			}
		}

	MakeMax(Pos.x, Clip.left  );
	MakeMin(Pos.x, Clip.right );
	MakeMax(Pos.y, Clip.top   );
	MakeMin(Pos.y, Clip.bottom);

	if( SetItemHandle(pPrim, Hand.m_Hand.m_pTag, Clip.GetSize(), Pos) ) {

		CHand Work        = Hand;

		Work.m_Hand.m_Pos = Pos;

		BuildItemHandle(pPrim, Work);

		m_Handles.SetAt(m_uHandle, Work);

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::SizeUpdateLine(CHand const &Hand)
{
	INDEX  Index = m_pAcceptList->GetHead();

	CPrim *pPrim = m_pAcceptList->GetItem(Index);

	m_TrackRect  = pPrim->GetRect();

	switch( Hand.m_Code ) {

		case handLine1:

			m_TrackRect.left = m_Pos.x;
			m_TrackRect.top  = m_Pos.y;

			break;
		
		case handLine2:

			m_TrackRect.right  = m_Pos.x;
			m_TrackRect.bottom = m_Pos.y;

			break;

		}
	}

void CPageEditorWnd::SizeUpdateRect(CHand const &Hand)
{
	CRect  Rect = m_SelRect;

	CSize  Min  = m_TrackMin;

	CSize  Size = Rect.GetSize() - CSize(1, 1);

	CPoint Ref  = Rect.GetTopLeft();

	CPoint Use  = CPoint(0, 0);

	switch( Hand.m_Code ) {

		case handNW:
		case handW:
		case handSW:
			Ref.x = Rect.right - 1;
			Use.x = 1;
			break;

		case handNE:
		case handE:
		case handSE:
			Ref.x = Rect.left;
			Use.x = 1;
			break;
		}

	switch( Hand.m_Code ) {

		case handNW:
		case handN:
		case handNE:
			Ref.y = Rect.bottom - 1;
			Use.y = 1;
			break;

		case handSW:
		case handS:
		case handSE:
			Ref.y = Rect.top;
			Use.y = 1;
			break;
		}

	if( IsDown(VK_SHIFT) ) {

		if( Use.x ) Ref.x  = Rect.GetCenter().x;
		if( Use.y ) Ref.y  = Rect.GetCenter().y;
		}

	if( Use.x && Use.y ) {
		
		Size = CSize(m_Pos - Ref);

		if( IsDown(VK_CONTROL) ) {

			Size.MakeSquare();
			}
		}
	else {
		if( Use.x ) Size.cx = m_Pos.x - Ref.x;
		if( Use.y ) Size.cy = m_Pos.y - Ref.y;
		}

	if( IsDown(VK_SHIFT) ) {

		Min = (Min + CSize(1, 1)) / 2;

		if( Size.cx < 0 ) Min.cx++;
		if( Size.cy < 0 ) Min.cy++;
		}

	Size.cx    = Sgn1(Size.cx) * max(Abs(Size.cx), Min.cx - 1);

	Size.cy    = Sgn1(Size.cy) * max(Abs(Size.cy), Min.cy - 1);

	CRect Work = CRect(Ref, Size);

	CSize Sign = Work.Normalize();

	if( IsDown(VK_SHIFT) ) {

		if( Use.x && Size.cx > 0 ) Work.left   -= Size.cx + 1;
		if( Use.x && Size.cx < 0 ) Work.right  -= Size.cx + 1;
		if( Use.y && Size.cy > 0 ) Work.top    -= Size.cy + 1;
		if( Use.y && Size.cy < 0 ) Work.bottom -= Size.cy + 1;
		}

	Work.right  += 1;
	Work.bottom += 1;

	ClipSizeRect(Work, m_TrackMin);

	m_TrackRect = Work;
	}

void CPageEditorWnd::SizeSetCursor(void)
{
	}

void CPageEditorWnd::SizeDraw(void)
{
	m_Error.Empty();

	if( HasLoneSelect() ) {

		if( GetLoneSelect()->IsMove() ) {

			CPrim *pPrim = m_pAcceptList->GetItem(0U);

			pPrim->Draw(m_pGDI, drawSet);
			}
		}

	UpdateImage(m_pAcceptList, 0);
	}

void CPageEditorWnd::SizeDraw(CDC &DC)
{
	CHand const &Hand = m_Handles[m_uHandle];

	CRect        Rect = PPtoDP(m_TrackRect);

	if( Hand.m_Code < handLine1 ) {

		if( IsDown(VK_SHIFT) ) {

			m_LinedUp.Empty();

			switch( Hand.m_Code ) {

				case handNW:
				case handW:
				case handSW:
				case handNE:
				case handE:
				case handSE:
					ShowLineUpLeft  (DC, FALSE);
					ShowLineUpCenter(DC, FALSE);
					ShowLineUpRight (DC, FALSE);
					break;
				}

			switch( Hand.m_Code ) {

				case handNW:
				case handN:
				case handNE:
				case handSW:
				case handS:
				case handSE:
					ShowLineUpTop   (DC, FALSE);
					ShowLineUpMiddle(DC, FALSE);
					ShowLineUpBottom(DC, FALSE);
					break;
				}
			}
		else {
			m_LinedUp.Empty();

			switch( Hand.m_Code ) {

				case handNW:
				case handW:
				case handSW:
					ShowLineUpLeft  (DC, FALSE);
					ShowLineUpCenter(DC, FALSE);
					break;

				case handNE:
				case handE:
				case handSE:
					ShowLineUpRight (DC, FALSE);
					ShowLineUpCenter(DC, FALSE);
					break;
				}

			switch( Hand.m_Code ) {

				case handNW:
				case handN:
				case handNE:
					ShowLineUpTop   (DC, FALSE);
					ShowLineUpMiddle(DC, FALSE);
					break;

				case handSW:
				case handS:
				case handSE:
					ShowLineUpBottom(DC, FALSE);
					ShowLineUpMiddle(DC, FALSE);
					break;
				}
			}
		}

	switch( Hand.m_Code ) {

		case handItem:

			BlendItemRect(DC, Rect, afxColor(RED));

			DrawHandle   (DC, m_Handles[m_uHandle], TRUE);

			break;

		case handLine1:
		case handLine2:

			BlendItemLine(DC, Rect, afxColor(RED));

			break;

		default:

			BlendItemRect(DC, Rect, afxColor(RED));

			break;
		}
	}

void CPageEditorWnd::SizeEnd(BOOL fAbort)
{
	if( !fAbort ) {

		CHand const &Hand = m_Handles[m_uHandle];

		if( Hand.m_Code == handItem ) {

			CString Item = m_System.GetNavPos();

			BOOL    fNew = TRUE;

			if( m_fCoalesceHand ) {
			
				CCmd *pCmd = m_System.GetLastCmd();

				if( pCmd ) {
					
					if( pCmd->IsKindOf(AfxRuntimeClass(CCmdHandle)) ) {

						if( pCmd->m_Item == Item ) {

							CCmdHandle *pHand = (CCmdHandle *) pCmd;

							pHand->m_Pos = Hand.m_Hand.m_Pos;

							if( pHand->m_Pos == pHand->m_Old ) {

								m_System.KillLastCmd();
								}

							SetSelectionHandle( Hand.m_Hand.m_pTag,
									    Hand.m_Hand.m_Clip.GetSize(),
									    Hand.m_Hand.m_Pos
									    );

							NewSelection();

							fNew = FALSE;
							}
						}
					}
				}

			if( fNew ){

				CCmd *pCmd = New CCmdHandle( Item,
							     Hand.m_Hand.m_pTag, 
							     Hand.m_Hand.m_Clip.GetSize(),
							     Hand.m_Old,
							     Hand.m_Hand.m_Pos
							     );

				LocalExecCmd(pCmd);
				}
			}
		else {
			CString Item = m_System.GetNavPos();

			BOOL    fNew = TRUE;

			if( m_fCoalesceSize ) {
			
				CCmd *pCmd = m_System.GetLastCmd();

				if( pCmd ) {
					
					if( pCmd->IsKindOf(AfxRuntimeClass(CCmdSize)) ) {

						if( pCmd->m_Item == Item ) {

							CCmdSize *pSize  = (CCmdSize *) pCmd;

							pSize->m_NewRect = m_TrackRect;

							if( pSize->m_NewRect == pSize->m_OldRect ) {

								m_System.KillLastCmd();
								}

							SizeSelection(m_TrackRect, TRUE);

							UpdateSelectData();

							fNew = FALSE;
							}
						}
					}
				}

			if( fNew ) {

				CCmd *pCmd = New CCmdSize(Item, m_SelRect, m_TrackRect);

				LocalExecCmd(pCmd);
				}
			}
		}

	FreeAcceptList();
	}

// Grab Helpers

void CPageEditorWnd::GrabSetCursor(void)
{
	if( IsDown(MK_LBUTTON) ) {

		SetCursor(m_GrabCloseCursor);

		return;
		}

	SetCursor(m_GrabOpenCursor);
	}

// Zoom Helpers

void CPageEditorWnd::ZoomSetCursor(void)
{
	if( IsDown(VK_CONTROL) ) {
	
		SetCursor(m_ZoomOutCursor);

		return;
		}

	SetCursor(m_ZoomInCursor);
	}

// Pick Helpers

BOOL CPageEditorWnd::InPickMode(void)
{
	return m_uMode == modePickAny || m_uMode == modePickLoc;
	}

BOOL CPageEditorWnd::CanPick(void)
{
	if( HasSelect() ) {
		
		if( m_SelList.GetCount() < m_pWorkList->GetItemCount() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CPageEditorWnd::PickUpdate(void)
{
	DefaultUpdate();

	ShowPickStatus();
	}

void CPageEditorWnd::PickSetCursor(void)
{
	if( m_pHover && IsPickable(m_nHover) ) {

		SetCursor(m_PickCursor);

		return;
		}

	SetCursor(m_MissCursor);
	}

BOOL CPageEditorWnd::PickDone(void)
{
	if( m_pHover && IsPickable(m_nHover) ) {

		m_pPick = m_pHover;

		m_nPick = m_nHover;

		if( !m_PickNav.IsEmpty() ) {
		
			m_System.Navigate(m_PickNav);

			m_PickNav.Empty();
			}

		switch( m_uPick ) {

			case IDM_EDIT_COPY_SIZE:

				OnEditCopySizeDone();

				break;

			case IDM_EDIT_COPY_ALL:
			case IDM_EDIT_COPY_FORMAT:
			case IDM_EDIT_COPY_FONT:
			case IDM_EDIT_COPY_FIGURE:
			case IDM_EDIT_COPY_ACTION:
			case IDM_EDIT_COPY_SELECT:

				OnEditCopyPropsDone();

				break;

			case IDM_ARRANGE_ALIGN_L:
			case IDM_ARRANGE_ALIGN_C:
			case IDM_ARRANGE_ALIGN_R:
			case IDM_ARRANGE_ALIGN_T:
			case IDM_ARRANGE_ALIGN_M:
			case IDM_ARRANGE_ALIGN_B:
			case IDM_ARRANGE_ALIGN_O:

				OnArrangeAlignDone();

				break;
			}

		return TRUE;
		}

	return FALSE;
	}

// Position Testing

BOOL CPageEditorWnd::UpdatePos(void)
{
	CPoint Pos = GetClientPos();

	if( GetClientRect().PtInRect(Pos) ) {

		if( UpdatePos(Pos) ) {

			TrackSetCursor();

			return TRUE;
			}
		}

	m_uHandle = HitTestHandle(m_ClientPos);

	TrackSetCursor();

	return FALSE;
	}

BOOL CPageEditorWnd::UpdatePos(CPoint Pos)
{
	if( m_ClientPos != Pos ) {

		BOOL fClip  = FALSE;

		m_fPosMove  = TRUE;

		m_ClientPos = Pos;

		Pos         = DPtoPP(Pos);

		if( m_fDrop || m_uCapture ) {

			fClip = TRUE;
			}
		else {
			UINT uHandle = HitTestHandle(m_ClientPos);
			
			if( m_uHandle - uHandle ) {

				m_uHandle = uHandle;
				}

			if( m_uHandle < NOTHING ) {

				fClip = TRUE;
				}
			}

		if( fClip ) {

			MakeMax(Pos.x, m_WorkRect.left);

			MakeMax(Pos.y, m_WorkRect.top);

			MakeMin(Pos.x, m_WorkRect.right  - 1);

			MakeMin(Pos.y, m_WorkRect.bottom - 1);

			m_fPosValid = TRUE;

			m_Pos       = Pos;
			}
		else {
			if( CRect(m_DispSize).PtInRect(Pos) ) {

				m_fPosValid = TRUE;

				m_Pos       = Pos;
				}
			else
				m_fPosValid = FALSE;
			}
		
		return TRUE;
		}

	m_fPosMove = FALSE;

	return FALSE;
	}

INDEX CPageEditorWnd::HitTestPrim(CPoint Pos)
{
	if( m_uMode == modePickAny ) {

		return HitTestPrim(m_pList, Pos, TRUE);
		}

	if( m_pPendSet ) {

		INDEX n = HitTestPrim(m_pPendSet->m_pList, Pos, FALSE);

		if( n ) {

			return n;
			}
		}
			
	return HitTestPrim(m_pWorkList, Pos, FALSE);
	}

INDEX CPageEditorWnd::HitTestPrim(CPrimList *pList, CPoint Pos, BOOL fTree)
{
        if( !fTree ) {

		if( IsDown(VK_CONTROL) ) {

			INDEX n = pList->GetHead();

			while( !pList->Failed(n) ) {

				if( pList->GetItem(n)->HitTest(Pos) ) {

					return n;
					}

				pList->GetNext(n);
				}

			return NULL;
			}
		}

	INDEX n = pList->GetTail();

	while( !pList->Failed(n) ) {

		CPrim *pPrim = pList->GetItem(n);

		if( fTree ) {

			if( pPrim->IsSet() ) {

				CPrimSet *pSet  = (CPrimSet *) pPrim;

				INDEX     Index = HitTestPrim(pSet->m_pList, Pos, TRUE);

				if( Index ) {

					return Index;
					}

				pList->GetPrev(n);

				continue;
				}
			}

		if( pPrim->HitTest(Pos) ) {

			return n;
			}

		pList->GetPrev(n);
		}

	return NULL;
	}

UINT CPageEditorWnd::HitTestHandle(CPoint Pos)
{
	for( UINT n = 0; n < m_Handles.GetCount(); n++ ) {

		CHand const &Hand = m_Handles[n];

		if( Hand.m_Rect.PtInRect(Pos) ) {

			return n;
			}
		}

	return NOTHING;
	}

// Grid Snapping

void CPageEditorWnd::SnapToGrid(CPoint &Pos, UINT uMode)
{
	if( !(GetKeyState(VK_MENU) & 0x8000) ) {

		if( m_pWorkSet ) {

			if( !(m_uGridSnap & 8) ) {

				return;
				}
			}

		if( m_uGridSnap & uMode ) {

			Pos.x = 8 * ((Pos.x + 4) / 8);

			Pos.y = 8 * ((Pos.y + 4) / 8);
			}
		}
	}

// Mouse Position

CPoint CPageEditorWnd::GetClientPos(void)
{
	CPoint Pos = GetCursorPos();

	ScreenToClient(Pos);

	return Pos;
	}

// Hook Support

void CPageEditorWnd::InstallHook(void)
{
	if( !m_hKeyHook ) {

		m_hKeyHook = SetWindowsHookEx( WH_KEYBOARD,
					       HOOKPROC(KeyboardProc),
					       NULL,
					       GetCurrentThreadId()
					       );
		}
	}

void CPageEditorWnd::RemoveHook(void)
{
	if( m_hKeyHook ) {

		UnhookWindowsHookEx(m_hKeyHook);

		m_hKeyHook = NULL;
		}
	}

// Hook Procedure

LRESULT CALLBACK CPageEditorWnd::KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	CPoint Pos = GetCursorPos();

	if( !(lParam & 0x80000000) ) {

		HWND  hDrop  = WindowFromPoint(Pos);

		DWORD dwThis = GetCurrentProcessId();

		DWORD dwDrop = 0;

		GetWindowThreadProcessId(hDrop, &dwDrop);

		if( dwDrop == dwThis ) {

			CPageEditorWnd &Drop = (CPageEditorWnd &) CWnd::FromHandle(hDrop);

			if( Drop.IsKindOf(AfxRuntimeClass(CPageEditorWnd)) ) {

				if( Drop.m_fDrop ) {

					int nStep = Drop.m_nScale;

					int xMin  = Drop.m_WorkRect.left;

					int yMin  = Drop.m_WorkRect.top;

					int xMax  = Drop.m_WorkRect.right;
					
					int yMax  = Drop.m_WorkRect.bottom;

					if( wParam == VK_LEFT ) {

						if( Drop.m_TrackRect.left > xMin ) {

							SetCursorPos(Pos.x - nStep, Pos.y);
							}
						}

					if( wParam == VK_RIGHT ) {

						if( Drop.m_TrackRect.right < xMax ) {

							SetCursorPos(Pos.x + nStep, Pos.y);
							}
						}
					
					if( wParam == VK_UP ) {

						if( Drop.m_TrackRect.top > yMin ) {

							SetCursorPos(Pos.x, Pos.y - nStep);
							}
						}
					
					if( wParam == VK_DOWN ) {

						if( Drop.m_TrackRect.bottom < yMax ) {

							SetCursorPos(Pos.x, Pos.y + nStep);
							}
						}
					}
				}
			}
		}

	return CallNextHookEx(m_hKeyHook, nCode, wParam, lParam);
	}

// End of File
