
#include "intern.hpp"

#include "contrex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Contrex Rotary Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CContrexRotaryDeviceOptions, CUIItem);

// Constructor

CContrexRotaryDeviceOptions::CContrexRotaryDeviceOptions(void)
{
	m_Drop	 = 1;
	m_Type   = 0;
	}

// UI Managament

void CContrexRotaryDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CContrexRotaryDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Type));

	return TRUE;
	}

// Meta Data Creation

void CContrexRotaryDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Type);
	}

//////////////////////////////////////////////////////////////////////////
//
// Contrex M-Trim/ML-Drive Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CContrexTrimDeviceOptions, CUIItem);

// Constructor

CContrexTrimDeviceOptions::CContrexTrimDeviceOptions(void)
{
	m_Drop	 = 1;
	m_Type	 = 0;
	}

// UI Managament

void CContrexTrimDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CContrexTrimDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddByte(BYTE(m_Type));

	return TRUE;
	}

// Meta Data Creation

void CContrexTrimDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(Type);
	}

//////////////////////////////////////////////////////////////////////////
//
// Contrex M-Rotary Comms Driver
//

// Instantiator

ICommsDriver *	Create_ContrexMRotaryDriver(void)
{
	return New CContrexMRotaryDriver;
	}

// Constructor

CContrexMRotaryDriver::CContrexMRotaryDriver(void)
{
	m_wID		= 0x3357;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Contrex";
	
	m_DriverName	= "M-Rotary/M-Cut";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Contrex M-Rotary/M-Cut Driver";

	AddSpaces();
	}

// Binding Control

UINT	CContrexMRotaryDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CContrexMRotaryDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CContrexMRotaryDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CContrexRotaryDeviceOptions);
	}

// Implementation

void	CContrexMRotaryDriver::AddSpaces(void)
{
	AddSpace( New CSpace(1,	"P",	"Parameter",			10,	1, 99,	RR ) );
	AddSpace( New CSpace("ERR",	"Response Error",			3,	LL ) );
	AddSpace( New CSpace("CMD",	"Command",				4,	LL ) );
	}

//////////////////////////////////////////////////////////////////////////
//
// Contrex M-Trim/ML Drive Comms Driver
//

// Instantiator

ICommsDriver *	Create_ContrexMTrimDriver(void)
{
	return New CContrexMTrimDriver;
	}

// Constructor

CContrexMTrimDriver::CContrexMTrimDriver(void)
{
	m_wID		= 0x400A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Contrex";
	
	m_DriverName	= "M(L)-Trim/ML-Drive";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Contrex M(L)-Trim/ML-Drive";

	AddSpaces();
	}

// Binding Control

UINT	CContrexMTrimDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CContrexMTrimDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CContrexMTrimDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CContrexTrimDeviceOptions);
	}

// Implementation

void	CContrexMTrimDriver::AddSpaces(void)
{
	AddSpace( New CSpace(1,	"P",	"Parameter",			10,	0, 99,	RR ) );
	AddSpace( New CSpace("ERR",	"Response Error",			3,	LL ) );
	AddSpace( New CSpace("CMD",	"Command",				4,	LL ) );
	}

// End of File
