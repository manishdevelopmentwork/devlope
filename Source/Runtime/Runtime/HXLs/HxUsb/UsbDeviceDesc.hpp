
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDeviceDesc_HPP

#define	INCLUDE_UsbDeviceDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Descriptor
//

class CUsbDeviceDesc : public UsbDeviceDesc
{
	public:
		// Constructor
		CUsbDeviceDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Init
		void Init(void);

		// Debug
		void Debug(void);
	};

// End of File

#endif
