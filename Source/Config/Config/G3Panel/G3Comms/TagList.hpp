
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagList_HPP

#define INCLUDE_TagList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CTag;

class CTagBlock;

//////////////////////////////////////////////////////////////////////////
//
// Tag List
//

class DLLNOT CTagList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagList(void);

		// Destructor
		~CTagList(void);

		// Item Access
		CTag * GetItem(INDEX Index) const;
		CTag * GetItem(UINT  uPort) const;

		// Item Lookup
		UINT FindNamePos(CString Name) const;

		// Attributes
		BOOL HasCircular(void) const;

		// Operations
		BOOL FindCircular(CStringArray &List);
		void MakeLite(void);

		// Persistance
		void PostLoad(void);
		void Kill(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		CArray <CTagBlock *> m_Blocks;

		// Implementation
		void MakeBlocks(void);
		void KillBlocks(void);

		// Friend Classes
		friend class CTagBlock;
	};

// End of File

#endif
