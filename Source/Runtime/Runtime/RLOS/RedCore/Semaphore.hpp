
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Semaphore_HPP
	
#define	INCLUDE_Semaphore_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Waitable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Semaphore Implementation
//

class CSemaphore : public CWaitable, public ISemaphore
{
	public:
		// Constructor
		CSemaphore(CExecutive *pExec, UINT uCount);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// CWaitable
		bool WaitInit(CExecThread *pThread, UINT uSlot, bool fWait);
		void WaitDone(CExecThread *pThread, UINT uSlot);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// ISemaphore
		void METHOD Signal(UINT uCount);
		bool METHOD Claim(UINT uCount);
		void METHOD ResetState(void);
		
	protected:
		// Data Members
		ULONG        m_uRefs;
		INT volatile m_nCount;
		INT	     m_nInit;
	};

// End of File

#endif
