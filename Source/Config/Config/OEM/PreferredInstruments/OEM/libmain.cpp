
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 OEM Resource DLLs
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load OEM\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading OEM\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// OEM Entry Points
//

CString DLLAPI OemGetCompany(void)
{
	return L"Preferred Instruments";
	}

CString DLLAPI OemGetModels(void)
{
	return L"CO04,CO07,CO07EQ,CO10,CO10EV"				L","
	       L"CA04,CA07,CA07EQ,CA10,CA10EV,CA15"				

		;
	}

WORD DLLAPI OemGetUsbBase(void)
{
	return 0x0000;
	}

BOOL DLLAPI OemGetPair(UINT n, CString &a, CString &b)
{
	static PCTXT Subst[][2] = {

		// Core Strings

		{ L"Red Lion Controls Inc",			L"Preferred Instruments",	},
		{ L"Red Lion Controls",				L"Preferred Instruments",	},
		{ L"Red Lion",					L"Preferred Instruments",	},
		{ L"Crimson 3.1",				L"OIT_Edit_3.1",		},
		{ L"Crimson 3.0",				L"OIT_Edit_3",			},
		{ L"Crimson 3",					L"OIT_Edit_3",			},
		{ L"Crimson device",				L"OIT_Edit Device",		},
		{ L"Crimson",					L"OIT_Edit",			},
		{ L"CR1000 HMI",				L"OIT X Series HMI"		},
		{ L"CR3000 HMI",				L"OIT X-W Series HMI"		},

		// Part Numbers

		{ L"CR10000400000210",				L"OIT-4X",			},
		{ L"CR10000700000210",				L"OIT-7X",			},
		{ L"CR10001000000210",				L"OIT-10X",			},

		{ L"CR30000400000310",				L"OIT-4X-W",			},
		{ L"CR30000700000420",				L"OIT-7X-W",			},
		{ L"CR30001000000420",				L"OIT-10X-W",			},
		{ L"CR30001500000420",				L"OIT-15X-W",			},

		// Colorado

		{ L"CO04",					L"OIT-4X",			},
		{ L"CO07",					L"OIT-7X",			},
		{ L"CO10",					L"OIT-10X",			},

		// Canyon

		{ L"CA04",					L"OIT-4X-W",			},
		{ L"CA07",					L"OIT-7X-W",			},
		{ L"CA10",					L"OIT-10X-W",			},
		{ L"CA15",					L"OIT-15X-W",			},

		// File Extensions

		{ L".cd3",					L".ot3",			},
		{ L".cd31",					L".ot31",			},
		{ L".ci3",					L".oi3",			},

		};

	if( n < elements(Subst) ) {

		a = Subst[n][0];
		b = Subst[n][1];

		return TRUE;
		}

	return FALSE;
	}

BOOL DLLAPI OemGetFeature(CString &f, BOOL d)
{
	if( f == L"PoweredBy"    ) return TRUE;
	if( f == L"Registration" ) return FALSE;
	if( f == L"BuildNotes"   ) return FALSE;
	if( f == L"Support"      ) return FALSE;
	if( f == L"Update"       ) return FALSE;
	if( f == L"Information" ) return FALSE;
	if( f == L"ShowSplash"   ) return TRUE;
	if( f == L"FuncHelp"     ) return TRUE;
	if( f == L"Import"	 ) return TRUE;
	if( f == L"Localize"     ) return FALSE;
	if( f == L"OPCProxy"     ) return TRUE;
	if( f == L"SMS"          ) return TRUE;

	return d;
	}

CString DLLAPI OemGetOption(CString const &f, CString const &d)
{
	return d;
	}

BOOL DLLAPI OemAllowDriver(UINT uIdent, BOOL fDefault)
{
	switch( uIdent ) {

		// CDL Drivers
		case 0x403D: return FALSE;
		case 0x404F: return TRUE;
		case 0x407E: return FALSE;

		// CCP Driver
		case 0x40E0: return FALSE;
		}

	return fDefault;
	}

DWORD DLLAPI OemGetColor(CString &c, DWORD d)
{
	return d;
	}

// End of File
