
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIExprDropEdit_HPP

#define INCLUDE_UIExprDropEdit_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Programmable Drop Edit
//

class CUIExprDropEdit : public CUIDropEdit
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIExprDropEdit(void);

	protected:
		// Data Members
		UINT m_cfCode;

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);
	};

// End of File

#endif
