
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Mspic437_HPP
	
#define	INCLUDE_AM437_Mspi437_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CClock437;

//////////////////////////////////////////////////////////////////////////
//
// AM437 Multichannel Serial Port Interface Module
//

class CMspi437 : public ISpi, public IEventSink
{
	public:
		// Constructor
		CMspi437(CClock437 *pClock);

		// Destructor
		~CMspi437(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISpi
		BOOL METHOD Init(UINT uChan, CSpiConfig const &Config);
		BOOL METHOD Send(UINT uChan, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData);
		BOOL METHOD Recv(UINT uChan, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Registers
		enum
		{
			regHLREV	= 0x0000 / sizeof(DWORD),
			regHLHWINFO	= 0x0004 / sizeof(DWORD),
			regHLSYSCONFIG	= 0x0010 / sizeof(DWORD),
			regREVISION	= 0x0100 / sizeof(DWORD),
			regSYSCONFIG	= 0x0110 / sizeof(DWORD),
			regSYSSTS	= 0x0114 / sizeof(DWORD),
			regIRQSTS	= 0x0118 / sizeof(DWORD),
			regIRQEN	= 0x011C / sizeof(DWORD),
			regWAKEUPEN	= 0x0120 / sizeof(DWORD),
			regSYST		= 0x0124 / sizeof(DWORD),
			regCTRL		= 0x0128 / sizeof(DWORD),
			regCHCONF	= 0x012C / sizeof(DWORD),
			regCHSTAT	= 0x0130 / sizeof(DWORD),
			regCHCTRL	= 0x0134 / sizeof(DWORD),
			regTXDATA	= 0x0138 / sizeof(DWORD),
			regRXDATA	= 0x013C / sizeof(DWORD),
			regXFERLEV	= 0x017C / sizeof(DWORD),
			regDAFTX	= 0x0180 / sizeof(DWORD),
			regDAFRX	= 0x01A0 / sizeof(DWORD),
			};

		// Channel Configuration & Control
		enum
		{
			bitClkExt	= Bit(29),
			bitFifoRxEn	= Bit(28),
			bitFifoTxEn	= Bit(27),
			bitChipSelDly	= 25,
			bitStartBitPol	= Bit(24),
			bitStartBitEn	= Bit(23),
			bitForce	= Bit(20),
			bitTurbo	= Bit(19),
			bitInputD0	= Bit(18),
			bitNoTxD1	= Bit(17),
			bitNoTxD0	= Bit(16),
			bitDmaRxEn	= Bit(15),
			bitDmaTxEn	= Bit(14),
			bitMode		= 12,
			bitClockDivHi	= 8,
			bitWordLen	= 7,
			bitChipSelPol	= Bit(6),
			bitClockDivLo	= 2,
			bitClockPol	= Bit(1),
			bitClockPha	= Bit(0),
			bitChanEn	= Bit(0),
			};

		// Interrupts
		enum
		{
			intEow		= Bit(17),
			intWke		= Bit(16),
			intRx3Full	= Bit(14),
			intTx3Under	= Bit(13),
			intTx3Empty	= Bit(12),
			intRx2Full	= Bit(10),
			intTx2Under	= Bit(9),
			intTx2Empty	= Bit(8),
			intRx1Full	= Bit(6),
			intTx1Under	= Bit(5),
			intTx1Empty	= Bit(4),
			intRx0Over	= Bit(3),
			intRx0Full	= Bit(2),
			intTx0Under	= Bit(1),
			intTx0Empty	= Bit(0),
			intRxOver	= intRx0Over,
			intRxData	= intRx0Full  | intRx1Full  | intRx2Full,
			intRx		= intRxOver   | intRxData,
			intTxUnder	= intTx0Under | intTx1Under | intTx2Under,
			intTxEmpty	= intTx0Empty | intTx1Empty | intTx2Empty,
			intTx		= intTxUnder  | intTxEmpty,
			intTxRx		= intTx | intRx,
			};

		// Status
		enum
		{
			bitRxFifoFull	= Bit(6),
			bitRxFifoEmpty	= Bit(5),
			bitTxFifoFull	= Bit(4),
			bitTxFifoEmpty	= Bit(3),
			bitEot		= Bit(2),
			bitTxEmpty	= Bit(1),
			bitRxFull	= Bit(0),
			};

		// Modes
		enum
		{
			modeTxRx	= 0,
			modeRx		= 1,
			modeTx		= 2,
			};

		// Request
		struct CRequest
		{
			UINT		m_uChan;
			PBYTE		m_pHead;
			UINT		m_uHead;
			PBYTE		m_pData;
			UINT		m_uData;
			UINT		m_uMode;
			UINT  volatile	m_uSend;
			UINT  volatile	m_uRecv;
			bool  volatile	m_fPoll;
			bool  volatile	m_fDone;
			};

		// Data Members
		PVDWORD	             m_pBase;
		UINT                 m_uRefs;
		IMutex		   * m_pMutex;
		IEvent             * m_pEvent;
		CRequest           * m_pList[16];
		CRequest * volatile  m_pReq;
		UINT       volatile  m_uHead;
		UINT	   volatile  m_uTail;
		UINT	             m_uLine;
		UINT	             m_uClock;

		// Events
		void OnTransfer(void);
		void OnRxOver(void);
		void OnRxData(void);
		void OnTxUnder(void);
		void OnTxEmpty(void);

		// Implementation
		void InitController(void);
		void InitEvents(void);
		bool Request(CRequest *pReq);
		void StartComms(void);
		void WriteFifo(void);
		UINT WriteFifo(PBYTE pData, UINT uCount);
		void ReadFifo(void);
		UINT ReadFifo(PBYTE pData, UINT uCount);
		void InitWait(CRequest *pReq);
		void TermWait(CRequest *pReq);
		void WaitDone(CRequest *pReq);
		void CommsDone(CRequest *pReq);
		void Reset(void);
		WORD FindDivider(UINT uFreq);
	};

// End of File

#endif
