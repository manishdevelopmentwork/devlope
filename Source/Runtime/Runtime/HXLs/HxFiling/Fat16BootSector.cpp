
#include "Intern.hpp"

#include "Fat16BootSector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Fat16 Boot Sector Object
//

// Constructor

CFat16BootSector::CFat16BootSector(void)
{
	memset(this, 0x00, sizeof(Fat16BootSector));
	}

CFat16BootSector::CFat16BootSector(CPartitionEntry const &Partition)
{
	Init(Partition);
	}

// Initialisation

void CFat16BootSector::Init(CPartitionEntry const &Partition)
{
	memset(this, 0x00, sizeof(Fat16BootSector));

	m_bBootCode[0] = 0xEB;

	m_bBootCode[1] = 0x3C;

	m_bBootCode[2] = 0x90;
	
	m_bDriveNum    = 0x80;
	
	m_bBootSig     = constExtended;

	m_wMagic       = constMagic;
	
	m_dwVolId      = GetTickCount();

	memcpy(m_bOemName,    "MSDOS5.0",    sizeof(m_bOemName));

	memcpy(m_bVolLabel,   "NO NAME    ", sizeof(m_bVolLabel));

	memcpy(m_bFileSystem, "FAT16   ",    sizeof(m_bFileSystem));

	GetBiosParamBlock().Init(Partition);
	}

// Conversion

void CFat16BootSector::HostToLocal(void)
{
	GetBiosParamBlock().HostToLocal();
	
	m_dwVolId = HostToIntel(m_dwVolId);

	m_wMagic  = HostToIntel(m_wMagic);
	}

void CFat16BootSector::LocalToHost(void)
{
	GetBiosParamBlock().LocalToHost();
	
	m_dwVolId = IntelToHost(m_dwVolId);

	m_wMagic  = IntelToHost(m_wMagic);
	}

// Attributes

BOOL CFat16BootSector::IsValid(void) const
{
	if( m_wMagic != constMagic ) {

		return false;
		}
	
	if( m_bBootSig == constExtended ) {

		if( memcmp(m_bFileSystem, "FAT16   ", 8) ) {
			
			return false;
			}
		}

	if( !GetBiosParamBlock().IsValid() ) {

		return false;
		}

	return true;
	}

UINT CFat16BootSector::GetClusterSize(void) const
{
	return GetBiosParamBlock().GetClusterSize();
	}

UINT CFat16BootSector::GetRootDirSize(void) const
{
	return GetBiosParamBlock().GetRootDirSize();
	}

UINT CFat16BootSector::GetRootDirSectors(void) const
{
	return GetBiosParamBlock().GetRootDirSectors();
	}

INT64 CFat16BootSector::GetDataSize(void) const
{
	return GetBiosParamBlock().GetDataSize();
	}

DWORD CFat16BootSector::GetDataSectors(void) const
{
	return GetBiosParamBlock().GetDataSectors();
	}

DWORD CFat16BootSector::GetDataClusters(void) const
{
	return GetBiosParamBlock().GetDataClusters();
	}

DWORD CFat16BootSector::GetFirstSector(DWORD dwCluster) const
{
	return GetBiosParamBlock().GetFirstSector(dwCluster);
	}

DWORD CFat16BootSector::GetFirstFatSector(WORD wFat) const
{
	return GetBiosParamBlock().GetFirstFatSector(wFat);
	}

DWORD CFat16BootSector::GetFirstRootDirSector(void) const
{
	return GetBiosParamBlock().GetFirstRootDirSector();
	}

DWORD CFat16BootSector::GetFirstDataSector(void) const
{
	return GetBiosParamBlock().GetFirstDataSector();
	}

DWORD CFat16BootSector::GetFatSectors(void) const
{
	return m_BiosParamBlock.m_wFat16Size;
	}

DWORD CFat16BootSector::GetFatPerSector(void) const
{
	return GetBiosParamBlock().GetFatPerSector();
	}

DWORD CFat16BootSector::GetTotalSectors(void) const
{
	return GetBiosParamBlock().GetTotalSectors();
	}

// Bios Parameter Block

CFat16BiosParamBlock & CFat16BootSector::GetBiosParamBlock(void) const
{
	return (CFat16BiosParamBlock &) m_BiosParamBlock;
	}

// Dump

void CFat16BootSector::Dump(void) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nFat16 Boot Sector\n");

	AfxTrace("Status              = %s\n",      IsValid() ? "OK" : "Invalid");
	AfxTrace("Boot Code           = %2.2X %2.2X %2.2X\n", m_bBootCode[0], m_bBootCode[1], m_bBootCode[2]);
	AfxTrace("OEM Name            = %s\n",      m_bOemName);
	AfxTrace("Driver Number       = 0x%2.2X\n", m_bDriveNum);
	AfxTrace("Boot Signature      = 0x%2.2X\n", m_bBootSig);
	AfxTrace("Voluem Id           = 0x%8.8X\n", m_dwVolId);
	AfxTrace("Volume Label        = %11s\n",    m_bVolLabel);
	AfxTrace("File System         = %8s\n",     m_bFileSystem);
	AfxTrace("Magic               = 0x%4.4X\n", m_wMagic);

	GetBiosParamBlock().Dump();

	#endif
	}

// End of File