
#include "Intern.hpp"

#include "HttpServerAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServerConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Authentication Method
//

// Operations

BOOL CHttpServerAuth::CanAccept(CString Meth)
{
	return FALSE;
	}

CString CHttpServerAuth::GetAuthHeader(CString Opaque, BOOL fStale)
{
	return "";
	}

UINT CHttpServerAuth::ProcessRequest(CHttpServerRequest *pReq, CString Line)
{
	return CHttpServerConnection::authFail;
	}

CString CHttpServerAuth::GetUser(CHttpServerRequest *pReq)
{
	return "";
	}

BOOL CHttpServerAuth::CheckPass(CHttpServerRequest *pReq, CString Pass)
{
	return FALSE;
	}

void CHttpServerAuth::ClearSession(CString Opaque)
{
	}

// End of File
