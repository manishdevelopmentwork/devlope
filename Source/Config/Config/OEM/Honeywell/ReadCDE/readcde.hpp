
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Honeywell CDE File Import
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_READCDE_HPP
	
#define	INCLUDE_READCDE_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <G3Comms.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_READCDE

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#endif

// End of File

#endif
