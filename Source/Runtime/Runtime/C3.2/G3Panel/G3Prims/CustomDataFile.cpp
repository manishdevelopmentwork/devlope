
#include "intern.hpp"

#include "CustomDataFile.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Custom Data Log File
//

// Constructor

CCustomDataFile::CCustomDataFile(CString const &Root, CUnicode const &File)
{
	m_Root  = Root;

	m_File  = File;

	m_pFile = Create_File();
	}

// Destructor

CCustomDataFile::~CCustomDataFile(void)
{
	m_pFile->Release();
	}

// Operations

BOOL CCustomDataFile::LoadFile(CUnicode const &File)
{
	m_Root.Replace('/', '\\');

	char  cDrive = m_Root[1] == ':' ? m_Root[0] : 'C';	

	if( FindFileSystem(cDrive) ) {

		if( m_pFileSys->ChangeDir(PCTXT(m_Root)) ) {

			CFilename const &Name = CFilename(UniConvert(File));

			AfxTrace("load file <%s><%s>\n", PCTXT(m_Root), PCTXT(Name));

			if( m_pFile->Open(Name, fileRead) ) {

				LoadData(m_pFile);

				m_pFile->Close();

				FreeFileSystem();

				return TRUE;
				}

			AfxTrace("open failed\n");
			}

		FreeFileSystem();
		}

	return FALSE;
	}

// Filing System Helpers

BOOL CCustomDataFile::FindFileSystem(char cDrive)
{
	return (m_pFileSys = AfxGetSingleton(IFileManager)->LockFileSystem(cDrive)) != NULL;
	}

void CCustomDataFile::FreeFileSystem(void)
{
	AfxGetSingleton(IFileManager)->FreeFileSystem(m_pFileSys, true);
	}

// Implementation

BOOL CCustomDataFile::IsCustom(void)
{
	DWORD dwHead;

	UINT uRead  = m_pFile->Read(PBYTE(&dwHead), sizeof(dwHead));

	if( uRead == sizeof(dwHead) ) {

		return dwHead == 0xFF00DDFF;
		}

	return FALSE;
	}

BOOL CCustomDataFile::IsRedLion(void)
{
	DWORD dwHead;

	UINT uRead  = m_pFile->Read(PBYTE(&dwHead), sizeof(dwHead));

	if( uRead == sizeof(dwHead) ) {

		return dwHead == 0x32574152;
		}

	return FALSE;
	}

void CCustomDataFile::LoadData(IFile *pFile)
{
	AfxTrace("CCustomDataFile::LoadData\n");

	DWORD dwHead;

	UINT uRead  = pFile->Read(PBYTE(&dwHead), sizeof(dwHead));

	if( uRead ) {

		AfxTrace(" head 0x%8.8X\n", dwHead);

		if( dwHead == 0x32574152 ) {

			LoadRedLionData(pFile);
			}

		if( dwHead == 0xFF00DDFF ) {

			LoadMurphyData(pFile);
			}
		}
	}

void CCustomDataFile::LoadRedLionData(IFile *pFile)
{
	AfxTrace("CPrimTrendViewer::LoadRedLionData\n");

	UINT uTags;

	pFile->Read(PBYTE(&uTags), sizeof(uTags));

	PBYTE pTags = New BYTE [ uTags * sizeof(DWORD) ];

	pFile->Read(pTags, uTags * sizeof(DWORD));

	PBYTE pTypes = New BYTE [ uTags * sizeof(DWORD) ];

	pFile->Read(pTypes, uTags * sizeof(DWORD));

	AfxTrace("tags %u\n", uTags);

	delete [] pTags;

	delete [] pTypes;
	}

void CCustomDataFile::LoadMurphyData(IFile *pFile)
{
	AfxTrace("CPrimTrendViewer::LoadMurphyData\n");

	struct CHead {

		DWORD dwChans;
		DWORD dwStart;
		DWORD dwPitch;
		DWORD dwCount;
		};

	CHead Head;

	UINT uHead = pFile->Read(PBYTE(&Head), sizeof(Head));

	if( uHead ) {

		AfxTrace("chans %u\n", Head.dwChans);
		AfxTrace("start %u\n", Head.dwStart);
		AfxTrace("pitch %u\n", Head.dwPitch);
		AfxTrace("count %u\n", Head.dwCount);		

		for( UINT n = 0; n < Head.dwChans; n ++ ) {
		
			struct CInfo {
			
				DWORD dwMin;
				DWORD dwMax;
				DWORD dwDPs;
				};

			CInfo Info;

			pFile->Read(PBYTE(&Info), sizeof(Info));

			AfxTrace("min %u, max %u, dps %u\n", 
				  Info.dwMin, 
				  Info.dwMax, 
				  Info.dwDPs
				  );
			}
		}

	if( TRUE ) {

		UINT  uData = Head.dwChans * Head.dwCount * sizeof(DWORD);

		PBYTE pData = New BYTE [ uData ];

		memset(pData, 0, uData);

		UINT uRead = pFile->Read(pData, uData);

		if( uRead ) {

			AfxTrace("data %u, read %u\n", uData, uRead);

			PDWORD pScan = PDWORD(pData);

			for( UINT uSamp = 0; uSamp < Head.dwCount; uSamp ++ ) {

				AfxTrace("[%u]\t", uSamp);

				for( UINT uChan = 0; uChan < Head.dwChans; uChan ++ ) {

					AfxTrace("<%u>", *pScan++);
					}

				AfxTrace("\n");
				}
			}

		delete [] pData;
		}
	}

// End of File
