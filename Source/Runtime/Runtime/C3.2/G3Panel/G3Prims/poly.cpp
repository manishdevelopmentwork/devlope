
#include "intern.hpp"

#include "poly.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline
//

// Constructor

CPrimPoly::CPrimPoly(void)
{
	m_uCount   = 0;

	m_dwRound  = 0;

	m_pList    = NULL;

	m_pSpinPos = NULL;

	m_pSpinMin = NULL;

	m_pSpinMax = NULL;

	m_fInit    = FALSE;
	}

// Destructor

CPrimPoly::~CPrimPoly(void)
{
	delete m_pSpinPos;

	delete m_pSpinMin;
	
	delete m_pSpinMax;

	delete [] m_pList;
	}

// Initialization

void CPrimPoly::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimPoly", pData);

	CPrimGeom::Load(pData);

	if( (m_uCount = GetByte(pData)) ) {

		m_pList = New P2 [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			m_pList[n].x = GetWord(pData);
			
			m_pList[n].y = GetWord(pData);
			}
		}

	m_dwRound = GetLong(pData);

	GetCoded(pData, m_pSpinPos);

	GetCoded(pData, m_pSpinMin);

	GetCoded(pData, m_pSpinMax);
	}

// Overridables

void CPrimPoly::MovePrim(int cx, int cy)
{
	if( cx || cy ) {

		if( m_uCount ) {

			for( UINT n = 0; n < m_uCount; n++ ) {

				m_pList[n].x += cx;
				
				m_pList[n].y += cy;
				}
			}

		CPrimGeom::MovePrim(cx, cy);
		}
	}

void CPrimPoly::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	if( TRUE ) {

		CPrimGeom::DrawPrep(pGDI, Erase, Trans);
		}

	if( !m_fInit ) {

		if( m_pSpinPos ) {

			int xp = (m_DrawRect.x1 + m_DrawRect.x2) / 2;

			int yp = (m_DrawRect.y1 + m_DrawRect.y2) / 2;

			int cx = (m_DrawRect.x2 - m_DrawRect.x1) / 2;

			int cy = (m_DrawRect.y2 - m_DrawRect.y1) / 2;

			int sm = 0;

			for( UINT n = 0; n < m_uCount; n++ ) {

				int dx = abs(m_pList[n].x - xp);

				int dy = abs(m_pList[n].y - yp);

				int ds = int(sqrt(double(dx*dx+dy*dy))+1);

				MakeMax(sm, ds);
				}

			pGDI->MakeIdentity(m_mScale);

			m_mScale.e[0][0] *= min(cx,cy)-2;
			m_mScale.e[1][1] *= min(cx,cy)-2;

			m_mScale.e[0][0] /= sm;
			m_mScale.e[1][1] /= sm;

			m_fTrans = TRUE;
			}
		
		m_Ctx.m_t = 0;

		m_fInit   = TRUE;
		}

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_fChange ) {

			Erase.Append(m_DrawRect);
			}
		}
	}

void CPrimPoly::DrawPrim(IGDI *pGDI)
{
	if( m_uCount ) {

		if( m_pSpinPos ) {

			GuardTask(TRUE);

			int xp = (m_DrawRect.x1 + m_DrawRect.x2) / 2;

			int yp = (m_DrawRect.y1 + m_DrawRect.y2) / 2;
			
			pGDI->AddMovement (-xp, -yp);

			pGDI->AddTransform(m_mScale);

			pGDI->AddRotation (m_Ctx.m_t);

			pGDI->AddMovement (+xp, +yp);

			m_pFill->FillPolygon(pGDI, m_pList, m_uCount, m_dwRound);

			m_pEdge->DrawPolygon(pGDI, m_pList, m_uCount, m_dwRound);

			pGDI->SetIdentity();

			GuardTask(FALSE);
			}
		else {
			m_pFill->FillPolygon(pGDI, m_pList, m_uCount, m_dwRound);

			m_pEdge->DrawPolygon(pGDI, m_pList, m_uCount, m_dwRound);
			}
		}

	CPrimWithText::DrawPrim(pGDI);
	}

// Context Creation

void CPrimPoly::FindCtx(CCtx &Ctx)
{
	Ctx.m_t = 0;

	if( m_pSpinPos ) {

		if( IsItemAvail(m_pSpinPos) && IsItemAvail(m_pSpinMin) && IsItemAvail(m_pSpinMax) ) {

			C3REAL pos = GetItemData(m_pSpinPos, C3REAL(  0));
			
			C3REAL min = GetItemData(m_pSpinMin, C3REAL(  0));
			
			C3REAL max = GetItemData(m_pSpinMax, C3REAL(360));

			if( max - min ) {

				Ctx.m_t = int(360 * (pos - min) / (max - min));
				}
			}
		}
	}

// Context Check

BOOL CPrimPoly::CCtx::operator == (CCtx const &That) const
{
	return m_t == That.m_t;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline (Text Outside Figure)
//

// Constructor

CPrimPolyTrans::CPrimPolyTrans(void)
{
	m_fTrans = TRUE;
	}

// Destructor

CPrimPolyTrans::~CPrimPolyTrans(void)
{
	}

// End of File
