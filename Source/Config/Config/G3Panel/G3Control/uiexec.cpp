
#include "intern.hpp"

#include "projview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Program Execution View Window
//

// Runtime Class

AfxImplementRuntimeClass(CProgramExecutionViewWnd, CViewWnd);

// Constructor

CProgramExecutionViewWnd::CProgramExecutionViewWnd(void)
{
	m_pTool     = New CToolbarWnd(barTight);

	m_pTree     = New CTreeView;

	m_hRoot     = NULL;

	m_hSelect   = NULL;

	m_fLoading  = FALSE;

	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CtrlTreeIcon16"), afxColor(MAGENTA));
	}

// Operations

void CProgramExecutionViewWnd::Load(void)
{
	m_pTree->DeleteChildren(m_hRoot);

	m_pTree->DeleteItem(m_hRoot);

	m_fLoading = TRUE;

	m_pTree->SetRedraw(FALSE);
	
	LoadRoot();

	LoadTree();

	m_pTree->Expand(m_hRoot, TVE_EXPAND);

	m_fLoading = FALSE;

	m_hSelect  = m_hRoot;

	m_pTree->SetRedraw(TRUE);
	}

CString CProgramExecutionViewWnd::Save(void)
{
	return CString();
	}

void CProgramExecutionViewWnd::Undo(CCmd *pCmd)
{
	OnUndo(pCmd);
	}

void CProgramExecutionViewWnd::Exec(CCmd *pCmd)
{
	OnExec(pCmd);
	}

// Overribables

void CProgramExecutionViewWnd::OnAttach(void)
{
	m_pItem     = (CProgramExecution *) m_pItem;

	m_pProject  = (CControlProject *) m_pItem->GetParent(AfxRuntimeClass(CControlProject));

	m_dwProject = m_pProject->GetHandle();
	}

BOOL CProgramExecutionViewWnd::OnNavigate(CString const &Nav)
{
	CViewWnd::OnNavigate(Nav);

	return TRUE;
	}

void CProgramExecutionViewWnd::OnExec(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CControlProjectView::CCmdMove) ) {

		CControlProjectView::CCmdMove *pMove = (CControlProjectView::CCmdMove *) pCmd;

		ExecMoveList(pMove->m_dwProg, pMove->m_Move);

		return;
		}
	}

void CProgramExecutionViewWnd::OnUndo(CCmd *pCmd)
{
	AfxAssume(pCmd);

	CLASS Class = AfxPointerClass(pCmd);

	if( Class == AfxRuntimeClass(CControlProjectView::CCmdMove) ) {

		CControlProjectView::CCmdMove *pMove = (CControlProjectView::CCmdMove *) pCmd;

		ExecMoveList(pMove->m_dwProg, pMove->m_Undo);

		return;
		}
	}

// Message Map

AfxMessageMap(CProgramExecutionViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_GOINGIDLE)
	AfxDispatchMessage(WM_UPDATEUI)

	AfxDispatchNotify(100, TVN_SELCHANGED,    OnTreeSelChanged)

	AfxDispatchGetInfoType(0xC0, OnToolGetInfo)
	AfxDispatchControlType(0xC0, OnToolControl)
	AfxDispatchCommandType(0xC0, OnToolCommand)

	AfxMessageEnd(CProgramExecutionViewWnd)
	};

// Message Handlers

void CProgramExecutionViewWnd::OnSize(UINT uCode, CSize Size)
{
	CRect Rect  = GetClientRect();

	CRect Tool  = Rect;

	CRect Tree  = Rect;

	Tool.bottom = Tree.top    = Tool.top    + m_nTop;

	m_pTool->MoveWindow(Tool, TRUE);
	
	m_pTree->MoveWindow(Tree, TRUE);
	}

void CProgramExecutionViewWnd::OnSetFocus(CWnd &Wnd)
{
	m_pTree->SetFocus();
	}

void CProgramExecutionViewWnd::OnPostCreate(void)
{
	SendMessage(WM_UPDATEUI);

	m_nTop      = m_pTool->GetHeight();

	m_nBotton   = 18;

	CRect Rect  = GetClientRect();

	CRect Tool  = Rect;

	CRect Tree  = Rect;

	Tool.bottom = Tree.top    = Tool.top    + m_nTop;

	CreateTool(Tool);

	CreateTree(Tree);

	m_pTool->PollGadgets();

	m_Proxy.Bind(this);
	}

void CProgramExecutionViewWnd::OnGoingIdle(void)
{
	m_pTool->PollGadgets();
	}

void CProgramExecutionViewWnd::OnUpdateUI(void)
{
	m_pTool->AddGadget(New CButtonGadget(0xC002, 0x1000001F, CString(IDS_MOVE_DOWN)));

	m_pTool->AddGadget(New CButtonGadget(0xC001, 0x10000020, CString(IDS_MOVE_UP)));

	m_pTool->AddGadget(New CRidgeGadget);
	}

// Notification Handlers

void CProgramExecutionViewWnd::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	if( !m_fLoading ) {

		if( m_hSelect != Info.itemNew.hItem ) {

			if( Info.action == TVC_BYMOUSE ) {
				
				SetFocus();
				}

			m_hSelect = Info.itemNew.hItem;

			NewItemSelected();
			}
		}
	}

// Command Handlers

BOOL CProgramExecutionViewWnd::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case 0xC001:
			Info.m_Prompt = CString(IDS_MOVE_PROGRAM_UP);
			break;

		case 0xC002:
			Info.m_Prompt = CString(IDS_MOVE_PROGRAM_DOWN);
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CProgramExecutionViewWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case 0xC001:

			Src.EnableItem(CanMoveUp());

			break;

		case 0xC002:

			Src.EnableItem(CanMoveDown());

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CProgramExecutionViewWnd::OnToolCommand(UINT uID)
{
	switch( uID ) {

		case 0xC001:

			OnMoveUp();

			break;

		case 0xC002:

			OnMoveDown();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CProgramExecutionViewWnd::CanMoveUp(void)
{
	if( m_hSelect ) {

		if( m_hSelect != m_hRoot ) {

			CLongArray Progs;

			UINT c;

			if( (c = afxDatabase->GetPrograms( m_dwProject, sectSfcMain, Progs)) ) {

				DWORD dwProg = m_pTree->GetItemParam(m_hSelect);
				
				if( dwProg == Progs[0] ) {
					
					return FALSE;
					}
				}

			HTREEITEM hPrev = m_pTree->GetPrevious(m_hSelect);

			return hPrev > 0;
			}
		}

	return FALSE;
	}

BOOL CProgramExecutionViewWnd::CanMoveDown(void)
{
	if( m_hSelect ) {

		if( m_hSelect != m_hRoot ) {

			CLongArray Progs;

			UINT c;

			if( (c = afxDatabase->GetPrograms( m_dwProject, sectSfcMain, Progs)) ) {

				DWORD dwProg = m_pTree->GetItemParam(m_hSelect);
				
				if( dwProg == Progs[c - 1] ) {
					
					return FALSE;
					}
				}

			HTREEITEM hNext = m_pTree->GetNext(m_hSelect);

			return hNext > 0;
			}
		}

	return FALSE;
	}

void CProgramExecutionViewWnd::OnMoveUp(void)
{
	HTREEITEM hItem = m_hSelect;

	HTREEITEM hPrev = m_pTree->GetPrevious(hItem);

	HTREEITEM hNext = m_pTree->GetNext(hItem);

	DWORD    dwProg = m_pTree->GetItemParam(hItem);

	DWORD    dwNext = m_pTree->GetItemParam(hNext);

	DWORD    dwPrev = m_pTree->GetItemParam(hPrev);

	CMoveList Exec;

	CMoveList Undo;

	if( CanMoveProgram(dwProg, moveUp, dwPrev) ) {		

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveUp;
			Cmnd.m_dwProg = 0;

			Exec.Append(Cmnd);
			}

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveDown;
			Cmnd.m_dwProg = 0;

			Undo.Append(Cmnd);
			}
		}
	else {
		CLongArray Begin;

		UINT c = afxDatabase->GetPrograms( m_dwProject, sectBegin, Begin);

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveBegin;
			Cmnd.m_dwProg = 0;

			Exec.Append(Cmnd);
			}

		if( c ) {

			CLongArray Sfc;

			if( afxDatabase->GetPrograms( m_dwProject, sectSfcMain, Sfc) ) {
				
				CMoveDesc Cmnd;

				Cmnd.m_dwMove = moveAfter;
				Cmnd.m_dwProg = Begin[c - 1];

				Exec.Append(Cmnd);
				}
			else {
				CMoveDesc Cmnd;

				Cmnd.m_dwMove = moveBefore;
				Cmnd.m_dwProg = Begin[c - 1];

				Exec.Append(Cmnd);
				}
			}

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveEnd;
			Cmnd.m_dwProg = 0;

			Undo.Append(Cmnd);
			}

		if( dwNext ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveBefore;
			Cmnd.m_dwProg = dwNext;

			Undo.Append(Cmnd);
			}
		}

	CString Item = m_pItem->GetFixedPath();

	CControlProjectView::CCmdMove *pMove = New CControlProjectView::CCmdMove( Item,
										  dwProg,
										  Exec,
										  Undo												  
										  );
	m_Proxy.ExecCmd(pMove);
	}

void CProgramExecutionViewWnd::OnMoveDown(void)
{
	HTREEITEM hItem = m_hSelect;

	HTREEITEM hPrev = m_pTree->GetPrevious(hItem);

	HTREEITEM hNext = m_pTree->GetNext(hItem);

	DWORD    dwProg = m_pTree->GetItemParam(hItem);

	DWORD    dwNext = m_pTree->GetItemParam(hNext);

	DWORD    dwPrev = m_pTree->GetItemParam(hPrev);

	CMoveList Exec;

	CMoveList Undo;

	if( CanMoveProgram(dwProg, moveDown, dwNext) ) {

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveDown;
			Cmnd.m_dwProg = 0;

			Exec.Append(Cmnd);
			}

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveUp;
			Cmnd.m_dwProg = 0;

			Undo.Append(Cmnd);
			}
		}
	else {
		CLongArray End;

		UINT c = afxDatabase->GetPrograms( m_dwProject, sectEnd, End);

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveEnd;
			Cmnd.m_dwProg = 0;

			Exec.Append(Cmnd);
			}

		if( c ) {

			CLongArray Sfc;

			if( afxDatabase->GetPrograms( m_dwProject, sectSfcMain, Sfc) ) {

				CMoveDesc Cmnd;

				Cmnd.m_dwMove = moveBefore;
				Cmnd.m_dwProg = End[0];
				
				Exec.Append(Cmnd);
				}
			else {
				CMoveDesc Cmnd;

				Cmnd.m_dwMove = moveAfter;
				Cmnd.m_dwProg = End[0];
				
				Exec.Append(Cmnd);
				}			
			}

		if( TRUE ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveBegin;
			Cmnd.m_dwProg = 0;

			Undo.Append(Cmnd);
			}

		if( dwPrev ) {

			CMoveDesc Cmnd;

			Cmnd.m_dwMove = moveAfter;
			Cmnd.m_dwProg = dwPrev;

			Undo.Append(Cmnd);
			}
		}

	CString Item = m_pItem->GetFixedPath();

	CControlProjectView::CCmdMove *pMove = New CControlProjectView::CCmdMove( Item,
										  dwProg,
										  Exec,
										  Undo
										  );
	m_Proxy.ExecCmd(pMove);
	}

// Tree Loading

void CProgramExecutionViewWnd::LoadRoot(void)
{
	CTreeViewItem Root;

	Root.SetText  (L"Programs");
	
	Root.SetParam (LPARAM(NOTHING));

	Root.SetImages(IDI_CTRL);

	m_hRoot = m_pTree->InsertItem(NULL, NULL, Root);

	m_MapNames.Insert(L"", m_hRoot);
	}

void CProgramExecutionViewWnd::LoadTree(void)
{
	CLongArray Progs;

	DWORD dwSection = sectMain;

	if( afxDatabase->GetPrograms( m_dwProject, dwSection, Progs) ) {
		
		for( UINT n = 0; n < Progs.GetCount(); n ++ ) {
			
			DWORD dwProgram = Progs[n];

			CStratonProgramDescriptor Desc(m_dwProject, dwProgram);

			if( !Desc.IsCalled() ) {
				
				CTreeViewItem Node;

				GetNodeCode(Node, dwProgram);

				HTREEITEM hItem = m_pTree->InsertItem( m_hRoot, NULL, Node);

				m_MapNames.Insert(Desc.m_Name, hItem);
				}
			}
		}
	}

// Implementation

BOOL CProgramExecutionViewWnd::GetNodeCode(CTreeViewItem &Node, CString Code)
{
	DWORD dwProgram = wcstoul(Code, NULL, 10);

	return GetNodeCode(Node, dwProgram);
	}

BOOL CProgramExecutionViewWnd::GetNodeCode(CTreeViewItem &Node, DWORD dwProgram)
{
	CStratonProgramDescriptor Desc(m_dwProject, dwProgram);

	Node.SetText  (Desc.m_Name);
	
	Node.SetParam (Desc.m_dwHandle);

	Node.SetImages(GetTreeImage(dwProgram));

	return TRUE;
	}

UINT CProgramExecutionViewWnd::GetTreeImage(DWORD dwProgram)
{
	CStratonProgramDescriptor Desc(m_dwProject, dwProgram);

	switch( Desc.m_dwLanguage ) {

		case langSfc:	return Desc.IsEnabled() ? IDI_PROGRAM_SFC_ENABLED : IDI_PROGRAM_SFC_DISABLED;
		
		default:	return Desc.IsEnabled() ? IDI_PROGRAM_ENABLED : IDI_PROGRAM_DISABLED;
		}
	}

void CProgramExecutionViewWnd::RefreshTree(void)
{
	CString ss = SaveSelState();

	m_fLoading = TRUE;

	m_pTree->SetRedraw(FALSE);

	KillTree(m_hRoot);

	LoadRoot();

	LoadTree();

	m_pTree->Expand(m_hRoot, TVE_EXPAND);

	m_fLoading = FALSE;

	LoadSelState(ss);

	m_pTree->SetRedraw(TRUE);

	m_pTree->Invalidate(TRUE);
	}

void CProgramExecutionViewWnd::KillTree(HTREEITEM hRoot)
{
	HTREEITEM hItem;

	if( hItem = m_pTree->GetChild(hRoot) ) {

		for(;;) {

			HTREEITEM hNext = m_pTree->GetNext(hItem);

			m_pTree->DeleteItem(hItem);

			if( hNext == NULL ) {

				break;
				}

			hItem = hNext;
			}	
		}

	m_pTree->DeleteItem(hRoot);

	m_MapNames.Empty();
	}

CString CProgramExecutionViewWnd::SaveSelState(void)
{
	AfxTrace(L" select is root %d\n", m_hSelect == m_hRoot);

	if( TRUE ) {

		CString Text;

		INDEX n = m_MapNames.GetHead();

		while( !m_MapNames.Failed(n) ) {

			if( !Text.IsEmpty() ) {
				
				Text += L", ";
				}

			Text += m_MapNames.GetName(n);			

			m_MapNames.GetNext(n);
			}

		AfxTrace(Text + L"\n");
		}

	DWORD dwProg = m_pTree->GetItemParam(m_hSelect);

	CStratonProgramDescriptor Prog(m_dwProject, dwProg);

	return Prog.m_Name;

	INDEX Index = m_MapNames.FindData(m_hSelect);

	AfxAssert(!m_MapNames.Failed(Index));

	return m_MapNames.GetName(Index);
	}

void CProgramExecutionViewWnd::LoadSelState(CString State)
{
	HTREEITEM hItem = m_MapNames[State];

	if( hItem ) {

		m_hSelect = NULL;

		m_pTree->SelectItem(NULL);

		m_pTree->SelectItem(hItem);

		return;
		}

	m_hSelect = NULL;

	m_pTree->SelectItem(NULL);

	m_pTree->SelectItem(m_hRoot);
	}

// Move Helpers

BOOL CProgramExecutionViewWnd::CanMoveProgram(DWORD dwProgram, DWORD dwMove, DWORD dwParent)
{
	CStratonProgramDescriptor Prog(m_dwProject, dwProgram);

	CStratonProgramDescriptor Pare(m_dwProject, dwParent);

	if( dwMove == moveUp ) {

		if( Prog.m_dwSection == Pare.m_dwSection ) {
			
			return TRUE;
			}

		if( Prog.m_dwSection & sectBegin ) {

			return TRUE;
			}

		if( Prog.m_dwSection & sectEnd ) {

			if( Pare.m_dwSection & sectBegin ) {
				
				return FALSE;
				}

			return !(Pare.m_dwSection & sectSfcMain);
			}

		return FALSE;
		}

	if( dwMove == moveDown ) {

		if( Prog.m_dwSection == Pare.m_dwSection ) {
			
			return TRUE;
			}

		if( Prog.m_dwSection & sectEnd ) {

			return TRUE;
			}

		if( Prog.m_dwSection & sectBegin ) {

			if( Pare.m_dwSection & sectEnd ) {
				
				return FALSE;
				}

			return !(Pare.m_dwSection & sectSfcMain);
			}

		return FALSE;
		}

	if( dwMove == moveBegin ) {

		if( Prog.m_dwSection & sectSfcMain ) {
			
			return FALSE;
			}

		if( Prog.m_dwSection & sectEnd ) {

			return TRUE;
			}

		return FALSE;
		}

	if( dwMove == moveEnd ) {

		if( Prog.m_dwSection & sectSfcMain ) {
			
			return FALSE;
			}

		if( Prog.m_dwSection & sectBegin ) {

			return TRUE;
			}

		return FALSE;
		}

	if( dwMove == moveSfcMain ) {

		if( Prog.m_dwSection & sectSfcMain ) {

			if( Pare.m_dwSection & sectEnd ) {
				
				return FALSE;
				}

			if( Pare.m_dwSection & sectBegin ) {
				
				return FALSE;
				}
			
			return TRUE;
			}

		return FALSE;
		}

	if( dwMove == moveBefore ) {

		return FALSE;
		}

	if( dwMove == moveAfter ) {

		return FALSE;
		}

	return FALSE;
	}

BOOL CProgramExecutionViewWnd::MoveProgram(DWORD dwProgram, DWORD dwMove, DWORD dwParent)
{
	return afxDatabase->MoveProgram( m_dwProject, 
					 dwProgram, 
					 dwMove,
					 dwParent					 
					 );
	}

void CProgramExecutionViewWnd::NewItemSelected(void)
{
	ShowDebug();
	}

void CProgramExecutionViewWnd::CreateTool(CRect Tool)
{
	m_pTool->Create(Tool, ThisObject);
	
	m_pTool->PollGadgets();

	m_pTool->ShowWindow(SW_SHOW);
	}

void CProgramExecutionViewWnd::CreateTree(CRect Tree)
{
	DWORD dwStyle   = TVS_NOTOOLTIPS
			| TVS_TRACKSELECT
			| TVS_FULLROWSELECT
			| TVS_HASBUTTONS;

	m_pTree->Create(dwStyle, Tree, ThisObject, 100);

	m_pTree->SetFont(afxFont(Dialog));

	m_pTree->SetImageList(TVSIL_NORMAL, m_Images);

	m_pTree->SetImageList(TVSIL_STATE,  m_Images);

	m_pTree->ShowWindow(SW_SHOW);
	}

void CProgramExecutionViewWnd::ExecMoveList(DWORD dwProg, CMoveList const &Move)
{
	BOOL fRefresh = FALSE;	

	for( UINT n = 0; n < Move.GetCount(); n ++ ) {

		CMoveDesc Desc = Move[n];

		if( MoveProgram(dwProg, Desc.m_dwMove, Desc.m_dwProg) ) {
					
			fRefresh = TRUE;
			}
		}

	if( fRefresh ) {

		m_pItem->SetDirty();

		RefreshTree();
		}
	}

// Debug

void CProgramExecutionViewWnd::ShowDebug(void)
{
	CString Text;

	if( m_hSelect != m_hRoot ) {

		HTREEITEM hPrev = m_pTree->GetPrevious(m_hSelect);

		HTREEITEM hNext = m_pTree->GetNext(m_hSelect);

		if( hPrev ) {

			DWORD dwPrev = m_pTree->GetItemParam(hPrev);

			CStratonProgramDescriptor Prev(m_dwProject, dwPrev);

			Text += Prev.m_Name;
			
			Text += CPrintf(L" (%X) %c", Prev.m_dwSection, Prev.GetSectName());
			
			Text += L", ";
			}

		if( m_hSelect ) {

			DWORD dwProg = m_pTree->GetItemParam(m_hSelect);

			CStratonProgramDescriptor Prog(m_dwProject, dwProg);

			Text += Prog.m_Name;
			
			Text += CPrintf(L" (%X) %c", Prog.m_dwSection, Prog.GetSectName());
			
			Text += L", ";
			}

		if( hNext ) {

			DWORD dwNext = m_pTree->GetItemParam(hNext);

			CStratonProgramDescriptor Next(m_dwProject, dwNext);

			Text += Next.m_Name;
			
			Text += CPrintf(L" (%X) %c", Next.m_dwSection, Next.GetSectName());
			}
		}

	afxThread->SetStatusText(Text);
	}

//////////////////////////////////////////////////////////////////////////
//
// Project Execution Window
//

// Runtime Class

AfxImplementRuntimeClass(CProgramExecutionCtrlWnd, CCtrlWnd);

// Constructor

CProgramExecutionCtrlWnd::CProgramExecutionCtrlWnd(void)
{
	m_pView = New CProgramExecutionViewWnd;
	}

// Operations

void CProgramExecutionCtrlWnd::Attach(CItem *pItem)
{
	m_pView->Attach(pItem);

	m_pProject = (CControlProject *) pItem->GetParent(AfxRuntimeClass(CControlProject));
	}

void CProgramExecutionCtrlWnd::Load(void)
{
	m_pView->Load();
	}

void CProgramExecutionCtrlWnd::Save(void)
{
	m_pView->Save();
	}

void CProgramExecutionCtrlWnd::Exec(CCmd *pCmd)
{
	m_pView->Exec(pCmd);
	}

void CProgramExecutionCtrlWnd::Undo(CCmd *pCmd)
{
	m_pView->Undo(pCmd);
	}

// Routing Control

BOOL CProgramExecutionCtrlWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView && m_pView->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	return CCtrlWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CProgramExecutionCtrlWnd, CCtrlWnd)
{
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_POSTCREATE)

	AfxMessageEnd(CProgramExecutionCtrlWnd)
	};

// Message Handlers

void CProgramExecutionCtrlWnd::OnSize(UINT uCode, CSize Size)
{
	m_pView->MoveWindow(GetClientRect() - 1, TRUE);
	}

void CProgramExecutionCtrlWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( !IsEnabled() ) {

		DC.FrameRect(GetClientRect(), afxColor(Disabled));

		return;
		}

	DC.FrameRect(GetClientRect(), afxColor(Enabled));
	}

void CProgramExecutionCtrlWnd::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

void CProgramExecutionCtrlWnd::OnPostCreate(void)
{
	CRect Rect = GetClientRect() - 1;

	DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN | WS_VISIBLE;

	m_pView->Create(dwStyle, Rect, ThisObject, IDVIEW, NULL);
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Program Execution
//

class CUITextProgramExecution : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextProgramExecution(void);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Program Execution
//

// Dynamic Class

AfxImplementDynamicClass(CUITextProgramExecution, CUITextElement);

// Constructor

CUITextProgramExecution::CUITextProgramExecution(void)
{
	m_uFlags = textEdit | textNoMerge;
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Program Execution
//

class CUIProgramExecution : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIProgramExecution(void);

	protected:
		// Data Members
		CLayItemSize              * m_pDataLayout;
		CProgramExecutionCtrlWnd  * m_pDataWindow;

		// Core Overidables
		void OnBind(void);
		void OnRebind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);

		// Implementation
		CRect GetDataWindowRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Program Execution
//

// Dynamic Class

AfxImplementDynamicClass(CUIProgramExecution, CUIControl);

// Constructor

CUIProgramExecution::CUIProgramExecution(void)
{
	m_pDataLayout = NULL;
	
	m_pDataWindow = New CProgramExecutionCtrlWnd;
	}

// Core Overidables

void CUIProgramExecution::OnBind(void)
{
	CUIControl::OnBind();

	m_pDataWindow->Attach(m_pData->GetObject(m_pItem));
	}

void CUIProgramExecution::OnRebind(void)
{
	CUIControl::OnRebind();

	m_pDataWindow->Attach(m_pData->GetObject(m_pItem));
	}

void CUIProgramExecution::OnLayout(CLayFormation *pForm)
{
	m_pDataLayout = New CLayItemSize(CSize(150, 250), CSize(4, 4), CSize(2, 100));

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem(New CLayItem());
	
	pForm->AddItem(m_pMainLayout);
	}

void CUIProgramExecution::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pDataWindow->Create( WS_CHILD | WS_TABSTOP | WS_CLIPCHILDREN,
			       GetDataWindowRect(),
			       Wnd,
			       uID++
			       );

	m_pDataWindow->SetFont(afxFont(Dialog));

	AddControl(m_pDataWindow);
	}

void CUIProgramExecution::OnPosition(void)
{
	m_pDataWindow->MoveWindow(GetDataWindowRect(), TRUE);
	}

// Data Overridables

void CUIProgramExecution::OnLoad(void)
{
	m_pDataWindow->Load();
	}

// Implementation

CRect CUIProgramExecution::GetDataWindowRect(void)
{
	CRect Rect;
	
	Rect = m_pDataLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

// End of File
