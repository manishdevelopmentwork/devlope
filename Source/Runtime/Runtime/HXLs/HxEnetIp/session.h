/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** SESSION.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Session collection declaration
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/


#ifndef SESSION_H
#define SESSION_H

#define INVALID_SESSION		(-1)			/* Invalid session index */

extern INT32 gnSessions;		/* Number of open sessions */

typedef enum tagOpenSessionState
{
	OpenSessionLogged							= 0x1,	/* Open Session request has just arrived */
    OpenSessionWaitingForPing					= 0x2,	/* Waiting for the successful Ping execution */	
	OpenSessionPingSuccessfullyCompleted		= 0x3,	/* Ping reply received */ 
	OpenSessionWaitingForTCPConnection			= 0x4,	/* Waiting for the TCP connection to be established to the target */	
	OpenSessionTCPConnectionEstablished			= 0x5,	/* TCP connection to the target has been successfully established */
	OpenSessionWaitingForRegisterResponse		= 0x6,	/* Waiting for the response for the Resister Session sent */
	OpenSessionWaitingForRegister				= 0x7,	/* Waiting for the Resister Session on the incoming session */
	OpenSessionEstablished						= 0x8,	/* Session is opened 	*/
}
OpenSessionState;

typedef enum tagSessionDirection
{
	Incoming		= 1,	
	Outgoing,	
	AnyDirection,	
}
SessionDirection;

typedef struct  tagSession		/* Session object data */
{
    UINT32   lSocket;			/* TCP Winsock socket identifier */	
	
	UINT32   lPingSocket;		/* Temporary socket allocated to send an ICMP Ping */	
	UINT32   lPingTimeout;		/* Ping timeout tick */

	UINT32   lSessionTag;		/* Session Tag is returned in response to the Register Session request and must be included in all subsequent requests to the same server */
	
	UINT32   lState;			/* One of the values of the OpenSessionState enumeration */

	UINT32   lWaitingForOpenSessionTimeout;  /* When the session should time out if there is no response */
	
	BOOL     bIncoming;			/* TRUE if the session was initiated by some other device, FALSE if it was us who sent Register Session request */
			
	struct sockaddr_in sClientAddr;	/* Socket address of the server this session is opened with */
	
	UINT16 iClientEncapProtocolVersion;	
	UINT16 iClientEncapProtocolFlags;	

	UINT16 iPartialRecvPacketOffset;		/* Temporary store any partial packets we received for this session */
	UINT16 iPartialRecvPacketSize;			/* Size of the partial recv packet */
	UINT32 lPartialRecvPacketTimeoutTick;	/* When the partial recv packet should be discarded as old */

	UINT16 iPartialSendPacketOffset;	/* Temporary store any partial packets we need to send for this session */
	UINT16 iPartialSendPacketSize;		/* Size of the partial send packet */
	    
} SESSION;

extern SESSION gSessions[/*MAX_SESSIONS*/];	/* Session object array */


extern void  sessionInit();
extern void  sessionInitialize( INT32 nSession );
extern INT32 sessionNew( UINT32 lIPAddress, BOOL bIncoming );
extern void  sessionRemove( INT32 nSession, BOOL bRelogOutgoingRequests );
extern void  sessionRemoveAll( );
extern INT32 sessionFindAddress( UINT32 lIPAddress, INT32 nDirection );
extern INT32 sessionFindAddressEstablished( UINT32 lIPAddress, INT32 nDirection );
extern void  sessionService( INT32 nSession );
extern INT32 sessionGetNumOutgoing( );
extern BOOL  sessionIsIdle( UINT32 lIPAddress, INT32 nConnectionExclude, INT32 nRequestExclude );

#endif /* #ifndef SESSION_H */
