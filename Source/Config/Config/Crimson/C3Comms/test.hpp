
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TEST_HPP
	
#define	INCLUDE_TEST_HPP 

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Driver Options
//

class CTestModeDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestModeDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Mode;
				
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Master Driver
//

class CTestMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CTestMasterDriver(void);

		//Destructor
		~CTestMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Driver Data
		UINT GetFlags(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Master TCP Driver
//

class CTestMasterTCPDriver : public CTestMasterDriver
{
	public:
		// Constructor
		CTestMasterTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

	};

#endif

// End of File