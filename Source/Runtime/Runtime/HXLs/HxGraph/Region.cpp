
#include "Intern.hpp"

#include "Region.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Region Implementation
//

// Instantiator

static IUnknown * Create_Region(PCTXT pName)
{
	return New CRegion;
	}

// Registration

global void Register_Region(void)
{
	piob->RegisterInstantiator("graph.region", Create_Region);
	}

// Constructor

CRegion::CRegion(void)
{
	StdSetRef();

	Clear();
	}

// Destructor

CRegion::~CRegion(void)
{
	}

// IUnknown

HRESULT CRegion::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IRegion);

	StdQueryInterface(IRegion);

	return E_NOINTERFACE;
	}

ULONG CRegion::AddRef(void)
{
	StdAddRef();
	}

ULONG CRegion::Release(void)
{
	StdRelease();
	}

// Attributes

BOOL CRegion::IsEmpty(void)
{
	return !m_uCount;
	}

BOOL CRegion::GetRect(R2 &Rect)
{
	SetRectEmpty(Rect);

	if( m_uCount ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			CombineRects(Rect, Rect, m_Rect[n]);
			}

		return !IsRectEmpty(Rect);
		}

	return FALSE;
	}

// Operations

void CRegion::Clear(void)
{
	m_uCount = 0;
	}

void CRegion::AddRect(R2 const &Rect)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( RectInRect(m_Rect[n], Rect) ) {

			return;
			}

		if( RectInRect(Rect, m_Rect[n]) ) {

			m_Rect[n++] = Rect;

			if( n < m_uCount ) {

				for( UINT s = n; s < m_uCount; s++ ) {

					if( RectInRect(Rect, m_Rect[s]) ) {

						continue;
						}

					if( n - s ) {

						m_Rect[n] = m_Rect[s];
						}

					n++;
					}

				m_uCount = n;
				}

			return;
			}
		}

	if( m_uCount == elements(m_Rect) ) {

		UINT m = 65535;

		UINT i = 0;

		for( UINT n = 0; n < m_uCount; n++ ) {

			UINT d = GetDistance(m_Rect[n], Rect);

			if( d < m ) {

				m = d;

				i = n;
				}
			}

		CombineRects(m_Rect[i], m_Rect[i], Rect);

		return;
		}

	m_Rect[m_uCount++] = Rect;
	}

void CRegion::AddEllipse(R2 const &Rect)
{
	AddRect(Rect);
	}

// Hit Testing

BOOL CRegion::HitTest(P2 const &Point)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( PtInRect(m_Rect[n], Point) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CRegion::HitTest(R2 const &Rect)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		if( RectsIntersect(m_Rect[n], Rect) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Filling

void CRegion::Fill(IGdi *pGdi)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		pGdi->FillRect( m_Rect[n].x1,
				m_Rect[n].y1,
				m_Rect[n].x2,
				m_Rect[n].y2
				);
		}
	}

// Diagnostics

void CRegion::Show(IGdi *pGdi)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		pGdi->DrawRect( m_Rect[n].x1-1,
				m_Rect[n].y1-1,
				m_Rect[n].x2+1,
				m_Rect[n].y2+1
				);
		}
	}

void CRegion::Dump(void)
{
	AfxTrace("Region:\n");

	for( UINT n = 0; n < m_uCount; n++ ) {

		AfxTrace( "%u,%u to %u,%u\n",
			  m_Rect[n].x1,
			  m_Rect[n].y1,
			  m_Rect[n].x2,
			  m_Rect[n].y2
			  );
		}

	AfxTrace("\n");
	}

// Implementation

UINT CRegion::GetDistance(R2 const &R1, R2 const &R2)
{
	// LATER -- This function provides a score which
	// is used to decide which rectangle to combine
	// with the new rectangle when we have used all
	// the available slots. It seems to work, but I
	// am not sure it is the optimum solution.

	P2 c1, c2;

	c1.x = (R1.x1 + R1.x2) / 2;
	c1.y = (R1.y1 + R1.y2) / 2;

	c2.x = (R2.x1 + R2.x2) / 2;
	c2.y = (R2.y1 + R2.y2) / 2;

	int dx = (c1.x > c2.x) ? (c1.x - c2.x) : (c2.x - c1.x);
	int dy = (c1.y > c2.y) ? (c1.y - c2.y) : (c2.y - c1.y);

	int si = RectsIntersect(R1, R2) ? 2 : 1;

	return (dx + dy) / si;
	}

// End of File
