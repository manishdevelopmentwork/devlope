
#include "../../Intern.hpp"

#include "AeonSocketManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Aeon Socket Manager
//

// Static Data

DWORD CAeonSocketManager::m_dwUsed = 0;

// Constructor

CAeonSocketManager::CAeonSocketManager(int nCount, UINT uFlags)
{
	StdSetRef();

	m_nCount  = nCount;

	m_uFlags  = uFlags;

	m_uPort   = OPCUA_TCP_DEFAULT_PORT;

	m_fMulti  = FALSE;

	m_dwLevel = 0;

	// Thread Priorities:
	//
	// 2200 - 2230		Server Sockets
	// 2231 - 2262		Client Sockets
	// 2263			Server History
	// 2264			Utilities
	// 2265			Service

	if( nCount == 1 ) {

		for( int n = 0; n < 32; n++ ) {

			long u = m_dwUsed;

			long m = 1 << n;

			if( !(u & m) && AtomicCompAndSwap(&m_dwUsed, u, u | m) == u ) {

				m_dwLevel    = m;

				m_uSockLevel = 2231 + n;

				break;
				}
			}

		AfxAssert(m_dwLevel);
		}
	else {
		m_uSockLevel = 2200;

		m_fMulti     = TRUE;
		}
		   
	m_pThread = NULL;

	m_pList   = ArrayAlloc(m_pList, nCount);

	m_pDns    = NULL;

	m_nBusy   = 0;
	}

// Destructor

CAeonSocketManager::~CAeonSocketManager(void)
{
	if( m_dwLevel ) {

		for(;;) {

			long m = m_dwLevel;

			long u = m_dwUsed;

			if( AtomicCompAndSwap(&m_dwUsed, u, u & ~m) == u ) {

				break;
				}
			}
		}
		
	delete [] m_pList;
	}

// IUnknown

HRESULT CAeonSocketManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CAeonSocketManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CAeonSocketManager::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CAeonSocketManager::TaskInit(UINT uTask)
{
	SetThreadName(m_fMulti ? CPrintf("OpcUa.Sock.%u", uTask) : "OpcUa.Sock");

	return TRUE;
	}

INT CAeonSocketManager::TaskExec(UINT uTask)
{
	if( m_fMulti ) {

		CAeonSocketRecord *pSocket = m_pList + uTask;

		for(;;) {

			switch( RunEntry(uTask) ) {
	
				case codeIdle:

					Sleep(5);

					break;

				case codeDone:

					if( pSocket->m_uParent < NOTHING ) {

						if( !m_pList[pSocket->m_uParent].m_fActive ) {

							return 0;
						}
					}

					pSocket->m_fActive = FALSE;

					pSocket->m_pThread->Release();

					return 0;
				}
			}
		}
	else {
		for(;;) {
			
			BOOL fBusy = FALSE;

			for( int n = 0; n < m_nCount; n++ ) {

				CAeonSocketRecord *pSocket = m_pList + n;

				switch( RunEntry(n) ) {

					case codeBusy:

						fBusy = TRUE;

						break;

					case codeDone:

						TidySocket(pSocket);

						TidyChildren(n);

						break;
					}
				}

			if( !fBusy ) {

				Sleep(5);
				}
			}
		}

	return 0;
	}

void CAeonSocketManager::TaskStop(UINT uTask)
{
	}

void CAeonSocketManager::TaskTerm(UINT uTask)
{
	if( m_fMulti ) {

		CAeonSocketRecord *pSocket = m_pList + uTask;

		TidySocket(pSocket);

		TidyChildren(uTask);
		}
	else {
		for( int n = 0; n < m_nCount; n++ ) {

			CAeonSocketRecord *pSocket = m_pList + n;

			if( pSocket->m_fActive ) {
			
				TidySocket(pSocket);
				}
			}
		}

	AtomicDecrement(&m_nBusy);
	}

// Management

BOOL CAeonSocketManager::Open(void)
{
	for( int n = 0; n < m_nCount; n++ ) {

		CAeonSocketRecord *pSocket = m_pList + n;

		memset(pSocket, 0, sizeof(CAeonSocketRecord));

		pSocket->m_pManager = this;

		pSocket->m_uIndex   = n;
		}

	if( !m_fMulti ) {

		m_pThread = CreateClientThread(this, NOTHING, m_uSockLevel);
					
		AtomicIncrement(&m_nBusy);
		}

	return TRUE;
	}

BOOL CAeonSocketManager::Close(void)
{
	if( m_fMulti ) {

		// Each of our child threads will exit on their own
		// when the associated socket is closed. We can just
		// spin here for a while until they're all gone.

		while( m_nBusy ) {

			Sleep(25);
			}
		}
	else
		m_pThread->Destroy();

	return TRUE;
	}

// Server Creation

UINT CAeonSocketManager::CreateServer( OpcUa_StringA              sAddress,
				       OpcUa_Boolean              fListenOnAllInterfaces,
				       OpcUa_Socket_EventCallback pfnSocketCallBack,
				       OpcUa_Void   *             pCallbackData,
				       OpcUa_Socket *             pSocketRecord
				       )
{
	char const *pFind = strrchr(sAddress, ':');

	if( pFind && isdigit(pFind[1]) ) {

		m_uPort = atoi(pFind + 1);
		}

	for( int n = 0; n < m_nCount; n++ ) {

		CAeonSocketRecord *pSocket = m_pList + n;

		if( AtomicCompAndSwap(&pSocket->m_fUsed, 0, 1) == 0 ) {

			pSocket->m_uType             = typeListen;
			pSocket->m_uParent           = NOTHING;
			pSocket->m_pfnSocketCallBack = pfnSocketCallBack;
			pSocket->m_pCallbackData     = pCallbackData;
			pSocket->m_pSocket           = NULL;
			pSocket->m_fOpened           = TRUE;
			pSocket->m_fClosed           = FALSE;
			pSocket->m_fWrite	     = FALSE;

			AfxNewObject("sock-tcp", ISocket, pSocket->m_pSocket);

			if( pSocket->m_pSocket ) {

				pSocket->m_pSocket->SetOption(OPT_NAGLE, FALSE);

				pSocket->m_pSocket->Listen(WORD(m_uPort));

				AtomicIncrement(&m_nBusy);

				pSocket->m_fActive = TRUE;
			
				pSocket->m_pThread = m_fMulti ? CreateClientThread(this, n, m_uSockLevel + n) : NULL;

				*pSocketRecord     = pSocket;

				return OpcUa_Good;
				}

			pSocket->m_fUsed = FALSE;

			break;
			}
		}

	return OpcUa_Bad;
	}

// Client Creation

UINT CAeonSocketManager::CreateClient( OpcUa_StringA               sRemoteAddress,
				       OpcUa_UInt16                uLocalPort,
				       OpcUa_Socket_EventCallback  pfnSocketCallBack,
				       OpcUa_Void   *              pCallbackData,
				       OpcUa_Socket *              pSocketRecord
				       )
{
	CArray<CIpAddr> List;

	CString         Name = sRemoteAddress;

	if( Name.StartsWith("opc.tcp://") ) {

		Name = Name.Mid(10);
		}

	if( Name.Find(':') < NOTHING ) {

		CString Port = Name;

		Name    = Port.StripToken(':');

		m_uPort = atoi(Port);
		}

	if( FindResolver() ) {
		
		if( ResolveName(List, Name) ) {

			for( int n = 0; n < m_nCount; n++ ) {

				CAeonSocketRecord *pSocket = m_pList + n;

				if( AtomicCompAndSwap(&pSocket->m_fUsed, 0, 1) == 0 ) {

					pSocket->m_uType             = typeClient;
					pSocket->m_uParent           = NOTHING;
					pSocket->m_pfnSocketCallBack = pfnSocketCallBack;
					pSocket->m_pCallbackData     = pCallbackData;
					pSocket->m_pSocket           = NULL;
					pSocket->m_fOpened           = FALSE;
					pSocket->m_fClosed           = FALSE;
					pSocket->m_fWrite	     = TRUE;

					AfxNewObject("sock-tcp", ISocket, pSocket->m_pSocket);

					if( pSocket->m_pSocket ) {

						pSocket->m_PeerAddr = List[rand() % List.GetCount()];
			
						pSocket->m_PeerPort = WORD(m_uPort);

						pSocket->m_pSocket->SetOption(OPT_NAGLE, FALSE);

						AtomicIncrement(&m_nBusy);

						pSocket->m_fActive = TRUE;

						pSocket->m_pThread = m_fMulti ? CreateClientThread(this, n, m_uSockLevel + n) : NULL;

						*pSocketRecord     = pSocket;

						return OpcUa_Good;
						}

					pSocket->m_fUsed = FALSE;

					break;
					}
				}
			}
		}

	return OpcUa_Bad;
	}

// Socket Operations

UINT CAeonSocketManager::SockRead(UINT uIndex, OpcUa_Byte *pBuffer, OpcUa_UInt32 uBufferSize, OpcUa_UInt32 *pBytesRead)
{
	CAeonSocketRecord *pSocket = m_pList + uIndex;

	if( pSocket->m_fUsed && pSocket->m_fActive ) {

		UINT uSize = uBufferSize;

		pSocket->m_pSocket->Recv(pBuffer, uSize);

		if( pBytesRead ) {

			*pBytesRead = uSize;
			}

		return OpcUa_Good;
		}

	return OpcUa_Bad;
	}

UINT CAeonSocketManager::SockWrite(UINT uIndex, OpcUa_Byte *pBuffer, OpcUa_UInt32 uBufferSize)
{
	CAeonSocketRecord *pSocket = m_pList + uIndex;

	if( pSocket->m_fUsed && pSocket->m_fActive ) {

		UINT uLeft = uBufferSize;

		while( uLeft ) {

			UINT Phase = 0;

			UINT uSize = uLeft;

			if( pSocket->m_pSocket->Send(pBuffer, uSize) == S_OK ) {

				pBuffer += uSize;

				uLeft   -= uSize;

				continue;
				}

			if( pSocket->m_pSocket->GetPhase(Phase) == S_OK ) {

				if( Phase == PHASE_OPEN ) {

					Sleep(5);

					continue;
					}
				}

			return 0;
			}

		pSocket->m_fWrite = TRUE;

		return uBufferSize;
		}

	return 0;
	}

UINT CAeonSocketManager::SockGetPeerInfo(UINT uIndex, OpcUa_CharA *pBuffer, OpcUa_UInt32 uBufferSize)
{
	CAeonSocketRecord *pSocket = m_pList + uIndex;

	if( pSocket->m_fUsed && pSocket->m_fActive ) {

		memset (pBuffer, 0, uBufferSize);

		sprintf(pBuffer, "%s:%u", PCTXT(pSocket->m_PeerAddr.GetAsText()), pSocket->m_PeerPort);

		return OpcUa_Good;
		}

	return OpcUa_Bad;
	}

UINT CAeonSocketManager::SockSetUserData(UINT uIndex, OpcUa_Void *pData)
{
	CAeonSocketRecord *pSocket = m_pList + uIndex;

	if( pSocket->m_fUsed && pSocket->m_fActive ) {

		pSocket->m_pCallbackData = pData;

		return OpcUa_Good;
		}

	return OpcUa_Bad;
	}

UINT CAeonSocketManager::SockClose(UINT uIndex)
{
	CAeonSocketRecord *pSocket = m_pList + uIndex;

	if( pSocket->m_fUsed && pSocket->m_fActive ) {

		pSocket->m_pSocket->Close();

		pSocket->m_fClosed = TRUE;

		return OpcUa_Good;
		}

	return OpcUa_Bad;
	}

// Implementation

UINT CAeonSocketManager::RunEntry(UINT uSlot)
{
	CAeonSocketRecord *pSocket = m_pList + uSlot;

	if( pSocket->m_fActive ) {

		switch( pSocket->m_uType ) {
			
			case typeListen:
				return RunListen(uSlot, pSocket);
				
			case typeServer:
				return RunServer(uSlot, pSocket);
				
			case typeClient:
				return RunClient(uSlot, pSocket);
			}
		}

	return codeIdle;
	}

UINT CAeonSocketManager::RunListen(UINT uSlot, CAeonSocketRecord *pSocket)
{
	UINT Phase;

	if( pSocket->m_pSocket->GetPhase(Phase) == S_OK ) {

		if( Phase == PHASE_IDLE ) {

			return codeIdle;
			}

		if( Phase == PHASE_OPENING ) {

			return codeIdle;
			}

		if( Phase == PHASE_OPEN ) {

			for( int n = 0; n < m_nCount; n++ ) {

				CAeonSocketRecord *pConnect = m_pList + n;

				if( AtomicCompAndSwap(&pConnect->m_fUsed, 0, 1) == 0 ) {

					Notify(pSocket, OPCUA_SOCKET_ACCEPT_EVENT);

					pConnect->m_uType             = typeServer;
					pConnect->m_uParent	      = uSlot;
					pConnect->m_pfnSocketCallBack = pSocket->m_pfnSocketCallBack;
					pConnect->m_pCallbackData     = pSocket->m_pCallbackData;
					pConnect->m_pSocket           = pSocket->m_pSocket;
					pConnect->m_fClosed           = FALSE;
					pConnect->m_fWrite	      = TRUE;
					
					ISocket *pListen = NULL;

					AfxNewObject("sock-tcp", ISocket, pListen);

					if( pListen ) {

						pConnect->m_pSocket->GetRemote(pConnect->m_PeerAddr, pConnect->m_PeerPort);

						pSocket->m_pSocket = pListen;

						pSocket->m_pSocket->SetOption(OPT_NAGLE, FALSE);

						pSocket->m_pSocket->Listen(WORD(m_uPort));

						AtomicIncrement(&m_nBusy);

						pConnect->m_fActive = TRUE;

						pConnect->m_pThread = m_fMulti ? CreateClientThread(this, n, m_uSockLevel + n) : NULL;

						break;
						}

					pConnect->m_fUsed = FALSE;
					}
				}

			return codeBusy;
			}

		if( Phase == PHASE_CLOSING ) {

			pSocket->m_pSocket->Close();

			return codeIdle;
			}

		if( Phase == PHASE_ERROR ) {

			if( !pSocket->m_fClosed ) {

				pSocket->m_pSocket->Release();

				AfxNewObject("sock-tcp", ISocket, pSocket->m_pSocket);

				pSocket->m_pSocket->Listen(WORD(m_uPort));

				return codeBusy;
				}

			return codeDone;
			}
		}

	AfxAssert(FALSE);

	return codeIdle;
	}

UINT CAeonSocketManager::RunServer(UINT uSlot, CAeonSocketRecord *pSocket)
{
	UINT Phase;

	if( pSocket->m_pSocket->GetPhase(Phase) == S_OK ) {

		if( Phase == PHASE_OPEN ) {

			BOOL fBusy = FALSE;

			UINT uRead = 0;

			if( pSocket->m_fWrite ) {

				pSocket->m_fWrite = FALSE;

				Notify(pSocket, OPCUA_SOCKET_WRITE_EVENT);

				fBusy = TRUE;
				}

			if( pSocket->m_pSocket->Recv(NULL, uRead) == S_OK ) {

				Notify(pSocket, OPCUA_SOCKET_READ_EVENT);

				fBusy = TRUE;
				}

			return fBusy ? codeBusy : codeIdle;
			}

		if( Phase == PHASE_CLOSING ) {

			pSocket->m_pSocket->Close();

			return codeIdle;
			}

		if( Phase == PHASE_ERROR ) {

			if( !pSocket->m_fClosed ) {

				Notify(pSocket, OPCUA_SOCKET_CLOSE_EVENT);
				}

			return codeDone;
			}
		}

	AfxAssert(FALSE);

	return codeIdle;
	}

UINT CAeonSocketManager::RunClient(UINT uSlot, CAeonSocketRecord *pSocket)
{
	UINT Phase;

	if( pSocket->m_pSocket->GetPhase(Phase) == S_OK ) {

		if( Phase == PHASE_IDLE ) {

			pSocket->m_pSocket->Connect(pSocket->m_PeerAddr, pSocket->m_PeerPort);

			return codeBusy;
			}

		if( Phase == PHASE_OPENING ) {

			return codeIdle;
			}

		if( Phase == PHASE_OPEN ) {

			BOOL fBusy = FALSE;

			UINT uRead = 0;

			if( !pSocket->m_fOpened ) {

				pSocket->m_fOpened = TRUE;

				Notify(pSocket, OPCUA_SOCKET_CONNECT_EVENT);

				fBusy = TRUE;
				}

			if( pSocket->m_fWrite ) {

				pSocket->m_fWrite = FALSE;

				Notify(pSocket, OPCUA_SOCKET_WRITE_EVENT);

				fBusy = TRUE;
				}

			if( pSocket->m_pSocket->Recv(NULL, uRead) == S_OK ) {

				Notify(pSocket, OPCUA_SOCKET_READ_EVENT);

				fBusy = TRUE;
				}

			return fBusy ? codeBusy : codeIdle;
			}

		if( Phase == PHASE_CLOSING ) {

			pSocket->m_pSocket->Close();

			return codeIdle;
			}

		if( Phase == PHASE_ERROR ) {

			if( !pSocket->m_fClosed ) {

				Notify(pSocket, OPCUA_SOCKET_EXCEPT_EVENT);

				Notify(pSocket, OPCUA_SOCKET_CLOSE_EVENT);
				}

			return codeDone;
			}
		}

	AfxAssert(FALSE);

	return codeIdle;
	}

void CAeonSocketManager::TidySocket(CAeonSocketRecord *pSocket)
{
	pSocket->m_pSocket->Release();

	pSocket->m_fActive = FALSE;

	pSocket->m_fUsed   = FALSE;
	}

void CAeonSocketManager::TidyChildren(UINT uSlot)
{
	for( int c = 0; c < m_nCount; c++ ) {

		CAeonSocketRecord *pSocket = m_pList + c;

		if( pSocket->m_fUsed && pSocket->m_fActive ) {

			if( pSocket->m_uParent == uSlot ) {

				if( m_fMulti ) {

					pSocket->m_pThread->Destroy();
				}
				else {
					TidySocket(pSocket);
				}
			}
		}
	}
}

BOOL CAeonSocketManager::FindResolver(void)
{
	if( !m_pDns ) {

		AfxGetObject("dns", 0, IDnsResolver, m_pDns);
		}

	return m_pDns ? TRUE : FALSE;
	}

BOOL CAeonSocketManager::ResolveName(CArray <CIpAddr> &List, PCTXT pName)
{
	return m_pDns->Resolve(List, pName);
	}

void CAeonSocketManager::Notify(CAeonSocketRecord *pSocket, UINT uCode)
{
	(*pSocket->m_pfnSocketCallBack)( pSocket,
					 uCode,
					 pSocket->m_pCallbackData,
					 pSocket->m_PeerPort,
					 FALSE
					 );
	}

// End of File
