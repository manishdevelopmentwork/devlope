
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Enum
//

class CUITextVarInitEnum : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarInitEnum(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD	           m_dwProject;
		BOOL		   m_fCommit;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Enum
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarInitEnum, CUITextEnum);

// Constructor

CUITextVarInitEnum::CUITextVarInitEnum(void)
{
	m_fCommit = FALSE;
	}

// Overridables

void CUITextVarInitEnum::OnBind(void)
{
	CUITextEnum::OnBind();

	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();
	}

void CUITextVarInitEnum::OnRebind(void)
{
	CUITextEnum::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();
	}

CString CUITextVarInitEnum::OnGetAsText(void)
{
	return m_pData->ReadString(m_pItem);
	}

UINT CUITextVarInitEnum::OnSetAsText(CError &Error, CString Text)
{
	if( m_fCommit ) {

		DWORD dwVariable = m_pVariable->GetHandle();

		DWORD    dwGroup = m_pVariable->GetGroup(m_dwProject);

		if( !afxDatabase->SetVarInitValue(m_dwProject, dwGroup, dwVariable, Text)  ) {

			return saveError;		
			}
		}

	m_pData->WriteString(m_pItem, Text);

	return saveChange;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Enum
//

class CUITextVarInitEnumCommit : public CUITextVarInitEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarInitEnumCommit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Enum
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarInitEnumCommit, CUITextVarInitEnum);

// Constructor

CUITextVarInitEnumCommit::CUITextVarInitEnumCommit(void)
{
	m_fCommit = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Data Array
//

class CUITextVarInitString : public CUITextString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarInitString(void);

	protected:
		// Data Members
		CControlVariable * m_pVariable;
		DWORD	           m_dwProject;
		BOOL		   m_fCommit;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);
		UINT    OnSetAsText(CError &Error, CString Text);

		// Implementation
		BOOL Check(CError &Error, CString &Val);
		BOOL CheckMin(CString Val, CString &Min);
		BOOL CheckMax(CString Val, CString &Max);
//		BOOL GetRange(CStratonDataTypeDescriptor const &Type, INT64 &nMin, INT64 &nMax);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Data Array
//

class CUITextVarInitStringCommit : public CUITextVarInitString
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarInitStringCommit(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Data Array
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarInitString, CUITextString);

// Constructor

CUITextVarInitString::CUITextVarInitString(void)
{
	m_fCommit = FALSE;
	}

// Overridables

void CUITextVarInitString::OnBind(void)
{
	CUITextString::OnBind();

	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();

	CStratonDataTypeDescriptor Type(m_dwProject, m_pVariable->m_TypeIdent);

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[3].IsEmpty() ) {

		if( List[3] == L"number" ) {

			m_uFlags |= textNumber;
			}

		if( List[3] == L"time" ) {

			m_uFlags |= textNumber;

			m_uFlags |= textUnits;

			m_Units   = L"ms";
			}

		if( List[3] == L"real" ) {

			m_uFlags |= textNumber;

			m_uFlags |= textPlaces;
			}

		if( Type.IsSigned() ) {

			m_uFlags |= textSigned;
			}
		}
	}

void CUITextVarInitString::OnRebind(void)
{
	CUITextString::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();
	}

UINT CUITextVarInitString::OnSetAsText(CError &Error, CString Text)
{
	CVariableInitial     *pInitial = (CVariableInitial *) m_pItem;

	CVariableInitialFormatter &Fmt = pInitial->GetFormat();

	if( Check(Error, Text) ) {

		if( m_fCommit ) {

			DWORD dwVariable = m_pVariable->GetHandle();

			DWORD    dwGroup = m_pVariable->GetGroup(m_dwProject);

			if( !afxDatabase->CheckVarInitValue(m_dwProject, dwGroup, dwVariable, Fmt.ToStraton(Text)) ) {
			
				Error.Set(CString(IDS_INITIAL_VALUE_IS));

				return saveError;
				}

			if( !afxDatabase->SetVarInitValue(m_dwProject, dwGroup, dwVariable, Fmt.ToStraton(Text))  ) {

				return saveError;
				}
			}

		return CUITextString::OnSetAsText(Error, Text);
		}

	return saveError;
	}

// Implementation

BOOL CUITextVarInitString::Check(CError &Error, CString &Val)
{
	CVariableInitial     *pInitial = (CVariableInitial *) m_pItem;

	CVariableInitialFormatter &Fmt = pInitial->GetFormat();

	CString Min;

	if( !CheckMin(Val, Min) ) {

		if( Error.AllowUI() ) {

			CPrintf Text( CString(IDS_SMALLEST),
					Fmt.ToStraton(Min)
					);

			Error.Set(Text);
			
			return FALSE;
			}

		Val = Min;

		return TRUE;
		}

	CString Max;

	if( !CheckMax(Val, Max) ) {

		if( Error.AllowUI() ) {

			CPrintf Text( CString(IDS_LARGEST),
					Fmt.ToStraton(Max)
					);

			Error.Set(Text);

			return FALSE;
			}

		Val = Max;

		return TRUE;
		}

	return TRUE;
	}

BOOL CUITextVarInitString::CheckMin(CString Val, CString &Min)
{
	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();

	CStratonDataTypeDescriptor Type(m_dwProject, m_pVariable->m_TypeIdent);

	INT64 nMin;
		
	INT64 nMax;

	if( Type.GetRange(nMin, nMax) ) {

		INT64 nVal =  watoi64(Val);

		if( nVal < nMin ) {

			Min = CPrintf(L"%ld", nMin);
				
			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CUITextVarInitString::CheckMax(CString Val, CString &Max)
{
	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();

	CStratonDataTypeDescriptor Type(m_dwProject, m_pVariable->m_TypeIdent);

	INT64 nMin;
		
	INT64 nMax;

	if( Type.GetRange(nMin, nMax) ) {

		INT64 nVal =  watoi64(Val);

		if( nVal > nMax ) {

			Max = CPrintf(L"%lu", nMax);
				
			return FALSE;
			}
		}

	return TRUE;
	}
/*
BOOL CUITextVarInitString::GetRange(CStratonDataTypeDescriptor const &Type, INT64 &nMin, INT64 &nMax)
{
	struct CInfo {
		
		CString m_Name;

		INT64 m_nMin;

		INT64 m_nMax;
		};

	static const CInfo Info[] = {
		
		{ L"SINT",	       -128,	    127	},
		{ L"USINT",	          0,	    255	},
		{ L"BYTE",	          0,	    255	},
		{ L"INT",	     -32768,	  32767	},
		{ L"UINT",	          0,	  65535	},
		{ L"WORD",	          0,	  65535	},
		{ L"DINT",	-2147483647, 2147483647	},
		{ L"UDINT",	          0, 4294967295L },
		{ L"DWORD",	          0, 4294967295L },		

		};

	for( UINT n = 0; n < elements(Info); n ++ ) {

		CInfo const &Entry = Info[n];
		
		if( Entry.m_Name == Type.m_Name ) {
			
			nMin = Entry.m_nMin;

			nMax = Entry.m_nMax;

			return TRUE;
			}
		}

	return FALSE;
	}
*/
//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Data Array
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarInitStringCommit, CUITextVarInitString);

// Constructor

CUITextVarInitStringCommit::CUITextVarInitStringCommit(void)
{
	m_fCommit = TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Data Array
//

class CUITextVarInitArray : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextVarInitArray(void);

	protected:
		// Structure
		struct CEntry {

			CString	m_Elem;
			UINT	m_uCount;
			};

		// Data Members
		CControlVariable * m_pVariable;
		DWORD	           m_dwProject;

		// Overridables
		void    OnBind(void);
		void    OnRebind(void);

		// Overridables
		CString	OnGetAsText(void);
		UINT	OnSetAsText(CError &Error, CString Text);
		BOOL    OnExpand(CWnd &Wnd);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Variable Initial Data Array
//

// Dynamic Class

AfxImplementDynamicClass(CUITextVarInitArray, CUITextElement);

// Constructor

CUITextVarInitArray::CUITextVarInitArray(void)
{
	m_uFlags = textExpand;
	}

// Overridables

void CUITextVarInitArray::OnBind(void)
{
	CUITextElement::OnBind();

	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();
	}

void CUITextVarInitArray::OnRebind(void)
{
	CUITextElement::OnRebind();

	m_pVariable = (CControlVariable *) m_pItem->GetParent(AfxRuntimeClass(CControlVariable));

	m_dwProject = m_pVariable->GetProject();
	}

CString CUITextVarInitArray::OnGetAsText(void)
{
	CVariableInitialArray *pInitial = (CVariableInitialArray *) m_pData->GetObject(m_pItem);

	return pInitial->ToStraton();
	}

UINT CUITextVarInitArray::OnSetAsText(CError &Error, CString Text)
{
	CVariableInitialArray *pInitial = (CVariableInitialArray *) m_pData->GetObject(m_pItem);

	DWORD dwVariable = m_pVariable->GetHandle();

	DWORD    dwGroup = m_pVariable->GetGroup(m_dwProject);

	if( afxDatabase->SetVarInitValue( m_dwProject, dwGroup, dwVariable, Text)  ) {

		pInitial->ToCrimson(Text);

		return saveChange;
		}

	return saveError;
	}

BOOL CUITextVarInitArray::OnExpand(CWnd &Wnd)
{
	CItem *pItem = m_pData->GetObject(m_pItem);

	CString Title;

	Title += CString(IDS_EDIT_INITIAL);

	Title += L" - ";

	Title += m_pItem->GetHumanName();

	CItemDialog Dialog(pItem, Title);

	return Dialog.Execute(Wnd);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Variable Initial Data Array
//

class CUIVarInitArray : public CUIPick
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIVarInitArray(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Variable Initial Data Array
//

// Dynamic Class

AfxImplementDynamicClass(CUIVarInitArray, CUIPick);

// Constructor

CUIVarInitArray::CUIVarInitArray(void)
{
	}

// Data Overridables

void CUIVarInitArray::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	m_pDataCtrl->SetWindowText(Data);
	}

UINT CUIVarInitArray::OnSave(BOOL fUI)
{
	CString Last = m_pText->GetAsText();

	return saveSame;
	}

// Notification Handlers

BOOL CUIVarInitArray::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		if( m_pText->HasFlag(textExpand) ) {

			CString Last = m_pText->GetAsText();

			afxThread->SetWaitMode(TRUE);

			if( m_pText->ExpandData(*m_pPickCtrl) ) {

				afxThread->SetWaitMode(FALSE);

				CString Data = m_pText->GetAsText();

				if( Data.CompareC(Last) ) {
					
					CError Error(TRUE);

					UINT uCode = m_pText->SetAsText(Error, Data);

					AfxTouch(uCode);

					if( Error.IsOkay() ) {

						LoadUI();
						}
					else {
						if( Error.AllowUI() ) {

							Error.Show(*afxMainWnd);
							}
						}
					}

				return TRUE;
				}

			afxThread->SetWaitMode(FALSE);
			}

		return FALSE;
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// End of File
