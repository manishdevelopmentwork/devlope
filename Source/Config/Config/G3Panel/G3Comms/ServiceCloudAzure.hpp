
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ServiceCloudAzure_HPP

#define INCLUDE_ServiceCloudAzure_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceCloudJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Azure Cloud Client Configuration
//

class CServiceCloudAzure : public CServiceCloudJson
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CServiceCloudAzure(void);

	// Type Access
	BOOL GetTypeData(CString Tag, CTypeDef &Type);

	// UI Update
	void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

	// Persistance
	void Init(void);

	// Download Support
	BOOL MakeInitData(CInitData &Init);

	// Item Properties
	UINT         m_Twin;
	CCodedItem * m_pPub;
	CCodedItem * m_pSub;
	CCodedItem * m_pKey;

protected:
	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void DoEnables(IUIHost *pHost);
	void InitOptions(void);
};

// End of File

#endif
