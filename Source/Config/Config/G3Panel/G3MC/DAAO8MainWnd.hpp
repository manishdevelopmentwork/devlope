
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAAO8MainWnd_HPP

#define INCLUDE_DAAO8MainWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAAO8Module;

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 Module Window
//

class CDAAO8MainWnd : public CProxyViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAAO8MainWnd(void);

protected:
	// Data Members
	CMultiViewWnd * m_pMult;
	CDAAO8Module * m_pItem;

	// Overridables
	void OnAttach(void);

	// Implementation
	void AddOutputConfig(void);
};

// End of File

#endif
