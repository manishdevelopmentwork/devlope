
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASKAWANS6_HPP
	
#define	INCLUDE_YASKAWANS6_HPP

#define AN addrNamed
#define LL addrLongAsLong
#define	BB addrBitAsBit

class CYaskawaNS600DeviceOptions;
class CYaskawaNS600Driver;

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa NS600 Device Options
//

class CYaskawaNS600DeviceOptions : CStdDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaNS600DeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa NS600 Comms Driver
//

class CYaskawaNS600Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaNS600Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

	protected:
		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
