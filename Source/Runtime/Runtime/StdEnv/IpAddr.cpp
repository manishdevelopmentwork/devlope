
#include "Intern.hpp"

#include "IpAddr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// LINK

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "String.hpp"

#include "Printf.hpp"

////////////////////////////////////////////////////////////////////////
//
// IP Address Class
//

// Static Data

IPADDR const CIpAddr::m_Broad = { { { 0xFF, 0xFF, 0xFF, 0xFF } } };

IPADDR const CIpAddr::m_Multi = { { { 0xE0, 0x00, 0x00, 0x01 } } };

IPADDR const CIpAddr::m_Exper = { { { 0xF0, 0x00, 0x00, 0x01 } } };

IPADDR const CIpAddr::m_Empty = { { { 0x00, 0x00, 0x00, 0x00 } } };

IPADDR const CIpAddr::m_Loop  = { { { 0x7F, 0x00, 0x00, 0x01 } } };

// String Assignment

CIpAddr const & CIpAddr::operator = (PCTXT pText)
{
	for( UINT n = 0; n < 4; n++ ) {

		PTXT pNext = NULL;

		UINT uData = strtoul(pText, &pNext, 10);

		PBYTE(this)[n] = BYTE(uData);

		if( *pNext ) {

			if( n < 3 && *pNext == '.' ) {

				pText = pNext + 1;

				continue;
				}

			m_dw = 0;

			return *this;
			}

		if( n < 3 ) {

			m_dw = 0;

			return *this;
			}

		break;
		}

	return *this;
	}

// Conversion

CString CIpAddr::GetAsText(void) const
{
	return CPrintf("%u.%u.%u.%u", m_b1, m_b2, m_b3, m_b4);
	}

// Attributes

BOOL CIpAddr::IsSpecial(void) const
{
	return IsBroadcast() || IsDirected() || IsMulticast() || IsExperimental();
	}

BOOL CIpAddr::IsPrivate(void) const
{
	if( !IsEmpty() ) {

		if( OnSubnet(CIpAddr(192, 168, 0, 0), CIpAddr(255, 255, 0, 0)) ) {

			return TRUE;
		}

		if( OnSubnet(CIpAddr(172, 16, 0, 0), CIpAddr(255, 240, 0, 0)) ) {

			return TRUE;
		}

		if( OnSubnet(CIpAddr(10, 0, 0, 0), CIpAddr(255, 0, 0, 0)) ) {

			return TRUE;
		}

		if( OnSubnet(CIpAddr(169, 254, 0, 0), CIpAddr(255, 255, 0, 0)) ) {

			return TRUE;
		}
	}

	return FALSE;
}

UINT CIpAddr::GetBitCount(void) const
{
	DWORD m = 0x80000000;

	DWORD a = HostToMotor(m_dw);

	UINT  n = 0;

	while( a & m ) {

		m >>= 1;

		n++;
	}

	return n;
}

// End of File
