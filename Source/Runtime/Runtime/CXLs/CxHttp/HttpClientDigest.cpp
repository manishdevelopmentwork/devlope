
#include "Intern.hpp"

#include "HttpClientDigest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpAuth.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Digest Helper
//

// Constructor

CHttpClientDigest::CHttpClientDigest(void)
{
	}

// Operations

BOOL CHttpClientDigest::ProcessRequest(CHttpAuth *p)
{
	if( p ) {

		m_Algorithm = p->GetParam("algorithm");

		if( m_Algorithm.IsEmpty() ) {

			m_Algorithm = "md5";
			}

		if( m_Algorithm == "md5" || m_Algorithm == "md5-sess" ) {

			m_qop.Empty();

			CStringArray qop;

			p->GetParam("qop").Tokenize(qop, ',');

			for( UINT n = 0; n < qop.GetCount(); n++ ) {

				if( qop[n] == "auth-int" || qop[n] == "auth" ) {

					if( qop[n].GetLength() > m_qop.GetLength() ) {

						m_qop = qop[n];
						}
					}
				}

			m_Realm  = p->GetParam("realm");

			m_Opaque = p->GetParam("opaque");

			m_Nonce  = p->GetParam("nonce");

			UpdateNonce();

			return TRUE;
			}
		}

	return FALSE;
	}

void CHttpClientDigest::UpdateNonce(void)
{
	m_ClientNonce = MakeNonce();

	m_uNonceCount = 0;
	}

// End of File
