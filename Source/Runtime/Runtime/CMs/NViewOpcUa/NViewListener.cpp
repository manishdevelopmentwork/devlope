
#include "Intern.hpp"

#include "NViewListener.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// N-View Listener
//

// Constructor

CNViewListener::CNViewListener(void)
{
	StdSetRef();

	ReadConfig();
	}

// Destructor

CNViewListener::~CNViewListener(void)
{
	for( UINT n = 0; n < m_uSwitch; n++ ) {

		delete [] m_pSwitch[n].m_pPorts;
		}

	delete [] m_pSwitch;
	}

// Attributes

UINT CNViewListener::GetSwitchCount(void) const
{
	return m_uSwitch;
	}

// Switch Attributes

CString CNViewListener::GetSwitchName(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Name;
		}

	return "";
	}

CString CNViewListener::GetSwitchModel(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Model;
		}

	return "";
	}

UINT CNViewListener::GetSwitchPorts(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_uPorts;
		}

	return 0;
	}

bool CNViewListener::IsSwitchActive(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_fActive;
		}

	return 0;
	}

CIpAddr CNViewListener::GetSwitchIp(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Ip;
		}

	return IP_EMPTY;
	}

CMacAddr CNViewListener::GetSwitchMac(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Mac;
		}

	return MAC_EMPTY;
	}

CMacAddr CNViewListener::GetSwitchRxMac(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_RxMac;
		}

	return MAC_EMPTY;
	}

UINT CNViewListener::GetSwitchVersion(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_uVersion;
		}

	return 0;
	}

timeval CNViewListener::GetSwitchUpdate(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Update;
		}

	timeval n;

	n.tv_sec  = 0;
	n.tv_usec = 0;

	return n;
	}

bool CNViewListener::IsRingManager(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Ring.is_nring_manager ? true : false;
		}

	return false;
	}

bool CNViewListener::IsRingActive(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Ring.is_active_nring_member ? true : false;
		}

	return false;
	}

UINT CNViewListener::GetRingState(UINT s) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		return sw.m_Ring.is_nring_manager_ring_state;
		}

	return 0;
	}

// Port Attributes

CString CNViewListener::GetPortName(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( sw.m_uGrouping ) {

			return CPrintf("Port%c%u", 'A' + p / sw.m_uGrouping, 1 + p % sw.m_uGrouping);
			}

		if( sw.m_uPorts >= 10 ) {

			return CPrintf("Port%2.2u", 1 + p);
			}

		return CPrintf("Port%u", 1 + p);
		}

	return "";
	}

UINT CNViewListener::GetPortChipId(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_Frame.m_Port.number.chip_id;
			}
		}

	return 0;
	}

UINT CNViewListener::GetPortPortId(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_Frame.m_Port.number.port_id;
			}
		}

	return 0;
	}

bool CNViewListener::GetPortStatus(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_Frame.m_Port.state.link_status ? true : false;
			}
		}

	return 0;
	}

bool CNViewListener::GetPortEnabled(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_Frame.m_Port.state.port_enable ? true : false;
			}
		}

	return 0;
	}

bool CNViewListener::GetPortDuplex(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_Frame.m_Port.state.duplex ? true : false;
			}
		}

	return 0;
	}

bool CNViewListener::GetPortPause(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_Frame.m_Port.state.pause ? true : false;
			}
		}

	return 0;
	}

UINT CNViewListener::GetPortSpeed(UINT s, UINT p) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			return po.m_uSpeed;
			}
		}

	return 0;
	}
// Counter Attributes

UINT CNViewListener::GetCounterCount(void) const
{
	return 39;
	}

CString CNViewListener::GetCounterName(UINT n) const
{
	static PCTXT pNames[] = {

		"TxOctets",		// 0  W
		"TxDropPackets",	// 1
		"TxQosPackets",		// 2
		"TxBroadcastPackets",	// 3
		"TxMulticastPackets",	// 4
		"TxUnicastPackets",	// 5
		"TxCollisions",		// 6
		"TxSingleCollision",	// 7
		"TxMultipleCollision",	// 8
		"TxDeferredTransmit",	// 9
		"TxLateCollision",	// 10
		"TxExcessiveCollision",	// 11
		"TxFrameInDisc",	// 12
		"TxPausePackets",	// 13
		"TxQosOctets",		// 14  W
		"RxOctets",		// 15  W
		"RxUndersizePackets",	// 16
		"RxPausePackets",	// 17
		"PacketsSize1",		// 18
		"PacketsSize2",		// 19
		"PacketsSize3",		// 20
		"PacketsSize4",		// 21
		"PacketsSize5",		// 22
		"PacketsSize6",		// 23
		"RxOversizePackets",	// 24
		"RxJabbers",		// 25
		"RxAlignmentErrors",	// 26
		"RxFcsErrors",		// 27
		"RxGoodOctets",		// 28  W
		"RxDropPackets",	// 29
		"RxUnicastPackets",	// 30
		"RxMulticastPackets",	// 31
		"RxBroadcastPackets",	// 32
		"RxSaChanges",		// 33
		"RxFragments",		// 34
		"RxExcessSizeDisc",	// 35
		"RxSymbolError",	// 36
		"RxQosPackets",		// 37
		"RxQosOctets"		// 38  W
		};

	if( n < elements(pNames) ) {

		return pNames[n];
		}

	return "";
	}

bool CNViewListener::IsCounter64(UINT n) const
{
	switch( n ) {

		case 0:
		case 14:
		case 15:
		case 28:
		case 38:

			return true;
		}

	return false;
	}

UINT CNViewListener::GetCounter32(UINT s, UINT p, UINT n) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			switch( n ) {

				case 1 : return po.m_Frame.m_Mib.tx_drop_pkts          ;
				case 2 : return po.m_Frame.m_Mib.tx_qos_pkts           ;
				case 3 : return po.m_Frame.m_Mib.tx_broadcast_pkts     ;
				case 4 : return po.m_Frame.m_Mib.tx_multicast_pkts     ;
				case 5 : return po.m_Frame.m_Mib.tx_unicast_pkts       ;
				case 6 : return po.m_Frame.m_Mib.tx_collisions         ;
				case 7 : return po.m_Frame.m_Mib.tx_single_collision   ;
				case 8 : return po.m_Frame.m_Mib.tx_multiple_collision ;
				case 9 : return po.m_Frame.m_Mib.tx_deferred_transmit  ;
				case 10: return po.m_Frame.m_Mib.tx_late_collision     ;
				case 11: return po.m_Frame.m_Mib.tx_excessive_collision;
				case 12: return po.m_Frame.m_Mib.tx_frame_in_disc      ;
				case 13: return po.m_Frame.m_Mib.tx_pause_pkts         ;
				case 16: return po.m_Frame.m_Mib.rx_under_size_pkts    ;
				case 17: return po.m_Frame.m_Mib.rx_pause_pkts         ;
				case 18: return po.m_Frame.m_Mib.pkts_64_octets        ;
				case 19: return po.m_Frame.m_Mib.pkts_65to127_octets   ;
				case 20: return po.m_Frame.m_Mib.pkts_128to255_octets  ;
				case 21: return po.m_Frame.m_Mib.pkts_256to511_octets  ;
				case 22: return po.m_Frame.m_Mib.pkts_512to1023_octets ;
				case 23: return po.m_Frame.m_Mib.pkts_1024to1522_octets;
				case 24: return po.m_Frame.m_Mib.rx_over_size_pkts     ;
				case 25: return po.m_Frame.m_Mib.rx_jabbers            ;
				case 26: return po.m_Frame.m_Mib.rx_alignment_errors   ;
				case 27: return po.m_Frame.m_Mib.rx_fcs_errors         ;
				case 29: return po.m_Frame.m_Mib.rx_drop_pkts          ;
				case 30: return po.m_Frame.m_Mib.rx_unicast_pkts       ;
				case 31: return po.m_Frame.m_Mib.rx_multicast_pkts     ;
				case 32: return po.m_Frame.m_Mib.rx_broadcast_pkts     ;
				case 33: return po.m_Frame.m_Mib.rx_sa_changes         ;
				case 34: return po.m_Frame.m_Mib.rx_fragments          ;
				case 35: return po.m_Frame.m_Mib.rx_excess_size_disc   ;
				case 36: return po.m_Frame.m_Mib.rx_symbol_error       ;
				case 37: return po.m_Frame.m_Mib.rx_qos_pkts           ;
				}
			}
		}
	
	return 0;
	}

UINT64 CNViewListener::GetCounter64(UINT s, UINT p, UINT n) const
{
	if( s < m_uSwitch ) {

		CSwitch const &sw = m_pSwitch[s];

		if( p < sw.m_uPorts ) {

			CPort const &po = sw.m_pPorts[p];

			switch( n ) {

				case 0 : return po.m_Frame.m_Mib.tx_octets     ;
				case 14: return po.m_Frame.m_Mib.tx_qos_octets ;
				case 15: return po.m_Frame.m_Mib.rx_octets     ;
				case 28: return po.m_Frame.m_Mib.rx_good_octets;
				case 38: return po.m_Frame.m_Mib.rx_qos_octets ;
				}
			}
		}

	return 0;
	}

// IUnknown

HRESULT CNViewListener::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CNViewListener::AddRef(void)
{
	StdAddRef();
	}

ULONG CNViewListener::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CNViewListener::TaskInit(UINT uTask)
{
	AfxNewObject("sock-raw", ISocket, m_pSock);

	return TRUE;
	}

INT CNViewListener::TaskExec(UINT uTask)
{
	if( m_uSwitch == 0 ) {

		Sleep(FOREVER);
		}

	if( m_pSock->Listen(CIpAddr(224, 1, 1, 0), 0x8874) == S_OK ) {

		m_pSock->SetOption(OPT_ADDRESS, 1);

		for(;;) {

			CBuffer *pBuff;

			if( m_pSock->Recv(pBuff) == S_OK ) {

				if( pBuff->GetSize() >= sizeof(CRawHeader) ) {

					CRawHeader *pRaw = BuffStripHead(pBuff, CRawHeader);

					pRaw->Translate();

					CAcFrame *pFrame = NULL;
					
					if( pBuff->GetSize() > sizeof(CAcPortFrame) ) {

						pFrame = BuffStripHead(pBuff, CAcFrame);

						pFrame->Translate();
						}

					for( UINT s = 0; s < m_uSwitch; s++ ) {

						CSwitch &sw = m_pSwitch[s];

						bool    hit = false;

						if( sw.m_uMatch == 0 ) {

							if( sw.m_Mac.IsEmpty() || sw.m_Mac == pRaw->m_Source ) {

								hit = true;
								}
							}

						if( sw.m_uMatch == 1 ) {

							if( pFrame ) {

								if( sw.m_Ip.IsEmpty() || sw.m_Ip == CIpAddr(pFrame->m_IpAddress) ) {

									hit = true;
									}
								}
							}

						if( hit ) {

							CAcPortFrame *pPort  = NULL;

							UINT          uSpeed = 0;

							if( pFrame ) {

								if( pFrame->m_nVersion == 1 ) {

									CAcFrameVersion1 *pv1 = (CAcFrameVersion1 *) pFrame;

									pBuff->StripHead(sizeof(*pv1) - sizeof(*pFrame));

									pv1->Translate();

									pPort  = &pv1->m_Port;

									uSpeed = 0;
									}

								if( pFrame->m_nVersion == 3 ) {

									CAcFrameVersion3 *pv3 = (CAcFrameVersion3 *) pFrame;

									pBuff->StripHead(sizeof(*pv3) - sizeof(*pFrame));

									pv3->Translate();

									pPort  = &pv3->m_Port;

									uSpeed = pv3->m_ExtData.m_nSpeedMb;
									}

								sw.m_uVersion = pFrame->m_nVersion;

								sw.m_Ip       = pFrame->m_IpAddress;

								sw.m_Ring     = pFrame->m_Ring;

								sw.m_Model    = CString(PTXT(pFrame->m_acModel));
								}
							else {
								pPort = BuffStripHead(pBuff, CAcPortFrame);

								sw.m_uVersion = 0;
								}

							if( pPort ) {

								pPort->Translate();

								UINT uPort = pPort->m_Port.number.port_id;

								UINT uChip = pPort->m_Port.number.chip_id;

								UINT uSlot = uChip * sw.m_uPerChip + uPort;

								if( uSlot < sw.m_uPorts ) {

									sw.m_pPorts[uSlot].m_uSpeed = uSpeed;

									sw.m_pPorts[uSlot].m_Frame  = *pPort;
									}
								}

							gettimeofday(&sw.m_Update, NULL);

							sw.m_fActive = true;

							sw.m_Mac     = pRaw->m_Source;

							sw.m_RxMac   = pRaw->m_Dest;

							break;
							}
						}
					}

				pBuff->Release();

				continue;
				}

			Sleep(5);
			}
		}

	return 0;
	}

void CNViewListener::TaskStop(UINT uTask)
{
	}

void CNViewListener::TaskTerm(UINT uTask)
{
	AfxRelease(m_pSock);
	}

// Implementation

void CNViewListener::ReadConfig(void)
{
	FILE *pFile = fopen("/root/nview.config", "rt");

	if( pFile ) {
	
		fseek(pFile, 0, SEEK_END);

		UINT uSize = ftell(pFile);

		PTXT pData = New char [ uSize + 1 ];

		fseek(pFile, 0, SEEK_SET);

		fread(pData, 1, uSize, pFile);

		pData[uSize] = 0;

		fclose(pFile);

		ParseConfig(pData);

		delete [] pData;

		return;
		}

	ParseConfig("TestSwitch,00-07-AF-E1-E8-60,8,0,0\n");
	}

void CNViewListener::ParseConfig(CString Text)
{
	m_uSwitch = Text.Count('\n');

	if( m_uSwitch ) {

		m_pSwitch = New CSwitch [ m_uSwitch ];

		UINT    n = 0;

		while( !Text.IsEmpty() ) {

			CString Line = Text.StripToken('\n');

			Line.TrimBoth();

			if( !Line.IsEmpty() && !Line.StartsWith(";") ) {

				CStringArray Fields;

				Line.Tokenize(Fields, ',');

				if( Fields.GetCount() == 5 ) {
				
					if( atoi(Fields[2]) ) {

						CSwitch &sw = m_pSwitch[n++];

						sw.m_Name   = Fields[0];

						sw.m_uMatch = 0;

						if( Fields[1].Count('.') ) {

							sw.m_uMatch = 1;

							sw.m_Ip     = CIpAddr(Fields[1]);
							}

						if( Fields[1].Count('-') ) {

							sw.m_uMatch = 0;

							sw.m_Mac     = CMacAddr(Fields[1]);
							}

						sw.m_uPorts         = atoi(Fields[2]);
					
						sw.m_uGrouping      = atoi(Fields[3]);
					
						sw.m_uPerChip       = atoi(Fields[4]);
					
						sw.m_fActive        = false;
					
						sw.m_Update.tv_sec  = 0;
					
						sw.m_Update.tv_usec = 0;
					
						sw.m_uVersion       = 0;

						sw.m_pPorts         = New CPort [ m_pSwitch->m_uPorts ];

						memset(sw.m_pPorts, 0, sw.m_uPorts * sizeof(CPort));

						memset(&sw.m_Ring,  0, sizeof(AcRingData));
						}
					}
				}
			}

		m_uSwitch = n;

		return;
		}

	m_pSwitch = NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Packet Translation
//

void CNViewListener::CRawHeader::Translate(void)
{
	m_Type = NetToHost(m_Type);
	}

void CNViewListener::CAcPortFrame::Translate(void)
{
	m_Mib.tx_octets              = IntelToHost(m_Mib.tx_octets             );
	m_Mib.tx_drop_pkts           = IntelToHost(m_Mib.tx_drop_pkts          );
	m_Mib.tx_qos_pkts            = IntelToHost(m_Mib.tx_qos_pkts           );
	m_Mib.tx_broadcast_pkts      = IntelToHost(m_Mib.tx_broadcast_pkts     );
	m_Mib.tx_multicast_pkts      = IntelToHost(m_Mib.tx_multicast_pkts     );
	m_Mib.tx_unicast_pkts        = IntelToHost(m_Mib.tx_unicast_pkts       );
	m_Mib.tx_collisions          = IntelToHost(m_Mib.tx_collisions         );
	m_Mib.tx_single_collision    = IntelToHost(m_Mib.tx_single_collision   );
	m_Mib.tx_multiple_collision  = IntelToHost(m_Mib.tx_multiple_collision );
	m_Mib.tx_deferred_transmit   = IntelToHost(m_Mib.tx_deferred_transmit  );
	m_Mib.tx_late_collision      = IntelToHost(m_Mib.tx_late_collision     );
	m_Mib.tx_excessive_collision = IntelToHost(m_Mib.tx_excessive_collision);
	m_Mib.tx_frame_in_disc       = IntelToHost(m_Mib.tx_frame_in_disc      );
	m_Mib.tx_pause_pkts          = IntelToHost(m_Mib.tx_pause_pkts         );
	m_Mib.tx_qos_octets          = IntelToHost(m_Mib.tx_qos_octets         );
	m_Mib.rx_octets              = IntelToHost(m_Mib.rx_octets             );
	m_Mib.rx_under_size_pkts     = IntelToHost(m_Mib.rx_under_size_pkts    );
	m_Mib.rx_pause_pkts          = IntelToHost(m_Mib.rx_pause_pkts         );
	m_Mib.pkts_64_octets         = IntelToHost(m_Mib.pkts_64_octets        );
	m_Mib.pkts_65to127_octets    = IntelToHost(m_Mib.pkts_65to127_octets   );
	m_Mib.pkts_128to255_octets   = IntelToHost(m_Mib.pkts_128to255_octets  );
	m_Mib.pkts_256to511_octets   = IntelToHost(m_Mib.pkts_256to511_octets  );
	m_Mib.pkts_512to1023_octets  = IntelToHost(m_Mib.pkts_512to1023_octets );
	m_Mib.pkts_1024to1522_octets = IntelToHost(m_Mib.pkts_1024to1522_octets);
	m_Mib.rx_over_size_pkts      = IntelToHost(m_Mib.rx_over_size_pkts     );
	m_Mib.rx_jabbers             = IntelToHost(m_Mib.rx_jabbers            );
	m_Mib.rx_alignment_errors    = IntelToHost(m_Mib.rx_alignment_errors   );
	m_Mib.rx_fcs_errors          = IntelToHost(m_Mib.rx_fcs_errors         );
	m_Mib.rx_good_octets         = IntelToHost(m_Mib.rx_good_octets        );
	m_Mib.rx_drop_pkts           = IntelToHost(m_Mib.rx_drop_pkts          );
	m_Mib.rx_unicast_pkts        = IntelToHost(m_Mib.rx_unicast_pkts       );
	m_Mib.rx_multicast_pkts      = IntelToHost(m_Mib.rx_multicast_pkts     );
	m_Mib.rx_broadcast_pkts      = IntelToHost(m_Mib.rx_broadcast_pkts     );
	m_Mib.rx_sa_changes          = IntelToHost(m_Mib.rx_sa_changes         );
	m_Mib.rx_fragments           = IntelToHost(m_Mib.rx_fragments          );
	m_Mib.rx_excess_size_disc    = IntelToHost(m_Mib.rx_excess_size_disc   );
	m_Mib.rx_symbol_error        = IntelToHost(m_Mib.rx_symbol_error       );
	m_Mib.rx_qos_pkts            = IntelToHost(m_Mib.rx_qos_pkts           );
	m_Mib.rx_qos_octets          = IntelToHost(m_Mib.rx_qos_octets         );
	}

void CNViewListener::CAcFrame::Translate(void)
{
	m_nVersion = IntelToHost(m_nVersion);
	}

void CNViewListener::CAcFrameVersion1::Translate(void)
{
	m_Port.Translate();
	}

void CNViewListener::CAcFrameVersion3::Translate(void)
{
	m_ExtData.m_nSpeedMb = IntelToHost(m_ExtData.m_nSpeedMb);

	m_Port.Translate();
	}

// End of File
