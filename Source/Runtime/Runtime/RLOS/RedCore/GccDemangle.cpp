
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Name Demangling
//

// Prototypes

global bool GccDemangleName(char *pName, UINT uName, PCTXT pMangled);
static void AddToName(PTXT pOut, PCTXT pIn, UINT uCount);

// Code

global bool GccDemangleName(char *pName, UINT uName, PCTXT pMangled)
{
	// This is far from perfect but it handles 99% of the real function
	// names that you'll come across. The link below explains the full
	// schema if you want to spend some time making this better! There
	// is also abi::__cxa_demangle provided by the compiler, but good
	// luck trying to get it to work properly!
	// 
	// https://itanium-cxx-abi.github.io/cxx-abi/abi.html#mangling

	if( pMangled[0] == '_' && pMangled[1] == 'Z' ) {

		*pName = 0;

		if( isdigit(pMangled[2]) ) {

			PCTXT pFrom = pMangled + 2;

			PTXT  pNext = NULL;

			UINT  uSize = strtoul(pFrom, &pNext, 10);

			AddToName(pName, pNext, uSize);

			return true;
			}

		if( pMangled[2] == 'T' && pMangled[3] == 'h' && pMangled[4] == 'n' ) {

			if( isdigit(pMangled[5]) ) {

				if( pMangled[6] == '_') {

					pMangled += 5;
					}
				else {
					if( isdigit(pMangled[6]) && pMangled[7] == '_') {

						pMangled += 6;
						}
					}
				}
			}

		if( pMangled[2] == 'n' && (pMangled[3] == 'w' || pMangled[3] == 'a') ) {

			strcpy(pName, "operator new");

			if( pMangled[3] == 'a' ) {

				strcat(pName, " []");
				}

			return true;
			}

		if( pMangled[2] == 'd' && (pMangled[3] == 'l' || pMangled[3] == 'a') ) {

			strcpy(pName, "operator delete");

			if( pMangled[3] == 'a' ) {

				strcat(pName, " []");
				}

			return true;
			}

		if( pMangled[2] == 'L' ) {

			for( UINT n = 3; pMangled[n]; n++ ) {

				if( isdigit(pMangled[n]) ) {

					PCTXT pFrom = pMangled + n;

					PTXT  pNext = NULL;

					UINT  uSize = strtoul(pFrom, &pNext, 10);

					AddToName(pName, pNext, uSize);

					return true;
					}
				}
			}

		if( pMangled[2] == 'N' ) {

			for( UINT n = 3; pMangled[n]; n++ ) {

				if( isdigit(pMangled[n]) ) {

					PCTXT pFrom = pMangled + n;

					for(;;) { 

						PTXT  pNext = NULL;

						UINT  uSize = strtoul(pFrom, &pNext, 10);

						AddToName(pName, pNext, uSize);

						pFrom = pNext + uSize;

						if( isdigit(*pFrom) ) {

							strcat(pName, "::");

							continue;
							}

						break;
						}

					return true;
					}
				}
			}
		}

	strcpy(pName, pMangled);

	return false;
	}

static void AddToName(PTXT pOut, PCTXT pIn, UINT uCount)
{
	UINT n = strlen(pOut);

	memcpy(pOut + n, pIn, uCount);

	pOut[n + uCount] = 0;
	}

// End of File
