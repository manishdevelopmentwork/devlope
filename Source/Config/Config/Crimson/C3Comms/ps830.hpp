
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PS830_HPP
	
#define	INCLUDE_PS830_HPP

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific 830 Device Options
//

class CPacSci830DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPacSci830DeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
						

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific Space Wrapper Class
//

class CSpacePacSci830 : public CSpace
{
	public:
		// Constructors

		CSpacePacSci830(UINT t, UINT i, CString c, CString p, AddrType Type, UINT n=0, UINT x=0, BOOL fRO=FALSE);

		// Public Data

		UINT m_Index;
		UINT m_ReadOnly;
		
	};


//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific Master Driver
//

class CPacSci830SerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CPacSci830SerialDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
							
	protected:
					
		// Implementation
		void	AddSpaces(void); 
	};

//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific Address Selection Dialog
//

class CPacSci830AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPacSci830AddrDialog(CPacSci830SerialDriver &Driver, CAddress &Addr, BOOL fPart);

		
	protected:
		// Overridables
		void    SetAddressText(CString Text);
	};



//////////////////////////////////////////////////////////////////////////
//
// Pacific Scientific Constants
//

#define FL	0x100
#define CM	0x200

/* ----------------------------------------------------
 * Identifier Codes for Real Time Calls
 * ---------------------------------------------------- */
#define MC_NVLoad                       0
#define MC_NVSave                       1
#define MC_Unconfigure                  2
#define MC_LoadCalTable                 3
#define MC_SaveCalTable                 4
#define MC_TableGen                     5

/* ----------------------------------------------------
 * Identifier Codes for Integer Variables
 * ---------------------------------------------------- */
#define MI_MfgConfig                    0
#define MI_CfgD                         1
#define MI_ILmtMinus                    2
#define MI_ILmtPlus                     3
#define MI_DBGI                         4
#define MI_DM1Map                       5
#define MI_Enable                       6
#define MI_Enabled                      7
#define MI_EncIn                        8
#define MI_EncPos                       9
#define MI_FaultCode                    10
#define MI_DBGL                         11
#define MI_Inp1                         12
#define MI_Inp2                         13
#define MI_Inp3                         14
#define MI_Inp4                         15
#define MI_Inp5                         16
#define MI_Inp6                         17
#define MI_Inputs                       18
#define MI_KpEnc                        19
#define MI_KiEnc                        20
#define MI_KdEnc                        21
#define MI_ElecAngTau                   22
#define MI_EncAlignTime                 23
#define MI_EncAlignTestDist             24
#define MI_EncAlignRampIcmd             25
#define MI_Out1                         26
#define MI_Out2                         27
#define MI_Out3                         28
#define MI_Out4                         29
#define MI_Outputs                      30
#define MI_PosCommand                   31
#define MI_PosError                     32
#define MI_ResPos                       33
#define MI_Position                     34
#define MI_PulsesIn                     35
#define MI_PulsesOut                    36
#define MI_EncOut                       37
#define MI_EncMode                      38
#define MI_PoleCount                    39
#define MI_BaudRate                     40
#define MI_Model                        41
#define MI_AxisAddr                     42
#define MI_ItThresh                     43
#define MI_FwV                          44
#define MI_BlkType                      45
#define MI_DBG4                         46
#define MI_DBG2                         47
#define MI_DBG1                         48
#define MI_HwV                          49
#define MI_CommEnbl                     50
#define MI_ExtFault                     51
#define MI_Motor1                       52
#define MI_CwInh                        53
#define MI_CcwInh                       54
#define MI_PulsesFOut                   55
#define MI_PosCmdSet                    56
#define MI_DM2Map                       57
#define MI_RCalMode                     58
#define MI_RCalMAng                     59
#define MI_RCalData                     60
#define MI_FaultReset                   61
#define MI_RunStop                      62
#define MI_CommSrc                      63
#define MI_Enable2                      64
#define MI_RemoteFB                     65
#define MI_InpMap1                      66
#define MI_InpMap2                      67
#define MI_InpMap3                      68
#define MI_InpMap4                      69
#define MI_InpMap5                      70
#define MI_InpMap6                      71
#define MI_OutMap1                      72
#define MI_OutMap2                      73
#define MI_OutMap3                      74
#define MI_OutMap4                      75
#define MI_VelCmdSrc                    76
#define MI_Brake                        77
#define MI_MfgLock                      78
#define MI_CCSNum                       79
#define MI_CCDate                       80
#define MI_EEInt                        81
#define MI_SIAV                         82
#define MI_AInNull                      83
#define MI_PosErrorMax                  84
#define MI_Fault                        85
#define MI_ILmtMode                     86
#define MI_AIn1Map                      87
#define MI_AIn2Map                      88
#define MI_AIn3Map                      89
#define MI_Motor2                       90
#define MI_DigitalCmd                   91
#define MI_Move0Type                    92
#define MI_Move0Distance                93
#define MI_DbgVar0                      94
#define MI_Move0Dwell                   95
#define MI_Move0DistOffset              96
#define MI_Move0HomeDir                 97
#define MI_Move0HomeMode                98
#define MI_Move1Type                    99
#define MI_Move1Distance                100
#define MI_DbgVar1                      101
#define MI_Move1Dwell                   102
#define MI_Move1DistOffset              103
#define MI_Move1HomeDir                 104
#define MI_Move1HomeMode                105
#define MI_Move2Type                    106
#define MI_Move2Distance                107
#define MI_DbgVar2                      108
#define MI_Move2Dwell                   109
#define MI_Move2DistOffset              110
#define MI_Move2HomeDir                 111
#define MI_Move2HomeMode                112
#define MI_Move3Type                    113
#define MI_Move3Distance                114
#define MI_DbgVar3                      115
#define MI_Move3Dwell                   116
#define MI_Move3DistOffset              117
#define MI_Move3HomeDir                 118
#define MI_Move3HomeMode                119
#define MI_Move4Type                    120
#define MI_Move4Distance                121
#define MI_DbgVar4                      122
#define MI_Move4Dwell                   123
#define MI_Move4DistOffset              124
#define MI_Move4HomeDir                 125
#define MI_Move4HomeMode                126
#define MI_Move5Type                    127
#define MI_Move5Distance                128
#define MI_DbgVar5                      129
#define MI_Move5Dwell                   130
#define MI_Move5DistOffset              131
#define MI_Move5HomeDir                 132
#define MI_Move5HomeMode                133
#define MI_Move6Type                    134
#define MI_Move6Distance                135
#define MI_DbgVar6                      136
#define MI_Move6Dwell                   137
#define MI_Move6DistOffset              138
#define MI_Move6HomeDir                 139
#define MI_Move6HomeMode                140
#define MI_Move7Type                    141
#define MI_Move7Distance                142
#define MI_DbgVar7                      143
#define MI_Move7Dwell                   144
#define MI_Move7DistOffset              145
#define MI_Move7HomeDir                 146
#define MI_Move7HomeMode                147
#define MI_ActiveMoveType               148
#define MI_ActiveDistance               149
#define MI_InPosLimit                   150
#define MI_ActiveDwell                  151
#define MI_ActiveDistOffset             152
#define MI_ActiveHomeDir                153
#define MI_ActiveHomeMode               154
#define MI_ActiveMove                   155
#define MI_StartMove                    156
#define MI_MoveDone                     157
#define MI_MoveSelectBit0               158
#define MI_MoveSelectBit1               159
#define MI_MoveSelectBit2               160
#define MI_GearingOn                    161
#define MI_Move0RegSelect               162
#define MI_Move1RegSelect               163
#define MI_Move2RegSelect               164
#define MI_Move3RegSelect               165
#define MI_Move4RegSelect               166
#define MI_Move5RegSelect               167
#define MI_Move6RegSelect               168
#define MI_Move7RegSelect               169
#define MI_Reg1ResolverPosition         170
#define MI_Reg1EncoderPosition          171
#define MI_Reg2ResolverPosition         172
#define MI_Reg2EncoderPosition          173
#define MI_Reg1ActiveEdge               174
#define MI_Reg2ActiveEdge               175
#define MI_ActiveRegSelect              176
#define MI_HomeSwitch                   177
#define MI_HallState                    178
#define MI_HallOffset			179
#define MI_DriveStatus			180

/* ----------------------------------------------------
 * Identifier Codes for Float Variables
 * ---------------------------------------------------- */
#define MF_AnalogIn                      0
#define MF_AnalogOut1                    1
#define MF_EncFreq                       2
#define MF_Velocity                      3
#define MF_ARF0                          4
#define MF_ARF1                          5
#define MF_Kvi                           6
#define MF_ItF0                          7
#define MF_Kpp                           8
#define MF_Kvp                           9
#define MF_Kvff                          10
#define MF_DM1F0                         11
#define MF_ADF0                          12
#define MF_ADOffset                      13
#define MF_Ipeak                         14
#define MF_DM1Gain                       15
#define MF_CmdGain                       16
#define MF_CommOff                       17
#define MF_ItFilt                        18
#define MF_VelCmd                        19
#define MF_VelErr                        20
#define MF_ICmd                          21
#define MF_IFB                           22
#define MF_FVelErr                       23
#define MF_DM1Out                        24
#define MF_VelFB                         25
#define MF_DM2Out                        26
#define MF_Kip                           27
#define MF_PWMFreq                       28
#define MF_PWMDeadBand                   29
#define MF_AnalogOut2                    30
#define MF_StopTime                      31
#define MF_DM2Gain                       32
#define MF_Kii                           33
#define MF_RCalTime                      34
#define MF_DM2F0                         35
#define MF_VelCmd2                       36
#define MF_VelCmdA                       37
#define MF_HSTemp                        38
#define MF_IU                            39
#define MF_IV                            40
#define MF_IW                            41
#define MF_IqCmd                         42
#define MF_VBusThresh                    43
#define MF_VBus                          44
#define MF_AccelLmt                      45
#define MF_DecelLmt                      46
#define MF_IqFB                          47
#define MF_VelLmtHi                      48
#define MF_VelLmtLo                      49
#define MF_B1                            50
#define MF_B2                            51
#define MF_K1                            52
#define MF_K2                            53
#define MF_ARZ0                          54
#define MF_ARZ1                          55
#define MF_EncInF0                       56
#define MF_COS_COEFF_H1                  57
#define MF_SIN_COEFF_H1                  58
#define MF_COS_COEFF_H2                  59
#define MF_SIN_COEFF_H2                  60
#define MF_COS_COEFF_H4                  61
#define MF_SIN_COEFF_H4                  62
#define MF_COS_COEFF_H5                  63
#define MF_SIN_COEFF_H5                  64
#define MF_COS_COEFF_H7                  65
#define MF_SIN_COEFF_H7                  66
#define MF_COS_COEFF_H8                  67
#define MF_SIN_COEFF_H8                  68
#define MF_COS_COEFF_H10                 69
#define MF_SIN_COEFF_H10                 70
#define MF_COS_COEFF_H11                 71
#define MF_SIN_COEFF_H11                 72
#define MF_COS_COEFF_H13                 73
#define MF_SIN_COEFF_H13                 74
#define MF_COS_COEFF_H14                 75
#define MF_SIN_COEFF_H14                 76
#define MF_COS_COEFF_H16                 77
#define MF_SIN_COEFF_H16                 78
#define MF_COS_COEFF_H17                 79
#define MF_SIN_COEFF_H17                 80
#define MF_COS_COEFF_H19                 81
#define MF_SIN_COEFF_H19                 82
#define MF_ItThreshA                     83
#define MF_VBusFTime                     84
#define MF_IqIH                          85
#define MF_IqMax                         86
#define MF_VdCmd                         87
#define MF_VqCmd                         88
#define MF_ImMax                         89
#define MF_CmdGain2                      90
#define MF_ZeroSpeedThresh               91
#define MF_ITDerateIntercept             92
#define MF_ITDerateSlope                 93
#define MF_DigitalCmdFreq                94
#define MF_Move0RunSpeed                 95
#define MF_Move0AccelRate                96
#define MF_Move0DecelRate                97
#define MF_Move1RunSpeed                 98
#define MF_Move1AccelRate                99
#define MF_Move1DecelRate                100
#define MF_Move2RunSpeed                 101
#define MF_Move2AccelRate                102
#define MF_Move2DecelRate                103
#define MF_Move3RunSpeed                 104
#define MF_Move3AccelRate                105
#define MF_Move3DecelRate                106
#define MF_Move4RunSpeed                 107
#define MF_Move4AccelRate                108
#define MF_Move4DecelRate                109
#define MF_Move5RunSpeed                 110
#define MF_Move5AccelRate                111
#define MF_Move5DecelRate                112
#define MF_Move6RunSpeed                 113
#define MF_Move6AccelRate                114
#define MF_Move6DecelRate                115
#define MF_Move7RunSpeed                 116
#define MF_Move7AccelRate                117
#define MF_Move7DecelRate                118
#define MF_ActiveRunSpeed                119
#define MF_ActiveAccelRate               120
#define MF_ActiveDecelRate               121
#define MF_IntgStopThresh                122
#define MF_I2tThresh                     123
#define MF_I2tF0                         124
#define MF_I2tFilt                       125
#define MF_AnalogILmt                    126
#define MF_AnalogILmtGain                127
#define MF_AnalogILmtFilt                128
#define MF_AnalogILmtOffset              129
#define MF_ActualILmtPlus		 130
#define MF_ActualILmtMinus		 131


// End of File	


#endif
