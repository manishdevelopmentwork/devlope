
#include "Intern.hpp"

#include "Emif437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 External Memory Interface Controller
//

// Register Access

#define Reg(x)	    (m_pBase[reg##x])

#define RegN(x, n)  (m_pBase[reg##x + n])

// Constructor

CEmif437::CEmif437(void)
{
	m_pBase = PVDWORD(ADDR_EMIF);
	}

// Operations

void CEmif437::StartConfigEnable(bool fEnable)
{
	Reg(SDRAMREFCTRL) = fEnable ? 0x80003000 : 0x00003000;
	}

void CEmif437::SetDdrPhyConfig(bool fHalf, bool fStall, bool fCalib, bool fInvClk, bool fFastLock)
{
	DWORD Mask = 0x003C0200;

	DWORD Data = (fHalf     ? Bit(21) : 0) | 
		     (fStall    ? Bit(20) : 0) | 
		     (fCalib    ? Bit(19) : 0) | 
		     (fInvClk   ? Bit(18) : 0) | 
		     (fFastLock ? Bit(9)  : 0);

	Reg(DDRPHYCTRL1)  &= ~Mask;

	Reg(DDRPHYCTRL1)  |= Data;

	Reg(DDRPHYCTRL1S) &= ~Mask;

	Reg(DDRPHYCTRL1S) |= Data;
	}

void CEmif437::SetDdrPhyConfig(UINT uLockDiff, UINT uReadLat)
{
	DWORD Mask = 0x0000FC1F;

	DWORD Data = (uLockDiff << 10) | uReadLat;

	Reg(DDRPHYCTRL1)  &= ~Mask;

	Reg(DDRPHYCTRL1)  |= Data;

	Reg(DDRPHYCTRL1S) &= ~Mask;

	Reg(DDRPHYCTRL1S) |= Data;
	}

void CEmif437::SetExtPhyCtrl(UINT uCtrl, DWORD dwData)
{
	if( uCtrl >= 1 && uCtrl <= 36 ) {
		
		uCtrl --;

		RegN(EXTPHYCTRL,  uCtrl * 2) = dwData;
	
		RegN(EXTPHYCTRLS, uCtrl * 2) = dwData;
		}
	}

void CEmif437::SetExtPhyCtrlBit(UINT uCtrl, UINT uBit)
{
	if( uCtrl >= 1 && uCtrl <= 36 ) {
		
		uCtrl --;

		RegN(EXTPHYCTRL,  uCtrl * 2) |= Bit(uBit);
	
		RegN(EXTPHYCTRLS, uCtrl * 2) |= Bit(uBit);
		}
	}

void CEmif437::SetPhySlaveRatio(UINT uRatio)
{
	DWORD Data = (uRatio << 20) | (uRatio << 10) | uRatio << 0;

	SetExtPhyCtrl(1, Data);
	}

void CEmif437::SetPhyGateLevelRatio(UINT uRatio)
{
	DWORD Data = (uRatio << 16) | uRatio;

	SetExtPhyCtrl(26, Data);
	SetExtPhyCtrl(27, Data);
	SetExtPhyCtrl(28, Data);
	SetExtPhyCtrl(29, Data);
	SetExtPhyCtrl(30, Data);
	}

void CEmif437::SetPhyWriteLevelRatio(UINT uRatio)
{
	DWORD Data = (uRatio << 16) | uRatio;

	SetExtPhyCtrl(31, Data);
	SetExtPhyCtrl(32, Data);
	SetExtPhyCtrl(33, Data);
	SetExtPhyCtrl(34, Data);
	SetExtPhyCtrl(35, Data);
	}

void CEmif437::SetPhyControlDelay(UINT uFifoDelay, UINT uCtrlDelay)
{
	DWORD Data = (uFifoDelay << 16) | uCtrlDelay;

	SetExtPhyCtrl(22, Data);
	}

void CEmif437::SetPhyWriteDQSDelay(UINT uRead, UINT uWrite)
{
	DWORD Data = (uWrite << 16) | uRead;

	SetExtPhyCtrl(23, Data);
	}

void CEmif437::SetPhyRank0Delay(UINT uDqOffset, UINT uGateLev, UINT uRank0, UINT uWrData)
{
	DWORD Data = (uDqOffset << 24) | (uGateLev << 16) | (uRank0 << 12) | uWrData;
	
	SetExtPhyCtrl(24, Data);
	}

void CEmif437::SetPhyDqOffset(UINT uOffset)
{
	DWORD Data = (uOffset << 21) | (uOffset << 14) | (uOffset << 7) | uOffset;
	
	SetExtPhyCtrl(25, Data);
	}

void CEmif437::SetPhyNumDq(UINT uWriteLevel, UINT uGateLevel)
{
	DWORD Data = (uWriteLevel << 4) | uGateLevel;
	
	SetExtPhyCtrl(36, Data);
	}

void CEmif437::SetPhyCtrlClear(bool fErc, bool fUnlock, bool fMissAligned)
{
	if( fErc ) {

		SetExtPhyCtrlBit(36, 10);
		}

	if( fUnlock ) {

		SetExtPhyCtrlBit(36, 9);
		}

	if( fMissAligned ) {

		SetExtPhyCtrlBit(36, 8);
		}
	}

void CEmif437::SetSdramConfig(UINT uType, UINT uRow, UINT uPage, UINT uBanks, UINT uWidth)
{
	DWORD Mask = 0xE000C3F7;

	DWORD Data = Reg(SDRAMCFG1);

	Data &= ~Mask;

	switch( uPage ) {

		case 256:  uPage = 0; break;
		case 512:  uPage = 1; break;
		case 1024: uPage = 2; break;
		case 2048: uPage = 3; break;
		default:   uPage = 3; break;
		}

	uRow  -= 9;

	uBanks = (uBanks == 8) ? 3 : uBanks / 2;

	uWidth = (uWidth == 32) ? 0 : 1;

	Data |= (uType  << 29);
	Data |= (uWidth << 14);
	Data |= (uRow   <<  7);
	Data |= (uBanks <<  4);
	Data |= (uPage  <<  0);
	
	Reg(SDRAMCFG1) &= ~Mask;

	Reg(SDRAMCFG1) |= Data;
	}

void CEmif437::SetSdramDrive(UINT uTerm, UINT DQS, UINT ODT, UINT RZQ, UINT CWL, UINT CL, UINT CS)
{
	DWORD Mask = 0x1FFF3C08;

	DWORD Data = Reg(SDRAMCFG1);

	Data &= ~Mask;

	Data |= (uTerm << 24);
	Data |= (DQS   << 23);
	Data |= (ODT   << 21);
	Data |= (RZQ   << 18);
	Data |= (CWL   << 16);
	Data |= (CL    << 10);
	Data |= (CS    <<  3); 
	
	Reg(SDRAMCFG1) &= ~Mask;

	Reg(SDRAMCFG1) |= Data;
	}

void CEmif437::SetSdramRefresh(UINT EN, UINT SRT, UINT ASR, UINT PASR, UINT uRate)
{
	DWORD Data = (EN << 31) | (SRT << 29) | (ASR << 28) | (PASR << 24) | uRate;	
	
	Reg(SDRAMREFCTRL)  = Data;

	Reg(SDRAMREFCTRLS) = Data;
	}

void CEmif437::SetSdramTimings1(UINT RTW, UINT RP, UINT RCD, UINT WR, UINT RA, UINT RAS, UINT RC, UINT RRD, UINT WTR)
{
	DWORD Data = (RTW << 29) | 
		     (RP  << 25) | 
		     (RCD << 21) | 
		     (WR  << 17) | 
		     (RA  << 16) |
		     (RAS << 12) | 
		     (RC  <<  6) |
		     (RRD <<  3) | 
		     (WTR <<  0) ;

	Reg(SDRAMTIM1)  = Data;

	Reg(SDRAMTIM1S) = Data;
	}

void CEmif437::SetSdramTimings2(UINT XP, UINT ODT, UINT XSNR, UINT XSRD, UINT RTP, UINT CKE)
{
	DWORD Data = (XP   << 28) | 
		     (ODT  << 25) | 
		     (XSNR << 16) | 
		     (XSRD <<  6) | 
		     (RTP  <<  3) | 
		     (CKE  <<  0) ;

	Reg(SDRAMTIM2)  = Data;

	Reg(SDRAMTIM2S) = Data;
	}

void CEmif437::SetSdramTimings3(UINT PDLL, UINT CSTA, UINT CKESR, UINT ZQCS, UINT TDQS, UINT RFC, UINT RAS)
{
	DWORD Data = (PDLL  << 28) | 
		     (CSTA  << 24) | 
		     (CKESR << 21) | 
		     (ZQCS  << 15) | 
		     (TDQS  << 13) | 
		     (RFC   <<  4) |
		     (RAS   <<  0) ; 

	Reg(SDRAMTIM3)  = Data;

	Reg(SDRAMTIM3S) = Data;
	}

void CEmif437::SetSdramZQConfig(bool fCS1, bool fCS0, bool fDual, bool fSFEXIT, UINT ZQINIT, UINT ZQCL, UINT REF)
{
	Reg(SDRAMCALIB) = (fCS1    ? Bit(31) : 0) |
		          (fCS0    ? Bit(30) : 0) |
		          (fDual   ? Bit(29) : 0) |
		          (fSFEXIT ? Bit(28) : 0) |
		          (ZQINIT    << 18      ) |
		          (ZQCL	     << 16	) |
		          (REF       <<  0	) ;
	}

void CEmif437::SetDLLCalib(UINT uAckWait, UINT uInterval)
{
	DWORD Data = (uAckWait << 16) | uInterval; 

	Reg(DLLCALIB)  = Data;

	Reg(DLLCALIBS) = Data;
	}

void CEmif437::SetPriorityClassMap(bool fEnable, WORD wMap)
{
	Reg(PRICOSMAP) = (fEnable ? Bit(31) : 0) | wMap;
	}

void CEmif437::SetConnect1ClassMap(bool fEnable, WORD wMap)
{
	Reg(CONN1COSMAP) = (fEnable ? Bit(31) : 0) | wMap;
	}

void CEmif437::SetConnect2ClassMap(bool fEnable, WORD wMap)
{
	Reg(CONN2COSMAP) = (fEnable ? Bit(31) : 0) | wMap;
	}

void CEmif437::SetExecuteThreshold(UINT uRead, UINT uWrite)
{
	Reg(RDWREXECTHR) = (uWrite << 8) | uRead;
	}

void CEmif437::SetPriorityCounters(UINT uCount1, UINT uCount2, UINT uOld)
{
	Reg(COSCONFIG) = (uCount1 << 16) | (uCount2 << 8) | uOld;
	}

void CEmif437::SetEnableReadWriteLeveling(bool fEnable)
{
	if( fEnable ) {

		Reg(RDWRLVLRMPWIN)  = 0x00000000;
		
		Reg(RDWRLVLRMPCTRL) = 0x80000000;
		
		Reg(RDWRLVLCTRL)    = 0x80000000;

		while( Reg(RDWRLVLCTRL) & Bit(31) );
		}
	else {
		Reg(RDWRLVLRMPCTRL) &= ~Bit(31);
		
		Reg(RDWRLVLCTRL)    &= ~Bit(31);
		}
	}

// End of File
