
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CATPYROMASTER_HPP
	
#define	INCLUDE_CATPYROMASTER_HPP

//////////////////////////////////////////////////////////////////////////
//
// CatPyro Master Driver
//

class CCatPyroMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CCatPyroMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Driver Options
//

class CCatPyroMasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCatPyroMasterDriverOptions(void);

		// Public Data
		UINT m_Drop;
	
	protected:
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
