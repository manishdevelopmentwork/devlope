#!/bin/sh

# c3-init
#
# Bring up the networking functionality by starting the
# required intrefaces and running the various init scripts
# created by the net applicator.

main()
{
	if [ -e /vap/opt/crimson/config/net/deps ]
	then
		init_net 1
		init_one firewall
		init_one macfilt
		init_net 2
		init_one dns
		init_one routes
		init_one tlss
		init_one tlsc
		init_one snmp
	fi
}

init_one()
{
	if [ -e /vap/opt/crimson/config/$1/init ]
	then
		/vap/opt/crimson/config/$1/init
	fi
}

init_net()
{
	if [ "$1" == "1" ]
	then
		/vap/opt/crimson/config/net/init1
	else
		init_dep fast

		init_dep slow
	
		if [ -f /vap/opt/crimson/config/net/init2 ]
		then
			/vap/opt/crimson/config/net/init2
		fi
	fi
}

init_dep()
{
	/opt/crimson/scripts/c3-dep ifup $1
}

main $*
