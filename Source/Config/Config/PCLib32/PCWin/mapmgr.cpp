
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Handle Map Manager
//

// Static Data

CMapManager * CMapManager::m_pThis = NULL;

// Constructor

CMapManager::CMapManager(void)
{
	AfxAssert(!m_pThis);
	
	m_pThis = this;
	
	m_uLock = 0;
	}

// Destructor

CMapManager::~CMapManager(void)
{
	EmptyAll();
	
	m_pThis = NULL;
	}

// Object Location

CMapManager * CMapManager::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Attributes

BOOL CMapManager::IsEmpty(void) const
{
	if( !m_PermMap.IsEmpty() ) {

		return FALSE;
		}
	
	if( !m_TempMap.IsEmpty() ) {

		return FALSE;
		}

	return TRUE;
	}

// Handle Lookup

CObject * CMapManager::FromHandle(HANDLE hObject, UINT uSpace)
{
	if( TRUE ) {

		CRdGuard Lock = m_PermMap.GetRdGuard();
		
		if( !m_PermMap.IsEmpty() ) {

			CName Name = { hObject, uSpace };

			INDEX nPos = m_PermMap.FindName(Name);

			if( !m_PermMap.Failed(nPos) ) {
		
				CObject *pObject = m_PermMap.GetData(nPos);

				return pObject;
				}
			}
		}

	if( TRUE ) {
			
		CRdGuard Lock = m_TempMap.GetRdGuard();
		
		if( !m_TempMap.IsEmpty() ) {

			CName Name = { hObject, uSpace };

			INDEX nPos = m_TempMap.FindName(Name);

			if( !m_TempMap.Failed(nPos) ) {
		
				CObject *pObject = m_TempMap.GetData(nPos);

				return pObject;
				}
			}
		}

	return NULL;
	}

BOOL CMapManager::IsTemporary(HANDLE hObject, UINT uSpace)
{
	CRdGuard Lock = m_TempMap.GetRdGuard();
	
	if( !m_TempMap.IsEmpty() ) {

		CName Name = { hObject, uSpace };

		INDEX nPos = m_TempMap.FindName(Name);

		return !m_TempMap.Failed(nPos);
		}

	return FALSE;
	}

// Handle Operations

void CMapManager::InsertPerm(HANDLE hObject, UINT uSpace, CObject *pObject)
{
	RemovePerm(hObject, uSpace, NULL);

	CWrGuard Lock = m_PermMap.GetWrGuard();
	
	CName Name = { hObject, uSpace };

	m_PermMap.Insert(Name, pObject);
	}

void CMapManager::InsertTemp(HANDLE hObject, UINT uSpace, CObject *pObject)
{
	CWrGuard Lock = m_TempMap.GetWrGuard();
	
	CName Name = { hObject, uSpace };

	m_TempMap.Insert(Name, pObject);
	}

BOOL CMapManager::RemoveBoth(HANDLE hObject, UINT uSpace, CObject *pObject)
{
	if( RemovePerm(hObject, uSpace, pObject) ) {

		return TRUE;
		}
		
	if( RemoveTemp(hObject, uSpace, pObject) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CMapManager::RemovePerm(HANDLE hObject, UINT uSpace, CObject *pObject)
{
	if( hObject ) {

		CWrGuard Lock = m_PermMap.GetWrGuard();

		CName    Name = { hObject, uSpace };

		INDEX    nPos = m_PermMap.FindName(Name);
	
		if( !m_PermMap.Failed(nPos) ) {
		
			if( !pObject || m_PermMap.GetData(nPos) == pObject ) {
			
				m_PermMap.Remove(nPos);

				return TRUE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMapManager::RemoveTemp(HANDLE hObject, UINT uSpace, CObject *pObject)
{
	if( hObject ) {

		CWrGuard Lock = m_TempMap.GetWrGuard();

		CName    Name = { hObject, uSpace };

		INDEX    nPos = m_TempMap.FindName(Name);
	
		if( !m_TempMap.Failed(nPos) ) {
		
			if( !pObject || m_TempMap.GetData(nPos) == pObject ) {
			
				m_TempMap.Remove(nPos);

				return TRUE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Global Operations

void CMapManager::EmptyAll(void)
{
	if( TRUE ) {

		CWrGuard LockPerm = m_PermMap.GetWrGuard();
		
		CWrGuard LockTemp = m_TempMap.GetWrGuard();
		
		m_PermMap.Empty();
		}

	DeleteTemporaries();
	}

void CMapManager::DeleteTemporaries(void)
{
	CWrGuard Lock = m_TempMap.GetWrGuard();

	INDEX    nPos = m_TempMap.GetHead();

	UINT     uPos = 0;

	CArray <CObject *> List;

	while( !m_TempMap.Failed(nPos) ) {

		List.Append(m_TempMap.GetData(nPos));

		m_TempMap.GetNext(nPos);
		}

	while( uPos < List.GetCount() ) {

		delete List[uPos];

		uPos++;
		}

	m_TempMap.Empty();
	}

void CMapManager::Lock(void)
{
	InterlockedIncrement(&m_uLock);
	}

BOOL CMapManager::Unlock(void)
{
        AfxAssert(m_uLock);
        
        if( !InterlockedDecrement(&m_uLock) ) {

		DeleteTemporaries();
		
		return TRUE;
		}

	return FALSE;
	}

// End of File
