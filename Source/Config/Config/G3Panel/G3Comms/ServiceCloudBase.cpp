
#include "Intern.hpp"

#include "ServiceCloudBase.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CloudDeviceDataSet.hpp"
#include "CloudTagSet.hpp"
#include "MqttClientOptions.hpp"
#include "CommsSystem.hpp"

#include <G3DevCon.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Basic MQTT Cloud Client Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudBase, CServiceItem);

// Constructor

CServiceCloudBase::CServiceCloudBase(void)
{
	m_bServCode = 0;

	m_pEnable   = NULL;

	m_Service   = 0;

	m_Mode      = 0;

	m_Reconn    = 0;

	m_Buffer    = 0;

	m_Sets      = 4;

	m_pIdent    = NULL;

	m_pStatus   = NULL;

	m_pDev      = New CCloudDeviceDataSet;

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		m_pSet[n] = New CCloudTagSet;
	}

	m_pSet[0]->m_Mode = 1;

	m_pOpts	    = New COpts;

	m_OldSets   = m_Sets;

	m_Drive     = 0;
}

// UI Management

CViewWnd * CServiceCloudBase::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		return CreateItemView(FALSE);
	}

	return NULL;
}

// UI Loading

BOOL CServiceCloudBase::OnLoadPages(CUIPageList *pList)
{
	BOOL fSave = (m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B);

	UINT uPage = fSave ? 1 : 2;

	pList->Append(New CUIStdPage(CString(IDS_SERVICE), AfxThisClass(), uPage));

	pList->Append(New CUIStdPage(CString(IDS_NETWORK), AfxThisClass(), 3));

	pList->Append(New CUIStdPage(CString(IDS_DEVICE_DATA), AfxThisClass(), 4));

	for( UINT n = 0; n < m_Sets; n++ ) {

		CPrintf Name(CString(IDS_TAG_SET_FMT), 1 + n);

		UINT        uPage = fSave ? 5 : 6;

		CUIStdPage *pPage = New CUIStdPage(Name, AfxThisClass(), uPage);

		pPage->SetSubstitute(L"set1", CPrintf("set%u", 1 + n));

		pList->Append(pPage);
	}

	return TRUE;
}

// UI Update

void CServiceCloudBase::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

			if( fEnable ) {

				pHost->EnableUI(m_pOpts, TRUE);

				pHost->EnableUI(m_pDev, TRUE);

				for( UINT n = 0; n < m_Sets; n++ ) {

					pHost->EnableUI(m_pSet[n], TRUE);
				}

				m_pOpts->OnUIChange(pHost, m_pOpts, L"");

				m_pDev->OnUIChange(pHost, m_pDev, L"");

				for( UINT n = 0; n < m_Sets; n++ ) {

					m_pSet[n]->OnUIChange(pHost, m_pSet[n], L"");
				}
			}
			else {
				pHost->EnableUI(m_pOpts, FALSE);

				pHost->EnableUI(m_pDev, FALSE);

				for( UINT n = 0; n < m_Sets; n++ ) {

					pHost->EnableUI(m_pSet[n], FALSE);
				}
			}

			DoEnables(pHost);
		}

		if( Tag == "Sets" ) {

			if( pHost->HasWindow() && m_Sets != m_OldSets ) {

				if( pHost->KillUndoList() ) {

					if( m_OldSets < m_Sets ) {

						m_OldSets = m_Sets;
					}
					else {
						while( m_OldSets > m_Sets ) {

							CCloudTagSet * &pSet = m_pSet[--m_OldSets];

							pSet->Kill();

							delete pSet;

							pSet = New CCloudTagSet;

							pSet->SetParent(this);

							pSet->Init();
						}
					}

					afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);

					m_OldSets = m_Sets;
				}
				else {
					m_Sets = m_OldSets;

					pHost->UpdateUI(Tag);
				}
			}
		}

		if( Tag == "Buffer" ) {

			DoEnables(pHost);
		}
	}

	if( pItem == m_pOpts ) {

		if( Tag == "Tls" ) {

			DoEnables(pHost);
		}
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Initial Values

void CServiceCloudBase::SetInitValues(void)
{
	SetInitial(L"Enable", m_pEnable, 0);
}

// Type Access

BOOL CServiceCloudBase::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Ident" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == "Status" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Attributes

BOOL CServiceCloudBase::IsEnabled(void) const
{
	UINT uGroup = m_pDbase->GetSoftwareGroup();

	if( uGroup == SW_GROUP_2 || uGroup >= SW_GROUP_3B ) {

		return TRUE;
	}

	return FALSE;
}

UINT CServiceCloudBase::GetTreeImage(void) const
{
	return IDI_CLOUD_ENABLED;
}

// Persistance

BOOL CServiceCloudBase::SaveProp(CString const &Tag) const
{
	if( Tag.StartsWith(L"set") ) {

		UINT n = watoi(Tag.Mid(3));

		if( n > m_Sets ) {

			return FALSE;
		}
	}

	return CServiceItem::SaveProp(Tag);
}

// Conversion

void CServiceCloudBase::PostConvert(void)
{
}

// Download Support

BOOL CServiceCloudBase::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(m_bServCode);

	CServiceItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddByte(BYTE(m_Service));

	Init.AddItem(itemVirtual, m_pStatus);

	Init.AddItem(itemVirtual, m_pIdent);

	Init.AddByte(BYTE(m_Sets));

	for( UINT n = 0; n < m_Sets; n++ ) {

		Init.AddItem(itemSimple, m_pSet[n]);
	}

	Init.AddItem(itemSimple, m_pDev);

	Init.AddItem(itemSimple, m_pOpts);

	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		Init.AddByte(BYTE(m_Buffer));
	}
	else {
		Init.AddByte(BYTE(0));
	}

	Init.AddByte(BYTE(m_Mode));
	Init.AddByte(BYTE(m_Reconn));
	Init.AddByte(BYTE(m_Drive));

	return TRUE;
}

// Meta Data Creation

void CServiceCloudBase::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddInteger(Service);
	Meta_AddInteger(Sets);
	Meta_AddInteger(Mode);
	Meta_AddInteger(Reconn);
	Meta_AddInteger(Buffer);
	Meta_AddVirtual(Ident);
	Meta_AddVirtual(Status);
	Meta_AddObject(Dev);

	for( UINT n = 0; n < elements(m_pSet); n++ ) {

		CPrintf Name(L"Set%u", 1 + n);

		AddMeta(Name, metaObject, PBYTE(&m_pSet[n]) - PBYTE(this));
	}

	Meta_AddObject(Opts);
	Meta_AddInteger(Drive);
}

// Implementation

void CServiceCloudBase::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Sets", fEnable);

	pHost->EnableUI(this, "Mode", fEnable);

	pHost->EnableUI(this, "Reconn", fEnable);

	pHost->EnableUI(this, "Buffer", fEnable);

	pHost->EnableUI(this, "Status", fEnable);

	pHost->EnableUI(this, "Ident", fEnable);

	pHost->EnableUI(this, "Drive", fEnable && m_Buffer == 2);
}

BOOL CServiceCloudBase::ImportCert(CString const &Base, CString const &CertFile, CString const &PrivFile)
{
	CByteArray Cert, Priv;

	if( LoadFile(Cert, CertFile) && LoadFile(Priv, PrivFile) ) {

		CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

		CDevCon      *pDevCon = pSystem->m_pDevCon;

		UINT uKey = pDevCon->ImportCert(Base, Cert, Priv);

		if( uKey ) {

			m_pOpts->m_IdSrc = uKey;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CServiceCloudBase::ImportCert(CString const &Base, CString const &AuthFile)
{
	CByteArray Cert;

	if( LoadFile(Cert, AuthFile) ) {

		CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

		CDevCon      *pDevCon = pSystem->m_pDevCon;

		UINT uKey = pDevCon->ImportCert(Base, Cert);

		if( uKey ) {

			m_pOpts->m_CaSrc = uKey;

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CServiceCloudBase::LoadFile(CByteArray &Data, CFilename const &File)
{
	HANDLE hFile;

	if( (hFile = File.OpenReadSeq()) != INVALID_HANDLE_VALUE ) {

		UINT  uSize = GetFileSize(hFile, NULL);

		DWORD uRead = 0;

		Data.SetCount(uSize);

		ReadFile(hFile, PBYTE(Data.GetPointer()), uSize, &uRead, NULL);

		if( uRead == uSize ) {

			CloseHandle(hFile);

			return TRUE;
		}

		CloseHandle(hFile);
	}

	Data.Empty();

	return FALSE;
}

// End of File
