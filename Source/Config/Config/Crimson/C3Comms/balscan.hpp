#include "ciasdo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BALSCAN_HPP
	
#define	INCLUDE_BALSCAN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Baldor CANOpen SDO Slave
//

class CCANOpenBaldorSDOSlave : public CCANOpenSlave
{
	public:
		// Constructor
		CCANOpenBaldorSDOSlave(void);
	};

// End of File

#endif
