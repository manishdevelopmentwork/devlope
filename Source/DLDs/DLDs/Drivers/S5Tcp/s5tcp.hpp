
#include "s5base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 TCPIP Master
//

#define	TIMEOUT		2000

#define	GAP_LENGTH	100 / PAUSE

#define	PAUSE		10

//////////////////////////////////////////////////////////////////////////
//
// Siemens S5 via AS511 TCPIP Master
//

class CS5AS511TCPMaster : public CS5AS511Base
{
	public:
		// Constructor
		CS5AS511TCPMaster(void);

		// Destructor
		~CS5AS511TCPMaster(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Context
		struct CContext : CBase
		{
			DWORD	  m_IP;
			UINT	  m_uPort;
			BOOL	  m_fKeep;
			BOOL	  m_fPing;
			UINT	  m_uTime1;
			UINT	  m_uTime2;
			ISocket * m_pSock;
			UINT	  m_uLast;
			};

		// Data
		CContext * m_pCtx;
		UINT	   m_uKeep;

		// Transport
		BOOL Send(PCBYTE pData, UINT uLen);
		BOOL Recv(PBYTE pData, UINT &uLen, BOOL fTerm);

		// Link Management
		BOOL CheckLink(void);
		void AbortLink(void);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File
