
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Thread Helper Object
//

// Runtime Class

AfxImplementRuntimeClass(CThreadHelper, CRawThread);

// Constructor

CThreadHelper::CThreadHelper(CThread *pThread)
{
	m_pThread = pThread;
	}

// Overridables

BOOL CThreadHelper::OnInit(void)
{
	return m_pThread->HelperInit();
	}

UINT CThreadHelper::OnExec(void)
{
	return m_pThread->HelperExec();
	}

void CThreadHelper::OnTerm(void)
{
	m_pThread->HelperTerm();
	}

// End of File
