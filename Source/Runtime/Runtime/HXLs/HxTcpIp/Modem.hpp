
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Modem_HPP

#define	INCLUDE_Modem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PppLayer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

class CHdlc;

//////////////////////////////////////////////////////////////////////////
//
// Modem Codes
//

enum {
	codeOK		 = 0,
	codeConnect      = 1,
	codeRing         = 2,
	codeNoCarrier    = 3,
	codeError        = 4,
	codeNoDialTone   = 6,
	codeBusy         = 7,
	codeNoAnswer     = 8,
	codeClient       = 12,
	codeServer	 = 13,
	codeExtended	 = 14,
	codePrompt	 = 15,
	codeEcho	 = 16,
	codeZero	 = 17,
	codeNone         = 99,
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Modem Object
//

class CModem : public CPppLayer
{
	public:
		// Constructor
		CModem(CPpp *Pppp, CConfigPpp const &Config);

		// Destructor
		virtual ~CModem(void);

		// Management
		void Open(void);
		void Close(void);
		void OnDropped(void);
		void OnTime(void);

		// Attributes
		BOOL  IsDirect (void) const;
		PCTXT GetStatus(void) const;

		// Operations
		void Bind(CHdlc *pHdlc);
		void Service(void);
		void StopTask(void);
		BOOL PutData(PCBYTE pData, UINT uSize);

		// Overridables
		virtual BOOL Init(void);
		virtual BOOL Kick(void);
		virtual BOOL Idle(void);
		virtual BOOL Poll(void);
		virtual BOOL Connect(BOOL &fLost);
		virtual BOOL HangUp(void);

	protected:
		// Reply Structure
		struct CReply
		{
			PCTXT m_pText;
			UINT  m_uCode;
			};

		// Reply Table
		static CReply const m_Reply[];

		// Data Members
		CConfigPpp     m_Config;
		BOOL	       m_fServer;
		BOOL	       m_fDirect;
		CHdlc        * m_pHdlc;
		IPortObject  * m_pPort;
		BOOL	       m_fExpand;
		IDataHandler * m_pData;
		UINT	       m_uReqSession;
		UINT	       m_uActSession;
		UINT	       m_uNewSession;
		UINT	       m_uSeqSession;
		UINT	       m_uTxCount;
		UINT	       m_uRxCount;
		BOOL	       m_fStopReq;
		UINT	       m_uRest;
		char	       m_sLine  [120];
		char	       m_sRest  [120];
		char	       m_sInit  [120];
		char	       m_sStatus[21];
		UINT	       m_uReply;

		// Session Management
		BOOL NewSession(BOOL &fOkay);
		void EndSession(void);
		BOOL EndConnect(void);

		// Modem Interface
		void ModemInit(void);
		BOOL BasicInit(void);
		BOOL ExtraInit(void);
		BOOL ModemList(PCTXT *pList);
		BOOL ModemList(PCTXT *pList, DWORD dwMask);
		UINT ModemCommand(PCTXT pText);
		UINT ModemCommand(PCTXT *pList, DWORD dwMask);
		BOOL IsModemReady(void);
		void ModemAttention(void);
		void BlindCommand(PCTXT pText);
		void BlindCommand(PCTXT pText, UINT uSleep);
		void WriteCommand(PCTXT pText);
		void DumpConfig(void);

		// Reply Parser
		UINT GetReply(UINT uTimeout);
		BOOL MatchReply(UINT uData, UINT uReply, UINT uPos);
		BOOL StepToNext(UINT uData, UINT uReply, UINT &uPos);
		
		// Serial Port Support
		BOOL OpenPort(void);
		void ClosePort(void);

		// Status
		void SetStatus(PCTXT pStatus);

		// Logging
		BOOL ModemLog(PCTXT p1, PCTXT p2);
	};

// End of File

#endif
