
#include "Intern.hpp"

#include "DispFormatSci.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedText.hpp"
#include "CommsSystem.hpp"
#include "DispFormatNumber.hpp"
#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scientific Display Format
//

// Dynamic Class

AfxImplementDynamicClass(CDispFormatSci, CDispFormat);

// Constructor

CDispFormatSci::CDispFormatSci(void)
{
	m_uType   = 2;

	m_fLimits = TRUE;

	m_After	  = 5;

	m_ManSign = 0;

	m_ExpSign = 1;

	m_pPrefix = NULL;

	m_pUnits  = NULL;

	m_pDynDP  = NULL;
	}

// UI Update

void CDispFormatSci::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
		}

	CDispFormat::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CDispFormatSci::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Prefix" || Tag == "Units" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "DynDP" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Formatting

CString CDispFormatSci::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		CString Text;

		if( m_pPrefix ) {

			Text += m_pPrefix->GetText();
			}

		if( m_pUnits ) {

			Text += m_pUnits->GetText();
			}

		return Text;
		}

	if( Type == typeInteger ) {

		C3REAL r = C3REAL(INT(Data));

		return Format(R2I(r), typeReal, Flags);
		}

	if( Type == typeReal ) {

		C3REAL r = I2R(Data);

		if( _isnan(r) ) {

			return L"NAN";
			}

		if( !_finite(r) ) {

			if( r > 0 ) {

				return L"+INF";
				}

			return L"-INF";
			}

		double  e = r ? floor(log10(fabs(r)) + 0.5) : 0;

		double  m = r / pow(10, e);

		UINT    a = GetAfter();

		CString Text;

		// LATER -- Can we use some special characters for the plus
		// and minus to ensure that they are the same size as a digit
		// when using a variable pitch font?

		if( m < 0 ) {

			Text += '-';

			m = fabs(m);
			}
		else {
			if( m_ManSign ) {

				Text += '+';
				}
			else {
				if( Flags & fmtPad ) {

					Text += spaceFigure;
					}
				}
			}

		if( m ) {

			m += 0.5 / pow(double(10), int(1 + a));
			}

		if( m && m < 1 ) {

			m *= 10;

			e -= 1;
			}

		char s[64];

		gcvt(m, 1 + a, s);

		UINT n = strlen(s);

		if( a ) {

			if( n == 1 ) {

				s[n++] = '.';
				}

			while( n < 2 + a ) {

				s[n++] = '0';
				}
			}
		else {
			if( s[n-1] == '.' ) {

				s[--n] = 0;
				}
			}

		s[n++] = 'E';

		if( e < 0 ) {

			s[n++] = '-';

			e = fabs(e);
			}
		else {
			if( m_ExpSign ) {

				s[n++] = '+';
				}
			}

		sprintf(s + n, "%u", INT(e));

		if( GetAfter() ) {

			Text += s[0];

			Text += GetPointChar();

			Text += s+2;
			}
		else
			Text += s;

		if( Flags & fmtPad ) {

			MakeDigitsFixed(Text);
			}

		if( !(Flags & fmtBare) ) {

			if( m_pPrefix ) {

				Text = m_pPrefix->GetText() + Text;
				}

			if( m_pUnits ) {

				Text = Text + m_pUnits->GetText();
				}
			}

		return Text;
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Limit Access

DWORD CDispFormatSci::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return DWORD(INT_MIN);
		}

	if( Type == typeReal ) {

		return R2I(-FLT_MAX);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatSci::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return DWORD(INT_MAX);
		}

	if( Type == typeReal ) {

		return R2I(+FLT_MAX);
		}

	return CDispFormat::GetMax(Type);
	}

// Property Preservation

BOOL CDispFormatSci::Preserve(CDispFormat *pOld)
{
	if( pOld->IsKindOf(AfxRuntimeClass(CDispFormatNumber)) ) {

		CDispFormatNumber *pNumber = (CDispFormatNumber *) pOld;

		m_pPrefix = (CCodedText *) CItem::MakeFromItem(this, pNumber->m_pPrefix);

		m_pUnits  = (CCodedText *) CItem::MakeFromItem(this, pNumber->m_pUnits);

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CDispFormatSci::MakeInitData(CInitData &Init)
{
	CDispFormat::MakeInitData(Init);

	Init.AddByte(BYTE(m_After));
	Init.AddByte(BYTE(m_ManSign));
	Init.AddByte(BYTE(m_ExpSign));

	Init.AddItem(itemVirtual, m_pPrefix);

	Init.AddItem(itemVirtual, m_pUnits);

	Init.AddItem(itemVirtual, m_pDynDP);

	return TRUE;
	}

// Meta Data Creation

void CDispFormatSci::AddMetaData(void)
{
	CDispFormat::AddMetaData();

	Meta_AddInteger(After);
	Meta_AddInteger(ManSign);
	Meta_AddInteger(ExpSign);
	Meta_AddVirtual(Prefix);
	Meta_AddVirtual(Units);
	Meta_AddVirtual(DynDP);

	Meta_SetName((IDS_SCIENTIFIC_FORMAT));
	}

// Implementation

void CDispFormatSci::DoEnables(IUIHost *pHost)
{
	}

TCHAR CDispFormatSci::GetPointChar(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	return pLang->GetDecPointChar();
	}

UINT CDispFormatSci::GetAfter(void)
{
	if( m_pDynDP ) {

		UINT uAfter = m_pDynDP->ExecVal();

		return max(0, min(m_After, uAfter));
		}

	return m_After;
	}

// End of File
