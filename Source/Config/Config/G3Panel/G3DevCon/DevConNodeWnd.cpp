
#include "Intern.hpp"

#include "DevConNodeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConNode.hpp"

#include "DevConTabWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Node View Window
//

// Base Class

#define CBaseClass CMultiViewWnd

// Runtime Class

AfxImplementDynamicClass(CDevConNodeWnd, CBaseClass);

// Constructor

CDevConNodeWnd::CDevConNodeWnd(void)
{
	m_fRecycle = FALSE;

	m_nMargin  = 4;
}

// Destructor

CDevConNodeWnd::~CDevConNodeWnd(void)
{
}

// Overridables

void CDevConNodeWnd::OnAttach(void)
{
	CBaseClass::OnAttach();

	m_pItem = (CDevConNode *) CBaseClass::m_pItem;

	AttachViews();
}

CString CDevConNodeWnd::OnGetNavPos(void)
{
	CString Item = m_Views[m_uFocus]->GetNavPos();

	CString Left = Item.StripToken(':');

	CString Page = CPrintf(L"%u", m_uFocus);

	CString Nav  = Left + L":" + Page + L":" + Item;

	return Nav;
}

BOOL CDevConNodeWnd::OnNavigate(CString const &Nav)
{
	UINT  uFind = Nav.Find(L'!');

	CString Src = Nav.Left(uFind);

	CString Loc = Nav.Mid(uFind+1);

	UINT uSep = Src.Count(':');

	if( uSep == 2 ) {

		CString Item = Src;

		CString Left = Item.StripToken(':');

		CString Page = Item.StripToken(':');

		CString Rest = Left + L":" + Item;

		if( !Page.IsEmpty() ) {

			UINT uPage = watoi(Page);

			if( uPage < m_Views.GetCount() ) {

				SelectFocus(uPage);

				if( m_Views[uPage]->Navigate(Rest) ) {

					if( !IsCurrent() ) {

						SetFocus();
					}

					return TRUE;
				}
			}
		}

		return FALSE;
	}

	if( uSep == 1 ) {

		for( UINT uPage = 0; uPage < m_Views.GetCount(); uPage++ ) {

			CreateView(uPage);

			if( m_Views[uPage]->Navigate(Nav) ) {

				SelectFocus(uPage);

				if( !IsCurrent() ) {

					SetFocus();
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

void CDevConNodeWnd::OnExec(CCmd *pCmd)
{
	m_Views[m_uFocus]->ExecCmd(pCmd);
}

void CDevConNodeWnd::OnUndo(CCmd *pCmd)
{
	m_Views[m_uFocus]->UndoCmd(pCmd);
}

// Routing Control

BOOL CDevConNodeWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	#if defined(_DEBUG)

	if( Message.message == WM_AFX_COMMAND ) {

		if( Message.wParam == IDM_VIEW_REFRESH ) {

			return FALSE;
		}
	}

	#endif

	return CBaseClass::OnRouteMessage(Message, lResult);
}

// Message Map

AfxMessageMap(CDevConNodeWnd, CBaseClass)
{
	AfxDispatchMessage(WM_POSTCREATE)

		AfxMessageEnd(CDevConNodeWnd)
};

// Message Handlers

void CDevConNodeWnd::OnPostCreate(void)
{
	SetTabStyle(0);

	MakeTabs();

	m_DropHelp.Bind(m_hWnd, this);

	CBaseClass::OnPostCreate();

	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {

		CreateView(n);
	}
}

// Implementation

void CDevConNodeWnd::MakeTabs(void)
{
	CJsonData *pTabs = m_pItem->m_pNode->GetChild(L"tabs");

	UINT c = pTabs->GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CJsonData *pTab = pTabs->GetChild(n);

		if( pTab ) {

			if( pTab->GetValue(L"icon").IsEmpty() ) {

				CString   Label = pTab->GetValue(L"label");

				CViewWnd *pView = New CDevConTabWnd(m_pItem->m_pData, m_pItem->m_pSchema, m_pItem->m_pPerson, m_pItem->m_Path, pTab);

				AddView(Label, pView);
			}
		}
	}

	AttachViews();
}

void CDevConNodeWnd::AttachViews(void)
{
	for( UINT n = 0; n < m_Views.GetCount(); n++ ) {

		CViewWnd *pWnd = m_Views[n];

		pWnd->Attach(m_pItem);
	}
}

// End of File
