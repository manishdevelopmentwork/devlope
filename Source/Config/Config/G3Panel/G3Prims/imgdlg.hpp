
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMGDLG_HPP
	
#define	INCLUDE_IMGDLG_HPP

//////////////////////////////////////////////////////////////////////////
//
// Integer Comparison
//

#define	intcmp(a,b)	(((a)==(b))?0:(((a)<(b))?-1:+1))

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CImageManagerDialog;
class CImageSelectItem;

//////////////////////////////////////////////////////////////////////////
//
// Image Management Dialog
//

class CImageManagerDialog : public CStdToolbarDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CImageManagerDialog(CImageManager *pImages);

		// Destructor
		~CImageManagerDialog(void);

	public:
		// Data Members
		CImageManager  * m_pImages;
		CImageFileList * m_pList;
		CListView      * m_pView;
		CImageFileItem * m_pFile;
		UINT	         m_uFile;
		BOOL		 m_fRead;
		UINT	         m_uCount;
		UINT		 m_uMiss;
		UINT	         m_uUsed;
		UINT             m_uSort;
		BOOL	         m_fFlip;
		BOOL	         m_fWarn;
		CRect	         m_Preview;
		CMemoryDC      * m_pWorkDC;
		CBitmap        * m_pBitmap;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnUpdateUI(void);
		void OnPaint(void);

		// Command Handlers
		BOOL OnToolGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnToolControl(UINT uID, CCmdSource &Src);
		BOOL OnToolCommand(UINT uID);
		BOOL OnCancel(UINT uID);
		BOOL OnExport(void);
		BOOL OnReplace(void);
		BOOL OnExportAll(void);
		BOOL OnPurge(void);

		// Notification Handlers
		void OnItemChanged(UINT uID, NMLISTVIEW &Info);
		void OnColumnClick(UINT uID, NMLISTVIEW &Info);

		// Sorting
		int SortList(LPARAM p1, LPARAM p2);
		int SortData(CImageFileItem *f1, CImageFileItem *f2, PCSTR pList);
		int SortData(CImageFileItem *f1, CImageFileItem *f2, char cKey);

		// Sort Function
		static int __stdcall SortStatic(LPARAM p1, LPARAM p2, LPARAM ps);

		// Implementation
		void MakeRepl(void);
		void MakeList(void);
		void LoadList(void);
		void ShowList(void);
		void UpdateUsed(void);
		void DoEnables(void);
		BOOL Warn(void);
		void FindPreviewData(void);
		void ShowPreview(void);
		void FreePreviewData(void);
		BOOL DoExport(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Image Selection Item
//

class CImageSelectItem : public CUIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CImageSelectItem(void);

		// Data Members
		CPrimImage * m_pImage;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
