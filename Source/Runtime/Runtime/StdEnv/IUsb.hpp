
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IUsb_HPP

#define INCLUDE_IUsb_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 21 -- Usb Interfaces
//

// https://

interface IUsbDriver;
interface IUsbEvents;
interface IUsbDevice;
interface IUsbPipe;
interface IUsbHostStack;
interface IUsbSystem;
interface IUsbHostFuncDriver;
interface IUsbHostFuncEvents;
interface IUsbHubEvents;
interface IUsbHubDriver;

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes 
//

class  CUsbDescList;
struct MACADDR;
struct NICDIAG;
struct CdcNtbParam;

//////////////////////////////////////////////////////////////////////////
//
// Framework Device States 
//

enum
{
	devDetached		=   0,
	devAttached		=   1,
	devPowered		=   2,
	devDefault		=   3,
	devAddress		=   4,
	devConfigured		=   5,
	devSuspended		=   6,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Request Types 
//

enum 
{
	reqGetStatus		=   0,
	reqClearFeature		=   1,
	reqSetFeature		=   3,
	reqSetAddress		=   5,
	reqGetDesc		=   6,
	reqSetDesc		=   7,
	reqGetConfig		=   8,
	reqSetConfig		=   9,
	reqGetInterface		=  10,
	reqSetInterface		=  11,
	reqSynchFrame		=  12,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Descriptor Types 
//

enum
{
	descDevice		=   1,
	descConfig		=   2,
	descString		=   3,
	descInterface		=   4,
	descEndpoint		=   5,
	descDeviceQual		=   6,
	descOtherSpeed		=   7,
	descInterfacePwr	=   8,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Device Class Codes 
//

enum
{
	devReserved		=   0,
	devAudio		=   1,
	devComms		=   2,
	devHid			=   3,
	devMonitor		=   4,
	devPhysical		=   5,
	devPower		=   6,
	devPrinter		=   7,
	devStorage		=   8,
	devHub			=   9,
	devMisc			= 239,
	devVendor		= 255,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Endpoint Types
//

enum
{
	eptControl		=   0,
	eptIsoch		=   1,
	eptBulk			=   2,
	eptInterrupt		=   3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Device Request Types
//

enum
{
	reqStandard		=   0,
	reqClass		=   1,
	reqVendor		=   2,
	reqOther		=   3
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Request Recipient
//

enum
{
	recDevice		=   0,
	recInterface		=   1,
	recEndpoint		=   2,
	recOther		=   3
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Request Direction
//

enum
{
	dirHostToDev		=   0,
	dirDevToHost		=   1,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Feature
//

enum
{
	selEndpointHalt		=   0,
	selDeviceWakeup		=   1,
	selTestMode		=   2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Interfaces Types
//

enum 
{
	usbEhci			=   0,
	usbUhci			=   1,
	usbOhci			=   2,
	usbXhci			=   3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Speed 
//

enum 
{
	usbSpeedLow		=   1,
	usbSpeedFull		=   0,
	usbSpeedHigh		=   2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Driver State 
//

enum 
{
	usbInit			=   0, 
	usbOpen			=   1, 
	usbClosed		=   2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Memory Buffer Type
//

enum 
{
	usbMemTransferDesc	=   0,
	usbMemQueueHeadDesc	=   1,
	usbMemPeriodicList	=   2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Device Request
//

#pragma pack(1)

struct UsbDeviceReq
{
	#ifdef AEON_LITTLE_ENDIAN
		
	BYTE	m_Recipient	:   5;
	BYTE	m_Type		:   2;
	BYTE	m_Direction	:   1;

	#else
			
	BYTE	m_Direction	:   1;
	BYTE	m_Type		:   2;
	BYTE	m_Recipient	:   5;

	#endif

	BYTE	m_bRequest;
	WORD	m_wValue;
	WORD	m_wIndex;
	WORD	m_wLength;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Base Descriptor
//

struct UsbDesc
{
	BYTE	m_bLength;
	BYTE	m_bType;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Device Descriptor
//

struct UsbDeviceDesc : UsbDesc
{
	WORD	m_wVersion;
	BYTE	m_bClass;
	BYTE	m_bSubClass;
	BYTE	m_bProtocol;
	BYTE	m_bMaxPacket;
	WORD	m_wVendorID;
	WORD	m_wProductID;
	WORD	m_wDeviceVer;
	BYTE	m_bVendorIdx;
	BYTE	m_bProductIdx;
	BYTE	m_bSerialNumIdx;
	BYTE	m_bConfigCount;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Device Qualifier
//

struct UsbDeviceQual : UsbDesc
{
	WORD	m_wVersion;
	BYTE	m_bClass;
	BYTE	m_bSubClass;
	BYTE	m_bProtocol;
	BYTE	m_bMaxPacket;
	BYTE	m_bConfigCount;
	BYTE	m_bReserved;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Configuration Descriptor
//

struct UsbConfigDesc : UsbDesc
{
	WORD	m_wTotal;
	BYTE	m_bInterfaces;
	BYTE	m_bConfig;
	BYTE	m_bIndex;

	#ifdef AEON_LITTLE_ENDIAN
		
	BYTE	m_bReserved0	:   5;
	BYTE	m_bWakeup	:   1;
	BYTE	m_bSelfPowered	:   1;
	BYTE	m_bReserved1	:   1;

	#else

	BYTE	m_bReserved1	:   1;
	BYTE	m_bSelfPowered	:   1;
	BYTE	m_bWakeup	:   1;
	BYTE	m_bReserved0	:   5;

	#endif
			
	BYTE	m_bMaxPower;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Interface Descriptor
//

struct UsbInterfaceDesc : UsbDesc
{
	BYTE	m_bThis;
	BYTE	m_bAltSetting;
	BYTE	m_bEndpoints;
	BYTE	m_bClass;
	BYTE	m_bSubClass;
	BYTE	m_bProtocol;
	BYTE	m_bNameIdx;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Endpoint Descriptor
//

struct UsbEndpointDesc : UsbDesc
{
	#ifdef AEON_LITTLE_ENDIAN
		
	BYTE	m_bAddr		:   4;
	BYTE	m_bReserved0	:   3;
	BYTE	m_bDirIn	:   1;
	BYTE	m_bTransfer	:   2;
	BYTE	m_bSynch	:   2;
	BYTE	m_bUsage	:   2;
	BYTE	m_bReserved1	:   2;

	#else

	BYTE	m_bDirIn	:   1;
	BYTE	m_bReserved0	:   3;
	BYTE	m_bAddr		:   4;
	BYTE	m_bReserved1	:   2;
	BYTE	m_bUsage	:   2;
	BYTE	m_bSynch	:   2;
	BYTE	m_bTransfer	:   2;

	#endif

	WORD	m_wMaxPacket;
	BYTE	m_bInterval;
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework String Descriptor
//

struct UsbStringDesc : UsbDesc
{
	WORD	m_wString[32];
	};

//////////////////////////////////////////////////////////////////////////
//
// Framework Hub Descriptor
//

struct UsbHubDesc : UsbDesc
{
	BYTE	m_bPorts;

	#ifdef AEON_LITTLE_ENDIAN
		
	WORD 	m_wPowerSwitch	:   2;
	WORD	m_wCompound	:   1;
	WORD	m_wProctect	:   2;
	WORD	m_wThinkTime	:   2;
	WORD	m_wPortLeds	:   1;
	WORD			:   8;

	#else

	WORD			:   8;
	WORD	m_wPortLeds	:   1;
	WORD	m_wThinkTime	:   2;
	WORD	m_wProctect	:   2;
	WORD	m_wCompound	:   1;
	WORD 	m_wPowerSwitch	:   2;

	#endif
			
	BYTE	m_bPwrOn2PwrGood;
	BYTE	m_bCurrent;
	BYTE    m_bRemovable[1];

	};

//////////////////////////////////////////////////////////////////////////
//
// Can Frame
//

struct UsbCanFrame 
{
	BYTE	m_bCount;
	BYTE	m_bFlags;
	DWORD	m_dwIdent;
	BYTE	m_bData[8];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// IO Request
//

struct UsbIor
{
	// Status
	enum 
	{	
		statInit	  = 0,
		statAlloc	  = 1,
		statActive	  = 2,
		statPassed	  = 3,
		statFailed	  = 4,
		statHalted	  = 5,
		statFatal	  = 6,
		statStall	  = 7,
		statAbort	  = 8,
		};

	// Flags
	enum
	{
		flagNotify	  = Bit(0),
		flagSetup	  = Bit(1),
		flagOut		  = Bit(2),
		flagIn		  = Bit(3), 
		flagQueue	  = Bit(4),
		flagData0	  = Bit(5),
		flagData1	  = Bit(6),
		flagShortOk	  = Bit(7),
		flagAsync	  = Bit(8),
		flagCompletion	  = Bit(9),
		flagControl	  = Bit(10),
		flagNoDataStage	  = Bit(11),
		flagInDataStage	  = Bit(12),
		flagOutDataStage  = Bit(13),
		flagStatusStage	  = Bit(14),
		flagCommand	  = Bit(15),
		};

	// Data
	DWORD		  m_iEndpt;
	UINT		  m_uFlags;
	PVBYTE 		  m_pData;
	UINT volatile	  m_uCount;
	UINT volatile	  m_uStatus;
	void *            m_pParam;
	UINT		  m_uParam;
	IEvent		* m_pDone;
	};	

//////////////////////////////////////////////////////////////////////////
//
// Routing
//

union UsbPortPath
{
	struct Path
	{
		DWORD	dwPort      : 4;
		DWORD	dwHost	    : 4;
		DWORD	dwCtrl	    : 4;
		DWORD   dwHubPort   : 4;
		DWORD   dwHubAddr   : 8;
		DWORD               : 8;
		
		} a;

	DWORD	dw;
	};

union UsbDevPath
{
	struct Path
	{
		DWORD	dwAddr      : 8;
		DWORD	dwHost	    : 4;
		DWORD	dwCtrl	    : 4;
		DWORD   dwInterface : 4;
		DWORD   dwPipe      : 4;
		DWORD	dwDirIn	    : 1;
		DWORD               : 7;
		
		} a;

	DWORD	dw;
	};

union UsbTreePath
{
	struct Path
	{
		DWORD	dwCtrl	    : 3;
		DWORD	dwHost	    : 2;
		DWORD   dwTier      : 3;
		DWORD	dwPort1	    : 4;
		DWORD	dwPort2	    : 4;
		DWORD	dwPort3	    : 4;
		DWORD	dwPort4	    : 4;
		DWORD	dwPort5	    : 4;
		DWORD	dwPort6	    : 4;
		
		} a;

	DWORD	dw;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub External Map
//

struct UsbHubLedMap
{
	UINT	m_uLed1[8];
	UINT	m_uLed2[8];
	};

//////////////////////////////////////////////////////////////////////////
//
// Driver
//

interface IUsbDriver : public IUnknown
{
	AfxDeclareIID(21, 1);

	virtual BOOL METHOD Bind(IUsbEvents *pEvents)		     = 0;
	virtual BOOL METHOD Bind(IUsbDriver *pDriver)		     = 0;
	virtual BOOL METHOD Init(void)				     = 0;
	virtual BOOL METHOD Start(void)				     = 0;
	virtual	BOOL METHOD Stop(void)				     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Events
//

interface IUsbEvents : public IUnknown
{
	AfxDeclareIID(21, 2);

	virtual void METHOD OnBind(IUsbDriver *pDriver)		     = 0;
	virtual void METHOD OnInit(void)			     = 0;
	virtual void METHOD OnStart(void)			     = 0;
	virtual void METHOD OnStop(void)			     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Hardware Driver
//

interface IUsbFuncHardwareDriver : public IUsbDriver
{
	AfxDeclareIID(21, 3);

	virtual BOOL METHOD GetHiSpeedCapable(void)		       = 0;
	virtual BOOL METHOD GetHiSpeedActual(void)		       = 0;
	virtual BOOL METHOD SetConfig(UINT uConfig)		       = 0;
	virtual BOOL METHOD SetAddress(UINT uAddr)		       = 0;
	virtual UINT METHOD GetAddress(void)			       = 0;
	virtual BOOL METHOD InitBulk(UINT iEndpt, BOOL fIn, UINT uMax) = 0;
	virtual BOOL METHOD KillBulk(UINT iEndpt)		       = 0;
	virtual BOOL METHOD Transfer(UsbIor &Urb)		       = 0;
	virtual BOOL METHOD Abort(UsbIor &Urb)			       = 0;
	virtual BOOL METHOD SetStall(UINT iEndpt, BOOL fSet)	       = 0;
	virtual BOOL METHOD GetStall(UINT iEndpt)		       = 0;
	virtual BOOL METHOD ResetDataToggle(UINT iEndpt)	       = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Hardware Driver Events
//

interface IUsbFuncHardwareEvents : public IUsbEvents
{
	AfxDeclareIID(21, 4);

	virtual void METHOD OnConnect(void)	    = 0;
	virtual void METHOD OnDisconnect(void)	    = 0;
	virtual void METHOD OnReset(void)	    = 0;
	virtual void METHOD OnSuspend(void)	    = 0;
	virtual void METHOD OnResume(void)	    = 0;
	virtual	BOOL METHOD OnSetup(PCBYTE pData)   = 0;
	virtual void METHOD OnTransfer(UsbIor &Urb) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
//  Function Drvier
//

interface IUsbFuncDriver : public IUsbDriver
{
	AfxDeclareIID(21, 5);

	virtual UINT METHOD GetState(void)					      = 0;
	virtual UINT METHOD GetMaxPacket(UINT iEndpt)				      = 0;
	virtual BOOL METHOD SendCtrlStatus(UINT iEndpt)				      = 0;
	virtual BOOL METHOD RecvCtrlStatus(UINT iEndpt)				      = 0;
	virtual BOOL METHOD SendCtrl(UINT iEndpt, PCBYTE p, UINT uLen)		      = 0;
	virtual BOOL METHOD RecvCtrl(UINT iEndpt, PBYTE  p, UINT uLen)		      = 0;
	virtual UINT METHOD SendBulk(UINT iEndpt, PCBYTE p, UINT uLen, UINT uTimeout) = 0;
	virtual UINT METHOD RecvBulk(UINT iEndpt, PBYTE  p, UINT uLen, UINT uTimeout) = 0;
	virtual BOOL METHOD SetStall(UINT iEndpt, BOOL fStall)			      = 0;
	virtual BOOL METHOD GetStall(UINT iEndpt)				      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Driver Events
//

interface IUsbFuncEvents : public IUsbEvents
{
	AfxDeclareIID(21, 6);

	virtual void METHOD OnState(UINT uState)		  = 0;
	virtual BOOL METHOD OnSetupClass(UsbDeviceReq &r)	  = 0;
	virtual BOOL METHOD OnSetupVendor(UsbDeviceReq &r)	  = 0;
	virtual BOOL METHOD OnSetupOther(UsbDeviceReq &r)	  = 0;
	virtual BOOL METHOD OnGetDevice(UsbDesc &d)		  = 0;
	virtual BOOL METHOD OnGetQualifier(UsbDesc &d)		  = 0;
	virtual BOOL METHOD OnGetConfig(UsbDesc &d)		  = 0;
	virtual BOOL METHOD OnGetInterface(UsbDesc &d)		  = 0;
	virtual BOOL METHOD OnGetEndpoint(UsbDesc &d)		  = 0;
	virtual BOOL METHOD OnGetString(UsbDesc *&p, UINT iIndex) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Composite Driver
//

interface IUsbFuncCompDriver : public IUsbFuncDriver
{
	AfxDeclareIID(21, 7);

	virtual void METHOD SetLanguages(PWORD pwLangId, UINT uCount) = 0;
	virtual void METHOD SetVendor(WORD wId, PCTXT pName)	      = 0;
	virtual void METHOD SetProduct(WORD wId, PCTXT pName)	      = 0;
	virtual void METHOD SetSerialNumber(PCTXT pText)	      = 0;
	virtual UINT METHOD FindInterface(IUsbEvents *pDriver)	      = 0;
	virtual UINT METHOD GetFirsEndptAddr(UINT iInterface)	      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Stack
//

interface IUsbFuncStack : public IUnknown
{
	AfxDeclareIID(21, 8);

	virtual	void METHOD Bind(IUsbFuncHardwareDriver *pHardware) = 0;
	virtual void METHOD Bind(IUsbFuncEvents *pAppDriver)	    = 0;
	virtual void METHOD SetVendor(WORD wId, PCTXT pName)	    = 0;
	virtual void METHOD SetProduct(WORD wId, PCTXT pName)	    = 0;
	virtual void METHOD SetSerialNumber(PCTXT pText)	    = 0;
	virtual BOOL METHOD Start(void)				    = 0;
	virtual BOOL METHOD Stop(void)				    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Hardware Driver Interface
//

interface IUsbHostHardwareDriver : public IUsbDriver
{
	AfxDeclareIID(21, 9);

	virtual DWORD METHOD GetBaseAddr(void)			= 0;
	virtual UINT  METHOD GetMemAlign(UINT uType)		= 0;
	virtual void  METHOD MemCpy(PVOID d, PCVOID s, UINT n)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Hardware Events Interface
//

interface IUsbHostHardwareEvents : public IUsbEvents
{
	AfxDeclareIID(21, 10);

	virtual	void METHOD OnEvent(void) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Controller Interface Driver
//

interface IUsbHostInterfaceDriver : public IUsbDriver
{
	AfxDeclareIID(21, 11);

	virtual UINT  METHOD GetType(void)							  = 0;
	virtual UINT  METHOD GetIndex(void)							  = 0;
	virtual UINT  METHOD GetPortCount(void)							  = 0;
	virtual BOOL  METHOD GetPortConnect(UINT iPort)						  = 0;
	virtual BOOL  METHOD GetPortEnabled(UINT iPort)						  = 0;
	virtual BOOL  METHOD GetPortCurrent(UINT iPort)						  = 0;
	virtual BOOL  METHOD GetPortSuspend(UINT iPort)						  = 0;
	virtual BOOL  METHOD GetPortReset(UINT iPort)						  = 0;
	virtual BOOL  METHOD GetPortPower(UINT iPort)						  = 0;
	virtual UINT  METHOD GetPortSpeed(UINT iPort)						  = 0;
	virtual void  METHOD SetIndex(UINT iIndex)						  = 0;
	virtual void  METHOD SetPortEnable(UINT iPort, BOOL fSet)				  = 0;
	virtual void  METHOD SetPortSuspend(UINT iPort, BOOL fset)				  = 0;
	virtual void  METHOD SetPortResume(UINT iPort, BOOL fSet)				  = 0;
	virtual void  METHOD SetPortReset(UINT iPort, BOOL fSet)				  = 0;
	virtual void  METHOD SetPortPower(UINT iPort, BOOL fSet)				  = 0;
	virtual void  METHOD EnableEvents(BOOL fEnable)						  = 0;
	virtual BOOL  METHOD SetEndptDevAddr(DWORD iEndpt, BYTE bAddr)				  = 0;
	virtual UINT  METHOD GetEndptDevAddr(DWORD iEndpt)					  = 0;
	virtual BOOL  METHOD SetEndptAddr(DWORD iEndpt, BYTE bAddr)				  = 0;
	virtual BOOL  METHOD SetEndptMax(DWORD iEndpt, WORD wMaxPacket)				  = 0;
	virtual BOOL  METHOD SetEndptHub(DWORD iEndpt, BYTE bHub, BYTE bPort)			  = 0;
	virtual DWORD METHOD MakeDevice(IUsbDevice *pDevice)					  = 0;
	virtual void  METHOD KillDevice(DWORD iDev)						  = 0;
	virtual BOOL  METHOD CheckConfig(DWORD iDev)						  = 0; 
	virtual DWORD METHOD MakeEndptCtrl(DWORD iDev, UINT uSpeed)				  = 0;
	virtual DWORD METHOD MakeEndptAsync(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc) = 0;
	virtual DWORD METHOD MakeEndptInt(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)   = 0;
	virtual DWORD METHOD MakeEndptIso(DWORD iDev, UINT uSpeed, UsbEndpointDesc const &Desc)   = 0;
	virtual BOOL  METHOD ResetEndpt(DWORD iEndpt)						  = 0;
	virtual BOOL  METHOD KillEndpt(DWORD iEndpt)						  = 0;
	virtual BOOL  METHOD Transfer(UsbIor &Urb)						  = 0;
	virtual BOOL  METHOD AbortPending(DWORD iEndpt)						  = 0;
	virtual UINT  METHOD AllocAddress(void)							  = 0;
	virtual void  METHOD FreeAddress(UINT uAddr)						  = 0;
	virtual BOOL  METHOD LockDefAddr(BOOL fLock)						  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Controller Interface Driver Events
//

interface IUsbHostInterfaceEvents : public IUsbEvents
{
	AfxDeclareIID(21, 12);

	virtual void METHOD OnPortConnect(UsbPortPath &Path) = 0;
	virtual void METHOD OnPortRemoval(UsbPortPath &Path) = 0;
	virtual void METHOD OnPortCurrent(UsbPortPath &Path) = 0;
	virtual void METHOD OnPortEnable(UsbPortPath &Path)  = 0;
	virtual void METHOD OnPortReset(UsbPortPath &Path)   = 0;
	virtual void METHOD OnTransfer(UsbIor &Urb)	     = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Enhanced Controller Interface Driver
//

interface IUsbHostEnhancedDriver : public IUsbHostInterfaceDriver
{
	AfxDeclareIID(21, 13);

	virtual BOOL METHOD GetGlobalOwner(void)		= 0;
	virtual BOOL METHOD GetPortOwner(UINT iPort)		= 0;
	virtual void METHOD SetGlobalOwner(BOOL fSet)		= 0;
	virtual void METHOD SetPortOwner(UINT iPort, BOOL fSet)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Controller Driver
//

interface IUsbHostControllerDriver : public IUsbDriver
{
	AfxDeclareIID(21, 14);

	virtual BOOL                      METHOD Bind(IUsbHostStack *pStack)	= 0;
	virtual UINT                      METHOD GetIndex(void)			= 0;
	virtual void                      METHOD SetIndex(UINT iIndex)		= 0;
	virtual void                      METHOD Poll(UINT uLapsed)		= 0;
	virtual void                      METHOD EnableEvents (BOOL fEnable)	= 0;
	virtual BOOL                      METHOD ResetPort(UINT uPort)		= 0;
	virtual IUsbHostInterfaceDriver * METHOD GetInterface(UINT i)		= 0;
	virtual IUsbHostEnhancedDriver  * METHOD GetEnhanced(void)		= 0;
	virtual IUsbHostInterfaceDriver * METHOD GetCompanion(UINT i)		= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Controller Driver Events
//

interface IUsbHostControllerEvents : public IUsbEvents
{
	AfxDeclareIID(21, 15);

	virtual	void METHOD OnDeviceArrival(UsbPortPath &Path)		 = 0;
	virtual	void METHOD OnDeviceRemoval(UsbPortPath &Path)		 = 0;
	virtual void METHOD OnDriverStarted(IUsbHostFuncDriver *pDriver) = 0;
	virtual void METHOD OnDriverStopped(IUsbHostFuncDriver *pDriver) = 0;
	virtual void METHOD OnTransferDone(UsbIor  &Urb)		 = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Stack
//

interface IUsbHostStack : public IUnknown
{
	AfxDeclareIID(21, 16);

	virtual BOOL                       METHOD Init(void)								   = 0;
	virtual	BOOL                       METHOD Start(void)								   = 0;
	virtual BOOL                       METHOD Stop(void)								   = 0;
	virtual BOOL                       METHOD Attach(IUsbSystem *pSink)						   = 0;
	virtual IUsbHostControllerDriver * METHOD GetHostController(UINT iCtrl)						   = 0;
	virtual IUsbHostInterfaceDriver  * METHOD GetHostInterface(UINT iCtrl, UINT iHost)				   = 0;
	virtual IUsbDevice               * METHOD GetDevice(UsbPortPath const &Path)					   = 0;
	virtual IUsbDevice               * METHOD GetDevice(UsbDevPath const &Path)					   = 0;
	virtual IUsbDevice               * METHOD GetDevice(UsbTreePath const &Path)					   = 0;
	virtual IUsbHostFuncDriver       * METHOD GetDriver(UsbDevPath const &Path)					   = 0;
	virtual IUsbPipe		 * METHOD GetPipe(UsbDevPath const &Path)					   = 0;
	virtual UINT			   METHOD GetPortSpeed(UsbPortPath const &Path)					   = 0;
	virtual BOOL			   METHOD ResetPort(UsbPortPath const &Path)					   = 0;
	virtual BOOL			   METHOD ResetPort(UsbTreePath const &Path)					   = 0;
	virtual	BOOL                       METHOD SetAddress(IUsbPipe *pPipe, BYTE bAddr)				   = 0;
	virtual	BOOL                       METHOD ClearDevFeature(IUsbPipe *pPipe, WORD wSel)				   = 0;
	virtual	BOOL                       METHOD ClearIntFeature(IUsbPipe *pPipe, WORD wSel, WORD iInt)		   = 0;
	virtual	BOOL                       METHOD ClearEndFeature(IUsbPipe *pPipe, WORD wSel, WORD iEnd)		   = 0;
	virtual	BOOL                       METHOD SetDevFeature(IUsbPipe *pPipe, WORD wSel)		 		   = 0;
	virtual	BOOL                       METHOD SetIntFeature(IUsbPipe *pPipe, WORD wSel, WORD iInt)			   = 0;
	virtual	BOOL                       METHOD SetEndFeature(IUsbPipe *pPipe, WORD wSel, WORD iEnd)			   = 0;
	virtual	BOOL                       METHOD GetConfig(IUsbPipe *pPipe, BYTE &bConfig)				   = 0;
	virtual	BOOL                       METHOD SetConfig(IUsbPipe *pPipe, BYTE  bConfig)				   = 0;
	virtual BOOL                       METHOD GetInterface(IUsbPipe *pPipe, WORD wInt, BYTE &bAlt)			   = 0;
	virtual BOOL                       METHOD SetInterface(IUsbPipe *pPipe, WORD wInt, BYTE  bAlt)			   = 0;
	virtual	BOOL                       METHOD GetDevStatus(IUsbPipe *pPipe, WORD &wStat)				   = 0;
	virtual	BOOL                       METHOD GetIntStatus(IUsbPipe *pPipe, WORD iInt, WORD &wStat)			   = 0;
	virtual	BOOL                       METHOD GetEndStatus(IUsbPipe *pPipe, WORD iEnd, WORD &wStat)			   = 0;
	virtual	BOOL                       METHOD GetDevDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen)			   = 0;
	virtual	BOOL                       METHOD GetConfigDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen)		   = 0;
	virtual BOOL                       METHOD GetHubDesc(IUsbPipe *pPipe, PBYTE pData, UINT uLen)			   = 0;
	virtual	BOOL                       METHOD GetStringDesc(IUsbPipe *pPipe, UINT iIdx, PTXT pText, UINT uLen)	   = 0;
	virtual UsbIor			 * METHOD AllocUrb(void)							   = 0;
	virtual void			   METHOD FreeUrb(UsbIor *pUrb)							   = 0;
	virtual void			   METHOD FreeUrbs(IUsbPipe *pPipe)						   = 0;
	virtual BOOL			   METHOD Transfer(IUsbPipe *pPipe, UsbIor *pUrb)				   = 0;
	virtual BOOL		           METHOD GetDriverClassOverride(UINT uVendor, UINT uProduct)			   = 0;
	virtual BOOL			   METHOD GetDriverClassFilter(UINT uVendor, UINT uProduct)			   = 0;
	virtual IUsbHostFuncDriver       * METHOD CreateDriver(UINT uClass, UINT uSub, UINT uProt)			   = 0;
	virtual IUsbHostFuncDriver       * METHOD CreateDriver(UINT uVend, UINT uProd, UINT uClass, UINT uSub, UINT uProt) = 0; 
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Interface
//

interface IUsbDevice : public IUnknown
{
	AfxDeclareIID(21, 17);

	virtual	UsbDeviceDesc const      & METHOD GetDevDesc(void)				= 0;
	virtual	CUsbDescList  const      & METHOD GetCfgDesc(void)				= 0;
	virtual UsbHubDesc    const      & METHOD GetHubDesc(void)				= 0;
	virtual	UINT                       METHOD GetSpeed(void)				= 0;
	virtual	BYTE		           METHOD GetAddr(void)					= 0;
	virtual IUsbPipe		 * METHOD GetCtrlPipe(void)				= 0;
	virtual IUsbPipe		 * METHOD GetPipe(BYTE bAddr, BOOL fIn)			= 0;
	virtual void                       METHOD SetHostStack(IUsbHostStack *pStack)		= 0;
	virtual IUsbHostStack            * METHOD GetHostStack(void)				= 0;	
	virtual IUsbHostControllerDriver * METHOD GetHostC(void)				= 0;
	virtual IUsbHostInterfaceDriver  * METHOD GetHostI(void)				= 0;
	virtual IUsbHubDriver		 * METHOD GetParentHub(void)				= 0;
	virtual UINT			   METHOD GetDriverCount(void)				= 0;
	virtual IUsbHostFuncDriver       * METHOD GetDriver(UINT iInt)				= 0;
	virtual UsbPortPath const        & METHOD GetPortPath(void)				= 0;
	virtual UsbTreePath const	 & METHOD GetTreePath(void)				= 0;
	virtual void			   METHOD SetObject(UINT i, PVOID p)			= 0;
	virtual PVOID			   METHOD GetObject(UINT i)				= 0;
	virtual BOOL			   METHOD SetRemoveLock(BOOL fLock)			= 0;
	virtual BOOL			   METHOD HasError(void) const				= 0;
	virtual void			   METHOD SetError(void)				= 0;
	virtual UINT			   METHOD GetState(void) const				= 0;
	virtual PCTXT			   METHOD GetStateText(void) const			= 0;
	virtual BOOL			   METHOD IsHub(void) const				= 0;
	virtual BOOL			   METHOD IsReady(void) const				= 0;
	virtual BOOL			   METHOD IsRemoved(void) const				= 0;
	virtual	void			   METHOD SetPath(UsbPortPath const &Path)		= 0;
	virtual	void			   METHOD SetPath(UsbTreePath const &Path)		= 0;
	virtual	void			   METHOD SetHostC(IUsbHostControllerDriver *pHost)	= 0;
	virtual	void			   METHOD SetHostI(IUsbHostInterfaceDriver  *pHost)	= 0;
	virtual	void			   METHOD SetSpeed(UINT uSpeed)				= 0;
	virtual BOOL			   METHOD SetAltInterface(UINT iInt, UINT iAlt)		= 0;
	virtual BOOL		           METHOD GetString(UINT iStr, PTXT pStr, UINT uLen)    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Device Events Interface
//

interface IUsbDeviceEvents : public IUnknown
{
	AfxDeclareIID(21, 18);

	virtual	void METHOD OnArrival(void)	 = 0;
	virtual	void METHOD OnRemoval(void)	 = 0;
	virtual	void METHOD OnPoll(UINT uLapsed) = 0;
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Pipe 
//

interface IUsbPipe : public IUnknown
{
	AfxDeclareIID(21, 19);

	virtual void	     METHOD SetDevice(IUsbDevice *pDev)				= 0;
	virtual IUsbDevice * METHOD GetDevice(void)					= 0;
	virtual void	     METHOD SetEndpoint(DWORD iEndpoint)			= 0;
	virtual DWORD        METHOD GetEndpoint(void)					= 0;
	virtual void	     METHOD SetDirIn(BOOL fIn)					= 0;
	virtual BOOL	     METHOD GetDirIn(void)					= 0;
	virtual void	     METHOD SetEndpointAddr(BYTE bAddr)				= 0;
	virtual BYTE         METHOD GetEndpointAddr(void)				= 0;
	virtual BYTE	     METHOD GetFeatureAddr(void)				= 0;
	virtual void	     METHOD SetMaxPacket(WORD wMaxPacket)			= 0;
	virtual void         METHOD Claim(void)						= 0;
	virtual void         METHOD Free(void)						= 0;
	virtual BOOL         METHOD SendSetup(PBYTE pData, UINT uCount, UINT uFlags)	= 0;
	virtual BOOL         METHOD SendSetupData(PBYTE pData, UINT uCount)		= 0;
	virtual UINT         METHOD RecvSetupData(PBYTE pData, UINT uCount)		= 0;
	virtual BOOL         METHOD SendStatus(void)					= 0;
	virtual BOOL         METHOD RecvStatus(void)					= 0;
	virtual BOOL         METHOD SendBulk(PBYTE pData, UINT uCount, BOOL fAsync)	= 0;
	virtual UINT         METHOD RecvBulk(PBYTE pData, UINT uCount, BOOL fAsync)	= 0;
	virtual BOOL         METHOD WaitAsync(UINT uTimeout, BOOL &fOk, UINT &uCount)	= 0;
	virtual BOOL         METHOD KillAsync(void)					= 0;
	virtual BOOL         METHOD CtrlTrans(UsbDeviceReq &Req)			= 0;
	virtual UINT         METHOD CtrlTrans(UsbDeviceReq &Req, PBYTE pData, UINT n)	= 0;
	virtual BOOL         METHOD IsHalted(void)					= 0;
	virtual BOOL         METHOD ClearStall(void)					= 0;
	virtual void         METHOD RequestCompletion(IUsbHostFuncEvents *pSink)	= 0;
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Pipe Events 
//

interface IUsbPipeEvents : public IUnknown
{
	AfxDeclareIID(21, 20);

	virtual void METHOD OnTransfer(UsbIor &Urb) = 0;
	virtual void METHOD OnPoll(UINT uLapsed)    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Function Driver
//

interface IUsbHostFuncDriver : public IUnknown
{
	AfxDeclareIID(21, 21);

	virtual void METHOD Bind(IUsbDevice *pDev, UINT iInterface) = 0;
	virtual void METHOD Bind(IUsbHostFuncEvents *pEvent)	    = 0;
	virtual void METHOD GetDevice(IUsbDevice *&pDevice)	    = 0;
	virtual BOOL METHOD SetRemoveLock(BOOL fLock)		    = 0;
	virtual UINT METHOD GetVendor(void)			    = 0;
	virtual UINT METHOD GetProduct(void)			    = 0;
	virtual UINT METHOD GetClass(void)			    = 0;
	virtual UINT METHOD GetSubClass(void)			    = 0;
	virtual UINT METHOD GetProtocol(void)			    = 0;
	virtual UINT METHOD GetInterface(void)			    = 0;
	virtual BOOL METHOD GetActive(void)			    = 0;
	virtual BOOL METHOD Open(CUsbDescList const &Config)	    = 0;
	virtual BOOL METHOD Close(void)				    = 0;
	virtual void METHOD Poll(UINT uLapsed)			    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Function Driver Events
//

interface IUsbHostFuncEvents : public IUnknown
{
	AfxDeclareIID(21, 22);

	virtual void METHOD OnPoll(UINT uLapsed) = 0;
	virtual void METHOD OnData(void)	 = 0;
	virtual void METHOD OnTerm(void)	 = 0;		
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Simple Bulk Driver
//

interface IUsbHostBulkDriver : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 23);

	virtual BOOL METHOD SendBulk(PCBYTE pData, UINT uCount)			= 0;
	virtual UINT METHOD RecvBulk(PBYTE  pData, UINT uCount)			= 0;
	virtual BOOL METHOD SendBulk(PCBYTE pData, UINT uCount, BOOL fAsync)	= 0;
	virtual UINT METHOD RecvBulk(PBYTE  pData, UINT uCount, BOOL fAsync)	= 0;
	virtual	BOOL METHOD SendBulk(CBuffer *pBuff)				= 0;
	virtual	BOOL METHOD RecvBulk(CBuffer *&pBuff)				= 0;
	virtual UINT METHOD WaitAsyncSend(UINT uTimeout)			= 0;
	virtual UINT METHOD WaitAsyncRecv(UINT uTimeout)			= 0;
	virtual BOOL METHOD KillAsyncSend(void)					= 0;
	virtual BOOL METHOD KillAsyncRecv(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Function Driver
//

interface IUsbHubDriver : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 24);

	virtual BOOL METHOD Bind(IUsbHubEvents *pSink)				    = 0;
	virtual BOOL METHOD SetLedMap(UsbHubLedMap const &Map)			    = 0;
	virtual BOOL METHOD ResetPort(WORD wPort)				    = 0;
	virtual UINT METHOD GetSpeed(WORD wPort)				    = 0;
	virtual	BOOL METHOD GetDescriptor(PBYTE pData, UINT uLen)		    = 0;
	virtual	BOOL METHOD GetStatus(WORD &wStatus, WORD &wChange)		    = 0;
	virtual	BOOL METHOD GetPortStatus(WORD wPort, WORD &wStatus, WORD &wChange) = 0;
	virtual	BOOL METHOD SetFeature(WORD wFeature)				    = 0;
	virtual	BOOL METHOD ClrFeature(WORD wFeature)				    = 0;
	virtual	BOOL METHOD SetPortFeature(WORD wPort, WORD wFeature)		    = 0;
	virtual	BOOL METHOD ClrPortFeature(WORD wPort, WORD wFeature)		    = 0;
	virtual BOOL METHOD HasMultiTT(void)					    = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hub Function Driver Events
//

interface IUsbHubEvents : public IUnknown
{
	AfxDeclareIID(21, 25);

	virtual	void METHOD OnHubDeviceArrival(UsbPortPath &Path) = 0;
	virtual	void METHOD OnHubDeviceRemoval(UsbPortPath &Path) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Hid Interface
//

interface IUsbHostHid : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 26);

	virtual UINT METHOD GetUsagePage(void)						= 0;
	virtual UINT METHOD GetUsageType(void)						= 0;
	virtual BOOL METHOD GetReportData(PCBYTE &pData, UINT &uSize)			= 0;
	virtual BOOL METHOD GetReportDesc(PBYTE pData, UINT uSize)			= 0;
	virtual BOOL METHOD GetReport(WORD wType, WORD wId, PBYTE pData, UINT uSize)	= 0;
	virtual BOOL METHOD SetReport(WORD wType, WORD wId, PBYTE pData, UINT uSize)	= 0;
	virtual BOOL METHOD GetIdle(BYTE &bIdle)					= 0;
	virtual BOOL METHOD SetIdle(BYTE bIdle)						= 0;
	virtual BOOL METHOD GetProtocol(BYTE &bProtocol)				= 0;
	virtual BOOL METHOD SetProtocol(BYTE bProtocol)					= 0;
	virtual BOOL METHOD RecvReport(PBYTE pData, UINT uCount)			= 0;
	virtual UINT METHOD WaitReport(UINT uTimeout)					= 0;
	virtual void METHOD SetConfig(PCBYTE pConfig, UINT uSize)			= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Hid Mapper Interface
//

interface IUsbHidMapper : public IUnknown
{
	AfxDeclareIID(21, 27);

	virtual void METHOD Bind(IUsbHostHid *pHost)			= 0;
	virtual BOOL METHOD SetReport(PCBYTE pReport, UINT uSize)	= 0;
	virtual UINT METHOD GetUsagePage(void)				= 0;
	virtual UINT METHOD GetUsageType(void)				= 0;
	virtual UINT METHOD GetRecvSize(UINT uId)			= 0;
	virtual UINT METHOD GetSendSize(UINT uId)			= 0;
	virtual void METHOD SetConfig(PCBYTE pConfig, UINT uSize)	= 0;
	virtual void METHOD Poll(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// System Interface
//

interface IUsbSystem : public IUnknown
{
	AfxDeclareIID(21, 28);

	virtual void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver) = 0;
	virtual void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// System Port Types
//

enum
{
	typeUnknown,
	typeSystem,
	typeOption,
	typeExtern,
	};

//////////////////////////////////////////////////////////////////////////
//
// System Port Mapper
//

interface IUsbSystemPortMapper : public IUnknown
{
	AfxDeclareIID(21, 29);

	virtual UINT METHOD GetPortCount(void)		   = 0;
	virtual UINT METHOD GetPortType(UsbTreePath const &Path)  = 0;
	virtual UINT METHOD GetPortReset(UsbTreePath const &Path) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// System Power Budget
//

interface IUsbSystemPower : public IUnknown
{
	AfxDeclareIID(21, 30);

	virtual void METHOD OnNewDevice(UsbTreePath const &Path, UINT uPower)	= 0;
	virtual void METHOD OnDelDevice(UsbTreePath const &Path, UINT uPower)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Mass Storage Function Driver
//

interface IUsbHostMassStorage : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 31);

	virtual BOOL METHOD TestReady(void)					= 0;
	virtual BOOL METHOD ReadInquiry(void)					= 0;
	virtual BOOL METHOD RequestSense(BYTE &bSk, BYTE &bAsc, BYTE &bAscq)	= 0;
	virtual BOOL METHOD ReadCapacity(DWORD &dwBlocks, DWORD &dwLenh)	= 0;
	virtual BOOL METHOD Read(DWORD dwBlock, PBYTE pData)			= 0;
	virtual BOOL METHOD Write(DWORD dwBlock, PBYTE pData)			= 0;
	virtual BOOL METHOD Verify(DWORD dwBlock, UINT uCount)			= 0;
	virtual BOOL METHOD StartStop(bool fStart)				= 0;
	virtual BOOL METHOD LockMedia(bool fLock)				= 0;
	virtual BOOL METHOD SendDiag(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host CDC Driver
//

interface IUsbHostCdc : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 32);

	virtual BOOL METHOD SendEncapCommand(PBYTE pData, UINT uLen)		= 0;
	virtual UINT METHOD RecvEncapResponse(PBYTE pData, UINT uLen)		= 0;
	virtual BOOL METHOD SendData(PCTXT pData, bool fAsync)			= 0;
	virtual BOOL METHOD SendData(PCBYTE pData, UINT uLen, bool fAsync)	= 0;
	virtual UINT METHOD RecvData(PBYTE  pData, UINT uLen, bool fAsync)	= 0;
	virtual UINT METHOD WaitSend(UINT uTimeout)				= 0;
	virtual UINT METHOD WaitRecv(UINT uTimeout)				= 0;
	virtual void METHOD KillSend(void)					= 0;
	virtual void METHOD KillRecv(void)					= 0;
	virtual void METHOD KillAsync(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host CDC ACM Driver
//

interface IUsbHostAcm : public IUsbHostCdc
{
	AfxDeclareIID(21, 33);

	virtual BOOL METHOD SetCommFeatureState(WORD wState)			= 0;
	virtual BOOL METHOD GetCommFeatureState(WORD &wState)			= 0;
	virtual BOOL METHOD ClearCommFeatureState(void)				= 0;
	virtual BOOL METHOD SetCommFeatureCountry(WORD wCountry)		= 0;
	virtual BOOL METHOD GetCommFeatureCountry(WORD &wCountry)		= 0;
	virtual BOOL METHOD ClearCommFeatureCountry(void)			= 0;
	virtual BOOL METHOD SetLineCoding(CSerialConfig const &Config)		= 0;
	virtual BOOL METHOD GetLineCoding(CSerialConfig &Config)		= 0;
	virtual BOOL METHOD SetControlLineState(bool fRTS, bool fDTR)		= 0;
	virtual BOOL METHOD SendBreak(WORD wDuration)				= 0;
	virtual BOOL METHOD VendorRequest(BYTE bReq, WORD wAddr, WORD wValue)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host CDC ECM Driver
//

interface IUsbHostEcm : public IUsbHostCdc
{
	AfxDeclareIID(21, 34);

	virtual BOOL METHOD IsLinkActive(void)					  = 0;
	virtual BOOL METHOD WaitLinkActive(UINT uTime)				  = 0;
	virtual void METHOD GetMac(MACADDR &Addr)				  = 0;
	virtual UINT METHOD GetMaxFilters(void)					  = 0;
	virtual BOOL METHOD GetFilterHashing(void)				  = 0;
	virtual UINT METHOD GetCapabilities(void)				  = 0;
	virtual BOOL METHOD SetMulticastFilters(MACADDR const *pList, UINT uList) = 0;
	virtual BOOL METHOD SetPacketFilters(WORD wFilter)			  = 0;
	virtual BOOL METHOD GetStatistics(UINT iSelector, DWORD &Data)  	  = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host CDC NCM Driver
//

interface IUsbHostNcm : public IUsbHostEcm
{
	AfxDeclareIID(21, 35);

	virtual BOOL METHOD GetNtbParams(CdcNtbParam &Param)			= 0;
	virtual BOOL METHOD GetNetAddress(MACADDR &Addr)			= 0; 
	virtual BOOL METHOD SetNetAddress(MACADDR const &Addr)			= 0;
	virtual UINT METHOD GetNtbFormat(void)					= 0;
	virtual BOOL METHOD SetNtbFormat(UINT uFormat)				= 0;
	virtual BOOL METHOD GetNtbInputSize(UINT &uSize, UINT &uCount)		= 0;
	virtual BOOL METHOD SetNtbInputSize(UINT uSize, UINT  uCount)		= 0;
	virtual UINT METHOD GetMaxDatagramSize(void)				= 0;
	virtual BOOL METHOD SetMaxDatagramSize(UINT uSize)			= 0;
	virtual UINT METHOD GetCrcMode(void)					= 0;
	virtual BOOL METHOD SetCrcMode(UINT uMode)				= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Netowrk Interface 
//

interface IUsbNetwork : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 36);

	virtual BOOL METHOD Startup(BOOL fFast, BOOL fFull)			= 0;
	virtual BOOL METHOD Shutdown(void)					= 0;
	virtual BOOL METHOD InitMac(MACADDR const &Addr)			= 0;
	virtual void METHOD ReadMac(MACADDR &Addr)				= 0;
	virtual UINT METHOD GetCapabilities(void)				= 0;
	virtual void METHOD SetFlags(UINT uFlags)				= 0;
	virtual BOOL METHOD SetMulticast(MACADDR const *pList, UINT uList)	= 0;
	virtual BOOL METHOD IsLinkActive(void)					= 0;
	virtual BOOL METHOD WaitLink(UINT uTime)				= 0;
	virtual BOOL METHOD SendData(CBuffer *  pBuff)				= 0;
	virtual BOOL METHOD RecvData(CBuffer * &pBuff)				= 0;
	virtual void METHOD GetCounters(NICDIAG &Diag)				= 0;
	virtual void METHOD ResetCounters(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Module Return Codes
//

enum
{
	respFail,
	respPass,
	respFalse,
	respBlock,
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Module Base Interface
//

interface IUsbHostModule : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 37);

	virtual BOOL METHOD Reset(void)				= 0;
	virtual BOOL METHOD ReadVersion(BYTE Version[16])	= 0; 
	virtual BOOL METHOD SendHeartbeat(void)			= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Module Generic Interface
//

interface IUsbHostModuleGeneric : public IUsbHostModule
{
	AfxDeclareIID(21, 38);

	virtual BOOL METHOD SendData(PCBYTE pData, UINT uCount)		= 0;
	virtual UINT METHOD RecvData(PBYTE  pData, UINT uCount)		= 0;
	virtual BOOL METHOD SendDataAsync(PCBYTE pData, UINT uCount)	= 0;
	virtual UINT METHOD RecvDataAsync(PBYTE pData, UINT uCount)	= 0;
	virtual UINT METHOD WaitAsyncSend(UINT uTimeout)		= 0;
	virtual UINT METHOD WaitAsyncRecv(UINT uTimeout)		= 0;
	virtual BOOL METHOD KillAsyncSend(void)				= 0;
	virtual BOOL METHOD KillAsyncRecv(void)				= 0;
	virtual BOOL METHOD Shutdown(void)				= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Module Boot Interface
//

interface IUsbHostModuleBoot : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 39);

	virtual UINT METHOD CheckVersion(PCBYTE pVersion, UINT uSize)		= 0;
	virtual UINT METHOD ClearProgram(UINT uSize)				= 0;
	virtual UINT METHOD WriteProgram(UINT uAddr, PCBYTE pData, UINT uSize)	= 0;
	virtual UINT METHOD WriteVersion(PCBYTE pVersion, UINT uSize)		= 0;
	virtual UINT METHOD StartProgram(void)					= 0;
	virtual UINT METHOD WaitComplete(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Serial Interface Driver
//

interface IUsbHostSerial : public IUsbHostModule
{
	AfxDeclareIID(21, 40);

	virtual BOOL METHOD SetRun(BOOL fRun)					= 0;
	virtual BOOL METHOD SetPhysical(UINT uPhysical)				= 0;
	virtual BOOL METHOD SetBaud(UINT uBaud)					= 0;
	virtual BOOL METHOD SetFormat(UINT uData, UINT uStop, UINT Parity)	= 0;
	virtual BOOL METHOD SetFlags(UINT uFlags)				= 0;
	virtual BOOL METHOD SetLatency(UINT uLatency)				= 0;
	virtual BOOL METHOD SendData(PCBYTE pData, UINT uCount)			= 0;
	virtual UINT METHOD RecvData(PBYTE  pData, UINT uCount)			= 0;
	virtual BOOL METHOD SendDataAsync(PCBYTE pData, UINT uCount)		= 0;
	virtual UINT METHOD RecvDataAsync(PBYTE pData, UINT uCount)		= 0;
	virtual UINT METHOD WaitAsyncSend(UINT uTimeout)			= 0;
	virtual UINT METHOD WaitAsyncRecv(UINT uTimeout)			= 0;
	virtual BOOL METHOD KillAsyncSend(void)					= 0;
	virtual BOOL METHOD KillAsyncRecv(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Profibus Interface Driver
//

interface IUsbHostProfibus : public IUsbHostModule
{
	AfxDeclareIID(21, 41);

	virtual BOOL METHOD IsOnline(void)				= 0;
	virtual	BOOL METHOD SetRun(BOOL fRun)				= 0;
	virtual	BOOL METHOD SetStation(BYTE bStation)			= 0;
	virtual UINT METHOD GetInputDataSize(void)			= 0;
	virtual UINT METHOD GetOutputDataSize(void)			= 0;
	virtual BOOL METHOD PutData(PCBYTE pData, UINT uCount)		= 0;
	virtual UINT METHOD GetData(PBYTE  pData, UINT uCount)		= 0;
	virtual BOOL METHOD PutDataAsync(PCBYTE pData, UINT uCount)	= 0;
	virtual UINT METHOD GetDataAsync(PBYTE pData, UINT uCount)	= 0;
	virtual UINT METHOD WaitAsyncPut(UINT uTimeout)			= 0;
	virtual UINT METHOD WaitAsyncGet(UINT uTimeout)			= 0;
	virtual BOOL METHOD KillAsyncPut(void)				= 0;
	virtual BOOL METHOD KillAsyncGet(void)				= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Dongle Interface Driver
//

interface IUsbHostDongle : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 42);

	virtual BOOL METHOD SetLed(UINT uState)	= 0;
	virtual BOOL METHOD Challenge(void)	= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Host DeviceNet Interface Driver
//

interface IUsbHostDevNet : public IUsbHostModule
{
	AfxDeclareIID(21, 43);

	virtual	BOOL METHOD SetRun(BOOL fRun)					= 0;
	virtual BOOL METHOD SetBaudRate(UINT uBaud)				= 0;
	virtual BOOL METHOD SetDrop(UINT uDrop)					= 0;
	virtual BOOL METHOD EnableBitStrobe(WORD wRecvSize, WORD wSendSize)	= 0;
	virtual BOOL METHOD EnablePolled(WORD wRecvSize, WORD wSendSize)	= 0;
	virtual BOOL METHOD EnableData(WORD wRecvSize, WORD wSendSize)		= 0;
	virtual BOOL METHOD SendAsync(PCBYTE pData, UINT uCount)		= 0;
	virtual UINT METHOD RecvAsync(PBYTE  pData, UINT uCount)		= 0;
	virtual UINT METHOD WaitSend(UINT uTimeout)				= 0;
	virtual UINT METHOD WaitRecv(UINT uTimeout)				= 0;
	virtual BOOL METHOD KillSend(void)					= 0;
	virtual BOOL METHOD KillRecv(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Can Led States
//

enum
{
	ledOff		= 0,
	ledRun		= 1,
	ledError	= 2,
	ledAuto		= 3,
	ledManual	= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// Can Flags
//

enum
{
	flagExt		= Bit(0),
	flagRem		= Bit(1),
	}; 

//////////////////////////////////////////////////////////////////////////
//
// Host Can Interface Driver
//

interface IUsbHostCan : public IUsbHostModule
{
	AfxDeclareIID(21, 44);

	virtual BOOL METHOD SetBaudRate(UINT uBaud)				= 0;
	virtual BOOL METHOD SetFilter(UINT iFilter, DWORD Data, DWORD Mask)	= 0;
	virtual BOOL METHOD SetLed(UINT iLed, UINT uState)			= 0;
	virtual	BOOL METHOD SetRun(BOOL fRun)					= 0;
	virtual BOOL METHOD SetLatency(UINT uLatency)				= 0;
	virtual BOOL METHOD GetError(void)					= 0;
	virtual BOOL METHOD SendFrame(UsbCanFrame *pFrame, UINT uCount)		= 0;
	virtual UINT METHOD RecvFrame(UsbCanFrame *pFrame, UINT uCount)		= 0;
	virtual BOOL METHOD SendFrameAsync(UsbCanFrame *pFrame, UINT uCount)	= 0;
	virtual UINT METHOD RecvFrameAsync(UsbCanFrame *pFrame, UINT uCount)	= 0;
	virtual UINT METHOD WaitAsyncSend(UINT uTimeout)			= 0;
	virtual UINT METHOD WaitAsyncRecv(UINT uTimeout)			= 0;
	virtual BOOL METHOD KillAsyncSend(void)					= 0;
	virtual BOOL METHOD KillAsyncRecv(void)					= 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Ftdi Mode Bits
//

enum
{
	modeOff		= 0,
	modeAsyncBit	= Bit(0),
	modeMPSSE	= Bit(1),
	modeSyncBit	= Bit(2),
	modeCpuEmul	= Bit(3),
	modeResetFast	= Bit(4),
	modeSyncFifo	= Bit(6),
	};

//////////////////////////////////////////////////////////////////////////
//
// Ftdi Modem Control Bits
//

enum
{
	modemDTR	= Bit(0),
	modemRTS	= Bit(1),
	};

//////////////////////////////////////////////////////////////////////////
//
// Ftdi Modem Flow Control
//

enum
{
	flowRTS		= Bit(0),
	flowDTR		= Bit(1),
	flowXON		= Bit(2),
	};

//////////////////////////////////////////////////////////////////////////
//
// Host Ftdi Chip Driver
//

interface IUsbHostFtdi : public IUsbHostFuncDriver
{
	AfxDeclareIID(21, 45);

	virtual BOOL METHOD Reset(BOOL fClearTx, BOOL fClearRx)				= 0;
	virtual BOOL METHOD ModemCtrl(BYTE bMask, BYTE bState)				= 0;
	virtual BOOL METHOD SetFlowCtrl(BYTE bMask, BYTE bXOn, BYTE bXOff)		= 0;
	virtual BOOL METHOD SetBaudRate(UINT uBaud)					= 0;
	virtual BOOL METHOD SetData(UINT uBits, UINT uParity, UINT uStop, BOOL fBreak)	= 0;
	virtual BOOL METHOD GetModemStat(WORD &wStatus)					= 0;
	virtual BOOL METHOD SetLatTimer(BYTE bLatency)					= 0;
	virtual BOOL METHOD GetLatTimer(BYTE &bLatency)					= 0;
	virtual BOOL METHOD SetBitMode(BYTE bMode, BYTE bMask)				= 0;
	virtual BOOL METHOD GetBitMode(BYTE &bData)					= 0;
	virtual UINT METHOD Send(PBYTE pData, UINT uLen, BOOL fAsync)			= 0;
	virtual UINT METHOD Recv(PBYTE pData, UINT uLen, BOOL fAsync)			= 0;
	virtual UINT METHOD WaitSend(UINT uTimeout)					= 0;
	virtual UINT METHOD WaitRecv(UINT uTimeout)					= 0;
	virtual void METHOD KillAsync(void)						= 0;
	virtual BOOL METHOD PromRead(BYTE bAddr, WORD &wData)				= 0;
	virtual BOOL METHOD PromWrite(BYTE bAddr, WORD wData)				= 0;
	virtual BOOL METHOD PromErase(void)						= 0;
	virtual WORD METHOD PromChecksum(void)						= 0;
	virtual BOOL METHOD SetOutputLo(BYTE bMask, BYTE bData)				= 0;
	virtual BOOL METHOD SetOutputHi(BYTE bMask, BYTE bData)				= 0;
	virtual BYTE METHOD GetInputLo(void)						= 0;
	virtual BYTE METHOD GetInputHi(void)						= 0;
	};

// End of File

#endif
