/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** IO.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Handles IO packets
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

/*
** Common Packet Format (CPF) header for CLASS 1 I/O packets.  
*/

typedef struct tagCPFHDR	
{
    UINT16 iS_count;       /* Structure count */
    UINT16 iAs_type;       /* Address structure type */
    UINT16 iAs_length;     /* Address structure length */
    UINT16 aiAs_cid[2];    /* Address structure connection id */
    UINT16 aiAs_seq[2];    /* Address structure sequence number */
    UINT16 iDs_type;       /* Data structure type */
    UINT16 iDs_length;     /* Data structure length */
} CPFHDR, *CPFHDR_P;

#define CPFHDR_SIZE		18

extern INT32 ioParseClass1Packet( UINT32 lOriginatingAddress, UINT8* pData, INT32 lBytesReceived );
extern INT32  ioSendClass1Packet( INT32 nConnection );
