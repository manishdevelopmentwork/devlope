
#include "Intern.hpp"

#include "UsbHubGetDescReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Get Descriptor Request
//

// Constructor

CUsbHubGetDescReq::CUsbHubGetDescReq(WORD wIndex)
{
	Init(wIndex);
	} 

// Operations

void CUsbHubGetDescReq::Init(WORD wIndex)
{
	CUsbClassReq::Init();

	m_bRequest  = reqGetDesc;

	m_Direction = dirDevToHost;

	m_Recipient = recHub;

	m_wValue    = MAKEWORD(wIndex, descHub);

	m_wLength   = sizeof(UsbHubDesc);
	}

// End of File
