
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramList_HPP

#define INCLUDE_ProgramList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Program List
//

class DLLNOT CProgramList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramList(void);

		// Item Access
		CMetaItem * GetItem(INDEX Index) const;
		CMetaItem * GetItem(UINT  uPort) const;

		// Item Lookup
		UINT FindNamePos(CString Name) const;

		// Operations
		void UpdatePending(void);

		// Persistance
		void Init(void);
	};

// End of File

#endif
