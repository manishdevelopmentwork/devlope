
#include "Intern.hpp"

#include "LinkFindDialog.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Link Find Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CLinkFindDialog, CStdDialog);

// Constructors

CLinkFindDialog::CLinkFindDialog(BOOL fAlone)
{
	m_fAlone = fAlone;

	m_uItem  = NOTHING;

	SetName(L"LinkFindDlg");
}

// Operations

void CLinkFindDialog::PreSelect(CString Prot, CString Host)
{
	m_Prot = Prot;

	m_Host = Host;
}

// Attributes

CString CLinkFindDialog::GetHost(void) const
{
	return m_Host;
}

CString CLinkFindDialog::GetProt(void) const
{
	return m_Prot;
}

// Message Map

AfxMessageMap(CLinkFindDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnCommandOK)
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)
	AfxDispatchCommand(300, OnCommandIdentify)
	AfxDispatchCommand(301, OnCommandBrowse)
	AfxDispatchCommand(302, OnCommandRefresh)
	AfxDispatchCommand(303, OnCommandUseName)
	AfxDispatchCommand(304, OnCommandOpenFtp)

	AfxDispatchNotify(200, LVN_ITEMCHANGED, OnItemChanged)
	AfxDispatchNotify(200, NM_DBLCLK, OnDoubleClick)

	AfxMessageEnd(CLinkFindDialog)
};

// Message Handlers

BOOL CLinkFindDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( m_fAlone ) {

		GetDlgItem(IDOK).SetWindowText(IDS("&Select"));

		GetDlgItem(IDCANCEL).SetWindowText(IDS("&Close"));
	}

	LoadDns();

	CreateView();

	PerformLoad();

	return FALSE;
}

// Command Handlers

BOOL CLinkFindDialog::OnCommandOK(UINT uID)
{
	if( m_uItem < NOTHING ) {

		if( m_Fields[1] == L"1" ) {

			CString Text(IDS("Programming an emulated device via TCP/IP is not supported."));

			Error(Text);

			return TRUE;
		}

		m_Host = FindName();

		m_Prot = m_Fields[6];
	}

	SaveDns();

	EndDialog(TRUE);

	return TRUE;
}

BOOL CLinkFindDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
}

BOOL CLinkFindDialog::OnCommandIdentify(UINT uID)
{
	if( m_uItem < NOTHING ) {

		if( m_Fields[6] == L"USB" ) {

			ILinkTransport *pLink = Create_TransportUSB(C3OemGetUsbBase());

			pLink->OnBind(m_hWnd);

			if( pLink->Open() ) {

				CConfigLoader Config;

				Config.Bind(pLink, FALSE);

				Config.Identify(3);

				pLink->Close();
			}

			pLink->Terminate();

			pLink->Release();
		}
		else {
			CString Mac = m_Fields[3];

			ExecIdentify(Mac);
		}

		Information(IDS("The LEDs on the selected device should now be flashing."));
	}

	return TRUE;
}

BOOL CLinkFindDialog::OnCommandBrowse(UINT uID)
{
	if( m_uItem < NOTHING ) {

		ShellExecute(*afxMainWnd,
			     L"open",
			     L"https://" + FindName(),
			     NULL,
			     NULL,
			     SW_SHOWNORMAL
		);
	}

	return TRUE;
}

BOOL CLinkFindDialog::OnCommandOpenFtp(UINT uID)
{
	if( m_uItem < NOTHING ) {

		ShellExecute(*afxMainWnd,
			     L"open",
			     L"ftp://" + FindName(),
			     NULL,
			     NULL,
			     SW_SHOWNORMAL
		);
	}

	return TRUE;
}

BOOL CLinkFindDialog::OnCommandRefresh(UINT uID)
{
	m_List.Empty();

	m_pView->DeleteAllItems();

	m_pView->UpdateWindow();

	PerformLoad();

	return TRUE;
}

BOOL CLinkFindDialog::OnCommandUseName(UINT uID)
{
	GetDlgItem(301).EnableWindow(!FindName().IsEmpty());

	GetDlgItem(302).EnableWindow(!FindName().IsEmpty());

	return TRUE;
}

// Notification Handlers

void CLinkFindDialog::OnItemChanged(UINT uID, NMLISTVIEW &Info)
{
	m_Fields.Empty();

	if( (Info.uOldState ^ Info.uNewState) & LVIS_SELECTED ) {

		if( Info.uNewState & LVIS_SELECTED ) {

			m_uItem = Info.iItem;
		}
		else
			m_uItem = NOTHING;
	}

	if( m_uItem < NOTHING ) {

		m_List[m_uItem].Tokenize(m_Fields, ',');
	}

	GetDlgItem(IDOK).EnableWindow(m_uItem < NOTHING);

	GetDlgItem(300).EnableWindow(m_Fields[6] == L"USB" || !m_Fields[3].IsEmpty());

	GetDlgItem(301).EnableWindow(!FindName().IsEmpty());

	GetDlgItem(302).EnableWindow(!FindName().IsEmpty());
}

void CLinkFindDialog::OnDoubleClick(UINT uID, NMHDR &Info)
{
	OnCommandOK(IDOK);
}

// Implementation

void CLinkFindDialog::PerformLoad(void)
{
	afxThread->SetWaitMode(TRUE);

	FindUsb();

	ExecFind();

	LoadView();

	afxThread->SetWaitMode(FALSE);

	GetDlgItem(IDOK).EnableWindow(!m_List.IsEmpty());

	m_pView->SetFocus();
}

void CLinkFindDialog::ExecFind(void)
{
	HANDLE hRead;

	HANDLE hWrite;

	SECURITY_ATTRIBUTES sa;

	sa.nLength              = sizeof(SECURITY_ATTRIBUTES);
	sa.bInheritHandle       = TRUE;
	sa.lpSecurityDescriptor = NULL;

	CreatePipe(&hRead, &hWrite, &sa, 65536);

	SetHandleInformation(hRead, HANDLE_FLAG_INHERIT, 0);

	PROCESS_INFORMATION pi = { 0 };

	STARTUPINFO si = { 0 };

	si.cb         = sizeof(STARTUPINFO);
	si.dwFlags    = STARTF_USESTDHANDLES;
	si.hStdError  = hWrite;
	si.hStdOutput = hWrite;

	CString Prog = pccModule->GetFilename().WithName(L"Utils\\FindC32.exe");

	CString Line = Prog + L" -f";

	CreateProcess(NULL,
		      PTXT(PCTXT(Line)),
		      NULL,
		      NULL,
		      TRUE,
		      CREATE_NO_WINDOW,
		      NULL,
		      NULL,
		      &si,
		      &pi
	);

	if( pi.hProcess ) {

		if( WaitForSingleObject(pi.hProcess, 5000) == WAIT_OBJECT_0 ) {

			DWORD uCode;

			GetExitCodeProcess(pi.hProcess, &uCode);

			if( uCode == 0 ) {

				// TODO -- I don't like this as it will hang if there is
				// no output available. Right now, there has to be output
				// with an exit code of zero, but it's still delicate...

				BYTE  bData[65536];

				DWORD uDone = 0;

				ReadFile(hRead, bData, sizeof(bData), &uDone, NULL);

				CString Data = CString(PCSTR(bData), uDone);

				Data.Remove('\r');

				Data.Tokenize(m_List, '\n');
			}
		}

		CloseHandle(pi.hThread);

		CloseHandle(pi.hProcess);
	}

	CloseHandle(hRead);

	CloseHandle(hWrite);
}

void CLinkFindDialog::ExecIdentify(CString Mac)
{
	PROCESS_INFORMATION pi = { 0 };

	STARTUPINFO si = { 0 };

	si.cb = sizeof(STARTUPINFO);

	CString Prog = pccModule->GetFilename().WithName(L"Utils\\FindC32.exe");

	CString Line = Prog + L" -i " + Mac;

	CreateProcess(NULL,
		      PTXT(PCTXT(Line)),
		      NULL,
		      NULL,
		      FALSE,
		      CREATE_NO_WINDOW,
		      NULL,
		      NULL,
		      &si,
		      &pi
	);

	if( pi.hProcess ) {

		WaitForSingleObject(pi.hProcess, 5000);

		CloseHandle(pi.hThread);

		CloseHandle(pi.hProcess);
	}
}

void CLinkFindDialog::CreateView(void)
{
	CRect Rect;

	GetDlgItem(100).GetWindowRect(Rect);

	ScreenToClient(Rect);

	Rect.left   += 10;
	Rect.right  -= 10;
	Rect.top    += 20;
	Rect.bottom -= 10;

	m_pView = New CListView;

	m_pView->Create(L"",
			WS_CHILD | WS_BORDER | WS_TABSTOP | WS_VISIBLE | LVS_REPORT | LVS_SINGLESEL | LVS_SHOWSELALWAYS,
			Rect,
			*this,
			200
	);

	DWORD dwExStyle = LVS_EX_FULLROWSELECT | LVS_EX_ONECLICKACTIVATE;

	m_pView->SetExtendedListViewStyle(dwExStyle, dwExStyle);

	CString List[] = {

		IDS(""),
		IDS("Model Name"),
		IDS("Serial Number"),
		IDS("Transport"),
		IDS("MAC Address"),
		IDS("IP Address"),
		IDS("mDNS Name"),
	};

	int x1 = 20;

	int x2 = (Rect.right - Rect.left - x1) / (elements(List) - 1);

	for( UINT c = 0; c < elements(List); c++ ) {

		CListViewColumn ListCol(c, LVCFMT_LEFT | LVCFMT_FIXED_WIDTH, c ? x2 : x1, List[c]);

		m_pView->InsertColumn(c, ListCol);
	}
}

void CLinkFindDialog::LoadView(void)
{
	BOOL fSel = TRUE;

	for( UINT r = 0; r < m_List.GetCount(); r++ ) {

		CStringArray Fields;

		m_List[r].Tokenize(Fields, ',');

		((CString &) Fields[0]).MakeUpper();

		((CString &) Fields[2]).MakeUpper();

		((CString &) Fields[5]).MakeLower();

		m_pView->InsertItem(CListViewItem(r, 0, CPrintf(L"%u", r+1), 0, LPARAM(r)));

		if( Fields[1] == L"1" ) {

			m_pView->SetItem(CListViewItem(r, 1, Fields[0] + L" " + IDS("Emulator"), 0));
		}
		else {
			m_pView->SetItem(CListViewItem(r, 1, Fields[0], 0));
		}

		m_pView->SetItem(CListViewItem(r, 2, Fields[2], 0));

		m_pView->SetItem(CListViewItem(r, 3, Fields[6], 0));

		m_pView->SetItem(CListViewItem(r, 4, Fields[3], 0));

		m_pView->SetItem(CListViewItem(r, 5, Fields[4].IsEmpty() ? IDS("Not Available") : Fields[4], 0));

		m_pView->SetItem(CListViewItem(r, 6, Fields[5], 0));
		
		if( fSel ) {

			BOOL fHit = FALSE;

			if( m_Prot.IsEmpty() ) {

				if( !r ) {

					fHit = TRUE;
				}
			}
			else {
				if( m_Prot == Fields[6] ) {

					if( m_Host.IsEmpty() || m_Host == Fields[4] || m_Host == Fields[5] ) {

						fHit = TRUE;
					}
				}
			}

			if( fHit ) {

				m_pView->SetItemState(r, LVIS_SELECTED, LVIS_SELECTED);

				fSel = FALSE;
			}
		}
	}
}

CString CLinkFindDialog::FindName(void)
{
	BOOL fDns = ((CButton &) GetDlgItem(303)).IsChecked();

	return m_Fields[(fDns && !m_Fields[5].IsEmpty()) ? 5 : 4];
}

void CLinkFindDialog::FindUsb(void)
{
	if( m_fAlone ) {

		ILinkTransport *pLink = Create_TransportUSB(C3OemGetUsbBase());

		pLink->OnBind(m_hWnd);

		if( pLink->Open() ) {

			CConfigLoader Config;

			Config.Bind(pLink, FALSE);

			Config.Identify(1);

			BYTE bData[256];

			UINT uData = Config.GetIdentity(bData, sizeof(bData));

			if( uData > 0 ) {

				CString Serial;
				CString Model;
				CString mDns;

				CStringArray Ips;

				UINT    uPtr = 1;

				if( bData[uPtr++] == 2 ) {

					while( uPtr < uData ) {

						BYTE bCode = bData[uPtr++];

						BYTE bSize = bData[uPtr++];

						switch( bCode ) {

							case 1:
							{
								Serial = CString(PCSTR(bData + uPtr), bSize);
							}
							break;

							case 2:
							{
								Ips.Append(CPrintf(L"%u.%u.%u.%u", bData[uPtr+0], bData[uPtr+1], bData[uPtr+2], bData[uPtr+3]));
							}
							break;

							case 3:
							{
								if( mDns.IsEmpty() ) {
									
									if( bSize ) {

										mDns = CString(PCSTR(bData + uPtr), bSize) + L".local";
									}
								}
							}
							break;

							case 4:
							{
								Model = CString(PCSTR(bData + uPtr), bSize);
							}
							break;
						}

						uPtr += bSize;
					}

					if( Ips.IsEmpty() ) {

						Ips.Append(L"");
					}

					for( UINT c = 0; c < Ips.GetCount(); c++ ) {

						CString Usb;

						Usb += Model + L',';

						Usb += L"0,";

						Usb += Serial + L',';

						Usb += CString(IDS("Not Available")) + L',';

						Usb += Ips[c] + L',';

						Usb += mDns   + L',';

						Usb += L"USB";

						m_List.Append(Usb);
					}
				}
			}

			pLink->Close();
		}

		pLink->Terminate();

		pLink->Release();
	}
}

void CLinkFindDialog::LoadDns(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"ShLink");

	BOOL fDns = Reg.GetValue(L"mDNS", 0U);

	((CButton &) GetDlgItem(303)).SetCheck(fDns);
}

void CLinkFindDialog::SaveDns(void)
{
	BOOL fDns = ((CButton &) GetDlgItem(303)).IsChecked();

	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"ShLink");

	Reg.SetValue(L"mDNS", DWORD(fDns));
}

// End of File
