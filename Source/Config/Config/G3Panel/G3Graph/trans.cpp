
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Transform Menu

BOOL CPageEditorWnd::OnTransformGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_TRANS_FLIP_X,	MAKELONG(0x001A, 0x3000),
		IDM_TRANS_FLIP_Y,	MAKELONG(0x001B, 0x3000),
		IDM_TRANS_ROT_POS,	MAKELONG(0x0018, 0x3000),
		IDM_TRANS_ROT_NEG,	MAKELONG(0x0019, 0x3000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnTransformControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	if( m_SelList.GetCount() == 1 ) {

		INDEX   nPrim = m_SelList[0];

		CPrim * pPrim = m_pWorkList->GetItem(nPrim);

		BOOL    fOkay = FALSE;

		AfxAssume(pPrim);

		if( AfxPointerClass(pPrim)->IsKindOf(AfxNamedClass(L"CPrimRubyOriented")) ) {

			CMetaData const *pMeta = pPrim->FindMetaData(L"Orient");

			UINT            Orient = pMeta->ReadInteger(pPrim);

			if( Orient < 4 ) {

				fOkay = TRUE;
				}
			}

		if( AfxPointerClass(pPrim)->IsKindOf(AfxNamedClass(L"CPrimRubyGaugeBase")) ) {

			CMetaData const *pMeta = pPrim->FindMetaData(L"Orient");

			UINT            Orient = pMeta->ReadInteger(pPrim);

			if( Orient < 4 ) {

				fOkay = TRUE;
				}
			}

		if( pPrim->IsLine() ) {

			fOkay = TRUE;
			}

		if( fOkay ) {

			if( uID == IDM_TRANS_ROT_POS || uID == IDM_TRANS_ROT_NEG ) {

				R2R Rect = pPrim->GetReal();

				if( int(fabs((Rect.m_x2 - Rect.m_x1))) <= m_WorkRect.cy() ) {

					if( int(fabs((Rect.m_y2 - Rect.m_y1))) <= m_WorkRect.cx() ) {

						Src.EnableItem(TRUE);
						}
					}
				}
			else
				Src.EnableItem(TRUE);
			}
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnTransformCommand(UINT uID)
{
	// The Undo is far more expensive than it needs to be here,
	// as we could do it all by simply replaying the inverse
	// command to reverse the transformation that we performed.

	INDEX   nPrim = m_SelList[0];

	CPrim * pPrim = m_pWorkList->GetItem(nPrim);

	HGLOBAL hPrev = pPrim->TakeSnapshot();

	if( uID == IDM_TRANS_FLIP_X || uID == IDM_TRANS_FLIP_Y ) {

		if( pPrim->IsLine() ) {

			R2R Rect = pPrim->GetReal();

			if( uID == IDM_TRANS_FLIP_X ) {
				
				Swap(Rect.m_x1, Rect.m_x2);
				}

			if( uID == IDM_TRANS_FLIP_Y ) {

				Swap(Rect.m_y1, Rect.m_y2);
				}

			pPrim->SetReal(Rect);

			pPrim->UpdateLayout();
			}
		else {
			CMetaData const *pMeta  = pPrim->FindMetaData(L"Reflect");

			UINT            Reflect = pMeta->ReadInteger(pPrim);

			if( uID == IDM_TRANS_FLIP_X ) {

				Reflect ^= 1;
				}

			if( uID == IDM_TRANS_FLIP_Y ) {

				Reflect ^= 3;
				}

			pMeta->WriteInteger(pPrim, Reflect);

			pPrim->UpdateLayout();
			}
		}

	if( uID == IDM_TRANS_ROT_POS || uID == IDM_TRANS_ROT_NEG ) {

		if( pPrim->IsLine() ) {

			R2R Rect = pPrim->GetReal();

			number xp = (Rect.m_x1 + Rect.m_x2) / 2;
			number yp = (Rect.m_y1 + Rect.m_y2) / 2;

			Rect.m_x1 -= xp;
			Rect.m_y1 -= yp;
			Rect.m_x2 -= xp;
			Rect.m_y2 -= yp;

			if( uID == IDM_TRANS_ROT_POS ) {

				Swap(Rect.m_x1, Rect.m_y1);
				Swap(Rect.m_x2, Rect.m_y2);

				Rect.m_y1 *= -1;
				Rect.m_y2 *= -1;
				}

			if( uID == IDM_TRANS_ROT_NEG ) {

				Swap(Rect.m_x1, Rect.m_y1);
				Swap(Rect.m_x2, Rect.m_y2);

				Rect.m_x1 *= -1;
				Rect.m_x2 *= -1;
				}

			Rect.m_x1 += xp;
			Rect.m_y1 += yp;
			Rect.m_x2 += xp;
			Rect.m_y2 += yp;

			pPrim->SetReal(Rect);

			CRect Move = pPrim->GetRect();

			if( ClipMoveRect(Move) ) {

				pPrim->SetRect(Move);
				}

			pPrim->UpdateLayout();
			}
		else {
			CMetaData const *pMeta = pPrim->FindMetaData(L"Orient");

			UINT            Orient = pMeta->ReadInteger(pPrim);

			if( uID == IDM_TRANS_ROT_POS ) {

				switch( Orient ) {

					case 0: Orient = 2; break; // R->U
					case 2: Orient = 1; break; // U->L
					case 1: Orient = 3; break; // L->D
					case 3: Orient = 0; break; // D->R
					}
				}

			if( uID == IDM_TRANS_ROT_NEG ) {

				switch( Orient ) {

					case 0: Orient = 3; break; // R->D
					case 3: Orient = 1; break; // D->L
					case 1: Orient = 2; break; // L->U
					case 2: Orient = 0; break; // U->R
					}

				}

			pMeta->WriteInteger(pPrim, Orient);

			R2R Rect = pPrim->GetReal();

			number cx = (Rect.m_x2 - Rect.m_x1) / 2;
			number cy = (Rect.m_y2 - Rect.m_y1) / 2;

			if( cx != cy ) {

				number xp = (Rect.m_x2 + Rect.m_x1) / 2;
				number yp = (Rect.m_y2 + Rect.m_y1) / 2;

				Rect.m_x1 = xp - cy;
				Rect.m_y1 = yp - cx;
				Rect.m_x2 = xp + cy;
				Rect.m_y2 = yp + cx;

				pPrim->SetReal(Rect);

				CRect Move = pPrim->GetRect();

				if( ClipMoveRect(Move) ) {

					pPrim->SetRect(Move);
					}
				}

			pPrim->UpdateLayout();
			}
		}

	CString    Menu = CString(IDS_TRANSFORM);

	CCmdItem * pCmd = New CCmdItem(Menu, pPrim, hPrev);

	if( pCmd->IsNull() ) {

		delete pCmd;
		}
	else {
		LocalSaveCmd(pCmd);

		pPrim->SetDirty();
		}

	UpdateImage();

	UpdateSelectData(TRUE);

	return TRUE;
	}

// End of File
