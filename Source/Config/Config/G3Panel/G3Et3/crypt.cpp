
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// RFC 2617 compliant encryption
//

// Constructor

CCrypt::CCrypt(CString One, CString Two) : m_Realm(L"I/O module")
{
	m_Salt.Append(One);

	m_Salt.Append(m_Realm);

	m_Salt.Append(Two);

	Build();

	if( AcquireContext() ) {

		if( CreateHash() ) {

			if( HashData() ) {
				
				if( GetHashParam() ) {

					return;
					}
				}
			}
		}

	AfxAssert(FALSE);
	}

// Destructor

CCrypt::~CCrypt(void)
{
	CryptDestroyHash(m_Hash);
	}

// Attribute

PCBYTE CCrypt::GetHash(void)
{
	return m_bHash;
	}

// Operation

CString CCrypt::AsText(void)
{
	CString Work;

	for( UINT n = 0; n < sizeof(m_bHash); n ++ ) {

		Work += CPrintf(L"%0.2x", m_bHash[n]);
		}

	return Work;
	}

// Implementation

void CCrypt::Build(void)
{
	m_Work.Empty();

	for( UINT n = 0; n < m_Salt.GetCount(); n ++ ) {

		if( !m_Work.IsEmpty() ) {

			m_Work += L":";
			}

		m_Work += m_Salt[n];
		}
	}

// Wrapper

BOOL CCrypt::AcquireContext(void)
{
	return CryptAcquireContext( &m_Prov,
				     NULL,
				     MS_DEF_PROV,
				     PROV_RSA_FULL,
				     CRYPT_VERIFYCONTEXT
				     );
	}

BOOL CCrypt::CreateHash(void)
{
	return CryptCreateHash( m_Prov,
				CALG_MD5,
				0,
				0,
				&m_Hash
				);
	}

BOOL CCrypt::HashData(void)
{
	CAnsiString Work(m_Work);

	UINT    uLen = Work.GetLength();

	PBYTE pData = New BYTE [ uLen ];

	memcpy(pData, Work, uLen);

	BOOL fResult = CryptHashData( m_Hash,
				      pData,
				      uLen, 
				      0
				      );

	delete pData;

	return fResult;
	}

BOOL CCrypt::GetHashParam(void)
{
	DWORD dwSize = sizeof(m_bHash);

	return CryptGetHashParam( m_Hash,
				  HP_HASHVAL,
				  m_bHash,
				  &dwSize, 
				  0
				  );
	}

// End of File
