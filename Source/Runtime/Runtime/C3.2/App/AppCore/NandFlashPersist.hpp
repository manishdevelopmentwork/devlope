
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandFlashDatabase_HPP

#define	INCLUDE_NandFlashDatabase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include "CxNand.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Persistence Manager
//

class CNandFlashPersist : public CNandClient,
			  public IPersist
{
	public:
		// Constructor
		CNandFlashPersist(CNandBlock const &Start, CNandBlock const &End);

		// Destructor
		~CNandFlashPersist(void);

		// IPersist
		void  Init(void);
		void  Term(void);
		void  ByeBye(void);
		BOOL  IsDirty(void);
		void  Commit(BOOL fReset);
		BYTE  GetByte(DWORD dwAddr);
		WORD  GetWord(DWORD dwAddr);
		LONG  GetLong(DWORD dwAddr);
		void  GetData(PBYTE pData, DWORD dwAddr, UINT uCount);
		void  PutByte(DWORD dwAddr, BYTE bData);
		void  PutWord(DWORD dwAddr, WORD wData);
		void  PutLong(DWORD dwAddr, LONG lData);
		void  PutData(PBYTE pData, DWORD dwAddr, UINT uCount);

	protected:
		// Constants
		static DWORD const magicBank     = 0x62616E6B;
		static DWORD const magicHead	 = 0x68656164;
		static DWORD const magicBody     = 0x626F6479;
		static DWORD const magicTail     = 0x7461696C;
		static UINT  const C64K          = 65536;

		// Bank State
		struct CBank
		{
			PBYTE	   m_pData;
			BOOL	   m_fDirty;
			CNandBlock m_Block;
			};

		// Page Header
		struct CPageHeader
		{
			DWORD Magic;
			DWORD Param;
			DWORD Pad1;
			DWORD Pad2;
			DWORD Pad3;
			};

		// Type Definitions
		typedef	CPageHeader       * PPAGE;
		typedef	CPageHeader const * PCPAGE;

		// Data Members
		CNandBlock m_BlockScan;
		CBank	   m_Bank[4];

		// Implementation
		bool InitBanks(void);
		bool FreeBanks(void);
		bool LoadBanks(void);
		bool FindFreeBlock(CNandBlock &Block);
		bool FormatBlock(CNandBlock &Block);
	};

// End of File

#endif
