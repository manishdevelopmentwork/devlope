
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Wi-Fi Sled Status
//

// Data

var m_index;

var m_page_init;

var m_page_make;

// Code

function pageMain() {

	if ((m_index = getParam("id")) == null) {

		m_index = "0";
	}

	m_page_init = true;

	m_page_make = true;

	$("#title").html("Wi-Fi " + (1 + parseInt(m_index)) + " Status");

	initLogArea("/ajax/read-pipe.ajax?type=wifi&id=" + m_index, false);

	m_log_save = "wifi.txt";

	sendInfoRequest();
}

function sendInfoRequest() {

	var url = "/ajax/wifi-status.ajax?id=" + m_index;

	url += "&tab=" + m_tab_id;

	url += "&init=" + (m_page_init ? 1 : 0);

	$.get({
		url: makeAjax(url),
		success: onInfoOkay,
		error: onInfoFailed,
		timeout: m_page_init ? 15000 : 0,
		xhrFields: { withCredentials: true },
	});
}

function onInfoOkay(data) {

	var info = $("#wifi-info");

	if (data.okay) {

		if (m_page_make) {

			info.empty();

			var tt = $("<table/>").appendTo(info);

			tt.addClass("table table-striped table-bordered");

			var tb = $("<tbody/>").attr("id", "wifi-table").appendTo(tt);

			if (m_access <= 1) {

				var bd = $("<div/>").appendTo(info);

				var b1 = $("<button/>").attr("type", "button").css("margin-right", "16px").addClass("btn btn-danger").html("Reset Sled").appendTo(bd);

				b1.off("click").on("click", function () { sendCommand("reset"); });

				$("<div/>").css("margin-top", "16px").html("<em>Do not spam these buttons!<br/>Press once and wait to observe results.</em>").appendTo(info);
			}

			var cb = $("<input/>").attr("id", "show-box").attr("type", "checkbox").addClass("custom-control-input").appendTo(bd);

			var la = $("<label/>").attr("for", "show-box").addClass("custom-control-label").html(getString("show-act")).appendTo(bd);

			cb.change(function () { onLogShow($(this).prop("checked")); });

			m_page_make = false;
		}

		var tb = $("#wifi-table");

		addProp(tb, data, "apmode", "Mode", toMode);

		addProp(tb, data, "state", "State");

		addProp(tb, data, "version", "Firmware");

		addProp(tb, data, "network", "Network");

		if (!data.apmode) {

			addProp(tb, data, "signal", "Signal");
		}

		addProp(tb, data, "ctime", "Up Time", upTime);
	}
	else {

		info.empty();

		$("<p/>").html(getString("not-init")).appendTo(info);

		m_page_make = true;

		onLogShow(false);
	}

	if (!("defer" in data) || !data.defer) {

		setTimeout(sendInfoRequest, 1000);
	}
	else {

		m_page_init = false;

		setTimeout(sendInfoRequest, 100);
	}
}

function onInfoFailed(error) {

	if (!check401(error.status)) {

		var info = $("#wifi-info");

		info.empty();

		$("<p/>").html("Failed to Load.").appendTo(info);

		m_page_init = true;

		m_page_make = true;

		onLogShow(false);

		setTimeout(sendInfoRequest, 5000);
	}
}

function toMode(x) {

	return x ? "Access Point" : "Station";
}

function addProp(tb, data, name, label, func) {

	if (name in data) {

		var td = $("#data-" + name);

		if (!td.length) {

			var tr = $("<tr/>").appendTo(tb);

			$("<td/>").html("<b>" + label + "</b>").css("width", "30%").appendTo(tr);

			td = $("<td/>").css("width", "70%").attr("id", "data-" + name).appendTo(tr);
		}

		td.html(func ? func(data[name]).toString() : data[name].toString());
	}
}

function sendCommand(cmd) {

	var url = "/ajax/wifi-command.ajax?id=" + m_index + "&command=" + cmd;

	$.get({
		url: makeAjax(url),
		xhrFields: { withCredentials: true },
	});
}

// End of File
