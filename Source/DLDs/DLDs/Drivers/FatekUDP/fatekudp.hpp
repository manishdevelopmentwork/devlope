
//////////////////////////////////////////////////////////////////////////
//
// Fatek PLC Driver
//
#include "fatebase.hpp"

#ifndef	FATEKUDPINC
#define	FATEKUDPINC

class CFatekPLCUDPDriver : public CFatekPLCBaseDriver
{
	public:
		// Constructor
		CFatekPLCUDPDriver(void);

		// Destructor
		~CFatekPLCUDPDriver(void);// Configuration

		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		// Device Data
		struct CContext : CFatekPLCBaseDriver::CBaseCtx
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;

			BYTE	 Drop[2];
			};

		CContext *	m_pCtx;

		UINT	m_uKeep;
		
		// Transport Layer
		BOOL	Transact(void);
		BOOL	Send(void);
		BOOL	GetReply(void);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File

#endif
