
#include "Intern.hpp"

#include "NativeDnsResolver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DNS Resolver
//

// Registration

global void Register_NativeDnsResolver(void)
{
	piob->RegisterSingleton("net.dns", 0, New CNativeDnsResolver);
}

global void Revoke_NativeDnsResolver(void)
{
	piob->RevokeSingleton("net.dns", 0);
}

// Constructor

CNativeDnsResolver::CNativeDnsResolver(void)
{
	StdSetRef();

	m_pLock = Create_Mutex();

	m_fUtil = !!CAutoFile("/opt/crimson/bin/Resolve", "r");

	FindTempPath();
}

// Destructor

CNativeDnsResolver::~CNativeDnsResolver(void)
{
	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CNativeDnsResolver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IDnsResolver);

	StdQueryInterface(IDnsResolver);

	return E_NOINTERFACE;
}

ULONG CNativeDnsResolver::AddRef(void)
{
	StdAddRef();
}

ULONG CNativeDnsResolver::Release(void)
{
	StdRelease();
}

// IDnsResolver

CIpAddr CNativeDnsResolver::Resolve(PCTXT pName)
{
	CArray <CIpAddr> List;

	if( Resolve(List, pName) ) {

		return List[rand() % List.GetCount()];
	}

	return IP_EMPTY;
}

BOOL CNativeDnsResolver::Resolve(CArray <CIpAddr> &List, PCTXT pName)
{
	if( pName && *pName ) {

		CIpAddr Ip = pName;

		if( Ip.IsEmpty() ) {

			m_pLock->Wait(FOREVER);

			CString Name = "/." + m_Temp + "dns.out";

			if( m_fUtil ) {

				CallProcess("/opt/crimson/bin/Resolve",
					    pName,
					    Name,
					    NULL
				);
			}
			else {
				CallProcess("/bin/ping",
					    CPrintf("-c 1 %s", pName),
					    Name,
					    NULL
				);
			}

			CAutoFile File(Name, "r");

			if( File ) {

				if( m_fUtil ) {

					CIpAddr Ip = PCTXT(File.GetLine());

					if( !Ip.IsEmpty() ) {

						List.Append(Ip);
					}
				}
				else {
					CString Line;

					while( !(Line = File.GetLine()).IsEmpty() ) {

						char pat[] = " (";

						UINT uFind = Line.Find(pat);

						if( uFind < NOTHING ) {

							UINT uLast = Line.Find(')', uFind);

							if( uLast < NOTHING ) {

								uFind += sizeof(pat);

								uFind -= 1;

								CIpAddr Ip = PCTXT(Line.Mid(uFind, uLast - uFind));

								if( !Ip.IsEmpty() ) {

									List.Append(Ip);
								}

								break;
							}
						}
					}
				}

				_unlink(Name);

				m_pLock->Free();

				return !List.IsEmpty();
			}
		}

		List.Append(Ip);

		return TRUE;
	}

	return FALSE;
}

// Implementation

bool CNativeDnsResolver::FindTempPath(void)
{
	char  sPath[MAX_PATH] = { 0 };

	char *pEnv;

	sPath[0] || ((pEnv = getenv("aeontmp")) && strcpy(sPath, pEnv));

	sPath[0] || ((pEnv = getenv("tmp"))     && strcpy(sPath, pEnv));

	sPath[0] || ((pEnv = getenv("temp"))    && strcpy(sPath, pEnv));

	sPath[0] || ((pEnv = getenv("HOME"))    && strcpy(sPath, pEnv));

	sPath[0] || strcpy(sPath, ".");

	UINT c = strlen(sPath);

	if( sPath[c-1] != '/' ) {

		strcat(sPath, "/");
	}

	m_Temp = sPath;

	return true;
}

// End of File
