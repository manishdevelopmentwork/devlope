/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** DATATBL.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Handling CIP Data Table Read/Write requests
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

/*---------------------------------------------------------------------------
** dataTableInit( )
**
** Initialize data table entries. The following code may only be used as an 
** example. These should be substituted with the real values appropriate for
** a particular application.
**---------------------------------------------------------------------------
*/
void dataTableInit()
{
	}

/*---------------------------------------------------------------------------
** dataTableParseRequest( )
**
** Handle CIP Data Table Read/Write request
**---------------------------------------------------------------------------
*/

void dataTableParseRequest( INT32 nRequest )
{
	gRequests[nRequest].bGeneralError = ROUTER_ERROR_INVALID_DESTINATION;
	}


