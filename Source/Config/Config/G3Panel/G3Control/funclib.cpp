
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function Blocks Navigation Window
//

class CFunctionBlocksResTreeWnd : public CResTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CFunctionBlocksResTreeWnd(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	protected:
		// Data Members
		UINT	m_cfFunction;
		CString	m_InfoTip;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnSetFocus(CWnd &Wnd);

		// Command Handlers
		BOOL OnResGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnResControl(UINT uID, CCmdSource &Src);
		BOOL OnResCommand(UINT uID);

		// Data Object Construction		
		BOOL MakeDataObject(IDataObject * &pData);

		// Tree Loading
		void LoadImageList(void);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void NewItemSelected(void);

		// Implementation
		BOOL    AddFunction(CDataObject *pData);
		void    ShowPrototype(void);
		CString FindDescription(DWORD dwIdent);
		BOOL    FindHelp(void);		
	};

//////////////////////////////////////////////////////////////////////////
//
// Function Blocks Navigation Window
//
	
// Dynamic Class

AfxImplementDynamicClass(CFunctionBlocksResTreeWnd, CResTreeWnd);
		
// Constructor

CFunctionBlocksResTreeWnd::CFunctionBlocksResTreeWnd(void) : CResTreeWnd( L"Library",
								          AfxRuntimeClass(CFolderItem),
									  AfxRuntimeClass(CLibraryItem)
									  )
{
	m_cfFunction = RegisterClipboardFormat(L"CF_K5FUNCBLOCK");

	m_fJumpTo    = FALSE;

	m_Accel1.Create(L"ControlLibAccel1");
	}

// IUpdate

HRESULT CFunctionBlocksResTreeWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateChildren ) {

		if( pItem == m_pItem ) {
			
			RefreshTree();
			}
		}

	return CResTreeWnd::ItemUpdated(pItem, uType);
	}

// Message Map

AfxMessageMap(CFunctionBlocksResTreeWnd, CResTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxDispatchGetInfoType(IDM_RES, OnResGetInfo)
	AfxDispatchControlType(IDM_RES, OnResControl)
	AfxDispatchCommandType(IDM_RES, OnResCommand)

	AfxMessageEnd(CFunctionBlocksResTreeWnd)
	};

// Message Handlers

void CFunctionBlocksResTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"ControlLibraryTool"));
		}
	}

void CFunctionBlocksResTreeWnd::OnSetFocus(CWnd &Wnd)
{
	CResTreeWnd::OnSetFocus(Wnd);

	ShowPrototype();
	}

// Notification Handlers

// Command Handlers

BOOL CFunctionBlocksResTreeWnd::OnResGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_RES_HELP,   MAKELONG(0x000E, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CFunctionBlocksResTreeWnd::OnResControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_RES_HELP:

			Src.EnableItem(!m_pTree->GetChild(m_hSelect));

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CFunctionBlocksResTreeWnd::OnResCommand(UINT uID)
{
	switch( uID ) {
		
		case IDM_RES_HELP:

			FindHelp();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Data Object Construction

BOOL CFunctionBlocksResTreeWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddFunction(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

// Tree Loading

void CFunctionBlocksResTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"CtrlTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CFunctionBlocksResTreeWnd::GetRootImage(void)
{
	return 1;
	}

UINT CFunctionBlocksResTreeWnd::GetItemImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(m_Folder) ) {

		return IDI_FOLDER;
		}

	return IDI_FUNCTION;
	}

BOOL CFunctionBlocksResTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {
	
		Name = L"SystemLibCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

void CFunctionBlocksResTreeWnd::NewItemSelected(void)
{
	ShowPrototype();
	}

// Implementation

BOOL CFunctionBlocksResTreeWnd::AddFunction(CDataObject *pData)
{
	if( m_pNamed ) {

		if( m_pNamed->IsKindOf(m_Class) ) {

			CString     Full = m_pNamed->GetName();			

			CString     Name = Full.Mid(Full.FindRev(L'.')+1);

			CAnsiString Ansi = CAnsiString(Name);

			pData->AddText(m_cfFunction, LPCSTR(Ansi));

			return TRUE;
			}
		}

	return FALSE;
	}

void CFunctionBlocksResTreeWnd::ShowPrototype(void)
{
	CString Text;

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CRegistryItem)) ) {

		CRegistryItem *pItem   = (CRegistryItem *) m_pSelect;

		DWORD          dwIdent = pItem->m_Ident;

		CString        Name    = afxRegistry->GetBlockName(dwIdent);

		CFunctionDefinition Defn(dwIdent);

		if( Defn.IsStandard() ) {
			
			Text += CString(IDS_STANDARD);
			}

		if( Defn.IsCustom() ) {
			
			Text += CString(IDS_CUSTOM);
			}

		if( Defn.IsOperator() ) {

			Text += L" ";
			
			Text += CString(IDS_OPERATOR);
			}

		if( Defn.IsFunction() ) {
			
			Text += L" ";
			
			Text += CString(IDS_FUNCTION);
			}

		if( Defn.IsFuncBlock() ) {
			
			Text += L" ";
			
			Text += CString(IDS_FUNCTION_BLOCK);
			}

		Text += L" - ";

		Text += Name;

		if( Defn.Is64Bit() ) {
			
			Text += L", ";
			
			Text += CString(IDS_BIT_TYPES);
			}

		if( Defn.IsIec() ) {
			
			Text += L", ";
			
			Text += CString(IDS_IEC_COMPLIANT);
			}

		CString Desc = FindDescription(dwIdent);

		Text += CPrintf(L" - %s.", Desc);
		}

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CControlItem)) ) {

	//	CControlItem *pItem = (CControlItem *) m_pSelect;

		Text += CString(IDS_DATABASE_ITEM);

		Text += L".";
		}
	
	afxThread->SetStatusText(Text);
	}

CString CFunctionBlocksResTreeWnd::FindDescription(DWORD dwIdent)
{
	CString Path;

	Path += afxRegistry->GetLibFolderPath();

	Path += afxRegistry->GetBlockLibName(dwIdent);

	Path += L"\\";

	Path += L"neutral";	

	Path += L"\\";

	Path += L"blocks.com";	

	FILE *    pFile = _wfopen(Path, L"rb");

	if( pFile ) {

		CAnsiString Find = CAnsiString(PCTXT(afxRegistry->GetBlockName(dwIdent)));

		for(;;) {

			char sLine[256] = { 0 };

			fgets(sLine, sizeof(sLine), pFile);

			if( sLine[0] ) {

				char *pArg = strchr(sLine, '=');

				if( pArg ) {

					*pArg = 0;

					if( !strcmp(sLine, Find) ) {

						fclose(pFile);

						return CString(pArg+1);
						}
					}

				continue;
				}

			break;
			}

		fclose(pFile);
		}

	return CString(IDS_DESCRIPTION_FOR);
	}

BOOL CFunctionBlocksResTreeWnd::FindHelp(void)
{
	if( m_pSelect->IsKindOf(AfxRuntimeClass(CLibraryItem)) ) {

		CLibraryItem * pItem   = (CLibraryItem *) m_pSelect;

		DWORD          dwIdent = pItem->m_Ident;

		CString        Name    = afxRegistry->GetBlockName(dwIdent);

		CPdfHelpSystem pdf(3);

		if( pdf.ShowHelp(Name) ) {

			return TRUE;
			}
		}

	Error(CString(IDS_HELP_IS_NOT));

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Manager Object
//

// Dynamic Class

AfxImplementDynamicClass(CFunctionBlocksManager, CMetaItem);

// Constructor

CFunctionBlocksManager::CFunctionBlocksManager(void)
{
	m_pLibrary = New CRegistryList;
	}

// Operations

void CFunctionBlocksManager::Update(void)
{
	m_pLibrary->DeleteAllItems(TRUE);

	LoadPrograms();

	m_pLibrary->LoadFromDisk();
	}

// UI Creation

CViewWnd * CFunctionBlocksManager::CreateView(UINT uType)
{
	if( uType == viewResource ) {
		
		CLASS Class = AfxNamedClass(L"CFunctionBlocksResTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return NULL;
	}

// Item Naming

CString CFunctionBlocksManager::GetHumanName(void) const
{
	return L"Functions";
	}

// Persistance

void CFunctionBlocksManager::Init(void)
{
	CMetaItem::Init();

	FindProject();

	LoadPrograms();

	m_pLibrary->LoadFromDisk();
	}

void CFunctionBlocksManager::PostLoad(void)
{
	CMetaItem::PostLoad();

	FindProject();

	LoadPrograms();

	m_pLibrary->LoadFromDisk();
	}

// Meta Data

void CFunctionBlocksManager::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddCollect(Library);
	}

// Implementation

void CFunctionBlocksManager::FindProject(void)
{
	m_pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));
	}

void CFunctionBlocksManager::LoadPrograms(void)
{
	m_pLibrary->AddCategory(CString(IDS_CONTROL_PROJECT));
	
	DWORD dwProject = m_pProject->GetHandle();

	DWORD dwSection = CControlProgram::sectUDFB  | 
			  CControlProgram::sectBegin | 
			  CControlProgram::sectEnd   ;

	CLongArray List;

	if( afxDatabase->GetPrograms(dwProject, dwSection, List) ) {

		for( UINT n = 0; n < List.GetCount(); n ++ ) {
			
			DWORD dwProgram = List[n];

			CStratonProgramDescriptor Desc(dwProject, dwProgram);

			if( Desc.m_dwSection & CControlProgram::sectUDFB ) {

				CControlItem *pItem = New CControlItem;

				pItem->m_Ident = dwProgram;

				m_pLibrary->AppendItem(pItem);

				pItem->OnCreate();

				continue;
				}

			if( Desc.IsCalled() ) {

				CControlItem *pItem = New CControlItem;

				pItem->m_Ident = dwProgram;

				m_pLibrary->AppendItem(pItem);

				pItem->OnCreate();

				continue;
				}
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Item List
//

// Dynamic Class

AfxImplementDynamicClass(CRegistryList, CNamedList);

// Constructor

CRegistryList::CRegistryList(void)
{
	}

// Operations

void CRegistryList::LoadFromDisk(void)
{
	DWORD dwMask = 0;
	
//	dwMask |= (regStdOp | regStdFunc | regStdFb);

//	dwMask |= regFunc;

//	dwMask |= regFb;

	dwMask |= regAll;

	LoadFromDisk(dwMask);
	}

// Implementation

void CRegistryList::LoadFromDisk(DWORD dwMask)
{
	UINT uCount;

	CLongArray List;

	if( (uCount = afxRegistry->GetBlocks(dwMask, List)) ) {

		for( UINT n = 0; n < uCount; n ++ ) {

			DWORD dwIdent = List[n];

			CFunctionDefinition Defn(dwIdent);

			if( Defn.CanLoad() ) {

				AddCategory(CString(IDS_ALL));

				CLibraryItem *pItem = New CRegistryItem;

				pItem->m_Ident = dwIdent;

				AppendItem(pItem);

				pItem->OnCreate(CString(IDS_ALL));
				}
			}

		for( UINT n = 0; n < uCount; n ++ ) {

			DWORD dwIdent = List[n];

			CFunctionDefinition Defn(dwIdent);

			if( Defn.CanLoad() ) {

				AddCategory(dwIdent);

				CLibraryItem *pItem = New CRegistryItem;

				pItem->m_Ident = dwIdent;

				AppendItem(pItem);

				pItem->OnCreate();
				}
			}
		}
	}

void CRegistryList::AddCategory(DWORD dwIdent)
{
	CString Name = afxRegistry->GetBlockLibName(dwIdent);

	AddCategory(Name);
	}

void CRegistryList::AddCategory(CString Name)
{
	if( Name == CString(IDS_PLUS) ) {

		Name = CString(IDS_COUNTERS);
		}

	if( !FindByName(Name) ) {

		CMetaItem *pFolder = New CFolderItem;
		
		AppendItem(pFolder);

		pFolder->SetName(Name);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Registry Item
//

// Dynamic Class

AfxImplementDynamicClass(CLibraryItem, CUIItem);

// Constructor

CLibraryItem::CLibraryItem(void)
{
	m_Ident = 0;
	}

// Attributes

UINT CLibraryItem::GetTreeImage(void) const
{
	return 2;
	}

// Overridables

void CLibraryItem::OnCreate(void)
{
	}

void CLibraryItem::OnCreate(CString Root)
{
	}

// Property Filters

BOOL CLibraryItem::SaveProp(CString const &Tag) const
{
	if( Tag == L"Ident" ) {
		
		return FALSE;
		}

	return CMetaItem::SaveProp(Tag);
	}

// Meta Data

void CLibraryItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Ident);
	Meta_AddString (Name);
	}

//////////////////////////////////////////////////////////////////////////
//
// Library Item
//

// Dynamic Class

AfxImplementDynamicClass(CRegistryItem, CLibraryItem);

// Constructor

CRegistryItem::CRegistryItem(void)
{
	}

// Overridables

void CRegistryItem::OnCreate(void)
{
	OnCreate(afxRegistry->GetBlockLibName(m_Ident));
	}

void CRegistryItem::OnCreate(CString Root)
{
	if( Root == CString(IDS_PLUS) ) {

		Root = CString(IDS_COUNTERS);
		}

	DWORD dwKind;
	DWORD dwLibNo; 
	DWORD dwNbInput;
	DWORD dwNbOutput;

	CString Name = afxRegistry->GetBlockDesc( m_Ident, 
						  dwKind, 
						  dwLibNo, 
						  dwNbInput, 
						  dwNbOutput
						  );

	SetName(CPrintf(L"%s.%s", Root, Name));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Item
//

// Dynamic Class

AfxImplementDynamicClass(CControlItem, CLibraryItem);

// Constructor

CControlItem::CControlItem(void)
{
	}

// Attributes

UINT CControlItem::GetTreeImage(void) const
{
	return 2;
	}

// Persistance

void CControlItem::Init(void)
{
	CLibraryItem::Init();

	FindProject();
	}

void CControlItem::PostLoad(void)
{
	CLibraryItem::PostLoad();

	FindProject();
	}

// Overridables

void CControlItem::OnCreate(void)
{
	CString    Root = CString(IDS_CONTROL_PROJECT);

	DWORD dwProject = m_pProject->GetHandle();

	CStratonProgramDescriptor Prog(dwProject, m_Ident);

	SetName(CPrintf(L"%s.%s", Root, Prog.m_Name));
	}

void CControlItem::FindProject(void)
{
	m_pProject = (CControlProject *) GetParent(AfxRuntimeClass(CControlProject));
	}

// End of File
