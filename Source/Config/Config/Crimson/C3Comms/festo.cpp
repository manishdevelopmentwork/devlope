
#include "intern.hpp"

#include "festo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Festo FPC, IPC, FEC Series Controller
//

// Instantiator

ICommsDriver *	Create_FestoFPCDriver(void)
{
	return New CFestoFPCDriver;
	}

// Constructor

CFestoFPCDriver::CFestoFPCDriver(void)
{
	m_wID		= 0x336C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Festo";
	
	m_DriverName	= "FPC,IPC,FEC Series Controller";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Festo FPC,IPC,FEC";

	AddSpaces();
	}

// Binding Control

UINT CFestoFPCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CFestoFPCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CFestoFPCDriver::GetDeviceConfig(void)
{
	return NULL;
	}


// Implementation

void CFestoFPCDriver::AddSpaces(void)
{
	AddSpace(New CSpace('R', "R",  "Registers",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('O', "AW", "Output Words",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('I', "EW", "Input Words",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('F', "MW", "Memory Words",	  10, 0, 9999,	addrWordAsWord));
	AddSpace(New CSpace('T', "T",  "Timer Status",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('U', "TW", "Timer Words",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('V', "TV", "Timer Presets",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('Z', "Z",  "Counter Status",  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('A', "ZW", "Counter Words",	  10, 0, 255,	addrWordAsWord));
	AddSpace(New CSpace('B', "ZV", "Counter Presets", 10, 0, 255,	addrWordAsWord));	 
	}

// End of File
