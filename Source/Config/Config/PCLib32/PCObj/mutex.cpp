
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mutex Object Class
//

// Runtime Class

AfxImplementRuntimeClass(CMutex, CWaitable);

// Constructor

CMutex::CMutex(void)
{
	m_hObject = CreateMutex(NULL, FALSE, NULL);
	}

// Operations

void CMutex::Release(void)
{
	ReleaseMutex(m_hObject);
	}

// End of File
