
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Deleting BSTR
//

// Constructors

CBSTR::CBSTR(BSTR bstr) : CString(bstr)
{
	SysFreeString(bstr);
	}

// End of File
