
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Runtime Class

AfxImplementRuntimeClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestWnd;

	CPoint Pos = CPoint(280, 180);

	CSize Size = CSize (800, 600);

	CRect Rect = CRect (Pos, Size);

	pWnd->Create( L"Test Application",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect, AfxNull(CWnd), CMenu(L"Test"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_EXIT ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CMenuWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	m_Accel.Create(L"Test");

	HDC hDC = GetDC(NULL);

	m_hDC   = CreateCompatibleDC(hDC);

	m_hBits = CreateCompatibleBitmap(hDC, 320, 240);

	m_hOld  = SelectObject(m_hDC, m_hBits);

	m_pGDI  = Create_WinGDI(m_hDC, 320, 240);

	ReleaseDC(NULL, hDC);

	m_pGDI->ClearScreen(GetRGB(31,0,0));
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	m_pGDI->Release();

	SelectObject(m_hDC, m_hOld);

	DeleteDC(m_hDC);
	}

// Message Map

AfxMessageMap(CTestWnd, CMenuWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

BOOL CTestWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

void CTestWnd::OnPostCreate(void)
{
	}

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(*this);

	DC.BitBlt(m_DispRect, CDC(m_hDC), CPoint(0, 0), SRCCOPY);
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = CWnd::GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	DC.Save();

	DC.ExcludeClipRect(m_DispRect);

	DC.FillRect(Rect, afxBrush(AppWorkspace));

	DC.Restore();

	return TRUE;
	}

void CTestWnd::OnSize(UINT uCode, CSize Size)
{
	FindLayout();

	Invalidate(TRUE);
	}

void CTestWnd::OnSetFocus(CWnd &Wnd)
{
	Invalidate(FALSE);
	}

void CTestWnd::OnKillFocus(CWnd &Wnd)
{
	Invalidate(FALSE);
	}

void CTestWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	}

void CTestWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	}

void CTestWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	}

// Command Handlers

BOOL CTestWnd::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_NEW ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_NEW ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

// Implementation

void CTestWnd::FindLayout(void)
{
	m_DispSize.cx = m_pGDI->GetCx();

	m_DispSize.cy = m_pGDI->GetCy();

	CRect Rect  = GetClientRect() - 8;

	m_DispScale = Rect.GetSize() / m_DispSize;

	m_DispScale.cx = 1; // min(m_DispScale.cx, m_DispScale.cy);

	m_DispScale.cy = 1; // min(m_DispScale.cx, m_DispScale.cy);

	CSize Size = m_DispScale * m_DispSize;

	m_DispPos  = Rect.GetTopLeft() + (Rect.GetSize() - Size) / 2;

	m_DispRect = CRect(m_DispPos, Size);
	}

// End of File
