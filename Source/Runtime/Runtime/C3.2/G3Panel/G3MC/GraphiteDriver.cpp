
#include "intern.hpp"

#include "GraphiteDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphite Driver
//

// Instantiator

IRackDriver * Create_GraphiteDriver(IRackHandler *pHandler)
{
	return New CGraphiteDriver(pHandler);
}

// Constructor

CGraphiteDriver::CGraphiteDriver(IRackHandler *pHandler)
{
	StdSetRef();

	m_pHandler = pHandler;

	m_bSeq     = 0;
}

// Destructor

CGraphiteDriver::~CGraphiteDriver(void)
{
	AfxRelease(m_pHandler);
}

// IUnknown

HRESULT CGraphiteDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IRackDriver);

	StdQueryInterface(IRackDriver);

	return E_NOINTERFACE;
}

ULONG CGraphiteDriver::AddRef(void)
{
	StdAddRef();
}

ULONG CGraphiteDriver::Release(void)
{
	StdRelease();
}

// IRackDriver

UINT CGraphiteDriver::RxGeneral(BYTE bDrop)
{
	return m_bData[2] == opAck ? msgAck : msgError;
}

UINT CGraphiteDriver::BootTxForceReset(BYTE bDrop)
{
	NewPacket(servBoot, bootForceReset);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxCheckModel(BYTE bDrop)
{
	NewPacket(servBoot, bootCheckModel);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxCheckVersion(BYTE bDrop, PBYTE pGuid)
{
	NewPacket(servBoot, bootCheckVersion);

	AddData(pGuid, 16);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxProgramReset(BYTE bDrop)
{
	NewPacket(servBoot, bootForceReset);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxClearProgram(BYTE bDrop, DWORD dwSize)
{
	NewPacket(servBoot, bootClearProgram);

	AddLong(dwSize);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxProgramSize(BYTE bDrop, DWORD dwSize)
{
	NewPacket(servBoot, bootProgramSize);

	AddWord(LOWORD(dwSize));

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxWriteProgram(BYTE bDrop, WORD wAddr, PBYTE pData, WORD wCount)
{
	NewPacket(servBoot, bootWriteProgram);

	AddWord(wAddr);

	AddData(pData, wCount);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxWriteProgram32(BYTE bDrop, DWORD dwAddr, PBYTE pData, WORD wCount)
{
	NewPacket(servBoot, bootWriteProgram32);

	AddLong(dwAddr);

	AddData(pData, wCount);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxWriteVerify(BYTE bDrop, DWORD Crc32)
{
	return msgError;
}

UINT CGraphiteDriver::BootTxWriteVersion(BYTE bDrop, PBYTE pGuid)
{
	NewPacket(servBoot, bootWriteVersion);

	AddData(pGuid, 16);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootTxStartProgram(BYTE bDrop)
{
	NewPacket(servBoot, bootStartProgram);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::BootRxCheckModel(BYTE bDrop, BYTE &ModelID)
{
	switch( m_bData[2] ) {

		case opReply:

			ModelID = m_bData[4];

			return msgFrame;

		case opNak:

			ModelID = m_bData[4];

			return msgNak;
	}

	return msgError;
}

UINT CGraphiteDriver::BootRxWriteVerify(BYTE bDrop, BOOL &fSame)
{
	return msgError;
}

UINT CGraphiteDriver::BootRxCheckVersion(BYTE bDrop, BOOL &fSame)
{
	if( m_bData[2] == opReply ) {

		fSame = m_bData[4];

		return msgFrame;
	}

	return msgError;
}

UINT CGraphiteDriver::ConfigTxCheckVersion(BYTE bDrop, PBYTE pGuid)
{
	NewPacket(servConfig, configCheckVersion);

	AddData(pGuid, 16);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::ConfigTxClearConfig(BYTE bDrop)
{
	NewPacket(servConfig, configClearConfig);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::ConfigTxWriteConfig(BYTE bDrop, PCBYTE &pData)
{
	NewPacket(servData, dataData);

	for( ;;) {

		WORD Comm = MotorToHost((LPWORD(pData))[0]);

		WORD Data = MotorToHost((LPWORD(pData))[1]);

		if( Comm ) {

			if( (HIBYTE(Comm) & 0xC0) == CMD_WRITE ) {

				if( (HIBYTE(Comm) & 0x07) == 0x02 ) {

					if( m_bPtr < PACKET_SIZE - 3 ) {

						AddWord(Comm);

						AddByte(Data);
					}
					else
						break;
				}

				if( (HIBYTE(Comm) & 0x07) == 0x03 ) {

					if( m_bPtr < PACKET_SIZE - 4 ) {

						AddWord(Comm);

						AddWord(Data);
					}
					else
						break;
				}

				pData += 4;
			}

			if( (HIBYTE(Comm) & 0xC0) == CMD_SET ) {

				if( m_bPtr < PACKET_SIZE - 2 ) {

					AddWord(Comm);
				}
				else
					break;

				pData += 2;
			}

			if( (HIBYTE(Comm) & 0xC0) == CMD_CLEAR ) {

				if( m_bPtr < PACKET_SIZE - 2 ) {

					AddWord(Comm);
				}
				else
					break;

				pData += 2;
			}
		}
		else
			break;
	}

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::ConfigTxWriteVersion(BYTE bDrop, PBYTE pGuid)
{
	NewPacket(servConfig, configWriteVersion);

	AddData(pGuid, 16);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::ConfigTxStartSystem(BYTE bDrop)
{
	NewPacket(servConfig, configStartSystem);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::ConfigTxCheckStatus(BYTE bDrop)
{
	NewPacket(servConfig, configCheckStatus);

	return PutFrame(bDrop);;
}

UINT CGraphiteDriver::ConfigRxCheckVersion(BYTE bDrop, BOOL &fSame)
{
	if( m_bData[2] == opReply ) {

		fSame = m_bData[4];

		return msgFrame;
	}

	return msgError;
}

UINT CGraphiteDriver::ConfigRxWriteConfig(BYTE bDrop)
{
	if( m_bData[2] == dataData ) {

		return msgFrame;
	}

	return msgError;
}

UINT CGraphiteDriver::ConfigRxCheckStatus(BYTE bDrop, BOOL &fRun)
{
	if( m_bData[2] == opReply ) {

		fRun = m_bData[4];

		return msgFrame;
	}

	return msgError;
}

void CGraphiteDriver::DataTxStartFrame(void)
{
	NewPacket(servData, dataData);
}

BOOL CGraphiteDriver::DataTxWrite(WORD PropID, DWORD Data)
{
	if( (HIBYTE(PropID) & 0x07) == TYPE_BOOL ) {

		if( m_bPtr < PACKET_SIZE - 2 ) {

			if( Data ) {

				AddWord(PropID | (CMD_SET   << 8));
			}
			else {
				AddWord(PropID | (CMD_CLEAR << 8));
			}

			return TRUE;
		}

		return FALSE;
	}

	if( (HIBYTE(PropID) & 0x07) == TYPE_WORD ) {

		if( m_bPtr < PACKET_SIZE - 4 ) {

			AddWord(PropID | (CMD_WRITE << 8));

			AddWord(Data);

			return TRUE;
		}

		return FALSE;
	}

	if( (HIBYTE(PropID) & 0x07) == TYPE_BYTE ) {

		if( m_bPtr < PACKET_SIZE - 3 ) {

			AddWord(PropID | (CMD_WRITE << 8));

			AddByte(Data);

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

BOOL CGraphiteDriver::DataTxRead(WORD PropID)
{
	if( m_bPtr < PACKET_SIZE - 2 ) {

		AddWord(PropID | (CMD_READ << 8));

		return TRUE;
	}

	return FALSE;
}

UINT CGraphiteDriver::DataTxSend(BYTE bDrop)
{
	return PutFrame(bDrop);
}

UINT CGraphiteDriver::DataRxData(BYTE bDrop, PBYTE &pData)
{
	if( m_bData[2] == dataData ) {

		pData = m_bData + 4;

		return msgFrame;
	}

	return msgError;
}

BOOL CGraphiteDriver::DataTooFull(WORD PropID)
{
	if( (HIBYTE(PropID) & 0x07) == TYPE_BOOL ) {

		if( m_bPtr < PACKET_SIZE - 4 ) {

			return FALSE;
		}

		return TRUE;
	}

	if( (HIBYTE(PropID) & 0x07) == TYPE_WORD ) {

		if( m_bPtr < PACKET_SIZE - 6 ) {

			return FALSE;
		}

		return TRUE;
	}

	if( (HIBYTE(PropID) & 0x07) == TYPE_BYTE ) {

		if( m_bPtr < PACKET_SIZE - 5 ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}

UINT CGraphiteDriver::TunnelTx(BYTE bDrop, PBYTE pData)
{
	NewPacket(pData[0], pData[2]);

	AddData(pData + 4, PACKET_SIZE - 4);

	return PutFrame(bDrop);
}

UINT CGraphiteDriver::TunnelRx(BYTE bDrop, PBYTE pData)
{
	memcpy(pData, m_bData, PACKET_SIZE);

	return msgFrame;
}

UINT CGraphiteDriver::SendEnq(BYTE bDrop)
{
	UINT uMsg = m_pHandler->Transact(bDrop, msgEnq);

	switch( uMsg ) {

		case msgNone:
		case msgBusy:
		case msgNak:

			return uMsg;

		case msgFrame:

			m_pHandler->GetReply(bDrop, m_bData);

			m_pHandler->Transact(bDrop, msgAck);

			return msgFrame;

		default:
			return msgError;
	}
}

BOOL CGraphiteDriver::Recycle(BYTE bDrop)
{
	return m_pHandler->Recycle(bDrop);
}

// Frame Building

void CGraphiteDriver::NewPacket(BYTE bService, BYTE bOpcode)
{
	memset(m_bData, 0, PACKET_SIZE);

	m_bData[0] = bService;

	m_bData[1] = m_bSeq++;

	m_bData[2] = bOpcode;

	m_bData[3] = 0;

	m_bPtr     = 4;
}

void CGraphiteDriver::AddByte(BYTE bData)
{
	m_bData[m_bPtr++] = bData;
}

void CGraphiteDriver::AddWord(WORD wData)
{
	m_bData[m_bPtr++] = HIBYTE(wData);

	m_bData[m_bPtr++] = LOBYTE(wData);
}

void CGraphiteDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
}

void CGraphiteDriver::AddData(PBYTE pData, UINT uCount)
{
	memcpy(m_bData + m_bPtr, pData, uCount);

	m_bPtr += uCount;
}

// Transport

UINT CGraphiteDriver::PutFrame(BYTE bDrop)
{
	return m_pHandler->Transact(bDrop, m_bData, m_bPtr);
}

// End of File

