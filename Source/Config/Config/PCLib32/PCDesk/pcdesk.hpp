
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCDESK_HPP
	
#define	INCLUDE_PCDESK_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdialog.hpp>

#include <shlobj.h>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "pcdesk.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_PCDESK

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "pcdesk.lib")

#endif

/////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CItem;
class CViewWnd;
class CMainWnd;

//////////////////////////////////////////////////////////////////////////
//								
// Framework Messages
//

#define	WM_SETCURRENT	(WM_APP + 0x0400)
#define	WM_UPDATEUI	(WM_APP + 0x0401)
#define	WM_CHILDCLICK	(WM_APP + 0x0402)
#define	WM_LOADMENU	(WM_APP + 0x0403)
#define	WM_LOADTOOL	(WM_APP + 0x0404)
#define	WM_SHOWUI	(WM_APP + 0x0405)
#define WM_MAINMOVED	(WM_APP + 0x0406)

//////////////////////////////////////////////////////////////////////////
//								
// Dispatch Macros
//

#include "pcdmsg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDragHelper;
class CDropHelper;

//////////////////////////////////////////////////////////////////////////
//
// Drag Helper
//

class DLLAPI CDragHelper : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDragHelper(void);

		// Destructor
		~CDragHelper(void);

		// Operations
		void AddImage( IDataObject * pData,
			       CBitmap     & Bitmap,
			       CSize         Size,
			       CSize         Offset
			       );

		void AddEmpty( IDataObject *pData
			       );

	protected:
		// Data Members
		IDragSourceHelper *m_pHelp;
	};

//////////////////////////////////////////////////////////////////////////
//
// Drop Helper
//

class DLLAPI CDropHelper : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDropHelper(void);

		// Destructor
		~CDropHelper(void);
		
		// Binding
		void Bind(HWND hWnd, IDropTarget *pDrop);

		// Attributes
		BOOL IsProxyDrop(void) const;

		// Operations
		void DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		void DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		void DragLeave(void);
		void Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		void Show(BOOL fShow);

		// Location
		static IDropTarget * Find(CPoint Pos);

	protected:
		// Static Data
		static CDropHelper * m_pHead;
		static CDropHelper * m_pTail;
		
		// Data Members
		IDropTargetHelper  * m_pHelp;
		HWND                 m_hWnd;
		IDropTarget        * m_pDrop;
		BOOL                 m_fProxy;
		CDropHelper        * m_pNext;
		CDropHelper        * m_pPrev;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGadget;
class CSpacerGadget;
class CRidgeGadget;
class CButtonGadget;
class CMenuGadget;
class CSelectMenuGadget;
class CSplitMenuGadget;
class CStickyMenuGadget;
class CToggleMenuGadget;
class CHelpMenuGadget;
class CTextGadget;

//////////////////////////////////////////////////////////////////////////
//
// Gadget Flags
//

#define	tbgNone		0x00
#define	tbgPadding	0x01
#define	tbgCanBreak	0x02
#define	tbgForceBreak	0x04
#define	tbgSizeToFit	0x08
#define	tbgShowTip	0x10
#define	tbgRight	0x20

//////////////////////////////////////////////////////////////////////////
//
// Text Flags
//

#define	textNormal	0x0000
#define	textBold	0x0001
#define	textRed		0x0002
#define	textRight	0x0004

//////////////////////////////////////////////////////////////////////////
//
// Toolbar Flags
//

#define	barDark		0x01
#define	barTight	0x02
#define	barDouble	0x04
#define	barFlat		0x08
#define	barDrop		0x10
#define barPrivate	0x20

//////////////////////////////////////////////////////////////////////////
//
// Generic Toolbar Gadget
//

class DLLAPI CGadget : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CGadget(void);
		
		// Attributes
		UINT  GetID(void) const;
		PCTXT GetText(void) const;
		UINT  GetFlags(void) const;
		UINT  GetProps(void) const;
		BOOL  IsVisible(void) const;
		CSize GetSize(void) const;
		CRect GetRect(void) const;
		
		// Operations
		void SetWindow(CWnd &Wnd);
		void SetID(UINT uID);
		void SetText(PCTXT pText);
		void SetFlags(UINT uFlags);
		void SetRight(CGadget *pSource);
		void SetPosition(CPoint const &Pos);
		void ShowGadget(BOOL fShow);
		void SetHeight(int nHeight);
		void SetWidth(int nWidth);
		
		// Flag Helpers
		BOOL TestFlag(UINT uMask) const;
		void AdjustFlag(UINT uMask, BOOL fState);
		void SetFlag(UINT uMask);
		void ClearFlag(UINT uMask);
		
		// Overridables
		virtual void OnCreate(CDC &DC);
		virtual void OnSetPosition(void);
		virtual void OnShowGadget(void);
		virtual void OnPaint(CDC &DC);
		virtual BOOL OnMouseMenu(CPoint const &Pos);
		virtual BOOL OnMouseDown(CPoint const &Pos);
		virtual BOOL OnMouseDblClk(CPoint const &Pos);
		virtual void OnMouseMove(CPoint const &Pos, BOOL fHit);
		virtual void OnMouseUp(CPoint const &Pos, BOOL fHit);
		virtual void OnSetText(void);
		virtual void OnSetFlags(void);
		virtual BOOL OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip);
		virtual void OnIdle(CWnd *pPrivate);
		
		// Data Members
		CGadget *m_pNext;
		CGadget *m_pPrev;

	protected:
		// Data Members
		CWnd    *m_pWnd;
		UINT     m_uID;
		CString  m_Text;
		UINT     m_uFlags;
		UINT     m_uProps;
		BOOL     m_fShow;
		CSize    m_Size;
		CRect    m_Rect;
		
		// Implementation
		void Update(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Spacer Gadget
//

class DLLAPI CSpacerGadget : public CGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSpacerGadget(void);
		CSpacerGadget(int xSize);
		CSpacerGadget(int xSize, int ySize);

		// Overridables
		void OnPaint(CDC &DC);
	};

//////////////////////////////////////////////////////////////////////////
//
// Ridge Gadget
//

class DLLAPI CRidgeGadget : public CGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CRidgeGadget(int nWidth = 0);
		
		// Overridables
		void OnPaint(CDC &DC);
	};

//////////////////////////////////////////////////////////////////////////
//
// Button Gadget
//

class DLLAPI CButtonGadget : public CGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CButtonGadget(UINT uID, DWORD Image, PCTXT pText);
		CButtonGadget(UINT uID, DWORD Image, BOOL fRight);
		
		// Overridables
		void OnCreate(CDC &DC);
		void OnPaint(CDC &DC);
		BOOL OnMouseDown(CPoint const &Pos);
		void OnMouseMove(CPoint const &Pos, BOOL fHit);
		void OnMouseUp(CPoint const &Pos, BOOL fHit);
		BOOL OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip);
		void OnSetFlags(void);
		void OnIdle(CWnd *pPrivate);
		
	protected:
		// Data Members
		DWORD m_Image;

		// Protected Constructor
		CButtonGadget(void);

		// Overridables
		virtual void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toggle Gadget
//

class DLLAPI CToggleGadget : public CGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CToggleGadget(UINT uID, DWORD Image, BOOL fRight);
		
		// Overridables
		void OnCreate(CDC &DC);
		void OnPaint(CDC &DC);
		BOOL OnMouseDown(CPoint const &Pos);
		void OnMouseMove(CPoint const &Pos, BOOL fHit);
		void OnMouseUp(CPoint const &Pos, BOOL fHit);
		BOOL OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip);
		void OnSetFlags(void);
		void OnIdle(CWnd *pPrivate);
		
	protected:
		// Data Members
		DWORD m_Image;

		// Overridables
		virtual void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Menu Gadget
//

class DLLAPI CMenuGadget : public CButtonGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CMenuGadget(DWORD Image, CMenu &Menu, PCTXT pText, PCTXT pTip, BOOL fOnOff);

		// Destructor
		~CMenuGadget(void);
		
		// Overridlables
		BOOL OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip);
		void OnIdle(CWnd *pPrivate);

	protected:
		// Data Members
		CMenu   m_Menu;
		CString m_Tip;
		BOOL    m_fOnOff;
		
		// Overridables
		void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Any Menu Gadget
//

class DLLAPI CAnyMenuGadget : public CMenuGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CAnyMenuGadget(DWORD Image, CMenu &Menu, PCTXT pText, PCTXT pTip);

	protected:
		// Overridables
		void OnIdle(CWnd *pPrivate);
		void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Select Menu Gadget
//

class DLLAPI CSelectMenuGadget : public CMenuGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSelectMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip);

	protected:
		// Overridables
		void OnIdle(CWnd *pPrivate);
		void DoAction(void);

		// Implementation
		void CopyID(UINT uID, BOOL fEnable);
	};

//////////////////////////////////////////////////////////////////////////
//
// Split Menu Gadget
//

class DLLAPI CSplitMenuGadget : public CMenuGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSplitMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip, BOOL fOnOff);

		// Overridlables
		void OnCreate(CDC &DC);
		void OnSetPosition(void);
		void OnPaint(CDC &DC);
		BOOL OnMouseDown(CPoint const &Pos);
		void OnMouseMove(CPoint const &Pos, BOOL fHit);
		void OnMouseUp(CPoint const &Pos, BOOL fHit);

	protected:
		// Data Members
		CSize m_LSize;
		CSize m_RSize;
		CRect m_LRect;
		CRect m_RRect;
	};

//////////////////////////////////////////////////////////////////////////
//
// Sticky Menu Gadget
//

class DLLAPI CStickyMenuGadget : public CSplitMenuGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CStickyMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip);

	protected:
		// Overridables
		void OnIdle(CWnd *pPrivate);
		void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toggle Menu Gadget
//

class DLLAPI CToggleMenuGadget : public CSplitMenuGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CToggleMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip);

	protected:
		
		// Overridables
		void OnIdle(CWnd *pPrivate);
		void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Help Menu Gadget
//

class DLLAPI CHelpMenuGadget : public CSplitMenuGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CHelpMenuGadget(CMenu &Menu, PCTXT pText, PCTXT pTip);

		// Overidables
		void OnPaint(CDC &DC);
		BOOL OnMouseDown(CPoint const &Pos);

	protected:
		// Overridables
		void OnIdle(CWnd *pPrivate);
		void DoAction(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text Panel Gadget
//

class DLLAPI CTextGadget : public CGadget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CTextGadget(UINT uID, int nWidth, UINT uFormat);
		CTextGadget(UINT uID, PCTXT pSet, UINT uFormat);
		
		// Overridables
		void OnPaint(CDC &DC);
		BOOL OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip);
		void OnSetText(void);
		void OnSetFlags(void);
		
	protected:
		// Data Members
		UINT m_uFormat;
		BOOL m_fClip;
		
		// Implementation
		CString CalcText(CDC &DC, int nMax);
		BOOL    IsBold(void);
		BOOL    IsRed(void);
		BOOL	IsRight(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CGadgetHolderWnd;
class CGadgetBarWnd;
class CToolbarWnd;
class CStatusWnd;

//////////////////////////////////////////////////////////////////////////
//
// Generic Gadget Holder
//

class DLLAPI CGadgetHolderWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGadgetHolderWnd(void);
		
		// Destructor
		~CGadgetHolderWnd(void);
		
		// Operations
		void AddGadget(CGadget *pGadget);
		void AddFromMenu(CMenu &Menu, BOOL fEnd);
		void PollGadgets(void);
		void SetSemiModal(void);

		// Gadget Location
		CGadget * GetGadget(UINT uID) const;

	protected:
		// Static Data
		static UINT timerCheck;
		
		// Data Members
		UINT       m_uFlags;
		CWnd     * m_pParent;
		CMainWnd * m_pMainWnd;
		CGadget  * m_pHead;
		CGadget  * m_pTail;
		CGadget  * m_pCurrent;
		CToolTip * m_pToolTip;
		BOOL	   m_fToolInit;
		BOOL       m_fCapture;

		// Drop Context
		CDropHelper m_DropHelp;
		CGadget   * m_pDrop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnDestroy(void);
		void OnRButtonDblClk(UINT uFlags, CPoint Pos);
		void OnRButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		
		// Implementation
		BOOL CheckForHide(void);
		BOOL FindCurrent(CPoint Pos);
		void ClearCurrent(void);
		BOOL PaintGadgets(CPaintDC &DC);
		void CreateGadgets(void);
		void AddToolTips(void);
		void UpdateToolTips(void);
		BOOL IsSemiModal(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Gadget Bar Window
//

class DLLAPI CGadgetBarWnd : public CGadgetHolderWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGadgetBarWnd(void);
		
		// Destructor
		~CGadgetBarWnd(void);
		
		// Creation
		BOOL Create(CRect const &Rect, CWnd &Parent);
		
		// Attributes
		int GetHeight(void);

	protected:
		// Data Members
		int   m_nHeight;
		CRect m_Border;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
		
		// Implementation
		void CalcHeight(void);
		void CalcPositions(void);
		int  SizeToFit(CGadget *pScan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Toolbar Window
//

class DLLAPI CToolbarWnd : public CGadgetBarWnd, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CToolbarWnd(UINT uFlags);

		// Attributes
		int GetWidth(void) const;

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		
	protected:
		// Static Data
		static UINT timerSelect;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPaint(void);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Implementation
		BOOL CanTakeDrop(void);
		void SetDrop(CPoint Pos);
		void SetDrop(CGadget *pDrop);
	};

//////////////////////////////////////////////////////////////////////////
//
// Status Bar Window
//

class DLLAPI CStatusWnd : public CGadgetBarWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStatusWnd(void);
		
		// Attributes
		BOOL InPromptMode(void) const;
		
		// Operations
		void LockPrompt(BOOL fLock);
		void SetPrompt(PCTXT pText);
		
	protected:
		// Data Members
		UINT    m_uLock;
		BOOL    m_fPrompt;
		CString m_PromptText;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPaint(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CStdToolbarDialog;

//////////////////////////////////////////////////////////////////////////
//
// Standard Dialog with Toolbar
//

class DLLAPI CStdToolbarDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CStdToolbarDialog(void);

		// Destructor
		~CStdToolbarDialog(void);

	public:
		// Data Members
		CToolbarWnd *m_pTB;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnGoingIdle(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCmd;
class CCmdMulti;
class CViewWnd;
class CProxyViewWnd;
class CDummyViewWnd;

//////////////////////////////////////////////////////////////////////////
//								
// Multi Navigation
//

enum
{
	navOne  = 0,
	navSeq  = 1,
	navAll  = 2,
	navLock = 3,
	};

//////////////////////////////////////////////////////////////////////////
//								
// Routing Codes
//

#define	RC_CURRENT	0x0000
#define	RC_FIRST	0x0001
#define	RC_ALL		0x0002
#define	RC_PRIME	0x0004
#define	RC_LOCAL1	0x0100
#define	RC_LOCAL2	0x0200
#define RC_BCAST	0x0400

//////////////////////////////////////////////////////////////////////////
//
// Generic Command
//

class DLLAPI CCmd : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmd(void);

		// Data Members
		CString m_Menu;
		UINT    m_View;
		UINT	m_Side;
		CString m_Item;
	};

//////////////////////////////////////////////////////////////////////////
//
// Grouped Command
//

class DLLAPI CCmdMulti : public CCmd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCmdMulti(CString Item, CString Menu, UINT uCode);

		// Data Members
		UINT m_uCode;
	};

//////////////////////////////////////////////////////////////////////////
//
// View Window
//

class DLLAPI CViewWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CViewWnd(void);
		
		// Attributes
		CItem * GetItem(void) const;
		BOOL    IsItem(CItem *pItem) const;
		BOOL	CanRecycle(void) const;
		BOOL    IsCurrent(void) const;
		CString GetNavPos(void) const;
		
		// Operations
		BOOL Attach(CItem *pItem);
		BOOL Detach(void);
		void SetCurrent(BOOL fCurrent);
		BOOL Navigate(CString const &Nav);
		void ExecCmd(CCmd *pCmd);
		void UndoCmd(CCmd *pCmd);

	protected:
		// Data Members
		CItem * m_pItem;
		BOOL	m_fRecycle;
		BOOL    m_fCurrent;
		DWORD   m_crc;

		// Overribables
		virtual void    OnAttach(void);
		virtual BOOL    OnPreDetach(void);
		virtual void    OnDetach(void);
		virtual CString OnGetNavPos(void);
		virtual BOOL    OnNavigate(CString const &Nav);
		virtual void    OnExec(CCmd *pCmd);
		virtual void    OnUndo(CCmd *pCmd);
		
		// Message Procedures
		LRESULT WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnLButtonDown(UINT uCode, CPoint Pos);
		void OnSetCurrent(BOOL fCurrent);
		
		// Routing Codes
		UINT GetRoutingCode(MSG const &Msg);
	};

//////////////////////////////////////////////////////////////////////////
//
// Proxy View Window
//

class DLLAPI CProxyViewWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CProxyViewWnd(void);

		// Destructor
		~CProxyViewWnd(void);

	protected:
		// Data Members
		CViewWnd * m_pView;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Overribables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnSetFocus(CWnd &Wnd);
		void OnSetCurrent(BOOL fCurrent);
		void OnSize(UINT uCode, CSize Size);
		void OnShowWindow(BOOL fShow, UINT uStatus);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dummy View Window
//

class DLLAPI CDummyViewWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CDummyViewWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnKillFocus(CWnd &Wnd);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);
		void OnRButtonDown(UINT uCode, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnLoadMenu(UINT uID, CMenu &Menu);
		void OnLoadTool(UINT uID, CMenu &Tool);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSplitViewWnd;
class CHorzSplitViewWnd;
class CVertSplitViewWnd;
class CTripleViewWnd;

//////////////////////////////////////////////////////////////////////////
//
// Generic Split View Window
//

class DLLAPI CSplitViewWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CSplitViewWnd(void);

	protected:
		// Data Members
		CAccelerator m_Accel;
		CCursor      m_Cursor;
		BOOL         m_fUse[2];
		CViewWnd *   m_pWnd[2];
		CRect        m_Rect[3];
		UINT         m_uFocus;
		BOOL	     m_fFixed;
		int          m_nTotal;
		int          m_nPercent;
		int          m_nBarPos;
		int          m_nTrackOrg;
		int          m_nTrackPos; 
		BOOL	     m_fCapture;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerator
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnCancelMode(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnSetCurrent(BOOL fCurrent);
		
		// Command Handlers
		BOOL OnWindowControl(UINT uID, CCmdSource &Src);
		BOOL OnWindowCommand(UINT uID);

		// Overridables
		virtual void CalcWndRects(void)  = 0;
		virtual void FindBestSplit(void) = 0;

		// Implementation
		void MoveWindows(void);
		BOOL SelectFocus(UINT uFocus);
		BOOL Select(UINT uFocus);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Horizontal Split View Window
//

class DLLAPI CHorzSplitViewWnd : public CSplitViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CHorzSplitViewWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnSize(UINT uCode, CSize Size);
		
		// Command Handlers
		BOOL OnWindowControl(UINT uID, CCmdSource &Src);
		BOOL OnWindowCommand(UINT uID);
		
		// Overridables
		void CalcWndRects(void);
		void FindBestSplit(void);

		// Implementation
		void StartAdjust(int nPos);
		BOOL TrackAdjust(int nPos);
		void EndAdjust(void);
		void AbortAdjust(void);
		void ShowTracking(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Vertical Split View Window
//

class DLLAPI CVertSplitViewWnd : public CSplitViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CVertSplitViewWnd(void);

	protected:
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnSize(UINT uCode, CSize Size);
		
		// Command Handlers
		BOOL OnWindowControl(UINT uID, CCmdSource &Src);
		BOOL OnWindowCommand(UINT uID);
		
		// Overridables
		void CalcWndRects(void);
		void FindBestSplit(void);

		// Implementation
		void StartAdjust(int nPos);
		BOOL TrackAdjust(int nPos);
		void EndAdjust(void);
		void AbortAdjust(void);
		void ShowTracking(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Triple View Window
//

class DLLAPI CTripleViewWnd : public CViewWnd, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CTripleViewWnd(void);
		
		// Attributes
		BOOL IsSemiModal(void) const;
		BOOL InTwinMode(void) const;
		BOOL InFloater(void) const;

		// Operations
		void SetView(UINT uView, CViewWnd *pView);
		void SetTwinMode(BOOL fTwin);
		void AddFloater(CWnd *pView);
		void RemFloater(CWnd *pView);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Static Data
		static UINT m_timerCheck;
		static UINT m_timerSelect;

		// Floating View List
		typedef CList <CWnd *> CFloatList;

		// Show Codes
		enum
		{
			showHidden = 0,
			showSlide  = 1,
			showLocked = 2
			};

		// Data Members
		CString	     m_KeyName;
		CAccelerator m_Accel;
		CCursor      m_Cursor;
		UINT	     m_uShow[2];
		CViewWnd *   m_pWnd [3];
		CRect        m_Rect [3];
		CRect	     m_Line [2];
		int          m_nBar [2];
		UINT         m_uFocus;
		UINT	     m_uHover;
		UINT	     m_uCapture;
		BOOL	     m_fPress;
		BOOL	     m_fAnim;
		BOOL	     m_fSemi;
		int          m_nTrackOrg;
		int          m_nTrackPos;
		int          m_nTrackMin;
		int          m_nTrackMax;
		UINT	     m_uTwin;
		UINT	     m_uSaveShow[2];
		int	     m_nSaveBar [2];
		CFloatList   m_Float;
		
		// Drop Context
		CDropHelper  m_DropHelp;
		UINT         m_uDrop;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerator
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnPreDestroy(void);
		void OnActivateApp(BOOL fActive, DWORD dwThread);
		void OnCancelMode(void);
		void OnSize(UINT uCode, CSize Size);
		void OnGetMetric(UINT uCode, CSize &Size);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnSetFocus(CWnd &Wnd);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnSetCurrent(BOOL fCurrent);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		
		// Command Handlers
		BOOL OnWindowControl(UINT uID, CCmdSource &Src);
		BOOL OnWindowCommand(UINT uID);
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Drawing Helpers
		void DrawSideBar(CDC &DC, UINT n);
		void ShowDragBar(CDC &DC);

		// Routing Codes
		UINT GetRoutingCode(MSG const &Msg);

		// Overridables
		virtual void OnUpdateLayout(void);

		// Implementation
		void CreateView(UINT uView);
		void CalcWndRects(void);
		UINT CheckHover(CPoint Pos);
		BOOL CancelSlideOut(void);
		void UpdateLayout(UINT uPane);
		void MoveWindows(BOOL fAnimate);
		void Animate(UINT uWnd, BOOL fShow);
		BOOL IsAnimateBroken(void);
		BOOL SelectFocus(UINT uFocus);
		BOOL Select(UINT uFocus);
		void FindConfig(CRegKey &Key);
		void LoadConfig(void);
		void SaveConfig(void);
		void SetDrop(CPoint Pos);
		void SetDrop(UINT uDrop);
		int  GetThirdWidth(void);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMultiViewWnd;
class CAutoMultiViewWnd;

//////////////////////////////////////////////////////////////////////////
//
// Tabbed View Window
//

class DLLAPI CMultiViewWnd : public CViewWnd, public IDropTarget
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CMultiViewWnd(void);

		// Destructor
		~CMultiViewWnd(void);

		// Attributes
		UINT    GetTabIndex(void) const;
		CString GetTabLabel(void) const;
		
		// Operations
		void AddView(PCTXT pLabel, CViewWnd *pView);
		void SetTabStyle(DWORD dwStyle);
		void SetMargin(int nMargin);
		BOOL SelectTab(UINT uTab);
		BOOL SelectTab(CString Tab);
		
		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

	protected:
		// Static Data
		static UINT m_timerSelect;

		// Typedefs
		typedef CArray <CViewWnd *> CViewList;

		// Data Members
		CAccelerator m_Accel;
		DWORD	     m_dwStyle;
		DWORD	     m_dwMade;
		int	     m_nMargin;
		UINT         m_uFocus;
		CViewList    m_Views;
		CStringArray m_Label;
		CTabCtrl *   m_pTab;
		CRect	     m_ViewRect;
		CRect	     m_TabRect;

		// Drop Context
		CDropHelper  m_DropHelp;
		UINT         m_uDrop;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnCancelMode(void);
		void OnPaint(void);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnGetMinMaxInfo(MINMAXINFO &Info);
		void OnSize(UINT uCode, CSize Size);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnSetFocus(CWnd &Wnd);
		void OnFocusNotify(UINT uID, CWnd &Wnd);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnSetCurrent(BOOL fCurrent);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnPrint(CDC &DC, UINT uFlags);

		// Notification Handlers
		void OnSelChange(UINT uID, NMHDR &Info);

		// Command Handlers
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);
		
		// Implementation
		void CreateTabCtrl(void);
		void CreateView(UINT uView);
		void CalcLayout(void);
		void MoveWindows(void);
		BOOL SelectFocus(UINT uFocus);
		BOOL Select(UINT uFocus);
		void SetDrop(CPoint Pos);
		void SetDrop(UINT uDrop);
	};		

//////////////////////////////////////////////////////////////////////////
//
// Tabbed View Window with Auto Attach
//

class DLLAPI CAutoMultiViewWnd : public CMultiViewWnd
{
		// Dynamic Class
		AfxDeclareDynamicClass();

	protected:
		// Overribables
		void OnAttach(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDialogViewWnd;

//////////////////////////////////////////////////////////////////////////
//
// Dialog View Window
//

class DLLAPI CDialogViewWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CDialogViewWnd(void);

		// Destructor
		~CDialogViewWnd(void);

		// Operations
		BOOL IsDialogMessage(MSG &Msg);
		void SetRadioGroup(UINT uFrom, UINT uTo, UINT uData);
		UINT GetRadioGroup(UINT uFrom, UINT uTo) const;
		void SetDlgFocus(CWnd &Wnd);
		void SetDlgFocus(UINT uID);

		// Modeless Translation
		static BOOL IsModelessMessage(MSG &Msg);

	protected:
		// Static Data
		static CDialogViewWnd *m_pHead;
		static CDialogViewWnd *m_pTail;
		
		// Data Members
		CDialogViewWnd *m_pNext;
		CDialogViewWnd *m_pPrev;

		// Message Filter
		virtual BOOL FilterMessage(MSG &Msg);
		
		// Message Procedures
		LRESULT DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Default Class Definition
		BOOL GetClassDetails(WNDCLASSEX &Class) const;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Msg, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);

		// Implementation
		void ModelessListAppend(void);
		void ModelessListRemove(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMenuWnd;
class CFrameWnd;
class CRecentList;
class CMainWnd;

//////////////////////////////////////////////////////////////////////////
//
// Window with Menus
//

class DLLAPI CMenuWnd : public CWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMenuWnd(void);

	protected:
		// Data Members
		BOOL m_fMouse;
		UINT m_uLast;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
		void OnSysCommand(UINT uID, CPoint Pos);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu);
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Measure);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);

		// Implementation
		void DrawMenu(CDC &DC, DRAWITEMSTRUCT &Item);
		void DrawStdMenu(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState);
		void DrawTopMenu(CDC &DC, CMenuInfo *pInfo, CRect Rect, UINT uState);
		void DrawMenuImage(CDC &DC, CRect const &Rect, UINT uImage, UINT uState);
		void DrawStdBack(CDC &DC, CRect &Rect, CRect &Wide, UINT uState);
		void DrawTopBack(CDC &DC, CRect &Rect, UINT uState);
		void FakeKey(UINT uCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Frame Window
//

class DLLAPI CFrameWnd : public CMenuWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFrameWnd(void);
		
		// Attributes
		CViewWnd * GetView(void) const;
		
		// Operations
		void SetView(CViewWnd *pView);

		// Client Calculation
		virtual CRect GetClientRect(void) const;

	protected:
		// Data Members
		CViewWnd *m_pView;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// View Creation
		virtual void OnCreateView(void);
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnCancelMode(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnMove(CPoint Pos);
		void OnSetFocus(CWnd &Wnd);
		void OnSize(UINT uCode, CSize Size);
		
		// Implementation
		void SizeView(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Recent File List
//

class DLLAPI CRecentList : public CObject
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CRecentList(void);

		// Attributes
		UINT	  GetBaseID(void) const;
		UINT	  GetCount(void) const;
		CFilename GetFile(UINT uIndex) const;
		CFilename GetFileFromID(UINT ID) const;
		UINT	  GetLimit(void) const;

		// Operations
		void SetBaseID(UINT BaseID);
		void SetLimit(UINT uLimit);
		void AddFile(PCTXT pFile);
		void RemoveFile(PCTXT pFile);

		// Persistance
		void LoadList(PCTXT pName = NULL);
		void SaveList(PCTXT pName = NULL);

		// Menu Operations
		void UpdateMenu(CMenu &Menu, UINT After);

	protected:
		// Data Members
		UINT		m_BaseID;
		CStringArray	m_List;
		UINT		m_uLimit;

		// Implementation
		BOOL    Remove(CMenu &Menu, UINT After);
		void    Append(CMenu &Menu, UINT After);
		CRegKey FindKey(PCTXT pName);
	};

//////////////////////////////////////////////////////////////////////////
//
// Main Application Window
//

class DLLAPI CMainWnd : public CFrameWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMainWnd(void);
		
		// Destructor
		~CMainWnd(void);
		
		// Attributes
		BOOL      IsMenuActive(void) const;
		BOOL      IsEdgeVisible(void) const;
		BOOL      IsSemiModal(void) const;
		CDialog * GetSemiModal(void) const;
		CString   GetStatus(void) const;
		UINT	  GetBalloonMode(void) const;
		BOOL	  AllowGhostBar(void) const;
		
		// Operations
		void SetSemiModal(CDialog *pSemi);
		void LockPrompt(BOOL fLock);
		void SetPrompt(PCTXT pPrompt);
		void SetPrompt(UINT  uID);
		void SetStatus(PCTXT pStatus);
		void SetOverwrite(BOOL fOver);
		void ShowToolbar(BOOL fShow);
		void ShowStatusBar(BOOL fShow);
		void ShowEdge(BOOL fShow);
		BOOL ForceUI(void);
		void AddStickyPopup(CWnd *pWnd);
		void RemoveStickyPopup(CWnd *pWnd);

		// Client Calculation
		virtual CRect GetClientRect(void) const;
		
	protected:
		// Static Data
		static UINT timerLocks;
		static UINT timerPrompt;
		static UINT timerUpdate;

		// Data Members
		BOOL           m_fSemi;
		CDialog      * m_pSemi;
		UINT	       m_uBalloon;
		CToolbarWnd  * m_pTB;
		CStatusWnd   * m_pSB;
		BOOL           m_fShowTB;
		BOOL	       m_fShowGB;
		BOOL           m_fShowSB;
		CRect          m_TBRect;
		CRect          m_SBRect;
		CRecentList    m_Recent;
		BOOL           m_fMenu;
		BOOL           m_fEdge;
		CString        m_Prompt;
		BOOL           m_fUpdate;
		BOOL	       m_fIconic;
		CBitmap	       m_MenuPad;
		CArray <CWnd*> m_Sticky;
		CRect	       m_OldRect;
		
		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Interface Control
		virtual void OnUpdateInterface(void);
		virtual void OnCreateStatusBar(void);
		virtual void OnUpdateStatusBar(void);
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnDestroy(void);
		void OnActivateApp(BOOL fActive, DWORD dwThread);
		void OnGetStatusBar(CString &Text);
		void OnGoingIdle(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu);
		void OnPaint(void);
		void OnSetStatusBar(PCTXT pText);
		void OnWindowPosChanging(WINDOWPOS &Pos);
		void OnWindowPosChanged(WINDOWPOS &Pos);
		void OnSize(UINT uCode, CSize Size);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);
		void OnFocusNotify(UINT uID, CWnd &Child);
		void OnUpdateUI(void);
		UINT OnNCHitTest(CPoint Pos);
		void OnSysColorChange(void);
		
		// View Menu
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);
		void OnViewRefresh(void);
		void OnViewCycleLang(void);

		// Help Menu
		BOOL OnHelpControl(UINT uID, CCmdSource &Src);
		BOOL OnHelpCommand(UINT uID);
		
		// Command Information
		BOOL OnGetInfo(UINT uID, CCmdInfo &Info);

		// Implementation
		void CalcPositions(void);
		void CreateStatusBar(void);
		void UpdateStatusBar(void);
		void SendReSize(void);
		void ClearToolbar(void);
		void UpdateToolbar(void);
		void GetConfig(void);
		void PutConfig(void);
		void DoUpdateUI(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMdiClientWnd;
class CMdiFrameWnd;
class CMdiChildWnd;

//////////////////////////////////////////////////////////////////////////
//
// Child Window List
//

typedef CList <CMdiChildWnd *> CMdiList;

//////////////////////////////////////////////////////////////////////////
//
// Child Management Codes
//

enum ChildManagement
{
	childCreate,
	childDestroy,
	childActivate,
	childDeactivate,
	childZoomed

	};

//////////////////////////////////////////////////////////////////////////
//
// MDI Client Window
//

class DLLAPI CMdiClientWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
		
		// Constructor
		CMdiClientWnd(void);

                // Creation
		BOOL Create(CMdiFrameWnd &Parent);
		
		// Active Child
		CMdiChildWnd * GetActiveChild(void) const;

		// List Access
		CMdiList const & GetMdiList(void) const;
		
		// Attributes
		HMENU FindWindowMenu(CMenu &MenuBar) const;

		// Child Operations
		void ShowChild(CMdiChildWnd &Child);
		void ActivateChild(CMdiChildWnd &Child);
		void DestroyChild(CMdiChildWnd &Child);
		void MaximizeChild(CMdiChildWnd &Child);
		void RestoreChild(CMdiChildWnd &Child);
		void NextChild(CMdiChildWnd &Child);
		void PrevChild(CMdiChildWnd &Child);
		
		// Frame Operations
		void SetParentMenu(CMenu &Menu);
		void RefreshParentMenu(void);
		void ShowParentEdge(BOOL fEdge);
		void ShowParentEdge(void);

		// Child Callback
		void ChildNotify(UINT uCode, CMdiChildWnd *pChild);

	protected:
	        // Data Members
	        CMdiChildWnd *m_pActive;
	        UINT          m_uCount;
		CMdiList      m_MdiList;

		// Default Class Name
		PCTXT GetDefaultClassName(void) const;

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		void OnKeyDown(UINT uCode, DWORD dwFlags);
		void OnFocusNotify(UINT uID, CWnd &Child);
		void OnGetMetric(UINT uCode, CSize &Size);
		void OnSetCurrent(BOOL fCurrent);
		void OnCancelMode(void);

		// Window Menu Routing
		BOOL OnWindowCommand(UINT uID);
		BOOL OnWindowControl(UINT uID, CCmdSource &Src);

		// Window Menu Handlers
		void OnCmdWindowCascade(void);
		void OnCmdWindowTile(void);
		void OnCmdWindowArrange(void);
		void OnCmdWindowCloseAll(void);

		// Window Menu Permissions
		BOOL CanWindowCascade(void) const;
		BOOL CanWindowTile(void) const;
		BOOL CanWindowArrange(void) const;
		BOOL CanWindowCloseAll(void) const;

		// Command Information
		BOOL OnGetInfo(UINT uID, CCmdInfo &Info);
		
		// Implementation
		BOOL SelectActive(CMdiChildWnd *pChild);
		BOOL Select(CMdiChildWnd *pChild);
		void SetChildCurrent(BOOL fCurrent);
	};

//////////////////////////////////////////////////////////////////////////
//
// MDI Frame Window
//

class DLLAPI CMdiFrameWnd : public CMainWnd
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMdiFrameWnd(void);
		
		// Active Child
		CMdiChildWnd * GetActiveChild(void) const;

		// List Access
		CMdiList const & GetMdiList(void) const;
		
		// Attributes
		CMdiClientWnd & GetClientWnd(void) const;
		
		// Child Operations
		void ShowChild(CMdiChildWnd &Child);
		void ActivateChild(CMdiChildWnd &Child);
		void DestroyChild(CMdiChildWnd &Child);
		void MaximizeChild(CMdiChildWnd &Child);
		void RestoreChild(CMdiChildWnd &Child);
		void NextChild(CMdiChildWnd &Child);
		void PrevChild(CMdiChildWnd &Child);

		// Frame Operations
		void SetMenu(CMenu &Menu);
		void RefreshMenu(void);
		
	protected:
		// Data Members
		CMdiClientWnd *m_pClient;
	
		// Message Procedures
		LRESULT DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam);

		// View Creation
		void OnCreateView(void);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl);
	};

//////////////////////////////////////////////////////////////////////////
//
// MDI Child Window
//

class DLLAPI CMdiChildWnd : public CFrameWnd
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMdiChildWnd(void);
		
		// Creation
		BOOL Create(PCTXT pName, DWORD dwStyle, CRect const &Rect, CMdiFrameWnd &Parent);
		
		// Destruction
		void DestroyWindow(void);
		
		// Attributes
		BOOL IsZoomed(void) const;
		BOOL IsIconic(void) const;
		
		// Operations
		void MaximizeWindow(void);
		void RestoreWindow(void);
		void SelectNextWindow(void);
		
		// Client Calculation
		CRect GetClientRect(void) const;

	protected:
		// Other Windows
		CMdiFrameWnd  *m_pFrame;
		CMdiClientWnd *m_pClient;
		
		// Internal State
		BOOL m_fZoomed;
		BOOL m_fIconic;
		BOOL m_fEatAct;
		BOOL m_fEatNow;
		
		// Message Procedures
		LRESULT DefProc(UINT uMessage, WPARAM wParam, LPARAM lParam);
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		UINT OnCreate(CREATESTRUCT &Create);
		void OnDestroy(void);
		void OnMdiActivate(CWnd &Wnd, CWnd &Prev);
		void OnMenuSelect(UINT uID, UINT uFlags, CMenu &Menu);
		UINT OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage);
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
	};

// End of File

#endif
