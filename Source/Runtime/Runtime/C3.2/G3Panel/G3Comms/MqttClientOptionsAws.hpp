
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsAws_HPP

#define	INCLUDE_MqqtClientOptionsAws_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client for AWS Options
//

class CMqttClientOptionsAws : public CMqttClientOptionsJson
{
	public:
		// Constructor
		CMqttClientOptionsAws(void);

		// Destructor
		~CMqttClientOptionsAws(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Config Fixup
		BOOL FixConfig(void);

		// Attributes
		CString GetExtra(void) const;

		// Data Members
		UINT    m_Shadow;
		CString m_Pub;
		CString m_Sub;
};

// End of File

#endif
