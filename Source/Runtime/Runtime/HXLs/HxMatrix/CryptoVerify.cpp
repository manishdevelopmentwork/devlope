
#include "Intern.hpp"

#include "CryptoVerify.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Verifier
//

// Constructor

CCryptoVerify::CCryptoVerify(void)
{
	StdSetRef();

	m_uState = stateNew;
	
	m_pHash  = NULL;
	}

// Destructor

CCryptoVerify::~CCryptoVerify(void)
{
	AfxRelease(m_pHash);
	}

// IUnknown

HRESULT CCryptoVerify::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ICryptoVerify);

	StdQueryInterface(ICryptoVerify);

	return E_NOINTERFACE;
	}

ULONG CCryptoVerify::AddRef(void)
{
	StdAddRef();
	}

ULONG CCryptoVerify::Release(void)
{
	StdRelease();
	}

// ICryptoVerify

BOOL CCryptoVerify::Verify(PCBYTE pSig, UINT uSig, UINT uFormat)
{
	AfxAssert(FALSE);

	return FALSE;
}

BOOL CCryptoVerify::Initialize(PCBYTE pKey, UINT uKey)
{
	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CCryptoVerify::Initialize(CByteArray const &Key)
{
	return ((ICryptoVerify *) this)->Initialize(Key.GetPointer(), Key.GetCount());
	}

void CCryptoVerify::SetHash(ICryptoHash *pHash)
{
	AfxAssert(!m_pHash);

	m_pHash = pHash;
	}

BOOL CCryptoVerify::SetHash(PCSTR pName)
{
	AfxAssert(!m_pHash);

	return AfxNewObject(pName, ICryptoHash, m_pHash) == S_OK;
	}

void CCryptoVerify::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	AfxAssert(m_pHash);

	m_pHash->Update(pData, uData);
	}

void CCryptoVerify::Update(CByteArray const &Data)
{
	AfxAssert(m_uState == stateActive);

	AfxAssert(m_pHash);

	m_pHash->Update(Data);
	}

void CCryptoVerify::Update(CString const &Data)
{
	AfxAssert(m_uState == stateActive);

	AfxAssert(m_pHash);

	m_pHash->Update(Data);
	}

void CCryptoVerify::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	m_pHash->Finalize();

	m_uState = stateDone;
}

// End of File
