
#include "intern.hpp"

#include "events.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tag Manager
//

// Persist Timing

#define	timeTesting	FALSE

#define	timeScore	(timeTesting ? 2 : 360)

#define	timeLimit	(timeTesting ? 1 :  30)

#define	timeLoop	ToTicks(1000)

// Constructor

CTagManager::CTagManager(void)
{
	StdSetRef();

	m_LogToDisk = 0;
	m_FileLimit = 60;
	m_FileCount = 12;
	m_WithBatch = 0;
	m_SignLogs  = 0;
	m_LogToPort = 0;
	m_pTags     = New CTagList;
	m_pQuick    = NULL;
	m_pDiag	    = NULL;
	m_uProv	    = 0;
	m_Drive     = 0;

	DiagRegister();
	}

// Destructor

CTagManager::~CTagManager(void)
{
	DiagRevoke();

	delete m_pTags;
	}

// Initialization

void CTagManager::Load(PCBYTE &pData)
{
	ValidateLoad("CTagManager", pData);

	m_LogToDisk = GetByte(pData);

	m_FileLimit = GetWord(pData);

	m_FileCount = GetWord(pData);

	m_WithBatch = GetByte(pData);

	m_SignLogs  = GetByte(pData);

	m_pTags->Load(pData);

	m_LogToPort = GetByte(pData);

	m_Drive = GetByte(pData);

	MakeQuickList();
	}

// Task List

void CTagManager::GetTaskList(CTaskList &List)
{
	CTaskDef Task;

	if( m_QuickList.GetCount() ) {

		Task.m_pEntry = this;
		Task.m_Name   = "QUICK";
		Task.m_uID    = 2;
		Task.m_uCount = 1;
		Task.m_uLevel = 8200;
		Task.m_uStack = 0;

		List.Append(Task);
		}

	Task.m_pEntry = this;
	Task.m_Name   = "SCANNER";
	Task.m_uID    = 0;
	Task.m_uCount = 1;
	Task.m_uLevel = 2800;
	Task.m_uStack = 0;

	List.Append(Task);

	Task.m_pEntry = this;
	Task.m_Name   = "PERSIST";
	Task.m_uID    = 1;
	Task.m_uCount = 1;
	Task.m_uLevel = 3000;
	Task.m_uStack = 0;

	List.Append(Task);
	}

// Task Entries

void CTagManager::TaskInit(UINT uID)
{
	switch( uID ) {

		case 0: ScannerInit  (); break;
		case 1: PersistInit  (); break;
		case 2: QuickPlotInit(); break;
		}
	}

void CTagManager::TaskExec(UINT uID)
{
	switch( uID ) {

		case 0: ScannerExec  (); break;
		case 1: PersistExec  (); break;
		case 2: QuickPlotExec(); break;
		}
	}

void CTagManager::TaskStop(UINT uID)
{
	}

void CTagManager::TaskTerm(UINT uID)
{
	switch( uID ) {

		case 0: ScannerTerm  (); break;
		case 1: PersistTerm  (); break;
		case 2: QuickPlotTerm(); break;
		}
	}

// Operations

void CTagManager::Commit(void)
{
	m_uCommit = 1;
	}

void CTagManager::CommitAndReset(void)
{
	m_uCommit = 2;
	}

void CTagManager::LockQuickPlots(void)
{
	if( m_pQuick ) {

		m_pQuick->Wait(FOREVER);
		}
	}

void CTagManager::FreeQuickPlots(void)
{
	if( m_pQuick ) {

		m_pQuick->Free();
		}
	}

// IEventSource

BOOL CTagManager::GetEventText(CUnicode &Text, UINT uIndex)
{
	UINT   uTag = LOWORD(uIndex);

	CTag * pTag = m_pTags->GetItem(uTag);

	if( pTag ) {

		UINT uItem = HIBYTE(HIWORD(uIndex));

		UINT uPos  = LOBYTE(HIWORD(uIndex));

		return pTag->GetEventText(Text, uItem, uPos);
		}

	return FALSE;
	}

void CTagManager::AcceptAlarm(UINT Method, UINT Code)
{
	}

void CTagManager::AcceptAlarm(CActiveAlarm *pInfo)
{
	CAlarmManager * pAlarms = CCommsSystem::m_pThis->m_pAlarms;

	UINT            uIndex  = pInfo->m_Code;
	
	pAlarms->AcceptAlarm(uIndex);
	}

// IUnknown

HRESULT CTagManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CTagManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CTagManager::Release(void)
{
	StdRelease();
	}

// IDiagProvider

UINT CTagManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	UINT uCmd;

	switch( (uCmd = pCmd->GetCode()) ) {

		case 1:
			return DiagList(pOut, pCmd);

		case 2:
			return DiagGet(pOut, pCmd);

		case 3:
			return DiagSet(pOut, pCmd);
		}

	return 0;
	}

// Quick Plot Task

void CTagManager::QuickPlotInit(void)
{
	for( UINT n = 0; n < m_QuickList.GetCount(); n++ ) {

		CTagQuickPlot *pQuick = m_QuickList[n];

		pQuick->Init();
		}

	m_pQuick = Create_Mutex();
	}

void CTagManager::QuickPlotExec(void)
{
	// REV3 -- Sleep needed for startup. We ought to make sure
	// the on-power-up action runs before anything else, and in
	// fact it ought to be moved away from the UI section.

	Sleep(1000);

	for(;;) {

		UINT uTime = GetTickCount();

		m_pQuick->Wait(FOREVER);

		for( UINT n = 0; n < m_QuickList.GetCount(); n++ ) {

			CTagQuickPlot *pQuick = m_QuickList[n];

			pQuick->Poll();
			}

		m_pQuick->Free();

		// REV3 -- We only need 500 when using an external timebase.

		UINT uGone = GetTickCount() - uTime;

		UINT uLeft = ToTicks(500)   - uGone;

		Sleep(ToTime(uLeft));
		}
	}

void CTagManager::QuickPlotTerm(void)
{
	m_pQuick->Release();
	}

// Scanner Task

void CTagManager::ScannerInit(void)
{
	CCommsSystem::m_pThis->m_pEvents->Register(this, "C3");

	m_pTags->SetPollScan(TRUE);
	}

void CTagManager::ScannerExec(void)
{
	CEventLogger::m_pThis->LogLoad();

	CCodedItem * pOnApply  = CCommsSystem::m_pThis->m_pComms->m_pOnApply;

	CCodedItem * pOnStart  = CCommsSystem::m_pThis->m_pComms->m_pOnStart;

	CCodedItem * pOnSecond = CCommsSystem::m_pThis->m_pComms->m_pOnSecond;

	if( pOnApply ) {
		
		pOnApply->SetScan(scanTrue);

		RunAction(pOnApply, 10000);
		}

	if( pOnStart ) {

		pOnStart->SetScan(scanTrue);

		RunAction(pOnStart, 10000);
	}

	if( pOnSecond ) {
		
		pOnSecond->SetScan(scanTrue);
		}

	DWORD uSecs1 = GetNow();

	UINT  uTime1 = GetTickCount();

	for(;;) {

		if( pOnSecond ) {
		
			UINT uSecs2 = GetNow();

			if( uSecs2 != uSecs1 ) {

				RunAction(pOnSecond, 0);

				uSecs1 = uSecs2;
				}
			}

		UINT uTime2 = GetTickCount();

		UINT uDelta = uTime2 - uTime1;

		m_pTags->Poll(ToTime(uDelta));

		Sleep(100);

		uTime1 = uTime2;
		}
	}

void CTagManager::ScannerTerm(void)
{
	}

// Persist Task

void CTagManager::PersistInit(void)
{
	m_uCommit = 0;
	}

void CTagManager::PersistExec(void)
{
	UINT uScore = 1;

	UINT uDelay = timeLimit;

	UINT uTimer = 0;

	UINT uLoop  = ToTicks(1000);

	UINT uTime1 = GetTickCount() + timeLoop;

	for(;;) {

		UINT uTime2 = GetTickCount();

		while( uTime2 >= uTime1 ) {

			if( m_uCommit ) {

				if( m_uCommit == 1 ) {

					g_pPersist->Commit(FALSE);
					}

				if( m_uCommit == 2 ) {

					g_pPersist->Commit(TRUE);
					}				

				m_uCommit = 0;
				}

			if( ++uTimer >= timeScore ) {

				uScore++;

				uTimer = 0;
				}

			if( uDelay ) {

				uDelay--;
				}
			else {
				if( g_pPersist->IsDirty() ) {

					if( uScore > 0 ) {

						if( timeTesting ) {

							Beep(96, 100);
							}

						Sleep(100);

						g_pPersist->Commit(FALSE);

						uScore = uScore - 1;

						uDelay = timeLimit;
						}
					}
				}

			uTime1 += timeLoop;
			}

		Sleep(ToTime(uTime1 - uTime2));
		}
	}

void CTagManager::PersistTerm(void)
{
	}

// Diagnostics

BOOL CTagManager::DiagRegister(void)
{
	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "tags");

		m_pDiag->RegisterCommand(m_uProv, 1, "list");

		m_pDiag->RegisterCommand(m_uProv, 2, "get");

		m_pDiag->RegisterCommand(m_uProv, 3, "set");

		return TRUE;
		}

	return FALSE;
	}

BOOL CTagManager::DiagRevoke(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;

		return TRUE;
		}

	return FALSE;
	}

UINT CTagManager::DiagList(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 0 || pCmd->GetArgCount() == 1 ) {

		pOut->AddTable(3);

		pOut->SetColumn(0, "Name",  "%s");
		pOut->SetColumn(1, "Value", "%s");
		pOut->SetColumn(2, "Avail", "%s");

		pOut->AddHead();

		pOut->AddRule('-');

		UINT uCount = m_pTags->GetCount();

		MakeMin(uCount, 1000);

		for( UINT uTag = 0; uTag < uCount; uTag++ ) {

			CTag *pTag = m_pTags->GetItem(uTag);

			if( pTag ) {

				if( pTag->GetDataType() ) {

					CString Name = UniConvert(pTag->m_Name);

					if( pCmd->GetArgCount() == 0 || Name.StartsWith(pCmd->GetArg(0)) ) {

						CDataRef Ref;

						Ref.m_Ref = 0;

						if( pTag->IsArray() ) {

							UINT c = pTag->GetExtent();

							UINT d = 1;

							MakeMin(c, 200);

							for( UINT t = c - 1; (t /= 10); d++ );

							for( UINT n = 0; n < c; n++ ) {

								CPrintf Show("%s[%*u]", PCTXT(Name), d, n);

								Ref.t.m_Array = n;

								pOut->AddRow();

								pOut->SetData(0, PCTXT(Show));

								pOut->SetData(1, PCTXT(UniConvert(pTag->GetAsText(Ref.t.m_Array, fmtBare | fmtANSI))));

								pOut->SetData(2, pTag->IsAvail(Ref) ? "Yes" : "No");

								pOut->EndRow();
								}
							}
						else {
							pOut->AddRow();

							pOut->SetData(0, PCTXT(Name));

							pOut->SetData(1, PCTXT(UniConvert(pTag->GetAsText(Ref.t.m_Array, fmtBare | fmtANSI))));

							pOut->SetData(2, pTag->IsAvail(Ref) ? "Yes" : "No");

							pOut->EndRow();
							}
						}
					}
				}
			}

		pOut->AddRule('-');

		pOut->EndTable();

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CTagManager::DiagGet(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 1 ) {

		CTag *   pTag;

		CDataRef Ref;

		if( FindTag(pTag, Ref, pCmd->GetArg(0)) ) {

			CString Name = UniConvert(pTag->m_Name);

			CString Data = UniConvert(pTag->GetAsText(Ref.t.m_Array, fmtBare | fmtANSI));

			if( pTag->IsArray() ) {

				Name.AppendPrintf("[%u]", Ref.t.m_Array);
				}

			pOut->Print("%s = %s\n", PCTXT(Name), PCTXT(Data));

			return 0;
			}

		pOut->Error("tag not found");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

UINT CTagManager::DiagSet(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( pCmd->GetArgCount() == 2 ) {

		CTag *   pTag;

		CDataRef Ref;

		if( FindTag(pTag, Ref, pCmd->GetArg(0)) ) {

			CString Name = UniConvert(pTag->m_Name);

			CString Data = pCmd->GetArg(1);

			if( pTag->SetAsText(Ref.t.m_Array, Data) ) {

				if( pTag->IsArray() ) {

					Name.AppendPrintf("[%u]", Ref.t.m_Array);
					}

				CString Read = UniConvert(pTag->GetAsText(Ref.t.m_Array, fmtBare | fmtANSI));

				pOut->Print("%s = %s\n", PCTXT(Name), PCTXT(Read));

				return 0;
				}

			pOut->Error("failed to write to tag");

			return 3;
			}

		pOut->Error("tag not found");

		return 2;
		}

	pOut->Error(NULL);

	return 1;
	}

// Implementation

void CTagManager::RunAction(CCodedItem *pCode, UINT uWait)
{
	if( pCode ) {

		SetTimer(uWait);

		for(;; ) {

			if( pCode->IsAvail() ) {

				pCode->Execute(typeVoid);

				return;
				}

			if( GetTimer() ) {

				Sleep(100);

				continue;
				}

			break;
			}
		}
	}

UINT CTagManager::MakeQuickList(void)
{
	UINT uCount = m_pTags->GetCount();

	for( UINT uTag = 0; uTag < uCount; uTag++ ) {

		CTag *pTag = m_pTags->GetItem(uTag);

		if( pTag ) {

			CTagQuickPlot *pQuick = NULL;

			if( pTag->GetQuickPlot(pQuick) ) {

				m_QuickList.Append(pQuick);
				}
			}
		}

	return m_QuickList.GetCount();
	}

BOOL CTagManager::FindTag(CTag * &pTag, CDataRef &Ref, CString Name)
{
	Ref.m_Ref = 0;

	UINT uTag = 0;

	UINT uPos = Name.Find('[');

	if( uPos < NOTHING ) {

		Ref.t.m_Array = atoi(Name.Mid(uPos+1));

		Name = Name.Left(uPos);
		}

	if( (uTag = m_pTags->FindByName(UniConvert(Name))) < NOTHING ) {

		if( (pTag = m_pTags->GetItem(uTag)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// End of File
