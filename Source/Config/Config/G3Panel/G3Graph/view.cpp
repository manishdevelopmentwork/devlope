
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// View Menu

BOOL CPageEditorWnd::OnViewGetInfo(UINT uID, CCmdInfo &Info)
{
	if( uID >= IDM_VIEW_LANG_00 && uID <= IDM_VIEW_LANG_19 ) {

		UINT uSlot    = uID - IDM_VIEW_LANG_00;

		UINT uFlag    = m_pLang->GetFlagFromSlot(uSlot);

		Info.m_Image  = MAKELONG(uFlag, 0x4200);

		Info.m_Prompt = CString(IDS_SELECT_INDICATED);

		return TRUE;
		}

	static UINT const List[] = 

	{	IDM_VIEW_ZOOM_MORE, MAKELONG(0x0004, 0x3000),
		IDM_VIEW_ZOOM_LESS, MAKELONG(0x0005, 0x3000),
		IDM_VIEW_GRID,      MAKELONG(0x0003, 0x3000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CPageEditorWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	if( m_uMode == modeText ) {

		return FALSE;
		}

	if( uID >= IDM_VIEW_LANG_00 && uID <= IDM_VIEW_LANG_19 ) {

		UINT    uSlot = uID - IDM_VIEW_LANG_00;

		CString Name  = m_pLang->GetNameFromSlot(uSlot);

		Src.SetItemText(CPrintf(L"%2.2u. %s", uSlot, Name));

		Src.CheckItem  (uSlot == m_pLang->m_Lang);

		Src.EnableItem (TRUE);

		return TRUE;
		}

	switch( uID ) {

		case IDM_VIEW_ZOOM_NORM:

			Src.EnableItem(m_nScale > m_nScaleMin);

			break;

		case IDM_VIEW_ZOOM_MORE:

			Src.EnableItem(m_nScale < m_nScaleMax);

			break;

		case IDM_VIEW_ZOOM_LESS:

			Src.EnableItem(m_nScale > m_nScaleMin);

			break;

		case IDM_VIEW_ALIGN_EDGE:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_fAlignEdge);

			break;
		
		case IDM_VIEW_ALIGN_CENT:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_fAlignCenter);

			break;

		case IDM_VIEW_GRID:

			Src.EnableItem(m_nScale > 0);

			Src.CheckItem (m_nScale > 0 && m_fGridShow);

			break;

		case IDM_VIEW_SNAP_CREATE:
		case IDM_VIEW_SNAP_MOVE:
		case IDM_VIEW_SNAP_SIZE:
		case IDM_VIEW_SNAP_GROUP:

			Src.EnableItem(TRUE);

			Src.CheckItem (!!(m_uGridSnap & (1 << (uID - IDM_VIEW_SNAP_CREATE))));

			break;

		case IDM_VIEW_SNAP_ALL:
		case IDM_VIEW_SNAP_NONE:

			Src.EnableItem(TRUE);

			break;

		case IDM_VIEW_SHOW_MASTER:

			Src.EnableItem(!m_fScratch && TRUE);

			Src.CheckItem (m_fMastShow);

			break;

		case IDM_VIEW_SHOW_ERROR:

			Src.EnableItem(!m_fScratch && TRUE);

			Src.CheckItem (m_fShowError);

			break;

		case IDM_VIEW_REFRESH:

			Src.EnableItem(TRUE);

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CPageEditorWnd::OnViewCommand(UINT uID)
{
	if( uID >= IDM_VIEW_LANG_00 && uID <= IDM_VIEW_LANG_19 ) {

		UINT uSlot = uID - IDM_VIEW_LANG_00;

		m_pLang->m_Lang = uSlot;

		m_pLang->SetDirty();

		UpdateImage();

		Invalidate(FALSE);

		return TRUE;
		}

	switch( uID ) {

		case IDM_VIEW_ZOOM_NORM:

			SetScale(m_nScaleMin);

			break;

		case IDM_VIEW_ZOOM_MORE:

			SetScale(m_nScale + 1);

			break;

		case IDM_VIEW_ZOOM_LESS:

			SetScale(m_nScale - 1);

			break;

		case IDM_VIEW_ALIGN_EDGE:

			m_fAlignEdge   = !m_fAlignEdge;

			break;

		case IDM_VIEW_ALIGN_CENT:

			m_fAlignCenter = !m_fAlignCenter;

			break;

		case IDM_VIEW_GRID:

			m_fGridShow = !m_fGridShow;

			Invalidate(FALSE);

			break;

		case IDM_VIEW_SNAP_CREATE:
		case IDM_VIEW_SNAP_MOVE:
		case IDM_VIEW_SNAP_SIZE:
		case IDM_VIEW_SNAP_GROUP:

			m_uGridSnap = m_uGridSnap ^ (1 << (uID - IDM_VIEW_SNAP_CREATE));

			break;

		case IDM_VIEW_SNAP_ALL:

			m_uGridSnap = 7;

			break;

		case IDM_VIEW_SNAP_NONE:

			m_uGridSnap = 0;

			break;

		case IDM_VIEW_SHOW_MASTER:

			m_fMastShow = !m_fMastShow;

			UpdateAll();

			break;

		case IDM_VIEW_SHOW_ERROR:

			m_fShowError = !m_fShowError;

			UpdateAll();

			break;

		case IDM_VIEW_REFRESH:

			FindMetrics();

			AdjustLayout(TRUE);

			UpdateAll();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// End of File
