
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Prim Paste Special Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPrimPasteSpecialDialog, CStdDialog);

// Constructors

CPrimPasteSpecialDialog::CPrimPasteSpecialDialog(void)
{
	SetName(L"PrimPasteSpecialDlg");
	}

// Attributes

UINT CPrimPasteSpecialDialog::GetMode(void) const
{
	return m_uMode;
	}

// Message Map

AfxMessageMap(CPrimPasteSpecialDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CPrimPasteSpecialDialog)
	};

// Message Handlers

BOOL CPrimPasteSpecialDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetRadioGroup(1100, 1104, 4);

	return TRUE;
	}

// Command Handlers

BOOL CPrimPasteSpecialDialog::OnCommandOK(UINT uID)
{
	m_uMode = GetRadioGroup(1100, 1104);

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CPrimPasteSpecialDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

// End of File
