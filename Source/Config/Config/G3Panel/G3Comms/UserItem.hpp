
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UserItem_HPP

#define INCLUDE_UserItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// User Item
//

class DLLNOT CUserItem : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUserItem(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Item Naming
		CString GetHumanName(void) const;

		// Item Location
		CCommsSystem * FindSystem(void) const;

		// Persistance
		void Init(void);
		void PostLoad(void);
		void PostPaste(void);
		void Kill(void);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CString	     m_Name;
		CCodedItem * m_pRealName;
		CCodedItem * m_pPassword;
		UINT         m_Force;
		UINT	     m_Rights[32];
		DWORD        m_Addr;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Memory Allocation
		void  PersistRegister(DWORD dwAddr, UINT uSize);
		DWORD PersistAllocate(UINT uSize);
		void  PersistFree(DWORD dwAddr, UINT uSize);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
