
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_PosixFileCopy_HPP

#define	INCLUDE_PosixFileCopy_HPP

//////////////////////////////////////////////////////////////////////////
//
// File Copier 
//

class CPosixFileCopy : public IFileCopier
{
	public:
		// Constructors
		CPosixFileCopy(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IFileCopier
		BOOL CopyFile(PCTXT pSrc, PCTXT pDst, UINT uFlags);
		BOOL MoveFile(PCTXT pSrc, PCTXT pDst, UINT uFlags);
		BOOL CopyDir (PCTXT pSrc, PCTXT pDst, UINT uFlags);
		BOOL MoveDir (PCTXT pSrc, PCTXT pDst, UINT uFlags);

	protected:
		// Internal Flags
		enum Flags
		{
			flagDelete = Bit(31)
			};

		// Data
		ULONG     m_uRefs;
		CAutoFile m_Src;
		CAutoFile m_Dst;

		// Destructor
		~CPosixFileCopy(void);

		// Implementation
		UINT OpenFiles(PCTXT pSrc, PCTXT pDst, UINT uFlags);
		void CloseFiles(void);
		bool CopyFile(void);
		bool PathAppendSlash(char *path);
		void PathJoin(char *path, char const *p1, char const *p2);
};

// End of File

#endif
