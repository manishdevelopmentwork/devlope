
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_PlatLinux_HPP

#define INCLUDE_PlatLinux_HPP

//////////////////////////////////////////////////////////////////////////
//
// Platform Flag
//

#define AEON_PLAT_LINUX

//////////////////////////////////////////////////////////////////////////
//
// Environment Check
//

#if !defined(AEON_COMP_GCC)

#error "Linux platform expects GCC."

#endif

//////////////////////////////////////////////////////////////////////////
//
// Windows API Access
//

#if defined(AEON_NEED_WIN32)

#error "Win32 API access not available from Linux."

#endif

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define MAX_PATH	260

//////////////////////////////////////////////////////////////////////////
//
// Host Macros
//

#if defined(__INTELLISENSE__)

#define	HostBreak()	((void) 0)

#define	HostTrap(n)	((void) n)

#define HostTrace()	((void) 0)

#else

#if defined(AEON_PROC_ARM)

#if 1

#define HostBreak()	(((DWORD *) 0)[0]++)

#define HostTrap(n)	(((DWORD *) 0)[n]++)

#define HostTrace()	((void) 0)

#else

#define HostBreak()	ASM(	"mov  r7, #248\n\t"	\
				"mov  r0, %0\n\t"	\
				"svc  #0\n\t"		\
				:			\
				: "r" (10)		\
				)			\

#define HostTrap(n)	ASM(	"mov  r7, #248\n\t"	\
				"mov  r0, %0\n\t"	\
				"svc  #0\n\t"		\
				:			\
				: "r" (10+(n))		\
				)			\

#define HostTrace()	((void) 0)

#endif

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// Linux API Headers
//

#if defined(AEON_NEED_LINUX)

#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/mmap.h>
#include <sys/reent.h>
#include <sys/signal.h>
#include <sys/stat.h>
#include <sys/tls.h>

#endif

//////////////////////////////////////////////////////////////////////////
//
// Linux System Call Macros for X86
//

#if defined(AEON_NEED_LINUX) && defined(AEON_PROC_X86)

#define	SysCall0(s, r)			 ASM(	"mov  %1, %%eax\n\t"	\
						"int  $0x80\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s)		\
						: "eax"			\
						)			\

#define	SysCall1(s, r, a)		 ASM(	"mov  %1, %%eax\n\t"	\
						"mov  %2, %%ebx\n\t"	\
						"int  $0x80\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s),		\
						  "g" (a)		\
						: "memory",		\
						  "eax",		\
						  "ebx"			\
						)			\

#define	SysCall2(s, r, a, b)		 ASM(	"mov  %1, %%eax\n\t"	\
						"mov  %2, %%ebx\n\t"	\
						"mov  %3, %%ecx\n\t"	\
						"int  $0x80\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s),		\
						  "g" (a),		\
						  "g" (b)		\
						: "memory",		\
						  "eax",		\
						  "ebx",		\
						  "ecx"			\
						)			\

#define	SysCall3(s, r, a, b, c)		 ASM(	"mov  %1, %%eax\n\t"	\
						"mov  %2, %%ebx\n\t"	\
						"mov  %3, %%ecx\n\t"	\
						"mov  %4, %%edx\n\t"	\
						"int  $0x80\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s),		\
						  "g" (a),		\
						  "g" (b),		\
						  "g" (c)		\
						: "memory",		\
						  "eax",		\
						  "ebx",		\
						  "ecx",		\
						  "edx"			\
						)			\

#define	SysCall4(s, r, a, b, c, d)	 ASM(	"mov  %1, %%eax\n\t"	\
						"mov  %2, %%ebx\n\t"	\
						"mov  %3, %%ecx\n\t"	\
						"mov  %4, %%edx\n\t"	\
						"mov  %5, %%esi\n\t"	\
						"int  $0x80\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s),		\
						  "g" (a),		\
						  "g" (b),		\
						  "g" (c),		\
						  "g" (d)		\
						: "memory",		\
						  "eax",		\
						  "ebx",		\
						  "ecx",		\
						  "edx",		\
						  "esi"			\
						)			\

#define	SysCall5(s, r, a, b, c, d, e)	 ASM(	"mov  %1, %%eax\n\t"	\
						"mov  %2, %%ebx\n\t"	\
						"mov  %3, %%ecx\n\t"	\
						"mov  %4, %%edx\n\t"	\
						"mov  %5, %%esi\n\t"	\
						"mov  %6, %%edi\n\t"	\
						"int  $0x80\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s),		\
						  "g" (a),		\
						  "g" (b),		\
						  "g" (c),		\
						  "g" (d),		\
						  "g" (e)		\
						: "memory",		\
						  "eax",		\
						  "ebx",		\
						  "ecx",		\
						  "edx",		\
						  "esi",		\
						  "edi"			\
						)			\

#define	SysCall6(s, r, a, b, c, d, e, f) ASM(	"mov  %2, %%ebx\n\t"	\
						"mov  %3, %%ecx\n\t"	\
						"mov  %4, %%edx\n\t"	\
						"mov  %5, %%esi\n\t"	\
						"mov  %6, %%edi\n\t"	\
						"mov  %7, %%eax\n\t"	\
						"push %%ebp\n\t"	\
						"mov  %%eax, %%ebp\n\t"	\
						"mov  %1, %%eax\n\t"	\
						"int  $0x80\n\t"	\
						"pop  %%ebp\n\t"	\
						"mov  %%eax, %0\n\t"	\
						: "=g"(r)		\
						: "e" (s),		\
						  "g" (a),		\
						  "g" (b),		\
						  "g" (c),		\
						  "g" (d),		\
						  "g" (e),		\
						  "g" (f)		\
						: "memory",		\
						  "eax",		\
						  "ebx",		\
						  "ecx",		\
						  "edx",		\
						  "esi",		\
						  "edi"			\
						)			\

#endif

//////////////////////////////////////////////////////////////////////////
//
// Linux System Call Macros for ARM
//

#if defined(AEON_NEED_LINUX) && defined(AEON_PROC_ARM)

#define	SysCall0(s, r)			ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00)	\
						: "r0",			\
						  "r7"			\
						)			\

#define	SysCall1(s, r, a)		ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"mov  r0, %3\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00),	\
						  "r" (a)		\
						: "memory",		\
						  "r0",			\
						  "r7"			\
						)			\

#define	SysCall2(s, r, a, b)		ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"mov  r0, %3\n\t"	\
						"mov  r1, %4\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00),	\
						  "r" (a),		\
						  "r" (b)		\
						: "memory",		\
						  "r0",			\
						  "r1",			\
						  "r7"			\
						)			\

#define	SysCall3(s, r, a, b, c)		ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"mov  r0, %3\n\t"	\
						"mov  r1, %4\n\t"	\
						"mov  r2, %5\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00),	\
						  "r" (a),		\
						  "r" (b),		\
						  "r" (c)		\
						: "memory",		\
						  "r0",			\
						  "r1",			\
						  "r2",			\
						  "r7"			\
						)			\

#define	SysCall4(s, r, a, b, c, d)	ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"mov  r0, %3\n\t"	\
						"mov  r1, %4\n\t"	\
						"mov  r2, %5\n\t"	\
						"ldr  r3, %6\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00),	\
						  "r" (a),		\
						  "r" (b),		\
						  "r" (c),		\
						  "m" (d)		\
						: "memory",		\
						  "r0",			\
						  "r1",			\
						  "r2",			\
						  "r3",			\
						  "r7"			\
						)			\

#define	SysCall5(s, r, a, b, c, d, e)	ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"mov  r0, %3\n\t"	\
						"mov  r1, %4\n\t"	\
						"mov  r2, %5\n\t"	\
						"ldr  r3, %6\n\t"	\
						"ldr  r4, %7\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00),	\
						  "r" (a),		\
						  "r" (b),		\
						  "r" (c),		\
						  "m" (d),		\
						  "m" (e)		\
						: "memory",		\
						  "r0",			\
						  "r1",			\
						  "r2",			\
						  "r3",			\
						  "r4",			\
						  "r7"			\
						)			\

#define	SysCall6(s, r, a, b, c, d, e, f) ASM(	"mov  r7, %1\n\t"	\
						"orr  r7, %2\n\t"	\
						"mov  r0, %3\n\t"	\
						"mov  r1, %4\n\t"	\
						"mov  r2, %5\n\t"	\
						"ldr  r3, %6\n\t"	\
						"ldr  r4, %7\n\t"	\
						"ldr  r5, %8\n\t"	\
						"svc  #0\n\t"		\
						"mov  %0, r0\n\t"	\
						: "=r"(r)		\
						: "I" (s & 0x00FF),	\
						  "I" (s & 0xFF00),	\
						  "r" (a),		\
						  "r" (b),		\
						  "r" (c),		\
						  "m" (d),		\
						  "m" (e),		\
						  "m" (f)		\
						: "memory",		\
						  "r0",			\
						  "r1",			\
						  "r2",			\
						  "r3",			\
						  "r4",			\
						  "r5",			\
						  "r7"			\
						)			\

#endif

// End of File

#endif
