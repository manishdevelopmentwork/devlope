
#include "intern.hpp"

#include "DADIDOViewWnd.hpp"

#include "DADIDOModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DO Main View
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOViewWnd, CUIViewWnd);

// Overibables

void CDADIDOViewWnd::OnAttach(void)
{
	m_pItem   = (CDADIDOModule *) CViewWnd::m_pItem;

	CUIViewWnd::OnAttach();
}

// UI Update

void CDADIDOViewWnd::OnUICreate(void)
{
	StartPage(1);

	AddIdent();

	EndPage(FALSE);
}

// Implementation

void CDADIDOViewWnd::AddIdent(void)
{
	StartGroup(IDS("Options"), 1, FALSE);

	CUIData Data;

	Data.m_Tag       = L"Ident";

	Data.m_Label     = IDS("Variant");

	Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

	Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");

	Data.m_Format    = IDS("32|Solid State Output|36|Relay Output");

	Data.m_Tip       = IDS("Select the Module Variant.");

	AddUI(m_pItem, L"root", &Data);

	EndGroup(TRUE);
}

// End of File
