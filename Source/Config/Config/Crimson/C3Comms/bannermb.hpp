
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BANNERMODBUSTCP_HPP
	
#define	INCLUDE_BANNERMODBUSTCP_HPP

// Type Acronyms
#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	WW	addrWordAsWord
#define	WL	addrWordAsLong
#define	LL	addrLongAsLong

// Header Space Defs
#define	HEADER_SPACE_OFFSET	0x7FF0
#define	SPINPUTFLAGS		0x7FF1
#define	SPOUTPUTFLAGS		0x7FF2
#define	SPACKFLAGS		0x7FF3
#define	SPOUTPUTR3		0x7FF4
#define	SPOUTPUTR4		0x7FF5
#define	SPINPUTR		0x7FF6
#define	SPMAX			0x7FFF // generic maximum

#define	SPH	239	// Generic Table number for Headers

// Spaces
// Starting Modbus Address = 00001
#define	CIT	0x101
#define	CIR	0x102
#define	CIP	0x103
#define	CIS	0x109

// Starting Modbus Address = 10001
#define	COP	0x201
#define	COF	0x202
#define	COE	0x203
#define	COR	0x205
#define	CIO1	0x209
#define	CIO2	0x20A
#define	CIO3	0x20B
#define	CIO4	0x20C
#define	CIO5	0x20D
#define	CIO6	0x20E

// Starting Modbus Address = 10017
#define	CAT	0x300 + 17
#define	CAR	0x300 + 18
#define	CAP	0x300 + 19
#define	CAS	0x300 + 25

// Starting Modbus Address = 30001
#define	CO3O	0x401
#define	CO3A	0x402
#define	CO3I	0x403
#define	CO3E	0x404
#define	CO3M	0x405
#define	CO3P	0x407
#define	CO3F	0x409
#define	CO3T	0x40B
#define	CO3C	0x40D
#define	CO3L	4	// Longs starting at 30033

// Starting Modbus Address = 41001
#define	CO4O	0x501
#define	CO4A	0x502
#define	CO4I	0x503
#define	CO4E	0x504
#define	CO4M	0x505
#define	CO4P	0x507
#define	CO4F	0x509
#define	CO4T	0x50B
#define	CO4C	0x50D
#define	CO4L	5	// Longs starting at 41033

// Starting Modbus Address = 40001
#define	CIRI	0x601
#define	CIRP	0x602
#define	CIRC	6	// String chars 40040 - 40139
#define	CIRM	7	// String chars 40140 - 40239

// Any Modbus Address
#define	CREG	1

//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP/IP Driver Options
//

class CBannerMbusTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CBannerMbusTCPDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;


	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP/IP Driver
//

class CBannerModbusTCPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CBannerModbusTCPDriver(void);

		//Destructor
		~CBannerModbusTCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL CheckAlignment(CSpace *pSpace);
						
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Banner Modbus TCP Address Selection
//

class CBannerModbusTCPAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CBannerModbusTCPAddrDialog(CBannerModbusTCPDriver &Driver, CAddress &Addr, BOOL fPart);
		                
	protected:
		// Data Members
		UINT	m_uAllowSpace;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL	OnOkay(UINT uID);

		// Overridables
		BOOL	AllowSpace(CSpace *pSpace);
		BOOL	AllowType(UINT uType);
		void	ShowDetails(void);

		// Helpers
		void	SetAllow(void);
		BOOL	IsAllowed(UINT uCommand, UINT uAllowed);
		UINT	HasAddr(CSpace * pSpace);
		void	NoAddress(BOOL fPutNone);

	};

// End of File

#endif
