
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatMulti_HPP

#define INCLUDE_DispFormatMulti_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispFormat.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispFormatMultiList;

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Display Format
//

class DLLAPI CDispFormatMulti : public CDispFormat
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatMulti(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Formatting
		CString Format(DWORD Data, UINT Type, UINT Flags);

		// Limit Access
		DWORD GetMin(UINT Type);
		DWORD GetMax(UINT Type);

		// Operations
		void UpdateTypes(BOOL fComp);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT                   m_Count;
		CCodedItem	     * m_pLimit;
		CCodedText           * m_pDefault;
		UINT                   m_Range;
		CDispFormatMultiList * m_pList;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		BOOL DoExport(void);
		BOOL DoImport(void);
		void DoEnables(IUIHost *pHost);
		UINT GetCount(void);

		// Friends
		friend class CDispFormatMultiPage;
	};

// End of File

#endif
