
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_COGNEXCAMERADRIVER_HPP
	
#define	INCLUDE_COGNEXCAMERADRIVER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cognex Camera Driver
//

class CCognexCameraDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CCognexCameraDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
	};

// End of File

#endif
