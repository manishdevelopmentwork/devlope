
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AnsiDebugConsole_HPP

#define INCLUDE_AnsiDebugConsole_HPP

////////////////////////////////////////////////////////////////////////
//
// ANSI Terminal Debug Console
//

class DLLAPI CAnsiDebugConsole
{
	public:
		// Constructor
		CAnsiDebugConsole(IDiagConsole *pConsole, bool fExit, bool fFull);
	
		// Destructor
		~CAnsiDebugConsole(void);
	
		// Operations
		void Hello(void);
		bool OnChar(char cData);

	protected:
		// Data Members
		IDiagConsole   * m_pConsole;
		bool		 m_fExit;
		bool		 m_fFull;
		IDiagManager   * m_pDiag;
		UINT		 m_uCon;
		UINT             m_uScroll;
		char		 m_sLine[1024];
		UINT		 m_uState;
		UINT		 m_uPtr;
		CStringArray	 m_History;
		IFileUtilities * m_pUtils;

		// Implementation
		void ShowPrompt(void);
		void CrLf(void);
		void Backspace(void);
		bool WipeLine(PCTXT pLine, UINT uPtr);
		bool WipeLine(UINT uLen, UINT uPtr);
		void Print(PCTXT p);
		void Print(char cData);
		bool CompleteFile(char *pLine, UINT uSize);
		void Subsitute(PTXT pLine);
	};

// End of File

#endif
