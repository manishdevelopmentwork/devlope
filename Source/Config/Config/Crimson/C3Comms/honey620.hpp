//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HONEYWELLIPC620_HPP
	
#define	INCLUDE_HONEYWELLIPC620_HPP

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Driver Options
//

class CHoneywell620DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHoneywell620DriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Connection;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Honeywell IPC620 Driver
//

class CHoneywellIPC620Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CHoneywellIPC620Driver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
