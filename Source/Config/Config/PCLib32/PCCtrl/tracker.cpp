
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Track Bar Control
//

// Dynamic Class

AfxImplementDynamicClass(CTrackBar, CCtrlWnd);

// Constructor

CTrackBar::CTrackBar(void)
{
	LoadControlClass(ICC_BAR_CLASSES);
	}

// Attributes

CWnd & CTrackBar::GetBuddy(BOOL fPos) const
{
	return CWnd::FromHandle(HWND(SendMessageConst(TBM_GETBUDDY, WPARAM(fPos))));
	}

CWnd & CTrackBar::GetBuddyLeft(void) const
{
	return GetBuddy(TRUE);
	}

CWnd & CTrackBar::GetBuddyRight(void) const
{
	return GetBuddy(TRUE);
	}

CRect CTrackBar::GetChannelRect(void) const
{
	CRect Rect;

	SendMessageConst(TBM_GETCHANNELRECT, 0, LPARAM(&Rect));

	return Rect;
	}

UINT CTrackBar::GetLineSize(void) const
{
	return UINT(SendMessageConst(TBM_GETLINESIZE));
	}

UINT CTrackBar::GetNumTicks(void) const
{
	return UINT(SendMessageConst(TBM_GETNUMTICS));
	}

UINT CTrackBar::GetPageSize(void) const
{
	return UINT(SendMessageConst(TBM_GETPAGESIZE));
	}

UINT CTrackBar::GetPos(void) const
{
	return UINT(SendMessageConst(TBM_GETPOS));
	}

UINT * CTrackBar::GetTickPointer(void) const
{
	return (UINT *) SendMessageConst(TBM_GETPTICS);
	}

UINT CTrackBar::GetRangeMax(void) const
{
	return UINT(SendMessageConst(TBM_GETRANGEMAX));
	}

UINT CTrackBar::GetRangeMin(void) const
{
	return UINT(SendMessageConst(TBM_GETRANGEMIN));
	}

CRange CTrackBar::GetRange(void) const
{
	return CRange(GetRangeMin(), GetRangeMax());
	}

UINT CTrackBar::GetSelEnd(void) const
{
	return UINT(SendMessageConst(TBM_GETSELEND));
	}

UINT CTrackBar::GetSelStart(void) const
{
	return UINT(SendMessageConst(TBM_GETSELSTART));
	}

CRange CTrackBar::GetSel(void) const
{
	return CRange(GetSelStart(), GetSelEnd());
	}

UINT CTrackBar::GetThumbLength(void) const
{
	return UINT(SendMessageConst(TBM_GETTHUMBLENGTH));
	}

CRect CTrackBar::GetThumbRect(void) const
{
	CRect Rect;

	SendMessageConst(TBM_GETTHUMBRECT, 0, LPARAM(&Rect));

	return Rect;
	}

UINT CTrackBar::GetTick(UINT uIndex) const
{
	return UINT(SendMessageConst(TBM_GETTIC, uIndex));
	}

int CTrackBar::GetTickPos(UINT uIndex) const
{
	return int(SendMessageConst(TBM_GETTICPOS, uIndex));
	}

// Operations

void CTrackBar::ClearSel(BOOL fRedraw)
{
	SendMessage(TBM_CLEARSEL, fRedraw);
	}

void CTrackBar::ClearTicks(BOOL fRedraw)
{
	SendMessage(TBM_CLEARTICS, fRedraw);
	}

void CTrackBar::SetBuddy(BOOL fPos, HWND hWnd)
{
	SendMessage(TBM_SETBUDDY, WPARAM(fPos), LPARAM(hWnd));
	}

void CTrackBar::SetBuddyLeft(HWND hWnd)
{
	SetBuddy(TRUE, hWnd);
	}

void CTrackBar::SetBuddyRight(HWND hWnd)
{
	SetBuddy(FALSE, hWnd);
	}

void CTrackBar::SetLineSize(UINT uSize)
{
	SendMessage(TBM_SETLINESIZE, 0, uSize);
	}

void CTrackBar::SetPageSize(UINT uSize)
{
	SendMessage(TBM_SETPAGESIZE, 0, uSize);
	}

void CTrackBar::SetPos(UINT uPos, BOOL fRedraw)
{
	SendMessage(TBM_SETPOS, WPARAM(fRedraw), uPos);
	}

void CTrackBar::SetRange(UINT uMin, UINT uMax, BOOL fRedraw)
{
	SendMessage(TBM_SETRANGE, fRedraw, MAKELPARAM(uMin, uMax));
	}

void CTrackBar::SetRange(CRange const &Range, BOOL fRedraw)
{
	SendMessage(TBM_SETRANGE, fRedraw, LPARAM(DWORD(Range)));
	}

void CTrackBar::SetRangeMax(UINT uMax, BOOL fRedraw)
{
	SendMessage(TBM_SETRANGEMAX, fRedraw, uMax);
	}

void CTrackBar::SetRangeMin(UINT uMin, BOOL fRedraw)
{
	SendMessage(TBM_SETRANGEMIN, fRedraw, uMin);
	}

void CTrackBar::SetSel(UINT uMin, UINT uMax, BOOL fRedraw)
{
	SendMessage(TBM_SETSEL, fRedraw, MAKELPARAM(uMin, uMax));
	}

void CTrackBar::SetSel(CRange const &Range, BOOL fRedraw)
{
	SendMessage(TBM_SETSEL, fRedraw, LPARAM(DWORD(Range)));
	}

void CTrackBar::SetSelEnd(UINT uPos, BOOL fRedraw)
{
	SendMessage(TBM_SETSELEND, fRedraw, uPos);
	}

void CTrackBar::SetSelStart(UINT uPos, BOOL fRedraw)
{
	SendMessage(TBM_SETSELSTART, fRedraw, uPos);
	}

void CTrackBar::SetThumbLength(UINT uSize, BOOL fRedraw)
{
	SendMessage(TBM_SETTHUMBLENGTH, fRedraw, uSize);
	}

void CTrackBar::SetTick(UINT uPos)
{
	SendMessage(TBM_SETTIC, 0, uPos);
	}

void CTrackBar::SetTickFreq(UINT uFreq)
{
	SendMessage(TBM_SETTICFREQ, uFreq);
	}

// Handle Lookup

CTrackBar & CTrackBar::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CTrackBar NullObject;

		return NullObject;
		}

	return (CTrackBar &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CTrackBar::GetDefaultClassName(void) const
{
	return TRACKBAR_CLASS;
	}

// End of File
