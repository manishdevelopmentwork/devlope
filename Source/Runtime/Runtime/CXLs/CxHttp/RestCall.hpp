
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cumulocity Cloud Interface
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RestCall_HPP
	
#define	INCLUDE_RestCall_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpClientConnection;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpClientRequest.hpp"

#include "JsonData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// REST Call
//

class DLLAPI CRestCall
{
	public:
		// Constructor
		CRestCall(void);
		CRestCall(CString Verb, CString Path, CString ReqType, CString RepType);
		CRestCall(CString Verb, CString Path, CString Type);
		CRestCall(CString Verb, CString Path);
		CRestCall(CString Path);

		// Destructor
		~CRestCall(void);

		// Attributes
		UINT GetStatus(void) const;

		// JSON Access
		CJsonData * GetJsonReq(void);
		CJsonData * GetJsonRep(void);

		// Operations
		void SetVerb(CString Verb);
		void SetPath(CString Path);
		BOOL SetReqType(CString ReqType);
		BOOL SetRepType(CString RepType);
		BOOL SetType(CString Type);

		// Transaction
		BOOL Transact(CHttpClientConnection *pConnect, UINT uStatus);
		BOOL Transact(CHttpClientConnection *pConnect);

	protected:
		// Data Members
		CHttpClientRequest m_Http;
		CJsonData	   m_JsonReq;
		CJsonData	   m_JsonRep;
	};

// End of File

#endif
