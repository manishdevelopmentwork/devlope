
#include "intern.hpp"

#include "g3usbdrv.hpp"

#include "g3master.hpp"

#include "xpusb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Transport
//

class CUSBTransport : public ILinkTransport
{
	public:
		// Constructor
		CUSBTransport(WORD wBase);

		// Destructor
		~CUSBTransport(void);

		// Deletion
		void Release(void);

		// Management
		BOOL Open(void);
		void Recycle(UINT uType);
		void Close(void);
		void Terminate(void);

		// Attributes
		UINT    GetType(void) const;
		UINT    GetBulkLimit(BOOL fDown = TRUE) const;
		CString GetErrorText(void) const;

		// Transport
		BOOL Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fReply);

		// Notifications
		void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
		void OnBind(HWND hWnd);

	protected:
		// Data Members
		WORD	    m_wBase;
		CUSBDriver *m_pUSB;
		WORD	    m_wTag;
		UINT 	    m_uCycle;
		CString	    m_Error;
		HDEVNOTIFY  m_hNotify;
		HWND        m_hWnd;
		CEvent      m_TermEvent;
		CEvent	    m_DevEvent;

		// Implementation
		BOOL SendHead(CLinkFrame &Req);
		BOOL SendBulk(CLinkFrame &Req);
		BOOL GetReply(CLinkFrame &Rep, BOOL fSlow);
		BOOL GetStatus(G3SB &s, BOOL fSlow);

		// Notifications
		void EnableEvents(BOOL fEnable);
		BOOL EventsEnabled(void) const;

		// Device State
		UINT FindProd(UINT uType) const;
		UINT FindMask(UINT uType) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// G3 Link USB Transport
//

// Instantiator

ILinkTransport * Create_TransportUSB(WORD wBase)
{
	return new CUSBTransport(wBase);
	}

// Constructor

CUSBTransport::CUSBTransport(WORD wBase)
{
	m_wBase    = wBase;

	m_pUSB     = NULL;

	m_wTag     = WORD(rand());

	m_uCycle   = transNone;

	m_hNotify  = NULL;

	m_hWnd     = NULL;

	m_DevEvent.Set();
	}

// Destructor

CUSBTransport::~CUSBTransport(void)
{
	AfxAssert(!m_hNotify);
	}

// Deletion

void CUSBTransport::Release(void)
{
	delete this;
	}

// Management

BOOL CUSBTransport::Open(void)
{
	if( !m_pUSB ) {

		m_pUSB = new CUSBDriver;
		}

	if( !m_pUSB->IsValid() ) {

		UINT uProd = FindProd(m_uCycle);

		UINT uMask = FindMask(m_uCycle);

		m_Error = CString(IDS_CANT_OPEN_PORT);

		if( EventsEnabled() ) {

			CWaitableList List(m_DevEvent, m_TermEvent);

			if( List.WaitForAnyObject(INFINITE) != waitSignal ) {

				return FALSE;
				}

			if( List.GetObjectIndex() != 0 ) {

				return FALSE;
				}
			
			return m_pUSB->Open(uProd, uMask);
			}

		for( UINT u = 0; u < 30; u ++ ) {
	
			m_pUSB->Refresh();

			if( m_pUSB->Open(uProd, uMask) ) {

				return TRUE;
				}
			
			Sleep(100);
			}

		return FALSE;
		}

	return TRUE;
	}

void CUSBTransport::Recycle(UINT uType)
{
	if( uType != transNone ) {

		m_DevEvent.Reset();
		
		Close();

		m_uCycle = uType;
		}
	}

void CUSBTransport::Close(void)
{
	if( m_pUSB ) {

		delete m_pUSB;

		m_pUSB = NULL;
		}
	}

void CUSBTransport::Terminate(void)
{
	m_TermEvent.Set();

	EnableEvents(FALSE);
	}

// Attributes

UINT CUSBTransport::GetType(void) const
{
	return typeUSB;
	}

UINT CUSBTransport::GetBulkLimit(BOOL fDown) const
{
	return 8192;
	}

CString CUSBTransport::GetErrorText(void) const
{
	return m_Error;
	}

// Transport

BOOL CUSBTransport::Transact(CLinkFrame &Req, CLinkFrame &Rep, BOOL fExpectReply)
{
	if( m_uCycle ) {
		
		if( !Open() ) {
		
			m_Error = CString(IDS_CANT_OPEN_PORT);
			
			return FALSE;
			}
		
		m_uCycle = 0;
		}

	if( SendHead(Req) && SendBulk(Req) ) {

		if( GetReply(Rep, Req.IsVerySlow()) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Notifications

void CUSBTransport::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	// TODO -- What about mulitple devices ?
	
	if( uMsg == WM_DEVICECHANGE ) {

		if( wParam == DBT_DEVICEARRIVAL ) {

			DEV_BROADCAST_HDR &h = *(DEV_BROADCAST_HDR *) lParam;

			if( h.dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE ) {

				m_DevEvent.Set();
				}

			return;
			}
		
		if( wParam == DBT_DEVICEREMOVECOMPLETE ) {

			DEV_BROADCAST_HDR &h = *(DEV_BROADCAST_HDR *) lParam;

			if( h.dbch_devicetype == DBT_DEVTYP_DEVICEINTERFACE ) {
				
				m_DevEvent.Reset();
				}

			return;
			}		
		}
	}

void CUSBTransport::OnBind(HWND hWnd)
{
	if( (m_hWnd = hWnd) ) {

		EnableEvents(TRUE);

		m_DevEvent.Set();
		}
	}

// Implementation

BOOL CUSBTransport::SendHead(CLinkFrame &Req)
{
	if( m_pUSB->IsValid() ) {

		m_pUSB->CancelIO();

		G3CB c;

		c.m_dwSig	= SIG_G3CB;

		c.m_wTag	= ++m_wTag;

		c.m_bService	= Req.GetService();

		c.m_bOpcode	= Req.GetOpcode();

		c.m_bFlags	= Req.GetFlags();

		c.m_bDataSize	= BYTE(Req.GetDataSize());

		c.m_wBulkSize	= HostToMotor(WORD(Req.GetBulkSize()));

		memcpy(c.m_bData, Req.GetData(), Req.GetDataSize());

		UINT uSend = sizeof(c);

		UINT uSent = uSend;

		if( m_pUSB->Send(PBYTE(&c), uSent) ) {
			
			if( uSend == uSent ) {

				return TRUE;
				}
			}

		m_Error = CString(IDS_XPUSB_REQ_NOT);

		return FALSE;
		}

	m_Error = CString(IDS_CANT_OPEN_PORT);

	return FALSE;
	}

BOOL CUSBTransport::SendBulk(CLinkFrame &Req)
{
	if( Req.GetBulkSize() ) {

		UINT uSend = Req.GetBulkSize();

		UINT uSent = uSend;

		if( m_pUSB->Send(Req.GetBulk(), uSent) ) {

			if( uSend == uSent ) {

				return TRUE;
				}
			}

		m_Error = CString(IDS_XPUSB_BULK_NOT);
	
		return FALSE;
		}

	return TRUE;
	}

BOOL CUSBTransport::GetReply(CLinkFrame &Rep, BOOL fSlow)
{
	G3SB s;

	if( GetStatus(s, fSlow) ) {

		switch( s.m_bOpcode ) {

			case opReplyTrue:
					
				Rep.StartFrame(0, opReply);
				
				Rep.AddByte(1);
				
				break;

			case opReplyFalse:
				
				Rep.StartFrame(0, opReply);
				
				Rep.AddByte(0);
				
				break;

			default:
				Rep.StartFrame(0, s.m_bOpcode);
				
				break;
			}

		if( s.m_bReadBulk ) {

			UINT uCount = MotorToHost(s.m_wBulkSize);

			UINT uTotal = 0;

			while( uTotal < uCount ) {

				UINT  uRecv = uCount - uTotal;
				
				PBYTE pData = New BYTE [ uRecv ];

				if( !m_pUSB->Recv(pData, uRecv, 1000) ) {

					m_pUSB->CancelIO();

					delete[] pData;

					break;
					}

				Rep.AddData(pData, uRecv);

				uTotal += uRecv;

				delete[] pData;
				}

			if( GetStatus(s, FALSE) ) {

				if( s.m_wBulkSize ) {

					m_Error = CString(IDS_XPUSB_TRUNC);

					return FALSE;
					}

				return TRUE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CUSBTransport::GetStatus(G3SB &s, BOOL fSlow)
{
	for(;;) {

		UINT uSize = sizeof(s);

		UINT uTime = fSlow ? 120000 : 20000;

		if( m_pUSB->Recv(PBYTE(&s), uSize, uTime) ) {

			if( uSize == sizeof(s) ) {

				if( s.m_dwSig == SIG_G3SB ) {

					if( s.m_wTag == m_wTag ) {

						return TRUE;
						}
					}
				}

			continue;
			}

		m_Error = CString(IDS_XPUSB_REPLY_NOT);

		m_pUSB->CancelIO();

		return FALSE;
		}
	}

// Notifications

BOOL CUSBTransport::EventsEnabled(void) const
{
	return m_hNotify != NULL;
	}

void CUSBTransport::EnableEvents(BOOL fEnable)
{
	if( fEnable ) {

		if( !m_hNotify ) {

			m_hNotify = RegisterDeviceNotify(m_hWnd);
			}
		}
	else {
		if( m_hNotify ) {

			UnregisterDeviceNotify(m_hNotify);
			
			m_hNotify = NULL;
			}
		}
	}

// Device State

UINT CUSBTransport::FindProd(UINT uType) const
{
	switch( uType ) {

		case transAppToBoot: 

			return m_wBase + PID_LOADER;
		
		case transAppToApp:
		case transBootToApp: 
			
			return m_wBase + PID_HMI;
		}

	return PRODUCT_ANY;
	}

UINT CUSBTransport::FindMask(UINT uType) const
{
	switch( uType ) {

		case transAppToApp:
		case transAppToBoot:
		case transBootToApp:
			
			return PID_TYPE_MASK;
		}

	return PRODUCT_ANY;
	}

// End of File
