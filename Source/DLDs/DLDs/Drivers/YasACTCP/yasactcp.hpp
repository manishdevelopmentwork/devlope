

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP Driver
//

class CYaskawaACTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CYaskawaACTCPDriver(void);

		// Destructor
		~CYaskawaACTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_uUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			UINT	 m_EC;
			UINT	 m_EV;
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bTx[300];
		BYTE	m_bRx[300];
		UINT	m_uPtr;
		UINT	m_uKeep;
				
		// Implementation
		
		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL	PutFrame(void);
		BOOL	GetFrame(void);
		BOOL	Transact(void);
		BOOL	CheckReply(void);

		// Read Handlers
		CCODE	DoBitRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoBitWrite (AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Connected
		CCODE	LoopBack(void);

		// Write Only
		BOOL	IsEnterOrAccept(UINT uTable);

		// Cached
		BOOL	IsErrCommandOrValue(UINT uTable);

		// Helpers
		BOOL	IsYACParam(UINT uTable);
		UINT	CheckCommParams(AREF Addr, UINT uCount);

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// Space Table
#define	SPDA4	1
#define	SPA	2
#define	SPB	3
#define	SPC	4
#define	SPD	5
#define	SPE	6
#define	SPF	7
#define	SPH	8
#define	SPL	9
#define	SPN	10
#define	SPO	11
#define	SPP	12
#define	SPT	13
#define	SPU	14
#define SPQ	15
#define	SPEN	19
#define	SPAC	20
#define	SPEC	30
#define	SPEV	31
#define	SPDA0	40
#define	SPDA1	41
#define	SPDA3	42

#define	COMBAUD	0x426
#define	COMPAR	0x427

// End of File
