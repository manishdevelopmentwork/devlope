
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display Color
//

// Class Creation

CDispColor * CDispColor::MakeObject(UINT uType)
{
	switch( uType ) {

		case 1: return New CDispColorFixed;
		case 2: return New CDispColorFlag;
		case 3: return New CDispColorMulti;
		case 4: return New CDispColorLinked;
		}

	return NULL;
	}

// Constructor

CDispColor::CDispColor(void)
{
	}

// Destructor

CDispColor::~CDispColor(void)
{
	}

// Scan Control

BOOL CDispColor::IsAvail(void)
{
	return TRUE;
	}

BOOL CDispColor::SetScan(UINT Code)
{
	return TRUE;
	}

// Color Access

DWORD CDispColor::GetColorPair(DWORD Data, UINT Type)
{
	return MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));
	}

COLOR CDispColor::GetForeColor(DWORD Data, UINT Type)
{
	return LOWORD(GetColorPair(Data, Type));
	}

COLOR CDispColor::GetBackColor(DWORD Data, UINT Type)
{
	return HIWORD(GetColorPair(Data, Type));
	}

// End of File
