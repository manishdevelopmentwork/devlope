
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Scaling and Scrolling

void CPageEditorWnd::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	if( m_uCapture == captNone ) {

		if( m_uMode != modeText ) {

			if( IsDown(VK_CONTROL) ) {
					
				if( nDelta > 0 ) {

					OnArrangeBack(FALSE);
					}

				if( nDelta < 0 ) {
				
					OnArrangeFront(FALSE);
					}
				}
			else {
				if( nDelta < 0 ) {

					ScreenToClient(Pos);

					SetScale(m_nScale - 1, Pos);
					}

				if( nDelta > 0 ) {

					ScreenToClient(Pos);

					SetScale(m_nScale + 1, Pos);
					}
				}
			}
		}
	}

void CPageEditorWnd::OnHScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( &Ctrl == m_pScrollH ) {

		if( m_uMode != modeText ) {

			OnScroll(uCode, nPos, m_pScrollH, m_ViewOrg.x);
			}
		}
	}

void CPageEditorWnd::OnVScroll(UINT uCode, int nPos, CWnd &Ctrl)
{
	if( &Ctrl == m_pScrollV ) {

		if( m_uMode != modeText ) {

			OnScroll(uCode, nPos, m_pScrollV, m_ViewOrg.y);
			}
		}
	}

BOOL CPageEditorWnd::OnScroll(UINT uCode, int nPos, CScrollBar *pBar, long &nValue)
{
	int nOld = -nValue;

	int nNew = nOld;

	int nMax = pBar->GetRangeMax() - pBar->GetPageSize();

	switch( uCode ) {

		case SB_LEFT:

			nNew = 0;

			break;

		case SB_RIGHT:

			nNew = nMax;

			break;

		case SB_LINELEFT:

			nNew -= 2 * m_nScale;
			
			break;

		case SB_LINERIGHT:

			nNew += 2 * m_nScale;
			
			break;

		case SB_PAGELEFT:

			nNew -= pBar->GetPageSize();
			
			break;

		case SB_PAGERIGHT :

			nNew += pBar->GetPageSize();
			
			break;

		case SB_THUMBPOSITION:
		case SB_THUMBTRACK:

			nNew = nPos;
			
			break;
		}
	
	MakeMax(nNew, 0);

	MakeMin(nNew, nMax);

	if( nNew - nOld ) {

		nValue = -nNew;

		pBar->SetScrollPos(nNew, TRUE);

		AdjustLayout(FALSE);

		return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::ScrollBy(CSize Step)
{
	CPoint Pos = -1 * (m_ViewOrg - Step);

	OnScroll(SB_THUMBPOSITION, Pos.x, m_pScrollH, m_ViewOrg.x);

	OnScroll(SB_THUMBPOSITION, Pos.y, m_pScrollV, m_ViewOrg.y);
	}

BOOL CPageEditorWnd::ScrollIntoView(CPoint Pos, BOOL fDrop)
{
	if( m_fScrollH || m_fScrollV ) {

		CRect Rect = GetClientRect();

		CRect Clip = Rect;

		if( m_fScrollH ) {

			Clip.bottom -= 18;
			}

		if( m_fScrollV ) {

			Clip.right -= 18;
			}

		if( fDrop ) {

			Clip -= 32;
			}

		if( !Clip.PtInRect(Pos) ) {

			BOOL  fHit = FALSE;

			CRect Work = PPtoDP(m_WorkRect);

			if( Work.left < Rect.left || Work.right > Rect.right ) {

				if( m_fScrollH ) {

					LPARAM lParam = LPARAM(m_pScrollH->GetHandle());

					if( Pos.x < Clip.left ) {

						SendMessage(WM_HSCROLL, SB_LINELEFT, lParam);
						}

					if( Pos.x > Clip.right ) {

						SendMessage(WM_HSCROLL, SB_LINERIGHT, lParam);
						}

					fHit = TRUE;
					}
				}

			if( Work.top < Rect.top || Work.bottom > Rect.bottom ) {

				if( m_fScrollV ) {

					LPARAM lParam = LPARAM(m_pScrollV->GetHandle());

					if( Pos.y < Clip.top ) {

						SendMessage(WM_VSCROLL, SB_LINELEFT, lParam);
						}

					if( Pos.y > Clip.bottom ) {

						SendMessage(WM_VSCROLL, SB_LINERIGHT, lParam);
						}

					fHit = TRUE;
					}
				}

			if( fHit ) {

				SetTimer(m_timerScroll, 50);

				return TRUE;
				}
			}
		}

	KillTimer(m_timerScroll);

	return FALSE;
	}

void CPageEditorWnd::SetScale(int nScale, CPoint Pos1, CPoint Pos2)
{
	if( nScale >= m_nScaleMin && nScale <= m_nScaleMax ) {

		if( m_nScale == 0 && nScale >= 1 ) {

			m_nScale = nScale;

			FindLayout();

			CRect Rect = GetClientRect();

			CSize Size = Rect.GetSize();

			if( m_fScrollH ) {

				Size.cy -= 18;
				}

			if( m_fScrollV ) {

				Size.cx -= 18;
				}

			Size.cx = (Size.cx - m_DispSize.cx) / 2;

			Size.cy = (Size.cy - m_DispSize.cy) / 2;

			m_ViewOrg.x = Size.cx - m_FrameRect.left;
			
			m_ViewOrg.y = Size.cy - m_FrameRect.top;
			}
		else {
			CPoint Org = Pos1;

			Org = DPtoLP(Org);

			m_nScale = nScale;

			FindLayout();

			Org = LPtoDP(Org);

			m_ViewOrg += Pos2;

			m_ViewOrg -= Org;
			}

		AdjustLayout(TRUE);

		m_ClientPos.x = 0;

		m_ClientPos.y = 0;

		UpdatePos();

		ShowDefaultStatus();
		}
	}

void CPageEditorWnd::SetScale(int nScale, CPoint Pos)
{
	SetScale(nScale, Pos, Pos);
	}

void CPageEditorWnd::SetScale(int nScale)
{
	CPoint Pos2 = GetClientRect().GetCenter();

	if( HasSelect() ) {

		CPoint Pos1 = PPtoDP(m_SelRect.GetCenter());

		SetScale(nScale, Pos1, Pos2);

		return;
		}

	SetScale(nScale, Pos2);
	}

void CPageEditorWnd::CreateScrollBars(void)
{
	CRect Null(0, 0, 1, 1);

	m_pScrollH->Create(SBS_HORZ,     Null, ThisObject, 1);

	m_pScrollV->Create(SBS_VERT,     Null, ThisObject, 2);

	m_pScrollC->Create(SS_WHITERECT, Null, ThisObject, 3);
	}

void CPageEditorWnd::PlaceScrollBars(void)
{
	CRect Horz = GetClientRect();

	CRect Vert = GetClientRect();

	CRect Corn = GetClientRect();

	if( m_fScrollH ) {

		Horz.top = Horz.bottom - 18;

		if( m_fScrollV ) {

			Horz.right = Horz.right - 18;
			}

		m_pScrollH->MoveWindow(Horz, FALSE);
		}

	if( m_fScrollV ) {

		Vert.left = Vert.right - 18;

		if( m_fScrollH ) {

			Vert.bottom = Vert.bottom - 18;
			}

		m_pScrollV->MoveWindow(Vert, FALSE);
		}

	Corn.left = Corn.right  - 18;

	Corn.top  = Corn.bottom - 18;

	m_pScrollC->MoveWindow(Corn, FALSE);
	}

void CPageEditorWnd::ShowScrollBar(CWnd *pWnd)
{
	if( pWnd->IsWindowVisible() ) {

		pWnd->Invalidate(FALSE);

		return;
		}

	pWnd->ShowWindow(SW_SHOW);
	}

void CPageEditorWnd::AdjustLayout(BOOL fSize)
{
	if( fSize ) {
	
		FindScaleLimits();

		FindLayout();

		CheckMode();
		}
	else
		ConfigDC(*m_pMap);

	BuildHandles();

	KillGhost();

	Invalidate(TRUE);
	}

void CPageEditorWnd::FindLayout(void)
{
	if( m_nScale == 0 ) {

		CSize Size   = GetClientRect().GetSize();

		m_ViewExt    = Size;

		m_ViewOrg.x  = 0;

		m_ViewOrg.y  = 0;

		m_fScrollH   = FALSE;

		m_fScrollV   = FALSE;

		double xFact = double(m_TotalSize.cx) / m_ViewExt.cx;

		double yFact = double(m_TotalSize.cy) / m_ViewExt.cy;

		double nFact = max(xFact, yFact);

		m_ViewExt.cx = int(m_TotalSize.cx / nFact);

		m_ViewExt.cy = int(m_TotalSize.cy / nFact);

		m_ViewOrg.x += (Size.cx - m_ViewExt.cx) / 2;

		m_ViewOrg.y += (Size.cy - m_ViewExt.cy) / 2;

		m_pScrollH->ShowWindow(SW_HIDE);

		m_pScrollV->ShowWindow(SW_HIDE);

		m_pScrollC->ShowWindow(SW_HIDE);
		}
	else {
		CRect Rect = GetClientRect();

		m_ViewExt  = m_nScale * m_TotalSize;

		m_fScrollH = FALSE;

		m_fScrollV = FALSE;

		for( UINT n = 0; n < 2; n++ ) {

			if( m_ViewExt.cx > Rect.cx() ) {

				if( !m_fScrollH ) {

					m_fScrollH = TRUE;

					Rect.bottom -= 18;
					}
				}

			if( m_ViewExt.cy > Rect.cy() ) {

				if( !m_fScrollV ) {

					m_fScrollV = TRUE;

					Rect.right -= 18;
					}
				}
			}

		PlaceScrollBars();

		if( !m_fScrollH ) {
			
			m_ViewOrg.x = (Rect.cx() - m_ViewExt.cx) / 2;

			m_pScrollH->ShowWindow(SW_HIDE);
			}
		else {
			int nMax = m_ViewExt.cx - Rect.cx();
			    
			int nMin = 0;

			MakeMax(m_ViewOrg.x, -nMax);

			MakeMin(m_ViewOrg.x, -nMin);

			m_pScrollH->SetScrollRange(0, m_ViewExt.cx, FALSE);

			m_pScrollH->SetPageSize(Rect.cx(), FALSE);

			m_pScrollH->SetScrollPos(-m_ViewOrg.x, FALSE);

			ShowScrollBar(m_pScrollH);
			}

		if( !m_fScrollV ) {
			
			m_ViewOrg.y = (Rect.cy() - m_ViewExt.cy) / 2;

			m_pScrollV->ShowWindow(SW_HIDE);
			}
		else {
			int nMax = m_ViewExt.cy - Rect.cy();

			int nMin = 0;

			MakeMax(m_ViewOrg.y, -nMax);

			MakeMin(m_ViewOrg.y, -nMin);

			m_pScrollV->SetScrollRange(0, m_ViewExt.cy, FALSE);

			m_pScrollV->SetPageSize(Rect.cy(), FALSE);

			m_pScrollV->SetScrollPos(-m_ViewOrg.y, FALSE);

			ShowScrollBar(m_pScrollV);
			}

		if( m_fScrollH && m_fScrollV ) {

			ShowScrollBar(m_pScrollC);
			}
		else
			m_pScrollC->ShowWindow(SW_HIDE);
		}

	ConfigDC(*m_pMap);
	}

void CPageEditorWnd::FindScaleLimits(void)
{
	CRect Rect  = GetClientRect();

	int   xFact = Rect.cx() / m_TotalSize.cx;

	int   yFact = Rect.cy() / m_TotalSize.cy;

	m_nScaleMin = min(xFact, yFact);

	m_nScaleMax = 32;

	MakeMax(m_nScale, m_nScaleMin);

	MakeMin(m_nScale, m_nScaleMax);
	}

void CPageEditorWnd::ConfigDC(CDC &DC)
{
	DC.SetMapMode(MM_ANISOTROPIC);

	DC.SetWindowOrg  (0, 0);

	DC.SetWindowExt  (m_TotalSize);
					
	DC.SetViewportOrg(m_ViewOrg);

	DC.SetViewportExt(m_ViewExt);
	}

// Scaling Helpers

CRect CPageEditorWnd::PPtoDP(CRect Rect)
{
	Rect += m_FrameRect.GetTopLeft();

	m_pMap->LPtoDP(Rect);

	return Rect;
	}

CPoint CPageEditorWnd::PPtoDP(CPoint Pos)
{
	Pos += m_FrameRect.GetTopLeft();

	m_pMap->LPtoDP(Pos);

	return Pos;
	}

CRect CPageEditorWnd::LPtoDP(CRect Rect)
{
	m_pMap->LPtoDP(Rect);

	return Rect;
	}

CPoint CPageEditorWnd::LPtoDP(CPoint Pos)
{
	m_pMap->LPtoDP(Pos);

	return Pos;
	}

CRect CPageEditorWnd::DPtoLP(CRect Rect)
{
	m_pMap->DPtoLP(Rect);

	return Rect;
	}

CPoint CPageEditorWnd::DPtoLP(CPoint Pos)
{
	m_pMap->DPtoLP(Pos);

	return Pos;
	}

CRect CPageEditorWnd::DPtoPP(CRect Rect)
{
	m_pMap->DPtoLP(Rect);

	Rect -= m_FrameRect.GetTopLeft();

	return Rect;
	}

CPoint CPageEditorWnd::DPtoPP(CPoint Pos)
{
	m_pMap->DPtoLP(Pos);

	Pos -= m_FrameRect.GetTopLeft();

	return Pos;
	}

// GDI Management

void CPageEditorWnd::CreateGDI(void)
{
	m_pMap = New CClientDC(ThisObject);

	m_pWin = Create_GdiWindowsA888(m_DispSize.cx, m_DispSize.cy);

	m_pGDI = m_pWin->GetGdi();
	}

void CPageEditorWnd::DestroyGDI(void)
{
	m_pGDI->Release();

	delete m_pMap;
	}

// Rectangle Clipping

BOOL CPageEditorWnd::ClipMoveRect(CRect &Rect)
{
	BOOL fClip = FALSE;

	if( Rect.left < m_WorkRect.left ) {

		Rect.right += m_WorkRect.left - Rect.left;

		Rect.left   = m_WorkRect.left;

		fClip = TRUE;
		}

	if( Rect.right > m_WorkRect.right ) {

		Rect.left  -= Rect.right - m_WorkRect.right;

		Rect.right  = m_WorkRect.right;

		fClip = TRUE;
		}

	if( Rect.top < m_WorkRect.top ) {

		Rect.bottom += m_WorkRect.top - Rect.top;

		Rect.top     = m_WorkRect.top;

		fClip = TRUE;
		}

	if( Rect.bottom > m_WorkRect.bottom ) {

		Rect.top    -= Rect.bottom - m_WorkRect.bottom;

		Rect.bottom  = m_WorkRect.bottom;

		fClip = TRUE;
		}

	return fClip;
	}

void CPageEditorWnd::ClipSizeRect(CRect &Rect, CSize Size)
{
	if( Rect.left < m_WorkRect.left ) {

		Rect.left = m_WorkRect.left;

		MakeMax(Rect.right, m_WorkRect.left + Size.cx);
		}

	if( Rect.right > m_WorkRect.right ) {

		Rect.right = m_WorkRect.right;

		MakeMin(Rect.left, m_WorkRect.right - Size.cx);
		}

	if( Rect.top < m_WorkRect.top ) {

		Rect.top = m_WorkRect.top;

		MakeMax(Rect.bottom, m_WorkRect.top + Size.cy);
		}

	if( Rect.bottom > m_WorkRect.bottom ) {

		Rect.bottom = m_WorkRect.bottom;

		MakeMin(Rect.top, m_WorkRect.bottom - Size.cy);
		}
	}

// End of File
