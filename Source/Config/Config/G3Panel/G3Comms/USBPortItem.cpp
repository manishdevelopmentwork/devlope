
#include "Intern.hpp"

#include "USBPortItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// USB Item Base Class
//

// Dynamic Class

AfxImplementDynamicClass(CUSBPortItem, CCodedHost);

// Constructor

CUSBPortItem::CUSBPortItem(void)
{
	}

// Attributes

UINT CUSBPortItem::GetTreeImage(void) const
{
	AfxAssert(FALSE);

	return 0;
	}

UINT CUSBPortItem::GetType(void) const
{
	AfxAssert(FALSE);

	return typeNull;
	}

// End of File
