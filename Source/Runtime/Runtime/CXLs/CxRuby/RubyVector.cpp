
#include "Intern.hpp"

#include "RubyVector.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Debugging

void CRubyVector::Trace(PCTXT pName) const
{
	AfxTrace("%s = (%.4f, %.4f)\n", pName, m_x, m_y);
	}

// End of File
