
#include "intern.hpp"

#include "SymFactLibrary.hpp"

#include "SymFactCategory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Library
//

// Global Data

global IManagedInterface * g_pSymHelp = NULL;

// Constructor

CSymFactLibrary::CSymFactLibrary(void)
{
	m_pCats  = NULL;

	m_uCount = 0;

	LoadHelper();

	LoadLibrary();
	}

// Destructor

CSymFactLibrary::~CSymFactLibrary(void)
{
	if( g_pSymHelp ) {

		g_pSymHelp->Release();
		}

	delete [] m_pCats;
	}

// Indexing

CSymFactCategory * const CSymFactLibrary::operator [] (UINT n) const
{
	AfxAssert(n < m_uCount);

	return n < m_uCount ? m_pCats + n : NULL;
	}

// Attributes

UINT CSymFactLibrary::GetCatCount(void) const
{
	return m_uCount;
	}

// Operations

void CSymFactLibrary::LoadLibrary(void)
{
	for( UINT p = 0; p < 2; p++ ) {

		UINT c, n;

		for( c = 0, n = 2;; n++ ) {

			CPrintf File(L"CatFile%2.2u", n);

			UINT   uSize1 = 0;

			HANDLE hFile1 = afxModule->LoadResource(CEntity(File), L"SFCAT1", uSize1);

			UINT   uSize3 = 0;

			HANDLE hFile3 = afxModule->LoadResource(CEntity(File), L"SFCAT3", uSize3);

			if( hFile1 || hFile3 ) {

				if( p ) {

					BOOL fLoad = FALSE;

					if( hFile3 ) {

						if( g_pSymHelp ) {

							PCBYTE pFile3 = PCBYTE(LockResource(hFile3));

							char * pText  = new char [ uSize3 + 1 ];

							memcpy(pText, pFile3, uSize3);

							pText[uSize3] = 0;

							m_pCats[c].ParseCat3(n, pText);

							delete[] pText;

							fLoad = TRUE;
							}

						UnlockResource(hFile3);

						FreeResource(hFile3);
						}

					if( hFile1 ) {

						PCBYTE pFile1 = PCBYTE(LockResource(hFile1));

						m_pCats[c].ParseCat1(n, pFile1);

						fLoad = TRUE;
						}

					if( fLoad ) {

						c++;
						}
					}
				else {
					if( hFile1 || (hFile3 && g_pSymHelp) ) {

						c++;
						}

					FreeResource(hFile1);

					FreeResource(hFile3);
					}

				continue;
				}

			break;
			}

		if( p == 0 ) {

			m_uCount = c;

			m_pCats  = New CSymFactCategory [ c ];
			}
		}
	}

void CSymFactLibrary::LoadHelper(void)
{
	CoCreateInstance( CLSID_SymbolExtraction,
			  NULL, 
			  CLSCTX_INPROC_SERVER,
			  IID_IManagedInterface, 
			  (void **) &g_pSymHelp
			  );

	if( !g_pSymHelp ) {

		AfxTrace(L"Could not load SWTB helper\n");
		}
	}

// End of File
