//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define DATA_MAX	60
#define TIME_OUT	1000

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// CBaldorMintHostSerialDriver
//

class CBaldorMintHostSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CBaldorMintHostSerialDriver(void);

		// Destructor
		~CBaldorMintHostSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE	m_bCard;
			BYTE	m_bHCP;
			};

		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		BYTE	   m_bTxBuff[72];
		BYTE	   m_bRxBuff[72];
		UINT	   m_uPtr;
		BYTE       m_bCheck;
		char 	   m_Data[60];
			
		// Implementation
		void Start(void);
		void AddByte(BYTE bByte);
										
		// Transport Layer
		BOOL Transact(void);
		BOOL Send(void);
		BOOL Recv(void);

		// Helpers
		void  InitData(void);
		BOOL  IsReal(UINT uType);
		DWORD GetReal(UINT uBytes);
		DWORD GetInt(UINT uBytes);
	};

// End of File
