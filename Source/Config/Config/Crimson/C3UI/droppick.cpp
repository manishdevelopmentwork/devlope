
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Drop Down List with Pick
//

// Dynamic Class

AfxImplementDynamicClass(CUIDropPick, CUIControl);

// Constructor

CUIDropPick::CUIDropPick(void)
{
	m_fEdit       = FALSE;

	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CComboBox;

	m_pPickCtrl   = NewHotLink();
	}

// Core Overridables

void CUIDropPick::OnBind(void)
{
	CUIElement::OnBind();

	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}

	m_Verb = m_pText->GetVerb() + L"...";
	}

void CUIDropPick::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayDropDown(m_pText, TRUE);

	m_pPickLayout = New CLayItemText(m_Verb, CSize(6, 4), CSize(1, 1));

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pDataLayout, horzLeft | vertCenter));
	
	m_pMainLayout->AddItem(New CLayFormPad(m_pPickLayout, horzNone | vertCenter));

	pForm->AddItem(New CLayFormPad (m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIDropPick::OnCreate(CWnd &Wnd, UINT &uID)
{
	DWORD dwStyle = m_fEdit ? CBS_DROPDOWN  : CBS_DROPDOWNLIST;

	UINT  uEvent  = m_fEdit ? CBN_KILLFOCUS : CBN_SELCHANGE;

	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP | WS_VSCROLL | dwStyle,
			     FindComboRect(),
			     Wnd,
			     uID++
			     );

	m_pPickCtrl->Create( m_Verb,
			     BS_PUSHBUTTON | WS_TABSTOP,
			     m_pPickLayout->GetRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	m_pPickCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, uEvent);

	AddControl(m_pPickCtrl);

	LockList();

	LoadList();

	UnlockList();
	}

void CUIDropPick::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(FindComboRect(), TRUE);

	m_pPickCtrl->MoveWindow(m_pPickLayout->GetRect(), TRUE);
	}

// Data Overridables

void CUIDropPick::OnLoad(void)
{
	if( m_pText->HasFlag(textRefresh) ) {

		LockList();

		m_pDataCtrl->ResetContent();

		LoadList();

		m_pDataCtrl->SelectStringExact(m_pText->GetAsText());

		UnlockList();

		return;
		}

	m_pDataCtrl->SelectStringExact(m_pText->GetAsText());
	}

UINT CUIDropPick::OnSave(BOOL fUI)
{
	CString Text = m_pDataCtrl->GetWindowText();

	Text.TrimLeft();

	return StdSave(fUI, Text);
	}

// Notification Handlers

BOOL CUIDropPick::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pPickCtrl->GetID() ) {

		if( m_pText->HasFlag(textExpand) ) {

			if( m_pText->ExpandData(*m_pPickCtrl) ) {

				LoadUI();
				
				return TRUE;
				}			
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

CRect CUIDropPick::FindComboRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.top    = Rect.top   - 1;

	Rect.bottom = Rect.top   + 8 * Rect.cy();

	return Rect;
	}

UINT CUIDropPick::LoadList(void)
{
	CStringArray List;

	m_pText->EnumValues(List);

	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		m_pDataCtrl->AddString(List[n]);
		}

	return uCount;
	}

BOOL CUIDropPick::Reload(void)
{
	if( m_pDataCtrl->IsWindow() ) {

		LockList();

		m_pDataCtrl->ResetContent();

		LoadList();

		LoadUI();

		UnlockList();

		return TRUE;
		}

	return FALSE;
	}

void CUIDropPick::LockList(void)
{
	m_pDataCtrl->SetRedraw(FALSE);

	m_pDataCtrl->SendMessage(CB_SETMINVISIBLE, 1);
	}

void CUIDropPick::UnlockList(void)
{
	m_pDataCtrl->SendMessage(CB_SETMINVISIBLE, 8);

	m_pDataCtrl->SetRedraw(TRUE);

	m_pDataCtrl->Invalidate(TRUE);
	}

// End of File
