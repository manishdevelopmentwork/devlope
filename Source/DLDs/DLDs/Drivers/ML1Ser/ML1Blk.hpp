#ifndef	INCLUDE_ML1BLK_HPP
	
#define	INCLUDE_ML1BLK_HPP

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
//  Debugging Help
//

static BOOL ML1_BLK_DEBUG = 0;

//////////////////////////////////////////////////////////////////////////
//
//  ML1 Enumerations
//

enum Flags {

	opRead  = 0x01,
	opWrite = 0x02,
	opSShot = 0x04,
	opForce	= 0x08,
	};

enum Mode {

	modeReply = 0,
	modeReqst = 1,
	};

//////////////////////////////////////////////////////////////////////////
//
//  Forward Declarations
//

class CCodedItem;

//////////////////////////////////////////////////////////////////////////
//
//  ML1 Block Implementation
//

class CML1Blk
{
	public:
		// Constructor
		CML1Blk(IHelper * pHelp);

		// Destructor
		~CML1Blk(void);

		// Initialization
		void   Init(void);
		
		// Data Access
		AREF  GetAddr(void);
		void  SetAddr(DWORD dwAddr);
		WORD  GetSize(void);
		void  SetSize(WORD wSize);
		void  SetCOV(BYTE bCOV);
		void  SetTO(WORD wTO);
		void  SetRetry(BYTE bRetry);
		void  SetCoded(PVOID pCoded);
		void  SetRxTime(BYTE bMode);
		void  SetTxTime(BYTE bMode);
		UINT  GetData(AREF Addr, PDWORD pData, UINT uCount);
		UINT  SetData(AREF Addr, PDWORD pData, UINT uCount);
		BYTE  GetTxByte(UINT uOffset);
		WORD  GetTxWord(UINT uOffset);
		DWORD GetTxLong(UINT uOffset);
		void  SetRxByte(UINT uOffset, BYTE bData);
		void  SetRxWord(UINT uOffset, WORD wData);
		void  SetRxBuff(UINT uOffset, PBYTE pData, UINT uBytes);
		BYTE  SendOnChange(void);
		BOOL  FindOpParams(AREF Addr, UINT &uOffset, UINT &uCount);
		BOOL  IsPending(BYTE bOp);
		BOOL  IsTrans(BYTE bOp, WORD wTrans);
		BOOL  IsTimedOut(BYTE bOp, BOOL fMode);
		BOOL  IsExhausted(BYTE bOp);
		BOOL  Force(void);
		void  SetNext(BYTE bOp);
		UINT  GetNext(UINT uOffset, BYTE bOp);
		BOOL  IsLong(void);
				
		// Operation
		BOOL  Execute(IExtraHelper * pExtra);
		BOOL  Request(BYTE bOp, BOOL fSend);
		void  MarkComplete(BYTE bOp, BYTE bMode);
		void  Mark(BYTE bOp);
		BOOL  ClrPend(BYTE bOp);
		BOOL  IncPend(BYTE bOp);
		
	protected:

		// Data Members
		DWORD	  m_Addr;
		WORD	  m_Size;
		WORD      m_Bytes;
		BYTE	  m_COV;
		DWORD	  m_TO;
		BYTE	  m_Retry;
		PVOID	  m_pRBE;
		BYTE	  m_Flags;
		BYTE	  m_Pend[opWrite];
		WORD	  m_Tran[opWrite];
		PBYTE	  m_pTx;
		PBYTE	  m_pRx;
		UINT	  m_RxTime[opWrite];
		UINT	  m_TxTime[opWrite];
		CML1Blk * m_pPrev;
		CML1Blk * m_pNext;
		BYTE	  m_Next[opWrite];
				
		// IHelper
		IHelper * m_pHelp;
								
		// Implementation
		UINT GetBitData (AREF Addr, PDWORD pData, UINT uCount);
		UINT GetWordData(AREF Addr, PDWORD pData, UINT uCount);
		UINT GetLongData(AREF Addr, PDWORD pData, UINT uCount);
		UINT SetBitData (AREF Addr, PDWORD pData, UINT uCount);
		UINT SetWordData(AREF Addr, PDWORD pData, UINT uCount);
		UINT SetLongData(AREF Addr, PDWORD pData, UINT uCount);
		BOOL SetFlags(BYTE bFlag);
		BOOL ClrFlags(BYTE bFlag);
		
		// Friends
		friend class CML1Base;
		friend class CML1Server;
		friend class CML1Driver;
		friend class CML1SerialDriver;
		friend class CML1TcpDriver;
		friend class CML1Tester;
		friend class CML1SerialTester;
		friend class CML1TcpTester;
		
		// Debugging Help
		void ShowDebug(PCTXT pName, ...);
		void Show(void);
		
	};

#endif

// End of File
