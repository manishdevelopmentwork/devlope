
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_OpcObjectTypeNode_HPP

#define INCLUDE_OpcObjectTypeNode_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "OpcNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Type Node
//

class COpcObjectTypeNode : public COpcNode
{
	public:
		// Constructors
		COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract);
		COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract);
		COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract);
		COpcObjectTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract);
		COpcObjectTypeNode(COpcObjectTypeNode const &That);

		// Assignment
		COpcObjectTypeNode operator = (COpcObjectTypeNode const &That);

		// Attributes
		bool IsAbstract(void) const;

	protected:
		// Data Members
		bool m_fAbstract;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE bool COpcObjectTypeNode::IsAbstract(void) const
{
	return m_fAbstract;
	}

// End of File

#endif
