
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Trace Helpers
//

// Base Address

clink BYTE _base[];

// Demangler

extern bool GccDemangleName(char *pName, UINT uName, PCTXT pMangled);

// Module Location

global BOOL FindModule(DWORD dwAddr, char *pName, DWORD &dwBase);

// Prototypes

global	void	ArmStackTrace(IDiagOutput *pOut);
global	void	ArmStackTrace(IDiagOutput *pOut, DWORD pc, DWORD fp);
global	void	ArmStackTrace(PBYTE &pData, DWORD pc, DWORD fp);
global	bool	ArmGetFuncName(char *pName, UINT uName, DWORD pc, DWORD fp, UINT uFlags);
static	bool	FormatName(char *pName, UINT uName, DWORD pc, DWORD dwFunc, UINT uFlags);
static	bool	IsValidFp(DWORD fp);
static	bool	IsValidPc(DWORD pc);

// Code

global void ArmStackTrace(IDiagOutput *pOut)
{
	DWORD pc, fp;

	#if !defined(__INTELLISENSE__)

	ASM (	"mov %0, fp"
		: "=r"(fp)
		);

	ASM (	"mov %0, pc"
		: "=r"(pc)
		);
	
	#endif

	ArmStackTrace(pOut, pc, fp);
	}

global void ArmStackTrace(IDiagOutput *pOut, DWORD pc, DWORD fp)
{
	char sName[256];

	while( fp && pc ) {		

		if( IsValidFp(fp) && IsValidPc(pc) ) {

			bool fFind = ArmGetFuncName(sName, sizeof(sName), pc, fp, 3);

			if( pOut ) {

				pOut->Print("    %s\n", sName);
				}
			else
				AfxTrace("    %s\n", sName);

			if( fFind ) {

				PDWORD pFrame = PDWORD(fp);

				pc = pFrame[-1];

				fp = pFrame[-3];

				continue;
				}
			}

		break;
		}
	}

global void ArmStackTrace(PBYTE &pData, DWORD pc, DWORD fp)
{
	UINT n = 0; 

	while( fp && pc && ++n < 16 ) {

		if( IsValidFp(fp) && IsValidPc(pc) ) {

			(PDWORD(pData))[0] = pc;

			pData += sizeof(DWORD);

			PDWORD pFrame = PDWORD(fp);

			pc = pFrame[-1];

			fp = pFrame[-3];

			continue;
			}

		break;
		}
	}

global bool ArmGetFuncName(char *pName, UINT uName, DWORD pc, DWORD fp, UINT uFlags)
{
	DWORD dwFunc = 1;

	if( fp ) {

		dwFunc = PDWORD(fp)[0] - 12;

		if( IsValidPc(dwFunc) ) {

			#if defined(_DEBUG)

			// We look back for up to 16 instructions to
			// deal with bad code generation from GCC...

			DWORD dwFlag = PDWORD(dwFunc)[-1];

			for( UINT n = 0; n < 16; n++ ) {

				if( (dwFlag & 0xFFFFF000) != 0xFF000000 ) {

					dwFunc = dwFunc - 4;

					dwFlag = PDWORD(dwFunc)[-1];

					continue;
					}

				break;
				}

			#endif
			}
		}

	if( !IsValidPc(dwFunc) ) {

		#if defined(_DEBUG)

		PDWORD pCode = PDWORD(pc);

		for( UINT n = 0; n < 8192; n++ ) {

			if( (pCode[-n] & 0xFFFFF000) == 0xFF000000 ) {

				dwFunc = DWORD(pCode - n + 1);

				break;
				}
			}

		#endif
		}

	if( IsValidPc(dwFunc) ) {

		return FormatName(pName, uName, pc, dwFunc, uFlags);
		}

	if( IsValidPc(pc) ) {

		return FormatName(pName, uName, pc, pc, uFlags);
		}

	siprintf(pName, "%8.8X ???", pc);

	return false;
	}

static bool FormatName(char *pName, UINT uName, DWORD pc, DWORD dwFunc, UINT uFlags)
{
	bool   fNamed = false;

	DWORD  dwFlag = PDWORD(dwFunc)[-1];

	UINT   uPrint = 0;

	DWORD  dwBase = DWORD(_base);

	char   sModule[64] = "Host";

	FindModule(dwFunc, sModule, dwBase);

	if( uFlags & 1 ) {

		DWORD dwAddr = dwFunc - dwBase + 0xBA000000;

		if( pc < dwFunc ) {
				
			uPrint += siprintf(	pName + uPrint,
						"%8.8X (%8.8X) - %4.4X = ",
						UINT(dwFunc),
						UINT(dwAddr),
						WORD(dwFunc - pc)
						);
			}
		else {
			uPrint += siprintf(	pName + uPrint,
						"%8.8X (%8.8X) + %4.4X = ",
						UINT(dwFunc),
						UINT(dwAddr),
						WORD(pc - dwFunc)
						);
			}
		}

	#if defined(_DEBUG)

	if( (dwFlag & 0xFFFFF000) == 0xFF000000 ) {

		UINT  uBytes = LOWORD(dwFlag);

		PCTXT pPoked = PCTXT(dwFunc - 4 - uBytes);

		GccDemangleName(pName + uPrint, uName - uPrint, pPoked);

		uPrint += strlen(pName + uPrint);

		fNamed  = true;
		}
	else {
		strcpy(pName + uPrint, "Unknown");

		uPrint += strlen(pName + uPrint);
		}

	#else

	if( uFlags & 1 ) {

		strcpy(pName + uPrint, "Unavailable");

		uPrint += strlen(pName + uPrint);

		fNamed  = true;
		}
	else {
		DWORD dwAddr = dwFunc - dwBase + 0xBA000000;

		if( pc < dwFunc ) {
				
			uPrint += siprintf(	pName + uPrint,
						"%8.8X (%8.8X) - %4.4X",
						UINT(dwFunc),
						UINT(dwAddr),
						WORD(dwFunc - pc)
						);
			}
		else {
			uPrint += siprintf(	pName + uPrint,
						"%8.8X (%8.8X) + %4.4X",
						UINT(dwFunc),
						UINT(dwAddr),
						WORD(pc - dwFunc)
						);
			}

		fNamed  = true;
		}

	#endif

	if( uFlags & 2 ) {

		siprintf( pName + strlen(pName),
				" in %s",
				sModule
				);
		}

	return fNamed;
	}

static bool IsValidFp(DWORD fp)
{
	return !(fp & 3) && phal->IsVirtualValid(fp, true);
	}

static bool IsValidPc(DWORD pc)
{
	return !(pc & 3) && phal->IsVirtualValid(pc, false);
	}

// End of File
