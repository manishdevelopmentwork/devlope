
#include "intern.hpp"

#include "legacy.h"

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8in 6out simple DIO Item
//

// Runtime Class

AfxImplementRuntimeClass(CDIO14ModCfg, CCommsItem);

// Property List

CCommsList CDIO14ModCfg::m_CommsList[] = {
		
	{ 1, "Input1",		PROPID_INP_STATE1,		usageRead,	IDS_NAME_I1	},
	{ 1, "Input2",		PROPID_INP_STATE2,		usageRead,	IDS_NAME_I2	},
	{ 1, "Input3",		PROPID_INP_STATE3,		usageRead,	IDS_NAME_I3	},
	{ 1, "Input4",		PROPID_INP_STATE4,		usageRead,	IDS_NAME_I4	},
	{ 1, "Input5",		PROPID_INP_STATE5,		usageRead,	IDS_NAME_I5	},
	{ 1, "Input6",		PROPID_INP_STATE6,		usageRead,	IDS_NAME_I6	},
	{ 1, "Input7",		PROPID_INP_STATE7,		usageRead,	IDS_NAME_I7	},
	{ 1, "Input8",		PROPID_INP_STATE8,		usageRead,	IDS_NAME_I8	},
		
	{ 1, "InputCoil1",	PROPID_SINP_STATE1,		usageWriteBoth,	IDS_NAME_IC1	},
	{ 1, "InputCoil2",	PROPID_SINP_STATE2,		usageWriteBoth,	IDS_NAME_IC2	},
	{ 1, "InputCoil3",	PROPID_SINP_STATE3,		usageWriteBoth,	IDS_NAME_IC3	},
	{ 1, "InputCoil4",	PROPID_SINP_STATE4,		usageWriteBoth,	IDS_NAME_IC4	},
	{ 1, "InputCoil5",	PROPID_SINP_STATE5,		usageWriteBoth,	IDS_NAME_IC5	},
	{ 1, "InputCoil6",	PROPID_SINP_STATE6,		usageWriteBoth,	IDS_NAME_IC6	},
	{ 1, "InputCoil7",	PROPID_SINP_STATE7,		usageWriteBoth,	IDS_NAME_IC7	},
	{ 1, "InputCoil8",	PROPID_SINP_STATE8,		usageWriteBoth,	IDS_NAME_IC8	},
	
	{ 2, "Output1",		PROPID_OUT_STATE1,		usageWriteBoth,	IDS_NAME_O1	},
	{ 2, "Output2",		PROPID_OUT_STATE2,		usageWriteBoth,	IDS_NAME_O2	},
	{ 2, "Output3",		PROPID_OUT_STATE3,		usageWriteBoth,	IDS_NAME_O3	},
	{ 2, "Output4",		PROPID_OUT_STATE4,		usageWriteBoth,	IDS_NAME_O4	},
	{ 2, "Output5",		PROPID_OUT_STATE5,		usageWriteBoth,	IDS_NAME_O5	},
	{ 2, "Output6",		PROPID_OUT_STATE6,		usageWriteBoth,	IDS_NAME_O6	},
	
	{ 2, "OutputCoil1",	PROPID_SOUT_STATE1,		usageRead,	IDS_NAME_OC1	},
	{ 2, "OutputCoil2",	PROPID_SOUT_STATE2,		usageRead,	IDS_NAME_OC2	},
	{ 2, "OutputCoil3",	PROPID_SOUT_STATE3,		usageRead,	IDS_NAME_OC3	},
	{ 2, "OutputCoil4",	PROPID_SOUT_STATE4,		usageRead,	IDS_NAME_OC4	},
	{ 2, "OutputCoil5",	PROPID_SOUT_STATE5,		usageRead,	IDS_NAME_OC5	},
	{ 2, "OutputCoil6",	PROPID_SOUT_STATE6,		usageRead,	IDS_NAME_OC6	},
	{ 2, "OutputCoil7",	PROPID_SOUT_STATE7,		usageRead,	IDS_NAME_OC7	},
	{ 2, "OutputCoil8",	PROPID_SOUT_STATE8,		usageRead,	IDS_NAME_OC8	},
	
	{ 3, "CounterPreset1",	PROPID_COUNTER_PRE1,		usageWriteBoth,	IDS_NAME_CP1	},
	{ 3, "CounterPreset2",	PROPID_COUNTER_PRE2,		usageWriteBoth,	IDS_NAME_CP2	},
	{ 3, "CounterPreset3",	PROPID_COUNTER_PRE3,		usageWriteBoth,	IDS_NAME_CP3	},
	{ 3, "CounterPreset4",	PROPID_COUNTER_PRE4,		usageWriteBoth,	IDS_NAME_CP4	},
	{ 3, "CounterPreset5",	PROPID_COUNTER_PRE5,		usageWriteBoth,	IDS_NAME_CP5	},
	{ 3, "CounterPreset6",	PROPID_COUNTER_PRE6,		usageWriteBoth,	IDS_NAME_CP6	},
	{ 3, "CounterPreset7",	PROPID_COUNTER_PRE7,		usageWriteBoth,	IDS_NAME_CP7	},
	{ 3, "CounterPreset8",	PROPID_COUNTER_PRE8,		usageWriteBoth,	IDS_NAME_CP8	},

	{ 3, "TimerPreset1",	PROPID_TIMER_PRE1,		usageWriteBoth,	IDS_NAME_TP1	},
	{ 3, "TimerPreset2",	PROPID_TIMER_PRE2,		usageWriteBoth,	IDS_NAME_TP2	},
	{ 3, "TimerPreset3",	PROPID_TIMER_PRE3,		usageWriteBoth,	IDS_NAME_TP3	},
	{ 3, "TimerPreset4",	PROPID_TIMER_PRE4,		usageWriteBoth,	IDS_NAME_TP4	},
	{ 3, "TimerPreset5",	PROPID_TIMER_PRE5,		usageWriteBoth,	IDS_NAME_TP5	},
	{ 3, "TimerPreset6",	PROPID_TIMER_PRE6,		usageWriteBoth,	IDS_NAME_TP6	},
	{ 3, "TimerPreset7",	PROPID_TIMER_PRE7,		usageWriteBoth,	IDS_NAME_TP7	},
	{ 3, "TimerPreset8",	PROPID_TIMER_PRE8,		usageWriteBoth,	IDS_NAME_TP8	},

	{ 4, "CounterValue1",	PROPID_COUNTER_VAL1,		usageRead,	IDS_NAME_CV1	},
	{ 4, "CounterValue2",	PROPID_COUNTER_VAL2,		usageRead,	IDS_NAME_CV2	},
	{ 4, "CounterValue3",	PROPID_COUNTER_VAL3,		usageRead,	IDS_NAME_CV3	},
	{ 4, "CounterValue4",	PROPID_COUNTER_VAL4,		usageRead,	IDS_NAME_CV4	},
	{ 4, "CounterValue5",	PROPID_COUNTER_VAL5,		usageRead,	IDS_NAME_CV5	},
	{ 4, "CounterValue6",	PROPID_COUNTER_VAL6,		usageRead,	IDS_NAME_CV6	},
	{ 4, "CounterValue7",	PROPID_COUNTER_VAL7,		usageRead,	IDS_NAME_CV7	},
	{ 4, "CounterValue8",	PROPID_COUNTER_VAL8,		usageRead,	IDS_NAME_CV8	},

	{ 4, "TimerValue1",	PROPID_TIMER_VAL1,		usageRead,	IDS_NAME_TV1	},
	{ 4, "TimerValue2",	PROPID_TIMER_VAL2,		usageRead,	IDS_NAME_TV2	},
	{ 4, "TimerValue3",	PROPID_TIMER_VAL3,		usageRead,	IDS_NAME_TV3	},
	{ 4, "TimerValue4",	PROPID_TIMER_VAL4,		usageRead,	IDS_NAME_TV4	},
	{ 4, "TimerValue5",	PROPID_TIMER_VAL5,		usageRead,	IDS_NAME_TV5	},
	{ 4, "TimerValue6",	PROPID_TIMER_VAL6,		usageRead,	IDS_NAME_TV6	},
	{ 4, "TimerValue7",	PROPID_TIMER_VAL7,		usageRead,	IDS_NAME_TV7	},
	{ 4, "TimerValue8",	PROPID_TIMER_VAL8,		usageRead,	IDS_NAME_TV8	},

	{ 5, "LogicDisable",	PROPID_ENABLE_LOGIC,		usageWriteBoth,	IDS_NAME_LR	},
	
	{ 0, "InputMode1",	PROPID_INPUT_MODE1,		usageWriteInit,	IDS_NAME_IM1	},
	{ 0, "InputMode2",	PROPID_INPUT_MODE2,		usageWriteInit,	IDS_NAME_IM2	},
	{ 0, "InputMode3",	PROPID_INPUT_MODE3,		usageWriteInit,	IDS_NAME_IM3	},
	{ 0, "InputMode4",	PROPID_INPUT_MODE4,		usageWriteInit,	IDS_NAME_IM4	},
	{ 0, "InputMode5",	PROPID_INPUT_MODE5,		usageWriteInit,	IDS_NAME_IM5	},
	{ 0, "InputMode6",	PROPID_INPUT_MODE6,		usageWriteInit,	IDS_NAME_IM6	},
	{ 0, "InputMode7",	PROPID_INPUT_MODE7,		usageWriteInit,	IDS_NAME_IM7	},
	{ 0, "InputMode8",	PROPID_INPUT_MODE8,		usageWriteInit,	IDS_NAME_IM8	},
	{ 0, "LogicDisable",	PROPID_LOGIC_RS,		usageWriteInit,	IDS_NAME_LR	},

	};

// Constructor

CDIO14ModCfg::CDIO14ModCfg(void)
{
	m_Input1	= FALSE;
	m_Input2	= FALSE;
	m_Input3	= FALSE;
	m_Input4	= FALSE;
	m_Input5	= FALSE;
	m_Input6	= FALSE;
	m_Input7	= FALSE;
	m_Input8	= FALSE;
	
	m_Output1	= FALSE;
	m_Output2	= FALSE;
	m_Output3	= FALSE;
	m_Output4	= FALSE;
	m_Output5	= FALSE;
	m_Output6	= FALSE;
		
	m_InputMode1	= ACTIVE_HI;
	m_InputMode2	= ACTIVE_HI;
	m_InputMode3	= ACTIVE_HI;
	m_InputMode4	= ACTIVE_HI;
	m_InputMode5	= ACTIVE_HI;
	m_InputMode6	= ACTIVE_HI;
	m_InputMode7	= ACTIVE_HI;
	m_InputMode8	= ACTIVE_HI;
	
	m_IncludeDownload = NO;

	m_LogicDisable    = STOP;

	InitLogic();
	
	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CDIO14ModCfg::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(IDS_MODULE_INPUTS);

		case 2:	return CString(IDS_MODULE_OUTPUTS);

		case 3:	return CString(IDS_MODULE_CTPRSTS);
		
		case 4:	return CString(IDS_MODULE_CTVALS);

		case 5:	return CString(IDS_MODULE_CTRL);
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CDIO14ModCfg::GetPageCount(void)
{
	return 2;
	}

CString CDIO14ModCfg::GetPageName(UINT n)
{
	switch( n ) {

		case 0: return CString(IDS_MODULE_CONFIG);
		
		case 1: return CString(IDS_MODULE_LOGED);
		
		}

	return L"";
	}

CViewWnd * CDIO14ModCfg::CreatePage(UINT n)
{
	switch( n ) {

		case 0: return New CDIO14ConfigWnd;
							
		case 1: return New CDIO14LogicWnd;
		}

	return NULL;
	}

// Implementation

void CDIO14ModCfg::AddMetaData(void)
{
	
	Meta_AddInteger(Input1);
	Meta_AddInteger(Input2);
	Meta_AddInteger(Input3);
	Meta_AddInteger(Input4);
	Meta_AddInteger(Input5);
	Meta_AddInteger(Input6);
	Meta_AddInteger(Input7);
	Meta_AddInteger(Input8);

	Meta_AddInteger(Output1);
	Meta_AddInteger(Output2);
	Meta_AddInteger(Output3);
	Meta_AddInteger(Output4);
	Meta_AddInteger(Output5);
	Meta_AddInteger(Output6);
		
	Meta_AddInteger(InputMode1);
	Meta_AddInteger(InputMode2);
	Meta_AddInteger(InputMode3);
	Meta_AddInteger(InputMode4);
	Meta_AddInteger(InputMode5);
	Meta_AddInteger(InputMode6);
	Meta_AddInteger(InputMode7);
	Meta_AddInteger(InputMode8);
		
	Meta_AddInteger(InputCoil1);
	Meta_AddInteger(InputCoil2);
	Meta_AddInteger(InputCoil3);
	Meta_AddInteger(InputCoil4);
	Meta_AddInteger(InputCoil5);
	Meta_AddInteger(InputCoil6);
	Meta_AddInteger(InputCoil7);
	Meta_AddInteger(InputCoil8);
	
	Meta_AddInteger(OutputCoil1);
	Meta_AddInteger(OutputCoil2);
	Meta_AddInteger(OutputCoil3);
	Meta_AddInteger(OutputCoil4);
	Meta_AddInteger(OutputCoil5);
	Meta_AddInteger(OutputCoil6);
	Meta_AddInteger(OutputCoil7);
	Meta_AddInteger(OutputCoil8);
	
	Meta_AddInteger(CounterPreset1);
	Meta_AddInteger(CounterPreset2);
	Meta_AddInteger(CounterPreset3);
	Meta_AddInteger(CounterPreset4);
	Meta_AddInteger(CounterPreset5);
	Meta_AddInteger(CounterPreset6);
	Meta_AddInteger(CounterPreset7);
	Meta_AddInteger(CounterPreset8);

	Meta_AddInteger(CounterValue1);
	Meta_AddInteger(CounterValue2);
	Meta_AddInteger(CounterValue3);
	Meta_AddInteger(CounterValue4);
	Meta_AddInteger(CounterValue5);
	Meta_AddInteger(CounterValue6);
	Meta_AddInteger(CounterValue7);
	Meta_AddInteger(CounterValue8);

	Meta_AddInteger(TimerPreset1);
	Meta_AddInteger(TimerPreset2);
	Meta_AddInteger(TimerPreset3);
	Meta_AddInteger(TimerPreset4);
	Meta_AddInteger(TimerPreset5);
	Meta_AddInteger(TimerPreset6);
	Meta_AddInteger(TimerPreset7);
	Meta_AddInteger(TimerPreset8);

	Meta_AddInteger(TimerValue1);
	Meta_AddInteger(TimerValue2);
	Meta_AddInteger(TimerValue3);
	Meta_AddInteger(TimerValue4);
	Meta_AddInteger(TimerValue5);
	Meta_AddInteger(TimerValue6);
	Meta_AddInteger(TimerValue7);
	Meta_AddInteger(TimerValue8);

	Meta_AddInteger(LogicDisable);
	Meta_AddInteger(IncludeDownload);
	}

BOOL CDIO14ModCfg::IncludeProp(WORD PropID)
{
	if(PropID == PROPID_ENABLE_LOGIC) return FALSE;
	
	if(m_IncludeDownload == NO) {

		switch( PropID ) {
						
			case PROPID_LOGIC_RS:
				
				return FALSE;
			}
		}

	return TRUE;
	}

void CDIO14ModCfg::InitLogic(void)
{
	m_NSI = START_USER_SYM;

	m_NumWires = 1;

	m_NumEquations = 0;
	
	DefRefSymbols();	

	UINT i;

	for(i = INP0; i <= INP7; i++) m_InputState[i-INP0] = 0;
		
	for(i = 0; i < NUM_SYMS; i++) m_InputFlag[i] = 0;

	SetInputStates();
	
	for(i = OUT0; i <= OUT5; i++) m_EResult[i] = 0;
	}

void CDIO14ModCfg::DefRefSymbols(void)
{
	/*------------------------------------------
	Define the basic reference symbol attributes
	------------------------------------------*/
	
	WORD i,inst;
	
	CRect Rect;

	Rect.left = 0;
	
	Rect.top = 0;

	//---------------------------------
		
	inst = JCT_PROTO;
	
	m_Sym[inst].Left       = 0;
	m_Sym[inst].Top        = 0;
	m_Sym[inst].Width      = 5;
	m_Sym[inst].Height     = 5;
	m_Sym[inst].Type       = JCT;
	m_Sym[inst].NumInputs  = 4;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 1;
	m_Sym[inst].PinOffset  = 3;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}

	UINT xPos = 10;

	UINT xSpacing = 35;

	UINT yPos = 30;

	//---------------------------------
		
	inst = AND_PROTO;
		
	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = AND;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;
	
	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;
	
	//---------------------------------
		
	inst = OR_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = OR;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = XOR_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = XOR;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;
	
	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;	

	//---------------------------------
		
	inst = NOT_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = NOT;
	m_Sym[inst].NumInputs  = 1;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = TIMERUP_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = TIMERUP;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;
	
	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = TIMERDN_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = TIMERDN;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;
	
	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = COUNTUP_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = COUNTUP;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = COUNTDN_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = COUNTDN;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = LATCH_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = LAT;
	m_Sym[inst].NumInputs  = 2;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;

	//---------------------------------
		
	inst = INCOIL_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = INC;
	m_Sym[inst].NumInputs  = 0;
	m_Sym[inst].NumOutputs = 1;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}
	
	xPos += xSpacing;
	
	//---------------------------------

	inst = OUTCOIL_PROTO;

	m_Sym[inst].Left       = Rect.left + xPos;
	m_Sym[inst].Top        = Rect.top + yPos;
	m_Sym[inst].Width      = 30;
	m_Sym[inst].Height     = 30;
	m_Sym[inst].Type       = OUTC;
	m_Sym[inst].NumInputs  = 1;
	m_Sym[inst].NumOutputs = 0;
	m_Sym[inst].PinSpace   = 10;
	m_Sym[inst].PinOffset  = 10;
	m_Sym[inst].BMPOffsetX = 0;
	
	for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

	for(i = 0; i < NUM_PINS; i++) {
		
		m_Sym[inst].PinWire[i] = 0;

		m_Sym[inst].WireEnd[i] = 0;
		}

	// cppcheck-suppress unreadVariable

	xPos += xSpacing;
	
	//---------------------------------

	for(inst = INP0; inst <= INP7; inst++) {

		m_Sym[inst].Left       = 15;
		m_Sym[inst].Top        = 119 + (inst-INP0)*40;
		m_Sym[inst].Width      = 20;
		m_Sym[inst].Height     = 12;
		m_Sym[inst].Type       = MINP;
		m_Sym[inst].NumInputs  = 0;
		m_Sym[inst].NumOutputs = 1;
		m_Sym[inst].PinSpace   = 0;
		m_Sym[inst].PinOffset  = 0;
		m_Sym[inst].BMPOffsetX = 0;
		
		for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

		for(i = 0; i < NUM_PINS; i++) {
			
			m_Sym[inst].PinWire[i] = 0;

			m_Sym[inst].WireEnd[i] = 0;
			}
		}

	//---------------------------------
	
	for(inst = OUT0; inst <= OUT5; inst++) {

		m_Sym[inst].Left       = 530;
		m_Sym[inst].Top        = 119 + (inst-OUT0)*40;
		m_Sym[inst].Width      = 20;
		m_Sym[inst].Height     = 10;
		m_Sym[inst].Type       = MOUT;
		m_Sym[inst].NumInputs  = 1;
		m_Sym[inst].NumOutputs = 0;
		m_Sym[inst].PinSpace   = 0;
		m_Sym[inst].PinOffset  = 6;
		m_Sym[inst].BMPOffsetX = 0;
		
		for(i = 0; i < NUM_PROPS; i++) m_Sym[inst].Prop[i] = 0;

		for(i = 0; i < NUM_PINS; i++) {
			
			m_Sym[inst].PinWire[i] = 0;

			m_Sym[inst].WireEnd[i] = 0;
			}
		}
	}

void CDIO14ModCfg::SetInputStates(void)
{
	/*------------------------------------

	------------------------------------*/
	
	for(UINT inst = INP0; inst <= INP7; inst++) {
				
		if(m_InputState[inst-INP0] == 1) {

			m_EResult[inst] = 1;
			}
		else {
			m_EResult[inst] = 0;
			}
		}
	}

void  CDIO14ModCfg::ArrayErr(UINT err)
{

	}

// Persistance

void CDIO14ModCfg::Init(void)
{
	CCommsItem::Init();
	}

void CDIO14ModCfg::Kill(void)
{
	CCommsItem::Kill();
	}

void CDIO14ModCfg::Load(CTreeFile &File)
{
	AddMeta();

	while( !File.IsEndOfData() ) {

		CString Name = File.GetName();

		if( Name == "Logic" ) {
			
			File.GetObject();

			LoadLogic(File);

			File.EndObject();
			}
		else {
			CMetaData const *pMeta = FindMetaData(Name);

			if( pMeta ) {

				LoadProp(File, pMeta);

				continue;
				}
			}
		}
	}

void CDIO14ModCfg::LoadLogic(CTreeFile &File)
{
	m_NSI = START_USER_SYM;
	
	m_NumWires = 1;

	File.GetName();
		
	UINT N = File.GetValueAsInteger();
	
	if(!N) return;
		
	m_NSI = N;

	UINT n;
		
	for(n = 1; n < m_NSI; n++) {

		File.GetName();
		
		File.GetObject();

		LoadLogicSymbol(File,m_Sym[n]);
		
		File.EndObject();
		}

	File.GetName();
	
	N = File.GetValueAsInteger();
	
	if(!N) return;
	
	m_NumWires = N+1;

	for(n = 1; n < N+1; n++) {

		File.GetName();

		File.GetObject();

		LoadWire(File,m_Wire[n]);

		File.EndObject();
		}
	}

void CDIO14ModCfg::LoadLogicSymbol(CTreeFile &File, SYMBOL &Sym)
{
	File.GetName();
	Sym.Left = File.GetValueAsInteger();

	File.GetName();
	Sym.Top = File.GetValueAsInteger();
	
	File.GetName();
	Sym.Width = File.GetValueAsInteger();
	
	File.GetName();
	Sym.Height = File.GetValueAsInteger();
	
	File.GetName();
	Sym.Type = File.GetValueAsInteger();
	
	File.GetName();
	Sym.NumInputs = File.GetValueAsInteger();
	
	File.GetName();
	Sym.NumOutputs = File.GetValueAsInteger();

	File.GetName();
	Sym.PinSpace = File.GetValueAsInteger();
	
	File.GetName();
	Sym.PinOffset = File.GetValueAsInteger();
	
	File.GetName();
	Sym.BMPOffsetX = File.GetValueAsInteger();

	UINT i;

	for(i = 0; i < NUM_PROPS; i++) {

		File.GetName();
		Sym.Prop[i] = (BYTE)File.GetValueAsInteger();
		}

	for(i = 0; i < Sym.NumInputs+1; i++) {
		
		File.GetName();
		Sym.PinWire[i] = File.GetValueAsInteger();
		}

	for(i = Sym.NumInputs+1; i < NUM_PINS; i++) Sym.PinWire[i] = 0;
		
	for(i = 0; i < Sym.NumInputs+1; i++) {
		
		File.GetName();
		Sym.WireEnd[i] = File.GetValueAsInteger();
		}

	for(i = Sym.NumInputs+1; i < NUM_PINS; i++) Sym.WireEnd[i] = 0;
	}

void CDIO14ModCfg::LoadWire(CTreeFile &File, WIRE &Wire)
{
	File.GetName();
	Wire.Sym1 = File.GetValueAsInteger();

	File.GetName();
	Wire.Pin1 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.Sym2 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.Pin2 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.x1 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.y1 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.x2 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.y2 = File.GetValueAsInteger();
	
	File.GetName();
	Wire.Frac = (double)File.GetValueAsInteger() / 100.0;
	
	File.GetName();
	Wire.Nfrac = (double)File.GetValueAsInteger() / 100.0;
	
	File.GetName();
	Wire.DrawMode = File.GetValueAsInteger();
	}

void CDIO14ModCfg::Save(CTreeFile &File)
{
	CCommsItem::Save(File);

	File.PutObject(L"Logic");

	SaveLogic(File);

	File.EndObject();
	}

void CDIO14ModCfg::SaveLogic(CTreeFile &File)
{
	TCHAR str[80],str1[80];
	
	UINT N = m_NSI;
	
	File.PutValue(L"NumSymbols",N);

	UINT n;

	for( n = 1; n < N; n++ ) {

		wstrcpy(str,L"Symbol");
		
		witoa(n,str1,10);

		wstrcat(str,str1);
		
		File.PutObject(str);

		SaveLogicSymbol(File, m_Sym[n]);

		File.EndObject();
		}
		
	File.PutValue(L"NumWires",m_NumWires-1);
	
	for(n = 1; n < m_NumWires; n++ ) {

		wstrcpy(str,L"Wire");
		
		witoa(n,str1,10);

		wstrcat(str,str1);
		
		File.PutObject(str);

		SaveWire(File, m_Wire[n]);

		File.EndObject();
		}
	}

void CDIO14ModCfg::SaveLogicSymbol(CTreeFile &File, SYMBOL &Sym)
{
	File.PutValue(L"Left      ", Sym.Left);
	File.PutValue(L"Top       ", Sym.Top);
	File.PutValue(L"Width     ", Sym.Width);
	File.PutValue(L"Height    ", Sym.Height);
	File.PutValue(L"Type      ", Sym.Type);
	File.PutValue(L"NumInputs ", Sym.NumInputs);
	File.PutValue(L"NumOutputs", Sym.NumOutputs);
	File.PutValue(L"PinSpace  ", Sym.PinSpace);
	File.PutValue(L"PinOffset ", Sym.PinOffset);
	File.PutValue(L"BMPOffsetX", Sym.BMPOffsetX);
	
	UINT i;

	for(i = 0; i < NUM_PROPS; i++)
		
		File.PutValue(L"Prop      ", (BYTE)(Sym.Prop[i]));
	
	for(i = 0; i < Sym.NumInputs+1; i++)
		
		File.PutValue(L"PinWire   ", Sym.PinWire[i]);

	for(i = 0; i < Sym.NumInputs+1; i++)
		
		File.PutValue(L"WireEnd   ", Sym.WireEnd[i]);
	}

void CDIO14ModCfg::SaveWire(CTreeFile &File, WIRE &Wire)
{
	File.PutValue(L"Sym1 ", Wire.Sym1);
	File.PutValue(L"Pin1 ", Wire.Pin1);
	File.PutValue(L"Sym2 ", Wire.Sym2);
	File.PutValue(L"Pin2 ", Wire.Pin2);
	File.PutValue(L"x1   ", Wire.x1);
	File.PutValue(L"y1   ", Wire.y1);
	File.PutValue(L"x2   ", Wire.x2);
	File.PutValue(L"y2   ", Wire.y2);
	File.PutValue(L"Frac ", (UINT)(Wire.Frac*100));
	File.PutValue(L"Nfrac", (UINT)(Wire.Nfrac*100));
	File.PutValue(L"Draw ", Wire.DrawMode);
	}

//////////////////////////////////////////////////////////////////////////
//
// DIO Basic Config View
// 

// Runtime Class

AfxImplementRuntimeClass(CDIO14ConfigWnd, CUIViewWnd);

// Overidables

void CDIO14ConfigWnd::OnAttach(void)
{
	m_pItem   = (CDIO14ModCfg *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(L"dio14");

	CUIViewWnd::OnAttach();
	}

// UI Update

void CDIO14ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddInputs();

	AddInit();

	EndPage(TRUE);
	}

void CDIO14ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	DoEnables();
	}

// UI Creation

void CDIO14ConfigWnd::AddInputs(void)
{
	StartTable(CString(IDS_MODULE_INPUTS), 1);

	AddColHead(L"Mode");

	for( UINT n = 1; n <= 8; n++ ) {

		AddRowHead(CPrintf(IDS_MODULE_INPUT, n));

		AddUI(m_pItem, TEXT("root"), CPrintf("InputMode%u", n));
		}

	EndTable();
	}

void CDIO14ConfigWnd::AddInit(void)
{
	StartGroup(CString(IDS_MODULE_INIT), 1);

	AddUI(m_pItem, TEXT("root"), TEXT("IncludeDownload"));

	AddUI(m_pItem, TEXT("root"), TEXT("LogicDisable"));

	EndGroup(TRUE);
	}

// Implementation

void CDIO14ConfigWnd::DoEnables(void)
{
	if(m_pItem->m_IncludeDownload == YES) {
		
		EnableUI("LogicDisable",TRUE);
		}
	else {
		EnableUI("LogicDisable",FALSE);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// DIO Logic View
// 

// Runtime Class

AfxImplementRuntimeClass(CDIO14LogicWnd, CUIViewWnd);

// Overidables

void CDIO14LogicWnd::OnAttach(void)
{
	m_pItem = (CDIO14ModCfg *) CViewWnd::m_pItem;

	CUIViewWnd::OnAttach();
	}

// UI Management

void CDIO14LogicWnd::OnUICreate(void)
{
	StartPage(1);

	EndPage(TRUE);

	InitUI();
	}

void CDIO14LogicWnd::OnUIChange(CItem *pItem, CString Tag)
{
	}

// Message Map

AfxMessageMap(CDIO14LogicWnd, CUIViewWnd)
{
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_RBUTTONDOWN)

	AfxMessageEnd(CDIO14LogicWnd)
	};

//End of File
