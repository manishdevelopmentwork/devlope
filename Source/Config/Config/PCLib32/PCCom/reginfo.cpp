
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 COM Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Component Registration Info
//

// Constructor

CComRegInfo::CComRegInfo(void)
{
	m_Version = 0;
	}

// End of File
