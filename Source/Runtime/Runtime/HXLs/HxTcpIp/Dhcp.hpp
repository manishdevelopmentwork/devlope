
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Dhcp_HPP

#define	INCLUDE_Dhcp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DhcpFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DHCP Client
//

class CDhcp
{
	public:
		// Constructor
		CDhcp(void);

		// Destructor
		~CDhcp(void);

		// Management
		void LinkActive(bool fActive);

		// Attributes
		DWORD GetOption(BYTE Type) const;

		// Operations
		void Bind(IRouter *pRouter, UINT uFace);
		void SetMAC(MACREF MAC);
		void Service(void);
		
		// Diagnostics
		UINT RunDiagCmd(IDiagOutput *pOut, UINT uCmd);

	protected:
		// State
		enum {
			stateIdle,
			stateWait,
			stateInit,
			stateDiscover,
			stateRequest,
			stateBound,
			stateRenew,
			stateRebind,
			stateRelease,
			stateRenewed,
			stateReleased,
			};

		// Data Members
		IRouter	* m_pRouter;
		UINT	  m_uFace;
		CMacAddr  m_Mac;
		ISocket	* m_pSocket;
		CIpAddr	  m_Client;
		CIpAddr	  m_Server;
		CIpAddr	  m_Mask;
		CIpAddr	  m_Gate;
		char	  m_Net[64];
		bool	  m_fActive;
		UINT	  m_State;
		UINT	  m_Cmd;
		UINT	  m_Try;
		UINT	  m_Limit;
		UINT	  m_Start;
		UINT	  m_Last;
		DWORD	  m_Secs;
		DWORD	  m_Lease;
		DWORD	  m_Renew;
		DWORD	  m_Rebind;
		DWORD	  m_TimeZone;
		CIpAddr	  m_NTP;
		CIpAddr	  m_SMTP;
		CIpAddr   m_DNS;
		bool	  m_fApipa;

		// Socket Management
		void OpenSocket(bool fServer);
		void CloseSocket(void);

		// Message Handlers
		bool SendDiscover(void);
		bool SendRequest(void);
		bool SendRenew(bool fServer);
		bool SendRelease(bool fServer);
		bool RecvOffer(void);
		WORD RecvAck(void);

		// State Machine
		bool StateRx(void);
		void StateTx(void);

		// Implementation
		void AdjustPacket(CBuffer *pBuff, UINT n);
		void ScanOptions(CDhcpFrame *pFrame, UINT uSize);
		BYTE ScanOptions(PBYTE pData, UINT uSize);
		void SetState(UINT State);
		void StartTimeout(bool fStep);
		bool CheckTimeout(void);
		bool ClearRecv(void);

		// APIPA Helpers
		bool MakeApipa(void);
		bool DropApipa(void);
		WORD Random(LONG &Seed);

		// Diagnostics
		UINT DiagStatus(IDiagOutput *pOut);
		UINT DiagRelease(IDiagOutput *pOut);
		UINT DiagRenew(IDiagOutput *pOut);
	};

// End of File

#endif
