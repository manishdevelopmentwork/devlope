
#include "Intern.hpp"

#include "Service.hpp"

#include "CloudServiceAzure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MqttClientAzure.hpp"

#include "MqttClientOptionsAzure.hpp"

#include "CloudDeviceDataSet.hpp"

#include "CloudTagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Azure Cloud Service
//

// Instantiator

IService * Create_CloudServiceAzure(void)
{
	return New CCloudServiceAzure;
	}

// Constructor

CCloudServiceAzure::CCloudServiceAzure(void)
{
	m_Name = "AZURE";
	}

// Initialization

void CCloudServiceAzure::Load(PCBYTE &pData)
{
	ValidateLoad("CCloudServiceAzure", pData);

	CCloudServiceCrimson::Load(pData);

	for( UINT n = 0; n < m_uSet; n++ ) {

		if( m_pSet[n]->m_Array == 0 ) {

			m_pSet[n]->AddRewrite('[', '-');

			m_pSet[n]->AddRewrite(']', 0);
			}

		if( m_pSet[n]->m_Tree == 0 ) {

			m_pSet[n]->AddRewrite('.', '-');
			}
		}

	CMqttClientOptionsAzure *pOpts = New CMqttClientOptionsAzure;

	pOpts->Load(pData);

	m_pOpts   = pOpts;

	CheckHistory(1000);

	m_pClient = New CMqttClientAzure(this, *pOpts);

	m_pOpts->m_DiskPath.Printf("%c:\\MQTT\\AZURE", 'C' + pOpts->m_Drive);

	FindConfigGuid(pOpts->GetExtra());
	}

// Service ID

UINT CCloudServiceAzure::GetID(void)
{
	return 12;
	}

// End of File
