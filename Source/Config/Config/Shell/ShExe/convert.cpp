
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Database Convertion Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CConvertDialog, CStdDialog);
		
// Constructors

CConvertDialog::CConvertDialog(void)
{
	SetName(L"ConvertDlg");
	}

// Message Map

AfxMessageMap(CConvertDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOK    )
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxDispatchCommand(100,     OnSelect)
	AfxDispatchCommand(101,     OnSelect)

	AfxMessageEnd(CConvertDialog)
	};

// Message Handlers

BOOL CConvertDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CString Root = L"Crimson 3";

	C3OemStrings(Root);

	SetCaption(L"Convert " + Root + L" Database");

	GetDlgItem( 101).SetWindowText(CPrintf(GetDlgItem( 101).GetWindowText(), Root));

	GetDlgItem(1000).SetWindowText(CPrintf(GetDlgItem(1000).GetWindowText(), Root));

	GetDlgItem(1001).SetWindowText(CPrintf(GetDlgItem(1001).GetWindowText(), Root));
	
	DoEnables();
	
	return FALSE;
	}

// Command Handlers

BOOL CConvertDialog::OnCommandOK(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CConvertDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

BOOL CConvertDialog::OnSelect(UINT uID)
{
	SaveData();

	DoEnables();

	return TRUE;
	}

// Implementation

void CConvertDialog::DoEnables(void)
{
	BOOL fAccept   = ((CButton &) GetDlgItem(100)).IsChecked();

	GetDlgItem(IDOK).EnableWindow(fAccept);
	}

void CConvertDialog::SaveData(void)
{
	BOOL  fWarn = ((CButton &) GetDlgItem(101)).IsChecked();

	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"Convert");	

	Reg.SetValue(L"Warning", UINT(!fWarn));
	}

// End of File
