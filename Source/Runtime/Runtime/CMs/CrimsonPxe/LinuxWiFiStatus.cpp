
#include "Intern.hpp"

#include "LinuxWiFiStatus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linux Wi-Fi Status
//

// Constructor

CLinuxWiFiStatus::CLinuxWiFiStatus(PCTXT pName, UINT uInst)
{
	m_Name  = pName;

	m_uInst = uInst;

	m_pLock = Create_Mutex();

	piob->RegisterSingleton("net.wifi", m_uInst, (IWiFiStatus *) this);

	m_timeStatus = 0;

	m_timeScan   = 0;

	StdSetRef();
}

// Destructor

CLinuxWiFiStatus::~CLinuxWiFiStatus(void)
{
	piob->RevokeSingleton("net.wifi", m_uInst);

	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CLinuxWiFiStatus::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWiFiStatus);

	StdQueryInterface(IWiFiStatus);

	return E_NOINTERFACE;
}

ULONG CLinuxWiFiStatus::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxWiFiStatus::Release(void)
{
	StdRelease();
}

// IWiFiStatus

BOOL CLinuxWiFiStatus::GetWiFiStatus(CWiFiStatusInfo &Info)
{
	CAutoLock Lock(m_pLock);

	ReadStatus();

	Info = m_WiFiInfo;

	return TRUE;
}

BOOL CLinuxWiFiStatus::ScanNetworks(CArray<CWiFiNetworkInfo> &List)
{
	CJsonData Data;

	if( GetTickCount() - m_timeScan >= 10000 ) {

		SendCommand("scan");

		Sleep(4000);

		m_timeScan = GetTickCount();
	}

	if( ReadJson(Data, "scan") ) {

		List.Empty();

		CJsonData *pList = Data.GetChild("scan");

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CWiFiNetworkInfo Info;

			CJsonData *pNet = pList->GetChild(n);

			Info.m_Network  = pNet->GetValue("ssid", "");

			Info.m_uSignal  = atoi(pNet->GetValue("signal", "0"));

			List.Append(Info);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxWiFiStatus::SendCommand(PCTXT pCmd)
{
	CAutoLock Lock(m_pLock);

	CPrintf   Name("/./tmp/crimson/face/%s/command", PCSTR(m_Name));

	CAutoFile File(Name, "w+");

	if( File ) {

		File.PutLine(pCmd);

		return TRUE;
	}

	return FALSE;
}

// Implementation

bool CLinuxWiFiStatus::ReadJson(CJsonData &Json, PCTXT pType)
{
	CPrintf   Name("/./tmp/crimson/face/%s/%s", PCSTR(m_Name), pType);

	CAutoFile File(Name, "r");

	if( File ) {

		UINT uSize = File.GetSize();

		CAutoArray<char> Data(uSize+1);

		if( File.Read(Data, uSize) == uSize ) {

			Data[uSize] = 0;

			if( Json.Parse(Data) ) {

				return true;
			}
		}
	}

	return false;
}

void CLinuxWiFiStatus::ReadStatus(void)
{
	DWORD time = GetTickCount();

	bool  good = false;

	if( time - m_timeStatus >= 1000 ) {

		for( int t = 0; t < 2; t++ ) {

			CJsonData Json;

			if( ReadJson(Json, "status") ) {

				m_WiFiInfo.m_fValid   = TRUE;

				m_WiFiInfo.m_Device   = m_Name;

				m_WiFiInfo.m_State    = Json.GetValue("state", "Undefined");

				m_WiFiInfo.m_Model    = Json.GetValue("model", "Unknown");

				m_WiFiInfo.m_Version  = Json.GetValue("version", "Unknown");

				m_WiFiInfo.m_Network  = Json.GetValue("network", "Unknown");

				m_WiFiInfo.m_uSignal  = atoi(Json.GetValue("signal", "99"));

				m_WiFiInfo.m_CTime    = atoi(Json.GetValue("ctime", "0"));

				m_WiFiInfo.m_fOnline  = (Json.GetValue("online", "") == "true");

				m_WiFiInfo.m_fApMode  = (Json.GetValue("mode", "0") == "1");

				m_WiFiInfo.m_uChannel = atoi(Json.GetValue("channel", "0"));

				m_WiFiInfo.m_PeerMac  = CMacAddr(Json.GetValue("apmac", ""));

				m_WiFiInfo.m_MxTime   = atoi(Json.GetValue("mxtime", "0"));

				m_WiFiInfo.m_RxBytes  = strtoull(Json.GetValue("rxbytes", "0"), NULL, 10);

				m_WiFiInfo.m_TxBytes  = strtoull(Json.GetValue("txbytes", "0"), NULL, 10);

				good = true;

				break;
			}

			Sleep(5);
		}

		if( !good ) {

			m_WiFiInfo.m_fValid  = FALSE;
		}

		m_timeStatus = time;
	}
}

// End of File
