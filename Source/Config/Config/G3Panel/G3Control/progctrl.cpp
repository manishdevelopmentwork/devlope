
#include "intern.hpp"

#include "catwnd.hpp"

#include "progview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Program Editor
//

// Runtime Class

AfxImplementRuntimeClass(CControlProgramCtrlWnd, CCtrlWnd);

// Constructor

CControlProgramCtrlWnd::CControlProgramCtrlWnd(void)
{
	m_pEdit   = NULL;

	m_fAttach = FALSE;

	FindCatView();
	}

// Attriutes

BOOL CControlProgramCtrlWnd::IsDirty(void) const
{
	return m_pEdit->IsDirty();
	}

// Operations

void CControlProgramCtrlWnd::Attach(CItem *pItem)
{
	m_pItem    = (CControlProgram *) pItem;

	m_pProject = (CControlProject *) pItem->GetParent(AfxRuntimeClass(CControlProject));

	m_fAttach  = TRUE;

	CheckEditor();
	}

void CControlProgramCtrlWnd::Exec(CCmd *pCmd)
{
	m_pCatView->StopDebug();

	CStratonWnd &Wnd = m_pEdit->GetEditor();

	m_pEdit->SetDirty();
	
	CString Text = Wnd.GetText();

	Text.ApplyDelta(((CEditorWnd::CCmdEdit *) pCmd)->m_Delta);

	Wnd.SetText(Text);

	Wnd.Invalidate(FALSE);

	m_pItem->SetDirty();
	}

void CControlProgramCtrlWnd::Undo(CCmd *pCmd)
{
	m_pCatView->StopDebug();

	CStratonWnd &Wnd = m_pEdit->GetEditor();

	m_pEdit->SetDirty();

	CString Text = Wnd.GetText();

	Text.UndoDelta(((CEditorWnd::CCmdEdit *) pCmd)->m_Delta);

	Wnd.SetText(Text);

	Wnd.Invalidate(FALSE);

	m_pItem->SetDirty();
	}

void CControlProgramCtrlWnd::LocateError(CString Error)
{
	CStratonWnd &Wnd = m_pEdit->GetEditor();

	Wnd.LocateError(Error);
	}

void CControlProgramCtrlWnd::SetDebug(BOOL fDebug, BOOL fOnline)
{
	if( m_fAttach ) {

		m_fDebug  = fDebug;

		m_fOnline = fOnline;

		CStratonWnd &Wnd = m_pEdit->GetEditor();

		Wnd.SetDebug(m_fDebug);

		Invalidate(FALSE);
		}
	}

void CControlProgramCtrlWnd::Commit(void)
{
	if( m_fAttach ) {

		if( m_pEdit ) {

			m_pEdit->Commit();
			}
		}
	}

void CControlProgramCtrlWnd::Reload(void)
{
	if( m_fAttach ) {

		if( m_pEdit ) {

			m_pEdit->Reload();
			}
		}
	}

void CControlProgramCtrlWnd::DetachEditor(CItem *pItem)
{
	if( m_fAttach ) {

		CViewWnd &View = (CViewWnd &) GetParent(AfxRuntimeClass(CControlProgramView));

		if( View.IsWindow() ) {

			if( View.IsItem(pItem) ) {

				View.Detach();

				m_fAttach = FALSE;
			
				return;
				}
			}
		}
	}

// Routing Control

BOOL CControlProgramCtrlWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pEdit && m_pEdit->IsWindow() ) {

		if( m_pEdit->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CControlProgramCtrlWnd, CCtrlWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PREDESTROY)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CControlProgramCtrlWnd)
	};

// Message Handlers

void CControlProgramCtrlWnd::OnPostCreate(void)
{
	CheckEditor();
	}

void CControlProgramCtrlWnd::OnPreDestroy(void)
{
	m_pCatView->DelProgramCtrl(this);
	}

void CControlProgramCtrlWnd::OnSize(UINT uCode, CSize Size)
{
	if( m_pEdit ) {

		m_pEdit->MoveWindow(GetEditorRect(), TRUE);
		}
	}

void CControlProgramCtrlWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_pEdit ) {

		m_pEdit->SetFocus();
		}
	}

void CControlProgramCtrlWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect       Rect     = GetClientRect();

	BOOL	    fDisable = !IsEnabled();

	for( int n = 0; n < 5; n++ ) {

		if( n == 0 || n == 2 ) {
			
			if( m_fDebug ) {
	
				if( m_fOnline ) 
					DC.FrameRect(Rect--, CColor(192,0,0));
				else
					DC.FrameRect(Rect--, CColor(0,0,192));

				continue;
				}

			if( fDisable ) {
	
				DC.FrameRect(Rect--, afxColor(Disabled));

				continue;
				}
			}

		if( n == 4 ) {

			DC.FrameRect(Rect--, afxColor(BLACK));

			continue;
			}

		DC.FrameRect(Rect--, afxColor(TabFace));
		}
	}

// Implementation

void CControlProgramCtrlWnd::FindCatView(void)
{
	CSysProxy Proxy;

	Proxy.Bind();

	m_pCatView = (CControlCatWnd *) Proxy.GetCatView(AfxRuntimeClass(CControlCatWnd));

	m_pCatView->AddProgramCtrl(this);

	m_fDebug  = m_pCatView->IsDebug();

	m_fOnline = m_pCatView->IsOnline();
	}

void CControlProgramCtrlWnd::CheckEditor(void)
{
	if( IsWindow() && m_pItem ) {

		CLASS Class = m_pItem->GetEditorClass();

		if( !m_pEdit || AfxPointerClass(m_pEdit) != Class ) {

			if( m_pEdit ) {

				CStratonWnd &Wnd = m_pEdit->GetEditor();

				Wnd.SetDebug(FALSE);

				m_pEdit->Commit();

				m_pEdit->ShowWindow(SW_HIDE);

				m_pEdit->DestroyWindow(FALSE);
				}

			m_pEdit = AfxNewObject(CEditorWnd, Class);

			m_pEdit->Create( WS_CHILD | WS_TABSTOP | WS_VISIBLE,
					 GetEditorRect(),
					 ThisObject,
					 100,
					 NULL
					 );

			CStratonWnd &Wnd = m_pEdit->GetEditor();

			CString     Path = m_pProject->m_Path;
	
			Wnd.SetReadOnly(m_pItem->GetDatabase()->IsReadOnly());
			}
	
		// Can't attach while in debug mode!

		CStratonWnd &Wnd = m_pEdit->GetEditor();

		Wnd.SetDebug(FALSE);

		m_pEdit->Attach(m_pItem);

		m_pEdit->Attach(m_pItem->GetProject(), m_pItem->GetHandle());

		Wnd.SetDebug(m_fDebug);
		}
	}

CRect CControlProgramCtrlWnd::GetEditorRect(void)
{
	return GetClientRect() - 5;
	}

// End of File
