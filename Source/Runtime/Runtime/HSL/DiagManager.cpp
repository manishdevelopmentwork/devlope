
#include "Intern.hpp"

#include "DiagManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DiagConsoleString.hpp"

#include "DiagCommand.hpp"

#include "DiagOutput.hpp"

#include "DiagOutputHtml.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics Manager
//

// Instantiator

#if !defined(AEON_ENVIRONMENT)

IDiagManager * Create_DiagManager(void)
{
	CDiagManager *p = New CDiagManager;

	AfxRegSingleton(IDiagManager, p);

	return p;
	}

#else

IUnknown * Create_DiagManager(void)
{
	AfxInstantiate(IDiagManager, CDiagManager);
	}

#endif

// Constructor

CDiagManager::CDiagManager(void)
{
	StdSetRef();

	m_fLock          = FALSE;

	m_pLock          = NULL;

	m_uProviderCount = 100;

	m_uCommandCount  = 400;

	m_uConsoleCount  = 10;

	m_pProviders = New CProvider [ m_uProviderCount ];

	m_pCommands  = New CCommand  [ m_uCommandCount  ];

	m_pConsoles  = New CConsole  [ m_uConsoleCount  ];

	memset(m_pProviders, 0, m_uProviderCount * sizeof(CProvider));

	memset(m_pCommands,  0, m_uCommandCount  * sizeof(CCommand));

	memset(m_pConsoles,  0, m_uConsoleCount  * sizeof(CConsole));

	m_uProviderHwm = 0;

	m_uCommandHwm  = 0;

	m_uConsoleHwm  = 0;

	m_uConsoleInst = 0;

	m_fSort = false;

	AddDiagCmds();
	}

// IUnknown

#if defined(AEON_ENVIRONMENT)

HRESULT CDiagManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagManager);

	StdQueryInterface(IDiagManager);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CDiagManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CDiagManager::Release(void)
{
	StdRelease();
	}

#endif

// IDiagManager

UINT CDiagManager::RegisterProvider(IDiagProvider *pProv, PCTXT pName)
{
	CAutoLock Lock(m_pLock, m_fLock);

	for( UINT n = 0; n <= m_uProviderHwm; n++ ) {

		CProvider &Prov = m_pProviders[n];

		if( !Prov.m_pName ) {
	
			Prov.m_pName = strdup(pName);
	
			Prov.m_pProv = pProv;

			MakeMax(m_uProviderHwm, n+1);

			return n;
			}
		}

	return NOTHING;
	}

void CDiagManager::RevokeProvider(UINT uProv)
{
	CAutoLock Lock(m_pLock, m_fLock);

	if( uProv < m_uProviderHwm ) {

		CProvider &Prov = m_pProviders[uProv];

		for( UINT n = 0; n < m_uCommandHwm; n++ ) {

			CCommand &Cmd = m_pCommands[n];

			if( Cmd.m_uProv == uProv ) {

				free(Cmd.m_pName);

				memset(&Cmd, 0, sizeof(Cmd));
				}
			}

		free(Prov.m_pName);

		memset(&Prov, 0, sizeof(Prov));

		m_fSort = false;
		}
	}

void CDiagManager::RegisterCommand(UINT uProv, UINT uCode, PCTXT pName)
{
	CAutoLock Lock(m_pLock, m_fLock);

	if( uProv < m_uProviderHwm ) {
	
		CProvider &Prov = m_pProviders[uProv];

		for( UINT n = 0; n <= m_uCommandHwm; n++ ) {

			CCommand &Cmd = m_pCommands[n];

			if( !Cmd.m_pName ) {
	
				char sLine[64] = { 0 };

				if( Prov.m_pName[0] ) {

					strcat(sLine, Prov.m_pName);

					strcat(sLine, ".");
					}

				strcat(sLine, pName);

				Cmd.m_pName = strdup(sLine);

				Cmd.m_uProv = uProv;

				Cmd.m_uCode = uCode;

				MakeMax(m_uCommandHwm, n+1);

				m_fSort = false;

				break;
				}
			}
		}
	}

UINT CDiagManager::RunCommand(IDiagConsole *pConsole, PCTXT pLine)
{
	CDiagCommand DiagCmd(pLine, !pConsole);

	PCTXT pName = DiagCmd.GetCommand();

	if( *pName ) {

		CAutoLock Lock(m_pLock, m_fLock);

		Sort();

		CCommand *pCmd = (CCommand *) bsearch(&pName, m_pCommands, m_uCommandHwm, sizeof(CCommand), SortFunc);

		Lock.Free();

		CDiagOutput DiagOut(pConsole, pName);

		if( pCmd ) {

			DiagCmd.SetCode(pCmd->m_uCode);

			IDiagProvider *pProv = m_pProviders[pCmd->m_uProv].m_pProv;

			UINT r = pProv->RunDiagCmd(&DiagOut, &DiagCmd);

			DiagOut.Finished();

			return r;
			}

		DiagOut.Print("error: unknown command '%s'\n", pName);

		return 1;
		}

	return 0;
	}

UINT CDiagManager::RunCommand(char **ppBuff, UINT uEnc, PCTXT pLine)
{
	CDiagCommand DiagCmd(pLine, FALSE);

	PCTXT pName = DiagCmd.GetCommand();

	if( *pName ) {

		CAutoLock Lock(m_pLock, m_fLock);

		Sort();

		CCommand *pCmd = (CCommand *) bsearch(&pName, m_pCommands, m_uCommandHwm, sizeof(CCommand), SortFunc);

		Lock.Free();

		if( ppBuff ) {

			CDiagConsoleString DiagCon(ppBuff);
			
			CDiagOutputHtml    DiagOut(&DiagCon, pName);

			if( pCmd ) {

				DiagCmd.SetCode(pCmd->m_uCode);

				IDiagProvider *pProv = m_pProviders[pCmd->m_uProv].m_pProv;

				UINT r = pProv->RunDiagCmd(&DiagOut, &DiagCmd);

				DiagOut.Finished();

				return r;
				}

			DiagOut.Print("error: unknown command '%s'\n", pName);
			}
		else {
			if( pCmd ) {

				return 0;
				}
			}

		return 1;
		}

	return 0;
	}

BOOL CDiagManager::CompleteCommand(char *pText, UINT uSize)
{
	// TODO -- Add partial completion for ambiguous match.

	CAutoLock Lock(m_pLock, m_fLock);

	UINT m = NOTHING;

	for( UINT n = 0; n < m_uCommandHwm; n++ ) {

		CCommand &Cmd = m_pCommands[n];

		if( uSize < strlen(Cmd.m_pName) ) {

			if( !strncmp(Cmd.m_pName, pText, uSize) ) {

				if( m == NOTHING ) {

					m = n;

					continue;
					}

				return FALSE;
				}
			}
		}

	if( m < NOTHING ) {

		strcpy(pText, m_pCommands[m].m_pName);

		return TRUE;
		}

	return FALSE;
	}

UINT CDiagManager::RegisterConsole(IDiagConsole *pConsole)
{
	CAutoLock Lock(m_pLock, m_fLock);

	for( UINT n = 0; n <= m_uConsoleHwm; n++ ) {

		CConsole &Con = m_pConsoles[n];

		if( !Con.m_pConsole ) {
	
			Con.m_pConsole = pConsole;

			MakeMax(m_uConsoleHwm, n+1);
			
			m_uConsoleInst++;

			return n;
			}
		}

	return NOTHING;
	}

void CDiagManager::RevokeConsole(UINT uConsole)
{
	CAutoLock Lock(m_pLock, m_fLock);

	if( uConsole < m_uConsoleHwm ) {

		CConsole &Con = m_pConsoles[uConsole];

		if( Con.m_pConsole ) {

			m_uConsoleInst--;

			memset(&Con, 0, sizeof(Con));
			}
		}
	}

void CDiagManager::WriteToConsoles(char const *pText)
{
	CAutoLock Lock(m_pLock, m_fLock);

	for( UINT n = 0; n < m_uConsoleHwm; n++ ) {

		if( m_pConsoles[n].m_pConsole ) {

			m_pConsoles[n].m_pConsole->Write(pText);
			}
		}
	}

BOOL CDiagManager::HasConsoles(void)
{
	return m_uConsoleInst > 0;
	}

BOOL CDiagManager::SerializeAccess(BOOL fLock)
{
	if( m_fLock != fLock ) {

		if( fLock ) {

			m_pLock = Create_Qutex();

			m_fLock = TRUE;
			}
		else {
			m_fLock = FALSE;

			m_pLock->Release();

			m_pLock = NULL;
			}

		return TRUE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

// IDiagProvider

UINT CDiagManager::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	switch( pCmd->GetCode() ) {

		case 1:
			return DiagHelp(pOut, pCmd);
		}

	return 0;
	}

// Implementation

bool CDiagManager::Sort(void)
{
	if( !m_fSort ) {

		qsort(m_pCommands, m_uCommandHwm, sizeof(CCommand), SortFunc);

		m_fSort = true;

		return true;
		}

	return false;
	}

// Diagnostics

bool CDiagManager::AddDiagCmds(void)
{
	UINT uProv = RegisterProvider(this, "diag");

	RegisterCommand(uProv, 1, "help");

	return true;
	}

UINT CDiagManager::DiagHelp(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		CAutoLock Lock(m_pLock, m_fLock);

		Sort();

		pOut->Print("The following commands are supported...\n\n");

		UINT c = 5;

		UINT p = 0;

		for( UINT m = 0; m < m_uCommandHwm; m++ ) {

			if( m_pCommands[m].m_pName ) {

				MakeMax(p, strlen(m_pCommands[m].m_pName) + 2);
				}
			}

		for( UINT d = 0, n = 0; n < m_uCommandHwm; n++ ) {

			if( m_pCommands[n].m_pName ) {

				if( d % c == 0 ) {

					pOut->Print("  ");
					}

				if( true ) {

					pOut->Print("%-*.*s", p, p, m_pCommands[n].m_pName);
					}

				if( d % c == c - 1 || n == m_uCommandHwm - 1 ) {

					pOut->Print("\n");
					}

				d++;

				continue;
				}

			if( n == m_uCommandHwm - 1 ) {

				pOut->Print("\n");
				}
			}

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// Sort Function

int CDiagManager::SortFunc(void const *p1, void const *p2)
{
	CCommand   *c1 = (CCommand *) p1;

	CCommand   *c2 = (CCommand *) p2;

	char const *n1 = c1->m_pName ? c1->m_pName : "~";

	char const *n2 = c2->m_pName ? c2->m_pName : "~";

	bool        d1 = c1 ? !!strchr(n1, '.') : true;

	bool        d2 = c2 ? !!strchr(n2, '.') : true;

	if( d1 == d2 ) {

		return strcasecmp(n1, n2);
		}

	if( d1 && !d2 ) {

		return +1;
		}

	return -1;
	}

// End of File
