
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Item View Window
//

// Dynamic Class

AfxImplementDynamicClass(CViewWnd, CWnd);

// Constructor

CViewWnd::CViewWnd(void)
{
	m_pItem    = NULL;
	
	m_fRecycle = FALSE;

	m_fCurrent = FALSE;

	m_crc      = NOTHING;
}

// Attributes

CItem * CViewWnd::GetItem(void) const
{
	return m_pItem;
	}

BOOL CViewWnd::IsItem(CItem *pItem) const
{
	if( m_pItem == pItem ) {

		if( !pItem ) {

			return TRUE;
		}

		CObject *pObject = (CObject *) pItem;

		UINT     uSize   = pObject->GetObjectSize();

		return m_crc == CRC32(PCBYTE(pItem), uSize);
	}

	return FALSE;
}

BOOL CViewWnd::CanRecycle(void) const
{
	return m_fRecycle;
	}
	
BOOL CViewWnd::IsCurrent(void) const
{
	return m_fCurrent;
	}

CString CViewWnd::GetNavPos(void) const
{
	return ((CViewWnd *) this)->OnGetNavPos();
	}

// Operations

BOOL CViewWnd::Attach(CItem *pItem)
{
	if( Detach() ) {

		if( pItem ) {

			CObject *pObject = (CObject *) pItem;

			UINT     uSize   = pObject->GetObjectSize();

			m_crc   = CRC32(PCBYTE(pItem), uSize);
		}
		else {
			m_crc = 0;
		}


		m_pItem = pItem;


		OnAttach();
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CViewWnd::Detach(void)
{
	if( m_pItem ) {
	
		if( OnPreDetach() ) {
			
			m_pItem = NULL;
			
			OnDetach();
			
			return TRUE;
			}
			
		return FALSE;
		}
		
	return TRUE;
	}
	
void CViewWnd::SetCurrent(BOOL fCurrent)
{
	if( this ) {

		SendMessage(WM_SETCURRENT, WPARAM(fCurrent));
		}
	}

BOOL CViewWnd::Navigate(CString const &Nav)
{
	return OnNavigate(Nav);
	}

void CViewWnd::ExecCmd(CCmd *pCmd)
{
	OnExec(pCmd);
	}

void CViewWnd::UndoCmd(CCmd *pCmd)
{
	OnUndo(pCmd);
	}

// Overribables

void CViewWnd::OnAttach(void)
{
	}

BOOL CViewWnd::OnPreDetach(void)
{
	return TRUE;
	}

void CViewWnd::OnDetach(void)
{
	}

CString CViewWnd::OnGetNavPos(void)
{
	return L"";
	}

BOOL CViewWnd::OnNavigate(CString const &Nav)
{
	return TRUE;
	}

void CViewWnd::OnExec(CCmd *pCmd)
{
	}
		
void CViewWnd::OnUndo(CCmd *pCmd)
{
	}
		
// Message Procedures

LRESULT CViewWnd::WndProc(UINT uMessage, WPARAM wParam, LPARAM lParam)
{
	if( uMessage == WM_SETCURRENT ) {

		if( m_fCurrent == BOOL(wParam) ) {

			return 0;
			}
			
		m_fCurrent = BOOL(wParam);
		}

	return CWnd::WndProc(uMessage, wParam, lParam);
	}

// Message Map

AfxMessageMap(CViewWnd, CWnd)
{
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_SETCURRENT)

	AfxMessageEnd(CViewWnd)
	};

// Message Handlers

void CViewWnd::OnLButtonDown(UINT uCode, CPoint Pos)
{
	SetFocus();
	}
	
void CViewWnd::OnSetCurrent(BOOL fCurrent)
{
	}

// Routing Codes

UINT CViewWnd::GetRoutingCode(MSG const &Msg)
{
	switch( Msg.message ) {
	
		case WM_GOINGIDLE:

			return RC_ALL;

		case WM_AFX_COMMAND:
		case WM_AFX_CONTROL:
		case WM_AFX_GETINFO:
		
			switch( HIBYTE(Msg.wParam) ) {
			
				case IDM_HELP:

					return RC_LOCAL1 | RC_ALL;
					
				case IDM_WINDOW:

					return RC_FIRST;
					
				case IDM_VIEW:

					if( Msg.wParam == IDM_VIEW_REFRESH ) {

						return RC_ALL;
						}

					if( LOWORD(Msg.wParam) < IDM_VIEW_TAB_1 ) {

						return RC_FIRST;
						}
				
					return RC_CURRENT;

				case IDM_GO:
					
					return RC_FIRST;
				}

			break;

		case WM_BROADCAST:

			return RC_BCAST | RC_ALL;
			
		case WM_LOADMENU:
		case WM_LOADTOOL:
		case WM_SHOWUI:

			return RC_LOCAL2;
		}

	return RC_CURRENT;
	}

// End of File
