
#include "intern.hpp"

#include "dadidomodule.hpp"

#include "dadidoviewwnd.hpp"

#include "dadidomainwnd.hpp"

#include "dadidoinput.hpp"

#include "dadidooutput.hpp"

#include "dadidoinputconfig.hpp"

#include "dadidooutputconfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Digial Input Output Module
//

// Dynamic Class

AfxImplementDynamicClass(CDADIDOModule, CManticoreGenericModule);

// Constructor

CDADIDOModule::CDADIDOModule(void)
{
	m_pInput        = New CDADIDOInput;

	m_pOutput       = New CDADIDOOutput;

	m_pInputConfig  = New CDADIDOInputConfig;

	m_pOutputConfig = New CDADIDOOutputConfig;

	m_Ident         = LOBYTE(ID_DADIDO);

	m_FirmID        = FIRM_DADIDO;

	m_Model         = "8DI/8DO";

	m_Power         = 36;

	m_Conv.Insert(L"Variables", L"DI,DO");
}

// Destructor

CDADIDOModule::~CDADIDOModule(void)
{
}

// UI Management

CViewWnd * CDADIDOModule::CreateMainView(void)
{
	return New CDADIDOMainWnd;
}

// Comms Object Access

UINT CDADIDOModule::GetObjectCount(void)
{
	return 4;
}

BOOL CDADIDOModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_DI;

			Data.Name  = L"DI";

			Data.pItem = m_pInput;

			return TRUE;

		case 1:
			Data.ID    = OBJ_DO;

			Data.Name  = L"DO";

			Data.pItem = m_pOutput;

			return TRUE;

		case 2:
			Data.ID    = OBJ_CONFIG_DI;

			Data.Name  = L"DI Config";

			Data.pItem = m_pInputConfig;

			return FALSE;

		case 3:
			Data.ID    = OBJ_CONFIG_DO;

			Data.Name  = L"DO Config";

			Data.pItem = m_pOutputConfig;

			return FALSE;
	}

	return FALSE;
}

// Conversion

BOOL CDADIDOModule::Convert(CPropValue *pValue)
{
	if( pValue ) {

		// Graphite

		m_pInputConfig->Convert(pValue->GetChild(L"Config"));

		m_pOutputConfig->Convert(pValue->GetChild(L"Config"));

		// Legacy

		m_pInputConfig->Convert(pValue->GetChild(L"Input"));

		return TRUE;
	}

	return FALSE;
}

// Meta Data

void CDADIDOModule::AddMetaData(void)
{
	Meta_AddInteger(Ident);
	Meta_AddObject(Input);
	Meta_AddObject(Output);
	Meta_AddObject(InputConfig);
	Meta_AddObject(OutputConfig);

	CManticoreGenericModule::AddMetaData();
}

// Download Data

void CDADIDOModule::MakeConfigData(CInitData &Init)
{
	FindFirmware();

	CManticoreGenericModule::MakeConfigData(Init);
}

// Firmware

void CDADIDOModule::FindFirmware(void)
{
	switch( LOBYTE(m_Ident) ) {

		case LOBYTE(ID_DADIDO):

			m_FirmID = FIRM_DADIDO;

			break;

		case LOBYTE(ID_DADIRO):

			m_FirmID = FIRM_DADIRO;

			break;
	}
}

// End of File
