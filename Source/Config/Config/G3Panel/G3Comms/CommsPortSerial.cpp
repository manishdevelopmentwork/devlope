
#include "Intern.hpp"

#include "CommsPortSerial.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsDeviceList.hpp"
#include "CommsPortList.hpp"
#include "CommsPortPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Serial Comms Port
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortSerial, CCommsPort);

// Constructor

CCommsPortSerial::CCommsPortSerial(void) : CCommsPort(AfxRuntimeClass(CCommsDeviceList))
{
	m_Binding      = bindRawSerial;

	m_Phys         = 0;

	m_fAux         = FALSE;

	m_pBaud        = NULL;

	m_pData	       = NULL;

	m_pStop	       = NULL;

	m_pParity      = NULL;

	m_pMode        = NULL;

	m_pShareEnable = NULL;

	m_pSharePort   = NULL;
	}

// Initial Values

void CCommsPortSerial::SetInitValues(void)
{
	SetInitial(L"Baud",        m_pBaud,        19200);

	SetInitial(L"Data",        m_pData,            0);

	SetInitial(L"Stop",        m_pStop,            0);

	SetInitial(L"Parity",      m_pParity,          0);

	SetInitial(L"Mode",        m_pMode,            0);

	SetInitial(L"ShareEnable", m_pShareEnable,     0);

	SetInitial(L"SharePort",   m_pSharePort,       0);
	}

// UI Overridables

BOOL CCommsPortSerial::OnLoadPages(CUIPageList *pList)
{
	CCommsPortPage *pPage = New CCommsPortPage(this);

	pList->Append(pPage);

	return FALSE;
	}

void CCommsPortSerial::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "ShareEnable" ) {

		DoEnables(pHost);
		}

	if( Tag == "DriverID" ) {

		UINT LastID = 0;

		if( m_pDriver ) {

			LastID = m_pDriver->GetID();
			}

		if( UpdateDriver() ) {

			if( m_DriverID ) {

				CError Error(TRUE);

				if( !pHost->InReplay() && !CanConfigure(Error) ) {

					m_DriverID = LastID;

					UpdateDriver();

					m_pDevices->UpdateDriver();

					pHost->UpdateUI(Tag);

					Error.Show(CWnd::GetActiveWindow());

					return;
					}
				}

			UpdateConfig(pHost);

			SetDefaults(pHost);

			CheckDevices(pHost);

			DoEnables(pHost);

			pHost->SendUpdate(updateRename);

			pHost->RemakeUI();
			}

		return;
		}

	CCommsPort::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CCommsPortSerial::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

UINT CCommsPortSerial::GetPageType(void) const
{
	UINT uView  = 0;

	UINT uMask  = ((1<<physicalRS422) | (1<<physicalRS485));

	BOOL fMode  = FALSE;

	BOOL fShare = TRUE;

	if( m_pDriver ) {

		switch( m_pDriver->GetType() ) {

			case driverModem:

				uView = 5;

				break;

			case driverMaster:
			case driverHoneywell:

				fShare = TRUE;

				break;

			default:

				fShare = FALSE;

				break;
			}

		if( m_pDriver->GetBinding() == bindRawSerial ) {

			fShare = FALSE;
			}

		if( m_pDriver->GetID() == 0x3705 ) {

			fShare = TRUE;
			}

		if( m_pDriver->GetID() == 0x3706 ) {

			fShare = FALSE;
			}
		}

	if( m_pDbase->GetSoftwareGroup() < SW_GROUP_3B ) {

		fShare = FALSE;
		}

	if( (m_Phys & uMask) == uMask ) {

		fMode = TRUE;
		}

	if( !uView && m_pDriver ) {

		if( fShare ) {

			uView = fMode ? 1 : 2;
			}
		else
			uView = fMode ? 3 : 4;
		}

	return uView;
	}

CHAR CCommsPortSerial::GetPortTag(void) const
{
	return 'S';
}

BOOL CCommsPortSerial::IsBroken(void) const
{
	if( m_PortPhys < NOTHING ) {

		CCommsPortList *pList = (CCommsPortList *) GetParent();

		INDEX		Index = pList->GetHead();

		UINT		uMode = 0;

		UINT	       uCount = 0;

		while( !pList->Failed(Index) ) {

			CCommsPortSerial * pPort = (CCommsPortSerial *) pList->GetItem(Index);

			if( pPort->m_PortPhys == m_PortPhys ) {

				if( pPort->m_DriverID ) {

					if( pPort->IsExclusive() ) {

						if( uMode < 2 ) {

							uMode = 2;
							}
						}

					if( uMode < 1 ) {

						uMode = 1;
						}

					uCount ++;
					}
				}

			pList->GetNext(Index);
			}

		if( uMode == 1 ) {

			if( !m_pDbase->HasFlag(L"AllowMux") ) {

				return uCount > 1;
				}

			if( IsExclusive() ) {

				return uCount > 1;
				}
			}

		if( uMode == 2 ) {

			return uCount > 1;
			}
		}

	return FALSE;
	}

// Download Support

BOOL CCommsPortSerial::MakeInitData(CInitData &Init)
{
	Init.AddByte(1);

	CCommsPort::MakeInitData(Init);

	if( m_pDriver ) {

		Init.AddItem(itemVirtual, m_pBaud);
		Init.AddItem(itemVirtual, m_pData);
		Init.AddItem(itemVirtual, m_pStop);
		Init.AddItem(itemVirtual, m_pParity);
		Init.AddItem(itemVirtual, m_pMode);
		Init.AddItem(itemVirtual, m_pShareEnable);
		Init.AddItem(itemVirtual, m_pSharePort);
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsPortSerial::AddMetaData(void)
{
	CCommsPort::AddMetaData();

	Meta_AddInteger(Phys);
	Meta_AddVirtual(Baud);
	Meta_AddVirtual(Data);
	Meta_AddVirtual(Stop);
	Meta_AddVirtual(Parity);
	Meta_AddVirtual(Mode);
	Meta_AddVirtual(ShareEnable);
	Meta_AddVirtual(SharePort);

	Meta_SetName((IDS_SERIAL_PORT));
	}

// Implementation

BOOL CCommsPortSerial::SetDefaults(IUIHost *pHost)
{
	ICommsDriver *pDriver = NULL;

	if( pDriver = GetDriver() ) {

		CBindSerial Serial;

		Serial.m_Binding = bindRawSerial;

		pDriver->GetBindInfo(Serial);

		if( !(m_Phys & (1<<physicalRS422)) ) {

			Serial.m_Mode = modeTwoWire;
			}

		InitCoded(pHost, L"Baud",   m_pBaud,   Serial.m_BaudRate);

		InitCoded(pHost, L"Data",   m_pData,   8 - Serial.m_DataBits);

		InitCoded(pHost, L"Stop",   m_pStop,   Serial.m_StopBits - 1);

		InitCoded(pHost, L"Parity", m_pParity, Serial.m_Parity);

		InitCoded(pHost, L"Mode",   m_pMode,   Serial.m_Mode);

		SetDirty();

		return TRUE;
		}

	return FALSE;
	}

void CCommsPortSerial::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_DriverID ? TRUE : FALSE;

	BOOL fShared = CheckEnable(m_pShareEnable, L">=", 1, TRUE);

	pHost->EnableUI("Baud",   fEnable);

	pHost->EnableUI("Data",   fEnable);

	pHost->EnableUI("Stop",   fEnable);

	pHost->EnableUI("Parity", fEnable);

	pHost->EnableUI("Mode",   fEnable);

	pHost->EnableUI("ShareEnable", fEnable);

	pHost->EnableUI("SharePort",   fEnable && fShared);
	}

BOOL CCommsPortSerial::CanConfigure(CError &Error)
{
	if( m_PortPhys < NOTHING ) {

		CCommsPortList *pList = (CCommsPortList *) GetParent();

		INDEX		Index = pList->GetHead();

		UINT		uMode = 0;

		CString         Other = L"";

		while( !pList->Failed(Index) ) {

			CCommsPort *pPort = pList->GetItem(Index);

			if( pPort != this ) {

				if( pPort->m_PortPhys == m_PortPhys ) {

					if( pPort->m_DriverID ) {

						if( pPort->IsExclusive() ) {

							if( uMode < 2 ) {

								Other = pPort->m_Name;

								uMode = 2;
								}
							}

						if( uMode < 1 ) {

							Other = pPort->m_Name;

							uMode = 1;
							}
						}
					}
				}

			pList->GetNext(Index);
			}

		if( uMode == 0 ) {

			return TRUE;
			}

		if( uMode == 1 ) {

			if( !m_pDbase->HasFlag(L"AllowMux") ) {

				CString Text;

				Text += CPrintf(CString(IDS_THIS_PORT_SHARES), Other);

				Text += L"\n\n";

				Text += CString(IDS_DRIVER_IS_ALREADY);

				Text += L"\n\n";

				Text += CString(IDS_YOU_MAY_NOT);

				Error.Set(Text);

				return FALSE;
				}

			if( IsExclusive() ) {

				CString Text;

				Text += CPrintf(CString(IDS_THIS_PORT_SHARES), Other);

				Text += L"\n\n";

				Text += CString(IDS_DRIVER_IS_ALREADY);

				Text += L"\n\n";

				Text += CString(IDS_YOU_MAY_NOT_2);

				Error.Set(Text);

				return FALSE;
				}

			return TRUE;
			}

		if( uMode == 2 ) {

			CString Text;

			Text += CPrintf(CString(IDS_THIS_PORT_SHARES), Other);

			Text += L"\n\n";

			Text += CString(IDS_DRIVER_ON_THAT);

			Text += L"\n\n";

			Text += CString(IDS_YOU_MAY_NOT_3);

			Error.Set(Text);

			return FALSE;
			}
		}

	return TRUE;
	}

// End of File
