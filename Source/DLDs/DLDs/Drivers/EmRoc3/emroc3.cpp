#include "intern.hpp"

#include "emroc3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson Process ROC 3 Communications Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

/////////////////////////////////////////////////////////////////////////
//
// Emerson ROC 3 Master Serial Driver
//

// Instantiator

INSTANTIATE(CEmRoc3MasterSerialDriver);

// Constructor

CEmRoc3MasterSerialDriver::CEmRoc3MasterSerialDriver(void)
{
	m_Ident         = DRIVER_ID; 

	m_bGroup	= 2;

	m_bUnit		= 1;

	m_TxPtr		= 0;

	m_RxPtr		= 0;

	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx));
	}

// Destructor

CEmRoc3MasterSerialDriver::~CEmRoc3MasterSerialDriver(void)
{
	}

// Configuration

void MCALL CEmRoc3MasterSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bGroup   = GetByte(pData);

		m_bUnit    = GetByte(pData);
		}
	}

	
void MCALL CEmRoc3MasterSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CEmRoc3MasterSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmRoc3MasterSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmRoc3MasterSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bGroup  = GetByte(pData);
			
			m_pCtx->m_bUnit	  = GetByte(pData);

			m_pCtx->m_bLast   = 0xFF;

			m_pCtx->m_uLast   = 0;
			
			m_pCtx->m_uTime   = 0;

			m_pCtx->m_uPoll   = ToTicks(GetWord(pData));

			m_pCtx->m_Pass    = GetWord(pData);

			m_pCtx->m_Use     = GetByte(pData);

			m_pCtx->m_Acc	  = GetByte(pData);

			m_pCtx->m_Inc	  = GetByte(pData);

			m_pCtx->m_Ping	  = GetLong(pData);

			m_pCtx->m_OpID    = GetString(pData);

			m_pCtx->m_Logon   = FALSE;

			m_pCtx->m_uSlots  = GetLong(pData);

			m_pCtx->m_pSlots  = new CSlot[m_pCtx->m_uSlots];

			for( UINT u = 0; u < m_pCtx->m_uSlots; u++ ) {

				CSlot * pSlot   = &m_pCtx->m_pSlots[u];

				pSlot->m_Slot   = GetLong(pData);

				pSlot->m_Target = GetLong(pData);

				pSlot->m_Extent = GetWord(pData);

				pSlot->m_Time   = 0;

				memset(pSlot->m_Data, 0, elements(pSlot->m_Data) * sizeof(DWORD));
				}
			
			m_pCtx->m_Timeout = GetWord(pData);

			pDevice->SetContext(m_pCtx);

			m_pBase = m_pCtx;

			//ShowMap();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmRoc3MasterSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx->m_pSlots;

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

BOOL CEmRoc3MasterSerialDriver::Send(void)
{
	m_pData->Write(m_bTx, m_TxPtr, FOREVER);

/*	AfxTrace("\nTx : ");

	for( UINT u = 0; u < m_TxPtr; u++ ) {

		AfxTrace("%2.2x ", m_bTx[u]);
		}
*/
	return TRUE;
	}

BOOL CEmRoc3MasterSerialDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData  = 0;

	UINT uTotal = 0;

	m_CRC.Clear();

	m_RxPtr = 0;

//	AfxTrace("\nRx : ");

	SetTimer(m_pCtx->m_Timeout);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

//		AfxTrace("%2.2x ", uData);

		m_bRx[m_RxPtr] = uData;

		m_CRC.Add(uData);

		if( m_RxPtr == 5 ) {

			uTotal = uData + 8;
			}

		else if( uTotal ) {

			if( m_RxPtr >= (uTotal - 1) ) {
								
				return CheckCRC();
				}

			if( m_RxPtr >= sizeof(m_bRx) ) {

				// TODO:  Request next block of data !! 

				return FALSE;
				}
			}

		m_RxPtr++;
		} 

	return FALSE;
	}

// End of file
