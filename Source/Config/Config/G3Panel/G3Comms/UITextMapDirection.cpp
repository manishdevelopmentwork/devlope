
#include "Intern.hpp"

#include "UITextMapDirection.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Mapping Block Direction Selection
//

// Dynamic Class

AfxImplementDynamicClass(CUITextMapDirection, CUITextEnum);

// Constructor

CUITextMapDirection::CUITextMapDirection(void)
{
	}

// Overridables

void CUITextMapDirection::OnBind(void)
{
	CUITextElement::OnBind();

	LoadEnum();
	}

void CUITextMapDirection::OnRebind(void)
{
	CUITextElement::OnRebind();

	LoadEnum();
	}

// Implementation

void CUITextMapDirection::LoadEnum(void)
{
	CString Model = m_pItem->GetDatabase()->GetTitleName();

	AddData(0, CPrintf(L"%s to Device", Model));

	AddData(1, CPrintf(L"Device to %s", Model));
	}

// End of File
