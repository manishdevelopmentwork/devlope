
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Pmui51_HPP
	
#define	INCLUDE_Pmui51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CGpio51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Power Management and User Interface Controller
//

class CPmui51 : public IEventSink
{
	public:
		// Constructor
		CPmui51(void);

		// Destructor
		~CPmui51(void);

		// Management
		BOOL METHOD Open(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

		// Attributes
		DWORD GetIdent(void);

		// Voltage Regulators & Switchers
		UINT GetVAud (void);
		UINT GetVVid (void);
		UINT GetVPll (void);
		UINT GetVDig (void);
		UINT GetVCam (void);
		UINT GetVSd  (void);
		UINT GetVGen1(void);
		UINT GetVGen2(void);
		UINT GetVGen3(void);
		UINT GetSw   (UINT i );
		UINT GetSw1  (void);
		UINT GetSw2  (void);
		UINT GetSw3  (void);
		UINT GetSw4  (void);
		bool SetVAud (UINT uMv);
		bool SetVVid (UINT uMv);
		bool SetVPll (UINT uMv);
		bool SetVDig (UINT uMv);
		bool SetVCam (UINT uMv);
		bool SetVSd  (UINT uMv);
		bool SetVGen1(UINT uMv);
		bool SetVGen2(UINT uMv);
		bool SetVGen3(UINT uMv);
		bool SetSw1  (UINT uMv);
		bool SetSw2  (UINT uMv);
		bool SetSw3  (UINT uMv);
		bool SetSw4  (UINT uMv);
		bool SetSw   (UINT i, UINT uMv);
		UINT EnumVAud(UINT n) const;
		UINT EnumVVid(UINT n) const;
		UINT EnumVPll(UINT n) const;
		UINT EnumVDig(UINT n) const;
		UINT EnumVCam(UINT n) const;
		UINT EnumVSd (UINT n) const;
		UINT EnumVGen(UINT i, UINT n) const;
		UINT EnumSw  (UINT n, bool f) const;
		
		// Real Time Clock
		bool  GetRtcValid(void);
		DWORD GetRtcTime (void);
		DWORD GetRtcDays (void);
		DWORD GetRtcCal  (void);
		bool  SetRtcValid(void);
		bool  SetRtcTime (DWORD dwTime);
		bool  SetRtcDays (DWORD dwDay);
		bool  SetRtcCal  (DWORD dwCal);

		// Memory 
		DWORD GetMemA(void);
		DWORD GetMemB(void);
		bool  SetMemA(DWORD dwData);
		bool  SetMemB(DWORD dwData);

		// Touch 
		void AttachTouch(IEventSink *pSink, UINT uParam);
		void ReqPress(void);
		void ReqTouch(void);
		void GetTouch(UINT &x0, UINT &x1, UINT &y0, UINT &y1, UINT &z0, UINT &z1);
		
		// ADC
		UINT GetVBat(void);
		void ReqVBat(void);
		UINT GetVBatTherm(void);
		void ReqVBatTherm(void);

		// Charger
		bool ChargerResetTimer(void);

		// Signaling Leds
		void SetLed(UINT uLed, int nPercent);

	protected:
		// Registers
		enum
		{
			regIntStat0	=  0,
			regIntMask0	=  1,
			regIntSense0	=  2,
			regIntStat1	=  3,
			regIntMask1	=  4,
			regIntSense1	=  5,
			regPwrUpMode	=  6,
			regId		=  7,
			regAcc0		=  9,
			regAcc1		= 10,
			regPwrCtrl0	= 13,
			regPwrCtrl1	= 14,
			regPwrCtrl2	= 15,
			regMemoryA	= 18,
			regMemoryB	= 19,
			regRtcTime	= 20,
			regRtcAlarm	= 21,
			regRtcDay	= 22,
			regRtcDayAlarm	= 23,
			regSwitchers0	= 24,
			regSwitchers1	= 25,
			regSwitchers2	= 26,
			regSwitchers3	= 27,
			regSwitchers4	= 28,
			regSwitchers5	= 29,
			regRegCtrl0	= 30,
			regRegCtrl1	= 31,
			regRegMode0	= 32,
			regRegMode1	= 33,
			regPwrMisc	= 34,
			regAdc0		= 43,
			regAdc1		= 44,
			regAdc2		= 45,
			regAdc3		= 46,
			regAdc4		= 47,
			regCharger0	= 48,
			regUsb0		= 49,
			regChargerUsb1	= 50,
			regLed0		= 51,
			regLed1		= 52,
			regLed2		= 53,
			regLed3		= 54,
			regTrim0	= 57,
			regTrim1	= 58,
			regTest0	= 59,
			regTest1	= 60,
			regTest2	= 61,
			regTest3	= 62,
			regTest4	= 63,
			};

		// Interrupt 0
		enum
		{
			int0Adc		= Bit(0),
			int0AdcBis	= Bit(1),
			int0Touch	= Bit(2),
			int0VbusValid	= Bit(3),
			int0IdFactory	= Bit(4),
			int0UsbOvr	= Bit(5),
			int0ChargeDet	= Bit(6),
			int0ChargeFlt	= Bit(7),
			int0ChargeRev	= Bit(8),
			int0ChargeShort	= Bit(9),
			int0ChargeCv	= Bit(10),
			int0ChargeLo	= Bit(11),
			int0BpOn	= Bit(12),
			int0LoBatLo	= Bit(13),
			int0LoBatHi	= Bit(14),
			int0UsbB	= Bit(16),
			int0IdUsbHi	= Bit(19),
			int0UsbIdLo	= Bit(20),
			int0ChargeWall	= Bit(21),
			};

		// Interrupt 1
		enum
		{
			int1Rtc		= Bit(0),
			int1TimeAl	= Bit(1),
			int1PwrOn3	= Bit(2),
			int1PwrOn1	= Bit(3),
			int1PwrOn2	= Bit(4),
			int1Watchgog	= Bit(5),
			int1SysReset	= Bit(6),
			int1RtcReset	= Bit(7),
			int1PwrCut	= Bit(8),
			int1WarmStart	= Bit(9),
			int1MemHold	= Bit(10),
			int1LowPwr	= Bit(11),
			int1TempLo	= Bit(12),
			int1TempHi	= Bit(13),
			int1ClkSrc	= Bit(14),
			int1Trip	= Bit(16),
			int1BatRemoval	= Bit(22),
			};

		// ADC Control Bits
		enum
		{
			adcLithium	= Bit(0),
			adcReset	= Bit(8),
			accTouchRefOn	= Bit(10),
			adcTouchMode0	= Bit(12),
			adcTouchMode1	= Bit(13),
			adcChargeScale	= Bit(15),

			adcEnable	= Bit(0),
			adcCalibrate	= Bit(2),
			adcSelectTouch	= Bit(3),
			adcAtoxDelayOn	= Bit(19),
			adcStart	= Bit(20),
			adcNoTrigger	= Bit(21),
			};

		// Data
		IGpio          * m_pGpio;
		UINT             m_uLine;
		ISpi           * m_pSpi;
		UINT             m_uChan;
		DWORD            m_dwMask0;
		DWORD            m_dwMask1;
		IEventSink     * m_pTouch;
		UINT             m_uParam;
		
		// Initialisation
		void InitSpi(void);
		void InitEvents(void);

		// Event Handlers
		void OnAdc(void);
		void OnRtc(void);
		void OnTouch(void);

		// ADC
		void AdcReset(void);
		void AdcCalibrate(void);
		void AdcWait(void);

		// Register Helpers
		BOOL  SetBit(UINT uReg, UINT uBit);
		BOOL  ClrBit(UINT uReg, UINT uBit);
		BOOL  SetReg(UINT uReg, DWORD dwData, DWORD dwMask, UINT uBit);
		BOOL  PutReg(UINT uReg, DWORD dwData);
		DWORD GetReg(UINT uReg);

		// Friends
		friend int CompareAdc(const void *arg1, const void *arg2);
	};

// End of File

#endif
