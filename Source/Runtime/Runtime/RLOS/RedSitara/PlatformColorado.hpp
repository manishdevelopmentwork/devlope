
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformColorado_HPP

#define INCLUDE_PlatformColorado_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDss437;
class CPwm437;
class CPru437;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PlatformSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Colorado HMI
//

class CPlatformColorado : public CPlatformSitara, public IPortSwitch, public ILeds, public IInputSwitch
{
	public:
		// Constructor
		CPlatformColorado(UINT uModel);

		// Destructor
		~CPlatformColorado(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPlatform
		PCTXT METHOD GetModel(void);
		PCTXT METHOD GetVariant(void);
		UINT  METHOD GetFeatures(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);
		void METHOD SetLedLevel(UINT uPercent, bool fPersist);
		UINT METHOD GetLedLevel(void);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

	protected:
		// Data Members
		CDss437 * m_pDss;
		CPwm437 * m_pPwm[5];
		CPru437 * m_pPru;

		// IPortSwitch
		UINT METHOD GetCount(UINT uUnit);
		UINT METHOD GetMask(UINT uUnit);
		UINT METHOD GetType(UINT uUnit, UINT uLog);
		BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable);
		BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485);
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);
		BOOL METHOD SetMode(UINT uUnit, BOOL fAuto);

		// Inititialisation
		void InitClocks(void);
		void InitMpu(void);
		void InitMux(void);
		void InitGpio(void);
		void InitMisc(void);
		void InitPru(void);
		void InitUarts(void);
		void InitPwms(void);
	};

// End of File

#endif
