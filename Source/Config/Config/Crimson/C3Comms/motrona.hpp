
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_MOTRONA_HPP
	
#define	INCLUDE_MOTRONA_HPP

//////////////////////////////////////////////////////////////////////////
//
// Motrona via LECOM Device Options
//

class CMotronaDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CMotronaDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);


		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Drop;
		UINT m_Addr;
	

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Motrona Space Wrapper Class
//

class CSpaceMotrona : public CSpace
{
	public:
		// Constructors
		CSpaceMotrona(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);

		// Matching
		BOOL MatchSpace(CAddress const &Addr);
				
		// Limits
		void GetMinimum(CAddress &Addr);
		void GetMaximum(CAddress &Addr);
	};


//////////////////////////////////////////////////////////////////////////
//
// Motrona via LECOM Driver
//

class CMotronaDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CMotronaDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Implementation
		void AddSpaces(CItem * pConfig);

	};

//////////////////////////////////////////////////////////////////////////
//
// Motrona Address Selection
//

class CMotronaAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CMotronaAddrDialog(CMotronaDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
	};

// End of File

#endif
