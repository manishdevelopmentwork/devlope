
//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Intern_HPP

#define INCLUDE_Intern_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Header Files
//

#include <StdEnv.hpp>

#include <StdUtils.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Stack Interfaces
//

#include "../../StdEnv/ITcpStack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Broker
//

extern IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Checksum Calculation
//

extern WORD  Checksum(PBYTE pData, UINT uCount);
extern WORD  Checksum(PBYTE pData, UINT uCount, DWORD Load);
extern DWORD BasicSum(PBYTE pData, UINT uCount);
extern DWORD BasicSum(PBYTE pData, UINT uCount, DWORD Load);

//////////////////////////////////////////////////////////////////////////
//
// Debugging Functions
//

extern BOOL TcpDebugInit(void);
extern void TcpDebugLock(void);
extern void TcpDebugFree(void);
extern void TcpDebug(UINT Object, UINT Level, PCTXT pText, ...);
extern void TcpDebugArgs(UINT Object, UINT Level, PCTXT pText, va_list pArgs);

//////////////////////////////////////////////////////////////////////////
//
// Hardware Types
//

#define	HW_ETHERNET	0x0001

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Protocols
//

#define ET_GRE_ISO	0x00FE
#define	ET_PUP		0x0200
#define	ET_IP		0x0800
#define ET_ARP		0x0806
#define ET_REVARP	0x8035
#define ET_NS		0x0600
#define	ET_SPRITE	0x0500
#define ET_TRAIL	0x1000
#define	ET_MOPDL	0x6001
#define	ET_MOPRC	0x6002
#define	ET_DN		0x6003
#define	ET_LAT		0x6004
#define ET_SCA		0x6007
#define	ET_LANBRIDGE	0x8038
#define	ET_DECDNS	0x803C
#define	ET_DECDTS	0x803E
#define	ET_VEXP		0x805B
#define	ET_VPROD	0x805C
#define ET_ATALK	0x809B
#define ET_AARP		0x80F3
#define	ET_8021Q	0x8100
#define ET_IPX		0x8137
#define ET_IPV6		0x86DD
#define	ET_PPP		0x880B
#define	ET_MPCP		0x8808
#define	ET_SLOW		0x8809
#define	ET_MPLS		0x8847
#define	ET_MPLS_MULTI	0x8848
#define ET_PPPOED	0x8863
#define ET_PPPOES	0x8864
#define	ET_INTEL	0x886D
#define ET_JUMBO        0x8870
#define	ET_NVIEW	0x8874
#define ET_LLDP         0x88CC
#define ET_EAPOL  	0x888E
#define ET_RRCP  	0x8899
#define	ET_LOOPBACK	0x9000
#define	ET_VMAN	        0x9100
#define	ET_CFM_OLD      0xABCD
#define	ET_CFM          0x8902

//////////////////////////////////////////////////////////////////////////
//
// IP Protocol Codes
//

#define	IP_STEALTH	0x8000

#define	IP_ICMP		0x0001
#define	IP_UDP		0x0011
#define	IP_TCP		0x0006
#define	IP_RAW		0x00FE
#define	IP_QUEUE	0x00FF

//////////////////////////////////////////////////////////////////////////
//
// ARP Message Codes
//

#define	ARP_REQUEST	0x0001
#define	ARP_REPLY	0x0002

//////////////////////////////////////////////////////////////////////////
//
// ICMP Message Types
//

#define ICMP_PONG	0x00
#define	ICMP_UNREACH	0x03
#define	ICMP_REDIRECT	0x05
#define ICMP_PING	0x08
#define	ICMP_EXCEED	0x0B

//////////////////////////////////////////////////////////////////////////
//
// ICMP Unreachable Code
//

#define	UNREACH_NET	0x00
#define	UNREACH_HOST	0x01
#define	UNREACH_PROT	0x02
#define	UNREACH_PORT	0x03

//////////////////////////////////////////////////////////////////////////
//
// ICMP Exceed Code
//

#define	EXCEED_TTL	0x00
#define	EXCEED_FRAG	0x01

//////////////////////////////////////////////////////////////////////////
//
// Debugging Objects
//

#define	OBJ_NIC		1
#define	OBJ_BUFFER	2
#define	OBJ_ETHERNET	3
#define	OBJ_ARP		4
#define	OBJ_LOOPBACK	5
#define	OBJ_ROUTER	6
#define	OBJ_ICMP	7
#define	OBJ_DHCP	8
#define	OBJ_UDP		9
#define	OBJ_TCP		10
#define	OBJ_TEST	11
#define	OBJ_PPP		12
#define	OBJ_LCP		13
#define	OBJ_IPCP	14
#define	OBJ_PAP		15
#define	OBJ_HDLC	16
#define	OBJ_MODEM	17
#define	OBJ_QUEUE	18
#define	OBJ_CHAP	19
#define	OBJ_RAW		20
#define	OBJ_DYNDNS	21

//////////////////////////////////////////////////////////////////////////
//
// Debugging Levels
//

#define	LEV_ERROR	1
#define	LEV_WARN	2
#define	LEV_INFO	3
#define	LEV_TRACE	4
#define	LEV_DETAIL	5

// End of File

#endif
