
#include "intern.hpp"

#include "g3drv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 OPC Driver
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved

// Instantiator

INSTANTIATE(CG3OPCDriver);

// Constructor

CG3OPCDriver::CG3OPCDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CG3OPCDriver::~CG3OPCDriver(void)
{
	}

// Configuration

void MCALL CG3OPCDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CG3OPCDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CG3OPCDriver::Open(void)
{
	}

// Device

CCODE MCALL CG3OPCDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP	 = HostToMotor(GetLong(pData));
			m_pCtx->m_wPort	 = GetWord(pData);
			m_pCtx->m_fKeep	 = GetByte(pData);
			m_pCtx->m_fPing	 = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_fWide  = GetByte(pData);
			m_pCtx->m_pSock	 = NULL;
			m_pCtx->m_uLast	 = GetTickCount();
			m_pCtx->m_bSeq	 = 0;
			m_pCtx->m_fDirty = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CG3OPCDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CG3OPCDriver::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		StartFrame(opcPing);

		return Transact() ? CCODE_SUCCESS : CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CG3OPCDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 3 ) {

			if( m_pCtx->m_fWide ) {
			
				return DoStringReadW(Addr, pData, uCount);
				}

			return DoStringRead(Addr, pData, uCount);
			}

		if( Addr.a.m_Table == addrNamed ) {

			switch( Addr.a.m_Type ) {

				case addrLongAsLong:
					
					return DoLongRead(Addr, pData, uCount);
				
				case addrBitAsBit:
				
					return DoBitRead(Addr, pData, uCount);
				
				case addrRealAsReal:
			
					return DoRealRead(Addr, pData, uCount);
				}
			}

		return CCODE_ERROR | CCODE_HARD;
		}
		
	return CCODE_ERROR;
	}

CCODE MCALL CG3OPCDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 3 ) {

			if( m_pCtx->m_fWide ) {
			
				return DoStringWriteW(Addr, pData, uCount);
				}

			return DoStringWrite(Addr, pData, uCount);
			}

		if( Addr.a.m_Table == addrNamed ) {

			switch( Addr.a.m_Type ) {

				case addrLongAsLong:

					return DoLongWrite(Addr, pData, uCount);

				case addrBitAsBit:
			
					return DoBitWrite(Addr, pData, uCount);

				case addrRealAsReal:

					return DoRealWrite(Addr, pData, uCount);
				}
			}

		return CCODE_ERROR | CCODE_HARD;
		}			
			
	return CCODE_ERROR;
	}

UINT MCALL CG3OPCDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pCtx  = (CContext *) pContext;
	
	if( uFunc == 1 ) {
		
		// Set Device IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_IP = MotorToHost(dwValue);
		
		pCtx->m_fDirty = TRUE;

		Free(pText);
			
		return 1;    
		} 

	if( uFunc == 3 ) {
		
		//Get IP Address

		return MotorToHost(pCtx->m_IP);
		}

	return 0;
	}

// Read Handlers

CCODE CG3OPCDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagBlockRdLong);

	uCount = min(uCount, 64);

	AddWord(Addr.a.m_Offset);

	AddWord(WORD(uCount));

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		uCount = MotorToHost((WORD &) m_bRxBuff[4]);

		if( uCount ) {

			for( UINT n = 0; n < uCount; n ++ ) {
			
				DWORD x = PDWORD(m_bRxBuff + 6)[n];

				pData[n] = MotorToHost(x);
				}

			return uCount;
			}
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoRealRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagBlockRdReal);

	uCount = min(uCount, 64);

	AddWord(Addr.a.m_Offset);

	AddWord(WORD(uCount));

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		uCount = MotorToHost((WORD &) m_bRxBuff[4]);

		if( uCount ) {

			for( UINT n = 0; n < uCount; n ++ ) {
			
				DWORD x = PDWORD(m_bRxBuff + 6)[n];

				pData[n] = MotorToHost(x);
				}

			return uCount;
			}
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagBlockRdBit);

	uCount = min(uCount, 256);

	AddWord(Addr.a.m_Offset);

	AddWord(WORD(uCount));

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		uCount = MotorToHost((WORD &) m_bRxBuff[4]);

		if( uCount ) {

			for( UINT n = 0; n < uCount; n ++ ) {
			
				pData[n] = m_bRxBuff[ n + 6 ];
				}

			return uCount;
			}
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoStringRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagStringRd);
	
	WORD wTag = WORD((Addr.a.m_Offset >> 6) | (Addr.a.m_Extra << 10));

	UINT uPos = UINT(Addr.a.m_Offset & 0x3F);

	AddWord(wTag);

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		UINT  uRead = MotorToHost((WORD &) m_bRxBuff[6]);

		PCTXT pText = PCTXT(m_bRxBuff + 8) + uPos;

		for( UINT n = 0; n < uCount; n++ ) {

			if( uPos < uRead ) {

				pData[n] = pText[n];
				}
			else
				pData[n] = 0;
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoStringReadW(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagStringRd);
	
	WORD wTag = WORD((Addr.a.m_Offset >> 6) | (Addr.a.m_Extra << 10));

	UINT uPos = UINT(Addr.a.m_Offset & 0x3F);

	AddWord(wTag);

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		UINT  uRead = MotorToHost((WORD &) m_bRxBuff[6]);

		PWORD pWide = PWORD(m_bRxBuff + 8) + uPos;

		for( UINT n = 0; n < uCount; n++ ) {

			if( uPos < uRead ) {

				pData[n] = MotorToHost(pWide[n]);
				}
			else
				pData[n] = 0;
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CG3OPCDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagBlockWrLong);

	uCount = min(uCount, 64);

	AddWord(Addr.a.m_Offset);

	AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n ++ ) {
	
		AddLong(pData[n]);
		}

	if( Transact() && m_bRxBuff[2] == opcAck ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoRealWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagBlockWrReal);

	uCount = min(uCount, 64);

	AddWord(Addr.a.m_Offset);

	AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n ++ ) {
	
		AddLong(pData[n]);
		}

	if( Transact() && m_bRxBuff[2] == opcAck ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagBlockWrBit);

	uCount = min(uCount, 256);

	AddWord(Addr.a.m_Offset);

	AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n ++ ) {
	
		AddByte(BYTE(pData[n]));
		}

	if( Transact() && m_bRxBuff[2] == opcAck ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoStringWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagStringRd);

	WORD wTag = WORD((Addr.a.m_Offset >> 6) | (Addr.a.m_Extra << 10));

	UINT uPos = UINT(Addr.a.m_Offset & 0x3F);

	AddWord(wTag);

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		UINT  uRead = MotorToHost((WORD &) m_bRxBuff[6]);
		
		PCTXT pText = PCTXT(m_bRxBuff + 8);

		PTXT  pWork = PTXT (m_bTxBuff + 8);

		PTXT  pAnsi = PTXT (pWork + uPos);
		
		StartFrame(opcTagStringWr);

		strcpy(pWork, pText);

		for( UINT n = 0; n < uCount; n++ ) {

			pAnsi[n] = CHAR(pData[n]);
			}

		pAnsi[n] = 0;

		WORD wSend = WORD(strlen(pAnsi)) + 1;
	
		AddWord(wTag);

		AddWord(wSend);

		AddSkip(wSend);

		if( Transact() && m_bRxBuff[2] == opcAck ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CG3OPCDriver::DoStringWriteW(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(opcTagStringRd);

	WORD wTag = WORD((Addr.a.m_Offset >> 6) | (Addr.a.m_Extra << 10));

	UINT uPos = UINT(Addr.a.m_Offset & 0x3F);

	AddWord(wTag);

	if( Transact() && m_bRxBuff[2] == opcReply ) {

		UINT   uRead = MotorToHost((WORD &) m_bRxBuff[6]);
		
		PCWORD pText = PCWORD(m_bRxBuff + 8);

		PWORD  pWork = PWORD (m_bTxBuff + 8);

		PWORD  pWide = PWORD (pWork + uPos);
		
		StartFrame(opcTagStringWr);

		memcpy(pWork, pText, uRead * sizeof(WORD));

		for( UINT n = 0; n < uCount; n++ ) {

			pWide[n] = HostToMotor(WORD(pData[n]));
			}

		pWide[n] = 0;

		WORD wSend = uPos + uCount + 1;
	
		AddWord(wTag);

		AddWord(wSend);

		AddSkip(wSend * sizeof(WORD));

		if( Transact() && m_bRxBuff[2] == opcAck ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CG3OPCDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		if( !m_pCtx->m_fDirty ) {

			UINT Phase;

			m_pCtx->m_pSock->GetPhase(Phase);

			if( Phase == PHASE_ERROR ) {

				CloseSocket(TRUE);

				return FALSE;
				}

			if( Phase == PHASE_CLOSING ) {

				CloseSocket(FALSE);

				return FALSE;
				}
			
			return TRUE;
			}
		
		CloseSocket(FALSE);

		return FALSE;
		}

	return FALSE;
	}

BOOL CG3OPCDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		m_pCtx->m_fDirty = FALSE;

		return FALSE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		WORD         wPort = m_pCtx->m_wPort;

		if( m_pCtx->m_pSock->Connect(IP, wPort) == S_OK ) {

			m_uKeep ++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CG3OPCDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CG3OPCDriver::StartFrame(BYTE bOpcode)
{
	m_bTxBuff[0] = 0;
	
	m_bTxBuff[1] = 0;

	m_bTxBuff[2] = bOpcode;

	m_bTxBuff[3] = ++ m_pCtx->m_bSeq;

	m_uPtr = 4;
	}

void CG3OPCDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTxBuff) ) {
	
		m_bTxBuff[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CG3OPCDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CG3OPCDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

void CG3OPCDriver::AddSkip(UINT uSize)
{
	m_uPtr += uSize;
	}

// Transport Layer

BOOL CG3OPCDriver::SendFrame(void)
{
	m_bTxBuff[0] = HIBYTE(LOWORD(m_uPtr));

	m_bTxBuff[1] = LOBYTE(LOWORD(m_uPtr));
	
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CG3OPCDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 2 ) {

				UINT uTotal = MotorToHost(PWORD(m_bRxBuff)[0]);

				if( uPtr >= uTotal ) {

					if( m_bRxBuff[3] == m_bTxBuff[3] ) {

						switch( m_bRxBuff[2] ) {

							case opcAck:
							case opcReply:
								
								return TRUE;
							}
						}
					
					return FALSE;
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CG3OPCDriver::Transact(void)
{
	if( !SendFrame() || !RecvFrame() ) {

		CloseSocket(TRUE);

		return FALSE;
		}

	return TRUE;
	}

//Helpers
BOOL CG3OPCDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL  CG3OPCDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CG3OPCDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
