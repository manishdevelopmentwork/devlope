
#include "Intern.hpp"

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Expansion Object
//

// Constructor

CUsbExpansion::CUsbExpansion(IUsbHostFuncDriver *pDriver)
{
	StdSetRef();

	m_uIndex    = pDriver->GetInterface();
	
	m_wVendor   = pDriver->GetVendor();
	
	m_wProduct  = pDriver->GetProduct();

	m_wProtocol = pDriver->GetProtocol();
	}

// Destructor

CUsbExpansion::~CUsbExpansion(void)
{
	}

// IUnknown

HRESULT METHOD CUsbExpansion::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IExpansionInterface);

	StdQueryInterface(IExpansionInterface);

	return E_NOINTERFACE;
	}

ULONG METHOD CUsbExpansion::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CUsbExpansion::Release(void)
{
	StdRelease();
	}

// IExpansionInterface

PCTXT METHOD CUsbExpansion::GetName(void)
{
	return NULL;
	}

UINT METHOD CUsbExpansion::GetClass(void)
{
	return rackClassNone;
	}

UINT METHOD CUsbExpansion::GetIndex(void)
{
	return m_uIndex;
	}

UINT METHOD CUsbExpansion::GetPower(void)
{
	return 0;
	}

UINT METHOD CUsbExpansion::GetIdent(void)
{
	return MAKELONG(m_wProduct, m_wVendor);
	}

BOOL METHOD CUsbExpansion::HasBootLoader(void)
{
	return FALSE;
	}

IDevice * METHOD CUsbExpansion::MakeObject(IUsbHostFuncDriver *pDrv)
{
	return NULL;
	}

IExpansionPnp * METHOD CUsbExpansion::QueryPnpInterface(IDevice *pObj)
{
	if( pObj ) {

		IExpansionPnp *pPnp = NULL;

		pObj->QueryInterface(AfxAeonIID(IExpansionPnp), (void **) &pPnp);

		return pPnp;
		}
	
	return NULL;
	}

// End of File
