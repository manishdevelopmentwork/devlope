
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Resource File Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CStdDialog, CDialog);

// Constructor

CStdDialog::CStdDialog(void)
{
	m_fAutoClose = TRUE;
	}

// Dialog Operations

BOOL CStdDialog::Create(CWnd &Parent)
{
	if( m_Loader.Load() ) {
	        
		DWORD dwStyle   = WS_POPUP | WS_BORDER | WS_DLGFRAME | WS_SYSMENU | WS_VISIBLE;
	
		DWORD dwExStyle = WS_EX_DLGMODALFRAME;
	        
		CWnd::Create(dwExStyle, dwStyle, CRect(), Parent, 0, NULL);

		m_Loader.Free();

		return TRUE;
		}

	AfxTrace(L"ERROR: Failed to find dialog resource\n");

	AfxThrowResourceException();

	return FALSE;
	}

// Operations

void CStdDialog::SetName(ENTITY ID)
{
	m_Loader.SetName(ID);
	}

// Modeless Operation

BOOL CStdDialog::CreateModeless(CWnd &Parent)
{
	if( Create(Parent) ) {

		m_fModeless = TRUE;

		ModelessListAppend();
		
		ShowWindow(SW_SHOW);

		return TRUE;
		}

	return FALSE;
	}

// Message Map

AfxMessageMap(CStdDialog, CDialog)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_COMMAND)
	
	AfxMessageEnd(CStdDialog)
	};
	
// Message Handlers

UINT CStdDialog::OnCreate(CREATESTRUCT &Create)
{
	BuildSystemMenu();
	
	m_Loader.Process(ThisObject, TRUE);

	SendInitDialog();
		
	AutoSizeDialog(TRUE);

	PlaceDialog();
		
	return 0;
	}

BOOL CStdDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetDlgFocus(Focus);
	
	return FALSE;
	}

void CStdDialog::OnClose(void)
{
	SendMessage(WM_COMMAND, IDCANCEL);
	}

BOOL CStdDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl)
{
	if( m_fAutoClose ) {
		
		if( !m_fModeless && uID == IDCANCEL ) {
			
			EndDialog(0);
				
			return TRUE;
			}
		}
		
	return FALSE;
	}

// End of File
