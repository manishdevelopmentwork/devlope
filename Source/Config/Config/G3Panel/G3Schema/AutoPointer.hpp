
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AutoPointer_HPP
	
#define	INCLUDE_AutoPointer_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Automatic Pointer
//

template <typename CData> class DLLAPI CAutoPointer
{
	public:
		// Constructors

		STRONG_INLINE CAutoPointer(CData *pData)
		{
			m_pData = pData;
			}

		// Destructor

		STRONG_INLINE ~CAutoPointer(void)
		{
			delete m_pData;
			}

		// Operations

		STRONG_INLINE CData * TakeOver(void)
		{
			AfxAssert(m_pData);

			CData *pData = m_pData;

			m_pData      = NULL;

			return pData;
			}

		// Conversion

		STRONG_INLINE operator bool (void) const
		{
			return m_pData ? true : false;
			}

		STRONG_INLINE operator CData * (void) const
		{
			return m_pData;
			}

		// Operators

		STRONG_INLINE bool operator ! (void) const
		{
			return m_pData ? false : true;
			}

		STRONG_INLINE CData * operator -> (void) const
		{
			return m_pData;
			}

	protected:
		// Data Members
		CData * m_pData;
	};

// End of File

#endif
