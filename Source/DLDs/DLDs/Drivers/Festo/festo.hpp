
//////////////////////////////////////////////////////////////////////////
//
// Festo CONSTANTS
//
//

#define	BEL	0x07
#define	XON	0x11
#define INITZ	0x14
#define X	0x58

//////////////////////////////////////////////////////////////////////////
//
// Festo FPC, IPC, FEC Series Controller Driver
//

class CFestoFPCDriver : public CMasterDriver
{
	public:
		// Constructor
		CFestoFPCDriver(void);

		// Destructor
		~CFestoFPCDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:
		// Members
		BOOL m_fError;
		BYTE m_bRx[32];
		BYTE m_bTx[32];

		// Implementation
		void PutByte(BYTE bData);
		WORD GetByte(void);
		BOOL CheckLink(void);
		BOOL GetReply(void);
		BOOL Send(LPCTXT pText, ...);
	};

// End of File
