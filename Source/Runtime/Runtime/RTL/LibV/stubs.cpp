
#include <StdEnv.hpp>

// Type Definitions

typedef unsigned int uintptr_t;

typedef struct threadlocaleinfostruct * pthreadlocinfo;

typedef struct threadmbcinfostruct    * pthreadmbcinfo;

typedef unsigned long LCID;

#define MAX_LANG_LEN        64

#define MAX_CTRY_LEN        64

#define MAX_MODIFIER_LEN    0

#define MAX_LC_LEN          (MAX_LANG_LEN+MAX_CTRY_LEN+MAX_MODIFIER_LEN+3)

struct LC_ID
{
        unsigned short wLanguage;
        unsigned short wCountry;
        unsigned short wCodePage;
	};

struct _is_ctype_compatible
{
        unsigned long id;
        int is_clike;
	};

struct setloc_struct
{
	char *			pchLanguage;
	char *			pchCountry;
	int			iLcidState;
	int			iPrimaryLen;
	BOOL			bAbbrevLanguage;
	BOOL			bAbbrevCountry;
	LCID			lcidLanguage;
	LCID			lcidCountry;
	LC_ID			_cacheid;
	UINT			_cachecp;
	char			_cachein [MAX_LC_LEN];
	char			_cacheout[MAX_LC_LEN];
	_is_ctype_compatible	_Lcid_c[5];
	};

struct _tiddata
{
	unsigned long   _tid;				/* thread ID */
	uintptr_t	_thandle;			/* thread handle */
	int		_terrno;			/* errno value */
	unsigned long   _tdoserrno;			/* _doserrno value */
	unsigned int    _fpds;				/* Floating Point data segment */
	unsigned long   _holdrand;			/* rand() seed value */
	char *		_token;				/* ptr to strtok() token */
	wchar_t *	_wtoken;			/* ptr to wcstok() token */
	unsigned char * _mtoken;			/* ptr to _mbstok() token */
	char *		_errmsg;			/* ptr to strerror()/_strerror() buff */
	wchar_t *	_werrmsg;			/* ptr to _wcserror()/__wcserror() buff */
	char *		_namebuf0;			/* ptr to tmpnam() buffer */
	wchar_t *	_wnamebuf0;			/* ptr to _wtmpnam() buffer */
	char *		_namebuf1;			/* ptr to tmpfile() buffer */
	wchar_t *	_wnamebuf1;			/* ptr to _wtmpfile() buffer */
	char *		_asctimebuf;			/* ptr to asctime() buffer */
	wchar_t *	_wasctimebuf;			/* ptr to _wasctime() buffer */
	void *		_gmtimebuf;			/* ptr to gmtime() structure */
	char *		_cvtbuf;			/* ptr to ecvt()/fcvt buffer */
	unsigned char	_con_ch_buf[MB_LEN_MAX];	/* ptr to putch() buffer */
	unsigned short	_ch_buf_used;			/* if the _con_ch_buf is used */
	void *		_initaddr;			/* initial user thread address */
	void *		_initarg;			/* initial user thread argument */
	void *		_pxcptacttab;			/* ptr to exception-action table */
	void *		_tpxcptinfoptrs;		/* ptr to exception info pointers */
	int		_tfpecode;			/* float point exception code */
	pthreadmbcinfo  _ptmbcinfo;			/* pointer to multibyte character information */
	pthreadlocinfo  _ptlocinfo;			/* pointer to locale informaton */
	int		_ownlocale;			/* if 1, this thread owns its own locale */
	unsigned long   _NLG_dwCode;			/* needed by NLG routines */
	void *		_terminate;			/* terminate() routine */
	void *		_unexpected;			/* unexpected() routine */
	void *		_translator;			/* S.E. translator */
	void *		_purecall;			/* called when pure virtual happens */
	void *		_curexception;			/* current exception */
	void *		_curcontext;			/* current exception context */
	int		_ProcessingThrow;		/* for uncaught_exception */
	void *          _curexcspec;			/* for handling exceptions thrown from std::unexpected */
	void *		_pFrameInfoChain;		/* frame information chain */
	setloc_struct	_setloc_data;			/* setloc data */
	void *		_reserved1;			/* nothing */
	void *		_reserved2;			/* nothing */
	void *		_reserved3;			/* nothing */
	void *		_reserved4;			/* nothing */
	void *		_reserved5;			/* nothing */
	int		_cxxReThrow;			/* Set to True if it's a rethrown C++ Exception */
	unsigned long	_initDomain;			/* initial domain used by _beginthread[ex] for managed function */
};

typedef struct _tiddata * _ptiddata;

// Externals

extern "C" void * __stdcall EncodePointer(void *);

// Code

extern "C" __declspec(noreturn) void __cdecl _invoke_watson(const wchar_t *, const wchar_t *, const wchar_t *, unsigned int, uintptr_t)
{
	// TODO -- Fail somehow!!!

	__asm int 3
	}

extern "C" int  __cdecl _mtinitlocknum(int)
{
	// TODO -- Does this ever get used? !!!

	__asm int 3

	return 0;
	}

extern "C" void __cdecl _lock(int)
{
	// TODO -- Does this ever get used? !!!

	__asm int 3
	}

extern "C" void __cdecl _unlock(int)
{
	// TODO -- Does this ever get used? !!!

	__asm int 3
	}

extern "C" void __cdecl _crt_debugger_hook(int)
{
	// TODO -- Fail somehow!!!

	__asm int 3
	}

extern "C" int strcpy_s(char *d, size_t n, const char *s)
{
	strcpy(d, s);

	return 0;
	}

extern "C" void * _getptd(void)
{
	PVOID p = GetThreadLibPointer();

	if( !p ) {

		p = malloc(sizeof(struct _tiddata));

		memset(p, 0, sizeof(struct _tiddata));

		SetThreadLibPointer(p);
		}

	return p;
	}

extern "C" void * _encoded_null(void)
{
	return EncodePointer(NULL);
	}

void __cdecl _inconsistency(void)
{
	// TODO -- Fail somehow!!!

	__asm int 3
	}

void __cdecl terminate(void)
{
	// TODO -- Fail somehow!!!

	__asm int 3
	}

void __cdecl unexpected(void)
{
	// TODO -- Fail somehow!!!

	__asm int 3
	}

// End of File
