
#include "intern.hpp"

#include "festo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Festo FPC, IPC, FEC Series Controller Driver
//

// Instantiator

INSTANTIATE(CFestoFPCDriver);

// Constructor

CFestoFPCDriver::CFestoFPCDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_fError = TRUE;
	}

// Destructor

CFestoFPCDriver::~CFestoFPCDriver(void)
{
	}

// Entry Points

CCODE MCALL CFestoFPCDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'R';
	Addr.a.m_Offset = 0;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrWordAsWord;

	return Read(Addr, Data, 1);
	}

// Configuration

void MCALL CFestoFPCDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CFestoFPCDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CFestoFPCDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CFestoFPCDriver::Open(void)
{	
	}

// Device

CCODE MCALL CFestoFPCDriver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice); 
	}

CCODE MCALL CFestoFPCDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CFestoFPCDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{		
	if( m_fError ) {

		if( !CheckLink() ) {

			return CCODE_ERROR;
			}
		}

	MakeMin(uCount, 1);
	
	CTEXT sDR[]  = "DR%u";
	CTEXT sDAW[] = "DAW%u";
	CTEXT sDEW[] = "DEW%u";
	CTEXT sDMW[] = "DMW%u"; 
	CTEXT sDT[]  = "DT%u";
	CTEXT sDTW[] = "DTW%u";
	CTEXT sDTV[] = "DTV%u";
	CTEXT sDZ[]  = "DZ%u";
	CTEXT sDZW[] = "DZW%u";
	CTEXT sDZV[] = "DZV%u";	
		
	LPCTXT pForm;
		
	switch( Addr.a.m_Table ) {
	
		case 'R':	pForm = sDR;		break;
		case 'O':	pForm = sDAW;		break;
		case 'I':	pForm = sDEW;		break;
		case 'F':	pForm = sDMW;		break;  
		case 'T':	pForm = sDT;		break;
		case 'U':	pForm = sDTW;		break;
		case 'V':	pForm = sDTV;		break;
		case 'Z':	pForm = sDZ;		break;
		case 'A':       pForm = sDZW;		break;
		case 'B':       pForm = sDZV;		break;
				
		default:	return FALSE;
		
		}
	
	if( !Send(pForm, Addr.a.m_Offset) ) {
	
		m_fError = TRUE;
		
		return CCODE_ERROR;
		}
		
	if( GetReply() ) {

		*pData = WORD(ATOI(PCSTR(m_bRx)));
		
		m_fError = FALSE;
		
		return uCount;
		}
		
	m_fError = TRUE;
	
	return CCODE_ERROR;
	}

CCODE MCALL CFestoFPCDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_fError ) {
	
		if( !CheckLink() ) {

			return CCODE_ERROR;
			}
		}

	MakeMin(uCount, 1);

	CTEXT sMR[]  = "MR%u=%d";	
	CTEXT sMAW[] = "MAW%u=%d";	
	CTEXT sMMW[] = "MMW%u=%d";
	CTEXT sMT[]  = "MT%u=%d";
	CTEXT sMTW[] = "MTW%u=%d";
	CTEXT sMTV[] = "MTV%u=%d";
	CTEXT sMZ[]  = "MZ%u=%d";
	CTEXT sMZW[] = "MZW%u=%d";
	CTEXT sMZV[] = "MZV%u=%d";
		
	LPCTXT pForm;
		
	switch( Addr.a.m_Table ) {
	
		case 'R':	pForm = sMR;	 	break;
		case 'O':	pForm = sMAW;		break;
		case 'F':	pForm = sMMW;		break; 
		case 'T':	pForm = sMT;		break;
		case 'U':	pForm = sMTW;		break;
		case 'V':	pForm = sMTV;		break;
		case 'Z':       pForm = sMZ;		break;
		case 'A':       pForm = sMZW;		break;
		case 'B':       pForm = sMZV;		break;
				
		default:	return FALSE;
		
		}
	
	if( !Send(pForm, Addr.a.m_Offset, pData[0]) ) {
	
		m_fError = TRUE;
		
		return CCODE_ERROR; 
		}
		
	if( GetReply() && !m_bRx[0] ) {

		m_fError = FALSE;
		
		return uCount;
		}
		
	m_fError = TRUE;
	
	return CCODE_ERROR;
	}

// Implementation

void CFestoFPCDriver::PutByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

BOOL CFestoFPCDriver::CheckLink(void)
{
	m_pData->ClearRx();
	
	PutByte(X);

	PutByte(CR);

	Sleep(500);
	
	PutByte(INITZ);
	
	BYTE bList[] = { CR, LF, '>', XON };
	
	UINT uState  = 0; 

	UINT uTimer  = 0;

	UINT uByte   = 0;

	SetTimer(500);

	while( (uTimer = GetTimer()) ) {
	
		if ( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		if( uByte == bList[uState] ) {
		
			if( ++uState == elements(bList) ) {

				return TRUE; 
				}
			}
		else {
			if( uState == 2 ) {

				uState = 0;
				}
				
			else {
				if( uState ) {

					return FALSE;
					}
				}
			} 
		}
		
	return FALSE;
	}

BOOL CFestoFPCDriver::GetReply(void)
{
	UINT uState = 0;
	
	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uByte  = 0;

	SetTimer(500);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uByte == '=' ) {

					uState = 1;

					uCount = 0;
					}
				else {
					if( uByte == BEL ) {
						
						return FALSE;
						}
						
					if( uByte == CR ) {
					
						m_bRx[0] = 0;
						
						uState = 2;
						}
					}
				break;
				
			case 1:
				if( uByte == 0x0D ) {
				
					m_bRx[uCount++] = 0;
					
					uState = 2;
					}
				else {
					if( uByte >= 0x20 && uByte <= 0x7E ) {
					
						m_bRx[uCount++] = BYTE(uByte);
						
						if( uCount == sizeof(m_bRx) )	{
						 	
							return FALSE;
							}
						}
					}
				break;
				
			case 2:
				if( uByte == 0x0A ) {
						
					uState = 3;
					}
				else {	
					return FALSE;
					}
				break;
				
			case 3:
				if( uByte == '>' ) {

					uState = 4;
					}
				else {		
					return FALSE;
					}
				break;
				
			case 4:
				if( uByte == XON ) {
						
					return TRUE;
					}
				else {		
					return FALSE;
					}
				break;
			}
		}
		      		
	return FALSE;
	}
	
BOOL CFestoFPCDriver::Send(LPCTXT pText, ...)
{
	char sWork[32];

	va_list		ap;
	va_start(ap, pText);
	VSPrintf(sWork, pText, ap);  // Previous second param was PTXT(&pText + 1));  Doesn't work with ARM926 compiler.
	va_end(ap);
	//VSPrintf(sWork, pText, (PSTR) (&pText + 1));

	UINT uIndex = 0;

	for( ; sWork[uIndex]; uIndex++ );

	m_pData->ClearRx(); 

	for( UINT u = 0; u < uIndex; u++ ) {

		PutByte(sWork[u]);
		}

	UINT uState = 0;

	UINT uTimer = 0;

	UINT uByte  = 0;
	
	SetTimer(500);

	while( (uTimer = GetTimer()) ) {
	
		if ( ( uByte = m_pData->Read(uTimer) ) == NOTHING ) {

			continue;
			}

		if( uByte == BYTE(sWork[uState]) ) {
		
			if( ++uState == uIndex ) {
			
				PutByte(CR);
				
				return TRUE;
				}
			}
		else {
			if( uState ) {
				
				return FALSE;
				}
			}
		}
		
	return FALSE;
	}

// End of File
