
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_FileSync_HPP

#define INCLUDE_FileSync_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "ServiceItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File Sync Configuration
//

class CFileSync : public CServiceItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFileSync(void);

		// Initial Values
		void SetInitValues(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		UINT GetTreeImage(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pEnable;
		CCodedItem * m_pLogSync;
		CCodedItem * m_pFtpServer;
		CCodedItem * m_pFtpPort;
		CCodedItem * m_pFtpSSL;
		CCodedItem * m_pUser;
		CCodedItem * m_pPass;
		CCodedItem * m_pPassive;
		CCodedItem * m_pPerFile;
		CCodedItem * m_pTestSize;
		CCodedItem * m_pKeep;
		CCodedItem * m_pLogFile;
		CCodedItem * m_pFtpBase;
		CCodedItem * m_pFreq;
		CCodedItem * m_pFreqMins;
		CCodedItem * m_pDelay;
		CCodedItem * m_pDrive;
		UINT	     m_Debug;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
