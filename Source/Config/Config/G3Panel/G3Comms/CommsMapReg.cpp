
#include "Intern.hpp"

#include "CommsMapReg.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsMapBlock.hpp"
#include "CommsMapRegList.hpp"
#include "CommsMapping.hpp"
#include "CommsMappingList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Communications Mapping
//

// Dynamic Class

AfxImplementDynamicClass(CCommsMapReg, CMetaItem);

// Constructor

CCommsMapReg::CCommsMapReg(void)
{
	m_Bits  = 0;

	m_pMaps = New CCommsMappingList;
	}

// Item Location

CCommsMapRegList * CCommsMapReg::GetParentList(void) const
{
	return (CCommsMapRegList *) GetParent();
	}

CCommsMapBlock * CCommsMapReg::GetParentBlock(void) const
{
	return GetParentList()->GetParentBlock();
	}

CCommsDevice * CCommsMapReg::GetParentDevice(void) const
{
	return GetParentList()->GetParentDevice();
	}

// Attributes

BOOL CCommsMapReg::IsMapped(void) const
{
	for( UINT n = 0; n < m_Bits; n++ ) {

		if( m_pMaps->GetItem(n)->IsMapped() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CCommsMapReg::IsBroken(void) const
{
	if( !m_Bits ) {

		if( m_pMaps->GetItem(0U)->IsBroken() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CCommsMapReg::GetTreeImage(void) const
{
	if( m_Bits ) {

		CCommsMapBlock *pBlock = GetParentBlock();

		if( pBlock->IsWriteBlock() ) {

			return IDI_RED_INTEGER;
			}

		return IDI_GREEN_INTEGER;
		}

	CCommsMapping *pMap = m_pMaps->GetItem(0U);

	return pMap->GetTreeImage();
	}

CString CCommsMapReg::GetTreeLabel(void) const
{
	CString Text = GetAddrText();

	if( !m_Bits ) {

		CCommsMapping *pMap = m_pMaps->GetItem(0U);

		if( pMap->IsMapped() ) {

			Text += pMap->GetMapLinkText();

			Text += pMap->GetMapAddrText();
			}
		}

	return Text;
	}

CString CCommsMapReg::GetAddrText(void) const
{
	CCommsMapBlock * pBlock  = GetParentBlock();

	ICommsDriver   * pDriver = pBlock->GetDriver();

	CAddress  Addr = pBlock->m_Addr;

	if( Addr.m_Ref ) {

		CString Text;

		UINT uPos = GetIndex();

		Addr.a.m_Offset += uPos * C3GetTypeScale(Addr.a.m_Type);

		pDriver->ExpandAddress(Text, pBlock->GetConfig(), Addr);

		return Text;
		}

	return L"WAS ???";
	}

// Operations

BOOL CCommsMapReg::Validate(BOOL fExpand)
{
	BOOL fSave = FALSE;

	if( m_Bits ) {

		UINT Bits = GetParentBlock()->GetBitCount();

		if( m_Bits - Bits ) {

			m_pMaps->SetCount(m_Bits = Bits);

			fSave = TRUE;
			}
		}

	m_pMaps->Validate(fExpand);

	return fSave;
	}

void CCommsMapReg::Collapse(void)
{
	m_Bits = 0;

	m_pMaps->SetCount(0);

	m_pMaps->SetCount(1);

	Validate(TRUE);
	}

void CCommsMapReg::Expand(void)
{
	m_Bits = GetParentBlock()->GetBitCount();

	m_pMaps->SetCount(0);

	m_pMaps->SetCount(m_Bits);

	Validate(TRUE);
	}

// UI Creation

CViewWnd * CCommsMapReg::CreateView(UINT uType)
{
	CCommsMapping *pMap = m_pMaps->GetItem(0U);

	return pMap->CreateView(uType);
	}

// Item Naming

CString CCommsMapReg::GetHumanName(void) const
{
	CCommsMapping *pMap = m_pMaps->GetItem(0U);

	return pMap->GetHumanName();
	}

// Persistance

void CCommsMapReg::Init(void)
{
	CMetaItem::Init();

	CCommsMapping *pMap = New CCommsMapping;

	m_pMaps->AppendItem(pMap);
	}

// Download Support

BOOL CCommsMapReg::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	UINT uRefs = m_Bits ? m_Bits : 1;

	Init.AddByte(BYTE(uRefs));

	for( UINT n = 0; n < uRefs; n++ ) {

		CCommsMapping *pMap = m_pMaps->GetItem(n);

		if( pMap && !pMap->m_Disable ) {

			Init.AddLong(pMap->m_Ref);
			}
		else
			Init.AddLong(0);
		}

	return TRUE;
	}

// Meta Data Creation

void CCommsMapReg::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Bits);
	Meta_AddCollect(Maps);

	Meta_SetName((IDS_MAPPING));
	}

// End of File
