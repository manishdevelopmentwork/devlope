
#include "intern.hpp"

#include "emsnptcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson SP Premapped Driver
//

// Instantiator

INSTANTIATE(CEmersonSPPTCPDriver);

// Constructor

CEmersonSPPTCPDriver::CEmersonSPPTCPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CEmersonSPPTCPDriver::~CEmersonSPPTCPDriver(void)
{
	}

// Configuration

void MCALL CEmersonSPPTCPDriver::Load(LPCBYTE pData)
{
	}

// Management

void MCALL CEmersonSPPTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEmersonSPPTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonSPPTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonSPPTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonSPPTCPDriver::Ping(void)
{
	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) return CCODE_ERROR;

		DWORD    Data[1];

		return DoWordRead(PING_REGISTER, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEmersonSPPTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	if( !m_pCtx->m_bUnit ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	switch( Addr.a.m_Type ) {

		case addrWordAsWord:
			return DoWordRead(Addr.a.m_Offset, pData, 1);

		case addrLongAsLong:
			return DoLongRead(Addr.a.m_Offset, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEmersonSPPTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	switch( Addr.a.m_Type ) {

		case addrWordAsWord:
			return DoWordWrite(Addr.a.m_Offset, pData, 1);

		case addrLongAsLong:
			return DoLongWrite(Addr.a.m_Offset, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Frame Building

void CEmersonSPPTCPDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);

	AddByte(bOpcode);
	}

void CEmersonSPPTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {

		m_bTx[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CEmersonSPPTCPDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEmersonSPPTCPDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CEmersonSPPTCPDriver::SendFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

//**/	AfxTrace0("\r\n"); for(UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEmersonSPPTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

//**/		for( UINT k = uPtr; k < uSize + uPtr; k++ ) AfxTrace1("<%2.2x>", m_bRx[k] );

		if( uSize ) {

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CEmersonSPPTCPDriver::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CEmersonSPPTCPDriver::CheckFrame(void)
{
	if( !(m_bRx[1] & 0x80) ) {

		return TRUE;
		}

	return FALSE;
	}

// Read Handlers

CCODE CEmersonSPPTCPDriver::DoWordRead(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(3);

	AddWord( uOff - WORD_OFFSET );

	AddWord(1);

	if( Transact(FALSE) ) {

		pData[0] = LONG(SHORT(MotorToHost(*PWORD(&m_bRx[3]))));

		return 1;
		}

	return CCODE_ERROR;
	}

CCODE CEmersonSPPTCPDriver::DoLongRead(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(3);
		
	AddWord( uOff - LONG_OFFSET );

	AddWord(2);

	if( Transact(FALSE) ) {

		if( m_bRx[2] == 4 ) {

			pData[0] = MotorToHost(*PDWORD(&m_bRx[3]));

			return 1;
			}
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEmersonSPPTCPDriver::DoWordWrite(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(6);
		
	AddWord( uOff - WORD_OFFSET );
			
	AddWord(LOWORD(pData[0]));

	return Transact(TRUE) ? 1 : CCODE_ERROR;
	}

CCODE CEmersonSPPTCPDriver::DoLongWrite(UINT uOff, PDWORD pData, UINT uCount)
{
	StartFrame(16);

	AddWord( uOff - LONG_OFFSET );

	AddWord(2);

	AddByte(4);

	AddLong(pData[0]);

	return Transact(TRUE) ? 1 : CCODE_ERROR;
	}

// Helpers

// Socket Management

BOOL CEmersonSPPTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CEmersonSPPTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		}

	return FALSE;
	}

void CEmersonSPPTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
