
//////////////////////////////////////////////////////////////////////////
//
// EMCO FP-93 Driver
//

// Table Spaces
#define	C1F	1
#define	C2L	2

// String Item Table Numbers
#define	T19	3
#define	T24	4
#define	T26	5
#define	T38	6
#define	T44	7
#define	T49	8
#define	T94	9

// Named Spaces
// String Item Command Numbers
#define	C19	19
#define	C24	24
#define	C26	26
#define	C38	38
#define	C44	44
#define	C49	49
#define	C94	94

// Reals
#define	C01	 1
#define	C02	 2
#define	C03	 3
#define	C04	 4
#define	C05	 5
#define	C06	 6
#define	C07	 7
#define	C10	10
#define	C11	11
#define	C12	12
#define	C13	13
#define	C14	14
#define	C15	15
#define	C16	16
#define	C17	17
#define	C18	18
#define	C20	20
#define	C21	21
#define	C22	22
#define	C23	23
#define	C25	25
#define	C27	27
#define	C30	30
#define	C31	31
#define	C32	32
#define	C33	33
#define	C34	34
#define	C35	35
#define	C36	36
#define	C37	37
#define	C40	40
#define	C41	41
#define	C42	42
#define	C43	43
#define	C45	45
#define	C46	46
#define	C47	47
#define	C48	48
#define	C50	50
#define	C51	51
#define	C57	57
#define	C59	59
#define	C60	60
#define	C61	61
#define	C62	62
#define	C63	63
#define	C64	64
#define	C65	65
#define	C66	66
#define	C67	67
#define	C68	68
#define	C69	69

// Longs
#define	C08	 8
#define	C52	52
#define	C53	53
#define	C54	54
#define	C55	55
#define	C56	56
#define	C58	58
#define	C70	70
#define	C71	71
#define	C72	72
#define	C73	73
#define	C74	74
#define	C75	75
#define	C76	76
#define	C77	77
#define	C78	78
#define	C79	79
#define	C80	80
#define	C81	81
#define	C82	82
#define	C83	83
#define	C84	84
#define	C85	85
#define	C90	90
#define	C91	91
#define	C92	92
#define	C93	93

// Op Codes
#define	READOP	'#'
#define	WRITEOP	'@'

// Response Codes
#define	WOKPOS	7	// OK/FAULT Position for write	
#define	RESPOK	','	// No Faults
#define	RESPFLT	'!'	// Faults Present

// Read Data Response Positions
#define	RTYPE	 9	// Data Type
#define	RDATA	10	// Start of Data

// Write Data Send Positions
#define	WTYPE	 7	// Data Type
#define	WDATA	 8	// Start of Data

// Data Type Codes
#define	DTYPEF	'a'	// Float Data
#define	DTYPEL	'e'	// Long Data
#define	DTYPEH	'h'	// Hex Data
#define	DTYPES	'x'	// String Data
#define	DTYPEW	'w'	// Clear Command - Write Only
#define	DTYPEI	'z'	// Non-existent register

// Invalid Real
#define	RNAN	0x7FFFFFFF

//////////////////////////////////////////////////////////////////////////
//
// Kollmorgen 600 Base Driver
//

class CEMCOFP93Driver : public CMasterDriver
{
	public:
		// Constructor
		CEMCOFP93Driver(void);

		// Destructor
		~CEMCOFP93Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT	m_uDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[64];
		BYTE	m_bRx[64];
		BYTE	m_bTxOp[2];
		BYTE	m_bRType;
		BYTE	m_bRData[32];

		UINT	m_uPtr;
		UINT	m_uWErrCt;

		CRC16	m_CRC;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Command Definition Table

		// Execute Read/Write
		BOOL	Read( AREF Addr, PDWORD pData, UINT * pCount);
		BOOL	Write(AREF Addr, PDWORD pData, UINT * pCount);

		// Frame Building
		void	StartFrame(BYTE bOp, UINT uID);
		void	EndFrame(BOOL fIsWrite);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		void	AddText(char * cText);
		void	AddCRC(void);
		void	AddWriteData(DWORD dData, UINT uType);

		// Data String Building
		void	MakeRealData(DWORD dData);
		void	MakeLongData(DWORD dData);
		void	MakeHexData( DWORD dData);

		// Transport
		BOOL	Transact(BOOL fIsWrite);
		void	Send(void);
		BOOL	GetReply(BYTE b);

		// Response Handling
		BOOL	CheckReply(BYTE bOp);
		void	GetResponse(PDWORD pData, UINT uType, UINT *pCount);

		// Helpers
		BOOL	NoReadTransmit( AREF Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(AREF Addr, PDWORD pData);
		BOOL	IsNumeric(BYTE b);
		BOOL	GetHex(BYTE b, PBYTE r);
		BOOL	IsStringItem(UINT uID);
		BOOL	IsRealItem(UINT uID);
		BOOL	IsLongItem(UINT uID);
		BOOL	IsClearCommand(UINT uID);
		UINT	GetDataType(UINT uID);
		UINT	GetRegisterType(UINT uID);
		UINT	CheckIfString(UINT uTable, UINT uOffset);
	};

// End of File
