
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ECODRIVE_HPP

#define	INCLUDE_ECODRIVE_HPP

//////////////////////////////////////////////////////////////////////////
//
//  Ecodrive Driver
//

#define	AN	addrNamed

// Space Identifiers
#define	PCUR	1
#define	PMIN	2
#define	PMAX	3
#define SCUR	4
#define	SMIN	5
#define	SMAX	6
#define LPP	7
#define	LPV	8
#define	LPA	9
#define	LPJ	10
#define SERR	11
#define	RLST	12
#define	WLST	13
#define	LCLR	14

class CEcodriveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEcodriveDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Rexroth SIS Address Selection
//

class CEcodriveAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CEcodriveAddrDialog(CEcodriveDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void	ShowDetails(void);

		// Helpers
	};

// End of File

#endif
