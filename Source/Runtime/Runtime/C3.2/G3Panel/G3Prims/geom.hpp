
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GEOM_HPP
	
#define	INCLUDE_GEOM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimGeom;
class CPrimRect;
class CPrimEllipse;
class CPrimWedge;
class CPrimEllipseQuad;
class CPrimEllipseHalf;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Geometric Figure
//

class CPrimGeom : public CPrimWithText
{
	public:
		// Constructor
		CPrimGeom(void);

		// Destructor
		~CPrimGeom(void);

		// Initialization
		void Load(PCBYTE &pData);
		
		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

		// Data Members
		CPrimTankFill * m_pFill;
		CPrimPen      * m_pEdge;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rectangle
//

class CPrimRect : public CPrimGeom
{
	public:
		// Constructor
		CPrimRect(void);

		// Destructor
		~CPrimRect(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse
//

class CPrimEllipse : public CPrimGeom
{
	public:
		// Constructor
		CPrimEllipse(void);

		// Destructor
		~CPrimEllipse(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(ITouchMap *pTouch);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Wedge
//

class CPrimWedge : public CPrimGeom
{
	public:
		// Constructor
		CPrimWedge(void);

		// Destructor
		~CPrimWedge(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(ITouchMap *pTouch);

		// Data Members
		UINT m_Orient;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse Quadrant
//

class CPrimEllipseQuad : public CPrimWedge
{
	public:
		// Constructor
		CPrimEllipseQuad(void);

		// Destructor
		~CPrimEllipseQuad(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(ITouchMap *pTouch);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Wedge
//

class CPrimEllipseHalf : public CPrimGeom
{
	public:
		// Constructor
		CPrimEllipseHalf(void);

		// Destructor
		~CPrimEllipseHalf(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(ITouchMap *pTouch);

		// Data Members
		UINT m_Orient;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimTextBox;
class CPrimDataBox;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Text Box
//

class CPrimTextBox : public CPrimRect
{
	public:
		// Constructor
		CPrimTextBox(void);

		// Destructor
		~CPrimTextBox(void);

		// Initialization
		void Load(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Data Box
//

class CPrimDataBox : public CPrimRect
{
	public:
		// Constructor
		CPrimDataBox(void);

		// Denstructor
		~CPrimDataBox(void);

		// Initialization
		void Load(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimAnimImage;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Animatable Image
//

class CPrimAnimImage : public CPrimRect
{
	public:
		// Constructor
		CPrimAnimImage(void);

		// Destructor
		~CPrimAnimImage(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT         m_Count;
		CCodedItem * m_pValue;
		CCodedItem * m_pGray;
		CCodedItem * m_pShow;
		CPrimImage * m_pImage[10];
		BOOL         m_fAny;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT m_uImage;
			BOOL m_fColor;
			BOOL m_fShow;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
