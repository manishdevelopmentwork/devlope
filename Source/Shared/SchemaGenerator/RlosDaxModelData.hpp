
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosDaxModelData_HPP

#define INCLUDE_RlosDaxModelData_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RlosBaseModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS DAx Model Data
//

class CRlosDaxModelData : public CRlosBaseModelData
{
public:
	// Constructors
	CRlosDaxModelData(void);
	CRlosDaxModelData(CString const &Model);

	// IPxeModel
	void    METHOD MakeAppObjects(void);
	void    METHOD MakePxeObjects(void);
	BOOL    METHOD GetDispList(CArray<DWORD> &List);
	UINT    METHOD GetObjCount(char cTag);
	PCDWORD METHOD GetUsbPaths(char cTag);

protected:
	// Function Pointers
	BOOL(CRlosDaxModelData::*m_pfnGetDispList)(CArray<DWORD> &List);
	UINT(CRlosDaxModelData::*m_pfnGetObjCount)(char cTag);
	PCDWORD(CRlosDaxModelData::*m_pfnGetUsbPaths)(char cTag);

	// Implementation
	void BindModel(void);

	// Model Data
	BOOL    DA10_GetDispList(CArray<DWORD> &List);
	UINT    DA10_GetObjCount(char cTag);
	PCDWORD DA10_GetUsbPaths(char cTag);
	BOOL    DA30_GetDispList(CArray<DWORD> &List);
	UINT    DA30_GetObjCount(char cTag);
	PCDWORD DA30_GetUsbPaths(char cTag);
};

// End of File

#endif
