
#include "Intern.hpp"

#include "UITextMapAddress.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsMapBlock.hpp"
#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Mapping Block Address Selection
//

// Dynamic Class

AfxImplementDynamicClass(CUITextMapAddress, CUITextElement);

// Constructor

CUITextMapAddress::CUITextMapAddress(void)
{
	m_uFlags = textExpand;
	}

// Overridables

void CUITextMapAddress::OnBind(void)
{
	FindBinding();

	CUITextElement::OnBind();
	}

void CUITextMapAddress::OnRebind(void)
{
	FindBinding();

	CUITextElement::OnRebind();
	}

CString CUITextMapAddress::OnGetAsText(void)
{
	if( m_pBlock->m_Addr.m_Ref ) {

		return m_pBlock->m_Text;
		}

	if( !m_pBlock->m_Text.IsEmpty() ) {

		CString Text;

		Text += L"WAS ";

		Text += m_pBlock->m_Text;

		return Text;
		}

	return IDS_COMMS_NONE;
	}

UINT CUITextMapAddress::OnSetAsText(CError &Error, CString Text)
{
	if( Text.StartsWith(L"WAS ") ) {

		Text = Text.Mid(4);

		if( m_pBlock->m_Text.CompareC(Text) ) {

			m_pBlock->m_Addr.m_Ref = 0;

			m_pBlock->m_Text       = Text;

			return saveChange;
			}

		return saveSame;
		}
	else {
		FindDriver();

		CAddress Addr = m_pBlock->m_Addr;

		CAddress Last = Addr;

		if( Text == CString(IDS_COMMS_NONE) ) {

			Addr.m_Ref = 0;
			}
		else {
			if( !m_pDriver->ParseAddress(Error, Addr, m_pDeviceConfig, Text) ) {

				return saveError;
				}
			}

		if( Addr.m_Ref != Last.m_Ref ) {

			m_pBlock->m_Addr = Addr;

			if( Addr.m_Ref ) {

				m_pDriver->ExpandAddress( m_pBlock->m_Text,
							  m_pDeviceConfig,
							  m_pBlock->m_Addr
							  );

				return saveChange;
				}

			m_pBlock->m_Text.Empty();

			return saveChange;
			}

		return saveSame;
		}
	}

BOOL CUITextMapAddress::OnExpand(CWnd &Wnd)
{
	FindDriver();

	CAddress Addr = m_pBlock->m_Addr;

	CAddress Last = Addr;

	BOOL     fHit = m_pDriver->SelectAddress( afxMainWnd->GetHandle(),
					          Addr,
					          m_pDeviceConfig,
					          FALSE,
					          m_pDriverConfig
					          );

	if( fHit ) {

		if( Addr.m_Ref != Last.m_Ref ) {

			m_pBlock->m_Addr = Addr;

			m_pDriver->ExpandAddress( m_pBlock->m_Text,
						  m_pDeviceConfig,
						  m_pBlock->m_Addr
						  );

			return TRUE;
			}
		}

	return FALSE;
	}

// Implementation

void CUITextMapAddress::FindBinding(void)
{
	m_pBlock = (CCommsMapBlock *) m_pItem;

	FindDriver();
	}

void CUITextMapAddress::FindDriver(void)
{
	CCommsDevice   *pDevice = m_pBlock->GetParentDevice();

	CCommsPort     *pPort   = pDevice->GetParentPort();

	m_pDriver       = pDevice->GetDriver();

	m_pDeviceConfig = pDevice->GetConfig();

	m_pDriverConfig = pPort->m_pConfig;
	}

// End of File
