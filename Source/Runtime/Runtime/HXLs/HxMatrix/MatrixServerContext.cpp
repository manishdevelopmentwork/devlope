
#include "Intern.hpp"

#include "MatrixServerContext.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classe
//

#include "MatrixObject.hpp"

#include "MatrixServerSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix Server Context
//

// Constructor

CMatrixServerContext::CMatrixServerContext(CMatrixSsl *pSsl) : CMatrixContext(pSsl)
{
	}

// Destructor

CMatrixServerContext::~CMatrixServerContext(void)
{
	}

// IUnknown

HRESULT CMatrixServerContext::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (ITlsServerContext);

	StdQueryInterface(ITlsServerContext);

	return E_NOINTERFACE;
	}

ULONG CMatrixServerContext::AddRef(void)
{
	StdAddRef();
	}

ULONG CMatrixServerContext::Release(void)
{
	StdRelease();
	}

// ITlsServerContext

BOOL CMatrixServerContext::LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)
{
	return CMatrixContext::LoadTrustedRoots(pRoot, uRoot);
}

BOOL CMatrixServerContext::LoadServerCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass)
{
	return CMatrixContext::LoadIdentCert(pCert, uCert, pPriv, uPriv, pPass);
}

ISocket * CMatrixServerContext::CreateSocket(ISocket *pSock)
{
	if( MakeKeys(FALSE) ) {

		if( !pSock ) {

			AfxNewObject("sock-tcp", ISocket, pSock);

			AfxAssert(pSock);
			}

		return New CMatrixServerSocket(m_pSsl, pSock, m_pKeys);
		}

	return pSock;
	}

// End of File
