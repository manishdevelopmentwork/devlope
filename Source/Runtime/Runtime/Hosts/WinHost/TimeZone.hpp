
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TimeZone_HPP

#define INCLUDE_TimeZone_HPP

//////////////////////////////////////////////////////////////////////////
//
// Time Zone Storage
//

class CTimeZone : public ITimeZone
{
public:
	// Constructor
	CTimeZone(void);

	// Destructor
	~CTimeZone(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// ITimeZone
	BOOL   METHOD GetDst(void);
	INT    METHOD GetOffset(void);
	time_t METHOD GetNext(void);
	BOOL   METHOD Lock(DWORD magic);
	void   METHOD SetDst(BOOL dst);
	void   METHOD SetOffset(INT offset);
	void   METHOD SetNext(time_t next);

protected:
	// Settings
	ULONG	m_uRefs;
	BOOL    m_dst;
	INT     m_offset;
	time_t  m_next;
};

// End of File

#endif
