
#include "Intern.hpp"

#include "PosixFileCopy.hpp"

#include <io.h>

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Posix File Copier
//

// Instantiators

static IUnknown * Create_PosixFileCopier(PCTXT pName)
{
	return New CPosixFileCopy();
}

// Registration

global void Register_PosixFileCopier(void)
{
	piob->RegisterInstantiator("fs.copier", Create_PosixFileCopier);
}

global void Revoke_PosixFileCopier(void)
{
	piob->RevokeInstantiator("fs.copier");
}

// Constructor

CPosixFileCopy::CPosixFileCopy(void)
{
	StdSetRef();
}

// Destrictor

CPosixFileCopy::~CPosixFileCopy(void)
{
}

// IUnknown

HRESULT CPosixFileCopy::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IFileCopier);

	StdQueryInterface(IFileCopier);

	return E_NOINTERFACE;
}

ULONG CPosixFileCopy::AddRef(void)
{
	StdAddRef();
}

ULONG CPosixFileCopy::Release(void)
{
	StdRelease();
}

// IFileCopier

BOOL CPosixFileCopy::CopyFile(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	UINT uResult = OpenFiles(pSrc, pDst, uFlags);

	if( uResult < NOTHING ) {

		if( uResult > 0 ) {

			if( CopyFile() ) {

				CloseFiles();

				struct stat s;

				if( !stat(pSrc, &s) ) {

					utime(pDst, s.st_mtime);
				}
			}
			else {
				CloseFiles();

				unlink(pDst);

				return FALSE;
			}
		}

		if( uFlags & flagDelete ) {

			unlink(pSrc);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CPosixFileCopy::MoveFile(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	return CopyFile(pSrc, pDst, uFlags | flagDelete);
}

BOOL CPosixFileCopy::CopyDir(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	mkdir(pDst, 0);

	CAutoDirentList List;

	BOOL fScan = FALSE;

	if( (fScan = List.ScanFiles(pSrc)) > 0 ) {

		char src[MAX_PATH];

		char dst[MAX_PATH];

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			struct dirent *p = List[n];

			PathJoin(src, pSrc, p->d_name);

			PathJoin(dst, pDst, p->d_name);

			if( !CopyFile(src, dst, uFlags) ) {

				return FALSE;
			}
		}
	}

	if( uFlags & copyRecursive ) {

		CAutoDirentList List;

		if( List.ScanDirs(pSrc) ) {

			char src[MAX_PATH];

			char dst[MAX_PATH];

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				struct dirent *p = List[n];

				PathJoin(src, pSrc, p->d_name);

				PathJoin(dst, pDst, p->d_name);

				if( !CopyDir(src, dst, uFlags) ) {

					return FALSE;
				}
			}

			fScan = TRUE;
		}
	}

	if( uFlags & flagDelete ) {

		rmdir(pSrc);
	}

	return fScan;
}

BOOL CPosixFileCopy::MoveDir(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	return CopyDir(pSrc, pDst, uFlags | flagDelete);
}

// Implementation

UINT CPosixFileCopy::OpenFiles(PCTXT pSrc, PCTXT pDst, UINT uFlags)
{
	if( m_Src.Open(pSrc, (uFlags & flagDelete) ? "r+" : "r") ) {

		if( m_Dst.Open(pDst, "r+") ) {

			if( uFlags & copyOverwrite ) {

				return 1;
			}

			if( uFlags & copyDiff ) {

				if( m_Src.GetSize() != m_Dst.GetSize() ) {

					return 1;
				}

				if( m_Src.GetUnix() != m_Dst.GetUnix() ) {

					return 1;
				}
			}

			if( uFlags & copyNew ) {

				if( m_Src.GetUnix() > m_Dst.GetUnix() ) {

					return 1;
				}
			}

			m_Dst.Close();

			m_Src.Close();

			return 0;
		}
		else {
			if( m_Dst.Open(pDst, "w") ) {

				return 1;
			}
		}

		m_Src.Close();
	}

	return NOTHING;
}

void CPosixFileCopy::CloseFiles(void)
{
	m_Src.Close();

	m_Dst.Close();
}

bool CPosixFileCopy::CopyFile(void)
{
	UINT uSize = 256 * 1024;

	CAutoArray<BYTE> Buff(uSize);

	for( ;;) {

		UINT uRead = m_Src.Read(Buff, uSize);

		if( uRead == NOTHING ) {

			return false;
		}

		if( uRead ) {

			if( m_Dst.Write(Buff, uRead) != uRead ) {

				return false;
			}

			if( uRead == uSize ) {

				continue;
			}
		}

		break;
	}

	m_Dst.Truncate();

	return true;
}

bool CPosixFileCopy::PathAppendSlash(char *path)
{
	#if defined(AEON_PLAT_LINUX)

	char const s = '/';

	#else

	char const s = '\\';

	#endif

	UINT n = strlen(path);

	if( !n || path[n-1] != s ) {

		path[n++] = s;

		path[n++] = 0;

		return true;
	}

	return false;
}

void CPosixFileCopy::PathJoin(char *path, char const *p1, char const *p2)
{
	strcpy(path, p1);

	PathAppendSlash(path);

	strcat(path, p2);
}

// End of File
