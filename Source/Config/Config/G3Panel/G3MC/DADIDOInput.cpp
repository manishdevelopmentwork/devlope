
#include "intern.hpp"

#include "dadidoinput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dadidoprops.h"

#include "import/manticore/dadidodbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI
//

// Runtime Class

AfxImplementRuntimeClass(CDADIDOInput, CCommsItem);

// Property List

CCommsList const CDADIDOInput::m_CommsList[] = {

	{ 1, "Input1",	PROPID_INPUT1,	usageRead,  IDS_NAME_I1  },
	{ 1, "Input2",	PROPID_INPUT2,	usageRead,  IDS_NAME_I2  },
	{ 1, "Input3",	PROPID_INPUT3,	usageRead,  IDS_NAME_I3  },
	{ 1, "Input4",	PROPID_INPUT4,	usageRead,  IDS_NAME_I4  },
	{ 1, "Input5",	PROPID_INPUT5,	usageRead,  IDS_NAME_I5  },
	{ 1, "Input6",	PROPID_INPUT6,	usageRead,  IDS_NAME_I6  },
	{ 1, "Input7",	PROPID_INPUT7,	usageRead,  IDS_NAME_I7  },
	{ 1, "Input8",	PROPID_INPUT8,	usageRead,  IDS_NAME_I8  },

	{ 2, "Count1",	PROPID_COUNT1,	usageRead,  IDS_NAME_CV1 },
	{ 2, "Count2",	PROPID_COUNT2,	usageRead,  IDS_NAME_CV2 },
	{ 2, "Count3",	PROPID_COUNT3,	usageRead,  IDS_NAME_CV3 },
	{ 2, "Count4",	PROPID_COUNT4,	usageRead,  IDS_NAME_CV4 },
	{ 2, "Count5",	PROPID_COUNT5,	usageRead,  IDS_NAME_CV5 },
	{ 2, "Count6",	PROPID_COUNT6,	usageRead,  IDS_NAME_CV6 },
	{ 2, "Count7",	PROPID_COUNT7,	usageRead,  IDS_NAME_CV7 },
	{ 2, "Count8",	PROPID_COUNT8,	usageRead,  IDS_NAME_CV8 },

};

// Constructor

CDADIDOInput::CDADIDOInput(void)
{
	m_Input1 = 0;
	m_Input2 = 0;
	m_Input3 = 0;
	m_Input4 = 0;
	m_Input5 = 0;
	m_Input6 = 0;
	m_Input7 = 0;
	m_Input8 = 0;

	m_Count1 = 0;
	m_Count2 = 0;
	m_Count3 = 0;
	m_Count4 = 0;
	m_Count5 = 0;
	m_Count6 = 0;
	m_Count7 = 0;
	m_Count8 = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDADIDOInput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return CString(L"Inputs");

		/*case 2:	return CString(L"Counters");*/
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDADIDOInput::AddMetaData(void)
{
	Meta_AddInteger(Input1);
	Meta_AddInteger(Input2);
	Meta_AddInteger(Input3);
	Meta_AddInteger(Input4);
	Meta_AddInteger(Input5);
	Meta_AddInteger(Input6);
	Meta_AddInteger(Input7);
	Meta_AddInteger(Input8);

	Meta_AddInteger(Count1);
	Meta_AddInteger(Count2);
	Meta_AddInteger(Count3);
	Meta_AddInteger(Count4);
	Meta_AddInteger(Count5);
	Meta_AddInteger(Count6);
	Meta_AddInteger(Count7);
	Meta_AddInteger(Count8);

	CCommsItem::AddMetaData();
}


// End of File
