
#include "intern.hpp"

#include "pps4201.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Klockner Moeller PS4-201 Driver
//

// Instantiator

ICommsDriver *	Create_PPS4201Driver(void)
{
	return New CPPS4201Driver;
	}

// Constructor

CPPS4201Driver::CPPS4201Driver(void)
{
	m_wID		= 0x3325;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Klockner Moeller";
	
	m_DriverName	= "PS4-201";
	
	m_Version	= "1.03";
	
	m_ShortName	= "PS4-201 Master";

	AddSpaces();
	}

// Binding Control

UINT CPPS4201Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CPPS4201Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CPPS4201Driver::GetDeviceConfig(void)
{
	return NULL;
	}


// Implementation

void CPPS4201Driver::AddSpaces(void)
{
	AddSpace(New CSpace('M', "MB",	 "Marker Bytes",	10, 0, 9999,	addrByteAsWord));
								
	AddSpace(New CSpace('C', "MISC", "Misc Registers",	10, 0, 1,	addrWordAsWord));
	}

// End of File
