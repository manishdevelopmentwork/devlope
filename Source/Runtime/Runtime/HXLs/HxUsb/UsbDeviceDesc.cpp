
#include "Intern.hpp"

#include "UsbDeviceDesc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Device Descriptor
//

// Constructor

CUsbDeviceDesc::CUsbDeviceDesc(void)
{
	Init();
	}

// Endianess

void CUsbDeviceDesc::HostToUsb(void)
{
	m_wVersion   = HostToIntel(m_wVersion);
	
	m_wVendorID  = HostToIntel(m_wVendorID);

	m_wProductID = HostToIntel(m_wProductID);

	m_wDeviceVer = HostToIntel(m_wDeviceVer);
	}

void CUsbDeviceDesc::UsbToHost(void)
{
	m_wVersion   = IntelToHost(m_wVersion);
	
	m_wVendorID  = IntelToHost(m_wVendorID);

	m_wProductID = IntelToHost(m_wProductID);

	m_wDeviceVer = IntelToHost(m_wDeviceVer);
	}

// Attributes

BOOL CUsbDeviceDesc::IsValid(void) const
{
	return m_bType == descDevice && m_bLength == sizeof(UsbDeviceDesc);
	}

// Init

void CUsbDeviceDesc::Init(void)
{
	memset(this, 0, sizeof(UsbDeviceDesc));
	
	m_bLength   = sizeof(UsbDeviceDesc);
	
	m_bType	    = descDevice;

	m_wVersion  = 0x0200;
	}

// Debug

void CUsbDeviceDesc::Debug(void)
{
	#if defined(_XDEBUG)

	AfxTrace("\nUsb Device Descriptor\n");

	AfxTrace("Type         = %d\n",      m_bType);
	AfxTrace("Len          = %d\n",      m_bLength);
	AfxTrace("Version      = 0x%4.4X\n", m_wVersion);
	AfxTrace("Class        = 0x%2.2X\n", m_bClass);
	AfxTrace("Subclass     = 0x%2.2X\n", m_bSubClass);
	AfxTrace("Protocol     = 0x%2.2X\n", m_bProtocol);
	AfxTrace("Max Packet   = %3.3d\n",   m_bMaxPacket);
	AfxTrace("Vendor       = 0x%4.4X\n", m_wVendorID);
	AfxTrace("Product      = 0x%4.4X\n", m_wProductID);
	AfxTrace("Device Ver   = 0x%4.4X\n", m_wDeviceVer);
	AfxTrace("iVendor      = 0x%2.2X\n", m_bVendorIdx);
	AfxTrace("iProduct     = 0x%2.2X\n", m_bProductIdx);
	AfxTrace("iSerial      = 0x%2.2X\n", m_bSerialNumIdx);
	AfxTrace("Config Count = %d\n",      m_bConfigCount);

	#endif
	}

// End of File
