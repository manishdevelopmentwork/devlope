
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

class CAnalogInputTCPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAnalogInputTCPage(CAnalogInputBaseItem *pInput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAnalogInputBaseItem  * m_pInput;
		CEt3CommsSystem       * m_pSystem;
		BOOL			m_fFeature1;

		// Implementation
		void LoadIntegration(IUICreate *pView);
		void LoadTemperature(IUICreate *pView);
		void LoadChannels   (IUICreate *pView);
		void AddRangeUI     (IUICreate *pView, UINT uChan);
		void AddFeature1UI  (IUICreate *pView, UINT uChan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

// Runtime Class

AfxImplementRuntimeClass(CAnalogInputTCPage, CUIStdPage);

// Constructor

CAnalogInputTCPage::CAnalogInputTCPage(CAnalogInputBaseItem *pInput)
{
	m_Class     = AfxRuntimeClass(CAnalogInputBaseItem);	

	m_pInput    = pInput;

	m_fFeature1 = TRUE;

	m_pSystem   = (CEt3CommsSystem *) pInput->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CAnalogInputTCPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadIntegration(pView);

	LoadTemperature(pView);

	LoadChannels(pView);

	return FALSE;
	}

// Implementation

void CAnalogInputTCPage::LoadIntegration(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAIIntegration") ) {

		CString Form;

		if( TRUE ) {

			Form += L"";	Form += "128|200ms per channel";
			Form += L"|";	Form += "129|100ms per channel";
			Form += L"|";	Form += "130|50ms per channel";
			Form += L"|";	Form += "131|25ms per channel";
			Form += L"|";	Form += "132|12.5ms per channel";
			Form += L"|";	Form += "133|6.25ms per channel";
			Form += L"|";	Form += "134|3.12s per channel";
			Form += L"|";	Form += "135|1.5ms per channel";
			Form += L"|";	Form += "136|1ms per channel";
			Form += L"|";	Form += "137|0.5ms per channel";
			}

		//
		pView->StartGroup(CString(IDS_OPTIONS), 1);

		CUIData Data;

		Data.m_Tag	 = "Integration";

		Data.m_Label 	 = CString(IDS_INTEGRATION);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);

		pView->EndGroup(TRUE);	
		}
	}

void CAnalogInputTCPage::LoadTemperature(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAITemperature") ) {

		pView->StartGroup(CString(IDS_TEMPERATURE), 1);

		pView->AddUI(m_pInput, L"root", L"TempUnits");

		pView->AddUI(m_pInput, L"root", L"TempFormat");
	
		pView->EndGroup(TRUE);
		}
	}

void CAnalogInputTCPage::LoadChannels(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAIs()) ) {

		UINT uCols = 0;

		if( TRUE ) {
			
			uCols ++;
			}

		if( m_fFeature1 ) {
			
			uCols ++;
			}

		pView->StartTable(CString(IDS_CHANNELS), uCols);

		if( TRUE ) {
			
			pView->AddColHead(CString(IDS_THERMOCOUPLE_TYPE));
			}

		if( m_fFeature1 ) {

			pView->AddColHead(CString(IDS_BURNOUT_DETECTION));
			}

		for( UINT n = 0; n < uCount; n ++ ) {

			pView->AddRowHead(CPrintf(L"AI%d", 1 + n));

			AddRangeUI   (pView, n);

			AddFeature1UI(pView, n);
			}

		pView->EndTable();
		}
	}

void CAnalogInputTCPage::AddRangeUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "-1|Disabled";
			Form += "|";	Form += "0|Type J";
			Form += "|";	Form += "1|Type K";
			Form += "|";	Form += "2|Type E";
			Form += "|";	Form += "3|Type R";
			Form += "|";	Form += "4|Type T";
			Form += "|";	Form += "5|Type B";
			Form += "|";	Form += "6|Type C";
			Form += "|";	Form += "7|Type N";
			Form += "|";	Form += "8|Type S";
			Form += "|";	Form += "131|+-62.5mV";
			Form += "|";	Form += "132|+-125mV";
			Form += "|";	Form += "133|+-250mV";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Range%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Range", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputTCPage::AddFeature1UI(IUICreate *pView, UINT uChan)
{
	if( m_fFeature1 ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "1|Disabled";
			Form += "|";	Form += "2|Upscale";
			Form += "|";	Form += "3|Downscale";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Feature1%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature1", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogInputTCItem, CAnalogInputBaseItem);

// Constructor

CAnalogInputTCItem::CAnalogInputTCItem(void)
{
	m_uType	= typeThermocouple;
	}

// UI Creation

BOOL CAnalogInputTCItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CAnalogInputTCPage(this));

	return FALSE;
	}

// UI Update

void CAnalogInputTCItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Range") ) {

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistence

void CAnalogInputTCItem::Init(void)
{
	CAnalogInputBaseItem::Init();

	m_Integration	 = 128;

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		m_Range[n] = 0;
		}

	for( UINT n = 0; n < elements(m_Feature1); n ++ ) {

		m_Feature1[n] = 1;
		}
	}

// Download Support

void CAnalogInputTCItem::PrepareData(void)
{
	CAnalogInputBaseItem::PrepareData();

	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysAIs();

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CAnalogInput &AI = File.m_AIs[n];

		if( m_Range[n] == -1 ) {
			
			UINT uRange = 0;

			AI.SetRange(uRange);

			continue;
			}

		if( m_Range[n] & 0x80 ) {

			UINT uRange = 0;

			uRange |= m_Range[n] & 0x7F;
			
			AI.SetRange(uRange);					
			}
		else {		
			UINT uRange = 0;

			uRange |= m_Feature1[n] << 4;
		
			uRange |= m_Range[n];

			AI.SetRange(uRange);
			}
		}
	}

// Meta Data Creation

void CAnalogInputTCItem::AddMetaData(void)
{
	CAnalogInputBaseItem::AddMetaData();
	}

// Implementation

void CAnalogInputTCItem::DoEnables(IUIHost *pHost)
{
	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		BOOL fIsVolt = m_Range[n] & 0x80;

		BOOL fEnable = m_Range[n] != -1;

		pHost->EnableUI(CPrintf(L"Feature1%2.2d", 1+n), fEnable && !fIsVolt);
		}
	}

// End of File
