
#include "Intern.hpp"

#include "SqlSync.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CodedText.hpp"
#include "CommsSystem.hpp"
#include "DataLog.hpp"
#include "DataLogger.hpp"
#include "DataLogList.hpp"
#include "SqlSyncSelectionDlg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Sync Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CSqlSync, CServiceItem);

// Constructor

CSqlSync::CSqlSync(void)
{
	m_pEnable         = NULL;
	m_pDatabaseServer = NULL;
	m_pPort           = NULL;
	m_pTlsMode	  = NULL;
	m_pUser           = NULL;
	m_pPass           = NULL;
	m_pDatabaseName   = NULL;
	m_pTablePrefix    = NULL;
	m_pFreq           = NULL;
	m_pFreqMins       = NULL;
	m_pDelay          = NULL;
	m_pLogFile        = NULL;
	m_pForce	  = NULL;
	m_pCheckLabels    = NULL;
	m_pLogSelect	  = NULL;
	m_pSyncSecurity	  = NULL;
	m_pSyncEvents     = NULL;
	m_pFireTriggers   = NULL;
	m_pDrive          = NULL;
	m_PrimaryKey	  = 0;
	m_LogInit         = FALSE;
	}

// UI Update

void CSqlSync::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Enable" || Tag == "Force" ) {

		DoEnables(pHost);
		}

	if( Tag == "LogSelect" ) {

		CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

		CDataLogList *pLogs = pSystem->m_pLog->m_pLogs;

		CSqlSyncSelectionDlg Dlg(this, pLogs);

		if( Dlg.Execute() ) {

			SetDirty();
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Initial Values

void CSqlSync::SetInitValues(void)
{
	SetInitial(L"Enable",		m_pEnable,		0);
	SetInitial(L"DatabaseServer",	m_pDatabaseServer,	0);
	SetInitial(L"Port",		m_pPort,		1433);
	SetInitial(L"TlsMode",		m_pTlsMode,		0);
	SetInitial(L"Freq",		m_pFreq,		1);
	SetInitial(L"FreqMins",		m_pFreqMins,		0);
	SetInitial(L"Delay",		m_pDelay,		0);
	SetInitial(L"LogFile",		m_pLogFile,		0);
	SetInitial(L"Force",		m_pForce,		0);
	SetInitial(L"CheckLabels",	m_pCheckLabels,		0);
	SetInitial(L"FireTriggers",     m_pFireTriggers,        0);
	SetInitial(L"SyncSecurity",	m_pSyncSecurity,	1);
	SetInitial(L"SyncEvents",	m_pSyncEvents,		1);
	SetInitial(L"Drive",            m_pDrive,               0);
	}

// Type Access

BOOL CSqlSync::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"User" || Tag == L"Pass" || Tag == L"DatabaseName" || Tag == L"TablePrefix") {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
	}

// Attributes

BOOL CSqlSync::IsEnabled(void) const
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B;
	}

UINT CSqlSync::GetTreeImage(void) const
{
	return IDI_SQL_SYNC;
	}

// Download Support

void CSqlSync::PrepareData(void)
{
	if( !m_LogInit ) {

		m_LogNames.Empty();

		IncludeAllLogs(m_LogNames);
		}
	}

BOOL CSqlSync::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(7));

	CServiceItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pEnable);

	Init.AddItem(itemVirtual, m_pDatabaseServer);

	Init.AddItem(itemVirtual, m_pPort);

	Init.AddItem(itemVirtual, m_pTlsMode);

	Init.AddItem(itemEncrypt, m_pUser);

	Init.AddItem(itemEncrypt, m_pPass);

	Init.AddItem(itemVirtual, m_pDatabaseName);

	Init.AddItem(itemVirtual, m_pTablePrefix);

	Init.AddItem(itemVirtual, m_pFreq);

	Init.AddItem(itemVirtual, m_pFreqMins);

	Init.AddItem(itemVirtual, m_pDelay);

	Init.AddItem(itemVirtual, m_pLogFile);

	Init.AddItem(itemVirtual, m_pForce);

	Init.AddItem(itemVirtual, m_pCheckLabels);

	Init.AddItem(itemVirtual, m_pFireTriggers);

	Init.AddItem(itemVirtual, m_pDrive);

	Init.AddByte(BYTE(m_PrimaryKey));

	IncludeEventsSecure(m_LogNames);

	UINT uCount = m_LogNames.GetCount();

	Init.AddLong(uCount);

	for( UINT n = 0; n < uCount; n++ ) {

		Init.AddText(m_LogNames[n]);
		}

	return TRUE;
	}

// Persistance

void CSqlSync::Load(CTreeFile &File)
{
	AddMeta();

	while( !File.IsEndOfData() ) {

		CString Name = File.GetName();

		if( Name == L"LogNames" ) {

			File.GetObject();

			LoadNames(File);

			File.EndObject();
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(File, pMeta);
			}
		}

	RegisterHandle();
	}

void CSqlSync::LoadNames(CTreeFile &File)
{
	File.GetName();

	UINT uCount = File.GetValueAsInteger();

	while( uCount-- ) {

		File.GetName();

		CString LogName = File.GetValueAsString();

		m_LogNames.Append(LogName);
		}
	}

void CSqlSync::Save(CTreeFile &File)
{
	CServiceItem::Save(File);

	File.PutObject(TEXT("LogNames"));

	UINT uCount = m_LogNames.GetCount();

	File.PutValue(TEXT("LogNameCount"), uCount);

	for( UINT n = 0; n < uCount; n++ ) {

		CString LogName = m_LogNames[n];

		File.PutValue(TEXT("LogName"), LogName);
		}

	File.EndObject();
	}

// Meta Data Creation

void CSqlSync::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(DatabaseServer);
	Meta_AddVirtual(Port);
	Meta_AddVirtual(TlsMode);
	Meta_AddVirtual(User);
	Meta_AddVirtual(Pass);
	Meta_AddVirtual(DatabaseName);
	Meta_AddVirtual(TablePrefix);
	Meta_AddVirtual(Freq);
	Meta_AddVirtual(FreqMins);
	Meta_AddVirtual(Delay);
	Meta_AddVirtual(LogFile);
	Meta_AddVirtual(Force);
	Meta_AddVirtual(CheckLabels);
	Meta_AddVirtual(FireTriggers);
	Meta_AddVirtual(LogSelect);
	Meta_AddVirtual(SyncSecurity);
	Meta_AddVirtual(SyncEvents);
	Meta_AddVirtual(Drive);
	Meta_AddInteger(PrimaryKey);
	Meta_AddInteger(LogInit);

	Meta_SetName((IDS_SQL_SYNC));
	}

// Implementation

void CSqlSync::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable,  L">=", 1, TRUE);

	BOOL fForce  = CheckEnable(m_pForce,   L">=", 1, TRUE);

	AfxTouch(fEnable);

	pHost->EnableUI("DatabaseServer", fEnable);
	pHost->EnableUI("Port"          , fEnable);
	pHost->EnableUI("TlsMode"       , fEnable);
	pHost->EnableUI("User"          , fEnable);
	pHost->EnableUI("Pass"          , fEnable);
	pHost->EnableUI("DatabaseName"  , fEnable);
	pHost->EnableUI("TablePrefix"   , fEnable);
	pHost->EnableUI("PrimaryKey"    , fEnable);
	pHost->EnableUI("Force"		, fEnable);
	pHost->EnableUI("Freq"          , fEnable && !fForce);
	pHost->EnableUI("FreqMins"      , fEnable && !fForce);
	pHost->EnableUI("Delay"         , fEnable && !fForce);
	pHost->EnableUI("LogFile"       , fEnable);
	pHost->EnableUI("CheckLabels"	, fEnable);
	pHost->EnableUI("FireTriggers"  , fEnable);
	pHost->EnableUI("LogSelect"	, fEnable);
	pHost->EnableUI("SyncSecurity"	, fEnable);
	pHost->EnableUI("SyncEvents"	, fEnable);
	pHost->EnableUI("Drive"         , fEnable);
	}

void CSqlSync::IncludeAllLogs(CStringArray &LogNames)
{
	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	CDataLogList *pLogs = pSystem->m_pLog->m_pLogs;

	for( INDEX i = pLogs->GetHead(); !pLogs->Failed(i); pLogs->GetNext(i) ) {

		CDataLog *pLog = pLogs->GetItem(i);

		if( pLog ) {

			CString Name;

			if( !pLog->m_Path.IsEmpty() )

				Name = pLog->m_Path;
			else
				Name = pLog->m_Name;

			if( LogNames.Find(Name) == NOTHING ) {

				LogNames.Append(Name);
				}
			}
		}
	}

void CSqlSync::IncludeEventsSecure(CStringArray &LogNames)
{
	const CString SecureName(L"SECURE");
	const CString EventsName(L"EVENTS");

	if( m_pSyncSecurity->ExecVal() == 0 ) {

		UINT uFind = LogNames.Find(SecureName);

		if( uFind < NOTHING ) {

			LogNames.Remove(uFind);
			}
		}
	else {
		if( LogNames.Find(SecureName) == NOTHING ) {

			LogNames.Append(SecureName);
			}
		}

	if( m_pSyncEvents->ExecVal() == 0 ) {

		UINT uFind = LogNames.Find(EventsName);

		if( uFind < NOTHING ) {

			LogNames.Remove(uFind);
			}
		}
	else {
		if( LogNames.Find(EventsName) == NOTHING ) {

			LogNames.Append(EventsName);
			}
		}
	}

// End of File
