
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_FakeSocket_HPP

#define INCLUDE_AppObject_HPP

//////////////////////////////////////////////////////////////////////////
//
// Fake Socket
//

class CFakeSocket : public ISocket
{
public:
	// Constructor
	CFakeSocket(void);

	// Operations
	bool GetTxData(PCBYTE &pData, UINT &uSize);
	bool SetRxData(PCBYTE pData, UINT uSize);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ISocket
	HRM Listen(WORD Loc);
	HRM Listen(IPADDR const &IP, WORD Loc);
	HRM Connect(IPADDR const &IP, WORD Rem);
	HRM Connect(IPADDR const &IP, WORD Rem, WORD Loc);
	HRM Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc);
	HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
	HRM Recv(PBYTE pData, UINT &uSize);
	HRM Send(PBYTE pData, UINT &uSize);
	HRM Recv(CBuffer * &pBuff, UINT uTime);
	HRM Recv(CBuffer * &pBuff);
	HRM Send(CBuffer   *pBuff);
	HRM GetLocal(IPADDR &IP);
	HRM GetRemote(IPADDR &IP);
	HRM GetLocal(IPADDR &IP, WORD &Port);
	HRM GetRemote(IPADDR &IP, WORD &Port);
	HRM GetPhase(UINT &Phase);
	HRM SetOption(UINT uOption, UINT uValue);
	HRM Abort(void);
	HRM Close(void);

protected:
	// Data Members
	ULONG m_uRefs;
	BYTE  m_bTxBuff[8192];
	BYTE  m_bRxBuff[8192];
	UINT  m_uTxBuff;
	UINT  m_uRxBuff;
};

// End of File

#endif
