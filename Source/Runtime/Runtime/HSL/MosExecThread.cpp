
#include "Intern.hpp"

#include "MosExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MosExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modern OS Executive Thread Object
//

// Externals

clink void __sinit(struct _reent *);

// Constructor

CMosExecThread::CMosExecThread(CMosExecutive *pExec) : CBaseExecThread(pExec)
{
	m_pExec      = pExec;

	m_uTimer     = 0;

	m_uSlept     = 0;

	m_pOwnedHead = NULL;

	m_pOwnedTail = NULL;
	}

// Destructor

CMosExecThread::~CMosExecThread(void)
{
	m_pExec->RemoveThread(this);

	DeleteImpure();
	}

// IThread

UINT CMosExecThread::GetTimer(void)
{
	// TODO -- Handle the 50-day wrap around!!!

	if( m_uTimer ) {

		DWORD t = m_pExec->GetTickCount();

		if( t >= m_uTimer ) {

			m_uTimer = 0;

			return 0;
			}

		return m_pExec->ToTime(m_uTimer - t);
		}

	return 0;
	}

void CMosExecThread::SetTimer(UINT uTime)
{
	m_uTimer = m_pExec->GetTickCount() + m_pExec->ToTicks(uTime);
	}

// Overridables

BOOL CMosExecThread::OnCreate(void)
{
	m_uTimer = 0;

	return TRUE;
	}

// End of File
