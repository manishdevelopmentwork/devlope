
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Et3 Download Link
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_E3LINK_HPP
	
#define	INCLUDE_E3LINK_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3look.hpp>

#include <g3et3.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "e3link.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_E3LINK

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "e3link.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Link APIs
//

BOOL DLLAPI Et3Link_Update(CDatabase *pDbase);

BOOL DLLAPI Et3Link_Send(CDatabase *pDbase);

BOOL DLLAPI Et3Link_UsingIP(CDatabase *pDbase);

void DLLAPI Et3Link_GetCalibrate(CDatabase *pDbase);

void DLLAPI Et3Link_SetCalibrate(CDatabase *pDbase);

BOOL DLLAPI Et3Link_CanUpload(void);

BOOL DLLAPI Et3Link_Upload(CDatabase *pDbase, CFilename &File);

BOOL DLLAPI Et3Link_SetOptions(CDatabase *pDbase);

BOOL DLLAPI Et3Link_HasVerify(CDatabase *pDbase);

BOOL DLLAPI Et3Link_Verify(CDatabase *pDbase);

// End of File

#endif
