/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** NOTIFY.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Sends notification events the calling application 
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

LogEventCallbackType* gfnLogEvent;		/* Client application callback function */

EVENT gEvents[MAX_EVENTS];		/* Events array */
INT32 gnEvents;					/* Number of outstanding events */

/*
** Now declare and initialize the translation table from the
** common and user specified status codes.
** Add a final entry to mark the end of the table.
*/

#undef ET_IP_MSG
#define ET_IP_MSG( name, code, string )  { string },

ERROR_STRING gCIPErrorMessages[] =		/* Allocate CIP error description array */
{
	_CIPErrorMessages_   	
};

/*---------------------------------------------------------------------------
** notifyInit( )
**
** Initialize event array
**---------------------------------------------------------------------------
*/

void notifyInit()
{	
	gnEvents = 0;	
} 

/*---------------------------------------------------------------------------
** notifyEvent( )
**
** Notify the client about an event received
**---------------------------------------------------------------------------
*/

void notifyEvent( INT32 nEvent, INT32 nParam )
{
#ifdef TRACE_OUTPUT

#ifdef ET_IP_SCANNER	
	EtIPErrorInfo errorInfo;
#endif

	switch( nEvent )
	{
		case NM_CONNECTION_ESTABLISHED:
			DumpStr1("New connection opened with instance %d", nParam); 
			break;
#ifdef ET_IP_SCANNER		
		case NM_CONN_CONFIG_FAILED_INVALID_NETWORK_PATH:
			DumpStr1("Connection with an instance %d could not be established. Target is offline.", nParam); 
			break;
		case NM_CONN_CONFIG_FAILED_NO_RESPONSE:
			DumpStr1("Connection with an instance %d could not be established. Target is online, but not responding.", nParam); 
			break;
		case NM_CONN_CONFIG_FAILED_ERROR_RESPONSE:
			clientGetConnectionErrorInfo( nParam, &errorInfo );
			DumpStr2("Connection with an instance %d could not be established: %s", nParam, errorInfo.errorDescription); 
			break;
#endif
		case NM_CONNECTION_TIMED_OUT:
			DumpStr1("Connection with instance %d timed out", nParam); 
			break;
		case NM_CONNECTION_CLOSED:
			DumpStr1("Connection with instance %d closed", nParam); 
			break;
		case NM_CONNECTION_NEW_INPUT_DATA:
			DumpStr1("New input data received for connection with instance %d", nParam); 
			break;
		case NM_CONNECTION_NEW_OUTPUT_DATA:
			DumpStr1("New output data received for connection with instance %d", nParam); 
			break;
		case NM_REQUEST_RESPONSE_RECEIVED:
			DumpStr1("Response received for request %d", nParam); 
			break;
		case NM_REQUEST_FAILED_INVALID_NETWORK_PATH:
			DumpStr1("Request %d failed - invalid network path", nParam); 
			break;
		case NM_REQUEST_TIMED_OUT:
			DumpStr1("Request %d failed - no response", nParam); 
			break;
		case NM_CLIENT_OBJECT_REQUEST_RECEIVED:
			DumpStr1("New client request received with Id %d", nParam); 
			break;
		case NM_OUT_OF_MEMORY:
			DumpStr0("Out of memory"); 
			break;
		case NM_UNABLE_INTIALIZE_WINSOCK:
			DumpStr0("Unable to initialize Winsock"); 
			break;
		case NM_UNABLE_START_THREAD:
			DumpStr0("Unable to start thread"); 
			break;
		case NM_ERROR_USING_WINSOCK:
			DumpStr0("Error using Sockets"); 
			break;
		case NM_ERROR_SETTING_SOCKET_TO_NONBLOCKING:
			DumpStr0("Error setting socket to non-blocking"); 
			break;
		case NM_ERROR_SETTING_TIMER:
			DumpStr0("Error setting timer"); 
			break;
		case NM_SESSION_COUNT_LIMIT_REACHED:
			DumpStr0("Session count limit reached"); 
			break;
		case NM_CONNECTION_COUNT_LIMIT_REACHED:
			DumpStr0("Connection count limit reached"); 
			break;
		case NM_PENDING_REQUESTS_LIMIT_REACHED:
			DumpStr0("Pending requests limit reached"); 
			break;
		default:
			break;
	}

#endif /* #ifdef TRACE_OUTPUT */

	if ( gnEvents < MAX_EVENTS )	
	{		
		gEvents[gnEvents].nEvent = nEvent;
		gEvents[gnEvents].nParam = nParam;
		gnEvents++;
	}
}
  
	
/*---------------------------------------------------------------------------
** notifyService( )
**
** Sends all events from the event array
**---------------------------------------------------------------------------
*/

void notifyService()
{
   INT32 i, nEvent, nParam;
	
   if ( gfnLogEvent )
   {
	   while( gnEvents )
	   {
			platformWaitMutex(ghClientMutex, MUTEX_TIMEOUT);
			
			gnEvents--;
			nEvent = gEvents[0].nEvent; 
			nParam = gEvents[0].nParam;
			for( i = 0; i < gnEvents; i++ )		/* Shift the events with the higher index to fill in the void */
				memcpy( &gEvents[i], &gEvents[i+1], sizeof(EVENT) );	
			
			platformReleaseMutex(ghClientMutex);
		   
			DumpStr2("notifyService Event %d, Param %d", nEvent, nParam);
			gfnLogEvent( nEvent, nParam );	   
			DumpStr0("<<");
	   }
   }   
}


#ifdef ET_IP_SCANNER
/*---------------------------------------------------------------------------
** notifyGetCIPErrorInfo( )
**
** Find a CIP error description based on the error codes and place it in 
** EtIPErrorInfo structure
**---------------------------------------------------------------------------
*/
void notifyGetCIPErrorInfo( EtIPErrorInfo* pErrorInfo )
{
	INT32 nErrMsgId;

	nErrMsgId = notifyGetCIPError( pErrorInfo->bGeneralStatus, pErrorInfo->iExtendedStatus );

#if defined ( _UNICODE )		
	wcstombs( (char*)pErrorInfo->errorDescription, gCIPErrorMessages[nErrMsgId].pszError, MAX_ERROR_DESC_SIZE);
#else /* #if defined ( _UNICODE ) */
	strcpy( (char*)pErrorInfo->errorDescription, gCIPErrorMessages[nErrMsgId].pszError);
#endif /* #if defined ( _UNICODE )	*/		
}
#endif

/*---------------------------------------------------------------------------
** notifyGetCIPErrorDescription( )
**
** Find a CIP error description based on the error codes
**---------------------------------------------------------------------------
*/
void notifyGetCIPErrorDescription( UINT8 bGeneralStatus, UINT16 iExtendedStatus, UINT8* szDescription )
{
	INT32 nErrMsgId;

	nErrMsgId = notifyGetCIPError( bGeneralStatus, iExtendedStatus );

	#if defined ( _UNICODE )		
	wcstombs( (char*)szDescription, gCIPErrorMessages[nErrMsgId].pszError, MAX_ERROR_DESC_SIZE);
#else /* #if defined ( _UNICODE )	*/	
	strcpy( (char*)szDescription, gCIPErrorMessages[nErrMsgId].pszError);
#endif /* #if defined ( _UNICODE )	*/			
}


/*---------------------------------------------------------------------------
** notifyGetCIPError( )
**
** Return an error message identifier based on the general and extended
** error codes.
**---------------------------------------------------------------------------
*/
INT32 notifyGetCIPError( UINT8 bGenStatus, UINT16 iExtendedErrorCode )
{
	switch( bGenStatus )
	{
		case ROUTER_ERROR_FAILURE:
			switch( iExtendedErrorCode )
			{
				case ROUTER_EXT_ERR_DUPLICATE_FWD_OPEN:
					return WRN_CONNECTION_IN_USE;					

				case ROUTER_EXT_ERR_CLASS_TRIGGER_INVALID:
					return WRN_TRANSPORT_TRIGGER_COMBO_NOT_SUPPORTED;					

				case ROUTER_EXT_ERR_OWNERSHIP_CONFLICT:
					return WRN_OWNERSHIP_CONFLICT;					

				case ROUTER_EXT_ERR_CONNECTION_NOT_FOUND:
					return WRN_CONNECTION_NOT_FOUND;					

				case ROUTER_EXT_ERR_INVALID_CONN_TYPE:
					return WRN_INVALID_CONNECTION_TYPE;					

				case ROUTER_EXT_ERR_INVALID_CONN_SIZE:
					return WRN_INVALID_CONNECTION_SIZE;					

				case ROUTER_EXT_ERR_DEVICE_NOT_CONFIGURED:
					return WRN_DEVICE_NOT_CONFIGURED;					

				case ROUTER_EXT_ERR_RPI_NOT_SUPPORTED:
					return WRN_RPI_NOT_SUPPORTED;					

				case ROUTER_EXT_ERR_CONNECTION_LIMIT_REACHED:
					return WRN_REACHED_MAX_CONNECTIONS;					

				case ROUTER_EXT_ERR_VENDOR_PRODUCT_CODE_MISMATCH:
					return WRN_NOT_MATCHED_VENDOR_OR_PRODUCT_CODE;					

				case ROUTER_EXT_ERR_PRODUCT_TYPE_MISMATCH:
					return WRN_NOT_MATCHED_PRODUCT_TYPE;					

				case ROUTER_EXT_ERR_REVISION_MISMATCH:
					return WRN_NOT_MATCHED_REVISIONS;					

				case ROUTER_EXT_ERR_INVALID_CONN_POINT:
					return WRN_INVALID_CONNECTION_POINT;					

				case ROUTER_EXT_ERR_INVALID_CONFIG_FORMAT:
					return WRN_INVALID_CONFIGURATION_FORMAT;					

				case ROUTER_EXT_ERR_NO_CONTROLLING_CONNECTION:
					return WRN_NO_CONTROLLING_CONNECTION;					

				case ROUTER_EXT_ERR_TARGET_CONN_LIMIT_REACHED:
					return WRN_TARGET_NO_MORE_CONNECTIONS;					

				case ROUTER_EXT_ERR_RPI_SMALLER_THAN_INHIBIT:
					return WRN_RPI_TOO_SMALL;					

				case ROUTER_EXT_ERR_CONNECTION_TIMED_OUT:
					return WRN_CONNECTION_TIMED_OUT;
					
				case ROUTER_EXT_ERR_UNCONNECTED_SEND_TIMED_OUT:
					return WRN_UNCONNECTED_SEND_TIMED_OUT;
					
				case ROUTER_EXT_ERR_PARAMETER_ERROR:
					return WRN_INVALID_UNCONNECTED_SEND_PARAMETER;
					
				case ROUTER_EXT_ERR_MESSAGE_TOO_LARGE:
					return WRN_MESSAGE_TOO_LARGE;
					
				case ROUTER_EXT_ERR_UNCONN_ACK_WITHOUT_REPLY:
					return WRN_UM_ACKNOWLEDGE_NO_REPLY;
					
				case ROUTER_EXT_ERR_NO_BUFFER_MEMORY_AVAILABLE:
					return WRN_NO_BUFFER_MEMORY;
					
				case ROUTER_EXT_ERR_BANDWIDTH_NOT_AVAILABLE:
					return WRN_NO_NETWORK_BANDWIDTH;
					
				case ROUTER_EXT_ERR_TAG_FILTERS_NOT_AVAILABLE:
					return WRN_NO_TAG_FILTERS;
					
				case ROUTER_EXT_ERR_REAL_TIME_DATA_NOT_CONFIG:
					return WRN_NOT_CONFIGURED_TO_SEND_REAL_TIME_DATA;
					
				case ROUTER_EXT_ERR_PORT_NOT_AVAILABLE:
					return WRN_PORT_NOT_AVAILABLE;
					
				case ROUTER_EXT_ERR_LINK_ADDR_NOT_AVAILABLE:
					return WRN_LINK_ADDR_NOT_AVAILABLE;
					
				case ROUTER_EXT_ERR_INVALID_SEGMENT_TYPE_VALUE:
					return WRN_INVALID_SEGMENT_TAG;
					
				case ROUTER_EXT_ERR_PATH_CONNECTION_MISMATCH:
					return WRN_NOT_MATCHED_PATH;
					
				case ROUTER_EXT_ERR_INVALID_NETWORK_SEGMENT:
					return WRN_SEGMENT_NOT_PRESENT;
					
				case ROUTER_EXT_ERR_INVALID_LINK_ADDRESS:
					return WRN_INVALID_SELF_ADDRESS;
					
				case ROUTER_EXT_ERR_SECOND_RESOURCES_NOT_AVAILABLE:
					return WRN_UNAVAILABLE_SECONDARY_RES;
					
				case ROUTER_EXT_ERR_CONNECTION_ALREADY_ESTABLISHED:
					return WRN_CONNECTION_ALREADY_ESTABLISHED;
					
				case ROUTER_EXT_ERR_DIRECT_CONN_ALREADY_ESTABLISHED:
					return WRN_DIRECT_CONNECTION_ALREADY_ESTABLISHED;
					
				case ROUTER_EXT_ERR_MISC:
					return WRN_MISCELLANEOUS;
				
				case ROUTER_EXT_ERR_REDUNDANT_CONNECTION_MISMATCH:
					return WRN_REDUNDANT_CONNECTION_MISMATCH;
					
				case ROUTER_EXT_ERR_NO_MORE_CONSUMER_RESOURCES:
					return WRN_NO_MORE_CONSUMER_RESOURCES;
					
				case ROUTER_EXT_ERR_NO_TARGET_PATH_RESOURCES:
					return WRN_NO_TARGET_PATH_CONNECTION_RESOURCES;
					
				case ROUTER_EXT_ERR_VENDOR_SPECIFIC:
					return WRN_VENDOR_SPECIFIC;
					
				default:
					return WRN_CONNECTION_FAILURE;					
			}
			break;

		case ROUTER_ERROR_NO_RESOURCE:
			return WRN_NO_MORE_CONNECTION_MANAGER_RESOURCES;
			
		case ROUTER_ERROR_INVALID_PARAMETER_VALUE:
			return WRN_INVALID_PARAMETER_VALUE;
			
		case ROUTER_ERROR_INVALID_SEG_TYPE:
			return WRN_PATH_SEGMENT_ERROR;
			
		case ROUTER_ERROR_INVALID_DESTINATION:
			return WRN_PATH_DESTINATION_UNKNOWN;

		case ROUTER_ERROR_PARTIAL_DATA:
			return WRN_PARTIAL_TRANSFER;
			
		case ROUTER_ERROR_CONN_LOST:
			return WRN_LOST_CONNECTION;
			
		case ROUTER_ERROR_BAD_SERVICE:
			return WRN_SERVICE_NOT_SUPPORTED;
			
		case ROUTER_ERROR_BAD_ATTR_DATA:
			return WRN_INVALID_ATTRIBUTE_VALUE;

		case ROUTER_ERROR_ATTR_LIST_ERROR:
			return WRN_ATTR_LIST_ERROR;

		case ROUTER_ERROR_ALREADY_IN_REQUESTED_MODE:
			return WRN_ALREADY_IN_REQUESTED_MODE;
			
		case ROUTER_ERROR_OBJECT_STATE_CONFLICT:
			return WRN_SERVICE_OBJECT_STATE_CONFLICT;

		case ROUTER_ERROR_OBJ_ALREADY_EXISTS:
			return WRN_OBJ_ALREADY_EXISTS;

		case ROUTER_ERROR_ATTR_NOT_SETTABLE:
			return WRN_ATTR_NOT_SETTABLE;

		case ROUTER_ERROR_PERMISSION_DENIED:
			return WRN_PRIVILEGE_VIOLATION;
			
		case ROUTER_ERROR_DEV_IN_WRONG_STATE:
			return WRN_SERVICE_DEVICE_STATE_CONFLICT;
			
		case ROUTER_ERROR_REPLY_DATA_TOO_LARGE:
			return WRN_RESPONSE_DATA_TOO_LARGE;

		case ROUTER_ERROR_FRAGMENT_PRIMITIVE:
			return WRN_FRAGMENT_PRIMITIVE;
			
		case ROUTER_ERROR_NOT_ENOUGH_DATA:
			return WRN_NOT_ENOUGH_DATA_RECEIVED;
			
		case ROUTER_ERROR_ATTR_NOT_SUPPORTED:
			return WRN_ATTRIBUTE_NOT_SUPPORTED;
			
		case ROUTER_ERROR_TOO_MUCH_DATA:
			return WRN_TOO_MUCH_DATA_RECEIVED;

		case ROUTER_ERROR_OBJ_DOES_NOT_EXIST:
			return WRN_OBJ_DOES_NOT_EXIST;

		case ROUTER_ERROR_NO_FRAGMENTATION:
			return WRN_NO_FRAGMENTATION;

		case ROUTER_ERROR_DATA_NOT_SAVED:
			return WRN_DATA_NOT_SAVED;

		case ROUTER_ERROR_DATA_WRITE_FAILURE:
			return WRN_DATA_STORE_FAILURE;

		case ROUTER_ERROR_REQUEST_TOO_LARGE:
			return WRN_REQUEST_TOO_LARGE;

		case ROUTER_ERROR_RESPONSE_TOO_LARGE:
			return WRN_RESPONSE_TOO_LARGE;

		case ROUTER_ERROR_MISSING_LIST_DATA:
			return WRN_MISSING_LIST_DATA;

		case ROUTER_ERROR_INVALID_LIST_STATUS:
			return WRN_INVALID_LIST_STATUS;

		case ROUTER_ERROR_SERVICE_ERROR:
			return WRN_EMBEDDED_SERVICE_ERROR;

		case ROUTER_ERROR_VENDOR_SPECIFIC:
			return WRN_VENDOR_SPECIFIC;

		case ROUTER_ERROR_INVALID_PARAMETER:
			return WRN_INVALID_PARAMETER;
		
		case ROUTER_ERROR_WRITE_ONCE_FAILURE:
			return WRN_WRITE_ONCE_FAILURE;

		case ROUTER_ERROR_INVALID_REPLY:
			return WRN_INVALID_REPLY;
			
		case ROUTER_ERROR_BAD_KEY_IN_PATH:
			switch( iExtendedErrorCode )
			{
				case ROUTER_EXT_ERR_VENDOR_PRODUCT_CODE_MISMATCH:
					return WRN_NOT_MATCHED_KEYSEG_VENDOR_PRODUCT_CODE;
					
				case ROUTER_EXT_ERR_PRODUCT_TYPE_MISMATCH:
					return WRN_NOT_MATCHED_KEYSEG_PRODUCT_TYPE;
					
				case ROUTER_EXT_ERR_REVISION_MISMATCH:
					return WRN_NOT_MATCHED_KEYSEG_REVISIONS;
					
				default:
					return WRN_KEY_FAILURE_IN_PATH;					
			}
			break;		

		case ROUTER_ERROR_BAD_PATH_SIZE:
			return WRN_INVALID_PATH_SIZE;
			break;

		case ROUTER_ERROR_UNEXPECTED_ATTR:
			return WRN_UNEXPECTED_ATTR;

		case ROUTER_ERROR_INVALID_MEMBER:
			return WRN_INVALID_MEMBER;

		case ROUTER_ERROR_MEMBER_NOT_SETTABLE:
			return WRN_MEMBER_NOT_SETTABLE;

		case ROUTER_ERROR_ENCAP_PROTOCOL:
			return WRN_ENCAP_ERROR;
				
		default:
			break;
	}

	return WRN_VENDOR_SPECIFIC;
}





#define ROUTER_ERROR_STILL_PROCESSING     0xFF  /* Special marker to indicate    */
												/* we haven't finished processing */
												/* the request yet               */
