
#include "intern.hpp"

#include "bmikels.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// LaserSpeed Structures
//

struct LaserSpeedItem {

	UINT  m_Number;
	PCSTR m_Name;
	UINT  m_Type;
	BOOL  m_fRO;
	BOOL  m_fString;
	};

struct LaserSpeedGroup {

	UINT		m_Number;
	PCSTR		m_Group;
	LaserSpeedItem	m_Item[64];
	};

//////////////////////////////////////////////////////////////////////////
//
// LaserSpeed Data List
//

static LaserSpeedGroup const m_pList[] = {

	6,	"Real Time Data",
		{
		{	321,	"Length",					addrLongAsLong, 1, 0,	},
		{	322,	"Velocity",					addrLongAsLong, 1, 0	},
		{	323,	"Quality Factor",				addrByteAsByte, 1, 0,	},
		{	324,	"Status",					addrByteAsByte,	1, 0,	},
		{	  0,	"",						addrLongAsLong, 0, 0,	},
		},

	1,	"Standard",
		{
		{	1,	"Material Present Input Mode",			addrByteAsByte, 0, 0,	},
		{	2,	"Material Present Dropout Time",		addrLongAsLong, 0, 0,	},
		{	3,	"Material Present Validation Time",		addrLongAsLong, 0, 0,	},
		{	4,	"Material Present Threshold",			addrLongAsLong, 0, 0,	},
		{	5,	"Walking Threshold",				addrLongAsLong, 0, 0,	},
		{	6,	"Minimum Final Length",				addrLongAsLong, 0, 0,	},
		{	7,	"Index Pulse Length",				addrLongAsLong, 0, 0,	},
		{	8,	"Index Pulse Every",				addrLongAsLong, 0, 0,	},
		{	9,	"High Speed Pulse Rate",			addrLongAsLong, 0, 0,	},
		{      10,	"Low Speed Pulse Rate",				addrLongAsLong, 0, 0,	},
		{      11,	"Measurement Units",				addrByteAsByte, 0, 0,	},
		{	0,	"",						addrLongAsLong, 0, 0,	},
		},

	2,	"Advanced",
		{
		{      65,	"Direction Inversion Switch",			addrByteAsByte, 0, 0,	},
		{      66,	"Hold Velocity If Above",			addrLongAsLong, 0, 0,	},
		{      67,	"Velocity Hold Timeout",			addrLongAsLong, 0, 0,	},
		{      68,	"Calibration Trim",				addrLongAsLong, 0, 0,	},
		{      69,	"User Update Rate",				addrLongAsLong, 0, 0,	},
		{      70,	"Length Reset Value",				addrLongAsLong, 0, 0,	},
		{      71,	"Minumum Velocity Limit",			addrLongAsLong, 0, 0,	},
		{      72,	"Maximum Velocity Limit",			addrLongAsLong, 0, 0,	},
		{      73,	"Length Reset Input Action",			addrByteAsByte, 0, 0,	},
		{      74,	"QF Warning Threshold",				addrLongAsLong, 0, 0,	},
		{      75,	"QF Warning Timeout",				addrLongAsLong, 0, 0,	},
		{      76,	"Velocity At Loss Of Material",			addrByteAsByte, 0, 0,	},
		{      77,	"Comm LED Control",				addrByteAsByte, 0, 0,	},
		{      78,	"Averaging Time",				addrWordAsWord, 0, 0,	},
		{      79,	"Setting Lock",					addrBitAsBit,	0, 0,	},
		{      80,	"Setting Lock Enabled",				addrBitAsBit,	0, 0,	},
		{      81,	"Reset Internal Length Count",			addrByteAsByte, 0, 0,	},
		{      82,	"High Speed Pulse Count At Last Reset",		addrLongAsLong, 1, 0,   },
		{      83,	"Low Speed Pulse Count At Last Reset",		addrLongAsLong, 1, 0,	},
		{      84,	"Delta Length Calculation Mode",		addrByteAsByte, 0, 0,   },
		{      85,	"Signal Threshold Settings",			addrByteAsByte, 0, 0,   },
		{      86,	"Shift Frequency",				addrByteAsByte, 0, 0,   },
		{      87,	"Reversal Zone Hits",				addrByteAsByte, 0, 0,	},
		{      88,	"Arbitrary Command Out",			addrLongAsLong, 0, 1,   },
		{      89,	"Arbitrary Result In",				addrLongAsLong, 1, 1,	},	
		{	0,	"",						addrLongAsLong, 0, 0,	},
		},

	3,	"Outputs",
		{
		{     129,	"Analog Full Scale",				addrLongAsLong, 0, 0,	},
		{     130,	"Auxilary Digital Output Func",			addrByteAsByte, 0, 0,	},
		{     131,	"High Speed Pulse Config",			addrByteAsByte, 0, 0,	},
		{     132,	"Low Speed Pulse Config",			addrByteAsByte, 0, 0,	},
		{     133,	"Length Threshold A",				addrLongAsLong, 0, 0,	},
		{     134,	"Length Threshold B",				addrLongAsLong, 0, 0,	},
		{     135,	"Analog Zero Scale",				addrLongAsLong,	0, 0,   },
		{	0,	"",						addrLongAsLong, 0, 0,	},
		},

	4,	"Gauge Info",
		{
		{     193,	"Gauge Serial Number",				addrLongAsLong, 1, 1,	},
		{     194,	"Firmware Version String",			addrLongAsLong, 1, 1,	},
		{     195,	"Hour Meter Current Value",			addrLongAsLong, 1, 0,	},
		{     196,	"Current Temp",					addrLongAsLong, 1, 0,	},
		{     197,	"Max Temp",					addrLongAsLong, 1, 0,	},
		{     198,	"Micro Software Version",			addrLongAsLong, 1, 1,   },
		{	0,	"",						addrLongAsLong, 0, 0,	},
		},

	5,	"Communication",
		{
		{     257,	"RS232 Baud Rate Settings",			addrByteAsByte, 0, 0,	},
		{     258,	"RS232 Power On Mode",				addrLongAsLong, 0, 1,	},
		{     259,	"RS422 Baud Rate Settings",			addrByteAsByte, 0, 0,	},
		{     260,	"RS422 Power On Mode",				addrLongAsLong, 0, 1,	},
		{     261,	"Ethernet Enabled",				addrBitAsBit,	1, 0,	},
		{     262,	"Ethernet Link Status",				addrByteAsByte, 1, 0,	},	
		{     263,	"Ethernet Hardware ID",				addrLongAsLong, 1, 1,	},
		{     264,	"DHCP Enabled",					addrBitAsBit,	0, 0,	},
		{     265,	"Host Name",					addrLongAsLong, 0, 1,	},
		{     266,	"IP Address",					addrLongAsLong, 0, 1,	},
		{     267,	"IP Default Gateway",				addrLongAsLong, 0, 1,	},
		{     268,	"IP Net Mask",					addrLongAsLong, 0, 1,	},
		{     269,	"UDP Data Port",				addrWordAsWord,	0, 0,	},
		{     270,	"UDP Power On Dest IP Address",			addrLongAsLong, 0, 1,	},
		{     271,	"UDP Power On Dest IP Port",			addrWordAsWord, 0, 0,	},
		{     272,	"UDP Power On Mode",				addrLongAsLong, 0, 1,	},
		{	0,	"",						addrLongAsLong, 0, 0,	},
		},
	};

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBMikeLSMasterDeviceOptions, CUIItem);       

// Constructor

CBMikeLSMasterDeviceOptions::CBMikeLSMasterDeviceOptions(void)
{
	m_Scan	  = 5000;

	m_Timeout = 1000;

	m_Auto	  = 0;
	}

// Download Support

BOOL CBMikeLSMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Scan));

	Init.AddWord(WORD(m_Timeout));

	Init.AddByte(BYTE(m_Auto));
		
	return TRUE;
	}

// Meta Data Creation

void CBMikeLSMasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Scan);

	Meta_AddInteger(Timeout);	

	Meta_AddInteger(Auto);
	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Master Driver
//

// Instantiator

ICommsDriver * Create_BMikeLSMasterDriver(void)
{
	return New CBMikeLSMasterDriver;
	}

// Constructor

CBMikeLSMasterDriver::CBMikeLSMasterDriver(void)
{
	m_wID		= 0x409B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "BETA LaserMike";
	
	m_DriverName	= "LaserSpeed";
	
	m_Version	= "1.11";
	
	m_ShortName	= "LS";

	m_fSingle	= TRUE;

	AddSpaces();
	}

// Destructor

CBMikeLSMasterDriver::~CBMikeLSMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CBMikeLSMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CBMikeLSMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CBMikeLSMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBMikeLSMasterDeviceOptions);
	}

// Address Management

BOOL CBMikeLSMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CBMikeLSAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CBMikeLSMasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{	
	if( Addr.m_Ref ) {

		UINT uMatch = GetItem(Addr);

		for( UINT g = 0; g < elements(m_pList); g++ ) {

			for( UINT i = 0; i < elements(m_pList[g].m_Item); i++ ) {

				if( uMatch == m_pList[g].m_Item[i].m_Number ) {

					return m_pList[g].m_Item[i].m_fRO;
					}
				}
			}
		}

	return FALSE;
	}

// Address Helpers

BOOL CBMikeLSMasterDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		CString Group = Text.Mid(0, uFind);

		CString Item  = Text.Mid(uFind + 1);

		for( UINT g = 0; g < elements(m_pList); g++ ) {

			CString Name = m_pList[g].m_Group;

			if( Group != Name ) {

				Name.Remove(' ');
				}

			if( Group == Name ) {

				for( UINT i = 0; i < elements(m_pList[g].m_Item); i++ ) {

					Name = m_pList[g].m_Item[i].m_Name;

					if( Name.IsEmpty() ) {

						break;
						}

					BOOL fFound = Item == Name;

					if( !fFound ) {

						Name.Remove(' ');

						fFound = Item == Name;
						}

					if( fFound ) {

						if( m_pList[g].m_Item[i].m_fString ) {

							Addr.a.m_Table  = m_pList[g].m_Number;

							Addr.a.m_Offset = ((m_pList[g].m_Item[i].m_Number % 64) * 16);

							Addr.a.m_Type   = addrLongAsLong;

							return TRUE;
							}

						Addr.a.m_Offset = m_pList[g].m_Item[i].m_Number;

						Addr.a.m_Type   = m_pList[g].m_Item[i].m_Type;

						Addr.a.m_Table  = addrNamed;

						return TRUE;
						}
					}
				}
			}
		}

	Error.Set("Invalid Parameter", 0);					

	return FALSE;
	}

BOOL CBMikeLSMasterDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		UINT uMatch = GetItem(Addr);

		for( UINT g = 0; g < elements(m_pList); g++ ) {

			for( UINT i = 0; i < elements(m_pList[g].m_Item); i++ ) {

				if( uMatch == m_pList[g].m_Item[i].m_Number ) {

					CString Group = m_pList[g].m_Group;

					Group.Remove(' ');

					CString Item = m_pList[g].m_Item[i].m_Name;

					Item.Remove(' ');

					Text.Printf("%s:%s", Group, Item);

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

// Data Access

UINT CBMikeLSMasterDriver::GetGroup(CAddress const &Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset / 64 + 1;
		}

	return Addr.a.m_Table;
	}

UINT CBMikeLSMasterDriver::GetItem(CAddress const &Addr)
{
	if( Addr.a.m_Table == addrNamed ) {

		return Addr.a.m_Offset;
		}

	return (Addr.a.m_Table - 1) * 64 + Addr.a.m_Offset / 16;
	}

UINT CBMikeLSMasterDriver::GetGroupIndex(CAddress const &Addr)
{
	UINT uGroup = GetGroup(Addr);

	for( UINT u = 0; u < elements(m_pList); u++ ) {

		if( uGroup == m_pList[u].m_Number ) {

			return u;
			}
		}

	return 0;
	}

UINT CBMikeLSMasterDriver::GetItemIndex(CAddress const &Addr)
{
	return GetItem(Addr) % 64 - 1;
	}

// Implementation

void CBMikeLSMasterDriver::AddSpaces(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Ethernet Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBMikeLSEthernetMasterDeviceOptions, CBMikeLSMasterDeviceOptions);       

// Constructor

CBMikeLSEthernetMasterDeviceOptions::CBMikeLSEthernetMasterDeviceOptions(void)
{
	m_IP		= DWORD(MAKELONG(MAKEWORD( 2, 15), MAKEWORD( 168, 192)));

	m_Port		= 1003;
	}

// Download Support

BOOL CBMikeLSEthernetMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CBMikeLSMasterDeviceOptions::MakeInitData(Init);

	Init.AddLong(LONG(m_IP));
	Init.AddWord(WORD(m_Port));
			
	return TRUE;
	}

// Meta Data Creation

void CBMikeLSEthernetMasterDeviceOptions::AddMetaData(void)
{
	CBMikeLSMasterDeviceOptions::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
		
	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Ethernet Master Driver
//

// Instantiator

ICommsDriver * Create_BMikeLSEthernetMasterDriver(void)
{
	return New CBMikeLSEthernetMasterDriver;
	}

// Constructor

CBMikeLSEthernetMasterDriver::CBMikeLSEthernetMasterDriver(void)
{
	m_wID		= 0x409C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Beta LaserMike";
	
	m_DriverName	= "LaserSpeed UDP";
	
	m_Version	= "1.11";
	
	m_ShortName	= "LS UDP";

	m_fSingle	= FALSE;
	}

// Destructor

CBMikeLSEthernetMasterDriver::~CBMikeLSEthernetMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CBMikeLSEthernetMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBMikeLSEthernetMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CBMikeLSEthernetMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBMikeLSEthernetMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CBMikeLSAddrDialog, CStdDialog);
		
// Constructor

CBMikeLSAddrDialog::CBMikeLSAddrDialog(CBMikeLSMasterDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;
		
	SetName(TEXT("LaserSpeedDlg"));
	}

// Message Map

AfxMessageMap(CBMikeLSAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(200, CBN_SELCHANGE, OnSelChange)
	AfxDispatchNotify(201, CBN_SELCHANGE, OnSelChange)
	
	AfxMessageEnd(CBMikeLSAddrDialog)
	};

// Message Handlers

BOOL CBMikeLSAddrDialog::OnInitDialog(CWnd &Wnd, DWORD dwData)
{
	LoadGroup();

	OnSelChange(ID_G, Wnd);

	return FALSE;
	}

// Notification Handlers

void CBMikeLSAddrDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == ID_G ) {

		LoadItem();

		SetType();
		}

	if( uID == ID_I ) {

		SetType();
		}
	}

// Command Handlers

BOOL CBMikeLSAddrDialog::OnOkay(UINT uID)
{
	if( CheckInit() ) {

		CString Group = GetDlgItem(ID_G).GetWindowTextW();

		CString Item  = GetDlgItem(ID_I).GetWindowTextW();

		CError Error(TRUE);

		CAddress Addr;

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Group + ":" + Item ) ) {

			*m_pAddr = Addr;
			}
		}

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CBMikeLSAddrDialog::LoadGroup(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_G);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	CStringArray Array;

	UINT uSel = 0;

	if( m_pAddr->m_Ref ) {

		uSel = m_pDriver->GetGroupIndex(*m_pAddr) + 1;
		}
		
	Array.Append(TEXT("No Selection"));

	for( UINT u = 0; u < elements(m_pList); u++ ) {

		CString Name = m_pList[u].m_Group;

		if( !Name.IsEmpty() ) {

			Array.Append(Name);
			}
		}
	
	Box.AddStrings(Array);

	Box.SetRedraw (TRUE);

	Box.Invalidate(TRUE);
		
	Box.SetCurSel(uSel);

	GetDlgItem(ID_I).EnableWindow(uSel ? TRUE : FALSE);
	}

void CBMikeLSAddrDialog::LoadItem(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_I);

	if( CheckInit() ) {

		Box.SetRedraw(FALSE);

		Box.ResetContent();

		CStringArray Array;

		UINT uSel = 0;

		if( m_pAddr->m_Ref ) {

			uSel = m_pDriver->GetItemIndex(*m_pAddr);
			}
		
		CComboBox &Group = (CComboBox &) GetDlgItem(ID_G);

		UINT uIndex = Group.GetCurSel() - 1;

		if( uIndex < NOTHING ) {

			for( UINT u = 0; u < elements(m_pList[uIndex].m_Item); u++ ) {

				CString Name = m_pList[uIndex].m_Item[u].m_Name;

				if( !Name.IsEmpty() ) {

					Array.Append(Name);

					continue;
					}

				break;
				}
			}
	
		Box.AddStrings(Array);

		Box.SetRedraw (TRUE);

		Box.Invalidate(TRUE);

		Box.SetCurSel(uSel);

		GetDlgItem(ID_I).EnableWindow(TRUE);

		return;
		}

	Box.SetCurSel(NOTHING);
	}

void CBMikeLSAddrDialog::SetType(void)
{
	if( CheckInit() ) {

		CComboBox &Group = (CComboBox &) GetDlgItem(ID_G);

		CComboBox &Item  = (CComboBox &) GetDlgItem(ID_I);

		UINT uGroup	 = Group.GetCurSel() - 1;

		UINT uItem	 = Item.GetCurSel();

		UINT uType	 = m_pList[uGroup].m_Item[uItem].m_Type;

		BOOL fString     = m_pList[uGroup].m_Item[uItem].m_fString;

		GetDlgItem(301).SetWindowTextW(fString ? "STRING" : m_pDriver->GetSpace("")->GetTypeModifier(uType));
		}
	}

BOOL CBMikeLSAddrDialog::CheckInit(void)
{
	CComboBox & Box = (CComboBox &) GetDlgItem(ID_G);

	if( Box.GetCurSel() ) {

		return TRUE;
		}

	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBMikeLSSlaveDriverOptions, CUIItem);       

// Constructor

CBMikeLSSlaveDriverOptions::CBMikeLSSlaveDriverOptions(void)
{
	
	}

// Download Support

BOOL CBMikeLSSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBMikeLSSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Slave UDP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBMikeLSSlaveUdpDriverOptions, CBMikeLSEthernetMasterDeviceOptions);       

// Constructor

CBMikeLSSlaveUdpDriverOptions::CBMikeLSSlaveUdpDriverOptions(void)
{
	}

// Download Support

BOOL CBMikeLSSlaveUdpDriverOptions::MakeInitData(CInitData &Init)
{
	CBMikeLSEthernetMasterDeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CBMikeLSSlaveUdpDriverOptions::AddMetaData(void)
{
	CBMikeLSEthernetMasterDeviceOptions::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Final Length Mode Slave Driver
//

// Instantiator

ICommsDriver * Create_BMikeLSFinalLenSlaveDriver(void)
{
	return New CBMikeLSFinalLenSlaveDriver;
	}

// Constructor

CBMikeLSFinalLenSlaveDriver::CBMikeLSFinalLenSlaveDriver(void)
{
	m_wID		= 0x40A0;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "BETA LaserMike";
	
	m_DriverName	= "LaserSpeed Final Length Mode Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "LS TF";

	AddSpaces();
	}

// Destructor

CBMikeLSFinalLenSlaveDriver::~CBMikeLSFinalLenSlaveDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CBMikeLSFinalLenSlaveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CBMikeLSFinalLenSlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CBMikeLSFinalLenSlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBMikeLSSlaveDriverOptions);
	}

CLASS CBMikeLSFinalLenSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Address Management

BOOL CBMikeLSFinalLenSlaveDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{	
	return TRUE;
	}

// Implementation

void CBMikeLSFinalLenSlaveDriver::AddSpaces(void)
{
	AddSpace(New CSpace(addrNamed,	"FL", "Final Length",	10, 2, 0, addrRealAsReal));
	AddSpace(New CSpace(addrNamed,	"QF", "Quality Factor",	10, 3, 0, addrByteAsByte));
	}

//////////////////////////////////////////////////////////////////////////
//
// Beta LaserMike LaserSpeed Final Length Mode UDP Slave Driver
//

// Instantiator

ICommsDriver * Create_BMikeLSFinalLenSlaveUdpDriver(void)
{
	return New CBMikeLSFinalLenSlaveUdpDriver;
	}

// Constructor

CBMikeLSFinalLenSlaveUdpDriver::CBMikeLSFinalLenSlaveUdpDriver(void)
{
	m_wID		= 0x40A1;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "BETA LaserMike";
	
	m_DriverName	= "LaserSpeed UDP Final Length Mode Slave";
	
	m_Version	= "1.10";
	
	m_ShortName	= "LS TF";
	}

// Binding Control

UINT CBMikeLSFinalLenSlaveUdpDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBMikeLSFinalLenSlaveUdpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

// Configuration

CLASS CBMikeLSFinalLenSlaveUdpDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBMikeLSSlaveUdpDriverOptions);
	}

CLASS CBMikeLSFinalLenSlaveUdpDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// End of File