
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SIMOVERT_HPP
	
#define	INCLUDE_SIMOVERT_HPP

// Model Types

enum {
	MODELVERT = 1,
	MODELREG  = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert P via USS Device Options
//

class CSimovertSerialDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSimovertSerialDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Drop;
		UINT m_PZD;
		UINT m_PKW;
		UINT m_ModelUSS;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert P via USS Master Driver
//

class CSimovertSerialDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSimovertSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration	
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
				
	protected:
		// Data
		UINT m_uPZD;

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Siemens Simovert P via USS Address Selection Dialog
//

class CSimovertAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSimovertAddrDialog(CSimovertSerialDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message and Notification Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Helpers
		void	UpdateMax(void);
	};

// End of File

#endif
