
#include "Intern.hpp"

#include "MatrixClientSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Matrix Client Socket
//

// Constructor

CMatrixClientSocket::CMatrixClientSocket(CMatrixSsl *pSsl, ISocket *pSock, sslKeys_t *pKeys, PCTXT pName, UINT uCheck) : CMatrixSocket(pSsl, pSock, pKeys)
{
	m_uCheck = uCheck;

	if( m_uCheck >= tlsCheckName ) {

		if( pName && *pName ) {

			m_Name = pName;
		}
	}

	InitSid();

	InitHello();
}

// Destructor

CMatrixClientSocket::~CMatrixClientSocket(void)
{
	KillHello();

	KillSid();
}

// ISocket Methods

HRESULT CMatrixClientSocket::Connect(IPADDR const &IP, WORD Rem)
{
	if( UseExisting() ) {

		return S_OK;
	}

	if( m_pSock->Connect(IP, Rem) == S_OK ) {

		m_uState = stateOpening;

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CMatrixClientSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	if( UseExisting() ) {

		return S_OK;
	}

	if( m_pSock->Connect(IP, Rem, Loc) == S_OK ) {

		m_uState = stateOpening;

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CMatrixClientSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	if( UseExisting() ) {

		return S_OK;
	}

	if( m_pSock->Connect(IP, IF, Rem, Loc) == S_OK ) {

		m_uState = stateOpening;

		return S_OK;
	}

	return E_FAIL;
}

// Overridables

void CMatrixClientSocket::InitSession(void)
{
	matrixSslNewClientSession(&m_pSession,
				  m_pKeys,
				  m_pSid,
				  NULL,
				  0,
				  HookCheckCert,
				  m_Name.IsEmpty() ? NULL : PCTXT(m_Name),
				  m_Name.IsEmpty() ? NULL : m_pHello,
				  NULL,
				  m_pOptions
	);

	PumpSendData();
}

// Implementation

void CMatrixClientSocket::InitSid(void)
{
	matrixSslNewSessionId(&m_pSid, NULL);
}

void CMatrixClientSocket::InitHello(void)
{
	if( !m_Name.IsEmpty() ) {

		matrixSslNewHelloExtension(&m_pHello, NULL);

		unsigned char *pServer;

		int32	       nServer;

		matrixSslCreateSNIext(NULL, PBYTE(PCTXT(m_Name)), m_Name.GetLength(), &pServer, &nServer);

		matrixSslLoadHelloExtension(m_pHello, pServer, nServer, EXT_SNI);

		psFree(pServer, NULL);

		return;
	}

	m_pHello = NULL;
}

void CMatrixClientSocket::KillSid(void)
{
	if( m_pSid ) {

		matrixSslDeleteSessionId(m_pSid);
	}
}

void CMatrixClientSocket::KillHello(void)
{
	if( m_pHello ) {

		matrixSslDeleteHelloExtension(m_pHello);
	}
}

// Callbacks

int32 CMatrixClientSocket::CheckCert(psX509Cert_t *cert, int32 alert)
{
	AfxTrace("Cert is %s with alert of %u\n", cert->subject.commonName, alert);

	if( alert ) {

		if( m_uCheck == tlsCheckNone ) {

			AfxTrace("Cert passed as errors ignored\n");

			return 0;
		}

		if( alert == SSL_ALERT_CERTIFICATE_EXPIRED ) {

			if( m_uCheck < tlsCheckDate ) {

				return 0;
			}

			AfxTrace("Cert failed for expiration\n");
		}
		else {
			switch( alert ) {

				case SSL_ALERT_UNKNOWN_CA:

					AfxTrace("Cert failed for unknown CA\n");

					break;

				case SSL_ALERT_CERTIFICATE_UNKNOWN:

					AfxTrace("Cert failed for mismatched name\n");

					break;

				default:

					AfxTrace("Cert failed\n");
			}
		}

		m_pSock->Abort();

		return -1;
	}

	return 0;
}

// Callback Hooks

int32 CMatrixClientSocket::HookCheckCert(ssl_t *ssl, psX509Cert_t *cert, int32 alert)
{
	return ((CMatrixClientSocket *) ssl->userPtr)->CheckCert(cert, alert);
}

// End of File
