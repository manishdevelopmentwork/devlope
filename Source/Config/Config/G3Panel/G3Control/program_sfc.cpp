
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Program Manager
//

// Dynamic Class

AfxImplementDynamicClass(CGroupChildPrograms, CGroupPrograms);

// Constructor

CGroupChildPrograms::CGroupChildPrograms(void)
{
	}

// Overridables

CString CGroupChildPrograms::GetTreeLabel(void) const
{
	return CString(IDS_CHILD_SFC);
	}

UINT CGroupChildPrograms::GetTreeImage(void) const
{
	return IDI_FOLDER;
	}

// Meta Data Creation

void CGroupChildPrograms::AddMetaData(void)
{
	CGroupObjects::AddMetaData();

	Meta_AddString(Name);

	Meta_SetName((IDS_CONTROL_CHILDREN));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - Sfc Main Program
//

// Dynamic Class

AfxImplementDynamicClass(CControlSfcProgram, CControlProgram);

// Constructor

CControlSfcProgram::CControlSfcProgram(void)
{
	m_Enable   = 0;

	m_pChilds  = New CGroupChildPrograms;
	}

// Attributes

BOOL CControlSfcProgram::HasChildren(void)
{
	return m_pChilds->m_pObjects->GetItemCount() > 0;
	}

// Overridables

UINT CControlSfcProgram::GetTreeImage(void) const
{
	return m_Enable > 0 ? IDI_PROGRAM_SFC_ENABLED : IDI_PROGRAM_SFC_DISABLED;
	}

void CControlSfcProgram::Validate(void)
{
	CControlProgram::Validate();

	m_pChilds->Validate();
	}

// Item Lookup

BOOL CControlSfcProgram::FindVariable(CControlVariable * &pVariable, CString Name)
{
	return m_pLocals->FindVariable(pVariable, Name) ||
	       m_pChilds->FindVariable(pVariable, Name) ;;
	}

BOOL CControlSfcProgram::FindVarGroup(CGroupVariables * &pGroup, CString Name)
{
	if( m_pLocals->m_Name == Name ) {
		
		pGroup = m_pLocals;

		return TRUE;
		}

	return m_pChilds->FindVarGroup(pGroup, Name);
	}

// UI Update

void CControlSfcProgram::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		}

	if( Tag == L"Enable" ) {

		pHost->SendUpdate(updateProps);		
		}

	CControlProgram::OnUIChange(pHost, pItem, Tag);
	}

// Meta Data Creation

void CControlSfcProgram::AddMetaData(void)
{
	CControlProgram::AddMetaData();

	Meta_AddInteger(Enable);
	Meta_AddCollect(Childs);

	Meta_SetName((IDS_CONTROL_PROGRAM));
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - Sfc Child Program
//

// Dynamic Class

AfxImplementDynamicClass(CControlSfcMainProgram, CControlSfcProgram);

// Constructor

CControlSfcMainProgram::CControlSfcMainProgram(void)
{
	m_Enable = 1;
	}

// Development

void CControlSfcMainProgram::AddTest(void)
{
	m_pChilds->AddTest();

	m_pLocals->AddTest();
	}

// UI Update

void CControlSfcMainProgram::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CControlSfcProgram::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CControlSfcMainProgram::Init(void)
{
	CControlSfcProgram::Init();

	DWORD dwProject = m_pProject->GetHandle();

	DWORD dwProgram = m_pProgram->GetHandle();

	CStratonProperties Props(dwProject, dwProgram);

	Props.Set(propProgCheck, L"#TRUE#");
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Program - Sfc Child Program
//

// Dynamic Class

AfxImplementDynamicClass(CControlSfcChildProgram, CControlSfcProgram);

// Constructor

CControlSfcChildProgram::CControlSfcChildProgram(void)
{
	m_Enable = 3;
	}

// Development

void CControlSfcChildProgram::AddTest(void)
{
	//m_pChilds->AddTest();

	m_pLocals->AddTest();
	}

// UI Update

void CControlSfcChildProgram::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CControlSfcProgram::OnUIChange(pHost, pItem, Tag);
	}

// End of File

