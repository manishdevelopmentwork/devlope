
#include "Intern.hpp"

#include "TimeZone.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Time Zone Storage
//

// Instantiators

IDevice * Create_TimeZone(void)
{
	return New CTimeZone;
}

// Constructor

CTimeZone::CTimeZone(void)
{
	StdSetRef();
}

// Destructor

CTimeZone::~CTimeZone(void)
{
}

// IUnknown

HRESULT CTimeZone::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ITimeZone);

	StdQueryInterface(ITimeZone);

	return E_NOINTERFACE;
}

ULONG CTimeZone::AddRef(void)
{
	StdAddRef();
}

ULONG CTimeZone::Release(void)
{
	StdRelease();
}

// IDevice

BOOL CTimeZone::CTimeZone::Open(void)
{
	mkdir("/vap/opt/crimson/tz", 0755);

	CAutoFile File("/vap/opt/crimson/tz/timezone", "r");

	if( File ) {

		CStringArray List;

		File.GetLine().Tokenize(List, ',');

		if( List.GetCount() == 4 ) {

			m_Data.m_magic  = strtoul(List[0], NULL, 16);

			m_Data.m_offset = strtol(List[1], NULL, 10);
			
			m_Data.m_dst    = strtol(List[2], NULL, 10) ? true : false;
			
			m_Data.m_next   = strtol(List[3], NULL, 10);
		}
	}

	return TRUE;
}

// ITimeZone

BOOL CTimeZone::GetDst(void)
{
	return m_Data.m_dst;
}

INT CTimeZone::GetOffset(void)
{
	return m_Data.m_offset;
}

time_t CTimeZone::GetNext(void)
{
	return m_Data.m_next;
}

BOOL CTimeZone::Lock(DWORD magic)
{
	if( magic ) {

		if( magic == NOTHING ) {

			m_Data.m_magic = 0;

			m_Data.m_next  = 0;

			return FALSE;
		}

		if( magic >= m_Data.m_magic ) {

			m_Pend         = m_Data;

			m_Pend.m_magic = magic;

			return TRUE;
		}
	}
	else {
		if( memcmp(&m_Data, &m_Pend, sizeof(m_Data)) ) {

			m_Data = m_Pend;

			CAutoFile File("/vap/opt/crimson/tz/timezone.tmp", "w");

			if( File ) {

				File.PutLine(CPrintf("%8.8X,%d,%u,%u",
						     m_Data.m_magic,
						     m_Data.m_offset,
						     m_Data.m_dst ? 1 : 0,
						     m_Data.m_next
				));

				File.Close();

				_unlink("/vap/opt/crimson/tz/timezone");

				_rename("/vap/opt/crimson/tz/timezone.tmp", "/vap/opt/crimson/tz/timezone");

				_sync();
			}
		}

		return TRUE;
	}

	return FALSE;
}

void CTimeZone::SetDst(BOOL dst)
{
	m_Pend.m_dst = dst;
}

void CTimeZone::SetOffset(INT offset)
{
	m_Pend.m_offset = offset;
}

void CTimeZone::SetNext(time_t next)
{
	m_Pend.m_next = next;
}

// End of File
