
#include "intern.hpp"

#include "imoloadg.hpp"

#define IDEBUG FALSE

//////////////////////////////////////////////////////////////////////////
//
// IMOKGEN Driver
//

// Instantiator

INSTANTIATE(CIMOGLoaderPortDriver);

// Constructor

CIMOGLoaderPortDriver::CIMOGLoaderPortDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex;

	m_ErrorCode	= 0;
	}

// Destructor

CIMOGLoaderPortDriver::~CIMOGLoaderPortDriver(void)
{
	}

// Configuration

void MCALL CIMOGLoaderPortDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CIMOGLoaderPortDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CIMOGLoaderPortDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CIMOGLoaderPortDriver::Open(void)
{	
	}

// Device

CCODE MCALL CIMOGLoaderPortDriver::DeviceOpen(IDevice *pDevice)
{
	return CMasterDriver::DeviceOpen(pDevice); 
	}

CCODE MCALL CIMOGLoaderPortDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CIMOGLoaderPortDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = OPMB;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrByteAsByte;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CIMOGLoaderPortDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == ERRCODE ) {
		*pData = m_ErrorCode;
		return 1;
		}

	UINT uReturnCount;

	uReturnCount = GetMaxCount( Addr, uCount );

	ConfigureRead( Addr, uReturnCount );
			
	if( Transact() ) {

		return GetResponse( pData, Addr, uReturnCount );
		}

	m_ErrorCode = (DWORD(Addr.a.m_Offset) << 16) + 1;

	return CCODE_ERROR;
	}

CCODE MCALL CIMOGLoaderPortDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{

	if( Addr.a.m_Table == ERRCODE ) {

		m_ErrorCode = *pData;

		return 1;
		}

	UINT uReturnCount;

	uReturnCount = GetMaxCount( Addr, uCount );

	ConfigureWrite( Addr, uReturnCount, pData );

	AddWriteData( Addr, pData, uReturnCount );

	if( Transact() ) {

		return uReturnCount;
		}

	return 1;
	}

// Frame Building

void CIMOGLoaderPortDriver::ConfigureRead( AREF Addr, UINT uCount )
{
	StartFrame( 'r', GetAddrCode(Addr.a.m_Table) );

	PutAddress( Addr );

	PutCount( Addr, uCount );
 	}

void CIMOGLoaderPortDriver::ConfigureWrite( AREF Addr, UINT uCount, PDWORD pData )
{
	switch ( Addr.a.m_Type ) {

		case addrBitAsBit:

			StartFrame( pData[0] ? 'o' : 'n', GetAddrCode( Addr.a.m_Table ) );

			PutAddress( Addr );

			AddData( 0, 0x10 ); // dummy byte

			break;

		case addrByteAsByte:
		case addrWordAsWord:
		case addrLongAsLong:

			StartFrame( 'w', GetAddrCode( Addr.a.m_Table ) );

			PutAddress( Addr );

			PutCount( Addr, uCount );

			break;
		}
 	}

void CIMOGLoaderPortDriver::StartFrame( BYTE bOpcode, BYTE bAddrCode )
{
	m_bCheck = 0;

	m_bTx[0] = 2;

	m_uPtr = 1;

	AddByte( bOpcode );

	AddByte( bAddrCode );
	}

void CIMOGLoaderPortDriver::EndFrame( void )
{
	AddData( m_bCheck, 0x10 );

	AddByte(3);
	}

void CIMOGLoaderPortDriver::PutAddress( AREF Addr )
{
	UINT uAddress = 0;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			uAddress = Addr.a.m_Offset/8;
			break;

		case addrByteAsByte:
			uAddress = Addr.a.m_Offset;
			break;

		case addrWordAsWord:
			uAddress = Addr.a.m_Offset*2;
			break;

		case addrLongAsLong:
			uAddress = Addr.a.m_Offset*4;
			break;
		}

	AddData( SwapLoWordBytes( uAddress ), 0x1000 );
	}

void CIMOGLoaderPortDriver::PutCount( AREF Addr, UINT uCount )
{
	switch ( Addr.a.m_Type ) {

		case addrBitAsBit:
			m_uSendCount = (uCount+7+(Addr.a.m_Offset%8))/8;
			break;

		case addrByteAsByte:
			m_uSendCount = uCount;
			break;

		case addrWordAsWord:
			m_uSendCount = uCount * 2;
			break;

		case addrLongAsLong:
			m_uSendCount = uCount * 4;
			break;
		}

	AddData( m_uSendCount, 0x1000 ); //nnnn
	}

void CIMOGLoaderPortDriver::AddWriteData(AREF Addr, PDWORD pData, UINT uCount )
{
	UINT i;
	UINT uMask = 1;
	UINT uData = 0;

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			uMask = ( 1 << (Addr.a.m_Offset % 8) );

			uData = ( m_bTx[1] == 'o' ) ? uMask : uMask ^ 0xFFFF;

			AddData( LOBYTE(LOWORD(uData)), 0x10 );

			return;

		case addrByteAsByte:

			for( i = 0; i < uCount; i++ ) {

				AddByte( LOBYTE(LOWORD(pData[i])) );
				}

			break;

		case addrWordAsWord:

			for( i = 0; i < uCount; i++ ) {

				AddData( SwapLoWordBytes( LOWORD(pData[i]) ), 0x1000 );
				}

			break;

		case addrLongAsLong:

			for( i = 0; i < uCount; i++ ) {

				AddData( SwapLoWordBytes( LOWORD(pData[i]) ), 0x1000 );

				AddData( SwapLoWordBytes( HIWORD(pData[i]) ), 0x1000 );
				}

			break;
		}
	}

BYTE CIMOGLoaderPortDriver::GetAddrCode( UINT uTable )
{
	switch ( uTable ) {

		case OPIX:
		case OPIB:
		case OPIW:
		case OPID:
			return 'X';
			break;

		case OPQX:
		case OPQB:
		case OPQW:
		case OPQD:
			return 'Y';
			break;

		case OPMX:
		case OPMB:
		case OPMW:
		case OPMD:
		default:
			return 'Z';
			break;
		}
	} 

// Data Transfer
	
BOOL CIMOGLoaderPortDriver::Transact(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
				
	return GetReply();
	}
	
BOOL CIMOGLoaderPortDriver::GetReply(void)
{	
	UINT uPtr   = 0;

	UINT uState = 0;

	BYTE bCheck = 0;

	SetTimer(1000);

	//if ( IDEBUG ) AfxTrace0("\r\n");

	while( GetTimer() ) {

		WORD wData = Get();
		
		if( wData == LOWORD(NOTHING) ) {
		
			Sleep(20);
			
			continue;
			}

		BYTE bData = LOBYTE(wData);

		//if ( IDEBUG ) AfxTrace1("<%2.2x>", bData );

		if( uPtr >= sizeof( m_bRx ) ) {

			return FALSE;
			}

		m_bRx[uPtr++] = bData;

		switch (uState) {

			case 0:
				if( bData == 6 ) {

					uPtr = 0;

					uState = 1;
					}
				break;

			case 1:
				if( bData == 4 ) {

					bCheck = 0;

					for( UINT i = 0; i < uPtr-3; i++ ) {

						bCheck += m_bRx[i];
						}

					if(	m_pHex[bCheck/16] == m_bRx[uPtr-3] &&
						m_pHex[bCheck%16] == m_bRx[uPtr-2]
						) {

						m_uLastByte = uPtr-4;

						return TRUE;
						}
					}
				break;

			default:
				break;
			}
		}
		
	return FALSE;
	}

// Response Handlers

CCODE CIMOGLoaderPortDriver::GetResponse( PDWORD pData, AREF Addr, UINT uReturnCount )
{
	if ( m_bRx[0] != 'r' ) return CCODE_ERROR;

	switch ( Addr.a.m_Type ) {

		case addrBitAsBit:

			GetBitResponse( pData, Addr.a.m_Offset, &uReturnCount );
			break;

		case addrByteAsByte:

			GetByteResponse( pData, &uReturnCount );
			break;

		case addrWordAsWord:

			GetWordResponse( pData, &uReturnCount );
			break;

		case addrLongAsLong:

			GetLongResponse( pData, &uReturnCount );
			break;
		}

	return uReturnCount;
	}

BOOL CIMOGLoaderPortDriver::GetBitResponse(PDWORD pData, UINT uOffset, UINT * pReturnCount)
{
	UINT uPtr = 1;
	UINT uData = 0;
	BYTE uMask = 1;

	uData = GetHex( uPtr, 2 );

	uMask <<= (uOffset % 8);

	for( UINT i = 0; i < *pReturnCount; i++ ) {

		if( uPtr > m_uLastByte ) { // ran out of data

			*pReturnCount = i;

			return TRUE;
			}

		pData[i] = DWORD( (uData & uMask) ? TRUE : FALSE);

		if( !(uMask <<= 1) ) {

			uPtr += 2;

			uData = GetHex( uPtr, 2 );

			uMask = 1;
			}
		}

	return TRUE;
	}

BOOL CIMOGLoaderPortDriver::GetByteResponse(PDWORD pData, UINT * pReturnCount)
{
	UINT uPtr = 1;

	for( UINT i = 0; i < *pReturnCount; i++ ) {

		if( uPtr > m_uLastByte ) { // ran out of data

			*pReturnCount = i;

			return TRUE;
			}

		pData[i] = MAKELONG( MAKEWORD( GetHex( uPtr, 2), 0), 0 );

		uPtr += 2;
		}

	return TRUE;
	}

BOOL CIMOGLoaderPortDriver::GetWordResponse(PDWORD pData, UINT * pReturnCount)
{
	UINT uData = 0;
	UINT uPtr = 1;

	for( UINT i = 0; i < *pReturnCount; i++ ) {

		if( uPtr > m_uLastByte ) { // ran out of data

			*pReturnCount = i;

			return TRUE;
			}

		uData = SwapLoWordBytes( GetHex( uPtr, 4 ) );

		pData[i] = MAKELONG( uData, uData & 0x8000 ? 0xFFFF : 0 );

		uPtr += 4;
		}

	return TRUE;
	}

BOOL CIMOGLoaderPortDriver::GetLongResponse(PDWORD pData, UINT * pReturnCount)
{
	UINT uDataL = 0;
	UINT uDataH = 0;
	UINT uPtr = 1;

	for( UINT i = 0; i < *pReturnCount; i++ ) {

		if( uPtr > m_uLastByte ) { // ran out of data

			*pReturnCount = i;

			return TRUE;
			}

		uDataL = SwapLoWordBytes( GetHex( uPtr, 4 ) );

		uDataH = SwapLoWordBytes( GetHex( uPtr+4, 4 ) );

		pData[i] = MAKELONG( uDataL, uDataH );

		uPtr += 8;
		}

	return TRUE;
	}

// Port Access

void CIMOGLoaderPortDriver::Put(void)
{
	/*if ( IDEBUG ) {
		AfxTrace0("\r\n");
		for ( UINT i = 0; i < m_uPtr; i++ )
			AfxTrace1("[%2.2x]", m_bTx[i] );
		}*/

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

UINT CIMOGLoaderPortDriver::Get( void )
{
	UINT uData;

	uData = m_pData->Read(100);

	return uData;
	}

// Helpers

void CIMOGLoaderPortDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck += bData;
	}
	
void CIMOGLoaderPortDriver::AddData(UINT uData, UINT uMask)
{
	while( uMask ) {
	
		AddByte(m_pHex[(uData / uMask) % 16]);
		
		uMask >>= 4;
		}
	}

UINT CIMOGLoaderPortDriver::GetHex(UINT p, UINT n)
{
	WORD d = 0;
	
	while( n-- ) {
	
		char c = char(m_bRx[p++]);

		if( c >= 'A' && c <= 'F' )
			d = (16 * d) + (c - '7');

		if( c >= 'a' && c <= 'f' )
			d = (16 * d) + (c - 'W');

		if( c >= '0' && c <= '9' )
			d = (16 * d) + (c - '0');
		}

	return d;
	}

UINT CIMOGLoaderPortDriver::GetMaxCount( AREF Addr, UINT uCount )
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			MakeMin( uCount, UINT(16-(Addr.a.m_Offset%16)) );
			break;

		case addrByteAsByte:
			MakeMin( uCount, 16 );
			break;

		case addrWordAsWord:
			MakeMin( uCount, 8 );
			break;

		case addrLongAsLong:
			MakeMin( uCount, 4 );
			break;
		}

	return uCount;
	}

UINT CIMOGLoaderPortDriver::SwapLoWordBytes( UINT uData )
{
	UINT u = UINT( HIBYTE(LOWORD(uData) ) );

	return ( u | ( LOBYTE(LOWORD(uData) ) << 8 ) );
	}

// End of File
