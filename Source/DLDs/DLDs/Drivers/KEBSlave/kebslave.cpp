
#include "intern.hpp"

#include "kebslave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// KEB Slave Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CKEBSlave);

// Constructor

CKEBSlave::CKEBSlave(void)
{
	m_Ident  = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Config

void MCALL CKEBSlave::Load(LPCBYTE pData)
{
	if( GetWord( pData ) == 0x1234 ) {

		m_bDrop = GetByte(pData);
		
		return;
		}
	}

void MCALL CKEBSlave::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, FALSE);
	}

void MCALL CKEBSlave::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

// Device

CCODE MCALL CKEBSlave::DeviceOpen(IDevice *pDevice)
{
	CSlaveDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_Model     = GetWord(pData);

			m_pCtx->m_bRcvDrop  = 255;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKEBSlave::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CSlaveDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CKEBSlave::Service(void)
{
	InitModelTags();

	for(;;) {

		if( !GetFrame() ) {

			continue;
			}

		if( m_pCtx->m_bRcvDrop == m_bDrop ) {

			if( !m_fIsWrite ) HandleRead();

			else HandleWrite();
			}
		}
	}

// Frame Handlers

void CKEBSlave::HandleRead(void)
{
	UINT uSet = 1;

	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Table  = 1;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Offset = FromHexRx( 0, 4 );

	BOOL fModelRequest = Addr.a.m_Offset == 0x180 || Addr.a.m_Offset == 0x160F;

	if( m_fLongCommand ) {

		UINT uSet = FromHexRx( 4, 2 );

		while( uSet > 1 ) {

			Addr.a.m_Extra += 1;
			uSet >>= 1;
			}
		}

	DWORD Data[1];

	if( fModelRequest || COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		if( fModelRequest ) Data[0] = m_pCtx->m_Model;

		m_uPtr  = 0;

		AddByte(STX);

		m_bCheck = 0;

		if( m_fLongCommand ) {

			AddByte(m_bRcvComm);
			AddByte(m_bIID);
			AddLong(Data[0]);
			}

		else {
			for( UINT i = 0; i < 4; i++ ) AddByte( m_bRx[i] );

			AddWord(LOWORD(Data[0]));
			}

		AddByte(ETX);

		m_bTx[m_uPtr++] = m_bCheck >= 0x20 ? m_bCheck : m_bCheck + 0x20;

		PutFrame();
		}
	}

void CKEBSlave::HandleWrite(void)
{
	CAddress Addr;

	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Table  = 1;
	Addr.a.m_Extra  = m_fLongCommand ? FromHexRx( 12, 2 ) : 1;
	Addr.a.m_Offset = FromHexRx( 0, 4 );

	DWORD Data[1];

	if( Addr.a.m_Offset == 0x180 || Addr.a.m_Offset == 0x160F ) Data[0] = m_pCtx->m_Model;

	else Data[0] = FromHexRx( 4, 8 );

	m_uPtr = 0;

	if( m_fLongCommand ) AddByte(m_bIID);

	if( COMMS_SUCCESS(Write(Addr, Data, 1)) ) {

		AddByte(ACK);
		}

	else AddByte(NAK);

	PutFrame();
	}

// Frame Building

void CKEBSlave::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck ^= bData;
	}

void CKEBSlave::AddWord(WORD wData)
{
	AddByte(m_pHex[HIBYTE(wData)/16]);
	AddByte(m_pHex[HIBYTE(wData)%16]);

	AddByte(m_pHex[LOBYTE(wData)/16]);
	AddByte(m_pHex[LOBYTE(wData)%16]);
	}

void CKEBSlave::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}
		
// Transport Layer & PortAccess

void CKEBSlave::PutFrame(void)
{
	m_pData->ClearRx();

//	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);	

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CKEBSlave::GetFrame(void)
{
	UINT uByte;

	UINT uPtr   = 0;

	UINT uState = RCV0;

	m_fIsWrite  = FALSE;

//	AfxTrace0("\r\n");

	for( ;; ) {
	
		if( (uByte = m_pData->Read(FOREVER)) == NOTHING ) {

			continue;
			}

//		AfxTrace1("<%2.2x>", uByte);

		switch( uState ) {

			case RCV0:
				switch( uByte ) {

					case EOT:
						uState = RCVAD1;
						break;

					case 'G':
					case 'H':
						m_bRcvComm     = uByte;
						m_fLongCommand = TRUE;
						uState         = RCVIID;
						break;

					case STX:
						m_fIsWrite = TRUE;
						break;

					default:
						if( uByte >= '0' && uByte <= '9' ) {
							m_bRx[0]       = uByte;
							m_fLongCommand = FALSE;
							uPtr           = 1;
							uState         = RCVDAT;
							}
						break;
					}

				break;

			case RCVAD1:
				m_pCtx->m_bRcvDrop = GetHexDigit(uByte) << 4;
				uState     = RCVAD2;
				break;

			case RCVAD2:
				m_pCtx->m_bRcvDrop += GetHexDigit(uByte);
				uState     = RCV0;
				break;

			case RCVIID:
				m_bIID = uByte;
				uState = RCVDAT;
				uPtr   = 0;
				break;

			case RCVDAT:
				m_bRx[uPtr++] = uByte;

				if( uByte == ENQ ) return TRUE;

				if( uByte == ETX ) uState = RCVCKS;

				break;

			case RCVCKS:
				return TRUE;
			}
		}

	return FALSE;
	}

DWORD CKEBSlave::FromHexRx(UINT uPos, UINT uCount)
{
	DWORD d = 0;

	for( UINT i = 0; i < uCount; i++ ) {

		BYTE b = m_bRx[uPos + i];

		if( b == ETX ) return d;

		d <<= 4;

		d += GetHexDigit( b );
		}

	return d;
	}

BYTE CKEBSlave::GetHexDigit(BYTE b)
{
	if( b >= '0' && b <= '9' ) return b - '0';
	if( b >= 'a' && b <= 'f' ) return b - 'W';
	if( b >= 'A' && b <= 'F' ) return b - '7';

	return 0;
	}

void CKEBSlave::InitModelTags(void)
{
	CAddress Addr;

	Addr.a.m_Table	= 1;
	Addr.a.m_Extra	= 0;
	Addr.a.m_Type	= addrLongAsLong;

	DWORD Data[1];
	Data[0] = m_pCtx->m_Model;

	Addr.a.m_Offset	= 0x180;
	Write( Addr, Data, 1 );

	Addr.a.m_Offset = 0x160F;
	Write( Addr, Data, 1 );
	}

// End of File
