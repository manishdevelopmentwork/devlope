
#include "Intern.hpp"

#include "UsbBase437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Hardware Drivers
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Macros  
//

#define RoundUp(x, n)	((x + (n-1)) & ~(n-1))

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define RegWrap(x)	(m_pBaseWrap[reg##x])

#define RegCore(x)	(m_pBaseCore[reg##x])

#define RegPhy(x)	(m_pBasePhy [reg##x])

//////////////////////////////////////////////////////////////////////////
//
// AM437 Usb Base Class
//

// Constructor

CUsbBase437::CUsbBase437(UINT iIndex)
{
	StdSetRef();	
	
	FindModule(iIndex);
	}

// Destructor

CUsbBase437::~CUsbBase437(void)
{
	}

// IEventSink

void CUsbBase437::OnEvent(UINT uLine, UINT uParam)
{
	}

// Operations

void CUsbBase437::ResetWrapper(void)
{
	RegWrap(SYSCONFIG) |= Bit(17);

	while( RegWrap(SYSCONFIG) & Bit(17) );
	}

void CUsbBase437::SetMode(UINT uMode)
{
	DWORD Data = RegCore(GCTL) & ~(0x3 << 12);

	RegCore(GCTL)  = Data;

	RegCore(GCTL) |= (uMode << 12);
	}

void CUsbBase437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLineCore, this, 0);

	phal->SetLineHandler(m_uLineMisc, this, 0);

	phal->EnableLine(m_uLineCore, true);

	phal->EnableLine(m_uLineMisc, true);
	}

void CUsbBase437::DisableEvents(void)
{
	phal->EnableLine(m_uLineMisc, false);

	phal->EnableLine(m_uLineCore, false);
	}

void CUsbBase437::EnableCoreInts(void)
{
	RegWrap(IRQENABLESETMAIN) = Bit(0);
	}

void CUsbBase437::DisableCoreInts(void)
{
	RegWrap(IRQENABLECLRMAIN) = Bit(0);
	}

void CUsbBase437::FindModule(UINT iIndex)
{
	switch( iIndex ) {

		case 0:
			m_pBaseWrap = PVDWORD(ADDR_USB0_WRAPPER);
		    
			m_pBaseCore = PVDWORD(ADDR_USB0_DWC3);
		    
			m_pBasePhy  = PVDWORD(ADDR_USB0_PHY);
	            
			m_uLineCore = INT_USB0_MAIN0;
		    
			m_uLineMisc = INT_USB0_MISC;
			
			break;
		
		case 1: 
			m_pBaseWrap = PVDWORD(ADDR_USB1_WRAPPER);
		    
			m_pBaseCore = PVDWORD(ADDR_USB1_DWC3);
		    
			m_pBasePhy  = PVDWORD(ADDR_USB1_PHY);
	            
			m_uLineCore = INT_USB1_MAIN0;
		    
			m_uLineMisc = INT_USB1_MISC;
		
			break;
		}
	}

// Memory Helpers

PVOID CUsbBase437::AllocNonCached(UINT uAlloc)
{
	return AllocNonCached(uAlloc, 64);
	}

PVOID CUsbBase437::AllocNonCached(UINT uAlloc, UINT uAlign)
{
	uAlign = RoundUp(uAlign, 64);

	uAlloc = RoundUp(uAlloc, 64);

	PVOID pMem  = memalign(uAlign, uAlloc);

	DWORD Alias = phal->GetNonCachedAlias(DWORD(pMem));

	DWORD Phy   = phal->VirtualToPhysical(DWORD(pMem));

	ProcPurgeDataCache(Phy, uAlloc);

	return PVOID(Alias);
	}

void CUsbBase437::FreeNonCached(PVOID pMem)
{
	if( pMem ) {

		DWORD Alias = phal->GetCachedAlias(DWORD(pMem));

		free(PVOID(Alias));
		}
	}

// End of File
