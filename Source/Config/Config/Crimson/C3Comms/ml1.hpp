//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ML1_HPP
	
#define	INCLUDE_ML1_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCodedItem : public CItem { };

class CML1Block;

class CML1BlockList;

class CSpaceML1;

//////////////////////////////////////////////////////////////////////////
//
// Enumerations
//

enum Space {

	spHR = 1,
	spAI = 2,
	spDC = 3,
	spDI = 4,
	spFR = 5,
	spSR = 6,
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Driver Options
//

class CML1DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CML1DriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_AuthInt;
				
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Driver Options
//

class CML1SerialDriverOptions : public CML1DriverOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CML1SerialDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Track;
		
				
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Device Options
//

class CML1DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CML1DeviceOptions(void);

		// Persistance
		void Load(CTreeFile &Tree);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_PingReg;
		UINT m_TO;
		UINT m_Retry;

		// Block Item Access
		CML1Block *	GetBlock(CAddress Addr);
		BOOL		AppendBlock(CML1Block * pBlock);
		BOOL		RemoveBlock(CML1Block * pBlock);
		BOOL		ExpandBlock(CAddress Addr, UINT uSpan);
		CML1BlockList * GetBlockList(void);
		BOOL		IsBlockBegin(UINT uRef);
		void		Rebuild(void);
						
	protected:

		// Data Members
		CML1BlockList * m_pBlocks;
				
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Device Options UI Page
//

class CML1DeviceOptionsUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CML1DeviceOptionsUIPage(CML1DeviceOptions * pOption);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CML1DeviceOptions * m_pOption;
	}; 

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Device Options
//

class CML1SerialDeviceOptions : public CML1DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CML1SerialDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Ethernet Device Options
//

class CML1TCPDeviceOptions : public CML1DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CML1TCPDeviceOptions(void);

		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Addr1;
		UINT m_Addr2;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_IpSel1;

		CString m_Text1;
							
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Device Options UI Page
//

class CML1TCPDeviceOptionsUIPage : public CML1DeviceOptionsUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CML1TCPDeviceOptionsUIPage(CML1DeviceOptions * pOption);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	}; 


//////////////////////////////////////////////////////////////////////////
//
// ML1 Base Driver
//

class CML1BaseDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CML1BaseDriver(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Notifications
		void NotifyInit(CItem * pConfig);
		void NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);

		// Overridables
		BOOL CheckAlignment(CSpace *pSpace);
						
	protected:
		// Data Members
		BOOL m_fRebuild;
		BOOL m_fSelect;

		// Implementation
		void AddSpaces(void);
		UINT GetSpan(CAddress Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Driver
//

class CML1SerialDriver : public CML1BaseDriver
{
	public:
		// Constructor
		CML1SerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Serial Tester Driver
//

class CML1SerialTesterDriver : public CML1SerialDriver
{
	public:
		// Constructor
		CML1SerialTesterDriver(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Ethernet Driver
//

class CML1TCPDriver : public CML1BaseDriver
{
	public:
		// Constructor
		CML1TCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Ethernet Tester Driver
//

class CML1TCPTesterDriver : public CML1TCPDriver
{
	public:
		// Constructor
		CML1TCPTesterDriver(void);

		// Address Management
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block Item List
//

class CML1BlockList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CML1BlockList(void);
		
		// Item Access
		CML1Block * GetItem(INDEX Index) const;
		CML1Block * GetItem(UINT  uPos ) const;

		// Operations
		CML1Block * AppendItem(CML1Block * pBlock);
		INDEX	    InsertItem(CML1Block * pID, CML1Block * pPre);

	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block Item
//

class CML1Block : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructors
		CML1Block(void);
		CML1Block(CML1Block const &Block, CAddress Addr, UINT uSpan);
		
		// UI Loading
		BOOL OnLoadPages(CUIPageList * pList);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Data Access
		UINT	     m_Space;
		UINT	     m_Reg;
		UINT	     m_File;
		UINT	     m_Record;
		UINT	     m_Type;
		CCodedItem * m_pRBE;
		UINT	     m_COV;
		UINT         m_TO;
		UINT	     m_Retry;
		BOOL	     m_fMark;
		UINT	     m_AddrRef;
			
		// Data Access
		CString	   GetSelection(void);
		CSpace	 * GetSpace(void);
		CSpaceList GetSpaceList(void);
		BOOL	   SetAddress(CAddress Addr);
		BOOL	   SetSize(UINT uSize);
		UINT	   GetSize(void);
		UINT	   GetRef(void);
		void	   SetDriver(CML1BaseDriver * pDriver);
		CString	   GetSelText(void);
		UINT	   SpaceToSelection(UINT uSpace);
		
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void PostLoad(void);
										
	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Data Members
		CML1BaseDriver * m_pDriver;
		CAddress	 m_Addr;
		UINT		 m_Size;
		UINT		 m_TypeRef;
		CString		 m_SelText;
														
		// Implementaion
		void DoEnables(IUIHost *pHost, UINT uSpace, BOOL fFile);
		void SetDefaults(void);
		void LoadFields(IUIHost *pHost, CItem *pItem, UINT uSpace);
		void TypeMetaToRef(UINT uType, UINT uSpace);
		void UpdateSelText(UINT uSpace, UINT uReg, UINT uType, UINT uFile = 0);

		//Helpers
		CSpace * GetSpace(UINT uSpace);
		UINT	 SelectionToSpace(UINT uSel);
				
	};
	
//////////////////////////////////////////////////////////////////////////
//
// ML1 Block Item UI
//

class CML1BlockUIPage : public CUIPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CML1BlockUIPage(CML1Block * pBlock);
		
		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:

		CML1Block * m_pBlock;
	}; 

//////////////////////////////////////////////////////////////////////////
//
// ML1 Space Wrapper Class
//

class CSpaceML1 : public CSpace
{
	public:
		// Constructors
		CSpaceML1(UINT uTable, PCSTR p, PCSTR c, UINT r, UINT n, UINT x, UINT t, UINT s = 0, UINT fd = 0, UINT fn = 0, UINT fx = 0);

		// Public Data
		UINT m_FileDef;
		UINT m_FileMin;
		UINT m_FileMax;

		// Data Encoding
		BOOL IsFileRecord(void);
		UINT GetFile(CAddress Addr);
		UINT GetRecord(CAddress Addr);
		BOOL SetFile(CAddress &Addr, UINT uFile);
		BOOL SetRecord(CAddress &Addr, UINT uRecord);

		// Type Checking
		BOOL IsTypeOutOfRange(UINT uRef);
	
	};

//////////////////////////////////////////////////////////////////////////
//
// ML1 Block View Dialog
//

class CML1BlockDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CML1BlockDialog(CML1DeviceOptions * pOptions);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCopy(UINT uID);
					
	protected:

		CML1DeviceOptions * m_pOptions;
		CStringArray	    m_Blocks;
		
		// Implementation
		void LoadBlockList(void);
	};


// End of File

#endif
