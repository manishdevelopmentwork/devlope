
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryPage_HPP

#define INCLUDE_SqlQueryPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlQuery;

//////////////////////////////////////////////////////////////////////////
//
// Sql Query Page
//

class CSqlQueryPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSqlQueryPage(CSqlQuery *pQuery);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CSqlQuery * m_pQuery;

		// Implementation
		void LoadContents(IUICreate *pView, CItem *pItem);
		void LoadColumns(IUICreate *pView, CItem *pItem);
	};

// End of File

#endif
