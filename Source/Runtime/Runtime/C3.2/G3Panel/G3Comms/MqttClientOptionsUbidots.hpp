
#include "Intern.hpp"

#include "G3Http.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqqtClientOptionsUbidots_HPP

#define	INCLUDE_MqqtClientOptionsUbidots_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptionsJson.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

class CMqttClientOptionsUbidots : public CMqttClientOptionsJson
{
public:
	// Constructor
	CMqttClientOptionsUbidots(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Config Fixup
	BOOL FixConfig(void);

	// Attributes
	CString GetExtra(void) const;

	// Data Members
	CString m_Token;
	CString m_Device;
	CString m_PubTopic;
	CString m_SubTopic;
};

// End of File

#endif
