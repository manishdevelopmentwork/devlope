//----------------------------------------------------------------------------
// Copyright(c) 2011-2012 N-Tron Corporation.  All rights reserved.
//----------------------------------------------------------------------------
//
//    Module: Autocast
//
//  File $Id:  $
//   Creator: Carlos Navia
//   Created: 3/30/2012
//
// $Revision:  $
//     $Date:  $
//   $Author:  $
//  $HeadURL:  $
//
// Description:
// ------------
// Autocast definitions
//----------------------------------------------------------------------------

#ifndef _nview_Autocast_h_
#define _nview_Autocast_h_

//-----------------------//
//--- System Includes ---//
//-----------------------//

//--------------------------//
//--- Component Includes ---//
//--------------------------//

//----------------------//
//--- Local Includes ---//
//----------------------//

//----------------------------------------------------------------------------
//--- Processor Byte Order ---------------------------------------------------
//----------------------------------------------------------------------------

#if defined(_E_LITTLE)

#undef  BIG_ENDIAN

#else

#define BIG_ENDIAN

#endif

//----------------------------------------------------------------------------
//--- Macros -----------------------------------------------------------------
//----------------------------------------------------------------------------

#define NT_NViewAc_Type				e_EthType_NTron_NView	// Special autocast packet type.
#define NVIEW_AC_EVENT_GROUP_NAME		"NView_Ac_EventGroup"
#define NT_NVIEW_EVENT_TIMER_AUTOCAST		0x00000001
#define NT_AC_MODEL_MAX_SIZE			32			// Value should not change.

//----------------------------------------------------------------------------
//
// Our Autocast Packet Version
//
//----------------------------------------------------------------------------
//#define NT_NViewAc_Version	1		// Includes IP Address, Model Name and NRing Information
//#define NT_NViewAc_Version	2		// AcRadioData
#define NT_NViewAc_Version		3		// Added Variable Data types using AcDataHeaders

//----------------------------------------------------------------------------
//--- Types ------------------------------------------------------------------
//----------------------------------------------------------------------------

typedef NT_U16	nview_AcPortIdType;			// NView Ac port ID [0..x]
typedef NT_U32	nview_AcPortMaskType;		// NView Ac port mask

typedef enum
{
	e_AcRingData_ManagerRingState_Broken,
	e_AcRingData_ManagerRingState_HalfBroken_Lo,
	e_AcRingData_ManagerRingState_HalfBroken_Hi,
	e_AcRingData_ManagerRingState_Healthy
} AcRingData_ManagerRingStateType;

// Autocast Frame Data Header
typedef NTOS_PACKED struct _AcFrameDataHeaderType
{
	NT_U8		dst_addr[6];
	NT_U8		src_addr[6];
	NT_U16		nviewAc_type;
} AcFrameDataHeaderType;

// Autocast Data Header
typedef NTOS_PACKED struct _AcDataHeaderType
{
	NT_U32	m_nCreatorId;
	NT_U16	m_nVersion;
	NT_U16	m_nSize;
} AcDataHeaderType;

/* 
 * Autocast N-Ring data 
 *  - Included in Ac Version 1 information
 */
typedef NTOS_PACKED struct _AcRingData
{
	NT_U8	nring_manager_version;
	#if defined(BIG_ENDIAN)
		NT_U8	reserved:4;
		// 00  RING_STATE_BROKEN
		// 01  RING_STATE_HALFBROKEN lo ring port not rx
		// 10  RING_STATE_HALFBROKEN hi ring port not rx
		// 11  RING_STATE_HEALTHY
		NT_U8	is_nring_manager_ring_state:2;
		NT_U8	is_active_nring_member:1;
		NT_U8	is_nring_manager:1;
	#else  /* BIG_ENDIAN */
		NT_U8	is_nring_manager:1;
		NT_U8	is_active_nring_member:1;
		// 00  RING_STATE_BROKEN
		// 01  RING_STATE_HALFBROKEN lo ring port not rx
		// 10  RING_STATE_HALFBROKEN hi ring port not rx
		// 11  RING_STATE_HEALTHY
		NT_U8	is_nring_manager_ring_state:2;
		NT_U8	reserved:4;
	#endif /* BIG_ENDIAN */
} AcRingData;


/*
 * Autocast Port Extension Data
 *  - Included In Ac Version 3 as Data - Using "NT_Creator_NViewPortExtension" in AcDataHeader
*/
typedef NTOS_PACKED struct _AcPortExtDataType
{
	AcDataHeaderType	m_oHeader;	// Data Header Containing CreatorID, Version and Size
	NT_U16				m_nSpeedMb;	// Speed in Mega-bits / Sec (10 = 10Mb, 100=100Mb, 1000=1Gb)
} AcPortExtDataType;

/*
 * Autocast frame
 */
typedef NTOS_PACKED struct _AcPortStateType
{
	#if defined(BIG_ENDIAN)
		NT_U8	speed:1;
		NT_U8	duplex:1;
		NT_U8	link_status:1;
		NT_U8	port_enable:1;
		NT_U8	reserved:3;
		NT_U8	pause:1;
	#else  /* BIG_ENDIAN */
		NT_U8	pause:1;
		NT_U8	reserved:3;
		NT_U8	port_enable:1;
		NT_U8	link_status:1;
		NT_U8	duplex:1;
		NT_U8	speed:1;
	#endif /* BIG_ENDIAN */
} AcPortStateType;

typedef NTOS_PACKED struct _AcPortNumberType
{
	#if defined(BIG_ENDIAN)
		NT_U8	port_id:4;
		NT_U8	reserved:2;
		NT_U8	chip_id:2;
	#else  /* BIG_ENDIAN */
		NT_U8	chip_id:2;
		NT_U8	reserved:2;
		NT_U8	port_id:4;
	#endif /* BIG_ENDIAN */
} AcPortNumberType;

typedef NTOS_PACKED struct _AcPortType
{
	AcPortNumberType	number;
	AcPortStateType		state;
} AcPortType;

typedef NTOS_PACKED struct _AcMibType
{
	NT_U64		tx_octets;
	NT_U32		tx_drop_pkts;
	NT_U32		tx_qos_pkts;			// Was: reserved1
	NT_U32		tx_broadcast_pkts;
	NT_U32		tx_multicast_pkts;
	NT_U32		tx_unicast_pkts;
	NT_U32		tx_collisions;
	NT_U32		tx_single_collision;
	NT_U32		tx_multiple_collision;
	NT_U32		tx_deferred_transmit;
	NT_U32		tx_late_collision;
	NT_U32		tx_excessive_collision;
	NT_U32		tx_frame_in_disc;
	NT_U32		tx_pause_pkts;
	NT_U64		tx_qos_octets;			// Was: NT_U32 reserved2; NT_U32 reserved3;
	NT_U64		rx_octets;
	NT_U32		rx_under_size_pkts;
	NT_U32		rx_pause_pkts;
	NT_U32		pkts_64_octets;
	NT_U32		pkts_65to127_octets;
	NT_U32		pkts_128to255_octets;
	NT_U32		pkts_256to511_octets;
	NT_U32		pkts_512to1023_octets;
	NT_U32		pkts_1024to1522_octets;
	NT_U32		rx_over_size_pkts;
	NT_U32		rx_jabbers;
	NT_U32		rx_alignment_errors;
	NT_U32		rx_fcs_errors;
	NT_U64		rx_good_octets;
	NT_U32		rx_drop_pkts;
	NT_U32		rx_unicast_pkts;
	NT_U32		rx_multicast_pkts;
	NT_U32		rx_broadcast_pkts;
	NT_U32		rx_sa_changes;
	NT_U32		rx_fragments;
	NT_U32		rx_excess_size_disc;
	NT_U32		rx_symbol_error;
	NT_U32		rx_qos_pkts;
	NT_U64		rx_qos_octets;
} AcMibType;

typedef NTOS_PACKED struct _AcFrameDataType
{
//	AcFrameDataHeaderType	m_oHeader;		// Added Automatically By Bcm_SendRawIpFrame();
	NT_U16					m_nVersion;		// Our Autocast Version
	// Start Version 1 Values
	NT_U32					m_nIpAddress;	// IP Address
	AcRingData				m_oRing;		// N-Ring Information
	NT_U8					m_acModel[NT_AC_MODEL_MAX_SIZE];	// Model Number
	// End Version 1 Values
	// Start Version 3 Values
//	AcNLinkDataType			m_oLinkData;	// N-Link Information (UNUSED)
	AcPortExtDataType		m_oPortExtData;	// Port Extension Data
	// End Version 3 Values
	AcPortType				m_oPort;		// Must Be At End - For N-View Decoding
	AcMibType				m_oMib;			// Must Be At End - For N-View Decoding
} AcFrameDataType;

// /* 
//  * Autocast N-Link data 
//  *  - Not Currently Included In Ac Version 3 as Data
// */
// typedef enum
// {
// 	e_AcNlink_State_AutoConfigure,			// This switch is auto configure
// 	e_AcNlink_State_Master,					// This switch is the master
// 	e_AcNlink_State_Slave,					// This switch is the slave
// 	e_AcNlink_State_Disabled				// This switch is the slave
// } AcNlink_RoleType;
// 
// typedef enum
// {
// 	e_AcNlink_Integrity_Failed,			
// 	e_AcNlink_Integrity_Good			
// } AcNlink_Primary_Standby_Integrity_Type;
// 
// typedef enum
// {
// 	e_AcNlink_State_Blocking,			
// 	e_AcNlink_State_Forward			
// } AcNlink_Primary_Standby_State_Type;
// 
// typedef enum
// {
// 	e_AcNlink_Link_Down,			
// 	e_AcNlink_Link_Up			
// } AcNlink_Port_Link_Status_Type;
// 
// typedef NTOS_PACKED struct _AcNLinkDataType
// {
//	AcDataHeaderType	m_oHeader;					// Data Header Containing CreatorID, Version and Size
// #if defined(BIG_ENDIAN)
// 	NT_U8				m_nreservedBit:1;
// 	NT_U8				m_nRoleState:2;				// AcNlink_RoleType (Master / Slave)
// 	NT_U8				m_nLinkState:1;				// AcNlink_Primary_Standby_State_Type
// 	NT_U8				m_nLinkIntegrity:1;			// AcNlink_Primary_Standby_Integrity_Type
// 	NT_U8				m_nControlPortStatus:1;		// AcNlink_Port_Link_Status_Type
// 	NT_U8				m_nPartnerPortStatus:1;		// AcNlink_Port_Link_Status_Type
// 	NT_U8				m_nNLinkPortStatus:1;		// AcNlink_Port_Link_Status_Type
// #else  /* BIG_ENDIAN */
// 	NT_U8				m_nNLinkPortStatus:1;		// AcNlink_Port_Link_Status_Type
// 	NT_U8				m_nPartnerPortStatus:1;		// AcNlink_Port_Link_Status_Type
// 	NT_U8				m_nControlPortStatus:1;		// AcNlink_Port_Link_Status_Type
// 	NT_U8				m_nLinkIntegrity:1;			// AcNlink_Primary_Standby_Integrity_Type
// 	NT_U8				m_nLinkState:1;				// AcNlink_Primary_Standby_State_Type
// 	NT_U8				m_nRoleState:2;				// AcNlink_RoleType
// 	NT_U8				m_nreservedBit:1;
// #endif /* BIG_ENDIAN */
// } AcNLinkDataType;

//----------------------------------------------------------------------------
//--- Export Global Data -----------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//--- Export Public Functions ------------------------------------------------
//----------------------------------------------------------------------------
NT_STATUS	nview_AutocastInit		();
NT_STATUS	nview_Autocast_Stop		();
NT_VOID		nview_DumpPortMibData		(port_NumberType nPortNumber, AcFrameDataType * poAcData);
NT_STATUS	nview_Autocast_Start		(NT_U32 nTimerInterval);

//
// The following functions are accessed from regular C functions,
// so we need to declare them in C style, not C++
//
//#ifdef __cplusplus
//extern "C" {			/* C declarations in C++     */
//#endif
//
//#ifdef __cplusplus
//}
//#endif

#endif	// _nview_Autocast_h_
