
#include "intern.hpp"

#include "UITextAO8Dynamic.hpp"

#include "UIAO8Dynamic.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Analog Output Module Dynamic Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextAO8Dynamic, CUITextInteger)

// Constructor

CUITextAO8Dynamic::CUITextAO8Dynamic(void)
{
}

// Destructor

CUITextAO8Dynamic::~CUITextAO8Dynamic(void)
{
}

// Core Overidables

void CUITextAO8Dynamic::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CDAAO8OutputConfig)) ) {

		m_pConfig = (CDAAO8OutputConfig *) m_pItem;
	}
	else
		AfxAssert(FALSE);

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
}

// Implementation

void CUITextAO8Dynamic::GetConfig(void)
{
	m_Type = m_UIData.m_Format;

	FindPlaces();

	FindUnits();

	FindRanges();

	CheckFlags();
}

void CUITextAO8Dynamic::FindPlaces(void)
{
	switch( m_Type[0] ) {

		case 'A':
		case 'B':
		case 'E':

			switch( m_Type[1] ) {

				case '1':	m_uPlaces = m_pConfig->m_DP1;	break;
				case '2':	m_uPlaces = m_pConfig->m_DP2;	break;
				case '3':	m_uPlaces = m_pConfig->m_DP3;	break;
				case '4':	m_uPlaces = m_pConfig->m_DP4;	break;
				case '5':	m_uPlaces = m_pConfig->m_DP5;	break;
				case '6':	m_uPlaces = m_pConfig->m_DP6;	break;
				case '7':	m_uPlaces = m_pConfig->m_DP7;	break;
				case '8':	m_uPlaces = m_pConfig->m_DP8;	break;

				default:	m_uPlaces = 3;			break;
			}

			break;

		default:
			m_uPlaces = 3;
			break;
	}
}

void CUITextAO8Dynamic::FindUnits(void)
{
	switch( m_Type[0] ) {

		case 'C':
		case 'D':

			switch( m_Type[1] ) {

				case '1':	m_Units = (m_pConfig->m_Type1 >= 7) ? "mA" : "V";	break;
				case '2':	m_Units = (m_pConfig->m_Type2 >= 7) ? "mA" : "V";	break;
				case '3':	m_Units = (m_pConfig->m_Type3 >= 7) ? "mA" : "V";	break;
				case '4':	m_Units = (m_pConfig->m_Type4 >= 7) ? "mA" : "V";	break;
				case '5':	m_Units = (m_pConfig->m_Type5 >= 7) ? "mA" : "V";	break;
				case '6':	m_Units = (m_pConfig->m_Type6 >= 7) ? "mA" : "V";	break;
				case '7':	m_Units = (m_pConfig->m_Type7 >= 7) ? "mA" : "V";	break;
				case '8':	m_Units = (m_pConfig->m_Type8 >= 7) ? "mA" : "V";	break;
			}
			break;

		default:
			m_Units = "";
			break;
	}
}

void CUITextAO8Dynamic::FindRanges(void)
{
	switch( m_Type[0] ) {

		case 'A':

			switch( m_Type[1] ) {

				case '1':	SetMinMax(m_pConfig->m_Type1);	break;
				case '2':	SetMinMax(m_pConfig->m_Type2);	break;
				case '3':	SetMinMax(m_pConfig->m_Type3);	break;
				case '4':	SetMinMax(m_pConfig->m_Type4);	break;
				case '5':	SetMinMax(m_pConfig->m_Type5);	break;
				case '6':	SetMinMax(m_pConfig->m_Type6);	break;
				case '7':	SetMinMax(m_pConfig->m_Type7);	break;
				case '8':	SetMinMax(m_pConfig->m_Type8);	break;
			}

			break;

		case 'B':
			switch( m_Type[1] ) {

				case 1:
					m_nMin = m_pConfig->m_DataLo1;
					m_nMax = m_pConfig->m_DataHi1;
					break;

				case 2:
					m_nMin = m_pConfig->m_DataLo2;
					m_nMax = m_pConfig->m_DataHi2;
					break;

				case 3:
					m_nMin = m_pConfig->m_DataLo3;
					m_nMax = m_pConfig->m_DataHi3;
					break;

				case 4:
					m_nMin = m_pConfig->m_DataLo4;
					m_nMax = m_pConfig->m_DataHi4;
					break;

				case 5:
					m_nMin = m_pConfig->m_DataLo5;
					m_nMax = m_pConfig->m_DataHi5;
					break;

				case 6:
					m_nMin = m_pConfig->m_DataLo6;
					m_nMax = m_pConfig->m_DataHi6;
					break;

				case 7:
					m_nMin = m_pConfig->m_DataLo7;
					m_nMax = m_pConfig->m_DataHi7;
					break;

				case 8:
					m_nMin = m_pConfig->m_DataLo8;
					m_nMax = m_pConfig->m_DataHi8;
					break;
			}
			break;

		default:
			SetMinMax(0);
			break;
	}
}

void CUITextAO8Dynamic::SetMinMax(UINT OutType)
{
	switch( OutType ) {

		case 1:
			m_nMin =      0;
			m_nMax =   5000;
			break;

		case 4:
			m_nMin =      0;
			m_nMax =  10000;
			break;

		case 5:
			m_nMin = -10000;
			m_nMax =  10000;
			break;

		case 7:
			m_nMin =      0;
			m_nMax =  20000;
			break;

		case 8:
			m_nMin =   4000;
			m_nMax =  20000;
			break;

		default:
			m_nMin = -30000;
			m_nMax =  30000;
			break;
	}
}

void CUITextAO8Dynamic::CheckFlags(void)
{
	if( m_uPlaces ) {

		m_uFlags |= textPlaces;
	}
	else
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
	}
	else
		m_uFlags &= ~textSigned;
}

// End of File
