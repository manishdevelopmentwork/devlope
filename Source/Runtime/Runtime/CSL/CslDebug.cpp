
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Debug Support
//

// Static Data

static IDebug * m_pDebug = NULL;

// Common Code

global void Bind_Debug(void)
{
	AfxGetObject("debug", 0, IDebug, m_pDebug);
	}

global void Free_Debug(void)
{
	m_pDebug->Release();

	m_pDebug = NULL;
	}

global bool AfxHasDebug(void)
{
	// TODO -- Forward somewhere...

	return true;
	}

clink void _AfxTrace(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	m_pDebug->AfxTraceArgs(pText, pArgs);	

	va_end(pArgs);
	}

global void AfxTrace(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	m_pDebug->AfxTraceArgs(pText, pArgs);	

	va_end(pArgs);
	}

global void AfxTraceArgs(PCTXT pText, va_list pArgs)
{
	m_pDebug->AfxTraceArgs(pText, pArgs);
	}

global void AfxPrint(PCTXT pText)
{
	m_pDebug->AfxPrint(pText);
	}

global void AfxDump(PCVOID pData, UINT uCount)
{
	m_pDebug->AfxDump(pData, uCount);
	}

global void AfxAssertFailed(PCTXT pFile, UINT uLine)
{
	m_pDebug->AfxAssertFailed(pFile, uLine);
	}

// Pointer Asserts

#if defined(_DEBUG)

global void AfxAssertReadPtr(PCVOID pData, UINT uSize)
{
	m_pDebug->AfxAssertReadPtr(pData, uSize);
	}

global void AfxAssertWritePtr(PVOID pData, UINT uSize)
{
	m_pDebug->AfxAssertWritePtr(pData, uSize);
	}

global void AfxAssertStringPtr(PCTXT pText, UINT uSize)
{
	m_pDebug->AfxAssertStringPtr(pText, uSize);
	}

global void AfxAssertStringPtr(PCUTF pText, UINT uSize)
{
	m_pDebug->AfxAssertStringPtr(pText, uSize);
	}

#endif

// Pointer Checks

global BOOL AfxCheckReadPtr(PCVOID pData, UINT uSize)
{
	return m_pDebug->AfxCheckReadPtr(pData, uSize);
	}

global BOOL AfxCheckWritePtr(PVOID pData, UINT uSize)
{
	return m_pDebug->AfxCheckWritePtr(pData, uSize);
	}

global BOOL AfxCheckStringPtr(PCTXT pText, UINT uSize)
{
	return m_pDebug->AfxCheckStringPtr(pText, uSize);
	}

global BOOL AfxCheckStringPtr(PCUTF pText, UINT uSize)
{
	return m_pDebug->AfxCheckStringPtr(pText, uSize);
	}

// Stack Walking

global void AfxStackTrace(void)
{
	m_pDebug->AfxStackTrace();
	}

// End of File
