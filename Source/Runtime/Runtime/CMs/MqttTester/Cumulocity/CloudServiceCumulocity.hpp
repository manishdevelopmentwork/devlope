
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudServiceSparkplug_HPP
	
#define	INCLUDE_CloudServiceSparkplug_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <G3Http.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Cloud Manager
//

class CCloudServiceCumulocity : public CServiceItem, public ITaskEntry
{
	public:
		// Constructor
		CCloudServiceCumulocity(void);

		// Destructor
		~CCloudServiceCumulocity(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Service ID
		UINT GetID(void);

		// Task List
		void GetTaskList(CTaskList &List);

		// Task Entries
		void TaskInit(UINT uID);
		void TaskExec(UINT uID);
		void TaskStop(UINT uID);
		void TaskTerm(UINT uID);

		// Item Properties
		CCodedItem    * m_pEnable;
		UINT		m_uTags;
		DWORD	      * m_pTags;
		BYTE	      * m_pType;
		CCodedItem    * m_pCredTag;

	protected:
		// Typedef
		typedef CHttpClientConnectionOptions COpts;

		// Configuration
		struct CConfig
		{
			BOOL	m_fEnable;
			UINT	m_uService;
			CString m_Host;
			CString m_Device;
			CString m_Node;
			CString m_User;
			CString m_Pass;
			UINT	m_uScan;
			UINT	m_uBatch;
			BOOL	m_fClose;
			COpts   m_Opts;
			};

		// Data Members
		CConfig			m_Cfg;
		CTagList	      * m_pSrc;
		CHttpClientConnection * m_pConnect;
		IEvent		      * m_pSave;

		// Cloud Parameters
		CString	m_Mac;
		CString	m_Make;
		CString	m_Model;
		CString	m_Serial;
		CString	m_Frag;
		CString m_Name;
		CString	m_MainId;

		// Data Buffer
		PDWORD m_pData;
		PDWORD m_pTime;
		UINT   m_uSize;
		UINT   m_uHead;
		UINT   m_uTail;

		// Transactions
		BOOL GetCredentials(void);
		BOOL EnsureRegistered(void);
		BOOL CheckRegistered(BOOL &fReply);
		BOOL CheckMainDevice(BOOL &fReply);
		BOOL CreateMainDevice(void);
		BOOL RegisterDeviceSerial(void);
		BOOL RegisterDeviceMac(void);
		BOOL SendMeasurements(UINT uTail);
		BOOL SendMeasurement(UINT uSlot);

		// Implementation
		void    FindMac(void);
		BOOL    LoadTagList(PCBYTE &pData);
		void    InitTagList(void);
		void	AllocDataBuffer(void);
		CString GetTime(DWORD dwTime);
		BOOL	TakeSample(DWORD dwTime);
		void    FreeHeadStrings(void);
		BOOL	ReadCredTag(void);
		BOOL	WriteCredTag(void);
	};

// End of File

#endif
