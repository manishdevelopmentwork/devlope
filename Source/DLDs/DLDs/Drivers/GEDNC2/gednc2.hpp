
//////////////////////////////////////////////////////////////////////////
//
// GE DNC2 Master Driver
//

#define GEDNC2_ID 0x403E

struct FAR GEDNC2CmdDef {

	UINT	uID;
	char 	sName[4];
	UINT	uFirst;
	};

class CGEDNC2MasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CGEDNC2MasterDriver(void);

		// Destructor
		~CGEDNC2MasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(UINT)	DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			DWORD	m_dWriteParam[3];
			DWORD	m_dWriteTool[15];
			DWORD	m_dMessage[10];
			};

		CContext * m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bRx[256];
		BYTE	m_bBCC;

		UINT	m_uPtr;
		UINT	m_uState;

		// CmdDef
		static	GEDNC2CmdDef CODE_SEG CL[];
		GEDNC2CmdDef FAR * m_pCL;
		GEDNC2CmdDef FAR * m_pItem;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Tool Offset Lookup
		LPCTXT	m_pTool;

		// Latest Nak
		DWORD	m_dNak;
	
		// Read Handler
		CCODE	DoRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handler
		CCODE	DoWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Process
		void	SelectProcess(BOOL fIsWrite);
		BOOL	Start(BOOL fIsWrite);
		BOOL	InitiateLink(void);
		BOOL	SendDatagram(UINT uState);
		void	SendDLE(BYTE b0or1);
		BOOL	SendMOK(UINT uState);
		BOOL	SendTNB(UINT uState);
		BOOL	SendTFD(UINT uState);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddDecWord(UINT uData, UINT uSize);
		void	AddHexWord(UINT uData);
		void	AddCommand(BOOL fIsWrite);
		void	AddParam( char * cParam );
		void	PutText(LPCTXT pCmd);
		void	MakeReadDatagram(AREF Addr);
		UINT	MakeAxisPattern(UINT uAxisNumber);
		void	AddAxisPattern(UINT uAxisNumber);
		BOOL	StartWrite(void);
		BOOL	MakeWriteDatagram(AREF Addr, PDWORD pData, UINT uCount);
		void	MakeCNCParWrite(DWORD dData);
		void	MakePitchErrWrite(UINT uOffset, DWORD dData);
		void	MakeToolOffWrite(DWORD dData);
		void	MakeCustomVarWrite(UINT uOffset, DWORD dData);
		void	MakeOperatorMessage(UINT uOffset, PDWORD pData, UINT uCount);

		// Transport Layer
		BOOL	Transact(UINT uState);
		void	Send(void);
		BOOL	GetReply(UINT uState);
		BOOL	CheckReply(UINT uState);
		
		// Port Access
		void	PutByte(BYTE bData);
		void	PutFrame(void);
		WORD	Get(UINT uTime);

		// Helpers
		void	SetpItem(UINT uID);
		DWORD	GetData(UINT uType, UINT uPos);
		DWORD	GetNumber(UINT uPos);
		DWORD	GetReal(UINT uPos);
		UINT	FindRcvPos(void);
		UINT	GetToolPos(void);
		UINT	GetLifePos(void);
		UINT	GetModalPos(void);
		BOOL	IsModalCmd(void);
		DWORD	GetModalData(UINT uType);
		UINT	FindKey(BYTE bKey);
		BOOL	ReadNoTransmit(PDWORD pData, UINT uCount);
		BOOL	WriteNoTransmit(PDWORD pData, UINT uCount);
		void	DoCNCPar(AREF Addr);
		void	DoToolOff(void);
		void	DoToolLife(UINT uOffset);
		void	DoModal(UINT uOffset);
		UINT	GetDefaultRadix(void);
		UINT	GetPosition(void);
		void	GetP1Size(PDWORD pType, BOOL fIsWrite);
		void	SetNakCode(void);
	};

// Special return value
#define	RTNZ	409600000

// Address Types
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// First Entry Types - Transmit Command Types
#define	TRT	0 // Transmit
#define	TRR	1 // Receive
#define	TRPT	2 // Prepare to Send
#define	TRPR	3 // Prepare to Receive
#define	TRM	4 // Operation Mode
#define	TRPRW	5 // PT for Read, PR for Write
#define	TRC	9 // Cached

// Process States
#define	PROCRD	0
#define	PROCRD1	1
#define	PROCRD2	2
#define	PROCPW	10
#define	PROCPW1	11
#define	PROCPW2	12
#define	PROCPW3	13
#define	PROCPR	20
#define	PROCPR1	21
#define	PROCPR2	22
#define	PROCPR3	23
#define	PROCPR4	24
#define	PROCEND	99 // process done

// Receive States
#define	RSTATE0	0 // First char received
#define	RECVDLE	1 // Expect DLE
#define	RECVD	2 // Wait for Data
#define	RECVD1	3 // DLE'd Data
#define	RECVPRA	4 // Wait for 'M'
#define	RECVPRB	5 // Wait for ' '
#define	RECVPRC	6 // Wait for 'P' or 'R'
#define	RECVP4	7 // End, Wait for BCC
#define	RECVT0	8 // Wait for 'T'
#define	RECVT1	9 // Wait for ' '
#define	RECVT2	10 // Wait for 'N' or 'F'
#define	RECVM	11 // Wait for 'M'
#define	RECVM1	12 // Wait for ' '
#define	RECVM2	13 // Wait for 'O'
#define	RECVM3	14 // Wait for 'K'
#define	RECVBCC	20 // Wait for BCC
#define	RECVE	21 // Expect EOT
#define	RECVPN	30 // Nak received
#define	RECVERR	98 // Check Nak

// Data Types
#define	TYP00	0 // no type needed
#define	TYPUI	1 // unsigned integer
#define	TYPSI	2 // signed integer
#define	TYPCH	3 // character
#define	TYPUR	4 // unsigned real
#define	TYPSR	5 // signed real
#define	TYPHX	6 // hex number
#define	TYPBB	7 // bit pattern

// Space ID's
#define	RMP	 1 // Tool Position - Machine
#define	RWP	 2 // Tool Position - Absolute
#define	RSP	 3 // Tool Position - Skip Signal Detect
#define	RSE	 4 // Servo Delay for Axis
#define	RAE	 5 // Accel / Decel Delay for Axis
#define	RMI	 6 // Machine Interface Signals
#define	RPN	 7 // Current Program Number
#define	RSN	 8 // Current Sequence Number
#define	RPAP	 9 // Read CNC Parameter Value
#define	RPAA	10 // CNC Parameter Axis Exponent
#define	WPA	11 // Execute CNC Parameter Write
#define	WPAP	12 // CNC Parameter Value to Write
#define	WPAA	13 // CNC Axis Exponent to Write
#define	RWPE	14 // Pitch Error Compensation Value
#define	RTOD	15 // Tool Offset Cutter Wear
#define	RTOK	16 // Tool Offset Cutter Geometry
#define	RTOH	17 // Tool Offset Length Wear
#define	RTOL	18 // Tool Offset Length Geometry
#define	RTOX	19 // Tool Offset X Axis Wear
#define	RTOU	20 // Tool Offset X Axis Geometry
#define	RTOY	21 // Tool Offset Y Axis Wear
#define	RTOV	22 // Tool Offset Y Axis Geometry
#define	RTOZ	23 // Tool Offset Z Axis Wear
#define	RTOW	24 // Tool Offset Z Axis Geometry
#define	RTOR	25 // Tool Offset Tip Radius Wear
#define	RTOP	26 // Tool Offset Tip Radius Geometry
#define	RTOQ	27 // Tool Offset Virtual Direction
#define	WTON	28 // Tool Offset Number
#define	WTOB	29 // Select Bit Pattern VPWUQYRZXLHKD
#define	WTOD	30 // Tool Offset Cutter Wear
#define	WTOK	31 // Tool Offset Cutter Geometry
#define	WTOH	32 // Tool Offset Length Wear
#define	WTOL	33 // Tool Offset Length Geometry
#define	WTOX	34 // Tool Offset X Axis Wear
#define	WTOU	35 // Tool Offset X Axis Geometry
#define	WTOY	36 // Tool Offset Y Axis Wear
#define	WTOV	37 // Tool Offset Y Axis Geometry
#define	WTOZ	38 // Tool Offset Z Axis Wear
#define	WTOW	39 // Tool Offset Z Axis Geometry
#define	WTOR	40 // Tool Offset Tip Radius Wear
#define	WTOP	41 // Tool Offset Tip Radius Geometry
#define	WTOQ	42 // Tool Offset Virtual Direction
#define	RWMV	43 // Custom Macro Variable Value
#define	RTLL	44 // Tool Life Value
#define	RTLQ	45 // Tool Life Count
#define	RTLT	46 // Tool Number
#define	RTLH	47 // Tool Life H Code
#define	RTLD	48 // Tool Life D Code
#define	RTLC	49 // Tool Information
#define	RMDG	50 // Modal Information G
#define	RMDD	51 // Modal Information D
#define	RMDE	52 // Modal Information E
#define	RMDH	53 // Modal Information H
#define	RMDL	54 // Modal Information L
#define	RMDM	55 // Modal Information M
#define	RMDN	56 // Modal Information N
#define	RMDO	57 // Modal Information O
#define	RMDS	58 // Modal Information S
#define	RMDT	59 // Modal Information T
#define	RMDF	60 // Modal Information F
#define	RAF	61 // Feed Rate for Axis
#define	RADI	62 // Analog Input - General
#define	RADS	63 // Spindle Voltage for Axis
#define	RADN	64 // NC Control Voltage for Axis
#define	RAL	65 // Alarm Information
#define	RST	66 // Status Information
#define	WDI	67 // Send Operator Message
#define	WDIS	68 // Set Up Operator Message
#define	WSL	69 // Select Part Program
#define	WCS	70 // Execute a Program
#define	WCC	71 // Reset
#define	RID	72 // System ID
#define	ERR	99 // Latest NAK

// End of File
