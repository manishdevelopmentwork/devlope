
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CloudTagSet_HPP

#define	INCLUDE_CloudTagSet_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CloudDataSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cloud Tag Set
//

class CCloudTagSet : public CCloudDataSet
{
public:
	// Constructor
	CCloudTagSet(UINT uSet);

	// Destructor
	~CCloudTagSet(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Attributes
	BOOL    HasLabel(void) const;
	CString GetLabel(BOOL fLower) const;
	BOOL    HasWrites(void) const;

	// Operations
	void Init(void);
	void AddRewrite(char cFind, char cRepl);
	UINT FindTag(CString Name);
	BOOL SetTagData(CString Name, CString Data);
	void ResetHistory(void);
	void DataWasSent(void);
	BOOL GetTagNames(CStringArray &List);

	// Json-Based Access
	BOOL GetJson(CMqttJsonData *pRep, CMqttJsonData *pDes, CString Ident, UINT64 uTime, UINT uRoot, UINT uCode, UINT uMode);

	// List-Based Access
	UINT GetCount(void);
	UINT GetProps(void);
	BOOL GetData(UINT uIndex, CString &Name, DWORD &Data, UINT &Type, BOOL &Free, UINT uMode);
	BOOL GetProp(UINT uIndex, UINT uProp, CString &Name, DWORD &Data, UINT &Type);
	BOOL SetData(UINT uIndex, DWORD Data, UINT Type);

	// Data Members
	UINT    m_Label;
	UINT	m_Name;
	UINT    m_Tree;
	UINT	m_Array;
	UINT    m_Props;
	UINT	m_Write;
	UINT    m_uTags;
	DWORD * m_pTags;

protected:
	// Rewrite Rule
	struct CRewrite
	{
		char m_cFind;
		char m_cRepl;
	};

	// Tag Info
	struct CTagInfo
	{
		CDataRef   m_Ref;
		CTag     * m_pTag;
		CString    m_Label;
		UINT	   m_Type;
		DWORD	   m_Data;
		DWORD	   m_Prev;
		BOOL       m_fWipe;
		BOOL	   m_fSent;
		BOOL       m_fInit;
	};

	// Tag Map
	CMap <CString, CTagInfo *> m_TagMap;

	// Rewrites
	CArray <CRewrite> m_Rewrite;

	// Data Members
	IMutex   * m_pLock;
	UINT       m_uSet;
	CTagList * m_pSrc;
	CTagInfo * m_pInfo;

	// Implementation
	BOOL LoadTagList(PCBYTE &pData);
	BOOL InitTagInfo(void);
	void SortTagList(void);
	void AddToStack(CArray <CMqttJsonData *> &Stack, UINT &uDepth, CString const &Name);

	// Tag Name Access
	static CString GetTagName(CTag *pTag, UINT uPos, UINT uName);
	static void    Normalize(CString &Name, CUnicode Base, bool fPath);

	// Sorting Mode
	static BOOL volatile m_fSortBusy;
	static UINT volatile m_uSortName;

	// Sorting Function
	static int SortTags(void const *p1, void const *p2);
};

// End of File

#endif
