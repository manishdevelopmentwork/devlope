
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUITextPattern;
class CUIPattern;
class CPatternComboBox;

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Pattern
//

class CUITextPattern : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextPattern(void);

	protected:
		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(UINT Data, CString Text);
		void AddData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Pattern
//

class CUIPattern : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIPattern(void);

	protected:
		// Data Members
		CString	           m_Label;
		CLayItemText     * m_pTextLayout;
		CLayItem         * m_pDataLayout;
		CStatic	         * m_pTextCtrl;
		CPatternComboBox * m_pDataCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Implementation
		CRect FindComboRect(void);
		void  LoadList(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Pattern Combo Box
//

class CPatternComboBox : public CDropComboBox
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPatternComboBox(CUIElement *pUI);

		// Operations
		void LoadList(void);
		void SetFore(CColor const &Fore);
		void SetBack(CColor const &Back);

	protected:
		// Data Members
		CColor m_Fore;
		CColor m_Back;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item);
		void OnChar(UINT uCode, DWORD dwFlags);

		// Implementation
		PCVOID GetData(UINT n);
		void   AddData(UINT Data, CString Text);

		// Shader Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL Shader1(IGDI *pGDI, int p, int c);
		static BOOL Shader2(IGDI *pGDI, int p, int c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Pattern
//

// Dynamic Class

AfxImplementDynamicClass(CUITextPattern, CUITextEnum);

// Constructor

CUITextPattern::CUITextPattern(void)
{
	}

// Overridables

void CUITextPattern::OnBind(void)
{
	CUITextEnum::OnBind();
	
	AddData();
	}

// Implementation

void CUITextPattern::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

void CUITextPattern::AddData(void)
{
	AddData(brushNull,   CString(IDS_PUI_FILL_NO));
	AddData(brushFore,   CString(IDS_PUI_WHITE));
	AddData(brushGray25, CString(IDS_PUI_GRAY_25));
	AddData(brushGray50, CString(IDS_PUI_GRAY_50));
	AddData(brushGray75, CString(IDS_PUI_GRAY_75));
	AddData(brushHatchF, CString(IDS_PUI_H_FOR));
	AddData(brushHatchB, CString(IDS_PUI_H_BACK));
	AddData(brushHatchH, CString(IDS_PUI_H_HOR));
	AddData(brushHatchV, CString(IDS_PUI_H_VER));
	AddData(brushHatchX, CString(IDS_PUI_H_ORTH));
	AddData(brushHatchD, CString(IDS_PUI_H_DIAG));
	AddData(brushWave,   CString(IDS_PUI_WAVE));

	AddData(16, CString(IDS_GRADUATED_FILL_1));
	AddData(17, CString(IDS_GRADUATED_FILL_2));
	AddData(18, CString(IDS_GRADUATED_FILL_3));
	AddData(19, CString(IDS_GRADUATED_FILL_4));
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Pattern
//

// Dynamic Class

AfxImplementDynamicClass(CUIPattern, CUIControl);

// Constructor

CUIPattern::CUIPattern(void)
{
	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CPatternComboBox(this);
	}

// Core Overridables

void CUIPattern::OnBind(void)
{
	m_Label = GetLabel() + L':';

	CUIElement::OnBind();
	}

void CUIPattern::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayDropDown(m_pText, TRUE);

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem( New CLayFormPad (m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIPattern::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP         |
			     WS_VSCROLL         |
			     CBS_DROPDOWNLIST   |
			     CBS_OWNERDRAWFIXED |
			     CBS_HASSTRINGS,
			     FindComboRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, CBN_SELCHANGE);

	LoadList();
	}

void CUIPattern::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(FindComboRect(), TRUE);
	}

// Data Overridables

void CUIPattern::OnLoad(void)
{
	CPrimBrush *pBrush = (CPrimBrush *) m_pItem;

	m_pDataCtrl->SetFore(C3GetWinColor(pBrush->m_pColor1->GetColor()));

	m_pDataCtrl->SetBack(C3GetWinColor(pBrush->m_pColor2->GetColor()));

	m_pDataCtrl->SelectStringExact(m_pText->GetAsText());
	}

UINT CUIPattern::OnSave(BOOL fUI)
{
	CString Text = m_pDataCtrl->GetWindowText();

	Text.TrimLeft();

	return StdSave(fUI, Text);
	}

// Implementation

CRect CUIPattern::FindComboRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.top    = Rect.top  - 1;

	Rect.bottom = Rect.top  + 8 * Rect.cy();

	return Rect;
	}

void CUIPattern::LoadList(void)
{
	m_pDataCtrl->LoadList();
	}

//////////////////////////////////////////////////////////////////////////
//
// Pattern Combo Box
//

// Dynamic Class

AfxImplementRuntimeClass(CPatternComboBox, CDropComboBox);

// Static Data

BOOL  CPatternComboBox::m_ShadeMode = 0;

COLOR CPatternComboBox::m_ShadeCol1 = 0;

COLOR CPatternComboBox::m_ShadeCol2 = 0;

// Constructor

CPatternComboBox::CPatternComboBox(CUIElement *pUI) : CDropComboBox(pUI)
{
	m_Fore = afxColor(3dDkShadow);

	m_Back = afxColor(3dFace);
	}

// Operations

void CPatternComboBox::LoadList(void)
{
	SetRedraw(FALSE);

	SendMessage(CB_SETMINVISIBLE, 1);

	AddData(brushNull, CString(IDS_PUI_FILL_NO));
	AddData(brushFore, CString(IDS_PUI_WHITE));

	AddData(16, CString(IDS_GRADUATED_FILL_1));
	AddData(17, CString(IDS_GRADUATED_FILL_2));
	AddData(18, CString(IDS_GRADUATED_FILL_3));
	AddData(19, CString(IDS_GRADUATED_FILL_4));

	AddData(brushGray25, CString(IDS_PUI_GRAY_25));
	AddData(brushGray50, CString(IDS_PUI_GRAY_50));
	AddData(brushGray75, CString(IDS_PUI_GRAY_75));
	AddData(brushHatchF, CString(IDS_PUI_H_FOR));
	AddData(brushHatchB, CString(IDS_PUI_H_BACK));
	AddData(brushHatchH, CString(IDS_PUI_H_HOR));
	AddData(brushHatchV, CString(IDS_PUI_H_VER));
	AddData(brushHatchX, CString(IDS_PUI_H_ORTH));
	AddData(brushHatchD, CString(IDS_PUI_H_DIAG));
	AddData(brushWave,   CString(IDS_PUI_WAVE));

	SendMessage(CB_SETMINVISIBLE, 8);

	SetRedraw(TRUE);
	}

void CPatternComboBox::SetFore(CColor const &Fore)
{
	m_Fore = Fore;

	Invalidate(TRUE);
	}

void CPatternComboBox::SetBack(CColor const &Back)
{
	m_Back = Back;

	Invalidate(TRUE);
	}

// Message Map

AfxMessageMap(CPatternComboBox, CDropComboBox)
{
	AfxDispatchMessage(WM_MEASUREITEM)
	AfxDispatchMessage(WM_DRAWITEM)

	AfxMessageEnd(CPatternComboBox)
	};

// Message Handlers

void CPatternComboBox::OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Item)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	Item.itemHeight = DC.GetTextExtent(L"ABC").cy + 2;

	DC.Deselect();
	}

void CPatternComboBox::OnDrawItem(UINT uID, DRAWITEMSTRUCT &Item)
{
	CDC   DC(Item.hDC);

	CRect Rect = Item.rcItem;

	CRect Fill = Rect;

	Fill.right = Rect.left += 16;

	if( Item.itemState & ODS_DISABLED ) {

		DC.SetTextColor(afxColor(3dShadow));

		DC.SetBkColor(afxColor(3dFace));

		DC.FillRect(--Fill, afxBrush(3dShadow));
		}
	else {
		DC.SetBkMode(OPAQUE);

		DC.SetTextColor(m_Back);

		DC.SetBkColor(m_Fore);

		DC.FrameRect(--Fill, afxBrush(WHITE));

		if( Item.itemData == 0x00 ) {

			DC.FrameRect(--Fill, afxBrush(BLACK));
			}

		if( Item.itemData >= 0x01 && Item.itemData <= 0x0F ) {

			HBITMAP h1 = CreateBitmap(8, 8, 1, 1, GetData(Item.itemData));

			HBRUSH  h2 = CreatePatternBrush(h1);

			SetBrushOrgEx(DC, Fill.left, Fill.top, NULL);

			DC.FillRect(Fill, CBrush::FromHandle(h2));

			DeleteObject(h2);

			DeleteObject(h1);
			}

		if( Item.itemData >= 0x10 && Item.itemData <= 0x13 ) {

			IGdiWindows *pWin = Create_GdiWindowsA888(Fill.cx(), Fill.cy());

			IGdi        *pGdi = pWin->GetGdi();

			m_ShadeMode = Item.itemData % 4 / 2;

			m_ShadeCol1 = C3GetGdiColor(m_Fore);

			m_ShadeCol2 = C3GetGdiColor(m_Back);
			
			PSHADER pShader = (Item.itemData % 2) ? Shader1 : Shader2;

			pGdi->ShadeRect(0, 0, Fill.cx(), Fill.cy(), pShader);

			DC.BitBlt(Fill, CDC::FromHandle(pWin->GetDC()), CPoint(0, 0), SRCCOPY);

			pGdi->Release();
			}

		if( Item.itemState & ODS_SELECTED ) {

			DC.SetTextColor(afxColor(SelectText));

			DC.SetBkColor(afxColor(SelectBack));
			}
		else {
			DC.SetTextColor(afxColor(WindowText));

			DC.SetBkColor(afxColor(WindowBack));
			}
		}

	if( Item.itemID < NOTHING ) {

		CString Text = GetLBText(Item.itemID);

		DC.ExtTextOut( Rect.GetTopLeft() + CPoint(3, 1),
			       ETO_OPAQUE | ETO_CLIPPED,
			       Rect, Text
			       );
		}

	if( Item.itemState & ODS_FOCUS ) {

		DC.DrawFocusRect(Rect);
		}
	}

// Implementation

PCVOID CPatternComboBox::GetData(UINT n)
{
	static WORD const wBrush[][8] = {

		{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 },
		{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF },
		{ 0xAA, 0x00, 0xAA, 0x00, 0xAA, 0x00, 0xAA, 0x00 },
		{ 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55, 0xAA, 0x55 },
		{ 0xFF, 0x55, 0xFF, 0x55, 0xFF, 0x55, 0xFF, 0x55 },
		{ 0x11, 0x22, 0x44, 0x88, 0x11, 0x22, 0x44, 0x88 },
		{ 0x88, 0x44, 0x22, 0x11, 0x88, 0x44, 0x22, 0x11 },
		{ 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0xFF, 0x00 },
		{ 0x44, 0x44, 0x44, 0x44, 0x44, 0x44, 0x44, 0x44 },
		{ 0x44, 0x44, 0xFF, 0x44, 0x44, 0x44, 0xFF, 0x44 },
		{ 0x81, 0x42, 0x24, 0x18, 0x18, 0x24, 0x42, 0x81 },
		{ 0x18, 0x24, 0x42, 0x81, 0x18, 0x24, 0x42, 0x81 },

		};

	return wBrush[n ? n-1 : 0];
	}

void CPatternComboBox::AddData(UINT Data, CString Text)
{
	AddString(Text, DWORD(Data));
	}

// Color Mixing

COLOR CPatternComboBox::Mix(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return GetRGB(rc, gc, bc);
	}

// Shaders

BOOL CPatternComboBox::Shader1(IGDI *pGDI, int p, int c)
{
	// TODO -- We could do this in 32-bits but is it worth it?

	static COLOR q = 0;

	if( c ) {

		COLOR k = Mix(m_ShadeCol1, m_ShadeCol2, p, c);

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = COL_INVALID;

	return m_ShadeMode;
	}

BOOL CPatternComboBox::Shader2(IGDI *pGDI, int p, int c)
{
	// TODO -- We could do this in 32-bits but is it worth it?

	static COLOR q = 0;

	if( c ) {

		COLOR k = Mix( m_ShadeCol2,
			       m_ShadeCol1,
			       abs(c/2-p),
			       c/2
			       );

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = COL_INVALID;

	return m_ShadeMode;
	}

// End of File
