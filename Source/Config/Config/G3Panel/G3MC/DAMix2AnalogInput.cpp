
#include "intern.hpp"

#include "DAMix2AnalogInput.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/manticore/dauin6props.h"

#include "import/manticore/dauin6dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI
//

// Runtime Class

AfxImplementRuntimeClass(CDAMix2AnalogInput, CCommsItem);

// Property List

CCommsList const CDAMix2AnalogInput::m_CommsList[] = {

	{ 1, "PV1",		PROPID_PV1,	    usageRead,  IDS_NAME_PV1 },
	{ 1, "PV2",		PROPID_PV2,	    usageRead,  IDS_NAME_PV2 },

	{ 1, "ColdJunc1",	PROPID_COLD_JUNC1,  usageRead,  IDS_NAME_CJ1 },
	{ 1, "ColdJunc2",	PROPID_COLD_JUNC2,  usageRead,  IDS_NAME_CJ2 },

	{ 1, "InputAlarm1",     PROPID_ALARM1,	    usageRead,  IDS_NAME_IA1 },
	{ 1, "InputAlarm2",     PROPID_ALARM2,	    usageRead,  IDS_NAME_IA2 },

};

// Constructor

CDAMix2AnalogInput::CDAMix2AnalogInput(void)
{
	m_InputAlarm1 = 0;
	m_InputAlarm2 = 0;

	m_PV1         = 0;
	m_PV2         = 0;

	m_uCommsCount = elements(m_CommsList);

	m_pCommsData  = m_CommsList;

	CheckCommsData();
}

// Group Names

CString CDAMix2AnalogInput::GetGroupName(WORD Group)
{
	switch( Group ) {

		case 1:	return L"Status";
	}

	return CCommsItem::GetGroupName(Group);
}

// Meta Data Creation

void CDAMix2AnalogInput::AddMetaData(void)
{
	Meta_AddInteger(InputAlarm1);
	Meta_AddInteger(InputAlarm2);

	Meta_AddInteger(PV1);
	Meta_AddInteger(PV2);

	CCommsItem::AddMetaData();
}


// End of File
