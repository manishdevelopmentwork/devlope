
#include "intern.hpp"

#include "ZoneSync.hpp"

#include <math.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Time Zone Sync
//

// API Key

#define API_KEY "9GSJ6W6JNMUA"

// Constructor

CZoneSync::CZoneSync(CJsonConfig *pJson, UINT uFaces)
{
	m_pZone   = NULL;

	m_uMode   = 0;

	m_fMobile = FALSE;

	m_uDMode  = 0;

	m_uCell   = 0;

	m_offset  = 0;

	m_dst     = FALSE;

	ApplyConfig(pJson);

	AfxGetObject("dev.tz", 0, ITimeZone, m_pZone);

	StdSetRef();
}

// Destructor

CZoneSync::~CZoneSync(void)
{
}

// IUnknown

HRESULT CZoneSync::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CZoneSync::AddRef(void)
{
	StdAddRef();
}

ULONG CZoneSync::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CZoneSync::TaskInit(UINT uTask)
{
	SetThreadName("ZoneSync");

	if( m_pZone ) {

		if( m_uMode ) {

			if( !m_uDMode ) {

				if( m_pZone->Lock(1) ) {

					m_pZone->SetDst(m_dst);

					m_pZone->Lock(0);
				}
			}

			return TRUE;
		}

		if( m_pZone->Lock(1) ) {

			m_pZone->SetOffset(m_offset);

			m_pZone->SetDst(m_dst);

			m_pZone->Lock(0);
		}
	}

	Release();

	return FALSE;
}

INT CZoneSync::TaskExec(UINT uTask)
{
	CAutoPointer<CHttpClientManager> pManager(New CHttpClientManager);

	if( pManager->Open() ) {

		CHttpClientConnectionOptions Opts;

		Opts.m_fTls   = TRUE;

		Opts.m_uPort  = 443;

		Opts.m_uCheck = tlsCheckNone;

		bool init     = true;

		for( ;;) {

			time_t next = m_pZone->GetNext();

			if( init || !next ) {

				Sleep(10 * 1000);
			}
			else {
				time_t wait = next;

				if( m_fMobile ) {

					MakeMin(wait, time(NULL) + 60);
				}

				do {
					Sleep(1000);

				} while( time(NULL) < wait );
			}

			CString args;

			if( m_uMode == 1 || m_uMode == 4 ) {

				AfxGetAutoObject(pTime, "net.time", 0, ITimeSource);

				if( pTime ) {

					CTimeSourceInfo Info;

					if( pTime->GetTimeData(Info) ) {

						if( Info.m_fValid ) {

							if( Info.m_Zone != 9999 ) {

								if( m_pZone->Lock(1) ) {

									m_offset = Info.m_Zone;

									m_pZone->SetOffset(m_offset);

									if( m_uDMode ) {

										if( Info.m_Dst != 9999 ) {

											m_dst = Info.m_Dst ? TRUE : FALSE;

											m_pZone->SetDst(m_dst);
										}
									}

									time_t next = time(NULL);

									next -= next % 3600;

									next += 3600;

									m_pZone->SetNext(next);

									m_pZone->Lock(0);

									AfxTrace("time zone set to %d %s from cell\n", m_offset, m_dst ? "DST" : "STD");
								}
								else {
									AfxTrace("time zone locked by crimson app\n");
								}

								init = false;

								continue;
							}
						}
					}
				}
			}

			if( (m_uMode == 1 || m_uMode == 2) && args.IsEmpty() ) {

				AfxGetAutoObject(pLoc, "c3.location", 0, ILocationSource);

				if( pLoc ) {

					CLocationSourceInfo Info;

					if( pLoc->GetLocationData(Info) ) {

						if( Info.m_uFix >= 2 ) {

							if( m_Info.m_uFix && GetDistance(m_Info, Info) >= 52800 ) {

								m_Info.m_uFix = 0;

								m_fMobile     = TRUE;
							}

							if( !m_Info.m_uFix ) {

								args.Printf("by=position&lat=%.6f&lng=%.6f", Info.m_Lat, Info.m_Long);

								m_Info = Info;
							}
						}
					}
				}
			}

			if( (m_uMode == 1 || m_uMode == 3) && args.IsEmpty() ) {

				args = "&by=ip";
			}

			if( !args.IsEmpty() ) {

				for( int n = 0; n < 4; n++ ) {

					if( true ) {

						CAutoPointer<CHttpClientConnection> pConnect(pManager->CreateConnection(Opts));

						if( pConnect ) {

							pConnect->SetServer("vip.timezonedb.com", "", "");

							CHttpClientRequest Req;

							Req.SetPath(CPrintf("/v2.1/get-time-zone?key=%s&format=json&%s", API_KEY, PCSTR(args)));

							Req.SetRequestBody("");

							Req.AddRequestHeader("Content-Type", "text/plain");

							Req.SetVerb("GET");

							if( Req.Transact(pConnect, 200) ) {

								CJsonData Data;

								if( Data.Parse(PCSTR(Req.GetReplyBody())) ) {

									if( Data.GetValue("status", "") == "OK" ) {

										if( m_pZone->Lock(1) ) {

											m_dst    = atoi(Data.GetValue("dst", "0")) ? TRUE : FALSE;

											m_offset = atoi(Data.GetValue("gmtOffset", "0")) / 60 - (m_dst ? 60 : 0);

											time_t n = atoi(Data.GetValue("zoneEnd", "0"));

											m_pZone->SetOffset(m_offset);

											if( m_uDMode ) {

												m_pZone->SetDst(m_dst);
											}

											m_pZone->SetNext(n);

											m_pZone->Lock(0);

											AfxTrace("time zone set to %d %s from web api\n", m_offset, m_dst ? "DST" : "STD");
										}
										else {
											AfxTrace("time zone locked by crimson app\n");
										}

										init = false;
									}
									else {
										AfxTrace("time zone web api call failed\n");
									}

									break;
								}
							}
						}
					}

					Sleep(30 * 1000);
				}
			}
		}
	}

	return 0;
}

void CZoneSync::TaskStop(UINT uTask)
{
}

void CZoneSync::TaskTerm(UINT uTask)
{
	Release();
}

// Implementation

void CZoneSync::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		if( !(m_uMode = pJson->GetValueAsUInt("mode", 1, 0, 4)) ) {

			int h = pJson->GetValueAsUInt("hours", 0, 0, 12);

			int m = pJson->GetValueAsUInt("mins", 0, 0, 59);

			int s = pJson->GetValueAsUInt("sign", 0, 0, 1);

			m_offset = (s ? -1 : +1) * (h * 60 + m);

			m_uMode = 0;
		}
		else {
			m_fMobile = pJson->GetValueAsBool("mobile", FALSE);

			m_uCell   = pJson->GetValueAsUInt("cell", 0, 0, 0);

			m_uDMode  = pJson->GetValueAsUInt("dmode", 1, 0, 1);
		}

		if( !m_uDMode ) {

			m_dst = pJson->GetValueAsBool("dst", false);
		}
	}
}

double CZoneSync::GetDistance(CLocationSourceInfo const &a, CLocationSourceInfo const &b)
{
	double base = 20903520;

	double dLat = ToRad(b.m_Lat  - a.m_Lat);

	double dLon = ToRad(b.m_Long - a.m_Long);

	double Lat1 = ToRad(a.m_Lat);

	double Lat2 = ToRad(b.m_Lat);

	double k1 = sin(dLat/2) * sin(dLat/2) + sin(dLon/2) * sin(dLon/2) * cos(Lat1) * cos(Lat2);

	double ds = base * 2 * atan2(sqrt(k1), sqrt(1-k1));

	return ds;
}

double CZoneSync::ToRad(double deg)
{
	return deg / 180.0 * 3.14159265;
}

// End of File
