
#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "honeyudc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CHoneywellUDC9000);

// Constructor

CHoneywellUDC9000::CHoneywellUDC9000(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CHoneywellUDC9000::~CHoneywellUDC9000(void)
{
	}

// Configuration

void MCALL CHoneywellUDC9000::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CHoneywellUDC9000::Attach(IPortObject *pPort)
{
	}

void MCALL CHoneywellUDC9000::Open(void)
{
	}

// Device

CCODE MCALL CHoneywellUDC9000::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= HostToMotor(GetLong(pData));
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			m_pCtx->m_uStatus	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CHoneywellUDC9000::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CHoneywellUDC9000::Ping(void)
{
	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( OpenSocket() ) return LoopBack();
		}

	return CCODE_ERROR;	
	}

CCODE MCALL CHoneywellUDC9000::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	if( Addr.a.m_Table == SPT ) {

		*pData = m_pCtx->m_uStatus;

		return 1;
		}

	StartFrame(IDREAD);

	switch( Addr.a.m_Type ) {
	
		case addrBitAsBit:
			return  DoBitRead(Addr, pData, min(uCount, 16) );

		case addrWordAsWord:
			return DoWordRead(Addr, pData, min(uCount, 16) );
			
		case addrLongAsLong:
		case addrRealAsReal:
			return DoLongRead(Addr, pData, min(uCount,  8) );
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CHoneywellUDC9000::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

//**/	AfxTrace0("\r\n***** WRITE *****");

	switch( Addr.a.m_Table ) {

		case SPM:
		case SPS:
		case SPT:
			return 1;
		}

	StartFrame(IDWRITE);

	switch( Addr.a.m_Type ) {
	
		case addrBitAsBit:
			return  DoBitWrite(Addr, pData, min(uCount, 16) );

		case addrWordAsWord:
			return DoWordWrite(Addr, pData, min(uCount, 16) );

		case addrLongAsLong:
		case addrRealAsReal:

			BOOL fCCC = Addr.a.m_Table == SPC || Addr.a.m_Table == SPB;

			UINT uCt  = fCCC ? 1 : min(uCount, 8); // scattered writes require adding addr+data

			CCODE Rtn = DoRealWrite(Addr, pData, uCt);

			return (fCCC && Rtn != 1) ? (CCODE_ERROR | CCODE_NO_RETRY) : Rtn;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Read Handlers

CCODE CHoneywellUDC9000::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	PWORD p;

	p = PWORD( &m_bRx[AddWordHeader( Addr, uCount )] );

	if( Transact() ) {

		if( m_bRx[RBSTAT] ) return CCODE_ERROR | CCODE_NO_RETRY;

		UINT n;

		switch( Addr.a.m_Table ) {

			case SPR:
			case SPIW:
				if( !m_bRx[RBQTY] ) {

					for( n = 0; n < uCount; n++ ) pData[n] = 0;

					return uCount;
					}

				uCount = m_bRx[RBQTY];

				break;

			case SPM:
			case SPS:
				uCount = 1;
				break;
			}
		
		for( n = 0; n < uCount; n++, p++ ) {
		
			WORD x   = *p;

			pData[n] = Addr.a.m_Table != SPIW ? IntelToHost(x) : MotorToHost(x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CHoneywellUDC9000::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	AddLongHeader( Addr, uCount );
	
	if( Transact() ) {

		if( m_bRx[RBSTAT] ) return CCODE_ERROR | CCODE_NO_RETRY;

		UINT n;

		if( !m_bRx[RBQTY] ) {

			for( n = 0; n < uCount; n++ ) pData[n] = 0;

			return uCount;
			}

		uCount = m_bRx[RBQTY];

		PDWORD p = PDWORD( &m_bRx[RDATA32] );

		for( n = 0; n < uCount; n++, p++ ) {
		
			DWORD x = *p;

			pData[n]  = IntelToHost(x);
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

CCODE CHoneywellUDC9000::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	AddBitsHeader(Addr, uCount);

	if( Transact() ) {

		if( m_bRx[RBSTAT] ) return CCODE_ERROR | CCODE_NO_RETRY;

		UINT n;

		if( !m_bRx[RBQTY] ) {

			for( n = 0; n < uCount; n++ ) pData[n] = 0;

			return uCount;
			}

		uCount = m_bRx[RBQTY];

		if( !uCount ) {

			pData[0] = 0;

			return 1;
			}

		for( n = 0; n < uCount; n++ ) {

			pData[n] = DWORD( (m_bRx[RDATAIO + n] & 1) ? TRUE : FALSE );
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CHoneywellUDC9000::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	AddWordHeader( Addr, uCount );

	for( UINT i = 0; i < uCount; i++ ) {

		UINT d = LOWORD(pData[i]);

		if( Addr.a.m_Table == SPIW ) {

			AddByte(HIBYTE(d));
			AddByte(LOBYTE(d));
			}

		else AddWord( d );
		}

	return Transact() ? uCount : CCODE_ERROR;
	}

CCODE CHoneywellUDC9000::DoRealWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	AddWordHeader( Addr, uCount );

	if( Addr.a.m_Table == SPC || Addr.a.m_Table == SPB ) AddLong(pData[0]);

	else {
		UINT uInt = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			char c[12];

			SPrintf( c, "%f", PassFloat(pData[n]) );

			uInt = ATOI(c);

			for( UINT k = 0; k < strlen(c); k++ ) {

				if( c[k] == '.' && c[k+1] >= '5' ) {

					uInt++;
					break;
					}
				}
			}

		AddWord( LOWORD(uInt) );
		}

	return Transact() ? uCount : CCODE_ERROR;
	}

CCODE CHoneywellUDC9000::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	AddBitsHeader(Addr, uCount);

	for( UINT i = 0; i < uCount; i++ ) AddByte( BOOL(pData[i]) ? 1 : 0 );

	return Transact() ? uCount : CCODE_ERROR;
	}

// Frame Building

void CHoneywellUDC9000::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(0); // 0 + MSB Length
	AddWord(0); // LSB Length + 0
	AddWord(0); // Byte 4 + 5
	AddByte(0); // Byte 6
	AddByte(bOpcode); // Identification
	}

void CHoneywellUDC9000::EndFrame(void)
{
	m_bTx[1] = HIBYTE(m_uPtr);
	m_bTx[2] = LOBYTE(m_uPtr);
	}

void CHoneywellUDC9000::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CHoneywellUDC9000::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CHoneywellUDC9000::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

// Transport Layer

BOOL CHoneywellUDC9000::SendFrame(void)
{
	EndFrame();

	UINT uSize = m_uPtr;

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k] );

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) return TRUE;
		}

	return FALSE;
	}

BOOL CHoneywellUDC9000::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/		AfxTrace0("\r\n"); for( UINT m = 0; m < uSize; m++ ) AfxTrace1("<%2.2x>", m_bRx[uPtr+m] );

			uPtr += uSize;

			if( uPtr > 3 ) {

				if( uPtr >= (WORD &)m_bRx[1] ) return TRUE;
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CHoneywellUDC9000::Transact(void)
{
	if( SendFrame() && RecvFrame() ) {

		m_pCtx->m_uStatus = IntelToHost( (WORD &)m_bRx[RBSTAT] );

		return TRUE;
		}

	CloseSocket(TRUE);

	return FALSE;
	}

// Helpers

CCODE CHoneywellUDC9000::LoopBack(void) // Connection
{
	StartFrame( IDLOOP );

	AddByte(LOOPBACKVALUE);

	return Transact() && (m_bRx[RLOOPB] == LOOPBACKVALUE) ? 1 : CCODE_ERROR;
	}

UINT CHoneywellUDC9000::AddWordHeader(AREF Addr, UINT uCount)
{
	UINT uPos = RDATA16;

	switch( Addr.a.m_Table ) {
	
		case SPR:
			AddByte(CREGS);
			AddByte(uCount);			
			AddWord(Addr.a.m_Offset);
			break;
	
		case SPIW:
			AddByte(CIOWORD);
			AddByte(uCount);			
			AddWord(Addr.a.m_Offset);
			break;
			
		case SPM:
			AddByte( CMODES );
			uPos = RDATAM;
			break;
			
		case SPS:
			AddByte( CSTATUS );
			uPos = RDATAS;
			break;

		case SPB:
		case SPC:
			AddByte(CSREAL);
			AddByte(1); // Count
			AddByte(0); // Alignment
			AddByte(Addr.a.m_Table == SPC ? 1 : 2); // ELPM CCC Config / ELPM CB I/O
			AddByte(Addr.a.m_Offset/100); // Control Block Number
			AddByte(Addr.a.m_Offset%100); // Control Block Parameter Number
			break;
		}

	return uPos;
	}

void CHoneywellUDC9000::AddLongHeader(AREF Addr, UINT uCount)
{
	AddByte(CLONG);

	AddByte( uCount );

	AddByte( 0 );

	switch( Addr.a.m_Table ) {

		case SPR:
			AddByte(0);
			AddByte(LOBYTE(Addr.a.m_Offset));
			AddByte(HIBYTE(Addr.a.m_Offset));
			break;

		case SPC:
		case SPB:
			AddByte(Addr.a.m_Table == SPC ? 1 : 2);
			AddByte(Addr.a.m_Offset/100);
			AddByte(Addr.a.m_Offset%100);
			break;
		}
	}

void CHoneywellUDC9000::AddBitsHeader(AREF Addr, UINT uCount)
{
	AddByte( CIOBIT );

	AddByte( uCount );

	AddWord( Addr.a.m_Offset );
	}

// Socket Management

BOOL CHoneywellUDC9000::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CHoneywellUDC9000::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CHoneywellUDC9000::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
