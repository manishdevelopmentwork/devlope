
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HBMAED_HPP
	
#define	INCLUDE_HBMAED_HPP

class	CHBMAEDDeviceOptions;
class	CHBMAEDDriver;

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

//////////////////////////////////////////////////////////////////////////
//
// HBM AED Device Options
//

class CHBMAEDDeviceOptions : CStdDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHBMAEDDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// HBM AED Comms Driver
//

class CHBMAEDDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CHBMAEDDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig();

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
