
//////////////////////////////////////////////////////////////////////////
//
// Shared Headers
//

#include "../MonSNMP/SnmpAgent.hpp"

#include "../MonSNMP/Asn1BerDecoder.hpp"

#include "../MonSNMP/Asn1BerEncoder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver v2.0
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved

class CMonicoSNMP2 : public CSlaveDriver, public ISnmpSource
{
	public:
		// Constructor
		CMonicoSNMP2(void);

		// Destructor
		~CMonicoSNMP2(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		DEFMETH(void) Open(void);

		// Entry Points
		DEFMETH(void) Service(void);
		
	protected:
		// Configuration
		UINT    m_Port1;
		UINT    m_Port2;
		char    m_sComm[64];
		UINT	m_AclAddr1;
		UINT	m_AclMask1;
		UINT	m_AclAddr2;
		UINT	m_AclMask2;
		UINT	m_TrapFrom;
		UINT	m_TrapAddr1;
		UINT	m_TrapMode1;
		UINT	m_TrapAddr2;
		UINT	m_TrapMode2;
		BOOL	m_fSysDefault;
		PTXT	m_pSysDesc;
		PTXT	m_pSysContact;
		PTXT	m_pSysName;
		PTXT	m_pSysLocation;

		// Data Members
		ISocket    * m_pSock;
		CSnmpAgent * m_pAgent;
		UINT         m_uTicks;
		UINT	     m_genData32;
		UINT	     m_genData16;
		UINT	     m_genTrap;
		UINT	     m_genNotify;
		UINT	     m_setData32;
		UINT	     m_setData16;
		UINT	     m_setTrap;
		UINT	     m_setNotify;
		BYTE	     m_Trap1[1000];
		BYTE	     m_Trap2[1000];
		UINT	     m_uBase1;
		UINT	     m_uBase2;

		// Extended OIDs

		struct CSet
		{
			UINT  m_uData;
			UINT  m_uTrap;
			UINT  m_uNotify;
			UINT  m_uTraps;
			PBYTE m_pTrap;
			UINT  m_uBase;
			UINT  m_uTable;
			};

		enum  {
			setCummins = 0,
			setGenerac = 1,
			setKohler  = 2,
			setMTU     = 3,
			setATS     = 4, 
			
			setTotal   = 5,
			};

		CSet * m_pOidSet[setTotal];
		UINT   m_uExtSet[setTotal];
		
		// Implementation
		bool CheckRequest(void);
		bool AllowIP(DWORD IP);
		void CheckTraps(void);
		void CheckTraps(PBYTE pHist, UINT &uBase, UINT uTable, UINT uCount, UINT uNotify, UINT uTrap);
		bool SendTrap(UINT &Mode, UINT &Addr, UINT uTable, UINT uPos, UINT uNotify, UINT uTrap);
		void FindMappedTraps(void);
		void FindMappedTraps(PBYTE pHist, UINT uTable, UINT uCount);
		UINT GetCode(UINT uTable);

		// Data Source
		bool IsSpace(COid const &Oid, UINT uTag, UINT uPos);
		bool GetData(CAsn1BerEncoder &rep, COid const &Oid, UINT uTag, UINT uPos);

		// Extended OID Support
		void AddExtendedOIDs(void);
		void FindExtendedMappedTraps(void);
		void CheckExtendedTraps(void);
		void AddCummins(void);
		void AddGenerac(void);
		void AddKohler(void);
		void AddMTU(void);
		void AddATS(void);
		void GetOid(char * pOid, char * pBase, UINT uOid, char * pSuffix, UINT uSuffix);
		void AddOidSet(UINT uIndex, UINT uTable, UINT uBase, UINT uSets, UINT * pData, UINT * pTraps);
		void InitOidSets(void);
		void KillOidSets(void);
		void KillOidSet(UINT uIndex);
		
	};

// End of File
