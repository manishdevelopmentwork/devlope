

#include "Intern.hpp"

#pragma  once

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Linux Support
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// VPN Interface
//

class CPeer : public CInterface
{
public:
	// Constructor
	CPeer(string const &face, string const &root);

	// Destructor
	~CPeer(void);

protected:
	// States
	enum
	{
		stateWaitPeer = 2,
		statePeerOkay = 3
	};
		

	// Data Members
	UINT   m_mode;
	string m_peer;

	// Overridables
	bool OnConfigure(void) override;
	int  OnExecute(void) override;
	void OnStopping(void) override;
	void OnNewState(void) override;
	bool OnCommand(string const &cmd) override;

	// Implementation
	bool   CheckPeer(bool good);
	string GetStateName(void);
	bool   WriteStatus(void);
};

// End of File
