
#include "Intern.hpp"

#include "LangManagerPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangItem.hpp"
#include "LangList.hpp"
#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Language Manager Page
//

// Runtime Class

AfxImplementRuntimeClass(CLangManagerPage, CUIPage);

// Constructor

CLangManagerPage::CLangManagerPage(CLangManager *pLang)
{
	m_pLang  = pLang;
	}

// Operations

BOOL CLangManagerPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	// REV3 -- Allow saving as default language set...

	pView->StartGroup(CString(IDS_AUTOTRANSLATION), 1);

	pView->AddUI(pItem, L"root", L"LexMode");

	pView->AddUI(pItem, L"root", L"Server");

	pView->EndGroup(TRUE);

	pView->StartGroup(CString(IDS_OPTIONS), 1);

	pView->AddUI(pItem, L"root", L"Global");

	pView->AddUI(pItem, L"root", L"LocSep");

	pView->EndGroup(TRUE);

	pView->StartTable(CString(IDS_LANGUAGES), 5);

	pView->AddColHead(CString(IDS_LANGUAGE));

	pView->AddColHead(CString(IDS_CODE));

	pView->AddColHead(CString(IDS_NUMERIC_FORMAT));

	pView->AddColHead(CString(IDS_DIACRITICAL_MARKS));

	pView->AddColHead(CString(IDS_SWITCH_KEYBOARD));

	UINT c = m_pLang->m_pLangs->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CItem *pItem = m_pLang->m_pLangs->GetItem(n);

		pView->AddRowHead(CPrintf(L"%2.2u", n));

		pView->AddUI(pItem, L"root", L"Lang");

		pView->AddUI(pItem, L"root", L"Code");

		pView->AddUI(pItem, L"root", L"Num");

		pView->AddUI(pItem, L"root", L"Upper");

		pView->AddUI(pItem, L"root", L"Keyb");
		}

	pView->EndTable();

	return TRUE;
	}

// End of File
