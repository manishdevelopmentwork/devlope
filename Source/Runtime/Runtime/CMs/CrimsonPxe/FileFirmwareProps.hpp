
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_FileFirmwareProps_HPP

#define INCLUDE_FileFirmwareProps_HPP

//////////////////////////////////////////////////////////////////////////
//
// Filing Systm Firmware Properties Object
//

class CFileFirmwareProps : public IFirmwareProps
{
public:
	// Constructor
	CFileFirmwareProps(CString const &Root);

	// Destructor
	~CFileFirmwareProps(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IFirmwareProps
	bool   METHOD IsCodeValid(void);
	PCBYTE METHOD GetCodeVersion(void);
	UINT   METHOD GetCodeSize(void);
	PCBYTE METHOD GetCodeData(void);

protected:
	// Data Members
	ULONG	  m_uRefs;
	CString	  m_Root;
	CGuid	  m_Guid;
};

// End of File

#endif
