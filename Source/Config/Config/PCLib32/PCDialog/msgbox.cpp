
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Message Box Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CMessageBox, CDialog);

// Constructor

CMessageBox::CMessageBox(PCTXT pText, PCTXT pTitle, UINT uStyle, PCTXT pDoNot)
{
	m_Text      = pText;
	
	m_Title     = pTitle ? CString(pTitle) : FindDefTitle(uStyle);

	m_DoNot     = pDoNot ? CString(pDoNot) : L"";

	m_pPattern  = FindPattern(uStyle);
	
	m_pIconName = FindIconName(uStyle);
	
	m_uAlert    = (uStyle & MB_ICONMASK) >> 0;
	
	m_uDefault  = (uStyle & MB_DEFMASK ) >> 8;

	m_Text.Remove('\r');
	}

// Dialog Operations

BOOL CMessageBox::Create(CWnd &Parent)
{
	DWORD dwStyle   = WS_POPUP | WS_BORDER | WS_DLGFRAME | WS_SYSMENU;

	DWORD dwExStyle = WS_EX_DLGMODALFRAME;

	return CWnd::Create(dwExStyle, dwStyle, CRect(), Parent, 0, NULL);
	}

UINT CMessageBox::Execute(CWnd &Parent)
{
	if( !m_DoNot.IsEmpty() ) {

		CRegKey Key = afxModule->GetApp()->GetUserRegKey();

		Key.MoveTo(L"PCDialog");

		Key.MoveTo(L"DoNotShow");

		UINT uCode = Key.GetValueAsDword(m_DoNot);

		if( uCode ) {

			return uCode;
			}
		}

	CWnd &Top   = GetActiveWindow();
	
	CWnd &Focus = GetFocus();
	
	if( Create(Parent) ) {

		SetWindowStyle(DS_MODALFRAME, DS_MODALFRAME);

		UINT uCode = ModalLoop(Top);
		
		Focus.SetFocus();

		DestroyWindow(TRUE);

		return uCode;
		}

	return 0;
	}

UINT CMessageBox::Execute(void)
{
	return CDialog::Execute();
	}

// Operations

void CMessageBox::ChangeIcon(ENTITY ID)
{
	m_Icon.Create(ID);
	}

// Message Map

AfxMessageMap(CMessageBox, CDialog)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	
	AfxMessageEnd(CMessageBox)
	};
	
// Message Handlers

UINT CMessageBox::OnCreate(CREATESTRUCT &Create)
{
	if( m_uAlert ) {

		if( m_uAlert == MB_ICONINFORMATION ) {

			PlaySound(L"notify.wav", NULL, SND_FILENAME | SND_ASYNC);
			}
		else
			MessageBeep(m_uAlert);
		}

	if( !m_Icon.IsValid() ) {

	        m_Icon.Create(m_pIconName);
		}

	SetWindowText(m_Title);

	m_uButtons = wstrlen(m_pPattern) - 1; //!m_Topic;
	
	FormatText();

	FindSizeInfo();

	MakeButtons();

	MakeDoNot();

	AdjustSize();

	PlaceDialogCentral();

	BuildSystemMenu();
	
	return 0;
	}

BOOL CMessageBox::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID == 100 ) {

		return TRUE;
		}

	if( uID == IDCANCEL ) {

		UINT uIDs[] = { IDCANCEL, IDNO, IDOK };

		for( UINT uPos = 0; uPos < elements(uIDs); uPos++ ) {

			CWnd &Button = GetDlgItem(uIDs[uPos]);

			if( Button.GetHandle() ) {
			
				if( Button.IsEnabled() ) {

					SaveDoNot(uIDs[uPos]);
			
					EndDialog(uIDs[uPos]);

					return TRUE;
					}

				break;
				}
			}

		MessageBeep(0);

		return TRUE;
		}

	SaveDoNot(uID);

	EndDialog(uID);

	return TRUE;
	}

void CMessageBox::OnClose(void)
{
	SendMessage(WM_COMMAND, IDCANCEL);
	}

void CMessageBox::OnPaint(void)
{
	Paint(CPaintDC(ThisObject));
	}

void CMessageBox::OnSetFocus(CWnd &Wnd)
{
	SetDlgFocus(m_uDefID);
	}

// Tooltip Control

BOOL CMessageBox::EnableTips(void)
{
	return FALSE;
	}

// Paint Helper

void CMessageBox::Paint(CDC &DC)
{
	DC.DrawIcon(m_IconOrg, m_Icon);

	DC.Select(afxFont(Dialog));

	int nPos = m_TextOrg.y;

	if( afxWin2K ) {

		DC.SetBkColor(afxColor(3dFace));
		}
	else
		DC.SetBkColor(afxColor(TabFace));

	for( UINT uScan = 0; uScan < m_uLines; uScan++ ) {
		
		if( m_Line[uScan].GetLength() ) {
	
			DC.TextOut(m_TextOrg.x, nPos, m_Line[uScan]);
			
			nPos += m_FullSize.cy;

			continue;
			}

		nPos += m_FullSize.cy / 2;
		}
		
	DC.Deselect();
	}

// Implementation

PCTXT CMessageBox::FindPattern(UINT uStyle)
{
	switch( uStyle & MB_TYPEMASK ) {
	
		case MB_OK:			return L"OH";
		case MB_OKCANCEL:		return L"OCH";
		case MB_ABORTRETRYIGNORE:	return L"ARIH";
		case MB_YESNOCANCEL:		return L"YNCH";
		case MB_YESNOALLCANCEL:		return L"YLNCH";
		case MB_YESNO:			return L"YNH";
		case MB_YESNOALL:		return L"YLNH";
		case MB_RETRYCANCEL:		return L"RCH";

		}

	AfxAssert(FALSE);

	return NULL;
	}
	
PCTXT CMessageBox::FindIconName(UINT uStyle)
{
	switch( uStyle & MB_ICONMASK ) {
	        
		case MB_ICONHAND:		return IDI_HAND;
		case MB_ICONQUESTION:		return IDI_QUESTION;
		case MB_ICONEXCLAMATION:	return IDI_EXCLAMATION;
		case MB_ICONINFORMATION:	return IDI_INFORMATION;

		}
	
	return NULL;
	}

CString CMessageBox::FindDefTitle(UINT uStyle)
{
	if( afxMainWnd ) {

		CString Text(IDS_APP_CAPTION);

		return Text;
		}

	switch( uStyle & MB_ICONMASK ) {
	        
		case MB_ICONHAND:		return IDS_MSGBOX_ERROR;
		case MB_ICONQUESTION:		return IDS_MSGBOX_QUESTION;
		case MB_ICONEXCLAMATION:	return IDS_MSGBOX_ERROR;
		case MB_ICONINFORMATION:	return IDS_MSGBOX_INFO;

		}
	
	return IDS_MSGBOX_ERROR;
	}

void CMessageBox::FormatText(void)
{
	TCHAR sWork[140];
	
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Dialog));

	PCTXT pText = m_Text;
	
	int xTotal = DC.GetTextExtent(pText).cx;
	
	int nChars = elements(sWork) / 2;
	
	int nLines = xTotal / (nChars * m_FullSize.cx);
	
	int xLimit = xTotal / (1 + nLines);
	
	int nClick = 0, nIndex = 0, nCount = 0;
	
	m_uLines = 0;

	for(;;) {
	
		BOOL  fClip = FALSE;
	
		TCHAR cChar = pText[nIndex++];

		switch( cChar ) {

			case 0x0A:
			case 0x00:
				
				fClip = TRUE;
				
				break;

			case 0x20:
				
				fClip = (DC.GetTextExtent(sWork, nCount).cx > xLimit);
				
				break;
			}
			
		if( fClip ) {
		
			sWork[nCount] = 0;
			
			int xSize = DC.GetTextExtent(sWork, nCount).cx;
			
			MakeMax(m_TextSize.cx, xSize);
				
			m_Line[m_uLines++] = sWork;
			
			AfxAssert(m_uLines < elements(m_Line));

			nClick = nClick + (nCount ? 2 : 1);
			
			nCount = 0;
			}
		else
			sWork[nCount++] = cChar;
		
		if( !cChar ) {
			
			break;
			}
		}
		
	m_TextSize.cy = nClick * m_FullSize.cy / 2;

	if( !m_DoNot.IsEmpty() ) {

		m_TextSize += 2 * m_FullSize.cy;
		}
	
	DC.Deselect();
	}

void CMessageBox::FindSizeInfo(void)
{
	m_IconSize = m_pIconName ? CSize(SM_CXICON) : CSize(0, 0);

	m_BorderSize = FromDlgUnits(CSize(10, 8));
	
	m_ButtonSize = FromDlgUnits(CSize(40, 14));
	
	m_BarSize = FromDlgUnits(CSize(40 + 45 * (m_uButtons - 1), 14));
	
	m_DialogSize.cx = 3 * m_BorderSize.cx + m_IconSize.cx + m_TextSize.cx;

	m_DialogSize.cy = 3 * m_BorderSize.cy + m_TextSize.cy + m_BarSize.cy;

	MakeMax(m_DialogSize.cx, 2 * m_BorderSize.cx + m_BarSize.cx);
				
	MakeMax(m_DialogSize.cy, 3 * m_BorderSize.cy + m_IconSize.cy + m_BarSize.cy);
				
	m_IconOrg.x = m_BorderSize.cx;
	
	m_IconOrg.y = Max(m_BorderSize.cy + (m_TextSize.cy - m_IconSize.cy) / 2, m_BorderSize.cy);
	
	m_TextOrg.x = 2 * m_BorderSize.cx + m_IconSize.cx;
	
	m_TextOrg.y = Max(m_BorderSize.cy + (m_IconSize.cy - m_TextSize.cy) / 2, m_BorderSize.cy);
	}

void CMessageBox::MakeButtons(void)
{
	CPoint Org;
	
	Org.x = (m_DialogSize.cx - m_BarSize.cx) / 2;

	Org.y = m_DialogSize.cy - m_BorderSize.cy - m_ButtonSize.cy;
	
	for( UINT uScan = 0;  uScan < m_uButtons; uScan++ ) {

		DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_TABSTOP;
		
		dwStyle |= (uScan == m_uDefault) ? BS_DEFPUSHBUTTON : BS_PUSHBUTTON;

		CButtonInfo Info;
		
		AfxVerify(FindButton(m_pPattern[uScan], Info));
				
		CButton *pButton = New CButton;
		
		CRect Rect = CRect(Org, m_ButtonSize);
		
		pButton->Create(Info.Label, dwStyle, Rect, ThisObject, Info.uID);
		
		pButton->SetFont(afxFont(Dialog));

		if( uScan == m_uDefault ) {
			
			m_uDefID = Info.uID;
			}
		
		Org.x += m_ButtonSize.cx + m_BorderSize.cx / 2;
		}
	}

void CMessageBox::MakeDoNot(void)
{
	if( !m_DoNot.IsEmpty() ) {

		CPoint Org;
	
		Org.x = m_TextOrg.x;

		Org.y = m_DialogSize.cy - m_BorderSize.cy - m_ButtonSize.cy - m_FullSize.cy * 2;

		DWORD dwStyle = WS_CHILD | WS_VISIBLE | WS_TABSTOP | BS_AUTOCHECKBOX;

		CButton *pButton = New CButton;
		
		CRect Rect = CRect(Org, CSize(200, m_FullSize.cy));
		
		pButton->Create(CString(IDS_DO_NOT_SHOW_THIS), dwStyle, Rect, ThisObject, 100);
		
		pButton->SetFont(afxFont(Dialog));
		}
	}
	
void CMessageBox::AdjustSize(void)
{
	CRect Rect(m_DialogSize);
	
	AdjustWindowRect(Rect);

	SetWindowSize(Rect.GetSize(), TRUE);
	}

void CMessageBox::SaveDoNot(UINT uCode)
{
	if( !m_DoNot.IsEmpty() ) {

		CButton &Button = (CButton &) GetDlgItem(100);

		if( Button.IsChecked() ) {

			CRegKey Key = afxModule->GetApp()->GetUserRegKey();

			Key.MoveTo(L"PCDialog");

			Key.MoveTo(L"DoNotShow");

			Key.SetValue(m_DoNot, DWORD(uCode));
			}
		}
	}

// Button Location

BOOL CMessageBox::FindButton(TCHAR cTag, CButtonInfo &Info)
{
	switch( Info.cTag = cTag ) {
	
		case 'O':
			Info.Label = CString(IDS_BUTTON_OK);
			Info.uID   = IDOK;
			break;
	
		case 'C':
			Info.Label = CString(IDS_BUTTON_CANCEL);
			Info.uID   = IDCANCEL;
			break;
	
		case 'A':
			Info.Label = CString(IDS_BUTTON_ABORT);
			Info.uID   = IDABORT;
			break;
	
		case 'R':
			Info.Label = CString(IDS_BUTTON_RETRY);
			Info.uID   = IDRETRY;
			break;
	
		case 'I':
			Info.Label = CString(IDS_BUTTON_IGNORE);
			Info.uID   = IDIGNORE;
			break;
	
		case 'Y':
			Info.Label = CString(IDS_BUTTON_YES);
			Info.uID   = IDYES;
			break;
	
		case 'N':
			Info.Label = CString(IDS_BUTTON_NO);
			Info.uID   = IDNO;
			break;
	
		case 'H':
			Info.Label = CString(IDS_BUTTON_HELP);
			Info.uID   = IDHELP;
			break;
			
		case 'L':
			Info.Label = CString(IDS_BUTTON_ALL);
			Info.uID   = IDALL;
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

// End of File
