
#include "intern.hpp"

#include "WebCamera.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CWebCameraDriverOptions, CUIItem);

// Constructor

CWebCameraDriverOptions::CWebCameraDriverOptions(void)
{
	}

// UI Managament

void CWebCameraDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CWebCameraDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CWebCameraDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CWebCameraDeviceOptions, CUIItem);

// Constructor

CWebCameraDeviceOptions::CWebCameraDeviceOptions(void)
{
	m_Camera     = 0;

	m_Port       = 80;
	
	m_Mode       = 0;

	m_Auth       = 0;

	m_Time4      = 10;

	m_Scale      = 10000;

	m_Update     = 0;

	m_Period     = 0;

	m_uLastScale = m_Scale;

	m_Addr       = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_URI        = "/jpg/image.jpg";
	}

// UI Managament

void CWebCameraDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == L"Auth" || Tag == L"Mode" || Tag == L"Update" ) {

			BOOL fAuth = m_Auth > 0;

			pWnd->EnableUI(L"User",   fAuth);

			pWnd->EnableUI(L"Pass",   fAuth);

			pWnd->EnableUI(L"Scale",  m_Mode   == imgModeJPG);

			pWnd->EnableUI(L"Period", m_Update == updatePeriodic);
			}

		if( Tag == L"Scale" ) {

			if( m_Scale % 625 ) {

				UINT uFactor = m_Scale / 625;

				if( m_Scale > m_uLastScale ) {

					uFactor = uFactor + 1;
					}

				m_Scale      = uFactor * 625;

				m_uLastScale = m_Scale;

				pWnd->UpdateUI();
				}
			}
		}
	}

// Download Support

BOOL CWebCameraDeviceOptions::MakeInitData(CInitData &Init)
{
	CString URI(m_URI);

	if( URI[0] != L'/' ) {

		URI.Insert(0, L'/');
		}

	URI.Replace(L"%", L"%%");

	URI.Replace(L" ", L"%%20");

	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Camera));

	Init.AddLong(LONG(m_Addr));

	Init.AddWord(WORD(m_Port));

	Init.AddByte(BYTE(m_Mode));

	Init.AddWord(WORD(m_Time4));

	Init.AddText(URI);

	Init.AddByte(BYTE(m_Auth));

	Init.AddLong(LONG(m_Scale));

	Init.AddText(m_User);

	Init.AddText(m_Pass);

	Init.AddByte(BYTE(m_Update));

	Init.AddLong(LONG(m_Period));

	return TRUE;
	}

// Meta Data Creation

void CWebCameraDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Camera);

	Meta_AddInteger(Addr);

	Meta_AddInteger(Port);

	Meta_AddInteger(Time4);

	Meta_AddString(URI);

	Meta_AddString(User);

	Meta_AddString(Pass);

	Meta_AddInteger(Auth);

	Meta_AddInteger(Update);

	Meta_AddInteger(Period);

	Meta_AddInteger(Mode);

	Meta_AddInteger(Scale);
	}

//////////////////////////////////////////////////////////////////////////
//
// Generic Web Camera Driver
//

// Instantiator

ICommsDriver * Create_WebCameraDriver(void)
{
	return New CWebCameraDriver;
	}

// Constructor

CWebCameraDriver::CWebCameraDriver(void)
{
	m_wID          = 0x40D3;

	m_uType        = driverCamera;

	m_Manufacturer = "<System>";

	m_DriverName   = "IP Camera Driver";

	m_Version      = "1.00";

	m_DevRoot      = "Cam";

	m_ShortName    = "IP Camera";
	}

// Configuration

CLASS CWebCameraDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CWebCameraDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CWebCameraDeviceOptions);
	}

// Binding Control

UINT CWebCameraDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
