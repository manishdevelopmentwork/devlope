
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_VIRTSER_HPP
	
#define	INCLUDE_VIRTSER_HPP

//////////////////////////////////////////////////////////////////////////
//
// Linked Driver Options
//

class CLinkedSerialDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLinkedSerialDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Port;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Linked Serial Driver
//

class CLinkedSerialDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CLinkedSerialDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dummy Serial Driver
//

class CDummySerialDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CDummySerialDriver(void);
	};

// End of File

#endif
