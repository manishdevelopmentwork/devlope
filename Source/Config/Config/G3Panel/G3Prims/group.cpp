
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Group Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimGroup, CPrimSet);

// Constructor

CPrimGroup::CPrimGroup(void)
{
	m_uType = 0x0C;
	}

// Operations

void CPrimGroup::SetRect(CRect const &NewRect)
{
	CPrimSet::SetRect(NewRect);
	}

// Overridables

void CPrimGroup::SetRect(CRect const &OldRect, CRect const &NewRect)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		m_pList->GetItem(Index)->SetRect(OldRect, NewRect);

		m_pList->GetNext(Index);
		}

	CPrimSet::SetRect(OldRect, NewRect);
	}

BOOL CPrimGroup::GetBindState(void)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		if( pPrim->IsGroup() ) {

			if( pPrim->GetBindState() ) {

				return TRUE;
				}
			}

		m_pList->GetNext(Index);
		}

	return FALSE;
	}

BOOL CPrimGroup::BindToTag(CString Top, CString Tag)
{
	BOOL  fOkay = TRUE;

	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		if( pPrim->IsGroup() ) {

			if( !pPrim->BindToTag(Top, Tag) ) {

				fOkay = FALSE;
				}
			}

		m_pList->GetNext(Index);
		}

	return fOkay;
	}

void CPrimGroup::ClearBinding(void)
{
	INDEX Index = m_pList->GetHead();

	while( !m_pList->Failed(Index) ) {

		CPrim *pPrim = m_pList->GetItem(Index);

		if( pPrim->IsGroup() ) {

			pPrim->ClearBinding();
			}

		m_pList->GetNext(Index);
		}
	}

// Meta Data

void CPrimGroup::AddMetaData(void)
{
	CPrimSet::AddMetaData();

	Meta_SetName((IDS_GROUP));
	}

// End of File
