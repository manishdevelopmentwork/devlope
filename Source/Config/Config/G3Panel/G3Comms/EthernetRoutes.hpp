
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_EthernetRoutes_HPP

#define INCLUDE_EthernetRoutes_HPP

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Routing Table
//

class DLLNOT CEthernetRoutes : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEthernetRoutes(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		DWORD m_Use   [6];
		DWORD m_Dest  [6];
		DWORD m_Mask  [6];
		DWORD m_Gate  [6];
		UINT  m_Metric[6];

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
