
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Lexical Constant
//

// Constructors

CLexConst::CLexConst(void)
{
	m_Type    = typeVoid;

	m_pString = NULL;
	}

CLexConst::CLexConst(CLexConst const &That)
{
	if( That.m_Type == typeString ) {

		AllocString(That.m_pString);

		m_Type = typeString;

		return;
		}

	memcpy(this, &That, sizeof(ThisObject));
	}

CLexConst::CLexConst(C3INT Value)
{
	m_Type    = typeInteger;

	m_Integer = Value;
	}

CLexConst::CLexConst(C3REAL Value)
{
	m_Type = typeReal;

	m_Real = Value;
	}

CLexConst::CLexConst(PCTXT pText)
{
	AllocString(pText);

	m_Type = typeString;
	}

// Destructor

CLexConst::~CLexConst(void)
{
	EmptyString();
	}

// Copy Assignment

CLexConst const & CLexConst::operator = (CLexConst const &That)
{
	EmptyString();

	if( That.m_Type == typeString ) {

		AllocString(That.m_pString);

		m_Type = typeString;

		return ThisObject;
		}

	memcpy(this, &That, sizeof(ThisObject));

	return ThisObject;
	}

// Value Assignment

CLexConst const & CLexConst::operator = (C3INT Value)
{
	SetValue(Value);

	return ThisObject;
	}

CLexConst const & CLexConst::operator = (C3REAL Value)
{
	SetValue(Value);

	return ThisObject;
	}

CLexConst const & CLexConst::operator = (PCTXT pText)
{
	SetValue(pText);

	return ThisObject;
	}

// Attributes

UINT CLexConst::GetType(void) const
{
	return m_Type;
	}

BOOL CLexConst::IsType(UINT Type) const
{
	return m_Type == Type;
	}

CString CLexConst::Describe(void) const
{
	switch( m_Type ) {

		case typeInteger:
			
			return IDS_CONST_INTEGER;

		case typeReal:
			
			return IDS_CONST_REAL;

		case typeString:
			
			return IDS_CONST_STRING;
		}

	return L"<const>";
	}

// Operations

void CLexConst::Empty(void)
{
	EmptyString();

	m_Type = typeVoid;
	}

// Value Access

C3INT CLexConst::GetIntegerValue(void) const
{
	AfxAssert(m_Type == typeInteger);

	return m_Integer;
	}

C3REAL CLexConst::GetRealValue(void) const
{
	AfxAssert(m_Type == typeReal);

	return m_Real;
	}

CString CLexConst::GetStringValue(void) const
{
	AfxAssert(m_Type == typeString);

	return m_pString;
	}

// Forced Access

C3INT CLexConst::ForceInteger(void) const
{
	return C3INT(m_Type == typeInteger ? m_Integer : m_Real);
	}

C3INT CLexConst::ForceLogical(void) const
{
	return C3INT(m_Type == typeInteger ? (m_Integer ? 1 : 0) : (m_Real ? 1 : 0));
	}

C3REAL CLexConst::ForceReal(void) const
{
	return C3REAL(m_Type == typeInteger ? m_Integer : m_Real);
	}

// Value Modification

void CLexConst::SetValue(C3INT Value)
{
	EmptyString();

	m_Type    = typeInteger;

	m_Integer = Value;
	}

void CLexConst::SetValue(C3REAL Value)
{
	EmptyString();

	m_Type = typeReal;

	m_Real = Value;
	}

void CLexConst::SetValue(PCTXT  pText)
{
	EmptyString();

	AllocString(pText);

	m_Type = typeString;
	}

// Implementation

void CLexConst::AllocString(PCTXT pText)
{
	m_pString = wstrdup(pText);
	}

void CLexConst::EmptyString(void)
{
	if( m_Type == typeString ) {

		free(m_pString);

		m_pString = NULL;
		}
	}

// End of File
