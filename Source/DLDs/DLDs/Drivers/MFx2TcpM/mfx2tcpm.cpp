#include "intern.hpp"

#include "mfx2tcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX2N Encapsulated TCP/IP Master Driver
//

// Instantiator

INSTANTIATE(CMitFx2TcpMasterDriver);

// Constructor

CMitFx2TcpMasterDriver::CMitFx2TcpMasterDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CMitFx2TcpMasterDriver::~CMitFx2TcpMasterDriver(void)
{
	m_pCtx = NULL;

	m_uKeep = 0;

	}

// Configuration

void MCALL CMitFx2TcpMasterDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CMitFx2TcpMasterDriver::Attach(IPortObject *pPort)
{

	}

void MCALL CMitFx2TcpMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CMitFx2TcpMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
		
			m_pCtx->m_bDrop  = 1;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CMitFx2TcpMasterDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CMitFx2TcpMasterDriver::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}

		return CMitFXMasterDriver::Ping();
		}

	return CCODE_ERROR; 
	}

// Socket Management

BOOL CMitFx2TcpMasterDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CMitFx2TcpMasterDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}
	
	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CMitFx2TcpMasterDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

BOOL CMitFx2TcpMasterDriver::Transact(void)
{      
	if( OpenSocket() ) {
	
		if( SendFrame() && RecvFrame() ) {
		
			return CheckFrame();
			}

		CloseSocket(TRUE);
		}

	return FALSE;
	}

BOOL CMitFx2TcpMasterDriver::SendFrame(void)
{
	AddByte(ETX);

	BYTE bCheck = 0;
	
	for( UINT i = 1; i < m_uPtr; i++ ) {
	
		bCheck += m_bTxBuff[i];

		}

	AddByte(m_pHex[bCheck / 16]);

	AddByte(m_pHex[bCheck % 16]);
		
	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE; 
	}

BOOL CMitFx2TcpMasterDriver::RecvFrame(void)
{
	m_uPtr  = 0;

	UINT uSize = 0;
	
	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + m_uPtr, uSize);

		if( uSize ) {

			m_uPtr += uSize;

			for( UINT u = 0; u < m_uPtr; u++ ) {

				BYTE bByte = m_bRxBuff[u];

				if( bByte == ETX ) {

					return TRUE;
					}

				if( u == 0 ) {

					if( bByte == ACK  || bByte == NAK ) {

						return TRUE;
						}
					}
				}
		
			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;

	}

BOOL CMitFx2TcpMasterDriver::CheckFrame(void)
{
	BYTE bByte = m_bRxBuff[0];

	if( bByte == ACK ) {

		return TRUE;
		}
	
	if( bByte == STX ) {

		BYTE bCheck = 0;

		for( UINT i = 1; i < m_uPtr - 2; i++ ) {
	
			bCheck += m_bRxBuff[i];
			}

		if( m_pHex[bCheck / 16] == m_bRxBuff[m_uPtr - 2] &&
		    m_pHex[bCheck % 16] == m_bRxBuff[m_uPtr - 1] ) {

			memcpy(m_bRxBuff, m_bRxBuff + 1, m_uPtr - 4);

			return TRUE;
			}
		}
	
	return FALSE;
	}

// End of File
