
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortPage_HPP

#define INCLUDE_CommsPortPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPort;

//////////////////////////////////////////////////////////////////////////
//
// Comms Port Page
//

class CCommsPortPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCommsPortPage(CCommsPort *pPort);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CCommsPort * m_pPort;

		// Implementation
		BOOL LoadDriverConfig(IUICreate *pView);
		BOOL LoadPortConfig(IUICreate *pView);
		BOOL LoadButtons(IUICreate *pView);
	};

// End of File

#endif
