
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CTotalFlow3SerialMasterDeviceOptionsUIList
//

CTotalFlow3SerialMasterDeviceOptionsUIList RCDATA
BEGIN
	"Estab,Link Establishment,,CUIEditInteger,|0|frames|1|75,"
	"\0"

	"Time1,Reply Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"\0"

	"UnkeyDelay,Unkey Delay,,CUIEditInteger,|0|ms|0|60000,"
	"Set the amount of time to wait before sending a frame. This will "
	"give the target device time to disable its transmitter and "
	"prepare to receive data. Generally, this should be set slightly "
	"longer than the Unkey Delay that is configured in the target "
	"device. This setting is only used for RS-485 communications. "
	"\0"

	"\0"
END

CTotalFlow3SerialMasterDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Name,Code\0"
	"G:1,root,Meter Configuration,Update,Timeout,StringSize\0"
	"G:1,root,Application Management,UseApp,AppConfig,AppImport,AppExport\0"
	"G:1,root,Protocol Options,Estab,Time1\0"
	"G:1,root,RS-485 Specific Configuration,UnkeyDelay\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CTotalFlow3TcpMasterDeviceOptionsUIList
//

CTotalFlow3TcpMasterDeviceOptionsUIList RCDATA
BEGIN
	"IP,IP Address,,CUIIPAddress,,"
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|65535,"
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a comms request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CTotalFlow3TcpMasterDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,IP,Port,Name,Code\0"
	"G:1,root,Meter Configuration,Update,Timeout,StringSize\0"
	"G:1,root,Application Management,UseApp,AppConfig,AppImport,AppExport\0"
	"G:1,root,Protocol Options,Keep,Ping,Time1,Time3,Time2\0"
	"\0"
END

// End of File

