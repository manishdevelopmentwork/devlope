
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttMessage_HPP

#define	INCLUDE_MqttMessage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttJsonData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MQTT Message
//

class DLLAPI CMqttMessage
{
	public:
		// Attributes
		BOOL GetJson (CMqttJsonData &Json) const;
		BOOL GetText (CString &Text) const;

		// Operations
		void SetCode (UINT uCode);
		void SetTopic(CString const &Topic);
		void SetData (CString const &Text);
		void SetData (CMqttJsonData const &Json);
		void SetData (CByteArray const &Data);

		// Persistance
		UINT GetBytes(void) const;
		BOOL Save(CByteArray &Data);
		UINT Load(PCBYTE &pData);

		// Data Members
		BOOL	   m_fKill;
		UINT       m_uPos;
		UINT       m_uCode;
		CString    m_Topic;
		CByteArray m_Data;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Attributes

STRONG_INLINE BOOL CMqttMessage::GetJson(CMqttJsonData &Json) const
{
	CString Text;

	if( GetText(Text) ) {

		if( Json.Parse(Text) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

STRONG_INLINE BOOL CMqttMessage::GetText(CString &Text) const
{
	PCTXT pText = PCTXT(m_Data.GetPointer());

	UINT  uText = m_Data.GetCount();

	if( uText ) {

		Text = CString(pText, uText);

		return TRUE;
		}

	Text.Empty();

	return FALSE;
	}

// Operations

STRONG_INLINE void CMqttMessage::SetCode(UINT uCode)
{
	m_uCode = uCode;
	}

STRONG_INLINE void CMqttMessage::SetTopic(CString const &Topic)
{
	m_Topic = Topic;
	}

STRONG_INLINE void CMqttMessage::SetData(CString const &Text)
{
	m_Data.Empty();

	m_Data.Append(PCBYTE(PCTXT(Text)), Text.GetLength());
	}

STRONG_INLINE void CMqttMessage::SetData(CMqttJsonData const &Json)
{
	SetData(Json.GetAsText(FALSE));
	}

STRONG_INLINE void CMqttMessage::SetData(CByteArray const &Data)
{
	m_Data = Data;
	}

// End of File

#endif
