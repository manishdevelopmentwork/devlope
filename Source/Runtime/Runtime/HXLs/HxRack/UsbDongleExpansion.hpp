
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbDongleExpansion_HPP

#define	INCLUDE_UsbDongleExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Dongle Expansion Object
//

class CUsbDongleExpansion : public CUsbExpansion
{
	public:
		// Constructor
		CUsbDongleExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT METHOD GetName(void);
		UINT  METHOD GetClass(void);
		UINT  METHOD GetPower(void);
	};

// End of File

#endif


