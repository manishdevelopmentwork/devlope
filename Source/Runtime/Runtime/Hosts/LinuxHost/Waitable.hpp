
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Waitable_HPP

#define INCLUDE_Waitable_HPP

//////////////////////////////////////////////////////////////////////////
//
// Generic Waitable Object
//

class CWaitable
{
public:
	// Constructor
	CWaitable(void);

protected:
	// Data Members
	int m_state;
	int m_swait;

	// Implementation
	BOOL WaitForChange(int nState, BOOL fAlert);
	BOOL WaitForChange(int nState, UINT &uWait, BOOL fAlert);
	BOOL WakeThreads(int nThread);
};

// End of File

#endif
