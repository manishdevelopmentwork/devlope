
#include "Intern.hpp"

#include "TransFlagCtrl.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Control
//

// Runtime Class

AfxImplementRuntimeClass(CTransFlagCtrl, CCtrlWnd);

// Constructor

CTransFlagCtrl::CTransFlagCtrl(void)
{
	m_pLang = NULL;
	}

// Operations

void CTransFlagCtrl::SetFlag(CLangManager *pLang, UINT uFlag)
{
	m_pLang = pLang;

	m_uFlag = uFlag;
	}

// Message Map

AfxMessageMap(CTransFlagCtrl, CCtrlWnd)
{
	AfxDispatchMessage(WM_PAINT)

	AfxMessageEnd(CTransFlagCtrl)
	};

// Message Handlers

void CTransFlagCtrl::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	if( m_pLang ) {

		DC.FillRect(Rect, afxBrush(TabFace));

		m_pLang->DrawFlag(DC, Rect, m_uFlag);

		return;
		}

	DC.FillRect(Rect, afxBrush(RED));
	}

// End of File
