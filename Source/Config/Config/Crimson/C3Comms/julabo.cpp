
#include "intern.hpp"

#include "julabo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Julabo Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CJulaboDeviceOptions, CUIItem);

// Constructor

CJulaboDeviceOptions::CJulaboDeviceOptions(void)
{
	m_Drop = 0;
	m_User = 0;
	}

// UI Management

void CJulaboDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
 	}

// Download Support

BOOL CJulaboDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	Init.AddWord(WORD(m_User));

	return TRUE;
	}

// Meta Data Creation

void CJulaboDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	Meta_AddInteger(User);
	}

//////////////////////////////////////////////////////////////////////////
//
// Julabo Serial Driver
//

// Instantiator

ICommsDriver *	Create_JulaboSerialDriver(void)
{
	return New CJulaboSerialDriver;
	}

// Constructor

CJulaboSerialDriver::CJulaboSerialDriver(void)
{
	m_wID		= 0x4068;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Julabo";
	
	m_DriverName	= "Circulator Head";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Julabo Circulator Head";

	m_DevRoot	= "Head";

	AddSpaces();
	AddUSRSpaces(TRUE);
	}

// Binding Control

UINT	CJulaboSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CJulaboSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 4800;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CJulaboSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CJulaboDeviceOptions);
	}

// Address Management

BOOL CJulaboSerialDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	SetUserAccess(pConfig->GetDataAccess("User")->ReadInteger(pConfig));

	CJulaboSerialDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

void CJulaboSerialDriver::SetUserAccess(UINT uUSRAccess)
{
	DeleteAllSpaces();

	AddSpaces();

	AddUSRSpaces(uUSRAccess == USRALLOW);
	}

// Implementation	

void CJulaboSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace(JSPT,	"SP",  "SP",				10,	0,	10, RR));
	AddSpace(New CSpace(JPAR,	"PR",  "Par",				10,	0,	20, RR));
	AddSpace(New CSpace(JPV,	"PV",  "PV",				10,	0,	10, RR));
	AddSpace(New CSpace(JHIL,	"HI",  "Cooling",			10,	0,	10, LL));
	AddSpace(New CSpace(JMODE,	"MO",  "Mode",				10,	0,	10, LL));
	AddSpace(New CSpace(JSTAT,	"ST",  "Status Value",			10,	0,	 0, LL));
	AddSpace(New CSpace(JSTSTR,	"STS", "Status String",			10,	0,	19, LL));
	AddSpace(New CSpace(JVERS,	"VR",  "Version String",		10,	0,	19, LL));
	}

void CJulaboSerialDriver::AddUSRSpaces(BOOL fOk)
{
	if( fOk  ) {

		AddSpace(New CSpace(JUSRS,	"US",  "Send UC, Receive UR",		10,	0,	 0, LL));
		AddSpace(New CSpace(JUSRC,	"UC",  "   User Command String",	10,	0,	19, LL));
		AddSpace(New CSpace(JUSRR,	"UR",  "   User Response String",	10,	0,	19, LL));
		}
	}

// Helpers

//////////////////////////////////////////////////////////////////////////
//
// Julabo Serial Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CJulaboSerialDialog, CStdAddrDialog);
		
// Constructor

CJulaboSerialDialog::CJulaboSerialDialog(CJulaboSerialDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_fPart		= fPart;

	m_pDriverData	= &Driver;

	m_fUpdateInfo	= FALSE;

	SetName(TEXT("JulaboSerialAddressDlg"));
	}

// Message Map

AfxMessageMap(CJulaboSerialDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)

	AfxDispatchCommand(2009, OnButtonClicked)

	AfxMessageEnd(CJulaboSerialDialog)
	};

// Message Handlers

BOOL CJulaboSerialDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	GetDlgItem(2001).EnableWindow(TRUE);
	GetDlgItem(2002).EnableWindow(TRUE);
	GetDlgItem(2010).EnableWindow(TRUE);
	GetDlgItem(2011).EnableWindow(TRUE);

	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	LoadList();
	
	if( !m_fPart ) {

		OnSpaceChange( 1000, ListBox );

		if( m_pSpace ) {

			if( !Addr.m_Ref ) m_pSpace->GetMinimum(Addr);

			ShowAddress(Addr);
			}

		SetDlgFocus(2002);

		return !(BOOL(m_pSpace));
		}
	else {
		if( m_pSpace ) {

			CString Text = m_pSpace->m_Prefix;

			Text += GetAddressText();

			CError   Error(FALSE);

			CAddress Addr;
			
			if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				CAddress Addr;

				m_pSpace->GetMinimum(Addr);
				}

			ShowAddress(Addr);
			}

		return FALSE;
		}
	}

// Notification Handlers

void CJulaboSerialDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CJulaboSerialDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		CSpace *pSpace;
		
		pSpace   = m_pSpace;

		m_pSpace = m_pDriver->GetSpace(Index);

		CAddress Addr;

		if( m_pSpace != pSpace ) {

			Addr.a.m_Table  = m_pSpace->m_uTable;

			Addr.a.m_Offset = 0;

			Addr.a.m_Type   = m_pSpace->m_uType;

			Addr.a.m_Extra  = 0;

			ShowAddress(Addr);
			}

		return;
		}

	for( UINT i = 2010; i <= 2018; i++ ) {

		GetDlgItem(i).SetWindowText(TEXT(""));
		}

	GetDlgItem(2001).SetWindowText(TEXT(""));
	GetDlgItem(2002).SetWindowText(TEXT(""));
	GetDlgItem(2002).EnableWindow(FALSE);
	GetDlgItem(2009).EnableWindow(FALSE);

	m_pSpace = NULL;
	}

BOOL CJulaboSerialDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		Text        += GetAddressText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			if( !m_fUpdateInfo ) {

				EndDialog(TRUE);
				}

			return TRUE;
			}

		Error.Show(ThisObject);

		SetDlgFocus( 2002 );

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CJulaboSerialDialog::OnButtonClicked(UINT uID)
{
	m_fUpdateInfo = TRUE;

	OnOkay(uID);

	m_fUpdateInfo = FALSE;

	CAddress Addr = *m_pAddr;

	ShowAddress(Addr);

	SetDlgFocus(2002);

	return TRUE;
	}

// Overrideables

void CJulaboSerialDialog::ShowAddress(CAddress Addr)
{
	CStdAddrDialog::ShowAddress(Addr);

	BOOL fNotText = Addr.a.m_Table < JSTAT;

	if( !fNotText ) {

		GetDlgItem(2002).SetWindowText(TEXT(""));
		}

	GetDlgItem(2002).EnableWindow(fNotText);

	GetInfo(Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type);
	}

// Information

void CJulaboSerialDialog::GetInfo(UINT uT, UINT uO, UINT uY)
{
	GetDlgItem(2009).EnableWindow(uT == JPAR);

	CString Text = "INTEGER";

	switch( uY ) {

		case RR:
			Text = "REAL";
			break;

		case LL:
			if( IsStringItem(uT) ) Text = "STRING";
			break;
		}

	GetDlgItem(2010).SetWindowText(Text);

	UINT k;

	UINT uDlgItem = 2011;

	UINT uCt      = 1;

	CString Desc;

	switch( uT ) {

		case JSPT:

			GetDlgItem(2011).SetWindowText(TEXT("0 - Working Temperature - Setpoint 1"));
			GetDlgItem(2012).SetWindowText(TEXT("1 - Working Temperature - Setpoint 2"));
			GetDlgItem(2013).SetWindowText(TEXT("2 - Working Temperature - Setpoint 3"));
			GetDlgItem(2014).SetWindowText(TEXT("3 - OVERTEMP"));
			GetDlgItem(2015).SetWindowText(TEXT("4 - SUBTEMP"));
			GetDlgItem(2016).SetWindowText(TEXT("5 -"));
			GetDlgItem(2017).SetWindowText(TEXT("6 - Manipulated variable for heater"));
			GetDlgItem(2018).SetWindowText(TEXT("7 - Pump Pressure Stage"));
			return;

		case JPAR:
			UINT uEnd;

			uEnd = uO + 8;

			if( uO < 9 ) {

				uEnd--;
				GetDlgItem(2018).SetWindowText(TEXT("<more...>"));
				}

			for( k = uO; k < uEnd; k++ ) {

				switch( k ) {

					case 0:
						Desc = "Work/Safety sensor temperature difference";
						break;

					case 1:
						Desc = "Te - Time constant of external bath";
						break;

					case 2:
						Desc = "Si - internal slope";
						break;

					case 3:
						Desc = "Ti - Time constant of internal bath";
						break;

					case 4:
						Desc = "CoSpeed - Band limit";
						break;

					case 5:
						Desc = "Factor pk/ph0";
						break;

					case 6:
						Desc = "Xp control parameter of internal controller";
						break;

					case 7:
						Desc = "Tn control parameter of internal controller";
						break;

					case 8:
						Desc = "Tv control parameter of internal controller";
						break;

					case 9:
						Desc = "Xp control parameter of cascade controller";
						break;

					case 10:
						Desc = "Proportional share of cascade controller";
						break;

					case 11:
						Desc = "Tn control parameter of cascade controller";
						break;

					case 12:
						Desc = "Tv control parameter of cascade controller";
						break;

					case 13:
						Desc = "Max. internal Temperature of cascade controller";
						break;

					case 14:
						Desc = "Min. internal Temperature of cascade controller";
						break;

					case 15:
						Desc = "Upper Band Limit";
						break;

					case 16:
						Desc = "Lower Band Limit";
						break;

					default:
						Desc = "";
						break;
					}

				Text.Printf( "%2.2d - %s", k, Desc);

				GetDlgItem(uDlgItem++).SetWindowText(Text);
				}

			return;

		case JPV:
			GetDlgItem(2011).SetWindowText(TEXT("0 - Current/Actual bath temperature"));
			GetDlgItem(2012).SetWindowText(TEXT("1 - Heating power being used"));
			GetDlgItem(2013).SetWindowText(TEXT("2 - Temperature @ Pt100 sensor"));
			GetDlgItem(2014).SetWindowText(TEXT("3 - Temperature @ Safety sensor"));
			GetDlgItem(2015).SetWindowText(TEXT("4 - Safe Temp"));
			uCt = 5;
			break;

		case JHIL:
			GetDlgItem(2011).SetWindowText(TEXT("0 - Maximum cooling power (%)"));
			GetDlgItem(2012).SetWindowText(TEXT("1 - Maximum heating power (%)"));
			uCt = 2;
			break;

		case JMODE:
			GetDlgItem(2011).SetWindowText(TEXT("1 - Working Temperature Setpoint Selection"));
			GetDlgItem(2012).SetWindowText(TEXT("2 - Selftuning type"));
			GetDlgItem(2013).SetWindowText(TEXT("3 - External programmer input"));
			GetDlgItem(2014).SetWindowText(TEXT("4 - Internal/External temperature control"));
			GetDlgItem(2015).SetWindowText(TEXT("5 - Start/Stop Condition"));
			GetDlgItem(2016).SetWindowText(TEXT("6 -"));
			GetDlgItem(2017).SetWindowText(TEXT("7 -"));
			GetDlgItem(2018).SetWindowText(TEXT("8 - Control Dynamics - Standard/Aperiodic"));
			return;

		case JSTAT:
			GetDlgItem(2011).SetWindowText(TEXT("Status Numerical Value"));
			break;

		case JSTSTR:
		case JVERS:
			GetDlgItem(2011).SetWindowText(TEXT("Display via String Tag"));
			break;

		case JUSRS:
			GetDlgItem(2011).SetWindowText(TEXT("Send user defined string UC"));
			break;

		case JUSRC:
			GetDlgItem(2011).SetWindowText(TEXT("User defined command"));
			break;

		case JUSRR:
			GetDlgItem(2011).SetWindowText(TEXT("Displays response to UC via US"));
			break;
		}

	for( k = 2011 + uCt; k <= 2018; k++ ) {

		GetDlgItem(k).SetWindowText(TEXT(""));
		}
	}

// Helpers
BOOL CJulaboSerialDialog::IsStringItem(UINT uT)
{
	switch(uT) {

		case JSTSTR:
		case JVERS:
		case JUSRC:
		case JUSRR:
			return TRUE;
		}

	return FALSE;
	}

// End of File
