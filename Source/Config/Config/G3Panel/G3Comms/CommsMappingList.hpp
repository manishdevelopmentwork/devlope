
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsMappingList_HPP

#define INCLUDE_CommsMappingList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsDevice;
class CCommsMapBlock;
class CCommsMapReg;
class CCommsMapping;

//////////////////////////////////////////////////////////////////////////
//
// Comms Mapping List
//

class DLLNOT CCommsMappingList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsMappingList(void);

		// Item Location
		CCommsMapReg   * GetParentReg(void) const;
		CCommsMapBlock * GetParentBlock(void) const;
		CCommsDevice   * GetParentDevice(void) const;

		// Item Access
		CCommsMapping * GetItem(INDEX Index) const;
		CCommsMapping * GetItem(UINT   uPos) const;

		// Operations
		void Validate(BOOL fExpand);
		void SetCount(UINT uCount);
	};

// End of File

#endif
