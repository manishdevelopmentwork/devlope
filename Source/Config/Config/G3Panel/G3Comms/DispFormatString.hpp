
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatString_HPP

#define INCLUDE_DispFormatString_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DispFormat.hpp"

//////////////////////////////////////////////////////////////////////////
//
// String Display Format
//

class DLLNOT CDispFormatString : public CDispFormat
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatString(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Formatting
		CString Format(DWORD Data, UINT Type, UINT Flags);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT    m_Length;
		CString m_Template;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
