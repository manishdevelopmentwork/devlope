
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Text Panel Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CTextGadget, CGadget);
		
// Constructor

CTextGadget::CTextGadget(UINT uID, int nWidth, UINT uFormat)
{
	m_uID     = uID;

	m_uFormat = uFormat;

	m_fClip   = FALSE;
	
	m_uProps  = tbgShowTip | (nWidth ? 0 : tbgSizeToFit);

	////////

	CClientDC DC(NULL);
	
	DC.Select(IsBold() ? afxFont(Bolder) : afxFont(Status));

	m_Size.cx = nWidth;

	m_Size.cy = DC.GetTextExtent(L"X").cy + 5;
	
	DC.Deselect();
	}

CTextGadget::CTextGadget(UINT uID, PCTXT pSet, UINT uFormat)
{
	m_uID     = uID;
	
	m_Text    = pSet;

	m_uFormat = uFormat;
	
	m_fClip   = FALSE;
	
	m_uProps  = tbgShowTip | (IsRight() ? tbgRight : 0);

	////////

	CClientDC DC(NULL);
	
	DC.Select(IsBold() ? afxFont(Bolder) : afxFont(Status));

	m_Size.cx = 5 + DC.GetTextExtent(pSet).cx;
	
	m_Size.cy = DC.GetTextExtent(L"X").cy + 5;
	
	DC.Deselect();
	}
		
// Overridables

void CTextGadget::OnPaint(CDC &DC)
{
	if( m_Rect.GetWidth() ) {
		
		DC.Select(IsBold() ? afxFont(Bolder) : afxFont(Status));

		DC.SetBkMode(TRANSPARENT);

		CString Text = CalcText(DC, m_Rect.GetWidth() - 5);

		CPoint  Pos  = m_Rect.GetTopLeft();

		if( IsRight() ) {

			Pos.x += m_Rect.cx() - 3 - DC.GetTextExtent(Text).cx;

			Pos.y += 2;
			}
		else {
			Pos.x += 3;

			Pos.y += 2;
			}
		
		if( TestFlag(MF_DISABLED) ) {
		
			DC.SetTextColor(afxColor(3dShadow));

			DC.ExtTextOut( Pos,
			               ETO_CLIPPED,
			       	       m_Rect - 1,
		       		       Text
		       		       );
			}
		else {
			if( IsRed() ) {

				DC.SetTextColor(afxColor(RED));
				}
			else
				DC.SetTextColor(afxColor(ButtonText));

			DC.ExtTextOut( Pos,
			               ETO_CLIPPED,
			       	       m_Rect - 1,
		       		       Text
		       		       );
		       	}
		
		DC.Deselect();
		}
	}

BOOL CTextGadget::OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip)
{
	if( m_fClip ) {

		Tip = m_Text;
		
		return TRUE;
		}
		
	return FALSE;
	}
	
void CTextGadget::OnSetText(void)
{
	Update();

	m_pWnd->UpdateWindow();
	}

void CTextGadget::OnSetFlags(void)
{
	Update();
	}

// Implementation

CString CTextGadget::CalcText(CDC &DC, int nMax)
{
	m_fClip = FALSE;
	
	CString Text = m_Text;

	BOOL fSpaces = (m_Text.Find(' ') < NOTHING);
		
	CString Show = Text;
			
	while( Text.GetLength() ) {
		
		Show = Text + (m_fClip ? L"..." : L"");
		
		if( DC.GetTextExtent(Show).cx > nMax ) {
		
			if( fSpaces ) {
			
				UINT uPos = Text.FindRev(' ');
			
				if( uPos < NOTHING ) {

					Text = Text.Left(uPos);
					}
				else
					Text = L"";
				}
			else {
				UINT uLen = Text.GetLength();
					
				Text = Text.Left(uLen - 1);
				}
			
			m_fClip = TRUE;
			}
		else
			break;
		}
		
	return Show;
	}

BOOL CTextGadget::IsBold(void)
{
	return (m_uFormat & textBold) ? TRUE : FALSE;
	}

BOOL CTextGadget::IsRed(void)
{
	return (m_uFormat & textRed) ? TRUE : FALSE;
	}

BOOL CTextGadget::IsRight(void)
{
	return (m_uFormat & textRight) ? TRUE : FALSE;
	}

// End of File
