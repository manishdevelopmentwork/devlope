
//////////////////////////////////////////////////////////////////////////
//
// Pane Window
//

class /*DLLAPI*/ CPaneWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CPaneWnd(void);

	protected:
		// Data Members
		CString	      m_Title;
		CMenu	      m_ToolMenu;
		UINT	      m_ShowID;
		CAccelerator  m_Accel;
		CViewWnd    * m_pView;
		CToolbarWnd * m_pTB1;
		CToolbarWnd * m_pTB2;
		CToolbarWnd * m_pTB3;
		CRect         m_TBRect1;
		CRect         m_TBRect2;
		CRect         m_TBRect3;
		BOOL	      m_fShow3;
		BOOL	      m_fValid;

		// Interface Control
		virtual void OnUpdateInterface(void);

		// Routing Control
		BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

		// Message Map
		AfxDeclareMessageMap();
		
		// Accelerator
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnPaint(void);
		void OnGoingIdle(void);
		void OnSetFocus(CWnd &Wnd);
		void OnSetCurrent(BOOL fCurrent);
		void OnUpdateUI(void);
		void OnKeyDown(UINT uCode, DWORD dwData);

		// Command Handlers
		BOOL OnViewControl(UINT uID, CCmdSource &Src);
		BOOL OnViewCommand(UINT uID);

		// Implementation
		void CalcPositions(void);
		void ClearToolbars(void);
		void UpdateToolbars(void);

		// Overridable
		virtual CRect GetClientRect(void);
	};		

// End of File
