
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#include "drvtable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Table Comms Driver
//

// Instantiator

ICommsDriver *	Create_TestTableCommsDriver(void)
{
	return New CTestTableCommsDriver;
	}

// Constructor

CTestTableCommsDriver::CTestTableCommsDriver(void)
{
	m_wID		= 0x0001;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Test Drivers";
	
	m_DriverName	= "Table Comms Driver";
	
	m_Version	= "1.01";
	
	m_ShortName	= "PLC";

	AddSpaces();
	}

// Binding Control

UINT	CTestTableCommsDriver::GetBinding(void)
{
	return bindRawSerial;
	}

void	CTestTableCommsDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation

void	CTestTableCommsDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "R",  "Registers",		10, 0, 100, addrWordAsWord));
	
	AddSpace(New CSpace(2, "XB", "Input Bits",		10, 0, 100, addrBitAsBit));
	
	AddSpace(New CSpace(3, "XWH", "Input Words (hex)",	16, 0, 100, addrBitAsWord));
	
	AddSpace(New CSpace(4, "XWO", "Input Words (oct)",	8, 0, 100,  addrBitAsWord));
	
	AddSpace(New CSpace(5, "XWB", "Input Words (bin)",	2, 0, 100,  addrBitAsWord));
	
	AddSpace(New CSpace(6, "XD", "Input Longs",	10, 0, 100, addrBitAsLong));
	
	AddSpace(New CSpace(7, "XW", "Input Words (dec)",	10, 0, 100, addrBitAsWord, 4));
	}

// End of File
