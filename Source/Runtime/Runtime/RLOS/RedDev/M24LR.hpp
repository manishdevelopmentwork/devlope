
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_M24LR_HPP
	
#define	INCLUDE_M24LR_HPP

//////////////////////////////////////////////////////////////////////////
//
// M24LR NFC RFID Serial EEPROM 
//

class CM24LR : public ISerialMemory
{
	public:
		// Constructor
		CM24LR(void);

		// Destructor
		~CM24LR(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISerialMemory
		UINT METHOD GetSize(void);
		BOOL METHOD IsFast(void);
		BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount);
		BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount);

	protected:
		// Data Members
		ULONG   m_uRefs;
		II2c  * m_pI2c;
		BYTE	m_bChip;
	};

// End of File

#endif
