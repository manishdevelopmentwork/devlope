
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramCatWnd_HPP

#define INCLUDE_ProgramCatWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CProgramItem;
class CProgramItemView;
class CProgramNavTreeWnd;
class CProgramManager;

//////////////////////////////////////////////////////////////////////////
//
// Program Category Window
//

class DLLAPI CProgramCatWnd : public CViewWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramCatWnd(void);

		// Destructor
		~CProgramCatWnd(void);

		// Operations
		void SetNavTree(CProgramNavTreeWnd *pNavTree);
		void SetItemView(CProgramItemView *pItemView);
		void OnReload(CProgramItem *pProg);

	protected:
		// Data Members
		CAccelerator         m_Accel;
		CSysProxy            m_System;
		CProgramNavTreeWnd * m_pNavTree;
		CProgramItemView   * m_pItemView;
		CProgramManager    * m_pManager;
		BOOL		     m_fSemi;
		BOOL		     m_fDisable;
		BOOL		     m_fPend;

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Command Handlers
		BOOL OnGlobalCommand(UINT uID);
		BOOL OnProgramControl(UINT uID, CCmdSource &Src);
		BOOL OnProgramCommand(UINT uID);
	};

// End of File

#endif
