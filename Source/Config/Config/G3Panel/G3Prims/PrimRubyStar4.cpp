
#include "intern.hpp"

#include "PrimRubyStar4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 4-Pointed Star Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyStar4, CPrimRubyStar)

// Constructor

CPrimRubyStar4::CPrimRubyStar4(void)
{
	m_nSides = 8;

	m_AltRad = 30;

	m_Rotate = +900;
	}

// Meta Data

void CPrimRubyStar4::AddMetaData(void)
{
	CPrimRubyStar::AddMetaData();

	Meta_SetName((IDS_POINTED_STAR));
	}

// End of File
