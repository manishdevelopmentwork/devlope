
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostModuleDriver_HPP

#define	INCLUDE_UsbHostModuleDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncBulkDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Module Base
//

class CUsbHostModuleDriver : public CUsbHostFuncBulkDriver, public IUsbHostModule
{
	public:
		// Constructor
		CUsbHostModuleDriver(void);

		// Destructor
		~CUsbHostModuleDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModule
		BOOL METHOD Reset(void);
		BOOL METHOD ReadVersion(BYTE Version[16]); 
		BOOL METHOD SendHeartbeat(void);
	
	protected:
		// Device Commands
		enum
		{
			cmdReset	= 0x01,
			cmdVersion	= 0x02,
			cmdHeartbeat	= 0x03,
			};

		// Interface Commands
		enum
		{
			cmdSetLatency	= 0x00,
			};
	};

// End of File

#endif
