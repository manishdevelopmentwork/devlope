
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive
//

// Constructor

CPrimWidget::CPrimWidget(void)
{
	m_pAbove    = NULL;

	m_pServer   = NULL;

	m_pOnSelect = NULL;

	m_pOnRemove = NULL;

	m_pOnUpdate = NULL;

	m_pOnSecond = NULL;

	m_pData     = New CPrimWidgetPropData;
	}

// Destructor

CPrimWidget::~CPrimWidget(void)
{
	delete m_pOnSelect;
	
	delete m_pOnRemove;
	
	delete m_pOnUpdate;
	
	delete m_pOnSecond;

	delete m_pData;
	}

// Initialization

void CPrimWidget::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimWidget", pData);

	CPrim::Load(pData);

	m_pList->Load(pData, this);

	GetCoded(pData, m_pOnSelect);

	GetCoded(pData, m_pOnRemove);

	GetCoded(pData, m_pOnUpdate);

	GetCoded(pData, m_pOnSecond);

	m_pData->Load(pData);
	}

// Operations

void CPrimWidget::Enter(void)
{
	if( m_pAbove ) {

		AfxTrace("*** CPrimWidget::Enter double call\n");

		Sleep(100);

		HostTrap(3);
		}

	CUISystem *pSystem = CUISystem::m_pThis;

	m_pAbove  = pSystem->GetDataServer();

	m_pServer = pSystem->m_pUserServer;

	pSystem->SetDataServer(this);
	}

void CPrimWidget::Leave(void)
{
	if( !m_pAbove ) {

		AfxTrace("*** CPrimWidget::Leave double call\n");

		Sleep(100);

		HostTrap(3);
		}

	CUISystem *pSystem = CUISystem::m_pThis;

	pSystem->SetDataServer(m_pAbove);

	m_pAbove  = NULL;

	m_pServer = NULL;
	}

// Overidables

void CPrimWidget::SetScan(UINT Code)
{
	Enter();

	CPrimGroup::SetScan(Code);

	SetItemScan(m_pOnSelect, Code);

	SetItemScan(m_pOnRemove, Code);
	
	SetItemScan(m_pOnUpdate, Code);
	
	SetItemScan(m_pOnSecond, Code);

	Leave();
	}

void CPrimWidget::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		Enter();

		CUIDataServer * pData = CUISystem::m_pThis->m_pUserServer;

		CPrim         * pPrev = pData->GetPrim();
		
		UINT c = m_pList->m_uCount;

		for( UINT n = 0; n < c; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			pData->SetPrim(pPrim);

			pPrim->DrawPrep(pGDI, Erase, Trans);
			}

		pData->SetPrim(pPrev);

		Leave();
		}
	}

void CPrimWidget::DrawExec(IGDI *pGDI, IRegion *pDirty)
{
	if( m_fShow ) {

		Enter();

		CUIDataServer * pData = CUISystem::m_pThis->m_pUserServer;

		CPrim         * pPrev = pData->GetPrim();
		
		UINT c = m_pList->m_uCount;

		for( UINT n = 0; n < c; n++ ) {

			CPrim *pPrim = m_pList->m_ppPrim[n];

			pData->SetPrim(pPrim);

			pPrim->DrawExec(pGDI, pDirty);
			}

		pData->SetPrim(pPrev);

		Leave();
		}
	}

UINT CPrimWidget::OnMessage(UINT uMsg, UINT uParam)
{
	if( uMsg == msgIsGroup ) {

		return 2;
		}

	if( uMsg == msgHasEvents ) {

		if( m_pOnSelect || m_pOnRemove ) {

			return 1;
			}

		if( m_pOnUpdate || m_pOnSecond ) {

			return 1;
			}

		return 0;
		}

	if( uMsg == msgTestEvent ) {

		Enter();

		BOOL fOkay = TRUE;

		switch( uParam ) {

			case 0:
				fOkay = IsItemAvail(m_pOnSelect);
				break;

			case 1:
				fOkay = IsItemAvail(m_pOnRemove);
				break;

			case 2:
				fOkay = IsItemAvail(m_pOnUpdate);
				break;

			case 3:
				fOkay = IsItemAvail(m_pOnSecond);
				break;
			}

		Leave();

		return fOkay;
		}

	if( uMsg == msgFireEvent ) {

		Enter();

		switch( uParam ) {

			case 0:
				if( m_pOnSelect ) {
					
					m_pOnSelect->Execute(typeVoid);
					}
				break;

			case 1:
				if( m_pOnRemove ) {
					
					m_pOnRemove->Execute(typeVoid);
					}
				break;

			case 2:
				if( m_pOnUpdate ) {

					m_pOnUpdate->Execute(typeVoid);
					}
				break;

			case 3:
				if( m_pOnSecond ) {
					
					m_pOnSecond->Execute(typeVoid);
					}
				break;
			}

		Leave();
		}

	return CPrimSet::OnMessage(uMsg, uParam);
	}

// Base Methods

UINT CPrimWidget::Release(void)
{
	return 0;
	}

// Data Server Methods

WORD CPrimWidget::CheckID(WORD ID)
{
	if( ID >= 0xFF80 ) {

		UINT n = ID - 0xFF80;

		if( n < m_pData->m_pList->m_uCount ) {

			CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

			if( pProp ) {

				CCodedItem *pCode = pProp->m_pValue;

				if( pCode ) {

					CPrim::TempLeave(this);

					ID = pCode->ExecVal();

					CPrim::TempEnter(this);

					return ID;
					}
				}
			}

		return 0;
		}

	return m_pServer->CheckID(ID);
	}

BOOL CPrimWidget::IsAvail(DWORD ID)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_IsTag ) {

		if( Ref.t.m_Index >= 0x7F80 ) {

			UINT n = Ref.t.m_Index - 0x7F80;

			if( n < m_pData->m_pList->m_uCount ) {

				CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

				if( pProp ) {

					CCodedItem *pCode = pProp->m_pValue;

					if( pCode ) {

						CPrim::TempLeave(this);

						BOOL fAvail = pCode->IsAvail();

						CPrim::TempEnter(this);

						return fAvail;
						}

					return TRUE;
					}
				}

			return FALSE;
			}
		}

	return m_pServer->IsAvail(ID);
	}

BOOL CPrimWidget::SetScan(DWORD ID, UINT Code)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_IsTag ) {

		if( Ref.t.m_Index >= 0x7F80 ) {

			UINT n = Ref.t.m_Index - 0x7F80;

			if( n < m_pData->m_pList->m_uCount ) {

				CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

				if( pProp ) {

					CCodedItem *pCode = pProp->m_pValue;

					if( pCode ) {

						CPrim::TempLeave(this);

						BOOL fSet = pCode->SetScan(Code);

						CPrim::TempEnter(this);

						return fSet;
						}
					}
				}

			return FALSE;
			}
		}

	return m_pServer->SetScan(ID, Code);
	}

DWORD CPrimWidget::GetData(DWORD ID, UINT Type, UINT Flags)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_HasBit ) {

		DWORD dwMask   = (1 << Ref.t.m_BitRef);

		Ref.t.m_HasBit = 0;

		Ref.t.m_BitRef = 0;

		DWORD dwData   = GetData(ID, Type, Flags);

		if( dwData & dwMask ) {

			return TRUE;
			}

		return FALSE;
		}

	if( Ref.t.m_IsTag ) {

		if( Ref.t.m_Index >= 0x7F80 ) {

			UINT n = Ref.t.m_Index - 0x7F80;

			if( n < m_pData->m_pList->m_uCount ) {

				CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

				if( pProp ) {

					CCodedItem *pCode = pProp->m_pValue;

					if( pCode ) {

						CPrim::TempLeave(this);

						if( pCode->IsWritable() ) {

							UINT i = Ref.x.m_Array;

							ID     = pCode->Execute(typeLValue);

							Ref.x.m_Array = i;
 
							CPrim::TempEnter(this);

							if( Type == typeLValue ) {

								return ID;
								}

							return m_pAbove->GetData(ID, Type, Flags);
							}

						if( pCode->IsConst() ) {

							if( Type == typeString ) {

								CCodedText *pText = (CCodedText *) pCode;

								DWORD Data = DWORD(wstrdup(pText->GetText()));

								CPrim::TempEnter(this);

								return Data;
								}
							}

						DWORD Data = pCode->Execute(Type);

						CPrim::TempEnter(this);

						return Data;
						}

					if( pProp->HasLocal() ) {

						return pProp->GetData(Type);
						}
					}
				}
			}
		}

	return m_pServer->GetData(ID, Type, Flags);
	}

BOOL CPrimWidget::SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_HasBit ) {

		DWORD dwMask   = (1 << Ref.b.m_BitRef);

		Ref.t.m_HasBit = 0;

		Ref.t.m_BitRef = 0;

		DWORD dwData   = GetData(ID, Type, Flags);

		if( Data ) {

			if( !(dwData & dwMask) ) {

				dwData |= dwMask;

				return SetData(ID, Type, Flags, dwData);
				}

			return TRUE;
			}
		else {
			if( dwData & dwMask ) {

				dwData &= ~dwMask;

				return SetData(ID, Type, Flags, dwData);
				}

			return TRUE;
			}
		}

	if( Ref.t.m_IsTag ) {

		if( Ref.t.m_Index >= 0x7F80 ) {

			UINT n = Ref.t.m_Index - 0x7F80;

			if( n < m_pData->m_pList->m_uCount ) {

				CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

				if( pProp ) {

					CCodedItem *pCode = pProp->m_pValue;

					if( pCode ) {
						
						CPrim::TempLeave(this);

						UINT i = Ref.x.m_Array;

						ID     = pCode->Execute(typeLValue);

						Ref.x.m_Array = i;

						CPrim::TempEnter(this);

						return m_pAbove->SetData(ID, Type, Flags, Data);
						}

					if( pProp->HasLocal() ) {

						return pProp->SetData(Data, Type);
						}
					}
				}
			}
		}

	return m_pServer->SetData(ID, Type, Flags, Data);
	}

DWORD CPrimWidget::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	CDataRef &Ref = (CDataRef &) ID;

	if( Ref.t.m_Index >= 0x7F80 ) {

		UINT n = Ref.t.m_Index - 0x7F80;

		if( n < m_pData->m_pList->m_uCount ) {

			CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

			if( pProp ) {

				CCodedItem *pCode = pProp->m_pValue;

				if( pCode ) {

					if( pCode->m_uRefs ) {

						UINT uTag = pCode->GetTagIndex();

						Ref.t.m_Index = uTag;

						Ref.t.m_IsTag = TRUE;
						}
					else {
						DWORD Data = pCode->ExecVal();

						Ref.t.m_Index = LOWORD(Data);

						Ref.t.m_IsTag = FALSE;
						}

					return m_pAbove->GetProp(ID, Prop, Type);
					}
				}
			}
		}

	return m_pServer->GetProp(ID, Prop, Type);
	}

DWORD CPrimWidget::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	if( ID >= 0x7F80 ) {

		UINT n = ID - 0x7F80;

		if( n < m_pData->m_pList->m_uCount ) {

			CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

			if( pProp ) {

				CCodedItem *pCode = pProp->m_pValue;

				if( pCode ) {

					CPrim::TempLeave(this);

					pCode->Execute(typeVoid);

					CPrim::TempEnter(this);

					return 0;
					}
				}
			}
		}

	return m_pServer->RunFunc(ID, uParam, pParam);
	}

BOOL CPrimWidget::GetName(DWORD ID, UINT m, PSTR pName, UINT uName)
{
	if( m == 0 ) {

		if( ID >= 0x7F80 ) {

			UINT n = ID - 0x7F80;

			if( n < m_pData->m_pList->m_uCount ) {

				CPrimWidgetProp *pProp = m_pData->m_pList->m_ppProp[n];

				if( pProp ) {

					}
				}
			}
		}

	return m_pServer->GetName(ID, m, pName, uName);
	}

// End of File
