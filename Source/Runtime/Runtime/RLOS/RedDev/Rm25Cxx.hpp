
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Rm25Cxx_HPP
	
#define	INCLUDE_Rm25Cxx_HPP

//////////////////////////////////////////////////////////////////////////
//
// Adesto Rm25Cxx SPI EEPROM
//

class CRm25Cxx : public ISerialMemory
{
	public:
		// Constructor
		CRm25Cxx(UINT uChan);

		// Destructor
		~CRm25Cxx(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ISerialMemory
		UINT METHOD GetSize(void);
		BOOL METHOD IsFast(void);
		BOOL METHOD GetData(UINT uAddr, PBYTE  pData, UINT uCount);
		BOOL METHOD PutData(UINT uAddr, PCBYTE pData, UINT uCount);

	protected:
		// Data Members
		ULONG   m_uRefs;
		ISpi  * m_pSpi;
		UINT    m_uChan;

		// Implementation
		bool WriteEnable(bool fWrite);
		bool IsWriteDone(void);
		bool ReadStatusReg(BYTE &Reg);
	};

// End of File

#endif
