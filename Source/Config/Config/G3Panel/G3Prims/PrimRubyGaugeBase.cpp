
#include "intern.hpp"

#include "PrimRubyGaugeBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Base Gauge Primitive
//

// Runtime Class

AfxImplementRuntimeClass(CPrimRubyGaugeBase, CPrimRuby);

// Constructor

CPrimRubyGaugeBase::CPrimRubyGaugeBase(void)
{
	m_pointCenter.m_x = 100;
	m_pointCenter.m_y = 100;
	m_pValue          = NULL;
	m_pMin            = NULL;
	m_pMax            = NULL;
	m_pMajorColor     = New CPrimColor;
	m_pMinorColor     = New CPrimColor;
	m_Major           = 4;
	m_Minor           = 5;
	m_PointMode       = 0;
	m_pPointColor     = New CPrimColor;
	m_BandShow1       = 0;
	m_pBandColor1     = New CPrimColor;
	m_pBandMin1       = NULL;
	m_pBandMax1       = NULL;
	m_BandShow2       = 0;
	m_pBandColor2     = New CPrimColor;
	m_pBandMin2       = NULL;
	m_pBandMax2       = NULL;
	m_BugShow1        = 0;
	m_pBugColor1      = New CPrimColor;
	m_pBugValue1      = NULL;
	m_BugShow2        = 0;
	m_pBugColor2      = New CPrimColor;
	m_pBugValue2      = NULL;
	m_Orient	  = 0;
	m_Reflect	  = 0;
	m_Rotate	  = 450;
	}

// UI Managament

void CPrimRubyGaugeBase::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Orient" ) {

			DoEnables(pHost);
			}

		if( Tag.Find(L"Show") < NOTHING ) {

			DoEnables(pHost);
			}
		}

	CPrimRuby::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimRubyGaugeBase::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" || Tag == "Min" || Tag == "Max" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"BandMin") || Tag.StartsWith(L"BandMax") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"BugValue") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return CPrimRuby::GetTypeData(Tag, Type);
	}

// Overridables

void CPrimRubyGaugeBase::Draw(IGDI *pGdi, UINT uMode)
{
	CRubyGdiLink gdi(pGdi);

	gdi.OutputSolid(m_listBand1, m_pBandColor1->GetColor(), 0);
	gdi.OutputSolid(m_listBand2, m_pBandColor2->GetColor(), 0);
	gdi.OutputSolid(m_listMajor, m_pMajorColor->GetColor(), 0);
	gdi.OutputSolid(m_listMinor, m_pMinorColor->GetColor(), 0);
	gdi.OutputSolid(m_listBug1,  m_pBugColor1 ->GetColor(), 0);
	gdi.OutputSolid(m_listBug2,  m_pBugColor2 ->GetColor(), 0);
	}

void CPrimRubyGaugeBase::SetInitState(void)
{
	CPrimRuby::SetInitState();

	m_pMajorColor->Set(GetRGB(0,0,0));
	m_pMinorColor->Set(GetRGB(0,0,0));
	
	m_pPointColor->Set(GetRGB(31,0,0));

	m_pBandColor1->Set(GetRGB(0,15,0));
	m_pBandColor2->Set(GetRGB(31,0,0));

	m_pBugColor1->Set(GetRGB(0,15,0));
	m_pBugColor2->Set(GetRGB(31,0,0));
	}

// Download Support

BOOL CPrimRubyGaugeBase::MakeInitData(CInitData &Init)
{
	CPrimRuby::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);
	Init.AddItem(itemVirtual, m_pMin);
	Init.AddItem(itemVirtual, m_pMax);
	Init.AddItem(itemSimple,  m_pMajorColor);
	Init.AddItem(itemSimple,  m_pMinorColor);
	
	Init.AddByte(BYTE(m_Major));
	Init.AddByte(BYTE(m_Minor));
	Init.AddByte(BYTE(m_PointMode));

	Init.AddItem(itemSimple, m_pPointColor);

	Init.AddByte(BYTE(m_BandShow1));

	if( m_BandShow1 ) {

		Init.AddItem(itemSimple,  m_pBandColor1);
		Init.AddItem(itemVirtual, m_pBandMin1);
		Init.AddItem(itemVirtual, m_pBandMax1);
		}

	Init.AddByte(BYTE(m_BandShow2));

	if( m_BandShow2 ) {

		Init.AddItem(itemSimple,  m_pBandColor2);
		Init.AddItem(itemVirtual, m_pBandMin2);
		Init.AddItem(itemVirtual, m_pBandMax2);
		}

	Init.AddByte(BYTE(m_BugShow1));

	if( m_BugShow1 ) {

		Init.AddItem(itemSimple,  m_pBugColor1);
		Init.AddItem(itemVirtual, m_pBugValue1);
		}

	Init.AddByte(BYTE(m_BugShow2));

	if( m_BugShow2 ) {

		Init.AddItem(itemSimple,  m_pBugColor2);
		Init.AddItem(itemVirtual, m_pBugValue2);
		}

	AddMatrix(Init, m_m);

	AddNumber(Init, m_scale);

	AddPoint (Init, m_pointCenter);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeBase::AddMetaData(void)
{
	CPrimRuby::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddVirtual(Min);
	Meta_AddVirtual(Max);
	Meta_AddObject (MajorColor);
	Meta_AddObject (MinorColor);
	Meta_AddInteger(Major);
	Meta_AddInteger(Minor);
	Meta_AddInteger(PointMode);
	Meta_AddObject (PointColor);
	Meta_AddInteger(BandShow1);
	Meta_AddObject (BandColor1);
	Meta_AddVirtual(BandMin1);
	Meta_AddVirtual(BandMax1);
	Meta_AddInteger(BandShow2);
	Meta_AddObject (BandColor2);
	Meta_AddVirtual(BandMin2);
	Meta_AddVirtual(BandMax2);
	Meta_AddInteger(BugShow1);
	Meta_AddObject (BugColor1);
	Meta_AddVirtual(BugValue1);
	Meta_AddInteger(BugShow2);
	Meta_AddObject (BugColor2);
	Meta_AddVirtual(BugValue2);
	Meta_AddInteger(Orient);
	Meta_AddInteger(Reflect);
	Meta_AddInteger(Rotate);

	Meta_SetName((IDS_BASE_GAUGE));
	}

// Path Management

void CPrimRubyGaugeBase::InitPaths(void)
{
	number sx = (m_DrawRect.x2 - m_DrawRect.x1) / 100.0;
	
	number sy = (m_DrawRect.y2 - m_DrawRect.y1) / 100.0;

	number sm = Min(sx, sy);

	m_scale   = 1.0 / (sm * sm);

	m_pathMajor.Empty();
	m_pathMinor.Empty();
	m_pathBand1.Empty();
	m_pathBand2.Empty();
	m_pathBug1 .Empty();
	m_pathBug2 .Empty();
	}

void CPrimRubyGaugeBase::MakeLists(void)
{
	SetTransform();

	m_pathMajor.Transform(m_m);
	m_pathMinor.Transform(m_m);
	m_pathBand1.Transform(m_m);
	m_pathBand2.Transform(m_m);
	m_pathBug1 .Transform(m_m);
	m_pathBug2 .Transform(m_m);

	m_listMajor.Load(m_pathMajor,  true);
	m_listMinor.Load(m_pathMinor,  true);
	m_listBand1.Load(m_pathBand1,  true);
	m_listBand2.Load(m_pathBand2,  true);
	m_listBug1 .Load(m_pathBug1,   true);
	m_listBug2 .Load(m_pathBug2,   true);
	}

// Scaling

number CPrimRubyGaugeBase::GetValue(CCodedItem *pValue, C3REAL Default)
{
	if( pValue ) {
		
		if( !pValue->IsBroken() ) {

			return I2R(pValue->Execute(typeReal));
			}
		}

	return Default;
	}

// Implementation

void CPrimRubyGaugeBase::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, L"Rotate", m_Orient == 4);

	pHost->EnableUI(this, L"BandColor1", m_BandShow1);
	pHost->EnableUI(this, L"BandMin1",   m_BandShow1);
	pHost->EnableUI(this, L"BandMax1",   m_BandShow1);
	
	pHost->EnableUI(this, L"BandColor2", m_BandShow2);
	pHost->EnableUI(this, L"BandMin2",   m_BandShow2);
	pHost->EnableUI(this, L"BandMax2",   m_BandShow2);

	pHost->EnableUI(this, L"BugColor1",  m_BugShow1);
	pHost->EnableUI(this, L"BugValue1",  m_BugShow1);

	pHost->EnableUI(this, L"BugColor2",  m_BugShow2);
	pHost->EnableUI(this, L"BugValue2",  m_BugShow2);
	}

void CPrimRubyGaugeBase::SetTransform(void)
{
	// Clear the transformation.

	m_m.SetIdentity();

	// Copy the outermost path.

	CRubyPath path(GetOuterPath());
	
	// Move the center of the figure to the origin.

	R2R rect;

	path.GetBoundingRect(rect);

	m_m.AddTranslation( -0.5 * (rect.m_x1 + rect.m_x2),
			    -0.5 * (rect.m_y1 + rect.m_y2)
			    );

	// Apply any rotations.

	switch( m_Orient ) {

		// Goofy values but they match those from
		// CRubyPrimOriented and that is what the 
		// graphics editor is expecting to see!

		case 1:
			m_m.AddRotation(180);

			break;

		case 2:
			m_m.AddRotation(270);

			break;

		case 3:
			m_m.AddRotation(90);

			break;

		case 4:
			m_m.AddRotation(INT(m_Rotate) / 10.0);

			break;
		}

	// Apply any reflections.

	switch( m_Reflect ) {

		// Likewise!

		case 1:
			m_m.AddReflectVert();

			break;

		case 2:
			m_m.AddReflectVert();

			m_m.AddReflectHorz();

			break;

		case 3:
			m_m.AddReflectHorz();

			break;
		}

	// Transform the outermost path;

	path.Transform(m_m);

	// Find the transformed bounding rectangle.

	path.GetBoundingRect(rect);

	// Recenter the figure.

	m_m.AddTranslation( -0.5 * (rect.m_x1 + rect.m_x2),
			    -0.5 * (rect.m_y1 + rect.m_y2)
			    );

	// Scale the figure to fit the drawing rectangle.

	m_m.AddScaleFactor( (m_DrawRect.x2 - m_DrawRect.x1 - 1) / fabs(rect.m_x2 - rect.m_x1),
			    (m_DrawRect.y2 - m_DrawRect.y1 - 1) / fabs(rect.m_y2 - rect.m_y1)
			    );

	// And move the figure to the correct location.

	m_m.AddTranslation( (m_DrawRect.x1 + m_DrawRect.x2) / 2,
			    (m_DrawRect.y1 + m_DrawRect.y2) / 2
			    );
	}

// Shaders

BOOL CPrimRubyGaugeBase::ShaderDark(IGdi *pGdi, int p, int c)
{
	static DWORD q = 0;

	if( c ) {

		COLOR a = GetRGB(0,0,0);

		COLOR b = GetRGB(20,20,20);

		DWORD k = Mix32(a, b, p, c);

		if( k - q ) {

			pGdi->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = 0;

	return FALSE;
	}

BOOL CPrimRubyGaugeBase::ShaderChrome(IGdi *pGdi, int p, int c)
{
	static int pos[] = { 0, 250, 300, 400, 500, 900, 1000 };

	static int col[] = { 0,  10,   2,  31,  29,  16,   31 };

	static DWORD q  = 0;

	static UINT  n  = 0;
	
	if( c ) {

		int pc = (c - p) * 1000 / c;

		if( n ) {
			
			if( pc <= pos[n] ) {
			
				n--;
				}
			}

		COLOR a = GetRGB(col[n+0], col[n+0], col[n+0]);

		COLOR b = GetRGB(col[n+1], col[n+1], col[n+1]);

		DWORD k = Mix32(a, b, pc - pos[n], pos[n+1] - pos[n]);

		if( k - q ) {

			pGdi->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = 0;

	n = elements(pos) - 1;

	return FALSE;
	}

DWORD CPrimRubyGaugeBase::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	ra = (ra << 3) | (3 & (ra >> 2));
	ga = (ga << 3) | (3 & (ga >> 2));
	ba = (ba << 3) | (3 & (ba >> 2));

	if( c && p ) {

		int rb = GetRED  (b);
		int gb = GetGREEN(b);
		int bb = GetBLUE (b);

		rb = (rb << 3) | (3 & (rb >> 2));
		gb = (gb << 3) | (3 & (gb >> 2));
		bb = (bb << 3) | (3 & (bb >> 2));

		if( p < c ) {

			int rc = ra + ((rb - ra) * p / c);
			int gc = ga + ((gb - ga) * p / c);
			int bc = ba + ((bb - ba) * p / c);

			return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
			}

		return MAKELONG(MAKEWORD(bb, gb), MAKEWORD(rb, 0xFF));
		}

	return MAKELONG(MAKEWORD(ba, ga), MAKEWORD(ra, 0xFF));
	}

// End of File
