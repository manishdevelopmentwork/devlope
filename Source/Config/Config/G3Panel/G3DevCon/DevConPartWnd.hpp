
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConPartWnd_HPP

#define INCLUDE_DevConPartWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConPart;

class CDevConGroupLabel;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Part View Window
//

class DLLAPI CDevConPartWnd : public CUIItemViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConPartWnd(CUIPageList *pList);

	// Destructor
	~CDevConPartWnd(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

protected:
	// Data Members
	CDevConPart * m_pPart;

	// Implementation
	void DoEnables(void);
	BOOL OnExport(void);
	BOOL OnExtract(void);
	BOOL OnCommit(BOOL fWarn);
	BOOL OnRevert(void);
	BOOL SaveFile(CFilename const &Name, CString const &Data);
};

// End of File

#endif
