
#include "Intern.hpp"

#include "RackItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsPortList.hpp"

#include "RackItemWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Backplane Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CRackItem, CUIItem);

// Constructor

CRackItem::CRackItem(void)
{
	m_pPorts = New CCommsPortList;
	}

// UI Creation

CViewWnd * CRackItem::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		return New CRackItemWnd;
		}

	return NULL;
	}

// Operations

void CRackItem::Validate(BOOL fExpand)
{
	m_pPorts->Validate(fExpand);
	}

void CRackItem::ClearSysBlocks(void)
{
	m_pPorts->ClearSysBlocks();
	}

void CRackItem::NotifyInit(void)
{
	m_pPorts->NotifyInit();
	}

void CRackItem::CheckMapBlocks(void)
{
	m_pPorts->CheckMapBlocks();
	}

// Item Naming

BOOL CRackItem::IsHumanRoot(void) const
{
	return TRUE;
	}

CString CRackItem::GetHumanName(void) const
{
	return CString(IDS_MODULES);
	}

// Download Support

BOOL CRackItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pPorts);

	return TRUE;
	}

// Meta Data Creation

void CRackItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddCollect(Ports);

	Meta_SetName((IDS_MODULES));
	}

// End of File
