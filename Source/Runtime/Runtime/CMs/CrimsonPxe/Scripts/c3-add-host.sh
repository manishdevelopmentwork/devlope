#!/bin/sh

# c3-add-host <interface> <address>
#
# Add a special entry to the local and global hosts files to provide
# a reference to the IP address of this interface. The host name is
# c3-eth0-addr for eth0 etc. and is used to allow tunnel scripts to
# refer to an interface's address during configuration.

con1="/etc/hosts"

con2="/tmp/crimson/dns/hosts"

temp="/tmp/crimson/dns/hosts.temp"

face="$1"

addr="$2"

name="c3-${face}-addr"

(
	flock -x 9

	for conf in $con1 $con2
	do
		cat $conf | grep -v $name > $temp

		echo $addr $name >> $temp
		
		cp $temp $conf
	done

) 9> /var/lock/c3-hosts
