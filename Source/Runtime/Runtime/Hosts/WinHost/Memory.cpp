
#include "Intern.hpp"

#include "Memory.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Windows Emulated Memory Device
//

// Constructor

CMemory::CMemory(void)
{
	m_uSize = 0;

	m_fMap  = TRUE;

	m_pData = NULL;

	m_hFile = NULL;

	m_hMap  = NULL;
	}

// Destructor

CMemory::~CMemory(void)
{
	FreeData();
	}

// Implementation

void CMemory::AllocData(void)
{
	BOOL i  = FALSE;

	m_Name  = CPrintf( "%s%s\\%s",
			   PCTXT(FindPath()),
			   PCTXT(CString(g_Config.m_pModel->m_pName).ToLower()),
			   PCTXT(m_Name)
			   );

	m_hFile = win32::CreateFileA( m_Name,
				      GENERIC_READ | GENERIC_WRITE,
				      FILE_SHARE_READ,
				      NULL,
				      OPEN_EXISTING,
				      0,//FILE_FLAG_WRITE_THROUGH,
				      NULL
				      );

	if( m_hFile == INVALID_HANDLE_VALUE ) {

		m_hFile = win32::CreateFileA( m_Name,
					      GENERIC_READ | GENERIC_WRITE,
					      FILE_SHARE_READ,
					      NULL,
					      OPEN_ALWAYS,
				              0,//FILE_FLAG_WRITE_THROUGH,
					      NULL
					      );

		i = TRUE;
		}

	if( m_hFile != INVALID_HANDLE_VALUE ) {

		if( m_fMap ) {

			m_hMap = win32::CreateFileMapping( m_hFile,
							   NULL,
							   PAGE_READWRITE,
							   0,
							   m_uSize,
							   NULL
							   );

			if( m_hMap != INVALID_HANDLE_VALUE ) {

				m_pData = PBYTE(win32::MapViewOfFile( m_hMap,
								      FILE_MAP_WRITE,
								      0,
								      0,
								      m_uSize
								      ));

				if( m_pData ) {

					if( i ) {

						memset(m_pData, 0xFF, m_uSize);
						}

					return;
					}

				win32::CloseHandle(m_hMap);

				m_hMap = NULL;
				}

			win32::CloseHandle(m_hFile);

			m_hFile = NULL;
			}
		else {
			if( i ) {

				UINT  uData = 65536;

				PBYTE pData = New BYTE [ uData ];

				memset(pData, 0xFF, uData);

				UINT uFill = m_uSize;

				while( uFill ) {

					UINT  uLump = min(uFill, uData);

					DWORD uDone = 0;

					win32::WriteFile(m_hFile, pData, uLump, &uDone, NULL);

					uFill -= uLump;
					}

				win32::SetFilePointer(m_hFile, 0, NULL, FILE_BEGIN);

				delete [] pData;
				}

			return;
			}
		}

	if( m_fMap ) {

		m_pData = New BYTE [ m_uSize ];

		memset(m_pData, 0xFF, m_uSize);

		return;
		}

	AfxAssert(FALSE);
	}

void CMemory::FreeData(void)
{
	if( m_pData || !m_fMap ) {

		if( m_hFile ) {

			if( m_pData ) {

				win32::UnmapViewOfFile(m_pData);
	
				win32::CloseHandle(m_hMap);
				}

			win32::CloseHandle(m_hFile);

			return;
			}

		delete [] m_pData;
		}
	}

CString CMemory::FindPath(void)
{
	char Path[MAX_PATH] = {0};

	strcpy(Path, g_Config.m_EmData);

	Path[0] || win32::GetEnvironmentVariableA("AeonTemp", Path, sizeof(Path));

	Path[0] || win32::GetEnvironmentVariableA("Tmp",      Path, sizeof(Path));

	Path[0] || win32::GetEnvironmentVariableA("Temp",     Path, sizeof(Path));

	Path[0] || strcpy(Path, ".");

	UINT c = strlen(Path);

	if( Path[c-1] != '\\' ) {

		strcat(Path, "\\");
		}

	return Path;
	}

// End of File
