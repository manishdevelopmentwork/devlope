
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs
//

// Times

unsigned int g_uTimeout = 30000;

unsigned long g_GSMTime = 0;

// App Features

UINT AppGetFeatures(void)
{
	CString Name = WhoGetName(TRUE);

	if( Name.StartsWith("CR1000") ) {

		return 0 * rfConsole	   |
		       1 * rfSystemMenu	   |
		       1 * rfCamera        |
		       0 * rfPdfViewer     |
		       0 * rfFileViewer    |
		       0 * rfLogToDisk     |
		       0 * rfPortSharing   |
		       0 * rfFileSystemApi |
		       0 * rfDnsApi        |
		       0 * rfWebServer     |
		       0 * rfPlayMusic     |
		       0 * rfPrintScreen   |
		       0 * rfRemoteDisplay |
		       0 * rfSqlQuery      |
		       0 * rfHoneywell     ;
		}

	if( Name.StartsWith("CR3000") ) {

		return 0 * rfConsole	   |
		       1 * rfSystemMenu	   |
		       1 * rfCamera        |
		       1 * rfPdfViewer     |
		       1 * rfFileViewer    |
		       1 * rfLogToDisk     |
		       1 * rfPortSharing   |
		       1 * rfFileSystemApi |
		       1 * rfDnsApi        |
		       1 * rfWebServer     |
		       1 * rfPlayMusic     |
		       1 * rfPrintScreen   |
		       0 * rfRemoteDisplay |
		       1 * rfSqlQuery      |
		       0 * rfHoneywell     ;
		}

	if( Name.StartsWith("G") ) {

		return 0 * rfConsole	   |
		       1 * rfSystemMenu	   |
		       1 * rfCamera        |
		       1 * rfPdfViewer     |
		       1 * rfFileViewer    |
		       1 * rfLogToDisk     |
		       1 * rfPortSharing   |
		       1 * rfFileSystemApi |
		       1 * rfDnsApi        |
		       1 * rfWebServer     |
		       1 * rfPlayMusic     |
		       1 * rfPrintScreen   |
		       0 * rfRemoteDisplay |
		       1 * rfSqlQuery      |
		       0 * rfHoneywell     ;
		}

	if( Name.StartsWith("DA") ) {

		return 0 * rfConsole	   |
		       1 * rfSystemMenu	   |
		       1 * rfCamera        |
		       1 * rfPdfViewer     |
		       1 * rfFileViewer    |
		       1 * rfLogToDisk     |
		       1 * rfPortSharing   |
		       1 * rfFileSystemApi |
		       1 * rfDnsApi        |
		       1 * rfWebServer     |
		       1 * rfPlayMusic     |
		       1 * rfPrintScreen   |
		       1 * rfRemoteDisplay |
		       1 * rfSqlQuery      |
		       0 * rfHoneywell     ;
		}

	AfxAssert(FALSE);

	return 0;
	}

// Debug Hook

BOOL DebugMode(void)
{
	return TRUE;
	}

UINT DebugPort(void)
{
	// !!!!

	return 2;
	}

// Other Hooks

void RunBoot(unsigned long)
{
	#if defined(AEON_PLAT_RLOS)

	for(;;) {

		ASM(	"mrs r0, cpsr\n\t"
			"orr r0, #192\n\t"
			"msr cpsr, r0"
			:
			:
			: "r0"
			);
		}

	#endif
	}

void SystemTimeChange(void)
{
	}

unsigned int AppGetBootVersion(void)
{
	return 10;
	}

BOOL SystemLoad(PCBYTE &pData)
{
	UINT uLevel = MotorToHost(PWORD(pData)[0]);

	switch( uLevel ) {

		case 1:
			pData += 2;

			(New CCommsSystem)->Load(pData);

			return TRUE;

		case 2:
			pData += 2;

			(New CUISystem)->Load(pData);

			return TRUE;
		}

	return FALSE;
	}

unsigned long DispGetPaletteEntry(unsigned int)
{
	return 0;
	}

void ChainCode(PCBYTE pImage, UINT uImage, UINT uParam)
{
	}

void RackSetError(UINT uError)
{
	}

UINT AddrGetCount(BOOL fSample)
{
	return 0;
	}

void PersistGet(BYTE bSlot, PBYTE pData, UINT uSize)
{
	}

void PersistPut(BYTE bSlot, PBYTE pData, UINT uSize)
{
	}

// End of File
