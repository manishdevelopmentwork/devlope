
#include "intern.hpp"

#include "ControlTask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "loader.hpp"

#include "dbver.hpp"

#include "g3comms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Control Task
//

// Instance Pointer

CControlTask * CControlTask::m_pThis = NULL;

// Memory Checkpoints

static UINT xm1, xm2;

// Bye Bye Hook

static void ByeBye(void)
{
	g_pPersist->ByeBye();
	}

// Constructor
 
CControlTask::CControlTask(void)
{
	StdSetRef();

	m_pThis     = this;

	m_hThis     = GetCurrentTask();

	m_pTimer    = CreateTimer();

	m_uTimer    = 0;

	m_fAuto     = FALSE;

	m_pReqStop  = Create_AutoEvent();

	m_pReqStart = Create_AutoEvent();
	
	m_pAckStop  = Create_ManualEvent();
	
	m_pAckStart = Create_ManualEvent();

	m_pTms      = Create_Semaphore();

	m_uReboot   = 0;

	m_hUpdate   = 0;

	m_uUpdate   = 0;

	m_pUpdate   = 0;
	
	m_fRun	    = FALSE;
	
	m_uLoadTime = 0;
	
	m_fLoaded   = FALSE;
	
	m_uTimeout1 = 0;

	m_uTimeout2 = 0;

	m_uAlarm    = 0;
	
	m_fSiren    = FALSE;
	
	m_uSiren    = NOTHING;
	
	m_uBright   = 0;

	m_fSmall    = FALSE;
	
	m_fNarrow   = FALSE;

	m_xDebug    = 16;

	m_yDebug    = 16;

	m_fSkipNet  = FALSE;

	m_uLedMode  = 0;

	m_pAckStop->Set();

	Beep(72, 150);

	FindLedType();
	}

// Destructor

CControlTask::~CControlTask(void)
{
	AfxRelease(m_pTimer);

	AfxRelease(m_pReqStop);

	AfxRelease(m_pReqStart);
	
	AfxRelease(m_pAckStop);
	
	AfxRelease(m_pAckStart);
	}

// Operations

BOOL CControlTask::Auto(void)
{
	m_fAuto = TRUE;

	return TRUE;
	}

BOOL CControlTask::Start(void)
{
	m_pReqStop ->Clear();

	m_pReqStart->Set();

	return m_pAckStart->Wait(5000);
	}

BOOL CControlTask::Stop(UINT uTimeout)
{
	m_pReqStart->Clear();

	m_pReqStop ->Set();

	return m_pAckStop->Wait(uTimeout);
	}

void CControlTask::Update(UINT hItem, PCBYTE pData, UINT uSize)
{
	m_hUpdate = hItem;

	m_pUpdate = New BYTE [ uSize ];

	m_uUpdate = uSize;

	memcpy(m_pUpdate, pData, uSize);

	SystemStop();
	}

void CControlTask::Restart(void)
{
	m_uReboot = 1;

	SystemStop();
	}

void CControlTask::Reboot(void)
{
	m_uReboot = 2;

	SystemStop();
	}

BOOL CControlTask::EnableMount(BOOL fEnable)
{
	DWORD Magic;

	FRAMGetData(Mem(Mounted), PBYTE(&Magic), sizeof(Magic));

	if( fEnable ) {

		if( Magic != 0x11111111 ) {

			Magic = 0x11111111;

			FRAMPutData(Mem(Mounted), PBYTE(&Magic), sizeof(Magic));

			return TRUE;
			}

		return FALSE;
		}
	else {
		FRAMGetData(Mem(Mounted), PBYTE(&Magic), sizeof(Magic));

		if( Magic == 0x11111111 ) {

			Magic = 0x00000000;

			FRAMPutData(Mem(Mounted), PBYTE(&Magic), sizeof(Magic));

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

void CControlTask::SetAlarm(UINT uState)
{
	m_uAlarm = uState;
	}

void CControlTask::SetSiren(BOOL fOn)
{
	if( m_fSiren != fOn ) {

		if( (m_fSiren = fOn) ) {

			m_uBright = DispGetBrightness();
			}

		m_uSiren = 0;
		}
	}

void CControlTask::SetTimeout(void)
{
	m_uTimeout1 = ToTimer(1000) * 90;

	m_uTimeout2 = ToTimer(1000) * 90;
	}

void CControlTask::SysDebug(PCTXT pForm, va_list pArgs)
{
	if( g_pGDI ) {

		char sText[128];

		VSPrintf(sText, pForm, pArgs);

		g_pGDI->BufferClaim();

		g_pGDI->SelectFont(fontHei12);

		char *p = sText;

		char *n = NULL;

		for(;;) {

			if( (n = strchr(p, '\n')) ) {
				
				*n = 0;
				}

			int cx = g_pGDI->GetTextWidth(p);

			if( m_xDebug + cx > g_pGDI->GetCx() - 16 ) {

				m_xDebug  = 16;

				m_yDebug += g_pGDI->GetTextHeight(p);
				}

			g_pGDI->TextOut(m_xDebug, m_yDebug, p);

			if( n ) {

				m_xDebug  = 16;

				m_yDebug += g_pGDI->GetTextHeight(p);

				p = n + 1;
				}
			else {
				m_xDebug += cx;

				break;
				}
			}

		g_pGDI->BufferFree();

		DispUpdate(g_pGDI->GetBuffer());
		}
	}

void CControlTask::SetLedMode(UINT uMode)
{
	m_uLedMode = uMode;
	}

UINT CControlTask::GetLedMode(void)
{
	return m_uLedMode;
	}

void CControlTask::SkipNetUpdate(void)
{
	m_fSkipNet = TRUE;
	}

// Attributes

BOOL CControlTask::GetSiren(void) const
{
	return m_fSiren;
	}

BOOL CControlTask::IsFlashEnabled(void) const
{
	DWORD Magic;

	FRAMGetData(Mem(Mounted), PBYTE(&Magic), sizeof(Magic));

	return Magic == 0x11111111;	
	}

BOOL CControlTask::IsRunningControl(void) const
{
	if( m_fRun ) {

		return CSystemItem::m_pThis->IsRunningControl();
		}

	return FALSE;
	}

// Entry Points

BOOL CControlTask::Init(void)
{
	SetThreadName("Control");

	DebugInit();

	return TRUE;
	}

int CControlTask::Exec(void)
{
	m_pTimer->SetHook(this, 0);

	m_pTimer->SetPeriod(TIMER_RATE);

	m_pTimer->Enable(true);

	PowerUp();

	if( g_pDbase->IsValid() ) {

		CreateGrab();

		LoadNetwork();

		CheckBattery();

		if( g_pDbase->IsValid() ) {

			if( !WasLoading() || !CheckFailed() ) {

				m_pReqStart->Set();

				CreateLink();
				}
			else {
				CreateLink();

				ShowInvalid();

				SetLoading(FALSE);
				}			
			}
		else {
			CreateLink();

			MinWait(1500);

			ShowInvalid();

			SetLoading(FALSE);
			}
		}
	else {
		CreateGrab();

		CreateLink();

		LoadNetwork();

		CheckBattery();

		MinWait(1500);

		ShowInvalid();

		SetLoading(FALSE);
		}

	if( ImageCheck() ) {

		if( ImageCanVerify() ) {

			ShowStatus("VERIFYING IMAGE ON MEMORY CARD");
			}

		if( ImageVerify() ) {

			ShowStatus("LOADING FROM MEMORY CARD");

			ImageLoad();
			
			m_pReqStart->Set();
			}
		}

	for(;;) {

		for( UINT d1 = 2 + rand() % 5;; ) {

			if( m_pReqStart->Wait(1000) ) {

				break;
				}

			if( m_fAuto && d1 && !--d1 ) {

				m_pReqStop ->Clear();

				m_pReqStart->Set();
				}
			}

		MinWait(1500);

		PerformStart();

		for( UINT d2 = 10 + rand() % 30;; ) {

			if( m_pReqStop->Wait(1000) ) {

				break;
				}

			if( m_fAuto && d2 && !--d2 ) {

				m_pReqStart->Clear();

				m_pReqStop ->Set();
				}
			}

		PerformStop();
		}

	return 0;
	}

void CControlTask::Stop(void)
{
	}

void CControlTask::Term(void)
{
	if( m_fRun ) {

		// TODO -- Not perfect, but catches it for now. !!!

		PerformStop();
		}

	Delete_XPNetwork();

	Delete_XPSerial();

	CNetConfig::Term();
	}

// IUnknown

HRESULT CControlTask::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
	}

ULONG CControlTask::AddRef(void)
{
	StdAddRef();
	}

ULONG CControlTask::Release(void)
{
	StdRelease();
	}

// IClientProcess

BOOL CControlTask::TaskInit(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	SetThreadName(Def.m_Name);

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskInit(uID);

		return TRUE;
		}

	switch( Def.m_uID ) {

		case 0:
			if( g_pProm ) {

				g_pProm->Init(0);
				}
			break;

		case 1:
			m_uTimeout1 = 0;
	
			m_uTimeout2 = 0;

			break;
		}

	return TRUE;
	}

INT CControlTask::TaskExec(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskExec(uID);

		return 0;
		}

	switch( Def.m_uID ) {

		case 0:
			if( g_pProm ) {

				g_pProm->Task(GetCurrentTask(), 0);
				}
			break;

		case 1:
			for(;;) {

				m_uTimeout1 = ToTimer(1000) * 90;

				Sleep(1000);
				}
			break;
		}

	return 0;
	}

void CControlTask::TaskStop(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskStop(uID);

		return;
		}

	switch( Def.m_uID ) {

		case 0:
			break;

		case 1:
			break;
		}
	}

void CControlTask::TaskTerm(UINT uTask)
{
	CTaskDef const &Def = m_List[uTask / 10];

	if( Def.m_pEntry ) {

		UINT uID = Def.m_uID + uTask % 10;

		Def.m_pEntry->TaskTerm(uID);

		return;
		}

	switch( Def.m_uID ) {

		case 0:
			if( g_pProm ) {
	
				g_pProm->Term(0);
				}
			break;

		case 1:
			m_uTimeout1 = 0;
	
			m_uTimeout2 = 0;

			break;
		}
	}

// IEventSink

void CControlTask::OnEvent(UINT uLine, UINT uParam)
{
	PollSiren();

	if( m_fRun ) {

		if( m_uTimeout1 && !--m_uTimeout1 ) {

			AfxTrace("\n*** TIMEOUT 1\n\n");

			//HostTrap(15);
			}

		if( m_uTimeout2 && !--m_uTimeout2 ) {

			AfxTrace("\n*** TIMEOUT 2\n\n");

			//HostTrap(14);
			}

		PollRunning();
		}
	else
		PollStopped();

	if( m_uLoadTime && !--m_uLoadTime ) {

		CLoadCounter().SetFlag(TRUE);

		BeepOff();

		AfxTrace("\n*** LOAD TIMEOUT\n\n");

		//for(;;) HostMaxIPL();
		}

	m_uTimer++;
	}

// Timer Helpers

void CControlTask::FindLedType(void)
{
	if( !stricmp(WhoGetName(FALSE), "GC") || !stricmp(WhoGetName(FALSE), "GCN") ) {

		m_uLedType = ledEdge;

		return;
		}

	if( !stricmp(WhoGetName(FALSE), "DA50") || !stricmp(WhoGetName(FALSE), "DA70") ) {

		m_uLedType = ledDA;

		return;
		}

	m_uLedType = ledStd;
	}

void CControlTask::PollSiren(void)
{
	if( m_fSiren ) {

		if( m_uSiren == NOTHING ) {

			m_uSiren = 0;
			}

		if( m_uSiren % FLASH_RATE == 0 ) {

			UINT n = m_uSiren / FLASH_RATE;

			if( n < 6 ) {

				switch( n % 3 ) {

					case 0:
						Beep(72, 150);

						break;

					case 1:
						Beep(84, 150);

						break;

					case 2:
						Beep(96, 150);

						break;
					}
				}

			UINT d = (n % 2) ? 1 : 2;

			DispSetBrightness(m_uBright / d);
			}

		m_uSiren = (m_uSiren + 1) % (8 * FLASH_RATE);

		return;
		}

	if( m_uSiren < NOTHING ) {

		DispSetBrightness(m_uBright);

		m_uSiren = NOTHING;
		}
	}

void CControlTask::PollRunning(void)
{
	if( m_uLedType == ledEdge ) {

		KeySetLED(1, TRUE);

		PollDriveStatus(2, TRUE);
	
		return;
		}

	if( !(m_uLedMode & (1 << 0)) ) {

		BOOL fAlarm;

		switch( m_uAlarm ) {
		
			case 2:	
				fAlarm = (m_uTimer / FLASH_RATE) % 2;
				break;

			case 1:	
				fAlarm = TRUE;
				break;

			default:
				fAlarm = FALSE;
				break;
			}

		if( m_uLedType == ledDA ) {

			KeySetLED(1, fAlarm ? stateRed : stateGreen);
			}
		else {
			KeySetLED(0, fAlarm ? stateOn : stateOff);
			}
		}

	if( !(m_uLedMode & (1 << 1)) ) {

		if( m_uLedType == ledDA ) {

			PollDriveStatus(2, TRUE);
			}
		else {
			PollDriveStatus(1, TRUE);
			}
		}

	if( !(m_uLedMode & (1 << 2)) ) {

		if( m_uLedType != ledDA ) {

			KeySetLED(2, TRUE);
			}
		}	
	}

void CControlTask::PollStopped(void)
{
	if( m_uLedType == ledEdge ) {

		KeySetLED(1, TRUE);

		PollDriveStatus(2, FALSE);
		}
	else {
		KeySetLED(0, stateOff);

		if( m_uLedType == ledDA ) {

			KeySetLED(1, stateRed);

			PollDriveStatus(2, TRUE);
			}
		else {
			PollDriveStatus(1, stateOff);

			KeySetLED(2, stateOn);
			}
		}
	}

void CControlTask::PollDriveStatus(UINT uLED, BOOL fState)
{
	#if 0 // !!!

	if( m_pDiskMan ) {

		switch( m_pDiskMan->GetDiskStatus(m_iDriveC) ) {

			case diskEmpty:

				KeySetLED(uLED, stateOff);

				break;

			case diskInvalid:

				KeySetLED(uLED, (m_uTimer & 0x10) ? stateRed : stateOff);

				break;

			case diskCheck:
			case diskFormat:

				KeySetLED(uLED, (m_uTimer & 0x04) ? stateGreen : stateOff);

				break;

			case diskLocked:

				KeySetLED(uLED, (m_uTimer & 0x01) ? stateGreen : stateOff);

				break;

			case diskReady:

				KeySetLED(uLED, fState ? stateGreen : stateOff);

				break;
			}
		}

	#endif
	}

// Power Up

void CControlTask::PowerUp(void)
{
	OemInit();

	ByeByeSetHook(ByeBye);

	CreateGDI();

	ShowTitle();

	m_t1 = GetTickCount();

	CreateSMS();

	g_pDbase->Init();

	CreatePNP();
	}

void CControlTask::CreateGDI(void)
{
	if( DispCheck() ) {

		int cx = DispGetCx();
	
		int cy = DispGetCy();

		if( cx && cy ) {

			g_pGDI = Create_GDI();
		
			g_pGDI->Create(cx, cy, NULL);

			switch( cx ) {

				case 800:
				
					m_fSmall  = FALSE;
				
					m_fNarrow = FALSE;

					break;

				case 640:
				
					m_fSmall  = FALSE;
				
					m_fNarrow = FALSE;
		
					break;

				case 480:
				
					m_fSmall  = FALSE;
				
					m_fNarrow = TRUE;

					break;

				case 320:
				
					m_fSmall  = TRUE;
				
					m_fNarrow = FALSE;
		
					break;

				case 240:
				
					m_fSmall  = TRUE;
				
					m_fNarrow = TRUE;

					break;
				}
			}
		}
	}

void CControlTask::CreateSMS(void)
{
	if( WhoHasFeature(rfModems) ) {

		CProxySMS::Create();
		}
	}

BOOL CControlTask::CreatePNP(void)
{
	g_pUsbMan = Create_UsbManager();

	if( g_pUsbMan ) {
		
		return g_pUsbMan->Open();
		}

	return FALSE;
	}

void CControlTask::LoadNetwork(void)
{
	if( !m_fSkipNet ) {

		CNetConfig::Init();
		}
	}

// Programming Link

BOOL CControlTask::CreateGrab(void)
{
	g_uGrabber = 0;

	g_pGrabber = Create_PortGrabber();

	return TRUE;
	}

void CControlTask::CreateLink(void)
{
	ILinkService   *pRouter = Create_RouterService();

	IUsbFuncStack  *pFunc   = NULL;

	IUsbDriver     *pCtrl   = NULL;

	IUsbFuncEvents *pLink   = Create_UsbLink(32500, pRouter);

	AfxGetObject("usb.func",  0, IUsbFuncStack, pFunc);

	AfxGetObject("usbctrl-d", 0, IUsbDriver, pCtrl);

	if( pFunc && pCtrl && pLink ) {

		pFunc->Bind((IUsbFuncHardwareDriver *) pCtrl);

		pFunc->Bind(pLink);

		pFunc->SetProduct(GetOemDriverBase() + 0x0201, "HMI");

		pFunc->Start();
		}

	Create_XPSerial (20000, g_uGrabber, pRouter, g_pGrabber);

	Create_XPNetwork(25000, pRouter);
	}

// Key Sequences

BOOL CControlTask::CheckFailed(void)
{
	if( WhoHasFeature(rfDisplay) ) {

		CLoadCounter Count;

		Count -= 1;

		ShowError( "FAILED TO START",
			   "%s to clear database",
			   "%s to try again"
			   );

		UINT uCode = GetAnswer(60000);

		if( uCode == 1 ) {

			g_pDbase->SetValid(FALSE);

			return TRUE;
			}

		if( uCode == 2 ) {

			return FALSE;
			}

		if( uCode == NOTHING ) {

			if( !CLoadCounter().GetFlag() ) {

				return TRUE;
				}

			g_pDbase->SetValid(FALSE);

			return TRUE;
			}
		}

	g_pDbase->SetValid(FALSE);

	return TRUE;
	}

BOOL CControlTask::CheckBattery(void)
{
	if( WhoHasFeature(rfDisplay) ) {		

		BYTE bData[2];

		FRAMGetData(Mem(Battery), bData, 2);

		if( bData[0] == BATT_MAGIC_1 ) {

			if( bData[1] == BATT_MAGIC_2 ) {

				return TRUE;
				}
			}

		if( !IsBatteryOK() ) {

			ShowError( "INTERNAL BATTERY IS LOW",
				   "%s to continue",
				   ""
				   );

			GetAnswer(60000);

			return FALSE;
			}
		}

	return TRUE;
	}

// System Loading

BOOL CControlTask::LoadSystem(void)
{
	xm1 = AfxMarkMemory();

	PCBYTE pData = PCBYTE(g_pDbase->LockItem(1));

	if( pData ) {
		
		WORD wForm = MotorToHost(PWORD(pData)[0]);

		WORD wType = MotorToHost(PWORD(pData)[1]);

		if( wForm == DBASE_FORMAT ) {

			if( wType == 0x0002 ) {

				pData += 4;

				if( SystemLoad(pData) ) {
					
					g_pDbase->FreeItem(1);

					AfxMarkMemory();

					return TRUE;
					}
				}
			}

		g_pDbase->FreeItem(1);

		return FALSE;
		}
	
	g_pDbase->SetValid(FALSE);

	return FALSE;
	}

void CControlTask::FreeSystem(void)
{
	delete CSystemItem::m_pThis;

	AfxDumpMemory(NULL, xm1);
	}

void CControlTask::SetLoading(BOOL fLoad)
{
	if( fLoad ) {

		CLoadCounter Count;

		Count += 1;			

		m_uLoadTime = ToTimer(1000) * LOAD_TIMEOUT;
		}
	else {
		CLoadCounter Count;

		Count = 0;

		Count.SetFlag(FALSE);

		m_uLoadTime = 0;
		}
	}

BOOL CControlTask::WasLoading(void)
{
	// TODO -- Isn't working on MC for some reason?

	CLoadCounter Count;

	return Count >= LOAD_CYCLES;
	}

void CControlTask::MinWait(UINT dt)
{
	if( m_t1 ) {

		UINT t2 = GetTickCount();

		UINT dd = ToTime(t2 - m_t1);

		if( dd < dt ) {

			Sleep(dt - dd);
			}

		m_t1 = 0;
		}
	}

// Task Management

void CControlTask::PerformStart(void)
{
	m_pAckStop->Clear();

	m_pAckStart->Set();

	ShowStatus("LOADING DATABASE");

	SetLoading(TRUE);

	g_pPersist->Init();

	g_pEvStg  ->Init();

	TimeZoneInit();

	if( LoadSystem() ) {

		m_fLoaded = TRUE;

		SaveImage();

		ShowStatus("STARTING SYSTEM");

		NotifyPNP(0);

		g_pDbase->SetRunning(TRUE);

		CSystemItem::m_pThis->SystemStart();

		CreateTasks();

		if( !m_fSkipNet ) {

			CNetConfig::Update();
			}

		xm2 = AfxMarkMemory();

		InitTasks();

		CSystemItem::m_pThis->SystemInit();

		if( WhoHasFeature(rfModems) ) {

			CProxySMS::m_pThis->SystemStarted();
			}

		NotifyPNP(1);

		StartTasks();

		Sleep(100);

		SetLoading(FALSE);
	
		return;
		}

	if( g_pDbase->IsValid() ) {		

		ShowStatus("VERSION MISMATCH");		
		}
	else
		ShowInvalid();

	SetLoading(FALSE);
	}

void CControlTask::PerformStop(void)
{
	if( m_fLoaded ) {

		CSystemItem::m_pThis->SystemSave();

		g_pPersist->Commit(false);

		if( g_pProm ) {
		
			g_pProm->Sync(TRUE);
			}

		if( WhoHasFeature(rfModems) ) {

			CProxySMS::m_pThis->SystemStopped();
			}

		StopTasks();

		ShowStatus("UPDATING DATABASE");

		SetAlarm(0);

		SetSiren(FALSE);

		NotifyPNP(2);

		CSystemItem::m_pThis->SystemTerm();

		g_pPersist->Term();

		TermTasks();

		g_pDbase->SetRunning(FALSE);

		CSystemItem::m_pThis->SystemStop();

		FreeSystem();

		DeleteTasks();

		AfxDumpMemory(NULL, xm2);

		NotifyPNP(3);

		m_fLoaded = FALSE;
		}
	else {
		ShowStatus("UPDATING DATABASE");

		SetAlarm(0);

		SetSiren(FALSE);

		g_pPersist->Term();
		}

	if( m_hUpdate ) {

		CItemInfo Info;

		Info.m_uItem  = m_hUpdate;
		Info.m_uClass = 0xFFFF;
		Info.m_uSize  = m_uUpdate;
		Info.m_uComp  = m_uUpdate;

		g_pDbase->WriteItem(Info, m_pUpdate);

		delete [] m_pUpdate;
		}

	if( m_uReboot ) {

		StopDrives();
		
		RunBoot(m_uReboot == 1 ? 0 : FOREVER);
		}

	if( TRUE ) {

		m_pAckStart->Clear();

		m_pAckStop->Set();
		}

	if( m_hUpdate ) {
		
		m_hUpdate = 0;

		m_uUpdate = 0;

		m_pUpdate = NULL;

		m_pReqStop ->Clear();

		m_pReqStart->Set();
		}
	}

void CControlTask::LockDrives(BOOL fLock)
{
	// TODO -- !!!
	}

void CControlTask::StopDrives(void)
{
	// TODO -- !!!
	}

void CControlTask::NotifyPNP(UINT uMsg)
{
	if( g_pUsbMan ) {

		g_pUsbMan->Notify(uMsg, 0);
		}
	}

void CControlTask::GetTaskList(CTaskList &List)
{
	CTaskDef Task;

	Task.m_Name   = "PROM";
	Task.m_pEntry = NULL;
	Task.m_uID    = 0;
	Task.m_uCount = 1;
	Task.m_uLevel = 1200;
	Task.m_uStack = 0;

	List.Append(Task);

	Task.m_Name   = "TEST";
	Task.m_pEntry = NULL;
	Task.m_uID    = 1;
	Task.m_uCount = 1;
	Task.m_uLevel = 100;
	Task.m_uStack = 0;

	List.Append(Task);

	CSystemItem::m_pThis->GetTaskList(m_List);
	}

void CControlTask::CreateTasks(void)
{
	AfxTrace("=== CreateThreads\n");

	m_List.Empty();

	m_Task.Empty();

	GetTaskList(m_List);

	UINT c = m_List.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		CTaskDef const &Task = m_List[n];
	
		for( UINT i = 0; i < Task.m_uCount; i++ ) {

			UINT    uCode = n * 10 + i;

			HTHREAD hTask = CreateClientThread(this, uCode, Task.m_uLevel + i, m_pTms);

			m_Task.Append(hTask);
			}
		}

	WaitTransition();
	}

void CControlTask::InitTasks(void)
{
	AfxTrace("=== InitThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		AdvanceThread(m_Task[n]);
		}

	WaitTransition();
	}

void CControlTask::StartTasks(void)
{
	AfxTrace("=== StartThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		AdvanceThread(m_Task[n]);
		}

	WaitTransition();

	m_fRun = TRUE;
	}

void CControlTask::StopTasks(void)
{
	AfxTrace("=== StopThreads\n");

	LockDrives(TRUE);

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		DestroyThread(m_Task[n]);
		}

	LockDrives(FALSE);

	WaitTransition();

	m_fRun = FALSE;
	}

void CControlTask::TermTasks(void)
{
	AfxTrace("=== TermThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		AdvanceThread(m_Task[n]);
		}

	WaitTransition();
	}

void CControlTask::DeleteTasks(void)
{
	AfxTrace("=== DeleteThreads\n");

	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		WaitThread(m_Task[n], FOREVER);

		m_Task[n]->Release();
		}

	m_Task.Empty();

	m_List.Empty();

	AfxTrace("=== Done\n");
	}

BOOL CControlTask::WaitTransition(void)
{
	UINT c = m_Task.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_pTms->Wait(30000) ) {

			continue;
			}

		// TODO -- This is actually fatal !!!

		AfxTrace("=== Transition Timeout!\n");

		return FALSE;
		}

	AfxTrace("=== Transition Complete (%u)\n", c);

	return TRUE;
	}

// Configuration Backup

BOOL CControlTask::SaveImage(void)
{
	if( !stricmp(GetOemName(), "Monico") ) {

		#if 0 // !!!

		if( m_pDiskMan && m_pVolMan ) {

			IFilingSystem *pFileSys = m_pVolMan->GetFileSystem(m_pDiskMan->FindVolume('C'));

			if( pFileSys ) {

				pFileSys->WaitLock(FOREVER);

				if( pFileSys->IsMounted() ) {
				
					CItemInfo Info;

					if( g_pDbase->GetItemInfo(0, Info) ) {

						UINT uSize = Info.m_uSize;

						if( uSize ) {

							PBYTE pData = PBYTE(g_pDbase->LockItem(0));

							UINT  uKeep = 8;
						
							if( uSize && pData ) {

								ShowStatus("SAVING CONFIG BACKUP");

								pFileSys->CreateDir("\\backup");

								pFileSys->ChangeDir("\\backup");

								IFile *pFile = Create_File(pFileSys);

								for( UINT uFile = 0; uFile < uKeep; uFile++ ) {

									char sName[32];

									SPrintf(sName, "ver%2.2u.dat", uFile);

									if( pFile->Open(sName, fileRead) ) {

										if( uFile == 0 ) {

											PBYTE pRead = New BYTE [ uSize ];

											pFile->Read(pRead, uSize);

											if( !memcmp(pRead, pData, uSize) ) {

												AfxTrace("backup: no change\n");

												pFile->Close();

												pFile->Release();

												pFileSys->FreeLock();
			
												pFileSys->Release();

												g_pDbase->FreeItem(0);

												delete [] pRead;

												return FALSE;
												}

											delete [] pRead;
											}

										pFile->Close();

										continue;
										}

									break;
									}

								if( TRUE ) {

									char sName[32];

									if( uFile == uKeep ) {

										SPrintf(sName, "ver%2.2u.dat", uFile-1);

										if( !pFile->Open(sName, fileWrite | fileRead) ) {

											pFile->Release();

											pFileSys->FreeLock();
			
											pFileSys->Release();

											g_pDbase->FreeItem(0);

											return FALSE;
											}

										AfxTrace("backup: re-using file %s\n", sName);
										}
									else {
										SPrintf(sName, "ver%2.2u.dat", uFile++);

										if( !pFile->Create(sName) ) {

											pFile->Release();

											pFileSys->FreeLock();
			
											pFileSys->Release();

											return FALSE;
											}

										AfxTrace("backup: creating file %s\n", sName);
										}

									m_uLoadTime = ToTimer(1000) * 360;

									pFile->Write(pData, uSize);

									pFile->Close();
									}

								if( uFile > 1 ) {
						
									for( UINT uScan = uFile; uScan; uScan-- ) {

										char sOld[32], sNew[32];

										SPrintf(sOld, "ver%2.2u.dat", uScan - 1);

										SPrintf(sNew, "ver%2.2u.dat", uScan - 0);

										AfxTrace("backup: %s -> %s\n", sOld, sNew);

										pFile->Open(sOld, fileWrite);

										pFile->Rename(sNew);

										pFile->Close();
										}

									char sOld[32], sNew[32];

									SPrintf(sOld, "ver%2.2u.dat", uFile);

									SPrintf(sNew, "ver%2.2u.dat", 0);

									AfxTrace("backup: %s -> %s\n", sOld, sNew);

									pFile->Open(sOld, fileWrite);

									pFile->Rename(sNew);

									pFile->Close();
									}

								pFile->Release();

								pFileSys->FreeLock();
			
								pFileSys->Release();

								return TRUE;
								}
							}
						}
					}

				pFileSys->FreeLock();
			
				pFileSys->Release();
				}
			}

		#endif
		}

	return FALSE;
	}

// User Interface

void CControlTask::DrawFrame(void)
{
	if( g_pGDI ) {

		int cx = g_pGDI->GetCx();

		int cy = g_pGDI->GetCy();

		g_pGDI->BufferClaim();

		g_pGDI->ClearScreen(0);

		g_pGDI->ResetAll();

		m_uFont[0] = m_fSmall ? fontHei16     : fontHei24;

		m_uFont[1] = m_fSmall ? fontHei16Bold : fontHei24Bold;

		m_uFont[2] = m_fSmall ? fontHei12     : fontHei18;
			
		m_uFont[3] = m_fSmall ? fontHei10     : fontHei16;

		g_pGDI->DrawRounded(1, 1, cx-1, cy-1, 16);

		g_pGDI->DrawRounded(5, 5, cx-5, cy-5, 12);

		g_pGDI->SetTextTrans(modeTransparent);
		}
	}

void CControlTask::TextOut(int yp, PCTXT pText, UINT uFont)
{
	if( g_pGDI ) {

		g_pGDI->SelectFont(m_uFont[uFont]);

		int dx = g_pGDI->GetCx();

		int cx = g_pGDI->GetTextWidth(pText);

		int xp = (dx - cx) / 2;

		g_pGDI->TextOut(xp, yp, pText);
		}
	}

void CControlTask::ShowTitle(void)
{
	if( g_pGDI ) {

		char sName[64] = { 0 };

		char sCopy[64] = { 0 };

		SPrintf(sName, "%s Runtime", GetOemAppName());

		if( !m_fNarrow ) {

			PCTXT pForm = "Copyright %c 1993-2019 %s";

			SPrintf(sCopy, pForm, 0xA9, "Red Lion Controls Inc");
			}
		else {
			PCTXT pForm = "%c 1993-2019 %s";

			SPrintf(sCopy, pForm, 0xA9, "Red Lion Controls");
			}

		DrawFrame();

		int cy = g_pGDI->GetCy();

		if( DebugMode() ) {

			char s[64];

			sprintf(s, "Debug Build 3.2.%4.4u.%u", C3_BUILD, C3_HOTFIX);

			TextOut(3 * cy / 10, sName, 1);

			TextOut(4 * cy / 10, s, 0);

			if( ShowCopyright() ) {

				TextOut(6 * cy / 10, sCopy, 2);
				}

			TextOut(7 * cy / 10, "NOT FOR RELEASE", 1);
			}
		else {
			char s[64];

			sprintf(s, "Release Build 3.2.%4.4u.%u", C3_BUILD, C3_HOTFIX);

			TextOut(3 * cy / 10, sName, 1);

			TextOut(4 * cy / 10, s, 0);

			if( ShowCopyright() ) {

				TextOut(6 * cy / 10, sCopy, 2);
				}
			}
		
		g_pGDI->BufferFree();

		DispUpdate(g_pGDI->GetBuffer());
		}
	}

BOOL CControlTask::ShowCopyright(void)
{
	if( !strcmp(GetOemAppName(), "CS&P Designer") ) {

		return FALSE;
		}

	return TRUE;
	}

void CControlTask::ShowDebug(PCTXT pForm, ...)
{
	if( g_pGDI ) {

		va_list pArgs;

		va_start(pArgs, pForm);

		SysDebug(pForm, pArgs);

		va_end(pArgs);
		}
	}

void CControlTask::ShowStatus(PCTXT pText)
{
	if( g_pGDI ) {

		DrawFrame();

		int cy = g_pGDI->GetCy();

		TextOut(4 * cy / 10, pText, 0);

		g_pGDI->BufferFree();

		DispUpdate(g_pGDI->GetBuffer());
		}

	if( 1 || !WhoHasFeature(rfDisplay) ) {
		
		AfxTrace("%s\n", pText);
		}
	}

void CControlTask::ShowInvalid(void)
{
	if( g_pGDI ) {

		DrawFrame();

		int   cy   = g_pGDI->GetCy();

		INic *pNic = NULL;

		AfxGetObject("nic", 0, INic, pNic);

		if( pNic ) {

			char s1[128];
			
			char s2[128];

			char s3[128];

			CMACAddr Mac;

			if( g_pRouter ) {

				CIPAddr  Addr;

				CIPAddr  Mask;

				CIPAddr  Gate;

				g_pRouter->GetConfig(1, Addr, Mask, Mac);

				g_pRouter->GetGateway(Gate);

				strcpy(s1, "Network Settings are ");

				strcat(s1, Addr.GetAsText());

				strcat(s1, ", ");

				strcat(s1, Mask.GetAsText());

				strcat(s1, ", ");

				strcat(s1, Gate.GetAsText());

				strcpy(s3, "with TCP/IP download enabled on port 789.");

				strcpy(s2, "MAC Address is ");

				strcat(s2, Mac.GetAsText());

				strcat(s2, ".");
				}
			else {
				pNic->ReadMac(Mac);

				strcpy(s1, "Network Settings are not configured");

				strcpy(s3, "and TCP/IP download is disabled.");

				strcpy(s2, "MAC Address is ");

				strcat(s2, Mac.GetAsText());

				strcat(s2, ".");
				}

			if( WhoHasFeature(rfKeyboard) ) {

				if( g_pRouter ) {
			
					PCTXT pText = "Hold 1st and 2nd keys on power-up to edit Network Settings.";

					TextOut(8 * cy / 10, pText, 3);
					}
				else {
					PCTXT pText = "Hold 1st and 2nd keys on power-up to configure Network Settings.";

					TextOut(8 * cy / 10, pText, 3);
					}
				}

			TextOut(10 * cy / 20, s1, 3);

			TextOut(11 * cy / 20, s3, 3);

			TextOut(14 * cy / 20, s2, 3);			

			TextOut( 6 * cy / 20, "INVALID DATABASE", 0);

			pNic->Release();
			}
		else
			TextOut(5 * cy / 10, "INVALID DATABASE", 0);

		if( !WhoHasFeature(rfDisplay) ) {
		
			AfxTrace("INVALID DATABASE\n");
			}

		g_pGDI->BufferFree();

		DispUpdate(g_pGDI->GetBuffer());
		}
	}

void CControlTask::ShowError(PCTXT pHead, PCTXT pOne, PCTXT pTwo)
{
	if( g_pGDI ) {

		int cx = g_pGDI->GetCx();

		int cy = g_pGDI->GetCy();

		DrawFrame();

		TextOut(3 * cy / 10, pHead, 1);

		if( WhoHasFeature(rfKeyboard) ) {

			TextOut(5 * cy / 10, CPrintf(pOne, "Soft-Key 1"), 0);

			TextOut(6 * cy / 10, CPrintf(pTwo, "Soft-Key 2"), 0);
			}
		else {
			if( !strcmp(WhoGetName(FALSE), "VS") ) {

				if( *pTwo ) {
					
					// TODO:  What text should be displayed?

					}
				else
					TextOut(5 * cy / 10, CPrintf(pOne, "Please Wait"), 0);
				}

			else {
				if( *pTwo ) {

					TextOut(5 * cy / 10, CPrintf(pOne, "Touch LEFT SIDE"),  0);

					TextOut(6 * cy / 10, CPrintf(pTwo, "Touch RIGHT SIDE"), 0);
					}
				else
					TextOut(5 * cy / 10, CPrintf(pOne, "Touch screen"), 0);
				}
			}

		g_pGDI->BufferFree();

		DispUpdate(g_pGDI->GetBuffer());
		}
	}

UINT CControlTask::GetAnswer(UINT uTime)
{
	if( WhoHasFeature(rfKeyboard) ) {

		return GetAnswerKeys(uTime);
		}

	if( TouchCheck() ) {

		return GetAnswerTouch(uTime);
		}

	return 2;
	}

UINT CControlTask::GetAnswerKeys(UINT uTime)
{
	UINT uCode = NOTHING;

	BOOL fDown[2];

	fDown[0] = FALSE;

	fDown[1] = FALSE;

	for(;;) {
	
		INPUT dwRead = InputRead(uTime);
		
		if( dwRead ) {

			CInput &i = (CInput &) dwRead;

			if( i.x.i.m_Type == typeKey ) {

				if( i.x.d.k.m_Code == VK_SOFT1 ) {
					
					if( i.x.i.m_State == stateDown ) {
						
						fDown[0] = TRUE;
						}

					if( i.x.i.m_State == stateUp ) {

						if( fDown[0] ) {

							uCode = 1;

							break;
							}
						}
					}

				if( i.x.d.k.m_Code == VK_SOFT2 ) {
					
					if( i.x.i.m_State == stateDown ) {

						fDown[1] = TRUE;
						}

					if( i.x.i.m_State == stateUp ) {

						if( fDown[1] ) {

							uCode = 2;

							break;
							}
						}	
					}
				}

			continue;
			}

		break;
		}

	DrawFrame();

	if( g_pGDI ) {

		g_pGDI->BufferFree();

		DispUpdate(g_pGDI->GetBuffer());
		}

	return uCode;
	}

UINT CControlTask::GetAnswerTouch(UINT uTime)
{
	UINT uCode = NOTHING;

	int cx = g_pGDI->GetCx();

	int cy = g_pGDI->GetCy();

	ITouchMap *pTouch = Create_TouchMap();

	pTouch->FillRect(0, 0, cx, cy);

	TouchSetMap(pTouch->GetBuffer());

	for(;;) {
	
		INPUT dwRead = InputRead(uTime);

		if( dwRead ) {

			CInput &i = (CInput &) dwRead;

			if( i.x.i.m_Type == typeTouch ) {

				if( i.x.i.m_State == stateUp ) {

					int xp = 4 * i.x.d.t.m_XPos;

					if( xp < cx / 2 ) {

						uCode = 1;

						break;
						}

					uCode = 2;

					break;
					}
				}

			continue;
			}

		break;
		}

	TouchSetMap(NULL);

	pTouch->Release();

	DrawFrame();

	g_pGDI->BufferFree();

	DispUpdate(g_pGDI->GetBuffer());

	return uCode;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Hooks
//

global BOOL ControlInit(void)
{
	extern void CreateTargetObjects(void);

	CreateTargetObjects();

	AfxGetObject("tls", 0, ITlsLibrary, g_pMatrix);

	New CControlTask;

	CControlTask::m_pThis->Init();

	return TRUE;
	}

global int ControlExec(void)
{
	CControlTask::m_pThis->Exec();

	return 0;
	}

global void ControlStop(void)
{
	CControlTask::m_pThis->Stop();
	}

global void ControlTerm(void)
{
	CControlTask::m_pThis->Term();

	delete CControlTask::m_pThis;
	}

global BOOL SystemAuto(void)
{
	return CControlTask::m_pThis->Auto();
	}

global BOOL SystemStart(void)
{
	return CControlTask::m_pThis->Start();
	}

global BOOL SystemStop(void)
{
	return CControlTask::m_pThis->Stop(30000);
	}

global BOOL SystemStop(UINT uTimeout)
{
	return CControlTask::m_pThis->Stop(uTimeout);
	}

global void SystemUpdate(UINT hItem, PCBYTE pData, UINT uSize)
{
	CControlTask::m_pThis->Update(hItem, pData, uSize);
	}

global void SystemReboot(void)
{
	CControlTask::m_pThis->Reboot();
	}

global void SystemRestart(void)
{
	CControlTask::m_pThis->Restart();
	}

global BOOL EnableMount(BOOL fEnable)
{
	return CControlTask::m_pThis->EnableMount(fEnable);
	}

global void SetAlarm(UINT uState)
{
	CControlTask::m_pThis->SetAlarm(uState);
	}

global void SetSiren(BOOL fOn)
{
	CControlTask::m_pThis->SetSiren(fOn);
	}

global BOOL GetSiren(void)
{
	return CControlTask::m_pThis->GetSiren();
	}

global void KickTimeout(void)
{
	CControlTask::m_pThis->SetTimeout();
	}

global void SysDebug(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	CControlTask::m_pThis->SysDebug(pText, pArgs);

	va_end(pArgs);
	}

global void SetLedMode(UINT uMode)
{
	CControlTask::m_pThis->SetLedMode(uMode);
	}

global UINT GetLedMode(void)
{
	return CControlTask::m_pThis->GetLedMode();
	}

global void SkipNetUpdate(void)
{
	CControlTask::m_pThis->SkipNetUpdate();
	}

global BOOL IsFlashEnabled(void)
{
	return CControlTask::m_pThis->IsFlashEnabled();
	}

global BOOL IsRunningControl(void)
{
	return CControlTask::m_pThis->IsRunningControl();
	}

// End of File
