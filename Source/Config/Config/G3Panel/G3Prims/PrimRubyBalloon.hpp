
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBalloon_HPP
	
#define	INCLUDE_PrimRubyBalloon_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyOriented.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Balloon Primitive
//

class CPrimRubyBalloon : public CPrimRubyOriented
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyBalloon(void);

		// Overridables
		void UpdateLayout(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void FindTextRect(void);

	protected:
		// Data Members
		CPoint m_Min1;
		CPoint m_Max1;
		CPoint m_Pos1;
		INT    m_Min2;
		INT    m_Max2;
		INT    m_Pos2;
		INT    m_Min3;
		INT    m_Max3;
		INT    m_Pos3;

		// Meta Data
		void AddMetaData(void);

		// Path Generation
		void MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
