
#include "Intern.hpp"

#include "SlaveSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP Slave Session - Device
//

// Constructor

CSlaveSession::CSlaveSession(IDnpChannel *pChan, WORD wDest, WORD wTO, DWORD dwLink, void * pCfg) : CSession(pChan, wDest, wTO, dwLink)
{
	sdnpsesn_initConfig(&m_Config);

	m_Config.destination = wDest;

	m_Config.source = pChan->GetSource();

	m_Config.applConfirmTimeout  = wTO;

	m_Config.unsolConfirmTimeout = wTO;

	m_pDatabase = new CUserObjects;

	if( Open(pChan) ) {

		m_pSession->pUserData = m_pDatabase;

		sdnpdata_init(m_pSession, m_pDatabase);

		m_Config.pStatCallback = StatCallback;

		m_Config.pStatCallbackParam    = this;

		m_Config.unsolClass1MaxDelay   = 0;

		m_Config.unsolClass2MaxDelay   = 0;

		m_Config.unsolClass3MaxDelay   = 0;

		m_Config.obj01DefaultVariation = 0;

		m_Config.obj02DefaultVariation = 0;

		m_Config.obj03DefaultVariation = 0;

		m_Config.obj04DefaultVariation = 0;

		m_Config.obj10DefaultVariation = 0;

		m_Config.obj11DefaultVariation = 0;

		m_Config.obj13DefaultVariation = 0;

		m_Config.obj20DefaultVariation = 0;

		m_Config.obj21DefaultVariation = 0;

		m_Config.obj22DefaultVariation = 0;

		m_Config.obj23DefaultVariation = 0;

		m_Config.obj30DefaultVariation = 0;

		m_Config.obj32DefaultVariation = 0;

		m_Config.obj34DefaultVariation = 0;

		m_Config.obj40DefaultVariation = 0;

		m_Config.obj42DefaultVariation = 0;

		m_Config.obj43DefaultVariation = 0;

		m_Config.linkStatusPeriod      = dwLink;

		SetEventConfig((CEventConfig *) pCfg);

		sdnpsesn_setSessionConfig(m_pSession, &m_Config);
	}
}

// Destructor

CSlaveSession::~CSlaveSession(void)
{
	Close();

	delete m_pDatabase;
}

// IUnknown

HRESULT CSlaveSession::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IDnpSlaveSession);

	return CSession::QueryInterface(riid, ppObject);
}

ULONG CSlaveSession::AddRef(void)
{
	return CSession::AddRef();
}

ULONG CSlaveSession::Release(void)
{
	return CSession::Release();
}

// IDnpSession

BOOL METHOD CSlaveSession::Open(IDnpChannel * pChan)
{
	m_pSession = sdnpsesn_openSession((dnpChan *) pChan->GetChannel(), &m_Config, m_pDatabase);

	return m_pSession ? TRUE : FALSE;
}

BOOL METHOD CSlaveSession::Close(void)
{
	if( m_pSession ) {

		if( sdnpsesn_closeSession(m_pSession) ) {

			m_pSession = NULL;

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL METHOD CSlaveSession::Ping(void)
{
	return CSession::Ping();
}

UINT METHOD CSlaveSession::Validate(BYTE o, WORD i, BYTE t, UINT uCount)
{
	return CSession::Validate(o, i, t, uCount);
}

UINT METHOD CSlaveSession::GetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount)
{
	return CSession::GetValue(o, i, t, pData, uCount);
}

UINT METHOD CSlaveSession::GetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetFlags(o, i, pData, uCount);
}

UINT METHOD CSlaveSession::GetTimeStamp(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetTimeStamp(o, i, pData, uCount);
}

UINT METHOD CSlaveSession::GetClass(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetClass(o, i, pData, uCount);
}

UINT METHOD CSlaveSession::GetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetOnTime(o, i, pData, uCount);
}

UINT METHOD CSlaveSession::GetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	return CSession::GetOffTime(o, i, pData, uCount);
}

// IDnpSlaveSession

UINT METHOD CSlaveSession::SetValue(BYTE o, WORD i, BYTE t, PDWORD pData, UINT uCount, BYTE eMask)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u, t);

			if( pUser ) {

				dnpTIME TS;

				Sys_GetDateTime(&TS);

				AddEvent(pUser, pData, TRUE, &TS, eMask);

				pData = pUser->SetData(pData, &TS);

				u++;

				continue;
			}

			break;
		}

		return u;
	}

	return CCODE_ERROR | CCODE_HARD;
}

UINT METHOD CSlaveSession::SetFlags(BYTE o, WORD i, PDWORD pData, UINT uCount, BYTE eMask)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				dnpTIME TS;

				Sys_GetDateTime(&TS);

				AddEvent(pUser, pData, FALSE, &TS, eMask);

				pUser->SetFlag(pData[u] & 0xFF);

				u++;

				continue;
			}

			break;
		}

		return u;
	}

	return CCODE_ERROR | CCODE_HARD;
}

UINT METHOD CSlaveSession::SetClass(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pUser->SetClass(pData[u] & 0xFF);

				u++;

				continue;
			}

			break;
		}

		return u;
	}

	return CCODE_ERROR | CCODE_HARD;
}

UINT METHOD CSlaveSession::SetOnTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pUser->SetOnTime(pData[u]);

				u++;

				continue;
			}

			break;
		}

		return u;
	}

	return CCODE_ERROR | CCODE_HARD;
}

UINT METHOD CSlaveSession::SetOffTime(BYTE o, WORD i, PDWORD pData, UINT uCount)
{
	CUserTable * pTable = m_pDatabase->Find(o);

	if( pTable ) {

		UINT u = 0;

		while( u < uCount ) {

			CUserData * pUser = pTable->Find(i + u);

			if( pUser ) {

				pUser->SetOffTime(pData[u]);

				u++;

				continue;
			}

			break;
		}

		return u;
	}

	return CCODE_ERROR | CCODE_HARD;
}

// Implementation

void CSlaveSession::SetEventConfig(CEventConfig * pCfg)
{
	if( pCfg ) {

		m_Config.binaryInputEventMode   = (dnpEM) pCfg[eventBI].m_bMode;

		m_Config.binaryInputMaxEvents   = pCfg[eventBI].m_wLimit;

		m_Config.doubleInputEventMode   = (dnpEM) pCfg[eventDBI].m_bMode;

		m_Config.doubleInputMaxEvents   = pCfg[eventDBI].m_wLimit;

		m_Config.binaryOutputEventMode  = (dnpEM) pCfg[eventBO].m_bMode;

		m_Config.binaryOutputMaxEvents  = pCfg[eventBO].m_wLimit;

		m_Config.binaryOutCmdEventMode  = (dnpEM) pCfg[eventBO].m_bMode;

		m_Config.binaryOutCmdMaxEvents  = pCfg[eventBO].m_wLimit;

		m_Config.binaryCounterEventMode = (dnpEM) pCfg[eventC].m_bMode;

		m_Config.binaryCounterMaxEvents = pCfg[eventC].m_wLimit;

		m_Config.frozenCounterEventMode = (dnpEM) pCfg[eventFC].m_bMode;

		m_Config.frozenCounterMaxEvents = pCfg[eventFC].m_wLimit;

		m_Config.analogInputEventMode   = (dnpEM) pCfg[eventAI].m_bMode;

		m_Config.analogInputMaxEvents   = pCfg[eventAI].m_wLimit;

		m_Config.analogOutputEventMode  = (dnpEM) pCfg[eventAO].m_bMode;

		m_Config.analogOutputMaxEvents  = pCfg[eventAO].m_wLimit;

		m_Config.analogOutCmdEventMode  = (dnpEM) pCfg[eventAO].m_bMode;

		m_Config.analogOutCmdMaxEvents  = pCfg[eventAO].m_wLimit;

		for( UINT e = 0; e < eventTotal; e++ ) {

			m_pDatabase->SetMode(GetObject(e), pCfg[e].m_bMode);
		}

		m_Config.deleteOldestEvent      = pCfg[eventTotal].m_bMode ? TRUE : FALSE;
	}
}

BOOL CSlaveSession::IsReadOnly(BYTE bObject, BYTE e)
{
	return e == 2;
}

BOOL CSlaveSession::IsWriteOnly(BYTE bObject, BYTE e)
{
	return FALSE;
}

BOOL CSlaveSession::AddEvent(CUserData *pUser, PDWORD pData, BOOL fValue, dnpTIME * pTime, BYTE eMask)
{
	if( pUser ) {

		if( TRUE ) {

			BYTE bObject = pUser->GetObject();

			BYTE bData   = LOBYTE(LOWORD(pData[0]));

			BYTE bFlags  = BYTE(!fValue ? bData : pUser->GetEventFlags() | Ping());

			DWORD pCurrent[2];

			pUser->GetData(pCurrent);

			if( IsAnalog(bObject) ) {

				dnpANA Value;

				pUser->SetValue(&Value, fValue ? pData : pCurrent);

				BOOL fEvent = FALSE;

				switch( bObject ) {

					case 30:

						dnpANA Deadband;

						fEvent = GetDeadband(bObject, pUser->GetIndex(), pUser->GetType(), &Deadband);

						if( !fEvent || pUser->IsEvent(&Value, &Deadband, eMask & maskAiCalc) ) {

							sdnpo032_addEvent(m_pSession,
									  TMWTYPES_USHORT(pUser->GetIndex()),
									  &Value,
									  bFlags,
									  pTime);

							pUser->SetEvent(pData);
						}

						return TRUE;

					case 40:

						sdnpo042_addEvent(m_pSession,
								  TMWTYPES_USHORT(pUser->GetIndex()),
								  &Value,
								  bFlags,
								  pTime);

						if( pUser->IsDirty(dirtyScl) ) {

							if( fValue ) {

								bFlags = bFlags & 0xFE;
							}

							sdnpo043_addEvent(m_pSession,
									  TMWTYPES_USHORT(pUser->GetIndex()),
									  &Value,
									  bFlags,
									  pTime);
						}

						return TRUE;
				}

				return FALSE;
			}

			BYTE bBinary = IsBinary(bObject) ? LOBYTE(LOWORD(pCurrent[0])) : 0;

			BYTE bValue  = fValue ? bData : bBinary;

			switch( bObject ) {

				case  1:
					sdnpo002_addEvent(m_pSession,
							  TMWTYPES_USHORT(pUser->GetIndex()),
							  bValue << 7 | bFlags,
							  pTime);
					return TRUE;

				case 3:
					sdnpo004_addEvent(m_pSession,
							  TMWTYPES_USHORT(pUser->GetIndex()),
							  bValue << 6 | bFlags,
							  pTime);
					return TRUE;

				case 10:

					sdnpo011_addEvent(m_pSession,
							  TMWTYPES_USHORT(pUser->GetIndex()),
							  bValue << 7 | bFlags,
							  pTime);

					if( pUser->IsDirty(dirtyScl) ) {

						if( fValue ) {

							bFlags = bFlags & 0xFE;
						}

						sdnpo013_addEvent(m_pSession,
								  TMWTYPES_USHORT(pUser->GetIndex()),
								  bValue << 7 | bFlags,
								  pTime);
					}

					return TRUE;

				case 20:
					sdnpo022_addEvent(m_pSession,
							  TMWTYPES_USHORT(pUser->GetIndex()),
							  fValue ? pData[0] : pCurrent[0],
							  bFlags,
							  pTime);

					return TRUE;

				case 21:
					sdnpo023_addEvent(m_pSession,
							  TMWTYPES_USHORT(pUser->GetIndex()),
							  fValue ? pData[0] : pCurrent[0],
							  bFlags,
							  pTime);
					return TRUE;
			}
		}

	}

	return FALSE;
}

// Helpers

BOOL CSlaveSession::IsAnalog(BYTE bObject)
{
	switch( bObject ) {

		case 30:
		case 40:
		case 41:
		case 234:
		case 236:

			return TRUE;
	}

	return FALSE;
}

BOOL CSlaveSession::IsCounter(BYTE bObject)
{
	return ((bObject == 20) || (bObject == 21));
}

BOOL CSlaveSession::IsBinary(BYTE bObject)
{
	return ((bObject == 1) || (bObject == 3) || (bObject == 10) || (bObject == 12));
}

BOOL CSlaveSession::GetDeadband(BYTE bObject, UINT uIndex, BYTE bType, dnpANA * pDeadband)
{
	if( bObject == 30 ) {

		CUserTable * pTable = m_pDatabase->Find(34);

		if( pTable ) {

			CUserData * pUser = pTable->Find(uIndex, bType);

			if( pUser ) {

				DWORD Deadband[2];

				pUser->GetData(Deadband);

				pUser->SetValue(pDeadband, Deadband);

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Helpers

BOOL CSlaveSession::SendUnsolicited(CUserData * pUser, BYTE bMask)
{
	if( pUser ) {

		if( bMask & pUser->GetClassMask() ) {

			return TRUE;
		}
	}

	if( m_pDatabase && pUser ) {

		return (m_pDatabase->GetUnsolicitMsgMask() & pUser->GetClassMask());
	}

	return FALSE;
}

BYTE CSlaveSession::GetObject(UINT uEvent)
{
	switch( uEvent ) {

		case eventBI:	return 1;
		case eventDBI:	return 3;
		case eventBO:	return 10;
		case eventC:	return 20;
		case eventFC:	return 21;
		case eventAI:	return 30;
		case eventAO:	return 40;
	}

	return 0;
}

// End of File
