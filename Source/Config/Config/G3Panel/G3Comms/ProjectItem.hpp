
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProjectItem_HPP

#define INCLUDE_ProjectItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Project Item Base Class
//

class DLLAPI CProjectItem : public CCodedHost
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CProjectItem(void);

		// Conversion
		virtual void PostConvert(void) = 0;

		// Build
		virtual BOOL NeedBuild(void)    = 0;
		virtual BOOL PerformBuild(void) = 0;
		virtual BOOL HasControl(void)   = 0;

		// Validation
		virtual void Validate(void) = 0;
	};

// End of File

#endif
