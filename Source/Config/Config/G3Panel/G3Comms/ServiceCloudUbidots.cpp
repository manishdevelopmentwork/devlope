
#include "Intern.hpp"

#include "ServiceCloudUbidots.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CloudTagSet.hpp"
#include "CodedItem.hpp"
#include "MqttClientOptions.hpp"
#include "TagSet.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ubidots Cloud Service
//

// Dynamic Class

AfxImplementDynamicClass(CServiceCloudUbidots, CServiceCloudJson);

// Constructor

CServiceCloudUbidots::CServiceCloudUbidots(void) : CServiceCloudJson()
{
	m_bServCode = servCloudUbidots;

	m_Root      = 1;

	m_Code      = 2;
}

// UI Loading

BOOL CServiceCloudUbidots::OnLoadPages(CUIPageList *pList)
{
	BOOL fSave = (m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B);

	UINT uPage = fSave ? 1 : 2;

	pList->Append(New CUIStdPage(CString(IDS_SERVICE), AfxThisClass(), uPage));

	pList->Append(New CUIStdPage(CString(IDS_NETWORK), AfxThisClass(), 3));

	for( UINT n = 0; n < m_Sets; n++ ) {

		CPrintf Name(CString(IDS_TAG_SET_FMT), 1 + n);

		UINT        uPage = fSave ? 5 : 6;

		CUIStdPage *pPage = New CUIStdPage(Name, AfxThisClass(), uPage);

		pPage->SetSubstitute(L"set1", CPrintf("set%u", 1 + n));

		pList->Append(pPage);
	}

	return TRUE;
}

// UI Update

void CServiceCloudUbidots::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Enable" ) {

			DoEnables(pHost);
		}
	}

	CServiceCloudJson::OnUIChange(pHost, pItem, Tag);
}

// Persistance

void CServiceCloudUbidots::Init(void)
{
	CServiceCloudJson::Init();

	InitOptions();
}

// Download Support

BOOL CServiceCloudUbidots::MakeInitData(CInitData &Init)
{
	CServiceCloudJson::MakeInitData(Init);

	Init.AddText(m_Token);
	Init.AddText(m_Device);

	return TRUE;
}

// Meta Data Creation

void CServiceCloudUbidots::AddMetaData(void)
{
	CServiceCloudJson::AddMetaData();

	Meta_AddString(Token);
	Meta_AddString(Device);

	Meta_SetName(IDS("Ubidots MQTT"));
}

// Implementation

void CServiceCloudUbidots::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L"==", 1, TRUE);

	pHost->EnableUI(this, "Token", fEnable);

	pHost->EnableUI(this, "Device", fEnable);
}

void CServiceCloudUbidots::InitOptions(void)
{
	m_pOpts->m_Tls      = FALSE;

	m_pOpts->m_Check    = 3;

	m_pOpts->m_Port     = 1883;

	m_pOpts->m_Advanced = 1;

	m_pOpts->SetPeerName(L"industrial.api.ubidots.com");
}

// End of File
