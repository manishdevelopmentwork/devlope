
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3BASIS_HPP

#define	INCLUDE_C3BASIS_HPP

////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3core.hpp>

#include "ascii.h"

//////////////////////////////////////////////////////////////////////////
//
// Unaligned Types
//

#if defined(_M_ARM)

#include "align.hpp"

#else

typedef WORD   U2;

typedef PWORD  PU2;

typedef DWORD  U4;

typedef PDWORD PU4;

#endif

////////////////////////////////////////////////////////////////////////
//
// Float Passing
//

inline INT64 PassFloat(DWORD i)
{
	float  f = (float &) i;

	double d = f;

	return (INT64 &) d;
	}

////////////////////////////////////////////////////////////////////////
//
// Trace Macros
//

#define	AfxTrace0(x)		BEGIN {			\
				CTEXT s[] = x;		\
				AfxTrace(s);		\
				} END			\

#define	AfxTrace1(x,a)		BEGIN {			\
				CTEXT s[] = x;		\
				AfxTrace(s,a);		\
				} END			\

#define	AfxTrace2(x,a,b)	BEGIN {			\
				CTEXT s[] = x;		\
				AfxTrace(s,a,b);	\
				} END			\

#define	AfxTrace3(x,a,b,c)	BEGIN {			\
				CTEXT s[] = x;		\
				AfxTrace(s,a,b,c);	\
				} END			\

////////////////////////////////////////////////////////////////////////
//
// Memory Management
//

void * operator new ( size_t uSize,
		      UINT   uDummy,
		      void   *pData
		      );
	
//////////////////////////////////////////////////////////////////////////
//
// Motorola Data Packer
//

class DLLAPI CMotorDataPacker 
{
	public:
		// Constructor
		CMotorDataPacker(PDWORD pData, UINT uCount, BOOL fSign);

		// Pack
		void Pack(PBYTE  pData) const;
		void Pack(PWORD  pData) const;
		void Pack(PDWORD pData) const;

		// Unpack
		void Unpack(PBYTE  pData);
		void Unpack(PWORD  pData);
		void Unpack(PDWORD pData);

	protected:
		// Data
		PDWORD m_pData;
		UINT   m_uCount;
		BOOL   m_fSign;
	};

//////////////////////////////////////////////////////////////////////////
//
// Intel Data Packer
//

class DLLAPI CIntelDataPacker
{
	public:
		// Constructor
		CIntelDataPacker(PDWORD pData, UINT uCount, BOOL fSign);

		// Pack
		void Pack(PBYTE  pData) const;
		void Pack(PWORD  pData) const;
		void Pack(PDWORD pData) const;

		// Unpack
		void Unpack(PBYTE  pData);
		void Unpack(PWORD  pData);
		void Unpack(PDWORD pData);

	protected:
		// Data
		PDWORD m_pData;
		UINT   m_uCount;
		BOOL   m_fSign;
	};

////////////////////////////////////////////////////////////////////////
//
// Base Driver Implementation
//

class DLLAPI CDriver : public IDriver
{
	public:
		// Constructor
		CDriver(void);

		// Destructor
		virtual ~CDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetIdentifier(void);
		WORD METHOD GetCategory(void);
		WORD METHOD GetFlags(void);
		void METHOD SetHelper(ICommsHelper *pHelper);
		void METHOD SetIdentifier(WORD id);

	protected:
		// Data Members
		ULONG     m_uRefs;
		WORD      m_Ident;
		WORD      m_Flags;
		ICommsHelper * m_pHelper;

		// Data Handlers
		IDataHandler * MakeSingleDataHandler(void);
		IDataHandler * MakeDoubleDataHandler(void);

		// Operating System
		PVOID     Alloc(UINT uSize);
		void      Free(PVOID lpData);
		ISocket * CreateSocket(WORD wProt);
		CBuffer * CreateBuffer(UINT uSize, BOOL fFlag);
		UINT      CheckIP(IPREF IP, UINT uLimit);
		void      ForceSleep(UINT uTime);
		DWORD     GetTimeStamp(void);

		// Init Helpers
		BYTE   GetByte(PCBYTE &pData);
		WORD   GetWord(PCBYTE &pData);
		DWORD  GetLong(PCBYTE &pData);
		DWORD  GetAddr(PCBYTE &pData);
		PTXT   GetString(PCBYTE &pData);
		PCUTF  GetWide  (PCBYTE &pData);
		PCBYTE GetData  (PCBYTE &pData, UINT uSize);

		// Additional Helpers
		BOOL MoreHelp(WORD ID, void **pHelp);
	};
	
////////////////////////////////////////////////////////////////////////
//
// Comms Driver Implementation
//

class DLLAPI CCommsDriver : public CDriver, public ICommsDriver
{
	public:
		// Constructor
		CCommsDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICommsDriver
		void  METHOD Load(LPCBYTE pData);
		void  METHOD Load(LPCBYTE pData, UINT uSize);
		void  METHOD CheckConfig(CSerialConfig &Config);
		void  METHOD Attach(IPortObject *pPort);
		void  METHOD Detach(void);
		void  METHOD Open(void);
		void  METHOD Close(void);
		CCODE METHOD DeviceOpen(ICommsDevice *pDevice);
		CCODE METHOD DeviceClose(BOOL fPersistConnection);
		void  METHOD Service(void);
		UINT  METHOD DrvCtrl(UINT uFunc, PCTXT pValue);
		UINT  METHOD DevCtrl(void *pContext, UINT uFunc, PCTXT pValue);
		
	protected:
		// Data Members
		ICommsDevice * m_pDevice;
		IDataHandler * m_pData;
		BOOL	       m_fOpen;

		// Implementation
		BOOL Make485(CSerialConfig &Config, BOOL fMaster);
		BOOL Make422(CSerialConfig &Config, BOOL fMaster);
	};
	
////////////////////////////////////////////////////////////////////////
//
// Slave Driver Implementation
//

class DLLAPI CSlaveDriver : public CCommsDriver, public ICommsSlave
{
	public:
		// Constructor
		CSlaveDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsSlave
		void METHOD SetHelper(ISlaveHelper *pHelper);

	protected:
		// Data
		ISlaveHelper * m_pSlaveHelper;

		// IO
		CCODE Read (AREF Addr, PDWORD pData, UINT uCount);
		CCODE Read (AREF Addr, PDWORD pData, IMetaData **ppm);
		CCODE Write(AREF Addr, PDWORD pData, UINT uCount);
		
		// Meta Data
		UINT GetMetaNumber(AREF Addr, WORD ID);
		BOOL GetMetaString(AREF Addr, WORD ID, PTXT pBuff, UINT uSize);
		UINT GetMetaNumber(IMetaData *pm, WORD ID);
		BOOL GetMetaString(IMetaData *pm, WORD ID, PTXT pBuff, UINT uSize);
	};
	
////////////////////////////////////////////////////////////////////////
//
// Master Driver Implementation
//

class DLLAPI CMasterDriver : public CCommsDriver, public ICommsMaster
{
	public:
		// Constructor
		CMasterDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsMaster
		WORD  METHOD GetMasterFlags(void);
		CCODE METHOD Ping(void);
		CCODE METHOD Read(AREF Addr, PDWORD pData, UINT  uCount);
		CCODE METHOD Write(AREF Addr, PDWORD pData, UINT  uCount);
		CCODE METHOD Atomic(AREF Addr, DWORD  Data,  DWORD dwMask);
	};

////////////////////////////////////////////////////////////////////////
//
// Producer Consumer Driver Implementation
//

class DLLAPI CProConDriver : public CMasterDriver, public ICommsSlave
{
	public:
		// Constructor
		CProConDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsMaster
		CCODE METHOD Write(AREF Addr, PDWORD pData, UINT uCount);

		// ICommsSlave
		void METHOD SetHelper(ISlaveHelper *pHelper);

	protected:
		// Data Members
		ISlaveHelper * m_pSlaveHelper;
		
		// IO
		CCODE ReadSlave (AREF Addr, PDWORD pData, UINT uCount);
		CCODE WriteSlave(AREF Addr, PDWORD pData, UINT uCount);
	};

////////////////////////////////////////////////////////////////////////
//
// Keyboard Driver Implementation
//

class DLLAPI CKeyboardDriver : public CCommsDriver 
{
	public:
		// Constructor
		CKeyboardDriver(void);

		// IDriver
		WORD METHOD GetCategory(void);

	protected:
		// Data Members
		IKeyboardHelper * m_pKeyboardHelper;
		
		// Keyboard Helper
		void PostKey(WORD wCode);
	};

////////////////////////////////////////////////////////////////////////
//
// Printer Driver Implementation
//

class DLLAPI CPrintDriver : public CCommsDriver, public ICommsPrinter
{
	public:
		// Constructor
		CPrintDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsPrinter
		void METHOD OutChar(char cData);
		void METHOD OutString(PCTXT pText);
		void METHOD OutNewLine(void);
		void METHOD OutFormFeed(void);
		
	protected:
		// Data Members
		WORD m_wState;
	};

////////////////////////////////////////////////////////////////////////
//
// Display Driver Implementation
//

class DLLAPI CDisplayDriver : public CCommsDriver, public ICommsDisplay
{
	public:
		// Constructor
		CDisplayDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsDisplay
		UINT METHOD GetSize(int cx, int cy);
		void METHOD Capture(PBYTE pDest, PCBYTE pSrc, int px, int py, int cx, int cy, UINT uSize, BOOL fPortrait);
		void METHOD Service(UINT uDrop, PBYTE pPrev, PBYTE pData, UINT uSize);
	};

////////////////////////////////////////////////////////////////////////
//
// Raw Port Driver Implementation
//

class DLLAPI CRawPortDriver : public CCommsDriver, public ICommsRawPort
{
	public:
		// Constructor
		CRawPortDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsRawPort
		void METHOD Connect(void);
		void METHOD Disconnect(void);
		UINT METHOD Read (UINT uTime);
		UINT METHOD Write(BYTE bData, UINT uTime);
		UINT METHOD Write(PCBYTE pData, UINT uCount, UINT uTime);
		void METHOD SetRTS(BOOL fState);
		BOOL METHOD GetCTS(void);

	protected:
		// Implementation
		BOOL SkipUpdate(LPCBYTE &pData);
	};

////////////////////////////////////////////////////////////////////////
//
// Camera Driver Implementation
//

class DLLAPI CCameraDriver : public CCommsDriver, public ICommsCamera
{
	public:
		// Constructor
		CCameraDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsCamera
		CCODE  METHOD Ping(void);
		CCODE  METHOD ReadImage(PBYTE &pData);
		void   METHOD SwapImage(PBYTE pData);
		void   METHOD KillImage(void);
		PCBYTE METHOD GetData(UINT uDevice);
		UINT   METHOD GetInfo(UINT uDevice, UINT uParam);
		BOOL   METHOD SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		BOOL   METHOD LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile);
		BOOL   METHOD UseSetup(PVOID pContext, UINT uIndex);
	};

////////////////////////////////////////////////////////////////////////
//
// Streamer Driver Implementation
//

class DLLAPI CStreamerDriver : public CRawPortDriver, public ICommsStreamer
{
	public:
		// Constructor
		CStreamerDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsStreamer
		DWORD  METHOD GetNextTime(DWORD dwTime);
		PCTXT  METHOD GetFilename(DWORD dwTime);
		UINT   METHOD GetDataCount(void);
		PCBYTE METHOD GetDataPointer(void);
		DWORD  METHOD GetValue(UINT uIndex);
		void   METHOD FreeData(void);
	};

// End of File

#endif
