
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Runtime
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DEBUGCMDS_HPP

#define INCLUDE_DEBUGCMDS_HPP

////////////////////////////////////////////////////////////////////////
//
// Debug Commands Object
//

class CDebugCmds : public IDiagProvider
{
	public:
		// Constructor
		CDebugCmds(void);

		// Destructor
		~CDebugCmds(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		ULONG	       m_uRefs;
		DWORD	       m_dwStart;
		IDiagManager * m_pDiag;
		UINT           m_uProv;

		// Registration
		void AddDiagCmds(void);

		// Command Codes
		enum
		{
			cmdNone,
			cmdAuto,
			cmdCFTest,	
			cmdChangeDir,	
			cmdChkDsk,	
			cmdClearDbase,	
			cmdClearGMC,	
			cmdCrash,	
			cmdCrashDump,	
			cmdCycle,	
			cmdDelete,	
			cmdDir,		
			cmdFakeTouch,	
			cmdFormat,	
			cmdFRAMDump,	
			cmdHello,
			cmdHexDump,	
			cmdMakeDir,
			cmdMemCheck,	
			cmdMemStat,	
			cmdMemTest,	
			cmdMount,
			cmdPing,	
			cmdPwd,
			cmdRemoveDir,
			cmdReset,
			cmdShowARP,	
			cmdSNTP,	
			cmdStart,	
			cmdStop,	
			cmdTime,	
			cmdTraps,	
			cmdType,	
			cmdUnmount,	
			cmdUpTime,	
			cmdWho
			};

		// Command Handlers
		UINT CmdAuto(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdCFTest(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdChangeDir(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdChkDsk(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdClearDbase(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdClearGMC(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdCrash(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdCrashDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdCycle(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdDelete(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdDir(IDiagOutput *pOut, IDiagCommand *pCmd);	
		UINT CmdDisk(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdFakeTouch(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdFormat(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdFRAMDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdHello(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdHexDump(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMakeDir(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMemCheck(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMemStat(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMemTest(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdMount(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdPing(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdPwd(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdRemoveDir(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdReset(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdSNTP(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdStart(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdStop(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdTime(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdTraps(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdType(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdUnmount(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdUpTime(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT CmdWho(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Implementation
		BOOL ParseRange(IDiagCommand *pCmd, UINT &uInit, UINT &uSize, UINT uBase, UINT uDefault);
		UINT ParseNumber(CString const &Text);
		void ShowTime(IDiagOutput *pOut);
	};

// End of File

#endif
