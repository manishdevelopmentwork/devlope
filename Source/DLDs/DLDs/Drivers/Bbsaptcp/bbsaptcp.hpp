//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Space Definitions
//
enum {
	SPBIT	=  1,
	SPBYTE	=  2,
	SPWORD	=  3,
	SPLONG	=  4,
	SPREAL	=  5,
	SPSTR   =  6,
	};

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCRDBACC = 0xA0, // remote database access
	};

enum { // Operational function codes
	OFCRDBYA  =  0x0, // Read Signal by Address
	OFCRDBYN  =  0x4, // Read By Name
	OFCRDBYL  = 0x0C, // First call to read Signal(s) by list
	OFCRDBYLC = 0x0D, // Continue to read Signal(s) by list
	OFCWRBYA  = 0x80, // Write Signal by Address
	OFCWRBYN  = 0x84, // Write By Name
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Field Select 2 codes
	FS2PROTECT = 0x80,
	FS2ALMPRI  = 0x40,
	FS2BNAME   = 0x20,
	FS2BEXT    = 0x10,
	FS2BATT    = 0x08,
	FS2ALMDBLO = 0x04,
	FS2ALMDBHI = 0x02,
	FS2MSDVER  = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	POLLMSG  = 0x85, // Poll message
	POLLDOWN = 0x86, // Down Transmit ACK, Slave ACKing message
	POLLNONE = 0x87, // ACK No Data, in response to a Poll message
	NRTSEND  = 0x88, // Send Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	POLLUPMS = 0x8B, // UP-ACK, Master ACKing message
	POLLNAK  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Transaction States

	STPOLL    = 1,
	STPWTDATA = 2,
	};

enum { // response return values

	PRNORSP		= 0,
	PRWRONG		= 1,
	PRACK		= 2,
	PRNAK		= 3,
	PRDATA		= 4,
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

// UDP Receive positions SH=Standard Header, EH = Extended Header
enum {  // Common Header
	UDPHSZ	= 0,	// Message Header Size
	UDPPCT	= 2,	// Number of Packets
	UDPFLG	= 4,	// Flags
	UDPSEQ	= 6,	// Send Sequence Number
	UDPLAK	= 10,	// Last Ack'ed Seq Number
	};

enum {  // Extra for extended header
	UDPRIP	= 14,	// Sender IP
	UDP000	= 18,	// Filler
	};

enum {  // Standard Header Subpackets
	UDPSSZ	= 14,	// Subpacket size
	UDPSTY	= 16,	// Type (BSAP = 0)
	UDPSMX	= 18,	// Exchange Code
	UDPSMS	= 19,	// Message Seq #
	UDPSGA	= 21,	// Global Address of Sender
	UDPSER	= 23,	// Error if not 0
	UDPSCT	= 24,	// Number of Items (= 1 for good reply)
	UDPSDT	= 25,	// Data start
	};

enum {  // Extended Header Subpackets
	UDPESZ	= 22,	// Subpacket size
	UDPETY	= 24,	// Type (BSAP = 0)
	UDPEMX	= 26,	// Exchange Code
	UDPEMS	= 27,	// Message Seq #
	UDPEGA	= 29,	// Global Address of Sender
	UDPEER	= 31,	// Error if not 0
	UDPECT	= 32,	// Number of Items (= 1 for good reply)
	UDPEDT	= 33,	// Data start
	};

// Receive Frame Check codes
enum {
	FRCONT	= 0,
	FRDONE	= 1,
	FRERR	= 2
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	SZHDREXT	((WORD)22)
#define	SZHDRSTD	((WORD)14)
#define SZDATA		((WORD)247)
#define SZRECV		((WORD)4096)
#define	POLLPOSN	23
#define MAX_CHARS	80
#define MAX_LTEXT	(MAX_CHARS / sizeof(DWORD))

#define	FLAGNEW		6
#define	FLAGOLD		4

// Add Var Name Responses
#define	VTXFULL	0
#define	VADDOK	1
#define	VNONAME	2

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock UDP Master Driver
//

class CBBBSAPTCPDriver : public  CMasterDriver
{
	public:
		// Constructor
		CBBBSAPTCPDriver(void);

		// Destructor
		~CBBBSAPTCPDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Detach(void);
		DEFMETH(void)	Open(void);
		
		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE)	Ping (void);

		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// String support
		struct CStringTag
		{
			WORD  m_Index;
			char  m_Text[MAX_CHARS];
			};

		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			DWORD	 m_dTrans;
			UINT	 m_uLast;
			BOOL	 m_fHasTags;
			DWORD	 m_ThisIP;
			BOOL     m_fDirty;
			// Protocol information
			UINT	 m_uAppSeq;
			BOOL	 m_fOpen;
			DWORD	 m_dLastAck;
			WORD	 m_wMsgSeq;
			UINT	 m_uMSDVers;
			BOOL     m_fMSD;
			BOOL     m_fInit;
			// Tag information
			WORD	 m_wTagCount;
			PBYTE	 m_pbDevName;
			PWORD	 m_pwMSDList;
			PBOOL    m_pfMSDList;
			PDWORD	 m_pdAddrList;
			PBYTE	 m_pbNameList;
			PWORD	 m_pwNameIndex;
			// String Tags
			UINT	     m_uStrings;
			CStringTag * m_pStrings;

			BYTE	 m_bIsC3;
			};

		struct READFRAME
		{
			UINT	uAddr;
			PDWORD	pData;
			UINT	uCount;
			UINT	uGood;
			BOOL	fDoMSD;
			BOOL	fReqMSD;
			};

		struct CMacAddr
		{
			DWORD  m_IP;
			WORD   m_Port;
			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bRx[SZRECV];
		BYTE       m_bTx[SZDATA];
		PBYTE	   m_pRx;
		UINT	   m_uPtr;
		UINT	   m_uWriteError;
		UINT	   m_uState;
		WORD	   m_wAddress;
		BOOL	   m_fExtended;
		BOOL	   m_fReqNotDone;
		READFRAME  m_RFrame;
		READFRAME* m_pRFrame;
		CBuffer  * m_pTxBuff;
		CBuffer  * m_pRxBuff;
		PBYTE      m_pTxData;
		CMacAddr   m_RxMac;
		ISocket  * m_pSock;
		BOOL	   m_fDebug;
		UINT	   m_uIndex;

		// Frame Building
		BOOL	StartFrame(void);
		void	EndFrame(void);
		void	AddUDPHeader(UINT uPktCt, DWORD dSeq, DWORD dAck);
		void	AddUDPPacketHeader(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
		void	AddText(PCTXT pText);
		UINT	AddVarName(AREF Addr, UINT uIndex, UINT uBytes = 0);
		UINT    AddVarAddr(UINT uOffset, UINT uBytes = 0);
		void    AddSize(void);
		UINT	GetNamePos(AREF Addr, UINT uIndex);
		BOOL	DoCommandMsg(BYTE bCommand, BOOL fIsPoll);
		BOOL	SendPoll(void);

		// Read Handlers
		CCODE	DoDataRead(AREF Addr);
		DWORD	GetReadData(AREF Addr, UINT *pOffset, UINT uIndex);
		DWORD	GetStringData(AREF Addr, UINT &uPos, UINT uDPos, UINT uIndex);
		CCODE   DoReadByName(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	AddReadByName(AREF Addr);
		void	AddReadByNameHeader(void);
		void    AddReadByNameHeaderMSD(void);
		CCODE   GetRead(UINT uPos, AREF Addr);
		BOOL	DoDataAddrRead(UINT uDataType);
		CCODE   DoReadByAddr(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   AddReadByAddr(AREF Addr);
		void    AddReadByAddrHeader(void);
								
		// Write Handlers
		CCODE   DoWriteByName(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	AddWriteByName(AREF Addr, PDWORD pData, UINT uCount);
		BYTE	AddDescriptor(UINT uTable, UINT uOffset, BOOL fBitData);
		void	AddWriteData(AREF Addr, DWORD dData);
		void    AddStringData(AREF Addr, PDWORD pData, UINT &uIndex, UINT uCount);
		void    AddWriteElementCount(BYTE bCount, BYTE bPos = 0);
		CCODE   DoWriteByAddr(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   AddWriteByAddr(AREF Addr, PDWORD pData, UINT uCount);

		// String Support
		UINT GetStringContextIndex(AREF Addr);
		void SetStringContext(AREF Addr, PCTXT pText, UINT uCount);
		UINT GetStringCount(AREF Addr, UINT uPos, UINT uCount);
		UINT GetCharCount(PCTXT pText, UINT uOffset);
		void RemoveTrailing(PBYTE &pByte, BYTE bByte, UINT uBytes);
				
		// Ack
		void	SendAck(void);
		void	SendAlarmAck(void);

		// Transport Layer
		BOOL	Transact (void);
		UINT	Send(void);
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);
		UINT	RcvDone(UINT uSize);
		BOOL	FrameOK(void);		
		void	DiscardRetry(void);
		WORD	GetTxWord( UINT uPos);
		DWORD	GetTxDWord(UINT uPos);

		// Tags
		BOOL	LoadTags(LPCBYTE &pData);
		void    LoadStringSupport(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	StoreName(LPCBYTE &pData, WORD wItem, PWORD pPosition);

		// Helpers
		BOOL	HasTags(PDWORD pData, UINT uCount);
		BOOL    HasMSD(AREF Addr, UINT uCount);
		void	UpdateMsgSeq(void);
		void	UpdateAppSeq(void);
		WORD	GetRcvWord (UINT uPos);
		DWORD	GetRcvDWord(UINT uPos);
		BOOL	IsOctetEnd(char c);
		BOOL	IsDigit(char c);
		BOOL	IsByte(UINT uNum);
		BOOL    IsWord(UINT uType);
		BOOL    IsLong(UINT uType);
		BOOL	IsString(UINT uTable);
		BOOL	IsStringText(UINT uChar);
		BOOL	CanAddToRequest(PBYTE pName, UINT uBytes = 0);
		UINT    GetDataByteCount(AREF Addr, UINT uCount);
		UINT    GetItemIndex(AREF Addr);
		UINT    FindOffset(AREF Addr, UINT uIndex);
		UINT    GetCount(CAddress Addr, UINT uCount);
		UINT    ExpandCount(CAddress Addr, UINT uCount, UINT uMax = 0);
		void    ClearTxBuffer(void);
		void    ClearRxBuffer(void);

		// Transport Header
		void AddTransportHeader(void);
		BOOL StripTransportHeader(void);

		// delete created buffers
		void	CloseDown(void);

		// List Testing
		BOOL    m_fListDebug;
		UINT	m_uListNumber;
		UINT	m_uListEntry;
		DWORD	m_dListResp;
		
		// List Testing Handlers
		CCODE   DoListRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE   DoListWrite(AREF Addr, PDWORD pData, UINT uCount);

		// List Testing Implementation
		void	MakeListRequest(AREF Addr, PDWORD pData, UINT uCount);
		void	AddReadByListHeader(void);
		BOOL	AddReadByList(void);
		BOOL	DoListRead(void);
		void    ListRecv(void);
		void    ListRecvDump(UINT uPtr);
	};
/*
FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File
