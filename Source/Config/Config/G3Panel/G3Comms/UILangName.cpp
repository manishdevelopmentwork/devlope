
#include "Intern.hpp"

#include "UILangName.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "LangManager.hpp"
#include "LangNameComboBox.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Language Name
//

// Dynamic Class

AfxImplementDynamicClass(CUILangName, CUIControl);

// Constructor

CUILangName::CUILangName(void)
{
	m_pTextLayout = NULL;

	m_pDataLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pDataCtrl   = New CLangNameComboBox(this);
	}

// Core Overridables

void CUILangName::OnBind(void)
{
	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}

	CUIElement::OnBind();
	}

void CUILangName::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pDataLayout = New CLayDropDown(m_pText, TRUE, 20);

	m_pMainLayout = New CLayFormPad (m_pDataLayout, horzLeft | vertCenter);

	pForm->AddItem( New CLayFormPad (m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUILangName::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create( m_Label,
			     0,
			     m_pTextLayout->GetRect(),
			     Wnd,
			     0
			     );

	m_pDataCtrl->Create( WS_TABSTOP         |
			     WS_VSCROLL         |
			     CBS_DROPDOWNLIST   |
			     CBS_OWNERDRAWFIXED |
			     CBS_HASSTRINGS,
			     FindComboRect(),
			     Wnd,
			     uID++
			     );

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pDataCtrl->SetFont(afxFont(Dialog));

	AddControl(m_pTextCtrl);

	AddControl(m_pDataCtrl, CBN_SELCHANGE);

	LockList();

	LoadList();

	UnlockList();
	}

void CUILangName::OnPosition(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pDataCtrl->MoveWindow(FindComboRect(), TRUE);
	}

// Data Overridables

void CUILangName::OnLoad(void)
{
	m_pDataCtrl->SelectStringExact(m_pText->GetAsText());
	}

UINT CUILangName::OnSave(BOOL fUI)
{
	CString Text = m_pDataCtrl->GetWindowText();

	Text.TrimLeft();

	Text.FoldUnicode(MAP_FOLDDIGITS | MAP_PRECOMPOSED);

	return StdSave(fUI, Text);
	}

// Implementation

CRect CUILangName::FindComboRect(void)
{
	CRect Rect  = m_pDataLayout->GetRect();

	Rect.right  = Rect.right - 16;

	Rect.top    = Rect.top   - 1;

	Rect.bottom = Rect.top   + 8 * Rect.cy();

	return Rect;
	}

BOOL CUILangName::LoadList(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CLangManager *pLang   = pSystem->m_pLang;

	CArray <UINT> List;

	UINT c = pLang->EnumLanguages(List);

	for( UINT n = 0; n < c; n++ ) {

		UINT    uCode = List[n];

		CString Name  = pLang->GetNameFromCode(uCode);

		m_pDataCtrl->AddString(Name, uCode);
		}

	return TRUE;
	}

void CUILangName::LockList(void)
{
	m_pDataCtrl->SetRedraw(FALSE);

	m_pDataCtrl->SendMessage(CB_SETMINVISIBLE, 1);
	}

void CUILangName::UnlockList(void)
{
	m_pDataCtrl->SendMessage(CB_SETMINVISIBLE, 8);

	m_pDataCtrl->SetRedraw(TRUE);

	m_pDataCtrl->Invalidate(TRUE);
	}

// End of File
