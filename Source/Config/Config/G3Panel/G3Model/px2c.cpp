#include "intern.hpp"

#include "Px2c.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 G3 Model Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PX2C System Item
//

class CPX2CItem : public CPx2CSystemItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPX2CItem(void);

		// Model Mapping
		CString GetModelList(void) const;
		CString GetModelInfo(CString Model) const;

	protected:

	};


//////////////////////////////////////////////////////////////////////////
//
// PX2C System Item
//

// Dynamic Class

AfxImplementDynamicClass(CPX2CItem, CPx2CSystemItem);

// Constructor

CPX2CItem::CPX2CItem(void)
{	
	}

// Model Mapping

CString CPX2CItem::GetModelList(void) const
{
	return L"px2c";
	}

CString CPX2CItem::GetModelInfo(CString Model) const
{
	if( Model == L"px2c" ) {

		return L"px2c"; //TODO ,promvs,loadvs";
		}

	return L"";
	}


// End of File