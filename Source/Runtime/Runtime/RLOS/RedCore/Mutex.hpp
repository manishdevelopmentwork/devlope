
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Executive Implementation
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Mutex_HPP
	
#define	INCLUDE_Mutex_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Waitable.hpp"

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mutex Implementation
//

class CMutex : public CWaitable, public IMutex
{
	public:
		// Constructor
		CMutex(CExecutive *pExec);

		// Destructor
		~CMutex(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// CWaitable
		bool WaitInit(CExecThread *pThread, UINT uSlot, bool fWait);
		void WaitDone(CExecThread *pThread, UINT uSlot);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IMutex
		void METHOD Free(void);

		// Task Teardown
		static void FreeMutexList(CExecThread *pThread);

	protected:
		// Data Members
		ULONG		       m_uRefs;
		UINT	      /*volatile*/ m_uCount;
		CExecThread * /*volatile*/ m_pOwner;
		CMutex      *	       m_pNext;
		CMutex      *	       m_pPrev;

		// Boost Support
		void FindBoost(void);
		void ReleaseBoost(void);

		// Implementation
		bool Claim(CExecThread *pThread);
		void ListAppend(void);
		void ListRemove(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE bool CMutex::Claim(CExecThread *pThread)
{
	if( unlikely(m_pOwner) ) {
		
		if( m_pOwner == pThread ) {

			m_uCount++;

			return true;
			}

		return false;
		}
	else {
		m_pOwner = pThread;

		m_uCount = 1;

		ListAppend();

		#if USE_BOOST

		FindBoost();

		#endif

		return true;
		}
	}

STRONG_INLINE void CMutex::ListAppend(void)
{
	AfxListAppend( m_pOwner->m_pOwnedHead,
		       m_pOwner->m_pOwnedTail,
		       this,
		       m_pNext,
		       m_pPrev
		       );
	}

STRONG_INLINE void CMutex::ListRemove(void)
{
	AfxListRemove( m_pOwner->m_pOwnedHead,
		       m_pOwner->m_pOwnedTail,
		       this,
		       m_pNext,
		       m_pPrev
		       );
	}

// End of File

#endif
