
//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Data Spaces
//

#define	PARAM_BOOL	3
#define	PARAM_WORD	2
#define	PARAM_REAL	1

#define	ISHEX		TRUE
#define	NOTHEX		FALSE
#define	ISWRITE		TRUE
#define	NOTWRITE	FALSE

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Old 590 Driver
//

class CEurothermBiSynchDriver : public CMasterDriver
{
	public:
		// Constructor
		CEurothermBiSynchDriver(void);

		// Destructor
		~CEurothermBiSynchDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_GID;
			BYTE m_UID;
			BOOL m_fChanSel;
			BYTE m_Channel;
			};

		CContext * m_pCtx;

		LPCTXT	m_pHex;
		UINT	m_uPtr;
		BYTE	m_bCheck;
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];
		
		// Implementation

		// Port Access
		UINT	RxByte(UINT uTime);

		// Frame Building
		void	StartFrame(void);
		void	EndFrame(BOOL fIsWrite);
		void	AddByte(BYTE bData);
		void	AddValue( char * pV );
		void	PutInteger(DWORD dData, BOOL fNotHex);
		
		// Transport Layer
		BOOL	Send(void);
		UINT	GetReply(BOOL fIsWrite);
		BOOL	Transact(BOOL fIsWrite);

		// Frame Formatting
		void	PutIdentifier(UINT uOffset, BOOL fIsWrite);

		// Read Handlers
		CCODE	DoRealRead(UINT Addr, PDWORD pData);
		CCODE	DoWordRead(UINT Addr, UINT uTable, PDWORD pData);
		CCODE	DoBitRead(UINT Addr, PDWORD pData);

		// Write Handlers
		CCODE	DoBitWrite(UINT Addr, UINT uTable, DWORD dData);
		CCODE	DoWordWrite(UINT Addr, UINT uTable, DWORD dData);
		CCODE	DoRealWrite(UINT Addr, DWORD dData);

		// Helpers
		BOOL	IsADigit( BYTE b, BOOL fIsHex );
		CCODE	SetCCError(BOOL fIsWrite);
	};

// End of File
