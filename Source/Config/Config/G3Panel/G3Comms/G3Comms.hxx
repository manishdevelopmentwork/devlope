
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3Comms_HXX
	
#define	INCLUDE_G3Comms_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdesk.hxx>

#include <pcwin.hxx>

// End of File

#endif
