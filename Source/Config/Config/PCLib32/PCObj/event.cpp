
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Event Object Class
//

// Runtime Class

AfxImplementRuntimeClass(CEvent, CWaitable);

// Constructor

CEvent::CEvent(BOOL fManual)
{
	m_hObject = CreateEvent(NULL, fManual, FALSE, NULL);
	}

// Operations

void CEvent::Set(void)
{
	SetEvent(m_hObject);
	}

void CEvent::Reset(void)
{
	ResetEvent(m_hObject);
	}

void CEvent::Pulse(void)
{
	PulseEvent(m_hObject);
	}

// End of File
