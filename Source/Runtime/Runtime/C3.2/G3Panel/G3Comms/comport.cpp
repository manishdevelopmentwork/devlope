
#include "intern.hpp"

#include "proxy.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Port List
//

// Constructor

CCommsPortList::CCommsPortList(void)
{
	m_uCount = 0;

	m_ppPort = NULL;
}

// Destructor

CCommsPortList::~CCommsPortList(void)
{
	while( m_uCount-- ) {

		delete m_ppPort[m_uCount];
	}

	delete[] m_ppPort;
}

// Initialization

void CCommsPortList::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortList", pData);

	if( (m_uCount = GetWord(pData)) ) {

		CCommsPort *pLast = NULL;

		m_ppPort          = New CCommsPort *[m_uCount];

		for( UINT n = 0; n < m_uCount; n++ ) {

			CCommsPort *pPort = NULL;

			if( GetByte(pData) ) {

				pPort = CCommsPort::MakeObject(GetByte(pData));
			}

			if( (m_ppPort[n] = pPort) ) {

				pPort->Load(pData);

				if( pPort->m_PortPhys < 100 ) {

					if( pLast ) {

						// NOTE -- This logic assumes the logical ports that
						// belong to the same physical port are sequential,
						// which is correct given how the config works. It
						// also assumes that one port object will take the
						// responsibility of freeing the mutex.

						if( pPort->m_PortPhys == pLast->m_PortPhys ) {

							if( !pLast->m_pMutex ) {

								pLast->m_pMutex = Create_Mutex();
							}

							pPort->m_pMutex = pLast->m_pMutex;
						}
					}
				}

				pLast = pPort;
			}
		}
	}
}

// Attributes

BOOL CCommsPortList::HasModem(void) const
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CCommsPort *pPort = m_ppPort[n];

		if( pPort ) {

			if( pPort->IsModem() ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

// Task List

void CCommsPortList::GetTaskList(CTaskList &List, UINT &uLevel)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		CCommsPort *pPort = m_ppPort[n];

		if( pPort ) {

			pPort->GetTaskList(List, uLevel);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Communications Port
//

// Class Creation 

CCommsPort * CCommsPort::MakeObject(UINT uType)
{
	switch( uType ) {

		case  1: return New CCommsPortSerial;
		case  2: return New CCommsPortNetwork;
		case  3: return New CCommsPortVirtual;
		case  4: return New CCommsPortCAN;
		case  5: return New CCommsPortProfibus;
		case  7: return New CCommsPortDevNet;
		case  8: return New CCommsPortCatLink;
		case  9: return New CCommsPortMPI;
		case 10: return New CCommsPortRack;
		case 11: return New CCommsPortJ1939;
	/*	case 12: return New CCommsPortDnp3;	*/
		case 13: return New CCommsPortTest;
	}

	return NULL;
}

// Constructor

CCommsPort::CCommsPort(void)
{
	m_Number    = 0;
	m_PortPhys  = 0;
	m_PortLog   = 0;
	m_Binding   = 0;
	m_DriverID  = 0;
	m_DriverRpc = 0;
	m_pImage    = NULL;
	m_uImage    = 0;
	m_dwContext = 0;
	m_pConfig   = NULL;
	m_uConfig   = 0;
	m_pDevices  = New CCommsDeviceList;
	m_pMutex    = NULL;
	m_pProxy    = NULL;
	m_pDriver   = NULL;
	m_pComms    = NULL;
	m_hModule   = NULL;

	memset(m_uCount, 0, sizeof(m_uCount));
}

// Destructor

CCommsPort::~CCommsPort(void)
{
	if( m_pMutex && !m_PortLog ) {

		// NOTE -- Only free if we are logical port 0 as
		// the object is shared between all the ports that
		// use the same physical port on a given host.

		m_pMutex->Release();
	}

	delete m_pProxy;
	delete m_pImage;
	delete m_pConfig;
	delete m_pDevices;

	FreeDLD();
}

// Initialization

void CCommsPort::Load(PCBYTE &pData)
{
	m_PortPhys = GetByte(pData);

	m_PortLog  = GetByte(pData);

	m_Number   = GetWord(pData);

	if( (m_DriverID = GetWord(pData)) ) {

		if( IsModem() ) {

			m_pConfig = GetCopy(pData);
		}
		else {
			m_DriverRpc = GetWord(pData);

			m_pImage    = GetCopy(pData, m_uImage);

			m_pConfig   = GetCopy(pData, m_uConfig);

			if( !m_DriverRpc && m_pImage ) {

				if( !LoadDLD() ) {

					AfxTrace("*** FIXUP ERROR\n");

					delete m_pImage;

					m_pImage = NULL;
				}
			}

			m_pDevices->Load(pData, this);

			m_pProxy = CProxy::Create(this);

			if( !m_pProxy ) {

				m_pDevices->SetError(errorHard);
			}
		}
	}

	CCommsSystem::m_pThis->m_pComms->RegPort(m_Number, this);
}

// Attributes

BOOL CCommsPort::IsModem(void) const
{
	if( m_DriverID >= 0xFF00 && m_DriverID <= 0xFF0F ) {

		return TRUE;
	}

	return FALSE;
}

// Operations

void CCommsPort::AddCount(UINT uType, UINT uCode)
{
	m_uCount[uType][uCode]++;
}

// Task List

void CCommsPort::GetTaskList(CTaskList &List, UINT &uLevel)
{
	if( m_pProxy ) {

		UINT uCount = m_pProxy->GetTaskCount();

		if( uCount ) {

			CTaskDef Task;

			Task.m_pEntry = this;
			Task.m_Name   = "COMMS";
			Task.m_uID    = 0;
			Task.m_uCount = uCount;
			Task.m_uLevel = uLevel;
			Task.m_uStack = 4096;

			List.Append(Task);

			uLevel += uCount;
		}
	}
}

// Task Entries

void CCommsPort::TaskInit(UINT uID)
{
	m_pProxy->Init(uID);
}

void CCommsPort::TaskExec(UINT uID)
{
	m_pProxy->Task(uID);
}

void CCommsPort::TaskStop(UINT uID)
{
	m_pProxy->Stop(uID);
}

void CCommsPort::TaskTerm(UINT uID)
{
	m_pProxy->Term(uID);
}

// Proxy Calls

BOOL CCommsPort::Open(IPortHandler *pData, UINT uFlags)
{
	return FALSE;
}

BOOL CCommsPort::Open(ICommsDriver *pDriver)
{
	return FALSE;
}

void CCommsPort::Poll(void)
{
}

void CCommsPort::Term(void)
{
}

// Management

BOOL CCommsPort::IsRemote(void)
{
	return FALSE;
}

UINT CCommsPort::GetConfig(UINT uParam)
{
	return NOTHING;
}

void CCommsPort::CommsDebug(IDiagOutput *pOut, BOOL &fHead)
{
	if( m_DriverID ) {

		if( fHead ) {

			ShowHeader(pOut);

			fHead = FALSE;
		}

		ShowStatus(pOut);
	}
}

UINT CCommsPort::Control(PVOID pCtx, UINT uCode, PCTXT pValue)
{
	return 0;
}

// Mutex Management

BOOL CCommsPort::MutexClaim(void)
{
	if( m_pMutex ) {

		m_pMutex->Wait(FOREVER);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPort::MutexFree(void)
{
	if( m_pMutex ) {

		m_pMutex->Free();

		return TRUE;
	}

	return FALSE;
}

// Debug Helpers

BOOL CCommsPort::ShowHeader(IDiagOutput *pOut)
{
	pOut->AddTable(11);

	pOut->SetColumn(0, "Num", "%u");
	pOut->SetColumn(1, "Path", "%s");
	pOut->SetColumn(2, "ID", "%4.4X");
	pOut->SetColumn(3, "Dir", "%s");
	pOut->SetColumn(4, "Send", "%u");
	pOut->SetColumn(5, "Okay", "%u");
	pOut->SetColumn(6, "NoData", "%u");
	pOut->SetColumn(7, "Retry", "%u");
	pOut->SetColumn(8, "Busy", "%u");
	pOut->SetColumn(9, "Silent", "%u");
	pOut->SetColumn(10, "Error", "%u");

	pOut->AddHead();

	pOut->AddRule('-');

	return TRUE;
}

BOOL CCommsPort::ShowStatus(IDiagOutput *pOut)
{
	char sName[16];

	switch( m_PortPhys ) {

		case 100:
			strcpy(sName, "Rack");
			break;

		case 255:
			strcpy(sName, "Net");
			break;

		default:
			SPrintf(sName, "%u.%u", m_PortPhys, m_PortLog);
			break;
	}

	for( UINT t = 0; t < 2; t++ ) {

		pOut->AddRow();

		pOut->SetData(0, m_Number);

		pOut->SetData(1, sName);

		pOut->SetData(2, m_DriverID);

		pOut->SetData(3, t ? "Put" : "Get");

		for( UINT c = 0; c < 7; c++ ) {

			pOut->SetData(4 + c, m_uCount[t][c]);
		}

		pOut->EndRow();
	}

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// Comms Port with Port Object
//

// Constructor

CCommsPortObject::CCommsPortObject(void)
{
	m_pPort = NULL;

	memset(&m_Config, 0, sizeof(m_Config));
}

// Destructor

CCommsPortObject::~CCommsPortObject(void)
{
	FreeObject();
}

// Proxy Calls

BOOL CCommsPortObject::Open(IPortHandler *pData, UINT uFlags)
{
	if( FindObject() ) {

		if( FindConfig() ) {

			m_pPort->Bind(pData);

			m_Config.m_uFlags = uFlags;

			if( m_pPort->Open(m_Config) ) {

				return TRUE;
			}
		}
	}

	return FALSE;
}

BOOL CCommsPortObject::Open(ICommsDriver *pComms)
{
	if( FindObject() ) {

		if( FindConfig() ) {

			MutexClaim();

			pComms->CheckConfig(m_Config);

			pComms->Attach(m_pPort);

			if( m_pPort->Open(m_Config) ) {

				pComms->Open();

				m_pComms = pComms;

				return TRUE;
			}

			pComms->Detach();

			MutexFree();
		}
	}

	return FALSE;
}

void CCommsPortObject::Term(void)
{
	if( m_pPort ) {

		if( m_pComms ) {

			m_pComms->Close();

			m_pPort->Close();

			m_pComms->Detach();

			m_pComms = NULL;
		}
		else
			m_pPort->Close();

		MutexFree();
	}
}

// Management

UINT CCommsPortObject::GetConfig(UINT uParam)
{
	switch( uParam ) {

		case 1: return m_Config.m_uBaudRate;
		case 2: return m_Config.m_uDataBits;
		case 3: return m_Config.m_uStopBits;
		case 4: return m_Config.m_uParity;
		case 5: return m_Config.m_uPhysical - 1;
	}

	return NOTHING;
}

UINT CCommsPortObject::Control(PVOID pCtx, UINT uCode, PCTXT pValue)
{
	if( m_pComms ) {

		if( !pCtx ) {

			return m_pComms->DrvCtrl(uCode, pValue);
		}

		return m_pComms->DevCtrl(pCtx, uCode, pValue);
	}

	if( m_pProxy ) {

		return m_pProxy->Control(pCtx, uCode, pValue);
	}

	return 0;
}

// Overridables

BOOL CCommsPortObject::FindObject(void)
{
	for( ;;) {

		AfxGetObject("pnp.pnp-uart", m_PortPhys, IPortObject, m_pPort);

		if( m_pPort ) {

			m_uPhysMask = m_pPort->GetPhysicalMask();

			AdjustForLogPort();

			return TRUE;
		}

		Sleep(500);
	}

	return FALSE;
}

void CCommsPortObject::FreeObject(void)
{
	AfxRelease(m_pPort);

	m_pPort = NULL;
}

BOOL CCommsPortObject::FindConfig(void)
{
	return TRUE;
}

// Implementation

BOOL CCommsPortObject::AdjustForLogPort(void)
{
	// TODO -- How to handle this???!!!

	#if 0

	if( m_Desc.m_uLogCount > 1 ) {

		UINT Type = m_Desc.m_bPorts[m_PortLog];

		if( Type == physicalRS232 ) {

			m_Desc.m_uMask  =  (1 << physicalRS232);
		}

		if( Type == physicalRS485 ) {

			m_Desc.m_uMask &= ~(1 << physicalRS232);
		}

		return TRUE;
	}

	return FALSE;

	#endif

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// Serial Comms Port
//

// Constructor

CCommsPortSerial::CCommsPortSerial(void)
{
	m_pGrabber     = NULL;

	m_Phys         = 0;

	m_pBaud        = NULL;

	m_pData	       = NULL;

	m_pStop	       = NULL;

	m_pParity      = NULL;

	m_pMode        = NULL;

	m_pShareEnable = NULL;

	m_pSharePort   = NULL;

	m_fRemote      = FALSE;

	m_fShare       = FALSE;

	m_uShare       = 0;

	m_pShare       = NULL;
}

// Destructor

CCommsPortSerial::~CCommsPortSerial(void)
{
	delete m_pBaud;

	delete m_pData;

	delete m_pStop;

	delete m_pParity;

	delete m_pMode;

	delete m_pShareEnable;

	delete m_pSharePort;

	AfxRelease(m_pGrabber);
}

// Initialization

void CCommsPortSerial::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortSerial", pData);

	CCommsPortObject::Load(pData);

	if( m_DriverID ) {

		GetCoded(pData, m_pBaud);
		GetCoded(pData, m_pData);
		GetCoded(pData, m_pStop);
		GetCoded(pData, m_pParity);
		GetCoded(pData, m_pMode);
		GetCoded(pData, m_pShareEnable);
		GetCoded(pData, m_pSharePort);
	}

	AfxGetObject("c3.grab", m_PortPhys, IPortGrabber, m_pGrabber);
}

// Proxy Calls

BOOL CCommsPortSerial::Open(IPortHandler *pData, UINT uFlags)
{
	GrabPort();

	if( CCommsPortObject::Open(pData, uFlags) ) {

		return TRUE;
	}

	GrabDone();

	return FALSE;
}

BOOL CCommsPortSerial::Open(ICommsDriver *pDriver)
{
	GrabPort();

	if( CCommsPortObject::Open(pDriver) ) {

		if( GetItemData(m_pShareEnable, 0) ) {

			C3INT Default = 4000 + m_Number;

			if( (m_uShare = GetItemData(m_pSharePort, Default)) ) {

				m_fShare  = TRUE;
			}
		}

		return TRUE;
	}

	GrabDone();

	return FALSE;
}

void CCommsPortSerial::Poll(void)
{
	if( m_pMutex ) {

		if( m_pMutex->HasRequest() ) {

			m_pComms->Close();

			m_pPort->Close();

			m_pComms->Detach();

			////////

			m_pMutex->Free();

			m_pMutex->Wait(FOREVER);

			////////

			m_pComms->Attach(m_pPort);

			m_pPort->Open(m_Config);

			m_pComms->Open();
		}
	}

	if( GoShared() ) {

		Suspend();

		m_fRemote = TRUE;

		SharePort();

		m_fRemote = FALSE;

		Resume();
	}
}

void CCommsPortSerial::Term(void)
{
	if( m_pShare ) {

		m_pShare->Abort();

		m_pShare->Release();
	}

	CCommsPortObject::Term();

	GrabDone();
}

// Management

BOOL CCommsPortSerial::IsRemote(void)
{
	return m_fRemote;
}

// Overridables

BOOL CCommsPortSerial::FindConfig(void)
{
	if( m_uPhysMask & (1 << physicalRS232) ) {

		m_Config.m_uPhysical = physicalRS232;

		m_Config.m_uPhysMode = 0;
	}
	else {
		if( m_uPhysMask & (1 << physicalRS485) ) {

			if( m_uPhysMask & (1 << physicalRS422Slave) ) {

				if( GetItemData(m_pMode, 0) ) {

					m_Config.m_uPhysical = physicalRS422Slave;

					m_Config.m_uPhysMode = 1;
				}
				else {
					m_Config.m_uPhysical = physicalRS485;

					m_Config.m_uPhysMode = 0;
				}
			}
			else {
				m_Config.m_uPhysical = physicalRS485;

				m_Config.m_uPhysMode = 0;
			}
		}
		else {
			m_Config.m_uPhysical = physicalRS422Slave;

			m_Config.m_uPhysMode = 1;
		}
	}

	m_Config.m_uBaudRate = 0 + GetItemData(m_pBaud, 9600);

	m_Config.m_uDataBits = 8 - GetItemData(m_pData, 0);

	m_Config.m_uStopBits = 1 + GetItemData(m_pStop, 0);

	m_Config.m_uParity   = 0 + GetItemData(m_pParity, 0);

	m_Config.m_uFlags    = 0;

	m_Config.m_bDrop     = 0;

	if( m_Config.m_uBaudRate >= 110 ) {

		if( m_Config.m_uBaudRate <= 115200 ) {

			return TRUE;
		}
	}

	return FALSE;
}

// Implementation

BOOL CCommsPortSerial::GoShared(void)
{
	if( m_fShare ) {

		if( !m_pShare ) {

			AfxNewObject("sock-tcp", ISocket, m_pShare);

			if( m_pShare ) {

				m_pShare->Listen(m_uShare);
			}

			return FALSE;
		}

		UINT Phase;

		if( m_pShare->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_IDLE ) {

				return FALSE;
			}

			if( Phase == PHASE_OPENING ) {

				return FALSE;
			}

			if( Phase == PHASE_OPEN ) {

				return TRUE;
			}
		}

		m_pShare->Release();

		m_pShare = NULL;
	}

	return FALSE;
}

BOOL CCommsPortSerial::IsPureShared(void)
{
	return m_DriverID == 0x3705;
}

UINT CCommsPortSerial::GetShareTimeout(void)
{
	return 1000 * (IsPureShared() ? 60 : 30);
}

void CCommsPortSerial::SharePort(void)
{
	SetTimer(GetShareTimeout());

	for( ;;) {

		UINT Phase;

		if( m_pShare->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_OPEN ) {

				if( ShareData() ) {

					SetTimer(GetShareTimeout());

					continue;
				}

				if( GetTimer() ) {

					Sleep(10);

					continue;
				}

				m_pShare->Close();
			}

			if( Phase == PHASE_ERROR ) {

				m_pShare->Abort();
			}

			if( Phase == PHASE_CLOSING ) {

				m_pShare->Close();
			}
		}

		m_pShare->Release();

		m_pShare = NULL;

		break;
	}
}

BOOL CCommsPortSerial::ShareData(void)
{
	BOOL f1 = ShareSend();

	BOOL f2 = ShareRecv();

	return f1 || f2;
}

BOOL CCommsPortSerial::ShareSend(void)
{
	CBuffer *pBuff = NULL;

	if( m_pShare->Recv(pBuff) == S_OK ) {

		PCBYTE pData = PCBYTE(pBuff->GetData());

		UINT   uSize = pBuff->GetSize();

		m_pLocal->Write(pData, uSize, FOREVER);

		BuffRelease(pBuff);

		return TRUE;
	}

	return FALSE;
}

BOOL CCommsPortSerial::ShareRecv(void)
{
	CBuffer *  pBuff  = NULL;

	BOOL       fData  = FALSE;

	UINT const uSize  = 256;

	UINT       uByte  = NOTHING;

	UINT       uPhase = 0;

	if( (uByte = m_pLocal->Read(20)) < NOTHING ) {

		while( !(pBuff = BuffAllocate(uSize)) ) {

			Sleep(10);
		}

		while( uByte < NOTHING ) {

			*pBuff->AddTail(1) = uByte;

			if( pBuff->GetSize() == uSize ) {

				break;
			}

			uByte = m_pLocal->Read(20);
		}

		while( m_pShare->Send(pBuff) == E_FAIL ) {

			m_pShare->GetPhase(uPhase);

			if( uPhase == PHASE_OPEN ) {

				if( ShareSend() ) {

					fData = TRUE;
				}
				else
					Sleep(10);

				continue;
			}

			BuffRelease(pBuff);

			break;
		}

		fData = TRUE;
	}

	return fData;
}

void CCommsPortSerial::Suspend(void)
{
	m_pProxy->Suspend();

	m_pComms->Close();

	m_pPort->Close();

	m_pComms->Detach();

	AfxNewObject("data-d", IDataHandler, m_pLocal);

	m_pLocal->SetRxSize(2048);

	m_pLocal->SetTxSize(2048);

	m_pPort->Bind(m_pLocal);

	m_pPort->Open(m_Config);
}

void CCommsPortSerial::Resume(void)
{
	m_pPort->Close();

	m_pLocal->Release();

	m_pComms->Attach(m_pPort);

	m_pPort->Open(m_Config);

	m_pComms->Open();

	m_pProxy->Resume();
}

void CCommsPortSerial::GrabPort(void)
{
	if( m_pGrabber ) {

		m_pGrabber->Claim();
	}
}

void CCommsPortSerial::GrabDone(void)
{
	if( m_pGrabber ) {

		m_pGrabber->Free();
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Network Comms Port
//

// Constructor

CCommsPortNetwork::CCommsPortNetwork(void)
{
}

// Destructor

CCommsPortNetwork::~CCommsPortNetwork(void)
{
}

// Initialization

void CCommsPortNetwork::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortNetwork", pData);

	CCommsPort::Load(pData);
}

// Proxy Calls

BOOL CCommsPortNetwork::Open(IPortHandler *pData, UINT uFlags)
{
	return TRUE;
}

BOOL CCommsPortNetwork::Open(ICommsDriver *pComms)
{
	m_pComms = pComms;

	pComms->Attach(NULL);

	pComms->Open();

	return TRUE;
}

void CCommsPortNetwork::Term(void)
{
	if( m_pComms ) {

		m_pComms->Detach();

		m_pComms = NULL;
	}
}

// Management

UINT CCommsPortNetwork::Control(PVOID pCtx, UINT uCode, PCTXT pValue)
{
	if( m_pComms ) {

		if( !pCtx ) {

			return m_pComms->DrvCtrl(uCode, pValue);
		}

		return m_pComms->DevCtrl(pCtx, uCode, pValue);
	}

	if( m_pProxy ) {

		return m_pProxy->Control(pCtx, uCode, pValue);
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// Virtual Comms Port
//

// Constructor

CCommsPortVirtual::CCommsPortVirtual(void)
{
	StdSetRef();

	m_pHandler = NULL;

	m_pSuspend = Create_Mutex();

	m_pMode    = NULL;

	m_pIP      = NULL;

	m_pNum     = NULL;

	m_pLinkOpt = NULL;

	m_pTimeout = NULL;

	m_fOpen    = FALSE;

	m_pSend    = Create_ManualEvent();

	m_hTask    = NULL;

	m_uLast    = 0;
}

// Destructor

CCommsPortVirtual::~CCommsPortVirtual(void)
{
	delete m_pMode;

	delete m_pIP;

	delete m_pNum;

	delete m_pLinkOpt;

	delete m_pTimeout;

	m_pSuspend->Release();

	m_pSend->Release();

	FreeObject();
}

// Initialization

void CCommsPortVirtual::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortVirtual", pData);

	CCommsPortObject::Load(pData);

	if( m_DriverID ) {

		GetCoded(pData, m_pMode);

		GetCoded(pData, m_pIP);

		GetCoded(pData, m_pNum);

		GetCoded(pData, m_pLinkOpt);

		GetCoded(pData, m_pTimeout);
	}
}

// Task List

void CCommsPortVirtual::GetTaskList(CTaskList &List, UINT &uLevel)
{
	if( m_pProxy ) {

		CTaskDef Task;

		Task.m_pEntry = this;
		Task.m_Name   = "PUMP";
		Task.m_uID    = 1;
		Task.m_uCount = 1;
		Task.m_uLevel = uLevel++;
		Task.m_uStack = 0;

		List.Append(Task);

		Task.m_pEntry = this;
		Task.m_Name   = "COMMS";
		Task.m_uID    = 0;
		Task.m_uCount = 1;
		Task.m_uLevel = uLevel++;
		Task.m_uStack = 0;

		List.Append(Task);
	}
}

// Task Entries

void CCommsPortVirtual::TaskInit(UINT uID)
{
	if( uID == 0 ) {

		CCommsPortObject::TaskInit(uID);
	}

	if( uID == 1 ) {

		return;
	}
}

void CCommsPortVirtual::TaskExec(UINT uID)
{
	if( uID == 0 ) {

		CCommsPortObject::TaskExec(uID);
	}

	if( uID == 1 ) {

		m_hTask    = GetCurrentTask();

		m_pSock    = NULL;

		m_pBuff    = NULL;

		m_uMode    = GetItemData(m_pMode, C3INT(0));

		m_uTimeout = GetItemData(m_pTimeout, C3INT(10000));

		m_IP       = HostToMotor(DWORD(GetItemData(m_pIP, C3INT(0))));

		m_uPort    = GetItemData(m_pNum, C3INT(0));

		if( m_IP && m_uPort ) {

			for( ;;) {

				AllocSocket();

				if( CheckSocket() ) {

					while( CheckSocket() ) {

						SendData();

						RecvData();

						PollData();
					}
				}
				else
					DumpSend();
			}
		}

		Sleep(FOREVER);
	}
}

void CCommsPortVirtual::TaskStop(UINT uID)
{
}

void CCommsPortVirtual::TaskTerm(UINT uID)
{
	if( uID == 0 ) {

		CCommsPortObject::TaskTerm(uID);
	}

	if( uID == 1 ) {

		return;
	}
}

// IUnknown

HRESULT CCommsPortVirtual::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPortObject);

	StdQueryInterface(IPortObject);

	return E_NOINTERFACE;
}

ULONG CCommsPortVirtual::AddRef(void)
{
	StdAddRef();
}

ULONG CCommsPortVirtual::Release(void)
{
	StdRelease();
}

// IDevice

BOOL METHOD CCommsPortVirtual::Open(void)
{
	return TRUE;
}

// IPortObject

void METHOD CCommsPortVirtual::Bind(IPortHandler *pHandler)
{
	m_pHandler = pHandler;

	m_pHandler->Bind(this);
}

void METHOD CCommsPortVirtual::Bind(IPortSwitch *pSwitch)
{
}

UINT METHOD CCommsPortVirtual::GetPhysicalMask(void)
{
	return (1 << physicalVirtual);
}

BOOL METHOD CCommsPortVirtual::Open(CSerialConfig const &Config)
{
	// REV3 -- Some remote serial servers have a protocol
	// which would let us configure the Baud rate etc. We
	// should add (optional) support for this.

	if( !m_fOpen ) {

		m_fOpen = TRUE;

		m_pHandler->OnOpen(Config);

		return TRUE;
	}

	return FALSE;
}

void METHOD CCommsPortVirtual::Close(void)
{
	if( m_fOpen ) {

		m_pHandler->OnClose();

		m_fOpen = FALSE;
	}
}

void METHOD CCommsPortVirtual::Send(BYTE bData)
{
	if( m_fOpen ) {

		m_bSend = bData;

		m_pSend->Set();
	}
}

void METHOD CCommsPortVirtual::SetBreak(BOOL fBreak)
{
	// REV3 -- Some remote serial servers have a protocol
	// which would let us set or clear a break state. We
	// should add (optional) support for this.
}

void METHOD CCommsPortVirtual::EnableInterrupts(BOOL fEnable)
{
	for( ;;) {

		if( m_hTask ) {

			if( fEnable ) {

				m_pSuspend->Free();
			}
			else
				m_pSuspend->Wait(FOREVER);

			return;
		}

		Sleep(10);
	}
}

void METHOD CCommsPortVirtual::SetOutput(UINT uOutput, BOOL fOn)
{
	// REV3 -- Some remote serial servers have a protocol
	// which would let us control output lines. We should
	// add (optional) support for this.
}

BOOL METHOD CCommsPortVirtual::GetInput(UINT uInput)
{
	return TRUE;
}

DWORD METHOD CCommsPortVirtual::GetHandle(void)
{
	return NOTHING;
}

// Port Handlers

BOOL METHOD CCommsPortVirtual::OnEvent(void)
{
	return FALSE;
}

void METHOD CCommsPortVirtual::OnTimer(void)
{
}

// Overridables

BOOL CCommsPortVirtual::FindObject(void)
{
	m_pPort = this;

	return TRUE;
}

void CCommsPortVirtual::FreeObject(void)
{
	m_pPort = NULL;
}

// Implementation

void CCommsPortVirtual::AllocSocket(void)
{
	if( !m_pSock ) {

		AfxNewObject("sock-tcp", ISocket, m_pSock);

		if( m_pSock ) {

			if( GetItemData(m_pLinkOpt, 0) == linkKeepAlive ) {

				m_pSock->SetOption(OPT_KEEP_ALIVE, TRUE);
			}

			if( m_uMode == 1 ) {

				m_pSock->Listen(m_uPort);
			}
		}
	}
}

BOOL CCommsPortVirtual::CheckSocket(void)
{
	if( m_pSock ) {

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_IDLE ) {

				return FALSE;
			}

			if( Phase == PHASE_OPENING ) {

				return FALSE;
			}

			if( Phase == PHASE_OPEN ) {

				SetLast(TRUE);

				return TRUE;
			}

			if( Phase == PHASE_ERROR ) {

				m_pSock->Abort();
			}

			if( Phase == PHASE_CLOSING ) {

				m_pSock->Close();
			}
		}
		else
			m_pSock->Abort();

		m_pSock->Release();

		m_pSock = NULL;
	}

	return FALSE;
}

void CCommsPortVirtual::CloseSocket(BOOL fAbort)
{
	if( fAbort )

		m_pSock->Abort();
	else
		m_pSock->Close();

	m_pSock->Release();

	m_pSock = NULL;
}

BOOL CCommsPortVirtual::IsInactive(void)
{
	if( GetItemData(m_pLinkOpt, 0) == linkTimeout ) {

		if( m_uLast ) {

			if( (GetTickCount() - m_uLast) > ToTicks(m_uTimeout) ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	return FALSE;
}

void CCommsPortVirtual::DumpSend(void)
{
	if( m_pSend->Wait(50) ) {

		if( m_uMode == 0 ) {

			if( m_pSock ) {

				m_pSock->Connect(CIPAddr(m_IP), m_uPort);
			}
		}

		BYTE bData;

		m_pSuspend->Wait(FOREVER);

		while( m_pHandler->OnTxData(bData) );

		m_pSend->Clear();

		m_pHandler->OnTxDone();

		m_pSuspend->Free();
	}
}

void CCommsPortVirtual::SendData(void)
{
	if( m_pSend->Wait(20) ) {

		if( !m_pBuff ) {

			m_pBuff = BuffAllocate(1280);
		}

		if( m_pBuff ) {

			PBYTE pData = m_pBuff->GetData();

			PBYTE pSave = pData;

			*pSave++    = m_bSend;

			m_pSuspend->Wait(FOREVER);

			while( m_pHandler->OnTxData(*pSave) ) {

				pSave++;
			}

			m_pSend->Clear();

			m_pHandler->OnTxDone();

			m_pSuspend->Free();

			m_pBuff->AddTail(pSave - pData);

			m_pSock->Send(m_pBuff);

			m_pBuff = NULL;
		}
	}
}

void CCommsPortVirtual::RecvData(void)
{
	CBuffer *pBuff = NULL;

	if( m_pSock->Recv(pBuff) == S_OK ) {

		if( m_fOpen ) {

			PBYTE pData = pBuff->GetData();

			UINT  uSize = pBuff->GetSize();

			m_pSuspend->Wait(FOREVER);

			while( uSize-- ) {

				m_pHandler->OnRxData(*pData++);
			}

			m_pHandler->OnRxDone();

			m_pSuspend->Free();

			SetLast(FALSE);
		}

		pBuff->Release();
	}
}

void CCommsPortVirtual::PollData(void)
{
	if( m_uMode == 1 ) {

		if( IsInactive() ) {

			CloseSocket(TRUE);

			m_uLast = 0;
		}
	}
}

void CCommsPortVirtual::SetLast(BOOL fInit)
{
	if( m_uMode == 1 ) {

		if( !fInit || !m_uLast ) {

			m_uLast = max(1, GetTickCount());
		}
	}
}

//////////////////////////////////////////////////////////////////////////
//
// CAN Comms Port
//

// Constructor

CCommsPortCAN::CCommsPortCAN(void)
{
	m_Baud = 125000;
}

// Destructor

CCommsPortCAN::~CCommsPortCAN(void)
{
}

// Initialization

void CCommsPortCAN::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortCAN", pData);

	CCommsPortObject::Load(pData);

	if( m_DriverID ) {

		m_Baud = GetLong(pData);
	}
}

// Overridables

BOOL CCommsPortCAN::FindObject(void)
{
	if( CCommsPortObject::FindObject() ) {

		if( m_uPhysMask & (1<<physicalCAN) ) {

			return TRUE;
		}

		FreeObject();

		AfxTrace("CAN port has wrong physical layer\n");
	}

	return FALSE;
}

BOOL CCommsPortCAN::FindConfig(void)
{
	m_Config.m_uBaudRate = m_Baud;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// Profibus Comms Port
//

// Constructor

CCommsPortProfibus::CCommsPortProfibus(void)
{
}

// Destructor

CCommsPortProfibus::~CCommsPortProfibus(void)
{
}

// Initialization

void CCommsPortProfibus::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortProfibusItem", pData);

	CCommsPortObject::Load(pData);
}

// Overridables

BOOL CCommsPortProfibus::FindObject(void)
{
	if( CCommsPortObject::FindObject() ) {

		if( m_uPhysMask & (1<<physicalProfibus) ) {

			return TRUE;
		}

		FreeObject();

		AfxTrace("Profibus port has wrong physical layer\n");
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// FireWire Comms Port
//

// Constructor

CCommsPortFireWire::CCommsPortFireWire(void)
{
}

// Destructor

CCommsPortFireWire::~CCommsPortFireWire(void)
{
}

// Initialization

void CCommsPortFireWire::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortFireWire", pData);

	CCommsPortObject::Load(pData);
}

//////////////////////////////////////////////////////////////////////////
//
// DeviceNet Comms Port
//

// Constructor

CCommsPortDevNet::CCommsPortDevNet(void)
{
	m_Baud = 125000;
}

// Destructor

CCommsPortDevNet::~CCommsPortDevNet(void)
{
}

// Initialization

void CCommsPortDevNet::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortDevNet", pData);

	CCommsPortObject::Load(pData);

	if( m_DriverID ) {

		m_Baud = GetLong(pData);
	}
}

// Overridables

BOOL CCommsPortDevNet::FindObject(void)
{
	if( CCommsPortObject::FindObject() ) {

		if( m_uPhysMask & (1<<physicalDeviceNet) ) {

			return TRUE;
		}

		FreeObject();

		AfxTrace("DeviceNet port has wrong physical layer\n");
	}

	return FALSE;
}

BOOL CCommsPortDevNet::FindConfig(void)
{
	m_Config.m_uBaudRate = m_Baud;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// CAT Link Comms Port
//

// Constructor

CCommsPortCatLink::CCommsPortCatLink(void)
{
}

// Destructor

CCommsPortCatLink::~CCommsPortCatLink(void)
{
}

// Initialization

void CCommsPortCatLink::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortCatLink", pData);

	CCommsPortObject::Load(pData);
}

//////////////////////////////////////////////////////////////////////////
//
// MPI Comms Port
//

// Constructor

CCommsPortMPI::CCommsPortMPI(void)
{
	m_ThisDrop = 7;
}

// Destructor

CCommsPortMPI::~CCommsPortMPI(void)
{
}

// Initialization

void CCommsPortMPI::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortMPI", pData);

	CCommsPortObject::Load(pData);

	if( m_DriverID ) {

		m_ThisDrop = GetByte(pData);
	}
}

// Overridables

BOOL CCommsPortMPI::FindConfig(void)
{
	m_Config.m_bDrop = m_ThisDrop;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Port
//

// Constructor

CCommsPortRack::CCommsPortRack(void)
{
}

// Destructor

CCommsPortRack::~CCommsPortRack(void)
{
}

// Initialization

void CCommsPortRack::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortRack", pData);

	CCommsPortObject::Load(pData);
}

// Overridables

BOOL CCommsPortRack::FindConfig(void)
{
	m_Config.m_uPhysical = physicalRS485;
	m_Config.m_uBaudRate = 1000000;
	m_Config.m_uDataBits = 8;
	m_Config.m_uStopBits = 1;
	m_Config.m_uParity   = 0;
	m_Config.m_uFlags    = 0;

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// J1939 Comms Port
//

// Constructor

CCommsPortJ1939::CCommsPortJ1939(void)
{
	m_Baud = 250000;
}

// Initialization

void CCommsPortJ1939::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortJ1939", pData);

	CCommsPortObject::Load(pData);

	if( m_DriverID ) {

		m_Baud = GetLong(pData);
	}
}

// Overridables

BOOL CCommsPortJ1939::FindObject(void)
{
	if( CCommsPortObject::FindObject() ) {

		if( m_uPhysMask & (1<<physicalJ1939) ) {

			return TRUE;
		}

		FreeObject();

		AfxTrace("J1939 port has wrong physical layer\n");
	}

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Test Comms Port
//

// Constructor

CCommsPortTest::CCommsPortTest(void)
{
}

// Destructor

CCommsPortTest::~CCommsPortTest(void)
{
}

// Initialization

void CCommsPortTest::Load(PCBYTE &pData)
{
	ValidateLoad("CCommsPortTest", pData);

	CCommsPortObject::Load(pData);
}

// Overridables

BOOL CCommsPortTest::FindObject(void)
{
	return CCommsPortObject::FindObject();
}

// End of File
