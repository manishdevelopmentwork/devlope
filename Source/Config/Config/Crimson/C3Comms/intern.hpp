
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_INTERN_HPP

#define INCLUDE_INTERN_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "c3comms.hpp"

#include "c3thunk.hpp"

#include "intern.hxx"

#include "space.hpp"

//////////////////////////////////////////////////////////////////////////
//
// About Override Checks
//

// Because the signatures of certain overriden functions have been
// changed, undefined versions of with the old signatures have been
// left in the base class, but with the return type changed. This
// means thta if you try to override the old signature, you'll get
// an error about the return type. This will ensure that you don't
// forget to use the new form when creating a driver!

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Marker
//

#define	C3_PASSED() m_fCrimson3 = TRUE

//////////////////////////////////////////////////////////////////////////
//
// Basic Comms Driver
//

class CBasicCommsDriver : public ICommsDriver
{
	public:
		// Constructor
		CBasicCommsDriver(void);

		// Destructor
		virtual ~CBasicCommsDriver(void);

		// IBase Methods
		UINT Release(void);

		// Driver Data
		WORD	GetID(void);
		UINT	GetType(void);
		CString	GetString(UINT ID);
		UINT	GetFlags(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Tag Import
		BOOL MakeTags(IMakeTags * pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem * pDrvCfg);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem * pDrvCfg);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart, CItem *pDrvCfg);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);
		BOOL IsAddrNamed(CItem *pConfig, CAddress const &Addr);

		// Notifications
		void	NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);
		void	NotifyInit(CItem * pConfig);

	protected:
		// Data Members
		WORD	m_wID;
		UINT	m_uType;
		CString	m_Manufacturer;
		CString	m_DriverName;
		CString	m_Version;
		CString	m_ShortName;
		CString m_DevRoot;
		BOOL	m_fSingle;
		BOOL	m_fNoMap;
		BOOL    m_fCrimson3;

	private:
		// Override Checks
		PVOID ParseAddress(CError &Error, CAddress &Addr, CString Text);
		PVOID ExpandAddress(CString &Text, CAddress const &Addr);
		PVOID SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		PVOID ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Comms Driver
//

class CStdCommsDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CStdCommsDriver(void);

		// Destructor
		~CStdCommsDriver(void);

		// IBase Methods
		UINT Release(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Tag Import
		BOOL MakeTags(IMakeTags * pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem * pDrvCfg);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem * pDrvCfg);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem * pDrvCfg);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);

		// Address Helpers
		virtual BOOL DoParseSpace(INDEX &Index, CItem *pConfig, CString Text);
		virtual BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		virtual BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		virtual BOOL CheckAlignment(CSpace *pSpace);

		// Space List Access
		CSpaceList & GetSpaceList(void);
		CSpace     * GetSpace(INDEX Index);
		CSpace     * GetSpace(CAddress const &Addr);
		CSpace     * GetSpace(CString Text);

	protected:
		// Data Members
		CSpaceList m_List;
		INDEX	   m_n;

		// Space List Support
		void AddSpace(CSpace *pSpace);
		void DeleteAllSpaces(void);

		// Implementation
		CString StripType(CSpace *pSpace, CString &Text);

	private:
		// Override Checks
		PVOID DoParseSpace(INDEX &Index, CString Text);
		PVOID DoParseAddress(CError &Error, CAddress &Addr, CSpace *pSpace, CString Text);
		PVOID DoExpandAddress(CString &Text, CAddress const &Addr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Device Options
//

class CStdDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CStdDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Drop;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Table Device Options (Legacy Only)
//

class CTableDeviceOptions : public CStdDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
	};

//////////////////////////////////////////////////////////////////////////
//
// Named Device Options (Legacy Only)
//

class CNamedDeviceOptions : public CStdDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();
	};

//////////////////////////////////////////////////////////////////////////
//
// Standard Address Selection Dialog
//

class CStdAddrDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CStdAddrDialog(CStdCommsDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:
		// Data Members
		CString		    m_Element;
		CStdCommsDriver   * m_pDriver;
		CItem		  * m_pConfig;
		CAddress	  * m_pAddr;
		BOOL		    m_fPart;
		CSpace            * m_pSpace;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSpaceChange(UINT uID, CWnd &Wnd);
		void OnTypeChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void    SetCaption(void);
		void    LoadElementUI(void);
		void    LoadList(void);
		void    LoadType(void);
		void    FindSpace(void);
		void    ShowType(UINT uType);
		void    ClearAddress(void);
		void    ClearDetails(void);
		void	ClearType(void);
		UINT    GetTypeCode(void);
		CString GetTypeText(void);

		// Overridables
		virtual BOOL    AllowType(UINT uType);
		virtual BOOL	AllowSpace(CSpace *pSpace);
		virtual void    SetAddressFocus(void);
		virtual void    SetAddressText(CString Text);
		virtual CString GetAddressText(void);
		virtual void    ShowAddress(CAddress Addr);
		virtual void    ShowDetails(void);
	};

// End of File

#endif
