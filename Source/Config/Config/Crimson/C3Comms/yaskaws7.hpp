
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASKAWAS7_HPP
	
#define	INCLUDE_YASKAWAS7_HPP

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa TCP/IP Master Device Options
//

class CYaskawaSeries7TCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaSeries7TCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// Space Table
#define	SPDA	1
#define	SPA	2
#define	SPB	3
#define	SPC	4
#define	SPD	5
#define	SPE	6
#define	SPF	7
#define	SPH	8
#define	SPL	9
#define	SPN	10
#define	SPO	11
#define	SPP	12
#define	SPT	13
#define	SPU	14
#define	SPEN	19
#define	SPAC	20
#define	SPEC	30
#define	SPEV	31

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 Inverters Driver
//

class CYaskawaSeries7Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaSeries7Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

	protected:

		// Implementation
		void	AddSpaces(void);
		CString	ValidateSelection(UINT uTable, UINT uAddress);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP/IP Master Driver
//

class CYaskawaSeries7TCPDriver : public CYaskawaSeries7Driver
{
	public:
		// Constructor
		CYaskawaSeries7TCPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

// End of File

#endif
