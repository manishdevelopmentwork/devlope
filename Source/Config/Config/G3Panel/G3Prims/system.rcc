
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//								
// Other Resources
//

#include "PrimMurphyTrendViewer.rcc"

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Generic Viewer
//

CPrimViewer_Schema RCDATA
BEGIN
	L"Back,Background,,Color/Color,,"
	L"Select the background color of the viewer."
	L"\0"

	L"FontMenu,Button Font,,Font/DropPick,,"
	L"Select the font used for the button labels."
	L"\0"

	L"FontWork,Display Font,,Font/DropPick,,"
	L"Select the font used for the main information display."
	L"\0"

	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Alarm Viewer
//

CPrimAlarmViewer_Pages RCDATA
BEGIN
	L"Options,Format,Actions,Time,Priority Colors,Show\0"
END

CPrimAlarmViewer_Schema RCDATA
BEGIN
	L"IncTime,Show Alarm Time,,Enum/DropDown,No|Yes,"
	L"Indicate whether the time of each alarm should be shown."
	L"\0"

	L"IncMarks,Show End Markers,,Enum/DropDown,No|Yes,"
	L"Indicate whether marks should be shown at the top and "
	L"bottom of the list to allow the user to know when they "
	L"have reached the limits of the available data."
	L"\0"

	L"IncNum,Show Numbers,,Enum/DropDown,No|2 Digits|3 Digits|4 Digits|5 Digits,"
	L"Indicate whether the lines should be numbered."
	L"\0"

	L"ColInactive,No Alarms,,ColorPair/ColorPair,,"
	L"Select the colors used to show the indicated state."
	L"\0"

	L"ColActive,Active,,ColorPair/ColorPair,f,"
	L"Select the color used to show the indicated state."
	L"\0"

	L"ColAccepted,Accepted,,ColorPair/ColorPair,f,"
	L"Select the color used to show the indicated state."
	L"\0"

	L"ColWaitAcc,Wait Accept,,ColorPair/ColorPair,f,"
	L"Select the color used to show the indicated state."
	L"\0"

	L"TxtEmpty,When Inactive,,IntlString/IntlString,24,"
	L"Define the text to be displayed when no alarms are present."
	L"\0"

	L"BtnPgUp,PgUp,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPgDn,PgDn,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPrev,Prev,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnNext,Next,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnMute,Mute,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnAccept,Accept,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnHelp,Help,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"ShowPage,Show Page,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"ShowMute,Show Mute,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"ShowAccept,Show Accept,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"ShowHelp,Show Help,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"OnHelp,On Help,,Coded/Action,|None|Data=HelpData/t=HelpTag/a=HelpAlarm/i=HelpIndex,"
	L"Define an expression to be executed when the Help button is pressed."
	L"\0"

	L"UsePriority,Use Prority Colors,,Enum/DropDown,No|Yes,"
	L"Indicate whether active alarms should use priority colors."
	L"\0"

	L"ColPriority1,Priority 1 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority2,Priority 2 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority3,Priority 3 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority4,Priority 4 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority5,Priority 5 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority6,Priority 6 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority7,Priority 7 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"ColPriority8,Priority 8 Color,,ColorPair/ColorPair,f,"
	L"Select the color pair to be used for this alarm priority."
	L"\0"

	L"Reverse,Sort Alarms by,,Enum/DropDown,Newest First|Oldest First,"
	L"Select the chronological ordering of alarms."
	L"\0"

	L"AutoScroll,Auto Scroll Timeout,,Integer/EditBox,|0|seconds|0|300,"
	L"Set the amount of time to wait before resetting the position "
	L"to the top of the list. Set this to 0 to disable the timeout."
	L"\0"

	L"\0"
END

CPrimAlarmViewer_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,IncTime,IncMarks,IncNum,Reverse,AutoScroll\0"
	L"G:1,root,Colors,ColInactive,UsePriority,ColActive,ColAccepted,ColWaitAcc\0"
	L"G:1,root,Messages,TxtEmpty\0"
	L"\0"
END

CPrimAlarmViewer_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back\0"
	L"G:1,root,Fonts,FontWork,FontMenu\0"
	L"T:2,Buttons,Show,Label\0"
	L"R:root,PgUp,ShowPage,BtnPgUp\0"
	L"R:root,PgDn,,BtnPgDn\0"
	L"R:root,Prev,,BtnPrev\0"
	L"R:root,Next,,BtnNext\0"
	L"R:root,Mute,ShowMute,BtnMute\0"
	L"R:root,Accept,ShowAccept,BtnAccept\0"
	L"R:root,Help,ShowHelp,BtnHelp\0"
	L"R:done\0"
	L"\0"
END

CPrimAlarmViewer_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Actions,OnHelp\0"
	L"\0"
END

CPrimAlarmViewer_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Format,Format Mode,Mode\0"
	L"G:1,Format,Time Format,TimeForm,TimeSep,Secs,AM,PM\0"
	L"G:1,Format,Date Format,DateForm,DateSep,Year,Month\0"
	L"\0"
END

CPrimAlarmViewer_Page5 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Priority Color Selection,ColPriority1,ColPriority2,ColPriority3,ColPriority4,ColPriority5,ColPriority6,ColPriority7,ColPriority8\0"
	L"\0"
END

CPrimAlarmViewer_Page6 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Event Viewer
//

CPrimEventViewer_Pages RCDATA
BEGIN
	L"Options,Format,Enables,Time,Show\0"
END

CPrimEventViewer_Schema RCDATA
BEGIN
	L"IncTime,Show Event Time,,Enum/DropDown,No|Yes,"
	L"Indicate whether the time of each event should be shown."
	L"\0"

	L"IncMarks,Show End Markers,,Enum/DropDown,No|Yes,"
	L"Indicate whether marks should be shown at the top and "
	L"bottom of the list to allow the user to know when they "
	L"have reached the limits of the available data."
	L"\0"

	L"IncType,Show Event Type,,Enum/DropDown,No|Yes,"
	L"Indicate whether the type of each event should be shown."
	L"\0"

	L"IncNum,Show Numbers,,Enum/DropDown,No|2 Digits|3 Digits|4 Digits|5 Digits,"
	L"Indicate whether the lines should be numbered."
	L"\0"

	L"TxtEmpty,When Empty,,IntlString/IntlString,24,"
	L"Define the text to be displayed when no events are present."
	L"\0"

	L"BtnPgUp,PgUp,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPgDn,PgDn,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPrev,Prev,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnNext,Next,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnClear,Clear,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"ShowPage,Show Page,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"ShowClear,Show Clear,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"UseClear,Enable Clear,,Coded/Expression,|true,"
	L"Enter an expression to enable or disable clearing of the event log."
	L"\0"

	L"ColEmpty,Event Log Empty,,ColorPair/ColorPair,f,"
	L"Select the colors used to show the message when the viewer is empty."
	L"\0"

	L"ColEvent,Events,,ColorPair/ColorPair,f,"
	L"Select the text colors used to display events."
	L"\0"

	L"ColAlarm,Active Alarms,,ColorPair/ColorPair,f,"
	L"Select the text colors used to display active alarm events."
	L"\0"

	L"ColAccept,Accepted Alarms,,ColorPair/ColorPair,f,"
	L"Select the text colors used to display alarm acceptance events."
	L"\0"

	L"ColClear,Cleared Alarms,,ColorPair/ColorPair,f,"
	L"Select the text colors used to display alarm clearing events."
	L"\0"

	L"AutoScroll,Auto Scroll Timeout,,Integer/EditBox,|0|seconds|0|300,"
	L"Set the amount of time to wait before resetting the position "
	L"to the top of the list. Set this to 0 to disable the timeout."
	L"\0"

	L"\0"
END

CPrimEventViewer_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,IncTime,IncMarks,IncNum,IncType,AutoScroll\0"
	L"G:1,root,Colors,ColEmpty,ColEvent,ColAlarm,ColAccept,ColClear\0"
	L"G:1,root,Messages,TxtEmpty\0"
	L"\0"
END

CPrimEventViewer_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back\0"
	L"G:1,root,Fonts,FontWork,FontMenu\0"
	L"T:2,Buttons,Show,Label\0"
	L"R:root,PgUp,ShowPage,BtnPgUp\0"
	L"R:root,PgDn,,BtnPgDn\0"
	L"R:root,Prev,,BtnPrev\0"
	L"R:root,Next,,BtnNext\0"
	L"R:root,Clear,ShowClear,BtnClear\0"
	L"R:done\0"
	L"\0"
END

CPrimEventViewer_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Enables,UseClear\0"
	L"\0"
END

CPrimEventViewer_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Format,Format Mode,Mode\0"
	L"G:1,Format,Time Format,TimeForm,TimeSep,Secs,AM,PM\0"
	L"G:1,Format,Date Format,DateForm,DateSep,Year,Month\0"
	L"\0"
END

CPrimEventViewer_Page5 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- File Viewer
//

CPrimFileViewer_Pages RCDATA
BEGIN
	L"Options,Format,Show\0"
END

CPrimFileViewer_Schema RCDATA
BEGIN
	L"Root,Root Directory,,ExprString/ExprEditBox,32,"
	L"Select the directory the contents of which are to be shown."
	L"\0"

	L"FontWork,Title Font,,Font/DropPick,,"
	L"Select the font used for the title bar."
	L"\0"

	L"FontText,Text Font,,Font/DropPick,,"
	L"Select the font used for the file contents."
	L"\0"

	L"Number,Line Numbers,,Enum/DropDown,Hide|Show,"
	L"Indicate whether line numbers should be shown."
	L"\0"

	L"Sort,Sort Order,,Enum/DropDown,Do Not Sort|Newest First|Oldest First,"
	L"Indicate how files should be sorted for display."
	L"\0"

	L"IncCSV,CSV Files,,OnOff/Check,,"
	L"Select whether the indicated file type should be shown."
	L"\0"

	L"IncTXT,TXT Files,,OnOff/Check,,"
	L"Select whether the indicated file type should be shown."
	L"\0"

	L"IncLOG,LOG Files,,OnOff/Check,,"
	L"Select whether the indicated file type should be shown."
	L"\0"

	L"TxtEmpty,Empty Directory,,IntlString/IntlString,24,"
	L"Enter the text to be displayed when the directory is empty."
	L"\0"

	L"TxtCannot,Cannot Load,,IntlString/IntlString,24,"
	L"Enter the text to be displayed when the file cannot be loaded."
	L"\0"

	L"TxtEnd,End of File,,IntlString/IntlString,24,"
	L"Enter the text to be displayed at the end of the file."
	L"\0"

	L"BtnDown,PgDn,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnUp,PgUp,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPrev,Prev,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnNext,Next,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnScan,Scan,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnLeft,Left,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnRight,Right,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"ShowScan,Show Scan,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"ShowLeft,Show Left,,Enum/DropDown,No|Yes,"
	L"Select whether the specified button should be shown."
	L"\0"

	L"\0"
END

CPrimFileViewer_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,Root,Number,Sort\0"
	L"G:3,root,File Types,IncCSV,IncTXT,IncLOG\0"
	L"G:1,root,Messages,TxtEmpty,TxtCannot,TxtEnd\0"
	L"\0"
END

CPrimFileViewer_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back\0"
	L"G:1,root,Fonts,FontWork,FontText,FontMenu\0"
	L"T:2,Buttons,Show,Label\0"
	L"R:root,Up,,BtnUp\0"
	L"R:root,Down,,BtnDown\0"
	L"R:root,Prev,,BtnPrev\0"
	L"R:root,Next,,BtnNext\0"
	L"R:root,Scan,ShowScan,BtnScan\0"
	L"R:root,Left,ShowLeft,BtnLeft\0"
	L"R:root,Right,,BtnRight\0"
	L"R:done\0"
	L"\0"
END

CPrimFileViewer_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Trend Viewer
//

CPrimTrendViewer_Pages RCDATA
BEGIN
	L"Options,Format,Buttons,Time,Pens,Fills,Show\0"
END

CPrimTrendViewer_Schema RCDATA
BEGIN
	L"Log,Data Log,,DataLog/DropPick,,"
	L"Select the data log to be displayed."
	L"\0"

	L"Width,View Width,,Enum/DropDown,30 secs|1 min|2 mins|3 mins|4 mins|5 mins|10 mins|20 mins|30 mins|1 hr|2 hrs|3 hrs|4 hrs|5 hrs|6 hrs|7 hrs|8 hrs|9 hrs|10 hrs|11 hrs|12 hrs|1 day|2 days,"
	L"Select the initial amount of data to be shown. The operator "
	L"can then display more or less data using the zoom buttons."
	L"\0"

	L"PenMask,Pen Mask,,Coded/Expression,|Show All,"
	L"Define an expression to show or hide channels according to its bit values."
	L"\0"

	L"FillMask,Fill Mask,,Coded/Expression,|Show All,"
	L"Define an expression to fill below channel plots according to its bit values."
	L"\0"

	L"PenWeight,Pen Weight,,Enum/DropDown,1|2|3|4|5|6|7|8|9|10,"
	L"Indicate how thick the trend lines should be drawn."
	L"\0"

	L"ShowData,Show Values,,Enum/DropDown,No|Yes,"
	L"Indicate whether the current or historical data values should be shown "
	L"in numeric format as well as being displayed graphically."
	L"\0"

	L"ShowCursor,Show Cursor,,Enum/DropDown,No|Yes,"
	L"Indicate whether the optional cursor should be enabled."
	L"\0"

	L"DataBox,Data Values,,Enum/DropDown,Shown as a Footer|Shown on the Left|Shown on the Right,"
	L"Indicate where the data values will be displayed."
	L"\0"

	L"UseFill,Data Display,,Enum/DropDown,Trend Lines|Trend Lines with Fill,"
	L"Select how the data will be displayed."
	L"\0"

	L"FontTitle,Title Font,,Font/DropPick,,"
	L"Select the font for the primitive's title."
	L"\0"

	L"FontWork,Label Font,,Font/DropPick,,"
	L"Select the font for the data items at the top of the viewer."
	L"\0"

	L"FontData,Data Font,,Font/DropPick,,"
	L"Select the font for the optional numeric data values."
	L"\0"

	L"GridTime,Time Axis,,Enum/DropDown,None|Automatic,"
	L"Indicate whether gridlines should be drawn on the time axis."
	L"\0"

	L"GridMode,Data Axis,,Enum/DropDown,None|Major Only|Major and Minor|Automatic,"
	L"Indicate whether and how grid lines should be drawn on the time axis."
	L"\0"

	L"GridMajor,Major Divisions,,Integer/EditBox,|0||2|20,"
	L"Enter the number of major divisions when using manual grid lines."
	L"\0"

	L"GridMinor,Minor Divisions,,Integer/EditBox,|0||2|20,"
	L"Enter the number of major divisions when using manual grid lines."
	L"\0"

	L"GridMin,Minimum,,Coded/Expression,|0,"
	L"Enter the minimum limit used for automatic grid lines."
	L"\0"

	L"GridMax,Maximum,,Coded/Expression,|100,"
	L"Enter the maximum limit used for automatic grid lines."
	L"\0"

	L"Bars,Number of Bars,,Coded/Expression,|0,"
	L"Enter the number of bar used to fill under the trendlines. Set this to 0 "
	L"draw one bar for each distinct point on the trend viewer. If many points are "
	L"being shown, then the display updates may be adversely affected. Setting a lower number of "
	L"bars can improve display performance, at the cost of detail. "
	L"\0"

	L"Precise,Limit Values,,Enum/DropDown,Rounded|Precise,"
	L"Define how the limit values will be chosen. Precise mode will "
	L"use the exact values entered, while Rounded mode will allow "
	L"the viewer to choose values that better fit the tick mark "
	L"spacing."
	L"\0"

	L"ColTitle,Title,,Color/Color,,"
	L"Select the color of the primitive's title."
	L"\0"

	L"ColLabel,Labels,,Color/Color,,"
	L"Select the color of the data items at the top of the viewer."
	L"\0"

	L"ColData,Values,,Color/Color,,"
	L"Select the color of the optional numeric data values."
	L"\0"

	L"ColMajor,Major Gridlines,,Color/Color,,"
	L"Select the color of the major grid lines."
	L"\0"

	L"ColMinor,Minor Gridlines,,Color/Color,,"
	L"Select the color of the minor grid lines."
	L"\0"

	L"ColCursor,Cursor,,Color/Color,,"
	L"Select the color of the cursor."
	L"\0"

	L"ColPen0,Pen 1,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen1,Pen 2,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen2,Pen 3,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen3,Pen 4,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen4,Pen 5,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen5,Pen 6,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen6,Pen 7,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen7,Pen 8,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen8,Pen 9,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPen9,Pen 10,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPenA,Pen 11,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPenB,Pen 12,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPenC,Pen 13,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPenD,Pen 14,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPenE,Pen 15,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColPenF,Pen 16,,Color/Color,,"
	L"Select the pen color for the indicated channel."
	L"\0"

	L"ColFill0,Fill 1,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill1,Fill 2,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill2,Fill 3,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill3,Fill 4,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill4,Fill 5,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill5,Fill 6,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill6,Fill 7,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill7,Fill 8,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill8,Fill 9,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFill9,Fill 10,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFillA,Fill 11,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFillB,Fill 12,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFillC,Fill 13,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFillD,Fill 14,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFillE,Fill 15,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"ColFillF,Fill 16,,Color/Color,,"
	L"Select the fill color for the indicated channel."
	L"\0"

	L"BtnPgLeft,Pg Left,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnLeft,Left,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnLive,Live,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnRight,Right,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPgRight,Pg Right,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnIn,Zoom In,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnOut,Zoom Out,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnLoad,Load,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"\0"
END

CPrimTrendViewer_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,Log,UseFill,Width,PenWeight,PenMask,FillMask,ShowData,ShowCursor,DataBox\0"
	L"G:1,root,Gridlines,GridTime,GridMode,GridMajor,GridMinor,GridMin,GridMax,Precise\0"
	L"\0"
END

CPrimTrendViewer_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back,ColLabel,ColMajor,ColMinor,ColCursor\0"
	L"G:1,root,Fonts,FontWork,FontData,FontMenu\0"
	L"\0"
END

CPrimTrendViewer_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Buttons,BtnPgLeft,BtnLeft,BtnLive,BtnRight,BtnPgRight,BtnIn,BtnOut\0"
	L"\0"
END

CPrimTrendViewer_Page4 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,Format,Time Format,TimeForm,TimeSep,Secs,AM,PM\0"
	L"G:1,Format,Date Format,DateForm,DateSep,Year,Month\0"
	L"\0"
END

CPrimTrendViewer_Page5 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Pens,ColPen0,ColPen1,ColPen2,ColPen3,ColPen4,ColPen5,ColPen6,ColPen7,ColPen8,ColPen9,ColPenA,ColPenB,ColPenC,ColPenD,ColPenE,ColPenF,\0"
	L"\0"
END

CPrimTrendViewer_Page6 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Fills,ColFill0,ColFill1,ColFill2,ColFill3,ColFill4,ColFill5,ColFill6,ColFill7,ColFill8,ColFill9,ColFillA,ColFillB,ColFillC,ColFillD,ColFillE,ColFillF\0"
	L"\0"
END

CPrimTrendViewer_Page7 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- PDF Viewer
//

CPrimPDFViewer_Pages RCDATA
BEGIN
	L"Options,Format,Show\0"
END

CPrimPDFViewer_Schema RCDATA
BEGIN
	L"File,File Name,,ExprString/ExprEditBox,32,"
	L"Select the file name of the PDF to display."
	L"\0"

	L"TxtCannot,Cannot Load,,IntlString/IntlString,24,"
	L"Enter the text to be displayed when the file cannot be loaded."
	L"\0"

	L"TxtWorking,Working,,IntlString/IntlString,24,"
	L"Enter the text to be displayed while the PDF is being loaded."
	L"\0"

	L"BtnLeft,Left,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnRight,Right,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPrev,Up,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnNext,Down,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnZoomIn,ZoomIn,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnZoomOut,ZoomOut,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"\0"
END

CPrimPDFViewer_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Contents,File\0"
	L"G:1,root,Messages,TxtCannot,TxtWorking\0"
	L"\0"
END

CPrimPDFViewer_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back\0"
	L"T:2,Buttons,,Label\0"
	L"R:root,Left,,BtnLeft\0"
	L"R:root,Right,,BtnRight\0"
	L"R:root,Prev,,BtnPrev\0"
	L"R:root,Next,,BtnNext\0"
	L"R:root,Zoom In,,BtnZoomIn\0"
	L"R:root,Zoom Out,,BtnZoomOut\0"
	L"R:done\0"
	L"\0"
END

CPrimPDFViewer_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- User Manager
//

CPrimUserManager_Pages RCDATA
BEGIN
	L"Format,Show\0"
END

CPrimUserManager_Schema RCDATA
BEGIN
	L"BtnPrev,Prev,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnNext,Next,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"BtnPass,Set Pass,,IntlString/IntlString,24,"
	L"Enter the label for the indicated button."
	L"\0"

	L"AutoScroll,Auto Scroll Timeout,,Integer/EditBox,|0|seconds|0|300,"
	L"Set the amount of time to wait before resetting the position "
	L"to the top of the list. Set this to 0 to disable the timeout."
	L"\0"

	L"\0"
END

CPrimUserManager_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back\0"
	L"G:1,root,Fonts,FontWork,FontMenu\0"
	L"G:1,root,Buttons,BtnPrev,BtnNext,BtnPass\0"
//	L"G:1,root,Options,AutoScroll\0"
	L"\0"
END

CPrimUserManager_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Touch Calibration
//

CPrimTouchCalib_Pages RCDATA
BEGIN
	L"Actions,Show\0"
END

CPrimTouchCalib_Schema RCDATA
BEGIN
	L"OnSuccess,On Success,,Coded/Action,|None,"
	L"Define an action to be executed when calibration is successful."
	L"\0"

	L"OnFailure,On Failure,,Coded/Action,|None,"
	L"Define an action to be executed when calibration fails."
	L"\0"

	L"\0"
END

CPrimTouchCalib_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

CPrimTouchCalib_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Actions,OnSuccess,OnFailure\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Touch Tester
//

CPrimTouchTester_Schema RCDATA
BEGIN
	L"Back,Background,,Color/Color,,"
	L"Select the background color of the primitive."
	L"\0"

	L"\0"
END

CPrimTouchTester_Pages RCDATA
BEGIN
	L"Figure,Show\0"
END

CPrimTouchTester_Page1 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Colors,Back\0"
	L"\0"
END

CPrimTouchTester_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Remote Display
//

CPrimRemoteDisplay_Schema RCDATA
BEGIN
	L"Unit,Unit Address,,Integer/EditBox,0|0||0|32,"
	L""
	L"\0"

	L"Port,Comms Port,,CommsPort/DropDown,,"
	L"Select the communications port through which the region bound by this "
	L"primitive is to be relayed. The port in question must have a suitable "
	L"Remote Display driver configured with appropriate comms settings."
	L"\0"

	L"\0"
END

CPrimRemoteDisplay_Page2 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI Information -- Camera
//

CPrimCamera_Schema RCDATA
BEGIN
	L"Scale,Scale,,Enum/DropDown,No Scaling|Increase 2:1|Increase 3:1|Increase 4:1|Scale to Fit - Fast|Scale to Fit - Smooth,"
	L""
	L"\0"

	L"Device,Comms Device,,CommsDevice/DropDown,,"
	L"Select the communications device through which the region bound by this "
	L"primitive is to be relayed. The device in question must have a suitable "
	L"Camera driver configured with appropriate comms settings."
	L"\0"

//	L"Edge,Edge,,,,\0"

//	L"Fill,Fill,,,,\0"

	L"\0"
END

//CPrimCamera_Page2 RCDATA
//BEGIN
//	L"P:1\0"
//	L"G:1,Fill,Fill Format,Pattern,Color1,Color2,Color3\0"
//	L"G:1,Edge,Edge Format,Width,Color\0"
//	L"\0"
//END

CPrimCamera_Page3 RCDATA
BEGIN
	L"P:1\0"
	L"G:1,root,Display State,Visible\0"
	L"\0"
END

// End of File
