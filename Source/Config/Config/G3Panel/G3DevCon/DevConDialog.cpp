
#include "Intern.hpp"

#include "DevConDialog.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Dialog Window
//

// Base Class

#define CBaseClass CDialog

// Runtime Class

AfxImplementRuntimeClass(CDevConDialog, CBaseClass);

// Constructor

CDevConDialog::CDevConDialog(CString Caption, CViewWnd *pView)
{
	m_Caption    = Caption;

	m_pView      = pView;

	m_fTranslate = TRUE;

	m_fLightBack = FALSE;

	m_pButton1   = New CButton;

	m_pButton2   = New CButton;
}

// Dialog Operations

BOOL CDevConDialog::Create(CWnd &Parent)
{
	DWORD dwStyle   = WS_POPUP | WS_SYSMENU | WS_CAPTION | WS_DLGFRAME | WS_CLIPCHILDREN;

	DWORD dwExStyle = WS_EX_DLGMODALFRAME;

	CWnd::Create(m_Caption,
		     dwExStyle, dwStyle,
		     CRect(0, 0, 1000, 1000),
		     Parent,
		     0,
		     NULL
	);

	return TRUE;
}

// Class Definition

PCTXT CDevConDialog::GetDefaultClassName(void) const
{
	return L"CrimsonDlg";
}

BOOL CDevConDialog::GetClassDetails(WNDCLASSEX &Class) const
{
	if( CWnd::GetClassDetails(Class) ) {

		Class.hIcon   = HICON(afxMainWnd->GetClassLong(GCL_HICON));

		Class.hIconSm = HICON(afxMainWnd->GetClassLong(GCL_HICONSM));

		return TRUE;
	}

	return FALSE;
}

// Routing Control

BOOL CDevConDialog::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView ) {

		if( m_pView->RouteMessage(Message, lResult) ) {

			return TRUE;
		}
	}

	return CBaseClass::OnRouteMessage(Message, lResult);
}

// Message Map

AfxMessageMap(CDevConDialog, CBaseClass)
{
	AfxDispatchCommand(IDOK, OnButton)
		AfxDispatchCommand(IDCANCEL, OnButton)

		AfxDispatchMessage(WM_POSTCREATE)
		AfxDispatchMessage(WM_CLOSE)
		AfxDispatchMessage(WM_SIZE)
		AfxDispatchMessage(WM_SETFOCUS)

		AfxMessageEnd(CDevConDialog)
};

// Command Handlers

BOOL CDevConDialog::OnButton(UINT uID)
{
	EndDialog(uID);

	return TRUE;
}

// Message Handlers

void CDevConDialog::OnPostCreate(void)
{
	FindLayout();

	CreateButtons();

	CreateView();

	FindBestSize();

	PlaceDialogCentral();
}

void CDevConDialog::OnClose(void)
{
	EndDialog(IDCANCEL);
}

void CDevConDialog::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
}

void CDevConDialog::OnSize(UINT uCode, CSize Size)
{
	m_pView->MoveWindow(GetViewRect(), TRUE);

	MoveButtons();
}

// Implementation

void CDevConDialog::FindLayout(void)
{
	m_Button.cx = 64;

	m_Button.cy = 24;

	m_Border.cx = 8;

	m_Border.cy = 8;
}

void CDevConDialog::CreateView(void)
{
	UINT  uID       = IDVIEW;

	DWORD dwStyle   = WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE;

	DWORD dwExStyle = WS_EX_CONTROLPARENT;

	m_pView->Create(dwExStyle, dwStyle,
			GetViewRect(),
			ThisObject,
			uID,
			NULL
	);
}

void CDevConDialog::CreateButtons(void)
{
	m_pButton1->Create(IDS("OK"),
			   WS_TABSTOP | WS_VISIBLE | BS_DEFPUSHBUTTON,
			   CRect(),
			   ThisObject,
			   IDOK
	);

	m_pButton2->Create(IDS("Cancel"),
			   WS_TABSTOP | WS_VISIBLE | BS_PUSHBUTTON,
			   CRect(),
			   ThisObject,
			   IDCANCEL
	);

	m_pButton1->SetFont(afxFont(Dialog));

	m_pButton2->SetFont(afxFont(Dialog));
}

void CDevConDialog::FindBestSize(void)
{
	MINMAXINFO Work;

	memset(&Work, 0, sizeof(Work));

	m_pView->SendMessage(WM_GETMINMAXINFO, 0, LPARAM(&Work));

	CSize MinSize = CSize(Work.ptMinTrackSize);

	AdjustWindowSize(MinSize);

	MinSize.cx += 12;

	MinSize.cy += 12;

	MinSize.cy += 48;

	SetWindowSize(MinSize, TRUE);
}

CRect CDevConDialog::GetViewRect(void)
{
	CRect Rect   = GetClientRect();

	Rect.left   += 6;

	Rect.right  -= 6;

	Rect.top    += 8;

	Rect.bottom -= m_Button.cy + 2 * m_Border.cy;

	return Rect;
}

void CDevConDialog::MoveButtons(void)
{
	CRect Rect = GetClientRect();

	Rect.bottom = Rect.bottom - m_Border.cy;

	Rect.top    = Rect.bottom - m_Button.cy;

	Rect.left   = Rect.left   + m_Border.cx;

	Rect.right  = Rect.left   + m_Button.cx;

	m_pButton1->MoveWindow(Rect, TRUE);

	Rect.left   = Rect.right  + m_Border.cx;

	Rect.right  = Rect.left   + m_Button.cx;

	m_pButton2->MoveWindow(Rect, TRUE);
}

// End of File
