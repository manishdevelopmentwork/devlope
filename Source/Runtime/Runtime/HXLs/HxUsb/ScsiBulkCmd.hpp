
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiBulkCmd_HPP

#define	INCLUDE_ScsiBulkCmd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Command Block Wrapper
//

class CScsiBulkCmd : public ScsiBulkCmd
{
	public:
		// Constructor
		CScsiBulkCmd(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Attributes
		bool IsValid(void) const;
		bool IsDataIn(void) const;
		bool IsDataOut(void) const;
		bool HasData(void) const;
		
		// Operations
		void Init(void);
		void Init(UINT uCmdLen);
		void Init(UINT uCmdLen, UINT uTransfer, bool fDataIn); 
		void SetDataIn(bool fIn);

		// Debug
		void Dump(void);
	};

// End of File

#endif
