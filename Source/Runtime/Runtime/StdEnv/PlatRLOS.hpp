
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_PlatRLOS_HPP

#define INCLUDE_PlatRLOS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Platform Flag
//

#define AEON_PLAT_RLOS

//////////////////////////////////////////////////////////////////////////
//
// Environment Check
//

#if !defined(AEON_COMP_GCC) || !defined(AEON_PROC_ARM)

#error "RLOS platform expects GCC and ARM."

#endif

//////////////////////////////////////////////////////////////////////////
//
// Windows API Access
//

#if defined(AEON_NEED_WIN32)

#error "Win32 API access not available from RLOS."

#endif

//////////////////////////////////////////////////////////////////////////
//
// Stack Trace Support
//

extern void ArmStackTrace(struct IDiagOutput *);

//////////////////////////////////////////////////////////////////////////
//
// Host Macros
//

#define HostBreak()	ProcBreak()

#define	HostTrap(x)	ProcTrap(x)

#define HostTrace()	ArmStackTrace(NULL)

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define MAX_PATH	260

//////////////////////////////////////////////////////////////////////////
//
// RLOS API Headers
//

#if defined(AEON_NEED_RLOS)

#include <errno.h>
#include <fcntl.h>
#include <sys/reent.h>
#include <sys/stat.h>
#include <sys/tls.h>

#include "IRlos.hpp"

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS IRQ Levels
//

#if defined(AEON_NEED_RLOS)

#define	IRQL_TASK	0x00
#define	IRQL_DISPATCH	0x10
#define	IRQL_HARDWARE	0x20
#define	IRQL_TIMER	0x21
#define	IRQL_MAXIMUM	0x22

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLSO Processor Panics
//

#if defined(AEON_NEED_RLOS)

#define PANIC_AEON_BREAKPOINT		0x0100
#define PANIC_AEON_NULL_CALL		0x0101
#define PANIC_AEON_PURE_VIRTUAL		0x0102
#define PANIC_AEON_ARRAY_LENGTH		0x0104
#define	PANIC_AEON_BAD_TYPEID		0x0105
#define PANIC_AEON_BAD_CAST		0x0106
#define PANIC_AEON_ABORT		0x0107
#define PANIC_AEON_RAISE		0x0108
#define PANIC_AEON_OUT_OF_MEMORY	0x0109
#define PANIC_FAULT_DATA_ABORT		0x0200
#define PANIC_FAULT_INST_ABORT		0x0201
#define PANIC_FAULT_UNDEFINED		0x0202
#define PANIC_FAULT_DOUBLE		0x02FF
#define PANIC_HAL_BAD_MODE		0x0301
#define PANIC_HAL_BAD_IRQL		0x0302
#define PANIC_EXECUTIVE			0x0400
#define PANIC_EXECUTIVE_UNSUPPORTED	0x0401
#define PANIC_EXECUTIVE_WRONG_THREAD	0x0402
#define PANIC_EXECUTIVE_GUARD_ERROR	0x0403
#define PANIC_EXECUTIVE_UNEXPECTED	0x0404
#define PANIC_EXECUTIVE_REAPER_BUSY	0x0405
#define PANIC_EXECUTIVE_OWNED_MUTEX	0x0406
#define PANIC_DEVICE			0x0500
#define PANIC_APPLICATION		0x0600

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS APIs
//

#if defined(AEON_NEED_RLOS)

extern void ProcSyncCaches(void);
extern void ProcInvalidateInstructionCache(void);
extern void ProcInvalidateDataCache(void);
extern void ProcPurgeDataCache(void);
extern void ProcPurgeDataCache(UINT uAddr, UINT uSize);

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS Interrupt Register
//

#if defined(AEON_NEED_RLOS)

#define	HostRaiseIpr(v)		ASM(	"mrs %0, cpsr\n\t"		\
					"mov r0, %0\n\t"		\
					"orr r0, #192\n\t"		\
					"msr cpsr, r0"			\
					: "=r"(v)			\
					:				\
					: "r0"				\
					)				\

#define	HostRelaxIpr(v)		ASM(	"msr cpsr, %0\n\t"		\
					"mov r0, %0\n\t"		\
					"orr r0, #192\n\t"		\
					"msr cpsr, r0"			\
					:				\
					: "r"(v)			\
					: "r0"				\
					)				\

#define	HostLowerIpr(v)		ASM(	"msr cpsr, %0"			\
					:				\
					: "r"(v)			\
					)				\

#define	HostMaxIpr()		ASM(	"mrs r0, cpsr\n\t"		\
					"orr r0, #192\n\t"		\
					"msr cpsr, r0"			\
					:				\
					:				\
					: "r0"				\
					)				\

#define	HostMinIpr()		ASM(	"mrs r0, cpsr\n\t"		\
					"bic r0, #192\n\t"		\
					"msr cpsr, r0"			\
					:				\
					:				\
					: "r0"				\
					)				\

#define	HostSetIpr(v)		ASM(	"mrs r0, cpsr\n\t"		\
					"bic r0, #192\n\t"		\
					"orr r0, %0\n\t"		\
					"msr cpsr, r0"			\
					:				\
					: "r"(v)			\
					: "r0"				\
					)				\

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS HAL Pointer
//

#if defined(AEON_NEED_RLOS)

extern IHal * phal;

#endif

// End of File

#endif
