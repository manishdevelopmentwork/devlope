
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubSetFeatureReq_HPP

#define	INCLUDE_UsbHubSetFeatureReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Set Feature Request
//

class CUsbHubSetFeatureReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubSetFeatureReq(WORD wFeature);

		// Operations
		void Init(WORD wFeature);
	};

// End of File

#endif
