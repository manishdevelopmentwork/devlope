
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Model Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4000
#define IDS_BLOCKS              0x4000
#define IDS_C04_BASE            0x4001
#define IDS_C04_PART            0x4002
#define IDS_C07Q_BASE           0x4003
#define IDS_C07_BASE            0x4004
#define IDS_C07_PART            0x4005
#define IDS_C10V_BASE           0x4006
#define IDS_C10_BASE            0x4007
#define IDS_C10_PART            0x4008
#define IDS_CA04C_BASE          0x4009
#define IDS_CA04C_PART          0x400A
#define IDS_CA04_BASE           0x400B
#define IDS_CA04_PART           0x400C
#define IDS_CA07CQ_BASE         0x400D
#define IDS_CA07C_BASE          0x400E
#define IDS_CA07C_PART          0x400F
#define IDS_CA07Q_BASE          0x4010
#define IDS_CA07_BASE           0x4011
#define IDS_CA07_PART           0x4012
#define IDS_CA10CV_BASE         0x4013
#define IDS_CA10C_BASE          0x4014
#define IDS_CA10C_PART          0x4015
#define IDS_CA10V_BASE          0x4016
#define IDS_CA10_BASE           0x4017
#define IDS_CA10_PART           0x4018
#define IDS_CA15C_BASE          0x4019
#define IDS_CA15C_PART          0x401A
#define IDS_CA15_BASE           0x401B
#define IDS_CA15_PART           0x401C
#define IDS_CANYON_DA           0x401D
#define IDS_CLOUD_BASE          0x401E /* NOT USED */
#define IDS_CLOUD_DEVICE        0x401F /* NOT USED */
#define IDS_CLOUD_PART          0x4020 /* NOT USED */
#define IDS_COLORADO_DA         0x4021
#define IDS_COLOR_TOUCH         0x4022
#define IDS_CONTROL             0x4023
#define IDS_DA10_BASE           0x4024
#define IDS_DA10_PART           0x4025
#define IDS_DA30_BASE           0x4026
#define IDS_DA30_PART           0x4027
#define IDS_DATA_STATION        0x4028 /* NOT USED */
#define IDS_DISCONTINUED        0x4029 /* NOT USED */
#define IDS_DSPGT_BASE          0x402A /* NOT USED */
#define IDS_DSPGT_PART          0x402B /* NOT USED */
#define IDS_DSPLE_BASE          0x402C /* NOT USED */
#define IDS_DSPLE_PART          0x402D /* NOT USED */
#define IDS_DSPSX_BASE          0x402E /* NOT USED */
#define IDS_DSPSX_PART          0x402F /* NOT USED */
#define IDS_DSPZR_BASE          0x4030 /* NOT USED */
#define IDS_DSPZR_PART          0x4031 /* NOT USED */
#define IDS_ET10RTD_BASE        0x4032
#define IDS_ET10RTD_PART        0x4033
#define IDS_ET16AI20M_BASE      0x4034
#define IDS_ET16AI20M_PART      0x4035
#define IDS_ET16AI8AO_BASE      0x4036
#define IDS_ET16AI8AO_PART      0x4037
#define IDS_ET16DI24_BASE       0x4038
#define IDS_ET16DI24_PART       0x4039
#define IDS_ET16DIAC_BASE       0x403A
#define IDS_ET16DIAC_PART       0x403B
#define IDS_ET16DO24_BASE       0x403C
#define IDS_ET16DO24_PART       0x403D
#define IDS_ET16DORLY_BASE      0x403E
#define IDS_ET16DORLY_PART      0x403F
#define IDS_ET16ISO20M_BASE     0x4040
#define IDS_ET16ISO20M_PART     0x4041
#define IDS_ET16ISOTC_BASE      0x4042
#define IDS_ET16ISOTC_PART      0x4043
#define IDS_ET32AI10V_BASE      0x4044
#define IDS_ET32AI10V_PART      0x4045
#define IDS_ET32AI20M_BASE      0x4046
#define IDS_ET32AI20M_PART      0x4047
#define IDS_ET32DI24_BASE       0x4048
#define IDS_ET32DI24_PART       0x4049
#define IDS_ET32DO24_BASE       0x404A
#define IDS_ET32DO24_PART       0x404B
#define IDS_ET3_BASE            0x404C /* NOT USED */
#define IDS_ET3_PART            0x404D /* NOT USED */
#define IDS_ET8AO20M_BASE       0x404E
#define IDS_ET8AO20M_PART       0x404F
#define IDS_ETHERTRAK3          0x4050
#define IDS_ETMIX20884_BASE     0x4051
#define IDS_ETMIX20884_PART     0x4052
#define IDS_ETMIX24880_BASE     0x4053
#define IDS_ETMIX24880_PART     0x4054
#define IDS_ETMIX24882_BASE     0x4055
#define IDS_ETMIX24882_PART     0x4056
#define IDS_G07_BASE            0x4057
#define IDS_G07_PART            0x4058
#define IDS_G09_BASE            0x4059
#define IDS_G09_PART            0x405A
#define IDS_G10R_BASE           0x405B
#define IDS_G10R_PART           0x405C
#define IDS_G10_BASE            0x405D
#define IDS_G10_PART            0x405E
#define IDS_G12_BASE            0x405F
#define IDS_G12_PART            0x4060
#define IDS_G15_BASE            0x4061
#define IDS_G15_PART            0x4062
#define IDS_G304K2_BASE         0x4063 /* NOT USED */
#define IDS_G304K2_PART         0x4064 /* NOT USED */
#define IDS_G304K_BASE          0x4065 /* NOT USED */
#define IDS_G304K_PART          0x4066 /* NOT USED */
#define IDS_G306K2_BASE         0x4067 /* NOT USED */
#define IDS_G306K2_PART         0x4068 /* NOT USED */
#define IDS_G306K_BASE          0x4069 /* NOT USED */
#define IDS_G306K_PART          0x406A /* NOT USED */
#define IDS_G306_BASE           0x406B /* NOT USED */
#define IDS_G306_PART           0x406C /* NOT USED */
#define IDS_G307K2_BASE         0x406D /* NOT USED */
#define IDS_G307K2_PART         0x406E /* NOT USED */
#define IDS_G308K_BASE          0x406F /* NOT USED */
#define IDS_G308K_PART          0x4070 /* NOT USED */
#define IDS_G308_BASE           0x4071 /* NOT USED */
#define IDS_G308_PART           0x4072 /* NOT USED */
#define IDS_G310K2_BASE         0x4073 /* NOT USED */
#define IDS_G310K2_PART         0x4074 /* NOT USED */
#define IDS_G310R_BASE          0x4075 /* NOT USED */
#define IDS_G310R_PART          0x4076 /* NOT USED */
#define IDS_G310_BASE           0x4077 /* NOT USED */
#define IDS_G310_PART           0x4078 /* NOT USED */
#define IDS_G315_BASE           0x4079 /* NOT USED */
#define IDS_G315_PART           0x407A /* NOT USED */
#define IDS_GENX_BASE           0x407B
#define IDS_GENX_PART           0x407C
#define IDS_GEX_BASE            0x407D
#define IDS_GEX_PART            0x407E
#define IDS_GRAPHITE_C          0x407F
#define IDS_GRAPHITE_CN         0x4080
#define IDS_GROUP_CANYON        0x4081
#define IDS_GROUP_CANYON_C      0x4082
#define IDS_GROUP_CLOUD         0x4083 /* NOT USED */
#define IDS_GROUP_COLORADO      0x4084
#define IDS_GROUP_DA            0x4085
#define IDS_GROUP_DSP           0x4086 /* NOT USED */
#define IDS_GROUP_ET3           0x4087
#define IDS_GROUP_GRAPHITE      0x4088
#define IDS_GROUP_GRAPHITE_C    0x4089
#define IDS_GROUP_GRAPHITE_CN   0x408A
#define IDS_GROUP_HMI           0x408B /* NOT USED */
#define IDS_GROUP_KADET         0x408C /* NOT USED */
#define IDS_GROUP_KADET2        0x408D /* NOT USED */
#define IDS_GROUP_MC            0x408E /* NOT USED */
#define IDS_GROUP_VS            0x408F /* NOT USED */
#define IDS_GSC_BASE            0x4090
#define IDS_GSC_PART            0x4091
#define IDS_MC2GT_BASE          0x4092 /* NOT USED */
#define IDS_MC2GT_PART          0x4093 /* NOT USED */
#define IDS_MC2LE_BASE          0x4094 /* NOT USED */
#define IDS_MC2LE_PART          0x4095 /* NOT USED */
#define IDS_MC2SX_BASE          0x4096 /* NOT USED */
#define IDS_MC2SX_PART          0x4097 /* NOT USED */
#define IDS_MC2V2_BASE          0x4098 /* NOT USED */
#define IDS_MC2V2_PART          0x4099 /* NOT USED */
#define IDS_MCZR_BASE           0x409A /* NOT USED */
#define IDS_MCZR_PART           0x409B /* NOT USED */
#define IDS_MODEM_SLOT          0x409C
#define IDS_MODULES             0x409D
#define IDS_MODULES_IO          0x409E
#define IDS_MOD_CONTROLLER      0x409F /* NOT USED */
#define IDS_NONE                0x40A0
#define IDS_NOTE_EDITING_IS     0x40A1
#define IDS_PROGRAMMING_PORT    0x40A2 /* NOT USED */
#define IDS_QVGA_EMULATION      0x40A3
#define IDS_RS_AUXILIARY_PORT   0x40A4 /* NOT USED */
#define IDS_RS_AUXILIARY_PORT_2 0x40A5 /* NOT USED */
#define IDS_RS_COM1             0x40A6
#define IDS_RS_COM1_2           0x40A7
#define IDS_RS_COM3             0x40A8 /* NOT USED */
#define IDS_RS_COM3_2           0x40A9 /* NOT USED */
#define IDS_RS_COMMS_PORT_1     0x40AA
#define IDS_RS_COMMS_PORT_2     0x40AB
#define IDS_RS_COMMS_PORT_3     0x40AC
#define IDS_RS_COMMS_PORT_B_1   0x40AD /* NOT USED */
#define IDS_RS_COMMS_PORT_B_2   0x40AE
#define IDS_RS_COMM_PORT        0x40AF /* NOT USED */
#define IDS_RS_COMM_PORT_1      0x40B0 /* NOT USED */
#define IDS_RS_COMM_PORT_2      0x40B1 /* NOT USED */
#define IDS_RS_COMM_PORT_B      0x40B2 /* NOT USED */
#define IDS_RS_PGM_PORT         0x40B3 /* NOT USED */
#define IDS_RS_PROGRAM_PORT     0x40B4
#define IDS_UDFB                0x40B5 /* NOT USED */
#define IDS_VARIABLES           0x40B6 /* NOT USED */
#define IDS_VIDEO_STATION       0x40B7 /* NOT USED */
#define IDS_VIRTUAL             0x40B8
#define IDS_VIRTUAL_MONO        0x40B9
#define IDS_VIRTUAL_X           0x40BA
#define IDS_VIRTUAL_X_2         0x40BB
#define IDS_VIRTUAL_X_3         0x40BC
#define IDS_VIRTUAL_X_4         0x40BD
#define IDS_VIRTUAL_X_5         0x40BE
#define IDS_VIRTUAL_X_6         0x40BF
#define IDS_VS_BASE             0x40C0 /* NOT USED */
#define IDS_VS_PART             0x40C1 /* NOT USED */
#define IDS_WVGA_MODE           0x40C2
#define IDS_X                   0x40C3
#define IDS_X_2                 0x40C4
#define IDS_X_EMULATED          0x40C5
#define IDS_X_EMULATED_2        0x40C6
#define IDS_DA50_BASE           0x40C7
#define IDS_DA500A_PART         0x40C8
#define IDS_DA500B_PART         0x40C9
#define IDS_DA500D_PART         0x40CA
#define IDS_DA70_BASE           0x40CB
#define IDS_DA70X0E_PART        0x40CC
#define IDS_DA70X0F_PART        0x40CD
#define IDS_DA70X0G_PART        0x40CE
#define IDS_DA70X0H_PART        0x40CF
#define IDS_MANTICORE_DA	0x40D0
#define IDS_GROUP_MANTICORE	0x40D3

// End of File

#endif
