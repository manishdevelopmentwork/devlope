
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CellTimeClient_HPP

#define	INCLUDE_CellTimeClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseTimeClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// Cell Time Client
//

class CCellTimeClient : public CBaseTimeClient
{
public:
	// Constructor
	CCellTimeClient(CJsonConfig *pJson);

protected:
	// Overridables
	BOOL PerformSync(void);
};

// End of File

#endif
