
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Printf Formatted String
//

// Constructors

CPrintf::CPrintf(PCTXT pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);

	PTXT pResult = IntPrintf(pFormat, pArgs);

	Alloc(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

CPrintf::CPrintf(PCSTR pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);

	PTXT pResult = IntPrintf(CString(pFormat), pArgs);

	Alloc(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

CPrintf::CPrintf(CEntity Format, ...)
{
	va_list pArgs;
	
	va_start(pArgs, Format);

	PTXT pFormat = IntLoadString(Format);

	PTXT pResult = IntPrintf(pFormat, pArgs);

	Alloc(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
			
	delete pFormat;

	va_end(pArgs);
	}

//////////////////////////////////////////////////////////////////////////
//
// Windows Formatted String
//

// Constructors

CFormat::CFormat(PCTXT pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(pFormat, pArgs);

	Alloc(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

CFormat::CFormat(PCSTR pFormat, ...)
{
	va_list pArgs;
	
	va_start(pArgs, pFormat);

	AfxValidateStringPtr(pFormat);
	
	PTXT pResult = IntFormat(CString(pFormat), pArgs);

	Alloc(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;

	va_end(pArgs);
	}

CFormat::CFormat(CEntity Format, ...)
{
	va_list pArgs;
	
	va_start(pArgs, Format);

	PTXT pFormat = IntLoadString(Format);

	PTXT pResult = IntFormat(pFormat, pArgs);

	Alloc(wstrlen(pResult));
	
	wstrcpy(m_pText, pResult);
	
	delete pResult;
			
	delete pFormat;

	va_end(pArgs);
	}

// End of File
