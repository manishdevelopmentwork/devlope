
#include "intern.hpp"

#include "NtpClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// NTP Client
//

// Constants

static INT64 cMicro = 1000000;

static INT64 cScale = INT64(0x100000000);

// Constructor

CNtpClient::CNtpClient(CJsonConfig *pJson, UINT uFaces) : CPxeIpClient(uFaces)
{
	m_bOption = 42;

	ApplyConfig(pJson);

	StdSetRef();
}

// Destructor

CNtpClient::~CNtpClient(void)
{
}

// IUnknown

HRESULT CNtpClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CNtpClient::AddRef(void)
{
	StdAddRef();
}

ULONG CNtpClient::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CNtpClient::TaskInit(UINT uTask)
{
	SetThreadName("NtpSync");

	if( m_uMode ) {

		return TRUE;
	}

	Release();

	return FALSE;
}

INT CNtpClient::TaskExec(UINT uTask)
{
	time_t uWhen = time(NULL);

	UINT   uBack = 10;

	for( ;;) {

		time_t t = time(NULL);

		if( FindServerList() ) {

			uWhen = t;
		}

		if( t >= uWhen ) {

			if( PerformSync() || PerformSync() || PerformSync() ) {

				uWhen = t + 60 * m_uPeriod;

				uBack = 5;
			}
			else {
				uWhen     = t + uBack;

				m_uActive = (m_uActive + 1) % m_ServerIps.GetCount();

				if( !m_uActive ) {

					if( uBack < 640 ) {

						uBack *= 2;
					}
				}
			}
		}

		Sleep(5000);
	}

	return 0;
}

void CNtpClient::TaskStop(UINT uTask)
{
}

void CNtpClient::TaskTerm(UINT uTask)
{
	Release();
}

// Implementation

void CNtpClient::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		m_uMode   = pJson->GetValueAsUInt("mode", 1, 0, 2);

		m_uPeriod = pJson->GetValueAsUInt("period", 720, 60, 2880);

		m_ConfigIps.Append(pJson->GetValueAsIp("ntp1", CIpAddr::m_Empty));

		m_ConfigIps.Append(pJson->GetValueAsIp("ntp2", CIpAddr::m_Empty));
	}

	m_DefaultIps.Append(CIpAddr(129, 6, 15, 28));
	m_DefaultIps.Append(CIpAddr(132, 163, 97, 1));
	m_DefaultIps.Append(CIpAddr(132, 163, 96, 1));
	m_DefaultIps.Append(CIpAddr(128, 138, 140, 44));
	m_DefaultIps.Append(CIpAddr(129, 6, 15, 29));
	m_DefaultIps.Append(CIpAddr(132, 163, 97, 2));
	m_DefaultIps.Append(CIpAddr(132, 163, 96, 2));
	m_DefaultIps.Append(CIpAddr(128, 138, 141, 172));
}

BOOL CNtpClient::PerformSync(void)
{
	AfxNewAutoObject(pSock, "sock-udp", ISocket);

	if( pSock ) {

		IPREF Ip = m_ServerIps[m_uActive];

		pSock->Connect(Ip, 123);

		for( UINT uTry = 0; uTry < 2; uTry++ ) {

			BOOL fKeyID = !uTry;

			BYTE bData[52] = { 0 };

			UINT uSize     = fKeyID ? sizeof(bData) : 48;

			memset(bData, 0, sizeof(bData));

			bData[0] = 0x1B;

			StoreTime(bData, 10, GetTime64());

			if( pSock->Send(bData, uSize) == S_OK ) {

				for( SetTimer(10000); GetTimer(); Sleep(50) ) {

					UINT uSize = sizeof(bData);

					if( pSock->Recv(bData, uSize) == S_OK ) {

						if( bData[0] == 0x00 || bData[0] == 0x1C ) {

							INT64 t1 = FetchTime(bData, 6);

							INT64 t2 = FetchTime(bData, 8);

							INT64 t3 = FetchTime(bData, 10);

							INT64 t4 = GetTime64();

							INT64 rt = (t4 - t1) - (t2 - t3);

							INT64 nt = t3 + rt / 2;

							if( SetTime64(nt, Ip) ) {

								return TRUE;
							}
						}

						break;
					}
				}
			}
		}
	}

	return FALSE;
}

BOOL CNtpClient::SetTime64(INT64 t64, IPREF ip)
{
	struct timeval tv;

	DWORD t1 = DWORD(t64 >> 32);

	DWORD t2 = DWORD(t64 & (cScale - 1));

	tv.tv_sec  = time_t(t1 - 2208988800);

	tv.tv_usec = time_t(t2 * cMicro / cScale);

	if( settimeofday(&tv, NULL) == 0 ) {

		struct tm *tm = gmtime(&tv.tv_sec);

		AfxTrace("ntp: time set to %2.2u/%2.2u/%4.4u %2.2u:%2.2u:%2.2u.%3.3u\n",
			 tm->tm_mon  + 1,
			 tm->tm_mday + 0,
			 tm->tm_year + 1900,
			 tm->tm_hour,
			 tm->tm_min,
			 tm->tm_sec,
			 tv.tv_usec / 1000
			 );

		return TRUE;
	}

	return FALSE;
}

INT64 CNtpClient::GetTime64(void)
{
	struct timeval tv;

	gettimeofday(&tv, NULL);

	DWORD t1 = DWORD(tv.tv_sec  + 2208988800);

	DWORD t2 = DWORD(tv.tv_usec * cScale / cMicro);

	return INT64(t2) + (INT64(t1) << 32);
}

void CNtpClient::StoreTime(PBYTE pData, UINT n, INT64 time)
{
	((INT64 *) (((PDWORD) pData) + n))[0] = MotorToHost(time);
}

INT64 CNtpClient::FetchTime(PBYTE pData, UINT n)
{
	return MotorToHost(((INT64 *) (((PDWORD) pData) + n))[0]);
}

// End of File
