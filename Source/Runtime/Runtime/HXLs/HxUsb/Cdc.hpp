
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbCdc_HPP

#define	INCLUDE_UsbCdc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cdc Sub Class
//

enum
{
	classDirectLine		= 0x01,
	classAbstract		= 0x02,
	classTelephone		= 0x03,
	classMultiChannel	= 0x04,
	classCAPI		= 0x05,
	classEthernet		= 0x06,
	classATM		= 0x07,
	classWirelessHandset	= 0x08,
	classDeviceManagement	= 0x09,
	classMobileDirectLine	= 0x0A,
	classOBEX		= 0x0B,
	classEthernetEmulation	= 0x0C,
	classNetworkControl	= 0x0D,
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Cdc Protocol Codes
//

enum
{
	protNone		= 0x00,
	protNTB			= 0x01,
	protV250		= 0x01,
	protPCCA101		= 0x02,
	protPCCA101a		= 0x03,
	protGSM707		= 0x04,
	prot3GPP27		= 0x05,
	protCDMA		= 0x06,
	protEEM			= 0x07,
	protExtern		= 0xFE,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Data Class Interfaces Class Codes
//

enum
{
	classData		= 0x0A,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Request Types
//

enum
{
	reqSendEncap		= 0x00,
	reqRecvEncap		= 0x01,
	reqSetCommFeature	= 0x02,
	reqGetCommFeature	= 0x03,
	reqClearCommFeature	= 0x04,
	reqSetAuxLineState	= 0x10,
	reqSetHookState		= 0x11,
	reqPulseSetup		= 0x12,
	reqSendPulse		= 0x13,
	reqSetPulsetime		= 0x14,
	reqRingAuxJack		= 0x15,
	reqSetLineCoding	= 0x20,
	reqGetLineCoding	= 0x21,
	reqSetCtrlLineState	= 0x22,
	reqSendBreak		= 0x23,
	reqSetRingerParams	= 0x30,
	reqGetRingerParams	= 0x31,
	reqSetOperationParams	= 0x32,
	reqGetOperationParams	= 0x33,
	reqSetLineParams	= 0x34,
	reqGetLineParams	= 0x35,
	reqDialDigits		= 0x36,
	reqSetUnitParam		= 0x37,
	reqGetUnitParam		= 0x38,
	reqClearUnitParam	= 0x39,
	reqGetProfile		= 0x3A,
	reqSetEnetMultFilter	= 0x40,
	reqSetEnetPowerFilter	= 0x41,
	reqGetEnetPowerFilter	= 0x42,
	reqSetEnetPacketFilter	= 0x43,
	reqGetEnetStats		= 0x44,
	reqSetATMDataFormat	= 0x50,
	reqGetATMDevStats	= 0x51,
	reqSetATMDefaultVC	= 0x52,
	reqGetATMVCStats	= 0x53,
	reqGetNTBParams		= 0x80,
	reqGetNetAddress	= 0x81,
	reqSetNetAddress	= 0x82,
	reqGetNTBFormat		= 0x83,
	reqSetNTBFormat		= 0x84,
	reqGetNTBSize		= 0x85,
	reqSetNTBSize		= 0x86,
	reqGetMaxSize		= 0x87,
	reqSetMaxSize		= 0x88,
	reqGetCrcMode		= 0x89,
	reqSetCrcMode		= 0x8A,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Notifications
//

enum
{
	reqConnection		= 0x00,
	reqResponse		= 0x01,
	reqRingDetect		= 0x09,
	reqSerialState		= 0x20,
	reqCallState		= 0x28,
	reqLineState		= 0x29,
	reqSpeedChange		= 0x2A,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Descriptor Types
//

enum
{
	descCsInterface		= 0x24,
	descCsEndpoint		= 0x25,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Descriptor Sub Types
//

enum
{
	descHeader		= 0x00,
	descCall		= 0x01,
	descAbstract		= 0x02,
	descDirectLine		= 0x03,
	descTelRinger		= 0x04,
	descTelState		= 0x05,
	descUnion		= 0x06,
	descCountry		= 0x07,
	descTelMode		= 0x08,
	descUsbTerm		= 0x09,
	descNetworkChan		= 0x0A,
	descProtocolUnit	= 0x0B,
	descExtension		= 0x0C,
	descMultiChannel	= 0x0D,
	descCAPI		= 0x0E,
	descEthernet		= 0x0F,
	descATM			= 0x10,
	descWirelessHandset	= 0x11,
	descMobileDirectLine	= 0x12,
	descMDLM		= 0x13,
	descDev			= 0x14,
	descOBEX		= 0x15,
	descCommand		= 0x16,
	descCommandDetail	= 0x17,
	descTelControl		= 0x18,
	descOBEXService		= 0x19,
	descNetworkControl	= 0x1A,
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Base Descriptor
//

#pragma pack(1)

struct CdcFuncDesc : public UsbDesc
{
	BYTE	m_bSubType;
	};

//////////////////////////////////////////////////////////////////////////
//
// Ethernet Networking Descriptor
//

struct CdcNetDesc : public CdcFuncDesc
{
	BYTE	m_iMacAddr;
	DWORD	m_dwNetStats;
	WORD	m_wMaxSegment;
	WORD	m_wNumMulti;
	BYTE	m_wNumPower;
	};
				
//////////////////////////////////////////////////////////////////////////
//
// Network Control Descriptor
//

struct CdcNcmDesc : public CdcFuncDesc
{
	WORD	m_wVer;
	BYTE	m_bCaps;
	};
				
//////////////////////////////////////////////////////////////////////////
//
// Cdc Basic Descriptor
//

struct CdcBasicDesc : public CdcFuncDesc
{
	BYTE	m_bCaps;
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Call Management Descriptor
//

struct CdcCallDesc : public CdcBasicDesc
{
	BYTE	m_bDataInterface;
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Telephone Ringer Descriptor
//

struct CdcTelRingerDescr : public CdcFuncDesc
{
	BYTE	m_bSteps;
	BYTE	m_bPatterns;
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Uninon Descriptor
//

struct CdcUnionDesc : public CdcFuncDesc
{
	BYTE	m_bControl;
	BYTE	m_bInterface[];
	};

//////////////////////////////////////////////////////////////////////////
//
// Cdc Abstract Descriptor
//

typedef struct CdcBasicDesc CdcAbstractDesc;

//////////////////////////////////////////////////////////////////////////
//
// Cdc Direct Line Descriptor
//

typedef struct CdcBasicDesc CdcDirectLineDesc;

//////////////////////////////////////////////////////////////////////////
//
// Cdc Telephone Descriptor
//

typedef struct CdcBasicDesc CdcTelephoneDesc;

//////////////////////////////////////////////////////////////////////////
//
// Cdc Line Coding
//

struct CdcLineCoding
{
	DWORD	m_dwBaud;
	BYTE	m_bStop;
	BYTE	m_bParity;
	BYTE	m_bData;
	};
				
//////////////////////////////////////////////////////////////////////////
//
// NTB Param Block
//

struct CdcNtbParam
{
	WORD	m_wLength;
	WORD	m_wFormats;
	DWORD	m_dwRecvSize;
	WORD	m_wRecvDivisor;
	WORD	m_wRecvReminder;
	WORD	m_wRecvAlign;
	WORD	m_wReserved;
	DWORD	m_dwSendSize;
	WORD	m_wSendDivisor;
	WORD	m_wSendReminder;
	WORD	m_wSendAlign;
	WORD	m_wSendCount;
	};
				
//////////////////////////////////////////////////////////////////////////
//
// NTB Size
//

struct CdcNtbSize
{
	DWORD	m_dwMaxSize;
	WORD	m_wMaxCount;
	WORD	m_wReserved;
	};
				
#pragma pack()

// End of File

#endif
