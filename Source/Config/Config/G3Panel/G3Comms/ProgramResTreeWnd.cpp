
#include "Intern.hpp"

#include "ProgramResTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramItem.hpp"

#include "ProgramPrototype.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Resource Window
//

// Dynamic Class

AfxImplementDynamicClass(CProgramResTreeWnd, CResTreeWnd);

// Constructor

CProgramResTreeWnd::CProgramResTreeWnd(void) : CResTreeWnd( L"Programs",
							    AfxRuntimeClass(CFolderItem),
							    AfxRuntimeClass(CProgramItem)
							    )
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfFunc = RegisterClipboardFormat(L"C3.1 Action");
	}

// Data Object Construction

BOOL CProgramResTreeWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddCodeFragment(pMake);

	AddIdentifier(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CProgramResTreeWnd::AddCodeFragment(CDataObject *pData)
{
	if( m_pNamed ) {

		if( m_pNamed->IsKindOf(m_Class) ) {

			CProgramItem *pProgram = (CProgramItem *) m_pNamed;

			// TODO -- Add default parameters.

			if( pProgram->m_pProt->m_Type ) {

				CString Name = m_pNamed->GetName();

				CPrintf Code = CPrintf(L"%s()", Name);

				pData->AddText(m_cfCode, Code);

				return TRUE;
				}
			else {
				CString Name = m_pNamed->GetName();

				CPrintf Code = CPrintf(L"%s()", Name);

				pData->AddText(m_cfFunc, Code);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Tree Loading

void CProgramResTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"ProgTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CProgramResTreeWnd::GetRootImage(void)
{
	return 0;
	}

UINT CProgramResTreeWnd::GetItemImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(m_Class) ) {

		CProgramItem *pProgram = (CProgramItem *) pItem;

		return pProgram->GetTreeImage();
		}

	return 22;
	}

// End of File
