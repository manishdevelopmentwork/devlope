
//////////////////////////////////////////////////////////////////////////
//
// EtherTrak3 Modbus Serial Driver
//

#include "E3ModbusSerial.hpp"

// Instantiator

INSTANTIATE(CE3ModbusSerialDriver);

// Constructor

CE3ModbusSerialDriver::CE3ModbusSerialDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_pCtx      = NULL;

	m_uMaxWords = 128;
	
	m_uMaxBits  = 2000;
	}

// Destructor

CE3ModbusSerialDriver::~CE3ModbusSerialDriver(void)
{
	}

// Device

CCODE MCALL CE3ModbusSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uPing		= GetWord(pData);
			m_pCtx->m_bDrop		= GetByte(pData);			
			m_pCtx->m_fFlipLong	= GetByte(pData);
			m_pCtx->m_fFlipReal	= GetByte(pData);

			m_pCtx->m_uPoll		= 0;

			m_pCtx->m_uMax01	= 512;
			m_pCtx->m_uMax02	= 512;
			m_pCtx->m_uMax03	= 32;
			m_pCtx->m_uMax04	= 32;
			m_pCtx->m_uMax15	= 512;
			m_pCtx->m_uMax16	= 32;

			m_pCtx->m_fRLCAuto	= FALSE;
			m_pCtx->m_fDisable15	= FALSE;
			m_pCtx->m_fDisable16	= FALSE;
			m_pCtx->m_fDisable5	= FALSE;
			m_pCtx->m_fDisable6	= FALSE;
			m_pCtx->m_fNoCheck	= FALSE;
			m_pCtx->m_fNoReadEx	= FALSE;
			m_pCtx->m_fSwapCRC	= FALSE;
			m_pCtx->m_fWriteReply	= FALSE;

			SetLastPoll();
			
			Limit(m_pCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax04, 1, m_uMaxWords);
			Limit(m_pCtx->m_uMax15, 1, m_pCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pCtx->m_uMax16, 1, m_pCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

// Entry Points

CCODE MCALL CE3ModbusSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address = Addr;

	if( Address.a.m_Table > addrNamed ) {

		Address.a.m_Table -= addrNamed;
		}

	return CModbusDriver::Read(Address, pData, uCount);
	}

CCODE MCALL CE3ModbusSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	CAddress Address = Addr;

	if( Address.a.m_Table > addrNamed ) {

		Address.a.m_Table -= addrNamed;
		}

	return CModbusDriver::Write(Address, pData, uCount);
	}

// End of File
