
#include "Intern.hpp"

#include "DispColorMultiEntry.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color Entry
//

// Dynamic Class

AfxImplementDynamicClass(CDispColorMultiEntry, CCodedHost);

// Constructor

CDispColorMultiEntry::CDispColorMultiEntry(void)
{
	m_pData = NULL;

	m_Color = MAKELONG(GetRGB(31,31,31),GetRGB(0,0,0));
	}

// Operations

void CDispColorMultiEntry::Set(CString Data)
{
	InitCoded(NULL, L"Data", m_pData, Data);
	}

void CDispColorMultiEntry::Set(CString Data, CString Fore, CString Back)
{
	InitCoded(NULL, L"Data", m_pData, Data);

	WORD Lo = 0;

	WORD Hi = 0;

	if( Fore[0] == '0' && tolower(Fore[1]) == 'x' ) {

		Lo = WORD(wcstoul(PCTXT(Fore)+2, NULL, 16));
		}

	if( Back[0] == '0' && tolower(Back[1]) == 'x' ) {

		Hi = WORD(wcstoul(PCTXT(Back)+2, NULL, 16));
		}

	m_Color = MAKELONG(Lo, Hi);
	}

void CDispColorMultiEntry::Set(UINT uData, UINT uColor)
{
	InitCoded(NULL, L"Data", m_pData, CPrintf(L"%d", uData));

	m_Color = ImportColor(uColor);
	}

// UI Update

void CDispColorMultiEntry::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Data" ) {

		DoEnables(pHost);

		return;
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Data Matching

BOOL CDispColorMultiEntry::MatchData(DWORD Data, UINT Type, BOOL fRange, DWORD &Color)
{
	if( m_pData ) {

		DWORD Test = m_pData->ExecVal();

		if( fRange ) {

			if( Type == typeInteger ) {

				if( INT(Data) > INT(Test) ) {

					Color = m_Color;

					return TRUE;
					}
				}

			if( Type == typeReal ) {

				if( I2R(Data) > I2R(Test) ) {

					Color = m_Color;

					return TRUE;
					}
				}

			return FALSE;
			}

		if( Data == Test ) {

			Color = m_Color;

			return TRUE;
			}
		}

	return FALSE;
	}

// Type Access

BOOL CDispColorMultiEntry::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Data" ) {

		CCodedHost *pHost = (CCodedHost *) GetParent(3);

		pHost->GetTypeData(L"SubValue", Type);

		return TRUE;
		}

	return FALSE;
	}

// Operations

void CDispColorMultiEntry::UpdateTypes(BOOL fComp)
{
	UpdateType(NULL, L"Data", m_pData, fComp);
	}

// Download Support

BOOL CDispColorMultiEntry::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pData);

	Init.AddLong(m_Color);

	return TRUE;
	}

// Meta Data Creation

void CDispColorMultiEntry::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddVirtual(Data);
	Meta_AddInteger(Color);

	Meta_SetName((IDS_MULTISTATE_ENTRY));
	}

// Implementation

void CDispColorMultiEntry::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Color", m_pData ? TRUE : FALSE);
	}

UINT CDispColorMultiEntry::ImportColor(UINT uData)
{
	BYTE bFore = LOBYTE(uData);

	BYTE bBack = HIBYTE(uData);

	WORD wFore = C3GetGdiColor(C3GetPaletteEntry(bFore));

	WORD wBack = C3GetGdiColor(C3GetPaletteEntry(bBack));

	return MAKELONG(wFore, wBack);
	}

// End of File
