
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAUIN6Module_HPP

#define INCLUDE_DAUIN6Module_HPP

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDAUIN6Input;
class CDAUIN6AnalogInputConfig;
class CDAUIN6InputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 Module
//

class CDAUIN6Module : public CManticoreGenericModule
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDAUIN6Module(void);

	// UI Management
	CViewWnd * CreateMainView(void);

	// Comms Object Access
	UINT GetObjectCount(void);
	BOOL GetObjectData(UINT uIndex, CObjectData &Data);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	CDAUIN6Input       * m_pInput;
	CDAUIN6InputConfig * m_pInputConfig;

protected:
	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
