
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_PersistPage_HPP

#define INCLUDE_PersistPage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Persistent Data Page
//

class DLLNOT CPersistPage : public CObject
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPersistPage(UINT uPage);

		// Destructor
		~CPersistPage(void);

		// Operations
		void  Register(DWORD dwAddr, UINT uSize);
		DWORD Allocate(UINT uSize);
		void  Free(DWORD dwAddr, UINT uSize);
		void  Free(void);

	protected:
		// Data Members
		UINT  m_uPage;
		PBYTE m_pMap;
		UINT  m_uBytes;
		UINT  m_uSize;

		// Implementation
		void  GeneralRegister(DWORD dwAddr, UINT uSize);
		DWORD GeneralAllocate(UINT uSize);
		void  GeneralFree(DWORD dwAddr, UINT uSize);
	};

// End of File

#endif
