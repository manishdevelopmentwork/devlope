
#include "intern.hpp"

#include "secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Item
//

// Constructor

CTagFlag::CTagFlag(void)
{
	m_TreatAs    = 0;
	m_TakeBit    = 0;
	m_Manipulate = 0;
	m_Atomic     = 0;
	m_HasSP      = 0;
	m_pSetpoint  = NULL;
	m_pEvent1    = NULL;
	m_pEvent2    = NULL;
	m_pTrigger1  = NULL;
	m_pTrigger2  = NULL;
	m_pSec       = New CSecDesc;
	m_uSize      = 0;
	m_pData      = NULL;
	m_fPoll      = FALSE;
	}

// Destructor

CTagFlag::~CTagFlag(void)
{
	delete m_pSetpoint;
	delete m_pEvent1;
	delete m_pEvent2;
	delete m_pTrigger1;
	delete m_pTrigger2;
	delete m_pData;
	}

// Initialization

void CTagFlag::Load(PCBYTE &pData)
{
	ValidateLoad("CTagFlag", pData);

	CDataTag::Load(pData);

	m_TreatAs    = GetByte(pData);
	m_TakeBit    = GetByte(pData);
	m_Manipulate = GetByte(pData);
	m_Atomic     = GetByte(pData);
	m_HasSP      = GetByte(pData);

	if( m_HasSP ) {

		GetCoded(pData, m_pSetpoint);
		}

	m_pEvent1   = CTagEventFlag  ::Create(pData);
	m_pEvent2   = CTagEventFlag  ::Create(pData);
	m_pTrigger1 = CTagTriggerFlag::Create(pData);
	m_pTrigger2 = CTagTriggerFlag::Create(pData);

	m_pSec->Load(pData);

	GetCoded(pData, m_pOnWrite);

	InitData();
	}

// Attributes

UINT CTagFlag::GetDataType(void) const
{
	return typeInteger;
	}

// Operations

void CTagFlag::SetIndex(UINT uIndex)
{
	if( m_pEvent1 ) {
		
		m_pEvent1->SetIndex(MAKELONG(uIndex, 0x0100));

		m_pEvent1->SetCount(m_Extent);
		}

	if( m_pEvent2 ) {
		
		m_pEvent2->SetIndex(MAKELONG(uIndex, 0x0200));

		m_pEvent2->SetCount(m_Extent);
		}

	if( m_pTrigger1 ) {

		m_pTrigger1->SetCount(m_Extent);
		}

	if( m_pTrigger2 ) {

		m_pTrigger2->SetCount(m_Extent);
		}

	CTag::SetIndex(uIndex);
	}

BOOL CTagFlag::SetPollScan(UINT uCode)
{
	if( m_pEvent1 ) {
		
		m_pEvent1->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pEvent2 ) {
		
		m_pEvent2->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pTrigger1 ) {
		
		m_pTrigger1->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pTrigger2 ) {
		
		m_pTrigger2->SetScan(uCode);

		m_fPoll = TRUE;
		}

	if( m_pSetpoint ) {

		m_fPoll = TRUE;
		}

	if( m_fPoll ) {

		UINT c = m_Extent ? m_Extent : 1;

		for( UINT n = 0; n < c; n++ ) {

			CDataRef Ref;

			Ref.m_Ref     = 0;

			Ref.x.m_Array = n;

			SetScan(Ref, uCode);
			}

		SetItemScan(m_pSetpoint, uCode);

		return TRUE;
		}

	return FALSE;
	}

void CTagFlag::Poll(UINT uDelta)
{
	if( m_fPoll ) {

		UINT c  = m_Extent ? m_Extent : 1;

		if( !IsItemAvail(m_pSetpoint) ) {

			return;
			}

		for( UINT n = 0; n < c; n++ ) {

			CDataRef Ref;

			Ref.m_Ref     = 0;

			Ref.x.m_Array = n;

			if( IsAvail(Ref, availNone) ) {

				DWORD SP = 0;

				DWORD PV = GetData(Ref, typeInteger, getNone);

				if( m_pSetpoint ) {

					SP = m_pSetpoint->Execute(typeInteger, PDWORD(&n));
					}

				if( n < 256 ) {

					if( m_pEvent1 ) {
						
						m_pEvent1->Poll(n, SP, PV, uDelta);
						}

					if( m_pEvent2 ) {
						
						m_pEvent2->Poll(n, SP, PV, uDelta);
						}
					}

				if( m_pTrigger1 ) {
					
					m_pTrigger1->Poll(n, SP, PV, uDelta);
					}

				if( m_pTrigger2 ) {
					
					m_pTrigger2->Poll(n, SP, PV, uDelta);
					}
				}
			}
		}
	}

BOOL CTagFlag::GetEventText(CUnicode &Text, UINT uItem, UINT uPos)
{
	if( uItem == 1 ) {

		if( m_pEvent1 ) {

			return m_pEvent1->GetEventText(Text, uPos);
			}
		}

	if( uItem == 2 ) {

		if( m_pEvent2 ) {

			return m_pEvent2->GetEventText(Text, uPos);
			}
		}

	return FALSE;
	}

// Evaluation

BOOL CTagFlag::IsAvail(CDataRef const &Ref, UINT Flags)
{
	if( !IsItemAvail(m_pOnWrite) ) {

		return FALSE;
		}

	if( LocalData() ) {

		return TRUE;
		}

	if( m_Ref ) {

		if( m_pBlock ) {

			if( (Flags & availData) || !m_Extent || !m_RdMode ) {

				int nPos = FindPos(Ref);

				return m_pBlock->IsAvail(nPos);
				}

			return TRUE;
			}

		return FALSE;
		}

	return CDataTag::IsAvail(Ref, 0); 
	}

BOOL CTagFlag::SetScan(CDataRef const &Ref, UINT Code)
{
	SetItemScan(m_pOnWrite, Code);

	if( LocalData() ) {

		return TRUE;
		}

	if( m_Ref ) {

		if( m_pBlock ) {

			if( m_Extent ) {

				if( !m_RdMode ) {

					for( UINT i = 0; i < m_uRegs; i++ ) {

						int nPos = m_nPos + i;

						m_pBlock->SetScan(nPos, Code);
						}
					}

				else if( m_RdMode == 1 ) {

					if( Code == scanUser ) {

						int nPos = FindPos(Ref);

						return m_pBlock->SetScan(nPos, Code);
						}

					return FALSE;
					}

				return TRUE;
				}

			int nPos = FindPos(Ref);

			return m_pBlock->SetScan(nPos, Code);
			}
		}

	return CDataTag::SetScan(Ref, Code);
	}

DWORD CTagFlag::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( Type == typeReal ) {

		DWORD Data = GetData(Ref, typeInteger, Flags);

		return R2I(C3REAL(C3INT(Data)));
		}

	if( IsInteger(Type) ) {

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( LocalData() ) {

				UINT i = n / 8;

				BYTE m = 1 << (n % 8);

				if( m_pData[i] & m ) {

					return TRUE;
					}

				return FALSE;
				}

			if( m_Ref ) {

				if( m_pBlock ) {
					
					int nPos = FindPos(Ref);

					if( m_Extent ) {

						if( m_RdMode >= 2 ) {

							int nSpan = m_RdMode - 2;

							int nFrom = nPos - (nSpan+0);

							int nTo   = nPos + (nSpan+1);

							for( int n = nFrom; n < nTo; n++ ) {

								m_pBlock->SetScan(n, scanOnce);
								}
							}
						}

					DWORD dwData = m_pBlock->GetData(nPos);

					DWORD dwMask = FindMask(Ref);

					DWORD Data   = (dwData & dwMask) ? TRUE : FALSE;

					if( (Flags & getMaskSource) <= getManipulated ) {

						Manipulate(Data);
						}

					return Data;
					}
				}

			DWORD dwData = CDataTag::GetData(Ref, typeVoid, Flags);

			DWORD dwMask = FindMask(Ref);

			DWORD Data   = (dwData & dwMask) ? TRUE : FALSE;

			if( (Flags & getMaskSource) <= getManipulated ) {

				Manipulate(Data);
				}
	
			return Data;
			}
		}

	return GetNull(Type);
	}

BOOL CTagFlag::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	if( IsNumeric(Type) ) {

		Data   = Data ? 1 : 0;

		UINT n = Ref.x.m_Array;

		if( n < m_uSize ) {

			if( m_Access < 2 || m_pOnWrite ) {

				if( !(Flags & setForce) ) {

					DWORD Read = GetData(Ref, Type, Flags);

					if( !Read == !Data ) {

						return TRUE;
						}
					}

				if( !AllowWrite(Ref, Data, Type) ) {

					return FALSE;
					}

				if( m_pOnWrite ) {

					DWORD Args[] = { Data, n };

					m_pOnWrite->Execute(typeVoid, Args);
					}

				if( m_Access < 2 ) {

					if( LocalData() ) {

						UINT i = n / 8;

						BYTE m = 1 << (n % 8);

						if( Data ) {

							m_pData[i] |=  m;
							}
						else
							m_pData[i] &= ~m;

						if( m_Addr ) {

							SaveData(i);
							}

						if( !m_Ref ) {

							return TRUE;
							}
						}

					if( m_Ref ) {

						if( m_pBlock ) {

							int   nPos   = FindPos(Ref);

							DWORD dwMask = FindMask(Ref);

							DWORD dwData = 0;

							if( (Flags & setMaskSource) <= setManipulated ) {

								Manipulate(Data);
								}

							if( dwMask < NOTHING ) {

								if( m_Atomic ) {

									return m_pBlock->SetWriteData(nPos, Flags, Data, dwMask);
									}

								dwData = m_pBlock->GetData(nPos);

								EditBit(dwData, dwMask, Data);
								}
							else
								dwData = MakeOneZero(Data);

							return m_pBlock->SetWriteData(nPos, Flags, dwData);
							}
						}
					else {
						DWORD dwMask = FindMask(Ref);

						DWORD dwData = 0;

						if( (Flags & setMaskSource) <= setManipulated ) {

							Manipulate(Data);
							}

						if( dwMask < NOTHING ) {

							dwData = m_pValue->ExecVal();

							EditBit(dwData, dwMask, Data);
							}
						else
							dwData = MakeOneZero(Data);

						return m_pValue->SetValue(dwData, typeVoid, Flags);
						}
					}
				}
			}
		}

	return CDataTag::SetData(Ref, Data, Type, Flags);
	}

DWORD CTagFlag::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	switch( ID ) {

		case tpSetPoint:
			return FindSP(Ref, Type);

		case tpMinimum:
			return 0;

		case tpMaximum:
			return 1;

		case tpTextOff:
			return FindAsText(Ref, typeInteger, 0);

		case tpTextOn:
			return FindAsText(Ref, typeInteger, 1);

		case tpAlarms:
			return FindEventStatusMask(Ref);
		}

	return CDataTag::GetProp(Ref, ID, Type);
	}

// Property Location

DWORD CTagFlag::FindSP(CDataRef const &Ref, UINT Type)
{
	if( m_pSetpoint ) {

		DWORD n = Ref.x.m_Array;

		return m_pSetpoint->Execute(Type, &n);
		}

	return GetNull(Type);
	}

DWORD CTagFlag::FindEventStatusMask(CDataRef const &Ref)
{
	DWORD n = Ref.x.m_Array;

	DWORD m = 0;

	if( m_pEvent1 && m_pEvent1->IsActive(n) ) {

		m |= 1;
		}

	if( m_pEvent2 && m_pEvent2->IsActive(n) ) {

		m |= 2;
		}

	return m;
	}

// Implementation

int CTagFlag::FindPos(CDataRef const &Ref)
{
	if( m_Extent ) {
		
		if( m_TreatAs >= 3 ) {

			return m_nPos + Ref.x.m_Array / m_uBits;
			}
		}

	return m_nPos + Ref.x.m_Array;
	}

DWORD CTagFlag::FindMask(CDataRef const &Ref)
{
	if( m_TreatAs >= 3 ) {

		UINT uIndex = m_Extent ? (Ref.x.m_Array % m_uBits) : m_TakeBit;

		if( m_TreatAs == 4 ) {

			uIndex = m_uBits - 1 - uIndex;
			}
	
		return (1 << uIndex);
		}
	
	return NOTHING;
	}

void CTagFlag::Manipulate(DWORD &Data)
{
	if( m_Manipulate ) {

		Data = !Data;
		}
	}

void CTagFlag::EditBit(DWORD &dwData, DWORD dwMask, DWORD Data)
{
	if( Data ) {
		
		dwData |= dwMask;

		return;
		}

	dwData &= ~dwMask;
	}

DWORD CTagFlag::MakeOneZero(DWORD Data)
{
	DWORD dwData;

	if( m_TreatAs == 2 ) {

		if( Data ) {
		
			dwData = R2I(1.0);
			}
		else
			dwData = R2I(0.0);
		}
	else {
		if( Data ) {
		
			dwData = 1;
			}
		else
			dwData = 0;
		}

	return dwData;
	}

BOOL CTagFlag::InitData(void)
{
	if( LocalData() ) {

		UINT uAlloc;

		m_uSize = max(m_Extent, 1);

		uAlloc  = (m_uSize + 7) / 8;

		m_pData = New BYTE [ uAlloc ];

		if( m_pSim ) {

			if( m_pSim->ExecVal() ) {

				memset(m_pData, 0xFF, uAlloc);
				}
			else
				memset(m_pData, 0x00, uAlloc);
			}
		else {
			if( m_Addr ) {

				g_pPersist->GetData( PBYTE(m_pData),
						m_Addr,
						uAlloc
						);
				}
			else
				memset(m_pData, 0x00, uAlloc);
			}

		return TRUE;
		}

	m_uSize = max(m_Extent, 1);

	return FALSE;
	}

void CTagFlag::SaveData(UINT n)
{
	UINT a = m_Addr + n;

	BYTE d = m_pData[n];

	g_pPersist->PutByte(a, d);
	}

// End of File
