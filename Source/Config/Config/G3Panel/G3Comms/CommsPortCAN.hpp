
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortCAN_HPP

#define INCLUDE_CommsPortCAN_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAN Comms Port
//

class DLLNOT CCommsPortCAN : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortCAN(void);

		// Overridables
		UINT GetPageType(void) const;
		char GetPortTag(void) const;
		BOOL IsRemotable(void) const;
		void FindBindingMask(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_Baud;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
