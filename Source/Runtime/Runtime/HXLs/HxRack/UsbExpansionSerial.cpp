
#include "Intern.hpp"

#include "UsbExpansionSerial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Expansion Serial Object
//

// Constructor

CUsbExpansionSerial::CUsbExpansionSerial(IUsbHostFuncDriver *pDriver) : CUsbExpansion(pDriver)
{
	}

// IUnknown

HRESULT METHOD CUsbExpansionSerial::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IExpansionSerial);

	return CUsbExpansion::QueryInterface(riid, ppObject);
	}

ULONG METHOD CUsbExpansionSerial::AddRef(void)
{
	return CUsbExpansion::AddRef();
	}

ULONG METHOD CUsbExpansionSerial::Release(void)
{
	return CUsbExpansion::Release();
	}

// IExpansionInterface

PCTXT METHOD CUsbExpansionSerial::GetName(void)
{
	return CUsbExpansion::GetName();
	}

UINT METHOD CUsbExpansionSerial::GetClass(void)
{
	return CUsbExpansion::GetClass();
	}

UINT METHOD CUsbExpansionSerial::GetIndex(void)
{
	return CUsbExpansion::GetIndex();
	}

UINT METHOD CUsbExpansionSerial::GetPower(void)
{
	return CUsbExpansion::GetPower();
	}

UINT METHOD CUsbExpansionSerial::GetIdent(void)
{
	return CUsbExpansion::GetIdent();
	}

BOOL METHOD CUsbExpansionSerial::HasBootLoader(void)
{
	return CUsbExpansion::HasBootLoader();
	}

IDevice * METHOD CUsbExpansionSerial::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return CUsbExpansion::MakeObject(pDriver);
	}

IExpansionPnp * METHOD CUsbExpansionSerial::QueryPnpInterface(IDevice *pObject)
{
	return CUsbExpansion::QueryPnpInterface(pObject);
	}

// IExpansionSerial

UINT METHOD CUsbExpansionSerial::GetPortCount(void)
{
	return 0;
	}

DWORD METHOD CUsbExpansionSerial::GetPortMask(void)
{
	return 0;
	}

UINT METHOD CUsbExpansionSerial::GetPortType(UINT iPort)
{
	return rackNone;
	}

// End of File
