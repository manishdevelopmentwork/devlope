
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4100
#define IDS_ADD_ADDITIONAL      0x4100
#define IDS_ADD_ANALOG          0x4101
#define IDS_ADD_DISCRETE        0x4102
#define IDS_ADD_NEW_ANALOG      0x4103
#define IDS_ADD_NEW_DISCRETE    0x4104
#define IDS_ANALOG              0x4105 /* NOT USED */
#define IDS_ANALOG_CHANNEL      0x4106 /* NOT USED */
#define IDS_ANALOG_INPUT        0x4107
#define IDS_ANALOG_INPUTS       0x4108
#define IDS_ANALOG_OUTPUT       0x4109 /* NOT USED */
#define IDS_ANALOG_OUTPUTS      0x410A
#define IDS_ANALOG_OUTPUT_2     0x410B
#define IDS_ANALOG_TRANSFER     0x410C
#define IDS_BURNOUT_DETECTION   0x410D
#define IDS_CHANNELS            0x410E
#define IDS_CHANNEL_CAPTION     0x410F
#define IDS_CHANNEL_MANAGER     0x4110
#define IDS_COLD_JUNCTION       0x4111
#define IDS_COMMS_CAPTION       0x4112
#define IDS_COMMS_DEVICE        0x4113
#define IDS_COMMS_MANAGER       0x4114
#define IDS_COMMS_NAME          0x4115
#define IDS_COMMS_PORT          0x4116
#define IDS_COMMS_PORT_ID       0x4117
#define IDS_CONFIGURATION       0x4118
#define IDS_COUNTERS            0x4119
#define IDS_COUNTER_RESET       0x411A
#define IDS_CREATE              0x411B
#define IDS_DELETE_1            0x411C /* NOT USED */
#define IDS_DELETE_2            0x411D
#define IDS_DELETE_3            0x411E
#define IDS_DEV                 0x411F
#define IDS_DEVICES             0x4120
#define IDS_DEVICE_COMMANDS     0x4121
#define IDS_DISCRETE            0x4122 /* NOT USED */
#define IDS_DISCRETE_CHANNEL    0x4123 /* NOT USED */
#define IDS_DISCRETE_INPUT      0x4124
#define IDS_DISCRETE_INPUTS     0x4125
#define IDS_DISCRETE_OUTPUT     0x4126
#define IDS_DISCRETE_OUTPUTS    0x4127
#define IDS_DISCRETE_TRANSFER   0x4128
#define IDS_DOWNLOAD            0x4129
#define IDS_DRIVER              0x412A
#define IDS_DRIVER_SELECTION    0x412B
#define IDS_DRIVER_SETTINGS     0x412C
#define IDS_ENABLE_COUNTER      0x412D
#define IDS_ETHERNET_1          0x412E
#define IDS_ETHERNET_2          0x412F
#define IDS_ETHERNET_3          0x4130
#define IDS_FEATURE             0x4131
#define IDS_FEATURE_2           0x4132
#define IDS_FEATURE_3           0x4133
#define IDS_FORMAT              0x4134
#define IDS_FORMAT_3            0x4135
#define IDS_FORMAT_4            0x4136
#define IDS_FREE_INTERNAL       0x4137
#define IDS_FREE_INTERNAL_2     0x4138
#define IDS_HEARTBEAT_BYPASS    0x4139
#define IDS_INTEGRATION         0x413A
#define IDS_IO_STATUS_READ      0x413B
#define IDS_IO_TRANSFER         0x413C
#define IDS_LINK_STATUS_READ    0x413D
#define IDS_LINK_STATUS_READ_2  0x413E
#define IDS_MODBUS              0x413F
#define IDS_MODBUS_ASCII        0x4140
#define IDS_MODBUS_MASTER       0x4141
#define IDS_MODBUS_RTU_SLAVE    0x4142
#define IDS_MODBUS_UDP          0x4143
#define IDS_MODE                0x4144
#define IDS_NAME_FMT_IS         0x4145
#define IDS_NETWORK             0x4146
#define IDS_NETWORK_OVERLOAD    0x4147
#define IDS_NETWORK_PORT        0x4148
#define IDS_NEW_ANALOG          0x4149
#define IDS_NEW_DISCRETE        0x414A
#define IDS_NONE                0x414B
#define IDS_OF_FMT              0x414C
#define IDS_OPTIONS             0x414D
#define IDS_PHYSICAL_IO         0x414E
#define IDS_PORT_COMMANDS       0x414F
#define IDS_POWER_OK_READ       0x4150
#define IDS_POWER_OK_READ_2     0x4151
#define IDS_RANGE               0x4152
#define IDS_RENAME_FMT          0x4153
#define IDS_REPORTING           0x4154
#define IDS_RESERVED            0x4155
#define IDS_RESET               0x4156
#define IDS_RESOLUTION          0x4157
#define IDS_RING_COMPLETE       0x4158
#define IDS_SCALE               0x4159
#define IDS_SCALE_BEYOND_PV     0x415A
#define IDS_SECURITY_MANAGER    0x415B
#define IDS_SELECT              0x415C
#define IDS_SELECT_BIT_TO       0x415D
#define IDS_SELECT_COUNTER      0x415E
#define IDS_SELECT_TIMEBASE     0x415F
#define IDS_SELF_TEST_OK_READ   0x4160
#define IDS_SERIAL_PORT         0x4161
#define IDS_SERVICES            0x4162
#define IDS_SIXNET_UDR          0x4163
#define IDS_STATUS_BIT          0x4164
#define IDS_SYSTEM              0x4165
#define IDS_TEMPERATURE         0x4166
#define IDS_THERMOCOUPLE_TYPE   0x4167
#define IDS_TIMEBASES           0x4168
#define IDS_TIME_BASES          0x4169
#define IDS_TPO_REGISTERS       0x416A
#define IDS_TRANSFER            0x416B
#define IDS_TRANSFER_CAPTION    0x416C
#define IDS_TRANSFER_MANAGER    0x416D
#define IDS_TYPE                0x416E
#define IDS_UNIVERSAL_MASTER    0x416F
#define IDS_UNIVERSAL_SLAVE     0x4170
#define IDS_USER                0x4171 /* NOT USED */
#define IDS_WATCHDOG            0x4172
#define IDS_WEBSERVER           0x4173

//////////////////////////////////////////////////////////////////////////
//
// Comms Image Index Values
//

#define	IDI_COMMS		0
#define	IDI_PORT		1
#define	IDI_PORT_SERIAL		1
#define	IDI_DEVICE		2
#define	IDI_PORT_ETHERNET	5
#define	IDI_USER		17
#define	IDI_SERVICES		21
#define	IDI_WEBSERVER		22
#define	IDI_SECURITY		27
#define IDI_TRANSFER_GROUP	43
#define	IDI_WATCHDOG		45
#define	IDI_IO			46
#define	IDI_DI			47
#define	IDI_DO			47
#define	IDI_AI			47
#define	IDI_AO			47
#define IDI_ANALOG_READ		48
#define IDI_ANALOG_WRITE	49
#define IDI_DISCRETE_READ	50
#define IDI_DISCRETE_WRITE	51

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_COMMS			0xC1
#define	IDM_COMMS_ADD_DEVICE		0xC101
#define	IDM_COMMS_DEL_DEVICE		0xC102

#define	IDM_TRANSFER			0xC2
#define	IDM_TRANSFER_NEW_DISCRETE	0xC200
#define	IDM_TRANSFER_NEW_ANALOG		0xC201

// End of File

#endif
