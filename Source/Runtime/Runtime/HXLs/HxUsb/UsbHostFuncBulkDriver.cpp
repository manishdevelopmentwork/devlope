
#include "Intern.hpp"

#include "UsbHostFuncBulkDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDescList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Function Bulk Driver 
//

// Constructor

CUsbHostFuncBulkDriver::CUsbHostFuncBulkDriver(void)
{
	m_pName = "Host Function Bulk Driver";

	m_pCtrl = NULL;
	
	m_pSend = NULL;
	
	m_pRecv = NULL;
	}

// IUnknown

HRESULT CUsbHostFuncBulkDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostBulkDriver);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostFuncBulkDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHostFuncBulkDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IUsbHostFuncDriver

void CUsbHostFuncBulkDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);
	}

void CUsbHostFuncBulkDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncDriver::Bind(pSink);
	}

void CUsbHostFuncBulkDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHostFuncBulkDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostFuncBulkDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHostFuncBulkDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHostFuncBulkDriver::GetClass(void)
{
	return CUsbHostFuncDriver::GetClass();
	}

UINT CUsbHostFuncBulkDriver::GetSubClass(void)
{
	return CUsbHostFuncDriver::GetSubClass();
	}

UINT CUsbHostFuncBulkDriver::GetProtocol(void)
{
	return CUsbHostFuncDriver::GetProtocol();
	}

UINT CUsbHostFuncBulkDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

BOOL CUsbHostFuncBulkDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHostFuncBulkDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( FindPipes(List) ) {

		m_uState = usbOpen; 

		return CUsbHostFuncDriver::Open();
		}

	return FALSE;
	}

BOOL CUsbHostFuncBulkDriver::Close(void)
{
	return CUsbHostFuncDriver::Close();
	}

void CUsbHostFuncBulkDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncDriver::Poll(uLapsed);
	}

// IUsbHostBulkDriver

BOOL CUsbHostFuncBulkDriver::SendBulk(PCBYTE pData, UINT uCount)
{
	Trace(debugData, "SendBulk(Count=%d)", uCount);

	return m_pSend->SendBulk(PBYTE(pData), uCount, false);
	}

UINT CUsbHostFuncBulkDriver::RecvBulk(PBYTE  pData, UINT uCount)
{
	Trace(debugData, "RecvBulk(Count=%d)", uCount);

	return m_pRecv->RecvBulk(pData, uCount, false);
	}

BOOL CUsbHostFuncBulkDriver::SendBulk(PCBYTE pData, UINT uCount, BOOL fAsync)
{
	Trace(debugData, "SendBulk(Count=%d, Async=%d)", uCount, fAsync);

	return m_pSend->SendBulk(PBYTE(pData), uCount, fAsync);
	}

UINT CUsbHostFuncBulkDriver::RecvBulk(PBYTE  pData, UINT uCount, BOOL fAsync)
{
	Trace(debugData, "RecvBulk(Count=%d, Async=%d)", uCount, fAsync);

	return m_pRecv->RecvBulk(pData, uCount, fAsync);
	}

BOOL CUsbHostFuncBulkDriver::SendBulk(CBuffer *pBuff)
{
	Trace(debugData, "SendBulk(Buffer=%d)", pBuff->GetSize());

	return m_pSend->SendBulk(pBuff->GetData(), pBuff->GetSize(), false);
	}

BOOL CUsbHostFuncBulkDriver::METHOD RecvBulk(CBuffer *&pBuff)
{
	Trace(debugData, "RecvBulk() - Not Supported");

	return FALSE;
	}

UINT CUsbHostFuncBulkDriver::WaitAsyncSend(UINT uTimeout)
{
	Trace(debugData, "WaitAsyncSend(Timeout=%d)", uTimeout);

	BOOL fOk;

	UINT uCount;
	
	if( m_pSend->WaitAsync(uTimeout, fOk, uCount) ) {
		
		return fOk;
		}

	return NOTHING;
	}

UINT CUsbHostFuncBulkDriver::WaitAsyncRecv(UINT uTimeout)
{
	Trace(debugData, "WaitAsyncRecv(Timeout=%d)", uTimeout);

	BOOL fOk;

	UINT uCount;
	
	if( m_pRecv->WaitAsync(uTimeout, fOk, uCount) ) {
		
		return fOk ? uCount : 0;
		}

	return NOTHING;
	}

BOOL CUsbHostFuncBulkDriver::KillAsyncSend(void)
{
	return m_pSend->KillAsync();
	}

BOOL CUsbHostFuncBulkDriver::KillAsyncRecv(void)
{
	return m_pRecv->KillAsync();
	}

// Implementation

BOOL CUsbHostFuncBulkDriver::FindPipes(CUsbDescList const &List)
{
	UINT iIndex;

	UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.FindInterface(iIndex, m_iInt);

	if( pInt && pInt->m_bEndpoints == 2 ) {

		UsbEndpointDesc *pEp0 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		UsbEndpointDesc *pEp1 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		if( pEp0 && pEp1 ) {

			m_pCtrl = m_pDev->GetCtrlPipe();

			if( pEp0->m_bDirIn && !pEp1->m_bDirIn ) {

				m_pRecv = m_pDev->GetPipe(pEp0->m_bAddr, true);

				m_pSend = m_pDev->GetPipe(pEp1->m_bAddr, false);

				m_iRecv = pEp0->m_bAddr | Bit(7);

				m_iSend = pEp1->m_bAddr;
				}
			
			if( !pEp0->m_bDirIn && pEp1->m_bDirIn ) {

				m_pSend = m_pDev->GetPipe(pEp0->m_bAddr, false);

				m_pRecv = m_pDev->GetPipe(pEp1->m_bAddr, true);

				m_iSend = pEp0->m_bAddr;

				m_iRecv = pEp1->m_bAddr | Bit(7);
				}

			Trace(debugCmds, "IN  Endpoint 0x%2.2X", m_iRecv);
	
			Trace(debugCmds, "OUT Endpoint 0x%2.2X", m_iSend);
			
			return m_pCtrl != NULL && m_pSend != NULL && m_pRecv != NULL;
			}
		}

	return false;
	}

// End of File
