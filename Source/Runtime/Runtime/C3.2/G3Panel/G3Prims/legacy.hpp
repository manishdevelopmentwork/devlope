
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LEGACY_HPP
	
#define	INCLUDE_LEGACY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../g3comms/events.hpp"

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyScale;
class CPrimLegacyVertScale;
class CPrimLegacyHorzScale;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Scale
//

class CPrimLegacyScale : public CPrim
{
	public:
		// Constructor
		CPrimLegacyScale(void);

		// Destructor
		~CPrimLegacyScale(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);

		// Data Members
		CPrimColor   * m_pLine;
		CPrimBrush   * m_pBack;
		UINT	       m_Style;
		UINT	       m_Orient;
		UINT	       m_Major;
		UINT	       m_Minor;
		UINT	       m_Interval;
		CCodedItem   * m_pLimitMin;
		CCodedItem   * m_pLimitMax;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			C3INT	m_Minor;
			C3INT	m_Major;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Implementation
		int MulDivRound(int a, int b, int c);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Scale
//

class CPrimLegacyVertScale : public CPrimLegacyScale
{
	public:
		// Constructor
		CPrimLegacyVertScale(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horozontal Scale
//

class CPrimLegacyHorzScale : public CPrimLegacyScale
{
	public:
		// Constructor
		CPrimLegacyHorzScale(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyFigure;
class CPrimLegacyShadow;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Figure
//

class CPrimLegacyFigure : public CPrim
{
	public:
		// Constructor
		CPrimLegacyFigure(void);

		// Destructor
		~CPrimLegacyFigure(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CPrimPen   * m_pEdge;
		CPrimBrush * m_pBack;
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Shadow
//

class CPrimLegacyShadow : public CPrimLegacyFigure
{
	public:
		// Constructor
		CPrimLegacyShadow(void);

		// Destructor
		~CPrimLegacyShadow(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);

		// Data Members
		UINT m_Style;
		UINT m_Border;

	protected:
		// Implementation
		void FindPoints(P2 *t, P2 *b, R2 &r);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyCfImage;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy CF Image
//

class CPrimLegacyCfImage : public CPrimLegacyFigure
{
	public:
		// Constructor
		CPrimLegacyCfImage(void);

		// Destructor
		~CPrimLegacyCfImage(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);

		// Data Members
		CCodedItem * m_pValue;
		CCodedItem * m_pShow;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			UINT m_Value;
			UINT m_Show;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx  m_Ctx;
		PBYTE m_pImage;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Implementation
		BOOL LoadImage(C3INT Image);
		void ShowMessage(IGDI *pGDI, PCTXT pMsg);
	};

// End of File

#endif
