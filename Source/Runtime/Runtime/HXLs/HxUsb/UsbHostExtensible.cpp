
#include "Intern.hpp"

#include "UsbHostExtensible.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

///////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceDesc.hpp"

#include "UsbEndpointDesc.hpp"

#include "UsbHubDesc.hpp"

///////////////////////////////////////////////////////////////////////////
//
// Register Access
//

#define Caps(x)	   (m_pCaps[cap##x])

#define CapsEx(x)  (m_pCapsEx[cap##])

#define OpReg(x)   (m_pOperate[reg##x])

#define RtReg(x)   (m_pRuntime[reg##x])

//////////////////////////////////////////////////////////////////////////
//
// Usb Open Host Controller Interface
//

// Instantiator

IUsbHostInterfaceDriver * Create_UsbHostExtensible(void)
{
	CUsbHostExtensible *p = New CUsbHostExtensible;

	return p;
	}

// Constructor

CUsbHostExtensible::CUsbHostExtensible(void)
{
	m_pCaps       = NULL;

	m_pCapsEx     = NULL;
		      
	m_pOperate    = NULL;
		      
	m_pRuntime    = NULL;
		      
	m_pDoorbell   = NULL;
		      
	m_pDevCtxList = NULL;

	m_pDevObjList = NULL;
		      
	m_pCmdRing    = NULL;
		      
	m_pEventRing  = NULL;
		      
	m_pEventTable = NULL;
		      
	m_pCmdLimit   = NULL;

	m_pCmdEvent   = NULL;

	m_pScratchpad = NULL;

	m_pScratchBuf = NULL;

	m_pLock       = Create_Mutex();
	}

// Destructor

CUsbHostExtensible::~CUsbHostExtensible(void)
{
	FreeDeviceList();

	FreeCmdRing();

	FreeEventRing();

	FreeScratchpad();

	m_pLock->Release();
	}

// IUsbDriver

BOOL CUsbHostExtensible::Init(void)
{
	if( CUsbHostHardwareDriver::Init() ) {
		
		StopController();

		ResetController();

		MakeScratchpad();

		MakeDeviceList();

		MakeCmdRing();

		MakeEventRing();

		return TRUE;
		}

	return FALSE;
	}

BOOL CUsbHostExtensible::Start(void)
{
	if( CUsbHostHardwareDriver::Start() ) {
		
		InitDeviceList();

		InitCmdRing();

		InitEventRing();

		InitController();

		StartController();

		EnableEvents();

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CUsbHostExtensible::Stop(void)
{
	if( m_pLowerDrv && m_pUpperDrv ) {

		DisableEvents();
		
		StopController();

		return m_pLowerDrv->Stop();
		}

	return false;
	}
		
// IUsbHostInterfaceDriver

UINT CUsbHostExtensible::GetType(void)
{
	return usbXhci;	
	}

UINT CUsbHostExtensible::GetPortCount(void)
{
	return GetNumPorts();
	}

BOOL CUsbHostExtensible::GetPortConnect(UINT iPort)
{
	return PortGet(iPort, portConnect);
	}

BOOL CUsbHostExtensible::GetPortEnabled(UINT iPort)
{
	return PortGet(iPort, portEnabled);
	}

BOOL CUsbHostExtensible::GetPortCurrent(UINT iPort)
{
	return PortGet(iPort, portOverCurrent);
	}

BOOL CUsbHostExtensible::GetPortSuspend(UINT iPort)
{
	return (PortGet(iPort, (0xF << portLinkStatus)) >> portLinkStatus) == 3;
	}

BOOL CUsbHostExtensible::GetPortReset(UINT iPort)
{
	return PortGet(iPort, portReset);
	}

BOOL CUsbHostExtensible::GetPortPower(UINT iPort)
{
	return PortGet(iPort, portPower);
	}

UINT CUsbHostExtensible::GetPortSpeed(UINT iPort)
{
	DWORD Data = PortGet(iPort, (0xF << portSpeed)) >> portSpeed;

	switch( Data ) {

		case speedFull: return usbSpeedFull;
		case speedLow:	return usbSpeedLow;
		case speedHigh: return usbSpeedHigh;
		}

	return usbSpeedHigh;	
	}

void CUsbHostExtensible::SetPortEnable(UINT iPort, BOOL fSet)
{
	if( !fSet ) {	

		PortSet(iPort, portEnabled);
		}
	}

void CUsbHostExtensible::SetPortSuspend(UINT iPort, BOOL fSet)
{
	DWORD Data = PortGet(iPort, portPower | (3 << portLeds));

	Data |= (portLinkStrobe | (3 << portLinkStatus));

	PortSet(iPort, Data);
	}

void CUsbHostExtensible::SetPortResume(UINT iPort, BOOL fSet)
{
	DWORD Data = PortGet(iPort, portPower | (3 << portLeds));

	Data |= (portLinkStrobe | (15 << portLinkStatus));

	PortSet(iPort, Data);
	}

void CUsbHostExtensible::SetPortReset(UINT iPort, BOOL fSet)
{
	if( fSet ) {	

		PortSet(iPort, portReset);
		}
	}

void CUsbHostExtensible::SetPortPower(UINT iPort, BOOL fSet)
{
	if( fSet ) {

		PortSet(iPort, portPower);

		while( !PortGet(iPort, portPower) );
		}
	else {
		PortClr(iPort, portPower);

		while( PortGet(iPort, portPower) );
		}
	}

void CUsbHostExtensible::EnableEvents(BOOL fEnable)
{
	if( fEnable ) {

		EnableEvents();
		}
	else {
		DisableEvents();
		}
	}

BOOL CUsbHostExtensible::SetEndptDevAddr(DWORD iEndpt, BYTE bAddr)
{
	return TRUE;
	}

UINT CUsbHostExtensible::GetEndptDevAddr(DWORD iEndpt)
{
	UINT iDev = HIWORD(iEndpt);

	if( iDev < GetMaxSlots() ) {

		CDevice *pDev = m_pDevObjList[iDev];

		if( pDev ) {

			CSlotCtx *pSlot = (CSlotCtx *) GetContext(pDev->m_pOutputCtx, 0);
			
			return pSlot->m_dwDevAddr;
			}
		}

	return NOTHING;
	}

BOOL CUsbHostExtensible::SetEndptAddr(DWORD iEndpt, BYTE bAddr)
{
	return TRUE;
	}

BOOL CUsbHostExtensible::SetEndptMax(DWORD iEndptIdx, WORD wMaxPacket)
{
	UINT iSlot  = HIWORD(iEndptIdx);

	UINT iEndpt = LOWORD(iEndptIdx); 

	CDevice *pDevObj = m_pDevObjList[iSlot];

	if( pDevObj ) {

		CControlCtx *pCtrlCtx    = (CControlCtx *) GetContext(pDevObj->m_pInputCtx, 0);

		CEndptCtx   *pEndptCtx   = (CEndptCtx   *) GetContext(pDevObj->m_pInputCtx, iEndpt + 2);

		pCtrlCtx->m_dwAddCtx     = Bit(iEndpt + 1);

		pEndptCtx->m_dwMaxPacket = wMaxPacket;

		return CmdEvalContext(iSlot, PCDWORD(pDevObj->m_pInputCtx));
		}

	return false;
	}

BOOL CUsbHostExtensible::SetEndptHub(DWORD iIndex, BYTE bHub, BYTE bPort)
{
	return TRUE;
	}

DWORD CUsbHostExtensible::MakeDevice(IUsbDevice *pDevice)
{
	UINT iSlot = CmdEnableSlot();

	if( iSlot != NOTHING ) {

		CDevice *pDevObj     = MakeDeviceObj();

		pDevObj->m_pDevice   = pDevice;

		m_pDevObjList[iSlot] = pDevObj;

		SetDeviceCtxItem(iSlot, pDevObj->m_pOutputCtx);

		return iSlot;
		}

	return NOTHING;
	}

void CUsbHostExtensible::KillDevice(DWORD iSlot)
{
	if( iSlot != NOTHING ) {

		if( CmdDisableSlot(iSlot) ) {

			FreeDeviceObj(m_pDevObjList[iSlot]);

			m_pDevObjList[iSlot] = NULL;

			SetDeviceCtxItem(iSlot, NULL);
			}
		}
	}

BOOL CUsbHostExtensible::CheckConfig(DWORD iSlot)
{
	if( iSlot != NOTHING ) {

		CDevice *pDevObj = m_pDevObjList[iSlot];

		if( pDevObj ) {

			IUsbDevice  *pDevInt  = pDevObj->m_pDevice;

			CControlCtx *pCtrlCtx = (CControlCtx *) GetContext(pDevObj->m_pInputCtx, 0);

			CSlotCtx    *pSlotCtx = (CSlotCtx    *) GetContext(pDevObj->m_pInputCtx, 1);

			CheckHubConfig(iSlot);

			CheckDevConfig(iSlot);

			return CmdConfigEndpoint(iSlot, PCDWORD(pCtrlCtx));
			}
		}

	return false;
	}

DWORD CUsbHostExtensible::MakeEndptCtrl(DWORD iSlot, UINT uSpeed)
{
	/*AfxTrace("CUsbHostExtensible::MakeEndptCtrl(iSlot=%d, Speed=%d)\n", iSlot, uSpeed);*/

	if( iSlot != NOTHING ) {

		CDevice *pDevObj = m_pDevObjList[iSlot];

		if( pDevObj ) {

			CControlCtx *pCtrlCtx  = (CControlCtx *) GetContext(pDevObj->m_pInputCtx, 0);

			CSlotCtx    *pSlotCtx  = (CSlotCtx    *) GetContext(pDevObj->m_pInputCtx, 1);

			CEndptCtx   *pEndptCtx = (CEndptCtx   *) GetContext(pDevObj->m_pInputCtx, 2);

			IUsbDevice  *pDevUsb   = pDevObj->m_pDevice; 

			if( pEndptCtx->m_dwState == endptInvalid ) {

				CEndpoint *pEndptObj        = MakeEndpointObj(); 

				pCtrlCtx->m_dwAddCtx        = Bit(0) | Bit(1);

				pSlotCtx->m_dwRouteStr	    = pDevUsb->GetTreePath().dw >> 12;
				pSlotCtx->m_dwSpeed	    = (uSpeed == usbSpeedHigh ? speedHigh : uSpeed == usbSpeedFull ? speedFull : speedLow);
				pSlotCtx->m_dwMulti	    = 0;
				pSlotCtx->m_dwHub	    = 0;
				pSlotCtx->m_dwContexts	    = 1;
				pSlotCtx->m_dwLatency	    = 0;
				pSlotCtx->m_dwRootHubPort   = pDevUsb->GetTreePath().a.dwPort1;
				pSlotCtx->m_dwPortCount	    = 0;
				pSlotCtx->m_dwHubSlot	    = 0;
				pSlotCtx->m_dwHubPort       = 0;
				pSlotCtx->m_dwThinkTime	    = 0;
				pSlotCtx->m_dwInterrupter   = 0;
				pSlotCtx->m_dwDevAddr	    = 0;
				pSlotCtx->m_dwState	    = 0;

				pEndptObj->m_iIndex         = 0;
				pEndptObj->m_pContext       = (CEndptCtx *) GetContext(pDevObj->m_pOutputCtx, 1);
				pEndptCtx->m_dwState	    = 0;
				pEndptCtx->m_dwMult	    = 0;
				pEndptCtx->m_dwMaxPrimStms  = 0;
				pEndptCtx->m_dwLinearStms   = 0;
				pEndptCtx->m_dwInterval	    = 0;
				pEndptCtx->m_dwServiceTime  = 0;
				pEndptCtx->m_dwErrorCount   = 3;
				pEndptCtx->m_dwType	    = endptControl;
				pEndptCtx->m_dwHostDisable  = 0;
				pEndptCtx->m_dwMaxBusrt	    = 0;
				pEndptCtx->m_dwMaxPacket    = (uSpeed == usbSpeedHigh) ? 64 : 8;
				pEndptCtx->m_dwDequeueState = true;
				pEndptCtx->m_dwDequeuePtrLo = VirtualToPhysical(pEndptObj->m_pTrbRing) >> 4;
				pEndptCtx->m_dwDequeuePtrHi = 0;
				pEndptCtx->m_dwAvgTrbLen    = 8;
				pEndptCtx->m_dwMaxEsit	    = 0;

				CheckHubConfig(iSlot);

				DumpContext(*pSlotCtx, iSlot);

				DumpContext(*pEndptCtx, 0);

				if( CmdAddressDevice(iSlot, PCDWORD(pDevObj->m_pInputCtx)) ) {

					pDevObj->m_pEndptObj[0] = pEndptObj;

					return MAKELONG(0, iSlot);
					}

				FreeEndpointObj(pEndptObj);
				}
			}
		}

	return NOTHING;	
	}

DWORD CUsbHostExtensible::MakeEndptAsync(DWORD iSlot, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	/*AfxTrace("CUsbHostExtensible::MakeEndptAsync(iSlot=0x%8.8X, Addr=%d, Index=%d)\n", iSlot, Desc.m_bAddr, AddrToIndex(Desc.m_bAddr, Desc.m_bDirIn));*/

	if( iSlot != NOTHING ) {

		CDevice *pDevObj = m_pDevObjList[iSlot];

		if( pDevObj ) {

			UINT       iEndptObj = AddrToIndex(Desc.m_bAddr, Desc.m_bDirIn);

			CEndptCtx *pEndptCtx = (CEndptCtx *) GetContext(pDevObj->m_pInputCtx, iEndptObj + 2);

			if( pEndptCtx->m_dwState == endptInvalid ) {

				CEndpoint *pEndptObj        = MakeEndpointObj(); 

				pEndptObj->m_iIndex         = iEndptObj;
				pEndptObj->m_pContext       = (CEndptCtx *) GetContext(pDevObj->m_pOutputCtx, iEndptObj + 1);
				pEndptCtx->m_dwState	    = 0;
				pEndptCtx->m_dwMult	    = 0;
				pEndptCtx->m_dwMaxPrimStms  = 0;
				pEndptCtx->m_dwLinearStms   = 0;
				pEndptCtx->m_dwInterval	    = Desc.m_bInterval;
				pEndptCtx->m_dwServiceTime  = 0;
				pEndptCtx->m_dwErrorCount   = 3;
				pEndptCtx->m_dwType	    = Desc.m_bDirIn ? endptBulkIn : endptBulkOut;
				pEndptCtx->m_dwHostDisable  = 0;
				pEndptCtx->m_dwMaxBusrt	    = (Desc.m_wMaxPacket >> 11) & 0x3;
				pEndptCtx->m_dwMaxPacket    = Desc.m_wMaxPacket & 0x7FF;
				pEndptCtx->m_dwDequeueState = true;
				pEndptCtx->m_dwDequeuePtrLo = VirtualToPhysical(pEndptObj->m_pTrbRing) >> 4;
				pEndptCtx->m_dwDequeuePtrHi = 0;
				pEndptCtx->m_dwAvgTrbLen    = pEndptCtx->m_dwMaxPacket;
				pEndptCtx->m_dwMaxEsit	    = pEndptCtx->m_dwMaxPacket * (pEndptCtx->m_dwMaxBusrt + 1);

				pDevObj->m_pEndptObj[iEndptObj] = pEndptObj;

				DumpContext(*pEndptCtx, iEndptObj);

				return MAKELONG(iEndptObj, iSlot);
				}
			}
		}

	return NOTHING;	
	}

DWORD CUsbHostExtensible::MakeEndptInt(DWORD iSlot, UINT uSpeed, UsbEndpointDesc const &Desc)
{
	/*AfxTrace("CUsbHostExtensible::MakeEndptInt(iSlot=0x%8.8X, Addr=%d, Index=%d)\n", iSlot, Desc.m_bAddr, AddrToIndex(Desc.m_bAddr, Desc.m_bDirIn));*/

	if( iSlot != NOTHING ) {

		CDevice *pDevObj = m_pDevObjList[iSlot];

		if( pDevObj ) {

			UINT       iEndptObj = AddrToIndex(Desc.m_bAddr, Desc.m_bDirIn);

			CEndptCtx *pEndptCtx = (CEndptCtx *) GetContext(pDevObj->m_pInputCtx, iEndptObj + 2);

			if( pEndptCtx->m_dwState == endptInvalid ) {

				CEndpoint *pEndptObj        = MakeEndpointObj(); 

				pEndptObj->m_iIndex         = iEndptObj;
				pEndptObj->m_pContext       = (CEndptCtx *) GetContext(pDevObj->m_pOutputCtx, iEndptObj + 1);
				pEndptCtx->m_dwState	    = 0;
				pEndptCtx->m_dwMult	    = 0;
				pEndptCtx->m_dwMaxPrimStms  = 0;
				pEndptCtx->m_dwLinearStms   = 0;
				pEndptCtx->m_dwInterval	    = FindEndpointInterval(Desc, uSpeed);
				pEndptCtx->m_dwServiceTime  = 0;
				pEndptCtx->m_dwErrorCount   = 3;
				pEndptCtx->m_dwType	    = Desc.m_bDirIn ? endptIntIn : endptIntOut;
				pEndptCtx->m_dwHostDisable  = 0;
				pEndptCtx->m_dwMaxBusrt	    = (Desc.m_wMaxPacket >> 11) & 0x3;
				pEndptCtx->m_dwMaxPacket    = Desc.m_wMaxPacket & 0x7FF;
				pEndptCtx->m_dwDequeueState = true;
				pEndptCtx->m_dwDequeuePtrLo = VirtualToPhysical(pEndptObj->m_pTrbRing) >> 4;
				pEndptCtx->m_dwDequeuePtrHi = 0;
				pEndptCtx->m_dwAvgTrbLen    = pEndptCtx->m_dwMaxPacket;
				pEndptCtx->m_dwMaxEsit	    = pEndptCtx->m_dwMaxPacket * (pEndptCtx->m_dwMaxBusrt + 1);

				pDevObj->m_pEndptObj[iEndptObj] = pEndptObj;

				DumpContext(*pEndptCtx, iEndptObj);

				return MAKELONG(iEndptObj, iSlot);
				}
			}
		}

	return NOTHING;
	}

BOOL CUsbHostExtensible::ResetEndpt(DWORD iIndex)
{
	if( iIndex != NOTHING ) {

		UINT iSlot  = HIWORD(iIndex);

		UINT iEndpt = LOWORD(iIndex);

		return CmdResetEndpoint(iSlot, iEndpt + 1, false);
		}

	return false;
	}

BOOL CUsbHostExtensible::KillEndpt(DWORD iIndex)
{
	if( iIndex != NOTHING ) {

		UINT iSlot  = HIWORD(iIndex);

		UINT iEndpt = LOWORD(iIndex);

		if( iEndpt ) {

			CDevice *pDev = m_pDevObjList[iSlot];

			if( pDev ) {

				CEndpoint *pEndpt = pDev->m_pEndptObj[iEndpt];

				if( pEndpt ) {

					if( pEndpt->m_pContext->m_dwState == stateRunning ) {

						CmdStopEndpoint(iSlot, iEndpt + 1);
						}
			
					AbortTransfers(pEndpt);

					FreeEndpointObj(pEndpt);

					pDev->m_pEndptObj[iEndpt] = NULL;
										
					return true;
					}
				}
			}
		}

	return false;
	}

BOOL CUsbHostExtensible::Transfer(UsbIor &Urb)
{
	/*AfxTrace("CUsbHostExtensible::Transfer(Urb=0x%8.8X, iEndpt=0x%8.8X, Flags=0x%8.8X, Count=%d)\n", &Urb, Urb.m_iEndpt, Urb.m_uFlags, Urb.m_uCount);*/

	UINT iSlot  = HIWORD(Urb.m_iEndpt);

	UINT iEndpt = LOWORD(Urb.m_iEndpt); 
	
	if( iSlot >= GetMaxSlots() ) {

		/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Slot Identifier\n");*/
		
		return false;
		}

	if( iEndpt >= constMaxEndpoints ) {

		/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Endpoint Identifier\n");*/
		
		return false;
		}

	if( Urb.m_uCount > constMaxEndpointData ) {

		/*AfxTrace("CUsbHostExtensible::Transfer - Data Limit Error\n");*/
		
		return false;
		}

	CDevice *pDev = m_pDevObjList[iSlot];
	
	if( pDev ) {

		CSlotCtx  *pSlotCtx  = (CSlotCtx  *) GetContext(pDev->m_pOutputCtx, 0);

		CEndptCtx *pEndptCtx = (CEndptCtx *) GetContext(pDev->m_pOutputCtx, iEndpt + 1);

		CEndpoint *pEndptObj = pDev->m_pEndptObj[iEndpt];

		if( pEndptObj ) {

			if( pSlotCtx->m_dwState == slotDisabled ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Slot State : %d\n", pSlotCtx->m_dwState);*/

				return false;
				}

			if( iEndpt > 0 && pSlotCtx->m_dwState != slotConfigured ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Slot State : %d\n", pSlotCtx->m_dwState);*/

				return false;
				}

			if( pEndptCtx->m_dwType == endptInvalid ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Slot Endpoint Type : %d\n", pEndptCtx->m_dwType);*/

				return false;
				}

			if( pEndptCtx->m_dwState == stateDisabled || pEndptCtx->m_dwState == stateError ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Endpoint State : %d\n", pEndptCtx->m_dwState);*/

				return false;
				}

			if( pEndptCtx->m_dwState == stateStopped && pEndptCtx->m_dwType != endptControl ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Endpoint State : %d\n", pEndptCtx->m_dwState);*/

				return false;
				}

			if( pEndptCtx->m_dwState == stateHalted && pEndptCtx->m_dwType != endptControl ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Endpoint State : %d\n", pEndptCtx->m_dwState);*/

				return false;
				}

			if( pEndptCtx->m_dwType != endptControl && (Urb.m_uFlags & UsbIor::flagControl) ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Endpoint Type : %d\n", pEndptCtx->m_dwType);*/

				return false;
				}

			if( pEndptCtx->m_dwType == endptControl && !(Urb.m_uFlags & UsbIor::flagControl) ) {

				/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Endpoint Type : %d\n", pEndptCtx->m_dwType);*/

				return false;
				}

			UINT iTrb = AllocTransfer(pEndptObj);

			if( iTrb != NOTHING ) {

				CTrb &Trb = pEndptObj->m_pTrbRing[iTrb];

				bool fNotify = (Urb.m_uFlags & Urb.flagNotify) ? true : false;

				bool fQueue  = false;

				pEndptObj->m_pUrbRing[iTrb] = NULL;

				pEndptObj->m_pUrbData[iTrb] = NULL;

				switch( pEndptCtx->m_dwType ) {

					case endptControl:

						if( Urb.m_uFlags & Urb.flagSetup ) {

							Trb.m_dwParam0 = PDWORD(Urb.m_pData)[0];
							Trb.m_dwParam1 = PDWORD(Urb.m_pData)[1];
							Trb.m_dwStatus = 8;
							Trb.m_dwType   = trbSetup;
							Trb.m_dwFlags |= flagImmediate;
							fQueue         = true;

							if( Urb.m_uFlags & Urb.flagNoDataStage ) {

								Trb.m_dwControl = setupNoDataStage;
								}
						
							else if( Urb.m_uFlags & Urb.flagInDataStage ) {

								Trb.m_dwControl = setupInData;
								}

							else if( Urb.m_uFlags & Urb.flagOutDataStage ) {

								Trb.m_dwControl = setupOutData;
								}

							break;
							}

						if( Urb.m_uFlags & Urb.flagStatusStage ) {

							Trb.m_dwParam0  = 0;
							Trb.m_dwParam1  = 0;
							Trb.m_dwStatus  = 0;
							Trb.m_dwType	= trbStatus;
							Trb.m_dwControl	= (Urb.m_uFlags & Urb.flagOut) ? 0 : 1;
							fQueue          = false;

							break;
							}

						if( Urb.m_uFlags & (Urb.flagOut | Urb.flagIn) ) {

							Trb.m_dwParam0  = VirtualToPhysical(PVOID(pEndptObj->m_pData));
							Trb.m_dwParam1  = 0;
							Trb.m_dwStatus  = Urb.m_uCount;
							Trb.m_dwType	= trbData;
							Trb.m_dwControl	= (Urb.m_uFlags & Urb.flagOut) ? 0 : 1;
							fQueue          = true;

							if( Urb.m_uFlags & Urb.flagOut ) {

								memcpy(PBYTE(pEndptObj->m_pData), PBYTE(Urb.m_pData), Urb.m_uCount);
								}

							break;
							}

						break;

					case endptBulkOut:
					case endptBulkIn:
					case endptIntOut:
					case endptIntIn:

						Trb.m_dwParam0  = VirtualToPhysical(PVOID(pEndptObj->m_pData));
						Trb.m_dwParam1  = 0;
						Trb.m_dwStatus  = Urb.m_uCount;
						Trb.m_dwType	= trbNormal;
						Trb.m_dwControl	= 0;
						fQueue          = false;

						if( Urb.m_uFlags & Urb.flagIn ) {

							Trb.m_dwFlags |= flagIoShort;
							}

						if( Urb.m_uFlags & Urb.flagOut ) {

							memcpy(PBYTE(pEndptObj->m_pData), PBYTE(Urb.m_pData), Urb.m_uCount);
							}

						break;
					}

				if( fQueue ) {

					QueueTransfer(pEndptObj, iSlot);

					if( Urb.m_uFlags & Urb.flagIn ) {

						pEndptObj->m_pUrbData[iTrb] = Urb.m_pData;
						}

					if( fNotify ) {

						Urb.m_uStatus = Urb.statPassed;

						m_pUpperDrv->OnTransfer(Urb);
						}

					return true;
					}

				Trb.m_dwFlags |= flagIoComp;

				Urb.m_uStatus  = Urb.statActive;

				if( fNotify ) {

					pEndptObj->m_pUrbRing[iTrb] = &Urb;
					}

				return ExecTransfer(pEndptObj, iSlot);
				}

			/*AfxTrace("CUsbHostExtensible::Transfer - Allocation Failed.\n");*/
			
			return false;
			}
		
		/*AfxTrace("CUsbHostExtensible::Transfer - Null Endpoint Object.\n");*/
		
		return false;
		}

	/*AfxTrace("CUsbHostExtensible::Transfer - Invalid Device or Endpoint\n");*/

	return false;
	}

BOOL CUsbHostExtensible::AbortPending(DWORD iIndex)
{
	/*AfxTrace("CUsbHostExtensible::AbortPending(Endpoint=0x%8.8X)\n", iIndex);*/

	if( iIndex != NOTHING ) {

		UINT iSlot  = HIWORD(iIndex);

		UINT iEndpt = LOWORD(iIndex);

		CDevice *pDev = m_pDevObjList[iSlot];

		if( pDev ) {

			CEndpoint *pEndpt = pDev->m_pEndptObj[iEndpt];

			if( pEndpt ) {

				if( pEndpt->m_pContext->m_dwState == stateRunning ) {

					CmdStopEndpoint(iSlot, iEndpt + 1);
					}

				AbortTransfers(pEndpt);

				InitEndpointObjRing(pEndpt);

				CmdSetDeqeuePtr(iSlot, iEndpt + 1, 0, VirtualToPhysical(pEndpt->m_pTrbRing));
				
				CControlCtx *pCtrlCtx = (CControlCtx *) GetContext(pDev->m_pInputCtx, 0);
							
				pCtrlCtx->m_dwAddCtx  = Bit(iEndpt + 1);

				pCtrlCtx->m_dwDropCtx = Bit(iEndpt + 1); 
					
				CmdConfigEndpoint(iSlot, PCDWORD(pCtrlCtx));

				return true;
				}
			}
		}

	return false;
	}

UINT CUsbHostExtensible::AllocAddress(void)
{
	return NOTHING;	
	}

void CUsbHostExtensible::FreeAddress(UINT uAddr)
{
	}

// IUsbEvents

void CUsbHostExtensible::OnBind(IUsbDriver *pDriver)
{
	m_pLowerDrv = (IUsbHostHardwareDriver *) pDriver;

	m_pCaps     = PVDWORD(m_pLowerDrv->GetBaseAddr());

	m_pCapsEx   = PVDWORD(m_pLowerDrv->GetBaseAddr()) + HIWORD(Caps(HccParams1));
		
	m_pOperate  = PVDWORD(m_pLowerDrv->GetBaseAddr() + GetCapsLen());

	m_pRuntime  = PVDWORD(m_pLowerDrv->GetBaseAddr() + Caps(Runtime));

	m_pDoorbell = PDWORD (m_pLowerDrv->GetBaseAddr() + Caps(Doorbell));
	}

// IUsbHostHardwareEvents

void CUsbHostExtensible::OnEvent(void)
{
	for(;;) {

		DWORD Data = OpReg(UsbSts);

		if( Data ) {

			if( Data & statEvent ) {

				OpReg(UsbSts)  = statEvent;

				RtReg(IntMan) |= intPending;
	
				OnCoreEvent();
				}

			if( Data & statPort ) {

				OpReg(UsbSts) = statPort;
				}

			continue;
			}

		break;
		}
	}

// Event Handlers

void CUsbHostExtensible::OnCoreEvent(void)
{
	for(;;) {

		CTrb volatile &Trb = m_pEventRing[m_iEventHead];

		if( m_fEventCycle == !!(Trb.m_dwFlags & flagCycle) ) {

			switch( Trb.m_dwType ) {

				case trbTransfer:

					OnTransferEvent((CTrb const &) Trb);

					break;

				case trbCmdDone:

					OnCommandEvent((CTrb const &) Trb);

					break;

				case trbPortStatus:

					OnPortStatusEvent((CTrb const &) Trb);

					break;

				case trbHostCtrl:

					OnHostEvent((CTrb const &) Trb);

					break;
					
				case trbBandwidthReq:
				case trbDoorbell:
				case trbDevNotify:
				case trbIndexWrap:
					
					break;
				}

			if( ++m_iEventHead == constEventRingSize ) {
				
				m_iEventHead  = 0;

				m_fEventCycle = !m_fEventCycle;
				}

			continue;
			}

		RtReg(IntDequeueLo) = VirtualToPhysical(PVOID(&Trb)) | eventBusy;

		RtReg(IntDequeueHi) = 0x00000000;

		break;
		}
	}

void CUsbHostExtensible::OnTransferEvent(CTrb const &Trb)
{
	UINT iSlot  = HIBYTE(Trb.m_dwControl);

	UINT iEndpt = LOBYTE(Trb.m_dwControl);

	CompTransfer(m_pDevObjList[iSlot]->m_pEndptObj[iEndpt-1], Trb);
	}

void CUsbHostExtensible::OnCommandEvent(CTrb const &Trb)
{
	m_pCmdLimit->Signal(1);

	m_pCmdComp = (CCmdEventTrb *) &Trb;

	m_pCmdEvent->Set();
	}

void CUsbHostExtensible::OnPortStatusEvent(CTrb const &Trb)
{
	DWORD dwMask = portConnectChange | portResetChange | portEnableChange | portOverCurrentChange | portEnabled;

	UINT uPortId = Trb.m_dwParam0 >> 24;

	UINT uCode   = Trb.m_dwStatus >> 24;

	DWORD volatile &PortSc = m_pOperate[regPortSC + 4 * (uPortId - 1)];

	if( uCode == codeSuccess ) {

		DWORD Data = PortSc;

		if( Data & portConnectChange ) {

			PortSc = (Data & ~dwMask) | portConnectChange;

			OnPortConnect(uPortId, Data & portConnect);
			}

		if( Data & portResetChange ) {

			PortSc = (Data & ~dwMask) | portResetChange;

			OnPortReset(uPortId, Data & portReset);
			}

		if( Data & portEnableChange ) {

			PortSc = (Data & ~dwMask) | portEnableChange;

			OnPortEnable(uPortId, Data & portEnabled);
			}

		if( Data & portOverCurrentChange ) {

			PortSc = (Data & ~dwMask) | portOverCurrentChange;

			OnPortCurrent(uPortId, Data & portOverCurrent);
			}
		}
	}

void CUsbHostExtensible::OnHostEvent(CTrb const &Trb)
{
	/*AfxTrace("CUsbHostExtensible::OnHostEvent Code:%d\n", Trb.m_dwStatus >> 24);*/
	}

void CUsbHostExtensible::OnPortConnect(UINT nPort, bool fConnect)
{
	UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };

	if( fConnect ) {

		m_pUpperDrv->OnPortConnect(Path);
		}
	else {
		m_pUpperDrv->OnPortRemoval(Path);
		}
	}

void CUsbHostExtensible::OnPortReset(UINT nPort, bool fReset)
{
	if( !fReset ) {

		UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };
	
		m_pUpperDrv->OnPortReset(Path);
		}
	}

void CUsbHostExtensible::OnPortEnable(UINT nPort, bool fEnable)
{
	UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };
	
	m_pUpperDrv->OnPortEnable(Path);
	}

void CUsbHostExtensible::OnPortCurrent(UINT nPort, bool fOverCurrent)
{
	UsbPortPath Path = { { nPort, m_iIndex, 0, 0, 0 } };
	
	m_pUpperDrv->OnPortCurrent(Path);
	}

// Capabilities

UINT CUsbHostExtensible::GetCapsLen(void) const
{
	return LOBYTE(LOWORD(Caps(CapLength)));
	}

PDWORD CUsbHostExtensible::EnumExtCaps(UINT &iEnum) const
{
	if( iEnum == 0 ) {

		iEnum = UINT(m_pCapsEx);

		return PDWORD(iEnum);
		}

	PDWORD pData = PDWORD(iEnum); 
	
	UINT   uNext = HIBYTE(LOWORD(*pData));

	if( uNext ) {

		pData += uNext;

		iEnum  = UINT(pData);

		return pData;
		}

	return NULL;
	}

PDWORD CUsbHostExtensible::GetExtCapsType(UINT uCode) const
{
	UINT i = 0;

	for(;;) {
		
		PDWORD p = EnumExtCaps(i);

		if( p ) {
			
			if( LOBYTE(LOWORD(p[0])) == uCode ) {

				return p;
				}

			continue;
			}

		return NULL;
		}
	}

UINT CUsbHostExtensible::GetVersion(void) const
{
	return HIWORD(Caps(CapLength));
	}

UINT CUsbHostExtensible::GetMaxSlots(void) const
{
	DWORD const Data = Caps(HcsParams1);

	return ((CSParams1 const &) Data).m_dwMaxSlots;
	}

UINT CUsbHostExtensible::GetNumPorts(void) const
{
	DWORD const Data = Caps(HcsParams1);

	return ((CSParams1 const &) Data).m_dwMaxPorts;
	}

bool CUsbHostExtensible::GetPowerCtrl(void) const
{
	return Caps(HccParams1) & Bit(3);
	}

bool CUsbHostExtensible::GetContext64(void) const
{
	return Caps(HccParams1) & Bit(2);
	}

UINT CUsbHostExtensible::GetPageSize(void) const
{
	return 1 << (OpReg(PageSize) + 11);
	}

UINT CUsbHostExtensible::GetScratchCount(void) const
{
	UINT uLo = (Caps(HcsParams2) >> 27) & 0x1F;

	UINT uHi = (Caps(HcsParams2) >> 21) & 0x1F;

	return (uHi << 5) | uLo;
	}

// Operations

void CUsbHostExtensible::InitAddress(void)
{
	m_Address.Init();
	}

void CUsbHostExtensible::InitController(void)
{
	OpReg(Config)       = GetMaxSlots();
			     
	OpReg(DevCtxLo)     = VirtualToPhysical(m_pDevCtxList);
			     
	OpReg(DevCtxHi)     = 0x00000000;
			    
	OpReg(CmdRingLo)    = VirtualToPhysical(m_pCmdRing) | (m_fCmdCycle ? ringCycleState : 0);
			    
	OpReg(CmdRingHi)    = 0x00000000;
			    
	RtReg(IntTableSize) = constEventRingSegments;

	RtReg(IntTableLo)   = VirtualToPhysical(m_pEventTable);

	RtReg(IntTableHi)   = 0x00000000;
			    
	RtReg(IntDequeueLo) = VirtualToPhysical(PVOID(m_pEventRing)) | eventBusy;

	RtReg(IntDequeueHi) = 0x00000000;

	RtReg(IntMod)       = 0x00000FA0;

	RtReg(IntMan)       = intEnable;
	}

void CUsbHostExtensible::ResetController(void)
{
	if( OpReg(UsbSts) & statHalted ) {

		OpReg(UsbCmd) |= cmdReset;

		while( OpReg(UsbCmd) & cmdReset );

		while( OpReg(UsbSts) & statNotReady );
		}
	}

void CUsbHostExtensible::StartController(void)
{
	OpReg(UsbCmd) |= cmdRunStop;

	while( OpReg(UsbSts) & statHalted );
	}

void CUsbHostExtensible::StopController(void)
{
	OpReg(UsbCmd) &= ~cmdRunStop;

	while( !(OpReg(UsbSts) & statHalted) );
	}

void CUsbHostExtensible::EnableEvents(void)
{
	OpReg(UsbCmd) |= cmdIntEnable;
	}

void CUsbHostExtensible::DisableEvents(void)
{
	OpReg(UsbCmd) &= ~cmdIntEnable;
	}

// Transfers

UINT CUsbHostExtensible::AllocTransfer(CEndpoint *pEndpt)
{
	CTrb *pTrb = &pEndpt->m_pTrbRing[pEndpt->m_iTrbTail];

	if( pTrb->m_dwType == trbLink ) {

		if( pEndpt->m_fTrbPCS ) {

			pTrb->m_dwFlags |= flagCycle;
			}
		else {
			pTrb->m_dwFlags &= ~flagCycle;
			}

		pEndpt->m_iTrbTail = 0;

		pEndpt->m_fTrbPCS  = !pEndpt->m_fTrbPCS;

		pTrb               = pEndpt->m_pTrbRing; 
		}

	if( pEndpt->m_iTrbHead != (pEndpt->m_iTrbTail + 1) % constTrbRingSize ) {

		pTrb->m_dwParam0  = 0;
			
		pTrb->m_dwParam1  = 0;
			
		pTrb->m_dwStatus  = 0;
			
		pTrb->m_dwType    = 0;
			
		pTrb->m_dwControl = 0;
			
		pTrb->m_dwFlags  &= flagCycle;

		return pEndpt->m_iTrbTail;
		}

	return NOTHING;
	}

bool CUsbHostExtensible::QueueTransfer(CEndpoint *pEndpt, UINT iSlot)
{
	CTrb &Trb = pEndpt->m_pTrbRing[pEndpt->m_iTrbTail++];

	if( pEndpt->m_fTrbPCS ) {

		Trb.m_dwFlags |= flagCycle;
		}
	else {
		Trb.m_dwFlags &= ~flagCycle;
		}

	return true;
	}

bool CUsbHostExtensible::ExecTransfer(CEndpoint *pEndpt, UINT iSlot)
{
	if( QueueTransfer(pEndpt, iSlot) ) {

		m_pDoorbell[iSlot] = doorbellControl + pEndpt->m_iIndex;

		return true;
		}

	return false;
	}

void CUsbHostExtensible::CompTransfer(CEndpoint *pEndpt, CTrb const &TrbEvent)
{
	UINT     uCode  = TrbEvent.m_dwStatus >> 24;

	UINT     uCount = TrbEvent.m_dwStatus & ~(0xFF << 24);

	UsbIor  *pUrb   = NULL;

	PVBYTE   pBuf   = NULL;

	while( pEndpt->m_iTrbHead != (pEndpt->m_iTrbTail + 1) % constTrbRingSize ) {

		CTrb *pTrb = &pEndpt->m_pTrbRing[pEndpt->m_iTrbHead];

		if( pTrb->m_dwType == trbLink ) {

			pEndpt->m_iTrbHead = 0;

			pEndpt->m_fTrbCCS  = !pEndpt->m_fTrbCCS;

			pTrb               = pEndpt->m_pTrbRing;
			}

		if( pEndpt->m_fTrbCCS == !!(pTrb->m_dwFlags & flagCycle) ) {

			pUrb = pEndpt->m_pUrbRing[pEndpt->m_iTrbHead];

			if( uCode == codeSuccess ) {

				PBYTE pBuff = PBYTE(pEndpt->m_pUrbData[pEndpt->m_iTrbHead]);

				if( pBuff ) {

					memcpy(pBuff, PBYTE(pEndpt->m_pData), pTrb->m_dwStatus);
					}
				}

			pEndpt->m_iTrbHead++;

			continue;
			}

		break;
		}

	if( pUrb ) {

		switch( uCode ) {
			
			case codeSuccess:
			case codeShort:

				pUrb->m_uStatus = UsbIor::statPassed;

				pUrb->m_uCount -= uCount;

				if( pUrb->m_uFlags & pUrb->flagIn ) {

					memcpy(PBYTE(pUrb->m_pData), PBYTE(pEndpt->m_pData), pUrb->m_uCount); 
					}

				break;

			case codeStall:

				pUrb->m_uStatus = UsbIor::statStall;

				AfxTrace("CUsbHostExtensible.CompTransfer Protocol Stall iEndpoint=%8.8X\n", pUrb->m_iEndpt);

				break;

			case codeTransact:
			case codeSplitTransact:

				AfxTrace("CUsbHostExtensible.CompTransfer Failed 0x%8.8X, Count=%d, iEndpoint=%8.8X\n", uCode, pUrb->m_uCount, pUrb->m_iEndpt);

				pUrb->m_uStatus = UsbIor::statHalted;

				break;

			case codeDevice:

				AfxTrace("CUsbHostExtensible.CompTransfer Failed 0x%8.8X\n", uCode);

				pUrb->m_uStatus = UsbIor::statFatal;

				break;

			case codeStopped:

				pUrb->m_uStatus = UsbIor::statAbort;

				break;

			case codeInvalid:
			case codeDataBuff:
			case codeBabble:
			case codeError:
			case codeResource:
			case codeBandwidth:
			case codeNoSlots:
			case codeBadStream:
			case codeSlotDisabled:
			case codeEndptDisabled:
			case codeRingUnderrun:
			case codeRingOverrun:
			case codeVFEventRingFull:
			case codeBadParam:
			case codeBandwidthOverrun:
			case codeContextState:
			case codeNoPing:
			case codeEventRingFull:
			case codeMissedService:
			case codeCmdRingStopped:
			case codeCmdAborted:
			case codeBadLength:
			case codeStoppedShortPacket:
			case codeBadMaxLatency:
			case codeIsochOverrun:
			case codeEventLost:
			case codeUndefined:
			case codeBadStreamId:
			case codeSecondaryBandwidth:
				
				// TODO -- Any special handling?

				/*AfxTrace("CUsbHostExtensible.CompTransfer Failed 0x%8.8X\n", uCode);*/

				pUrb->m_uStatus = UsbIor::statFailed;
			
				break;
			}

		m_pUpperDrv->OnTransfer(*pUrb);
		}
	}

void CUsbHostExtensible::AbortTransfers(CEndpoint *pEndpt)
{
	while( pEndpt->m_iTrbHead != pEndpt->m_iTrbTail ) {

		CTrb &Trb = pEndpt->m_pTrbRing[pEndpt->m_iTrbHead];

		if( Trb.m_dwType == trbLink ) {

			pEndpt->m_iTrbHead = 0;

			pEndpt->m_fTrbCCS  = !pEndpt->m_fTrbCCS;

			continue;
			}

		if( pEndpt->m_fTrbCCS == !!(Trb.m_dwFlags & flagCycle) ) {

			UsbIor *pUrb = pEndpt->m_pUrbRing[pEndpt->m_iTrbHead];

			if( pUrb ) {

				/*AfxTrace("CUsbHostExtensible::AbortTransfer\n");*/

				pUrb->m_uStatus = UsbIor::statAbort;

				m_pUpperDrv->OnTransfer(*pUrb);
				}

			pEndpt->m_iTrbHead++;

			continue;
			}

		break;
		}
	}

// Commands

bool CUsbHostExtensible::CmdNop(void)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType = trbNopCmd;
		
		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}
		
	m_pLock->Free();

	return false;
	}

UINT CUsbHostExtensible::CmdEnableSlot(void)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType = trbEnableSlot;

		if( ExecCmd() ) {

			UINT iSlot = m_pCmdComp->m_dwSlotId;

			m_pLock->Free();

			return iSlot;
			}
		}

	m_pLock->Free();

	return NOTHING;
	}

bool CUsbHostExtensible::CmdDisableSlot(UINT iSlot)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbDisableSlot;

		Trb.m_dwControl = iSlot << 8;

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdAddressDevice(UINT iSlot, PCDWORD pCtx)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbAddressDev;

		Trb.m_dwParam0  = VirtualToPhysical(pCtx);

		Trb.m_dwControl = iSlot << 8;

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdConfigEndpoint(UINT iSlot, PCDWORD pCtx)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbConfigEndpt;

		Trb.m_dwParam0  = VirtualToPhysical(pCtx);

		Trb.m_dwControl = iSlot << 8;

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdDeConfigEndpoint(UINT iSlot)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbAddressDev;

		Trb.m_dwControl = iSlot << 8;

		Trb.m_dwFlags  |= flagDeconfig;

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdEvalContext(UINT iSlot, PCDWORD pCtx)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbEvalContext;

		Trb.m_dwParam0  = VirtualToPhysical(pCtx);

		Trb.m_dwControl = iSlot << 8;

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdResetEndpoint(UINT iSlot, UINT iEndpt, bool fPreserve)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbResetEndpt;

		Trb.m_dwControl = iEndpt | (iSlot << 8);

		Trb.m_dwFlags  |= (fPreserve ? flagPreserve : 0);

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdStopEndpoint(UINT iSlot, UINT iEndpt)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbStopEndpt;

		Trb.m_dwControl = iEndpt | (iSlot << 8);

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdSetDeqeuePtr(UINT iSlot, UINT iEndpt, UINT iStm, DWORD dwPtr)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbSetDequePtr;

		Trb.m_dwParam0  = dwPtr;

		Trb.m_dwStatus  = iStm << 16;

		Trb.m_dwControl = iEndpt | (iSlot << 8);

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

bool CUsbHostExtensible::CmdResetDevice(UINT iSlot)
{
	m_pLock->Wait(FOREVER);
	
	UINT iCmd = AllocCmd();

	if( iCmd != NOTHING ) {

		CTrb &Trb = m_pCmdRing[iCmd];

		Trb.m_dwType    = trbResetDev;

		Trb.m_dwControl = iSlot << 8;

		if( ExecCmd() ) {

			m_pLock->Free();

			return true;
			}
		}

	m_pLock->Free();

	return false;
	}

// Command Ring

void CUsbHostExtensible::MakeCmdRing(void)
{
	m_pCmdRing = (CTrb *) AllocNonCached(sizeof(CTrb) * constCmdRingSize, 64); 

	m_pCmdEvent = Create_ManualEvent();
	
	m_pCmdLimit = Create_Semaphore();

	m_pCmdLimit->Signal(constCmdRingSize - constCmdRingLinks);
	}

void CUsbHostExtensible::InitCmdRing(void)
{
	memset(m_pCmdRing, 0, sizeof(CTrb) * constCmdRingSize);

	CTrb &Trb = m_pCmdRing[constCmdRingSize - 1];

	Trb.m_dwParam0 = VirtualToPhysical(m_pCmdRing);
	
	Trb.m_dwType   = trbLink;

	Trb.m_dwFlags  = flagToggle;

	m_iCmdHead     = 0;

	m_iCmdTail     = 0;

	m_fCmdCycle    = true;

	while( m_pCmdLimit->Wait(5) );

	m_pCmdLimit->Signal(constCmdRingSize - constCmdRingLinks);

	m_pCmdEvent->Set();
	}

void CUsbHostExtensible::FreeCmdRing(void)
{
	if( m_pCmdRing ) {

		FreeNonCached(m_pCmdRing);

		m_pCmdRing = NULL;
		}

	if( m_pCmdEvent ) {

		m_pCmdEvent->Release();

		m_pCmdEvent = NULL;
		}

	if( m_pCmdLimit ) {

		m_pCmdLimit->Release();

		m_pCmdLimit = NULL;
		}
	}

UINT CUsbHostExtensible::AllocCmd(void)
{
	if( m_pCmdLimit->Wait(FOREVER) ) {
		
		if( m_pLock->Wait(FOREVER) ) {

			CTrb *pTrb = &m_pCmdRing[m_iCmdTail];

			if( pTrb->m_dwType == trbLink ) {

				if( m_fCmdCycle ) {

					pTrb->m_dwFlags |= flagCycle;
					}
				else {
					pTrb->m_dwFlags &= ~flagCycle;
					}

				m_iCmdTail  = 0;

				m_fCmdCycle = !m_fCmdCycle;

				pTrb        = m_pCmdRing; 
				}

			pTrb->m_dwParam0  = 0;
			
			pTrb->m_dwParam1  = 0;
			
			pTrb->m_dwStatus  = 0;
			
			pTrb->m_dwType    = 0;
			
			pTrb->m_dwControl = 0;
			
			pTrb->m_dwFlags  &= flagCycle;

			UINT i = m_iCmdTail;

			m_pLock->Free();

			return i;
			}

		m_pCmdLimit->Signal(1);
		}
	
	return NOTHING;
	}

bool CUsbHostExtensible::ExecCmd(void)
{
	if( m_pCmdEvent->Wait(FOREVER) ) {

		CTrb &Trb = m_pCmdRing[m_iCmdTail++];
	
		if( m_fCmdCycle ) {

			Trb.m_dwFlags |= flagCycle;
			}
		else {
			Trb.m_dwFlags &= ~flagCycle;
			}
	
		m_pCmdEvent->Clear();

		m_pCmdComp = NULL;

		m_pDoorbell[0] = doorbellCommand;

		if( m_pCmdEvent->Wait(FOREVER) ) {

			if( m_pCmdComp->m_dwCode != codeSuccess ) {

				/*AfxTrace("Command Failed\n");*/

				DumpTrbData(*(CTrb *) m_pCmdComp, 0);
				}

			return m_pCmdComp->m_dwCode == codeSuccess;
			}
		}
	
	return false;
	}

// Event Ring

void CUsbHostExtensible::MakeEventRing(void)
{
	m_pEventRing  = (CTrb     *) AllocNonCachedInit(sizeof(CTrb) * constEventRingSize, 64); 

	m_pEventTable = (CSegment *) AllocNonCachedInit(sizeof(CSegment) * constEventRingSegments, 64);
	}

void CUsbHostExtensible::InitEventRing(void)
{
	memset(PVOID(m_pEventRing), 0, sizeof(CTrb) * constEventRingSize);

	memset(m_pEventTable, 0, sizeof(CSegment) * constEventRingSegments);

	CSegment &Seg  = m_pEventTable[0];

	m_iEventHead   = 0;
		         
	m_iEventTail   = 0;
		        
	m_fEventCycle  = true;
		        
	Seg.m_dwSize   = constEventRingSize;

	Seg.m_dwBaseLo = VirtualToPhysical(PVOID(m_pEventRing));

	Seg.m_dwBaseHi = 0x00000000;
	}

void CUsbHostExtensible::FreeEventRing(void)
{
	FreeNonCached(PVOID(m_pEventRing));
		
	FreeNonCached(m_pEventTable);

	m_pEventRing  = NULL;

	m_pEventTable = NULL;
	}

// Scratch Pad

void CUsbHostExtensible::MakeScratchpad(void)
{
	UINT uCount   = GetScratchCount();

	m_pScratchpad = PDWORD(AllocNonCachedInit(uCount * sizeof(DWORD) * 2, 64));

	m_pScratchBuf = AllocNonCachedInit(uCount * GetPageSize(), GetPageSize());

	for( UINT n = 0; n < uCount; n ++ ) {

		m_pScratchpad[n * 2 + 0] = VirtualToPhysical(m_pScratchBuf) + n * GetPageSize();

		m_pScratchpad[n * 2 + 1] = 0x00000000;  
		}
	}

void CUsbHostExtensible::FreeScratchpad(void)
{
	FreeNonCached(m_pScratchpad);

	FreeNonCached(m_pScratchBuf);

	m_pScratchpad = NULL;

	m_pScratchBuf = NULL;
	}

// Device 

void CUsbHostExtensible::MakeDeviceList(void)
{
	m_pDevCtxList = (PDWORD) AllocNonCachedInit((GetMaxSlots() + 1) * sizeof(DWORD) * 2, 64);

	m_pDevObjList = New CDevice * [ GetMaxSlots() ]; 
	}

void CUsbHostExtensible::InitDeviceList(void)
{
	memset(m_pDevCtxList, 0, (GetMaxSlots() + 1) * sizeof(DWORD) * 2);

	memset(m_pDevObjList, 0, GetMaxSlots() * sizeof(CDevice *));

	m_pDevCtxList[0] = VirtualToPhysical(m_pScratchpad);
	}

void CUsbHostExtensible::FreeDeviceList(void)
{
	FreeNonCached(m_pDevCtxList);

	if( m_pDevObjList ) {

		for( UINT i = 0; i < GetMaxSlots(); i ++ ) {

			FreeDeviceObj(m_pDevObjList[i]);
			}
		
		delete [] m_pDevObjList;
		}

	m_pDevCtxList = NULL;

	m_pDevObjList = NULL;
	}

PVDWORD CUsbHostExtensible::MakeDeviceCtx(void)
{
	if( GetContext64() ) {
	
		return (PVDWORD) AllocNonCachedInit(64 * 32, 64);
		}

	return (PVDWORD) AllocNonCachedInit(32 * 32, 32);
	}

PVDWORD CUsbHostExtensible::MakeInputCtx(void)
{
	if( GetContext64() ) {
	
		return (PVDWORD) AllocNonCachedInit(64 * 33, 64);
		}

	return (PVDWORD) AllocNonCachedInit(32 * 33, 32);
	}

void CUsbHostExtensible::FreeDeviceCtx(PVDWORD pCtx)
{
	FreeNonCached(PVOID(pCtx));
	}

void CUsbHostExtensible::FreeInputCtx(PVDWORD pCtx)
{
	FreeNonCached(PVOID(pCtx));
	}

CUsbHostExtensible::CDevice * CUsbHostExtensible::MakeDeviceObj(void)
{
	CDevice *pObj      = New CDevice;

	pObj->m_pInputCtx  = MakeInputCtx();

	pObj->m_pOutputCtx = MakeDeviceCtx();

	memset(pObj->m_pEndptObj, 0, sizeof(CEndpoint *) * constMaxEndpoints);

	return pObj;
	}

void CUsbHostExtensible::FreeDeviceObj(CDevice *pObj)
{
	if( pObj ) {

		FreeInputCtx(pObj->m_pInputCtx);

		FreeDeviceCtx(pObj->m_pOutputCtx);

		for( UINT i = 0; i < elements(pObj->m_pEndptObj); i ++ ) {

			FreeEndpointObj(pObj->m_pEndptObj[i]);
			}

		delete pObj;
		}
	}

PVDWORD CUsbHostExtensible::GetContext(PVDWORD pBase, UINT i)
{
	if( GetContext64() ) {

		return PVDWORD(DWORD(pBase) + 64 * i);
		}

	return PVDWORD(DWORD(pBase) + 32 * i);
	}

void CUsbHostExtensible::SetDeviceCtxItem(UINT iSlot, PVDWORD pCtx)
{
	m_pDevCtxList[iSlot * 2 + 0] = pCtx ? VirtualToPhysical(PVOID(pCtx)) : NULL;

	m_pDevCtxList[iSlot * 2 + 1] = 0x00000000;
	}

void CUsbHostExtensible::CheckHubConfig(UINT iSlot)
{
	if( iSlot != NOTHING ) {

		CDevice *pDevObj = m_pDevObjList[iSlot];

		if( pDevObj ) {

			IUsbDevice  *pDevInt  = pDevObj->m_pDevice;

			CControlCtx *pCtrlCtx = (CControlCtx *) GetContext(pDevObj->m_pInputCtx, 0);

			CSlotCtx    *pSlotCtx = (CSlotCtx    *) GetContext(pDevObj->m_pInputCtx, 1);

			pCtrlCtx->m_dwAddCtx  = Bit(0);  

			if( pDevInt->GetDevDesc().m_bClass == devHub ) {

				UsbDeviceDesc const &Dev = pDevInt->GetDevDesc();

				UsbHubDesc    const &Hub = pDevInt->GetHubDesc();

				pSlotCtx->m_dwHub       = true;

				pSlotCtx->m_dwPortCount = Hub.m_bPorts;  

				if( Dev.m_bProtocol == 2 ) {

					pSlotCtx->m_dwMulti = true;
					}

				if( pDevInt->GetSpeed() == usbSpeedHigh ) {

					pSlotCtx->m_dwThinkTime = Hub.m_wThinkTime;
					}
				}
			else {
				if( pDevInt->GetSpeed() != usbSpeedHigh ) {

					BYTE bHubAddr = pDevInt->GetPortPath().a.dwHubAddr;
					
					UINT uPortNum = pDevInt->GetPortPath().a.dwHubPort;

					UINT iSlotHub = FindDeviceSlot(bHubAddr);

					if( iSlotHub != NOTHING ) {

						IUsbHubDriver *pHub = (IUsbHubDriver *) m_pDevObjList[iSlotHub]->m_pDevice->GetDriver(0);;

						if( pHub ) {
							
							pSlotCtx->m_dwMulti   = pHub->HasMultiTT();
						
							pSlotCtx->m_dwHubSlot = iSlotHub;

							pSlotCtx->m_dwHubPort = uPortNum;
							}
						}
					}
				}
			}
		}
	}

void CUsbHostExtensible::CheckDevConfig(UINT iSlot)
{
	if( iSlot != NOTHING ) {

		CDevice *pDevObj = m_pDevObjList[iSlot];

		if( pDevObj ) {

			CControlCtx *pCtrlCtx = (CControlCtx *) GetContext(pDevObj->m_pInputCtx, 0);

			CSlotCtx    *pSlotCtx = (CSlotCtx    *) GetContext(pDevObj->m_pInputCtx, 1);
			
			for( UINT iEndpt = 1; iEndpt < constMaxEndpoints; iEndpt ++ ) {

				CEndpoint *pEndpt = pDevObj->m_pEndptObj[iEndpt];

				if( pEndpt ) {

					pCtrlCtx->m_dwAddCtx  |= Bit(iEndpt + 1);

					pSlotCtx->m_dwContexts = iEndpt + 1;
					}
				}
			}
		}
	}

DWORD CUsbHostExtensible::FindDeviceSlot(BYTE bAddr)
{
	if( m_pDevObjList ) {

		for( UINT i = 0; i < GetMaxSlots(); i ++ ) {

			if( m_pDevObjList[i] ) {

				if( m_pDevObjList[i]->m_pDevice->GetAddr() == bAddr ) {

					return i;
					}					
				}
			}
		}
	
	return NOTHING;
	}

// Endpoints

CUsbHostExtensible::CEndpoint * CUsbHostExtensible::MakeEndpointObj(void)
{
	CEndpoint *pObj  = New CEndpoint;

	pObj->m_pTrbRing = (CTrb *) AllocNonCachedInit(sizeof(CTrb) * constTrbRingSize, 64); 

	pObj->m_pData    = (PBYTE)  AllocNonCachedInit(constMaxEndpointData, 64);

	pObj->m_iIndex   = NOTHING;

	InitEndpointObjRing(pObj);

	return pObj;
	}

void CUsbHostExtensible::InitEndpointObjRing(CEndpoint *pObj)
{
	memset(pObj->m_pTrbRing, 0, sizeof(CTrb)     * constTrbRingSize);

	memset(pObj->m_pUrbRing, 0, sizeof(UsbIor *) * constTrbRingSize);

	pObj->m_iTrbHead = 0;

	pObj->m_iTrbTail = 0;

	pObj->m_fTrbPCS  = true;

	pObj->m_fTrbCCS  = true;

	CTrb &Trb        = (CTrb &) pObj->m_pTrbRing[constTrbRingSize - 1];

	Trb.m_dwParam0   = VirtualToPhysical(pObj->m_pTrbRing);
	
	Trb.m_dwType     = trbLink;

	Trb.m_dwFlags    = flagToggle;
	}

void CUsbHostExtensible::FreeEndpointObj(CEndpoint *pObj)
{
	if( pObj ) {

		FreeNonCached(PVOID(pObj->m_pTrbRing));

		FreeNonCached(PVOID(pObj->m_pData));

		delete pObj;
		}
	}

UINT CUsbHostExtensible::AddrToIndex(BYTE bAddr, bool fDirIn)
{
	if( bAddr == 0 ) {

		return 0;
		}

	if( bAddr < 16 ) {

		return bAddr * 2 + (fDirIn ? 1 : 0) - 1;
		}

	return NOTHING;
	}

DWORD CUsbHostExtensible::FindEndpointInterval(UsbEndpointDesc const &Desc, UINT uSpeed)
{
	DWORD dwInterval = Desc.m_bInterval;

	if( uSpeed == usbSpeedLow || uSpeed == usbSpeedFull ) {

		UINT uTarget = Desc.m_bInterval * 8;

		UINT uTest   = 8;
					
		UINT uRound  = 3;

		while( uTest < uTarget ) {

			uTest <<= 1;

			uRound++;
			}

		dwInterval = uRound;
		}

	return dwInterval;
	}

// Port Helpers

void CUsbHostExtensible::PortPower(BOOL fOn)
{
	 if( GetPowerCtrl() ) {

		for( UINT n = 0; n < GetNumPorts(); n ++ ) {
		
			PortSet(n, portPower);
			}
		}
	}

DWORD CUsbHostExtensible::PortGet(UINT i, DWORD Mask)
{
	DWORD volatile &Reg = m_pOperate[regPortSC + 4 * (i - 1)];

	return Reg & Mask;
	}

void CUsbHostExtensible::PortSet(UINT i, DWORD Data)
{
	DWORD volatile &Reg = m_pOperate[regPortSC + 4 * (i - 1)];

	Reg |= Data;
	}

void CUsbHostExtensible::PortClr(UINT i, DWORD Data)
{
	DWORD volatile &Reg = m_pOperate[regPortSC + 4 * (i - 1)];

	Reg &= ~Data;
	}

// Debug

void CUsbHostExtensible::DumpHardwareCaps(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nHardware Capabilities\n");	

	AfxTrace("VERSION      : 0x%4.4X\n", GetVersion()    ); 
	AfxTrace("HCSPARAMS1   : 0x%8.8X\n", Caps(HcsParams1)); 
	AfxTrace("HCSPARAMS2   : 0x%8.8X\n", Caps(HcsParams2)); 
	AfxTrace("HCSPARAMS3   : 0x%8.8X\n", Caps(HcsParams3)); 
	AfxTrace("HCCPARAMS1   : 0x%8.8X\n", Caps(HccParams1)); 
	AfxTrace("HCCPARAMS2   : 0x%8.8X\n", Caps(HccParams2)); 
	AfxTrace("SLOTS        : %d\n",      GetMaxSlots()   );
	AfxTrace("PORTS        : %d\n",      GetNumPorts()   );
	AfxTrace("OPERATION    : 0x%8.8X\n", GetCapsLen()    );
	AfxTrace("RUNTIME      : 0x%8.8X\n", Caps(Runtime)   );
	AfxTrace("DOORBELL     : 0x%8.8X\n", Caps(Doorbell)  ); 

	#endif
	}

void CUsbHostExtensible::DumpHardwareCapsEx(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nExtended Hardware Capabilities\n");

	UINT iEnum = 0;
	
	for(;;) {
	
		PDWORD pCaps = EnumExtCaps(iEnum);

		if( pCaps ) {

			AfxTrace("CAPABILITY   : 0x%2.2X\n", LOBYTE(LOWORD(pCaps[0]))); 
			AfxTrace("NEXT         : 0x%2.2X\n", HIBYTE(LOWORD(pCaps[0]))); 
			AfxTrace("PARAM        : 0x%4.4X\n", HIWORD(pCaps[0]));

			continue;
			}

		break;
		}

	#endif
	}

void CUsbHostExtensible::DumpHardware(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nHardware\n");	

	AfxTrace("USBCMD      : 0x%8.8X\n", OpReg(UsbCmd)   );
	AfxTrace("USBSTS      : 0x%8.8X\n", OpReg(UsbSts)   );
	AfxTrace("PAGESIZE    : 0x%8.8X\n", OpReg(PageSize) );
	AfxTrace("DEVCTLR     : 0x%8.8X\n", OpReg(DevCtrl)  );
	AfxTrace("CMDRINGHI   : 0x%8.8X\n", OpReg(CmdRingHi));
	AfxTrace("CMDRINGLO   : 0x%8.8X\n", OpReg(CmdRingLo));
	AfxTrace("CTXBASEHI   : 0x%8.8X\n", OpReg(DevCtxHi) );
	AfxTrace("CTXBASELO   : 0x%8.8X\n", OpReg(DevCtxLo) );
	AfxTrace("CONFIG      : 0x%8.8X\n", OpReg(Config)   );
	AfxTrace("PORTSC      : 0x%8.8X\n", OpReg(PortSC)   );

	#endif
	}

void CUsbHostExtensible::DumpRuntime(void) const 
{
	#if defined(_XDEBUG)	

	AfxTrace("\nRuntime\n");	

	AfxTrace("MFINDEX     : 0x%8.8X\n", RtReg(MfIndex)     );
	AfxTrace("INTMAN      : 0x%8.8X\n", RtReg(IntMan)      );
	AfxTrace("INTMOD      : 0x%8.8X\n", RtReg(IntMod)      );
	AfxTrace("INTTABLESIZ : 0x%8.8X\n", RtReg(IntTableSize));
	AfxTrace("INTTABLELO  : 0x%8.8X\n", RtReg(IntTableLo)  );
	AfxTrace("INTTABLEHI  : 0x%8.8X\n", RtReg(IntTableHi)  );
	AfxTrace("DEQUEUELO   : 0x%8.8X\n", RtReg(IntDequeueLo));
	AfxTrace("DEQUEUEHI   : 0x%8.8X\n", RtReg(IntDequeueHi));

	#endif
	}

void CUsbHostExtensible::DumpCmdRing(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nCommand Ring\n");

	AfxTrace("HEAD         : %3.3d\n", m_iCmdHead ); 
	AfxTrace("TAIL         : %3.3d\n", m_iCmdTail ); 
	AfxTrace("CYCLE        : %d\n",    m_fCmdCycle);

	DumpTrbRing(m_pCmdRing, constCmdRingSize);

	#endif
	}

void CUsbHostExtensible::DumpEventRing(void) const
{
	#if defined(_XDEBUG)	

	AfxTrace("\nEvent Ring\n");

	AfxTrace("HEAD         : %3.3d\n", m_iEventHead ); 
	AfxTrace("TAIL         : %3.3d\n", m_iEventTail ); 
	AfxTrace("CYCLE        : %d\n",    m_fEventCycle);

	DumpTrbRing((CTrb const *) m_pEventRing, constEventRingSize);

	#endif
	}

void CUsbHostExtensible::DumpTrbRing(CTrb const *pTrb, UINT uCount) const
{
	#if defined(_XDEBUG)	

	for( UINT n = 0; n < uCount; n ++ ) {

		DumpTrbData(pTrb[n], n);
		}

	#endif
	}

void CUsbHostExtensible::DumpTrbData(CTrb const &Trb, UINT i) const
{
	#if defined(_XDEBUG)	

	AfxTrace("TRB          : [%d]{0x%8.8X}\n", i, &Trb);
	
	AfxTrace("TYPE         : %d\n",      Trb.m_dwType   ); 
	AfxTrace("PARAM0       : 0x%8.8X\n", Trb.m_dwParam0 );
	AfxTrace("PARAM1       : 0x%8.8X\n", Trb.m_dwParam1 );
	AfxTrace("STATUS       : 0x%8.8X\n", Trb.m_dwStatus );
	AfxTrace("FLAGS        : 0x%4.4X\n", Trb.m_dwFlags  );
	AfxTrace("CONTROL      : 0x%4.4X\n", Trb.m_dwControl);
	AfxTrace("\n");

	#endif
	}

void CUsbHostExtensible::DumpContext(CSlotCtx const &Ctx, UINT i) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nSlot Context : [%d]{0x%8.8X}\n", i, &Ctx);
	
	AfxTrace("ROUTE        : 0x%8.8X\n", Ctx.m_dwRouteStr	);
	AfxTrace("SPEED        : 0x%4.4X\n", Ctx.m_dwSpeed	);
	AfxTrace("MULTI        : 0x%4.4X\n", Ctx.m_dwMulti	);
	AfxTrace("HUB          : 0x%4.4X\n", Ctx.m_dwHub	);
	AfxTrace("CONTEXTS     : 0x%4.4X\n", Ctx.m_dwContexts	);
	AfxTrace("LATENCY      : 0x%4.4X\n", Ctx.m_dwLatency	);
	AfxTrace("ROOTHUBPORT  : 0x%4.4X\n", Ctx.m_dwRootHubPort);
	AfxTrace("PORTS        : 0x%4.4X\n", Ctx.m_dwPortCount	);
	AfxTrace("HUBSLOT      : 0x%4.4X\n", Ctx.m_dwHubSlot	);
	AfxTrace("HUBPORT      : 0x%4.4X\n", Ctx.m_dwHubPort	);
	AfxTrace("THINKTIME    : 0x%4.4X\n", Ctx.m_dwThinkTime	);
	AfxTrace("INTERRUPT    : 0x%4.4X\n", Ctx.m_dwInterrupter);
	AfxTrace("DEVADDR      : 0x%4.4X\n", Ctx.m_dwDevAddr	);
	AfxTrace("STATE        : 0x%4.4X\n", Ctx.m_dwState	);

	#endif
	}

void CUsbHostExtensible::DumpContext(CEndptCtx const &Ctx, UINT n) const
{
	#if defined(_XDEBUG)

	AfxTrace("\nEndpoint Context : [%d]{0x%8.8X}\n", n, &Ctx);

	AfxTrace("STATE        : 0x%8.8X\n", Ctx.m_dwState	      );
	AfxTrace("MULT         : 0x%8.8X\n", Ctx.m_dwMult	      );
	AfxTrace("MAXPRIMSTM   : 0x%8.8X\n", Ctx.m_dwMaxPrimStms      );
	AfxTrace("LINEARSTMS   : 0x%8.8X\n", Ctx.m_dwLinearStms	      );
	AfxTrace("INTERVAL     : 0x%8.8X\n", Ctx.m_dwInterval	      );
	AfxTrace("SERVTIME     : 0x%8.8X\n", Ctx.m_dwServiceTime      );
	AfxTrace("ERRCOUNT     : 0x%8.8X\n", Ctx.m_dwErrorCount	      );
	AfxTrace("TYPE         : 0x%8.8X\n", Ctx.m_dwType	      );
	AfxTrace("HOSTDIS      : 0x%8.8X\n", Ctx.m_dwHostDisable      );
	AfxTrace("MAXBURST     : 0x%8.8X\n", Ctx.m_dwMaxBusrt	      );
	AfxTrace("MAXPACKET    : 0x%8.8X\n", Ctx.m_dwMaxPacket	      );
	AfxTrace("DEQUEUECYCLE : 0x%8.8X\n", Ctx.m_dwDequeueState     );
	AfxTrace("DEQUEUEPTRHI : 0x%8.8X\n", Ctx.m_dwDequeuePtrHi << 4);
	AfxTrace("DEQUEUEPTRLO : 0x%8.8X\n", Ctx.m_dwDequeuePtrLo << 4);
	AfxTrace("AVGTRBLEN    : 0x%8.8X\n", Ctx.m_dwAvgTrbLen	      );
	AfxTrace("MAXESIT      : 0x%8.8X\n", Ctx.m_dwMaxEsit	      );
	
	#endif
	}

void CUsbHostExtensible::DumpContext(CStmCtx const &Ctx) const
{
	#if defined(_XDEBUG)	
	
	AfxTrace("\nStream Context : {0x%8.8X}\n", &Ctx);

	AfxTrace("DEQUEUESTATE : 0x%8.8X\n", Ctx.m_dwDequeueState);
	AfxTrace("TYPE         : 0x%8.8X\n", Ctx.m_dwType	 );
	AfxTrace("DEQUEUEPTRHI : 0x%8.8X\n", Ctx.m_dwDequeuePtrHi);
	AfxTrace("DEQUEUEPTRLO : 0x%8.8X\n", Ctx.m_dwDequeuePtrLo);
	AfxTrace("STOPPED      : 0x%8.8X\n", Ctx.m_dwStopped	 );

	#endif
	}

void CUsbHostExtensible::DumpContext(CControlCtx const &Ctx) const
{
	#if defined(_XDEBUG)	
	
	AfxTrace("\nControl Context : {0x%8.8X}\n", &Ctx);

	AfxTrace("DROPCTX      : 0x%8.8X\n", Ctx.m_dwDropCtx  );
	AfxTrace("ADDCTX       : 0x%8.8X\n", Ctx.m_dwAddCtx   );
	AfxTrace("CONFIG       : 0x%8.8X\n", Ctx.m_dwConfig   );
	AfxTrace("INTERFACE    : 0x%8.8X\n", Ctx.m_dwInterface);
	AfxTrace("ALTINTERFACE : 0x%8.8X\n", Ctx.m_dwAlternate);

	#endif
	}

void CUsbHostExtensible::DumpContext(CPortCtx const &Ctx) const
{
	#if defined(_XDEBUG)	
	
	AfxTrace("\nPort Context : {0x%8.8X}\n", &Ctx);

	for( UINT n = 0; n < elements(Ctx.m_bBandwidth); n++ ) {

		AfxTrace("PORT[%2.2d]     : 0x%2.2X\n", Ctx.m_bBandwidth[n]);
		}

	#endif
	}

void CUsbHostExtensible::DumpOutputContext(PCDWORD pCtx)
{
	#if defined(_XDEBUG)	
	
	AfxTrace("\nDevice Context : {0x%8.8X}\n", pCtx);

	DumpContext(*(CSlotCtx *) GetContext(PVDWORD(pCtx), 0), 0);

	for( UINT n = 0; n < 31; n++ ) {

		DumpContext(*(CEndptCtx *)  GetContext(PVDWORD(pCtx), n + 1), n);
		}

	#endif
	}

void CUsbHostExtensible::DumpInputContext(PCDWORD pCtx)
{
	#if defined(_XDEBUG)	
	
	AfxTrace("\nInput Context : {0x%8.8X}\n\n", pCtx);

	DumpContext(*(CControlCtx *) GetContext(PVDWORD(pCtx), 0));

	DumpContext(*(CSlotCtx    *) GetContext(PVDWORD(pCtx), 1), 0);

	for( UINT n = 0; n < 31; n++ ) {

		DumpContext(*(CEndptCtx *)  GetContext(PVDWORD(pCtx), n + 2), n);
		}

	#endif
	}

// End of File
