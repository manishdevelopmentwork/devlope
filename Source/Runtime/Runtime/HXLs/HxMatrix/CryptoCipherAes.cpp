
#include "Intern.hpp"

#include "CryptoCipherAes.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AES-CBC Symmetric Cipher
//

// Instantiator

static IUnknown * Create_CryptoCipherAes(PCTXT pName)
{
	return New CCryptoCipherAes;
	}

// Registration

global void Register_CryptoCipherAes(void)
{
	piob->RegisterInstantiator("crypto.cipher-aes", Create_CryptoCipherAes);
	}

// Constructor

CCryptoCipherAes::CCryptoCipherAes(void)
{
	}

// ICryptoHash

CString CCryptoCipherAes::GetName(void)
{
	return "aes";
	}

void CCryptoCipherAes::Initialize(PCBYTE pPass, UINT uPass)
{
	BYTE bVec[16];

	BYTE bKey[32];

	MakeBytes(bVec, sizeof(bVec), pPass, uPass);

	MakeBytes(bKey, sizeof(bKey), pPass, uPass);

	psAesInitCBC(&m_Ctx, bVec, bKey, sizeof(bKey), 0);
	}

void CCryptoCipherAes::Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psAesEncryptCBC(&m_Ctx, pIn, pOut, uSize);
	}

void CCryptoCipherAes::Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize)
{
	psAesDecryptCBC(&m_Ctx, pIn, pOut, uSize);
	}

void CCryptoCipherAes::Finalize(void)
{
	psAesClearCBC(&m_Ctx);
	}

// End of File
