
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOInputWnd_HPP

#define INCLUDE_DADIDOInputWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDADIDOInputConfig;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI Config Window
//

class CDADIDOInputWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDADIDOInputConfig * m_pItem;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);

	// Implementation
	void AddInputs(void);
};

// End of File

#endif
