
#include "Dnp3IpSv1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Tcp Slave v1 Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CDnp3IpSlave);

// End of File

