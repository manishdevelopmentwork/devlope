
#include "intern.hpp"

#include "toyotcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC TCP Driver
//

// Instantiator

INSTANTIATE(CToyodatcpDriver);

// Constructor

CToyodatcpDriver::CToyodatcpDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uKeep     = 0;
	}

// Destructor

CToyodatcpDriver::~CToyodatcpDriver(void)
{
	}

// Configuration

void MCALL CToyodatcpDriver::Load(LPCBYTE pData)
{ 
	}
	
// Management

void MCALL CToyodatcpDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CToyodatcpDriver::Open(void)
{
	}

// Device

CCODE MCALL CToyodatcpDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= HostToMotor(GetLong(pData));
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CToyodatcpDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CToyodatcpDriver::Ping(void)
{
	if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			}	

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = CMX;
		Addr.a.m_Offset = 0x1;
		Addr.a.m_Type   = addrBitAsBit;
		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CToyodatcpDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	uCount = SetCommand( Addr, TRUE, uCount );

	return Transact(Addr.a.m_Table, Addr.a.m_Type, pData, uCount, TRUE) ? uCount : CCODE_ERROR;
	}

CCODE MCALL CToyodatcpDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	uCount = SetCommand( Addr, FALSE, uCount );

	AddWriteData(Addr.a.m_Table, Addr.a.m_Type, pData, uCount);

	DWORD d;

	return Transact(Addr.a.m_Table, Addr.a.m_Type, &d, uCount, FALSE) ? uCount : CCODE_ERROR;
	}

// Socket Management

BOOL CToyodatcpDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CToyodatcpDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CToyodatcpDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

UINT	CToyodatcpDriver::SetCommand(AREF Addr, BOOL fIsRead, UINT uCount)
{
	StartFrame();

	AddCommand(Addr, fIsRead);

	UINT r = SpecifyCount(Addr, uCount);

	m_uEndOfCommand = IsTimerCounter(Addr.a.m_Table) ? m_uPtr - 4 : m_uPtr;

	return r;
	}

void	CToyodatcpDriver::StartFrame(void)
{
	m_uPtr   = 0;

	AddByte(':');
	AddByte(':');

	m_bCheck = 0;

	AddByte( m_pHex[m_pCtx->m_bUnit / 8] );
	AddByte( m_pHex[m_pCtx->m_bUnit % 8] );

	AddByte( '?' );

	AddByte( '0' );
	}

void	CToyodatcpDriver::AddCommand(AREF Addr, BOOL fIsRead)
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	AddBitCommand( Addr, fIsRead);	break;
		case addrByteAsByte:	AddByteCommand(Addr, fIsRead);	break;
		case addrWordAsWord:	AddWordCommand(Addr, fIsRead);	break;
		}

	AddIdentifier(Addr.a.m_Table);

	AddAddress(Addr.a.m_Offset);

	if( Addr.a.m_Type == addrByteAsByte ) AddByte('L'); // TODO find out what this is supposed to be
	}

void	CToyodatcpDriver::AddIdentifier(UINT uTable)
{
	switch( uTable ) {

		case CMEX:	AddByte('E');
		case CMX:	AddByte('X');	return;

		case CMEY:	AddByte('E');
		case CMY:	AddByte('Y');	return;

		case CMEM:	AddByte('E');
		case CMM:	AddByte('M');	return;

		case CMEK:	AddByte('E');
		case CMK:	AddByte('K');	return;

		case CMEV:	AddByte('E');
		case CMV:	AddByte('V');	return;

		case CMET:	AddByte('E');
		case CMT:	AddByte('T');	return;

		case CMEC:	AddByte('E');
		case CMC:	AddByte('C');	return;

		case CMEL:	AddByte('E');
		case CML:	AddByte('L');	return;

		case CMEP:	AddByte('E');
		case CMP:	AddByte('P');	return;

//		case CMEN:	AddByte('E');		// not supported in protocol
		case CMZ:
		case CMN:			return; // no identifier

		case CMES:	AddByte('E');
		case CMS:	AddByte('S');	return;

		case CMD:	AddByte('D');	return;

		case CMR:	AddByte('R');	return;

//		case CMH:	AddByte('H');	return; // not supported in protocol

		case CMU:	AddByte('U');	return;

		case CMGX:	AddByte('G');	AddByte('X');	return;

		case CMGY:	AddByte('G');	AddByte('Y');	return;

		case CMGM:	AddByte('G');	AddByte('M');	return;

		case CMEB:	AddByte('E');
		case CMB:	AddByte('B');	return;
		}
	}

void	CToyodatcpDriver::AddAddress(UINT uOffset)
{
	AddData(uOffset, 4);
	}

UINT	CToyodatcpDriver::SpecifyCount(AREF Addr, UINT uCount)
{
	if( IsTimerCounter(Addr.a.m_Table) ) return 1;

	UINT uSz = min( uCount, 8 );

	switch( Addr.a.m_Type ) {

		case addrByteAsByte:

			AddData( uSz - 1, 2 );
			return uSz;

		case addrWordAsWord:

			AddData( Addr.a.m_Offset + uSz - 1, 4 );
			return uSz;
		}

	return 1;
	}

void	CToyodatcpDriver::AddWriteData(UINT uTable, UINT uType, PDWORD pData, UINT uCount)
{
	UINT u;

	switch( uType ) {

		case addrBitAsBit:
			AddByte( (*pData) & 1 ? '1' : '0' );
			return;

		case addrByteAsByte:
			for( u = 0; u < uCount; u++ ) AddData( pData[u], 2 );
			return;

		case addrWordAsWord:

			if( !IsTimerCounter(uTable) ) for( u = 0; u < uCount; u++ ) AddData( pData[u], 4 );

			else AddData( pData[0], 5 );

			return;
		}
	}

void	CToyodatcpDriver::AddChecksum(void)
{
	AddData( 0x100 - UINT(m_bCheck), 2 );
	}

void	CToyodatcpDriver::EndFrame(void)
{
	AddByte( CR );
	}

void	CToyodatcpDriver::AddByte(BYTE b)
{
	m_bTx[m_uPtr++] = b;

	m_bCheck += b;
	}

void	CToyodatcpDriver::AddData(DWORD u, UINT uCount)
{
	switch( uCount ) {

		case 8:	AddByte(m_pHex[(u >> 28) & 0xF]);
		case 7:	AddByte(m_pHex[(u >> 24) & 0xF]);
		case 6:	AddByte(m_pHex[(u >> 20) & 0xF]);
		case 5:	AddByte(m_pHex[(u >> 16) & 0xF]);
		case 4:	AddByte(m_pHex[(u >> 12) & 0xF]);
		case 3:	AddByte(m_pHex[(u >>  8) & 0xF]);
		case 2:	AddByte(m_pHex[(u >>  4) & 0xF]);
		case 1:	AddByte(m_pHex[ u        & 0xF]);
		}
	}

void	CToyodatcpDriver::AddBitCommand(AREF Addr, BOOL fIsRead)
{
	AddProgramNumber(Addr.a.m_Table, Addr.a.m_Extra);

	AddByte( fIsRead ? 'M' : 'S' );
	AddByte( 'R');
	AddByte( fIsRead ? 'L' : 'R' );
	}

void	CToyodatcpDriver::AddByteCommand(AREF Addr, BOOL fIsRead)
{
	AddProgramNumber(Addr.a.m_Table, Addr.a.m_Extra);
 
	AddByte( fIsRead ? 'M' : 'S' );
	AddByte( 'R' );
	AddByte( 'B' );
	}

void	CToyodatcpDriver::AddWordCommand(AREF Addr, BOOL fIsRead)
{
	AddProgramNumber(Addr.a.m_Table, Addr.a.m_Extra);
 
	if( IsTimerCounter(Addr.a.m_Table) ) {

		AddByte( fIsRead ? 'T' : Addr.a.m_Table == CMN ? 'P' : 'S' );
		AddByte( fIsRead ? 'C' : 'P' );
		AddByte( fIsRead ? 'R' : 'W' );
		return;
		}

	AddByte( fIsRead ? 'R' : 'W' );
	AddByte( 'D' );
	AddByte( 'R' );
	}

// Transport Layer

BOOL	CToyodatcpDriver::Transact(UINT uTable, UINT uType, PDWORD pData, UINT uCount, BOOL fIsRead)
{
	if( Send() ) {

		if( GetReply() ) {

			GetResponse(uTable, uType, pData, fIsRead ? uCount : 1);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL	CToyodatcpDriver::Send(void)
{
	AddChecksum();

	EndFrame();

	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Receive Processing

BOOL	CToyodatcpDriver::GetReply(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

			uPtr += uSize;

			if( m_bRx[uPtr-1] == CR )  {

				for( UINT u = 0; uPtr && (u < m_uEndOfCommand); u++ ) {

					if( m_bRx[u] != m_bTx[u] ) {

						if( u != 5 ) uPtr = 0; // wait for proper response (ignore RI)
						}

					if( uPtr ) return CheckResponse(uPtr);
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL	CToyodatcpDriver::CheckResponse(UINT uEnd)
{
	UINT uData = 0;

	for( UINT u = 2; u < uEnd - 3; u++ ) uData -= m_bRx[u];

	if( m_pHex[LOBYTE(uData)/16] != m_bRx[uEnd-3] ) return FALSE;
	if( m_pHex[LOBYTE(uData)%16] != m_bRx[uEnd-2] ) return FALSE;

	return TRUE;
	}

void	CToyodatcpDriver::GetResponse(UINT uTable, UINT uType, PDWORD pData, UINT uCount)
{
	if( IsTimerCounter(uTable) ) {

		pData[0] = GetData( m_uEndOfCommand + (uTable == CMN ? 5 : 0), 5 );

		return;
		}

	UINT uSize = uType == addrBitAsBit ? 1 : uType == addrByteAsByte ? 2 : 4;

	for( UINT i = 0; i < uCount; i++ ) pData[i] = GetData(m_uEndOfCommand + (i * uSize), uSize);
	}

DWORD	CToyodatcpDriver::GetData(UINT uPos, UINT uSize)
{
	DWORD d   = 0;

	UINT uEnd = uPos + uSize;

	while( uPos < uEnd ) {

		BYTE b = m_bRx[uPos++];

		if( b >= '0' && b <= '9' ) {

			b -= '0';
			}

		else {
			if( b >= 'A' && b <= 'F' ) b -= '7';

			else {
				if( b >= 'a' && b <= 'f' ) b -= 'W';

				else b = 0;
				}
			}

		d = (16 * d) + b;
		}

	return d;
	}

// Helpers

void	CToyodatcpDriver::AddProgramNumber(UINT uTable, UINT uExtra)
{
	switch( uTable ) {

		case CMX:
		case CMY:
		case CMM:
		case CMK:
		case CMV:
		case CMT:
		case CMC:
		case CML:
		case CMP:
		case CMD:
		case CMR:
		case CMS:
		case CMN:
		case CMZ:
			AddByte(uExtra);
			return;

		case CMEX:
		case CMEY:
		case CMEM:
		case CMEK:
		case CMEV:
		case CMET:
		case CMEC:
		case CMEL:
		case CMEP:
		case CMES:
//		case CMEN:	// not supported in protocol
//		case CMH:	// not supported in protocol
		case CMGX:
		case CMGY:
		case CMGM:
			AddByte('0');
			return;

		case CMU:
			AddByte('8');
			return;
		}
	}

BOOL	CToyodatcpDriver::IsTimerCounter(UINT uTable)
{
	return uTable == CMN || uTable == CMZ;
	}

// End of File
