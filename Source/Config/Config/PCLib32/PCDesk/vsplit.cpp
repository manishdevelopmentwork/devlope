
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Vertical Split View Window
//

// Dynamic Class

AfxImplementDynamicClass(CVertSplitViewWnd, CSplitViewWnd);

// Constructor

CVertSplitViewWnd::CVertSplitViewWnd(void)
{
	m_Cursor.Create(L"VSplitAdjust");
	}

// Message Map

AfxMessageMap(CVertSplitViewWnd, CSplitViewWnd)
{
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_GETMETRIC)
	AfxDispatchMessage(WM_SIZE)
	
	AfxDispatchControlType(IDM_WINDOW, OnWindowControl)
	AfxDispatchCommandType(IDM_WINDOW, OnWindowCommand)

	AfxMessageEnd(CVertSplitViewWnd)
	};

// Message Handlers

void CVertSplitViewWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !m_fFixed && !m_fCapture ) {
	
		if( m_Rect[2].PtInRect(Pos) ) {
		
			StartAdjust(Pos.y);
			
			return;
			}
		}
	}

void CVertSplitViewWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( m_fCapture ) {
	
		TrackAdjust(Pos.y);
		
		return;
		}	
	}

void CVertSplitViewWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_fCapture ) {
	
		EndAdjust();
		
		return;
		}
	}

void CVertSplitViewWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	if( m_fCapture ) {
	
		int nPos = m_nTrackPos;
	
		switch( uCode ) {
		
			case VK_ESCAPE:
				AbortAdjust();
				break;
				
			case VK_RETURN:
				EndAdjust();
				break;
				
			case VK_UP:
				nPos -= 4;
				break;
				
			case VK_DOWN:
				nPos += 4;
				break;
				
			case VK_PRIOR:
				nPos -= 16;
				break;
				
			case VK_NEXT:
				nPos += 16;
				break;
			}
			
		if( TrackAdjust(nPos) ) {

			CPoint Pos = GetCursorPos();
				
			ScreenToClient(Pos);
		
			Pos.y = m_nTrackPos;

			ClientToScreen(Pos);
			
			SetCursorPos(Pos.x, Pos.y);
			}
		}
	}

void CVertSplitViewWnd::OnGetMetric(UINT uCode, CSize &Size)
{
	if( uCode == MC_BEST_SIZE ) {

		if( m_fUse[1] ) {
	
			Size = m_pWnd[1]->GetWindowRect().GetSize();
				
			m_pWnd[1]->SendMessage(WM_GETMETRIC, uCode, LPARAM(&Size));
				
			AdjustWindowSize(Size);
				
			Size.cy = GetWindowRect().GetHeight();
			
			return;
			}
		}
	}

void CVertSplitViewWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_RESTORED || uCode == SIZE_MAXIMIZED ) {
		
		if( m_nTotal != Size.cy ) {
			
			m_nTotal  = Size.cy;
		
			m_nBarPos = MulDiv(m_nTotal, m_nPercent, 1000);
			}
		
		if( m_pWnd[0] && m_pWnd[0]->IsWindow() ) {
		
			CalcWndRects();
		
			MoveWindows();
			}
		}
	}

// Command Handlers

BOOL CVertSplitViewWnd::OnWindowControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_WINDOW_ADJUST:
			
			Src.EnableItem(!m_fFixed && m_fUse[0] && m_fUse[1]);
			
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

BOOL CVertSplitViewWnd::OnWindowCommand(UINT uID)
{
	if( uID == IDM_WINDOW_ADJUST ) {
	
		CPoint Pos = GetCursorPos();
			
		ScreenToClient(Pos);
			
		Pos.y = (m_Rect[2].top + m_Rect[2].bottom) / 2;
			
		MakeMax(Pos.x, m_Rect[2].left);
	
		MakeMin(Pos.x, m_Rect[2].right);
			
		StartAdjust(Pos.y);
	
		ClientToScreen(Pos);
			
		SetCursorPos(Pos.x, Pos.y);
	
	        return TRUE;
		}
		
	return FALSE;
	}

// Overridables

void CVertSplitViewWnd::FindBestSplit(void)
{
	if( m_fUse[0] && m_fUse[1] ) {
		
		CSize Init = m_pWnd[1]->GetWindowRect().GetSize();
		
		CSize Size = Init;
		
		m_pWnd[1]->SendMessage(WM_GETMETRIC, MC_BEST_SIZE, (LPARAM) &Size);
		
		if( Size.cy < Init.cy ) {
			
			m_nBarPos += Init.cy - Size.cy;
			
			CalcWndRects();
			
			MoveWindows();
			}
		}
	}

void CVertSplitViewWnd::CalcWndRects(void)
{
	if( m_fUse[0] && m_fUse[1] ) {
		
		m_Rect[0] = m_Rect[1] = m_Rect[2] = GetClientRect();
		
		m_Rect[0].bottom = m_Rect[2].top = m_nBarPos - 2;
		
		m_Rect[2].bottom = m_Rect[1].top = m_nBarPos + 2;
		}
	else {
		if( m_fUse[0] ) {

			m_Rect[0] = GetClientRect();
			
			m_Rect[1].Set(0, 0, 0, 0);
			
			m_Rect[2].Set(0, 0, 0, 0);
			}
		else {
			m_Rect[0].Set(0, 0, 0, 0);

			m_Rect[1] = GetClientRect();
			
			m_Rect[2].Set(0, 0, 0, 0);
			}
		}
	}

// Implementation

void CVertSplitViewWnd::StartAdjust(int nPos)
{
	SetCapture();
		
	m_fCapture = TRUE;
			
	SetFocus();
			
	m_nTrackOrg = nPos;
			
	m_nTrackPos = nPos;
			
	ShowTracking();

	SetCursor(m_Cursor);
	}

BOOL CVertSplitViewWnd::TrackAdjust(int nPos)
{
	MakeMin(nPos, m_Rect[1].bottom - 4);
		
	MakeMax(nPos, m_Rect[0].top    + 4);

	if( m_nTrackPos != nPos ) {
	
		ShowTracking();
		
		m_nTrackPos = nPos;
		
		ShowTracking();
		
		return TRUE;
		}
		
	return FALSE;
	}

void CVertSplitViewWnd::EndAdjust(void)
{
	ReleaseCapture();
		
	m_fCapture = FALSE;
		
	ShowTracking();
		
	if( m_nTrackPos != m_nTrackOrg ) {
		
		m_nBarPos += m_nTrackPos - m_nTrackOrg;
		
		m_nPercent = MulDiv(1000, m_nBarPos, m_nTotal);

		CalcWndRects();
				
		MoveWindows();
			
		FindBestSplit();
				
		Invalidate(TRUE);
		}			
		
	m_pWnd[m_uFocus]->SetFocus();
	}
	
void CVertSplitViewWnd::AbortAdjust(void)
{
	ReleaseCapture();
		
	m_fCapture = FALSE;
		
	ShowTracking();
		
	m_pWnd[m_uFocus]->SetFocus();
	}

void CVertSplitViewWnd::ShowTracking(void)
{
	CExtendedDC DC(ThisObject, DCX_CACHE | DCX_CLIPSIBLINGS);
	
	CRect Rect   = m_Rect[2];
	
	Rect.top    += m_nTrackPos - m_nTrackOrg;
	
	Rect.bottom += m_nTrackPos - m_nTrackOrg;
	
	DC.PatBlt(Rect, DSTINVERT);
	}

// End of File
