
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "toyopuc.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "toyopuc.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "toyopuc.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "toyopuc.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// Toyoda PUC Address Selection Dialog
//
//

ToyodaPUCElementDlg DIALOG 0, 0, 0, 0
CAPTION "Select Address"
BEGIN
	EDITTEXT				2002,		  4,   0,  20,  12,
	LTEXT		"<type>",		2003,		 26,   1,  20,  12
	LTEXT		"Program",		2004,		-28,  19,  24,  12
	EDITTEXT				2005,		  4,  18,  20,  12
	CONTROL		"&High Byte",		2007, "button", BS_AUTOCHECKBOX, 50, 0, 36, 12
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CToyodaDeviceOptions
//

CToyodaDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop,,CUIEditInteger,|0||0|63,"
	"Indicate the Drop of the PUC device."
	"\0"

	"Program,Enable Program,,CUICheck,,"
	"Select if Program numbers are required for this device. "
	"\0"

	"\0"
END

CToyodaDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Drop,Program,\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CToyodaTCPDeviceOptions
//

CToyodaTCPDeviceOptionsUIList RCDATA
BEGIN
	"IPAddr,IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the PUC device."
	"\0"

	"Program,Enable Program,,CUICheck,,"
	"Select if Program numbers are required for this device. "
	"\0"

	"Port,TCP Port,,CUIEditInteger,|0||1|9999,"
	"Indicate the TCP port number on which the PUC protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Unit,Unit Number,,CUIEditInteger,|0||1|255,"
	"Indicate the unit within the PUC server that you wish to address. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the PUC driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a PUC request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

CToyodaTCPDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,IPAddr,Program,Port,Unit\0"
	"G:1,root,Protocol Options,Keep,Time1,Time3,Time2\0"
	"\0"
END

// End of File
