
#include "intern.hpp"

#include "banner.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CBannerCameraDriverOptions, CUIItem);

// Constructor

CBannerCameraDriverOptions::CBannerCameraDriverOptions(void)
{
	m_Type = 0;
	}

// UI Managament

void CBannerCameraDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {
	
		if( Tag == "Type" ) {   

			AfxTrace(L"type change\n");
			}
		}
	}

// Download Support

BOOL CBannerCameraDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Type));

	return TRUE;
	}

// Meta Data Creation

void CBannerCameraDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);
	}

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBannerCameraDeviceOptions, CUIItem);

// Constructor

CBannerCameraDeviceOptions::CBannerCameraDeviceOptions(void)
{
	m_Camera = 0;

	m_IP     = DWORD(MAKELONG(MAKEWORD(248, 200), MAKEWORD(9, 192)));

	m_Port   = 20000;

	m_Time4  = 10;
	}

// Download Support

BOOL CBannerCameraDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Camera));

	Init.AddLong(LONG(m_IP));
	
	Init.AddWord(WORD(m_Port));

	Init.AddWord(WORD(m_Time4));

	return TRUE;
	}

// Persistence

void CBannerCameraDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if(Name == "Device") {

			m_Camera = Tree.GetValueAsInteger();

			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// Meta Data Creation

void CBannerCameraDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Camera);

	Meta_AddInteger(IP);

	Meta_AddInteger(Port);

	Meta_AddInteger(Time4);
	}

//////////////////////////////////////////////////////////////////////////
//
// Banner Camera Driver
//

// Instantiator

ICommsDriver *	Create_BannerCameraDriver(void)
{
	return New CBannerCameraDriver;
	}

// Constructor

CBannerCameraDriver::CBannerCameraDriver(void)
{
	m_wID		= 0x3F01;

	m_uType		= driverCamera;
	
	m_Manufacturer	= "Banner";
	
	m_DriverName	= "Vision Systems";
	
	m_Version	= "1.01";

	m_DevRoot	= "Cam";
	
	m_ShortName	= "Vision";
	}

// Configuration

CLASS CBannerCameraDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CBannerCameraDriverOptions);
	}

CLASS CBannerCameraDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBannerCameraDeviceOptions);
	}

// Binding Control

UINT CBannerCameraDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
