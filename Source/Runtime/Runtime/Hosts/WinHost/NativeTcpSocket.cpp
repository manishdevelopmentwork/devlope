
#include "Intern.hpp"

#include "NativeTcpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Native TCP Socket Object
//

// Libraries

#pragma comment(lib, "Ws2_32.lib")

// Instantiator

static IUnknown * Create_NativeTcpSocket(PCTXT pName)
{
	return (IUnknown *) New CNativeTcpSocket;
}

// Registration

global void Register_NativeTcpSocket(void)
{
	win32::WSADATA Data;

	win32::WSAStartup(MAKEWORD(2, 2), &Data);

	piob->RegisterInstantiator("sock-tcp", Create_NativeTcpSocket);
}

global void Revoke_NativeTcpSocket(void)
{
	piob->RevokeInstantiator("sock-tcp");
}

// Constructor

CNativeTcpSocket::CNativeTcpSocket(void)
{
	StdSetRef();

	m_hSocket = NULL;

	m_hEvent  = win32::CreateEvent(NULL, TRUE, FALSE, NULL);

	m_fDebug  = FALSE;

	m_uState  = stateInit;
}

// Destructor

CNativeTcpSocket::~CNativeTcpSocket(void)
{
	Abort();

	win32::CloseHandle(m_hEvent);
}

// IUnknown

HRESULT CNativeTcpSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
}

ULONG CNativeTcpSocket::AddRef(void)
{
	StdAddRef();
}

ULONG CNativeTcpSocket::Release(void)
{
	StdRelease();
}

// ISocket

HRESULT CNativeTcpSocket::Listen(WORD Loc)
{
	return Listen(IP_EMPTY, Loc);
}

HRESULT CNativeTcpSocket::Listen(IPADDR const &IP, WORD Loc)
{
	if( m_uState == stateInit ) {

		if( (m_hSocket = win32::socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) ) {

			SetNonBlocking();

			AllowDupAddress();

			sockaddr_in loc;

			LoadAddress(loc, IP, Loc);

			if( win32::bind(m_hSocket, (sockaddr *) &loc, sizeof(loc)) == 0 ) {

				win32::listen(m_hSocket, 1);

				m_uState = stateListen;

				return S_OK;
			}

			Close();
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Connect(IPADDR const &IP, WORD Rem)
{
	return Connect(IP, IP_EMPTY, Rem, 0);
}

HRESULT CNativeTcpSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	return Connect(IP, IP_EMPTY, Rem, Loc);
}

HRESULT CNativeTcpSocket::Connect(IPADDR const &IP, IPADDR const &IF, WORD Rem, WORD Loc)
{
	if( m_uState == stateInit ) {

		if( (m_hSocket = win32::socket(AF_INET, SOCK_STREAM, IPPROTO_IP)) ) {

			sockaddr_in rem;

			sockaddr_in loc;

			LoadAddress(rem, IP, Rem);

			if( !LoadAddress(loc, IF, Loc) || win32::bind(m_hSocket, (sockaddr *) &loc, sizeof(loc)) == 0 ) {

				SetNonBlocking();

				int nCode = win32::connect(m_hSocket, (sockaddr *) &rem, sizeof(rem));

				if( nCode == 0 ) {

					win32::WSAEventSelect(m_hSocket, m_hEvent, FD_CLOSE);

					m_uState = stateOpen;

					return S_OK;
				}

				if( win32::WSAGetLastError() == WSAEWOULDBLOCK ) {

					win32::WSAEventSelect(m_hSocket, m_hEvent, FD_CONNECT | FD_CLOSE);

					m_uState = stateConnect;

					return S_OK;
				}
			}

			Close();
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pData, uSize) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( m_uState == stateOpen ) {

		int nRead = 0;

		if( pData ) {

			nRead = win32::recv(m_hSocket, PSTR(pData), uSize, 0);
		}
		else {
			BYTE b = 0;

			nRead  = win32::recv(m_hSocket, PSTR(&b), 1, MSG_PEEK);
		}

		if( nRead > 0 ) {

			Debug("recv", pData, nRead);

			uSize = nRead;

			return S_OK;
		}

		uSize = 0;
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Send(PBYTE pData, UINT &uSize)
{
	if( m_uState == stateOpen ) {

		if( UINT(win32::send(m_hSocket, PSTR(pData), uSize, 0)) == uSize ) {

			Debug("send", pData, uSize);

			return S_OK;
		}

		uSize = 0;
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	// TODO -- Can we use some sort of event here?!!!

	for( ;;) {

		if( Recv(pBuff) == S_OK ) {

			return S_OK;
		}

		if( uTime ) {

			UINT uSleep = min(uTime, 5);

			Sleep(uSleep);

			uTime -= uSleep;

			continue;
		}

		break;
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Recv(CBuffer * &pBuff)
{
	if( m_uState == stateOpen ) {

		pBuff = BuffAllocate(0);

		int   nLimit = pBuff->GetTailSpace();

		PBYTE pData  = pBuff->GetData() + pBuff->GetSize();

		int   nRead  = win32::recv(m_hSocket, PSTR(pData), nLimit, 0);

		if( nRead > 0 ) {

			Debug("recv", pData, nRead);

			pBuff->AddTail(nRead);

			return S_OK;
		}

		BuffRelease(pBuff);

		pBuff = NULL;
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Send(CBuffer *pBuff)
{
	if( m_uState == stateOpen ) {

		PCBYTE pData = PCBYTE(pBuff->GetData());

		UINT   uData = pBuff->GetSize();

		if( UINT(win32::send(m_hSocket, PCTXT(pData), uData, 0)) == uData ) {

			Debug("send", pData, uData);

			BuffRelease(pBuff);

			return S_OK;
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::GetLocal(IPADDR &IP)
{
	if( m_hSocket ) {

		sockaddr_in loc;

		int size = sizeof(loc);

		if( win32::getsockname(m_hSocket, (sockaddr *) &loc, &size) == 0 ) {

			if( loc.sin_family == AF_INET ) {

				IP.m_dw = loc.sin_addr.S_un.S_addr;

				return S_OK;
			}
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::GetRemote(IPADDR &IP)
{
	if( m_hSocket ) {

		sockaddr_in rem;

		int size = sizeof(rem);

		if( win32::getpeername(m_hSocket, (sockaddr *) &rem, &size) == 0 ) {

			if( rem.sin_family == AF_INET ) {

				IP.m_dw = rem.sin_addr.S_un.S_addr;

				return S_OK;
			}
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	if( m_hSocket ) {

		sockaddr_in loc;

		int size = sizeof(loc);

		if( win32::getsockname(m_hSocket, (sockaddr *) &loc, &size) == 0 ) {

			if( loc.sin_family == AF_INET ) {

				IP.m_dw = loc.sin_addr.S_un.S_addr;

				Port    = win32::ntohs(loc.sin_port);

				return S_OK;
			}
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	if( m_hSocket ) {

		sockaddr_in rem;

		int size = sizeof(rem);

		if( win32::getpeername(m_hSocket, (sockaddr *) &rem, &size) == 0 ) {

			if( rem.sin_family == AF_INET ) {

				IP.m_dw = rem.sin_addr.S_un.S_addr;

				Port    = win32::ntohs(rem.sin_port);

				return S_OK;
			}
		}
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::GetPhase(UINT &Phase)
{
	if( m_uState == stateInit ) {

		Phase = PHASE_IDLE;

		return S_OK;
	}

	if( m_uState == stateListen ) {

		SOCKET hSocket;

		if( (hSocket = win32::accept(m_hSocket, NULL, 0)) != INVALID_SOCKET ) {

			win32::closesocket(m_hSocket);

			m_hSocket = hSocket;

			SetNonBlocking();

			win32::WSAEventSelect(m_hSocket, m_hEvent, FD_CLOSE);

			m_uState = stateOpen;

			Phase    = PHASE_OPEN;

			return S_OK;
		}

		Phase = PHASE_IDLE;

		return S_OK;
	}

	if( m_uState == stateConnect ) {

		if( win32::WaitForSingleObject(m_hEvent, 0) == WAIT_OBJECT_0 ) {

			win32::WSANETWORKEVENTS Events;

			memset(&Events, 0xFF, sizeof(Events));

			win32::WSAEnumNetworkEvents(m_hSocket, m_hEvent, &Events);

			if( Events.iErrorCode[FD_CLOSE_BIT] == -1 ) {

				if( Events.iErrorCode[FD_CONNECT_BIT] == 0 ) {

					m_uState = stateOpen;

					Phase    = PHASE_OPEN;

					return S_OK;
				}
			}

			Abort();

			Phase = PHASE_ERROR;

			return S_OK;
		}

		Phase = PHASE_OPENING;

		return S_OK;
	}

	if( m_uState == stateOpen ) {

		if( CheckRemoteClose() ) {

			Phase = PHASE_ERROR;

			return S_OK;
		}

		Phase = PHASE_OPEN;

		return S_OK;
	}

	if( m_uState == stateClosing ) {

		if( CheckRemoteClose() ) {

			Phase = PHASE_ERROR;

			return S_OK;
		}

		Phase = PHASE_CLOSING;

		return S_OK;
	}

	if( m_uState == stateError ) {

		Phase = PHASE_ERROR;

		return S_OK;
	}

	AfxAssert(FALSE);

	return E_FAIL;
}

HRESULT CNativeTcpSocket::SetOption(UINT uOption, UINT uValue)
{
	if( uOption == OPT_LINGER ) {

		ULONG uEnable = uValue ? TRUE : FALSE;

		win32::setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, PCTXT(&uEnable), sizeof(uEnable));

		return S_OK;
	}

	if( uOption == OPT_KEEP_ALIVE ) {

		ULONG uEnable = uValue ? TRUE : FALSE;

		win32::setsockopt(m_hSocket, SOL_SOCKET, SO_KEEPALIVE, PCTXT(&uEnable), sizeof(uEnable));

		return S_OK;
	}

	if( uOption == OPT_NAGLE ) {

		ULONG uEnable = uValue ? FALSE : TRUE;

		win32::setsockopt(m_hSocket, win32::IPPROTO_TCP, TCP_NODELAY, PCTXT(&uEnable), sizeof(uEnable));

		return S_OK;
	}

	if( uOption == OPT_DEBUG ) {

		m_fDebug = uValue ? TRUE : FALSE;

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Abort(void)
{
	if( m_uState < stateError ) {

		if( m_uState >= stateOpen ) {

			if( CheckRemoteClose() ) {

				return S_OK;
			}
		}


		SetAbortMode();

		CloseHandle();

		return S_OK;
	}

	return E_FAIL;
}

HRESULT CNativeTcpSocket::Close(void)
{
	if( m_uState <= stateConnect ) {

		return Abort();
	}

	if( m_uState <= stateOpen ) {

		if( CheckRemoteClose() ) {

			return S_OK;
		}

		win32::shutdown(m_hSocket, SD_SEND);

		m_uState = stateClosing;

		return S_OK;
	}

	return E_FAIL;
}

// Implementation

void CNativeTcpSocket::SetNonBlocking(void)
{
	ULONG uEnable = TRUE;

	win32::ioctlsocket(m_hSocket, FIONBIO, &uEnable);
}

void CNativeTcpSocket::AllowDupAddress(void)
{
	ULONG uEnable = TRUE;

	win32::setsockopt(m_hSocket, SOL_SOCKET, SO_REUSEADDR, PCTXT(&uEnable), sizeof(uEnable));
}

BOOL CNativeTcpSocket::LoadAddress(sockaddr_in &addr, IPADDR const &IP, WORD Port)
{
	memset(&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;

	if( IP.m_dw || Port ) {

		addr.sin_port = win32::htons(Port);

		addr.sin_addr.S_un.S_addr = IP.m_dw;

		return TRUE;
	}

	return FALSE;
}

BOOL CNativeTcpSocket::LoadAddress(sockaddr_in &addr, WORD Port)
{
	memset(&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;

	if( Port ) {

		addr.sin_port = win32::htons(Port);

		return TRUE;
	}

	return FALSE;
}

BOOL CNativeTcpSocket::CheckRemoteClose(void)
{
	if( win32::WaitForSingleObject(m_hEvent, 0) == WAIT_OBJECT_0 ) {

		win32::WSANETWORKEVENTS Events;

		memset(&Events, 0xFF, sizeof(Events));

		win32::WSAEnumNetworkEvents(m_hSocket, m_hEvent, &Events);

		CloseHandle();

		return TRUE;
	}

	return FALSE;
}

void CNativeTcpSocket::SetAbortMode(void)
{
	struct win32::linger linger;

	linger.l_onoff  = 1;

	linger.l_linger = 0;

	win32::setsockopt(m_hSocket, SOL_SOCKET, SO_LINGER, PCTXT(&linger), sizeof(linger));
}

void CNativeTcpSocket::CloseHandle(void)
{
	win32::closesocket(m_hSocket);

	m_hSocket = NULL;

	m_uState  = stateError;
}

void CNativeTcpSocket::Debug(PCTXT pType, PCBYTE pData, UINT uSize)
{
	#if defined(_DEBUG)

	if( m_fDebug ) {

		CIpAddr LocIp, RemIp;

		WORD    LocPort, RemPort;

		GetLocal(LocIp, LocPort);

		GetRemote(RemIp, RemPort);

		AfxTrace("Local=%s:%u Remote=%s:%u Type=%s\n",
			 PCTXT(LocIp.GetAsText()),
			 LocPort,
			 PCTXT(RemIp.GetAsText()),
			 RemPort,
			 pType
		);

		if( uSize ) {

			AfxDump(pData, uSize);
		}
	}

	#endif
}

// End of File
