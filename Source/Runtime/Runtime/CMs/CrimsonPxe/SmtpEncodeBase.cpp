
#include "intern.hpp"

#include "SmtpEncodeBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SmtpDefs.hpp"

#include "StdEndpoint.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Basic Mail Enoder
//

// Constructor

CSmtpEncodeBase::CSmtpEncodeBase(CStdEndpoint *pEndpoint)
{
	m_pEndpoint = pEndpoint;
}

// Operations

BOOL CSmtpEncodeBase::Encode(CMsgPayload const *pMessage)
{
	return	SendHead(pMessage) &&
		SendBody(pMessage->m_Message);
}

// Implementation

BOOL CSmtpEncodeBase::SendHead(CMsgPayload const *pMessage)
{
	return	SendFrom(pMessage->m_OrigName, pMessage->m_OrigAddr) &&
		SendTo(pMessage->m_RcptName, pMessage->m_RcptAddr) &&
		SendDate() &&
		SendSubj(pMessage->m_Subject);
}

BOOL CSmtpEncodeBase::SendFrom(CString const &Name, CString const &Addr)
{
	CString s;

	s += "From: ";

	s += GetAddr(Name, Addr);

	s += "\r\n";

	return Send(s);
}

BOOL CSmtpEncodeBase::SendTo(CString const &Name, CString const &Addr)
{
	CString s;

	s += "To: ";

	s += GetAddr(Name, Addr);

	s += "\r\n";

	return Send(s);
}

BOOL CSmtpEncodeBase::SendDate(void)
{
	CString s;

	s += "Date: ";

	s += GetDate();

	s += "\r\n";

	return Send(s);
}

BOOL CSmtpEncodeBase::SendSubj(PCTXT pText)
{
	CString s;

	s += "Subject: ";

	s += pText;

	s += "\r\n";

	return Send(s);
}

BOOL CSmtpEncodeBase::SendBody(PCTXT pText)
{
	CString s;

	s += "\r\n";

	s += pText;

	s += "\r\n";

	return Send(s);
}

// Formatting

CString CSmtpEncodeBase::GetAddr(CString const &Name, CString const &Addr)
{
	CString Text;

	if( !Name.IsEmpty() ) {

		Text += Name;
	}

	if( !Addr.IsEmpty() ) {

		if( !Text.IsEmpty() ) {

			Text += " ";
		}

		Text += '<';

		Text += Addr;

		Text += '>';
	}

	return Text.IsEmpty() ? "<>" : Text;
}

CString CSmtpEncodeBase::GetDate(void)
{
	CString Date;

	time_t     tt = time(NULL);

	struct tm *tm = gmtime(&tt);

	Date.Printf("%s, %.2u %s %.4u %2.2u:%2.2u +0000",
		    PCTXT(GetDayName(tm->tm_wday)),
		    tm->tm_mday,
		    PCTXT(GetMonthName(tm->tm_mon + 1)),
		    tm->tm_year + 1900,
		    tm->tm_hour,
		    tm->tm_min
	);

	return Date;
}

PCTXT CSmtpEncodeBase::GetDayName(UINT d)
{
	switch( d ) {

		case 0: return "Sun";
		case 1: return "Mon";
		case 2: return "Tue";
		case 3: return "Wed";
		case 4: return "Thu";
		case 5: return "Fri";
		case 6: return "Sat";
		case 7: return "Sun";
	}

	return "<day>";
}

PCTXT CSmtpEncodeBase::GetMonthName(UINT m)
{
	switch( m ) {

		case 1:  return "Jan";
		case 2:  return "Feb";
		case 3:  return "Mar";
		case 4:  return "Apr";
		case 5:  return "May";
		case 6:  return "Jun";
		case 7:  return "Jul";
		case 8:  return "Aug";
		case 9:  return "Sep";
		case 10: return "Oct";
		case 11: return "Nov";
		case 12: return "Dec";
	}

	return "<mon>";
}

// Transport

BOOL CSmtpEncodeBase::Send(PCTXT pText)
{
	m_pEndpoint->Send(pText);

	return TRUE;
}

// End of File
