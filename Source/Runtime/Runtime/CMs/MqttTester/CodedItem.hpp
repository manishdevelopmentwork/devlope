
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CodedItem_HPP

#define INCLUDE_CodedItem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "CrimsonDefs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Coded Item
//

class CCodedItem
{
	public:
		// Operations
		void SetScan (UINT uScan);
		void SetValue(DWORD Data, UINT Type, UINT uFlags);

		// Data Members
		DWORD m_Data;
	};

// End of File

#endif
