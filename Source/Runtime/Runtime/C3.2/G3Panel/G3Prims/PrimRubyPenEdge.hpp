
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPenEdge_HPP
	
#define	INCLUDE_PrimRubyPenEdge_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPenBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

class DLLAPI CPrimRubyPenEdge : public CPrimRubyPenBase
{
	public:
		// Constructor
		CPrimRubyPenEdge(void);

		// Destructor
		~CPrimRubyPenEdge(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Attributes
		BOOL IsNull(void) const;
		int  GetInnerWidth(void) const;
		int  GetOuterWidth(void) const;

		// Operations
		void SetScan(UINT Code);
		BOOL DrawPrep(IGDI *pGDI);
		void AdjustRect(R2 &r, UINT m);
		void AdjustRect(R2 &r);

		// Stroking
		BOOL StrokeTrim(CRubyPath &output, CRubyPath const &figure);

		// Drawing
		BOOL Trim(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver);

		// Data Members
		UINT         m_TrimWidth;
		UINT         m_TrimMode;
		CPrimColor * m_pTrimColor;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			COLOR  m_TrimColor;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

// End of File

#endif
