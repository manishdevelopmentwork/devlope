
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbNetworkCdcExpansion_HPP

#define	INCLUDE_UsbNetworkCdcExpansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Network Cdc Expansion Object
//

class CUsbNetworkCdcExpansion : public CUsbExpansion
{
	public:
		// Constructor
		CUsbNetworkCdcExpansion(IUsbHostFuncDriver *pDriver);

		// IExpansionInterface
		PCTXT	  METHOD GetName(void);
		UINT	  METHOD GetClass(void);
		IDevice * METHOD MakeObject(IUsbHostFuncDriver *pDrv);
	};

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IDevice * Create_UsbNetworkCdc(IUsbHostFuncDriver *pDriver);

// End of File

#endif


