
#include "Intern.hpp"

#include "DevCon.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConPart.hpp"

#include "DevConPartHardware.hpp"

#include "DevConWnd.hpp"

#include "HttpBase64.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration
//

// Base Class

#define CBaseClass CMetaItem

// Externals

DLLAPI IPxeModel         * Create_LinuxModelData(CString const &Model);

DLLAPI ISchemaGenerator  * Create_LinuxDaxSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel);

DLLAPI IConfigApplicator * Create_LinuxDaxConfigApplicator(CString Model, IPxeModel *pModel);

DLLAPI IPxeModel         * Create_RlosModelData(CString const &Model);

DLLAPI ISchemaGenerator  * Create_RlosSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel);

DLLAPI IConfigApplicator * Create_RlosConfigApplicator(CString Model, IPxeModel *pModel);

// Dynamic Class

AfxImplementDynamicClass(CDevCon, CBaseClass);

// Constructor

CDevCon::CDevCon(void)
{
	m_pHConfig = New CDevConPartHardware;

	m_pPConfig = New CDevConPart('p');

	m_pSConfig = New CDevConPart('s');

	m_pUConfig = New CDevConPart('u');
	
	m_pModel   = NULL;
	
	m_pSchema  = NULL;

	m_pApply   = NULL;

	m_pUpdate  = NULL;

	StdSetRef();
}

// Destructor

CDevCon::~CDevCon(void)
{
	AfxRelease(m_pModel);

	AfxRelease(m_pSchema);

	AfxRelease(m_pApply);
}

// Operations

void CDevCon::FinalizeImport(void)
{
	m_pHConfig->ImportFromJson();

	m_pUpdate->OnConfigUpdate('h');

	m_pSConfig->UpdateSchema();
}

void CDevCon::UpdateHardware(void)
{
	m_pUpdate->OnConfigUpdate('h');

	m_pSConfig->ImportFromText();

	m_pSConfig->UpdateSchema();

	afxMainWnd->PostMessage(WM_COMMAND, 0x8164);
}

UINT CDevCon::GetSoftwareGroup(void)
{
	return m_pHConfig->GetSoftwareGroup();
}

void CDevCon::SetSoftwareGroup(UINT uGroup)
{
	m_pHConfig->SetSoftwareGroup(uGroup);
}

BOOL CDevCon::HasExpansion(void)
{
	if( GetSoftwareGroup() >= SW_GROUP_2 ) {

		CJsonData *pActive = m_pHConfig->GetActive();

		return m_pApply->HasModules(pActive);
	}

	return FALSE;
}

CSize CDevCon::GetDisplaySize(void)
{
	CJsonData *pActive = m_pHConfig->GetActive();

	DWORD dwSize = watoi(pActive->GetValue(L"general.format", L"0"));

	return dwSize ? CSize(dwSize) : CSize(640, 480);
}

void CDevCon::ApplyModelSpec(void)
{
	CommitEdits();

	AfxRelease(m_pModel);

	AfxRelease(m_pSchema);

	AfxRelease(m_pApply);

	FindObjects();

	CJsonData *pConfig = m_pSConfig->GetConfig();

	CString    SConfig = pConfig->GetAsText(FALSE);

	FindSchemas();

	m_pHConfig->UpdateSchema();

	m_pModel->AdjustHardware(m_pHConfig->GetConfig());

	m_pSConfig->m_Config = SConfig;

	m_pHConfig->ImportFromJson();

	m_pSConfig->ImportFromText();

	UpdateHardware();
}

void CDevCon::AppendModelSpec(CString &Model)
{
	m_pModel->AppendModelSpec(Model, m_pHConfig->GetActive());
}

void CDevCon::GetPorts(CStringArray &List)
{
	m_pApply->GetPorts(List, m_pHConfig->GetActive());
}

void CDevCon::GetModules(CStringArray &List)
{
	m_pApply->GetModules(List, m_pHConfig->GetActive());
}

void CDevCon::GetCertList(CUIntArray &Data, CStringArray &Text, BOOL fClient)
{
	CJsonData *pConfig = m_pSConfig->GetConfig();

	CJsonData *pList   = pConfig->GetChild(fClient ? L"security.client.certs" : L"security.server.certs");

	if( pList ) {

		UINT c = pList->GetCount();

		for( UINT n = 0; n < c; n++ ) {

			CJsonData *pCert = pList->GetChild(n);

			if( pCert ) {

				UINT uKey = watoi(pCert->GetValue(L"key", L"0"));

				if( uKey >= 100 ) {

					Data.Append(uKey);

					Text.Append(pCert->GetValue(L"name", L"Untitled"));
				}
			}
		}
	}
}

UINT CDevCon::ImportCert(CString const &Base, CByteArray const &Cert, CByteArray const &Priv)
{
	CJsonData *pConfig = m_pSConfig->GetConfig();

	CJsonData *pList   = pConfig->GetChild(L"security.client.certs");

	if( !pList ) {

		pConfig->AddChild(L"security.client.certs", TRUE, pList);
	}

	if( pList ) {

		UINT c = pList->GetCount();

		UINT m = 100;

		for( UINT n = 0; n < c; n++ ) {

			CJsonData *pCert = pList->GetChild(n);

			if( pCert ) {

				UINT uKey = watoi(pCert->GetValue(L"key", L"0"));

				MakeMax(m, uKey+1);
			}
		}

		CJsonData *pCert;

		CPrintf Name(L"%s-C%u", PCTXT(Base), m);

		pList->AddChild(FALSE, pCert);

		pCert->AddValue(L"key", CPrintf(L"%u", m), jsonString);

		pCert->AddValue(L"name", Name);

		AddFile(pCert, L"cert", Name + L".crt", Cert, L"application/x-x509-ca-cert");

		AddFile(pCert, L"priv", Name + L".key", Priv, L"application/octet-stream");

		return m;
	}

	return 0;
}

UINT CDevCon::ImportCert(CString const &Base, CByteArray const &Cert)
{
	CJsonData *pConfig = m_pSConfig->GetConfig();

	CJsonData *pList   = pConfig->GetChild(L"security.server.certs");

	if( !pList ) {

		pConfig->AddChild(L"security.server.certs", TRUE, pList);
	}

	if( pList ) {

		UINT c = pList->GetCount();

		UINT m = 100;

		for( UINT n = 0; n < c; n++ ) {

			CJsonData *pCert = pList->GetChild(n);

			if( pCert ) {

				UINT uKey = watoi(pCert->GetValue(L"key", L"0"));

				MakeMax(m, uKey+1);
			}
		}

		CJsonData *pCert;

		CPrintf Name(L"%s-S%u", PCTXT(Base), m);

		pList->AddChild(FALSE, pCert);

		pCert->AddValue(L"key", CPrintf(L"%u", m), jsonString);

		pCert->AddValue(L"name", Name);

		AddFile(pCert, L"data", Name + L".crt", Cert, L"application/x-x509-ca-cert");

		return m;
	}

	return 0;
}

// IUnknown

HRESULT CDevCon::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IConfigStorage);

	StdQueryInterface(IConfigStorage);

	return E_NOINTERFACE;
}

ULONG CDevCon::AddRef(void)
{
	StdAddRef();
}

ULONG CDevCon::Release(void)
{
	StdRelease();
}

// IConfigStorage

bool CDevCon::AddUpdateSink(IConfigUpdate *pUpdate)
{
	m_pUpdate = pUpdate;

	return true;
}

bool CDevCon::SetConfig(char cTag, CString const &Text, bool fEdit)
{
	switch( cTag ) {

		// TODO -- Not sure about this re hardware update!!!

		case 'h':
			m_pHConfig->m_Active = m_pHConfig->m_Config = Text;
			return true;

		case 's':
			m_pSConfig->m_Config = Text;
			return true;

		case 'p':
			m_pPConfig->m_Config = Text;
			return true;

		case 'u':
			m_pUConfig->m_Config = Text;
			return true;
	}

	AfxAssert(FALSE);

	return false;
}

bool CDevCon::GetConfig(char cTag, CString &Text)
{
	switch( cTag ) {

		case 'h':
			Text = m_pHConfig->m_Active;
			return true;

		case 's':
			Text = m_pSConfig->m_Config;
			return true;

		case 'p':
			Text = m_pPConfig->m_Config;
			return true;

		case 'u':
			Text = m_pUConfig->m_Config;
			return true;

	}

	AfxAssert(FALSE);

	return false;
}

// UI Creation

CViewWnd * CDevCon::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CDevConNavWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	if( uType == viewResource ) {

		CLASS Class = AfxNamedClass(L"CDevConResWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	if( uType == viewItem ) {

		CUIViewWnd *pView = New CDevConWnd;

		pView->SetBorder(6);

		return pView;
	}

	return CBaseClass::CreateView(uType);
}

// Item Naming

CString CDevCon::GetHumanName(void) const
{
	return IDS("Device Configuration");
}

// Persistance

void CDevCon::Init(void)
{
	FindObjects();

	CBaseClass::Init();

	FindSchemas();
}

void CDevCon::PostLoad(void)
{
	FindObjects();

	CBaseClass::PostLoad();

	FindSchemas();
}

// Meta Data Creation

void CDevCon::AddMetaData(void)
{
	CBaseClass::AddMetaData();

	Meta_AddString(Model);
	Meta_AddObject(HConfig);
	Meta_AddObject(PConfig);
	Meta_AddObject(SConfig);
	Meta_AddObject(UConfig);

	Meta_SetName(IDS("Device Configuration"));
}

// Implementation

void CDevCon::FindObjects(void)
{
	CString Base = m_Model.TokenLeft('|');

	if( Base == L"da50" || Base == L"da70" ) {

		m_pModel  = Create_LinuxModelData(m_Model);

		m_pSchema = Create_LinuxDaxSchemaGenerator(this, m_pModel);

		m_pApply  = Create_LinuxDaxConfigApplicator(m_Model, m_pModel);
	}
	else {
		m_pModel  = Create_RlosModelData(m_Model);

		m_pSchema = Create_RlosSchemaGenerator(this, m_pModel);

		m_pApply  = Create_RlosConfigApplicator(m_Model, m_pModel);
	}
}

void CDevCon::FindSchemas(void)
{
	m_pSchema->Open();

	m_pHConfig->FindSchema();
	m_pPConfig->FindSchema();
	m_pSConfig->FindSchema();
	m_pUConfig->FindSchema();
}

void CDevCon::CommitEdits(void)
{
	m_pHConfig->ImportFromJson();

	m_pPConfig->CommitEdits();
	m_pSConfig->CommitEdits();
	m_pUConfig->CommitEdits();
}

BOOL CDevCon::AddFile(CJsonData *pJson, PCTXT pKey, PCTXT pName, CByteArray const &Data, PCTXT pType)
{
	UINT uCode = CHttpBase64::GetEncodeSize(Data.GetCount());

	PSTR pCode = New char[uCode+1];

	CHttpBase64::Encode(pCode, Data.GetPointer(), Data.GetCount(), FALSE);

	CPrintf Text(L"%s|data:%s;base64,%s", pName, pType, CString(pCode));

	pJson->AddValue(pKey, Text);

	delete[] pCode;

	return TRUE;
}

// End of File
