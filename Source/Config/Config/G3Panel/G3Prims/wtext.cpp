
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive with Text
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWithText, CPrim);

// Constructor

CPrimWithText::CPrimWithText(void)
{
	m_pTextItem = NULL;

	m_pDataItem = NULL;

	m_pAction   = NULL;

	m_uActMode  = actOptional;
	}

// Destructor

CPrimWithText::~CPrimWithText(void)
{
	}

// UI Overridables

BOOL CPrimWithText::OnLoadPages(CUIPageList *pList)
{
	if( m_pTextItem ) {

		m_pTextItem->OnLoadPages(pList);
		}

	if( m_pDataItem ) {

		m_pDataItem->OnLoadPages(pList);
		}

	if( m_pAction ) {

		m_pAction->OnLoadPages(pList);
		}

	return TRUE;
	}

// Attributes

CRect CPrimWithText::GetTextRect(void) const
{
	CRect Rect;

	Rect.left   = m_TextRect.x1;
	Rect.top    = m_TextRect.y1;
	Rect.right  = m_TextRect.x2;
	Rect.bottom = m_TextRect.y2;

	return Rect;
	}

BOOL CPrimWithText::HasText(void) const
{
	return m_pTextItem ? TRUE : FALSE;
	}

BOOL CPrimWithText::HasData(void) const
{
	return m_pDataItem ? TRUE : FALSE;
	}

BOOL CPrimWithText::HasAction(void) const
{
	return m_pAction ? TRUE : FALSE;
	}

BOOL CPrimWithText::IsTextEditable(void) const
{
	return m_pTextItem && m_pTextItem->IsEditable();
	}

BOOL CPrimWithText::IsActionNull(void) const
{
	return m_pAction && m_pAction->IsNull();
	}

UINT CPrimWithText::GetActionMode(void) const
{
	return m_uActMode;
	}

int CPrimWithText::GetTripSize(void) const
{
	if( m_pDataItem ) {

		int nSize = m_pDataItem->GetFontSize(1);

		return nSize;
		}

	if( m_pTextItem ) {

		int nSize = m_pTextItem->GetFontSize(1);

		nSize += m_pTextItem->m_Lead;

		return nSize;
		}

	return 18;
	}

// Operations

BOOL CPrimWithText::AddText(void)
{
	if( !m_pTextItem ) {
		
		m_pTextItem = New CPrimText;

		m_pTextItem->SetParent(this);

		m_pTextItem->Init();

		RemData();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWithText::AddData(void)
{
	if( !m_pDataItem ) {
		
		m_pDataItem = New CPrimData;

		m_pDataItem->SetParent(this);

		m_pDataItem->Init();

		RemText();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWithText::AddAction(void)
{
	if( !m_pAction ) {

		m_pAction = New CPrimAction;

		m_pAction->SetParent(this);

		m_pAction->Init();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWithText::RemText(void)
{
	if( m_pTextItem ) {

		m_pTextItem->Kill();
		
		delete m_pTextItem;

		m_pTextItem = NULL;

		FindTextRect();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWithText::RemData(void)
{
	if( m_pDataItem ) {

		m_pDataItem->Kill();
		
		delete m_pDataItem;

		m_pDataItem = NULL;

		FindTextRect();

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimWithText::RemAction(void)
{
	if( m_pAction ) {

		m_pAction->Kill();
		
		delete m_pAction;

		m_pAction = NULL;

		return TRUE;
		}

	return FALSE;
	}

// Overridables

void CPrimWithText::Draw(IGDI *pGDI, UINT uMode)
{
	if( uMode != drawNoText ) {

		if( m_pTextItem ) {

			m_pTextItem->Draw(pGDI, m_TextRect, uMode);
			}

		if( m_pDataItem ) {

			m_pDataItem->Draw(pGDI, m_TextRect, uMode);
			}
		}
	}

void CPrimWithText::FindTextRect(void)
{
	m_TextRect.x1 = m_DrawRect.x1;
	
	m_TextRect.y1 = m_DrawRect.y1;
	
	m_TextRect.x2 = m_DrawRect.x2;
	
	m_TextRect.y2 = m_DrawRect.y2;
	}

void CPrimWithText::GetRefs(CPrimRefList &Refs)
{
	if( m_pTextItem ) {

		m_pTextItem->GetRefs(Refs);
		}

	if( m_pDataItem ) {

		m_pDataItem->GetRefs(Refs);
		}
	}

void CPrimWithText::EditRef(UINT uOld, UINT uNew)
{
	if( m_pTextItem ) {

		m_pTextItem->EditRef(uOld, uNew);
		}

	if( m_pDataItem ) {

		m_pDataItem->EditRef(uOld, uNew);
		}
	}

void CPrimWithText::Validate(BOOL fExpand)
{
	if( m_pDataItem ) {

		m_pDataItem->Validate(fExpand);
		}

	if( m_pAction ) {

		m_pAction->Validate(fExpand);
		}
	}

void CPrimWithText::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	if( m_pDataItem ) {

		m_pDataItem->TagCheck(Done, Tags);
		}
	}

void CPrimWithText::SetTextColor(COLOR Color)
{
	if( m_pTextItem ) {

		m_pTextItem->SetTextColor(Color);
		}

	if( m_pDataItem ) {

		m_pDataItem->SetTextColor(Color);
		}
	}

BOOL CPrimWithText::StepAddress(void)
{
	BOOL f1 = m_pDataItem && m_pDataItem->StepAddress();

	BOOL f2 = m_pTextItem && m_pTextItem->StepAddress();

	BOOL f3 = m_pAction   && m_pAction  ->StepAddress();

	return f1 || f2 || f3;
	}

BOOL CPrimWithText::HasError(void)
{
	if( m_pDataItem ) {

		return m_pDataItem->HasError();
		}

	if( m_pTextItem ) {

		return m_pTextItem->HasError();
		}

	if( m_pAction ) {

		return m_pAction->IsBroken();
		}

	return CPrim::HasError();
	}

// Persistance

void CPrimWithText::Init(void)
{
	if( m_uActMode == actAlways ) {

		AddAction();
		}

	CPrim::Init();
	}

void CPrimWithText::PostLoad(void)
{
	if( m_uActMode == actAlways ) {

		AddAction();
		}

	CPrim::PostLoad();
	}

// Download Support

BOOL CPrimWithText::MakeInitData(CInitData &Init)
{
	CPrim::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pTextItem);

	Init.AddItem(itemVirtual, m_pDataItem);

	Init.AddItem(itemVirtual, m_pAction);

	if( m_pTextItem || m_pDataItem ) {

		Init.AddWord(WORD(m_TextRect.x1));
		Init.AddWord(WORD(m_TextRect.y1));
		Init.AddWord(WORD(m_TextRect.x2));
		Init.AddWord(WORD(m_TextRect.y2));
		}

	return TRUE;
	}

// Meta Data

void CPrimWithText::AddMetaData(void)
{
	CPrim::AddMetaData();

	Meta_AddRect   (TextRect);
	Meta_AddVirtual(TextItem);
	Meta_AddVirtual(DataItem);
	Meta_AddVirtual(Action);

	Meta_SetName((IDS_PRIMITIVE_WITH));
	}

// Implementation

void CPrimWithText::LoadHeadPages(CUIPageList *pList)
{
	if( m_pDataItem ) {

		m_pDataItem->OnLoadPages(pList);
		}

	if( m_pTextItem ) {

		m_pTextItem->OnLoadPages(pList);
		}
	}

void CPrimWithText::LoadTailPages(CUIPageList *pList)
{
	if( m_pAction ) {

		m_pAction->OnLoadPages(pList);
		}
	}

// End of File
