
#include "intern.hpp"

#include "bwcan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw CAN Port Driver
//

// Import

#include "..\rawcan\rawcan.cpp"

// Instantiator

INSTANTIATE(CBoulderWindPowerRawCANDriver);

// Constructor

CBoulderWindPowerRawCANDriver::CBoulderWindPowerRawCANDriver(void)
{
	m_Ident = DRIVER_ID;
	}

BOOL MCALL CBoulderWindPowerRawCANDriver::MakeRxMailBox(UINT uBox, UINT uMask, UINT uFilter, UINT uDLC)
{
	return m_pHandler->MakeRxMail(uBox, uMask, uFilter, uDLC, m_RxMailMax);
	}

BOOL MCALL CBoulderWindPowerRawCANDriver::MakeTxMailBox(UINT uBox, UINT uId, UINT uDLC)
{
	return m_pHandler->MakeTxMail(uBox, uId, uDLC, m_TxMailMax);
	}

BOOL MCALL CBoulderWindPowerRawCANDriver::RxMail(PTXT Mail)
{
	return m_pHandler->GetRxMail(Mail);
	}

BOOL MCALL CBoulderWindPowerRawCANDriver::TxMailBox(UINT uBox, PDWORD pData)
{
	return m_pHandler->SendMail(uBox, pData);
	}

UINT MCALL CBoulderWindPowerRawCANDriver::GetMailDLC(UINT uBox)
{
	return m_pHandler->GetMailDLC(uBox);
	}

// End of File
