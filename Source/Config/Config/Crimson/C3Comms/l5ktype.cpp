
#include "intern.hpp"

#include "l5kitem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#define END_MARKER	"<end>"

// Raw Atomic Types

CABL5kTypeList::CAtomicEntry CABL5kTypeList::m_AtomicList[] = 

{
	{ "BIT",		addrBitAsBit   },
	{ "BOOL",		addrBitAsBit   },
	{ "BOOL[",		addrBitAsLong },
	{ "SINT",		addrByteAsByte },
	{ "INT",		addrWordAsWord },
	{ "DINT",		addrLongAsLong },
	{ "REAL",		addrRealAsReal },

	};

// Raw Predefined Types

CABL5kTypeList::CPredefinedEntry CABL5kTypeList::m_PredefinedList[] = 

{
/*	{
		"AA",
	{
	{ "EnableIn",			"BOOL" },
	{ "Member",			"DINT[10,20]" },
	{  END_MARKER,			END_MARKER },
	}
	},
*/
	{
		"ALARM",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "HHLimit",			"REAL" },
	{ "HLimit",			"REAL" },
	{ "LLimit",			"REAL" },
	{ "LLLimit",			"REAL" },
	{ "Deadband",			"REAL" },
	{ "ROCPosLimit",		"REAL" },
	{ "ROCNegLimit",		"REAL" },
	{ "ROCPeriod",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "HHAlarm",			"BOOL" },
	{ "HAlarm",			"BOOL" },
	{ "LAlarm",			"BOOL" },
	{ "LLAlarm",			"BOOL" },
	{ "ROCPosAlarm",		"BOOL" },
	{ "ROCNegAlarm",		"BOOL" },
	{ "ROC",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "DeadbandInv",		"BOOL" },
	{ "ROCPosLimitInv",		"BOOL" },
	{ "ROCNegLimitInv",		"BOOL" },
	{ "ROCPeriodInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"AXIS_CONSUMED",
	{
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" }, 
	{ "ConfigFault",		"BOOL" }, 
	{ "AxisStatus",			"DINT" }, 
	{ "ServoActionStatus",		"BOOL" }, 
	{ "DriveEnableStatus",		"BOOL" }, 
	{ "ShutdownStatus",		"BOOL" }, 
	{ "ConfigUpdateInProcess",	"BOOL" }, 
	{ "MotionStatus",		"DINT" }, 
	{ "AccelStatus",		"BOOL" }, 
	{ "DecelStatus",		"BOOL" }, 
	{ "MoveStatus",			"BOOL" }, 
	{ "JogStatus",			"BOOL" }, 
	{ "GearingStatus",		"BOOL" }, 
	{ "HomingStatus",		"BOOL" }, 
	{ "StoppingStatus",		"BOOL" }, 
	{ "AxisHomedStatus",		"BOOL" }, 
	{ "PositionCamStatus",		"BOOL" }, 
	{ "TimeCamStatus",		"BOOL" }, 
	{ "PositionCamPendingStatus",	"BOOL" }, 
	{ "TimeCamPendingStatus",	"BOOL" }, 
	{ "GearingLockStatus",		"BOOL" }, 
	{ "PositionCamLockStatus",	"BOOL" },
	{ "MasterOffsetMoveStatus",	"BOOL" }, 
	{ "AxisEvent",			"DINT" }, 
	{ "WatchEventArmedStatus",	"BOOL" }, 
	{ "WatchEventStatus",		"BOOL" }, 
	{ "RegEvent1ArmedStatus",	"BOOL" }, 
	{ "RegEvent1Status",		"BOOL" }, 
	{ "RegEvent2ArmedStatus",	"BOOL" }, 
	{ "RegEvent2Status",		"BOOL" }, 
	{ "HomeEventArmedStatus",	"BOOL" }, 
	{ "HomeEventStatus",		"BOOL" }, 
	{ "OutputCamStatus",		"DINT" }, 
	{ "OutputCamPendingStatus",	"DINT" }, 
	{ "OutputCamLockStatus",	"DINT" }, 
	{ "OutputCamTransitionStatus",	"DINT" }, 
	{ "ActualPosition",		"REAL" }, 
	{ "StrobeActualPosition",	"REAL" }, 
	{ "StartActualPosition",	"REAL" }, 
	{ "AverageVelocity",		"REAL" }, 
	{ "ActualVelocity",		"REAL" }, 
	{ "ActualAcceleration",		"REAL" }, 
	{ "WatchPosition",		"REAL" },
	{ "Registration1Position",	"REAL" }, 
	{ "Registration2Position",	"REAL" }, 
	{ "Registration1Time",		"DINT" }, 
	{ "Registration2Time",		"DINT" }, 
	{ "InterpolationTime",		"DINT" }, 
	{ "InterpolatedActualPosition",	"REAL" }, 
	{ "MasterOffset",		"REAL" }, 
	{ "StrobeMasterOffset",		"REAL" }, 
	{ "StartMasterOffset",		"REAL" }, 
	{ "CommandPosition",		"REAL" }, 
	{ "StrobeCommandPosition",	"REAL" }, 
	{ "StartCommandPosition",	"REAL" }, 
	{ "CommandVelocity",		"REAL" }, 
	{ "CommandAcceleration",	"REAL" }, 
	{ "InterpolatedCommandPosition","REAL" }, 
	{ "ModuleFaults",		"DINT" }, 
	{ "ControlSyncFault",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"AXIS_GENERIC",
	{
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },
	{ "AxisStatus",			"DINT" },
	{ "ServoActionStatus",		"BOOL" },
	{ "DriveEnableStatus",		"BOOL" },
	{ "ShutdownStatus",		"BOOL" },
	{ "ConfigUpdateInProcess",	"BOOL" },
	{ "InhibitStatus",		"BOOL" },
	{ "MotionStatus",		"BOOL" },
	{ "AccelStatus",		"BOOL" },
	{ "DecelStatus",		"BOOL" },
	{ "MoveStatus",			"BOOL" },
	{ "JogStatus",			"BOOL" },
	{ "GearingStatus",		"BOOL" },
	{ "HomingStatus",		"BOOL" },
	{ "StoppingStatus",		"BOOL" },
	{ "AxisHomedStatus",		"BOOL" },
	{ "PositionCamStatus",		"BOOL" },
	{ "TimeCamStatus",		"BOOL" },
	{ "PositionCamPendingStatus",	"BOOL" },
	{ "TimeCamPendingStatus",	"BOOL" },
	{ "GearingLockStatus",		"BOOL" },
	{ "PositionCamLockStatus",	"BOOL" },
	{ "MasterOffsetMoveStatus",	"BOOL" },
	{ "AxisEvent",			"DINT" },
	{ "WatchEventArmedStatus",	"BOOL" },
	{ "WatchEventStatus",		"BOOL" },
	{ "RegEvent1ArmedStatus",	"BOOL" },
	{ "RegEvent1Status",		"BOOL" },
	{ "RegEvent2ArmedStatus",	"BOOL" },
	{ "RegEvent2Status",		"BOOL" },
	{ "HomeEventArmedStatus",	"BOOL" },
	{ "HomeEventStatus",		"BOOL" },
	{ "OutputCamStatus",		"DINT" },
	{ "OutputCamPendingStatus",	"DINT" },
	{ "OutputCamLockStatus",	"DINT" },
	{ "OutputCamTransitionStatus",	"DINT" },
	{ "ActualPosition",		"REAL" },
	{ "StrobeActualPosition",	"REAL" },
	{ "StartActualPosition",	"REAL" },
	{ "AverageVelocity",		"REAL" },
	{ "ActualVelocity",		"REAL" },
	{ "ActualAcceleration",		"REAL" },
	{ "WatchPosition",		"REAL" },
	{ "Registration1Position",	"REAL" },
	{ "Registration2Position",	"REAL" },
	{ "Registration1Time",		"DINT" },
	{ "Registration2Time",		"DINT" },
	{ "InterpolationTime",		"DINT" },
	{ "InterpolatedActualPosition",	"REAL" },
	{ "MasterOffset",		"REAL" },
	{ "StrobeMasterOffset",		"REAL" },
	{ "StartMasterOffset",		"REAL" },
	{ "CommandPosition",		"REAL" },
	{ "StrobeCommandPosition",	"REAL" },
	{ "StartCommandPosition",	"REAL" },
	{ "CommandVelocity",		"REAL" },
	{ "CommandAcceleration",	"REAL" },
	{ "InterpolatedCommandPosition","REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"AXIS_GENERIC_DRIVE",
	{
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },
	{ "AxisStatus",			"DINT" },
	{ "ServoActionStatus",		"BOOL" },
	{ "DriveEnableStatus",		"BOOL" },
	{ "ShutdownStatus",		"BOOL" },
	{ "ConfigUpdateInProcess",	"BOOL" },
	{ "InhibitStatus",		"BOOL" },
	{ "MotionStatus",		"BOOL" },
	{ "AccelStatus",		"BOOL" },
	{ "DecelStatus",		"BOOL" },
	{ "MoveStatus",			"BOOL" },
	{ "JogStatus",			"BOOL" },
	{ "GearingStatus",		"BOOL" },
	{ "HomingStatus",		"BOOL" },
	{ "StoppingStatus",		"BOOL" },
	{ "AxisHomedStatus",		"BOOL" },
	{ "PositionCamStatus",		"BOOL" },
	{ "TimeCamStatus",		"BOOL" },
	{ "PositionCamPendingStatus",	"BOOL" },
	{ "TimeCamPendingStatus",	"BOOL" },
	{ "GearingLockStatus",		"BOOL" },
	{ "PositionCamLockStatus",	"BOOL" },
	{ "MasterOffsetMoveStatus",	"BOOL" },
	{ "CoordinatedMotionStatus",	"BOOL" },
	{ "AxisEvent",			"DINT" },
	{ "WatchEventArmedStatus",	"BOOL" },
	{ "WatchEventStatus",		"BOOL" },
	{ "RegEvent1ArmedStatus",	"BOOL" },
	{ "RegEvent1Status",		"BOOL" },
	{ "RegEvent2ArmedStatus",	"BOOL" },
	{ "RegEvent2Status",		"BOOL" },
	
	{ "HomeEventArmedStatus",	"BOOL" },
	{ "HomeEventStatus",		"BOOL" },
	{ "OutputCamStatus",		"DINT" },
	{ "OutputCamPendingStatus",	"DINT" },
	{ "OutputCamLockStatus",	"DINT" },
	{ "OutputCamTransitionStatus",	"DINT" },
	{ "ActualPosition",		"REAL" },
	{ "StrobeActualPosition",	"REAL" },
	{ "StartActualPosition",	"REAL" },
	{ "AverageVelocity",		"REAL" },
	{ "ActualVelocity",		"REAL" },
	{ "ActualAcceleration",		"REAL" },
	{ "WatchPosition",		"REAL" },
	{ "Registration1Position",	"REAL" },
	{ "Registration2Position",	"REAL" },
	{ "Registration1Time",		"DINT" },
	{ "Registration2Time",		"DINT" },
	{ "InterpolationTime",		"DINT" },
	{ "InterpolatedActualPosition",	"REAL" },
	{ "MasterOffset",		"REAL" },
	{ "StrobeMasterOffset",		"REAL" },
	{ "StartMasterOffset",		"REAL" },
	{ "CommandPosition",		"REAL" },
	{ "StrobeCommandPosition",	"REAL" },
	{ "StartCommandPosition",	"REAL" },
	{ "CommandVelocity",		"REAL" },
	{ "CommandAcceleration",	"REAL" },
	{ "InterpolatedCommandPosition","REAL" },
	{ "ModuleFaults",		"DINT" },
	{ "ControlSyncFault",		"BOOL" },
	{ "ModuleSyncFault",		"BOOL" },
	{ "TimerEventFault",		"BOOL" },
	{ "ModuleHardwareFault",	"BOOL" },
	{ "SERCOSRingFault",		"BOOL" },
	{ "AttributeErrorCode",		"INT"  },
	{ "AttributeErrorID",		"INT"  },	
	{ "DriveStatus",		"DINT" },
	{ "VelocityLockStatus",		"BOOL" },
	{ "VelocityStandstillStatus",	"BOOL" },
	{ "VelocityThresholdStatus",	"BOOL" },
	{ "TorqueThresholdStatus",	"BOOL" },
	{ "TorqueLimitStatus",		"BOOL" },
	{ "VelocityLimitStatus",	"BOOL" },
	{ "PositionLockStatus",		"BOOL" },
	{ "PowerLimitStatus",		"BOOL" },
	{ "LowVelocityThresholdStatus",	"BOOL" },
	{ "HighVelocityThresholdStatus","BOOL" },
	{ "DriveFault",			"DINT" },
	{ "OverloadFault",		"BOOL" },
	{ "DriveOvertempFault",		"BOOL" },
	{ "MotorOvertempFault",		"BOOL" },
	{ "DriveCoolingFault",		"BOOL" },
	{ "DriveCoolingVoltageFault",	"BOOL" },
	{ "FeedbackFault",		"BOOL" },
	{ "CommutationFault",		"BOOL" },
	{ "DriveOvercurrentFault",	"BOOL" },
	{ "DriveOvervoltageFault",	"BOOL" },
	{ "DriveUndervoltageFault",	"BOOL" },
	{ "PowerPhaseLossFault",	"BOOL" },
	{ "PositionErrorFault",		"BOOL" },
	{ "SERCOSFault",		"BOOL" },
	{ "OvertravelFault",		"BOOL" },
	{ "ManufacturerSpecificFault",	"BOOL" },
	{ "SERCOSErrorCode",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"AXIS_SERVO",
	{
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },
	{ "AxisStatus",			"DINT" },
	{ "ServoActionStatus",		"BOOL" },
	{ "DriveEnableStatus",		"BOOL" },
	{ "ShutdownStatus",		"BOOL" },
	{ "ConfigUpdateInProcess",	"BOOL" },
	{ "MotionStatus",		"DINT" },
	{ "AccelStatus",		"BOOL" },
	{ "DecelStatus",		"BOOL" },
	{ "MoveStatus",			"BOOL" },
	{ "JogStatus",			"BOOL" },
	{ "GearingStatus",		"BOOL" },
	{ "HomingStatus",		"BOOL" },
	{ "StoppingStatus",		"BOOL" },
	{ "AxisHomedStatus",		"BOOL" },
	{ "PositionCamStatus",		"BOOL" },
	{ "TimeCamStatus",		"BOOL" },
	{ "PositionCamPendingStatus",	"BOOL" },
	{ "TimeCamPendingStatus",	"BOOL" },
	{ "GearingLockStatus",		"BOOL" },
	{ "PositionCamLockStatus",	"BOOL" },
	{ "MasterOffsetMoveStatus",	"BOOL" },
	{ "CoordinatedMotionStatus",	"BOOL" },
	{ "AxisEvent",			"DINT" },
	{ "WatchEventArmedStatus",	"BOOL" },
	{ "WatchEventStatus",		"BOOL" },
	{ "RegEvent1ArmedStatus",	"BOOL" },
	{ "RegEvent1Status",		"BOOL" },
	{ "RegEvent2ArmedStatus",	"BOOL" },
	{ "RegEvent2Status",		"BOOL" },
	{ "HomeEventArmedStatus",	"BOOL" },
	{ "HomeEventStatus",		"BOOL" },
	{ "OutputCamStatus",		"DINT" },
	{ "OutputCamPendingStatus",	"DINT" },
	{ "OutputCamLockStatus",	"DINT" },
	{ "OutputCamTransitionStatus",	"DINT" },
	{ "ActualPosition",		"REAL" },
	{ "StrobeActualPosition",	"REAL" },
	{ "StartActualPosition",	"REAL" },
	{ "AverageVelocity",		"REAL" },
	{ "ActualVelocity",		"REAL" },
	{ "ActualAcceleration",		"REAL" },
	{ "WatchPosition",		"REAL" },
	{ "Registration1Position",	"REAL" },
	{ "Registration2Position",	"REAL" },
	{ "Registration1Time",		"DINT" },
	{ "Registration2Time",		"DINT" },
	{ "InterpolationTime",		"DINT" },
	{ "InterpolatedActualPosition",	"REAL" },
	{ "MasterOffset",		"REAL" },
	{ "StrobeMasterOffset",		"REAL" },
	{ "StartMasterOffset",		"REAL" },
	{ "CommandPosition",		"REAL" },
	{ "StrobeCommandPosition",	"REAL" },
	{ "StartCommandPosition",	"REAL" },
	{ "CommandVelocity",		"REAL" },
	{ "CommandAcceleration",	"REAL" },
	{ "InterpolatedCommandPosition","REAL" },
	{ "ServoStatus",		"DINT" },
	{ "ProcessStatus",		"BOOL" },
	{ "OutputLimitStatus",		"BOOL" },
	{ "PositionLockStatus",		"BOOL" },
	{ "HomeInputStatus",		"BOOL" },
	{ "Reg1InputStatus",		"BOOL" },
	{ "Reg2InputStatus",		"BOOL" },
	{ "DriveFaultInputStatus",	"BOOL" },
	{ "ServoFault",			"DINT" },
	{ "PosSoftOvertravelFault",	"BOOL" },
	{ "NegSoftOvertravelFault",	"BOOL" },
	{ "FeedbackFault",		"BOOL" },
	{ "FeedbackNoiseFault",		"BOOL" },
	{ "PositionErrorFault",		"BOOL" },
	{ "DriveFault",			"BOOL" },
	{ "ModuleFaults",		"DINT" },
	{ "ControlSyncFault",		"BOOL" },
	{ "ModuleSyncFault",		"BOOL" },
	{ "TimerEventFault",		"BOOL" },
	{ "ModuleHardwareFault",	"BOOL" },
	{ "InterModuleSyncFault",	"BOOL" },
	{ "AttributeErrorCode",		"INT"  },
	{ "AttributeErrorID",		"INT"  },
	{ "PositionCommand",		"REAL" },
	{ "PositionFeedback",		"REAL" },
	{ "AuxPositionFeedback",	"REAL" },
	{ "PositionError",		"REAL" },
	{ "PositionIntegratorError",	"REAL" },
	{ "VelocityCommand",		"REAL" },
	{ "VelocityFeedback",		"REAL" },
	{ "VelocityError",		"REAL" },
	{ "VelocityIntegratorError",	"REAL" },
	{ "AccelerationCommand",	"REAL" },
	{ "AccelerationFeedback",	"REAL" },
	{ "ServoOutputLevel",		"REAL" },
	{ "MarkerDistance",		"REAL" },
	{ "VelocityOffset",		"REAL" },
	{ "TorqueOffset",		"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"AXIS_SERVO_DRIVE",
	{
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },
	{ "AxisStatus",			"DINT" },
	{ "ServoActionStatus",		"BOOL" },
	{ "DriveEnableStatus",		"BOOL" },
	{ "ShutdownStatus",		"BOOL" },
	{ "ConfigUpdateInProcess",	"BOOL" },
	{ "MotionStatus",		"DINT" },
	{ "AccelStatus",		"BOOL" },
	{ "DecelStatus",		"BOOL" },
	{ "MoveStatus",			"BOOL" },
	{ "JogStatus",			"BOOL" },
	{ "GearingStatus",		"BOOL" },
	{ "HomingStatus",		"BOOL" },
	{ "StoppingStatus",		"BOOL" },
	{ "AxisHomedStatus",		"BOOL" },
	{ "PositionCamStatus",		"BOOL" },
	{ "TimeCamStatus",		"BOOL" },
	{ "PositionCamPendingStatus",	"BOOL" },
	{ "TimeCamPendingStatus",	"BOOL" },
	{ "GearingLockStatus",		"BOOL" },
	{ "PositionCamLockStatus",	"BOOL" },
	{ "MasterOffsetMoveStatus",	"BOOL" },
	{ "AxisEvent",			"DINT" },
	{ "WatchEventArmedStatus",	"BOOL" },
	{ "WatchEventStatus",		"BOOL" },
	{ "RegEvent1ArmedStatus",	"BOOL" },
	{ "RegEvent1Status",		"BOOL" },
	{ "RegEvent2ArmedStatus",	"BOOL" },
	{ "RegEvent2Status",		"BOOL" },
	{ "HomeEventArmedStatus",	"BOOL" },
	{ "HomeEventStatus",		"BOOL" },
	{ "OutputCamStatus",		"DINT" },
	{ "OutputCamPendingStatus",	"DINT" },
	{ "OutputCamLockStatus",	"DINT" },
	{ "OutputCamTransitionStatus",	"DINT" },
	{ "ActualPosition",		"REAL" },
	{ "StrobeActualPosition",	"REAL" },
	{ "StartActualPosition",	"REAL" },
	{ "AverageVelocity",		"REAL" },
	{ "ActualVelocity",		"REAL" },
	{ "ActualAcceleration",		"REAL" },
	{ "WatchPosition",		"REAL" },
	{ "Registration1Position",	"REAL" },
	{ "Registration2Position",	"REAL" },
	{ "Registration1Time",		"DINT" },
	{ "Registration2Time",		"DINT" },
	{ "InterpolationTime",		"DINT" },
	{ "InterpolatedActualPosition",	"REAL" },
	{ "MasterOffset",		"REAL" },
	{ "StrobeMasterOffset",		"REAL" },
	{ "StartMasterOffset",		"REAL" },
	{ "CommandPosition",		"REAL" },
	{ "StrobeCommandPosition",	"REAL" },
	{ "StartCommandPosition",	"REAL" },
	{ "CommandVelocity",		"REAL" },
	{ "CommandAcceleration",	"REAL" },
	{ "InterpolatedCommandPosition","REAL" },
	{ "ModuleFaults",		"DINT" },
	{ "ControlSyncFault",		"BOOL" },
	{ "ModuleSyncFault",		"BOOL" },
	{ "TimerEventFault",		"BOOL" },
	{ "ModuleHardwareFault",	"BOOL" },
	{ "SERCORingFault",		"BOOL" },
	{ "AttributeErrorCode",		"INT"  },
	{ "AttributeErrorID",		"INT"  },
	{ "PositionCommand",		"REAL" },
	{ "PositionFeedback",		"REAL" },
	{ "AuxPositionFeedback",	"REAL" },
	{ "PositionError",		"REAL" },
	{ "PositionIntegratorError",	"REAL" },
	{ "VelocityCommand",		"REAL" },
	{ "VelocityFeedback",		"REAL" },
	{ "VelocityError",		"REAL" },
	{ "VelocityIntegratorError",	"REAL" },
	{ "AccelerationCommand",	"REAL" },
	{ "AccelerationFeedback",	"REAL" },
	{ "ServoOutputLevel",		"REAL" },
	{ "MarkerDistance",		"REAL" },
	{ "VelocityOffset",		"REAL" },
	{ "TorqueOffset",		"REAL" },
	{ "TorqueCommand",		"REAL" },
	{ "TorqueFeedback",		"REAL" },
	{ "PosDynamicTorqueLimit",	"REAL" },
	{ "NegDynamicTorqueLimit",	"REAL" },
	{ "MotorCapacity",		"REAL" },
	{ "DriveCapacity",		"REAL" },
	{ "PowerCapacity",		"REAL" },
	{ "BusRegulatorCapacity",	"REAL" },
	{ "MotorElectricAngle",		"REAL" },
	{ "TorqueLimitStorage",		"DINT" },
	{ "DCBusVoltage",		"DINT" },
	{ "DriveStatus",		"DINT" },
	{ "ProcessStatus",		"BOOL" },	
	{ "HomeInputStatus",		"BOOL" },
	{ "Reg1InputStatus",		"BOOL" },
	{ "Reg2InputStatus",		"BOOL" },
	{ "PosOvertravelInputStatus",	"BOOL" },
	{ "NegOvertravelInputStatus",	"BOOL" },
	{ "EnableInputStatus",		"BOOL" },
	{ "AccelLimitStatus",		"BOOL" },
	{ "AbsoluteReferenceStatus",	"BOOL" },
	{ "VelocityLockStatus",		"BOOL" },
	{ "VelocityStandstillStatus",	"BOOL" },
	{ "VelocityThresholdStatus",	"BOOL" },
	{ "TorqueLimitStatus",		"BOOL" },
	{ "VelocityLimitStatus",	"BOOL" },
	{ "PositionLockStatus",		"BOOL" },
	{ "PowerLimitStatus",		"BOOL" },
	{ "LowVelocityThresholdStatus",	"BOOL" },
	{ "HighVelocityThresholdStatus","BOOL" },
	{ "DriveFault",			"BOOL" },
	{ "PosSoftOvertravelFault",	"BOOL" },
	{ "NegSoftOvertravelFault",	"BOOL" },
	{ "PosHardOvertravelFault",	"BOOL" },
	{ "NegHardOvertravelFault",	"BOOL" },
	{ "MotFeedbackFault",		"BOOL" },
	{ "MotFeedbackNoiseFault",	"BOOL" },
	{ "AuxFeedbackFault",		"BOOL" },
	{ "AuxFeedbackNoiseFault",	"BOOL" },
	{ "GroundShortFault",		"BOOL" },
	{ "DriveHardFault",		"BOOL" },
	{ "OverSpeedFault",		"BOOL" },
	{ "OverloadFault",		"BOOL" },
	{ "DriveOvertempFault",		"BOOL" },
	{ "MotorOvertempFault",		"BOOL" },
	{ "DriveCoolingFault",		"BOOL" },
	{ "DriveControlVoltageFault",	"BOOL" },
	{ "FeedbackFault",		"BOOL" },
	{ "CommutationFault",		"BOOL" },
	{ "DriveOvercurrentFault",	"BOOL" },
	{ "DriveOvervoltageFault",	"BOOL" },
	{ "DriveUndervoltageFault",	"BOOL" },
	{ "PowerPhaseLossFault",	"BOOL" },
	{ "PositionErrorFault",		"BOOL" },
	{ "SERCOSFault",		"BOOL" },
	{ "SERCOSErrorCode",		"INT"  },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"AXIS_VIRTUAL",
	{
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" }, 
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },
	{ "AxisStatus",			"DINT" },
	{ "ServoActionStatus",		"BOOL" },
	{ "DriveEnableStatus",		"BOOL" },
	{ "ShutdownStatus",		"BOOL" },
	{ "ConfigUpdateInProcess",	"BOOL" },
	{ "MotionStatus",		"DINT" },
	{ "AccelStatus",		"BOOL" },
	{ "DecelStatus",		"BOOL" },
	{ "MoveStatus",			"BOOL" },
	{ "JogStatus",			"BOOL" },
	{ "GearingStatus",		"BOOL" },
	{ "HomingStatus",		"BOOL" },
	{ "StoppingStatus",		"BOOL" },
	{ "AxisHomedStatus",		"BOOL" },
	{ "PositionCamStatus",		"BOOL" },
	{ "TimeCamStatus",		"BOOL" },
	{ "PositionCamPendingStatus",	"BOOL" },
	{ "TimeCamPendingStatus",	"BOOL" },
	{ "GearingLockStatus",		"BOOL" },
	{ "PositionCamLockStatus",	"BOOL" },
	{ "MasterOffsetMoveStatus",	"BOOL" },
	{ "AxisEvent",			"DINT" },
	{ "WatchEventArmedStatus",	"BOOL" },
	{ "WatchEventStatus",		"BOOL" },
	{ "RegEvent1ArmedStatus",	"BOOL" },
	{ "RegEvent1Status",		"BOOL" },
	{ "RegEvent2ArmedStatus",	"BOOL" },
	{ "RegEvent2Status",		"BOOL" },
	{ "HomeEventArmedStatus",	"BOOL" },
	{ "HomeEventStatus",		"BOOL" },
	{ "OutputCamStatus",		"DINT" },
	{ "OutputCamPendingStatus",	"DINT" },
	{ "OutputCamLockStatus",	"DINT" },
	{ "OutputCamTransitionStatus",	"DINT" },
	{ "ActualPosition",		"REAL" },
	{ "StrobeActualPosition",	"REAL" },
	{ "StartActualPosition",	"REAL" },
	{ "AverageVelocity",		"REAL" },
	{ "ActualVelocity",		"REAL" },
	{ "ActualAcceleration",		"REAL" },
	{ "WatchPosition",		"REAL" },
	{ "Registration1Position",	"REAL" },
	{ "Registration2Position",	"REAL" },
	{ "Registration1Time",		"DINT" },
	{ "Registration2Time",		"DINT" },
	{ "InterpolationTime",		"DINT" },
	{ "InterpolatedActualPosition",	"REAL" },
	{ "MasterOffset",		"REAL" },
	{ "StrobeMasterOffset",		"REAL" },
	{ "StartMasterOffset",		"REAL" },
	{ "CommandPosition",		"REAL" },
	{ "StrobeCommandPosition",	"REAL" },
	{ "StartCommandPosition",	"REAL" },
	{ "CommandVelocity",		"REAL" },
	{ "CommandAcceleration",	"REAL" },
	{ "InterpolatedCommandPosition","REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"CAM",
	{
	{ "Master",			"REAL" },
	{ "Slave",			"REAL" },
	{ "SegmentType",		"DINT" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"CAM_PROFILE",
	{
	{ "Status",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"CONNECTION_STATUS",
	{
	{ "RunMode",			"BOOL" },
	{ "ConnectionFaulted",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"CONTROL",
	{
	{ "LEN",			"DINT" },
	{ "POS",			"DINT" },
	{ "CTL",			"DINT" },
	{ "EN",				"BOOL" },
	{ "EU",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "EM",				"BOOL" },
	{ "ER",				"BOOL" },
	{ "UL",				"BOOL" },
	{ "IN",				"BOOL" },
	{ "FD",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"COORDINATE_SYSTEM",
	{
	{ "CoordinateSystemStatus",	"DINT" },
	{ "ShutdownStatus",		"BOOL" },
	{ "ReadyStatus",		"BOOL" },
	{ "MotionStatus",		"BOOL" },
	{ "CoordinateMotionStatus",	"DINT" },
	{ "AccelStatus",		"BOOL" },
	{ "DecelStatus",		"BOOL" },
	{ "ActualPosToleranceStatus",	"BOOL" },
	{ "CommandPosToleranceStatus",	"BOOL" },
	{ "StoppingStatus",		"BOOL" },
	{ "MoveStatus",			"BOOL" },
	{ "MoveTransitionStatus",	"BOOL" },
	{ "MovePendingStatus",		"BOOL" },
	{ "MovePendingQueueFullStatus",	"BOOL" },
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },
	{ "PhysicalAxesFaulted",	"DINT" },
	{ "ModulesFaulted",		"DINT" },
	{ "AxesConfigurationFaulted",	"DINT" },
	{ "AxesShutdownStatus",		"DINT" },
	{ "AxesServoOnStatus",		"DINT" },
	{ "ActualPosition",		"REAL[8]" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"COUNTER",
	{
	{ "PRE",			"DINT" },
	{ "ACC",			"DINT" },
	{ "CTL",			"DINT" },
	{ "CU",				"BOOL" },
	{ "CD",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "OV",				"BOOL" },
	{ "UN",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"DEADTIME",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"BOOL" },
	{ "InFault",			"BOOL" },
	{ "DeadTime",			"REAL" },
	{ "Gain",			"REAL" },
	{ "Bias",			"REAL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "InFaulted",			"BOOL" },
	{ "DeadTimeInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"DERIVATIVE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"BOOL" },
	{ "Gain",			"REAL" },
	{ "ByPass",			"REAL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"DISCRETE_2STATE",
	{
	{ "EnableIn",			"BOOL" },
	{ "ProgCommand",		"BOOL" },
	{ "Oper0Req",			"BOOL" },
	{ "Oper1Req",			"BOOL" },
	{ "State0Perm",			"BOOL" },
	{ "State1Perm",			"BOOL" },
	{ "FB0",			"BOOL" },
	{ "FB1",			"BOOL" },
	{ "HandFB",			"BOOL" },
	{ "FaultTime",			"REAL" },
	{ "FaultAlarmLatch",		"BOOL" },
	{ "FaultAlmUnlatch",		"BOOL" },
	{ "OverrideOnInit",		"BOOL" },
	{ "OverrideOnFault",		"BOOL" },
	{ "OutReverse",			"BOOL" },
	{ "OverrideState",		"BOOL" },
	{ "FB0State0",			"BOOL" },
	{ "FB0State1",			"BOOL" },
	{ "FB1State0",			"BOOL" },
	{ "FB1State1",			"BOOL" },
	{ "ProgProgReq",		"BOOL" },
	{ "ProgOperReq",		"BOOL" },
	{ "ProgOverrideReq",		"BOOL" },
	{ "ProgHandReq",		"BOOL" },
	{ "OperProgReq",		"BOOL" },
	{ "OperOperReq",		"BOOL" },
	{ "ProgValueReset",		"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "Device0State",		"BOOL" },
	{ "Device1State",		"BOOL" },
	{ "CommandStatus",		"BOOL" },
	{ "FaultAlarm",			"BOOL" },
	{ "ModeAlarm",			"BOOL" },
	{ "ProgOper",			"BOOL" },
	{ "Override",			"BOOL" },
	{ "Hand",			"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "FaultTimeInv",		"BOOL" },
	{ "OperReqInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"DISCRETE_3STATE",
	{
	{ "EnableIn",			"BOOL" },
	{ "Prog0Command",		"BOOL" },
	{ "Prog1Command",		"BOOL" },
	{ "Prog2Command",		"BOOL" },
	{ "Oper0Req",			"BOOL" },
	{ "Oper1Req",			"BOOL" },
	{ "Oper2Req",			"BOOL" },
	{ "State0Perm",			"BOOL" },
	{ "State1Perm",			"BOOL" },
	{ "State2Perm",			"BOOL" },
	{ "FB0",			"BOOL" },
	{ "FB1",			"BOOL" },
	{ "FB2",			"BOOL" },
	{ "FB3",			"BOOL" },
	{ "HandFB0",			"BOOL" },
	{ "HandFB1",			"BOOL" },
	{ "HandFB2",			"BOOL" },
	{ "FaultTime",			"REAL" },
	{ "FaultAlarmLatch",		"BOOL" },
	{ "FaultAlmUnlatch",		"BOOL" },
	{ "OverrideOnInit",		"BOOL" },
	{ "OverrideOnFault",		"BOOL" },
	{ "Out0State0",			"BOOL" },
	{ "Out0State1",			"BOOL" },
	{ "Out0State2",			"BOOL" },
	{ "Out1State0",			"BOOL" },
	{ "Out1State1",			"BOOL" },
	{ "Out1State2",			"BOOL" },
	{ "Out2State0",			"BOOL" },
	{ "Out2State1",			"BOOL" },
	{ "Out2State2",			"BOOL" },
	{ "OverrideState",		"DINT" },
	{ "FB0State0",			"BOOL" },
	{ "FB0State1",			"BOOL" },
	{ "FB0State2",			"BOOL" },
	{ "FB1State0",			"BOOL" },
	{ "FB1State1",			"BOOL" },
	{ "FB1State2",			"BOOL" },
	{ "FB2State0",			"BOOL" },
	{ "FB2State1",			"BOOL" },
	{ "FB2State2",			"BOOL" },	 
	{ "FB3State0",			"BOOL" },
	{ "FB3State1",			"BOOL" },
	{ "FB3State2",			"BOOL" },
	{ "ProgProgReq",		"BOOL" },
	{ "ProgOperReq",		"BOOL" },
	{ "ProgOverrideReq",		"BOOL" },
	{ "ProgHandReq",		"BOOL" },
	{ "OperProgReq",		"BOOL" },
	{ "OperOperReq",		"BOOL" },
	{ "ProgValueReset",		"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out0",			"BOOL" },
	{ "Out1",			"BOOL" },	 
	{ "Out2",			"BOOL" },
	{ "Device0State",		"BOOL" },
	{ "Device1State",		"BOOL" },
	{ "Device2State",		"BOOL" },
	{ "Command0Status",		"BOOL" },
	{ "Command1Status",		"BOOL" },
	{ "Command2Status",		"BOOL" },
	{ "FaultAlarm",			"BOOL" },
	{ "ModeAlarm",			"BOOL" },
	{ "ProgOper",			"BOOL" },
	{ "Override",			"BOOL" },
	{ "Hand",			"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "FaultTimeInv",		"BOOL" },
	{ "OverrideStateInv",		"BOOL" },
	{ "ProgCommandInv",		"BOOL" },
	{ "OperReqInv",			"BOOL" },
	{ "HandCommandInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"DIVERSE_INPUT",
	{
	{ "EnableIn",			"BOOL" },
	{ "ResetType",			"BOOL" },
	{ "ChannelA",			"BOOL" },
	{ "ChannelB",			"BOOL" },
	{ "CircuitReset",		"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "C1",				"BOOL" },
	{ "CRHO",			"BOOL" },
	{ "II",				"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"DOMINANT_RESET",
	{
	{ "EnableIn",			"BOOL" },
	{ "Set",			"BOOL" },
	{ "Reset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "OutNot",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"DOMINANT_SET",
	{
	{ "EnableIn",			"BOOL" },
	{ "Set",			"BOOL" },
	{ "Reset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "OutNot",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"EMERGENCY_STOP",
	{
	{ "EnableIn",			"BOOL" },
	{ "ResetType",			"BOOL" },
	{ "ChannelA",			"BOOL" },
	{ "ChannelB",			"BOOL" },
	{ "CircuitReset",		"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "CI",				"BOOL" },
	{ "CRHO",			"BOOL" },
	{ "II",				"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"ENABLE_PENDANT",
	{
	{ "EnableIn",			"BOOL" },
	{ "ResetType",			"BOOL" },
	{ "ChannelA",			"BOOL" },
	{ "ChannelB",			"BOOL" },
	{ "CircuitReset",		"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "CI",				"BOOL" },
	{ "CRHO",			"BOOL" },
	{ "II",				"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"EXT_ROUTINE_CONTROL",
	{
	{ "ErrorCode",			"SINT" },
	{ "NumParams",			"SINT" },
	{ "ParameterDefs",		"EXT_ROUTINE_PARAMETERS[10]" },
	{ "ReturnParamDef",		"EXT_ROUTINE_PARAMETERS" },
	{ "EN",				"BOOL" },
	{ "ReturnsValue",		"BOOL" },
	{ "DN",				"BOOL" },
	{ "ER",				"BOOL" },
	{ "FirstScan",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "EnableIn",			"BOOL" },
	{ "User1",			"BOOL" },
	{ "User0",			"BOOL" },
	{ "ScanType1",			"BOOL" },
	{ "ScanType0",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"EXT_ROUTINE_PARAMETERS",
	{
	{ "ElementSize",		"DINT" },
	{ "ElementCount",		"DINT" },
	{ "ParamType",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_BIT_FIELD_DISTRIBUTE",
	{
	{ "EnableIn",			"BOOL" },
	{ "Source",			"DINT" },
	{ "SourceBit",			"DINT" },
	{ "Length",			"DINT" },
	{ "DestBit",			"DINT" },
	{ "Target",			"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_BOOLEAN_AND",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"BOOL" },
	{ "In2",			"BOOL" },
	{ "In3",			"BOOL" },
	{ "In4",			"BOOL" },
	{ "In5",			"BOOL" },
	{ "In6",			"BOOL" },
	{ "In7",			"BOOL" },
	{ "In8",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_BOOLEAN_NOT",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_BOOLEAN_OR",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"BOOL" },
	{ "In2",			"BOOL" },
	{ "In3",			"BOOL" },
	{ "In4",			"BOOL" },
	{ "In5",			"BOOL" },
	{ "In6",			"BOOL" },
	{ "In7",			"BOOL" },
	{ "In8",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_BOOLEAN_XOR",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"BOOL" },
	{ "In2",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_COMPARE",
	{
	{ "EnableIn",			"BOOL" },
	{ "SourceA",			"BOOL" },
	{ "SourceB",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_CONVERT",
	{
	{ "EnableIn",			"BOOL" },
	{ "Source",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_COUNTER",
	{
	{ "EnableIn",			"BOOL" },
	{ "CUEnable",			"BOOL" },
	{ "CDEnable",			"BOOL" },
	{ "PRE",			"DINT" },
	{ "Reset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "ACC",			"DINT" },
	{ "CU",				"BOOL" },
	{ "CD",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "OV",				"BOOL" },
	{ "UN",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_LIMIT",
	{
	{ "EnableIn",			"BOOL" },
	{ "LowLimit",			"BOOL" },
	{ "Test",			"REAL" },
	{ "HighLimit",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_LOGICAL",
	{
	{ "EnableIn",			"BOOL" },
	{ "SourceA",			"DINT" },
	{ "SourceB",			"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_MASKED_MOVE",
	{
	{ "EnableIn",			"BOOL" },
	{ "Source",			"DINT" },
	{ "Mask",			"DINT" },
	{ "Target",			"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_MASK_EQUAL",
	{
	{ "EnableIn",			"BOOL" },
	{ "Source",			"DINT" },
	{ "Mask",			"DINT" },
	{ "Compare",			"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_MATH",
	{
	{ "EnableIn",			"BOOL" },
	{ "SourceA",			"REAL" },
	{ "SourceB",			"REAL" },
	{ "EnableOut",			"REAL" },
	{ "Dest",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_MATH_ADVANCED",
	{
	{ "EnableIn",			"BOOL" },
	{ "Source",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_ONE_SHOT",
	{
	{ "EnableIn",			"BOOL" },
	{ "InputBit",			"BOOL" },
	{ "EnableBit",			"BOOL" },
	{ "OutputBit",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_TIMER",
	{
	{ "EnableIn",			"BOOL" },
	{ "TimerEnable",		"BOOL" },
	{ "PRE",			"DINT" },
	{ "Reset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "ACC",			"DINT" },
	{ "EN",				"BOOL" },
	{ "TT",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "PresetInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FBD_TRUNCATE",
	{
	{ "EnableIn",			"BOOL" },
	{ "Source",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "Dest",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FILTER_HIGH_PASS",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Initialize",			"BOOL" },
	{ "WLead",			"REAL" },
	{ "Order",			"DINT" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "WLeadInv",			"BOOL" },
	{ "OrderInv",			"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FILTER_LOW_PASS",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Initialize",			"BOOL" },
	{ "WLag",			"REAL" },
	{ "Order",			"DINT" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "WLagInv",			"BOOL" },
	{ "OrderInv",			"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FILTER_NOTCH",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Initialize",			"BOOL" },
	{ "WNotch",			"REAL" },
	{ "QFactor",			"REAL" },
	{ "Order",			"DINT" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "WNotchInv",			"BOOL" },
	{ "QFactorInv",			"BOOL" },
	{ "OrderInv",			"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FIVE_POS_MODE_SELECTOR",
	{
	{ "EnableIn",			"BOOL" },
	{ "Input1",			"BOOL" },
	{ "Input2",			"BOOL" },
	{ "Input3",			"BOOL" },
	{ "Input4",			"BOOL" },
	{ "Input5",			"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "O2",				"BOOL" },
	{ "O3",				"BOOL" },
	{ "O4",				"BOOL" },
	{ "O5",				"BOOL" },
	{ "NM",				"BOOL" },
	{ "NMS",			"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FLIP_FLOP_D",
	{
	{ "EnableIn",			"BOOL" },
	{ "D",				"BOOL" },
	{ "Clear",			"BOOL" },
	{ "Clock",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Q",				"BOOL" },
	{ "QNot",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FLIP_FLOP_JK",
	{
	{ "EnableIn",			"BOOL" },
	{ "Clear",			"BOOL" },
	{ "Clock",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Q",				"BOOL" },
	{ "QNot",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"FUNCTION_GENERATOR",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "XY1Size",			"DINT" },
	{ "XY2Size",			"DINT" },
	{ "Select",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "XY1SizeInv",			"BOOL" },
	{ "XY2SizeInv",			"BOOL" },
	{ "XisOutofOrder",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"HL_LIMIT",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "HighLimit",			"REAL" },
	{ "LowLimit",			"REAL" },
	{ "SelectLimit",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "HighAlarm",			"BOOL" },
	{ "LowAlarm",			"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "LimitsInv",			"BOOL" },
	{ "SelectLimitInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"INTEGRATOR",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"BOOL" },
	{ "Initialize",			"REAL" },
	{ "IGain",			"REAL" },
	{ "HighLimit",			"REAL" },
	{ "LowLimit",			"REAL" },
	{ "HoldHigh",			"BOOL" },
	{ "HoldLow",			"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },	
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "HighAlarm",			"BOOL" },
	{ "LowAlarm",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "IGainInv",			"BOOL" },
	{ "HighLowLimsInv",		"BOOL" },	
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"LEAD_LAG",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"BOOL" },
	{ "Initialize",			"REAL" },
	{ "Lead",			"REAL" },
	{ "Lag",			"REAL" },
	{ "Gain",			"REAL" },
	{ "Bias",			"REAL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },	
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "LeadInv",			"BOOL" },
	{ "LagInv",			"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"LEAD_LAG_SEC_ORDER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"BOOL" },
	{ "Initialize",			"REAL" },
	{ "WLead",			"REAL" },
	{ "WLag",			"REAL" },
	{ "ZetaLead",			"REAL" },
	{ "ZetaLag",			"REAL" },
	{ "Order",			"REAL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },	
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "WLeadInv",			"BOOL" },
	{ "WLagInv",			"BOOL" },
	{ "ZetaLeadInv",		"BOOL" },
	{ "ZetaLagInv",			"BOOL" },
	{ "OrderInv",			"BOOL" },
	{ "WLagRatioInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"LIGHT_CURTAIN",
	{
	{ "EnableIn",			"BOOL" },
	{ "ResetType",			"BOOL" },
	{ "ChannelA",			"BOOL" },
	{ "ChannelB",			"BOOL" },
	{ "MuteLightCurtain",		"BOOL" },
	{ "CircuitReset",		"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "InputFilterTime",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "C1",				"BOOL" },
	{ "CRHO",			"BOOL" },
	{ "LCB",			"BOOL" },
	{ "LCM",			"BOOL" },
	{ "II",				"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MAXIMUM_CAPTURE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Reset",			"BOOL" },
	{ "ResetValue",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MESSAGE",
	{
	{ "Flags",			"INT" },
	{ "EW",				"BOOL" },
	{ "ER",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "ST",				"BOOL" },
	{ "EN",				"BOOL" },
	{ "TO",				"BOOL" },
	{ "EN_CC",			"BOOL" },
	{ "ERR",			"INT" },
	{ "EXERR",			"DINT" },
	{ "ERR_SRC",			"SINT" },
	{ "DN_LEN",			"INT" },
	{ "REQ_LEN",			"INT" },
	{ "DestinationLink",		"INT" },
	{ "DestinationNode",		"INT" },
	{ "SourceLink",			"INT" },
	{ "Class",			"INT" },
	{ "Attribute",			"INT" },
	{ "Instance",			"DINT" },
	{ "Channel",			"SINT" },
	{ "Rack",			"SINT" },
	{ "Group",			"SINT" },
	{ "Slot",			"SINT" },
	{ "Path",			"STRING" },
	{ "RemoteIndex",		"DINT" },
	{ "RemoteElement",		"STRING" },
	{ "UnconnectedTimeout",		"DINT" },
	{ "ConnectionRate",		"DINT" },
	{ "TimeoutMultiplier",		"SINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MINIMUM_CAPTURE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Reset",			"BOOL" },
	{ "ResetValue",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MOTION_GROUP",
	{
	{ "GroupStatus",		"DINT" },
	{ "InhibStatus",		"BOOL" },
	{ "GroupSynced",		"BOOL" },
	{ "AxisInhibStatus",		"BOOL" },
	{ "GroupFault",			"DINT" },
	{ "GroupOverlapFault",		"BOOL" },
	{ "CSTLossFault",		"BOOL" },
	{ "GroupTaskLoadingFault",	"BOOL" },	
	{ "AxisFault",			"DINT" },
	{ "PhysicalAxisFault",		"BOOL" },
	{ "ModuleFault",		"BOOL" },
	{ "ConfigFault",		"BOOL" },	
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MOTION_INSTRUCTION",
	{
	{ "FLAGS",			"DINT" },
	{ "ERR",			"INT" },
	{ "STATUS",			"SINT" },
	{ "STATE",			"SINT" },
	{ "SEGMENT",			"DINT" },
	{ "EN",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "ER",				"BOOL" },
	{ "PC",				"BOOL" },
	{ "IP",				"BOOL" },
	{ "AC",				"BOOL" },
	{ "ACCEL",			"BOOL" },
	{ "DECEL",			"BOOL" },
	{ "EXERR",			"SINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MOVING_AVERAGE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "InFault",			"BOOL" },
	{ "Initialize",			"BOOL" },
	{ "SampleEnable",		"BOOL" },
	{ "NumberOfSamples",		"DINT" },
	{ "UseWeights",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "InFaulted",			"BOOL" },
	{ "NumberOfSampInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MOVING_STD_DEV",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "InFault",			"BOOL" },
	{ "Initialize",			"BOOL" },
	{ "SampleEnable",		"BOOL" },
	{ "NumberOfSamples",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "Average",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "InFaulted",			"BOOL" },
	{ "NumberOfSampInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"MULTIPLEXER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"REAL" },
	{ "In2",			"REAL" },
	{ "In3",			"REAL" },
	{ "In4",			"REAL" },
	{ "In5",			"REAL" },
	{ "In6",			"REAL" },
	{ "In7",			"REAL" },
	{ "In8",			"REAL" },
	{ "Selector",			"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "SelectorInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"OUTPUT_CAM",
	{
	{ "OutputBit",			"DINT" },
	{ "LatchType",			"DINT" },
	{ "UnlatchType",		"DINT" },
	{ "Left",			"REAL" },
	{ "Right",			"REAL" },
	{ "Duration",			"REAL" },
	{ "EnableType",			"DINT" },
	{ "EnableBit",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"OUTPUT_COMPENSATION",
	{
	{ "Offset",			"REAL" },
	{ "LatchDelay",			"REAL" },
	{ "UnlatchDelay",		"REAL" },
	{ "Mode",			"DINT" },
	{ "CycleTime",			"REAL" },
	{ "DutyCycle",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"PHASE",
	{
	{ "State",					"DINT" },
	{ "Running",					"BOOL" },
	{ "Holding",					"BOOL" },
	{ "Restarting",					"BOOL" },
	{ "Stopping",					"BOOL" },
	{ "Aborting",					"BOOL" },
	{ "Resetting",					"BOOL" },
	{ "Idle",					"BOOL" },
	{ "Held",					"BOOL" },
	{ "Complete",					"BOOL" },
	{ "Stopped",					"BOOL" },
	{ "Aborted",					"BOOL" },
	{ "Substate",					"DINT" },
	{ "Pausing",					"BOOL" },
	{ "Paused",					"BOOL" },
	{ "AutoPause",					"BOOL" },
	{ "StepIndex",					"DINT" },
	{ "Failure",					"DINT" },
	{ "UnitMD",					"DINT" },
	{ "Owner",					"DINT" },
	{ "PendingRequest",				"DINT" },
	{ "DownloadInputParameters",			"BOOL" },
	{ "DownloadInputParametersSubset",		"BOOL" },
	{ "UploadInputParameters",			"BOOL" },
	{ "UploadInputParametersSubset",		"BOOL" },
	{ "DownloadOutputParameterLimits",		"BOOL" },
	{ "AcquireResources",				"BOOL" },
	{ "ReleaseResources",				"BOOL" },
	{ "SendMessageToLinkedPhase",			"BOOL" },
	{ "SendMessageToLinkedPhaseAndWait",		"BOOL" },
	{ "ReceiveMessageFromLinkedPhase",		"BOOL" },
	{ "CancelMessageToLinkedPhase",			"BOOL" },
	{ "SendMessageToOperator",			"BOOL" },
	{ "ClearMessaegToOperator",			"BOOL" },
	{ "GenerateESignature",				"BOOL" },
	{ "DownloadBatchData",				"BOOL" },
	{ "DownloadMaterialTrackDataContainerInUse",	"BOOL" },
	{ "DownloadContainerBindingPriority",		"BOOL" },
	{ "DownloadSufficientMaterial",			"BOOL" },
	{ "DownloadMaterialTrackDatabaseData",		"BOOL" },
	{ "UploadMaterialTrackDataContainerInUse",	"BOOL" },
	{ "UploadContainerBindingPriority",		"BOOL" },
	{ "UploadMaterialTrackDatabaseData",		"BOOL" },
	{ "AbortingRequest",				"BOOL" },
	{ "NewInputParameters",				"BOOL" },
	{ "Producing",					"BOOL" },
	{ "Standby",					"BOOL" },
	{  END_MARKER,					END_MARKER },
	},
	},

	{
		"PHASE_INSTRUCTION",
	{
	{ "Status",					"DINT" },
	{ "EN",						"BOOL" },
	{ "ER",						"BOOL" },
	{ "PC",						"BOOL" },
	{ "IP",						"BOOL" },
	{ "WA",						"BOOL" },
	{ "ABORT",					"BOOL" },
	{ "ERR",					"INT" },
	{ "EXERR",					"INT" },
	{  END_MARKER,					END_MARKER },
	},
	},

	{
		"PID",
	{
	{ "CTL",			"DINT" },
	{ "EN",				"BOOL" },
	{ "CT",				"BOOL" },
	{ "CL",				"BOOL" },
	{ "PVT",			"BOOL" },
	{ "DOE",			"BOOL" },
	{ "SWM",			"BOOL" },
	{ "CA",				"BOOL" },			
	{ "MO",				"BOOL" },
	{ "PE",				"BOOL" },
	{ "NDF",			"BOOL" },
	{ "NOBC",			"BOOL" },
	{ "NOZC",			"BOOL" },
	{ "INI",			"BOOL" },
	{ "SPOR",			"BOOL" },
	{ "OLL",			"BOOL" },
	{ "OLH",			"BOOL" },
	{ "EWD",			"BOOL" },
	{ "DVNA",			"BOOL" },
	{ "DVPA",			"BOOL" },
	{ "PVLA",			"BOOL" },
	{ "PVHA",			"BOOL" },
	{ "SP",				"REAL" },
	{ "KP",				"REAL" },
	{ "KI",				"REAL" },
	{ "KD",				"REAL" },
	{ "BIAS",			"REAL" },
	{ "MAXS",			"REAL" },
	{ "MINS",			"REAL" },
	{ "DB",				"REAL" },
	{ "SO",				"REAL" },
	{ "MAXO",			"REAL" },
	{ "MINO",			"REAL" },
	{ "UPD",			"REAL" },
	{ "PV",				"REAL" },
	{ "ERR",			"REAL" },
	{ "OUT",			"REAL" },
	{ "PVH",			"REAL" },
	{ "PVL",			"REAL" },
	{ "DVP",			"REAL" },
	{ "DVN",			"REAL" },
	{ "PVDB",			"REAL" },
	{ "DVDB",			"REAL" },
	{ "MAXI",			"REAL" },
	{ "MINI",			"REAL" },
	{ "TIE",			"REAL" },
	{ "MAXCV",			"REAL" },
	{ "MINCV",			"REAL" },
	{ "MINTIE",			"REAL" },
	{ "MAXTIE",			"REAL" },
	{ "DATA",			"REAL[17]" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"PIDE_AUTOTUNE",
	{
	{ "ProcessType",		"DINT" },
	{ "ResponseSpeed",		"DINT" },
	{ "TestLength",			"REAL" },
	{ "PVTuneLimit",		"REAL" },
	{ "StepSize",			"REAL" },
	{ "TunedGood",			"BOOL" },
	{ "TunedUncertain",		"BOOL" },
	{ "ATuneAcquired",		"BOOL" },
	{ "UsedProcessType",		"DINT" },
	{ "Gain",			"REAL" },
	{ "TimeConstant",		"REAL" },
	{ "DeadTime",			"REAL" },
	{ "PGainTunedFast",		"REAL" },
	{ "IGainTunedFast",		"REAL" },
	{ "DGainTunedFast",		"REAL" },
	{ "PGainTunedMed",		"REAL" },
	{ "IGainTunedMed",		"REAL" },
	{ "DGainTunedMed",		"REAL" },
	{ "PGainTunedSlow",		"REAL" },
	{ "IGainTunedSlow",		"REAL" },
	{ "DGainTunedSlow",		"REAL" },
	{ "StepSizeUsed",		"REAL" },
	{ "AtuneStatus",		"DINT" },
	{ "ATuneFault",			"BOOL" },
	{ "PVOutOfLimit",		"BOOL" },
	{ "ModeInv",			"BOOL" },
	{ "CVWindupFault",		"BOOL" },
	{ "StepSizeZero",		"BOOL" },
	{ "CVLimitsFault",		"BOOL" },
	{ "CVInitFault",		"BOOL" },
	{ "EUSpanChanged",		"BOOL" },
	{ "CVChanged",			"BOOL" },
	{ "ATuneTimedOut",		"BOOL" },
	{ "PVNotSettled",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"PID_ENHANCED",
	{
	{ "EnableIn",			"BOOL" },
	{ "PV",				"REAL" },
	{ "PVFault",			"BOOL" },
	{ "PVEUMax",			"REAL" },
	{ "PVEUMin",			"REAL" },
	{ "SPProg",			"REAL" },
	{ "SPOper",			"REAL" },
	{ "SPCascade",			"REAL" },
	{ "SPHLimit",			"REAL" },
	{ "SPLLimit",			"REAL" },
	{ "UseRatio",			"BOOL" },
	{ "RatioProg",			"REAL" },
	{ "RatioOper",			"REAL" },
	{ "RatioHLimit",		"REAL" },
	{ "RatioLLimit",		"REAL" },
	{ "CVFault",			"BOOL" },
	{ "CVInitReq",			"BOOL" },
	{ "CVInitValue",		"REAL" },
	{ "CVProg",			"REAL" },
	{ "CVOper",			"REAL" },
	{ "CVOverride",			"REAL" },
	{ "CVPrevious",			"REAL" },
	{ "CVSetPrevious",		"BOOL" },
	{ "CVManLimiting",		"BOOL" },
	{ "CVEUMax",			"REAL" },
	{ "CVEUMin",			"REAL" },
	{ "CVHLimit",			"REAL" },
	{ "CVLLimit",			"REAL" },
	{ "CVROCLimit",			"REAL" },
	{ "FF",				"REAL" },
	{ "FFPrevious",			"REAL" },
	{ "FFSetPrevious",		"BOOL" },
	{ "HandFB",			"REAL" },
	{ "HandFBFault",		"BOOL" },
	{ "WindupHIn",			"BOOL" },
	{ "WindupLIn",			"BOOL" },
	{ "ControlAction",		"BOOL" },
	{ "DependIndepend",		"BOOL" },
	{ "PGain",			"REAL" },
	{ "IGain",			"REAL" },
	{ "DGain",			"REAL" },
	{ "PVEProportional",		"BOOL" },
	{ "PVEDerivative",		"BOOL" },
	{ "DSmoothing",			"BOOL" },
	{ "PVTracking",			"BOOL" },
	{ "ZCDeadband",			"REAL" },
	{ "ZCOff",			"BOOL" },
	{ "PVHHLimit",			"REAL" },
	{ "PVHLimit",			"REAL" },
	{ "PVLLimit",			"REAL" },
	{ "PVLLLimit",			"REAL" },
	{ "PVDeadband",			"REAL" },
	{ "PVROCPosLimit",		"REAL" },
	{ "PVROCNegLimit",		"REAL" },
	{ "PVROCPeriod",		"REAL" },
	{ "DevHHLimit",			"REAL" },
	{ "DevHLimit",			"REAL" },
	{ "DevLLimit",			"REAL" },
	{ "DevLLLimit",			"REAL" },
	{ "DevDeadband",		"REAL" },
	{ "AllowCasRat",		"BOOL" },
	{ "ManualAfterInit",		"BOOL" },
	{ "ProgProgReq",		"BOOL" },
	{ "ProgOperReq",		"BOOL" },
	{ "ProgCasRatReq",		"BOOL" },
	{ "ProgAutoReq",		"BOOL" },
	{ "ProgManualReq",		"BOOL" },
	{ "ProgOverrideReq",		"BOOL" },
	{ "ProgHandReq",		"BOOL" },
	{ "OperProgReq",		"BOOL" },
	{ "OperOperReq",		"BOOL" },
	{ "OperCasRatReq",		"BOOL" },
	{ "OperAutoReq",		"BOOL" },
	{ "OperManualReq",		"BOOL" },
	{ "ProgValueReset",		"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "AtuneAcquire",		"BOOL" },
	{ "AtuneStart",			"BOOL" },
	{ "AtuneUseGains",		"BOOL" },
	{ "AtuneAbort",			"BOOL" },
	{ "AtuneUnacquire",		"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "CVEU",			"REAL" },
	{ "CV",				"REAL" },
	{ "CVInitializing",		"BOOL" },
	{ "CVHAlarm",			"BOOL" },
	{ "CVLAlarm",			"BOOL" },
	{ "CVROCAlarm",			"BOOL" },
	{ "SP",				"REAL" },
	{ "SPPercent",			"REAL" },
	{ "SPHAlarm",			"BOOL" },
	{ "SPLAlarm",			"BOOL" },
	{ "PVPercent",			"REAL" },
	{ "E",				"REAL" },
	{ "EPercent",			"REAL" },
	{ "InitPrimary",		"BOOL" },
	{ "WindupHOut",			"BOOL" },
	{ "WindupLOut",			"BOOL" },
	{ "Ratiov",			"REAL" },
	{ "RatioHAlarm",		"BOOL" },
	{ "RatioLAlarm",		"BOOL" },
	{ "ZCDeadbandOn",		"BOOL" },
	{ "PVHHAlarm",			"BOOL" },
	{ "PVHAlarm",			"BOOL" },
	{ "PVLAlarm",			"BOOL" },
	{ "PVLLAlarm",			"BOOL" },
	{ "PVROCPosAlarm",		"BOOL" },
	{ "PVROCNegAlarm",		"BOOL" },
	{ "DevHHAlarm",			"BOOL" },
	{ "DevHAlarm",			"BOOL" },
	{ "DevLAlarm",			"BOOL" },
	{ "DevLLAlarm",			"BOOL" },
	{ "ProgOper",			"BOOL" },
	{ "CasRat",			"BOOL" },
	{ "Auto",			"BOOL" },
	{ "Manual",			"BOOL" },
	{ "Override",			"BOOL" },
	{ "Hand",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "AtuneReady",			"BOOL" },
	{ "AtuneOn",			"BOOL" },
	{ "AtuneDone",			"BOOL" },
	{ "AtuneAborted",		"BOOL" },
	{ "AtuneBusy",			"BOOL" },
	{ "Status1",			"DINT" },
	{ "Status2",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "PVFaulted",			"BOOL" },
	{ "CVFaulted",			"BOOL" },
	{ "HandFBFaulted",		"BOOL" },
	{ "PVSpanInv",			"BOOL" },
	{ "SPProgInv",			"BOOL" },
	{ "SPOperInv",			"BOOL" },
	{ "SPCascadeInv",		"BOOL" },
	{ "SPLimitsInv",		"BOOL" },
	{ "RatioProgInv",		"BOOL" },
	{ "RatioOperInv",		"BOOL" },
	{ "RatioLimitsInv",		"BOOL" },
	{ "CVProgInv",			"BOOL" },
	{ "CVOperInv",			"BOOL" },
	{ "CVOverrideInv",		"BOOL" },
	{ "CVPreviousInv",		"BOOL" },
	{ "CVEUSpanInv",		"BOOL" },
	{ "CVLimitsInv",		"BOOL" },
	{ "CVROCLimitInv",		"BOOL" },
	{ "FFInv",			"BOOL" },
	{ "FFPreviousInv",		"BOOL" },
	{ "HandFBInv",			"BOOL" },
	{ "PGainInv",			"BOOL" },
	{ "IGainInv",			"BOOL" },
	{ "DGainInv",			"BOOL" },
	{ "ZCDeadbandInv",		"BOOL" },
	{ "PVDeadbandInv",		"BOOL" },
	{ "PVROCLimitsInv",		"BOOL" },
	{ "DevHLLimitsInv",		"BOOL" },
	{ "DevDeadbandInv",		"BOOL" },
	{ "AtuneDataInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"POSITION_PROP",
	{
	{ "EnableIn",			"BOOL" },
	{ "SP",				"REAL" },
	{ "Position",			"REAL" },
	{ "OpenedFB",			"BOOL" },
	{ "ClosedFB",			"BOOL" },
	{ "PositionEUMax",		"REAL" },
	{ "PositionEUMin",		"REAL" },
	{ "CycleTime",			"REAL" },
	{ "OpenRate",			"REAL" },
	{ "CloseRate",			"REAL" },
	{ "MaxOnTime",			"REAL" },
	{ "MinOnTime",			"REAL" },
	{ "Deadtime",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "OpenOut",			"BOOL" },
	{ "CloseOut",			"BOOL" },
	{ "PositionPercent",		"REAL" },
	{ "SPPercent",			"REAL" },
	{ "OpenTime",			"REAL" },
	{ "CloseTime",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "CycleTimeInv",		"BOOL" },
	{ "OpenRateInv",		"BOOL" },
	{ "CloseRateInv",		"BOOL" },
	{ "MaxOnTimeInv",		"BOOL" },
	{ "MinOnTimeInv",		"BOOL" },
	{ "DeadtimeInv",		"BOOL" },
	{ "PositionPctInv",		"BOOL" },
	{ "SPPercentInv",		"BOOL" },
	{ "PositionSpanInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"PROP_INT",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Initialize",			"BOOL" },
	{ "InitialValue",		"REAL" },
	{ "Kp",				"REAL" },
	{ "Wld",			"REAL" },
	{ "HighLimit",			"REAL" },
	{ "LowLimit",			"REAL" },
	{ "HoldHigh",			"BOOL" },
	{ "HoldLow",			"BOOL" },
	{ "ShapeKpPlus",		"REAL" },
	{ "ShapeKpMinus",		"REAL" },
	{ "KpInRange",			"REAL" },
	{ "ShapeWldPlus",		"REAL" },
	{ "ShapeWldMinus",		"REAL" },
	{ "WldInRange",			"REAL" },
	{ "NonLinearMode",		"BOOL" },
	{ "ParabolicLinear",		"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "HighAlarm",			"BOOL" },
	{ "LowAlarm",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "KpInv",			"BOOL" },
	{ "WldInv",			"BOOL" },
	{ "HighLowLimsInv",		"BOOL" },
	{ "ShapeKpPlusInv",		"BOOL" },
	{ "ShapeKpMinusInv",		"BOOL" },
	{ "KpInRangeInv",		"BOOL" },
	{ "ShapeWldPlusInv",		"BOOL" },
	{ "ShapeWldMinusInv",		"BOOL" },
	{ "WldInRangeInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"PULSE_MULTIPLIER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"DINT" },
	{ "Initialize",			"BOOL" },
	{ "InitialValue",		"DINT" },
	{ "Mode",			"BOOL" },
	{ "WordSize",			"DINT" },
	{ "Multiplier",			"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "WordSizeInv",		"BOOL" },
	{ "OutOverflow",		"BOOL" },
	{ "LostPrecision",		"BOOL" },
	{ "MultiplierInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"RAMP_SOAK",
	{
	{ "EnableIn",			"BOOL" },
	{ "PV",				"REAL" },
	{ "PVFault",			"BOOL" },
	{ "NumberOfSegs",		"DINT" },
	{ "ManHoldAftInit",		"BOOL" },
	{ "CyclicSingle",		"BOOL" },
	{ "TimeRate",			"BOOL" },
	{ "GuarRamp",			"BOOL" },
	{ "RampDeadband",		"REAL" },
	{ "GuarSoak",			"BOOL" },
	{ "SoakDeadband",		"REAL" },
	{ "CurrentSegProg",		"DINT" },
	{ "OutProg",			"REAL" },
	{ "SoakTimeProg",		"REAL" },
	{ "CurrentSegOper",		"DINT" },
	{ "OutOper",			"REAL" },
	{ "SoakTimeOper",		"REAL" },
	{ "ProgProgReq",		"BOOL" },
	{ "ProgOperReq",		"BOOL" },
	{ "ProgAutoReq",		"BOOL" },
	{ "ProgManualReq",		"BOOL" },
	{ "ProgHoldReq",		"BOOL" },
	{ "OperProgReq",		"BOOL" },
	{ "OperOperReq",		"BOOL" },
	{ "OperAutoReq",		"BOOL" },
	{ "OperManualReq",		"BOOL" },
	{ "Initialize",			"BOOL" },
	{ "ProgValueReset",		"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "CurrentSeg",			"DINT" },
	{ "SoakTimeLeft",		"REAL" },
	{ "GuarRampOn",			"BOOL" },
	{ "GuarSoakOn",			"BOOL" },
	{ "ProgOper",			"BOOL" },
	{ "Auto",			"BOOL" },
	{ "Manual",			"BOOL" },
	{ "Hold",			"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "PVFaulted",			"BOOL" },
	{ "NumberOfSegsInv",		"BOOL" },
	{ "RampDeadbandInv",		"BOOL" },
	{ "SoakDeadbandInv",		"BOOL" },
	{ "CurrSegProgInv",		"BOOL" },
	{ "SoakTimeProgInv",		"BOOL" },
	{ "CurrSegOperInv",		"BOOL" },
	{ "SoakTimeOperInv",		"BOOL" },
	{ "RampValueInv",		"BOOL" },
	{ "SoakTimeInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"RATE_LIMITER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "IncRate",			"REAL" },
	{ "DecRate",			"REAL" },
	{ "ByPass",			"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "IncRateInv",			"BOOL" },
	{ "DecRateInv",			"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"REDUNDANT_INPUT",
	{
	{ "EnableIn",			"BOOL" },
	{ "ResetType",			"BOOL" },
	{ "ChannelA",			"BOOL" },
	{ "ChannelB",			"BOOL" },
	{ "CircuitReset",		"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "CI",				"BOOL" },
	{ "CRHO",			"BOOL" },
	{ "II",				"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"REDUNDANT_OUTPUT",
	{
	{ "EnableIn",			"BOOL" },
	{ "FeedbackType",		"BOOL" },
	{ "Enable",			"BOOL" },
	{ "Feedback1",			"BOOL" },
	{ "Feedback2",			"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "O1",				"BOOL" },
	{ "O2",				"BOOL" },
	{ "O1FF",			"BOOL" },
	{ "O2FF",			"BOOL" },
	{ "FP",				"BOOL" },

	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SCALE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "InRawMax",			"REAL" },
	{ "InRawMin",			"REAL" },
	{ "InEUMax",			"REAL" },
	{ "InEUMin",			"REAL" },
	{ "Limiting",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "MaxAlarm",			"BOOL" },
	{ "MinAlarm",			"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "InRawRangeInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"SEC_ORDER_CONTROLLER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Initialize",			"BOOL" },
	{ "InitialValue",		"REAL" },
	{ "Gain",			"REAL" },
	{ "WLag",			"REAL" },
	{ "WLead",			"REAL" },
	{ "ZetaLead",			"REAL" },
	{ "HighLimit",			"REAL" },
	{ "LowLimit",			"REAL" },
	{ "HoldHigh",			"BOOL" },
	{ "HoldLow",			"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "HighAlarm",			"BOOL" },
	{ "LowAlarm",			"BOOL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "GainInv",			"BOOL" },
	{ "WLagInv",			"BOOL" },
	{ "WLeadInv",			"BOOL" },
	{ "ZetaLeadInv",		"BOOL" },
	{ "HighLowLimsInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SELECT",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"REAL" },
	{ "In2",			"REAL" },
	{ "SelectorIn",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SELECTABLE_NEGATE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "NegateEnable",		"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SELECTED_SUMMER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"REAL" },
	{ "Gain1",			"REAL" },
	{ "Select1",			"BOOL" },
	{ "In2",			"REAL" },
	{ "Gain2",			"REAL" },
	{ "Select2",			"BOOL" },
	{ "In3",			"REAL" },
	{ "Gain3",			"REAL" },
	{ "Select3",			"BOOL" },
	{ "In4",			"REAL" },
	{ "Gain4",			"REAL" },
	{ "Select4",			"BOOL" },
	{ "In5",			"REAL" },
	{ "Gain5",			"REAL" },
	{ "Select5",			"BOOL" },
	{ "In6",			"REAL" },
	{ "Gain6",			"REAL" },
	{ "Select6",			"BOOL" },
	{ "In7",			"REAL" },
	{ "Gain7",			"REAL" },
	{ "Select7",			"BOOL" },
	{ "In8",			"REAL" },
	{ "Gain8",			"REAL" },
	{ "Select8",			"BOOL" },
	{ "Bias",			"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SELECT_ENHANCED",
	{
	{ "EnableIn",			"BOOL" },
	{ "In1",			"REAL" },
	{ "In2",			"REAL" },
	{ "In3",			"REAL" },
	{ "In4",			"REAL" },
	{ "In5",			"REAL" },
	{ "In6",			"REAL" },
	{ "In1Fault",			"BOOL" },
	{ "In2Fault",			"BOOL" },
	{ "In3Fault",			"BOOL" },
	{ "In4Fault",			"BOOL" },
	{ "In5Fault",			"BOOL" },
	{ "In6Fault",			"BOOL" },
	{ "InsUsed",			"DINT" },
	{ "SelectorMode",		"DINT" },
	{ "ProgSelector",		"DINT" },
	{ "OperSelector",		"DINT" },
	{ "ProgProgReq",		"BOOL" },
	{ "ProgOperReq",		"BOOL" },
	{ "ProgOverrideReq",		"BOOL" },
	{ "OperProgReq",		"BOOL" },
	{ "OperOperReq",		"BOOL" },
	{ "ProgValueReset",		"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{ "SelectedIn",			"DINT" },
	{ "ProgOper",			"BOOL" },
	{ "Override",			"BOOL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "InsFaulted",			"BOOL" },
	{ "InsUsedInv",			"BOOL" },
	{ "SelectorModeInv",		"BOOL" },
	{ "ProgSelectorInv",		"BOOL" },
	{ "OperSelectorInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SERIAL_PORT_CONTROL",
	{
	{ "LEN",			"DINT" },
	{ "POS",			"DINT" },
	{ "ERR",			"DINT" },
	{ "EN",				"BOOL" },
	{ "EU",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "DM",				"BOOL" },
	{ "EM",				"BOOL" },
	{ "ER",				"BOOL" },
	{ "UL",				"BOOL" },
	{ "RN",				"BOOL" },
	{ "FD",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SFC_ACTION",
	{
	{ "Status",			"DINT" },
	{ "A",				"BOOL" },
	{ "Q",				"BOOL" },
	{ "PRE",			"DINT" },
	{ "T",				"DINT" },
	{ "Count",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},


	{
		"SFC_STEP",
	{
	{ "Status",			"DINT" },
	{ "X",				"BOOL" },
	{ "FS",				"BOOL" },
	{ "SA",				"BOOL" },
	{ "LS",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "OV",				"BOOL" },
	{ "AlarmEn",			"BOOL" },
	{ "AlarmLow",			"BOOL" },
	{ "AlarmHigh",			"BOOL" },
	{ "Reset",			"BOOL" },
	{ "PRE",			"DINT" },
	{ "T",				"DINT" },
	{ "TMax",			"DINT" },
	{ "Count",			"DINT" },
	{ "LimitLow",			"DINT" },
	{ "LimitHigh",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SFC_STOP",
	{
	{ "X",				"DINT" },
	{ "Reset",			"BOOL" },
	{ "Count",			"DINT" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"SPLIT_RANGE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "CycleTime",			"REAL" },
	{ "MaxHeatIn",			"REAL" },
	{ "MinHeatIn",			"REAL" },
	{ "MaxCoolIn",			"REAL" },
	{ "MinCoolIn",			"REAL" },
	{ "MaxHeatTime",		"REAL" },
	{ "MinHeatTime",		"REAL" },
	{ "MaxCoolTime",		"REAL" },
	{ "MinCoolTime",		"REAL" },
	{ "EnableOut",			"BOOL" },
	{ "HeatOut",			"BOOL" },
	{ "CoolOut",			"BOOL" },
	{ "HeatTimePercent",		"REAL" },
	{ "CoolTimePercent",		"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "CycleTimeInv",		"BOOL" },
	{ "MaxHeatTimeInv",		"BOOL" },
	{ "MinHeatTimeInv",		"BOOL" },
	{ "MaxCoolTimeInv",		"BOOL" },
	{ "MinCoolTimeInv",		"BOOL" },
	{ "HeatSpanInv",		"BOOL" },
	{ "CoolSpanInv",		"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},
	
	{
		"STRING",
	{
	{ "LEN",			"DINT"		},
	{ "DATA",			"SINT[82]"	},
	{  END_MARKER,			END_MARKER	},
	}
	},

	{
		"S_CURVE",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "Initialize",			"BOOL" },
	{ "InitialValue",		"REAL" },
	{ "AbsAlgRamp",			"BOOL" },
	{ "AccelRate",			"REAL" },
	{ "DecelRate",			"REAL" },
	{ "JerkRate",			"REAL" },
	{ "HoldMode",			"BOOL" },
	{ "HoldEnable",			"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OversampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "S_Mode",			"BOOL" },
	{ "Out",			"REAL" },
	{ "Rate",			"REAL" },
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "AccelRateInv",		"BOOL" },
	{ "DecelRateInv",		"BOOL" },
	{ "JerkRateInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"TIMER",	
	{
	{ "PRE",			"DINT" },
	{ "ACC",			"DINT" },
	{ "CTL",			"DINT" },
	{ "EN",				"BOOL" },
	{ "TT",				"BOOL" },
	{ "DN",				"BOOL" },
	{ "FS",				"BOOL" },
	{ "LS",				"BOOL" },
	{ "OV",				"BOOL" },
	{ "ER",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	}
	},

	{
		"TOTALIZER",
	{
	{ "EnableIn",			"BOOL" },
	{ "In",				"REAL" },
	{ "InFault",			"BOOL" },
	{ "TimeBase",			"DINT" },
	{ "Gain",			"REAL" },
	{ "ResetValue",			"REAL" },
	{ "Target",			"REAL" },
	{ "TargetDev1",			"REAL" },
	{ "TargetDev2",			"REAL" },
	{ "LowInCutOff",		"REAL" },
	{ "ProgProgReq",		"BOOL" },
	{ "ProgOperReq",		"BOOL" },
	{ "ProgStartReq",		"BOOL" },
	{ "ProgStopReq",		"BOOL" },
	{ "ProgResetReq",		"BOOL" },
	{ "OperProgReq",		"BOOL" },
	{ "OperOperReq",		"BOOL" },
	{ "OperStartReq",		"BOOL" },
	{ "OperStopReq",		"BOOL" },
	{ "ProgValueReset",		"BOOL" },
	{ "TimingMode",			"DINT" },
	{ "OverSampleDT",		"REAL" },
	{ "RTSTime",			"DINT" },
	{ "RTSTimeStamp",		"DINT" },
	{ "EnableOut",			"BOOL" },
	{ "Total",			"REAL" },
	{ "OldTotal",			"REAL" },	
	{ "ProgOper",			"BOOL" },
	{ "RunStop",			"BOOL" },
	{ "ProgResetDone",		"BOOL" },
	{ "TargetFlag",			"BOOL" },
	{ "TargetDev1Flag",		"BOOL" },
	{ "TargetDev2Flag",		"BOOL" },
	{ "LowInCutOffFlag",		"BOOL" },	
	{ "DeltaT",			"REAL" },
	{ "Status",			"DINT" },
	{ "InstructFault",		"BOOL" },
	{ "InFaulted",			"BOOL" },
	{ "TimeBaseInv",		"BOOL" },
	{ "TimingModeInv",		"BOOL" },
	{ "RTSMissed",			"BOOL" },
	{ "RTSTimeInv",			"BOOL" },
	{ "RTSTimeStampInv",		"BOOL" },
	{ "DeltaTInv",			"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"TWO_HAND_RUN_STATION",
	{
	{ "EnableIn",			"BOOL" },
	{ "ActivePinType",		"BOOL" },
	{ "ActivePin",			"BOOL" },
	{ "RightButtonNormallyOpen",	"BOOL" },
	{ "RightButtonNormallyClosed",	"BOOL" },
	{ "LeftButtonNormallyOpen",	"BOOL" },
	{ "LeftButtonNormallyClosed",	"BOOL" },
	{ "FaultReset",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "BP",				"BOOL" },
	{ "SA",				"BOOL" },
	{ "BT",				"BOOL" },
	{ "CB",				"BOOL" },
	{ "SAF",			"BOOL" },
	{ "RBF",			"BOOL" },
	{ "LBF",			"BOOL" },
	{ "FP",				"BOOL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	{
		"UP_DOWN_ACCUM",
	{
	{ "EnableIn",			"BOOL" },
	{ "Initialize",			"BOOL" },
	{ "InitialValue",		"REAL" },
	{ "InPlus",			"REAL" },
	{ "InMinus",			"REAL" },
	{ "Hold",			"BOOL" },
	{ "EnableOut",			"BOOL" },
	{ "Out",			"REAL" },
	{  END_MARKER,			END_MARKER },
	},
	},

	};

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Base Item List
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kTypeList, CABL5kList);

// Constructor

CABL5kTypeList::CABL5kTypeList(void)
{
	ScanAtomic();

	ScanPredefined();
	}

// Look Ups

BOOL CABL5kTypeList::IsAtomicType(CString const &Type)
{
	CString Base = Type.Left(Type.FindRev('['));
	
	INDEX  Index = m_AtomicRev.FindName(Base);
	
	return !m_AtomicRev.Failed(Index);
	}

BOOL CABL5kTypeList::IsPredefinedType(CString const &Type)
{
	CString Base = Type.Left(Type.FindRev('['));

	for( UINT n = 0; n < elements(m_PredefinedList); n ++ ) {

		CPredefinedEntry &Entry = m_PredefinedList[n];
		
		if( Base == Entry.m_pName )
			return TRUE;
		}
	
	return FALSE;
	}

CString CABL5kTypeList::GetAtomicName(CAddress const &Addr)
{
	return m_AtomicFwd[Addr.a.m_Type];
	}

// Parsing

BOOL CABL5kTypeList::IsArrayType(CString const &Type)
{
	UINT p1;

	if( (p1 = Type.FindRev('.')) == NOTHING) {
		
		p1 = 0;
		}

	return Type.Find('[', p1) < NOTHING;
	}

BOOL CABL5kTypeList::IsAtomicArray(CString const &Type)
{
	return IsAtomicType(Type) && IsArrayType(Type);
	}

DWORD CABL5kTypeList::GetAtomicType(CString Type)
{
	UINT p1;

	if( (p1 = Type.FindRev('[')) < NOTHING ) {

		INDEX n = m_AtomicRev.FindName(Type.Left(p1 + 1));

		if( !m_AtomicRev.Failed(n) ) {

			return m_AtomicRev.GetData(n);
			}
		
		Type = Type.Left(p1);
		}

	INDEX n = m_AtomicRev.FindName(Type);

	if( !m_AtomicRev.Failed(n) ) {

		return m_AtomicRev.GetData(n);
		}

	return NOTHING;
	}

// Persistance

void CABL5kTypeList::Save(CTreeFile &File)
{
	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CABL5kItem *pItem = GetItem(Index);

		if( IsUserType(pItem->m_Name) ) {

			if( !m_Class ) {

				CString Name = pItem->GetClassName();

				File.PutObject(Name.Mid(1));
				}
			else
				File.PutElement();

			pItem->Save(File);

			File.EndObject();
			}
		
		m_List.GetNext(Index);
		}
	}

// Debug

void CABL5kTypeList::ShowList(void)
{
	CABL5kList::ShowList(this);
	}

// Overridables

BOOL CABL5kTypeList::LoadConfig(CError &Error, CABL5kController &Config)
{
	DeleteAllItems(TRUE);

	ScanAtomic();

	ScanPredefined();

	CList <CABL5kComponent *> &Types = Config.m_Types;

	INDEX Index = Types.GetHead();

	while( !Types.Failed(Index) ) {

		CABL5kComponent *pType = Types[Index];

		CString   Name = pType->m_Name;

		if( FindName(Name) ) {

			Error.Set( CPrintf("The data type <%s> already exists", Name), 
				   0
				   );
			
			ThrowError();
			}
		
		CABL5kStructItem *pCreate = New CABL5kStructType(Name);

		AppendItem(pCreate);

		CArray <CABTypeDef> &Members = pType->m_Members;

		for( UINT n = 0; n < Members.GetCount(); n ++ ) {

			CABTypeDef &Member = (CABTypeDef &) Members[n];

			CString Name = Member.m_Name;

			CString Type = Member.m_Type;

			UINT p1, p2;

			if( (p1 = Name.FindRev('[')) < NOTHING ) {

				if( (p2 = Name.FindRev(']', p1)) < NOTHING ) {
				
					Type = Type + Name.Mid(p1, p2 - p1 + 1);

					Name = Name.Left(p1);
					}
				}

			pCreate->AddMember(New CABL5kStructType(Name, Type));			
			}

		Types.GetNext(Index);
		}

	//ShowList();

	return TRUE;
	}

// Implementation

void CABL5kTypeList::ScanAtomic(void)
{
	m_AtomicFwd.Empty();

	m_AtomicRev.Empty();

	for( UINT n = 0; n < elements(m_AtomicList); n ++ ) {
		
		CAtomicEntry &Entry = m_AtomicList[n];

		AppendItem(New CABL5kAtomicType(Entry.m_pName));
		
		//
		m_AtomicFwd.Insert(Entry.m_Type,  Entry.m_pName);

		m_AtomicRev.Insert(Entry.m_pName, Entry.m_Type);
		}
	}

void CABL5kTypeList::ScanPredefined(void)
{
	for( UINT n = 0; n < elements(m_PredefinedList); n ++ ) {
		
		CPredefinedEntry &Entry = m_PredefinedList[n];

		CABL5kStructItem *pType = New CABL5kStructType(Entry.m_pName);

		AppendItem(pType);

		if( pType ) {
			
			for( UINT m = 0; m < elements(Entry.m_Elements); m ++ ) {

				CStructEntry &Elem = Entry.m_Elements[m];

				if( strcmp(Elem.m_pType, END_MARKER) ) {

					pType->AddMember(New CABL5kStructType(Elem.m_pName, Elem.m_pType));
					}
				else
					break;				
				}
			}
		}
	}

BOOL CABL5kTypeList::IsUserType(CString Name)
{
	if( IsAtomicType(Name) ) {
		
		return FALSE;
		}

	if( IsPredefinedType(Name) ) {
		
		return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Atomic Data Type
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kAtomicType, CABL5kAtomicItem);

// Constructor

CABL5kAtomicType::CABL5kAtomicType(void)
{
	}

CABL5kAtomicType::CABL5kAtomicType(CString Name, CString Type)
{
	m_Name = Name;

	m_Type = Type;
	}

CABL5kAtomicType::CABL5kAtomicType(CString Name, CString Type, UINT uType)
{
	m_Name = Name;

	m_Type = Type;
	}

CABL5kAtomicType::CABL5kAtomicType(CString Name)
{
	m_Name = Name;
	}

//////////////////////////////////////////////////////////////////////////
//
// AB Logix 5000 Structure Data Type
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kStructType, CABL5kStructItem);

// Constructor

CABL5kStructType::CABL5kStructType(void)
{
	m_pElements = New CABL5kList;
	}

CABL5kStructType::CABL5kStructType(CString Name)
{
	m_Name      = Name;

	m_pElements = New CABL5kList;
	}

CABL5kStructType::CABL5kStructType(CString Name, CString Type)
{
	m_Name      = Name;

	m_Type      = Type;

	m_pElements = New CABL5kList;
	}

// End of File
