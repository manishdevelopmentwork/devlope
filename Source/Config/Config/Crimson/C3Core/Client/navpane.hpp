
//////////////////////////////////////////////////////////////////////////
//
// Navigation Pane Window
//

class /*DLLAPI*/ CNavPaneWnd : public CPaneWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructor
		CNavPaneWnd(void);

	protected:
		// Static Data
		static UINT timerCheck;

		// Layout Constants
		static int const sizeCategory;
		static int const sizeToolbar;
		static int const sizeSplit;

		// Data Members
		CAccelerator m_Accel1;
		CAccelerator m_Accel2;
		CString      m_KeyName;
		CDatabase *  m_pDbase;
		CMetaItem *  m_pSystem;
		CCursor      m_Cursor1;
		CCursor      m_Cursor2;
		CRect	     m_Split;
		UINT         m_uCats;
		PCAT         m_pCats;
		UINT	     m_uShow;
		UINT	     m_uSelect;
		UINT	     m_uHover;
		BOOL	     m_fPress;
		int          m_nTrackOrg;
		int          m_nTrackPos; 
		int          m_nTrackMin;
		int          m_nTrackMax; 
		UINT	     m_uCapture;

		// Overridables
		void OnAttach(void);
		BOOL OnNavigate(CString Nav);
		void OnExec(CCmd *pCmd);
		void OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerator
		BOOL OnAccelerator(MSG &Msg);
		
		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		void OnPaint(void);
		void OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnSetCurrent(BOOL fCurrent);
		BOOL OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage);
		void OnLButtonDown(UINT uFlags, CPoint Pos);
		void OnLButtonDblClk(UINT uFlags, CPoint Pos);
		void OnMouseMove(UINT uFlags, CPoint Pos);
		void OnLButtonUp(UINT uFlags, CPoint Pos);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Command Handlers
		BOOL OnGoGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);

		// Overridables
		CRect GetClientRect(void);

		// Drawing Helpers
		void DrawSplit(CDC &DC);
		void DrawCategory(CDC &DC, CRect Draw, UINT uCat);
		void ShowDragBar(CDC &DC);

		// Implementation
		BOOL Select(CString Nav);
		void Select(UINT uSelect);
		void AttachItemView(void);
		void InvalidateCat(UINT uCat);
		UINT CheckHover(CPoint Pos);
		UINT CalcShowCount(void);
		void FindConfig(CRegKey &Key);
		void LoadConfig(void);
		void SaveConfig(void);
	};		

// End of File
