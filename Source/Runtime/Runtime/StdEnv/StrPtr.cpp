
#include "Intern.hpp"

#include "StrPtr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/_YKgE

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "String.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

// Constructors

CStrPtr::CStrPtr(CString const &That)
{
	AfxAssert(FALSE);

	m_pText = NULL;
	}

UINT CStrPtr::GetHashValue(void) const
{
	UINT uHash = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {
	
		uHash = (uHash << 5) + uHash;
		
		uHash = uHash + m_pText[n];
		}
		
	return uHash;
	}

// Read-Only Access

TCHAR CStrPtr::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : TCHAR(0);
	}

// Array Conversion

UINT CStrPtr::GetCharCodes(CWordArray &Code)
{
	UINT uLen = GetLength();

	Code.Empty();

	Code.Expand(uLen);

	for( UINT n = 0; n < uLen; n++ ) {

		Code.Append(m_pText[n]);
		}

	return uLen;
	}

// Comparison Functions

int CStrPtr::GetScore(PCTXT pText) const
{
	if( strcasecmp(m_pText, pText) ) {
	
		UINT n = strlen(pText);
		
		if( strncasecmp(m_pText, pText, n) ) {

			return 0;
			}
			
		return n;
		}
		
	return 10000;
	}

// Comparison Helper

int AfxCompare(CStrPtr const &a, CStrPtr const &b)
{
	return strcasecmp(a.m_pText, b.m_pText);
	}

// Substring Extraction

CString CStrPtr::Left(UINT uCount) const
{
	UINT uLen = GetLength();
	
	MakeMin(uCount, uLen);

	return CString(m_pText, uCount);
	}

CString CStrPtr::Right(UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uCount, uLen);

	PCTXT pSrc = m_pText + uLen - uCount;

	return CString(pSrc, uCount);
	}

CString CStrPtr::Mid(UINT uPos, UINT uCount) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	MakeMin(uCount, uLen - uPos);

	PCTXT pSrc = m_pText + uPos;

	return CString(pSrc, uCount);
	}

CString CStrPtr::Mid(UINT uPos) const
{
	UINT uLen = GetLength();

	MakeMin(uPos, uLen);

	PCTXT pSrc = m_pText + uPos;

	return CString(pSrc, uLen - uPos);
	}

// Case Conversion

CString CStrPtr::ToUpper(void) const
{
	CString Result(ThisObject);

	Result.MakeUpper();

	return Result;
	}

CString CStrPtr::ToLower(void) const
{
	CString Result(ThisObject);

	Result.MakeLower();

	return Result;
	}

// Searching

UINT CStrPtr::Find(TCHAR cChar) const
{
	AfxAssert(cChar);
	
	PTXT pFind = strchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, TCHAR cQuote) const
{
	AfxAssert(cChar);

	AfxAssert(cQuote);

	int s = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cQuote ) {

			s = !s;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, TCHAR cOpen, TCHAR cClose) const
{
	AfxAssert(cChar);

	AfxAssert(cOpen);

	AfxAssert(cClose);

	int s = 0;

	for( UINT n = 0; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cOpen ) {

			s++;
			}

		if( c == cClose ) {

			s--;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos <= GetLength());
	
	PTXT pFind = strchr(m_pText + uPos, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::Find(TCHAR cChar, UINT uPos, TCHAR cQuote) const
{
	AfxAssert(cChar);

	AfxAssert(cQuote);

	AfxAssert(uPos <= GetLength());

	int s = 0;

	for( UINT n = uPos; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cQuote ) {

			s = !s;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::Find(TCHAR cChar, UINT uPos, TCHAR cOpen, TCHAR cClose) const
{
	AfxAssert(cChar);

	AfxAssert(cOpen);

	AfxAssert(cClose);

	AfxAssert(uPos <= GetLength());

	int s = 0;

	for( UINT n = uPos; m_pText[n]; n++ ) {

		TCHAR c = m_pText[n];

		if( !s ) {
			
			if( c == cChar ) {

				return n;
				}
			}

		if( c == cOpen ) {

			s++;
			}

		if( c == cClose ) {

			s--;
			}
		}
	
	return NOTHING;
	}

UINT CStrPtr::FindRev(TCHAR cChar) const
{
	AfxAssert(cChar);

	PTXT pFind = strrchr(m_pText, cChar);
	
	return pFind ? pFind - m_pText : NOTHING;
	}
	
UINT CStrPtr::FindRev(TCHAR cChar, UINT uPos) const
{
	AfxAssert(cChar);

	AfxAssert(uPos < GetLength());

	for( INT n = uPos; n >= 0; n-- ) {

		if( m_pText[n] == cChar ) {

			return n;
			}
		}

	return NOTHING;
	}
	
UINT CStrPtr::Find(PCTXT pText) const
{
	AfxAssertStringPtr(pText);
			
	PTXT pFind = strstr(m_pText, pText);
		
	return pFind ? pFind - PTXT(m_pText) : NOTHING;
	}
	
UINT CStrPtr::Find(PCTXT pText, UINT uPos) const
{
	AfxAssertStringPtr(pText);
			
	AfxAssert(uPos <= GetLength());

	PTXT pFind = strstr(m_pText + uPos, pText);
		
	return pFind ? pFind - PTXT(m_pText) : NOTHING;
	}
	
UINT CStrPtr::FindNot(PCTXT pList) const
{
	AfxAssertStringPtr(pList);

	UINT uFind = strspn(m_pText, pList);
	
	return uFind;
	}

UINT CStrPtr::FindNot(PCTXT pList, UINT uPos) const
{
	AfxAssertStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = strspn(m_pText + uPos, pList);
	
	return uFind ? uFind + uPos : 0;
	}

UINT CStrPtr::FindOne(PCTXT pList) const
{
	AfxAssertStringPtr(pList);

	UINT uFind = strcspn(m_pText, pList);
	
	return m_pText[uFind] ? uFind : NOTHING;
	}

UINT CStrPtr::FindOne(PCTXT pList, UINT uPos) const
{
	AfxAssertStringPtr(pList);
			
	AfxAssert(uPos <= GetLength());

	UINT uFind = uPos + strcspn(m_pText + uPos, pList);
	
	return m_pText[uFind] ? uFind : NOTHING;
}

// Searching

UINT CStrPtr::Search(PCTXT pText, UINT uMethod) const
{
	return Search(pText, 0, uMethod);
	}

UINT CStrPtr::Search(PCTXT pText, UINT uFrom, UINT uMethod) const
{
	UINT uFind;

	switch( LOBYTE(uMethod) ) {

		case searchIsEqualTo:

			if( uMethod & searchMatchCase ) {

				if( !strcmp(pText, m_pText) ) {

					return 0;
					}

				return NOTHING;
				}

			if( !strcasecmp(pText, m_pText) ) {

				return 0;
				}

			return NOTHING;

		case searchStartsWith:

			if( uMethod & searchMatchCase ) {

				if( !strncmp(pText, m_pText, strlen(pText)) ) {

					return 0;
					}

				return NOTHING;
				}

			if( !strncasecmp(pText, m_pText, strlen(pText)) ) {

				return 0;
				}

			return NOTHING;

		case searchContains:

			uFind = strlen(pText);

			for(;;) {

				PCTXT pFind;

				if( uMethod & searchMatchCase ) {

					pFind = strstr(m_pText + uFrom, pText);
					}
				else
					pFind = strstr(m_pText + uFrom, pText); // !!!

				if( pFind ) {

					if( uMethod & searchWholeWords ) {

						if( pFind > m_pText + uFrom && wisalnum(pFind[-1]) ) {

							uFrom = pFind - m_pText + uFind;

							continue;
							}

						if( pFind[uFind] && wisalnum(pFind[uFind]) ) {

							uFrom = pFind - m_pText + uFind;

							continue;
							}
						}

					return pFind - m_pText;
					}

				return NOTHING;
				}

			break;
		}

	return NOTHING;
	}

// Partials

BOOL CStrPtr::StartsWith(PCTXT pText) const
{
	UINT c1 = strlen(m_pText);

	UINT c2 = strlen(pText);

	if( c1 >= c2 ) {

		return !strncasecmp(m_pText, pText, c2);
		}

	return FALSE;
	}

BOOL CStrPtr::EndsWith(PCTXT pText) const
{
	UINT c1 = strlen(m_pText);

	UINT c2 = strlen(pText);

	if( c1 >= c2 ) {

		return !strncasecmp(m_pText + c1 - c2, pText, c2);
		}

	return FALSE;
	}

BOOL CStrPtr::StartsWith(TCHAR cChar) const
{
	return m_pText[0] == cChar;
	}

BOOL CStrPtr::EndsWith(TCHAR cChar) const
{
	return m_pText[0] && m_pText[strlen(m_pText)-1] == cChar;
	}

// Counting

UINT CStrPtr::Count(TCHAR cChar) const
{
	UINT c = 0;
	
	for( UINT n = 0; m_pText[n]; n++ ) {

		if( m_pText[n] == cChar ) {

			c++;
			}
		}
		
	return c;
	}

// Diagnostics

void CStrPtr::AssertValid(void) const
{
	AfxAssertReadPtr(this, sizeof(ThisObject));
	
	AfxAssertStringPtr(m_pText);
	}

// End of File
