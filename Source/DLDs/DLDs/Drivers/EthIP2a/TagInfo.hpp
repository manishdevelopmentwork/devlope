#include "Intern.hpp"

#ifndef INCLUDE_TagInfo_HPP
#define INCLUDE_TagInfo_HPP

#include "CipPath.hpp"

#include "WatchList.hpp"

#include "CipClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CWatchList;

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Native Tag Information
//

class CTagInfo : public CCipCommon
{
	public:
		// Constructor
		CTagInfo(void);

		// Destructor
		~CTagInfo(void);

		void SetClient(CCipClient *pClient);

		// Tag reading/writing
		UINT Read(AREF Addr, PDWORD pData, UINT uCount);
		UINT ReadTable(AREF Addr, PDWORD pData, UINT uCount);
		UINT ReadBitTable(AREF Addr, PDWORD pData, UINT uCount);
		UINT Write(AREF Addr, PDWORD pData, UINT uCount);
		UINT WriteBitTable(AREF Addr, PDWORD pData, UINT uCount);

		// Tag management
		BOOL ShouldTouch(void);
		BOOL IsWatched(void);
		void Touch(void);
		void ResetPos(void);
		void ClearTouch(void);
		UINT GetSize(void);
		BOOL FindInfo(void);
		void SetDim(UINT uDim, UINT uVal);

		CTagInfo   * m_pNext;
		CTagInfo   * m_pPrev;

		PTXT         m_pName;
		CWatchList * m_pWatch;

	protected:
		
		// Data Members
		CCipPath     m_Path;
		UINT	     m_uSize;
		UINT	     m_uType;
		UINT	     m_uExtra;
		BOOL	     m_fTouch;
		UINT	     m_uPos;
		WORD	     m_wMembers;
		WORD	     m_wDims;
		WORD	     m_XDim;
		WORD	     m_YDim;
		WORD	     m_ZDim;
		CCipClient * m_pClient;

		// Implementation
		CCipPath ParseDims(UINT uOffset);
		CCipPath ParseWatchDims(void);
		void GetTokenDims(PTXT pTok, UINT &x, UINT &y, UINT &z);
		UINT CountDots(PCTXT pName);

		void UpdateWatch(UINT uVal);

		friend class CWatchList;
		
};

// End of File

#endif
