
#include "daprops.h"

//////////////////////////////////////////////////////////////////////////
//
// Manticore DARO8 Module
//
// Copyright (c) 2001-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DARO8PROPS_H

#define	INCLUDE_DARO8PROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Installation
//

// Object : DO

#define PROPID_OUTPUT(n)	MAKEPROP(TYPE_DO, n)

#define	PROPID_OUTPUT1		PROPID_OUTPUT(1)
#define	PROPID_OUTPUT2		PROPID_OUTPUT(2)
#define	PROPID_OUTPUT3		PROPID_OUTPUT(3)
#define	PROPID_OUTPUT4		PROPID_OUTPUT(4)
#define	PROPID_OUTPUT5		PROPID_OUTPUT(5)
#define	PROPID_OUTPUT6		PROPID_OUTPUT(6)
#define	PROPID_OUTPUT7		PROPID_OUTPUT(7)
#define	PROPID_OUTPUT8		PROPID_OUTPUT(8)

// Object : DO Config

#define PROPID_ENABLE(n)	MAKEPROP(TYPE_BOOL, PROPIDX(n, 1))

#define PROPID_ENABLE1		PROPID_ENABLE(1)
#define PROPID_ENABLE2		PROPID_ENABLE(2)
#define PROPID_ENABLE3		PROPID_ENABLE(3)
#define PROPID_ENABLE4		PROPID_ENABLE(4)
#define PROPID_ENABLE5		PROPID_ENABLE(5)
#define PROPID_ENABLE6		PROPID_ENABLE(6)
#define PROPID_ENABLE7		PROPID_ENABLE(7)
#define PROPID_ENABLE8		PROPID_ENABLE(8)

#define PROPID_POMODE(n)	MAKEPROP(TYPE_BYTE, PROPIDX(n, 2))

#define PROPID_POMODE1		PROPID_POMODE(1)
#define PROPID_POMODE2		PROPID_POMODE(2)
#define PROPID_POMODE3		PROPID_POMODE(3)
#define PROPID_POMODE4		PROPID_POMODE(4)
#define PROPID_POMODE5		PROPID_POMODE(5)
#define PROPID_POMODE6		PROPID_POMODE(6)
#define PROPID_POMODE7		PROPID_POMODE(7)
#define PROPID_POMODE8		PROPID_POMODE(8)

// End of File

#endif
