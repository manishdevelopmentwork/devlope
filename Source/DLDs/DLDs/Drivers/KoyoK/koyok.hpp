
//////////////////////////////////////////////////////////////////////////
//
// Koyo K Sequence Constants
//

#define TIMEOUT 1000

//////////////////////////////////////////////////////////////////////////
//
// Koyo K Sequence Driver
//

class CKoyoKSequenceDriver : public CMasterDriver
{
	public:
		// Constructor
		CKoyoKSequenceDriver(void);

		// Destructor
		~CKoyoKSequenceDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BYTE m_PingEnable;
			WORD m_PingReg;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE m_bTxSend[256];
		BYTE m_bTxBuff[256];
		BYTE m_bRxBuff[256];
		UINT m_uPtr;
		UINT m_uSend;
		BYTE m_bCheck;
	
		// Port Access
		void Put(BYTE b);
		void Put(void);
		UINT Get(void);

		// Frame Building
		void StartHeader(void);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddData(PDWORD pData, UINT uCount);
		void AddLongData(PDWORD pData, UINT uCount);
		
		// Opcode Handlers
		BOOL Read (UINT uAddr, PDWORD pData, UINT uCount, UINT uType);
		BOOL Write(UINT uAddr, PDWORD pData, UINT uCount, UINT uType);
		
		// Transport Layer
		BOOL Transact(BOOL fEOT);
		BOOL TxHeader(void);
		void TxHeaderData(void);
		BOOL RxData(void);

		// Checksum Handler
		void ClearCheck(void);
		void AddToCheck(BYTE b);
		
		// Data Link Layer
		void TxAck(void);
		void TxNak(void);
		void TxEOT(void);
		void TxEnq(void);
		BOOL RxEOT(void);
		UINT RxAck(void);
		BOOL RxAckEnq(void);

		// Helpers
		BOOL IsLong(UINT uType);
		BOOL GetLong(PDWORD pData, UINT uCount);
		UINT FindCount(UINT uType);
		
	};

// End of File
