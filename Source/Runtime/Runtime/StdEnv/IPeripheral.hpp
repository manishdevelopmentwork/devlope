
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_IPeripheral_HPP

#define INCLUDE_IPeripheral_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 14 -- Peripheral Bus & Devices 
//

interface ISpi;
interface II2c;

//////////////////////////////////////////////////////////////////////////
//
// SPI Config
//

struct CSpiConfig
{
	UINT	m_uFreq;
	BOOL	m_fDataIdle;
	BOOL	m_fClockIdle;
	UINT	m_uClockPol;
	UINT	m_uClockPha;
	UINT	m_uSelect;
	BOOL	m_fBurst;
	};

//////////////////////////////////////////////////////////////////////////
//
// SPI Interface Bus
//

interface ISpi : public IDevice
{
	// https://

	AfxDeclareIID(14, 1);

	virtual BOOL METHOD Init(UINT uChan, CSpiConfig const &Config)				 = 0;
	virtual BOOL METHOD Send(UINT uChan, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData) = 0;
	virtual BOOL METHOD Recv(UINT uChan, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// I2C Interface Bus
//

interface II2c : public IDevice
{
	// https://

	AfxDeclareIID(14, 2);
	
	virtual BOOL METHOD Lock(UINT uTime)							 = 0;
	virtual void METHOD Free(void)								 = 0;
	virtual BOOL METHOD Send(BYTE bChip, PCBYTE pHead, UINT uHead, PCBYTE pData, UINT uData) = 0;
	virtual BOOL METHOD Recv(BYTE bChip, PCBYTE pHead, UINT uHead, PBYTE  pData, UINT uData) = 0;
	};

// End of File

#endif
