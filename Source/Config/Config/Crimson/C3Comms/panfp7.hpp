//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PANFP7_HPP
	
#define	INCLUDE_PANFP7_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPanFp7Space;

//////////////////////////////////////////////////////////////////////////
//
// Enumerations
//

enum Space {

	spaceDT  =  1,
	spaceLD  =  3,
	spaceC   =  9,
	spaceT   = 10,
	spaceR   = 11,
	spaceL   = 13,
	spaceX   = 14,
	spaceY   = 15,
	spaceWR  = 16,
	spaceWL  = 18,
	spaceWX	 = 19,
	spaceWY  = 20,
	spaceUM  = 41,
	spaceSD  = 42,
	spaceS   = 43,
	spaceP   = 44,
	spaceE   = 45,
	spaceIN  = 46,
	spaceOT  = 47,
	spaceI   = 48,
	spaceTS  = 49,
	spaceTE  = 50,
	spaceCS  = 51,
	spaceCE  = 52,
	spaceWI  = 53,
	spaceWO  = 54,
	spaceWS  = 55,
	spaceLER = 239,
	spaceLEC = 240,
	};

//////////////////////////////////////////////////////////////////////////
//
// FP Map Structure
//

struct CFpMap {

	CAddress m_Addr;
	DWORD    m_Map;
	WORD     m_Extent;
		
	bool operator ==(const CFpMap &That) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// FP Type Definitions
//

typedef CArray<CFpMap> CFpMapArray;

//////////////////////////////////////////////////////////////////////////
//
// FP Map Sort Helper
//

inline int AfxCompare(CFpMap const &m1, CFpMap const &m2)
{
	BOOL fTest = (m1.m_Addr.a.m_Type == m2.m_Addr.a.m_Type);

	if( fTest ) {

		if( m1.m_Map > m2.m_Map )		return +1;

		if( m1.m_Map < m2.m_Map )		return -1;
		}

	if( m1.m_Addr.a.m_Type > m2.m_Addr.a.m_Type )	return +1;

	if( m1.m_Addr.a.m_Type < m2.m_Addr.a.m_Type )	return -1;

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Limits
//

#define MakeMixedMax(x)		x = x | ((x & 0xFFFFF) << 4) | 0xF;

#define MakeSlotMax(x)		x = x | (999 << 22);

/////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Driver Options
//

class CPanFp7DriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPanFp7DriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
				
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Device Options
//

class CPanFp7DeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPanFp7DeviceOptions(void);

		// Download Support
		void PrepareData(void);
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void Load(CTreeFile &Tree);
		void Save(CTreeFile &Tree);

		// Access
		void  FindMap(CAddress &Addr, DWORD dwMap);
		DWORD GetMap(CAddress Addr);
		void  EmptyMap(void);
		void  UpdateMap(CAddress Addr, INT nSize);

		// Data Members
		CFpMapArray m_MapArray;
	
	protected:
		// Index References
		UINT m_UM;
		UINT m_IN;
		UINT m_OT;
		UINT m_WI;
		UINT m_WO;

		// Data Members
		BOOL m_fUpdate;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void  MakeInitDataMap(CInitData &Init);
		void  SaveMap(CTreeFile &Tree);
		void  LoadMap(CTreeFile &Tree);
		void  GetNextRef(CAddress &Ref, UINT uTable, UINT uType, INT nSize);
		void  Increment(UINT &uIndex, UINT uType, INT nSize);
		void  IncrementRef(UINT uTable, UINT uType, INT nSize);
		BOOL  IsLastRef(CAddress Addr, INT nSize);

		// Helpers
		void PrintMap(void);

	};

//////////////////////////////////////////////////////////////////////////
//
//  Panasonic FP7 MEWTOCOL7-COM Serial Master Device Options
//

class CPanFp7SerialMasterDeviceOptions : public CPanFp7DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPanFp7SerialMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Station;
					
	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};


//////////////////////////////////////////////////////////////////////////
//
//  Panasonic FP7 MEWTOCOL7-COM TCP Master Device Options
//

class CPanFp7TcpMasterDeviceOptions : public CPanFp7DeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPanFp7TcpMasterDeviceOptions(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Addr;
		UINT m_Addr2;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Ping;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
			
	protected:

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Panasonic FP7 MEWTOCOL7-COM Master Driver
//

class CPanFp7MasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CPanFp7MasterDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Notifications
		void NotifyInit(CItem * pConfig);
		void NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL DoExpandAddress(CString &Text, UINT uValue, UINT uType, CSpace * pSpace);
		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Access
		BOOL    IsMixed(UINT uTable);
		BOOL    IsExtended(UINT uTable);
		BOOL    HasSlot(UINT uTable);
		WORD    GetSlot(DWORD dwMap);
		void    SetSlot(UINT uValue, DWORD &dwMap, UINT uTable);
		DWORD   GetExtended(DWORD dwMap);
		DWORD   GetExtended(CAddress Addr);
		void    SetExtended(UINT uValue, DWORD &dwMap, UINT uTable);
		BOOL    SetExtended(UINT uValue, CAddress &Addr);
		DWORD   GetMixedBit(DWORD dwMap);
		DWORD   GetMixedDec(DWORD dwMap);
		void    SetMixed(UINT uBit, CAddress Addr, DWORD &dwMap);
		BOOL    SetMixed(UINT uValue, CAddress &Addr);
		UINT    GetMinimum(CSpace * pSpace);
		UINT    GetMaximum(CSpace * pSpace);
		CString GetFormat(CSpace * pSpace);

	protected:
			
		// Implementation
		void AddSpaces(void);
		UINT GetOffset(CAddress  Addr, CItem *pConfig);
		BOOL VerifyOffset(CError &Error, CAddress Addr, CSpace * pSpace);
		WORD FindSlot(CString &Text, CSpace * pSpace);
		UINT FindOffset(CString &Text, CSpace * pSpace);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Panasonic FP7 MEWTOCOL7-COM Serial Master Driver
//

class CPanFp7SerialMasterDriver : public CPanFp7MasterDriver
{
	public:
		// Constructor
		CPanFp7SerialMasterDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);


	};

//////////////////////////////////////////////////////////////////////////
//
//  Panasonic FP7 MEWTOCOL7-COM TCP/IP Master Driver
//

class CPanFp7TcpMasterDriver : public CPanFp7MasterDriver
{
	public:
		// Constructor
		CPanFp7TcpMasterDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);


	};

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 MEWTOCOL7-COM Address Selection
//

class CPanFp7AddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CPanFp7AddrDialog (CPanFp7MasterDriver &Driver, CAddress &Addr, CItem * pConfig, BOOL fPart);

	protected:

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnSpaceChange(UINT uID, CWnd &Wnd);

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);
		void    ShowDetails(void);
	};		                

// End of File

#endif
