
#include "intern.hpp"

#include "execute.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Aeon Thunks
//

#if !defined(AEON_ENVIRONMENT)

#define CThreadData		CTaskData

#define GetThreadData(x)	GetTaskData(x)

#define SetThreadData(p)	SetTaskData(p)

#define GetThreadName(t)	GetTaskLabel()

#define GetCurrentThread()

#endif

//////////////////////////////////////////////////////////////////////////
//
// Call Frame and Context
//

struct CFrame
{
	UINT	m_Flags;
	CString	m_Scope;
	PCBYTE  m_pDebug;
	PDWORD  m_pLocal;
	UINT    m_uLocal;
	};

struct CContext : public CThreadData
{
	CContext(void) : CThreadData(200) {}

	CFrame *	  m_pFrame;
	CArray <CFrame *> m_Stack;
	char		  m_sLine[1024];
	char		  m_sName[512];
	};

//////////////////////////////////////////////////////////////////////////
//
// Byte Code Executer (Debug Version)
//

// Prototypes

global	DWORD	C3ExecuteCodeDebug(PCBYTE pCode, IDataServer *pData, PDWORD pParam);
global	void	C3ExecStackTrace(void);
global	void	C3ExecDumpLocals(void);
global	void	C3ExecPrint(PCTXT pLine);
static	BOOL	IsDebugValid(PCBYTE pDebug);
static	void	Print(CContext *pCtx, PCTXT pText, ...);
static	void	Write(CContext *pCtx, PCTXT pText);
static	void	PrintValue(CContext *pCtx, IDataServer *pData, UINT uType, DWORD Data);
static	void	OutputLine(CContext *pCtx);
static	void	ShowContext(CContext *pCtx, BOOL fHead = FALSE);
static	void	ShowInvocation(CContext *pCtx, PDWORD pParam, UINT uParam);
static	void	ShowReturnValue(CContext *pCtx, UINT uType, DWORD Data);
static	void	ShowPutLocal(CContext *pCtx, UINT uSlot, DWORD Data);
static	void	ShowPutGlobal(CContext *pCtx, IDataServer *pData, DWORD Ref, UINT uType, DWORD Data);
static	void	ShowFuncCall(CContext *pCtx, IDataServer *pData, UINT uArgs, PCBYTE pType, PCDWORD pArgs, WORD Func);
static	void	CreateFrame(CContext *pCtx, PCBYTE pDebug, PDWORD pLocal, UINT uLocal);
static	void	DeleteFrame(CContext *pCtx);

// Code

global DWORD C3ExecuteCodeDebug(PCBYTE pCode, IDataServer *pData, PDWORD pParam)
{
	CContext *pCtx   = NULL;
	
	PCBYTE    pDebug = NULL;

	BYTE   bType  = pCode[0];

	BYTE   bDepth = pCode[1] * 2;

	BYTE   bLocal = pCode[2];

	BYTE   bParam = pCode[3];

	MakeMax(bLocal, bParam);

	PDWORD pLocal = PDWORD(alloca(bLocal * sizeof(DWORD)));

	PDWORD pAlloc = PDWORD(alloca(bDepth * sizeof(DWORD)));

	PDWORD pInit  = pAlloc + bDepth - 1;

	PDWORD pStack = pInit;

	if( bLocal           ) memset(pLocal,      0, bLocal * sizeof(DWORD));

	if( bParam && pParam ) memcpy(pLocal, pParam, bParam * sizeof(DWORD));

	pCode += 4;

	DWORD Temp;

	PUTF  pNew;

	BYTE  b;

	SHORT j;

	if( *pCode == bcDebugInfo ) {

		pCode += 1;

		pDebug = pCode - 5 + *PWORD(pCode);

		pCode += 2;

		if( IsDebugValid(pDebug) ) {

			if( !(pCtx = (CContext *) GetThreadData(200)) ) {

				pCtx = New CContext;

				SetThreadData(pCtx);
				}

			CreateFrame(pCtx, pDebug, pLocal, bLocal);

			ShowInvocation(pCtx, pParam, bParam);
			}
		else
			pDebug = NULL;
		}

	while( (b = *pCode++) != bcNull ) {

		if( b < bcNull ) {

			SI0 = b;
			
			PUSH();

			continue;
			}

		if( b == bcReturn ) {

			break;
			}

		if( b == bcDebugFunc ) {

			b       = *pCode++;

			ShowFuncCall(pCtx, pData, b, pCode, pStack + 1, ReadWord(pCode + b));

			pCode  += b;

			Temp    = pData->RunFunc(ReadWord(pCode), b, pStack + 1);

			pCode  += 2;

			pStack += b;

			SX0 = Temp;
			
			PUSH();

			continue;
			}

		if( b == bcFunction ) {

			b       = *pCode++;

			Temp    = pData->RunFunc(ReadWord(pCode), b, pStack + 1);

			pCode  += 2;

			pStack += b;

			SX0 = Temp;
			
			PUSH();

			continue;
			}

		switch( b ) {

			// Conversions

			case bcToInteger:
				SI1 = C3INT(SR1);
				break;

			case bcToReal:
				SR1 = C3REAL(SI1);
				break;

			case bcTestInteger:
				SI1 = SI1?1:0;
				break;

			case bcTestReal:
				SI1 = SR1?1:0;
				break;

			// Stack Helpers

			case bcPop:
				PULL();
				break;

			case bcPopString:
				if( SS1 ) Free(SS1);
				PULL();
				break;

			case bcDupTop:
				SX0 = SX1;
				PUSH();
				break;

			case bcDupTopString:
				SS0 = wstrdup(SS1);
				PUSH();
				break;

			case bcDup3rd:
				SX0 = SX1;
				SX1 = SX2;
				SX2 = SX0;
				PUSH();
				break;

			case bcDup3rdString:
				SS0 = SS1;
				SS1 = SS2;
				SS2 = wstrdup(SS0);
				PUSH();
				break;

			case bcSwap:
				Temp = SX1;
				SX1  = SX2;
				SX2  = Temp;
				break;

			// Branches

			// TODO -- These will leak memory left by temporary
			// variable if we throw. We should ideally have some
			// clean up in the byte code to handle exceptions.
			
			case bcBranchZero:
				pCode += (j = SI1 ? 2 : SHORT(ReadWord(pCode)));
				if( j < 0 ) CheckThreadCancellation();
				PULL();
				break;

			case bcBranchNonZero:
				pCode += (j = SI1 ? SHORT(ReadWord(pCode)) : 2);
				if( j < 0 ) CheckThreadCancellation();
				PULL();
				break;

			case bcBranch:
				pCode += (j = SHORT(ReadWord(pCode)));
				if( j < 0 ) CheckThreadCancellation();
				break;

			// Referencing
			
			case bcLoadAddr:
				SX0     = ReadWord(pCode);
				pCode  += 2;
				PUSH();
				break;
			
			case bcIndexString:
				Temp = (SI1 < wstrlen(SS2)) ? SS2[SI1] : 0;
				Free(SS2);
				SI2  = ((WCHAR) Temp);
				PULL();
				break;
			
			case bcIndexArray:
				((CDataRef &) SX2).t.m_Array = SX1;
				PULL();
				break;

			case bcIndexExtend:
				((CDataRef &) SX2).x.m_Array = SX1;
				PULL();
				break;
			
			case bcIndexBit:
				((CDataRef &) SX2).t.m_HasBit = 1;
				((CDataRef &) SX2).t.m_BitRef = SX1;
				PULL();
				break;

			// Save Local

			case bcPutLocal:
				ShowPutLocal(pCtx, *pCode, SX1);
				pLocal[*pCode++] = SX1;
				PULL();
				break;

			case bcPutLocalString:
				ShowPutLocal(pCtx, *pCode, SX1);
				Free(PTXT(pLocal[*pCode]));
				pLocal[*pCode++] = SX1;
				PULL();
				break;

			case bcPutLocal0:
				ShowPutLocal(pCtx, 0, SX1);
				pLocal[0] = SX1;
				PULL();
				break;

			case bcPutLocal1:
				ShowPutLocal(pCtx, 1, SX1);
				pLocal[1] = SX1;
				PULL();
				break;

			case bcPutLocal2:
				ShowPutLocal(pCtx, 2, SX1);
				pLocal[2] = SX1;
				PULL();
				break;

			case bcPutLocal3:
				ShowPutLocal(pCtx, 3, SX1);
				pLocal[3] = SX1;
				PULL();
				break;

			case bcPutLocal4:
				ShowPutLocal(pCtx, 4, SX1);
				pLocal[4] = SX1;
				PULL();
				break;

			case bcPutLocal5:
				ShowPutLocal(pCtx, 5, SX1);
				pLocal[5] = SX1;
				PULL();
				break;

			// Load Local

			case bcGetLocal:
				SX0 = pLocal[*pCode++];
				PUSH();
				break;

			case bcGetLocalString:
				SS0 = wstrdup(PCUTF(pLocal[*pCode++]));
				PUSH();
				break;

			case bcGetLocal0:
				SX0 = pLocal[0];
				PUSH();
				break;

			case bcGetLocal1:
				SX0 = pLocal[1];
				PUSH();
				break;

			case bcGetLocal2:
				SX0 = pLocal[2];
				PUSH();
				break;

			case bcGetLocal3:
				SX0 = pLocal[3];
				PUSH();
				break;

			case bcGetLocal4:
				SX0 = pLocal[4];
				PUSH();
				break;

			case bcGetLocal5:
				SX0 = pLocal[5];
				PUSH();
				break;

			// Save Direct

			case bcPutInteger:
				ShowPutGlobal(pCtx, pData, ReadWord(pCode), typeInteger, SX1);
				pData->SetData(ReadWord(pCode), typeInteger, setNone, SX1);
				pCode += 2;
				PULL();
				break;

			case bcPutReal:
				ShowPutGlobal(pCtx, pData, ReadWord(pCode), typeReal, SX1);
				pData->SetData(ReadWord(pCode), typeReal, setNone, SX1);
				pCode += 2;
				PULL();
				break;

			case bcPutString:
				ShowPutGlobal(pCtx, pData, ReadWord(pCode), typeString, SX1);
				pData->SetData(ReadWord(pCode), typeString, setNone, SX1);
				pCode += 2;
				PULL();
				break;

			// Load Direct

			case bcGetInteger:
				SX0     = pData->GetData(ReadWord(pCode), typeInteger, getNone);
				pCode  += 2;
				PUSH();
				break;

			case bcGetReal:
				SX0     = pData->GetData(ReadWord(pCode), typeReal, getNone);
				pCode  += 2;
				PUSH();
				break;

			case bcGetString:
				SX0     = pData->GetData(ReadWord(pCode), typeString, getNone);
				pCode  += 2;
				PUSH();
				break;

			// Save Indirect

			case bcSaveInteger:
				ShowPutGlobal(pCtx, pData, SI2, typeInteger, SX1);
				pData->SetData(SI2, typeInteger, setNone, SX1);
				PULL();
				PULL();
				break;

			case bcSaveReal:
				ShowPutGlobal(pCtx, pData, SI2, typeReal, SX1);
				pData->SetData(SI2, typeReal, setNone, SX1);
				PULL();
				PULL();
				break;

			case bcSaveString:
				ShowPutGlobal(pCtx, pData, SI2, typeString, SX1);
				pData->SetData(SI2, typeString, setNone, SX1);
				PULL();
				PULL();
				break;

			// Load Indirect

			case bcLoadInteger:
				SX1 = pData->GetData(SX1, typeInteger, getNone);
				break;

			case bcLoadReal:
				SX1 = pData->GetData(SX1, typeReal, getNone);
				break;

			case bcLoadString:
				SX1 = pData->GetData(SX1, typeString, getNone);
				break;

			// Load Property

			case bcLoadPropInteger:
				SX2 = pData->GetProp(SX2, WORD(SX1), typeInteger);
				PULL();
				break;

			case bcLoadPropReal:
				SX2 = pData->GetProp(SX2, WORD(SX1), typeReal);
				PULL();
				break;

			case bcLoadPropString:
				SX2 = pData->GetProp(SX2, WORD(SX1), typeString);
				PULL();
				break;

			// Constants

			case bcPushInteger1:
				SI0     = ReadByte(pCode);
				pCode  += 1;
				PUSH();
				break;

			case bcPushInteger2:
				SI0     = ReadWord(pCode);
				pCode  += 2;
				PUSH();
				break;

			case bcPushInteger4:
				SI0     = ReadLong(pCode);
				pCode  += 4;
				PUSH();
				break;

			case bcPushReal:
				SI0     = ReadLong(pCode);
				pCode  += 4;
				PUSH();
				break;

			case bcPushString:
				SS0    = wstrdup(PSTR(pCode + 2));
				pCode += 2 + 1 * ReadWord(pCode);
				PUSH();
				break;

			case bcPushWideString:
				SS0    = ReadText(PUTF(pCode + 2));
				pCode += 2 + 2 * ReadWord(pCode);
				PUSH();
				break;

			// Integer Operators

			case bcLogicalNot:
				SI1 = !SI1;
				break;

			case bcBitwiseNot:
				SI1 = ~SI1;
				break;

			case bcUnaryMinus:
				SI1 = -SI1;
				break;

			case bcIncrement:
				SI1++;
				break;

			case bcDecrement:
				SI1--;
				break;

			case bcBitSelect:
				SI2 = !!(SI2&1<<SI1);
				PULL();
				break;

			case bcDivide:
				SI2 = SI1 ? (SI2 / SI1) : 0;
				PULL();
				break;
			
			case bcRemainder:
				SI2 = SI1 ? (SI2 % SI1) : 0;
				PULL();
				break;
			
			case bcMultiply:
				SI2 = (SI2 *  SI1);
				PULL();
				break;

			case bcAdd:
				SI2 = (SI2 +  SI1);
				PULL();
				break;

			case bcSubtract:
				SI2 = (SI2 -  SI1);
				PULL();
				break;
			
			case bcLeftShift:
				SI2 = (SI2 << SI1);
				PULL();
				break;
			
			case bcRightShift:
				SI2 = (SI2 >> SI1);
				PULL();
				break;
			
			case bcLessThan:
				SI2 = (SI2 <  SI1);
				PULL();
				break;
			
			case bcLessOr:
				SI2 = (SI2 <= SI1);
				PULL();
				break;
			
			case bcGreaterThan:
				SI2 = (SI2 >  SI1);
				PULL();
				break;
			
			case bcGreaterOr:
				SI2 = (SI2 >= SI1);
				PULL();
				break;
			
			case bcEqual:
				SI2 = (SI2 == SI1);
				PULL();
				break;
			
			case bcNotEqual:
				SI2 = (SI2 != SI1);
				PULL();
				break;

			case bcBitwiseAnd:
				SI2 = (SI2 &  SI1);
				PULL();
				break;
			
			case bcBitwiseXor:
				SI2 = (SI2 ^  SI1);
				PULL();
				break;
			
			case bcBitwiseOr:
				SI2 = (SI2 |  SI1);
				PULL();
				break;

			case bcBitChange:
				SI1 ? (SI3 |= (1<<SI2)) : (SI3 &= ~(1<<SI2));
				PULL();
				PULL();
				break;

			// Real Operators

			case bcRealLogicalNot:
				SI1 = !SR1;
				break;

			case bcRealUnaryMinus:
				SR1 = -SR1;
				break;

			case bcRealIncrement:
				SR1++;
				break;

			case bcRealDecrement:
				SR1--;
				break;
			
			case bcRealDivide:
				SR2 = (SR2 / SR1);
				PULL();
				break;

			case bcRealMultiply:
				SR2 = (SR2 *  SR1);
				PULL();
				break;

			case bcRealAdd:
				SR2 = (SR2 +  SR1);
				PULL();
				break;

			case bcRealSubtract:
				SR2 = (SR2 -  SR1);
				PULL();
				break;

			case bcRealLessThan:
				SI2 = (SR2 <  SR1);
				PULL();
				break;

			case bcRealLessOr:
				SI2 = (SR2 <= SR1);
				PULL();
				break;

			case bcRealGreaterThan:
				SI2 = (SR2 >  SR1);
				PULL();
				break;

			case bcRealGreaterOr:
				SI2 = (SR2 >= SR1);
				PULL();
				break;

			case bcRealEqual:
				SI2 = (SR2 == SR1);
				PULL();
				break;

			case bcRealNotEqual:
				SI2 = (SR2 != SR1);
				PULL();
				break;

			// String Operators

			case bcStringAdd:
				Temp = wstrlen(SS2)+wstrlen(SS1)+1;
				pNew = PUTF(Malloc(sizeof(WCHAR)*Temp));
				wstrcpy(pNew, SS2); Free(SS2);
				wstrcat(pNew, SS1); Free(SS1);
				SS2  = pNew;
				PULL();
				break;

			case bcStringAddChar:
				Temp = wstrlen(SS2);
				pNew = PUTF(Malloc(sizeof(WCHAR)*(Temp+2)));
				memcpy(pNew, SS2, sizeof(WCHAR)*Temp);
				pNew[Temp+0] = WCHAR(SI1);
				pNew[Temp+1] = WCHAR(0);
				Free(SS2);
				SS2  = pNew;
				PULL();
				break;

			case bcStringLessThan:
				Temp = (wstricmp(SS2, SS1)< 0);
				Free(SS2);
				Free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringLessOr:
				Temp = (wstricmp(SS2, SS1)<=0);
				Free(SS2);
				Free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringGreaterThan:
				Temp = (wstricmp(SS2, SS1)> 0);
				Free(SS2);
				Free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringGreaterOr:
				Temp = (wstricmp(SS2, SS1)>=0);
				Free(SS2);
				Free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringEqual:
				Temp = (wstricmp(SS2, SS1)==0);
				Free(SS2);
				Free(SS1);
				SI2  = Temp;
				PULL();
				break;

			case bcStringNotEqual:
				Temp = (wstricmp(SS2, SS1)!=0);
				Free(SS2);
				Free(SS1);
				SI2  = Temp;
				PULL();
				break;
			}
		}

	if( pDebug ) {

		ShowReturnValue(pCtx, bType & 0x3F, SI1);

		DeleteFrame(pCtx);
		}

	return (bType & 0x3F) ? SI1 : 0;
	}

global void C3ExecStackTrace(void)
{
	CContext *pCtx = (CContext *) GetThreadData(200);

	if( pCtx ) {

		ShowContext(pCtx);

		Write(pCtx, "Stack:");

		OutputLine(pCtx);

		UINT c = pCtx->m_Stack.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			ShowContext(pCtx);

			Write(pCtx, "  ");

			Write(pCtx, PCTXT(pCtx->m_Stack[c - 1 - n]->m_Scope));

			OutputLine(pCtx);
			}
		}

	}

global void C3ExecDumpLocals(void)
{
	CContext *pCtx = (CContext *) GetThreadData(200);

	if( pCtx ) {

		ShowContext(pCtx);

		Write(pCtx, "Locals:");

		OutputLine(pCtx);

		CFrame *pFrame = pCtx->m_pFrame;

		for( UINT uSlot = 0; uSlot < pFrame->m_uLocal; uSlot++ ) {

			ShowContext(pCtx);
	
			UINT uType = *PBYTE(pFrame->m_pDebug + 8 + 3 * uSlot);

			UINT uName = *PWORD(pFrame->m_pDebug + 9 + 3 * uSlot);

			Print(pCtx, "  %s = ", PCSTR(pFrame->m_pDebug + uName));

			PrintValue(pCtx, NULL, uType, pFrame->m_pLocal[uSlot]);

			OutputLine(pCtx);
			}
		}
	}

global void C3ExecPrint(PCTXT pLine)
{
	CContext *pCtx = (CContext *) GetThreadData(200);

	if( pCtx ) {

		ShowContext(pCtx);

		Write(pCtx, pLine);

		OutputLine(pCtx);
		}
	}

static BOOL IsDebugValid(PCBYTE pDebug)
{
	return pDebug[0] == 0xDD &&
	       pDebug[1] == 0xEE &&
	       pDebug[2] == 0xBB &&
	       pDebug[3] == 0x00 ;
	}

static void Print(CContext *pCtx, PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	VSPrintf(pCtx->m_sLine + strlen(pCtx->m_sLine), pText, pArgs);

	va_end(pArgs);
	}

static void Write(CContext *pCtx, PCTXT pText)
{
	strcat(pCtx->m_sLine, pText);
	}

static void PrintValue(CContext *pCtx, IDataServer *pData, UINT uType, DWORD Data)
{
	switch( uType ) {

		case typeInteger:

			Print(pCtx, "%d", C3INT(Data));

			break;

		case typeReal:
		{
			C3REAL r = I2R(Data);

			if( r < 0 ) {

				r = -r;

				pCtx->m_sName[0] = '-';

				gcvt(r, 8, pCtx->m_sName + 1);
				}
			else
				gcvt(r, 8, pCtx->m_sName);

			Write(pCtx, pCtx->m_sName);

			break;
			}

		case typeString:
		{
			PCUTF p = PCUTF(Data);

			PSTR  c = PSTR(alloca(wcslen(p)+1));

			for( PSTR s = c; (*s++ = char(*p++)); );

			Print(pCtx, "\"%s\"", c);

			break;
			}

		default:

			if( uType >= typeObject ) {

				if( pData ) {

					if( !pData->GetName(Data, uType, pCtx->m_sName, sizeof(pCtx->m_sName)) ) {

						SPrintf(pCtx->m_sName, "0x%4.4X", Data);
						}

					Write(pCtx, pCtx->m_sName);
					}
				else
					Print(pCtx, "0x%4.4X", Data);
				}
			else
				Write(pCtx, "????");

			break;
		}
	}

static void OutputLine(CContext *pCtx)
{
	strcat(pCtx->m_sLine, "\n");

	AfxPrint(pCtx->m_sLine);
	}

static void ShowContext(CContext *pCtx, BOOL fHead)
{
	pCtx->m_sLine[0] = 0;

	UINT  n = 2 * (pCtx->m_Stack.GetCount() - (fHead ? 1 : 0));

	DWORD t = ToTime(GetTickCount()) % 1000000;

	PCTXT p = GetThreadName(GetCurrentThread());

	Print(pCtx, "%3.3u.%3.3u : %-8s : %*.*s", t / 1000, t % 1000, p, n, n, "");
	}

static void ShowInvocation(CContext *pCtx, PDWORD pParam, UINT uParam)
{
	if( pCtx && (pCtx->m_pFrame->m_Flags & 1) ) {

		ShowContext(pCtx, TRUE);

		CFrame *pFrame = pCtx->m_pFrame;

		UINT    uFunc  = *PWORD(pFrame->m_pDebug + 6);

		Write(pCtx, PCSTR(pFrame->m_pDebug + uFunc));

		for( UINT n = 0; n < uParam; n++ ) {

			Write(pCtx, n ? ", " : "(");

			UINT uType = *PBYTE(pFrame->m_pDebug + 8 + 3 * n);

			UINT uName = *PWORD(pFrame->m_pDebug + 9 + 3 * n);

			Print(pCtx, "%s = ", PCSTR(pFrame->m_pDebug + uName));

			PrintValue(pCtx, NULL, uType, pParam[n]);
			}

		Write(pCtx, uParam ? ")" : "(void)");

		OutputLine(pCtx);
		}
	}

static void ShowReturnValue(CContext *pCtx, UINT uType, DWORD Data)
{
	if( pCtx && (pCtx->m_pFrame->m_Flags & 1) ) {

		ShowContext(pCtx);

		CFrame *pFrame = pCtx->m_pFrame;

		UINT    uFunc  = *PWORD(pFrame->m_pDebug + 6);

		Print(pCtx, "return ", PCSTR(pFrame->m_pDebug + uFunc));

		if( uType ) {

			PrintValue(pCtx, NULL, uType, Data);
			}

		OutputLine(pCtx);
		}
	}

static void ShowPutLocal(CContext *pCtx, UINT uSlot, DWORD Data)
{
	if( pCtx && (pCtx->m_pFrame->m_Flags & 2) ) {

		ShowContext(pCtx);

		CFrame *pFrame = pCtx->m_pFrame;
	
		UINT    uType  = *PBYTE(pFrame->m_pDebug + 8 + 3 * uSlot);

		UINT    uName  = *PWORD(pFrame->m_pDebug + 9 + 3 * uSlot);

		Print(pCtx, "%s := ", PCSTR(pFrame->m_pDebug + uName));

		PrintValue(pCtx, NULL, uType, Data);

		OutputLine(pCtx);
		}
	}

static void ShowPutGlobal(CContext *pCtx, IDataServer *pData, DWORD Ref, UINT uType, DWORD Data)
{
	if( pCtx && (pCtx->m_pFrame->m_Flags & 4) ) {

		ShowContext(pCtx);

		if( !pData->GetName(Ref, 0, pCtx->m_sName, sizeof(pCtx->m_sName)) ) {

			SPrintf(pCtx->m_sName, "0x%8.8X", Ref);
			}
	
		Print(pCtx, "%s := ", pCtx->m_sName);

		PrintValue(pCtx, NULL, uType, Data);

		OutputLine(pCtx);
		}
	}

static void ShowFuncCall(CContext *pCtx, IDataServer *pData, UINT uArgs, PCBYTE pType, PCDWORD pArgs, WORD Func)
{
	if( pCtx && (pCtx->m_pFrame->m_Flags & 8) ) {

		if( !(Func == 0x214E || Func == 0x214F || Func == 0x2150) ) {

			ShowContext(pCtx);

			if( !pData->GetName(Func, 1, pCtx->m_sName, sizeof(pCtx->m_sName)) ) {

				SPrintf(pCtx->m_sName, "F%4.4X", Func);
				}

			Write(pCtx, pCtx->m_sName);

			for( UINT n = 0; n < uArgs; n++ ) {

				Write(pCtx, n ? ", " : "(");

				PrintValue(pCtx, pData, pType[n], pArgs[n]);
				}

			Write(pCtx, uArgs ? ")" : "(void)");

			OutputLine(pCtx);
			}
		}
	}

static void CreateFrame(CContext *pCtx, PCBYTE pDebug, PDWORD pLocal, UINT uLocal)
{
	CFrame *pFrame   = New CFrame;

	pFrame->m_pDebug = pDebug;

	pFrame->m_pLocal = pLocal;

	pFrame->m_uLocal = uLocal;

	pFrame->m_Flags  = *PWORD(pDebug + 4);

	UINT    uFunc    = *PWORD(pDebug + 6);

	pFrame->m_Scope  = PCSTR(pDebug + uFunc);

	pCtx->m_Stack.Append(pFrame);

	pCtx->m_pFrame = pFrame;
	}

static void DeleteFrame(CContext *pCtx)
{
	delete pCtx->m_pFrame;

	UINT n = pCtx->m_Stack.GetCount() - 1;

	pCtx->m_Stack.Remove(n);

	pCtx->m_pFrame = n ? pCtx->m_Stack[n-1] : NULL;
	}

// End of File
