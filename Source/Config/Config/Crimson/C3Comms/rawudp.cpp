
#include "intern.hpp"

#include "rawudp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Raw UDP/IP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CRawUDPDriverOptions, CUIItem);

// Constructor

CRawUDPDriverOptions::CRawUDPDriverOptions(void)
{
	m_pService = NULL;

	m_Port     = 1234;
	}

// Download Support

BOOL CRawUDPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pService);

	Init.AddWord(WORD(m_Port));

	return TRUE;
	}

// Meta Data Creation

void CRawUDPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddVirtual(Service);

	Meta_AddInteger(Port);

	Meta_SetName(IDS_DRIVER_OPTIONS);
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Port
//

// Instantiator

ICommsDriver * Create_RawUDPDriver(void)
{
	return New CRawUDPDriver;
	}

// Constructor

CRawUDPDriver::CRawUDPDriver(void)
{
	m_wID		= 0x3709;

	m_uType		= driverRawPort;
	
	m_Manufacturer	= "<System>";
	
	m_DriverName	= "Raw UDP/IP";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Raw UDP/IP";

	m_fSingle	= TRUE;

	C3_PASSED();
	}

// Configuration

CLASS CRawUDPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CRawUDPDriverOptions);
	}

// Binding Control

UINT CRawUDPDriver::GetBinding(void)
{
	return bindEthernet;
	}

// End of File
