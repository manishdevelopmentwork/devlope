
//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsBytes_HPP

#define INCLUDE_TDS_TdsBytes_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"


//////////////////////////////////////////////////////////////////////////
//
// Some additional types
//

#if !defined(__GNUC__)

AfxDefineType(ULONGLONG, unsigned __int64);

#else

AfxDefineType(ULONGLONG, unsigned long long);

#endif

AfxDefineType(LONGLONG, INT64);
AfxDefineType(FLOAT,    float);
AfxDefineType(DOUBLE,   double);
AfxDefineType(USHORT,   unsigned short);
AfxDefineType(ULONG,    unsigned long);

//////////////////////////////////////////////////////////////////////////
//
// TDS Byte Stream
//

class CTdsBytes
{
	public:
		// Constructors
		CTdsBytes(UINT uSize = 16);
		CTdsBytes(CTdsBytes const &That);

		// Destructor
		~CTdsBytes(void);

		// Assignment
		CTdsBytes & operator = (CTdsBytes &That);

		// Attributes
		UINT  GetSize(void) const;
		PBYTE GetData(void) const;

		// Reading
		BYTE   GetByte(UINT &uPos) const;
		USHORT GetUShort(UINT &uPos) const;
		ULONG  GetULong(UINT &uPos) const;
		ULONGLONG GetULongLong(UINT &uPos) const;
		FLOAT  GetFloat(UINT &uPos) const;
		DOUBLE GetDouble(UINT &uPos) const;
		USHORT GetUShortReverse(UINT &uPos) const;
		ULONG  GetULongReverse(UINT &uPos) const;
		ULONGLONG GetULongLongReverse(UINT &uPos) const;
		FLOAT  GetFloatReverse(UINT &uPos) const;
		DOUBLE GetDoubleReverse(UINT &uPos) const;
		PCSTR  GetText(UINT &uPos) const;
		
		bool   GetByteCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos) const;
		bool   GetReverseCharacterData(CHAR* pOutputString, USHORT uLength, UINT &uPos) const;

		// Operations
		void Clear(void);
		void AddByte(BYTE bData);
		void AddUShort(USHORT uData);
		void AddULong(ULONG uData);
		void AddULongLong(ULONGLONG uData);
		void AddUShortReverse(USHORT uData);
		void AddULongReverse(ULONG uData);
		void AddULongLongReverse(ULONGLONG uData);
		void AddFloatReverse(FLOAT uData);
		void AddDoubleReverse(DOUBLE uData);
		void AddText(PCSTR pText);
		void AddReverseCharText(PCSTR pText);
		void AddData(PCBYTE pData, UINT uSize);
		void RemoveHead(UINT uPos);
		void Dump(void);
		void DumpASCII(void);
	protected:
		// Data Members
		PBYTE m_pData;
		UINT  m_uPtr;
		UINT  m_uAlloc;

		// Memory Management
		void ExpandBuffer(UINT uNewSize);
	};

// End of File

#endif
