
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "elmo.hpp"
	
// Command Def
//struct FAR ElmoCmdDef {
//	UINT	uID,
//	char 	sName[6];
//	BOOL	fHasParam;
//	UINT	uType;	
//	};

ElmoCmdDef CODE_SEG CElmoDriver::CL[] = {
{CAC,	"AC",	/*Acceleration",*/				FALSE,	LL},
{CBG,	"BG",	/*Begin motion",*/				FALSE,	LL},
{CBT,	"BT",	/*Begin motion at defined time",*/		FALSE,	LL},
{CDC,	"DC",	/*Deceleration",*/				FALSE,	LL},
{CIL,	"IL",	/*Input logic",*/				TRUE,	LL},
{CJV,	"JV",	/*Speed of jogging motion",*/			FALSE,	LL},
{CMO,	"MO",	/*Motor on/off",*/				FALSE,	LL},
{CPA,	"PA",	/*Absolute position",*/				FALSE,	LL},
{CPR,	"PR",	/*Relative position",*/				FALSE,	LL},
{CSD,	"SD",	/*Stop deceleration",*/				FALSE,	LL},
{CSF,	"SF",	/*Smooth factor for motion command",*/		FALSE,	LL},
{CSP,	"SP",	/*Speed for point-to-point motion",*/		FALSE,	LL},
{CST,	"ST",	/*Stop motion using deceleration value",*/	FALSE,	LL},
{CTC,	"TC",	/*Torque command",*/				FALSE,	RR},
{CAN,	"AN",	/*Read analog inputs",*/			TRUE,	RR},
{CIB,	"IB",	/*Bit-wise digital input",*/			TRUE,	LL},
{CIF,	"IF",	/*Digital input filter",*/			TRUE,	RR},
{CIP,	"IP",	/*Read all digital inputs",*/			FALSE,	LL},
{COB,	"OB",	/*Bit-wise digital output",*/			TRUE,	LL},
{COC,	"OC",	/*Output Compare",*/				TRUE,	LL},
{COL,	"OL",	/*Output Logic",*/				TRUE,	LL},
{COP,	"OP",	/*Set all digital outputs",*/			FALSE,	LL},
{CBV,	"BV",	/*Maximum motor DC voltage",*/			FALSE,	LL},
{CDD,	"DD",	/*CAN controller status",*/			FALSE,	LL},
{CDV,	"DV",	/*Reference desired value",*/			TRUE,	RR},
{CEC,	"EC",	/*Error code",*/				FALSE,	LL},
{CLC,	"LC",	/*Current limitation",*/			FALSE,	LL},
{CMF,	"MF",	/*Motor fault",*/				FALSE,	LL},
{CMS,	"MS",	/*Motion status reporting",*/			FALSE,	LL},
{CPK,	"PK",	/*Peak memory",*/				FALSE,	LL},
{CSN,	"SN",	/*Serial number",*/				TRUE,	LL},
{CSR,	"SR",	/*Numerical",*/					FALSE,	LL},
{CTI,	"TI[1]",/*Temperature indication",*/			FALSE,	LL},
{CVR,	"VR",	/*Software (firmware) version",*/		FALSE,	LL},
{CAB,	"AB",	/*Absolute encoder setting parameters",*/	TRUE,	LL},
{CID,	"ID",	/*Read active current",*/			FALSE,	RR},
{CIQ,	"IQ",	/*Read reactive current",*/			FALSE,	RR},
{CPE,	"PE",	/*Position error",*/				FALSE,	LL},
{CPX,	"PX",	/*Main encoder position",*/			FALSE,	LL},
{CPY,	"PY",	/*Auxiliary position",*/			FALSE,	LL},
{CVE,	"VE",	/*Velocity error",*/				FALSE,	LL},
{CVX,	"VX",	/*Main encoder velocity",*/			FALSE,	LL},
{CVY,	"VY",	/*Velocity of auxiliary feedback",*/		FALSE,	LL},
{CYA,	"YA",	/*Auxiliary position sensor parameters",*/	TRUE,	LL},
{CAG,	"AG",	/*Analog gains array",*/			TRUE,	RR},
{CAS,	"AS",	/*Analog input offsets array",*/		TRUE,	RR},
{CBP,	"BP",	/*Brake parameter",*/				TRUE,	LL},
{CCA,	"CA",	/*Commutation parameters array",*/		TRUE,	LL},
{CCL,	"CL",	/*Current continuous limitations array",*/	TRUE,	RR},
{CEF,	"EF",	/*Encoder filter frequency",*/			TRUE,	LL},
{CEM,	"EM",	/*ECAM parameters",*/				TRUE,	LL},
{CET,	"ET",	/*Entries for ECAM table",*/			TRUE,	LL},
{CFF,	"FF",	/*Feed forward",*/				TRUE,	RR},
{CFR,	"FR",	/*Follower ratio",*/				TRUE,	RR},
{CHM,	"HM",	/*Homing and capture mode",*/			TRUE,	LL},
{CHY,	"HY",	/*Auxiliary home and capture mode",*/		TRUE,	LL},
{CMC,	"MC",	/*Maximum peak current",*/			FALSE,	RR},
{CMP,	"MP",	/*Motion (PT/PVT) parameters",*/		TRUE,	LL},
{CPL,	"PL",	/*Peak duration and limit",*/			TRUE,	RR},
{CPM,	"PM",	/*Profiler mode",*/				FALSE,	LL},
{CPT,	"PT",	/*Position time command",*/			FALSE,	LL},
{CPV,	"PV",	/*Position velocity time command",*/		FALSE,	LL},
{CPW,	"PW",	/*PWM signal parameters",*/			TRUE,	RR},
{CQP,	"QP",	/*Position",*/					TRUE,	LL},
{CQT,	"QT",	/*Time",*/					TRUE,	LL},
{CQV,	"QV",	/*Velocity",*/					TRUE,	LL},
{CRM,	"RM",	/*Reference mode (analog en./dis.",*/		FALSE,	LL},
{CUM,	"UM",	/*Unit mode stepper",*/				FALSE,	LL},
{CTR,	"TR",	/*Target radius",*/				TRUE,	LL},
{CVH,	"VH",	/*High reference limit",*/			TRUE,	LL},
{CVL,	"VL",	/*Low reference limit",*/			TRUE,	LL},
{CXM,	"XM",	/*X Modulo",*/					TRUE,	LL},
{CYM,	"YM",	/*Y Modulo",*/					TRUE,	LL},
{CGS,	"GS",	/*Gain scheduling",*/				TRUE,	LL},
{CKG,	"KG",	/*Gain scheduled controller Integers",*/	TRUE,	LL},
{CKR,	"KG",	/*Gain scheduled controller Reals",*/		TRUE,	RR},
{CKI,	"KI",	/*PID integral terms array",*/			TRUE,	RR},
{CKP,	"KP",	/*PID proportional terms array",*/		TRUE,	RR},
{CKV,	"KV",	/*Advanced filter for speed loop",*/		TRUE,	LL},
{CXA,	"XA",	/*Extra parameters (more)",*/			TRUE,	LL},
{CXP,	"XP",	/*Extra parameters",*/				TRUE,	LL},
{CER,	"ER",	/*Maximum tracking errors",*/			TRUE,	LL},
{CHL,	"HL",	/*Over-speed and position range limit",*/	TRUE,	LL},
{CLL,	"LL",	/*Low actual feedback limit",*/			TRUE,	LL},
{CBH,	"BH",	/*Get a sample signal as hexadecimal",*/	FALSE,	LL},
{CRC,	"RC",	/*Variables to record",*/			FALSE,	LL},
{CRG,	"RG",	/*Recording gap",*/				FALSE,	LL},
{CRL,	"RL",	/*Record length",*/				FALSE,	LL},
{CRP,	"RP",	/*Recorder parameters",*/			TRUE,	LL},
{CRR,	"RR",	/*Recording on/off",*/				FALSE,	LL},
{CRV,	"RV",	/*Recorded variables",*/			TRUE,	LL},
{CHP,	"HP",	/*Halt program execution",*/			FALSE,	LL},
{CKL,	"KL",	/*Kill motion and stop program",*/		FALSE,	LL},
{CMI,	"MI",	/*Mask interrupt",*/				FALSE,	LL},
{CPS,	"PS",	/*Program status",*/				FALSE,	LL},
{CXC,	"XC",	/*Continue program execution",*/		FALSE,	LL},
{CXQ,	"XQ",	/*Execute program",*/				FALSE,	LL},
{CLD,	"LD",	/*Load parameters from flash memory",*/		FALSE,	LL},
{CRS,	"RS",	/*Reset Metronome",*/				FALSE,	LL},
{CSV,	"SV",	/*Save parameters to flash memory",*/		FALSE,	LL},
{CTM,	"TM",	/*System time",*/				FALSE,	LL},
{CTS,	"TS",	/*Sampling time",*/				FALSE,	LL},
{CUF,	"UF",	/*User float array",*/				TRUE,	RR},
{CUI,	"UI",	/*User integer",*/				TRUE,	LL},
{CWI,	"WI",	/*Metronome data",*/				TRUE,	LL},
{CWS,	"WS",	/*Metronome data",*/				TRUE,	LL},
{CZX,	"ZX",	/*Program and auto-tune storage",*/		TRUE,	LL}
	};

//////////////////////////////////////////////////////////////////////////
//
// Elmo Driver
//

// Instantiator

INSTANTIATE(CElmoDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CElmoDriver::CElmoDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uError    = 0;

	memset( m_dLabel, 0, sizeof(m_dLabel) );
	}

// Destructor

CElmoDriver::~CElmoDriver(void)
{
	}

// Configuration

void MCALL CElmoDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CElmoDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CElmoDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CElmoDriver::Open(void)
{
	m_pCL = (ElmoCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CElmoDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CElmoDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CElmoDriver::Ping(void)
{
	return InitComms();
	}

CCODE MCALL CElmoDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	SetpItem( Addr.a.m_Table );

	if( m_pItem == NULL ) return CCODE_ERROR | CCODE_HARD;

	if( ReadNoTransmit(Addr.a.m_Table, pData, &uCount) ) {

		return HasString() ? uCount : 1;
		}

	Sleep(50); // G3 appears to be too fast for Elmo to keep up

	if( DoRead( Addr ) ) {

		if( Transact() ) {

			if( GetResponse( pData, Addr.a.m_Type, &uCount ) ) {

				return HasString() ? uCount : 1;
				}
			}

		else {
			if( m_uError == 0xFFFF ) {

				m_uError = 0;

				return CCODE_ERROR | CCODE_NO_RETRY;
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CElmoDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n*****WRITE***** %d %d %d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	SetpItem( Addr.a.m_Table );

	if( m_pItem == NULL ) return CCODE_ERROR | CCODE_HARD;

	if( WriteNoTransmit( Addr.a.m_Table, pData ) ) return uCount;

	if( DoWrite(Addr, pData, uCount) ) {

		if( Transact() ) {

			if( HasString() ) {

				memcpy( m_dLabel, pData, uCount * sizeof(DWORD) );

				return uCount;
				}

			return HasString() ? uCount : 1;
			}

		if( m_uError == 0xFFFF ) { // same error code repeated

			m_uError = 0;

			return uCount; // stop trying for now
			}
		}

	return CCODE_ERROR;
	}

UINT CElmoDriver::DevCtrl(void * pContext, UINT uFunc, PCTXT Value)
{	
	if( uFunc == 1 ) {

		m_uPtr = 0;

		PutText( LPCTXT(Value) );

		Transact();

		return 1;
		}

	return 0;
	}

// Opcode Handlers

BOOL CElmoDriver::DoRead(AREF Addr)
{
	StartFrame(Addr.a.m_Offset);

	if( m_pItem->uID == CBH ) { // Special case of read

		AddByte('=');

		PutNumber(DWORD(Addr.a.m_Offset), FALSE);
		}

	return TRUE;
	}

BOOL CElmoDriver::DoWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(Addr.a.m_Offset);

	if( !HasNoValue() ) {

		if( HasString() ) AddLabel(pData);

		else {

			AddByte('=');

			DWORD d = *pData;

			PutNumber(d, Addr.a.m_Type == RR && m_pItem->uType == RR);
			}
		}

	return TRUE;
	}

// Header Building

void CElmoDriver::StartFrame(UINT uOffset)
{
	m_uPtr = 0;

	AddCommand();

	if( m_pItem->fHasParam ) {

		AddByte('[');

		PutNumber(DWORD(uOffset), FALSE);

		AddByte(']');
		}
	}

void CElmoDriver::EndFrame(void)
{
	AddByte( CR );
	}

void CElmoDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CElmoDriver::AddCommand(void)
{
	PutText(m_pItem->sName);
	}
	
void CElmoDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) {

		AddByte( BYTE(pCmd[i]) );
		}
	}

void CElmoDriver::AddLabel(PDWORD pData)
{
	BOOL fDone  = FALSE;

	UINT n      = 0;

	while( !fDone ) {

		UINT uShift = 32;

		while( uShift ) {

			uShift -= 8;

			BYTE b = (pData[n] >> uShift) & 0xFF;

			if( !b ) return;

			AddByte(b);
			}

		n++;
		}
	}

// Transport Layer

BOOL CElmoDriver::Transact(void)
{
	Send();

	return GetReply();
	}

void CElmoDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
	}

BOOL CElmoDriver::GetReply(void)
{
	memset( m_bRx, 0, sizeof(m_bRx) );

	UINT uCount    = 0;
	UINT uData     = 0;
	UINT uState    = 0;
	UINT uTime     = TIMEOUT;

	BYTE bData     = 0;

	BYTE bError    = 0;
	BYTE bPrev     = 0;

	char cEcho[MAXTX] = {0};

	SetTimer(uTime);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uData = Get(uTime)) < NOTHING ) {

			bPrev = bData;

			bData = LOBYTE(uData);

//**/			AfxTrace1("<%2.2x>", bData);

			switch( uState ) {

				case 0: // Handle Echo

					if( uCount < MAXTX ) cEcho[uCount++] = bData;

					if( bData == CR ) {

						uState = 1;
						uCount = 0;
						}

					break;

				case 1: // Handle Data

					switch( bData ) {

						case '?':
							bError = bPrev;
							break;

						case ';':
							if( !bError ) {

								m_uError = 0;

								return CheckReply(cEcho);
								}

							if( LOBYTE(m_uError) == bError ) m_uError = 0xFFFF;

							else m_uError = UINT(bError);

							return FALSE;

						default:
							m_bRx[uCount++] = bData;

							if( uCount >= sizeof(m_bRx) ) return FALSE;
							break;
						}

					break;
				}
			}
		}

	return FALSE;
	}

BOOL CElmoDriver::CheckReply(char * cEcho)
{
	for( UINT i = 0; (i < MAXTX) && (cEcho[i] != CR); i++ ) {

		if( cEcho[i] != m_bTx[i] ) return FALSE;
		}

	return TRUE;
	}

BOOL CElmoDriver::GetResponse( PDWORD pData, UINT uType, UINT * pCount )
{
	UINT i = 0;

	if( !m_bRx[0] ) return FALSE;

	if( HasString() ) {

		for( UINT i = 0; i < *pCount; i++ ) {

			DWORD x  = PU4(m_bRx)[i];

			pData[i] = MotorToHost(x);

			for( UINT k = 0; k < 4; k++ ) {

				if( !m_bRx[k] ) {

					*pCount = 1 + i;

					return TRUE;
					}
				}
			}

		return TRUE;
		}

	pData[0] = uType == addrRealAsReal ? GetReal() : GetInteger();

	*pCount  = 1;

	return TRUE;
	}

// Port Access

void CElmoDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

UINT CElmoDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

void CElmoDriver::SetpItem(UINT uID)
{
	m_pItem = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == m_pItem->uID ) return;

		m_pItem++;
		}

	m_pItem = NULL;
	}

void CElmoDriver::PutNumber(DWORD wNum, BOOL fIsReal)
{
	if( wNum == NEGZERO ) wNum = 0;

	char c[12];

	if( fIsReal ) SPrintf( c, "%f", PassFloat(wNum) );

	else SPrintf( c, "%d", wNum);

	for( UINT i = 0; i < strlen(c); i++ ) AddByte(c[i]);
	}

DWORD CElmoDriver::GetInteger(void)
{
	return ATOI((const char *)m_bRx);
	}

DWORD	CElmoDriver::GetReal(void)
{
	return ATOF( (const char * )&m_bRx[0] );
	}

BOOL	CElmoDriver::ReadNoTransmit(UINT uTable, PDWORD pData, UINT * pCount)
{
	switch( uTable ) {

		case CBG:
		case CHP:
		case CKL:
		case CLD:
		case CRS:
		case CST:
		case CSV:
		case CXC:
			*pData = 0;
			return TRUE;

		case CMO:
			*pData = RTNZ;
			return TRUE;

		case CXQ:
			UINT i;
			for( i = 0; i < *pCount; i++ ) pData[i] = m_dLabel[i];
			return TRUE;

		case CBT:
			return TRUE;
		}

	return FALSE;
	}

BOOL	CElmoDriver::WriteNoTransmit(UINT uTable, PDWORD pData)
{
	DWORD dData = *pData;

	switch( uTable ) {

		case CAN:
		case CBH:
		case CBV:
		case CDV:
		case CEC:
		case CIB:
		case CID:
		case CIP:
		case CIQ:
		case CLC:
		case CMC:
		case CMF:
		case CMS:
		case CPE:
		case CPK:
		case CPS:
		case CPX:
		case CPY:
		case CSR:
		case CTI:
		case CVE:
		case CVR:
		case CVX:
		case CVY:
		case CWI:
		case CWS:
			return TRUE;

		case CBT:
			if( !dData ) return TRUE;
			break;

		case CXQ: // must start with "##"
			if( (dData & 0xFFFF0000) != 0x23230000 ) return TRUE;
			break;

		case CBG:
		case CHP:
		case CKL:
		case CLD:
		case CRS:
		case CST:
		case CSV:
		case CXC:
			if( !dData ) return TRUE;

		case CMO:
			if( dData == RTNZ ) return TRUE;
			break;
		}

	return FALSE;
	}

CCODE	CElmoDriver::InitComms(void)
{
	char c[5] = {0};

	SPrintf( c, "EO=1", 0 ); // Turn Echo On

	if( SendInit(c) ) {

		SPrintf( c, "HX=0", 0 );

		if( SendInit(c) ) return 1;
		}

	memset( c, 0, 4 );

	SendInit(c); // reset input buffer

	return CCODE_ERROR;
	}

BOOL	CElmoDriver::SendInit(char * c)
{
	m_uPtr = 0;

	PutText(PCTXT(c));

	return Transact();
	}

BOOL	CElmoDriver::HasNoValue(void)
{
	switch( m_pItem->uID ) {

		case CBG:	return TRUE;
		case CHP:	return TRUE;
		case CKL:	return TRUE;
		case CLD:	return TRUE;
		case CRS:	return TRUE;
		case CST:	return TRUE;
		case CSV:	return TRUE;
		}

	return FALSE;
	}

BOOL	CElmoDriver::HasString(void)
{
	switch( m_pItem->uID ) {

		case CVR:	return TRUE;
		case CXQ:	return TRUE;
		}

	return FALSE;
	}

// End of File
