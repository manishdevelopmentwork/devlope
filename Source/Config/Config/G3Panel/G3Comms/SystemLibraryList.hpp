
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SystemLibraryList_HPP

#define INCLUDE_SystemLibraryList_HPP

//////////////////////////////////////////////////////////////////////////
//
// System Library List
//

class CSystemLibraryList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSystemLibraryList(void);

		// Item Access
		CMetaItem * GetItem(INDEX Index) const;
		CMetaItem * GetItem(UINT uPos) const;

		// Item Lookup
		UINT FindNamePos(CString Name) const;
	};

// End of File

#endif
