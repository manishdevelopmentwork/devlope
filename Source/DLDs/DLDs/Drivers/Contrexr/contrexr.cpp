
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "contrexr.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Contrext M-Rotary Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CContrexRotaryDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CContrexRotaryDriver::CContrexRotaryDriver(void)
{
	m_Ident     = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
}

// Destructor

CContrexRotaryDriver::~CContrexRotaryDriver(void)
{
}

// Configuration

void MCALL CContrexRotaryDriver::Load(LPCBYTE pData)
{
}

void MCALL CContrexRotaryDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CContrexRotaryDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CContrexRotaryDriver::Open(void)
{
	m_fPing = FALSE;
}

// Device

CCODE MCALL CContrexRotaryDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_pCtx->m_bType = GetByte(pData);

			m_pCtx->m_dCommandRead = 0;

			m_pCtx->m_dError = 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CContrexRotaryDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CContrexRotaryDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 70;
	Addr.a.m_Type   = addrRealAsReal;
	Addr.a.m_Extra	= 0;

	m_fPing = TRUE;

	return Read(Addr, Data, 1);
}

CCODE MCALL CContrexRotaryDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( NoTransmit(Addr, pData, FALSE) ) {

		return 1;
	}

//**/	AfxTrace2("\r\nREAD T=%d O=%d ", Addr.a.m_Table, Addr.a.m_Offset);

	AddRead(Addr.a.m_Offset);

	if( Transact() ) {

		if( GetResponse(pData) ) {

			return 1;
		}
	}

	return CCODE_ERROR;
}

CCODE MCALL CContrexRotaryDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\nWRITE T=%d O=%d ", Addr.a.m_Table, Addr.a.m_Offset);

	if( NoTransmit(Addr, pData, TRUE) ) {

		return 1;
	}

	if( Addr.a.m_Table == 1 ) {

		AddRead(Addr.a.m_Offset);

		if( !Transact() || CheckError() ) {

			return 1; // couldn't read variable
		}
	}

	AddWrite(Addr, *pData);

	if( Transact() ) {

		CheckError();

		return 1;
	}

	return CCODE_ERROR;
}

// PRIVATE METHODS

// Frame Building

void CContrexRotaryDriver::StartFrame(void)
{
	m_uPtr = 0;
	AddByte(STX);
	AddByte(m_pHex[m_pCtx->m_bDrop/10]);
	AddByte(m_pHex[m_pCtx->m_bDrop%10]);
}

void CContrexRotaryDriver::EndFrame(void)
{
	m_bTx[15] = ETX;
}

void CContrexRotaryDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
}

void CContrexRotaryDriver::AddData(DWORD dData, DWORD dFactor)
{
	while( dFactor ) {

		AddByte(m_pHex[(dData/dFactor) % 10]);

		dFactor /= 10;
	}
}

void CContrexRotaryDriver::AddRead(UINT uOffset)
{
	StartFrame();

	AddByte(INQUIRY);

	AddData(uOffset, 10);

	AddData(0, 10000000);

	AddByte('0');
}

void CContrexRotaryDriver::AddWrite(AREF Addr, DWORD dData)
{
	StartFrame();

	switch( Addr.a.m_Table ) {

		case 1:
			AddByte(PARSEND);

			AddData(Addr.a.m_Offset, 10); // Parameter Number

			AddRealData(dData, Addr.a.m_Offset); // Construct real data

			break;

		case addrNamed:

			m_pCtx->m_dCommandRead = dData;

			AddByte(COMMAND);

			AddData(0, 10000000); // param 00 + 6 more

			AddData(dData, 10); // 2 char command number

			AddByte('0');

			break;
	}
}

void CContrexRotaryDriver::AddRealData(DWORD dData, UINT uOffset)
{
	HostAlignStack();

	BOOL fNeg = FALSE;

	DWORD dExp;

	char cStart[64] = { 0 };

	if( dData & 0x80000000 ) {

		dData &= 0x7FFFFFFF;

		fNeg = TRUE;
	}

	dExp = dData >> 23;

	if( dExp > 146 ) { // > 999999

		AddData(999999, 10000000);
		AddByte(fNeg ? '7' : '0');
		return;
	}

	else {
		if( dExp < 110 ) { // < .00001

			AddData(0, 10000000);
			AddByte('0');
			return;
		}
	}

	SPrintf(cStart, "%f", PassFloat(dData));

	PutRealData(cStart, fNeg, uOffset);
}

// Transport Layer

BOOL CContrexRotaryDriver::Transact(void)
{
	Send();

	return GetReply();
}

void CContrexRotaryDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

	Put();
}

BOOL CContrexRotaryDriver::GetReply(void)
{
	if( m_pCtx->m_bDrop == 0 ) {

		return TRUE;
	}

	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uData;

	BYTE bData;

	SetTimer(TIMEOUT);

//**/	AfxTrace0("\r\n");

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
		}

		bData = LOBYTE(uData) & 0x7F;

//**/		AfxTrace1("<%2.2x>", bData);

		if( bData == STX ) uCount = 0;

		m_bRx[uCount++] = bData;

		if( bData == ETX ) {

			if( uCount == 16 ) return TRUE;

			else return FALSE;
		}

		if( uCount > 16 ) return FALSE;
	}

	return FALSE;
}

BOOL CContrexRotaryDriver::GetResponse(PDWORD pData)
{
	for( UINT i = 0; i < 3; i++ ) {

		if( m_bRx[i] != m_bTx[i] )

			return FALSE;
	}

	if( !m_fPing ) {

		if( CheckError() ) {

			*pData = 0;

			return TRUE;
		}
	}

	else {
		m_fPing = FALSE;
	}

	GetRealData(pData);

	return TRUE;
}

// Port Access

void CContrexRotaryDriver::Put(void)
{
//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < 16; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write(PCBYTE(m_bTx), 16, FOREVER);
}

UINT CContrexRotaryDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
}

// Helpers

BOOL  CContrexRotaryDriver::NoTransmit(AREF Addr, PDWORD pData, BOOL fIsWrite)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case 3:
				if( !fIsWrite )

					*pData = m_pCtx->m_dError;

				else

					m_pCtx->m_dError = *pData;

				return TRUE;

			case 4:
				if( !fIsWrite ) {

					*pData = m_pCtx->m_dCommandRead;

					return TRUE;
				}

				else
					return FALSE;

			default:
				*pData = 0;

				return TRUE;
		}
	}

	if( !fIsWrite && m_pCtx->m_bDrop == 0 ) {

		*pData = 0;

		return TRUE;
	}

	if( Addr.a.m_Offset > 99 ) {

		*pData = 0;

		return TRUE;
	}

	if( m_pCtx->m_bType ) { // M-Cut

		switch( Addr.a.m_Offset ) {

			case 0:
			case 29:
			case 67:
			case 69:
			case 75:
			case 76:
			case 77:
				*pData = 0;
				return TRUE;// invalid parameters
		}

		if( fIsWrite ) {

			if( (Addr.a.m_Offset >= 40 && Addr.a.m_Offset < 60) ||
			   Addr.a.m_Offset == 74 || Addr.a.m_Offset == 99
			   ) {

				return TRUE; // invalid write parameters
			}
		}
	}

	else { // M-Rotary

		switch( Addr.a.m_Offset ) {

			case 0:
			case 9:
			case 13:
			case 34:
			case 38:
			case 39:
			case 58:
			case 68:
			case 69:
			case 75:
			case 76:
			case 77:
			case 78:
			case 79:
			case 89:
				*pData = 0;
				return TRUE;// invalid parameters
		}

		if( fIsWrite ) {

			if( (Addr.a.m_Offset >= 40 && Addr.a.m_Offset < 60) ||
				(Addr.a.m_Offset >= 74 && Addr.a.m_Offset < 90) ||
			   (Addr.a.m_Offset >= 95)
			   ) {

				return TRUE; // invalid write parameters
			}
		}
	}

	return FALSE;
}

DWORD CContrexRotaryDriver::GetGeneric(PBYTE p, UINT uCt)
{
	DWORD d = 0L;
	BYTE b;

	for( UINT i = 0; i < uCt; i++ ) {

		d *= 10;

		b = p[i];

		if( b >= '0' && b <= '9' )
			d += (b - '0');
	}

	return d;
}

UINT CContrexRotaryDriver::GetDPFromFormat(PBYTE pNeg)
{
	UINT uDP = m_bRx[14] - '0'; // format number

	*pNeg = 0;

	if( uDP > 5 ) {

		*pNeg = 1;

		return uDP - 6;
	}

	return uDP;
}

BOOL CContrexRotaryDriver::GetDPRange(UINT uOffset, UINT * uMax, BOOL * pFixed)
{
	*pFixed	= FALSE;

	if( m_pCtx->m_bType ) {

		switch( uOffset ) {

			case 1:
			case 2:
			case 6:
			case 8:
			case 9:
			case 13:
			case 15:
			case 16:
			case 20:
			case 22:
			case 23:
			case 27:
			case 30:
			case 32:
			case 35:
			case 36:
			case 37:
			case 38:
			case 39:
			case 43:
			case 44:
			case 45:
			case 46:
				*uMax = 3;
				return TRUE;

			case 3:
			case 4:
			case 10:
			case 11:
			case 17:
			case 18:
			case 24:
			case 25:
			case 64:
				*uMax = 2;
				*pFixed = TRUE;
				return TRUE;
		}
	}

	return FALSE;
}

void CContrexRotaryDriver::GetRealData(PDWORD pData)
{
	unsigned char s[10] = { 0 };

	BYTE bNeg   = 0;

	UINT i      = 0;

	UINT uS     = 6;

	UINT uDP    = GetDPFromFormat(&bNeg); // format number

	while( uS < 14 ) {

		if( i == 8 - uDP ) {

			s[i] = '.';
		}

		else {
			s[i] = m_bRx[uS++];
		}
		i++;
	}

	pData[0] = ATOF((const char *) &s[0]);

	if( bNeg ) {

		pData[0] |= 0x80000000;
	}
}

void CContrexRotaryDriver::PutRealData(char * pS, BOOL fNeg, UINT uOffset)
{
	BOOL fFirst      = FALSE;
	BOOL fGotDP      = FALSE;
	BOOL fFixedDP    = FALSE;

	char cInt[8]     = { 0 };
	char cFrac[8]    = { 0 };
	char cRound;

	UINT uI		 = 0;
	UINT uF          = 0;
	UINT uMax        = 0;
	UINT uDP         = 0;
	UINT uMaxCharCt  = fNeg ? 5 : 6;

	if( !GetDPRange(uOffset, &uMax, &fFixedDP) ) {

		uDP = m_bRx[14] - '0'; // use format value from initial read operation

		if( uDP > 5 ) {

			uDP -= 6;
		}
	}

	else {
		if( fFixedDP ) {

			uDP = 2;
		}
	}

	for( UINT i = 0; pS[i]; i++ ) {

		char c = pS[i];

		if( c != '0' || fFirst ) {

			fFirst = TRUE;

			if( c == '.' ) {

				fGotDP = TRUE;

				if( !uI ) {

					cInt[uI++] = '0';
				}
			}

			else {
				if( fGotDP ) {

					cFrac[uF++] = c;
				}

				else {

					cInt[uI++] = c;
				}
			}
		}

		if( uI > uMaxCharCt ) break;

		else {
			if( uF && (uI + uF >= uMaxCharCt) ) {

				i++; // to get rounding character

				break;
			}
		}
	}

	cRound = pS[i] | '0'; // make a null into '0', does nothing to a digit

	if( uI > uMaxCharCt - uDP ) {

		AddData(999999, 10000000);
	}

	else {
		if( m_pCtx->m_bType && !fFixedDP ) {

			if( uF ) {

				uDP = min(uMax, uMaxCharCt - uI);
			}
		}

		if( uF != uDP ) {

			if( uF > uDP ) { // too many

				while( uF > uDP + 1 ) { // remove all but 1

					cFrac[--uF] = 0;
				}

				cRound = cFrac[--uF]; // use the remaining one for round

				cFrac[uF] = 0;
			}

			else {
				while( uF < uDP ) { // fill short fraction

					cFrac[uF++] = '0';

					cRound = '0';
				}
			}
		}

		AddData(Round(cInt, cFrac, cRound, uDP), 10000000);
	}

	if( m_pCtx->m_bType ) {

		AddByte(fFixedDP ? '0' : (fNeg ? uDP + '6' : uDP +'0'));
	}

	else {

		AddByte(fNeg ? '7' : '0');
	}

	return;
}

DWORD CContrexRotaryDriver::Round(char * pI, char * pF, char cRound, UINT uFSize)
{
	DWORD dInt = ATOI(pI);

	if( uFSize ) {

		for( UINT i = 0; i < uFSize; i++ ) {

			dInt *= 10;
		}

		dInt += ATOI(pF);
	}

	dInt *= 10;

	dInt += cRound - '+'; // = cRound - '0' + 5

	return dInt > 9999999 ? 999999 : dInt / 10;
}

BOOL CContrexRotaryDriver::CheckError(void)
{
	BYTE bR3 = m_bRx[3] & 0x3F;

	if( bR3 ) {

		if( m_bTx[3] == '1' ) {

			m_pCtx->m_dError = ((GetGeneric(&m_bTx[12], 2) + 100) << 16) + bR3;
		}

		else {
			if( bR3 == 2 ) {

				return FALSE; // don't signal lone parameter error
			}

			m_pCtx->m_dError = (GetGeneric(&m_bTx[4], 2) << 16) + bR3;
		}

		return TRUE;
	}

	return FALSE;
}

// End of File
