
#include "intern.hpp"

#include "hbmaed.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HBM AED Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CHBMAEDDeviceOptions, CUIItem);

// Constructor

CHBMAEDDeviceOptions::CHBMAEDDeviceOptions(void)
{
	m_Drop = 31;
	}

// Download Support

BOOL CHBMAEDDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CHBMAEDDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// HBM AED Comms Driver
//

// Instantiator

ICommsDriver *	Create_HBMAEDDriver(void)
{
	return New CHBMAEDDriver;
	}

// Constructor

CHBMAEDDriver::CHBMAEDDriver(void)
{
	m_wID		= 0x4012;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "HBM";
	
	m_DriverName	= "AED/PW/FIT";
	
	m_Version	= "1.20";
	
	m_ShortName	= "HBM AED/PW/FIT";

	AddSpaces();
	}

// Binding Control

UINT	CHBMAEDDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CHBMAEDDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration
CLASS	CHBMAEDDriver::GetDeviceConfig()
{
	return AfxRuntimeClass(CHBMAEDDeviceOptions);
	}

// Address Helpers

BOOL	CHBMAEDDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace->m_uTable == 198 || pSpace->m_uTable == 199) {

		Addr.a.m_Offset = 0;
		Addr.a.m_Table  = pSpace->m_uTable;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = pSpace->m_uType;

		return TRUE;
		}

	return CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text);
	}

BOOL	CHBMAEDDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.a.m_Table == 198 || Addr.a.m_Table == 199 ) {

		Text.Printf( "USR%s",

			Addr.a.m_Table == 198 ? "C" : "R"
			);

		return TRUE;
		}

	return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);
	}

// Implementation

void	CHBMAEDDriver::AddSpaces(void)
{
	AddSpace( New CSpace("ACL",	"Automatic Calibration On/Off",			1,	BB) );
	AddSpace( New CSpace("ADR",	"Address",					2,	LL) );
	AddSpace( New CSpace("AOV",	"ADC Overflow Counter",				116,	LL) ); //FEB 06
	AddSpace( New CSpace("ASF",	"Amplifier Signal Filter",			3,	LL) );
	AddSpace( New CSpace("ASS",	"Amplifier Signal Source",			4,	LL) );

	AddSpace( New CSpace("BRK",	"Break",					89,	BB) );

	AddSpace( New CSpace("CAL",	"Calibration",					5,	BB) );
	AddSpace( New CSpace("CBK",	"Coarse Break Limit",				90,	LL) );
	AddSpace( New CSpace("CBT",	"Coarse Break Time",				117,	LL) ); //FEB 06
	AddSpace( New CSpace("CDL",	"Zero setting (+/- 2%)",			118,	BB) ); //FEB 06
	AddSpace( New CSpace("CFD",	"Coarse Flow Disconnect",			91,	LL) );
	AddSpace( New CSpace("CFT",	"Coarse flow time (x10ms)",			119,	LL) ); //FEB 06
	AddSpace( New CSpace("COF",	"Configure MSV/MAV Output Format",		6,	LL) );
	AddSpace( New CSpace("CPV",	"Clear Peak Values",				120,	BB) ); //FEB 06
	AddSpace( New CSpace("CRC",	"Cyclic Redundancy Check",			7,	LL) );
	AddSpace( New CSpace("CSM",	"Checksum",					187,	LL) ); //MAR 11
	AddSpace( New CSpace("CSN",	"Clear Weight/Batch Counter",			92,	BB) );
	AddSpace( New CSpace("CTR",	"Clear Trigger Results",			121,	BB) ); //FEB 06
	AddSpace( New CSpace("CWTN",	"Next Calibration Weight",			8,	LL) );
	AddSpace( New CSpace("CWTL",	"Read Previous Calibration Weight",		87,	LL) );

	AddSpace( New CSpace("DGA",	"Diagnostic Activation",			176,	LL) ); //MAR 06
	AddSpace( New CSpace("DGL",	"Diagnostic Trigger Level",			177,	LL) ); //MAR 06
	AddSpace( New CSpace("DGN",	"Diagnostic Number",				178,	LL) ); //MAR 06
	AddSpace( New CSpace(180, "DGRV","Diagnostic Read Value",		10,	0, 511,	LL) ); //MAR 06
	AddSpace( New CSpace(181, "DGRS","Diagnostic Read Status",		10,	0, 511,	LL) ); //MAR 06
	AddSpace( New CSpace("DGS",	"Diagnostic Start and Status",			179,	LL) ); //MAR 06
	AddSpace( New CSpace("DMD",	"Dosing Mode",					122,	LL) ); //FEB 06
	AddSpace( New CSpace("DPT",	"Decimal Point for MSV",			123,	LL) ); //NOV 08
	AddSpace( New CSpace("DPW",	"Send DPW1+DPW2",				11,	BB) );
	AddSpace( New CSpace("DPW1",	"   Define Password. Characters 1 - 4",		9,	LL) );
	AddSpace( New CSpace("DPW2",	"   Define Password. Characters 5 - 7",		10,	LL) );
	AddSpace( New CSpace("DST",	"Dosing time (x100ms)",				124,	LL) ); //FEB 06
	AddSpace( New CSpace("DZR1",	"Read Dynamic Zero Tracking - Timing",		158,	LL) ); //FEB 06
	AddSpace( New CSpace("DZR2",	"Read Dynamic Zero Tracking - Band",		159,	LL) ); //FEB 06
	AddSpace( New CSpace("DZT",	"Send DZT1+DZT2",				125,	BB) ); //FEB 06
	AddSpace( New CSpace("DZT1",	"   Dynamic Zero Tracking - Timing",		126,	LL) ); //FEB 06
	AddSpace( New CSpace("DZT2",	"   Dynamic Zero Tracking - Band",		127,	LL) ); //FEB 06

	AddSpace( New CSpace("EMD",	"Emptying Mode",				128,	LL) ); //FEB 06
	AddSpace( New CSpace("ENU",	"Engineering Unit",				12,	LL) );
	AddSpace( New CSpace("EPT",	"Emptying Time",				93,	LL) );
	AddSpace( New CSpace("ESR",	"Event Status Register",			13,	LL) );
	AddSpace( New CSpace("EWT",	"Empty Weight",					94,	LL) );

	AddSpace( New CSpace("FBK",	"Fine Break Difference",			95,	LL) );
	AddSpace( New CSpace("FBT",	"Fine break time",				129,	LL) ); //FEB 06
	AddSpace( New CSpace("FFD",	"Fine Flow Disconnect",				96,	LL) );
	AddSpace( New CSpace("FFL",	"First Fine Flow Time",				130,	LL) ); //FEB 06
	AddSpace( New CSpace("FFM",	"Fine Feed Minimum",				97,	LL) );
	AddSpace( New CSpace("FFT",	"Fine flow time (x10ms)",			131,	LL) ); //FEB 06
	AddSpace( New CSpace("FMD",	"Filter Mode",					14,	LL) );
	AddSpace( New CSpace("FNB",	"Actual Parameter Set",				132,	LL) ); //FEB 06
	AddSpace( New CSpace("FRS",	"Filling Result",				98,	LL) );
	AddSpace( New CSpace("FWT",	"Filling Weight",				99,	LL) );

	AddSpace( New CSpace("GRU",	"Group Address",				15,	LL) );

	AddSpace( New CSpace("HSM",	"High Speed Mode",				133,	LL) ); //FEB 06

	AddSpace( New CSpace("ICR",	"Internal Conversion Rate",			16,	LL) );
	AddSpace( New CSpace("IDN0",	"IDN Characters  1 -  4",			17,	LL) );
	AddSpace( New CSpace("IDN1",	"IDN Characters  5 -  8",			18,	LL) );
	AddSpace( New CSpace("IDN2",	"IDN Characters  9 - 12",			19,	LL) );
	AddSpace( New CSpace("IDN3",	"IDN Characters 13 - 16",			20,	LL) );
	AddSpace( New CSpace("IDN4",	"IDN Characters 17 - 20",			21,	LL) );
	AddSpace( New CSpace("IDN5",	"IDN Characters 21 - 24",			22,	LL) );
	AddSpace( New CSpace("IDN6",	"IDN Characters 25 - 28",			23,	LL) );
	AddSpace( New CSpace("IDN7",	"IDN Characters 29 - 32",			24,	LL) );
	AddSpace( New CSpace("IMD",	"Input Mode",					26,	LL) );

	AddSpace( New CSpace("LDWC",	"Load Cell Dead Weight - Command",		27,	BB) );
	AddSpace( New CSpace("LDWV",	"Load Cell Dead Weight - Value",		28,	LL) );
	AddSpace( New CSpace("LDST",	"LDWC/LDWV Write Status",			183,	BB) );
	AddSpace( New CSpace("LFT",	"Legal For Trade",				29,	LL) );
	AddSpace( New CSpace("LIC0",	"Linearization Coefficient 0",			30,	LL) );
	AddSpace( New CSpace("LIC1",	"Linearization Coefficient 1",			31,	LL) );
	AddSpace( New CSpace("LIC2",	"Linearization Coefficient 2",			32,	LL) );
	AddSpace( New CSpace("LIC3",	"Linearization Coefficient 3",			33,	LL) );

	AddSpace( New CSpace("L1RM",	"(LIV) Read Limit 1 Monitoring on/off",		34,	LL) );
	AddSpace( New CSpace("L1RI",	"(LIV) Read Limit 1 Input Value",		35,	LL) );
	AddSpace( New CSpace("L1RO",	"(LIV) Read Limit 1 Switch On Value",		36,	LL) );
	AddSpace( New CSpace("L1RF",	"(LIV) Read Limit 1 Switch Off Value",		37,	LL) );
	AddSpace( New CSpace("L2RM",	"(LIV) Read Limit 2 Monitoring on/off",		38,	LL) );
	AddSpace( New CSpace("L2RI",	"(LIV) Read Limit 2 Input Value",		39,	LL) );
	AddSpace( New CSpace("L2RO",	"(LIV) Read Limit 2 Switch On Value",		40,	LL) );
	AddSpace( New CSpace("L2RF",	"(LIV) Read Limit 2 Switch Off Value",		41,	LL) );
	AddSpace( New CSpace("L3RM",	"(LIV) Read Limit 3 Monitoring on/off",		160,	LL) );
	AddSpace( New CSpace("L3RI",	"(LIV) Read Limit 3 Input Value",		161,	LL) );
	AddSpace( New CSpace("L3RO",	"(LIV) Read Limit 3 Switch On Value",		162,	LL) );
	AddSpace( New CSpace("L3RF",	"(LIV) Read Limit 3 Switch Off Value",		163,	LL) );
	AddSpace( New CSpace("L4RM",	"(LIV) Read Limit 4 Monitoring on/off",		164,	LL) );
	AddSpace( New CSpace("L4RI",	"(LIV) Read Limit 4 Input Value",		165,	LL) );
	AddSpace( New CSpace("L4RO",	"(LIV) Read Limit 4 Switch On Value",		166,	LL) );
	AddSpace( New CSpace("L4RF",	"(LIV) Read Limit 4 Switch Off Value",		167,	LL) );
	AddSpace( New CSpace("LIV",	"Send Limits LxWM-LxWF ( x = <1,2,3,4> )",	50,	YY) );
	AddSpace( New CSpace("L1WM",	"   (LIV) Store Limit 1 Monitoring on/off",	42,	LL) );
	AddSpace( New CSpace("L1WI",	"   (LIV) Store Limit 1 Input Value",		43,	LL) );
	AddSpace( New CSpace("L1WO",	"   (LIV) Store Limit 1 Switch On Value",	44,	LL) );
	AddSpace( New CSpace("L1WF",	"   (LIV) Store Limit 1 Switch Off Value",	45,	LL) );
	AddSpace( New CSpace("L2WM",	"   (LIV) Store Limit 2 Monitoring on/off",	46,	LL) );
	AddSpace( New CSpace("L2WI",	"   (LIV) Store Limit 2 Input Value",		47,	LL) );
	AddSpace( New CSpace("L2WO",	"   (LIV) Store Limit 2 Switch On Value",	48,	LL) );
	AddSpace( New CSpace("L2WF",	"   (LIV) Store Limit 2 Switch Off Value",	49,	LL) );
	AddSpace( New CSpace("L3WM",	"   (LIV) Store Limit 3 Monitoring on/off",	168,	LL) );
	AddSpace( New CSpace("L3WI",	"   (LIV) Store Limit 3 Input Value",		169,	LL) );
	AddSpace( New CSpace("L3WO",	"   (LIV) Store Limit 3 Switch On Value",	170,	LL) );
	AddSpace( New CSpace("L3WF",	"   (LIV) Store Limit 3 Switch Off Value",	171,	LL) );
	AddSpace( New CSpace("L4WM",	"   (LIV) Store Limit 4 Monitoring on/off",	172,	LL) );
	AddSpace( New CSpace("L4WI",	"   (LIV) Store Limit 4 Input Value",		173,	LL) );
	AddSpace( New CSpace("L4WO",	"   (LIV) Store Limit 4 Switch On Value",	174,	LL) );
	AddSpace( New CSpace("L4WF",	"   (LIV) Store Limit 4 Switch Off Value",	175,	LL) );

	AddSpace( New CSpace("LTC",	"Lockout Time Coarse",				100,	LL) );
	AddSpace( New CSpace("LTF",	"Lockout Time Fine",				101,	LL) );
	AddSpace( New CSpace("LTL",	"Lower Tolerance Limit",			102,	LL) );
	AddSpace( New CSpace("LWTC",	"Load Cell Weight - Command",			51,	BB) );
	AddSpace( New CSpace("LWTV",	"Load Cell Weight - Value",			52,	LL) );
	AddSpace( New CSpace("LWST",	"LWTC/LWTV Write Status",			184,	BB) );

	AddSpace( New CSpace("MAVR",	"Measured Alternative Value",			186,	RR) ); //NOV 08
	AddSpace( New CSpace("MDT",	"Max. dosing time (x100ms)",			134,	LL) ); //FEB 06
	AddSpace( New CSpace("MRA",	"Multi Range Switchpoint",			135,	LL) ); //FEB 06
	AddSpace( New CSpace("MSVR",	"Measured Signal Value - Real",			185,	RR) ); //NOV 08
	AddSpace( New CSpace("MTD",	"Motion Detection - Standstill Value",		55,	LL) );

	AddSpace( New CSpace("NDS",	"Number of Dosings",				103,	LL) );
	AddSpace( New CSpace("NOV",	"Nominal Output Value",				56,	LL) );
	AddSpace( New CSpace("NTR1",	"Read Notchfilter 1 Cutoff Frequency",		136,	LL) ); //FEB 06
	AddSpace( New CSpace("NTR2",	"Read Notchfilter 2 Cutoff Frequency",		137,	LL) ); //FEB 06
	AddSpace( New CSpace("NTF",	"Send NTW1+NTW2",				138,	BB) ); //FEB 06
	AddSpace( New CSpace("NTW1",	"   Notchfilter 1 Cutoff Frequency",		139,	LL) ); //FEB 06
	AddSpace( New CSpace("NTW2",	"   Notchfilter 2 Cutoff Frequency",		140,	LL) ); //FEB 06

	AddSpace( New CSpace("OMD",	"Output Mode",					104,	YY) );
	AddSpace( New CSpace("OSN",	"Optimisation",					105,	YY) );

	AddSpace( New CSpace("POR1",	"Set Output 1 On, Output 2 Unchanged",		57,	BB) );
	AddSpace( New CSpace("POR2",	"Set Output 2 On, Output 1 Unchanged",		88,	BB) );
	AddSpace( New CSpace("POR",	"I/O (O6,O5,O4,O3,I2,I1,O2,O1)",		58,	YY) );
	AddSpace( New CSpace("PVAL",	"Read Minimum Peak Value",			141,	LL) ); //FEB 06
	AddSpace( New CSpace("PVAH",	"Read Maximum Peak Value",			142,	LL) ); //FEB 06
	AddSpace( New CSpace("PVS",	"Send PVS1+PVS2",				143,	BB) ); //FEB 06
	AddSpace( New CSpace("PVS1",	"   Extreme Value Function Off(0), On(1)",	144,	LL) ); //FEB 06
	AddSpace( New CSpace("PVS2",	"   Net(0), Gross(1), Trigger(2)",		145,	LL) ); //FEB 06

	AddSpace( New CSpace("RDP",	"Dosing Parameter Set",				146,	LL) ); //FEB 06
	AddSpace( New CSpace("RDS",	"Redosing",					147,	LL) ); //FEB 06
	AddSpace( New CSpace("RES",	"Restart",					59,	BB) );
	AddSpace( New CSpace("RFT",	"Residual Flow Time",				106,	LL) );
	AddSpace( New CSpace("RIO",	"Read Status-Digital I/O and Measurement",	182,	LL) ); //MAR 06
	AddSpace( New CSpace("RSN",	"Resolution",					107,	LL) );
	AddSpace( New CSpace("RUN",	"Run",						108,	BB) );

	AddSpace( New CSpace("SDF",	"Special Dosing Functions",			148,	LL) ); //FEB 06
	AddSpace( New CSpace("SDM",	"Mean Value Dosing Results",			149,	LL) ); //FEB 06
	AddSpace( New CSpace("SDO",	"State of Dosing",				109,	YY) );
	AddSpace( New CSpace("SDS",	"Standard Deviation, Dosing Results",		150,	LL) ); //FEB 06
	AddSpace( New CSpace("SEL",	"Select Device",				60,	YY) );
	AddSpace( New CSpace("SFAC",	"Sensor Full Scale Adjust - Command",		61,	BB) );
	AddSpace( New CSpace("SFAV",	"Sensor Full Scale Adjust - Value",		62,	LL) );
	AddSpace( New CSpace("SOV",	"Sensor Overflow Counter",			151,	LL) ); //FEB 06
	AddSpace( New CSpace("SPW",	"Send SPW1+SPW2",				65,	BB) );
	AddSpace( New CSpace("SPW1",	"   Set Password. Characters 1 - 4",		63,	LL) );
	AddSpace( New CSpace("SPW2",	"   Set Password. Characters 5 - 7",		64,	LL) );
	AddSpace( New CSpace("STR",	"Bus Termination - On/Off",			66,	LL) );
	AddSpace( New CSpace("STT",	"Stablilization Time",				110,	LL) );
	AddSpace( New CSpace("SUM",	"Cumulative Weight",				111,	LL) );
	AddSpace( New CSpace("SYD",	"Systematic Difference",			112,	LL) );
	AddSpace( New CSpace("SZAC",	"Sensor Zero Adjust - Command",			67,	BB) );
	AddSpace( New CSpace("SZAV",	"Sensor Zero Adjust - Value",			68,	LL) );

	AddSpace( New CSpace("TAD",	"Tare Delay",					113,	LL) );
	AddSpace( New CSpace("TAR",	"Taring",					69,	BB) );
	AddSpace( New CSpace("TAS",	"Tare Set - Gross/Net",				70,	BB) );
	AddSpace( New CSpace("TAV",	"Tare Value",					71,	LL) );
	AddSpace( New CSpace("TCR",	"Trade Counter",				72,	LL) );
	AddSpace( New CSpace("TDD",	"Transmit Device Data",				73,	YY) );
	AddSpace( New CSpace("TMD",	"Tare Mode",					114,	BB) );

	AddSpace( New CSpace("TRC",	"Send TRW1+TRW2+TRW3+TRW4+TRW5",		84,	BB) );
	AddSpace( New CSpace("TRW1",	"   Store Trigger Setting 1",			79,	BB) );
	AddSpace( New CSpace("TRW2",	"   Store Trigger Setting 2",			80,	YY) );
	AddSpace( New CSpace("TRW3",	"   Store Trigger Setting 3",			81,	LL) );
	AddSpace( New CSpace("TRW4",	"   Store Trigger Setting 4",			82,	LL) );
	AddSpace( New CSpace("TRW5",	"   Store Trigger Setting 5",			83,	LL) );
	AddSpace( New CSpace("TRR1",	"Read Trigger Setting 1",			74,	BB) );
	AddSpace( New CSpace("TRR2",	"Read Trigger Setting 2",			75,	YY) );
	AddSpace( New CSpace("TRR3",	"Read Trigger Setting 3",			76,	LL) );
	AddSpace( New CSpace("TRR4",	"Read Trigger Setting 4",			77,	LL) );
	AddSpace( New CSpace("TRR5",	"Read Trigger Setting 5",			78,	LL) );

	AddSpace( New CSpace("TRF",	"Trigger Correction Factor",			152,	LL) ); //FEB 06
	AddSpace( New CSpace("TRM",	"Trigger Mean Value",				153,	LL) ); //FEB 06
	AddSpace( New CSpace("TRN",	"Number of Trigger Results",			154,	LL) ); //FEB 06
	AddSpace( New CSpace("TRS",	"Standard Deviation, Trigger",			155,	LL) ); //FEB 06

	AddSpace( New CSpace("USR",	"Send USRC",					197,	BB) );		//NOV 08
	AddSpace( New CSpace(198, "USRC","User Command String",				10,	0, 9,	LL) );	//NOV 08	
	AddSpace( New CSpace(199, "USRR","USRC Response String",			10,	0, 9,	LL) );	//NOV 08	
	AddSpace( New CSpace("UTL",	"Upper Tolerance Limit",			115,	LL) );

	AddSpace( New CSpace("VCT",	"Valve control",				156,	LL) ); //FEB 06

	AddSpace( New CSpace("WDP",	"Store Parameter Set",				157,	LL) ); //FEB 06

	AddSpace( New CSpace("ZSE",	"Initial Zero Setting",				85,	LL) );
	AddSpace( New CSpace("ZTR",	"Zero Tracking On/Off",				86,	BB) );

	AddSpace( New CSpace("MAV",	"Measured Alternative Value (Legacy)",		53,	LL) );
	AddSpace( New CSpace("MSV",	"Measured Signal Value (Legacy)",		54,	LL) );
	}

// End of File
