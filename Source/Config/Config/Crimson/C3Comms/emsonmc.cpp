
#include "intern.hpp"

#include "emsonmc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emerson Driver Options in emsonep.hpp
//

//////////////////////////////////////////////////////////////////////////
//
// Emerson MC Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEmersonMCDeviceOptions, CUIItem);       

// Constructor

CEmersonMCDeviceOptions::CEmersonMCDeviceOptions(void)
{
	m_Unit     = 1;
	}

// UI Management

void CEmersonMCDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEmersonMCDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Unit));

	return TRUE;
	}

// Meta Data Creation

void CEmersonMCDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Unit);
       	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson MC Driver
//

// Instantiator

ICommsDriver *	Create_EmersonMCDriver(void)
{
	return New CEmersonMCDriver;
	}

// Constructor

CEmersonMCDriver::CEmersonMCDriver(void)
{
	m_wID		= 0x403C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Nidec - Control Techniques";
	
	m_DriverName	= "Motion Coordinator";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Motion Coordinator";

	m_DevRoot	= "MC";

	AddSpaces();
	}

// Binding Control

UINT	CEmersonMCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void	CEmersonMCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration
CLASS CEmersonMCDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEmersonDriverOptions);
	}

// Configuration
CLASS CEmersonMCDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonMCDeviceOptions);
	}

// Implementation	

void CEmersonMCDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SPOUT, "MCOP", "Motion Coordinator Output Bit",	10, 0,	9998,	BB));
	AddSpace(New CSpace(SPINP, "MCIN", "Motion Coordinator Input Bit",	10, 0,	9998,	BB));
	AddSpace(New CSpace(SPHOLD,"MCVR", "Motion Coordinator Register",	10, 0,	9998,	WW));
	}

// End of File
