
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_ACTECHSS_HPP

#define	INCLUDE_ACTECHSS_HPP

#define	AN	addrNamed
#define BB	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Valid Function Code Definitions
#define	FA		0x7000 // Drive Paramters
#define	FB		0x7001 // IO Control
#define	FC		0x7002 // Limits Handling
#define	FD		0x7003 // Monitoring Function
#define	FE		0x7004 // Indexer Variables
#define	FF		0x7005 // Motion Commands
#define	FZ		0x7013 // Section Title

#define	SPDRIVE		0x1000
#define	SPIO		0x1001
#define	SPLIMITS	0x1002
#define	SPMONITOR	0x1003
#define	SPINDEXER	0x1004
#define	SPMOTION	0x1005

#define	SPNONE		0x2000

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo Master Driver
//

class CACTechSSDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CACTechSSDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Helpers
		CSpace * GetSpace(CAddress const &Addr);
		BOOL MatchSpace(CSpace *pSpace, CAddress const &Addr);

	protected:
		// Data

		// Implementation
		void AddSpaces(void);

		// Helpers

		// Friend
		friend class CACTechSSAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo UDP Master Device Options
//

class CACTechSSUDPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CACTechSSUDPDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo UDP Master Driver
//

class CACTechSSUDPDriver : public CACTechSSDriver
{
	public:
		// Constructor
		CACTechSSUDPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// ACTech Simple Servo Address Selection
//

class CACTechSSAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CACTechSSAddrDialog(CACTechSSDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		UINT m_uAllowSpace;
		CACTechSSDriver * m_pACDriver;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSpaceChange(UINT uID, CWnd &Wnd);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Overridables
		void ShowAddress(CAddress Addr);

		// Helpers
		BOOL AllowSpace(CSpace *pSpace);
		BOOL SelectList(CSpace *pSpace, UINT uCommand, UINT uAllow);
		void SetAllow(void);
		UINT IsHeader(UINT uCommand);
		UINT GetTableSpace(CSpace *pSpace);
	};

// End of File

#endif
