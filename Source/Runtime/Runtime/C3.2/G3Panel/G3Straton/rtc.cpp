
#include "intern.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Prototypes

clink	T5_BYTE		rtcGetString(T5_LONG uType, T5_PTCHAR pString);
clink	T5_DWORD	rtcGetCurDateStamp (void);
clink	T5_DWORD	rtcGetCurTimeStamp (void);
clink	void		rtcGetCurDateTimeStamp(T5_BOOL bLocal, T5_PTDWORD pdwDate, T5_PTDWORD pdwTime, T5_PTBOOL pbDST);
static	PCTXT		GetDayOfWeek(void);

// Code

clink T5_BYTE rtcGetString(T5_LONG uType, T5_PTCHAR pString)
{
	UINT p[3];

	switch( uType ) {

		case 0:
			::GetWholeDate(::GetNow(), p);

			AfxTrace("date:%u:%u:%u\n", p[2], p[1], p[0]);

			sprintf(pString, "%4.4d/%2.2d/%2.2d", p[2], p[1], p[0]);

			break;

		case 1:
			::GetWholeTime(::GetNow(), p);

			sprintf(pString, "%2.2d:%2.2d:%2.2d", p[2], p[1], p[0]);

			break;

		case 2:	
			sprintf(pString, "%s", GetDayOfWeek());
			
			break;
		}

	return strlen(pString);
	}

global T5_DWORD rtcGetCurDateStamp (void)
{
	CTime t;

	::GetTime(t);

	return MAKELONG(MAKEWORD(t.uDate, t.uMonth), t.uYear);
	}

global T5_DWORD	rtcGetCurTimeStamp (void)
{
	CTime t;

	::GetTime(t);	

	return ::Time(t.uHours, t.uMinutes, t.uSeconds) * 1000;
	}

global void rtcGetCurDateTimeStamp(T5_BOOL bLocal, T5_PTDWORD pdwDate, T5_PTDWORD pdwTime, T5_PTBOOL pbDST)
{
	DWORD t = ::GetNow() + (bLocal ? 0 : GetZuluOffset());

	if( pdwDate ) {

		UINT p[3];

		::GetWholeDate(t, p);

		*pdwDate = MAKELONG(MAKEWORD(p[0], p[1]), p[2]);
		}

	if( pdwTime ) {

		UINT p[3];

		::GetWholeTime(t, p);

		*pdwTime = ::Time(p[2], p[1], p[0]) * 1000;
		}

	if( pbDST ) {

		*pbDST = ::GetDaylight();
		}
	}

static PCTXT GetDayOfWeek(void)
{
	static const PCTXT DayOfWeek[] = {

			"Sunday",
			"Monday",
			"Tuesday",
			"Wednesday",
			"Thursday",
			"Friday",
			"Saturday",
		};

	return DayOfWeek[::GetDay(::GetNow())];
	}

// End of File
