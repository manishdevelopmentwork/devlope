
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Slave Proxy Object
//

class CProxySlave : public CProxy
{
	public:
		// Constructor
		CProxySlave(void);

		// Destructor
		~CProxySlave(void);

		// Binding
		BOOL Bind(CCommsPort *pPort, IDriver *pDriver);

		// Entry Points
		void Init(UINT uID);
		void Task(UINT uID);
		void Term(UINT uID);

	protected:
		// Data Members
		BOOL               m_fOpen;
		ICommsSlave      * m_pSlave;
		ISlaveHelper     * m_pSlaveHelper;
		CCommsDeviceList * m_pDevices;
		CCommsDevice     * m_pDevice;
	};

//////////////////////////////////////////////////////////////////////////
//
// Slave Proxy Object
//

// Helper Creation

extern ISlaveHelper * Create_SlaveHelper(CCommsDevice *pDevice);

// Instantiator

CProxy * Create_ProxySlave(void)
{
	return New CProxySlave;
	}

// Constructor

CProxySlave::CProxySlave(void)
{
	m_fOpen        = FALSE;
	
	m_pSlave       = NULL;
	
	m_pSlaveHelper = NULL;

	m_pDevice      = NULL;
	}

// Destructor

CProxySlave::~CProxySlave(void)
{
	AfxRelease(m_pSlave);

	AfxRelease(m_pSlaveHelper);
	}

// Binding

BOOL CProxySlave::Bind(CCommsPort *pPort, IDriver *pDriver)
{
	if( CProxy::Bind(pPort, pDriver) ) {

		m_pDriver->QueryInterface(AfxAeonIID(ICommsSlave), (void **) &m_pSlave);

		if( m_pSlave ) {

			m_pDevices = m_pPort->m_pDevices;

			if( m_pDevices->m_uCount ) {

				m_pDevice = m_pDevices->m_ppDevice[0];
				}

			m_pSlaveHelper = Create_SlaveHelper(m_pDevice);

			m_pSlave->SetHelper(m_pSlaveHelper);

			return TRUE;
			}
		}

	return FALSE;
	}

// Entry Points

void CProxySlave::Init(UINT uID)
{
	}

void CProxySlave::Task(UINT uID)
{
	m_pCommsHelper->WontReturn();

	if( m_pPort->Open(m_pComms) ) {

		m_fOpen = TRUE;

		if( m_pDevice ) {
		
			if( m_pDevice->IsEnabled() ) {

				CCODE cc = m_pComms->DeviceOpen(m_pDevice);

				if( COMMS_SUCCESS(cc) ) {

					m_pDevice->SetError(errorNone);

					for(;;) {
						
						m_pComms->Service();

						ForceSleep(5);
						}
					}
				}
			}
		else {
			for(;;) {
				
				m_pComms->Service();

				ForceSleep(20);
				}
			}
		}
	
	Sleep(FOREVER);
	}

void CProxySlave::Term(UINT uID)
{
	if( m_fOpen ) {

		m_pPort->Term();
		}
	}

// End of File
