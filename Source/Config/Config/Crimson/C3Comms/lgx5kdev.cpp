 
#include "intern.hpp"

#include "logix5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABLogix5DeviceOptions, CUIItem);

// Constructor

CABLogix5DeviceOptions::CABLogix5DeviceOptions(void)
{
	}

// Attributes

CString CABLogix5DeviceOptions::GetDeviceName(void)
{
	CMetaItem       *pItem = (CMetaItem *) GetParent();

	CMetaData const *pData = pItem->FindMetaData(TEXT("Name"));

	return pData ? pData->ReadString(pItem) : TEXT("Device");
	}

// Operations

BOOL CABLogix5DeviceOptions::CreateTag(CError &Error, CString const &Name, CAddress Addr, BOOL fNew)
{
	INDEX Index = m_TagsFwdMap.FindName(Name);
	
	if( !m_TagsFwdMap.Failed(Index) ) {

		Error.Set(CString("This name is already in use."), 0);

		return FALSE;
		}

	if( fNew ) {
	
		UINT uIndex = CreateIndex();

		if( IsTable(Addr) ) {

			if( uIndex < 0xF0 ) {

				Addr.a.m_Offset = 0;
				
				Addr.a.m_Table  = uIndex;
				}
			else {
				Error.Set("Could not create an array tag.", 0);
				
				return FALSE;
				}
			}
		else 
			Addr.a.m_Offset = uIndex;
		}
	
	m_TagsFwdMap.Insert(Name, Addr.m_Ref);
	
	m_TagsRevMap.Insert(Addr.m_Ref, Name);

	return TRUE;
	}

void CABLogix5DeviceOptions::DeleteTag(CAddress const &Addr, BOOL fKill)
{
	INDEX Index = m_TagsFwdMap.FindData(Addr.m_Ref);

	if( !m_TagsFwdMap.Failed(Index) ) {

		CString Name = m_TagsFwdMap.GetName(Index);
		
		m_TagsFwdMap.Remove(Name);

		m_TagsRevMap.Remove(Addr.m_Ref);

		if( fKill ) {

			UINT uIndex = GetIndex(Addr);
		
			m_AddrArray.Append(uIndex);
			}
		}
	}

BOOL CABLogix5DeviceOptions::RenameTag(CString const &Name, CAddress const &Addr)
{
	CString Old;

	ExpandAddress(Old, Addr);
	
	DeleteTag(Addr, FALSE);

	CError Error(TRUE);

	if( !CreateTag(Error, Name, Addr, FALSE) ) {
		
		Error.Show(*afxMainWnd);

		CreateTag(Error, Old, Addr, FALSE);

		return FALSE;
		}

	return TRUE;
	}

void CABLogix5DeviceOptions::ListTags(CTagDataL5Array &List)
{
	INDEX Index = m_TagsFwdMap.GetHead();

	while( !m_TagsFwdMap.Failed(Index) ) {

		CTagDataL5 Data;

		Data.m_Name = m_TagsFwdMap.GetName(Index);
		
		Data.m_Addr = m_TagsFwdMap.GetData(Index);

		List.Append(Data);
		
		m_TagsFwdMap.GetNext(Index);
		}
	}

// Address Management

BOOL CABLogix5DeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CString Name  = GetTagName(Text);

	INDEX   Index = m_TagsFwdMap.FindName(Name);

	if( !m_TagsFwdMap.Failed(Index) ) {

		Addr.m_Ref   = m_TagsFwdMap.GetData(Index);

		UINT uOffset = GetOffset(Addr, Text);

		if( IsTable(Addr) ) {

			Addr.a.m_Offset = uOffset;
			}

		if( IsString(Addr) ) {

			Addr.a.m_Offset = uOffset * AB_STR_MAX;
			}

		return TRUE;
		}

	//AfxTrace("name not in fwd map <%s>\n", Text);

	return FALSE;
	}

BOOL CABLogix5DeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	CAddress Find = Addr;

	if( IsTable(Addr) ) {
		
		Find.a.m_Offset = 0;
		}

	INDEX Index = m_TagsRevMap.FindName(Find.m_Ref);

	if( !m_TagsRevMap.Failed(Index) ) {

		Text = m_TagsRevMap.GetData(Index);

		UINT uOffset  = GetOffset(Addr);

		if( IsString(Addr) ) {

			uOffset /= AB_STR_MAX;
			}

		if( IsArray(Addr) ) {

			Text += CPrintf("[%d]", IsTriplet(Addr) ? uOffset / 3 : uOffset);
			}

		if( IsTriplet(Addr) ) {

			CStringArray List;

			GetTriplets(Addr, List);
			
			Text += CPrintf(".%s", List[uOffset % 3]);
			}

		return TRUE;
		}

	// Debug

	if( Addr.m_Ref ) {

		//AfxTrace("addr not in rev map [%X]\n", Addr.m_Ref);
		}

	return FALSE;
	}

BOOL CABLogix5DeviceOptions::SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart)
{
	CABLogix5Driver Driver;
	
	CABLogix5Dialog Dlg(Driver, Addr, this, fPart);

	CString Title;

	if( fPart ) {

		CString Name;

		ExpandAddress(Name, Addr);
		
		Title = CPrintf("Select Address for Tag %s", GetTagName(Name));
		}
	else 
		Title = CPrintf("Select Tag for %s", GetDeviceName());

	Dlg.SetCaption(Title);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CABLogix5DeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			m_List.Empty();
			
			ListTags(m_List);

			if( !m_List.GetCount() ) {

				return FALSE;
				}
			}
		else {
			if( uItem >= m_List.GetCount() ) {

				return FALSE;
				}
			}
		
		CTagDataL5 &TagData = (CTagDataL5 &) m_List[uItem];

		CAddress Addr = (CAddress &) TagData.m_Addr;
			
		Data.m_Name   = TagData.m_Name;
		Data.m_Addr   = Addr;
		Data.m_fPart  = IsTable(Addr);
		Data.m_Type   = FindDataTagType(Addr);

		return TRUE;
		}

	return FALSE;
	}

// UI Management

void CABLogix5DeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Push" ) {

			switch( m_Push ) {

				case 0:
					OnManage(&pHost->GetWindow());
					break;

				case 1:
					OnImport(&pHost->GetWindow());
					break;

				case 2:
					OnExport(&pHost->GetWindow());
					break;
				}

			return;
			}
		}
	
	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CABLogix5DeviceOptions::Init(void)
{
	CUIItem::Init();

	MakeTestTags();
	}

void CABLogix5DeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Tags" ) {

			Tree.GetObject();

			LoadTags(Tree);

			Tree.EndObject();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CABLogix5DeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);	

	if( m_TagsFwdMap.GetCount() ) {

		Tree.PutObject(TEXT("Tags"));

		SaveTags(Tree);

		Tree.EndObject();
		}
	}

// Download Support

void CABLogix5DeviceOptions::PrepareData(void)
{
	m_Tags.Empty();

	CTagDataL5Array List;

	ListTags(List);

	List.Sort();

	for( UINT j = 0, i = 0; i < List.GetCount(); j ++ ) {

		CTagDataL5 &Data = (CTagDataL5 &) List[i];

		UINT    uIndex = GetIndex((CAddress &) Data.m_Addr);

		if( uIndex == j ) {
		
			m_Tags.Append(Data.m_Name);

			i ++;
			}
		else 
			m_Tags.Append("");
		}
	}

BOOL CABLogix5DeviceOptions::MakeInitData(CInitData &Init)
{
	UINT uCount = m_Tags.GetCount();

	Init.AddWord(WORD(uCount));

	for( UINT i = 0; i < uCount; i ++ ) {

		AddText(Init, m_Tags[i]);
		}
	
	return TRUE;
	}

// Meta Data Creation

void CABLogix5DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	
	Meta_AddInteger(Push);
	}

// Property Save Filter

BOOL CABLogix5DeviceOptions::SaveProp(CString Tag) const
{
	if( Tag == "Push" ) {
		
		return FALSE;
		}

	return CUIItem::SaveProp(Tag);
	}

// Implementation

void CABLogix5DeviceOptions::OnManage(CWnd *pWnd)
{
	CAddress Addr;

	Addr.m_Ref = 0;

	CABLogix5Driver Driver;
	
	CABLogix5Dialog Dlg(Driver, Addr, this);

	Dlg.SetCaption(CPrintf("Tag Names for %s", GetDeviceName()));

	Dlg.SetSelect(FALSE);

	Dlg.Execute(*pWnd);
	}

void CABLogix5DeviceOptions::OnImport(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("AB Tags"));

	Dlg.SetCaption(CPrintf("Import Tags for %s", GetDeviceName()));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			if( m_TagsFwdMap.GetCount() ) {
				
				CString Text =  TEXT("All tags will be replaced with data from the selected file.\n\n")
						TEXT("Do you want to continue?");

				if( pWnd->YesNo(Text) == IDNO ) {

					fclose(pFile);
					
					return;
					}

				m_TagsFwdMap.Empty();
				
				m_TagsRevMap.Empty();
			
				m_AddrArray.Empty();
				}

			if( Import(pFile) ) {

				SetDirty();
				}

			ShowErrors(pWnd);

			Dlg.SaveLastPath(TEXT("AB Tags"));
			
			fclose(pFile);
			}
		}
	}

void CABLogix5DeviceOptions::OnExport(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(TEXT("AB Tags"));

	Dlg.SetCaption(CPrintf("Export Tags for %s", GetDeviceName()));

	Dlg.SetFilter (TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\n\n")
				       TEXT("Do you want to overwrite it?");

			if( pWnd->YesNo(Text) == IDNO ) {

				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = TEXT("Unable to open file for writing.");

			pWnd->Error(Text);

			return;
			}

		Export(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(TEXT("AB Tags"));
		}
	}

CString CABLogix5DeviceOptions::CreateName(void)
{
	return CreateName(GetDeviceName() + "_Tag%u");
	}

CString CABLogix5DeviceOptions::CreateName(PCTXT pFormat)
{
	for( UINT n = 1;; n++ ) {

		CPrintf Name(pFormat, n);

		INDEX Index = m_TagsFwdMap.FindName(Name);

		if( m_TagsFwdMap.Failed(Index) ) {
			
			return Name;
			}
		}
	}

UINT CABLogix5DeviceOptions::CreateIndex(void)
{
	if( m_AddrArray.GetCount() ) {

		UINT uIndex = m_AddrArray[0];

		m_AddrArray.Remove(0);

		return uIndex;
		}

	CTagDataL5Array List;

	ListTags(List);

	List.Sort();

	UINT uCreate = 1;

	UINT uCount = List.GetCount();

	for( UINT i = 0; i < uCount; i ++ ) {

		CTagDataL5 &Data = (CTagDataL5 &) List[i];

		UINT uIndex = GetIndex((CAddress &) Data.m_Addr);

		if( uCreate != uIndex ) {
			
			break;
			}

		uCreate ++;
		}

	return uCreate;
	}

void CABLogix5DeviceOptions::SaveTags(CTreeFile &Tree)
{
	CTagDataL5Array List;
	
	ListTags(List);

	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		Tree.PutValue(TEXT("Name"), List[n].m_Name);

		Tree.PutValue(TEXT("Data"), List[n].m_Addr);
		}
	}

void CABLogix5DeviceOptions::LoadTags(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Name" ) {
			
			CString Name = Tree.GetValueAsString();

			Tree.GetName();

			CAddress Addr;
			
			Addr.m_Ref = Tree.GetValueAsInteger();
			
			CreateTag( CError(FALSE), Name, Addr, FALSE);

			continue;
			}
		}
	}

CString CABLogix5DeviceOptions::GetTagName(CString const &Text)
{
	UINT uPos;

	if( (uPos = Text.Find(sepTableOpen)) < NOTHING ) {

		return Text.Left(uPos);
		}

	if( (uPos = Text.Find('.')) < NOTHING ) {

		return Text.Left(uPos);
		}

	return Text;
	}

UINT CABLogix5DeviceOptions::GetOffset(CAddress const &Addr, CString const &Text)
{
	UINT uOffset = 0;
	
	UINT uPos;

	if( (uPos = Text.Find(sepTableOpen)) < NOTHING) {

		UINT uEnd = Text.Find(sepTableClose);

		CString Offset = Text.Mid(uPos, uEnd - uPos);

		Offset.Remove(sepTableOpen);

		Offset.Remove(sepTableClose);
	
		uOffset = tatoi(Offset);
		}

	if( (uPos = Text.Find('.')) < NOTHING) {
		
		CStringArray List;

		GetTriplets(Addr, List);

		CString Triplet = Text.Mid(uPos + 1);

		for( UINT n = 0; n < List.GetCount(); n ++ ) {

			if( Triplet == List[n] ) {
				
				return (uOffset * 3) + n;
				}
			}
		}

	return uOffset;
	}

BOOL CABLogix5DeviceOptions::IsArray(CString const &Text)
{
	return Text.Find(sepTableOpen) < NOTHING;
	}

BOOL CABLogix5DeviceOptions::IsTable(CAddress const &Addr)
{
	return IsTable_m(Addr);
	}

BOOL CABLogix5DeviceOptions::IsArray(CAddress const &Addr)
{
	if( IsTriplet(Addr) ) {
		
		return (Addr.a.m_Extra & 0x08) ? TRUE : FALSE;
		}

	if( IsString(Addr) ) {

		return (Addr.a.m_Extra & 0x08) ? TRUE : FALSE;
		}

	return IsTable(Addr);
	}

BOOL CABLogix5DeviceOptions::IsTriplet(CAddress const &Addr)
{
	switch( Addr.a.m_Extra & 0x07 ) {
		
		case addrTimer:
		case addrCounter:
		case addrControl:
			return TRUE;
		}
	
	return FALSE;
	}

BOOL CABLogix5DeviceOptions::IsString(CAddress const &Addr)
{
	return (Addr.a.m_Extra & 0x07) == addrString;
	}

BOOL CABLogix5DeviceOptions::AllowArray(CAddress const &Addr)
{
	switch( Addr.a.m_Type ) {

		case addrBitAsBit:
			return FALSE;

		default:
			break;
		}

	return TRUE;
	}

CString CABLogix5DeviceOptions::FindDataTagType(CAddress const &Addr)
{
	CString Type;

	if( IsString(Addr) ) {
	
		Type = "String";

		return Type;
		}

	switch( Addr.a.m_Type ) {
		
		case addrBitAsBit:
			Type = "Flag";
			break;
		
		case addrRealAsReal:
			Type = "Real";
			break;

		default:
			Type = "Word";
			break;
		}		

	if( IsArray(Addr) ) {

		Type += "Array";
		}

	return Type;
	}

UINT CABLogix5DeviceOptions::GetIndex(CAddress const &Addr)
{
	return IsTable(Addr) ? Addr.a.m_Table : Addr.a.m_Offset;
	}

UINT CABLogix5DeviceOptions::GetOffset(CAddress const &Addr)
{
	return IsTable(Addr) ? Addr.a.m_Offset : Addr.a.m_Table;
	}

void CABLogix5DeviceOptions::GetTriplets(CAddress const &Addr, CStringArray &List)
{
	switch( Addr.a.m_Extra & 0x07 ) {
		
		case addrTimer:
		case addrCounter:
			List.Append("CTL");
			List.Append("PRE");
			List.Append("ACC");
			break;
		
		case addrControl:
			List.Append("CTL");
			List.Append("LEN");
			List.Append("POS");
			break;
		}
	}

// Import/Export Support

void CABLogix5DeviceOptions::Export(FILE *pFile)
{
	CTagDataL5Array List;

	ListTags(List);

	List.Sort();

	fprintf(pFile, TEXT("TYPE,SCOPE,NAME,DESCRIPTION,DATATYPE,SPECIFIER,ATTRIBUTES\n"));

	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		CString  Name = List[n].m_Name;

		CAddress Addr = (CAddress &) List[n].m_Addr;

		CString  Type;
		
		if( ExpandType(Type, Addr) ) {

			fprintf(pFile, TEXT("TAG,,%s,,%s,,\n"), PCTXT(Name), PCTXT(Type));
			}
		}
	}

BOOL CABLogix5DeviceOptions::Import(FILE *pFile)
{
	m_Errors.Empty();

	DWORD dwPos = ftell(pFile);

	for( UINT n = 0; n < 2; n ++ ) {

		UINT uLine = 0;

		BOOL fInit = TRUE;

		fseek(pFile, dwPos, SEEK_SET);

		while( !feof(pFile) ) {

			char sLine[256] = {0};

			fgets(sLine, sizeof(sLine), pFile);

			if( sLine[0] ) {

				uLine ++;

				CStringArray List;

				sLine[strlen(sLine)-1] = 0;

				CString(sLine).Tokenize(List, ',');

				if( fInit ) {

					if( List.GetCount() ) {

						if( List[0] == "TYPE" ) {
						
							fInit = FALSE;
							}
						}

					continue;
					}

				if( List.GetCount() > 4 ) {

					if( List[0] != "TAG" ) {
						
						continue;
						}

					CString Scope = List[1];

					Scope.Remove('"');

					if( !Scope.IsEmpty() ) {

						continue;
						}
					     
					CString Name = List[2];

					CString Type = List[4];

					Name.Remove('"');
					
					CAddress Addr;

					if( !ParseType(Addr, Type) ) {
						
						if( m_Errors.Find(uLine) == NOTHING ) {

							m_Errors.Append(uLine);
							}

						continue;
						}

					if( n == 0 && !IsTable(Addr) ) {

						continue;
						}

					if( n == 1 && IsTable(Addr) ) {
						
						continue;
						}

					if( !CreateTag(CError(FALSE), Name, Addr, TRUE) ) {
						
						if( m_Errors.Find(uLine) == NOTHING ) {

							m_Errors.Append(uLine);
							}
						}
					}
				}
			}
		}

	return m_Errors.GetCount() == 0;
	}

BOOL CABLogix5DeviceOptions::ParseType(CAddress &Addr, CString Text)
{
	Text.Remove('"');

	UINT    uPos = Text.Find('[');
	
	BOOL  fArray = (uPos < NOTHING);

	CString Type = Text.Left(uPos);

	if( Type == "REAL" ) {

		if( MakeReal(Addr) ) {

			goto Done;
			}
		}

	if( Type == "DINT" ) {

		if( MakeLong(Addr) ) {

			goto Done;
			}
		}

	if( Type == "INT" ) {

		if( MakeWord(Addr) ) {

			goto Done;
			}
		}

	if( Type == "SINT" ) {

		if( MakeByte(Addr) ) {

			goto Done;
			}
		}

	if( Type == "BOOL" ) {

		if( MakeBool(Addr) ) {

			goto Done;
			}
		}

/*	if( Type == "STRING" ) {

		if( MakeString(Addr) ) {

			goto Done;
			}
		}
*/
	if( Type == "TIMER" ) {

		if( MakeTimer(Addr) ) {

			goto Done;
			}
		}

	if( Type == "COUNTER" ) {

		if( MakeCounter(Addr) ) {

			goto Done;
			}
		}

	if( Type == "CONTROL" ) {

		if( MakeControl(Addr) ) {

			goto Done;
			}
		}

	return FALSE;

Done :

       if( fArray ) {

		if( !AllowArray(Addr) ) {

			return FALSE;
			}

		MakeArray(Addr);
		}

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::ExpandType(CString &Text, CAddress const &Addr)
{
	if( Addr.a.m_Extra ) {
	
		switch( Addr.a.m_Extra & 0x07 ) {
			
			case addrTimer:		Text = "TIMER";		break;	
			case addrCounter:	Text = "COUNTER";     	break;
			case addrControl:	Text = "CONTROL";	break;
			case addrString:	Text = "STRING";	break;
			}
		}
	else {
		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	Text = "BOOL";	break;
			//case addrBitAsByte:	return "";	break;
			//case addrBitAsWord:	return "";	break;
			//case addrBitAsLong:	return "";	break;
			//case addrBitAsReal:	return "";	break;
			case addrByteAsByte:	Text = "SINT";	break;
			//case addrByteAsWord:	return "";	break;
			//case addrByteAsLong:	return "";	break;
			//case addrByteAsReal:	return "";	break;
			case addrWordAsWord:	Text = "INT";	break;
			//case addrWordAsLong:	return "";	break;
			//case addrWordAsReal:	return "";	break;
			case addrLongAsLong:	Text = "DINT";	break;
			//case addrLongAsReal:	return "";	break;
			case addrRealAsReal:	Text = "REAL";	break;
			//case addrReserved:	return "";	break;
			default:		Text = "<type>";
				return FALSE;
			}
		}
		
	if( IsArray(Addr) ) {

		UINT uDim = 0;
		
		Text += CPrintf("[%d]", uDim);
		}

	return TRUE;
	}

void CABLogix5DeviceOptions::ShowErrors(CWnd *pWnd)
{
	if( m_Errors.GetCount() ) {
		
		afxThread->SetWaitMode(TRUE);
		
		CString Lines;

		UINT uErrMax = 101;
		
		for( UINT i = 0; i < m_Errors.GetCount(); ) {

			if( i )
				Lines += ", ";
			
			Lines += CPrintf("%d", m_Errors[i]);

			if( ++ i >= uErrMax ) {

				CString Text = "...\nFile contains more than %d errors";
				
				Lines += CPrintf(Text, uErrMax);

				break;
				}
			}

		afxThread->SetWaitMode(FALSE);

		CString Text = "The following lines in the file contained errors :\n\n%s."
			       "\n\nThe lines without errors were still imported.";

		pWnd->Error(CPrintf(Text, Lines));
		}
	else {
		CString Text = "The operation completed without error.";

		pWnd->Information(Text);
		}
	}

// Address Builders

void CABLogix5DeviceOptions::MakeArray(CAddress &Addr)
{
	switch( Addr.a.m_Extra & 0x07 ) {
		
		case addrTimer:
		case addrCounter:
		case addrControl:
		case addrString:
			Addr.a.m_Extra |= 0x08;
			break;

		default:
			Addr.a.m_Table  = 0;
			break;
		}
	}

BOOL CABLogix5DeviceOptions::MakeReal(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrRealAsReal;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeLong(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeWord(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrWordAsWord;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeByte(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrByteAsByte;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeBool(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrBitAsBit;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeTimer(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = 0;
	Addr.a.m_Extra  = addrTimer;
	Addr.a.m_Type   = addrLongAsLong;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeCounter(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = 0;
	Addr.a.m_Extra  = addrCounter;
	Addr.a.m_Type   = addrLongAsLong;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeControl(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = 0;
	Addr.a.m_Extra  = addrControl;
	Addr.a.m_Type   = addrLongAsLong;

	return TRUE;
	}

BOOL CABLogix5DeviceOptions::MakeString(CAddress &Addr)
{
	Addr.a.m_Offset = 0;
	Addr.a.m_Table  = 0;
	Addr.a.m_Extra  = addrString;
	Addr.a.m_Type   = addrByteAsByte;

	return TRUE;
	}

// Download Support

BOOL CABLogix5DeviceOptions::AddText(CInitData &Init, CString const &Text)
{
#if defined(C3_VERSION)

	UINT uLen = Text.GetLength() + 1;

	Init.AddWord(WORD(uLen));

	for( UINT n = 0; n < uLen; n ++) {

		Init.AddByte(BYTE(Text[n]));
		}

#else

		Init.AddText(Text);

#endif

	return TRUE;
	}

// Testing

void CABLogix5DeviceOptions::MakeTestTags(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options - Serial Master
//

// Dynamic Class

AfxImplementDynamicClass(CABLogix5SerialMasterDeviceOptions, CABLogix5DeviceOptions);

// Constructor

CABLogix5SerialMasterDeviceOptions::CABLogix5SerialMasterDeviceOptions(void)
{
	m_Device = 0;
	
	m_Dest	 = 1;

	m_Time1	 = 1000; 

	m_Time2	 = 500;
	}

// Download Support	     

BOOL CABLogix5SerialMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
	
	Init.AddByte(BYTE(m_Device));
	Init.AddByte(BYTE(m_Dest));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	
	CABLogix5DeviceOptions::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CABLogix5SerialMasterDeviceOptions::AddMetaData(void)
{
	CABLogix5DeviceOptions::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Dest);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	}

// Address Overidables

BOOL CABLogix5SerialMasterDeviceOptions::MakeBool(CAddress &Addr)
{
	return FALSE;
	}

BOOL CABLogix5SerialMasterDeviceOptions::MakeTimer  (CAddress &Addr)
{
	return FALSE;
	}

BOOL CABLogix5SerialMasterDeviceOptions::MakeCounter(CAddress &Addr)
{
	return FALSE;
	}

BOOL CABLogix5SerialMasterDeviceOptions::MakeControl(CAddress &Addr)
{
	return FALSE;
	}

BOOL CABLogix5SerialMasterDeviceOptions::MakeString (CAddress &Addr)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options - EIP Master
//

// Dynamic Class

AfxImplementDynamicClass(CABLogix5EIPMasterDeviceOptions, CABLogix5DeviceOptions);

// Constructor

CABLogix5EIPMasterDeviceOptions::CABLogix5EIPMasterDeviceOptions(void)
{
	m_Device  = 0;
	
	m_Address = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Routing = 1;

	m_Slot	  = 0;

	m_Time	  = 1000;
	}

// UI Management

void CABLogix5EIPMasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Routing" ) {

		pHost->EnableUI("Slot", m_Routing ? TRUE: FALSE);
		}

	CABLogix5DeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CABLogix5EIPMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddLong(DWORD(m_Address));
	Init.AddWord(WORD(m_Routing ? m_Slot : 0xFFFF));
	Init.AddWord(WORD(m_Time));
	
	CABLogix5DeviceOptions::MakeInitData(Init);
	
	return TRUE;
	}

// Meta Data Creation

void CABLogix5EIPMasterDeviceOptions::AddMetaData(void)
{
	CABLogix5DeviceOptions::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Address);
	Meta_AddInteger(Slot);
	Meta_AddInteger(Time);
	Meta_AddInteger(Routing);
	}

// End of File
