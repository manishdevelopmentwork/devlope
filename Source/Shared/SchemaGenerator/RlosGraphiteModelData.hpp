
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_RlosGraphiteModelData_HPP

#define INCLUDE_RlosGraphiteModelData_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RlosBaseModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RLOS Graphite Model Data
//

class CRlosGraphiteModelData : public CRlosBaseModelData
{
public:
	// Constructors
	CRlosGraphiteModelData(void);
	CRlosGraphiteModelData(CString const &Model);

	// IPxeModel
	void    METHOD MakeAppObjects(void);
	void    METHOD MakePxeObjects(void);
	BOOL    METHOD GetDispList(CArray<DWORD> &List);
	UINT    METHOD GetObjCount(char cTag);
	PCDWORD METHOD GetUsbPaths(char cTag);

protected:
	// Function Pointers
	BOOL(CRlosGraphiteModelData::*m_pfnGetDispList)(CArray<DWORD> &List);
	UINT(CRlosGraphiteModelData::*m_pfnGetObjCount)(char cTag);
	PCDWORD(CRlosGraphiteModelData::*m_pfnGetUsbPaths)(char cTag);

	// Implementation
	void BindModel(void);

	// Model Data
	BOOL    G07_GetDispList(CArray<DWORD> &List);
	UINT    G07_GetObjCount(char cTag);
	PCDWORD G07_GetUsbPaths(char cTag);
	BOOL    G09_GetDispList(CArray<DWORD> &List);
	UINT    G09_GetObjCount(char cTag);
	PCDWORD G09_GetUsbPaths(char cTag);
	BOOL    G10_GetDispList(CArray<DWORD> &List);
	BOOL    G11_GetDispList(CArray<DWORD> &List);
	UINT    G10_GetObjCount(char cTag);
	PCDWORD G10_GetUsbPaths(char cTag);
	BOOL    G12_GetDispList(CArray<DWORD> &List);
	UINT    G12_GetObjCount(char cTag);
	PCDWORD G12_GetUsbPaths(char cTag);
	BOOL    G15_GetDispList(CArray<DWORD> &List);
	UINT    G15_GetObjCount(char cTag);
	PCDWORD G15_GetUsbPaths(char cTag);
	BOOL    GCE_GetDispList(CArray<DWORD> &List);
	UINT    GCE_GetObjCount(char cTag);
	PCDWORD GCE_GetUsbPaths(char cTag);
	BOOL    GSR_GetDispList(CArray<DWORD> &List);
	UINT    GSR_GetObjCount(char cTag);
	PCDWORD GSR_GetUsbPaths(char cTag);
	BOOL    Gen_GetDispList(CArray<DWORD> &List);
	UINT    Gen_GetObjCount(char cTag);
	PCDWORD Gen_GetUsbPaths(char cTag);
};

// End of File

#endif
