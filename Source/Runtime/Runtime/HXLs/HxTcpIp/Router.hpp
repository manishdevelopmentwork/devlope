
//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Router_HPP

#define	INCLUDE_Router_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIpHeader;

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//
//

extern IPacketDriver * Create_Ethernet(CConfigEth const &Config);
extern IPacketDriver * Create_Ppp(CConfigPpp const &Config);
extern IPacketDriver * Create_Loopback(void);
extern IProtocol     * Create_ICMP(void);
extern IProtocol     * Create_UDP(void);
extern IProtocol     * Create_TCP(void);
extern IProtocol     * Create_Raw(void);
extern IQueue        * Create_Queue(void);

//////////////////////////////////////////////////////////////////////////
//
// IP Router
//

class CRouter :
	public IRouter,
	public INetUtilities,
	public IPacketSink,
	public IClientProcess,
	public IDiagProvider
{
	public:
		// Constructor
		CRouter(void);

		// Destructor
		~CRouter(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IRouter Configuration
		BOOL METHOD AddEthernet(CConfigEth const &Config);
		BOOL METHOD AddPpp(CConfigPpp const &Config);
		UINT METHOD AddInterface(IPacketDriver *pPacket, IPREF Ip, IPREF Mask, BOOL fSafe);
		UINT METHOD AddNullInterface(void);
		BOOL METHOD AddLoopback(void);
		void METHOD EnableRouting(BOOL fFarSide);

		// IRouter User APIs
		BOOL	  METHOD Open(void);
		BOOL	  METHOD Close(void);
		ISocket * METHOD CreateSocket(WORD ID);

		// IRouter Protocol Calls
		DWORD METHOD GetPseudoSum(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff);
		BOOL  METHOD Send(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff, IProtocol * pProt);
		BOOL  METHOD Send(UINT uFace, IPREF Gate, CBuffer *pBuff);
		BOOL  METHOD Queue(CIpAddr Dest, CIpAddr From, WORD Prot, CBuffer *pBuff);
		BOOL  METHOD Queue(UINT uFace, IPREF Gate, CBuffer *pBuff);
		void  METHOD SendReq(UINT uMask, BOOL fState);
		void  METHOD CopyOptions(ISocket *pSock, IPREF Ip);
		void  METHOD EnableMulticast(IPREF Ip, BOOL fEnable);

		// IRouter Interface Calls
		BOOL METHOD SetConfig(UINT uFace, IPREF Ip, IPREF Mask);
		BOOL METHOD GetConfig(UINT uFace, CIpAddr &Ip, CIpAddr &Mask, CMacAddr &MAC);

		// IRouter Route Management
		void METHOD SetGateway(UINT uFace, IPREF Ip);
		BOOL METHOD GetGateway(CIpAddr &Ip);
		void METHOD AddRoute(UINT uFace, IPREF Ip, IPREF Mask, IPREF Gate);
		void METHOD RemoveRoute(UINT uFace, IPREF Ip, IPREF Mask);
		void METHOD AddRoutes(UINT uFace);
		void METHOD RemoveRoutes(UINT uFace);

		// INetUtilities
		BOOL  METHOD SetInterfaceNames(CStringArray const &Names);
		BOOL  METHOD SetInterfaceDescs(CStringArray const &Descs);
		BOOL  METHOD SetInterfacePaths(CStringArray const &Paths);
		BOOL  METHOD SetInterfaceIds(CUIntArray const &Ids);
		UINT  METHOD GetInterfaceCount(void);
		UINT  METHOD FindInterface(CString const &Name);
		UINT  METHOD FindInterface(UINT Id);
		BOOL  METHOD HasInterfaceType(UINT uType);
		UINT  METHOD GetInterfaceType(UINT uFace);
		UINT  METHOD GetInterfaceOrdinal(UINT uFace);
		BOOL  METHOD GetInterfaceName(UINT uFace, CString &Name);
		BOOL  METHOD GetInterfaceDesc(UINT uFace, CString &Desc);
		BOOL  METHOD GetInterfacePath(UINT uFace, CString &Path);
		BOOL  METHOD GetInterfaceMac(UINT uFace, MACADDR &Mac);
		BOOL  METHOD GetInterfaceAddr(UINT uFace, IPADDR &Addr);
		BOOL  METHOD GetInterfaceMask(UINT uFace, IPADDR &Mask);
		BOOL  METHOD GetInterfaceGate(UINT uFace, IPADDR &Gate);
		BOOL  METHOD GetInterfaceStatus(UINT uFace, CString &Status);
		BOOL  METHOD IsInterfaceUp(UINT uFace);
		DWORD METHOD GetOption(BYTE bType);
		DWORD METHOD GetOption(BYTE bType, UINT uFace);
		BOOL  METHOD IsBroadcast(IPADDR const &IP);
		BOOL  METHOD GetBroadcast(IPADDR const &IP, IPADDR &Broad);
		BOOL  METHOD EnumBroadcast(UINT &uFace, IPADDR &Broad);
		BOOL  METHOD IsFastAddress(IPADDR const &IP);
		BOOL  METHOD IsSafeAddress(IPADDR const &IP);
		BOOL  METHOD GetDefaultGateway(IPADDR const &IP, IPADDR &Gate);
		UINT  METHOD Ping(IPADDR const &IP, UINT uTimeout);

		// IPacketSink
		void OnLinkActive(UINT uFace);
		void OnLinkDown(UINT uFace);
		void OnPacket(UINT uFace, CBuffer *pBuff);

		// IClientProcess
		BOOL METHOD TaskInit(UINT uTask);
		INT  METHOD TaskExec(UINT uTask);
		void METHOD TaskStop(UINT uTask);
		void METHOD TaskTerm(UINT uTask);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Instance Pointer
		static CRouter * m_pThis;

		// Protocol Data
		struct CProt
		{
			IProtocol     * m_pDriver;
			WORD		m_ID;
			UINT		m_uMask;
		};

		// Interface Data
		struct CFace
		{
			IPacketDriver * m_pPacket;
			CString         m_Name;
			CString		m_Desc;
			CString		m_Path;
			UINT		m_Id;
			CIpAddr		m_Ip;
			CIpAddr		m_Mask;
			BOOL		m_fSafe;
			BOOL            m_fUp;
		};

		// Route Data
		struct CRoute
		{
			UINT		m_uFace;
			CIpAddr		m_Ip;
			CIpAddr		m_Mask;
			CIpAddr		m_Gate;
			UINT		m_uTarg;
		};

		// Multi Data
		struct CMulti
		{
			CIpAddr		m_Ip;
			UINT		m_uRefs;
		};

		// Data Members
		ULONG	     m_uRefs;
		UINT	     m_uProv;
		HTHREAD      m_hThread;
		CIpAddr	     m_Gateway;
		CProt	     m_Prot[5];
		CFace	     m_Face[16];
		CRoute       m_Route[32];
		CMulti       m_Multi[16];
		IQueue     * m_pQueue;
		UINT	     m_uProts;
		UINT	     m_uFaces;
		UINT         m_uRoutes;
		UINT         m_uMultis;
		BOOL         m_fRouting;
		BOOL	     m_fFarSide;
		UINT	     m_uSend;
		IEvent     * m_pSend;
		IMutex     * m_pLock1;
		IMutex     * m_pLock2;

	protected:
		// ICMP Calls
		void SendUnreachable(CIpHeader *pIp, BYTE bCode);
		void SendTimeExceeded(CIpHeader *pIp, BYTE bCode);
		void SendHostRedirect(CIpHeader *pIp, IPREF Ip);

		// Packet Routing
		BOOL RoutePacket(UINT uFace, CBuffer *pBuff, CIpHeader *pIp);
		UINT FindRoute(CIpAddr &Dest, CIpAddr &From, CIpAddr &Gate);
		UINT FindRoute(IPREF Dest);
		UINT FindInterface(IPREF Dest);

		// Implementation
		void AddProtocols(void);
		BOOL AddProtocol(IProtocol *pProt, WORD ID);
		void OpenInterfaces(void);
		void OpenProtocols(void);
		void PollForSend(void);
		void PollProtocols(void);
		void PollDrivers(void);
		void AddHeader(CIpAddr From, CIpAddr Dest, WORD Prot, CBuffer * pBuff);
		void CheckRoutes(UINT uFace, BOOL fAdd);
		void UpdateMulticast(void);

		// Diagnostics
		BOOL DiagRegister(void);
		BOOL DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagConfig(IDiagOutput *pOut, IDiagCommand *pCmd);
		UINT DiagRoutes(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Instantiators
		static IUnknown * Create_SockTcp(PCTXT pName);
		static IUnknown * Create_SockUdp(PCTXT pName);
		static IUnknown * Create_SockRaw(PCTXT pName);
};

// End of File

#endif
