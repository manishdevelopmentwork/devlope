
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_COGNEXDATADRIVER_HPP
#define INCLUDE_COGNEXDATADRIVER_HPP

// Driver Table Definitions

// Commands
#define	GF	128 // Get File
#define	SE	241 // Set Event (Trigger Event)
#define OL	242 // Online Status

// User Strings
#define	TS0	64
#define	TS1	65
#define	TS2	66
#define	TS3	67
#define	TS4	68
#define	TS5	69
#define	TS6	70
#define	TS7	71
#define	TS8	72
#define	TS9	73

//////////////////////////////////////////////////////////////////////////
//
// Cognex Camera Data Driver Options
//

class CCognexDataDeviceOptions : public CUIItem
{
	public:
		
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCognexDataDeviceOptions(void);

		// Download support
		BOOL MakeInitData(CInitData &Init);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Config Data
		UINT		m_Device;
		UINT		m_IP;
		UINT		m_Port;
		UINT		m_Keep;
		UINT		m_Ping;
		UINT		m_Time1;
		UINT		m_Time2;
		UINT		m_Time3;
		CString		m_User;
		CString		m_Pass;

	protected:

		// Meta Data Creation
		void AddMetaData(void);

	};

class CCognexDataDriver : public CStdCommsDriver
{
	public:

		// Constructor
		CCognexDataDriver(void);

		// Destructor
		~CCognexDataDriver(void);

		// Binding Control
		UINT GetBinding(void);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		void AddSpaces(void);

		enum {
			cmdString  = 1,
			cmdInteger = 2
			};

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		
		//// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL IsString(const CSpace *pSpace);
		UINT IsCommand(const CSpace *pSpace);

	protected:

	};

class CCognexDataDialog : public CStdDialog
{
	public:
		// Runtime class
		AfxDeclareRuntimeClass();

		// Constructor
		CCognexDataDialog(CCognexDataDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:

		CCognexDataDriver	*m_pDriver;
		CAddress		*m_pAddr;
		BOOL			 m_fPart;
		CSpace			*m_pSpace;
		UINT			 m_uDevice;

		AfxDeclareMessageMap();

		// Notification Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnDblClk(UINT uID, CWnd &Wnd);
		void OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadList(void);
		void OnSelChange(UINT uID, CWnd &Wnd);
		void ShowAddress(void);
		void ShowDetails(void);
		void ClearDetails(void);
		void DoEnables(void);
		CString GetTypeText(void);

		// Address type helpers
		BOOL IsCell(const CSpace *pSpace);

	};
// End of file

#endif