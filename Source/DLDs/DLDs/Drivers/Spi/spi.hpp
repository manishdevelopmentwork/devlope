//////////////////////////////////////////////////////////////////////////
//
// SPI Receive Modes

enum {
	recvPoll = 1,
	recvAck  = 2,
	recvSel	 = 3,
	recvEOT	 = 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// SPI Master Driver
//

class CSpiDriver : public CMasterDriver {

	public:
		// Constructor
		CSpiDriver(void);

		// Destructor
		~CSpiDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDevID;
			BYTE m_bAddr;
							
			};

		// Data Members
		LPCTXT 		m_pHex;
		CContext *	m_pCtx;
		BYTE		m_bTx[256];
		BYTE		m_bRx[256];
		UINT		m_uPtr;
		CRC16		m_CRC;

		// Implementation

		void PollRequest(UINT uOffset);
		void SelectRequest(UINT uOffset);
		void SelectSequence(UINT uType, PDWORD pData, UINT uCount);
		void Begin(void);

		void AddByte(BYTE bByte, BOOL fData = FALSE);
		void AddWord(WORD wWord, BOOL fData = FALSE);
		void AddLong(DWORD dwWord, BOOL fData = FALSE);
		void AddToCheck(BYTE bByte, BOOL fData);
		UINT GetByteData(PDWORD pData, UINT uCount);
		UINT GetWordData(PDWORD pData, UINT uCount);
		UINT GetLongData(PDWORD pData, UINT uCount);
		
		BOOL Transact(UINT uMode);
		BOOL Send(void);
		BOOL Recv(UINT uMode);
		BOOL Check(void);
		BOOL IsValid(UINT uOffset);

	
	};

// End of File
