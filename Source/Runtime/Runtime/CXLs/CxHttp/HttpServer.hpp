

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServer_HPP

#define	INCLUDE_HttpServer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpServerConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CHttpServerSession;

class CHttpServerManager;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Auth Methods
//

enum {

	methodAnon = 0,
	methodForm = 1,
	methodHttp = 2,
};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server HTTP Methods
//

enum {

	httpBasic  = 1,
	httpDigest = 2,
	httpNTLM   = 4,
};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Options
//

class DLLAPI CHttpServerOptions : public CHttpServerConnectionOptions
{
public:
	// Constructor
	CHttpServerOptions(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Data Members
	UINT	m_uAuthMethod;
	UINT    m_uHttpMethod;
	CString m_Realm;
	UINT    m_uHttpRedirect;
	UINT    m_uSockCount;
	UINT    m_uIdleTimeout;
	UINT	m_uInitTimeout;
	UINT    m_uSessTimeout;
};

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server
//

class DLLAPI CHttpServer : public IDiagProvider
{
public:
	// Constructor
	CHttpServer(CHttpServerManager *pManager, CHttpServerOptions const &Opts);

	// Destructor
	~CHttpServer(void);

	// Attributes
	BOOL IsIdle(void) const;
	UINT GetSockCount(void) const;

	// Operations
	BOOL Service(void);
	BOOL AcceptPost(IHttpStreamWrite * &pStm, CHttpServerConnection *pCon, CHttpServerRequest *pReq, PCTXT pPath);
	void Disconnect(void);
	void Disconnect(UINT uSock);
	void Term(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDiagProvider
	UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

private:
	// No Copying
	CHttpServer(CHttpServer const &That);

protected:
	// Connection Info
	struct CConnectionInfo
	{
		// Constructor
		CConnectionInfo(void)
		{
			m_pCon      = NULL;
			m_pDefer    = NULL;
			m_fRedirect = FALSE;
			m_uTime     = 0;
		};

		// Data Members
		UINT			n;
		CHttpServerConnection * m_pCon;
		CHttpServerRequest    * m_pDefer;
		UINT			m_uDefer;
		BOOL			m_fRedirect;
		UINT			m_uReqs;
		UINT		        m_uTime;
	};

	// Options	
	CHttpServerOptions const &m_Opts;

	// Session Tree
	CTree <CHttpServerSession *> m_Tree;

	// Session List
	CHttpServerSession * m_pHead;
	CHttpServerSession * m_pTail;

	// Data Members
	ULONG		     m_uRefs;
	CHttpServerManager * m_pManager;
	CHttpServerAuth    * m_pMethod[3];
	CConnectionInfo	   * m_pInfo;
	UINT		     m_uPoll;
	CString		     m_SessionKey;

	// Overridables
	virtual CString FindReal(CString User);
	virtual CString FindPass(CString User);
	virtual BOOL    SkipAuth(CHttpServerRequest *pReq);
	virtual void    ServePage(CHttpServerSession *pSess, CConnectionInfo &Info, CHttpServerRequest *pReq);
	virtual BOOL    CatchPost(IHttpStreamWrite * &pStm, CHttpServerConnection *pCon, CHttpServerRequest *pReq, PCTXT pPath);
	virtual void    MakeSession(CHttpServerSession * &pSess);
	virtual void    KillSession(CHttpServerSession *pSess);
	virtual BOOL    UseForRedirect(UINT n, UINT &uPort, BOOL &fTls);

	// Implementation
	void    FindSession(CHttpServerSession * &pSess, CConnectionInfo &Info, CHttpServerRequest *pReq);
	void    FindSession(CHttpServerSession * &pSess, CHttpServerConnection *pCon, CHttpServerRequest *pReq);
	void    EndSession(CHttpServerSession *pFind, UINT uActive);
	void    DiscardAllSessions(void);
	void    DiscardOldSessions(void);
	CString AdjustPath(CString Path);

	// Diagnostics
	bool AddDiagCmds(void);
	UINT DiagConnections(IDiagOutput *pOut, IDiagCommand *pCmd);
	UINT DiagSessions(IDiagOutput *pOut, IDiagCommand *pCmd);
};

// End of File

#endif
