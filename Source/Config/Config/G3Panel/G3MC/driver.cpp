
#include "intern.hpp"

#include "legacy.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "NewCommsDeviceRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Rack Comms Driver
//

// Constructor

CRackDriver::CRackDriver(void)
{
	m_pModule = NULL;
	}

// Destructor

CRackDriver::~CRackDriver(void)
{
	}

// IBase

UINT CRackDriver::Release(void)
{
	delete this;

	return 0;
	}

// Driver Data

WORD CRackDriver::GetID(void)
{
	return 0xFE01;
	}

UINT CRackDriver::GetType(void)
{
	return driverMaster;
	}

CString CRackDriver::GetString(UINT ID)
{
	switch( ID ) {

		case stringManufacturer: return L"";
		case stringDriverName:	 return L"";
		case stringVersion:	 return L"";
		case stringShortName:	 return L"";
		case stringDevRoot:	 return L"Module";
		}

	return L"";
	}

UINT CRackDriver::GetFlags(void)
{
	return dflagNone;
	}

// Binding Control

UINT CRackDriver::GetBinding(void)
{
	return bindRack;
	}

void CRackDriver::GetBindInfo(CBindInfo &Info)
{
	}

// Configuration

CLASS CRackDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CRackDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CRackDeviceOptions);
	}

// Tag Import

BOOL CRackDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	return FALSE;
	}

// Address Management

BOOL CRackDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	if( FindModule(pConfig) ) {

		UINT uPos = Text.Find('.');

		if( uPos < NOTHING ) {

			CStringArray List;

			CString Obj  = Text.Left(uPos);

			CString Prop = Text.Mid (uPos+1);

			List.Append(Obj);

			m_pModule->GetObjectConv(Obj).Tokenize(List, L",");

			for( UINT i = 0; i < List.GetCount(); i ++ ) {

				UINT uIndex = m_pModule->FindObject(List[i]);

				if( uIndex != NOTHING ) {

					CObjectData ObjData;

					if( m_pModule->GetObjectData(uIndex, ObjData) ) {

						m_uObject = ObjData.ID & 0x0F;

						uIndex    = ObjData.pItem->FindCommsData(Prop);

						if( uIndex < NOTHING ) {

							CCommsData ComData;

							if( ObjData.pItem->GetCommsData(uIndex, ComData) ) {

								Addr.a.m_Type   = GetType(ComData.PropID);
								Addr.a.m_Offset = GetRest(ComData.PropID);
								Addr.a.m_Table  = addrNamed;
								Addr.a.m_Extra  = GetExtra(ObjData.ID);

								return TRUE;
								}
							}
						}
					}
				}

			Error.SetError(L"Invalid object name.");

			return FALSE;
			}
		}

	Error.SetError(L"Invalid address.");

	return FALSE;
	}

BOOL CRackDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem * pDrvCfg)
{
	return ParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CRackDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( FindModule(pConfig) ) {

		BYTE ObjID  = BYTE(Addr.a.m_Offset >> 11);

		WORD PropID = WORD(Addr.a.m_Offset &  0x7FF);

		BYTE Tag    = BYTE(ObjID | (Addr.a.m_Extra << 4));

		UINT uIndex = m_pModule->FindObject(Tag);

		if( uIndex < NOTHING ) {

			CObjectData ObjData;

			if( m_pModule->GetObjectData(uIndex, ObjData) ) {

				uIndex = ObjData.pItem->FindCommsData(PropID);

				if( uIndex < NOTHING ) {

					CCommsData ComData;

					if( ObjData.pItem->GetCommsData(uIndex, ComData) ) {

						Text  = ObjData.Name;

						Text += '.';

						Text += ComData.UserName;

						return TRUE;
						}
					}
				}
			}
		}

	Text.Printf(L"P%4.4X", Addr.a.m_Offset);

	return TRUE;
	}

BOOL CRackDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem * pDrvCfg)
{
	return ExpandAddress(Text, pConfig, Addr);
	}

BOOL CRackDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	if( FindModule(pConfig) ) {

		CRackAddrDialog Dlg(ThisObject, Addr, pConfig);
		
		return Dlg.Execute(CWnd::FromHandle(hWnd));
		}

	return FALSE;
	}

BOOL CRackDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem * pDrvCfg)
{
	return SelectAddress(hWnd, Addr, pConfig, fPart);
	}

BOOL CRackDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( FindModule(pConfig) ) {

		if( pRoot == NULL ) {

			CObjectData ObjData;

			if( m_pModule->GetObjectData(uItem, ObjData) ) {

				m_uObject = ObjData.ID & 0x0F;

				m_uExtra  = GetExtra(ObjData.ID);

				m_pItem   = ObjData.pItem;

				Data.m_Addr.m_Ref = 0;
				Data.m_fPart	  = FALSE;
				Data.m_Name	  = ObjData.Name;
				Data.m_uData      = 1;

				return TRUE;
				}
			}
		else {
			if( pRoot->m_uData == 1 ) {

				CString Name = m_pItem->GetGroupName(WORD(uItem+1));

				if( !Name.IsEmpty() ) {

					m_uIndex = 0;

					m_uGroup = uItem+1;

					Data.m_Addr.m_Ref = 0;
					Data.m_fPart	  = FALSE;
					Data.m_Name	  = Name;
					Data.m_uData      = 2;

					return TRUE;
					}
				}

			if( pRoot->m_uData == 2 ) {

				CCommsData ComData;

				while( m_pItem->GetCommsData(m_uIndex++, ComData) ) {

					if( ComData.Group == m_uGroup ) {

						if( ComData.Usage != usageWriteInit ) {

							Data.m_Name = ComData.UserName;

							Data.m_Addr.a.m_Table  = addrNamed;
							Data.m_Addr.a.m_Type   = GetType(ComData.PropID);
							Data.m_Addr.a.m_Offset = GetRest(ComData.PropID);
							Data.m_Addr.a.m_Extra  = m_uExtra;
							
							Data.m_fPart = FALSE;
							
							Data.m_fRead = (ComData.Usage == usageRead);
							
							Data.m_uData = 0;

							return TRUE;
							}
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CRackDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	if( FindModule(pConfig) ) {

		BYTE ObjID  = BYTE(Addr.a.m_Offset >> 11);

		WORD PropID = WORD(Addr.a.m_Offset &  0x7FF);

		BYTE Tag    = BYTE(ObjID | (Addr.a.m_Extra << 4));

		UINT uIndex = m_pModule->FindObject(Tag);

		if( uIndex < NOTHING ) {

			CObjectData ObjData;

			if( m_pModule->GetObjectData(uIndex, ObjData) ) {

				uIndex = ObjData.pItem->FindCommsData(PropID);

				if( uIndex < NOTHING ) {

					CCommsData ComData;

					if( ObjData.pItem->GetCommsData(uIndex, ComData) ) {

						if( ComData.Usage == usageRead ) {

							return TRUE;
							}
						}
					}
				}
			}
		}

	return FALSE;
	}

BOOL CRackDriver::IsMappingDisabled(void)
{
	return FALSE;
	}

BOOL CRackDriver::IsAddrNamed(CItem *pConfig, CAddress const &Addr)
{
	return TRUE;
	}

// Notifications

void CRackDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	}

void CRackDriver::NotifyInit(CItem * pConfig)
{
	}

// Module Location

BOOL CRackDriver::FindModule(CItem *pConfig)
{
	if( pConfig ) {

		CNewCommsDeviceRack *pDev = (CNewCommsDeviceRack *) pConfig->GetParent();

		m_pModule = pDev->m_pModule;

		return TRUE;
		}

	return FALSE;
	}

// Implementation

UINT CRackDriver::GetType(WORD PropID)
{
	switch( (HIBYTE(PropID) & 7) ) {

		case TYPE_BOOL:

			return addrBitAsBit;

		case TYPE_BYTE:
		case TYPE_WORD:
		case TYPE_LONG:
		case TYPE_REAL:

			return addrWordAsWord;

		case TYPE_INT32:
		case TYPE_UINT32:

			return addrLongAsLong;
		}

	return addrWordAsWord;
	}

UINT CRackDriver::GetRest(WORD PropID)
{
	return (PropID | (m_uObject<<11));
	}

UINT CRackDriver::GetExtra(BYTE ID)
{
	return ID >> 4;
	}

//////////////////////////////////////////////////////////////////////////
//
// Rack Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CRackDeviceOptions, CUIItem);

// Constructor

CRackDeviceOptions::CRackDeviceOptions(void)
{
	}

// Download Support

BOOL CRackDeviceOptions::MakeInitData(CInitData &Init)
{
	CNewCommsDeviceRack *pDev = (CNewCommsDeviceRack *) GetParent();

	CGenericModule      *pMod = pDev->m_pModule;

	return pMod->MakeInitData(Init);
	}

// Meta Data Creation

void CRackDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Rack Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CRackAddrDialog, CStdDialog);
		
// Constructor

CRackAddrDialog::CRackAddrDialog(CRackDriver &Driver, CAddress &Addr, CItem *pConfig)
{
	m_pDriver = &Driver;

	m_pModule = m_pDriver->m_pModule;

	m_pConfig = pConfig;
	
	m_pAddr   = &Addr;

	m_uGroup  = 0;

	SetName(L"RackAddrDlg");
	}

// Message Map

AfxMessageMap(CRackAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1201, LBN_DBLCLK,	OnDblClk      )
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnObjectChange)
	AfxDispatchNotify(1101, LBN_SELCHANGE,	OnGroupChange )
	AfxDispatchNotify(1201, LBN_SELCHANGE,	OnPropChange  )

	AfxMessageEnd(CRackAddrDialog)
	};

// Message Handlers

BOOL CRackAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetCaption();

	if( m_pAddr->m_Ref ) {

		m_ObjectID  = BYTE(m_pAddr->a.m_Offset >> 11) | BYTE(m_pAddr->a.m_Extra << 4);

		m_PropID    = WORD(m_pAddr->a.m_Offset & 0x7FF);

		UINT uIndex = m_pModule->FindObject(m_ObjectID);

		if( uIndex < NOTHING ) {

			CObjectData ObjData;

			if( m_pModule->GetObjectData(uIndex, ObjData) ) {

				uIndex = ObjData.pItem->FindCommsData(m_PropID);

				if( uIndex < NOTHING ) {

					CCommsData ComData;

					if( ObjData.pItem->GetCommsData(uIndex, ComData) ) {

						m_uGroup = ComData.Group;
						}
					}
				}
			}
		}

	LoadObjectList(TRUE);

	return TRUE;
	}

// Notification Handlers

void CRackAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CRackAddrDialog::OnObjectChange(UINT uID, CWnd &Wnd)
{
	LoadGroupList(FALSE);
	}

void CRackAddrDialog::OnGroupChange(UINT uID, CWnd &Wnd)
{
	LoadPropList(FALSE);
	}

void CRackAddrDialog::OnPropChange(UINT uID, CWnd &Wnd)
{
	}

// Command Handlers

BOOL CRackAddrDialog::OnOkay(UINT uID)
{
	CListBox &Obj  = (CListBox &) GetDlgItem(1001);
	
	CListBox &Prop = (CListBox &) GetDlgItem(1201);

	if( Prop.IsEnabled() ) {

		UINT uObj  = Obj.GetCurSel();
		
		UINT uProp = Prop.GetCurSel();

		if( uProp < NOTHING ) {

			CString Text;

			Text += Obj.GetText(uObj);

			Text += '.';

			Text += Prop.GetText(uProp);

			CError   Error(TRUE);

			CAddress Addr;
			
			if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

				*m_pAddr = Addr;

				EndDialog(TRUE);

				return TRUE;
				}

			Error.Show(ThisObject);

			return TRUE;
			}
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CRackAddrDialog::SetCaption(void)
{
	CNewCommsDeviceRack *pDevice = (CNewCommsDeviceRack *) m_pConfig->GetParent();

	CString   Model = pDevice->m_pModule->m_Model;

	CString    Name = pDevice->m_Name;

	C3OemStrings(Model);

	if( Model.IsEmpty() ) {

		CString Text;

		Text.Printf( IDS_F_FOR_F_MODULE,
			     GetWindowText(),
			     Name
			    );

		SetWindowText(Text);
		}
	else {
		CString Text;

		Text.Printf( IDS_F_FOR_F_MODULE_F,
			     GetWindowText(),
			     Name,
			     Model
			    );

		SetWindowText(Text);
		}
	}

void CRackAddrDialog::LoadObjectList(BOOL fInit)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	ListBox.AddString(IDS("None"), NOTHING);

	ListBox.SetCurSel(0);

	for( UINT uIndex = 0;; uIndex++ ) {

		CObjectData ObjData;

		if( m_pModule->GetObjectData(uIndex, ObjData) ) {

			if( m_pModule->HasUserData(uIndex) ) {

				ListBox.AddString(ObjData.Name, uIndex);

				if( fInit ) {

					if( ObjData.ID == m_ObjectID ) {

						ListBox.SelectData(uIndex);
						}
					}
				}

			continue;
			}

		break;
		}

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	LoadGroupList(fInit);
	}

void CRackAddrDialog::LoadGroupList(BOOL fInit)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1101);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	ListBox.EnableWindow(FALSE);

	ListBox.SetCurSel(0);

	UINT uObject = ((CListBox &) GetDlgItem(1001)).GetCurSelData();

	if( uObject < NOTHING ) {

		CObjectData ObjData;

		if( m_pModule->GetObjectData(uObject, ObjData) ) {

			for( UINT uIndex = 1;; uIndex++ ) {

				CString Name = ObjData.pItem->GetGroupName(WORD(uIndex));

				if( !Name.IsEmpty() ) {

					ListBox.AddString(Name, uIndex);

					if( fInit ) {

						if( uIndex == m_uGroup ) {

							ListBox.SelectData(uIndex);
							}
						}

					continue;
					}

				break;
				}

			ListBox.EnableWindow(TRUE);
			}
		}

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	GetDlgItem(1100).EnableWindow(ListBox.IsEnabled());

	LoadPropList(fInit);
	}

void CRackAddrDialog::LoadPropList(BOOL fInit)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1201);

	ListBox.SetRedraw(FALSE);
	
	ListBox.ResetContent();

	ListBox.EnableWindow(FALSE);

	ListBox.SetCurSel(0);

	UINT uObject = ((CListBox &) GetDlgItem(1001)).GetCurSelData();

	UINT uGroup  = ((CListBox &) GetDlgItem(1101)).GetCurSelData();

	if( uObject < NOTHING && uGroup < NOTHING ) {

		CObjectData ObjData;

		if( m_pModule->GetObjectData(uObject, ObjData) ) {

			for( UINT uIndex = 0;; uIndex++ ) {

				CCommsData ComData;

				if( ObjData.pItem->GetCommsData(uIndex, ComData) ) {

					if( ComData.Group == uGroup ) {

						if( ComData.Usage != usageWriteInit ) {

							ListBox.AddString(ComData.UserName, ComData.PropID);

							if( fInit ) {

								if( ComData.PropID == m_PropID ) {

									ListBox.SelectData(ComData.PropID);
									}
								}
							}
						}

					continue;
					}

				break;
				}

			ListBox.EnableWindow(TRUE);
			}
		}

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	GetDlgItem(1200).EnableWindow(ListBox.IsEnabled());

	OnPropChange(1201, ListBox);
	}

// End of File
