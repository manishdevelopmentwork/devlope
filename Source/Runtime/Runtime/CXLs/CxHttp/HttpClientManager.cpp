
#include "Intern.hpp"

#include "HttpClientManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpClientConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Client Manager
//

// Constructor

CHttpClientManager::CHttpClientManager(void)
{
	m_pClient = NULL;
}

// Destructor

CHttpClientManager::~CHttpClientManager(void)
{
	AfxRelease(m_pClient);
}

// Operations

BOOL CHttpClientManager::Open(void)
{
	if( m_pTls ) {

		m_pTls->CreateClientContext(m_pClient);

		if( m_pClient ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CHttpClientManager::LoadTrustedRoots(PCBYTE pRoot, UINT uRoot)
{
	if( m_pClient ) {

		if( m_pClient->LoadTrustedRoots(pRoot, uRoot) ) {

			return TRUE;
		}

		return FALSE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

BOOL CHttpClientManager::LoadClientCert(PCBYTE pCert, UINT uCert, PCBYTE pPriv, UINT uPriv, PCTXT pPass)
{
	if( m_pClient ) {

		if( m_pClient->LoadClientCert(pCert, uCert, pPriv, uPriv, pPass) ) {

			return TRUE;
		}

		return FALSE;
	}

	AfxAssert(FALSE);

	return FALSE;
}

CHttpClientConnection * CHttpClientManager::CreateConnection(CHttpClientConnectionOptions const &Opts)
{
	return New CHttpClientConnection(this, Opts);
}

ISocket	* CHttpClientManager::CreateStdSocket(void)
{
	ISocket *pSock = NULL;

	AfxNewObject("sock-tcp", ISocket, pSock);

	return pSock;
}

ISocket	* CHttpClientManager::CreateTlsSocket(PCTXT pName, UINT uCheck)
{
	return m_pClient ? m_pClient->CreateSocket(NULL, pName, uCheck) : NULL;
}

// End of File
