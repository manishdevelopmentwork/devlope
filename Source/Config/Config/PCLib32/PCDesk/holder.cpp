
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Gadget Holder Window
//

// Dynamic Class

AfxImplementDynamicClass(CGadgetHolderWnd, CWnd);

// Static Data

UINT CGadgetHolderWnd::timerCheck  = AllocTimerID();

// Constructor

CGadgetHolderWnd::CGadgetHolderWnd(void)
{
	m_uFlags    = 0;
	m_pParent   = NULL;
	m_pHead     = NULL;
	m_pTail     = NULL;
	m_pCurrent  = NULL;
	m_pToolTip  = New CToolTip;
	m_fToolInit = FALSE;
	m_fCapture  = FALSE;
	}

// Destructor

CGadgetHolderWnd::~CGadgetHolderWnd(void)
{
	CGadget *pScan = m_pHead;
	
	while( pScan ) {
	
		CGadget *pNext = pScan->m_pNext;
		
		delete pScan;
		
		pScan = pNext;
		}
	}
		
// Operations

void CGadgetHolderWnd::AddGadget(CGadget *pGadget)
{
	AfxValidateObject(pGadget);

	AfxAssert(!m_hWnd);
	
	AfxListAppend(m_pHead, m_pTail, pGadget, m_pNext, m_pPrev);
	}

void CGadgetHolderWnd::AddFromMenu(CMenu &Menu, BOOL fEnd)
{
	int     nCount = Menu.GetMenuItemCount();
	
	int     nSects = 1;

	CGadget *pLast = NULL;
	
	CGadget *pWork = NULL;
	
	for( int nPos = 0; nPos < nCount; nPos++ ) {
	
		if( Menu.GetMenuState(nPos, MF_BYPOSITION) & MF_POPUP ) {

			CStringArray List;

			Menu.GetMenuString(nPos, MF_BYPOSITION).Tokenize(List, '|');

			if( !List[0].IsEmpty() ) {

				if( List[3] == L"Any" ) {

					AddGadget(pLast = New CAnyMenuGadget( wcstol(List[0], NULL, 16),
									      Menu.GetSubMenu(nPos),
									      List[1],
									      List[2]
									      ));
					}
				else {
					AddGadget(pLast = New CMenuGadget( wcstol(List[0], NULL, 16),
									   Menu.GetSubMenu(nPos),
									   List[1],
									   List[2],
									   List[3].GetLength() > 0
									   ));
					}
				}
			else {
				if( !List[3].CompareC(L"Toggle") ) {

					AddGadget(pLast = New CToggleMenuGadget( Menu.GetSubMenu(nPos),
										 List[1],
										 List[2]
										 ));
					}

				if( !List[3].CompareC(L"Sticky") ) {

					AddGadget(pLast = New CStickyMenuGadget( Menu.GetSubMenu(nPos),
										 List[1],
										 List[2]
										 ));
					}

				if( !List[3].CompareC(L"Help") ) {

					AddGadget(pLast = New CHelpMenuGadget(   Menu.GetSubMenu(nPos),
										 List[1],
										 List[2]
										 ));
					}

				if( !List[3].CompareC(L"Select") ) {

					AddGadget(pLast = New CSelectMenuGadget( Menu.GetSubMenu(nPos),
										 List[1],
										 List[2]
										 ));
					}
				}

			AddGadget(pWork = New CSpacerGadget(1));

			pLast->SetRight(pLast);
			}
		else {
			UINT uID = Menu.GetMenuItemID(nPos);
		
			if( uID ) {

				CStringArray List;

				Menu.GetMenuString(nPos, MF_BYPOSITION).Tokenize(List, '|');

				AddGadget(pLast = New CButtonGadget( uID,
								     wcstol(List[0], NULL, 16),
								     List[1]
								     ));
				
				AddGadget(pWork = New CSpacerGadget(1));

				pLast->SetRight(pLast);
				}
			else {
				AddGadget(pWork = New CRidgeGadget);
				
				AddGadget(New CSpacerGadget(1));

				pWork->SetRight(pLast);
					
				nSects++;
				}
			}
		}
		
	if( fEnd ) {
	
		if( nSects > 1 || fEnd > 1 ) {

			AddGadget(New CRidgeGadget);
			}
		}
	}

void CGadgetHolderWnd::PollGadgets(void)
{
	CGadget *pScan = m_pHead;

	while( pScan ) {

		pScan->OnIdle((m_uFlags & barPrivate) ? m_pParent : NULL);

		pScan = pScan->m_pNext;
		}
	}

void CGadgetHolderWnd::SetSemiModal(void)
{
	ClearCurrent();

	m_pToolTip->Pop();
	}

// Gadget Location

CGadget * CGadgetHolderWnd::GetGadget(UINT uID) const
{
	CGadget *pScan = m_pHead;
	
	while( pScan ) {
	
		if( pScan->GetID() == uID ) {

			return pScan;
			}

		pScan = pScan->m_pNext;
		}
		
	return NULL;
	}	

// Message Map

AfxMessageMap(CGadgetHolderWnd, CWnd)
{
	AfxDispatchMessage(WM_CREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_RBUTTONDBLCLK)
	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	AfxDispatchMessage(WM_TIMER)

	AfxMessageEnd(CGadgetHolderWnd)
	};

// Message Handlers

UINT CGadgetHolderWnd::OnCreate(CREATESTRUCT &Create)
{
	m_pParent  = &GetParent();

	m_pMainWnd = (CMainWnd *) afxMainWnd;

	HWND hWnd  = m_pMainWnd->GetHandle();
	
	AfxAssert(m_pMainWnd->IsKindOf(AfxRuntimeClass(CMainWnd)));

	m_pToolTip->Create(hWnd, TTS_ALWAYSTIP | TTS_NOPREFIX);

	m_pToolTip->Activate(TRUE);

	CreateGadgets();

	return 0;
	}

void CGadgetHolderWnd::OnDestroy(void)
{
	// LATER -- Why do we have to comment this out? If
	// we don't then we get a crash when we shut down.

	/*m_pToolTip->DestroyWindow(TRUE);*/
	}

void CGadgetHolderWnd::OnRButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( !IsSemiModal() ) {

		if( FindCurrent(Pos) ) {
		
			m_pCurrent->OnMouseMenu(Pos);
			}
		}
	}

void CGadgetHolderWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	if( !IsSemiModal() ) {

		if( FindCurrent(Pos) ) {
		
			m_pCurrent->OnMouseMenu(Pos);
			}
		}
	}

void CGadgetHolderWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	if( !IsSemiModal() ) {

		if( FindCurrent(Pos) ) {
		
			if( m_pCurrent->OnMouseDblClk(Pos) ) {
			
				if( !IsKindOf(AfxRuntimeClass(CStatusWnd)) ) {
				
					m_pMainWnd->SetPrompt(m_pCurrent->GetID());
					}
			
				m_fCapture = TRUE;
			
				SetCapture();
				}
			}

		m_pToolTip->RelayEvent(m_MsgCtx.Msg);
		}
	}

void CGadgetHolderWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( !IsSemiModal() ) {

		if( FindCurrent(Pos) ) {
		
			if( m_pCurrent->OnMouseDown(Pos) ) {

				if( !IsKindOf(AfxRuntimeClass(CStatusWnd)) ) {
				
					m_pMainWnd->SetPrompt(m_pCurrent->GetID());
					}
			
				m_fCapture = TRUE;
			
				SetCapture();
				}
			}

		m_pToolTip->RelayEvent(m_MsgCtx.Msg);
		}
	}

void CGadgetHolderWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( !IsSemiModal() ) {

		if( m_fCapture ) {
		
			BOOL fHit = m_pCurrent->GetRect().PtInRect(Pos);
			
			m_pCurrent->OnMouseUp(Pos, fHit);
			
			m_fCapture = NULL;
			
			ReleaseCapture();

			if( !IsKindOf(AfxRuntimeClass(CStatusWnd)) ) {
				
				m_pMainWnd->SetPrompt(NOTHING);
				}
			
			if( !fHit ) {

				FindCurrent(Pos);
				}
			}

		m_pToolTip->RelayEvent(m_MsgCtx.Msg);
		}
	}

void CGadgetHolderWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( !IsSemiModal() ) {

		if( m_fCapture ) {
		
			BOOL fHit = m_pCurrent->GetRect().PtInRect(Pos);
			
			m_pCurrent->OnMouseMove(Pos, fHit);
			}
		else
			FindCurrent(Pos);

		m_pToolTip->RelayEvent(m_MsgCtx.Msg);
		}
	}
	
UINT CGadgetHolderWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	if( IsSemiModal() ) {

		CDialog *pSemi = m_pMainWnd->GetSemiModal();
		
		pSemi->ForceActive();

		return MA_NOACTIVATEANDEAT;
		}

	return AfxCallDefProc();
	}

void CGadgetHolderWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == timerCheck ) {
			
		if( !m_fCapture && m_pCurrent ) {
		
			if( !CheckForHide() ) {
	
				CPoint Pos = GetCursorPos();
					
				ScreenToClient(Pos);
					
				if( !m_pCurrent->GetRect().PtInRect(Pos) ) {

					ClearCurrent();
					}
				}
			}
		}
	}

// Implementation

BOOL CGadgetHolderWnd::CheckForHide(void)
{
	if( m_pMainWnd->IsMenuActive() ) {
	
		ClearCurrent();
			
		return TRUE;
		}
	
	return FALSE;                 
	}

BOOL CGadgetHolderWnd::FindCurrent(CPoint Pos)
{
	if( !CheckForHide() ) {
	
		if( m_pCurrent && !m_pCurrent->GetRect().PtInRect(Pos) ) {

			ClearCurrent();
			}
		
		for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {
			
			if( pScan->GetRect().PtInRect(Pos) ) {
				
				if( m_pCurrent != pScan ) {
					
					m_pCurrent = pScan;
						
					m_pCurrent->SetFlag(MF_HILITE);
						
					SetTimer(timerCheck, 10);
					}
	
				return TRUE;
				}
			}
		}
	
	return FALSE;
	}

void CGadgetHolderWnd::ClearCurrent(void)
{
	if( m_pCurrent ) {
		
		m_pCurrent->ClearFlag(MF_HILITE);
				
		m_pCurrent = NULL;
		
		KillTimer(timerCheck);
		}
	}

BOOL CGadgetHolderWnd::PaintGadgets(CPaintDC &DC)
{
	DC.Select(afxFont(Status));
	
	CRect    Paint  = DC.GetPaintRect();

	CRect    Client = GetClientRect();

	CGadget *pScan  = m_pHead;

	BOOL     fOkay  = TRUE;
		
	while( pScan ) {

		CRect Rect = pScan->GetRect();

		if( Client.Encloses(Rect) ) {

			if( Paint.Overlaps(Rect) ) {
			
				pScan->OnPaint(DC);
				}
			}
		else
			fOkay = FALSE;
			
		pScan = pScan->m_pNext;
		}

	DC.Deselect();

	return fOkay;
	}

void CGadgetHolderWnd::CreateGadgets(void)
{
	CClientDC DC(ThisObject);

	DC.Select(afxFont(Status));

	CGadget *pScan = m_pHead;
	
	while( pScan ) {
	
		pScan->OnCreate(DC);

		pScan = pScan->m_pNext;
		}

	DC.Deselect();
	}
		
void CGadgetHolderWnd::AddToolTips(void)
{
	CWnd *pWnd = (m_uFlags & barPrivate) ? m_pParent : NULL;

	for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {

		CString Tip;

		if( pScan->OnGetTip(pWnd, FALSE, Tip) ) {

			CToolInfo Info(m_hWnd, pScan->GetID(), pScan->GetRect(), Tip);

			m_pToolTip->AddTool(Info);
			}
		}

	m_pToolTip->SetTipBkColor(CColor(255, 255, 200));

	m_fToolInit = TRUE;
	}

void CGadgetHolderWnd::UpdateToolTips(void)
{
	if( m_fToolInit ) {

		CWnd *pWnd = (m_uFlags & barPrivate) ? m_pParent : NULL;

		for( CGadget *pScan = m_pHead; pScan; pScan = pScan->m_pNext ) {

			CString Tip;

			if( pScan->OnGetTip(pWnd, FALSE, Tip) ) {

				m_pToolTip->NewToolRect( m_hWnd,
							 pScan->GetID(),
							 pScan->GetRect()
							 );
				}
			}
		}
	}

BOOL CGadgetHolderWnd::IsSemiModal(void)
{
	if( m_pParent == m_pMainWnd ) {

		return m_pMainWnd->IsSemiModal();
		}

	return FALSE;
	}

// End of File
