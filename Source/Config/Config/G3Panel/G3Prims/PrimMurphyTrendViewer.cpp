
#include "intern.hpp"

#include "PrimMurphyTrendViewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Trend Viewer
//

// Dynamic Class

AfxImplementDynamicClass(CPrimMurphyTrendViewer, CPrimViewer);

// Constructor

CPrimMurphyTrendViewer::CPrimMurphyTrendViewer(void)
{
	m_uType         = 0x40;
	m_Width         = 4;
	m_pPenMask      = NULL;
	m_PenWeight     = 3;
	m_ShowData      = 0;
	m_ShowCursor    = 0;
	m_DataBox       = 0;
	m_FontTitle     = fontHei16Bold;
	m_FontData      = fontHei16;
	m_GridTime      = 0;
	m_GridMode      = 0;
	m_GridMajor     = 10;
	m_GridMinor     = 2;
	m_pGridMin      = NULL;
	m_pGridMax      = NULL;
	m_Precise       = 1;
	m_pColTitle     = New CPrimColor;
	m_pColLabel     = New CPrimColor;
	m_pColData      = New CPrimColor;
	m_pColMajor     = New CPrimColor;
	m_pColMinor     = New CPrimColor;
	m_pColCursor    = New CPrimColor;
	m_pColPen[0x0]  = New CPrimColor;
	m_pColPen[0x1]  = New CPrimColor;
	m_pColPen[0x2]  = New CPrimColor;
	m_pColPen[0x3]  = New CPrimColor;
	m_pColPen[0x4]  = New CPrimColor;
	m_pColPen[0x5]  = New CPrimColor;
	m_pColPen[0x6]  = New CPrimColor;
	m_pColPen[0x7]  = New CPrimColor;
	m_pColPen[0x8]  = New CPrimColor;
	m_pColPen[0x9]  = New CPrimColor;
	m_pColPen[0xA]  = New CPrimColor;
	m_pColPen[0xB]  = New CPrimColor;
	m_pColPen[0xC]  = New CPrimColor;
	m_pColPen[0xD]  = New CPrimColor;
	m_pColPen[0xE]  = New CPrimColor;
	m_pColPen[0xF]  = New CPrimColor;
	m_pBtnPgLeft    = NULL;
	m_pBtnLeft      = NULL;
	m_pBtnLive      = NULL;
	m_pBtnRight     = NULL;
	m_pBtnPgRight   = NULL;
	m_pBtnIn        = NULL;
	m_pBtnOut       = NULL;
	m_pBtnLoad      = NULL;
	m_pFormat       = New CDispFormatTimeDate;
	m_fUseLoad      = 1;	// !!! Set to 0 for final delivery
	m_pRoot         = NULL;
	m_pFileName     = NULL;
	m_pZoomMax      = NULL;
	m_pZoomMin      = NULL;
	m_pZoomInit     = NULL;
	m_pPerLane      = NULL;
	m_pTimeStart    = NULL;
	m_pTimeEnd      = NULL;
}

// Initial Values

void CPrimMurphyTrendViewer::SetInitValues(void)
{
	m_pBack->SetInitial(GetRGB(8, 8, 8));

	CPrimViewer::SetInitValues();

	m_pColTitle->SetInitial(GetRGB(31, 31, 31));
	m_pColLabel->SetInitial(GetRGB(31, 31, 31));
	m_pColData->SetInitial(GetRGB(31, 31, 31));
	m_pColMajor->SetInitial(GetRGB(16, 16, 16));
	m_pColMinor->SetInitial(GetRGB(0, 0, 0));
	m_pColCursor->SetInitial(GetRGB(31, 0, 0));

	m_pColPen[0x0]->SetInitial(GetRGB(31, 0, 0));
	m_pColPen[0x1]->SetInitial(GetRGB(0, 31, 0));
	m_pColPen[0x2]->SetInitial(GetRGB(31, 31, 0));
	m_pColPen[0x3]->SetInitial(GetRGB(0, 0, 31));
	m_pColPen[0x4]->SetInitial(GetRGB(31, 0, 31));
	m_pColPen[0x5]->SetInitial(GetRGB(0, 31, 31));
	m_pColPen[0x6]->SetInitial(GetRGB(16, 0, 0));
	m_pColPen[0x7]->SetInitial(GetRGB(0, 16, 0));

	// REV3 -- Find some better defaults!

	m_pColPen[0x8]->SetInitial(GetRGB(31, 0, 0));
	m_pColPen[0x9]->SetInitial(GetRGB(0, 31, 0));
	m_pColPen[0xA]->SetInitial(GetRGB(31, 31, 0));
	m_pColPen[0xB]->SetInitial(GetRGB(0, 0, 31));
	m_pColPen[0xC]->SetInitial(GetRGB(31, 0, 31));
	m_pColPen[0xD]->SetInitial(GetRGB(0, 31, 31));
	m_pColPen[0xE]->SetInitial(GetRGB(16, 0, 0));
	m_pColPen[0xF]->SetInitial(GetRGB(0, 16, 0));

	SetInitial(L"BtnPgLeft", m_pBtnPgLeft, L"\"<<\"");
	SetInitial(L"BtnLeft", m_pBtnLeft, L"\"<\"");
	SetInitial(L"BtnLive", m_pBtnLive, L"\"Live\"");
	SetInitial(L"BtnRight", m_pBtnRight, L"\">\"");
	SetInitial(L"BtnPgRight", m_pBtnPgRight, L"\">>\"");
	SetInitial(L"BtnIn", m_pBtnIn, L"\"In\"");
	SetInitial(L"BtnOut", m_pBtnOut, L"\"Out\"");
	SetInitial(L"BtnLoad", m_pBtnLoad, L"\"File\"");

	SetInitial(L"Root", m_pRoot, L"\"/LOGS/LOG1\"");
	SetInitial(L"ZoomMin", m_pZoomMin, 0);
	SetInitial(L"ZoomMax", m_pZoomMax, 20);
	SetInitial(L"ZoomInit", m_pZoomInit, 4);
	SetInitial(L"PerLane", m_pPerLane, 0);

	((CDispFormatTimeDate *) m_pFormat)->m_Mode = 3;
}

// Overridables

void CPrimMurphyTrendViewer::GetRefs(CPrimRefList &Refs)
{
	GetFontRef(Refs, m_FontTitle);

	GetFontRef(Refs, m_FontData);

	CPrimViewer::GetRefs(Refs);
}

void CPrimMurphyTrendViewer::EditRef(UINT uOld, UINT uNew)
{
	EditFontRef(m_FontTitle, uOld, uNew);

	EditFontRef(m_FontData, uOld, uNew);

	CPrimViewer::EditRef(uOld, uNew);
}

// UI Creation

BOOL CPrimMurphyTrendViewer::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIStdPage(IDS("Options"), AfxPointerClass(this), 1));
	pList->Append(New CUIStdPage(IDS("Format"), AfxPointerClass(this), 2));
	pList->Append(New CUIStdPage(IDS("Buttons"), AfxPointerClass(this), 3));
	pList->Append(New CUIStdPage(IDS("Time"), AfxPointerClass(this), 4));
	pList->Append(New CUIStdPage(IDS("Pens"), AfxPointerClass(this), 5));
	pList->Append(New CUIStdPage(IDS("Advanced"), AfxPointerClass(this), 8));
	pList->Append(New CUIStdPage(IDS("Show"), AfxPointerClass(this), 7));

	return TRUE;
}

// UI Update

void CPrimMurphyTrendViewer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag.Left(4) == L"Show" || Tag == L"GridMode" ) {

		DoEnables(pHost);
	}

	if( Tag == L"Source" ) {

		DoEnables(pHost);
	}

	CPrimViewer::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CPrimMurphyTrendViewer::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(3) == L"Btn" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"GridMin" || Tag == L"GridMax" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"FileName" || Tag == L"Root" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag.StartsWith(L"Zoom") ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag.StartsWith(L"Time") ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
	}

	if( Tag == L"PerLane" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
	}

	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Download Support

BOOL CPrimMurphyTrendViewer::MakeInitData(CInitData &Init)
{
	CPrimViewer::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pRoot);
	Init.AddItem(itemVirtual, m_pFileName);
	Init.AddItem(itemVirtual, m_pZoomMin);
	Init.AddItem(itemVirtual, m_pZoomMax);
	Init.AddItem(itemVirtual, m_pZoomInit);
	Init.AddItem(itemVirtual, m_pPerLane);
	Init.AddItem(itemVirtual, m_pTimeStart);
	Init.AddItem(itemVirtual, m_pTimeEnd);

	Init.AddByte(BYTE(m_Width));

	Init.AddItem(itemVirtual, m_pPenMask);

	Init.AddByte(BYTE(m_PenWeight));
	Init.AddByte(BYTE(m_ShowData));
	Init.AddByte(BYTE(m_ShowCursor));
	Init.AddByte(BYTE(m_DataBox));

	Init.AddWord(WORD(m_FontTitle));
	Init.AddWord(WORD(m_FontData));

	Init.AddByte(BYTE(m_GridTime));
	Init.AddByte(BYTE(m_GridMode));
	Init.AddByte(BYTE(m_GridMajor));
	Init.AddByte(BYTE(m_GridMinor));

	Init.AddItem(itemVirtual, m_pGridMin);
	Init.AddItem(itemVirtual, m_pGridMax);

	Init.AddByte(BYTE(m_Precise));

	Init.AddItem(itemSimple, m_pColTitle);
	Init.AddItem(itemSimple, m_pColLabel);
	Init.AddItem(itemSimple, m_pColData);
	Init.AddItem(itemSimple, m_pColMajor);
	Init.AddItem(itemSimple, m_pColMinor);
	Init.AddItem(itemSimple, m_pColCursor);

	Init.AddItem(itemSimple, m_pColPen[0x0]);
	Init.AddItem(itemSimple, m_pColPen[0x1]);
	Init.AddItem(itemSimple, m_pColPen[0x2]);
	Init.AddItem(itemSimple, m_pColPen[0x3]);
	Init.AddItem(itemSimple, m_pColPen[0x4]);
	Init.AddItem(itemSimple, m_pColPen[0x5]);
	Init.AddItem(itemSimple, m_pColPen[0x6]);
	Init.AddItem(itemSimple, m_pColPen[0x7]);
	Init.AddItem(itemSimple, m_pColPen[0x8]);
	Init.AddItem(itemSimple, m_pColPen[0x9]);
	Init.AddItem(itemSimple, m_pColPen[0xA]);
	Init.AddItem(itemSimple, m_pColPen[0xB]);
	Init.AddItem(itemSimple, m_pColPen[0xC]);
	Init.AddItem(itemSimple, m_pColPen[0xD]);
	Init.AddItem(itemSimple, m_pColPen[0xE]);
	Init.AddItem(itemSimple, m_pColPen[0xF]);

	Init.AddItem(itemVirtual, m_pBtnPgLeft);
	Init.AddItem(itemVirtual, m_pBtnLeft);
	Init.AddItem(itemVirtual, m_pBtnLive);
	Init.AddItem(itemVirtual, m_pBtnRight);
	Init.AddItem(itemVirtual, m_pBtnPgRight);
	Init.AddItem(itemVirtual, m_pBtnIn);
	Init.AddItem(itemVirtual, m_pBtnOut);
	Init.AddItem(itemVirtual, m_pBtnLoad);

	Init.AddItem(itemSimple, m_pFormat);

	return TRUE;
}

// Meta Data

void CPrimMurphyTrendViewer::AddMetaData(void)
{
	CPrimViewer::AddMetaData();

	Meta_AddInteger(Width);
	Meta_AddVirtual(PenMask);
	Meta_AddInteger(PenWeight);
	Meta_AddInteger(ShowData);
	Meta_AddInteger(ShowCursor);
	Meta_AddInteger(DataBox);
	Meta_AddInteger(FontTitle);
	Meta_AddInteger(FontData);
	Meta_AddInteger(GridTime);
	Meta_AddInteger(GridMode);
	Meta_AddInteger(GridMajor);
	Meta_AddInteger(GridMinor);
	Meta_AddVirtual(GridMin);
	Meta_AddVirtual(GridMax);
	Meta_AddInteger(Precise);
	Meta_AddObject(ColTitle);
	Meta_AddObject(ColLabel);
	Meta_AddObject(ColData);
	Meta_AddObject(ColMajor);
	Meta_AddObject(ColMinor);
	Meta_AddObject(ColCursor);

	Meta_Add(L"ColPen0", m_pColPen[0x0], metaObject);
	Meta_Add(L"ColPen1", m_pColPen[0x1], metaObject);
	Meta_Add(L"ColPen2", m_pColPen[0x2], metaObject);
	Meta_Add(L"ColPen3", m_pColPen[0x3], metaObject);
	Meta_Add(L"ColPen4", m_pColPen[0x4], metaObject);
	Meta_Add(L"ColPen5", m_pColPen[0x5], metaObject);
	Meta_Add(L"ColPen6", m_pColPen[0x6], metaObject);
	Meta_Add(L"ColPen7", m_pColPen[0x7], metaObject);
	Meta_Add(L"ColPen8", m_pColPen[0x8], metaObject);
	Meta_Add(L"ColPen9", m_pColPen[0x9], metaObject);
	Meta_Add(L"ColPenA", m_pColPen[0xA], metaObject);
	Meta_Add(L"ColPenB", m_pColPen[0xB], metaObject);
	Meta_Add(L"ColPenC", m_pColPen[0xC], metaObject);
	Meta_Add(L"ColPenD", m_pColPen[0xD], metaObject);
	Meta_Add(L"ColPenE", m_pColPen[0xE], metaObject);
	Meta_Add(L"ColPenF", m_pColPen[0xF], metaObject);

	Meta_AddVirtual(BtnPgLeft);
	Meta_AddVirtual(BtnLeft);
	Meta_AddVirtual(BtnLive);
	Meta_AddVirtual(BtnRight);
	Meta_AddVirtual(BtnPgRight);
	Meta_AddVirtual(BtnIn);
	Meta_AddVirtual(BtnOut);
	Meta_AddVirtual(BtnLoad);
	Meta_AddObject(Format);

	Meta_AddVirtual(Root);
	Meta_AddVirtual(FileName);
	Meta_AddVirtual(ZoomMax);
	Meta_AddVirtual(ZoomMin);
	Meta_AddVirtual(ZoomInit);
	Meta_AddVirtual(PerLane);
	Meta_AddVirtual(TimeStart);
	Meta_AddVirtual(TimeEnd);

	Meta_SetName(IDS("FW Murphy Trend Viewer"));
}

// Overridables

BOOL CPrimMurphyTrendViewer::MakeList(void)
{
	m_List.Empty();

	m_List.Append(m_pBtnPgLeft->GetText());

	m_List.Append(m_pBtnLeft->GetText());

	m_List.Append(m_pBtnLive->GetText());

	m_List.Append(m_pBtnRight->GetText());

	m_List.Append(m_pBtnPgRight->GetText());

	m_List.Append(m_pBtnIn->GetText());

	m_List.Append(m_pBtnOut->GetText());

	if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

		if( m_fUseLoad ) {

			m_List.Append(m_pBtnLoad->GetText());
		}
	}

	m_Work = "MODIFIED TREND VIEWER";

	return TRUE;
}

// Implementation

void CPrimMurphyTrendViewer::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("GridMajor", m_GridMode == 1 || m_GridMode == 2);

	pHost->EnableUI("GridMinor", m_GridMode == 2);

	pHost->EnableUI("GridMin", m_GridMode == 3);

	pHost->EnableUI("GridMax", m_GridMode == 3);

	pHost->EnableUI("Precise", m_GridMode == 3);

	pHost->EnableUI("ColMajor", m_GridMode >= 1 || m_GridTime >= 1);

	pHost->EnableUI("ColMinor", m_GridMode == 2 || m_GridTime >= 1);

	pHost->EnableUI("ColData", m_ShowData);

	pHost->EnableUI("ColCursor", m_ShowCursor);

	pHost->EnableUI("FontData", m_ShowData || m_ShowCursor);

	pHost->EnableUI("DataBox", m_ShowData || m_ShowCursor);
}

// End of File
