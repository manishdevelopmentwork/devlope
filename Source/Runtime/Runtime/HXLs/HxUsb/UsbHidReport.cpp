
#include "Intern.hpp"

#include "UsbHidReport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hid Report
//

// Constructor

CUsbHidReport::CUsbHidReport(void)
{
	m_pData = NULL;

	m_uSize = 0;
	}

CUsbHidReport::CUsbHidReport(PCBYTE pData, UINT uSize)
{
	Attach(pData, uSize);
	}

// Operations

void CUsbHidReport::Attach(PCBYTE pData, UINT uSize)
{
	m_pData = pData;

	m_uSize = uSize;
	}

// Enumerators

UINT CUsbHidReport::GetFirst(void) const
{
	return 0;
	}

bool CUsbHidReport::GetNext(UINT &iIndex) const
{
	if( !Failed(iIndex) ) {

		HidItem const *p = operator[](iIndex);

		if( p->IsItemShort() ) {

			iIndex += 1;

			iIndex += p->m_bSize < 3 ? p->m_bSize : 4;

			return !Failed(iIndex);
			}
			
		iIndex += 3;

		iIndex += ((HidLong *) p)->m_bDataSize;

		return !Failed(iIndex);
		}

	return FALSE;
	}

// Operators

HidItem const * CUsbHidReport::operator[] (UINT iIndex) const
{
	return iIndex < m_uSize ? (HidItem *) m_pData + iIndex : NULL;
	}

// Failure Checks

bool CUsbHidReport::Failed(UINT iIndex) const
{
	return iIndex >= m_uSize;
	}

// End of File
