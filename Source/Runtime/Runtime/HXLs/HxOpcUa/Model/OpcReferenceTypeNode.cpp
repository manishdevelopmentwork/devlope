
#include "Intern.hpp"

#include "OpcReferenceTypeNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Reference Type Node
//

// Constructors

COpcReferenceTypeNode::COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, UINT Value, bool fAbstract, bool fSymmetric) : COpcNode(pModel, classReferenceType, Namespace, Value)
{
	m_fAbstract  = fAbstract;

	m_fSymmetric = fSymmetric;
	}

COpcReferenceTypeNode::COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, bool fAbstract, bool fSymmetric) : COpcNode(pModel, classReferenceType, Namespace, Value)
{
	m_fAbstract  = fAbstract;

	m_fSymmetric = fSymmetric;
	}

COpcReferenceTypeNode::COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, bool fAbstract, bool fSymmetric) : COpcNode(pModel, classReferenceType, Namespace, Value)
{
	m_fAbstract  = fAbstract;

	m_fSymmetric = fSymmetric;
	}

COpcReferenceTypeNode::COpcReferenceTypeNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, bool fAbstract, bool fSymmetric) : COpcNode(pModel, classReferenceType, Namespace, Value)
{
	m_fAbstract  = fAbstract;

	m_fSymmetric = fSymmetric;
	}

COpcReferenceTypeNode::COpcReferenceTypeNode(COpcReferenceTypeNode const &That) : COpcNode(That)
{
	m_fAbstract  = That.m_fAbstract;

	m_fSymmetric = That.m_fSymmetric;
	}

// Assignment

COpcReferenceTypeNode COpcReferenceTypeNode::operator = (COpcReferenceTypeNode const &That)
{
	COpcNode::operator = (That);

	m_fAbstract  = That.m_fAbstract;

	m_fSymmetric = That.m_fSymmetric;

	return ThisObject;
	}

// Operations

void COpcReferenceTypeNode::FindInverses(void)
{
	if( NeedInverse() ) {

		if( m_BrowseName.StartsWith("Has") ) {

			m_BrowseInverse = m_BrowseName.Mid(3) + "Of";
			}
		else {
			if( m_BrowseName.EndsWith("s") ) {

				m_BrowseInverse = m_BrowseName.Left(m_BrowseName.GetLength() - 1) + "dBy";
				}
			}

		m_DisplayInverse = m_BrowseInverse;
		}
	}

// End of File
