
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_Lexicon_HPP

#define INCLUDE_Lexicon_HPP

//////////////////////////////////////////////////////////////////////////
//
// Web Translation Thread
//

class CLexicon
{
	public:
		// Constructor
		CLexicon(void);

		// Destructor
		~CLexicon(void);

		// Attributes
		BOOL    IsValid(void) const;
		CString GetFrom(void) const;
		BOOL    Lookup(CString const &Text) const;
		CString Lookup(CString const &Text, CString const &To) const;
		CString Translate(CString Text, CString const &To) const;

		// Operations
		BOOL Open(CError &Error, CFilename const &Name, CString const &From);
		void Close(void);

	protected:
		// Data Members
		BOOL                                m_fValid;
		CString				    m_From;
		CMap <CString, UINT>		    m_Cols;
		CMap <CCaseString, CStringArray * > m_Data;

		// Implementation
		void StripQuotes(CStringArray &List);
	};

// End of File

#endif
