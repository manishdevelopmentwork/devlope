
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMentor2MasterDriver;

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define FRAME_TIMEOUT	500 

//////////////////////////////////////////////////////////////////////////
//
// Definitions
//

typedef signed int	C2INT;

//////////////////////////////////////////////////////////////////////////
//
// Control Techniques Mentor II Serial Driver
//

class CMentor2MasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CMentor2MasterDriver(void);

		// Destructor
		~CMentor2MasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			
			};

		
		CContext * m_pCtx;

		// Data Members
		LPCTXT 	m_pHex;
		BYTE	m_bRx[32];
		BYTE	m_bTx[32];
		UINT	m_uPtr;
		BYTE	m_bCheck;
		BOOL	m_fWide;

		// Implementation
		CCODE DoRead(AREF Addr, PDWORD pData);
		void PutRead(UINT uAddr);

		CCODE DoWrite(AREF Addr, PDWORD pData);
		void PutWrite(UINT uAddr, PDWORD pData);

		void StartFrame(void);
		void AddByte(BYTE bData);

		void Send(void);
		BOOL GetFrame(void);

		void GetDataFormat(UINT uOffset);
		UINT GetDataLength(UINT uOffset);
		DWORD GetGeneric(UINT uRadix, UINT uLength, UINT uOffset);

		void PutData(DWORD dData);
		void PutGeneric(DWORD dData, UINT uRadix, UINT uFactor);

		DWORD Abs(DWORD dData);
		BOOL Neg(DWORD dData);
		
	};

// End of File
