
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextChangeLog_HPP

#define INCLUDE_UITextChangeLog_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Change Logging Mode
//

class CUITextChangeLog : public CUITextEnumPick
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextChangeLog(void);

	protected:
		// Overridables
		void OnBind(void);
	};

// End of File

#endif
