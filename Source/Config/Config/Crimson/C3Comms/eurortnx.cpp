
#include "intern.hpp"

#include "eurortnx.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEurortnxDeviceOptions, CUIItem);

// Constructor

CEurortnxDeviceOptions::CEurortnxDeviceOptions(void)
{
	m_Drop		= 12;
	}

// UI Managament

void CEurortnxDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CEurortnxDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop >> 8 ) );
	Init.AddByte(BYTE(m_Drop &  0xFF) );

	return TRUE;
	}

// Meta Data Creation

void CEurortnxDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Driver
//

// Instantiator

ICommsDriver *	Create_EurortnxDriver(void)
{
	return New CEurortnxDriver;
	}

// Constructor

CEurortnxDriver::CEurortnxDriver(void)
{
	m_wID		= 0x400C;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SSD Drives";
	
	m_DriverName	= "890 - RTNX";
	
	m_Version	= "1.10";
	
	m_ShortName	= "SSD Drives 890 - RTNX";

	AddSpaces();  
	}

// Binding Control

UINT CEurortnxDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEurortnxDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 57600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEurortnxDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEurortnxDeviceOptions);
	}

// Address Management

BOOL CEurortnxDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CEurortnxAddrDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CEurortnxDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( !pSpace )

		return FALSE;

	if( pSpace->m_uTable == 1 ) {

		Addr.a.m_Table  = 1;
		Addr.a.m_Offset = 0;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Type   = addrWordAsWord;

		return TRUE;
		}

	CString sT = Text;

	sT.MakeUpper();

	UINT uFindReadSlot = sT.Find('R');

	UINT uFindWriteSlot = sT.Find('W');

	UINT uR = 0;

	UINT uW = 0;

	if( uFindReadSlot < NOTHING && uFindWriteSlot < NOTHING ) {

		if( uFindReadSlot < uFindWriteSlot ) {

			uR = tatoi(sT.Mid(uFindReadSlot+1, uFindWriteSlot-uFindReadSlot-1) );

			uW = tatoi(sT.Mid(uFindWriteSlot+1) );
			}

		else {

			uR = tatoi(sT.Mid(uFindReadSlot+1) );

			uW = tatoi(sT.Mid(uFindWriteSlot+1, uFindReadSlot-uFindWriteSlot-1) );
			}
		}

	else {

		if( uFindReadSlot < NOTHING ) {

			uR = tatoi(sT.Mid(uFindReadSlot+1));
			}

		if( uFindWriteSlot < NOTHING ) {

			uW = tatoi(sT.Mid(uFindWriteSlot+1));
			}
		}

	if( uR > 4095 || uW > 4095 ) {

		Error.Set( "ID <= 4095",
				0
			);

		return FALSE;
		}

	switch( pSpace->m_Prefix[0] ) {

		case 'B':
			Addr.a.m_Type = addrBitAsBit;
			break;

		case 'I':
			Addr.a.m_Type = addrLongAsLong;
			break;

		case 'R':
			Addr.a.m_Type = addrRealAsReal;
			break;
		}

	Addr.a.m_Offset = LOBYTE(uR) + ( LOBYTE(uW) << 8 );

	Addr.a.m_Extra  = (uR & 0xF00)>>8;

	Addr.a.m_Table  = 0xF0 | ( (uW & 0xF00)>>8 );

	return TRUE;
	}

BOOL CEurortnxDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace;

	if( Addr.a.m_Table == 1 ) {

		Text.Printf( "TO" );
		return TRUE;
		}

	switch( Addr.a.m_Type ) {

		case addrLongAsLong:
			pSpace = GetSpace( "INT" );
			break;

		case addrBitAsBit:
			pSpace = GetSpace( "BIT" );
			break;

		case addrRealAsReal:
		default:
			pSpace = GetSpace( "RL" );
			break;
		}

	if( pSpace ) {

		UINT uTable  = Addr.a.m_Table;

		UINT uOffset = Addr.a.m_Offset;

		UINT uExtra  = Addr.a.m_Extra;
				
		Text.Printf( "%s_R%d_W%d",
			pSpace->m_Prefix,
			LOBYTE(uOffset) + (uExtra << 8),
			HIBYTE(uOffset) + ( (uTable & 0xF) << 8)
			);
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CEurortnxDriver::AddSpaces(void)
{
	AddSpace(New CSpace(addrNamed, "RL",   "Real (Value) Parameter ID",	10, 0, 4095, addrRealAsReal));
	AddSpace(New CSpace(addrNamed, "INT",  "Integer (Ordinal) Parameter ID",10, 0, 4095, addrLongAsLong));
//	AddSpace(New CSpace(addrNamed, "BIT",  "Bit (Boolean) Parameter ID",	10, 0, 4095, addrBitAsBit));
	AddSpace(New CSpace(1,         "TO",   "Comm Timeout Adjust (500-3000)",10, 0, 0,    addrWordAsWord));
	}

// Helpers

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm RTNX Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CEurortnxAddrDialog, CStdAddrDialog);
		
// Constructor

CEurortnxAddrDialog::CEurortnxAddrDialog(CEurortnxDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "EurortnxElementDlg";
	}

AfxMessageMap(CEurortnxAddrDialog, CStdAddrDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSpaceChange)
	};

// Overridables

BOOL CEurortnxAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadElementUI();

	CAddress &Addr = (CAddress &) *m_pAddr;

	CString c = "None";

	if( Addr.a.m_Table >= 0xF0 ) {

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:
				c = "BIT";
				break;

			case addrLongAsLong:
				c = "INT";
				break;

			case addrRealAsReal:
				c = "RL";
				break;
			}
		}

	else {
		if( Addr.a.m_Table == 1 ) c = "TO";
		}
	
	if( !m_fPart ) {

		SetCaption();

		m_pSpace = m_pDriver->GetSpace(c);

		LoadList();

		if( m_pSpace ) {

			ShowAddress(Addr);

			SetAddressFocus();

			return FALSE;
			}

		GetDlgItem(2002).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);

		return TRUE;
		}
	else {
		
		m_pSpace = m_pDriver->GetSpace(c);

		LoadType();
		
		ShowAddress(Addr);

		SetAddressFocus();

		return FALSE;
		}
	}

void CEurortnxAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnSpaceChange(uID, Wnd);

	if( m_pSpace == NULL ) {

		GetDlgItem(2002).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);

		return;
		}

	if( m_pSpace->m_uTable == 1 ) {

		GetDlgItem(2002).EnableWindow(FALSE);
		
		GetDlgItem(2005).EnableWindow(FALSE);
		}

	ShowDetails();
	}

BOOL CEurortnxAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		Text += GetAddressText();

		Text += GetTypeText();

		CError   Error(TRUE);

		CAddress Addr;
		
		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}

		Error.Show(ThisObject);

		SetAddressFocus();

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

void CEurortnxAddrDialog::SetAddressText(CString Text)
{
	Text.MakeUpper();

	CString s       = Text;

	BOOL    fEnable = !Text.IsEmpty();

	if( fEnable ) {

		if( m_pSpace->m_uTable == 1 ) {

			ShowDetails();

			GetDlgItem(2005).SetWindowText("");

			GetDlgItem(2002).SetWindowText("");

			GetDlgItem(2002).EnableWindow(FALSE);

			GetDlgItem(2004).EnableWindow(FALSE);

			GetDlgItem(2005).EnableWindow(FALSE);

			GetDlgItem(2006).EnableWindow(FALSE);

			return;
			}

		UINT uL = s.GetLength();

		UINT uFR = s.Find('R');

		if( uFR == 0 ) { // Found the R in RL Prefix

			uFR = (s.Mid(1).Find('R')) + 1;
			}

		UINT uFW = s.Find('W');

		if( uFR < NOTHING && uFW < NOTHING ) {

			if( uFW > uFR ) {

				GetDlgItem(2002).SetWindowText( s.Mid(uFR+1,uFW - uFR - 2) );

				GetDlgItem(2005).SetWindowText( s.Mid(uFW+1, uL - uFW - 1) );
				}

			else {

				GetDlgItem(2002).SetWindowText( s.Mid(uFR+1, uL - uFR - 1) );

				GetDlgItem(2005).SetWindowText( s.Mid(uFW+1,uFR - uFW - 2) );
				}
			}

		else {
			GetDlgItem(2002).SetWindowText( uFR >= NOTHING ? "0" : s.Mid(uFR+1, uL - uFR - 1) );

			GetDlgItem(2005).SetWindowText( uFW >= NOTHING ? "0" : s.Mid(uFW+1, uL - uFW - 1) );
			}
		}

	else {

		GetDlgItem(2002).SetWindowText("0");
		GetDlgItem(2005).SetWindowText("0");
		}

	ShowDetails();

	GetDlgItem(2002).EnableWindow(TRUE);
		
	GetDlgItem(2005).EnableWindow(TRUE);
	}

CString CEurortnxAddrDialog::GetAddressText(void)
{
	if( m_pSpace && m_pSpace->m_uTable == 1 ) return "";

	CString Text = "";

	Text += "_R";

	Text += GetDlgItem(2002).GetWindowText();

	Text += "_W";

	Text += GetDlgItem(2005).GetWindowText();

	return Text;
	}

void CEurortnxAddrDialog::ShowAddress(CAddress Addr)
{
	GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

	CString Text;

	switch( m_pSpace->m_Prefix[0] ) {

		case 'B':
			Addr.a.m_Type = addrBitAsBit;
			break;

		case 'I':
			Addr.a.m_Type = addrLongAsLong;
			break;

		case 'T':
			Addr.a.m_Type  = addrWordAsWord;
			Addr.a.m_Table = 1;
			break;

		case 'R':
		default:
			Addr.a.m_Type = addrRealAsReal;
			break;
		}

	m_pDriver->ExpandAddress(Text, m_pConfig, Addr);

	SetAddressText(Text);
	
	GetDlgItem(IDCLEAR).EnableWindow(TRUE);
	}

// Helpers

void CEurortnxAddrDialog::ShowDetails(void)
{
	if( !m_pSpace ) return;

	CListBox &ListBox = (CListBox &) GetDlgItem(4001);

	ListBox.ResetContent();

	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	ListBox.AddString(m_pSpace->GetTypeAsText(m_pSpace->m_uType));

	if( m_pSpace->m_uTable == 1 ) {

		GetDlgItem(3004).SetWindowText("");

		GetDlgItem(3006).SetWindowText("");

		GetDlgItem(3008).SetWindowText("");

		return;
		}

	switch( m_pSpace->m_uType ) {

		case addrLongAsLong:

			GetDlgItem(3004).SetWindowText("INT_R0_W0");
	
			GetDlgItem(3006).SetWindowText("INT_R4095_W4095");

			break;

		case addrBitAsBit:

			GetDlgItem(3004).SetWindowText("BIT_R0_W0");
	
			GetDlgItem(3006).SetWindowText("BIT_R4095_W4095");

			break;

		case addrRealAsReal:
		default:

			GetDlgItem(3004).SetWindowText("RL_R0_W0");
	
			GetDlgItem(3006).SetWindowText("RL_R4095_W4095");

			break;
		}

	GetDlgItem(3008).SetWindowText("10");
	}

// End of File
