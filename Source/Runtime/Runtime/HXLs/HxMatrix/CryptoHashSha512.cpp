
#include "Intern.hpp"

#include "CryptoHashSha512.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SHA512 Cryptographic Hash
//

// Instantiator

static IUnknown * Create_CryptoHashSha512(PCTXT pName)
{
	return New CCryptoHashSha512;
	}

// Registration

global void Register_CryptoHashSha512(void)
{
	piob->RegisterInstantiator("crypto.hash-sha512", Create_CryptoHashSha512);
	}

// Constructor

CCryptoHashSha512::CCryptoHashSha512(void)
{
	m_uHash = sizeof(m_bHash);

	m_pHash = m_bHash;

	psSha512PreInit(&m_Ctx);
	}

// ICryptoHash

CString CCryptoHashSha512::GetName(void)
{
	return "sha512";
	}

void CCryptoHashSha512::GetHashOid(CByteArray &oid)
{
	BYTE b[] = { 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x03 };

	oid.Empty();

	oid.Append(b, sizeof(b));
	}

void CCryptoHashSha512::Initialize(void)
{
	AfxAssert(m_uState == stateNew || m_uState == stateDone);

	psSha512Init(&m_Ctx);

	m_uState = stateActive;
	}

void CCryptoHashSha512::Update(PCBYTE pData, UINT uData)
{
	AfxAssert(m_uState == stateActive);

	psSha512Update(&m_Ctx, pData, uData);
	}

void CCryptoHashSha512::Finalize(void)
{
	AfxAssert(m_uState == stateActive);

	psSha512Final(&m_Ctx, m_pHash);

	m_uState = stateDone;
	}

// End of File
