
#include "intern.hpp"

#include "yasksgdh.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa SGDH Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaSGDHDeviceOptions, CUIItem);

// Constructor

CYaskawaSGDHDeviceOptions::CYaskawaSGDHDeviceOptions(void)
{
	m_Drop = 1;

	m_Ping = 0x8800;
	}

// Download Support

BOOL CYaskawaSGDHDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddWord(WORD(m_Ping));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaSGDHDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Ping);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa SGDH Series
//

// Instantiator

ICommsDriver *	Create_YaskawaSGDHDriver(void)
{
	return New CYaskawaSGDHDriver;
	}

// Constructor

CYaskawaSGDHDriver::CYaskawaSGDHDriver(void)
{
	m_wID		= 0x4009;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "SGDH";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Yaskawa SGDH";

	AddSpaces();
	}

// Binding Control

UINT CYaskawaSGDHDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYaskawaSGDHDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration
CLASS	CYaskawaSGDHDriver::GetDeviceConfig()
{
	return AfxRuntimeClass(CYaskawaSGDHDeviceOptions);
	}

// Implementation

void CYaskawaSGDHDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1, "W",  "Parameters",	16,      0,    0xFFFF,	addrWordAsWord));
	}

// End of File
