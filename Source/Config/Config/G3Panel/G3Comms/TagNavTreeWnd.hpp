
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TagNavTreeWnd_HPP

#define INCLUDE_TagNavTreeWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CDataTag;
class CTagList;
class CTagManager;
class CTagNavTreeWnd;

//////////////////////////////////////////////////////////////////////////
//
// Tag Navigation Window
//

class CTagNavTreeWnd : public CNavTreeWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTagNavTreeWnd(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

	private:
		// Sort Context
		static CTagNavTreeWnd * m_pSort;

	protected:
		// Typedefs
		typedef CMap <INDEX, UINT> CPropMap;

		// Data Members
		CAccelerator    m_Accel;
		CCommsSystem  * m_pSystem;
		CTagManager   * m_pManager;
		CTagList      * m_pTags;
		BOOL		m_fRefresh;
		UINT            m_uMany;
		WORD		m_cfSpec;
		CStringTree     m_Last;

		// Overridables
		void OnAttach(void);
		void OnExec(CCmd *pCmd);
		void OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Accelerator
		BOOL OnAccelerator(MSG &Msg);

		// Message Handlers
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);

		// Notification Handlers
		void OnTreePickItem(UINT uID, NMTREEVIEW &Info);
		UINT OnTreeCustomDraw(UINT uID, NMCUSTOMDRAW &Info);

		// Command Handlers
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnTagsGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnTagsControl(UINT uID, CCmdSource &Src);
		BOOL OnTagsCommand(UINT uID);

		// Tag Commands
		BOOL CanTagsAddToWatch(void);
		void OnTagsNewTag(CLASS Class);
		BOOL OnTagsExport(void);
		BOOL OnTagsImport(void);
		BOOL OnTagsFindAlarms(void);
		BOOL OnTagsFindTriggers(void);
		BOOL OnTagsFindLocked(void);
		BOOL OnTagsFindUnused(void);
		BOOL OnTagsAddToWatch(void);
		BOOL AddToWatch(CDataTag *pTag);
		UINT HasCustomImportIDS(void);

		// Edit Commands
		BOOL CanEditDuplicate(void);
		BOOL CanEditPasteSpecial(void);
		void OnEditDuplicate(void);
		void OnEditPasteSpecial(void);

		// Copy From
		BOOL OnCopyFrom(CUIItem *pPick);
		BOOL OnCopyFrom(CUIItem *pPick, CStringArray &Props, CString Name, UINT uID);
		void StripOther(CStringArray *pProps, ...);
		BOOL StepAddress(CStringArray *pProps, UINT uStep);

		// Tree Loading
		void LoadImageList(void);
		void LoadTree(void);

		// Data Object Construction
		BOOL MakeDataObject(IDataObject * &pData, BOOL fRich);

		// Item Hooks
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void OnItemDeleted(CItem *pItem, BOOL fExec);
		void OnItemRenamed(CItem *pItem);

		// Lock Support
		BOOL IsItemLocked(HTREEITEM hItem);
		BOOL IsTagLocked(HTREEITEM hItem);

		// Implementation
		void LoadTreeImages(HTREEITEM hItem);
		void LockItem(BOOL fLock, BOOL fPrev);
		void SaveMultiCmd(CString Nav, CString Verb);

		// Sort Function
		static int SortByDevice(PCVOID p1, PCVOID p2);
	};

// End of File

#endif
