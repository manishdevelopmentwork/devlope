
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	CString Text = L"!! Hello world. ??";

	CStringNormData   Data;

	CStringNormalizer Norm;

	AfxTrace(L"Text = [%s]\n", Text);

	if( Norm.Normalize(Text, Data) ) {

		AfxTrace(L"Text = [%s]\n", Text);
		AfxTrace(L"Tail = [%s]\n", Data.m_Tail);
		AfxTrace(L"Head = [%s]\n", Data.m_Head);
		AfxTrace(L"Case = %d\n", Data.m_Case);

		Norm.Denormalize(Text, Data);

		AfxTrace(L"Text = [%s]\n", Text);
		}
	}

// End of File
