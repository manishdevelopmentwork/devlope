
#include "intern.hpp"

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CPrimDialog, CItemDialog);

// Static Datat

CStringArray CPrimDialog::m_History;

// Constructor

CPrimDialog::CPrimDialog(CPrim *pPrim, COLOR Back, CString Tab) : CItemDialog(pPrim)
{
	m_pPrim    = pPrim;

	m_Back     = Back;

	m_Tab      = Tab;

	m_pSystem  = (CUISystem *) m_pPrim->GetDatabase()->GetSystemItem();

	m_Caption  = CFormat(CString(IDS_FMT_PROPERTIES), pPrim->GetHumanName());

	m_fCapture = NULL;

	m_pGdi     = NULL;

	FindPreviewSize();

	CreateGdi();
	}

// Destructor

CPrimDialog::~CPrimDialog(void)
{
	DeleteGdi();
	}

// Operations

void CPrimDialog::PrimChanged(void)
{
	if( m_pPrim->IsKindOf(AfxRuntimeClass(CPrimWithText)) ) {

		CPrimWithText *pHost = (CPrimWithText *) m_pPrim;

		if( pHost->m_pTextItem ) {

			pHost->m_pTextItem->TextDirty();
			}
		}

	if( TRUE ) {

		m_pPrim->UpdateLayout();

		CRect Bound = m_pPrim->GetBoundingRect() + 4;

		if( Bound.cx() > m_PrimSize.cx || Bound.cy() > m_PrimSize.cy ) {

			DeleteGdi();

			FindPreviewSize();
		
			CreateGdi();

			FindPreviewLayout();

			FindBestSize();

			PlaceDialogCentral();
			}
		else {
			m_PrimBound = Bound + 4;

			m_PrimNorm  = m_pPrim->GetNormRect();

			m_PrimRect  = m_pPrim->GetRect();
			}
		}

	Invalidate(m_ShowRect + 1, FALSE);
	}

// Message Map

AfxMessageMap(CPrimDialog, CItemDialog)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)

	AfxDispatchCommand(IDOK,     OnOkay  )
	AfxDispatchCommand(IDCANCEL, OnCancel)

	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxMessageEnd(CPrimDialog)
	};

// Message Handlers

void CPrimDialog::OnPostCreate(void)
{
	CItemDialog::OnPostCreate();

	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		CMultiViewWnd *pView = (CMultiViewWnd *) m_pView;

		if( m_Tab.IsEmpty() ) {

			UINT c = m_History.GetCount();

			for( UINT n = c; n > 0; n-- ) {

				CString Tab = m_History[n-1];

				if( pView->SelectTab(Tab) ) {

					m_Tab = Tab;

					break;
					}
				}
			}
		else
			pView->SelectTab(m_Tab);
		}

	FindPreviewLayout();
	}

void CPrimDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	////////

	m_pGdi->SelectBrush(brushFore);

	m_pGdi->SetBrushFore(m_Back);

	m_pGdi->FillRect(0, 0, m_PrimSize.cx, m_PrimSize.cy);

	////////

	CPoint MovePos  = m_PrimNorm.GetTopLeft() - m_PrimBound.GetTopLeft();

	CSize  MoveAdd  = m_PrimSize - m_PrimBound.GetSize();

	CRect  MoveRect = CRect(MovePos + MoveAdd / 2, m_PrimNorm.GetSize());

	if( m_pPrim->IsLine() ) {

		if( m_PrimRect.cx() < 0 ) {

			Swap(MoveRect.left, MoveRect.right);
			}

		if( m_PrimRect.cy() < 0 ) {

			Swap(MoveRect.top, MoveRect.bottom);
			}

		m_pPrim->SetRect(MoveRect);

		m_pPrim->Draw(m_pGdi, drawDialog);

		m_pPrim->SetRect(m_PrimRect);
		}
	else {
		m_pPrim->SetRect(m_PrimRect, MoveRect);

		m_pPrim->Draw(m_pGdi, drawDialog);

		m_pPrim->SetRect(MoveRect, m_PrimRect);
		}

	////////

	if( m_nZoom ) {

		DC.StretchBlt( m_ShowRect,
			       CDC::FromHandle(m_pWin->GetDC()),
			       CRect(m_PrimSize),
			       SRCCOPY
			);
		}
	else {
		GpBitmap *pImage = NULL;

		m_pWin->ReleaseBitmap();

		GdipCreateBitmapFromHBITMAP( m_pWin->GetBitmap(),
					     NULL,
					     &pImage
					     );

		m_pWin->RestoreBitmap();

		GpGraphics *pGraph = NULL;

		DC.SetBkColor(afxColor(TabFace));

		GdipCreateFromHDC(DC.GetHandle(), &pGraph);

		GdipDrawImageRectRectI( pGraph,
					pImage,
					m_ShowRect.left,
					m_ShowRect.top,
					m_ShowRect.cx(),
					m_ShowRect.cy(),
					0,
					0,
					m_PrimSize.cx,
					m_PrimSize.cy,
					UnitPixel,
					NULL,
					NULL,
					NULL
					);
			
		GdipDeleteGraphics(pGraph);

		GdipDisposeImage(pImage);
		}

	DC.FrameRect(m_ShowRect + 1, afxBrush(3dShadow));
	}

void CPrimDialog::OnSize(UINT uCode, CSize Size)
{
	CItemDialog::OnSize(uCode, Size);

	FindPreviewLayout();

	Invalidate(TRUE);
	}

void CPrimDialog::OnLButtonDblClk(UINT uCode, CPoint Pos)
{
	OnLButtonDown(uCode, Pos);
	}

void CPrimDialog::OnLButtonDown(UINT uCode, CPoint Pos)
{
	if( m_ShowRect.PtInRect(Pos) ) {
	
		SetCapture();

		m_fCapture = TRUE;

		m_pPrim->SetPressed(TRUE);

		PrimChanged();
		}
	}

void CPrimDialog::OnLButtonUp(UINT uCode, CPoint Pos)
{
	if( m_fCapture ) {

		KillCapture();

		PrimChanged();
		}
	}

// Command Handlers

BOOL CPrimDialog::OnOkay(UINT uID)
{
	SaveTab();

	KillCapture();

	return CItemDialog::OnOkay(uID);
	}

BOOL CPrimDialog::OnCancel(UINT uID)
{
	SaveTab();

	KillCapture();

	return CItemDialog::OnCancel(uID);
	}

BOOL CPrimDialog::OnViewRefresh(UINT uID)
{
	m_Caption = CFormat(CString(IDS_FMT_PROPERTIES), m_pPrim->GetHumanName());

	SetWindowText(m_Caption);

	return FALSE;
	}

// Implementation

void CPrimDialog::SaveTab(void)
{
	if( m_pView->IsKindOf(AfxRuntimeClass(CMultiViewWnd)) ) {

		CMultiViewWnd *pView = (CMultiViewWnd *) m_pView;

		CString        Tab   = pView->GetTabLabel();

		if( Tab != m_Tab ) {

			if( Tab != "Show" ) {

				UINT c = m_History.GetCount();

				for( UINT n = 0; n < c; n++ ) {

					if( m_History[n] == Tab ) {

						m_History.Remove(n);

						break;
						}
					}

				if( c > 10 ) {

					m_History.Remove(0);
					}

				m_History.Append(Tab);
				}
			}
		}
	}

void CPrimDialog::FindPreviewSize(void)
{
	m_PrimNorm  = m_pPrim->GetNormRect();

	m_PrimRect  = m_pPrim->GetRect();

	m_PrimBound = m_pPrim->GetBoundingRect() + 8;

	m_PrimSize  = m_PrimBound.GetSize();

	if( m_PrimSize.cx < 200 && m_PrimSize.cy < 400 ) {

		if( m_PrimSize.cx < 100 && m_PrimSize.cy < 200 ) {

			m_nZoom = 2;
			}
		else
			m_nZoom = 1;

		m_DrawSize = m_nZoom * m_PrimSize;
		}
	else {
		double xDiv = 200.0 / m_PrimSize.cx;

		double yDiv = 400.0 / m_PrimSize.cy;

		double nDiv = min(xDiv, yDiv);

		m_DrawSize.cx = int(m_PrimSize.cx * nDiv);

		m_DrawSize.cy = int(m_PrimSize.cy * nDiv);

		m_nZoom = 0;
		}

	m_xExtra = m_DrawSize.cx + 16;

	m_yLimit = m_DrawSize.cy + 48;
	}

void CPrimDialog::FindPreviewLayout(void)
{
	CRect View   = GetViewRect();

	CRect Client = GetClientRect();

	m_ShowRect.left   = View.right     +  4;

	m_ShowRect.right  = Client.right   - 12;

	m_ShowRect.top    = View.top       + 29;

	m_ShowRect.bottom = m_ShowRect.top + m_DrawSize.cy;
	}

void CPrimDialog::CreateGdi(void)
{
	if( !m_pGdi ) {

		m_pWin = Create_GdiWindowsA888(m_PrimSize.cx, m_PrimSize.cy);

		m_pGdi = m_pWin->GetGdi();
		}
	}

void CPrimDialog::DeleteGdi(void)
{
	if( m_pGdi ) {

		m_pGdi->Release();

		m_pGdi = NULL;
		}
	}

void CPrimDialog::KillCapture(void)
{
	if( m_fCapture ) {

		ReleaseCapture();

		m_fCapture = FALSE;

		m_pPrim->SetPressed(FALSE);
		}
	}

// End of File
