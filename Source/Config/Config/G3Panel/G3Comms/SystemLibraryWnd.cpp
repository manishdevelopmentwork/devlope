
#include "Intern.hpp"

#include "SystemLibraryWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SystemLibraryItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// System Library Window
//

// Dynamic Class

AfxImplementDynamicClass(CSystemLibraryWnd, CResTreeWnd);

// Constructor

CSystemLibraryWnd::CSystemLibraryWnd(void) : CResTreeWnd( L"Lib",
							  AfxRuntimeClass(CFolderItem),
							  AfxRuntimeClass(CSystemLibraryItem)
							  )
{
	m_cfCode  = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfFunc  = RegisterClipboardFormat(L"C3.1 Action");

	m_fJumpTo = FALSE;

	m_Accel1.Create(L"SystemLibAccel1");
	}

// Message Map

AfxMessageMap(CSystemLibraryWnd, CResTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxDispatchGetInfoType(IDM_RES, OnResGetInfo)
	AfxDispatchControlType(IDM_RES, OnResControl)
	AfxDispatchCommandType(IDM_RES, OnResCommand)

	AfxMessageEnd(CSystemLibraryWnd)
	};

// Message Handlers

void CSystemLibraryWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"SystemLibraryTool"));
		}
	}

void CSystemLibraryWnd::OnSetFocus(CWnd &Wnd)
{
	CResTreeWnd::OnSetFocus(Wnd);

	ShowPrototype();
	}

// Command Handlers

BOOL CSystemLibraryWnd::OnResGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{	IDM_RES_HELP,   MAKELONG(0x000E, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CSystemLibraryWnd::OnResControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_RES_HELP:

			if( C3OemFeature(L"FuncHelp", TRUE) ) {

				Src.EnableItem(!m_pTree->GetChild(m_hSelect));
				}

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

BOOL CSystemLibraryWnd::OnResCommand(UINT uID)
{
	switch( uID ) {

		case IDM_RES_HELP:

			FindHelp();

			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

// Tree Loading

void CSystemLibraryWnd::SortTree(void)
{
	INDEX Index = m_MapNames.GetHead();

	while( !m_MapNames.Failed(Index) ) {

		HTREEITEM hRoot = m_MapNames.GetData(Index);

		CMetaItem *pItem = (CMetaItem *) m_pTree->GetItemParam(hRoot);

		if( pItem->IsKindOf(m_Folder) ) {

			m_pTree->SortChildren(hRoot, PFNTVCOMPARE(&SortHelp), LPARAM(this));
			}

		m_MapNames.GetNext(Index);
		}
	}

void CSystemLibraryWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"ProgTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CSystemLibraryWnd::GetRootImage(void)
{
	return 25;
	}

UINT CSystemLibraryWnd::GetItemImage(CMetaItem *pItem)
{
	if( pItem->IsKindOf(m_Folder) ) {

		return 22;
		}

	return ((CSystemLibraryItem *) pItem)->m_Image;
	}

BOOL CSystemLibraryWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"SystemLibCtxMenu";

		return TRUE;
		}

	return FALSE;
	}

void CSystemLibraryWnd::NewItemSelected(void)
{
	ShowPrototype();
	}

// Data Object Creation

BOOL CSystemLibraryWnd::MakeDataObject(IDataObject * &pData)
{
	CDataObject *pMake = New CDataObject;

	AddCodeFragment(pMake);

	if( pMake->IsEmpty() ) {

		delete pMake;

		pData = NULL;

		return FALSE;
		}

	pData = pMake;

	return TRUE;
	}

BOOL CSystemLibraryWnd::AddCodeFragment(CDataObject *pData)
{
	if( m_pNamed ) {

		if( m_pNamed->IsKindOf(m_Class) ) {

			CSystemLibraryItem *pItem = (CSystemLibraryItem *) m_pSelect;

			if( !pItem->m_Code.IsEmpty() ) {

				if( pItem->m_fAct ) {

					pData->AddText(m_cfFunc, pItem->m_Code);
					}
				else
					pData->AddText(m_cfCode, pItem->m_Code);
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Sort Helpers

int CSystemLibraryWnd::Sort(CMetaItem *p1, CMetaItem *p2)
{
	if( p1->GetName() < p2->GetName() ) {

		return -1;
		}

	if( p1->GetName() > p2->GetName() ) {

		return +1;
		}

	return 0;
	}

// Sort Callback

int CALLBACK CSystemLibraryWnd::SortHelp(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	CMetaItem *p1 = (CMetaItem *) lParam1;

	CMetaItem *p2 = (CMetaItem *) lParam2;

	return ((CSystemLibraryWnd *) lParamSort)->Sort(p1, p2);
	}

// Implementation

void CSystemLibraryWnd::ShowPrototype(void)
{
	CString Text;

	if( m_pSelect->IsKindOf(AfxRuntimeClass(CSystemLibraryItem)) ) {

		CSystemLibraryItem *pItem = (CSystemLibraryItem *) m_pSelect;

		Text = pItem->m_Prot;
		}

	afxThread->SetStatusText(Text);
	}

BOOL CSystemLibraryWnd::FindHelp(void)
{
	CPdfHelpSystem pdf(1);

	if( pdf.ShowHelp(m_pTree->GetItemText(m_hSelect)) ) {

		return TRUE;
		}

	afxMainWnd->Error(CString(IDS_HELP_IS_NOT));

	return FALSE;
	}

// End of File
