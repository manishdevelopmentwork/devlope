
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Camera Driver Implementation
//

// Constructor

CCameraDriver::CCameraDriver(void)
{
	}

// IUnknown

HRESULT CCameraDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsCamera);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CCameraDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CCameraDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CCameraDriver::GetCategory(void)
{
	return DC_COMMS_CAMERA;
	}

// ICommsCamera

CCODE CCameraDriver::Ping(void)
{
	return 1;
	}

CCODE CCameraDriver::ReadImage(PBYTE &pData)
{
	return 1;
	}

void CCameraDriver::SwapImage(PBYTE pData)
{
	}

void CCameraDriver::KillImage(void)
{
	}

PCBYTE CCameraDriver::GetData(UINT uDevice)
{
	return 0;
	}

UINT CCameraDriver::GetInfo(UINT uDevice, UINT uParam)
{
	return NOTHING;
	}

BOOL CCameraDriver::SaveSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL CCameraDriver::LoadSetup(PVOID pContext, UINT uIndex, FILE *pFile)
{
	return FALSE;
	}

BOOL CCameraDriver::UseSetup(PVOID pContext, UINT uIndex)
{
	return FALSE;
	}

// End of File
