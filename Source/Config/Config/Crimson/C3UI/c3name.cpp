
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Validated Name
//

// Dynamic Class

AfxImplementDynamicClass(CUITextName, CUITextElement);

// Constructor

CUITextName::CUITextName(void)
{
	m_uFlags = textEdit;
	}

// Overridables

void CUITextName::OnBind(void)
{
	CUITextElement::OnBind();

	CString Size = GetFormat().StripToken('|');

	if( !Size.IsEmpty() ) {

		m_uLimit = watoi(Size);

		MakeMax(m_uLimit, 1);
		}

	m_uWidth = Min(m_uLimit, 40);
	}

CString CUITextName::OnGetAsText(void)
{
	return m_pData->ReadString(m_pItem);
	}

UINT CUITextName::OnSetAsText(CError &Error, CString Text)
{
	if( C3ValidateName(Error, Text) ) {
		
		m_pData->WriteString(m_pItem, Text);

		return saveChange;		
		}

	return saveError;
	}

// End of File
