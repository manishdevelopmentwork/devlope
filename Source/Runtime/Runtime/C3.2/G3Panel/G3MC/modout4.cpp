
#include "intern.hpp"

#include "modout4.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// OUT4 Module Configuration
//

// Instantiator

CModule * Create_OUT4(void)
{
	return New COUT4Module;
	}

// Constructor

COUT4Module::COUT4Module(void)
{
	m_FirmID = FIRM_OUT4;
	}

// Destructor

COUT4Module::~COUT4Module(void)
{
	}

// Overridables

void COUT4Module::OnLoad(PCBYTE &pData)
{
	GetWord(pData);
	}

// End of File
