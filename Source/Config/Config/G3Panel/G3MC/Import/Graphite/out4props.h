
//////////////////////////////////////////////////////////////////////////
//
// R307 Tacoma - 4-Channel Analog Output Module
//
// Copyright (c) 2001-2006 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_OUT4PROPS_H

#define	INCLUDE_OUT4PROPS_H

//////////////////////////////////////////////////////////////////////////
//
// Macros
//

#define	MAKEPROP(type, id)	MAKEWORD(id, type)

//////////////////////////////////////////////////////////////////////////
//
// Object IDs
//

#define	OBJ_LOOP_1		0x01

//////////////////////////////////////////////////////////////////////////
//
// Property Types
//

#define	TYPE_BOOL		0x01
#define	TYPE_BYTE		0x02
#define	TYPE_WORD		0x03
#define	TYPE_LONG		0x04
#define	TYPE_REAL		0x05

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Installation
//

#define	PROPID_OUT_TYPE_1	MAKEPROP(TYPE_BYTE, 0x01)
#define	PROPID_OUT_TYPE_2	MAKEPROP(TYPE_BYTE, 0x02)
#define	PROPID_OUT_TYPE_3	MAKEPROP(TYPE_BYTE, 0x03)
#define	PROPID_OUT_TYPE_4	MAKEPROP(TYPE_BYTE, 0x04)

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Configuration
//

#define	PROPID_DATA_1		MAKEPROP(TYPE_WORD, 0x05)
#define	PROPID_DATA_2		MAKEPROP(TYPE_WORD, 0x06)
#define	PROPID_DATA_3		MAKEPROP(TYPE_WORD, 0x07)
#define	PROPID_DATA_4		MAKEPROP(TYPE_WORD, 0x08)

#define	PROPID_DATA_LO_1	MAKEPROP(TYPE_WORD, 0x09)
#define	PROPID_DATA_LO_2	MAKEPROP(TYPE_WORD, 0x0A)
#define	PROPID_DATA_LO_3	MAKEPROP(TYPE_WORD, 0x0B)
#define	PROPID_DATA_LO_4	MAKEPROP(TYPE_WORD, 0x0C)

#define	PROPID_DATA_HI_1	MAKEPROP(TYPE_WORD, 0x0D)
#define	PROPID_DATA_HI_2	MAKEPROP(TYPE_WORD, 0x0E)
#define	PROPID_DATA_HI_3	MAKEPROP(TYPE_WORD, 0x0F)
#define	PROPID_DATA_HI_4	MAKEPROP(TYPE_WORD, 0x10)

#define	PROPID_OUT_LO_1		MAKEPROP(TYPE_WORD, 0x11)
#define	PROPID_OUT_LO_2		MAKEPROP(TYPE_WORD, 0x12)
#define	PROPID_OUT_LO_3		MAKEPROP(TYPE_WORD, 0x13)
#define	PROPID_OUT_LO_4		MAKEPROP(TYPE_WORD, 0x14)

#define	PROPID_OUT_HI_1		MAKEPROP(TYPE_WORD, 0x15)
#define	PROPID_OUT_HI_2		MAKEPROP(TYPE_WORD, 0x16)
#define	PROPID_OUT_HI_3		MAKEPROP(TYPE_WORD, 0x17)
#define	PROPID_OUT_HI_4		MAKEPROP(TYPE_WORD, 0x18)

//////////////////////////////////////////////////////////////////////////
//
// Property IDs -- Status
//

#define	PROPID_ALARM_1		MAKEPROP(TYPE_BOOL, 0x55)
#define	PROPID_ALARM_2		MAKEPROP(TYPE_BOOL, 0x56)
#define	PROPID_ALARM_3		MAKEPROP(TYPE_BOOL, 0x57)
#define	PROPID_ALARM_4		MAKEPROP(TYPE_BOOL, 0x58)

// End of File

#endif
