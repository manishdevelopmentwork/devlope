
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAUIN6Input_HPP

#define INCLUDE_DAUIN6Input_HPP

//////////////////////////////////////////////////////////////////////////
//
// DAUIN6 Inputs
//

class CDAUIN6Input : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAUIN6Input(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_InputAlarm1;
	UINT m_InputAlarm2;
	UINT m_InputAlarm3;
	UINT m_InputAlarm4;
	UINT m_InputAlarm5;
	UINT m_InputAlarm6;

	UINT m_PV1;
	UINT m_PV2;
	UINT m_PV3;
	UINT m_PV4;
	UINT m_PV5;
	UINT m_PV6;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
