
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Single Image
//

// Constructor

CPrimSimpleImage::CPrimSimpleImage(void)
{
	m_pGray  = NULL;

	m_pImage = NULL;
	}

// Destructor

CPrimSimpleImage::~CPrimSimpleImage(void)
{
	delete m_pGray;

	delete m_pImage;
	}

// Initialization

void CPrimSimpleImage::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimAnimImage", pData);

	CPrimWithText::Load(pData);

	GetCoded(pData, m_pGray);

	if( GetByte(pData) ) {

		m_pImage = New CPrimImage;
	
		m_pImage->Load(pData);
		}
	}

// Overridables

void CPrimSimpleImage::SetScan(UINT Code)
{
	SetItemScan(m_pGray, Code);

	CPrimWithText::SetScan(Code);
	}

void CPrimSimpleImage::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrimWithText::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( !m_pImage || m_pImage->IsTransparent(m_DrawRect) ) {

			if( m_pImage && m_pImage->IsAntiAliased() ) {

				(m_fChange ? Erase : Trans).Append(m_DrawRect);
				}
			else {
				if( m_fChange ) {

					Erase.Append(m_DrawRect);
					}
				}
			}
		}
	}

void CPrimSimpleImage::DrawPrim(IGDI *pGDI)
{
	if( m_pImage ) {

		UINT rop = 0;

		if( !m_Ctx.m_fColor ) {

			rop = ropDisable;
			}

		m_pImage->DrawItem(pGDI, m_DrawRect, rop);
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCUTF pt = L"IMAGE";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimWithText::DrawPrim(pGDI);
	}

// Context Creation

void CPrimSimpleImage::FindCtx(CCtx &Ctx)
{
	Ctx.m_fColor = GetItemData(m_pGray, C3INT(1));
	}

// Context Check

BOOL CPrimSimpleImage::CCtx::operator == (CCtx const &That) const
{
	return m_fColor == That.m_fColor;
	}

// End of File
