
#include "intern.hpp"

#include "Micro800.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CIP Address Descriptor Comparison
//

bool operator >(CCipDesc const &Left, CCipDesc const &Right)
{
	return Left.m_Name.CompareC(Right.m_Name) < 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Base Driver
//

// Constructor

CABMicro800BaseDriver::CABMicro800BaseDriver(void)
{
	}

// Driver Data

UINT CABMicro800BaseDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagImport;
	}

// Address Management

BOOL CABMicro800BaseDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CABMicro800DeviceOptions * pDevice = (CABMicro800DeviceOptions *) pConfig;

	return pDevice->ParseAddress(Error, Addr, Text);
	} 

BOOL CABMicro800BaseDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CABMicro800DeviceOptions * pDevice = (CABMicro800DeviceOptions *) pConfig;

	return pDevice->ExpandAddress(Text, Addr);
	}

BOOL CABMicro800BaseDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CABMicro800Dlg Dlg(this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CABMicro800BaseDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	CABMicro800DeviceOptions *pDevice = (CABMicro800DeviceOptions *) pConfig;

	return pDevice->ListAddress(pRoot, uItem, Data);
	}

// Notifications

void CABMicro800BaseDriver::NotifyInit(CItem * pConfig)
{
	CABMicro800DeviceOptions *pDevice = (CABMicro800DeviceOptions *) pConfig;

	if( pDevice ) {

		pDevice->Rebuild();
		}
	}

void CABMicro800BaseDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	CABMicro800DeviceOptions *pDevice = (CABMicro800DeviceOptions *) pConfig;

	if( pDevice ) {

		pDevice->UpdateExtent(Addr, nSize);
		}
	}

// Tag Import

BOOL CABMicro800BaseDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	CAbMicro800TagsCreator AbTags(DevName);

	AbTags.SetParent(pConfig);

	AbTags.GetTagConfig(pConfig);
	
	CItemDialog TagDlg(&AbTags, CString(IDS_AB_TAG));

	if( TagDlg.Execute(*afxMainWnd) ) {

		CABMicro800DeviceOptions * pOpt = (CABMicro800DeviceOptions *)pConfig;

		if( pOpt ) {

			CString Error;

			if( pOpt->ImportTags(pTags, DevName, AbTags, Error) ) {

				if( !Error.IsEmpty() ) {

					CStringArray List;

					Error.Tokenize(List, ',');

					CABMicro800ImportErrorDialog Dlg(List);

					Dlg.Execute();
					}

				AbTags.SaveTagConfig(pConfig);

				return TRUE;
				}
			}
		}

	return FALSE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 TCP Driver
//

// Instantiator

ICommsDriver *	Create_ABMicro800TcpMaster(void)
{
	return New CABMicro800TcpMaster;
	}

// Constructor

CABMicro800TcpMaster::CABMicro800TcpMaster(void)
{
	m_wID		= 0x40D0;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Micro800 Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Micro800";
	}

// Binding

UINT CABMicro800TcpMaster::GetBinding(void)
{
	return bindEthernet;
	}

void CABMicro800TcpMaster::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CABMicro800TcpMaster::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CABMicro800TcpMaster::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABMicro800TcpDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Serial Driver
//

// Instantiator

ICommsDriver *	Create_ABMicro800SerialMaster(void)
{
	return New CABMicro800SerialMaster;
	}

// Constructor

CABMicro800SerialMaster::CABMicro800SerialMaster(void)
{
	m_wID		= 0x40CF;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Micro800 Master";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Micro800";
	}

// Binding

UINT CABMicro800SerialMaster::GetBinding(void)
{
	return bindStdSerial;
	}

void CABMicro800SerialMaster::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CABMicro800SerialMaster::GetDriverConfig(void)
{
	return AfxRuntimeClass(CABMicro800SerialDriverOptions);
	}

CLASS CABMicro800SerialMaster::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABMicro800SerialDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CABMicro800SerialDriverOptions, CUIItem);

// Constructor

CABMicro800SerialDriverOptions::CABMicro800SerialDriverOptions(void)
{
	m_Address = 0;

	m_fHalf   = FALSE;

	m_fCRC    = TRUE;		
	}

// Download Support

BOOL CABMicro800SerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Address));
	Init.AddByte(BYTE(m_fHalf));
	Init.AddByte(BYTE(m_fCRC));

	return TRUE;
	}
			
// Meta Data Creation

void CABMicro800SerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Address);
	Meta_AddBoolean(Half);
	Meta_AddBoolean(CRC);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABMicro800DeviceOptions, CUIItem);

// Constructor

CABMicro800DeviceOptions::CABMicro800DeviceOptions(void)
{
	m_Push    = 0;

	m_Root    = "";

	m_Name    = 1;

	m_Label   = 256;
	}

// Address Management

BOOL CABMicro800DeviceOptions::ParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CCipDesc Find;

	CString  Dims;

	UINT uFind = NOTHING;

	if( (uFind = Text.Find(L"[")) < NOTHING ) {

		Dims = Text.Mid(uFind);

		Text = Text.Left(uFind);
		}

	Find.m_Name = Text;

	// cppcheck-suppress uninitStructMember

	INDEX i = m_Map.FindName(Find);

	if( !m_Map.Failed(i) ) {

		CCipDesc &Desc = (CCipDesc &)m_Map.GetName(i);

		UINT uOffset   = 0;

		if( !Desc.m_fNamed ) {

			if( !GetOffset(Dims, Desc, uOffset) ) {

				Error.Set(CString(IDS_ARRAY_OFFSET_OUT));

				return FALSE;
				}
			}

		Addr.m_Ref  = Desc.m_uIndex + uOffset;

		SetUsed(Desc.m_uIndex);

		return TRUE;
		}

	Error.Set(CString(IDS_ADDRESS_NOT_FOUND));

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::ExpandAddress(CString &Text, CAddress const &Addr)
{
	CTableSlot Slot;

	CAddress   Base;

	Base.m_Ref = FindTableSlot(Addr, Slot) < NOTHING ? Slot.m_Slot : Addr.m_Ref;

	INDEX i    = m_Map.FindData(Base.m_Ref);

	if( !m_Map.Failed(i) ) {

		CCipDesc Desc = m_Map.GetName(i);

		Text = Desc.m_Name;

		if( Addr.a.m_Table < addrNamed ) {
			
			Text += ExpandDims(Addr.m_Ref - Base.m_Ref, Desc);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)	
{
	if( !pRoot ) {

		if( uItem >= m_Map.GetCount() ) {

			return FALSE;
			}

		UINT  n = 0;

		INDEX i = m_Map.GetHead();

		while( n < uItem ) {

			m_Map.GetNext(i);

			n++;
			}

		CCipDesc Desc = m_Map.GetName(i);

		BOOL fNamed   = Desc.m_fNamed;

		Data.m_Addr.a.m_Extra  = IsSTR(CipTypeFromAtomic(Desc.m_CipType));

		Data.m_Addr.a.m_Offset = fNamed ? Desc.m_uIndex : 0;

		Data.m_Addr.a.m_Table  = fNamed ? addrNamed     : Desc.m_uIndex;

		Data.m_Addr.a.m_Type   = GetCipTypeEnum(Desc.m_CipType);

		Data.m_fPart      = TRUE;

		Data.m_Name       = Desc.m_Name;

		Data.m_uData      = 0;

		return TRUE;
		}

	return FALSE;
	}

// UI Management

void CABMicro800DeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == L"Push" ) {

			switch( m_Push ) {

				case pushManage:

					ManageTags();

					break;

				case pushImport:

					ImportFromFile();

					break;

				case pushExport:

					ExportToFile();

					break;
				}
			}
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

void CABMicro800DeviceOptions::PrepareData(void)
{
	m_List.Empty();

	for( INDEX i = m_Map.GetHead(); !m_Map.Failed(i); m_Map.GetNext(i) ) {

		CCipDesc Desc = m_Map.GetName(i);

		if( Desc.m_fUsed ) {

			m_List.Append(Desc);
			}
		}
	}

BOOL CABMicro800DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	UINT uCount = m_List.GetCount();

	Init.AddLong(uCount);

	for( INDEX i = m_List.GetHead(); !m_List.Failed(i); m_List.GetNext(i) ) {

		CCipDesc Desc = m_List.GetAt(i);

		Init.AddLong(Desc.m_uIndex);

		UINT x = NOTHING;
		UINT y = NOTHING;
		UINT z = NOTHING;

		switch( Desc.m_Dims.GetCount() ) {

			// Fall through is intentional.

			case 3:
				z = Desc.m_Dims[2];
			case 2:
				y = Desc.m_Dims[1];
			case 1:
				x = Desc.m_Dims[0];
			}

		Init.AddLong(x);
		Init.AddLong(y);
		Init.AddLong(z);

		AddInitString(PCTXT(Desc.m_Name), Init);

		Init.AddWord(WORD(CipTypeFromAtomic(Desc.m_CipType)));

		Init.AddByte(BYTE(Desc.m_uChars));
		}

	//PrintTags();

	return TRUE;
	}

// Persistance

void CABMicro800DeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Tags" ) {

			Tree.GetObject();

			LoadTags(Tree);

			Tree.EndObject();
			
			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CABMicro800DeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	if( m_Map.GetCount() > 0 ) {

		Tree.PutObject(TEXT("Tags"));

		SaveTags(Tree);

		Tree.EndObject();
		}
	}

void CABMicro800DeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	INDEX Index = m_Map.GetHead();

	while( !m_Map.Failed(Index) ) {

		CCipDesc &Desc = (CCipDesc &)m_Map.GetName(Index);

		if( Desc.m_CipType.Find(CIP_PREFIX) == NOTHING ) {

			if( Desc.m_CipType == CipTypeFromEnum(typeBYTE) ) {

				Desc.m_CipType = CipTypeFromEnum(typeUSINT);
				}
			
			Desc.m_CipType.Insert(0, CIP_PREFIX);
			
			Index = m_Map.FindName(Desc);
			}

		m_Map.GetNext(Index);
		}
	}

// CIP Conversions

CString CABMicro800DeviceOptions::ExtToCip(CString Ext)
{
	StripCipPrefix(Ext);

	for( UINT u = 0; u < elements(CIP_TYPES); u++ ) {

		if( Ext == CipTypeFromEnum(u) ) {

			switch( u ) {

				case typeBOOL:
				case typeSTR:
				case typeINT:
				case typeDINT:
				case typeREAL:
				
					return Ext;
				}

			if( IsBYTE(u) ) {

				return CipTypeFromEnum(typeBYTE);
				}

			if( IsWORD(u) ) {
				
				return CipTypeFromEnum(typeINT);
				}

			break;
			}
		}

	return CipTypeFromEnum(GetCipDefault());
	}

BYTE CABMicro800DeviceOptions::GetCipTypeEnum(CString const &Type)
{
	UINT uType = TypeFromAtomic(Type);

	if( uType < NOTHING ) {

		return LOBYTE(LOWORD(uType));
		}

	return GetCipDefault();
	}

void CABMicro800DeviceOptions::InitSlotIndex(void)
{
	m_Index = 0;

	m_Named = addrNamed;
	}

UINT CABMicro800DeviceOptions::GetSlotIndex(CCipDesc const &Desc)
{
	UINT uType = CipTypeFromAtomic(Desc.m_CipType);

	switch( uType ) {

		case typeBOOL:
		case typeSTR:
		case typeINT:
		case typeDINT:
		case typeREAL:
		case typeUSINT:

			break;

		default:
			return NOTHING;
		}

	if( Desc.m_Dims.GetCount() || IsSTR(uType) ) {

		m_Index++;

		return m_Index;
		}

	m_Named++;

	return m_Named;
	}

void CABMicro800DeviceOptions::LoadTags(CTreeFile &Tree)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "TagName" ) {

			CCipDesc Desc;

			Desc.m_Name = Tree.GetValueAsString();

			Tree.GetName();			

			Desc.m_uIndex = Tree.GetValueAsInteger();

			Tree.GetName();

			Desc.m_fNamed = Tree.GetValueAsInteger();

			Tree.GetName();

			UINT uCount = Tree.GetValueAsInteger();

			for( UINT n = 0; n < uCount; n++ ) {

				Tree.GetName();

				UINT uDim = Tree.GetValueAsInteger();

				Desc.m_Dims.Append(uDim);
				}
			
			Tree.GetName();

			Desc.m_CipType = Tree.GetValueAsString();

			if( Tree.IsName(L"Chars")  ) {

				Tree.GetName();

				Desc.m_uChars = Tree.GetValueAsInteger();

				Tree.GetName();

				Desc.m_fUsed  = Tree.GetValueAsInteger();

				if( Tree.IsName(L"ExtType") ) {

					Tree.GetName();

					Desc.m_CipType = Tree.GetValueAsString();

					Tree.GetName();

					Desc.m_uIndex = Tree.GetValueAsInteger();
					}

				Desc.m_uType = TypeFromAtomic(Desc.m_CipType);

				AddDesc(Desc, Desc.m_uIndex);
				}
			else {
				Desc.m_uChars = IsSTR(CipTypeFromAtomic(Desc.m_CipType)) ? 80 : 0;

				Desc.m_fUsed  = TRUE;

				Desc.m_uType  = TypeFromAtomic(Desc.m_CipType);

				AddDesc(Desc);
				}
			}
		}
	}

void CABMicro800DeviceOptions::SaveTags(CTreeFile &Tree)
{
	InitSlotIndex();

	for( INDEX i = m_Map.GetHead(); !m_Map.Failed(i); m_Map.GetNext(i) ) {

		CCipDesc Desc = m_Map.GetName(i);

		UINT uSlot    = m_Map.GetData(i);

		Tree.PutValue(L"TagName",  Desc.m_Name);

		Tree.PutValue(L"Slot",     GetSlotIndex(Desc));

		Tree.PutValue(L"Named",    Desc.m_fNamed);

		Tree.PutValue(L"DimCount", Desc.m_Dims.GetCount());

		for( UINT n = 0; n < Desc.m_Dims.GetCount(); n++ ) {

			Tree.PutValue(CPrintf("Dim%u", n), Desc.m_Dims[n]);
			}

		Tree.PutValue(L"CipType",  ExtToCip(Desc.m_CipType));

		Tree.PutValue(L"Chars",    Desc.m_uChars);

		Tree.PutValue(L"Used",     Desc.m_fUsed);

		Tree.PutValue(L"ExtType",  Desc.m_CipType);

		Tree.PutValue(L"CipMap",   uSlot);
		}
	}

void CABMicro800DeviceOptions::Rebuild(void)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		WalkSlots();

		ClearUsed();

		pSystem->Rebuild(1);
		}
	}

void CABMicro800DeviceOptions::UpdateExtent(CAddress const &Addr, INT nSize)
{
	CTableSlot Slot;

	if( FindTableSlot(Addr, Slot) < NOTHING ) {

		Slot.m_Size = nSize;
		}	
	}

// Tags

UINT CABMicro800DeviceOptions::AddDesc(CCipDesc &Desc)
{
	if( IsValid(Desc) ) {

		UINT uSlot = Desc.m_fNamed ? GetNextNamedSlot(Desc) : GetNextTableSlot(Desc);

		if( uSlot < NOTHING ) {

			Desc.m_uIndex   = uSlot;

			Desc.m_CipType  = GetCipType(Desc.m_CipType);

			CCipDesc Search = Desc;

			if( !m_Map.FindName(Search) ) {

				m_Map.Insert(Desc, uSlot);

				GetDatabase()->SetDirty();

				return uSlot;
				}
			}
		}

	return NOTHING;
	}

UINT CABMicro800DeviceOptions::AddDesc(CCipDesc &Desc, UINT uSlot)
{
	if( IsValid(Desc) ) {

		CCipDesc Search = Desc;

		if( !m_Map.FindName(Search) ) {

			m_Map.Insert(Desc, uSlot);

			if( Desc.m_fNamed ) {

				m_NamedSlots.Append(uSlot);

				return uSlot;
				}

			return AddTableSlot(uSlot, CipTypeFromAtomic(Desc.m_CipType), Desc, GetSize(Desc));
			}
		}
	
	return NOTHING;
	}

BOOL CABMicro800DeviceOptions::RemoveDesc(CTableSlot &Slot, BOOL fNamed)
{
	UINT uSlot = Slot.m_Slot;

	INDEX i    = m_Map.FindData(uSlot);

	if( !m_Map.Failed(i) ) {

		m_Map.Remove(i);

		if( !fNamed ) {

			UINT uPos = m_TableSlots.Find(Slot);

			if( uPos < NOTHING ) {

				m_TableSlots.Remove(uPos);

				m_TableSlots.Sort();
				}
			}
		else {
			UINT uPos = m_NamedSlots.Find(uSlot);

			if( uPos < NOTHING ) {

				m_NamedSlots.Remove(uPos);
				}
			}

		Rebuild();

		return TRUE;
		}

	return FALSE;
	}

// Meta Data Creation

void CABMicro800DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Push);
	Meta_AddString (Root);
	Meta_AddInteger(Name);
	Meta_AddInteger(Label);
	}

// Implementation

void CABMicro800DeviceOptions::ImportFromFile(void)
{
	CTextStreamMemory Stream;

	CString Path;

	if( PreImport(Stream, Path) ) {

		CStringArray Parts;

		CString      Dims;

		while ( LineImport(Stream, Parts, Dims) ) {

			CCipDesc Desc;

			CString  Error;

			MakeDesc(Desc, Parts, Dims, Error);

			Dims.Empty();
			}
		
		PostImport(Stream, TRUE);
		}
	}

void CABMicro800DeviceOptions::ExportToFile(void)
{
	CSaveFileDialog Dlg;

	Dlg.LoadLastPath(L"Micro800");

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute() ) {

		Dlg.SaveLastPath(L"Micro800");

		CFilename Filename = Dlg.GetFilename();

		FILE *pFile;

		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = TEXT("The selected file already exists.\n\n")
				       TEXT("Do you want to overwrite it?");

			if( afxMainWnd->YesNo(Text) == IDNO ) {

				return;
				}
			}

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			CString Header("Name,Data Type,Dimension,String Size,Initial Value,Direction,Attribute,Retained,Comment,Alias,Retained Flags,Comment Fields,Project Value\r\n");

			Stream.PutLine(Header);

			for( INDEX i = m_Map.GetHead(); !m_Map.Failed(i); m_Map.GetNext(i) ) {

				CCipDesc Desc = m_Map.GetName(i);

				CString Dimensions;

				UINT uDims = Desc.m_Dims.GetCount();

				if( uDims > 0 ) {

					Dimensions += uDims > 0 ? L"\"[" : L"[";
					
					if( uDims >= 1 ) {

						Dimensions += CPrintf("0..%u", Desc.m_Dims[0] - 1);
						}

					if( uDims >= 2 ) {

						Dimensions += CPrintf(",0..%u", Desc.m_Dims[1] - 1);
						}

					if( uDims >= 3 ) {

						Dimensions += CPrintf(",0..%u", Desc.m_Dims[2] - 1);
						}

					Dimensions += uDims > 0 ? L"\"]" : L"]";
					}
				
				CString Line = CPrintf("%s,%s,%s,%u,,Var,ReadWrite,FALSE,,,,\r\n",
					PCTXT(Desc.m_Name),
					PCTXT(Desc.m_CipType),
					PCTXT(Dimensions),
					Desc.m_uChars);

				Stream.PutLine(Line);
				}

			if( Stream.SaveToFile(Filename, saveRaw) ) {

				Stream.Close();
				}
			}
		}
	}

void CABMicro800DeviceOptions::ManageTags(void)
{
	CABMicro800ManageDlg Dlg(this);

	Dlg.Execute();
	}

UINT CABMicro800DeviceOptions::TypeFromAtomic(CString const &Type)
{	
	CString Atomic = Type;

	StripCipPrefix(Atomic);

	if( Atomic == CipTypeFromEnum(typeBOOL ) ) return addrBitAsBit;

	if( Atomic == CipTypeFromEnum(typeBYTE ) ) return addrByteAsByte;

	if( Atomic == CipTypeFromEnum(typeINT  ) ) return addrWordAsWord;	

	if( Atomic == CipTypeFromEnum(typeDINT ) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeREAL ) ) return addrRealAsReal;

	if( Atomic == CipTypeFromEnum(typeSTR  ) ) return addrByteAsByte;

	if( Atomic == CipTypeFromEnum(typeSINT ) ) return addrByteAsByte;

	if( Atomic == CipTypeFromEnum(typeUSINT) ) return addrByteAsByte;

	if( Atomic == CipTypeFromEnum(typeUINT ) ) return addrWordAsWord;

	if( Atomic == CipTypeFromEnum(typeWORD ) ) return addrWordAsWord;

	if( Atomic == CipTypeFromEnum(typeUDINT) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeDWORD) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeTIME ) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeDATE ) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeLINT ) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeULINT) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeLWORD) ) return addrLongAsLong;

	if( Atomic == CipTypeFromEnum(typeLREAL) ) return addrRealAsReal;

	return NOTHING;
	}

CString CABMicro800DeviceOptions::GetTypeString(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:

			return IDS_SPACE_BAB;

		case addrByteAsByte:

			return IDS_SPACE_BYABY;

		case addrWordAsWord:

			return IDS_SPACE_WAW;

		case addrLongAsLong:

			return IDS_SPACE_LAL;

		case addrRealAsReal:

			return IDS_SPACE_RAR;
		}

	return "";
	}

BOOL CABMicro800DeviceOptions::IsNameValid(CString const &Name)
{
	UINT uLen = Name.GetLength();

	for( UINT n = 0; n < uLen; n++ ) {

		TCHAR c = Name.GetAt(n);

		if( !isalnum(c) && c != L'_' ) {

			return FALSE;
			}
		}

	return TRUE;
	}

void CABMicro800DeviceOptions::AddInitString(CString const &Text, CInitData &Init)
{
	UINT uCount = Text.GetLength();

	Init.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		CHAR c = CHAR(Text.GetAt(n));

		Init.AddByte(c);
		}
	}

void CABMicro800DeviceOptions::SetUsed(UINT uPos)
{
	INDEX i = m_Map.FindData(uPos);

	if( !m_Map.Failed(i) ) {

		CCipDesc &Desc = (CCipDesc &) m_Map.GetName(i);

		Desc.m_fUsed = TRUE;
		}
	}

void CABMicro800DeviceOptions::ClearUsed(void)
{
	for( INDEX i = m_Map.GetHead(); !m_Map.Failed(i); m_Map.GetNext(i) ) {

		CCipDesc &Desc = (CCipDesc &) m_Map.GetName(i);

		Desc.m_fUsed = FALSE;
		}
	}

CString CABMicro800DeviceOptions::CipTypeFromEnum(UINT uEnum)
{
	CString Type = uEnum < elements(CIP_TYPES) ? CIP_TYPES[uEnum] : L"";

	StripCipPrefix(Type);
	
	return Type;
	}

// Tag Creation

BOOL CABMicro800DeviceOptions::ImportTags(IMakeTags *pTags, CString DevName, CAbMicro800TagsCreator &AbTags, CString &Error)
{
	if( pTags ) {

		CTextStreamMemory Stream;

		CString Path;

		if( PreImport(Stream, Path) ) {

			CStringArray Root;

			AbTags.m_Root.Tokenize(Root, '.');

			if( pTags->InitImport(Root[0], TRUE, Path) ) {

				UINT uRoots = Root.GetCount();

				UINT r;

				for( r = 1; r < uRoots; r++ ) {

					pTags->AddFolder(L"", Root[r]);
					}

				CStringArray Parts;

				CString      Dims;

				UINT         uOffset = 0;

				while( LineImport(Stream, Parts, Dims) ) {

					CCipDesc Desc;

					if( MakeDesc(Desc, Parts, Dims, Error) ) {

						MakeTag(pTags, DevName, Desc, Parts, AbTags, uOffset, Error);

						Dims.Empty();
						}
					}

				PostImport(Stream, TRUE);
								
				for( r = 0; r < uRoots; r++ ) {

					pTags->EndFolder();
					}

				pTags->ImportDone(TRUE);

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Data Acccess

UINT CABMicro800DeviceOptions::FindTableSlot(CAddress const &Addr, CTableSlot &Slot)
{
	UINT uSlots = m_TableSlots.GetCount();

	for( UINT s = 0; s < uSlots; s++ ) {

		CTableSlot Table = m_TableSlots.GetAt(s);

		if( Addr.m_Ref >= Table.m_Slot && Addr.m_Ref < Table.m_Slot + Table.m_Size ) {

			Slot = Table;

			return s;
			}
		}
	
	return NOTHING;
	}

// Implementation

UINT CABMicro800DeviceOptions::GetNextTableSlot(CCipDesc const &Desc)
{
	CTableSlot Slot;

	Slot.m_Slot = 0;

	Slot.m_Key  = CipTypeFromAtomic(Desc.m_CipType);

	Slot.m_Size = GetSize(Desc);

	if( !m_TableSlots.IsEmpty() ) {

		UINT uCount = m_TableSlots.GetCount();

		UINT u;

		for( u = 0; u < uCount; u++ ) {

			CTableSlot Table = m_TableSlots.GetAt(u);

			if( Table.m_Key == Slot.m_Key ) {

				if( Table < Slot ) {

					Slot.m_Slot = Table.m_Slot + Table.m_Size;

					return AddTableSlot(Slot.m_Slot, Table.m_Key, Desc, Slot.m_Size);
					}

				break;
				}
			}

		Slot = m_TableSlots.GetAt(u == m_TableSlots.GetCount() ? 0 : u);
		}

	CAddress Addr;

	Addr.m_Ref = Slot.m_Slot;

	Addr.a.m_Table++;

	if( Addr.a.m_Table < addrNamed ) {

		Addr.a.m_Offset = 0;

		Slot.m_Slot     = Addr.m_Ref;
			        
		Slot.m_Key      = CipTypeFromAtomic(Desc.m_CipType);
			        
		Slot.m_Size     = GetSize(Desc);

		return AddTableSlot(Addr.m_Ref, Slot.m_Key, Desc, Slot.m_Size);
		}

	return NOTHING;
	}

UINT CABMicro800DeviceOptions::GetNextNamedSlot(CCipDesc const &Desc)
{
	CAddress Addr;
		
	Addr.m_Ref = NOTHING;

	if( !m_NamedSlots.IsEmpty() ) {

		Addr.m_Ref = m_NamedSlots.GetAt(m_NamedSlots.GetCount() - 1);
		}

	Addr.a.m_Table = addrNamed;

	Addr.a.m_Type  = GetCipTypeEnum(Desc.m_CipType);

	Addr.a.m_Extra = IsSTR(CipTypeFromAtomic(Desc.m_CipType));
		
	Addr.a.m_Offset++;

	m_NamedSlots.Append(Addr.m_Ref);

	return Addr.m_Ref;
	}

BOOL CABMicro800DeviceOptions::GetOffset(CString Dims, CCipDesc const &Desc, UINT &uOut)
{
	Dims.Remove(L'[');

	Dims.Remove(L']');

	CStringArray List;

	Dims.Tokenize(List, L",");

	UINT uOffset = 0;

	UINT uFactor = IsDouble(CipTypeFromAtomic(Desc.m_CipType)) ? 2 : 1;

	for( int i = Desc.m_Dims.GetCount() - 1; i >= 0; i-- ) {

		UINT uDim = tstrtol(List[i], NULL, 10);

		if( uDim >= Desc.m_Dims[i] ) {

			return FALSE;
			}

		uOffset += uDim * uFactor;
				
		uFactor *= Desc.m_Dims[i];
		}

	uOut = uOffset;

	return TRUE;
	}

UINT CABMicro800DeviceOptions::GetSize(CCipDesc const &Desc)
{
	UINT uType = CipTypeFromAtomic(Desc.m_CipType);

	UINT uSize = IsSTR(uType) ? Desc.m_uChars : IsDouble(uType) ? 2 : 1;

	switch( Desc.m_Dims.GetCount() ) {

			// Fall through is intentional.

			case 3:
				uSize *= Desc.m_Dims[2];	//
			case 2:
				uSize *= Desc.m_Dims[1];	//
			case 1:
				uSize *= Desc.m_Dims[0];	//
			}

	return uSize;
	}

UINT CABMicro800DeviceOptions::AddTableSlot(UINT uSlot, UINT uCip, CCipDesc Desc, UINT uSize)
{
	CAddress Addr;

	Addr.m_Ref = uSlot;

	if( Addr.a.m_Table < addrNamed ) {

		Addr.a.m_Table  = !Addr.a.m_Table ? 1 : Addr.a.m_Table;
			        
		Addr.a.m_Extra  = IsSTR(uCip);
			        
		Addr.a.m_Type   = GetCipTypeEnum(Desc.m_CipType);

		CTableSlot Slot;

		Slot.m_Key      = uCip;

		Slot.m_Size     = uSize;
					        
		Slot.m_Slot     = Addr.m_Ref;

		Slot.m_Desc     = Desc;

		m_TableSlots.Append(Slot);

		m_TableSlots.Sort();

		return Slot.m_Slot;
		}

	return NOTHING;
	}

CString CABMicro800DeviceOptions::ExpandDims(UINT uOffset, CCipDesc const &Desc)
{
	CLongArray Dims = Desc.m_Dims;

	CString Text;

	UINT uCount = Dims.GetCount();

	if( Desc.m_uChars ) {

		uOffset /= Desc.m_uChars;
		}

	if( IsDouble(CipTypeFromAtomic(Desc.m_CipType)) ) {

		uOffset /= 2;
		}

	if( uCount ) {

		Text += L"[";

		switch( uCount - 1 ) {

			case 0:
				Text += CPrintf(L"%d", uOffset % Dims[0]);
				break;

			case 1:
				Text += CPrintf(L"%d", uOffset / Dims[1]);

				Text += L",";

				Text += CPrintf(L"%d", uOffset % Dims[1]);

				break;

			case 2:
				Text += CPrintf(L"%d", uOffset / (Dims[1] * Dims[2]));

				uOffset = uOffset % (Dims[1] * Dims[2]);

				Text += L",";

				Text += CPrintf(L"%d", uOffset / Dims[2]);

				Text += L",";

				Text += CPrintf(L"%d", uOffset % Dims[2]);

				break;
			}

		Text += L"]";
		}

	return Text;
	}

CString CABMicro800DeviceOptions::FindFilename(CString Path)
{
	CString File = Path;

	UINT uBegin = 0;

	while( File.Find('\\') < NOTHING ) {

		uBegin = File.Find('\\');

		File = File.Mid(uBegin + 1);
		}

	uBegin = 0;
			
	while( File.Find('/') < NOTHING ) {

		uBegin = File.Find('/');

		File = File.Mid(uBegin + 1);
		}

	UINT uEnd   = File.Find('.');

	if( uEnd < NOTHING ) {

		File = File.Mid(0, uEnd);
		}

	Validate(File);

	return File;
	}

// Import Help 

BOOL CABMicro800DeviceOptions::PreImport(CTextStreamMemory &Stream, CString &Path)
{
	COpenFileDialog Dlg;

	Dlg.SetFilter(L"CSV Files|*.csv");

	Dlg.LoadLastPath(L"Micro800");

	if( Dlg.Execute() ) {

		CFilename Filename = Dlg.GetFilename();

		Path   = Filename.GetBareName();

		if( TRUE ) {

			if( m_Map.GetCount() > 0 ) {

				CString Message = TEXT("You are about to make changes to the Micro800 device configuration \n")
						  TEXT("that may invalidate data tag references in the database.\n\n")
						  TEXT("If you choose to continue, all parameters will be replaced with \n")
						  TEXT("data from the selected file. You should run the Rebuild Comms \n")
						  TEXT("Blocks utility from the File menu after this operation is complete.\n\n")
						  TEXT("Do you want to continue?");

				if( afxMainWnd->YesNo(Message) == IDNO ) {

					return FALSE;
					}
				}
			}

		if( Stream.LoadFromFile(Filename) ) {

			Dlg.SaveLastPath(L"Micro800");

			m_Map.Empty();
			
			m_NamedSlots.Empty();
			
			m_TableSlots.Empty();

			return TRUE;
			}

		afxMainWnd->Error(IDS_UNABLE_TO_OPEN);
		}
	
	return FALSE;
	}

BOOL CABMicro800DeviceOptions::LineImport(CTextStreamMemory &Stream, CStringArray &Parts, CString &Dims)
{
	TCHAR Buff[1024];

	Parts.Empty();

	while( Stream.GetLine(Buff, 1024) ) {

		CString Line(Buff);
		
		if( Line.StartsWith(L"Name,") ) {

			continue;
			}

		if( Line.Find(L"..") < NOTHING ) {

			UINT uStart = Line.Find(L"[");

			UINT uEnd   = Line.FindRev(']');

			Dims = Line.Mid(uStart, uEnd - uStart + 1);

			Dims.Replace(L',', L'|');

			Line.Remove('[');

			Line.Remove(']');

			Line.Delete(uStart, uEnd - uStart - 1);
			}

		Line.Tokenize(Parts, L',');

		return TRUE;
		}

	return FALSE;
	}

void CABMicro800DeviceOptions::PostImport(CTextStreamMemory &Stream, BOOL fRebuild)
{
	Stream.Close();

	if( fRebuild ) {

		Rebuild();
		}
	}

// Import Operations
		
BOOL CABMicro800DeviceOptions::MakeDesc(CCipDesc &Desc, CStringArray &Parts, CString &Dims, CString &Error)
{
	BOOL fNamed = IsNamed(CipTypeFromAtomic(Parts[partType]), !Dims.IsEmpty());

	if( !Dims.IsEmpty() ) {

		CStringArray DimArray;

		UINT uCount = Dims.Tokenize(DimArray, L'|');

		for( UINT n = 0; n < uCount; n++ ) {

			UINT uFind = NOTHING;

			CString S = DimArray.GetAt(n);

			if( (uFind = S.Find(L"..")) < NOTHING ) {

				UINT uDim   = tstrtol(S.Mid(uFind+2), NULL, 10) + 1;

				Desc.m_Dims.Append(uDim);
				}
			}
		}
	
	UINT uType = TypeFromAtomic(Parts[partType]);

	if( uType != NOTHING ) {

		CString Name = Parts[partName];

		if( IsArrayElement(Name) ) {

			return FALSE;
			}
				
		if( Name.GetLength() <= 40 && IsNameValid(Name) ) {

			Desc.m_Name	 = Name;
			Desc.m_CipType	 = Parts[partType];
			Desc.m_uType	 = uType;
			Desc.m_fNamed	 = fNamed;
			Desc.m_fUsed     = FALSE;
			Desc.m_uChars    = tatoi(Parts[partChars]);

			AddDesc(Desc);

			return TRUE;
			}

		Error += GetErrorText(Name, Name);
		}

	return FALSE;
	}

void CABMicro800DeviceOptions::MakeTag(IMakeTags * pTags, CString DevName, CCipDesc &Desc, CStringArray Parts, CAbMicro800TagsCreator &AbTags, UINT &uOffset, CString &Error)
{	
	CString Name  = AbTags.Construct(Parts, tagName, uOffset);
	
	CString Label = AbTags.Construct(Parts, tagLabel, uOffset);
	
	if( Name.IsEmpty() ) {
		
		Name = Desc.m_Name;
		}
	
	if( Label.IsEmpty() ) {
		
		Label = Name;
		}

	Validate(Name);

	CString Value = L"[" + Name + L"]";

	UINT uWrite   = Parts[partAttr].Find(L"Write") == NOTHING ? 0 : Parts[partAttr].Find(L"Read") == NOTHING ? 1 : 2;

	UINT uExtent  = Desc.m_Dims.GetCount() ? Desc.m_Dims.GetAt(0) : 0;

	UINT uCipType = CipTypeFromAtomic(Desc.m_CipType);

	if( IsDouble(uCipType) ) {

		MakeMax(uExtent, 1);

		uExtent *= 2;
		}
	
	if( IsBOOL(uCipType) ) {

		if( !pTags->AddFlag( Name, Label, Value, uExtent, 0, uWrite, L"False", L"True" ) ) {

			Error += GetErrorText(Name, Desc.m_Name);
			}
		}
	
	else if( IsREAL(uCipType) && !IsDouble(uCipType) ) {
		
		if( !pTags->AddReal( Name, Label, Value, uExtent, uWrite, L"", L"", 0 ) ) {

			Error += GetErrorText(Name, Desc.m_Name);
			}
		}

	else if( IsSTR(uCipType) ) {

		if( !pTags->AddText( Name, Label, Value, Desc.m_uChars, uExtent, uWrite, 0 ) ) {

			Error += GetErrorText(Name, Desc.m_Name);
			}
		}
	else {
		UINT uTreat = !IsSigned(uCipType);

		UINT uDP    = !IsSigned(uCipType) ? NOTHING : 0;

		if( !pTags->AddInt( Name, Label, Value, uExtent, uWrite, L"", L"", uDP, uTreat ) ) {

			Error += GetErrorText(Name, Desc.m_Name);
			}
		}

	if( Error.IsEmpty() ) {

		uOffset++;
		}
	}


// Helpers

CString	CABMicro800DeviceOptions::GetErrorText(CString Name, CString Tag)
{
	CString Error = Tag;

	UINT uLength  = Name == Tag ? 40 : 32;

	if( Error.GetLength() > 35 ) {

		Error = Error.Mid(0, 32);

		Error += CString("...");
		}

	Error += CString("\t");

	if( Name.GetLength() > uLength ) {

		Error += CPrintf(IDS_ERR_TOO_LONG, uLength);
		}
	else {
		if( !IsValid(Name) ) {

			Error += CString(IDS_TAG_NAME_CONTAINS);
			}

		else if( IsDuplicate(Name) ) {

			Error += CPrintf(IDS_ERR_EXIST_AS, Name);
			}
		else {
			Error += CString(IDS_ERR_DATA_TYPE);
			}
		}

	Error += CString("\r\n%c");
	
	Error.Printf(Error, ',');

	return Error;
	}

void CABMicro800DeviceOptions::StripCipPrefix(CString &Name)
{
	if( Name.StartsWith(CIP_PREFIX) ) {

		Name = Name.Mid(CIP_PREFIX.GetLength());
		}
	}

CString CABMicro800DeviceOptions::GetCipType(CString Type)
{
	if( Type.StartsWith(CIP_PREFIX) ) {

		return Type;
		}

	return CIP_PREFIX + Type;
	}

BYTE CABMicro800DeviceOptions::GetCipDefault(void)
{
	return typeDINT;
	}

void CABMicro800DeviceOptions::Validate(CString &String)
{
	UINT uCount = String.GetLength();

	for( UINT u = 0; u < uCount; u++ ) {

		TCHAR c = String.GetAt(u);

		if( u == 0 && isdigit(c) ) {

			String.Replace(c, '_');
			}

		else if( !isalnum(c) || isspace(c) ) {

			String.Replace(c, '_');
			}
		}
	}

BOOL CABMicro800DeviceOptions::IsValid(CString String)
{
	UINT uCount = String.GetLength();

	for( UINT u = 0; u < uCount; u++ ) {

		TCHAR c = String.GetAt(u);

		if( u == 0 && isdigit(c) ) {

			return FALSE;
			}

		if( !isalnum(c) || isspace(c) ) {

			if( c != '.' ) {

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CABMicro800DeviceOptions::IsValid(CCipDesc const &Desc)
{
	return ((Desc.m_uType < NOTHING) && (Desc.m_uIndex < NOTHING));
	}

BOOL CABMicro800DeviceOptions::IsDuplicate(CString Name)
{
	CCipDesc Find;

	Find.m_Name = Name;

	INDEX i = m_Map.FindName(Find);

	return !m_Map.Failed(i);
	}

UINT CABMicro800DeviceOptions::CipTypeFromAtomic(CString const &Type)
{	
	CString Atomic = Type;

	StripCipPrefix(Atomic);

	if( Atomic == CipTypeFromEnum(typeBOOL ) ) return typeBOOL;
	
	if( Atomic == CipTypeFromEnum(typeBYTE ) ) return typeBYTE;
		
	if( Atomic == CipTypeFromEnum(typeINT  ) ) return typeINT;	
	
	if( Atomic == CipTypeFromEnum(typeDINT ) ) return typeDINT;
	
	if( Atomic == CipTypeFromEnum(typeREAL ) ) return typeREAL;
	
	if( Atomic == CipTypeFromEnum(typeSTR  ) ) return typeSTR;
		
	if( Atomic == CipTypeFromEnum(typeSINT ) ) return typeSINT;
	   
	if( Atomic == CipTypeFromEnum(typeUSINT) ) return typeUSINT;
	    
	if( Atomic == CipTypeFromEnum(typeUINT ) ) return typeUINT;
	    
	if( Atomic == CipTypeFromEnum(typeWORD ) ) return typeWORD;
	   
	if( Atomic == CipTypeFromEnum(typeUDINT) ) return typeUDINT;
	    
	if( Atomic == CipTypeFromEnum(typeDWORD) ) return typeDWORD;
	    
	if( Atomic == CipTypeFromEnum(typeTIME ) ) return typeTIME;
	    
	if( Atomic == CipTypeFromEnum(typeDATE ) ) return typeDATE;
	    
	if( Atomic == CipTypeFromEnum(typeLINT ) ) return typeLINT;
	    
	if( Atomic == CipTypeFromEnum(typeULINT) ) return typeULINT;
	    
	if( Atomic == CipTypeFromEnum(typeLWORD) ) return typeLWORD;
	    
	if( Atomic == CipTypeFromEnum(typeLREAL) ) return typeLREAL;

	return NOTHING;
	}

BOOL CABMicro800DeviceOptions::IsSTR(UINT uType)
{
	return uType == typeSTR;
	}

BOOL CABMicro800DeviceOptions::IsTIME(UINT uType)
{
	return uType == typeTIME;
	}

BOOL CABMicro800DeviceOptions::IsDATE(UINT uType)
{
	return uType == typeDATE;
	}

BOOL CABMicro800DeviceOptions::IsBOOL(UINT uType)
{
	return uType == typeBOOL;
	}

BOOL CABMicro800DeviceOptions::IsBYTE(UINT uType)
{
	switch( uType ) {

		case typeBYTE:
		case typeSINT:
		case typeUSINT:
			
			return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::IsWORD(UINT uType)
{
	switch( uType ) {

		case typeINT:
		case typeUINT:
		case typeWORD:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::IsLONG(UINT uType)
{
	switch( uType ) {

		case typeDINT:
		case typeUDINT:
		case typeDWORD:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::IsREAL(UINT uType)
{
	switch( uType ) {

		case typeREAL:
		case typeLREAL:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::IsDouble(UINT uType)
{
	switch( uType ) {

		case typeLINT:
		case typeULINT:
		case typeLWORD:
		case typeLREAL:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::IsSigned(UINT uType)
{
	switch( uType ) {

		case typeSINT:
		case typeINT:
		case typeDINT:
		case typeLINT:

			return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800DeviceOptions::IsNamed(UINT uType, UINT uDims)
{
	return !uDims && !IsSTR(uType) && !IsDouble(uType);
	}

BOOL CABMicro800DeviceOptions::IsArrayElement(CString Name)
{
	if( Name.Find('[') < NOTHING ) {

		return Name.Find(']') < NOTHING;
		}

	return FALSE;
	}

void CABMicro800DeviceOptions::WalkSlots(void)
{
	UINT uSlots = m_TableSlots.GetCount();

	for( UINT s = 0; s < uSlots; s++ ) {

		CTableSlot Slot = m_TableSlots.GetAt(s);

		for( UINT n = s + 1; n < uSlots; n++ ) {

			CTableSlot Next = m_TableSlots.GetAt(n);

			if( Slot == Next ) {

				if( m_Map.FindName(Next.m_Desc) ) {

					CCipDesc Desc;

					Desc.m_CipType = Next.m_Desc.m_CipType;
					Desc.m_Dims    = Next.m_Desc.m_Dims;
					Desc.m_fNamed  = Next.m_Desc.m_fNamed;
					Desc.m_Name    = Next.m_Desc.m_Name;
					Desc.m_uChars  = Next.m_Desc.m_uChars;
					Desc.m_uType   = Next.m_Desc.m_uType;
					Desc.m_fUsed   = Next.m_Desc.m_fUsed;
										
					m_Map.Remove(Next.m_Desc);

					m_TableSlots.Remove(n);

					AddDesc(Desc, Desc.m_uIndex);

					uSlots = m_TableSlots.GetCount();
					}
				}
			}
		}
	}

void CABMicro800DeviceOptions::PrintTags(void)
{
	for( INDEX i = m_Map.GetHead(); !m_Map.Failed(i); m_Map.GetNext(i) ) {

		CCipDesc Desc = m_Map.GetName(i);

		AfxTrace(L"\nTag %s named %u type %s, index %8.8x chars %u used %u", Desc.m_Name,
		                                                                     Desc.m_fNamed,
		                                                                     Desc.m_CipType,
		                                                                     Desc.m_uIndex,
		                                                                     Desc.m_uChars,
		                                                                     Desc.m_fUsed);
		}

	UINT uSlots = m_TableSlots.GetCount();

	for( UINT s = 0; s < uSlots; s++ ) {

		CTableSlot Slot = m_TableSlots.GetAt(s);

		CAddress Addr;

		Addr.m_Ref = Slot.m_Slot;

		AfxTrace(L"\nKey %2.2x Slot %8.8x Size %u table %2.2x", Slot.m_Key,
									Slot.m_Slot,
									Slot.m_Size,
									Addr.a.m_Table);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 TCP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABMicro800TcpDeviceOptions, CABMicro800DeviceOptions);

// Constructor

CABMicro800TcpDeviceOptions::CABMicro800TcpDeviceOptions(void)
{
	m_Addr    = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(  168, 192)));

	m_Timeout = 1000;
	}

// Download Support

BOOL CABMicro800TcpDeviceOptions::MakeInitData(CInitData &Init)
{
	CABMicro800DeviceOptions::MakeInitData(Init);

	Init.AddLong(m_Addr);

	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}
		
// Meta Data Creation

void CABMicro800TcpDeviceOptions::AddMetaData(void)
{
	CABMicro800DeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Serial Device Options
//

AfxImplementDynamicClass(CABMicro800SerialDeviceOptions, CABMicro800DeviceOptions);

// Constructor

CABMicro800SerialDeviceOptions::CABMicro800SerialDeviceOptions(void)
{
	m_Station = 1;

	m_Timeout = 1000;
	}

// Download Support

BOOL CABMicro800SerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CABMicro800DeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_Station));

	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}

// Meta Data Creation

void CABMicro800SerialDeviceOptions::AddMetaData(void)
{
	CABMicro800DeviceOptions::AddMetaData();

	Meta_AddInteger(Timeout);

	Meta_AddInteger(Station);
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CABMicro800Dlg, CStdDialog);

// Message Map

AfxMessageMap(CABMicro800Dlg, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange)

	AfxMessageEnd(CABMicro800Dlg)
	};

// Constructor

CABMicro800Dlg::CABMicro800Dlg(CABMicro800BaseDriver *pDriver, CAddress &Addr, CItem *pConfig, BOOL fPart) : m_Addr(Addr)
{
	m_pConfig = (CABMicro800DeviceOptions *) pConfig;

	m_pDriver = pDriver;
	
	SetName(L"ABMicro800Dlg");
	}

// Message Handlers

BOOL CABMicro800Dlg::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadListBox();

	DoEnables();

	if( m_Addr.m_Ref ) {

		CTableSlot Slot;

		if( m_pConfig->FindTableSlot(m_Addr, Slot) < NOTHING ) {

			ShowAddress(m_Addr.m_Ref - Slot.m_Slot);
			}
		}

	ShowDetails();

	return TRUE;
	}

// Command Handlers

BOOL CABMicro800Dlg::OnOkay(UINT uID)
{
	CString Text = m_Desc.m_Name;

	if( !Text.IsEmpty() ) {

		CError Error;

		UINT uCount = m_Desc.m_Dims.GetCount();

		if( uCount > 0 ) {

			if( uCount == 1 ) {

				Text += CPrintf(L"[%s]", GetDlgItem(2002).GetWindowText());
				}

			else if( uCount == 2) {

				Text += CPrintf(L"[%s, %s]", GetDlgItem(2002).GetWindowText(),
							     GetDlgItem(2003).GetWindowText());
				}

			else if( uCount == 3 ) {

				Text += CPrintf(L"[%s, %s, %s]", GetDlgItem(2002).GetWindowText(),
								 GetDlgItem(2003).GetWindowText(),
								 GetDlgItem(2004).GetWindowText());
				}
			}

		if( m_pDriver->ParseAddress(Error, m_Addr, m_pConfig, Text) ) {

			EndDialog(TRUE);

			return TRUE;
			}
	
		if( !Error.IsOkay() ) {

			Error.Show(ThisObject);
			}
		}
	else {
		EndDialog(FALSE);
		}

	return FALSE;
	}

void CABMicro800Dlg::OnSelChange(UINT uId, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CString Text = ListBox.GetText(ListBox.GetCurSel());
	
	CCipDesc Find;

	Find.m_Name = Text;

	// cppcheck-suppress uninitStructMember

	INDEX i = m_pConfig->m_Map.FindName(Find);

	if( !m_pConfig->m_Map.Failed(i) ) {

		CCipDesc Desc = m_pConfig->m_Map.GetName(i);

		m_Desc = Desc;

		ShowDetails();

		DoEnables();

		ShowAddress(0);
		}
	}

// Implementation

void CABMicro800Dlg::LoadListBox(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	CDescMap const &Map = m_pConfig->m_Map;

	for( INDEX i = Map.GetHead(); !Map.Failed(i); Map.GetNext(i) ) {

		CCipDesc Desc = Map.GetName(i);

		ListBox.AddString(Desc.m_Name);

		if( m_Addr.m_Ref ) {

			if( Desc.m_uIndex == m_Addr.m_Ref ) {

				m_Desc = Desc;
				}
			}
		}

	if( m_Desc.m_Name.IsEmpty() ) {

		CTableSlot Slot;

		if( m_pConfig->FindTableSlot(m_Addr, Slot) < NOTHING ) {

			INDEX i = Map.FindData(Slot.m_Slot);

			m_Desc  = Map.GetName(i);
			}
		}

	if( !m_Desc.m_Name.IsEmpty() ) {

		ListBox.SelectString(0, m_Desc.m_Name);
		}
	}

CString CABMicro800Dlg::GetTypeString(UINT uType)
{
	return m_pConfig->GetTypeString(uType);
	}

void CABMicro800Dlg::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(GetTypeString(m_Desc.m_uType));

	GetDlgItem(3008).SetWindowText(m_pConfig->CipTypeFromEnum(m_pConfig->CipTypeFromAtomic(m_Desc.m_CipType)));

	CLongArray Dims = m_Desc.m_Dims;

	UINT uCount = Dims.GetCount();

	if( uCount > 0 ) {

		CString Min, Max;

		CString Name = m_Desc.m_Name;

		if( uCount == 1 ) {

			Min = Name + L"[0]";

			Max = Name + CPrintf("[%u]", Dims[0]-1);
			}

		else if( uCount == 2 ) {

			Min = Name + L"[0, 0]";

			Max = Name + CPrintf("[%u, %u]", Dims[0]-1, Dims[1]-1);
			}

		else if( uCount == 3 ) {

			Min = Name + L"[0, 0, 0]";

			Max = Name + CPrintf("[%u, %u, %u]", Dims[0]-1, Dims[1]-1, Dims[2]-1);
			}

		GetDlgItem(3004).SetWindowText(Min);

		GetDlgItem(3006).SetWindowText(Max);
		}
	else {
		GetDlgItem(3004).SetWindowText(L"");

		GetDlgItem(3006).SetWindowText(L"");
		}
	}

void CABMicro800Dlg::DoEnables(void)
{
	GetDlgItem(2002).EnableWindow(FALSE);
	GetDlgItem(2003).EnableWindow(FALSE);
	GetDlgItem(2004).EnableWindow(FALSE);

	switch( m_Desc.m_Dims.GetCount() ) {

	// Fall through is intentional.

		case 3:
			GetDlgItem(2004).EnableWindow(TRUE);
		case 2:
			GetDlgItem(2003).EnableWindow(TRUE);
		case 1:
			GetDlgItem(2002).EnableWindow(TRUE);
			break;
		}
	}

void CABMicro800Dlg::ShowAddress(UINT uOffset)
{
	GetDlgItem(2002).SetWindowText(L"");

	GetDlgItem(2003).SetWindowText(L"");

	GetDlgItem(2004).SetWindowText(L"");

	GetDlgItem(2001).SetWindowText(m_Desc.m_Name);

	CLongArray Dims = m_Desc.m_Dims;

	UINT uCount = Dims.GetCount();

	if( uCount ) {

		if( m_Desc.m_uChars ) {

			uOffset /= m_Desc.m_uChars;
			}

		if( m_pConfig->IsDouble(m_pConfig->CipTypeFromAtomic(m_Desc.m_CipType)) ) {

			uOffset /= 2;
			}

		switch( uCount ) {

			case 1:
				GetDlgItem(2002).SetWindowText(CPrintf(L"%d", uOffset % Dims[0]));

				break;

			case 2:
				GetDlgItem(2002).SetWindowText(CPrintf(L"%d", uOffset / Dims[1]));

				GetDlgItem(2003).SetWindowText(CPrintf(L"%d", uOffset % Dims[1]));

				break;

			case 3:
				GetDlgItem(2002).SetWindowText(CPrintf(L"%d", uOffset / (Dims[1] * Dims[2])));

				uOffset = uOffset % (Dims[1] * Dims[2]);

				GetDlgItem(2003).SetWindowText(CPrintf(L"%d", uOffset / Dims[2]));

				GetDlgItem(2004).SetWindowText(CPrintf(L"%d", uOffset % Dims[2]));

				break;
			}
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Management Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CABMicro800ManageDlg, CStdDialog);

// Message Map

AfxMessageMap(CABMicro800ManageDlg, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(4001, OnDel)
	AfxDispatchCommand(5001, OnCreate)

	AfxDispatchNotify(1001, LBN_SELCHANGE, OnSelChange)
	AfxDispatchNotify(5003, BN_CLICKED,    OnCheckChange)
	AfxDispatchNotify(5004, EN_CHANGE,     OnEditChange)

	AfxMessageEnd(CABMicro800Dlg)
	};


// Constructor

CABMicro800ManageDlg::CABMicro800ManageDlg(CABMicro800DeviceOptions *pConfig)
{
	m_pConfig = pConfig;

	SetName(L"ABMicro800ManageDlg");
	}

// Message Handlers

BOOL CABMicro800ManageDlg::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadListBox();

	LoadCombo();

	CSpinner &Spinner = (CSpinner &) GetDlgItem(5005);

	Spinner.SetRange(1, 3);

	Spinner.SetPos(1);

	DoEnables();

	ShowDetails();

	return TRUE;
	}

// Command Handlers

BOOL CABMicro800ManageDlg::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CABMicro800ManageDlg::OnCreate(UINT uID)
{
	CString Name("NewTag");

	CStringDialog Dlg(CString("Create Tag"), CString("Tag Name"), Name);

	Dlg.SetLimit(40);

	if( Dlg.Execute(ThisObject) ) {

		Name = Dlg.GetData();
		}
	else {
		return TRUE;
		}

	if( !m_pConfig->IsNameValid(Name) ) {

		afxMainWnd->Error(CString(IDS_TAG_NAME_CONTAINS));

		return FALSE;
		}

	CString Type = GetDlgItem(5002).GetWindowText();

	CCipDesc Desc;

	Desc.m_Name	 = Name;
	Desc.m_CipType	 = Type;
	Desc.m_uType	 = m_pConfig->TypeFromAtomic(Type);
	Desc.m_fUsed     = FALSE;
	Desc.m_uChars    = m_pConfig->IsSTR(m_pConfig->CipTypeFromAtomic(Type)) ? 80 : 0;

	CButton &Button = (CButton &) GetDlgItem(5003);

	UINT uDims      = 0;

	if( Button.IsChecked() ) {

		uDims = tstrtol(GetDlgItem(5004).GetWindowText(), 0, 10);

		if( uDims >= 1 ) {

			UINT uDim = tstrtol(GetDlgItem(5006).GetWindowText(), 0, 10) + 1;

			Desc.m_Dims.Append(uDim);
			}

		if( uDims >= 2 ) {

			UINT uDim = tstrtol(GetDlgItem(5007).GetWindowText(), 0, 10) + 1;

			Desc.m_Dims.Append(uDim);
			}

		if( uDims == 3 ) {

			UINT uDim = tstrtol(GetDlgItem(5008).GetWindowText(), 0, 10) + 1;

			Desc.m_Dims.Append(uDim);
			}
		}

	Desc.m_fNamed = m_pConfig->IsNamed(m_pConfig->CipTypeFromAtomic(Type), uDims);

	if( m_pConfig->AddDesc(Desc) != NOTHING ) {

		LoadListBox();
	
		return TRUE;
		}

	return FALSE;
	}

BOOL CABMicro800ManageDlg::OnDel(UINT uID)
{
	CTableSlot Slot;

	Slot.m_Slot = m_Desc.m_uIndex;

	Slot.m_Key  = m_pConfig->CipTypeFromAtomic(m_Desc.m_CipType);

	m_pConfig->RemoveDesc(Slot, m_Desc.m_fNamed);

	CListBox &List = (CListBox &) GetDlgItem(1001);

	UINT uPos = List.GetCurSel();

	LoadListBox();

	List.SetCurSel(uPos ? uPos - 1 : 0);

	OnSelChange(uID, ThisObject);

	return TRUE;
	}

void CABMicro800ManageDlg::OnSelChange(UINT uId, CWnd &Wnd)
{
	CListBox &List = (CListBox &) GetDlgItem(1001);

	UINT uIndex = List.GetCurSelData();

	CDescMap const &Map = m_pConfig->m_Map;

	INDEX i = Map.FindData(uIndex);

	if( !Map.Failed(i) ) {

		m_Desc = Map.GetName(i);
		}

	ShowDetails();
	}

void CABMicro800ManageDlg::OnCheckChange(UINT uId, CWnd &Wnd)
{
	DoEnables();

	ShowDims();
	}

void CABMicro800ManageDlg::OnEditChange(UINT uId, CWnd &Wnd)
{
	CString Text = GetDlgItem(5004).GetWindowText();

	UINT uVal = tstrtol(Text, NULL, 10);

	if( uVal < 1 ) {

		uVal = 1;
		}

	if( uVal > 3 ) {

		uVal = 3;
		}

	GetDlgItem(5004).SetWindowText(CPrintf("%u", uVal));

	DoEnables();

	ShowDims();
	}

// Implementation

void CABMicro800ManageDlg::LoadListBox(void)
{
	CListBox &List = (CListBox &) GetDlgItem(1001);

	List.ResetContent();

	CDescMap const &Map = m_pConfig->m_Map;

	for( INDEX i = Map.GetHead(); !Map.Failed(i); Map.GetNext(i) ) {

		CCipDesc const &Desc = Map.GetName(i);

		List.AddString(Desc.m_Name, Desc.m_uIndex);
		}
	}

void CABMicro800ManageDlg::LoadCombo(void)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(5002);

	for( UINT n = 0; n < elements(CIP_TYPES); n++ ) {

		Combo.AddString(m_pConfig->CipTypeFromEnum(n));
		}

	Combo.SetCurSel(0);
	}

CString CABMicro800ManageDlg::GetTypeString(UINT uType)
{
	return m_pConfig->GetTypeString(uType);
	}

void CABMicro800ManageDlg::DoEnables(void)
{
	DisableArrays();

	CButton &Button = (CButton &) GetDlgItem(5003);

	if( Button.IsChecked() ) {

		GetDlgItem(5004).EnableWindow(TRUE);
		GetDlgItem(5005).EnableWindow(TRUE);

		CSpinner &Spinner = (CSpinner &) GetDlgItem(5005);

		UINT uPos = Spinner.GetPos();

		switch( uPos ) {

			// Fall through is intentional.

			case 3:
				GetDlgItem(5008).EnableWindow(TRUE);
			case 2:
				GetDlgItem(5007).EnableWindow(TRUE);
			case 1:
				GetDlgItem(5006).EnableWindow(TRUE);

				break;
			}
		}
	}

void CABMicro800ManageDlg::ShowDetails(void)
{
	GetDlgItem(3002).SetWindowText(GetTypeString(m_Desc.m_uType));

	GetDlgItem(3008).SetWindowText(m_pConfig->CipTypeFromEnum(m_pConfig->CipTypeFromAtomic(m_Desc.m_CipType)));

	CLongArray Dims = m_Desc.m_Dims;

	UINT uCount = Dims.GetCount();

	if( uCount > 0 ) {

		CString Min, Max;

		CString Name = m_Desc.m_Name;

		if( uCount == 1 ) {

			Min = Name + L"[0]";

			Max = Name + CPrintf("[%u]", Dims[0]-1);
			}

		else if( uCount == 2 ) {

			Min = Name + L"[0, 0]";

			Max = Name + CPrintf("[%u, %u]", Dims[0]-1, Dims[1]-1);
			}

		else if( uCount == 3 ) {

			Min = Name + L"[0, 0, 0]";

			Max = Name + CPrintf("[%u, %u, %u]", Dims[0]-1, Dims[1]-1, Dims[2]-1);
			}

		GetDlgItem(3004).SetWindowText(Min);

		GetDlgItem(3006).SetWindowText(Max);
		}
	else {
		GetDlgItem(3004).SetWindowText(L"");

		GetDlgItem(3006).SetWindowText(L"");
		}
	}

void CABMicro800ManageDlg::ShowDims(void)
{
	GetDlgItem(5006).SetWindowText(L"");
	GetDlgItem(5007).SetWindowText(L"");
	GetDlgItem(5008).SetWindowText(L"");

	CString Text = GetDlgItem(5004).GetWindowText();

	UINT uCount = tstrtol(Text, NULL, 10);

	switch( uCount ) {

		// Fall through is intentional.

		case 3:
			GetDlgItem(5008).SetWindowText(L"1");
		case 2:
			GetDlgItem(5007).SetWindowText(L"1");
		case 1:
			GetDlgItem(5006).SetWindowText(L"1");

			break;
		}
	}

void CABMicro800ManageDlg::DisableArrays(void)
{
	for( UINT n = 5004; n <= 5008; n++ ) {

		GetDlgItem(n).EnableWindow(FALSE);
		}
	}

/////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Data Tag Create Helper
//

// Dynamic Class

AfxImplementDynamicClass(CAbMicro800TagsCreator, CUIItem);

// Constructor

CAbMicro800TagsCreator::CAbMicro800TagsCreator(void)
{	       
	CAbMicro800TagsCreator(L"_");
   	}

CAbMicro800TagsCreator::CAbMicro800TagsCreator(CString Name)
{
	m_Root	 = L"Tags" + Name;

	m_Define = 0;

	SetDefaults();

	UpdateFields(m_Name);

	AddMeta();
   	}

// UI Managament

void CAbMicro800TagsCreator::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || (Tag != "Root" && Tag != "Define") ) {

		UINT uType = pItem->GetDataAccess("Define")->ReadInteger(pItem);

		UINT uMask = GetFieldMask(m_Tag,     partName)  |
			     GetFieldMask(m_Type,    partType)  |
			     GetFieldMask(m_Comment, partNote)  |
			     GetFieldMask(m_Alias,   partAlias) ;
			    
		if( uType == tagLabel ) 

			m_Label = uMask;
		else 
			m_Name  = uMask;

		UpdateFields(uMask);

		pHost->UpdateUI();
		}

	if( Tag == "Define" ) {

		UINT uValue = pItem->GetDataAccess("Define")->ReadInteger(pItem) ? m_Label : m_Name;

		UpdateFields(uValue);

		pHost->UpdateUI();
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistance

void CAbMicro800TagsCreator::GetTagConfig(CItem * pItem)
{
	if( pItem ) {
		
		m_Name  = pItem->GetDataAccess("Name" )->ReadInteger(pItem);

		m_Label = pItem->GetDataAccess("Label")->ReadInteger(pItem);

		CString Root = pItem->GetDataAccess("Root" )->ReadString (pItem);

		if( !Root.IsEmpty() ) {

			m_Root = Root;
			}

		UpdateFields(m_Name);

		return;
		}

	SetDefaults();
	}

void CAbMicro800TagsCreator::SaveTagConfig(CItem * pItem)
{	 
	if( pItem ) {

		pItem->GetDataAccess("Name" )->WriteInteger(pItem, m_Name);

		pItem->GetDataAccess("Label")->WriteInteger(pItem, m_Label);

		pItem->GetDataAccess("Root" )->WriteString (pItem, m_Root);
		}
	}

// UI Management

CViewWnd * CAbMicro800TagsCreator::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		CUIPageList *pList = New CUIPageList;
		
		pList->Append(New CUIStdPage(AfxPointerClass(this)));
		
		pList->Append(New CAbMicro800TagsCreatorPage());

		CUIViewWnd *pView = New CUIItemViewWnd(pList);

		pView->SetBorder(6);
		
		return pView;
		}
	
	return NULL;
	}

// Access

CString CAbMicro800TagsCreator::Construct(CStringArray Parts, UINT uEnum, UINT uOffset)
{
	CString Result;

	UINT uMask = uEnum == tagName ? m_Name : m_Label;

	UINT uPart = partName;
	
	while( uMask ) {

		if( uMask & 0x01 ) {

			Result += Parts[uPart];
			}

		uPart++;

		uMask >>= 1;
		}

	if( Result.IsEmpty() ) {

		if( uEnum == tagName ) {

			Result.Printf(L"%s_%u", m_Root, uOffset);
			}
		else {
			return Construct(Parts, tagName, uOffset);
			}
		}

	return Result;
	}

// Implementation

void CAbMicro800TagsCreator::SetDefaults(void)
{
	m_Name   = GetFieldMask(1, partName);

	m_Label  = GetFieldMask(1, partAlias);
	}

void CAbMicro800TagsCreator::UpdateFields(UINT uValue)
{	
	m_Tag     = (uValue & GetFieldMask(1, partName));
		  			  
	m_Type    = (uValue & GetFieldMask(1, partType));
					  
	m_Comment = (uValue & GetFieldMask(1, partNote));

	m_Alias	  = (uValue & GetFieldMask(1, partAlias));
	}

UINT CAbMicro800TagsCreator::GetFieldMask(UINT uValue, UINT uField)
{
	return (uValue << uField);
	}

// Meta Data Creation

void CAbMicro800TagsCreator::AddMetaData(void)	
{
	CUIItem::AddMetaData();		  

	Meta_AddString(Root);

	Meta_AddInteger(Define);
	
	Meta_AddInteger(Tag);

	Meta_AddInteger(Type);

	Meta_AddInteger(Comment);

	Meta_AddInteger(Alias);

	Meta_SetName(IDS_AB_TAG);
	}

/////////////////////////////////////////////////////////////////////////
//
// AB Tag Creator Page
//

// Runtime Class

AfxImplementRuntimeClass(CAbMicro800TagsCreatorPage, CUIStdPage);

// Constructor

CAbMicro800TagsCreatorPage::CAbMicro800TagsCreatorPage()
{
	}

// Operations

BOOL CAbMicro800TagsCreatorPage::LoadIntoView(IUICreate *pView, CItem *pItem) {

	pView->StartGroup(CString(IDS_INFORMATION), 1);

	CString Text;
	
	Text += CString(IDS_ONLY_THOSE_TAG);
	Text += CString(IDS_WILL_BE_USED_TO);

	pView->AddNarrative(Text);

	pView->EndGroup(FALSE);

	return FALSE;
	};

//////////////////////////////////////////////////////////////////////////
//
// AB Micro800 Import Error List
//

// Runtime Class

AfxImplementRuntimeClass(CABMicro800ImportErrorDialog, CStdDialog);
		
// Constructor

CABMicro800ImportErrorDialog::CABMicro800ImportErrorDialog(CStringArray List)
{
	m_Errors = List;

	SetName(L"Micro800ImportErrorDlg");
	}

// Message Map

AfxMessageMap(CABMicro800ImportErrorDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1003, OnCopy)
	
	AfxMessageEnd(CABMicro800ImportErrorDialog)
	};
 
// Message Handlers

BOOL CABMicro800ImportErrorDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadErrorList();
		
	GetDlgItem(1002).SetWindowText(CString(IDS_ERR_IMPORT));

	return TRUE;
	}

// Command Handlers

BOOL CABMicro800ImportErrorDialog::OnOkay(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CABMicro800ImportErrorDialog::OnCopy(UINT uID)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(L"Micro800");

	Dlg.SetCaption(CPrintf("Micro800 Import Errors"));

	Dlg.SetFilter(TEXT("TEXT Files (*.txt)|*.txt|CSV Files (*.csv)|*.csv"));

	if( Dlg.Execute(*afxMainWnd) ) {

		FILE *pFile;

		CString Text = "";
		
		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			Text = CString(IDS_UNABLE_TO_OPEN_2);

			afxMainWnd->Error(Text);
			}
		else {
			CString Path = Dlg.GetFilename();
			
			CString Error = "";

			BOOL fCSV = Dlg.GetFilename().EndsWith(TEXT(".csv"));
			
			UINT uCount = m_Errors.GetCount();

			AfxAssume(pFile);

			for( UINT u = 0; u < uCount; u++ ) {

				CString Text = m_Errors.GetAt(u);  

				if( fCSV ) {

					Text.Replace('\t', ',');

					Text.Remove('\r');
					}

				fprintf(pFile, TEXT("%s"), PCTXT(Text));
				}

			Information(CString(IDS_COPY_IS_COMPLETE));

			fclose(pFile);
			}
		}

	return TRUE;
	}

// Implementation

void CABMicro800ImportErrorDialog::LoadErrorList(void)
{
	if( !m_Errors.IsEmpty() ) {

		CListBox &Box = (CListBox &) GetDlgItem(1001);

		Box.ResetContent();

		int nTab[] = { 150 };

		Box.SetTabStops(elements(nTab), nTab);

		Box.AddStrings(m_Errors);
		}
	}

// End of File
