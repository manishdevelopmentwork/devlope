
#include "Intern.hpp"

#include "Mutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mutex Object
//

// Instantiators

AfxStdInstantiator(IMutex, Mutex);

// Registration

global void Register_Mutex(void)
{
	piob->RegisterInstantiator("exec.mutex", Create_Mutex);
}

// Constructor

CMutex::CMutex(void)
{
	m_hSync  = win32::CreateMutex(NULL, FALSE, NULL);

	m_uCount = 0;

	m_pOwner = NULL;
}

// Destructor

CMutex::~CMutex(void)
{
	AfxAssert(!m_pOwner);
}

// IUnknown

HRESULT CMutex::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IMutex);

	return CWaitable::QueryInterface(riid, ppObject);
}

ULONG CMutex::AddRef(void)
{
	return CWaitable::AddRef();
}

ULONG CMutex::Release(void)
{
	return CWaitable::Release();
}

// IWaitable

PVOID CMutex::GetWaitable(void)
{
	return CWaitable::GetWaitable();
}

BOOL CMutex::Wait(UINT uWait)
{
	if( m_pOwner == CExecThread::m_pThread ) {

		m_uCount++;

		return TRUE;
	}

	if( CWaitable::Wait(uWait, TRUE) ) {

		m_uCount = 1;

		m_pOwner = CExecThread::m_pThread;

		ListAppend();

		return TRUE;
	}

	return FALSE;
}

BOOL CMutex::HasRequest(void)
{
	return CWaitable::HasRequest();
}

// IMutex

void CMutex::Free(void)
{
	if( m_pOwner == CExecThread::m_pThread ) {

		if( !--m_uCount ) {

			ListRemove();

			m_pOwner = NULL;

			win32::ReleaseMutex(m_hSync);
		}

		return;
	}

	AfxAssert(!m_pOwner);
}

// Task Teardown

void CMutex::FreeMutexList(CExecThread *pThread)
{
	if( pThread->m_pOwnedHead ) {

		AfxTrace("exec: thread %u exiting with owned mutexes\n", pThread->GetIdent());

		while( pThread->m_pOwnedHead ) {

			pThread->m_pOwnedHead->Free();
		}
	}
}

// Implementation

void CMutex::ListAppend(void)
{
	AfxListAppend(m_pOwner->m_pOwnedHead,
		      m_pOwner->m_pOwnedTail,
		      this,
		      m_pNext,
		      m_pPrev
	);
}

void CMutex::ListRemove(void)
{
	AfxListRemove(m_pOwner->m_pOwnedHead,
		      m_pOwner->m_pOwnedTail,
		      this,
		      m_pNext,
		      m_pPrev
	);
}

// End of File
