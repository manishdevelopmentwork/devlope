
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FilingSystem_HPP

#define	INCLUDE_FilingSystem_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File System
//

class CFilingSystem : public IFilingSystem
{
	public:
		// Constructor
		CFilingSystem(UINT iDisk, UINT iVolume);

		// Destructor
		virtual ~CFilingSystem(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Attributes
		UINT  GetIdent(void);
		UINT  GetIndex(void);
		UINT  GetDisk(void);
		CHAR  GetDrive(void);
		INT64 GetSize(void);
		INT64 GetFree(void);
		INT64 GetUsed(void);

		// Methods
		BOOL Init(void);
		BOOL Start(void);
		BOOL Stop(void);
		BOOL IsMounted(void);
		BOOL WaitLock(UINT uWait);
		BOOL FreeLock(void);

		// Enumeration
		BOOL   GetRoot(FSIndex &Index);
		BOOL   GetFirst(FSIndex &Index);
		BOOL   GetNext(FSIndex &Index);
		BOOL   GetName(FSIndex const &Index, CFilename &Name);
		BOOL   GetPath(FSIndex const &Index, CFilename &Name);
		time_t GetUnixTime(FSIndex const &Index);
		UINT   GetSize(FSIndex const &Index);
		BOOL   IsRoot(FSIndex const &Index);
		BOOL   IsValid(FSIndex const &Index);
		BOOL   IsDirectory(FSIndex const &Index);
		BOOL   IsFile(FSIndex const &Index);
		BOOL   IsVolume(FSIndex const &Index);
		BOOL   IsSpecial(FSIndex const &Index);
		BOOL   GetArchive(FSIndex const &Index);
		
		// Directory
		BOOL LocateDir(CFilename const &Name, FSIndex &Index);
		BOOL LocateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL CreateDir(CFilename const &Name);
		BOOL CreateDir(CFilename const &Name, FSIndex const &Dir);
		BOOL CreateDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL ChangeDir(CFilename const &Name);
		BOOL ChangeDir(CFilename const &Name, FSIndex const &Dir);
		BOOL ChangeDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL ChangeDir(FSIndex const &Dir, FSIndex &Index);
		BOOL MoveDir(CFilename const &Name, FSIndex &Index);
		BOOL MoveDir(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		BOOL MoveDir(FSIndex const &Dir, FSIndex &Index);
		UINT ScanDir(FSIndex *&pList, UINT uFlags);
		UINT ScanDir(CFilename const &Name, FSIndex *&pList, UINT uFlags);
		UINT ScanDir(FSIndex const &Dir, FSIndex *&pList, UINT uFlags);
		BOOL RenameDir(CFilename const &From, CFilename const &To);
		BOOL RenameDir(CFilename const &Name, FSIndex &Index); 
		BOOL EmptyDir(CFilename const &Name);
		BOOL EmptyDir(CFilename const &Name, FSIndex const &Dir);
		BOOL EmptyDir(FSIndex const &Index);
		BOOL RemoveDir(CFilename const &Name);
		BOOL RemoveDir(FSIndex const &Index);
		UINT ParentDir(FSIndex const &Index);
		BOOL CurrentDir(FSIndex &Index);

		// File
		BOOL   LocateFile(CFilename const &Name, FSIndex &Index);
		BOOL   LocateFile(CFilename const &Name, FSIndex const &Dir, FSIndex &Index);
		HFILE  CreateFile(CFilename const &Name);
		HFILE  CreateFile(CFilename const &Name, FSIndex const &Dir);
		HFILE  OpenFile(CFilename const &Name, UINT Mode);
		HFILE  OpenFile(CFilename const &Name, FSIndex const &Dir, UINT Mode);
		HFILE  OpenFile(FSIndex const &Index, UINT Mode);
		BOOL   CloseFile(HFILE hFile);
		BOOL   DeleteFile(CFilename const &Name);
		BOOL   DeleteFile(CFilename const &Name, FSIndex const &Dir);
		BOOL   DeleteFile(FSIndex const &Index);
		BOOL   DeleteFile(HFILE hFile);
		BOOL   RenameFile(CFilename const &From, CFilename const &To);
		BOOL   RenameFile(HFILE hFile, CFilename const &Name);
		BOOL   FileSetLength(HFILE hFile, UINT uSize);
		UINT   FileGetLength(HFILE hFile);
		BOOL   FileSetUnixTime(HFILE hFile, time_t time);
		time_t FileGetUnixTime(HFILE hFile);
		BOOL   FileGetIndex(HFILE hFile, FSIndex &Index);
		BOOL   FileAllocData(HFILE hFile, UINT uSize);
		UINT   FileWriteData(HFILE hFile, PCBYTE pData, UINT uOffset, UINT uCount);
		UINT   FileReadData(HFILE hFile, PBYTE pData, UINT uOffset, UINT uCount);

		// Synchronisation
		BOOL WaitReadLock(FSIndex const &Index);
		BOOL WaitWriteLock(FSIndex const &Index);
		BOOL FreeReadLock(FSIndex const &Index);
		BOOL FreeWriteLock(FSIndex const &Index);

		// Debugging
		UINT DebugSetMask(UINT Mask);

	protected:
		// Constants
		enum
		{
			constMaxHandles	= 256
			};

		// Debug Mask
		enum
		{
			debugNone	= 0x00000000,
			debugErr	= Bit(0),
			debugWarn	= Bit(1),
			debugInfo	= Bit(2),
			debugFile	= Bit(3),
			debugDev	= Bit(4),
			debugSync	= Bit(5),
			debugApi	= Bit(6),
			debugData	= Bit(7),
			debugHandle	= Bit(8),
			debugDebug	= Bit(9),
			debugAll	= 0xFFFFFFFF,
			};

		// Handle
		struct CHandle
		{
			INT	          m_nRef;
			FSIndex	          m_Index;
			UINT volatile     m_uOwner;
			INT  volatile     m_nLockRead;
			INT  volatile     m_nLockWrite;
			ISemaphore	* m_pSemaphore;
			IEvent		* m_pRead;
			IMutex		* m_pMutex;
			void		* m_pObject;
			};

		// Data Sector
		struct CDataSector
		{
			UINT	 m_uSector;
			UINT	 m_uOffset;
			};

		// Sort Data
		struct CSort
		{
			UINT	 m_iIndex;
			time_t   m_time;
			};

		// Data
		ULONG	         m_uRefs;
		IDiskManager   * m_pDiskMan;
		IVolumeManager * m_pVolMan;
		IBlockCache    * m_pCache;
		IBlockDevice   * m_pBlock;
		IMutex         * m_pMutex;
		INT              m_nRef;
		UINT		 m_uSector;
		BOOL	         m_fActive;
		UINT		 m_uIdent;
		UINT             m_iVolume;
		CHAR		 m_cDrive;
		CHandle          m_Handle[constMaxHandles];
		UINT             m_iHost;
		UINT             m_iDisk;
		UINT             m_uFirst;
		UINT             m_uSize;
		UINT	         m_Debug;

		// Task Data
		bool FindTaskDir(FSIndex &Index);
		bool LoadTaskDir(FSIndex &Index);
		bool SaveTaskDir(FSIndex const &Index);

		// Device Access
		PBYTE LockSector(UINT uSector, BOOL fSkipRead);
		BOOL  CommitSector(UINT uSector);
		BOOL  UnlockSector(UINT uSector, BOOL fDirty);
		BOOL  ReadSector(UINT uSector, PBYTE pData);
		BOOL  WriteSector(UINT uSector, PCBYTE pData);
		BOOL  ZeroSector(UINT uSector);
		BOOL  ReadSector(UINT uSector, PBYTE pData, UINT uCount);
		BOOL  WriteSector(UINT uSector, PCBYTE pData, UINT uCount);
		BOOL  ZeroSector(UINT uSector, UINT uCount);
		BOOL  ReadSector(UINT uSector, PBYTE pData, UINT uOffset, UINT uCount);
		BOOL  WriteSector(UINT uSector, PCBYTE pData, UINT uOffset, UINT uCount);
		BOOL  ZeroSector(UINT uSector, UINT uOffset, UINT uCount);
		BOOL  ReadSector(CDataSector &Sector, PBYTE pData, UINT uCount);
		BOOL  WriteSector(CDataSector &Sector, PCBYTE pData, UINT uCount);
		BOOL  ZeroSector(CDataSector &Sector, UINT uCount);

		// Disk Access
		BOOL DiskWaitLock(UINT uLock);
		BOOL DiskFreeLock(UINT uLock);
		BOOL DiskWaitReadLock(void);
		BOOL DiskWaitWriteLock(void);
		BOOL DiskFreeReadLock(void);
		BOOL DiskFreeWriteLock(void);

		// Object Access
		BOOL WaitReadLock(HFILE hFile);
		BOOL WaitWriteLock(HFILE hFile);
		BOOL FreeReadLock(HFILE hFile);
		BOOL FreeWriteLock(HFILE hFile);

		// Handles
		void  InitHandles(void);
		void  KillHandles(void);
		HFILE OpenHandle(HFILE hFile);
		HFILE OpenHandle(UINT iCluster);
		HFILE OpenHandle(FSIndex const &Index);
		HFILE FindHandle(FSIndex const &Index);
		HFILE AllocHandle(FSIndex const &Index);
		void  CloseHandle(HFILE hFile);

		// Overrideables
		virtual void OnInitHandle(HFILE hFile);
		virtual void OnCloseHandle(HFILE hFile);

		// Sort
		void SortList(FSIndex *pList, UINT uCount, UINT uFlags);

		// Sort Function
		static int SortFunc1(const void *p1, const void *p2);
		static int SortFunc2(const void *p1, const void *p2);

		// Diagnostics
		void Debug(UINT Mask, PCTXT pFormat, ...) const;
	};

// End of File

#endif

