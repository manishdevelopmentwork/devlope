
#include "Intern.hpp"

#include "LinuxFileSupport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File System Support
//

// Instantiator

IUnknown * Create_FileSupport(void)
{
	return (IFileSupport *) New CLinuxFileSupport;
}

// Constructor

CLinuxFileSupport::CLinuxFileSupport(void)
{
	m_HostSep  = '/';

	m_HostBase = "\\vap\\opt\\crimson\\";

	m_HostRoot = "\\vap\\opt\\crimson\\disk\\";

	MkDir("C:\\");
}

// IUnknown

HRESULT CLinuxFileSupport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IFileUtilities);

	return CBaseFileSupport::QueryInterface(riid, ppObject);
}

ULONG CLinuxFileSupport::AddRef(void)
{
	return CBaseFileSupport::AddRef();
}

ULONG CLinuxFileSupport::Release(void)
{
	return CBaseFileSupport::Release();
}

// IFileUtilities

BOOL CLinuxFileSupport::IsDiskMounted(char cDrive)
{
	CPrintf Path("/vap/opt/crimson/disk/%c", toupper(cDrive));

	struct stat s;

	if( _stat(Path, &s) == 0 ) {

		return TRUE;
	}

	return FALSE;
}

UINT CLinuxFileSupport::GetDiskStatus(char cDrive)
{
	CPrintf Path("/vap/opt/crimson/disk/%c", toupper(cDrive));

	struct stat s;

	if( _stat(Path, &s) == 0 ) {

		return 5;
	}

	return 0;
}

DWORD CLinuxFileSupport::GetDiskIdent(char cDrive)
{
	return MAKELONG(MAKEWORD(cDrive, cDrive), MAKEWORD(cDrive, cDrive));
}

UINT64 CLinuxFileSupport::GetDiskSize(char cDrive)
{
	CPrintf Path("/vap/opt/crimson/disk/%c", toupper(cDrive));

	UINT64  Data = 0;

	_getdrivesize(Path, &Data);

	return Data;
}

UINT64 CLinuxFileSupport::GetDiskFree(char cDrive)
{
	CPrintf Path("/vap/opt/crimson/disk/%c", toupper(cDrive));

	UINT64  Data = 0;

	_getdrivefree(Path, &Data);

	return Data;
}

BOOL CLinuxFileSupport::FormatDisk(char cDrive)
{
	if( isalpha(cDrive) ) {

		CString Root = m_HostRoot + char(toupper(cDrive)) + '\\';

		Root.Replace('\\', m_HostSep);

		EmptyDirectory(Root);

		return TRUE;
	}

	return FALSE;
}

BOOL CLinuxFileSupport::EjectDisk(char cDrive)
{
	// TODO -- Consider how we handle memory sticks!!!

	return TRUE;
}

// Host Hooks

bool CLinuxFileSupport::HostRename(CString const &From, CString const &To)
{
	if( To.Find('/') == NOTHING ) {

		UINT uPos;

		if( (uPos = From.FindRev('/')) < NOTHING ) {

			CString Target;

			Target = From.Left(uPos+1) + To;

			return _rename(From, Target) >= 0;
		}
	}

	return _rename(From, To) >= 0;
}

bool CLinuxFileSupport::HostUnlink(CString const &Path)
{
	return _unlink(Path) >= 0;
}

bool CLinuxFileSupport::HostStat(CString const &Path, struct stat *buffer)
{
	return _stat(Path, buffer) >= 0;
}

bool CLinuxFileSupport::HostUTime(CString const &Path, time_t time)
{
	return _utime(Path, time, time) >= 0;
}

bool CLinuxFileSupport::HostChMod(CString const &Path, mode_t mode)
{
	return _chmod(Path, mode) >= 0;
}

PVOID CLinuxFileSupport::HostOpen(CString const &Path, int oflag, int pmode)
{
	int fd = _open(Path, oflag, pmode);

	return (fd > 0) ? PVOID(fd) : NULL;
}

void CLinuxFileSupport::HostClose(PVOID hFile)
{
	_close(int(hFile));
}

bool CLinuxFileSupport::HostStat(PVOID hFile, struct stat *buffer)
{
	return _fstat(int(hFile), buffer) >= 0;
}

int CLinuxFileSupport::HostRead(PVOID hFile, void *buffer, unsigned int count)
{
	return _read(int(hFile), buffer, count);
}

int CLinuxFileSupport::HostWrite(PVOID hFile, void const *buffer, unsigned int count)
{
	return _write(int(hFile), buffer, count);
}

int CLinuxFileSupport::HostSeek(PVOID hFile, long offset, int origin)
{
	return _lseek(int(hFile), offset, origin);
}

bool CLinuxFileSupport::HostTruncate(PVOID hFile, DWORD size)
{
	return _ftruncate(int(hFile), size) >= 0;
}

int CLinuxFileSupport::HostIoCtl(PVOID hFile, int func, void *data)
{
	return _ioctl(int(hFile), func, data);
}

bool CLinuxFileSupport::HostIsValidDir(CString const &Path)
{
	return _chdir(Path) >= 0;
}

bool CLinuxFileSupport::HostRmDir(CString const &Path)
{
	return _rmdir(Path) >= 0;
}

bool CLinuxFileSupport::HostMkDir(CString const &Path)
{
	return _mkdir(Path, 0755) >= 0;
}

int CLinuxFileSupport::HostScanDir(CString const &Path, struct dirent ***list, int (*selector)(struct dirent const *))
{
	int c = 0;

	struct linux_dirent64
	{
		INT64		d_ino;
		INT64		d_off;
		unsigned short	d_reclen;
		unsigned char	d_type;
		char		d_name[];
	};

	for( int p = 0; p < 2; p++ ) {

		int fd = _open(Path, O_RDONLY, 0);

		if( fd < 0 ) {

			return -1;
		}
		else {
			BYTE buff[1024];

			int  used = 0;

			int  pos  = 0;

			for( int n = 0;; ) {

				if( pos >= used ) {

					if( (used = _getdents64(fd, buff, sizeof(buff))) <= 0 ) {

						c = n;

						_close(fd);

						break;
					}

					pos = 0;
				}

				linux_dirent64 *read = (linux_dirent64 *) (buff + pos);

				dirent d = { 0 };

				d.d_type = read->d_type;

				strcpy(d.d_name, read->d_name);

				if( !selector || selector(&d) ) {

					if( !IsSpecial(d.d_name) || !IsRoot(Path) ) {

						if( p == 1 && n < c ) {

							(*list)[n] = (dirent *) malloc(sizeof(dirent));

							memcpy((*list)[n], &d, sizeof(d));
						}

						n++;
					}
				}

				pos += read->d_reclen;
			}

			if( p == 0 ) {

				if( !c )
					break;

				*list = (dirent **) calloc(c, sizeof(dirent *));
			}
		}
	}

	return c;
}

bool CLinuxFileSupport::HostSync(void)
{
	return _sync() > 0;
}

// Implementation

bool CLinuxFileSupport::IsSpecial(char const *d)
{
	return d[0] == '.' && (d[1] == 0 || (d[1] == '.' && d[2] == 0));
}

// End of File
