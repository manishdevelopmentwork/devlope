
#include "Intern.hpp"

#include "UIExprDropEdit.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Programmable Drop Edit
//

// Dynamic Class

AfxImplementDynamicClass(CUIExprDropEdit, CUIDropEdit);

// Constructor

CUIExprDropEdit::CUIExprDropEdit(void)
{
	m_cfCode = RegisterClipboardFormat(L"C3.1 Code Fragment");
	}

// Notification Handlers

BOOL CUIExprDropEdit::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_uID ) {

		if( uCode == CBN_SELCHANGE ) {

			UINT  uIndex = m_pDataCtrl->GetCurSel();

			CString Text = m_pDataCtrl->GetLBText(uIndex);

			Text.TrimLeft();

			switch( StdSave(FALSE, Text) ) {

				case saveChange:

					return TRUE;

				case saveSame:

					return FALSE;

				case saveError:

					LoadUI();

					return FALSE;
				}

			AfxAssert(FALSE);
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Data Overridables

BOOL CUIExprDropEdit::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptText(pData, m_cfCode) ) {

		dwEffect = DROPEFFECT_LINK;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIExprDropEdit::OnAcceptData(IDataObject *pData)
{
	m_pDataCtrl->SetFocus();

	if( AcceptText(pData, m_cfCode, L"=") ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File
