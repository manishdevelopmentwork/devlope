
#include "Intern.hpp"

#include "ExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Executive.hpp"

#include "Mutex.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Close-Coupled APIs
//

HTHREAD GetCurrentThread(void)
{
	return CExecThread::m_pThread;
	}

global UINT GetThreadIndex(void)
{
	return CExecThread::m_pThread->GetIndex();
	}

global void CheckThreadCancellation(void)
{
	CExecThread::m_pThread->CheckCancellation();
	}

global void SetTimer(UINT uTime)
{
	CExecThread::m_pThread->SetTimer(uTime);
	}

global UINT GetTimer(void)
{
	return CExecThread::m_pThread->GetTimer();
	}

global void AddToSlept(UINT uTime)
{
	CExecThread::m_pThread->AddToSlept(uTime);
	}

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Object
//

// Current Thread

CExecThread _tls_decl * CExecThread::m_pThread = NULL;

// Library Pointer

PVOID _tls_decl CExecThread::m_pLibData = NULL;

// Constructor

CExecThread::CExecThread(CMosExecutive *pExec) : CMosExecThread(pExec)
{
	m_hThread = INVALID_HANDLE_VALUE;
	}

// Destructor

CExecThread::~CExecThread(void)
{
	Info(0, "deleted\n");

	win32::CloseHandle(m_hThread);
	}

// IWaitable

PVOID CExecThread::GetWaitable(void)
{
	AfxAssert(FALSE);

	return NULL;
	}

BOOL CExecThread::Wait(UINT uWait)
{
	// Only the parent is allowed to wait on the thread. This is a
	// restriction that is only really imposed on Linux but we honor
	// it here to to ensure compatability.

	if( win32::GetCurrentThreadId() == DWORD(m_parent) ) {

		if( win32::WaitForSingleObjectEx(m_hThread, uWait, FALSE) == WAIT_OBJECT_0 ) {

			return TRUE;
			}

		return FALSE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CExecThread::HasRequest(void)
{
	AfxAssert(FALSE);

	return FALSE;
	}

// IThread

void CExecThread::Guard(BOOL fGuard)
{
	if( win32::GetCurrentThreadId() == DWORD(m_pid) ) {

		if( fGuard ) {

			m_guard++;
			}
		else {
			AfxAssert(m_guard);

			if( !--m_guard ) {

				m_pGuardProc = NULL;

				m_pGuardData = NULL;
				}
			}

		return;
		}

	AfxAssert(FALSE);
	}

void CExecThread::Guard(PGUARD pProc, void *pData)
{
	AfxAssert(!m_pGuardProc);
	
	m_pGuardProc = pProc;

	m_pGuardData = pData;

	Guard(TRUE);
	}

void CExecThread::Exit(int nCode)
{
	if( win32::GetCurrentThreadId() == DWORD(m_pid) ) {

		if( AtomicCompAndSwap(&m_state, 1, 2) == 1 ) {

			Info(1, "exiting\n");

			CExecCancel c;

			c.r = nCode;

			throw c;
			}

		// The thread is already being destroyed, so
		// just wait and allow the process to proceed.

		for(;;) Sleep(FOREVER);
		}

	AfxAssert(FALSE);
	}

BOOL CExecThread::Destroy(void)
{
	if( win32::GetCurrentThreadId() == DWORD(m_parent) ) {

		if( m_state == 1 ) {

			CAutoGuard Guard(SafeToGuard());

			if( AtomicCompAndSwap(&m_state, 1, 2) == 1 ) {

				if( m_state == 3 ) {

					// The thread did returned or threw an exception while
					// we were waiting for any guards to be released, so
					// we don't need to kill the cancellation request.

					Info(1, "returned already\n");
					}
				else {
					// Call any user cancellation procedure.

					if( m_pGuardProc ) {

						(*m_pGuardProc)(this, m_pGuardData);

						m_pGuardProc = NULL;

						m_pGuardData = NULL;
						}

					// This set the cancellation and then interrupt
					// any waits so that we're able to respond.

					win32::QueueUserAPC(KillerProc, m_hThread, m_exit);
					}

				// If we're using thread state management, we do
				// not wait for an exit at this point as the thread
				// will just move to the next state.

				if( !m_pTms ) {

					// Wait for the Windows thread to exit.

					win32::WaitForSingleObjectEx(m_hThread, FOREVER, FALSE);

					// And it's now safe to release the thread. This ought
					// to delete it as the thread's own reference will have
					// been removed during its shutdown process.

					Release();
					}

				return TRUE;
				}
			}
		
		Release();

		return FALSE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

void  CExecThread::SetLibPointer(PVOID pLibData)
{
	m_pLibData = pLibData;
	}

PVOID CExecThread::GetLibPointer(void)
{
	return m_pLibData;
	}

// Overridables

BOOL CExecThread::OnCreate(void)
{
	if( CMosExecThread::OnCreate() ) {

		m_state  = 1;

		m_parent = win32::GetCurrentThreadId();

		HANDLE h = win32::CreateThread(NULL, 655360, ThreadProc, this, 0, NULL);

		if( h != INVALID_HANDLE_VALUE ) {

			if( m_uLevel == THREAD_REALTIME ) {

				win32::SetThreadPriority(h, THREAD_PRIORITY_TIME_CRITICAL);
				}

			m_pid     = win32::GetThreadId(h);
		
			m_hThread = h;

			return TRUE;
			}
		}

	AfxTrace("exec: thread creation failed\n");

	m_state = 0;

	return FALSE;
	}

// Implementation

int CExecThread::ExecuteThread(void)
{
	m_pThread = this;
	
	m_pid     = win32::GetCurrentThreadId();

	m_hThread = GetThreadHandle();

	CreateImpure();

	CommonExecute();

	CMutex::FreeMutexList(this);

	// Take a copy of the exit state as the next call to
	// release might delete this object from under us!

	int nCode = m_exit;

	// Clear the thread pointer so we don't try to do
	// crazy things like call Guard while we're exiting.

	m_pThread = NULL;

	// Only one release here as the parent does its own.
		
	Release();

	// Release any library data.

	free(m_pLibData);

	// Exit the Windows thread.

	return nCode;
	}

HANDLE CExecThread::GetThreadHandle(void)
{
	HANDLE hProcess = win32::GetCurrentProcess();

	HANDLE hPseudo  = win32::GetCurrentThread();

	HANDLE hThread  = NULL;

	win32::DuplicateHandle( hProcess, 
				hPseudo,
				hProcess, 
				&hThread,
				0,
				FALSE,
				DUPLICATE_SAME_ACCESS
				);

	return hThread;
	}

// Callbacks

DWORD CExecThread::ThreadProc(void *pParam)
{
	CExecThread *pThread = (CExecThread *) pParam;

	return pThread->ExecuteThread();
	}

void CExecThread::KillerProc(DWORD dwParam)
{
	m_pThread->SetCancelFlag();
	}

// End of File
