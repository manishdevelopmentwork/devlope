#include "TagInfo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Native Tag Information
//

// Constructor

CTagInfo::CTagInfo(void)
{
	m_uPos = 0;

	m_pWatch = NULL;

	m_fTouch = FALSE;

	m_uExtra = 0;
	}

// Destructor

CTagInfo::~CTagInfo(void)
{
	}

void CTagInfo::SetClient(CCipClient *pClient)
{
	m_pClient = pClient;
	}

// Tag Management

void CTagInfo::Touch(void)
{
	m_fTouch = TRUE;

	m_pWatch->Touch();
	}

BOOL CTagInfo::ShouldTouch(void)
{
	return m_fTouch;
	}

void CTagInfo::ClearTouch(void)
{
	m_fTouch = FALSE;
	}

BOOL CTagInfo::IsWatched(void)
{
	return m_pWatch != NULL;
	}

void CTagInfo::SetDim(UINT uDim, UINT uVal)
{
	if( uVal > 0 ) {

		m_wDims++;
		}

	switch( uDim ) {

		case 0:
			m_XDim = uVal;
			break;
		case 1:
			m_YDim = uVal;
			break;
		case 2:
			m_ZDim = uVal;
			break;
		}
	}

void CTagInfo::ResetPos(void)
{
	m_uPos = 0;
	}

UINT CTagInfo::GetSize(void)
{
	return m_uSize;
	}

BOOL CTagInfo::FindInfo(void)
{
	if( m_pClient && m_pClient->IsLayerUp() ) {

		UINT uSize = 0;
		UINT uType = 0;
		UINT uExtra = 0;

		bool fFoundTag = false;

		fFoundTag = m_pClient->GetTagInfo(m_pName,
						  m_Path,
						  uSize, 
						  uType, 
						  uExtra
						  );

		if( strchr(m_pName, '[') && fFoundTag ) {

			// We have a complex array arrangement,
			// so we need to sort out the watch list
			// configuration arguments.

			m_Path = ParseWatchDims();
			}

		if( fFoundTag ) {

			m_uSize = uSize; // Size of data

			m_uType = uType;

			m_uExtra = uExtra;

			return TRUE;
			}
		else {
			m_uSize = 0;
			}
		}

	return FALSE;
	}

// Tag reading/writing

UINT CTagInfo::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsWatched() ) {
		
		m_fTouch = TRUE;

		if( m_uSize > 0 ) {

			if( m_pWatch->HasData() ) {

				PBYTE pRead = m_pWatch->GetData() + m_uPos;

				switch( m_uType ) {

					case 0x00C1:
						*pData = *pRead;

						// Mask and shift to get the packed boolean bit
						*pData &= (1 << m_uExtra);

						// Coerce result to boolean 1 or 0
						*pData = !!*pData;
						break;

					case 0x00C2:
						*pData = *pRead;
						break;

					case 0x00C3:
						*pData = IntelToHost(*PU2(pRead));
						break;

					case 0x00C4:
					case 0x00CA:
					case 0x00D3:
						*pData = IntelToHost(*PU4(pRead));						
						break;
					}

				return uCount;
				}
			}

		return CCODE_ERROR;
		}
	else {
		// NOTE -- This approach reads the tag the hard way until it
		// ends up on the watch list. The alternative is to use the
		// CC_NO_DATA mechanism to indicate that the tag is valid but
		// that we don't have any data available right now.

		UINT uRead = 0;

		CCipPath Path = ParseDims(0);

		if( (uRead = m_pClient->ReadTag(m_pName, pData, uCount, Path)) ) {

			m_fTouch = TRUE;

			return uRead;
			}
		else {
			if( m_pClient->IsLayerUp() ) {

				// If the layer is up, but the read failed,
				// then we will not be able to read this tag.
				return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
				}
			}
		}

	return CCODE_ERROR;
	}

UINT CTagInfo::ReadTable(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	CCipPath Path = ParseDims(uOffset);

	UINT uRead = 0;

	if( (uRead = m_pClient->ReadTag(m_pName, pData, uCount, Path)) ) {

		return uRead;
		}

	else {
		if( m_pClient->IsLayerUp() ) {

			// If the layer is up, but the read failed,
			// then we will not be able to read this tag.
			return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
			}
		}
	
	return CCODE_ERROR;
	}

UINT CTagInfo::ReadBitTable(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 2048);

	UINT uStart = Addr.a.m_Offset / 32;

	UINT uEnd = (Addr.a.m_Offset + uCount - 1) / 32;

	UINT uReqCount = (uEnd - uStart) + 1;

	CCipPath Path = ParseDims(uStart);

	UINT uRead = 0;

	if( (uRead = m_pClient->ReadTag(m_pName, pData, uReqCount, Path)) ) {

		if( uRead * 32 >= uCount ) {

			DWORD *pPacked = new DWORD[ uReqCount ];

			for( UINT t = 0; t < uReqCount; t++ ) {

				pPacked[t] = pData[t];
				}

			UINT uTableOffset = Addr.a.m_Offset % m_XDim;

			for( UINT n = 0; n < uCount; n++ ) {

				UINT uMask = 1 << ((uTableOffset + n) % 32);

				UINT uPos = ((Addr.a.m_Offset + n) / 32) - uStart;

				pData[n] = !!(uMask & pPacked[uPos]);
				}

			delete [] pPacked;

			return uCount;
			}
		}
	else {
		if( m_pClient->IsLayerUp() ) {

			// If the layer is up, but the read failed,
			// then we will not be able to read this tag.
			return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;
			}
		}
	
	return CCODE_ERROR;
	}

UINT CTagInfo::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT x = m_XDim;
	UINT y = m_YDim;
	UINT z = m_ZDim;

	UINT uOffset = Addr.a.m_Table >= addrNamed ? 0 : Addr.a.m_Offset;

	UINT uWrite = 0;

	CCipPath Path = ParseDims(uOffset);

	if( (uWrite = m_pClient->WriteTag(m_pName, pData, uCount, Path)) ) {

		if( IsWatched() ) {

			UpdateWatch(*pData);
			}

		return uWrite;
		}

	return CCODE_ERROR;
	}

UINT CTagInfo::WriteBitTable(AREF Addr, PDWORD pData, UINT uCount)
{
	MakeMin(uCount, 2048);

	UINT uStart = Addr.a.m_Offset / 32;

	UINT uEnd = (Addr.a.m_Offset + uCount - 1) / 32;

	UINT uReqCount = (uEnd - uStart) + 1;

	CCipPath Path = ParseDims(uStart);

	DWORD *pValues = new DWORD[ uCount ];

	DWORD *pTemp   = new DWORD[ uReqCount ];

	for( UINT t = 0; t < uCount; t++ ) {

		pValues[t] = pData[t];
		}

	if( m_pClient->ReadTag(m_pName, pTemp, uReqCount, Path) ) {

		UINT uBitOffset = Addr.a.m_Offset % m_XDim;

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD Data = pValues[n];

			UINT uPos = ((Addr.a.m_Offset + n) / 32) - uStart;

			DWORD dwMask = 1 << ((uBitOffset + n) % 32);

			if( Data )

				pTemp[uPos] |= dwMask;
			else
				pTemp[uPos] &= ~dwMask;
			}

		for( UINT m = 0; m < uReqCount; m++ ) {

			pData[m] = pTemp[m];
			}

		UINT uWrite = 0;

		if( (uWrite = m_pClient->WriteTag(m_pName, pData, uReqCount, Path)) ) {

			if( uWrite * 32 >= uCount ) {

				delete [] pValues;

				delete [] pTemp;

				return uCount;
				}
			}
		}

	delete [] pValues;

	delete [] pTemp;

	return CCODE_ERROR;
	}

CCipPath CTagInfo::ParseDims(UINT uOffset)
{
	CCipPath Path;

	PTXT pCopy = strdup(m_pName);

	UINT uDots = CountDots(pCopy);

	UINT uTok = 0;

	PTXT pTok = strtok(pCopy, ".");

	if( !strnicmp(pTok, "Program:", 8) ) {

		// Program tags have an extra dot, so we need to ignore it.
		uDots--;

		Path.AddString(pTok);

		pTok = strtok(NULL, ".");
		}

	UINT x, y, z;

	x = y = z = NOTHING;

	GetTokenDims(pTok, x, y, z);

	Path.AddString(pTok);

	if( uTok < uDots ) {

		if( x < NOTHING ) Path.AddLogical(2, x);
		if( y < NOTHING ) Path.AddLogical(2, y);
		if( z < NOTHING ) Path.AddLogical(2, z);
		}

	if( uTok == uDots ) {

		switch( m_wDims ) {

			case 1:
				x = uOffset % m_XDim;

				break;
			case 2:
				x = uOffset / m_YDim;

				y = uOffset % m_YDim;

				break;
			case 3:
				x = uOffset / ( m_YDim * m_ZDim );

				uOffset = uOffset % ( m_YDim * m_ZDim );

				y = uOffset / m_ZDim;

				z = uOffset % m_ZDim;
					
				break;
			}

		if( x < NOTHING ) Path.AddLogical(2, x);
		if( y < NOTHING ) Path.AddLogical(2, y);
		if( z < NOTHING ) Path.AddLogical(2, z);
		}

	while( (pTok = strtok(NULL, ".")) ) {

		uTok++;

		UINT uDim = NOTHING;

		char *pCh = strchr(pTok, '[');

		if( pCh ) {

			uDim = strtoul(pCh + 1, NULL, 10);

			*pCh = '\0';
			}

		Path.AddString(pTok);

		if( pCh ) {

			Path.AddLogical(2, uDim);
			}		
		}

	if( m_XDim > 0 && uDots > 0 && uDots == uTok ) {
		
		Path.AddLogical(2, uOffset);
		}

	free(pCopy);

	return Path;
	}

CCipPath CTagInfo::ParseWatchDims(void)
{
	CCipPath Path(m_Path);

	PTXT pCopy = strdup(m_pName);

	UINT uDots = CountDots(pCopy);

	UINT uTok = 0;

	BYTE Data[64] = {0};

	memcpy(Data, Path.GetData(), Path.GetSize());

	UINT uOrig = Path.GetSize();

	UINT uPos = 0;

	Path.Clear();

	PTXT pTok = strtok(pCopy, ".");

	if( !strnicmp(pTok, "Program:", 8) ) {

		uTok++;
		
		// For a tag that will be added to a watch list, 
		// copy the first 12 bytes of the buffer,
		// this includes the tag index and program handle.
		while( uPos < 12 ) {

			Path.AddByte(Data[uPos++]);
			}

		pTok = strtok(NULL, ".");
		}

	else {
		// Copy the tag index so it can be
		// used in a watch list configuration.
		while( uPos < 6 ) {

			Path.AddByte(Data[uPos++]);
			}
		}

	UINT x, y, z;

	x = y = z = NOTHING;

	GetTokenDims(pTok, x, y, z);

	if( uTok < uDots ) {

		if( x < NOTHING ) Path.AddLogical(2, x);
		if( y < NOTHING ) Path.AddLogical(2, y);
		if( z < NOTHING ) Path.AddLogical(2, z);
		}

	while( (pTok = strtok(NULL, ".")) ) {

		uTok++;

		UINT uDim = NOTHING;

		char *pCh = strchr(pTok, '[');

		if( pCh ) {

			uDim = strtoul(pCh + 1, NULL, 10);

			*pCh = '\0';
		
			Path.AddByte(Data[uPos++]);
			Path.AddByte(Data[uPos++]);

			Path.AddLogical(2, uDim);
			}
		}

	while( uPos < uOrig ) {

		Path.AddByte(Data[uPos++]);
		}

	free(pCopy);

	return Path;
	}

void CTagInfo::GetTokenDims(PTXT pTok, UINT &x, UINT &y, UINT &z)
{
	char *pCh = strchr(pTok, '[');

	if( pCh ) {

		x = strtoul(pCh + 1, NULL, 10);

		char *pYDim = strchr(pTok, ',');

		char *pZDim = NULL;

		if( pYDim ) {

			pZDim = strchr(pYDim + 1, ',');

			y = strtoul(pYDim + 1, NULL, 10);
			}

		if( pZDim )
			z = strtoul(pZDim + 1, NULL, 10);
				
		*pCh = '\0';
		}
	}

UINT CTagInfo::CountDots(PCTXT pName)
{
	UINT uSize = strlen(pName);

	UINT uDots = 0;

	for( UINT n = 0; n < uSize; n++ ) {

		if( pName[n] == '.' ) {

			uDots++;
			}
		}

	return uDots;
	}

void CTagInfo::UpdateWatch(UINT uVal)
{
	PBYTE pData = m_pWatch->GetData() + m_uPos;

	// The tag is on a watch list, so we'll update
	// the written data so we don't need to wait for
	// the next service routine.

	switch( m_uType ) {

		case 0x00C1:
							
			// Special handling for packed booleans
			if( uVal ) {

				*pData |= 1 << m_uExtra;
				}
			else {
				*pData &= ~(1 << m_uExtra);
				}
			break;

		case 0x00C2:
			*pData = uVal;
			break;

		case 0x00C3:
			*PU2(pData) = HostToIntel(WORD(uVal));
			break;

		case 0x00C4:
		case 0x00CA:
		case 0x00D3:
			*PU4(pData) = HostToIntel(DWORD(uVal));
			break;
		}
	}

// End of File
