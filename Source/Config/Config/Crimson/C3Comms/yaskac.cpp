
#include "intern.hpp"

#include "yaskac.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaACDrivesDeviceOptions, CUIItem);

// Constructor

CYaskawaACDrivesDeviceOptions::CYaskawaACDrivesDeviceOptions(void)
{
	m_Unit		= 1;

	m_DriveType	= DRVA;
	}

// UI Management

void CYaskawaACDrivesDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CYaskawaACDrivesDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Unit));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaACDrivesDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Unit);
	Meta_AddInteger(DriveType);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 Inverters Driver
//

// Instantiator

ICommsDriver *	Create_YaskawaACDrivesDriver(void)
{
	return New CYaskawaACDrivesDriver;
	}

// Constructor

CYaskawaACDrivesDriver::CYaskawaACDrivesDriver(void)
{
	m_wID		= 0x4099;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "AC Drives";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Yaskawa AC Drives";

	m_pYACData	= &YACData;

	m_pYACData->uMAlp = 'A';
	m_pYACData->uMPar = 101;
	m_pYACData->uModb = 0x101;

	SetpConfig(NULL);

	AddSpaces();  
	}

// Binding Control

UINT CYaskawaACDrivesDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYaskawaACDrivesDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CYaskawaACDrivesDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CYaskawaACDrivesDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaACDrivesDeviceOptions);
	}

// Address Management

BOOL CYaskawaACDrivesDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CYaskawaACDrivesAddrDialog Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CYaskawaACDrivesDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pConfig ) {

		SetpConfig(pConfig);
		}

	CString Type   = StripType(pSpace, Text);

	Addr.a.m_Table = pSpace->m_uTable;
	Addr.a.m_Extra = 0;
	Addr.a.m_Type  = pSpace->TypeFromModifier(Type);

	switch( pSpace->m_uTable ) {

		case SPDA0:
		case SPDA1:
			Addr.a.m_Type = addrBitAsBit;
		case SPDA3:
		case SPDA4:
			Addr.a.m_Offset = tatoi(Text);
			return TRUE;

		case SPEN:
			Addr.a.m_Offset = 0x900;
			return TRUE;

		case SPAC:
			Addr.a.m_Offset = 0x910;
			return TRUE;

		case SPEC:
		case SPEV:
			Addr.a.m_Offset = 0;
			return TRUE;
		}

	SetItemRanges(pSpace->m_uTable);

	CRangeArray p = m_Ranges;

	UINT uAddress	= p[0];
	
	CString s;

	if( uAddress == 0xFFFF ) {	// list has no members

		s = TEXT("(");

		switch(m_pYACData->uDriveType) {

			case DRVA: s += AGOODLIST;	break;
			case DRVF: s += FGOODLIST;	break;
			case DRVG: s += GGOODLIST;	break;
			case DRVJ: s += JGOODLIST;	break;
			case DRVV: s += VGOODLIST;	break;
			}

		s += TEXT(")n_dd");

		Error.Set( s,
			0
			);

		return FALSE;
		}

	UINT uMenu	= p[1] / 100;

	UINT uItem	= p[1] % 100;

	UINT uFind	= Text.Find('_');

	if( uFind < NOTHING ) {

		uMenu	= tatoi(Text.Left(uFind));

		uItem	= tatoi(Text.Mid(uFind + 1));

		m_pYACData->uMPar = (uMenu * 100) + uItem;

		if( ParamToModbus() ) {

			uAddress = m_pYACData->uModb;
			}
		}

	if( !(uAddress >= pSpace->m_uMinimum && uAddress <= pSpace->m_uMaximum) ) {

		s.Printf( TEXT("%s%1.1d_%2.2d...%s%1.1d_%2.2d"),
			pSpace->m_Prefix,
			pSpace->m_uMinimum / 100,
			pSpace->m_uMinimum % 100,
			pSpace->m_Prefix,
			pSpace->m_uMaximum / 100,
			pSpace->m_uMaximum % 100
			);

		Error.Set( s,
			0
			);

		return FALSE;
		}

	m_pYACData->uMAlp = pSpace->m_uTable;

	s = ValidateSelection();

	if( s.GetLength() > 1 ) {

		s.Printf( "%s %s", pSpace->m_Prefix, s );

		Error.Set( s,
			0
			);

		return FALSE;
		}

	Addr.a.m_Offset	= uAddress;

	return TRUE;
	}

BOOL CYaskawaACDrivesDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( pConfig ) {

		SetpConfig(pConfig);
		}

	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		switch( Addr.a.m_Table ) {

			case SPDA0:
			case SPDA1:
			case SPDA3:
			case SPDA4:
				return CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr);

			case SPEN: Text.Printf( TEXT("EN900") );	return TRUE;
			case SPAC: Text.Printf( TEXT("AC910") );	return TRUE;
			case SPEC:
			case SPEV:
				Text.Printf( TEXT("%s0"), pSpace->m_Prefix );
				return TRUE;
			}

		SetItemRanges(pSpace->m_uTable);

		CString Type = Addr.a.m_Type == pSpace->m_uType ? "" : "." + pSpace->GetTypeModifier(Addr.a.m_Type);

		m_pYACData->uMAlp = GetAlphaFromTable(Addr.a.m_Table);
		m_pYACData->uModb = Addr.a.m_Offset;

		if( ModbusToParam() ) {

			Text.Printf( TEXT("%s%1.1d_%2.2d%s"),
				pSpace->m_Prefix,
				m_pYACData->uMPar/100,
				m_pYACData->uMPar%100,
				Type
				);

			return TRUE;
			}
		}
	
	return FALSE;
	}

UINT CYaskawaACDrivesDriver::GetAlphaFromTable(UINT uTable)
{
	switch( uTable ) {

		case SPB:	return (UINT)'B';
		case SPC:	return (UINT)'C';
		case SPD:	return (UINT)'D';
		case SPE:	return (UINT)'E';
		case SPF:	return (UINT)'F';
		case SPH:	return (UINT)'H';
		case SPL:	return (UINT)'L';
		case SPN:	return (UINT)'N';
		case SPO:	return (UINT)'O';
		case SPP:	return (UINT)'P';
		case SPQ:	return (UINT)'Q';
		case SPT:	return (UINT)'T';
		case SPU:	return (UINT)'U';
		}

	return 'A';
	}

void CYaskawaACDrivesDriver::SetYACMPar(UINT uYACMPar)
{
	m_pYACData->uMPar = uYACMPar;
	}

void CYaskawaACDrivesDriver::SetYACModb(UINT uYACModb)
{
	m_pYACData->uModb = uYACModb;
	}

YACDATA * CYaskawaACDrivesDriver::GetYACDataPtr(void)
{
	return m_pYACData;
	}

void CYaskawaACDrivesDriver::SetItemRanges(UINT uTable)
{
	switch( uTable ) {

		case SPA:	GetA();	return;
		case SPB:	GetB();	return;
		case SPC:	GetC();	return;
		case SPD:	GetD();	return;
		case SPE:	GetE();	return;
		case SPF:	GetF();	return;
		case SPH:	GetH();	return;
		case SPL:	GetL();	return;
		case SPN:	GetN();	return;
		case SPO:	GetO();	return;
		case SPP:	GetP();	return;
		case SPQ:	GetQ(); return;
		case SPT:	GetT();	return;
		case SPU:	GetU();	return;
		}
	}

CRangeArray * CYaskawaACDrivesDriver::GetRanges(void)
{
	return &m_Ranges;
	}

BOOL CYaskawaACDrivesDriver::ParamToModbus(void)
{
	UINT uDSel = m_pYACData->uMPar;

	if( !uDSel ) {

		m_pYACData->uModb = 0xFFFF; // list has no members

		return TRUE;
		}

	CRangeArray p = m_Ranges;

	UINT i        = 0;

	while( p[i] ) {

		UINT uDecLo = p[i + 1];

		if( uDSel >= uDecLo ) {

			UINT uDecHi = uDecLo + p[i + 2];

			if( uDSel < uDecHi ) {

				m_pYACData->uModb = p[i] + uDSel - uDecLo;

				return TRUE;
				}

			else { // if in between ranges, or at end, select this max.
				if( uDSel < p[i + 4] || (i + 4) > m_pYACData->uSize ) {

					m_pYACData->uModb = p[i] + p[i+2] - 1;

					m_pYACData->uMPar = uDecHi - 1;

					return TRUE;
					}
				}
			}

		i += 3;
		}

	return FALSE;
	}

BOOL CYaskawaACDrivesDriver::ModbusToParam(void)
{
	CRangeArray p = m_Ranges;

	UINT uHexAddr = m_pYACData->uModb;

	if( uHexAddr == 0xFFFF ) {

		m_pYACData->uMPar = 0;
		m_pYACData->uSize = 1;

		return TRUE;
		}

	UINT uSize    = m_pYACData->uSize;

	BOOL f = FALSE;

	UINT i = 0;

	while( i < uSize ) {

		UINT uHSel = p[i];

		UINT uDSel = p[i+1];

		UINT uEnd  = uHSel + p[i+2] - 1;

		if( uHexAddr >= uHSel && uHexAddr <= uEnd ) {

			m_pYACData->uMPar = uDSel + uHexAddr - uHSel;

			f = TRUE;

			break;
			}

		i += 3;
		}

	if( !f ) {

		m_pYACData->uModb = p[0];

		m_pYACData->uMPar = p[1];
		}

	return f;
	}

CItem * CYaskawaACDrivesDriver::GetpConfig(void)
{
	return m_pYACData->pConfig;
	}

void CYaskawaACDrivesDriver::SetpConfig(CItem *pConfig)
{
	if( pConfig ) {

		m_pYACData->uDriveType	= pConfig->GetDataAccess("DriveType")->ReadInteger(pConfig);
		}

	m_pYACData->pConfig	= pConfig;
	}

// Implementation

void CYaskawaACDrivesDriver::AddSpaces(void)
{
	AddSpace(New CSpace(SPA,   "A",		"A Parameters",			10, 0x100, 0x128, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPB,   "B",		"B Parameters",			10, 0x177, 0x1FF, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPC,   "C",		"C Parameters",			10, 0x200, 0x386, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPD,   "D",		"D Parameters",			10, 0x280, 0x2B6, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPE,   "E",		"E Parameters",			10, 0x300, 0x353, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPF,   "F",		"F Parameters",			10, 0x364, 0x3FF, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPH,   "H",		"H Parameters",			10, 0x2F0, 0x43F, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPL,   "L",		"L Parameters",			10, 0x2CC, 0x4FD, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPN,   "N",		"N Parameters",			10, 0x2D3, 0x65D, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPO,   "O",		"O Parameters",			10, 0x500, 0x528, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPP,   "P",		"P Parameters",			10, 0x100, 0x310, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPQ,   "Q",		"Q Parameters",			10, 0x1600, 0x17A4, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPT,   "T",		"T Parameters",			10, 0x700, 0x763, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPU,   "U",		"U Parameters",			10, 0x3C, 0x195C, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPEN,  "EN",	"ENTER  (Modbus = Hex 900)",	10, 0x900, 0x900, addrWordAsWord));
	AddSpace(New CSpace(SPAC,  "AC",	"ACCEPT (Modbus = Hex 910)",	10, 0x910, 0x910, addrWordAsWord));
	AddSpace(New CSpace(SPDA0, "DA0",	"Modbus Output",		10,	0, 65535, addrBitAsBit));
	AddSpace(New CSpace(SPDA1, "DA1",	"Modbus Input",			10,	0, 65535, addrBitAsBit));
	AddSpace(New CSpace(SPDA3, "DA3",	"Modbus Analog Register",	10,	0, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPDA4, "DA4",	"Modbus Holding Register",	10,	0, 65535, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace(SPEC,  "EC",	"Latest Error Command",		10,	0,     0, addrWordAsWord));
	AddSpace(New CSpace(SPEV,  "EV",	"Latest Error Value",		10,	0,     0, addrWordAsWord));
	}

CString CYaskawaACDrivesDriver::ValidateSelection(void)
{
	CRangeArray p	= m_Ranges;

	UINT uParIn	= m_pYACData->uMPar;

	UINT uMenu	= uParIn / 100;

	UINT uLoErr	= 999;

	UINT uHiErr	= 0;

	UINT i		= 1;

	while( i < m_pYACData->uSize ) {

		UINT uPar = p[i];

		if( uParIn >= uPar && uParIn < uPar + p[i+1] ) {

			return TEXT("1");
			}

		if( uMenu == uPar / 100 ) {

			if( uParIn < uLoErr || uParIn > uHiErr) {

				uLoErr = uPar;
				uHiErr = uPar + p[i+1] - 1;
				}
			}

		i += 3;
		}

	CString s;

	s.Printf(TEXT("%1.1d_%2.2d - %1.1d_%2.2d"),

		uLoErr / 100,
		uLoErr % 100,
		uHiErr / 100,
		uHiErr % 100
		);

	return s;
	}

void CYaskawaACDrivesDriver::MakeList(UINT *pList, UINT uTable)
{
	m_Ranges.Empty();

	UINT uItem = pList[0];

	UINT i = 0;

	while( (BOOL)(uItem = pList[i]) ) {

		m_Ranges.Append(uItem);

		i++;
		}

	m_Ranges.Append(0);

	m_Ranges.Append(uTable);
	m_pYACData->uSize = i;

	for( i = 1; i < 10; i++ ) { // for each menu #, set min and max

		UINT uMenuLo	= i * 100;
		UINT uMenuHi	= uMenuLo + 100;
		UINT uLowest	= 0xFFFF;
		UINT uHighest	= 0;

		UINT j		= 0;

		while( j < m_pYACData->uSize ) {

			UINT uPar = pList[j + 1];
			UINT uMod = pList[j];

			if( uPar >= uMenuLo && uPar < uMenuHi ) {

				if( uMod < uLowest ) {

					uLowest = uMod;
					}

				if( uMod > uHighest ) {

					uHighest = uMod + pList[j+2] - 1;
					}
				}

			j += 3;
			}

		m_Ranges.Append(uLowest);
		m_Ranges.Append(uHighest);
		}
	}

// List format = <modbus start address in hex>,<param number in decimal>,<# of consecutive addresses>...
// <0 = terminator>
void CYaskawaACDrivesDriver::GetA(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AALIST;
			break;
		case DRVF:
			pList = FALIST;
			break;
		case DRVG:
			pList = GALIST;
			break;
		case DRVJ:
			pList = JALIST;
			break;
		case DRVV:
			pList = VALIST;
			break;
		}

	MakeList(pList, (UINT)'A');
	}

void CYaskawaACDrivesDriver::GetB(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = ABLIST;
			break;
		case DRVF:
			pList = FBLIST;
			break;
		case DRVG:
			pList = GBLIST;
			break;
		case DRVJ:
			pList = JBLIST;
			break;
		case DRVV:
			pList = VBLIST;
			break;
		}

	MakeList(pList, (UINT)'B');
	}

void CYaskawaACDrivesDriver::GetC(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = ACLIST;
			break;
		case DRVF:
			pList = FCLIST;
			break;
		case DRVG:
			pList = GCLIST;
			break;
		case DRVJ:
			pList = JCLIST;
			break;
		case DRVV:
			pList = VCLIST;
			break;
		}

	MakeList(pList, (UINT)'C');
	}

void CYaskawaACDrivesDriver::GetD(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = ADLIST;
			break;
		case DRVF:
			pList = FDLIST;
			break;
		case DRVG:
			pList = GDLIST;
			break;
		case DRVJ:
			pList = JDLIST;
			break;
		case DRVV:
			pList = VDLIST;
			break;
		}

	MakeList(pList, (UINT)'D');
	}

void CYaskawaACDrivesDriver::GetE(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AELIST;
			break;
		case DRVF:
			pList = FELIST;
			break;
		case DRVG:
			pList = GELIST;
			break;
		case DRVJ:
			pList = JELIST;
			break;
		case DRVV:
			pList = VELIST;
			break;
		}

	MakeList(pList, (UINT)'E');
	}

void CYaskawaACDrivesDriver::GetF(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AFLIST;
			break;
		case DRVF:
			pList = FFLIST;
			break;
		case DRVG:
			pList = GFLIST;
			break;
		case DRVJ:
			pList = JFLIST;
			break;
		case DRVV:
			pList = VFLIST;
			break;
		}

	MakeList(pList, (UINT)'F');
	}

void CYaskawaACDrivesDriver::GetH(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AHLIST;
			break;
		case DRVF:
			pList = FHLIST;
			break;
		case DRVG:
			pList = GHLIST;
			break;
		case DRVJ:
			pList = JHLIST;
			break;
		case DRVV:
			pList = VHLIST;
			break;
		}

	MakeList(pList, (UINT)'H');
	}

void CYaskawaACDrivesDriver::GetL(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = ALLIST;
			break;
		case DRVF:
			pList = FLLIST;
			break;
		case DRVG:
			pList = GLLIST;
			break;
		case DRVJ:
			pList = JLLIST;
			break;
		case DRVV:
			pList = VLLIST;
			break;
		}

	MakeList(pList, (UINT)'L');
	}

void CYaskawaACDrivesDriver::GetN(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = ANLIST;
			break;
		case DRVF:
			pList = FNLIST;
			break;
		case DRVG:
			pList = GNLIST;
			break;
		case DRVJ:
			pList = JNLIST;
			break;
		case DRVV:
			pList = VNLIST;
			break;
		}

	MakeList(pList, (UINT)'N');
	}

void CYaskawaACDrivesDriver::GetO(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AOLIST;
			break;
		case DRVF:
			pList = FOLIST;
			break;
		case DRVG:
			pList = GOLIST;
			break;
		case DRVJ:
			pList = JOLIST;
			break;
		case DRVV:
			pList = VOLIST;
			break;
		}

	MakeList(pList, (UINT)'O');
	}

void CYaskawaACDrivesDriver::GetP(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = APLIST;
			break;
		case DRVF:
			pList = FPLIST;
			break;
		case DRVG:
			pList = GPLIST;
			break;
		case DRVJ:
			pList = JPLIST;
			break;
		case DRVV:
			pList = JTLIST;
			break;
		}

	MakeList(pList, (UINT)'P');
	}

void CYaskawaACDrivesDriver::GetQ(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AQLIST;
			break;
		}

	MakeList(pList, (UINT)'Q');
	}

void CYaskawaACDrivesDriver::GetT(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = ATLIST;
			break;
		case DRVF:
			pList = FTLIST;
			break;
		case DRVG:
			pList = GTLIST;
			break;
		case DRVJ:
			pList = JTLIST;
			break;
		case DRVV:
			pList = VTLIST;
			break;
		}

	MakeList(pList, (UINT)'T');
	}

void CYaskawaACDrivesDriver::GetU(void)
{
	UINT *pList = 0;

	switch( m_pYACData->uDriveType ) {

		case DRVA:
			pList = AULIST;
			break;
		case DRVF:
			pList = FULIST;
			break;
		case DRVG:
			pList = GULIST;
			break;
		case DRVJ:
			pList = JULIST;
			break;
		case DRVV:
			pList = VULIST;
			break;
		}

	MakeList(pList, (UINT)'U');
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaACDrivesTCPDeviceOptions, CUIItem);

// Constructor

CYaskawaACDrivesTCPDeviceOptions::CYaskawaACDrivesTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(20, 1), MAKEWORD(168, 192) ));

	m_Socket = 502;

	m_Unit   = 1;

	m_DriveType = DRVA;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CYaskawaACDrivesTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CYaskawaACDrivesTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddWord(WORD(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaACDrivesTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	Meta_AddInteger(DriveType);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Series 7 TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_YaskawaACDrivesTCPDriver(void)
{
	return New CYaskawaACDrivesTCPDriver;
	}

// Constructor

CYaskawaACDrivesTCPDriver::CYaskawaACDrivesTCPDriver(void)
{
	m_wID		= 0x3541;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "AC Drives";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Yaskawa AC Drives TCP/IP";
	}

// Binding Control

UINT CYaskawaACDrivesTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYaskawaACDrivesTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYaskawaACDrivesTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CYaskawaACDrivesTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaACDrivesTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa AC Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CYaskawaACDrivesAddrDialog, CStdAddrDialog);
		
// Constructor

CYaskawaACDrivesAddrDialog::CYaskawaACDrivesAddrDialog(CYaskawaACDrivesDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_pDriver	= &Driver;
	
	m_pAddr		= &Addr;

	m_fPart		= fPart;

	m_pSpace	= NULL;

	m_pGDrv		= &Driver;

	m_pYACData	= m_pGDrv->GetYACDataPtr();

	m_pGDrv->SetpConfig(pConfig);

	m_pConfig	= pConfig;

	SetName(TEXT("YaskawaACDrivesAddrDlg"));
	}

// Destructor
CYaskawaACDrivesAddrDialog::~CYaskawaACDrivesAddrDialog(void)
{
	}

// Message Map

AfxMessageMap(CYaskawaACDrivesAddrDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify (1001,	LBN_SELCHANGE,	OnSpaceChange)
	AfxDispatchNotify (DLGN,	CBN_SELCHANGE,	OnComboChange)
	AfxDispatchNotify (DLGP,	CBN_SELCHANGE,	OnComboChange)
//	AfxDispatchCommand(2027,	OnUpdateAddress)
	AfxDispatchNotify (4001,	LBN_SELCHANGE,	OnTypeChange )

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CStdAddrDialog)
	};

// Message Handlers

BOOL CYaskawaACDrivesAddrDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;

	FindSpace();

	LoadList();

	if( m_pSpace ) {

		UINT uTable = Addr.m_Ref ? Addr.a.m_Table : m_pSpace->m_uTable;

		BOOL f = IsYACParam(uTable);

		if( f ) {

			m_pGDrv->SetItemRanges(uTable);

			m_pRanges = m_pGDrv->GetRanges();

			if( Addr.m_Ref ) {

				m_pYACData->uMAlp = m_pGDrv->GetAlphaFromTable(Addr.a.m_Table);
				m_pYACData->uModb = Addr.a.m_Offset;

				m_pGDrv->ModbusToParam();

				LoadCBMenuNumbers(m_pYACData->uMPar / 100);
				LoadCBParameters (m_pYACData->uMPar % 100);

				LoadType();

				ShowParamData(GetTypeCode());

				YACParamEnables(Addr.m_Ref);

				return FALSE;
				}

			else {
				Addr.a.m_Table	= uTable;
				Addr.a.m_Offset	= (*m_pRanges)[0];
				Addr.a.m_Extra	= 0;
				Addr.a.m_Type	= m_pSpace->m_uType;

				m_pYACData->uMAlp = m_pGDrv->GetAlphaFromTable(Addr.a.m_Table);
				m_pYACData->uModb = Addr.a.m_Offset;
				m_pYACData->uMPar = (*m_pRanges)[1];

				LoadCombos(1);

				YACParamEnables(Addr.m_Ref);
				}

			if( !m_fPart ) {

				OnSpaceChange(1001, Focus);

				return FALSE;
				}
			}
	
		if( !m_fPart ) {

			if( m_pSpace ) {

				ShowAddress(Addr);

				ShowType(Addr.a.m_Type);

				SetAddressFocus();

				YACParamEnables(Addr.m_Ref);

				return FALSE;
				}

			YACParamEnables(ENBNONE);

			return TRUE;
			}

		else {
			LoadType();

			ShowAddress(Addr);

			ShowType(Addr.a.m_Type);

			if( TRUE ) {

				CString Text = m_pSpace->m_Prefix;

				Text += GetAddressText();

				Text += GetTypeText();

				CError   Error(FALSE);

				CAddress Addr;
			
				if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

					CAddress Addr;

					m_pSpace->GetMinimum(Addr);

					ShowAddress(Addr);
					}
				}

			YACParamEnables(Addr.m_Ref);

			SetAddressFocus();

			return FALSE;
			}
		}

	return FALSE;
	}

// Notification Handlers

void CYaskawaACDrivesAddrDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CYaskawaACDrivesAddrDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = (INDEX)ListBox.GetItemData(uPos);

	if( DWORD(Index) != NOTHING ) {

		m_pSpace = m_pDriver->GetSpace(Index);

		LoadType();

		UINT uTable = m_pSpace->m_uTable;

		BOOL f      = IsYACParam(uTable);

		CAddress Addr;

		if( f ) {

			m_pGDrv->SetItemRanges(uTable);

			m_pRanges = m_pGDrv->GetRanges();

			m_pYACData->uMAlp = m_pGDrv->GetAlphaFromTable(uTable);

			LoadCombos(1);

			ShowParamData(m_pSpace->m_uType);

			ShowDetails();

			Addr.a.m_Table	= uTable;
			Addr.a.m_Offset	= m_pYACData->uModb;
			Addr.a.m_Extra	= 0;
			Addr.a.m_Type	= m_pSpace->m_uType;
			}

		else {
			Addr.a.m_Type = GetTypeCode();

			m_pSpace->GetMinimum(Addr);

			CStdAddrDialog::ShowAddress(Addr);

			ShowDetails();
			}

		YACParamEnables(Addr.m_Ref);
		}

	else {
		ClearAddress();
		ClearDetails();

		YACParamEnables(ENBNONE);

		m_pSpace = NULL;
		}
	}

void CYaskawaACDrivesAddrDialog::OnComboChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		UINT uType = GetTypeCode();

		ShowParamData(uType);

		LoadCBMenuNumbers(m_pYACData->uMPar / 100);
		LoadCBParameters (m_pYACData->uMPar % 100);

		CAddress Addr;

		Addr.a.m_Table	= GetTableFromAlpha(m_pYACData->uMAlp);
		Addr.a.m_Offset	= m_pYACData->uModb;
		Addr.a.m_Extra	= 0;
		Addr.a.m_Type	= uType;

		YACParamEnables(Addr.m_Ref);

		SetDlgFocus(uID);
		}

	else SetDlgFocus(1001);
	}

void CYaskawaACDrivesAddrDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	CStdAddrDialog::OnTypeChange(uID, Wnd);
	}

BOOL CYaskawaACDrivesAddrDialog::OnUpdateAddress(UINT uID)
{
	if( m_pSpace ) {

		if( IsModbus(m_pSpace->m_uTable) ) {

			CAddress Addr;

			Addr.a.m_Table	= m_pSpace->m_uTable;
			Addr.a.m_Offset	= tatoi(GetDlgItem(2002).GetWindowText());
			Addr.a.m_Extra	= 0;
			Addr.a.m_Type	= GetTypeCode();

			YACParamEnables(Addr.m_Ref);
			}
		}

	return TRUE;
	}

void CYaskawaACDrivesAddrDialog::LoadType(void)
{
	CStdAddrDialog::LoadType();
	}

// Command Handlers
BOOL CYaskawaACDrivesAddrDialog::OnOkay(UINT uID)
{
	if( m_pSpace ) {

		CString Text = m_pSpace->m_Prefix;

		CString s    = TEXT("");

		BOOL fYAC    = TRUE;

		switch( m_pSpace->m_uTable ) {

			case SPDA0:
			case SPDA1:
			case SPDA3:
			case SPDA4:
				Text += GetAddressText();
				Text += GetTypeText();

				fYAC  = FALSE;
				break;

			case SPEN:
			case SPAC:
			case SPEC:
			case SPEV:
				break;

			default:
				Text.Printf( TEXT("%s%1.1d_%d"),

					Text,
					GetBoxData(DLGN),
					GetBoxData(DLGP)
					);

				Text += GetTypeText();

				break;
			}

		CError   Error(TRUE);

		CAddress Addr;

		if( !m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

			Addr.a.m_Table	= m_pSpace->m_uTable;
			Addr.a.m_Offset	= fYAC ? 0x101 : IsModbus(m_pSpace->m_uTable) ? 40000 : 0;
			Addr.a.m_Type	= m_pSpace->m_uType;
			Addr.a.m_Extra	= 0;
			}

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Overrides
BOOL CYaskawaACDrivesAddrDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsBit:
		case addrWordAsWord:
		case addrWordAsLong:
		case addrWordAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CYaskawaACDrivesAddrDialog::AllowSpace(CSpace * pSpace)
{
	if( pSpace ) {

		m_pGDrv->SetItemRanges(pSpace->m_uTable);

		m_pRanges = m_pGDrv->GetRanges();

		if( (*m_pRanges)[0] == 0xFFFF ) {

			return FALSE;
			}
		}

	return TRUE;
	}

// Selection Handling
void CYaskawaACDrivesAddrDialog::LoadCombos(UINT uMenu)
{	
	LoadCBMenuNumbers(uMenu);
	LoadCBParameters(0);
	}

void CYaskawaACDrivesAddrDialog::LoadCBMenuNumbers(UINT uMenu)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DLGN);

	ClearBox(DLGN);

	CRangeArray p = *m_pRanges;

	for( UINT n = 1; n < 10; n++ ) {

		UINT j = 1;

		CString sCount;

		while( j < m_pYACData->uSize ) {

			UINT uThis = p[j] / 100;

			if( uThis == n ) {

				sCount.Printf(TEXT("%d"), n);

				Box.AddString(sCount, n);

				break;
				}

			j += 3;
			}
		}

	uMenu = FindCBPosition(DLGN, uMenu);

	Box.SetCurSel(uMenu);
	}

void CYaskawaACDrivesAddrDialog::LoadCBParameters(UINT uSelect)
{
	CComboBox & Box	= (CComboBox &)GetDlgItem(DLGP);

	UINT uMenu	= GetBoxData(DLGN);

	CRangeArray p	= *m_pRanges;

	UINT n = 1;

	ClearBox(DLGP);

	while( n < m_pYACData->uSize ) {

		CString sCount;

		UINT uSel = p[n] / 100;

		if( uSel == uMenu ) {

			UINT uStart = p[n] % 100;
			UINT uEnd   = uStart + p[n+1];

			for( UINT k = uStart; k < uEnd; k++ ) {

				sCount.Printf(TEXT("%2.2d"), k);

				Box.AddString(sCount, k);
				}
			}

		if( uSel > uMenu ) {

			break;
			}

		else {
			n += 3;
			}
		}

	uSelect = FindCBPosition(DLGP, uSelect);

	Box.SetCurSel(uSelect);
	}

void CYaskawaACDrivesAddrDialog::SetBoxPosition(UINT uID, UINT uPos)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	Box.SetCurSel(uPos);
	}

UINT CYaskawaACDrivesAddrDialog::GetBoxPosition(UINT uID)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return Box.GetCurSel();
	}

void CYaskawaACDrivesAddrDialog::GetParamData(void)
{
	UINT uMenu = 100 * GetBoxData(DLGN);

	m_pYACData->uMPar = uMenu + GetBoxData(DLGP);
	}

void CYaskawaACDrivesAddrDialog::PutParamData(void)
{
	GetParamData();

	m_pGDrv->SetYACMPar(m_pYACData->uMPar);
	}

void CYaskawaACDrivesAddrDialog::ShowParamData(UINT uType)
{
	GetParamData();

	CAddress Addr;

	Addr.a.m_Table	= GetTableFromAlpha(m_pYACData->uMAlp);
	Addr.a.m_Extra	= 0;
	Addr.a.m_Type	= uType;

	Addr.a.m_Offset = m_pGDrv->ParamToModbus() ? m_pYACData->uModb : (*m_pRanges)[0];

	ShowAddress(Addr);

	ShowDetails();

	CString s;

	s.Printf( TEXT("%4.4X"), Addr.a.m_Offset );

	GetDlgItem(2024).SetWindowText(s);
	}

// Helpers

void CYaskawaACDrivesAddrDialog::ClearBox(CComboBox & ccb)
{
	UINT uCount = ccb.GetCount();

	for( UINT i = 0; i < uCount; i++ ) {

		ccb.DeleteString(0);
		}
	}

void CYaskawaACDrivesAddrDialog::ClearBox(UINT uID)
{
	ClearBox((CComboBox &)GetDlgItem(uID));
	}

BOOL CYaskawaACDrivesAddrDialog::IsYACParam(UINT uTable)
{
	return uTable >= SPA && uTable <= SPQ;
	}

BOOL CYaskawaACDrivesAddrDialog::IsModbus(UINT uTable)
{
	switch( uTable ) {

		case SPDA0:
		case SPDA1:
		case SPDA3:
		case SPDA4:
			return TRUE;
		}

	return FALSE;
	}

UINT CYaskawaACDrivesAddrDialog::GetBoxData( UINT uID )
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	return (UINT)Box.GetCurSelData();
	}

UINT CYaskawaACDrivesAddrDialog::FindCBPosition(UINT uID, UINT uValue)
{
	CComboBox & Box = (CComboBox &)GetDlgItem(uID);

	UINT uEnd = uID == DLGN ? 10 : 100;

	UINT i = 0;

	while( i < uEnd ) {

		UINT u = (UINT)Box.GetItemData(i);

		if( u == uValue ) {

			return i;
			}

		i++;
		}

	return 0;
	}

void CYaskawaACDrivesAddrDialog::YACParamEnables(DWORD dAddr)
{
	CAddress Addr;

	Addr.m_Ref   = dAddr;

	UINT uTable  = Addr.a.m_Table;

	UINT uEnable = IsYACParam(uTable) ? ENBPARM : IsModbus(uTable) ? ENBMDBS : ENBNONE;

	GetDlgItem(2002).EnableWindow(uEnable == ENBMDBS);
	GetDlgItem(DLGN).EnableWindow(uEnable == ENBPARM);
	GetDlgItem(DLGP).EnableWindow(uEnable == ENBPARM);
	GetDlgItem(DLGM).EnableWindow(uEnable == ENBPARM);
	GetDlgItem(DLGH).EnableWindow(uEnable == ENBPARM);

	if( uEnable != ENBPARM ) {

		GetDlgItem(DLGH).SetWindowText( TEXT("") );
		}

	if( uEnable == ENBNONE ) {

		CString s;

		switch( Addr.a.m_Table ) {

			case SPEN: s = TEXT("900"); break;
			case SPAC: s = TEXT("910"); break;
			case SPEC:
			case SPEV: s = TEXT("0");   break;
			default:   s = TEXT("");    break;
			}

		GetDlgItem(2002).SetWindowText(s);
		}
	}

UINT CYaskawaACDrivesAddrDialog::GetTableFromAlpha(UINT uAlpha)
{
	switch( uAlpha ) {

		case 'A': return SPA;
		case 'B': return SPB;
		case 'C': return SPC;
		case 'D': return SPD;
		case 'E': return SPE;
		case 'F': return SPF;
		case 'H': return SPH;
		case 'L': return SPL;
		case 'N': return SPN;
		case 'O': return SPO;
		case 'P': return SPP;
		case 'Q': return SPQ;
		case 'T': return SPT;
		case 'U': return SPU;
		}

	return SPA;
	}

// more override
void CYaskawaACDrivesAddrDialog::ShowDetails(void)
{
	if( !IsYACParam(m_pSpace->m_uTable) ) {

		CStdAddrDialog::ShowDetails();

		return;
		}

	GetDlgItem(3002).SetWindowText(m_pSpace->GetNativeText());

	CString  Min;

	CString  Max;

	CString  Rad;

	CRangeArray p	=  *m_pRanges;

	UINT uMenu	= GetBoxData(DLGN);

	UINT i		= 1;

	UINT uPos	= 1;

	while( i < m_pYACData->uSize ) {

		UINT uMPar = p[i] / 100;

		if( uMPar && uMPar < uMenu ) {

			i += 3;

			continue;
			}

		uPos = i;

		break;
		}

	UINT uPar = p[uPos];

	Min.Printf( TEXT("%1.1d_%2.2d"), uPar / 100, uPar % 100 );

	while( i < m_pYACData->uSize ) {

		UINT uNext = p[i+3] / 100;

		if( uNext && uNext <= uMenu ) {

			i += 3;

			continue;
			}

		uPos = i;

		uPar = p[uPos];

		break;
		}

	Max.Printf( TEXT("%1.1d_%2.2d"), uPar / 100, (uPar + p[uPos+1] - 1) % 100);

	Rad = m_pSpace->GetRadixAsText();

	GetDlgItem(3004).SetWindowText(Min);
	
	GetDlgItem(3006).SetWindowText(Max);

	GetDlgItem(3008).SetWindowText(Rad);
	}

// End of File
