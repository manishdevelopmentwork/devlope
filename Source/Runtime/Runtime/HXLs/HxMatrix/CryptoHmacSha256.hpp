
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHmacSha256_HPP

#define INCLUDE_CryptoHmacSha256_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHmac.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA256 Cryptographic HMAC
//

class CCryptoHmacSha256 : public CCryptoHmac
{
	public:
		// Constructor
		CCryptoHmacSha256(void);

		// ICryptoHmac
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE           m_bHash[SHA256_HASH_SIZE];
		psHmacSha256_t m_Ctx;
	};

// End of File

#endif
