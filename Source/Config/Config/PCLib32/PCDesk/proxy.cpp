
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Proxy View Window
//

// Runtime Class

AfxImplementRuntimeClass(CProxyViewWnd, CViewWnd);

// Constructor

CProxyViewWnd::CProxyViewWnd(void)
{
	m_pView = NULL;
	}

// Destructor

CProxyViewWnd::~CProxyViewWnd(void)
{
	}

// Overribables

void CProxyViewWnd::OnAttach(void)
{
	m_pView->Attach(m_pItem);
	}

CString CProxyViewWnd::OnGetNavPos(void)
{
	return m_pView->GetNavPos();
	}

BOOL CProxyViewWnd::OnNavigate(CString const &Nav)
{
	return m_pView->Navigate(Nav);
	}

void CProxyViewWnd::OnExec(CCmd *pCmd)
{
	m_pView->ExecCmd(pCmd);
	}

void CProxyViewWnd::OnUndo(CCmd *pCmd)
{
	m_pView->UndoCmd(pCmd);
	}

// Routing Control

BOOL CProxyViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView ) {

		if( m_pView->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CProxyViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_COMMAND)
 	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_SETCURRENT)
 	AfxDispatchMessage(WM_SIZE)
 	AfxDispatchMessage(WM_SHOWWINDOW)

	AfxMessageEnd(CProxyViewWnd)
	};

// Message Handlers

void CProxyViewWnd::OnPostCreate(void)
{
	CRect Rect = GetClientRect();

	m_pView->Create( L"",
			 WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS,
			 Rect,
			 ThisObject,
			 IDVIEW,
			 NULL
			 );

	m_pView->ShowWindow(SW_SHOW);
	}

BOOL CProxyViewWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	if( uID >= 0x8000 && uID <= 0xEFFF ) {

		RouteCommand(uID, m_MsgCtx.Msg.lParam);

		return TRUE;
		}
	
	SendMessage(WM_AFX_COMMAND, uID);

	return TRUE;
	}

void CProxyViewWnd::OnSetFocus(CWnd &Wnd)
{
	m_pView->SetFocus();
	}

void CProxyViewWnd::OnSetCurrent(BOOL fCurrent)
{
	m_pView->SendMessage(WM_SETCURRENT, fCurrent);
	}

void CProxyViewWnd::OnSize(UINT uCode, CSize Size)
{
	CRect Rect = GetClientRect();

	m_pView->MoveWindow(Rect, TRUE);

	Invalidate(TRUE);
	}

void CProxyViewWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	m_pView->SendMessage(WM_SHOWWINDOW, m_MsgCtx.Msg.wParam, m_MsgCtx.Msg.lParam);
	}

// End of File
