#include "basefp2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Command Codes
//

#define CC_WR_DATA	0xD0
#define CC_RD_DATA	0xD1
#define CC_WR_CONTACT	0xD2
#define CC_RD_CONTACT	0xD3

//////////////////////////////////////////////////////////////////////////
//
// Area Codes
//

#define AREA_WL		0x00
#define AREA_WR		0x01
#define AREA_WY		0x02
#define AREA_WX		0x03
#define AREA_SV		0x04
#define AREA_EV		0x05
#define AREA_LD		0x06
#define AREA_SWR	0x07
#define AREA_SDT	0x08
#define AREA_DT		0x09
#define AREA_FL		0x0A

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP via MEWTOCOL-DAT Base Driver
//

class CMatFPDatBaseDriver : public CMatFP2BaseDriver {

	public:
		// Constructor
		CMatFPDatBaseDriver(void);

		// Destructor
		~CMatFPDatBaseDriver(void);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
				
		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
						
		// Overrideables
		virtual	void StartFrame(void);

		// Implementation
		void AddWord(WORD wWord);
		
		// Helpers
		UINT FindCommandCode(UINT uSpace, BOOL fWrite);
		UINT FindAreaCode(UINT uSpace);

		
	};

//////////////////////////////////////////////////////////////////////////
//
// Matsushita FP via MEWTOCOL-DAT Serial Master
//

class CMatFPDatSerialDriver : public CMatFPDatBaseDriver {

	public:
		// Constructor
		CMatFPDatSerialDriver(void);

		// Destructor
		~CMatFPDatSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
					
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:

		// Transport
		BOOL SendFrame(void);
		BOOL GetFrame(void);
		BOOL CheckReply(UINT uCount);
			       
		// Overridables
		BOOL Transact(void);
		

		
	};

// End of File
