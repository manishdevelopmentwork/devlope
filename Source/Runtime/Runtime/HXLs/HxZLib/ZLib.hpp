
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ZLib_HPP

#define	INCLUDE_ZLib_HPP

//////////////////////////////////////////////////////////////////////////
//
// ZLib Object
//

class CZLib : public IZLib
{
	public:
		// Constructor
		CZLib(void);

		// Destructor
		~CZLib(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		
		// IZLib
		UINT METHOD Expand  (PCBYTE pIn, UINT uIn, PBYTE pOut, UINT uOut);
		UINT METHOD Compress(PCBYTE pIn, UINT uIn, PBYTE pOut, UINT uOut);

	protected:
		// Data Methods
		ULONG m_uRefs;
	};

// End of File

#endif

