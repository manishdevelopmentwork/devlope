
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// W5EditRes Object
//

// Static Data

CEditResource * CEditResource::m_pThis	= NULL;

// Object Location

CEditResource * CEditResource::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructor

CEditResource::CEditResource(void)
{
	AfxAssert(m_pThis == NULL);

	LoadLib(L"W5EditRes");

	m_pThis		= this;
	}

// Destructor

CEditResource::~CEditResource(void)
{
	FreeLib();
	}

// Operations

CString CEditResource::SelBlockEx(HWND hWnd, PCTXT pPath, PCTXT pName, int &nIn, int &nOut, DWORD &dwIdent, BOOL &fInst, BOOL &fDb)
{
	LPCSTR pBlock = m_pSelBlockEx( hWnd, 
				       LPCSTR(CAnsiString(pPath)), 
				       LPCSTR(CAnsiString(pName)),
				       &nIn, 
				       &nOut, 
				       &dwIdent, 
				       &fInst, 
				       &fDb
				       );

	return pBlock ? CString(pBlock) : CString();
	}

// Library Management

void CEditResource::LoadLib(PCTXT pName)
{
	CFilename Path = afxModule->GetFilename().GetDirectory();

	CFilename Name = Path + L"Straton\\" + pName;

	if( (m_hLib = LoadLibrary(Name)) ) {

		m_pSelBlockEx = (PPW5RES_SelBlockEx) GetProcAddress(m_hLib, "W5RES_SelBlockEx");

		AfxAssert(m_pSelBlockEx);

		return;
		}

	DWORD dwError = GetLastError();

	AfxTrace(L"ERROR: Failed to load %s\t%8.8X\n", pName, dwError);

	AfxAssert(m_hLib);
	}

void CEditResource::FreeLib(void)
{
	AfxVerify(FreeLibrary(m_hLib));
	}

//////////////////////////////////////////////////////////////////////////
//
// W5EditRes APIs
//

global CString Straton_SelBlockEx(HWND hWnd, PCTXT pPath, PCTXT pName, int &nIn, int &nOut, DWORD &dwIdent, BOOL &fInst, BOOL &fDB)
{
	return CEditResource::FindInstance()->SelBlockEx( hWnd, 
							  pPath, 
							  pName,
							  nIn,
							  nOut,
							  dwIdent,
							  fInst,
							  fDB
							  );
	}

// End of File
