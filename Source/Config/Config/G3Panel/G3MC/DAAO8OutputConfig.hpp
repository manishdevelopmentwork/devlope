
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAAO8OutputConfig_HPP

#define INCLUDE_DAAO8OutputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 AI Configuration
//

class CDAAO8OutputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAAO8OutputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Group Names
	CString GetGroupName(WORD Group);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	UINT	m_Type1;
	UINT	m_Type2;
	UINT	m_Type3;
	UINT	m_Type4;
	UINT	m_Type5;
	UINT	m_Type6;
	UINT	m_Type7;
	UINT	m_Type8;

	UINT	m_DP1;
	UINT	m_DP2;
	UINT	m_DP3;
	UINT	m_DP4;
	UINT	m_DP5;
	UINT	m_DP6;
	UINT	m_DP7;
	UINT	m_DP8;

	INT	m_DataLo1;
	INT	m_DataLo2;
	INT	m_DataLo3;
	INT	m_DataLo4;
	INT	m_DataLo5;
	INT	m_DataLo6;
	INT	m_DataLo7;
	INT	m_DataLo8;

	INT	m_DataHi1;
	INT	m_DataHi2;
	INT	m_DataHi3;
	INT	m_DataHi4;
	INT	m_DataHi5;
	INT	m_DataHi6;
	INT	m_DataHi7;
	INT	m_DataHi8;

	INT	m_OutputLo1;
	INT	m_OutputLo2;
	INT	m_OutputLo3;
	INT	m_OutputLo4;
	INT	m_OutputLo5;
	INT	m_OutputLo6;
	INT	m_OutputLo7;
	INT	m_OutputLo8;

	INT	m_OutputHi1;
	INT	m_OutputHi2;
	INT	m_OutputHi3;
	INT	m_OutputHi4;
	INT	m_OutputHi5;
	INT	m_OutputHi6;
	INT	m_OutputHi7;
	INT	m_OutputHi8;

	INT	m_DataInit1;
	INT	m_DataInit2;
	INT	m_DataInit3;
	INT	m_DataInit4;
	INT	m_DataInit5;
	INT	m_DataInit6;
	INT	m_DataInit7;
	INT	m_DataInit8;

	UINT	m_InitData;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Property Filter
	BOOL IncludeProp(WORD PropID);

	// Meta Data Creation
	void AddMetaData(void);

	// Implementation
	void ConvertType(UINT uIndex);
};

// End of File

#endif
