
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2001 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Dialog Resource Loader
//

// Runtime Class

AfxImplementRuntimeClass(CDlgLoader, CObject)

// Constructor

CDlgLoader::CDlgLoader(void)
{
	m_hData = NULL;
	}

// Operations

void CDlgLoader::SetOrigin(CPoint Org)
{
	m_Org = Org;
	}

void CDlgLoader::SetName(ENTITY ID)
{
	m_Name = ID;
	}

// Resource Loading

BOOL CDlgLoader::Load(void)
{
	return LoadTemplate();
	}

BOOL CDlgLoader::Process(CDialog &Dlg, BOOL fCaption)
{
	UINT uCount = ReadHeader(Dlg, fCaption);

	for( UINT n = 0; n < uCount; n++ ) {

		MakeControl(Dlg);
		}

	return TRUE;
	}

void CDlgLoader::Free(void)
{
	FreeTemplate();
	}

// Implementation

BOOL CDlgLoader::LoadTemplate(void)
{
	WORD Lang = LANGIDFROMLCID(GetThreadLocale());

	UINT Prim = PRIMARYLANGID(Lang);

	if( !(Prim == LANG_ENGLISH || Prim == LANG_CHINESE) ) {

		CString Name = CString(m_Name.GetName()) + L"_Wide";

		CEntity Wide = CEntity(Name, m_Name.GetModule());

		m_hData = CModule::LoadResource(Wide, RT_DIALOG);
		}

	if( !m_hData ) {

		m_hData = CModule::LoadResource(m_Name, RT_DIALOG);
		}
		
	if( m_hData ) {
		
		m_pData = (BYTE *) LockResource(m_hData);
		
		if( !m_pData ) {

			FreeResource(m_hData);
			
			return FALSE;
			}
			
		return TRUE;
		}
	
	return FALSE;
	}

void CDlgLoader::FreeTemplate(void)
{
	AfxAssert(m_hData);
	
	UnlockResource(m_hData);
	
	FreeResource(m_hData);
	}

UINT CDlgLoader::ReadHeader(CDialog &Dlg, BOOL fCaption)
{
	DLGTEMPLATE *pHeader = (DLGTEMPLATE *) m_pData;

	WCHAR *pText = (WCHAR *) (pHeader + 1);

	CString s1 = ReadText(pText, 2);
	
	CString s2 = ReadText(pText, 2);
	
	CString s3 = ReadText(pText, 2);

	AfxAssert(s1.IsEmpty());

	AfxAssert(s2.IsEmpty());

	if( fCaption && !s3.IsEmpty() ) {

		Dlg.SetWindowText(s3);
		}

	m_pData = (BYTE *) pText;

	return pHeader->cdit;
	}

void CDlgLoader::MakeControl(CDialog &Dlg)
{
	m_pData = (BYTE *) Align(m_pData, 4);

	DLGITEMTEMPLATE *pItem = (DLGITEMTEMPLATE *) m_pData;

	WCHAR *pText = (WCHAR *) (pItem + 1);

	CString s1 = ReadText(pText, 2);
	
	CString s2 = ReadText(pText, 2);
	
	CString s3 = ReadText(pText, 2);

	CLASS Type = FindControlClass(s1);

	if( Type ) {
	
		CPoint Pos = Dlg.FromDlgUnits(CPoint(pItem->x, pItem->y)) + m_Org;
			
		CSize Size = Dlg.FromDlgUnits(CSize(pItem->cx, pItem->cy));

		CWnd *pWnd = AfxNewObject(CWnd, Type);
			
		pWnd->Create( s2,
			      pItem->dwExtendedStyle,
			      pItem->style,
			      CRect(Pos, Size),
			      Dlg,
			      pItem->id,
			      NULL
			      );

		pWnd->SetFont(afxFont(Dialog));

		if( !pWnd->HasMessageMap() ) {

			delete pWnd;
			}
		}
	else {
		UINT uID = pItem->id;

		AfxTrace(L"ERROR: Failed to load control %u\n", uID);
		}

	m_pData = (BYTE *) pText;
	}

// String Access

CString CDlgLoader::ReadText(WCHAR * &pText, UINT uFactor)
{
	pText = Align(pText, uFactor);

	if( pText[0] == 0xFFFF ) {

		CPrintf Text(L"%c%c", 0xF0AD, pText[1]);

		pText += 2;

		return Text;
		}
	else {
		CString Text(pText);

		pText += 1;

		pText += Text.GetLength();

		return Text;
		}
	}

// Pointer Alignment

BYTE * CDlgLoader::Align(BYTE *pData, UINT uFactor)
{
	return (BYTE *) ((UINT(pData) + uFactor - 1) & ~(uFactor - 1));
	}

WCHAR * CDlgLoader::Align(WCHAR *pData, UINT uFactor)
{
	return (WCHAR *) ((UINT(pData) + uFactor - 1) & ~(uFactor - 1));
	}

// Control Location

CLASS CDlgLoader::FindControlClass(PCTXT pName)
{
	if( pName[0] == 0xF0AD ) {
	
		switch( pName[1] ) {
		
			case 0x80: return AfxRuntimeClass(CButton);
			case 0x81: return AfxRuntimeClass(CEditCtrl);
			case 0x82: return AfxRuntimeClass(CStatic);
			case 0x83: return AfxRuntimeClass(CListBox);
			case 0x84: return AfxRuntimeClass(CScrollBar);
			case 0x85: return AfxRuntimeClass(CComboBox);

			}
			
		return NULL;
		}

	return AfxNamedClass(CEntity(pName, m_Name.GetModule()));
	}

// End of File
