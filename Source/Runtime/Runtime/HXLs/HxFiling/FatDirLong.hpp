
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FatDirLong_HPP

#define	INCLUDE_FatDirLong_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Directory Entry
//

#pragma pack(1)

struct FatDirLong
{
	BYTE	m_bOrder;
	WORD	m_wName1[5];
	BYTE	m_bAttribute;
	BYTE	m_bType;
	BYTE	m_bChecksum;
	WORD	m_wName2[6];
	WORD	m_wZero;
	WORD	m_wName3[2];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Long Entry
//

class CFatDirLong : public FatDirLong
{
	public:
		// Constructor
		CFatDirLong(void);

		// Initialisation
		void Init(void);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL IsValid(void) const;
		BOOL IsEmpty(void) const;
		BOOL IsFree(void) const;
		BOOL IsLong(void) const;
		BOOL IsFirst(void) const;
		BOOL IsLast(void) const;
		BYTE GetOrder(void) const;

		// Operations
		void MakeEmpty(void);
		void MakeFree(void);
		void MakeLast(void);
		UINT GetLength(void) const;
		BOOL GetText(PTXT pText) const;
		UINT SetText(PCTXT pText);

		// Dump
		void Dump(void) const;

	protected:
		// Constants
		enum
		{
			constFree	= 0xE5,
			constLastLong	= 0x40,
			};

		// Attributes
		enum
		{
			attrReadOnly	= Bit(0),
			attrHidden	= Bit(1),
			attrSystem	= Bit(2),
			attrVolume	= Bit(3),
			attrDir		= Bit(4),
			attrArchive	= Bit(5),
			attrLong	= attrReadOnly | attrHidden | attrSystem | attrVolume,
			attrLongMask	= attrReadOnly | attrHidden | attrSystem | attrVolume | attrDir | attrArchive,
			};

		// Implementation
		void HostToLocal(PWORD pData, UINT uCount);
		void LocalToHost(PWORD pData, UINT uCount);
		BOOL CheckText(PCWORD pData, UINT uCount) const;   
		void ClearText(PWORD pData, UINT uCount);
		UINT GetTextLength(PCWORD pData, UINT uCount) const; 
		UINT GetText(PTXT pText, PCWORD pData, UINT uCount) const;
		UINT SetText(PCTXT pText, PWORD pData, UINT uCount);
	};

// End of File

#endif

