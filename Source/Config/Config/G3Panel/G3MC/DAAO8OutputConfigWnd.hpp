
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAAO8OutputConfigWnd_HPP

#define INCLUDE_DAAO8OutputConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAAO8OutputConfig;

class CDAAO8Module;

//////////////////////////////////////////////////////////////////////////
//
// DAAO8 AI Config Window
//

class CDAAO8OutputConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAAO8OutputConfigWnd(UINT uPage);

protected:
	// Data Members
	CDAAO8Module       * m_pModule;
	CDAAO8OutputConfig * m_pItem;
	UINT		     m_uPage;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Implementation
	void AddOutputs(void);

	// Data Access
	UINT GetInteger(CMetaItem *pItem, CString Tag);
	void PutInteger(CMetaItem *pItem, CString Tag, UINT Data);

	// Enabling
	void DoEnables(void);
};

// End of File

#endif
