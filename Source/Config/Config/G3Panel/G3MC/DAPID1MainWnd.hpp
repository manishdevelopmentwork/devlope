
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAPID1MainWnd_HPP

#define INCLUDE_DAPID1MainWnd_HPP

#include "slcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAPID1Module;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO Module Window
//

class CDAPID1MainWnd : public CSLCMainWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDAPID1MainWnd(void);

		// Overribables
		void OnAttach(void);

	protected:
		// Implementation
		void AddIdentPage(void);
	};

// End of File

#endif
