
//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Spaces
//

#define	SPACE_HOLD	0x01

#define	PING_REGISTER	40615 // Drive Enable

#define	WORD_OFFSET	40001

#define	LONG_OFFSET	23617 // 40001 - 16384

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Driver
//

class CEmersonSPPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmersonSPPDriver(void);

		// Destructor
		~CEmersonSPPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BOOL m_fDisableCheck;
			BOOL m_fIgnoreReadEx;
			};

		// Data Members

		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		CRC16	   m_CRC;
		UINT	   m_uTimeout;
				
		// Implementation
		void AllocBuffers(void);
		void FreeBuffers(void);
		BOOL IgnoreException(void);

		// String Item Handling
		BOOL StringAllocBad(AREF Addr);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);

		// Port Access
		UINT RxByte(UINT uTime);
		
		// Transport Layer
		BOOL PutFrame(void);
		BOOL GetFrame(BOOL fWrite);
		BOOL BinaryTx(void);
		BOOL BinaryRx(BOOL fWrite);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckReply(BOOL fIgnore);

		// Read Handlers
		CCODE DoWordRead(UINT uOff, PDWORD pData, UINT uCount);
		CCODE DoLongRead(UINT uOff, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(UINT uOff, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(UINT uOff, PDWORD pData, UINT uCount);

		// Helpers
	};

// End of File
