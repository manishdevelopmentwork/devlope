
#include "Intern.hpp"

#include "SqlFilter.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "SqlColumn.hpp"
#include "SqlColumnList.hpp"
#include "SqlFilterList.hpp"
#include "SqlFilterPage.hpp"
#include "SqlFilterUIItemViewWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Filter
//

// Dynamic Class

AfxImplementDynamicClass(CSqlFilter, CCodedHost);

// Constructors

CSqlFilter::CSqlFilter(void)
{
	m_NDX      = 0;

	m_Column   = 0;

	m_Text     = "";

	m_Operator = 0;

	m_pValue   = NULL;

	m_Binding  = bindAnd;
	}

CSqlFilter::CSqlFilter(CSqlQuery * pQuery)
{
	m_NDX      = 0;

	m_Column   = 0;

	m_Text     = "";

	m_Operator = 0;

	m_pValue   = NULL;

	m_Binding  = bindAnd;
	}

CSqlFilter::CSqlFilter(CSqlQuery * pQuery, UINT uColumn, CString const &Filter)
{
	m_NDX      = 0;

	m_Column   = uColumn;

	m_Operator = 0;

	m_Text     = Filter;

	m_pValue   = NULL;

	m_Binding  = bindAnd;
	}

// Meta Data Creation

void CSqlFilter::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddString(Text);

	Meta_AddString(ColName);

	Meta_AddInteger(Column);

	Meta_AddInteger(Operator);

	Meta_AddVirtual(Value);

	Meta_AddInteger(Binding);

	Meta_AddInteger(NDX);
	}

// Item Naming

CString CSqlFilter::GetHumanName(void) const
{
	CSqlFilterList *pList = (CSqlFilterList *) GetParent(AfxRuntimeClass(CSqlFilterList));

	UINT uPos = pList->FindItemPos(this);

	return CPrintf("Condition %u", uPos + 1);
	}

// Download Support

void CSqlFilter::PrepareData(void)
{
	CCodedHost::PrepareData();

	CSqlQuery *pQuery = (CSqlQuery *) GetParent(AfxRuntimeClass(CSqlQuery));

	m_Column = pQuery->GetColumnPos(m_ColName);
	}

BOOL CSqlFilter::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(m_Column);

	Init.AddByte(BYTE(m_Operator));

	Init.AddItem(itemVirtual, m_pValue);

	Init.AddByte(BYTE(m_Binding));

	return TRUE;
	}

// UI Creation

BOOL CSqlFilter::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CSqlFilterPage(this));

	return FALSE;
	}

// UI Management

CViewWnd * CSqlFilter::CreateView(UINT uType)
{
	CUIPageList *pList = New CUIPageList;

	OnLoadPages(pList);

	CSqlQuery *pQuery = (CSqlQuery *) GetParent(AfxRuntimeClass(CSqlQuery));

	return New CSqlFilterUIItemViewWnd(pQuery, pList, TRUE);
	}

// Type Access

BOOL CSqlFilter::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Value" ) {

		CSqlQuery *pQuery = (CSqlQuery *) GetParent(AfxRuntimeClass(CSqlQuery));

		if( pQuery ) {

			CSqlColumn *pColumn = pQuery->GetColumn(m_Column);

			if( pColumn ) {

				Type.m_Flags = 0;

				Type.m_Type  = pColumn->GetC3Type();

				return TRUE;
				}
			}
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Helpers

BOOL CSqlFilter::NeedsOperand(void)
{
	switch( m_Operator ) {

		case opNull:
		case opNotNull:

			return FALSE;
		}

	return TRUE;
	}

CString CSqlFilter::GetOperator(void)
{
	PCTXT pOps[] = { L"=",          
			 L"<>",
			 L"<" ,
			 L"<=",				    
			 L">" ,
			 L">=",
			 L"LIKE",
			 L"IS NULL",
			 L"IS NOT NULL"
			};

	return pOps[m_Operator];
	}

CString CSqlFilter::GetBinding(void)
{
	switch( m_Binding ) {

		case bindAnd: return L"AND";

		case bindOr:  return L"OR";
		}

	return L"";
	}


void CSqlFilter::Validate(BOOL fExpand)
{
	UpdateType(NULL, L"Value", m_pValue, TRUE);
	}

BOOL CSqlFilter::IsValid(CError &Error)
{
	if( NeedsOperand() ) {

		if( !m_pValue || m_pValue->IsEmpty() ) {

			Error.Set(CString(IDS("The filter must have a value.")));

			return FALSE;
			}

		if( m_pValue->IsBroken() ) {

			Error.Set(CString(IDS("The filter is invalid.")));

			return FALSE;
			}
		}

	return TRUE;
	}

BOOL CSqlFilter::Check(CStringArray &Errors)
{
	BOOL fOk = TRUE;

	CSqlQuery *pQuery = (CSqlQuery *) GetParent(AfxRuntimeClass(CSqlQuery));

	if( pQuery ) {

		CSqlColumn *pColumn = pQuery->GetColumn(m_ColName);

		if( !pColumn ) {

			CString Error;

			Error += GetHumanPath();

			Error += L": ";

			Error += IDS("Filter condition refers to non-existent column.");

			Error += L"\n";

			Error += GetFixedPath();

			Errors.Append(Error);

			fOk = FALSE;
			}
		}

	if( NeedsOperand() ) {

		if( !m_pValue || (m_pValue && m_pValue->IsBroken()) ) {

			CString Error;

			Error += GetHumanPath();

			Error += L": ";

			Error += IDS("Filter condition is invalid.");

			Error += L"\n";

			Error += GetFixedPath();

			Errors.Append(Error);

			fOk = FALSE;
			}
		}

	return fOk;
	}

void CSqlFilter::Fixup(void)
{
	if( m_ColName.IsEmpty() ) {

		CSqlQuery *pQuery = (CSqlQuery *) GetParent(AfxRuntimeClass(CSqlQuery));

		m_ColName = pQuery->GetColumnSqlName(m_Column);
		}

	if( !m_pValue && !m_Text.IsEmpty() ) {

		m_pValue = New CCodedItem;

		m_pValue->SetParent(this);

		m_pValue->Init();

		UpdateType(NULL, L"Value", m_pValue, TRUE);

		BOOL fString = FALSE;

		if( m_Text.Find(L"'") < NOTHING ) {

			fString = TRUE;
			}

		m_Operator   = FindOperator(m_Text);

		m_Binding    = bindAnd;

		CString Text = StripOperator(m_Text, fString);

		if( NeedsOperand() ) {

			m_pValue->Compile(Text);
			}
		}
}

// Implementation

UINT CSqlFilter::FindOperator(CString const &Text)
{
	PCTXT pOps[] = { L"= ",          
			 L"<> ",
			 L"< " ,
			 L"<= ",				    
			 L"> " ,
			 L">= ",
			 L"LIKE ",
			 L"IS NULL",
			 L"IS NOT NULL"
			};

	for( UINT n = 0; n < elements(pOps); n++ ) {

		CString Op(pOps[n]);
		
		if( Text.StartsWith(pOps[n]) ) {

			return n;
			}
		}

	return 0;
	}

CString CSqlFilter::StripOperator(CString const &Text, BOOL fString)
{
	PCTXT pOps[] = { L"= ",          
			 L"<> ",
			 L"< " ,
			 L"<= ",				    
			 L"> " ,
			 L">= ",
			 L"LIKE ",
			 L"IS NULL",
			 L"IS NOT NULL"
			};

	for( UINT n = 0; n < elements(pOps); n++ ) {

		CString Op(pOps[n]);
		
		if( Text.StartsWith(pOps[n]) ) {

			CString Strip = Text.Mid(Op.GetLength());

			Strip.StripAll();

			if( fString ) {

				Strip.Replace(L"'", L"\"");
				}

			return Strip;
			}
		}

	return L"";
	}

// End of File
