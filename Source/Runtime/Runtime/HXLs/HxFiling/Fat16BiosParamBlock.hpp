
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_Fat16BiosParamBlock_HPP

#define	INCLUDE_Fat16BiosParamBlock_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Objects
//

class CPartitionEntry;

//////////////////////////////////////////////////////////////////////////
//
// Bios Param Block
//

#pragma pack(1)

struct Fat16BiosParamBlock
{
	WORD	m_wSectorSize;
	BYTE	m_bClusterSize;
	WORD	m_wReservedSize;
	BYTE	m_bFatCount;
	WORD	m_wRootSize;
	WORD	m_wTotalSectors;
	BYTE	m_bMedia;
	WORD	m_wFat16Size;
	WORD	m_wTrackSize;
	WORD	m_wHeadCount;
	DWORD	m_dwHiddenSize;
	DWORD	m_dwTotalSectors;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat16 Bios Param Block
//

class CFat16BiosParamBlock : public Fat16BiosParamBlock
{
	public:
		// Constructor
		CFat16BiosParamBlock(void);
		CFat16BiosParamBlock(CFat16BiosParamBlock const &That);

		// Initialisation
		void Init(CPartitionEntry const &Partition);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL  IsValid(void) const;
		UINT  GetClusterSize(void) const;
		UINT  GetRootDirSize(void) const;
		UINT  GetRootDirSectors(void) const;
		INT64 GetDataSize(void) const;
		DWORD GetDataSectors(void) const;
		DWORD GetDataClusters(void) const;
		DWORD GetFirstSector(DWORD dwCluster) const;
		DWORD GetFirstFatSector(WORD wFat) const;
		DWORD GetFirstRootDirSector(void) const;
		DWORD GetFirstDataSector(void) const;
		DWORD GetFatPerSector(void) const;
		DWORD GetTotalSectors(void) const;

		// Dump
		void Dump(void) const;

	protected:
		// Implementation
		BOOL CheckSectorSize(void) const;
		BOOL CheckClusterSize(void) const;
		UINT FindClusterSize(void) const;
		WORD FindFatSize(void) const;
	};

// End of File

#endif

