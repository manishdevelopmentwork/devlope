
#include "intern.hpp"

#include "baldor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Baldor Mint Host Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBaldorSerialDeviceOptions, CUIItem);

// Constructor

CBaldorSerialDeviceOptions::CBaldorSerialDeviceOptions(void)
{
	m_Drop	   = 0;

	m_Protocol = 0;
	}

// UI Managament

void CBaldorSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem ) {

		UINT uHCP = pItem->GetDataAccess("Protocol")->ReadInteger(pItem);

		if( !uHCP && m_Drop > 99 ) {

			m_Drop = 0;

			pWnd->UpdateUI();
			}
		}
	}


// Download Support

BOOL CBaldorSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	Init.AddByte(BYTE(m_Protocol));
       
	return TRUE;
	}

// Meta Data Creation

void CBaldorSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);

	Meta_AddInteger(Protocol);
	}



//////////////////////////////////////////////////////////////////////////
//
// Baldor Mint Host Serial Master Driver
//

// Instantiator

ICommsDriver * Create_BaldorSerialMasterDriver(void)
{
	return New CBaldorSerialMasterDriver;
	}

// Constructor

CBaldorSerialMasterDriver::CBaldorSerialMasterDriver(void)
{
	m_wID		= 0x405B;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Baldor";
	
	m_DriverName	= "Mint Host";
	
	m_Version	= "1.01";
	
	m_ShortName	= "Mint Host";

	AddSpaces(NULL);
	}

// Destructor

CBaldorSerialMasterDriver::~CBaldorSerialMasterDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CBaldorSerialMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CBaldorSerialMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CBaldorSerialMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBaldorSerialDeviceOptions);
	}

// Address Management

BOOL CBaldorSerialMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	DeleteAllSpaces();

	AddSpaces(pConfig);

	return CStdCommsDriver::SelectAddress(hWnd, Addr, pConfig, fPart);
	}

// Implementation

void CBaldorSerialMasterDriver::AddSpaces(CItem * pConfig)
{
	CBaldorSerialDeviceOptions * pOptions = (CBaldorSerialDeviceOptions *) pConfig;
	
	UINT uMax = 0xFF;
	
	if( pOptions && !pOptions->m_Protocol ) {

		uMax = 99;
		}
	
	AddSpace(New CSpace(1,	"Comms", "Comms Array Element",	10, 1, uMax, addrLongAsLong, addrLongAsReal));
	}

// End of File