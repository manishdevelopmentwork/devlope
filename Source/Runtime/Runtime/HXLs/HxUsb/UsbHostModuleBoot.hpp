
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostModuleBoot_HPP

#define	INCLUDE_UsbHostModuleBoot_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncBulkDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Module Boot Driver
//

class CUsbHostModuleBootDriver : public CUsbHostFuncBulkDriver, public IUsbHostModuleBoot
{
	public:
		// Constructor
		CUsbHostModuleBootDriver(void);

		// Destructor
		~CUsbHostModuleBootDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbHostModuleBoot
		UINT METHOD CheckVersion(PCBYTE pVersion, UINT uSize);
		UINT METHOD ClearProgram(UINT uSize);
		UINT METHOD WriteProgram(UINT uAddr, PCBYTE pData, UINT uSize);
		UINT METHOD WriteVersion(PCBYTE pVersion, UINT uSize);
		UINT METHOD StartProgram(void);
		UINT METHOD WaitComplete(void);
	
	protected:
		// Custom Data

		#pragma pack(1)

		struct CB
		{
			DWORD	m_dwSig;
			WORD    m_wTag;
			BYTE    m_bService;
			BYTE	m_bOpcode;
			BYTE	m_bFlags;
			BYTE	m_bDataSize;
			WORD    m_wBulkSize;
			BYTE	m_bData[19];
			};

		struct SB
		{
			DWORD	m_dwSig;
			WORD	m_wTag;
			BYTE	m_bOpcode;
			BYTE	m_bReadBulk;
			WORD	m_wBulkSize;
			};

		#pragma pack()

		// Constants
		enum
		{
			constSigCB		= 0x53544342,
			constSigSB		= 0x53545342,
			};

		// Service Codes
		enum
		{
			servBoot		= 0x01,
			};

		// Standard Operation Codes
		enum
		{
			opNull			= 0x00,
			opAck			= 0x01,
			opNak			= 0x02,
			opReply			= 0x03,
			opReplyFalse		= 0x04,
			opReplyTrue		= 0x05,
			};

		// Boot Operation Codes
		enum
		{
			bootCheckVersion	= 0x10,
			bootClearProgram	= 0x11,
			bootWriteProgram	= 0x12,
			bootWriteVersion	= 0x13,
			bootStartProgram	= 0x14,
			};

		// State
		enum
		{	
			waitDone,
			waitData,
			waitStat,
			};

		// Data
		CB	m_Cb;
		SB	m_Sb;
		UINT	m_wTag;
		bool	m_fWait;

		// Implementation
		bool SendHead(void);
		bool RecvStat(void);
		UINT Transact(void);
		UINT Transact(PBYTE pData, UINT uSize, bool fSend);
	};

// End of File

#endif
