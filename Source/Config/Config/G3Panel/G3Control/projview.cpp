
#include "intern.hpp"

#include "projview.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

/////////////////////////////////////////////////////////////////////////
//
// Control Project Item View
//

// Runtime Class

AfxImplementRuntimeClass(CControlProjectView, CUIItemMultiWnd);

// Constructor

CControlProjectView::CControlProjectView(CUIPageList *pList) : CUIItemMultiWnd(pList, FALSE)
{
	}

// Overridables

void CControlProjectView::OnExec(CCmd *pCmd)
{
	CUIItemMultiWnd::OnExec(pCmd);

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMove)) ) {

		m_pEditor->Exec(pCmd);

		return;
		}
	}

void CControlProjectView::OnUndo(CCmd *pCmd)
{
	CUIItemMultiWnd::OnUndo(pCmd);

	if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMove)) ) {

		m_pEditor->Undo(pCmd);

		return;
		}
	}

// Message Map

AfxMessageMap(CControlProjectView, CUIItemMultiWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_LOADTOOL)

	AfxMessageEnd(CControlProjectView)
	};

// Message Handlers

void CControlProjectView::OnPostCreate(void)
{
	CUIItemMultiWnd::OnPostCreate();

	FindEditor();
	}

void CControlProjectView::OnLoadTool(UINT uCode, CMenu &Menu)
{
	}

// Implementation

BOOL CControlProjectView::FindEditor(void)
{
	CWnd *pWnd   = m_Views[0];

	CWnd *pChild = pWnd->GetWindowPtr(GW_CHILD);

	while( pChild->IsWindow() ) {

		if( pChild->IsKindOf(AfxRuntimeClass(CProgramExecutionCtrlWnd)) ) {

			m_pEditor = (CProgramExecutionCtrlWnd *) pChild;

			return TRUE;
			}

		pChild = pChild->GetWindowPtr(GW_HWNDNEXT);
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Control Project Item View -- Move Command
//

// Runtime Class

AfxImplementRuntimeClass(CControlProjectView::CCmdMove, CCmd);

// Constructor

CControlProjectView::CCmdMove::CCmdMove(CString Item, DWORD dwProgram, CMoveList Move, CMoveList Undo)
{
/*	PCTXT pName[] = {
		
		L"",
		L"Up",
		L"Down",
		L"Begin",
		L"End",
		L"SFC",
		L"Before",
		L"After",

		};

	m_Menu	   = CPrintf(L"Move Program %s", pName[dwMove]);

*/	m_Menu     = CString(IDS_MOVE_PROGRAM);

	m_Item	   = Item;

	m_dwProg   = dwProgram;

	m_Move     = Move;

	m_Undo     = Undo;
	}

// End of File
