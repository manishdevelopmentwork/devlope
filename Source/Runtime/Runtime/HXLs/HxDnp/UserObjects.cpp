#include "Intern.hpp"

#include "UserObjects.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// DNP User Objects
//

// Constructor

CUserObjects::CUserObjects(void)
{
	AfxGetObject("dev.tz", 0, ITimeZone, m_pZone);

	for( UINT u = 0; u < elements(m_pObjects); u++ ) {

		m_pObjects[u] = NULL;

		m_pPending[u] = FALSE;

		m_pDoPoll[u] = FALSE;

		m_bMode[u] = 0;
	}

	m_bPoll  = 0;

	m_IIbits = 0;

	m_bUnsolicitMsgMask = 0;
}

// Destructor

CUserObjects::~CUserObjects(void)
{
	for( UINT u = 0; u < elements(m_pObjects); u++ ) {

		if( m_pObjects[u] ) {

			delete m_pObjects[u];
		}
	}
}

// Data Access

CUserTable * CUserObjects::Find(BYTE bObject)
{
	if( !m_pObjects[bObject] ) {

		m_pObjects[bObject] = new CUserTable(bObject, m_bMode[bObject], m_pZone);
	}

	return m_pObjects[bObject];
}

CUserData * CUserObjects::Find(BYTE bObject, UINT uIndex)
{
	if( m_pObjects[bObject] ) {

		return m_pObjects[bObject]->Find(uIndex);
	}

	return NULL;
}

DWORD CUserObjects::GetIIbits(void)
{
	return m_IIbits;
}

void CUserObjects::SetIIbit(BYTE bBit, BOOL fState)
{
	m_IIbits &= ~(1 << bBit);

	m_IIbits |= fState;
}

void CUserObjects::SetUnsolicitMsgMask(BYTE bMask)
{
	m_bUnsolicitMsgMask = bMask;
}

BYTE CUserObjects::GetUnsolicitMsgMask(void)
{
	return m_bUnsolicitMsgMask;
}

WORD CUserObjects::GetFeedback(void)
{
	return m_Feedback;
}

void CUserObjects::SetFeedback(WORD wFeedback)
{
	m_Feedback = wFeedback;
}

void CUserObjects::SetMode(BYTE bOject, BYTE bMode)
{
	m_bMode[bOject] = bMode;
}

BYTE CUserObjects::GetMode(BYTE bObject)
{
	return m_bMode[bObject];
}

// End of File
