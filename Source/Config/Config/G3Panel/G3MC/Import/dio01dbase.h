
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - Digital Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CDIO14DBASE_H

#define	INCLUDE_CDIO14DBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Debug
//

//#define	DEBUG_ASCII
//#define	DEBUG_BINARY

#ifdef DEBUG_ASCII
#define DEBUG_INST
#endif

#ifdef DEBUG_BINARY
#define DEBUG_INST
#endif

//////////////////////////////////////////////////////////////////////////
//
// Model Numbers
//

#define	DIO14	6

//////////////////////////////////////////////////////////////////////////
//
// General defines
//

#define	ACTIVE_LO	0
#define	ACTIVE_HI	1

#define	YES		0
#define	NO		1

#define	RUN		0
#define	STOP		1

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigDIO
{
	BYTE	InputMode1;
	BYTE	InputMode2;
	BYTE	InputMode3;
	BYTE	InputMode4;
	BYTE	InputMode5;
	BYTE	InputMode6;
	BYTE	InputMode7;
	BYTE	InputMode8;

	WORD	Dummy;

	BYTE	GUID[16];
	BYTE	Valid;

	} CONFIGDIO;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusDIO
{
	BYTE	Running;

	} STATUSDIO;
	
//////////////////////////////////////////////////////////////////////////
//
// IO Data and Control
//

typedef struct tagInOutDIO
{
	BOOL	RelayOutput1;
	BOOL	RelayOutput2;
	BOOL	RelayOutput3;
	BOOL	RelayOutput4;
	BOOL	RelayOutput5;
	BOOL	RelayOutput6;

	BOOL	UserInput1;
	BOOL	UserInput2;
	BOOL	UserInput3;
	BOOL	UserInput4;
	BOOL	UserInput5;
	BOOL	UserInput6;
	BOOL	UserInput7;
	BOOL	UserInput8;

	WORD	Count[8];
	WORD	TmpCount[8];
	} INOUTDIO;


//////////////////////////////////////////////////////////////////////////
//
// Logic configuration
//

#define MBOK		MessageBeep(MB_OK);
#define MBBAD		MessageBeep(MB_ICONEXCLAMATION);

#define PS_INIT		1

#define INPUT_GREEN	0x0000FF00
#define INPUT_RED	0x00000090

#define DDRAW		1

#define SPLACE		1
#define SPREMOVE	2
#define SMOVE		3
#define SXOR		4
#define SERASE		5

#define DERASE		6
#define DSELECT1	7
#define DSELECT2	8
#define DNORM		9
#define DXOR		10


#define DWIRE		11
#define DSYMBOL		12


#define WM_Z		1
#define WM_U		2
#define WM_PLACE	3

#define UEXT		10

#define GRID_SIZE	5

#define LOGIC_WIN_TOP	  70
#define LOGIC_WIN_RIGHT	  560
#define	LOGIC_WIN_BOTTOM  445

//Buttons
#define B_UNDO		0
#define B_REDO		1
#define B_REFRESH	2
#define NO_BUTTON	99

#define IB_PEND		1
#define IB_ON		2
#define IB_OFF		3
#define IB_START	4
#define IB_STOP		5
#define IB_DRAWN	6

#define NUM_PROPS	5
#define NUM_PINS	20
#define NUM_SYMS	81
#define NUM_WIRES	100
#define NUM_EQUATIONS	200
#define NUM_EQ_ELEMENTS	80
#define NUM_BUTTONS	3
#define NUM_UNDO_LEVELS	11

//Edit modes

#define PLACE		1
#define CONNECT		2
#define DRAG_SEGMENT	3
#define DRAG_SYM	4

#define IWIRE		1
#define ISEG		2
#define ISYMBOL		3
#define IBUTTON		4

#define	END_PROTO	NUM_SYMS-1
#define	JCT_PROTO	NUM_SYMS-2
#define	AND_PROTO	NUM_SYMS-3
#define	OR_PROTO	NUM_SYMS-4
#define	XOR_PROTO	NUM_SYMS-5
#define	NOT_PROTO	NUM_SYMS-6
#define	TIMERUP_PROTO	NUM_SYMS-7
#define	TIMERDN_PROTO	NUM_SYMS-8
#define	COUNTUP_PROTO	NUM_SYMS-9
#define	COUNTDN_PROTO	NUM_SYMS-10
#define LATCH_PROTO	NUM_SYMS-11
#define	INCOIL_PROTO	NUM_SYMS-12
#define	OUTCOIL_PROTO	NUM_SYMS-13
#define	START_PROTO	NUM_SYMS-14

//User symbols exist in this space...

#define	START_USER_SYM	15

#define	INP7		14
#define	INP6		13
#define	INP5		12
#define	INP4		11
#define	INP3		10
#define	INP2		9
#define	INP1		8
#define	INP0		7

#define	OUT5		6
#define	OUT4		5
#define	OUT3		4
#define	OUT2		3
#define	OUT1		2
#define	OUT0		1

//General I/O and special symbols

#define JCT	1
#define MINP    2
#define MOUT	3

#define SYM_START  9

//Gates
#define GATE_START 10
#define AND	   11
#define OR	   12
#define XOR	   13
#define NOT	   14
#define GATE_STOP  15

//Timers
#define TIMER_START 20
#define TIMERUP	    21
#define TIMERDN	    22
#define TIMER_STOP  23

//Counters
#define COUNTER_START 30
#define COUNTUP	      31
#define COUNTDN       32
#define COUNTER_STOP  33

//Ladder Logic
#define LADDER_START 40
#define INP_NO	     41
#define INP_NC	     42
#define OUTP	     43
#define LADDER_STOP  44

#define IOCOIL_START	50
#define INC		51
#define OUTC		52
#define IOCOIL_STOP	53

#define LAT		60

#define SYM_STOP	61

#endif

// End of File