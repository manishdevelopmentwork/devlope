
#include "intern.hpp"

#include "orientm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Oriental Motor Driver
//

// Instantiator

INSTANTIATE(COrientalMotorDriver);

// Constructor

COrientalMotorDriver::COrientalMotorDriver(void)
{
	m_Ident		= DRIVER_ID;
	
	m_uWErrCt	= 0;

	m_bFwdRev	= 0;

	m_bDNum		= 0;

	m_uNoFrameSent	= 0;
	}

// Destructor

COrientalMotorDriver::~COrientalMotorDriver(void)
{
	}

// Configuration

void MCALL COrientalMotorDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL COrientalMotorDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL COrientalMotorDriver::Open(void)
{
	m_t4000 = ToTicks(4000);
	m_t1000 = m_t4000 >> 2;
	}

// Device

CCODE MCALL COrientalMotorDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop		= GetByte(pData);
			m_pCtx->m_bActiveDrop	= m_pCtx->m_bDrop;
			m_pCtx->m_bPingCount	= 0;
			m_pCtx->m_dDropInterval	= m_t1000;
			m_pCtx->m_dDropStart	= GetTickCount();
			m_pCtx->m_uActiveTO	= 10;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL COrientalMotorDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL COrientalMotorDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING ");

	CCODE cc = DoNoOp();

	if( cc & CCODE_ERROR ) {

		if( m_pCtx->m_bActiveDrop != m_pCtx->m_bDrop ) {

			if( (++m_pCtx->m_bPingCount) > m_pCtx->m_uActiveTO ) {

				m_pCtx->m_bActiveDrop = m_pCtx->m_bDrop;

				m_pCtx->m_bPingCount = 0;
				}
			}
		}

	else m_pCtx->m_bPingCount = 0;

	return cc;
	}

CCODE MCALL COrientalMotorDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\nREAD %x %x", Addr.a.m_Table, Addr.a.m_Offset);

	SetSelection(Addr);

	if( NoReadTransmit(pData) ) return 1;

	return SendRead(Addr, pData);
	}

CCODE MCALL COrientalMotorDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\nWRITE %x %x %x\r\n", Addr.a.m_Table, Addr.a.m_Offset, *pData);

	SetSelection(Addr);

	if( NoWriteTransmit(*pData) ) return 1;

	return SendWrite(Addr, pData);
	}

// User Function
UINT MCALL COrientalMotorDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	return 1;
	}

// Frame Building

void COrientalMotorDriver::BuildFrame(BYTE bAction, UINT uOffset, DWORD dData)
{
	BYTE bHeader = HMASTER;

	if( GBLADDR(m_pCtx->m_bActiveDrop) ) {

		Sleep(5);		// 5 ms wait required for simultaneous send
		bHeader |= HSIMUL;
		bHeader &= ~HADDR;
		}

	else bHeader |= m_pCtx->m_bActiveDrop;

	m_uPtr = 0;

	AddByte(bHeader);

	AddByte(bAction | m_bFwdRev);

	AddByte(SelectMode(uOffset, dData));

	AddByte(SelectCommand());

	PutNum(dData);
	}

BYTE COrientalMotorDriver::SelectMode(UINT uOffset, DWORD dData)
{
	if( IsGenu() )		return m_fGenNoPar ? 0 : m_bGenPar; 

	if( m_fTable )		return LOBYTE(uOffset);

	if( IsSelDataNumber() )	return LOBYTE(dData);

	if( IsReqDataNumber() )	return 0;

	return m_bDNum;
	}

BYTE COrientalMotorDriver::SelectCommand(void)
{
	return IsDirect() ? 0 : IsGenu() ? m_bGenOpc : m_bOp;
	}

BOOL COrientalMotorDriver::IsDirect(void)
{
	return m_bOp == DOPC;
	}

BOOL COrientalMotorDriver::IsSelDataNumber(void)
{
	return m_bOp == C00D;
	}

BOOL COrientalMotorDriver::IsReqDataNumber(void)
{
	return m_bOp == C36D;
	}

BOOL COrientalMotorDriver::IsGenu(void)
{
	return m_bOp == GENU;
	}

BOOL COrientalMotorDriver::IsUserAddr()
{
	return m_bOp == UADD;
	}

BOOL COrientalMotorDriver::HasZeroData(void)
{
	return IsDirect() || IsSelDataNumber() || (IsGenu() && m_fGenNoPar);
	}

void COrientalMotorDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void COrientalMotorDriver::EndFrame(void)
{
	BYTE bCRC = 0;

	for( UINT i = 0; i < 8; i++ ) bCRC ^= m_bTx[i];

	m_bTx[FCRC] = bCRC;
	}
		
// Transport Layer

BOOL COrientalMotorDriver::Transact(void)
{
	EndFrame();

	memset(m_bRx, 0, PBSZ);

	Put();

	return GetReply();
	}

BOOL COrientalMotorDriver::CheckReply(BOOL fIsWrite)
{
	BYTE b = 0;

	for( UINT i = FHEAD; i <= FCRC; i++ ) b ^= m_bRx[i];

	if( b ) return FALSE;

	b = m_bRx[FHEAD];

	if( b & HMASTER ) return FALSE; // not slave response

	if( (b & HADDR) != m_pCtx->m_bActiveDrop ) {

		return m_pCtx->m_bActiveDrop <= HADDR; // not this address, not global
		}

	UINT uEnd = fIsWrite ? FDATAHH : FCMD; 

	for( i = FACTION; i <= uEnd; i++ ) {

		if( m_bTx[i] != m_bRx[i] ) return FALSE;
		}

	return TRUE;
	}

// Read Handlers

CCODE COrientalMotorDriver::SendRead(AREF Addr, PDWORD pData)
{
//**/	AfxTrace0("-- Send Read ");

	BYTE bAction = AEREAD + m_bFwdRev;

	BuildFrame(AEREAD, Addr.a.m_Offset, 0L);

	if( Transact() && CheckReply(FALSE) ) {

		if( IsReqDataNumber() ) {

			m_bDNum = m_bRx[FDATALL];
			}

		DWORD x = *PU4(&m_bRx[FDATALL]);

		*pData  = IntelToHost(x);

		return 1;
		}

//**/	AfxTrace0("\r\nFAIL\r\n");

	return CCODE_ERROR;
	}

CCODE COrientalMotorDriver::DoNoOp(void)
{
	CAddress Addr;

	Addr.a.m_Table  = addrNamed;
	Addr.a.m_Offset = C00D;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;

	DWORD Data[1];

	SetSelection(Addr);

	return SendRead(Addr, Data);
	}

// Write Handlers

CCODE COrientalMotorDriver::SendWrite(AREF Addr, PDWORD pData)
{
//**/	AfxTrace0("-- Send Write ");

	DWORD dData  = *pData;

	BYTE bAction = 0;

	if( IsDirect() ) {

		bAction  |= LOBYTE(dData) & DIRMASK;

		dData     = 0L;
		}

	BuildFrame(bAction, Addr.a.m_Offset, dData);

	if( Transact() && CheckReply(TRUE) ) {

		if( IsSelDataNumber() ) m_bDNum = LOBYTE(dData);

		m_uWErrCt = 0;

//**/		AfxTrace0("\r\n\n");

		return 1;
		}

	if( IsDirect() ) m_bFwdRev = 0;

	if( m_uWErrCt++ > 2 ) {

		m_uWErrCt = 0;

//**/		AfxTrace0("\r\n\n");

		return 1;
		}

//**/	AfxTrace0("\r\nFAIL\r\n");

	return CCODE_ERROR;
	}

// Helpers

void COrientalMotorDriver::SetSelection(AREF Addr)
{
	UINT t   = Addr.a.m_Table;

	m_fTable = t >= FIRSTPARAM && t <= LASTTABLE;

	m_bOp    = GetOMOpcode(Addr);
	}

BYTE COrientalMotorDriver::GetOMOpcode(AREF Addr)
{
	switch( Addr.a.m_Table ) {

		case DRCT:	return DOPC;
		case C0FS:	return 0x0F;
		case C10P:	return 0x10;
		case C11O:	return 0x11;
		case C3DC:	return 0x3D;
		case C3EA:	return 0x3E;
		case C3FW:	return 0x3F;
		case C82A:	return 0x82;
		case C83D:	return 0x83;
		case C88O:	return 0x88;
		case CA0P:	return 0xA0;
		case CUAD:	return UADD;
		case CGEN:	return SetGeneric(Addr);
		}

	return Addr.a.m_Offset; // addrNamed
	}

BYTE COrientalMotorDriver::SetGeneric(AREF Addr)
{
	UINT uOffset = Addr.a.m_Offset;

	m_fGenNoPar  = Addr.a.m_Extra & 2;

	m_bGenPar    = HIBYTE(uOffset);

	m_bGenOpc    = LOBYTE(uOffset);

	return GENU;
	}

BOOL COrientalMotorDriver::NoReadTransmit(PDWORD pData)
{
	BOOL fReturn = FALSE;

	if( IsDirect() ) {

		*pData = DWORD(m_bFwdRev);

		fReturn = TRUE;
		}

	if( IsSelDataNumber() ) {

		*pData = DWORD(m_bDNum);

		fReturn = TRUE;
		}

	if( IsWriteCommand() ) {

		*pData = 0;

		fReturn = TRUE;
		}

	if( IsUserAddr() ) { // check if timeout on user address change

		CContext * p;
		BYTE bA;
		BYTE bD;

		p  = m_pCtx;

		bA = p->m_bActiveDrop;
		bD = p->m_bDrop;

		if( bA == bD ) {

			*pData = DWORD(bD);

			fReturn = TRUE;
			}

		BOOL  fIntvl;
		BYTE  bDrop;
		DWORD dTick;

		fIntvl = p->m_dDropInterval == m_t4000;

		bDrop  = fIntvl ? bA : bD; // assume no change yet

		dTick  = GetTickCount();

		if( dTick > p->m_dDropStart + p->m_dDropInterval ) {

			p->m_dDropStart     = dTick;

			p->m_dDropInterval  = fIntvl ? m_t1000 : m_t4000;

			bDrop = fIntvl ? bD : bA;
			}

		*pData = DWORD(bDrop);

		fReturn = TRUE;
		}

	if( fReturn ) {

		if( ++m_uNoFrameSent > 10 ) { // Keep Alive

			DoNoOp();
			}

		else Sleep(20);

		return TRUE;
		}

	return FALSE;
	}

BOOL COrientalMotorDriver::IsWriteCommand(void)
{
	if( m_bOp >= C20C && m_bOp <= C27C ) return TRUE;

	return m_bOp >= C48B && m_bOp <= C4AB;
	}

BOOL COrientalMotorDriver::NoWriteTransmit(DWORD dData)
{
	switch( m_bOp ) {

		case C00D:
			if( dData > 0x3F ) {

				Sleep(20);
				return TRUE;
				}

			return FALSE;

		case GENU:
			if( dData > 0xFF ) {

				Sleep(20);
				return TRUE;
				}

			return FALSE;
		}

	if( IsDirect() ) {

		BYTE b = LOBYTE(dData);

		m_bFwdRev = b & FWDREV;

		return FALSE;
		}

	if( IsReadOnly() ) {

		Sleep(20);

		return TRUE;
		}

	if( IsUserAddr() ) {

		BYTE b = LOBYTE(dData);

		m_pCtx->m_bActiveDrop = b & HADDR;

		m_pCtx->m_uActiveTO   = 10 + ((b >> 5) * 60);

		return TRUE;
		}

	return FALSE;
	}

BOOL COrientalMotorDriver::IsReadOnly(void)
{
	if( m_bOp >= 0x36 && m_bOp <= 0x3F ) return TRUE;

	switch( m_bOp ) {

		case C8BC:
		case CA1C:
		case CA2E:
			return TRUE;
		}

	return FALSE;
	}

void COrientalMotorDriver::PutNum(DWORD dData)
{
	if( HasZeroData() ) dData = 0L;

	AddByte(LOBYTE(LOWORD(dData)));
	AddByte(HIBYTE(LOWORD(dData)));
	AddByte(LOBYTE(HIWORD(dData)));
	AddByte(HIBYTE(HIWORD(dData)));
	}

// Port Access
void COrientalMotorDriver::Put(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < PBSZ; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_uNoFrameSent = 0;

	m_pData->Write( m_bTx, PBSZ, FOREVER );
	}

BOOL COrientalMotorDriver::GetReply(void)
{
	UINT uTimeout = 1000;
	UINT uPtr     = 0;
	UINT uData;

	SetTimer(uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (uData = Get(uTimeout)) < NOTHING ) {

			BYTE bData    = LOBYTE(uData);

			m_bRx[uPtr++] = bData;

//**/			AfxTrace1("<%2.2x>", bData);

			if( uPtr >= PBSZ ) return TRUE;
			}
		}

	return FALSE;
	}

UINT COrientalMotorDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// End of File
