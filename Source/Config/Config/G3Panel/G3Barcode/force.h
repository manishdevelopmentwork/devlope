
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Barcode Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_FORCE_H

#define	INCLUDE_FORCE_H

//////////////////////////////////////////////////////////////////////////
//
// Project Specific Warning Control
//

#pragma warning(disable: 6001)
#pragma warning(disable: 6011)
#pragma warning(disable: 6054)

// End of File

#endif
