
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsPortCatLink_HPP

#define INCLUDE_CommsPortCatLink_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsPort.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CatLink Comms Port
//

class DLLNOT CCommsPortCatLink : public CCommsPort
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCommsPortCatLink(void);

		// Overridable
		BOOL IsRemotable(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
