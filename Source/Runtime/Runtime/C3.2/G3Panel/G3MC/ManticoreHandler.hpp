
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ManticoreHandler_HPP

#define	INCLUDE_ManticoreHandler_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RackInterface.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Manticore Handler
//

class CManticoreHandler : public IRackHandler, public IPortHandler
{
public:
	// Constructor
	CManticoreHandler(void);

	// Destructor
	~CManticoreHandler(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IRackHandler
	UINT METHOD Transact(BYTE bDrop, PBYTE pData, UINT uCount);
	UINT METHOD Transact(BYTE bDrop, UINT  Msg);
	BOOL METHOD GetReply(BYTE bDrop, PBYTE pData);
	BOOL METHOD Recycle(BYTE bDrop);

	// IPortHandler
	void METHOD Bind(IPortObject *pPort);
	void METHOD OnRxData(BYTE bData);
	void METHOD OnRxDone(void);
	BOOL METHOD OnTxData(BYTE &bData);
	void METHOD OnTxDone(void);
	void METHOD OnOpen(CSerialConfig const &Config);
	void METHOD OnClose(void);
	void METHOD OnTimer(void);

protected:
	// Constants
	enum
	{
		constHeadSize	= 6,
		constDataSize	= 255,
		constBuffSize	= constHeadSize + constDataSize,
	};

	// Data Members
	ULONG         m_uRefs;
	IPortObject * m_pPort;
	IEvent      * m_pDone;
	BOOL	      m_fBusy;
	UINT	      m_uRxMsg;
	UINT	      m_uTxMsg;
	BYTE	      m_bDrop;
	BYTE          m_bSeq;
	BYTE	      m_bTxBuff[constBuffSize];
	BYTE	      m_bRxBuff[constBuffSize];
	UINT	      m_uRxPtr;
	UINT	      m_uTxPtr;
	UINT	      m_uTxReq;
	       
	// Implementation
	void Exchange(void);
	void Exchange(UINT uCount, UINT uTime);
	void Done(UINT uMsg);
	WORD CalcSum(PCBYTE pData, UINT uCount);
	BOOL TestSum(PCBYTE pData, UINT uCount);
	UINT AddSum(UINT uCount);
	void StartRx(void);
	void StartTx(UINT uCount);
};

// End of File

#endif
