@echo off

rem %1 = build path

mkdir "%~1include\runtime\c3.2" 2>nul

copy g3comms.hpp  "%~1include\runtime\c3.2" >nul

copy usbhostp.hpp "%~1include\runtime\c3.2" >nul

attrib -r "%~1include\runtime\c3.2"\*.*
