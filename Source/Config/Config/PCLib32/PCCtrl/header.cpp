
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Header Control
//

// Dynamic Class

AfxImplementDynamicClass(CHeaderCtrl, CCtrlWnd);

// Constructor

CHeaderCtrl::CHeaderCtrl(void)
{
	LoadControlClass(ICC_LISTVIEW_CLASSES);
	}

// Attributes

UINT CHeaderCtrl::GetBitmapMargin(void) const
{
	return UINT(SendMessageConst(HDM_GETBITMAPMARGIN));
	}

CImageList & CHeaderCtrl::GetImageList(void) const
{
	return CImageList::FromHandle(HIMAGELIST(SendMessageConst(HDM_GETIMAGELIST)));
	}

BOOL CHeaderCtrl::GetItem(UINT uIndex, HDITEM &Item) const
{
	return BOOL(SendMessageConst(HDM_GETITEM, WPARAM(uIndex), LPARAM(&Item)));
	}

CHeaderItem CHeaderCtrl::GetItem(UINT uIndex) const
{
	CHeaderItem Item;

	Item.SetMask( HDI_BITMAP
		    | HDI_FORMAT
		    | HDI_FILTER
		    | HDI_HEIGHT
		    | HDI_IMAGE
		    | HDI_LPARAM
		    | HDI_ORDER
		    | HDI_TEXT
		    | HDI_WIDTH
		    );
	
	Item.SetTextBuffer(256);

	GetItem(uIndex, Item);

	return Item;
	}

CString CHeaderCtrl::GetItemText(UINT uIndex) const
{
	CHeaderItem Item;
	
	Item.SetTextBuffer(256);

	GetItem(uIndex, Item);

	return Item.GetText();
	}

HBITMAP CHeaderCtrl::GetItemBitmap(UINT uIndex) const
{
	CHeaderItem Item;
	
	Item.SetMask(HDI_BITMAP);

	GetItem(uIndex, Item);

	return Item.GetBitmap();
	}

UINT CHeaderCtrl::GetItemImage(UINT uIndex) const
{
	CHeaderItem Item;
	
	Item.SetMask(HDI_IMAGE);

	GetItem(uIndex, Item);

	return Item.GetImage();
	}

LPARAM CHeaderCtrl::GetItemParam(UINT uIndex) const
{
	CHeaderItem Item;
	
	Item.SetMask(HDI_LPARAM);

	GetItem(uIndex, Item);

	return Item.GetParam();
	}

UINT CHeaderCtrl::GetItemCount(void) const
{
	return UINT(SendMessageConst(HDM_GETITEMCOUNT));
	}

void CHeaderCtrl::GetItemRect(UINT uIndex, RECT &Rect) const
{
	SendMessageConst(HDM_GETITEMRECT, WPARAM(uIndex), LPARAM(&Rect));
	}

CRect CHeaderCtrl::GetItemRect(UINT uIndex) const
{
	CRect Rect;

	GetItemRect(uIndex, Rect);

	return Rect;
	}

BOOL CHeaderCtrl::GetOrderArray(UINT uCount, UINT *pArray) const
{
	return BOOL(SendMessageConst(HDM_GETORDERARRAY, WPARAM(uCount), LPARAM(pArray)));
	}

BOOL CHeaderCtrl::GetOrderArray(CArray <UINT> &Array) const
{
	Array.Empty();

	UINT uCount = GetItemCount();

	Array.Expand(uCount);

	return GetOrderArray(uCount, (UINT *) Array.GetPointer());
	}

UINT CHeaderCtrl::HitTest(HDHITTESTINFO &Info) const
{
	return UINT(SendMessageConst(HDM_HITTEST, 0, LPARAM(&Info)));
	}

UINT CHeaderCtrl::HitTestItem(CPoint const &Pos) const
{
	HDHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(Info);

	return Info.iItem;
	}

UINT CHeaderCtrl::HitTestFlags(CPoint const &Pos) const
{
	HDHITTESTINFO Info;

	Info.pt = Pos;

	HitTest(Info);

	return Info.flags;
	}

UINT CHeaderCtrl::HitTestItem(int xPos, int yPos) const
{
	return HitTestItem(CPoint(xPos, yPos));
	}

UINT CHeaderCtrl::HitTestFlags(int xPos, int yPos) const
{
	return HitTestFlags(CPoint(xPos, yPos));
	}

BOOL CHeaderCtrl::Layout(HDLAYOUT &Layout) const
{
	return BOOL(SendMessageConst(HDM_LAYOUT, 0, LPARAM(&Layout)));
	}

UINT CHeaderCtrl::OrderToIndex(UINT uOrder) const
{
	return BOOL(SendMessageConst(HDM_ORDERTOINDEX, WPARAM(uOrder)));
	}

// Operations

BOOL CHeaderCtrl::AddItem(HDITEM const &Item)
{
	return InsertItem(GetItemCount(), Item);
	}

BOOL CHeaderCtrl::ClearFilter(UINT uIndex)
{
	return BOOL(SendMessage(HDM_CLEARFILTER, WPARAM(uIndex)));
	}

HIMAGELIST CHeaderCtrl::CreateDragImage(UINT uIndex)
{
	return HIMAGELIST(SendMessage(HDM_CREATEDRAGIMAGE, WPARAM(uIndex)));
	}

BOOL CHeaderCtrl::DeleteItem(UINT uIndex)
{
	return BOOL(SendMessage(HDM_DELETEITEM, WPARAM(uIndex)));
	}

BOOL CHeaderCtrl::EditFilter(UINT uIndex, BOOL fDiscard)
{
	return BOOL(SendMessage(HDM_EDITFILTER, WPARAM(uIndex), LPARAM(fDiscard)));
	}

UINT CHeaderCtrl::InsertItem(UINT uIndex, HDITEM const &Item)
{
	return BOOL(SendMessage(HDM_INSERTITEM, WPARAM(uIndex), LPARAM(&Item)));
	}

BOOL CHeaderCtrl::SetBitmapMargin(UINT uMargin)
{
	return BOOL(SendMessage(HDM_SETBITMAPMARGIN, WPARAM(uMargin)));
	}

BOOL CHeaderCtrl::SetFilterChangeTimeout(UINT uTimeout)
{
	return BOOL(SendMessage(HDM_SETBITMAPMARGIN, 0, LPARAM(uTimeout)));
	}

BOOL CHeaderCtrl::SetHotDivider(UINT uIndex)
{
	return BOOL(SendMessage(HDM_SETHOTDIVIDER, WPARAM(FALSE), LPARAM(uIndex)));
	}

BOOL CHeaderCtrl::SetHotDivider(CPoint const &Pos)
{
	return BOOL(SendMessage(HDM_SETHOTDIVIDER, WPARAM(TRUE), LPARAM(Pos)));
	}

BOOL CHeaderCtrl::SetHotDivider(int xPos, int yPos)
{
	return SetHotDivider(CPoint(xPos, yPos));
	}

BOOL CHeaderCtrl::SetImageList(HIMAGELIST hList)
{
	return BOOL(SendMessage(HDM_SETIMAGELIST, 0, LPARAM(hList)));
	}

BOOL CHeaderCtrl::SetItem(UINT uIndex, HDITEM const &Item)
{
	return BOOL(SendMessage(HDM_SETITEM, WPARAM(uIndex), LPARAM(&Item)));
	}

BOOL CHeaderCtrl::SetOrderArray(UINT uCount, UINT const *pArray)
{
	return BOOL(SendMessage(HDM_SETORDERARRAY, WPARAM(uCount), LPARAM(pArray)));
	}

BOOL CHeaderCtrl::SetOrderArray(CArray <UINT> const &Array)
{
	return SetOrderArray(Array.GetCount(), Array.GetPointer());
	}

// Handle Lookup

CHeaderCtrl & CHeaderCtrl::FromHandle(HWND hWnd)
{
	if( hWnd == NULL ) {

		static CHeaderCtrl NullObject;

		return NullObject;
		}

	return (CHeaderCtrl &) CWnd::FromHandle(hWnd, AfxStaticClassInfo());
	}

// Default Class Name

PCTXT CHeaderCtrl::GetDefaultClassName(void) const
{
	return WC_HEADER;
	}

// End of File
