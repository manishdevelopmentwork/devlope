
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_IRtc_HPP

#define INCLUDE_IRtc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 16 -- Real Time Clock Devices 
//

interface IRtc;

interface IBatteryMonitor;

//////////////////////////////////////////////////////////////////////////
//
// Calibration Status
//

enum 
{
	rtcCalibNone,
	rtcCalibGood,
	rtcCalibBad,
	};

//////////////////////////////////////////////////////////////////////////
//
// Real Time Clock
//

interface IRtc : public IDevice
{
	// https://

	AfxDeclareIID(16, 1);
	
	virtual void METHOD GetTime(struct timeval *tv)       = 0;
	virtual bool METHOD SetTime(struct timeval const *tv) = 0;
	virtual UINT METHOD GetCalibStatus(void)	      = 0;
	virtual INT  METHOD GetCalib(void)	              = 0;
	virtual bool METHOD PutCalib(INT Calib)               = 0;
	virtual UINT METHOD GetMonotonic(void)		      = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Battery Monitor
//

interface IBatteryMonitor : public IDevice
{
	// https://

	AfxDeclareIID(16, 2);
	
	virtual bool METHOD IsBatteryGood(void)	= 0;
	virtual bool METHOD IsBatteryBad(void)	= 0;
	};

// End of File

#endif
