
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbInterfaceDesc_HPP

#define	INCLUDE_UsbInterfaceDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Interface Descriptor
//

class CUsbInterfaceDesc : public UsbInterfaceDesc
{
	public:
		// Constructor
		CUsbInterfaceDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Init
		void Init(void);

		// Debug
		void Debug(void);
	};

// End of File

#endif
