
#include "intern.hpp"

#include "rpc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Remoting Frame
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//


// Constructor

CRemotingFrame::CRemotingFrame(void)
{
	m_wCount = 0;
	
	m_wTrans = 0;
	}

// Attributes

bool CRemotingFrame::IsValid(void)
{
	return GetMagic() == Magic;
	}

bool CRemotingFrame::IsRaw(void)
{
	return GetMagic() != Magic;
	}

// Frame Creattion

void CRemotingFrame::Start(WORD wService, WORD wOpcode)
{
	Start(wService, wOpcode, ++m_wTrans);
	}

void CRemotingFrame::Start(WORD wService, WORD wOpcode, WORD wTrans)
{
	m_wTrans    = wTrans;

	m_bData[ 0] = HIBYTE(HIWORD(Magic));
	
	m_bData[ 1] = LOBYTE(HIWORD(Magic));

	m_bData[ 2] = HIBYTE(LOWORD(Magic));
	
	m_bData[ 3] = LOBYTE(LOWORD(Magic));

	m_bData[ 4] = HIBYTE(wService);
	
	m_bData[ 5] = LOBYTE(wService);

	m_bData[ 6] = HIBYTE(wOpcode);
	
	m_bData[ 7] = LOBYTE(wOpcode);

	m_bData[ 8] = HIBYTE(m_wTrans);
	
	m_bData[ 9] = LOBYTE(m_wTrans);

	m_bData[10] = 0;

	m_bData[11] = 0;

	m_wCount    = 12;
	}

void CRemotingFrame::End(void)
{
	m_bData[10] = HIBYTE(m_wCount);

	m_bData[11] = LOBYTE(m_wCount);
	}

void CRemotingFrame::Clear(void)
{
	m_wCount = 0;
	}

void CRemotingFrame::SetService(WORD wService)
{
	m_bData[4] = HIBYTE(wService);
	
	m_bData[5] = LOBYTE(wService);
	}

void CRemotingFrame::SetOpcode(WORD wOpcode)
{
	m_bData[6] = HIBYTE(wOpcode);
	
	m_bData[7] = LOBYTE(wOpcode);
	}

void CRemotingFrame::SetTransact(WORD wTransact)
{
	m_bData[8] = HIBYTE(m_wTrans);
	
	m_bData[9] = LOBYTE(m_wTrans);
	}

void CRemotingFrame::SetCount(WORD wCount)
{
	m_wCount = wCount;
	}

// Simple Data

void CRemotingFrame::AddByte(BYTE bData)
{
	if( m_wCount <= (sizeof(m_bData) - sizeof(bData)) ) {

		m_bData[m_wCount++] = bData;
		}
	}

void CRemotingFrame::AddWord(WORD wData)
{
	if( m_wCount <= (sizeof(m_bData) - sizeof(wData)) ) {

		m_bData[m_wCount++] = HIBYTE(wData);
		
		m_bData[m_wCount++] = LOBYTE(wData);
		}
	}

void CRemotingFrame::AddLong(LONG lData)
{
	if( m_wCount <= (sizeof(m_bData) - sizeof(lData)) ) {

		m_bData[m_wCount++] = HIBYTE(HIWORD(lData));
		
		m_bData[m_wCount++] = LOBYTE(HIWORD(lData));
		
		m_bData[m_wCount++] = HIBYTE(LOWORD(lData));
		
		m_bData[m_wCount++] = LOBYTE(LOWORD(lData));
		}
	}

void CRemotingFrame::AddGuid(GUID Guid)
{
	AddData(PCBYTE(&Guid), sizeof(Guid));
	}

void CRemotingFrame::AddText(PCTXT pText)
{
	AddData(PCBYTE(pText), strlen(pText) + 1);
	}

// Larger Data

void CRemotingFrame::AddData(PCBYTE pData, UINT uCount)
{
	if( m_wCount <= (sizeof(m_bData) - uCount) ) {

		memcpy(m_bData + m_wCount, pData, uCount);

		m_wCount += uCount;
		}
	}

// Data Reads

PCBYTE CRemotingFrame::ReadData(UINT &uPtr, UINT uCount) const
{
	PCBYTE pData = m_bData + uPtr;

	uPtr += uCount;

	return pData;
	}

GUID & CRemotingFrame::ReadGuid(UINT &uPtr) const
{
	return (GUID &) (ReadData(uPtr, 16)[0]);
	}

BYTE CRemotingFrame::ReadByte(UINT &uPtr) const
{
	return m_bData[uPtr++];
	}

WORD CRemotingFrame::ReadWord(UINT &uPtr) const
{
	BYTE hi = ReadByte(uPtr);
	
	BYTE lo = ReadByte(uPtr);

	return MAKEWORD(lo, hi);
	}

DWORD CRemotingFrame::ReadLong(UINT &uPtr) const
{
	WORD hi = ReadWord(uPtr);

	WORD lo = ReadWord(uPtr);

	return MAKELONG(lo, hi);
	}

// Attributes

DWORD CRemotingFrame::GetMagic(void) const
{
	UINT uPtr = 0;

	return ReadLong(uPtr);
	}

WORD CRemotingFrame::GetService(void) const
{
	UINT uPtr = 4;	

	return ReadWord(uPtr);
	}

WORD CRemotingFrame::GetOpcode(void) const
{
	UINT uPtr = 6;	

	return ReadWord(uPtr);
	}

WORD CRemotingFrame::GetTransact(void) const
{
	UINT uPtr = 8;	

	return ReadWord(uPtr);
	}

UINT CRemotingFrame::GetCount(void) const
{
	UINT uPtr = 10;	

	return ReadWord(uPtr);
	}

UINT CRemotingFrame::GetSize(void) const
{
	return m_wCount;
	}

PBYTE CRemotingFrame::GetData(void) const
{
	return PBYTE(m_bData);
	}

UINT CRemotingFrame::GetSpace(void) const
{
	return sizeof(m_bData) - m_wCount;	
	}

UINT CRemotingFrame::GetHeadSize(void) const
{
	return 12;
	}

UINT CRemotingFrame::GetLimit(void) const
{
	return sizeof(m_bData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Remoting Link
//

// Constructor

CRemotingLink::CRemotingLink(void)
{
	m_pData  = NULL;

	m_pPort  = NULL;

	m_hPort  = NULL;

	m_pMutex = Create_Mutex();
	}

// Destructor

CRemotingLink::~CRemotingLink(void)
{
	m_pMutex->Release();
	}

// Management

void CRemotingLink::Attach(IPortObject *pPort)
{
	m_pPort = pPort;

	m_hPort = pPort->GetHandle();
	}

void CRemotingLink::Attach(IDataHandler *pData)
{
	m_pData = pData;

	m_pData->SetTxSize(m_Req.GetLimit());

	m_pData->SetRxSize(m_Rep.GetLimit());
	}

void CRemotingLink::Detach(void)
{
	m_pData = NULL;

	m_pPort = NULL;

	m_hPort = NULL;
	}

// Attributes

CRemotingFrame const & CRemotingLink::GetReq(void) const
{
	return m_Req;	
	}

CRemotingFrame const & CRemotingLink::GetRep(void) const
{
	return m_Rep;
	}

// Port

bool CRemotingLink::CheckPort(void)
{
	if( m_pPort ) {
		
		if( m_hPort != m_pPort->GetHandle() ) {

			m_hPort = m_pPort->GetHandle();

			if( m_hPort != DWORD(NULL) ) {
		
				return false;
				}
			}
		}
	
	return true;
	}

bool CRemotingLink::ClaimPort(void)
{
	GuardTask(true);

	return m_pMutex->Wait(FOREVER);
	}

void CRemotingLink::FreePort(void)
{
	m_pMutex->Free();

	GuardTask(false);
	}

// Driver 

bool CRemotingLink::DriverSelect(WORD wIdent)
{
	//AfxTrace("CRemotingLink::DriverSelect(Ident=0x%4.4X)\n", wIdent);

	ClaimPort();

	m_Req.Start(servComms, opDriverSelect);

	m_Req.AddWord(wIdent);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::DriverCheck(CSerialConfig const &Config)
{
	//AfxTrace("CRemotingLink::DriverCheck()\n");

	ClaimPort();

	m_Req.Start(servComms, opDriverCheck);

	m_Req.AddLong(Config.m_uPhysical);

	m_Req.AddLong(Config.m_uPhysMode);

	m_Req.AddLong(Config.m_uBaudRate);

	m_Req.AddLong(Config.m_uFlags);

	m_Req.AddByte(Config.m_bDrop);

	m_Req.AddByte(Config.m_uDataBits);

	m_Req.AddByte(Config.m_uStopBits);

	m_Req.AddByte(Config.m_uParity);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::DriverConfig(PCBYTE pConfig, UINT uSize)
{
	//AfxTrace("CRemotingLink::DriverConfig()\n");

	ClaimPort();
	
	m_Req.Start(servComms, opDriverConfig);

	m_Req.AddWord(WORD(uSize));

	m_Req.AddData(pConfig, uSize);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::DriverFlags(UINT uSelect, WORD &wFlags)
{
	//AfxTrace("CRemotingLink::DriverFlags()\n");

	ClaimPort();
	
	m_Req.Start(servComms, opDriverFlags);

	m_Req.AddWord(WORD(uSelect));

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opRep ) {

			UINT uPtr = m_Rep.GetHeadSize();

			wFlags = m_Rep.ReadWord(uPtr);
	
			FreePort();
			
			return true;
			}
		}
	
	wFlags = 0;

	FreePort();

	return false;
	}

bool CRemotingLink::DriverOpen(void)
{
	//AfxTrace("CRemotingLink::DriverOpen()\n");

	ClaimPort();
	
	m_Req.Start(servComms, opDriverOpen);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::DriverClose(void)
{
	//AfxTrace("CRemotingLink::DriverClose()\n");

	ClaimPort();

	m_Req.Start(servComms, opDriverClose);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::DriverService(void)
{
	ClaimPort();

	m_Req.Start(servComms, opDriverService);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

CCODE CRemotingLink::DriverCtrl(UINT uFunc, PCTXT pValue)
{
	//AfxTrace("CRemotingLink::DriverCtrl()\n");

	ClaimPort();
	
	m_Req.Start(servComms, opDriverCtrl);

	m_Req.AddLong(LONG(uFunc));

	m_Req.AddText(pValue);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();

			return CCODE_SUCCESS;
			}
		}

	FreePort();

	return CCODE_ERROR;
	}

// Device

CCODE CRemotingLink::DeviceCtrl(DWORD dwDevice, UINT uFunc, PCTXT pValue)
{
	//AfxTrace("CRemotingLink::DeviceCtrl()\n");

	ClaimPort();
	
	m_Req.Start(servComms, opDeviceCtrl);

	m_Req.AddLong(dwDevice);

	m_Req.AddLong(LONG(uFunc));

	m_Req.AddText(pValue);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();
			
			return CCODE_SUCCESS;
			}
		}

	FreePort();

	return CCODE_ERROR;
	}

CCODE CRemotingLink::DeviceOpen(DWORD dwDevice, PCBYTE pConfig, UINT uSize)
{
	//AfxTrace("CRemotingLink::DeviceOpen(Device=0x%8.8X, Config=0x%8.8x, Size=%d)\n", dwDevice, pConfig, uSize);

	ClaimPort();
	
	m_Req.Start(servComms, opDeviceOpen);

	m_Req.AddLong(LONG(dwDevice));

	m_Req.AddWord(WORD(uSize));

	m_Req.AddData(pConfig, uSize);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();
			
			return CCODE_SUCCESS;
			}
		}

	FreePort();

	return CCODE_ERROR;
	}

CCODE CRemotingLink::DeviceClose(DWORD dwDevice, bool fPersist)
{
	//AfxTrace("CRemotingLink::DeviceClose()\n");

	ClaimPort();
	
	m_Req.Start(servComms, opDeviceClose);

	m_Req.AddLong(LONG(dwDevice));

	m_Req.AddByte(BYTE(fPersist));

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opAck ) {

			FreePort();
			
			return CCODE_SUCCESS;
			}
		}

	FreePort();

	return CCODE_ERROR;
	}

// Comms

CCODE CRemotingLink::Ping(void)
{
	ClaimPort();

	m_Req.Start(servComms, opPing);

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opRep ) {

			UINT uPtr = m_Rep.GetHeadSize();

			UINT Code = m_Rep.ReadLong(uPtr);

			FreePort();

			return Code;
			}
		}

	FreePort();

	return CCODE_ERROR;
	}

CCODE CRemotingLink::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	//AfxTrace("CRemotingLink::Read(Addr=0x%8.8X, Count=%d)\n", Addr.m_Ref, uCount);

	ClaimPort();

	m_Req.Start(servComms, opRead);

	m_Req.AddLong(LONG(Addr.m_Ref));

	m_Req.AddWord(WORD(uCount));
	
	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opRep ) {
		
			UINT  uPtr = m_Rep.GetHeadSize();

			CCODE Code = m_Rep.ReadLong(uPtr);

			if( COMMS_SUCCESS(Code) ) {

				uCount = LOWORD(Code) * sizeof(DWORD);

				memcpy(pData, m_Rep.ReadData(uPtr, uCount), uCount);
				}

			FreePort();
		
			return Code;
			}
		}

	FreePort();

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CRemotingLink::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	//AfxTrace("CRemotingLink::Write()\n");

	ClaimPort();

	m_Req.Start(servComms, opWrite);

	m_Req.AddLong(LONG(Addr.m_Ref));

	m_Req.AddWord(WORD(uCount));

	m_Req.AddData(PBYTE(pData), uCount * sizeof(DWORD));
	
	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opRep ) {

			UINT  uPtr = m_Rep.GetHeadSize();

			CCODE Code = m_Rep.ReadLong(uPtr);

			FreePort();

			return Code;
			}
		}

	FreePort();

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE CRemotingLink::Atomic(AREF Addr, DWORD Data, DWORD Mask)
{
	//AfxTrace("CRemotingLink::Atomic()\n");

	ClaimPort();

	m_Req.Start(servComms, opAtomic);

	m_Req.AddLong(LONG(Addr.m_Ref));

	m_Req.AddLong(LONG(Data));

	m_Req.AddLong(LONG(Mask));

	if( Transact() ) {
		
		if( m_Rep.GetOpcode() == opRep ) {

			UINT uPtr = m_Rep.GetHeadSize();

			UINT Code = m_Rep.ReadLong(uPtr);

			FreePort();

			return Code;
			}
		}

	FreePort();

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

// Transaction 

bool CRemotingLink::RecvFrame(CRemotingFrame &Frame, UINT uTimeout)
{	
	ClaimPort();

	if( m_pData ) {

		Frame.Clear();

		PBYTE pData = Frame.GetData();

		UINT  uRecv = 0;

		for(;;) {

			UINT Data = m_pData->Read(uRecv ? FOREVER : uTimeout);

			if( Data != NOTHING ) {

				pData[uRecv++] = BYTE(Data);

				if( uRecv >= Frame.GetHeadSize() && uRecv == Frame.GetCount() ) {

					FreePort();
							
					return true;
					}

				continue;
				}
		
			break;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::SendFrame(CRemotingFrame &Frame, UINT uTimeout)
{
	ClaimPort();

	if( m_pData ) {

		Frame.End();

		if( m_pData->Write(Frame.GetData(), Frame.GetSize(), 2500) ) {

			FreePort();

			return true;
			}
		}

	FreePort();

	return false;
	}

bool CRemotingLink::Transact(void)
{
	ClaimPort();

	m_Rep.Clear();

	if( SendFrame(m_Req, 2500) ) {

		if( RecvFrame(m_Rep, 2500) ) {

			if( m_Rep.GetTransact() == m_Req.GetTransact() ) {

				FreePort();

				return true;
				}
			}

		FreePort();

		return false;
		}

	FreePort();
			
	return false;
	}

//////////////////////////////////////////////////////////////////////////
//
// Remoting Master Driver
//

// Instantiator

clink void CreateRemotingMasterDriver(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CRemotingMasterDriver);
		
		return;
		}
		
	NewHere(pData) CRemotingMasterDriver;
	}

// Constructor

CRemotingMasterDriver::CRemotingMasterDriver(void)
{
	//::AfxTrace("CRemotingMasterDriver::CRemotingMasterDriver\n");

	m_Flags		= DF_REMOTED;

	m_pConfig	= NULL;

	m_uConfig	= 0;

	m_MasterFlags	= 0; 
	}

// Config

void MCALL CRemotingMasterDriver::Load(LPCBYTE pData, UINT uSize)
{
	//::AfxTrace("CRemotingMasterDriver::Load\n");

	m_pConfig = pData;

	m_uConfig = uSize;
	}

void MCALL CRemotingMasterDriver::CheckConfig(CSerialConfig &Config)
{
	//::AfxTrace("CRemotingMasterDriver::CheckConfig\n");

	m_Serial = Config;

	Config.m_uFlags |= flagEncapMode;
	}
		
// Management

void MCALL CRemotingMasterDriver::Attach(IPortObject *pPort)
{
	//::AfxTrace("CRemotingMasterDriver::Attach\n");

	if( pPort ) {

		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);

		m_Link.Attach(pPort);

		m_Link.Attach(m_pData);
		}
	}

void MCALL CRemotingMasterDriver::Detach(void)
{
	//::AfxTrace("CRemotingMasterDriver::Detach\n");

	m_Link.DriverSelect(0);

	m_Link.Detach();

	CMasterDriver::Detach();
	}

void MCALL CRemotingMasterDriver::Open(void)
{
	//::AfxTrace("CRemotingMasterDriver::Open\n");

	CMasterDriver::Open();

	m_Link.ClaimPort();

	m_Link.DriverSelect(m_Ident);

	m_Link.DriverConfig(m_pConfig, m_uConfig);

	m_Link.DriverCheck(m_Serial);

	m_Link.DriverFlags(0, m_Flags);

	m_Link.DriverFlags(1, m_MasterFlags);

	m_Link.DriverOpen();

	m_Link.FreePort();

	m_Flags |= DF_REMOTED;
	}
		
void MCALL CRemotingMasterDriver::Close(void)
{
	//::AfxTrace("CRemotingMasterDriver::Close\n");

	m_Link.DriverClose();

	CMasterDriver::Close();
	}
		
// Device

CCODE MCALL CRemotingMasterDriver::DeviceOpen(ICommsDevice *pDevice)
{
	//::AfxTrace("CRemotingMasterDriver::DeviceOpen\n");

	CMasterDriver::DeviceOpen(pDevice);

	if( COMMS_ERROR(m_Link.DeviceOpen(DWORD(pDevice), NULL, 0)) ) {

		PCBYTE pConfig = m_pDevice->GetConfig();

		UINT   uConfig = m_pDevice->GetConfigSize();

		return m_Link.DeviceOpen(DWORD(pDevice), pConfig, uConfig);
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CRemotingMasterDriver::DeviceClose(BOOL fPersistConnection)
{
	//::AfxTrace("CRemotingMasterDriver::DeviceClose\n");

	m_Link.DeviceClose(DWORD(m_pDevice), fPersistConnection);

	return CMasterDriver::DeviceClose(fPersistConnection);
	}

// Entry Points

void MCALL CRemotingMasterDriver::Service(void)
{
	if( !m_Link.CheckPort() ) {

		Restart();
		}

	if( !(m_Flags & DF_NO_SERVICE) ) {

		m_Link.DriverService();
		}
	}

// User Access

UINT MCALL CRemotingMasterDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	//::AfxTrace("CRemotingMasterDriver::DrvCtrl\n");

	return m_Link.DriverCtrl(uFunc, Value);
	}

UINT MCALL CRemotingMasterDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	//::AfxTrace("CRemotingMasterDriver::DevCtrl\n");

	return m_Link.DeviceCtrl(DWORD(m_pDevice), uFunc, Value);
	}

// Master

WORD MCALL CRemotingMasterDriver::GetMasterFlags(void)
{
	//::AfxTrace("CRemotingMasterDriver::GetMasterFlags\n");

	return m_MasterFlags;
	}

CCODE MCALL CRemotingMasterDriver::Ping(void)
{
	//::AfxTrace("CRemotingMasterDriver::Ping\n");
	
	return m_Link.Ping();
	}

CCODE MCALL CRemotingMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	//::AfxTrace("CRemotingMasterDriver::Read\n");

	return m_Link.Read(Addr, pData, uCount);
	}

CCODE MCALL CRemotingMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	//::AfxTrace("CRemotingMasterDriver::Write\n");

	return m_Link.Write(Addr, pData, uCount);
	}

CCODE MCALL CRemotingMasterDriver::Atomic(AREF Addr, DWORD Data, DWORD Mask)
{
	//::AfxTrace("CRemotingMasterDriver::Atomic\n");

	return m_Link.Atomic(Addr, Data, Mask);
	}

// Implementation

void CRemotingMasterDriver::Restart(void)
{
	//::AfxTrace("CRemotingDriver::Restart()\n");

	m_Link.ClaimPort();

	m_Link.DriverClose();

	m_Link.DriverSelect(m_Ident);

	m_Link.DriverCheck(m_Serial);

	m_Link.DriverConfig(m_pConfig, m_uConfig);

	m_Link.DriverFlags(0, m_Flags);

	m_Link.DriverFlags(1, m_MasterFlags);

	m_Link.DriverOpen();

	if( m_pDevice ) {

		if( COMMS_ERROR(m_Link.DeviceOpen(DWORD(m_pDevice), NULL, 0)) ) {

			PCBYTE pConfig = m_pDevice->GetConfig();

			UINT   uConfig = m_pDevice->GetConfigSize();

			m_Link.DeviceOpen(DWORD(m_pDevice), pConfig, uConfig);
			}
		}

	m_Link.FreePort();
	}

//////////////////////////////////////////////////////////////////////////
//
// Remting Slave Driver
//

// Instantiator

clink void CreateRemotingSlaveDriver(void *pData, UINT *pSize)
{
	if( !pData ) {
		
		*pSize = sizeof(CRemotingSlaveDriver);
		
		return;
		}
		
	NewHere(pData) CRemotingSlaveDriver;
	}

// Constructor

CRemotingSlaveDriver::CRemotingSlaveDriver(void)
{
	//::AfxTrace("CRemotingSlaveDriver::CRemotingSlaveDriver\n");

	m_Flags   = DF_REMOTED;

	m_pConfig = NULL;

	m_uConfig = 0;
	}

// Config

void MCALL CRemotingSlaveDriver::Load(LPCBYTE pData, UINT uSize)
{
	//::AfxTrace("CRemotingSlaveDriver::Load\n");

	m_pConfig = pData;

	m_uConfig = uSize;
	}

void MCALL CRemotingSlaveDriver::CheckConfig(CSerialConfig &Config)
{
	//::AfxTrace("CRemotingSlaveDriver::CheckConfig\n");

	m_Serial = Config;

	Config.m_uFlags |= flagEncapMode;
	}
		
// Management

void MCALL CRemotingSlaveDriver::Attach(IPortObject *pPort)
{
	//::AfxTrace("CRemotingSlaveDriver::Attach\n");

	if( pPort ) {

		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);

		m_Link.Attach(pPort);

		m_Link.Attach(m_pData);
		}
	}

void MCALL CRemotingSlaveDriver::Detach(void)
{
	//::AfxTrace("CRemotingSlaveDriver::Detach\n");

	m_Link.Detach();

	CSlaveDriver::Detach();
	}

void MCALL CRemotingSlaveDriver::Open(void)
{
	//::AfxTrace("CRemotingSlaveDriver::Open\n");
	
	CSlaveDriver::Open();

	m_Flags |= DF_REMOTED;
	}
		
// Entry Points

void MCALL CRemotingSlaveDriver::Service(void)
{
	Start();
	
	CRemotingFrame &Req = (CRemotingFrame &) m_Link.GetReq();
	
	CRemotingFrame &Rep = (CRemotingFrame &) m_Link.GetRep();
	
	for(;;) {

		if( !m_Link.CheckPort() ) {

			AfxTrace("CRemotingSlaveDriver::PnP Event detected!\n");

			Start();
			}
		
		if( m_Link.RecvFrame(Req, 100) ) {

			#if _XDEBUG 

			AfxTrace("Slave Request\n");

			AfxTrace("Magic    : %8.8X\n", Req.GetMagic());
			AfxTrace("Service  : %4.4X\n", Req.GetService());
			AfxTrace("Opcode   : %4.4X\n", Req.GetOpcode());
			AfxTrace("Transact : %4.4X\n", Req.GetTransact());
			AfxTrace("Count    : %d\n",    Req.GetCount());

			#endif
			
			Rep.Clear();

			Rep.Start(Req.GetService(), CRemotingLink::opNak, Req.GetTransact()); 

			if( Req.IsValid() && Req.GetService() == CRemotingLink::servComms ) {
				
				if( Req.GetOpcode() == CRemotingLink::opRead ) {

					Read(Req, Rep);
					}

				if( Req.GetOpcode() == CRemotingLink::opWrite ) {

					Write(Req, Rep);
					}
				}

			#if _XDEBUG 

			AfxTrace("Slave Respsonse\n");

			AfxTrace("Magic    : %8.8X\n", Rep.GetMagic());
			AfxTrace("Service  : %4.4X\n", Rep.GetService());
			AfxTrace("Opcode   : %4.4X\n", Rep.GetOpcode());
			AfxTrace("Transact : %4.4X\n", Rep.GetTransact());
			AfxTrace("Count    : %d\n",    Rep.GetSize());

			#endif

			m_Link.SendFrame(Rep, FOREVER);
			}
		}
	}
		
// User Access

UINT MCALL CRemotingSlaveDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	//::AfxTrace("CRemotingSlaveDriver::DrvCtrl - TODO\n");
	
	return CCODE_ERROR;
	}

UINT MCALL CRemotingSlaveDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	//::AfxTrace("CRemotingSlaveDriver::DevCtrl - TODO\n");
	
	return CCODE_ERROR;
	}

// Implementation

void CRemotingSlaveDriver::Start(void)
{
	//::AfxTrace("CRemotingSlaveDriver::Start\n");

	m_Link.DriverSelect(m_Ident);

	m_Link.DriverConfig(m_pConfig, m_uConfig);

	m_Link.DriverCheck(m_Serial);

	m_Link.DriverFlags(0, m_Flags);

	m_Link.DriverOpen();

	if( m_pDevice ) {

		if( COMMS_ERROR(m_Link.DeviceOpen(DWORD(m_pDevice), NULL, 0)) ) {

			PCBYTE pConfig = m_pDevice->GetConfig();

			UINT   uConfig = m_pDevice->GetConfigSize();

			m_Link.DeviceOpen(DWORD(m_pDevice), pConfig, uConfig);
			}
		}

	m_Link.DriverService();
	}

void CRemotingSlaveDriver::Read(CRemotingFrame const &Req, CRemotingFrame &Rep)
{
	CAddress Addr;

	UINT uPtr   = Req.GetHeadSize();

	Addr.m_Ref  = Req.ReadLong(uPtr);

	WORD uCount = Req.ReadWord(uPtr);

	Rep.AddLong(0);

	MakeMin(uCount, Rep.GetSpace() / sizeof(DWORD));

	PDWORD pData = PDWORD(Rep.GetData() + Rep.GetSize());

	CCODE  Code  = CSlaveDriver::Read(Addr, pData, uCount);

	Rep.SetOpcode(CRemotingLink::opRep);
	
	Rep.SetCount(Rep.GetHeadSize());

	Rep.AddLong(Code);
		
	if( COMMS_SUCCESS(Code) ) {

		uCount = uCount * sizeof(DWORD); 

		Rep.SetCount(Rep.GetSize() + uCount);
		}
	}

void CRemotingSlaveDriver::Write(CRemotingFrame const &Req, CRemotingFrame &Rep)
{
	CAddress Addr;

	UINT   uPtr   = Req.GetHeadSize();

	Addr.m_Ref    = Req.ReadLong(uPtr);

	WORD   wCount = Req.ReadWord(uPtr);

	PCBYTE pData  = Req.ReadData(uPtr, wCount * sizeof(DWORD));

	CCODE  Code   = CSlaveDriver::Write(Addr, PDWORD(pData), wCount);
		
	Rep.SetOpcode(CRemotingLink::opRep);
	
	Rep.AddLong(Code);
	}

// End of File
