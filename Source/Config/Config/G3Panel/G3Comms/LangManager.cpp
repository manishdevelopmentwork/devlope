
#include "Intern.hpp"

#include "LangManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangItem.hpp"
#include "LangList.hpp"
#include "LangManagerPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Language Manager
//

// Dynamic Class

AfxImplementDynamicClass(CLangManager, CUIItem);

// Constructor

CLangManager::CLangManager(void)
{
	m_Handle  = HANDLE_NONE;

	m_Lang    = 0;

	m_System  = C3L_ENGLISH_US;

	m_Global  = 1;

	m_LocSep  = 1;

	m_LexMode = 0;

	m_Server  = 0;

	m_pLangs  = New CLangList;

	m_Bitmap.Create(L"Flags");

	FindSystemInfo();
	}

// UI Update

void CLangManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Server" ) {

		pHost->EnableUI("AppID", m_Server == 1);
		}

	if( Tag.IsEmpty() || Tag == "LexMode" ) {

		pHost->EnableUI("Server", m_LexMode < 2);

		pHost->EnableUI("AppID",  m_LexMode < 2);
		}
	}

// Current Language

UINT CLangManager::GetCurrentSlot(void) const
{
	return m_Lang;
	}

UINT CLangManager::GetCurrentCode(void) const
{
	return GetCodeFromSlot(m_Lang);
	}

UINT CLangManager::GetEffectiveCode(void) const
{
	CLangItem *pLang = m_pLangs->GetItem(m_Lang);

	if( pLang->m_Lang ) {

		return pLang->m_Lang;
		}

	return m_System;
	}

UINT CLangManager::GetNumericCode(void) const
{
	CLangItem *pLang = m_pLangs->GetItem(m_Lang);

	if( pLang->m_Num ) {

		if( pLang->m_Lang ) {

			return pLang->m_Lang;
			}

		return m_System;
		}

	return C3L_ENGLISH_US;
	}

WCHAR CLangManager::GetDecPointChar(void) const
{
	switch( GetNumericCode() ) {

		case C3L_FRENCH_FR:
		case C3L_FRENCH_BE:
		case C3L_SPANISH_ES:
		case C3L_SPANISH_MX:
		case C3L_GERMAN_DE:
		case C3L_GERMAN_AT:
		case C3L_GERMAN_CH:
		case C3L_ITALIAN_IT:
		case C3L_ITALIAN_CH:
		case C3L_FINNISH:

			return L',';
		}

	return L'.';
	}

WCHAR CLangManager::GetNumGroupChar(void) const
{
	switch( GetNumericCode() ) {

		case C3L_FRENCH_FR:
		case C3L_FRENCH_BE:
		case C3L_FRENCH_CH:

			return spaceNarrow;

		case C3L_SPANISH_ES:
		case C3L_SPANISH_MX:
		case C3L_GERMAN_DE:
		case C3L_GERMAN_AT:
		case C3L_GERMAN_CH:
		case C3L_ITALIAN_IT:
		case C3L_ITALIAN_CH:
		case C3L_FINNISH:

			return L'.';
		}

	return L',';
	}

BOOL CLangManager::IsGroupBoundary(UINT p) const
{
	if( p ) {

		switch( GetNumericCode() ) {

			case C3L_ENGLISH_IN:

				if( p == 3 ) {

					return TRUE;
					}

				if( p >= 5 && (p - 5) % 2 == 0 ) {

					return TRUE;
					}

				return FALSE;
			}

		return p % 3 == 0;
		}

	return FALSE;
	}

CString CLangManager::GetMonthName(UINT m) const
{
	UINT c1 = GetEffectiveCode();

	UINT c2 = c1 & 0xFF00;

	switch( c1 ) {

		case C3L_SPANISH_ES:

			switch( m ) {

				case  1: return L"Ene";
				case  2: return L"Feb";
				case  3: return L"Mar";
				case  4: return L"Abr";
				case  5: return L"May";
				case  6: return L"Jun";
				case  7: return L"Jul";
				case  8: return L"Agos";
				case  9: return L"Sep";
				case 10: return L"Oct";
				case 11: return L"Nov";
				case 12: return L"Dic";
				}
			break;

		case C3L_FINNISH:

			switch( m ) {

				case  1: return L"Tam";
				case  2: return L"Hel";
				case  3: return L"Maa";
				case  4: return L"Huh";
				case  5: return L"Tou";
				case  6: return L"Kes";
				case  7: return L"Hei";
				case  8: return L"Elo";
				case  9: return L"Syy";
				case 10: return L"Lok";
				case 11: return L"Mar";
				case 12: return L"Jou";
				}
			break;

		case C3L_TRAD_CHINESE:
		case C3L_SIMP_CHINESE:

			switch( m ) {

				case  1: return L"\x4E00\x6708";
				case  2: return L"\x4E8C\x6708";
				case  3: return L"\x4E09\x6708";
				case  4: return L"\x56DB\x6708";
				case  5: return L"\x4E94\x6708";
				case  6: return L"\x516D\x6708";
				case  7: return L"\x4E03\x6708";
				case  8: return L"\x516B\x6708";
				case  9: return L"\x4E5D\x6708";
				case 10: return L"\x5341\x6708";
				case 11: return L"\x5341\x4E00\x6708";
				case 12: return L"\x5341\x4E8C\x6708";
				}
			break;

		case C3L_KOREAN_FULL:

			switch( m ) {

				case  1: return L"1\xC8D4";
				case  2: return L"2\xC8D4";
				case  3: return L"3\xC8D4";
				case  4: return L"4\xC8D4";
				case  5: return L"5\xC8D4";
				case  6: return L"6\xC8D4";
				case  7: return L"7\xC8D4";
				case  8: return L"8\xC8D4";
				case  9: return L"9\xC8D4";
				case 10: return L"10\xC8D4";
				case 11: return L"11\xC8D4";
				case 12: return L"12\xC8D4";
				}
			break;

		case C3L_RUSSIAN:

			switch( m ) {

				case  1: return L"\x042F\x041D\x0412";
				case  2: return L"\x0424\x0415\x0412";
				case  3: return L"\x043C\x0435\x0441";
				case  4: return L"\x0410\x041F\x0420";
				case  5: return L"\x041C\x0410\x0419";
				case  6: return L"\x0418\x042E\x041D";
				case  7: return L"\x0418\x042E\x041B";
				case  8: return L"\x0410\x0412\x0413";
				case  9: return L"\x0421\x0415\x041D";
				case 10: return L"\x041E\x041A\x0422";
				case 11: return L"\x041D\x041E\x042F";
				case 12: return L"\x0414\x0415\x041A";
				}
			break;

		case C3L_JAPANESE:

			switch( m ) {

				case  1: return L"\x4E00\x6708";
				case  2: return L"\x4E8C\x6708";
				case  3: return L"\x4E09\x6708";
				case  4: return L"\x56DB\x6708";
				case  5: return L"\x4E94\x6708";
				case  6: return L"\x516D\x6708";
				case  7: return L"\x4E03\x6708";
				case  8: return L"\x516B\x6708";
				case  9: return L"\x4E5D\x6708";
				case 10: return L"\x5341\x6708";
				case 11: return L"\x5341\x4E00\x6708";
				case 12: return L"\x5341\x4E8C\x6708";
				}
			break;
		}

	switch( c2 ) {

		case C3L_FRENCH:

			switch( m ) {

				case  1: return L"Jan";
				case  2: return L"F�v";
				case  3: return L"Mar";
				case  4: return L"Avr";
				case  5: return L"Mai";
				case  6: return L"Juin";
				case  7: return L"Juil";
				case  8: return L"Ao�";
				case  9: return L"Sep";
				case 10: return L"Oct";
				case 11: return L"Nov";
				case 12: return L"Dec";
				}
			break;

		case C3L_GERMAN:

			switch( m ) {

				case  1: return L"Jan";
				case  2: return L"Feb";
				case  3: return L"M�r";
				case  4: return L"Apr";
				case  5: return L"Mai";
				case  6: return L"Jun";
				case  7: return L"Jul";
				case  8: return L"Aug";
				case  9: return L"Sep";
				case 10: return L"Okt";
				case 11: return L"Nov";
				case 12: return L"Dez";
				}
			break;

		case C3L_ITALIAN:

			switch( m ) {

				case  1: return L"Gen";
				case  2: return L"Feb";
				case  3: return L"Mar";
				case  4: return L"Apr";
				case  5: return L"Mag";
				case  6: return L"Giu";
				case  7: return L"Lug";
				case  8: return L"Ago";
				case  9: return L"Set";
				case 10: return L"Ott";
				case 11: return L"Nov";
				case 12: return L"Dic";
				}
			break;
		}

	switch( m ) {

		case  1: return L"Jan";
		case  2: return L"Feb";
		case  3: return L"Mar";
		case  4: return L"Apr";
		case  5: return L"May";
		case  6: return L"Jun";
		case  7: return L"Jul";
		case  8: return L"Aug";
		case  9: return L"Sep";
		case 10: return L"Oct";
		case 11: return L"Nov";
		case 12: return L"Dec";
		}

	return L"xxx";
	}

UINT CLangManager::GetTimeFormat(void) const
{
	switch( GetEffectiveCode() ) {

		case C3L_ENGLISH_US:
		case C3L_ENGLISH_IN:

			return 2;
		}

	return 1;
	}

UINT CLangManager::GetDateFormat(void) const
{
	switch( GetEffectiveCode() ) {

		case C3L_ENGLISH_US:

			return 1;

		case C3L_SIMP_CHINESE:
		case C3L_TRAD_CHINESE:
		case C3L_KOREAN_JAMO:
		case C3L_KOREAN_FULL:
		case C3L_JAPANESE:

			return 3;
		}

	return 2;
	}

WCHAR CLangManager::GetDateSepChar(void) const
{
	switch( GetEffectiveCode() ) {

		case C3L_ENGLISH_IN:
		case C3L_FRENCH_FR:
		case C3L_FRENCH_BE:
		case C3L_FRENCH_CH:
		case C3L_ITALIAN_IT:
		case C3L_ITALIAN_CH:

			return L'/';

		case C3L_JAPANESE:

			return L'.';
		}

	return L'-';
	}

// Attributes

UINT CLangManager::GetSlotCount(void) const
{
	return m_pLangs->GetItemCount();
	}

UINT CLangManager::GetUsedCount(void) const
{
	UINT n = m_pLangs->GetItemCount();

	while( n && GetAbbrFromSlot(n-1).IsEmpty() ) {

		n--;
		}

	return n;
	}

BOOL CLangManager::IsCodeUsed(UINT uCode) const
{
	UINT c = m_pLangs->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		if( GetCodeFromSlot(n) == uCode ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CLangManager::IsSlotUsed(UINT uSlot) const
{
	if( uSlot < GetSlotCount() ) {

		CLangItem *pLang = m_pLangs->GetItem(uSlot);

		if( pLang->m_Lang == C3L_GENERIC ) {

			if( pLang->m_Code.IsEmpty() ) {

				return FALSE;
				}

			return TRUE;
			}

		if( uSlot ) {

			if( pLang->m_Lang == GetCodeFromSlot(0) ) {

				return FALSE;
				}
			}

		return TRUE;
		}

	return FALSE;
	}

UINT CLangManager::EnumLanguages(CArray <UINT> &List) const
{
	List.Append(C3L_SYSTEM);
	List.Append(C3L_GENERIC);
	List.Append(C3L_ENGLISH_US);
	List.Append(C3L_ENGLISH_UK);
	List.Append(C3L_ENGLISH_IN);
	List.Append(C3L_FINNISH);
	List.Append(C3L_FRENCH_FR);
	List.Append(C3L_FRENCH_CA);
	List.Append(C3L_FRENCH_BE);
	List.Append(C3L_FRENCH_CH);
	List.Append(C3L_GERMAN_DE);
	List.Append(C3L_GERMAN_AT);
	List.Append(C3L_GERMAN_CH);
	List.Append(C3L_ITALIAN_IT);
	List.Append(C3L_ITALIAN_CH);
	List.Append(C3L_SPANISH_ES);
	List.Append(C3L_SPANISH_MX);
	List.Append(C3L_RUSSIAN);
	List.Append(C3L_SIMP_CHINESE);
	List.Append(C3L_KOREAN_JAMO);
	List.Append(C3L_KOREAN_FULL);
	List.Append(C3L_JAPANESE);

	return List.GetCount();
	}

UINT CLangManager::GetSystemCode(void) const
{
	return m_System;
	}

UINT CLangManager::GetCodeFromSlot(UINT uSlot) const
{
	if( uSlot < GetSlotCount() ) {

		CLangItem *pLang  = m_pLangs->GetItem(uSlot);

		return pLang->m_Lang;
		}

	return 0;
	}

UINT CLangManager::GetCurrentFlag(void) const
{
	return GetFlagFromCode(GetCurrentCode());
	}

UINT CLangManager::GetFlagFromSlot(UINT uSlot) const
{
	return GetFlagFromCode(GetCodeFromSlot(uSlot));
	}

UINT CLangManager::GetFlagFromCode(UINT uCode) const
{
	if( uCode == 0 ) {

		uCode = GetSystemCode();
		}

	switch( uCode ) {

		case C3L_SYSTEM:	return 81;
		case C3L_GENERIC:	return 81;
		case C3L_ENGLISH_US:	return 230;
		case C3L_ENGLISH_UK:	return 78;
		case C3L_ENGLISH_IN:	return 105;
		case C3L_FRENCH_FR:	return 76;
		case C3L_FRENCH_CA:	return 35;
		case C3L_FRENCH_BE:	return 19;
		case C3L_FRENCH_CH:	return 41;
		case C3L_SPANISH_ES:	return 67;
		case C3L_SPANISH_MX:	return 155;
		case C3L_GERMAN_DE:	return 55;
		case C3L_GERMAN_AT:	return 11;
		case C3L_GERMAN_CH:	return 41;
		case C3L_ITALIAN_IT:	return 110;
		case C3L_ITALIAN_CH:	return 41;
		case C3L_FINNISH:	return 71;
		case C3L_SIMP_CHINESE:	return 46;
		case C3L_TRAD_CHINESE:	return 225;
		case C3L_RUSSIAN:	return 189;
		case C3L_KOREAN_JAMO:	return 121;
		case C3L_KOREAN_FULL:	return 121;
		case C3L_JAPANESE:	return 113;
		}

	return 81;
	}

UINT CLangManager::GetFlagFromAbbr(CString Abbr) const
{
	if( Abbr == L"en"    ) return 230;
	if( Abbr == L"fr"    ) return 76;
	if( Abbr == L"es"    ) return 67;
	if( Abbr == L"de"    ) return 55;
	if( Abbr == L"it"    ) return 110;
	if( Abbr == L"zh-cn" ) return 46;
	if( Abbr == L"ru"    ) return 189;
	if( Abbr == L"ko-1"  ) return 121;
	if( Abbr == L"ko-2"  ) return 121;
	if( Abbr == L"fi"    ) return 71;
	if( Abbr == L"ja"    ) return 113;

	return 81;
	}

CString CLangManager::GetCurrentName(void) const
{
	return GetNameFromCode(GetCurrentCode());
	}

CString CLangManager::GetNameFromSlot(UINT uSlot) const
{
	if( uSlot < GetSlotCount() ) {

		CLangItem *pLang = m_pLangs->GetItem(uSlot);

		if( pLang->m_Lang == C3L_GENERIC ) {

			CString Name = IDS_GENERIC;

			if( pLang->m_Code.GetLength() ) {

				Name += L" (";

				Name += pLang->m_Code;

				Name += L")";
				}

			return Name;
			}

		return GetNameFromCode(pLang->m_Lang);
		}

	return L"";
	}

CString CLangManager::GetNameFromCode(UINT uCode) const
{
	switch( uCode ) {

		case C3L_SYSTEM:	return IDS_SYSTEM_LOCALE;
		case C3L_GENERIC:	return IDS_GENERIC;
		case C3L_ENGLISH_US:	return IDS_ENGLISH_US;
		case C3L_ENGLISH_UK:	return IDS_ENGLISH_UK;
		case C3L_ENGLISH_IN:	return IDS_ENGLISH_INDIA;
		case C3L_FRENCH_FR:	return IDS_FRENCH_FRANCE;
		case C3L_FRENCH_CA:	return IDS_FRENCH_CANADA;
		case C3L_FRENCH_BE:	return IDS_FRENCH_BELGIUM;
		case C3L_FRENCH_CH:	return IDS_FRENCH;
		case C3L_SPANISH_ES:	return IDS_SPANISH_SPAIN;
		case C3L_SPANISH_MX:	return IDS_SPANISH_MEXICO;
		case C3L_GERMAN_DE:	return IDS_GERMAN_GERMANY;
		case C3L_GERMAN_AT:	return IDS_GERMAN_AUSTRIA;
		case C3L_GERMAN_CH:	return IDS_GERMAN;
		case C3L_ITALIAN_IT:	return IDS_ITALIAN_ITALY;
		case C3L_ITALIAN_CH:	return IDS_ITALIAN;
		case C3L_FINNISH:	return IDS_FINNISH;
		case C3L_SIMP_CHINESE:	return IDS_SIMPLIFIED;
		case C3L_TRAD_CHINESE:	return IDS_TRADITIONAL;
		case C3L_RUSSIAN:	return IDS_RUSSIAN;
		case C3L_KOREAN_JAMO:	return IDS_KOREAN_JAMO;
		case C3L_KOREAN_FULL:	return IDS_KOREAN_FULL;
		case C3L_JAPANESE:	return CString(IDS_JAPANESE);

		}

	return L"";
	}

CString CLangManager::GetAbbrFromSlot(UINT uSlot) const
{
	if( uSlot < GetSlotCount() ) {

		CLangItem *pLang = m_pLangs->GetItem(uSlot);

		if( pLang->m_Lang == C3L_GENERIC ) {

			return pLang->m_Code;
			}

		return GetAbbrFromCode(pLang->m_Lang);
		}

	return L"";
	}

CString CLangManager::GetAbbrFromCode(UINT uCode) const
{
	switch( uCode ) {

		case C3L_SYSTEM:	return L"en";
		case C3L_GENERIC:	return L"en";
		case C3L_ENGLISH_US:	return L"en";
		case C3L_ENGLISH_UK:	return L"en";
		case C3L_ENGLISH_IN:	return L"en";
		case C3L_FRENCH_FR:	return L"fr";
		case C3L_FRENCH_CA:	return L"fr";
		case C3L_FRENCH_BE:	return L"fr";
		case C3L_FRENCH_CH:	return L"fr";
		case C3L_SPANISH_ES:	return L"es";
		case C3L_SPANISH_MX:	return L"es";
		case C3L_GERMAN_DE:	return L"de";
		case C3L_GERMAN_AT:	return L"de";
		case C3L_GERMAN_CH:	return L"de";
		case C3L_ITALIAN_IT:	return L"it";
		case C3L_ITALIAN_CH:	return L"it";
		case C3L_FINNISH:	return L"fi";
		case C3L_SIMP_CHINESE:	return L"zh-cn";
		case C3L_TRAD_CHINESE:	return L"zh-tw";
		case C3L_RUSSIAN:	return L"ru";
		case C3L_KOREAN_JAMO:	return L"ko-1";
		case C3L_KOREAN_FULL:	return L"ko-2";
		case C3L_JAPANESE:	return L"ja";
		}

	return L"";
	}

CString CLangManager::GetKeybFromCode(UINT uCode) const
{
	switch( uCode ) {

		case C3L_ENGLISH_US:	return L"00000409";
		case C3L_ENGLISH_UK:	return L"00000809";
		case C3L_FRENCH_FR:	return L"0000040C";
		case C3L_FRENCH_CA:	return L"00000C0C";
		case C3L_FRENCH_BE:	return L"0000080C";
		case C3L_FRENCH_CH:	return L"0000100C";
		case C3L_SPANISH_ES:	return L"0000040A";
		case C3L_SPANISH_MX:	return L"0000080A";
		case C3L_GERMAN_DE:	return L"00000407";
		case C3L_GERMAN_AT:	return L"00000C07";
		case C3L_GERMAN_CH:	return L"00000807";
		case C3L_ITALIAN_IT:	return L"00000410";
		case C3L_ITALIAN_CH:	return L"00000810";
		case C3L_FINNISH:	return L"0000040B";
		case C3L_SIMP_CHINESE:	return L"00000804";
		case C3L_TRAD_CHINESE:	return L"00000404";
		case C3L_RUSSIAN:	return L"00000419";
		case C3L_KOREAN_JAMO:	return L"00000412";
		case C3L_KOREAN_FULL:	return L"00000412";
		case C3L_JAPANESE:	return L"00000411";
		}

	return m_SystemKeyb;
	}

CString CLangManager::GetAppID(void) const
{
	// MIKEG -- Is this obsolete?

	return L"F94643276DB21D820443452CC2D0B6AB150299A0";
	}

// Operations

BOOL CLangManager::InvokeDialog(CWnd &Wnd)
{
	CItemDialog Dlg(this, CString(IDS_CONFIGURE));

	return Dlg.Execute(Wnd);
	}

void CLangManager::DrawFlagFromSlot(CDC &DC, CRect Rect, UINT uSlot)
{
	DrawFlag(DC, Rect, GetFlagFromSlot(uSlot));
	}

void CLangManager::DrawFlagFromCode(CDC &DC, CRect Rect, UINT uCode)
{
	DrawFlag(DC, Rect, GetFlagFromCode(uCode));
	}

void CLangManager::DrawFlagFromAbbr(CDC &DC, CRect Rect, CString Abbr)
{
	DrawFlag(DC, Rect, GetFlagFromAbbr(Abbr));
	}

void CLangManager::DrawFlag(CDC &DC, CRect Rect, UINT uFlag)
{
	CSize  Size = CSize(16, 16);

	CPoint Pos  = Rect.GetTopLeft() + (Rect.GetSize() - Size) / 2;

	CPoint Org  = CPoint(16 * uFlag, 0);

	DC.SetBkColor(afxColor(MAGENTA));

	DC.TransBlt(CRect(Pos, Size), m_Bitmap, Org);
	}

BOOL CLangManager::SetKeyboard(void)
{
	return SetKeyboard(NOTHING);
	}

BOOL CLangManager::SetKeyboard(UINT uSlot)
{
	UINT uTest = 1;

	if( uSlot == NOTHING ) {

		uSlot = m_Lang;

		uTest = 2;
		}

	if( uSlot < GetSlotCount() ) {

		CLangItem *pItem = m_pLangs->GetItem(uSlot);

		if( pItem->m_Keyb >= uTest ) {

			UINT uCode = pItem->m_Lang;

			LoadKeyboardLayout( GetKeybFromCode(uCode),
					    KLF_ACTIVATE
					    );

			return TRUE;
			}
		}

	return FALSE;
	}

void CLangManager::ClearKeyboard(void)
{
	LoadKeyboardLayout( m_SystemKeyb,
			    KLF_ACTIVATE
			    );
	}

// UI Creation

BOOL CLangManager::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CLangManagerPage(this));

	return FALSE;
	}

// Persistance

void CLangManager::Init(void)
{
	CUIItem::Init();

	m_pLangs->SetItemCount(20);

	CLangItem *pLang = m_pLangs->GetItem(0u);

	pLang->m_Lang    = 0;

	pLang->m_Code    = GetAbbrFromCode(0);

	m_System         = m_SystemCode;
	}

void CLangManager::PostLoad(void)
{
	CUIItem::PostLoad();

	m_pLangs->SetItemCount(20);
	}

// Download Support

BOOL CLangManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_LANGUAGE);

	CUIItem::MakeInitData(Init);

	UINT uCount = GetSlotCount();

	Init.AddWord(WORD(m_System));

	Init.AddByte(BYTE(m_Global));

	Init.AddByte(BYTE(m_LocSep));

	Init.AddWord(WORD(uCount));

	for( UINT n = 0; n < uCount; n++ ) {

		CLangItem *pLang = m_pLangs->GetItem(n);

		Init.AddWord(WORD(pLang->m_Lang));

		Init.AddWord(WORD(pLang->m_Num));
		}

	return TRUE;
	}

// Meta Data Creation

void CLangManager::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(Lang);
	Meta_AddInteger(System);
	Meta_AddInteger(Global);
	Meta_AddInteger(LocSep);
	Meta_AddInteger(LexMode);
	Meta_AddInteger(Server);
	Meta_AddString (AppID);
	Meta_AddCollect(Langs);

	Meta_SetName((IDS_LANGUAGE_MANAGER));
	}

// Implementation

void CLangManager::FindSystemInfo(void)
{
	LCID   Lang  = GetThreadLocale();

	WCHAR *pText = New WCHAR [ KL_NAMELENGTH ];

	GetKeyboardLayoutName(pText);

	m_SystemKeyb = pText;

	m_SystemCode = C3L_ENGLISH_US;

	switch( PRIMARYLANGID(Lang) ) {

		case LANG_ENGLISH:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_ENGLISH_US:

					m_SystemCode = C3L_ENGLISH_US;

					break;

				case SUBLANG_ENGLISH_UK:

					m_SystemCode = C3L_ENGLISH_UK;

					break;
				}
			break;

		case LANG_FRENCH:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_FRENCH:

					m_SystemCode = C3L_FRENCH_FR;

					break;

				case SUBLANG_FRENCH_CANADIAN:

					m_SystemCode = C3L_FRENCH_CA;

					break;

				case SUBLANG_FRENCH_BELGIAN:

					m_SystemCode = C3L_FRENCH_BE;

					break;

				case SUBLANG_FRENCH_SWISS:

					m_SystemCode = C3L_FRENCH_CH;

					break;
				}
			break;

		case LANG_SPANISH:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_SPANISH:
				case SUBLANG_SPANISH_MODERN:

					m_SystemCode = C3L_SPANISH_ES;

					break;

				case SUBLANG_SPANISH_MEXICAN:

					m_SystemCode = C3L_SPANISH_MX;

					break;
				}
			break;

		case LANG_GERMAN:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_GERMAN:

					m_SystemCode = C3L_GERMAN_DE;

					break;

				case SUBLANG_GERMAN_AUSTRIAN:

					m_SystemCode = C3L_GERMAN_AT;

					break;

				case SUBLANG_GERMAN_SWISS:

					m_SystemCode = C3L_GERMAN_CH;

					break;
				}
			break;

		case LANG_ITALIAN:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_ITALIAN:

					m_SystemCode = C3L_ITALIAN_IT;

					break;

				case SUBLANG_ITALIAN_SWISS:

					m_SystemCode = C3L_ITALIAN_CH;

					break;
				}
			break;

		case LANG_RUSSIAN:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_DEFAULT:

					m_SystemCode = C3L_RUSSIAN;

					break;
				}
			break;

		case LANG_CHINESE:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_CHINESE_SIMPLIFIED:

					m_SystemCode = C3L_SIMP_CHINESE;

					break;

/*				case SUBLANG_CHINESE_TRADITIONAL:

					m_SystemCode = C3L_TRAD_CHINESE;

					break;
*/				}
			break;

		case LANG_KOREAN:

			switch( SUBLANGID(Lang) ) {

				case SUBLANG_DEFAULT:

					m_SystemCode = C3L_KOREAN_JAMO;

					break;
				}
		}

	delete [] pText;
	}

// End of File
