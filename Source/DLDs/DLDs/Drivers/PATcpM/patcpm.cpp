#include "intern.hpp"

#include "patcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop TCP/IP Driver
//

// Instantiator

INSTANTIATE(CPAMasterTCPDriver);

// Constructor

CPAMasterTCPDriver::CPAMasterTCPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CPAMasterTCPDriver::~CPAMasterTCPDriver(void)
{
	}

// Configuration

void MCALL CPAMasterTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CPAMasterTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CPAMasterTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CPAMasterTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;
		     			
			m_pCtx->m_IP      = GetAddr(pData);
			m_pCtx->m_uPort   = GetWord(pData);
			m_pCtx->m_fKeep   = GetByte(pData);
			m_pCtx->m_fPing   = GetByte(pData);
			m_pCtx->m_uTime1  = GetWord(pData);
			m_pCtx->m_uTime2  = GetWord(pData);
			m_pCtx->m_uTime3  = GetWord(pData);
			m_pCtx->m_bTimeWD = GetByte(pData);
			m_pCtx->m_bTickWD = GetByte(pData);
			m_pCtx->m_pSock   = NULL;
			m_pCtx->m_uLast   = GetTickCount();
			m_pCtx->m_pWatch  = NULL;
			m_pCtx->m_bDevice = GetByte(pData);
			m_pCtx->m_wGVA_Hi = 0;
			m_pCtx->m_wGVA_Lo = 0;
			m_pCtx->m_wDimGV  = 0;
		       				
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CPAMasterTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CPAMasterTCPDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}
			}
		}

	if( COMMS_SUCCESS(CAcroloopDriver::Ping()) ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

// Socket Management

BOOL CPAMasterTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			} 

		if( Phase == PHASE_OPEN )  {

			if( !m_pCtx->m_pWatch ) {

				SetWatchdog();
				} 
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPAMasterTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		if( m_pCtx->m_pSock->Connect(IP, m_pCtx->m_uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}

	return FALSE;
	}

void CPAMasterTCPDriver::CloseSocket(BOOL fAbort)
{
	KillWatchdog();
	
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CPAMasterTCPDriver::Transact(WORD CheckBytes)
{
	if( OpenSocket() ) {

		if( Send() && RecvFrame(CheckBytes) ) {

			return CheckReply(CheckBytes); 
			}

		CloseSocket(FALSE);
		}

	return FALSE; 
	}

BOOL CPAMasterTCPDriver::Send(void)
{
	// Binary padding
	
	UINT uPad  = m_uPtr % 4;

	if( uPad ) {

		uPad = 4 - uPad;

		while( uPad ) {

			AddByte(0x00);

			uPad--;
			}
		}
	
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {
			
			return TRUE;
			}
		}

	return FALSE; 
	}

BOOL CPAMasterTCPDriver::RecvFrame(UINT uBytes)
{
	UINT uPtr  = 0;

	UINT uSize = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) { 
			
			uPtr += uSize;
			
			if( uPtr >= 4 && uPtr >= UINT(uBytes + 4) ) {

				return( m_bRx[0] == 0 ); 
				}
								
			continue;
			}
	
		if( !CheckSocket() ) {

			return FALSE;
			} 
		
		Sleep(10);
	       	}

	return FALSE;  
	}

// Implementation

BOOL CPAMasterTCPDriver::IsAwake(AREF Addr)
{
	return TRUE;
	}

void CPAMasterTCPDriver::AddLong(DWORD dwData, PBYTE pBuff)
{
	UINT u = 0;
	
	pBuff[u++] = HIBYTE(HIWORD(dwData));

	pBuff[u++] = LOBYTE(HIWORD(dwData));

	pBuff[u++] = HIBYTE(LOWORD(dwData));

	pBuff[u]   = LOBYTE(LOWORD(dwData));
	}

// Watchdog

BOOL CPAMasterTCPDriver::SetWatchdog(void)
{
	if( m_pCtx->m_bTickWD * m_pCtx->m_bTimeWD == 0 ) {

		return TRUE;
		}
	
	UINT uCount = 6;

	UINT uSize  = sizeof(DWORD);
	
	PBYTE pDog = PBYTE(alloca(uCount * uSize));

	IPADDR IP;

	memset(PBYTE(&IP), m_pCtx->m_IP, sizeof(IPADDR));

	DWORD dwData = 1;

	WORD wPort = 0;

	for( UINT u = 0; u < uCount; u++) {

		dwData = 1;

		wPort  = 0;
	     		
		switch( u ) {

			case 1:
				dwData = m_pCtx->m_bTimeWD * 1000;
				break;
		       	case 2:
				dwData = m_pCtx->m_bTickWD;
				break;

			case 3:
				m_pCtx->m_pSock->GetLocal(IP, wPort);
				dwData = IP.m_b1 << 24 | IP.m_b2 << 16 | IP.m_b3 << 8 | IP.m_b4;
				break;

			case 4:
				m_pCtx->m_pSock->GetLocal(IP, wPort);
			     	dwData = wPort;
				break;

			case 5:
				dwData = 0;
				break;
			}

		AddLong(dwData, pDog + u * uSize);
		}
	 
	KillWatchdog();
		
	if( (m_pCtx->m_pWatch = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		uSize = uCount * uSize;

		if( m_pCtx->m_pWatch->Connect(IP, 5004) == S_OK ) {

			uCount = uSize;

			if( m_pCtx->m_pWatch->Send(pDog, uSize) == S_OK ) {

				if( uCount == uSize ) {

					SetTimer(m_pCtx->m_uTime1);

					while( GetTimer() ) {

						if( m_pCtx->m_pWatch->Recv(pDog, uSize) == S_OK ) {

							return TRUE;
							} 
						}
					}
				}

			KillWatchdog();
			}
		}

	return FALSE;
	}

void CPAMasterTCPDriver::KillWatchdog(void) 
{
	if( m_pCtx->m_pWatch ) {

		m_pCtx->m_pWatch->Close();

		m_pCtx->m_pWatch->Release();

		m_pCtx->m_pWatch = NULL;
		}
	}

BOOL CPAMasterTCPDriver::BinaryTx(void)
{
	if( OpenSocket() ) {

		if( Send() ) {
	
			return TRUE;
			}

		CloseSocket(FALSE);
		}
	
	return FALSE;
	}

UINT CPAMasterTCPDriver::GetSystemPointer(void)
{
	switch( m_pBase->m_bDevice ) {

		case 1:		return 0xA0600020;
		default:	return 0x00400009;
		
		}
	}

UINT CPAMasterTCPDriver::GetPeek(UINT uIndex)
{
	UINT uPeek = MAKELONG(m_pBase->m_wGVA_Lo, m_pBase->m_wGVA_Hi);

	if( m_pBase->m_bDevice ) {

		return uPeek + 4 + uIndex * 8;
		} 

	return uPeek + 1 + uIndex * 2;
	}

void CPAMasterTCPDriver::FixupDataIn(DWORD &dwData)
{
	// Aries CE's transfer data in IEEE64 format
	
	if( dwData > 0 && m_pBase->m_bDevice ) {
	
		Convert64To32(dwData);
		}
	}

void CPAMasterTCPDriver::FixupDataOut(DWORD &dwData)
{
	// Aries CE's transfer data in IEEE64 format
	
	if( dwData > 0 && m_pBase->m_bDevice ) {
	
		Convert32To64(dwData);
		}
	}

// End of File
