
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EMERSONEPI_HPP

#define	INCLUDE_EMERSONEPI_HPP

#include "emsepdlg.hpp"

#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

#define	SPHOLD	1
#define	SPAIN	2
#define	SPOUT	3
#define	SPINP	4
#define	SPH32	5
#define	SPA32	6

// Device Selection - Serial
#define	DVSEPP	1
#define	DVSSP	3
#define	DVSSK	5
#define	DVSGP	7
#define	DVSST	9
#define	DVSMC	11
#define	DVSBR	13
#define	DVSEPI	15
#define	DVSEPB	17
#define	DVSMP	19

// Device Selection - TCP
#define	DVTEPP	1
#define	DVTSP	3
#define	DVTSK	5
#define	DVTGP	7
#define	DVTST	9
#define	DVTMC	11
#define	DVTBR	13
#define	DVTMP	15

// Device ID's
#define	DVEPP	1
#define	DVSP	2
#define	DVSK	3
#define	DVGP	4
#define	DVST	5
#define	DVMC	6
#define	DVBR	7
#define	DVMP	8
#define	DVEPI	20
#define	DVEPB	21

#define	DMPRE	1	// Premapped names
#define	DMREG	3	// Registers only

// General Table Items
#define	ILFC	4	// Input Line Force On/Off Command Array
#define	ILFE	5	// Input Line Force On/Off Enable Array
#define	OLFC	6	// Output Line Force On/Off Command Array
#define	OLFE	7	// Output Line Force On/Off Enable Array
#define	OLA	8	// Output Line Active Off Array
#define	IFOA	9	// Input Function Active Off Array
#define	MCEA	11	// Motion Command Execute Array
#define	UDB	12	// User Defined Bits
#define	ILS	13	// Input Line Status Array"
#define	ILR	14	// Input Line Raw Status Array
#define	ILDS	15	// Input Line Debounced Status Array
#define	OLS	16	// Output Line Status Array
#define	IFSA	17	// Input Function Status Array
#define	OFS	18	// Output Function Status Array
#define	ILDT	19	// Input Line Debounce Time
#define	IFM	20	// Input Function Mapping 32
#define	OFM	21	// Output Function Mapping 32
#define	UDR	22	// User Defined Registers

// String Item Table Numbers
#define	TFPN	23	// Firmware Part Number
#define	TBPN	24	// Boot Part Number Drive
#define	TFRB	25	// Firmware Revision Base
#define	TFRO	26	// Firmware Revision Option (FM)
#define	TBRS	27	// Boot Revision
#define	TANS	28	// Axis Name String
#define	TUUS	29	// User Unit
#define	TUDM	30	// User Defined Motor Name
#define	TPSN	31	// Product Serial Number

// Generic Modbus Address Table Numbers
#define	GMBO	32	// 0xxx bit modbus address
#define	GMBI	33	// 1xxx bit modbus address
#define	GMBA	34	// 3xxx word modbus address
#define	GMBB	35	// 3xxx two word 32 bit modbus address
#define	GMBC	36	// 3xxx one word 32 bit modbus address
#define	GMBD	37	// 4xxx two word 32 bit modbus address
#define	GMBE	38	// 4xxx word modbus address
#define	GMBF	39	// 4xxx one word 32 bit modbus address

// Table Numbers for special commands
#define	IAC	40	// Index Acceleration
#define	ICR	41	// Index Control
#define	IXC	42	// Index Count
#define	IDC	43	// Index Deceleration
#define	IDS2	44	// Index Distance
#define	IDW	45	// Index Dwell
#define	IXT	46	// Index Type
#define	IXV	47	// Index Velocity
#define	RGO	48	// Registration Offset
#define	CNX	49	// Chain Next
#define	FPC	50	// Fault Power Up Count
#define	FPT	51	// Fault Power Up Time
#define	FTY	52	// Fault Type

// Solutions Module Table Addresses
#define	M15	60
#define	M16	61
#define	M17	62

// Motion Controller Table Addresses
#define	MCO	68
#define	MCI	69
#define	MCH	70

// String Item Offsets
#define	OFPN	39901	// Firmware Part Number
#define	OFPNL	39906	// Firmware Part Number Last
#define	OBPN	39910	// Boot Part Number Drive
#define	OBPNL	39916	// Boot Part Number Drive Last
#define	OFRB	39988	// Firmware Revision Base
#define	OFRBL	39989	// Firmware Revision Base Last
#define	OFRO	39990	// Firmware Revision Option (FM)
#define	OFROL	39991	// Firmware Revision Option (FM) Last
#define	OBRS	39995	// Boot Revision
#define	OBRSL	39996	// Boot Revision Last
#define	OANS	40005	// Drive Axis Name String
#define	OANSL	40016	// Drive Axis Name String Last
#define	OUUS	41029	// User Unit
#define	OUUSL	41031	// User Unit Last
#define	OUDM	42101	// User Defined Motor Name
#define	OUDML	42106	// User Defined Motor Name Last
#define	OPSN	49903	// Product Serial Number
#define	OPSNL	49910	// Product Serial Number Last

// List Sort Control
#define	SELMB	0	// Select Modbus Function List
#define	SELNM	1	// Numeric order
#define	SELAZ	2	// Alphabetic order

// Table addresses for headers

// Top Level Selections
#define	TLMB	164	// Modbus List Select
#define	TLMF	165	// Machine Function Select
#define	TLKY	166	// Keyword Select
#define	TLMC	167	// Motion Coordinator Select

// Sort Method Selections
#define	TLAL	169	// Alphabetic List Select
#define	TLRO	170	// Register Order List Select

// List Selection Headers for SP Pre-mapped
#define	TM01	171 // Speed Reference:Menu1 
#define	TM02	172 // Ramps: Menu2
#define	TM03	173 // Speed loop/Frequency: Menu3 
#define	TM04	174 // Torque&Current Control:Menu4 
#define	TM05	175 // MotorControl:Menu5
#define	TM06	176 // SequencerControl:Menu6
#define	TM07	177 // AnalogIO:Menu7
#define	TM08	178 // DigitalIO:Menu8
#define	TM09	179 // Logic/Mpot/BinarySum:Menu9
#define	TM10	180 // StatusTrips:Menu10
#define	TM11	181 // General:Menu11
#define	TM12	182 // Thresholds/Brake:Menu12
#define	TM13	183 // PositionControl:Menu13
#define	TM14	184 // UserPID:Menu14
#define	TM15	185 // Solutions Module:Menu15
#define	TM16	186 // Solutions Module: Menu16
#define	TM17	187 // Solutions Module: Menu17
#define	TM18	188 // ApplicationWord/Bit:Menu18
#define	TM19	189 // ApplicationWord/Bit:Menu19
#define	TM20	190 // ApplicationWord/DWord:Menu20
#define	TM21	191 // Motor 2 Basic
#define	TM22	192 // Menu0 Source 
#define	TM23	193 // Menu0 Functions
#define	TMEND	TM21 // End for all but MP
#define	TMENDM	TM23 // MP end
#define	MAXMENU	TMEND - TM01 + 1 // Last Menu Number

// Modbus Headers
#define	THDO	197	// Coil Select
#define	THDI	198	// Input Select
#define	THAI	199	// Input Reg Select
#define	THHR	200	// Holding Reg Select

// Motion Controller Header
#define	TMCH	201

// Operations Sorting EB/EI
#define	TDIA	227	// Diagnostics
#define	THOM	228	// Homing
#define	TIOS	229	// I/O
#define	TINX	230	// Index
#define	TJOG	231	// Jog
#define	TMOT	232	// Motion
#define	TPUL	233	// Pulse
#define	TREG	234	// Registration
#define	TSTT	235	// Status
#define	TSYS	236	// System

//////////////////////////////////////////////////////////////////////////
//
// Emerson Driver Options
//

#ifndef	EMERSON_DRIVER_OPTIONS
#define	EMERSON_DRIVER_OPTIONS

class CEmersonDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonDriverOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Delay;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Device Options
//

class CEmersonEPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonEPDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Unit;
		UINT m_Type;
		UINT m_DispMode;
		UINT m_EmID;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
		UINT GetType(UINT uID);
		UINT GetID(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP Driver
//

class CEmersonEPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEmersonEPDriver(void);

		// Destructor
		~CEmersonEPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL	CheckAlignment(CSpace *pSpace);

		// Listing Control
		void	SetListSelect(UINT *pListSelect);
		void	SetDeviceSelect(UINT *pDeviceSelect);
		void	GetLoaded(UINT *pLoaded);
		void	InitSpaces(void);

		// Space Selection
		void	SetIBSpaces(BOOL fDialog);
		void	SetSPSpaces(BOOL fDialog);
		void	SetSKSpaces(BOOL fDialog);
		void	SetGPSpaces(BOOL fDialog);
		void	SetSTSpaces(BOOL fDialog);
		void	SetMPSpaces(BOOL fDialog);
		void	SetMCSpaces(void);
		void	SetNonPSpaces(void);
		void	SetValidateSpaces(UINT uSelDev);

		BOOL	GetDriverType(void);

	protected:

		struct EMNamed
		{
			CString	P; // Prefix
			CString	C; // Caption
			UINT	M; // Minimum
			UINT	T; // Type
			};

 		// Data
		UINT	m_uListSel;
		UINT	m_DeviceSelect;
		UINT	m_FirstLoad;
		BOOL	m_fM15Loaded;
		BOOL	m_fM16Loaded;
		BOOL	m_fM17Loaded;
		BOOL	m_fMCLoaded;
		BOOL	m_fSPSLoaded;
		BOOL	m_fSKSLoaded;
		BOOL	m_fMPSLoaded;
		BOOL	m_fVSPLoaded;
		BOOL	m_fVSKLoaded;
		BOOL	m_fVMPLoaded;
		BOOL	m_fIBLoaded;
		BOOL	m_fILoaded;
		BOOL	m_fBLoaded;
		BOOL	m_fMBLoaded;
		BOOL	m_fIBHLoaded;
		BOOL	m_fMBHLoaded;
		BOOL	m_fComHLoaded;
		BOOL	m_fMFHLoaded;
		BOOL	m_fKYHLoaded;
		BOOL	m_fSortHLoaded;
		BOOL	m_fDriverType;

		EMNamed	EM[689];
		EMNamed	EK[476];
		EMNamed	VP[224];
		EMNamed	VK[28];
		EMNamed	MP[810];

		// Implementation
		void	AddSpace(CSpace * pSpace);

		// Pre-mapped Selection Spaces
		// EPB/EPI
		void	AddSpacesIB(void);
		void	AddSpacesB(void);
		void	AddSpacesI(void);

		// SP Pre-mapped
		void	AddSpacesSP1(void);

		// SK Pre-mapped
		void	AddSpacesSK1(void);

		// GP-20 Pre-mapped
		void	AddSpacesGP1(void);

		// Digitax Pre-mapped
		void	AddSpacesST1(void);

		// Mentor MP Pre-mapped
		void	AddSpacesMP1(void);

		// Commander Solutions Modules
		void	AddSpacesSM15(void);
		void	AddSpacesSM16(void);
		void	AddSpacesSM17(void);

		// Motion Commander
		void	AddSpacesMC(void);

		// Header Spaces
		// Generic Modbus
		void	AddSpacesMB(void);

		// EPB/EPI Headers
		void	AddSpacesIBH(void);

		// Modbus Headers
		void	AddSpacesMBH(void);

		// Commander Pre-mapped Headers
		void	AddSpacesComH(UINT uMenuSol);

		// Function Headers
		void	AddSpacesMFH(void);

		// Keyword Header
		void	AddSpacesKYH(void);

		// Sort Alpha/Numeric Headers
		void	AddSpacesSortH(void);

		// On Load
		void	AddSpaces(BOOL fPart);

		// All Commander Pre-mapped
		void	AddCommanderSpaces(BOOL fPart);
		void	AddSPSpaces(void);
		void	AddSKSpaces(void);
		void	AddGPSpaces(void);
		void	AddSTSpaces(void);
		void	AddMPSpaces(void);

		// All Pre-mapped Extra Headers
		void	AddPreHSpaces(void);

		// Spaces for Validate Tags
		void	AddValidateSpacesSP(void);
		void	AddValidateSpacesSK(void);
		void	AddValidateSpacesMP(void);

		// Helpers
		BOOL	IsCommanderP(CItem *pConfig);
		BOOL	IsSolutionMod(UINT uTable);
		void	ClearLoaded(void);
		UINT	GetID(UINT uType);

		// Friend
		friend class CEmersonEPAddrDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP TCP/IP Device Options
//

class CEmersonEPTCPDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEmersonEPTCPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Addr;
		UINT m_Socket;
		UINT m_Unit;
		UINT m_Keep;
		UINT m_Time1;
		UINT m_Time2;
		UINT m_Time3;
		UINT m_Type;
		UINT m_DispMode;
		UINT m_EmID;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
		UINT GetType(UINT uID);
		UINT GetID(UINT uType);
	};

//////////////////////////////////////////////////////////////////////////
//
// Emerson EP TCP/IP Driver
//

class CEmersonEPTCPDriver : public CEmersonEPDriver
{
	public:
		// Constructor
		CEmersonEPTCPDriver(void);

		// Destructor
		~CEmersonEPTCPDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);
	};


// End of File

#endif
