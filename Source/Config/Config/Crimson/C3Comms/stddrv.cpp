
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Standard Comms Driver
//

// Constructor

CStdCommsDriver::CStdCommsDriver(void)
{
	}

// Destructor

CStdCommsDriver::~CStdCommsDriver(void)
{
	DeleteAllSpaces();
	}

// IBase Methods

UINT CStdCommsDriver::Release(void)
{
	delete this;

	return 0;
	}

// Configuration

CLASS CStdCommsDriver::GetDeviceConfig(void)
{
	if( m_fSingle ) {

		return NULL;
		}

	return AfxRuntimeClass(CTableDeviceOptions);
	}

// Tag Import

BOOL CStdCommsDriver::MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName)
{
	return FALSE;
	}

// Address Management

BOOL CStdCommsDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CSpace *pSpace = GetSpace(Text);

	if( !pSpace ) {

		// NOTE -- This lets us accept old addresses in
		// a special text format used by the SEW driver.

		UINT uBegin = Text.Find(':');

		Text   = Text.Mid(uBegin + 1);
	
		pSpace = GetSpace(Text);
		}
	
	if( pSpace ) {

		UINT uLen = pSpace->m_Prefix.GetLength();

		return DoParseAddress(Error, Addr, pConfig, pSpace, Text.Mid(uLen));
		}

	Error.Set(IDS_DRIVER_ADDR_INVALID);

	return FALSE;
	}

BOOL CStdCommsDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem *pDrvCfg)
{
	return ParseAddress(Error, Addr, pConfig, Text);
	}

BOOL CStdCommsDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return DoExpandAddress(Text, pConfig, Addr);
	}

BOOL CStdCommsDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr, CItem * pDrvCfg)
{
	return ExpandAddress(Text, pConfig, Addr);
	}

BOOL CStdCommsDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CStdAddrDialog Dlg(ThisObject, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CStdCommsDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem * pDrvCfg)
{
	return SelectAddress(hWnd, Addr, pConfig, fPart);
	}

BOOL CStdCommsDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {
		
		if( m_List.GetCount() ) {

			if( uItem == 0 ) {

				m_n = m_List.GetHead();
				}
			else {
				if( !m_List.GetNext(m_n) ) {

					return FALSE;
					}
				}

			CSpace *pSpace = m_List[m_n];

			if( !pSpace->IsNamed() ) {
				
				Data.m_Name            = pSpace->m_Caption;
				Data.m_Addr.a.m_Type   = pSpace->m_uType;
				Data.m_Addr.a.m_Table  = pSpace->m_uTable;
				Data.m_fPart           = TRUE;
				}
			else {
				Data.m_Name            = pSpace->m_Caption;
				Data.m_Addr.a.m_Type   = pSpace->m_uType;
				Data.m_Addr.a.m_Table  = pSpace->m_uTable;
				Data.m_Addr.a.m_Offset = pSpace->m_uMinimum;
				Data.m_Addr.a.m_Extra  = 0;
				Data.m_fPart           = FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Address Helpers

BOOL CStdCommsDriver::DoParseSpace(INDEX &Index, CItem *pConfig, CString Text)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CSpace *pSpace = m_List[n];

			UINT    uMatch = pSpace->m_Prefix.GetLength();

			if( pSpace->m_Prefix == Text.Left(uMatch) ) {

				Index = n;

				return TRUE;
				}

			m_List.GetNext(n);
			}
		}
		
	return FALSE;
	}

BOOL CStdCommsDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type = StripType(pSpace, Text);

	UINT    uPos = 0;

	if( pSpace->IsNamed() ) {

		uPos = pSpace->m_uMinimum;
		}
	else {
		PTXT pError = NULL;
	
		uPos        = wcstoul(Text, &pError, pSpace->m_uRadix);

		if( pError && pError[0] ) {
			
			Error.Set(IDS_ERROR_ADDR);
			
			return FALSE;
			}
		else {	
			if( pSpace->IsOutOfRange(uPos) ) {
			
				Error.Set(IDS_ERROR_OFFSETRANGE);
				
				return FALSE;
				}

			if( CheckAlignment(pSpace) ) {

				if( pSpace->IsBadAlignment(uPos, pSpace->TypeFromModifier(Type) ) ) {
			
					Error.Set(IDS_ERROR_BADOFFSET);
				
					return FALSE;
					}
				}
			}
		}

	Addr.a.m_Type	= pSpace->TypeFromModifier(Type);

	Addr.a.m_Table	= pSpace->m_uTable;
	
	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uPos;
	
	return TRUE;
	}

BOOL CStdCommsDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( pSpace->IsNamed() ) {

			Text = pSpace->m_Prefix;
			}
		else {
			UINT uOffset = Addr.a.m_Offset;

			UINT uType   = Addr.a.m_Type;

			if( uType == pSpace->m_uType ) {

				Text.Printf( L"%s%s", 
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText(uOffset)
					     );
				}
			else {
				Text.Printf( L"%s%s.%s",
					     pSpace->m_Prefix, 
					     pSpace->GetValueAsText (uOffset),
					     pSpace->GetTypeModifier(uType)
					     );
				}
			}

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CStdCommsDriver::CheckAlignment(CSpace *pSpace)
{
	return FALSE;
	}

// Space List Access

CSpaceList & CStdCommsDriver::GetSpaceList(void)
{
	return m_List;
	}
		
CSpace * CStdCommsDriver::GetSpace(INDEX Index)
{
	return m_List[Index];
	}

CSpace * CStdCommsDriver::GetSpace(CAddress const &Addr)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CSpace *pSpace = m_List[n];

			if( pSpace->MatchSpace(Addr) ) {

				return pSpace;
				}

			m_List.GetNext(n);
			}
		}

	return NULL;
	}

CSpace * CStdCommsDriver::GetSpace(CString Text)
{
	CSpace *pSave = NULL;

	UINT    uLen  = 0;

	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			CSpace *pSpace = m_List[n];

			UINT uMatch = pSpace->m_Prefix.GetLength();

			if( pSpace->m_Prefix == Text.Left(uMatch) ) {
				
				if( uMatch > uLen) {

					pSave = pSpace;

					uLen = uMatch;
					}
				}
			
			m_List.GetNext(n);
			}
		}
		
	return pSave;
	}

// Space List Support

void CStdCommsDriver::AddSpace(CSpace *pSpace)
{
	m_List.Append(pSpace);
	}

void CStdCommsDriver::DeleteAllSpaces(void)
{
	if( m_List.GetCount() ) {

		INDEX n = m_List.GetHead();

		while( !m_List.Failed(n) ) {

			delete m_List[n];

			m_List.GetNext(n);
			}

		m_List.Empty();
		}
	}

// Implementation

CString CStdCommsDriver::StripType(CSpace *pSpace, CString &Text)
{
	CString Type;

	UINT    uPos;

	if( (uPos = Text.Find('.')) == NOTHING ) {

		Type = pSpace->GetTypeModifier(pSpace->m_uType);
		}
	else {
		Type = Text.Mid(uPos + 1);

		Text = Text.Left(uPos);
		}

	return Type;
	}

// End of File
