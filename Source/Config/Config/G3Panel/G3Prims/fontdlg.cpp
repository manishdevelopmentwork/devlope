
#include "intern.hpp"

#include "fontdlg.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Font Management Dialog
//

// REV3 -- Switch from dialog to dynamically sized window.

// Runtime Class

AfxImplementRuntimeClass(CFontManagerDialog, CStdToolbarDialog);

// Constructor

CFontManagerDialog::CFontManagerDialog(CFontManager *pFonts)
{
	m_pFonts  = pFonts;

	m_pSystem = (CUISystem *) m_pFonts->GetDatabase()->GetSystemItem();

	m_pList   = m_pFonts->m_pFonts;

	m_fRead   = m_pFonts->GetDatabase()->IsReadOnly();

	m_pView   = New CListView;

	m_fSystem = FALSE;

	m_uSort   = 0;

	m_fFlip   = FALSE;

	m_fWarned = FALSE;

	SetName(L"FontManagerDialog");
	}

// Destructor

CFontManagerDialog::~CFontManagerDialog(void)
{
	FreePreviewData();
	}

// Message Map

AfxMessageMap(CFontManagerDialog, CStdToolbarDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_UPDATEUI)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchGetInfoType(0xC0, OnToolGetInfo)
	AfxDispatchControlType(0xC0, OnToolControl)
	AfxDispatchCommandType(0xC0, OnToolCommand)

	AfxDispatchCommand(IDCANCEL, OnCancel)
	
	AfxDispatchNotify (200, LVN_ITEMCHANGED, OnItemChanged)
	AfxDispatchNotify (200, LVN_COLUMNCLICK, OnColumnClick)
	AfxDispatchNotify (200, NM_DBLCLK,       OnListDblClk )

	AfxMessageEnd(CFontManagerDialog)
	};

// Message Handlers

BOOL CFontManagerDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	MakeList();

	FindPreviewData();

	LoadList();

	ShowList();

	return FALSE;
	}

void CFontManagerDialog::OnUpdateUI(void)
{
	m_pTB->AddGadget(New CButtonGadget(0xC001, 0x10000028, CString(IDS_EDIT_2)));
	
	m_pTB->AddGadget(New CButtonGadget(0xC002, 0x10000008, CString(IDS_DELETE)));

	m_pTB->AddGadget(New CButtonGadget(0xC003, 0x1000002A, CString(IDS_REPLACE)));

	m_pTB->AddGadget(New CRidgeGadget);
	
	m_pTB->AddGadget(New CButtonGadget(0xC004, 0x1000002B, CString(IDS_SHOW_SYSTEM)));

	m_pTB->AddGadget(New CRidgeGadget);
	
	m_pTB->AddGadget(New CButtonGadget(0xC005, 0x10000029, CString(IDS_PURGE_UNUSED)));

	m_pTB->AddGadget(New CRidgeGadget);
	}

void CFontManagerDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_pWin ) {

		CPoint Pos;

		DC.BitBlt( m_Preview,
			   CDC::FromHandle(m_pWin->GetDC()),
			   Pos,
			   SRCCOPY
			   );
		}
	}

// Command Handlers

BOOL CFontManagerDialog::OnToolGetInfo(UINT uID, CCmdInfo &Info)
{
	return TRUE;
	}

BOOL CFontManagerDialog::OnToolControl(UINT uID, CCmdSource &Src)
{
	if( uID == 0xC004 ) {

		Src.EnableItem(TRUE);

		Src.CheckItem (m_fSystem);

		return TRUE;
		}

	if( uID == 0xC005 ) {

		Src.EnableItem(m_uUsed < m_uCount);

		return TRUE;
		}

	if( !m_fRead && m_uItem < NOTHING ) {

		UINT uFont   = GetFont(m_uItem);

		BOOL fSystem = m_pFonts->IsSystem(uFont);

		BOOL fUsed   = m_pFonts->IsUsed  (uFont);

		switch( uID ) {

			case 0xC001:

				Src.EnableItem(!fSystem);

				break;

			case 0xC002:

				Src.EnableItem(!fSystem && !fUsed);

				break;

			case 0xC003:

				Src.EnableItem(fUsed);

				break;
			}

		return TRUE;
		}

	Src.EnableItem(FALSE);

	return TRUE;
	}

BOOL CFontManagerDialog::OnToolCommand(UINT uID)
{
	if( !m_fRead ) {

		switch( uID ) {

			case 0xC001:

				OnEdit();

				break;

			case 0xC002:

				OnDelete();

				break;

			case 0xC003:

				OnReplace();

				break;

			case 0xC004:

				m_fSystem = !m_fSystem;

				LoadList();

				break;

			case 0xC005:

				OnPurge();

				break;
			}

		return TRUE;
		}

	return TRUE;
	}

BOOL CFontManagerDialog::OnCancel(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CFontManagerDialog::OnEdit(void)
{
	if( Warn() ) {

		UINT       uFont = GetFont(m_uItem);

		CFontItem *pFont = m_pFonts->GetFont(uFont);

		if( pFont->Edit(ThisObject) ) {

			CString Opts = m_pFonts->GetOptions(uFont);

			m_pView->SetItem(CListViewItem(m_uItem, 5, Opts, 0));

			ShowPreview();
			}
		}

	return TRUE;
	}

BOOL CFontManagerDialog::OnDelete(void)
{
	if( Warn() ) {

		CFontItem *pFont = m_pFonts->GetFont(GetFont(m_uItem));

		m_pList->DeleteItem(pFont);
		
		m_pView->DeleteItem(m_uItem);

		if( --m_uCount ) {

			if( m_uItem == m_uCount ) {

				m_uItem--;
				}

			DWORD dwState = LVIS_FOCUSED | LVIS_SELECTED;

			m_pView->SetItemState(m_uItem, dwState, dwState);
			}
		else {
			m_uItem = NOTHING;

			ShowPreview();
			}

		DoEnables();
		}

	return TRUE;
	}

BOOL CFontManagerDialog::OnReplace(void)
{
	CFontSelectItem *pItem = New CFontSelectItem;

	pItem->SetParent(m_pFonts);

	pItem->Init();

	UINT    uFont = GetFont(m_uItem);

	CString Title = m_pFonts->GetName(uFont);

	pItem->m_Font = uFont;

	CItemDialog Dialog(pItem, Title);

	if( Dialog.Execute(ThisObject) ) {

		if( pItem->m_Font != uFont ) {

			if( Warn() ) {

				UINT uOld = uFont | refFont;

				UINT uNew = pItem->m_Font;

				CUIManager *pUI = (CUIManager *) m_pFonts->GetParent();

				pUI->m_pPages->EditRef(uOld, uNew);

				pUI->SetDirty();

				m_pFonts->ClearUsed(uFont);

				if( m_pList->GetItemCount() > m_uCount ) {

					LoadList();
					}
				else {
					UpdateUsed();

					DoEnables();
					}
				}
			}
		}

	pItem->Kill();

	delete pItem;

	return TRUE;
	}

BOOL CFontManagerDialog::OnPurge(void)
{
	if( Warn() ) {

		m_pFonts->PurgeUnused();

		LoadList();
		}

	return TRUE;
	}

// Notification Handlers

void CFontManagerDialog::OnItemChanged(UINT uID, NMLISTVIEW &Info)
{
	if( Info.uNewState & LVIS_SELECTED ) {

		m_uItem = Info.iItem;

		ShowPreview();

		DoEnables();
		}
	}

void CFontManagerDialog::OnColumnClick(UINT uID, NMLISTVIEW &Info)
{
	if( Info.iSubItem < 5 ) {

		if( m_uSort == UINT(Info.iSubItem) ) {

			m_fFlip = !m_fFlip;
			}
		else {
			m_uSort = Info.iSubItem;

			m_fFlip = FALSE;
			}

		m_pView->SortItems(FARPROC(SortStatic), LPARAM(this));
		}
	}

void CFontManagerDialog::OnListDblClk(UINT uID, NMHDR &Info)
{
	CPoint Pos = GetCursorPos();

	m_pView->ScreenToClient(Pos);

	if( m_pView->HitTestItem(TRUE, Pos) < NOTHING ) {

		if( !m_pFonts->IsSystem(GetFont(m_uItem)) ) {

			OnEdit();
			}
		}
	}

// Sorting

int CFontManagerDialog::SortList(LPARAM p1, LPARAM p2)
{
	switch( m_uSort ) {

		case 0:
			return SortData(p1, p2, "FSWUT");

		case 1:
			return SortData(p1, p2, "SFWUT");

		case 2:
			return SortData(p1, p2, "WFSUT");

		case 3:
			return SortData(p1, p2, "TFSWU");

		case 4:
			return SortData(p1, p2, "UFSWT");
		}

	return 0;
	}

int CFontManagerDialog::SortData(UINT f1, UINT f2, PCSTR pList)
{
	while( *pList ) {

		int n = SortData(f1, f2, *pList);

		if( n ) {

			return m_fFlip ? -n : +n;
			}

		pList++;
		}

	return 0;
	}

int CFontManagerDialog::SortData(UINT f1, UINT f2, char cKey)
{
	if( cKey == 'F' ) {

		return wstrcmp( m_pFonts->GetFace(f1), 
				m_pFonts->GetFace(f2)
				);
		}

	if( cKey == 'S' ) {

		return intcmp( m_pFonts->GetHeight(f1),
			       m_pFonts->GetHeight(f2)
			       );
		}

	if( cKey == 'W' ) {

		return intcmp( m_pFonts->IsBold(f1),
			       m_pFonts->IsBold(f2)
			       );
		}

	if( cKey == 'T' ) {

		return intcmp( m_pFonts->IsSystem(f1),
			       m_pFonts->IsSystem(f2)
			       );
		}

	if( cKey == 'U' ) {

		return intcmp( m_pFonts->IsUsed(f1),
			       m_pFonts->IsUsed(f2)
			       );
		}

	return 0;
	}

// Sort Function

int CFontManagerDialog::SortStatic(LPARAM p1, LPARAM p2, LPARAM ps)
{
	return ((CFontManagerDialog *) ps)->SortList(p1, p2);
	}

// Implementation

void CFontManagerDialog::MakeList(void)
{
	CWnd  &Wnd = GetDlgItem(100);

	CRect Rect = Wnd.GetWindowRect();

	ScreenToClient(Rect);

	DWORD dwStyle = LVS_REPORT        |
			LVS_SHOWSELALWAYS |
			LVS_SINGLESEL     |
			WS_BORDER         |
			WS_TABSTOP        ;

	m_pView->Create( dwStyle,
			 Rect,
			 ThisObject,
			 200
			 );

	DWORD dwExStyle = LVS_EX_FULLROWSELECT;

	m_pView->SetExtendedListViewStyle( dwExStyle,
					   dwExStyle
					   );

	int cx = Rect.cx() / 16;

	int c0 = 4 * cx;

	int c1 = 2 * cx;
	
	int c2 = 2 * cx;
	
	int c3 = 2 * cx;

	int c4 = 2 * cx;

	int c5 = Rect.cx() - c0 - c1 - c2 - c3 - c4 - 20;

	CListViewColumn Col0(0, LVCFMT_LEFT, c0, CString(IDS_FACE));

	CListViewColumn Col1(1, LVCFMT_LEFT, c1, CString(IDS_SIZE));
	
	CListViewColumn Col2(2, LVCFMT_LEFT, c2, CString(IDS_WEIGHT));

	CListViewColumn Col3(3, LVCFMT_LEFT, c3, CString(IDS_TYPE));
	
	CListViewColumn Col4(4, LVCFMT_LEFT, c4, CString(IDS_USED));
	
	CListViewColumn Col5(5, LVCFMT_LEFT, c5, CString(IDS_OPTIONS));

	m_pView->InsertColumn(0, Col0);
	
	m_pView->InsertColumn(1, Col1);
	
	m_pView->InsertColumn(2, Col2);
	
	m_pView->InsertColumn(3, Col3);

	m_pView->InsertColumn(4, Col4);
	
	m_pView->InsertColumn(5, Col5);

	Wnd.DestroyWindow(TRUE);
	}

void CFontManagerDialog::LoadList(void)
{
	m_pFonts->UpdateUsed();

	m_pView->DeleteAllItems();

	m_uCount = 0;

	m_uUsed  = 0;

	CArray <UINT> List;

	EnumFonts(List);

	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CString Face, Size, Bold, Type, Used, Opts;

		UINT    uFont = List[n];

		Face  = m_pFonts->GetFace(uFont);

		Size  = CPrintf(L"%u", m_pFonts->GetHeight(uFont));

		Bold = m_pFonts->IsBold(uFont) ? CString(IDS_BOLD) : CString(IDS_REGULAR);

		Used = m_pFonts->IsUsed(uFont) ? CString(IDS_YES)  : CString(IDS_NO);

		Opts = m_pFonts->GetOptions(uFont);

		if( m_pFonts->IsCustom(uFont) ) {

			if( m_pFonts->IsUsed(uFont) ) {

				m_uUsed++;
				}

			Type = CString(IDS_CUSTOM);
			}
		else {
			Type = CString(IDS_SYSTEM);

			m_uUsed++;
			}

		int i = m_pView->InsertItem(CListViewItem(n, 0, Face, 0, LPARAM(uFont)));
		
		m_pView->SetItem(CListViewItem(i, 1, Size, 0));
		
		m_pView->SetItem(CListViewItem(i, 2, Bold, 0));

		m_pView->SetItem(CListViewItem(i, 3, Type, 0));
		
		m_pView->SetItem(CListViewItem(i, 4, Used, 0));
		
		m_pView->SetItem(CListViewItem(i, 5, Opts, 0));

		m_uCount++;
		}

	m_pView->SortItems(FARPROC(SortStatic), LPARAM(this));

	if( m_uCount ) {

		DWORD dwState = LVIS_FOCUSED | LVIS_SELECTED;

		m_pView->SetItemState(0, dwState, dwState);

		m_uItem = 0;
		}
	else
		m_uItem = NOTHING;

	ShowPreview();

	DoEnables();
	}

void CFontManagerDialog::ShowList(void)
{
	m_pView->ShowWindow(SW_SHOW);

	m_pView->SetFocus();
	}

void CFontManagerDialog::UpdateUsed(void)
{
	m_pFonts->UpdateUsed();

	m_uUsed = 0;

	UINT c  = m_pView->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CListViewItem Item(LVIF_PARAM);

		m_pView->GetItem(n, 0, Item);

		if( m_pFonts->IsUsed(Item.lParam) ) {

			m_pView->SetItem(CListViewItem(n, 4, CString(IDS_YES), 0));

			m_uUsed++;
			}
		else {
			m_pView->SetItem(CListViewItem(n, 4, CString(IDS_NO),  0));

			if( !m_pFonts->IsSystem(Item.lParam) ) {

				m_uUsed++;
				}
			}
		}
	}

void CFontManagerDialog::DoEnables(void)
{
	SendMessage(WM_GOINGIDLE);
	}

BOOL CFontManagerDialog::Warn(void)
{
	// REV3 -- Can we make these things undoable?

	if( !m_fWarned ) {

		CSystemWnd &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

		if( System.KillUndoList() ) {

			m_fWarned = TRUE;

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

void CFontManagerDialog::FindPreviewData(void)
{
	CWnd  &Wnd = GetDlgItem(101);

	CRect Rect = Wnd.GetWindowRect() - 8;

	ScreenToClient(Rect);

	CClientDC DC(ThisObject);

	CSize Size = Rect.GetSize();

	m_Preview  = Rect;

	m_pWin     = Create_GdiWindowsA888(Size.cx, Size.cy);

	m_pGDI     = m_pWin->GetGdi();

	m_pGDI->SetBrushFore(GetRGB(0,0,0));

	m_pGDI->FillRect(0, 0, Size.cx, Size.cy);
	}

void CFontManagerDialog::ShowPreview(void)
{
	CSize Size = m_Preview.GetSize();

	m_pGDI->SetBrushFore(GetRGB(0,0,0));

	m_pGDI->FillRect(0, 0, Size.cx, Size.cy);

	if( m_uItem < NOTHING ) {

		UINT uFont = GetFont(m_uItem);

		m_pGDI->SetTextFore (GetRGB(31,31,31));

		m_pGDI->SetTextTrans(modeTransparent);

		m_pFonts->Select(m_pGDI, uFont);

		CStringArray Text;

		if( m_pFonts->IsNumeric(uFont) ) {

			Text.Append(L"0123456789");
			}
		else {
			CString s = L"THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG";

			Text.Append(s.ToUpper());

			Text.Append(s.ToLower());

			// LATER -- Add other samples in different languages?

			if( m_pFonts->HasGlyphs(uFont, glyphPinYin) ) {

				Text.Append(L"\x4E16\x754C\x60A8\x597D "
					    L"\x4E16\x754C\x60A8\x597D "
					    L"\x4E16\x754C\x60A8\x597D"
					    );
				}
			else
				Text.Append(L"0123456789");
			}

		if( !Text.IsEmpty() ) {

			int cn = Text.GetCount();

			int cy = m_pGDI->GetTextHeight(L"X");

			int yp = (Size.cy - cn * cy - 4 * (cn - 1)) / 2;

			for( int n = 0; n < cn; n++  ) {

				int cx = m_pGDI->GetTextWidth(Text[n]);

				int xp = (Size.cx - cx) / 2;

				m_pGDI->TextOut(xp, yp, Text[n]);

				yp += cy;

				yp += 4;
				}
			}

		m_pGDI->SelectFont(fontHei16);
		}

	Invalidate(m_Preview, FALSE);
	}

void CFontManagerDialog::FreePreviewData(void)
{
	if( m_pGDI ) {

		m_pGDI->Release();

		m_pGDI = NULL;
		}
	}

void CFontManagerDialog::EnumFonts(CArray <UINT> &List)
{
	if( m_fSystem ) {

		m_pFonts->EnumSystem(List);
		}

	m_pFonts->EnumCustom(List);
	}

UINT CFontManagerDialog::GetFont(UINT uItem)
{
	CListViewItem Item(LVIF_PARAM);

	m_pView->GetItem(uItem, 0, Item);

	return Item.lParam;
	}

//////////////////////////////////////////////////////////////////////////
//
// Font Selection Item
//

// Runtime Class

AfxImplementRuntimeClass(CFontSelectItem, CUIItem);

// Constructor

CFontSelectItem::CFontSelectItem(void)
{
	}

// Meta Data

void CFontSelectItem::AddMetaData(void)
{
	Meta_AddInteger(Font);
	}

// End of File
