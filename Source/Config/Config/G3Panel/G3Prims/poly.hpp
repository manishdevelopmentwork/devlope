
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_POLY_HPP
	
#define	INCLUDE_POLY_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPoly;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline
//

class CPrimPoly : public CPrimGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPoly(void);
	
		// Destructor
		~CPrimPoly(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		BOOL HitTest(P2 Pos);
		void FindTextRect(void);
		void Draw(IGDI *pGDI, UINT uMode);
		void UpdateLayout(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	     m_Orient;
		CCodedItem * m_pSpinPos;
		CCodedItem * m_pSpinMin;
		CCodedItem * m_pSpinMax;

	protected:
		// Data Members
		UINT  m_uCount;
		DWORD m_dwRound;
		P2 *  m_pList;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Implementation
		void MakePoints(void);
		void FreePoints(void);
		void AdjustTextRect(void);

		// Generation
		virtual void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPolygon;
class CPrimPolygon3;
class CPrimPolygon5;
class CPrimPolygon6;
class CPrimPolygon8;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Polygon or Star
//

class CPrimPolygon : public CPrimPoly
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolygon(void);

		// Data Members
		INT m_Rotate;
		INT m_AltRad;
	
	protected:
		// Data Members
		int m_nSides;

		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Triangle
//

class CPrimPolygon3 : public CPrimPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolygon3(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Pentagon
//

class CPrimPolygon5 : public CPrimPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolygon5(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Hexagon
//

class CPrimPolygon6 : public CPrimPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolygon6(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Octagon
//

class CPrimPolygon8 : public CPrimPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolygon8(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPolyStar;
class CPrimPolyStar3;
class CPrimPolyStar4;
class CPrimPolyStar5;
class CPrimPolyStar6;
class CPrimPolyStar8;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Polygonal Star
//

class CPrimPolyStar : public CPrimPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyStar(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Three Pointed Star
//

class CPrimPolyStar3 : public CPrimPolyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyStar3(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Four Pointed Star
//

class CPrimPolyStar4 : public CPrimPolyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyStar4(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Five Pointed Star
//

class CPrimPolyStar5 : public CPrimPolyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyStar5(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Six Pointed Star
//

class CPrimPolyStar6 : public CPrimPolyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyStar6(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Eight Pointed Star
//

class CPrimPolyStar8 : public CPrimPolyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyStar8(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPolyArrow;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Arrow
//

class CPrimPolyArrow : public CPrimPoly
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyArrow(void);
	
	protected:
		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPolyTrimRect;
class CPrimPolyRoundRect;
class CPrimPolyPlaque;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Trimmed Rectangle
//

class CPrimPolyTrimRect : public CPrimPoly
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyTrimRect(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);
	
	protected:
		// Data Members
		CSize m_Corner;

		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Rounded Rectangle
//

class CPrimPolyRoundRect : public CPrimPolyTrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyRoundRect(void);

		// Overridables
		void FindTextRect(void);

	protected:
		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Plaque
//

class CPrimPolyPlaque : public CPrimPolyTrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyPlaque(void);
	
		// Overridables
		void FindTextRect(void);

	protected:
		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPolySemiTrimRect;
class CPrimPolySemiRoundRect;
class CPrimPolySemiPlaque;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Semi Trimmed Rectangle
//

class CPrimPolySemiTrimRect : public CPrimPoly
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolySemiTrimRect(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);
	
	protected:
		// Data Members
		CSize m_Corner;

		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Semi Rounded Rectangle
//

class CPrimPolySemiRoundRect : public CPrimPolySemiTrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolySemiRoundRect(void);

		// Overridables
		void FindTextRect(void);
	
	protected:
		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Semi Plaque
//

class CPrimPolySemiPlaque : public CPrimPolySemiTrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolySemiPlaque(void);
	
		// Overridables
		void FindTextRect(void);

	protected:
		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimPolyBalloon;
class CPrimPolyConicalTank;
class CPrimPolyRoundTank;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Balloon
//

class CPrimPolyBalloon : public CPrimPoly
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyBalloon(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);
	
	protected:
		// Data Members
		CSize m_Corner;
		INT   m_Base;
		INT   m_Point;

		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Round Tank
//

class CPrimPolyRoundTank : public CPrimPoly
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyRoundTank(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);

	protected:
		// Data Members
		INT m_Base;

		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Filled Polyline -- Conical Tank
//

class CPrimPolyConicalTank : public CPrimPoly
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPolyConicalTank(void);

		// Overridables
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);
		void SetHand(BOOL fInit);

	protected:
		// Data Members
		INT m_Base;
		INT m_Flat;

		// Generation
		void MakePoints(CSize Max);

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
