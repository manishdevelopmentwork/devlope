
#include "intern.hpp"

#include "PrimRubyBevel.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Bevel Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyBevel, CPrimRubyBevelBase);

// Constructor

CPrimRubyBevel::CPrimRubyBevel(void)
{
	m_uType = 0x84;
	}

// Meta Data

void CPrimRubyBevel::AddMetaData(void)
{
	CPrimRubyBevelBase::AddMetaData();

	Meta_SetName((IDS_BEVEL));
	}

// End of File
