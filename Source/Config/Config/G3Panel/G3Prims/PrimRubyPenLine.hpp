
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPenLine_HPP
	
#define	INCLUDE_PrimRubyPenLine_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPenBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Line Pen
//

class DLLAPI CPrimRubyPenLine : public CPrimRubyPenBase
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPenLine(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Stroking
		BOOL Stroke(CRubyPath &output, CRubyPath const &figure);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT m_End1;
		UINT m_End2;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
