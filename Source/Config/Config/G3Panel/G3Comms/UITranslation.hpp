
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITranslation_HPP

#define INCLUDE_UITranslation_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsSystem;
class CLangManager;
class CTransEditCtrl;
class CTransFlagCtrl;

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element -- Translation Editor
//

class CUITranslation : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITranslation(void);

	protected:
		// Data Members
		CCommsSystem   * m_pSystem;
		CLangManager   * m_pLang;
		UINT		 m_uSlot;
		UINT		 m_uFlag;
		CString		 m_Label;
		DWORD		 m_dwStyle;
		CLayItemText   * m_pSlotLayout;
		CLayItemSize   * m_pFlagLayout;
		CLayItemText   * m_pNameLayout;
		CLayFormRow    * m_pTextLayout;
		CLayItemText   * m_pDataLayout;
		CStatic	       * m_pSlotCtrl;
		CTransFlagCtrl * m_pFlagCtrl;
		CStatic	       * m_pNameCtrl;
		CTransEditCtrl * m_pDataCtrl;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnPosition(void);
		BOOL OnFindFocus(CWnd * &pWnd);

		// Data Overridables
		void OnLoad(void);
		UINT OnSave(BOOL fUI);

		// Nofication Overridbles
		BOOL OnNotify(UINT uID, NMHDR &Info);

		// Implementation
		CRect   GetDataCtrlRect(void);
		CString GetDataCtrlText(void);
	};

// End of File

#endif
