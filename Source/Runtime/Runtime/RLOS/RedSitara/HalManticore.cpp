
#include "Intern.hpp"

#include "HalManticore.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Manticore
//

// Instantiator

bool Create_HalManticore(void)
{
	(New CHalManticore)->Open();

	return true;
	}

// Constructor

CHalManticore::CHalManticore(void)
{
	m_uDebugAddr = ADDR_UART4;

	m_uDebugLine = INT_UART4;
	}

// End of File
