
#include "Intern.hpp"

#include "Pwm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AM437 Enhanced PWM Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CPwm437::CPwm437(UINT iIndex, CClock437 *pClock)
{
	m_pBase   = PVWORD(FindBase(iIndex));

	m_uFreq   = 0;

	m_uPeriod = 0;

	m_uClock  = pClock->GetCoreM4Freq();

	Init();
	}

// Operations

UINT CPwm437::GetFreq(void)
{
	return m_uFreq;
	}

void CPwm437::SetFreq(UINT uFreq)
{
	UINT uDiv = 0;

	m_uPeriod = uFreq ? m_uClock / uFreq : 1;

	m_uFreq   = uFreq;

	while( m_uPeriod & 0xFFFF0000 ) {

		m_uPeriod >>= 1;

		uDiv ++;
		}

	Reg(TBCNT)  = 0x0000;

	Reg(TBCTL)  = (uDiv << 10);

	Reg(TBPRD)  = 0x0000;

	Reg(CMPCTL) = 0x0050;

	Reg(TBPRD)  = m_uPeriod - 1;
	}

void CPwm437::SetDuty(UINT uChan, UINT uDuty)
{
	UINT uLoad = (m_uPeriod * uDuty) / 100;

	if( uChan == pwmChanA ) {

		Reg(CMPA)   = uLoad;

		Reg(AQCTLA) = 0x0012;
		}
	else {
		Reg(CMPB)   = uLoad;

		Reg(AQCTLB) = 0x0102;
		}
	}

// Implementation 

void CPwm437::Init(void)
{
	Reg(TBCTL)  = 0x0003;

	Reg(TBCNT)  = 0x0000;

	Reg(TBPRD)  = 0x0000;

	Reg(CMPCTL) = 0x0000;

	Reg(AQCTLA) = 0x0000;

	Reg(AQCTLB) = 0x0000;
	}

DWORD CPwm437::FindBase(UINT iIndex) const
{
	switch( iIndex ) {

		case 0 : return ADDR_PWMSS0_EPWM;
		case 1 : return ADDR_PWMSS1_EPWM;
		case 2 : return ADDR_PWMSS2_EPWM;
		case 3 : return ADDR_PWMSS3_EPWM;
		case 4 : return ADDR_PWMSS4_EPWM;
		case 5 : return ADDR_PWMSS5_EPWM;
		}

	return 0;
	}

// End of File
