
#include "intern.hpp"

#include "barcode.hpp"

#include <g3barcode.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Barcode
//

// Constructor

CPrimBarcode::CPrimBarcode(void)
{
	m_pValue      = NULL;

	m_pColor1     = New CPrimColor(GetRGB(31,31,31));

	m_pColor2     = New CPrimColor(GetRGB(0,0,0));

	m_Ctx.m_pData = NULL;
	}

// Destructor

CPrimBarcode::~CPrimBarcode(void)
{
	delete m_pValue;

	delete m_pColor1;
	
	delete m_pColor2;

	delete m_Ctx.m_pData;
	}

// Initialization

void CPrimBarcode::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimBarcode", pData);

	CPrim::Load(pData);

	GetCoded(pData, m_pValue);

	m_Code     = GetWord(pData);
	m_Border   = GetWord(pData);

	m_pColor1->Load(pData);
	m_pColor2->Load(pData);

	m_NoShrink = GetByte(pData);
	m_NoGrow   = GetByte(pData);
	m_NoNonInt = GetByte(pData);
	m_AlignH   = GetByte(pData);
	m_AlignV   = GetByte(pData);
	}

// Overridables

void CPrimBarcode::SetScan(UINT Code)
{
	SetItemScan(m_pValue, Code);

	CPrim::SetScan(Code);
	}

void CPrimBarcode::DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans)
{
	CPrim::DrawPrep(pGDI, Erase, Trans);

	if( m_fShow ) {

		CCtx Ctx;

		FindCtx(Ctx);

		if( !(m_Ctx == Ctx) ) {

			Ctx.m_pData = NULL;
			
			Ctx.m_xSize = 0;

			Ctx.m_ySize = 0;

			Ctx.m_nStep = 0;

			if( !Ctx.m_Value.IsEmpty() ) {

				int cx = m_DrawRect.x2 - m_DrawRect.x1 - 2 * m_Border;

				int cy = m_DrawRect.y2 - m_DrawRect.y1 - 2 * m_Border;

				if( cx > 0 &&  cy > 0 ) {

					Barcode_Render( Ctx.m_Value,
							m_Code,
							cx, cy,
							GetMask(),
							Ctx.m_pData,
							Ctx.m_nStep,
							Ctx.m_xSize,
							Ctx.m_ySize
							);
					}
				}

			delete m_Ctx.m_pData;

			m_Ctx     = Ctx;

			m_fChange = TRUE;
			}

		if( m_fChange ) {

			Erase.Append(m_DrawRect);
			}
		}
	}

void CPrimBarcode::DrawPrim(IGDI *pGDI)
{
	pGDI->SetBrushFore(m_Ctx.m_Paper);

	pGDI->SelectBrush (brushFore);

	pGDI->FillRect(PassRect(m_DrawRect));

	if( m_Ctx.m_pData ) {

		int cx = m_DrawRect.x2 - m_DrawRect.x1 - 2 * m_Border;

		int cy = m_DrawRect.y2 - m_DrawRect.y1 - 2 * m_Border;

		int xp = m_DrawRect.x1 + m_Border;

		int yp = m_DrawRect.y1 + m_Border;

		if( m_Ctx.m_xSize <= cx && m_Ctx.m_ySize <= cy ) {

			pGDI->ResetFont();

			pGDI->SetTextFore (m_Ctx.m_Ink);

			pGDI->SetTextBack (m_Ctx.m_Paper);

			pGDI->SetTextTrans(modeOpaque);

			xp += m_AlignH * (cx - m_Ctx.m_xSize) / 2;

			yp += m_AlignV * (cy - m_Ctx.m_ySize) / 2;

			pGDI->TextOut(xp, yp, L""); // Configure GDI

			pGDI->CharBlt(xp, yp, m_Ctx.m_xSize, m_Ctx.m_ySize, m_Ctx.m_nStep, m_Ctx.m_pData);
			}
		}

	CPrim::DrawPrim(pGDI);
	}

// Context Creation

void CPrimBarcode::FindCtx(CCtx &Ctx)
{
	Ctx.m_Value = GetItemData(m_pValue, L"12345678");

	Ctx.m_Value = Ctx.m_Value.Left(1400);

	Ctx.m_Paper = m_pColor1->GetColor();

	Ctx.m_Ink   = m_pColor2->GetColor();
	}

// Context Check

BOOL CPrimBarcode::CCtx::operator == (CCtx const &That) const
{
	return m_Value == That.m_Value &&
	       m_Paper == That.m_Paper &&
	       m_Ink   == That.m_Ink   ;
	}

// Implementation

UINT CPrimBarcode::GetMask(void)
{
	UINT uMask = 0;

	if( m_NoShrink ) uMask |= 1;
	if( m_NoGrow   ) uMask |= 2;
	if( m_NoNonInt ) uMask |= 4;

	return uMask;
	}
// End of File
