
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ItemCreateDialog_HPP

#define INCLUDE_ItemCreateDialog_HPP

//////////////////////////////////////////////////////////////////////////
//
// Item Create Dialog
//

class DLLAPI CItemCreateDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructors
		CItemCreateDialog(CNamedList *pList, UINT Type);
		CItemCreateDialog(CNamedList *pList, UINT Type, CString Name);
		CItemCreateDialog(CString Name, UINT Type, UINT Size);
		CItemCreateDialog(CString Name, UINT Type);

		// Attributes
		CString GetName(void) const;
		UINT    GetType(void) const;
		UINT    GetSize(void) const;

		// Last Type
		static UINT GetLastType(void);

	protected:
		// Static Data
		static UINT m_Last;

		// Data Members
		CNamedList * m_pList;
		UINT         m_Type;
		UINT         m_Size;
		BOOL	     m_fOne;
		CString      m_Name;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnEditChange(UINT uID, CWnd &Ctrl);
		void OnModeChange(UINT uID, CWnd &Ctrl);

		// Command Handlers
		BOOL OnCommandYes(UINT uID);
		BOOL OnCommandYesAll(UINT uID);
		BOOL OnCommandNo(UINT uID);

		// Implementation
		void LoadType(void);
		void LoadName(void);
		void LoadSize(void);
		BOOL ReadType(void);
		BOOL ReadName(void);
		BOOL ReadSize(void);
		BOOL ValidateName(CString Name);
	};

// End of File

#endif
