
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

// Runtime Class

AfxImplementRuntimeClass(CABL5kDialog, CStdDialog);

// Constructor

CABL5kDialog::CABL5kDialog(CABL5kDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart, BOOL fSelect)
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = (CABL5kDeviceOptions *) pConfig;

	m_fPart   = fPart;

	m_fSelect = fSelect;

	m_pSelect = NULL;

	m_hSelect = NULL;

	m_fImport = FALSE;

	SetName(fPart ? L"CABL5kEIPPartDlg" : L"CABL5kEIPFullDlg");
	}

// Destructor

CABL5kDialog::~CABL5kDialog(void)
{
	afxThread->SetStatusText(L"");
	}

// Attributes

BOOL CABL5kDialog::FileImported(void)
{
	return m_fImport;
	}

// Message Map

AfxMessageMap(CABL5kDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, TVN_SELCHANGED, OnTreeSelChanged)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(5001, OnImport)

	AfxMessageEnd(CABL5kDialog)
	};

// Message Handlers

BOOL CABL5kDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CAddress &Addr = (CAddress &) *m_pAddr;
	
	EnableImport(!m_fSelect);

	if( !m_fPart ) {

		SetCaption();

		afxThread->SetWaitMode(TRUE);
		
		LoadTags();

		afxThread->SetWaitMode(FALSE);

		if( Addr.m_Ref ) {

			if( IsTable(Addr) ) {

				CString Name;

				if( m_pConfig->DoExpandAddress(Name, Addr) ) {

					CABL5kItem *pTag = m_pConfig->FindFromMap(Name);

					if( pTag ) {

						CString Type = pTag->m_Type;

						CArray <UINT> Dims;

						if( m_pConfig->ParseDims(Dims, Type) ) {

							UINT uOffset = GetOffset(Addr);

							ShowTableIndices(uOffset, Dims);

							SetDlgFocus(2004);
							
							return FALSE;
							}
						}
					}
				}

			return FALSE;
			}

		EnableCancel(m_fSelect);

		GetDlgItem(IDOK).SetWindowText(m_fSelect ? "OK" : "Close");
		
		return TRUE;
		}
	else {
		CString Name;

		if( m_pConfig->DoExpandAddress(Name, Addr) ) {

			SetCaption();				
			
			SetAddressText(Name);

			CABL5kItem *pTag = m_pConfig->FindFromMap(Name);

			if( pTag ) {

				CString Type = pTag->m_Type;

				CString Name = pTag->m_Name;

				CArray <UINT> Dims;

				if( m_pConfig->ParseDims(Dims, Type) ) {
					
					UINT uCount = Dims.GetCount();

					UINT uMin   = 0;

					UINT uMax;

					UINT n;

					for( n = 0, uMax = 1; n < uCount; uMax *= Dims[n++] );

					CString Min = Name;

					CString Max = Name;

					m_pConfig->ExpandDims(Min, Dims, uMin);

					m_pConfig->ExpandDims(Max, Dims, uMax - 1);

					SetMinimumText(Min);
					
					SetMaximumText(Max);

					SetDataTypeText(Type);

					UINT uOffset = Addr.a.m_Extra ? uMin : GetOffset(Addr);

					ShowTableIndices(uOffset, Dims);

					SetDlgFocus(2004);
					
					return FALSE;
					}
				}

			SetDlgFocus(2002);
			}

		return FALSE;
		}
	}

// Notification Handlers

void CABL5kDialog::OnTreeSelChanged(UINT uID, NMTREEVIEW &Info)
{
	m_pSelect = (CABL5kItem *) Info.itemNew.lParam;

	m_hSelect = Info.itemNew.hItem;

	if( m_pSelect ) {

		CString Name = m_pSelect->m_Name;

		CString Type = m_pSelect->m_Type;

		CString Info = m_pSelect->m_Info;
		
		if( m_pSelect->IsStruct() ) {

			SetAddressText ("");

			SetAddrTypeText("");
	
			SetMinimumText ("");
			
			SetMaximumText ("");

			SetDataTypeText(Type);
			
			SetDataDescText(Info);

			HideTableIndices();

			return;
			}

		CArray <UINT> Dims;

		if( m_pConfig->ParseDims(Dims, Type) ) {
			
			UINT uCount = Dims.GetCount();

			UINT uMin   = 0;

			UINT uMax;

			UINT n;

			for( n = 0, uMax = 1; n < uCount; uMax *= Dims[n++] );

			CString Min = Name;

			CString Max = Name;

			m_pConfig->ExpandDims(Min, Dims, uMin);

			m_pConfig->ExpandDims(Max, Dims, uMax - 1);

			SetMinimumText(Min);
			
			SetMaximumText(Max);

			ShowTableIndices(uMin, Dims);
			}
		else {
			SetMinimumText("");
			
			SetMaximumText("");
			
			HideTableIndices();
			}
		
		SetAddressText(Name);

		UINT uType = m_pConfig->m_Types.GetAtomicType(Type);
		
		SetAddrTypeText(CSpace("", "", 0).GetTypeAsText(uType));

		SetDataTypeText(Type);
		
		SetDataDescText(Info);
		
		return;
		}

	SetAddressText ("");

	SetAddrTypeText("");
	
	SetMinimumText ("");
	
	SetMaximumText ("");

	SetDataTypeText("");

	SetDataDescText("");

	HideTableIndices();
	}

// Command Handlers

BOOL CABL5kDialog::OnOkay(UINT uID)
{
	if( m_fSelect ) {

		CString Text = GetAddressText();

		if( !Text.IsEmpty() ) {

			CError Error;

			CAddress Addr;
			
			if( m_pConfig->DoParseAddress(Error, Addr, Text) ) {
				
				*m_pAddr = Addr;

				afxThread->SetWaitMode(TRUE);
				
				EndDialog(TRUE);

				afxThread->SetWaitMode(FALSE);

				return TRUE;
				}

			Error.Show(ThisObject);

			CABL5kItem *pTag;

			if( (pTag = m_pConfig->FindFromMap(Text)) ) {

				CArray <UINT> Dims;

				CArray <UINT> List;
				
				if( m_pConfig->ParseDims(Dims, pTag->m_Type) && m_pConfig->ParseDims(List, Text) ) {

					UINT uDim = m_pConfig->CheckDims(List, Dims);

					if( uDim < NOTHING ) {

						CArray <UINT> Visible;

						GetVisibleIndices(Visible);

						SetDlgFocus(Visible[Visible.GetCount() - 1 - uDim]);
						}
					}
				}

			return TRUE;		
			}

		m_pAddr->m_Ref = 0;
		}

	afxThread->SetWaitMode(TRUE);

	EndDialog(TRUE);

	afxThread->SetWaitMode(FALSE);

	return TRUE;		
	}

BOOL CABL5kDialog::OnImport(UINT uID)
{
	COpenFileDialog Dlg;

	Dlg.LoadLastPath(L"AB Tags");

	Dlg.SetCaption(CPrintf(IDS_AB_IMPORT_TAGS, m_pConfig->m_DeviceName));

	Dlg.SetFilter(L"RSLogix 5000 Import/Export Files (*.l5k)|*.l5k");

	if( Dlg.Execute(ThisObject) ) {
		
		CFilename Filename = Dlg.GetFilename();
		
		afxThread->SetWaitMode(TRUE);
		
		if( m_pConfig->LoadFromFile(Filename) ) {
			
			LoadTags();

			m_fImport = TRUE;
			
			Dlg.SaveLastPath(L"AB Tags");
			}

		afxThread->SetWaitMode(FALSE);
		}

	return TRUE;
	}

// Implementation

void CABL5kDialog::SetCaption(void)
{
	CString Text;

	if( m_fSelect ) {

		Text.Printf( IDS_AB_SELECT_TAG, 
			     m_pDriver->GetString(stringShortName)
			     );
		}
	else {
		Text.Printf( IDS_AB_VIEW_TAG, 
			     m_pDriver->GetString(stringShortName)
			     );
		}

	SetWindowText(Text);
	}

void CABL5kDialog::LoadTags(void)
{
	CTreeView &Tree = (CTreeView &) GetDlgItem(1001);

	DWORD dwStyle = TVS_HASBUTTONS
		      | TVS_NOTOOLTIPS
		      | TVS_SHOWSELALWAYS
		      | TVS_DISABLEDRAGDROP
		      | TVS_HASLINES
		      | TVS_LINESATROOT;

	Tree.SetWindowStyle(dwStyle, dwStyle);

	Tree.DeleteItem(TVI_ROOT);

	m_fLoading = TRUE;
	
	LoadRoot(Tree);

	LoadTags(Tree, L"Controller Tags", m_pConfig->m_pController->m_pTags);

	LoadTags(Tree, L"Program Tags",    m_pConfig->m_pPrograms);

	Tree.Expand(m_hRoot, TVE_EXPAND);

	Tree.EnsureVisible(m_hSelect);

	m_fLoading = FALSE;

	Tree.SetFocus();
	}

void CABL5kDialog::LoadRoot(CTreeView &Tree)
{
	CTreeViewItem Node;

	Node.SetText(m_pConfig->m_DeviceName);

	Node.SetParam(NULL);

	m_hRoot = Tree.InsertItem(NULL, NULL, Node);

	Tree.SelectItem(m_hRoot, TVGN_CARET);
	}

void CABL5kDialog::LoadTags(CTreeView &Tree, CString Root, CABL5kNamesList *pPrograms)
{
	CTreeViewItem Node;

	Node.SetText(Root);

	Node.SetParam(NULL);

	HTREEITEM hRoot = Tree.InsertItem(m_hRoot, NULL, Node);

	for( UINT n = 0; n < pPrograms->GetItemCount(); n++ ) {

		CABL5kNames *pProgram = pPrograms->GetItem(n);

		if( pProgram->m_pTags->GetItemCount() ) {

			CString Scope = pProgram->m_Scope;
		
			CTreeViewItem Node;

			Node.SetText(Scope);

			Node.SetParam(NULL);

			HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);
			
			LoadTags(Tree, hNode, pProgram->m_pTags);
			}
		}

	Tree.Expand(hRoot, TVE_EXPAND);
	}

void CABL5kDialog::LoadTags(CTreeView &Tree, CString Root, CABL5kItemList *pTags)
{
	CTreeViewItem Node;

	Node.SetText(Root);

	Node.SetParam(NULL);

	HTREEITEM hRoot = Tree.InsertItem(m_hRoot, NULL, Node);

	LoadTags(Tree, hRoot, pTags);

	Tree.Expand(hRoot, TVE_EXPAND);
	}

void CABL5kDialog::LoadTags(CTreeView &Tree, HTREEITEM hRoot, CABL5kItemList *pTags)
{
	for( UINT n = 0; n < pTags->GetItemCount(); n++ ) {
		
		CABL5kItem    *pTag = pTags->GetItem(n);

		CString Name = pTag->m_Name;

		CString Type = pTag->m_Type;

		if( pTag->IsStruct() ) {

			CTreeViewItem Node;

			Node.SetText(Name);

			Node.SetParam(DWORD(pTag));

			HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);
			
			LoadTags(Tree, hNode, ((CABL5kStruct *) pTag)->m_pMembers);
			
			continue;
			}

		LoadTag(Tree, hRoot, pTag);
		}
	}

void CABL5kDialog::LoadTag(CTreeView &Tree, HTREEITEM hRoot, CABL5kItem *pTag)
{
	CString Scope = pTag->m_Name;

	CString  Name = pTag->m_Name;

	Scope = Scope.StripToken('.');

	Scope = Scope.StripToken('[');

	UINT p1;

	if( (p1 = Scope.Find(',')) < NOTHING ) {
		
		Name = Name.Mid(p1 + 1);
		}

	if( m_pConfig->m_Types.IsAtomicType(pTag->m_Type) ) {

		CTreeViewItem Node;

		Node.SetText(Name);

		Node.SetParam(DWORD(pTag));

		HTREEITEM hNode = Tree.InsertItem(hRoot, NULL, Node);

		AfxTouch(hNode);

		CString Select;
		
		if( m_fSelect && m_pConfig->DoExpandAddress(Select, *m_pAddr) ) {

			if( IsTable(*m_pAddr) ) {
				
				Select = Select.Left(Select.FindRev('['));
				}
			
			if( pTag->m_Name == Select ) {

				Tree.SelectItem(hNode, TVGN_CARET);
				}
			}

		return;
		}

	AfxAssert(FALSE);
	}

void CABL5kDialog::SetAddressText(CString Text)
{
	UINT p1, p2;

	if( (p1 = Text.FindRev('.')) == NOTHING ) {

		p1 = 0;
		}

	if( (p2 = Text.Find('[', p1)) < NOTHING) {
		
		Text = Text.Left(p2);
		}

	GetDlgItem(2001).SetWindowText(Text);
	}

void CABL5kDialog::SetAddrTypeText(CString const &Text)
{
	GetDlgItem(3002).SetWindowText(Text);
	}

void CABL5kDialog::SetDataTypeText(CString const &Text)
{
	GetDlgItem(3008).SetWindowText(Text);
	}

void CABL5kDialog::SetMinimumText(CString const &Text)
{
	GetDlgItem(3004).SetWindowText(Text);
	}

void CABL5kDialog::SetMaximumText(CString const &Text)
{
	GetDlgItem(3006).SetWindowText(Text);
	}

void CABL5kDialog::SetDataDescText(CString const &Text)
{
	GetDlgItem(3010).SetWindowText(Text);
	}

void CABL5kDialog::GetVisibleIndices(CArray <UINT> &List)
{
	DWORD dwID[] = { 2002, 2003, 2004 };

	for( UINT n = elements(dwID); n; n -- ) {

		CWnd &Wnd = GetDlgItem(dwID[n - 1]);

		if( Wnd.IsWindowVisible() ) {

			List.Append(dwID[n - 1]);
			}
		}
	}

CString CABL5kDialog::GetAddressText(void)
{
	CString Text = GetDlgItem(2001).GetWindowText();

	CArray <UINT> Visible;
	
	GetVisibleIndices(Visible);

	if( Visible.GetCount() ) {

		Text += "[";

		for( UINT n = 0; n < Visible.GetCount(); n ++ ) {

			CWnd &Wnd = GetDlgItem(Visible[n]);

			if( n ) {
				
				Text += ",";
				}

			Text += Wnd.GetWindowText();
			}

		Text += "]";
		}

	return Text;
	}

void CABL5kDialog::HideTableIndices(void)
{
	DWORD dwID[3] = { 2002, 2003, 2004 };		

	for( UINT n = 0; n < elements(dwID); n ++ ) {

		GetDlgItem(dwID[n]).ShowWindow(SW_HIDE);
		}
	}

void CABL5kDialog::ShowTableIndices(UINT uIndex, CArray <UINT> Dims)
{
	CString Text;
	
	m_pConfig->ExpandDims(Text, Dims, uIndex);

	CStringArray List;

	Text.Mid(1, Text.GetLength() - 2).Tokenize(List, ',');

	DWORD dwID[] = { 2004, 2003, 2002 };		

	for( UINT n = 0; n < elements(dwID); n ++ ) {

		CWnd &Wnd = GetDlgItem(dwID[n]);
		
		if( n < Dims.GetCount() ) {

			Wnd.ShowWindow(SW_SHOW);
			
			Wnd.SetWindowText(List[n]);
			}
		else
			Wnd.ShowWindow(SW_HIDE);

		Wnd.EnableWindow(m_fSelect);
		}
	}

void CABL5kDialog::EnableOkay(BOOL fEnable)
{
	GetDlgItem(IDOK).EnableWindow(fEnable);
	}

void CABL5kDialog::EnableCancel(BOOL fEnable)
{
	GetDlgItem(IDCANCEL).EnableWindow(fEnable);
	}

void CABL5kDialog::EnableImport(BOOL fEnable)
{
	GetDlgItem(5001).EnableWindow(fEnable);
	}

// End of File
