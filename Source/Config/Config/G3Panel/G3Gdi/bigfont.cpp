
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// GDI Version 2.0
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Big Numeric Font
//

class CBigFont : public IGdiFont
{
	public:
		// Constructor
		CBigFont(int cy);

		// Destructor
		~CBigFont(void);

		// Management
		UINT Release(void);

		// Attributes
		BOOL IsProportional(void);
		int  GetBaseLine   (void);
		int  GetGlyphWidth (WORD c);
		int  GetGlyphHeight(WORD c);

		// Operations
		void InitBurst(IGdi *pGDI, CLogFont const &Font);
		void DrawGlyph(IGdi *pGDI, int &x, int &y, WORD c);
		void BurstDone(IGdi *pGDI, CLogFont const &Font);

	protected:
		// Font 0 Data
		static BYTE const m_bFontData0[];
		static WORD const m_wFontCode0[];
		static BYTE const m_xFontSize0[];
		static WORD const m_wFontFind0[];
		static UINT const m_uCount0;
		
		// Font 1 Data
		static BYTE const m_bFontData1[];
		static WORD const m_wFontCode1[];
		static BYTE const m_xFontSize1[];
		static WORD const m_wFontFind1[];
		static UINT const m_uCount1;
		
		// Font 2 Data
		static BYTE const m_bFontData2[];
		static WORD const m_wFontCode2[];
		static BYTE const m_xFontSize2[];
		static WORD const m_wFontFind2[];
		static UINT const m_uCount2;
		
		// Font 3 Data
		static BYTE const m_bFontData3[];
		static WORD const m_wFontCode3[];
		static BYTE const m_xFontSize3[];
		static WORD const m_wFontFind3[];
		static UINT const m_uCount3;

		// Data Members
		int    m_ySize;
		PCBYTE m_pData;
		PCWORD m_pCode;
		PCBYTE m_pSize;
		PCWORD m_pFind;
		UINT   m_uCount;
		int    m_xSpace;
		int    m_xDigit;
		BOOL   m_fFill;
		COLOR  m_Fore;
		COLOR  m_Back;

		// Implementation
		BOOL IsFixed(WORD c);
		BOOL IsFixedDigit(WORD c);
		BOOL IsFixedLetter(WORD c);
		BOOL IsSpace(WORD c);

		// Sort Function
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Big Numeric Font
//

// Font Data

#include "bigfont.hpp"

// Instantiator

IGdiFont * Create_BigFont(int ySize)
{
	return new CBigFont(ySize);
	}

// Constructor

CBigFont::CBigFont(int ySize)
{
	switch( (m_ySize = ySize) ) {

		case 24:
			m_pData  = m_bFontData0;
			m_pCode  = m_wFontCode0;
			m_pSize  = m_xFontSize0;
			m_pFind  = m_wFontFind0;
			m_uCount = m_uCount0;
			m_xSpace = 5;
			break;

		case 32:
			m_pData  = m_bFontData1;
			m_pCode  = m_wFontCode1;
			m_pSize  = m_xFontSize1;
			m_pFind  = m_wFontFind1;
			m_uCount = m_uCount1;
			m_xSpace = 3;
			break;

		case 64:
			m_pData  = m_bFontData2;
			m_pCode  = m_wFontCode2;
			m_pSize  = m_xFontSize2;
			m_pFind  = m_wFontFind2;
			m_uCount = m_uCount2;
			m_xSpace = 4;
			break;

		case 96:
			m_pData  = m_bFontData3;
			m_pCode  = m_wFontCode3;
			m_pSize  = m_xFontSize3;
			m_pFind  = m_wFontFind3;
			m_uCount = m_uCount3;
			m_xSpace = 5;
			break;
		}

	m_xDigit = GetGlyphWidth(L'4');

	m_fFill  = FALSE;
	}

// Destructor

CBigFont::~CBigFont(void)
{
	}

// Management

UINT CBigFont::Release(void)
{
	delete this;

	return 0;
	}

// Attributes

BOOL CBigFont::IsProportional(void)
{
	return TRUE;
	}

int CBigFont::GetBaseLine(void)
{
	return 4;
	}

int CBigFont::GetGlyphWidth(WORD c)
{
	PVOID p;

	switch( c ) {

		case spaceNormal:
		case spaceNoBreak:

			return m_xSpace;
		
		case spaceNarrow:

			return m_xSpace / 2;

		case spaceHair:

			return 1;

		case spaceFigure:

			return m_xDigit;
		}

	if( IsFixed(c) ) {

		return m_xDigit;
		}

	if( c >= 'a' && c <= 'z' ) {

		c -= 'a';

		c += 'A';

		return GetGlyphWidth(c);
		}

	if( c == 0x0478 ) {

		int x1 = GetGlyphWidth('O');
		
		int x2 = GetGlyphWidth('y');

		return x1 + x2;
		}

	if( c == 0x0479 ) {

		int x1 = GetGlyphWidth('o');
		
		int x2 = GetGlyphWidth('y');

		return x1 + x2;
		}

	if( (p = bsearch(&c, m_pCode, m_uCount, sizeof(WORD), SortFunc)) ) {

		UINT n = PWORD(p) - m_pCode;

		return m_pSize[n];
		}

	return GetGlyphWidth('?');
	}

int CBigFont::GetGlyphHeight(WORD c)
{
	return m_ySize;
	}

// Operations

void CBigFont::InitBurst(IGdi *pGDI, CLogFont const &Font)
{
	if( Font.m_Trans == modeOpaque ) {

		pGDI->PushBrush();

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Font.m_Back);

		m_fFill = TRUE;
		}

	m_Fore = Font.m_Fore;

	m_Back = Font.m_Back;
	}

void CBigFont::DrawGlyph(IGdi *pGDI, int &x, int &y, WORD c)
{
	PVOID p;

	if( IsSpace(c) ) {

		int cx = GetGlyphWidth(c);

		if( m_fFill ) {

			pGDI->FillRect(x, y, x+cx, y+m_ySize);
			}

		x += cx;

		return;
		}

	if( IsFixed(c) ) {

		if( IsFixedDigit(c) ) {

			c -= digitFixed;

			c += digitSimple;
			}
		else {
			c -= letterFixed;

			c += letterSimple;
			}

		int cx = GetGlyphWidth(c);

		int x1 = (m_xDigit - cx + 1) / 2;

		int x2 = (m_xDigit - cx + 0) / 2;

		if( c == '1' ) {

			x1 -= 1;

			x2 += 1;
			}

		if( x1 ) {
			
			if( m_fFill ) {

				pGDI->FillRect(x, y, x+x1, y+m_ySize);
				}
	
			x += x1;
			}

		if( TRUE ) {

			DrawGlyph(pGDI, x, y, c);
			}

		if( x2 ) {
			
			if( m_fFill ) {

				pGDI->FillRect(x, y, x+x2, y+m_ySize);
				}

			x += x2;
			}

		return;
		}

	if( c >= 'a' && c <= 'z' ) {

		c -= 'a';

		c += 'A';

		DrawGlyph(pGDI, x, y, c);

		return;
		}

	if( c == 0x0478 ) {

		DrawGlyph(pGDI, x, y, 'O');
	
		DrawGlyph(pGDI, x, y, 'y');

		return;
		}

	if( c == 0x0479 ) {

		DrawGlyph(pGDI, x, y, 'o');
	
		DrawGlyph(pGDI, x, y, 'y');

		return;
		}

	if( (p = bsearch(&c, m_pCode, m_uCount, sizeof(WORD), SortFunc)) ) {

		UINT  n = PWORD(p) - m_pCode;

		int  cx = m_pSize[n];

		int  cy = m_ySize;

		if( m_fFill ) {

			pGDI->CharBlt( x,
				       y,
				       cx,
				       cy,
				       0,
				       PBYTE(m_pData + m_pFind[n]),
				       m_Back,
				       m_Fore
				       );
			}
		else {
			pGDI->CharBlt( x,
				       y,
				       cx,
				       cy,
				       0,
				       PBYTE(m_pData + m_pFind[n]),
				       m_Fore
				       );
			}

		x += cx;

		return;
		}

	DrawGlyph(pGDI, x, y, '?');
	}

void CBigFont::BurstDone(IGdi *pGDI, CLogFont const &Font)
{
	if( m_fFill ) {

		pGDI->PullBrush();

		m_fFill = FALSE;
		}
	}

// Implementation

BOOL CBigFont::IsFixed(WORD c)
{
	return IsFixedDigit(c) || IsFixedLetter(c);
	}

BOOL CBigFont::IsFixedDigit(WORD c)
{
	if( c >= digitFixed && c < digitFixed + 10 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CBigFont::IsFixedLetter(WORD c)
{
	if( c >= letterFixed && c < letterFixed + 6 ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CBigFont::IsSpace(WORD c)
{
	switch( c ) {

		case spaceNormal:
		case spaceNarrow:
		case spaceHair:
		case spaceNoBreak:
		case spaceFigure:

			return TRUE;
		}

	return FALSE;
	}

// Sort Function

int CBigFont::SortFunc(PCVOID p1, PCVOID p2)
{
	WORD w1 = PWORD(p1)[0];

	WORD w2 = PWORD(p2)[0];

	if( w1 < w2 ) return -1;

	if( w1 > w2 ) return +1;

	return 0;
	}

// End of File
