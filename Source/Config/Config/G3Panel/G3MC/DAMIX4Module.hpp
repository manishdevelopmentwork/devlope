
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4Module_HPP

#define INCLUDE_DAMix4Module_HPP

#include "ManticoreGenericModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDAMix4AnalogInput;
class CDAMix4AnalogInputConfig;

class CDAMix4AnalogOutput;
class CDAMix4AnalogOutputConfig;

class CDAMix4DigitalInput;
class CDAMix4DigitalInputConfig;

class CDAMix4DigitalOutput;
class CDAMix4DigitalOutputConfig;

class CDAMixDigitalConfig;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module
//

class CDAMix4Module : public CManticoreGenericModule
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDAMix4Module(void);

	// UI Management
	CViewWnd * CreateMainView(void);

	// Comms Object Access
	UINT GetObjectCount(void);
	BOOL GetObjectData(UINT uIndex, CObjectData &Data);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	CDAMix4AnalogInput         * m_pAnalogInput;
	CDAMix4AnalogInputConfig   * m_pAnalogInputConfig;
	CDAMix4AnalogOutput        * m_pAnalogOutput;
	CDAMix4AnalogOutputConfig  * m_pAnalogOutputConfig;
	CDAMix4DigitalInput        * m_pDigitalInput;
	CDAMix4DigitalInputConfig  * m_pDigitalInputConfig;
	CDAMix4DigitalOutput       * m_pDigitalOutput;
	CDAMix4DigitalOutputConfig * m_pDigitalOutputConfig;
	CDAMixDigitalConfig        * m_pDigitalConfig;

protected:
	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
