
#include "intern.hpp"

#include "rawact.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Port Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CRawTCPActiveDriver);

// Constructor

CRawTCPActiveDriver::CRawTCPActiveDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pSock = NULL;

	m_IP    = 0;

	m_uPort = 1234;

	m_fKeep = FALSE;

	m_uTime = 10000;

	m_fDirty = FALSE;
	}

// Destructor

CRawTCPActiveDriver::~CRawTCPActiveDriver(void)
{
	}

// Configuration

void MCALL CRawTCPActiveDriver::Load(LPCBYTE pData)
{
	SkipUpdate(pData);

	m_IP    = GetAddr(pData);

	m_uPort = GetWord(pData);

	m_uTime = GetWord(pData);

	m_fKeep = GetByte(pData);
	}

// Management

void MCALL CRawTCPActiveDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CRawTCPActiveDriver::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Abort();
		
		m_pSock->Release();
		}
	}

// User Access

UINT MCALL CRawTCPActiveDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	if( uFunc == 1 ) { // Set target IP address
		
		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		m_IP = MotorToHost(dwValue);

		m_fDirty = TRUE;
				
		Free(pText);
			
		return 1;    
		}
	
	if( uFunc == 2 )  {  // Set target port

		UINT uValue = strtol(Value, NULL, 10);

		if( uValue > 0 && uValue <= 0xFFFF ) {

			m_uPort = uValue;

			m_fDirty = TRUE;

			return 1;
			}
		}

	return 0;
	}

// Entry Points

void MCALL CRawTCPActiveDriver::Disconnect(void)
{
	if( m_pSock ) {

		m_pSock->Close();
		}
	}

UINT MCALL CRawTCPActiveDriver::Read(UINT uTime)
{
	UINT uInit = GetTickCount();

	UINT uTest = ToTicks(uTime);

	for(;;) {

		if( CheckSocket(FALSE) ) {

			BYTE bData;

			UINT uSize = 1;

			if( m_pSock->Recv(&bData, uSize) == S_OK ) {

				return bData;
				}
			}

		if( GetTickCount() - uInit >= uTest ) {

			return NOTHING;
			}

		Sleep(10);
		}
	}

UINT MCALL CRawTCPActiveDriver::Write(BYTE bData, UINT uTime)
{
	UINT uInit = GetTickCount();

	UINT uTest = ToTicks(uTime);

	while( CheckSocket(uTime ? TRUE : FALSE) ) {

		UINT uSize = 1;

		if( m_pSock->Send(&bData, uSize) == S_OK ) {

			return 1;
			}

		if( GetTickCount() - uInit >= uTest ) {

			break;
			}

		Sleep(10);
		}

	return 0;
	}

UINT MCALL CRawTCPActiveDriver::Write(PCBYTE pData, UINT uCount, UINT uTime)
{
	UINT uSent = 0;

	UINT uInit = GetTickCount();

	UINT uTest = ToTicks(uTime);

	while( CheckSocket(uTime ? TRUE : FALSE) ) {

		UINT uSend = uCount - uSent;

		if( m_pSock->Send(PBYTE(pData), uSend) == S_OK ) {

			pData += uSend;

			uSent += uSend;

			if( uSent == uCount ) {

				return uCount;
				}

			}

		if( GetTickCount() - uInit >= uTest ) {

			break;
			}

		Sleep(10);
		}

	return uSent;
	}

// Implementation

BOOL CRawTCPActiveDriver::CheckSocket(BOOL fWait)
{
	if( fWait ) {

		SetTimer(m_uTime);
		}

	for(;;) {

		if( !m_pSock ) {

			m_pSock = CreateSocket(IP_TCP);

			if( m_pSock ) {

				if( m_fKeep ) {

					m_pSock->SetOption(OPT_KEEP_ALIVE, TRUE);
					}

				IPADDR const &IP = (IPADDR const &) m_IP;

				m_pSock->Connect(IP, m_uPort);
				}
			else
				Sleep(100);

			if( GetTimer() ) {

				continue;
				}

			return FALSE;
			}
		else {
			UINT uPhase;

			m_pSock->GetPhase(uPhase);

			if( uPhase == PHASE_CLOSING ) {

				m_pSock->Close();

				m_pSock->Release();

				m_pSock = NULL;

				Sleep(20);

				continue;
				}

			if( m_fDirty || uPhase == PHASE_ERROR ) {

				m_fDirty = FALSE;

				m_pSock->Abort();

				m_pSock->Release();

				m_pSock = NULL;

				Sleep(20);

				continue;
				}

			if( uPhase == PHASE_OPEN ) {

				return TRUE;
				}

			if( fWait ) {

				if( GetTimer() ) {

					Sleep(20);

					continue;
					}
				}

			return FALSE;
			}
		}
	}

// Helpers

BOOL CRawTCPActiveDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CRawTCPActiveDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CRawTCPActiveDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}

// End of File
