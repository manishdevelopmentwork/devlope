
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP
	
#define	INCLUDE_TESTING_HPP

//////////////////////////////////////////////////////////////////////////
//
// Test Device Options
//

class CTestDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestDeviceOptions(void);

	public:
		// Public Data
		UINT m_Drop;

	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Driver Options
//

class CTestDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestDriverOptions(void);

	public:
		// Public Data
		UINT m_Delay;

	protected:
		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Driver
//

class CTestDriver : public ICommsDriver
{
	public:
		// Constructor
		CTestDriver(void);

		// IBase Methods
		void Release(void);

		// Driver Data
		WORD	GetID(void);
		UINT	GetType(void);
		CString	GetString(UINT ID);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDriverConfig(void);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	ParseAddress(CError &Error, CAddress &Addr, CString Text);
		BOOL	ExpandAddress(CString &Text, CAddress const &Addr);
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart);
		BOOL	ListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data);
	};

// End of File

#endif
