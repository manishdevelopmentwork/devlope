/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** CONNMGR.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Connection Manager class header file
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef CONNMGR_H
#define CONNMGR_H

#define DATA_SIZE_MASK          0x01FF	/* Data size mask in the connection parameters word */
#define TRANSPORT_CLASS_MASK    0x0F	/* Transport class mask in the transport type byte */
#define CLASS_TYPE_MASK         0x70	/* Transport type mask in the transport type byte */

#define CONNMGR_CLASS			6		/* Connection Manager class Id */

#define FWD_OPEN_CMD_CODE         0x54	    /* Open Connection request */
#define FWD_CLOSE_CMD_CODE        0x4E		/* Close Connection request */
#define UNCONNECTED_SEND_CMD_CODE 0x52	    /* Unconnected Send request */

#define PRIORITY_TICK_TIME	   0x0A
#define TIMEOUT_TICKS		   0xF0

#define INVALID_CLASS_8BIT			0xff
#define INVALID_INSTANCE_8BIT		0xff

#define REVISION_RELAXED_FLAG		0x80
#define MAJOR_REVISION_MASK			0x7F

typedef struct tagFWD_OPEN				/* Forward open parameters structure */
{
   UINT8     bOpenPriorityTickTime;		/* Priority bit is bit 4. Low nibble is the number of milliseconds per tick */
   UINT8     bOpenTimeoutTicks;			/* Number of ticks to timeout */

   UINT32    lLeOTConnId;				/* Originator -> Target connection id */
   UINT32    lLeTOConnId;				/* Target -> Originator connection id */

   UINT16    iLeOrigConnectionSn;		/* Unique number identifying this connection */
   UINT16    iLeOrigVendorId;			/* Unique number identifying the originator vendor */
   UINT32    lLeOrigDeviceSn;			/* Unique number identifying the originator device serial number */

   UINT8     bTimeoutMult;				/* Timeout multiplier determines timeout interval on I/O receive */

   UINT8     bReserved;					/* Must be 0 */
   UINT16    iReserved;					/* Must be 0 */

   UINT32    lLeOTRpi;					/* Originator -> Target requested packet interval */
   UINT16    iLeOTConnParams;			/* Originator -> Target connection parameters */

   UINT32    lLeTORpi;					/* Target -> Originator requested packet interval */
   UINT16    iLeTOConnParams;			/* Target -> Originator connection parameters */

   UINT8     bClassTrigger;				/* Transport class and type */ 
   UINT8     bConnPathSize;				/* connection path size in words */

}
FWD_OPEN;

#define FWD_OPEN_SIZE  36

typedef enum							/* Forward Open structure member offsets */
{
    offs_fwdopen_bOpenPriorityTickTime = 0,
    offs_fwdopen_bOpenTimeoutTicks = 1,    
    offs_fwdopen_lLeOTConnId = 2,           
    offs_fwdopen_lLeTOConnId = 6,
	offs_fwdopen_iLeOrigConnectionSn = 10,
	offs_fwdopen_iLeOrigVendorId = 12,
	offs_fwdopen_lLeOrigDeviceSn = 14,
	offs_fwdopen_bTimeoutMult = 18,
	offs_fwdopen_bReserved = 19,
	offs_fwdopen_iReserved = 20,
	offs_fwdopen_lLeOTRpi = 22,
	offs_fwdopen_iLeOTConnParams = 26,
	offs_fwdopen_lLeTORpi = 28,
	offs_fwdopen_iLeTOConnParams = 32,
	offs_fwdopen_bClassTrigger = 34,
	offs_fwdopen_bConnPathSize = 35	
} 
FwdOpenOffsetEnum;


typedef struct tagFWD_OPEN_REPLY
{
   UINT32        lLeOTConnId;			/* Originator -> Target connection id */
   UINT32        lLeTOConnId;			/* Target -> Originator connection id */

   UINT16        iLeOrigConnectionSn;	/* Unique number identifying this connection */
   UINT16        iLeOrigVendorId;		/* Unique number identifying the originator vendor */
   UINT32        lLeOrigDeviceSn;		/* Unique number identifying the originator device serial number */

   UINT32        lLeOTApi;				/* Originator -> Target actual packet interval */
   UINT32        lLeTOApi;				/* Target -> Originator actual packet interval */
   UINT16        iReserved;
}
FWD_OPEN_REPLY;

#define FWD_OPEN_REPLY_SIZE   26


typedef struct tagFWD_CLOSE
{
   UINT8     bOpenPriorityTickTime;		/* Priority bit is bit 4. Low nibble is the number of milliseconds per tick */
   UINT8     bOpenTimeoutTicks;			/* Number of ticks to timeout */

   UINT16    iLeOrigConnectionSn;		/* Unique number identifying this connection */
   UINT16    iLeOrigVendorId;			/* Unique number identifying the originator vendor */
   UINT32    lLeOrigDeviceSn;			/* Unique number identifying the originator device serial number */

   UINT8     bClosePathSize;			/* close path size in words */
   UINT8     bReserved;
}
FWD_CLOSE;

#define FWD_CLOSE_SIZE  12

typedef enum							/* Forward Close structure member offsets */
{
    offs_fwdclose_bOpenPriorityTickTime = 0,
    offs_fwdclose_bOpenTimeoutTicks = 1,    
    offs_fwdclose_iLeOrigConnectionSn = 2,           
    offs_fwdclose_iLeOrigVendorId = 4,
	offs_fwdclose_lLeOrigDeviceSn = 6,
	offs_fwdclose_bClosePathSize = 10,
	offs_fwdclose_bReserved = 11,	
} 
FwdCloseOffsetEnum;


typedef struct tagFWD_CLOSE_REPLY
{
   UINT16    iLeOrigConnectionSn;		/* Unique number identifying this connection */
   UINT16    iLeOrigVendorId;			/* Unique number identifying the originator vendor */
   UINT32    lLeOrigDeviceSn;			/* Unique number identifying the originator device serial number */
   UINT8     bPathSize;					/* Message Path size in words */
   UINT8     bReserved;
} 
FWD_CLOSE_REPLY;

#define FWD_CLOSE_REPLY_SIZE   10

extern void connmgrIncomingConnection( INT32 nRequest );
extern void connmgrPrepareFwdOpenReply( INT32 nSession, INT32 nConnection, INT32 nRequest );

extern void connmgrProcessFwdClose( INT32 nRequest );
extern void connmgrPrepareFwdCloseReply( INT32 nConnection, UINT8 bPathSize, INT32 nRequest );

extern void connmgrInitFwdOpenTypeFromBuf( UINT8* buf, FWD_OPEN* fwdOpenType );
extern void connmgrInitBufFromFwdOpenType( UINT8* buf, FWD_OPEN* fwdOpenType );
extern void connmgrInitFwdCloseTypeFromBuf( UINT8* buf, FWD_CLOSE* fwdCloseType );
extern void connmgrInitBufFromFwdCloseType( UINT8* buf, FWD_CLOSE* fwdCloseType );

extern void connmgrParseClassInstanceRequest( INT32 nRequest );
extern void connmgrParseClassRequest( INT32 nRequest );
extern void connmgrParseInstanceRequest( INT32 nRequest );

extern void  connmgrProcessUnconnectedSend( INT32 nRequest );







#endif /* #ifndef CONNMGR_H */
