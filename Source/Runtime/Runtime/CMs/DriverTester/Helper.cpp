
#include "Intern.hpp"

#include "Helper.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Helper Object
//

// Instantiator

clink IUnknown * Create_Helper(PCSTR pName)
{
	return (IHelper *) New CHelper;
	}

// Constructor

CHelper::CHelper(void)
{
	StdSetRef();
	}

// IUnknown

HRESULT CHelper::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IHelper);

	StdQueryInterface(IHelper);

	return E_NOINTERFACE;
	}

ULONG CHelper::AddRef(void)
{
	StdAddRef();
	}

ULONG CHelper::Release(void)
{
	StdRelease();
	}

// IHelper

BOOL METHOD CHelper::MoreHelp(WORD ID, void **pHelp)
{
	ULONG Code = E_FAIL;

	switch( ID ) {

		case IDH_ENETIP:	

			Code = AfxGetObject("helper-enetip", 0, IEthernetIPHelper, pHelp);

			break;

		case IDH_DISPLAY:

			Code = AfxGetObject("helper-display", 0, IDisplayHelper, pHelp);

			break;

		case IDH_SYNC:

			Code = AfxGetObject("helper-sync", 0, ISyncHelper, pHelp);

			break;

		case IDH_DEVNET:

			Code = AfxGetObject("helper-dnet", 0, IDeviceNetHelper, pHelp);

			break;

		case IDH_EXTRA:

			Code = AfxGetObject("helper-extra", 0, IExtraHelper, pHelp);

			break;

		case IDH_MPI:

			Code = AfxGetObject("helper-mpi", 0, IMPIHelper, pHelp);

			break;

		case IDH_TRACE:

			Code = AfxGetObject("helper-trace", 0, ITraceHelper, pHelp);

			break;
		}

	AfxAssert(Code == S_OK);

	return Code == S_OK;
	}

// End of File
