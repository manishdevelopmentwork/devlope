
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GDI Brush Object
//

// Dynamic Class

AfxImplementDynamicClass(CBrush, CGdiObject);

// Constructors

CBrush::CBrush(void)
{
	}

CBrush::CBrush(CBrush const &That)
{
	AfxAssert(FALSE);
	}

CBrush::CBrush(int nElement)
{
	CheckException(Create(nElement));
	}

CBrush::CBrush(CColor const &Color)
{
	CheckException(Create(Color));
	}

CBrush::CBrush(int nStyle, CColor const &Color)
{
	CheckException(Create(nStyle, Color));
	}

CBrush::CBrush(CBitmap const &Bitmap)
{
	CheckException(Create(Bitmap));
	}

CBrush::CBrush(CColor const &C1, CColor const &C2, BYTE bMix)
{
	CheckException(Create(C1, C2, bMix));
	}

// Creation

BOOL CBrush::Create(int nElement)
{
	Detach(TRUE);

	HANDLE hBrush = CreateSolidBrush(GetSysColor(nElement));

	return Attach(hBrush);
	}

BOOL CBrush::Create(CColor const &Color)
{
	Detach(TRUE);

	HANDLE hBrush = CreateSolidBrush(Color);

	return Attach(hBrush);
	}

BOOL CBrush::Create(int nStyle, CColor const &Color)
{
	Detach(TRUE);

	HANDLE hBrush = CreateHatchBrush(nStyle, Color);

	return Attach(hBrush);
	}

BOOL CBrush::Create(CBitmap const &Bitmap)
{
	Detach(TRUE);

	HANDLE hBrush = CreatePatternBrush(Bitmap);

	return Attach(hBrush);
	}

BOOL CBrush::Create(CColor const &C1, CColor const &C2, BYTE bMix)
{
	Detach(TRUE);

	CBitmap Work(CClientDC(NULL), 16, 16);

	CBitmap Tone(16, 16, 1, 1);
	
	CMemoryDC DC;
	
	DC.Select(Tone);
	
	DC.FillRect(0, 0, 16, 16, CBrush(CColor(bMix, bMix, bMix)));
	
	DC.Deselect();
	
	DC.Select(Work);
	
	DC.SetTextColor(C2);
	
	DC.SetBkColor(C1);
	
	DC.FillRect(0, 0, 16, 16, CBrush(Tone));
	
	DC.Deselect();
	
	HANDLE hBrush = CreatePatternBrush(Work);

	return Attach(hBrush);
	}

// Handle Lookup

CBrush & CBrush::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CBrush NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CBrush &) CGdiObject::FromHandle(hObject, Class);
	}

// End of File
