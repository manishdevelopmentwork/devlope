
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GEOM_HPP
	
#define	INCLUDE_GEOM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimGeom;
class CPrimRect;
class CPrimEllipse;
class CPrimWedge;
class CPrimEllipseQuad;
class CPrimEllipseHalf;
class CPrimTextBox;
class CPrimDataBox;
class CPrimAnimImage;
class CPrimTimeDate;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Geometric Figure
//

class CPrimGeom : public CPrimWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimGeom(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Overridables
		BOOL StepAddress(void);

		// Data Members
		CPrimTankFill * m_pFill;
		CPrimPen      * m_pEdge;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rectangle
//

class CPrimRect : public CPrimGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRect(void);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void FindTextRect(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse
//

class CPrimEllipse : public CPrimGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimEllipse(void);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Wedge
//

class CPrimWedge : public CPrimGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWedge(void);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Orient;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse Quadrant
//

class CPrimEllipseQuad : public CPrimWedge
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimEllipseQuad(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Wedge
//

class CPrimEllipseHalf : public CPrimGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimEllipseHalf(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Orient;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Text Box
//

class CPrimTextBox : public CPrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimTextBox(void);

		// Operations
		void Set(CString Text, CSize MaxSize);

		// Overridables
		void  SetInitState(void);
		void  FindTextRect(void);
		CSize GetMinSize(IGDI *pGDI);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Data Box
//

class CPrimDataBox : public CPrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimDataBox(void);

		// Operations
		void Set(CString Text);

		// Overridables
		void  SetInitState(void);
		void  FindTextRect(void);
		CSize GetMinSize(IGDI *pGDI);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Animatable Image
//

class CPrimAnimImage : public CPrimRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimAnimImage(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void SetImage(UINT n, PCTXT pFile);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	     m_Keep;
		UINT         m_Count;
		CCodedItem * m_pValue;
		CCodedItem * m_pColor;
		CCodedItem * m_pShow;
		CPrimImage * m_pImage[10];

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Time and Date
//

class CPrimTimeDate : public CPrimDataBox
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimTimeDate(void);

		// Overridables
		void SetInitState(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
