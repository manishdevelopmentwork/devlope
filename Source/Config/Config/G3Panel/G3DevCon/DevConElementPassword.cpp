
#include "Intern.hpp"

#include "DevConElementPassword.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration String UI Element
//

// Base Class

#define CBaseClass CDevConElementEditBox

// Dynamic Class

AfxImplementDynamicClass(CDevConElementPassword, CBaseClass);

// Constructor

CDevConElementPassword::CDevConElementPassword(void)
{
	m_dwStyle |= ES_PASSWORD;
}

// Destructor

CDevConElementPassword::~CDevConElementPassword(void)
{
}

// Operations

void CDevConElementPassword::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	m_pEditCtrl->SetDefault(m_Default);
}

void CDevConElementPassword::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CStringArray List;

	pField->GetValue(L"format").Tokenize(List, ',');

	m_Type    = List[0];

	m_Default = List[1];
}

BOOL CDevConElementPassword::OnCheckData(CString &Data)
{
	if( m_Type == L"general" ) {

		return TRUE;
	}

	if( !m_Type.IsEmpty() && isdigit(m_Type[0]) ) {

		if( Data.GetLength() < UINT(watoi(m_Type)) ) {

			m_pEditCtrl->Error(CPrintf(IDS("The password must be at least %s characters."), PCTXT(m_Type)));

			return FALSE;
		}

		return TRUE;
	}

	if( m_Type == L"gre" ) {

		if( Data.GetLength() > 0 ) {

			if( !IsNumber(Data) ) {

				BOOL good = TRUE;

				CStringArray List;

				Data.Tokenize(List, '.');

				if( Data.GetLength() == 4 ) {

					for( UINT v = 0; v < 4; v++ ) {

						if( !IsNumber(List[v]) || watoi(List[v]) > 255 ) {

							good = FALSE;

							break;
						}
					}
				}
				else {
					good = FALSE;
				}

				if( !good ) {

					m_pEditCtrl->Error(IDS("GRE keys must be numbers or dotted-decimal IP addresses."));

					return FALSE;
				}
			}
		}

		return TRUE;
	}

	if( m_Type == L"pin" ) {

		if( Data.GetLength() > 0 && (Data.GetLength() < 4 || Data.GetLength() > 8) ) {

			m_pEditCtrl->Error(IDS("SIM PINs must be between 4 and 8 characters."));

			return false;
		}

		if( Data.GetLength() > 0 && !IsNumber(Data) ) {

			m_pEditCtrl->Error(IDS("SIM PINs must comprised numeric characters."));

			return false;
		}

		return true;
	}

	if( m_Type == L"wifi" ) {

		if( Data.GetLength() > 0 && (Data.GetLength() < 8 || Data.GetLength() > 63) ) {

			m_pEditCtrl->Error(IDS("Wi-Fi passwords must be between 8 and 63 characters."));

			return false;
		}

		return true;
	}

	return TRUE;
}

// Implementation

BOOL CDevConElementPassword::IsNumber(CString const &Data)
{
	UINT c = Data.GetLength();

	for( UINT n = 0; n < c; n++ ) {

		if( !iswdigit(Data[n]) ) {

			return FALSE;
		}
	}

	return TRUE;
}

// End of File
