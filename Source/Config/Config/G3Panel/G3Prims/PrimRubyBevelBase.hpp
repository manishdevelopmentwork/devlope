
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyBevelBase_HPP
	
#define	INCLUDE_PrimRubyBevelBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyWithText.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPrimRubyBrush;
class CPrimRubyPenEdge;

//////////////////////////////////////////////////////////////////////////
//
// Ruby BevelBase Primitive
//

class CPrimRubyBevelBase : public CPrimRubyWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyBevelBase(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void GetRefs(CPrimRefList &Refs);
		void SetInitState(void);
		void UpdateLayout(void);
		void FindTextRect(void);
		BOOL GetHand(UINT uHand, CPrimHand &Hand);

		// Data Members
		INT                m_Border;
		INT		   m_BordMin;
		INT		   m_BordMax;
		UINT		   m_Style;
		CPrimRubyBrush   * m_pFill;
		CPrimRubyPenEdge * m_pEdge;
		CPrimColor       * m_pHilite;
		CPrimColor       * m_pShadow;

	protected:
		// Data Members
		CRubyPath    m_pathFill;
		CRubyPath    m_pathEdge;
		CRubyPath    m_pathHilite;
		CRubyPath    m_pathShadow;
		CRubyGdiList m_listFill;
		CRubyGdiList m_listEdge;
		CRubyGdiList m_listHilite;
		CRubyGdiList m_listShadow;

		// Meta Data
		void AddMetaData(void);

		// Path Management
		void InitPaths(void);
		void MakePaths(void);
		void MakeLists(void);
	};

// End of File

#endif
