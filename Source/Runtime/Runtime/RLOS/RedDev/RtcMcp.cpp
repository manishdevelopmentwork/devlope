
#include "Intern.hpp"

#include "RtcMcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Shared Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// MCP7941x Real Time Clock
//

// Instantiator

IDevice * Create_RtcMcp(UINT uSlot)
{
	CRtcMcp *pDevice = New CRtcMcp(uSlot);

	pDevice->Open();

	return pDevice;
	}

// Constants

const DWORD CRtcMcp::constRtcMagic = 0x87654321;

// Constructor

CRtcMcp::CRtcMcp(UINT uSlot)
{
	m_bChip = 0xDE;

	m_uSlot = uSlot;

	AfxGetObject("i2c", 0, II2c, m_pI2c);

	AfxGetObject("fram", 0, ISerialMemory, m_pMem);
	}

// Destructor

CRtcMcp::~CRtcMcp(void)
{
	m_pI2c->Release();

	m_pMem->Release();
	}

// IRtc

UINT METHOD CRtcMcp::GetCalibStatus(void)
{
	DWORD ds;

	m_pMem->GetData(m_uSlot, PBYTE(&ds), sizeof(ds));

	return ds == constRtcMagic ? rtcCalibGood : rtcCalibBad;
	}

INT METHOD CRtcMcp::GetCalib(void)
{
	return GetCalFromChip();
	}

bool METHOD CRtcMcp::PutCalib(INT nCalib)
{
	if( nCalib < -127 || nCalib > 127 ) {

		return false;
		}

	SaveCalib(nCalib);
	
	return WriteCalToChip(nCalib);
	}

// Overridables

bool CRtcMcp::InitChip(void)
{
	BYTE bCtrl = 0x43;

	PutData(0x07, &bCtrl, 1);

	BYTE bBatt;

	if( GetData(0x03, &bBatt, 1) ) {

		if( !(bBatt & Bit(3)) ) {

			bBatt |= Bit(3);
		
			PutData(0x03, &bBatt, 1);
			}
		}

	CalibFromMem();

	Start();

	return true;
	}

bool CRtcMcp::GetTimeFromChip(void)
{
	BYTE bTime[7];

	if( GetData(0x00, bTime, sizeof(bTime)) ) {

		struct tm tm = { 0 };

		tm.tm_sec  = ToBin(bTime[0], 7);
		tm.tm_min  = ToBin(bTime[1], 7);
		tm.tm_hour = ToBin(bTime[2], 6);

		tm.tm_mday = ToBin(bTime[4], 6) + 0;
		tm.tm_mon  = ToBin(bTime[5], 5) - 1;
		tm.tm_year = ToBin(bTime[6], 8) + 100;

		AfxTrace("Got %2.2u/%2.2u/%4.4u %2.2u:%2.2u:%2.2u\n",
			 tm.tm_mon  + 1,
			 tm.tm_mday + 0,
			 tm.tm_year + 1900,
			 tm.tm_hour,
			 tm.tm_min,
			 tm.tm_sec
			 );

		AfxDump(bTime, sizeof(bTime));

		m_time = timegm(&tm);
		
		return true;
		}

	return false;
	}

bool CRtcMcp::GetSecsFromChip(UINT &uSecs)
{
	BYTE bSecs;

	if( GetData(0x00, &bSecs, 1) ) {

		uSecs = ToBin(bSecs, 7);

		return true;
		}

	return false;
	}

bool CRtcMcp::WriteTimeToChip(void)
{
	BYTE bData[7] = { 0, 0, 0, 8, 0, 0, 0 };

	struct tm *tm = gmtime(&m_time);

	AfxTrace("Put %2.2u/%2.2u/%4.4u %2.2u:%2.2u:%2.2u\n",
		 tm->tm_mon  + 1,
		 tm->tm_mday + 0,
		 tm->tm_year + 1900,
		 tm->tm_hour,
		 tm->tm_min,
		 tm->tm_sec
		 );

	bData[0] = ToBcd(tm->tm_sec,  7);
	bData[1] = ToBcd(tm->tm_min,  7);
	bData[2] = ToBcd(tm->tm_hour, 6);

	bData[4] = ToBcd(tm->tm_mday - 0,   6);
	bData[5] = ToBcd(tm->tm_mon  + 1,   5);
	bData[6] = ToBcd(tm->tm_year - 100, 8);

	AfxDump(bData, sizeof(bData));

	Stop();

	PutData(0x08, bData, sizeof(bData));

	Start();

	return true;
	}

// Implementation

bool CRtcMcp::GetData(BYTE bAddr, PBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		if( m_pI2c->Recv(m_bChip, &bAddr, 1, pData, uCount) ) {

			m_pI2c->Free();

			return true;
			}

		m_pI2c->Free();
		}

	return false;
	}

bool CRtcMcp::PutData(BYTE bAddr, PBYTE pData, UINT uCount)
{
	if( m_pI2c->Lock(FOREVER) ) {

		if( m_pI2c->Send(m_bChip, &bAddr, 1, pData, uCount) ) {

			m_pI2c->Free();

			return true;
			}
		
		m_pI2c->Free();
		}

	return false;
	}

void CRtcMcp::Start(void)
{
	BYTE bData;

	if( GetData(0x00, &bData, 1) ) {

		if( !(bData & Bit(7)) ) {

			bData |= Bit(7);

			PutData(0x00, &bData, 1);
			}
		}
	}

void CRtcMcp::Stop(void)
{
	BYTE bData;

	if( GetData(0x00, &bData, 1) ) {

		bData &= ~Bit(7);

		if( PutData(0x00, &bData, 1) ) {

			for(;;) {

				BYTE bStop;

				GetData(0x03, &bStop, 1);
			
				if( !(bStop & Bit(5)) ) {

					break;
					}
				}
			}
		}
	}

INT CRtcMcp::GetCalFromChip(void)
{
	INT nCal = 0;

	BYTE bData = 0;

	GetData(0x08, &bData, 1);

	nCal = bData & 0x7F;

	if( !(bData & Bit(7)) ) {

		nCal = -nCal;
		}

	return nCal;
	}

bool CRtcMcp::WriteCalToChip(INT nCalib)
{
	BYTE bData = 0;

	if( nCalib > 0 ) {

		bData |= Bit(7);
		}

	bData |= abs(nCalib);

	return PutData(0x08, &bData, 1);
	}

void CRtcMcp::SaveCalib(INT nCalib)
{
	DWORD ds[2];

	ds[0] = constRtcMagic;

	ds[1] = nCalib;

	m_pMem->PutData(m_uSlot, PBYTE(&ds), sizeof(ds));
	}

void CRtcMcp::CalibFromMem(void)
{
	DWORD ds[2];

	m_pMem->GetData(m_uSlot, PBYTE(&ds), sizeof(ds));

	if( ds[0] == constRtcMagic ) {

		INT nCalib = ds[1];

		WriteCalToChip(nCalib);
		}
	}

UINT CRtcMcp::ToBin(BYTE bData, UINT uWidth)
{
	BYTE bLoMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 0) & 0x0F;

	BYTE bHiMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 4) & 0x0F;

	return (((bData >> 4) & bHiMask) * 10) + ((bData >> 0) & bLoMask);
	}

BYTE CRtcMcp::ToBcd(UINT uData, UINT uWidth)
{
	BYTE bLoMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 0) & 0x0F;

	BYTE bHiMask = ((BYTE(0xFF) >> (8 - uWidth)) >> 4) & 0x0F;

	return (((uData / 10) & bHiMask) << 4) | (((uData % 10) & bLoMask) << 0);
	}

// End of File
