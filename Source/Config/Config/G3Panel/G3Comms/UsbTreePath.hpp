
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UsbTreePath_HPP

#define INCLUDE_UsbTreePath_HPP

//////////////////////////////////////////////////////////////////////////
//
// USB Rack Path Data
//

union DLLAPI CUsbTreePath
{
	struct
	{
		DWORD	dwCtrl	    : 3;
		DWORD	dwHost	    : 2;
		DWORD   dwTier      : 3;
		DWORD	dwPort1	    : 4;
		DWORD	dwPort2	    : 4;
		DWORD	dwPort3	    : 4;
		DWORD	dwPort4	    : 4;
		DWORD	dwPort5	    : 4;
		DWORD	dwPort6	    : 4;

		} a;

	DWORD	dw;

	void SetPort(UINT n, UINT uPort);
	};

// End of File

#endif
