
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HLINK_HPP
	
#define	INCLUDE_HLINK_HPP 

/////////////////////////////////////////////////////////////////////////
//
// Hardy Instruments Hardy Link Device Options
//

class CHardyLinkDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CHardyLinkDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_ID;
		UINT m_Mode;
		UINT m_Check;
							
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// Hardy Instruments Hardy Link Driver
//

class CHardyLinkDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CHardyLinkDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDeviceConfig(void);
		 

	protected:

		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
