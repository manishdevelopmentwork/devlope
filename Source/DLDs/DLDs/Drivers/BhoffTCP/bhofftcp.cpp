
#include "intern.hpp"

#include "bhofftcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Beckhoff Driver
//

// Instantiator

INSTANTIATE(CBeckhoffTCPDriver);

// Constructor

CBeckhoffTCPDriver::CBeckhoffTCPDriver(void)
{
	m_Ident  = DRIVER_ID;

	m_uKeep  = 0;

	m_dErrValue = 0;
	m_dErrItem  = 0;
	}

// Destructor

CBeckhoffTCPDriver::~CBeckhoffTCPDriver(void)
{
	}

// Configuration

void MCALL CBeckhoffTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CBeckhoffTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CBeckhoffTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CBeckhoffTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_AMSPort	= GetWord(pData);
			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_AMSID		= GetAddr(pData);
			m_pCtx->m_AMSSuffix	= GetWord(pData);
			m_pCtx->m_dTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			m_pCtx->m_WRCADS	= 0;
			m_pCtx->m_WRCDEV	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBeckhoffTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBeckhoffTCPDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING ");

	if( !OpenSocket() ) return CCODE_ERROR;

//**/	AfxTrace0("- Socket Open");

	DWORD    Data[1];
	
	CAddress Addr;

	Addr.a.m_Table  = SP_MEMBYTE;
		
	Addr.a.m_Offset = 0;
		
	Addr.a.m_Type   = addrByteAsByte;

	Addr.a.m_Extra	= 0;
	
	return Read(Addr, Data, 1);
	}

CCODE MCALL CBeckhoffTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SP_DERRV: *pData = m_dErrValue; return 1;
		case SP_DERRI: *pData = m_dErrItem;  return 1;
		}

	if( !OpenSocket() ) return CCODE_ERROR;

//**/	AfxTrace3("\r\nRead T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	UINT uType = Addr.a.m_Type;

	switch( uType ) {

		case addrBitAsBit:

			if( Addr.a.m_Table == addrNamed ) {

				pData[0] = 0; // write only
				return 1;
				}

			uCount = 1; // protocol returns only one bit

			if( !DoBitRead(Addr, pData, &uCount) ) return CCODE_ERROR;

			break;

		case addrByteAsByte:

			MakeMin( uCount, 32 );

			if( !DoByteRead(Addr, pData, &uCount) ) return CCODE_ERROR;

			break;

		case addrWordAsWord:

			MakeMin( uCount, 16 );

			if( !DoWordRead(Addr, pData, &uCount) ) return CCODE_ERROR;

			break;

		case addrLongAsLong:
		case addrRealAsReal:

			MakeMin( uCount, 8 );

			if( !DoLongRead(Addr, pData, &uCount, uType == addrRealAsReal) ) return CCODE_ERROR;

			break;

		default: return CCODE_ERROR | CCODE_HARD;
		}

	return CheckFrame(uCount);
	}

CCODE MCALL CBeckhoffTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SP_DERRV:
		case SP_DERRI:
			m_dErrValue = 0;
			m_dErrItem  = 0;
			return 1;
		}

	if( !OpenSocket() ) return CCODE_ERROR;

//**/	AfxTrace3("\r\n***** WRITE ***** T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( Addr.a.m_Table == SP_INPUTS )  return uCount;

	UINT uType = Addr.a.m_Type;

	switch( uType ) {

		case addrBitAsBit:

			uCount = 1;

			if( !DoBitWrite(Addr, pData, uCount) ) return CCODE_ERROR;

			break;

		case addrByteAsByte:

			MakeMin( uCount, 32 );
			
			if( !DoByteWrite(Addr, pData, uCount) ) return CCODE_ERROR;

			break;

		case addrWordAsWord:

			MakeMin( uCount, 16 );
			
			if( !DoWordWrite(Addr, pData, uCount) ) return CCODE_ERROR;

			break;

		case addrLongAsLong:
		case addrRealAsReal:

			MakeMin( uCount, 8 );

			if( !DoLongWrite(Addr, pData, uCount, uType == addrRealAsReal) ) return CCODE_ERROR;

			break;

		default: return CCODE_ERROR | CCODE_HARD;
		}

	return CheckFrame(uCount);
	}

// Frame Building

void CBeckhoffTCPDriver::PutRWHeader(AREF Addr, UINT uCmd, UINT uSize, UINT uDFLen)
{
	StartFrame(uCmd);

	AddDataFrameLength( uDFLen );

	PutDataLength(uSize);

	AddGroup(Addr.a.m_Table, Addr.a.m_Type == addrByteAsByte);

	AddIndex(Addr.a.m_Offset);

	CopyToTx( PBYTE(&BPD), DSZ );
	}

void CBeckhoffTCPDriver::StartFrame(UINT uCmdID)
{
	m_uPtr		= 0;

	BPH.rsv		= 0;

	BPH.ltotal	= 0;

	BPH.ldata	= 0;

	BPH.error	= 0;

	AddNetInfo();

	AddCmd(uCmdID);

	AddState();

	AddInvoke();
	}

void CBeckhoffTCPDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CBeckhoffTCPDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBeckhoffTCPDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBeckhoffTCPDriver::AddReal(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CBeckhoffTCPDriver::SendFrame(void)
{
	UINT uSize  = PutTotalLength();
	UINT uTotal = uSize;

//**/	for( UINT k = 0; k < uSize; k++ ) {

//**/		switch(k) {
//**/			case 0:
//**/			case 6:
//**/			case 14:
//**/			case 22:
//**/			case 26:
//**/			case 38:
//**/			case 46: AfxTrace0("\r\n"); break;
//**/			case 42: AfxTrace0("___");    break;
//**/			}
//**/		AfxTrace1("[%2.2x]", m_bTx[k]);
//**/		}
			
	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == uTotal ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	UINT uTotal = 0;

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/			for( UINT k = 0; k < uSize; k++ ) {
//**/				switch( k ) {
//**/					case 6:  AfxTrace0("\r\nIP1 ");  break;
//**/					case 14: AfxTrace0("\r\nIP2 ");  break;
//**/					case 22: AfxTrace0("\r\nCMD ");  break;
//**/					case 26: AfxTrace0("\r\nLEN ");  break;
//**/					case 30: AfxTrace0("\r\nE01 ");  break;
//**/					case 38: AfxTrace0("\r\nERR ");  break;
//**/					case 42: AfxTrace0("\r\nDLEN "); break;
//**/					case 46: AfxTrace0("\r\nDATA "); break;
//**/					}
//**/				AfxTrace1("<%2.2x>", m_bRx[k] );
//**/				}

			uPtr += uSize;

			if( !uTotal ) {

				if( uPtr >= 6 ) {

					uTotal = GetRxLong(2, FALSE) + 6;
					}
				}

			if( uTotal ) {

				if( uPtr >= uTotal ) return TRUE;
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::Transact(void)
{
	if( SendFrame() && RecvFrame() ) return NoError();

	CloseSocket(TRUE);

	return FALSE;
	}

CCODE CBeckhoffTCPDriver::CheckFrame(UINT uCount)
{
	if( GetRxWord(B_STATE) != (S_ADS | S_RSP) ) return NORETRY;

	if( MisCompare( B_TARGETID, B_SOURCEID, TRUE ) ) return NORETRY;

	if( MisCompare( B_TARGETPORT, B_SOURCEPORT, FALSE ) ) return NORETRY;

	if( MisCompare( B_SOURCEID, B_TARGETID, TRUE ) ) return NORETRY;

	if( MisCompare( B_SOURCEPORT, B_TARGETPORT, FALSE ) ) return NORETRY;

	if( MisCompare( B_INVOKE, B_INVOKE, TRUE ) ) return NORETRY;

	if( MisCompare( B_COMMAND, B_COMMAND, FALSE ) ) return NORETRY;

	DWORD d = IntelToHost(*((PU4)&m_bRx[B_DATA_ERR]));

	if( d ) {

		m_dErrValue = d;

		m_dErrItem  = IntelToHost(*((PU2)&m_bTx[B_DATA])) << 16;

		m_dErrItem += IntelToHost(*((PU2)&m_bTx[B_DATA+4]));

		return NORETRY;
		}

	d = *((PU4)&m_bRx[B_DATALEN]); // byte ordering doesn't matter

	if( !d ) return NORETRY;

	return uCount;
	}

BOOL CBeckhoffTCPDriver::MisCompare(UINT u1, UINT u2, BOOL fIsDWORD )
{
	if( fIsDWORD ) return (BOOL)(*((PU4)(&m_bRx[u1])) != *((PU4)(&m_bTx[u2])));

	return (BOOL)(*((PU2)(&m_bRx[u1])) != *((PU2)(&m_bTx[u2])));
	}

// Read Handlers

BOOL CBeckhoffTCPDriver::DoBitRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table == addrNamed ) return FALSE;

	PutRWHeader( Addr, CID_READ, *pCount, FIXEDRWSIZE );
	
	if( Transact() ) {

		UINT uCt = min( *pCount, LOWORD(GetRxLong(B_DATALEN, FALSE)) );

		for( UINT n = 0; n < uCt; n++ ) {
			
			pData[n] = m_bRx[B_DATAPOS + n] ? 1 : 0;
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::DoByteRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	if( Addr.a.m_Table == addrNamed ) return FALSE;

	PutRWHeader( Addr, CID_READ, *pCount, FIXEDRWSIZE );
	
	if( Transact() ) {

		UINT uCt = min( *pCount, LOWORD(GetRxLong(B_DATALEN, FALSE)) );

		for( UINT n = 0; n < uCt; n++ ) {
			
			pData[n] = LONG(m_bRx[B_DATAPOS + n]);
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::DoWordRead(AREF Addr, PDWORD pData, UINT * pCount)
{
	UINT uOffset  = Addr.a.m_Offset;
	UINT uItemPos = B_ADSSTATE;

	switch( Addr.a.m_Table ) {
	
		case SP_MEMBYTE:
		case SP_INPUTS:
		case SP_OUTPUTS:

			PutRWHeader( Addr, CID_READ, *pCount + 1, FIXEDRWSIZE );

			break;
			
		case addrNamed:

			switch( uOffset ) {

				case SP_DEVSTATE:

					uItemPos = B_DEVSTATE; // then fall through

				case SP_ADSSTATE:
				
					StartFrame(CID_RSTATE);

					AddDataFrameLength(0);

					memcpy(m_bTx, &BPH, HSZ);

					m_uPtr = HSZ;

					break;

				case SP_WRCADS:

					*pData  = DWORD( m_pCtx->m_WRCADS );

					*pCount = 1;

					return TRUE;

				case SP_WRCDEV:

					*pData  = DWORD( m_pCtx->m_WRCDEV );

					*pCount = 1;

					return TRUE;

				default:
					return FALSE;
				}

			break;
			
		default:
			return FALSE;
		}
	
	if( Transact() ) {

		if( Addr.a.m_Table == addrNamed ) {

			pData[0] = LONG(SHORT(GetRxWord(uItemPos)));

			*pCount  = 1;

			return TRUE;
			}

		UINT uCt = min( *pCount, GetRxLong(B_DATALEN, FALSE) - 1 );
		
		for( UINT n = 0; n < uCt; n++ ) {
			
			pData[n] = LONG(SHORT(GetRxWord(B_DATAPOS + n )));
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::DoLongRead(AREF Addr, PDWORD pData, UINT * pCount, BOOL fIsReal)
{
	if( Addr.a.m_Table == addrNamed ) return FALSE;

	PutRWHeader( Addr, CID_READ, *pCount + 3, FIXEDRWSIZE );
	
	if( Transact() ) {

		UINT uCt = min( *pCount, GetRxLong(B_DATALEN, FALSE) - 3 );
		
		for( UINT n = 0; n < uCt; n++ ) {
		
			pData[n] = GetRxLong(B_DATAPOS + n, fIsReal );
			}

		*pCount = uCt;

		return TRUE;
		}

	return FALSE;
	}

// Write Handlers

BOOL CBeckhoffTCPDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == addrNamed ) {

		if( Addr.a.m_Offset == SP_WRCONT ) {

			StartFrame(CID_WCONTROL);

			AddDataFrameLength(FIXEDWCSIZE);

			BPC.ADS = HostToIntel((WORD)m_pCtx->m_WRCADS);
			BPC.DEV = HostToIntel((WORD)m_pCtx->m_WRCDEV);
			BPC.length = 0;

			CopyToTx( PBYTE(&BPC), WSZ );
			}

		else return FALSE;
		}

	else {
		PutRWHeader( Addr, CID_WRITE, uCount, FIXEDRWSIZE + uCount );

		for( UINT i = 0; i < uCount; i++ ) {

			AddByte( pData[i] ? 1 : 0 );
			}
		}

	return Transact();
	}

BOOL CBeckhoffTCPDriver::DoByteWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	PutRWHeader( Addr, CID_WRITE, uCount, FIXEDRWSIZE + uCount );

	for( UINT i = 0; i < uCount; i++ ) {

		AddByte( LOBYTE(pData[i]) );
		}

	return Transact();
	}

BOOL CBeckhoffTCPDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	UINT i = 0;

	UINT uDataBytes = uCount * sizeof(WORD);

	switch( Addr.a.m_Table ) {

		case SP_MEMBYTE:
		case SP_OUTPUTS:

			PutRWHeader( Addr, CID_WRITE, uDataBytes, FIXEDRWSIZE + uDataBytes );

			for( i = 0; i < uCount; i++ ) {

				AddWord( LOWORD(pData[i]) );
				}

			return Transact();

		case addrNamed:

			switch( uOffset ) {

				case SP_WRCADS:

					m_pCtx->m_WRCADS = *pData;
					break;

				case SP_WRCDEV:
					m_pCtx->m_WRCDEV = *pData;
					break;

				case SP_ADSSTATE:
				case SP_DEVSTATE:
					break;
				}

			return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount, BOOL fIsReal)
{
	UINT uDataBytes = uCount * sizeof(DWORD);

	PutRWHeader( Addr, CID_WRITE, uDataBytes, FIXEDRWSIZE + uDataBytes );

	for( UINT i = 0; i < uCount; i++ ) !fIsReal ? AddLong( pData[i] ) : AddReal( pData[i] );

	return Transact();
	}

// Helpers

void CBeckhoffTCPDriver::AddNetInfo(void)
{
	AddNetID();

	AddPort();
	}

void CBeckhoffTCPDriver::AddNetID(void)
{
	AddNetID( *PDWORD(&m_IP), &(BPH.sid[0]) );

	AddNetID( m_pCtx->m_AMSID, &(BPH.tid[0]) );

	if( m_pCtx->m_AMSSuffix > 1 ) {

		BPH.tid[5] = m_pCtx->m_AMSSuffix;
		}
	}

void CBeckhoffTCPDriver::AddNetID(DWORD dID, PBYTE pDest)
{
	// MTS - 07/30/2012:
	// The protocol seems to expect the ID as a string of bytes in big-endian order.
	// This is different than the rest of the fields, which are little-endian.
	dID = HostToMotor(dID);

	for( UINT i = 0; i < 4; i++ ) {

		*(pDest + i) = HIBYTE(HIWORD(dID));

		dID <<= 8;
		}

	*(pDest + 4) = 1;
	*(pDest + 5) = 1;
  	}

void CBeckhoffTCPDriver::AddPort(void)
{
	BPH.tport = HostToIntel((WORD)m_pCtx->m_AMSPort);
	BPH.sport = HostToIntel((WORD)(m_pCtx->m_AMSPort + 1));
	}

void CBeckhoffTCPDriver::AddCmd(UINT uCmdID)
{
	BPH.cmd = HostToIntel(LOWORD(uCmdID));
	}

void CBeckhoffTCPDriver::AddState(void)
{
	BPH.state = HostToIntel((WORD)S_ADS);
	}

void CBeckhoffTCPDriver::AddInvoke(void)
{
	BPH.invoke = HostToIntel((DWORD)m_pCtx->m_dTrans++);
	}

void CBeckhoffTCPDriver::AddGroup(UINT uTable, BOOL fIsByte)
{
	switch( uTable ) {

		case SP_MEMBYTE:	BPD.group = MEMBYTEADDR; break;
		case SP_INPUTS:		BPD.group = fIsByte ? INPBYTEADDR : INPBITADDR; break;
		case SP_OUTPUTS:	BPD.group = fIsByte ? OUTBYTEADDR : OUTBITADDR; break;
		}
	}

void CBeckhoffTCPDriver::AddIndex(UINT uOffset)
{
	BPD.offset = HostToIntel((DWORD)uOffset);
	}

void CBeckhoffTCPDriver::PutDataLength(UINT uLen)
{
	BPD.length = HostToIntel(DWORD(uLen));
	}

void CBeckhoffTCPDriver::AddDataFrameLength(DWORD dLength)
{
	BPH.ldata  = HostToIntel(dLength);
	}

UINT CBeckhoffTCPDriver::PutTotalLength(void)
{
	*((PU4)&m_bTx[2]) = HostToIntel((DWORD)(m_uPtr - 6));

	return m_uPtr;
	}

DWORD CBeckhoffTCPDriver::GetRxLong(UINT uOffset, BOOL fIsReal)
{
	if( fIsReal ) {

		return (GetRxWord(uOffset) << 16) + GetRxWord(uOffset + 2);
		}

	return IntelToHost(*((PU4)&m_bRx[uOffset]));
	}

WORD CBeckhoffTCPDriver::GetRxWord(UINT uOffset)
{
	return IntelToHost((WORD)(*((PU2)(&m_bRx[uOffset]))));
	}

void CBeckhoffTCPDriver::CopyToTx(PBYTE pSource, UINT uSize)
{
	memcpy(m_bTx, &BPH, HSZ);
	memcpy(m_bTx + HSZ, pSource, uSize);

	m_uPtr = HSZ + uSize;
	}

BOOL CBeckhoffTCPDriver::NoError(void)
{
	PU4 x = (PU4)&m_bRx[B_ERROR];

	return !(BOOL)(*x);
	}

// Socket Management

BOOL CBeckhoffTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CBeckhoffTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					m_pCtx->m_pSock->GetLocal(m_IP);

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}

		}

	return FALSE;
	}

void CBeckhoffTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// End of File
