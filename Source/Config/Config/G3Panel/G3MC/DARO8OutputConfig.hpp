
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DARO8OutputConfig_HPP

#define INCLUDE_DARO8OutputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// DARO8 DO Configuration
//

class CDARO8OutputConfig : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDARO8OutputConfig(void);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Item Properties
		UINT m_Enable1;
		UINT m_Enable2;
		UINT m_Enable3;
		UINT m_Enable4;
		UINT m_Enable5;
		UINT m_Enable6;
		UINT m_Enable7;
		UINT m_Enable8;

		UINT m_Mode1;
		UINT m_Mode2;
		UINT m_Mode3;
		UINT m_Mode4;
		UINT m_Mode5;
		UINT m_Mode6;
		UINT m_Mode7;
		UINT m_Mode8;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
