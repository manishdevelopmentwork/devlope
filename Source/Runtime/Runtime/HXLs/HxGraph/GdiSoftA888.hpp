
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GdiSoft888_HPP
	
#define	INCLUDE_GdiSoft888_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "GdiGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Software Gdi Driver for 32-bit Displays
//

class DLLAPI CGdiSoftA888 : public CGdiGeneric
{	
	public:
		// Constructor
		CGdiSoftA888(void);

		// Destructor
		~CGdiSoftA888(void);

		// Initialization
		static void InitTables(void);
		static void FreeTables(void);

		// IGdi Initialization
		void Create(int cx, int cy, PVOID pData);

		// IGdi Buffer Access
		PVOID GetBuffer(void);

		// IGdi Buffer Rendering
		UINT Render(PBYTE pData, UINT uBits);

		// IGdi Serialization
		void BufferClaim(void);
		void BufferFree (void);

		// IGdi Color Format
		UINT GetColorFormat(void);

		// IGdi Bitmaps
		void BitBlt(int x, int y, int cx, int cy, int s, PCBYTE p, UINT rop);

		// IGdi Text Helpers
		void CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p);
		void CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p, COLOR c1, COLOR c2);
		void CharBlt(int x, int y, int cx, int cy, int s, PCBYTE p, COLOR c1);

	protected:
		// Static Data
		static DWORD const m_bBitMask[8];

		// Lookup Tables
		static PDWORD m_pGray;
		static PBYTE  m_pBlend;
		static PBYTE  m_pEight;

		// Locking
		IMutex * m_pLock;

		// Data Members
		PDWORD m_pData;
		BOOL   m_fData;

		// Glyph Buffer
		DWORD m_Glyph[10240];

		// Font Blending
		DWORD m_pm[16];
		PBYTE m_p1;
		PBYTE m_p2;
		PBYTE m_p3;
		PBYTE m_p4;
		WORD  m_c1;
		WORD  m_c2;
		DWORD m_r1;
		DWORD m_r2;

		// Drawing Context
		BYTE  m_bBrush[8];
		DWORD m_dwFontFore;
		DWORD m_dwFontBack;
		DWORD m_dwBrushFore;
		DWORD m_dwBrushBack;
		DWORD m_dwBrushColor;
		DWORD m_dwPenFore;
		DWORD m_dwPenBack;

		// Area Fill Support
		void FillArea(int x1, int y1, int x2, int y2);

		// Alpha Blending
		void BlendHorz(int yd);
		void BlendVert(int xd);

		// Line Drawing Support
		void LineFill(int x, int y, int cx, int cy, BOOL fFore);
		void LineSet (int x, int y, BOOL fFore);
		void LineCap (int l, int r, int y);

		// Low-Level Output
		void IntSetPixel(int x, int y, COLOR Color);
		void IntWriteRun(int x, int y, int n, DWORD c);
		void IntWriteRun(PDWORD p, int n, DWORD c);
		void IntWriteBits(int x, int y, int n);

		// Tool Realization
		void OnNewFont (void);
		void OnNewBrush(void);
		void OnNewPen  (void);

		// Glyph Expansion
		bool ExpandGlyph(int cx, int cy, int &k, PCBYTE &p);

		// Patterned Brushes
		void GetBrushData(void);

		// Table Management
		static void MakeGrayTable(void);
		static void LoadGrayTable(void);
		static void FreeGrayTable(void);
		static void MakeBlendTable(double gamma);
		static void LoadBlendTable(double gamma);
		static void FreeBlendTable(void);
		static void MakeEightTable(void);
		static void LoadEightBasic(void);
		static void LoadEightExact(void);
		static void FreeEightTable(void);

		// Blend Table Access
		void GetBlendBase(DWORD &c, PBYTE &p1, PBYTE &p2, PBYTE &p3, PBYTE &p4);
		void GetBlendBase(DWORD &c, PBYTE &p1, PBYTE &p2, PBYTE &p3, PBYTE &p4, BOOL fFore);
		void GetBlendBase(PBYTE &p1, PBYTE &p2, PBYTE &p3, PBYTE &p4, DWORD c);
		
		// Conversion
		static DWORD To888(WORD a, WORD c);
		static DWORD To888(WORD c);
		static WORD  To555(DWORD d);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE DWORD CGdiSoftA888::To888(WORD a, WORD c)
{
	DWORD  x = ((c & (31U<<0)) << 19) | ((c & (31U<<5)) << 6) | ((c & (31U<<10)) >> 7);

	return x | ((x >> 5) & 0x070707) | (a << 24);
	}

STRONG_INLINE DWORD CGdiSoftA888::To888(WORD c)
{
	return To888(255, c);
	}

STRONG_INLINE WORD CGdiSoftA888::To555(DWORD d)
{
	return ((d >> 3) & (31U<<0)) | ((d >> 6) & (31U<<5)) | ((d >> 9) & (31U<<10));
	}

// Blend Table Access

STRONG_INLINE void CGdiSoftA888::GetBlendBase(DWORD &c, PBYTE &p1, PBYTE &p2, PBYTE &p3, PBYTE &p4)
{
	GetBlendBase(p1, p2, p3, p4, c = m_dwBrushColor);
	}

STRONG_INLINE void CGdiSoftA888::GetBlendBase(DWORD &c, PBYTE &p1, PBYTE &p2, PBYTE &p3, PBYTE &p4, BOOL fFore)
{
	GetBlendBase(p1, p2, p3, p4, c = (fFore ? m_dwBrushFore : m_dwBrushBack));
	}

STRONG_INLINE void CGdiSoftA888::GetBlendBase(PBYTE &p1, PBYTE &p2, PBYTE &p3, PBYTE &p4, DWORD c)
{
	// For a given foreground color, return a pointer into the blend
	// table for each element. We can index from this pointer by
	// 32 times the blend value plus the background color channel to
	// get the resulting output value.

	c >>= 3; p1 = m_pBlend + ((c & 31) << 9);
	c >>= 8; p2 = m_pBlend + ((c & 31) << 9);
	c >>= 8; p3 = m_pBlend + ((c & 31) << 9);
	c >>= 8; p4 = m_pBlend + ((c & 31) << 9);
	}

// Low-Level Output

STRONG_INLINE void CGdiSoftA888::IntSetPixel(int x, int y, COLOR Color)
{
	m_pData[x + y * m_cx] = To888(Color);
	}

STRONG_INLINE void CGdiSoftA888::IntWriteRun(int x, int y, int n, DWORD c)
{
	IntWriteRun(m_pData + x + y * m_cx, n, c);
	}

STRONG_INLINE void CGdiSoftA888::IntWriteRun(PDWORD p, int n, DWORD c)
{
	if( n ) {

		if( n >= 8 ) {

			while( (DWORD(p) & 15) && n-- ) {

				*p++ = c;
				}

			#if !defined(__INTELLISENSE__) && defined(_M_ARM)

			ASM(	"	mov	r4,  %2		\n"
				"	mov	r5,  %2		\n"
				"	mov	r6,  %2		\n"
				"	mov	r7,  %2		\n"
				"1:	stmia	%0!, {r4-r7}	\n"
				"	sub	%1,  #4		\n"
				"	cmp	%1,  #4		\n"
				"	bge	1b		\n"
				: "+r"(p), "+r"(n)
				: "r"(c)
				: "r4", "r5", "r6", "r7"
				);

			#else

			while( n >= 4 ) {

				*p++ = c;
				*p++ = c;
				*p++ = c;
				*p++ = c;

				n -= 4;
				}

			#endif
			}

		while( n-- ) {

			*p++ = c;
			}
		}
	}

STRONG_INLINE void CGdiSoftA888::IntWriteBits(int x, int y, int n)
{
	if( n ) {

		PDWORD pd = m_pData + x + y * m_cx;

		PDWORD pf = PDWORD(m_Brush.m_Bits);

		int    bx = x % m_Brush.m_bcx;

		int    by = y % m_Brush.m_bcy;

		pf += by * m_Brush.m_bcx;

		while( n ) {

			int m = Min(n, m_Brush.m_bcx - bx);

			memcpy(pd, pf + bx, 4 * m);

			bx = 0;

			pd = pd + m;

			n  = n  - m;
			}

		}
	}

// End of File

#endif
