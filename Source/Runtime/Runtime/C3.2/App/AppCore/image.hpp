
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Image File Loader
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMAGE_HPP

#define INCLUDE_IMAGE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Image File Structures
//

struct CImageHeader
{
	DWORD	m_dwMagic;
	WORD	m_wVersion;
	WORD	m_wFlags;
	WORD	m_wTargCount;
	WORD	m_wFileCount;
	WORD	m_wDbase;
	BYTE	m_bPad[18];
	char	m_sOEM[32];
	};

struct CImageTargRecord
{
	char	m_sName[16];
	WORD	m_wFile[8];
	};

struct CImageFileRecord
{
	char	m_sName[8];
	DWORD	m_dwPos;
	DWORD	m_dwSize;
	BYTE	m_bGUID[16];
	};

//////////////////////////////////////////////////////////////////////////
//
// Checksum Support
//

extern	DWORD	CRC32_GetValue(void);
extern	void	CRC32_Clear(void); 
extern  void	CRC32_AddData(PCBYTE pData, UINT uSize);

// End of File

#endif
