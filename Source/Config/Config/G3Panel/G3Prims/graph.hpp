
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_GRAPH_HPP
	
#define	INCLUDE_GRAPH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "legacy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyBarGraph;
class CPrimLegacyVertBarGraph;
class CPrimLegacyHorzBarGraph;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar Graph
//

class CPrimLegacyBarGraph : public CPrimLegacyFigure
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyBarGraph(void);

		// Initial Values
		void SetInitValues(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem   * m_pCount;
		CCodedItem   * m_pValue;
		CCodedItem   * m_pMin;
		CCodedItem   * m_pMax;
		CPrimBrush   * m_pFill;

	protected:
		// Meta Data
		void AddMetaData(void);
		
		// Implementation
		void DrawBack(IGDI *pGDI, R2 &Rect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar Graph
//

class CPrimLegacyVertBarGraph : public CPrimLegacyBarGraph
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyVertBarGraph(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar Graph
//

class CPrimLegacyHorzBarGraph : public CPrimLegacyBarGraph
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyHorzBarGraph(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacyScatterGraph;
class CPrimLegacyGraphLine;
class CGraphLineLimits;

//////////////////////////////////////////////////////////////////////////
//
// Graph Line Limits
//

class CGraphLineLimits : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGraphLineLimits(void);

		// Attributes
		C3REAL GetMinX(void);
		C3REAL GetMaxX(void);
		C3REAL GetMinY(void);
		C3REAL GetMaxY(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pMinX;
		CCodedItem * m_pMaxX;
		CCodedItem * m_pMinY;
		CCodedItem * m_pMaxY;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Graph Line
//

class CPrimLegacyGraphLine : public CCodedHost
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimLegacyGraphLine(UINT uChan);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Initial Values
		void SetInitValues(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		void Draw(IGDI *pGDI, R2 Rect);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Item Properties
		CCodedItem       * m_pValueX;
		CCodedItem       * m_pValueY;
		CGraphLineLimits * m_pLimits;
		CCodedItem       * m_pCount;
		UINT	           m_Style;
		UINT	           m_Fit;
		CPrimColor       * m_pDataFill;
		CPrimColor       * m_pDataLine;
		CPrimColor       * m_pBestLine;

	protected:
		// Data Members
		UINT m_uChan;
		BOOL m_fXAutoLimits;
		BOOL m_fYAutoLimits;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		BOOL HasXAutoLimits(void);
		BOOL HasYAutoLimits(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Scatter Graph
//

class CPrimLegacyScatterGraph : public CPrimLegacyFigure
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLegacyScatterGraph(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CPrimLegacyGraphLine * m_pLines[4];
		UINT                   m_GridType;
		CPrimColor           * m_pVertCol;
		CPrimColor           * m_pHorzCol;
		UINT		       m_GridFont;
		UINT		       m_HorzGap;
		UINT                   m_VertPrecise;
		UINT                   m_HorzPrecise;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		void LoadLinePages(CUIPageList *pList);
		void DrawGrid(IGDI *pGDI, R2 Rect);
		
		// Limits Access
		C3REAL GetMinX(void);
		C3REAL GetMaxX(void);
		C3REAL GetMinY(void);
		C3REAL GetMaxY(void);
	};

// End of File

#endif
