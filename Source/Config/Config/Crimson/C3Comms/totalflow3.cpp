
#include "intern.hpp"

#include "totalflow3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlow3MasterDeviceOptions, CTotalFlow2MasterDeviceOptions);

// Constructor

CTotalFlow3MasterDeviceOptions::CTotalFlow3MasterDeviceOptions(void)
{
	m_uStep   = NOTHING;

	m_fUpdate = FALSE;

	EmptySlots();
	}

// Download Support

BOOL CTotalFlow3MasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlow2MasterDeviceOptions::MakeInitData(Init);

	Init.AddByte(BYTE(m_StringSize));
	
	return TRUE;
	}

// Slot Overridables

BOOL CTotalFlow3MasterDeviceOptions::GetSlot(UINT uAddr, CTotalFlowSlot &Slot)
{
	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i) ) {
				
		if( m_Slots.GetAt(i).m_Slot == uAddr ) {

			Slot = m_Slots.GetAt(i);

			return TRUE;
			}
		}

	return FALSE;
	}


UINT CTotalFlow3MasterDeviceOptions::AddSlot(CTotalFlowSlot &Slot, UINT uStart, UINT uEnd)
{
	UINT uSlot = FindSlot(Slot);

	if( uSlot < NOTHING ) {

		return uSlot;
		}

	if( Slot.m_Size == 0 ) {

		Slot.m_Size = Slot.m_IsString ? 16 : 1;
		}

	if( uStart >= addrNamed ) {

		uStart = 1;

		uEnd   = addrNamed - 1;
		}
	
	for( UINT u = uStart; u < uEnd; u++ ) {

		if( AddSlot(Slot, u) ) {

			return Slot.m_Slot;
			}
		}

	return NOTHING;
	}

void CTotalFlow3MasterDeviceOptions::DeleteSlot(CTotalFlowSlot &Slot)
{
	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i) ) {

		if( m_Slots.GetAt(i).m_Slot == Slot.m_Slot ) {

			m_Slots.Remove(i);

			return;
			}
		}	
	}

void CTotalFlow3MasterDeviceOptions::EmptySlots(void)
{
	if( !m_fUpdate ) {

		memset((PBYTE)&m_bDef, 0xFF, elements(m_bDef) * sizeof(BYTE ));

		memset((PBYTE)&m_uRef, 0xFF, elements(m_uRef) * sizeof(DWORD));

		for( UINT d = 0; d < tdMax; d++ ) {

			m_uRef[d] = GetReference(d + 1, TableDefinitionToType(d), d == tdString);

			m_bDef[d] = BYTE(d);
			}

		m_Slots.Empty();
		}
	}

UINT CTotalFlow3MasterDeviceOptions::GetSlotSize(CTotalFlowSlot const &Slot)
{
	return Slot.m_Size;
	}

// Slot Access

UINT CTotalFlow3MasterDeviceOptions::FindSlot(CTotalFlowSlot Slot)
{	
	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i) ) {

		CTotalFlowSlot Index = m_Slots.GetAt(i);

		if( Index == Slot ) {

			return Index.m_Slot;
			}
		}

	return NOTHING;
	}

void CTotalFlow3MasterDeviceOptions::UpdateSlots(CAddress Addr, INT nSize)
{
	if( !m_fUpdate ) {

		CSystemItem *pSystem = GetDatabase()->GetSystemItem();

		if( pSystem ) {

			for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i) ) {

				CTotalFlowSlot Slot = m_Slots.GetAt(i);

				if( Addr.m_Ref == Slot.m_Slot ) {

					UINT uSize = nSize;

					if( uSize > Slot.m_Size ) {

						m_fUpdate = !IsLastReference(Slot);

						Slot.m_Size = uSize;

						m_Slots.Remove(i);

						if( !SetReferences(Slot, Addr) ) {

							AddSlot(Slot, 1, addrNamed - 1);

							m_fUpdate = TRUE;
							}

						if( m_fUpdate ) {

							pSystem->Rebuild(1);

							m_fUpdate = FALSE;
							}
						}
					
					return;
					}
				}
			}
		}
	}

BOOL CTotalFlow3MasterDeviceOptions::MakeNextSlot(CAddress Addr)
{
	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i)) {

		CTotalFlowSlot Slot = m_Slots.GetAt(i);

		UINT uSize = Slot.m_Size;

		if( Slot.m_Slot + uSize == Addr.m_Ref ) {

			CTotalFlowSlot Next = Slot;

			Next.m_Slot  = Addr.m_Ref;

			Next.m_Size  = uSize;

			Next.m_Reg  += 1;

			if( Next.m_Reg > Slot.m_Reg ) {

				if( AddSlot(Next, Addr.a.m_Table, addrNamed - 1) < NOTHING ) {

					m_uStep = Addr.m_Ref;

					return TRUE;
					}
				}

			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CTotalFlow3MasterDeviceOptions::FindPrevSlot(CAddress Addr, CTotalFlowSlot &Prev)
{
	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i)) {

		CTotalFlowSlot Slot = m_Slots.GetAt(i);

		if( Slot.m_Slot + Slot.m_Size == Addr.m_Ref ) {

			Prev = m_Slots.GetAt(i);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTotalFlow3MasterDeviceOptions::ExpandAddress(CString &Text, CAddress const Addr)
{
	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i)) {

		CTotalFlowSlot Slot = m_Slots.GetAt(i);

		if( Slot.m_Slot <= Addr.m_Ref && Slot.m_Slot + Slot.m_Size >= Addr.m_Ref ) {

			Slot.m_Reg += Addr.m_Ref - Slot.m_Slot;

			return Expand(Slot, Addr, Text);
			}
		}

	return FALSE;
	}

// Implementation

BOOL CTotalFlow3MasterDeviceOptions::AddSlot(CTotalFlowSlot &Slot, UINT uTable)
{
	if( IsTypeMatch(Slot, uTable) ) {

		return SetSlot(Slot, uTable);
		}
	
	return FALSE;
	}

BOOL CTotalFlow3MasterDeviceOptions::SetSlot(CTotalFlowSlot &Slot, UINT uTable)
{
	if( !SetThis(Slot, uTable) ) {

		return SetNext(Slot, uTable + 1);
		}

	return TRUE;
	}

BOOL CTotalFlow3MasterDeviceOptions::SetThis(CTotalFlowSlot &Slot, UINT uTable)
{
	CAddress Addr;

	UINT uDef  = GetTableDefinition(Slot);

	Addr.m_Ref = m_uRef[uDef];

	return SetReferences(Slot, Addr);
	}

BOOL CTotalFlow3MasterDeviceOptions::SetNext(CTotalFlowSlot &Slot, UINT uTable)
{
	if( uTable ) {

		for( UINT d = uTable; d < elements(m_bDef); d++ ) {

			if( m_bDef[d - 1] == 0xFF ) {

				CAddress Addr;

				m_bDef[d - 1]  = BYTE(GetTableDefinition(Slot));

				Addr.m_Ref     = GetReference(d, Slot.m_Type, Slot.m_IsString);
			
				return SetReferences(Slot, Addr);
				}
			}
		}

	return FALSE;
	}

BOOL CTotalFlow3MasterDeviceOptions::SetReferences(CTotalFlowSlot &Slot, CAddress Addr)
{
	if( Addr.a.m_Offset + Slot.m_Size < 0xFFFF ) {

		Slot.m_Slot  = Addr.m_Ref;

		SetReference(Slot);

		m_Slots.Append(Slot);

		return TRUE;
		}

	return FALSE;
	}

void  CTotalFlow3MasterDeviceOptions::SetReference(CTotalFlowSlot Slot)
{
	UINT uDef    = GetTableDefinition(Slot);

	m_uRef[uDef] = Slot.m_Slot + Slot.m_Size;
	}

DWORD CTotalFlow3MasterDeviceOptions::GetReference(UINT uTable, UINT uType, BOOL fString)
{
	CAddress Addr;

	Addr.m_Ref     = 0;

	Addr.a.m_Table = uTable & 0xFF;

	Addr.a.m_Type  = uType  & 0xF;

	Addr.a.m_Extra = fString ? 1 : 0;

	return Addr.m_Ref;
	}

// Helpers

BOOL CTotalFlow3MasterDeviceOptions::IsTypeMatch(CTotalFlowSlot Slot, UINT uTable)
{
	UINT uDef = GetTableDefinition(uTable);

	if( uDef < NOTHING ) {

		return TableDefinitionToType(uDef) == Slot.m_Type;
		}

	return FALSE;
	}

BOOL CTotalFlow3MasterDeviceOptions::IsLastReference(CTotalFlowSlot Slot)
{
	CAddress Addr;

	Addr.m_Ref = Slot.m_Slot;

	UINT uDef  = GetTableDefinition(Addr.a.m_Table);

	if( uDef < NOTHING ) {

		if( uDef < elements(m_uRef) ) {
			
			return Slot.m_Slot == m_uRef[uDef] - Slot.m_Size;
			}
		}

	return FALSE;
	}

UINT CTotalFlow3MasterDeviceOptions::GetTableDefinition(CTotalFlowSlot Slot)
{
	switch( Slot.m_Type ) {

		case addrByteAsByte:	return tdByte;

		case addrRealAsReal:	return tdReal;

		case addrLongAsLong:	return Slot.m_IsString ? tdString : tdLong;
		};

	return tdWord;
	}

UINT CTotalFlow3MasterDeviceOptions::GetTableDefinition(UINT uTable)
{
	if( uTable && uTable < elements(m_bDef) ) {

		return m_bDef[uTable - 1];
		}

	return NOTHING;
	}

UINT CTotalFlow3MasterDeviceOptions::TableDefinitionToType(UINT uDef)
{
	switch( uDef ) {

		case tdByte:		return addrByteAsByte;

		case tdReal:		return addrRealAsReal;

		case tdLong:	
		case tdString:		return addrLongAsLong;
		};

	return addrWordAsWord;
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlow3SerialMasterDeviceOptions, CTotalFlow3MasterDeviceOptions);

// Constructor

CTotalFlow3SerialMasterDeviceOptions::CTotalFlow3SerialMasterDeviceOptions(void)
{
	m_Estab = 1;

	m_Time1 = 2000;

	m_UnkeyDelay = 50;
	}

// Download Support

BOOL CTotalFlow3SerialMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlow3MasterDeviceOptions::MakeInitData(Init);

	Init.AddWord(WORD(m_Estab));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_UnkeyDelay));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlow3SerialMasterDeviceOptions::AddMetaData(void)
{
	CTotalFlow3MasterDeviceOptions::AddMetaData();
	
	Meta_AddInteger(Time1);
	Meta_AddInteger(Estab);
	Meta_AddInteger(UnkeyDelay);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 TCP/IP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlow3TcpMasterDeviceOptions, CTotalFlow3MasterDeviceOptions);

// Constructor

CTotalFlow3TcpMasterDeviceOptions::CTotalFlow3TcpMasterDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Port   = 9999;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CTotalFlow3TcpMasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Keep" ) {

		pHost->EnableUI("Time3", !m_Keep);
		}

	CTotalFlow3MasterDeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CTotalFlow3TcpMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlow3MasterDeviceOptions::MakeInitData(Init);

	Init.AddLong(m_IP);
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlow3TcpMasterDeviceOptions::AddMetaData(void)
{
	CTotalFlow3MasterDeviceOptions::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Master Comms Driver
//

// Constructor

CTotalFlow3MasterDriver::CTotalFlow3MasterDriver(void)
{
	m_uType		= driverMaster;

	m_Version	= "3.00";

	m_ShortName	= "TotalFlow 3.0";
	}

// Notifications

void CTotalFlow3MasterDriver::NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig)
{
	CTotalFlow3MasterDeviceOptions *pDevice = (CTotalFlow3MasterDeviceOptions *) pConfig;

	if( pDevice ) {

		pDevice->UpdateSlots(Addr, nSize);
		}
	}

// Address Management

BOOL CTotalFlow3MasterDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CTotalFlow3MasterDeviceOptions *pDevice = (CTotalFlow3MasterDeviceOptions *) pConfig;

	CStringArray Part;

	Text.Tokenize(Part, '.');

	UINT uType = FindType(Part, Error);

	if( uType < NOTHING ) {

		CTotalFlowSlot Slot;

		Slot.m_IsString =  Part.GetCount() == 5 && Part[3] == L"S";

		Slot.m_App      = pDevice->ParseApp(Part[0]);

		Slot.m_Arr      = watoi(Part[1]);

		Slot.m_Reg      = watoi(Part[2]);

		Slot.m_Size     = 0;

		Slot.m_Slot	= NOTHING;

		if( CheckBounds(Slot.m_App, Slot.m_Arr, Slot.m_Reg) ) {

			Slot.m_Type = uType;

			UINT uRef = pDevice->AddSlot(Slot, 1, addrNamed - 1);

			if( uRef < NOTHING ) {	

				Addr.m_Ref = uRef;

				return TRUE;
				}

			Error.Set(CString(IDS_THIS_DEVICE));

			return FALSE;
			}
		else {
			Error.Set(CString(IDS_ADDRESS_OUT_OF));
				
			return FALSE;
			}
		}

	return FALSE;
	}

BOOL CTotalFlow3MasterDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( !CTotalFlow2MasterDriver::ExpandAddress(Text, pConfig, Addr) ) {

		CTotalFlow3MasterDeviceOptions * pDevice = (CTotalFlow3MasterDeviceOptions *) pConfig;

		return pDevice->ExpandAddress(Text, Addr);
		}

	return TRUE;
	}

// Address Reference
		
UINT CTotalFlow3MasterDriver::GetAddress(CAddress Addr)
{	
	return Addr.m_Ref;
	}

// Implementation

UINT CTotalFlow3MasterDriver::FindType(CStringArray Array, CError &Error)
{
	if( Array.GetCount() == 3 ) {

		return addrRealAsReal;
		}

	if( Array.GetCount() == 5 ) {

		return addrLongAsLong;
		}

	if( Array.GetCount() == 4 ) {

		if( Array[3].GetLength() == 1 ) {

			switch( Array[3].ToUpper()[0] ) {

				case 'B':

					return addrByteAsByte;

				case 'W':

					return addrWordAsWord;

				case 'L':

					return addrLongAsLong;

				case 'R':

					return addrRealAsReal;

				case 'S':

					Error.Set(CString(IDS_STRING_REFERENCES));

					return NOTHING;

				default:
					Error.Set(CString(IDS_TYPE_SUFFIX_MUST));

					return NOTHING;
				}

			}
		}

	Error.Set(CString(IDS_INVALID_ADDRESS));

	return NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Master Comms Driver
//

// Instantiator

ICommsDriver * Create_TotalFlow3SerialMasterDriver(void)
{
	return New CTotalFlow3SerialMasterDriver();
	}

// Constructor

CTotalFlow3SerialMasterDriver::CTotalFlow3SerialMasterDriver(void)
{
	m_DriverName = "TotalFlow Enhanced Serial Master 3.0";

	m_wID        = 0x40DE;
	}

// Binding

UINT CTotalFlow3SerialMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTotalFlow3SerialMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CTotalFlow3SerialMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTotalFlow3SerialMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 TCP/IP Master Comms Driver
//

// Instantiator

ICommsDriver * Create_TotalFlow3TcpMasterDriver(void)
{	
	return New CTotalFlow3TcpMasterDriver();
	}

// Constructor

CTotalFlow3TcpMasterDriver::CTotalFlow3TcpMasterDriver(void)
{
	m_DriverName = "TotalFlow Enhanced TCP/IP Master 3.0";

	m_wID        = 0x40DF;
	}

// Binding

UINT CTotalFlow3TcpMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CTotalFlow3TcpMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount  = 1;

	Ether.m_UDPCount  = 0;
	}

// Configuration

CLASS CTotalFlow3TcpMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTotalFlow3TcpMasterDeviceOptions);
	}

// End of File
