
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAPID2MainWnd_HPP

#define INCLUDE_DAPID2MainWnd_HPP

#include "dlcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAPID2Module;

//////////////////////////////////////////////////////////////////////////
//
// DADIDO Module Window
//

class CDAPID2MainWnd : public CDLCMainWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAPID2MainWnd(void);

	// Overribables
	void OnAttach(void);

protected:
	// Implementation
	void AddIdentPage(void);
};

// End of File

#endif
