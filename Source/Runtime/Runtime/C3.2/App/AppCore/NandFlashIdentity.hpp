
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_NandFlashIdentity_HPP

#define	INCLUDE_NandFlashIdentity_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Nand Flash Identity Manager
//

class CNandFlashIdentity : public CNandClient,
			   public ICrimsonIdentity
{
	public:
		// Constructor
		CNandFlashIdentity(CNandBlock const &Start, CNandBlock const &End);

		// Destructor
		~CNandFlashIdentity(void);

		// ICrimsonIdentity
		void Init(void);
		int  GetOemName(PTXT   pName, UINT uSize);
		int  GetOemApp (PTXT   pName, UINT uSize);
		bool GetKeyData(PBYTE  pData, UINT uSize);
		bool GetSerial (PTXT   pName, UINT uSize);
		bool SetOemName(PCTXT  pName, UINT uSize);
		bool SetOemApp (PCTXT  pName, UINT uSize);
		bool SetKeyData(PCBYTE pData, UINT uSize);
		bool SetSerial (PCTXT  pName, UINT uSize);

	protected:
		// Constants
		static DWORD const magicOem	 = 0x004F454D;
		static DWORD const magicEmpty	 = 0xFFFFFFFF;

		// Page Header
		struct CPageHeader
		{
			DWORD		Magic;
			DWORD		Generation;
			};

		// Property Page
		struct CPropsPage
		{
			CPageHeader	Page;
			char		App   [ 64];
			char		Name  [ 64];			
			BYTE		Key   [128];	
			char            Serial[128];
			};

		// Type Definitions
		typedef	CPageHeader	    * PPAGE;
		typedef	CPageHeader   const * PCPAGE;
		typedef	CPropsPage          * PPROPS;
		typedef	CPropsPage    const * PCPROPS;

		// Data
		CNandPage	m_PageProps;
		UINT		m_uPropsGen;
		char		m_App   [ 64];
		char		m_Name  [ 64];
		char            m_Serial[ 32];
		BYTE		m_Key   [128];

		// Implementation
		bool FindItems(void);
		bool CommitPropPage(void);

		// Overridables
		bool OnPageReloc(PCBYTE pData, CNandPage const &Dest);
	};

// End of File

#endif

