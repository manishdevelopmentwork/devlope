
#ifndef	INCLUDE_ABL5K_HPP

#define	INCLUDE_ABL5K_HPP

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - EIP Master
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Interface
//

#include "imeip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CLogixData;

/////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABL5kDriver;

//////////////////////////////////////////////////////////////////////////
//
// Address Decoding Macros
//

#define	AddrIsTable(addr)	((addr).a.m_Table < addrNamed)

#define	AddrIsNamed(addr)	((addr).a.m_Table == addrNamed)

#define AddrTagIndex(addr)	(AddrIsTable(addr) ? (addr).a.m_Table : (addr).a.m_Offset)

#define AddrDataIndex(addr)	(AddrIsTable(addr) ? (addr).a.m_Offset : 0)

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - Tag Name List
//

class CNameList
{
	public:
		// Constructor
		CNameList(void);

		// Destructor
		~CNameList(void);

		// Configuration
		void Load(LPCBYTE &pData, CABL5kDriver &Driver);

		// Attributes		
		CLogixData * FindObject(UINT uIndex);

	protected:
		// Data
		UINT	      m_uCount;
		CLogixData ** m_pList;
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - EIP Master Driver
//

class CABL5kDriver : public CMasterDriver, public IEIPMaster
{
	public:
		// Constructor
		CABL5kDriver(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// IEIPMaster
		DEFMETH(BOOL ) Send(BYTE bCode, CCIPPath &Path, CDataBuf &Data);
		DEFMETH(BOOL ) Send(BYTE bCode, CCIPPath &Path, WORD wClass, WORD wInst, PBYTE pData, UINT uSize);
		DEFMETH(BOOL ) Recv(CBuffer * &pBuff);
		DEFMETH(BYTE ) GetLastError(void);
		DEFMETH(void ) Dump(CBuffer *pBuff);
		DEFMETH(void ) Dump(PBYTE pData, UINT uSize);
		DEFMETH(void ) Trace(LPCTXT pText, ...);

		// Text Loading
		PTXT GetString(PCBYTE &pData);

	protected:
		// Device Data
		struct CContext
		{
			DWORD		m_IP;			
			WORD		m_wPort;
			WORD		m_wSlot;
			PCTXT		m_pPath;
			WORD		m_wTimeout;			
			CNameList       m_Named;
			CNameList       m_Table;
			CLogixData    * m_pPing;
			};

		// Data Members
		IEthernetIPHelper * m_pEnetHelper;
		IExplicit         * m_pExplicit;
		CContext          * m_pCtx;
		BYTE		    m_bError;
		BOOL		    m_fOpen;

		// Transport Layer
		BOOL MakeLink(void);
		BOOL CheckLink(void);

		// Logix Data Object Support
		CLogixData * FindDataObject(AREF Addr);

		// Implementation
		void FindPing(void);

		// Parsing Help
		BOOL IsDigit(char c);
		BOOL IsDelim(char c);
		BOOL IsOctet(DWORD d);

		// Friends
		friend class CNameList;
	};

// End of File

#endif
