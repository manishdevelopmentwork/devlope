
#include "intern.hpp"

#include "yaskmp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
 
//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaMPMasterDeviceOptions, CStdDeviceOptions);

// Constructor

CYaskawaMPMasterDeviceOptions::CYaskawaMPMasterDeviceOptions(void)
{
	m_Drop = 1;

	}


//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Series Master Driver
//

// Instantiator	   

ICommsDriver * Create_YaskawaMPMasterDriver(void)
{
	return New CYaskawaMPMasterDriver;
	}

// Constructor

CYaskawaMPMasterDriver::CYaskawaMPMasterDriver(void)
{
	m_wID		= 0x336D;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "MP Series Controller";
	
	m_Version	= "1.01";
	
	m_ShortName	= "MP Series";

	AddSpaces();
	}

// Configuration

CLASS CYaskawaMPMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaMPMasterDeviceOptions);
	}

// Binding Control

UINT CYaskawaMPMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYaskawaMPMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Address Management

BOOL CYaskawaMPMasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CYaskawaMPDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CYaskawaMPMasterDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	BOOL fHexBits = HasHexBits(pSpace);

	UINT uFind = 0;

	if( fHexBits ) {
	
		uFind = FindHexBit(Text, pSpace);

		if( uFind != NOTHING ) {

			if( uFind < 0 || uFind > 15 ) {
					
				Error.Set( CString(IDS_DRIVER_ADDR_INVALID),
					   0
					   );
		
				return FALSE;
				}
			}

		pSpace->m_uMaximum = 0x0FFF;
		}
	
	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {
		
		if( fHexBits ) {

			Addr.a.m_Offset = (Addr.a.m_Offset << 4) + uFind;
			}

		return TRUE; 
		}

	return FALSE;
	}

BOOL CYaskawaMPMasterDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uItem = Addr.a.m_Offset;

		if( HasHexBits(pSpace) ) {

			PCSTR pHex = "0123456789ABCDEF";

			Text.Printf( "%s%s%c",
				     pSpace->m_Prefix,
				     pSpace->GetValueAsText(uItem >> 4),
				     pHex[uItem & 0x0F]
				     );
			}
		else {
			Text.Printf( "%s%s.%s", 
				     pSpace->m_Prefix, 
				     pSpace->GetValueAsText(uItem),
				     pSpace->GetTypeModifier(Addr.a.m_Type)
				     );
			}

		return TRUE;
		}
	
	return FALSE;
	}

UINT CYaskawaMPMasterDriver::FindHexBit(CString &Text, CSpace *pSpace)
{
	UINT uFind = NOTHING;

	StripType(pSpace, Text);

	CString pChar = FindHexChar(Text, pSpace);

	if( !pChar.IsEmpty() ) {

		uFind = pChar.GetAt(0) - '0';

		if( uFind > 9 ) {

			uFind -= 7;
			}
		}

	return uFind;
	}

CString CYaskawaMPMasterDriver::FindHexChar(CString &Text, CSpace *pSpace)
{
	CString pChar = "";

	if( pSpace ) {

		if( pSpace->m_uTable == 3 ) {

			pChar = Text.Right(1);

			Text.Delete(Text.GetLength() - 1, 1);

			}
		}

	return pChar;
	}

BOOL CYaskawaMPMasterDriver::HasHexBits(CSpace *pSpace)
{
	return pSpace ? (pSpace->m_uTable == 3 ? TRUE : FALSE) : FALSE;
	}

 // Implementation

void CYaskawaMPMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "Mx",  "Memory",		10, 0, 65534,  addrWordAsWord, addrWordAsReal));  

	AddSpace(New CSpace(2, "Ix",  "Input",		16, 0, 0xFFFF, addrWordAsWord, addrWordAsReal));
	
	AddSpace(New CSpace(3, "MB",  "Memory Bits",	10, 0, 0xFFFF,  addrBitAsBit, addrBitAsBit, 4));
	
	AddSpace(New CSpace(4, "IB",  "Input Bits",	16, 0, 0xFFFF, addrBitAsBit));
							
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Memobus TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYaskawaMPTCPDeviceOptions, CUIItem);

// Constructor

CYaskawaMPTCPDeviceOptions::CYaskawaMPTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD( 1, 1), MAKEWORD(  168, 192)));;

	m_Socket = 510;

	m_Unit	 = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Managament

void CYaskawaMPTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time3", !m_Keep);
			}
		}
	}

// Download Support

BOOL CYaskawaMPTCPDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CYaskawaMPTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Memobus TCP/IP Master Driver
//

// Instantiator

ICommsDriver * Create_YaskawaMemobusTCPDriver(void)
{
	return New CYaskawaMemobusTCPDriver;
	}

// Constructor

CYaskawaMemobusTCPDriver::CYaskawaMemobusTCPDriver(void)
{
	m_wID		= 0x3519;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yaskawa";
	
	m_DriverName	= "TCP/IP Memobus Master ";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Yaskawa TCP/IP Memobus Master";
	}

// Binding Control

UINT CYaskawaMemobusTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CYaskawaMemobusTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CYaskawaMemobusTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS	CYaskawaMemobusTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYaskawaMPTCPDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa Address Selection
//

// Runtime Class

AfxImplementRuntimeClass(CYaskawaMPDialog, CStdAddrDialog);
		
// Constructor

CYaskawaMPDialog::CYaskawaMPDialog(CStdCommsDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "YaskawaMPElementDlg";
	}

// Overridables

void CYaskawaMPDialog::SetAddressFocus(void)
{
	SetDlgFocus(2002);
	}

void CYaskawaMPDialog::SetAddressText(CString Text)
{
	if( m_pDriver ) {

		CYaskawaMPMasterDriver *pDriver = (CYaskawaMPMasterDriver *) m_pDriver;

		if( pDriver->HasHexBits(m_pSpace) ) {

			GetDlgItem(2005).SetWindowText(pDriver->FindHexChar(Text, m_pSpace));

			GetDlgItem(2005).EnableWindow(TRUE);

			}
		else {

			GetDlgItem(2005).SetWindowText("");

			GetDlgItem(2005).EnableWindow(FALSE);
			}
		}

	CStdAddrDialog::SetAddressText(Text);
	}

CString CYaskawaMPDialog::GetAddressText(void)
{
	if( m_pSpace ) {

		CString Text;

		Text += GetDlgItem(2002).GetWindowText();

		Text += GetDlgItem(2005).GetWindowText().ToUpper();

		return Text;
		}

	return GetDlgItem(2002).GetWindowText();
	}
// End of File
