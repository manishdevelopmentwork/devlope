
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Heartbeat/Watchdog Page
//

class CWatchdogPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CWatchdogPage(CWatchdogItem *pItem);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEt3CommsSystem     * m_pSystem;
		CWatchdogItem       * m_pWatch;

		// Implementation
		void LoadIOPollSettings   (IUICreate *pView);
		void LoadHeartbeatSettings(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Heartbeat/Watchdog Page
//

// Runtime Class

AfxImplementRuntimeClass(CWatchdogPage, CUIStdPage);

// Constructor

CWatchdogPage::CWatchdogPage(CWatchdogItem *pItem)
{
	m_Class   = AfxRuntimeClass(CWatchdogItem);

	m_pWatch  = pItem;

	m_pSystem = (CEt3CommsSystem *) pItem->GetDatabase()->GetSystemItem();
	}

// Operations

BOOL CWatchdogPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadIOPollSettings   (pView);

	LoadHeartbeatSettings(pView);

	return TRUE;
	}

// Implementation

void CWatchdogPage::LoadIOPollSettings(IUICreate *pView)
{
	BOOL fHasPhysDO = m_pSystem->GetPhysDOs() > 0;

	BOOL fHasPhysAO = m_pSystem->GetPhysAOs() > 0;

	pView->StartGroup(L"I/O Poll Timeout Settings", 1);

	pView->AddUI(m_pWatch, L"root", L"IOPollTimeout");

	if( fHasPhysDO || fHasPhysAO ) {

		pView->AddUI(m_pWatch, L"root", L"IODropPhysOutput");
		}

	if( fHasPhysDO ) {

		pView->AddUI(m_pWatch, L"root", L"IODropFirstDO");
		}

	pView->AddUI(m_pWatch, L"root", L"IODropVirtualOut");

	pView->EndGroup(FALSE);
	}

void CWatchdogPage::LoadHeartbeatSettings(IUICreate *pView)
{
	BOOL fHasPhysDO = m_pSystem->GetPhysDOs() > 0;

	BOOL fHasPhysAO = m_pSystem->GetPhysAOs() > 0;

	pView->StartGroup(L"Heartbeat Timeout Settings", 1);	

	pView->AddUI(m_pWatch, L"root", L"HeartType");

	pView->AddUI(m_pWatch, L"root", L"HeartAddr");

	pView->AddUI(m_pWatch, L"root", L"HBBootDelay");

	pView->AddUI(m_pWatch, L"root", L"HBMaxTimeout");

	if( fHasPhysDO || fHasPhysAO ) {

		pView->AddUI(m_pWatch, L"root", L"HBDropPhysOutput");
		}

	if( fHasPhysDO ) {

		pView->AddUI(m_pWatch, L"root", L"HBDropFirstDO");
		}

	pView->AddUI(m_pWatch, L"root", L"HBDropVirtualOut");

	pView->EndGroup(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Watchdog Item
//

// Dynamic Class

AfxImplementDynamicClass(CWatchdogItem, CEt3UIItem);

// Constructor

CWatchdogItem::CWatchdogItem(void)
{
	// Output

	m_Power1Fail		= 0;

	m_Power2Fail		= 0;

	m_PoEFail		= 0;

	m_RingFail		= 0;

	m_CPUMonitor		= 1;

	m_IOPollEnable		= 0;

	m_HeartEnable		= 0;

	// Action

	m_IODropPhysOutput	= 0;

	m_IODropFirstDO		= 0;

	m_IODropVirtualOut	= 0;

	m_HBDropPhysOutput	= 0;

	m_HBDropVirtualOut	= 0;

	m_HBDropFirstDO		= 0;

	//

	m_IOPollTimeout		= 0;
	
	m_HBBootDelay		= 0;

	m_HBMaxTimeout		= 0;

	//	

	m_HeartType		= 10;

	m_HeartAddr		= 1;
	}

// Attributes

UINT CWatchdogItem::GetTreeImage(void) const
{
	return IDI_WATCHDOG;
	}

// UI Creation

BOOL CWatchdogItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CWatchdogPage(this));
	
	return FALSE;
	}

// UI Update

void CWatchdogItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag.EndsWith(L"Enable") ) {

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"IODrop") || Tag.StartsWith(L"HBDrop") ) {
		
		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

void CWatchdogItem::PrepareData(void)
{
	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	File.SetWatchdogOutputEnables(BuildWatchdogEnables());

	File.SetOutputIOTimeout(m_IOPollTimeout * 1000);

	File.SetIOTimeoutActions(BuildIOTimeoutActionFlags());

	if( TRUE ) {

		UINT  uData = m_HeartAddr - 1;

		SetBit(uData, m_HeartType == 11, 15);

		File.SetHBDiscreteRegister(uData);
		}

	File.SetHBTimeout    (m_HeartEnable ? m_HBMaxTimeout : 0);

	File.SetHBBootTimeout(m_HeartEnable ? m_HBBootDelay  : 0);
	}

// Meta Data Creation

void CWatchdogItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Power1Fail);
	Meta_AddInteger(Power2Fail);
	Meta_AddInteger(PoEFail);
	Meta_AddInteger(RingFail);
	Meta_AddInteger(CPUMonitor);
	Meta_AddInteger(IOPollEnable);
	Meta_AddInteger(HeartEnable);
	Meta_AddInteger(HeartType);
	Meta_AddInteger(HeartAddr);
	Meta_AddInteger(IOPollTimeout);
	Meta_AddInteger(IODropPhysOutput);
	Meta_AddInteger(IODropFirstDO);
	Meta_AddInteger(IODropVirtualOut);
	Meta_AddInteger(HBBootDelay);
	Meta_AddInteger(HBMaxTimeout);
	Meta_AddInteger(HBDropPhysOutput);
	Meta_AddInteger(HBDropFirstDO);
	Meta_AddInteger(HBDropVirtualOut);

	Meta_SetName((IDS_WATCHDOG));
	}

// Implementation

void CWatchdogItem::DoEnables(IUIHost *pHost)
{
	BOOL fIOEnable = m_IODropPhysOutput > 0 ||
			 m_IODropFirstDO    > 0 ||
			 m_IODropVirtualOut > 0 ||
			 m_IOPollEnable	    > 0	;;

	BOOL fHBEnable = m_HBDropPhysOutput > 0 ||
			 m_HBDropFirstDO    > 0 ||
			 m_HBDropVirtualOut > 0 ||
			 m_HeartEnable	    > 0	;;

	pHost->EnableUI(L"IOPollTimeout",  fIOEnable);

	pHost->EnableUI(L"HeartType",      fHBEnable);
	pHost->EnableUI(L"HeartAddr",      fHBEnable);
	pHost->EnableUI(L"HBBootDelay",    fHBEnable);
	pHost->EnableUI(L"HBMaxTimeout",   fHBEnable);
	}

//

WORD CWatchdogItem::BuildWatchdogEnables(void)
{
	WORD wData = 0;

	SetBit(wData, m_Power1Fail      > 0, 0);
	SetBit(wData, m_Power2Fail      > 0, 1);
	SetBit(wData, m_PoEFail         > 0, 2);
	SetBit(wData, m_RingFail        > 0, 3);
	SetBit(wData, m_CPUMonitor      > 0, 4);
	SetBit(wData, m_IOPollEnable    > 0, 5);
	SetBit(wData, m_HeartEnable     > 0, 6);

	return wData;
	}

BYTE CWatchdogItem::BuildIOTimeoutActionFlags(void)
{
	BYTE bData = 0;

	SetBit(bData, m_IODropPhysOutput > 0, 0);
	SetBit(bData, m_IODropFirstDO    > 0, 1);
	SetBit(bData, m_IODropVirtualOut > 0, 2);
	SetBit(bData, m_HBDropPhysOutput > 0, 3);
	SetBit(bData, m_HBDropVirtualOut > 0, 4);
	SetBit(bData, m_HBDropFirstDO    > 0, 5);

	return bData;
	}

// End of File
