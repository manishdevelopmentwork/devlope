
//////////////////////////////////////////////////////////////////////////
//
// Koyo DirectNet Constants
//

#define	TIMEOUT 500

//////////////////////////////////////////////////////////////////////////
//
// Koyo DirectNet Driver
//

class CKoyoDirectNetDriver : public CMasterDriver
{
	public:
		// Constructor
		CKoyoDirectNetDriver(void);

		// Destructor
		~CKoyoDirectNetDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BYTE m_PingEnable;
			WORD m_PingReg;
			};
		CContext *	m_pCtx;

		BYTE m_MasterID;

		// Hex Lookup
		LPCTXT m_pHex;
		
		// Comms Data
		BYTE m_bTx[256];
		BYTE m_bRx[256];
		BYTE m_bCheck;
		UINT m_uPtr;

		// Frame Building
		void SendCtrl(BYTE bData);
		void SendByte(BYTE bData);
		void SendWord(WORD wData);
		void SendLong(DWORD dwData);
		void SendData(BYTE bData);
		void SendPacket(void);

		// Transport Layer
		BOOL InitSequence(BYTE bDrop);
		void SendHeader(BYTE bDrop, BYTE bCode, UINT uAddr, UINT uCount,BYTE bType);
		BOOL GetAck(void);
		BOOL GetEOT(void);
		BOOL GetFrame(UINT uCount);
		void PutFrame(PDWORD pData, UINT uCount, BYTE bType);
		void PutLong(PDWORD pData, UINT uCount, BYTE bType);

		// Helpers
		BOOL IsLong(UINT uType);
		BOOL GetLong(PDWORD pData, UINT uCount);
		UINT FindCount(UINT uType);

	};

// End of File
