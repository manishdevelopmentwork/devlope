
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "NewCommsDeviceRack.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Firmware Collection
//

// Runtime Class

AfxImplementRuntimeClass(CFirmwareList, CItemList)

// Constructor

CFirmwareList::CFirmwareList(void)
{
	m_Class = AfxRuntimeClass(CFirmwareFile);
}

// Download Support

void CFirmwareList::PrepareData(void)
{
	MarkNotUsed();

	ScanRack();

	ScanComms();

	DeleteOld();

	CItemList::PrepareData();
}

BOOL CFirmwareList::MakeInitData(CInitData &Init)
{
	CItem::MakeInitData(Init);

	Init.AddWord(WORD(m_List.GetCount()));

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CFirmwareFile *pFile = GetItem(Index);

		Init.AddByte(BYTE(pFile->m_FirmID));

		Init.AddByte(0);

		Init.AddWord(WORD(pFile->m_Handle));

		m_List.GetNext(Index);
	}

	return TRUE;
}

// Item Access

CFirmwareFile * CFirmwareList::GetItem(BYTE FirmID) const
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CFirmwareFile *pFile = GetItem(n);

		if( pFile->m_FirmID == FirmID ) {

			return pFile;
		}
	}

	return NULL;
}

CFirmwareFile * CFirmwareList::GetItem(INDEX Index) const
{
	return (CFirmwareFile *) CItemList::GetItem(Index);
}

// Implementation

void CFirmwareList::MarkNotUsed(void)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CFirmwareFile *pFile = GetItem(n);

		pFile->m_fUsed = FALSE;

		GetNext(n);
	}
}

void CFirmwareList::ScanRack(void)
{
	CCommsDeviceList *pList = (CCommsDeviceList *) GetParent(3);

	INDEX             Index = pList->GetHead();

	while( !pList->Failed(Index) ) {

		CCommsDevice *pDevice = pList->GetItem(Index);

		if( pDevice->IsKindOf(AfxRuntimeClass(CNewCommsDeviceRack)) ) {

			CNewCommsDeviceRack *pRack   = (CNewCommsDeviceRack *) pDevice;

			CGenericModule      *pModule = pRack->m_pModule;

			if( pModule->m_FirmID ) {

				CFirmwareFile *pFile = GetItem(pModule->m_FirmID);

				if( !pFile ) {

					pFile = New CFirmwareFile;

					pFile->m_FirmID = pModule->m_FirmID;

					AppendItem(pFile);
				}

				pFile->m_fUsed = TRUE;
			}
		}

		pList->GetNext(Index);
	}
}

void CFirmwareList::ScanComms(void)
{
	CExpansionItem *pRack = ((CCommsManager *) GetParent(7))->m_pExpansion;

	CArray<UINT> List;

	pRack->GetFirmwareList(List);

	for( UINT n = 0; n < List.GetCount(); n++ ) {

		CFirmwareFile *pFile = GetItem(BYTE(List[n]));

		if( !pFile ) {

			pFile = New CFirmwareFile;

			pFile->m_FirmID = List[n];

			AppendItem(pFile);
		}

		pFile->m_fUsed = TRUE;
	}
}

void CFirmwareList::DeleteOld(void)
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CFirmwareFile *pFile = GetItem(n);

		if( !pFile->m_fUsed ) {

			INDEX k = n;

			GetNext(n);

			DeleteItem(k);
		}
		else
			GetNext(n);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Firmware File
//

// Dynamic Class

AfxImplementDynamicClass(CFirmwareFile, CMetaItem);

// Constructor

CFirmwareFile::CFirmwareFile(void)
{
	m_fUsed  = FALSE;

	m_Handle = HANDLE_NONE;

	m_FirmID = 0;
}

// Download Support

BOOL CFirmwareFile::MakeInitData(CInitData &Init)
{
	CMetaItem::MakeInitData(Init);

	for( UINT p = 0; p < 2; p++ ) {

		CFilename Name;

		if( p == 0 ) {

			Name  = pccModule->GetFilename().WithName(L"Firmware\\");

			Name += GetProc() + L"\\";
		}
		else {
			CString Setup;

			if( GetProc() == L"STM32" ) {

				Setup = L"RunSTM32";
			}

			if( GetProc() == L"8051" ) {

				Setup = L"Run8051";
			}

			Name  = pccModule->GetFilename().WithName(L"..\\..\\..\\..\\..\\Source\\Runtime\\Setup\\");
			
			Name += Setup + L"\\Firmware\\";
		}

		switch( m_FirmID ) {

			case FIRM_GMPID1:  Name += L"gmpid1.bin";	break;
			case FIRM_GMPID2:  Name += L"gmpid2.bin";	break;
			case FIRM_GMIN8:   Name += L"gmin8.bin";	break;
			case FIRM_GMRTD6:  Name += L"gmrtd6.bin";	break;
			case FIRM_GMDIO14: Name += L"gmdio14.bin";	break;
			case FIRM_GMOUT4:  Name += L"gmout4.bin";	break;
			case FIRM_GMUIN4:  Name += L"gmuin4.bin";	break;
			case FIRM_GMSG:	   Name += L"gmsg1.bin";	break;
			case FIRM_GMRS:	   Name += L"gmrs.bin";		break;

			case FIRM_GMCN:    Name += L"gmcn.bin";		break;
			case FIRM_GMCDL:   Name += L"gmcdl.bin";	break;
			case FIRM_GMDN:	   Name += L"gmdn.bin";		break;
			case FIRM_GMDNP3:  Name += L"gmdnp3.bin";	break;
			case FIRM_GMJ1939: Name += L"gmj1939.bin";	break;
			case FIRM_GMPB:	   Name += L"gmpb.bin";		break;
			case FIRM_GMRC:	   Name += L"gmrc.bin";		break;
			case FIRM_DADIDO:  Name += L"dadido.bin";	break;
			case FIRM_DADIRO:  Name += L"dadiro.bin";	break;
			case FIRM_DAUIN6:  Name += L"dauin6.bin";	break;
			case FIRM_DAPID1:  Name += L"dapid1.bin";	break;
			case FIRM_DAPID2:  Name += L"dapid2.bin";	break;
			case FIRM_DASG1:   Name += L"dasg1.bin";	break;
			case FIRM_DAAO8:   Name += L"daao8.bin";	break;
			case FIRM_DARO8:   Name += L"daro8.bin";	break;
			case FIRM_DAMIX4:  Name += L"damix4.bin";	break;
			case FIRM_DAMIX2:  Name += L"damix2.bin";	break;

		}

		HANDLE hFile = Name.OpenReadSeq();

		if( hFile != INVALID_HANDLE_VALUE ) {

			BYTE  bData[256 * 1024];

			DWORD uSize = 0;

			if( ReadFile(hFile, bData, sizeof(bData), &uSize, NULL) ) {

				CGuid Guid;

				memcpy(&Guid, bData + uSize - sizeof(CGuid), 16);

				uSize -= sizeof(CGuid);

				uSize -= 24;

				Init.AddLong(uSize);

				Init.AddGuid(Guid);

				Init.AddData(bData, uSize);

				CloseHandle(hFile);

				return TRUE;
			}

			CloseHandle(hFile);
		}
	}

	Init.AddLong(0);

	return TRUE;
}

// Meta Data

void CFirmwareFile::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);

	Meta_AddInteger(FirmID);
}

CString CFirmwareFile::GetProc(void)
{
	switch( m_FirmID ) {

		case FIRM_GMPID1: 
		case FIRM_GMPID2: 
		case FIRM_GMIN8:  
		case FIRM_GMRTD6: 
		case FIRM_GMDIO14:
		case FIRM_GMOUT4: 
		case FIRM_GMUIN4: 
		case FIRM_GMSG:	  
		case FIRM_GMRS:
			
			return L"8051";

		case FIRM_GMCN:   
		case FIRM_GMCDL:  
		case FIRM_GMDN:	  
		case FIRM_GMDNP3: 
		case FIRM_GMJ1939:
		case FIRM_GMPB:	  
		case FIRM_GMRC:	  
		case FIRM_DADIDO: 
		case FIRM_DADIRO: 
		case FIRM_DAUIN6: 
		case FIRM_DAPID1: 
		case FIRM_DAPID2: 
		case FIRM_DASG1:  
		case FIRM_DAAO8:  
		case FIRM_DARO8:  
		case FIRM_DAMIX4: 
		case FIRM_DAMIX2:
			
			return L"STM32";
	}

	return L"NONE";
}

// End of File
