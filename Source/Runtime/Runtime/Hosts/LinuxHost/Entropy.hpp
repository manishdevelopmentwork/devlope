
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Entropy_HPP

#define INCLUDE_Entropy_HPP

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

class CEntropy : public IEntropy
{
public:
	// Constructor
	CEntropy(void);

	// Destructor
	~CEntropy(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IEntropy
	BOOL METHOD GetEntropy(PBYTE pData, UINT uData);

protected:
	// Data Members
	ULONG    m_uRefs;
	IMutex * m_pLock;
	int      m_fd1;
	int      m_fd2;
	int      m_nb;
	bool     m_out;
};

// End of File

#endif
