
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SerialBase_HPP

#define INCLUDE_SerialBase_HPP

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HANDLE;

using win32::OVERLAPPED;

using win32::CRITICAL_SECTION;

//////////////////////////////////////////////////////////////////////////
//
// Serial Port
//

class CSerialBase : public IPortObject, public IClientProcess
{
public:
	// Constructor
	CSerialBase(CString Name);

	// Destructor
	~CSerialBase(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// IPortObject
	void  METHOD Bind(IPortHandler *pHandler);
	void  METHOD Bind(IPortSwitch  *pSwitch);
	UINT  METHOD GetPhysicalMask(void);
	BOOL  METHOD Open(CSerialConfig const &Config);
	void  METHOD Close(void);
	void  METHOD Send(BYTE bData);
	void  METHOD SetBreak(BOOL fBreak);
	void  METHOD EnableInterrupts(BOOL fEnable);
	void  METHOD SetOutput(UINT uOutput, BOOL fOn);
	BOOL  METHOD GetInput(UINT uInput);
	DWORD METHOD GetHandle(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG            m_uRefs;
	CString		 m_Name;
	HANDLE           m_hPort;
	HANDLE           m_hTimer;
	HANDLE           m_hSend;
	HANDLE	         m_hRecv;
	HANDLE		 m_hConn;
	HANDLE           m_hDone;
	HANDLE           m_hTerm;
	OVERLAPPED       m_oSend;
	OVERLAPPED       m_oRecv;
	OVERLAPPED	 m_oConn;
	IThread        * m_pThread;
	IPortHandler   * m_pHandler;
	BOOL             m_fOpen;
	CSerialConfig    m_Config;
	CRITICAL_SECTION m_cs;
	BYTE		 m_bRecv[256];
	BYTE		 m_bSend[256];
	BOOL		 m_fRecv;
	BOOL		 m_fSend;
	UINT		 m_uRecv;
	UINT		 m_uSend;
	BOOL		 m_fDone;

	// Handlers
	virtual BOOL OnConn(void);
	virtual BOOL OnTest(void);
	virtual void OnTerm(void);
	virtual void OnTimer(void);
	virtual void OnRecv(void);
	virtual void OnSend(void);
	virtual void OnDone(void);
	virtual void OnBreak(void);
	virtual BOOL OnError(void);

	// Comms
	void StartRecv(void);
	void StartSend(BYTE bData);
	void WaitDone(void);

	// Objects
	void InitObjects(void);
	void TermObjects(void);
	void InitThread(void);
	void TermThread(void);
	void InitOverlapped(void);

	// Interrupts
	void EnterGuarded(void);
	void LeaveGuarded(void);

	// Port
	virtual BOOL PortOpen(void);
	virtual void PortClose(void);
};

// End of File

#endif
