
#include "intern.hpp"

#include "EthIP2a.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Native Tags Enhanced
//

// Instantiator

INSTANTIATE(CEthernetIpDriver);

// Constructor

CEthernetIpDriver::CEthernetIpDriver(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CEthernetIpDriver::~CEthernetIpDriver(void)
{
	}

// Configuration

void MCALL CEthernetIpDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CEthernetIpDriver::CheckConfig(CSerialConfig &Config)
{
	}
	
// Management

void MCALL CEthernetIpDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CEthernetIpDriver::Detach(void)
{
	}

void MCALL CEthernetIpDriver::Open(void)
{
	}

// Device

CCODE MCALL CEthernetIpDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CCtx;

			m_pCtx->m_Addr		= GetAddr(pData);			
			m_pCtx->m_Port		= GetWord(pData);
			m_pCtx->m_pRoute	= GetString(pData);
			m_pCtx->m_uTags		= GetWord(pData);

			m_pCtx->m_pTags = New CTagInfo [ m_pCtx->m_uTags ];

			memset( m_pCtx->m_pTags,
				0,
				m_pCtx->m_uTags * sizeof(CTagInfo)
				);

			for( UINT n = 0; n < m_pCtx->m_uTags; n++ ) {

				BYTE s = GetByte(pData);

				PTXT p = NULL;

				if( s ) {

					p = New char [ s + 1 ];

					memcpy(p, pData, s);

					p[s]   = 0;

					pData += s;
					}

				m_pCtx->m_pTags[n].m_pName = p;

				// Get the array dimensions for this tag.
				if( p ) {

					UINT uDims = GetByte(pData);

					WORD x, y, z;
					x = y = z = 0;

					switch( uDims ) {

						case 1:
							x = GetWord(pData);
							break;
						case 2:
							x = GetWord(pData);
							y = GetWord(pData);
							break;
						case 3:
							x = GetWord(pData);
							y = GetWord(pData);
							z = GetWord(pData);
							break;
						default:
							// Not an atomic array
							break;
						}
					
					m_pCtx->m_pTags[n].SetDim(0, x);
					m_pCtx->m_pTags[n].SetDim(1, y);
					m_pCtx->m_pTags[n].SetDim(2, z);
					}
				}

			m_pCtx->m_pHead    = NULL;

			m_pCtx->m_pTail    = NULL;

			m_pCtx->m_pSocket  = NULL;

			m_pCtx->m_pSession = NULL;

			m_pCtx->m_pConnect = NULL;

			m_pCtx->m_pClient  = NULL;

			m_pCtx->m_fDirty   = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEthernetIpDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		for( UINT n = 0; n < m_pCtx->m_uTags; n++ ) {

			if( m_pCtx->m_pTags[n].m_pName ) {

				PTXT p = m_pCtx->m_pTags[n].m_pName;

				delete [] p;
				}
			}

		FreeWatchLists();

		delete [] m_pCtx->m_pTags;

		delete m_pCtx->m_pClient;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEthernetIpDriver::Ping(void)
{
	if( OpenStack() ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

void MCALL CEthernetIpDriver::Service(void)
{
	// Anywhere we get a comms error in here, we
	// need to call a function to tear down all our watch
	// list data and close the connection.
	
	for( CWatchList *pList = m_pCtx->m_pHead; pList; pList = pList->m_pNext ) {

		pList->ClearTouch();
		}

	if( TouchTags() > 0 ) {

		if( UpdateWatchLists() ) {

			CleanupEmptyWatchLists();

			ClearTagTouches();
			}
		}
	}

CCODE MCALL CEthernetIpDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenStack() ) {

		if( Addr.a.m_Table >= addrNamed ) {
					
			UINT uSlot = Addr.a.m_Offset;

			if( uSlot < m_pCtx->m_uTags ) {

				CTagInfo *pTag = m_pCtx->m_pTags + uSlot;

				return pTag->Read(Addr, pData, uCount);
				}
			}

		else {
			UINT uSlot = Addr.a.m_Table;

			if( uSlot < m_pCtx->m_uTags ) {

				CTagInfo *pTag = m_pCtx->m_pTags + uSlot;

				if( Addr.a.m_Type == addrBitAsBit ) {

					return pTag->ReadBitTable(Addr, pData, uCount);
					}

				else {
					return pTag->ReadTable(Addr, pData, uCount);
					}
				}
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEthernetIpDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenStack() ) {

		UINT uSlot = Addr.a.m_Table >= addrNamed ? Addr.a.m_Offset : Addr.a.m_Table;

		if( uSlot < m_pCtx->m_uTags ) {

			CTagInfo *pTag = m_pCtx->m_pTags + uSlot;

			if( Addr.a.m_Type == addrBitAsBit && uSlot < addrNamed ) {
				
				return pTag->WriteBitTable(Addr, pData, uCount);
				}

			else {
				return pTag->Write(Addr, pData, uCount);
				}
			}
		}

	return CCODE_ERROR;
	}

// User Access

UINT CEthernetIpDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CCtx *pCtx = (CCtx *) pContext;

	if( uFunc == 1 ) {
		
		// Set Device IP Address

		DWORD dwValue = 0;

		PTXT pText = PTXT(Alloc(3 + 1));
			
		for( UINT u = 0, x = 0, s = 24, b = 0; u < 4; u++, x++, s -= 8 ) {

			memset(pText, 0, sizeof(pText));
				
			b = x;

			while( !IsOctetEnd(Value[x]) ) {

				if( !IsDigit(Value[x]) ) {

					Free(pText);

					return 0;
					}

				x++;
				}

			memcpy(pText, Value + b, x - b);

			UINT uOctet = ATOI(pText);

			if( !IsByte(uOctet) || b == x ) {

				Free(pText);

				return 0;
				}

			dwValue |= (uOctet & 0xFF) << s;
			}

		pCtx->m_Addr = MotorToHost(dwValue);

		pCtx->m_fDirty = TRUE;

		Free(pText);

		return 1;
		}

	if( uFunc == 2 ) {

		// Get Device IP

		return MotorToHost(pCtx->m_Addr);
		}

	return 0;
	}

// CIP Stack

void CEthernetIpDriver::CreateStack(void)
{
	m_pCtx->m_pSocket  = New CTcpIpSock (m_pCtx->m_Addr, m_pCtx->m_Port);

	m_pCtx->m_pSession = New CEthernetIp(m_pCtx->m_pSocket, 1000);

	m_pCtx->m_pConnect = New CCipConnect(m_pCtx->m_pSession, m_pCtx->m_pRoute);

	m_pCtx->m_pClient  = New CCipClient (m_pCtx->m_pConnect);
	}

BOOL CEthernetIpDriver::CheckStack(void)
{
	if( m_pCtx->m_pClient ) {

		if( !m_pCtx->m_fDirty ) {

			if( m_pCtx->m_pClient->IsLayerUp() ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CEthernetIpDriver::OpenStack(void)
{	
	if( CheckStack() ) {

		return TRUE;
		}

	if( m_pCtx->m_fDirty ) {

		FreeWatchLists();

		delete m_pCtx->m_pClient;

		m_pCtx->m_pClient = NULL;

		m_pCtx->m_fDirty = FALSE;
		}

	if( !m_pCtx->m_pClient ) {

		CreateStack();
		}

	// Tell the client which tags we are interested in.

	m_pCtx->m_pClient->AllocKeepTags(m_pCtx->m_uTags);

	for( UINT u = 0; u < m_pCtx->m_uTags; u++ ) {

		m_pCtx->m_pClient->AddTagToKeep(m_pCtx->m_pTags[u].m_pName);
		}

	if( m_pCtx->m_pClient->Open() ) {

		for( UINT u = 0; u < m_pCtx->m_uTags; u++ ) {

			m_pCtx->m_pTags[u].SetClient(m_pCtx->m_pClient);
			}

		return TRUE;
		}

	FreeWatchLists();

	return FALSE;
	}

// Implementation

void CEthernetIpDriver::FreeWatchLists(void)
{
	CWatchList *pList;
	
	while( (pList = m_pCtx->m_pHead) ) {

		CTagInfo *pTag;

		for( pTag = pList->m_pHead; pTag; pTag = pTag->m_pNext ) {

			pTag->m_pWatch = NULL;

			pTag->ResetPos();
			}

		// Attempt to tell the controller to delete its watch list instance;
		// this may not succeed depending on the reason we are here --
		// e.g. we may have lost the connection to the controller itself.

		pList->Delete();

		AfxListRemove( m_pCtx->m_pHead,
			       m_pCtx->m_pTail,
			       pList,
			       m_pNext,
			       m_pPrev
			       );

		delete    pList;
		}

	m_pCtx->m_pClient->Close();
	}

void CEthernetIpDriver::CleanupEmptyWatchLists(void)
{
	for( CWatchList *pList = m_pCtx->m_pHead; pList; ) {

		// NOTE -- Look for lists that don't have any tags
		// in them. Remove them from our list, and delete.

		CWatchList *pNext = pList->m_pNext;

		if( pList->IsEmpty() ) {

			pList->Delete();

			AfxListRemove(  m_pCtx->m_pHead,
					m_pCtx->m_pTail,
					pList,
					m_pNext,
					m_pPrev
					);

			delete pList;
			}

		pList = pNext;
		}
	}

void CEthernetIpDriver::ClearTagTouches(void)
{
	for( UINT n = 0; n < m_pCtx->m_uTags; n++ ) {

		CTagInfo *pTag = m_pCtx->m_pTags + n;

		if( pTag->m_pName ) {

			pTag->ClearTouch();
			}
		}
	}

UINT CEthernetIpDriver::TouchTags(void)
{
	UINT uTouch = 0;

	for( UINT t = 0; t < m_pCtx->m_uTags; t++ ) {

		CTagInfo *pTag = m_pCtx->m_pTags + t;

		if( pTag->m_pName ) {

			if( pTag->ShouldTouch() ) {

				uTouch++;

				if( pTag->IsWatched() ) {

					pTag->Touch();
					}
				else {
					if( pTag->FindInfo() ) {

						if( m_pCtx->m_pTail == NULL || m_pCtx->m_pTail->IsTooLarge(pTag->GetSize()) ) {
							
							CWatchList *pList = New CWatchList(m_pCtx->m_pClient);

							if( !pList->CreateInstance() ){

								delete pList;

								return 0;
								}

							AfxListAppend( m_pCtx->m_pHead,
								       m_pCtx->m_pTail,
								       pList,
								       m_pNext,
								       m_pPrev
								       );
							}

						CWatchList *pList = m_pCtx->m_pTail;

						pList->AddTag(pTag);
						}
					}
				}
			}
		}

	return uTouch;
	}

BOOL CEthernetIpDriver::UpdateWatchLists(void)
{
	for( CWatchList *pList = m_pCtx->m_pHead; pList; pList = pList->m_pNext ) {

		if( pList->IsSparse() ) {

			CTagInfo *pTag;

			for( pTag = pList->m_pHead; pTag; pTag = pTag->m_pNext ) {

				if( !pTag->ShouldTouch() ) {

					pList->RemoveTag(pTag);
					}

				pTag->ResetPos();
				}

			pList->SetRebuild();
			}

		if( pList->IsDirty() ) {

			// NOTE -- List has changed so we need to delete it in the PLC
			// and then create it. When we create it, we set the tags so
			// that they reference the appropriate place in the buffer.

			pList->Rebuild();

			pList->ConfigureAllTags();
			}

		if( !pList->UpdateData() ) {

			FreeWatchLists();

			return FALSE;
			}
		}

	return TRUE;
	}

// Helpers

BOOL CEthernetIpDriver::IsOctetEnd(char c)
{
	return c == '.' || c == '\0';
	}

BOOL CEthernetIpDriver::IsDigit(char c)
{
	return c >= '0' && c <= '9';
	}

BOOL CEthernetIpDriver::IsByte(UINT uNum)
{
	return uNum <= 255;
	}


// End of File
