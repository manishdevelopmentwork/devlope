#define BBB_IDENT 0x4053

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Space Definitions
//
enum {
	SPBIT	=  1,
	SPBYTE	=  2,
	SPWORD	=  3,
	SPLONG	=  4,
	SPREAL	=  5,
	SPSTR   =  6,
	};

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCRDBACC = 0xA0, // remote database access
	};

enum { // Operational function codes
	OFCRDBYN  =  0x4, // Read By Name
	OFCWRBYN  = 0x84, // Write By Name
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Field Select 2 codes
	FS2PROTECT = 0x80,
	FS2ALMPRI  = 0x40,
	FS2BNAME   = 0x20,
	FS2BEXT    = 0x10,
	FS2BATT    = 0x08,
	FS2ALMDBLO = 0x04,
	FS2ALMDBHI = 0x02,
	FS2MSDVER  = 0x01,
	};

enum { // Field Select 3 codes
	FS3ALMLMLO = 0x80,
	FS3ALMLMHI = 0x40,
	FS3ALMEXLO = 0x20,
	FS3ALMEXHI = 0x10,
	FS3RBERSVD = 0x08,
	FS3LCKRSVD = 0x04,
	FS3SIGVAL  = 0x02,
	FS3NAMELNG = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	POLLMSG  = 0x85, // Poll message
	POLLDOWN = 0x86, // Down Transmit ACK, Slave ACKing message
	POLLNONE = 0x87, // ACK No Data, in response to a Poll message
	NRTSEND  = 0x88, // Send Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	POLLUPMS = 0x8B, // UP-ACK, Master ACKing message
	POLLNAK  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMINI = 0xA9, // Alarm Initialization
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Transaction States

	STPOLL    = 1,
	STPWTDATA = 2,
	};

enum { // response return values

	PRNORSP		= 0,
	PRWRONG		= 1,
	PRACK		= 2,
	PRNAK		= 3,
	PRDATA		= 4,
	PRGENACK	= 5
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

// UDP Receive positions SH=Standard Header, EH = Extended Header
enum {  // Common Header
	UDPHSZ	= 0,	// Message Header Size
	UDPPCT	= 2,	// Number of Packets
	UDPFLG	= 4,	// Flags
	UDPSEQ	= 6,	// Send Sequence Number
	UDPLAK	= 10,	// Last Ack'ed Seq Number
	};

enum {  // Extra for extended header
	UDPRIP	= 14,	// Sender IP
	UDP000	= 18,	// Filler
	};

enum {  // Standard Header Subpackets
	UDPSSZ	= 14,	// Subpacket size
	UDPSTY	= 16,	// Type (BSAP = 0)
	UDPSMX	= 18,	// Exchange Code
	UDPSMS	= 19,	// Message Seq #
	UDPSGA	= 21,	// Global Address of Sender
	UDPSER	= 23,	// Error if not 0
	UDPSCT	= 24,	// Number of Items (= 1 for good reply)
	UDPSDT	= 25,	// Data start
	};

enum {  // Extended Header Subpackets
	UDPESZ	= 22,	// Subpacket size
	UDPETY	= 24,	// Type (BSAP = 0)
	UDPEMX	= 26,	// Exchange Code
	UDPEMS	= 27,	// Message Seq #
	UDPEGA	= 29,	// Global Address of Sender
	UDPEER	= 31,	// Error if not 0
	UDPECT	= 32,	// Number of Items (= 1 for good reply)
	UDPEDT	= 33,	// Data start
	};

// Receive Frame Check codes
enum {
	FRCONT	= 0,
	FRDONE	= 1,
	FRERR	= 2
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	SZHDREXT	((WORD)22)
#define	SZHDRSTD	((WORD)14)
#define SZDATA		((WORD)247)
#define	POLLPOSN	23
#define MAX_CHARS	80
#define MAX_LTEXT	(MAX_CHARS / sizeof(DWORD))

#define	FLAGNEW		6
#define	FLAGOLD		4

struct BSHeader { // Serial Comms Header
	BYTE	bAdd;
	BYTE	bSNum;
	BYTE	bDFC;
	BYTE	bSQL;
	BYTE	bSQH;
	BYTE	bSFC;
	BYTE	bNSB;
	};

enum { // Poll - Serial Tx Positions (0=DLE, 1=STX)

	TXDADD	= 2,	// Destination Address
	TXSERN	= 3,	// Serial Number
	TXDFC	= 4,	// Function code
	TXSADD	= 5,	// Poll Priority Code (=0)
	TXASQL	= 5,	// App Sequence Number LO
	TXASQH	= 6,	// App Sequence Number HI
	TXSFC	= 7,	// Source Function Code
	TXNSB	= 8,	// Node Status Byte
	TXDATA0	= 9,	// Data
	TXFSSCT	= 10,	// Field Select Count
	TXFS1	= 11,	// Field Select 1;
	TXFS2	= 12,	// Field Select 2;
	TXFS3	= 13,	// Field Select 3;
	TXFS4	= 14,	// Field Select 4;
	};

enum { // Serial Rx Positions (DLE, STX excluded from receive buffer)

	RXDADD	= 0,	// Destination Address
	RXSERN	= 1,	// Serial Number
	RXDFC	= 2,	// Response Function Code
	RXPSADD	= 3,	// Poll - Slave Address (Not UP Ack)
	RXUPSN	= 3,	// Poll - Up Ack Serial Number
	RXASQL	= 3,	// App Sequence Number LO
	RXPNSB	= 4,	// Poll - Node Status Byte
	RXASQH	= 4,	// App Sequence Number HI
	RXBUFCT	= 5,	// Poll - Number of Buffers in use
	RXSFC	= 5,	// Source Function Code
	RXNSB	= 6,	// Node Status Byte
	RXRER	= 7,	// Return error result
	RXDCT	= 8,	// Data count
	RXDATA0	= 9,	// Data Start
	RXALMVA	= 10,	// Alarm Load Version A
	RXALMVB	= 11,	// Alarm Load Version B
	RXALMMA	= 12,	// Alarm MSD A
	RXALMMB	= 13,	// Alarm MSD B
	};

// Add Var Name Responses
#define	VTXFULL	0
#define	VADDOK	1
#define	VNONAME	2

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock BSAP Serial Master Driver
//

class CBBBSAPSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CBBBSAPSerialDriver(void);

		// Destructor
		~CBBBSAPSerialDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);
		DEFMETH(void)	CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Open(void);
		
		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		DEFMETH(UINT)	DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
		
		// Entry Points
		DEFMETH(CCODE)	Ping (void);
		DEFMETH(CCODE)	Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE)	Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// String support
		struct CStringTag
		{
			WORD  m_Index;
			char  m_Text[MAX_CHARS];
			};

		// Device Context
		struct CContext
		{
			UINT	m_uDrop;

			// Protocol information
			UINT	m_uMsgSeq;
			UINT	m_uAppSeq;
			UINT	m_uACCOLVers;
			UINT	m_uMSDVers;

			BYTE	m_bIsC3;

			// Tag information
			BOOL	m_fHasTags;
			WORD	m_wTagCount;
			PBYTE	m_pbDevName;
			PDWORD	m_pdAddrList;
			PBYTE	m_pbNameList;
			PWORD	m_pwNameIndex;

			// String Tags
			UINT	     m_uStrings;
			CStringTag * m_pStrings;
			};

		struct READFRAME
		{
			UINT	uAddr;
			PDWORD	pData;
			UINT	uCount;
			UINT	uGood;
			};

		// Data Members
		CContext * m_pCtx;

		BYTE	m_bTx[256];
		BYTE	m_bRx[512];
		BYTE	m_bPoll[16];
		BYTE	m_bAlm[24];

		PBYTE	m_pTx;

		UINT	m_uPtr;
		UINT	m_uWriteError;

		// Header and Control Structures
		BSHeader	 Head;
		BSHeader	*m_pHead;

		BOOL		m_fVarSuccess;

		READFRAME	m_RFrame;
		READFRAME	*m_pRFrame;

		// Frame Building
		void	StartFrame(BOOL fFunc, BOOL fIsWrite);
		void	PutStart(BOOL fFunc);
		void	EndFrame(void);
		void	SetBasicHeader(BOOL fIsWrite);
		void	AddHeader(void);
		void	AddCRC(void);
		void	NextSeq(UINT *pSeq, BOOL fByte);

		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
		void	AddText(PCTXT pText);
		UINT	AddVarName(AREF Addr, UINT uIndex, UINT uBytes = 0);
		UINT	GetNamePos(AREF Addr, UINT uIndex);

		// Read Handlers
		CCODE	DoDataRead(AREF Addr);
		DWORD	GetReadData(AREF Addr, UINT *pOffset, UINT uIndex);
		DWORD	GetStringData(AREF Addr, UINT &uPos, UINT uDPos, UINT uIndex);
		BOOL	AddReadByName(AREF Addr);
		void	AddReadByNameHeader(void);

		// Write Handlers
		BOOL	DoBitWrite (PDWORD pData);
		BOOL	DoByteWrite(PDWORD pData);
		BOOL	DoWordWrite(PDWORD pData);
		BOOL	DoLongWrite(PDWORD pData);
		BOOL	DoRealWrite(PDWORD pData);
		BOOL    DoStringWrite(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	AddWriteByName(AREF Addr);
		BYTE	AddDescriptor(UINT uTable, UINT uOffset, BOOL fBitData);
		void	AddWriteData(UINT uType, DWORD dData);
		void    AddStringData(AREF Addr, PDWORD pData, UINT uCount);

		// String Support
		UINT GetStringContextIndex(AREF Addr);
		void SetStringContext(AREF Addr, PCTXT pText, UINT uCount);
		UINT GetStringCount(AREF Addr, UINT uPos, UINT uCount);
		UINT GetCharCount(PCTXT pText, UINT uOffset);
		void RemoveTrailing(PBYTE &pByte, BYTE bByte, UINT uBytes);

		// Polling
		UINT	MainPoll(BOOL fInPing);
		void	StartPoll(void);
		BOOL	SynchPolls(void);
		void	SendDataAck(void);
		void	SendAlarmAck(void);
		BOOL	GetACCOLVersion(void);
		void	SendListRequest(UINT uList);

		// Response Checking
		UINT	CheckForResponse(void);
		BOOL	SendAndWaitForData(void);
		BOOL	CheckForProperData(void);
		
		// Transport Layer
		UINT	SendDataFrame(void);
		BOOL	SendFrame(void);
		BOOL	RecvFrame(void);
		BOOL	Transact(BOOL fWantReply);

		// Tags
		BOOL	LoadTags(LPCBYTE &pData);
		void    LoadStringSupport(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	StoreName(LPCBYTE &pData, WORD wItem, PWORD pPosition);

		// Helpers
		BOOL	CheckHeader(void);
		BOOL	SlAddressOK(void);
		BOOL	SerialNumOK(void);
		BOOL	AppSeqNumOK(void);
		BOOL	REROK(void);
		BOOL	HasTags(PDWORD pData, UINT uCount);
		BYTE	MakeUpper(BYTE b);
		BOOL	IsString(UINT uTable);
		BOOL	IsStringText(UINT uChar);
		BOOL	CanAddToRequest(PBYTE pName, UINT uBytes = 0);
		UINT    FindOffset(AREF Addr, UINT uIndex);
		UINT    GetCount(CAddress Addr, UINT uCount);
		UINT    ExpandCount(CAddress Addr, UINT uCount, UINT uMax = 0);

		// delete created buffers
		void	CloseDown(void);
	};
/*
static BYTE crctbl[] = {
// 0x
	0x087,0x00F,0x00E,0x01E,0x095,0x02C,0x01C,0x03D,
	0x0A3,0x049,0x02A,0x058,0x0B1,0x06A,0x038,0x07B,
	0x0CF,0x083,0x046,0x092,0x0DD,0x0A0,0x054,0x0B1,
	0x0EB,0x0C5,0x062,0x0D4,0x0F9,0x0E6,0x070,0x0F7,
// 1x
	0x006,0x01F,0x08F,0x00E,0x014,0x03C,0x09D,0x02D,
	0x022,0x059,0x0AB,0x048,0x030,0x07A,0x0B9,0x06B,
	0x04E,0x093,0x0C7,0x082,0x05C,0x0B0,0x0D5,0x0A1,
	0x06A,0x0D5,0x0E3,0x0C4,0x078,0x0F6,0x0F1,0x0E7,
// 2x
	0x085,0x02E,0x00C,0x03F,0x097,0x00D,0x01E,0x01C,
	0x0A1,0x068,0x028,0x079,0x0B3,0x04B,0x03A,0x05A,
	0x0CD,0x0A2,0x044,0x0B3,0x0DF,0x081,0x056,0x090,
	0x0E9,0x0E4,0x060,0x0F5,0x0FB,0x0C7,0x072,0x0D6,
// 3x
	0x004,0x03E,0x08D,0x02F,0x016,0x01D,0x09F,0x00C,
	0x020,0x078,0x0A9,0x069,0x032,0x05B,0x0BB,0x04A,
	0x04C,0x0B2,0x0C5,0x0A3,0x05E,0x091,0x0D7,0x080,
	0x068,0x0F4,0x0E1,0x0E5,0x07A,0x0D7,0x0F3,0x0C6,
// 4x
	0x083,0x04D,0x00A,0x05C,0x091,0x06E,0x018,0x07F,
	0x0A7,0x00B,0x02E,0x01A,0x0B5,0x028,0x03C,0x039,
	0x0CB,0x0C1,0x042,0x0D0,0x0D9,0x0E2,0x050,0x0F3,
	0x0EF,0x087,0x066,0x096,0x0FD,0x0A4,0x074,0x0B5,
// 5x
	0x002,0x05D,0x08B,0x04C,0x010,0x07E,0x099,0x06F,
	0x026,0x01B,0x0AF,0x00A,0x034,0x038,0x0BD,0x029,
	0x04A,0x0D1,0x0C3,0x0C0,0x058,0x0F2,0x0D1,0x0E3,
	0x06E,0x097,0x0E7,0x086,0x07C,0x0B4,0x0F5,0x0A5,
// 6x
	0x081,0x06C,0x008,0x07D,0x093,0x04F,0x01A,0x05E,
	0x0A5,0x02A,0x02C,0x03B,0x0B7,0x009,0x03E,0x018,
	0x0C9,0x0E0,0x040,0x0F1,0x0DB,0x0C3,0x052,0x0D2,
	0x0ED,0x0A6,0x064,0x0B7,0x0FF,0x085,0x076,0x094,
// 7x
	0x000,0x07C,0x089,0x06D,0x012,0x05F,0x09B,0x04E,
	0x024,0x03A,0x0AD,0x02B,0x036,0x019,0x0BF,0x008,
	0x048,0x0F0,0x0C1,0x0E1,0x05A,0x0D3,0x0D3,0x0C2,
	0x06C,0x0B6,0x0E5,0x0A7,0x07E,0x095,0x0F7,0x084,
// 8x
	0x08F,0x08B,0x006,0x09A,0x09D,0x0A8,0x014,0x0B9,
	0x0AB,0x0CD,0x022,0x0DC,0x0B9,0x0EE,0x030,0x0FF,
	0x0C7,0x007,0x04E,0x016,0x0D5,0x024,0x05C,0x035,
	0x0E3,0x041,0x06A,0x050,0x0F1,0x062,0x078,0x073,
// 9x
	0x00E,0x09B,0x087,0x08A,0x01C,0x0B8,0x095,0x0A9,
	0x02A,0x0DD,0x0A3,0x0CC,0x038,0x0FE,0x0B1,0x0EF,
	0x046,0x017,0x0CF,0x006,0x054,0x034,0x0DD,0x025,
	0x062,0x051,0x0EB,0x040,0x070,0x072,0x0F9,0x063,
// Ax
	0x08D,0x0AA,0x004,0x0BB,0x09F,0x089,0x016,0x098,
	0x0A9,0x0EC,0x020,0x0FD,0x0BB,0x0CF,0x032,0x0DE,
	0x0C5,0x026,0x04C,0x037,0x0D7,0x005,0x05E,0x014,
	0x0E1,0x060,0x068,0x071,0x0F3,0x043,0x07A,0x052,
// Bx
	0x00C,0x0BA,0x085,0x0AB,0x01E,0x099,0x097,0x088,
	0x028,0x0FC,0x0A1,0x0ED,0x03A,0x0DF,0x0B3,0x0CE,
	0x044,0x036,0x0CD,0x027,0x056,0x015,0x0DF,0x004,
	0x060,0x070,0x0E9,0x061,0x072,0x053,0x0FB,0x042,
// Cx
	0x08B,0x0C9,0x002,0x0D8,0x099,0x0EA,0x010,0x0FB,
	0x0AF,0x08F,0x026,0x09E,0x0BD,0x0AC,0x034,0x0BD,
	0x0C3,0x045,0x04A,0x054,0x0D1,0x066,0x058,0x077,
	0x0E7,0x003,0x06E,0x012,0x0F5,0x020,0x07C,0x031,
// Dx
	0x00A,0x0D9,0x083,0x0C8,0x018,0x0FA,0x091,0x0EB,
	0x02E,0x09F,0x0A7,0x08E,0x03C,0x0BC,0x0B5,0x0AD,
	0x042,0x055,0x0CB,0x044,0x050,0x076,0x0D9,0x067,
	0x066,0x013,0x0EF,0x002,0x074,0x030,0x0FD,0x021,
// Ex
	0x089,0x0E8,0x000,0x0F9,0x09B,0x0CB,0x012,0x0DA,
	0x0AD,0x0AE,0x024,0x0BF,0x0BF,0x08D,0x036,0x09C,
	0x0C1,0x064,0x048,0x075,0x0D3,0x047,0x05A,0x056,
	0x0E5,0x022,0x06C,0x033,0x0F7,0x001,0x07E,0x010,
// Fx
	0x008,0x0F8,0x081,0x0E9,0x01A,0x0DB,0x093,0x0CA,
	0x02C,0x0BE,0x0A5,0x0AF,0x03E,0x09D,0x0B7,0x08C,
	0x040,0x074,0x0C9,0x065,0x052,0x057,0x0DB,0x046,
	0x064,0x032,0x0ED,0x023,0x076,0x011,0x0FF,0x000
	};

FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File
