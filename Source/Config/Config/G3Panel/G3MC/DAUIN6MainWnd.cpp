
#include "intern.hpp"

#include "DAUIN6MainWnd.hpp"

#include "DAUIN6Module.hpp"

#include "DAUIN6Input.hpp"

#include "DAUIN6InputConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Universal Analog Input Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CDAUIN6MainWnd, CProxyViewWnd);

// Constructor

CDAUIN6MainWnd::CDAUIN6MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
}

// Overridables

void CDAUIN6MainWnd::OnAttach(void)
{
	m_pItem = (CDAUIN6Module *) CProxyViewWnd::m_pItem;

	AddInputPages();

	CProxyViewWnd::OnAttach();
}

// Implementation

void CDAUIN6MainWnd::AddInputPages(void)
{
	CDAUIN6InputConfig *pConfig = m_pItem->m_pInputConfig;

	for( UINT n = 0; n < pConfig->GetPageCount(); n++ ) {

		CString   Name  = pConfig->GetPageName(n);

		CViewWnd *pPage = pConfig->CreatePage(n);

		m_pMult->AddView(Name, pPage);

		pPage->Attach(pConfig);
	}
}

// End of File
