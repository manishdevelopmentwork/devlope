
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UdpSocket_HPP

#define	INCLUDE_UdpSocket_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UdpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CUdp;

class CUdpHeader;

//////////////////////////////////////////////////////////////////////////
//
// UDP Socket
//

class CUdpSocket : public ISocket
{
	public:
		// Constructor
		CUdpSocket(void);

		// Binding
		void Bind(CUdp *pUdp);

		// Attributes
		BOOL IsFree(void) const;

		// Operations
		void Create(void);
		void NetStat(IDiagOutput *pOut);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ISocket Methods
		HRM Listen(WORD Loc);
		HRM Listen(IPADDR const &Ip, WORD Loc);
		HRM Connect(IPADDR const &Ip, WORD Rem);
		HRM Connect(IPADDR const &Ip, WORD Rem, WORD Loc);
		HRM Connect(IPADDR const &Ip, IPADDR const &If, WORD Rem, WORD Loc);
		HRM Recv(PBYTE pData, UINT &uSize, UINT uTime);
		HRM Recv(PBYTE pData, UINT &uSize);
		HRM Send(PBYTE pData, UINT &uSize);
		HRM Recv(CBuffer * &pBuff, UINT uTime);
		HRM Recv(CBuffer * &pBuff);
		HRM Send(CBuffer   *pBuff);
		HRM GetLocal (IPADDR &Ip);
		HRM GetRemote(IPADDR &Ip);
		HRM GetLocal (IPADDR &Ip, WORD &Port);
		HRM GetRemote(IPADDR &Ip, WORD &Port);
		HRM GetPhase(UINT &Phase);
		HRM SetOption(UINT uOption, UINT uValue);
		HRM Abort(void);
		HRM Close(void);

		// Event Handlers
		BOOL OnTest(CUdpHeader *pUdp);
		BOOL OnRecv(IPREF Face, PSREF Ps, CUdpHeader *pUdp, CBuffer *pBuff);
		BOOL OnSend(void);

		// Linked List
		PVOID        m_pRoot;
		CUdpSocket * m_pNext;
		CUdpSocket * m_pPrev;

	protected:
		// Static Data
		static UINT m_NextPort;

		// Data Members
		ULONG	  m_uRefs;
		CUdp	* m_pUdp;
		BOOL	  m_fUsed;
		BOOL	  m_fOpen;
		UINT	  m_uAddress;
		BOOL	  m_fActive;
		CIpAddr	  m_LocIp;
		CIpAddr	  m_RemIp;
		CIpAddr	  m_Multi;
		WORD	  m_LocPort;
		WORD	  m_RemPort;
		CBuffer * m_pTxBuff;
		CBuffer	* m_pRxBuff[16];
		UINT	  m_uRxHead;
		UINT	  m_uRxTail;
		UINT	  m_uRxSize;

		// Implementation
		void ClearRx(void);
		void ClearTx(void);
		void WaitTx(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

STRONG_INLINE BOOL CUdpSocket::OnTest(CUdpHeader *pUdp)
{
	return m_fOpen && m_LocPort == pUdp->m_RemPort;
	}

// End of File

#endif

