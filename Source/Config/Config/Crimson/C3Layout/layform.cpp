
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Layout Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Layout Formation Base Class
//

// Constructor

CLayFormation::CLayFormation(void)
{
	}

// Destructor

CLayFormation::~CLayFormation(void)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];

		delete pItem;
		}

	m_List.Empty();
	}

// Operations

void CLayFormation::AddItem(CLayItem *pItem)
{
	m_List.Append(pItem);
	}

// Overridables

void CLayFormation::OnSetOffset(void)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CLayItem *pItem = m_List[n];
		
		pItem->SetOffset(m_Offset);
		}
	}

// End of File
