
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Uart51_HPP
	
#define	INCLUDE_Uart51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCcm51;

//////////////////////////////////////////////////////////////////////////
//
// iMX51 UART
//

class CUart51 : public IPortObject, public IEventSink
{
	public:
		// Constructor
		CUart51(UINT iIndex, CCcm51 *pCcm);

		// Destructor
		~CUart51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPortObject
		void  METHOD Bind(IPortHandler *pHandler);
		void  METHOD Bind(IPortSwitch  *pSwitch);
		UINT  METHOD GetPhysicalMask(void);
		BOOL  METHOD Open(CSerialConfig const &Config);
		void  METHOD Close(void);
		void  METHOD Send(BYTE bData);
		void  METHOD SetBreak(BOOL fBreak);
		void  METHOD EnableInterrupts(BOOL fEnable);
		void  METHOD SetOutput(UINT uOutput, BOOL fOn);
		BOOL  METHOD GetInput(UINT uInput);
		DWORD METHOD GetHandle(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// States
		enum
		{
			stateClosed = 0,
			stateOpen   = 1,
			};

		// Registers
		enum
		{
			regRecv	   = 0x0000 / sizeof(DWORD),
			regSend	   = 0x0040 / sizeof(DWORD),
			regCtrl1   = 0x0080 / sizeof(DWORD),
			regCtrl2   = 0x0084 / sizeof(DWORD),
			regCtrl3   = 0x0088 / sizeof(DWORD),
			regCtrl4   = 0x008C / sizeof(DWORD),
			regFifo    = 0x0090 / sizeof(DWORD),
			regStat1   = 0x0094 / sizeof(DWORD),
			regStat2   = 0x0098 / sizeof(DWORD),
			regEscChar = 0x009C / sizeof(DWORD),
			regEscTime = 0x00A0 / sizeof(DWORD),
			regBaudInc = 0x00A4 / sizeof(DWORD),
			regBaudMod = 0x00A8 / sizeof(DWORD),
			regBaudCnt = 0x00AC / sizeof(DWORD),
			regOneMs   = 0x00B0 / sizeof(DWORD),
			regTest    = 0x00B4 / sizeof(DWORD),
			};

		// Control / Status Bits 
		enum
		{
			bitTxNoRTS	= Bit(14),
			bitTxCTS	= Bit(13),
			bitTxFifo	= Bit(13),
			bitRxFifo	= Bit( 9),
			bitRxAged	= Bit( 8),
			bitTxEmpty	= Bit( 6),
			bitRxEmpty	= Bit( 5),
			bitTxFull	= Bit( 4),
			bitRxFull	= Bit( 3),
			bitTxDone	= Bit( 3),
			bitRxTimer	= Bit( 3),
			bitTxEnable	= Bit( 2),
			bitRxEnable	= Bit( 1),
			bitRxReady	= Bit( 0),
			bitEnable	= Bit( 0),
			bitReset	= Bit( 0),
			};

		// Error Bits
		enum 
		{
			errReady	= Bit(15),
			errError	= Bit(14),
			errOverrun	= Bit(13),
			errFrame	= Bit(12),
			errBreak	= Bit(11),
			errParity	= Bit(10), 
			};
		
		// Data Members
		PVDWORD	        m_pBase;
		ULONG		m_uRefs;
		UINT            m_uUnit;
		UINT            m_uClock;
		UINT	        m_uLine;
		UINT		m_uState;
		ITimer        * m_pTimer;
		IPortHandler  * m_pHandler;
		CSerialConfig   m_Config;
		IPortSwitch   * m_pSwitch;
		UINT		m_uSave;
		BYTE		m_bMask;

		// Implementation
		bool InitUart(void);
		bool InitBaudRate(void);
		bool InitFlags(void);
		bool InitFormat(void);
		WORD GetParityInit(void);
		WORD GetStopBitsInit(void);
		WORD GetDataBitsInit(void);
		WORD GetPhysical(void);
		BYTE GetDataBitsMask(void);
		void EnableEvents(void);
		void DisableEvents(void);
		void Reset(void);
		void EnablePort(void);
		void DisablePort(void);
		void EnableRecv(void);
		void EnableSend(void);
		void SwitchTxOn(bool fOn);
		bool WaitDone(void);
		void SetBaudRate(UINT uBaud);
		void ConfigurePIC(void);
		UINT FindBase(UINT iIndex) const;
		UINT FindLine(UINT iIndex) const;
	};

// End of File

#endif
