
#include "Intern.hpp"

#include "UserManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Headers
//

#include "JsonConfig.hpp"

#include "RadiusClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Manager
//

// Constructor

CUserManager::CUserManager(void)
{
	m_pSchema = NULL;

	m_pConfig = NULL;

	m_pUcon   = NULL;

	m_fLoad   = true;

	m_pLock   = Create_Mutex();

	AfxGetObject("c3.schema-generator", 0, ISchemaGenerator, m_pSchema);

	AfxGetObject("c3.config-storage", 0, IConfigStorage, m_pConfig);

	m_pConfig->AddUpdateSink(this);

	LoadUserConfig();

	StdSetRef();
}

// Destructor

CUserManager::~CUserManager(void)
{
	AfxRelease(m_pLock);

	AfxRelease(m_pConfig);

	AfxRelease(m_pSchema);
}

// IUnknown

HRESULT CUserManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IConfigUpdate);

	StdQueryInterface(IConfigUpdate);

	return E_NOINTERFACE;
}

ULONG CUserManager::AddRef(void)
{
	StdAddRef();
}

ULONG CUserManager::Release(void)
{
	StdRelease();
}

// IConfigUpdate

void CUserManager::OnConfigUpdate(char cTag)
{
	if( cTag == 'u' ) {

		m_fLoad = TRUE;
	}
}

// Operations

BOOL CUserManager::GetUserInfo(CString const &User, CAuthInfo const &Auth, CUserInfo &Info)
{
	CAutoLock Lock(m_pLock);

	if( m_fLoad ) {

		LoadUserConfig();
	}

	if( m_pUcon ) {

		CJsonConfig *pList = m_pUcon->GetChild("set.users");

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CJsonConfig *pUser = pList->GetChild(n);

			if( pUser->GetValue("user", "") == User ) {

				if( CheckUser(pUser, Auth) ) {

					LoadUser(pUser, Info);

					return TRUE;
				}

				return FALSE;
			}
		}

		CJsonConfig *pRadius = m_pUcon->GetChild("radius");

		if( pRadius ) {

			if( pRadius->GetValueAsUInt("mode", 0, 0, 1) ) {
				
				CRadiusClient Client(pRadius);

				UINT c = Client.GetUserInfo(User, Auth, Info);

				if( c == CRadiusClient::accessAllowed ) {

					if( pRadius->GetValueAsUInt("cache", 0, 0, 1) ) {

						AddToCache(Auth, Info);
					}

					return TRUE;
				}

				if( c == CRadiusClient::accessDenied ) {

					RemoveFromCache(Info.m_User);

					return FALSE;
				}
			}
		}

		if( FindInCache(User, Auth, Info) ) {

			return TRUE;
		}
	}

	return FALSE;
}

BOOL CUserManager::SetUserPass(CString const &User, CString const &From, CString const &To)
{
	CAutoLock Lock(m_pLock);

	if( m_fLoad ) {

		LoadUserConfig();
	}

	if( m_pUcon ) {

		CJsonConfig *pList = m_pUcon->GetChild("set.users");

		for( UINT n = 0; n < pList->GetCount(); n++ ) {

			CJsonConfig *pUser = pList->GetChild(n);

			if( pUser->GetValue("user", "") == User ) {

				if( !strcasecmp(pUser->GetValue("pass", ""), From) ) {

					pUser->GetJsonData()->AddValue("pass", To);

					m_pConfig->SetConfig('u', m_pUcon->GetJsonData()->GetAsText(FALSE), true);

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// Implementation

bool CUserManager::LoadUserConfig(void)
{
	for( UINT p = 0; p < 2; p++ ) {

		CString Text;

		if( m_pConfig->GetConfig('u', Text) ) {

			CAutoPointer<CJsonData> pJson(new CJsonData);

			if( pJson->Parse(Text) ) {

				delete m_pUcon;

				IConfigStorage *pPerson = NULL;

				m_pUcon = new CJsonConfig(pJson.TakeOver(), pPerson);

				m_fLoad = false;

				return true;
			}
		}

		if( p == 0 ) {

			m_pSchema->GetDefault('u', Text);

			m_pConfig->SetConfig('u', Text, true);
		}
	}

	AfxAssert(FALSE);

	return false;
}

bool CUserManager::AddToCache(CAuthInfo const &Auth, CUserInfo const &Info)
{
	// TODO -- Implement!!!

	return false;
}

bool CUserManager::RemoveFromCache(CString const &User)
{
	// TODO -- Implement!!!

	return false;
}

bool CUserManager::FindInCache(CString const &User, CAuthInfo const &Auth, CUserInfo &Info)
{
	// TODO -- Implement!!!

	return false;
}

bool CUserManager::CheckUser(CJsonConfig *pUser, CAuthInfo const &Auth)
{
	CString Password = pUser->GetValue("pass", "password");

	if( Password.GetLength() ) {

		if( Auth.m_Challenge.GetCount() == 16 ) {

			if( Auth.m_Response.GetCount() == 17 ) {

				PCBYTE pPass = PCBYTE(PCSTR(Password));

				UINT   uPass = Password.GetLength();

				UINT   uData = 1 + uPass + Auth.m_Challenge.size();

				CAutoArray<BYTE> pData(uData);

				pData[0] = Auth.m_Response.GetAt(0);

				memcpy(pData + 1, pPass, uPass);

				memcpy(pData + 1 + uPass, Auth.m_Challenge.data(), Auth.m_Challenge.size());

				BYTE hash[16];

				md5(pData, uData, hash);

				if( !memcmp(hash, Auth.m_Response.data() + 1, Auth.m_Response.size() - 1) ) {

					return true;
				}
			}

			return false;
		}
		else {
			if( !strcmp(Password, Auth.m_Password) ) {

				return true;
			}

			return false;
		}
	}

	return true;
}

bool CUserManager::LoadUser(CJsonConfig *pUser, CUserInfo &Info)
{
	Info.m_User       = pUser->GetValue("user", "");

	Info.m_Real       = pUser->GetValue("name", Info.m_User);

	Info.m_fLocal     = TRUE;

	Info.m_uWebAccess = pUser->GetValueAsUInt("access", 3, 0, 3);

	Info.m_uFtpAccess = pUser->GetValueAsUInt("ftp", 0, 0, 3);

	if( !Info.m_uFtpAccess ) {

		Info.m_uFtpAccess = (Info.m_uWebAccess == 3) ? 1 : 2;
	}
	else {
		Info.m_uFtpAccess--;
	}

	return true;
}

// End of File
