
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Edit Command Handlers

BOOL CSourceEditorWnd::OnEditGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] = 

	{	IDM_EDIT_INDENT,     MAKELONG(0x0000, 0x5000),
		IDM_EDIT_OUTDENT,    MAKELONG(0x0001, 0x5000),
		IDM_EDIT_SHOW_INFO,  MAKELONG(0x000E, 0x1000),

		};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
			}
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_SHOW_INFO:

			Src.EnableItem(CanEditShowInfo());

			return TRUE;
		
		case IDM_EDIT_JUMP_TO:

			Src.EnableItem(CanEditJumpTo());

			return TRUE;
		
		case IDM_EDIT_DELETE:
		case IDM_EDIT_CUT:

			Src.EnableItem(CanEditCut());

			return TRUE;
		
		case IDM_EDIT_COPY:

			Src.EnableItem(CanEditCopy());

			return TRUE;
		
		case IDM_EDIT_PASTE:

			Src.EnableItem(CanEditPaste());

			return TRUE;
		
		case IDM_EDIT_SELECT_ALL:

			Src.EnableItem(m_Text.GetLength() > 0);

			return TRUE;
		
		case IDM_EDIT_BLOCK_COMMENT:

			Src.EnableItem(!m_fRead && m_fSelect && m_Select.m_nTo > m_Select.m_nFrom);

			return TRUE;

		case IDM_EDIT_LINE_COMMENT:

			Src.EnableItem(!m_fRead && IsBlockSelect());

			return TRUE;

		case IDM_EDIT_INDENT:
		case IDM_EDIT_OUTDENT:

			Src.EnableItem(!m_fRead && IsBlockSelect());

			return TRUE;

		case IDM_EDIT_FIND:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_EDIT_FIND_NEXT:
		case IDM_EDIT_FIND_PREV:

			Src.EnableItem(!m_LastFind.IsEmpty());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_SHOW_INFO:

			OnEditShowInfo();

			return TRUE;
		
		case IDM_EDIT_DELETE:

			OnEditDelete();
			
			return TRUE;
		
		case IDM_EDIT_CUT:			

			OnEditCut();
			
			return TRUE;
		
		case IDM_EDIT_COPY:

			OnEditCopy();
			
			return TRUE;
		
		case IDM_EDIT_PASTE:

			OnEditPaste();

			return TRUE;

		case IDM_EDIT_SELECT_ALL:

			OnEditSelectAll();

			return TRUE;
		
		case IDM_EDIT_BLOCK_COMMENT:

			OnEditBlockComment();

			return TRUE;
		
		case IDM_EDIT_LINE_COMMENT:

			OnEditLineComment();

			return TRUE;
		
		case IDM_EDIT_INDENT:
		case IDM_EDIT_OUTDENT:

			OnEditIndent(uID == IDM_EDIT_INDENT);

			return TRUE;

		case IDM_EDIT_FIND:

			OnEditFind();

			return TRUE;

		case IDM_EDIT_FIND_NEXT:

			OnEditFindNext();

			return TRUE;

		case IDM_EDIT_FIND_PREV:

			OnEditFindPrev();

			return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::CanEditCut(void)
{
	return !m_fRead && m_fSelect;
	}

BOOL CSourceEditorWnd::CanEditCopy(void)
{
	return m_fSelect;
	}

BOOL CSourceEditorWnd::CanEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {

		UINT uType;

		if( CanAcceptDataObject(pData, uType) ) {

			pData->Release();

			return TRUE;
			}

		pData->Release();
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::CanEditShowInfo(void)
{
	if( C3OemFeature(L"FuncHelp", TRUE) ) {

		CString Text;

		if( FindSelectedWord(Text) ) {

			CPdfHelpSystem pdf(1);

			m_uHelp = pdf.FindTopic(Text);

			return TRUE;
			}
		}

	m_uHelp = NOTHING;

	return FALSE;
	}

BOOL CSourceEditorWnd::CanEditJumpTo(void)
{
	// LATER -- This is a bit trickier as we need a name
	// server to find the tag etc. and set up the jump.

	return FALSE;
	}

BOOL CSourceEditorWnd::FindSelectedWord(CString &Text)
{
	int nFrom = 0;

	int nTo   = 0;

	if( m_fSelect ) {

		nFrom = m_Select.m_nFrom;

		nTo   = m_Select.m_nTo;
		}

	if( nFrom == nTo ) {

		SyncFrom2D();

		nFrom = m_nPos;

		nTo   = m_nPos;

		while( wisalpha(m_Text[nTo]) ) {

			nTo++;
			}

		while( nFrom && wisalpha(m_Text[nFrom-1]) ) {

			nFrom--;
			}
		}

	if( nFrom < nTo ) {

		int nSize = nTo - nFrom;

		Text      = m_Text.Mid(nFrom, nSize);

		return !Text.IsEmpty() && !Text.Count(' ');
		}

	return FALSE;
	}

void CSourceEditorWnd::OnEditCut(void)
{
	OnEditCopy();

	OnEditDelete();
	}

void CSourceEditorWnd::OnEditCopy(void)
{
	if( m_fSelect ) {

		IDataObject *pData = NULL;

		if( MakeDataObject(pData) ) {

			OleSetClipboard(pData);
			
			OleFlushClipboard();

			pData->Release();
			}
		}
	}

void CSourceEditorWnd::OnEditPaste(void)
{
	IDataObject *pData = NULL;

	if( OleGetClipboard(&pData) == S_OK ) {
		
		if( AcceptDataObject(pData) ) {

			pData->Release();
			}

		pData->Release();
		}
	}

void CSourceEditorWnd::OnEditDelete(void)
{
	if( DeleteSelect() ) {
		
		UpdateAll();
		}
	}

void CSourceEditorWnd::OnEditSelectAll(void)
{
	SetSel(CRange(0, m_Text.GetLength()));
	}

void CSourceEditorWnd::OnEditBlockComment(void)
{
	HideCaret(m_hWnd);

	SyncFrom2D();

	PCTXT pComment[] = { L"/*", L"*/" };

	for( UINT n = 0; n < elements(pComment); n ++ ) {

		UINT uPos = (n == 0) ? m_Select.m_nFrom : m_Select.m_nTo;

		m_Text.Insert(uPos, pComment[n]);

		int nLen = wstrlen(pComment[n]);

		if( m_nPos == m_Select.m_nTo ) {

			m_nPos  += nLen;
			}

		m_Select.m_nTo += nLen;
		}

	ReflowFromHere();

	SyncFrom1D();

	UpdateAll();

	ShowCaret(m_hWnd);
	}

void CSourceEditorWnd::OnEditLineComment(void)
{
	HideCaret(m_hWnd);

	SelectBlock();
			
	BOOL fChange = FALSE;

	SelectSyncFrom1D();
	
	for( int r = m_SelectStart.y; r <= m_SelectEnd.y; r ++ ) {
		
		GetLineInfo(r);

		if( m_Select.m_nTo > m_nFrom ) {

			int nLen = wstrlen(L"//");

			UINT p1;

			if( FindLineComment(PCTXT(m_Text) + m_nFrom, m_nSize, p1) ) {
				
				m_Text.Delete(m_nFrom + p1, nLen);

				m_nSize -= nLen;

				m_nTo   -= nLen;
				
				SyncRange(r);

				AdjustFromHere(r, -nLen);

				if( m_nPos == m_Select.m_nTo ) {

					m_nPos  -= nLen;
					}
				
				m_Select.m_nTo -= nLen;
				}
			else {
				m_Text.Insert(m_nFrom, L"//");

				m_nSize += nLen;
				
				m_nTo   += nLen;

				SyncRange(r);

				AdjustFromHere(r, +nLen);

				if( m_nPos == m_Select.m_nTo ) {

					m_nPos  += nLen;
					}

				m_Select.m_nTo += nLen;
				}

			SelectSyncFrom1D();
			
			fChange = TRUE;
			}
		}

	SyncFrom1D();

	if( fChange ) {

		UpdateAll();
		}

	ShowCaret(m_hWnd);
	}

void CSourceEditorWnd::OnEditIndent(BOOL fIndent)
{
	HideCaret(m_hWnd);
	
	SelectBlock();
			
	if( fIndent ? Indent() : Outdent() ) {
		
		UpdateAll();
		}

	ShowCaret(m_hWnd);
	}

void CSourceEditorWnd::OnEditShowInfo(void)
{
	if( m_uHelp == NOTHING ) {

		CanEditShowInfo();
		}

	if( m_uHelp < NOTHING ) {

		CPdfHelpSystem pdf(1);

		pdf.ShowHelp(m_uHelp);

		return;
		}

	m_uHelp = NOTHING;
	}

BOOL CSourceEditorWnd::OnEditFind(void)
{
	CStringDialog Dlg;

	Dlg.SetCaption(CString(IDS_FIND_TEXT_IN));

	Dlg.SetGroup  (CString(IDS_FIND));

	Dlg.SetData   (m_LastFind);

	if( Dlg.Execute() ) {

		CString Find = Dlg.GetData();

		UINT    uPos = m_Text.Search(Find, searchContains);

		if( uPos == NOTHING ) {

			CString Text(IDS_TEXT_NOT_FOUND);

			CWnd::GetActiveWindow().Error(Text);

			return FALSE;
			}

		SetSel(CRange(uPos, uPos + Find.GetLength()));

		m_LastFind = Find;

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::OnEditFindNext(void)
{
	SyncFrom2D();

	UINT uPos = m_Text.Search(m_LastFind, m_nPos, searchContains);

	if( uPos == NOTHING ) {

		CString Text(IDS_TEXT_NOT_FOUND);

		CWnd::GetActiveWindow().Error(Text);

		return FALSE;
		}

	SetSel(CRange(uPos, uPos + m_LastFind.GetLength()));

	return TRUE;
	}

BOOL CSourceEditorWnd::OnEditFindPrev(void)
{
	// REV3 -- This is horrible, but it works for now!

	SyncFrom2D();

	UINT uPos  = 0;

	UINT uLast = NOTHING;

	UINT uTest = GetSel().m_nFrom;

	for(;;) {

		UINT uFind = m_Text.Search(m_LastFind, uPos, searchContains);

		if( uFind == NOTHING ) {

			break;
			}

		if( uFind < uTest ) {

			uLast = uFind;

			uPos  = uFind + m_LastFind.GetLength();

			continue;
			}

		break;
		}

	if( uLast == NOTHING ) {

		CString Text(IDS_TEXT_NOT_FOUND);

		CWnd::GetActiveWindow().Error(Text);

		return FALSE;
		}

	SetSel(CRange(uLast, uLast + m_LastFind.GetLength()));

	return TRUE;
	}

// Helpers

BOOL CSourceEditorWnd::FindLineComment(PCTXT pText, UINT uSize, UINT &uPos)
{
	CArray <CTokenSpan> List;

	BOOL fComment = FALSE;

	C3SyntaxTokens(List, fComment, pText, uSize);

	uPos = 0;
	
	for( UINT n = 0; n < List.GetCount(); n ++ ) {
		
		CTokenSpan const &Span = List[n];

		if( Span.m_Code == tokenLineComment ) {					
			
			return TRUE;
			}

		uPos += Span.m_uCount;
		}

	return FALSE;
	}

// End of File
