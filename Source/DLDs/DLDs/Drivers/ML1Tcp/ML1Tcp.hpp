#ifndef	INCLUDE_ML1TCP_HPP
	
#define	INCLUDE_ML1TCP_HPP

#include "../ML1Ser/ml1drv.hpp"

#include "../ML1Ser/ml1base.cpp"

#include "../ML1Ser/ml1serv.cpp"

#include "../ML1Ser/ml1drv.cpp"

#include "../ML1Ser/ml1blk.cpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP/IP Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CML1TcpDriver : public CML1Driver
{
	public:
		// Constructor
		CML1TcpDriver(void);

		// Destructor
		~CML1TcpDriver(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
							
	
	protected:

		struct SOCKET
		{
			ISocket * m_pSocket;
			BYTE	  m_pBuff[600];
			UINT	  m_uIdle;
			};

		// Device Data
		struct CML1Tcp : public CML1Dev
		{
			DWORD	 m_IP1;
			DWORD	 m_IP2;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			SOCKET * m_pClient;
			UINT	 m_uLast;
			BOOL     m_fDirty;
			BOOL	 m_fAux;
			BYTE	 m_bSockets;
			SOCKET * m_pListen;
			BOOL     m_fClose;
			PCTXT	 m_pAddr;
			BOOL	 m_fRemote;
			BYTE	 m_bIpSel;
			};
		
		// Data Members
		CML1Tcp *	m_pTcp;
		BOOL		m_fTrack;
		UINT		m_uKeep;

		// Special Block Support
		void InitSpecial(void);
		void SetSr(CML1Dev * pDev, UINT& uOffset, PWORD pData);
		void AddSpecial(CML1Dev * pDev, BYTE bCount);
		BYTE GetSpecialExtra(void);

		// Authentication Support
		BOOL DoPostAuthentication(CML1Dev * pDev);
		
		// Socket Management
		BOOL CheckSocket(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen);
		BOOL CheckSocket(CML1Tcp * pCtx);
		BOOL OpenSocket (CML1Tcp * pCtx);
		void CloseSocket(CML1Tcp * pCtx, BOOL fAbort);
		BOOL CheckListener(SOCKET * pSock);
		BOOL OpenListener (SOCKET * pSock);
		void CloseListener(SOCKET * pSock);
		void CheckIdle  (CML1Tcp * pCtx);
		void CheckSilent(CML1Tcp * pCtx, SOCKET * pSock);

		// Implementation
		void InitTcp(void);
		BOOL TxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fRespond);
		BOOL RxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen);

		// Transport
	virtual BOOL Send(void);
	virtual BOOL Recv(void);
	virtual BOOL Listen(void);	
	virtual BOOL Respond(PBYTE pBuff);
	virtual BOOL Respond(CML1Dev * pDev);
		
		// Transport Helpers
		UINT  FindRxSize(void);
		PVOID FindContext(void);

		// Helpers
		BOOL SetIP(CML1Tcp * pTcp, CML1Blk * pBlock, DWORD dwData);
		void GetIP(CML1Tcp * pTcp);
		
		
	};

#endif

// End of File
