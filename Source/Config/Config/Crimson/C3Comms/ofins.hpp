
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_OFINS_HPP
	
#define	INCLUDE_OFINS_HPP

//////////////////////////////////////////////////////////////////////////
//
// OMRON Master via FINS Device Options
//

class COmronFINSMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmronFINSMasterDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Dna;	 
		UINT	m_Da1; 	
		UINT	m_Da2;
		UINT	m_Sna;
		UINT	m_Sa1;
		UINT	m_Sa2;
		UINT	m_Mode;
								
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};



//////////////////////////////////////////////////////////////////////////
//
// OMRON UDP/IP Master via FINS Device Options
//

class COmronFINSMasterUDPDeviceOptions : public COmronFINSMasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmronFINSMasterUDPDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT	m_Addr;
		UINT    m_Addr2;
		UINT	m_Dna2;
		UINT	m_Da12;
		UINT	m_Da22;
		UINT	m_Port;
		UINT	m_Keep;
		UINT	m_Ping;
		UINT	m_Time1;
		UINT	m_Time2;
		UINT	m_Time3;
		UINT	m_Poll;
		UINT	m_NonFatal;
		UINT	m_Fatal;
								
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omron Master via FINS Driver
//

class COmronFINSMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		COmronFINSMasterDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);


		// Implementation
		void AddSpaces(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// Omron UDP/IP Master via FINS Driver
//

class COmronFINSMasterUDPDriver : public CStdCommsDriver
{
	public:
		// Constructor
		COmronFINSMasterUDPDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// OMRON UDP/IP G9SP-Series via FINS Device Options
//

class COmronFINsG9spMasterUDPDeviceOptions : public COmronFINSMasterUDPDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmronFINsG9spMasterUDPDeviceOptions(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omron UDP/IP G9SP-Series Master via FINS Driver
//

class COmronFINsG9spMasterUDPDriver : public COmronFINSMasterUDPDriver
{
	public:
		// Constructor
		COmronFINsG9spMasterUDPDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Implementation
		void AddSpaces(void);
		
	};


//////////////////////////////////////////////////////////////////////////
//
// OMRON G9SP-Series Master via FINS Device Options
//

class COmronFINsG9spMasterDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COmronFINsG9spMasterDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		UINT m_Poll;
								
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Omron G9SP-Series Master via FINS Driver
//

class COmronFINsG9spMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		COmronFINsG9spMasterDriver(void);

		// Configuration
		CLASS GetDriverConfig(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		BOOL IsReadOnly(CItem *pConfig, CAddress const &Addr);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);


		// Implementation
		void AddSpaces(void);
	};




// End of File

#endif
