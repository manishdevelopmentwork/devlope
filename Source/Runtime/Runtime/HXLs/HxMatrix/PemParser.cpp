
#include "Intern.hpp"

#include "PemParser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// PEM Parser
//

// Instantiator

static IUnknown * Create_PemParser(PCTXT pName)
{
	return New CPemParser;
}

// Registration

global void Register_PemParser(void)
{
	piob->RegisterInstantiator("crypto.pem-parser", Create_PemParser);
}

// Constructor

CPemParser::CPemParser(void)
{
	StdSetRef();
}

// IUnknown

HRESULT CPemParser::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IPemParser);

	StdQueryInterface(IPemParser);

	return E_NOINTERFACE;
}

ULONG CPemParser::AddRef(void)
{
	StdAddRef();
}

ULONG CPemParser::Release(void)
{
	StdRelease();
}

// IPemParser

BOOL CPemParser::Decode(CByteArray &Data, PCSTR pPass, PCSTR pText)
{
	CString Text(pText);

	return IntDecode(Data, pPass, Text);
}

BOOL CPemParser::Decode(CByteArray &Data, PCSTR pPass, PCBYTE pText, UINT uSize)
{
	CString Text(PCSTR(pText), uSize);

	return IntDecode(Data, pPass, Text);
}

// Implementation

BOOL CPemParser::IntDecode(CByteArray &Data, PCSTR pPass, CString &Text)
{
	CStringArray List;

	Text.Remove('\r');

	Text.TrimRight();

	if( Text.Tokenize(List, '\n') > 2 ) {

		UINT c = List.GetCount();

		UINT e = c-1;

		if( List[0].StartsWith("-----BEGIN") && List[0].EndsWith("-----") ) {

			if( List[e].StartsWith("-----END") && List[e].EndsWith("-----") ) {

				UINT s = 1;

				if( List[s] == "Proc-Type: 4,ENCRYPTED" ) {

					while( s < e && !List[s].IsEmpty() ) {

						s++;
					}

					while( s < e && List[s].IsEmpty() ) {

						s++;
					}
				}

				for( UINT n = s; n < e; n++ ) {

					CByteArray Code;

					CBase64::ToBytes(Code, List[n]);

					Data.Append(Code);
				}

				if( s > 1 ) {

					if( pPass && *pPass ) {

						CStringArray Info;

						if( List[2].Tokenize(Info, ':') == 2 ) {

							CString Line(Info[1]);

							Line.TrimBoth();

							Info.Empty();

							if( Line.Tokenize(Info, ',') == 2 ) {

								if( Info[0] == "DES-EDE3-CBC" && Info[1].size() == 16 ) {

									AfxNewAutoObject(Cipher, "crypto.cipher-des3", ICryptoSym);

									AfxNewAutoObject(Hasher, "crypto.hash-md5", ICryptoHash);

									if( Cipher && Hasher ) {

										CByteArray iv(CHex::ToBytes(Info[1]));

										CByteArray cc(PCBYTE(pPass), strlen(pPass));

										cc.Append(iv);

										CByteArray hash;

										Hasher->Initialize();

										Hasher->Update(cc);

										Hasher->Finalize();

										Hasher->GetHashData(hash);

										cc.Insert(0, hash);

										iv.Append(hash);

										Hasher->Initialize();

										Hasher->Update(cc);

										Hasher->Finalize();

										Hasher->GetHashData(hash);

										iv.Append(hash.data(), 8);

										Cipher->Initialize(iv);

										Cipher->Decrypt(Data);

										Cipher->Finalize();

										if( Data[0] == 0x30 && Data[1] == 0x82 ) {

											return TRUE;
										}
									}
								}
							}
						}
					}

					return FALSE;
				}

				return TRUE;
			}
		}
	}

	return FALSE;
}

// End of File
