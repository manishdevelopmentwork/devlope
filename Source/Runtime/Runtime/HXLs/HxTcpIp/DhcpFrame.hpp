
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DhcpFrame_HPP

#define	INCLUDE_DhcpFrame_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DhcpOption.hpp"

//////////////////////////////////////////////////////////////////////////////
//
// DHCP Magic Number
//

#define	DHCP_MAGIC	DWORD(0x63825363)

//////////////////////////////////////////////////////////////////////////
//
// DHCP Message Types
//

enum DhcpMsgType
{
	msgDISCOVER = 1,
	msgOFFER    = 2,
	msgREQUEST  = 3,
	msgDECLINE  = 4,
	msgACK	    = 5,
	msgNAK	    = 6,
	msgRELEASE  = 7,
	};

//////////////////////////////////////////////////////////////////////////
//
// DHCP Opcodes
//

enum DhcpOpcode
{
	opREQ	= 1,
	opREPLY	= 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// DHCP Flags
//

enum DhcpFlags
{
	flagBROADCAST = 0x8000,
	};

//////////////////////////////////////////////////////////////////////////
//
// DHCP Frame
//

#pragma pack(1)

struct DHCPFRAME
{
	BYTE	  m_Opcode;
	BYTE	  m_HardType;
	BYTE	  m_HardLen;
	BYTE	  m_Ops;
	DWORD     m_Id;
	WORD	  m_Secs;
	WORD	  m_Flags;
	CIpAddr   m_IpC;
	CIpAddr   m_IpY;
	CIpAddr   m_IpS;
	CIpAddr	  m_IpG;
	CMacAddr  m_Mac;
	BYTE	  m_Skip[10];
	char 	  m_Name[64];
	char	  m_File[128];
	BYTE	  m_Opts[312];
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////////
//
// DHCP Frame Wrapper
//

class CDhcpFrame : public DHCPFRAME
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Attributes
		BOOL MatchID(void) const;
		BOOL HasOptions(void) const;
		BYTE GetType(void) const;

		// Operations
		void Init(BYTE Opcode);
		UINT MakeDiscover(MACREF Mac);
		UINT MakeRequest(MACREF Mac, IPREF Client, IPREF Server, PCTXT pSuffix);
		UINT MakeRenew(MACREF Mac, IPREF Client, PCTXT pSuffix, BOOL fServer);
		UINT MakeRelease(MACREF Mac, IPREF Client, PCTXT pSuffix, BOOL fServer);
		UINT ClearOptions(void);

	private:
		// Constructor
		CDhcpFrame(void);

	protected:
		// Static Data
		static DWORD m_ThisId;

		// Implementation
		void AddType(UINT &Ptr, BYTE Type);
		void AddHost(UINT &Ptr, PCTXT pHost);
		void AddFQDN(UINT &Ptr, PCTXT pHost);
		void AddHost(UINT &Ptr, MACREF Mac);
		void AddFQDN(UINT &Ptr, MACREF Mac, PCTXT pSuffix);
		void AddRedHost(CDhcpOption &Opt, MACREF Mac);
		void AddServer(UINT &Ptr, IPREF Ip);
		void AddClient(UINT &Ptr, IPREF Ip);
		void AddParams(UINT &Ptr);
		void AddEnd(UINT &Ptr);

		// Option Location
		CDhcpOption & GetOption(UINT Ptr);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Conversion

STRONG_INLINE void CDhcpFrame::NetToHost(void)
{
	m_Id    = ::NetToHost(m_Id);

	m_Secs  = ::NetToHost(m_Secs);

	m_Flags = ::NetToHost(m_Flags);
	}

STRONG_INLINE void CDhcpFrame::HostToNet(void)
{
	m_Id    = ::HostToNet(m_Id);

	m_Secs  = ::HostToNet(m_Secs);

	m_Flags = ::HostToNet(m_Flags);
	}

// Option Location

STRONG_INLINE CDhcpOption & CDhcpFrame::GetOption(UINT Ptr)
{
	return (CDhcpOption &) m_Opts[Ptr];
	}

// End of File

#endif
