
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SYNCOL_HPP
	
#define	INCLUDE_SYNCOL_HPP

/////////////////////////////////////////////////////////////////////////
//
// Template File Name
//

#undef	TP_FILE

#define	TP_FILE TEXT(__FILE__)

/////////////////////////////////////////////////////////////////////////
//
// Synchronisation Helper
//

class CSyncCollection
{
	public:
		// Access Control 
		void LockRead (void) { m_Lock.LockRead (); }
		void FreeRead (void) { m_Lock.FreeRead (); }
		void LockWrite(void) { m_Lock.LockWrite(); }
		void FreeWrite(void) { m_Lock.FreeWrite(); }

		// Access Guards
		CRdGuard GetRdGuard(void) { return CRdGuard(m_Lock); }
		CWrGuard GetWrGuard(void) { return CWrGuard(m_Lock); }

	protected:
		// Data Members
		CAccessLock m_Lock;
	};

/////////////////////////////////////////////////////////////////////////
//
// Synchronised Collections
//

template <typename t> class CSyncArray : public CArray <t>, public CSyncCollection {};

template <typename t> class CSyncList : public CList <t>, public CSyncCollection {};

template <typename n, typename d> class CSyncMap : public CMap<n, d>, public CSyncCollection {};

template <typename t> class CSyncTree : public CTree <t>, public CSyncCollection {};

// End of File

#endif
