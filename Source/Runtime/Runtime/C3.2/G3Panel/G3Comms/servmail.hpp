
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SERVMAIL_HPP

#define	INCLUDE_SERVMAIL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMailManager;
class CMailAddress;
class CMailContacts;
class CMailInfo;

//////////////////////////////////////////////////////////////////////////
//
// Mail Manager Server
//

class CMailManager :
	public CServiceItem,
	public IServiceMail,
	public IMsgSendNotify,
	public IMsgRecvNotify
{
public:
	// Constructor
	CMailManager(void);

	// Destructor
	~CMailManager(void);

	// Initialization
	void Load(PCBYTE &pData);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IService
	UINT GetID(void);

	// IServiceMail
	BOOL SendFile(UINT  uIndex, CFilename Name, PCTXT pSubject, DWORD dwFlags);
	BOOL SendFile(UINT  uIndex, CFilename Name);
	BOOL SendFile(CString Rcpt, CFilename Name);
	BOOL SendFile(CString Rcpt, CFilename Name, DWORD dwAck);
	BOOL SendMail(UINT  uIndex, PCTXT pSubject, PCTXT pMessage);
	BOOL SendMail(UINT  uIndex, PCTXT pSubject, PCTXT pMessage, DWORD dwSkip);
	BOOL SendMail(UINT  uIndex, PCTXT pType, DWORD dwTime, CUnicode const &Text);
	BOOL SendMail(CString Rect, PCTXT pSubject, PCTXT pMessage);
	BOOL SendMail(CString Rect, PCTXT pSubject, PCTXT pMessage, DWORD dwAck);

	// IMsgSendNotify
	void METHOD OnMsgSent(CMsgPayload const *pMessage, BOOL fOkay);

	// IMsgRecvNotify
	BOOL METHOD OnMsgRecv(PCTXT pName, CMsgPayload const *pMessage);

	// Item Properties
	CMailContacts   * m_pContacts;
	CCodedItem      * m_pPanelName;
	UINT              m_DateForm;
	CCodedItem      * m_pRelay;
	CCodedItem      * m_pOnSMS;

protected:
	// Configuration
	struct CConfig
	{
		CString m_PanelName;
		UINT	m_uRelay;
	};

	// Data Members
	CConfig         m_Cfg;
	IMutex	      * m_pLock;
	CString		m_XpNames[3];
	UINT		m_uSend[3];
	UINT		m_uRecv;

	// Implementation
	BOOL SendMail(UINT uIndex, CMailInfo &Info);
	BOOL SendMail(CString Rcpt, CMailInfo &Info);
	BOOL SendMail(CMailInfo &Info);
	void FindConfig(CConfig &Cfg);
	void SetAckData(DWORD dwAck, DWORD dwData);
	void SetAckMask(DWORD dwAck, DWORD dwMask);
	void RegisterWithBroker(void);
	void RevokeFromBroker(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Mail Address
//

class CMailAddress : public CCodedHost
{
public:
	// Constructor
	CMailAddress(void);

	// Destructor
	~CMailAddress(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Operations
	void PreRegister(void);
	BOOL Parse(CStringArray &Addr);

	// Item Properties
	CString      m_Name;
	CCodedText * m_pAddr;
	CCodedItem * m_pEnable;
};

//////////////////////////////////////////////////////////////////////////
//
// Mail Contacts
//

class CMailContacts : public CItem
{
public:
	// Constructor
	CMailContacts(void);

	// Destructor
	~CMailContacts(void);

	// Initialization
	void Load(PCBYTE &pData);

	// Operations
	void PreRegister(void);

	// Item Properties
	UINT            m_uCount;
	CMailAddress ** m_ppAddr;
};

//////////////////////////////////////////////////////////////////////////
//
// Mail List Support
//

class CMailInfo
{
public:
	// Data Members
	CStringArray	m_Rcpt;
	CString         m_Subject;
	CString         m_Message;
	CFilename       m_File;
	DWORD           m_dwDone;
	DWORD		m_dwAck;
};

// End of File

#endif
