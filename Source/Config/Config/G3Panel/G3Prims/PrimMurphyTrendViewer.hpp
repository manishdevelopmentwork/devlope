
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimMurphyTrendViewer_HPP
	
#define	INCLUDE_PrimMurphyTrendViewer_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "viewer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- FW Murphy Trend Viewer
//

class CPrimMurphyTrendViewer : public CPrimViewer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimMurphyTrendViewer(void);

		// Initial Values
		void SetInitValues(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Overridables
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	      m_Width;
		CCodedItem  * m_pPenMask;
		UINT	      m_PenWeight;
		UINT          m_ShowData;
		UINT          m_ShowCursor;
		UINT          m_DataBox;
		UINT	      m_FontTitle;
		UINT	      m_FontData;
		UINT	      m_GridTime;
		UINT	      m_GridMode;
		UINT	      m_GridMinor;
		UINT	      m_GridMajor;
		CCodedItem  * m_pGridMin;
		CCodedItem  * m_pGridMax;
		UINT	      m_Precise;
		CPrimColor  * m_pColTitle;
		CPrimColor  * m_pColLabel;
		CPrimColor  * m_pColData;
		CPrimColor  * m_pColMajor;
		CPrimColor  * m_pColMinor;
		CPrimColor  * m_pColCursor;
		CPrimColor  * m_pColPen[16];
		CCodedText  * m_pBtnPgLeft;
		CCodedText  * m_pBtnLeft;
		CCodedText  * m_pBtnLive;
		CCodedText  * m_pBtnRight;
		CCodedText  * m_pBtnPgRight;
		CCodedText  * m_pBtnIn;
		CCodedText  * m_pBtnOut;
		CCodedText  * m_pBtnLoad;
		CDispFormat * m_pFormat;
		UINT	      m_fUseLoad;
		CCodedText  * m_pRoot;
		CCodedText  * m_pFileName;
		CCodedItem  * m_pZoomMax;
		CCodedItem  * m_pZoomMin;
		CCodedItem  * m_pZoomInit;
		CCodedItem  * m_pPerLane;
		CCodedItem  * m_pTimeStart;
		CCodedItem  * m_pTimeEnd;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Overridables
		BOOL MakeList(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
