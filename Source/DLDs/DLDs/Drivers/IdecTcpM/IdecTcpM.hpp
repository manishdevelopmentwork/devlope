#ifndef	INCLUDE_IdecTcpM_HPP
	
#define	INCLUDE_IdecTcpM_HPP

#include "idecBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Idec MicroSmart TCP/IP Master Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

class CIdecMicroSmartTcpMasterDriver : public CIdecBaseDriver
{
	public:
		// Constructor
		CIdecMicroSmartTcpMasterDriver(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		DEFMETH(UINT)  DevCtrl(void *pContext, UINT uFunc, PCTXT Value);
				
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		
	protected:
		// Device Context
		struct CTcpCtx : CIdecBaseDriver::CContext
		{
			DWORD	 m_IP1;
			DWORD	 m_IP2;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL     m_fDirty;
			BOOL	 m_fAux;
			};

		// Data Members
		CTcpCtx *  m_pTcp;
		UINT	   m_uKeep;
		
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Overridable Methods
		BOOL Send(void);
		BYTE GetFrame(void);
		UINT GetMaxWords(void);
		UINT GetMaxBits(void);
				
		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);


	};

#endif

// End of File
