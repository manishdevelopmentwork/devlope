
#include "intern.hpp"

#include "FtpSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// FTP Session
//

// TODO -- Add support UTF8 filenames?

// TODO -- Add support for root directory?

// Externals

clink int sync(void);

// Path Separators

static char const sepLocal  = '\\';

static char const sepRemote = '/';

// Buffer Sizes

static UINT const buffMisc = 64;

static UINT const buffPath = 256;

// Time Constants

static UINT const timeSockTimeout = 30000;

static UINT const timePollDelay   = 10;

static UINT const timeLinkTimeout = 60000;

// Constructor

CFtpSession::CFtpSession(void)
{
	m_pDataSock = NULL;

	m_uState    = stateWaitRouter;
}

// Destructor

CFtpSession::~CFtpSession(void)
{
	CloseDataSocket();
}

// Operations

void CFtpSession::SetConfig(CFtpConfig const *pConfig)
{
	m_pConfig = pConfig;
}

BOOL CFtpSession::Poll(void)
{
	if( m_uState == stateWaitRouter ) {

		m_uState = stateMakeSocket;

		return TRUE;
	}

	if( m_uState == stateMakeSocket ) {

		if( OpenCmdSocket(WORD(m_pConfig->m_uCmdPort), TRUE) ) {

			m_uState = stateWaitSocket;

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState == stateWaitSocket ) {

		if( WaitCmdSocket() ) {

			if( CheckCmdSocket() ) {

				InitSession();

				m_uTime = GetTickCount();
			}
			else
				m_uState = stateMakeSocket;

			return TRUE;
		}

		return FALSE;
	}

	if( m_uState >= stateLogonUser ) {

		if( CheckCmdSocket() ) {

			if( RecvCommand() ) {

				OnCommand();

				m_uTime = GetTickCount();

				return TRUE;
			}
			else {
				if( GetTickCount() - m_uTime >= ToTicks(timeLinkTimeout) ) {

					SendReply(221, "Session timeout");

					KillSession();

					return TRUE;
				}
			}
		}
		else {
			AbortDataSocket();

			m_uState = stateMakeSocket;

			return TRUE;
		}

		return FALSE;
	}

	return FALSE;
}

// Socket Management

BOOL CFtpSession::OpenDataSocket(UINT uPort, BOOL fSend)
{
	WORD wPort = WORD(uPort);

	AbortDataSocket();

	if( m_fTlsData && CreateTlsContext() ) {

		if( wPort ) {

			// Active mode does not make sense in TLS mode
			// as it means we shall have to be a client and a
			// server, and right now we don't like that.

			m_pDataSock = NULL;
		}
		else
			m_pDataSock = m_pTls->CreateSocket(NULL);
	}
	else {
		if( m_pConfig->m_uTlsMode == 2 ) {

			// TLS is required and we haven't had a PROT command
			// so fail the data connection. This won't happen as
			// clients using TLS will have sent the PROT.

			m_pDataSock = NULL;
		}
		else {
			m_pDataSock = NULL;

			AfxNewObject("sock-tcp", ISocket, m_pDataSock);
		}
	}

	if( m_pDataSock ) {

		if( !wPort || fSend ) {

			UINT uOption = OPT_SEND_QUEUE;

			UINT uValue  = 255;

			m_pDataSock->SetOption(uOption, uValue);
		}

		if( !wPort || !fSend ) {

			UINT uOption = OPT_RECV_QUEUE;

			UINT uValue  = 255;

			m_pDataSock->SetOption(uOption, uValue);
		}

		if( wPort ) {

			IPADDR IP;

			if( m_pCmdSock->GetRemote(IP) == S_OK ) {

				if( m_pDataSock->Connect(IPREF(IP), wPort) == S_OK ) {

					return TRUE;
				}
			}

			AbortDataSocket();

			return FALSE;
		}
		else {
			if( m_pDataSock->Listen(0) == S_OK ) {

				return TRUE;
			}
			else {
				AbortDataSocket();

				return FALSE;
			}
		}
	}

	return FALSE;
}

BOOL CFtpSession::WaitDataSocket(void)
{
	SetTimer(timeSockTimeout);

	while( GetTimer() ) {

		UINT Phase;

		m_pDataSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			return TRUE;
		}

		if( Phase == PHASE_CLOSING ) {

			return TRUE;
		}

		if( Phase == PHASE_ERROR ) {

			break;
		}

		Sleep(timePollDelay);
	}

	return FALSE;
}

void CFtpSession::CloseDataSocket(void)
{
	if( m_pDataSock ) {

		m_pDataSock->Close();

		m_pDataSock->Release();

		m_pDataSock = NULL;

		m_uPort     = NOTHING;
	}
}

void CFtpSession::AbortDataSocket(void)
{
	if( m_pDataSock ) {

		m_pDataSock->Abort();

		m_pDataSock->Release();

		m_pDataSock = NULL;

		m_uPort     = NOTHING;
	}
}

// Session Control

void CFtpSession::InitSession(void)
{
	// Don't give our system type away in the welcome banner!

	SendReply(220, "Hello");

	ClearSession();
}

void CFtpSession::ClearSession(void)
{
	m_Prefix   = "/";

	m_fTlsData = FALSE;

	m_fASCII   = TRUE;

	m_fRecord  = FALSE;

	m_uRestart = 0;

	m_uPort    = NOTHING;

	m_uState   = stateLogonUser;

	m_Rename.Empty();

	FindLocalPrefix();
}

void CFtpSession::KillSession(void)
{
	CloseCmdSocket();

	AbortDataSocket();

	if( m_pTls ) {

		m_pTls->Release();

		m_pTls = NULL;
	}

	m_uState = stateMakeSocket;
}

// Command Processing

void CFtpSession::OnCommand(void)
{
	struct CCmd
	{
		char m_sName[8];
		void (CFtpSession::*m_pFunc)(PTXT);
		bool m_fUnauth;
	};

	static CCmd const Cmd[] =
	{
		// Keep this ordered for bsearch!

		{ "AUTH", &CFtpSession::OnAuth, true  },
		{ "CDUP", &CFtpSession::OnCDUP, false },
		{ "CWD",  &CFtpSession::OnCWD , false },
		{ "DELE", &CFtpSession::OnDele, false },
		{ "FEAT", &CFtpSession::OnFeat, true  },
		{ "LIST", &CFtpSession::OnList, false },
		{ "MDTM", &CFtpSession::OnMDTM, false },
		{ "MKD",  &CFtpSession::OnMKD , false },
		{ "MODE", &CFtpSession::OnMode, false },
		{ "NLST", &CFtpSession::OnList, false },
		{ "NOOP", &CFtpSession::OnNoop, false },
		{ "PASS", &CFtpSession::OnPass, true  },
		{ "PASV", &CFtpSession::OnPasv, false },
		{ "PBSZ", &CFtpSession::OnPBSZ, true  },
		{ "PORT", &CFtpSession::OnPort, false },
		{ "PROT", &CFtpSession::OnProt, true  },
		{ "PWD",  &CFtpSession::OnPWD , false },
		{ "QUIT", &CFtpSession::OnQuit, true  },
		{ "REIN", &CFtpSession::OnRein, true  },
		{ "REST", &CFtpSession::OnRest, false },
		{ "RETR", &CFtpSession::OnRetr, false },
		{ "RMD",  &CFtpSession::OnRMD , false },
		{ "RNFR", &CFtpSession::OnRnFr, false },
		{ "RNTO", &CFtpSession::OnRnTo, false },
		{ "SIZE", &CFtpSession::OnSize, false },
		{ "STOR", &CFtpSession::OnStor, false },
		{ "STRU", &CFtpSession::OnStru, false },
		{ "SYST", &CFtpSession::OnSyst, false },
		{ "TYPE", &CFtpSession::OnType, false },
		{ "USER", &CFtpSession::OnUser, true  },
		{ "XCWD", &CFtpSession::OnCWD , false },
		{ "XMKD", &CFtpSession::OnMKD , false },
		{ "XPWD", &CFtpSession::OnPWD , false },
		{ "XRMD", &CFtpSession::OnRMD , false },

	};

	PCTXT pSep = " \t\r\n";

	UINT  uLen = strcspn(m_sCmdData, pSep);

	m_sCmdData[uLen] = 0;

	int n1 = 0;

	int n2 = elements(Cmd);

	while( n2 > n1 ) {

		int n = (n1 + n2 - 1) / 2;

		int c = strcasecmp(m_sCmdData, Cmd[n].m_sName);

		if( !c ) {

			if( !Cmd[n].m_fUnauth && m_uState < stateActive ) {

				SendReply(530, "Please login with USER and PASS");

				return;
			}

			PTXT pLine = m_sCmdData + uLen + 1;

			UINT uGap  = strspn(pLine, pSep);

			(this->*Cmd[n].m_pFunc)(pLine + uGap);

			return;
		}

		(c < 0) ? (n2 = n) : (n1 = n + 1);
	}

	SendReply(500, "Unknown command");
}

void CFtpSession::OnUser(PTXT pLine)
{
	if( m_uState == stateLogonUser ) {

		if( m_pConfig->m_uTlsMode == 2 && !m_pTls ) {

			SendReply(421, "TLS required so closing session");

			KillSession();

			return;
		}

		if( strlen(pLine) > sizeof(m_sUser) - 1 ) {

			SendReply(500, "Parameter too long");
		}
		else {
			strcpy(m_sUser, pLine);

			if( !strcasecmp(m_sUser, "ftp") || !strcasecmp(m_sUser, "anonymous") ) {

				SendReply(331, "Anonymous access okay, send email as password");

				m_fAnon  = TRUE;
			}
			else {
				SendReply(331, "Send password for this user");

				m_fAnon = FALSE;
			}

			m_uState = stateLogonPass;
		}

		return;
	}

	SendReply(503, "Command not expected");
}

void CFtpSession::OnPass(PTXT pLine)
{
	if( m_uState == stateLogonPass ) {

		if( strlen(pLine) > sizeof(m_sPass) - 1 ) {

			SendReply(500, "Parameter too long");
		}
		else {
			strcpy(m_sPass, pLine);

			if( m_fAnon ) {

				if( m_pConfig->m_uAnon ) {

					SendReply(230, "Anonymous user logged in");

					m_fWrite = (m_pConfig->m_uAnon == 2);

					m_uState = stateActive;
				}
				else {
					SendReply(530, "Invalid credentials");

					m_uState = stateLogonUser;
				}
			}
			else {
				AfxGetAutoObject(pPxe, "pxe", 0, ICrimsonPxe);

				if( pPxe ) {

					CAuthInfo Auth;

					CUserInfo Info;

					Auth.m_Password = m_sPass;

					if( pPxe->GetUserInfo(m_sUser, Auth, Info) ) {

						if( Info.m_uFtpAccess ) {

							SendReply(230, "User logged in");

							m_fWrite = (Info.m_uFtpAccess == 2);

							m_uState = stateActive;

							return;
						}
					}
				}

				SendReply(530, "Invalid credentials");

				SendReply(221, "Session terminated");

				KillSession();
			}
		}

		return;
	}

	SendReply(503, "Command not expected");
}

void CFtpSession::OnAuth(PTXT pLine)
{
	if( m_pConfig->m_uTlsMode ) {

		if( !strcasecmp(pLine, "TLS") ) {

			if( CreateTlsContext() ) {

				SendReply(200, "Switching to TLS");

				SwitchCmdSocket();

				ClearSession();

				return;
			}

			SendReply(431, "Security mechanism unavailable");

			return;
		}

		SendReply(504, "Unknown security mechanism");

		return;
	}

	SendReply(500, "Unknown command");
}

void CFtpSession::OnPBSZ(PTXT pLine)
{
	if( m_pConfig->m_uTlsMode ) {

		if( m_pTls ) {

			SendReply(200, "PBSZ=0");

			return;
		}

		SendReply(503, "Command not expected");

		return;
	}

	SendReply(500, "Unknown command");
}

void CFtpSession::OnProt(PTXT pLine)
{
	if( m_pConfig->m_uTlsMode ) {

		if( m_pTls ) {

			if( pLine[0] == 'P' && pLine[1] == 0 ) {

				m_fTlsData = TRUE;

				SendReply(200, "Data connection set to TLS");

				return;
			}

			if( pLine[0] == 'C' && pLine[1] == 0 ) {

				m_fTlsData = FALSE;

				SendReply(200, "Data connection set to clear");

				return;
			}

			SendReply(504, "Protection not supported");

			return;
		}

		SendReply(503, "Command not expected");

		return;
	}

	SendReply(500, "Unknown command");
}

void CFtpSession::OnRein(PTXT pLine)
{
	SendReply(220, "Ready for New user");

	ClearSession();
}

void CFtpSession::OnQuit(PTXT pLine)
{
	SendReply(221, "Cheerio");

	KillSession();
}

void CFtpSession::OnSyst(PTXT pLine)
{
	if( m_fAnon ) {

		// Per security best practice, do not send
		// system information to anonymous clients.

		SendReply(502, "No clues, anon!");

		return;
	}

	SendReply(215, "UNIX");
}

void CFtpSession::OnFeat(PTXT pLine)
{
	CString Text;

	Text += "211-Feature support:\r\n";

	if( m_pConfig->m_uTlsMode ) {

		Text += " AUTH TLS\r\n";
		Text += " PBSZ\r\n";
		Text += " PROT C;P;\r\n";
	}

	Text += " SIZE\r\n";
	Text += " MDTM\r\n";
	Text += " REST STREAM\r\n";

	Text += "211 END\r\n";

	Send(Text);
}

void CFtpSession::OnPort(PTXT pLine)
{
	PTXT pScan    = pLine;

	BYTE bData[6] = { 0 };

	for( UINT n = 0; pScan && n < 6; n++ ) {

		bData[n] = BYTE(atoi(pScan));

		pScan    = strchr(pScan, ',');

		pScan    = pScan ? pScan + 1 : NULL;
	}

	if( (m_uPort = MAKEWORD(bData[5], bData[4])) ) {

		SendReply(200, "PORT command successful");

		return;
	}

	SendReply(501, "Syntax error");
}

void CFtpSession::OnPasv(PTXT pLine)
{
	if( m_pDataSock || OpenDataSocket(0, TRUE) ) {

		char s[buffMisc];

		DWORD Addr;

		WORD  Port;

		m_pDataSock->GetLocal((IPADDR &) Addr, Port);

		m_pCmdSock->GetLocal((IPADDR &) Addr);

		SPrintf(s,
			"Entering passive mode (%u,%u,%u,%u,%u,%u)",
			PBYTE(&Addr)[0],
			PBYTE(&Addr)[1],
			PBYTE(&Addr)[2],
			PBYTE(&Addr)[3],
			HIBYTE(Port),
			LOBYTE(Port)
		);

		SendReply(227, s);

		m_uPort = 0;

		return;
	}

	SendReply(425, "Can't open data connection");
}

void CFtpSession::OnType(PTXT pLine)
{
	if( pLine[0] == 'A' ) {

		if( !pLine[1] || pLine[1] == 'N' ) {

			m_fASCII = TRUE;

			SendReply(200, "Type set to ASCII");

			return;
		}
	}

	if( pLine[0] == 'I' ) {

		m_fASCII = FALSE;

		SendReply(200, "Type set to BINARY");

		return;
	}

	SendReply(504, "Type not supported");
}

void CFtpSession::OnMode(PTXT pLine)
{
	if( pLine[0] == 'S' ) {

		SendReply(200, "Mode set to STREAM");

		return;
	}

	SendReply(504, "Mode not supported");
}

void CFtpSession::OnStru(PTXT pLine)
{
	if( pLine[0] == 'F' ) {

		m_fRecord = FALSE;

		SendReply(200, "Structure set to FILE");

		return;
	}

	if( FALSE ) {

		if( pLine[0] == 'R' ) {

			m_fRecord = TRUE;

			SendReply(200, "Structure set to RECORD");

			return;
		}
	}

	SendReply(504, "Structure not supported");
}

void CFtpSession::OnRest(PTXT pLine)
{
	m_uRestart = strtoul(pLine, NULL, 10);

	SendReply(200, "Restart point set");
}

void CFtpSession::OnRetr(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( m_uPort < NOTHING ) {

		if( FileToLocal(pLine) ) {

			CAutoFile File(pLine, "r");

			if( File ) {

				if( m_uRestart ) {

					File.Seek(m_uRestart);
				}

				if( !m_uPort || OpenDataSocket(m_uPort, TRUE) ) {

					NotifyConnection();

					if( WaitDataSocket() ) {

						if( SendFile(m_pDataSock, File) ) {

							NotifyComplete();

							m_uRestart = 0;

							return;
						}

						AbortDataSocket();

						SendReply(426, "Transfer aborted");

						m_uRestart = 0;

						return;
					}
				}

				SendReply(425, "Can't open data connection");

				m_uRestart = 0;

				return;
			}
		}

		SendReply(550, "File cannot be found");

		AbortDataSocket();

		m_uRestart = 0;

		return;
	}

	SendReply(503, "No PORT or PASV command received");

	m_uRestart = 0;
}

void CFtpSession::OnStor(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( m_fWrite ) {

		if( m_uPort < NOTHING ) {

			if( FileToLocal(pLine) ) {

				CAutoFile File(pLine, "r+", "w+");

				if( File ) {

					if( m_uRestart ) {

						File.Seek(m_uRestart);
					}

					if( !m_uPort || OpenDataSocket(m_uPort, FALSE) ) {

						NotifyConnection();

						if( WaitDataSocket() ) {

							if( RecvFile(m_pDataSock, File) ) {

								File.Truncate();

								File.Close();

								sync();

								NotifyComplete();

								m_uRestart = 0;

								return;
							}

							AbortDataSocket();

							SendReply(426, "Transfer aborted");

							m_uRestart = 0;

							return;
						}
					}

					SendReply(425, "Can't open data connection");

					m_uRestart = 0;

					return;
				}
			}

			SendReply(550, "File cannot be created");

			AbortDataSocket();

			m_uRestart = 0;

			return;
		}

		SendReply(503, "No PORT or PASV command received");

		m_uRestart = 0;

		return;
	}

	SendReply(550, "Access is denied");

	AbortDataSocket();

	m_uRestart = 0;
}

void CFtpSession::OnSize(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( FileToLocal(pLine) ) {

		struct stat s;

		if( !stat(pLine, &s) ) {

			CPrintf Text("%u", s.st_size);

			SendReply(213, PCTXT(Text));

			return;
		}
	}

	SendReply(550, "File cannot be found");
}

void CFtpSession::OnMDTM(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( FileToLocal(pLine) ) {

		struct stat s;

		if( !stat(pLine, &s) ) {

			struct tm *tm = gmtime(&s.st_mtime);

			CPrintf Text("%4.4u%2.2u%2.2u%2.2u%2.2u%2.2u",
				     1900 + tm->tm_year,
				     1    + tm->tm_mon,
				     tm->tm_mday,
				     tm->tm_hour,
				     tm->tm_min,
				     tm->tm_sec
			);

			SendReply(213, PCTXT(Text));

			return;
		}
	}

	SendReply(550, "File cannot be found");
}

void CFtpSession::OnDele(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( m_fWrite ) {

		if( FileToLocal(pLine) ) {

			if( !unlink(pLine) ) {

				sync();

				SendReply(250, "File deleted");

				return;
			}

			SendReply(550, "File cannot be found");

			return;
		}
	}

	SendReply(550, "Access is denied");
}

void CFtpSession::OnNoop(PTXT pLine)
{
	SendReply(200, "OK");
}

void CFtpSession::OnMKD(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( m_fWrite ) {

		char sPath[buffPath];

		strcpy(sPath, pLine);

		if( FileToLocal(sPath) ) {

			if( !mkdir(sPath, 0) ) {

				sync();

				char sLine[buffPath];

				SPrintf(sLine, "\"%s\" created", pLine);

				SendReply(257, sLine);

				return;
			}

			SendReply(550, "Directory cannot be created");

			return;
		}
	}

	SendReply(550, "Access is denied");
}

void CFtpSession::OnRMD(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( m_fWrite ) {

		if( FileToLocal(pLine) ) {

			if( !rmdir(pLine) ) {

				sync();

				SendReply(250, "RMD successful");

				return;
			}

			SendReply(550, "Directory cannot be removed");

			return;
		}
	}

	SendReply(550, "Access is denied");
}

void CFtpSession::OnPWD(PTXT pLine)
{
	char sLine[buffPath];

	SPrintf(sLine, "\"%s\" is the current directory", PCSTR(m_Prefix));

	SendReply(257, sLine);
}

void CFtpSession::OnCWD(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	CString Prefix;

	CString Local;

	if( pLine[0] == sepRemote ) {

		Prefix = pLine;
	}
	else {
		Prefix  = m_Prefix;

		Prefix += pLine;
	}

	if( !Prefix.EndsWith(sepRemote) ) {

		Prefix += sepRemote;
	}

	if( !chdir(Local = FindLocalPrefix(Prefix)) ) {

		m_Prefix = Prefix;

		m_Local  = Local;

		SendReply(250, "CWD successful");

		return;
	}

	SendReply(550, "Directory cannot be found");
}

void CFtpSession::OnCDUP(PTXT pLine)
{
	UINT uSize;

	if( (uSize = m_Prefix.GetLength()) > 1 ) {

		UINT uFind = m_Prefix.FindRev(sepRemote, uSize-2);

		m_Prefix.Delete(uFind + 1, NOTHING);

		FindLocalPrefix();

		SendReply(250, "CDUP successful");

		return;
	}

	SendReply(550, "Directory cannot be found");
}

void CFtpSession::OnList(PTXT pLine)
{
	if( m_uPort < NOTHING ) {

		if( !m_uPort || OpenDataSocket(m_uPort, TRUE) ) {

			NotifyConnection();

			if( WaitDataSocket() ) {

				StripOptions(pLine);

				if( SendDirectory(pLine) ) {

					NotifyComplete();

					return;
				}

				SendReply(426, "Transfer aborted");

				AbortDataSocket();

				return;
			}
		}

		SendReply(425, "Can't open data connection");

		return;
	}

	SendReply(503, "No PORT or PASV command received");
}

void CFtpSession::OnRnFr(PTXT pLine)
{
	if( !*pLine ) {

		SendReply(501, "Invalid filename");

		return;
	}

	if( m_fWrite ) {

		if( FileToLocal(pLine) ) {

			struct stat s;

			if( !stat(pLine, &s) ) {

				m_Rename = pLine;

				SendReply(350, "Awaiting further information");

				return;
			}
		}

		SendReply(550, "File cannot be found");

		return;
	}

	SendReply(550, "Access is denied");
}

void CFtpSession::OnRnTo(PTXT pLine)
{
	if( !m_Rename.IsEmpty() ) {

		if( !strchr(pLine, sepLocal) && !strchr(pLine, sepRemote) ) {

			if( !rename(m_Rename, pLine) ) {

				sync();

				SendReply(250, "File renamed successfully");

				m_Rename.Empty();

				return;
			}

			SendReply(553, "File cannot be renamed");

			m_Rename.Empty();

			return;
		}

		SendReply(553, "Files cannot be moved");

		m_Rename.Empty();

		return;
	}

	SendReply(503, "No RNFR received");
}

// Implementation

void CFtpSession::FindLocalPrefix(void)
{
	m_Local = FindLocalPrefix(m_Prefix);
}

CString CFtpSession::FindLocalPrefix(CString &Prefix)
{
	CString Local;

	if( Prefix.StartsWith("/DRIVE-") ) {

		if( Prefix[7] >= 'C' && Prefix[7] <= 'E' ) {

			Local = Prefix;

			Local.Delete(0, 6);

			Local.SetAt(0, Local[1]);

			Local.SetAt(1, ':');
		}
		else {
			Local = "C:" + Prefix;
		}
	}
	else {
		Local = "C:" + Prefix;
	}

	Local.Replace(sepRemote, sepLocal);

	return Local;
}

BOOL CFtpSession::FileToLocal(PTXT pPath)
{
	UINT n;

	for( n = 0; pPath[n]; n++ ) {

		if( pPath[n] == sepRemote ) {

			pPath[n] = sepLocal;
		}
	}

	if( n && pPath[--n] == sepLocal ) {

		pPath[n] = 0;
	}

	if( pPath[0] == sepLocal ) {

		return TRUE;
	}

	char sPath[buffPath];

	PathJoin(sPath, m_Local, pPath);

	strcpy(pPath, sPath);

	return TRUE;
}

void CFtpSession::StripOptions(PTXT &pLine)
{
	while( *pLine == '-' ) {

		while( *pLine ) {

			if( isspace(*pLine++) ) {

				while( isspace(*pLine) ) {

					pLine++;
				}

				break;
			}
		}
	}
}

BOOL CFtpSession::SendDirectory(PTXT pPath)
{
	if( !chdir(m_Local) ) {

		char sPerm[buffMisc];

		strcpy(sPerm, m_fWrite ? "-rw-rw-rw-" : "-r--r--r--");

		CAutoDirentList List;

		if( List.Scan(".") ) {

			for( UINT n = 0; n < List.GetCount(); n++ ) {

				struct dirent *d = List[n];

				struct stat    s = { 0 };

				if( d->d_type == DT_DIR ) {

					if( !strcmp(d->d_name, ".") || !strcmp(d->d_name, "..") ) {

						continue;
					}

					sPerm[0] = 'd';
				}
				else {
					sPerm[0] = '-';
				}

				List.stat(".", n, &s);

				struct tm *tm = gmtime(&s.st_mtime);

				char sLine[buffPath];

				SPrintf(sLine,
					"%s 1 owner group %10u %3.3s %2.2u %2.2u:%2.2u %s%s",
					sPerm,
					s.st_size,
					GetMonthName(1 + tm->tm_mon),
					tm->tm_mday,
					tm->tm_hour,
					tm->tm_min,
					d->d_name,
					m_fASCII ? "\n" : "\r\n"
				);

				if( !Send(m_pDataSock, sLine) ) {

					return FALSE;
				}
			}
		}

		if( m_Prefix == "/" ) {

			sPerm[0] = 'd';

			for( int c = 'D'; c <= 'E'; c++ ) {

				char sLine[buffPath];

				SPrintf(sLine,
					"%s 1 owner group %10u %3.3s %2.2u %2.2u:%2.2u DRIVE-%c%s",
					sPerm,
					0,
					GetMonthName(1),
					1,
					0,
					0,
					c,
					m_fASCII ? "\n" : "\r\n"
				);

				if( !Send(m_pDataSock, sLine) ) {

					return FALSE;
				}
			}
		}
	}

	return TRUE;
}

PCTXT CFtpSession::GetMonthName(UINT uMonth)
{
	switch( uMonth ) {

		case  1: return "Jan";
		case  2: return "Feb";
		case  3: return "Mar";
		case  4: return "Apr";
		case  5: return "May";
		case  6: return "Jun";
		case  7: return "Jul";
		case  8: return "Aug";
		case  9: return "Sep";
		case 10: return "Oct";
		case 11: return "Nov";
		case 12: return "Dec";

	}

	return "XXX";
}

void CFtpSession::NotifyConnection(void)
{
	if( m_uPort ) {

		SendReply(150, "Opening data connection");

		return;
	}

	SendReply(125, "Using open data connection");
}

void CFtpSession::NotifyComplete(void)
{
	// I do not like this ordering, and especially the sleep,
	// but it does not work well with Firefox otherwise, asking
	// for credentials all the time. We should figure out why.

	SendReply(226, "Transfer completed");

	Sleep(50);

	CloseDataSocket();
}

// End of File
