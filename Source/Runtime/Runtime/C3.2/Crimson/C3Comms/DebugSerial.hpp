
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CAnsiDebugConsole;

//////////////////////////////////////////////////////////////////////////
//
// Debug Serial Driver
//

class CDebugSerialDriver : public CCommsDriver, public IDiagConsole
{
	public:
		// Constructor
		CDebugSerialDriver(void);

		// Destructor
		~CDebugSerialDriver(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

		// Entry Points
		DEFMETH(void) Close(void);
		DEFMETH(void) Service(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagConsole
		DEFMETH(void) Write(PCTXT pText);

	protected:
		// Data Members
		IMutex            * m_pMutex;
		CAnsiDebugConsole * m_pConsole;
	};

// End of File
