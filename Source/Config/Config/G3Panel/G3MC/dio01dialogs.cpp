
#include "intern.hpp"

#include "legacy.h"

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Resource File Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CDIO14LogicDialog, CStdDialog);

// Constructor

CDIO14LogicDialog::CDIO14LogicDialog(void)
{
	m_fStacked   = FALSE;

	m_fAutoClose = TRUE;

	m_Topic      = 0;
	}

// Message Map

AfxMessageMap(CDIO14LogicDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_COMMAND)
	
	AfxMessageEnd(CDIO14LogicDialog)
	};
	
// Message Handlers

BOOL CDIO14LogicDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	SetDlgFocus(Focus);
	
	return FALSE;
	}

void CDIO14LogicDialog::OnClose(void)
{
	SendMessage(WM_COMMAND, IDCANCEL);
	}

BOOL CDIO14LogicDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl)
{
	if( m_fAutoClose ) {
		
		if( !m_fModeless && uID == IDCANCEL ) {
			
			EndDialog(0);
				
			return TRUE;
			}
		}
		
	if( uID == IDHELP && m_Topic ) {
	
//		afxThread->Help(m_Topic);
		
		return TRUE;
		}
	
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Configure Symbol Dialog
// 

// Runtime Class

AfxImplementRuntimeClass(CDIO14CfgSym, CDIO14LogicDialog);
		
// Constructors

CDIO14CfgSym::CDIO14CfgSym(BYTE Data0,BYTE Data1,BYTE Data2)
{		
	m_Prop0 = Data0;	
	m_Prop1 = Data1;	
	m_Prop2 = Data2;	

	SetName(L"DIO14CfgSym");
	}

// Attributes

CString CDIO14CfgSym::GetComment(void) const
{
	return m_Data1;
	}

// Overridables

BOOL CDIO14CfgSym::OnCheckText(CString Text)
{
	return TRUE;
	}

// Message Map

AfxMessageMap(CDIO14CfgSym, CDIO14LogicDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK, OnCommandOK)

	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CDIO14CfgSym)
	};

// Message Handlers

void CDIO14CfgSym::OnPaint(void)
{
	PAINTSTRUCT Paint;
		
	CClientDC DC(ThisObject);
	
	HWND hWnd = GetActiveWindow();
		
	BeginPaint(hWnd, &Paint);

	DC.Save();
					
	DC.SetBkMode(TRANSPARENT);
	
	DC.Select(afxFont(Dialog));
	
	DC.SetTextColor(0x00000000);

	TCHAR str1[80];
	
	TCHAR str2[80];

	CEditCtrl &Edit1 = (CEditCtrl &) GetDlgItem(100);

	CEditCtrl &Edit2 = (CEditCtrl &) GetDlgItem(101);
		
	if(m_Type > GATE_START && m_Type < GATE_STOP) {
	
		Edit2.EnableWindow(FALSE);
		
		wstrcpy(str1,CString(IDS_DIO_INPUT_GATE));
	
		wstrcpy(str2,L" ");
		}

	if(m_Type > TIMER_START && m_Type < TIMER_STOP) {
				
		WORD val = (WORD)(m_Prop1*256 + m_Prop2);
		
		witoa(val,str2,10);

		if(val < 10) {

			wstrcpy(str1,L"0");

			wstrcat(str1,str2);
			}
		else wstrcpy(str1,str2);
		
		BYTE len = (BYTE)wstrlen(str1);
		
		str1[len] = str1[len-1];

		str1[len-1] = '.';

		str1[len+1] = '\0';

		m_Data1 = (CString)str1;
				
		witoa(m_Prop0,str1,10);
		m_Data2 = (CString)str1;

		wstrcpy(str1, CString(IDS_DIO_TO_SEC));
		
		wstrcpy(str2, CString(IDS_DIO_TMR_MAP_1TO8));
		}
	
	if(m_Type > COUNTER_START && m_Type < COUNTER_STOP) {
		
		witoa(m_Prop1*256u + m_Prop2,str1,10);
		m_Data1 = (CString)str1;
				
		witoa(m_Prop0,str1,10);
		m_Data2 = (CString)str1;
	
		wstrcpy(str1, CString(IDS_DIO_COMP_CT));
		
		wstrcpy(str2, CString(IDS_DIO_CNT_MAP_1TO8));
		}

	if(m_Type > IOCOIL_START && m_Type < IOCOIL_STOP) {
	
		Edit1.EnableWindow(FALSE);
		
		m_Data1 = " ";
				
		witoa(m_Prop0,str1,10);
		m_Data2 = (CString)str1;
		
		wstrcpy(str1, CString(IDS_DIO_PARAM_NO));
		
		wstrcpy(str2, CString(IDS_DIO_MAP_1TO8));
		}

	switch(m_Type) {

		case NOT:
		case LAT:		
			
			Edit1.EnableWindow(FALSE);
			
			Edit2.EnableWindow(FALSE);

			wstrcpy(str1, CString(IDS_DIO_EDIT_NO));
			
			wstrcpy(str2,L" ");

			break;
		}
	
	DC.TextOut(10,10, str1);
	
	DC.DrawText(str2, CRect(CPoint(10,75), CSize(230,36)), DT_WORDBREAK | DT_NOCLIP);

	DC.Deselect();

	DC.Restore();
	
	EndPaint(hWnd, &Paint);
	}

BOOL CDIO14CfgSym::OnInitDialog(CWnd &Focus, DWORD dwData)
{			
	
	OnPaint();
	
	SetWindowText(CString(IDS_DIO_SYM_CONFIG));
		
	CEditCtrl &Edit1 = (CEditCtrl &) GetDlgItem(100);

	CEditCtrl &Edit2 = (CEditCtrl &) GetDlgItem(101);
	
	Edit1.LimitText(15);
	
	Edit1.SetWindowText(m_Data1);
	
	Edit1.SetSel(CRange(TRUE));
	
	Edit2.LimitText(15);
	
	Edit2.SetWindowText(m_Data2);
	
	Edit2.SetSel(CRange(TRUE));

	return TRUE;
	}
	
// Command Handlers

BOOL CDIO14CfgSym::OnCommandOK(UINT uID)
{
	m_Data1 = GetDlgItem(100).GetWindowText();

	m_Data2 = GetDlgItem(101).GetWindowText();

	if( OnCheckText(m_Data1) || OnCheckText(m_Data2)) {
	
		EndDialog(TRUE);

		return TRUE;
		}

	return TRUE;
	}

BOOL CDIO14CfgSym::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}

void CDIO14CfgSym::SetType(UINT type)
{
	m_Type = type;
	}


//////////////////////////////////////////////////////////////////////////
//
// Confirm Delete Dialog
// 

// Runtime Class

AfxImplementRuntimeClass(CDIO14Confirm, CDIO14LogicDialog);
		
// Constructors

CDIO14Confirm::CDIO14Confirm(BYTE type)
{		
	m_Type = type;
	
	SetName(L"DIO14Confirm");
	}

// Overridables

BOOL CDIO14Confirm::OnCheckText(CString Text)
{
	return TRUE;
	}

// Message Map

AfxMessageMap(CDIO14Confirm, CDIO14LogicDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK, OnCommandOK)

	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CDIO14Confirm)
	};

// Message Handlers

void CDIO14Confirm::OnPaint(void)
{
	CClientDC DC(ThisObject);
	
	PAINTSTRUCT Paint;
		
	HWND hWnd = GetActiveWindow();
		
	BeginPaint(hWnd, &Paint);

	DC.Save();
					
	DC.SetBkMode(TRANSPARENT);
	
	DC.Select(afxFont(Dialog));
	
	DC.SetTextColor(0x00000000);

	TCHAR str1[80];
	
	if(m_Type == DWIRE) {

		wstrcpy(str1, CPrintf(CString(IDS_DIO_DELETE), CString(IDS_DIO_WIRE)));
	
		DC.TextOut(25,25, str1);
		}
	
	if(m_Type == DSYMBOL) {

		wstrcpy(str1, CPrintf(CString(IDS_DIO_DELETE), CString(IDS_DIO_SYMBOL)));
	
		DC.TextOut(20,25, str1);
		}
	
	DC.Deselect();

	DC.Restore();
	
	EndPaint(hWnd, &Paint);
	}

BOOL CDIO14Confirm::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	OnPaint();
	
	SetWindowText(CString(IDS_DIO_DELETE_CFRM));
		
	return TRUE;
	}
	
// Command Handlers

BOOL CDIO14Confirm::OnCommandOK(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

BOOL CDIO14Confirm::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// General Information Dialog
// 

// Runtime Class

AfxImplementRuntimeClass(CDIO14Info, CDIO14LogicDialog);
		
// Constructors

CDIO14Info::CDIO14Info(BYTE message)
{		
	m_Message = message;
	
	SetName(L"DIO14Info");
	}

// Overridables

BOOL CDIO14Info::OnCheckText(CString Text)
{
	return TRUE;
	}

// Message Map

AfxMessageMap(CDIO14Info, CDIO14LogicDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK, OnCommandOK)

	//AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CDIO14Info)
	};

// Message Handlers

void CDIO14Info::OnPaint(void)
{
	CClientDC DC(ThisObject);
	
	PAINTSTRUCT Paint;
		
	HWND hWnd = GetActiveWindow();
		
	BeginPaint(hWnd, &Paint);

	DC.Save();
					
	DC.SetBkMode(TRANSPARENT);

	DC.Select(afxFont(Dialog));
	
	DC.SetTextColor(0x00000000); 

	CString Str;

	//Error messages
	switch(m_Message) {

		case 0:

			Str = CString(CPrintf(CString(IDS_DIO_PLACE_TOTAL), CString(IDS_DIO_8CTRS))); break;

		case 1:

			Str = CString(CPrintf(CString(IDS_DIO_PLACE_TOTAL), CString(IDS_DIO_8TMRS))); break;

		case 2:

			Str = CString(IDS_DIO_SYMBOL_LIMIT); break;

		case 3:

			Str = CString(IDS_DIO_SYMBOL_ON); break;
		
		case 4:

			Str = CString(CPrintf(CString(IDS_DIO_PLACE_TOTAL), CString(IDS_DIO_8INPS))); break;
		
		case 5:

			Str = CString(IDS_DIO_WIRE_NOJUNC); break;
		
		case 6:

			Str = CString(IDS_DIO_NODE_DRVN); break;

		case 7:

			Str = CString(IDS_DIO_TIE_OUTPUTS); break;
		
		case 8:

			Str = CString(CPrintf(CString(IDS_DIO_PLACE_TOTAL), CString(IDS_DIO_8OUTS))); break;
		
		case 9:

			Str = CString(IDS_DIO_SAME_CONN); break;
		
		case 10:

			Str = CString(IDS_DIO_PLACE_LATCH); break;
		
		case 11:

			Str = CString(IDS_DIO_MAP_DUPE); break;
		
		case 12:

			Str =  CString(IDS_DIO_VAL_OUT); break;
		
		case 13:

			Str = CString(IDS_DIO_MAP_INVALID); break;
		}

	CBitmap Bitmap;
	
	Bitmap.Create(L"INFO_MSG", CSize(19, 19));
	
	CDIO14LogicWnd DTB;
	
	DTB.DrawTransparentBitmap(DC,Bitmap,6,15,34,34,0,0x00FFFFFF);

	DC.DrawText(Str, CRect(CPoint(45,25), CSize(234,36)), DT_WORDBREAK | DT_NOCLIP);
	
	DC.Deselect();

	DC.Restore();
	
	EndPaint(hWnd, &Paint);
	}

BOOL CDIO14Info::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	OnPaint();
	
	SetWindowText(CString(IDS_DIO_MESSAGE));
		
	return TRUE;
	}
	
// Command Handlers

BOOL CDIO14Info::OnCommandOK(UINT uID)
{
	EndDialog(TRUE);

	return TRUE;
	}

/*
BOOL CDIO14Info::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
	}
*/

// End of File
