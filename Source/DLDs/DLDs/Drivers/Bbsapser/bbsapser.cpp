
#include "intern.hpp"

#include "bbsapser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Master Driver
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

static BYTE crctbl[] = {
	0x087,0x00F,0x00E,0x01E,0x095,0x02C,0x01C,0x03D,
	0x0A3,0x049,0x02A,0x058,0x0B1,0x06A,0x038,0x07B,
	0x0CF,0x083,0x046,0x092,0x0DD,0x0A0,0x054,0x0B1,
	0x0EB,0x0C5,0x062,0x0D4,0x0F9,0x0E6,0x070,0x0F7,
	0x006,0x01F,0x08F,0x00E,0x014,0x03C,0x09D,0x02D,
	0x022,0x059,0x0AB,0x048,0x030,0x07A,0x0B9,0x06B,
	0x04E,0x093,0x0C7,0x082,0x05C,0x0B0,0x0D5,0x0A1,
	0x06A,0x0D5,0x0E3,0x0C4,0x078,0x0F6,0x0F1,0x0E7,
	0x085,0x02E,0x00C,0x03F,0x097,0x00D,0x01E,0x01C,
	0x0A1,0x068,0x028,0x079,0x0B3,0x04B,0x03A,0x05A,
	0x0CD,0x0A2,0x044,0x0B3,0x0DF,0x081,0x056,0x090,
	0x0E9,0x0E4,0x060,0x0F5,0x0FB,0x0C7,0x072,0x0D6,
	0x004,0x03E,0x08D,0x02F,0x016,0x01D,0x09F,0x00C,
	0x020,0x078,0x0A9,0x069,0x032,0x05B,0x0BB,0x04A,
	0x04C,0x0B2,0x0C5,0x0A3,0x05E,0x091,0x0D7,0x080,
	0x068,0x0F4,0x0E1,0x0E5,0x07A,0x0D7,0x0F3,0x0C6,
	0x083,0x04D,0x00A,0x05C,0x091,0x06E,0x018,0x07F,
	0x0A7,0x00B,0x02E,0x01A,0x0B5,0x028,0x03C,0x039,
	0x0CB,0x0C1,0x042,0x0D0,0x0D9,0x0E2,0x050,0x0F3,
	0x0EF,0x087,0x066,0x096,0x0FD,0x0A4,0x074,0x0B5,
	0x002,0x05D,0x08B,0x04C,0x010,0x07E,0x099,0x06F,
	0x026,0x01B,0x0AF,0x00A,0x034,0x038,0x0BD,0x029,
	0x04A,0x0D1,0x0C3,0x0C0,0x058,0x0F2,0x0D1,0x0E3,
	0x06E,0x097,0x0E7,0x086,0x07C,0x0B4,0x0F5,0x0A5,
	0x081,0x06C,0x008,0x07D,0x093,0x04F,0x01A,0x05E,
	0x0A5,0x02A,0x02C,0x03B,0x0B7,0x009,0x03E,0x018,
	0x0C9,0x0E0,0x040,0x0F1,0x0DB,0x0C3,0x052,0x0D2,
	0x0ED,0x0A6,0x064,0x0B7,0x0FF,0x085,0x076,0x094,
	0x000,0x07C,0x089,0x06D,0x012,0x05F,0x09B,0x04E,
	0x024,0x03A,0x0AD,0x02B,0x036,0x019,0x0BF,0x008,
	0x048,0x0F0,0x0C1,0x0E1,0x05A,0x0D3,0x0D3,0x0C2,
	0x06C,0x0B6,0x0E5,0x0A7,0x07E,0x095,0x0F7,0x084,
	0x08F,0x08B,0x006,0x09A,0x09D,0x0A8,0x014,0x0B9,
	0x0AB,0x0CD,0x022,0x0DC,0x0B9,0x0EE,0x030,0x0FF,
	0x0C7,0x007,0x04E,0x016,0x0D5,0x024,0x05C,0x035,
	0x0E3,0x041,0x06A,0x050,0x0F1,0x062,0x078,0x073,
	0x00E,0x09B,0x087,0x08A,0x01C,0x0B8,0x095,0x0A9,
	0x02A,0x0DD,0x0A3,0x0CC,0x038,0x0FE,0x0B1,0x0EF,
	0x046,0x017,0x0CF,0x006,0x054,0x034,0x0DD,0x025,
	0x062,0x051,0x0EB,0x040,0x070,0x072,0x0F9,0x063,
	0x08D,0x0AA,0x004,0x0BB,0x09F,0x089,0x016,0x098,
	0x0A9,0x0EC,0x020,0x0FD,0x0BB,0x0CF,0x032,0x0DE,
	0x0C5,0x026,0x04C,0x037,0x0D7,0x005,0x05E,0x014,
	0x0E1,0x060,0x068,0x071,0x0F3,0x043,0x07A,0x052,
	0x00C,0x0BA,0x085,0x0AB,0x01E,0x099,0x097,0x088,
	0x028,0x0FC,0x0A1,0x0ED,0x03A,0x0DF,0x0B3,0x0CE,
	0x044,0x036,0x0CD,0x027,0x056,0x015,0x0DF,0x004,
	0x060,0x070,0x0E9,0x061,0x072,0x053,0x0FB,0x042,
	0x08B,0x0C9,0x002,0x0D8,0x099,0x0EA,0x010,0x0FB,
	0x0AF,0x08F,0x026,0x09E,0x0BD,0x0AC,0x034,0x0BD,
	0x0C3,0x045,0x04A,0x054,0x0D1,0x066,0x058,0x077,
	0x0E7,0x003,0x06E,0x012,0x0F5,0x020,0x07C,0x031,
	0x00A,0x0D9,0x083,0x0C8,0x018,0x0FA,0x091,0x0EB,
	0x02E,0x09F,0x0A7,0x08E,0x03C,0x0BC,0x0B5,0x0AD,
	0x042,0x055,0x0CB,0x044,0x050,0x076,0x0D9,0x067,
	0x066,0x013,0x0EF,0x002,0x074,0x030,0x0FD,0x021,
	0x089,0x0E8,0x000,0x0F9,0x09B,0x0CB,0x012,0x0DA,
	0x0AD,0x0AE,0x024,0x0BF,0x0BF,0x08D,0x036,0x09C,
	0x0C1,0x064,0x048,0x075,0x0D3,0x047,0x05A,0x056,
	0x0E5,0x022,0x06C,0x033,0x0F7,0x001,0x07E,0x010,
	0x008,0x0F8,0x081,0x0E9,0x01A,0x0DB,0x093,0x0CA,
	0x02C,0x0BE,0x0A5,0x0AF,0x03E,0x09D,0x0B7,0x08C,
	0x040,0x074,0x0C9,0x065,0x052,0x057,0x0DB,0x046,
	0x064,0x032,0x0ED,0x023,0x076,0x011,0x0FF,0x000
	};

//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Driver
//

// Instantiator

INSTANTIATE(CBBBSAPSerialDriver);

// Constructor

CBBBSAPSerialDriver::CBBBSAPSerialDriver(void)
{
	m_Ident		= DRIVER_ID;

	m_pCtx		= NULL;

	m_pHead		= &Head;

	m_pRFrame	= &m_RFrame;

	m_pTx		= m_bPoll;
	}

// Destructor

CBBBSAPSerialDriver::~CBBBSAPSerialDriver(void)
{
	}

// Configuration

void MCALL CBBBSAPSerialDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CBBBSAPSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CBBBSAPSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CBBBSAPSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CBBBSAPSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx  = new CContext;

			m_pCtx->m_uDrop		= LOWORD(GetLong(pData));

			m_pCtx->m_fHasTags	= FALSE;
			m_pCtx->m_wTagCount     = 0;
			m_pCtx->m_uStrings      = 0;
			m_pCtx->m_pdAddrList	= NULL;
			m_pCtx->m_pbNameList	= NULL;
			m_pCtx->m_pwNameIndex	= NULL;
			m_pCtx->m_pStrings      = NULL;

			WORD wKey		= GetWord(pData);

			switch( wKey ) {

				case 0xABCD:	m_pCtx->m_bIsC3 = 2; break;
				case 0xBCDE:	m_pCtx->m_bIsC3 = 3; break;

				default:	return CCODE_ERROR | CCODE_HARD;
				}

			if( LoadTags(pData) ) {

				LoadStringSupport(pData);
				}

			m_pCtx->m_uMsgSeq	= 1;
			m_pCtx->m_uAppSeq	= 1;
			m_pCtx->m_uACCOLVers	= 0;
			m_pCtx->m_uMSDVers	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBBBSAPSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		CloseDown();

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

UINT MCALL CBBBSAPSerialDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;

	if( uFunc == 1 )  {  // Drop Number

		UINT uValue = ATOI(Value);

		if( uValue > 0 && uValue <= 255 ) {

			pCtx->m_uDrop = uValue;

			return 1;
			}
		}

	if( uFunc == 4 ) {  // Current target drop number 

		return pCtx->m_uDrop;
		}
	
	return 0;
	}

// Entry Points

CCODE MCALL CBBBSAPSerialDriver::Ping(void)
{
//**/	AfxTrace0("\r\n-PING- ");

	if( m_pCtx->m_fHasTags ) {

		m_pData->Write( 0, FOREVER );

		if( SynchPolls() ) {

//			if( !m_pCtx->m_uACCOLVers ) {

//				if( GetACCOLVersion() ) {

//					SendListRequest(4);
//					}
//				}

			return 1;
			}
		}

	return CCODE_ERROR;
	}

// Entry Points

CCODE MCALL CBBBSAPSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\n\nSerial T = %d, O = %d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	if( !HasTags(pData, uCount) ) return uCount;

	MakeMin(uCount, MAX_LTEXT);

	StartFrame(FALSE, FALSE);

	AddReadByNameHeader();

	m_pRFrame->uAddr  = Addr.a.m_Offset;
	m_pRFrame->pData  = pData;
	m_pRFrame->uCount = uCount;
	m_pRFrame->uGood  = 0;

	m_fVarSuccess     = FALSE;

	if( AddReadByName(Addr) ) {

		if( m_fVarSuccess ) { // at least one name is ok

			CCODE cCode = DoDataRead(Addr);

			if( !COMMS_SUCCESS(cCode) ) {

				return CCODE_ERROR;
				}

			if( !m_bRx[UDPPCT] ) { // RTU start up fix

//**/				AfxTrace0("\r\nNO DATA\r\n");

				return CCODE_ERROR | CCODE_NO_DATA;
				}
			
			return cCode;
			}
		}

	return uCount; // names aren't configured
	}

CCODE MCALL CBBBSAPSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace2("\r\n\n**** WRITE T = %d, O = %d ", Addr.a.m_Table, Addr.a.m_Offset);

	if( !HasTags(pData, uCount) ) return uCount;

	StartFrame(FALSE, TRUE);

	m_fVarSuccess = FALSE;

	if( AddWriteByName(Addr) ) {

		if( m_fVarSuccess ) {

			BOOL fWResp = FALSE;

			if( IsString(Addr.a.m_Table) ) {

				fWResp = DoStringWrite(Addr, pData, uCount);
				}
			else {
				switch( Addr.a.m_Table ) {

					case SPBIT:
						if( *pData > 15 ) return 1;

						fWResp = DoBitWrite(pData);
						break;

					case SPBYTE:
						fWResp = DoByteWrite(pData);
						break;

					case SPWORD:
						fWResp = DoWordWrite(pData);
						break;

					case SPLONG:

						fWResp = DoLongWrite(pData);
						break;

					case SPREAL:
						fWResp = DoRealWrite(pData);
						break;

					default:
						return CCODE_ERROR | CCODE_HARD;
					}
				}

			if( fWResp ) {

				m_uWriteError = 0;

				return IsString(Addr.a.m_Table) ? MAX_LTEXT : 1;
				}
			}

		if( ++m_uWriteError > 2 ) {

			m_uWriteError = 0;

			return 1;
			}

		return CCODE_ERROR;
		}

	return 1; // Name doesn't exist
	}

// Frame Building

void CBBBSAPSerialDriver::StartFrame(BOOL fFunc, BOOL fIsWrite)
{
	m_pTx = m_bTx;

	PutStart(fFunc);

	SetBasicHeader(fIsWrite);
	}

void CBBBSAPSerialDriver::PutStart(BOOL fFunc)
{
	m_pTx[0] = DLE; // don't double this DLE

	m_uPtr   = 1;

	AddByte(STX);

	BYTE a = LOBYTE(m_pCtx->m_uDrop);

	BYTE b = m_pCtx->m_uMsgSeq;

	NextSeq(&m_pCtx->m_uMsgSeq, TRUE);

	if( fFunc ) {

		AddByte(a);
		AddByte(b);

		return;
		}

	m_pHead->bAdd  = a;
	m_pHead->bSNum = b;
	}

void CBBBSAPSerialDriver::EndFrame(void)
{
	m_pTx[m_uPtr++] = DLE; // don't double this DLE

	AddByte(ETX);

	AddCRC();
	}

void CBBBSAPSerialDriver::SetBasicHeader(BOOL fIsWrite)
{
	m_pHead->bDFC = MFCRDBACC;
	m_pHead->bSQL = LOBYTE(m_pCtx->m_uAppSeq);
	m_pHead->bSQH = HIBYTE(m_pCtx->m_uAppSeq);
	m_pHead->bSFC = fIsWrite ? MFCWRDB : MFCRDDB;
	m_pHead->bNSB = 0;

	NextSeq(&m_pCtx->m_uAppSeq, FALSE);

	AddHeader();
	}

void CBBBSAPSerialDriver::AddHeader(void)
{
	AddByte(m_pHead->bAdd);
	AddByte(m_pHead->bSNum);
	AddByte(m_pHead->bDFC);
	AddByte(m_pHead->bSQL);
	AddByte(m_pHead->bSQH);
	AddByte(m_pHead->bSFC);
	AddByte(m_pHead->bNSB);
	}

void CBBBSAPSerialDriver::AddCRC(void)
{
	BYTE crc_l = 0;
	BYTE crc_h = 0xFF;

	BOOL bUseDLE = FALSE;
	BOOL bSkip   = FALSE;

	for(UINT i = 2; i < m_uPtr; i++ ) { // skip DLE, STX

		BYTE b = m_pTx[i];

		bSkip  = b == DLE && !bUseDLE; // skip 1st DLE if present

		if( !bSkip ) {

			UINT TPos  = 2 * (b ^ crc_l);

			crc_l = (crc_h ^ crctbl[TPos]);
			crc_h = crctbl[TPos+1];

			bUseDLE = FALSE;
			}

		else bUseDLE = TRUE;
		}

	m_pTx[m_uPtr++] = ~crc_l;
	m_pTx[m_uPtr++] = crc_h;
	}

void CBBBSAPSerialDriver::NextSeq(UINT *pSeq, BOOL fByte)
{
	UINT uSeq = *pSeq + 1;

	if( !(BOOL)uSeq || (fByte && !(BOOL)LOBYTE(uSeq)) ) {

		uSeq = 1;
		}

	*pSeq = uSeq;
	}

void CBBBSAPSerialDriver::AddByte(BYTE bData)
{
	if( bData == DLE ) {

		m_pTx[m_uPtr++] = DLE;
		}

	m_pTx[m_uPtr++] = bData;
	}

void CBBBSAPSerialDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CBBBSAPSerialDriver::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

void CBBBSAPSerialDriver::AddReal(DWORD dwData)
{
	AddLong(dwData);
	}

void CBBBSAPSerialDriver::AddText(PCTXT pText)
{
//**/	AfxTrace0("\r\nName- ");

	for( UINT i = 0; i < strlen(pText); i++ ) {

//**/		AfxTrace1("%c", pText[i]);

		AddByte(pText[i]);
		}
	}

UINT CBBBSAPSerialDriver::AddVarName(AREF Addr, UINT uIndex, UINT uBytes)
{
	uIndex = GetNamePos(Addr, uIndex);

	if( uIndex < 0xFFFF ) {

		WORD wIndex = m_pCtx->m_pwNameIndex[uIndex];

		PBYTE pName = &m_pCtx->m_pbNameList[wIndex];

		if( pName ) {

			if( CanAddToRequest(pName, uBytes) ) {

				AddText((PCTXT)pName);

				AddByte(0);

				return VADDOK;
				}

			return VTXFULL;
			}
		}

	return VNONAME;
	}

UINT CBBBSAPSerialDriver::GetNamePos(AREF Addr, UINT uIndex)
{
	UINT uOffset = FindOffset(Addr, uIndex);

	for( UINT i = 0; i < m_pCtx->m_wTagCount; i++ ) {

		if( uOffset == LOWORD(m_pCtx->m_pdAddrList[i]) ) {

			return i;
			}
		}

	return 0xFFFF;
	}

// Read Handlers

CCODE CBBBSAPSerialDriver::DoDataRead(AREF Addr)
{
	READFRAME * p = m_pRFrame;

	PDWORD pData  = p->pData;

	UINT uCount   = p->uCount;

	UINT uGood    = p->uGood;

	UINT uMask    = 1;

	UINT uPos     = 0;

	if( (BOOL)(uPos = SendDataFrame())  ) {

		BOOL fHasErr	= m_bRx[uPos - 2] & 0x80;			// error(s) present code

		uCount		= min( m_bRx[uPos - 1], p->uCount );		// item count to process

		p->uCount	= uCount;

		for( UINT i = 0, j = 0; i < uCount; i++, uMask <<= 1, pData++ ) {

			DWORD dData = 0;

			if( uMask & uGood ) {					// this name was sent

				BOOL fErr = fHasErr && (BOOL)m_bRx[uPos + j];

				if( fHasErr ) j++;

				if( !fErr ) {

					dData = GetReadData(Addr, &j, i);

					if( !IsString(Addr.a.m_Table) ) {
				
						*pData = dData;
						}

					continue;
					}
				}
			break;
			}

		return i ? ExpandCount(Addr, i) : CCODE_ERROR;
		}

	return CCODE_ERROR;
	}

DWORD CBBBSAPSerialDriver::GetReadData(AREF Addr, UINT *pOffset, UINT uIndex)
{
	UINT uPos  = RXDATA0 + *pOffset;

	UINT uDPos = uPos + 1;

	BYTE bType = (m_bRx[uPos]) & 0x3;

	DWORD d    = 0;

	switch( bType ) {

		case 0:	// type is bit

			*pOffset += 2;

			BOOL f;

			f = (BOOL)m_bRx[uDPos];

			d = (DWORD)(f ? (Addr.a.m_Type == addrRealAsReal ? 0x3F800000 : 1) : 0);

			return d;

		case 2:	// type is analog

			*pOffset += 5;

			d = (DWORD)IntelToHost(*(PU4)&m_bRx[uDPos]);

			break;

		case 3:	// type is string

			d = GetStringData(Addr, uPos, uDPos, uIndex);

			break;
		}

	if( !IsString(Addr.a.m_Table) ) {

		float Real = I2R(d);

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	return (DWORD)((BOOL)d);
			case addrByteAsByte:	return (DWORD)LOBYTE(Real);
			case addrWordAsWord:	return (DWORD)LOWORD(Real);
			case addrLongAsLong:	return (DWORD)Real;
			}
		}
	
	return d;
	}

DWORD CBBBSAPSerialDriver::GetStringData(AREF Addr, UINT &uPos, UINT uDPos, UINT uIndex)
{
	UINT uStart  = ExpandCount(Addr, uIndex);

	UINT uChar   = GetCharCount((PCTXT)m_bRx, uDPos);

	UINT uAdd    = uChar % 4 ? 1 : 0;

	UINT uCopy   = min(uChar / sizeof(DWORD) + uAdd, MAX_LTEXT);

	for( UINT u  = uStart; u < uStart + uCopy; u++, uDPos += 4 ) {

		m_pRFrame->pData[u] = (DWORD)IntelToHost(*(PU4)&m_bRx[uDPos]);
		}

	PTXT pText   = (PTXT)(m_pRFrame->pData + uStart);
	
	CAddress Address;
	
	Address.m_Ref = Addr.m_Ref;
	
	Address.a.m_Offset += uStart;
	
	SetStringContext((AREF)Address, (PCTXT)pText, min(uChar, uCopy * sizeof(DWORD)));

	uPos += uChar + 1;
	
	uPos++;

	return m_pRFrame->pData[0];
	}

BOOL CBBBSAPSerialDriver::AddReadByName(AREF Addr)
{
	UINT uPos    = m_uPtr;	// position of count

	READFRAME *p = m_pRFrame;

	UINT uOffset = p->uAddr;

	UINT uCount  = GetCount(Addr, p->uCount);

	UINT uGood   = 0;

	UINT uOk     = 0;

	AddByte(0);		// will be count

	for( UINT i = 0; i < uCount; i++ ) {

		switch( AddVarName(Addr, i) ) {

			case VTXFULL: // Tx buffer full
				i = uCount;
				break;

			case VADDOK:
				uOk++;
				uGood |= (1 << i);
				break;

			case VNONAME:
				break;
			}
		}

	p->uGood = uGood;

	if( (BOOL)uOk ) {

		m_pTx[uPos]   = uOk; // number of successful adds

		m_fVarSuccess = TRUE;

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPSerialDriver::AddReadByNameHeader(void)
{
	AddByte(OFCRDBYN);	// Read Signal by Name

	AddByte(0x01);	// FSS quantity
	AddByte(FS1TYPINH + FS1VALUE);	// FSS Value

	AddByte(0xFF);	// Security level
	}

// Write Handlers

BOOL CBBBSAPSerialDriver::DoBitWrite(PDWORD pData)
{
	AddWriteData(addrBitAsBit, *pData);

	return SendDataFrame();
	}

BOOL CBBBSAPSerialDriver::DoByteWrite(PDWORD pData)
{
	AddWriteData(addrByteAsByte, *pData);

	return SendDataFrame();
	}

BOOL CBBBSAPSerialDriver::DoWordWrite(PDWORD pData)
{
	AddWriteData(addrWordAsWord, *pData);

	return SendDataFrame();
	}

BOOL CBBBSAPSerialDriver::DoLongWrite(PDWORD pData)
{
	AddWriteData(addrLongAsLong, *pData);

	return SendDataFrame();
	}

BOOL CBBBSAPSerialDriver::DoRealWrite(PDWORD pData)
{
	AddWriteData(addrRealAsReal, *pData);

	return SendDataFrame();
	}

BOOL CBBBSAPSerialDriver::DoStringWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	AddStringData(Addr, pData, uCount);

	return SendDataFrame();
	}

BOOL CBBBSAPSerialDriver::AddWriteByName(AREF Addr)
{
	AddByte(OFCWRBYN);	// write by name opcode
	AddByte(0xFF);		// Security level
	AddByte(1);		// 1 element

	m_fVarSuccess = AddVarName(Addr, 0) == VADDOK;

	return m_fVarSuccess;
	}

void CBBBSAPSerialDriver::AddWriteData(UINT uType, DWORD dData)
{
	switch(uType) {

		case addrByteAsByte:	dData &= 0xFF;		break;

		case addrWordAsWord:	dData &= 0xFFFF;	break;
		}

	float Real = float(LONG(dData));

	switch(uType) {

		case addrBitAsBit:

			AddByte(dData ? 9 : 10);
			break;

		case addrByteAsByte:


			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrWordAsWord:


			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrLongAsLong:

			AddByte(WFDANALG);
			AddLong(R2I(Real));
			break;

		case addrRealAsReal:

			AddByte(WFDANALG);
			AddLong(dData);
			break;
		}
	}

void CBBBSAPSerialDriver::AddStringData(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uMax    = MAX_LTEXT;

	UINT uFrom   = ExpandCount(Addr, 0);

	UINT uOffset = (Addr.a.m_Offset + uFrom) % uMax;

	PBYTE pCopy  = PBYTE(alloca(MAX_CHARS));

	memset(pCopy, 0, MAX_CHARS);

	MakeMin(uCount, uMax);

	if( uOffset ) {

		CAddress Address;

		Address.m_Ref = Addr.m_Ref + uFrom - uOffset;

		UINT uString  = GetStringContextIndex((AREF)Address);

		if( uString < NOTHING ) {

			memcpy(pCopy, m_pCtx->m_pStrings[uString].m_Text, MAX_CHARS);
			}
		}

	PDWORD pStr = PDWORD(pCopy);

	for( UINT n = 0; n < uCount; n++ ) {

		pStr[n + uOffset] = HostToIntel(pData[n + uFrom]);
		}

	RemoveTrailing(pCopy, 0x20, MAX_CHARS);

	AddByte(WFDSTRNG);

	UINT uChar = GetCharCount((PCTXT)pCopy, 0);

	UINT uAdd  = uChar % 4 ? 1 : 0;

	UINT uCopy = uChar / sizeof(DWORD) + uAdd;

	for( UINT u = 0; u < uCopy; u++ ) {

		AddLong(pStr[u]);
		}

	while( m_pTx[m_uPtr - 1] == 0 ) {

		m_uPtr--;
		}

	AddByte(0);
	}

// String Support

UINT CBBBSAPSerialDriver::GetStringContextIndex(AREF Addr)
{
	if( IsString(Addr.a.m_Table) ) {

		UINT uString = Addr.a.m_Offset / MAX_LTEXT;

		if( uString < m_pCtx->m_uStrings ) {

			return uString;
			}
		}

	return NOTHING;
	}

void CBBBSAPSerialDriver::SetStringContext(AREF Addr, PCTXT pText, UINT uCount)
{
	CStringTag String;

	UINT uIndex = GetStringContextIndex(Addr);

	if( uIndex < NOTHING ) {

		memcpy(m_pCtx->m_pStrings[uIndex].m_Text, pText, uCount);
		}
	}

UINT CBBBSAPSerialDriver::GetStringCount(AREF Addr, UINT uPos, UINT uCount)
{
	UINT uSize    = MAX_LTEXT;

	UINT uOffset  = Addr.a.m_Offset % uSize;

	if( uPos == 0 ) {

		return uSize - uOffset;
		}

	return min(uCount, uSize);
	}

UINT CBBBSAPSerialDriver::GetCharCount(PCTXT pText, UINT uOffset)
{
	UINT uChars = 0;

	if( pText ) {

		while( IsStringText(pText[uOffset + uChars]) ) {

			uChars++;
			}
		}

	return uChars;
	}

void CBBBSAPSerialDriver::RemoveTrailing(PBYTE &pByte, BYTE bByte, UINT uBytes)
{
	UINT uRemove = pByte? uBytes - 1 : 0;

	while( uRemove > 0 ) {

		if( pByte[uRemove] == 0x20 ) {

			pByte[uRemove] = 0;

			uRemove--;

			continue;
			}
		break;
		}
	}

// Polling

UINT CBBBSAPSerialDriver::MainPoll(BOOL fInPing)
{
	StartPoll();

//**/	AfxTrace0("\r\nMain Poll ");

	AddByte(POLLMSG);

	AddByte(0x10);

	if( Transact(TRUE) ) {

		BYTE bFC = m_bRx[RXDFC];

		switch( bFC ) {

			case POLLDOWN:	return fInPing ? 1 : (UINT)POLLDOWN;

			case POLLNONE:	return POLLNONE;

			case NRTREQ:	return PRGENACK;

			case POLLNAK:			// insufficient buffer space
					return PRNAK;

			case ALARMNOT:	return PRGENACK;
			}

		return fInPing ? 1 : PRDATA;
		}

	return PRNORSP;
	}

void CBBBSAPSerialDriver::StartPoll(void)
{
	Sleep(10);

	m_pTx = m_bPoll;

	m_pData->Write(0, FOREVER);

	PutStart(TRUE);

	m_bRx[RXDFC] = 0;
	}

BOOL CBBBSAPSerialDriver::SynchPolls(void)
{
//**/	AfxTrace0("\r\n\nSync Polls ");

	UINT u = 20;

	while( TRUE ) {

		UINT uPoll = MainPoll(TRUE);

		if( uPoll == PRNORSP ) {

			return FALSE;
			}

		BYTE b = m_bRx[RXDFC];

		if( !((BOOL)b & 0x80) ) {

			m_pCtx->m_uMsgSeq = m_bRx[RXSERN] + 1;

			SendDataAck();
			}

		else {
			switch( b ) {

				case POLLMSG:	return FALSE;
				case POLLNONE:	return TRUE;
				case ALARMNOT:	SendAlarmAck();	return TRUE;
				case POLLDOWN:	break;
				default:	return FALSE;
				}
			}

		if( !(BOOL)(u--) ) {

			return FALSE;
			}
		}

	return FALSE;
	}

void CBBBSAPSerialDriver::SendDataAck(void)
{
	StartPoll();

	AddByte(POLLUPMS);

	AddByte(m_bRx[RXSERN]);

	Transact(FALSE);

	MainPoll(FALSE);

//**/	AfxTrace0("\r\nFinished Data Ack ");
	}

void CBBBSAPSerialDriver::SendAlarmAck(void)
{
	m_pTx = m_bTx;

	PutStart(TRUE);

	AddByte(ALARMACK);
	AddByte(LOBYTE(m_pCtx->m_uAppSeq));
	AddByte(HIBYTE(m_pCtx->m_uAppSeq));
	AddByte(ALARMNOT);
	AddByte(0);

	NextSeq(&m_pCtx->m_uAppSeq, FALSE);

	AddByte(1);		// Ack one at a time

	AddByte(m_bRx[10]);	// Load Version A
	AddByte(m_bRx[11]);	// Load Version B
	AddByte(m_bRx[12]);	// MSD Address A
	AddByte(m_bRx[13]);	// MSD Address B

	Transact(TRUE);
	}

BOOL CBBBSAPSerialDriver::GetACCOLVersion(void)
{
//**/	AfxTrace0("\r\n\nGet Version ");

	m_pTx = m_bTx;

	PutStart(FALSE);

	SetBasicHeader(FALSE);

	AddByte(MFCCOMREQ);
	AddByte(8);
	AddByte(0);

	if( Transact(TRUE) ) {
	
		if( m_bRx[RXDFC] == POLLDOWN ) {

			MainPoll(FALSE);
			}

		if( m_bRx[RXDFC] == MFCRDDB ) {

			m_pCtx->m_uACCOLVers = m_bRx[9] + (m_bRx[10] << 8);

			SendDataAck();
			}
		}

	return TRUE;
	}

void CBBBSAPSerialDriver::SendListRequest(UINT uList)
{
	m_pTx = m_bTx;

//**/	AfxTrace0("\r\nSend List Request ");

	PutStart(FALSE);

	SetBasicHeader(FALSE);

	AddByte(0xC);
	AddByte(1);
	AddByte(0x08);
	AddByte(LOBYTE(m_pCtx->m_uACCOLVers));
	AddByte(HIBYTE(m_pCtx->m_uACCOLVers));
	AddByte(0xFF);
	AddByte(uList);

	m_bRx[RXDFC] = 0;

	if( Transact(TRUE) ) {

//**/		AfxTrace0("\r\n\nSent List Request ");

		if( m_bRx[RXDFC] == POLLDOWN ) {

			if( MainPoll(FALSE) ) {		// get data

				SendDataAck();		// send thanks

				MainPoll(FALSE);	// re-poll
				}
			}
		}
	}

// Response Checking

UINT CBBBSAPSerialDriver::CheckForResponse(void)
{
	if( SlAddressOK() && SerialNumOK() ) {

		BYTE b = m_bRx[RXDFC];

		if( b == m_pHead->bSFC ) {

			return PRDATA; // got the data
			}

		switch( b ) {

			case POLLDOWN:	return POLLDOWN;

			case POLLNONE:	return POLLNONE;

			case POLLUPSL:
			case POLLUPMS:

				return PRACK;

			case POLLNAK:		// insufficient buffer space
				return PRNAK;

			case ALARMNOT:

				return PRGENACK;
			}
		}

	return PRWRONG;
	}

BOOL CBBBSAPSerialDriver::SendAndWaitForData(void)
{
	if( Transact(TRUE) ) {

		UINT uRsp = CheckForResponse();

		switch( uRsp ) {

			case PRDATA:
				BYTE bSave[16];

				memcpy(bSave, m_bRx, 16);
				
				SendDataAck();

				memcpy(m_bRx, bSave, 16);

				return CheckForProperData();

			case PRACK:
				MainPoll(FALSE);
				return TRUE;

			case PRNAK:
				MainPoll(FALSE);
				return FALSE;

			case POLLDOWN:	// data request was acked

				if( CheckForProperData() ) {

					SendDataAck();

					return TRUE;
					}

				return FALSE;

			case PRGENACK:	// alarm notification

				return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

BOOL CBBBSAPSerialDriver::CheckForProperData(void)
{
	if( m_bRx[RXDFC] == m_pHead->bSFC && AppSeqNumOK() ) {

		return TRUE;
		}

	for( UINT i = 0; i < 10; i++ ) {

		Sleep(20);

		MainPoll(FALSE);
		
		if( m_bRx[RXDFC] == m_pHead->bSFC && AppSeqNumOK() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Transport Layer
UINT CBBBSAPSerialDriver::SendDataFrame(void)
{
	return SendAndWaitForData() ? RXDATA0 : 0;
	}

BOOL CBBBSAPSerialDriver::SendFrame(void)
{
//**/	AfxTrace0("\r\n\nMASTER "); for(UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_pTx[k]);

	m_pData->ClearRx();

	m_pData->Write((PCBYTE)m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CBBBSAPSerialDriver::RecvFrame(void)
{
	UINT uPtr   = 0;
	UINT uState = 0;
	UINT uTime  = 500;
	UINT uData;

	BYTE bData;

	BOOL fIsAck = FALSE;

	SetTimer(500);

//**/	AfxTrace0("\r\nMASTER ");

	while( GetTimer() ) {

		if( (uData = m_pData->Read(uTime)) == NOTHING ) {

			continue;
			}

		bData = LOBYTE(uData);

//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				if( bData == DLE ) {

					uState = 1;
					}

				break;

			case 1:
				switch( bData ) {

					case STX:
						uState = 2;
						uPtr   = 0;
						break;

					case DLE:
						break;

					default:
						uState = 0;
						break;
					}

				break;

			case 2:
				if( bData == DLE ) uState = 3;

				else m_bRx[uPtr++] = bData;

				break;

			case 3:
				m_bRx[uPtr++] = bData;

				uState = (bData == ETX) ? 4 : 2;

				break;

			case 4: // CRC 1

				uState = 5;

				break;

			case 5: // CRC 2

				return TRUE;
			}

		if( uPtr >= sizeof(m_bRx) ) return FALSE;
		}

	return FALSE;
	}

BOOL CBBBSAPSerialDriver::Transact(BOOL fWantReply)
{
	EndFrame();

	if( SendFrame() ) {

		if( fWantReply ) {

			return RecvFrame();
			}

		Sleep(40);

		return TRUE;
		}

	return FALSE;
	}	

// Tags

BOOL CBBBSAPSerialDriver::LoadTags(LPCBYTE &pData)
{
	if( StoreDevName(pData) ) {

		UINT uCount = GetWord(pData);

		if( uCount ) {

			m_pCtx->m_wTagCount  = (WORD)uCount;

			m_pCtx->m_fHasTags   = TRUE;

			m_pCtx->m_pdAddrList = new DWORD [uCount + 1];

			for( WORD i = 0; i < uCount; i++ ) {

				m_pCtx->m_pdAddrList[i] = GetLong(pData);
				}

			m_pCtx->m_pdAddrList[uCount] = 0; // end marker

			UINT uTotalSize = GetWord(pData) + uCount; // adding nulls

			m_pCtx->m_pwNameIndex = new WORD [uCount + 1];
			m_pCtx->m_pbNameList  = new BYTE [uTotalSize];

			WORD wNamePosition = 0;

			for( i = 0; i < uCount; i++ ) {

				StoreName(pData, i, &wNamePosition);
				}

			m_pCtx->m_pwNameIndex[uCount] = uTotalSize; // end marker

			return TRUE;
			}
		}

	return FALSE;
	}

void CBBBSAPSerialDriver::LoadStringSupport(LPCBYTE &pData)
{
	m_pCtx->m_uStrings = GetWord(pData);

	m_pCtx->m_pStrings = new CStringTag[m_pCtx->m_uStrings];

	for( UINT u = 0; u < m_pCtx->m_uStrings; u++ ) {

		m_pCtx->m_pStrings[u].m_Index = GetWord(pData);

		memset(m_pCtx->m_pStrings[u].m_Text, 0, MAX_CHARS);
		}
	}

BOOL CBBBSAPSerialDriver::StoreDevName(LPCBYTE &pData)
{
	UINT uCount = GetWord(pData); // size of device name

	if( uCount ) {

		m_pCtx->m_pbDevName = new BYTE[uCount];

		memset(m_pCtx->m_pbDevName, 0, uCount);

		BOOL fWide = m_pCtx->m_bIsC3 == 3;

		for( UINT i = 0; i < uCount; i++ ) {

			BYTE b = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData);

			m_pCtx->m_pbDevName[i] = b;
			}

		return TRUE;
		}

	return FALSE;
	}

void CBBBSAPSerialDriver::StoreName(LPCBYTE &pData, WORD wItem, PWORD pPosition)
{
	UINT uByteCount	= GetWord(pData);

	if( uByteCount ) {

		WORD wPos  = *pPosition;

		m_pCtx->m_pwNameIndex[wItem] = wPos; // position of start of name

		BOOL fWide = m_pCtx->m_bIsC3 == 3;

		BYTE b		= GetByte(pData);

		if( b ) {

			fWide = FALSE;
			}

		else {
			b = GetByte(pData);
			}
			
		BOOL fUpper = b == '.';

//**/		UINT u = wPos;

		if( !fUpper ) m_pCtx->m_pbNameList[wPos++] = b; // skip if leading '.' for 3310

		for( WORD i = 1; i < uByteCount; i++ ) {

			BYTE b = fWide ? LOBYTE(GetWord(pData)) : GetByte(pData);

			m_pCtx->m_pbNameList[wPos++] = fUpper ? MakeUpper(b) : b;
			}

		*pPosition = wPos;

//**/		AfxTrace0("\r\nNames ");

//**/		for( i = u; i < wPos; i++ ) {

//**/			BYTE b = m_pCtx->m_pbNameList[i];

//**/			if( b ) AfxTrace1("%c", m_pCtx->m_pbNameList[i]);

//**/			else AfxTrace0(" NULL");
//**/	 		}
		}
	}

// Helpers

BOOL CBBBSAPSerialDriver::CheckHeader(void)
{
	if( !SlAddressOK() )	return FALSE;

	if( !SerialNumOK() )	return FALSE;

	if( !AppSeqNumOK() )	return FALSE;

	return TRUE;
	}

BOOL CBBBSAPSerialDriver::SlAddressOK(void)
{
	return m_bRx[RXDADD] == 0;
	}

BOOL CBBBSAPSerialDriver::SerialNumOK(void)
{
	UINT uRcvPos = m_bRx[RXDFC] == POLLUPMS ? RXUPSN : RXSERN;

	return m_pTx[TXSERN + ((m_pCtx->m_uDrop == DLE) ? 1 : 0)] == m_bRx[uRcvPos];
	}

BOOL CBBBSAPSerialDriver::AppSeqNumOK(void)
{
	PBYTE pR = m_bRx + RXASQL;

	return (m_pHead->bSQL == *pR) && (m_pHead->bSQH == *(pR+1));
	}

BOOL CBBBSAPSerialDriver::REROK(void)
{
	return !m_bRx[RXRER];
	}

BOOL CBBBSAPSerialDriver::HasTags(PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_fHasTags ) return TRUE;

	for( UINT i = 0; i < uCount; i++ ) *pData = 0xFFFFFFFF;

	return FALSE;
	}

BYTE CBBBSAPSerialDriver::MakeUpper(BYTE b)
{
	if( b >= 'a' && b <= 'z' ) b &= 0x5F;

	return b;
	}

BOOL CBBBSAPSerialDriver::IsString(UINT uTable)
{
	return uTable == SPSTR;
	}

BOOL CBBBSAPSerialDriver::IsStringText(UINT uChar)
{
	return (isalpha(uChar) || isdigit(uChar) || ispunct(uChar) || isspace(uChar));
	}

BOOL CBBBSAPSerialDriver::CanAddToRequest(PBYTE pName, UINT uBytes)
{
	WORD wLen  = pName ? (WORD)strlen((const char *)pName) : 0;

	WORD wSize = wLen + m_uPtr + uBytes;

	return wSize < SZDATA;
	}

UINT CBBBSAPSerialDriver::FindOffset(AREF Addr, UINT uIndex)
{
	if( IsString(Addr.a.m_Table) ) {

		UINT uSize = MAX_LTEXT;

		return Addr.a.m_Offset + (uIndex * uSize) - (Addr.a.m_Offset % uSize);
		}

	return Addr.a.m_Offset + uIndex;
	}

UINT CBBBSAPSerialDriver::GetCount(CAddress Addr, UINT uCount)
{
	if( IsString(Addr.a.m_Table) ) {

		uCount /= MAX_LTEXT;

		if( Addr.a.m_Offset % MAX_LTEXT ) {

			uCount++;
			}

		return uCount;
		}

	return uCount;
	}

UINT CBBBSAPSerialDriver::ExpandCount(CAddress Addr, UINT uCount, UINT uMax)
{
	if( IsString(Addr.a.m_Table) ) {

		uCount *= MAX_LTEXT;

		if( uMax ) {

			return min(uCount, uMax);
			}

		return uCount;
		}

	return uCount;
	}

// Delete created buffers

void CBBBSAPSerialDriver::CloseDown(void)
{
	if( m_pCtx->m_pbDevName ) {

		delete m_pCtx->m_pbDevName;

		m_pCtx->m_pbDevName = NULL;
		}

	if( m_pCtx->m_pdAddrList ) {

		delete m_pCtx->m_pdAddrList;

		m_pCtx->m_pdAddrList = NULL;
		}

	if( m_pCtx->m_pbNameList ) {

		delete m_pCtx->m_pbNameList;

		m_pCtx->m_pbNameList = NULL;
		}

	if( m_pCtx->m_pwNameIndex ) {

		delete m_pCtx->m_pwNameIndex;

		m_pCtx->m_pwNameIndex = NULL;
		}

	if( m_pCtx->m_pStrings ) {

		delete [] m_pCtx->m_pStrings;

		m_pCtx->m_pStrings = NULL;
		}
	}

// End of File
