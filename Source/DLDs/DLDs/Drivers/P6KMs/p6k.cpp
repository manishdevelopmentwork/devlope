#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "p6k.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Compumotor 6K Master Serial Driver
//

Cmd CODE_SEG CParker6KBaseDriver::m_Cmd[] = {

{SEL_A,		"A",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_AA,	"AA",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_AD,	"AD",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_ADA,	"ADA",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_ADDR,	"ADDR",		FORM_INT,		RESP_NODP,	ACC_RW	},
{SEL_CMDDIR,	"CMDDIR",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_COMEXC,	"COMEXC",	FORM_X,			RESP_BITS,	ACC_RW	},
{SEL_COMEXL,	"COMEXL",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_COMEXR,	"COMEXR",	FORM_X,			RESP_BITS,	ACC_RW	},
{SEL_COMEXS,	"COMEXS",	FORM_INT,		RESP_NODP,	ACC_RW	},
{SEL_D,		"D",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_DACLIM,	"DACLIM",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_DRES,	"DRES",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_DRFEN,	"DRFEN",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_DRFLVL,	"DRFLVL",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_DRIVE,	"DRIVE",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_EFAIL,	"EFAIL",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_ENCCNT,	"ENCCNT",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_ENCPOL,	"ENCPOL",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_ENCSEND,	"ENCSEND",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_ERES,	"ERES",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_ESDB,	"ESDB",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_ESK,	"ESK",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_ESTALL,	"ESTALL",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_FFILT,	"FFILT",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_FGADV,	"FGADV",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_W	},
{SEL_FMAXA,	"FMAXA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FMAXV,	"FMAXV",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FMCLEN,	"FMCLEN",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FMCNEW,	"FMCNEW",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_W	},
{SEL_FMCP,	"FMCP",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FOLEN,	"FOLEN",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_FOLMD,	"FOLMD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FOLRD,	"FOLRD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FOLRN,	"FOLRN",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_FPPEN,	"FPPEN",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_FSHFC,	"FSHFC",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_W	},
{SEL_FSHFD,	"FSHFD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_W	},
{SEL_FVMACC,	"FVMACC",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_FVMFRQ,	"FVMFRQ",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_GO,	"GO",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_W	},
{SEL_GOL,	"GOL",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_W	},
{SEL_HALT,	"HALT",		FORM_NONE,		RESP_NODP,	ACC_W	},
{SEL_HOM,	"HOM",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_W	},
{SEL_HOMA,	"HOMA",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_HOMAA,	"HOMAA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_HOMAD,	"HOMAD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_HOMADA,	"HOMADA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_HOMBAC,	"HOMBAC",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_HOMDF,	"HOMDF",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_HOMDG,	"HOMDG",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_W	},
{SEL_HOMV,	"HOMV",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_HOMVF,	"HOMVF",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_HOMZ,	"HOMZ",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_INDUSE,	"INDUSE",	FORM_BIN,		RESP_BITS,	ACC_RW	},
{SEL_JOG,	"JOG",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_JOGA,	"JOGA",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOGAA,	"JOGAA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOGAD,	"JOGAD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOGADA,	"JOGADA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOGVH,	"JOGVH",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOGVL,	"JOGVL",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOY,	"JOY",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_JOYA,	"JOYA",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOYAA,	"JOYAA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOYAD,	"JOYAD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOYADA,	"JOYADA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOYVH,	"JOYVH",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_JOYVL,	"JOYVL",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_LH,	"LH",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_LHAD,	"LHAD",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_LHADA,	"LHADA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_LS,	"LS",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_LSAD,	"LSAD",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_LSADA,	"LSADA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_LSNEG,	"LSNEG",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_LSPOS,	"LSPOS",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_MA,	"MA",		FORM_AXIS | FORM_BIN,	RESP_RBITS,	ACC_RW	},
{SEL_MC,	"MC",		FORM_AXIS | FORM_BIN,	RESP_RBITS,	ACC_RW	},
{SEL_MEPOL,	"MEPOL",	FORM_X,			RESP_BITS,	ACC_RW	},
{SEL_MESND,	"MESND",	FORM_X,			RESP_BITS,	ACC_RW	},
{SEL_OUT,	"OUT",		FORM_BRK | FORM_X,	RESP_NONE,	ACC_W	},
{SEL_PA,	"PA",		FORM_REAL,		RESP_DP,	ACC_RW	},
{SEL_PAA,	"PAA",		FORM_REAL,		RESP_DP,	ACC_RW	},
{SEL_PAD,	"PAD",		FORM_REAL,		RESP_DP,	ACC_RW	},
{SEL_PADA,	"PADA",		FORM_REAL,		RESP_DP,	ACC_RW	},
{SEL_PV,	"PV",		FORM_REAL,		RESP_DP,	ACC_RW	},
{SEL_RE,	"RE",		FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_REGLOD,	"REGLOD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_REGSS,	"REGSS",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_S,		"S",		FORM_BIN,		RESP_RBITS,	ACC_W	},
{SEL_SCALE,	"SCALE",	FORM_BIN,		RESP_BITS,	ACC_RW	},
{SEL_SCLA,	"SCLA",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_SCLD,	"SCLD",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_SCLMAS,	"SCLMAS",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_SCLV,	"SCLV",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_SFB,	"SFB",		FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_SGAF,	"SGAF",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SGI,	"SGI",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SGILIM,	"SGILIM",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SGP,	"SGP",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SGV,	"SGV",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SGVF,	"SGVF",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SINAMP,	"SINAMP",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_SINANG,	"SINANG",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SINGO,	"SINGO",	FORM_AXIS | FORM_BIN,	RESP_RBITS,	ACC_RW	},
{SEL_SMPER,	"SMPER",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_SOFFS,	"SOFFS",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_STRGTD,	"STRGTD",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_STRGTE,	"STRGTE",	FORM_AXIS | FORM_X,	RESP_RBITS,	ACC_RW	},
{SEL_STRGTT,	"STRGTT",	FORM_AXIS | FORM_INT,	RESP_NODP,	ACC_RW	},
{SEL_STRGTV,	"STRGTV",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_TAS,	"TAS",		FORM_AXIS | FORM_IO,	RESP_RBITS,	ACC_R	},
{SEL_TASX,	"TASX",		FORM_AXIS | FORM_IO,	RESP_RBITS,	ACC_R	},
{SEL_TDAC,	"TDAC",		FORM_AXIS,		RESP_DP,	ACC_R	},
{SEL_TFB,	"TFB",		FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TFS,	"TFS",		FORM_AXIS | FORM_IO,	RESP_RBITS,	ACC_R	},
{SEL_TIN,	"TIN",		FORM_IO,		RESP_RBITS,	ACC_R	},
{SEL_TINO,	"TINO",		FORM_IO,		RESP_RBITS,	ACC_R	},
{SEL_TLIM,	"TLIM",		FORM_IO,		RESP_RBITS,	ACC_R	},
{SEL_TOUT,	"TOUT",		FORM_IO,		RESP_RBITS,	ACC_R	},
{SEL_TPC,	"TPC",		FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TPCME,	"TPCME",	FORM_NONE,		RESP_NODP,	ACC_R	},
{SEL_TPE,	"TPE",		FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TPER,	"TPER",		FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TPM,	"TPM",		FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TPMAS,	"TPMAS",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_R	},
{SEL_TPME,	"TPME",		FORM_NONE,		RESP_NODP,	ACC_R	},
{SEL_TPSHF,	"TPSHF",	FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TPSLV,	"TPSLV",	FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TRGLOT,	"TRGLOT",	FORM_REAL,		RESP_DP,	ACC_RW	},
{SEL_TSS,	"TSS",		FORM_IO,		RESP_RBITS,	ACC_R	},
{SEL_TSTLT,	"TSTLT",	FORM_AXIS,		RESP_NODP,	ACC_R	},
{SEL_TTASK,	"TTASK",	FORM_NONE,		RESP_NODP,	ACC_R	},
{SEL_TTIM,	"TTIM",		FORM_NONE,		RESP_NODP,	ACC_R	},
{SEL_TUS,	"TUS",		FORM_IO,		RESP_RBITS,	ACC_R	},
{SEL_TVEL,	"TVEL",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_R	},
{SEL_TVELA,	"TVELA",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_R	},
{SEL_TVMAS,	"TVMAS",	FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_R	},
{SEL_V,		"V",		FORM_AXIS | FORM_REAL,	RESP_DP,	ACC_RW	},
{SEL_VAR,	"VAR",		FORM_VAR  | FORM_REAL,	RESP_DPEQ,	ACC_RW	},
{SEL_VARB,	"VARB",		FORM_VAR  | FORM_X,	RESP_BITS,	ACC_RW	},
{SEL_VARCLR,	"VARCLR",	FORM_NONE,		RESP_NONE,	ACC_W	},	 
{SEL_VARI,	"VARI",		FORM_VAR  | FORM_INT,	RESP_EQ,	ACC_RW	},
{SEL_VARS,	"VARS",		FORM_VAR  | FORM_STR,	RESP_EQ,	ACC_RW	},
{SEL_RUNARG,	"RUNARG",	FORM_STR,		RESP_NONE,	ACC_RW	},
{SEL_RUN,	"RUN",		FORM_ARG,		RESP_NONE,	ACC_W	},
};

 // Constructor

CParker6KBaseDriver::CParker6KBaseDriver(void)
{
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 

	m_pCmd		= (Cmd FAR *)m_Cmd;
	}

// Destructor

CParker6KBaseDriver::~CParker6KBaseDriver(void)
{
	}

CCODE MCALL CParker6KBaseDriver::Ping(void)
{	
	
	return CCODE_SUCCESS;
			
	}

CCODE MCALL CParker6KBaseDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{

	return uCount;
		
	}

CCODE MCALL CParker6KBaseDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{

	return uCount;

	}
 
// Implementation

BOOL CParker6KBaseDriver::GetRead(UINT uOffset, UINT uType, PDWORD pData, UINT uCount)
{
	UINT uAddr = LOBYTE(uOffset);

	DWORD x = 0;

	switch( uType ) {

		case addrRealAsReal:

			ProcessResponse(m_pItem->m_Response);

			pData[0] = GetReal();

			break;

		case addrBitAsBit:

			ProcessResponse(m_pItem->m_Response);

			pData[0] = (m_pItem->m_Form & FORM_BLOC) ? GetBinary(uAddr) : GetBinary();

			break;

		case addrBitAsByte:
		case addrBitAsWord:
		case addrBitAsLong:

			ProcessResponse(m_pItem->m_Response);

			pData[0] = GetBinary();

			break;
	       
		default:

			ProcessResponse(m_pItem->m_Response);

			if ( m_pItem->m_Form == (FORM_VAR  | FORM_STR) ) {

				GetString(pData, uCount); 
				
				break;
				}

			if ( m_pItem->m_Response != RESP_BITS && m_pItem->m_Response != RESP_RBITS ) {

				pData[0] = GetInteger();
				}

			else {
				pData[0] = GetBinary();
				}

			break;
		}
	
	return TRUE;
	}
	
void CParker6KBaseDriver::Start(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));
	
	m_uPtr = 0;

	if ( m_pBase->m_bDrop > 9 ) {

		PutChar(m_pHex[m_pBase->m_bDrop / 10]);
		}
		
	PutChar(m_pHex[m_pBase->m_bDrop % 10]);

	PutChar('_');

	PutChar('!');
}	

BOOL CParker6KBaseDriver::PutReadCommand(UINT uOffset, UINT uType )
{      
	UINT uAddr = LOBYTE(uOffset);

	switch ( m_pItem->m_Form ) { 

		case FORM_NONE:
		case FORM_INT:
		case FORM_X:
		case FORM_BIN:
		case FORM_IO:
		case FORM_REAL:
		
			PutText(m_pItem->m_Text);

			break;

		case FORM_AXIS | FORM_REAL:
		case FORM_AXIS | FORM_X:
		case FORM_AXIS | FORM_INT:
		case FORM_AXIS | FORM_BIN:
		case FORM_AXIS | FORM_IO:
		case FORM_BRK  | FORM_X:
		case FORM_BRK  | FORM_INT:
		case FORM_BRK  | FORM_IO:
		case FORM_AXIS:

			if ( uType != addrBitAsByte ) {

				PutPrefix(uAddr, FALSE);
				}

			PutText(m_pItem->m_Text);

			break;

		case FORM_VAR | FORM_STR:

			uAddr = uOffset / 13;

		case FORM_VAR | FORM_REAL:
		case FORM_VAR | FORM_INT:
		case FORM_VAR | FORM_X:
		

			PutVARCommand(uAddr);
			
			break;

		default:
			return FALSE;
	       	}
	 
	return TRUE;
	}

void CParker6KBaseDriver::PutVARCommand(UINT uAddr)
{	
	PutText(m_pItem->m_Text);

	if ( uAddr > 99) {

		PutGeneric(uAddr, 10 , 100);
		}

	else if ( uAddr > 9 )	{

		PutGeneric(uAddr, 10 , 10);
		}

	else {	
		PutGeneric(uAddr, 10 , 1); 
		}

	return;
	
	}

BOOL CParker6KBaseDriver::PutWriteCommand(UINT uOffset, UINT uType, PDWORD pData, BOOL fGlobal)
{      
       	UINT uAddr = LOBYTE(uOffset);

	switch ( m_pItem->m_Form ) { 

		case FORM_NONE:
				
			PutText(m_pItem->m_Text);
		
			break;

		case FORM_AXIS | FORM_REAL:

			PutPrefix(uAddr, fGlobal);

		case FORM_REAL:
		
			PutText(m_pItem->m_Text);

			PutReal(pData);

			break;

		case FORM_AXIS | FORM_INT:
		case FORM_BRK  | FORM_INT:
		
			PutPrefix(uAddr, fGlobal);

		case FORM_INT:

			PutText(m_pItem->m_Text);

			PutInteger(pData);

			break;

		case FORM_AXIS | FORM_BIN:
		
			PutPrefix(uAddr, fGlobal);

		case FORM_X:
		case FORM_BIN:
	
			PutText(m_pItem->m_Text);

			if ( uType == addrBitAsBit ) {

				PutBinary(pData);
				}

			else {
			       	PutRBinary(pData, uType);
				}

			break;

		case FORM_AXIS | FORM_X:
		
			if ( fGlobal ) {

				PutPrefix(uAddr, fGlobal);
				}

		case FORM_BRK  | FORM_X:

			PutText(m_pItem->m_Text);

			if ( uType == addrBitAsBit ) {

				PutRBinaryBitAsByte(pData, uAddr, fGlobal);
				}

			else {
			       	PutRBinary(pData, uType);
				}

			break;

		case FORM_VAR | FORM_REAL:
		case FORM_VAR | FORM_INT:
				
			PutVARCommand(uAddr);

			PutChar('=');

			if ( uType == addrRealAsReal ) {

				PutReal(pData);
				}

			else if ( uType == addrLongAsLong ) {

				PutInteger(pData);
				}
			
			break;

		case FORM_VAR | FORM_X:

			PutVARCommand(uAddr);

			PutChar('=');

			PutChar('b');

			PutBinaryLong(pData);

			break;

		case FORM_VAR | FORM_STR:

			PutVARCommand(uOffset / 13);

			PutChar('=');

			PutChar('"');

			PutText(PCTXT(pData), 50);

			PutChar('"');

			break;
		 

		case FORM_ARG:

			PutText(m_pItem->m_Text);

			PutText(m_pBase->m_Arg1);

			break;
	       
		default:
			return FALSE;
	       	}

	return TRUE;
	}

BOOL CParker6KBaseDriver::GetArg(PDWORD pData, UINT uCount)
{	
	switch( m_pItem->m_ID ) {

		case SEL_RUNARG:

			for( UINT u = 0; u < uCount; u++ ) {

				if( u < sizeof(m_pBase->m_Arg1) - 1 ) {

					pData[u] = m_pBase->m_Arg1[u];
					}
				}

			return TRUE;
		}

	return FALSE;
	}

BOOL CParker6KBaseDriver::SetArg(PDWORD pData, UINT uCount)
{
	PCTXT pBytes = PCTXT(pData);
	
	switch( m_pItem->m_ID ) {

		case SEL_RUNARG:
			
			for( UINT u = 0; u < uCount; u++ ) {

				if( u < sizeof(m_pBase->m_Arg1) - 1 ) {

					m_pBase->m_Arg1[u] = pData[u] & 0xFF;
					}
				}

			return TRUE;
		}

	return FALSE;
	}

	
void CParker6KBaseDriver::End(void)
{
    	
	}
	
	
void CParker6KBaseDriver::PutText(LPCTXT pCmd)
{
	for( UINT i = 0; pCmd[i]; i++ ) {

		PutChar( pCmd[i] );
		}
	}

void CParker6KBaseDriver::PutText(LPCTXT pCmd, UINT uLimit)
{
	for( UINT i = 0; pCmd[i] && i < uLimit; i++ ) {

		PutChar( pCmd[i] );
		}
	}

void CParker6KBaseDriver::PutChar(char cChar)
{
	m_bTxBuff[m_uPtr++] = cChar;

	}

void CParker6KBaseDriver::PutGeneric(DWORD dwData, UINT uBase, DWORD dwFactor)
{
	while( dwFactor ) {

		PutChar( m_pHex[(dwData / dwFactor) % uBase] );

		dwFactor /= uBase;
		}
	}

void CParker6KBaseDriver::ProcessResponse(UINT uType)
{
	UINT uPtr1 = 1;  
	
	UINT uPtr2 = 0;
	
	UINT uChar;
	
	if ( m_pItem->m_Form & FORM_VAR ) { 
	
		while ( m_bRxBuff[uPtr1] != '=' ) {
			
			uPtr1++; 
			}
		
		uPtr1++; 

		}
	else {
		while ( m_bRxBuff[uPtr1] >= 'A' && m_bRxBuff[uPtr1] <= 'Z' ) {
			
			uPtr1++;
			}
		}

	while ( m_bRxBuff[uPtr1] != CR ) {

		switch ( m_pItem->m_Response ) {

			case RESP_BITS:
			case RESP_RBITS:

				if ( m_bRxBuff[uPtr1] == '_' ) {
					
					uPtr1++;
					}
				
				else if ( m_bRxBuff[uPtr1] == 'X' ) {
					
					m_bRxBuff[uPtr1] = '0';
					}
				
				m_bRxBuff[uPtr2++] = m_bRxBuff[uPtr1++];
				
				break;

			default:

				m_bRxBuff[uPtr2++] = m_bRxBuff[uPtr1++];
			}
		}
	
	m_bRxBuff[uPtr2] = CR;
	
	if ( uType == RESP_BITS ) { 
		
		uPtr1 = 0;
		
		while ( uPtr2 > uPtr1 ) {
		
			uChar = m_bRxBuff[--uPtr2];
		
			m_bRxBuff[uPtr2] = m_bRxBuff[uPtr1];
		
			m_bRxBuff[uPtr1++] = uChar;
			}
		}
	}
		
DWORD CParker6KBaseDriver::GetReal(void)
{
	DWORD dwData = 0;
	
	float Real = 0.0;

	LONG lDP = 0;

	BOOL fNeg = FALSE;

	for ( UINT u = 0; u < sizeof(m_bRxBuff); u++ ) {

		if ( m_bRxBuff[u] == CR ) {

			Real = fNeg ? Real * -1 : Real;

			dwData = *((DWORD*) &Real);

			return dwData;
			}

		else if ( m_bRxBuff[u] == '-' ) {

			fNeg = TRUE;

			continue;
			}

		else if ( m_bRxBuff[u] == '.' ) {

			lDP = 10;

			continue;
			}

		if( ( m_bRxBuff[u] >= '0' ) && ( m_bRxBuff[u] <= '9' ) ) {

			if ( !lDP ) {

				Real = (Real * 10) + ( m_bRxBuff[u] - '0' );

				}

			else {
				UINT uDigit = m_bRxBuff[u] - '0'; 

				if ( uDigit ) {

					float u = uDigit;

					float l = lDP;
					
					Real += ( u / l );

					}

				lDP *= 10;

				}
			}
		}

	return dwData;
       	}

void  CParker6KBaseDriver::PutReal(PDWORD pData)
{      
	HostAlignStack();

	char sFloat[20];

	memset(sFloat, 0, 20);

	SPrintf(sFloat, "%f", PassFloat(pData[0]));

	for( UINT u = 0; sFloat[u] && u < 12; u++ ) {

		PutChar(sFloat[u]);
		}
	}
	

DWORD CParker6KBaseDriver::GetInteger(void)
{
	DWORD dwData = 0;
	
	BOOL fNeg = FALSE;

	for ( UINT u = 0; u < sizeof(m_bRxBuff); u++ ) {

		if ( m_bRxBuff[u] == CR ) {

			return fNeg ? ~dwData + 1 : dwData;
			}

		if ( m_bRxBuff[u] == '-' ) {

			fNeg = TRUE;
			}

		if( ( m_bRxBuff[u] >= '0' ) && ( m_bRxBuff[u] <= '9' ) ) {

			dwData = (dwData * 10) + ( m_bRxBuff[u] - '0' );
				
			}
		}
	 
	return 0;
	}

void  CParker6KBaseDriver::GetString(PDWORD pData, UINT uCount)
{
	UINT uPtr;

	for( uPtr = 0; uPtr < uCount * 4; uPtr++ ) {

		if( m_bRxBuff[uPtr] == CR ) {

			m_bRxBuff[uPtr - 1] = '\0';

			m_bRxBuff[uPtr + 0] = '\0';

			m_bRxBuff[uPtr + 1] = '\0';

			m_bRxBuff[uPtr + 2] = '\0';

			break;
			}
		}

	UINT Count = (uPtr - 2) / 4;

	if( Count % 4 ) {

		Count++;
		}

	for( UINT u = 0; u < Count; u++ ) {

		pData[u] = PU4(m_bRxBuff + 1)[u];
		}
	}

void  CParker6KBaseDriver::PutInteger(PDWORD pData)
{	
	DWORD Data = DWORD(pData[0]);

	if ( Data > 0x7FFFFFFF ) {

		PutChar('-');

		Data = ~Data + 1;
		}

	else {
		PutChar('+');
		}

	DWORD Div = 1000000000;

	BOOL fFirst = FALSE;

	while ( Div ) {

		UINT uDigit = Data / Div;

		if ( uDigit && !fFirst ) {

			fFirst = TRUE;
			}

		if ( fFirst ) {

			PutChar(uDigit + '0');

			Data -= ( uDigit * Div );
			}

		Div /= 10;
		}
	}

DWORD CParker6KBaseDriver::GetBinary(UINT uBit)
{
	for ( UINT u = 0; u < sizeof(m_bRxBuff); u++ ) {

		if ( m_bRxBuff[u] == CR ) {

			return 0;
			}

		if( u == uBit - 1 ) {

			return m_bRxBuff[u] == '0' ? FALSE : TRUE;
				
			}
		}
	 
	return 0;
	}

void  CParker6KBaseDriver::PutBinaryLong(PDWORD pData)
{
	DWORD Data = pData[0];

	DWORD Mask = 0x80000000;

	for ( UINT u = 32; u > 0; u--, Mask /= 2 ) {

		if ( Data & Mask ) {

			PutChar('1');

			continue;
			}
		
		PutChar('0');
		
		}
       	}

DWORD CParker6KBaseDriver::GetBinary()
{
	DWORD dwData = 0;
	
	for ( UINT u = 0; u < 32; u++ ) {

		if ( m_bRxBuff[u] == CR ) {

			return dwData;
			}

		if( m_bRxBuff[u] == '0' || m_bRxBuff[u] == '1' ) {

			dwData |= ((m_bRxBuff[u] - '0') << u);

			}
		}
	 
	return dwData;
	}

void  CParker6KBaseDriver::PutBinary(PDWORD pData)
{
	pData[0] ? PutChar('1') : PutChar('0');

	}

void  CParker6KBaseDriver::PutRBinaryBitAsByte(PDWORD pData, UINT uBit, BOOL fGlobal) 
{
	if ( fGlobal ) {

		PutBinary(pData);
		}
	
	for ( UINT u = 0; u < 8; u++ ) {

		if ( uBit == u + 1 ) {

			pData[0] ? PutChar('1') : PutChar('0');

			continue;
			}
		
		PutChar('X');
		}
	}

void  CParker6KBaseDriver::PutRBinary(PDWORD pData, UINT uType)
{
	DWORD Data = pData[0];

	DWORD Mask = 0x01;

	UINT uSize = 0;

	switch ( uType ) {

		case addrBitAsByte:

			uSize = 8;
			break;

		case addrBitAsWord:

			uSize = 16;
			break;

		case addrBitAsLong:

			uSize = 32;
			break;
		}
	

	for ( UINT u = 0; u < uSize; u++, Mask *=2 ) {

		if ( Data & Mask ) {

			PutChar('1');

			continue;
			}

		PutChar('0');
		}
	}
	
void  CParker6KBaseDriver::PutPrefix(UINT uAddr, BOOL fGlobal)
{
	if( m_pBase->m_bDevice != DEV_GEM6K ) {
		
		if ( fGlobal ) {

			PutChar('@');

			return;
			}
			
		if ( uAddr > 9 ) {

			PutChar(m_pHex[uAddr/10]);
			}
		
		PutChar(m_pHex[uAddr % 10]);
		}
	}

void CParker6KBaseDriver::FindCmd(UINT uIndex)
{
	m_pItem = m_pCmd;
	
	for ( UINT u = 0; u < elements(m_Cmd); u++ ) {

		if ( m_pItem->m_ID == uIndex ) {

			return;
			}

		m_pItem++;
		}

	m_pItem = NULL;	
	}

UINT CParker6KBaseDriver::FindIndex(UINT uTable, UINT uOffset)
{
	return uTable != addrNamed ? uTable : uOffset;
	}
 
// End of File
