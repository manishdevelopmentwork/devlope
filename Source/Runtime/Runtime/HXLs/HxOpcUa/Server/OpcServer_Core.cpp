
#include "Intern.hpp"

#include "OpcServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "../Model/OpcVariableNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#define Add(n)							\
								\
	AddService( OpcUaId_##n##Request,			\
		    &OpcUa_##n##Response_EncodeableType,	\
		    OpcUa_Server_Begin##n,			\
		    (OpcUa_PfnInvokeService *) n##Stub		\
		    )						\

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Server
//

// Instantiator

static IUnknown * Create_OpcUaServer(PCTXT pName)
{
	return New COpcServer;
}

// Registration

global void Register_OpcUaServer(void)
{
	piob->RegisterInstantiator("opcua.opcua-server", Create_OpcUaServer);
}

global void Revoke_OpcUaServer(void)
{
	piob->RevokeInstantiator("opcua.opcua-server");
}

// Constructor

COpcServer::COpcServer(void)
{
	StdSetRef();

	m_uDebug     = 1;

	m_nMaxRefs   = 32;

	m_hEndpoint  = NULL;

	m_uSessId    = UINT(100000 + time(NULL) % 800000);

	m_uSubId     = 1001 + rand() % 8999;

	m_pMutex     = Create_Qutex();

	m_nOpcSec    = 0;

	m_pOpcSec    = NULL;

	for( UINT n = 0; n < elements(m_Session); n++ ) {

		CSession &Session = m_Session[n];

		Session.m_pServer = this;

		Session.m_uIndex  = n;

		Session.m_hTimer  = 0;

		Session.m_fInUse  = false;

		Session.m_fActive = false;

		Session.m_pEvent  = Create_AutoEvent();
	}

	memset(m_bHistGuid, 0, 16);

	m_uHistSample = 1;

	m_uHistQuota  = 50;

	AddServices();
}

// Destructor

COpcServer::~COpcServer(void)
{
	for( UINT n = 0; n < m_Services.GetCount(); n++ ) {

		delete m_Services[n];
	}

	for( UINT n = 0; n < elements(m_Session); n++ ) {

		CSession &Session = m_Session[n];

		Session.m_pEvent->Release();
	}

	m_pMutex->Release();

	delete m_pModel;
}

// IUnknown

HRESULT COpcServer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IOpcUaServer);

	StdQueryInterface(IOpcUaServer);

	return E_NOINTERFACE;
}

ULONG COpcServer::AddRef(void)
{
	StdAddRef();
}

ULONG COpcServer::Release(void)
{
	StdRelease();
}

// IOpcUaServer

void COpcServer::OpcFree(void)
{
	delete this;
}

void COpcServer::OpcLoad(COpcUaServerConfig const &Config)
{
	m_uDebug      = (Config.m_uSize > offset(COpcUaServerConfig, m_uDebug))  ? Config.m_uDebug  : 0;

	m_uHistSample = (Config.m_uSize > offset(COpcUaServerConfig, m_uSample)) ? Config.m_uSample : 1;

	m_uHistQuota  = (Config.m_uSize > offset(COpcUaServerConfig, m_uQuota))  ? Config.m_uQuota  : 50;

	m_uHistTime   = (Config.m_uSize > offset(COpcUaServerConfig, m_uTime))   ? Config.m_uTime   : 65;

	if( Config.m_uSize > offset(COpcUaServerConfig, m_pGuid) ) {

		memcpy(m_bHistGuid, Config.m_pGuid, 16);
	}

	if( Config.m_uSize > offset(COpcUaServerConfig, m_Pass) ) {

		AfxNewAutoObject(pParse, "crypto.pem-parser", IPemParser);

		if( pParse ) {

			if( Config.m_Cert.size() ) {

				pParse->Decode(m_Cert,
					       NULL,
					       Config.m_Cert.data(),
					       Config.m_Cert.size()
				);
			}

			if( Config.m_Priv.size() ) {

				pParse->Decode(m_Priv,
					       Config.m_Pass,
					       Config.m_Priv.data(),
					       Config.m_Priv.size()
				);
			}
		}

	}
}

bool COpcServer::OpcInit(IOpcUaServerHost *pHost, CString Endpoint)
{
	m_pHost = pHost;

	Endpoint.Tokenize(m_Endpoint, ',');

	BaseInit();

	InitSecurity();

	InitModel();

	OpcUa_Trace_Initialize();

	OpcUa_Trace_ChangeTraceLevel(OPCUA_TRACE_OUTPUT_LEVEL_NONE);

	m_StartTime = OpcUa_DateTime_UtcNow();

	HistoryInit();

	return true;
}

bool COpcServer::OpcExec(void)
{
	OpcUa_ServiceType **pServices = (OpcUa_ServiceType **) m_Services.GetPointer();

	OpcUa_Endpoint_Create(&m_hEndpoint,
			      OpcUa_Endpoint_SerializerType_Binary,
			      pServices
	);

	OpcUa_Endpoint_Open(m_hEndpoint,
			    PTXT(PCTXT(m_Endpoint[0])),
			    OpcUa_True,
			    EndpointCallbackStub,
			    this,
			    &m_OpcCert,
			    &m_OpcPriv,
			    &m_OpcPki,
			    m_nOpcSec,
			    m_pOpcSec
	);

	HistoryPoll();

	return true;
}

bool COpcServer::OpcStop(void)
{
	HistoryStop();

	if( m_hEndpoint ) {

		OpcUa_Endpoint_Close(m_hEndpoint);

		OpcUa_Endpoint_Delete(&m_hEndpoint);
	}

	for( UINT n = 0; n < elements(m_Session); n++ ) {

		CSession &Session = m_Session[n];

		OpcUa_Timer_Delete(&Session.m_hTimer);
	}

	BaseStop();

	return true;
}

bool COpcServer::OpcTerm(void)
{
	HistoryTerm();

	BaseTerm();

	return true;
}

// Implementation

void COpcServer::InitSecurity(void)
{
	m_OpcCert.Length = m_Cert.size();
	m_OpcCert.Data   = m_Cert.size() ? m_Cert.data() : PBYTE("");

	m_OpcPriv.Type          = OpcUa_Crypto_KeyType_Rsa_Private;
	m_OpcPriv.Key.Length    = m_Priv.size();
	m_OpcPriv.Key.Data      = m_Priv.size() ? m_Priv.data() : PBYTE("");
	m_OpcPriv.fpClearHandle = NULL;

	m_OpcPki.PkiType                           = OpcUa_NO_PKI;
	m_OpcPki.CertificateTrustListLocation      = NULL;
	m_OpcPki.CertificateRevocationListLocation = NULL;
	m_OpcPki.CertificateUntrustedListLocation  = NULL;
	m_OpcPki.Flags                             = 0;
	m_OpcPki.Override                          = NULL;

	m_nOpcSec = 1;

	m_pOpcSec = OpcAlloc(m_nOpcSec, OpcUa_Endpoint_SecurityPolicyConfiguration);

	SetSecurityNone(m_pOpcSec);
}

void COpcServer::AddServices(void)
{
	Add(GetEndpoints);
	Add(CreateSession);
	Add(ActivateSession);
	Add(CloseSession);
	Add(Call);
	Add(Browse);
	Add(BrowseNext);
	Add(TranslateBrowsePathsToNodeIds);
	Add(Read);
	Add(Write);
	Add(FindServers);
	Add(CreateSubscription);
	Add(ModifySubscription);
	Add(TransferSubscriptions);
	Add(SetPublishingMode);
	Add(DeleteSubscriptions);
	Add(CreateMonitoredItems);
	Add(ModifyMonitoredItems);
	Add(SetMonitoringMode);
	Add(DeleteMonitoredItems);
	Add(Publish);
	Add(Republish);
	Add(HistoryRead);

	m_Services.Append(NULL);
}

void COpcServer::InitModel(void)
{
	m_pModel = New COpcNanoModel;

	m_pModel->AddStandard();

	m_pHost->LoadModel(m_pModel);

	m_pModel->Validate();

	m_pModel->AddInverses();
}

void COpcServer::AddService(UINT uType, OpcUa_EncodeableType *pResp, OpcUa_PfnBeginInvokeService *pBegin, OpcUa_PfnInvokeService *pInvoke)
{
	OpcUa_ServiceType *pService = New OpcUa_ServiceType;

	pService->RequestTypeId = uType;
	pService->ResponseType  = pResp;
	pService->BeginInvoke   = pBegin;
	pService->Invoke        = pInvoke;

	m_Services.Append(pService);
}

void COpcServer::FillResponse(OpcUa_ResponseHeader *pRep, const OpcUa_RequestHeader *pReq, OpcUa_StatusCode Status)
{
	OpcUa_ResponseHeader_Initialize(pRep);

	pRep->RequestHandle = pReq->RequestHandle;

	OpcUa_DateTime t2 = OpcUa_DateTime_UtcNow();

	if( false ) {

		// This timeout mechanism is broken, as it depends
		// on having the clocks in sync, or very close...

		OpcUa_DateTime t1 = pReq->Timestamp;

		OpcUa_UInt32   dt = 0;

		GetDateTimeDiff(t1, t2, &dt);

		if( dt >= 10 ) {

			pRep->ServiceResult = OpcUa_BadTimeout;
		}
		else
			pRep->ServiceResult = Status;
	}
	else
		pRep->ServiceResult = Status;

	pRep->Timestamp = t2;

	if( pReq->ReturnDiagnostics ) {

		pRep->ServiceDiagnostics.SymbolicId    = -1;
		pRep->ServiceDiagnostics.NamespaceUri  = -1;
		pRep->ServiceDiagnostics.LocalizedText = -1;
		pRep->ServiceDiagnostics.Locale        = -1;
	}

	pRep->NoOfStringTable = 0;

	pRep->StringTable     = OpcUa_Null;
}

bool COpcServer::GetDateTimeDiff(OpcUa_DateTime Value1, OpcUa_DateTime Value2, OpcUa_UInt32 *puResult)
{
	OpcUa_UInt64 ullValue1;

	OpcUa_UInt64 ullValue2;

	OpcUa_UInt64 ullResult;

	*puResult = 0;

	ullValue1 = Value1.dwHighDateTime;

	ullValue1 = (ullValue1 << 32) + Value1.dwLowDateTime;

	ullValue2 = Value2.dwHighDateTime;

	ullValue2 = (ullValue2 << 32) + Value2.dwLowDateTime;

	ullResult = (OpcUa_UInt64) ((ullValue2 - ullValue1) / 10000);

	if( ullResult > OpcUa_UInt32_Max ) {

		*puResult = OpcUa_UInt32_Max;

		return true;
	}

	*puResult = (OpcUa_UInt32) ullResult;

	return true;
}

void COpcServer::SetSecurityNone(OpcUa_Endpoint_SecurityPolicyConfiguration *pSec)
{
	pSec->pbsClientCertificate = OpcUa_Null;

	OpcUa_String_Initialize(&pSec->sSecurityPolicy);

	OpcUa_String_AttachReadOnly(&pSec->sSecurityPolicy, OpcUa_SecurityPolicy_None);

	pSec->uMessageSecurityModes = OPCUA_ENDPOINT_MESSAGESECURITYMODE_NONE;
}

void COpcServer::CopyBytes(OpcUa_ByteString *pBytes, CByteArray const &Source)
{
	if( Source.size() ) {

		pBytes->Length = Source.size();

		pBytes->Data   = OpcAlloc(Source.size(), OpcUa_Byte);

		memcpy(pBytes->Data, Source.data(), Source.size());
	}
}

// Debugging

void COpcServer::AfxTrace(PCTXT pText, ...)
{
	if( m_uDebug >= 1 ) {

		va_list pArgs;

		va_start(pArgs, pText);

		::AfxTraceArgs(pText, pArgs);

		va_end(pArgs);
	}
}

void COpcServer::SubTrace(PCTXT pText, ...)
{
	if( m_uDebug >= 2 ) {

		va_list pArgs;

		va_start(pArgs, pText);

		::AfxTraceArgs(pText, pArgs);

		va_end(pArgs);
	}
}

// End of File
