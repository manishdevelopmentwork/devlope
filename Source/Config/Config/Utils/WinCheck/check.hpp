
//////////////////////////////////////////////////////////////////////////
//
// Signature Verification Application
//
// Copyright (c) 1993-2006 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CHECK_HPP
	
#define	INCLUDE_CHECK_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CCheckApp;
class CCheckFrameWnd;
class CCheckView;
class CCheckListBox;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CCheckApp : public CThread
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCheckApp(void);

		// Destructor
		~CCheckApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Frame Window
//

class CCheckFrameWnd : public CMainWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCheckFrameWnd(void);

		// Destructor
		~CCheckFrameWnd(void);

		// Operations
		BOOL Validate(CFilename File);

	protected:
		// Data Members
		CAccelerator m_Accel;
		CIcon        m_Icon;

		// Class Definition
		PCTXT GetDefaultClassName(void) const;
		BOOL  GetClassDetails(WNDCLASSEX &Class) const;

		// Message Map
		AfxDeclareMessageMap();

		// Accelerators
		BOOL OnAccelerator(MSG &Msg);
           
		// Message Handlers
		void OnClose(void);
           
		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test View Window
//

class CCheckViewWnd : public CViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCheckViewWnd(void);

		// Destructor
		~CCheckViewWnd(void);

		// Operations
		void Validate(CFilename File);

	protected:
		// Data Members
		CFilename        m_File;
		CCheckListBox  * m_pList;
		CMSCrypto        m_Crypto;
		HCRYPTKEY        m_hKey;
		UINT	         m_uBits;
		CString	         m_Error;
		UINT	         m_uLine;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Old);

		// Implementation
		BOOL LoadFile(void);
		BOOL LoadFile(FILE *pFile);
		void ReadSig(PSTR pData, KEY_SIG &Sig);
		BYTE FromHex(char c);
		UINT HostToMotor(UINT x);
		void ShowStatus(void);
		void AddString(PCSTR pText, UINT uCount);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test List Box
//

class CCheckListBox : public CListBox
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CCheckListBox(void);

		// Destructor
		~CCheckListBox(void);

		// Opertations
		void SetErrorLine(UINT uLine);

	protected:
		// Data Members
		CFont m_Font;
		UINT  m_uLine;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnMeasureItem(UINT uID, MEASUREITEMSTRUCT &Measure);
		void OnDrawItem(UINT uID, DRAWITEMSTRUCT &Draw);

		// Implementation
		void DrawText(CDC &DC, CRect &Rect, CString Text);
	};

// End of File

#endif
