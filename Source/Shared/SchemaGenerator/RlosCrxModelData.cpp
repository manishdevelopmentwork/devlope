
#include "Intern.hpp"

#include "RlosCrxModelData.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

#if defined(AEON_ENVIRONMENT)

extern ICrimsonIdentity    * Create_NandIdentity(UINT uStart, UINT uEnd);
extern IDatabase           * Create_NandDatabase(UINT uStart, UINT uEnd, UINT uFram, UINT uBase, UINT uPool);
extern IEventStorage       * Create_NandEventStorage(UINT uStart, UINT uEnd);
extern IPersist            * Create_NandPersist(UINT uStart, UINT uEnd);
extern IFirmwareProps      * Create_NandFirmwareProps(UINT uStart, UINT uEnd);
extern IFirmwareProgram    * Create_NandFirmwareProgram(UINT uStart, UINT uEnd);
extern IConfigStorage      * Create_NandConfigStorage(UINT uStart, UINT uEnd);
extern IDatabase	   * Create_FileDatabase(CString const &Root, UINT uBase, UINT uPool);
extern IConfigStorage      * Create_FileConfigStorage(CString const &Root);
extern ISchemaGenerator    * Create_RlosSchemaGenerator(void);
extern INetApplicator      * Create_RlosNetApplicator(void);
extern IHardwareApplicator * Create_RlosHardwareApplicator(void);

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS CR Series Model Data
//

// Instantiators

DLLAPI IPxeModel * Create_RlosCrxModelData(void)
{
	return new CRlosCrxModelData();
}

DLLAPI IPxeModel * Create_RlosCrxModelData(CString const &Model)
{
	return new CRlosCrxModelData(Model);
}

// Constructors

CRlosCrxModelData::CRlosCrxModelData(void) : CRlosBaseModelData()
{
	BindModel();
}

CRlosCrxModelData::CRlosCrxModelData(CString const &Model) : CRlosBaseModelData(Model)
{
	BindModel();
}

// IPxeModel

//	START	END	BLOCKS	SIZE	USAGE
//	=======	=======	=======	=======	===============
//	0	2	2	256K	Primary Loader
//	2	18	16	2MB	Boot Loader
//	18	146	128	16MB	Firmware
//	146	274	128	16MB	Upgrade
//	274	282	8	1MB	Identity
//	282	410	128	16MB    Event Log
//	410	538	128	16MB	Persistence
//	538	602	64	8MB	Device Config
//	602	4096	3584	440MB	Database
//	=======	=======	=======	=======	===============

void CRlosCrxModelData::MakeAppObjects(void)
{
	#if defined(AEON_ENVIRONMENT)

	piob->RegisterSingleton("c3.firmprops", 0, Create_NandFirmwareProps(18, 146));

	piob->RegisterSingleton("c3.firmpend", 0, Create_NandFirmwareProgram(146, 274));

	piob->RegisterSingleton("c3.identity", 0, Create_NandIdentity(274, 282));

	#if defined(AEON_PLAT_WIN32)

	piob->RegisterSingleton("c3.database", 0, Create_FileDatabase("\\!\\AppDbase\\", 16, 20480));

	#else

	piob->RegisterSingleton("c3.database", 0, Create_NandDatabase(602, 2048, 4, 16, 20480));

	#endif

	piob->RegisterSingleton("c3.eventstorage", 0, Create_NandEventStorage(282, 410));

	piob->RegisterSingleton("c3.persist", 0, Create_NandPersist(410, 538));

	#endif
}

void CRlosCrxModelData::MakePxeObjects(void)
{
	#if defined(AEON_ENVIRONMENT)

	piob->RegisterSingleton("c3.schema-generator", 0, Create_RlosSchemaGenerator());

	#if defined(AEON_PLAT_WIN32)

	piob->RegisterSingleton("c3.config-storage", 0, Create_FileConfigStorage("\\!\\SysDbase\\"));

	#else

	piob->RegisterSingleton("c3.config-storage", 0, Create_NandConfigStorage(538, 602));

	#endif

	piob->RegisterSingleton("c3.hardware-applicator", 0, Create_RlosHardwareApplicator());

	piob->RegisterSingleton("c3.net-applicator", 0, Create_RlosNetApplicator());

	#endif
}

BOOL CRlosCrxModelData::GetDispList(CArray<DWORD> &List)
{
	return (this->*m_pfnGetDispList)(List);
}

UINT CRlosCrxModelData::GetObjCount(char cTag)
{
	return (this->*m_pfnGetObjCount)(cTag);
}

PCDWORD CRlosCrxModelData::GetUsbPaths(char cTag)
{
	return (this->*m_pfnGetUsbPaths)(cTag);
}

// Implementation

void CRlosCrxModelData::BindModel(void)
{
	if( m_Model == T("co04") ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR104_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR1XX_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR1XX_GetUsbPaths;

		return;
	}

	if( m_Model.StartsWith(T("co07")) ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR107_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR1XX_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR1XX_GetUsbPaths;

		return;
	}

	if( m_Model.StartsWith(T("co10")) ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR110_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR1XX_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR1XX_GetUsbPaths;

		return;
	}

	if( m_Model == T("ca04") ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR304_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR304_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR3XX_GetUsbPaths;

		return;
	}

	if( m_Model.StartsWith(T("ca07")) ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR307_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR3XX_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR3XX_GetUsbPaths;

		return;
	}

	if( m_Model.StartsWith(T("ca10")) ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR310_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR3XX_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR3XX_GetUsbPaths;

		return;
	}

	if( m_Model == T("ca15") ) {

		m_pfnGetDispList = &CRlosCrxModelData::CR315_GetDispList;
		m_pfnGetObjCount = &CRlosCrxModelData::CR1XX_GetObjCount;
		m_pfnGetUsbPaths = &CRlosCrxModelData::CR1XX_GetUsbPaths;

		return;
	}

	AfxAssert(FALSE);
}

// Model Data

BOOL CRlosCrxModelData::CR104_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(480, 272));

	return TRUE;
}

BOOL CRlosCrxModelData::CR107_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 480));

	List.Append(MAKELONG(320, 240));

	return TRUE;
}

BOOL CRlosCrxModelData::CR110_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 600));

	List.Append(MAKELONG(640, 480));

	return TRUE;
}

UINT CRlosCrxModelData::CR1XX_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 1;
		case 'p': return 3;
		case 'b': return 0;
		case 'f': return 0;
		case 't': return 0;
		case 'g': return 4;
	}

	return 0;
}

PCDWORD CRlosCrxModelData::CR1XX_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

BOOL CRlosCrxModelData::CR304_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(480, 272));

	return TRUE;
}

UINT CRlosCrxModelData::CR304_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 1;
		case 'p': return 3;
		case 'b': return 1;
		case 'f': return 0;
		case 't': return 0;
		case 'g': return 16;
	}

	return 0;
}

BOOL CRlosCrxModelData::CR307_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 480));

	List.Append(MAKELONG(320, 240));

	return TRUE;
}

BOOL CRlosCrxModelData::CR310_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(800, 600));

	List.Append(MAKELONG(640, 480));

	return TRUE;
}

BOOL CRlosCrxModelData::CR315_GetDispList(CArray<DWORD> &List)
{
	List.Append(MAKELONG(1024, 768));

	return TRUE;
}

UINT CRlosCrxModelData::CR3XX_GetObjCount(char cTag)
{
	switch( cTag ) {

		case 'e': return 2;
		case 'p': return 4;
		case 'b': return 1;
		case 'f': return 0;
		case 't': return 0;
		case 'g': return 16;
	}

	return 0;
}

PCDWORD CRlosCrxModelData::CR3XX_GetUsbPaths(char cTag)
{
	#if defined(AEON_ENVIRONMENT)

	if( cTag == 'b' ) {

		static const UsbTreePath p[] = {

			{ { 1, 0, 2, 0, 2, 0, 0, 0, 0 } },

			{ 0 },

		};

		return PCDWORD(p);
	}

	#endif

	static DWORD const z[] = { 0, 0 };

	return z;
}

// End of File
