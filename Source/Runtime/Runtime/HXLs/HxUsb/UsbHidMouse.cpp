
#include "Intern.hpp"  

#include "UsbHidMouse.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hid Mouse Mapper
//

// Instantiator

IUsbHidMapper * Create_HidMouseMapper(void)
{
	IUsbHidMapper *p = New CUsbHidMouse;

	return p;
	}

// Constructor

CUsbHidMouse::CUsbHidMouse(void)
{
	m_pName    = "Hid Mouse Mapper";

	m_Debug	   = debugWarn;

	m_fEnable  = false;

	m_nScale   = 100;

	m_uDelay   = 10;

	m_fShow	   = false;

	m_fPress   = false;

	m_pPointer = NULL;

	AfxGetObject("input-d", 0, IInputQueue, m_pInput);
	
	AfxGetObject("display", 0, IDisplay,    m_pDisplay);

	AfxGetObject("display", 0, IPointer,    m_pPointer);

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHidMouse::~CUsbHidMouse(void)
{
	Trace(debugInfo, "Driver Destroyed");

	HidePointer();

	AfxRelease(m_pInput);

	AfxRelease(m_pDisplay);

	AfxRelease(m_pPointer);

	FreeBuffers();
	}

// IUsbHidMapper

BOOL CUsbHidMouse::SetReport(PCBYTE pReport, UINT uSize)
{
	if( m_pPointer ) {

		ParseReportDesc(pReport, uSize);
		
		MakeMax(m_uRecvSize, 24);
		
		InitBuffers();

		Configure();

		ResetPointer();

		ShowPointer();

		return true;
		}

	return false;
	}

UINT CUsbHidMouse::GetUsagePage(void)
{
	return pageDesktop;
	}

UINT CUsbHidMouse::GetUsageType(void)
{
	return desktopMouse;
	}

void CUsbHidMouse::SetConfig(PCBYTE pConfig, UINT uSize)
{
	if( uSize >= 1 * sizeof(UINT) ) {

		if( m_fEnable != PUINT(pConfig)[0] ) {

			m_fEnable = PUINT(pConfig)[0];

			HidePointer();

			ShowPointer();
			}
		}

	if( uSize >= 2 * sizeof(UINT) ) {

		if( m_nScale != PUINT(pConfig)[1] ) {

			m_nScale = PUINT(pConfig)[1];

			ResetPointer();
			}
		}

	if( uSize >= 3 * sizeof(UINT) ) {

		if( m_uDelay != PUINT(pConfig)[2] ) {

			m_uDelay = PUINT(pConfig)[2];

			ShowPointer();
			}
		}
	}

void CUsbHidMouse::Poll(void)
{
	while( RecvReport() ) {

		ParseReport();
		}

	CheckRepeat();

	PollPointer();
	}

// Helpers

void CUsbHidMouse::Configure(void)
{
	m_pHostDriver->SetIdle(0);

	m_pHostDriver->SetProtocol(protBoot);
	}

bool CUsbHidMouse::ParseReport(void)
{
	BOOL fPress = false;
	
	if( m_pRecvBuff[0] & 1 ) {

		fPress = true;
		}

	if( !m_fPress ) {

		// NOTE -- We clip to an area slightly larger than the display and
		// then again to the display itself. This avoids jitter at the edges
		// that can occur when the mouse changes its mind and sends a -ve
		// step straight after a +ve one.

		if( m_pRecvBuff[1] || m_pRecvBuff[2] ) {

			Move(m_xRaw, m_xMaxRaw, (signed char) m_pRecvBuff[1]);

			Move(m_yRaw, m_yMaxRaw, (signed char) m_pRecvBuff[2]);

			Clip(m_xRaw, m_xMaxRaw, 16 * m_nScale);

			Clip(m_yRaw, m_yMaxRaw, 16 * m_nScale);

			m_xPos = ScaleDown(m_xRaw);

			m_yPos = ScaleDown(m_yRaw);

			Clip(m_xPos, m_xMaxPos, 0);

			Clip(m_yPos, m_yMaxPos, 0);

			m_pPointer->SetPointerPos(m_xPos, m_yPos);

			ShowPointer();
			}
		}

	if( m_fPress ^ fPress ) {

		CInput Input;

		Input.x.i.m_Type   = typeTouch;

		Input.x.i.m_State  = fPress ? stateDown : stateUp;

		Input.x.i.m_Local  = TRUE;

		Input.x.d.t.m_XPos = m_xPos / 4;
	
		Input.x.d.t.m_YPos = m_yPos / 4;
	
		m_pInput->Store(Input.m_Ref);

		if( (m_fPress = fPress) ) {

			m_uRepeat = GetTickCount() + ToTicks(constDelay);
			}

		ShowPointer();
		}

	return true;
	}

// Implementation

void CUsbHidMouse::CheckRepeat(void)
{
	if( m_fPress ) {
		
		if( GetTickCount() > m_uRepeat ) {
						
			CInput Input;

			Input.x.i.m_Type   = typeTouch;

			Input.x.i.m_State  = stateRepeat;

			Input.x.i.m_Local  = TRUE;

			Input.x.d.t.m_XPos = m_xPos / 4;
	
			Input.x.d.t.m_YPos = m_yPos / 4;
	
			m_pInput->Store(Input.m_Ref);

			m_uRepeat = GetTickCount() + ToTicks(1000 / constRate);

			ShowPointer();
			}
		}
	}

void CUsbHidMouse::ResetPointer(void)
{
	m_pDisplay->GetSize(m_xMaxPos, m_yMaxPos);

	m_xMaxRaw = ScaleUp(m_xMaxPos);

	m_yMaxRaw = ScaleUp(m_yMaxPos);

	m_xPos	  = m_xMaxPos / 2;

	m_yPos	  = m_yMaxPos / 2;

	m_xRaw	  = m_xMaxRaw / 2;

	m_yRaw    = m_yMaxRaw / 2;

	m_pPointer->SetPointerPos(m_xPos, m_yPos);
	}

void CUsbHidMouse::ShowPointer(void)
{	
	if( !m_fShow ) {

		if( m_fEnable ) {

			m_pPointer->ShowPointer(true);
			}

		m_fShow = true;
		}

	m_uHide = GetTickCount() + ToTicks(1000 * m_uDelay);
	}

void CUsbHidMouse::HidePointer(void)
{
	if( m_fShow ) {

		m_pPointer->ShowPointer(false);

		m_fShow = false;
		}
	}

void CUsbHidMouse::PollPointer(void)
{
	if( m_fShow && m_uDelay ) {

		if( GetTickCount() >= m_uHide ) {

			m_pPointer->ShowPointer(false);

			m_fShow = false;
			}
		}
	}

void CUsbHidMouse::Move(int &nData, int nMax, int nRel)
{
	if( nRel < 0 ) {

		MakeMin(nData, nMax);
		}

	if( nRel > 0 ) {

		MakeMax(nData, 0);
		}

	nData += nRel;
	}

void CUsbHidMouse::Clip(int &nData, int nMax, int nMargin)
{
	if( nData < -nMargin ) {

		nData = -nMargin;
		}

	if( nData >= nMax + nMargin ) {

		nData = nMax + nMargin;
		}
	}

INT CUsbHidMouse::ScaleDown(int nData)
{
	return (nData * 100) / m_nScale;
	}

INT CUsbHidMouse::ScaleUp(int nData)
{
	return (nData * m_nScale) / 100;
	}

// End of File
