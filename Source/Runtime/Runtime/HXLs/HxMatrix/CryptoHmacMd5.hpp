
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHmacMd5_HPP

#define INCLUDE_CryptoHmacMd5_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHmac.hpp"

//////////////////////////////////////////////////////////////////////////
//
// MD5 Cryptographic HMAC
//

class CCryptoHmacMd5 : public CCryptoHmac
{
	public:
		// Constructor
		CCryptoHmacMd5(void);

		// ICryptoHmac
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE        m_bHash[MD5_HASH_SIZE];
		psHmacMd5_t m_Ctx;
	};

// End of File

#endif
