
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

class CAnalogInputVPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAnalogInputVPage(CAnalogInputBaseItem *pInput);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAnalogInputBaseItem  * m_pInput;
		CEt3CommsSystem       * m_pSystem;
		BOOL			m_fFeature1;
		BOOL			m_fFeature2;

		// Implementation
		void LoadIntegration(IUICreate *pView);
		void LoadChannels(IUICreate *pView);
		void AddRangeUI   (IUICreate *pView, UINT uChan);
		void AddFeature1UI(IUICreate *pView, UINT uChan);
		void AddFeature2UI(IUICreate *pView, UINT uChan);
	};

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Page
//

// Runtime Class

AfxImplementRuntimeClass(CAnalogInputVPage, CUIStdPage);

// Constructor

CAnalogInputVPage::CAnalogInputVPage(CAnalogInputBaseItem *pInput)
{
	m_Class   = AfxRuntimeClass(CAnalogInputBaseItem);	

	m_pInput  = pInput;

	m_pSystem = (CEt3CommsSystem *) pInput->GetDatabase()->GetSystemItem();

	UINT uIdent = m_pSystem->GetModuleIdent();

	if( uIdent == E3_MOD_32AI10V ){

		m_fFeature1 = TRUE;

		m_fFeature2 = TRUE;
		}
	else {
		m_fFeature1 = FALSE;

		m_fFeature2 = TRUE;
		}
	}

// Operations

BOOL CAnalogInputVPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);

	LoadIntegration(pView);

	LoadChannels(pView);

	return FALSE;
	}

// Implementation

void CAnalogInputVPage::LoadIntegration(IUICreate *pView)
{
	if( m_pSystem->HasFlag(L"HasAIIntegration") ) {

		CString Form;

		if( TRUE ) {

			Form += L"";	Form += L"0|Best 50/60 Hz (131ms per channel)";
			Form += L"|";	Form += L"16|Standard 50 Hz (130ms per channel)";
			Form += L"|";	Form += L"32|Standard 60 Hz (110ms per channel)";
			Form += L"|";	Form += L"80|Fast 50 Hz (21ms per channel)";
			Form += L"|";	Form += L"96|Fast 60 Hz (21ms per channel)";
			}

		//
		pView->StartGroup(CString(IDS_OPTIONS), 1);

		CUIData Data;

		Data.m_Tag	 = "Integration";

		Data.m_Label 	 = CString(IDS_INTEGRATION);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);

		pView->EndGroup(TRUE);	
		}
	}

void CAnalogInputVPage::LoadChannels(IUICreate *pView)
{
	UINT uCount;

	if( (uCount = m_pSystem->GetPhysAIs()) ) {

		UINT uCols = 0;

		if( TRUE ) {
			
			uCols ++;
			}

		if( m_fFeature1 ) {
			
			uCols ++;
			}

		if( m_fFeature2 ) {
			
			uCols ++;
			}

		pView->StartTable(CString(IDS_CHANNELS), uCols);

		if( TRUE ) {
			
			pView->AddColHead(CString(IDS_RANGE));
			}

		if( m_fFeature1 ) {
			
			CString Text = CString(IDS_FEATURE);

			Text = CString(IDS_SCALE_BEYOND_PV);
			
			pView->AddColHead(Text);
			}

		if( m_fFeature2 ) {

			CString Text = CString(IDS_FEATURE_2);

			Text = CString(IDS_RESOLUTION);
			
			pView->AddColHead(Text);
			}

		for( UINT n = 0; n < uCount; n ++ ) {

			pView->AddRowHead(CPrintf(L"AI%d", 1 + n));

			AddRangeUI   (pView, n);

			AddFeature1UI(pView, n);

			AddFeature2UI(pView, n);
			}

		pView->EndTable();
		}
	}

void CAnalogInputVPage::AddRangeUI(IUICreate *pView, UINT uChan)
{
	if( TRUE ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "0|Disabled";
			Form += "|";	Form += "3|+-62.5mV";
			Form += "|";	Form += "4|+-125mV";
			Form += "|";	Form += "5|+-250mV";
			Form += "|";	Form += "6|+-500mV";
			Form += "|";	Form += "7|+-1.0V";
			Form += "|";	Form += "8|+-2.0V";
			Form += "|";	Form += "9|+-5.0V";
			Form += "|";	Form += "10|0-10V";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Range%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Range", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputVPage::AddFeature1UI(IUICreate *pView, UINT uChan)
{
	if( m_fFeature1 ) {

		CString Form;

		if( TRUE ) {

			Form += "";	Form += "0|Disabled";
			Form += "|";	Form += "1|Enabled";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Feature1%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature1", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

void CAnalogInputVPage::AddFeature2UI(IUICreate *pView, UINT uChan)
{
	if( m_fFeature2 ) {

		CString Form;

		if( TRUE ) {

			Form += L"";	Form += L"0|16-bit integrating";
			Form += L"|";	Form += L"1|10-bit high speed";
			}

		//
		CUIData Data;

		Data.m_Tag	 = CPrintf(L"Feature2%2.2d", 1 + uChan);

		Data.m_Label	 = CPrintf(L"Channel %d Feature1", 1 + uChan);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

		Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

		Data.m_Format	 = Form;

		Data.m_Tip	 = CString(IDS_FORMAT);

		pView->AddUI(m_pInput, L"root", &Data);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Analog Input
//

// Dynamic Class

AfxImplementDynamicClass(CAnalogInputVItem, CAnalogInputBaseItem);

// Constructor

CAnalogInputVItem::CAnalogInputVItem(void)
{
	m_uType	= typeVoltage;
	}

// UI Creation

BOOL CAnalogInputVItem::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CAnalogInputVPage(this));

	return FALSE;
	}

// UI Update

void CAnalogInputVItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		for( UINT n = 0; n < elements(m_Range); n ++ ) {

			LimitEnum(pHost, CPrintf(L"Range%2.2d", 1+n), m_Range[n], 1 << 8);
			}
		
		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Range") ) {

		DoEnables(pHost);
		}

	if( Tag.StartsWith(L"Feature1") ) {

		UINT uLen  = CString(L"Feature1").GetLength();

		UINT uChan = watoi(Tag.Mid(uLen));

		if( m_Feature1[uChan-1] == 0 ) {

			m_Feature2[uChan-1] = 0;

			pHost->UpdateUI(CPrintf(L"Feature2%2.2d", uChan));
			}

		DoEnables(pHost);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Persistence

void CAnalogInputVItem::Init(void)
{
	CAnalogInputBaseItem::Init();

	m_Integration = 32;

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		m_Range[n] = 0;
		}

	for( UINT n = 0; n < elements(m_Feature1); n ++ ) {

		m_Feature1[n] = 1;
		}

	for( UINT n = 0; n < elements(m_Feature2); n ++ ) {

		m_Feature2[n] = 0;
		}
	}

// Download Support

void CAnalogInputVItem::PrepareData(void)
{
	CAnalogInputBaseItem::PrepareData();

	CFileDataBaseCfgSetup &File = m_pSystem->m_CfgSetup;

	UINT c = m_pSystem->GetPhysAIs();

	for( UINT n = 0; n < c; n ++ ) {
		
		CFileDataBaseCfgSetup::CAnalogInput &AI = File.m_AIs[n];

		UINT uRange = 0;

		if( m_Feature1[n] ) {

			uRange |= m_uType;

			uRange |= m_Range[n];
			}

		SetBit(uRange, m_Feature2[n], 7);

		AI.SetRange(uRange);
		}
	}

// Meta Data Creation

void CAnalogInputVItem::AddMetaData(void)
{
	CAnalogInputBaseItem::AddMetaData();
	}

// Implementation

void CAnalogInputVItem::DoEnables(IUIHost *pHost)
{
	UINT uCount = 2;

	for( UINT n = 0; n < elements(m_Range); n ++ ) {

		BOOL fFeature1 = m_Feature1[n] > 0;

		BOOL fFeature2 = n < uCount;

		pHost->EnableUI(CPrintf(L"Feature2%2.2d", 1+n), fFeature1 && fFeature2);
		}
	}

// End of File
