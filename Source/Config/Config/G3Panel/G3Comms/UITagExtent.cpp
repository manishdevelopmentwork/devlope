
#include "Intern.hpp"

#include "UITagExtent.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Expression
//

// Dynamic Class

AfxImplementDynamicClass(CUITagExtent, CUICategorizer);

// Constructor

CUITagExtent::CUITagExtent(void)
{
	}

// Category Overridables

BOOL CUITagExtent::LoadModeButton(void)
{
	CModeButton::COption Opt;

	m_pModeCtrl->ClearOptions();

	Opt.m_uID     = 0;
	Opt.m_Text    = CString(IDS_ONE_ITEM);
	Opt.m_fEnable = TRUE;
	Opt.m_fHidden = FALSE;
	Opt.m_Image   = 0;

	m_pModeCtrl->AddOption(Opt);

	Opt.m_uID     = 1;
	Opt.m_Text    = CString(IDS_ARRAY);
	Opt.m_fEnable = TRUE;
	Opt.m_fHidden = FALSE;
	Opt.m_Image   = 0;

	m_pModeCtrl->AddOption(Opt);

	return TRUE;
	}

BOOL CUITagExtent::SwitchMode(UINT uMode)
{
	if( uMode == 0 ) {

		StdSave(FALSE, L"0");

		ForceUpdate();

		return TRUE;
		}

	if( uMode == 1 ) {

		StdSave(FALSE, L"16");

		ForceUpdate();

		return TRUE;
		}

	return FALSE;
	}

UINT CUITagExtent::FindDispMode(CString Text)
{
	if( watoi(Text) ) {

		return 1;
		}

	return 0;
	}

CString CUITagExtent::FindDispText(CString Text, UINT uMode)
{
	return Text;
	}

CString CUITagExtent::FindDataText(CString Text, UINT uMode)
{
	return Text;
	}

UINT CUITagExtent::FindDispType(UINT uMode)
{
	if( uMode == 0 ) {

		return 0;
		}

	if( m_UIData.m_Format == L"*" ) {

		return 0;
		}

	return 2;
	}

CString CUITagExtent::FindDispVerb(UINT uMode)
{
	return L"";
	}

// End of File
