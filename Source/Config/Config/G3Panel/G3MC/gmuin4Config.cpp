
#include "intern.hpp"

#include "gmuin4.hpp"

#include "uiinpproc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Import Data
//

#include "import/graphite/uin4props.h"

#include "import/graphite/uin4dbase.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration
//

// Runtime Class

AfxImplementRuntimeClass(CGraphiteUIN4Config, CCommsItem);

// Property List

CCommsList const CGraphiteUIN4Config::m_CommsList[] = {

	{ 0, "InputType1",		PROPID_INPUT_TYPE1,		usageWriteInit,	IDS_NAME_IT1	},
	{ 0, "InputType2",		PROPID_INPUT_TYPE2,		usageWriteInit,	IDS_NAME_IT2	},
	{ 0, "InputType3",		PROPID_INPUT_TYPE3,		usageWriteInit,	IDS_NAME_IT3	},
	{ 0, "InputType4",		PROPID_INPUT_TYPE4,		usageWriteInit,	IDS_NAME_IT4	},

	{ 0, "InputTC1",		PROPID_INPUT_TC1,		usageWriteInit,	IDS_NAME_ITC1	},
	{ 0, "InputTC2",		PROPID_INPUT_TC2,		usageWriteInit,	IDS_NAME_ITC2	},
	{ 0, "InputTC3",		PROPID_INPUT_TC3,		usageWriteInit,	IDS_NAME_ITC3	},
	{ 0, "InputTC4",		PROPID_INPUT_TC4,		usageWriteInit,	IDS_NAME_ITC4	},

	{ 0, "TempUnits1",		PROPID_TEMP_UNITS1,		usageWriteInit,	IDS_NAME_TU1	},
	{ 0, "TempUnits2",		PROPID_TEMP_UNITS2,		usageWriteInit,	IDS_NAME_TU2	},
	{ 0, "TempUnits3",		PROPID_TEMP_UNITS3,		usageWriteInit,	IDS_NAME_TU3	},
	{ 0, "TempUnits4",		PROPID_TEMP_UNITS4,		usageWriteInit,	IDS_NAME_TU4	},

	{ 0, "SquareRoot1",		PROPID_SQUARE_ROOT1,		usageWriteInit,	IDS_NAME_SQRT1	},
	{ 0, "SquareRoot2",		PROPID_SQUARE_ROOT2,		usageWriteInit,	IDS_NAME_SQRT2	},
	{ 0, "SquareRoot3",		PROPID_SQUARE_ROOT3,		usageWriteInit,	IDS_NAME_SQRT3	},
	{ 0, "SquareRoot4",		PROPID_SQUARE_ROOT4,		usageWriteInit,	IDS_NAME_SQRT4	},

	{ 1, "PV1",			PROPID_PV1,			usageRead,	IDS_NAME_PV1	},
	{ 1, "PV2",			PROPID_PV2,			usageRead,	IDS_NAME_PV2	},
	{ 1, "PV3",			PROPID_PV3,			usageRead,	IDS_NAME_PV3	},
	{ 1, "PV4",			PROPID_PV4,			usageRead,	IDS_NAME_PV4	},

	{ 1, "ColdJunc1",		PROPID_COLD_JUNC1,		usageRead,	IDS_NAME_CJ1	},
	{ 1, "ColdJunc2",		PROPID_COLD_JUNC2,		usageRead,	IDS_NAME_CJ2	},
	{ 1, "ColdJunc3",		PROPID_COLD_JUNC3,		usageRead,	IDS_NAME_CJ3	},
	{ 1, "ColdJunc4",		PROPID_COLD_JUNC4,		usageRead,	IDS_NAME_CJ4	},

	{ 1, "InputAlarm1",		PROPID_INPUT_ALARM1,		usageRead,	IDS_NAME_IA1	},
	{ 1, "InputAlarm2",		PROPID_INPUT_ALARM2,		usageRead,	IDS_NAME_IA2	},
	{ 1, "InputAlarm3",		PROPID_INPUT_ALARM3,		usageRead,	IDS_NAME_IA3	},
	{ 1, "InputAlarm4",		PROPID_INPUT_ALARM4,		usageRead,	IDS_NAME_IA4	},

	{ 2, "InputFilter1",		PROPID_INPUT_FILTER1,		usageWriteBoth,	IDS_NAME_IF1	},
	{ 2, "InputFilter2",		PROPID_INPUT_FILTER2,		usageWriteBoth,	IDS_NAME_IF2	},
	{ 2, "InputFilter3",		PROPID_INPUT_FILTER3,		usageWriteBoth,	IDS_NAME_IF3	},
	{ 2, "InputFilter4",		PROPID_INPUT_FILTER4,		usageWriteBoth,	IDS_NAME_IF4	},

	{ 2, "InputOffset1",		PROPID_INPUT_OFFSET1,		usageWriteBoth,	IDS_NAME_IO1	},
	{ 2, "InputOffset2",		PROPID_INPUT_OFFSET2,		usageWriteBoth,	IDS_NAME_IO2	},
	{ 2, "InputOffset3",		PROPID_INPUT_OFFSET3,		usageWriteBoth,	IDS_NAME_IO3	},
	{ 2, "InputOffset4",		PROPID_INPUT_OFFSET4,		usageWriteBoth,	IDS_NAME_IO4	},

	{ 2, "InputSlope1",		PROPID_INPUT_SLOPE1,		usageWriteBoth,	IDS_NAME_IS1	},
	{ 2, "InputSlope2",		PROPID_INPUT_SLOPE2,		usageWriteBoth,	IDS_NAME_IS2	},
	{ 2, "InputSlope3",		PROPID_INPUT_SLOPE3,		usageWriteBoth,	IDS_NAME_IS3	},
	{ 2, "InputSlope4",		PROPID_INPUT_SLOPE4,		usageWriteBoth,	IDS_NAME_IS4	},

	{ 2, "ProcMin1",		PROPID_PROC_MIN1,		usageWriteInit,	IDS_NAME_PMI1	},
	{ 2, "ProcMin2",		PROPID_PROC_MIN2,		usageWriteInit,	IDS_NAME_PMI2	},
	{ 2, "ProcMin3",		PROPID_PROC_MIN3,		usageWriteInit,	IDS_NAME_PMI3	},
	{ 2, "ProcMin4",		PROPID_PROC_MIN4,		usageWriteInit,	IDS_NAME_PMI4	},

	{ 2, "ProcMax1",		PROPID_PROC_MAX1,		usageWriteInit,	IDS_NAME_PMA1	},
	{ 2, "ProcMax2",		PROPID_PROC_MAX2,		usageWriteInit,	IDS_NAME_PMA2	},
	{ 2, "ProcMax3",		PROPID_PROC_MAX3,		usageWriteInit,	IDS_NAME_PMA3	},
	{ 2, "ProcMax4",		PROPID_PROC_MAX4,		usageWriteInit,	IDS_NAME_PMA4	},

	};

// Constructor

CGraphiteUIN4Config::CGraphiteUIN4Config(void)
{
	m_InputType1	= INPUT_010V;
	m_InputType2	= INPUT_010V;
	m_InputType3	= INPUT_010V;
	m_InputType4	= INPUT_010V;

	m_InputTC1	= TC_TYPEB;
	m_InputTC2	= TC_TYPEB;
	m_InputTC3	= TC_TYPEB;
	m_InputTC4	= TC_TYPEB;

	m_TempUnits1	= DEGREES_F;
	m_TempUnits2	= DEGREES_F;
	m_TempUnits3	= DEGREES_F;
	m_TempUnits4	= DEGREES_F;

	m_InputOffset1	= 0;
	m_InputOffset2	= 0;
	m_InputOffset3	= 0;
	m_InputOffset4	= 0;

	m_InputSlope1	= 1000;
	m_InputSlope2	= 1000;
	m_InputSlope3	= 1000;
	m_InputSlope4	= 1000;

	m_InputFilter1	= 20;
	m_InputFilter2	= 20;
	m_InputFilter3	= 20;
	m_InputFilter4	= 20;

	m_SquareRoot1	= FALSE;
	m_SquareRoot2	= FALSE;
	m_SquareRoot3	= FALSE;
	m_SquareRoot4	= FALSE;

	m_PV1		= 0;
	m_PV2		= 0;
	m_PV3		= 0;
	m_PV4		= 0;

	m_InputAlarm1	= 0;
	m_InputAlarm2	= 0;
	m_InputAlarm3	= 0;
	m_InputAlarm4	= 0;

	m_ColdJunc1	= 0;
	m_ColdJunc2	= 0;
	m_ColdJunc3	= 0;
	m_ColdJunc4	= 0;

	m_ProcUnits1	= "%";
	m_ProcUnits2	= "%";
	m_ProcUnits3	= "%";
	m_ProcUnits4	= "%";

	m_ProcMin1	= 0;
	m_ProcMin2	= 0;
	m_ProcMin3	= 0;
	m_ProcMin4	= 0;

	m_ProcMax1	= 10000;
	m_ProcMax2	= 10000;
	m_ProcMax3	= 10000;
	m_ProcMax4	= 10000;

	m_ProcDP1	= 2;
	m_ProcDP2	= 2;
	m_ProcDP3	= 2;
	m_ProcDP4	= 2;

	m_uCommsCount = elements(m_CommsList);
	
	m_pCommsData  = m_CommsList;

	CheckCommsData();
	}

// Group Names

CString CGraphiteUIN4Config::GetGroupName(WORD Group)
{
	switch( Group ) {
		
		case 1:	return L"Status";
		case 2:	return L"Control";
		}

	return CCommsItem::GetGroupName(Group);
	}

// View Pages

UINT CGraphiteUIN4Config::GetPageCount(void)
{
	return 4;
	}

CString CGraphiteUIN4Config::GetPageName(UINT n)
{
	switch( n ) {

		case 0: 
		case 1: 
		case 2: 
		case 3: 
			return CPrintf("Input %d", n + 1);
		}

	return L"";
	}

CViewWnd * CGraphiteUIN4Config::CreatePage(UINT n)
{
	switch( n ) {

		case 0: 
		case 1: 
		case 2: 
		case 3: 
			return New CGraphiteUIN4ConfigWnd(n);
		}

	return NULL;
	}

// Property Filter

BOOL CGraphiteUIN4Config::IncludeProp(WORD PropID)
{
	return TRUE;
	}

// Meta Data Creation

void CGraphiteUIN4Config::AddMetaData(void)
{
	Meta_AddInteger(InputType1);
	Meta_AddInteger(InputType2);
	Meta_AddInteger(InputType3);
	Meta_AddInteger(InputType4);

	Meta_AddInteger(InputTC1);
	Meta_AddInteger(InputTC2);
	Meta_AddInteger(InputTC3);
	Meta_AddInteger(InputTC4);

	Meta_AddInteger(TempUnits1);
	Meta_AddInteger(TempUnits2);
	Meta_AddInteger(TempUnits3);
	Meta_AddInteger(TempUnits4);

	Meta_AddInteger(InputOffset1);
	Meta_AddInteger(InputOffset2);
	Meta_AddInteger(InputOffset3);
	Meta_AddInteger(InputOffset4);

	Meta_AddInteger(InputSlope1);
	Meta_AddInteger(InputSlope2);
	Meta_AddInteger(InputSlope3);
	Meta_AddInteger(InputSlope4);

	Meta_AddInteger(InputFilter1);
	Meta_AddInteger(InputFilter2);
	Meta_AddInteger(InputFilter3);
	Meta_AddInteger(InputFilter4);

	Meta_AddInteger(SquareRoot1);
	Meta_AddInteger(SquareRoot2);
	Meta_AddInteger(SquareRoot3);
	Meta_AddInteger(SquareRoot4);

	Meta_AddInteger(PV1);
	Meta_AddInteger(PV2);
	Meta_AddInteger(PV3);
	Meta_AddInteger(PV4);

	Meta_AddInteger(InputAlarm1);
	Meta_AddInteger(InputAlarm2);
	Meta_AddInteger(InputAlarm3);
	Meta_AddInteger(InputAlarm4);

	Meta_AddInteger(ColdJunc1);
	Meta_AddInteger(ColdJunc2);
	Meta_AddInteger(ColdJunc3);
	Meta_AddInteger(ColdJunc4);

	Meta_AddString(ProcUnits1);
	Meta_AddString(ProcUnits2);
	Meta_AddString(ProcUnits3);
	Meta_AddString(ProcUnits4);

	Meta_AddInteger(ProcMin1);
	Meta_AddInteger(ProcMin2);
	Meta_AddInteger(ProcMin3);
	Meta_AddInteger(ProcMin4);

	Meta_AddInteger(ProcMax1);
	Meta_AddInteger(ProcMax2);
	Meta_AddInteger(ProcMax3);
	Meta_AddInteger(ProcMax4);

	Meta_AddInteger(ProcDP1);
	Meta_AddInteger(ProcDP2);
	Meta_AddInteger(ProcDP3);
	Meta_AddInteger(ProcDP4);

	CCommsItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Configuration View
// 

// Runtime Class

AfxImplementRuntimeClass(CGraphiteUIN4ConfigWnd, CUIViewWnd);

// Constructor

CGraphiteUIN4ConfigWnd::CGraphiteUIN4ConfigWnd(UINT uPage)
{
	m_uPage = uPage;
	}

// Overidables

void CGraphiteUIN4ConfigWnd::OnAttach(void)
{
	CUIViewWnd::OnAttach();

	m_pItem   = (CGraphiteUIN4Config *) CViewWnd::m_pItem;

	m_pSchema = New CUISchema(m_pItem);

	m_pSchema->LoadFromFile(TEXT("gmuin4"));

	CUIViewWnd::OnAttach();
	}

// UI Management

void CGraphiteUIN4ConfigWnd::OnUICreate(void)
{
	StartPage(1);

	AddOperation();

	AddUnits();

	EndPage(TRUE);
	}

void CGraphiteUIN4ConfigWnd::OnUIChange(CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {		

		DoEnables(1);
		DoEnables(2);
		DoEnables(3);
		DoEnables(4);

		return;
		}

	if( Tag.StartsWith(L"InputType") ) {

		UINT  uIndex = watoi(Tag.Mid(wstrlen(L"InputType")));

		CString Type = CPrintf("InputTC%d", uIndex);

		if( GetInteger(Tag) == INPUT_RTD ) {

			if( GetInteger(Type) < TC_TYPE385 ) {

				PutInteger(Type, TC_TYPE385);

				UpdateUI(Type);
				}
			}
		
		if( GetInteger(Tag) == INPUT_TC ) {

			if( GetInteger(Type) >= TC_TYPE385 ) {

				PutInteger(Type, TC_TYPEB);

				UpdateUI(Type);
				}
			}
		
		EnableTC(uIndex);
		}

	if( Tag.StartsWith(L"InputTC") ) {

		UINT  uIndex = watoi(Tag.Mid(wstrlen(L"InputTC")));

		CString Mode = CPrintf("InputType%d", uIndex);

		if( GetInteger(Tag) < TC_TYPE385 ) {			

			if( GetInteger(Mode) == INPUT_RTD ) {

				PutInteger(Mode, INPUT_TC);

				UpdateUI(Mode);
				}
			}

		if( GetInteger(Tag) >= TC_TYPE385 ) {

			if( GetInteger(Mode) == INPUT_TC ) {

				PutInteger(Mode, INPUT_RTD);

				UpdateUI(Mode);
				}
			}
		}

	CUIInputProcess::CheckUpdate(m_pItem, Tag);
	}

// Implementation

void CGraphiteUIN4ConfigWnd::AddOperation(void)
{
	StartGroup(CString("Operation"), 1);

	AddUI(m_pItem, TEXT("root"), CPrintf("InputType%d",    m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("InputTC%d",      m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("InputFilter%d",  m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("InputOffset%d",  m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("InputSlope%d",   m_uPage + 1));

	EndGroup(TRUE);
	}

void CGraphiteUIN4ConfigWnd::AddUnits(void)
{
	StartGroup(CString("Units"), 1);

	AddUI(m_pItem, TEXT("root"), CPrintf("TempUnits%d",   m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("ProcUnits%d",   m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("ProcDP%d",      m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("ProcMin%d",     m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("ProcMax%d",     m_uPage + 1));
	AddUI(m_pItem, TEXT("root"), CPrintf("SquareRoot%d",  m_uPage + 1));

	EndGroup(TRUE);
	}

UINT CGraphiteUIN4ConfigWnd::GetInteger(CString Tag)
{
	CMetaData const *pData = m_pItem->FindMetaData(Tag);

	return pData->ReadInteger(m_pItem);
	}

void CGraphiteUIN4ConfigWnd::PutInteger(CString Tag, UINT Data)
{
	CMetaData const *pData = m_pItem->FindMetaData(Tag);

	pData->WriteInteger(m_pItem, Data);
	}

// Enabling

void CGraphiteUIN4ConfigWnd::DoEnables(UINT uIndex)
{
	EnableTC(uIndex);
	}

void CGraphiteUIN4ConfigWnd::EnableTC(UINT uIndex)
{
	BOOL fEnable = (GetInteger(CPrintf("InputType%d", uIndex)) >= INPUT_RTD);

	BOOL fMilli  = (GetInteger(CPrintf("InputType%d", uIndex)) == INPUT_050MV);

	EnableUI(CPrintf("InputTC%d",     uIndex),  fEnable);
	EnableUI(CPrintf("TempUnits%d",   uIndex),  fEnable);
	EnableUI(CPrintf("InputOffset%d", uIndex),  fEnable);
	EnableUI(CPrintf("InputSlope%d",  uIndex),  fEnable);
	EnableUI(CPrintf("ProcUnits%d",   uIndex), !fEnable);
	EnableUI(CPrintf("ProcDP%d",      uIndex), !fEnable);
	EnableUI(CPrintf("ProcMin%d",     uIndex), !fEnable);
	EnableUI(CPrintf("ProcMax%d",     uIndex), !fEnable);
	EnableUI(CPrintf("SquareRoot%d",  uIndex), !fEnable && !fMilli);
	}

// End of File
