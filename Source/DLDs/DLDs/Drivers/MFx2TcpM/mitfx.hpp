/////////////////////////////////////////////////////////////////////////
//
// Constants
//
 
#define	FRAME_TIMEOUT	1000

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi FX Series PLC Master Driver
//

class CMitFXMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CMitFXMasterDriver(void);

		// Destructor
		~CMitFXMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Data Members
		UINT		m_uPtr;
		LPCTXT 		m_pHex;
		BYTE		m_bTxBuff[256];
		BYTE		m_bRxBuff[256];

	virtual	void NewPacket(void);
		void AddHex(WORD wData, UINT uMask);
		void AddByte(BYTE bNew);
		void SwapBytes(WORD &wSwap);
		void AddWord(WORD wNew);
		void AddLong(DWORD dwNew);
	virtual	BOOL Transact(void);
		BOOL TxPacket(void);
		BOOL RxPacket(void);
		WORD xtoin(PCTXT pText, UINT uCount);

		// Read Handlers

		CCODE ReadWord(PDWORD pData, UINT uCount);
		CCODE ReadLong(PDWORD pData, UINT uCount);

		// Write Handlers

		CCODE WriteWord(PDWORD pData, UINT uCount);
		CCODE WriteLong(PDWORD pData, UINT uCount);

		// Additions based on Edict-97 code

		BOOL RegisterRelay(UINT uTarget, UINT uCount);
		BOOL RegisterCounter(UINT uTarget, UINT uCount);
		BOOL TriggerRead(UINT uCount);
		BOOL Confirm(void);
		BOOL Begin(void);
		BOOL End(void);

		
	};

// End of File
