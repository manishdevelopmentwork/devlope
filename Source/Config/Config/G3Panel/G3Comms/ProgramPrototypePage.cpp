
#include "Intern.hpp"

#include "ProgramPrototypePage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ProgramPrototype.hpp"
#include "ProgramParameter.hpp"
#include "ProgramParameterList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Prototype Page
//

// Runtime Class

AfxImplementRuntimeClass(CProgramPrototypePage, CUIPage);

// Constructor

CProgramPrototypePage::CProgramPrototypePage(CProgramPrototype *pPrototype)
{
	m_pPrototype = pPrototype;
	}

// Operations

BOOL CProgramPrototypePage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartTable(CString(IDS_PARAMETERS), 2);

	pView->AddColHead(CString(IDS_TYPE));

	pView->AddColHead(CString(IDS_NAME));

	UINT c = m_pPrototype->m_pParams->GetItemCount();

	for( UINT n = 0; n < c; n++ ) {

		CItem *pItem = m_pPrototype->m_pParams->GetItem(n);

		pView->AddRowHead(CPrintf(L"%u", 1+n));

		pView->AddUI(pItem, L"root", L"Type");

		pView->AddUI(pItem, L"root", L"Name");
		}

	pView->EndTable();

	return TRUE;
	}

// End of File
