
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Test Application
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP

#define	INCLUDE_TESTING_HPP
	
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTestApp;
class CTestWnd;
class CTestName;
class CTestData;

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

class CTestApp : public CThread
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTestApp(void);

		// Destructor
		~CTestApp(void);

	protected:
		// Overridables
		BOOL OnInitialize(void);
		BOOL OnTranslateMessage(MSG &Msg);
		void OnException(EXCEPTION Ex);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnGoingIdle(void);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

class CTestWnd : public CMenuWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTestWnd(void);

		// Destructor
		~CTestWnd(void);

	protected:
		// Data Members
		CAccelerator m_Accel;
		CEditCtrl *  m_pEdit;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnAccelerator(MSG &Msg);
		void OnPostCreate(void);
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSize(UINT uCode, CSize Size);
		void OnSetFocus(CWnd &Wnd);

		// Command Handlers
		BOOL OnCommandControl(UINT uID, CCmdSource &Src);
		BOOL OnCommandExecute(UINT uID);
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Name Server
//

class CTestName : public INameServer
{
	public:
		// Constructor
		CTestName(void);

		// Destructor
		~CTestName(void);

		// IBase
		UINT Release(void);

		// INameServer Methods
		BOOL FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NameIdent(WORD ID, CString &Name);
		BOOL FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type);
		BOOL NameProp(WORD ID, WORD PropID, CString &Name);
		BOOL FindDirect(CError *pError, CString Name, DWORD &ID, CTypeDef &Type);
		BOOL NameDirect(DWORD ID, CString &Name);
		BOOL FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount);
		BOOL NameFunction(WORD ID, CString &Name);
		BOOL EditFunction(WORD &ID, CTypeDef &Type);
		BOOL GetFuncParam(WORD ID, UINT	uParam, CString	&Name, CTypeDef	&Type);
		BOOL FindClass(CError *pError, CString Name, UINT &Type);
		BOOL NameClass(UINT Type, CString &Name);
		BOOL SaveClass(UINT Type);

	protected:
		// Data Members
		IFuncLibrary *m_pLib;
	};

//////////////////////////////////////////////////////////////////////////
//
// Test Data Source
//

class CTestData : public IDataServer
{
	public:
		// Constructor
		CTestData(void);

		// Operations
		void Dump(void);

		// IDataServer Methods
		DWORD GetData(DWORD ID, UINT Type);
		PVOID GetItem(DWORD ID);
		void  SetData(DWORD ID, UINT Type, DWORD dwData);
		DWORD GetProp(DWORD ID, WORD Prop, UINT	Type);
		DWORD RunFunc(WORD  ID, UINT uParam, PDWORD pParam);

	protected:
		// Data Members
		UINT m_I[10];
	};

// End of File

#endif
