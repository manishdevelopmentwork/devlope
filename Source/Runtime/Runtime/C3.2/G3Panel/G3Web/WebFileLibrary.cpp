
#include "Intern.hpp"

#include "WebFileLibrary.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "WebFileData.hpp"

#include "EnhancedWebServer.hpp"

#include "TheHttpServer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// File Library
//

// Static Data

static UINT const defExpire = 60 * 60 * 24;

// Instance Pointer

CWebFileLibrary * CWebFileLibrary::m_pThis = NULL;

// File Registration

void AddWebFile(CWebFileData const *pFile)
{
	CWebFileLibrary::m_pThis->AddFile(pFile);
	}

// Constructor

CWebFileLibrary::CWebFileLibrary(CEnhancedWebServer *pServer, CHttpServerManager *pManager)
{
	m_pServer  = pServer;

	m_pManager = pManager;

	m_pThis    = this;

	FindTimes();

	MakeIndex();

	extern void AddWebFiles(void);

	AddWebFiles();
	}

// Destructor

CWebFileLibrary::~CWebFileLibrary(void)
{
	}

// Operations

void CWebFileLibrary::AddFile(CWebFileData const *pFile)
{
	CString Name = pFile->m_pName;

	if( Name.Left(6) == "/html/" ) {

		Name = Name.Mid(5);
		}
	else
		Name = "/assets" + Name;

	m_Index.Insert(Name, pFile);
	}

CString CWebFileLibrary::GetFileText(CWebReqContext const &Ctx, CString Name)
{
	INDEX i = m_Index.FindName(Name);

	if( !m_Index.Failed(i) ) {

		CWebFileData const *pFile = m_Index[i];

		return CString(PCSTR(pFile->m_pData));
		}

	return "";
	}

// Implementation

BOOL CWebFileLibrary::ReplyWithFile(CWebReqContext const &Ctx, CString Name)
{
	INDEX i = m_Index.FindName(Name);

	if( !m_Index.Failed(i) ) {

		CWebFileData const *pFile = m_Index[i];
		
		BOOL	fHtml       = !strnicmp(pFile->m_pType, "htm", 3);

		BOOL	fJson       = !stricmp (pFile->m_pType, "json");
		
		CPrintf CacheTag    = CPrintf("SFile:%8.8x:%8.8x", pFile, m_timeComp);
		
		time_t	timeCreated = m_timeComp;
		
		time_t	timeExpires = time(NULL) + defExpire;

		BOOL	fCache      = TRUE;

		if( fHtml || fJson ) {

			CacheTag.AppendPrintf(":%8.8x", m_pServer->GetDatabaseTime());

			if( fHtml ) {

				if( !m_pServer->CanAccessHtml(Name) ) {

					Ctx.pReq->SetStatus(403);

					return TRUE;
					}

				timeCreated = m_timeBoot;
				}
			
			timeExpires = 0;
			}

		if( !Ctx.pReq->IsUnchanged(timeCreated, CacheTag) ) {

			Ctx.pReq->SetContentType(m_pManager->GetMimeType(pFile->m_pType));

			if( fHtml ) {

				CString Text(PCTXT(pFile->m_pData));

				m_pServer->ExpandAllElements(Ctx, Text, fCache);

				if( m_pServer->m_SameOrigin ) {

					Ctx.pReq->AddReplyHeader("X-Frame-Options", "SAMEORIGIN");
				}

				Ctx.pReq->SetReplyBody(Text);
				}
			else {
				UINT s = pFile->m_uSize - 1;

				Ctx.pReq->SetReplyBody(pFile->m_pData, s);
				}

			if( fCache ) {

				Ctx.pReq->AddCacheInfo( timeCreated,
							timeExpires,
							CacheTag
							);
				}

			Ctx.pReq->SetStatus(200);

			return TRUE;
			}

		Ctx.pReq->AddCacheInfo( timeCreated,
					timeExpires,
					CacheTag
					);

		Ctx.pReq->SetStatus(304);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CWebFileLibrary::MakeIndex(void)
{
	for( UINT n = 0; m_Files[n].m_pType; n++ ) {

		AddFile(m_Files + n);
		}

	return TRUE;
	}

void CWebFileLibrary::FindTimes(void)
{
	m_timeBoot = time(NULL);

	m_timeComp = CHttpTime::Parse("Day, " __DATE__ " " __TIME__ " EDT");
	}

// End of File
