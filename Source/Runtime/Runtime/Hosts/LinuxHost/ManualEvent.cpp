
#include "Intern.hpp"

#include "ManualEvent.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Manual Event Object
//

// Instantiator

static IUnknown * Create_ManualEvent(PCTXT pName)
{
	return New CManualEvent;
}

// Registration

global void Register_ManualEvent(void)
{
	piob->RegisterInstantiator("exec.event-m", Create_ManualEvent);
}

// Constructor

CManualEvent::CManualEvent(void)
{
	StdSetRef();
}

// IUnknown

HRESULT CManualEvent::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IWaitable);

	StdQueryInterface(IWaitable);

	StdQueryInterface(IEvent);

	return E_NOINTERFACE;
}

ULONG CManualEvent::AddRef(void)
{
	StdAddRef();
}

ULONG CManualEvent::Release(void)
{
	StdRelease();
}

// IWaitable

PVOID CManualEvent::GetWaitable(void)
{
	return NULL;
}

BOOL CManualEvent::Wait(UINT uWait)
{
	int c;

	while( !m_state ) {

		if( !WaitForChange(0, uWait, TRUE) ) {

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CManualEvent::HasRequest(void)
{
	return m_swait > 0;
}

// IEvent

void CManualEvent::Set(void)
{
	if( !AtomicCompAndSwap(&m_state, 0, 1) ) {

		WakeThreads(32000);
	}
}

void CManualEvent::Clear(void)
{
	m_state = 0;
}

// End of File
