
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LocalCertGen_HPP

#define	INCLUDE_LocalCertGen_HPP

//////////////////////////////////////////////////////////////////////////
//
// Local Certificate Generator
//

class CLocalCertGen : public ICertGen
{
	public:
		// Constructor
		CLocalCertGen(void);

		// Destructor
		~CLocalCertGen(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICertGen
		void   AddName(PCTXT pName);
		void   AddAddr(IPREF Addr );
		void   SetRootCert(PCBYTE pCert, UINT uCert);
		void   SetRootPriv(PCBYTE pPriv, UINT uPriv);
		void   SetRootPass(PCTXT pPass);
		bool   Generate(void);
		PCBYTE GetRootData(void);
		UINT   GetRootSize(void);
		PCBYTE GetCertData(void);
		UINT   GetCertSize(void);
		PCBYTE GetPrivData(void);
		UINT   GetPrivSize(void);
		PCTXT  GetPrivPass(void);

	protected:
		// Static Data
		static CString m_LastList;
		static BYTE    m_bLastRoot[4096];
		static BYTE    m_bLastCert[4096];
		static UINT    m_uLastRoot;
		static UINT    m_uLastCert;

		// Data Members
		ULONG		     m_uRefs;
		CString		     m_List;
		CStringArray         m_Names;
		CStringArray         m_Addrs;
		CByteArray	     m_CustRootCert;
		CByteArray	     m_CustRootPriv;
		CString		     m_CustRootPass;
		psPubKey_t           m_RootKey;
		psPubKey_t           m_CertKey;
		psX509Cert_t       * m_pRootCert;
		psCertConfig_t	     m_Config;
		PBYTE		     m_pReq;
		int32		     m_uReq;
		x509DNattributes_t * m_pDN;
		x509v3extensions_t * m_pV3;
		psPubKey_t         * m_pPub;
		PBYTE		     m_pCert;
		UINT		     m_uCert;
		PBYTE		     m_pPriv;
		UINT		     m_uPriv;

		// Implementation
		void  FindHostName(void);
		bool  LoadRootKey(void);
		bool  LoadCertKey(void);
		bool  LoadRootCert(void);
		bool  LoadCertConfig(void);
		bool  WriteCert(PCTXT pFile);
		bool  ReadCert(PCTXT pFile);
		PBYTE LoadBlob(PCBYTE pData, UINT uSize, BOOL fDecrypt);
		void  MakeFile(char *pName, PCBYTE pData, UINT uSize, BOOL fDecrypt);
	};

// End of File

#endif
