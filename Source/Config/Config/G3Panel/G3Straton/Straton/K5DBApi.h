// K5DB Server API

#if !defined(_K5DBAPI_H__INCLUDED_)
#define _K5DBAPI_H__INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef K5DBDLL
	/* Build the DLL */
	#define K5DB_APICALL __declspec(dllexport)
#else
	#define K5DB_APICALL __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////
// compatibility

int K5DB_APICALL K5DB_GetVersion		// get API version number
	(
	void
	);									// returns the API version number

/////////////////////////////////////////////////////////////////////////////
// internal - for tests

void K5DB_APICALL K5DB_X_HotSetup (HWND hwndParent,
                                   DWORD hClient, DWORD hProject);

/////////////////////////////////////////////////////////////////////////////
// error reports

#define K5DBEV_ERROR			-1		// error code is in lParam
#define K5DBEV_EXTDATACHANGED	-2		// external data of an object has been changed

#define K5DB_OK					0		// OK - other are errors: K5DBERR_...

#define K5DBERR_INTERNAL		1		// internal error - not documented
#define K5DBERR_SAMERENAME		2		// rename with same name !!

#define K5DBERR_CREATEPROJECT	3		// cant create project
#define K5DBERR_UNKNOWNCLIENT	4		// invalid client handle
#define K5DBERR_UNKNOWNPROJECT	5		// invalid project handle
#define K5DBERR_PROJECTSHARED	6		// project is shared - cant be deleted
#define K5DBERR_DELETEPROJECT	7		// cant delete project
#define K5DBERR_RENAMEPROJECT	8		// cant rename project
#define K5DBERR_COPYPROJECT		9		// cant rename project

#define K5DBERR_UNKNOWNPROGRAM	10		// unknown program
#define K5DBERR_DUPPROGNAME		11		// program name already used
#define K5DBERR_BADPROGNAME		12		// invalid program name
#define K5DBERR_DELPARENTPROG	13		// parent program cant be deleted
#define K5DBERR_PROGLOCKED		14		// program is locked
#define K5DBERR_BADPRGMOVE		15		// invalid program move in hierarchy
#define K5DBERR_TOOMANYPROGS	16		// too many programs
#define K5DBERR_BADPROGSECTION	17		// invalid section
#define K5DBERR_BADPROGLANGUAGE	18		// invalid language

#define K5DBERR_UNKNOWNGROUP	19		// unknown group
#define K5DBERR_CANTDELGROUP	20		// cant delete group (not an IO)
#define K5DBERR_GROUPLOCKED		21		// group is locked

#define K5DBERR_CANTDELIOVAR	22		// cant delete variable in an IO group
#define K5DBERR_UNKNOWNVAR		23		// unknown variable in group
#define K5DBERR_VARLOCKED		24		// variable is locked
#define K5DBERR_CANTRENIOVAR	25		// cant rename variable in an IO group
#define K5DBERR_DUPVARNAME		26		// variable name already used
#define K5DBERR_BADVARNAME		27		// variable name already used
#define K5DBERR_UNKNOWNTYPE		28		// unknow type
#define K5DBERR_BADIOTYPE		29		// invalid change of IO type
#define K5DBERR_IOVARDIM		30		// IO cant have no dim
#define K5DBERR_TYPEDIM			31		// type cant have a dim
#define K5DBERR_BADDIM			32		// invalid dim
#define K5DBERR_NOSTRINGLEN		33		// type cant have a string length
#define K5DBERR_BADSTRINGLEN	34		// invalid string length
#define K5DBERR_CANTCHANGEATTR	35		// invalid change of attribute
#define K5DBERR_CANTCHANGERO	36		// RO attribute cant be changed
#define K5DBERR_CANTALIASMEMVAR 37		// aliases are only for IOs
#define K5DBERR_CANTINITFB		38		// no init value for FB instances
#define K5DBERR_CANTINITARRAY	39		// no init value for arrays
#define K5DBERR_BADINITVALUE	40		// invalid init value
#define K5DBERR_NOFBIOINIT		41		// FB In/Out cannot have an initial value
#define K5DBERR_FBIOVARDIM		42		// FB in/out cannot have dimension

#define K5DBERR_UNKNOWNOBJECT	43		// unknown object (in project)

#define K5DBERR_BADCOMMLANGUAGE 44		// invalid text language identifier
#define K5DBERR_NOVARMLINECOMM	45		// no multiline comm for variables
#define K5DBERR_NORETAININST	46		// no instances in RETAIN group

#define K5DBERR_EXPCANTCREATE   47      // cant create export file
#define K5DBERR_IMPCANTREAD     48      // cant open import file
#define K5DBERR_IMPBADFILE      49      // invalid import file
#define K5DBERR_IMPCREATEPROG   50      // cant create import program
#define K5DBERR_EXPHIDEUDFB     51      // cant hide exported source if not UDFB
#define K5DBERR_EXPHIDENOCODE   52      // cant hide exported source if UDFB not compiled

#define K5DBERR_HOTENABLED      53      // invalid operation when On Line change is enabled
#define K5DBERR_XV              54      // invalid use of "external" attributes

#define K5DBERR_DLGASKCREATEVAR 55      // DlgAskCreateVar
#define K5DBERR_DLGTYPE         56      // DlgType
#define K5DBERR_DLGYES          57      // DlgYes
#define K5DBERR_DLGNO           58      // DlgNo
#define K5DBERR_DLGCANCEL       59      // DlgCancel
#define K5DBERR_DLGCREATEVARKO  60      // DlgCreateVarKO

#define K5DBERR_CANTDELFOLDER           61 // del non empty folder
#define K5DBERR_CANTRENFOLDER           62 // folder name used
#define K5DBERR_CANTMOVECHILDTOFOLDER   63 // cant move child program to folder
#define K5DBERR_CANTMOVETOPFOLDER       64 // cant move root folder
#define K5DBERR_CANTMOVEFOLDERSAME      65 // cant move to the same folder
#define K5DBERR_CANTMOVEFOLDERRECURSE   66 // cant move to chil folder

#define K5DBERR_FILELOCKED      67      // file is locked
#define K5DBERR_BADFILEEXT      68      // invalid file extension
#define K5DBERR_DUPFILENAME     69      // file name already used
#define K5DBERR_BADFILENAME     70      // invalid file name

#define K5DBERR_CRYPTED         71      // program is protected

#define K5DBERR_DLGASKVAR       72      // DlgAskVar
#define K5DBERR_DLGASKCREATE    73      // DlgAskCreate
#define K5DBERR_DLGASKRENAME    74      // DlgAskRename
#define K5DBERR_DLGWHERE        75      // DlgRange

#define K5DBERR_BADENUM         76      // invalid enumerated value(s)
#define K5DBERR_DUPTYPENAME     77      // type name already used

#define K5DBERR_DLGVARS         78      // variables
#define K5DBERR_DLGKWS          79      // keywords and functions
#define K5DBERR_DLGNOGROUP      80      // (no user group)
#define K5DBERR_DLGALL          81      // (all)

#define K5DBERR_DLGLOCALONLY    82      // local variables only
#define K5DBERR_DLGNOINST       83      // no FB instance

#define K5DBERR_GLOBAL          84      // "GLOBAL"
#define K5DBERR_RETAIN          85      // "RETAIN"
#define K5DBERR_PRDISPRG        86      // prompt to disable instead of delete when hot enabled

#define K5DBERR_MTNAMECLASH     87      // variable name already used for variables of other tasks
#define K5DBERR_MTUNLINK        88      // the variable will be removed in other tasks. Continue?
#define K5DBERR_MTCFB           89      // instances of C function blocks cannot be shared
#define K5DBERR_DLGOTHERPRJ     90      // Other project...

#define K5DBERR_DLGSCLOCK       91      // DlgSCLocked
#define K5DBERR_DESC            92      // Desc
#define K5DBERR_HASLOCKS        93      // HasLocks

#define K5DBTEXT_EVENTS         100     // index of the first event text

/////////////////////////////////////////////////////////////////////////////
// object Identification

#define K5DBID_USER		256	// first available id for declared objects

// types of objects

#define K5DBOBJ_CLIENT	    1	// client
#define K5DBOBJ_PROJECT	    2	// open project
#define K5DBOBJ_PROGRAM	    3	// program
#define K5DBOBJ_GROUP	    4	// group
#define K5DBOBJ_TYPE	    5	// type (may refer to a UDFB)
#define K5DBOBJ_VAR		    6	// variable
#define K5DBOBJ_UNKNOWN     7   // external object (not stored)
#define K5DBOBJ_FOLDER      8   // program folders
#define K5DBOBJ_FILE        9   // program folders
#define K5DBOBJ_BINDING     10  // binding configuration
#define K5DBOBJ_BUS         11  // all fieldbus configurations
#define K5DBOBJ_GLOBALDEF   12  // global definitions
#define K5DBOBJ_HMI         13  // embedded HMI
#define K5DBOBJ_WKSP        14  // workspace organization

// methods

DWORD K5DB_APICALL K5DB_GetKindOfObject (	// get the type of an object
	DWORD dwID								// ID of the specified object
	); // RETURN: type of object for this ID or 0 if invalid

BOOL K5DB_IsStructure (DWORD hClient, DWORD hProject, DWORD dwID);

DWORD K5DB_APICALL K5DB_IsObjectLocked (	// check if an object is locked
	DWORD dwID								// ID of the specified object
	); // RETURN: TRUE if the object is locked

DWORD K5DB_APICALL K5DB_CheckSymbol (		// check if symbol is correct
	LPCSTR szName							// symbol
	); // return OK or error (no detailed error)

LPCSTR K5DB_APICALL K5DB_GetGhostName (	// get name of a deleted object
    DWORD hObject                       // object ID
	); // RETURN: ghost name or NULL

/////////////////////////////////////////////////////////////////////////////
// reserved to the compiler

DWORD K5DB_APICALL K5DB_GetCmpType (DWORD hType);
DWORD K5DB_APICALL K5DB_GetGlobalFilePath (DWORD hClient, DWORD hProject,
											LPCSTR szSuffix, LPSTR szPath);
DWORD K5DB_APICALL K5DB_GetProjectPath (DWORD hClient, DWORD hProject,
										LPSTR szPath);

DWORD K5DB_APICALL K5DB_GetEqvPath (DWORD hClient, DWORD hProject,
                                    DWORD bCommon, DWORD hProgram, LPSTR szPath);

DWORD K5DB_APICALL K5DB_GetHistoryPath (DWORD hClient, DWORD hProject,
                                        LPSTR szPath);

DWORD K5DB_APICALL K5DB_IsEqv (         // test if a symbol is a #define
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program (NULL: global only)
    LPCSTR szName                       // symbol
    );  // RETURN: TRUE if defined

/////////////////////////////////////////////////////////////////////////////
// constant values

DWORD K5DB_APICALL K5DB_CheckValue (    // check value - auto fix details
    LPCSTR szTypeName,                  // type name (basic types only!)
    DWORD  dwStringLength,              // max length for STRING type
    LPSTR  szValue,                     // pointer to the value (IN/OUT)
    DWORD  dwBufSize                    // size of the szValue buffer
    ); // RETURN: OK or error

/////////////////////////////////////////////////////////////////////////////
// reload the registry - there should be no project open!

DWORD K5DB_APICALL K5DB_ReloadReg (void); // return= OK or error

/////////////////////////////////////////////////////////////////////////////
// enable spontaneous events - to be called regularly

void K5DB_APICALL K5DB_Idle (void);

/////////////////////////////////////////////////////////////////////////////
// connection

#define K5DBCNX_SELFNOTIF	0x00000001	// flag: self-notifications wished
#define K5DBCNX_Q	        0x00000002	// don't use that
#define K5DBCNX_W6MAIN      0x00000004	// don't use that

// methods

DWORD K5DB_APICALL K5DB_Connect (		// connect to the K5 database
	HWND hwndCallback,					// handle of the parent window for callbacks
	DWORD msgCallback,					// wished callback message
	DWORD dwFlags,						// 1 if self-notifications wished
	DWORD dwData,						// reserved for extension
	LPCSTR szClientName					// client name
	); // RETURN: client ID or NULL if error

DWORD K5DB_APICALL K5DB_SetCallback (	// change client callback
	DWORD hClient,						// client ID
	HWND hwndCallback,					// handle of the parent window for callbacks
	DWORD msgCallback					// wished callback message
	); // RETURN: ok or error

void K5DB_APICALL K5DB_Disconnect (		// disconnect from the K5 database
	DWORD hClient						// client ID
	);

LPCSTR K5DB_APICALL K5DB_GetClientName ( // get the name of a registered client
	DWORD hClient						// client ID
	); // RETURN: name of the client

BOOL K5DB_APICALL K5DB_PopQ (
    DWORD hClient,
    DWORD *pdwEvent,
    DWORD *pdwArg,
    DWORD *pdwProject);

/////////////////////////////////////////////////////////////////////////////
// DB extensions - not documented

DWORD K5DB_APICALL K5DB_OpenXS (HWND hwndParent, LPCSTR szProjectPath, DWORD dwFlags);
void K5DB_APICALL K5DB_CloseXS (DWORD hXS);

/////////////////////////////////////////////////////////////////////////////
// error messages

// methods

LPCSTR K5DB_APICALL K5DB_GetMessage (	// get text description of an error
	DWORD dwRC							// K5 error report
	); // RETURN: description text for the specified error

/////////////////////////////////////////////////////////////////////////////
// events

// flags

#define K5EVENT_POORINTERESET   0x0001

// methods

DWORD K5DB_APICALL K5DB_GetEventListSize ( // get number of known events
    void
    );

DWORD K5DB_APICALL K5DB_GetEventList (  // get list of possible events
    DWORD *pdwEvents                    // array of events to be filles
    );                                  // number of events

LPCSTR K5DB_APICALL K5DB_GetEventDesc ( // get description of an event
    DWORD dwEvent,                      // event number
    DWORD *pdwFlags                     // return: event flags
    );

LPCSTR K5DB_APICALL K5DB_GetEventName ( // get internal name of an event
    DWORD dwEvent                       // event number
    );

DWORD K5DB_APICALL K5DB_DispatchEvent ( // generates an event
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD dwEvent,                      // event
    DWORD dwArg                         // argument (generally an ID)
	);                                  // return: OK or error

DWORD K5DB_APICALL K5DB_EnableEvents (  // enable/disable project events
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
    DWORD dwEnable                      // enabling flag
	);                                  // value of flag before call

DWORD K5DB_APICALL K5DB_RegisterEvent ( // register custom event
    LPCSTR szEventName,                 // unique event name
    LPCSTR szEventDesc                  // description (for history)
    ); // returns the value of the registered event or 0 if fail

// registered external events

#define K5DBEV_BUILD    	0x80000001	// application built
#define K5DBEV_BUILDOPT    	0x80000002	// build options changed
#define K5DBEV_DOWNLOAD    	0x80000003	// application downloaded
#define K5DBEV_LOADCHANGE 	0x80000004	// application changes downloaded
#define K5DBEV_HOTCHANGE    0x80000005	// on line change
#define K5DBEV_EQVCOM    	0x80000006	// common defines changed
#define K5DBEV_EQVGLOBAL 	0x80000007	// global defines changed
#define K5DBEV_HISTORYON	0x80000008	// history activated
#define K5DBEV_HISTORYOFF	0x80000009	// history deactivated
#define K5DBEV_WATCHFILES   0x8000000a  // watch file(s) have changed - obsolete
#define K5DBEV_CONFIGCHANGE 0x8000000b  // active configuration has changed
#define K5DBEV_LIBSCHANGE   0x8000000c  // libraries have been updated
#define K5DBEV_BEFORELOOP   0x8000000d  // internal use
#define K5DBEV_AFTERLOOP    0x8000000e  // internal use
#define K5DBEV_ERCRELOADED  0x0000000f  // ERC reloaded
#define K5DBEV_SUBGROUPS    0x00000010  // global sub-groups changed

/////////////////////////////////////////////////////////////////////////////
// projects

// methods

DWORD K5DB_APICALL K5DB_OpenProject (	// open an existing project
	DWORD hClient,						// client ID
	LPCSTR szProjectPath				// project pathname (no file name)
	);	// RETURN: handle of a project - NULL if not found

void K5DB_APICALL K5DB_CloseProject (	// close the project and unlocks it
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	);

DWORD K5DB_APICALL K5DB_CreateProject (	// creates and opens and a new project
	DWORD hClient,						// client ID
	LPCSTR szProjectPath				// project pathname (no file name)
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_DeleteProject (	// delete and close a project
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_CopyProject (	// copy a project to another
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szDstPath					// destination project pathname
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_CopyProjectDCL ( // copy all project declarations
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szDstPath					// destination project pathname
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_RenameProject ( // rename a project on the disk
	DWORD hClient,						// client ID
	DWORD hProject,						// project ID
	LPCSTR szNewPath					// new project pathname
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_SaveProject (	// flush project data to disk
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_GetProjectDateStamp ( // get date of last modification
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	); // RETURN: 0 or date stamp

DWORD K5DB_APICALL K5DB_SetProjectModified ( // mark project as modified
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_ResetModifMark ( // unmark project as modified
	DWORD hClient,						// client ID
	DWORD hProject						// project ID
	); // RETURN: ok or error

// events

#define K5DBEV_PRJRENAMED		0x00010001	// project has been renamed / moved on disk
#define K5DBEV_PRJRELOADED		0x00010002	// project has been reloaded - BIG CHANGES!

/////////////////////////////////////////////////////////////////////////////
// programs

// limits

#define K5DBMAX_PROGRAM			32767       // max nb of programs in a project

// program sections

#define K5DBSECTION_BEGIN		0x00000001	// BEGIN (non sfc)
#define K5DBSECTION_SFC			0x00000002	// SFC: main only
#define K5DBSECTION_END			0x00000004	// END (non sfc)
#define K5DBSECTION_CHILD		0x00000008	// SFC children (all)
#define K5DBSECTION_UDFB		0x00000010	// UDFBs
#define K5DBSECTION_MAIN		0x00000007	// all main programs - not UDFBs
#define K5DBSECTION_PROG		0x0000000f	// all programs
#define K5DBSECTION_ANY			0x000000ff	// all programs and UDFBs

#define K5DBSECTION_UDFBTREE	0x00008000	// reserved for the compiler

// programming languages

#define K5DBLANGUAGE_SFC		0x00000001	// SFC
#define K5DBLANGUAGE_ST			0x00000002	// ST
#define K5DBLANGUAGE_FBD		0x00000004	// FBD
#define K5DBLANGUAGE_LD			0x00000008	// LD
#define K5DBLANGUAGE_IL			0x00000010	// IL
#define K5DBLANGUAGE_FFSFC      0x00000020	// free form SFC
#define K5DBLANGUAGE_SAMA       0x00000040  // SAMA
#define K5DBLANGUAGE_MULTI      0x00000080  // new free form LD/FBD
#define K5DBLANGUAGE_ANY		0x0000007f	// any language

#define K5DBLANGUAGE_C          0x00000100  // "C"
#define K5DBLANGUAGE_CPP        0x00000200  // C++
#define K5DBLANGUAGE_FFL        0x00000400  // free form Ladder

#define K5DBLANGUAGE_ISSFC(l) (((l)==K5DBLANGUAGE_SFC)||((l)==K5DBLANGUAGE_FFSFC))

// sort methods

#define K5DBPROG_SORTBYNAME     1           // sort by name

// move commands

#define K5DBMOVEPROGRAM_UP		1			// up in the same section
#define K5DBMOVEPROGRAM_DOWN	2			// down in the same section
#define K5DBMOVEPROGRAM_BEGIN	3			// to BEGIN section
#define K5DBMOVEPROGRAM_END		4			// to END section
#define K5DBMOVEPROGRAM_SFCMAIN	5			// to top of SFC section
#define K5DBMOVEPROGRAM_CHILDOF	6			// under the specified SFC parent program
#define K5DBMOVEPROGRAM_BEFORE	7			// before the specified program
#define K5DBMOVEPROGRAM_AFTER	8			// after the specified program

// methods

DWORD K5DB_APICALL K5DB_GetNbProgram (	// get nb of program in section(s)
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD dwSection						// wished section(s)
	); // RETURN: number of programs

DWORD K5DB_APICALL K5DB_GetPrograms (	// get IDs of programs in section(s)
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD dwSection,					// wished section(s)
	DWORD *phPrograms					// buffer where to store IDs
	); // RETURN: number of programs

DWORD K5DB_APICALL K5DB_GetNbChildren (	// get number of children of a parent SFC
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hParent						// ID of the parent program
	); // RETURN: number of programs

DWORD K5DB_APICALL K5DB_GetChildren (	// get IDs of children of a parent SFC
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hParent,						// ID of the parent program
	DWORD *phChildren					// buffer where to store IDs
	); // RETURN: number of programs

DWORD K5DB_APICALL K5DB_GetSortedChildren (	// get IDs of children of a parent SFC
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hParent,						// ID of the parent program
	DWORD *phChildren,					// buffer where to store IDs
    DWORD dwMethod                      // sort method
	); // RETURN: number of programs

DWORD K5DB_APICALL K5DB_FindProgram (	// find a program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	LPCSTR szProgName					// program name
	); // RETURN: handle of the found program or NULL

LPCSTR K5DB_APICALL K5DB_GetProgramDesc ( // Get name and description of a program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	DWORD *pdwLanguage,					// language (if not NULL)
	DWORD *pdwSection,					// section (if not NULL)
	DWORD *phParent						// ID of parent SFC (if not NULL)
	); // RETURN: program name of NULL if error

DWORD K5DB_APICALL K5DB_CanCreateProgram (	// test creation of a new program - does not create
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD dwLanguage,					// language
	DWORD dwSection,					// wished section
	DWORD hParent,						// parent SFC program (if child)
	LPCSTR szProgName					// program name
	); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_CreateProgram (	// create a new program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD dwLanguage,					// language
	DWORD dwSection,					// wished section
	DWORD hParent,						// parent SFC program (if child)
	LPCSTR szProgName					// program name
	); // RETURN: handle of the new program

DWORD K5DB_APICALL K5DB_CopyProgram (	// copy a program to another (can be a new one)
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the source program
	DWORD dwSection,					// wished section (if new)
	DWORD hParent,						// parent SFC program (if child and new)
	LPCSTR szProgName					// program name
	); // RETURN: handle of the destination program

DWORD K5DB_APICALL K5DB_RenameProgram (	// rename a program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	LPCSTR szNewName					// new program name
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_MoveProgram (	// move a program in the hierarchy
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	DWORD dwMove,						// move order (K5MOVEPROGRAM_...)
	DWORD hParent						// move location (can be NULL)
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_DeleteProgram (	// delete a program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_LockProgram (	// lock a program for editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_SaveProgramChanges ( // changes a program while editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_RegisterProgramChanges ( // internal use
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_UnlockProgram (	// unlock a program after editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetProgramOwner ( // identifies the client who locked a program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: name of the owner client

DWORD K5DB_APICALL K5DB_GetProgramPath (	// get pathname of a source file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	LPSTR szPath						// buffer where to copy the full pathname
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_GetProgramSchedule ( // get program scheduling
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	DWORD *pdwPeriod,                   // period (number of cycles)
    DWORD *pdwOffset                    // offset (within period)
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_SetProgramSchedule ( // set program scheduling
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	DWORD dwPeriod,                     // period (number of cycles)
    DWORD dwOffset                      // offset (within period)
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_GetProgramOnCallFlag ( // get "OnCall" exec mode
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram						// ID of the program
	); // RETURN: TRUE if program has "OnCall" exec mode

DWORD K5DB_APICALL K5DB_SetProgramOnCallFlag ( // set "OnCall" exec mode
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	DWORD dwOnCall                      // TRUE if OnCall mode wished
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_OmitStructuresWhileSaving ( // dont save structures
	DWORD hClient,						// client ID
	DWORD hProject						// handle of the project
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_ChangeProgramLanguage ( // NEVER CALL THAT
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hProgram,						// ID of the program
	DWORD dwLanguage
	); // RETURN: ok or error

// events

#define K5DBEV_PRGNEW			0x00020001	// new program has been created
#define K5DBEV_PRGDUPLICATED	0x00020002	// new program has been created by copy
#define K5DBEV_PRGRENAMED		0x00020003	// program has been renamed
#define K5DBEV_PRGDELETED		0x00020004	// program has been deleted
#define K5DBEV_PRGMOVED			0x00020005	// program has been moved
#define K5DBEV_PRGCOPIED		0x00020006	// program has been copied
#define K5DBEV_PRGLOCKED		0x00020007	// program has been locked for editing
#define K5DBEV_PRGCHANGED		0x00020008	// edited program has been changed and saved
#define K5DBEV_PRGUNLOCKED		0x00020009	// program has been unlocked after editing
#define K5DBEV_PRGVARCHANGED	0x0002000a	// local variables have changed
#define K5DBEV_PRGPLEASEMOVE	0x0002000b	// ask to move the program in its DB folder

/////////////////////////////////////////////////////////////////////////////
// types

// flags

#define K5DBTYPE_INVALID		0x00001000	// invalid data type
#define K5DBTYPE_BASIC			0x00000001	// simple data type (predefined)
#define K5DBTYPE_STRING			0x00000002	// string length is required
#define K5DBTYPE_IO				0x00000004	// type can be used for an IO
#define K5DBTYPE_CSTRUCT        0x00000008	// structure defined in libray
#define K5DBTYPE_STDFB			0x00000010	// that's a standard or "C" function block
#define K5DBTYPE_CFB			0x00000020	// that's a standard or "C" function block
#define K5DBTYPE_UDFB			0x00000040	// thats a UDFB
#define K5DBTYPE_FB				0x000000f0	// that's a function block or a UDFB
#define K5DBTYPE_ENUM           0x00000100	// enumeratade data type (user defined)
#define K5DBTYPE_BITFIELD       0x00000200	// bit field integer data type (user defined)

#define K5DBTYPE_SINGLE         (K5DBTYPE_BASIC|K5DBTYPE_ENUM|K5DBTYPE_BITFIELD)

// methods

DWORD K5DB_APICALL K5DB_GetNbType (		// get nb of types (basic or FBs or UDFBs)
	DWORD hClient,						// client ID
	DWORD hProject						// handle of the project
	); // RETURN: number of types

DWORD K5DB_APICALL K5DB_GetTypes (		// get the list of type IDs for types
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD *phTypes						// buffer where to store IDs
	); // RETURN: number of types

DWORD K5DB_APICALL K5DB_FindType (		// find a type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	LPCSTR szTypeName					// type name
	); // RETURN: handle of the found type or NULL

LPCSTR K5DB_APICALL K5DB_GetTypeDesc (	// get the name of a type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hType,						// ID of the type
	DWORD *pdwFlags						// attributes of the type
	); // RETURN: type name

DWORD K5DB_APICALL K5DB_GetTypeUDFB (	// get the handle of the UDFB identified by a type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hType							// ID of the type
	); // RETURN: handle of the corresponding UDFB or 0

DWORD K5DB_APICALL K5DB_GetNbTypeParam ( // get nb of parameters of a type (when FB)
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hType							// ID of the type
	); // RETURN: number of parameters

DWORD K5DB_APICALL K5DB_GetTypeParams (	// get IDs of type parameters (when FB)
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hType,						// ID of the type
	DWORD *phParams						// buffer where to store IDs
	); // RETURN: number of parameters

DWORD K5DB_APICALL K5DB_GetUDFBNbIO (	// get nb of IOs for a UDFB
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hType,						// ID of the type associated to the UDFB
	DWORD *pdwNbIn,                     // nb of input parameters
	DWORD *pdwNbOut                     // nb of output parameters
	); // RETURN: total nb of IOs (nbIn + nbOut)

// User defined data types (Enums or bitfields)

DWORD K5DB_APICALL K5DB_RenameType (    // rename a user defined data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hType,                        // handle of the type
    LPCSTR szNewName                    // new name
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_DeleteType (    // delete a user defined data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hType                         // handle of the type
    ); // RETURN: OK or error

// ENUM data types

DWORD K5DB_APICALL K5DB_CreateEnumType ( // create a new enumerated data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    LPCSTR szName,                      // type name
    LPCSTR szValues                     // enum values (sep = ',')
    ); // RETURN: handle of new type

LPCSTR K5DB_APICALL K5DB_GetEnumTypeValues ( // get values of an enumerated data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hType                         // handle of the type
    ); // RETURN: enum values (sep = ',')

DWORD K5DB_APICALL K5DB_SetEnumTypeValues ( // change values of an enumerated data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hType,                        // handle of the type
    LPCSTR szValues                     // enum values (sep = ',')
    ); // RETURN: OK or error

// BIT FIELDS data types

DWORD K5DB_APICALL K5DB_CreateBitFieldType ( // create a new bitfield data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hBaseType,                    // handle of the template integer data type
    LPCSTR szName,                      // type name
    LPCSTR szBits                       // bit names (sep = ',')
    ); // RETURN: handle of new type

LPCSTR K5DB_APICALL K5DB_GetBitFieldType ( // get description of a bitfield data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hType,                        // handle of the type
	DWORD *phBaseType                   // handle of the template integer data type
    ); // RETURN: bit names (sep = ',')

DWORD K5DB_APICALL K5DB_SetBitFieldType ( // change values of an enumerated data type
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hType,                        // handle of the type
	DWORD hBaseType,                    // handle of the template integer data type
    LPCSTR szBits                       // bit names (sep = ',')
    ); // RETURN: OK or error

// events

#define K5DBEV_TYPENEW			0x00050001	// new type has been created
#define K5DBEV_TYPERENAMED		0x00050003	// type has been renamed
#define K5DBEV_TYPEDELETED		0x00050004	// type has been deleted
#define K5DBEV_TYPECHANGED		0x00050007	// type has been changed

////////////////////////////////////////////////////////////////////////////////////////////////
// Variable groups

#define K5DBGROUPNAME_GLOBAL	"(Global)"
#define K5DBGROUPNAME_RETAIN	"(Retain)"

// limits

#define K5DBMAX_SLOT			256		// max nb of slots
#define K5DBMAX_SUBSLOT			16		// max nb of subslots
#define K5DBMAX_CHANNEL			256		// max nb of channels in group

// group styles

#define K5DBGROUP_GLOBAL		1		// global variables
#define K5DBGROUP_RETAIN		2		// global RETAIN variables
#define K5DBGROUP_INPUT			3		// input IO board
#define K5DBGROUP_OUTPUT		4		// output IO board
#define K5DBGROUP_CPXINPUT		5		// input IO board within a complex device
#define K5DBGROUP_CPXOUTPUT		6		// output IO board within a complex device
#define K5DBGROUP_LOCAL			7		// variables local to a program		
#define K5DBGROUP_UDFB			8		// private data and parameters of a UDFB

#define K5DBGROUP_SORTDEFAULT   0       // natural order
#define K5DBGROUP_SORTIOLAST    1       // IOs are the last ones
#define K5DBGROUP_SORTNATURAL   2       // natural order - sorted by class
#define K5DBGROUP_SORTLOCAL     0x80000000 // 1 local first - OR combined with local group handle

// methods

DWORD K5DB_APICALL K5DB_GetNbGroup (	// get nb of variable groups
	DWORD hClient,						// client ID
	DWORD hProject						// handle of the project
	); // RETURN: number of groups

DWORD K5DB_APICALL K5DB_GetGroups (		// get the list of group IDs
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD *phGroups						// buffer where to store IDs
	); // RETURN: number of groups

DWORD K5DB_APICALL K5DB_GetSortedGroups ( // get the list of group IDs - sorted
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD *phGroups,					// buffer where to store IDs
    DWORD dwSort                        // sorting method
	); // RETURN: number of groups

DWORD K5DB_APICALL K5DB_FindGroup (		// find a group
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	LPCSTR szGroupName					// group name
	); // RETURN: handle of the found group or NULL

LPCSTR K5DB_APICALL K5DB_GetGroupDesc (	// get description of a group
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup,						// group ID
	DWORD *pdwGroupStyle,				// type of group (global, IO...)
	DWORD *phIOType,					// type of connected IOs
	DWORD *pdwSlot,						// slot number for an IO board
	DWORD *pdwSubSlot,					// sub slot number for a part of a complex IO
	DWORD *pdwNbVar						// number of variables in the group
	); // RETURN: name of group - "\0" for globals

DWORD K5DB_APICALL K5DB_CreateIOGroup (	// create a new group for an IO board
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD dwGroupStyle,					// type of group (in, out...)
	DWORD hIOType,						// type of connected IOs
	DWORD dwSlot,						// slot number for an IO board
	DWORD dwSubSlot,					// sub slot number for a part of complex IO
	DWORD dwNbVar						// number of variables in the group
	); // RETURN: handle of the new group

DWORD K5DB_APICALL K5DB_DeleteIOGroup (	// delete a group describing an IO board
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// group ID
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_LockGroup (		// lock a group for editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// ID of the group
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_UnlockGroup (	// unlock a group after editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// ID of the group
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetGroupOwner ( // identifies the client who locked a group
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// ID of the group
	); // RETURN: name of the owner client

DWORD K5DB_APICALL K5DB_OmitGroupSaving ( // ignore the group when saving
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// ID of the group
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_LockGroupEx (	// lock a group and its vars for editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// ID of the group
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_UnlockGroupEx (	// unlock a group and its vars after editing
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD hGroup						// ID of the group
	); // RETURN: ok or error

// events

#define K5DBEV_GROUPNEW			0x00040001	// new group has been created
#define K5DBEV_GROUPRENAMED		0x00040002	// group has been renamed
#define K5DBEV_GROUPDELETED		0x00040003	// group has been deleted
#define K5DBEV_GROUPLOCKED		0x00040004	// group has been locked
#define K5DBEV_GROUPCHANGED		0x00040005	// group has been changed (itself - not vars inside)
#define K5DBEV_GROUPUNLOCKED	0x00040006	// group has been unlocked
#define K5DBEV_GROUPMOVED		0x00040007	// group has been moved

#define K5DBEV_GROUPOWNED       0x00040008	// group + its vars has been locked
#define K5DBEV_GROUPRELEASED    0x00040009	// group + its vars has been locked

////////////////////////////////////////////////////////////////////////////////////////////////
// Variables

// limits

#define K5DBMAX_DIM				65534		// max nb of items in an array
#define K5DBMAX_STRING			255			// max nb of chars in string

// flags

#define K5DBVAR_READONLY		0x00000001	// read only attribute (always TRUE for inputs)
#define K5DBVAR_INPUT			0x00000002	// physical input
#define K5DBVAR_OUTPUT			0x00000004	// physical output
#define K5DBVAR_FBINPUT			0x00000008	// input parameter of a FB or UDFB
#define K5DBVAR_FBOUTPUT		0x00000010	// output parameter of a FB or UDFB
#define K5DBVAR_EXTERN          0x00000020  // shared external variable
#define K5DBVAR_INOUT           0x00000040  // FB input is INOUT
#define K5DBVAR_ADDED   		0x00000100	// added for on line change
#define K5DBVAR_DELETED         0x00000200	// deleted for on line change
#define K5DBVAR_LOCALRETAIN     0x00000400	// local RETAIN flag

#define K5DBVAR_FINDEXACT   	0x00000001	// find exact name
#define K5DBVAR_FINDPVID       	0x00000002	// find by PVID

#define K5DBVAR_SORTBYNAME      1           // sort by variable name
#define K5DBVAR_SORTBYTYPE      2           // sort by data type
#define K5DBVAR_SORTBYATTR      3           // sort by attribute
#define K5DBVAR_SORTBYPROPS     4           // sort by properties (profile & embedded)

#define K5DBMOVEPARAM_UP        1           // move command: FB parameter UP
#define K5DBMOVEPARAM_DOWN      2           // move command: FB parameter DOWN
#define K5DBMOVEVAR_TOEND       3           // move command: move to end of group (vars only)

// methods

DWORD K5DB_APICALL K5DB_GetNbVar (			// get nb of variables in a group
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup							// ID of the parent group
	); // RETURN: number of variables

DWORD K5DB_APICALL K5DB_GetVars (			// get the list of variable IDs for a group
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD *phVars							// buffer where to store IDs
	); // RETURN: number of variables

DWORD K5DB_APICALL K5DB_GetSortedVars (		// get the list of variable IDs for a group
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
    DWORD dwMethod,                         // sorting method
	DWORD *phVars							// buffer where to store IDs
	); // RETURN: number of variables

DWORD K5DB_APICALL K5DB_FindVarInGroup (	// find a variable within a group
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	LPCSTR szVarName						// variable name
	); // RETURN: handle of the found variable or NULL

DWORD K5DB_APICALL K5DB_CreateVar (			// create a new variable in a group
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD dwPosID,							// ID of the position (insert before / -1: at the end)
	DWORD hType,							// handle of the data type or UDFB
	DWORD dwDim,							// array dimension or 0 if not an array
	DWORD dwStringLength,					// maximum length for a string
	DWORD dwFlags,							// attributes
	LPCSTR szVarName						// symbol
	); // RETURN: handle of the new variable

DWORD K5DB_APICALL K5DB_DeleteVar (			// delete a variable (cant be an IO)
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_RenameVar (			// rename a variable (cant be an IO)
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	LPCSTR szNewName						// new name of the variable
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_CanRenameVar (		// check if a variable can be renamed
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	LPCSTR szNewName						// new name of the variable
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetVarPrevName (   // get old name of a renamed variable
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
	DWORD hVar                              // ID of the variable
	); // RETURN: prev name or NULL

DWORD K5DB_APICALL K5DB_MoveVar (			// move a variable within a group
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	DWORD dwPosID							// move command
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_GetVarGroup (		// get the parent group of a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hVar								// ID of the variable
	); // RETURN: handle of the group where var is defined

LPCSTR K5DB_APICALL K5DB_GetVarDesc (		// get the full description of a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	DWORD *phType,							// handle of the data type or UDFB
	DWORD *pdwDim,							// array dimension or 0 if not an array
	DWORD *pdwStringLength,					// maximum length for a string
	DWORD *pdwFlags							// attributes
	); // RETURN: symbol of the variable

DWORD K5DB_APICALL K5DB_SetVarDesc (		// set the full description of a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	DWORD hType,							// handle of the data type or UDFB
	DWORD dwDim,							// array dimension or 0 if not an array
	DWORD dwStringLength,					// maximum length for a string
	DWORD dwFlags							// attributes
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_CanSetVarDesc (		// test if description of a variable is OK
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	DWORD hType,							// handle of the data type or UDFB
	DWORD dwDim,							// array dimension or 0 if not an array
	DWORD dwStringLength,					// maximum length for a string
	DWORD dwFlags							// attributes
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_SetVarInitValue (	// assign the init value of a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	LPCSTR szValue							// init value (string)
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_CheckVarInitValue (	// check an init value for a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	LPCSTR szValue							// init value (string)
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetVarInitValue (	// get the init value of a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	DWORD *pdwValid							// zero if init value is invalid
	); // RETURN: init value (string) or NULL if not def

DWORD K5DB_APICALL K5DB_SetVarIOAlias (		// assign the alias of an IO variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	LPCSTR szAlias							// alias
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_CanSetVarIOAlias (	// test alias for an IO variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
	LPCSTR szAlias							// alias
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetVarIOAlias (	// get the alias of an IO variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: alias or NULL if not def

DWORD K5DB_APICALL K5DB_LockVar (			// lock a variable for editing
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_LockVarForever (	// permanently lock a variable for editing
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: ok or error

BOOL K5DB_APICALL K5DB_WantVarEdit (        // check for variable editing
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: true if modification allowed

DWORD K5DB_APICALL K5DB_UnlockVar (			// unlock a locked variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetVarOwner (		// identifies the client who locked a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hVar								// ID of the group
	); // RETURN: name of the owner client

DWORD K5DB_APICALL K5DB_FindVar (           // search for a variable symbol
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    LPCSTR szSymbol,                        // searched text
	DWORD hStartPos,    					// handle of selected var (where to start)
    DWORD hLocalGroup,                      // group for local search or NULL
    DWORD dwFlags                           // search options
    );                                      // found var ID or NULL

DWORD K5DB_APICALL K5DB_FindProgramVar (	// find a variable: global or local to a program
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hProgram,							// handle of the parent program
	LPCSTR szVarName,						// variable name
	DWORD *phGroup,							// ID of the found var group
	DWORD *pbLocal							// TRUE if local to the specified program
	); // RETURN: handle of the found variable or NULL

DWORD K5DB_APICALL K5DB_SetVarOmitSaving (  // set "save" flag for a variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
    DWORD dwOmit                            // TRUE: dont save the variable
	);

DWORD K5DB_APICALL K5DB_GetVarDimensions (  // get details of multi dimensions
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
    DWORD dwBufSize,                        // size of the buffer
    DWORD *pdwDims                          // size of each dimension
	); // RETURN: total raw dimension

DWORD K5DB_APICALL K5DB_SetVarDimensions (  // set details of multi dimensions
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar,								// ID of the variable
    DWORD dwBufSize,                        // size of the buffer
    DWORD *pdwDims                          // size of each dimension
	); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_SerUserGroups (     // serialize list of user groups
	DWORD hClient,							// client ID
	DWORD hProject 							// handle of the project
    ); // number of groups - serialized in SerBuffer - sep is '\n'

// events

#define K5DBEV_VARNEW			0x00070001	// new variable has been created
#define K5DBEV_VARRENAMED		0x00070002	// variable name or IO alias has been changed
#define K5DBEV_VARDELETED		0x00070003	// variable has been deleted
#define K5DBEV_VARLOCKED		0x00070004	// variable has been locked
#define K5DBEV_VARCHANGED		0x00070005	// variable has been changed
#define K5DBEV_VARUNLOCKED		0x00070006	// variable has been unlocked
#define K5DBEV_VARMOVED			0x00070007	// variable has been moved within its parent group
#define K5DBEV_VARBEGINMOVE     0x00070008  // variable will be moved (arg: hVar before)
#define K5DBEV_VARENDMOVE       0x00070009  // move is finished (arg: hVar after or NULL if fail)

// reserved - CD only

DWORD K5DB_APICALL K5DB_GetVarPVID (DWORD hClient, DWORD hProject, DWORD hVar);
DWORD K5DB_APICALL K5DB_SetVarPVID (DWORD hClient, DWORD hProject, DWORD hVar, DWORD dwPVID);
DWORD K5DB_APICALL K5DB_FindVarByPVID (DWORD hClient, DWORD hProject, DWORD dwPVID);

/////////////////////////////////////////////////////////////////////////////
// properties

// convention

#define K5DBPROP_USER			4096		// 1rst available prop ID for external apps

// predefined properties - project

#define K5DBPROP_CYCLETIMING    1           // cycle triggering (project's property)
#define K5DBPROP_TGTSIZING      2           // target memory sizing for On Line Change (syntax in RTL)
#define K5DBPROP_ENABLEHOT      3           // enable on line change (when defined)
#define K5DBPROP_LASTCHANGE     4           // date stamp of last change bad for on line change
#define K5DBPROP_PRJHISTORY     5           // project history settings
#define K5DBPROP_VERSIONING     6           // project version info
#define K5DBPROP_IOCHANBASE     7           // base index for IO channels
#define K5DBPROP_TGTCTSIZING    8           // min size of CT segment (for On Line Change)
#define K5DBPROP_CONFIG         9           // active configuration name
#define K5DBPROP_ISLIBRARY      10          // non null if project is library
#define K5DBPROP_LASTLIBUPDATE  11          // time stamp of last updates of libraries
#define K5DBPROP_PASCRIPT       12          // no null if PA scripting project
#define K5DBPROP_FBUNDEF        13          // list of undef fieldbuses (\t separated)
#define K5DBPROP_COMMSETTINGS   14          // communication settings
#define K5DBPROP_BINDPORT       15          // port use for the binding
#define K5DBPROP_COMMDRIVER     16          // communication driver
#define K5DBPROP_K5MDEST        17          // Monitoring builder default destination folder
#define K5DBPROP_NONIECSYB      18          // allow non IEC variable names
#define K5DBPROP_ONLINECSTS     19          // Constants and init values can be changed On Line
#define K5DBPROP_PRJVARUID      20          // next unique ID for On Line Change
#define K5DBPROP_DDKHIDE        21          // list of hidden configs (DLL prefixes, separated by ',')
#define K5DBPROP_MTPROJECT      22          // project is a MT task
#define K5DBPROP_MTTASKNO       23          // MT project task number
#define K5DBPROP_MTTASKNAME     24          // MT project task name: prefix of .MTDEF file
#define K5DBPROP_BINDINGID      25          // binding object handle
#define K5DBPROP_GDEFID         26          // global definitions object handle
#define K5DBPROP_SC             27          // source control system
#define K5DBPROP_HMIID          28          // embedded HMI object handle
#define K5DBPROP_WKSPID         29          // workspace organization object handle
#define K5DBPROP_DTSC           30          // data type marker for SC
#define K5DBPROP_IOLIST         31          // IO list marker for SC

#define K5DBPROP_LIBLIST        128         // properties after are library folders
#define K5DBPROP_FOLDER         256         // properties after this number are reserved to K5DBSRV

// predefined properties - programs and types

#define K5DBPROP_PRGSCHEDULE    1           // program scheduling: '%period=offset'
#define K5DBPROP_PRGONCALL      2           // program flag: 'on call' exec mode
#define K5DBPROP_PRGTASK        3           // task name (NULL = default cycle)
#define K5DBPROP_UDFBSTRUCTURE  4           // UDFB is a data structure
#define K5DBPROP_PRGCHECK       5           // program validity: "#FALSE#" if not valid
#define K5DBPROP_PRGPATH        6           // path in folder tree
#define K5DBPROP_PRGVISIBLE     7           // reserved for WB
#define K5DBPROP_PRGFAMILY_OBSO 8           // obsolete
#define K5DBPROP_LIBRARY        9           // library pathname
#define K5DBPROP_CRYPT          10          // program has been crypred
#define K5DBPROP_HIDDEN         11          // program is hidden in workspace
#define K5DBPROP_ISFC           12          // instianciable SFC langguage
#define K5DBPROP_PRGOEMEDIT     13          // OEM dll for editing
#define K5DBPROP_PRGCOLOR       14          // optional RGB color
#define K5DBPROP_UDFBSAMA       15          // UDFB is usable in SAMA
#define K5DBPROP_UDFBSAMASHAPE1	16	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE2	17	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE3	18	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE4	19	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE5	20	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE6	21	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE7	22	        // customized shape for SAMA UDFB
#define K5DBPROP_UDFBSAMASHAPE8	23	        // customized shape for SAMA UDFB
#define K5DBPROP_PRGOWNED       40          // this program is managed by another part of straton


// predefined properties - IO groups

#define K5DBPROP_IOGROUPKEY		1			// driver key for an IO board
#define K5DBPROP_IOGROUPOEM		2			// first OEM defined property
#define K5DBPROP_IOGROUPNAME	256			// driver name for an IO board / name of a complex
#define K5DBPROP_IOPARENTNAME	257			// parent driver name for an complex IO board
#define K5DBPROP_IOVIRTUAL		258			// parent driver name for an complex IO board

// predefined properties - variables

#define K5DBPROP_VAREMBED		1			// variable embedded properties
#define K5DBPROP_VARPROFILE     2           // variable profile
#define K5DBPROP_VARDIMS        3           // array multi dimensions: readable: D1,D2[,D3]
                                            // undefined if less than 2 dimensions
#define K5DBPROP_VARHIDE        4           // hide variable: [ "<EDIT>" ] [ "<SEL>" ]
#define K5DBPROP_VARFBDFLOW     5           // FBD flow (hidden) - NULL or "FBDFLOW"
#define K5DBPROP_VARPVID        6           // zenon PVID
#define K5DBPROP_VARSHARED      7           // shared for multitasking
#define K5DBPROP_VARUID         8           // unique ID for On Line Change
#define K5DBPROP_VARMTSHARED    9           // variable is shared on MT: [ Public | Extern ]
#define K5DBPROP_VARMTTASK      10          // task name of the owner (for externs)
#define K5DBPROP_VARSUBGROUP    11          // variable sub-groups (for retains and globals)
#define K5DBPROP_VAR_XFLAGS     12          // mine! don't touch this

#define K5DBPROP_VARUSERGROUP1  256         // user defined group name
#define K5DBPROP_VARUSERGROUP2  257         // user defined group name
#define K5DBPROP_VARUSERGROUP3  258         // user defined group name

// methods

DWORD K5DB_APICALL K5DB_GetNbProp (			// get number of defined properties
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject							// ID of the object (in the project)
	); // RETURN: number of defined properties for the specified object

DWORD K5DB_APICALL K5DB_GetProps (			// get IDs of defined properties
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject,							// ID of the object (in the project)
	DWORD *phProps							// where to store property IDs
	); // RETURN: number of defined properties for the specified object

DWORD K5DB_APICALL K5DB_SetProperty (		// assign a value to a property
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject,							// ID of the object (in the project)
	DWORD hProp,							// ID of the property
	LPCSTR szValue							// new prop value - NULL to erase
	); // RETURN: ok or error

LPCSTR K5DB_APICALL K5DB_GetProperty (		// get the value of a property
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject,							// ID of the object (in the project)
	DWORD hProp								// ID of the property
	); // RETURN: property current value or NULL if not defined

DWORD K5DB_APICALL K5DB_RemoveProperties (	// remove all properties of an object
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject							// ID of the object (in the project)
	); // RETURN: ok or error

// events

#define K5DBEV_K5PROPCHANGED	0x00080001	// K5 reserved property has changed
#define K5DBEV_EXTPROPCHANGED	0x00080002	// external property has changed

/////////////////////////////////////////////////////////////////////////////
// comments

// predefined types of text

#define K5DBCOMM_SHORT			1			// short tag for graphic languages
#define K5DBCOMM_LONG			2			// long description text
#define K5DBCOMM_MULTILINE		3			// multiline description text

// obsolete

DWORD K5DB_APICALL K5DB_GetNbLanguage (DWORD hClient, DWORD hProject);
DWORD K5DB_APICALL K5DB_GetLanguageList (DWORD hClient, DWORD hProject, DWORD dwMax, LPCSTR *pszText);
DWORD K5DB_APICALL K5DB_SetLanguage (DWORD hClient, DWORD hProject, LPCSTR szLanguage);
LPCSTR K5DB_APICALL K5DB_GetLanguage (DWORD hClient, DWORD hProject);
LPCSTR K5DB_APICALL K5DB_GetLanguageName (DWORD hClient, DWORD hProject);

// methods

DWORD K5DB_APICALL K5DB_SetComment (		// change a comment
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject,							// ID of the object (in the project)
	DWORD dwCommType,						// type of comment text
	LPCSTR szValue							// new comment text
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_GetComment (		// get a comment
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject,							// ID of the object (in the project)
	DWORD dwCommType,						// type of comment text
	char *szBuffer,							// where to copy text
	DWORD dwBufSize							// size of the application buffer
	); // RETURN: number of copied characters or 0 if no text available

DWORD K5DB_APICALL K5DB_GetCommentLength (	// get length of a comment
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hObject,							// ID of the object (in the project)
	DWORD dwCommType						// type of comment text
	); // RETURN: number of characters in the specified text

// events

#define K5DBEV_COMMLANGCHANGED	0x00090001	// selected language has changed
#define K5DBEV_COMMCHANGED		0x00090003	// a comment has changed

/////////////////////////////////////////////////////////////////////////////
// serialization

LPCSTR K5DB_APICALL K5DB_GetSerBuffer (		// get the serial buffer
	DWORD hClient							// client ID
	); // RETURN: pointer to the serialization buffer or "\0" if bad client

void K5DB_APICALL K5DB_SetSerBuffer (		// fills the serial buffer
	DWORD hClient,							// client ID
	LPCSTR szText							// serial text
	);

void K5DB_APICALL K5DB_RealeaseSerBuffer (	// empty the serial buffer
	DWORD hClient							// client ID
	);

DWORD K5DB_APICALL K5DB_SerializeVar (		// serialize variable description
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD hVar								// ID of the variable
	); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_PasteSerializedVar ( // paste serialized variable
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
	DWORD hGroup,							// ID of the parent group
	DWORD dwPosID							// ID of the position (insert before / -1: at the end)
	); // RETURN: handle of new variable or NULL if fail

/////////////////////////////////////////////////////////////////////////////
// advanced features

DWORD K5DB_APICALL K5DB_ExportProgram (     // export a program
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    DWORD hProgram,                         // handle of the program
    LPCSTR szExportFileName                 // export file full pathname
    ); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_ExportHiddenUDFB (  // export a program
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    DWORD hProgram,                         // handle of the program
    LPCSTR szExportFileName                 // export file full pathname
    ); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_ImportProgram (     // export a program
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    LPCSTR szImportFileName,                // export file full pathname
    LPCSTR szWishedName                     // name for the imported program
    ); // RETURN: ok or error

DWORD K5DB_APICALL K5DB_GetImportProgramName ( // get default name for import program
	DWORD hClient,							// client ID
    LPCSTR szImportFileName,                // export file full pathname
    LPSTR szNameBuffer,                     // buffer for storing name
    DWORD dwBufSize                         // size of the buffer
    ); // OK or error

DWORD K5DB_APICALL K5DB_GetImportProgramInfo ( // get info about import file
	DWORD hClient,							// client ID
    LPCSTR szImportFileName,                // export file full pathname
    DWORD *pdwIn,                           // number of inputs
    DWORD *pdwOut,                          // number of outputs
    BOOL *pbUDFB                            // TRUE if it is a UDFB
    ); // OK or error

/////////////////////////////////////////////////////////////////////////////
// Hot changes

DWORD K5DB_APICALL K5DB_PutHotSettings (
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    DWORD dwEnabled,
    DWORD dwStamp,
    LPCSTR szSettings
    );

DWORD K5DB_APICALL K5DB_GetHotSettings (
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    DWORD *pdwEnabled,
    DWORD *pdwStamp,
    LPCSTR *pszSettings
    );

DWORD K5DB_APICALL K5DB_IsHotEnabled (
	DWORD hClient,
	DWORD hProject
    );

void K5DB_APICALL K5DB_SetHotSizing (
	DWORD hClient,							// client ID
	DWORD hProject,							// handle of the project
    LPCSTR szSettings
    );

void K5DB_APICALL K5DB_EnableHot (
	DWORD hClient,							// client ID
	DWORD hProject							// handle of the project
    );

void K5DB_APICALL K5DB_DisableHot (
	DWORD hClient,							// client ID
	DWORD hProject							// handle of the project
    );

DWORD K5DB_APICALL K5DB_CleanHotTracks (
	DWORD hClient,
	DWORD hProject
	);

#define K5DBEV_HOTCHANGED	0x000a0001	// hot settings have changed

/////////////////////////////////////////////////////////////////////////////
// custom objects

DWORD K5DB_APICALL K5DB_CreateXObject (     // create a custom object
	DWORD hClient,                          // client ID
	DWORD hProject                          // handle of the project
	); // ID of the new object or NULL if failed

DWORD K5DB_APICALL K5DB_DeleteXObject (     // delete a custom object
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD hObject                           // handle of the object
	); // OK or error

DWORD K5DB_APICALL K5DB_LockXObject (       // lock a custom object
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD hObject                           // handle of the object
	); // OK or error

DWORD K5DB_APICALL K5DB_UnlockXObject (     // unlock a custom object
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD hObject                           // handle of the object
	); // OK or error

#define K5DBEV_XOBJNEW			0x000b0001	// new object has been created
#define K5DBEV_XOBJDELETED		0x000b0002	// object has been deleted
#define K5DBEV_XOBJLOCKED		0x000b0003	// object has been locked
#define K5DBEV_XOBJUNLOCKED		0x000b0004	// object has been unlocked

/////////////////////////////////////////////////////////////////////////////
// configuration data

// IDs of possible configuration data

#define K5DBCONFIG_BINDING      1           // event based binding
#define K5DBCONFIG_OBSOLETE2    2
#define K5DBCONFIG_OBSOLETE3    3
#define K5DBCONFIG_BUS          4           // obsolete
#define K5DBCONFIG_GLOBALDEF    5           // global definitions

// services

DWORD K5DB_APICALL K5DB_LockConfig (        // lock a configuration object
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD dwConfID                          // ID of the configuration
	); // OK or error

DWORD K5DB_APICALL K5DB_UnlockConfig (      // unlock a configuration object
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD dwConfID                          // ID of the configuration
	); // OK or error

DWORD K5DB_APICALL K5DB_IsConfigLocked (    // test if a config object is locked
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD dwConfID,                         // ID of the configuration
    DWORD *phOwner                          // handle of the owner client
	); // TRUE if locked

DWORD K5DB_APICALL K5DB_SaveConfigChanges ( // notifies a change of config
	DWORD hClient,                          // client ID
	DWORD hProject,                         // handle of the project
    DWORD dwConfID                          // ID of the configuration
	); // OK or error

#define K5DBEV_CFBINDLOCKED		0x000c0001	// BINDING has been locked
#define K5DBEV_CFBINDUNLOCKED	0x000c0002	// BINDING has been unlocked
#define K5DBEV_CFBINDSAVED		0x000c0003	// BINDING has been saved
#define K5DBEV_CFBUSLOCKED		0x000c000a
#define K5DBEV_CFBUSUNLOCKED	0x000c000b
#define K5DBEV_CFBUSSAVED		0x000c000c
#define K5DBEV_CFEQVLOCKED		0x000c000d	// global def has been locked
#define K5DBEV_CFEQVUNLOCKED	0x000c000e	// global def has been unlocked
#define K5DBEV_CFEQVSAVED		K5DBEV_EQVGLOBAL

/////////////////////////////////////////////////////////////////////////////
// visual dialog box for variable selection

#define K5DBVSEL_NOTEXTSEL      0x00000001  // no text selected if edit box
#define K5DBVSEL_CREATEVAR      0x00000002  // create var if not existing
#define K5DBVSEL_DEBUG          0x00000004  // debug mode (False=edit)
#define K5DBVSEL_FOCUSEDIT      0x00000008  // focus edit box at open
#define K5DBVSEL_DEFAULTPOS     0x00000010  // dont apply window position
#define K5DBVSEL_COMPLETE       0x00000020  // completion mode (filter possible vars)
#define K5DBVSEL_SHOWALWAYS     0x00000040  // always display box (even if 1 choice)
#define K5DBVSEL_CREATEONLY     0x00000080  // no selection - check for exist and create
#define K5DBVSEL_FILTERTYPE     0x00000100  // use preferred type for filtering list
#define K5DBVSEL_FROMLIST       0x00000200  // list of items passed by the caller
#define K5DBVSEL_GLOBALONLY     0x00000400  // only global and retain
#define K5DBVSEL_NOEXPAND       0x00000800  // do not expand complex data
#define K5DBVSEL_CREATEGLOBAL   0x00001000  // for creation, propose Global per default
#define K5DBVSEL_HIDEOPTION     0x00002000  // used to hide combo and options
#define K5DBVSEL_MULTIPROJECT   0x00004000  // multi-project mode

#define K5DBVSEL_APPLYOPTIONS   0x80000000  // remember options for future calls

#pragma pack (push, 8)
typedef struct _s_K5DBvsel
{
    DWORD hParentGroup;     // parent group handle or NULL
    DWORD hPrefType;        // preferred data type
    HWND  hwndParent;       // calling window handle
    POINT ptPos;            // wished position (parent window coordinates)
    DWORD dwOptions;        // possible options
}
str_K5DBvsel;
#pragma pack (pop)

BOOL K5DB_APICALL K5DB_SelectVar (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    LPCSTR szText,          // input selection
    str_K5DBvsel *pContext, // options and context
    DWORD *phVar            // handle of selected var (top level) or NULL
    );  // TRUE if OK - result in K5DB_GetSerBuffer();

/////////////////////////////////////////////////////////////////////////////
// enumerate child sub items of a complex item

DWORD K5DB_APICALL K5DB_GetExpSubItems (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hParentProgram,   // handle of parent program or NULL
    LPCSTR szText,          // input text = expression
    DWORD *pdwMinIndex,     // start index for enumeration (if % in result)
    DWORD *pdwMaxIndex,     // stop index for enumeration (if % in result)
    DWORD *phVar            // handle of leave item
    ); // number of sub items
       // list in K5DB_GetSerBuffer(), separated by \t
       // eg for an array: tab -> tab[%u]  min=0 max=9
       // str -> item1\titem2

/////////////////////////////////////////////////////////////////////////////
// program folders

#define K5DBFOLDER_ROOTPRG  "<Root>"

DWORD K5DB_APICALL K5DB_GetNbFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hParentFolder     // handle of parent folder or NULL for root
    ); // RETURN: number of child folders

DWORD K5DB_APICALL K5DB_GetFolders (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hParentFolder,    // handle of parent folder or NULL for root
    DWORD *phFolders        // array filled with folder IDs
    ); // RETURN: number of child folders

DWORD K5DB_APICALL K5DB_CreateFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hParentFolder,    // handle of parent folder or NULL for root
    LPCSTR szName           // folder name
    ); // RETURN: handle of the folder or NULL if fail

DWORD K5DB_APICALL K5DB_DeleteFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hFolder           // handle of folder
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_RenameFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hFolder,          // handle of folder
    LPCSTR szNewName        // new folder name
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_CanMoveFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hFolder,          // handle of folder
    DWORD hParentFolder     // handle of new parent folder or NULL for root
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_MoveFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hFolder,          // handle of folder
    DWORD hParentFolder     // handle of new parent folder or NULL for root
    ); // RETURN: OK or error

LPCSTR K5DB_APICALL K5DB_GetFolderDesc (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hFolder           // handle of folder
    ); // RETURN: number of child folders

DWORD K5DB_APICALL K5DB_GetNbFolderProg (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hParentFolder     // handle of parent folder or NULL for root
    ); // RETURN: number of programs in the folder

DWORD K5DB_APICALL K5DB_GetFolderProgs (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hParentFolder,    // handle of parent folder or NULL for root
    DWORD *phprograms       // array filled with program IDs
    ); // RETURN: number of programs in the folder

DWORD K5DB_APICALL K5DB_CanMoveProgramToFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hProgram,         // handle of program
    DWORD hParentFolder     // handle of new parent folder or NULL for root
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_MoveProgramToFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hProgram,         // handle of program
    DWORD hParentFolder     // handle of new parent folder or NULL for root
    ); // RETURN: OK or error

LPCSTR K5DB_APICALL K5DB_GetFolderProgramPath (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hProgram          // handle of program
    ); // RETURN: number of child folders

DWORD K5DB_APICALL K5DB_GetProgramFolder (
    DWORD hClient,          // client handle
    DWORD hProject,         // project handle
    DWORD hProgram          // handle of program
    ); // RETURN: handle of the parent folder or NULL

#define K5DBEV_FOLDERNEW		0x000d0001	// new folder has been created
#define K5DBEV_FOLDERRENAMED	0x000d0002	// folder name has been changed
#define K5DBEV_FOLDERDELETED	0x000d0003	// folder has been deleted
#define K5DBEV_FOLDERMOVED		0x000d0004	// folder has been moved
#define K5DBEV_FOLDERPRGMOVED   0x000d0005	// program has been moved to folder

#define K5DBPRGEX_SP            0x0001  // program is sub-program
#define K5DBPRGEX_STRUCT        0x0002  // UDFB is structure

DWORD K5DB_APICALL K5DB_CreateProgramEx (	// create a new program
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
	DWORD dwLanguage,					// language
	DWORD dwSection,					// wished section
	DWORD hParent,						// parent SFC program (if child)
	LPCSTR szProgName,  				// program name
    DWORD dwFlags,                      // creation flags
    DWORD hParentFolder                 // parent folder
	); // RETURN: handle of the new program

/////////////////////////////////////////////////////////////////////////////
// Files: .SPL / .RCP / .GRA  (.SPY: reserved for programs - not in list)

DWORD K5DB_APICALL K5DB_GetNbFile (     // get number of files in the project
	DWORD hClient,						// client ID
	DWORD hProject, 					// handle of the project
    LPCSTR szExt                        // NULL or list of suffixes separated by '|' - ex: ".rcp|.spy"
    ); // RETURN: nb of files

DWORD K5DB_APICALL K5DB_GetFiles (      // get handles of files in the project
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    LPCSTR szExt,                       // NULL or list of suffixes separated by '|' - ex: ".rcp|.spy"
    DWORD *phFiles                      // array of file handles
    ); // RETURN: nb of files

DWORD K5DB_APICALL K5DB_FindFile (      // find file in project
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    LPCSTR szName                       // searched name
    ); // RETURN: found file handle or NULL

LPCSTR K5DB_APICALL K5DB_GetFileDesc (  // get file name
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile                         // handle of the file
    ); // RETURN: file name

DWORD K5DB_APICALL K5DB_CanCreateFile ( // create new file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    LPCSTR szName                       // file name
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_CreateFile (    // create new file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    LPCSTR szName                       // file name
    ); // RETURN: new file handle or NULL if error

DWORD K5DB_APICALL K5DB_RenameFile (    // rename a file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile,                        // handle of the file
    LPCSTR szNewName                    // new name
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_DeleteFile (    // delete a file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile                         // handle of the file
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_LockFile (      // lock a file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile                         // handle of the file
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_UnlockFile (    // unlock a file
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile                         // handle of the file
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_SaveFileChanges ( // say file is changed
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile                         // handle of the file
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_LookForNewFiles ( // re-scan (for new files only)
	DWORD hClient,						// client ID
	DWORD hProject						// handle of the project
    ); // RETURN: OK or error

DWORD K5DB_APICALL K5DB_CopyFile (      // copy a file (creation only!)
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hFile,                        // handle of the source file
    LPCSTR szDstName                    // name of dest file to be created
    ); // RETURN: handle of the new file or NULL if error

#define K5DBEV_FILENEW		    0x000e0001	// new file has been created
#define K5DBEV_FILERENAMED	    0x000e0002	// file name has been changed
#define K5DBEV_FILEDELETED	    0x000e0003	// file has been deleted
#define K5DBEV_FILELOCKED       0x000e0004	// file has been locked
#define K5DBEV_FILEUNLOCKED     0x000e0005	// file has been unlocked
#define K5DBEV_FILECHANGED      0x000e0006	// file contents has been changed
#define K5DBEV_FILESRELOADED    0x000e0007	// files have been re-scanned

/////////////////////////////////////////////////////////////////////////////
// source control

// states

#define K5DBSC_EDITABLE         0       // checked out or offline
#define K5DBSC_LOCKED           1       // checked in

DWORD K5DB_APICALL K5DB_GetSCState (    // get item status in source control
	DWORD hClient,						// client ID
	DWORD hProject,						// handle of the project
    DWORD hObject                       // handle of the object (prj|prg|grp|typ|file)
    ); // object status

BOOL K5DB_APICALL K5DB_HasSCLocks (     // check if some items are locked by source control
    DWORD hClient,						// client ID
    DWORD hProject 						// handle of the project
    );

#define K5DBEV_SCCHANGED        0x000f0001	// source control state changed
#define K5DBEV_SCRELOADED       0x000f0002  // object was reloaded from source control

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // !defined(_K5DBAPI_H__INCLUDED_)
