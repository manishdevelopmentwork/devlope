
#include "Intern.hpp"

#include "Service.hpp"

#include "MqttClientOptionsUbidots.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic MQTT Client Options
//

// Constructor

CMqttClientOptionsUbidots::CMqttClientOptionsUbidots(void)
{
}

// Initialization

void CMqttClientOptionsUbidots::Load(PCBYTE &pData)
{
	CMqttClientOptionsJson::Load(pData);

	m_Token    = UniConvert(GetWide(pData));

	m_Device   = UniConvert(GetWide(pData));

	m_PubTopic = "/v1.6/devices/" + m_Device.ToLower();

	m_SubTopic = "/v1.6/devices/" + m_Device.ToLower() + "/+/lv";

	m_Root     = 2;
}

// Config Fixup

BOOL CMqttClientOptionsUbidots::FixConfig(void)
{
	BOOL fFix = CMqttClientOptionsJson::FixConfig();

	m_UserName = m_Token;

	return fFix;
}

// Attributes

CString CMqttClientOptionsUbidots::GetExtra(void) const
{
	CString Extra;

	Extra += m_PubTopic + '&';
	
	Extra += m_SubTopic;

	return Extra;
}

// End of File
