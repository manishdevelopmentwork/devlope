
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Registration
//

extern void Register_OpcUaServer(void);

extern void Register_OpcUaClient(void);

extern void Revoke_OpcUaServer(void);

extern void Revoke_OpcUaClient(void);

//////////////////////////////////////////////////////////////////////////
//
// Host Extension Registration
//

void Register_HxOpcUa(void)
{
	Register_OpcUaServer();
	
	Register_OpcUaClient();
	}

void Revoke_HxOpcUa(void)
{
	Revoke_OpcUaServer();

	Revoke_OpcUaClient();
	}

// End of File
