
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_CdcNtbSize_HPP

#define	INCLUDE_CdcNtbSize_HPP

//////////////////////////////////////////////////////////////////////////
//
// Cdc Framework
//

#include "Cdc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// NTB Size
//

class CCdcNtbSize : public CdcNtbSize
{
	public:	
		// Constructor
		CCdcNtbSize(void);

		// Endianess
		void HostToCdc(void);
		void CdcToHost(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
