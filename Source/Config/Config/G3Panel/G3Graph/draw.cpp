
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor
//

// Drawing Messages

void CPageEditorWnd::OnPaint(void)
{
	// TODO -- The problem probably isn't in here, but we sometimes overdraw primitives
	// which causes issue when they are alpha-blended. We need to be a bit more careful
	// about the exact drawing sequence when editing!!!!

	CRect  Work = GetClientRect();

	CRect  Disp = CRect(m_FrameRect.GetTopLeft(), m_DispSize);

	CPoint From = CPoint(0, 0);
		    
	CPaintDC  MainDC(ThisObject);

	CMemoryDC WorkDC(MainDC);

	CBitmap   Bitmap(MainDC, Work.GetSize());

	WorkDC.Select(Bitmap);

	WorkDC.Save();

	WorkDC.FillRect(Work, afxBrush(TabFace));

	ConfigDC(WorkDC);

	DrawFrame(WorkDC);

	if( ShowTempItems() ) {

		// LATER -- This always put the object on top
		// of everything, which lets you see what you
		// are doing, but might be misleading...

		SaveImage();

		TrackDraw();

		WorkDC.BitBlt(Disp, CDC::FromHandle(m_pWin->GetDC()), From, SRCCOPY);

		RestoreImage();

		WorkDC.Restore();

		DrawErrors(WorkDC);
		}
	else {
		WorkDC.BitBlt(Disp, CDC::FromHandle(m_pWin->GetDC()), From, SRCCOPY);

		WorkDC.Restore();

		DrawErrors(WorkDC);
		}

	TrackDraw(WorkDC);

	DrawGrid(WorkDC);

	MainDC.BitBlt(Work, WorkDC, From, SRCCOPY);
	
	Sym_Init();
	}

BOOL CPageEditorWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

// Drawing Helpers

BOOL CPageEditorWnd::ShowTempItems(void)
{
	if( m_fDrop ) {

		return TRUE;
		}

	switch( m_uCapture ) {

		case captCreate:
		case captSize:
			
			return TRUE;
		}

	return FALSE;
	}

void CPageEditorWnd::DrawErrors(CDC &DC)
{
	if( m_fShowError ) {

		UINT e = m_Error.GetCount();

		for( UINT n = 0; n < e; n++ ) {

			CRect Rect = m_Error[n];
	
			Rect.Normalize();

			Rect = PPtoDP(Rect) + 2;

			DrawError(DC, Rect);
			}
		}
	}

void CPageEditorWnd::DrawError(CDC &DC, CRect Rect)
{
	CSize Size = Rect.GetSize();

	CRect Draw = CRect(Size);

	if( m_bItemBlend ) {

		CMemoryDC WorkDC(DC);

		PBYTE     pData = NULL;

		CBitmap   Bitmap(Size, 1, 32, pData);

		CBrush    Brush(CBitmap(L"PrimError"));

		WorkDC.Select(Bitmap);

		WorkDC.SetBkColor  (afxColor(BLACK));

		WorkDC.SetTextColor(afxColor(RED));

		WorkDC.FillRect (Draw, Brush);
		
		WorkDC.FrameRect(Draw, afxBrush(WHITE));

		Bitmap.SetAlpha (pData, 255);

		BLENDFUNCTION Blend;

		Blend.BlendOp             = AC_SRC_OVER;
		Blend.BlendFlags          = 0;
		Blend.SourceConstantAlpha = 160;
		Blend.AlphaFormat         = AC_SRC_ALPHA;

		AlphaBlend( DC,
			    Rect.left,
			    Rect.top,
			    Size.cx,
			    Size.cy,
			    WorkDC,
			    0,
			    0,
			    Size.cx,
			    Size.cy,
			    Blend
			    );

		WorkDC.Deselect();
		}
	else {
		CMemoryDC WorkDC(DC);

		CBitmap   Bitmap(Size, 1, 32);

		CBrush    Brush(CBitmap(L"PrimError"));

		WorkDC.Select(Bitmap);

		WorkDC.SetBkColor  (afxColor(MAGENTA));

		WorkDC.SetTextColor(afxColor(RED));

		WorkDC.FillRect (Draw, Brush);
		
		WorkDC.FrameRect(Draw, afxBrush(WHITE));

		DC.SetBkColor(afxColor(MAGENTA));

		DC.TransBlt  (Rect, WorkDC, CPoint());

		WorkDC.Deselect();
		}
	}

void CPageEditorWnd::DrawGrid(CDC &DC)
{
	if( m_fGridShow && m_nScale ) {

		if( m_bGridBlend ) {

			CMemoryDC WorkDC(DC);

			PBYTE     pData = NULL;

			CBitmap   Bitmap(m_DispSize, 1, 32, pData);

			WorkDC.Select(Bitmap);

			WorkDC.FillRect(m_DispSize, afxBrush(BLACK));

			CBrush Brush1(CColor(afxColor(WHITE)));

			CBrush Brush2(CColor(afxColor(3dShadow)));

			CSize  Dot  = CSize(1, 1);

			CPoint Top  = m_FrameRect.GetTopLeft();

			CPoint Org1 = CPoint( 8,  8);

			CPoint Org2 = CPoint(64, 48);

			CPoint Pos1;
			
			CPoint Pos2;

			Pos2.y = Org2.y;
			
			for( Pos1.y = Org1.y; Pos1.y < m_DispSize.cy; Pos1.y += 8 ) {

				Pos2.x = Org2.x;

				for( Pos1.x = Org1.x; Pos1.x < m_DispSize.cx; Pos1.x += 8) {

					CPoint From = Pos1;

					CRect  Rect = CRect(From, Dot);

					if( Pos1.x == Pos2.x || Pos1.y == Pos2.y ) {
						
						WorkDC.FillRect(Rect, Brush1);					
						}
					else 
						WorkDC.FillRect(Rect, Brush2);

					if( Pos1.x == Pos2.x ) {

						Pos2.x += 64;
						}
					}

				if( Pos1.y == Pos2.y ) {

					Pos2.y += 48;
					}
				}

			Bitmap.SetAlpha(pData, 255);

			BLENDFUNCTION Blend;

			Blend.BlendOp             = AC_SRC_OVER;
			Blend.BlendFlags          = 0;
			Blend.SourceConstantAlpha = m_bGridBlend;
			Blend.AlphaFormat         = AC_SRC_ALPHA;

			CPoint Pos = PPtoDP(CPoint(0,0));

			AlphaBlend( DC,
				    Pos.x,
				    Pos.y,
				    m_DispSize.cx * m_nScale,
				    m_DispSize.cy * m_nScale,
				    WorkDC,
				    0,
				    0,
				    m_DispSize.cx,
				    m_DispSize.cy,
				    Blend
				    );

			WorkDC.Deselect();
			}
		else {
			CBrush &Brush1 = afxBrush(WHITE);

			CBrush &Brush2 = afxBrush(3dShadow);

			int    nDiv = m_nScale;

			CSize  Grid = CSize(nDiv, nDiv);

			CPoint Top  = m_FrameRect.GetTopLeft();

			CPoint Org1 = CPoint( 8,  8);

			CPoint Org2 = CPoint(64, 48);

			CPoint Init = PPtoDP(CPoint(0, 0));

			CPoint Pos1;
			
			CPoint Pos2;

			Pos2.y = Org2.y;
			
			for( Pos1.y = Org1.y; Pos1.y < m_DispSize.cy; Pos1.y += 8 ) {

				Pos2.x = Org2.x;

				for( Pos1.x = Org1.x; Pos1.x < m_DispSize.cx; Pos1.x += 8) {

					CPoint From = Init + Pos1 * m_nScale;

					CRect  Rect = CRect(From, Grid);

					if( Pos1.x == Pos2.x || Pos1.y == Pos2.y ) {
						
						DC.FillRect(Rect, Brush1);					
						}
					else 
						DC.FillRect(Rect, Brush2);

					if( Pos1.x == Pos2.x ) {

						Pos2.x += 64;
						}
					}

				if( Pos1.y == Pos2.y ) {

					Pos2.y += 48;
					}
				}
			}
		}
	}

void CPageEditorWnd::DrawSelect(CDC &DC)
{
	if( HasSelect() ) {

		CRect Rect = PPtoDP(m_SelRect);

		BlendItemRect(DC, Rect, afxColor(RED));
		}
	}

void CPageEditorWnd::DrawHandles(CDC &DC)
{
	for( int n = m_Handles.GetCount(); n--; ) {

		CHand const &Hand = m_Handles[n];

		DrawHandle(DC, Hand, UINT(n) == m_uHandle);
		}
	}

void CPageEditorWnd::DrawHandle(CDC &DC, CHand const &Hand, BOOL fOver)
{
	CRect Rect = Hand.m_Rect;

	CSize Size = Rect.GetSize();

	if( m_bItemBlend ) {

		CRect Work = CRect(Size);

		CMemoryDC WorkDC(DC);

		PBYTE     pData = NULL;

		CBitmap   Bitmap(Size, 1, 32, pData);

		CPen      DarkPen(CColor(1,1,1));

		WorkDC.Select(Bitmap);

		if( Hand.m_Code == handItem ) {

			WorkDC.Select(DarkPen);

			WorkDC.Select(afxBrush(YELLOW));
			}
		else {
			WorkDC.Select(afxPen(WHITE));

			WorkDC.Select(afxBrush(Orange1));
			}

		WorkDC.FillRect(Work, afxBrush(BLACK));

		DrawHandle(WorkDC, Hand, Work);

		Bitmap.SetAlpha(pData, 255);

		BLENDFUNCTION Blend;

		Blend.BlendOp             = AC_SRC_OVER;
		Blend.BlendFlags          = 0;
		Blend.SourceConstantAlpha = BYTE((Hand.m_Code == handItem) ? 200 : 160);
		Blend.AlphaFormat         = AC_SRC_ALPHA;

		AlphaBlend( DC,
			    Rect.left,
			    Rect.top,
			    Size.cx,
			    Size.cy,
			    WorkDC,
			    0,
			    0,
			    Size.cx,
			    Size.cy,
			    Blend
			    );

		WorkDC.Deselect();

		WorkDC.Deselect();
		
		WorkDC.Deselect();
		}
	else {
		DC.Select(afxPen(WHITE));

		DC.Select(afxBrush(Orange1));

		DrawHandle(DC, Hand, Rect);

		DC.Deselect();

		DC.Deselect();
		}
	}

void CPageEditorWnd::DrawHandle(CDC &DC, CHand const &Hand, CRect const &Rect)
{
	switch( Hand.m_Code ) {

		case handMid:

			DC.Rectangle(Rect);

			break;

		case handItem:
			
			DC.Diamond(Rect);
			
			break;

		default:

			DC.Ellipse(Rect);

			break;
		}
	}

void CPageEditorWnd::UpdateAll(void)
{
	UpdateSelectData();

	UpdateImage();
	}

BOOL CPageEditorWnd::UpdateImage(void)
{
	// LATER -- This should be optimized to ensure that
	// we don't redraw everything on every little edit.

	m_Error.Empty();

	UpdateBack();

	if( !m_uCapture ) {

		ShowDefaultStatus();
		}

	if( m_uMode == modePickAny ) {

		UpdateImage(m_pList, 1);

		return TRUE;
		}

	if( m_pWorkSet || m_uMode == modeText ) {

		UpdateImage(m_pList, 1);

		if( m_uMode == modeText ) {
		
			if( m_pWorkSet ) {

				UpdateImage(m_pWorkList, 1);
				}
			}

		BlendImage(m_bBackBlend);

		if( m_uMode == modeText ) {

			CPrim *pPrim = GetLoneSelect();

			pPrim->Draw(m_pGDI, drawNoText);
			}
		else {
			m_pWorkSet->Draw(m_pGDI, drawSet);

			UpdateImage(m_pWorkList, 1);
			}

		return FALSE;
		}

	UpdateImage(m_pList, 1);

	return TRUE;
	}

void CPageEditorWnd::UpdateImage(CPrimList *pList, UINT uCode)
{
	INDEX n = pList->GetHead();

	while( !pList->Failed(n) ) {

		CPrim *pPrim = pList->GetItem(n);

		if( uCode == 1 ) {

			if( IsSelected(pPrim) ) {

				if( m_uMode == modeText ) {

					m_pWorkList->GetNext(n);

					continue;
					}

				if( m_uCapture == captSize ) {

					m_pWorkList->GetNext(n);

					continue;
					}

				if( m_fDrop && m_fDropMove ) {
					
					if( m_uDropType == dropSamePage ) {

						m_pWorkList->GetNext(n);

						continue;
						}
					}
				}
			}

		if( uCode == 2 && pList == m_pWorkList ) {

			if( !IsSelected(pPrim) ) {

				m_pWorkList->GetNext(n);

				continue;
				}
			}

		if( pPrim->IsSet() ) {

			CPrimSet *pSet = (CPrimSet *) pPrim;

			if( m_uMode == modePickAny ) {

				UpdateImage(pSet->m_pList, uCode);

				pList->GetNext(n);

				continue;
				}

			if( pSet != m_pWorkSet ) {

				UpdateImage(pSet->m_pList, uCode);
				}
			}
		else {
			pPrim->Draw(m_pGDI, drawWhole);

			if( pPrim->HasError() ) {

				CRect Rect = pPrim->GetRect();

				m_Error.Append(Rect);
				}
			}

		pList->GetNext(n);
		}
	}

void CPageEditorWnd::UpdateBack(void)
{
	if( m_pProps->m_pMaster ) {

		DWORD  hItem = m_pProps->m_pMaster->GetObjectRef();

		CItem *pItem = m_pItem->GetDatabase()->GetHandleItem(hItem);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CDispPage)) ) {

				CDispPage      *pPage  = (CDispPage *) pItem;

				CDispPageProps *pProps = pPage->m_pProps;

				COLOR           Back   = pProps->m_pBack->GetColor();

				m_pGDI->ClearScreen(Back);

				if( m_fMastShow ) {

					UpdateImage(pPage->m_pList, 0);

					BlendImage(m_bMastBlend);
					}
				else {
					if( Back ) {

						BlendImage(m_bMastBlend);
						}
					}

				return;
				}
			}
		}

	COLOR Back = m_pProps->m_pBack->GetColor();

	m_pGDI->ClearScreen(Back);
	}

void CPageEditorWnd::SaveImage(void)
{
	m_pWin->SaveImage();
	}

void CPageEditorWnd::RestoreImage(void)
{
	m_pWin->RestoreImage();
	}

// Line-Up Checks

void CPageEditorWnd::ShowLineUp(CDC &DC, BOOL fSelect)
{
	m_LinedUp.Empty();

	ShowLineUpTop   (DC, fSelect);

	ShowLineUpBottom(DC, fSelect);
	
	ShowLineUpLeft  (DC, fSelect);
	
	ShowLineUpRight (DC, fSelect);
	
	ShowLineUpCenter(DC, fSelect);
	
	ShowLineUpMiddle(DC, fSelect);
	}

BOOL CPageEditorWnd::ShowLineUp(CPrim *pPrim, BOOL fSelect)
{
	if( !pPrim->IsLine() ) {

		if( fSelect ) {

			return TRUE;
			}

		return !IsSelected(pPrim);
		}

	return FALSE;
	}

void CPageEditorWnd::ShowLineUpBottom(CDC &DC, BOOL fSelect)
{
	if( m_fAlignEdge ) {

		INDEX n = m_pWorkList->GetHead();

		int   t = m_TrackRect.bottom;

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( ShowLineUp(pPrim, fSelect) ) {

				if( pPrim->GetRect().bottom == t ) {

					CRect Line = PPtoDP(m_TrackRect);

					CRect Item = PPtoDP(pPrim->GetRect());

					Line.top   = Line.bottom - 1;

					Line.left  = min(Line.left,  Item.left);

					Line.right = max(Line.right, Item.right);

					Line.top    += m_nItemHalf;

					Line.bottom += m_nItemHalf;

					if( m_LinedUp.Failed(m_LinedUp.Find(pPrim)) ) {

						BlendItemRect(DC, Item, afxColor(BLUE));

						m_LinedUp.Insert(pPrim);
						}

					DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

					Invalidate(FALSE);
					}
				}

			m_pWorkList->GetNext(n);
			}
		}
	}

void CPageEditorWnd::ShowLineUpTop(CDC &DC, BOOL fSelect)
{
	if( m_fAlignEdge ) {

		INDEX n = m_pWorkList->GetHead();

		int   t = m_TrackRect.top;

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( ShowLineUp(pPrim, fSelect) ) {

				if( pPrim->GetRect().top == t ) {

					CRect Line  = PPtoDP(m_TrackRect);

					CRect Item  = PPtoDP(pPrim->GetRect());

					Line.bottom = Line.top + 1;

					Line.left   = min(Line.left,  Item.left);

					Line.right  = max(Line.right, Item.right);

					Line.top    -= m_nItemHalf;

					Line.bottom -= m_nItemHalf;

					if( m_LinedUp.Failed(m_LinedUp.Find(pPrim)) ) {

						BlendItemRect(DC, Item, afxColor(BLUE));

						m_LinedUp.Insert(pPrim);
						}

					DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

					Invalidate(FALSE);
					}
				}

			m_pWorkList->GetNext(n);
			}
		}
	}

void CPageEditorWnd::ShowLineUpLeft(CDC &DC, BOOL fSelect)
{
	if( m_fAlignEdge ) {

		INDEX n = m_pWorkList->GetHead();

		int   t = m_TrackRect.left;

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( ShowLineUp(pPrim, fSelect) ) {

				if( pPrim->GetRect().left == t ) {

					CRect Line  = PPtoDP(m_TrackRect);

					CRect Item  = PPtoDP(pPrim->GetRect());

					Line.right  = Line.left + 1;

					Line.top    = min(Line.top,    Item.top);

					Line.bottom = max(Line.bottom, Item.bottom);

					Line.left  -= m_nItemHalf;

					Line.right -= m_nItemHalf;

					if( m_LinedUp.Failed(m_LinedUp.Find(pPrim)) ) {

						BlendItemRect(DC, Item, afxColor(BLUE));

						m_LinedUp.Insert(pPrim);
						}

					DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

					Invalidate(FALSE);
					}
				}

			m_pWorkList->GetNext(n);
			}
		}
	}

void CPageEditorWnd::ShowLineUpRight(CDC &DC, BOOL fSelect)
{
	if( m_fAlignEdge ) {

		INDEX n = m_pWorkList->GetHead();

		int   t = m_TrackRect.right;

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( ShowLineUp(pPrim, fSelect) ) {

				if( pPrim->GetRect().right == t ) {

					CRect Line  = PPtoDP(m_TrackRect);

					CRect Item  = PPtoDP(pPrim->GetRect());

					Line.left   = Line.right - 1;

					Line.top    = min(Line.top,    Item.top);

					Line.bottom = max(Line.bottom, Item.bottom);

					Line.left  += m_nItemHalf;

					Line.right += m_nItemHalf;

					if( m_LinedUp.Failed(m_LinedUp.Find(pPrim)) ) {

						BlendItemRect(DC, Item, afxColor(BLUE));

						m_LinedUp.Insert(pPrim);
						}

					DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

					Invalidate(FALSE);
					}
				}

			m_pWorkList->GetNext(n);
			}
		}
	}

void CPageEditorWnd::ShowLineUpCenter(CDC &DC, BOOL fSelect)
{
	if( m_fAlignCenter ) {

		INDEX n = m_pWorkList->GetHead();

		int   t = m_TrackRect.left + m_TrackRect.right;

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( ShowLineUp(pPrim, fSelect) ) {

				if( pPrim->GetRect().left + pPrim->GetRect().right == t ) {

					CRect Line  = PPtoDP(m_TrackRect);

					CRect Item  = PPtoDP(pPrim->GetRect());

					Line.left   = (Line.left + Line.right) / 2;

					Line.right  = Line.left + 1;

					Line.top    = min(Line.top,    Item.top);

					Line.bottom = max(Line.bottom, Item.bottom);

					if( m_LinedUp.Failed(m_LinedUp.Find(pPrim)) ) {

						BlendItemRect(DC, Item, afxColor(BLUE));

						m_LinedUp.Insert(pPrim);
						}

					DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

					Invalidate(FALSE);
					}
				}

			m_pWorkList->GetNext(n);
			}

		// LATER -- Align relative to work rect?

		if( t == m_DispSize.cx ) {

			CRect Line  = PPtoDP(CRect(m_DispSize.cx / 2, 0, 0, m_DispSize.cy));

			Line.right  = Line.left   + 1;

			Line.bottom = Line.bottom + 1;

			DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

			Invalidate(FALSE);
			}
		}
	}

void CPageEditorWnd::ShowLineUpMiddle(CDC &DC, BOOL fSelect)
{
	if( m_fAlignCenter ) {

		INDEX n = m_pWorkList->GetHead();

		int   t = m_TrackRect.top + m_TrackRect.bottom;

		while( !m_pWorkList->Failed(n) ) {

			CPrim *pPrim = m_pWorkList->GetItem(n);

			if( ShowLineUp(pPrim, fSelect) ) {

				if( pPrim->GetRect().top + pPrim->GetRect().bottom == t ) {

					CRect Line  = PPtoDP(m_TrackRect);

					CRect Item  = PPtoDP(pPrim->GetRect());

					Line.top    = (Line.top + Line.bottom) / 2;

					Line.bottom = Line.top + 1;

					Line.left   = min(Line.left,  Item.left);

					Line.right  = max(Line.right, Item.right);

					if( m_LinedUp.Failed(m_LinedUp.Find(pPrim)) ) {

						BlendItemRect(DC, Item, afxColor(BLUE));

						m_LinedUp.Insert(pPrim);
						}

					DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

					Invalidate(FALSE);
					}
				}

			m_pWorkList->GetNext(n);
			}

		// LATER -- Align relative to work rect?

		if( t == m_DispSize.cy ) {

			CRect Line  = PPtoDP(CRect(0, m_DispSize.cy / 2, m_DispSize.cx, 0));

			Line.bottom = Line.top   + 1;

			Line.right  = Line.right + 1;

			DC.FrameRect(Line, CBrush(afxColor(RED), afxColor(BLUE), 128));

			Invalidate(FALSE);
			}
		}
	}


// Alpha Blending

void CPageEditorWnd::BlendItemRect(CDC &DC, CRect Rect, CColor Color)
{
	if( m_bItemBlend ) {

		CColor *pColor = (CColor *) alloca(m_nItemEdge * sizeof(CColor));

		BYTE   *pAlpha = (BYTE   *) alloca(m_nItemEdge * sizeof(BYTE  ));

		for( int n = 0; n < m_nItemEdge; n++  ) {

			int  f = (n >= m_nItemHalf) ? (m_nItemEdge - n) : (n + 1);

			BYTE a = BYTE(f * m_bItemBlend / m_nItemHalf);

			BYTE c = BYTE(f * 128          / m_nItemHalf);

			pColor[n] = Color + CColor(c, c, c);

			pAlpha[n] = a;
			}

		Rect += m_nItemEdge;

		CSize Size = CSize(8, 8);

		CRect Work = Size;

		CMemoryDC WorkDC(DC);

		CBitmap   Bitmap(Size, 1, 32);

		WorkDC.Select(Bitmap);

		for( int i = 0; i < m_nItemEdge; i++ ) {

			WorkDC.FillRect(Work, CBrush(pColor[i]));

			CRect Side[4];

			Side[0] = Rect;
			Side[1] = Rect;
			Side[2] = Rect;
			Side[3] = Rect;

			Side[0].left   = Side[0].right  - 1;
			Side[1].right  = Side[1].left   + 1;
			Side[2].top    = Side[2].bottom - 1;
			Side[3].bottom = Side[3].top    + 1;

			Side[0].top    += 1;
			Side[0].bottom -= 1;

			Side[1].top    += 1;
			Side[1].bottom -= 1;

			BLENDFUNCTION Blend;

			Blend.BlendOp             = AC_SRC_OVER;
			Blend.BlendFlags          = 0;
			Blend.SourceConstantAlpha = pAlpha[i];
			Blend.AlphaFormat         = 0;

			for( UINT n = 0; n <  4; n++ ) {

				AlphaBlend( DC,
					    Side[n].left,
					    Side[n].top,
					    Side[n].cx(),
					    Side[n].cy(),
					    WorkDC,
					    0,
					    0,
					    Size.cx,
					    Size.cy,
					    Blend
					    );
				}

			Rect -= 1;
			}

		WorkDC.Deselect();

		return;
		}

	DC.FrameRect(Rect + m_nItemHalf, CBrush(Color));
	}

// cppcheck-suppress passedByValue

void CPageEditorWnd::BlendItemLine(CDC &DC, CRect Rect, CColor Color)
{
	Rect.Normalize();

	BlendItemRect(DC, Rect, Color);
	}

BOOL CPageEditorWnd::BlendImage(BYTE bBlend)
{
	if( bBlend ) {

		CDC &     MainDC = CDC::FromHandle(m_pWin->GetDC());

		CBitmap   Bitmap(MainDC, m_DispSize);

		CMemoryDC GreyDC(MainDC);

		GreyDC.Select(Bitmap);

		BLENDFUNCTION Blend;

		Blend.BlendOp             = AC_SRC_OVER;
		Blend.BlendFlags          = 0;
		Blend.SourceConstantAlpha = bBlend;
		Blend.AlphaFormat         = 0;

		AlphaBlend( GreyDC.GetHandle(),
			    0,
			    0,
			    m_DispSize.cx,
			    m_DispSize.cy,
			    MainDC.GetHandle(),
			    0,
			    0,
			    m_DispSize.cx,
			    m_DispSize.cy,
			    Blend
			    );

		MainDC.BitBlt( m_DispSize,
			       GreyDC,
			       CPoint(0, 0),
			       SRCCOPY
			       );

		GreyDC.Deselect();

		return TRUE;
		}

	return FALSE;
	}

// End of File
