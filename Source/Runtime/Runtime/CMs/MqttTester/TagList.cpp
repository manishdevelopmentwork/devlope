
#include "Intern.hpp"

#include "TagList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Tag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Tag List
//

// Constructor

CTagList::CTagList(void)
{
	}

// Destructor

CTagList::~CTagList(void)
{
	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		delete m_List[n];
		}

	m_List.Empty();
	}

// Attributes

CTag * CTagList::GetItem(UINT uTag) const
{
	return (uTag < m_List.GetCount()) ? m_List[uTag] : NULL;
	}

// End of File
