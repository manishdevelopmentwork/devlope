
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modUserLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load G3Comm\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
				}

			CButtonBitmap *pComm16 = New CButtonBitmap( CBitmap(L"ToolComm16"),
								    CSize(16, 16),
								    21
								    );

			CButtonBitmap *pProg16 = New CButtonBitmap( CBitmap(L"ToolProg16"),
								    CSize(16, 16),
								    1
								    );

			CButtonBitmap *pFlag16 = New CButtonBitmap( CBitmap(L"Flags"),
								    CSize(16, 16),
								    240
								    );

			afxButton->AppendBitmap(0x4000, pComm16);

			afxButton->AppendBitmap(0x4100, pProg16);

			afxButton->AppendBitmap(0x4200, pFlag16);

			return TRUE;
			}

		catch(CException const &Exception)
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading G3Comm\n");

			return FALSE;
			}
		}

	if( uReason == DLL_PROCESS_DETACH ) {

		afxButton->RemoveBitmap(0x4000);

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
		}

	return TRUE;
	}

// End of File
