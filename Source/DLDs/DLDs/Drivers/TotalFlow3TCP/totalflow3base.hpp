
#ifndef	INCLUDE_totalflow3base_HPP
	
#define	INCLUDE_totalflow3base_HPP

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Base Driver
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "../TotalFlow2TCP/totalflow2base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTotalFlowRequestList;

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow v3 Slot Definition
//

struct CSlot3 : CSlot
{
	CTotalFlowRequestList * m_pReq;
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Base Driver
//

class CTotalFlow3Master : public CTotalFlow2Master
{
	public:
		// Constructor
		CTotalFlow3Master(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:

		struct CBase3Ctx : public CTotalFlow2Master::CBaseCtx
		{			
			BYTE m_bStringSize;
			};

		CBase3Ctx * m_pBase3;

		// Slot Overridables
		void    GetSlots(BYTE const * &pData, CBaseCtx *pCtx);
		void    CleanupSlots(void);
		CSlot * FindSlot(AREF Addr, UINT &uCount);
		CSlot * FindSlot(AREF Addr);

		// Implementation
		void GetSlotArrayData(CBlock Block, CSlot ** pSlots, UINT uReply, UINT &uSlot, PCBYTE pData);
		void GetStringData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset = 0);
		void ConstructArray(CBlock * pBlocks, UINT &uIndex, CSlot3 * pSlot);
		void ConstructBlock(CBlock * pBlocks, UINT &uIndex, CSlot ** pSlots, UINT &uSlot, UINT uCount);
		UINT GetDataCount(CBlock Block, UINT uBytes);
		UINT GetDataOffset(CBlock Block, UINT uOffset);

		// Implementation Overridables
		void GetSlotData(CBlock Block, CSlot ** pSlots, UINT uReply, UINT &uSlot, PCBYTE pData);
		UINT ConstructBlocks(CBlock * pBlocks, CSlot ** pSlots, UINT uCount);
		void UpdateReferences(void);
		UINT GetDataOffset(CSlot * pSlot, AREF Addr);
		UINT GetStringOffset(CSlot * pSlot, AREF Addr);
		WORD GetWriteOffset(CSlot * pSlot, AREF Addr);
		WORD GetWriteCount(CSlot * pSlot, UINT uCount);
		UINT GetStringSize(CSlot * pSlot);
				
		// Helpers
		BOOL IsArray(CSlot * pSlot);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Slot Request
//

struct CTotalFlowRequest
{
	UINT m_Offset;
	UINT m_Count;
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Slot Request List
//

class CTotalFlowRequestList
{
	public:
		// Constructor
		CTotalFlowRequestList(IHelper * pHelp);

		// Destructor
		~CTotalFlowRequestList(void);

		//Operations
		void			AddRequest(UINT uOffset, UINT uCount, UINT uStringSize = 0);
		void			Empty(void);
		UINT			GetCount(void);
		CTotalFlowRequest *	GetRequest(UINT uIndex);
		
	protected:

		UINT		    m_uCount;
		CTotalFlowRequest * m_pReq;
		IHelper		  * m_pHelper;

		// Debugging
		void PrintRequests(void);
	};

#endif




