
//////////////////////////////////////////////////////////////////////////
//
// Giddings and Lewis C/E Driver
//

#ifndef	INCLUDE_GANDLCE_HPP
#define	INCLUDE_GANDLCE_HPP

// No Read Transmit return codes
#define	RNT_CONTINUE	0
#define	RNT_DATA	1
#define	RNT_NODATA	2

class CGandLCEDriver : public CMasterDriver
{
	public:
		// Constructor
		CGandLCEDriver(void);

		// Destructor
		~CGandLCEDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			};
		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[32];
		BYTE	m_bRx[32];

		UINT	m_uPtr;

		// Caches
		DWORD	m_dICache[11];
		DWORD	m_dPCache[19];
		DWORD	m_dCCache[10];
		BYTE	m_bSSV;

		// Command
		BYTE	m_bCommand;
		
		// Frame Building
		void	StartFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		UINT	AddWrite(UINT uCommand, DWORD dData);
		
		// Transport Layer
		BOOL	Transact(UINT uCount);
		BOOL	SendStart(void);
		void	Send(void);
		BOOL	GetReply(UINT uCount);
		BOOL	GetResponse(UINT uCommand, PDWORD pData);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		UINT	NoReadTransmit(UINT uCommand, PDWORD pData);
		BOOL	NoWriteTransmit(UINT uCommand, DWORD dData);
		BYTE	GetCommand( UINT uCommand );
		PDWORD	GetCachePosition( UINT Command, UINT * pPos );
		WORD	MakeWord(UINT uPos);

	};

#define	ISEL	 1
#define	IPHS	 2
#define	IBS1	 3
#define	IBS2	 4
#define	ISO	 5
#define	IFFO	 6
#define	IWO	 7
#define	INO	 8
#define	IRFO	 9
#define	IDSP	10
#define	IOSP	11

#define	POIP	21
#define	POP	22
#define	PSC	23
#define	PFL1	24
#define	PFL2	25
#define	PFL3	26
#define	PWR	27
#define	PFFR	28
#define	PFCR	29
#define	PSR	30
#define	PNR	31
#define	POSR	32
#define	PDSR	33
#define	PWP	34
#define	PFP	35
#define	PSP	36
#define	PNP	37
#define	POSP	38
#define	PDSP	39

#define	CBCT	41
#define	CUO	42
#define	CBCL	43
#define	COC	44
#define	CWC	45
#define	CFC	46
#define	CSC	47
#define	CNC	48
#define	COS	49
#define	CDS	50

#define	SS	61
#define	DV	62
#define	SSV	63

#define	IEXE	90
#define PEXE	91
#define	CEXE	92

// Response Sizes
#define	ISIZE	 2
#define	PSIZE	34
#define	CSIZE	 2
#define	RDVSIZE	 4
#define	WDVSIZE  2
#define	SSSIZE	 1
#define	SSVSIZE	 1

#endif
// End of File
