
#include "Intern.hpp"

#include "Consumer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Consumer
//

// Instantiator

global IUnknown * Create_Consumer(PCSTR pName)
{
	CConsumer *p = New CConsumer;

	return p;
	}

// Constructors

CConsumer::CConsumer(void)
{
	StdSetRef();

	m_pAssy = NULL;

	m_fUsed = FALSE;
	}

// Destructor

CConsumer::~CConsumer(void)
{
	}

// Binding

void CConsumer::Bind(UINT uIndex, IImplicit *pAssembly)
{
	m_pAssy  = pAssembly;

	m_uIndex = uIndex;
	}

// Mapping

void CConsumer::SetIdent(DWORD dwInstance, DWORD dwIdent)
{
	m_dwInst  = dwInstance;

	m_dwIdent = dwIdent;
	}

void CConsumer::SetMapping(WORD wOffset, WORD wSize)
{
	m_wOffset = wOffset;

	m_wSize   = wSize; 
	}

// Attributes

DWORD CConsumer::GetInstance(void) const
{
	return m_dwInst;
	}

UINT CConsumer::GetIndex(void) const
{
	return m_uIndex;
	}

BOOL CConsumer::IsFree(void) const
{
	return !m_fUsed;
	}

// Operations

void CConsumer::Create(void)
{
	m_dwInst  = INVALID_INSTANCE;

	m_dwIdent = INVALID_CONN_POINT;

	m_wSize   = 0;

	m_wOffset = 0;

	m_fDirty  = FALSE;
	
	m_fUsed   = TRUE;
	}

void CConsumer::Close(void)
{
	m_fUsed = FALSE;
	}

void CConsumer::SetNewData(void)
{
	m_fDirty = TRUE;
	}

// IUnknown

HRESULT CConsumer::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IConsumer);

	StdQueryInterface(IConsumer);

	return E_NOINTERFACE;
	}

ULONG CConsumer::AddRef(void)
{
	StdAddRef();
	}

ULONG CConsumer::Release(void)
{
	StdRelease();
	}

// IConsumer

DWORD METHOD CConsumer::GetIdent(void)
{
	return m_dwIdent;
	}

UINT METHOD CConsumer::GetSize(void)
{
	return m_wSize;
	}

BOOL METHOD CConsumer::IsActive(void)
{
	if( !m_pAssy || !m_fUsed ) {

		return FALSE;
		}

	if( m_dwInst == INVALID_INSTANCE || m_dwIdent == INVALID_CONN_POINT ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL METHOD CConsumer::HasNewData(void)
{
	return m_fDirty;
	}

void METHOD CConsumer::ClearNewData(void)
{
	m_fDirty = FALSE;
	}

UINT METHOD CConsumer::Recv(PBYTE pData, UINT uSize)
{
	return IsActive() ? m_pAssy->Recv(m_dwInst, pData, uSize) : 0;
	}

// End of File
