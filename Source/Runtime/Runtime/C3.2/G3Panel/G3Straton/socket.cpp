
#include "intern.hpp"

#include "vm\t5vm.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Data

static	const	PCTXT	m_Phase[] = 

	{
	
	"idle",
	"opening",
	"open",
	"closing",
	"error",

	};

static	ISocket	* m_pSocket = NULL;

// Prototypes

static	BOOL	CheckSocket(ISocket *pSocket);
static	void	ParseT5(PBYTE pData);

// Code

clink	T5_RET		socketInitialize(T5HND_CS pfCallback)
{
	AfxTrace("socketInitialize\n");

	return T5RET_ERROR;
	}

clink	void		socketTerminate(void)
{
	AfxTrace("socketTerminate\n");
	}

clink	T5_RET		socketCreateListeningSocket(T5_WORD wPort, T5_WORD wMaxCnx, T5_PTSOCKET pSocket, T5HND_CS pfCallback)
{
	AfxTrace( "socketCreateListeningSocket, port %d, max %d\n", 
		  wPort,
		  wMaxCnx
		  );

	return T5RET_ERROR;
	}

clink	void		socketCloseSocket(T5_SOCKET sock)
{
	AfxTrace("socketCloseSocket, %8.8X\n", sock);

	}

clink	T5_SOCKET	socketAccept(T5_SOCKET sockListen, T5HND_CS pfCallback)
{
	AfxTrace("socketAccept, listen %8.8X\n", sockListen);

	return T5_INVSOCKET;
	}

clink	T5_WORD		socketSend(T5_SOCKET sock, T5_WORD wSize, T5_PTR pData, T5_PTBOOL pbFail)
{
	AfxTrace("socketSend, size %d\n", wSize);


	*pbFail = FALSE;

	return 0;
	}

clink	T5_WORD		socketReceive(T5_SOCKET sock, T5_WORD wSize, T5_PTR pData, T5_PTBOOL pbFail)
{
	AfxTrace("socketReceive.sock %8.8X %d bytes\n", sock, wSize);

	*pbFail = FALSE;

	return 0;
	}

clink	T5_RET		socketCreateConnectedSocket(T5_PTCHAR szAddr, T5_WORD wPort, T5_PTSOCKET pSocket, T5_PTBOOL pbWait, T5HND_CS pfCallback)
{
	AfxTrace("socketCreateConnectedSocket\n");

	return T5RET_ERROR;
	}

clink	T5_RET		socketCheckPendingConnect(T5_SOCKET sock, T5_PTBOOL pbFail)
{
	AfxTrace("socketCheckPendingConnect\n");

	*pbFail = TRUE;

	return T5RET_ERROR;
	}

static	BOOL	CheckSocket(ISocket *pSocket)
{

	return FALSE;
	}

#pragma pack(1)

struct CT5Head
{
	WORD	m_Mark;
	WORD	m_Size;
	};

struct CT5Request
{
	CT5Head	m_Head;
	BYTE	m_bSrc;
	BYTE	m_bHID;
	BYTE	m_bReq;
	WORD	m_wSize;
	BYTE	m_bData[0];
	};

#pragma pack()

static	void	ParseT5(PBYTE pData)
{
	CT5Head const &Head = (CT5Head &) *pData;

	if( Head.m_Mark == MAKEWORD('T', '5') ) {
		
		AfxTrace("size %d\n", HostToMotor(Head.m_Size));

		CT5Request const &Req	= (CT5Request &) *pData;

		AfxTrace(" src %d\n", Req.m_bSrc);
		AfxTrace(" hid %d\n", Req.m_bHID);
		AfxTrace(" req %d\n", Req.m_bReq);
		AfxTrace("size %d\n", HostToMotor(Req.m_wSize));

		switch( Req.m_bReq ) {
			
			case T5REQ_INIT:

				if( TRUE ) {

					AfxTrace("init request\n");

					UINT uSize = HostToMotor(Req.m_wSize);

					for( UINT n = 0; n < uSize; n ++ ) {
						
						AfxTrace("%2.2X\n", Req.m_bData[n]);
						}					
					}



				break;
			
			case T5REQ_READ:
				break;
			
			case T5REQ_EXECUTE:
				break;
			}
		}

	AfxTrace("mark %4.4X\n", Head.m_Mark);
	AfxTrace("size %4.4X\n", Head.m_Size);
	}

// End of File
