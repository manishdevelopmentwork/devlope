
#include "Intern.hpp"

#include "WebServerNavTreeWnd.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "WebPage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Server Navigation Window
//

// Dynamic Class

AfxImplementDynamicClass(CWebServerNavTreeWnd, CNavTreeWnd);

// Constructor

CWebServerNavTreeWnd::CWebServerNavTreeWnd(void) : CNavTreeWnd( L"Pages",
						   NULL,
						   AfxRuntimeClass(CWebPage)
						   )
{
	m_Empty     = CString(IDS_CLICK_ON_NEW_4);

	m_fInitRoot = TRUE;
	}

// Message Map

AfxMessageMap(CWebServerNavTreeWnd, CNavTreeWnd)
{
	AfxDispatchMessage(WM_LOADTOOL)

	AfxDispatchGetInfoType(IDM_ITEM,  OnItemGetInfo)
	AfxDispatchControlType(IDM_ITEM,  OnItemControl)
	AfxDispatchCommandType(IDM_ITEM,  OnItemCommand)

	AfxMessageEnd(CWebServerNavTreeWnd)
	};

// Message Handlers

void CWebServerNavTreeWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
	if( uCode == 2 ) {

		Menu.AppendMenu(CMenu(L"WebServerNavTreeTool"));
		}
	}

// Command Handlers

BOOL CWebServerNavTreeWnd::OnItemGetInfo(UINT uID, CCmdInfo &Info)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Info.m_Image   = 0x40000009;

			Info.m_ToolTip = CString(IDS_NEW_PAGE);

			Info.m_Prompt  = CString(IDS_ADD_NEW_PAGE_TO);

			return TRUE;
		}

	return FALSE;
	}

BOOL CWebServerNavTreeWnd::OnItemControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			Src.EnableItem(!IsReadOnly());

			return TRUE;
		}

	return FALSE;
	}

BOOL CWebServerNavTreeWnd::OnItemCommand(UINT uID)
{
	switch( uID ) {

		case IDM_ITEM_NEW:

			OnItemNew();

			return TRUE;
		}

	return FALSE;
	}

void CWebServerNavTreeWnd::OnItemNew(void)
{
	CString Root = GetRoot(TRUE);

	for( UINT n = 1;; n++ ) {

		CFormat Name(CString(IDS_FORMAT_9), n);

		if( !m_MapNames[Root + Name] ) {

			CCmd *pCmd = New CCmdCreate(m_pSelect, Name, m_Class);

			m_System.ExecCmd(pCmd);

			break;
			}
		}
	}

// Tree Loading

void CWebServerNavTreeWnd::LoadImageList(void)
{
	m_Images.Create(16, 16, ILC_COLORDDB | ILC_MASK, 16, 256);

	m_Images.AddMasked(CBitmap(L"TagsTreeIcon16"), afxColor(MAGENTA));
	}

// Item Hooks

UINT CWebServerNavTreeWnd::GetRootImage(void)
{
	return IDI_WEB_SERVER;
	}

UINT CWebServerNavTreeWnd::GetItemImage(CMetaItem *pItem)
{
	return IDI_WEB_PAGE;
	}

BOOL CWebServerNavTreeWnd::GetItemMenu(CString &Name, BOOL fHit)
{
	if( fHit ) {

		Name = L"WebNavTreeCtxMenu";

		return TRUE;
		}

	Name = L"WebNavTreeMissCtxMenu";

	return FALSE;
	}

// End of File
