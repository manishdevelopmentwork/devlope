#ifndef	INCLUDE_ML1TTEST_HPP
	
#define	INCLUDE_ML1TTEST_HPP

#include "../ML1Ser/ml1serv.hpp"

#include "../ML1Ser/ml1base.cpp"

#include "../ML1Ser/ml1serv.cpp"

#include "../ML1Ser/ml1blk.cpp"

//////////////////////////////////////////////////////////////////////////
//
//  Debugging Help
//

static BOOL ML1_TCP_TEST = 0;

//////////////////////////////////////////////////////////////////////////
//
// ML1 TCP/IP Tester Implementation
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CML1Tester : public CML1Server
{
	public:
		// Constructor
		CML1Tester(void);

		// Destructor
		~CML1Tester(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// User Access
		DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value); 
		DEFMETH(UINT) DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
				
	protected:

		// Help
		IExtraHelper *	m_pExtra;

		// Implementation
		BOOL SendBlockRequest(CML1Dev * pDev, CML1Blk * pBlock, BYTE bOp);
		BOOL DoBlockRead(CML1Dev * pDev, CML1Blk * pBlock);
		BOOL DoBlockWrite(CML1Dev * pDev, CML1Blk * pBlock, UINT uOffset = 0, UINT uCount = 0);
				
		// Read Handlers
		BOOL DoWordRead(CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoLongRead(CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoBitRead (CML1Dev * pDev, AREF Addr, UINT uCount);
		BOOL DoFileRead(CML1Dev * pDev, AREF Addr, UINT uCount);

		// Write Handlers
		BOOL DoWordWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoLongWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoBitWrite (CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);
		BOOL DoFileWrite(CML1Dev * pDev, AREF Addr, PBYTE pData, UINT uCount);

		// Authentication Support
		BOOL GetMacId(void);
		BOOL IsAuthenticated(CML1Dev * pDev);
		BOOL IsAuthPollTimedOut(CML1Dev * pDev);
		void SetAuthPoll(CML1Dev * pDev);
		BOOL PshAuthentication(CML1Dev * pDev);
		BOOL FrcAuthentication(CML1Dev * pDev, UINT uCount);
		
	};

//////////////////////////////////////////////////////////////////////////
//
//  ML1 TCP Tester
//

class CML1TcpTester : public CML1Tester
{
	public:
		// Constructor
		CML1TcpTester(void);

		// Destructor
		~CML1TcpTester(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
							
	
	protected:

		struct SOCKET
		{
			ISocket * m_pSocket;
			BYTE	  m_pBuff[600];
			UINT	  m_uIdle;
			};

		// Device Data
		struct CML1Tcp : public CML1Dev
		{
			DWORD	 m_IP1;
			DWORD	 m_IP2;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			SOCKET * m_pClient;
			UINT	 m_uLast;
			BOOL     m_fDirty;
			BOOL	 m_fAux;
			BYTE	 m_bSockets;
			SOCKET * m_pListen;
			BOOL     m_fClose;
			PCTXT	 m_pAddr;
			BOOL	 m_fRemote;
			BYTE	 m_bIpSel;
			};
		
		// Data Members
		CML1Tcp *	m_pTcp;
		BOOL		m_fTrack;
		UINT		m_uKeep;

		// Special Block Support
		void InitSpecial(void);
		void SetSr(CML1Dev * pDev, UINT& uOffset, PWORD pData);
		void AddSpecial(CML1Dev * pDev, BYTE bCount);
		BYTE GetSpecialExtra(void);
		BYTE FindSpecialWriteCount(UINT uOffset);
		
		// Socket Management
		BOOL CheckSocket(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen);
		BOOL CheckSocket(CML1Tcp * pCtx);
		BOOL OpenSocket (CML1Tcp * pCtx);
		void CloseSocket(CML1Tcp * pCtx, BOOL fAbort);
		BOOL CheckListener(SOCKET * pSock);
		BOOL OpenListener (SOCKET * pSock);
		void CloseListener(SOCKET * pSock);
		void CheckIdle  (CML1Tcp * pCtx);
		void CheckSilent(CML1Tcp * pCtx, SOCKET * pSock);

		// Implementation
		void InitTcp(void);
		BOOL TxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fRespond);
		BOOL RxData(CML1Tcp * pCtx, SOCKET * pSock, BOOL fListen);

		// Transport
	virtual BOOL Send(void);
	virtual BOOL Recv(void);
	virtual BOOL Listen(void);	
	virtual BOOL Respond(PBYTE pBuff);
	virtual BOOL Respond(CML1Dev * pDev);
		
		// Transport Helpers
		UINT  FindRxSize(void);
		PVOID FindContext(void);

		// Helpers
		BOOL SetIP(CML1Tcp * pTcp, CML1Blk * pBlock, DWORD dwData);
		
	};



#endif

// End of File
