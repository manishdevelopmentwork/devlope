
#include "Intern.hpp"

#include "UITagSet.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "TagSetWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Tag Set
//

// Dynamic Class

AfxImplementDynamicClass(CUITagSet, CUIControl);

// Constructor

CUITagSet::CUITagSet(void)
{
	m_pTreeLayout = NULL;

	m_pDataWindow = New CTagSetWnd(this);
	}

// Core Overidables

void CUITagSet::OnBind(void)
{
	CUIControl::OnBind();

	m_pDataWindow->Attach(m_pData->GetObject(m_pItem));
	}

void CUITagSet::OnRebind(void)
{
	CUIControl::OnRebind();

	m_pDataWindow->Attach(m_pData->GetObject(m_pItem));
	}

void CUITagSet::OnLayout(CLayFormation *pForm)
{
	m_pTreeLayout = New CLayItemSize(CSize(300, 250), CSize(4, 4), CSize(2, 100));

	m_pMainLayout = New CLayFormPad (m_pTreeLayout, horzLeft | vertCenter);

	pForm->AddItem(New CLayItem());

	pForm->AddItem(m_pMainLayout);
	}

void CUITagSet::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pDataWindow->Create( WS_CHILD | WS_TABSTOP | WS_CLIPCHILDREN,
			       GetDataWindowRect(),
			       Wnd,
			       uID++
			       );

	m_pDataWindow->SetFont(afxFont(Dialog));

	AddControl(m_pDataWindow);
	}

void CUITagSet::OnPosition(void)
{
	m_pDataWindow->MoveWindow(GetDataWindowRect(), TRUE);
	}

// Data Overridables

void CUITagSet::OnLoad(void)
{
	CString Data = m_pText->GetAsText();

	m_pDataWindow->LoadTree(Data);
	}

UINT CUITagSet::OnSave(BOOL fUI)
{
	CString Data = m_pDataWindow->SaveTree();

	CString Last = m_pText->GetAsText();

	if( Data.CompareC(Last) ) {

		CError Error(fUI);

		UINT uCode = m_pText->SetAsText(Error, Data);

		if( !Error.IsOkay() ) {

			if( Error.AllowUI() ) {

				Error.Show(*afxMainWnd);
				}
			}

		return uCode;
		}

	return saveSame;
	}

// Notification Handler

BOOL CUITagSet::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_pDataWindow->GetID() ) {

		if( uCode == IDM_UI_CHANGE ) {

			OnSave(TRUE);

			return TRUE;
			}
		}

	return CUIControl::OnNotify(uID, uCode);
	}

// Implementation

CRect CUITagSet::GetDataWindowRect(void)
{
	CRect Rect;

	Rect = m_pTreeLayout->GetRect();

	Rect.top    -= 1;

	Rect.bottom += 1;

	return Rect;
	}

// End of File
