
#include "Intern.hpp"

#include "UsbDevNetExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb DeviceNet Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbDevNetExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionSerial *) New CUsbDevNetExpansion(pDriver);

	return p;
	}

// Constructor

CUsbDevNetExpansion::CUsbDevNetExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansionSerial(pDriver)
{
	}

// IExpansionInterface

PCTXT METHOD CUsbDevNetExpansion::GetName(void)
{
	return "GMDN";
	}

UINT METHOD CUsbDevNetExpansion::GetClass(void)
{
	return rackClassComms;
	}

UINT METHOD CUsbDevNetExpansion::GetPower(void)
{
	return 12;
	}

BOOL METHOD CUsbDevNetExpansion::HasBootLoader(void)
{
	return TRUE;
	}

IDevice * METHOD CUsbDevNetExpansion::MakeObject(IUsbHostFuncDriver *pDriver)
{
	return Create_UsbDevNet(pDriver);
	}

// IExpansionSerial

UINT METHOD CUsbDevNetExpansion::GetPortCount(void)
{
	return 1;
	}

DWORD METHOD CUsbDevNetExpansion::GetPortMask(void)
{
	return Bit(physicalRS485);
	}

UINT METHOD CUsbDevNetExpansion::GetPortType(UINT iPort)
{
	return rackDevNet;
	}

// End of File
