
//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "../Df1Shared/df1ms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CABLogix5SerialDriver;

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Logix5000 Driver - Serial Master
//

class CABLogix5SerialMaster : public CDF1SerialMaster
{
	public:
		// Constructor
		CABLogix5SerialMaster(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Frame Building
		void AddAddress(AREF Addr);
		void AddType(AREF Addr, UINT uCount);

		// Tag Name Support
		void LoadTagNames(PCBYTE pData);

		// Text Loading
		PTXT GetString(PCBYTE &pData);
	};

// End of File
