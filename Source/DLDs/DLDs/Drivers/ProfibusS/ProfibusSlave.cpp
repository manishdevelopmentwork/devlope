
#include "intern.hpp"

#include "ProfibusSlave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Profibus-DP Slave Driver
//

// Instantiator

INSTANTIATE(CProfibusDPSlave);

// Constructor

CProfibusDPSlave::CProfibusDPSlave(void)
{
	m_Ident = DRIVER_ID;
	
	m_pPort = NULL;
	}

// Destructor

CProfibusDPSlave::~CProfibusDPSlave(void)
{
	}

// Configuration

void MCALL CProfibusDPSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bStation = GetByte(pData);
		}
	}

// Management

void MCALL CProfibusDPSlave::Attach(IPortObject *pPort)
{
	m_pPort = (IProfibusPort *) pPort;
	}

void MCALL CProfibusDPSlave::Detach(void)
{
	}

// Entry Point

void MCALL CProfibusDPSlave::Service(void)
{
	for(;;) {

		//m_pPort->Poll();

		if( m_pPort->IsOnline() ) {

			CheckWrites();

			CheckReads();
			}
	
		ForceSleep(50);
		}
	}

// Implementation

void CProfibusDPSlave::CheckWrites(void)
{
	}

void CProfibusDPSlave::CheckReads(void)
{
	}

// End of File
