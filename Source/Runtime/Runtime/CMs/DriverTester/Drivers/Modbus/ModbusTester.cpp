
#include "Intern.hpp"

#include "ModbusTester.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver Tester
//

// Instantiator

INSTANTIATE(CModbusDriverTester);

// Constructor

CModbusDriverTester::CModbusDriverTester(void)
{
	}

// Destructor

CModbusDriverTester::~CModbusDriverTester(void)
{
	}

// IDriverTester

BOOL CModbusDriverTester::GetSerialConfig(CSerialConfig &Config)
{
	Config.m_uPhysical = physicalRS232;
	Config.m_uPhysMode = 0;
	Config.m_uBaudRate = 115200;
	Config.m_uDataBits = 8;
	Config.m_uStopBits = 1;
	Config.m_uParity   = parityNone;
	Config.m_uFlags    = flagNone;
	Config.m_bDrop     = 0;	

	return TRUE;
	}

BOOL CModbusDriverTester::GetDriverConfig(PBYTE &pData)
{
	AddByte(pData, 0);	// Ascii
	AddByte(pData, 0);	// Track
	AddWord(pData, 1000);	// Timeout

	return TRUE;
	}

BOOL CModbusDriverTester::GetDeviceConfig(PBYTE &pData)
{
	AddByte(pData, 1);	// Drop      
	AddByte(pData, 0);	// RLCAuto   
	AddByte(pData, 0);	// Disable15 
	AddByte(pData, 0);	// Disable16 
	AddWord(pData, 512);	// Max01     
	AddWord(pData, 512);	// Max02     
	AddWord(pData, 32);	// Max03     
	AddWord(pData, 32);	// Max04     
	AddWord(pData, 512);	// Max15     
	AddWord(pData, 32);	// Max16     
	AddWord(pData, 1);	// Ping	   
	AddByte(pData, 0);	// Disable5  
	AddByte(pData, 0);	// Disable6  
	AddByte(pData, 0);	// NoCheck   
	AddByte(pData, 0);	// NoReadEx  
	AddWord(pData, 0);	// Poll	   
	AddByte(pData, 0);	// SwapCRC   
	AddByte(pData, 0);	// FlipLong  
	AddByte(pData, 0);	// FlipReal  
	AddByte(pData, 1);	// WriteReply

	return TRUE;
	}

BOOL CModbusDriverTester::ExecDriverCtrl(ICommsDriver *pDriver)
{
	pDriver->DrvCtrl(1, "1");

	return TRUE;
	}

BOOL CModbusDriverTester::ExecDeviceCtrl(ICommsDriver *pDriver)
{
	return TRUE;
	}

BOOL CModbusDriverTester::ExecTest(CCODE &Code, ICommsMaster *pDriver, UINT uIndex)
{
	if( uIndex == 0 ) {

		CAddress Addr;

		Addr.a.m_Table  = 0x01;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1;

		DWORD Data[4];

		Code = pDriver->Read(Addr, Data, elements(Data));

		ShowRead(Code, uIndex, Addr, Data, elements(Data), TRUE);

		return TRUE;
		}

	if( uIndex == 1 ) {

		static WORD n = 0;

		CAddress Addr;

		Addr.a.m_Table  = 0x01;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		Addr.a.m_Offset = 1;

		DWORD Data[1] = { n++ };

		Code = pDriver->Write(Addr, Data, elements(Data));

		ShowWrite(Code, uIndex, Addr, Data, elements(Data), FALSE);

		return TRUE;
		}

	return FALSE;
	}

// End of File
