
#include "intern.hpp"

#include "CognexCameraDeviceOptions.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cognex Camera Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCognexCameraDeviceOptions, CUIItem);

// Constructor

CCognexCameraDeviceOptions::CCognexCameraDeviceOptions(void)
{
	m_Camera = 0;

	m_IP     = DWORD(MAKELONG(MAKEWORD(50, 15), MAKEWORD(168, 192)));

	m_Port   = 23;

	m_Time4  = 10;

	m_User = "admin";

	m_Pass = "";
	}


// Download Support

BOOL CCognexCameraDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Camera));

	Init.AddLong(LONG(m_IP));
	
	Init.AddWord(WORD(m_Port));

	Init.AddWord(WORD(m_Time4));

	Init.AddText(m_User);

	Init.AddText(m_Pass);

	return TRUE;
	}


// Meta Data Creation

void CCognexCameraDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Camera);

	Meta_AddInteger(IP);

	Meta_AddInteger(Port);

	Meta_AddInteger(Time4);

	Meta_AddString(User);

	Meta_AddString(Pass);

	}

// Persistence

void CCognexCameraDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		if(Name == "Device") {

			m_Camera = Tree.GetValueAsInteger();

			continue;
			}

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// End of File
