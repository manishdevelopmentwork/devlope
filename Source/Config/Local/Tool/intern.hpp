
//////////////////////////////////////////////////////////////////////////
//								
// Warning Control
//

#include "level4.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// Header Control
//

#define	_CRT_SECURE_NO_WARNINGS	TRUE

#define	NO_STRICT		TRUE

#define	_WIN32_WINNT		0x0400

#define	WINVER			0x0400

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <winsock2.h>
#include <olectl.h>
#include <ctype.h>
#include <eh.h>
#include <errno.h>
#include <fcntl.h>
#include <io.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <comcat.h>
#include <conio.h>
#include <malloc.h>

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#define	DLLAPI

//////////////////////////////////////////////////////////////////////////
//								
// Warning Control
//

#include "level4.hpp"

//////////////////////////////////////////////////////////////////////////
//								
// General Typedefs
//

typedef	char *		PTXT;

typedef char const *	PCTXT;

typedef	char		CHAR;

typedef int		INT;

typedef	void *		PVOID;

typedef void const *	PCVOID;

typedef BYTE const *	PCBYTE;

typedef DWORD const *	PCDWORD;

typedef struct _xx_ *	INDEX;

typedef __int64		INT64;

//////////////////////////////////////////////////////////////////////////
//
// General Macros
//

#define	global		/**/

#define	NOTHING		UINT(-1)

#define	elements(x)	(sizeof(x) / sizeof((x)[0]))

#define	offset(t, x)	UINT(&(((t *) 0)->x))

#define	CTEXT		static char 

#define	CLINK		extern "C"

#define	BEGIN		do

#define	END		while(0)

#define	BREAKPOINT	BEGIN { __asm int 3 } END

#define	METHOD		STDMETHODCALLTYPE

#define	HRM		HRESULT METHOD

#define	VHRM		virtual HRESULT METHOD

#define	AfxTouch(x)	((void) x)

#define	TLS		__declspec(thread)

#define	ThisObject	(*this)

#define	CMakeUnicode	CUnicode	

#define	AfxFileHeader()	/**/

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#define	MakeMin(a, b)	BEGIN { if( (b) < (a) ) (a) = (b); } END

#define	MakeMax(a, b)	BEGIN { if( (b) > (a) ) (a) = (b); } END

#define	Min(a, b)	(((a) < (b)) ? (a) : (b))

#define	Max(a, b)	(((a) > (b)) ? (a) : (b))

//////////////////////////////////////////////////////////////////////////
//
// Utility Templates
//

template <typename type> inline void Swap(type &a, type &b)
{
	type c = a; a = b; b = c;
	}

template <typename type> inline int AfxCompare(type const &a, type const &b)
{
	return (a > b) ? +1 : ((b > a ) ? -1 : 0);
	}

//////////////////////////////////////////////////////////////////////////
//
// GUID Comparison
//

inline int AfxCompare(GUID const &a, GUID const &b)
{
	return memcmp(&a, &b, sizeof(GUID));
	}

//////////////////////////////////////////////////////////////////////////
//
// Linked List Macros
//

#define	AfxListAppend(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->p = t; o->n = NULL;			\
	t ? (t->n = o) : (h = o);		\
	t = o;					\
	} END					\

#define	AfxListInsert(h, t, o, n, p, b)		\
						\
	BEGIN {					\
	o->p = b ? b->p : t;			\
	o->n = b;				\
	o->p ? (o->p->n = o) : (h = o);		\
	o->n ? (o->n->p = o) : (t = o);		\
	} END					\

#define	AfxListRemove(h, t, o, n, p)		\
						\
	BEGIN {					\
	o->n ? (o->n->p = o->p) : (t = o->p);	\
	o->p ? (o->p->n = o->n) : (h = o->n);	\
	} END					\

//////////////////////////////////////////////////////////////////////////
//
// Memory Diagnostic Functions
//

inline BOOL AfxCheckBlock(void *pData) { return TRUE; }
inline BOOL AfxCheckMemory(void) { return TRUE; }
inline void AfxDumpBlock(void *pData) { }
inline UINT AfxMarkMemory(void) { return 0; }
inline BOOL AfxDumpMemory(UINT uSince) { return TRUE; }

//////////////////////////////////////////////////////////////////////////
//
// General Diagnostic Functions
//

inline void AfxTraceArgs(PCTXT pText, va_list pArgs) { }
inline void AfxTrace(PCTXT pText, ...) { }
inline void AfxLog(PCTXT pText) { }
inline BOOL AfxAborting(void) { return FALSE; }
inline void AfxAbort(void) { FatalExit(0); }

//////////////////////////////////////////////////////////////////////////
//
// Assertion Macros
//

#define AfxAssert(x)	BEGIN { __assume(x); ((void) (0)); } END

#define	AfxVerify(x)	BEGIN { __assume(x); ((void) (x)); } END

//////////////////////////////////////////////////////////////////////////
//
// Pointer Validation
//

inline void AfxValidateReadPtr(void const *pData, UINT uSize = 1) { }
inline void AfxValidateWritePtr(void *pData, UINT uSize = 1) { }
inline void AfxValidateStringPtr(PCTXT pText, UINT uSize = 1) { }
inline void AfxValidateResourceName(PCTXT pText, UINT uSize = 1) { }
inline void AfxValidateObject(class CObject const *pObject) { }
inline void AfxValidateObject(class CObject const &Object) { }

//////////////////////////////////////////////////////////////////////////
//
// Debugging Allocator
//

#define	New		new

#define	tp_new		new

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CEntity;
class CModule;
class CString;

//////////////////////////////////////////////////////////////////////////
//								
// Current Module
//

extern CModule * afxModule;

//////////////////////////////////////////////////////////////////////////
//
// Collection Templates
//

#include "named.hpp"

#include "scalar.hpp"

#include "tree.hpp"

#include "map.hpp"

#include "array.hpp"

#include "linked.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Collections
//

typedef CArray <class CString> CStringArray;

typedef CArray <BYTE>          CByteArray;

typedef CArray <WORD>          CWordArray;

//////////////////////////////////////////////////////////////////////////
//								
// Forward Declarations
//

class CStrPtr;
class CString;

//////////////////////////////////////////////////////////////////////////
//
// String Information
//

struct DLLAPI CStringData
{
	UINT	m_uAlloc;
	UINT	m_uLen;
	int	m_nRefs;
	UINT	m_uDummy;
	char	m_cData[];
	};

//////////////////////////////////////////////////////////////////////////
//
// Simple String Pointer
//

class DLLAPI CStrPtr
{
	public:
		// Constructors
		CStrPtr(CStrPtr const &That);
		CStrPtr(CString const &That);
		CStrPtr(PCTXT pText);

		// Conversion
		operator PCTXT (void) const;
		
		// Attributes
		BOOL IsEmpty(void) const;
		BOOL operator ! (void) const;
		UINT GetLength(void) const;
		UINT GetHashValue(void) const;
		
		// Read-Only Access
		char GetAt(UINT uIndex) const;
		
		// Comparison Functions
		int CompareC(PCTXT pText) const;
		int CompareN(PCTXT pText) const;
		int CompareC(PCTXT pText, UINT uCount) const;
		int CompareN(PCTXT pText, UINT uCount) const;
		int GetScore(PCTXT pText) const;

		// Comparison Helper
		friend DLLAPI int AfxCompare(CStrPtr const &a, CStrPtr const &b);

		// Comparison Operators
		BOOL operator == (PCTXT pText) const;
		BOOL operator != (PCTXT pText) const;
		BOOL operator <  (PCTXT pText) const;
		BOOL operator >  (PCTXT pText) const;
		BOOL operator <= (PCTXT pText) const;
		BOOL operator >= (PCTXT pText) const;

		// Substring Extraction
		CString Left(UINT uCount) const;
		CString Right(UINT uCount) const;
		CString Mid(UINT uPos, UINT uCount) const;
		CString Mid(UINT uPos) const;

		// Charset Conversion
		CString ToOem(void) const;
		CString ToAnsi(void) const;

		// Case Conversion
		CString ToUpper(void) const;
		CString ToLower(void) const;

		// Searching
		UINT Find(char cChar) const;
		UINT Find(char cChar, UINT uPos) const;
		UINT FindRev(char cChar) const;
		UINT FindRev(char cChar, UINT uPos) const;
		UINT Find(PCTXT pText) const;
		UINT Find(PCTXT pText, UINT uPos) const;
		UINT FindNot(PCTXT pList) const;
		UINT FindNot(PCTXT pList, UINT uPos) const;
		UINT FindOne(PCTXT pList) const;
		UINT FindOne(PCTXT pList, UINT uPos) const;

		// Counting
		UINT Count(char cChar) const;

		// Diagnostics
		void AssertValid(void) const;
		
	protected:
		// Data Members
		PTXT m_pText;
		
		// Protected Constructors
		CStrPtr(void) { }
	};

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

class DLLAPI CString : public CStrPtr
{
	public:
		// Constructors
		CString(void);
		CString(CString const &That);
		CString(PCTXT pText, UINT uCount);
		CString(char cData, UINT uCount);
		CString(GUID const &Guid);
		CString(LPOLESTR pText);
		CString(PCTXT pText);
		
		// Destructor
		~CString(void);
		
		// Assignment Operators
		CString const & operator = (CString const &That);
		CString const & operator = (LPOLESTR pText);
		CString const & operator = (PCTXT pText);
		
		// Attributes
		UINT GetLength(void) const;
		
		// Read-Only Access
		char GetAt(UINT uIndex) const;
		
		// Substring Extraction
		CString Left(UINT uCount) const;
		CString Right(UINT uCount) const;
		CString Mid(UINT uPos, UINT uCount) const;
		CString Mid(UINT uPos) const;

		// Buffer Managment
		void Empty(void);
		void Expand(UINT uAlloc);
		void Compress(void);
		void CopyOnWrite(void);
		void FixLength(void);

		// Concatenation In-Place
		CString const & operator += (CString const &That);
		CString const & operator += (PCTXT pText);
		CString const & operator += (char cData);

		// Concatenation via Friends
		friend DLLAPI CString operator + (CString const &A, CString const &B);
		friend DLLAPI CString operator + (CString const &A, PCTXT pText);
		friend DLLAPI CString operator + (CString const &A, char cData);
		friend DLLAPI CString operator + (PCTXT pText, CString const &B);
		friend DLLAPI CString operator + (char cData, CString const &B);

		// Write Data Access
		void SetAt(UINT uIndex, char cData);

		// Comparison Helper
		friend DLLAPI int AfxCompare(CString const &a, CString const &b);

		// Insert and Remove
		void Insert(UINT uIndex, PCTXT pText);
		void Insert(UINT uIndex, CString const &That);
		void Insert(UINT uIndex, char cData);
		void Delete(UINT uIndex, UINT uCount);

		// Whitespace Trimming
		void TrimLeft(void);
		void TrimRight(void);
		void TrimBoth(void);
		void StripAll(void);

		// Charset Switching
		void MakeOem(void);
		void MakeAnsi(void);

		// Case Switching
		void MakeUpper(void);
		void MakeLower(void);

		// Replacement
		UINT Replace(char  cFind, char  cNew);
		UINT Replace(PCTXT pFind, PCTXT pNew);

		// Removal
		void    Remove(char cFind);
		CString Without(char cFind);
		
		// Parsing
		UINT    Tokenize(CStringArray &Array, char  cFind) const;
		UINT    Tokenize(CStringArray &Array, PCTXT pFind) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, char  cFind) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, PCTXT pFind) const;
		CString StripToken(char  cFind);
		CString StripToken(PCTXT pFind);
		
		// Printf Formatting
		void Printf(PCTXT pFormat, ...);
		void Printf(CEntity Format, ...);
		void VPrintf(PCTXT pFormat, PTXT pArgs);

		// Windows Formatting
		void Format(PCTXT pFormat, ...);
		void Format(CEntity Format, ...);
		void VFormat(PCTXT pFormat, PTXT pArgs);

		// Resource Name Encoding
		void SetResourceName(PCTXT pName);

		// Diagnostics
		void AssertValid(void) const;
		
	protected:
		// Static Data
		static CStringData m_Empty;

		// Protected Constructor
		CString(PCTXT p1, UINT u1, PCTXT p2, UINT u2);

		// Initialisation
		void InitFrom(LPOLESTR pText);
		
		// Implementation
		void Alloc(UINT uLen, UINT uAlloc);
		void Alloc(UINT uLen);
		void GrowString(UINT uLen);
		void Release(void);
		UINT AdjustSize(UINT uAlloc);
		PTXT IntPrintf(PCTXT pFormat, PTXT pArgs);
		PTXT IntFormat(PCTXT pFormat, PTXT pArgs);
	};

// End of File
