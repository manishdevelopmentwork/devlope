
#include "intern.hpp"

#include "testing.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Test Application
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Test Function
//

void TestFunc(void)
{
	(New CTestApp)->Execute();
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Application
//

// Runtime Class

AfxImplementRuntimeClass(CTestApp, CThread);

// Constructor

CTestApp::CTestApp(void)
{
	}

// Destructor

CTestApp::~CTestApp(void)
{
	}

// Overridables

BOOL CTestApp::OnInitialize(void)
{
	CWnd *pWnd = New CTestWnd;

	CPoint Pos = CPoint(280, 180);

	CSize Size = CSize (586, 400);

	CRect Rect = CRect (Pos, Size);

	pWnd->Create( L"Test Application",
		      WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN,
		      Rect, AfxNull(CWnd), CMenu(L"Test"),
		      NULL
		      );

	pWnd->ShowWindow(afxModule->GetShowCommand());

	return TRUE;
	}

BOOL CTestApp::OnTranslateMessage(MSG &Msg)
{
	return FALSE;
	}

void CTestApp::OnException(EXCEPTION Ex)
{
	MessageBeep(0);
	}

// Message Map

AfxMessageMap(CTestApp, CThread)
{
	AfxDispatchMessage(WM_GOINGIDLE)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestApp)
	};

// Message Handlers

void CTestApp::OnGoingIdle(void)
{
	}

// Command Handlers

BOOL CTestApp::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_EXIT ) {
	
		afxMainWnd->DestroyWindow(FALSE);

		return TRUE;
		}
		
	return FALSE;
	}

BOOL CTestApp::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_EXIT ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Window
//

// Dynamic Class

AfxImplementDynamicClass(CTestWnd, CMenuWnd);

// Constructor

CTestWnd::CTestWnd(void)
{
	m_Accel.Create(L"Test");

	m_pEdit = New CEditCtrl;
	}

// Destructor

CTestWnd::~CTestWnd(void)
{
	}

// Message Map

AfxMessageMap(CTestWnd, CMenuWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_SETFOCUS)

	AfxDispatchCommandType(IDM_FILE, OnCommandExecute)
	AfxDispatchControlType(IDM_FILE, OnCommandControl)

	AfxMessageEnd(CTestWnd)
	};

// Message Handlers

BOOL CTestWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

void CTestWnd::OnPostCreate(void)
{
	CRect Rect = GetClientRect() - 4;

	m_pEdit->Create( WS_CHILD | WS_CLIPCHILDREN | ES_MULTILINE,
			 Rect, ThisObject, IDVIEW
			 );

	m_pEdit->SetFont(afxFont(Fixed));

	m_pEdit->ShowWindow(SW_NORMAL);
	}

void CTestWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);
	}

BOOL CTestWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = CWnd::GetClientRect();

	DC.DrawEdge(Rect, EDGE_SUNKEN, BF_RECT);

	Rect -= 2;

	DC.FrameRect(Rect, afxBrush(WHITE));

	Rect -= 1;

	DC.FrameRect(Rect, afxBrush(WHITE));

	return TRUE;
	}

void CTestWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		CRect Rect = GetClientRect() - 4;

		m_pEdit->SetWindowPos(Rect, TRUE);
		}
	}

void CTestWnd::OnSetFocus(CWnd &Wnd)
{
	m_pEdit->SetFocus();
	}

// Command Handlers

BOOL CTestWnd::OnCommandExecute(UINT uID)
{
	if( uID == IDM_FILE_NEW ) {

		CTestName * pName = New CTestName;

		CString     Text  = m_pEdit->GetWindowText();

		CByteArray  Object;

		CByteArray  Source;

		CCompileIn  In;

		In.m_pName        = pName;
		In.m_pParam       = NULL;
		In.m_uParam       = 0;
		In.m_pText        = Text;
		In.m_Type.m_Type  = typeNumeric;
		In.m_Type.m_Flags = flagInherent;
		In.m_uOptim       = 2;

		CCompileOut Out;

		Out.m_pSource  = &Source;
		Out.m_pRefList = NULL;
		Out.m_pObject  = &Object;

		if( !C3CompileFunc(In, Out) ) {

			Out.m_Error.Show(ThisObject);

			m_pEdit->SetSel(Out.m_Range);
			}
		else {
			Text.Empty();

			C3ExpandSource(Source.GetPointer(), pName, In.m_Using, Text);

			m_pEdit->SetWindowText(Text);

			/*CTestData * pData = New CTestData;

			C3ExecuteCode(Object.GetPointer(), pData, NULL);

			pData->Dump();

			delete pData;*/
			}

		delete pName;

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestWnd::OnCommandControl(UINT uID, CCmdSource &Src)
{
	if( uID == IDM_FILE_NEW ) {

		Src.EnableItem(TRUE);

		return TRUE;
		}
		
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Name Server
//

// Constructor

CTestName::CTestName(void)
{
	C3MakeFuncLib(m_pLib);

	CError Error(TRUE);

	m_pLib->AddFunc(Error, NULL, 0x8000, L"void Test(int &ref)");
	}

// Destructor

CTestName::~CTestName(void)
{
	m_pLib->Release();
	}

// IBase

UINT CTestName::Release(void)
{
	delete this;

	return 0;
	}

// INameServer

BOOL CTestName::FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	if( Name == L"Fred" ) {

		ID = 1;

		Type.m_Type  = typeObject;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	if( Name == L"Fred.Jim" ) {

		ID = 2;

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	if( Name == L"Bob" ) {

		ID = 3;

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagWritable;

		return TRUE;
		}

	if( Name == L"Big" ) {

		ID = 4;

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagArray | flagExtended | flagWritable;

		return TRUE;
		}
 
	if( Name == L"Small" ) {

		ID = 5;

		Type.m_Type  = typeInteger;

		Type.m_Flags = flagArray | flagWritable;

		return TRUE;
		}
 
	return FALSE;
	}

BOOL CTestName::NameIdent(WORD ID, CString &Name)
{
	if( ID == 1 ) {

		Name = L"Fred";

		return TRUE;
		}

	if( ID == 2 ) {

		Name = L"Fred.Jim";

		return TRUE;
		}

	if( ID == 3 ) {

		Name = L"Bob";

		return TRUE;
		}

	if( ID == 4 ) {

		Name = L"Big";

		return TRUE;
		}

	if( ID == 5 ) {

		Name = L"Small";

		return TRUE;
		}

	return FALSE;
	}

BOOL CTestName::FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type)
{
	if( ID == 1 ) {

		if( Name == L"" ) {

			PropID = 999;

			Type.m_Type  = typeInteger;

			Type.m_Flags = 0;

			return TRUE;
			}
		}

	if( ID == 3 || ID == 2 ) {

		if( Name == L"LowLimit" ) {

			PropID = 1;

			Type.m_Type  = typeInteger;

			Type.m_Flags = 0;

			return TRUE;
			}

		if( Name == L"HighLimit" ) {

			PropID = 2;

			Type.m_Type  = typeInteger;

			Type.m_Flags = 0;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTestName::NameProp(WORD ID, WORD PropID, CString &Name)
{
	if( ID == 3 || ID == 2 ) {

		if( PropID == 1 ) {

			Name = L"LowLimit";

			return TRUE;
			}

		if( PropID == 2 ) {

			Name = L"HighLimit";

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTestName::FindDirect(CError *pError, CString Name, DWORD &ID, CTypeDef &Type)
{
	return FALSE;
	}

BOOL CTestName::NameDirect(DWORD ID, CString &Name)
{
	return FALSE;
	}

BOOL CTestName::FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount)
{
	return m_pLib->FindFunction(pError, Name, ID, Type, uCount);
	}

BOOL CTestName::NameFunction(WORD ID, CString &Name)
{
	return m_pLib->NameFunction(ID, Name);
	}

BOOL CTestName::EditFunction(WORD &ID, CTypeDef &Type)
{
	return m_pLib->EditFunction(ID, Type);
	}

BOOL CTestName::GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type)
{
	return m_pLib->GetFuncParam(ID, uParam, Name, Type);
	}

BOOL CTestName::FindClass(CError *pError, CString Name, UINT &Type)
{
	return FALSE;
	}

BOOL CTestName::NameClass(UINT Type, CString &Name)
{
	Name = L"a folder";

	return TRUE;
	}

BOOL CTestName::SaveClass(UINT Type)
{
	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Test Data Source
//

// Constructor

CTestData::CTestData(void)
{
	memset(m_I, 0, sizeof(m_I));
	}

// Operations

void CTestData::Dump(void)
{
	for( UINT n = 0; n < elements(m_I); n++ ) {

		AfxTrace(L"%2u) %8.8X (%d)\n", n, m_I[n], m_I[n]);
		}
	}

// IData Server Methods

DWORD CTestData::GetData(DWORD ID, UINT Type)
{
	CDataRef const &Ref = (CDataRef const &) ID;

	return m_I[Ref.t.m_Index + Ref.t.m_Array];
	}

PVOID CTestData::GetItem(DWORD ID)
{
	return NULL;
	}

void CTestData::SetData(DWORD ID, UINT Type, DWORD dwData)
{
	CDataRef const &Ref = (CDataRef const &) ID;

	m_I[Ref.t.m_Index + Ref.t.m_Array] = dwData;
	}

DWORD CTestData::GetProp(DWORD ID, WORD Prop, UINT Type)
{
	return 0;
	}

DWORD CTestData::RunFunc(WORD ID, UINT uParam, PDWORD pParam)
{
	return 0;
	}

// End of File
