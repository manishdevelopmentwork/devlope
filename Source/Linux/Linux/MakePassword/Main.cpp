
#include "Intern.hpp"

#include "Words.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Password Generation
//
// Copyright (c) 2019-2020 Red Lion Controls
//
// All Rights Reserved
//

// Static Data

static	int	m_count = 1;

// Prototypes

global	int	main(int nArg, char *pArg[]);
static	bool	ParseCommandLine(int nArg, char *pArg[]);
static	void	Error(char const *p, ...);
static	void	ShowUsage(void);

// Code

int main(int nArg, char *pArg[])
{
	if( ParseCommandLine(nArg, pArg) ) {

		ATCAIfaceCfg dc;

		dc.iface_type            = ATCA_I2C_IFACE;
		dc.devtype               = ATECC608A;
		dc.atcai2c.baud          = 1000000;
		dc.atcai2c.bus           = 0;
		dc.atcai2c.slave_address = 0xC0;
		dc.wake_delay	         = 800;
		dc.rx_retries            = 3;
		dc.cfg_data              = NULL;

		if( !atcab_init(&dc) ) {

			for( int n = 0; n < m_count; n++ ) {

				BYTE rand[32] = { 0 };

				for( ;;) {

					if( !atcab_random(rand) ) {

						string w1 = words[((UINT *) rand)[0] % elements(words)];

						string w2 = words[((UINT *) rand)[1] % elements(words)];

						if( w1.size() + w2.size() > 16 ) {

							continue;
						}

						if( w2.substr(w2.size()-3) == "ing" ) {

							if( w1.substr(w1.size()-3) == "ing" ) {

								continue;
							}

							string w3 = w1;

							w1 = w2;

							w2 = w3;
						}

						int nn = ((UINT *) rand)[2] % 10000;

						printf("%s-%s-%4.4u\n", w1.c_str(), w2.c_str(), nn);

						break;
					}

					Error("cannot generate random data");
				}
			}

			return 0;
		}

		Error("cannot open security module");
	}

	return 1;
}

static bool ParseCommandLine(int nArg, char *pArg[])
{
	int c;

	while( (c = getopt(nArg, pArg, "n:")) != -1 ) {

		switch( c ) {

			case 'n':

				if( !optarg ) {

					Error("missing password count");
				}

				if( (m_count = atoi(optarg)) < 1 || m_count > 1000 ) {

					Error("invalid password count");
				}

				break;

			case '?':

				Error("syntax error");

				break;

			default:

				Error("unexpected switch %c", c);

				break;
		}
	}

	return true;
}

static void Error(char const *p, ...)
{
	va_list v;

	va_start(v, p);

	fprintf(stderr, "%s: ", "MakePasswod");

	vfprintf(stderr, p, v);

	fprintf(stderr, "\n");

	va_end(v);

	exit(1);
}

static void ShowUsage(void)
{
	fprintf(stderr, "usage: MakePassword [-n <count>]\n");
}

// End of File
