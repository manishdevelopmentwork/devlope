
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Graphics
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyVector_HPP
	
#define	INCLUDE_RubyVector_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "RubyTrig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Vector Object
//

class DLLAPI CRubyVector : public P2R
{
	public:
		// Constructors
		CRubyVector(void);
		CRubyVector(P2R const &p);
		CRubyVector(CRubyVector const &v);
		CRubyVector(number x, number y);
		CRubyVector(number t);

		// Attributes
		number GetLength(void) const;
		number GetInverseLength(void) const;
		number GetSquare(void) const;

		// Operations
		void MakeUnit(void);
		void MakeUnit(number ds);
		void MakeNormal(void);
		void MakeNormal(number ds);
		void Rotate90(void);
		void Multiply(number k);

		// Operators
		CRubyVector operator +  (CRubyVector const &v) const;
		CRubyVector operator -  (CRubyVector const &v) const;
		number      operator ^  (CRubyVector const &v) const;
		number      operator *  (CRubyVector const &v) const;
		CRubyVector operator << (CRubyVector const &v) const;
		CRubyVector operator >> (CRubyVector const &v) const;
		CRubyVector operator *  (number k) const;
		CRubyVector operator /  (number k) const;
		CRubyVector operator *  (int k) const;
		CRubyVector operator /  (int k) const;
		CRubyVector operator +  (void) const;
		CRubyVector operator -  (void) const;

		// Debugging
		void Trace(PCTXT pName) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Constructors

inline CRubyVector::CRubyVector(void)
{
	}

inline CRubyVector::CRubyVector(P2R const &p)
{
	m_x = p.m_x;

	m_y = p.m_y;
	}

inline CRubyVector::CRubyVector(CRubyVector const &v)
{
	m_x = v.m_x;

	m_y = v.m_y;
	}

inline CRubyVector::CRubyVector(number x, number y)
{
	m_x = x;

	m_y = y;
	}

inline CRubyVector::CRubyVector(number t)
{
	m_x = CRubyTrig::Cos(t);

	m_y = CRubyTrig::Sin(t);
	}

// Attributes

inline number CRubyVector::GetLength(void) const
{
	return num_sqrt(GetSquare());
	}

inline number CRubyVector::GetInverseLength(void) const
{
	return num_inv_sqrt(GetSquare());
	}

inline number CRubyVector::GetSquare(void) const
{
	return m_x * m_x + m_y * m_y;
	}

// Operations

inline void CRubyVector::MakeUnit(void)
{
	MakeUnit(GetSquare());
	}

inline void CRubyVector::MakeUnit(number ds)
{
	number s = num_inv_sqrt(ds);

	m_x *= s;

	m_y *= s;
	}

inline void CRubyVector::Rotate90(void)
{
	number tmp;

	tmp = m_x;

	m_x = m_y;

	m_y = num_add_invert(tmp);
	}

inline void CRubyVector::MakeNormal(void)
{
	MakeNormal(GetSquare());
	}

inline void CRubyVector::MakeNormal(number ds)
{
	MakeUnit(ds);

	Rotate90();
	}

inline void CRubyVector::Multiply(number k)
{
	m_x *= k;

	m_y *= k;
	}

// Operators

inline CRubyVector CRubyVector::operator + (CRubyVector const &v) const
{
	CRubyVector r;

	r.m_x = m_x + v.m_x;

	r.m_y = m_y + v.m_y;

	return r;
	}

inline CRubyVector CRubyVector::operator - (CRubyVector const &v) const
{
	CRubyVector r;

	r.m_x = m_x - v.m_x;

	r.m_y = m_y - v.m_y;

	return r;
	}

inline number CRubyVector::operator ^ (CRubyVector const &v) const
{
	return m_x * v.m_y - m_y * v.m_x;
	}

inline number CRubyVector::operator * (CRubyVector const &v) const
{
	return m_x * v.m_x + m_y * v.m_y;
	}

inline CRubyVector CRubyVector::operator << (CRubyVector const &v) const
{
	CRubyVector r;

	r.m_x = m_x * v.m_x;

	r.m_y = m_y * v.m_y;

	return r;
	}

inline CRubyVector CRubyVector::operator >> (CRubyVector const &v) const
{
	CRubyVector r;

	r.m_x = m_x / v.m_x;

	r.m_y = m_y / v.m_y;

	return r;
	}

inline CRubyVector CRubyVector::operator * (number k) const
{
	CRubyVector v;

	v.m_x = k * m_x;

	v.m_y = k * m_y;

	return v;
	}

inline CRubyVector CRubyVector::operator / (number k) const
{
	CRubyVector v;

	v.m_x = m_x / k;

	v.m_y = m_y / k;

	return v;
	}

inline CRubyVector CRubyVector::operator * (int k) const
{
	CRubyVector v;

	v.m_x = m_x * k;

	v.m_y = m_y * k;

	return v;
	}

inline CRubyVector CRubyVector::operator / (int k) const
{
	CRubyVector v;

	v.m_x = m_x / k;

	v.m_y = m_y / k;

	return v;
	}

inline CRubyVector CRubyVector::operator + (void) const
{
	return ThisObject;
	}

inline CRubyVector CRubyVector::operator - (void) const
{
	CRubyVector v;

	v.m_x = -m_x;

	v.m_y = -m_y;

	return v;
	}

// End of File

#endif
