
#include "Intern.hpp"

#include "UsbFuncLoopback.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Loopback Function Driver
//

// Instantiators 

static IUnknown * Create_UsbFuncLoopback(PCTXT pName)
{
	return New CUsbFuncLoopback(100);
	}

IUsbFuncEvents * Create_UsbFuncLoopback(UINT uPriority)
{
	CUsbFuncLoopback *p = New CUsbFuncLoopback(uPriority);

	return p;
	}

// Registration

global void Register_UsbFuncLoopback(void)
{
	piob->RegisterInstantiator("usbloop", Create_UsbFuncLoopback);
	}

// Constructor

CUsbFuncLoopback::CUsbFuncLoopback(UINT uPriority)
{
	m_pName = "Function Loopback Driver";	

	m_pThread = CreateThread(TaskUsbLoopback, uPriority, this, 0);

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbFuncLoopback::~CUsbFuncLoopback(void)
{
	m_pThread->Destroy();
	}

// IUsbEvents

void CUsbFuncLoopback::OnInit(void)
{
	m_iEndptRecv = m_pLowerDrv->GetFirsEndptAddr(m_iInterface) + 0;

	m_iEndptSend = m_pLowerDrv->GetFirsEndptAddr(m_iInterface) + 1;
	}

void CUsbFuncLoopback::OnStart(void)
{
	}

void CUsbFuncLoopback::OnStop(void)
{
	}

// IUsbFuncEvents

BOOL CUsbFuncLoopback::OnGetInterface(UsbDesc &Desc)
{
	UsbInterfaceDesc &Int = (UsbInterfaceDesc &) Desc;

	Int.m_bEndpoints = 2;
			
	Int.m_bClass	 = devVendor;
			
	Int.m_bSubClass	 = devVendor;
			
	Int.m_bProtocol	 = devVendor;
			
	return true;
	}

BOOL CUsbFuncLoopback::OnGetEndpoint(UsbDesc &Desc)
{
	UsbEndpointDesc &Ep = (UsbEndpointDesc &) Desc;

	if( Ep.m_bAddr == 1 ) {

		Ep.m_bDirIn	= false;

		Ep.m_bTransfer	= eptBulk;

		return true;
		}

	if( Ep.m_bAddr == 2 ) {

		Ep.m_bDirIn	= true;

		Ep.m_bTransfer	= eptBulk;

		return true;
		}

	return false;
	}

// Test

void CUsbFuncLoopback::TaskEntry(void)
{
	for(;;) {

		WaitRunning(FOREVER);

		UINT uCount = RecvBulk(m_iEndptRecv, m_bData, sizeof(m_bData), FOREVER);

		if( uCount != NOTHING ) {

			uCount = SendBulk(m_iEndptSend, m_bData, uCount, FOREVER);
			}
		}
	}

// Task Entry

int CUsbFuncLoopback::TaskUsbLoopback(IThread *pThread, void *pParam, UINT uParam)
{
	pThread->SetName("Usb Function Loopback");

	((CUsbFuncLoopback *) pParam)->TaskEntry();

	return 0;
	}

// End of File
