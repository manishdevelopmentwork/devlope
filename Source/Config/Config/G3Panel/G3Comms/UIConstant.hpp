
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIConstant_HPP

#define INCLUDE_UIConstant_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UIExpression.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Constant
//

class CUIConstant : public CUIExpression
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIConstant(void);
	};

// End of File

#endif
