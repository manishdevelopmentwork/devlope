
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoCipherRsa_HPP

#define INCLUDE_CryptoCipherRsa_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoAsym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RSA Cryptographic Signer
//

class CCryptoCipherRsa : public CCryptoAsym
{
	public:
		// Constructor
		CCryptoCipherRsa(void);

		// Destructor
		~CCryptoCipherRsa(void);

		// ICryptoAsym
		CString GetName(void);
		BOOL    Initialize(PCBYTE pKey, UINT uKey, CString const &Pass, UINT uFlags);
		BOOL	Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		BOOL    Decrypt(CByteArray &Out, PCBYTE pIn);
		void    Finalize(void);

	protected:
		// Data Members
		UINT       m_uFlags;
		psRsaKey_t m_Key;

		// Implementation
		void MakeFile(char *pName, PCBYTE pData, UINT uSize);
		void PostInitialize(void);
	};

// End of File

#endif
