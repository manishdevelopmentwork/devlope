
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Support
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3STRATON_HXX
	
#define	INCLUDE_G3STRATON_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hxx>

//////////////////////////////////////////////////////////////////////////
//
// Control Menu Commands
//

#define	IDM_CTRL		0xD0
#define	IDM_CTRL_SYNTAX		0xD010
#define IDM_CTRL_BUILD		0xD011
#define IDM_CTRL_CLEAN		0xD012
#define IDM_CTRL_ONLINE		0xD013
#define IDM_CTRL_SIMULATE	0xD014
#define	IDM_CTRL_STOP		0xD015
#define IDM_CTRL_PAUSE_RESUME	0xD016
#define IDM_CTRL_SINGLE_STEP	0xD017

// End of File

#endif
