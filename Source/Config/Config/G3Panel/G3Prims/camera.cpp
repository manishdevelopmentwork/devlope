
#include "intern.hpp"

#include "camera.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Camera Page
//

class CCameraPage : public CUIStdPage
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CCameraPage(CPrimCamera *pPrim, UINT uPage);

	// Operations
	BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

protected:
	// Data Members
	CPrimCamera * m_pPrim;
	UINT          m_uPage;

	// Implementation
};

//////////////////////////////////////////////////////////////////////////
//
// Camera Page
//

// Runtime Class

AfxImplementRuntimeClass(CCameraPage, CUIStdPage);

// Constructor

CCameraPage::CCameraPage(CPrimCamera *pPrim, UINT uPage)
{
	m_pPrim = pPrim;

	m_Class = AfxRuntimeClass(CPrimCamera);

	switch( uPage ) {

		case 0:
			m_Title = CString(L"Options");
			break;

		case 1:
			m_Title = CString(L"Figure");
			break;

		case 2:
			m_Title = CString(L"Show");
			break;
	}

	m_uPage = uPage;
}

// Operations

BOOL CCameraPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 0 ) {

		pView->StartGroup(CString(L"Configuration"), 1);

		pView->AddUI(pItem, L"root", L"Device");

		pView->AddUI(pItem, L"root", L"Scale");

		pView->EndGroup(TRUE);
	}

	if( m_uPage == 1 ) {

		pView->StartGroup(CString(L"Figure"), 1);

		pView->AddUI(pItem, L"root", L"Edge");

		pView->AddUI(pItem, L"root", L"Fill");

		pView->EndGroup(TRUE);
	}

	if( m_uPage == 2 ) {

		pView->StartGroup(CString(L"Display State"), 1);

		pView->AddUI(pItem, L"root", L"Visible");

		pView->EndGroup(TRUE);
	}

	pView->NoRecycle();

	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Camera
//

// Dynamic Class

AfxImplementDynamicClass(CPrimCamera, CPrimGeom);

// Constructor

CPrimCamera::CPrimCamera(void)
{
	m_uType  = 0x35;

	m_uPort  = 0;

	m_Device = NOTHING;

	m_Scale  = 0;
}

// UI Creation

BOOL CPrimCamera::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CCameraPage(this, 0));

	//pList->Append(New CCameraPage(this, 1));

	pList->Append(New CUIStdPage(CString(IDS_FIGURE),
		      AfxPointerClass(this),
		      1
	));

	pList->Append(New CCameraPage(this, 2));

	return TRUE;
}

// UI Update

void CPrimCamera::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

	}

	CPrim::OnUIChange(pHost, pItem, Tag);
}

// Overridables

void CPrimCamera::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillRect(pGDI, Rect);

	m_pEdge->DrawRect(pGDI, Rect, uMode);

	DrawDevice(pGDI);

	DrawNotSupported(pGDI);
}

void CPrimCamera::SetInitState(void)
{
	CPrimGeom::SetInitState();

	SetInitSize(80, 60);
}

CSize CPrimCamera::GetMinSize(IGDI *pGDI)
{
	return CSize(40, 30);
}

BOOL CPrimCamera::IsSupported(void)
{
	return m_pDbase->GetSoftwareGroup() >= SW_GROUP_3A;
}

// Download Support

BOOL CPrimCamera::MakeInitData(CInitData &Init)
{
	CPrimGeom::MakeInitData(Init);

	Init.AddByte(BYTE(FindDevicePort()));

	Init.AddWord(WORD(m_Device));

	Init.AddByte(BYTE(m_Scale));

	return FALSE;
}

// Meta Data

void CPrimCamera::AddMetaData(void)
{
	CPrimGeom::AddMetaData();

	Meta_AddInteger(Device);
	Meta_AddInteger(Scale);

	Meta_SetName(IDS_CAMERA);
}

// Implementation

void CPrimCamera::DrawDevice(IGDI *pGDI)
{
	CSize Size;

	Size.cx = m_DrawRect.x2 - m_DrawRect.x1;

	Size.cy = m_DrawRect.y2 - m_DrawRect.y1;

	CString Text;

	Text.Printf(L"%s", FindDeviceName());

	pGDI->SetBackMode(modeTransparent);

	pGDI->SetTextFore(m_pEdge->m_pColor->GetColor());

	pGDI->SelectFont(fontHei10);

	int tx = pGDI->GetTextWidth(Text);

	int ty = pGDI->GetTextHeight(Text);

	if( tx < Size.cx && ty < Size.cy ) {

		int px = m_DrawRect.x1 + 2 + (Size.cx - tx) / 2;

		int py = m_DrawRect.y1 + 2 + (Size.cy - ty) / 2;;

		pGDI->TextOut(px, py, Text);
	}
}

void CPrimCamera::DrawNotSupported(IGDI *pGDI)
{
	if( !IsSupported() ) {

		R2 Work = m_DrawRect;

		CString Text = IDS_NOT_SUPPORTED;

		pGDI->SelectFont(fontHei16);

		int cx = pGDI->GetTextWidth(Text);

		int cy = pGDI->GetTextHeight(Text);

		int px = (Work.x1 + Work.x2 - cx) / 2;

		int py = (Work.y1 + Work.y2 - cy) / 2;

		pGDI->SetTextFore(GetRGB(255, 0, 0));

		pGDI->TextOut(px, py, Text);
	}
}

UINT CPrimCamera::FindDevicePort(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	CCommsManager *pComms = pSystem->m_pComms;

	CCommsDevice *pDevice = pComms->FindDevice(m_Device);

	if( pDevice ) {

		CCommsPort *pPort = pDevice->GetParentPort();

		if( pPort ) {

			return pPort->m_Number;
		}
	}

	return NOTHING;
}

CString CPrimCamera::FindDeviceName(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) GetDatabase()->GetSystemItem();

	CCommsManager *pComms = pSystem->m_pComms;

	CCommsDevice *pDevice = pComms->FindDevice(m_Device);

	if( pDevice ) {

		return pDevice->m_Name;
	}

	return CString(IDS_NO_DEVICE);
}

// End of File
