
#include "intern.hpp"

#include "PrimRubyGaugeTypeAR1.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A1 Radial Gague Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyGaugeTypeAR1, CPrimRubyGaugeTypeAR);

// Constructor

CPrimRubyGaugeTypeAR1::CPrimRubyGaugeTypeAR1(void)
{
	}

// Download Support

BOOL CPrimRubyGaugeTypeAR1::MakeInitData(CInitData &Init)
{
	CPrimRubyGaugeTypeAR::MakeInitData(Init);

	return TRUE;
	}

// Meta Data Creation

void CPrimRubyGaugeTypeAR1::AddMetaData(void)
{
	CPrimRubyGaugeTypeAR::AddMetaData();

	Meta_SetName((IDS_TYPE_RADIAL_GAUGE_2));
	}

// Path Management

void CPrimRubyGaugeTypeAR1::MakePaths(void)
{
	LoadLayout();

	CPrimRubyGaugeTypeAR::MakePaths();

	m_bezelBase = 52;

	m_d.Circle(m_pathFace, m_pointCenter, 52, m_scale);

	Ring(m_pathOuter, 70, 90);
	Ring(m_pathInner, 55, 70);
	Ring(m_pathRing1, 89, 91);
	Ring(m_pathRing2, 69, 70);
	Ring(m_pathRing3, 51, 55);
	Ring(m_pathRing4, 59, 54);
	}

// Implementation

void CPrimRubyGaugeTypeAR1::LoadLayout(void)
{
	m_radiusPivot     = 10 * m_ScalePivot / 100.0;

	m_pointPivot.m_x  = 100;
	m_pointPivot.m_y  = 140 - m_radiusPivot;
	
	m_angleMin        = -50 - 90;
	m_angleMax        = +50 - 90;
	
	m_radiusOuter.m_x = 55;
	m_radiusOuter.m_y = 65;

	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBug.m_x   = m_PointMode ? 60 : 60;
	m_radiusMajor.m_x = m_PointMode ? 47 : 40;
	m_radiusMinor.m_x = m_PointMode ? 49 : 45;
	// cppcheck-suppress duplicateExpressionTernary
	m_radiusBand.m_x  = m_PointMode ? 50 : 50;
	m_radiusPoint.m_x = m_PointMode ? 46 : 47;
	m_radiusSweep.m_x = m_PointMode ? 35 : 47;

	CalcRadii();
	}

// End of File
