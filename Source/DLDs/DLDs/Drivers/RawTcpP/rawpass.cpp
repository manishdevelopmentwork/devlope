
#include "intern.hpp"

#include "rawpass.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Passive Port Driver
//

// Instantiator

INSTANTIATE(CRawTCPPassiveDriver);

// Constructor

CRawTCPPassiveDriver::CRawTCPPassiveDriver(void)
{
	m_Ident = DRIVER_ID;
	
	m_pSock = NULL;

	m_uPort = 1234;

	m_fKeep = FALSE;
	}

// Destructor

CRawTCPPassiveDriver::~CRawTCPPassiveDriver(void)
{
	}

// Configuration

void MCALL CRawTCPPassiveDriver::Load(LPCBYTE pData)
{
	SkipUpdate(pData);

	m_uPort = GetWord(pData);

	m_fKeep = GetByte(pData);
	}

// Management

void MCALL CRawTCPPassiveDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CRawTCPPassiveDriver::Detach(void)
{
	if( m_pSock ) {

		m_pSock->Abort();
		
		m_pSock->Release();
		}
	}

// Entry Points

void MCALL CRawTCPPassiveDriver::Disconnect(void)
{
	if( m_pSock ) {

		m_pSock->Close();
		}
	}

UINT MCALL CRawTCPPassiveDriver::Read(UINT uTime)
{
	UINT uInit = GetTickCount();

	UINT uTest = ToTicks(uTime);

	for(;;) {

		if( CheckSocket() ) {

			BYTE bData;

			UINT uSize = 1;

			if( m_pSock->Recv(&bData, uSize) == S_OK ) {

				return bData;
				}
			}

		if( GetTickCount() - uInit >= uTest ) {

			return NOTHING;
			}

		Sleep(25);
		}
	}

UINT MCALL CRawTCPPassiveDriver::Write(BYTE bData, UINT uTime)
{
	UINT uInit = GetTickCount();

	UINT uTest = ToTicks(uTime);

	while( CheckSocket() ) {

		UINT uSize = 1;

		if( m_pSock->Send(&bData, uSize) == S_OK ) {

			return 1;
			}

		if( GetTickCount() - uInit >= uTest ) {

			break;
			}

		Sleep(10);
		}

	return 0;
	}

UINT MCALL CRawTCPPassiveDriver::Write(PCBYTE pData, UINT uCount, UINT uTime)
{
	UINT uSent = 0;

	UINT uInit = GetTickCount();

	UINT uTest = ToTicks(uTime);

	while( CheckSocket() ) {

		UINT uSend = uCount - uSent;

		if( m_pSock->Send(PBYTE(pData), uSend) == S_OK ) {

			pData += uSend;

			uSent += uSend;

			if( uSent == uCount ) {

				return uCount;
				}

			}

		if( GetTickCount() - uInit >= uTest ) {

			break;
			}

		Sleep(10);
		}

	return uSent;
	}

// Implementation

BOOL CRawTCPPassiveDriver::CheckSocket(void)
{
	for(;;) {

		if( !m_pSock ) {

			m_pSock = CreateSocket(IP_TCP);

			if( m_pSock ) {

				if( m_fKeep ) {

					m_pSock->SetOption(OPT_KEEP_ALIVE, TRUE);
					}

				m_pSock->Listen(m_uPort);
				}
			else {
				Sleep(100);

				continue;
				}
			}

		UINT uPhase;

		m_pSock->GetPhase(uPhase);

		if( uPhase == PHASE_CLOSING ) {

			m_pSock->Close();

			m_pSock->Release();

			m_pSock = NULL;

			continue;
			}

		if( uPhase == PHASE_ERROR ) {

			m_pSock->Abort();

			m_pSock->Release();

			m_pSock = NULL;

			continue;
			}

		if( uPhase == PHASE_OPEN ) {

			return TRUE;
			}

		return FALSE;
		}
	}

// End of File
