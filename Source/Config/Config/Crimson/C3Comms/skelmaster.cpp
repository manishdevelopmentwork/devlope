
#include "intern.hpp"

#include "skelmaster.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Driver
//

// Instantiator

ICommsDriver *	Create_SkeletonMasterDriver(void)
{
	return New CSkeletonMasterDriver;
	}

// Constructor

CSkeletonMasterDriver::CSkeletonMasterDriver(void)
{
	m_wID		= 0x40A7;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Skeleton";
	
	m_DriverName	= "Master";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Skeleton Master";

	AddSpaces();
	}

// Binding Control

UINT CSkeletonMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSkeletonMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CSkeletonMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSkeletonMasterDriverOptions);
	}

CLASS CSkeletonMasterDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Implementation

void CSkeletonMasterDriver::AddSpaces(void)
{
	AddSpace( New CSpace(	'D',			// Tag to represent table to runtime
				"D",			// Prefix presented to user
				"Pyro Data",		// Long name presented to user
				10,			// Radix of numeric portion
				0,			// Minimum value of numeric portion
				25,			// Maximum value of numeric portion
				addrLongAsLong,		// Type of data and presentation
				addrLongAsLong,		// Same as above in most instances
				2			// Width of numeric portion
				));
	}

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Master Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSkeletonMasterDriverOptions, CUIItem);

// Constructor

CSkeletonMasterDriverOptions::CSkeletonMasterDriverOptions(void)
{
	m_Drop = 0x4D;
	}

// UI Managament

void CSkeletonMasterDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support	     

BOOL CSkeletonMasterDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CSkeletonMasterDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

// End of File
