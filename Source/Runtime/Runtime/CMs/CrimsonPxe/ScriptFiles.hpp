
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ScriptFiles_HPP

#define INCLUDE_ScriptFiles_HPP

//////////////////////////////////////////////////////////////////////////
//
// File Data
//

struct CScriptFileData
{
	PCTXT  m_pName;
	PCBYTE m_pData;
	UINT   m_uSize;
};

//////////////////////////////////////////////////////////////////////////
//
// Script Files
//

class CScriptFiles
{
public:
	// Constructors
	CScriptFiles(void);

	// File Lookup
	BOOL FindFile(CScriptFileData const * &pFile, UINT uPos);
	BOOL FindFile(CScriptFileData const * &pFile, PCTXT pName);

protected:
	// File Table
	static CScriptFileData const m_Files[];

	// File Index
	CMap<PCTXT, UINT> m_Index;

	// Implementation
	BOOL MakeIndex(void);
};

// End of File

#endif
