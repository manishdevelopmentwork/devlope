
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConElementEditBox_HPP

#define INCLUDE_DevConElementEditBox_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DevConElementBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDevConEditCtrl;

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Edit Box UI Element
//

class CDevConElementEditBox : public CDevConElementBase
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConElementEditBox(void);

	// Destructor
	~CDevConElementEditBox(void);

	// Operations
	void AddLayout(CLayFormation *pForm);
	void CreateControls(CWnd &Wnd, UINT &id);
	UINT OnNotify(UINT uID, UINT uNotify, CWnd &Wnd);

protected:
	// Data Member
	CLayItem        * m_pUnitLayout;
	CDevConEditCtrl * m_pEditCtrl;
	CStatic	        * m_pUnitCtrl;
	DWORD		  m_dwStyle;
	CString		  m_Units;
	UINT		  m_uWidth;
	BOOL		  m_fBusy;

	// Overridables
	void OnSetData(void);

	// Implementation
	void SetEditCtrl(CString const &Data);
};

// End of File

#endif
