
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConTabWnd_HPP

#define INCLUDE_DevConTabWnd_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Referenced Classes
//

class CDevCon;

class CDevConNode;

class CDevConElement;

//////////////////////////////////////////////////////////////////////////
//								
// Device Configuration Tab View Window
//

class CDevConTabWnd : public CDialogViewWnd, public IDropTarget
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDevConTabWnd(CJsonData *pData, CJsonData *pSchema, CJsonData *pPerson, CString const &Path, CJsonData *pTab);

	// Destructor
	~CDevConTabWnd(void);

	// IUnknown
	HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
	ULONG   METHOD AddRef(void);
	ULONG   METHOD Release(void);

	// IDropTarget
	HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
	HRESULT METHOD DragLeave(void);
	HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

protected:
	// Commands
	class CCmdEdit;

	// Section
	struct CSection
	{
		CSection(void)
		{
			m_fState = TRUE;
		}

		CSection(CString const &Enable) : m_Enable(Enable)
		{
			m_fState = TRUE;
		}

		CString m_Enable;
		BOOL    m_fState;
	};

	// Element
	struct CElement
	{
		CElement(void)
		{
			m_pElem  = NULL;
			m_pField = NULL;
			m_fTable = FALSE;
		}

		CElement(CDevConElement *pElem)
		{
			m_pElem  = pElem;
			m_pField = NULL;
			m_fTable = FALSE;
		}

		CDevConElement * m_pElem;
		CJsonData      * m_pField;
		CString		 m_Name;
		CString		 m_Label;
		CString		 m_Default;
		CString		 m_Enable;
		BOOL		 m_fTable;
	};

	// Typedefs
	typedef CArray<CElement>    CElementList;
	typedef CArray<CSection>    CSectionList;
	typedef CMap<UINT, UINT>    CIdMap;
	typedef CMap<CString, UINT> CNameMap;

	// Bound Item
	CJsonData   * m_pData;
	CJsonData   * m_pSchema;
	CJsonData   * m_pPerson;
	CString       m_Path;
	CJsonData   * m_pTab;
	CSysProxy     m_System;

	// Drop Context
	CDropHelper m_DropHelp;

	// Coloring
	CColor m_Color;
	CBrush m_Brush;

	// Layout Data
	BOOL		m_fHasUndo;
	BOOL		m_fReadOnly;
	BOOL		m_fStatus;
	int		m_nScroll;
	CScrollBar    * m_pScroll;
	BOOL		m_fScroll;
	CLayFormation * m_pForm;
	CElementList    m_Elements;
	CSectionList    m_Sections;
	CIdMap		m_Ids;
	CNameMap	m_Names;
	UINT		m_uMinId;
	UINT		m_uMaxId;

	// Current Status
	CWnd	* m_pFocus;
	UINT	  m_uFocus;
	BOOL	  m_fLoaded;
	CString   m_Root;

	// Overribables
	void    OnAttach(void);
	CString OnGetNavPos(void);
	BOOL    OnNavigate(CString const &Nav);
	void    OnExec(CCmd *pCmd);
	void    OnUndo(CCmd *pCmd);

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnPostCreate(void);
	void OnDestroy(void);
	void OnCancelMode(void);
	void OnSetCurrent(BOOL fCurrent);
	void OnPaint(void);
	void OnPaint(CDC &DC);
	BOOL OnEraseBkGnd(CDC &DC);
	void OnSize(UINT uCode, CSize Size);
	void OnGetMinMaxInfo(MINMAXINFO &Info);
	BOOL OnCommand(UINT uID, UINT uNotify, CWnd &Wnd);
	BOOL OnNotify(UINT uID, NMHDR &Info);
	void OnFocusNotify(UINT uID, CWnd &Wnd);
	void OnSetFocus(CWnd &Wnd);
	void OnShowWindow(BOOL fShow, UINT uStatus);
	void OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos);
	void OnHScroll(UINT uCode, int nPos, CWnd &Ctrl);
	void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);
	void OnScroll(UINT uCode, int nPos, CWnd &Ctrl);
	HOBJ OnCtlColorStatic(CDC &DC, CWnd &Wnd);
	HOBJ OnCtlColorBtn(CDC &DC, CWnd &Wnd);

	// Tooltip
	void CreateToolTip(void);
	void DestroyToolTip(void);
	void HideToolTip(void);
	void MoveToolTips(void);

	// String Helpers
	CString Join(CString Base, CString const &More);

	// Implementation
	void MakeUI(void);
	void LoadSection(CJsonData *pSection);
	void LoadFields(CLayFormation *pGrid, CJsonData *pFields);
	void LoadField(CLayFormation *pGrid, CJsonData *pField);
	void LoadTable(CLayFormation *pForm, CJsonData *pTable);
	void MakeTable(CJsonData *pTable, CJsonData *pRoot, CString const &Full);
	void ApplyRowCount(CJsonData *pData, CJsonData *pFields, UINT uRows);
	void ApplyTemplate(CJsonData *pData, CJsonData *pTemplate);
	void ApplyKeyFields(CJsonData *pData);
	BOOL FindKey(CJsonData *pData, CString const &Key);
	void EnableUI(CString const &Name);
	void EnableUI(CElement const &Elem);
	void CreateUI(void);
	void LayoutUI(void);
	void KillUI(void);
	BOOL CheckScroll(void);
	BOOL Scroll(int nScroll);
	BOOL Evaluate(CString const &Code);
	void TakeAction(CElement const &Elem, UINT uCode);
	void PlayEditCmd(CCmdEdit *pCmd, BOOL fExec);
	void SetDirty(void);
};

// End of File

#endif
