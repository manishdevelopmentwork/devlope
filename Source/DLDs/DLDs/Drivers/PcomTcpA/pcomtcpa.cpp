#include "intern.hpp"

#include "pcomtcpa.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unitronics PCOM ASCII Master TCP/IP Driver
//

// Instantiator

INSTANTIATE(CPcomAMasterTCPDriver);

// Constructor

CPcomAMasterTCPDriver::CPcomAMasterTCPDriver(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CPcomAMasterTCPDriver::~CPcomAMasterTCPDriver(void)
{
	}

// Configuration

void MCALL CPcomAMasterTCPDriver::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CPcomAMasterTCPDriver::Attach(IPortObject *pPort)
{
	}

void MCALL CPcomAMasterTCPDriver::Open(void)
{
	}

// Device

CCODE MCALL CPcomAMasterTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pBase = m_pCtx;

			m_pCtx->m_bUnit  = GetByte(pData);
			m_pCtx->m_IP     = GetAddr(pData);
			m_pCtx->m_uPort  = GetWord(pData);
			m_pCtx->m_fKeep  = GetByte(pData);
			m_pCtx->m_fPing  = GetByte(pData);
			m_pCtx->m_uTime1 = GetWord(pData);
			m_pCtx->m_uTime2 = GetWord(pData);
			m_pCtx->m_uTime3 = GetWord(pData);
			m_pCtx->m_wTrans = 0;
			m_pCtx->m_pSock  = NULL;
			m_pCtx->m_uLast  = GetTickCount();
					
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pBase = m_pCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CPcomAMasterTCPDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);

	}

// Entry Points

CCODE MCALL CPcomAMasterTCPDriver::Ping(void)
{
	if( m_pCtx->m_fPing ) {

		if( CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

			if( !OpenSocket() ) {

				return CCODE_ERROR;
				}

			return CCODE_SUCCESS;
			}
		}

	return CPcomAMasterDriver::Ping();	
	}

// Socket Management

BOOL CPcomAMasterTCPDriver::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CPcomAMasterTCPDriver::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {
	
		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}
				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}
				
			CloseSocket(TRUE); 

			return FALSE;
			}
		}

	return FALSE;
	}

void CPcomAMasterTCPDriver::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CPcomAMasterTCPDriver::Send(void)
{
	UINT uSize = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CPcomAMasterTCPDriver::RecvFrame(void)
{
	m_uPtr  = 0;

	UINT uSize = 0;

	UINT uTotal = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + m_uPtr, uSize);

		if( uSize ) {
			
			for( UINT u = m_uPtr; u < m_uPtr + uSize; u++ ) {

				if( m_bRx[u] == CR ) {

					if( memcmp(m_bRx, m_bTx, 3) ) {

						return FALSE;
						}

					m_uPtr = m_uPtr + uSize - 6;

					memcpy(m_bRx, m_bRx + 6, m_uPtr);

					return TRUE;
					}
				}
			
			m_uPtr += uSize;

			continue;
			}

		if( !CheckSocket() ) {
			
			return FALSE;
			}

		Sleep(10);
		} 
	
	return FALSE;
	}

// Overridables

BOOL CPcomAMasterTCPDriver::Transact(void)
{
	PBYTE pBuff = PBYTE(alloca(sizeof(m_bTx)));

	memset(pBuff, 0, m_uPtr + 6);

	memcpy(pBuff + 6, m_bTx, m_uPtr);

	pBuff[0] = LOBYTE(m_pCtx->m_wTrans);

	pBuff[1] = HIBYTE(m_pCtx->m_wTrans);

	m_pCtx->m_wTrans++;

	pBuff[2] = 101;

	pBuff[4] = LOBYTE(m_uPtr);

	pBuff[5] = HIBYTE(m_uPtr);

	m_uPtr += 6;

	memcpy(m_bTx, pBuff, m_uPtr);
	
	if( OpenSocket() ) {

		if( Send() && RecvFrame() && CheckFrame() ) {

			return TRUE;
			}

		CloseSocket(TRUE); 
		}

	return FALSE; 
	}

// Helpers

BOOL CPcomAMasterTCPDriver::CheckFrame(void)
{
	m_uCheck = 0;

	UINT u = 0;

	for( u = 2; u < m_uPtr - 3; u++ ) {

		m_uCheck += m_bRx[u];
		}

	BYTE bCheck = ByteFromAscii(m_bRx[u]) << 4;

	u++;

	bCheck     |= ByteFromAscii(m_bRx[u]);
      
	return (bCheck == (m_uCheck % 256));
	}

// End of File

