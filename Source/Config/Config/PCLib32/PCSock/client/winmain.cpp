
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Test Application
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Test Harness
//

int WINAPI WinMain(HINSTANCE hThis, HINSTANCE hPrev, PSTR pCmdLine, int nCmdShow)
{
	afxModule = New CModule(modApplication);

	if( afxModule->InitApp(hThis, pCmdLine, nCmdShow) ) {

		extern void TestFunc(void);

		TestFunc();

		afxModule->Terminate();
		}

	delete afxModule;

	return 0;
	}

// End of File
