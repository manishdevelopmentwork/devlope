
#include "Intern.hpp"

#include "TagTriggerFlag.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

#include "TagFlag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Flag Tag Trigger
//

// Dynamic Class

AfxImplementDynamicClass(CTagTriggerFlag, CTagTrigger);

// Constructor

CTagTriggerFlag::CTagTriggerFlag(void)
{
	}

// UI Update

void CTagTriggerFlag::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		UINT uMask = HasSetpoint() ? 0xFF : 0x87;

		LimitEnum(pHost, L"Mode", m_Mode, uMask);

		DoEnables(pHost);
		}

	CTagTrigger::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CTagTriggerFlag::NeedSetpoint(void) const
{
	return m_Mode >= 3 && m_Mode <= 6;
	}

BOOL CTagTriggerFlag::HasSetpoint(void) const
{
	CTagFlag * pTag = (CTagFlag *) GetParent(AfxRuntimeClass(CTagFlag));

	return pTag->HasSetpoint();
	}

// Download Support

BOOL CTagTriggerFlag::MakeInitData(CInitData &Init)
{
	CTagTrigger::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CTagTriggerFlag::AddMetaData(void)
{
	CTagTrigger::AddMetaData();

	Meta_SetName((IDS_TAG_TRIGGER));
	}

// End of File
