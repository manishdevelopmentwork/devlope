
#include "intern.hpp"

#include "r29id.hpp" 

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit ID Entry Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Imports

#include "..\j1939\j1939.cpp"

// Constructor

CCAN29bitIdEntryRawDriver::CCAN29bitIdEntryRawDriver(void)
{
	m_Ident = DRIVER_ID;
       	}

// Destructor

CCAN29bitIdEntryRawDriver::~CCAN29bitIdEntryRawDriver(void)
{
	}

// Configuration

void MCALL CCAN29bitIdEntryRawDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {
		
		}
	}
	
void MCALL CCAN29bitIdEntryRawDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uFlags |= flagPrivate;

	Config.m_uFlags |= flagExtID;
	}
	
// Management

void MCALL CCAN29bitIdEntryRawDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCAN29bitIdEntryRawHandler(m_pHelper);

	pPort->Bind(m_pHandler);
	}

void MCALL CCAN29bitIdEntryRawDriver::Detach(void)
{
	m_pHandler->Release();
	}

void MCALL CCAN29bitIdEntryRawDriver::Open(void)
{	
	}

// Device

CCODE MCALL CCAN29bitIdEntryRawDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);
	
	if( !(m_pIdent29 = (CIdent29 *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pIdent29		= new CIdent29;

			m_pCtx			= m_pIdent29;

			m_pIdent29->m_uBackOff	= GetLong(pData);

			if( GetWord(pData) == 0x1234 ) {

				m_pIdent29->m_uPGNs = GetWord(pData);
			
				m_pIdent29->m_pIDs  = new ID29[m_pIdent29->m_uPGNs];

				for( UINT x = 0; x < m_pIdent29->m_uPGNs; x++ ) {

					m_pIdent29->m_pIDs[x].m_Number	 = GetLong(pData);

					m_pIdent29->m_pIDs[x].m_Ref      = GetLong(pData);

					m_pIdent29->m_pIDs[x].m_SendReq	 = GetByte(pData);

					m_pIdent29->m_pIDs[x].m_uRequest = GetTickCount();

					m_pIdent29->m_pIDs[x].m_Diag	 = 0;

					m_pIdent29->m_pIDs[x].m_Enh      = 0;

					m_pIdent29->m_pIDs[x].m_Dir      = 0;

					if( GetWord(pData) == 0x1234 ) {

						m_pIdent29->m_pIDs[x].m_uSPNs   = GetWord(pData);

						m_pIdent29->m_pIDs[x].m_pSPNs	= new SPN[m_pIdent29->m_pIDs[x].m_uSPNs];

						for( UINT y = 0; y < m_pIdent29->m_pIDs[x].m_uSPNs; y++ ) {

							m_pIdent29->m_pIDs[x].m_pSPNs[y].m_Size = GetByte(pData) & 0x7F;
							}

						m_pIdent29->m_pIDs[x].m_RepRate = GetLong(pData);

						BYTE bActive                    = GetByte(pData);

						BYTE bEnhanced                  = GetByte(pData);

						if( bActive ) {

							ID id;

							FindID(id, m_pIdent29->m_pIDs[x].m_Number, 0);

							UINT uBytes = FindPGNByteSize(&m_pIdent29->m_pIDs[x], m_pIdent29->m_pIDs[x].m_uSPNs);

							m_pHandler->MakePDU(&id,
									    uBytes,
									    m_pIdent29->m_pIDs[x].m_RepRate,
									    0,
									    bEnhanced,
									    0,
									    m_pIdent29->m_pIDs[x].m_Ref);
							}
						}
					}
				}
									 
			m_pIdent29->m_uLast = GetTickCount();
			
			pDevice->SetContext(m_pIdent29);
			
			m_pHandler->ShowPDUs();

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pIdent29;

	return CCODE_SUCCESS;
 	}

CCODE MCALL CCAN29bitIdEntryRawDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete [] m_pIdent29->m_pIDs->m_pSPNs;

		delete [] m_pIdent29->m_pIDs;
		
		delete m_pIdent29;

		m_pIdent29 = NULL;

		m_pCtx	   = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE CCAN29bitIdEntryRawDriver::Ping(void)
{
	return m_pHandler->ClaimAddress(0, transDriver);
	}

CCODE CCAN29bitIdEntryRawDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	ID id;	

	if( FindID(id, Addr) )  {

		PDU * pPDU = NULL;

		ID29 * pPG = FindPG(id, Addr, pPDU);

		if( pPDU ) {

			if( pPG ) {

				m_pIdent29->m_bDA = id.i.m_PS;

				if( !m_pHandler->HasData(pPDU) ) {

					return CCODE_SILENT;
					}

				if( true ) {

					UINT uIndex = Addr.a.m_Offset & 0x3F;

					UINT uShift = 0;
					
					for( UINT x = 0, y = 0; x < pPG->m_uSPNs; x++ ) {

						UINT uSize = pPG->m_pSPNs[x].m_Size;

						if( uSize && (x == uIndex || ( x > uIndex && x < uIndex + uCount )) ) {

							UINT uMask = 0x1 << (uSize - 1);
				
							while( !(uMask & 0x1) ) {

								uMask |= uMask >> 1;
								}
						
							UINT uBegin   = uShift / 8;

							UINT uEnd     = (uShift + uSize) / 8;

							DWORD dwData  = 0;

							UINT i        = 0;

							UINT s	      = uShift % 8;
					
							PBYTE pBytes  = PBYTE(pPDU->m_pData + uBegin);

							dwData |= pBytes[i] >> s;

							while( uEnd > uBegin ) {

								i++;

								dwData |= (pBytes[i] << (8 * i)) >> s;

								uBegin++;
					   			}

							dwData = dwData & uMask;

							pData[y] = dwData;

							y++;
							}
												
						uShift += uSize;
						}

					return uCount;
					} 
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE CCAN29bitIdEntryRawDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	ID id;

	if( FindID(id, Addr) )  {

		PDU * pPDU = NULL;

		ID29 * pPG = FindPG(id, Addr, pPDU);

		if( pPDU ) {

			if( pPG ) {

				BYTE bSrc = pPG->m_Number & 0xFF;

				m_pHandler->SetSA(bSrc);

				SetDirection(pPDU, TRUE);

				UINT uShift = 0;

				UINT uSize  = 0;

				UINT uIndex = Addr.a.m_Offset & 0x3F;

				BOOL fPass  = FALSE;

				for( UINT x = 0, y = 0; x < pPG->m_uSPNs; x++ ) {

					DWORD Data = 0;

				 	if( x == uIndex || ( x > uIndex && x < uIndex + uCount ) ) {
						
						Data = pData[y];
					
						y++;

						fPass = TRUE;
						}
				
					UINT uSize = pPG->m_pSPNs[x].m_Size;

					if( uSize && fPass ) {

						UINT uBegin   = uShift / 8;

						UINT uEnd     = (uShift + uSize) / 8;

						UINT uMask = 0x1 << (uSize - 1);

						while( !(uMask & 0x1) ) {

						 	uMask |= uMask >> 1;
							}

						PBYTE pBytes = PBYTE(pPDU->m_pData + uBegin);

						UINT i = 0;

						UINT s = uShift % 8;

						while( uEnd >= uBegin ) {

							*pBytes &= ~(uMask << s) >> i;

							*pBytes |= (((Data & uMask) << s) >> i);

							pBytes++;
						
							i += 8;

							if( i >= sizeof(DWORD) * 8 ) {

								break;
								}

							uBegin++;
							}
						
						fPass = FALSE;
						}

					uShift += uSize;
					}

				if( m_pHandler->SendPDU(pPDU, 0, transDriver) ) {

					return uCount;
					}
				}
			}
		}
		
	return CCODE_ERROR;
	}

// Implementation

BOOL  CCAN29bitIdEntryRawDriver::FindID(ID &id, UINT uID, UINT uPriority)
{
	if( m_pIdent29 ) {

		id = (ID &)uID;

		id.i.m_X   = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CCAN29bitIdEntryRawDriver::FindID(ID &id, AREF Addr)
{
	if( m_pIdent29 ) {

		for( UINT u = 0; u < m_pIdent29->m_uPGNs; u++ ) {

			if( m_pIdent29->m_pIDs[u].m_Ref == (Addr.m_Ref & 0xFFFFFF00)) {

				return CCAN29bitIdEntryRawDriver::FindID(id, m_pIdent29->m_pIDs[u].m_Number, 0);
				}
			}
		}

	return FALSE;
	}


PGN * CCAN29bitIdEntryRawDriver::FindPG(PDU * pPDU)
{
	if( pPDU && m_pIdent29 ) {

		for( UINT u = 0; u < m_pCtx->m_uPGNs; u++ ) {

			if( m_pIdent29->m_pIDs[u].m_Ref == pPDU->m_Ref ) {

				return (PGN*) &m_pIdent29->m_pIDs[u];
				}
			}
		}

	return NULL;
	}

ID29 * CCAN29bitIdEntryRawDriver::FindPG(ID id, AREF Addr, PDU * &pPDU)
{
	CCAN29bitIdEntryRawHandler * pHandler = (CCAN29bitIdEntryRawHandler *)m_pHandler;

	if( pHandler ) {

		PDU * pWork = NULL;
	
		while( (pWork = pHandler->FindNextPDU(&id, pWork)) ) {

			ID29 * pPG = (ID29 *) FindPG(pWork);
	
			if( pPG ) {

				if( IsRef(pPG, Addr) ) {

					pPDU = pWork;
					
					return pPG;
					}
				}
			}
		}

	return NULL;
	}

BOOL CCAN29bitIdEntryRawDriver::IsDM(PGN * pPGN)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawDriver::IsRef(ID29 * pID, AREF Addr)
{
	if( pID ) {

		if( Addr.m_Ref >= pID->m_Ref ) {
			
			if( Addr.m_Ref < pID->m_Ref + pID->m_uSPNs ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Helpers

BOOL CCAN29bitIdEntryRawDriver::SkipUpdate(LPCBYTE &pData)
{
	if( GetByte(pData) == 0x01 ) {
		       
		if( GetWord(pData) == 0x1234 ) {

			UINT uCount = GetWord(pData);

			pData += 4 * uCount;

			Item_Align4(pData);

			uCount = GetWord(pData);
				
			pData += 1 * uCount;
			
			return TRUE;
			}
	
		return FALSE;
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Raw 29-bit Identifier Entry Handler
//

// Constructor

CCAN29bitIdEntryRawHandler::CCAN29bitIdEntryRawHandler(IHelper *pHelper) : CCANJ1939Handler(pHelper)
{
	SetSA(0xFF);
	}

// IPortHandler

void METHOD CCAN29bitIdEntryRawHandler::OnOpen(CSerialConfig const &Config)
{	
	CCANJ1939Handler::OnOpen(Config);

	m_fClaim[0] = TRUE;
	}

// FRAME_EXT Access

BOOL CCAN29bitIdEntryRawHandler::SendPDU(PDU * pPDU, BOOL fRetry, UINT uTrans)
{
	if( m_fClaim[0] ) {
		
		Start(uTrans);

		ID id;	   

		id.m_Ref   = pPDU->m_ID.m_Ref;

		AddID(id, uTrans);

		AddCtrl(pPDU, uTrans);

		AddData(pPDU, uTrans);

		return PutFrame(uTrans);
		}

	return FALSE;
	}

// Driver Access

PDU* CCAN29bitIdEntryRawHandler::FindNextPDU(ID * pID, PDU * pLast)
{
	if( pID ) {

		UINT uMask  = GetMask();

		BOOL fBegin = pLast ? FALSE : TRUE;

		for( UINT u = 0; u < m_uPDUs; u++ ) {

			if( !fBegin ) {

				fBegin = (pLast->m_Ref == m_pPDUs[u].m_Ref);

				continue;
				}

			if( (pID->m_Ref & uMask) == (m_pPDUs[u].m_ID.m_Ref & uMask) ) {

				return &m_pPDUs[u];
				}
			}
		}

	return NULL;
	}


void CCAN29bitIdEntryRawHandler::SetSA(BYTE bSA)
{
	m_bSrc[0] = bSA;
	}

UINT CCAN29bitIdEntryRawHandler::GetMask(void)
{
	return 0x1FFFFFFF;
	}

// Implementation

BOOL CCAN29bitIdEntryRawHandler::DoListen(void)
{
	return IsThisNode();
	}

BOOL CCAN29bitIdEntryRawHandler::HandleRequest(void)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawHandler::HandleSpecificRequest(PDU * pPDU, BOOL fRTR)
{
	return FALSE;
	}
		
BOOL CCAN29bitIdEntryRawHandler::HandleRequest(PDU * pPDU)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawHandler::HandleClaimAddressReq(PDU * pPDU)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawHandler::HandleClaimAddress(void)
{
	return FALSE;
	}
		
BOOL CCAN29bitIdEntryRawHandler::HandleTransport(void)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawHandler::HandleRapidFire(PDU * pPDU)
{
	return FALSE;
	}

void CCAN29bitIdEntryRawHandler::DoNak(void)
{
	}

void CCAN29bitIdEntryRawHandler::FlopBytes(PDU * pPDU, UINT uCount)
{
	}

// Overridables

BOOL CCAN29bitIdEntryRawHandler::IsThisNode(void)
{
	ID id;
	
	id.m_Ref = m_Rx.m_ID;

	for( UINT u = 0; u < m_uPDUs; u++ ) {

		DWORD dwRef = m_pPDUs[u].m_ID.m_Ref;

		if( dwRef == id.m_Ref ) {

			SetSA(id.i.m_PS);

			return TRUE;
			}
		}
	
	return FALSE;
	}

// Helpers

BOOL CCAN29bitIdEntryRawHandler::IsDestinationSpecific(ID id)
{
	return FALSE;
       	}

BOOL CCAN29bitIdEntryRawHandler::IsBroadcast(ID id)
{
	return FALSE;
	}

BOOL CCAN29bitIdEntryRawHandler::IsDM(PDU * pPDU)
{
	return FALSE;
	}

// End of File



// End of File

