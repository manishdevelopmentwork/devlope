
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformCanyonLvds_HPP

#define INCLUDE_PlatformCanyonLvds_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPwm437;
class CPru437;

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PlatformCanyon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Canyon LVDS HMI 
//

class CPlatformCanyonLvds : public CPlatformCanyon
{
	public:
		// Constructor
		CPlatformCanyonLvds(UINT uModel);

		// Destructor
		~CPlatformCanyonLvds(void);

		// IDevice
		BOOL METHOD Open(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

	protected:
		// Data Members
		CPwm437 * m_pPwm[3];
		CPru437 * m_pPru[2];

		// IPortSwitch
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);

		// IUsbSystemPortMapper
		UINT METHOD GetPortType(UsbTreePath const &Path);

		// Inititialisation
		void InitClocks(void);
		void InitMux(void);
		void InitGpio(void);
		void InitMisc(void);
		void InitPru(void);
	};

// End of File

#endif
