
#include "intern.hpp"

#include "sma.hpp"

//////////////////////////////////////////////////////////////////////////
//
//  SMA Base Driver
//

// Constructor

CSmaDriver::CSmaDriver(void)
{
	memset(m_pBuff, 0, sizeof(m_pBuff));

	m_pExtra = NULL;
	}

// Destructor

CSmaDriver::~CSmaDriver(void)
{
	}

// Entry Points

CCODE MCALL CSmaDriver::Ping(void)
{
	if( !m_pBase->m_fInit ) {

		m_pBase->m_fAux = FALSE;

		for( UINT u = 0; u < 2; u++ ) {

			if( !m_pBase->m_fAux && m_pBase->m_pBook ) {	// Primary device has map!

				ToggleAux();

				continue;
				}

			if( m_pBase->m_fAux && m_pBase->m_pBook2 ) {	// Fallback device has map!

				ToggleAux();

				break;
				}

			BOOL fSaved = FALSE;

			if( m_pBase->m_fMemStore && !m_pBase->m_fAux && !m_pBase->m_pBook ) {	// Primary device map storage!

				if( m_pExtra ) {

					PCTXT pName = "SMA_%3.3u.map";

					PTXT  pFile = "";

					SPrintf(pFile, pName, m_pBase->m_Addr);

					m_uBuff = elements(m_pBuff);

					fSaved = m_pExtra->ReadFile(pFile, m_pBuff, m_uBuff);
					}
				}

			if( m_pBase->m_fMemStore2 && m_pBase->m_fAux && !m_pBase->m_pBook2 ) {	// Fallback device map storage!

				if( m_pExtra ) {

					PCTXT pName = "SMA_%3.3u.map";

					PTXT  pFile = "";

					SPrintf(pFile, pName, m_pBase->m_Addr2);

					m_uBuff = elements(m_pBuff);

					fSaved = m_pExtra->ReadFile(pFile, m_pBuff, m_uBuff);
					}
				}

			Start();

			AddAddress();

			AddControl();

			AddTele(CMD_GET_CINFO);

			AddFCS();

			Stop();

			if( fSaved || Transact() ) {

				if( MakeLookup() ) {

					if( !fSaved && m_pBase->m_fAux ? m_pBase->m_fMemStore2 : m_pBase->m_fMemStore) {

						PCTXT pName = "SMA_%3.3u.map";

						PTXT  pFile = "";

						SPrintf(pFile, pName, m_pBase->m_fAux ? m_pBase->m_Addr2 : m_pBase->m_Addr);

						m_pExtra->WriteFile(pFile, m_pBuff, m_uBuff);
						}

					LookupMap();

					//PrintLookup();

					//FindSN();

					ToggleAux();

					if( !m_pBase->m_fAux ) {

						break;
						}
					}
				}

			if( u == 1 ) {

				ToggleAux();

				u = 0;
				}			
			}

		if( m_pBase->m_pBook || m_pBase->m_pBook2 ) {

			m_pBase->m_fInit = TRUE;

			m_pBase->m_fAux  = FALSE;	// Ensure primary is first in line!
			}

		m_pBase->m_fReset = FALSE;
		}

	if( m_pBase->m_fInit ) {

		if( COMMS_SUCCESS(DoPoll(m_pBase->m_pDoPoll[0])/*Search()*/) ) {

			if( !m_pBase->m_fReset ) {

				return CCODE_SUCCESS;
				}

			m_pBase->m_fReset = FALSE;

			return CCODE_ERROR;
			}

		ToggleAux();
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSmaDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	Channel * pChan = FindChannel(Addr);

	if( !pChan ) {

		m_pBase->m_fInit = FALSE;

		return CCODE_ERROR;
		}

	if( m_pBase->m_fReset ) {

		return CCODE_ERROR;
		}

	if( pChan ) {

		BYTE bIndex = FindPoll(pChan->Type2);

		if( m_pBase->m_Time[bIndex] && !IsTimedOut(m_pBase->m_Time[bIndex]) ) {

			GetData(Addr, pData, uCount);

			return uCount;
			}

		if( m_pBase->m_fAux ) {

			ToggleAux();

			if( COMMS_SUCCESS(DoPoll(pChan->Type2) ) ) {

				m_pBase->m_fReset = TRUE;

				m_pBase->m_fInit = TRUE;

				m_pBase->m_Time[bIndex] = 0;

				return CCODE_ERROR;
				}

			ToggleAux();
			}

		if( COMMS_SUCCESS(DoPoll(pChan->Type2) ) ) {

			if( GetData(Addr, pData, uCount ) ) {

				m_pBase->m_Time[bIndex] = GetTickCount();

				return uCount;
				}
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CSmaDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	// Writes not supported at this time due to strong warnings and inability to test fully.

/*	Start();

	AddAddress();

	AddControl();

	AddTele(CMD_SET_DATA);

	Channel * pChan = FindChannel(Addr);

	if( pChan ) {

		AddMask(pChan->Type1, pChan->Type2, pChan->Index);

		AddByte(1);

		AddByte(0);

		switch( pChan->Size ) {

			case sizeByte:	AddByte(pData[0] & 0xFF	 );	break;
			case sizeWord:	AddWord(pData[0] & 0xFFFF);	break;
			case sizeDWord:
			case sizeFloat: AddLong(pData[0]);		break;
			}

		AddFCS();

		Stop();

		if( Transact() ) {

			Poll();

			return 1;
			}
		}

	return CCODE_ERROR;*/

	return uCount;
	}

// Implementation

void CSmaDriver::Start(void)
{
	m_uPtr = 0;

	m_FCS  = 0xFFFF;

	m_pTx[m_uPtr++] = 0x7E;
	}

void CSmaDriver::AddAddress(void)
{
	AddByte(0xFF);
	}

void CSmaDriver::AddControl(void)
{
	AddByte(0x03);
	}

void CSmaDriver::AddTele(BYTE bCode, BOOL fSearch)
{
	AddByte(0x40);

	AddByte(0x41);   

	// Source address

	AddByte(m_Src);

	AddByte(0x00);

	// Destination address

	if( !fSearch ) {

		AddByte(m_pBase->m_fAux ? m_pBase->m_Addr2 : m_pBase->m_Addr);
		}
	else {
		AddByte(0x00);
		}

	AddByte(0x00);

	// Control

	AddByte(fSearch ? 0x80 : 0x00);

	// Packet Counter

	AddByte(0x00);

	// Command 

	AddByte(bCode);
	}

void CSmaDriver::AddMask(BYTE bType1, BYTE bType2, BYTE bIndex)
{
	AddByte(bType1);

	AddByte(bType2);

	AddByte(bIndex);
	}

void CSmaDriver::AddFCS(void)
{
	WORD FCS = m_FCS ^ 0xFFFF;

	if( !HandleEscape(LOBYTE(FCS)) ) {

		m_pTx[m_uPtr++] = LOBYTE(FCS);
		}

	if( !HandleEscape(HIBYTE(FCS)) ) {

		m_pTx[m_uPtr++] = HIBYTE(FCS);
		}
	}

void CSmaDriver::Stop(void)
{
	m_pTx[m_uPtr++] = 0x7E;
	}

BOOL CSmaDriver::Check(void)
{
	m_FCS = 0xFFFF;

	for(UINT u = 0; u < m_uPtr - 2; u++ ) {

		AddToFCS(m_pRx[u]);
		}

	m_FCS ^= 0xFFFF;

	m_FCS = SHORT(LONG(MotorToHost(m_FCS)));

	WORD wCheck = PU2(m_pRx + m_uPtr - 2)[0];

	if( LOBYTE(wCheck) == HIBYTE(m_FCS) ) {

		if( HIBYTE(wCheck) == LOBYTE(m_FCS) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

void CSmaDriver::AddByte(BYTE bByte)
{
	if( !HandleEscape(bByte) ) {

		m_pTx[m_uPtr++] = bByte;

		AddToFCS(bByte);
		}
	}

void CSmaDriver::AddWord(WORD wWord)
{
	AddByte((wWord >>  0) & 0xFF);

	AddByte((wWord >>  8) & 0xFF);
	}

void CSmaDriver::AddLong(DWORD dwLong)
{
	AddByte((dwLong >>  0) & 0xFF);

	AddByte((dwLong >>  8) & 0xFF);

	AddByte((dwLong >> 16) & 0xFF);

	AddByte((dwLong >> 24) & 0xFF);
	}

void CSmaDriver::AddToFCS(BYTE bByte) 
{
	m_FCS = WORD((m_FCS >> 8) ^ CRCTable16[(m_FCS ^ bByte) & 0xFF]);
	}

BOOL CSmaDriver::MakeLookup(void)
{
	UINT uPtr  = 0;

	SetBookCount(FindDepth());

	UINT uBook = GetBookCount();

	if( uBook == NOTHING ) {

		return FALSE;
		}

	if( m_pBase->m_fAux ) {

		m_pBase->m_pBook2 = new Channel[uBook];
		}
	else {
		m_pBase->m_pBook = new Channel[uBook];
		}

	for( UINT b = 0; b < uBook && uPtr < m_uBuff; b++ ) {

		Channel * pChan = GetBook(b);

		pChan->Index = m_pBuff[uPtr++];

		pChan->Type1 = m_pBuff[uPtr++] & 0xF;

		pChan->Type2 = m_pBuff[uPtr++] & 0xF;

		pChan->Size  = m_pBuff[uPtr++] & 0xF;

		pChan->Depth = m_pBuff[uPtr++];

		pChan->Data  = 0;

		pChan->Error = TRUE;

		uPtr += 2;	// FILL
		
		memcpy(pChan->Name, m_pBuff + uPtr, 16);

		char * space = strchr(pChan->Name, ' ');

		while ( space ) {

			*space = '_';

			space = strchr(space + 1, ' ');
			}
		
		uPtr += 16;	
				
		if( IsAnalog(pChan->Type1) ) {

			memcpy(pChan->Unit, m_pBuff + uPtr, 8);

			uPtr += 8;

			pChan->Gain = PU4(m_pBuff + uPtr)[0];

			uPtr += 4;

			pChan->Offset = PU4(m_pBuff + uPtr)[0];

			uPtr += 4;

			continue;
			}

		if( IsDigital(pChan->Type1) ) {

			memcpy(pChan->Lo, m_pBuff + uPtr, 16);

			uPtr += 16;

			memcpy(pChan->Hi, m_pBuff + uPtr, 16);

			uPtr += 16;

			continue;
			}

		if( IsCounter(pChan->Type1) ) {

			memcpy(pChan->Unit, m_pBuff + uPtr, 8);

			uPtr += 8;

			pChan->Gain = PU4(m_pBuff + uPtr)[0];

			uPtr += 4;

			continue;
			}

		if( IsStatus(pChan->Type1) ) {

			WORD TSize = PU2(m_pBuff + uPtr)[0];

			pChan->TSize = SHORT(LONG(IntelToHost(TSize)));

			uPtr += 2;

			uPtr += pChan->TSize;

			continue;
			}
		}

	return TRUE;
	}

UINT CSmaDriver::FindDepth(void)
{
	UINT uPtr  = 0;

	UINT uElements = 0;

	do { 
		uElements++;

		uPtr++;	

		BYTE bType   = m_pBuff[uPtr++];

		uPtr++;

		uPtr += 20;	
				
		if( IsAnalog(bType) ) {

			uPtr += 16;

			continue;
			}

		if( IsDigital(bType) ) {

			uPtr += 32;

			continue;
			}

		if( IsCounter(bType) ) {

			uPtr += 12;

			continue;
			}

		if( IsStatus(bType) ) {

			WORD TSize = PU2(m_pBuff + uPtr)[0];

			TSize = SHORT(LONG(IntelToHost(TSize)));

			uPtr += TSize + 2;

			continue;
			}

		} while( uPtr < m_uBuff );

	return uElements;
	}

BOOL CSmaDriver::IsAnalog(BYTE bType)
{
	return bType & 0x1;	
	}

BOOL CSmaDriver::IsDigital(BYTE bType)
{
	return bType & 0x2;
	}

BOOL CSmaDriver::IsCounter(BYTE bType)
{
	return bType & 0x4;
	}

BOOL CSmaDriver::IsStatus(BYTE bType)
{
	return bType & 0x8;
	}

BOOL CSmaDriver::LookupMap(void)
{
	if( m_pBase->m_uParam ) {

		for( UINT p = 0; p < m_pBase->m_uParam; p++ ) {

			Param * pParam = GetParam(p);

			pParam->m_Channel = NOTHING;

			PTXT Name = pParam->m_Name;

			UINT uSize = strlen(Name);

			UINT uBook = GetBookCount();

			for( UINT b = 0; b < uBook; b++ ) {

				if( !memcmp(Name, GetBook(b)->Name, uSize) ) {

					pParam->m_Channel = b;

					BYTE bType2 = GetBook(b)->Type2;

					for( UINT u = 0; u < elements(m_pBase->m_pDoPoll); u++ ) {

						if( m_pBase->m_pDoPoll[u] == 0 ) {

							m_pBase->m_pDoPoll[u] = bType2;

							break;
							}

						if( m_pBase->m_pDoPoll[u] == bType2 ) {

							break;
							}
						}

					break;
					}
				}
			}

		//PrintMap();

		return TRUE;
		}

	return FALSE;
	}

CCODE CSmaDriver::Poll(void)
{
	for( UINT u = 0; u < elements(m_pBase->m_pDoPoll); u++ ) {

		if( m_pBase->m_pDoPoll[u] != 0 ) {

			if( !COMMS_SUCCESS(DoPoll(m_pBase->m_pDoPoll[u])) ) {

				return CCODE_ERROR;
				}

			continue;
			}

		break;
		}

	return CCODE_SUCCESS;
	}

CCODE CSmaDriver::DoPoll(BYTE bType2)
{
	Start();

	AddAddress();

	AddControl();

	AddTele(CMD_GET_DATA);

	AddMask(0xF, bType2, 0);

	AddFCS();

	Stop();
	
	if( Transact() ) {

		StoreData(bType2);

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

CCODE CSmaDriver::Sync(void)
{
	Start();

	AddAddress();

	AddControl();

	AddTele(CMD_SYN_ONLINE);

	AddFCS();

	Stop();

	Send();

	Delay();

	return CCODE_SUCCESS;
	}

CCODE CSmaDriver::Search(void)
{
	Start();

	AddAddress();

	AddControl();

	AddTele(CMD_SEARCH_DEV, TRUE);

	DWORD dwSN = GetSN();

	AddByte((dwSN >>  0) & 0xFF);

	AddByte((dwSN >>  8) & 0xFF);

	AddByte((dwSN >> 16) & 0xFF);

	AddByte((dwSN >> 24) & 0xFF);	

	AddFCS();

	Stop();

	if( Transact() ) {

		return CCODE_SUCCESS;
		}

	return CCODE_ERROR;
	}

BYTE CSmaDriver::FindPoll(BYTE bType2)
{
	for( UINT u = 0; u < elements(m_pBase->m_pDoPoll); u++ ) {

		if( m_pBase->m_pDoPoll[u] == bType2 ) {

			return u;
			}
		}

	return 0;		
	}

BOOL CSmaDriver::FindSN(void)
{
	Start();

	AddAddress();

	AddControl();

	AddTele(CMD_GET_DATA);

	AddMask(0xF, 0x4, 0);

	AddFCS();

	Stop();
	
	if( Transact() ) {

		DWORD dwSN = PU4(m_pBuff + 5)[0];

		if( m_pBase->m_fAux ) {

			m_pBase->m_SN2 = IntelToHost(dwSN);
			}
		else {
			m_pBase->m_SN = IntelToHost(dwSN);
			}
		
		return TRUE;
		}

	return FALSE;
	}

void CSmaDriver::StoreData(BYTE bType2)
{
	UINT uPtr = (bType2 & 0x8) ? 13 : 5;

	UINT uBook = GetBookCount();

	if( uBook == NOTHING ) {

		return;
		}

	for( UINT b = 0; b < uBook && uPtr < m_uBuff; b++ ) {

		Channel * pChan = GetBook(b);

		if( pChan ) {

			if( pChan->Type2 == bType2 ) {

				WORD w = 0;

				DWORD d = 0;

				switch( pChan->Size ) {

					case sizeByte:

						pChan->Data = m_pBuff[uPtr++];

						pChan->Error = FALSE;

						break;

					case sizeWord:

						w = PU2(m_pBuff + uPtr)[0];

						pChan->Data = SHORT(LONG(IntelToHost(w)));
						
						uPtr += 2;

						pChan->Error = FALSE;

						break;

					case sizeDWord:
					case sizeFloat:

						d = PU4(m_pBuff + uPtr)[0];

						pChan->Data = IntelToHost(d);

						uPtr += 4;

						pChan->Error = FALSE;

						break;
					}
				}
			}
		}
	}

BOOL CSmaDriver::GetData(AREF Addr, PDWORD pData,  UINT uCount)
{
	UINT i = 0;

	for( UINT u = 0; u < m_pBase->m_uParam && i < uCount;  ) {

		if( GetParam(u)->m_Addr.m_Ref == Addr.m_Ref + i ) {

			UINT uChannel = GetParam(u)->m_Channel;

			if( uChannel < NOTHING ) {

				if( !GetBook(uChannel)->Error ) {

					pData[i] = GetBook(uChannel)->Data;
					}
				}

			i++;

			u = 0;

			continue;
			}

		u++;

		if( u == m_pBase->m_uParam ) {

			pData[i] = 0;

			i++;

			u = 0;
			}
		}

	return i == uCount;
	}

// Transport

BOOL CSmaDriver::Transact(void)
{
	if( Send() && Recv() ) {

		m_pBase->m_fError = FALSE;

		Delay();

		return TRUE;
		}

	if( m_pBase->m_fInit ) {

		m_pBase->m_fError = TRUE;
		}

	Delay();

	return FALSE;
	}

BOOL CSmaDriver::Send(void)
{
	return FALSE;
	}

BOOL CSmaDriver::Recv(void)
{
	return FALSE;
	}

void CSmaDriver::SendAck(void)
{
	Start();

	for( UINT u = 1; u < 12; u++ ) {

		if( u == 10 ) {

			AddByte(m_pRx[9]);

			continue;
			}

		AddByte(m_pTx[u]);
		}

	AddFCS();

	Stop();

	Send();	
	}

void CSmaDriver::Delay(void)
{
	UINT uTick = GetTickCount();

	UINT uCount = ToTicks(25);

	while( int(GetTickCount() - uTick - uCount) < 0 ) {

		if( uTick > GetTickCount() ) {

			break;
			}
		}
	}

// Helpers

void CSmaDriver::ToggleAux(void)
{
	if( m_pBase->m_Addr2 ) {

		m_pBase->m_fAux = !m_pBase->m_fAux;
		}
	}

UINT CSmaDriver::GetBookCount(void)
{
	return m_pBase->m_fAux ? m_pBase->m_uBook2 : m_pBase->m_uBook;
	}

void CSmaDriver::SetBookCount(UINT uCount)
{
	if( m_pBase->m_fAux ) {

		m_pBase->m_uBook2 = uCount;

		return;
		}

	m_pBase->m_uBook = uCount;
	}


Channel * CSmaDriver::GetBook(UINT uIndex)
{
	if( m_pBase->m_fAux ) {

		if( m_pBase->m_pBook2 ) {

			return &m_pBase->m_pBook2[uIndex];
			}

		return NULL;
		}

	if( m_pBase->m_pBook ) {

		return &m_pBase->m_pBook[uIndex];
		}

	return NULL;
	}

Param * CSmaDriver::GetParam(UINT uIndex)
{
	if( m_pBase->m_fAux ) {

		return &m_pBase->m_pParam2[uIndex]; 
		}

	return &m_pBase->m_pParam[uIndex];
	}

DWORD  CSmaDriver::GetSN(void)
{
	return m_pBase->m_fAux ? m_pBase->m_SN2 : m_pBase->m_SN;
	}

BOOL CSmaDriver::IsTimedOut(UINT uTime)
{
	return int(GetTickCount() - uTime - m_pBase->m_Poll) >= 0;
	}

BOOL CSmaDriver::IsMultiPacket(void) 
{
	return m_pRx[9] != 0;
	}

BOOL CSmaDriver::HandleEscape(BYTE bByte)
{
	if( bByte == 0x7E || bByte == 0x7D ) {

		m_pTx[m_uPtr++] = 0x7D;

		AddToFCS(0x7D);

		m_pTx[m_uPtr++] = bByte ^ 0x20;

		AddToFCS(bByte ^ 0x20);

		return TRUE;
		}

	return FALSE;
	}

Channel * CSmaDriver::FindChannel(AREF Addr)
{
	for( UINT p = 0; p < m_pBase->m_uParam; p++ ) {

		if( GetParam(p)->m_Addr.m_Ref == Addr.m_Ref ) {

			return GetBook(GetParam(p)->m_Channel);
			}
		}

	return NULL;
	}

void CSmaDriver::PrintLookup(void)
{
	UINT uBook  = m_pBase->m_fAux ? m_pBase->m_uBook2 : m_pBase->m_uBook;

	for( UINT b = 0; b < uBook; b++ ) {

		Channel * pChan = GetBook(b);

		if( pChan->Index == 0 ) {

			return;
			}

		AfxTrace("\nIndex %2.2x Type 1 %2.2x Type 2 %2.2x Size %2.2x Depth %2.2x Name %s", pChan->Index, 
												   pChan->Type1,
												   pChan->Type2,
												   pChan->Size,
												   pChan->Depth,
												   pChan->Name);
		}
	}


void CSmaDriver::PrintDeviceList(void)
{
	for( UINT p = 0; p < m_pBase->m_uParam; p++ ) {

		AfxTrace("\nDev List %u of %u is %s", p, m_pBase->m_uParam, m_pBase->m_pParam[p].m_Name);
		}
	}

void CSmaDriver::PrintMap(void)
{
	for( UINT p = 0; p < m_pBase->m_uParam; p++ ) {

		AfxTrace("\nDev List %u of %u is %s at index %u ref %8.8x", p, m_pBase->m_uParam, m_pBase->m_pParam[p].m_Name, m_pBase->m_pParam[p].m_Channel, m_pBase->m_pParam[p].m_Addr);
		}
	}


// End of File
