//////////////////////////////////////////////////////////////////////////
//
// FlowComm Constants
//

#define FLOW_READ	0x68
#define FLOW_WRITE	0x70
#define FLOW_TIMEOUT	500
#define FLOW_MAX	80
#define FLOW_OFFSET	0x20

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Macro's
//

#define READ(d)		(FLOW_READ  | d)
#define WRITE(d)	(FLOW_WRITE | d)

//////////////////////////////////////////////////////////////////////////
//
// FlowComm Master Serial Driver
//

class CFlowCommMasterSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CFlowCommMasterSerialDriver(void);

		// Destructor
		~CFlowCommMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		
	protected:

		// Device Context
		struct CContext
		{
			BYTE m_bDrop;
			};

		// Data Members
		CContext * m_pCtx;

		BYTE m_bTxBuff[84];
		BYTE m_bRxBuff[84];
		UINT m_uPtr;

		// Implementation
		void AddByte(BYTE bByte);

		// Transport
		void SendHeader(BYTE bCommand, BYTE bInstruct);
		void SendAck(void);
		BOOL SendData(PDWORD pData, UINT uCount);
		BOOL RecvData(void);
		BOOL RecvAck(void);

		// Helpers
		BYTE CalcSPLRC(PBYTE pBuff);
		UINT FindCode(UINT uOffset);
	};

// End of File
