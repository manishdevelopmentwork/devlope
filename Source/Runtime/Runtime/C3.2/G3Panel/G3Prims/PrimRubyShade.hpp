
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_RubyShade_HPP
	
#define	INCLUDE_RubyShade_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyTankFill.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Shading Brush
//

class DLLAPI CPrimRubyShade : public CPrimRubyTankFill
{
	public:
		// Constructor
		CPrimRubyShade(void);

		// Destructor
		~CPrimRubyShade(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Operations
		BOOL DrawPrep(IGDI *pGDI);

		// Recoloring
		PCBYTE MakeCopy(IGdi *pGdi, PCBYTE pData, int cx, int cy);
		void   Recolor(IGdi *pGdi, PDWORD pData, int cx, int cy);

		// Data Members
		UINT m_Mask;

	protected:
		// Data Members
		PDWORD m_pData;

		// Implementation
		DWORD To888(BYTE a, WORD c);
	};

// End of File

#endif
