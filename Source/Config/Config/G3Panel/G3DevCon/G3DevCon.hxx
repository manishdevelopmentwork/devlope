
////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3DevCon_HXX

#define	INCLUDE_G3DevCon_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <pcdesk.hxx>

#include <pcwin.hxx>

// End of File

#endif
