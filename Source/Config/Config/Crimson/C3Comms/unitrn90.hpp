
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_UNITRONICSM90_HPP

#define	INCLUDE_UNITRONICSM90_HPP

//////////////////////////////////////////////////////////////////////////
//
// Unitronics M90 Master Driver
//

class CUnitronicsM90Driver : public CStdCommsDriver
{
	public:
		// Constructor
		CUnitronicsM90Driver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration

		// Address Management

		// Address Helpers

		// Helpers

	protected:
		// Data

		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
