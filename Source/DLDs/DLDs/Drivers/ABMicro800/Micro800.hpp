//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Series TCP Driver
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_AB_MICRO_800_TCP

#define INCLUDE_AB_MICRO_800_TCP

#include "IOISegment.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define UTC_DIFF	852076800

/////////////////////////////////////////////////////////////////////////
//
// CIP Services
//

#define CIP_READ		0x4C

#define CIP_WRITE		0x4D

struct CTagDesc 
{
	PTXT	m_pName;
	UINT	m_uIndex;
	UINT	m_Dims[3];
	WORD    m_CipType;
	BYTE    m_Chars;
	};

 //////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Enumerations
//

enum type {

	typeBOOL  = 0,
	typeUSINT = 1,
	typeINT   = 2, 
	typeDINT  = 3, 
	typeREAL  = 4,
	typeSTR   = 5,
	typeSINT  = 6,
	typeBYTE  = 7,
	typeUINT  = 8,
	typeWORD  = 9,
	typeUDINT = 10,
	typeDWORD = 11,
	typeTIME  = 12,
	typeDATE  = 13,
	typeLINT  = 14,
	typeULINT = 15,
	typeLWORD = 16,
	typeLREAL = 17,
	};

enum code {

	codeBOOL  = 0xC1,
	codeUSINT = 0xC6,
	codeINT   = 0xC3,
	codeDINT  = 0xC4,
	codeREAL  = 0xCA,
	codeSTR   = 0xDA,
	codeSINT  = 0xC2,
	codeBYTE  = 0xC6,
	codeUINT  = 0xC7,
	codeWORD  = 0xC7,
	codeUDINT = 0xC8,
	codeDWORD = 0xC8,
	codeTIME  = 0xC8,
	codeDATE  = 0xC8,
	codeLINT  = 0xC5,
	codeULINT = 0xC9,
	codeLWORD = 0xD4,
	codeLREAL = 0xCB,
	};

enum dims {

	dimsX = 0,
	dimsY = 1,
	dimsZ = 2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Micro800 Series Driver
//

class CABMicro800Master : public CMasterDriver
{
	public:
		// Constructor
		CABMicro800Master(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Detach(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			DWORD	   m_IP;			
			WORD       m_wTimeout;
			UINT       m_uCount;
			CTagDesc * m_pTags;
			UINT	   m_Map[255];
			};

		// Data Members
		IEthernetIPHelper * m_pEnetHelper;
		IExplicit         * m_pExplicit;
		CContext          * m_pCtx;
		BOOL		    m_fOpen;
		BYTE		    m_bRxBuff[512];
		BYTE		    m_bTxBuff[512];
		UINT		    m_uPtr;
		BYTE		    m_bError;

		// Implementation
		BOOL ReadData(CTagDesc * pTag, AREF Addr, UINT &uCount);
		BOOL ReadTag(CTagDesc &Tag, AREF Addr, UINT &uCount);
		BOOL WriteData(CTagDesc * pTag, AREF Addr, PDWORD pData, UINT &uCount);
		UINT WriteString(CTagDesc * pTag, AREF Addr, PDWORD pData, UINT uCount);
		void GetTags(PCBYTE &pData);
		PTXT GetString(PCBYTE &pData);
		void CleanupTagNames(void);
		BOOL IsArray(CTagDesc *pTag);
		void AddOffset(CIOISegment &Name, CTagDesc *pTag, AREF Addr);
		CTagDesc *FindTagDesc(AREF Addr);
		static int SortFunc(void const *p1, void const *p2);
		static int CompareFunc(void const *p1, void const *p2);

		// CIP Support
		BOOL Send(BYTE bCode, CIOISegment &Name);
		BOOL Recv(void);
		BOOL AddCIPType(UINT uType);
		UINT GetCIPCode(UINT uType);
		UINT GetCount(CTagDesc * pDesc, UINT &uCount, AREF Addr);
		void AddCount(UINT uCount);
		void AddBits(PDWORD pData, UINT uCount);

		// Transport Layer
		BOOL MakeLink(void);
		BOOL CheckLink(void);

		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddByte(PDWORD pData, UINT uCount);
		void AddWord(PDWORD pData, UINT uCount);
		void AddLong(PDWORD pData, UINT uCount);
		void AddDate(PDWORD pData, UINT uCount);
		void AddString(PDWORD pData, UINT uCount);
		void AddDouble(PDWORD pData, UINT uCount);
		void CopyByte(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyWord(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyLong(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyDouble(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyDate(PBYTE pReply, PDWORD pData, UINT uCount);
		void CopyChar(PBYTE pReply, PDWORD pData, UINT uCount);
		UINT CopyString(PBYTE pReply, PDWORD &pData, UINT uCount, UINT uChars);

		// Map Support
		void MakeMap(void);
		UINT GetMapIndex(AREF Addr);
		UINT GetMapCount(AREF Addr, UINT uMap);
		
		// Helpers
		BOOL IsSTR(UINT uCode);
		BOOL IsTIME(UINT uCode);
		BOOL IsDATE(UINT uCode);
		BOOL IsBOOL(UINT uCode);
		BOOL IsBYTE(UINT uCode);
		BOOL IsWORD(UINT uCode);
		BOOL IsLONG(UINT uCode);
		BOOL IsREAL(UINT uCode);
		BOOL IsDouble(UINT uCode);
		BOOL IsStringText(UINT uChar);
		UINT GetStrCount(CTagDesc * pDesc, UINT &uCount);
		UINT GetCharCount(PDWORD pData, UINT uOffset);
		UINT GetSize(CTagDesc * pDesc);
		UINT GetMaxCount(CTagDesc * pDesc);
		UINT RemoveTrailing(PDWORD pData, BYTE bByte, UINT uBytes);
		void PrintTags(void);
		void PrintMap(void);
	};

#endif

// End of File
