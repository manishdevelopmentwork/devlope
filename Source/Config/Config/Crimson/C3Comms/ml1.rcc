//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// UI for CML1DriverOptions 
//

CML1DriverOptionsUIList RCDATA
BEGIN
	"Server,Server ID,,CUIEditInteger,|0||1|254,"
	"Indicate the server identification number of this device."
	"\0"

	"AuthInt,Authentication Request Interval,,CUIEditInteger,|0|sec|0|2000000,"
	"Specify the time interval authentication should be requested on power up.  "
	"This interval will be maintained until authentication is acheived.  "
	"\0"

	"\0"
END

CML1DriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Driver Settings,AuthInt\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CML1SerialDriverOptions 
//

CML1SerialDriverOptionsUIList RCDATA
BEGIN

	"Track,Frame End Detection,,CUIDropDown,Detect via Timing|Track Frame Contents,"
	"Indicate the method which should be used to detect the end of an ML1 frame."
	"\0"	

	"\0"
END

CML1SerialDriverOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Driver Settings,AuthInt,Track\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CML1DeviceOptions 
//

CML1DeviceOptionsUIList RCDATA
BEGIN
	"Server,Server ID,,CUIEditInteger,|0||1|255,"
	"Indicate the server identification number of this device."
	"\0"

	"Unit,Unit ID,,CUIEditInteger,|0||0|16777114,"
	"Indicate the unit identification number of this device."
	"\0"

	"PingReg,Ping Holding Register,,CUIEditInteger,|0||0|65535,"
	"Indicate the Holding Register that the driver should use to detect the online "
	"device.  You must be sure that this register is valid in the target device or "
	"the connection will never be established."
	"\0"

	"TO,Timeout,,CUIEditInteger,|0|seconds|0|65535,"
	"Indicate the amount of time in seconds the driver should wait for a reply to a request "
	"before resending the request."
	"\0"

	"Retry,Retry,,CUIEditInteger,|0||0|255,"
	"Indicate the number of times the driver should resend a specific request with no response "
	"from the server."
	"\0"

	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for CML1TCPDeviceOptions
//

CML1TCPDeviceOptionsUIList RCDATA
BEGIN
	"IpSel1,IP Address Selection,,CUIDropDown,Use Fixed IP Address|Use Name via DNS,"
	"Indicate if the primary IP address of the ML1 device should be based upon a fixed IP address "
	"or a name using DNS lookup."
	"\0"

	"Addr1,Fixed IP Address,,CUIIPAddress,,"
	"Indicate the IP address of the ML1 device."
	"\0"

	"Text1,Domain Name,,CUIEditString,256,"
	"Indicate the name of the ML1 device. Please ensure that DNS is enabled under Network Settings."
	"\0"

	"Addr2,Fallback IP Address,,CUIIPAddress,,"
	"Indicate the secondary IP address of the ML1 device. This will be used "
	"if the primary address does not respond, and can be used to implement "
	"redundant communications. Leave the field at 0.0.0.0 to disable fallback."
	"\0"

	"Socket,TCP Port,,CUIEditInteger,|0||1|65535,"
	"Indicate the TCP port number on which the ML1 protocol is to operate. "
	"The default value is suitable for most applications."
	"\0"

	"Keep,Link Type,,CUIDropDown,Use Shared Socket|Use Dedicated Socket,"
	"Indicate whether you want the ML1 driver to use a dedicated socket for this "
	"device. A dedicated socket results in higher performance as the driver does not "
	"have to close and re-open the connection when switching between devices. The "
	"driver will not allocate more than four dedicated sockets."
	"\0"

	"Ping,ICMP Ping,,CUIDropDown,Disabled|Enable,"
	"Indicate whether you want the driver to use an ICMP ping frame to check if "
	"the ML1 server is available. If you enable this feature, Crimson will take "
	"less time to dectect an offline device. You must be sure that the target device "
	"supports ICMP pings or the connection will never be established."
	"\0"

	"Time1,Connection Timeout,,CUIEditInteger,|0|ms|100|600000,"
	"Indicate the period for which the driver will wait when trying to establish a "
	"TCP/IP connection to the remote device. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time2,Transaction Timeout,,CUIEditInteger,|0|ms|100|60000,"
	"Indicate the period for which the driver will wait when waiting for a reply to "
	"a ML1 request or an ICMP ping. Smaller values produce quicker detection "
	"of offline devices, but can only be used on faster links. Larger values make "
	"the driver more tolerant of slower connections."
	"\0"

	"Time3,Connection Backoff,,CUIEditInteger,|0|ms|0|5000,"
	"Indicate the period for which the driver will wait before reconnecting to a "
	"device to which it has recently connected. This timer is used to prevent problems "
	"when using a shared socket. If the time is too low, the slave device may not be "
	"able to cope with the rapid connections and disconnections that will occur."
	"\0"

	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// UI for ML1 Address Selection
//

CML1BlockUIList RCDATA
BEGIN
	"Space,Item,,CUIDropDown,,"
	""
	"\0"

	"Reg,Offset,,CUIEditInteger,|0||1|65535,"
	""
	"\0"

	"File,File,,CUIEditInteger,|0||1|64,"
	""
	"\0"

	"Record,Record,,CUIEditInteger,|0||1|10000,"
	""
	"\0"

	"Type,Type,,CUIDropDown,,"
	""
	"\0"
	
	"RBE,On Report By Exception,,CUIExpression,|None,"
	""
	"\0"

	"COV,On Change of Value,,CUIDropDown,No|Yes,"
	""
	"\0"

	"TO,Timeout,,CUIEditInteger,|0|seconds|1|65535,"
	""
	"\0"

	"Retry,Retries,,CUIEditInteger,|0||1|255,"
	""
	"\0"

	"\0"
END


//////////////////////////////////////////////////////////////////////////
//
// ML1 Block View Dialog
//
//

ML1BlockDlg DIALOG 0, 0, 0, 0
CAPTION "ML1 Block Dialog"
BEGIN
	LTEXT	 "<Block>"			1002,	  4,   4,  158,  14
	GROUPBOX "&Block List",			1000,	  4,   20, 158, 312
	LISTBOX					1001,	 10,   34, 146, 302, XS_LISTBOX | LBS_USETABSTOPS

	////////
	DEFPUSHBUTTON	"OK",			IDOK,	  59,  340,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Copy To File",		1003,	 103,  340,  60,  14, XS_BUTTONREST
	
END

// End of File
