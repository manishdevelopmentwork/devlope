
#include "Intern.hpp"

#include "HalGraphite.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Graphite HMI
//

// Instantiator

bool Create_HalGraphite(void)
{
	(New CHalGraphite)->Open();

	return true;
	}

// Constructor

CHalGraphite::CHalGraphite(void)
{
	m_uDebugAddr = ADDR_UART3;

	m_uDebugLine = INT_UART3;
	}

// End of File
