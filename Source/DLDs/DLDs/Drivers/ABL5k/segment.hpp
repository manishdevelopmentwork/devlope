
#ifndef	INCLUDE_SEGMENT_HPP

#define	INCLUDE_SEGMENT_HPP

//////////////////////////////////////////////////////////////////////////
//
// EIP Master Driver Interface
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class     CCIPPath;
class     CSegment;
interface IEIPMaster;

//////////////////////////////////////////////////////////////////////////
//
// Data Buffer
//

#define BUFF_SIZE	512

class CDataBuf
{
	public:
		// Constructor
		CDataBuf(void);
		CDataBuf(PBYTE pData, UINT uSize);

		// Attributes
		UINT  GetFree(void);
		UINT  GetSize(void);
		PBYTE GetDataPtr(void);

		// Management
		void Alloc(UINT uAlloc);
		void Empty(void);

		// Encoding
		void Encode(CCIPPath &Path);
		void Encode(CSegment *pSeg);
		void AddByte(BYTE Data);
		void AddWord(WORD Data);
		void AddLong(DWORD Data);
		void AddData(PBYTE pData, UINT uSize);

		// Parsing
		BYTE  GetByte(UINT &uIndex);
		WORD  GetWord(UINT &uIndex);
		DWORD GetLong(UINT &uIndex);

	protected:
		// Data Members
		UINT	m_uIndex;
		BYTE	m_bData[BUFF_SIZE];
		UINT	m_uAlloc;
		PBYTE	m_pBuff;
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment Types
//

enum {
	segPort,
	segLogical,
	segNetwork,
	segSymbolic,
	segData,
	segDataTypeC,
	segDataTypeE,
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment
//

struct CSEG {

	BYTE	m_bType;
//	BYTE	m_bData[];
};

class CSegment : public CSEG
{
	public:
		// Constructor
		CSegment(void);

		// Destructor
		virtual ~CSegment(void);

		// Attributes
		UINT GetType(void) const;
		UINT GetFormat(void) const;
		BOOL GetPadded(void) const;

		// Encoding
		virtual UINT GetLength(void) const;
		virtual UINT Encode(CDataBuf &Buff);

		// Parsing
		virtual UINT Parse(PBYTE pData, UINT uSize);

		// Path
		void SetPath(CCIPPath *pPath);

		// Linked List
		CSegment	* m_pPrev;
		CSegment	* m_pNext;

	protected:
		// Data
		CCIPPath	* m_pPath;

		// Implementation
		void SetType(UINT uType);
		void SetFormat(UINT uFormat);
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Logical Type

enum {
	logClass,
	logInstance,
	logMember,
	logConnPoint,
	logAttribute,
	logSpecial,
	logService,
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Logical Format

enum {
	addr8bit,
	addr16bit,
	addr32bit,
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Logical
//

class CSegLogical : public CSegment
{
	public:
		// Constructor
		CSegLogical(void);
		CSegLogical(UINT uType, DWORD dwValue);

		// Operations
		UINT GetLogType(void) const;
		UINT GetLogFormat(void) const;

		// Operations
		void SetLogType(UINT uType);
		void SetLogValue(DWORD dwValue);
		void SetLogFormat(UINT uFormat);

		// Encoding
		UINT GetLength(void) const;
		UINT Encode(CDataBuf &Buff);

	protected:
		// Data
		DWORD	m_dwValue;

		// Implementation
		UINT FindLogFormat(void) const;
		BOOL UsePadding(void) const;
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data Segment SubType

enum {
	dataSimple = 0x00,
	dataSymbol = 0x11,
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data
//

class CSegData : public CSegment
{
	public:
		// Constructor
		CSegData(void);

		// Destructor
		~CSegData(void);

		// Operations
		void SetSimple(PBYTE pData, UINT uSize);
		void SetSymbol(PCTXT pSymbol);
		void SetSymbol(PBYTE pData, UINT uSize);

		// Encoding
		UINT GetLength(void) const;
		UINT Encode(CDataBuf &Buff);

	protected:
		// Data
		union CData {

			BYTE	b;
			WORD	w;
			DWORD	l;
			PBYTE	p;
		};

		CData	m_Data;
		UINT	m_uSize;

		// Encoding Help
		UINT EncodeSimple(CDataBuf &Buff);
		UINT EncodeSymbol(CDataBuf &Buff);

		// Implementation
		void Free(void);
		BOOL UsePadding(void) const;
};

//////////////////////////////////////////////////////////////////////////
//
// Elementary Data Type Specification
//

enum {
	typeAlt,
	typeBool,
	typeSint,
	typeInt,
	typeDint,
	typeLint,
	typeUsint,
	typeUint,
	typeUdint,
	typeUlint,
	typeReal,
	typeLreal,
	typeStime,
	typeData,
	typeTod,
	typeDat,
	typeStr1,
	typeByte,
	typeWord,
	typeDword,
	typeLword,
	typeStr2,
	typeFtime,
	typeLtime,
	typeItime,
	typeStrN,
	typeShstr,
	typeTime,
	typeEpath,
	typeEngUnit,
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data Type Elementary
//

class CSegDataTypeE : public CSegment
{
	public:
		// Constructor
		CSegDataTypeE(void);

		// Destructor
		~CSegDataTypeE(void);

		// Attributes
		UINT  GetTypeCode(void) const;

		// Operations
		void SetTypeCode(UINT uType);

		// Encoding
		UINT GetLength(void) const;
		UINT Encode(CDataBuf &Buff);

		// Parsing
		UINT Parse(PBYTE pData, UINT uSize);

		// Data Parsing/Encoding
		UINT ParseData(PDWORD pData, UINT uCount);
		UINT EncodeData(CDataBuf &Buff, PDWORD pData, UINT uCount);

	protected:
		// Data
		PBYTE	m_pData;
		UINT	m_uSize;
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data Type Constructed Types
//

enum {
	typeAbbrevStruc,
	typeAbbrevArray,
	typeFormalStruc,
	typeFormalArray,
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Segment - Data Type Constructed
//

class CSegDataTypeC : public CSegment
{
	public:
		// Constructor
		CSegDataTypeC(void);

		// Attributes
		WORD GetTypeCRC(void);
		UINT GetTypeCode(void) const;

		// Parsing
		UINT Parse(PBYTE pData, UINT uSize);

	protected:
		// Data
		WORD	m_wCRC;

		// Parsing Help
		UINT ParseAbbrevStruct(PBYTE pData, UINT uSize);
};

//////////////////////////////////////////////////////////////////////////
//
// CIP Path
//

class CCIPPath
{
	public:
		// Constructor
		CCIPPath(void);
		CCIPPath(IEIPMaster *pDriver);

		// Destructor
		~CCIPPath(void);

		// Attributes
		CSegment * GetHead(void) const;
		CSegment * GetTail(void) const;
		UINT       GetCount(void) const;
		BOOL       GetPadded(void) const;

		// Management
		void            Empty(void);
		CSegment      * Append(CSegment *pSeg);

		// Encoding
		UINT GetLength(void) const;
		UINT Encode(CDataBuf &Buff);

		// Parsing
		UINT Parse(CBuffer *pBuffer);
		UINT Parse(PBYTE pData, UINT uSize);

	public:
		// Debug

		IEIPMaster *m_pDriver;

	protected:
		// Linked List
		CSegment	 * m_pHead;
		CSegment	 * m_pTail;
		UINT		   m_uCount;
		BOOL               m_fPadded;

		// Implementation
		void Free(void);
};

// End of File

#endif
