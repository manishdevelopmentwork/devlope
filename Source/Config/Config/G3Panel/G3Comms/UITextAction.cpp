
#include "Intern.hpp"

#include "UITextAction.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Action Item
//

// Dynamic Class

AfxImplementDynamicClass(CUITextAction, CUITextCoded);

// Constructor

CUITextAction::CUITextAction(void)
{
	}

// Default Data Type

void CUITextAction::FindReqType(CTypeDef &Type)
{
	Type.m_Type  = typeVoid;

	Type.m_Flags = flagActive;
	}

// End of File
