
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Bidi_HPP
	
#define	INCLUDE_Bidi_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "String.hpp"

#include "Unicode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Unicode Bidirectional Algorithm
//

class CBidi
{
	public:
		// Constructor
		CBidi(CUnicode Text);

		// Destructor
		~CBidi(void);

		// Attributes
		BOOL IsComplex(void);

		// Operations
		CUnicode GetDisplayOrder(void);
		void     GetDisplayOrder(CUIntArray &Order);
		void     GetDisplayOrder(CUIntArray &Order, CUIntArray &Level);

	protected:
		// Character Types
		enum CCharType
		{
			typeNA,

			// Strong
			typeL,
			typeLRE,
			typeLRO,
			typeR,
			typeAL,
			typeRLE,
			typeRLO,

			// Weak
			typePDF,
			typeEN,
			typeES,
			typeET,
			typeAN,
			typeCS,
			typeNSM,
			typeBN,

			// Neutral
			typeB,
			typeS,
			typeWS,
			typeON,
			};

		// Type Definition
		struct CSpanDef
		{
			WCHAR cFrom;
			WCHAR cTo;
			};

		// Character Info
		struct CCharInfo
		{
			WCHAR		m_cChar;
			CCharType	m_Type;
			UINT		m_uLevel;
			UINT		m_uPos;
			};

		// String Type Data
		static CSpanDef const m_RTL[];
		static CSpanDef	const m_LTR[];
		static CSpanDef	const m_LRM[];
		static CSpanDef	const m_RLM[];
		static CSpanDef	const m_PDF[];
		static CSpanDef	const m_EuropeNumber[];
		static CSpanDef	const m_EuropeSeparator[];
		static CSpanDef	const m_EuropeTerminator[];
		static CSpanDef	const m_ArabicNumber[];
		static CSpanDef	const m_CommonSeparator[];
		static CSpanDef	const m_BlockSeparator[];
		static CSpanDef	const m_SegmentSeparator[];
		static CSpanDef	const m_WhiteSpace[];
		static CSpanDef	const m_OtherNeutral[];
		static CSpanDef	const m_NonSpacingMark[];
		static CSpanDef	const m_ArabicLetter[];

		// Data Members
		CUnicode		m_Text;
		BOOL			m_fComplex;
		CCharType		m_BaseType;
		UINT			m_uBaseLevel;
		CCharType		m_SOR;
		CArray <CCharInfo>	m_Chars;
		CCharType		m_EOR;

		// Debug Help
		CStringArray		m_Rule;

		// Rule Reporting
		void  Rule(WCHAR t, UINT r, UINT n);
		void  ShowRules(void);
		PCUTF EnumTypes(CCharType Type);

		// Implementation
		void FindBaseType(CUnicode Text);
		void FindBaseLevel(void);
		void AnalyzeText(CUnicode Text);
		void ResolveAll(void);
		void ResolveWeakTypes(void);
		void ResolveWeakTypes(UINT uRule);
		void ResolveNeutralTypes(void);
		void ResolveNeutralTypes(UINT uRule);
		void ResolveImplicitLevels(void);
		void ReorderResolvedLevels(void);
		void ReorderResolvedLevels(UINT uRule);
		UINT FindHighestLevel(void);
		UINT FindLowestOddLevel(void);
		void Reverse(UINT uPos, UINT uLen);
		
		// Type Classification
		BOOL      IsStrongType(CCharType Type);
		BOOL      IsWeakType(CCharType Type);
		BOOL      IsNeutralType(CCharType Type);
		CCharType FindStrongDir(CCharType Type);

		// Character Type
		CCharType GetType(WCHAR c);
		
		// Type Table Access
		BOOL IsType(WCHAR c, CSpanDef const *pTable);

		// Debug
		void ShowChars(UINT Prop);
	};

// End of File

#endif
