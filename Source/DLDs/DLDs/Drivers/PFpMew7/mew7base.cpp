
#include "mew7base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 Base Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CPanFpMewtocol7BaseMasterDriver::CPanFpMewtocol7BaseMasterDriver(void)
{
	CTEXT Hex[]	= "0123456789ABCDEF";

	m_pHex		= Hex; 

	m_uPtr		= 0;

	m_pBase		= NULL;

	m_uTxSize	= elements(m_pTx);

	m_uRxSize	= elements(m_pRx);

	m_bFrame	= 0;

	m_uOffset       = 0;
	}

// Entry Points

CCODE MCALL CPanFpMewtocol7BaseMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = spaceDT;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CPanFpMewtocol7BaseMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReportErrorCode(Addr.a.m_Table) ) {

		pData[0] = m_pBase->m_dwLatestErrCode;

		return uCount;
		}

	if( IsReportErrorRequest(Addr.a.m_Table) ) {

		if( m_pBase->m_LatestErrReq[0] != '\0' ) {

			for( UINT u = 0; u < uCount; u++ ) {

				pData[u] = m_pBase->m_LatestErrReq[u];
				}
			}

		return uCount;
		}

	MakeMaxCount(Addr.a.m_Type, uCount);

	if( HasSlot(Addr.a.m_Table) ) {

		if( IsBit(Addr.a.m_Type) ) {

			return DoMappedBitRead(Addr, pData, uCount);
			}

		return DoMappedRead(Addr, pData, uCount);
		}

	if( IsBit(Addr.a.m_Type) ) {

		return DoBitRead(Addr, pData, uCount);
		}

	return DoStandardRead(Addr, pData, uCount);
	}

CCODE MCALL CPanFpMewtocol7BaseMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsReadOnly(Addr.a.m_Table) ) {

		return uCount;
		}

	if( IsReportErrorCode(Addr.a.m_Table) ) {

		InitErrorCode();

		return uCount;
		}

	if( IsReportErrorRequest(Addr.a.m_Table) ) {

		InitErrorRequest();

		return uCount;
		}

	MakeMaxCount(Addr.a.m_Type, uCount);

	if( HasSlot(Addr.a.m_Table) ) {

		if( IsBit(Addr.a.m_Type) ) {

			return DoMappedBitWrite(Addr, pData, uCount);
			}

		return DoMappedWrite(Addr, pData, uCount);
		}

	if( IsBit(Addr.a.m_Type) ) {

		return DoBitWrite(Addr, pData, uCount);
		}

	return DoStandardWrite(Addr, pData, uCount);
	}

// Read Handlers

CCODE CPanFpMewtocol7BaseMasterDriver::DoMappedRead(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwMap = FindMap(Addr, uCount);

	if( dwMap < NOTHING ) {

		Start();

		AddCommand(FALSE);

		AddRequest(Addr, dwMap, uCount);

		AddCRC();

		End();
		
		if( Transact() ) {

			GetData(pData, Addr, uCount);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CPanFpMewtocol7BaseMasterDriver::DoStandardRead(AREF Addr, PDWORD pData, UINT uCount)
{
	Start();

	AddCommand(FALSE);

	AddRequest(Addr, uCount, FALSE);

	AddCRC();

	End();

	if( Transact() ) {

		GetData(pData, Addr, uCount);

		return uCount;
		}
	
	return CCODE_ERROR;
	}

CCODE CPanFpMewtocol7BaseMasterDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	Start();

	AddCommand(FALSE);

	AddBitRequest(Addr, uCount);

	AddCRC();

	End();

	if( Transact() ) {

		GetBitData(pData, uCount);

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CPanFpMewtocol7BaseMasterDriver::DoMappedBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwMap = FindMap(Addr, uCount);

	if( dwMap < NOTHING ) {

		Start();

		AddCommand(FALSE);

		AddBitRequest(Addr, dwMap, uCount);

		AddCRC();

		End();
		
		if( Transact() ) {

			GetBitData(pData, uCount);

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CPanFpMewtocol7BaseMasterDriver::DoMappedWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwMap = FindMap(Addr, uCount);

	if( dwMap < NOTHING ) {

		Start();

		AddCommand(TRUE);

		AddRequest(Addr, dwMap, uCount);

		AddData(Addr, pData, uCount);

		AddCRC();

		End();
		
		if( Transact() ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE CPanFpMewtocol7BaseMasterDriver::DoStandardWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	Start();

	AddCommand(TRUE);

	AddRequest(Addr, uCount, TRUE);

	AddData(Addr, pData, uCount);

	AddCRC();

	End();

	if( Transact() ) {

		return uCount;
		}
	
	return CCODE_ERROR;
	}

CCODE CPanFpMewtocol7BaseMasterDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	Start();

	AddCommand(TRUE);

	AddBitRequest(Addr, uCount);

	AddData(Addr, pData, uCount);

	AddCRC();

	End();

	if( Transact() ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CPanFpMewtocol7BaseMasterDriver::DoMappedBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	DWORD dwMap = FindMap(Addr, uCount);

	if( dwMap < NOTHING ) {

		Start();

		AddCommand(TRUE);

		AddBitRequest(Addr, dwMap, uCount);

		AddData(Addr, pData, uCount);

		AddCRC();

		End();
		
		if( Transact() ) {

			return uCount;
			}
		}

	return CCODE_ERROR;
	}

// Map Support

void CPanFpMewtocol7BaseMasterDriver::MakeMap(PCBYTE &pData)
{
	if( m_pBase ) {

		m_pBase->m_uMap = GetLong(pData);

		m_pBase->m_pMap = new CFpMap[m_pBase->m_uMap];

		for( UINT u = 0; u < m_pBase->m_uMap; u++ ) {

			m_pBase->m_pMap[u].m_Addr.m_Ref = GetLong(pData);

			m_pBase->m_pMap[u].m_Map	= GetLong(pData);

			m_pBase->m_pMap[u].m_Extent	= GetWord(pData);
			}
		}
	}

void CPanFpMewtocol7BaseMasterDriver::DeleteMap(void)
{
	if( m_pBase && m_pBase->m_pMap ) {

		delete [] m_pBase->m_pMap;

		m_pBase->m_pMap = NULL;
		}
	}

DWORD CPanFpMewtocol7BaseMasterDriver::FindMap(AREF Addr, UINT &uCount)
{
	for( UINT u = 0; u < m_pBase->m_uMap; u++ ) {

		CFpMap Map  = m_pBase->m_pMap[u];

		if( Addr.a.m_Table == Map.m_Addr.a.m_Table ) {

			UINT uSpan    = IsWordLong(Map.m_Addr.a.m_Type) ? 2 : 1;

			UINT uOffset  = GetOffset(Map.m_Addr);

			UINT uRequest = GetOffset(Addr);

			UINT uMaximum = uOffset + Map.m_Extent * uSpan;

			if( uRequest >= uOffset && uRequest < uMaximum ) {

				if( Map.m_Extent > 1 ) {

					MakeMin(uCount, (uMaximum - uRequest) / uSpan); 

					return Map.m_Map + (uRequest - uOffset);
					}

				if( uCount > 1 ) {

					UINT uMax = uCount;   

					uCount    = 1;

					uSpan     = IsWordLong(Addr.a.m_Type) ? 2 : 1;

					for( UINT m = u + 1; m < m_pBase->m_uMap && uCount < uMax; m++ ) {

						UINT uStep = Map.m_Map + (uSpan * Map.m_Extent);

						if( m_pBase->m_pMap[m].m_Map != uStep ) {

							break;
							}

						Map = m_pBase->m_pMap[m];

						uCount += Map.m_Extent;
						}

					MakeMin(uCount, uMax);
					}

				return m_pBase->m_pMap[u].m_Map;
				}
			}
		}

	return NOTHING;
	}

DWORD CPanFpMewtocol7BaseMasterDriver::FindSlot(DWORD dwMap)
{
	return (dwMap >> 22) & 0x3FF;
	}

DWORD CPanFpMewtocol7BaseMasterDriver::FindOffset(DWORD dwMap)
{
	return (dwMap & 0xFFFFF);	
	}

// Frame Building

void CPanFpMewtocol7BaseMasterDriver::Start(void)
{
	m_uPtr = 0;

	AddHeader();

	AddReceiver();

	AddFrameNumber();
	}

void CPanFpMewtocol7BaseMasterDriver::AddHeader(void)
{
	AddByte('>');
	}

void CPanFpMewtocol7BaseMasterDriver::AddReceiver(void)
{
	AddByte('@');

	BOOL fHex    =  m_pBase->m_dwStation > 999;

	UINT uRadix  = fHex ? 16    : 10;

	UINT uFactor = fHex ? 0x100 : 100;

	Add(m_pBase->m_dwStation, uRadix, uFactor);
	}


void CPanFpMewtocol7BaseMasterDriver::AddFrameNumber(void)
{
	Add(DWORD(m_bFrame), 16, 0x10);
	}

void CPanFpMewtocol7BaseMasterDriver::AddCommand(BOOL fWrite)
{
	AddCommandCode(fWrite);

	AddCommandName(fWrite);
	}

void CPanFpMewtocol7BaseMasterDriver::AddCommandCode(BOOL fWrite)
{
	AddByte('#');

	AddByte('0');

	AddByte(fWrite ? '1' : '0');
	}

void CPanFpMewtocol7BaseMasterDriver::AddCommandName(BOOL fWrite)
{
	AddByte('M');

	AddByte('M');

	AddByte(fWrite ? 'W' : 'R');

	AddByte(fWrite ? 'T' : 'D');
	}

void CPanFpMewtocol7BaseMasterDriver::AddRequest(AREF Addr, UINT uCount, BOOL fWrite)
{
	AddByte('D');

	AddByte('G');

	if( IsWordLong(Addr.a.m_Type) ) {

		uCount *= 2;
		}

	if( fWrite ) {

		AddByte('0');
		}
	else {
		Add(GetByteCount(Addr.a.m_Type) / 2, 10, 1);
		}
	
	AddPrefix(Addr.a.m_Table);

	AddRange(Addr, uCount);
	}

void CPanFpMewtocol7BaseMasterDriver::AddRequest(AREF Addr, DWORD dwMap, UINT uCount)
{
	AddByte('D');

	AddByte('U');

	Add(FindSlot(dwMap), 10, 100);

	AddPrefix(Addr.a.m_Table);

	if( IsWordLong(Addr.a.m_Type) ) {

		uCount *= 2;
		}

	AddRange(Addr.a.m_Table, FindOffset(dwMap), uCount);
	}

void CPanFpMewtocol7BaseMasterDriver::AddBitRequest(AREF Addr, UINT uCount)
{
	AddByte('C');

	AddByte('0');

	AddByte('0');

	AddByte('1');

	AddByte('G');

	AddPrefix(Addr.a.m_Table);

	if( IsMixed(Addr.a.m_Table) ) {

		AddMixed(Addr.a.m_Offset, 10000000);

		return;
		}
	
	UINT uRadix  = GetRadix(Addr.a.m_Table);

	UINT uFactor = uRadix == 10 ? 10000000 : 0x10000000;
	
	AddOffset(Addr.a.m_Offset, uRadix, uFactor);
	}

void CPanFpMewtocol7BaseMasterDriver::AddBitRequest(AREF Addr, DWORD dwMap, UINT uCount)
{
	AddByte('C');		

	AddByte('0');

	AddByte('0');

	AddByte('1');

	AddByte('U');

	Add(FindSlot(dwMap), 10, 100);

	AddPrefix(Addr.a.m_Table);

	if( IsMixed(Addr.a.m_Table) ) {

		AddMixed(Addr.a.m_Offset, 10000000);

		return;
		}

	UINT uRadix  = GetRadix(Addr.a.m_Table);

	UINT uFactor = uRadix == 10 ? 10000000 : 0x10000000;
	
	AddOffset(Addr.a.m_Offset, uRadix, uFactor);
	}

void CPanFpMewtocol7BaseMasterDriver::AddPrefix(BYTE bTable)
{
	PCTXT pPrefix = GetPrefix(bTable);

	UINT p = 0;

	while( pPrefix[p] ) {

		AddByte(pPrefix[p]);

		p++;
		}
	}

void CPanFpMewtocol7BaseMasterDriver::AddRange(AREF Addr, UINT uCount)
{
	UINT uOffset = GetOffset(Addr);

	AddRange(Addr.a.m_Table, uOffset, uCount);
	}

void CPanFpMewtocol7BaseMasterDriver::AddRange(BYTE bTable, DWORD dwOffset, UINT uCount)
{
	UINT uRadix  = GetRadix(bTable);

	UINT uFactor = uRadix == 10 ? 100000 : 0x100000;

	AddOffset(dwOffset, uRadix, uFactor);

	Add(uCount, 10, 1000000); 
	}

void CPanFpMewtocol7BaseMasterDriver::AddCRC(void)
{
	CRCCalc(m_pTx, m_uPtr);
	
	Add(m_CRC, 16, 0x1000);
	}

void CPanFpMewtocol7BaseMasterDriver::End(void)
{
	AddByte(CR);
	}

void CPanFpMewtocol7BaseMasterDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {

		m_pTx[m_uPtr] = bData;

		m_uPtr++;
		} 
	}

void CPanFpMewtocol7BaseMasterDriver::Add(DWORD dwData, UINT uRadix, UINT uFactor)
{
	while( uFactor ) {
	
		AddByte( m_pHex[(dwData / uFactor) % uRadix] );

		uFactor /= uRadix;
		}
	}

void CPanFpMewtocol7BaseMasterDriver::AddMixed(DWORD dwData, UINT uFactor)
{
	DWORD dDec = ((dwData & 0xFFFFFFF0) >> 4) * 10;

	DWORD dHex = dwData & 0xF;

	while( uFactor > 1 ) {

		AddByte( m_pHex[(dDec / uFactor) % 10] );
				
		uFactor /= 10;
		}

	AddByte(m_pHex[(dHex / uFactor) % 16]);

	m_uOffset = m_uPtr;
	}

void CPanFpMewtocol7BaseMasterDriver::AddOffset(DWORD dwData, UINT uRadix, UINT uFactor)
{
	Add(dwData, uRadix, uFactor);

	m_uOffset = m_uPtr;
	}

void CPanFpMewtocol7BaseMasterDriver::AddBitData(AREF Addr, PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++) {

		AddByte(pData[0] == 0 ? '0' : '1');
		}
	}

void CPanFpMewtocol7BaseMasterDriver::AddWordData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		Add(LOWORD(pData[u]), 16, 0x1000);
		}
	}

void CPanFpMewtocol7BaseMasterDriver::AddLongData(UINT uType, PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		Add(LOWORD(pData[u]), 16, 0x1000);

		Add(HIWORD(pData[u]), 16, 0x1000);
		}
	}

void CPanFpMewtocol7BaseMasterDriver::AddData(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsBit(Addr.a.m_Type) ) {

		return AddBitData(Addr, pData, uCount);
		}

	if( IsLong(Addr.a.m_Type) ) {

		return AddLongData(Addr.a.m_Type, pData, uCount);
		}

	AddWordData(pData, uCount);
	}

// Implementation

PCTXT CPanFpMewtocol7BaseMasterDriver::GetPrefix(BYTE bTable)
{
	switch( bTable ) {

		case spaceDT :	return "DT";
		case spaceLD :	return "LD";
		case spaceUM :	return "UM";
		case spaceSD :	return "SD";
		case spaceX  :	return "X";
		case spaceY  :	return "Y";
		case spaceR  :	return "R";
		case spaceL  :	return "L";
		case spaceP  :	return "P";
		case spaceIN :	return "IN";
		case spaceOT :	return "OT";
		case spaceT  :	return "T";
		case spaceC  :	return "C";
		case spaceS :	return "S";
		case spaceE  :	return "E";
		case spaceI  :	return "IX";
		case spaceTS :	return "TS";
		case spaceTE :	return "TE";
		case spaceCS :	return "CS";
		case spaceCE :	return "CE";
		case spaceWR :	return "WR";
		case spaceWL :	return "WL";
		case spaceWI :	return "WI";
		case spaceWO :  return "WO";
		case spaceWX :  return "WX";
		case spaceWY :	return "WY";
		case spaceWS :  return "WS";
		}

	return "";
	}

UINT CPanFpMewtocol7BaseMasterDriver::GetRadix(BYTE bTable)
{
	if( IsMixed(bTable) ) {

		return 16;
		}

	return 10;
	}

UINT  CPanFpMewtocol7BaseMasterDriver::GetOffset(AREF Addr)
{
	return Addr.a.m_Offset | (Addr.a.m_Extra << 16);
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsMixed(BYTE bTable)
{
	switch( bTable ) {

		case spaceX :	
		case spaceY :	
		case spaceR :	
		case spaceL :	
		case spaceP :	
		case spaceIN:	
		case spaceOT:
		case spaceS :

			return TRUE;	
		}

	return FALSE;
	}

void CPanFpMewtocol7BaseMasterDriver::GetData(PDWORD pData, AREF Addr, UINT uCount)
{
	UINT uBytes = GetByteCount(Addr.a.m_Type);

	BOOL fSlot  = HasSlot(Addr.a.m_Table);

	BOOL fSwap  = SwapLong(Addr);

	for( UINT u = 0; u < uCount; u++ ) {

		PCTXT pText = PCTXT(m_pRx + 14 + uBytes * u);

		pData[u]    = Convert(pText, uBytes);

		if( fSwap ) {

			DWORD d  = pData[u];

			pData[u] = MAKELONG(HIWORD(d), LOWORD(d));
			}
		}
	 }

void CPanFpMewtocol7BaseMasterDriver::GetBitData(PDWORD pData, UINT uCount)
{
	for( UINT u = 0; u < uCount; u++ ) {

		PCTXT pText = PCTXT(m_pRx + 14 + u * 2);

		pData[u]    = Convert(pText, 2);
		}
	 }

UINT CPanFpMewtocol7BaseMasterDriver::GetByteCount(UINT uType)
{
	if( IsLong(uType) ) {

		return 8;
		}

	if( IsBit(uType) ) {

		return 1;
		}

	return 4;
	}

void  CPanFpMewtocol7BaseMasterDriver::MakeMaxCount(UINT uType, UINT &uCount)
{
	UINT uMax = MAX_WORD;
	
	if( IsLong(uType) ) {

		uMax /= 2;
		}

	else if( IsBit(uType) ) {

		uMax = 1;
		}
	
	MakeMin(uCount, uMax);
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsWordLong(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:
		
			return TRUE;
		}

	return FALSE;
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsLong(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrWordAsReal:
		case addrLongAsLong:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsBit(UINT uType)
{
	return ( uType == addrBitAsBit );
	}

BOOL CPanFpMewtocol7BaseMasterDriver::HasSlot(BYTE bTable)
{
	switch( bTable ) {

		case spaceUM:
		case spaceIN:
		case spaceOT:
		case spaceWI:
		case spaceWO:

			return TRUE;

		}

	return FALSE;
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsReadOnly(BYTE bTable)
{
	switch( bTable ) {

		case spaceS:
		case spaceT:
		case spaceC:
		case spaceP:
		case spaceE:
		case spaceIN:
		case spaceWI:
		case spaceSD:

			return TRUE;
		}

	return FALSE;
	}

BOOL CPanFpMewtocol7BaseMasterDriver::SwapLong(AREF Addr)
{
	if( IsWordLong(Addr.a.m_Type) ) {

		return Addr.a.m_Table == spaceUM;
		}

	return IsLong(Addr.a.m_Type);
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsReportErrorCode(BYTE bTable)
{
	return bTable == spaceLEC;
	}

BOOL CPanFpMewtocol7BaseMasterDriver::IsReportErrorRequest(BYTE bTable)
{
	return bTable == spaceLER;
	}

void  CPanFpMewtocol7BaseMasterDriver::InitErrorCode(void)
{
	if( m_pBase ) {

		m_pBase->m_dwLatestErrCode = 0;
		}
	}

void  CPanFpMewtocol7BaseMasterDriver::InitErrorRequest(void)
{
	if( m_pBase ) {

		m_pBase->m_LatestErrReq[0] = '\0';
		}
	}

void CPanFpMewtocol7BaseMasterDriver::SetReportErrors(void)
{
	UINT c = 0;

	for( UINT u = 10; u < m_uOffset && c < 60; u++, c++ ) {

		m_pBase->m_LatestErrReq[c] = m_pTx[u];
		}

	m_pBase->m_LatestErrReq[c] = '\0';

	m_pBase->m_dwLatestErrCode = Convert(PCTXT(m_pRx + 14), 2);
	}

// Transport

BOOL CPanFpMewtocol7BaseMasterDriver::CheckFrame(UINT uBytes)
{
	UINT uSuccess = 7;

	if( uBytes > uSuccess ) {

		CRCCalc(m_pRx, uBytes);

		WORD wCheck = WORD(Convert(PCTXT(m_pRx + uBytes), 4));

		if( m_CRC == wCheck ) {

			if( m_pRx[7] != '$' ) {

				SetReportErrors();
				
				return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Overridables

BOOL CPanFpMewtocol7BaseMasterDriver::Transact(void)
{
	return FALSE;
	}

// CRC-16-CCITT

void CPanFpMewtocol7BaseMasterDriver::CRCInit(void)
{
	m_CRC = 0xFFFF;
	}

void CPanFpMewtocol7BaseMasterDriver::CRCCalc(PBYTE pBytes, UINT uCount)
{
	CRCInit();

	for( UINT u = 0; u < uCount; u++ ) {

		BYTE bByte = pBytes[u];

		Reverse(bByte);
			
		WORD c = bByte << 8;

		for( UINT b = 0; b < 8; b++ ) {

			if( (c & 0x8000) ^ (m_CRC & 0x8000) ) {

				m_CRC <<= 1;

				m_CRC ^= CRC_POLY;
				}
			else {
				m_CRC <<= 1;
				}

			c <<= 1;
			}
		}

	CRCReverse();
	}

void CPanFpMewtocol7BaseMasterDriver::CRCReverse(void)
{
	UINT uBits = 15;

	WORD wCRC = m_CRC;

	m_CRC = wCRC & 1;

	for( wCRC >>= 1; wCRC; wCRC >>= 1 ) {

		m_CRC <<= 1;

		m_CRC |= wCRC & 1;

		uBits--;
		}

	m_CRC <<= uBits;
	}

void CPanFpMewtocol7BaseMasterDriver::Reverse(BYTE &bByte)
{
	UINT uBits = 7;

	BYTE bWork = bByte;

	bByte = bWork & 1;

	for( bWork >>= 1; bWork; bWork >>= 1 ) {

		bByte <<= 1;

		bByte |= bWork & 1;

		uBits--;
		}

	bByte <<= uBits;
	}

// Helpers

DWORD CPanFpMewtocol7BaseMasterDriver::Convert(PCTXT pText, UINT uCount)
{
	DWORD dwData = 0;
	
	while( uCount-- ) {
	
		char cData = *(pText++);
		
		if( cData >= '0' && cData <= '9' ) {

			dwData = 16 * dwData + cData - '0';
			}

		else if( cData >= 'A' && cData <= 'F' ) {

			dwData = 16 * dwData + cData - 'A' + 10;
			}

		else if( cData >= 'a' && cData <= 'f' ) {

			dwData = 16 * dwData + cData - 'a' + 10;
			}

		else {
			break;
			}
		}

	return dwData;
	}

void CPanFpMewtocol7BaseMasterDriver::PrintMap(void)
{
	for( UINT u = 0; u < m_pBase->m_uMap; u++ ) {

		CFpMap Map = m_pBase->m_pMap[u];

		AfxTrace("\nMap %u Ref %8.8x Map %8.8x Extent %u", u, Map.m_Addr.m_Ref, Map.m_Map, Map.m_Extent);
		}
	}


// End of File


