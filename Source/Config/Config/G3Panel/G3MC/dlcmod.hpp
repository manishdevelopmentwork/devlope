
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DLCMOD_HPP

#define INCLUDE_DLCMOD_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//
				           
#include "dlcmap.hpp"

#include "pidloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CDLCModule;
class CDLCMainWnd;

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Module
//

class CDLCModule : public CGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDLCModule(void);

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		CDLCMapper *m_pMapper;
		CPIDLoop   *m_pLoop1;	
		CPIDLoop   *m_pLoop2;	

	protected:
		// Implementation
		void AddMetaData(void);

		// Download Support
		void MakeConfigData(CInitData &Init);
		void MakeConfigData(CInitData &Init, CPIDLoop *m_pLoop);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dual Loop Module Main Window
//

class CDLCMainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CDLCMainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd *	m_pMult;
		CDLCModule    *	m_pItem;
		CPIDLoop      * m_pLoop1;
		CPIDLoop      * m_pLoop2;
		
		// Overribables
		void OnAttach(void);

		// Implementation
		void AddPIDPages(CPIDLoop *pLoop, CString Loop);
		void AddMapPages(void);
	};

// End of File

#endif
