
#include "intern.hpp"

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Primitive Edge Pen
//

// Constructor

CPrimRubyPenEdge::CPrimRubyPenEdge(void)
{
	m_TrimWidth  = 0;

	m_TrimMode   = 0;

	m_pTrimColor = New CPrimColor(naFeature);
	}

// Destructor

CPrimRubyPenEdge::~CPrimRubyPenEdge(void)
{
	delete m_pTrimColor;
	}

// Initialization

void CPrimRubyPenEdge::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimRubPenEdge", pData);

	CPrimRubyPenBase::Load(pData);

	m_TrimWidth = GetByte(pData);
	
	m_TrimMode  = GetByte(pData);

	m_pTrimColor->Load(pData);
	}

// Attributes

BOOL CPrimRubyPenEdge::IsNull(void) const
{
	return CPrimRubyPenBase::IsNull() && m_TrimWidth == 0;
	}

int CPrimRubyPenEdge::GetInnerWidth(void) const
{
	int n = CPrimRubyPenBase::GetInnerWidth();
	
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		n += (m_TrimWidth + 1) / 2;
		}

	return n;
	}

int CPrimRubyPenEdge::GetOuterWidth(void) const
{
	int n = CPrimRubyPenBase::GetOuterWidth();
	
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		n += (m_TrimWidth + 1) / 2;
		}

	return n;
	}

// Operations

void CPrimRubyPenEdge::SetScan(UINT Code)
{
	SetItemScan(m_pTrimColor, Code);

	CPrimRubyPenBase::SetScan(Code);
	}

BOOL CPrimRubyPenEdge::DrawPrep(IGDI *pGDI)
{
	CCtx Ctx;

	FindCtx(Ctx);

	if( !(m_Ctx == Ctx) ) {

		m_Ctx = Ctx;

		CPrimRubyPenBase::DrawPrep(pGDI);

		return TRUE;
		}

	return CPrimRubyPenBase::DrawPrep(pGDI);
	}

void CPrimRubyPenEdge::AdjustRect(R2 &r, UINT m)
{
	if( m_Width == 1 ) {

		if( m & 1 ) r.x1++;
		if( m & 2 ) r.y1++;
		if( m & 4 ) r.x2--;
		if( m & 8 ) r.y2--;
		}
	}

void CPrimRubyPenEdge::AdjustRect(R2 &r)
{
	if( m_Width == 1 ) {

		DeflateRect(r, 1, 1);
		}
	}

// Stroking

BOOL CPrimRubyPenEdge::StrokeTrim(CRubyPath &output, CRubyPath const &figure)
{
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		CRubyStroker s;

		s.SetEdgeMode (CRubyStroker::edgeCenter);

		s.SetJoinStyle(CRubyStroker::joinMiter);

		s.StrokeLoop(output, figure, m_TrimMode, m_TrimWidth);

		return TRUE;
		}

	return FALSE;
	}

// Drawing

BOOL CPrimRubyPenEdge::Trim(IGdi *pGdi, CRubyGdiList const &list, BOOL fOver)
{
	if( m_Width >= 4 && m_TrimWidth > 0 ) {

		CRubyGdiLink link(pGdi);

		if( fOver )
			link.OutputSolid(list, m_Ctx.m_TrimColor, 0);
		else
			link.OutputSolid(list, m_Ctx.m_TrimColor);

		return TRUE;
		}

	return FALSE;
	}

// Context Creation

void CPrimRubyPenEdge::FindCtx(CCtx &Ctx)
{
	Ctx.m_TrimColor = m_pTrimColor->GetColor();
	}

// Context Check

BOOL CPrimRubyPenEdge::CCtx::operator == (CCtx const &That) const
{
	return m_TrimColor == That.m_TrimColor;
	}

// End of File
