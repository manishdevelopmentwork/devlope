
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Zoom Helper
//

// Runtime Class

AfxImplementRuntimeClass(CZoom, CObject);

// Constructor

CZoom::CZoom(CWnd &Wnd)
{
	m_hWnd = Wnd.GetHandle();
	}

// Operations

void CZoom::DrawZoom(CRect const &From, CRect const &To)
{
	DrawZoom(From, To, 200);
	}

void CZoom::DrawZoom(CRect const &From, CRect const &To, DWORD tZoom)
{
	CWnd &Wnd = CWnd::FromHandle(m_hWnd);

	CRect r1 = From;

	CRect r2 = To;

	Wnd.ClientToScreen(r1);

	Wnd.ClientToScreen(r2);

	CWindowDC DC(NULL);

	PlaySound(From, To);

	DWORD tInit = timeGetTime();

	DWORD tLast = 0;

	CRect Rect  = r1;

	for(;;) {

		DrawAnimRect(DC, Rect);

		CRect Last = Rect;

		DWORD tTime;

		do {
			tTime = timeGetTime() - tInit;

			if( tTime >= tZoom ) {

				DrawAnimRect(DC, Rect);

				return;
				}

			} while( tTime < tLast + 10 );

		Rect.left   = r1.left   + MulDiv(r2.left   - r1.left,   tTime, tZoom);

		Rect.right  = r1.right  + MulDiv(r2.right  - r1.right,  tTime, tZoom);
		
		Rect.top    = r1.top    + MulDiv(r2.top    - r1.top,    tTime, tZoom);
		
		Rect.bottom = r1.bottom + MulDiv(r2.bottom - r1.bottom, tTime, tZoom);

		DrawAnimRect(DC, Last);

		tLast = tTime;
		}
	}

// Implementation

void CZoom::DrawAnimRect(CDC &DC, CRect const &Rect)
{
	CBrush Brush(CColor(0xFF, 0xFF, 0x00), CColor(0x80, 0x80, 0x00), 128);
	
	DC.Select(Brush);

	int n = 3;
	
	DC.PatBlt(CRect(Rect.left+n, Rect.top, Rect.right, Rect.top+n), PATINVERT);

	DC.PatBlt(CRect(Rect.right-n, Rect.top+n, Rect.right, Rect.bottom), PATINVERT);

	DC.PatBlt(CRect(Rect.left+n, Rect.bottom-n, Rect.right-n, Rect.bottom), PATINVERT);
	
	DC.PatBlt(CRect(Rect.left, Rect.top, Rect.left+n, Rect.bottom), PATINVERT);

	DC.Deselect();
	}

void CZoom::PlaySound(CRect const &From, CRect const &To)
{
	if( To.cx() > From.cx() ) {

		sndPlaySound(L"Maximize", SND_ASYNC | SND_NODEFAULT);

		return;
		}

	if( To.cx() < From.cx() ) {

		sndPlaySound(L"Minimize", SND_ASYNC | SND_NODEFAULT);

		return;
		}
	}

// End of File
