
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Transfer Manager
//

// Dynamic Class

AfxImplementDynamicClass(CTransferManager, CEt3UIItem);

// Constructor

CTransferManager::CTransferManager(void)
{
	m_Handle	= HANDLE_NONE;

	m_pTransfers	= New CTransferList;	

	m_ScanTime	= 1000;

	m_NetTimeout	= 1000;

	m_SerialTimeout	= 3000;
	}

// UI Creation

CViewWnd * CTransferManager::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CTransferNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewResource ) {

		CLASS Class = AfxNamedClass(L"CTransferResTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return CEt3UIItem::CreateView(uType);
	}

// Operations

void CTransferManager::Validate(void)
{
	// TODO -- check transfer remote device is present

	INDEX Index = m_pTransfers->GetHead();

	while( !m_pTransfers->Failed(Index) ) {

		CTransferItem * pTransfer = m_pTransfers->GetItem(Index);

		pTransfer->CheckDevice();

		AfxTouch(pTransfer);

		m_pTransfers->GetNext(Index);
		}
	}

// Item Naming

CString CTransferManager::GetHumanName(void) const
{
	return CString(IDS_TRANSFER_MANAGER);
	}

// Download Support

void CTransferManager::PrepareData(void)
{
	CFileDataBaseCfgXfers &File = m_pSystem->m_CfgXfers;

	File.PutScanTime     (m_ScanTime);
	
	File.PutSerialTimeout(m_SerialTimeout);
	
	File.PutNetTimeout  (m_NetTimeout);

	if( TRUE ) {

		UINT uPtr = 0x0018;

		File.PutZero(uPtr, 8);
		}

	CEt3UIItem::PrepareData();
	}

// Meta Data Creation

void CTransferManager::AddMetaData(void)
{
	CEt3UIItem::AddMetaData();

	/*Meta_AddInteger(Handle);*/
	Meta_AddCollect(Transfers);
	Meta_AddInteger(ScanTime);
	Meta_AddInteger(NetTimeout);
	Meta_AddInteger(SerialTimeout);

	Meta_SetName((IDS_TRANSFER_MANAGER));
	}

/////////////////////////////////////////////////////////////////////////
//
// Transfer List
//

// Dynamic Class

AfxImplementDynamicClass(CTransferList, CNamedList);

// Constructor

CTransferList::CTransferList(void)
{
	}

// Item Access

CTransferItem * CTransferList::GetItem(INDEX Index) const
{
	return (CTransferItem *) CNamedList::GetItem(Index);
	}

CTransferItem * CTransferList::GetItem(UINT uPos) const
{
	return (CTransferItem *) CNamedList::GetItem(uPos);
	}

// Persistence

void CTransferList::Init(void)
{
	CNamedList::Init();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

void CTransferList::PostLoad(void)
{
	CNamedList::PostLoad();

	m_pSystem = (CEt3CommsSystem *) GetParent(AfxRuntimeClass(CEt3CommsSystem));
	}

// Download Support

void CTransferList::PrepareData(void)
{
	CFileDataBaseCfgXfers &File = m_pSystem->m_CfgXfers;

	File.ClearTransfers();

	CNamedList::PrepareData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item Page
//

class CTransferItemPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTransferItemPage(CTransferItem *pTransfer);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CTransferItem * m_pTransfer;

		// Implementation
		void LoadStatus(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// IO Item - Analog Page
//

// Runtime Class

AfxImplementRuntimeClass(CTransferItemPage, CUIStdPage);

// Constructor

CTransferItemPage::CTransferItemPage(CTransferItem *pTransfer)
{
	m_Class   = AfxPointerClass(pTransfer);	

	m_pTransfer = pTransfer;
	}

// Operations

BOOL CTransferItemPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIStdPage::LoadIntoView(pView, pItem);	

	LoadStatus(pView);

	pView->NoRecycle();

	return FALSE;
	}

// Implementation

void CTransferItemPage::LoadStatus(IUICreate *pView)
{
	CString Form = CString(IDS_NONE);

	for( UINT n = 0; n < 16; n ++ ) {

		Form += CPrintf(L"|%d|DI %d", 49 + n - 1, 49 + n);
		}

	CString Tip = CString(IDS_SELECT_BIT_TO);

	pView->StartGroup(CString(IDS_STATUS_BIT), 1);

	CUIData Data;

	Data.m_Tag	 = L"Status";

	Data.m_Label	 = CString(IDS_STATUS_BIT);

	Data.m_ClassText = AfxNamedClass(L"CUITextEnumIndexed");

	Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Data.m_Format	 = Form;

	Data.m_Tip	 = Tip;

	pView->AddUI(m_pTransfer, L"root", &Data);

	pView->EndGroup(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item
//

// Dynamic Class

AfxImplementRuntimeClass(CTransferItem, CEt3UIItem);

// Constructor

CTransferItem::CTransferItem(void)
{
	m_Direction	= 0;

	m_Remote	= NOTHING;

	m_SrceType	= 0;

	m_SrceAddr	= 1;

	m_DestType	= 0;

	m_DestAddr	= 1;

	m_Count		= 8;

	m_Status	= NOTHING;

	m_pDevice	= NULL;
	}

// Operations

void CTransferItem::CheckDevice(void)
{
	if( m_Remote < NOTHING ) {
		
		CEt3CommsManager *pComms = m_pSystem->m_pComms;

		if( !pComms->FindDevice(m_Remote) ) {
			
			m_Remote = NOTHING;
			}
		}
	}

// UI Creation

BOOL CTransferItem::OnLoadPages(CUIPageList *pList)
{			 
	pList->Append(New CTransferItemPage(this));

	return FALSE;
	}

// UI Update

void CTransferItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == "Remote" ) {

		m_pDevice = FindDevice();

		if( m_pDevice ) {

			AfxTrace(L" device station %d\n", m_pDevice->m_Station);
			
			AfxTrace(L" ip address %8.8X\n", m_pDevice->m_IPAddr);
			}
		else {
			AfxTrace(L"no device\n");
			}
		
		DoEnables(pHost);
		}

	if( Tag == "Direction" ) {

		pHost->SendUpdate(updateProps);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Item Naming

CString CTransferItem::GetHumanName(void) const
{
	return m_Name;
	}

// Persistence

void CTransferItem::PostLoad(void)
{
	CEt3UIItem::PostLoad();

	m_pDevice = FindDevice();
	}

// Download Support

void CTransferItem::PrepareData(void)
{
	AfxTrace(L"CTransferItem::PrepareData\n");

	CTransferList *pList = (CTransferList *) GetParent();

	UINT            uPos = pList->FindItemPos(this);

	CFileDataBaseCfgXfers                &File = m_pSystem->m_CfgXfers;

	CFileDataBaseCfgXfers::CTransfer &Transfer = File.m_Transfers[uPos];

	// Copy Block Type
	Transfer.PutBlockType(BuildType());

	// Station Info Block
	Transfer.PutStation  (BuildStation());
	Transfer.PutInterface(BuildInterface());
	Transfer.PutIPAddr   (BuildIPAddr());
	Transfer.PutDestPort (BuildDestPort());
	Transfer.PutStatusBit(m_Status);

	// Copy Info Block
	Transfer.PutRegCount (m_Count);
	Transfer.PutSrceType (m_SrceType);
	Transfer.PutSrceAddr (m_SrceAddr - 1);
	Transfer.PutDestType (m_DestType);
	Transfer.PutDestAddr (m_DestAddr - 1);	
	}

// Meta Data Creation

void CTransferItem::AddMetaData(void)
{
	CEt3UIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Direction);
	Meta_AddInteger(Remote);
	Meta_AddInteger(SrceType);
	Meta_AddInteger(SrceAddr);
	Meta_AddInteger(DestType);
	Meta_AddInteger(DestAddr);
	Meta_AddInteger(Count);
	Meta_AddInteger(Status);

	Meta_SetName((IDS_TRANSFER));
	}

// Implementation

void CTransferItem::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_Remote < NOTHING;

	pHost->EnableUI(L"Direction", fEnable);
	pHost->EnableUI(L"Count",     fEnable);
	pHost->EnableUI(L"SrceType",  fEnable);
	pHost->EnableUI(L"SrceAddr",  fEnable);
	pHost->EnableUI(L"DestType",  fEnable);
	pHost->EnableUI(L"DestAddr",  fEnable);
	pHost->EnableUI(L"Status",    fEnable);
	}

// Config File Help

UINT CTransferItem::BuildType(void)
{
	if( m_pDevice ) {

		if( m_Direction == 0 ) {

			// read

			if( IsDeviceModbus() ) {
			
				return 0x03;
				}

			if( IsDeviceSixnet() ) {
			
				return 0x02;
				}
			}
		else {
			// write

			if( IsDeviceModbus() ) {
			
				return 0x05;
				}

			if( IsDeviceSixnet() ) {
			
				return 0x04;
				}			
			}
		}

	return 0x00;
	}

UINT CTransferItem::BuildStation(void)
{
	if( m_pDevice ) {

		return m_pDevice->m_Station;
		}

	return 0;
	}

UINT CTransferItem::BuildInterface(void)
{
	if( m_pDevice ) {

		CEt3CommsPort *pPort = m_pDevice->GetParentPort();

		if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {
		
			return 0x01;
			}

		if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {
		
			return 0x10;
			}
		}

	return 0xFF;
	}

PCBYTE CTransferItem::BuildIPAddr(void)
{
	// TODO -- fix this

	if( m_pDevice ) {

		CEt3CommsPort *pPort = m_pDevice->GetParentPort();

		if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {

			UINT uAddr = m_pDevice->m_IPAddr;

			static	BYTE Data[16] = { 0 };

			Data[3] = LOBYTE(LOWORD(uAddr));
			Data[2] = HIBYTE(LOWORD(uAddr));
			Data[1] = LOBYTE(HIWORD(uAddr));
			Data[0] = HIBYTE(HIWORD(uAddr));

			return Data;		
			}
		}

	static	BYTE Null[16] = { 0 };

	return Null;
	}

UINT CTransferItem::BuildDestPort(void)
{
	if( m_pDevice ) {

		return m_pDevice->m_Number;
		}

	return 0;
	}

CEt3CommsDevice * CTransferItem::FindDevice(void)
{
	CEt3CommsManager *pComms = m_pSystem->m_pComms;

	CEt3CommsPortList *pPorts;

	for( UINT s = 0; pComms->GetPortList(pPorts, s); s ++ ) {

		for( INDEX i = pPorts->GetHead(); !pPorts->Failed(i); pPorts->GetNext(i) ) {

			CEt3CommsPort          *pPort = pPorts->GetItem(i);

			CEt3CommsDeviceList *pDevices = pPort->m_pDevices;

			for( INDEX d = pDevices->GetHead(); !pDevices->Failed(d); pDevices->GetNext(d) ) {

				CEt3CommsDevice  *pDevice = pDevices->GetItem(d);

				if( m_Remote == pDevice->m_Number ) {
					
					return pDevice;
					}				
				}
			}
		}

	return NULL;
	}

BOOL CTransferItem::IsDeviceModbus(void)
{
	CEt3CommsPort *pPort = m_pDevice->GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		UINT uProtocol = ((CEt3CommsPortSerial *) pPort)->m_Protocol;		

		switch( uProtocol ) {

			case 0:
				// udr binary master
			case 1:
				// udr ascii master
			case 4:
				// udr binary and ascii slave

				return FALSE;

			case 2:
				// modbus rtu master
			case 3:
				// modbus ascii slave
			case 5:
				// modbus rtu slave
			case 6:
				// modbus ascii slave

				return TRUE;

			default:
				break;
			}

		return FALSE;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkModbus)) ) {
			
		return TRUE;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkSixnet)) ) {
			
		return FALSE;
		}

	return FALSE;
	}

BOOL CTransferItem::IsDeviceSixnet(void)
{
	CEt3CommsPort *pPort = m_pDevice->GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {

		UINT uProtocol = ((CEt3CommsPortSerial *) pPort)->m_Protocol;

		switch( uProtocol ) {

			case 0:
				// udr binary master
			case 1:
				// udr ascii master
			case 4:
				// udr binary and ascii slave

				return TRUE;

			case 2:
				// modbus rtu master
			case 3:
				// modbus ascii slave
			case 5:
				// modbus rtu slave
			case 6:
				// modbus ascii slave

				return FALSE;

			default:
				break;
			}

		return FALSE;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkModbus)) ) {
			
		return FALSE;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetworkSixnet)) ) {
			
		return TRUE;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Discrete
//

// Dynamic Class

AfxImplementDynamicClass(CTransferDiscreteItem, CTransferItem);

// Constructor

CTransferDiscreteItem::CTransferDiscreteItem(void)
{
	m_SrceType = 10;

	m_SrceAddr = 1;

	m_DestType = 10;

	m_DestAddr = 1;
	}

// Attributes

UINT CTransferDiscreteItem::GetTreeImage(void) const
{
	return m_Direction ? IDI_DISCRETE_READ : IDI_DISCRETE_WRITE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Analog
//

// Dynamic Class

AfxImplementDynamicClass(CTransferAnalogItem, CTransferItem);

// Constructor

CTransferAnalogItem::CTransferAnalogItem(void)
{
	m_SrceType = 0;

	m_SrceAddr = 1;

	m_DestType = 0;

	m_DestAddr = 1;
	}

// Attributes

UINT CTransferAnalogItem::GetTreeImage(void) const
{
	return m_Direction ? IDI_ANALOG_READ : IDI_ANALOG_WRITE;
	}

// End of File
