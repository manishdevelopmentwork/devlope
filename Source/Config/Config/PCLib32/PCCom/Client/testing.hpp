
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Test Application
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TESTING_HPP
	
#define	INCLUDE_TESTING_HPP

//////////////////////////////////////////////////////////////////////////
//
// Test Class
//

class CTestClass : public CConnectableObject
{
	public:
		// Component Class
		AfxDeclareComponentClass();

		// Constructor
		CTestClass(void);

		// Destructor
		~CTestClass(void);
	};

// End of File

#endif
