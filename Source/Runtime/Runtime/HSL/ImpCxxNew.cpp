
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Tracking Hooks
//

#if defined(_TRACKING)

clink void * _malloc_t(struct _reent *ptr, size_t size, char const *file, int line);

#endif

//////////////////////////////////////////////////////////////////////////
//
// C++ New Implementation
//

// Code

void * operator new (size_t size)
{
	return _malloc_r(_REENT, size);
	}

void * operator new (size_t size, char const *file, int line)
{
	#if defined(_TRACKING)

	return _malloc_t(_REENT, size, file, line);

	#else

	return _malloc_r(_REENT, size);

	#endif
	}

void * operator new [] (size_t size)
{
	return _malloc_r(_REENT, size);
	}

void * operator new [] (size_t size, char const *file, int line)
{
	#if defined(_TRACKING)

	return _malloc_t(_REENT, size, file, line);

	#else

	return _malloc_r(_REENT, size);

	#endif
	}

void operator delete (void *data)
{
	_free_r(_REENT, data);
	}

void operator delete (void *data, size_t size)
{
	_free_r(_REENT, data);
	}

void operator delete [] (void *data)
{
	_free_r(_REENT, data);
	}

void operator delete [] (void *data, size_t size)
{
	_free_r(_REENT, data);
	}

// End of File
