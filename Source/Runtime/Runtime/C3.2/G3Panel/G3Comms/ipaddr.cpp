
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// IP Address Display Format
//

// Constructor

CDispFormatIPAddr::CDispFormatIPAddr(void)
{
	}

// Destructor

CDispFormatIPAddr::~CDispFormatIPAddr(void)
{
	}

// Initialization

void CDispFormatIPAddr::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatIPAddr", pData);
	}

// Formatting

CUnicode CDispFormatIPAddr::Format(DWORD Data, UINT Type, UINT Flags)
{
	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeVoid ) {

		return L"-.-.-.-";
		}

	if( Type == typeInteger ) {

		CUnicode Text;
		
		char     s[64];

		SPrintf( s,
			 "%u.%u.%u.%u",
			 HIBYTE(HIWORD(Data)),
			 LOBYTE(HIWORD(Data)),
			 HIBYTE(LOWORD(Data)),
			 LOBYTE(LOWORD(Data))
			 );

		Text = s;

		if( Flags & fmtPad ) {

			if( !(Flags & fmtANSI) ) {

				MakeDigitsFixed(Text);
				}
			}

		return Text;
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormatIPAddr::Parse(DWORD &Data, CString Text, UINT Type)
{
	if( Type == typeInteger ) {

		UINT b1 = atoi(Text.StripToken('.'));
		UINT b2 = atoi(Text.StripToken('.'));
		UINT b3 = atoi(Text.StripToken('.'));
		UINT b4 = atoi(Text.StripToken('.'));

		if( (b1 | b2 | b3 | b4) & 0xFFFFFF00 ) {

			return FALSE;
			}

		Data = MAKELONG(MAKEWORD(b4,b3),MAKEWORD(b2,b1));

		return TRUE;
		}

	return GeneralParse(Data, Text, Type);
	}

// Editing

UINT CDispFormatIPAddr::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		pEdit->m_uKeypad  = keypadUnsigned;

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		pEdit->m_fError   = TRUE;

		return editUpdate;
		}

	if( uCode == 0x0D ) {

		if( !pEdit->m_fDefault ) {

			if( pEdit->m_Edit.Count('.') == 3 ) {

				DWORD Data;

				if( Parse(Data, UniConvert(pEdit->m_Edit), pEdit->m_Type) ) {

					if( Validate(pEdit, DWORD(Data)) ) {

						if( pEdit->m_pValue->SetValue(Data, pEdit->m_Type, setNone) ) {

							return editCommit;
							}

						return editUpdate;
						}
					}

				pEdit->m_uCursor = cursorError;

				pEdit->m_fError  = TRUE;

				return editError;
				}

			return editNone;
			}

		return editAbort;
		}

	if( uCode == 0x1B ) {

		if( pEdit->m_fDefault ) {

			return editAbort;
			}

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		pEdit->m_fError   = FALSE;

		return editUpdate;
		}

	if( uCode == 0x7F ) {

		if( pEdit->m_fDefault ) {

			pEdit->m_Edit.Empty();

			pEdit->m_uCursor  = cursorLast;

			pEdit->m_fDefault = FALSE;

			return editUpdate;
			}

		return editNone;
		}

	if( uCode >= 0x20 ) {

		if( pEdit->m_fDefault || pEdit->m_fError ) {

			if( uCode == '.' ) {

				return editNone;
				}

			pEdit->m_Edit.Empty();

			pEdit->m_uCursor  = cursorLast;

			pEdit->m_fDefault = FALSE;

			pEdit->m_fError   = FALSE;
			}

		UINT uLen = pEdit->m_Edit.GetLength();

		if( uCode == '.' ) {

			if( pEdit->m_Edit[uLen-1] != '.' ) {

				if( pEdit->m_Edit.Count('.') < 3 ) {

					pEdit->m_Edit += WCHAR(uCode);

					return editUpdate;
					}
				}

			return editNone;
			}

		if( uCode >= '0' && uCode <= '9' ) {

			if( uLen < 15 ) {

				UINT uDot = pEdit->m_Edit.FindRev('.');

				if( uLen - uDot == 4 ) {

					return editNone;
					}

				pEdit->m_Edit += WCHAR(uCode);

				return editUpdate;
				}

			return editNone;
			}
		}

	return editNone;
	}

// Limit Access

DWORD CDispFormatIPAddr::GetMin(UINT Type)
{
	if( Type == typeInteger ) {

		return 0x80000000;
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatIPAddr::GetMax(UINT Type)
{
	if( Type == typeInteger ) {

		return 0x7FFFFFFF;
		}

	if( Type == typeReal ) {

		return R2I(C3INT(0xFFFFFFFF));
		}

	return CDispFormat::GetMax(Type);
	}

// End of File
