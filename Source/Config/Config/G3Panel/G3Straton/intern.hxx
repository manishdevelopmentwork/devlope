
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Support
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_INTERN_HXX
	
#define	INCLUDE_INTERN_HXX

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define IDS_BASE                0x4600
#define IDS_CHANGE_SOURCE       0x4600
#define IDS_CHILD_OF_FMT        0x4601
#define IDS_COIL_SELECTED       0x4602
#define IDS_CONTACT_SELECTED    0x4603
#define IDS_CREATE_NEW          0x4604
#define IDS_CREATE_NEW_2        0x4605
#define IDS_DUMMY               0x4606 /* NOT USED */
#define IDS_FUNCTION_BLOCK      0x4607
#define IDS_FUNCTION_SELECTED   0x4608
#define IDS_GENERAL             0x4609
#define IDS_IN                  0x460A
#define IDS_INPUT               0x460B
#define IDS_INPUTOUTPUT         0x460C
#define IDS_INPUTOUTPUT_2       0x460D
#define IDS_INSTRUCTION_LIST    0x460E
#define IDS_ITEM_WITH_THIS      0x460F /* NOT USED */
#define IDS_LADDER_DIAGRAM      0x4610
#define IDS_LINE_FMT_CHAR_FMT   0x4611
#define IDS_NAME_CONTAINS       0x4612
#define IDS_NAME_IS_EMPTY       0x4613
#define IDS_NAME_IS_TOO_LONG    0x4614
#define IDS_NEW_VARIABLE        0x4615
#define IDS_OUTPUT              0x4616
#define IDS_PARAMETER           0x4617
#define IDS_PICK_VARIABLEN      0x4618
#define IDS_POSITION_FMT        0x4619
#define IDS_POSITION_FMT_2      0x461A
#define IDS_PROGRAM_WITH_NAME   0x461B
#define IDS_READ_AND_WRITE      0x461C
#define IDS_READ_ONLY           0x461D
#define IDS_SEQUENTIAL_FLOW     0x461E
#define IDS_SIZE_FMT            0x461F
#define IDS_SPECIFIED_PROGRAM   0x4620
#define IDS_STRUCTURED_TEXT     0x4621
#define IDS_UNKNOWN             0x4622
#define IDS_VARIABLE            0x4623
#define IDS_VARIABLE_SELECTED   0x4624
#define IDS_VARIABLE_WITH       0x4625
#define IDS_ZOOM_FMT            0x4626

//////////////////////////////////////////////////////////////////////////
//
// Command Identifiers
//

#define	IDM_EDIT_SEL_VAR		0x8230
#define	IDM_EDIT_SEL_FB			0x8231

#define IDM_VIEW_EXECUTION_ORDER	0x8350
#define IDM_VIEW_GRID_SHOW		0x8351
#define IDM_VIEW_GRID_SNAP		0x8352
#define IDM_VIEW_ZOOM_MORE		0x8353
#define IDM_VIEW_ZOOM_LESS		0x8354
#define IDM_VIEW_ZOOM_FIT		0x8355
#define IDM_VIEW_ZOOM_FITSEL		0x8356
#define IDM_VIEW_ZOOM_16_1		0x8357
#define IDM_VIEW_ZOOM_8_1		0x8358
#define IDM_VIEW_ZOOM_4_1		0x8359
#define IDM_VIEW_ZOOM_2_1		0x835A
#define IDM_VIEW_ZOOM_NORM		0x835B
#define IDM_VIEW_ZOOM_1_2		0x835C
#define IDM_VIEW_ZOOM_1_4		0x835D
#define IDM_VIEW_ZOOM_1_8		0x835E
#define IDM_VIEW_ZOOM_1_16		0x835F
#define IDM_VIEW_LINES_SHOW		0x8360
#define IDM_VIEW_COLOR_SHOW		0x8361
#define IDM_VIEW_VALUE_SHOW		0x8362
#define IDM_VIEW_SWAP_HEX		0x8363

#define IDM_TOOL			0xC5
#define IDM_TOOL_SELECT			0xC501
#define IDM_TOOL_FUNCTION		0xC502
#define IDM_TOOL_VARIABLE		0xC503
#define IDM_TOOL_COMMENT		0xC504
#define IDM_TOOL_ARC			0xC505
#define IDM_TOOL_CORNER			0xC506
#define IDM_TOOL_BREAK			0xC507
#define IDM_TOOL_LABEL			0xC508
#define IDM_TOOL_JUMP			0xC509
#define IDM_TOOL_LEFT_RAIL		0xC50A
#define IDM_TOOL_CONTACT		0xC50B
#define IDM_TOOL_OR_RAIL		0xC50C
#define IDM_TOOL_COIL			0xC50D
#define IDM_TOOL_RIGHT_RAIL		0xC50E

#define	IDM_TOOL_CONTACT_BEFORE		0xC510
#define	IDM_TOOL_CONTACT_AFTER		0xC511
#define	IDM_TOOL_CONTACT_PARALLEL	0xC512
#define	IDM_TOOL_RUNG			0xC513
#define	IDM_TOOL_HORZ			0xC514
#define	IDM_TOOL_ALIGN_COILS		0xC515
#define	IDM_TOOL_SWAP_STYLE		0xC516
#define	IDM_TOOL_FB_BEFORE		0xC517
#define	IDM_TOOL_FB_AFTER		0xC518
#define	IDM_TOOL_FB_PARALLEL		0xC519

#define	IDM_TOOL_INIT_STEP		0xC520
#define	IDM_TOOL_STEP			0xC521
#define	IDM_TOOL_TRANSITION		0xC522
#define	IDM_TOOL_SET_TIMER		0xC523
#define	IDM_TOOL_DIVERGENCE		0xC524
#define	IDM_TOOL_DIVERGENCE_MAIN	0xC525
#define	IDM_TOOL_CONVERGENCE		0xC526
#define	IDM_TOOL_RENUMBER		0xC527
#define	IDM_TOOL_EDIT_REF		0xC528

#define	IDM_TOOL_INDENT			0xC530
#define	IDM_TOOL_INSERT_COMMENT		0xC531
#define	IDM_TOOL_REMOVE_COMMENT		0xC532

#define IDM_TOOL_KEEP_FBD_SELECT	0xC540
#define IDM_TOOL_UNDERLINE_GLOBAL	0xC541
#define IDM_TOOL_INSERT_VAR_WITH_FB	0xC542
#define IDM_TOOL_CREATE_GLOBAL		0xC543

// End of File

#endif
