
#include "intern.hpp"

#include "PrimRubyBeveled.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Beveled Rectangle Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyBeveled, CPrimRubyTrimmed);

// Constructor

CPrimRubyBeveled::CPrimRubyBeveled(void)
{
	m_style = 0;
	}

// Meta Data

void CPrimRubyBeveled::AddMetaData(void)
{
	CPrimRubyTrimmed::AddMetaData();

	Meta_SetName((IDS_BEVELED_RECTANGLE));
	}

// End of File
