
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#include "Module.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphite Module Configuration
//

class CGraphiteModule : public CModule
{
	public:
		// Constructor
		CGraphiteModule(void);

	protected:
		// Overridables
		void OnLoad(PCBYTE &pData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite Module Configuration
//

// Instantiator

CModule * Create_GraphiteModule(void)
{
	return New CGraphiteModule;
	}

// Constructor

CGraphiteModule::CGraphiteModule(void)
{
	}

// Overridables

void CGraphiteModule::OnLoad(PCBYTE &pData)
{
	m_FirmID  = GetByte(pData);

	m_wNumber = GetWord(pData);

	m_dwSlot  = GetLong(pData);
	}

//////////////////////////////////////////////////////////////////////////
//
// Graphite Module Configuration 32-Bit
//

class CGraphiteModule32 : public CGraphiteModule
{
	public:
		// Constructor
		CGraphiteModule32(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphite Module Configuration 32-Bit
//

// Instantiator

CModule * Create_GraphiteModule32(void)
{
	return New CGraphiteModule32;
	}

// Constructor

CGraphiteModule32::CGraphiteModule32(void)
{
	m_fAddr32 = true;
	}

// End of File
