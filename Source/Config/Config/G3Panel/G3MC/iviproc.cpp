
#include "intern.hpp"

#include "legacy.h"

#include "ivimod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- 8-Channel PI and PV Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUIIVIProcess, CUIEditBox)

// Linked List

CUIIVIProcess * CUIIVIProcess::m_pHead = NULL;

CUIIVIProcess * CUIIVIProcess::m_pTail = NULL;

// Constructor

CUIIVIProcess::CUIIVIProcess(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Destructor

CUIIVIProcess::~CUIIVIProcess(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Update Support

void CUIIVIProcess::CheckUpdate(CIVIInput *pLoop, CString const &Tag)
{
	if( Tag.Left(4) == "Proc" ) {

		CUIIVIProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextIVIProcess *pText = (CUITextIVIProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( Tag.Left(4) == "Proc" )

					pScan->Update();
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag == "LinDP" || Tag == "LinChannel" ) {

		CUIIVIProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextIVIProcess *pText = (CUITextIVIProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				pScan->Update();
				}

			pScan = pScan->m_pNext;
			}
		}
	}

// Operations

void CUIIVIProcess::Update(void)
{
	CUITextIVIProcess *pText = (CUITextIVIProcess *) m_pText;

	pText->GetConfig();

	m_pDataCtrl->SetModify(TRUE);

	OnSave(FALSE);

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- 8-Channel PI and PV Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextIVIProcess, CUITextInteger)

// Constructor

CUITextIVIProcess::CUITextIVIProcess(void)
{
	}

// Destructor

CUITextIVIProcess::~CUITextIVIProcess(void)
{
	}

// Core Overidables

void CUITextIVIProcess::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CIVIInput)) ) {

		m_pLoop = (CIVIInput *) m_pItem;
		}
	else
		AfxAssert(FALSE);

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
	}

// Implementation

void CUITextIVIProcess::GetConfig(void)
{
	m_cType = char(m_UIData.m_Format[0]);

	switch( m_cType ) {

		case 'A':
			m_uPlaces = m_pLoop->m_ProcessDP1;
			break;
		case 'B':
			m_uPlaces = m_pLoop->m_ProcessDP2;
			break;
		case 'C':
			m_uPlaces = m_pLoop->m_ProcessDP3;
			break;
		case 'D':
			m_uPlaces = m_pLoop->m_ProcessDP4;
			break;
		case 'E':
			m_uPlaces = m_pLoop->m_ProcessDP5;
			break;
		case 'F':
			m_uPlaces = m_pLoop->m_ProcessDP6;
			break;
		case 'G':
			m_uPlaces = m_pLoop->m_ProcessDP7;
			break;
		case 'H':
			m_uPlaces = m_pLoop->m_ProcessDP8;
			break;
		case 'I':
			m_uPlaces = m_pLoop->m_LinDP;
			break;
		default:
			m_uPlaces = 2;
			break;
		}

	m_nMin = -30000;

	m_nMax = +30000;

	CheckFlags();
	}

void CUITextIVIProcess::CheckFlags(void)
{
	if( m_uPlaces ) {
		
		m_uFlags |= textPlaces;
		}
	else 
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
		}
	else 
		m_uFlags &= ~textSigned;
	}

// End of File
