
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical User Interface Element
//

// Runtime Class

AfxImplementRuntimeClass(CUIElement, CUIBaseElement);

// Constructor

CUIElement::CUIElement(void)
{
	m_fTable      = FALSE;

	m_pText       = NULL;

	m_pMainLayout = NULL;

	m_fMulti      = FALSE;

	m_fShow	      = FALSE;

	m_fBusy       = FALSE;
	}

// Destructor

CUIElement::~CUIElement(void)
{
	delete m_pText;
	}

// Binding

BOOL CUIElement::Bind(CItem *pItem, CUIData const *pUIData, BOOL fTable)
{
	m_fTable = fTable;

	m_Fixed  = pItem->GetFixedPath();

	return CUIBaseElement::Bind(pItem, pUIData);
	}

// Attributes

CRect CUIElement::GetRect(void) const
{
	return m_pMainLayout->GetRect();
	}

BOOL CUIElement::IsVisible(void) const
{
	return m_fShow;
	}

CString CUIElement::GetFixed(void) const
{
	return m_Fixed;
	}

// Text Access

CUITextElement * CUIElement::GetText(void) const
{
	return m_pText;
	}

// Core Operations

void CUIElement::LayoutUI(CLayFormation *pForm)
{
	OnLayout(pForm);
	}

void CUIElement::CreateUI(CWnd &Wnd, UINT &uID)
{
	OnCreate(Wnd, uID);

	OnLoad();
	}

void CUIElement::PositionUI(void)
{
	OnPosition();
	}

void CUIElement::ShowUI(BOOL fShow)
{
	OnShow(m_fShow = fShow);
	}

void CUIElement::EnableUI(BOOL fEnable)
{
	OnEnable(fEnable);
	}

void CUIElement::UpdateUI(void)
{
	LoadUI();
	}

void CUIElement::OffsetUI(CSize const &Offset)
{
	m_pMainLayout->SetOffset(Offset);
	}

void CUIElement::ScrollData(UINT uCode)
{
	OnScrollData(uCode);
	}

void CUIElement::PaintUI(CRect const &Work, CDC &DC)
{
	OnPaint(Work, DC);
	}

BOOL CUIElement::FindFocus(CWnd * &pWnd)
{
	return OnFindFocus(pWnd);
	}

void CUIElement::AllowMulti(BOOL fMulti)
{
	m_fMulti = fMulti;
	}

// Data Operations

void CUIElement::LoadUI(void)
{
	OnLoad();
	}

UINT CUIElement::SaveUI(BOOL fUI)
{
	m_fBusy++;

	UINT uCode = OnSave(fUI);

	m_fBusy--;

	return uCode;
	}

BOOL CUIElement::CanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	return OnCanAcceptData(pData, dwEffect);
	}

BOOL CUIElement::AcceptData(IDataObject *pData)
{
	return OnAcceptData(pData);
	}

// Notifications

BOOL CUIElement::NotifyUI(UINT uID, UINT uCode)
{
	return OnNotify(uID, uCode);
	}

BOOL CUIElement::NotifyUI(UINT uID, NMHDR &Info)
{
	return OnNotify(uID, Info);
	}

// Core Overidables

void CUIElement::OnBind(void)
{
	if( !m_pText ) {

		if( !m_UIData.m_ClassText ) {

			AfxTrace(L"ERROR: ui element has neither own nor schema-defined text element\n");

			AfxAssert(FALSE);
			}

		if( !(m_pText = AfxNewObject(CUITextElement, m_UIData.m_ClassText)) ) {

			AfxTrace(L"ERROR: ui element cannot create text element\n");

			AfxAssert(FALSE);
			}
		}

	m_pText->Bind(m_pItem, &m_UIData);
	}

void CUIElement::OnRebind(void)
{
	m_pText->RebindUI(m_pItem);

	m_Fixed = m_pItem->GetFixedPath();

	LoadUI();
	}

void CUIElement::OnLayout(CLayFormation *pForm)
{
	}

void CUIElement::OnCreate(CWnd &Wnd, UINT &uID)
{
	}

void CUIElement::OnPosition(void)
{
	}

void CUIElement::OnShow(BOOL fShow)
{
	}

void CUIElement::OnEnable(BOOL fEnable)
{
	}

void CUIElement::OnScrollData(UINT uCode)
{
	}

void CUIElement::OnPaint(CRect const &Work, CDC &DC)
{
	}

BOOL CUIElement::OnFindFocus(CWnd * &pWnd)
{
	return FALSE;
	}

// Data Overridables

void CUIElement::OnLoad(void)
{
	}

UINT CUIElement::OnSave(BOOL fUI)
{
	return saveSame;
	}

BOOL CUIElement::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	return FALSE;
	}

BOOL CUIElement::OnAcceptData(IDataObject *pData)
{
	return FALSE;
	}

// Notification Overridables

BOOL CUIElement::OnNotify(UINT uID, UINT uCode)
{
	return FALSE;
	}

BOOL CUIElement::OnNotify(UINT uID, NMHDR &Info)
{
	return FALSE;
	}

// Implementation

UINT CUIElement::StdSave(BOOL fUI, CString Data)
{
	CString Last = m_pText->GetAsText();

	if( Data.CompareC(Last) ) { 

		CError Error(fUI);

		UINT uCode = m_pText->SetAsText(Error, Data);

		if( Error.IsOkay() ) {

			LoadUI();
			}
		else {
			if( Error.AllowUI() ) {

				Error.Show(*afxMainWnd);
				}
			}

		return uCode;
		}

	LoadUI();

	return saveSame;
	}

// End of File
