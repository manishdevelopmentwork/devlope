
#include "Intern.hpp"

#include "ScsiCmd6.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Descriptor Block 6
//

// Constructor

CScsiCmd6::CScsiCmd6(void)
{
	}

// Endianess

void CScsiCmd6::HostToScsi(void)
{
	m_wLba = HostToMotor(m_wLba);
	}

void CScsiCmd6::ScsiToHost(void)
{
	m_wLba = MotorToHost(m_wLba);
	}

// Attributes

DWORD CScsiCmd6::GetLBA(void) 
{
	return (DWORD(m_bLba) << 16) | m_wLba;
	}

// Operations

void CScsiCmd6::Init(void)
{
	memset(this, 0, sizeof(ScsiCmd6));
	}

void CScsiCmd6::Init(BYTE bOpcode)
{
	Init(bOpcode, 0, 0);
	}

void CScsiCmd6::Init(BYTE bOpcode, BYTE bLength)
{
	Init(bOpcode, bLength, 0);
	}

void CScsiCmd6::Init(BYTE bOpcode, BYTE bLength, DWORD dwLBA)
{
	Init();

	m_bOpcode = bOpcode;

	m_bLength = bLength;

	m_bLba    = dwLBA >> 16;

	m_wLba    = dwLBA & 0xFFFF;
	}

// End of File
