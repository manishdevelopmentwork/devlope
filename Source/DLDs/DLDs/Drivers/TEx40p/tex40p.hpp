//////////////////////////////////////////////////////////////////////////
//
// Toshiba EX40+ Space Offsets 
//
//

WORD Offset[] = { 0x0100, 0x0140, 0x0180, 0x01E0, 0x0200, 0x0220, 0x0260, 0x0300, 0x0400 };

//////////////////////////////////////////////////////////////////////////
//
// Toshiba EX40+ Master Serial Driver 
//
//

class CToshEx40pMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CToshEx40pMasterDriver(void);

		// Destructor
		~CToshEx40pMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			BYTE m_bDrop;
			BOOL m_fCheck;
						
			};

		
		CContext * m_pCtx;

		// Data Members

		LPCTXT 	m_pHex;
		BYTE	m_bCheck;
		UINT	m_uRxLen;
		UINT	m_bPtr;
		BYTE	m_bRx[96];
		BYTE	m_bTx[96];

		// Implementation

		UINT GetRead(AREF Addr, PDWORD pData, UINT uCount);
		UINT GetByteData(PDWORD pData, UINT uCount);
		UINT GetWordData(PDWORD pData, UINT uCount);
		UINT GetLongData(PDWORD pData, UINT uCount);
		UINT GetByteCount(UINT uType, UINT uCount);
		void Begin(BOOL fRead);
		void AddByte(BYTE bByte);
		void AddHex(UINT n, UINT d);
		void AddDec(UINT n, UINT d);
		void AddTextData(AREF Addr, UINT uCount, PDWORD pData);
		BOOL Transact(void);
		BOOL Send(void);
		BOOL Recv(void);
		BOOL Check(void);

		// Helpers
		
		BOOL  IsBitReg(UINT uType);
		BOOL  IsByte(UINT uType);
		BOOL  IsWord(UINT uType);
		BOOL  IsLong(UINT uType);
		DWORD xtoin(PCTXT pText, UINT uCount);
		BYTE  FromAscii(BYTE bByte);

	}; 

// End of File
