//////////////////////////////////////////////////////////////////////////
//
// Stiebel WPMII Master Serial Driver
//

class CWpmIIMasterSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CWpmIIMasterSerialDriver(void);

		// Destructor
		~CWpmIIMasterSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		
	protected:

		BYTE	m_bRx[12];
		BYTE	m_bTx[12];
		UINT	m_Ptr;
		WORD	m_Check;
		
		// Implementation
		BOOL	SendInit(void);
		void	Begin(void);
		void	AddByte(BYTE bByte);
		void	AddWord(WORD wWord);
		void	End(void);
		BOOL	Transact(void);
		BOOL	Send(void);
		BOOL	Recv(void);
		BOOL	CheckFrame(void);
		
		// Helpers
		BOOL	IsRead(UINT uAdd);
		BOOL	IsWrite(UINT uAdd);
		BYTE	FindByte3(UINT uAdd, UINT uTable);
		BYTE	FindByte5(UINT uAdd, UINT uTable);
		UINT	FindAdd(UINT uTable, UINT uAdd);

	};

// End of file

