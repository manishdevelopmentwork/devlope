
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_ScsiCmdLock_HPP

#define	INCLUDE_ScsiCmdLock_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "Scsi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Lock Media
//

class CScsiCmdLock : public ScsiCmdLock
{
	public:
		// Constructor
		CScsiCmdLock(void);

		// Endianess
		void HostToScsi(void);
		void ScsiToHost(void);

		// Operations
		void Init(bool fLock);
	};

// End of File

#endif
