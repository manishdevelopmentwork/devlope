
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubDesc_HPP

#define	INCLUDE_UsbHubDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Descriptor
//

class CUsbHubDesc : public UsbHubDesc
{
	public:
		// Constructor
		CUsbHubDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Init
		void Init(void);

		// Debug
		void Debug(void);
	};

// End of File

#endif
