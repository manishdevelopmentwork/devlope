
#include "Intern.hpp"

#include "TcpProtocol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "PseudoHeader.hpp"

#include "TcpHeader.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP Protocol
//

// Instantiator

IProtocol * Create_TCP(void)
{
	return New CTcp;
	}

// Constructor

CTcp::CTcp(void)
{
	StdSetRef();

	m_pLock   = Create_Rutex();

	m_pRouter = NULL;

	m_pUtils  = NULL;

	m_uMask   = 0;

	m_uSend   = 0;

	memset(m_Used, 0, sizeof(m_Used));

	BindSockets();
	}

// Destructor

CTcp::~CTcp(void)
{
	AfxRelease(m_pLock);
	}

// Attributes

BOOL CTcp::AcceptIP(IPREF IP) const
{
	return m_pUtils->IsSafeAddress(IP);
	}

// Operations

void CTcp::Enable(WORD wPort)
{
	m_Used[wPort / 32] |= (1 << (wPort % 32));
	}

BOOL CTcp::Send(IPREF Dest, IPREF From, CBuffer *pBuff, BOOL fKeep)
{
	CAutoBuffer Buff(pBuff);

	CTcpHeader *pTcp = (CTcpHeader *) pBuff->GetData();

	UINT uSize = pBuff->GetSize();

	UINT uInit = pBuff->GetInit();

	pTcp->HostToNet();

	if( !m_pRouter->Send(Dest, From, IP_TCP, pBuff, this) ) {

		if( fKeep ) {

			pTcp->NetToHost(uSize);

			pBuff->SetInit(uInit);

			Buff.TakeOver();
			}

		TcpDebug(OBJ_TCP, LEV_TRACE, "failed to send\n");

		return FALSE;
		}

	if( fKeep ) {

		pTcp->NetToHost(uSize);

		pBuff->SetInit(uInit);

		Buff.TakeOver();
		}

	return TRUE;
	}

void CTcp::SendReq(BOOL fState)
{
	if( fState ) {

		if( !m_uSend++ ) {

			m_pRouter->SendReq( m_uMask,
					    TRUE
					    );
			}
		}
	else {
		if( !--m_uSend ) {

			m_pRouter->SendReq( m_uMask,
					    FALSE
					    );
			}
		}
	}

void CTcp::CopyOptions(ISocket *pSock, IPREF IP)
{
	m_pRouter->CopyOptions(pSock, IP);
	}

void CTcp::FreeSocket(CTcpSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) pSock->m_pRoot;

	AfxListRemove(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);
	}

// IUnknown

HRESULT CTcp::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IProtocol);

	StdQueryInterface(IThreadNotify);

	StdQueryInterface(IProtocol);

	return E_NOINTERFACE;
	}

ULONG CTcp::AddRef(void)
{
	StdAddRef();
	}

ULONG CTcp::Release(void)
{
	StdRelease();
	}

// IThreadNotify

UINT CTcp::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	CListRoot *pRoot = New CListRoot;

	pRoot->m_pHead = NULL;

	pRoot->m_pTail = NULL;

	return UINT(pRoot);
	}

void CTcp::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam)
{
	CListRoot *pRoot = (CListRoot *) uParam;

	if( pRoot->m_pHead ) {

		AfxTrace("tcpip: thread %u exiting with open tcp sockets\n", pThread->GetIdent());

		while( pRoot->m_pHead ) {

			CTcpSocket *pSock = pRoot->m_pHead;

			pSock->Release();

			AfxAssert(pRoot->m_pHead != pSock);
			}
		}

	delete pRoot;
	}

// IProtocol

BOOL CTcp::Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask)
{
	m_pRouter = pRouter;

	m_pUtils  = pUtils;

	m_uMask   = uMask;

	return TRUE;
	}

BOOL CTcp::CreateSocket(ISocket * &pSocket)
{
	for( UINT p = 0; p < 4; p++ ) {

		UINT uWaitSock = NOTHING;

		UINT uWaitTime = NOTHING;

		BOOL fEndClose = FALSE;

		UINT n;

		CAutoLock Lock(m_pLock);

		for( n = 0; n < elements(m_Sock); n++ ) {

			CTcpSocket &Sock = m_Sock[n];

			if( !Sock.IsFree() ) {

				if( Sock.IsWaiting() ) {

					if( Sock.GetWait() < uWaitTime ) {

						uWaitTime = Sock.GetWait();

						uWaitSock = n;
						}
					}
				}
			else {
				Sock.Create();

				AddSocket(&Sock);

				pSocket = &Sock;

				return TRUE;
				}
			}

		if( uWaitSock < NOTHING ) {

			CTcpSocket &Sock = m_Sock[uWaitSock];

			if( Sock.EndWait() ) {

				Sock.Create();

				AddSocket(&Sock);

				pSocket = &Sock;

				return TRUE;
				}

			continue;
			}

		for( n = 0; n < elements(m_Sock); n++ ) {

			CTcpSocket &Sock = m_Sock[n];
			
			if( Sock.EndClose() ) {

				fEndClose = TRUE;
				}
			}

		Lock.Free();

		if( fEndClose ) {

			Sleep(25);

			continue;
			}

		break;
		}

	pSocket = NULL;

	return FALSE;
	}

BOOL CTcp::Recv(UINT uFace, IPREF Face, PSREF PS, CBuffer *pBuff)
{
	CAutoBuffer Buff(pBuff);

	UINT uSize = pBuff->GetSize();
	
	if( uSize >= sizeof(TCPHEADER) ) {

		CTcpHeader *pTcp = BuffStripHead(pBuff, CTcpHeader);

		if( pBuff->TestFlag(packetSumsValid) || pTcp->TestChecksum(PS, uSize) ) {

			pTcp->NetToHost(uSize);

			BOOL fHit = FALSE;

			if( IsPortUsed(pTcp->m_RemPort) ) {

				UINT uOpts = pTcp->GetOptionsSize();

				pBuff->StripHead(uOpts);
			
				for( UINT n = 0; n < elements(m_Sock); n++ ) {

					CTcpSocket &Sock = m_Sock[n];

					if( Sock.OnTest(PS, pTcp, fHit) ) {

						if( Sock.OnRecv(PS, pTcp, pBuff) ) {

							Buff.TakeOver();

							return TRUE;
							}
						}
					}
				}

			if( !fHit ) {

				RejectSegment(PS, pTcp);
				}

			return TRUE;
			}

		TcpDebug(OBJ_TCP, LEV_WARN, "checksum invalid\n");

		return TRUE;
		}

	TcpDebug(OBJ_TCP, LEV_WARN, "packet too small\n");

	return TRUE;
	}

BOOL CTcp::Send(void)
{
	BOOL fFail = FALSE;

	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CTcpSocket &Sock = m_Sock[n];

		if( !Sock.OnSend() ) {

			fFail = TRUE;
			}
		}

	return !fFail;
	}

BOOL CTcp::Poll(void)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CTcpSocket &Sock = m_Sock[n];

		Sock.OnPoll();
		}

	return TRUE;
	}

BOOL CTcp::NetStat(IDiagOutput *pOut)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CTcpSocket &Sock = m_Sock[n];

		Sock.NetStat(pOut);
		}

	return TRUE;
	}

BOOL CTcp::SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff)
{
	CTcpHeader *pTcp = (CTcpHeader *) pBuff->GetData();

	if( pTcp ) {

		DWORD PSSum = m_pRouter->GetPseudoSum(Dest, From, IP_TCP, pBuff);

		UINT uSize  = pBuff->GetSize();

		pTcp->AddChecksum(PSSum, uSize);

		return TRUE;
		}

	return FALSE;
	}

// Implementation

void CTcp::BindSockets(void)
{
	for( UINT n = 0; n < elements(m_Sock); n++ ) {

		CTcpSocket &Sock = m_Sock[n];

		Sock.Bind(this, n);
		}
	}

void CTcp::RejectSegment(PSREF PS, CTcpHeader *pReq)
{
	if( !PS.m_Dest.IsBroadcast() && !PS.m_Dest.IsMulticast() ) {

		if( !pReq->m_FlagRST ) {

			CBuffer *pBuff = BuffAllocate(0);

			if( pBuff ) {

				CTcpHeader *pTcp = BuffAddHead(pBuff, CTcpHeader);

				pTcp->Init();

				pTcp->m_LocPort = pReq->m_RemPort;

				pTcp->m_RemPort = pReq->m_LocPort;

				if( !pReq->m_FlagACK ) {

					pTcp->m_Seq     = 0;

					pTcp->m_Ack     = pReq->m_Seq + pReq->GetSymbols();

					pTcp->m_FlagACK = TRUE;

					pTcp->m_FlagRST = TRUE;
					}
				else {
					pTcp->m_Seq     = pReq->m_Ack;

					pTcp->m_Ack     = 0;

					pTcp->m_FlagRST = TRUE;
					}

				UINT  uSize = pBuff->GetSize();

				DWORD PSSum = m_pRouter->GetPseudoSum(PS.m_Src, PS.m_Dest, IP_TCP, pBuff);

				pTcp->HostToNet();

				pTcp->AddChecksum(PSSum, uSize);

				m_pRouter->Queue(PS.m_Src, PS.m_Dest, IP_TCP | IP_STEALTH, pBuff);
				}

			TcpDebug(OBJ_TCP, LEV_TRACE, "rejected packet for %u\n", pReq->m_RemPort);
			}
		}
	}

BOOL CTcp::IsPortUsed(WORD wPort)
{
	if( m_Used[wPort / 32] & (1 << (wPort % 32)) ) {

		return TRUE;
		}

	return FALSE;
	}

void CTcp::AddSocket(CTcpSocket *pSock)
{
	CListRoot *pRoot = (CListRoot *) AddThreadNotify(this);

	AfxListAppend(pRoot->m_pHead, pRoot->m_pTail, pSock, m_pNext, m_pPrev);

	pSock->m_pRoot = pRoot;
	}

// End of File
