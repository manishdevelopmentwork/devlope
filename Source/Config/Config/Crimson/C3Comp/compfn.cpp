
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Function Compiler
//

DLLAPI BOOL C3CompileFunc( CError	  & Error,
			   CRange	  & Range,
			   PCTXT	    pText,
			   CTypeDef       & Type,
			   UINT		    uParam,
			   CFuncParam     * pParam,
			   INameServer    * pName,
			   CByteArray     * pSource,
			   CLongArray     * pRefList,
			   CByteArray     * pObject
			   )
{
	CFuncParser *pParser = New CFuncParser;

	CTypeDef     Real    = Type;

	if( pParser->ParseFunc(Error, pText, Real, uParam, pParam, pName) ) {

		if( pObject ) {

			CParseTree &Tree = pParser->GetTree();

			if( Tree.GetCount() ) {

				CTreeOptimizer *pOptim = New CTreeOptimizer(Tree);

				if( pOptim->Optimize(Error) ) {

					CCodeGenerator *pCoder = New CCodeGenerator(Tree);

					UINT uDepth = pParser->GetDepth() + 2;

					UINT uLocal = pParser->GetLocal();

					pCoder->Prepare(uDepth, uLocal, uParam, pParam);

					if( !(Type.m_Flags & flagInherent) ) {

						Real = Type;
						}

					if( pCoder->GenerateFunc(Error, Real) ) {

						pObject->Empty();

						pObject->Append(pCoder->GetCode());
						}
					else
						Range.Empty();

					delete pCoder;
					}
				else
					Range = pOptim->GetErrorPos();

				delete pOptim;
				}
			}

		if( pSource ) {

			pSource->Empty();

			pSource->Append(pParser->GetSource());
			}

		if( pRefList ) {

			pRefList->Empty();

			pRefList->Append(pParser->GetRefList());
			}
		}
	else
		Range = pParser->GetTokenPos();

	delete pParser;

	return Error.IsOkay();
	}

// End of File
