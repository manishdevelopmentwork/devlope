
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Splash Window
//

// Runtime Class

AfxImplementRuntimeClass(CSplashWnd, CWnd);

// Constructor

CSplashWnd::CSplashWnd(void)
{
	m_Size.cx = 500;

	m_Size.cy = 300;

	m_Bitmap.Create(L"Splash");
	}

// Destructor

CSplashWnd::~CSplashWnd(void)
{
	}

// Message Map

AfxMessageMap(CSplashWnd, CWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_TIMER)

	AfxMessageEnd(CSplashWnd)
	};

// Message Handlers

void CSplashWnd::OnPostCreate(void)
{
	CSize  Screen = CSize(SM_CXSCREEN);

	CRect  Rect   = CRect(80, 20, Screen.cx - 20, Screen.cy - 60);

	CSize  Size   = m_Size + CSize(2, 2);

	CPoint Point  = Rect.GetTopLeft() + (Rect.GetSize() - Size) / 2;

	MoveWindow(CRect(Point, Size), TRUE);

	BuildList();
	}

BOOL CSplashWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	DC.FrameRect(Rect, afxBrush(BLACK));

	return TRUE;
	}

void CSplashWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect  Rect = GetClientRect();

	CRect  Draw = CRect(Rect.GetTopLeft() + CSize(1, 1), m_Size);

	CPoint From = CPoint(0, 0);

	DC.BitBlt(Draw, m_Bitmap, From, SRCCOPY);

	DC.Select(afxFont(Bolder));

	DC.SetBkMode(TRANSPARENT);

	BOOL rl = (C3OemCompany() != L"Honeywell-01");

	int  cy = DC.GetTextExtent(L"X").cy + (rl ? 0 : 2);

	int  yp = Draw.bottom - 10 - 2 * m_List.GetCount() / 3 * cy;

	for( UINT n = 0; n < m_List.GetCount(); n++ ) {

		CString Text = m_List[n];

		if( Text.IsEmpty() ) {

			yp += cy / 2;
			}
		else {
			if( n || !rl ) {

				int cx = DC.GetTextExtent(Text).cx;

				int xp = Draw.right - 20 - cx;

			//	DC.SetTextColor(afxColor(WHITE));

			//	DC.TextOut(xp + 1, yp + 1, Text);

				DC.SetTextColor(afxColor(BLACK));

				DC.TextOut(xp + 0, yp + 0, Text);
				}

			yp += cy;
			}
		}

	DC.Deselect();
	}

void CSplashWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	ShowWindow(SW_HIDE);

	DestroyWindow(FALSE);
	}

// Implementation

void CSplashWnd::BuildList(void)
{
	CRegKey Key     = afxModule->GetApp()->GetMachineRegKey();

	CString Version = Key.GetValue(L"Version", L"3.2");

	CString Branch  = CString(C3_BRANCH);

	Branch.Delete(0, Branch.Find('-') + 1);

	Branch.Delete(Branch.Find('-'), NOTHING);

	C3OemStrings(Version);

	CPrintf Text( IDS_APP_BUILD,
		      Version,
		      Branch,
		      C3_BUILD,
		      C3_HOTFIX,
		      L"\x00A9",
		      2020,
		      L"Red Lion Controls Inc"
		      );

	Text.Tokenize(m_List, '\n');
	}

// End of File
