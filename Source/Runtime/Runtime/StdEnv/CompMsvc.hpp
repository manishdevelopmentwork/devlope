
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_CompMsvc_HPP

#define INCLUDE_CompMsvc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Compiler Flag
//

#define AEON_COMP_MSVC

//////////////////////////////////////////////////////////////////////////
//
// What Else Goes In Here?
//

// Aligment control?
// Structure packing?
// Function attributes?

//////////////////////////////////////////////////////////////////////////
//
// Visual C++ 2010 Warning Control
//

#if _MSC_VER == 1600

#if defined(_DEBUG)

#pragma warning(disable: 4189)	// Unreferenced local variable

#endif

#pragma warning(disable: 4065)	// Switch has no case statements
#pragma warning(disable: 4100)	// Unreference formal parameter
#pragma warning(disable: 4127)	// Constant conditional expression
#pragma warning(disable: 4200)	// Zero-sized array in structure
#pragma warning(disable: 4201)	// Nameless structure or union
#pragma warning(disable: 4231)	// Nonstandard extension used
#pragma warning(disable: 4239)	// Non-const temporary reference
#pragma warning(disable: 4251)	// Mismatched export on classes
#pragma warning(disable: 4291)	// No matching delete operator
#pragma warning(disable: 4355)	// This used in base class init
#pragma warning(disable: 4511)	// No default copy constructor
#pragma warning(disable: 4512)	// No default assignment operator
#pragma warning(disable: 4514)	// Inline function not expanded
#pragma warning(disable: 4702)	// Unreachable code in function
#pragma warning(disable: 4706)	// Assignment within conditional
#pragma warning(disable: 4710)	// Function skipped for expansion
#pragma warning(disable: 4711)	// Function selected for expansion
#pragma warning(disable: 4748)	// Buffer overrun detection skipped
#pragma warning(disable: 4815)	// Zero sized array in stack struct
#pragma warning(disable: 4996)	// Deprecated function names

#else

#pragma message("Warning: Can't disable warnings for MSVC version!")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Assembly Code
//

#define	ASM __asm

//////////////////////////////////////////////////////////////////////////
//
// Min and Max Macros
//

#define	Min(a, b) ((a) < (b) ? (a) : (b))

#define	Max(a, b) ((a) > (b) ? (a) : (b))

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

AfxDefineType(INT64,  __int64);

AfxDefineType(UINT64, unsigned __int64);

AfxDefineType(SINT64, signed __int64);

//////////////////////////////////////////////////////////////////////////
//
// Basic COM Support
//

#define METHOD	__stdcall

//////////////////////////////////////////////////////////////////////////
//
// Variable Argument Lists
//

#include <stdarg.h>

//////////////////////////////////////////////////////////////////////////
//
// Inline Expansion
//

#define	INLINE		__inline

#define	STRONG_INLINE	__inline

//////////////////////////////////////////////////////////////////////////
//
// Likely and Unlikely Macros
//

#define likely(x)	(x)

#define unlikely(x)	(x)

//////////////////////////////////////////////////////////////////////////
//
// Dummy Assertion Macros
//

#if !defined(_DEBUG)

#define AfxAssert(x)	BEGIN { __assume(x); ((void) (0)); } END

#define	AfxVerify(x)	BEGIN { __assume(x); ((void) (x)); } END

#endif

//////////////////////////////////////////////////////////////////////////
//
// Other Built-Ins
//

#if !defined(__INTELLISENSE__)

STRONG_INLINE int BuiltInClz(DWORD x)
{
	if( x ) {

		__asm bsr eax, x
		__asm mov x, eax

		return 31 - x;
		}

	return 32;
	}

#else

#define BuiltInClz(n) 0

#endif

//////////////////////////////////////////////////////////////////////////
//
// Atomic Operations
//

#if !defined(__INTELLISENSE__)

extern "C" long _InterlockedCompareExchange(PVLONG, LONG, LONG);

extern "C" long _InterlockedExchangeAdd(PVLONG, LONG);

extern "C" long _InterlockedExchangeSub(PVLONG, LONG);

extern "C" long _InterlockedIncrement(PVLONG);

extern "C" long _InterlockedDecrement(PVLONG);

#define AtomicCompAndSwap(pv, ov, nv) (_InterlockedCompareExchange(PVLONG(pv), nv, ov))

#define AtomicFetchAndAdd(pv, n)      (_InterlockedExchangeAdd(PVLONG(pv), n))

#define AtomicFetchAndSub(pv, n)      (_InterlockedExchangeSub(PVLONG(pv), n))

#define AtomicIncrement(pv)	      (_InterlockedIncrement(PVLONG(pv)))
				      
#define AtomicDecrement(pv)	      (_InterlockedDecrement(PVLONG(pv)))

#else

#define AtomicCompAndSwap(pv, ov, nv) (*pv)

#define AtomicFetchAndSub(pv, n)      ((*pv)++)

#define AtomicFetchAndAdd(pv, n)      ((*pv)--)

#define AtomicIncrement(pv)	      (++(*pv))
				      
#define AtomicDecrement(pv)	      (--(*pv))

#endif

// End of File

#endif
