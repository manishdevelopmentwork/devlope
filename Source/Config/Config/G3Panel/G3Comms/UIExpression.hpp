
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UIExpression_HPP

#define INCLUDE_UIExpression_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsManager;

class CCommsSystem;

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Expression
//

class CUIExpression : public CUICategorizer
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIExpression(void);

	protected:
		// Modes
		enum
		{
			modeInternal   = 0,
			modeGeneral    = 1,
			modeComplex    = 2,
			modeTag	       = 3,
			modeNewTag     = 4,
			modeNext       = 5,
			modeGeneralWas = 1000,
			modeComplexWas = 1001,
			modeDevice     = 2000,
			};

		// Data Members
		CCommsSystem  * m_pSystem;
		CCommsManager * m_pComms;
		CString		m_Params;
		UINT	        m_cfCode;
		BOOL            m_fEmpty;
		BOOL		m_fComp;
		BOOL            m_fComms;
		BOOL		m_fTags;
		BOOL		m_fExpr;

		// Core Overridables
		void OnBind(void);

		// Notification Overridables
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);

		// Category Overridables
		BOOL    LoadModeButton(void);
		BOOL    SwitchMode(UINT uMode);
		UINT    FindDispMode(CString Text);
		CString FindDispText(CString Text, UINT uMode);
		CString FindDataText(CString Text, UINT uMode);
		UINT    FindDispType(UINT uMode);
		CString FindDispVerb(UINT uMode);

		// Implementation
		BOOL SelectTag(CString &Tag);
		BOOL IsTagName(CString Text);
		BOOL NewTag(void);
		BOOL EditCode(CString &Text);
		BOOL IsComplex(CString Text);
		void MakeComplex(CString &Code);
	};

// End of File

#endif
