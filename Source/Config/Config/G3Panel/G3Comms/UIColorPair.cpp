
#include "Intern.hpp"

#include "UIColorPair.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "ColorComboBox.hpp"

#include "ColorSample.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Color Pair
//

// Dynamic Class

AfxImplementDynamicClass(CUIColorPair, CUIControl);

// Constructor

CUIColorPair::CUIColorPair(void)
{
	m_fLockBack   = FALSE;

	m_pTextLayout = NULL;

	m_pWordLayout = NULL;

	m_pSampLayout = NULL;

	m_pForeLayout = NULL;

	m_pBackLayout = NULL;

	m_pTextCtrl   = New CStatic;

	m_pWordCtrl   = New CStatic;

	m_pSampCtrl   = New CColorSample;

	m_pForeCtrl   = New CColorComboBox(this);

	m_pBackCtrl   = New CColorComboBox(this);
	}

// Core Overridables

void CUIColorPair::OnBind(void)
{
	if( !m_fTable ) {

		m_Label = GetLabel() + L':';
		}

	if( m_UIData.m_Format[0] == 'f' ) {

		m_fLockBack = TRUE;
		}

	CUIControl::OnBind();
	}

void CUIColorPair::OnLayout(CLayFormation *pForm)
{
	m_pTextLayout = New CLayItemText(m_Label, 1, 1);

	m_pForeLayout = New CLayItemText(L"0123456789012345", 4, 1);

	m_pBackLayout = New CLayItemText(L"0123456789012345", 4, 1);

	m_pWordLayout = New CLayItemText(CString(IDS_ON_2),   2, 1);

	m_pSampLayout = New CLayItemText(L"ABCDABCD", 2, 1);

	m_pMainLayout = New CLayFormRow;

	m_pMainLayout->AddItem(New CLayFormPad(m_pForeLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pWordLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pBackLayout, horzLeft | vertCenter));

	m_pMainLayout->AddItem(New CLayFormPad(m_pSampLayout, horzLeft | vertCenter));

	pForm->AddItem(New CLayFormPad(m_pTextLayout, horzLeft | vertCenter));

	pForm->AddItem(m_pMainLayout);
	}

void CUIColorPair::OnCreate(CWnd &Wnd, UINT &uID)
{
	m_pTextCtrl->Create(	m_Label,
				0,
				m_pTextLayout->GetRect(),
				Wnd,
				0
				);

	m_pForeCtrl->Create(	WS_TABSTOP	   |
				WS_VSCROLL	   |
				CBS_DROPDOWNLIST   |
				CBS_OWNERDRAWFIXED,
				FindForeRect(),
				Wnd,
				uID++
				);

	m_pWordCtrl->Create(	CString(IDS_ON_2),
				0,
				m_pWordLayout->GetRect(),
				Wnd,
				0
				);

	m_pBackCtrl->Create(	WS_TABSTOP	   |
				WS_VSCROLL	   |
				CBS_DROPDOWNLIST   |
				CBS_OWNERDRAWFIXED,
				FindBackRect(),
				Wnd,
				uID++
				);

	m_pSampCtrl->Create(	L"ABCD",
				0,
				FindSampRect(),
				Wnd,
				0
				);

	if( m_fLockBack ) {

		m_pBackCtrl->EnableWindow(FALSE);
		}

	m_pTextCtrl->SetFont(afxFont(Dialog));

	m_pForeCtrl->SetFont(afxFont(Dialog));

	m_pWordCtrl->SetFont(afxFont(Dialog));

	m_pBackCtrl->SetFont(afxFont(Dialog));

	m_pSampCtrl->SetFont(afxFont(Bolder));

	LoadLists();

	AddControl(m_pTextCtrl);

	AddControl(m_pForeCtrl);

	AddControl(m_pWordCtrl);

	AddControl(m_pBackCtrl);

	AddControl(m_pSampCtrl);
	}

void CUIColorPair::OnShow(BOOL fShow)
{
	UINT uCount = m_List.GetCount();

	UINT uCode  = fShow ? SW_SHOW : SW_HIDE;

	for( UINT n = 0; n < uCount; n++ ) {

		CCtrlWnd *pCtrl = m_List[n];

		if( m_fLockBack ) {

			if( pCtrl == m_pBackCtrl || pCtrl == m_pWordCtrl ) {

				pCtrl->ShowWindow(SW_HIDE);

				continue;
				}
			}

		pCtrl->ShowWindow(uCode);
		}
	}

void CUIColorPair::OnMove(void)
{
	m_pTextCtrl->MoveWindow(m_pTextLayout->GetRect(), TRUE);

	m_pForeCtrl->MoveWindow(FindForeRect(), TRUE);

	m_pWordCtrl->MoveWindow(m_pWordLayout->GetRect(), TRUE);

	m_pBackCtrl->MoveWindow(FindBackRect(), TRUE);

	m_pSampCtrl->MoveWindow(FindSampRect(), TRUE);
	}

// Data Overridables

void CUIColorPair::OnLoad(void)
{
	DWORD Pair = m_pData->ReadInteger(m_pItem);

	COLOR Fore = LOWORD(Pair);

	COLOR Back = HIWORD(Pair);

	m_pForeCtrl->SetColor(Fore);

	m_pBackCtrl->SetColor(Back);

	m_pSampCtrl->SetColor(Pair);
	}

UINT CUIColorPair::OnSave(BOOL fUI)
{
	COLOR Fore = m_pForeCtrl->GetColor();

	COLOR Back = m_pBackCtrl->GetColor();

	DWORD Pair = MAKELONG(Fore, Back);

	DWORD Prev = m_pData->ReadInteger(m_pItem);

	if( Prev != Pair ) {

		m_pData->WriteInteger(m_pItem, Pair);

		m_pSampCtrl->SetColor(Pair);

		return saveChange;
		}

	return saveSame;
	}

// Notification Handlers

BOOL CUIColorPair::OnNotify(UINT uID, UINT uCode)
{
	if( uCode == CBN_SELCHANGE ) {

		BOOL fHit = FALSE;

		if( uID == m_pForeCtrl->GetID() ) {

			fHit = TRUE;
			}

		if( uID == m_pBackCtrl->GetID() ) {

			fHit = TRUE;
			}

		if( !m_fBusy ) {

			if( fHit ) {

				switch( SaveUI(FALSE) ) {

					case saveChange:

						return TRUE;

					case saveSame:

						return FALSE;
					}

				AfxAssert(FALSE);
				}
			}
		}

	return FALSE;
	}

// Implementation

CRect CUIColorPair::FindForeRect(void)
{
	CRect Rect  = m_pForeLayout->GetRect();

	Rect.top    = Rect.top - 1;

	Rect.bottom = Rect.top + 8 * Rect.cy();

	return Rect;
	}

CRect CUIColorPair::FindBackRect(void)
{
	CRect Rect  = m_pBackLayout->GetRect();

	Rect.top    = Rect.top - 1;

	Rect.bottom = Rect.top + 8 * Rect.cy();

	return Rect;
	}

CRect CUIColorPair::FindSampRect(void)
{
	CRect Rect  = m_pSampLayout->GetRect();

	Rect.left   = Rect.left + 8;

	return Rect;
	}

void CUIColorPair::LoadLists(void)
{
	m_pForeCtrl->LoadList();

	m_pBackCtrl->LoadList();
	}

// End of File
