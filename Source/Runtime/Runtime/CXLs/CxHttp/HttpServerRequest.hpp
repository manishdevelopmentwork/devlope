
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerRequest_HPP
	
#define	INCLUDE_HttpServerRequest_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpRequest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class DLLAPI CHttpServerConnection;

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Request
//

class DLLAPI CHttpServerRequest : public CHttpRequest
{
	public:
		// Constructor
		CHttpServerRequest(void);

		// Attributes
		PCTXT   GetRequestProtocol(void) const;
		UINT    GetRequestVersion (void) const;
		PCBYTE  GetRequestBody    (void) const;
		UINT    GetRequestSize    (void) const;
		PCTXT   GetRequestHeader  (PCTXT pName, UINT uIndex) const;
		PCTXT   GetRequestHeader  (PCTXT pName) const;
		PVOID   GetAuthContext    (void) const;
		CString GetFullPath	  (void) const;
		CString GetContentType    (void) const;
		PCTXT   GetCookieHeader   (void) const;

		// Parameters
		BOOL    HasParam(PCTXT pName) const;
		CString GetParamString(PCTXT pName, CString Default) const;
		UINT    GetParamDecimal(PCTXT pName, UINT uDefault) const;
		UINT    GetParamHex(PCTXT pName, UINT uDefault) const;

		// Operations
		void SetRequestInfo (CString Line);
		void SetReplyBody   (PCBYTE pData, UINT uData);
		void SetReplyBody   (CBytes  Body);
		void SetReplyBody   (CString Body);
		void SetContentType (CString Type);
		void AddReplyHeader (CString Name, CString Value);
		void SetCookieHeader(CString Value);
		void SetAuthContext (PVOID   pAuth);

		// Cache Helpers
		void AddCacheInfo(time_t timeCreated, time_t timeExpires, CString Tag);
		BOOL IsUnchanged (time_t timeModified, CString Tag);

	protected:
		// Typedefs
		typedef CMap <CString, CString> CParams;

		// Data Members
		CString m_Prot;
		UINT    m_uVer;
		PVOID   m_pAuth;
		CString m_Type;
		CString m_Full;
		CParams m_Params;
		CString m_Cookie;

		// Implementation
		BOOL ParseParams(void);
	};

// End of File

#endif
