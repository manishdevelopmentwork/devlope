
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Combo Box Control
//

// Dynamic Class

AfxImplementDynamicClass(CComboBox, CCtrlWnd);

// Constructor

CComboBox::CComboBox(void)
{
	}

// Attributes

UINT CComboBox::GetCount(void) const
{
	return UINT(SendMessageConst(CB_GETCOUNT));
	}

UINT CComboBox::GetCurSel(void) const
{
	return UINT(SendMessageConst(CB_GETCURSEL));	
	}

DWORD CComboBox::GetCurSelData(void) const
{
	UINT uIndex = int(SendMessageConst(CB_GETCURSEL));
	
	if( uIndex == CB_ERR ) return DWORD(-1L);
		
	return SendMessageConst(CB_GETITEMDATA, WPARAM(uIndex));
	}

CRect CComboBox::GetDroppedControlRect(void) const
{
	CRect Result;

	SendMessageConst(CB_GETDROPPEDCONTROLRECT, 0, LPARAM(PRECT(&Result)));
	
	return Result;
	}

void CComboBox::GetDroppedControlRect(CRect &Rect) const
{
	SendMessageConst(CB_GETDROPPEDCONTROLRECT, 0, LPARAM(PRECT(&Rect)));
	}

BOOL CComboBox::GetDroppedState(void) const
{
	return BOOL(SendMessageConst(CB_GETDROPPEDSTATE));
	}

int CComboBox::GetDroppedWidth(void) const
{
	return int(SendMessageConst(CB_GETDROPPEDWIDTH));
	}

CRange CComboBox::GetEditSel(void) const
{
	INT nFrom = 0;

	INT nTo   = 0;

	SendMessageConst(CB_GETEDITSEL, WPARAM(&nFrom), LPARAM(&nTo));

	return CRange(nFrom, nTo);
	}

BOOL CComboBox::GetExtendedUI(void) const
{
	return BOOL(SendMessageConst(CB_GETEXTENDEDUI));
	}

int CComboBox::GetHorizontalExtent(void) const
{
	return int(SendMessageConst(CB_GETHORIZONTALEXTENT));
	}

DWORD CComboBox::GetItemData(UINT uIndex) const
{
	return SendMessageConst(CB_GETITEMDATA, WPARAM(uIndex));	
	}

int CComboBox::GetItemHeight(UINT uIndex) const
{
	return int(SendMessageConst(CB_GETITEMHEIGHT, WPARAM(uIndex)));
	}

CString CComboBox::GetLBText(UINT uIndex) const
{
	CString Text(' ', GetLBTextLen(uIndex));
	
	SendMessageConst(CB_GETLBTEXT, WPARAM(uIndex), LPARAM(PTXT(PCTXT(Text))));
	
	return Text;
	}

UINT CComboBox::GetLBTextLen(UINT uIndex) const
{
	return UINT(SendMessageConst(CB_GETLBTEXTLEN, WPARAM(uIndex)));	
	}

UINT CComboBox::GetTopIndex(void) const
{
	return UINT(SendMessageConst(CB_GETTOPINDEX));
	}

// General Operations

UINT CComboBox::AddString(PCTXT pString)
{
	return UINT(SendMessage(CB_ADDSTRING, 0, LPARAM(pString)));
	}

UINT CComboBox::AddString(PCSTR pString)
{
	return AddString(CString(pString));
	}

UINT CComboBox::AddString(PCTXT pString, DWORD dwData)
{
	UINT uIndex = UINT(SendMessage(CB_ADDSTRING, 0, LPARAM(pString)));
	
	SendMessage(CB_SETITEMDATA, WPARAM(uIndex), LPARAM(dwData));
	
	return uIndex;
	}

UINT CComboBox::AddString(PCSTR pString, DWORD dwData)
{
	return AddString(CString(pString), dwData);
	}

void CComboBox::AddStrings(CStringArray const &Array)
{
	SetRedraw(FALSE);

	for( UINT n = 0; n < Array.GetCount(); n++ ) {

		CString const &Text = Array[n];

		AddString(Text);
		}

	SetRedraw(TRUE);

	Invalidate(FALSE);
	}

void CComboBox::DeleteString(UINT uIndex)
{
	SendMessage(CB_DELETESTRING, WPARAM(uIndex));
	}

UINT CComboBox::Dir(UINT uAttr, PCTXT pFileSpec)
{
	return UINT(SendMessage(CB_DIR, WPARAM(uAttr), LPARAM(pFileSpec)));
	}

UINT CComboBox::FindString(UINT uStart, PCTXT pString)
{
	return UINT(SendMessage(CB_FINDSTRING, WPARAM(uStart), LPARAM(pString)));
	}

UINT CComboBox::FindString(PCTXT pString)
{
	return UINT(SendMessage(CB_FINDSTRING, WPARAM(NOTHING), LPARAM(pString)));
	}

UINT CComboBox::FindStringExact(UINT uStart, PCTXT pString)
{
	return UINT(SendMessage(CB_FINDSTRINGEXACT, WPARAM(uStart), LPARAM(pString)));
	}

UINT CComboBox::FindStringExact(PCTXT pString)
{
	return UINT(SendMessage(CB_FINDSTRINGEXACT, WPARAM(NOTHING), LPARAM(pString)));
	}

void CComboBox::InitStorage(UINT uCount, DWORD dwMemory)
{
	SendMessage(LB_INITSTORAGE, WPARAM(uCount), LPARAM(dwMemory));
	}
	
UINT CComboBox::InsertString(UINT uIndex, PCTXT pString)
{
	return UINT(SendMessage(CB_INSERTSTRING, WPARAM(uIndex), LPARAM(pString)));
	}

UINT CComboBox::InsertString(UINT uIndex, PCTXT pString, DWORD dwData)
{
	UINT uInsert = UINT(SendMessage(CB_INSERTSTRING, WPARAM(uIndex), LPARAM(pString)));
	
	SendMessage(CB_SETITEMDATA, WPARAM(uInsert), LPARAM(dwData));
	
	return uInsert;
	}

void CComboBox::InsertStrings(UINT uIndex, CStringArray const &Array)
{
	SetRedraw(FALSE);

	for( UINT n = 0; n < Array.GetCount(); n++ ) {

		CString const &Text = Array[n];

		InsertString(uIndex, Text);
		}

	SetRedraw(TRUE);

	Invalidate(FALSE);
	}

void CComboBox::LimitText(UINT uLimit)
{
	SendMessage(CB_LIMITTEXT, WPARAM(uLimit));
	}

void CComboBox::ResetContent(void)
{
	SendMessage(CB_RESETCONTENT);	
	}

void CComboBox::SelectString(PCTXT pString)
{
	SelectString(NOTHING, pString);
	}

void CComboBox::SelectString(UINT uIndex, PCTXT pString)
{
	SendMessage(CB_SELECTSTRING, WPARAM(uIndex), LPARAM(pString));
	}

void CComboBox::SelectStringExact(PCTXT pString)
{
	SelectStringExact(NOTHING, pString);
	}

void CComboBox::SelectStringExact(UINT uIndex, PCTXT pString)
{
	SetCurSel(FindStringExact(uIndex, pString));
	}

void CComboBox::SetCurSel(UINT uIndex)
{
	SendMessage(CB_SETCURSEL, WPARAM(uIndex));
	}

BOOL CComboBox::SelectData(DWORD dwData)
{
	UINT uCount = UINT(SendMessageConst(CB_GETCOUNT));
	
	for( UINT uIndex = 0; uIndex < uCount; uIndex++ ) {
	
		DWORD dwTest = SendMessageConst(CB_GETITEMDATA, WPARAM(uIndex));
		
		if( dwTest == dwData ) {
		
			SendMessage(CB_SETCURSEL, WPARAM(uIndex));
			
			return TRUE;
			}
		}
	
	return FALSE;
	}

void CComboBox::SetDroppedWidth(int nWidth)
{
	SendMessage(CB_SETDROPPEDWIDTH, WPARAM(nWidth));
	}

void CComboBox::SetEditSel(CRange const &Range)
{
	SendMessage(CB_SETEDITSEL, 0, LPARAM(DWORD(Range)));
	}

void CComboBox::SetExtendedUI(BOOL fSetting)
{
	SendMessage(CB_SETEXTENDEDUI, WPARAM(fSetting));
	}

void CComboBox::SetHorizontalExtent(int nExtent)
{
	SendMessage(CB_SETHORIZONTALEXTENT, WPARAM(nExtent));
	}

void CComboBox::SetItemData(UINT uIndex, DWORD dwData)
{
	SendMessage(CB_SETITEMDATA, WPARAM(uIndex), LPARAM(dwData));
	}

void CComboBox::SetItemHeight(UINT uIndex, int nHeight)
{
	SendMessage(CB_SETITEMHEIGHT, WPARAM(uIndex), WPARAM(nHeight));	
	}

void CComboBox::SetTopIndex(UINT uIndex)
{
	SendMessage(CB_SETTOPINDEX, WPARAM(uIndex));
	}

void CComboBox::ShowDropDown(BOOL fShow)
{
	SendMessage(CB_SHOWDROPDOWN, WPARAM(fShow));
	}

// Clipboard Functions

void CComboBox::Clear(void)
{
	SendMessage(WM_CLEAR);
	}

void CComboBox::Copy(void)
{
	SendMessage(WM_COPY);
	}

void CComboBox::Cut(void)
{
	SendMessage(WM_CUT);
	}

void CComboBox::Paste(void)
{
	SendMessage(WM_PASTE);
	}

// Handle Lookup

CComboBox & CComboBox::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CComboBox NullObject;

		return NullObject;
		}

	CLASS Class = AfxStaticClassInfo();

	return (CComboBox &) CWnd::FromHandle(hWnd, Class);
	}

// Default Class Name

PCTXT CComboBox::GetDefaultClassName(void) const
{
	return L"COMBOBOX";
	}

// Message Map

AfxMessageMap(CComboBox, CCtrlWnd)
{
	AfxDispatchMessage(WM_MOUSEWHEEL)

	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxMessageEnd(CComboBox)
	};

// Message Handlers

void CComboBox::OnMouseWheel(UINT uFlags, short nDelta, CPoint Pos)
{
	if( ForwardMouseWheel(Pos) ) {

		return;
		}

	AfxCallDefProc();
	}

// Command Handlers

BOOL CComboBox::OnEditControl(UINT uID, CCmdSource &Src)
{
	if( (GetWindowLong(GWL_STYLE) & 15) != CBS_DROPDOWNLIST ) {

		switch( uID ) {

			case IDM_EDIT_CUT:
			case IDM_EDIT_COPY:
			case IDM_EDIT_DELETE:
				Src.EnableItem(!GetEditSel().IsEmpty());
				break;

			case IDM_EDIT_PASTE:
				Src.EnableItem(CanPasteText());
				break;

			case IDM_EDIT_SELECT_ALL:
				Src.EnableItem(CanSelectAll());
				break;

			default:
				return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CComboBox::OnEditCommand(UINT uID)
{
	if( (GetWindowLong(GWL_STYLE) & 15) != CBS_DROPDOWNLIST ) {

		switch( uID ) {

			case IDM_EDIT_CUT:
				Cut();
				break;

			case IDM_EDIT_COPY:
				Copy();
				break;

			case IDM_EDIT_PASTE:
				Paste();
				break;

			case IDM_EDIT_DELETE:
				Clear();
				break;

			case IDM_EDIT_SELECT_ALL:
				SetEditSel(CRange(TRUE));
				break;

			default:
				return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CComboBox::CanPasteText(void) const
{
	CClipboard Clip(ThisObject);

	if( Clip.IsFormatAvailable(CF_UNICODETEXT) ) {

		return TRUE;
		}

	if( Clip.IsFormatAvailable(CF_TEXT) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CComboBox::CanSelectAll(void) const
{
	UINT   uSize     = GetWindowTextLength();

	CRange Selection = GetEditSel();

	return uSize && Selection.GetCount() < uSize;
	}

// End of File
