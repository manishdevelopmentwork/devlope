
#include "intern.hpp"

#include "dupmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();


//////////////////////////////////////////////////////////////////////////
//
// Dupline Master Generator MODBUS Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CDuplineModbusDriverOptions, CUIItem);

// Constructor

CDuplineModbusDriverOptions::CDuplineModbusDriverOptions(void)
{
	m_Timeout  = 2000;

	}

// Download Support

BOOL CDuplineModbusDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Timeout));

	return TRUE;
	}

// Meta Data Creation

void CDuplineModbusDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Timeout);
	}


//////////////////////////////////////////////////////////////////////////
//
// Dupline Master Generator MODBUS Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CDuplineModbusDeviceOptions, CUIItem);

// Constructor

CDuplineModbusDeviceOptions::CDuplineModbusDeviceOptions(void)
{
	m_Drop = 1;
	}

// Download Support

BOOL CDuplineModbusDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CDuplineModbusDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	
	}

//////////////////////////////////////////////////////////////////////////
//
// Dupline Master Generator MODBUS Driver
//

// Instantiator

ICommsDriver *	Create_DuplineModbusDriver(void)
{
	return New CDuplineModbusDriver;
	}

// Constructor

CDuplineModbusDriver::CDuplineModbusDriver(void)
{
	m_wID		= 0x4000;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Dupline";
	
	m_DriverName	= "Mastergenerator via Modbus";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Dupline Master";

	AddSpaces();
	}

// Binding Control

UINT CDuplineModbusDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CDuplineModbusDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CDuplineModbusDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CDuplineModbusDriverOptions);
	}

// Configuration

CLASS CDuplineModbusDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CDuplineModbusDeviceOptions);
	}

void CDuplineModbusDriver::AddSpaces(void)
{
//	AddSpace(New CSpaceDupline(1, "DigOutput",	"Output Status",		10, "A",	"P",	 1, 8,	addrBitAsBit,	addrBitAsBit));
//	AddSpace(New CSpaceDupline(2, "Digital",	"Digital Status",		10, "A",	"P",	 1, 8,	addrBitAsBit,	addrBitAsWord));
	AddSpace(New CSpaceDupline(3, "Analink",	"AnaLink Values",		10, "A",	"P",	 1, 8,	addrWordAsWord,	addrWordAsWord));
	AddSpace(New CSpaceDupline(4, "Counter",	"Counter Values",		10, "CNT0",	"CNT31", 1, 4,	addrLongAsLong,	addrLongAsLong));
	AddSpace(New CSpaceDupline(5, "MuxAnalog",	"Mux Analog Values",		16, "CD",	"OP",	 0, 15, addrWordAsWord,	addrWordAsWord)); 
	AddSpace(New CSpaceDupline(6, "MuxOutput",	"Mux Analog Output Values",	16, "CD",	"OP",	 0, 15, addrWordAsWord,	addrWordAsWord));
	AddSpace(New CSpaceDupline(7, "RstCtr",		"Reset Counter",		10, "CNT0",	"CNT31", 1, 4,	addrBitAsBit,	addrBitAsBit));
//	AddSpace(New CSpaceDupline(8, "DigitalW",	"Digital Status (WORDS)",	10, "A",	"P",	 1, 1,	addrWordAsWord, addrWordAsWord));
	
	// NOTE:  Digital of space 9 combines support of previously released spaces 2 and 8.
	AddSpace(New CSpaceDupline(9,  "Digital",	"Digital Status",		10, "A",	"P",	1, 8, addrBitAsBit,   addrBitAsWord));
	AddSpace(New CSpaceDupline(10, "DigitalIn",	"Digital Input",		10, "A",	"P",    1, 8, addrBitAsBit,   addrBitAsWord  ));
	AddSpace(New CSpaceDupline(11, "DigitalOut",	"Digital Output",		10, "A",	"P",    1, 8, addrBitAsBit,   addrBitAsWord  ));
	AddSpace(New CSpaceDupline(12, "Limit",		"Analink Limit Values",		10, "A",	"P",	1, 8, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceDupline(15, "LimitOnly",	"Analink Limit Value Only",	10, "A", 	"P",	1, 8, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceDupline(13, "Time",		"Real Time Settings",		10, "A",	"P",	1, 8, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceDupline(14, "TimeText",	"Real Time Settings Text",	10, "A",	"P",	1, 8, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceDupline(16, "ClockText",	"Internal Clock Text",		10, "A",	"A",    0, 0, addrLongAsLong, addrLongAsLong));
	}

// Address Management

BOOL CDuplineModbusDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{
	CDuplineModbusDialog Dlg(*this, Addr, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	} 

// Address Helpers

BOOL CDuplineModbusDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	if( pSpace ) {

		CString Type = StripType(pSpace, Text);

		UINT uPos = Text.Find(':');

		UINT uPrefix = 0;

		UINT uSuffix = 0;

		UINT uExtended = 0;

		if ( uPos < NOTHING ) {

			uPrefix = tatoi(Text.Left(uPos));

			Text = Text.Mid(uPos + 1);

			uPos = Text.Find(':');

			if( uPos < NOTHING ) {

				uSuffix = tatoi(Text.Left(uPos));

				uExtended = tatoi(Text.Mid(uPos + 1));
				}
			else {

				uSuffix = tatoi(Text);
				}

			Addr.a.m_Table = pSpace->m_uTable;

			Addr.a.m_Type = pSpace->TypeFromModifier(Type);

			Addr.a.m_Offset = FromDuplineToModbus(pSpace, uPrefix, uSuffix, uExtended, Addr.a.m_Type);

			Addr.a.m_Extra = 0;

			return TRUE;
	       		}

		uPos = Text.Find('>');

		if ( uPos < NOTHING ) {	

			uPrefix = GetDuplinePrefix(Text, pSpace, pSpace->TypeFromModifier(Type));

			uSuffix = GetDuplineSuffix(Text, pSpace);

			uExtended = GetDuplineExtended(Text, pSpace);

			Addr.a.m_Table = pSpace->m_uTable;

			Addr.a.m_Type =  pSpace->TypeFromModifier(Type);

			if( pSpace->m_uTable == 8 ) {

				Addr.a.m_Type = addrBitAsWord;
				}
			
			Addr.a.m_Offset = FromDuplineToModbus(pSpace, uPrefix, uSuffix, uExtended, Addr.a.m_Type);

			Addr.a.m_Extra = 0;

			return TRUE;
			}
		}

	Error.Set( CString(IDS_DRIVER_ADDR_INVALID), 0);

	return FALSE;

	}

BOOL CDuplineModbusDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpaceDupline *pSpace = (CSpaceDupline *) GetSpace(Addr);

	if( pSpace ) {

		UINT uFormat = GetFormat(pSpace);

		CString Format = ( uFormat > 1 ) ? "%s->%s-%s%s" : "%s->%s%s%s";

		CString Label = ( uFormat > 2 ) ? pSpace->m_Minimum.Left(uFormat) : "";

		UINT uSuffix = Addr.a.m_Offset;

		UINT uOffset = uSuffix;

		UINT uExtended = GetDuplineExtended(uSuffix, (CSpace *)pSpace);

		BOOL fModbus = IsModbus(uOffset);

		uOffset = fModbus ? FromModbusToDupline(pSpace, uOffset, 0, uExtended, Addr.a.m_Type) : uOffset;

		CString Prefix = Label + GetDuplinePrefixText(uFormat, uOffset, (CSpace *)pSpace, Addr.a.m_Type);

		CString Suffix = GetDuplineSuffixText(uFormat, GetDuplineSuffix(uSuffix, (CSpace *)pSpace, Addr.a.m_Type), (CSpace *)pSpace);

		CString Extended = GetDuplineExtendedText((CSpace *)pSpace, uExtended, !fModbus);

		if ( !fModbus ) {

			if ( uOffset == pSpace->m_uMinimum ) {

				Prefix = pSpace->m_Minimum;

				Suffix.Printf("%X", pSpace->m_uMinimum);

				}

			else if ( uOffset == pSpace->m_uMaximum ) {

				Prefix = pSpace->m_Maximum;

				Suffix.Printf("%X", pSpace->m_uMaximum);

				}
			}

		if( HasExtended(pSpace->m_uTable) ) {

			Text.Printf("%s->%s%s\\%s", pSpace->m_Prefix, Prefix, Suffix, Extended);
			
			return TRUE;
			}

		CString Type = "";

		if( Addr.a.m_Type != pSpace->m_uType ) {

			Type = "." + pSpace->GetTypeModifier(Addr.a.m_Type);
			}

		Text.Printf(Format, pSpace->m_Prefix, Prefix, Suffix, Type);
				
		return TRUE;
		}

	return FALSE;
	}

UINT	CDuplineModbusDriver::GetFormat(CSpaceDupline* pSpace)
{
	UINT uFormat = 0;

	if( pSpace ) {

		uFormat = pSpace->m_Minimum.GetLength();

		if ( uFormat > 2 ) {

			uFormat = 3;
			}
		}
	
	return uFormat;

	}

UINT CDuplineModbusDriver::GetDuplinePrefix(CString Text, CSpace* pSpace, UINT uType)
{
	UINT uPrefix = Text.Find('>');

	if( uPrefix < NOTHING && pSpace ) {

		Text = Text.Mid(uPrefix + 1);

		CString Sub; 

		TCHAR   Char;

		UINT    uDiv = uType == addrBitAsWord ? 2 : 1;

		switch( pSpace->m_uTable ) {

			case 1:
			case 2:
			case 3:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
									
				Sub = Text.Mid(0, 1);

				Char = Sub.GetAt(0);  

				return UINT(Char - 'A') / uDiv;
			
			case 4:
			case 7:
			       	if ( isdigit(Text.GetAt(4)) ) {

					Sub = Text.Mid(3, 2);
					}
				else {
					Sub = Text.Mid(3, 1);
					}
			       				
				return 	UINT(tatoi(Sub));

			case 5:
			case 6:
				Sub = Text.Mid(0, 1);

				Char = Sub.GetAt(0);

				return UINT(Char - 'A') / 2 - 1;

			}
		}

	return 0;
	}

UINT CDuplineModbusDriver::GetDuplineSuffix(CString Text, CSpace* pSpace)
{
	UINT uMark = Text.Find('>');

	Text = Text.Mid(uMark + 1);
	
	UINT uSuffix = Text.Find('-');

	if( pSpace ) {

		CString Sub;

		if( uSuffix >= NOTHING) {

			Sub = Text.Mid(1, 1);

			return UINT(tatoi(Sub) - 1);
			}

		else {
			Sub = Text.Mid(uSuffix + 1, 1);

			UINT uChar;

			switch( pSpace->m_uTable ) {

				case 5:
				case 6:
					uChar = Sub.GetAt(0) - '0';

					if ( uChar > 9 ) {

						uChar = uChar - 7;
						}

					return uChar;

				default:
					return UINT(tatoi(Sub) - 1);
				}
			}
		}
		
	return 0;
	}

UINT CDuplineModbusDriver::GetDuplineExtended(UINT &uOffset, CSpace* pSpace)
{
	UINT uExtended = 0;

	if( HasExtended(pSpace->m_uTable) ) {

		UINT uElements = GetExtendedElements(pSpace->m_uTable);

		uOffset &= 0x7FFF;

		uExtended = uOffset % uElements;

		uOffset -= uExtended;

		uOffset /= uElements;
		}

	return uExtended;
	}

UINT CDuplineModbusDriver::GetDuplineExtended(CString pText, CSpace* pSpace)
{
	UINT uMark = pText.Find('-');

	pText = pText.Mid(uMark + 1);

	uMark = pText.Find('\\');

	pText = pText.Mid(uMark + 1).ToUpper();

	UINT uCount = GetExtendedElements(pSpace->m_uTable);

	for( UINT u = 0; u < uCount; u++ ) {

		if( pText == GetDuplineExtendedText(pSpace, u, FALSE).ToUpper() ) {

			return u;
			}
		}
		
	return 0;
	}


UINT    CDuplineModbusDriver::GetDuplinePrefix(UINT uOffset, CSpace* pSpace)
{
	UINT uPrefix = 0;

	if( pSpace  ) {

		switch( pSpace->m_uTable ) {

			case 1:
			case 2:
			case 3:
			case 4:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				uPrefix = uOffset / pSpace->m_uMaximum;
				break;
			
			case 5:
			case 6:
				uPrefix = uOffset / (pSpace->m_uMaximum + 1);
				break;

			}
		}

	return uPrefix;
	}

UINT    CDuplineModbusDriver::GetDuplineSuffix(UINT uOffset, CSpace* pSpace, UINT uType)
{
	UINT uSuffix = 0;

	if( pSpace  && uType != addrBitAsByte ) {

		switch( pSpace->m_uTable ) {

			case 1:
			case 2:
			case 3:
			case 4:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:
			case 12:
			case 13:
			case 14:
			case 15:
				uSuffix = uOffset % pSpace->m_uMaximum;
				break;
		
			case 5:
			case 6:
				uSuffix = uOffset % (pSpace->m_uMaximum + 1);
				break;
			}
		}

	return uSuffix;
	}

CString CDuplineModbusDriver::GetDuplinePrefixText(UINT uFormat, UINT uIndex, CSpace* pSpace, UINT uType)
{
	CString Text = "ABCDEFGHIJKLMNOP";

	UINT uPos = uIndex;
	
	if( uType == addrBitAsWord ) {

		uPos  = uIndex / pSpace->m_uMaximum;

		}

	if( pSpace->m_uTable == 16 ) {

		return "";
		}

	switch ( uFormat ) {

		case 1:
			Text.Delete(0, uPos);

			return Text.Left(uFormat);

		case 2:
			Text.Delete(0, 2 + (uIndex) * uFormat);
			
			return Text.Left(uFormat);

		case 3:
			return pSpace->GetValueAsText(uIndex , 2);
		}

	return "";
	}


CString CDuplineModbusDriver::GetDuplineSuffixText(UINT uFormat, UINT uIndex, CSpace* pSpace)
{
	switch ( uFormat ) {

		case 1:
		case 3:
			uIndex++;
			break;
		}

	return pSpace->GetValueAsText(uIndex);
	}

CString CDuplineModbusDriver::GetDuplineExtendedText(CSpace* pSpace, UINT uOffset, BOOL fDefault)
{
	UINT uTable = pSpace->m_uTable;

	UINT uExtended = 0;

	switch( uTable ) {

		case 12:
		case 15:
			if ( fDefault ) {

				return ( uOffset ? "1L" : "2H");
				}

			uExtended = uOffset % 2;

			if( uExtended ) {

				return CPrintf("%uH", uOffset / 2 + 1);
				}
			
			return CPrintf("%uL", uOffset / 2 + 1);

		case 13:
		case 14:

			if ( fDefault ) {

				return (uOffset % 2) ? "On1" : "Day4";
				}
			
			uExtended = uOffset % 3;
		       
			switch( uExtended ) {

				case 1:
					return CPrintf("Off%u", (uOffset / 3 + 1));

				case 2:
					return CPrintf("Day%u", (uOffset / 3 + 1));

				default:
					return CPrintf("On%u", (uOffset / 3 + 1));
				}
		}

	return "";
	}

UINT CDuplineModbusDriver::FromDuplineToModbus(CSpace* pSpace, UINT uOffset, UINT uExtra, UINT uExtended, UINT uType)
{
	if ( pSpace ) {

		UINT uElements = GetExtendedElements(pSpace->m_uTable);

		switch ( pSpace->m_uTable ) {

			case 1:
				return (READ_OFFSET_OUTPUT + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

			case 2:
				return (READ_OFFSET_INPUT + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

			case 3:
				return (READ_OFFSET_ANALINK + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

			case 4:
				return (READ_OFFSET_COUNTER + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

			case 5:
				return (READ_OFFSET_MUX_IN + uOffset * (pSpace->m_uMaximum + 1) + uExtra) | MARK_MODBUS;

			case 6:
				return (READ_OFFSET_MUX_OUT + uOffset * (pSpace->m_uMaximum + 1) + uExtra) | MARK_MODBUS;

			case 7:
				return (READ_OFFSET_RESET_CTR + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

			case 8:
				return (READ_OFFSET_OUTPUT_W + uOffset) | MARK_MODBUS;

			case 9:
				switch( uType ) {

					case addrBitAsBit:
						return (READ_OFFSET_INPUT + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

					case addrBitAsByte:
						return (READ_OFFSET_OUTPUT_W + uOffset * 8) | MARK_MODBUS;

					case addrBitAsWord:
						return (READ_OFFSET_OUTPUT_W + uOffset * 16) | MARK_MODBUS;
					}
					
				break;

			case 10:
				switch( uType ) {

					case addrBitAsBit:
						return (READ_OFFSET_INPUT + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

					case addrBitAsByte:
						return (READ_OFFSET_DIGIN + uOffset * 8) | MARK_MODBUS;

					case addrBitAsWord:
						return (READ_OFFSET_DIGIN + uOffset * 16) | MARK_MODBUS;
					}
					
				break;

			case 11:
				switch( uType ) {

					case addrBitAsBit:
						return (READ_OFFSET_OUTPUT + uOffset * pSpace->m_uMaximum + uExtra) | MARK_MODBUS;

					case addrBitAsByte:
						return (READ_OFFSET_OUTPUT_W + uOffset * 8) | MARK_MODBUS;

					case addrBitAsWord:
						return (READ_OFFSET_OUTPUT_W + uOffset * 16) | MARK_MODBUS;
					}
					
				break;


			case 12:
			case 15:
				return ((READ_OFFSET_ANALIMIT + uOffset * pSpace->m_uMaximum * uElements) + uExtra * uElements + uExtended) | MARK_MODBUS;
				
			case 13:
			case 14:
				return ((READ_OFFSET_TIMESETS + uOffset * pSpace->m_uMaximum * uElements) + uExtra * uElements + uExtended) | MARK_MODBUS;
			
			case 16:
				return (READ_OFFSET_CLOCK + uOffset) | MARK_MODBUS; 
			}
		}
	
	return 0;
	}

UINT CDuplineModbusDriver::FromModbusToDupline(CSpace* pSpace, UINT uOffset, UINT uExtra, UINT uExtended, UINT uType)
{
	if ( pSpace ) {

		uOffset = uOffset & 0x7FFF;

		switch ( pSpace->m_uTable ) {

			case 1:
				return ((uOffset - READ_OFFSET_OUTPUT - uExtra) / pSpace->m_uMaximum);

			case 2:
				return ((uOffset - READ_OFFSET_INPUT - uExtra) / pSpace->m_uMaximum);

			case 3:
				return ((uOffset - READ_OFFSET_ANALINK - uExtra) / pSpace->m_uMaximum);

			case 4:
				return ((uOffset - READ_OFFSET_COUNTER - uExtra) / pSpace->m_uMaximum);

			case 5:
				return ((uOffset - READ_OFFSET_MUX_IN - uExtra) / (pSpace->m_uMaximum + 1));

			case 6:
				return ((uOffset - READ_OFFSET_MUX_OUT - uExtra) / (pSpace->m_uMaximum + 1));

			case 7:
				return ((uOffset - READ_OFFSET_RESET_CTR - uExtra) / pSpace->m_uMaximum);

			case 8:
				return (uOffset - READ_OFFSET_OUTPUT_W);

			case 9:
				switch( uType ) {

					case addrBitAsBit:
						return ((uOffset - READ_OFFSET_INPUT - uExtra) / pSpace->m_uMaximum);

					case addrBitAsByte:
						return (uOffset - READ_OFFSET_OUTPUT_W);

					case addrBitAsWord:
						return (uOffset - READ_OFFSET_OUTPUT_W);
					}
				break;

			case 10:
				switch( uType ) {

					case addrBitAsBit:
						return ((uOffset - READ_OFFSET_INPUT - uExtra) / pSpace->m_uMaximum);

					case addrBitAsByte:
						return (uOffset - READ_OFFSET_DIGIN);

					case addrBitAsWord:
						return (uOffset - READ_OFFSET_DIGIN);
					}
				break;

			case 11:
				switch( uType ) {

					case addrBitAsBit:
						return ((uOffset - READ_OFFSET_OUTPUT - uExtra) / pSpace->m_uMaximum);

					case addrBitAsByte:
						return (uOffset - READ_OFFSET_OUTPUT_W);

					case addrBitAsWord:
						return (uOffset - READ_OFFSET_OUTPUT_W);
					}
				break;

			case 12:
			case 15:
				return ((uOffset - READ_OFFSET_ANALIMIT - uExtra) / pSpace->m_uMaximum / GetExtendedElements(pSpace->m_uTable));


			case 13:
			case 14:
				return ((uOffset - READ_OFFSET_TIMESETS - uExtra) / pSpace->m_uMaximum / GetExtendedElements(pSpace->m_uTable));
			
			case 16:
				return (uOffset - READ_OFFSET_CLOCK);
			} 
		}

	return 0;
	}

BOOL	CDuplineModbusDriver::IsModbus(UINT uOffset)
{
	return (uOffset & MARK_MODBUS);
	}

BOOL	CDuplineModbusDriver::HasExtended(UINT uTable)
{
	switch( uTable ) {

		case 12:
		case 13:
		case 14:
		case 15:

			return TRUE;
		}

	return FALSE;
	}

UINT   CDuplineModbusDriver::GetExtendedElements(UINT uTable)
{
	switch( uTable) {

		case 12:
		case 15:
			return 4;

		case 13:
		case 14:
			return 12;
		}

	return 1;
	
	} 

//////////////////////////////////////////////////////////////////////////
//
// Dupline Modbus Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CDuplineModbusDialog, CStdAddrDialog);
		
// Constructor

CDuplineModbusDialog::CDuplineModbusDialog(CDuplineModbusDriver &Driver, CAddress &Addr, BOOL fPart) : CStdAddrDialog(Driver, Addr, NULL, fPart)
{
	m_Element = "DuplineElementDlg";
	}

// Message Map

AfxMessageMap(CDuplineModbusDialog, CStdAddrDialog)
{
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChange )

	AfxMessageEnd(CDuplineModbusDialog)
	};

// Message Handlers

void CDuplineModbusDialog::OnTypeChange(UINT uID, CWnd &Wnd)
{
	if( m_pSpace ) {

		LoadAddressPrefixList(0);

		LoadAddressSuffixList(0);

		CStdAddrDialog::OnTypeChange(uID, Wnd);
		}
	}
	
 
// Overridables

BOOL CDuplineModbusDialog::AllowType(UINT uType)
{
	switch( uType ) {

		case addrBitAsByte:
				
			return FALSE;
		}
	
	return TRUE;
	}


void CDuplineModbusDialog::SetAddressText(CString Text)
{
	UINT uPrefix = 0;

	UINT uSuffix = 0;

	UINT uExtended = 0;

	if( !Text.IsEmpty() ) {

		if ( m_pDriver ) {

			CDuplineModbusDriver *pDriver = (CDuplineModbusDriver *) m_pDriver;

			uExtended = pDriver->GetDuplineExtended(Text, m_pSpace);

			uPrefix = pDriver->GetDuplinePrefix(Text, m_pSpace, GetTypeCode());

			uSuffix = pDriver->GetDuplineSuffix(Text, m_pSpace);
			
			}
		}
	 
	LoadAddressPrefixList(uPrefix);

	LoadAddressSuffixList(uSuffix);

	LoadAddressExtendedList(uExtended);

	CEditCtrl &Prefix = (CEditCtrl &) GetDlgItem(2001);
	
	Prefix.SetWindowSize(60,14, FALSE);

	}

CString CDuplineModbusDialog::GetAddressText(void)
{
	CComboBox &Combo1 = (CComboBox &) GetDlgItem(2003);

	CComboBox &Combo2 = (CComboBox &) GetDlgItem(2005);

	CComboBox &Combo3 = (CComboBox &) GetDlgItem(2006);

	CString Text;

	Text.Printf("%u:%u:%u", Combo1.GetCurSel(), Combo2.GetCurSel(), Combo3.GetCurSel());
	
	return Text;
	
	}

// Helpers

void CDuplineModbusDialog::LoadAddressPrefixList(UINT uOffset)
{
	ClearComboList(2003);
	
	CComboBox &Combo = (CComboBox &) GetDlgItem(2003);

	BOOL fEnable = FALSE;
	
	if ( m_pSpace && m_pDriver ) {

		CSpaceDupline *pSpace = (CSpaceDupline *) m_pSpace;
	
		CDuplineModbusDriver *pDriver = (CDuplineModbusDriver *) m_pDriver;

		UINT uFormat = pDriver->GetFormat(pSpace);

		UINT uMin = 0;

		UINT uMax = 0;

		CString Label = " ";

		if ( uFormat == 1 ) {

			uMax = UINT(pSpace->m_Maximum.GetAt(0) - 'A');

			GetDlgItem(2004).SetWindowText( " " );
			}

		else if ( uFormat == 2 ) {
	
			uMax = UINT((pSpace->m_Maximum.GetAt(0) - 'A') / 2 - 1);

			GetDlgItem(2004).SetWindowText( "-" );

			GetDlgItem(2004).ShowWindow(TRUE);
			}

		else if ( uFormat == 3 ) {

			CString Text = pSpace->m_Maximum;

			Label = Text.Left(uFormat);

			Text.Delete(0, uFormat);

			uMax = tatoi(Text);

			GetDlgItem(2004).SetWindowText( "-" );

			GetDlgItem(2004).ShowWindow(TRUE);
			}
		
		UINT uSpan = 1;

		if ( pSpace->m_uSpan != pSpace->m_uType ) {

			switch( GetTypeCode() ) {

				case addrBitAsWord:

					uSpan = 2;

					break;
				}
			}

		fEnable = uMin != uMax;
		
		for (	; uMin <= uMax; uMin += uSpan ) {

			Combo.AddString(Label + pDriver->GetDuplinePrefixText(uFormat, uMin, m_pSpace));
			}

		Combo.SetCurSel(uOffset);
	       	}

	Combo.ShowWindow(fEnable);
	}

void CDuplineModbusDialog::LoadAddressSuffixList(UINT uOffset)
{
	ClearComboList(2005);
		
	CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

	BOOL fEnable = FALSE;
	
	if ( m_pSpace && m_pDriver ) {

		CSpaceDupline *pSpace = (CSpaceDupline *) m_pSpace;

		CDuplineModbusDriver *pDriver = (CDuplineModbusDriver *) m_pDriver;

		UINT uFormat = pDriver->GetFormat(pSpace);

		UINT uMin = 0;

		UINT uMax = pSpace->m_uMaximum;

		if ( uFormat == 2 ) {

			uMax++;
			}

		if ( pSpace->m_uSpan != pSpace->m_uType ) {
		
			switch( GetTypeCode() ) {

				case addrBitAsByte:
				case addrBitAsWord:

					uMax = uMin + 1;

					break;
				}
			}

		fEnable = uMax != 0;
		
		for (	; uMin < uMax; uMin++ ) {

			Combo.AddString(pDriver->GetDuplineSuffixText(uFormat, uMin, m_pSpace));
			}

		Combo.SetCurSel(uOffset);
		}

	Combo.ShowWindow(fEnable);
	}

void CDuplineModbusDialog::ClearComboList(UINT uID)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(uID);

	UINT uCount = Combo.GetCount();

	while(uCount) {

		Combo.DeleteString(--uCount);
		}
	}

void CDuplineModbusDialog::LoadAddressExtendedList(UINT uOffset)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2006);
	
	if ( m_pSpace && m_pDriver ) {
	
		ClearComboList(2006);
	
		CSpaceDupline *pSpace = (CSpaceDupline *) m_pSpace;

		UINT u = 0;

		switch( pSpace->m_uTable ) {

			case 12:
			case 15:
				for( u = 0; u < 2; u++ ) {
				
					Combo.AddString(CPrintf(CString(IDS_DUP_LIMLO), u + 1));

					Combo.AddString(CPrintf(CString(IDS_DUP_LIMHI), u + 1));
					}

				Combo.ShowWindow(TRUE);

				Combo.SetCurSel(uOffset);
			
				return;
			
			case 13:
			case 14:

				for( u = 0; u < 4; u++ ) {
				
					Combo.AddString(CPrintf(CString(IDS_DUP_TIMEON), u + 1));

					Combo.AddString(CPrintf(CString(IDS_DUP_TIMEOFF), u + 1));

					Combo.AddString(CPrintf(CString(IDS_DUP_TIMEDAY), u + 1));
					}

				Combo.ShowWindow(TRUE);

				Combo.SetCurSel(uOffset);

				return;

			}
		}  

	Combo.SetCurSel(0);

	Combo.ShowWindow(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Dupline Space Wrapper Class
//

// Constructors

CSpaceDupline::CSpaceDupline(UINT t, CString p, CString c, UINT r, CString i, CString a, UINT n, UINT x, AddrType type, AddrType s) : CSpace(t, p, c, r, n, x, type, s)
{
	m_Minimum	= i;
	
	m_Maximum	= a;

	}

// End of File
