
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_FatDirShort_HPP

#define	INCLUDE_FatDirShort_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Directory Entry
//

#pragma pack(1)

struct FatDirShort
{
	CHAR	m_sText[11];
	BYTE	m_bAttribute;
	BYTE	m_bNtrReserved;
	BYTE	m_bCreateTimeTenth;
	WORD    m_wCreateTime;
	WORD    m_wCreateDate;
	WORD    m_wDateAccess;
	WORD	m_wFirstClusterHi;
	WORD    m_wWriteTime;
	WORD    m_wWriteDate;
	WORD	m_wFirstClusterLo;
	DWORD	m_dwSize;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// Fat Directory Entry
//

class CFatDirShort : public FatDirShort
{
	public:
		// Constructor
		CFatDirShort(void);

		// Creation
		BOOL Create(PCTXT pName);
		BOOL CreateFile(PCTXT pName, DWORD dwCluster, BYTE bFlags);
		BOOL CreateDir(PCTXT pName, DWORD dwCluster, BYTE bFlags);
		BOOL CreateDot(CFatDirShort const &Dir);
		BOOL CreateDot2(CFatDirShort const &Dir, CFatDirShort const &Parent);
		BOOL CreateDot2(CFatDirShort const &Dir, UINT uParent);

		// Conversion
		void HostToLocal(void);
		void LocalToHost(void);

		// Attributes
		BOOL   IsValid(void) const;
		BOOL   IsEmpty(void) const;
		BOOL   IsFree(void) const;
		BOOL   IsLast(void) const;
		BOOL   IsDot(void) const;
		BOOL   IsDotDot(void) const;
		BOOL   IsSpecial(void) const;
		BOOL   IsReadOnly(void) const;
		BOOL   IsHidden(void) const;
		BOOL   IsSystem(void) const;
		BOOL   IsArchive(void) const;
		BOOL   IsFile(void) const;
		BOOL   IsDirectory(void) const;
		BOOL   IsVolume(void) const;
		BOOL   IsShort(void) const;
		BOOL   IsLong(void) const;
		DWORD  GetFirstCluster(void) const;
		time_t GetUnixTime(void) const;
		DWORD  GetPackedTime(void) const;

		// Operations
		void MakeEmpty(void);
		void MakeFree(void);
		void MakeFreeLast(void);
		void MakeReadOnly(void);
		void MakeHidden(void);
		bool MakeArchive(void);
		void MakeDirectory(void);
		bool ClearArchive(void);
		bool UpdateTimeStamps(void);
		bool UpdateAccessTime(void);
		void SetFirstCluster(DWORD dwCluster);
		void SetUnixTime(time_t time);
		void SetPackedTime(DWORD dwPacked);
		UINT GetFullName(PTXT pText) const;
		UINT GetBareName(PTXT pText) const;
		UINT GetExtension(PTXT pText) const;
		BOOL SetFullName(PCTXT pName);
		BOOL SetBareName(PCTXT pName);
		BOOL SetExtension(PCTXT pName);
		BYTE GetChecksum(void) const;
		BOOL CheckName(PCTXT pName) const;

		// Dump
		void Dump(void) const;

	protected:
		// Constants
		enum
		{
			constFree = 0xE5,
			constLast = 0x00,
			};

		// Attributes
		enum
		{
			attrReadOnly	= Bit(0),
			attrHidden	= Bit(1),
			attrSystem	= Bit(2),
			attrVolume	= Bit(3),
			attrDir		= Bit(4),
			attrArchive	= Bit(5),
			attrLong	= attrReadOnly | attrHidden | attrSystem | attrVolume,
			attrLongMask	= attrReadOnly | attrHidden | attrSystem | attrVolume | attrDir | attrArchive,
			};

		// Implementation
		void MakeDot(void);
		void MakeDotDot(void);
		UINT GetFullNameLen(void) const;
		UINT GetBareNameLen(void) const;
		UINT GetExtensionLen(void) const;
		BOOL SetBareName(PCTXT pName, UINT uLen);
		BOOL SetExtension(PCTXT pName, UINT uLen);
		BOOL CheckName(void) const;
		BOOL CheckName(CHAR c) const;
		void CheckGuard(PTXT pName) const;
		void RemoveGuard(PTXT pName) const;
		void MakeUpper(void);
		void MakeUpper(UINT iStart, UINT uCount);
	};

// End of File

#endif

