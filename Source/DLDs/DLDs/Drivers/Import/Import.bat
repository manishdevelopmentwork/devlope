@echo off

if #%1# == ## goto done

set proj=%1

set targ=j:\tfs\c3\dev\dlds\unified\source\drivers\%proj%

if exist j:\tfs\c3\dev\dlds\m68k\source\%proj%\*.cpp goto okay1

echo %proj%: No such project!

goto done

:okay1

rmdir /s /q %targ%

mkdir %targ%

xcopy /s j:\tfs\c3\dev\dlds\m68k\source\%proj%\*.?pp %targ% >nul

erase %targ%\intern.hpp

set main=

set id=

for /f "delims=" %%i in ('grep -l "m_Ident[^=]*=" %targ%\*.cpp') do set main=%%i

if #%main%# == ## goto none

type %main% | grep "m_Ident[^=]*=[^0]*0x" | sed "s/.*m_Ident[^0]*0x//" | sed s/;.*// > temp\id.txt

for /f "delims=" %%i in ('type temp\id.txt') do set id=%%i

if not #%id%# == ## goto okay2

:none

echo %proj%: Can't find DLD identifier!

goto done

:okay2

copy base\base.vcxproj         %targ%\%proj%.vcxproj	     >nul

copy base\base.vcxproj.filters %targ%\%proj%.vcxproj.filters >nul

copy base\base.props           %targ%\Project.props	     >nul

for /f "delims=" %%i in ('uuidgen')          do set GUID1=%%i

for /f "delims=" %%i in ('uuidgen')          do set GUID2=%%i

for /f "delims=" %%i in ('uuidgen')          do set GUID3=%%i

for /f "delims=" %%i in ('uuidgen')          do set GUID4=%%i

for /f "delims=" %%i in ('uuidgen')          do set GUID5=%%i

dir %targ%\*.cpp   /b | sed "s/^.*$/<ClCompile Include=\x22&\x22 \/>/" > temp\proj_src.txt

dir %targ%\*.hpp   /b | sed "s/^.*$/<ClInclude Include=\x22&\x22 \/>/" > temp\proj_inc.txt

dir %targ%\*.cpp   /b | sed "s/^.*$/<ClCompile Include=\x22&\x22><Filter>Source<\/Filter><\/ClCompile>/"  > temp\filt_src.txt

dir %targ%\*.hpp   /b | sed "s/^.*$/<ClInclude Include=\x22&\x22><Filter>Headers<\/Filter><\/ClInclude>/" > temp\filt_inc.txt

dir %targ%\*.props /b | sed "s/^.*$/<None Include=\x22&\x22><Filter>Build<\/Filter><\/None>/"             > temp\filt_top.txt

c:\temp\rep32 __SRC__   -s temp\proj_src.txt %targ%\%proj%.vcxproj

c:\temp\rep32 __INC__   -s temp\proj_inc.txt %targ%\%proj%.vcxproj

c:\temp\rep32 __GUID__  %GUID1%              %targ%\%proj%.vcxproj

c:\temp\rep32 __PROPS__ Project.props        %targ%\%proj%.vcxproj

c:\temp\rep32 __SRC__   -s temp\filt_src.txt %targ%\%proj%.vcxproj.filters

c:\temp\rep32 __INC__   -s temp\filt_inc.txt %targ%\%proj%.vcxproj.filters

c:\temp\rep32 __TOP__   -s temp\filt_top.txt %targ%\%proj%.vcxproj.filters

c:\temp\rep32 __GUID1__ %GUID2%              %targ%\%proj%.vcxproj.filters

c:\temp\rep32 __GUID2__ %GUID3%              %targ%\%proj%.vcxproj.filters

c:\temp\rep32 __GUID3__ %GUID4%              %targ%\%proj%.vcxproj.filters

c:\temp\rep32 __ID__    %id%                 %targ%\Project.props

type %main%	    | sed "s/0x%id%/DRIVER_ID/"            > temp\main1.cpp

type temp\main1.cpp | sed "s/clink void Create.*/START()/" > temp\main2.cpp

copy temp\main2.cpp %main% >nul

echo Project("{%GUID5%}") = "%proj%", "Drivers\%proj%\%proj%.vcxproj", "{%GUID1%}" >> Solution1.txt
echo   ProjectSection(ProjectDependencies) = postProject >> Solution1.txt
echo     {D86E6CE2-693E-4FE1-8FE6-0680A527C5C5} = {D86E6CE2-693E-4FE1-8FE6-0680A527C5C5} >> Solution1.txt
echo   EndProjectSection >> Solution1.txt
echo EndProject >> Solution1.txt

echo {%GUID1%} = {F01F81F4-5535-402F-A340-A4215AB9C9E5} >> Solution2.txt

:done
