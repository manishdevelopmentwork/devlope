
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Window Proxy
//

// Runtime Class

AfxImplementRuntimeClass(CSysProxy, CObject);

// Constructor

CSysProxy::CSysProxy(void)
{
	m_pView   = NULL;

	m_pSystem = NULL;

	m_uUpdate = 0;
}

// Destructor

CSysProxy::~CSysProxy(void)
{
	UnregisterForUpdates();
}

// Binding

void CSysProxy::Bind(void)
{
	m_pView   = NULL;

	m_pSystem = (CSystemWnd *) &afxMainWnd->GetDlgItem(IDVIEW);
}

void CSysProxy::Bind(CViewWnd *pView)
{
	m_pView   = pView;

	m_pSystem = (CSystemWnd *) &m_pView->GetParent(AfxRuntimeClass(CSystemWnd));
}

void CSysProxy::Bind(CSystemWnd *pSystem)
{
	m_pView   = NULL;

	m_pSystem = pSystem;
}

// Attributes

CViewWnd * CSysProxy::GetCatView(CLASS Class) const
{
	return m_pSystem->GetCatView(Class);
}

CViewWnd * CSysProxy::GetItemView(void) const
{
	return m_pSystem->GetItemView();
}

CCmd * CSysProxy::GetLastCmd(void) const
{
	return m_pSystem->GetLastCmd();
}

CString CSysProxy::GetNavPos(void) const
{
	return m_pSystem->GetNavPos();
}

BOOL CSysProxy::InTwinMode(void) const
{
	return m_pSystem->InTwinMode();
}

BOOL CSysProxy::IsSemiModal(void) const
{
	return m_pSystem->IsSemiModal();
}

CDialog * CSysProxy::GetSemiModal(void) const
{
	return m_pSystem->GetSemiModal();
}

BOOL CSysProxy::IsNavBusy(void) const
{
	return m_pSystem->IsNavBusy();
}

// Navigation

BOOL CSysProxy::Navigate(CString Nav)
{
	return m_pSystem->Navigate(Nav);
}

void CSysProxy::SetNavCheckpoint(void)
{
	m_pSystem->SetNavCheckpoint();
}

void CSysProxy::SetViewedItem(CItem *pItem)
{
	m_pSystem->SetViewedItem(pItem);
}

void CSysProxy::SetNavCategory(CItem *pItem)
{
	m_pSystem->SetNavCategory(pItem);
}

void CSysProxy::SetResCategory(CItem *pItem)
{
	m_pSystem->SetResCategory(pItem);
}

// Dialog Boxes

UINT CSysProxy::ExecSemiModal(CDialog &Dlg, CString Cat)
{
	return m_pSystem->ExecSemiModal(Dlg, Cat);
}

UINT CSysProxy::ExecSemiModal(CDialog &Dlg)
{
	return m_pSystem->ExecSemiModal(Dlg);
}

// Pane Management

void CSysProxy::SetTwinMode(BOOL fTwin)
{
	m_pSystem->SetTwinMode(fTwin);
}

void CSysProxy::LockResPane(BOOL fLock)
{
	m_pSystem->LockResPane(fLock);
}

BOOL CSysProxy::HidePaneOnDrop(UINT uPane)
{
	return m_pSystem->HidePaneOnDrop(uPane);
}

BOOL CSysProxy::CanSyncPanes(void)
{
	return m_pSystem->CanSyncPanes();
}

void CSysProxy::SyncPanes(void)
{
	m_pSystem->SyncPanes();
}

BOOL CSysProxy::FlipToItemView(BOOL fNow)
{
	if( InTwinMode() ) {

		FlipToNavPane(fNow);

		return FALSE;
	}
	else {
		CWnd &Wnd = m_pSystem->GetDlgItem(IDVIEW+2);

		if( Wnd.IsWindowVisible() ) {

			if( fNow ) {

				Wnd.SetFocus();

				return TRUE;
			}

			Wnd.PostMessage(WM_TAKEFOCUS);

			return TRUE;
		}

		return FALSE;
	}
}

BOOL CSysProxy::FlipToNavPane(BOOL fNow)
{
	CWnd &Wnd = m_pSystem->GetDlgItem(IDVIEW+0);

	if( Wnd.IsWindowVisible() ) {

		if( fNow ) {

			Wnd.SetFocus();

			return TRUE;
		}

		Wnd.PostMessage(WM_TAKEFOCUS);

		return TRUE;
	}

	return FALSE;
}

BOOL CSysProxy::FlipToResPane(BOOL fNow)
{
	CWnd &Wnd = m_pSystem->GetDlgItem(IDVIEW+1);

	if( Wnd.IsWindowVisible() ) {

		if( fNow ) {

			Wnd.SetFocus();

			return TRUE;
		}

		Wnd.PostMessage(WM_TAKEFOCUS);

		return TRUE;
	}

	return FALSE;
}

void CSysProxy::SendPaneCommand(UINT uPane, UINT uID)
{
	CWnd &  Wnd     = m_pSystem->GetDlgItem(IDVIEW + uPane);

	MSG     Msg     = { Wnd.GetHandle(), WM_AFX_COMMAND, uID, 0 };

	LRESULT lResult = 0;

	Wnd.SetFocus();

	Wnd.RouteMessage(Msg, lResult);
}

void CSysProxy::SendPaneCommand(UINT uID)
{
	CDialog *pSemi = m_pSystem->GetSemiModal();

	if( pSemi ) {

		MSG     Msg     = { pSemi->GetHandle(), WM_AFX_COMMAND, uID, 0 };

		LRESULT lResult = 0;

		pSemi->SetFocus();

		pSemi->RouteMessage(Msg, lResult);

		return;
	}

	SendPaneCommand(InTwinMode() ? 0 : 2, uID);
}

void CSysProxy::AssumeViewerActive(void)
{
	m_pSystem->AssumeViewerActive();
}

// Command Management

void CSysProxy::ExecCmd(CCmd *pCmd)
{
	m_pSystem->ExecCmd(m_pView, pCmd);
}

void CSysProxy::SaveCmd(CCmd *pCmd)
{
	m_pSystem->SaveCmd(m_pView, pCmd);
}

BOOL CSysProxy::KillLastCmd(void)
{
	return m_pSystem->KillLastCmd();
}

BOOL CSysProxy::KillUndoList(void)
{
	return m_pSystem->KillUndoList();
}

// Global Find

BOOL CSysProxy::SetFindList(CString Head, CStringArray const &List, BOOL fJump)
{
	return m_pSystem->SetFindList(Head, List, fJump);
}

// Update Management

void CSysProxy::RegisterForUpdates(IUpdate *pUpdate)
{
	m_uUpdate = m_pSystem->RegisterForUpdates(pUpdate);
}

BOOL CSysProxy::UnregisterForUpdates(void)
{
	if( m_uUpdate ) {

		m_pSystem->UnregisterForUpdates(m_uUpdate);

		m_uUpdate = 0;

		return TRUE;
	}

	return FALSE;
}

void CSysProxy::ItemUpdated(CItem *pItem, UINT uType)
{
	m_pSystem->ItemUpdated(m_uUpdate, pItem, uType);
}

// End of File
