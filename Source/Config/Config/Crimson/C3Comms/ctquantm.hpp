
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_CTQUANTUM_HPP
	
#define	INCLUDE_CTQUANTUM_HPP

#include "mentor2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Control Techniques Quantum Driver : Control Techniques Mentor2 Driver
//

class CCTQuantumDriver : public CMentor2MasterSerialDriver
{
	public:
		// Constructor
		CCTQuantumDriver(void);
		
	protected:
		
	};

// End of File

#endif
