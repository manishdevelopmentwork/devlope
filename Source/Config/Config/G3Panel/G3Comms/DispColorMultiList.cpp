
#include "Intern.hpp"

#include "DispColorMultiList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispColorMultiEntry.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color List
//

// Dynamic Class

AfxImplementDynamicClass(CDispColorMultiList, CItemList);

// Constructor

CDispColorMultiList::CDispColorMultiList(void)
{
	m_Class = AfxRuntimeClass(CDispColorMultiEntry);
	}

// Item Access

CDispColorMultiEntry * CDispColorMultiList::GetItem(INDEX Index) const
{
	return (CDispColorMultiEntry *) CItemList::GetItem(Index);
	}

CDispColorMultiEntry * CDispColorMultiList::GetItem(UINT uPos) const
{
	return (CDispColorMultiEntry *) CItemList::GetItem(uPos);
	}

// End of File
