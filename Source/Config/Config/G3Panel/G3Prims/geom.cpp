
#include "intern.hpp"

#include "geom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Geometric Figure
//

// Dynamic Class

AfxImplementDynamicClass(CPrimGeom, CPrimWithText);

// Constructor

CPrimGeom::CPrimGeom(void)
{
	m_pFill = New CPrimTankFill;

	m_pEdge = New CPrimPen;
	}

// UI Overridables

BOOL CPrimGeom::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       2
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// Overridables

BOOL CPrimGeom::StepAddress(void)
{
	BOOL f1 = m_pFill->StepAddress();
	
	BOOL f2 = CPrimWithText::StepAddress();

	return f1 || f2;
	}

// Download Support

BOOL CPrimGeom::MakeInitData(CInitData &Init)
{
	CPrimWithText::MakeInitData(Init);

	Init.AddItem(itemSimple, m_pFill);

	Init.AddItem(itemSimple, m_pEdge);

	return TRUE;
	}

// Meta Data

void CPrimGeom::AddMetaData(void)
{
	CPrimWithText::AddMetaData();

	Meta_AddObject(Fill);
	Meta_AddObject(Edge);

	Meta_SetName((IDS_GEOMETRIC));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Rectangle
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRect, CPrimGeom);

// Constructor

CPrimRect::CPrimRect(void)
{
	m_uType = 0x01;
	}

// Overridables

BOOL CPrimRect::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

void CPrimRect::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	// REV3 -- The tank fills go into the border!

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillRect(pGDI, Rect);

	m_pEdge->DrawRect(pGDI, Rect, uMode);

	CPrimWithText::Draw(pGDI, uMode);
	}

void CPrimRect::FindTextRect(void)
{
	BOOL fHoney  = C3OemFeature(L"OemSD", FALSE);

	int  nExtra  = fHoney ? 1 : 0;

	int  nAdjust = m_pEdge->GetWidth() + nExtra;

	m_TextRect.x1 = m_DrawRect.x1 + nAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + nAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - nAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - nAdjust;
	}

// Meta Data

void CPrimRect::AddMetaData(void)
{
	CPrimGeom::AddMetaData();

	Meta_SetName((IDS_RECTANGLE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse
//

// Dynamic Class

AfxImplementDynamicClass(CPrimEllipse, CPrimGeom);

// Constructor

CPrimEllipse::CPrimEllipse(void)
{
	m_uType = 0x02;
	}

// Overridables

BOOL CPrimEllipse::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

void CPrimEllipse::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillEllipse(pGDI, Rect, etWhole);

	m_pEdge->DrawEllipse(pGDI, Rect, etWhole, uMode);

	CPrimWithText::Draw(pGDI, uMode);
	}

// Meta Data

void CPrimEllipse::AddMetaData(void)
{
	CPrimGeom::AddMetaData();

	Meta_SetName((IDS_ELLIPSE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Wedge
//

// Dynamic Class

AfxImplementDynamicClass(CPrimWedge, CPrimGeom);

// Constructor

CPrimWedge::CPrimWedge(void)
{
	m_uType  = 0x03;

	m_Orient = 0;
	}

// Overridables

BOOL CPrimWedge::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

void CPrimWedge::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillWedge(pGDI, Rect, m_Orient + 1);

	m_pEdge->DrawWedge(pGDI, Rect, m_Orient + 1, uMode);

	CPrimWithText::Draw(pGDI, uMode);
	}

// Download Support

BOOL CPrimWedge::MakeInitData(CInitData &Init)
{
	CPrimGeom::MakeInitData(Init);

	Init.AddByte(BYTE(m_Orient));

	return TRUE;
	}

// Meta Data

void CPrimWedge::AddMetaData(void)
{
	CPrimGeom::AddMetaData();

	Meta_AddInteger(Orient);

	Meta_SetName((IDS_WEDGE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse Quadrant
//

// Dynamic Class

AfxImplementDynamicClass(CPrimEllipseQuad, CPrimWedge);

// Constructor

CPrimEllipseQuad::CPrimEllipseQuad(void)
{
	m_uType = 0x04;
	}

// UI Update

void CPrimEllipseQuad::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		CLASS           Class = AfxRuntimeClass(CUITextEnum);

		CUITextElement *pText = pHost->FindUI(Class, m_pEdge, L"Width");

		CUITextEnum    *pEnum = (CUITextEnum *) pText;

		if( pEnum ) {

			pEnum->SetMask(3);
			}
		}

	CPrimGeom::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

BOOL CPrimEllipseQuad::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

void CPrimEllipseQuad::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillEllipse(pGDI, Rect, m_Orient + 1);

	m_pEdge->DrawEllipse(pGDI, Rect, m_Orient + 1, uMode);

	CPrimWithText::Draw(pGDI, uMode);
	}

// Meta Data

void CPrimEllipseQuad::AddMetaData(void)
{
	CPrimWedge::AddMetaData();

	Meta_SetName((IDS_C_ELLIPSE));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Ellipse Half
//

// Dynamic Class

AfxImplementDynamicClass(CPrimEllipseHalf, CPrimGeom);

// Constructor

CPrimEllipseHalf::CPrimEllipseHalf(void)
{
	m_uType  = 0x05;

	m_Orient = 0;
	}

// UI Update

void CPrimEllipseHalf::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		CLASS        Class = AfxRuntimeClass(CUITextEnum);

		CUITextEnum *pEnum = (CUITextEnum *) pHost->FindUI(Class, m_pEdge, L"Width");

		if( pEnum ) {

			pEnum->SetMask(3);
			}
		}

	CPrimGeom::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

BOOL CPrimEllipseHalf::HitTest(P2 Pos)
{
	return PtInRect(m_DrawRect, Pos);
	}

void CPrimEllipseHalf::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	m_pFill->FillEllipse(pGDI, Rect, m_Orient + 5);

	m_pEdge->DrawEllipse(pGDI, Rect, m_Orient + 5, uMode);

	CPrimWithText::Draw(pGDI, uMode);
	}

// Download Support

BOOL CPrimEllipseHalf::MakeInitData(CInitData &Init)
{
	CPrimGeom::MakeInitData(Init);

	Init.AddByte(BYTE(m_Orient));

	return TRUE;
	}

// Meta Data

void CPrimEllipseHalf::AddMetaData(void)
{
	CPrimGeom::AddMetaData();

	Meta_AddInteger(Orient);

	Meta_SetName((IDS_C_ELLIPSE_HALF));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Text Box
//

// Dynamic Class

AfxImplementDynamicClass(CPrimTextBox, CPrimRect);

// Constructor

CPrimTextBox::CPrimTextBox(void)
{
	m_uType = 0x06;
	}

// Operations

void CPrimTextBox::Set(CString Text, CSize MaxSize)
{
	// REV3 -- Allow tabs in pasted text.

	CStringArray Lines;

	Text.Replace('\t', ' ');

	Text.Replace('|',  '?');

	Text.Tokenize(Lines, L"\r\n");

	Text.Empty();

	int  cx = 0;

	UINT cl = Lines.GetCount();

	UINT n;

	for( n = 0; n < cl; n++ ) {

		CString Line = Lines[n];

		if( n < cl - 1 || !Line.IsEmpty() ) {

			if( !Text.IsEmpty() ) {

				Text += L'|';
				}

			Text += Line;
			}

		MakeMax(cx, m_pTextItem->GetTextWidth(Line) + 8);
		}

	if( cx <= 3 * MaxSize.cx / 4 ) {

		SetInitSize(cx, m_pTextItem->GetFontSize(n));

		UpdateLayout();
		}
	else {
		SetInitSize(MaxSize / 2);

		UpdateLayout();
		}

	m_pTextItem->Set(Text, TRUE);
	}

// Overridables

void CPrimTextBox::SetInitState(void)
{
	CPrimRect::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pFill->m_Pattern = 0;

	AddText();

	m_pTextItem->m_Lead = 0;

	SetInitSize(54, m_pTextItem->GetLineSize());
	}

void CPrimTextBox::FindTextRect(void)
{
	int yAdjust = m_pEdge->GetWidth();

	int xAdjust = yAdjust;

	m_TextRect.x1 = m_DrawRect.x1 + xAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + yAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - xAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - yAdjust;
	}

CSize CPrimTextBox::GetMinSize(IGDI *pGDI)
{
	CSize Size(8, 8);

	if( m_pTextItem ) {
	
		Size.cy = m_pTextItem->GetLineSize();
		}

	return Size;
	}

// Meta Data

void CPrimTextBox::AddMetaData(void)
{
	CPrimRect::AddMetaData();

	Meta_SetName((IDS_TEXT_BOX));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Data Box
//

// Dynamic Class

AfxImplementDynamicClass(CPrimDataBox, CPrimRect);

// Constructor

CPrimDataBox::CPrimDataBox(void)
{
	m_uType = 0x07;
	}

// Operations

void CPrimDataBox::Set(CString Text)
{
	m_pDataItem->Set(Text);

	m_pDataItem->m_Content = 1;

	CString Sizing = m_pDataItem->GetSizingText();

	int     nWidth = m_pDataItem->GetTextWidth(Sizing);

	SetInitSize(8 + nWidth);

	UpdateLayout();
	}

// Overridables

void CPrimDataBox::SetInitState(void)
{
	CPrimRect::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pFill->m_Pattern = 0;

	AddData();

	SetInitSize(54, m_pDataItem->GetFontSize(1) + 1);
	}

void CPrimDataBox::FindTextRect(void)
{
	int yAdjust = m_pEdge->GetWidth();

	int xAdjust = yAdjust;

	m_TextRect.x1 = m_DrawRect.x1 + xAdjust;
	
	m_TextRect.y1 = m_DrawRect.y1 + yAdjust;
	
	m_TextRect.x2 = m_DrawRect.x2 - xAdjust;
	
	m_TextRect.y2 = m_DrawRect.y2 - yAdjust;
	}

CSize CPrimDataBox::GetMinSize(IGDI *pGDI)
{
	CSize Size(8, 8);

	if( m_pDataItem ) {

		// REV3 -- If we've got a drop shadow, this could
		// be too small, but for the moment we let it go
		// to avoid too many red error bars.
	
		Size.cy = m_pDataItem->GetFontSize(1);

		if( m_pDataItem->m_Entry ) {

			Size.cy += 1;
			}
		}

	return Size;
	}

// Meta Data

void CPrimDataBox::AddMetaData(void)
{
	CPrimRect::AddMetaData();

	Meta_SetName((IDS_DATA_BOX));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Animatable Image
//

// Dynamic Class

AfxImplementDynamicClass(CPrimAnimImage, CPrimRect);

// Constructor

CPrimAnimImage::CPrimAnimImage(void)
{
	m_uType  = 0x0A;

	m_Keep   = 0;

	m_Count  = 1;

	m_pValue = NULL;

	m_pColor = NULL;

	m_pShow  = NULL;

	memset(m_pImage, 0, sizeof(m_pImage));
	}

// UI Overridables

BOOL CPrimAnimImage::OnLoadPages(CUIPageList *pList)
{
	LoadHeadPages(pList);

	pList->Append( New CUIStdPage( CString(IDS_IMAGES),
				       AfxPointerClass(this),
				       1
				       ));

	pList->Append( New CUIStdPage( CString(IDS_FIGURE),
				       AfxPointerClass(this),
				       2
				       ));

	pList->Append( New CUIStdPage( CString(IDS_SHOW),
				       AfxPointerClass(this),
				       3
				       ));

	LoadTailPages(pList);

	return TRUE;
	}

// UI Update

void CPrimAnimImage::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"Keep" ) {

		for( UINT n = 0; n < elements(m_pImage); n++ ) {

			if( m_pImage[n] ) {

				m_pImage[n]->m_Opts.m_Keep = m_Keep;

				pHost->UpdateUI(CPrintf(L"Image%2.2u", 1 + n));
				}
			}
		}

	if( Tag == L"Count" ) {

		DoEnables(pHost);
		}

	CPrimRect::OnUIChange(pHost, pItem, Tag);
	}

// Operations

void CPrimAnimImage::SetImage(UINT n, PCTXT pFile)
{
	CPrimImage * &pImage = m_pImage[n];

	if( !pImage ) {

		pImage = New CPrimImage;

		pImage->SetParent(this);

		pImage->Init();
		}

	if( n == 0 ) {

		CSize Max = CSize(500, 500);

		SetInitSize(pImage->LoadFromFile(pFile, Max));

		UpdateLayout();
		}
	else {
		CSize Max;

		Max.cx = m_DrawRect.x2 - m_DrawRect.x1;

		Max.cy = m_DrawRect.y2 - m_DrawRect.y1;

		pImage->LoadFromFile(pFile, Max);
		}
	}

// Overridables

void CPrimAnimImage::Draw(IGDI *pGDI, UINT uMode)
{
	if( TRUE ) {

		R2 Rect = m_DrawRect;

		m_pEdge->AdjustRect(Rect);

		m_pFill->FillRect(pGDI, Rect);

		m_pEdge->DrawRect(pGDI, Rect, uMode);
		}

	UINT n = NOTHING;

	for( UINT i = 0; i < elements(m_pImage); i++ ) {

		if( m_pImage[i] ) {

			m_pImage[i]->Draw(NULL, m_DrawRect, uMode, 0U);

			n = i;
			}
		}

	if( n < elements(m_pImage) ) {

		int  nImg = 0;

		UINT rop  = 0;
		
		if( m_pColor ) {
			
			if( !m_pColor->ExecVal() ) {

				rop = ropDisable;
				}
			}

		if( m_pValue ) {

			nImg = m_pValue->ExecVal();

			nImg = nImg % int(m_Count);
			}

		if( m_pImage[nImg] ) {

			R2  Rect = m_DrawRect;

			int nAdj = 2 * m_pEdge->GetWidth();

			DeflateRect(Rect, nAdj, nAdj);

			m_pImage[nImg]->Draw(pGDI, Rect, uMode, rop);
			}
		}
	else {
		pGDI->ResetFont();

		pGDI->SetTextTrans(modeTransparent);

		PCTXT pt = L"IMG";

		int   cx = pGDI->GetTextWidth(pt);

		int   cy = pGDI->GetTextHeight(pt);

		int   xp = m_DrawRect.x1 + (m_DrawRect.x2 - m_DrawRect.x1 - cx) / 2;

		int   yp = m_DrawRect.y1 + (m_DrawRect.y2 - m_DrawRect.y1 - cy) / 2;

		pGDI->TextOut(xp, yp, pt);
		}

	CPrimWithText::Draw(pGDI, uMode);
	}

void CPrimAnimImage::SetInitState(void)
{
	CPrimRect::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pFill->m_Pattern = 0;
	}

void CPrimAnimImage::GetRefs(CPrimRefList &Refs)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {
			
			m_pImage[n]->GetRefs(Refs);
			}
		}

	CPrimWithText::GetRefs(Refs);
	}

void CPrimAnimImage::EditRef(UINT uOld, UINT uNew)
{
	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		if( m_pImage[n] ) {
			
			m_pImage[n]->EditRef(uOld, uNew);
			}
		}

	CPrimWithText::EditRef(uOld, uNew);
	}

// Download Support

BOOL CPrimAnimImage::MakeInitData(CInitData &Init)
{
	CPrimRect::MakeInitData(Init);

	Init.AddByte(BYTE(m_Count));

	for( UINT n = 0; n < m_Count; n++ ) {

		Init.AddItem(itemVirtual, m_pImage[n]);
		}

	Init.AddItem(itemVirtual, m_pValue);

	Init.AddItem(itemVirtual, m_pColor);

	Init.AddItem(itemVirtual, m_pShow);

	return TRUE;
	}

// Meta Data

void CPrimAnimImage::AddMetaData(void)
{
	CPrimRect::AddMetaData();

	Meta_AddInteger(Keep);
	Meta_AddInteger(Count);
	Meta_AddVirtual(Value);
	Meta_AddVirtual(Color);
	Meta_AddVirtual(Show);

	for( UINT n = 0; n < elements(m_pImage); n++ ) {

		UINT uOffset = (PBYTE(m_pImage + n) - PBYTE(this));

		AddMeta(CPrintf(L"Image%2.2u", 1 + n), metaVirtual, uOffset);
		}

	Meta_SetName((IDS_ANIMATED_IMAGE));
	}

// Implementation

void CPrimAnimImage::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Value",   m_Count > 1);
	pHost->EnableUI("Image01", m_Count > 0);
	pHost->EnableUI("Image02", m_Count > 1);
	pHost->EnableUI("Image03", m_Count > 2);
	pHost->EnableUI("Image04", m_Count > 3);
	pHost->EnableUI("Image05", m_Count > 4);
	pHost->EnableUI("Image06", m_Count > 5);
	pHost->EnableUI("Image07", m_Count > 6);
	pHost->EnableUI("Image08", m_Count > 7);
	pHost->EnableUI("Image09", m_Count > 8);
	pHost->EnableUI("Image10", m_Count > 9);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Time and Data
//

// Dynamic Class

AfxImplementDynamicClass(CPrimTimeDate, CPrimDataBox);

// Constructor

CPrimTimeDate::CPrimTimeDate(void)
{
	}

// Overridables

void CPrimTimeDate::SetInitState(void)
{
	CPrimDataBox::SetInitState();
	
	m_pDataItem->SetTimeDate();

	CString Sizing = m_pDataItem->GetSizingText();

	int     nWidth = m_pDataItem->GetTextWidth(Sizing);

	SetInitSize(8 + nWidth);
	}

// Meta Data

void CPrimTimeDate::AddMetaData(void)
{
	CPrimDataBox::AddMetaData();

	Meta_SetName((IDS_TIME_DATE));
	}

// End of File
