
#include "intern.hpp"

#include "catlink.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CAT Data Link Driver
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CCatLinkDriver);

// Constructor

CCatLinkDriver::CCatLinkDriver(void)
{
	m_Ident      = DRIVER_ID;

	m_bSelf      = dropSelf;

	m_fGetFaults = FALSE;

	m_fGetDetail = FALSE;

	m_fAltFaults = FALSE;

	m_uStdPend   = 15;

	m_fSeeFaults = FALSE;

	m_uTryFaults = 0;

	m_fWrites    = FALSE;
	}

// Destructor

CCatLinkDriver::~CCatLinkDriver(void)
{
	}

// Configuration

void MCALL CCatLinkDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bSelf      = GetByte(pData);
		
		m_fGetFaults = GetByte(pData);
		
		m_fGetDetail = GetByte(pData);

		m_fWrites    = GetByte(pData);

		m_uStdPend   = GetByte(pData);

		m_uConnTime  = GetWord(pData);

		return;
		}
	}
	
void MCALL CCatLinkDriver::CheckConfig(CSerialConfig &Config)
{
	Config.m_uBaudRate = 56000;
	Config.m_uDataBits = 8;
	Config.m_uStopBits = 1;
	Config.m_uParity   = parityNone;
	Config.m_uFlags    = flagFastRx | flagTimeout;
	Config.m_uPhysical = 2;
	}
	
// Management

void MCALL CCatLinkDriver::Attach(IPortObject *pPort)
{
	m_pHandler = new CCatLinkHandler(m_pHelper, this, m_bSelf);

	pPort->Bind(m_pHandler);
	}

void MCALL CCatLinkDriver::Detach(void)
{
	m_pHandler->Stop();

	m_pHandler->Release();
	}

void MCALL CCatLinkDriver::Open(void)
{
	m_pHead     = NULL;

	m_pTail     = NULL;

	m_pScan     = NULL;

	m_uTime     = SetTimeout(timeSystemTick);

	m_uTick     = 0;

	m_uFaultHWM = 0;

	m_fFaultNew = FALSE;

	m_fDropNew  = FALSE;

	m_fForce    = FALSE;

	memset(m_bDrop,     0, sizeof(m_bDrop));

	memset(m_pPID,      0, sizeof(m_pPID));

	memset(&m_Site,     0, sizeof(m_Site));

	memset(m_FaultWork, 0, sizeof(m_FaultWork));

	memset(m_FaultList, 0, sizeof(m_FaultList));

	memset(m_uPoll,	    0, elements(m_uPoll) * 4);

	memset(m_pRepPID,   0, elements(m_pRepPID) * 4);

	memset(m_pRepSRC,   0, elements(m_pRepSRC));

	memset(m_pRepVAL,   0, elements(m_pRepVAL) * 4);

	m_fReport = FALSE;

	m_uReport = 0;
	
	m_pHandler->Start();
	}

// Device

CCODE MCALL CCatLinkDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::DeviceClose(BOOL fPersist)
{
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CCatLinkDriver::Service(void)
{
	Update();
	}

CCODE MCALL CCatLinkDriver::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CCatLinkDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == 100 ) {

		// This table returns flags showing the status of each
		// MID on the network. A zero value indicates that we
		// have not seen that drop. A non-zero value means we
		// have seen it transmitting or being targetted.

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_bDrop) ) { 

				pData[n] = m_bDrop[s];
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table == 101 ) {

		// This table returns the engine serial number.

		CPID *pPID = LookupPID(pidSerial);

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_sSerial) ) { 

				pData[n] = BYTE(m_sSerial[s]);
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table == 102 ) {

		// This table returns the MID poll counter.

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_uPoll) ) { 

				pData[n] = m_uPoll[s];
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table == 103 ) {

		// This table returns the report PIDs.

		m_fReport = TRUE;

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_pRepPID) ) { 

				pData[n] = m_pRepPID[s];
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table == 104 ) {

		// This table returns the respective report PIDs source.

		m_fReport = TRUE;

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_pRepSRC) ) { 

				pData[n] = m_pRepSRC[s];
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table == 105 ) {

		// This table returns the respective report PIDs value.

		m_fReport = TRUE;

		UINT s = Addr.a.m_Offset;

		for( UINT n = 0; n < uCount; n++ ) {

			if( s < elements(m_pRepVAL) ) { 

				pData[n] = m_pRepVAL[s];
				}
			else
				pData[n] = 0;

			s++;
			}

		return uCount;
		}

	if( Addr.a.m_Table >= 1 && Addr.a.m_Table <= 57 ) {

		// These tables return various bits of fault data. We
		// extract them from the snapshot in m_FaultList which
		// we update when a fault sequence has been completed.

		if( m_fGetFaults ) {

			UINT uItem = Addr.a.m_Offset;

			if( uItem < elements(m_FaultList) ) {

				CFault *pList = m_FaultList + uItem;

				switch( Addr.a.m_Table ) {

					case  1: *pData = pList->m_bSrc;     return 1;
					case  2: *pData = pList->m_wCode;    return 1;
					case  3: *pData = pList->m_bSubcode; return 1;
					case  4: *pData = pList->m_bFlags;   return 1;
					case  5: *pData = pList->m_wCount;   return 1;
					case  6: *pData = pList->m_dwFirst;  return 1;
					case  7: *pData = pList->m_dwLast;   return 1;
					}

				BOOL fActive = (pList->m_bFlags & 0x1);

				// Active only list

				if( fActive && Addr.a.m_Table >= 11 && Addr.a.m_Table <= 17 ) {

					switch( Addr.a.m_Table % 10 ) {

						case  1: *pData = pList->m_bSrc;	return 1;
						case  2: *pData = pList->m_wCode;	return 1;
						case  3: *pData = pList->m_bSubcode;	return 1;
						case  4: *pData = pList->m_bFlags >> 7;	return 1;
						case  5: *pData = pList->m_wCount;	return 1;
						case  6: *pData = pList->m_dwFirst;	return 1;
						case  7: *pData = pList->m_dwLast;	return 1;
						} 
					}

				// InActive only list

				if( Addr.a.m_Table >= 21 && Addr.a.m_Table <= 27 ) { 

					// Revert to beginning of list to ensure start of inactives

					uItem = 0;

					pList = m_FaultList + uItem;

					fActive = (pList->m_bFlags & 0x1);

					while( fActive ) {

						uItem++;

						if( uItem < elements(m_FaultList) ) {

							pList = m_FaultList + uItem;

							fActive = (pList->m_bFlags & 0x1);
							}
						else
							fActive = FALSE;
						}
					
					uItem += Addr.a.m_Offset;

					if( uItem < elements(m_FaultList) ) {

						pList = m_FaultList + uItem;

						switch( Addr.a.m_Table % 10 ) {

							case  1: *pData = pList->m_bSrc;	return 1;
							case  2: *pData = pList->m_wCode;	return 1;
							case  3: *pData = pList->m_bSubcode;	return 1;
							case  4: *pData = pList->m_bFlags >> 7;	return 1;
							case  5: *pData = pList->m_wCount;	return 1;
							case  6: *pData = pList->m_dwFirst;	return 1;
							case  7: *pData = pList->m_dwLast;	return 1;
							}
						}
					}

				// Active Event only and Active Diag only lists

				if( Addr.a.m_Table >= 31 && Addr.a.m_Table <= 47 ) { 

					BOOL fDiag = Addr.a.m_Table >= 41;

					uItem = 0;

					for( UINT uOffset = 0; uItem < elements(m_FaultList); uItem++ ) {

						pList = m_FaultList + uItem;

						fActive = (pList->m_bFlags & 0x1);

						if ( fActive ) {

							if( BYTE(fDiag << 7) == BYTE(pList->m_bFlags & 0x80) ) {

								if( uOffset == Addr.a.m_Offset ) {

									switch( Addr.a.m_Table % 10 ) {

										case  1: *pData = pList->m_bSrc;	return 1;
										case  2: *pData = pList->m_wCode;	return 1;
										case  3: *pData = pList->m_bSubcode;	return 1;
										case  4: *pData = pList->m_bFlags >> 7;	return 1;
										case  5: *pData = pList->m_wCount;	return 1;
										case  6: *pData = pList->m_dwFirst;	return 1;
										case  7: *pData = pList->m_dwLast;	return 1;
										}
									}

								uOffset++;
								}

							continue;
							}
						
						break;
						}
					}

				}
			}

		*pData = 0;

		return 1;
		}

	if( Addr.a.m_Table >= addrNamed ) {

		// These tables hold the PID values and the associated
		// status and disable information. The status data has
		// the source PID in the low byte, and bit 8 set if we
		// are getting valid data. Bit 9 toggles on each data
		// frame we receive to allow update speed to be judged.

		UINT uName = Addr.a.m_Offset;

		UINT uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

		UINT uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;

		switch( uType ) {

			case 1:	uName += 0x000000; break;
			case 2:	uName += 0xD00000; break;
			case 3:	uName += 0xD10000; break;
			}

		if( IsValidPID(uName) ) {

			// Note that the LookupPID function will create a
			// a new PID list entry and, if we have a site config
			// figured out, it will kick off a poll sequence.

			CPID *pPID = LookupPID(uName);

			if( pPID ) {

				if( uFunc == 0 ) {

					if( pPID->m_fValid ) {
						
						*pData = pPID->m_Data;

						return 1;
						}

					*pData = 0;

					return 1;
					}

				if( uFunc == 1 ) {

					BYTE bLo = (pPID->m_bSrc);

					BYTE bHi = (pPID->m_fValid | ((pPID->m_bOkay & 1) << 1));

					*pData = MAKEWORD(bLo, bHi);

					return 1;
					}

				if( uFunc == 2 ) {

					*pData = !pPID->m_fDisable;

					return 1;
					}
				}
			}
		}

	*pData = 0;

	return 1;
	}

CCODE MCALL CCatLinkDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table >= addrNamed ) {

		UINT uName = Addr.a.m_Offset;

		UINT uType = (Addr.a.m_Table - addrNamed) % 3 + 1;

		UINT uFunc = (Addr.a.m_Table - addrNamed) / 3 + 0;

		switch( uType ) {

			case 1:	uName += 0x000000; break;
			case 2:	uName += 0xD00000; break;
			case 3:	uName += 0xD10000; break;
			}

		if( IsValidPID(uName) ) {

			CPID *pPID = LookupPID(uName);

			if( pPID ) {

				if( uFunc == 0 ) {

					// Writes are only allowed to certain PIDs. If we can
					// write, we don't have a write active, and we have
					// a source MID for this PID, we queue a request and
					// set the retry counter. Writes that are attempted
					// during other writes result in a busy reply.

					if( m_fWrites && IsWritablePID(uName) ) {

						pPID->m_Write = *pData;

						if( !pPID->m_fDisable ) {

							if( !pPID->m_bSend ) {

								if( pPID->m_bSrc ) {

									CMID *pMID = LookupMID(pPID->m_bSrc);

									UINT uSlot = PIDToSlot(uName);

									QueueRequest(pMID, reqWrite, uSlot);

									pPID->m_bSend = tryWrite;
									}

								return 1;
								}

							return CCODE_BUSY;
							}
						}

					return 1;
					}
				
				if( uFunc == 1 ) {

					// A write to the status value sets that value as the
					// forced source MID for that PID. Only that MID will
					// be polled, and no deferrals will occur. Once one
					// PID has been forced, the site survey functions are
					// disabled and only forced PIDs will be updated. This
					// mode is intended for test use only.

					if( !pPID->m_bSrc ) {

						UINT uSlot = PIDToSlot(uName);

						pPID->m_fDisable = FALSE;

						pPID->m_bSrc     = BYTE(*pData);

						pPID->m_fForce   = 1;

						UpdatePID(pPID, uSlot);
						}
					else {
						UINT uSlot = PIDToSlot(uName);

						pPID->m_fDisable = FALSE;

						pPID->m_bSrc     = BYTE(*pData);

						pPID->m_fForce   = 1;
						}

					m_fForce = TRUE;

					return 1;
					}

				if( uFunc == 2 ) {

					// A write to this value can be used to enable or
					// disable a PID. Enabling a PID means queueing
					// a read via UpdatePID if we have site data.

					if( !*pData ) {

						pPID->m_fDisable = TRUE; 
						}
					else {
						if( pPID->m_fDisable ) {

							UINT uSlot = PIDToSlot(uName);

							pPID->m_fDisable = FALSE;

							pPID->m_bSrc     = 0;

							pPID->m_bLoop    = 0;

							if( m_Site.m_uType ) {
							
								UpdatePID(pPID, uSlot);
								}
							}
						}

					return 1;
					}
				}
			}
		}
	
	return 1;
	}

// Handler Calls

void CCatLinkDriver::Handler_SeeDrop(BYTE bDrop)
{
	if( bDrop != m_bSelf && bDrop != dropBroadcast ) {

		if( m_bDrop[bDrop] == 0 ) {

			m_bDrop[bDrop] = 1;

			m_fDropNew     = TRUE;
			}
		}
	}

BOOL CCatLinkDriver::Handler_ReportPID(void)
{
	return m_fReport;
	}

void CCatLinkDriver::Handler_SeePID(BYTE bDrop, UINT uName, UINT uData)
{
	if( m_fReport ) {

		for( UINT u = 0; u < m_uReport; u++ ) {

			if( (m_pRepPID[u] == uName) && (m_pRepSRC[u] == bDrop) ) {
						
				m_pRepVAL[u] = uData;

				return;
				}
			}

		if( m_uReport < elements(m_pRepPID) - 1 ) {

			m_pRepPID[m_uReport] = uName;

			m_pRepSRC[m_uReport] = bDrop;

			m_pRepVAL[m_uReport] = uData;
			
			m_uReport++;
			}
		}
	}

UINT CCatLinkDriver::Handler_GetSendData(PBYTE pData)
{
	// This function is called when the handler needs a new frame
	// to send. We have already figured out which frame to transmit
	// in the prior call to GetNextSend. We call BuildRequest to
	// construct the fram for the request. If we get a send count
	// of zero, we skip the frame and set its timeout to zero so
	// that it will immediately fail. If we get a count of NOTHING
	// we also skip the frame, but this time we leave the timeout
	// alone so it will stay in the pending queue. This mechanism
	// is used to place Diag and Event frames in the pend queue to
	// accept (possibly) unsolicited fault messages.

	if( m_pScan ) {

		BYTE bDrop = m_pScan->m_bDrop;

		CReq *pReq = m_pScan->m_pSendHead;

		UINT uSend = BuildRequest(pData, bDrop, pReq->m_wType, pReq->m_wSlot);

		if( uSend == NOTHING ) {

			return 0;
			}

		if( uSend ) {

			BYTE bSum = 0;

			for( UINT n = 0; n < uSend; n++ ) {

				bSum += pData[n];
				}

			pData[n] = BYTE(256 - bSum);

			return uSend + 1;
			}

		pReq->m_uTime = 0;
		}
	
	return 0;
	}

void CCatLinkDriver::Handler_GetNextSend(void)
{
	if( m_pScan ) {

		// Before we figure out what to send next, we first decided what
		// to do with the previously transmitted frame. We first remove
		// it from the send queue. If it expects a reply, we move it to
		// the pending queue, and set the timeout running if desired. We
		// also increment the pending counter for that MID by the score
		// assigned to the frame. (Most frames score one, but writes have
		// a larger score to limit the number outstanding.) If the frame
		// does not expect a reply, we move it to the done queue and set
		// the m_uTime member to 1 to indicate sucess.

		CReq *pReq = m_pScan->m_pSendHead;

		UINT uPend = GetPendScore(m_pScan, pReq);

		AfxListRemove( m_pScan->m_pSendHead,
			       m_pScan->m_pSendTail,
			       pReq,
			       m_pNext,
			       m_pPrev
			       );

		if( GetReplyFlag(pReq) ) {

			AfxListAppend( m_pScan->m_pPendHead,
				       m_pScan->m_pPendTail,
				       pReq,
				       m_pNext,
				       m_pPrev
				       );

			if( pReq->m_uTime < NOTHING ) {

				pReq->m_uTime += GetTickCount();
				}

			m_pScan->m_uPendCount += uPend;
			}
		else {
			AfxListAppend( m_pScan->m_pDoneHead,
				       m_pScan->m_pDoneTail,
				       pReq,
				       m_pNext,
				       m_pPrev
				       );

			pReq->m_uTime = 1;
			}
		}

	if( m_pHead ) {

		// We now try to find a frame to send. If we previously sent from
		// a MID, we start at the next unit. Otherwise, we start at the
		// start of our linked list. As we scan the MIDs, we check the
		// timeouts on their outstandiong frames. If we find a frame in
		// the MID's send queue and if the pend score won't take the MID
		// over its pending limit, that is what we send. Otherwise, we
		// keep checking MIDs until we have checked them all.

		CMID *pFind = m_pScan ? m_pScan->m_pNext : m_pHead;

		CMID *pInit = pFind;

		UINT  uTime = GetTickCount();

		while( pFind ) {

			CheckRequestTimeouts(pFind, uTime);

			if( pFind->m_pSendHead ) {

				UINT uPend = GetPendScore(pFind, pFind->m_pSendHead);

				if( pFind->m_uPendCount + uPend <= pFind->m_uPendLimit ) {

					m_pScan = pFind;

					return;
					}
				}

			if( (pFind = pFind->m_pNext) == pInit ) {

				break;
				}
			}

		m_pScan = NULL;
		}
	}

void CCatLinkDriver::Handler_WriteDone(BYTE bSrc, UINT uName)
{
	// If we see a frame than signifies that a write has been
	// completed, find the associated request in the pending
	// queue and indicate that it completed sucessfully.

	WORD wSlot = PIDToSlot(uName);

	CReq *pReq = FindRequest(bSrc, reqWrite, wSlot);

	if( pReq ) {

		SetRequestDone(bSrc, pReq, TRUE);
		}
	}

void CCatLinkDriver::Handler_DataFrame(BYTE bSrc, UINT uName, UINT uData)
{
	// If we see a data frame, find the associated read
	// request in the pending queue, update the data value
	// and indicate that it has completed sucessfully.

	WORD wSlot = PIDToSlot(uName);

	CReq *pReq = FindRequest(bSrc, reqRead, wSlot);

	if( pReq ) {

		pReq->m_uData = uData;

		SetRequestDone(bSrc, pReq, TRUE);
		}
	}

void CCatLinkDriver::Handler_FaultData(BYTE bSrc, PCBYTE pData, UINT uCount)
{
	// If we see a fault frame, find the associated frame in the
	// pending queue and indicate that it completed. Note that for
	// these frames we copy data into a buffer defined in the request
	// for later processing. Note also that 0x0D and 0x0E frames match
	// any of the entries left sitting in the pending queue for just
	// this type of data; they are not matched to specific requests. If
	// we are in alternate fault mode, we use a modified algorithm that
	// polls for faults using the 0x82 frame, but that treats replies
	// in more-or-less the same way.

	WORD wType = 0;

	WORD wSlot = 0;

	switch( pData[0] ) {

		case 0x82:

			if( m_fAltFaults ) {

				wType = reqDiags;
				}

			break;

		case 0x84:

			if( m_fAltFaults ) {
				
				wType = reqDiagDetail;

				wSlot = FindFault(bSrc, MAKEWORD(pData[3], pData[2]), TRUE);
				}

			break;

		case 0xFA:

			switch( m_fAltFaults ? 0x00 : pData[1] ) {

				case 0x0D:
					
					wType = reqDiags;
					
					break;

				case 0x0E:
					
					wType = reqEvents;
					
					break;

				case 0x12:
					
					wType = reqDiagDetail;

					wSlot = FindFault(bSrc, MAKEWORD(pData[4], pData[3]), TRUE);

					break;

				case 0x10:
					
					wType = reqEventDetail;

					wSlot = FindFault(bSrc, MAKEWORD(pData[4], pData[3]), FALSE);
					
					break;
				}
			break;
		}

	if( wType ) {

		CReq *pReq = FindRequest(bSrc, wType, wSlot);

		if( pReq ) {

			memcpy(pReq->m_pData, pData, uCount);

			pReq->m_uData = uCount;

			SetRequestDone(bSrc, pReq, TRUE);

			m_fSeeFaults = TRUE;

			return;
			}
		}
	}

void CCatLinkDriver::Handler_SerialNum(PCTXT pSerial, BYTE bSrc)
{
	strcpy(m_sSerial, pSerial);
	}

void CCatLinkDriver::Handler_SetPoll(void)
{
	BYTE bDrop = m_pScan->m_bDrop;
	
	m_uPoll[bDrop] = m_uPoll[bDrop] + 1;
	}

UINT CCatLinkDriver::Handler_GetConnectionTime(void)
{
	return m_uConnTime;
	}

// Main Scan Loop

void CCatLinkDriver::Update(void)
{
	// This is the main update loop. The first thing we do is look for
	// any new MIDs and create the MID structures for them, adding them
	// to our linked list. We can't do this in the handler calls as those
	// occur in the interrupt level and we can't allocate memory there.

	if( m_fDropNew ) {

		for( UINT i = 0; i < 256; i++ ) {

			if( m_bDrop[i] == 1 ) {

				m_bDrop[i]    = 2;

				CMID *pMID    = CreateMID(i);

				pMID->m_fSeen = TRUE;
				}
			}

		m_fDropNew = FALSE;
		}

	// The next step is to walk the MID list and process any
	// requests that have been moved into the done queue. We
	// also call a function to perform any MID-specific stuff.

	for( CMID *pMID = m_pHead; pMID; pMID = pMID->m_pNext ) {

		CReq *pReq;
		
		while( (pReq = pMID->m_pDoneHead) ) {

			HostMaxIPL();

			AfxListRemove( pMID->m_pDoneHead,
				       pMID->m_pDoneTail,
				       pReq,
				       m_pNext,
				       m_pPrev
				       );

			HostMinIPL();

			ProcessReply(pMID, pReq);

			delete pReq->m_pData;

			delete pReq;
			}

		UpdateMID(pMID);
		}

	// If one second has gone by, we perform various other housekeeping
	// tasks. We periodically perform the site survey function, which
	// attempts to figure out what kind of site this is and what MIDs
	// we should be talking to; we send diag and event poll requests if
	// we are collecting fault data; we bring back to life a subset of
	// those PIDs that have been deferred ie. taken off-scan; and we
	// see if we have complete fault information in our work buffer from
	// which we can update the user-accessible information.

	if( Timeout(m_uTime) ) {

		m_uTime = SetTimeout(timeSystemTick);

		m_uTick = m_uTick + 1;

		if( !m_fForce ) {

			if( m_uTick % secsSiteSurvey == 0 ) {

				UpdateSite();
				}

			if( m_Site.m_uType ) {

				// The gap between fault polls is much shorter until
				// we see fault data. We start off using the standard
				// method, and flip to alternative processing if no
				// replies come within 20 seconds. In alt mode, we poll
				// ever few seconds. In normal mode, faults are sent
				// as they occur, so we only poll every minute or so
				// to make sure we didn't miss anything.

				UINT uPeriod = secsStdFaults;
				
				if( m_fAltFaults ) {

					if( m_Site.m_bECM1 == 0x0E ) {

						uPeriod = secsStdFaults;
						}
					else
						uPeriod = secsAltFaults;
					}
				else {
					if( !m_fSeeFaults ) {

						if( ++m_uTryFaults >= 20 ) {

							m_fAltFaults = TRUE;
							}

						uPeriod = secsHitFaults;
						}
					}

				if( m_uTick % uPeriod == 0 ) {

					UpdatePolls();
					}

				m_fReport = FALSE;
				}

			UpdateDeferred();

			UpdateFaults();
			}
		}
	}

void CCatLinkDriver::UpdateSite(void)
{
	// This function performs the site survey. If the MID list is empty, we
	// haven't seen any activity for a while so we create a number of standard
	// MIDs to which we can send polls. If MIDs are present, we call FindSiteType
	// and see if a new site definition has been returned. If so, we start
	// polls to all our defined PIDs. If enabled, we also queue the diag and
	// event request that will hold any incoming fault information. (If the
	// site changes, we might queue more than we need, but who cares?)

	if( m_pHead ) {

		CSite Site;

		memset(&Site, 0, sizeof(Site));

		if( FindSiteType(Site) ) {

			if( memcmp(&m_Site, &Site, sizeof(Site)) ) {

				memcpy(&m_Site, &Site, sizeof(Site));

				for( UINT uSlot = 0; uSlot < elements(m_pPID); uSlot++ ) {

					if( m_pPID[uSlot] ) {

						UpdatePID(m_pPID[uSlot], uSlot);
						}
					}

				if( m_fGetFaults ) {

					for( UINT n = 0; n < siteCount; n++ ) {

						BYTE bDrop = PBYTE(&m_Site.m_bGSC)[n];

						if( bDrop ) {

							CMID *pMID = LookupMID(bDrop);

							for( UINT i = 0; i < faultSlots; i++ ) {

								QueueRequest(pMID, reqDiags,  0);

								QueueRequest(pMID, reqEvents, 0);
								}
							}
						}
					}

				ShowSiteInfo();
				}
			}

		return;
		}

	CreateStandardMIDs();
	}

void CCatLinkDriver::UpdatePolls(void)
{
	// This function resends the diag and event polls. It is called
	// every minute or so. We don't need it on some sites, but on
	// others than info is not broadcast. Note that we only send
	// to MIDs that are part of our site definition.

	if( m_fGetFaults ) {

		for( UINT n = 0; n < siteCount; n++ ) {

			BYTE bDrop = PBYTE(&m_Site.m_bGSC)[n];

			if( bDrop ) {

				CMID *pMID = LookupMID(bDrop);

				if( m_fAltFaults ) {

					if( !pMID->m_uDiagPend ) {

						QueueRequest(pMID, reqDiagPoll, 0);
						}
					}
				else {
					QueueRequest(pMID, reqDiagPoll,  0);

					QueueRequest(pMID, reqEventPoll, 0);
					}
				}
			}
		}
	}

void CCatLinkDriver::UpdateDeferred(BYTE bSrc)
{
	// This function is called when a PID is first found in an
	// MID. It finds PIDs that could be read from the same MID
	// and that are off-scan, and kicks off a new poll request
	// as we can now most likely read them sucessfully.

	if( m_Site.m_uType ) {

		for( UINT uSlot = 0; uSlot < elements(m_pPID); uSlot++ ) {

			CPID *pPID = m_pPID[uSlot];

			if( pPID && !pPID->m_bSrc ) {

				if( bSrc == GetSource(pPID) ) {

					UpdatePID(pPID, uSlot);
					}
				}
			}
		}
	}

void CCatLinkDriver::UpdateDeferred(void)
{
	// This function is called once a second, and puts a subset of
	// the deferred PIDs back on the scan so that we eventually retry
	// them all over a one minute period or so.

	if( m_Site.m_uType ) {

		UINT uInit = m_uTick % secsDeferPoll;

		for( UINT uSlot = uInit; uSlot < elements(m_pPID); uSlot += secsDeferPoll ) {

			CPID *pPID = m_pPID[uSlot];

			if( pPID && !pPID->m_bSrc ) {

				UpdatePID(pPID, uSlot);
				}
			}
		}
	}

void CCatLinkDriver::UpdateFaults(void)
{
	// This function is called to check if we have new fault data
	// and if that data is ready to publish. We cannot update the
	// data if we are in the middle of a multiframe diag or event
	// sequence, or if we have outstanding detail requests. If we
	// are okay to update, we sort it and copy it over.

	if( m_fGetFaults ) {

		if( m_fFaultNew ) {

			for( CMID *pMID = m_pHead; pMID; pMID = pMID->m_pNext ) {
				
				if( pMID->m_bFaultFlag ) {

					return;
					}

				if( pMID->m_uDiagPend || pMID->m_uEventPend ) {

					return;
					}
				}

			qsort(m_FaultWork, m_uFaultHWM, sizeof(CFault), SortFunc);
			
			ShowFaults();

			Critical(TRUE);

			memcpy(m_FaultList, m_FaultWork, m_uFaultHWM * sizeof(CFault));

			Critical(FALSE);

			m_fFaultNew = FALSE;
			}
		}
	}

void CCatLinkDriver::UpdateMID(CMID *pMID)
{
	// This function only does anything when we don't have a site
	// determined. It periodically polls automatically added standard
	// MIDs for their key PIDs to force them to reply if present.

        if( !m_Site.m_uType && !m_fForce ) {
		
		if( !pMID->m_fSeen && !pMID->m_uPendCount ) {

			if( Timeout(pMID->m_uTime) ) {

				UINT uName = GetKeyPID(pMID->m_bType);

				if( uName ) {

					CPID *pPID = LookupPID(uName);

					UINT uSlot = PIDToSlot(uName);

					QueueRequest(pMID, reqRead, uSlot);

					pMID->m_uTime = SetTimeout(timePingDrop);

					pMID->m_uPing = pMID->m_uPing + 1;
					}
				}
			}
		}
	}

void CCatLinkDriver::UpdatePID(CPID *pPID, UINT uSlot)
{
	// This function kicks off a poll sequence. Once a sequence is
	// running, it will only cease if the PID is deferred as a result
	// too many timeouts. Deferred PIDs have zero in m_bSrc. If the
	// user has forced the MID for the PID, we use that, otherwise we
	// use the first possible source MID by setting the preference
	// value to zero and then calling GetSource.

	if( pPID->m_fForce ) {

		CMID *pMID = CreateMID(pPID->m_bSrc);

		QueueRequest(pMID, reqRead, uSlot);

		return;
		}

	if( !pPID->m_bSrc && !pPID->m_fDisable ) {

		pPID->m_bPref = 0;

		pPID->m_bFail = 0;

		pPID->m_bOkay = 0;

		BYTE bSrc = GetSource(pPID);

		if( bSrc ) {

			CMID *pMID = LookupMID(bSrc);

			if( pMID ) {

				pPID->m_bSrc = bSrc;

				QueueRequest(pMID, reqRead, uSlot);
				}
			}

		return;
		}
	}

// Reply Processing

void CCatLinkDriver::ProcessReply(CMID *pMID, CReq *pReq)
{
	switch( pReq->m_wType ) {
		
		case reqRead:
	
			ProcessReadReply(pMID, pReq);

			break;

		case reqWrite:

			ProcessWriteReply(pMID, pReq);

			break;

		case reqDiags:

			ProcessDiagsReply(pMID, pReq);

			break;

		case reqEvents:

			ProcessEventsReply(pMID, pReq);

			break;

		case reqDiagDetail:

			ProcessDiagDetailReply(pMID, pReq);

			break;

		case reqEventDetail:

			ProcessEventDetailReply(pMID, pReq);

			break;
		}
	}

void CCatLinkDriver::ProcessReadReply(CMID *pMID, CReq *pReq)
{
	UINT uSlot = pReq->m_wSlot;

	CPID *pPID = m_pPID[uSlot];

	if( pPID ) {

		if( pMID->m_bDrop == pPID->m_bSrc ) {

			if( pPID->m_fDisable ) {

				// If the PID has been disabled, indicate that it is
				// no longer valid and take it off the scan by setting
				// m_bSrc to zero. Look above to note that disabled
				// PIDs won't be put back on-scan by UpdatePID.

				pPID->m_fValid = 0;

				pPID->m_bSrc   = 0;
				}
			else {
				UINT uName = pPID->m_uName;

				if( pReq->m_uTime ) {

					// If the read request was sucessful, get the data
					// from the frame and perform any sign extension
					// according to the tables contained in catbase.

					UINT uData = pReq->m_uData;

					if( ReOrderWord(uName) ) {

						uData = UINT(IntelToHost(WORD(uData)));
						}

					if( ReOrderLong(uName) ) {

						uData = UINT(IntelToHost(DWORD(uData)));
						}

					if( SignExtendByte(uName) ) {

						uData = UINT(LONG(SHORT(SCHAR(uData))));
						}

					if( SignExtendWord(uName) ) {

						uData = UINT(LONG(SHORT(uData)));
						}

					if( IsDataValid(uName, uData) ) {

						pPID->m_Data   = uData;

						pPID->m_bOkay += 1;

						pPID->m_bFail  = 0;

						pPID->m_bLoop  = 0;

						if( !pPID->m_fValid ) {

							// If a key PID wasn't previously valid, we might
							// just have seen a new MID so bring any off-scan
							// PIDs that from this MID back on-scan.

							if( IsKeyPID(pPID->m_uName) ) {

								UpdateDeferred(pPID->m_bSrc);
								}

							pPID->m_fValid = TRUE;
							}

						/*if( !pPID->m_fForce && pPID->m_bPref ) {
						
							// If we're not forced and we're using something
							// other than our first preference as a source,
							// after a given number of frames, flip back to our
							// first preference but with a preloaded retry count
							// so that we'll fail straight away on a timeout.

							if( pPID->m_bOkay >= GetRevertCount(pPID) ) {

								pPID->m_bPref = 0;

								pPID->m_bFail = 15;

								pPID->m_bSrc  = GetSource(pPID);
								}
							}*/
						}
					else {
						// Some devices send invalid data for sensors that don't 
						// exist!  In this case update the preference so that the
						// next MID is tried.

						pPID->m_bPref += 1;

						pPID->m_fValid = FALSE;

						pPID->m_bSrc   = GetSource(pPID);

						if( !pPID->m_bSrc ) {

							// If we have no furthe MIDs to try, if we
							// are not reading a key PID, take the PID
							// off-scan if we have been around the loop
							// too many times. It will get put back on
							// scan periodically or if its preferential
							// MID comes back online.

							pPID->m_bPref = 0;

							if( !IsKeyPID(pPID->m_uName) ) {

								pPID->m_bLoop += 1;

								if( pPID->m_bLoop >= tryLoops ) {

									pPID->m_bLoop -= 1;

									pPID->m_bSrc   = 0;

									return;
									}
								}
								
							pPID->m_bSrc = GetSource(pPID);
							}
						}
					}
				else {
					if( !pPID->m_fForce ) {

						// If we failed and we're not forced, check if
						// we should retry this MID of if we should try
						// the next preference. Note that TryNextMid
						// clears m_bOkay and leaves m_bFail alone if
						// it is 15 to ensure that attempts to retry
						// more preferential MIDs fail straight away.

						if( TryNextMID(pPID) ) {

							pPID->m_bPref += 1;

							pPID->m_fValid = IsGSC(pPID->m_bSrc) ? TRUE : FALSE;

							pPID->m_bSrc   = GetSource(pPID);

							if( !pPID->m_bSrc ) {

								// If we have no furthe MIDs to try, if we
								// are not reading a key PID, take the PID
								// off-scan if we have been around the loop
								// too many times. It will get put back on
								// scan periodically or if its preferential
								// MID comes back online.

								pPID->m_bPref = 0;

								if( !IsKeyPID(pPID->m_uName) ) {

									pPID->m_bLoop += 1;

									if( pPID->m_bLoop >= tryLoops ) {

										pPID->m_bLoop -= 1;

										pPID->m_bSrc   = 0;

										pPID->m_fValid = FALSE;

										return;
										}
									}
								
								pPID->m_bSrc = GetSource(pPID);
								}
							}
						}
					}

				if( pPID->m_bSrc ) {

					// If we have a MID to target, see if we need
					// to re-lookup the MID pointer, and then queue
					// a new read request. This queuing occurs on
					// every call to this function unless we take
					// the PID off-scan after too many timeouts.

					if( pMID->m_bDrop - pPID->m_bSrc ) {

						pMID = LookupMID(pPID->m_bSrc);
						}

					QueueRequest(pMID, reqRead, uSlot);
					}
				}
			}
		}
	}

void CCatLinkDriver::ProcessWriteReply(CMID *pMID, CReq *pReq)
{
	UINT uSlot = pReq->m_wSlot;

	CPID *pPID = m_pPID[uSlot];

	if( pPID ) {

		if( pMID->m_bDrop == pPID->m_bSrc ) {

			if( pPID->m_fDisable ) {

				pPID->m_bSend = 0;
				}
			else {
				if( !pReq->m_uTime ) {
					
					if( --pPID->m_bSend ) {

       						CMID *pMID = LookupMID(pPID->m_bSrc);

						QueueRequest(pMID, reqWrite, uSlot);
						}
					}
				else
					pPID->m_bSend = 0;
				}
			}
		}
	}

void CCatLinkDriver::ProcessDiagsReply(CMID *pMID, CReq *pReq)
{
	if( IsActiveMID(pMID->m_bDrop) ) {

		if( m_fAltFaults ) {

			if( !pMID->m_uDiagPend ) {

				// If we're using alt faults, all the data is in one
				// frame so we clear everything down and then start
				// to scan for fault records.

				ClearFaults(pMID->m_bDrop, TRUE);

				PBYTE pData = pReq->m_pData;

				int   nSize = pData[1];

				UINT  uPtr  = 2;

				while( nSize >= 3 ) {

					WORD wCode = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

					BOOL fCount = (pData[uPtr+2]&128) ? TRUE : FALSE;

					int  nStep  = fCount ? 4 : 3;

					if( wCode ) {

						UINT uFind = AllocFault();

						if( uFind < NOTHING ) {

							// This is where we extract the fault data as
							// per the specifiation. If details are required,
							// we also queue a detail request and increment
							// our pending request counter so that we don't
							// publish the data until we have a reply.

							CFault *pWork = m_FaultWork + uFind;
						
							pWork->m_bSrc     = pMID->m_bDrop;
						
							pWork->m_wCode    = wCode;

							pWork->m_bSubcode = (pData[uPtr+2]&15);

							pWork->m_bFlags   = (0x80|(~pData[uPtr+2]&64)>>6);

							pWork->m_bRaw     = pData[uPtr+2];

							pWork->m_wCount   = fCount ? pData[uPtr+3] : 0;

							pWork->m_dwFirst  = 0;

							pWork->m_dwLast   = 0;

							pWork->m_uFetch   = 0;
						
							if( m_fGetDetail ) {

								pWork->m_uFetch = tryDetail;

								QueueRequest(pMID, reqDiagDetail, uFind);

								pMID->m_uDiagPend++;
								}
							}
						}
						 
					uPtr  += nStep;

					nSize -= nStep;
					}
				}
			}
		else {
			PBYTE pData = pReq->m_pData;

			int   nSize = pData[2] - 2;

			BYTE  bType = pData[4];

			// The type byte indicates what sort of frame this is,
			// per the CDL specification. See that document for a
			// clearer explanation. Note that we can't accept new
			// faults if we have details requests pending. Note
			// also that we set and clear a flag to ensure that
			// we don't update during multi-frame sequences, and
			// that we drop end frame if we don't see a start.

			if( bType == 0x80 ) {
		
				QueueRequest(pMID, reqDiags, 0);

				return;
				}

			if( bType == 0x00 || bType == 0x60 || bType == 0x70 ) {

				if( pMID->m_uDiagPend ) {

					QueueRequest(pMID, reqDiags, 0);

					return;
					}

				if( bType == 0x60 || bType == 0x70 ) {

					ClearFaults(pMID->m_bDrop, TRUE);
					}
				}

			if( bType == 0x60 ) {

				pMID->m_bFaultFlag |=  1;
				}

			if( bType == 0x30 ) {

				// If we get a closing frame of a multi-frame
				// sequence and we're not expecting it, drop the
				// frame and re-queue our request for later.

				if( !(pMID->m_bFaultFlag & 1) ) {

					QueueRequest(pMID, reqDiags, 0);

					return;
					}

				pMID->m_bFaultFlag &= ~1;
				}

			UINT uPtr = 5;

			while( nSize >= 4 ) {

				WORD wCode = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

				if( wCode ) {

					UINT uFind = NOTHING;
					
					if( bType == 0x00 ) {

						// If the type is 0x00 this is an update to an
						// existing fault so we try to find it first.
						
						uFind = FindFault(pMID->m_bDrop, wCode, TRUE);
						}

					if( uFind == NOTHING ) {

						uFind = AllocFault();
						}

					if( uFind < NOTHING ) {

						// This is where we extract the fault data as
						// per the specifiation. If details are required,
						// we also queue a detail request and increment
						// our pending request counter so that we don't
						// publish the data until we have a reply.

						CFault *pWork = m_FaultWork + uFind;

						pWork->m_bSrc     = pMID->m_bDrop;
						
						pWork->m_wCode    = wCode;

						pWork->m_bSubcode = (pData[uPtr+2]&15);

						pWork->m_bFlags   = (0x80 | ((~pData[uPtr+3]&2)>>1));

						pWork->m_bRaw     = pData[uPtr+2];

						pWork->m_wCount   = 0;

						pWork->m_dwFirst  = 0;

						pWork->m_dwLast   = 0;

						pWork->m_uFetch   = 0;
						
						if( m_fGetDetail ) {

							pWork->m_uFetch = tryDetail;

							QueueRequest(pMID, reqDiagDetail, uFind);

							pMID->m_uDiagPend++;
							}
						}
					}

				uPtr  += 4;

				nSize -= 4;
				}
			}

		// We always queue another request before leaving
		// as these sit in the pending queue ready to accept
		// unsolicited incoming fault frames.
	
		QueueRequest(pMID, reqDiags, 0);

		m_fFaultNew = TRUE;

		return;
		}

	ClearFaults(pMID->m_bDrop, TRUE);
	}

void CCatLinkDriver::ProcessEventsReply(CMID *pMID, CReq *pReq)
{
	if( IsActiveMID(pMID->m_bDrop) ) {

		PBYTE pData = pReq->m_pData;

		int   nSize = pData[2] - 2;

		BYTE  bType = pData[4];

		// The type byte indicates what sort of frame this is,
		// per the CDL specification. See that document for a
		// clearer explanation. Note that we can't accept new
		// faults if we have details requests pending. Note
		// also that we set and clear a flag to ensure that
		// we don't update during multi-frame sequences, and
		// that we drop end frame if we don't see a start.

		if( bType == 0x80 ) {
	
			QueueRequest(pMID, reqEvents, 0);

			return;
			}

		if( bType == 0x60 || bType == 0x70 ) {

			if( pMID->m_uEventPend ) {

				QueueRequest(pMID, reqEvents, 0);

				return;
				}

			ClearFaults(pMID->m_bDrop, FALSE);
			}

		if( bType == 0x60 ) {

			pMID->m_bFaultFlag |=  2;
			}

		if( bType == 0x30 ) {

			if( !(pMID->m_bFaultFlag & 2) ) {

				QueueRequest(pMID, reqDiags, 0);

				return;
				}

			pMID->m_bFaultFlag &= ~2;
			}

		UINT uPtr = 5;

		while( nSize >= 3 ) {

			WORD wCode = MAKEWORD(pData[uPtr+1], pData[uPtr+0]);

			if( wCode ) {

				UINT uFind = NOTHING;
				
				if( bType == 0x00 ) {
					
					// If the type is 0x00 this is an update to an
					// existing fault so we try to find it first.
					
					uFind = FindFault(pMID->m_bDrop, wCode, FALSE);
					}

				if( uFind == NOTHING ) {

					uFind = AllocFault();
					}

				if( uFind < NOTHING ) {

					// This is where we extract the fault data as
					// per the specifiation. If details are required,
					// we also queue a detail request and increment
					// our pending request counter so that we don't
					// publish the data until we have a reply.

					CFault *pWork = m_FaultWork + uFind;

					pWork->m_bSrc     = pMID->m_bDrop;
					
					pWork->m_wCode    = wCode;

					pWork->m_bSubcode = (pData[uPtr+2]>>5);

					pWork->m_bFlags   = (0x00 | ((~pData[uPtr+2]&2)>>1));

					pWork->m_bRaw     = pData[uPtr+2];

					pWork->m_wCount   = 0;

					pWork->m_dwFirst  = 0;

					pWork->m_dwLast   = 0;

					pWork->m_uFetch   = 0;

					if( m_fGetDetail ) {
			
						pWork->m_uFetch = tryDetail;

						QueueRequest(pMID, reqEventDetail, uFind);

						pMID->m_uEventPend++;
						}
					}
				}

			uPtr  += 3;

			nSize -= 3;
			}

		// We always queue another request before leaving
		// as these sit in the pending queue ready to accept
		// unsolicited incoming fault frames.

		QueueRequest(pMID, reqEvents, 0);

		m_fFaultNew = TRUE;

		return;
		}

	ClearFaults(pMID->m_bDrop, FALSE);
	}

void CCatLinkDriver::ProcessDiagDetailReply(CMID *pMID, CReq *pReq)
{
	UINT    uSlot = pReq->m_wSlot;

	CFault *pWork = m_FaultWork + uSlot;

	if( pReq->m_uTime ) {

		if( pWork->m_uFetch ) {

			// If the reply is valid and expected, update the
			// fault details and decrement the request counter,
			// perhaps allowing the data to be published.

			PBYTE pData = pReq->m_pData;

			if( m_fAltFaults ) {

				// Adjust times from hours to seconds in alt fault
				// mode so that the data stays the same as before.

				pWork->m_wCount  = pData[5];

				pWork->m_dwFirst = MotorToHost(PWORD(pData+6)[0]) * 60 * 60;

				pWork->m_dwLast  = MotorToHost(PWORD(pData+6)[1]) * 60 * 60;
				}
			else {
				pWork->m_wCount  = pData[7];

				pWork->m_dwFirst = IntelToHost(PDWORD(pData+9)[0]);

				pWork->m_dwLast  = IntelToHost(PDWORD(pData+9)[1]);
				}

			pWork->m_uFetch  = 0;

			pMID->m_uDiagPend--;
			}
		}
	else {
		// Retry failed requests up to a point, and then simply
		// store zeroes in the details fields and pretend that
		// the reply was okay so as to allow publication.

		if( --pWork->m_uFetch ) {

			QueueRequest(pMID, reqDiagDetail, pReq->m_wSlot);
			}
		else {
			pWork->m_wCount  = 0;

			pWork->m_dwFirst = 0;

			pWork->m_dwLast  = 0;

			pMID->m_uDiagPend--;
			}
		}
	}

void CCatLinkDriver::ProcessEventDetailReply(CMID *pMID, CReq *pReq)
{
	UINT    uSlot = pReq->m_wSlot;

	CFault *pWork = m_FaultWork + uSlot;

	if( pReq->m_uTime ) {

		if( pWork->m_uFetch ) {

			// If the reply is valid and expected, update the
			// fault details and decrement the request counter,
			// perhaps allowing the data to be published.

			PBYTE pData = pReq->m_pData;

			pWork->m_wCount  = pData[7];

			pWork->m_dwFirst = IntelToHost(PDWORD(pData+9)[0]);

			pWork->m_dwLast  = IntelToHost(PDWORD(pData+9)[1]);

			pWork->m_uFetch  = 0;

			pMID->m_uEventPend--;
			}
		}
	else {
		// Retry failed requests up to a point, and then simply
		// store zeroes in the details fields and pretend that
		// the reply was okay so as to allow publication.

		if( --pWork->m_uFetch ) {

			QueueRequest(pMID, reqEventDetail, pReq->m_wSlot);
			}
		else {
			pWork->m_wCount  = 0;

			pWork->m_dwFirst = 0;

			pWork->m_dwLast  = 0;

			pMID->m_uEventPend--;
			}
		}
	}

// Site Survey

BOOL CCatLinkDriver::FindSiteType(CSite &Site)
{
	// This is the logic for figuring out at which kind of site we
	// are installed. We look for well-known combinations of MID
	// and then build a map of available ECMs, GCS, and ICM modules
	// that we can access. The order in which we try each MID is
	// based upon the PID's coding as defined in catbase.

	if( CountMID(0x21) && CountMID(0x23) ) {

		if( CountMID(0x24)  ) {

			Site.m_uType = siteSlave2;

			Site.m_bECM1 = 0x24;

			Site.m_bECM2 = 0x21;
			
			Site.m_bECM3 = 0x23;

			Site.m_bGSC  = FindGSC();

			Site.m_bICM1 = FindICM1();

			Site.m_bICM2 = FindICM2();

			return TRUE;
			}

		Site.m_uType = sitePort;

		Site.m_bECM1 = 0x21;

		Site.m_bECM2 = 0x23;

		return TRUE;
		}

	if( CountMID(0x22) && CountMID(0x25) ) {

		Site.m_uType = siteStarboard;

		Site.m_bECM1 = 0x22;

		Site.m_bECM2 = 0x25;

		return TRUE;
		}

	if( CountMID(0x24) && CountMID(0x26) ) {

		Site.m_uType = siteCenter;

		Site.m_bECM1 = 0x24;

		Site.m_bECM2 = 0x26;

		return TRUE;
		}

	if( CountMID(0x24) && CountMID(0x6D) && CountMID(0x6E) ) {

		Site.m_uType = siteG3600;

		Site.m_bECM1 = 0x24;

		Site.m_bGSC  = FindGSC();

		Site.m_bICM1 = 0x6D;

		Site.m_bICM2 = 0x6E;

		return TRUE;
		}

	if( CountMID(0x24) && CountMID(0x21) && CountMID(0x6F) ) {

		Site.m_uType = siteG3520C;

		Site.m_bECM1 = 0x24;

		Site.m_bECM2 = 0x21;

		Site.m_bGSC  = FindGSC();

		Site.m_bICM1 = 0x6F;

		Site.m_bICM2 = FindICM2();

		return TRUE;
		}

	if( CountMID(0x24) && CountMID(0x21) ) {

		Site.m_uType = siteSlave1;

		Site.m_bECM1 = 0x24;

		Site.m_bECM2 = 0x21;

		Site.m_bGSC  = FindGSC();

		Site.m_bICM1 = FindICM1();

		Site.m_bICM2 = FindICM2();

		return TRUE;
		}

	if( (Site.m_bECM1 = FindECM()) ) {

		Site.m_uType = siteStandard;

		Site.m_bGSC  = FindGSC();

		Site.m_bICM1 = FindICM1();

		Site.m_bICM2 = FindICM2();

		Site.m_bVIMS = FindVIMS();

		Site.m_bTran = FindTran();

		Site.m_bPay  = FindPay();

		Site.m_bDenox = FindDenox();

		Site.m_bImplement = FindImplement();

		return TRUE;
		}

	if( CountMID(0x58, 0x5F) ) {

		Site.m_uType = siteMechanical;

		Site.m_bGSC  = FindGSC();

		Site.m_bICM1 = FindICM1();

		Site.m_bICM2 = FindICM2();

		return TRUE;
		}
	
	return FALSE;
	}

void CCatLinkDriver::CreateStandardMIDs(void)
{
	for( UINT ecm = 0x0E; ecm <= 0x2F; ecm++ ) {

		if( GetMIDType(ecm) == midECM ) {

			CreateMID(ecm);
			}
		}

	for( UINT GSC = 0x58; GSC <= 0x5F; GSC++ ) {

		CreateMID(GSC);
		}
	}

BYTE CCatLinkDriver::GetMIDType(BYTE bDrop)
{
	if( bDrop >= 0x0E && bDrop <= 0x0F ) {

		return midECM;
		}

	if( bDrop >= 0x11 && bDrop <= 0x13 ) {

		return midECM;
		}

	if( bDrop == 0x1B ) {

		return midTran;
		}

	if( bDrop == 0x1F ) {

		return midTran;
		}

	if( bDrop >= 0x21 && bDrop <= 0x2B ) {

		return midECM;
		}

	if( bDrop == 0x2F ) {

		return midECM;
		}
	
	if( bDrop == 0x31 ) {

		return midVIMS;
		}

	if( bDrop == 0x4A ) {

		return midPay;
		}

	if( bDrop == 0x4C ) {

		return midDenox;
		}

	if( bDrop == 0x4F ) {

		return midTran;
		}

	if( bDrop == 0x51 ) {

		return midTran;
		}

	if( bDrop == 0x52 ) {

		return midImplement;
		}

	if( bDrop == 0x55 ) {

		return midECM;
		}

	if( bDrop == 0x57 ) {

		return midChassis;
		}

	if( bDrop >= 0x58 && bDrop <= 0x5F ) {

		return midGSC;
		}

	if( bDrop == 0x71 ) {

		return midTran;
		}

	if( bDrop == 0x92 ) {

		return midTran;
		}

	return midOther;
	}

UINT CCatLinkDriver::GetKeyPID(BYTE bType)
{
	// The key PID is the one used to poll for a given type
	// of MID when we've not heard anything. It is also never
	// taken off-scan so as to allow for quicker recovery.

	switch( bType ) {

		case midECM:

			return pidRPM;

		case midGSC:

			return pidVoltage;
		}

	return 0;
	}

BOOL CCatLinkDriver::IsKeyPID(UINT uName)
{
	switch( uName ) {

		case pidRPM:

		case pidVoltage:

			return TRUE;
		}

	return FALSE;
	}

BOOL CCatLinkDriver::IsActiveMID(BYTE bDrop)
{
	for( UINT n = 0; n < siteCount; n++ ) {

		if( bDrop == PBYTE(&m_Site.m_bGSC)[n] ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BYTE CCatLinkDriver::FindECM(void)
{
	// The list below is Doyle's prioritization of ECMs
	// if we should happen to find more than one on a site
	// that doesn't fit one of our well-known topologies.
	// Note that GetMIDType must return midECM for each
	// entry in this list, or it won't be recognized.

	static BYTE const ecm[] = {

		0x24,
		0x21, 0x22, 0x23,
		0x2B, 0x2A, 0x29, 0x28, 0x27, 0x26, 0x25,
		0x13, 0x12, 0x11,
		0x0F, 0x0E,
		0x2F,
		0x55

		};

	for( UINT i = 0; i <= elements(ecm); i++ ) {

		if( GetMIDType(ecm[i]) == midECM ) {

			if( CountMID(ecm[i]) ) {

				return ecm[i];
				}
			}
		}

	return 0;
	}

BYTE CCatLinkDriver::FindGSC(void)
{
	for( UINT GSC = 0x58; GSC <= 0x5F; GSC++ ) {

		if( CountMID(GSC) ) {

			return GSC;
			}
		}

	return 0;
	}

BYTE CCatLinkDriver::FindICM1(void)
{
	if( CountMID(0x6D)  ) {

		return 0x6D;
		}
	
	if( CountMID(0x6F)  ) {

		return 0x6F;
		}

	return 0;
	}

BYTE CCatLinkDriver::FindICM2(void)
{
	if( CountMID(0x6D)  ) {

		if( CountMID(0x6E) ) {

			return 0x6E;
			}

		return 0;
		}
	
	if( CountMID(0x6F) ) {

		if( CountMID(0x6B) ) {

			return 0x6B;
			}

		return 0;
		}

	return 0;
	}

BYTE CCatLinkDriver::FindVIMS(void)
{
	if( CountMID(0x31) ) {

		return 0x31;
		}

	return 0;
	}

BYTE CCatLinkDriver::FindTran(void)
{
	// MID 0x1B is most common, but there should only be one tran per system.

	static BYTE const tran[] = {

		0x1B, 0x1F,
		0x4F,
		0x51,
		0x71,
		0x92

		};

	for( UINT i = 0; i <= elements(tran); i++ ) {

		if( GetMIDType(tran[i]) == midTran ) {

			if( CountMID(tran[i]) ) {

				return tran[i];
				}
			}
		}

	return 0;
	}

BYTE CCatLinkDriver::FindPay(void)
{
	if( CountMID(0x4A) ) {

		return 0x4A;
		}

	return 0;
	}

BYTE CCatLinkDriver::FindDenox(void)
{
	if( CountMID(0x4C) ) {

		return 0x4C;
		}

	return 0;
	}

BYTE CCatLinkDriver::FindImplement(void)
{
	if( CountMID(0x52) ) {

		return 0x52;
		}

	return 0;
	}

BYTE CCatLinkDriver::FindChassis(void)
{
	if( CountMID(0x57) ) {

		return 0x57;
		}

	return 0;
	}

UINT CCatLinkDriver::CountMID(BYTE bDrop)
{
	return (m_bDrop[bDrop] == 2) ? 1 : 0;
	}

UINT CCatLinkDriver::CountMID(BYTE bFrom, BYTE bTo)
{
	UINT n = 0;

	while( bFrom <= bTo ) {

		n += CountMID(bFrom++);
		}

	return n;
	}

BYTE CCatLinkDriver::GetSource(CPID *pPID)
{
	return GetSource(pPID, pPID->m_bPref);
	}

BYTE CCatLinkDriver::GetSource(CPID *pPID, UINT uPref)
{
	// This function is used to find the source MID for a
	// given PID based upon its name and its preference
	// value. The exact sequence also depends upon the type
	// of site on which we are installed. Returning a value
	// of zero results in starting the scan again until
	// we eventually give up and take the PID off-scan.

	PCTXT pCode = GetPIDCode(pPID->m_uName);

	switch( pCode[0] ) {

		case 'G':

			return m_Site.m_bGSC;

		case 'V':

			if( m_Site.m_bVIMS ) {

				switch( pPID->m_bPref ) {

					case 0:  return m_Site.m_bVIMS;
					case 1:  return m_Site.m_bECM1;
					}
				
				return 0;
				}  

			return m_Site.m_bECM1;

		case 'A':

			if( (m_Site.m_uType == siteG3600) && ((pCode[0] == 'A') && (pCode[1] != '\0')) ) {

				UINT uIndex = ATOI(PTXT(pCode + 1));

				if( uIndex % 2 ) {

					return m_Site.m_bICM1;
					}

				return m_Site.m_bICM2;
				}

			if( m_Site.m_bICM1 && m_Site.m_bICM2 ) {

				switch( uPref ) {

					case 0: return m_Site.m_bICM1;
					case 1: return m_Site.m_bICM2;
					case 2: return m_Site.m_bECM1;
					}

				return 0;
				}

			// The idiom of bPref + !bSrc is used to skip
			// the first case by adding one to the switch
			// selection value when bSrc is equal to zero.

			switch( uPref + !m_Site.m_bICM1 ) {

				case 0: return m_Site.m_bICM1;
				case 1: return m_Site.m_bECM1;
				}

			return 0;

		case 'M':

			if( m_Site.m_bImplement ) {

				switch( pPID->m_bPref ) {

					case 0: return m_Site.m_bImplement;
					case 1: return m_Site.m_bECM1;
					case 2: return m_Site.m_bECM2;	
					case 3: return m_Site.m_bECM3;	
					}

				return 0;
				}

			// Fall-Thru

		case 'S':
			
			if( m_Site.m_bGSC ) {

				switch( pPID->m_bPref ) {

					case 0: return m_Site.m_bGSC;
					case 1: return m_Site.m_bECM1;
					case 2: return m_Site.m_bECM2;	
					case 3: return m_Site.m_bECM3;	
					}

				return 0;
				}

			// Fall-Thru

		case 'E':

			if( (m_Site.m_uType == siteG3520C) && ((pCode[0] == 'E') && (pCode[1] != '\0')) ) {

				UINT uIndex = ATOI(PTXT(pCode + 1));

				if( uIndex % 2 ) {

					return m_Site.m_bECM2;
					}

				return m_Site.m_bECM1;
				}

			switch( pPID->m_bPref ) {

				case 0: return m_Site.m_bECM1;
				case 1: return m_Site.m_bECM2;
				case 2: return m_Site.m_bECM3;
				}

			return 0;

		case 'I':

			if( (m_Site.m_uType == siteG3600) && ((pCode[0] == 'I') && (pCode[1] != '\0')) ) {

				UINT uIndex = ATOI(PTXT(pCode + 1));

				if( uIndex % 2 ) {

					return m_Site.m_bICM1;
					}

				return m_Site.m_bICM2;
				}

			switch( pPID->m_bPref ) {

				case 0: return m_Site.m_bICM1;
				case 1: return m_Site.m_bICM2;
				}

			return 0;

		case 'T':

			switch( pPID->m_bPref ) {

				case 0: return m_Site.m_bTran;
				case 1: return m_Site.m_bChassis;
				}

			return 0;

		case 'P':

			return m_Site.m_bPay;

		case 'D':

			return m_Site.m_bDenox;

		case 'C':

			return m_Site.m_bChassis;
	
		}

	return 0;
	}

UINT CCatLinkDriver::GetRetryCount(CPID *pPID)
{
	// This returns the number of times to retry a PID read
	// operation. We don't try more than once if we are trying
	// to bring a PID back on-scan. Note that m_bLoop is only
	// cleared once a valid reply is received.

	if( pPID->m_bLoop >= tryLoops ) {

		return 1;
		}

	return tryRead;
	}

UINT CCatLinkDriver::GetRevertCount(CPID *pPID)
{
	// The revert count is how often we will attempt to get a PID
	// from the first preference MID if we're using something else
	// as the source. PIDs read from ICMs are never reverted as
	// they exist in only one module or the other. If we are in a
	// marine application, reversion is used to switch back to the
	// primary controller if it comes back on line. We only attempt
	// to revert for the rpm value, unles the rpm has returned to
	// the primary, in which case we revert the rest immediately.

	PCTXT pCode = GetPIDCode(pPID->m_uName);

	switch( m_Site.m_uType ) {

		case sitePort:
		case siteStarboard:
		case siteCenter:

			if( pPID->m_uName == pidRPM ) {

				return 32;
				}

			if( LookupPID(pidRPM)->m_bPref == 0 ) {

				switch( pCode[0] ) {

					case 'G':
					case 'E':

						return 1;
					}
				
				}

			return NOTHING;
		}

	switch( pCode[0] ) {

		case 'I':
		case 'G':
			return NOTHING;
		}

	return tryRevert;
	}

BOOL CCatLinkDriver::TryNextMID(CPID *pPID)
{
	// This function is called after a read request has
	// failed to see if we should switch to the next possible
	// source MID. It contains special logic to deal with
	// reversion by ensuring that no retries are applied
	// when trying an inactive first preference.

	pPID->m_bOkay = 0;

	if( pPID->m_bFail < 15 ) {

		if( ++pPID->m_bFail >= GetRetryCount(pPID) ) {

			pPID->m_bFail = 0;

			return TRUE;
			}

		return FALSE;
		}
	
	return TRUE;
	}

// Fault List Management

void CCatLinkDriver::ClearFaults(BYTE bSrc, BOOL fDiag)
{
	// This function clears faults from a particular MID
	// from the working fault list. The gaps don't matter
	// as they will be sorted away when we publish.

	BYTE bMask = 0x80;

	BYTE bData = fDiag ? 0x80 : 0x00;

	for( UINT n = 0; n < m_uFaultHWM; n++ ) {

		if( m_FaultWork[n].m_bSrc == bSrc ) {

			if( (m_FaultWork[n].m_bFlags & bMask) == bData ) {

				memset(m_FaultWork + n, 0, sizeof(CFault));
				}
			}
		}
	}

UINT CCatLinkDriver::FindFault(BYTE bSrc, WORD wCode, BOOL fDiag)
{
	BYTE bMask = 0x80;

	BYTE bData = fDiag ? 0x80 : 0x00;

	for( UINT n = 0; n < m_uFaultHWM; n++ ) {

		if( m_FaultWork[n].m_bSrc == bSrc ) {

			if( m_FaultWork[n].m_wCode == wCode ) {

				if( (m_FaultWork[n].m_bFlags & bMask) == bData ) {

					return n;
					}
				}
			}
		}

	return NOTHING;
	}

UINT CCatLinkDriver::AllocFault(void)
{
	for( UINT n = 0; n < elements(m_FaultWork); n++ ) {

		if( m_FaultWork[n].m_bSrc ) {

			continue;
			}

		if( n >= m_uFaultHWM ) {
			
			m_uFaultHWM = n + 1;
			}

		return n;
		}

	return NOTHING;
	}

// PID List Management

CCatLinkDriver::CPID * CCatLinkDriver::LookupPID(UINT uName)
{
	// Find a given PID record, creating it if required. If
	// we create it and we have a site configuration figured
	// out, we can start the poll sequence for the PID.

	UINT   uSlot = PIDToSlot(uName);

	CPID * &pPID = m_pPID[uSlot];

	if( !pPID ) {

		pPID = new CPID;

		memset(pPID, 0, sizeof(CPID));

		pPID->m_uName = uName;

		if( m_Site.m_uType ) {

			UpdatePID(pPID, uSlot);
			}
		}

	return pPID;
	}

UINT CCatLinkDriver::PIDToSlot(UINT uName)
{
	// Convert a PID name to the slot number in our
	// PID table. We do this to allow quicker indexing
	// without having to search a linked list.

	if( uName >= 0x000000 && uName <= 0x0000FF ) {

		return 0x0000 + (uName - 0x000000);
		}

	if( uName >= 0xD00000 && uName <= 0xD00FFF ) {

		return 0x0100 + (uName - 0xD00000);
		}
	
	if( uName >= 0xD10000 && uName <= 0xD10FFF ) {

		return 0x1100 + (uName - 0xD10000);
		}

	if( uName >= 0x00F000 && uName <= 0x00FFFF ) {

		return 0x2100 + (uName - 0x00F000);
		}

	if( uName >= 0xD01000 && uName <= 0xD01FFF ) {

		return 0x3100 + (uName - 0xD01000);
		}

	return 0;
	}

// MID List Management

CCatLinkDriver::CMID * CCatLinkDriver::CreateMID(BYTE bDrop)
{
	// Lookup and if appropriate create an MID record. Set
	// the timeout to tigger straight away so that we will
	// kick off poll requests if we have not heard from
	// the MID before it and we have no site figured out.

	CMID *pMID = LookupMID(bDrop);

	if( pMID == NULL ) {

		pMID = new CMID;

		memset(pMID, 0, sizeof(CMID));

		pMID->m_bDrop      = bDrop;

		pMID->m_bType      = GetMIDType(bDrop);

		pMID->m_uTime      = SetTimeout(0);

		pMID->m_uPendLimit = GetPendLimit(bDrop);

		HostMaxIPL();

		AfxListAppend( m_pHead,
			       m_pTail,
			       pMID,
			       m_pNext,
			       m_pPrev
			       );
	
		HostMinIPL();
		}

	return pMID;
	}

CCatLinkDriver::CMID * CCatLinkDriver::LookupMID(BYTE bDrop)
{
	for( CMID *pMID = m_pHead; pMID; pMID = pMID->m_pNext ) {

		if( pMID->m_bDrop == bDrop ) {

			break;
			}
		}

	return pMID;
	}

// Request Control

void CCatLinkDriver::QueueRequest(CMID *pMID, UINT uType, UINT uSlot)
{
	// Add a request to the MID's send queue. This request
	// will be moved to the pending queue once it has been
	// transmitted, and then to the done queue once a reply
	// has been received or a timeout has occured.

	CReq *pReq = new CReq;

	memset(pReq, 0, sizeof(CReq));

	pReq->m_wType = WORD(uType);

	pReq->m_wSlot = WORD(uSlot);

	pReq->m_uTime = GetRequestTimeout(pMID->m_bDrop, pReq);

	pReq->m_pData = AllocRequestData (pReq);

	HostMaxIPL();

	AfxListAppend( pMID->m_pSendHead,
		       pMID->m_pSendTail,
		       pReq,
		       m_pNext,
		       m_pPrev
		       );
	
	HostMinIPL();
	}

UINT CCatLinkDriver::GetRequestTimeout(BYTE bDrop, CReq *pReq)
{
	// Find the timeout for a given request type. Note
	// diag and event requests do not timeout but just
	// sit in the pending queue waiting unsolicited data.

	if( bDrop == 0x55 ) {

		// Slow things down for the tester.

		return ToTicks(1200);
		}

	switch( pReq->m_wType ) {

		case reqDiagDetail:

		case reqEventDetail:

			return ToTicks(m_fAltFaults ? timeAltDetail : timeStdDetail);

		case reqDiags:

		case reqEvents:

			return NOTHING;
		}

	return ToTicks(timeRequest);
	}

PBYTE CCatLinkDriver::AllocRequestData(CReq *pReq)
{
	// Allocate data if the request needs a work buffer.

	switch( pReq->m_wType ) {

		case reqDiags:

		case reqEvents:

		case reqDiagDetail:

		case reqEventDetail:

			return new BYTE [ 100 ];
		}

	return NULL;
	}

UINT CCatLinkDriver::GetPendScore(CMID *pMID, CReq *pReq)
{
	// Get the pending score for a request. The pending scores
	// for all outstanding frames may not exceed a limit set
	// for the MID. This avoids flooding a unit will too much
	// data. Note that writes score more than 50% of the limit
	// to ensure that only one write is outstanding at a time
	// for a given MID. Note also that some frames do not have
	// a pend score, as they either do not send anything, or
	// they do not expect to have a reply.

	switch( pReq->m_wType ) {

		case reqRead:

			return 1;
		
		case reqWrite:

			return pMID->m_uPendLimit / 2 + 1;
			
		case reqDiagPoll:

		case reqEventPoll:

			return 0;

		case reqDiags:

		case reqEvents:

			return 0;

		case reqDiagDetail:

		case reqEventDetail:

			return 1;

		}
	
	return 1;
	}

UINT CCatLinkDriver::GetPendLimit(BYTE bDrop)
{
	// Older ITSM modules have small queues! And we
	// reserve one slot if we're using alt faults.

	UINT uAdjust = m_fAltFaults ? 1 : 0;

	switch( bDrop ) {

		case 0x0E:

			return 1;

		case 0x58:
		case 0x59:
		case 0x5A:
		case 0x5B:
		case 0x5C:
		case 0x5D:
		case 0x5E:
		case 0x5F:

			return 12 - uAdjust;

		case 0x6B:
		case 0x6F:
			
			return 4 - uAdjust;
		}

	return m_uStdPend - uAdjust;
	}

BOOL CCatLinkDriver::GetReplyFlag(CReq *pReq)
{
	// Return whether a request expects to see a reply. If no
	// reply is expected, the request will be moved straight to
	// the done queue by means of an immediate timeout so that
	// it can be removed and deleted.

	switch( pReq->m_wType ) {
		
		case reqDiagPoll:

		case reqEventPoll:

			return FALSE;
		}
	
	return TRUE;
	}

void CCatLinkDriver::CheckRequestTimeouts(CMID *pMID, UINT uTime)
{
	CReq *pReq = pMID->m_pPendHead;
	
	while( pReq ) {

		CReq *pNext = pReq->m_pNext;

		if( Timeout(pReq->m_uTime, uTime) ) {

			SetRequestDone(pMID, pReq, FALSE);
			}

		pReq = pNext;
		}
	}

void CCatLinkDriver::SetRequestDone(BYTE bDrop, CReq *pReq, BOOL fOkay)
{
	SetRequestDone(LookupMID(bDrop), pReq, fOkay);
	}

void CCatLinkDriver::SetRequestDone(CMID *pMID, CReq *pReq, BOOL fOkay)
{
	if( pMID ) {

		// When a request completes, move it from the pending
		// queue to the done queue, subtract any pending score
		// from the MID's score and indicate via the m_uTime
		// member whether the request was sucessful.

		AfxListRemove( pMID->m_pPendHead,
			       pMID->m_pPendTail,
			       pReq,
			       m_pNext,
			       m_pPrev
			       );

	
		AfxListAppend( pMID->m_pDoneHead,
			       pMID->m_pDoneTail,
			       pReq,
			       m_pNext,
			       m_pPrev
			       );
		       
		UINT uPend          = GetPendScore(pMID, pReq);

		pReq->m_uTime       = fOkay ? 1 : 0;

		pMID->m_uPendCount -= uPend;
		}
	}

// Request Location

CCatLinkDriver::CReq * CCatLinkDriver::FindRequest(BYTE bDrop, WORD wType, WORD wSlot)
{
	CMID *pMID = LookupMID(bDrop);

	if( pMID ) {

		return FindRequest(pMID, wType, wSlot);
		}

	return NULL;
	}

CCatLinkDriver::CReq * CCatLinkDriver::FindRequest(CMID *pMID, WORD wType, WORD wSlot)
{
	// Find the request frame associated with a given MID,
	// type and slot number. This is used to match replies
	// to requests for subsequent processing.

	CReq *pReq = pMID->m_pPendHead;
	
	while( pReq ) {

		if( pReq->m_wType == wType ) {

			if( pReq->m_wSlot == wSlot ) {

				return pReq;
				}
			}

		pReq = pReq->m_pNext;
		}

	return NULL;
	}

// Request Buidling

UINT CCatLinkDriver::BuildRequest(PBYTE pData, BYTE bDrop, UINT uType, UINT uSlot)
{
	switch( uType ) {

		case reqRead:

			return BuildReadRequest(pData, bDrop, uSlot);

		case reqWrite:

			return BuildWriteRequest(pData, bDrop, uSlot);

		case reqDiags:

			return NOTHING;

		case reqEvents:

			return NOTHING;

		case reqDiagPoll:

			return BuildDiagPollRequest(pData, bDrop, uSlot);

		case reqEventPoll:

			return BuildEventPollRequest(pData, bDrop, uSlot);

		case reqDiagDetail:

			return BuildDiagDetailRequest(pData, bDrop, uSlot);

		case reqEventDetail:

			return BuildEventDetailRequest(pData, bDrop, uSlot);
		}

	return 0;
	}

UINT CCatLinkDriver::BuildReadRequest(PBYTE pData, BYTE bDrop, UINT uSlot)
{
	// See CDL specificion for frame details.

	CPID *pPID = m_pPID[uSlot];

	UINT uName = pPID->m_uName;

	pData[0] = m_bSelf;

	pData[1] = bDrop;

	if( !(uName & 0xFFFFFF00) ) {

		pData[2] = 0x20;

		SetNameBytes(pData + 3, uName, 1);

		return 4;
		}

	if( !(uName & 0xFFFF0000) ) {

		pData[2] = 0x60;

		SetNameBytes(pData + 3, uName, 2);

		return 5;
		}

	if( !(uName & 0xFF000000) ) {

		pData[2] = 0xCF;

		pData[3] = 0x00;

		SetNameBytes(pData + 4, uName, 3);

		return 7;
		}

	return 0;
	}

UINT CCatLinkDriver::BuildWriteRequest(PBYTE pData, BYTE bDrop, UINT uSlot)
{
	// See CDL specification for frame details.

	CPID *pPID = m_pPID[uSlot];

	UINT uName = pPID->m_uName;

	if( m_fWrites && IsWritablePID(uName) ) {

		pData[0] = m_bSelf;

		pData[1] = bDrop;

		UINT uData    = pPID->m_Write;

		UINT uNameLen = GetNameLen(uName);

		UINT uDataLen = GetDataLen(uName);

		UINT uNamePos = 4;

		UINT uDataPos = 4 + uNameLen;

		pData[2] = 0xB2;

		pData[3] = uNameLen + uDataLen;

		SetNameBytes(pData + uNamePos, uName, uNameLen);

		SetDataBytes(pData + uDataPos, uData, uDataLen);

		return 4 + pData[3];
		}

	return 0;
	}

UINT CCatLinkDriver::BuildDiagPollRequest(PBYTE pData, BYTE bDrop, UINT uSlot)
{
	// See CDL specification for frame details.

	if( m_fAltFaults ) {

		pData[0] = m_bSelf;

		pData[1] = bDrop;

		pData[2] = 0x20;

		pData[3] = 0x82;

		return 4;
		}
	else {
		pData[0] = m_bSelf;

		pData[1] = bDrop;

		pData[2] = 0x60;

		pData[3] = 0xFA;

		pData[4] = 0x0D;

		return 5;
		}
	}

UINT CCatLinkDriver::BuildEventPollRequest(PBYTE pData, BYTE bDrop, UINT uSlot)
{
	// See CDL specification for frame details.

	pData[0] = m_bSelf;

	pData[1] = bDrop;

	pData[2] = 0x60;

	pData[3] = 0xFA;

	pData[4] = 0x0E;

	return 5;
	}

UINT CCatLinkDriver::BuildDiagDetailRequest(PBYTE pData, BYTE bDrop, UINT uSlot)
{
	// See CDL specification for frame details.

	if( m_fAltFaults ) {

		CFault *pInfo = m_FaultWork + uSlot;

		pData[0] = m_bSelf;

		pData[1] = bDrop;

		pData[2] = 0x83;

		pData[3] = 0x03;

		pData[4] = HIBYTE(pInfo->m_wCode);

		pData[5] = LOBYTE(pInfo->m_wCode);

		pData[6] = ((pInfo->m_bRaw & 0x0F) | 0xC0);

		return 7;
		}
	else {
		CFault *pInfo = m_FaultWork + uSlot;

		pData[0] = m_bSelf;

		pData[1] = bDrop;

		pData[2] = 0xFA;

		pData[3] = 0x11;

		pData[4] = 0x04;

		pData[5] = HIBYTE(pInfo->m_wCode);

		pData[6] = LOBYTE(pInfo->m_wCode);

		pData[7] = (pInfo->m_bRaw & 0xFF);

		pData[8] = 0x02;

		return 9;
		}
	}

UINT CCatLinkDriver::BuildEventDetailRequest(PBYTE pData, BYTE bDrop, UINT uSlot)
{
	// See CDL specification for frame details.

	CFault *pInfo = m_FaultWork + uSlot;

	pData[0] = m_bSelf;

	pData[1] = bDrop;

	pData[2] = 0xFA;

	pData[3] = 0x0F;

	pData[4] = 0x04;

	pData[5] = HIBYTE(pInfo->m_wCode);

	pData[6] = LOBYTE(pInfo->m_wCode);

	pData[7] = (pInfo->m_bRaw & 0xF0);

	pData[8] = 0x02;

	return 9;
	}

// Implementation

BOOL CCatLinkDriver::Timeout(UINT uTime)
{
	return Timeout(uTime, GetTickCount());
	}

BOOL CCatLinkDriver::Timeout(UINT uTime, UINT uNow)
{
	return uTime < NOTHING && int(uTime - uNow) < 0;
	}

UINT CCatLinkDriver::SetTimeout(UINT uTime)
{
	return GetTickCount() + ToTicks(uTime);
	}

// Helpers

BOOL CCatLinkDriver::IsGSC(BYTE bSrc)
{
	switch(bSrc) {

		case 0x58:
		case 0x59:
		case 0x5A:
		case 0x5B:
		case 0x5C:
		case 0x5D:
		case 0x5E:
		case 0x5F:

			return TRUE;
		}

	return FALSE;
	}

// Fault Sorting

int CCatLinkDriver::SortFunc(PCVOID p1, PCVOID p2)
{
	// This function is used to sort fault data before it is
	// published to the user. Active status is given the highest
	// weight, followed by time of last activation, source MID
	// and finally the fault code itself. This is a per what
	// is displayed on CAT's own operator panels.

	CFault *f1 = (CFault *) p1;

	CFault *f2 = (CFault *) p2;

	BYTE    a1 = (f1->m_bFlags & 0x01);
	
	BYTE    a2 = (f2->m_bFlags & 0x01);

	////////

	if( a1 < a2 ) return +1;

	if( a1 > a2 ) return -1;

	////////

	if( f1->m_dwLast < f2->m_dwLast ) return +1;
	
	if( f1->m_dwLast > f2->m_dwLast ) return -1;

	////////

	if( f1->m_bSrc < f2->m_bSrc ) return -1;

	if( f1->m_bSrc > f2->m_bSrc ) return +1;

	////////

	if( f1->m_wCode < f2->m_wCode ) return -1;

	if( f1->m_wCode > f2->m_wCode ) return +1;

	////////

	return 0;
	}

// Debug

void CCatLinkDriver::ShowSiteInfo(void)
{
	AfxTrace( "Site %u %2.2X %2.2X %2.2X %2.2X %2.2X %2.2X\n",
		  m_Site.m_uType,
		  m_Site.m_bECM1,
		  m_Site.m_bECM2,
		  m_Site.m_bECM3,
		  m_Site.m_bGSC,
		  m_Site.m_bICM1,
		  m_Site.m_bICM2
		  );
	}

void CCatLinkDriver::ShowFaults(void)
{
	AfxTrace("\nFaults Updated:\n");

	for( UINT n = 0; n < m_uFaultHWM; n++ ) {

		if( m_FaultWork[n].m_bSrc ) {

			AfxTrace( "  %2.2X %c%4u.%u(%u)[%u,%u,%u]\n",
				  (m_FaultWork[n].m_bSrc),
				  (m_FaultWork[n].m_bFlags & 0x80) ? 'D' : 'E',
				  (m_FaultWork[n].m_wCode),
				  (m_FaultWork[n].m_bSubcode),
				  (m_FaultWork[n].m_bFlags & 3),
				  (m_FaultWork[n].m_wCount),
				  (m_FaultWork[n].m_dwFirst),
				  (m_FaultWork[n].m_dwLast)
				  );
			}
		}
	}

void CCatLinkDriver::DumpQueue(CReq * &pHead)
{
	static CReq *pList[256];

	UINT uCount = 0;

	HostMaxIPL();

	CReq *pScan = pHead;

	while( pScan ) {

		pList[uCount++] = pScan;

		pScan = pScan->m_pNext;
		}

	HostMinIPL();

	AfxTrace(" %u ", uCount);

	for( UINT n = 0; n < uCount; n++ ) {

		//CPID * pPID = m_pPID[pList[n]->m_wSlot];

		//AfxTrace("%2.2x/", pPID->m_bSrc);
		//AfxTrace("%8.8x ", pPID->m_uName);
		}
	}

// End of File
