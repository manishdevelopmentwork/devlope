
#include "Intern.hpp"

#include "USBPortList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "USBPortItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Port List
//

// Dynamic Class

AfxImplementDynamicClass(CUSBPortList, CItemIndexList);

// Constructor

CUSBPortList::CUSBPortList(void)
{
	}

// Destructor

CUSBPortList::~CUSBPortList(void)
{
	}

// Item Location

CUSBPortItem * CUSBPortList::GetItem(INDEX Index) const
{
	return (CUSBPortItem *) CItemIndexList::GetItem(Index);
	}

CUSBPortItem * CUSBPortList::GetItem(UINT uPos) const
{
	return (CUSBPortItem *) CItemIndexList::GetItem(uPos);
	}

// Item Search

CUSBPortItem * CUSBPortList::FindItem(UINT uType)
{
	INDEX Index = GetHead();

	while( !Failed(Index) ) {

		CUSBPortItem *pItem= GetItem(Index);

		if( pItem->GetType() == uType ) {

			return pItem;
			}

		GetNext(Index);
		}

	return NULL;
	}

// End of File
