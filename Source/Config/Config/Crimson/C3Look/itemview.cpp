
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Proxy View Window
//

// Runtime Class

AfxImplementRuntimeClass(CItemViewWnd, CViewWnd);

// Constructor

CItemViewWnd::CItemViewWnd(UINT uType, UINT uFlags)
{
	m_uType   = uType;

	m_uFlags  = uFlags;

	m_pView   = NULL;

	m_pTool   = NULL;

	m_fNull   = FALSE;

	m_pParent = NULL;

	m_Class   = NULL;
	}

// Destructor

CItemViewWnd::~CItemViewWnd(void)
{
	}

// IUnknown

HRESULT CItemViewWnd::QueryInterface(REFIID iid, void **ppObject)
{
	return E_NOINTERFACE;
	}

ULONG CItemViewWnd::AddRef(void)
{
	return 1;
	}

ULONG CItemViewWnd::Release(void)
{
	return 1;
	}

// IUpdate

HRESULT CItemViewWnd::ItemUpdated(CItem *pItem, UINT uType)
{
	if( uType == updateContents ) {

		if( m_pItem == NULL || m_pItem == pItem ) {

			PostMessage(WM_AFX_COMMAND, IDM_VIEW_REFRESH);
			}
		}

	if( uType == updateRename ) {

		if( m_uFlags & viewCaption ) {

			if( pItem == m_pItem ) {

				Invalidate(TRUE);
				}
			}
		}

	return S_OK;
	}

// Attributes

CViewWnd * CItemViewWnd::GetView(void) const
{
	return m_pView;
	}

// Operations

void CItemViewWnd::Refresh(void)
{
	OnAttach();
	}

void CItemViewWnd::Purge(void)
{
	CString Nav = m_System.GetNavPos();

	INDEX Index = m_Cache.GetHead();

	BOOL  fDone = FALSE;

	while( !m_Cache.Failed(Index) ) {

		CViewWnd *pView = m_Cache.GetData(Index);

		CItem    *pItem = pView->GetItem();

		CString   Nav   = pView->GetNavPos();

		if( pView == m_pView ) {

			fDone = TRUE;
			}

		pView->DestroyWindow(TRUE);

		pView = pItem->CreateView(m_uType);

		pView->Attach(pItem);

		pView->Create( WS_CHILD | WS_CLIPCHILDREN,
			       GetViewRect(),
			       ThisObject, IDVIEW, NULL
			       );

		pView->Navigate(Nav);

		m_Cache.SetData(Index, pView);

		m_Cache.GetNext(Index);
		}

	if( !fDone ) {

		m_pView->DestroyWindow(TRUE);
		}

	m_pView = NULL;

	OnAttach();

	OnSetFocus(GetParent());
	}

void CItemViewWnd::SetFlag(UINT uFlag, BOOL fSet)
{
	UINT uFlags;

	if( fSet ) {

		uFlags = m_uFlags |  uFlag;
		}
	else
		uFlags = m_uFlags & ~uFlag;

	if( m_uFlags - uFlags ) {

		m_uFlags = uFlags;

		if( uFlag & viewCaption ) {

			if( m_pView ) {

				m_pView->MoveWindow(GetViewRect(), TRUE);
				}

			Invalidate(TRUE);

			return;
			}

		AfxAssert(FALSE);
		}
	}

void CItemViewWnd::SetPreNav(CString PreNav)
{
	m_PreNav = PreNav;
	}

void CItemViewWnd::EndPreNav(BOOL fExec)
{
	if( fExec ) {
		
		if( !m_PreNav.IsEmpty() ) {

			Navigate(m_PreNav);
			}
		}

	m_PreNav.Empty();
	}

// Overribables

void CItemViewWnd::OnAttach(void)
{
	if( m_hWnd ) {

		CViewWnd *pPrev = m_pView;

		CLASS    Viewer = GetViewClass(pPrev);

		m_fNull         = FALSE;

		if( m_uFlags & viewCache ) {

			if( !m_pItem ) {

				CreateView();
				}
			else {
				INDEX Index = m_Cache.FindName(m_pItem);

				if( !m_Cache.Failed(Index) ) {

					m_pView = m_Cache.GetData(Index);
					}
				else
					CreateView();
				}

			if( !pPrev ) {

				if( m_pView ) {

					m_pView->SetCurrent(IsCurrent());

					m_pView->ShowWindow(SW_SHOWNA);
					}
				}
			else {
				if( m_pView == pPrev ) {

					AfxAssert(m_uFlags & viewMerge);
					}
				else {
					if( m_pView ) {

						m_pView->SetCurrent(IsCurrent());

						m_pView->ShowWindow(SW_SHOWNA);
						}
					
					pPrev->ShowWindow(SW_HIDE);

					pPrev->SendMessage(WM_CANCELMODE);

					pPrev->SetCurrent(FALSE);
					}
				}
			}
		else {
			if( !m_pItem ) {

				CreateView();
				}
			else {
				if( IsItemPrivate() ) {

					m_pView = New CPrivateViewWnd;

					m_pView->Create( WS_CHILD | WS_CLIPCHILDREN,
						         GetViewRect(),
						         ThisObject, IDVIEW, NULL
						         );
					}
				else {
					CLASS Class = AfxPointerClass(m_pItem);

					INDEX Index = m_Cache.FindName(Class);

					if( !m_Cache.Failed(Index) ) {
				
						m_pView = m_Cache.GetData(Index);

						if( !m_pView->IsItem(m_pItem) ) {

							m_pView->Attach(m_pItem);
							}
						}
					else {
						CreateView();
					}
					}
				}

			if( m_pView != pPrev ) {

				if( m_pView ) {

					m_pView->SetCurrent(IsCurrent());

					m_pView->ShowWindow(SW_SHOWNA);
					}

				if( pPrev ) {

					if( pPrev->CanRecycle() ) {

						pPrev->ShowWindow(SW_HIDE);

						pPrev->SendMessage(WM_CANCELMODE);

						pPrev->SetCurrent(FALSE);
						}
					else
						pPrev->DestroyWindow(TRUE);
					}
				}
			}

		if( GetViewClass(m_pView) != Viewer ) {

			afxMainWnd->SendMessage(WM_UPDATEUI);
			}

		if( !pPrev || !m_pView ) {

			Invalidate(TRUE);
			}

		ShowInfo();
		}
	}

BOOL CItemViewWnd::OnPreDetach(void)
{
	return TRUE;
	}

void CItemViewWnd::OnDetach(void)
{
	}

CString CItemViewWnd::OnGetNavPos(void)
{
	if( m_pView ) {

		return m_pView->GetNavPos();
		}

	if( m_pItem ) {

		return m_pItem->GetFixedPath();
		}

	return L"";
	}

BOOL CItemViewWnd::OnNavigate(CString const &Nav)
{
	if( m_pView ) {

		return m_pView->Navigate(Nav);
		}

	return TRUE;
	}

void CItemViewWnd::OnExec(CCmd *pCmd)
{
	if( m_pView ) {

		m_pView->ExecCmd(pCmd);
		}
	}

void CItemViewWnd::OnUndo(CCmd *pCmd)
{
	if( m_pView ) {

		m_pView->UndoCmd(pCmd);
		}
	}

// Routing Control

BOOL CItemViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	UINT uCode = GetRoutingCode(Message);

	BOOL fAll  = (LOBYTE(uCode) == RC_ALL);

	if( Message.message == WM_AFX_COMMAND ) {

		if( Message.wParam == IDM_VIEW_REFRESH ) {

			ShowInfo();
			}
		}

	if( uCode & RC_BCAST ) {

		if( !(m_uFlags & viewCache) ) {

			for( INDEX Index = m_Cache.GetHead(); !m_Cache.Failed(Index); m_Cache.GetNext(Index) ) {

				CWnd *pWnd = m_Cache.GetData(Index);

				if( pWnd && pWnd != m_pView ) {

					pWnd->RouteMessage(Message, lResult);
					}
				}
			}
		}

	if( m_pView && m_pView->RouteMessage(Message, lResult) ) {

		if( !fAll ) {

			return TRUE;
			}
		}

	if( m_pTool && m_pTool->RouteMessage(Message, lResult) ) {

		if( !fAll ) {

			return TRUE;
			}
		}

	if( CWnd::OnRouteMessage(Message, lResult) ) {

		if( !fAll ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Interface Control

void CItemViewWnd::OnUpdateInterface(void)
{
	if( m_pTool ) {

		m_pTool->AddGadget(New CTextGadget  (1, 0,   textBold));

		m_pTool->AddGadget(New CTextGadget  (2, 200, textRight));

		m_pTool->AddGadget(New CButtonGadget(IDM_GO_PREV_2, 0x10000026, 1));

		m_pTool->AddGadget(New CButtonGadget(IDM_GO_NEXT_2, 0x10000025, 1));
		}
	}
		
// Message Map

AfxMessageMap(CItemViewWnd, CViewWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_SETCURRENT)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_GOINGIDLE)
	AfxDispatchMessage(WM_UPDATEUI)

	AfxDispatchCommand(IDM_VIEW_REFRESH, OnViewRefresh)

	AfxMessageEnd(CItemViewWnd)
	};

// Message Handlers

void CItemViewWnd::OnPostCreate(void)
{
	m_System.Bind(this);

	m_System.RegisterForUpdates(this);

	CreateView();

	if( m_pView ) {

		m_pView->SetCurrent(IsCurrent());

		m_pView->ShowWindow(SW_SHOWNA);
		}

	OnUpdateUI();
	}

void CItemViewWnd::OnCancelMode(void)
{
	if( m_pView ) {

		m_pView->SendMessage(WM_CANCELMODE);
		}
	}

void CItemViewWnd::OnSetCurrent(BOOL fCurrent)
{
	if( m_pView ) {

		m_pView->SetCurrent(fCurrent);
		}
	}

BOOL CItemViewWnd::OnEraseBkGnd(CDC &DC)
{
	if( m_fNull ) {

		CRect Rect = GetViewRect();

		DC.FillRect(Rect, afxBrush(TabFace));

		return TRUE;
		}

	return FALSE;
	}

void CItemViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	if( m_uFlags & viewEdge ) {

		DC.FrameRect(Rect, afxBrush(3dShadow));

		Rect--;
		}

	if( m_fNull ) {

		DC.SetBkMode(OPAQUE);

		DC.SetTextColor(afxColor(ButtonText));

		DC.SetBkColor(afxColor(TabFace));

		DC.Select(afxFont(Dialog));

		CStringArray List;

		if( !m_pItem ) {

			List.Append(CString(IDS_MULTIPLE_ITEMS));

			List.Append(CString(IDS_YOU_MAY_APPLY_1));

			List.Append(CString(IDS_YOU_MAY_APPLY_2));
			}
		else
			List.Append(CString(IDS_THIS_ITEM_HAS));

		int cy = DC.GetTextExtent(L"X").cy;

		int nn = List.GetCount();

		int ct = ((nn > 1) ? (4 * nn - (nn - 1)) : 2) * cy / 2;

		int yp = Rect.top + (2 * (Rect.cy() - ct) / 5);

		if( nn > 1 ) {

			DC.Select(afxFont(Bolder));
			}

		for( int n = 0; n < nn; n++ ) {

			int cx = DC.GetTextExtent(List[n]).cx;

			int xp = Rect.left + (Rect.cx() - cx) / 2;

			DC.TextOut(xp, yp, List[n]);

			yp += (n ? 3 : 4) * cy / 2;

			DC.Select(afxFont(Dialog));
			}

		DC.Deselect();
		}
	}

void CItemViewWnd::OnSetFocus(CWnd &Wnd)
{
	if( !m_pView ) {

		Invalidate(FALSE);

		return;
		}

	m_pView->SetFocus();
	}

void CItemViewWnd::OnKillFocus(CWnd &Wnd)
{
	if( !m_pView ) {

		Invalidate(FALSE);

		return;
		}
	}

void CItemViewWnd::OnSize(UINT uType, CSize Size)
{
	CRect Rect  = GetViewRect();

	INDEX Index = m_Cache.GetHead();

	while( !m_Cache.Failed(Index) ) {

		CViewWnd *pView = m_Cache.GetData(Index);

		if( pView != m_pView ) {

			pView->MoveWindow(Rect, FALSE);
			}

		m_Cache.GetNext(Index);
		}

	if( m_pView ) {

		m_pView->MoveWindow(Rect, TRUE);
		}

	if( m_pTool ) {

		CRect Rect  = GetClientRect() - 1;

		Rect.bottom = Rect.top + 20;

		m_pTool->MoveWindow(Rect, TRUE);
		}
	}

void CItemViewWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	if( uCode == VK_TAB ) {

		SendParentCommand(1, IDM_WINDOW_NEXT_PANE);
		}
	}

void CItemViewWnd::OnGoingIdle(void)
{
	if( m_pTool ) {

		m_pTool->PollGadgets();
		}
	}

void CItemViewWnd::OnUpdateUI(void)
{
	try {
		ClearToolbar();

		OnUpdateInterface();
		}
			
	catch(CException &) {
		
		UpdateToolbar();
		
		throw;
		}

	UpdateToolbar();
	}

// Command Handlers

BOOL CItemViewWnd::OnViewRefresh(UINT uID)
{
	if( !m_pItem ) {

		INDEX Index = m_Cache.GetHead();

		while( !m_Cache.Failed(Index) ) {

			CViewWnd *pView = m_Cache.GetData(Index);

			CItem    *pItem = pView->GetItem();

			CString   Nav   = pView->GetNavPos();

			pView->DestroyWindow(TRUE);

			pView = pItem->CreateView(m_uType);

			pView->Attach(pItem);

			pView->Create( WS_CHILD | WS_CLIPCHILDREN,
				       GetViewRect(),
				       ThisObject, IDVIEW, NULL
				       );

			pView->Navigate(Nav);

			m_Cache.SetData(Index, pView);

			m_Cache.GetNext(Index);
			}
		}
	else {
		CString Nav = m_System.GetNavPos();

		CViewWnd *pPrev = m_pView;

		CreateView();

		if( m_pView ) {

			m_pView->ShowWindow(SW_SHOWNA);

			m_pView->SetFocus();
			}

		if( pPrev ) {

			pPrev->DestroyWindow(TRUE);
			}

		m_System.Navigate(Nav);
		}

	ShowInfo();

	return TRUE;
	}

// Implementation

BOOL CItemViewWnd::CreateView(void)
{
	CViewWnd *pPrev = m_pView;

	if( m_pItem ) {

		if( IsItemPrivate() ) {

			m_pView = New CPrivateViewWnd;
			}
		else {
			m_pView = m_pItem->CreateView(m_uType);
			}

		if( m_pView ) {

			if( m_uFlags & viewMerge ) {

				if( GetViewClass(pPrev) == GetViewClass(m_pView) ) {

					delete m_pView;

					m_pView = pPrev;

					return FALSE;
					}
				}

			m_pView->Attach(m_pItem);

			m_pView->Create( WS_CHILD | WS_CLIPCHILDREN,
					 GetViewRect(),
					 ThisObject, IDVIEW, NULL
					 );

			if( !m_PreNav.IsEmpty() ) {

				m_pView->Navigate(m_PreNav);

				m_PreNav.Empty();
				}

			if( m_uFlags & viewCache ) {

				m_Cache.Insert(m_pItem, m_pView);
				}
			else {
				if( m_pView->CanRecycle() ) {

					CLASS Class = AfxPointerClass(m_pItem);

					m_Cache.Insert(Class, m_pView);
					}
				}

			m_pParent = m_pItem->GetParent();

			m_Class   = AfxPointerClass(m_pItem);

			return TRUE;
			}
		}

	m_pView   = NULL;

	m_fNull   = TRUE;

	m_pParent = NULL;

	m_Class   = NULL;

	Invalidate(TRUE);

	return TRUE;
	}

CRect CItemViewWnd::GetViewRect(void)
{
	CRect Rect = GetClientRect();

	if( m_uFlags & viewEdge ) {

		Rect--;
		}

	if( m_uFlags & viewCaption ) {

		Rect.top += 20;
		}

	return Rect;
	}

CLASS CItemViewWnd::GetViewClass(CWnd *pView)
{
	return pView ? AfxPointerClass(pView) : NULL;
	}

void CItemViewWnd::HideView(CViewWnd *pView)
{
	if( pView ) {

		pView->ShowWindow(SW_HIDE);

		pView->SendMessage(WM_CANCELMODE);

		pView->SetCurrent(FALSE);
		}
	}

BOOL CItemViewWnd::KillView(CViewWnd *pView)
{
	if( pView ) {

		if( pView->CanRecycle() ) {

			pView->ShowWindow(SW_HIDE);

			pView->SendMessage(WM_CANCELMODE);

			pView->SetCurrent(FALSE);
			}
		else {
			pView->DestroyWindow(TRUE);

			return TRUE;
			}
		}

	return FALSE;
	}

void CItemViewWnd::ClearToolbar(void)
{
	if( m_pTool ) {

		m_pTool->DestroyWindow(TRUE);

		m_pTool = NULL;
		}

	if( m_uFlags & viewCaption ) {

		m_pTool = New CToolbarWnd(barTight | barFlat | barDark);
		}
	}

BOOL CItemViewWnd::UpdateToolbar(void)
{
	if( m_pTool ) {

		CRect Rect  = GetClientRect() - 1;

		Rect.bottom = Rect.top + m_pTool->GetHeight();
		
		m_pTool->Create(Rect, ThisObject);

		m_pTool->PollGadgets();
	
		m_pTool->ShowWindow(SW_SHOW);

		return TRUE;
		}

	return FALSE;
	}

void CItemViewWnd::ShowInfo(void)
{
	if( m_pTool ) {

		CGadget *pText1 = m_pTool->GetGadget(1);

		CGadget *pText2 = m_pTool->GetGadget(2);

		if( m_pItem ) {

			CString Path = m_pItem->GetHumanPath();

			CString Text = m_pItem->GetItemOrdinal();

			if( m_pItem->GetPrivate() ) {

				Path += L" - ";

				Path += L"(";
				
				Path += CString(IDS_PRIVATE_ACCESS);

				Path += L")";
				}

			pText1->SetText(Path);

			pText2->SetText(Text);
			}
		else {
			pText1->SetText(CString(IDS_MULTIPLE_ITEMS_2));

			pText2->SetText(L"");
			}

		m_pTool->PollGadgets();
		}
	}

BOOL CItemViewWnd::IsItemPrivate(void)
{
	return m_pItem->GetDatabase()->IsPrivate() && m_pItem->GetPrivate();
	}

// End of File
