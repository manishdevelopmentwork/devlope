
#include "Intern.hpp"

#include "UserItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "CommsSystem.hpp"
#include "PersistManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// User Item
//

// Dynamic Class

AfxImplementDynamicClass(CUserItem, CCodedHost);

// Constructor

CUserItem::CUserItem(void)
{
	m_pRealName = NULL;

	m_pPassword = NULL;

	m_Force     = 0;

	m_Addr      = 0;

	memset(m_Rights, 0, sizeof(m_Rights));
}

// Initial Values

void CUserItem::SetInitValues(void)
{
	SetInitial(L"RealName", m_pRealName, L"\"John Doe\"");

	SetInitial(L"Password", m_pPassword, L"\"password\"");
}

// UI Update

void CUserItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);

		return;
	}
}

// Type Access

BOOL CUserItem::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"RealName" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	if( Tag == L"Password" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
	}

	return FALSE;
}

// Item Naming

CString CUserItem::GetHumanName(void) const
{
	return m_Name;
}

// Item Location

CCommsSystem * CUserItem::FindSystem(void) const
{
	return (CCommsSystem *) GetDatabase()->GetSystemItem();
}

// Persistance

void CUserItem::Init(void)
{
	m_Addr = PersistAllocate(64);

	CUIItem::Init();
}

void CUserItem::PostPaste(void)
{
	m_Addr = PersistAllocate(64);

	CUIItem::PostPaste();
}

void CUserItem::PostLoad(void)
{
	if( !m_Addr ) {

		m_Addr = PersistAllocate(64);

		CUIItem::PostLoad();

		return;
	}

	PersistRegister(m_Addr, 64);

	CUIItem::PostLoad();
}

void CUserItem::Kill(void)
{
	PersistFree(m_Addr, 64);

	CUIItem::Kill();
}

// Property Save Filter

BOOL CUserItem::SaveProp(CString const &Tag) const
{
	if( Tag.StartsWith(L"Right") ) {

		if( m_Rights[watoi(Tag.Mid(5))] ) {

			return TRUE;
		}

		return FALSE;
	}

	return CUIItem::SaveProp(Tag);
}

// Download Support

BOOL CUserItem::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(m_Addr);

	Init.AddText(m_Name);

	Init.AddItem(itemEncrypt, m_pRealName);

	Init.AddItem(itemEncrypt, m_pPassword);

	DWORD dwMask = 0;

	for( UINT n = 0; n < 32; n++ ) {

		if( m_Rights[n] ) {

			dwMask |= (1 << n);
		}
	}

	Init.AddLong(dwMask);

	Init.AddByte(BYTE(m_Force));

	return TRUE;
}

// Meta Data Creation

void CUserItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Name);

	Meta_AddVirtual(RealName);

	Meta_AddVirtual(Password);

	for( UINT n = 0; n < elements(m_Rights); n++ ) {

		Meta_Add(CPrintf(L"Right%2.2u", n), m_Rights[n], metaInteger);
	}

	Meta_AddInteger(Force);

	Meta_AddInteger(Addr);
}

// Memory Allocation

void CUserItem::PersistRegister(DWORD dwAddr, UINT uSize)
{
	FindSystem()->m_pPersist->Register(dwAddr, uSize);
}

DWORD CUserItem::PersistAllocate(UINT uSize)
{
	return FindSystem()->m_pPersist->Allocate(uSize);
}

void CUserItem::PersistFree(DWORD dwAddr, UINT uSize)
{
	FindSystem()->m_pPersist->Free(dwAddr, uSize);
}

// Implementation

void CUserItem::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = (GetDatabase()->GetSoftwareGroup() >= SW_GROUP_3B);

	pHost->EnableUI("Right12", fEnable);

	pHost->EnableUI("Right13", fEnable);

	pHost->EnableUI("Right14", fEnable);
}

// End of File
