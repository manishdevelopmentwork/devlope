
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3COMMS_HXX
	
#define	INCLUDE_C3COMMS_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hxx>

#include <c3core.hxx>

// End of File

#endif
