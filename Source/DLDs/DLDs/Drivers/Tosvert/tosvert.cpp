#include "intern.hpp"

#include "tosvert.hpp"

// Instantiator

INSTANTIATE(CTosvertSerialMasterDriver);

//////////////////////////////////////////////////////////////////////////
//
// Toshiba Tosvert Master Serial Driver
//
//

// Constructor

CTosvertSerialMasterDriver::CTosvertSerialMasterDriver(void)
{
	memset(m_bTx, 0, sizeof(m_bTx));

	memset(m_bRx, 0, sizeof(m_bRx)); 

	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;
	}

// Destructor

CTosvertSerialMasterDriver::~CTosvertSerialMasterDriver(void)
{
	}

// Configuration

void MCALL CTosvertSerialMasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CTosvertSerialMasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);		
	}
	
// Management

void MCALL CTosvertSerialMasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CTosvertSerialMasterDriver::Open(void)
{
	}

// Device

CCODE MCALL CTosvertSerialMasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bID    = GetByte(pData);
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CTosvertSerialMasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

      	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CTosvertSerialMasterDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 'A';
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrByteAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CTosvertSerialMasterDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable  = Addr.a.m_Table;
		     
	UINT uOffset = Addr.a.m_Offset;

	if( uTable == MISC_DATA_SPACE ) {

		return GetMiscData(pData, uOffset) ? uCount : CCODE_ERROR;
		}

	if( uOffset % 2 == 1 ) {
		
		return uCount;
		}

	if( !SetBank(uTable) ) {

		return CCODE_ERROR;
		}

	if( !SetAddress(uOffset) ) {

		return CCODE_ERROR;
		}

	BOOL fIncrement = TRUE;

	for( UINT u = 0; u < uCount; u++ ) {

		if( PutCmnd(READ_CMND_CODE, 0, fIncrement) ) {

			UINT &uData = (UINT &) pData[u];

			if( Recv(READ_CMND_CODE, uData) ) {

				continue;
				}
			}

		return u;
		}
	
	return uCount;
	}

CCODE MCALL CTosvertSerialMasterDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable  = Addr.a.m_Table;

	UINT uOffset = Addr.a.m_Offset;

	UINT uData   = 0;
	
	if( uTable == MISC_DATA_SPACE ) {

		return TRUE;
		}

	if( uOffset % 2 == 1 ) {
		
		return TRUE;
		}

	if( !SetBank(uTable) ) {

		return CCODE_ERROR;
		}

	if( !SetAddress(uOffset) ) {

		return CCODE_ERROR;
		}

	BOOL fIncrement = TRUE;

	for( UINT u = 0; u < uCount; u++ ) {

		if( PutCmnd(WRITE_CMND_CODE, pData[u], fIncrement) ) {

			if( Recv(WRITE_CMND_CODE, uData)) {

				continue;
				}
			}

		return u;
		}

	return uCount;
	}
 
// Implementation

void CTosvertSerialMasterDriver::AddByte(BYTE bByte)
{
	m_bTx[m_uPtr] = bByte;

	m_uPtr++;

	m_wCheck += bByte;
	}

BOOL CTosvertSerialMasterDriver::PutCmnd(BYTE bCmnd, UINT uData, BOOL fIncrement)
{
	BOOL fResult = TRUE;			

	UINT uASCIICheck = 0;

	char szTemp[100];

	UINT uIndex = 0;

	m_uPtr = 0;

	m_wCheck = 0;

	AddByte(START_CMND_CODE);
	
	AddByte(m_pHex[m_pCtx->m_bID / 10]);

	AddByte(m_pHex[m_pCtx->m_bID % 10]);

	AddByte(bCmnd);

	if( bCmnd != READ_CMND_CODE ) {

		CTEXT sFormat[] = "%X";
		
		SPrintf( szTemp, sFormat, uData );		

		UINT uStrLen = strlen(szTemp);

		for(uIndex = 0; uIndex < uStrLen; uIndex++) {

			AddByte(szTemp[uIndex]);
			}
		}

	if( fIncrement ) {

		AddByte(ADDR_INC_CODE);
		}

	AddByte(PRE_CHECKSUM_CODE);	

	FormChecksum(m_wCheck, uASCIICheck);		

	m_bTx[m_uPtr++] = HIBYTE(LOWORD(uASCIICheck));
	m_bTx[m_uPtr++] = LOBYTE(LOWORD(uASCIICheck));

	m_bTx[m_uPtr++] = END_CMND_CODE;	

	m_bTx[m_uPtr++] = TERMINATOR_CODE;	

	Send();

	return fResult;
	}

// Transport Layer

BOOL CTosvertSerialMasterDriver::Send(void)
{
	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CTosvertSerialMasterDriver::Recv(BYTE bCmnd, UINT &uData)
{
	UINT uState = INITIAL_STATE;

	UINT uPtr   = 0;

	UINT uByte  = 0;
		     
	UINT uTimer = 0;

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uByte = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}
		
		switch( uState ) {

			case INITIAL_STATE:

				if( uByte == START_CMND_CODE ) {

					m_bRx[0] = uByte;

					uPtr = 1;

					uState++;
					}
				break;
				
			case RX_DATA_STATE:

				m_bRx[uPtr++] = uByte;
					
				if( uByte == END_CMND_CODE ) {
					
					uState++;
					}
				break;
				
			case RX_TERM_STATE:

				if( uByte == TERMINATOR_CODE ) {
					
					m_RxLen = uPtr;

					m_bRx[uPtr] = 0;
					
					return CheckFrame(bCmnd, uData);
					}
				break;
			}
		}
		
	return FALSE;		
	}

BOOL CTosvertSerialMasterDriver::CheckFrame(BYTE bCmnd, UINT &uData)
{
    	BOOL fResult = TRUE;			

	UINT uIndex = 0;

	if( m_bRx[1 + 2] == ERROR_CMND_CODE ) {

		fResult = FALSE;

		atox(PSTR(m_bRx + 2 + 2), m_LastError );
		}

	if( fResult ) {

		if( m_pHex[m_pCtx->m_bID / 10] != m_bRx[1] ) {

			fResult = FALSE;
			}

		if( m_pHex[m_pCtx->m_bID % 10] != m_bRx[2] ) {

			fResult = FALSE;
			}
		}

	if( bCmnd != m_bRx[1 + 2] ) {

		fResult = FALSE;
		}
	
	if( ! ValidCheck() ) {

		fResult = FALSE;
		}

    	if( fResult && (m_RxLen >= MIN_REPLY_LENGTH + 2) ) {

		atox( PSTR(m_bRx + 2 + 2), uData );
		}

	char *pcTemp = strchr(PSTR(m_bRx), INVTR_TRIPPED_CODE);

	m_State = pcTemp != NULL;

    	return fResult;
	}

void CTosvertSerialMasterDriver::FormChecksum(UINT uCheck, UINT &uASCIICheck )
{
	m_wCheck = (0xFF & uCheck);

	uASCIICheck  = UINT( m_pHex[ LOBYTE(LOWORD(m_wCheck)) / 16 ] * 256 );
	uASCIICheck += UINT( m_pHex[ LOBYTE(LOWORD(m_wCheck)) % 16 ] );
	}

BOOL CTosvertSerialMasterDriver::ValidCheck(void)
{
	BOOL fResult = FALSE;			

	UINT uCheck = 0;

	UINT uASCIICheck;

	UINT uIndex = 0;

	BOOL fContinue = TRUE;

	while( fContinue ) {

		uCheck += PU2(m_bRx + uIndex)[0];

		if(( m_bRx[uIndex] == PRE_CHECKSUM_CODE ) || ( uIndex >= m_RxLen )) {

			fContinue = FALSE;
			}

		uIndex++;
		}

	FormChecksum(uCheck, uASCIICheck );	

	if(( m_bRx[m_RxLen - 3] == HIBYTE(LOWORD(uASCIICheck))) &&
	   ( m_bRx[m_RxLen - 2] == LOBYTE(LOWORD(uASCIICheck)))) {
		
		fResult = TRUE;
		}

	return fResult;
	}

BOOL CTosvertSerialMasterDriver::SetBank(UINT uType)
{
	UINT uThisBank = 0;

	UINT uData = 0;

	BOOL fResult = TRUE;

	BOOL fIncrement = FALSE;

	if( uType != m_CurrentBank ) {

		switch( uType ) {

			case RAM_DATA_SPACE:
				uThisBank = RAM_BANK; break;

			case INT_ROM_DATA_SPACE:
				uThisBank = INT_ROM_BANK; break;

			case EXT_ROM_DATA_SPACE:
				uThisBank = EXT_ROM_BANK; break;

			case OPTION_BUS_DATA_SPACE:
				uThisBank = OPTION_BUS_BANK; break;

			case EEPROM_DATA_SPACE:
				uThisBank = EEPROM_BANK; break;

			default:
			
				return FALSE;
				
			}

		if( PutCmnd(BANK_CMND_CODE, uThisBank, fIncrement) ) {

			if( Recv(BANK_CMND_CODE, uData)) {

				if( uData == uThisBank ) {

					m_CurrentBank = uType;
					}
				}
			else {
				fResult = FALSE;
				}
			}
		else {
			fResult = FALSE;
			}
		}

	return fResult;
	}

BOOL CTosvertSerialMasterDriver::SetAddress(UINT uOffset)
{
	BOOL fResult = TRUE;				

	UINT uData = 0;

	BOOL fIncrement = FALSE;

	if( !PutCmnd(ADDRESS_CMND_CODE, uOffset, fIncrement) ) {

		fResult = FALSE;
		}
	else {
		if( Recv(ADDRESS_CMND_CODE, uData) ) {

			if( uData != uOffset ) {

				fResult = FALSE;
				}
			}

		else {
			fResult = FALSE;
  			}
		}

	return fResult;
	}

void CTosvertSerialMasterDriver::atox(PSTR pText, UINT &uData)
{
	UINT uMask = 0x1000;

	uData = 0;

	for(UINT u = 0; u < 4; u++ ) {

		if(( pText[u] >= '0') && ( pText[u] <= '9')) {

			uData += (pText[u] - '0') * uMask;
			}
		else {
			if(( pText[u] >= 'A') && ( pText[u] <= 'F')) {

				uData += (pText[u] - 55) * uMask;
				}
			else
				break;
			}

		uMask /= 0x10;
		}
	}

BOOL CTosvertSerialMasterDriver::GetMiscData(PDWORD pData, UINT uOffset)
{
	switch( uOffset ) {

		case CURR_TRIPPED_STATE_MISC:

			pData[0] = m_State;

			return TRUE;

		case LAST_ERROR_CODE_MISC:

			pData[0] = m_LastError;

			return TRUE;

		}

	return FALSE;
	}

// End of file
