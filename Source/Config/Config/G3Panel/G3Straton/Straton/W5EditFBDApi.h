#if !defined(_W5EDITFBDAPI_H_)
#define _W5EDITFBDAPI_H_

#define W5EDITFBD_CLASSNAME		_T("W5EditFBD")

#define FBD_ADDITEM 17

#define FBD_SELECT          0
#define FBD_ADD_FB          1
#define FBD_ADD_VAR         2
#define FBD_ADD_COMMENT     3
#define FBD_ADD_ARC         4
#define FBD_ADD_CORNER      5
#define FBD_ADD_BREAK       6
#define FBD_ADD_LABEL       7
#define FBD_ADD_JUMP        8
#define FBD_ADD_LEFTRAIL    9
#define FBD_ADD_CONTACT     10
#define FBD_ADD_OR          11
#define FBD_ADD_COIL        12
#define FBD_ADD_RIGHTRAIL   13
#define FBD_PASTE           14
#define FBD_ADDRULE         15
#define FBD_ADD_CONDITION   16

//symetries for corner rotation
#define SYM_VERT          1
#define SYM_HORZ          2

#ifdef __cplusplus
extern "C" {
#endif

#ifdef W5EDITFBDDLL
	/* Build the DLL */
	#define W5EDITFBD_APICALL __declspec(dllexport)
#else
	#define W5EDITFBD_APICALL __declspec(dllimport)
#endif

#ifdef __cplusplus
}
#endif

#endif // !defined(_W5EDITFBDAPI_H_)
