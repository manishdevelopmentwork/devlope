
#include "Intern.hpp"

#include "UsbHostExtensibleCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB Framework
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Extensible Controller Driver
//

// Instantiator

IUsbHostControllerDriver * Create_UsbHostExtensibleController(void)
{
	CUsbHostExtensibleCtrl *p = New CUsbHostExtensibleCtrl;

	return p;
	}

// Constructor

CUsbHostExtensibleCtrl::CUsbHostExtensibleCtrl(void)
{
	m_pLowerDrv = NULL;

	m_pPorts    = NULL;

	m_uPorts    = 0;
	}

// IUsbDriver

BOOL CUsbHostExtensibleCtrl::Init(void)
{
	if( m_pLowerDrv && m_pLowerDrv->Init() ) {

		MakePorts();

		return true;
		}
	
	return false;
	}

BOOL CUsbHostExtensibleCtrl::Start(void)
{
	if( m_pLowerDrv && m_pLowerDrv->Start() ) {

		InitPorts();

		return true;
		}
	
	return false;
	}

BOOL CUsbHostExtensibleCtrl::Stop(void)
{
	if( m_pLowerDrv && m_pLowerDrv->Stop() ) {

		return true;
		}
	
	return false;
	}
		
// IUsbHostControllerDriver

void CUsbHostExtensibleCtrl::Poll(UINT uLapsed)
{
	PollPorts(uLapsed);
	}

void CUsbHostExtensibleCtrl::EnableEvents(BOOL fEnable)
{
	if( m_pLowerDrv ) {

		m_pLowerDrv->EnableEvents(fEnable);
		}
	}

BOOL CUsbHostExtensibleCtrl::ResetPort(UINT uPort)
{
	if( uPort < m_uPorts ) {
		
		CPort &Port = m_pPorts[uPort];

		Port.m_uFlags |= flagReqReset;

		return TRUE;
		}

	return FALSE;
	}

// Host Interface Drivers

IUsbHostInterfaceDriver * CUsbHostExtensibleCtrl::GetInterface(UINT iHost)
{
	return iHost == 0 ? m_pLowerDrv : NULL;
	}

// IUsbEvents

void CUsbHostExtensibleCtrl::OnBind(IUsbDriver *pDriver)
{
	IUsbHostInterfaceDriver *pHost = (IUsbHostInterfaceDriver *) pDriver;
		
	if( pHost->GetType() == usbXhci ) {

		if( !m_pLowerDrv ) {

			m_pLowerDrv = pHost;

			m_pLowerDrv->SetIndex(0);
			}
		}
	}

// IUsbHostInterfaceEvents

void CUsbHostExtensibleCtrl::OnPortConnect(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostExtensibleCtrl::OnPortConnect(nPort=%d)\n", Path.a.dwPort);*/

	if( Path.a.dwPort < m_uPorts ) {

		CPort &Port = m_pPorts[Path.a.dwPort];
		
		if( Port.m_uState == stateDetached ) {

			Port.m_uState = stateAttached;

			Port.m_uTimer = 100;
			}
		}
	}

void CUsbHostExtensibleCtrl::OnPortRemoval(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostExtensibleCtrl::OnPortRemoval(nPort=%d)\n", Path.a.dwPort);*/

	if( Path.a.dwPort < m_uPorts ) {

		CPort &Port = m_pPorts[Path.a.dwPort];
		
		if( Port.m_uState > stateDetached && Port.m_uState < stateRestart ) {

			Port.m_uState = stateDetached;

			OnDeviceRemoval(Path);
			}
		}
	}

void CUsbHostExtensibleCtrl::OnPortCurrent(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostExtensibleCtrl::OnPortCurrent(nPort=%d)\n", Path.a.dwPort);*/

	CPort &Port = m_pPorts[Path.a.dwPort];

	if( Port.m_uState != stateOverCurrent ) {

		m_pLowerDrv->SetPortPower (Path.a.dwPort, false);

		m_pLowerDrv->SetPortEnable(Path.a.dwPort, false);
		
		Port.m_uState = stateOverCurrent;

		Port.m_uTimer = 10000;
		}
	}

void CUsbHostExtensibleCtrl::OnPortReset(UsbPortPath &Path)
{
	/*AfxTrace("CUsbHostExtensibleCtrl::OnPortReset(nPort=%d)\n", Path.a.dwPort);*/

	if( Path.a.dwPort < m_uPorts ) {

		CPort &Port = m_pPorts[Path.a.dwPort];
		
		if( Port.m_uState == stateReset ) {

			Port.m_uState = stateCheckSpeed;
			}
		}
	}

// Ports

void CUsbHostExtensibleCtrl::MakePorts(void)
{
	m_uPorts = FindPorts();

	m_pPorts = New CPort[ m_uPorts ];

	memset(m_pPorts, 0, m_uPorts * sizeof(CPort));
	}

void CUsbHostExtensibleCtrl::InitPorts(void)
{
	for( UINT i = 0; i < m_uPorts; i ++ ) {

		m_pPorts[i].m_uState = stateDetached;
		}
	}

void CUsbHostExtensibleCtrl::KillPorts(void)
{
	if( m_pPorts ) {

		free(m_pPorts);

		m_pPorts = NULL;

		m_uPorts = 0;
		}
	}

UINT CUsbHostExtensibleCtrl::FindPorts(void)
{
	return m_pLowerDrv->GetPortCount();
	}

void CUsbHostExtensibleCtrl::PollPorts(UINT uLapsed)
{
	UsbPortPath Path = { { 0 } };

	Path.a.dwCtrl = m_iIndex;

	for( UINT i = 0; i < m_uPorts; i ++ ) {

		CPort &Port = m_pPorts[i];

		switch( Port.m_uState ) {

			case stateDetached:

				break;

			case stateAttached:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_uState = statePowered;
					}
				
				break;
				
			case statePowered:

				m_pLowerDrv->SetPortReset(i, true);

				Port.m_uState = stateReset;

				break;
				
			case stateReset:
				
				break;

			case stateCheckSpeed:

				Port.m_uSpeed = m_pLowerDrv->GetPortSpeed(i);

				Port.m_uTimer = 10;

				Port.m_uState = stateResetDelay;
				
				break;

			case stateResetDelay:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_uState = stateConnected;

					Path.a.dwPort = i;
				
					Path.a.dwHost = m_pLowerDrv->GetIndex();

					OnDeviceArrival(Path);
					}

				break;

			case stateConnected:

				if( Port.m_uFlags & flagReqReset ) {

					Port.m_uFlags &= ~flagReqReset;

					Path.a.dwPort = i;
				
					Path.a.dwHost = m_pLowerDrv->GetIndex();

					OnPortRemoval(Path);

					m_pLowerDrv->SetPortPower(i, false);

					Port.m_uState = stateRestart;

					Port.m_uTimer = 1000;
					}
				
				break;

			case stateRestart:
			case stateOverCurrent:

				if( !CheckTimer(Port.m_uTimer, uLapsed) ) {

					Port.m_uState = stateDetached;

					m_pLowerDrv->SetPortPower(i, true);
					}
				
				break;
			}
		}
	}

// End of File
