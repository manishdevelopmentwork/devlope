
#include "intern.hpp"

#include "image.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Application View
//

// Runtime Class

AfxImplementRuntimeClass(CCrimsonView, CSystemWnd);

// Static Data

UINT CCrimsonView::m_timerSave = AllocTimerID();

UINT CCrimsonView::m_uSequence = 0;

// Constructor

CCrimsonView::CCrimsonView(CCrimsonWnd *pParent)
{
	m_fUpload = TRUE;

	m_fOnline = FALSE;

	m_pParent = pParent;

	m_hMarker = INVALID_HANDLE_VALUE;

	m_fHasSim = Link_HasEmulate();
}

// Destructor

CCrimsonView::~CCrimsonView(void)
{
	DeleteFile(m_AutoFile);

	CloseHandle(m_hMarker);

	Link_KillEmulate();

	delete m_pDbase;
}

// Attributes

BOOL CCrimsonView::IsDirty(void) const
{
	return !m_pDbase->IsReadOnly() && m_pDbase->IsDirty();
}

BOOL CCrimsonView::IsMucky(void) const
{
	return m_pDbase->CanSaveCopy() && m_pDbase->IsMucky();
}

BOOL CCrimsonView::HasFilename(void) const
{
	return !m_pDbase->GetFilename().IsEmpty();
}

CDatabase * CCrimsonView::GetDatabase(void)
{
	return m_pDbase;
}

CFilename CCrimsonView::GetFilename(void) const
{
	return m_pDbase->GetFilename();
}

CString CCrimsonView::GetFullModel(void) const
{
	return m_pDbase->GetSystemItem()->GetModel();
}

CString CCrimsonView::GetDisplayName(void) const
{
	return m_pDbase->GetSystemItem()->GetDisplayName();
}

CString CCrimsonView::GetCaption(void) const
{
	return m_Caption;
}

// Operations

void CCrimsonView::SetNewFile(void)
{
	m_uSequence += 1;

	m_Caption.Printf(IDS_UNTITLED_FILE, m_uSequence);

	m_pParent->UpdateTitleBar();
}

void CCrimsonView::SetCaption(void)
{
	CFilename Name = GetFilename();

	m_Caption = Name.GetBareName();

	if( Name.GetType() != CString(IDS_CD32).Mid(1) ) {

		m_Caption += L" - ";

		m_Caption += CString(IDS_IMPORTED_FILE);
	}

	m_pParent->UpdateTitleBar();
}

void CCrimsonView::SetDirty(void)
{
	m_pDbase->SetDirty();
}

void CCrimsonView::ClearDirty(void)
{
	m_pDbase->ClearDirty();
}

BOOL CCrimsonView::CheckSave(void)
{
	if( SendCommit() ) {

		if( IsDirty() ) {

			if( m_fOnline ) {

				switch( YesNoCancel(CString(IDS_CONFIGURATION_HAS_2)) ) {

					case IDNO:
						return TRUE;

					case IDCANCEL:
						return FALSE;
				}

				if( Link_Update(m_pDbase) ) {

					ClearDirty();

					return TRUE;
				}

				return FALSE;
			}
			else {
				CString Text;

				if( HasFilename() ) {

					CString Name = GetFilename().GetBareName();

					Text.Printf(IDS_CHECK_SAVE, Name);
				}
				else {
					CString Name = IDS_CHECK_UNTITLED;

					Text.Printf(IDS_CHECK_SAVE, Name);
				}

				switch( YesNoCancel(Text) ) {

					case IDNO:
						return TRUE;

					case IDCANCEL:
						return FALSE;
				}

				OnFileSave();

				return !IsDirty();
			}
		}

		return TRUE;
	}

	return FALSE;
}

// Overribables

void CCrimsonView::OnAttach(void)
{
	CSystemWnd::OnAttach();

	Link_SetDatabase(m_pDbase);
}

// Message Map

AfxMessageMap(CCrimsonView, CSystemWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchGetInfoType(IDM_LINK, OnLinkGetInfo)
	AfxDispatchControlType(IDM_LINK, OnLinkControl)
	AfxDispatchCommandType(IDM_LINK, OnLinkCommand)
	AfxDispatchControlType(IDM_FILE, OnFileControl)
	AfxDispatchCommandType(IDM_FILE, OnFileCommand)

	AfxMessageEnd(CCrimsonView)
};

// Message Handlers

void CCrimsonView::OnPostCreate(void)
{
	FindAutoParams();

	CSystemWnd::OnPostCreate();
}

void CCrimsonView::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( uID == m_timerSave ) {

		if( m_fAutoSave ) {

			if( IsMucky() ) {

				if( afxMainWnd->IsActive() ) {

					HWND hWnd = FindWindow(L"#32768", NULL);

					if( !hWnd || !::IsWindowVisible(hWnd) ) {

						if( !(GetKeyState(VK_LBUTTON) & 0x8000) ) {

							if( !(GetKeyState(VK_RBUTTON) & 0x8000) ) {

								CString Text = afxThread->GetStatusText();

								afxThread->SetStatusText(CString(IDS_AUTOSAVING));

								if( m_pDbase->SaveAuto(m_AutoFile) ) {

									afxThread->SetStatusText(Text);

									LoadAutoTimer(m_uAutoTime);

									return;
								}

								afxThread->SetStatusText(Text);

								KillAutoTimer();

								return;
							}
						}
					}
				}

				SetTimer(m_timerSave, 1000);

				return;
			}

			LoadAutoTimer(m_uAutoTime / 2);

			return;
		}

		KillAutoTimer();

		return;
	}

	CSystemWnd::OnTimer(uID, pfnProc);
}

// Link Menu Support

BOOL CCrimsonView::OnLinkGetInfo(UINT uID, CCmdInfo &Info)
{
	static UINT const List[] =

	{
		IDM_LINK_SEND,     MAKELONG(0x000B, 0x4000),
		IDM_LINK_UPDATE,   MAKELONG(0x000A, 0x4000),
		IDM_LINK_UPLOAD,   MAKELONG(0x000C, 0x4000),
		IDM_LINK_VERIFY,   MAKELONG(0x0011, 0x4000),
		IDM_LINK_FIND,	   MAKELONG(0x0040, 0x1000),
		IDM_LINK_BROWSE,   MAKELONG(0x003F, 0x1000),
		IDM_LINK_FILES,	   MAKELONG(0x0041, 0x1000),
	};

	for( UINT n = 0; n < elements(List); n += 2 ) {

		if( uID == List[n] ) {

			Info.m_Image = List[n+1];

			break;
		}
	}

	return FALSE;
}

BOOL CCrimsonView::OnLinkControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_LINK_UPDATE:
		case IDM_LINK_SEND:

			if( Link_AlwaysSend() ) {

				Src.EnableItem(TRUE);
			}
			else
				Src.EnableItem(IsDirty() || HasFilename());

			return TRUE;

		case IDM_LINK_VERIFY:

			if( Link_HasVerify() ) {

				Src.EnableItem(IsDirty() || HasFilename());
			}
			else
				Src.EnableItem(FALSE);

			break;

		case IDM_LINK_SET_TIME:

			Src.EnableItem(Link_HasSetTime());

			break;

		case IDM_LINK_OPTIONS:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_LINK_FIND:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_LINK_SEND_IMAGE:

			Src.EnableItem(!m_pDbase->IsReadOnly());

			Src.CheckItem(m_pDbase->HasImage());

			return TRUE;

		case IDM_LINK_EMULATE:

			Src.EnableItem(m_fHasSim);

			Src.CheckItem(m_fHasSim && Link_GetEmulate());

			return TRUE;

		case IDM_LINK_CALIBRATE:

			Src.EnableItem(Link_GetCalibrate());

			return TRUE;

		case IDM_LINK_BROWSE:
		case IDM_LINK_FILES:

			Src.EnableItem(Link_CanBrowse());

			return TRUE;
	}

	return FALSE;
}

BOOL CCrimsonView::OnLinkCommand(UINT uID)
{
	switch( uID ) {

		case IDM_LINK_UPDATE:

			LinkUpdate();

			return TRUE;

		case IDM_LINK_SEND:

			LinkSend();

			return TRUE;

		case IDM_LINK_VERIFY:

			LinkVerify();

			return TRUE;

		case IDM_LINK_SET_TIME:

			Link_SetTime();

			break;

		case IDM_LINK_OPTIONS:

			LinkOptions();

			return TRUE;

		case IDM_LINK_FIND:

			Link_Find();

			return TRUE;

		case IDM_LINK_SEND_IMAGE:

			m_pDbase->SetImage(!m_pDbase->HasImage());

			m_pDbase->SetDirty();

			return TRUE;

		case IDM_LINK_EMULATE:

			Link_SetEmulate(!Link_GetEmulate());

			return TRUE;

		case IDM_LINK_CALIBRATE:

			Link_SetCalibrate();

			return TRUE;

		case IDM_LINK_BROWSE:

			Link_Browse();

			return TRUE;

		case IDM_LINK_FILES:

			Link_ShowFiles();

			return TRUE;
	}

	return FALSE;
}

// File Menu Support

BOOL CCrimsonView::OnFileControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_FILE_SAVE:

			Src.EnableItem(!m_pDbase->IsReadOnly() && IsDirty() || !HasFilename());

			break;

		case IDM_FILE_SAVE_IMAGE:

			Src.EnableItem(!m_fOnline);

			break;

		case IDM_FILE_SAVE_AS:

			Src.EnableItem(!m_fOnline && m_pDbase->CanSaveCopy());

			break;

		case IDM_FILE_SECURITY:

			Src.EnableItem(!m_pDbase->IsReadOnly());

			break;

		default:

			return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonView::OnFileCommand(UINT uID)
{
	switch( uID ) {

		case IDM_FILE_SAVE:

			OnFileSave();

			break;

		case IDM_FILE_SAVE_AS:

			OnFileSaveAs();

			break;

		case IDM_FILE_SAVE_IMAGE:

			OnFileSaveImage();

			break;

		case IDM_FILE_SECURITY:

			m_pDbase->EditSecurity();

			m_pParent->UpdateTitleBar();

			SendMessage(WM_SETFOCUS);

			break;

		default:

			return FALSE;
	}

	return TRUE;
}

void CCrimsonView::OnFileSave(void)
{
	if( !m_fOnline ) {

		if( !HasFilename() ) {

			OnFileSaveAs();
		}
		else {
			CFilename Name = GetFilename();

			if( SendCommit() ) {

				if( Name.GetType() == CString(IDS_CD32).Mid(1) ) {

					if( SaveFile(Name) ) {

						if( m_pDbase->IsReadOnly() ) {

							m_pDbase->ClearReadOnly();

							m_pViewer->Purge();
						}

						SetCaption();
					}

					return;
				}

				if( Name.GetType() == CString(IDS_CD3).Mid(1) || Name.GetType() == CString(IDS_CD31).Mid(1) ) {

					CString Prompt;

					Prompt += CString(IDS_IMPORTED_CRIMSON);

					Prompt += CString(IDS_YOU_WILL_NOW_BE);

					if( OkCancel(Prompt) == IDOK ) {

						CModule * pApp  = afxModule->GetApp();

						CFilename Path  = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Databases");

						CSaveFileDialog::LoadLastPath(L"Prime", Path);

						CSaveFileDialog Dialog;

						Dialog.SetCaption(CString(IDS_SAVE_CONVERSION_2));

						Dialog.SetFilter(CString(IDS_SAVE_FILTER));

						Dialog.SetFilename(Name.WithType(CString(IDS_CD32).Mid(1)));

						for( ;;) {

							if( Dialog.ExecAndCheck(ThisObject) ) {

								Name = Dialog.GetFilename();

								if( Name.GetType() == CString(IDS_CD32).Mid(1) ) {

									m_pDbase->ClearFilename();

									SaveFile(Name);

									SetCaption();

									return;
								}

								CString Text = CString(IDS_ALL_FILES_MUST_BE);

								Error(Text);

								continue;
							}

							break;
						}
					}

					return;
				}
			}
		}

		return;
	}

	afxMainWnd->SendMessage(WM_COMMAND, IDM_LINK_UPDATE);
}

void CCrimsonView::SaveConversion(void)
{
	OnFileSave();
}

BOOL CCrimsonView::OnFileSaveAs(void)
{
	if( SendCommit() ) {

		CModule * pApp = afxModule->GetApp();

		CFilename Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Databases");

		CSaveFileDialog::LoadLastPath(L"Prime", Path);

		CSaveFileDialog Dialog;

		Dialog.SetFilter(CString(IDS_SAVE_FILTER));

		for( ;;) {

			if( Dialog.ExecAndCheck(ThisObject) ) {

				CFilename Name = Dialog.GetFilename();

				if( Name.GetType() == CString(IDS_CD32).Mid(1) ) {

					if( SaveFile(Dialog.GetFilename()) ) {

						if( m_pDbase->IsReadOnly() ) {

							m_pDbase->ClearReadOnly();

							m_pViewer->Purge();
						}

						SetCaption();
					}

					Dialog.SaveLastPath(L"Prime");

					return TRUE;
				}

				CString Text = CString(IDS_ALL_FILES_MUST_BE);

				Error(Text);

				continue;
			}

			break;
		}
	}

	return FALSE;
}

BOOL CCrimsonView::OnFileSaveImage(void)
{
	if( SendCommit() ) {

		CFilename Name = GetFilename().GetBareName();

		CSaveFileDialog::LoadLastPath(L"DbImg");

		for( ;;) {

			CSaveFileDialog Dialog;

			Dialog.SetFilter(CString(IDS_IMAGE_FILTER));

			Dialog.SetCaption(CString(IDS_IMAGE_SAVE));

			if( !Name.IsEmpty() ) {

				CString Type(IDS_IMAGE_EXT);

				Name.ChangeType(Type.Mid(1));

				Dialog.SetFilename(Name);
			}

			if( Dialog.Execute(ThisObject) ) {

				CFilename Name = Dialog.GetFilename();

				switch( CanOverwrite(Name) ) {

					case IDNO:

						continue;

					case IDCANCEL:

						return FALSE;
				}

				SaveImage(Name);

				Dialog.SaveLastPath(L"DbImg");

				return TRUE;
			}

			return FALSE;
		}
	}

	return FALSE;
}

// Implementation

BOOL CCrimsonView::SaveFile(CFilename const &Name)
{
	afxMainWnd->UpdateWindow();

	afxThread->SetStatusText(CString(IDS_SAVING_FILE));

	if( m_pDbase->SaveFile(Name) ) {

		afxThread->SetStatusText(L"");

		if( m_fAutoSave ) {

			DeleteFile(m_AutoFile);

			LoadAutoTimer(m_uAutoTime);
		}

		m_pParent->AddRecent(Name);

		return TRUE;
	}

	afxThread->SetStatusText(L"");

	Error(IDS_SAVE_ERROR);

	return FALSE;
}

BOOL CCrimsonView::SaveImage(CFilename const &Name)
{
	if( SendCommit() ) {

		BOOL    fOld = m_pDbase->HasImage();

		CString Text = IDS_DBASE_UPSUPPORT;

		switch( YesNoCancel(Text) ) {

			case IDCANCEL:

				return FALSE;

			case IDYES:

				m_pDbase->SetImage(TRUE);

				break;

			case IDNO:

				m_pDbase->SetImage(FALSE);

				break;
		}

		afxMainWnd->UpdateWindow();

		afxThread->SetWaitMode(TRUE);

		if( TRUE ) {

			CImageBuilder Builder(m_pDbase);

			if( Builder.SaveImage(Name) ) {

				afxThread->SetWaitMode(FALSE);

				m_pDbase->SetImage(fOld);

				return TRUE;
			}
		}

		afxThread->SetWaitMode(FALSE);

		m_pDbase->SetImage(fOld);

		Error(IDS_SAVE_ERROR);

		return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonView::SendCommit(void)
{
	return !afxMainWnd->RouteCommand(IDM_GLOBAL_COMMIT, 0);
}

BOOL CCrimsonView::DropLink(void)
{
	afxMainWnd->RouteCommand(IDM_GLOBAL_DROP_LINK, 0);

	return TRUE;
}

BOOL CCrimsonView::CheckPending(void)
{
	if( m_pDbase->GetPending() ) {

		CString Text;

		Text += CString(IDS_YOU_HAVE);

		Text += CString(IDS_TRANSLATE_THESE);

		Text += CString(IDS_YOUR_DATABASE_MAY);

		Text += CString(IDS_DO_YOU_WANT_TO_4);

		if( NoYes(Text) == IDYES ) {

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonView::CheckControl(void)
{
	SendCommit();

	CSystemItem *pSystem = m_pDbase->GetSystemItem();

	if( pSystem->NeedBuild() ) {

		switch( YesNoCancel(CString(IDS_YOU_HAVE_UNBUILT_2)) ) {

			case IDYES:

				if( !pSystem->PerformBuild() ) {

					return FALSE;
				}

			case IDNO:

				break;

			default:

				return FALSE;
		}
	}

	return TRUE;
}

BOOL CCrimsonView::CheckQueries(void)
{
	CSystemItem *pSystem = m_pDbase->GetSystemItem();

	if( !pSystem->CheckSqlQueries() ) {

		if( NoYes(IDS("You have one or more errors in SQL queries. If you do not fix these errors, these queries will not be executed. Do you want to proceed?")) == IDYES ) {

			return TRUE;
		}

		return FALSE;
	}

	return TRUE;
}

BOOL CCrimsonView::FindAutoParams(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"AutoSave");

	if( Reg.IsValid() ) {

		m_fAutoSave = Reg.GetValue(L"Enabled", 1);

		m_uAutoTime = Reg.GetValue(L"Minutes", 10);

		Reg.SetValue(L"Enabled", m_fAutoSave);

		Reg.SetValue(L"Minutes", m_uAutoTime);

		if( m_fAutoSave ) {

			CModule * pApp = afxModule->GetApp();

			CFilename Path = pApp->GetFolder(CSIDL_LOCAL_APPDATA, L"AutoSave");

			m_AutoFile.MakeTemporary(Path);

			m_hMarker = CreateFile(m_AutoFile.WithType(L"mrk"),
					       GENERIC_READ | GENERIC_WRITE,
					       FILE_SHARE_WRITE,
					       NULL,
					       CREATE_ALWAYS,
					       FILE_FLAG_DELETE_ON_CLOSE,
					       NULL
			);

			LoadAutoTimer(m_uAutoTime);

			return TRUE;
		}
	}

	return FALSE;
}

void CCrimsonView::LoadAutoTimer(UINT uMins)
{
	SetTimer(m_timerSave, 1000 * 60 * uMins);
}

void CCrimsonView::KillAutoTimer(void)
{
	KillTimer(m_timerSave);
}

void CCrimsonView::LinkSend(void)
{
	if( CheckPending() && CheckControl() && CheckQueries() ) {

		DropLink();

		Link_Send(m_pDbase);
	}
}

void CCrimsonView::LinkUpdate(void)
{
	if( CheckPending() && CheckControl() && CheckQueries() ) {

		DropLink();

		Link_Update(m_pDbase);
	}
}

void CCrimsonView::LinkVerify(void)
{
	if( CheckPending() ) {

		DropLink();

		Link_Verify(m_pDbase, FALSE);
	}
}

void CCrimsonView::LinkOptions(void)
{
	DropLink();

	Link_SetOptions();
}

// End of File
