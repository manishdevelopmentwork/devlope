
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_String_HPP
	
#define	INCLUDE_String_HPP

//////////////////////////////////////////////////////////////////////////
//
// Documentation Link
//

// https://redlion.atlassian.net/wiki/x/ywDUE

/////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "StrPtr.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CModule;

//////////////////////////////////////////////////////////////////////////
//
// String Information
//

struct DLLAPI CStringData
{
	UINT	m_uAlloc;
	UINT	m_uLen;
	int	m_nRefs;
	UINT	m_uDummy;
	TCHAR	m_cData[];
	};

//////////////////////////////////////////////////////////////////////////
//
// String Searching
//

#ifndef DEFINE_SearchOptions

#define  DEFINE_SearchOptions

enum SearchOptions
{
	searchContains	 = 0x0001,
	searchIsEqualTo  = 0x0002,
	searchStartsWith = 0x0003,
	searchMatchCase  = 0x0100,
	searchWholeWords = 0x0200,
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Dynamic String
//

class DLLAPI CString : public CStrPtr
{
	public:
		// Constructors
		CString(void);
		CString(CString const &That);
		CString(PCTXT pText, UINT uCount);
		CString(TCHAR cData, UINT uCount);
		CString(GUID const &Guid);
		CString(PCTXT pText);

		// Destructor
		~CString(void);

		// Assignment Operators
		CString const & operator = (CString const &That);
		CString const & operator = (GUID const &Guid);
		CString const & operator = (PCTXT pText);

		// Quick Init
		void QuickInit(PCTXT pText, UINT uSize);

		// Attributes
		UINT GetLength(void) const;

		// Read-Only Access
		TCHAR GetAt(UINT uIndex) const;

		// Substring Extraction
		CString Left(UINT uCount) const;
		CString Right(UINT uCount) const;
		CString Mid(UINT uPos, UINT uCount) const;
		CString Mid(UINT uPos) const;

		// Buffer Managment
		void Empty(void);
		void Expand(UINT uAlloc);
		void Compress(void);
		void CopyOnWrite(void);
		void FixLength(UINT uLen);
		void FixLength(void);

		// Concatenation In-Place
		CString const & operator += (CString const &That);
		CString const & operator += (PCTXT pText);
		CString const & operator += (TCHAR cData);

		// Concatenation via Friends
		friend DLLAPI CString operator + (CString const &A, CString const &B);
		friend DLLAPI CString operator + (CString const &A, PCTXT pText);
		friend DLLAPI CString operator + (CString const &A, TCHAR cData);
		friend DLLAPI CString operator + (PCTXT pText, CString const &B);
		friend DLLAPI CString operator + (TCHAR cData, CString const &B);

		// Concatenation Functions
		BOOL Append(CString const &That);
		BOOL Append(PCTXT pText);
		BOOL Append(PCTXT pText, UINT uText);
		BOOL Append(char cData);
		BOOL AppendPrintf(PCTXT pFormat, ...);
		BOOL AppendVPrintf(PCTXT pFormat, va_list pArgs);

		// Write Data Access
		void SetAt(UINT uIndex, TCHAR cData);

		// Comparison Helper
		friend DLLAPI int AfxCompare(CString const &a, CString const &b);

		// Insert and Remove
		void Insert(UINT uIndex, PCTXT pText);
		void Insert(UINT uIndex, CString const &That);
		void Insert(UINT uIndex, TCHAR cData);
		void Delete(UINT uIndex, UINT uCount);

		// Whitespace Trimming
		void TrimLeft(void);
		void TrimRight(void);
		void TrimBoth(void);
		void StripAll(void);

		// Case Switching
		void MakeUpper(void);
		void MakeLower(void);

		// Replacement
		UINT Replace(TCHAR cFind, TCHAR cNew);
		UINT Replace(PCTXT pFind, PCTXT pNew);

		// Removal
		void    Remove (TCHAR cFind);
		CString Without(TCHAR cFind);

		// Parsing
		UINT    Tokenize(CStringArray &Array, TCHAR cFind) const;
		UINT    Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cQuote) const;
		UINT    Tokenize(CStringArray &Array, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const;
		UINT    Tokenize(CStringArray &Array, PCTXT pFind) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, TCHAR pFind, TCHAR cQuote) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, TCHAR cFind, TCHAR cOpen, TCHAR cClose) const;
		UINT    Tokenize(CStringArray &Array, UINT uLimit, PCTXT pFind) const;
		CString TokenLeft(TCHAR cFind) const;
		CString TokenFrom(TCHAR cFind) const;
		CString TokenLast(TCHAR cFind) const;
		CString StripToken(TCHAR cFind);
		CString StripToken(TCHAR cFind, TCHAR cQuote);
		CString StripToken(TCHAR cFind, TCHAR cOpen, TCHAR cClose);
		CString StripToken(PCTXT pFind);

		// Building
		void Build(CStringArray &Array, TCHAR cSep);
		void Build(CStringArray &Array, PCTXT pSep);

		// Display Ordering
		void MakeDisplayOrder(void);

		// Printf Formatting
		void Printf(PCTXT pFormat, ...);
		void VPrintf(PCTXT pFormat, va_list pArgs);

		// Diagnostics
		void AssertValid(void) const;

	protected:
		// Static Data
		static DWORD         m_Null[];
		static CStringData & m_Empty;

		// Protected Constructor
		CString(PCTXT p1, UINT u1, PCTXT p2, UINT u2);

		// Initialisation
		void InitFrom(GUID const &Guid);

		// Interal Helpers
		PTXT IntPrintf(UINT &uLen, PCTXT pFormat, va_list pArgs);

		// Clearing
		friend void DLLAPI AfxSetZero(CString &Text);

		// Implementation
		void Alloc(UINT uLen, UINT uAlloc);
		void Alloc(UINT uLen);
		BOOL GrowString(UINT uLen);
		void Release(void);
		UINT AdjustSize(UINT uAlloc);
		UINT FindAlloc(UINT uLen);

		// Friends
		friend CStringData * GetStringData(CString const *p);
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Data Access

INLINE CStringData * GetStringData(CString const *p)
{
	return (((CStringData *) p->m_pText) - 1);
	}

// Constructors

INLINE CString::CString(void)
{
	m_pText = m_Empty.m_cData;
	}

INLINE CString::CString(CString const &That)
{
	That.AssertValid();

	if( !That.IsEmpty() ) {

		m_pText = That.m_pText;

		GetStringData(this)->m_nRefs++;

		return;
		}

	m_pText = m_Empty.m_cData;
	}

INLINE CString::CString(PCTXT pText)
{
	AfxValidateStringPtr(pText);

	if( *pText ) {

		Alloc(strlen(pText));

		strcpy(m_pText, pText);

		return;
		}

	m_pText = m_Empty.m_cData;
	}

// Destructor

INLINE CString::~CString(void)
{
	Release();
	}

// Attributes

INLINE UINT CString::GetLength(void) const
{
	return GetStringData(this)->m_uLen;
	}

// Read-Only Access

INLINE TCHAR CString::GetAt(UINT uIndex) const
{
	return (uIndex < GetLength()) ? m_pText[uIndex] : char(0);
	}

// Comparison Helper

INLINE int AfxCompare(CString const &a, CString const &b)
{
	return _stricmp(a.m_pText, b.m_pText);
	}

// Diagnostics

#ifndef _DEBUG

INLINE void CString::AssertValid(void) const
{
	}

#endif

// Clearing

INLINE void AfxSetZero(CString &Text)
{
	Text.Empty();
	}

// Implementation

INLINE void CString::Alloc(UINT uLen, UINT uAlloc)
{
	uAlloc     = AdjustSize(uAlloc);
	
	UINT uSize = uAlloc + sizeof(CStringData);
	
	CStringData *pData = (CStringData *) new BYTE [ uSize ];
	
	pData->m_uLen   = uLen;
	
	pData->m_uAlloc = uAlloc;
	
	pData->m_nRefs  = 1;

	// cppcheck-suppress uninitStructMember -- Lint doesn't like this as we're
	// referencing an unitialized variable, even though that's perfectly okay.

	m_pText = pData->m_cData;

	// cppcheck-suppress memleak -- Lint thinks we are going to leak memory
	// here as we've allocated pData but not stored that pointer anywhere.
	}

INLINE void CString::Alloc(UINT uLen)
{
	Alloc(uLen, FindAlloc(uLen));
	}

INLINE void CString::Release(void)
{
	CStringData *pData = GetStringData(this);
	
	if( pData->m_nRefs ) {
		
		if( !--(pData->m_nRefs) ) {

			delete pData;
			}
		
		m_pText = m_Empty.m_cData;
		}
	}

INLINE UINT CString::AdjustSize(UINT uAlloc)
{
	return ((uAlloc + 15) & ~15);
	}

INLINE UINT CString::FindAlloc(UINT uLen)
{
	return sizeof(TCHAR) * (1 + uLen);
	}

// End of File

#endif
