
#include "intern.hpp"

#include "omron.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Omron C Series PLC
//

// Instantiator

ICommsDriver *	Create_OmronPLCDriver(void)
{
	return New COmronPLCDriver;
	}

// Constructor

COmronPLCDriver::COmronPLCDriver(void)
{
	m_wID		= 0x3311;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Omron";
	
	m_DriverName	= "Series C PLC";
	
	m_Version	= "1.02";
	
	m_ShortName	= "Omron C Series";

	AddSpaces();
	}

// Binding Control

UINT COmronPLCDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void COmronPLCDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 2;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Implementation

void COmronPLCDriver::AddSpaces(void)
{
	AddSpace(New CSpace('I', "IM",	"IO Registers (Binary)",	10,  0, 9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('J', "IB",	"IO Registers (BCD)",		10,  0, 9999, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('D', "DM",	"Data Memories (Binary)",	10,  0,	9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('B', "DB",	"Data Memories (BCD)",		10,  0,	9999, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('A', "AM",	"Auxiliary Registers (Binary)",	10,  0,	9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('C', "AB",	"Auxiliary Registers (BCD)",	10,  0,	9999, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('H', "HM",	"Retentive Reg (Binary)",	10,  0, 9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('K', "HB",	"Retentive Reg (BCD)",		10,  0, 9999, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('L', "LM",	"Network Link Reg (Binary)",	10,  0,	9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('M', "LB",	"Network Link Reg (BCD)",	10,  0,	9999, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('P', "PV",	"Timer/Counter Values",		10,  0,	9999, addrWordAsWord));
	AddSpace(New CSpace('E', "EM",	"Ext Data Memory (Binary)",	10,  0,	9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('F', "EB",	"Ext Data Memory (BCD)",	10,  0, 9999, addrWordAsWord, addrWordAsLong));
	AddSpace(New CSpace('O', "IOM",	"CIO Registers (Binary)",	10,  0,	9999, addrWordAsWord, addrWordAsReal));
	AddSpace(New CSpace('Q', "IOB",	"CIO Registers (BCD)",		10,  0,	9999, addrWordAsWord, addrWordAsLong));
	}

// Address Helpers

BOOL COmronPLCDriver::CheckAlignment(CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case 'D':
		case 'B':
		case 'I':
		case 'J':
		case 'A':
		case 'C':
		case 'H':
		case 'K':
		case 'L':
		case 'M':
		case 'E':
		case 'F':
		case 'O':
		case 'Q':
				
			return FALSE;
		}

	return TRUE;
	}


// End of File
