
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Variable Naming Help
//

global CString	Variable_FindNextName(DWORD dwProject, PCTXT pFormat)
{
	// How do these play with other stuff?!!!!

	/*"Variable%1!u!"*/

	for( UINT n = 1;; n++ ) {

		CFormat Name(pFormat, n);

		if( !afxDatabase->FindVarExact(dwProject, Name, 0, 0) ) {
			
			return Name;
			}
		}

	return CString();
	}

global CString	Program_FindNextName(DWORD dwProject, DWORD dwLanguage, DWORD dwSection, DWORD dwParent, PCTXT pFormat)
{
	for( UINT n = 1;; n++ ) {

		CFormat Name(pFormat, n);

		if( afxDatabase->CanCreateProgram(dwProject, dwLanguage, dwSection, dwParent, Name) ) {
			
			return Name;
			}
		}

	return CString();
	}

// End of File

