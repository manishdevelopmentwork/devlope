
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Producer_HPP

#define INCLUDE_Producer_HPP

//////////////////////////////////////////////////////////////////////////
//
// EtherNet/IP Producer
//

class CProducer : public IProducer
{
	public:
		// Constructor
		CProducer(void);
		
		// Destructor
		~CProducer(void);

		// Binding
		void Bind(UINT uIndex, IImplicit *pImplicit);
		
		// Mapping
		void SetIdent(DWORD wInstance, DWORD wID);
		void SetMapping(WORD wOffset, WORD wSize);

		// Attributes
		DWORD GetInstance(void) const;
		UINT  GetIndex(void) const;
		BOOL  IsFree(void) const;

		// Operations
		void Create(void);
		void Close(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IProducer
		DWORD METHOD GetIdent(void);
		UINT  METHOD GetSize(void);
		BOOL  METHOD IsActive(void);
		UINT  METHOD Send(PBYTE pData, UINT uSize);

	protected:
		// Data
		ULONG	    m_uRefs;
		UINT        m_uIndex;
		IImplicit * m_pAssy;
		BOOL        m_fUsed;
		DWORD	    m_dwInst;
		DWORD	    m_dwIdent;
		WORD	    m_wOffset;
		WORD	    m_wSize;
	};

// End of File

#endif
