
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Core Runtime
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_C3CORE_HPP

#define	INCLUDE_C3CORE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include <c3shim.hpp>

#include <c3faces.hpp>

////////////////////////////////////////////////////////////////////////
//
// Platform Macros
//

#define CODE_SEG	const

#define	CTEXT		static char CODE_SEG

#define	NewHere(p)	new(0, p)

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef signed int	C3INT;

typedef float		C3REAL;

//////////////////////////////////////////////////////////////////////////
//
// Type Conversion
//

inline C3REAL I2R(DWORD d)
{
	return *((C3REAL *) &d);
	}

inline DWORD R2I(C3REAL r)
{
	return *((DWORD *) &r);
	}

//////////////////////////////////////////////////////////////////////////
//
// Data Types
//

enum DataType
{
	typeVoid	= 0x0000,
	typeInteger	= 0x0001,
	typeReal	= 0x0002,
	typeString	= 0x0003,
	typeLogical	= 0x0004,
	typeLValue	= 0x0005,
	typeNumeric	= 0x0006,
	typeObject	= 0x0040,
	typeMask	= 0x00FF
	};

//////////////////////////////////////////////////////////////////////////
//
// Type Flags
//

enum TypeFlag
{
	flagConstant  = 0x0001,
	flagWritable  = 0x0002,
	flagArray     = 0x0004,
	flagExtended  = 0x0008,
	flagElement   = 0x0010,
	flagBitRef    = 0x0020,
	flagLocal     = 0x0040,
	flagActive    = 0x0080,
	flagSoftWrite = 0x0100,
	flagInherent  = 0x0200,
	flagCommsRef  = 0x0400,
	flagCommsTab  = 0x0800,
	flagTagRef    = 0x1000,
	};

//////////////////////////////////////////////////////////////////////////
//
// GetData Flags
//

enum
{
	getNone		= 0x0000,
	getMaskSource	= 0x000F,
	getScaled	= 0x0000,
	getManipulated	= 0x0001,
	getCommsData	= 0x0002,
	getNoString     = 0x0010,
	};

//////////////////////////////////////////////////////////////////////////
//
// SetData Flags
//

enum
{
	setNone		= 0x0000,
	setMaskSource	= 0x000F,
	setScaled	= 0x0000,
	setManipulated	= 0x0001,
	setCommsData	= 0x0002,
	setForce	= 0x0010,
	setDirect	= 0x0020,
	setFlush	= 0x0040,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scan Codes
//

enum
{
	scanFalse   = 0,
	scanTrue    = 1,
	scanUser    = 2,
	scanOnce    = 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Reference
//

#if !defined(_E_BIG)

struct CDataRef
{
	union {
		struct {
			INT	m_Offset : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Block  : 10;
			UINT    m_BitRef : 5;
			UINT	m_HasBit : 1;
			} b;

		struct {
			UINT	m_Index  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Array  : 10;
			UINT	m_BitRef : 5;
			UINT    m_HasBit : 1;
			} t;

		struct {
			UINT	m_Index  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Array  : 15;
			UINT    m_HasBit : 1;
			} x;

		DWORD m_Ref;
		};
	};

#else

struct CDataRef
{
	union {
		struct {
			UINT	m_HasBit : 1;
			UINT    m_BitRef : 5;
			UINT	m_Block  : 10;
			UINT	m_IsTag  : 1;
			INT	m_Offset : 15;
			} b;

		struct {
			UINT    m_HasBit : 1;
			UINT	m_BitRef : 5;
			UINT	m_Array  : 10;
			UINT	m_IsTag  : 1;
			UINT	m_Index  : 15;
			} t;

		struct {
			UINT    m_HasBit : 1;
			UINT	m_Array  : 15;
			UINT	m_IsTag  : 1;
			UINT	m_Index  : 15;
			} x;
		
		DWORD m_Ref;
		};
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// Data Server Interface
//

interface IDataServer : public IBase
{
	virtual	WORD	CheckID(	WORD		ID
					) = 0;

	virtual BOOL	IsAvail(	DWORD		ID
					) = 0;

	virtual BOOL    SetScan(	DWORD		ID,
					UINT		Code
					) = 0;

	virtual DWORD	GetData(	DWORD		ID,
					UINT		Type,
					UINT		Flags
					) = 0;
	
	virtual BOOL	SetData(	DWORD		ID,
					UINT		Type,
					UINT		Flags,
					DWORD		Data
					) = 0;

	virtual	DWORD	GetProp(	DWORD		ID,
					WORD		Prop,
					UINT		Type
					) = 0;

	virtual DWORD	RunFunc(	WORD		ID,
					UINT		uParam,
					PDWORD		pParam
					) = 0;

	virtual BOOL	GetName(	DWORD		ID,
					UINT		m,
					PSTR		pName,
					UINT		uName
					) = 0;
	};

/////////////////////////////////////////////////////////////////////////
//
// Database Item
//

class CItem
{
	public:
		// Destructor
		virtual ~CItem(void);

		// Persistance
		virtual void Load(PCBYTE &pData);

	protected:
		// Init Debug
		static void ValidateLoad(PCTXT pClass, PCBYTE &pData);

		// Init Helpers
		inline static BYTE   GetByte(PCBYTE &pData);
		inline static WORD   GetWord(PCBYTE &pData);
		inline static DWORD  GetLong(PCBYTE &pData);
		inline static DWORD  GetAddr(PCBYTE &pData);
		inline static PBYTE  GetCopy(PCBYTE &pData);
		inline static PBYTE  GetCopy(PCBYTE &pData, UINT &uSize);
		inline static PCBYTE GetData(PCBYTE &pData);
		inline static PCUTF  GetWide(PCBYTE &pData);

		// Init Helpers
		static PCUTF GetCryp(PCBYTE &pData);

		// Other Items
		static UINT GetItem(PCBYTE &pData, CItem *pItem, WORD Class);

		// Data Fallback
		static DWORD GetValue(UINT Type);
		static BOOL  SetValue(UINT Type, DWORD Data);

		// International Strings
		PCTXT GetIntl(PCTXT pText);
	};

/////////////////////////////////////////////////////////////////////////
//
// Database Init Alignment
//

#if 1 || defined(_M_ARM)

inline void Item_Align2(PCBYTE &pData)
{
	pData += (DWORD(pData) & 1);
	}

inline void Item_Align4(PCBYTE &pData)
{
	UINT n = (DWORD(pData) & 3);

	pData += n ? (4-n) : 0;
	}

#else

inline void Item_Align2(PCBYTE &pData)
{
	}

inline void Item_Align4(PCBYTE &pData)
{
	}

#endif

/////////////////////////////////////////////////////////////////////////
//
// Database Init Helpers
//

inline BYTE CItem::GetByte(PCBYTE &pData)
{
	BYTE x = *PCBYTE(pData);

	pData += sizeof(x);

	return x;
	}

inline WORD CItem::GetWord(PCBYTE &pData)
{
	Item_Align2(pData);

	WORD x = MotorToHost(*PCWORD(pData));

	pData += sizeof(x);

	return x;
	}

inline DWORD CItem::GetLong(PCBYTE &pData)
{
	Item_Align4(pData);

	DWORD x = MotorToHost(*PCDWORD(pData));

	pData += sizeof(x);

	return x;
	}

inline DWORD CItem::GetAddr(PCBYTE &pData)
{
	Item_Align4(pData);

	DWORD x = *PCDWORD(pData);

	pData += sizeof(x);

	return x;
	}

inline PBYTE CItem::GetCopy(PCBYTE &pData)
{
	UINT s = GetLong(pData);

	if( s ) {

		PBYTE p = New BYTE [ s ];

		memcpy(p, pData, s);

		pData += s;

		return p;
		}

	return NULL;
	}

inline PBYTE CItem::GetCopy(PCBYTE &pData, UINT &uSize)
{
	UINT s = GetLong(pData);

	if( s ) {

		PBYTE p = New BYTE [ s ];

		memcpy(p, pData, s);

		pData += s;

		uSize  = s;

		return p;
		}

	uSize = 0;

	return NULL;
	}

inline PCBYTE CItem::GetData(PCBYTE &pData)
{
	UINT s = GetWord(pData);

	if( s ) {
	
		PCBYTE p = PCBYTE(pData);

		pData += s;

		return p;
		}

	return NULL;
	}

inline PCUTF CItem::GetWide(PCBYTE &pData)
{
	UINT  s = GetWord(pData);

	PCUTF p = PCUTF(pData);

	pData  += s * sizeof(WCHAR);

	#if 1 || defined(_E_LITTLE)

	static WCHAR t[256];

	for( UINT n = 0; n < s; n++ ) {

		t[n] = MotorToHost(WORD(p[n]));
		}

	t[s] = 0;

	return t;

	#else

	return p;

	#endif
	}

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

interface ITaskEntry;

struct    CTaskDef;

//////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

typedef CArray <CTaskDef> CTaskList;

//////////////////////////////////////////////////////////////////////////
//
// Task Entry Interface
//

interface ITaskEntry
{
	virtual void TaskInit(UINT uID) = 0;
	virtual void TaskExec(UINT uID) = 0;
	virtual void TaskStop(UINT uID) = 0;
	virtual void TaskTerm(UINT uID) = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Task Definition
//

struct CTaskDef
{
	CString      m_Name;
	ITaskEntry * m_pEntry;
	UINT	     m_uID;
	UINT	     m_uCount;
	UINT	     m_uLevel;
	UINT	     m_uStack;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CSystemItem;

class CEthernet;

//////////////////////////////////////////////////////////////////////////
//
// System Item
//

class CSystemItem : public CItem
{
	public:
		// Constructor
		CSystemItem(void);

		// Destructor
		~CSystemItem(void);

		// System Calls
		virtual void SystemStart(void);
		virtual void SystemStop(void);
		virtual void SystemSave(void);
		virtual void SystemInit(void);
		virtual void SystemTerm(void);
		virtual void GetTaskList(CTaskList &List);
		virtual BOOL IsPriority(void);
		virtual UINT GetCommsStatus(void);
		virtual BOOL GetDataServer(IDataServer * &pData);
		virtual BOOL IsRunningControl(void);

		// Instance Pointer
		static CSystemItem * m_pThis;
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CRC16;

//////////////////////////////////////////////////////////////////////////
//
// 16-bit CRC Generator
//

class CRC16 
{
	public:
		// Constructor
		inline CRC16(void);
			
		// Attributes
		inline WORD GetValue(void) const;
			
		// Operations
		inline void Clear(void);
		inline void Preset(void);
		inline void Add(BYTE b);
			
	protected:
		// Lookup Data
		static WORD CODE_SEG m_Table[];

		// Data Members
		WORD m_CRC;
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

#include "crc16.ipp"

//////////////////////////////////////////////////////////////////////////
//
// Global Objects
//

extern ICrimsonPxe      * g_pPxe;
extern ICrimsonApp	* g_pApp;
extern IDatabase        * g_pDbase;
extern IPersist	        * g_pPersist;
extern IEventStorage    * g_pEvStg;
extern ICrimsonIdentity * g_pIdentity;
extern ILicenseManager  * g_pLicense;

//////////////////////////////////////////////////////////////////////////
//
// Application Functions
//

// system.cpp

extern	BOOL	SystemLoad(PCBYTE &pData);
extern	BOOL	SystemGetRemote(PCBYTE &pData, UINT &uSize);
extern	void	SystemTimeChange(void);
extern	BOOL	SystemGetLED(BOOL fRun);

//////////////////////////////////////////////////////////////////////////
//
// Time Structure
//

struct CTime
{
	UINT	uSeconds;
	UINT	uMinutes;
	UINT	uHours;
	UINT	uDay;
	UINT	uDate;
	UINT	uMonth;
	UINT	uYear;
	};

//////////////////////////////////////////////////////////////////////////
//
// Time Functions
//

extern	void	TimeZoneInit(void);
extern	void	SetTimeZone(INT nZone, BOOL fDst);
extern	void	SetTimeZone(INT nZone);
extern	void	SetTimeZoneMins(INT nZone, BOOL fDst);
extern	void	SetTimeZoneMins(INT nZone);
extern	void	SetDaylight(BOOL fDst);
extern	INT	GetTimeZone(void);
extern	INT	GetTimeZoneMins(void);
extern	BOOL	GetDaylight(void);
extern	INT	GetZuluOffset(void);
extern  DWORD   GetNow(void);
extern  DWORD   GetNowTimes5(void);
extern  UINT    GetNowFraction(void);
extern  BOOL    SetTime(CTime &Time);
extern  void    GetTime(CTime &Time);
extern  DWORD   Time(UINT h, UINT m, UINT s);
extern  DWORD   Date(UINT y, UINT m, UINT d);
extern  void    GetWholeDate(DWORD t, UINT *p);
extern  void    GetWholeTime(DWORD t, UINT *p);
extern  UINT    GetYear(DWORD t);
extern  UINT    GetMonth(DWORD t);
extern  UINT    GetDate(DWORD t);
extern  UINT    GetDays(DWORD t);
extern  UINT    GetWeeks(DWORD t);
extern  UINT    GetDay(DWORD t);
extern  UINT    GetWeek(DWORD t);
extern  UINT    GetWeekYear(DWORD t);
extern  UINT    GetHours(DWORD t);
extern  UINT    GetHour(DWORD t);
extern  UINT    GetMin(DWORD t);
extern  UINT    GetSec(DWORD t);
extern  UINT    GetMonthDays(UINT y, UINT m);

//////////////////////////////////////////////////////////////////////////
//
// CRC32 Generation
//

extern	DWORD	CRC32(PCBYTE pData, UINT uSize);
extern	DWORD	CRC32(PCBYTE pData, UINT uSize, DWORD Crc);

//////////////////////////////////////////////////////////////////////////
//
// API for Code Execution
//

extern DWORD C3ExecuteCode( PCBYTE	  pCode,
			    IDataServer * pData,
			    PDWORD	  pParam
			    );

//////////////////////////////////////////////////////////////////////////
//
// Global Data Items
//

extern IMatrixSsl * g_pMatrix;

// End of File

#endif
