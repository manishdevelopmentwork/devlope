
#include "intern.hpp"

#include "proxy.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Proxy Object
//

// Helper Creation

extern ICommsHelper * Create_CommsHelper(void);

// Instatantation

CProxy * CProxy::Create(CCommsPort *pPort)
{
	CProxy *pProxy = NULL;

	if( GetSpecial(pProxy, pPort->m_DriverID) ) {

		if( pProxy ) {

			pProxy->Bind(pPort, NULL);
			}
		}
	else {
		IDriver *pDriver = AllocDriver(pPort);

		if( !pDriver ) {

			AfxTrace("*** NO DRIVER FOR %4.4X\n", pPort->m_DriverID);
			}
		else {
			switch( pDriver->GetCategory() ) {

				case DC_NULL:

					pProxy = Create_ProxySimple();

					break;

				case DC_COMMS_MASTER:
					
					pProxy = Create_ProxyMaster();
					
					break;

				case DC_COMMS_PROCON:
					
					pProxy = Create_ProxyProCon();
					
					break;

				case DC_COMMS_SLAVE:
					
					pProxy = Create_ProxySlave();
					
					break;

				case DC_COMMS_RAWPORT:

					pProxy = Create_ProxyRaw();

					break;

				case DC_COMMS_DISPLAY:

					pProxy = Create_ProxyDisplay();

					break;

				case DC_COMMS_CAMERA:

					pProxy = Create_ProxyCamera();

					break;
				}

			if( !pProxy ) {

				AfxTrace("*** CAN'T MAKE PROXY FOR %4.4X\n", pPort->m_DriverID);

				pDriver->Release();

				return NULL;
				}

			if( !pProxy->Bind(pPort, pDriver) ) {

				AfxTrace("*** CAN'T BIND PROXY FOR %4.4X\n", pPort->m_DriverID);

				delete pProxy;

				return NULL;
				}
			}
		}

	return pProxy;
	}

// Constructor

CProxy::CProxy(void)
{
	m_pPort        = NULL;
	
	m_pDriver      = NULL;

	m_pComms       = NULL;
	
	m_pCommsHelper = NULL;
	
	m_pNext        = NULL;
	
	m_pPrev        = NULL;
	}

// Destructor

CProxy::~CProxy(void)
{
	AfxRelease(m_pDriver);

	AfxRelease(m_pComms);

	AfxRelease(m_pCommsHelper);
	}

// Binding

BOOL CProxy::Bind(CCommsPort *pPort, IDriver *pDriver)
{
	m_pPort = pPort;

	if( (m_pDriver = pDriver) ) {

		m_pCommsHelper = Create_CommsHelper();

		m_pDriver->SetHelper(m_pCommsHelper);

		m_pDriver->QueryInterface(AfxAeonIID(ICommsDriver), (void **) &m_pComms);

		if( m_pComms ) {

			m_pComms->Load(m_pPort->m_pConfig, m_pPort->m_uConfig);

			return TRUE;
			}
		}

	return FALSE;
	}

// Task Count

UINT CProxy::GetTaskCount(void)
{
	return 1;
	}

// Entry Points

void CProxy::Init(UINT uID)
{
	}

void CProxy::Task(UINT uID)
{
	}

void CProxy::Stop(UINT uID)
{
	}

void CProxy::Term(UINT uID)
{
	}

// Suspension

void CProxy::Suspend(void)
{
	}

void CProxy::Resume(void)
{
	}

// Control

UINT CProxy::Control(void *pContext, UINT uFunc, PCTXT pValue)
{
	AfxTrace("Control %u %s\n", uFunc, pValue);

	return 0;
	}

// Driver Creation

IDriver * CProxy::AllocDriver(CCommsPort *pPort)
{
	if( pPort->m_DriverRpc ) {

		IDriver *pDriver = LoadFromLib(pPort->m_DriverRpc);

		if( pDriver ) {

			pDriver->SetIdentifier(pPort->m_DriverID);

			return pDriver;
			}

		return NULL;
		}
		
	if( !pPort->m_pImage ) {

		return LoadFromLib(pPort->m_DriverID);
		}

	return pPort->m_pDriver;
	}

IDriver * CProxy::LoadFromLib(WORD DriverID)
{
	UINT uSize = 0;

	if( CreateDriver(DriverID, NULL, &uSize) ) {

		PVOID pData = New BYTE [ uSize ];

		CreateDriver(DriverID, pData, NULL);

		return (IDriver *) (CDriver *) pData;
		}
	
	return NULL;
	}

// Implementation

BOOL CProxy::GetSpecial(CProxy * &pProxy, UINT DriverID)
{
	switch( DriverID ) {

		case 0x9998:

			pProxy = Create_ProxyHoneywell(FALSE);

			return TRUE;

		case 0x9999:

			pProxy = Create_ProxyHoneywell(TRUE);

			return TRUE;

		case 0xFE01:

			pProxy = Create_ProxyRack();

			return TRUE;
		}

	return FALSE;
	}

// End of File
