
#include "intern.hpp"

#include "emsonmc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Emerson MC Driver
//

// Instantiator

INSTANTIATE(CEmersonMCDriver);

// Constructor

CEmersonMCDriver::CEmersonMCDriver(void)
{
	m_Ident     = DRIVER_ID;
	
	m_uMaxWords = 16;
	
	m_uMaxBits  = 32;

	m_pTx       = NULL;

	m_pRx       = NULL;

	m_uTimeout  = 1000;

	m_ReadDelay = 0;
	}

// Destructor

CEmersonMCDriver::~CEmersonMCDriver(void)
{
	FreeBuffers();
	}

// Configuration

void MCALL CEmersonMCDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) m_ReadDelay = GetWord(pData);
	
	AllocBuffers();
	}
	
void MCALL CEmersonMCDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) {
		
		Config.m_uFlags |= flagFastRx;
		}
	
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEmersonMCDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEmersonMCDriver::Open(void)
{
	}

// Device

CCODE MCALL CEmersonMCDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop         = GetByte(pData);
			m_pCtx->m_fDisableCheck = TRUE;
			m_pCtx->m_fIgnoreReadEx = TRUE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEmersonMCDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEmersonMCDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = SPACE_HOLD;
	Addr.a.m_Offset = 1;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CEmersonMCDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_ReadDelay ) Sleep(m_ReadDelay);

	CAddress CAdd = (CAddress)Addr;

	CAdd.a.m_Offset += 1; // Config Addresses are 0 based

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			return DoWordRead(CAdd, pData, 1);

		case SPACE_OUTPUT:
			
			return DoBitRead (CAdd, pData, 1);

		case SPACE_INPUT:
			
			return DoBitRead (CAdd, pData, 1);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEmersonMCDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress CAdd = Addr;

	CAdd.a.m_Offset += 1; // Config Addresses are 0 based

//**/	AfxTrace3("\r\n** WRITE** Tab=%d Off=%d Ct=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	switch( CAdd.a.m_Table ) {

		case SPACE_HOLD:

			return DoWordWrite(CAdd, pData, 1);

		case SPACE_OUTPUT:
			
			return DoBitWrite (CAdd, pData, 1);

		case SPACE_INPUT:
			return uCount;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

void CEmersonMCDriver::AllocBuffers(void)
{
	UINT c1 = 2 * m_uMaxWords;
	
	UINT c2 = (m_uMaxBits + 7) / 8;
	
	UINT c3 = max(c1, c2) + 20;

	m_uTxSize = c3;
	
	m_uRxSize = c3;
	
	m_pTx = new BYTE [ m_uTxSize ];

	m_pRx = new BYTE [ m_uRxSize ];
	}

void CEmersonMCDriver::FreeBuffers(void)
{
	if( m_pTx ) {

		delete [] m_pTx;

		m_pTx = NULL;
		}

	if( m_pRx ) {

		delete [] m_pRx;

		m_pRx = NULL;
		}
	}
	
BOOL CEmersonMCDriver::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}
		
	return FALSE;
	}

WORD CEmersonMCDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}
		
	return 0;
	}

BOOL CEmersonMCDriver::IgnoreException(void)
{
	return (m_pCtx->m_fIgnoreReadEx && (m_pRx[1] & 0x80));
	}

// Port Access

void CEmersonMCDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CEmersonMCDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CEmersonMCDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr   = 0;

	m_pRx[1] = 0;
	
	AddByte(m_pCtx->m_bDrop);
	
	AddByte(bOpcode);
	}

void CEmersonMCDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {
	
		m_pTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CEmersonMCDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}
		
// Transport Layer

BOOL CEmersonMCDriver::PutFrame(void)
{
	m_pData->ClearRx();
	
	return BinaryTx();
	}

BOOL CEmersonMCDriver::GetFrame(BOOL fWrite)
{
	return BinaryRx(fWrite);
	}

BOOL CEmersonMCDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {
	
		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_pTx[k]);

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CEmersonMCDriver::BinaryRx(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		UINT uByte;
		
		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}
		
		if( ++uGap >= 5 ) {
			
			if( uPtr >= 4 ) {

				if( m_pCtx->m_fDisableCheck ) {
					
					if( uPtr >= ( fWrite ? 8 : UINT(m_pRx[2] + 5) ) ) {

						return TRUE;
						}
					}
				else {
				
					m_CRC.Preset();
				
					PBYTE p = m_pRx;
				
					UINT  n = uPtr - 2;
			
					for( UINT i = 0; i < n; i++ ) {

						m_CRC.Add(*(p++));
						}

					WORD c1 = IntelToHost(PU2(p)[0]);
				
					WORD c2 = m_CRC.GetValue();
					
					if( c1 == c2 ) {
						 
						return TRUE;
						}
					}
				}
				
			uPtr = 0;
			
			uGap = 0;
			}
		}

	return FALSE;
	}

BOOL CEmersonMCDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {
	
		if( GetFrame(fIgnore) ) {

			return CheckReply(fIgnore);
			}
		}

	return FALSE;
	}

BOOL CEmersonMCDriver::CheckReply(BOOL fIgnore)
{
	if( m_pRx[0] == m_pTx[0] ) {

		if( fIgnore ) {

			return TRUE;
			}
	
		if( !(m_pRx[1] & 0x80) ) {
		
			return TRUE;
			}
		}
		
	return FALSE;
	}

// Read Handlers

CCODE CEmersonMCDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x03);
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(uCount);
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_pRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEmersonMCDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPACE_OUTPUT:
			StartFrame(0x01);
			break;
			
		case SPACE_INPUT:
			StartFrame(0x02);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(Addr.a.m_Offset - 1);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	if( (m_pRx[1] & 0x80) && IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEmersonMCDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_HOLD ) {
			
		StartFrame(6);
			
		AddWord(Addr.a.m_Offset - 1);
			
		AddWord(LOWORD(pData[0]));

		if( Transact(TRUE) ) {
			
			return 1;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CEmersonMCDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SPACE_OUTPUT ) {

		if( uCount == 1 ) {
			
			StartFrame(5);
			
			AddWord(Addr.a.m_Offset - 1);
			
			AddWord(pData[0] ? 0xFF00 : 0x0000);
			}
		else {
			StartFrame(15);

			AddWord(Addr.a.m_Offset - 1);

			AddWord(uCount);

			AddByte((uCount + 7) / 8);

			UINT b = 0;

			BYTE m = 1;

			for( UINT n = 0; n < uCount; n++ ) {

				if( pData[n] ) {
					
					b |= m;
					}

				if( !(m <<= 1) ) {

					AddByte(b);

					b = 0;

					m = 1;
					}
				}

			if( m > 1 ) {

				AddByte(b);
				}
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// End of File
