
#include "Intern.hpp"

#include "DevConElementFloat.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DevConEditCtrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration Float UI Element
//

// Base Class

#define CBaseClass CDevConElementEditBox

// Dynamic Class

AfxImplementDynamicClass(CDevConElementFloat, CBaseClass);

// Constructor

CDevConElementFloat::CDevConElementFloat(void)
{
}

// Destructor

CDevConElementFloat::~CDevConElementFloat(void)
{
}

// Operations

void CDevConElementFloat::CreateControls(CWnd &Wnd, UINT &id)
{
	CBaseClass::CreateControls(Wnd, id);

	m_pEditCtrl->SetContent(EC_FLOAT);
}

void CDevConElementFloat::ParseConfig(CJsonData *pSchema, CJsonData *pField)
{
	CStringArray List;

	pField->GetValue(L"format").Tokenize(List, ',');

	m_rDef   = !List[0].IsEmpty() ? wcstod(List[0], NULL) : 0.0;

	m_rMin   = !List[1].IsEmpty() ? wcstod(List[1], NULL) : 0.0;

	m_rMax   = !List[2].IsEmpty() ? wcstod(List[2], NULL) : 9999.0;

	m_Units  = List[3];

	m_uWidth = 10;
}

// Overridables

void CDevConElementFloat::OnSetData(void)
{
	if( m_Data.IsEmpty() ) {

		m_Data.Printf(L"%f", m_rDef);
	}

	CBaseClass::OnSetData();
}

BOOL CDevConElementFloat::OnCheckData(CString &Data)
{
	double uData = wcstod(Data, NULL);

	if( uData < m_rMin || uData > m_rMax ) {

		CPrintf Error(IDS("The value must be between %f and %f."), m_rMin, m_rMax);

		m_pEditCtrl->Error(Error);

		return FALSE;
	}

	return TRUE;
}

// End of File
