
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramResourceItem_HPP

#define INCLUDE_ProgramResourceItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsSystem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSystemResourceItem;

//////////////////////////////////////////////////////////////////////////
//
// Programming Resource Item
//

class CProgramResourceItem : public CCommsSystem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramResourceItem(void);

		// Persistance
		void Init(void);

	protected:
		// Data Members
		CSystemResourceItem *m_pSystem;

		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
