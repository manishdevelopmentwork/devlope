
#include "Intern.hpp"

#include "TouchGeneric.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Generic Touch Screen Controller
//

// Constructor

CTouchGeneric::CTouchGeneric(void)
{
	StdSetRef();

	m_fBeep = true;

	m_pMap  = NULL;	

	m_uSlot = NOTHING;

	AfxGetObject("display", 0, IDisplay, m_pDisplay);

	AfxGetObject("input-d", 0, IInputQueue, m_pInput);

	AfxGetObject("beeper", 0, IBeeper, m_pBeep);

	AfxGetObject("fram", 0, ISerialMemory, m_pMem);
	}

// Destructor

CTouchGeneric::~CTouchGeneric(void)
{
	m_pDisplay->Release();

	m_pInput->Release();

	m_pBeep->Release();

	m_pMem->Release();
	}

// IUnknown

HRESULT CTouchGeneric::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDevice);

	StdQueryInterface(IDevice);

	StdQueryInterface(ITouchScreen);

	StdQueryInterface(ITouchScreenCalib);

	return E_NOINTERFACE;
	}

ULONG CTouchGeneric::AddRef(void)
{
	StdAddRef();
	}

ULONG CTouchGeneric::Release(void)
{
	StdRelease();
	}

// IDevice

BOOL METHOD CTouchGeneric::Open(void)
{
	m_pDisplay->GetSize(m_xDisp, m_yDisp);

	m_xCells = (m_xDisp + constCellSize - 1) / constCellSize;
	
	m_yCells = (m_yDisp + constCellSize - 1) / constCellSize;

	return TRUE;
	}

// ITouchScreen

void METHOD CTouchGeneric::GetCellSize(int &xCell, int &yCell)
{
	xCell = constCellSize;

	yCell = constCellSize;
	}

void METHOD CTouchGeneric::GetDispCells(int &xDisp, int &yDisp)
{
	xDisp = m_xDisp;

	yDisp = m_yDisp;
	}

void METHOD CTouchGeneric::SetBeepMode(bool fBeep)
{
	m_fBeep = fBeep;
	}

void METHOD CTouchGeneric::SetDragMode(bool fDrag)
{
	}

PCBYTE METHOD CTouchGeneric::GetMap(void)
{
	return m_pMap;
	}

void METHOD CTouchGeneric::SetMap(PCBYTE pMap)
{
	m_pMap = pMap;
	}

bool METHOD CTouchGeneric::GetRaw(int &xRaw, int &yRaw)
{
	xRaw = m_xRaw;

	yRaw = m_yRaw;

	return true;
	}

// ITouchScreenCalib

bool METHOD CTouchGeneric::SetCalib(int xMin, int yMin, int xMax, int yMax)
{
	m_xMin = xMin;
	m_yMin = yMin;
	m_xMax = xMax;
	m_yMax = yMax;

	SaveCalib();

	return true;
	}

void METHOD CTouchGeneric::ClearCalib(bool fPersist)
{
	DefaultCalib();

	if( fPersist ) {

		SaveCalib();
		}
	}

void METHOD CTouchGeneric::LoadCalib(void)
{
	FindCalib();
	}

// Scaling

int CTouchGeneric::Scale(int nRaw, int nMin, int nMax, int nSize, bool fFlip)
{
	int d = ((nRaw - nMin) * nSize / (nMax - nMin));

	return fFlip ? (nSize - 1 - d) : d;
	}

// Implementation

void CTouchGeneric::FindCalib(void)
{
	WORD calib[5];

	if( m_pMem->GetData(m_uSlot, PBYTE(calib), sizeof(calib)) ) {

		if( calib[0] == MAKEWORD('T', 'S') ) {

			m_xMin = SHORT(calib[1]);
			m_yMin = SHORT(calib[2]);
			m_xMax = SHORT(calib[3]);
			m_yMax = SHORT(calib[4]);

			return;
			}
		}

	DefaultCalib();
	}

void CTouchGeneric::SaveCalib(void)
{
	WORD calib[5];

	calib[0] = MAKEWORD('T', 'S');
	calib[1] = WORD(m_xMin);
	calib[2] = WORD(m_yMin);
	calib[3] = WORD(m_xMax);
	calib[4] = WORD(m_yMax);

	m_pMem->PutData(m_uSlot, PCBYTE(calib), sizeof(calib));
	}

bool CTouchGeneric::IsValidTouch(void)
{
	if( m_xRaw >= m_xMin && m_xRaw <= m_xMax ) {

		if( m_yRaw >= m_yMin && m_yRaw <= m_yMax ) {

			m_xPos = Scale(m_xRaw, m_xMin, m_xMax, m_xDisp, m_fFlipX);

			m_yPos = Scale(m_yRaw, m_yMin, m_yMax, m_yDisp, m_fFlipY);

			m_xPos = (m_xPos + 2) / 4;

			m_yPos = (m_yPos + 2) / 4;

			return OnTouchValue();
			}
		}

	return false;
	}

bool CTouchGeneric::IsActive(void)
{
	if( !m_pDisplay->IsBacklightEnabled() ) {

		return true;
		}

	if( m_pMap ) {

		UINT n = m_xPos + m_xDisp * m_yPos;

		BYTE b = (0x80 >> (n%8));

		return (m_pMap[n/8] & b) ? true : false;
		}

	return false;
	}

void CTouchGeneric::PostEvent(UINT uState)
{
	if( m_fBeep ) {

		if( uState == stateDown ) {

			m_pBeep->Beep(72, 50);
			}
		}

	if( m_uCode < NOTHING ) {

		CInput Input;

		Input.x.i.m_Type   = typeKey;

		Input.x.i.m_State  = uState;

		Input.x.i.m_Local  = true;

		Input.x.d.k.m_Code = m_uCode;

		m_pInput->Store(Input.m_Ref);
		}
	else {
		CInput Input;

		Input.x.i.m_Type   = typeTouch;

		Input.x.i.m_State  = uState;

		Input.x.i.m_Local  = true;

		Input.x.d.t.m_XPos = m_xPos;
	
		Input.x.d.t.m_YPos = m_yPos;
	
		m_pInput->Store(Input.m_Ref);		
		}
	}

// Overridables

void CTouchGeneric::DefaultCalib(void)
{
	}

bool CTouchGeneric::OnTouchValue(void)
{
	return true;
	}

// End of File
