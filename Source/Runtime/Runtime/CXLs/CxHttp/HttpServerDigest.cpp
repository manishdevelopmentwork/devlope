
#include "Intern.hpp"

#include "HttpServerDigest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpAuth.hpp"

#include "HttpServerConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Helper
//

// Constructor

CHttpServerDigest::CHttpServerDigest(void)
{
	memset(m_dwUsed, 0, sizeof(m_dwUsed));

	m_pNext = NULL;

	m_pPrev = NULL;
	}

// Operations

BOOL CHttpServerDigest::MatchNonce(CString Nonce)
{
	return m_Nonce == Nonce;
	}

BOOL CHttpServerDigest::MatchOpaque(CString Opaque)
{
	return m_Opaque == Opaque;
	}

UINT CHttpServerDigest::CheckRequest(CHttpAuth *p)
{
	if( p ) {

		m_Algorithm = p->GetParam("algorithm");

		m_qop       = p->GetParam("qop");

		if( m_Algorithm == "md5" || m_Algorithm == "md5-sess" ) {

			if( m_qop == "auth" || m_qop == "auth-int" ) {

				if( m_Realm == p->GetParam("realm") ) {

					if( m_Opaque == p->GetParam("opaque") ) {

						if( m_Nonce == p->GetParam("nonce") ) {

							m_uNonceCount = strtoul(p->GetParam("nc"), NULL, 16);

							if( m_uNonceCount < 32 * elements(m_dwUsed) ) {

								DWORD & dwData = m_dwUsed[m_uNonceCount / 32];

								DWORD   dwMask = 1 <<    (m_uNonceCount % 32);

								if( !(dwData & dwMask) ) {

									m_User        = p->GetParam("username");

									m_ClientNonce = p->GetParam("cnonce");

									m_Digest      = p->GetParam("response");

									dwData |= dwMask;

									return CHttpServerConnection::authOkay;
									}
								}

							return CHttpServerConnection::authStale;
							}

						return CHttpServerConnection::authStale;
						}

					return CHttpServerConnection::authStale;
					}
				}
			}
		}

	return CHttpServerConnection::authFail;
	}

void CHttpServerDigest::UpdateNonce(CString Opaque)
{
	m_Nonce  = MakeNonce();

	m_Opaque = Opaque;

	memset(m_dwUsed, 0, sizeof(m_dwUsed));
	}

// End of File
