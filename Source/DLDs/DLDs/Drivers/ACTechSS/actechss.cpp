#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "actechss.hpp"

WORD CODE_SEG CACTechSSDriver::FCSTABLE[] = {

      0x0000, 0x1189, 0x2312, 0x329b, 0x4624, 0x57ad, 0x6536, 0x74bf,
      0x8c48, 0x9dc1, 0xaf5a, 0xbed3, 0xca6c, 0xdbe5, 0xe97e, 0xf8f7,
      0x1081, 0x0108, 0x3393, 0x221a, 0x56a5, 0x472c, 0x75b7, 0x643e,
      0x9cc9, 0x8d40, 0xbfdb, 0xae52, 0xdaed, 0xcb64, 0xf9ff, 0xe876,
      0x2102, 0x308b, 0x0210, 0x1399, 0x6726, 0x76af, 0x4434, 0x55bd,
      0xad4a, 0xbcc3, 0x8e58, 0x9fd1, 0xeb6e, 0xfae7, 0xc87c, 0xd9f5,
      0x3183, 0x200a, 0x1291, 0x0318, 0x77a7, 0x662e, 0x54b5, 0x453c,
      0xbdcb, 0xac42, 0x9ed9, 0x8f50, 0xfbef, 0xea66, 0xd8fd, 0xc974,
      0x4204, 0x538d, 0x6116, 0x709f, 0x0420, 0x15a9, 0x2732, 0x36bb,
      0xce4c, 0xdfc5, 0xed5e, 0xfcd7, 0x8868, 0x99e1, 0xab7a, 0xbaf3,
      0x5285, 0x430c, 0x7197, 0x601e, 0x14a1, 0x0528, 0x37b3, 0x263a,
      0xdecd, 0xcf44, 0xfddf, 0xec56, 0x98e9, 0x8960, 0xbbfb, 0xaa72,
      0x6306, 0x728f, 0x4014, 0x519d, 0x2522, 0x34ab, 0x0630, 0x17b9,
      0xef4e, 0xfec7, 0xcc5c, 0xddd5, 0xa96a, 0xb8e3, 0x8a78, 0x9bf1,
      0x7387, 0x620e, 0x5095, 0x411c, 0x35a3, 0x242a, 0x16b1, 0x0738,
      0xffcf, 0xee46, 0xdcdd, 0xcd54, 0xb9eb, 0xa862, 0x9af9, 0x8b70,
      0x8408, 0x9581, 0xa71a, 0xb693, 0xc22c, 0xd3a5, 0xe13e, 0xf0b7,
      0x0840, 0x19c9, 0x2b52, 0x3adb, 0x4e64, 0x5fed, 0x6d76, 0x7cff,
      0x9489, 0x8500, 0xb79b, 0xa612, 0xd2ad, 0xc324, 0xf1bf, 0xe036,
      0x18c1, 0x0948, 0x3bd3, 0x2a5a, 0x5ee5, 0x4f6c, 0x7df7, 0x6c7e,
      0xa50a, 0xb483, 0x8618, 0x9791, 0xe32e, 0xf2a7, 0xc03c, 0xd1b5,
      0x2942, 0x38cb, 0x0a50, 0x1bd9, 0x6f66, 0x7eef, 0x4c74, 0x5dfd,
      0xb58b, 0xa402, 0x9699, 0x8710, 0xf3af, 0xe226, 0xd0bd, 0xc134,
      0x39c3, 0x284a, 0x1ad1, 0x0b58, 0x7fe7, 0x6e6e, 0x5cf5, 0x4d7c,
      0xc60c, 0xd785, 0xe51e, 0xf497, 0x8028, 0x91a1, 0xa33a, 0xb2b3,
      0x4a44, 0x5bcd, 0x6956, 0x78df, 0x0c60, 0x1de9, 0x2f72, 0x3efb,
      0xd68d, 0xc704, 0xf59f, 0xe416, 0x90a9, 0x8120, 0xb3bb, 0xa232,
      0x5ac5, 0x4b4c, 0x79d7, 0x685e, 0x1ce1, 0x0d68, 0x3ff3, 0x2e7a,
      0xe70e, 0xf687, 0xc41c, 0xd595, 0xa12a, 0xb0a3, 0x8238, 0x93b1,
      0x6b46, 0x7acf, 0x4854, 0x59dd, 0x2d62, 0x3ceb, 0x0e70, 0x1ff9,
      0xf78f, 0xe606, 0xd49d, 0xc514, 0xb1ab, 0xa022, 0x92b9, 0x8330,
      0x7bc7, 0x6a4e, 0x58d5, 0x495c, 0x3de3, 0x2c6a, 0x1ef1, 0x0f78
};

//////////////////////////////////////////////////////////////////////////
//
// AC Tech Simple Servo Driver
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CACTechSSDriver);

// Constants

static UINT const TIMEOUT = 3000;

// Constructor

CACTechSSDriver::CACTechSSDriver(void)
{
	m_Ident     = DRIVER_ID;

	m_fSeqErr   = FALSE;
}

// Destructor

CACTechSSDriver::~CACTechSSDriver(void)
{
}

// Configuration

void MCALL CACTechSSDriver::Load(LPCBYTE pData)
{
}

void MCALL CACTechSSDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CACTechSSDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CACTechSSDriver::Open(void)
{
	m_pFCS = FCSTABLE;

	m_uSequence = 0x2020;
}

// Device

CCODE MCALL CACTechSSDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx          = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			m_dError        = 0;

			m_fNewError     = FALSE;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CACTechSSDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CACTechSSDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = TSVAR;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrRealAsReal;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
}

CCODE MCALL CACTechSSDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( UINT r = ReadNoTransmit(Addr, pData) ) {

		return r == RCNODATA ? CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY : 1;
	}

	Sleep(150); // device cannot handle 20 updates/sec when running a program

	if( AddRead(Addr) ) {

		if( Transact() ) {

			if( !m_fNewError ) {

				GetResponse(Addr, pData);

				return 1;
			}

			return CCODE_ERROR | CCODE_NO_DATA | CCODE_NO_RETRY;
		}

		return Addr.a.m_Table != TSUNOP ? CCODE_ERROR : 1;
	}

	return 1;
}

CCODE MCALL CACTechSSDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( WriteNoTransmit(Addr, pData) ) {

		return 1;
	}

//**/	AfxTrace0("\r\n** WRITE **\r\n");

	if( AddWrite(Addr, *pData) ) {

		return Transact() ? 1 : CCODE_ERROR;
	}

	return 1;
}

// PRIVATE METHODS

// Frame Building

void CACTechSSDriver::StartFrame(BOOL fIsSpecial)
{
	m_uPtr   = 1;

	m_uFCS   = FCSINIT;

	m_bTxRaw[0] = CFLAG;

	// PPP Header
	AddByte(m_pCtx->m_bDrop);
	AddByte(BCONT);
	AddByte(HIBYTE(PROTOA));
	AddByte(LOBYTE(PROTOA));

	// Data Header
	AddByte(DH1);
	AddByte(DH2);
	AddByte(DH3);
	AddByte(DH4);
	AddByte(DH5);
	AddByte(DH6);
	AddByte(fIsSpecial ? DH7W : DH7);

	if( m_fSeqErr ) {

		m_uSequence--;

		m_fSeqErr = FALSE;
	}

	AddWord(++m_uSequence);
}

void CACTechSSDriver::EndFrame(void)
{
//	AddByte( 0 );
 // Note: FCS bytes must be ex'ored with 0xFF.  Spec does not say that.
	m_bTxRaw[m_uPtr++] = LOBYTE(m_uFCS) ^ 0xFF;

	m_bTxRaw[m_uPtr++] = HIBYTE(m_uFCS) ^ 0xFF;

	m_bTxRaw[m_uPtr++] = CFLAG;
}

void CACTechSSDriver::AddByte(BYTE bData)
{
	AddFCS(&m_uFCS, bData);

	m_bTxRaw[m_uPtr++] =  bData;
}

void CACTechSSDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));
	AddByte(HIBYTE(wData));
}

void CACTechSSDriver::AddLong(DWORD dData)
{
	AddWord(LOWORD(dData));
	AddWord(HIWORD(dData));
}

BOOL CACTechSSDriver::AddRead(AREF Addr)
{
	StartFrame(Addr.a.m_Table == TSUNOP || Addr.a.m_Table == TSID);

	switch( Addr.a.m_Table ) {

		case TSID: // Drive Identification
			AddWord(CMDIAX);
			break;

		case TSOC: // Output Configuration
			AddWord(CMOCR | CMD_OFFSET);
			AddWord(1);
			AddByte(LOBYTE(Addr.a.m_Offset));
			return TRUE;

		case TSVAR: // Indexer variable
			AddWord(CMVARR | CMD_OFFSET);
			AddWord(1);
			AddByte(LOBYTE(Addr.a.m_Offset));
			break;

		case TSUNOP:
			AddWord(LOWORD(m_dCMUNOP) & 0x00FF);
			UINT uC;
			UINT i;
			uC = LOBYTE(HIWORD(m_dCMUNOP));
			if( uC ) {
				AddWord(uC);
				AddByte(HIBYTE(LOWORD(m_dCMUNOP)) & 0xF);
				UINT i;
				for( i = 1; i < uC; i++ ) {
					AddByte(0);
				}
			}
			return TRUE;

		case addrNamed:

			AddReadOpcode(Addr.a.m_Offset);

			break;

		default:
			return FALSE;
	}

	AddByte(0); // read has 1 byte of 0 added to data

	return TRUE;
}

void  CACTechSSDriver::AddReadOpcode(UINT uOffset)
{
// Offsets for some items are programmed as Write function codes, need to be adjusted for reads
	UINT u = 0;

	switch( uOffset ) {

		case	CMCL:	u = CMCLR;	break;
		case	CMPC:	u = CMPCR;	break;
		case	CMPFPG:	u = CMPFPGR;	break;
		case	CMPFIG:	u = CMPFIGR;	break;
		case	CMPFDG:	u = CMPFDGR;	break;
		case	CMVFFG:	u = CMVFFGR;	break;
		case	CMPFIL:	u = CMPFILR;	break;
		case	CMVFPG:	u = CMVFPGR;	break;
		case	CMVFIG:	u = CMVFIGR;	break;
		case	CMMPE:	u = CMMPER;	break;
		case	CMMPET:	u = CMMPETR;	break;

		case	CMEIC:	u = CMEICR;	break;
		case	CMO:	u = CMOR;	break;
		case	CMAOV:	u = CMAOVR;	break;

		case	CMSL:	u = CMSLR;	break;
		case	CMHL:	u = CMHLR;	break;
		case	CMNSL:	u = CMNSLR;	break;
		case	CMPSL:	u = CMPSLR;	break;

		case	CMU:	u = CMUR;	break;
		case	CMA:	u = CMAR;	break;
		case	CMD:	u = CMDR;	break;
		case	CMQD:	u = CMQDR;	break;
		case	CMMV:	u = CMMVR;	break;
		case	CMIPL:	u = CMIPLR;	break;
		case	CMV:	u = CMVR;	break;
		case	CMMER:	u = CMMERR;	break;
		case	CMGC:	u = CMGCR;	break;

		default:	u = uOffset;	break;
	}

	AddWord(u | CMD_OFFSET);
}

BOOL CACTechSSDriver::AddWrite(AREF Addr, DWORD dData)
{
	if( Addr.a.m_Table == TSEEPR ) {

		if( !CanWriteToEEPROM(Addr.a.m_Table, Addr.a.m_Offset) ) {

			return FALSE;
		}

		AddEEPROMWrite(Addr, dData);
	}

	else {
		StartFrame(FALSE);

		AddWriteData(Addr, dData);
	}

	return TRUE;
}

void CACTechSSDriver::AddWriteData(AREF Addr, DWORD dData)
{
	UINT uCommand = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case addrNamed:

			AddWord(uCommand | CMD_OFFSET);

			switch( uCommand ) {

				case CMCL:
				case CMPC:
				case CMPFIG:
				case CMVFFG:
				case CMAOV:
				case CMNSL:
				case CMPSL:
				case CMU:
				case CMA:
				case CMD:
				case CMQD:
				case CMMV:
				case CMSP:
				case CMIPL:
				case CMV:
				case CMGC:
				case CMMP:
				case CMMD:
					AddLength(8);
					PutRealData(&dData);
					break;

				case CMPFPG:
				case CMPFDG:
				case CMVFPG:
				case CMPFIL:
				case CMVFIG:
				case CMMER:
					AddLength(4);
					AddLong(dData);
					break;

				case CMMPET:
				case CMMPE:
					AddLength(2);
					AddWord(LOWORD(dData));
					break;

				case CMSL:
				case CMHL:
				case CMEIC:
				case CMO:
				case CMMPI1:
				case CMMPI0:
				case CMMNI1:
				case CMMNI0:
					AddLength(1);
					AddByte(LOBYTE(dData));
					break;

				case CMRI:
				case CMMCE:
				case CMMCD:
				case CMSUSP:
				case CMRM:
				case CMVME:
				case CMVMD:
				case CMGME:
				case CMGMD:
				case CMS:
				case CMSQ:
					AddLength(0);
					break;

				case CMMDV:
					AddLength(16);
					PutRealData(&m_dCMMDVD);
					PutRealData(&m_dCMMDVV);
					break;

				case CMMDUR:
					AddLength(16);
					PutRealData(&m_dCMMDURD);
					PutRealData(&m_dCMMDURS);
					break;

				case CMMPUR:
					AddLength(16);
					PutRealData(&m_dCMMPURD);
					PutRealData(&m_dCMMPURS);
					break;

				case CMSIP:
					AddLength(16);
					PutRealData(&m_dCMSIPP);
					PutRealData(&m_dCMSIPD);
					break;
			}

			break;

		case TSOC:
			AddWord(CMOC | CMD_OFFSET);
			AddLength(2);
			AddByte(LOBYTE(Addr.a.m_Offset));
			AddByte(LOBYTE(dData));
			break;

		case TSVAR:
			AddWord(CMVAR | CMD_OFFSET);
			AddLength(9);
			AddByte(LOBYTE(Addr.a.m_Offset));
			PutRealData(&dData);
			break;
	}
}

void CACTechSSDriver::AddEEPROMWrite(AREF Addr, DWORD dData)
{
	StartFrame(TRUE);

	if( Addr.a.m_Table == TSOC ) {

		AddWord(CMOCS);
		AddLength(2);
		AddByte(LOBYTE(Addr.a.m_Offset));
		AddByte(LOBYTE(dData));

		return;
	}

	UINT uCmd = 0;
	UINT uLen = 0;

	switch( Addr.a.m_Offset ) {

		case CMPFPG:
			uCmd = CMPFPGS;
			uLen = 4;
			break;

		case CMPFDG:
			uCmd = CMPFDGS;
			uLen = 4;
			break;

		case CMVFPG:
			uCmd = CMVFPGS;
			uLen = 4;
			break;

		case CMPFIL:
			uCmd = CMPFILS;
			uLen = 4;
			break;

		case CMVFIG:
			uCmd = CMVFIGS;
			uLen = 4;
			break;

		case CMCL:
			uCmd = CMCLS;
			uLen = 8;
			break;

		case CMPC:
			uCmd = CMPCS;
			uLen = 8;
			break;

		case CMPFIG:
			uCmd = CMPFIGS;
			uLen = 8;
			break;

		case CMVFFG:
			uCmd = CMVFFGS;
			uLen = 8;
			break;

		case CMMPET:
			uCmd = CMMPETS;
			uLen = 2;
			break;

		case CMMPE:
			uCmd = CMMPES;
			uLen = 2;
			break;

		case CMSL:
			uCmd = CMSLS;
			uLen = 1;
			break;

		case CMHL:
			uCmd = CMHLS;
			uLen = 1;
			break;
	}

	AddWord(uCmd);

	AddLength(uLen);

	switch( uLen ) {

		case 1:
			AddByte(LOBYTE(dData));
			break;

		case 2:
			AddWord(LOWORD(dData));
			break;

		case 4:
			AddLong(dData);
			break;

		case 8:
			PutRealData(&dData);
			break;
	}
}

void CACTechSSDriver::AddLength(UINT uLength)
{
	AddWord(uLength);
}

void CACTechSSDriver::AddZero(void)
{
	AddLong(0);
	AddLong(0);
}

// Transport Layer

BOOL CACTechSSDriver::Transact(void)
{
	Send();

	return GetReply();
}

void CACTechSSDriver::Send(void)
{
	m_pData->ClearRx();

	EndFrame();

	Put();
}

BOOL CACTechSSDriver::GetReply(void)
{
	if( m_pCtx->m_bDrop == BROADC ) { // broadcast

		m_pData->Read(0);

		return TRUE;
	}

	UINT uCount = 0;

	UINT uTimer = 0;

	UINT uState = RCV_CFLAG;

	UINT uData;

	BYTE bData;

	SetTimer(TIMEOUT);

//**/	UINT uSz = 0; AfxTrace0("\r\n");

	while( (uTimer = GetTimer()) ) {

		if( (uData = Get(uTimer)) == NOTHING ) {

			continue;
		}

		bData = LOBYTE(uData);

		switch( uState ) {

			case RCV_CFLAG:

				if( bData == CFLAG ) {

					m_bRx[0] = CFLAG;

//**/					AfxTrace0("<7E>");

					uState   = RCV_NORMAL;

					uCount = 1;
				}

				break;

			case RCV_NORMAL:

				switch( bData ) {

					case CFLAG:

						if( uCount > RSP_DATA0 ) {

							m_bRx[uCount++] = CFLAG;

//**/							AfxTrace0("<7E>  ");

							return TestFCSR(uCount-1) ? CheckResponse() : FALSE;
						}

						uCount = 1; // else reset loop, waiting for data

						break;

					case EFLAG:
						uState = RCV_EFLAG;
						break;

					default:
						m_bRx[uCount++] = bData;

//**/						AfxTrace1("<%2.2x>", m_bRx[uCount-1] );

//**/						if( uCount == uSz ) AfxTrace0("\r\n");

//**/						if( uCount == RSP_DATA0) AfxTrace0("\r\n>>>>    Data = ");

//**/						if( uCount == RSP_SZH ) uSz = RSP_ERR + m_bRx[uCount-1];

						break;
				}

				break;

			case RCV_EFLAG:

				m_bRx[uCount++] = bData ^ EMASK;

				uState = RCV_NORMAL;

//**/				AfxTrace1("<%2.2x>", m_bRx[uCount-1] );

//**/				if( uCount == uSz ) AfxTrace0("\r\n");

//**/				if( uCount == RSP_DATA0) AfxTrace0("\r\n>>>>    Data = ");

//**/				if( uCount == RSP_SZH ) uSz = RSP_ERR + m_bRx[uCount-1];

				break;
		}

		if( uCount >= sizeof(m_bRx) ) {

			return FALSE;
		}
	}

	return FALSE;
}

void CACTechSSDriver::GetResponse(AREF Addr, PDWORD pData)
{
	switch( Addr.a.m_Type ) {

		case addrByteAsByte:

			*pData = GetValue(RSP_DATA0, 1);
			break;

		case addrWordAsWord:

			*pData = GetValue(RSP_DATA0, 2);
			break;

		case addrLongAsLong:

			UINT uPos;

			uPos = Addr.a.m_Table != TSID ? RSP_DATA0 : RSP_DATA0 + (4*Addr.a.m_Offset);

			*pData = GetValue(uPos, 4);

			if( Addr.a.m_Table == TSID ) {

				UINT i;

				DWORD d;

				d  = (*pData & 0xFF000000) >> 24;
				d += (*pData & 0x00FF0000) >> 8;
				d += (*pData & 0x0000FF00) << 8;
				d += (*pData & 0x000000FF) << 24;

				*pData = d;
			}

			break;

		case addrRealAsReal:

			*pData = GetRealData();

			break;
	}
}

// Port Access

void CACTechSSDriver::Put(void)
{
	StoreDataToTx(); // move raw data to the Tx Buffer

	m_pData->Write(PCBYTE(m_bTx), m_uPtr, FOREVER);

//**/	UINT u = m_uPtr;

//**/	UINT v = m_bTxRaw[16] ? m_bTxRaw[16] + 17 : 0;

//**/	AfxTrace0("\r\n[7E]");

//**/	for( UINT i = 1; i < u; i++ ) {

//**/		AfxTrace1("[%2.2x]", m_bTxRaw[i] );

//**/		if( v && i == 17 ) AfxTrace0("\r\n<<<<    Data = ");

//**/		if( i == v ) AfxTrace0("\r\n");

//**/		}
}

UINT CACTechSSDriver::Get(UINT uTime)
{
	return m_pData->Read(uTime);
}

// Helpers

UINT CACTechSSDriver::ReadNoTransmit(AREF Addr, PDWORD pData)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case CMSP:
			case CMRI:
			case CMMCE:
			case CMMCD:
			case CMSUSP:
			case CMRM:
			case CMMP:
			case CMMD:
			case CMMPI1:
			case CMMPI0:
			case CMMNI1:
			case CMMNI0:
			case CMVME:
			case CMVMD:
			case CMGME:
			case CMGMD:
			case CMS:
			case CMSQ:
			case CMSIP:
				return RCNODATA;

			case CMMDV:
			case CMMDUR:
			case CMMPUR:
				*pData = 0;
				return RCISDATA;

			case CMMDVD:
				*pData = m_dCMMDVD;
				return RCISDATA;

			case CMMDVV:
				*pData = m_dCMMDVV;
				return RCISDATA;

			case CMMDURD:
				*pData = m_dCMMDURD;
				return RCISDATA;

			case CMMDURS:
				*pData = m_dCMMDURS;
				return RCISDATA;

			case CMMPURD:
				*pData = m_dCMMPURD;
				return RCISDATA;

			case CMMPURS:
				*pData = m_dCMMPURS;
				return RCISDATA;

			case CMSIPP:
				*pData = m_dCMSIPP;
				return RCISDATA;

			case CMSIPD:
				*pData = m_dCMSIPD;
				return RCISDATA;

			case CMERR:
				*pData = m_dError;
				return RCISDATA;

			case CMUNOP:
				*pData = m_dCMUNOP;
				return RCISDATA;
		}
	}

	if( Addr.a.m_Table == TSEEPR ) {

		*pData = 0;
		return RCISDATA;
	}

	return 0;
}

BOOL CACTechSSDriver::WriteNoTransmit(AREF Addr, PDWORD pData)
{
	if( Addr.a.m_Table == addrNamed ) {

		switch( Addr.a.m_Offset ) {

			case CMDIS:
			case CMAIV:
			case CMGAP:
			case CMGTP:
			case CMGPE:
			case CMGLR:
			case CMMPF:
			case CMMS:
			case CMMRMS:
			case CMMAV:
			case CMMTV:
			case CMMMER:
			case CMMMPC:
				return TRUE;

			case CMMDVD:
				m_dCMMDVD = *pData;
				return TRUE;

			case CMMDVV:
				m_dCMMDVV = *pData;
				return TRUE;

			case CMMDURD:
				m_dCMMDURD = *pData;
				return TRUE;

			case CMMDURS:
				m_dCMMDURS = *pData;
				return TRUE;

			case CMMPURD:
				m_dCMMPURD = *pData;
				return TRUE;

			case CMMPURS:
				m_dCMMPURS = *pData;
				return TRUE;

			case CMSIPP:
				m_dCMSIPP = *pData;
				return TRUE;

			case CMSIPD:
				m_dCMSIPD = *pData;
				return TRUE;

			case CMERR:
				m_dError = 0;
				return TRUE;

			case CMUNOP:
				m_dCMUNOP = *pData;
				return TRUE;
		}
	}

	if( Addr.a.m_Table == TSEEPR ) {

		return !CanWriteToEEPROM(Addr.a.m_Table, Addr.a.m_Offset);
	}

	return Addr.a.m_Table == TSID; // can't write Drive Information
}

BOOL  CACTechSSDriver::CanWriteToEEPROM(UINT uTable, UINT uOffset)
{
	switch( uTable ) {

		case addrNamed:
		case TSEEPR:

			switch( uOffset ) {

				case CMCL:
				case CMPC:
				case CMPFPG:
				case CMPFDG:
				case CMVFPG:
				case CMPFIL:
				case CMVFIG:
				case CMVFFG:
				case CMPFIG:
				case CMMPE:
				case CMMPET:
				case CMHL:
				case CMSL:
				case CMOC:
					return TRUE;

				default:
					return FALSE;
			}

		case TSOC:
			return TRUE;
	}

	return FALSE;
}

DWORD CACTechSSDriver::GetRealData(void)
{
	DWORD dF = GetValue(RSP_DATA0, 4);
	DWORD dI = GetValue(RSP_DATA0 + 4, 4);

	BOOL fNeg = FALSE;

	if( dI & 0x80000000 ) {

		fNeg = TRUE;

		dI  += 1;

		dF  ^= 0xFFFFFFFF;
	}

	char s1[32];
	char s2[16];

	memset(s1, 0, sizeof(s1));
	memset(s2, 0, sizeof(s2));

	char * ssI = &s1[0];
	char * ssF = &s2[0];

	SPrintf(ssI, "%d", dI);

	s1[strlen(ssI)] = '.';

	ConvertDF(&dF); // scale 0-0x100000000 to 0-100000000

	SPrintf(ssF, "%8.8d", dF);

	memcpy(&s1[strlen(ssI)], ssF, 8); // ss2 = "iiiiiiii.ffffffff"

	DWORD dwReal = ATOF((const char *) &s1[0]);

	if( fNeg ) {

		dwReal |= 0x80000000;
	}

	return dwReal;
}

void CACTechSSDriver::PutRealData(PDWORD pData)
{
	if( *pData == 0 || *pData == 0x80000000 ) {

		AddZero();

		return;
	}

	DWORD dData;
	DWORD dDataI;

	char s[36]  = { 0 };
	char sI[10] = { 0 };
	char sF[10] = { 0 };

	char * ss  = &s[0];
	char * ssI = &sI[0];
	char * ssF = &sF[0];

	UINT uPos   = 0;

	BOOL fDone  = FALSE;
	BOOL fNeg   = FALSE;

	if( *pData & 0x80000000 ) {

		fNeg = TRUE;

		*pData &= 0x7FFFFFFF;
	}

	SPrintf(ss, "%f", PassFloat(*pData));

	if( !strlen(ss) ) {

		AddZero();

		return;
	}

	while( ss[uPos] == '+' || ss[uPos] == ' ' || ss[uPos] == '0' ) {

		uPos++;

		if( uPos >= strlen(ss) ) {

			AddZero();
			return;
		}
	}

	if( uPos ) {

		memcpy(ss, &(ss[uPos]), min(strlen(ss), sizeof(s)));
	}

	uPos = 0;

	while( !fDone ) {

		if( ss[uPos] == '.' ) {

			fDone = TRUE;

			if( uPos ) {

				memcpy(ssI, ss, uPos);
			}

			else { // char 0 == '.'
				sI[0] = '0';
				sI[1] = 0;
			}

			memcpy(ssF, &(ss[uPos+1]), min(strlen(ss), 9));
		}

		else {
			if( uPos >= strlen(ss) ) {

				sF[0]   = '0';
				sF[1]   = 0;

				fDone = TRUE;
			}
		}

		++uPos;
	}

	if( !strcmp(ssF, "0") ) {

		AddLong(0);

		dData = ATOI(ssI);

		AddLong(fNeg ? -dData : dData);

		return;
	}

// desired result is the decimal fraction * (2^32).
// e.g. 0.5 should result in 0x80000000

	DWORD dDiv = 625; // = 10000/16

	UINT uDigit;

	dData = 0;

	for( uPos = 0; uPos < strlen(ssF); uPos++ ) {

		uDigit = ssF[uPos] - '0';

		switch( uPos ) { // first 3 digits would overflow

			case 0:
				switch( uDigit ) {

					case 0:	dData = 0; break;
					case 1: dData = 0x19999999;	break; // 2^32 / 10
					case 2: dData = 0x33333333;	break;
					case 3: dData = 0x4CCCCCCC;	break;
					case 4: dData = 0x66666666;	break;
					case 5: dData = 0x80000000;	break;
					case 6: dData = 0x99999999;	break;
					case 7: dData = 0xB3333333;	break;
					case 8: dData = 0xCCCCCCCC;	break;
					case 9: dData = 0xE6666666;	break;
						break;
				}
				break;

			case 1:
				switch( uDigit ) {

					case 0:				break;
					case 1: dData += 0x28F5C28;	break; // 2^32 / 100
					case 2: dData += 0x51EB851;	break;
					case 3: dData += 0x7AE147A;	break;
					case 4: dData += 0xA3D70A3;	break;
					case 5: dData += 0xCCCCCCC;	break;
					case 6: dData += 0xF5C28F5;	break;
					case 7: dData += 0x11EB851E;	break;
					case 8: dData += 0x147AE147;	break;
					case 9: dData += 0x170A3D70;	break;
						break;
				}
				break;

			case 2:
				switch( uDigit ) {

					case 0:				break;
					case 1: dData += 0x418937;	break; // 2^32 / 1000
					case 2: dData += 0x83126E;	break;
					case 3: dData += 0xC49BA5;	break;
					case 4: dData += 0x10624DD;	break;
					case 5: dData += 0x147AE14;	break;
					case 6: dData += 0x189374B;	break;
					case 7: dData += 0x1CAC083;	break;
					case 8: dData += 0x20C49BA;	break;
					case 9: dData += 0x24DD2F1;	break;
						break;
				}
				break;

			default:
				dData += uDigit * 0x10000000 / dDiv;

				dDiv *= 10;
		}
	}

	dDataI = ATOI(ssI);

	if( fNeg ) {

		dDataI = -dDataI - 1;

		dData  ^= 0xFFFFFFFF;
	}

	AddLong(dData);
	AddLong(dDataI);
}

DWORD CACTechSSDriver::GetValue(UINT uPos, UINT uCount)
{
	DWORD dData = 0;

	for( UINT i = 1; i <= uCount; i++ ) {

		dData <<= 8;

		dData += m_bRx[uPos + uCount - i];
	}

	return dData;
}

BOOL  CACTechSSDriver::CheckResponse(void)
{
// Data Format: <0x7E>ACPPHHHHHHHSSFFLLE<LL Bytes of DATA>0<FCS><0x7E>
// A=Addr, C=Control, P=Protocol, H=Fixed Header, SS=Sequence#, FF=Function#, E=Error
// FCS = Frame Check Sequence

	m_fNewError = FALSE;

	if( m_bRx[RSP_CONT] != BCONT ) {

		return FALSE;
	}

	m_fSeqErr = FALSE;

	if( *(PDWORD(&(m_bRx[RSP_SEQL]))) != *(PDWORD(&(m_bTxRaw[RSP_SEQL]))) ) {

		m_fSeqErr = TRUE;

		return FALSE; // Seq # or Function # does not match
	}

	if( m_bRx[RSP_SZL] > 16 || m_bRx[RSP_SZH] ) {

		return FALSE; // response too big
	}

	if( m_bRx[RSP_ERR] ) { // Log Error response - Not a comms error

		m_dError = m_bRx[RSP_FNCH];
		m_dError = ((m_dError <<  8) + m_bRx[RSP_FNCL]) & ~CMD_OFFSET;
		m_dError = (m_dError << 16) + m_bRx[RSP_ERR];

		m_fNewError = TRUE;
	}

	return TRUE;
}

void CACTechSSDriver::ConvertDF(PDWORD pdF)
{
// Scale data 0 - 0x100000000 to 0 - decimal 100000000 ).  G3 resolution is .00000001.
// Each entering digit is multiplied by 10000000 * 1/16.
// Then divided by 16^n (n = digit position) and added to previous 64 bit result
// example: 0x80000000 in -> 50000000 out ( 0x80000000/0x100000000 = .5 )
// example: 0xAAAAAAAA in -> 66666666 out ( 0xAAAAAAAA/0x100000000 = .666...)

	#define K1 6250000 // = 1/16 shifted up so that the decimal digit is digit*10^8

	DWORD dF = *pdF;

	if( dF > 0xFFFFFFCF ) {

		*pdF = 99999999;

		return;
	}

	if( dF < 21 ) { // 42.949.. -> .00000001

		*pdF = 0;

		return;
	}

	DWORD dRH   = 0; // high dword of sum of decimal values
	DWORD dRL   = 0; // low dword of sum of decimal values
	DWORD dR1   = 0; // digit * K1
	DWORD dR2   = 0; // (digit * K1) / (16^n)
	DWORD dMask = 0;

	for( UINT uShift = 0; uShift <= 28; uShift += 4 ) {

		dR1 = ((dF >> (28 - uShift)) & 0xF) * K1;

		dRH += dR1 >> uShift;

		dR2 = (dR1 & dMask) << (32 - uShift);

		if( dR2 + dRL < dRL ) {

			dRH++;
		}

		dRL   += dR2;

		dMask <<= 4;

		dMask |= 0xF;
	}

	*pdF = dRH;
}

void CACTechSSDriver::StoreDataToTx(void)
{
	UINT uEnd = m_uPtr - 1;

	m_uPtr    = 1;

	m_bTx[0]  = CFLAG;

	for( UINT i = 1; i < uEnd; i++ ) {

		BYTE b;

		b = m_bTxRaw[i];

		if( b < EMASK || b == EFLAG || b == CFLAG ) {

			m_bTx[m_uPtr++] = EFLAG;

			m_bTx[m_uPtr++] = b ^ EMASK;
		}

		else {
			m_bTx[m_uPtr++] = b;
		}
	}

	m_bTx[m_uPtr++] = CFLAG;
}

BOOL CACTechSSDriver::TestFCSR(UINT uCount)
{
	UINT uFCS = 0xFFFF;
	UINT i    = 1;

	while( i < uCount ) {

		AddFCS(&uFCS, m_bRx[i++]);
	}

	return uFCS == FCSGOOD;
}

void  CACTechSSDriver::AddFCS(UINT *pFCS, BYTE b)
{
	UINT uFCS = *pFCS;

	*pFCS = (uFCS >> 8) ^ m_pFCS[(uFCS ^ b) & 0xFF];
}

// End of File
