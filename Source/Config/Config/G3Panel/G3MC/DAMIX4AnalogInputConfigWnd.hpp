
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4AnalogInputConfigWnd_HPP

#define INCLUDE_DAMix4AnalogInputConfigWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects 
//

class CDAMix4AnalogInputConfig;

class CDAMix4Module;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Analog Input Config Window
//

class CDAMix4AnalogInputConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix4AnalogInputConfigWnd(UINT uPage);

protected:
	// Data Members
	CDAMix4Module            * m_pModule;
	CDAMix4AnalogInputConfig * m_pItem;
	UINT		           m_uPage;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Implementation
	void AddOperation(void);
	void AddUnits(void);
	void AddRates(void);

	// Data Access
	UINT GetInteger(CMetaItem *pItem, CString Tag);
	void PutInteger(CMetaItem *pItem, CString Tag, UINT Data);

	// Enabling
	void DoEnables(UINT uIndex);
};

// End of File

#endif
