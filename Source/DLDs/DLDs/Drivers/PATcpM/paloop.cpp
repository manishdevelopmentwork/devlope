
#include "intern.hpp"

#include "paloop.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Parker Acroloop Motion Controller Driver
//

// Instantiator

//INSTANTIATE(CAcroloopDriver);

// Constructor

CAcroloopDriver::CAcroloopDriver(void)
{
	m_Ident  = 0x3374;

	m_fAwake = FALSE;

	m_uTxSize = sizeof(m_bTx);

	m_uRxSize = sizeof(m_bRx);

	}

// Destructor

CAcroloopDriver::~CAcroloopDriver(void)
{
	}

// Entry Points

CCODE MCALL CAcroloopDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 0xF2;
	Addr.a.m_Offset = 0;
	Addr.a.m_Extra  = 0;
	Addr.a.m_Type   = addrLongAsLong;

	return Read(Addr, Data, 1);
	}

// Configuration

void MCALL CAcroloopDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CAcroloopDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CAcroloopDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CAcroloopDriver::Open(void)
{	
	}

// Device

CCODE MCALL CAcroloopDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pBase = (CBaseCtx *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pBase = new CBaseCtx;

			m_pBase->m_bDevice = GetByte(pData);

			m_pBase->m_wGVA_Hi = 0;
			m_pBase->m_wGVA_Lo = 0;
			m_pBase->m_wDimGV  = 0;

			pDevice->SetContext(m_pBase);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CAcroloopDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pBase;

		m_pBase = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);

	}

CCODE MCALL CAcroloopDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{		
	if( IsAwake(Addr) ) {

		MakeMin(uCount, 1);
			
		switch( Addr.a.m_Table & 0x0F ) {
		
			case SPACE_PU:
				
				return ReadUserParam(Addr, pData, uCount);
			
			case SPACE_P:
						
				return ReadSysParam(Addr, pData, uCount);

			case SPACE_BF:

				return uCount;
			}
		}

	return CCODE_ERROR;
	}

CCODE MCALL CAcroloopDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{	
	if( IsAwake(Addr) ) { 

		MakeMin(uCount, 1);
			
		switch( Addr.a.m_Table & 0x0F ) {
		
			case SPACE_PU:
			
				return WriteUserParam(Addr, pData, uCount);
				
			case SPACE_P:

				return WriteSysParam(Addr, pData, uCount);

			case SPACE_BF:
			
				return WriteBit(Addr, pData, uCount);
			}
		}
	
	return CCODE_ERROR; 
	}

// Implementation

BOOL CAcroloopDriver::BinaryTx(void)
{
	BYTE m_bSend[64];

	memcpy(m_bSend, m_bTx, sizeof(m_bSend));

	UINT uPtr = m_uPtr;

	Encode(m_bSend, uPtr);

	for( UINT u = 0; u < uPtr; u++ ) {

		m_pData->Write(m_bSend[u], FOREVER);
		}

	return TRUE;
	}

BOOL CAcroloopDriver::BinaryRx(void)
{
	UINT uPtr = 0;
	
	UINT uTimer = 0;

	UINT uData = 0;

	UINT uGap = 0;

	SetTimer(1500);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(5)) < NOTHING ) {
		
			m_bRx[uPtr++] = uData;

			if( uPtr <= m_uRxSize ) {
			
				uGap = 0;
				
				continue;
				}
				
			return FALSE;
			}

		uGap++;

		if( uGap == 4 ) {
			
			if( uPtr >= 4 ) {

				Decode(uPtr);

				return TRUE;
				}
			
			uPtr = uGap = 0;
			}

		Sleep(10);
		}
	
	m_fAwake = FALSE;

	return FALSE;
	}

BOOL CAcroloopDriver::Transact(WORD CheckBytes)
{
	if( BinaryTx() )
	
		return BinaryRx() && CheckReply(CheckBytes);
	
	return FALSE;
	}

BOOL CAcroloopDriver::CheckReply(WORD CheckBytes)
{
	for( UINT u = 0; u < CheckBytes; u++ ) {

		if( m_bRx[u] != m_bTx[u] ) {

			return FALSE;
			} 
		}

	return TRUE;
	}

// Frame Building

void CAcroloopDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;
	
	AddByte(0x00);
	
	AddByte(bOpcode);
	}

void CAcroloopDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {

		m_bTx[m_uPtr++] = bData;
		}
	}

void CAcroloopDriver::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}
		

BOOL CAcroloopDriver::IsAwake(AREF Addr)
{
	if( m_fAwake == TRUE ) {
	
		return TRUE;
		}

	UINT uData = 0;

	UINT uTimer = 0;

	for( UINT i = 0; i < 2; i++ ) {
	
		m_pData->Write(CR, FOREVER);
		
		SetTimer(2000);

		while( (uTimer = GetTimer()) ) {
	
			if( (uData = m_pData->Read(uTimer)) != NOTHING ) {

				if( uData == 0x3E ) {

					if( PeekGVA() ) {

						m_fAwake = TRUE;

						return m_fAwake;
						}

					return FALSE;
					}

				continue;
				}
			}
		}

	return FALSE;
        }

BOOL CAcroloopDriver::PeekGVA(void)
{
	StartFrame(0x90);
	
	AddByte(0x00);
	
	AddByte(0x01);

	UINT uPoint = GetSystemPointer();

	AddWord(LOWORD(uPoint));

	AddWord(HIWORD(uPoint));
	
	if( Transact(8) ) {

		m_pBase->m_wGVA_Lo = ( (m_bRx[9] << 8L) + m_bRx[8] );

		m_pBase->m_wGVA_Hi = ( (m_bRx[11] << 8L) + m_bRx[10] );

		return PeekDimGV();
		}
	
	return FALSE;
	}

BOOL CAcroloopDriver::PeekDimGV(void)
{
	if( !(m_pBase->m_wGVA_Hi || m_pBase->m_wGVA_Lo) ) {
	
		m_pBase->m_wDimGV = 0;

		return TRUE;
		}
	
	StartFrame(0x90);
	
	AddByte(0x00);
	
	AddByte(0x01);
	
	AddWord(m_pBase->m_wGVA_Lo);
	
	AddWord(m_pBase->m_wGVA_Hi);
	
	if( Transact(8) ) {

		m_pBase->m_wDimGV = ( (m_bRx[9] << 8L) + m_bRx[8] );
		
		return TRUE;
		}
	
	return FALSE;
	}

UINT CAcroloopDriver::GetSystemPointer(void)
{
	switch( m_pBase->m_bDevice ) {

		case 2:		return 0x00403E08;
		case 3:		return 0x00400009;
		case 4:		return 0x00C08008;
		case 5:		return 0xA0600020;
		default:	return 0x00400008;
		
		}
	}

UINT CAcroloopDriver::GetPeek(UINT uIndex)
{
	UINT uPeek = MAKELONG(m_pBase->m_wGVA_Lo, m_pBase->m_wGVA_Hi);

	return uPeek + 1 + uIndex * 2;
	}

// Comms Handlers

CCODE CAcroloopDriver::ReadUserParam(AREF Addr, PDWORD pData, UINT uCount)
{
        UINT uIndex = Addr.a.m_Offset;

	if ( uIndex >= m_pBase->m_wDimGV ) {
	
		BOOL fOkay;
		
		if ( m_pBase->m_wDimGV == 0 ) {
		
                        fOkay = PeekGVA();
			
			if ( (!fOkay) || (uIndex > m_pBase->m_wDimGV) ) {

				return CCODE_ERROR;
				}
			}
		
		else {
			fOkay = PeekDimGV();
			
			if ( (!fOkay) || (uIndex > m_pBase->m_wDimGV) ) {
				
				return CCODE_ERROR;
				}
			}
		}  

	StartFrame(0x90);
	
	AddByte(0x01);
	
	AddByte(0x01);

	UINT uPeek = GetPeek(uIndex);
	
	AddWord(LOWORD(uPeek));
	
	AddWord(HIWORD(uPeek));
	
	if( Transact(8) ) {

		DWORD x = PU4(m_bRx + 8)[0];

		pData[0] = IntelToHost(x);

		FixupDataIn(pData[0]);
	
		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CAcroloopDriver::WriteUserParam(AREF Addr, PDWORD pData, UINT uCount)
{
        UINT uIndex = Addr.a.m_Offset;
        
	if ( uIndex >= m_pBase->m_wDimGV ) {
	
		BOOL fOkay;
		
		if ( m_pBase->m_wDimGV == 0 ) {
		
                        fOkay = PeekGVA();
			
			if ( (!fOkay) || (uIndex > m_pBase->m_wDimGV) )  {
			
				return CCODE_ERROR;
				}
			}
		
		else {
			fOkay = PeekDimGV();
			
			if ( (!fOkay) || (uIndex > m_pBase->m_wDimGV) ) {
			
				return CCODE_ERROR;
				}
			}
		}
	
	StartFrame(0x91);
	
	AddByte(0x01);
	
	AddByte(0x01);
		
	UINT uPeek = GetPeek(uIndex);
	
	AddWord(LOWORD(uPeek));
	
	AddWord(HIWORD(uPeek));

	FixupDataOut(pData[0]);
	
	AddWord( LOWORD(pData[0]) );
	
	AddWord( HIWORD(pData[0]) );

	return BinaryTx() ? uCount : CCODE_ERROR;
	}

CCODE CAcroloopDriver::ReadSysParam(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bOpcode = Addr.a.m_Type == addrLongAsLong ? 0x88 : 0x8A;
	
	StartFrame(bOpcode);
	
	AddWord( Addr.a.m_Offset );
	
	if( Transact(4) ) {

		DWORD x = PU4(m_bRx + 4)[0];
			
		pData[0] = IntelToHost(x);

		return uCount;
		}
	
	return CCODE_ERROR;
	}

CCODE CAcroloopDriver::WriteSysParam(AREF Addr, PDWORD pData, UINT uCount)
{
	BYTE bOpcode = Addr.a.m_Type == addrLongAsLong ? 0x89 : 0x8B;
	
	StartFrame(bOpcode);
		
	AddWord( Addr.a.m_Offset );
	
	AddWord( LOWORD(pData[0]) );
	
	AddWord( HIWORD(pData[0]) );
	
	return BinaryTx() ? uCount : CCODE_ERROR;
        }

CCODE CAcroloopDriver::WriteBit(AREF Addr, PDWORD pData, UINT uCount)
{
	m_uPtr = 0;
	
	if( pData[0] ) {

		AddByte(0x1C);
		}
	else {
		AddByte(0x1D);
		}

	AddWord( Addr.a.m_Offset );
	
	BOOL fOkay = BinaryTx();
	
	return fOkay ? uCount : CCODE_ERROR;
	} 

void  CAcroloopDriver::Encode(PBYTE pTx, UINT &n)
{
	n = 0;

	for( UINT u = 0; u < m_uPtr; u++ ) {

		BYTE bData = m_bTx[u];

		if ( (bData <= 0x1F || ( bData >= 0x7F && bData <= 0x9F )) && u > 0 ) {
		
			pTx[n++] = 0x23;
			       
			bData = ( bData ^ 0x40 );
			}
		
		if ( bData == 0x23 ) {
		
			pTx[n++] = 0x23;
			}
					
		pTx[n++] = bData;
		} 
	}

void  CAcroloopDriver::Decode(UINT n)
{
	UINT uPtrCCP = 0, uPtrWoCCP = 0;
				
	while ( uPtrCCP <= n ) {
				
		if( m_bRx[uPtrCCP] != 0x23 ) {
					
			m_bRx[uPtrWoCCP] = m_bRx[uPtrCCP];
			}
		
		else {	 
			uPtrCCP++;
			
			if( m_bRx[uPtrCCP] == 0x23 || m_bRx[uPtrCCP] == 0xA3 ) {
						
				m_bRx[uPtrWoCCP] = m_bRx[uPtrCCP];
				}

			else {
				m_bRx[uPtrWoCCP] = ( m_bRx[uPtrCCP] ^ 0x40);
				}
			}
					
		uPtrCCP++;
					
		uPtrWoCCP++;
		}
	}

void CAcroloopDriver::FixupDataIn(DWORD &dwData)
{
	// Aries CE's transfer data in IEEE64 format
	
	if( dwData > 0 && m_pBase->m_bDevice == 5 ) {
	
		Convert64To32(dwData);
		}
	}

void CAcroloopDriver::FixupDataOut(DWORD &dwData)
{
	// Aries CE's transfer data in IEEE64 format
	
	if( dwData > 0 && m_pBase->m_bDevice == 5 ) {
	
		Convert32To64(dwData);
		}
	}

void  CAcroloopDriver::Convert64To32(DWORD &dwData)
{
	UINT uExp = ((dwData & 0x7FF00000) >> 20) - 1023;  

	UINT uSig = (dwData & 0xFFFFF) << 3;

	dwData &= ~0x7FFFFFFF;

	dwData |= ((uExp + 127) << 23) + uSig;
	}
		
void  CAcroloopDriver::Convert32To64(DWORD &dwData)
{
	UINT uExp = ((dwData & 0x7F800000) >> 23) - 127;  

	UINT uSig = (dwData & 0x7FFFFF) >> 3;

	dwData &= ~0x7FFFFFFF;

	dwData |= ((uExp + 1023) << 20) + uSig;
	}

	

// End of File
