
#include "Intern.hpp"

#include "ProgramManager.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "ProgramList.hpp"
#include "ProgramItem.hpp"
#include "ProgramMonitor.hpp"
#include "ProgramCatWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Program Manager
//

// Dynamic Class

AfxImplementDynamicClass(CProgramManager, CUIItem);

// Constructor

CProgramManager::CProgramManager(void)
{
	m_Handle        = HANDLE_NONE;
	m_pPrograms     = New CProgramList;
	m_AutoCast      = 0;
	m_PrecFix       = 0;
	m_OptLevel      = 2;
	m_DebugEnable   = 0;
	m_DebugPrograms = 1;
	m_DebugActive   = 1;
	m_DebugPassive  = 0;
	m_DebugFlags    = 15;
	m_StoreInFiles  = 0;
	m_pMonitor      = NULL;
	m_pCatWnd       = NULL;
	}

// Destructor

CProgramManager::~CProgramManager(void)
{
	KillMonitor();
	}

// UI Creation

CViewWnd * CProgramManager::CreateView(UINT uType)
{
	if( uType == viewCategory ) {

		CLASS Class = AfxNamedClass(L"CProgramCatWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CProgramNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewResource ) {

		CLASS Class = AfxNamedClass(L"CProgramResTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	return CUIItem::CreateView(uType);
	}

// UI Update

void CProgramManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "DebugEnable" ) {

			DoEnables(pHost);
			}

		if( Tag == "OptLevel" || Tag == "AutoCast" || Tag == "PrecFix" || Tag.StartsWith(L"Debug") ) {

			CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

			pSystem->Validate(TRUE);
			}

		if( Tag == "StoreInFiles" ) {

			if( m_StoreInFiles ) {

				for( INDEX Index = m_pPrograms->GetHead(); !m_pPrograms->Failed(Index); m_pPrograms->GetNext(Index) ) {

					CMetaItem *pItem = m_pPrograms->GetItem(Index);

					if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

						CProgramItem *pProgram = (CProgramItem *) pItem;

						pProgram->CheckSaveCode();
						}
					}
				}

			CheckMonitor();
			}
		}
	}

// Item Naming

CString CProgramManager::GetHumanName(void) const
{
	return CString(IDS_PROGRAMS);
	}

// Operations

void CProgramManager::SetCatWnd(CProgramCatWnd *pCatWnd)
{
	if( m_pMonitor ) {

		m_pMonitor->SetWindow(pCatWnd);
		}

	m_pCatWnd = pCatWnd;
	}

void CProgramManager::Create(CString Name, CString Prot, CString Code)
{
	CProgramItem *pProgram = New CProgramItem;

	m_pPrograms->AppendItem(pProgram);

	pProgram->SetName(Name);

	pProgram->Create(Prot, Code);
	}

void CProgramManager::MonitoredFilesChanged(void)
{
	CStringArray List;

	if( m_pMonitor->GetChangeList(List) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CFilename Name  = List[n];

			CString   Prog  = Name.GetBareName();

			Prog.Replace('-', '.');

			CItem *   pItem = m_pPrograms->FindByName(Prog);

			if( pItem ) {
			
				if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

					CProgramItem *pProg = (CProgramItem *) pItem;

					if( pProg->CheckCodeChange() ) {

						if( m_pCatWnd ) { 
						
							m_pCatWnd->OnReload(pProg);
							}
						}
					}
				}
			}
		}
	}

// Persistance

void CProgramManager::Init(void)
{
	CUIItem::Init();

	m_PrecFix = 1;
	}

void CProgramManager::PostLoad(void)
{
	CUIItem::PostLoad();

	if( m_StoreInFiles ) {

		m_pDbase->AddFlag(L"Recomp");
		}

	CheckMonitor();
	}

void CProgramManager::PostSave(void)
{
	CUIItem::PostSave();

	KillMonitor();

	CheckMonitor();
	}

// Download Support

BOOL CProgramManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_PROG_MANAGER);

	CUIItem::MakeInitData(Init);

	for( UINT p = 0; p < 2; p++ ) {

		UINT n = 0;
	
		for( INDEX Index = m_pPrograms->GetHead(); !m_pPrograms->Failed(Index); m_pPrograms->GetNext(Index) ) {

			CMetaItem *pItem = m_pPrograms->GetItem(Index);

			if( pItem->IsKindOf(AfxRuntimeClass(CProgramItem)) ) {

				if( p == 1 ) {

					CProgramItem *pProg = (CProgramItem *) pItem;

					Init.AddLong(DWORD(pProg->m_Handle));

					Init.AddText(pProg->m_Name);

					Init.AddByte(BYTE(pProg->m_Comms == 1 ? 1 : 0));
					}

				n++;
				}
			}

		if( p == 0 ) {

			Init.AddWord(WORD(n));
			}
		}

	return TRUE;
	}

// Meta Data Creation

void CProgramManager::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(StoreInFiles);
	Meta_AddCollect(Programs);
	Meta_AddInteger(AutoCast);
	Meta_AddInteger(PrecFix);
	Meta_AddInteger(OptLevel);
	Meta_AddInteger(DebugEnable);
	Meta_AddInteger(DebugPrograms);
	Meta_AddInteger(DebugActive);
	Meta_AddInteger(DebugPassive);
	Meta_AddInteger(DebugFlags);

	Meta_SetName((IDS_PROGRAM_MANAGER));
	}

// Implementation

void CProgramManager::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "DebugPrograms", m_DebugEnable);
	pHost->EnableUI(this, "DebugActive",   m_DebugEnable);
	pHost->EnableUI(this, "DebugPassive",  m_DebugEnable);
	pHost->EnableUI(this, "DebugFlags",    m_DebugEnable);
	}

void CProgramManager::CheckMonitor(void)
{
	if( !m_StoreInFiles ) {

		KillMonitor();
		}
	else {
		if( !m_pMonitor ) {

			CFilename Base = m_pDbase->GetFilename();

			if( !Base.IsEmpty() ) {

				CString Name = Base.GetBareName();

				CString Path = Base.GetDirectory() + Name + L"\\Programs\\";

				m_pMonitor = New CProgramMonitor(Path);

				if( m_pCatWnd ) {

					m_pMonitor->SetWindow(m_pCatWnd);
					}

				m_pMonitor->Create();
				}
			}
		}
	}

void CProgramManager::KillMonitor(void)
{
	if( m_pMonitor ) {

		m_pMonitor->Terminate(1000);

		delete m_pMonitor;

		m_pMonitor = NULL;
		}
	}

// End of File
