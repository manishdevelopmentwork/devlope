
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3PRIMS_HPP
	
#define	INCLUDE_G3PRIMS_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <g3comms.hpp>

#include <g3gdi.hpp>

#include <g3ruby.hpp>

#include <g3syms.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3prims.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3PRIMS

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3prims.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//
// Sizing Modes
//

enum
{
	sizeNorm,
	sizeInit,
	sizeMove
	};

//////////////////////////////////////////////////////////////////////////
//
// Class Identifiers
//

#define IDC_FONT	0x7001
#define IDC_IMAGE	0x7002
#define IDC_UI_MANAGER	0x7003
#define IDC_DISP_PAGE	0x7004

//////////////////////////////////////////////////////////////////////////
//
// Primitive Reference Structures
//

typedef CMap  <UINT, UINT> CPrimRefMap;

typedef CTree <UINT>       CPrimRefList;

//////////////////////////////////////////////////////////////////////////
//
// Operator Panel Framer
//

interface IFramer
{
	// Management
	virtual void Release(void) = 0;

	// Attributes
	virtual int    GetTotalKeys(void) const    = 0;
	virtual CRect  GetFrameRect(void) const    = 0;
	virtual CSize  GetTotalSize(void) const    = 0;
	virtual CRect  GetKeyRect(int nKey) const  = 0;
	virtual UINT   GetKeyCode(int nKey) const  = 0;
	virtual CColor GetKeyColor(UINT uCode) const = 0;

	// Operations
	virtual void FindLayout(CSize Size, int nKeys)		    = 0;
	virtual void DrawFrame(CDC &DC, BOOL fWarn, BOOL fSim)	    = 0;
	virtual void DrawKeyBorder(CDC &DC, CRect Rect)		    = 0;
	virtual void DrawKeyText(CDC &DC, CRect Rect, CString Text) = 0;
	virtual void DrawKeyIcon(CDC &DC, CRect Rect, UINT uCode)   = 0;
	};

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CImageManager;
class CFontManager;
class CRubyPatternLib;
class CPrim;
class CPrimList;
class CPrimColor;
class CEventMap;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUISystem;
class CUIManager;
class CPageList;
class CDispFolder;
class CDispPage;
class CDispPageProps;

//////////////////////////////////////////////////////////////////////////
//
// Standard Properties for Pages
//

enum
{
	ppName		= 1,
	ppLabel		= 2,
	ppDescription	= 3,
	};

//////////////////////////////////////////////////////////////////////////
//
// UI System Item
//

class DLLAPI CUISystem : public CCommsSystem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUISystem(BOOL fAux);

		// Destructor
		~CUISystem(void);

		// Attributes
		UINT HasFont(UINT uFont) const;

		// Reference Helpers
		BOOL ReadRef(CTreeFile &Tree, CString const &Name, CPrimRefMap &Refs, BOOL fRefs, BOOL fFixup);
		BOOL SaveRefs(CTreeFile &Tree,CPrimRefList const &Refs);
		BOOL FixRefs(CDispPage *pPage, CPrimRefMap const &Refs, UINT uPass);
		BOOL FixRefs(CPrim *pPrim, CPrimRefMap const &Refs, UINT uPass);
		BOOL AdjustRef(UINT &uRef);
		UINT AcceptRef(CTreeFile &Tree, UINT uOld, BOOL fFixup);
		
		// Conversion
		void PostConvert(void);

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree);
		void PostLoad(void);

		// Property Filters
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CUIManager * m_pUI;
		IFramer    * m_pFramer;
		CSize        m_DispSize;
		UINT	     m_SoftKeys;

	protected:
		// Obsolete Data
		UINT m_LoColor;

		// Static Data
		static CDatabase * m_pDbGraph;
		static UINT        m_uCount;

		// System Data
		void AddNavCats(void);
		void AddResCats(void);
		void AddNavCatHead(void);
		void AddNavCatTail(void);
		void AddResCatHead(void);
		void AddResCatTail(void);

		// Validation
		BOOL DoValidate(CCodedTree &Done, BOOL fExpand, UINT uStep);
		BOOL DoTagCheck(CCodedTree &Done, CIndexTree &Tags, UINT uTag, UINT uStep);

		// Server Creation
		void MakeServers(void);

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL InitAux(void);
		BOOL KillAux(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// UI Manager Object
//

class DLLAPI CUIManager : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIManager(void);

		// Initial Values
		void SetInitValues(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		BOOL CreatePage(CString Name);
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		void PostConvert(void);

		// Data Testing
		BOOL CanAcceptDataObject(IDataObject *pData);

		// Data Acceptance
		BOOL AcceptDataObject(IDataObject *pData, CPrimList *pList, CSize MaxSize);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Load(CTreeFile &Tree);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT              m_Handle;
		CImageManager   * m_pImages;
		CFontManager    * m_pFonts;
		CRubyPatternLib * m_pPatterns;
		CPageList       * m_pScratch;
		CPageList       * m_pPages;
		CEventMap       * m_pEvents;
		CCodedItem      * m_pOnStart;
		CCodedItem      * m_pOnInit;
		CCodedItem      * m_pOnUpdate;
		CCodedItem      * m_pOnSecond;
		UINT              m_TimeLock;
		UINT              m_TimePad;
	//	UINT		  m_TimeDisp;
		UINT		  m_PadSize;
		UINT		  m_PadLayout;
		UINT	          m_PadNumeric;
		UINT	          m_ShowNext;
		UINT		  m_AutoEntry;
		UINT		  m_EntryOrder;
		UINT		  m_TwoMulti;
		UINT		  m_TwoFlag;
		UINT		  m_TwoNumeric;
		UINT		  m_Beeper;
		UINT              m_PopAlignH;
		UINT              m_PopAlignV;
		UINT		  m_GMC1;
		UINT		  m_LedAlarm;
		UINT		  m_LedOrb;
		UINT		  m_LedHome;
		UINT		  m_PopKeypad;
		CCodedItem      * m_pTimeoutDisp;
		
	protected:
		// Data Members
		UINT m_cfCode;
		UINT m_cfTagList;

		// Meta Data
		void AddMetaData(void);

		// Creation Helpers
		BOOL CreatePage(CString *pFull, CString Name);
		BOOL CreatePage(CSysProxy &System, CString *pFull, CString Name, UINT Type);
		BOOL CreatePage(CMetaItem * &pPage, UINT Type);

		// Implementation
		BOOL CanAcceptCode(IDataObject *pData);
		BOOL CanAcceptTags(IDataObject *pData);
		BOOL CanAcceptText(IDataObject *pData);
		BOOL AcceptCode(IDataObject *pData, CPrimList *pList, CSize MaxSize);
		BOOL AcceptTags(IDataObject *pData, CPrimList *pList, CSize MaxSize);
		BOOL AcceptText(IDataObject *pData, CPrimList *pList, CSize MaxSize);
	};

//////////////////////////////////////////////////////////////////////////
//
// Page List
//

class DLLAPI CPageList : public CNamedList
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPageList(BOOL fScratch);

		// Item Access
		CMetaItem * GetItem(INDEX Index) const;
		CMetaItem * GetItem(UINT  uPos ) const;
		CDispPage * GetInit(void) const;

		// Operations
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		void ScanFonts(CFontManager *pFonts);
		void ScanImages(CImageManager *pImages);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);
		void PostConvert(void);
		void PostInit(void);

		// Persistance
		void Init(void);

	protected:
		// Data Members
		BOOL m_fScratch;
	};

//////////////////////////////////////////////////////////////////////////
//
// Page Folder
//

class DLLAPI CDispFolder : public CFolderItem
{
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFolder(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Page Item
//

class DLLAPI CDispPage : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispPage(void);

		// UI Creation
		CViewWnd * CreateView(UINT uType);

		// Operations
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);
		void PostConvert(void);
		void AddHelloWorld(void);

		// Property Access
		DWORD GetProp(WORD ID, UINT Type);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT             m_Handle;
		CString          m_Name;
		CPrimList      * m_pList;
		CEventMap      * m_pEvents;
		CDispPageProps * m_pProps;
		CSize            m_Size;
		UINT              m_Private;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		DWORD FindLabel(void);
		DWORD FindDesc(void);
		int   HCF(int a, int b);

		// Conversion Helpers
		BOOL ConvertSize(CSize NewSize, double &nRatio);
		BOOL ConvertFonts(double nRatio);
		void SizeToFit(CRect NewRect, CRect OldRect);
		void MoveToFit(CRect NewRect, CRect OldRect);
	};

//////////////////////////////////////////////////////////////////////////
//
// Page Properties
//

class DLLAPI CDispPageProps : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispPageProps(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Change
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		void Validate(BOOL fExpand);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedText * m_pLabel;
		CString      m_Desc;
		CPrimColor * m_pBack;
		CCodedItem * m_pMaster;
		CCodedItem * m_pOnSelect;
		CCodedItem * m_pOnRemove;
		CCodedItem * m_pOnUpdate;
		CCodedItem * m_pOnSecond;
		CCodedItem * m_pOnTimeout;
		CCodedItem * m_pPageNext;
		CCodedItem * m_pPagePrev;
		CCodedItem * m_pPageExit;
		UINT	     m_AutoEntry;
		UINT	     m_EntryOrder;
		UINT	     m_NoBack;
		UINT	     m_Timeout;
		UINT	     m_Update;
		UINT         m_PopAlign;
		UINT         m_PopAlignH;
		UINT         m_PopAlignV;
		UINT         m_PopMaster;
		UINT         m_PopKeypad;
		CSecDesc   * m_pSec;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void AddPageHandle(CInitData &Init, CCodedItem *pItem);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUINameServer;
class CUIDataServer;

//////////////////////////////////////////////////////////////////////////
//
// Name Server
//

class DLLAPI CUINameServer : public CNameServer
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUINameServer(CUISystem *pSystem);

		// Initialization
		void LoadLibrary(void);

		// INameServer Methods
		BOOL FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NameIdent(WORD ID, CString &Name);
		BOOL FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type);
		BOOL NameProp(WORD ID, WORD PropID, CString &Name);
		BOOL FindClass(CError *pError, CString Name, UINT &Type);
		BOOL NameClass(UINT Type, CString &Name);
		BOOL SaveClass(UINT Type);

		// Standard Properties
		static BOOL FindPageProp(CString Name, WORD &PropID, CTypeDef &Type);
		static BOOL NamePageProp(WORD PropID, CString &Name);

	protected:
		// Standard Properties
		static PCTXT m_pPageProps[];

		// Data Members
		CUISystem   * m_pSystem;
		CUIManager  * m_pUI;
		CPageList   * m_pPages;

		// Initialization
		void LoadFuncLib1(void);
		void LoadFuncLib2(void);
		void LoadIdentLib(void);

		// Implementation
		BOOL FindPage(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NamePage(WORD ID, CString &Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Data Server
//

class DLLAPI CUIDataServer : public CDataServer
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIDataServer(CUISystem *pSystem);

		// IDataServer Methods
		DWORD GetData(DWORD ID, UINT Type, UINT Flags);
		DWORD GetProp(DWORD ID, WORD Prop, UINT Type);
		DWORD RunFunc(WORD ID, UINT uParam, PDWORD pParam);

	protected:
		// Data Members
		CUISystem   * m_pSystem;
		CUIManager  * m_pUI;
		CPageList   * m_pPages;

		// Implementation
		C3INT ColFlash(C3INT n, C3INT c1, C3INT c2);
		C3INT ColPick2(C3INT n, C3INT c1, C3INT c2);
		C3INT ColPick4(C3INT n1, C3INT n2, C3INT c1, C3INT c2, C3INT c3, C3INT c4);
		C3INT ColBlend(C3REAL d, C3REAL f, C3REAL t, C3INT c1, C3INT c2);
		C3INT ColSelFlash(C3INT n1, C3INT n2, C3INT c1, C3INT c2, C3INT c3);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Reference Flags
//

#define	refPending	0x80000000
#define	refImage	0x01000000
#define	refFont		0x02000000
#define	refTypeMask	0x0F000000

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CImageOpts;
class CImageManager;
class CImageFileList;
class CImageViewList;
class CImageFileItem;
class CImageViewItem;

//////////////////////////////////////////////////////////////////////////
//
// Image Types
//

static UINT const typeBMP  = 1;
static UINT const typeTEX  = 2;
static UINT const typeGDIP = 3;
static UINT const typeEMF  = 4;
static UINT const typeWMF  = 5;
static UINT const typeXAML = 6;

//////////////////////////////////////////////////////////////////////////
//
// Image Options
//

class DLLAPI CImageOpts
{
	public:
		// Constructor
		CImageOpts(void);

		// Comparison Functions
		BOOL IsEntireSame(CImageOpts const &That) const;
		BOOL IsFormatSame(CImageOpts const &That) const;

		// Comparison Operators
		int operator == (CImageOpts const &That) const;

		// Operations
		void SetAspect(CRect &Rect, CSize Aspect) const;
		void SetTransform(CDC &DC, CRect const &Rect) const;
		void ClearTransform(CDC &DC) const;

		// Data Members
		CSize	m_Size;
		UINT	m_Keep;
		INT	m_Rotate;
		UINT	m_Scale;
		UINT	m_Flip;
		INT	m_dx;
		INT	m_dy;

	protected:
		// Implementation
		static void MakeRotate(XFORM &x, INT a);
		static void MakeScale (XFORM &x, UINT s);
		static void MakeFlip  (XFORM &x, UINT f);
		static void MakeTrans (XFORM &x, int dx, int dy);
	};

//////////////////////////////////////////////////////////////////////////
//
// Image Manager
//

class DLLAPI CImageManager : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImageManager(void);

		// Destructor
		~CImageManager(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// View Location
		UINT FindView(UINT uImage, CImageOpts const &Opts);

		// Data Testing
		BOOL CanAcceptDataObject(IDataObject *pData);

		// Data Acceptance
		UINT LoadFromDataObject(IDataObject *pData);
		UINT LoadFromFile(CFilename File, BOOL fKeep);
		UINT LoadFromTreeFile(CTreeFile &Tree, BOOL fFixup);

		// Attributes
		CString GetRelBase(void) const;
		CString GetFilename(UINT uImage) const;
		CSize   GetImageNativeSize(UINT uImage) const;
		CSize   GetImageMinSize(UINT uImage) const;
		CSize   GetImageMaxSize(UINT uImage) const;
		BOOL    IsTexture(UINT uImage) const;
		BOOL    IsMetaFile(UINT uImage) const;
		BOOL    IsXaml(UINT uImage) const;
		UINT    FindImageFile(DWORD CRC) const;

		// Operations
		BOOL UpdateRelative(void);
		void ClearCache(void);
		void InvokeDialog(CViewWnd &Wnd);
		void UpdateUsed(void);
		BOOL MarkUsed(CPrimRefList const &Refs);
		BOOL MarkUsed(UINT uImage);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Data Members
		UINT		 m_SymLevel;
		UINT		 m_Include;
		UINT		 m_RelMode;
		CString		 m_RelBase;
		CString		 m_CfPath;
		CImageFileList * m_pFiles;
		CImageViewList * m_pViews;

		// Rendering Cache Entry
		struct CCache
		{
			BOOL	   m_fUsed;
			DWORD	   m_CRC;
			CImageOpts m_Opts;
			UINT	   m_uBits;
			CByteArray m_Data;
			UINT       m_uHit;
			};

		// Rendering Cache
		CCache m_Cache[128];
		
	protected:
		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Base Path Helpers
		BOOL FindRelBase(void);
		void SaveRelBase(void);
		BOOL FindCfPath(void);
		void SaveCfPath(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Image File List
//

class DLLAPI CImageFileList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImageFileList(void);

		// Destructor
		~CImageFileList(void);

		// Operations
		void DeleteUnused(void);
		void MarkAsUnused(void);
		void SetSymLevel(void);
		void FixTypes(void);

		// Item Location
		CImageFileItem * GetItem(INDEX Index) const;
		CImageFileItem * GetItem(UINT  uPos)  const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Image View List
//

class DLLAPI CImageViewList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImageViewList(void);

		// Destructor
		~CImageViewList(void);

		// Operations
		UINT FindView(UINT uImage, CImageOpts const &Opts);

		// Item Location
		CImageViewItem * GetItem(INDEX Index) const;
		CImageViewItem * GetItem(UINT  uPos)  const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Image File Item
//

class DLLAPI CImageFileItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImageFileItem(void);

		// Destructor
		~CImageFileItem(void);

		// Data Testing
		BOOL CanAcceptDataObject(IDataObject *pData);

		// Data Acceptance
		BOOL LoadFromFile(CFilename File, BOOL fKeep);
		BOOL LoadFromDataObject(IDataObject *pData);
		BOOL LoadFromTreeFile(CTreeFile &Tree, BOOL fFixup);

		// Attributes
		BOOL	IsValid(void) const;
		BOOL    IsOnDisk(void) const;
		BOOL    IsTexture(void) const;
		BOOL    IsMetaFile(void) const;
		BOOL    IsXaml(void) const;
		DWORD   GetCRC32(void) const;
		CSize   GetNativeSize(void) const;
		CSize   GetMinSize(void) const;
		CSize   GetMaxSize(void) const;
		CString GetFilename(void) const;
		CString GetSourceType(void) const;
		CString GetSourceName(void) const;
		CString GetTypeName(void) const;
		CString SuggestExt(void) const;

		// Operations
		BOOL  Preview(CDC &DC, CPoint Pos, CImageOpts const &Opts);
		CSize Render(CByteArray &Data, CImageOpts const &Opts, UINT uBits);
		void  PutHandle(CInitData &Data, CImageOpts const &Opts);
		BOOL  UpdateRelative(void);
		void  SetSymLevel(void);
		void  FixType(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Data Members
		UINT	   m_Used;
		CString	   m_File;
		UINT	   m_Type;
		CByteArray m_Data;
		CSize      m_Nat;
		CSize      m_Min;
		CSize      m_Max;

	protected:
		// Clipboard Formats
		static UINT m_cfFile;
		static UINT m_cfSym;
		static UINT m_cfEMF;
		static UINT m_cfBMP;
		static UINT m_cfTEX;

		// Cache Location
		CImageManager::CCache * m_pCache;
		UINT                    m_nCache;

		// CRC Cache
		DWORD mutable m_CRC;
		BOOL  mutable m_fDirty;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Data Testing Helpers
		BOOL CanAcceptEMF (IDataObject *pData);
		BOOL CanAcceptBMP (IDataObject *pData);
		BOOL CanAcceptTEX (IDataObject *pData);
		BOOL CanAcceptFile(IDataObject *pData);

		// Data Acceptance Helpers
		BOOL AcceptEMF (IDataObject *pData);
		BOOL AcceptBMP (IDataObject *pData);
		BOOL AcceptTEX (IDataObject *pData);
		BOOL AcceptFile(IDataObject *pData);

		// Image Expansion
		BOOL ExpandBMP (CDC &DC, CPoint Pos, CImageOpts const &Opts);
		BOOL ExpandGDIP(CDC &DC, CPoint Pos, CImageOpts const &Opts);
		BOOL ExpandEMF (CDC &DC, CPoint Pos, CImageOpts const &Opts);
		BOOL ExpandXAML(CDC &DC, CPoint Pos, CImageOpts const &Opts);

		// Image Preview
		void PreviewBMP (CDC &DC, CPoint Pos, CImageOpts const &Opts);
		void PreviewGDIP(CDC &DC, CPoint Pos, CImageOpts const &Opts);
		void PreviewEMF (CDC &DC, CPoint Pos, CImageOpts const &Opts);
		void PreviewXAML(CDC &DC, CPoint Pos, CImageOpts const &Opts);
		
		// Image Rendering
		CSize RenderBMP (CByteArray &Data, CImageOpts const &Opts, UINT uBits);
		CSize RenderGDIP(CByteArray &Data, CImageOpts const &Opts, UINT uBits);
		CSize RenderEMF (CByteArray &Data, CImageOpts const &Opts, UINT uBits);
		CSize RenderXAML(CByteArray &Data, CImageOpts const &Opts, UINT uBits);

		// Enhanced Rendering
		void CreateRenderingBitmap(HDC &hDC, PVOID &pBits, HBITMAP &hBits, HBITMAP &hOld, CSize Size, BOOL fAlpha);
		void RenderUsingBitmap16(CByteArray &Data, PVOID pBits, CSize Size, BOOL fAlpha);
		void RenderUsingBitmap32(CByteArray &Data, PVOID pBits, CSize Size, BOOL fAlpha);
		void DeleteRenderingBitmap(HDC hDC, HBITMAP hBits, HBITMAP hOld);

		// Rendering Helper
		CSize RenderViaBitmap(CByteArray &Data, CSize Req, UINT uBits, CDC &DC, CBitmap &Map);

		// Cache Helpers
		BOOL SearchLocalCache(CByteArray &Data, CSize &Size, CImageOpts const &Opts, UINT uBits);
		BOOL SearchViewList(CByteArray &Data, CSize &Size, CImageOpts const &Opts, UINT uBits);

		// Relative Path Handling
		CFilename StripFile(CFilename File) const;
		CFilename BuildFile(CFilename File) const;

		// Drop Name Extraction
		CFilename GetDropName(DROPFILES *pDrop);

		// Implementation
		void  FindCache(void);
		BOOL  FindSizes(void);
		BOOL  DoLoad(CFilename File);
		BOOL  FindType(CString Type);
		PBYTE GrowData(UINT uSize);
		UINT  FindBase(HANDLE hFile);
		BOOL  ConvertWMF(void);
		BOOL  ReduceBitmap(void);
		UINT  GetColorCount(BITMAPINFOHEADER *pHead);
		BOOL  IsSymFile(CString const &File) const;
		UINT  Round(UINT n, UINT f) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Image View Item
//

class DLLAPI CImageViewItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CImageViewItem(void);

		// Destructor
		~CImageViewItem(void);

		// Operations
		BOOL Render(void);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Data Members
		UINT	   m_Handle;
		UINT       m_Image;
		CImageOpts m_Opts;
		CByteArray m_Data;
		BOOL       m_fTile;
		CSize      m_Size;
		
	protected:
		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Render Formats
		void Render8Bit (UINT ColFmt);
		void Render16Bit(UINT ColFmt);
	};

//////////////////////////////////////////////////////////////////////////
//
// Glyph Classes
//

enum {
	glyphNumeric	  = 0x0001,
	glyphCore	  = 0x0002,
	glyphSymbols	  = 0x0004,
	glyphCoreAccented = 0x0008,
	glyphExtended	  = 0x0010,
	glyphExtAccented  = 0x0020,
	glyphHiragana	  = 0x0040,
	glyphFullKatakana = 0x0080,
	glyphHalfKatakana = 0x0100,
	glyphPinYin	  = 0x0200,
	glyphLegacy	  = 0x0400,
	glyphJamo	  = 0x0800,
	glyphHangul	  = 0x1000,
	glyphHebrew	  = 0x2000,
	glyphIdeographs	  = 0x4000,
	glyphComplete     = 0x7AFE,
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CFontManager;
class CFontList;
class CFontItem;

//////////////////////////////////////////////////////////////////////////
//
// Font Manager
//

class DLLAPI CFontManager : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFontManager(void);

		// Destructor
		~CFontManager(void);

		// Operations
		UINT Create(CFontDialog &Dlg);
		UINT Create(CString Face, UINT uSize, BOOL fBold);
		UINT LoadFromTreeFile(CTreeFile &Tree);
		BOOL EnumSystem(CArray <UINT> &List);
		BOOL EnumCustom(CArray <UINT> &List);
		void InvokeDialog(CViewWnd &Wnd);
		void UpdateUsed(void);
		void PurgeUnused(void);
		BOOL MarkUsed(CPrimRefList const &Refs);
		BOOL MarkUsed(UINT uFont);
		BOOL ClearUsed(UINT uFont);
		
		// Font Location
		CFontItem * GetFont(UINT uFont) const;

		// Font Selection
		BOOL Select(IGDI *pGDI, UINT uFont);

		// Font Matching
		UINT FindMatch(CFontItem *pFont) const;

		// Font Attributes
		BOOL    IsCustom(UINT uFont) const;
		BOOL    IsSystem(UINT uFont) const;
		BOOL    IsNumeric(UINT uFont) const;
		BOOL    IsSmooth(UINT uFont) const;
		BOOL    IsComplete(UINT uFont) const;
		BOOL    IsLegacy(UINT uFont) const;
		BOOL    HasGlyphs(UINT uFont, UINT uMask) const;
		BOOL    IsUsed(UINT uFont) const;
		CString GetOptions(UINT uFont) const;
		CString GetFace(UINT uFont) const;
		CString GetName(UINT uFont) const;

		// Font Metrics
		int GetHeight(UINT uFont) const;
		int GetWidth (UINT uFont, PCTXT pText) const;

		// Font Weight
		BOOL IsBold(UINT uFont) const;
		BOOL CanSetBold(UINT uFont) const;
		BOOL CanClearBold(UINT uFont) const;
		BOOL CanFlipBold(UINT uFont) const;
		UINT SetBold(UINT uFont) const;
		UINT ClearBold(UINT uFont) const;
		UINT FlipBold(UINT uFont) const;

		// Font Sizing
		BOOL CanMakeLarger(UINT uFont) const;
		BOOL CanMakeSmaller(UINT uFont) const;
		UINT GetLarger(UINT uFont) const;
		UINT GetSmaller(UINT uFont) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CFontList * m_pFonts;
		
	protected:
		// Data Members
		CMap <UINT, BOOL> m_Used;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Font List
//

class DLLAPI CFontList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFontList(void);

		// Destructor
		~CFontList(void);

		// Item Location
		CFontItem * GetItem(INDEX Index) const;
		CFontItem * GetItem(UINT  uPos ) const;
		CFontItem * GetFont(UINT  uFont) const;

		// Operations
		void MarkAsUnused(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Font Item
//

class DLLAPI CFontItem : public CUIItem , public IGDIFont
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFontItem(void);

		// Destructor
		~CFontItem(void);

		// IBase
		UINT Release(void);

		// IGDIFont Attributes
		BOOL IsProportional(void);
		int  GetBaseLine(void);
		int  GetGlyphWidth(WORD c);
		int  GetGlyphHeight(WORD c);

		// IGDIFont Operations
		void InitBurst(IGDI *pGDI, CLogFont const &Font);
		void DrawGlyph(IGDI *pGDI, int &x, int &y, WORD c);
		void BurstDone(IGDI *pGDI, CLogFont const &Font);

		// Attributes
		CString Describe(void) const;
		BOOL    IsSame(CFontDialog &Dlg) const;
		BOOL    IsSame(CString Face, UINT uSize, BOOL fBold) const;
		BOOL    IsSame(CFontItem *pFont) const;
		UINT    GetFontID(void) const;
		BOOL	IsSmooth(void) const;

		// Operations
		void Create(CFontDialog &Dlg);
		void Create(CString Face, UINT uSize, BOOL fBold);
		void Render(void);
		BOOL LoadFromTreeFile(CTreeFile &Tree);
		BOOL Edit(CWnd &Wnd);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	   m_Handle;
		UINT	   m_Used;
		UINT	   m_Auto;
		UINT	   m_Glyphs;
		UINT	   m_Level;
		UINT	   m_Smooth;
		CString    m_Face;
		INT	   m_Size;
		BOOL	   m_Bold;
		CByteArray m_Data;
		
	protected:
		// Chinese Glyphs
		static BOOL m_fSorted;
		static WORD m_Chinese[];

		// Data Members
		CMemoryDC * m_pDC;
		CFont     * m_pFont;
		UINT        m_uSize;
		PBYTE	    m_pData;
		BOOL        m_fFill;
		COLOR	    m_Fore;
		COLOR	    m_Back;
		UINT	    m_Edit;
		int	    m_nFrom;
		int         m_nBase;
		int	    m_xDigit;
		int	    m_xSpace;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void CreateObjects(void);
		void CreateFont(void);
		INT  ScanGlyphs(void);
		BOOL FindGlyph(GLYPHMETRICS &Metrics, TCHAR cData, BOOL fSmooth, BOOL &fDraw);
		BOOL IsFixed(TCHAR cData);
		BOOL IsFixedDigit(TCHAR cData);
		BOOL IsFixedLetter(TCHAR cData);
		BOOL IsSpace(TCHAR cData);
		BOOL IsExplicitCode(TCHAR cData);
		void AddByte(CByteArray &Data, UINT n);
		void AddWord(CByteArray &Data, UINT n);
		void AddLong(CByteArray &Data, UINT n);
		void RenderFont(void);
		BOOL CanDraw(WORD cData);
		BOOL UseGlyph(WORD cData);

		// Sort Function
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTextFlow;

//////////////////////////////////////////////////////////////////////////
//
// Text Flow Object
//

class DLLAPI CTextFlow
{
	public:
		// Constructor
		CTextFlow(void);

		// Format
		struct CFormat
		{
			INT   m_AlignH;
			INT   m_AlignV;
			INT   m_Lead;
			COLOR m_Color;
			COLOR m_Shadow;
			BOOL  m_fMove;
			UINT  m_MoveDir;
			UINT  m_MoveStep;
			};

		// Attributes
		void GetBoundingRect(R2 &Rect) const;
		void AddBoundingRect(R2 &Rect) const;

		// Operations
		void Dirty(void);
		BOOL Flow(IGDI *pGDI, CUnicode const &Text, R2 const &Rect);
		BOOL Draw(IGDI *pGDI, CFormat  const &Fmt);
		void SetBoundingRect(int x1, int y1, int cx, int cy);

	protected:
		// Data Members
		CUnicode        m_Text;
		R2              m_Rect;
		R2		m_Bound;
		S2		m_Size;
		CArray <CRange> m_Lines;

		// Implementation
		BOOL IsBreak(WCHAR cData);
		void ClearBoundingRect(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimColor;
class CPrimBrush;
class CPrimRubyShade;
class CPrimTankFill;
class CPrimPen;
class CPrimImage;
class CPrimBlock;
class CPrimData;
class CPrimText;
class CPrimAction;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Color
//

class DLLAPI CPrimColor : public CCodedItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimColor(void);

		// Operations
		void Set(COLOR Color);
		void SetInitial(COLOR Color);

		// Color Access
		COLOR GetColor(void);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		COLOR ImportColor(BYTE bData);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Brush
//

class DLLAPI CPrimBrush : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimBrush(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL IsNull(void) const;

		// Operations
		void Set(COLOR Color);

		// Drawing
		void FillRect   (IGDI *pGDI, R2 Rect);
		void FillEllipse(IGDI *pGDI, R2 Rect, UINT uType);
		void FillWedge  (IGDI *pGDI, R2 Rect, UINT uType);
		void FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Pattern;
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;
		UINT         m_Flip;

	protected:
		// Static Data
		static BOOL  m_ShadeMode;
		static COLOR m_ShadeCol1;
		static COLOR m_ShadeCol2;

		// Color Mixing
		static COLOR Mix16(COLOR a, COLOR b, int p, int c);
		static DWORD Mix32(COLOR a, COLOR b, int p, int c);

		// Shaders
		static BOOL Shader1(IGDI *pGDI, int p, int c);
		static BOOL Shader2(IGDI *pGDI, int p, int c);

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Tank Fill
//

class DLLAPI CPrimTankFill : public CPrimBrush
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimTankFill(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		void Set(COLOR Color);
		BOOL StepAddress(void);

		// Drawing
		void FillRect   (IGDI *pGDI, R2 Rect);
		void FillEllipse(IGDI *pGDI, R2 Rect, UINT uType);
		void FillWedge  (IGDI *pGDI, R2 Rect, UINT uType);
		void FillPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Mode;
		CCodedItem * m_pValue;
		CCodedItem * m_pMin;
		CCodedItem * m_pMax;
		CPrimColor * m_pColor3;

	protected:
		// Data Members
		BOOL m_fAutoLimits;

		// Static Data
		static COLOR  m_ShadeCol3;
		static C3REAL m_ShadeData;

		// Shader
		static BOOL Shader(IGDI *pGDI, int p, int c);

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void   DoEnables(IUIHost *pHost);
		C3REAL GetValue(CCodedItem *pItem, C3REAL Default);
		BOOL   HasAutoLimits(void);
		BOOL   PrepTankFill(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Pen
//

class DLLAPI CPrimPen : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimPen(void);

		// UI Managament
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL  IsNull(void) const;
		COLOR GetColor(void) const;
		int   GetAdjust(void) const;
		int   GetWidth(void) const;

		// Operations
		void Set(COLOR Color);
		BOOL AdjustRect(R2 &Rect);
		void Configure(IGDI *pGDI);

		// Drawing
		void DrawRect   (IGDI *pGDI, R2 Rect, UINT uMode);
		void DrawWedge  (IGDI *pGDI, R2 Rect, UINT uType, UINT uMode);
		void DrawEllipse(IGDI *pGDI, R2 Rect, UINT uType, UINT uMode);
		void DrawPolygon(IGDI *pGDI, P2 *pList, UINT uCount, DWORD dwRound, UINT uMode);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT         m_Width;
		UINT         m_Corner;
		CPrimColor * m_pColor;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Image
//

class DLLAPI CPrimImage : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimImage(void);

		// Destructor
		~CPrimImage(void);

		// Creation
		CSize LoadFromFile(CFilename File, CSize MaxSize);
		CSize LoadFromDataObject(IDataObject *pData, CSize MaxSize);
		CSize LoadFromTreeFile(CTreeFile &Tree, CSize MaxSize, BOOL fFixup);

		// Attributes
		BOOL CanAcceptDataObject(IDataObject *pData) const;

		// Operations
		void Draw(IGDI *pGDI, R2 Rect, UINT uMode, UINT rop2);
		void Draw(IGDI *pGDI, R2 Rect, UINT uMode, UINT rop2, CPrimRubyShade *pShade);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT       m_Image;
		CImageOpts m_Opts;

	protected:
		// Data Members
		CImageManager * m_pImages;

		// Implementation
		void  FindManager(void);
		BOOL  FindKeep(void);
		CSize FindInitSize(CSize MaxSize);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Block
//

class DLLAPI CPrimBlock : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimBlock(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL HasError(void) const;
		int  GetFontSize(int nLines) const;
		int  GetTextWidth(PCTXT pText) const;

		// Operations
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);
		BOOL SelectFont(IGDI *pGDI);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		INT   m_AlignH;
		INT   m_AlignV;
		INT   m_Lead;
		UINT  m_Font;
		UINT  m_MoveDir;
		UINT  m_MoveStep;
		CRect m_Margin;

	protected:
		// Data Members
		BOOL m_fError;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL IsPressed(void);
		BOOL CanMove(void);
		BOOL CanEdit(void);
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Data Block
//

class DLLAPI CPrimData : public CPrimBlock
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimData(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL HasError(void) const;
		BOOL CanWrite(void) const;
		BOOL IsString(void) const;
		BOOL IsTagRef(void) const;
		BOOL IsUnboundTag(void) const;
		BOOL GetTagItem(CTag * &pTag) const;
		BOOL GetTagLabel(CCodedText * &pLabel) const;
		BOOL GetTagFormat(CDispFormat * &pFormat) const;
		BOOL GetTagColor(CDispColor * &pColor) const;
		BOOL GetDataLabel(CCodedText * &pLabel) const;
		BOOL GetDataFormat(CDispFormat * &pFormat) const;
		BOOL GetDataColor(CDispColor * &pColor) const;
		BOOL HasTagLabel(void) const;
		BOOL HasTagLimits(void) const;
		BOOL HasTagFormat(void) const;
		BOOL HasTagColor(void) const;
		BOOL UseTagLabel(void) const;
		BOOL UseTagLimits(void) const;
		BOOL UseTagFormat(void) const;
		BOOL UseTagColor(void) const;
		BOOL GetBoundingRect(R2 &Rect) const;
		BOOL AddBoundingRect(R2 &Rect) const;

		// Limit Access
		DWORD GetMinValue(UINT Type) const;
		DWORD GetMaxValue(UINT Type) const;

		// Sizing Text
		CString GetSizingText(void) const;

		// Operations
		BOOL Draw(IGDI *pGDI, R2 Rect, UINT uMode);
		void SetTextColor(COLOR Color);
		void SetContent(UINT Content);
		void SetEntry(UINT Entry);
		void Set(CString Text);
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		BOOL StepAddress(void);
		void SetTimeDate(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Persistance
		void Init(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem *  m_pValue;
		UINT	      m_Entry;
		UINT	      m_Flash;
		UINT	      m_Layout;
		UINT	      m_Content;
		UINT	      m_TagLimits;
		UINT	      m_TagLabel;
		UINT	      m_TagFormat;
		UINT	      m_TagColor;
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pValidate;
		CCodedItem  * m_pOnSetFocus;
		CCodedItem  * m_pOnKillFocus;
		CCodedItem  * m_pOnComplete;
		CCodedItem  * m_pOnError;
		CCodedText  * m_pLabel;
		CCodedItem  * m_pLimitMin;
		CCodedItem  * m_pLimitMax;
		UINT          m_Local;
		UINT	      m_FormType;
		UINT	      m_ColType;
		UINT	      m_UseBack;
		CDispFormat * m_pFormat;
		CDispColor  * m_pColor;
		CPrimColor  * m_pTextColor;
		CPrimColor  * m_pTextShadow;
		CCodedText  * m_pKeyTitle;
		CCodedText  * m_pKeyStatus;
		UINT	      m_KeyNumeric;
		UINT	      m_Accel;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			CString  m_Label;
			CString  m_Value;
			COLOR    m_Back1;
			COLOR    m_Back2;
			COLOR    m_Fore1;
			COLOR    m_Fore2;
			COLOR    m_Shadow;
			BOOL     m_fFocus;
			UINT	 m_uPress;
			};

		// Draw Context
		CCtx m_Ctx;

		// Layout Data
		CTextFlow m_Flow;
		
		// Data Members
		CCodedItem * m_pPrev;

		// Meta Data
		void AddMetaData(void);
		
		// Type Updates
		void UpdateLimitTypes(IUIHost *pHost);
		void UpdateChildTypes(void);

		// Child Updates
		BOOL SetFormatClass(CDispFormat * &pFormat, CLASS Class);
		BOOL SetColorClass (CDispColor  * &pColor,  CLASS Class);

		// Copying from Tag
		BOOL CopyTagLabel(IUIHost *pHost);
		BOOL CopyTagFormat(void);
		BOOL CopyTagColor(void);

		// Text Extraction
		CString FindLabelText(void) const;
		CString FindValueText(DWORD Data, UINT Type) const;

		// Context Creation
		void FindCtx(CCtx &Ctx);

		// Color Setting
		void SetColors(IGDI *pGDI, UINT uMode);
		void SetColors(IGDI *pGDI, COLOR Fore, COLOR Back);

		// Drawing Helpers
		void DrawSingle(IGDI *pGDI, R2 Rect, UINT uMode);
		void DrawMulti (IGDI *pGDI, R2 Rect, UINT uMode);

		// Implementation
		COLOR FindCompColor(COLOR c);
		void  LimitTypes(IUIHost *pHost);
		void  KillUnused(IUIHost *pHost);
		BOOL  KillFormat(void);
		BOOL  KillColor(void);
		BOOL  CheckTagItems(void);
		BOOL  FixTagItems(void);
		void  DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Text Block
//

class DLLAPI CPrimText : public CPrimBlock
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimText(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Attributes
		BOOL IsEmpty(void) const;
		BOOL IsEditable(void) const;
		int  GetLineSize(void) const;
		int  GetTextSize(int nLines) const;
		BOOL GetBoundingRect(R2 &Rect) const;
		BOOL AddBoundingRect(R2 &Rect) const;

		// Operations
		void TextDirty(void);
		BOOL Draw(IGDI *pGDI, R2 Rect, UINT uMode);
		void SetTextColor(COLOR Color);
		void Set(PCTXT pText, BOOL fAll);
		BOOL StepAddress(void);

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedText * m_pText;
		CPrimColor * m_pColor;
		CPrimColor * m_pShadow;

	protected:
		// Layout Data
		CTextFlow m_Flow;
		CString   m_Text;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void InitText(CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action
//

class DLLAPI CPrimAction : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimAction(void);

		// Destructor
		~CPrimAction(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL    IsNull  (void) const;
		BOOL    IsBroken(void) const;
		CString Describe(void) const;

		// Operations
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		BOOL LoadPage(CUIPageList *pList, CString Title);
		BOOL StepAddress(void);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Persistance
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	      m_Mode;
		UINT	      m_Protect;
		UINT	      m_Local;
		CCodedItem  * m_pEnable;
		CCodedItem  * m_pPage;
		UINT	      m_Popup;
		UINT	      m_Button;
		UINT	      m_Delay;
		UINT	      m_Limit;
		CCodedItem  * m_pBitData;
		CCodedItem  * m_pNumData;
		CCodedItem  * m_pNumValue;
		CCodedItem  * m_pNumLimit;
		CCodedItem  * m_pPress;
		CCodedItem  * m_pRepeat;
		CCodedItem  * m_pRelease;
		CString	      m_Tune;

	protected:
		// Data Members
		BOOL m_fTouch;

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Type Updates
		void UpdateNumTypes(IUIHost *pHost);

		// Implementation
		BOOL IsMomentary(void);
		void DoEnables(IUIHost *pHost);
		void BuildCode(CInitData &Init, PCTXT pCode);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimDialog;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Dialog
//

class DLLAPI CPrimDialog : public CItemDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimDialog(CPrim *pPrim, COLOR Back, CString Tab);

		// Destructor
		~CPrimDialog(void);

		// Operations
		void PrimChanged(void);

	protected:
		// Static Data
		static CStringArray m_History;

		// Data Members
		CPrim       * m_pPrim;
		COLOR         m_Back;
		CString       m_Tab;
		CUISystem   * m_pSystem;
		int           m_nZoom;
		CRect	      m_PrimNorm;
		CRect         m_PrimRect;
		CRect	      m_PrimBound;
		CSize         m_PrimSize;
		CSize         m_DrawSize;
		CRect         m_ShowRect;


//		CSize	      m_PrimPad;
//		CRect         m_PrimRect;
//		CSize         m_PrimSize;
//		CSize         m_DrawSize;
		IGdiWindows * m_pWin;
		IGdi        * m_pGdi;
		BOOL          m_fCapture;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPaint(void);
		void OnSize(UINT uCode, CSize Size);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);
		void OnLButtonDown(UINT uCode, CPoint Pos);
		void OnLButtonUp(UINT uCode, CPoint Pos);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCancel(UINT uID);
		BOOL OnViewRefresh(UINT uID);

		// Implementation
		void SaveTab(void);
		void FindPreviewSize(void);
		void FindPreviewLayout(void);
		void CreateGdi(void);
		void DeleteGdi(void);
		void KillCapture(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CEventMap;
class CEventEntry;
class CDualEvent;

//////////////////////////////////////////////////////////////////////////
//
// Event Map
//

class DLLAPI CEventMap : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEventMap(void);

		// Destructor
		~CEventMap(void);

		// Item Location
		CEventEntry * GetItem(INDEX Index) const;

		// Attributes
		BOOL	      HasEntry(UINT uCode) const;
		BOOL	      IsBroken(UINT uCode) const;
		CEventEntry * GetEntry(UINT uCode) const;

		// Operations
		void ForceEntry(UINT uCode);
		void CleanEntry(UINT uCode);
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		void PostConvert(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Event Map Entry
//

class DLLAPI CEventEntry : public CPrimAction
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEventEntry(void);

		// Destructor
		~CEventEntry(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT m_Code;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Dual Event Wrapper
//

class DLLAPI CDualEvent : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDualEvent(void);

		// Destructor
		~CDualEvent(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// Operations
		void SimpleInit(void);

		// Item Properties
		CEventEntry *m_pGlobal;
		CEventEntry *m_pLocal;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimList;
class CPrim;
class CPrimLine;
class CPrimBarcode;
class CPrimWithText;
class CPrimSet;
class CPrimMove;
class CPrimMove2D;
class CPrimMovePolar;
class CPrimGroup;
class CPrimWidget;
class CPrimWidgetActions;
class CPrimWidgetPropData;
class CPrimWidgetPropList;
class CPrimWidgetProp;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Drawing Modes
//

static UINT const drawWhole  = 0;
static UINT const drawDelta  = 1;
static UINT const drawThumb  = 2;
static UINT const drawNoText = 3;
static UINT const drawSet    = 4;
static UINT const drawDialog = 5;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action Modes
//

static UINT const actNone     = 0;
static UINT const actOptional = 1;
static UINT const actAlways   = 2;

//////////////////////////////////////////////////////////////////////////
//
// Primitive Custom Handle
//

struct DLLAPI CPrimHand
{
	// TODO -- This still isn't right. We need a better way
	// of defining handles and managing their behavior during
	// a resize so that certain things are invariant. But we
	// don't want to make the Undo harder. Hmmmmmmm...

	public:
		// Data Members
		PCTXT   m_pTag;
		CPoint  m_Pos;
		UINT    m_uRef;
		CRect   m_Clip;
		};

//////////////////////////////////////////////////////////////////////////
//
// Primitive List
//

class DLLAPI CPrimList : public CItemList
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimList(void);

		// Item Access
		CPrim * GetItem(INDEX Index) const;
		CPrim * GetItem(UINT  uPos ) const;

		// List Operations
		CPrim * RemoveItem(INDEX Index);
		CPrim * RemoveItem(CPrim *pPrim);

		// Searching
		INDEX FindFixedName(CString const &Fixed);
		INDEX FindFixedPath(CString const &Fixed);

		// Attributes
		CRect GetUsedRect(void) const;
		UINT  GetItemIndex(CPrim const *pPrim) const;

		// Operations
		void Draw(IGDI *pGDI, UINT uMode);
		void SizePrims(CRect const &OldRect, CRect const &NewRect);
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);
		void PostConvert(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive
//

class DLLAPI CPrim : public CCodedHost
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrim(void);

		// Destructor
		~CPrim(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Server Access
		INameServer * GetNameServer(CNameServer *pName);
		IDataServer * GetDataServer(CDataServer *pData);

		// Attributes
		R2R   GetReal(void) const;
		CRect GetRect(void) const;
		CRect GetNormRect(void) const;
		BOOL  IsSet(void) const;
		BOOL  IsMove(void) const;
		BOOL  IsGroup(void) const;
		BOOL  IsWidget(void) const;
		BOOL  IsLine(void) const;
		BOOL  IsTextBox(void) const;
		BOOL  HasImages(void) const;
		BOOL  HasProps(void) const;
		BOOL  IsPressed(void) const;

		// Operations
		BOOL HitTest(CPoint Pos);
		void SetReal(R2R   const &NewRect);
		void SetRect(CRect const &NewRect);
		void SetPressed(BOOL fPress);

		// Overridables
		virtual BOOL  HitTest(P2 Pos);
		virtual CRect GetBoundingRect(void);
		virtual void  Draw(IGDI *pGDI, UINT uMode);
		virtual void  SetRect(CRect const &OldRect, CRect const &NewRect);
		virtual void  SetInitState(void);
		virtual void  UpdateLayout(void);
		virtual void  FindTextRect(void);
		virtual CSize GetMinSize(IGDI *pGDI);
		virtual BOOL  GetHand(UINT uHand, CPrimHand &Hand);
		virtual void  SetHand(BOOL fInit);
		virtual void  GetRefs(CPrimRefList &Refs);
		virtual void  EditRef(UINT uOld, UINT uNew);
		virtual UINT  GetBindMode(void);
		virtual BOOL  GetBindState(void);
		virtual BOOL  GetBindClass(CString &Class);
		virtual BOOL  BindToTag(CString Top, CString Tag);
		virtual void  ClearBinding(void);
		virtual void  Validate(BOOL fExpand);
		virtual void  TagCheck(CCodedTree &Done, CIndexTree &Tags);
		virtual void  SetTextColor(COLOR Color);
		virtual BOOL  StepAddress(void);
		virtual BOOL  HasError(void);
		virtual BOOL  IsSupported(void);
		virtual void  PostConvert(void);

		// Item Naming
		CString GetHumanName(void) const;

		// Persistance
		void Init(void);
		void Load(CTreeFile &Tree);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Static Data
		static CPrimWidget * m_pAdopter;

		// Public Data
		R2R	     m_RealRect;
		CCodedItem * m_pVisible;

	protected:
		// Data Members
		UINT m_uType;
		R2   m_DrawRect;
		BOOL m_fPressed;

		// Meta Data
		void AddMetaData(void);

		// Widget Location
		CPrimWidget * FindWidget(void);

		// Implementation
		void SetInitSize(int cx);
		void SetInitSize(int cx, int cy);
		void SetInitSize(CSize Size);
		void FindDrawRect(void);
		BOOL GetFontRef(CPrimRefList &Refs, UINT Font);
		BOOL GetImageRef(CPrimRefList &Refs, UINT Image);
		BOOL EditFontRef(UINT &Font, UINT uOld, UINT uNew);
		BOOL EditImageRef(UINT &Image, UINT uOld, UINT uNew);
		BOOL CanBeAdopted(void);
		BOOL SelectFont(IGDI *pGDI, UINT Font);
	};

//////////////////////////////////////////////////////////////////////////
//
// Line Primitive
//

class DLLAPI CPrimLine : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimLine(void);

		// Overridables
		BOOL HitTest(P2 Pos);
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CPrimPen * m_pPen;

	protected:
		static CPoint m_HitPos;
		static BOOL   m_fHit;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void HitTestPoint(int x, int y);
		
		// DDA Callback
		friend void CALLBACK HitDDA(int x, int y, LPARAM p);
	};

//////////////////////////////////////////////////////////////////////////
//
// Barcode Primitive
//

class DLLAPI CPrimBarcode : public CPrim
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimBarcode(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void SetInitState(void);
		BOOL HasError(void);
		BOOL IsSupported(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		CCodedText * m_pValue;
		UINT	     m_Code;
		UINT	     m_NoShrink;
		UINT	     m_NoGrow;
		UINT	     m_NoNonInt;
		CPrimColor * m_pColor1;
		CPrimColor * m_pColor2;
		UINT	     m_Border;
		INT	     m_AlignH;
		INT	     m_AlignV;

	protected:
		// Data Members
		BOOL m_fError;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		UINT GetMask(void);
		void DrawNotSupported(IGDI *pGDI);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphics Primitive with Text
//

class DLLAPI CPrimWithText : public CPrim
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWithText(void);

		// Destructor
		~CPrimWithText(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// Attributes
		CRect GetTextRect(void) const;
		BOOL  HasText(void) const;
		BOOL  HasData(void) const;
		BOOL  HasAction(void) const;
		BOOL  IsTextEditable(void) const;
		BOOL  IsActionNull(void) const;
		UINT  GetActionMode(void) const;
		int   GetTripSize(void) const;

		// Operations
		BOOL AddText(void);
		BOOL AddData(void);
		BOOL AddAction(void);
		BOOL RemText(void);
		BOOL RemData(void);
		BOOL RemAction(void);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void FindTextRect(void);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);
		void Validate(BOOL fExpand);
		void TagCheck(CCodedTree &Done, CIndexTree &Tags);
		void SetTextColor(COLOR Color);
		BOOL StepAddress(void);
		BOOL HasError(void);

		// Persistance
		void Init(void);
		void PostLoad(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		R2            m_TextRect;
		CPrimText   * m_pTextItem;
		CPrimData   * m_pDataItem;
		CPrimAction * m_pAction;

	protected:
		// Data Members
		UINT m_uActMode;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void LoadHeadPages(CUIPageList *pList);
		void LoadTailPages(CUIPageList *pList);
	};

//////////////////////////////////////////////////////////////////////////
//
// Set Primitive
//

class DLLAPI CPrimSet : public CPrim
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimSet(void);

		// Attributes
		CRect GetUsedRect(void) const;

		// Operations
		void SetSkip(BOOL fSkip);

		// Overridables
		BOOL  HitTest(P2 Pos);
		CRect GetBoundingRect(void);
		void  Draw(IGDI *pGDI, UINT uMode);
		void  GetRefs(CPrimRefList &Refs);
		void  EditRef(UINT uOld, UINT uNew);
		void  Validate(BOOL fExpand);
		void  TagCheck(CCodedTree &Done, CIndexTree &Tags);
		BOOL  StepAddress(void);

		// Persistance
		void PostLoad(void);

		// Property Save Filter
		BOOL SaveProp(CString const &Tag) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		CPrimList * m_pList;
		UINT	    m_LockList;

	protected:
		// Data Members
		BOOL m_fSkip;

		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Movement Primitive
//

class DLLAPI CPrimMove : public CPrimSet
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimMove(void);

		// Operations
		void SetRect(CRect const &NewRect);

		// Overridables
		void SetRect(CRect const &OldRect, CRect const &NewRect);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 2D Movement Primitive
//

class DLLAPI CPrimMove2D : public CPrimMove
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimMove2D(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pPosX;
		CCodedItem * m_pMinX;
		CCodedItem * m_pMaxX;
		CCodedItem * m_pPosY;
		CCodedItem * m_pMinY;
		CCodedItem * m_pMaxY;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Polar Movement Primitive
//

class DLLAPI CPrimMovePolar : public CPrimMove
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimMovePolar(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		CCodedItem * m_pPosT;
		CCodedItem * m_pMinT;
		CCodedItem * m_pMaxT;
		CCodedItem * m_pPosR;
		CCodedItem * m_pMinR;
		CCodedItem * m_pMaxR;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Group Primitive
//

class DLLAPI CPrimGroup : public CPrimSet
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimGroup(void);

		// Operations
		void SetRect(CRect const &NewRect);

		// Overridables
		void SetRect(CRect const &OldRect, CRect const &NewRect);
		BOOL GetBindState(void);
		BOOL BindToTag(CString Top, CString Tag);
		void ClearBinding(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive
//

class DLLAPI CPrimWidget : public CPrimGroup,
			   public IIdentServer,
			   public IFuncServer,
			   public IDataServer
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWidget(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL    IsBound(void) const;
		BOOL    HasZoomPages(void) const;
		CString GetDesc(void) const;

		// Operations
		void SetDataServer(CDataServer *pServer);

		// Overridables
		UINT GetBindMode(void);
		BOOL GetBindState(void);
		BOOL GetBindClass(CString &Class);
		BOOL BindToTag(CString Top, CString Tag);
		void ClearBinding(void);
		BOOL StepAddress(void);

		// Base Methods
		UINT Release(void);

		// Name Server Methods
		BOOL FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NameIdent(WORD ID, CString &Name);
		BOOL FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type);
		BOOL NameProp(WORD ID, WORD PropID, CString &Name);
		BOOL FindFunction(CError *pError, CString Name, WORD &ID, CTypeDef &Type, UINT &uCount);
		BOOL NameFunction(WORD ID, CString &Name);
		BOOL EditFunction(WORD &ID, CTypeDef &Type);
		BOOL GetFuncParam(WORD ID, UINT uParam, CString &Name, CTypeDef &Type);

		// Data Server Methods
		DWORD GetData(DWORD ID, UINT Type, UINT Flags);
		void  SetData(DWORD ID, UINT Type, UINT Flags, DWORD Data);
		DWORD GetProp(DWORD ID, WORD Prop, UINT Type);
		PVOID GetItem(DWORD ID);
		DWORD RunFunc(WORD  ID, UINT uParam, PDWORD pParam);

		// Item Naming
		CString GetHumanName(void) const;

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Public Data
		CString		      m_Desc;
		CString		      m_Cat;
		CString               m_File;
		CSize		      m_Target;
		UINT		      m_LockData;
		CPrimWidgetActions  * m_pActions;
		CPrimWidgetPropData * m_pData;

	protected:
		// Data Members
		IDataServer * m_pServer;
		IDataServer * m_pAbove;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Actions
//

class DLLAPI CPrimWidgetActions : public CCodedHost
{
	public:
		// Dyanamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWidgetActions(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Server Access
		INameServer * GetNameServer(CNameServer *pName);
		IDataServer * GetDataServer(CDataServer *pData);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
		
		// Public Data
		CCodedItem * m_pOnSelect;
		CCodedItem * m_pOnRemove;
		CCodedItem * m_pOnUpdate;
		CCodedItem * m_pOnSecond;

	protected:
		// Meta Data
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property Data
//

class DLLAPI CPrimWidgetPropData : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWidgetPropData(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		BOOL GetBindState(void) const;

		// Operations
		BOOL Bind(CString Top, CString Tag);
		void ClearBinding(void);
		void Recompile(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Persistance
		void Init(void);

		// Public Data
		UINT                  m_Count;
		UINT		      m_AutoBind;
		UINT		      m_AutoZoom;
		CString		      m_Class;
		CString		      m_SubItem;
		CString		      m_Zoom;
		CPrimWidgetPropList * m_pList;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property List
//

class DLLAPI CPrimWidgetPropList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWidgetPropList(void);

		// Item Access
		CPrimWidgetProp * GetItem(INDEX Index) const;
		CPrimWidgetProp * GetItem(UINT  uPos ) const;

		// Item Lookup
		CPrimWidgetProp * FindName(CString const &Name) const;
	};

//////////////////////////////////////////////////////////////////////////
//
// Widget Primitive Property
//

class DLLAPI CPrimWidgetProp : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimWidgetProp(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);
		BOOL GetTypeData(CTypeDef &Type);

		// Attributes
		BOOL GetBindState(void) const;
		BOOL IsDetailsRef(void) const;
		BOOL IsExpression(void) const;
		BOOL IsAction(void) const;
		BOOL IsPageRef(void) const;
		BOOL IsTagRef(void) const;
		UINT GetTagType(void) const;

		// Operations
		BOOL Bind(CString Code, BOOL fForce);
		BOOL ClearBinding(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CString      m_Name;
		CString	     m_Bind;
		CString      m_Desc;
		UINT         m_Type;
		UINT         m_Flags;
		CCodedItem * m_pValue;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimSimpleImage;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Single Image
//

class DLLAPI CPrimSimpleImage : public CPrimWithText
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimSimpleImage(void);

		// Destructor
		~CPrimSimpleImage(void);

		// UI Overridables
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Operations
		void LoadFromDataObject(IDataObject *pData, CSize MaxSize);

		// Overridables
		void Draw(IGDI *pGDI, UINT uMode);
		void GetRefs(CPrimRefList &Refs);
		void EditRef(UINT uOld, UINT uNew);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Members
		UINT	     m_Keep;
		CCodedItem * m_pColor;
		CPrimImage * m_pImage;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
