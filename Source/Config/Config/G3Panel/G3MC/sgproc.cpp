
#include "intern.hpp"

#include "legacy.h"

#include "sgmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Strain Gage Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUISGProcess, CUIEditBox)

// Linked List

CUISGProcess * CUISGProcess::m_pHead = NULL;

CUISGProcess * CUISGProcess::m_pTail = NULL;

// Constructor

CUISGProcess::CUISGProcess(void)
{
	AfxListAppend(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Destructor

CUISGProcess::~CUISGProcess(void)
{
	AfxListRemove(m_pHead, m_pTail, this, m_pNext, m_pPrev);
	}

// Update Support

void CUISGProcess::CheckUpdate(CSGLoop *pLoop, CString const &Tag)
{
	if( Tag.Left(4) == "Disp"     ||
	    Tag.Left(4) == "Proc"     ||
	    Tag.Left(7) == "PVLimit"  ||
	    Tag         == "PVAssign" ){

		CUISGProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextSGProcess *pText = (CUITextSGProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'P' ||
				    pText->m_cType == 'L' ||
				    pText->m_cType == 'S' ){

					if( Tag == "ProcDP" ) {

						pScan->Update(TRUE);
						}

					if( Tag == "PVAssign" ) {

						pScan->Update(TRUE);
						}

					if( Tag == "ProcUnits" ) {

						pScan->Update(TRUE);
						}
					}
				else
					pScan->Update(TRUE);
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag.Left(9) == "AlarmMode" || Tag.Left(11) == "AlarmAssign" ) {

		CUISGProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextSGProcess *pText = (CUITextSGProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'A' ||
				    pText->m_cType == 'B' ||
				    pText->m_cType == 'C' ||
				    pText->m_cType == 'E' ||
				    pText->m_cType == 'H' ||
				    pText->m_cType == 'I' ||
				    pText->m_cType == 'J' ||
				    pText->m_cType == 'K' ){

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag == "LinOutMap" ) {

		CUISGProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextSGProcess *pText = (CUITextSGProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'M' || pText->m_cType == 'N' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag == "SetRampBase" ) {

		CUISGProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextSGProcess *pText = (CUITextSGProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'R' ) {

					pScan->UpdateUnits();
					}
				}

			pScan = pScan->m_pNext;
			}
		}

	if( Tag.Left(10) == "InputRange" ) {

		CUISGProcess *pScan = m_pHead;

		while( pScan ) {

			CUITextSGProcess *pText = (CUITextSGProcess *) pScan->m_pText;

			if( pText->m_pLoop == pLoop ) {

				if( pText->m_cType == 'T' || pText->m_cType == 'U' ) {

					pScan->Update(TRUE);
					}
				}

			pScan = pScan->m_pNext;
			}
		}
	}

// Operations

void CUISGProcess::Update(BOOL fKeep)
{
	CUITextSGProcess *pText = (CUITextSGProcess *) m_pText;

	pText->GetConfig();

	if( fKeep ) {

		m_pDataCtrl->SetModify(TRUE);

		OnSave(FALSE);
		}
	else {
		INT nData = m_pData->ReadInteger(m_pItem);

		INT nCopy = nData;

		pText->Check(CError(FALSE), nData);

		if( nData != nCopy ) {

			m_pData->WriteInteger(m_pItem, UINT(nData));

			m_pItem->SetDirty();
			}
		
		LoadUI();
		}

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

void CUISGProcess::UpdateUnits(void)
{
	CUITextSGProcess *pText = (CUITextSGProcess *) m_pText;

	pText->GetConfig();

	m_Units = pText->GetUnits();

	m_pUnitCtrl->SetWindowText(m_Units);
	}

//////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Strain Gage Process Value
//

// Dynamic Class

AfxImplementDynamicClass(CUITextSGProcess, CUITextInteger)

// Constructor

CUITextSGProcess::CUITextSGProcess(void)
{
	}

// Destructor

CUITextSGProcess::~CUITextSGProcess(void)
{
	}

// Core Overidables

void CUITextSGProcess::OnBind(void)
{
	CUITextInteger::OnBind();

	if( m_pItem->IsKindOf(AfxRuntimeClass(CSGLoop)) ) {

		m_pLoop = (CSGLoop *) m_pItem;
		}
	else {
		if( m_pItem->IsKindOf(AfxRuntimeClass(CSGMapper)) ) {

			CSGMapper *pMapper = (CSGMapper *) m_pItem;

			CSGModule *pModule = (CSGModule *) pMapper->GetParent();

			m_pLoop = pModule->m_pLoop;
			}
		else
			AfxAssert(FALSE);
		}

	m_uWidth = 8;

	m_uLimit = 8;

	GetConfig();
	}

// Implementation

void CUITextSGProcess::GetConfig(void)
{
	m_cType = char(m_UIData.m_Format[0]);

	switch( m_cType ){

		case 'M':
		case 'N':
			if( IsOutputPercent() ) {

				m_uPlaces = 2;
				m_Units   = "%";
				m_nMin    = 0;
				m_nMax    = 20000;

				CheckFlags();

				return;
				}

			break;

		case 'T':
			m_uPlaces = (m_pLoop->m_InputRange1 == SG_INPUT_200MV) ?      2 :      3;
			m_Units   = "mV";
			m_nMin    = (m_pLoop->m_InputRange1 == SG_INPUT_33MV)  ? -33000 : -20000;
			m_nMax    = (m_pLoop->m_InputRange1 == SG_INPUT_33MV)  ?  33000 :  20000;

			CheckFlags();

			return;

		case 'U':
			m_uPlaces = (m_pLoop->m_InputRange2 == SG_INPUT_200MV) ?      2 :      3;
			m_Units   = "mV";
			m_nMin    = (m_pLoop->m_InputRange2 == SG_INPUT_33MV)  ? -33000 : -20000;
			m_nMax    = (m_pLoop->m_InputRange2 == SG_INPUT_33MV)  ?  33000 :  20000;

			CheckFlags();

			return;

		case 'A':
		case 'B':
		case 'C':
		case 'E':
		case 'P':
			m_uPlaces = m_pLoop->m_ProcDP;
			m_Units   = m_pLoop->m_ProcUnits;
			m_nMin    = -30000;
			m_nMax    =  30000;

			CheckFlags();

			return;

		case 'H':
		case 'I':
		case 'J':
		case 'K':
			m_uPlaces = m_pLoop->m_ProcDP;
			m_Units   = m_pLoop->m_ProcUnits;
			m_nMin    = 0;
			m_nMax    = 30000;
			
			CheckFlags();
			
			return;
		}

	switch( GetInputAssignment() ) {

		case 1:
			m_nMin = Min(m_pLoop->m_DispLo1, m_pLoop->m_DispHi1);
			m_nMax = Max(m_pLoop->m_DispLo1, m_pLoop->m_DispHi1);
			break;

		case 2:
			m_nMin = Min(m_pLoop->m_DispLo2, m_pLoop->m_DispHi2);
			m_nMax = Max(m_pLoop->m_DispLo2, m_pLoop->m_DispHi2);
			break;

		default:
			m_nMin = Min(m_pLoop->m_PVLimitLo, m_pLoop->m_PVLimitHi);
			m_nMax = Max(m_pLoop->m_PVLimitLo, m_pLoop->m_PVLimitHi);
			break;
		}

	m_uPlaces = m_pLoop->m_ProcDP;

	m_Units   = m_pLoop->m_ProcUnits;

	CheckType();

	CheckFlags();
	}

BYTE CUITextSGProcess::GetInputAssignment(void)
{
	CSGMapper *pMapper = (CSGMapper *) m_pItem;

	BYTE Channel = 0;

	switch( m_cType ) {

		case 'L':
		case 'T':
			Channel = 1;
			break;

		case 'S':
		case 'U':
			Channel = 2;
			break;

		case 'M':
		case 'N':
			if( pMapper->m_LinOutMap == ANL_INP1 )  Channel = 1;
			if( pMapper->m_LinOutMap == ANL_INP2 )  Channel = 2;
			break;
		}

	return Channel;
	}

void CUITextSGProcess::CheckType(void)
{
	BOOL fDelta = FALSE;

	INT  nRange = 0;

	INT  nLimit = 0;

	switch( m_cType ) {

		case 'L':
		case 'S':
			m_nMin = -30000;

			m_nMax = +30000;

			break;

		case 'R':
		case 'G':
			if( m_cType == 'R' ) {

				CString Rate = GetRateUnits();

				m_Units += Rate;
				}

			nRange = abs(m_nMax - m_nMin);

			nLimit = StoreToDisp(300000);

			MakeMin(nRange, nLimit);

			m_nMin = ((m_cType == 'R') ? 10 : 0);

			m_nMax = nRange;

			break;

		case 'D':
		case 'N':
			fDelta = TRUE;

			break;

		case 'M':
			fDelta = !IsOutputAbsolute();

			break;
		}

	if( fDelta ) {

		nRange = abs(m_nMax - m_nMin);

		nLimit = StoreToDisp(300000);

		MakeMin(nRange, nLimit);

		m_nMin = -nRange;

		m_nMax = +nRange;
		}
	}

void CUITextSGProcess::CheckFlags(void)
{
	if( m_uPlaces ) {
		
		m_uFlags |= textPlaces;
		}
	else 
		m_uFlags &= ~textPlaces;

	if( m_nMin < 0 ) {

		m_uFlags |= textSigned;
		}
	else 
		m_uFlags &= ~textSigned;
	}

// Scaling

INT CUITextSGProcess::StoreToDisp(INT nData)
{
	switch( m_cType ){

		case 'P':
		case 'L':
		case 'S':
		case 'A':
		case 'B':
		case 'C':
		case 'E':
		case 'H':
		case 'I':
		case 'J':
		case 'K':

			return nData;
		}

	double a, b, c;

	GetConstants(a, b, c);

	BOOL fDelta = FALSE;

	switch( m_cType ) {

		case 'R':
		case 'D':
		case 'G':
		case 'N':
			fDelta = TRUE;

			break;

		case 'M':
			fDelta = !IsOutputAbsolute();

			break;
		}

	if( fDelta )

		return Scale(nData, b - a, c, 0);

	return Scale(nData, b - a, c, a);
	}

INT CUITextSGProcess::DispToStore(INT nData)
{
	switch( m_cType ){

		case 'P':
		case 'L':
		case 'S':
		case 'A':
		case 'B':
		case 'C':
		case 'E':
		case 'H':
		case 'I':
		case 'J':
		case 'K':

			return nData;
		}

	double a, b, c;

	GetConstants(a, b, c);

	BOOL fDelta = FALSE;

	switch( m_cType ) {

		case 'R':
		case 'D':
		case 'G':
		case 'N':
			fDelta = TRUE;

			break;

		case 'M':
			fDelta = !IsOutputAbsolute();

			break;
		}

	if( fDelta )

		return Scale(nData, c, b - a, 0);

	return Scale(nData - a, c, b - a, 0);
	}

// Implementation

INT CUITextSGProcess::Scale(double a, double b, double c, double d)
{
	double e = (a * b / c) + d;

	if( e < 0 ) e -= 0.5;

	if( e > 0 ) e += 0.5;

	return INT(e);
	}

void CUITextSGProcess::GetConstants(double &a, double &b, double &c)
{
	if( m_cType == 'M' || m_cType == 'N' ) {

		if( IsOutputPercent() ) {

			a = 0;
			b = 10000;
			c = 100000;
			return;
			}
		}

	if( m_cType == 'T' ) {

		a = (m_pLoop->m_InputRange1 == SG_INPUT_33MV) ? -33000 : -20000;
		b = (m_pLoop->m_InputRange1 == SG_INPUT_33MV) ?  33000 :  20000;
		c = 60000;
		return;
		}

	if( m_cType == 'U' ) {

		a = (m_pLoop->m_InputRange2 == SG_INPUT_33MV) ? -33000 : -20000;
		b = (m_pLoop->m_InputRange2 == SG_INPUT_33MV) ?  33000 :  20000;
		c = 60000;
		return;
		}

	switch( GetInputAssignment() ) {

		case 1:
			a = m_pLoop->m_DispLo1;
			b = m_pLoop->m_DispHi1;
			c = 300000;
			break;

		case 2:
			a = m_pLoop->m_DispLo2;
			b = m_pLoop->m_DispHi2;
			c = 300000;
			break;

		default:
			a = m_pLoop->m_PVLimitLo;
			b = m_pLoop->m_PVLimitHi;
			c = 300000;
			break;
		}
	}

BOOL CUITextSGProcess::IsOutputAbsolute(void)
{
	CSGMapper *pMapper = (CSGMapper *) m_pItem;

	if( pMapper->m_LinOutMap == ANL_ERROR ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CUITextSGProcess::IsOutputPercent(void)
{
	CSGMapper *pMapper = (CSGMapper *) m_pItem;

	switch( pMapper->m_LinOutMap ) {
		
		case ANL_NULL:
		case ANL_COOL:
		case ANL_HEAT:
		case ANL_REMOTE1:
		case ANL_REMOTE2:
		case ANL_REMOTE3:
		case ANL_REMOTE4:

			return TRUE;
		}

	return FALSE;
	}

CString CUITextSGProcess::GetRateUnits(void)
{
	switch(  m_pLoop->m_SetRampBase ) {

		case 0:  return CString(IDS_PER_UNIT);
		case 1:  return CString(IDS_PER_SEC);
		case 2:  return CString(IDS_PER_MIN);
		case 3:  return CString(IDS_PER_HR);
		}

	return L"";
	}

// End of File
