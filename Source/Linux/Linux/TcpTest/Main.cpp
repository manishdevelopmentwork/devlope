
#include "Intern.hpp"

int main(int nArg, char *pArg[])
{
	if( nArg == 3 ) {

		addrinfo hint = { 0 };

		addrinfo *res = NULL;

		hint.ai_family    = AF_INET;
		hint.ai_socktype  = SOCK_STREAM;
		hint.ai_flags     = AI_PASSIVE;
		hint.ai_protocol  = IPPROTO_TCP;

		getaddrinfo(pArg[1], NULL, &hint, &res);

		if( res ) {

			vector<UINT> list;

			for( addrinfo *walk = res; walk; walk = walk->ai_next ) {

				if( walk->ai_addr->sa_family == AF_INET ) {

					UINT a = ((UINT *) (walk->ai_addr->sa_data + 2))[0];

					list.push_back(a);
				}
			}

			freeaddrinfo(res);

			if( list.size() ) {

				char name[256];

				UINT host = list[rand() % list.size()];

				int  port = atoi(pArg[2]);

				inet_ntop(AF_INET, &host, name, sizeof(name));

				struct sockaddr_in addr = { 0 };

				addr.sin_family      = AF_INET;

				addr.sin_port        = htons(port);

				addr.sin_addr.s_addr = host;

				int s = socket(AF_INET, SOCK_STREAM, 0);

				fcntl(s, F_SETFL, fcntl(s, F_GETFL) | O_NONBLOCK);

				printf("connecting to %s port %u\n\n", name, port);

				fflush(stdout);

				int c = connect(s, (sockaddr *) &addr, sizeof(addr));

				if( c == 0 || (c == -1 && errno == EINPROGRESS) ) {

					if( c == -1 ) {

						fd_set set;

						FD_ZERO(&set);

						FD_SET(s, &set);

						timeval tv = { 10, 0 };

						if( select(s+1, NULL, &set, NULL, &tv) != 1 ) {

							fprintf(stderr, "failed to connect\n\n");

							return 1;
						}

						printf("connected\n");

						fflush(stdout);

						bool init = true;

						for( ;;) {

							fd_set set;

							FD_ZERO(&set);

							FD_SET(s, &set);

							timeval tv = { init ? 5 : 1, 0 };

							if( select(s+1, &set, NULL, NULL, &tv) == 1 ) {

								char data[256];

								ssize_t n = read(s, data, sizeof(data)-1);

								if( n > 0 ) {

									if( init ) {

										printf("\n");

										init = false;
									}

									data[n] = 0;

									printf("%s", data);

									fflush(stdout);

									continue;
								}
							}

							if( init ) {

								printf("\n(no data)\n");
							}

							break;
						}

						close(s);

						return 0;
					}
				}

				fprintf(stderr, "failed to connect\n\n");

				return 1;
			}
		}

		fprintf(stderr, "cannot resolve %s\n", pArg[1]);

		return 1;
	}

	fprintf(stderr, "usage: TcpTest <hostname> <port>\n");

	return 2;
}
