
//////////////////////////////////////////////////////////////////////////
//
// Driver Library
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Creation Functions
//

clink void CreateRemotingMasterDriver(void *pData, UINT *pSize);
clink void CreateRemotingSlaveDriver (void *pData, UINT *pSize);
clink void CreateDummySerial	     (void *pData, UINT *pSize);
clink void CreateLinkedSerial	     (void *pData, UINT *pSize);
clink void CreateOpcUaDriver	     (void *pData, UINT *pSize);
clink void CreateDebugTcpDriver	     (void *pData, UINT *pSize);
clink void CreateDebugSerialDriver   (void *pData, UINT *pSize);

//////////////////////////////////////////////////////////////////////////
//
// Creation Dispatcher
//

global BOOL CreateDriver(UINT ID, void *pData, UINT *pSize)
{	
	switch( ID ) {		

		case 0x0001:	CreateRemotingMasterDriver(pData, pSize); break;
		case 0x0002:	CreateRemotingSlaveDriver (pData, pSize); break;
		case 0x3705:	CreateDummySerial	  (pData, pSize); break;
		case 0x3706:	CreateLinkedSerial	  (pData, pSize); break;
		case 0x40E2:	CreateOpcUaDriver	  (pData, pSize); break;
		case 0x40E4:	CreateDebugTcpDriver	  (pData, pSize); break;
		case 0x40E5:	CreateDebugSerialDriver	  (pData, pSize); break;

		default:	return FALSE;
		}
	
	return TRUE;
	}

// End of File
