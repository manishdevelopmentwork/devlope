
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Object Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Waitable Object List
//

// Constructors

CWaitableList::CWaitableList(void)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;
	}

CWaitableList::CWaitableList(WAITREF o1)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;

	m_List.Expand(1);

	m_List.Append(&o1);
	}

CWaitableList::CWaitableList(WAITREF o1, WAITREF o2)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;

	m_List.Expand(2);

	m_List.Append(&o1);

	m_List.Append(&o2);
	}

CWaitableList::CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;

	m_List.Expand(3);

	m_List.Append(&o1);

	m_List.Append(&o2);

	m_List.Append(&o3);
	}

CWaitableList::CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3, WAITREF o4)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;

	m_List.Expand(4);

	m_List.Append(&o1);

	m_List.Append(&o2);

	m_List.Append(&o3);

	m_List.Append(&o4);
	}

CWaitableList::CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3, WAITREF o4, WAITREF o5)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;

	m_List.Expand(5);

	m_List.Append(&o1);

	m_List.Append(&o2);

	m_List.Append(&o3);

	m_List.Append(&o4);

	m_List.Append(&o5);
	}

CWaitableList::CWaitableList(WAITREF o1, WAITREF o2, WAITREF o3, WAITREF o4, WAITREF o5, WAITREF o6)
{
	m_uIndex   = NOTHING;

	m_pHandles = NULL;

	m_List.Expand(6);

	m_List.Append(&o1);

	m_List.Append(&o2);

	m_List.Append(&o3);

	m_List.Append(&o4);

	m_List.Append(&o5);

	m_List.Append(&o6);
	}

// Destructor

CWaitableList::~CWaitableList(void)
{
	FreeHandleList();
	}

// List Attributes

BOOL CWaitableList::IsEmpty(void) const
{
	return m_List.IsEmpty();
	}

BOOL CWaitableList::operator ! (void) const
{
	return m_List.operator ! ();
	}

UINT CWaitableList::GetCount(void) const
{
	return m_List.GetCount();
	}

UINT CWaitableList::GetLimit(void) const
{
	return m_List.GetLimit();
	}

// List Lookup Operator

WAITREF  CWaitableList::operator [] (UINT uIndex) const
{
	return *(m_List.operator [] (uIndex));
	}

// List Data Read

WAITREF  CWaitableList::GetAt(UINT uIndex) const
{
	return *(m_List.GetAt(uIndex));
	}

// List Operations

void CWaitableList::Empty(void)
{
	FreeHandleList();

	m_List.Empty();
	}

UINT CWaitableList::Append(WAITREF Object)
{
	FreeHandleList();

	return m_List.Append(&Object);
	}

void CWaitableList::Append(CWaitableList const &List)
{
	FreeHandleList();

	m_List.Append(List.m_List);
	}

UINT CWaitableList::Insert(UINT uIndex, WAITREF Object)
{
	FreeHandleList();

	return m_List.Insert(uIndex, &Object);
	}

void CWaitableList::Insert(UINT uIndex, CWaitableList const &List)
{
	FreeHandleList();

	m_List.Insert(uIndex, List.m_List);
	}

void CWaitableList::Remove(UINT uIndex, UINT uCount)
{
	FreeHandleList();

	m_List.Remove(uIndex, uCount);
	}

void CWaitableList::Remove(UINT uIndex)
{
	FreeHandleList();

	m_List.Remove(uIndex);
	}

// Wait Operations

UINT CWaitableList::WaitForAllObjects(DWORD Timeout)
{
	MakeHandleList();

	DWORD r = WaitForMultipleObjects( m_uCount,
					  m_pHandles,
					  TRUE,
					  Timeout
					  );
	
	if( r >= WAIT_OBJECT_0 && r < WAIT_OBJECT_0 + m_uCount ) {

		m_uIndex = NOTHING;

		return waitSignal;
		}

	if( r >= WAIT_ABANDONED && r < WAIT_ABANDONED + m_uCount ) {

		m_uIndex = NOTHING;

		return waitSignal;
		}

	if( r == WAIT_TIMEOUT ) {

		return waitTimeout;
		}

	AfxTrace(L"ERROR: Failed on wait for objects\n");

	return waitFailed;
	}

UINT CWaitableList::WaitForAnyObject(DWORD Timeout)
{
	MakeHandleList();

	DWORD r = WaitForMultipleObjects( m_uCount,
					  m_pHandles,
					  FALSE,
					  Timeout
					  );
	
	if( r >= WAIT_OBJECT_0 && r < WAIT_OBJECT_0 + m_uCount ) {

		m_uIndex = UINT(r - WAIT_OBJECT_0);

		return waitSignal;
		}

	if( r >= WAIT_ABANDONED && r < WAIT_ABANDONED + m_uCount ) {

		m_uIndex = UINT(r - WAIT_ABANDONED);

		return waitSignal;
		}

	if( r == WAIT_TIMEOUT ) {

		return waitTimeout;
		}

	AfxTrace(L"ERROR: Failed on wait for objects\n");

	return waitFailed;
	}

UINT CWaitableList::WaitForAllObjectsAndMsg(DWORD Timeout)
{
	MakeHandleList();

	DWORD r = MsgWaitForMultipleObjects( m_uCount,  
					     m_pHandles,
					     TRUE,
					     Timeout,
					     QS_ALLINPUT
					     );

	if( r >= WAIT_OBJECT_0 && r < (WAIT_OBJECT_0 + m_uCount) ) {

		m_uIndex = NOTHING;

		return waitSignal;
		}

	if( r >= WAIT_ABANDONED && r < (WAIT_ABANDONED + m_uCount) ) {

		m_uIndex = NOTHING;

		return waitSignal;
		}

	if( r == WAIT_OBJECT_0 + m_uCount ) {

		m_uIndex = NOTHING;

		return waitMessage;
		}

	if( r == WAIT_ABANDONED + m_uCount ) {

		m_uIndex = NOTHING;

		return waitMessage;
		}

	if( r == WAIT_TIMEOUT ) {

		return waitTimeout;
		}

	AfxTrace(L"ERROR: Failed on wait for objects\n");

	return waitFailed;
	}

UINT CWaitableList::WaitForAnyObjectOrMsg(DWORD Timeout)
{
	MakeHandleList();

	DWORD r = MsgWaitForMultipleObjects( m_uCount,  
					     m_pHandles,
					     FALSE,
					     Timeout,
					     QS_ALLINPUT
					     );

	if( r >= WAIT_OBJECT_0 && r < WAIT_OBJECT_0 + m_uCount ) {

		m_uIndex = UINT(r - WAIT_OBJECT_0);

		return waitSignal;
		}

	if( r >= WAIT_ABANDONED && r < WAIT_ABANDONED + m_uCount ) {

		m_uIndex = UINT(r - WAIT_ABANDONED);

		return waitSignal;
		}

	if( r == WAIT_OBJECT_0 + m_uCount ) {

		m_uIndex = NOTHING;

		return waitMessage;
		}

	if( r == WAIT_ABANDONED + m_uCount ) {

		m_uIndex = NOTHING;

		return waitMessage;
		}

	if( r == WAIT_TIMEOUT ) {

		return waitTimeout;
		}

	AfxTrace(L"ERROR: Failed on wait for objects\n");

	return waitFailed;
	}

// Wait Attributes

UINT CWaitableList::GetObjectIndex(void) const
{
	return m_uIndex;
	}

// Implementation

void CWaitableList::MakeHandleList(void)
{
	if( !m_pHandles ) {

		m_uCount   = m_List.GetCount();

		m_pHandles = New HANDLE [ m_uCount ];

		for( UINT n = 0; n < m_uCount; n++ ) {

			HANDLE h = m_List[n]->GetHandle();

			m_pHandles[n] = h;
			}
		}
	}

void CWaitableList::FreeHandleList(void)
{
	if( m_pHandles ) {

		delete m_pHandles;

		m_pHandles = NULL;
		}
	}

// End of File
