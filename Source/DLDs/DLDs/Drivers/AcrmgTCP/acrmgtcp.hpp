
//////////////////////////////////////////////////////////////////////////
//
// Acromag TCP Driver : ModbusTCP Driver
//

#include "mbtcpm.hpp"

#define ACROMAGTCP_ID 0x352A

class CAcromagTCPDriver : public CModbusTCPMaster
{
	public:
		// Constructor
		CAcromagTCPDriver(void);

		// Destructor
		~CAcromagTCPDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(UINT)  DevCtrl(void * pContext, UINT uFunc, PCTXT Value);

	protected:
		BOOL RemapAcromagSpaces(CAddress &Addr);
	};

enum {
	CIC3 = 11,
	CCS3 = 12,
	CCT3 = 13,
	CCP4 = 14,
	CDI4 = 15,
	CDO4 = 16,
	CCS4 = 17,
	CCH4 = 18,
	CCR4 = 19,
	CIR3 = 21,
	COR4 = 20
	};

enum {
	USEASIS	= 0,
	LONGOK	= 1,
	NOWRITE	= 2
	};

//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Spaces
//

#define	SPACE_HOLDING	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06

// End of File
