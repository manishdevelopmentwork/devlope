
#include "Intern.hpp"

#include "LangItem.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Language Item
//

// Dynamic Class

AfxImplementDynamicClass(CLangItem, CUIItem);

// Constructor

CLangItem::CLangItem(void)
{
	m_Lang  = 1;

	m_Keyb  = 0;

	m_Num   = 0;

	m_Upper = 0;
	}

// Attributes

BOOL CLangItem::HasLowerDiacriticals(void) const
{
	switch( m_Lang ) {

		case C3L_SYSTEM:
		case C3L_GENERIC:
		case C3L_ENGLISH_US:
		case C3L_ENGLISH_UK:
		case C3L_ENGLISH_IN:
		case C3L_RUSSIAN:
		case C3L_SIMP_CHINESE:
		case C3L_TRAD_CHINESE:

			return FALSE;
		}

	return TRUE;
	}

BOOL CLangItem::HasUpperDiacriticals(void) const
{
	if( HasLowerDiacriticals() ) {

		switch( m_Lang ) {

			case C3L_FRENCH_FR:
			case C3L_FRENCH_BE:
			case C3L_FRENCH_CH:

				return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// UI Update

void CLangItem::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == L"Lang" ) {

			if( pHost ) {

				pHost->EnableUI(this, L"Keyb",  m_Lang >= 0x0100);

				pHost->EnableUI(this, L"Upper", HasUpperDiacriticals());

				pHost->EnableUI(this, L"Code",  m_Lang == C3L_GENERIC);
				}
			}

		if( Tag == L"Lang" ) {

			if( !HasUpperDiacriticals() ) {

				m_Upper = 0;

				if( pHost ) {

					pHost->UpdateUI(this, L"Upper");
					}
				}

			if( m_Lang < 0x0100 ) {

				m_Keyb = 0;

				if( pHost ) {

					pHost->UpdateUI(this, L"Keyb");
					}
				}

			if( m_Lang & 0x8000 ) {

				m_Keyb = 1;

				if( pHost ) {

					pHost->UpdateUI(this, L"Keyb");
					}
				}

			if( m_Lang == C3L_GENERIC ) {

				m_Code.Empty();
				}
			else {
				CLangManager *pLang = (CLangManager *) GetParent(2);

				m_Code = pLang->GetAbbrFromCode(m_Lang);

				m_Code.MakeLower();
				}

			if( pHost ) {

				pHost->UpdateUI(this, L"Code");
				}
			}
		}
	}

// Persistance

void CLangItem::PostLoad(void)
{
	if( m_Code.IsEmpty() ) {

		if( m_Lang == C3L_GENERIC ) {

			m_Code.Empty();
			}
		else {
			CLangManager *pLang = (CLangManager *) GetParent(2);

			m_Code = pLang->GetAbbrFromCode(m_Lang);

			m_Code.MakeLower();
			}
		}

	CUIItem::PostLoad();
	}

// Meta Data Creation

void CLangItem::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Lang);
	Meta_AddString (Code);
	Meta_AddInteger(Keyb);
	Meta_AddInteger(Num);
	Meta_AddInteger(Upper);

	Meta_SetName((IDS_LANGUAGE_2));
	}

// Implementation

void CLangItem::DoEnables(IUIHost *pHost)
{
	}

// End of File
