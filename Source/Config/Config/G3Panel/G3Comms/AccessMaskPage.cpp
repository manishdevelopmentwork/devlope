
#include "Intern.hpp"

#include "AccessMaskPage.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "AccessMask.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Access Mask Page
//

// Runtime Class

AfxImplementRuntimeClass(CAccessMaskPage, CUIStdPage);

// Constructor

CAccessMaskPage::CAccessMaskPage(CAccessMask *pMask)
{
	m_pMask = pMask;

	m_Class = AfxRuntimeClass(CAccessMask);
	}

// Operations

BOOL CAccessMaskPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( TRUE ) {

		pView->StartGroup(CString(IDS_ACCESS_MODE), 2);

		CString Form, Help;

		Form += CString(IDS_NO);

		Form += m_pMask->IsDefault() ? CString(IDS_DEFAULT_ACCESS) : L"";

		Form += CString(IDS_USERS_WITH);

		Help += CString(IDS_DEFINES_ACCESS);

		CUIData Data;

		Data.m_Tag       = "Mode";

		Data.m_Label     = CString(IDS_ALLOW);

		Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

		Data.m_ClassUI   = AfxNamedClass(L"CUIDropDown");

		Data.m_Format    = Form;

		Data.m_Tip       = Help;

		pView->AddUI(pItem, L"root", &Data);

		pView->EndGroup(TRUE);
		}

	if( TRUE ) {

		pView->StartGroup(CString(IDS_SYSTEM_RIGHTS), 1);

		CUIData Data;

		Data.m_Tag       = CPrintf(L"Right%2.2u", 15);

		Data.m_Label     = CString(IDS_MAINTENANCE);

		Data.m_ClassText = AfxNamedClass(L"CUITextOnOff");

		Data.m_ClassUI   = AfxNamedClass(L"CUICheck");

		Data.m_Format    = L"";

		Data.m_Tip       = CString(IDS_IF_CHECKED_USER_1);

		pView->AddUI(pItem, L"root", &Data);

		pView->EndGroup(TRUE);
		}

	if( TRUE ) {

		pView->StartGroup(CString(IDS_CUSTOM_RIGHTS), 2);

		for( UINT n = 0; n < 8; n ++ ) {

			CUIData Data;

			Data.m_Tag       = CPrintf(L"Right%2.2u", 16+n);

			Data.m_Label     = CPrintf(L"User Right %u", 1+n);

			Data.m_ClassText = AfxNamedClass(L"CUITextOnOff");

			Data.m_ClassUI   = AfxNamedClass(L"CUICheck");

			Data.m_Format    = L"";

			Data.m_Tip       = CString(IDS_IF_CHECKED_USER_2);

			pView->AddUI(pItem, L"root", &Data);
			}

		pView->EndGroup(TRUE);
		}

	if( TRUE ) {

		pView->StartGroup(CString(IDS_PROGRAM_ACCESS), 1);

		CUIData Data;

		Data.m_Tag       = L"Right29";

		Data.m_Label     = CString(IDS_ALLOW_ACCESS_FROM);

		Data.m_ClassText = AfxNamedClass(L"CUITextOnOff");

		Data.m_ClassUI   = AfxNamedClass(L"CUICheck");

		Data.m_Format    = L"";

		Data.m_Tip       = CString(IDS_IF_CHECKED);

		pView->AddUI(pItem, L"root", &Data);

		pView->EndGroup(TRUE);
		}

	if( TRUE ) {

		pView->StartGroup(CString(IDS_CHECK_BEFORE), 1);

		CUIData Data;

		Data.m_Tag       = L"Right28";

		Data.m_Label     = CString(IDS_ALWAYS_GET);

		Data.m_ClassText = AfxNamedClass(L"CUITextOnOff");

		Data.m_ClassUI   = AfxNamedClass(L"CUICheck");

		Data.m_Format    = L"";

		Data.m_Tip       = CString(IDS_CHECK_TO_FORCE);

		pView->AddUI(pItem, L"root", &Data);

		pView->EndGroup(TRUE);
		}

	return FALSE;
	}

// End of File
