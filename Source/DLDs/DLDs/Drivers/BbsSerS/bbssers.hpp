//////////////////////////////////////////////////////////////////////////
//
// Bristol Babcock Serial Slave Driver
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

class CBBBSAPSerialSDriver : public CSlaveDriver
{
	public:
		// Constructor
		CBBBSAPSerialSDriver(void);

		// Destructor
		~CBBBSAPSerialSDriver(void);

		// Configuration
		DEFMETH(void)	Load(LPCBYTE pData);
		DEFMETH(void)	CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void)	Attach(IPortObject *pPort);
		DEFMETH(void)	Detach(void);

		// Device
		DEFMETH(CCODE)	DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE)	DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(void)	Service (void);

	protected:
		struct POLLRCVD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			BYTE	DestFC;
			BYTE	PollPri;
			};

		struct	POLLRESP
		{
			BYTE	Addr;	// always 0;
			BYTE	MsgSer;
			BYTE	DestFC;
			BYTE	SlvAdd;
			BYTE	NodeSB;
			BYTE	NumBuff;
			};

		struct LOCMSGHEAD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			};

		struct GLBMSGHEAD
		{
			BYTE	Addr;
			BYTE	MsgSer;
			BYTE	DAddrL;
			BYTE	DAddrH;
			BYTE	SAddrL;
			BYTE	SAddrH;
			BYTE	Control;
			};

		struct	ENDHEADLIST
		{
			BYTE	DestFC;
			BYTE	AppSeqL;
			BYTE	AppSeqH;
			BYTE	SrceFC;
			BYTE	NodeST;
			BYTE	DataSize;
			};

		struct	RBYADDRRX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	RBYNAMERX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	RSIGNALTX
		{
			BYTE	NodeStat;
			BYTE	RER;
			BYTE	NumElem;
			PBYTE	Elements;
			};

		struct	RBYLISTRX
		{
			BYTE	FuncCode;
			BYTE	FSS[5];
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	ListNum;
			BYTE	ListNum2L;
			BYTE	ListNum2H;
			};

		struct	RBYLISTTX
		{
			BYTE	NodeStat;
			BYTE	RER;
			BYTE	NumElem;
			BYTE	ListNum;
			};

		struct	WBYADDRRX
		{
			BYTE	FuncCode;
			BYTE	VersL;
			BYTE	VersH;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct	WBYNAMERX
		{
			BYTE	FuncCode;
			BYTE	SecLevel;
			BYTE	NumElem;
			};

		struct VARINFO
		{
			DWORD	Addr;
			DWORD	Data;
			BOOL	Logical;
			UINT	NameInx;
			WORD	Offset;
			WORD	MSDAddr;
			WORD	Length;
			WORD	Index;
			BYTE	EER;
			BYTE	FSS[5];
			};

		typedef VARINFO * PVARINFO;

		// Data Members
		PBYTE		m_pbDevName;
		PDWORD		m_pdAddrList;
		PBYTE		m_pbNameList;
		PWORD		m_pwNameIndex;
		PWORD		m_pwMSDList;

		BYTE		m_bIsC3;

		UINT		m_uTagCount;	// Tag information
		BOOL		m_fHasTags;	// Tag information

		// Master Controls
		BOOL		m_fGlobal;
		BYTE		m_bRER;		// Master Error Flag
		WORD		m_wVersion;	// Tag Version

		// Buffer Pointers
		PBYTE		m_pSave;
		PBYTE		m_pHeadList;
		PBYTE		m_pFuncList;
		PBYTE		m_pReqList;
		PBYTE		m_pDataList;

		// Buffer Control
		DWORD		m_dLBMagic;
		DWORD		m_dRCMagic;

		// Variable info

		VARINFO	SVarInfo;
		VARINFO	*m_pVarInfo;
		WORD	m_wVarSel;

		UINT	m_bDrop;
		UINT	m_uTPtr;
		UINT	m_uRPtr;
		BYTE	m_bTx [300];
		BYTE	m_bRx [300];
		UINT	m_uState;
		UINT	m_uETXPos;

		UINT	m_uNameListItem;
		UINT	m_uListCount;
		BYTE	m_bList;

		// Allocated pointers
		PBYTE	m_pSend;
		PDWORD	m_pListAddr;
		PDWORD	m_pListData;
		PWORD	m_pListPosn;

		// Field Selects
		BYTE	m_bFSS[5];

		// Receive Processing
		void	ReadData(void);
		void	ProcReceive();
		void	DoReceive(void);
		PBYTE	MakeReqBuffer(UINT uSize, UINT *pPos);

		// Read Routines
		void	ReadByList(BOOL fFirst);
		void	ReadSignal(BYTE bType);

		// Write routines
		void	WriteByName(void);
		void	WriteByAddress(void);
		BOOL	SetWriteData(UINT * pPos);
		BOOL	CheckWriteDesc(BYTE bDesc, UINT uItem);
		void	ExecuteWrite(PVARINFO *pVI, UINT uQty);
		
		// Item info
		BOOL	FindName(UINT uPos);
		BOOL	FindAddr(UINT uPos);
		BYTE	LoadInfoAboutItem(void);
		BOOL	GetReadInfo(UINT uIndex);
		void	FillInfo(UINT uIndex, DWORD dData);

		// Read Helpers
		void	ConstructSignalSend(PVARINFO *pVI, UINT uQty);
		
		// Read By List Helpers
		void	InitRBL(void);
		void	CountListElements(PDWORD pA, PDWORD pD, PWORD pP);
		void	AddListStart(UINT uCount, UINT uNext);
		void	GetVarInfo(UINT uIndex);

		// Write Helpers
		void	ConstructWriteReply(PVARINFO *pVI, UINT uQty);
				
		// Construct Frame
		UINT	AddSerialHeader(void);
		void	AddSerialFunc(void);
		void	FinishTxBuffer(UINT uCount);
		BOOL	PutItemEnd(BYTE bBad);

		// Field Select Helpers
		BOOL	ParseVarName(PCTXT Name, UINT uSelect, PBYTE pItem);
		void	SetFieldSelects(void);
		BOOL	AddFieldSels(void);
		void	AddField1(void);
		void	AddField2(void);
		void	AddField3(void);

		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddCRC(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	AddReal(DWORD dwData);
		void	AddText(PCTXT pText);
		void	AddTextPlusNull(PCTXT pText);

		// Transport Layer
		void	Send(void);
		void	SendFrame(void);

		// Ack
		void	SendAckNak(BYTE bType);
		void	SendPollAck(void);
		void	SendDownAck(void);
		void	SendAlarmAck(void);
		void	SendNak(void);

		// RER
		void	SetRER(BYTE bRER);
		void	AddEER(BYTE bError);

		// Helpers
		BOOL	HasTags(PDWORD pData, UINT uCount);
		void	InitGlobal(void);
		BOOL	CheckDrop(BYTE bData);
		BOOL	IsPollOrAck(BYTE bData);
		
		// Tag Data Loading
		void	LoadTags(LPCBYTE &pData);
		BOOL	StoreDevName(LPCBYTE &pData);
		void	StoreName(LPCBYTE &pData, UINT uItem, UINT *pPosition);
		BYTE	MakeUpper(BYTE b);

		// delete created buffers
		void	CloseListBuffers(void);
		void	CloseDown(void);
		void	ListDeAlloc(void);

		// Conversions
		DWORD LongToReal(UINT uType, DWORD Data);
		DWORD RealToLong(UINT uType, DWORD Data);
	};

enum { // Local Serial Rx Header Positions

	RXDADD	= 0,	// Destination Drop - same for global
	RXSERN	= 1,	// Serial Number - same for global
	RXDFC	= 2,	// Destination Function code
	RXSPRI	= 3,	// POLL Priority Code (=0)
	RXLSQL	= 3,	// App Sequence Number LO
	RXLSQH	= 4,	// App Sequence Number HI
	RXLSFC	= 5,	// Source Function Code
	RXLNSB	= 6,	// Node Status Byte
	RXLDAT0	= 7	// Packet Data Start
	};

enum { // Global Serial Rx Header Positions

//	RXDADD	= 0,	// Destination Drop - same for global as local
//	RXSERN	= 1,	// Serial Number - same for global as local
	RXGDAL	= 2,	// Destination Addr Low
	RXGDAH	= 3,	// Destination Addr High
	RXGSAL	= 4,	// Source Addr Low
	RXGSAH	= 5,	// Source Addr High
	RXGCTL	= 6,	// Control Byte
	RXGDFC	= 8,	// Destination Function Code
	RXGSQL	= 9,	// Application Sequence Number Low
	RXGSQH	= 10,	// Application Sequence Number High
	RXGSFC	= 11,	// Source Function Code
	RXGNSB	= 12,	// Node Status Byte
	RXGDAT0	= 13	// Packet Data Start
	};

enum { // List Serial Rx Data Positions

	RXSTRQ	= 0,	// List Command 0xC = Start, 0xD = Continue
	RXFUN	= 1,	// FSS define
	RXFSS1	= 2,	// FSS1
	RXFSS2	= 3,	// FSS2
	RXFSS3	= 4,	// FSS3
	RXFSS4	= 5,	// FSS4
	RXVERL	= 6,	// Version Low
	RXVERH	= 7,	// Version High
	RXSECC	= 8,	// Security Code
	RXLIST	= 9	// List Number
	};

#define	TXRERPOS	9

enum { // Message function codes
	MFCPEIPC  =  0x3, // for HMI
	MFCPEIRTU = 0x04, // to RTU
	MFCRDDB   = 0x0B, // Read Database
	MFCWRDB   = 0x0C, // Write Database
	MFCRDBYN  = 0x0D, // Read By Name in example file from BB
	MFCCOMREQ = 0x70, // Command Handler Requests
	MFCRDBACC = 0xA0, // remote database access
	};

enum { // Operational function codes
	OFCRDBYA  =  0x0, // Read Signal by Address
	OFCRDBYN  =  0x4, // Read Signal By Name
	OFCRDBYL  = 0x0C, // First call to read Signal(s) by list
	OFCRDBYLC = 0x0D, // Continue to read Signal(s) by list
	OFCWRBYA  = 0x80, // Write Signal by Address
	OFCWRBYN  = 0x84, // Write By Name
	};

enum { // Field Select 1 codes
	FS1TYPINH = 0x80,
	FS1VALUE  = 0x40,
	FS1UNITS  = 0x20,
	FS1MSDADD = 0x10,
	FS1NAME   = 0x08,
	FS1ALARM  = 0x04,
	FS1DESC   = 0x02,
	FS1ONOFF  = 0x01,
	};

enum { // Polls and Acks
	RTURESP  = 0x40, // Data Response
	POLLMSG  = 0x85, // Poll message
	ACKDOWN  = 0x86, // Down Transmit ACK, Slave ACKing message
	ACKPOLL  = 0x87, // ACK No Data, in response to a Poll message
	NRTSEND  = 0x88, // Send Node routing Table
	NRTREQ   = 0x89, // Request for NRT message
	POLLUPSL = 0x8A, // UP-ACK with poll (recognized by VSAT Slave)
	UPACK    = 0x8B, // UP-ACK, Master ACKing message
	NAKDATA  = 0x95, // NAK, No buffer available for received data
	ALARMACK = 0xA8, // Alarm Acknowledge
	ALARMNOT = 0xAA, // Alarm notification
	};

enum { // Write Field Descriptors
	WFDALDIS = 0x1,
	WFDALEN  = 0x2,
	WFDCNDIS = 0x3,
	WFDCNEN  = 0x4,
	WFDMNDIS = 0x5,
	WFDMNEN  = 0x6,
	WFDQUSET = 0x7,
	WFDQUCLR = 0x8,
	WFDLVON  = 0x9,
	WFDLVOFF = 0xA,
	WFDANALG = 0xB,
	WFDSTRNG = 0xD,
	WFDRDSEC = 0xE,
	WFDWRSEC = 0xF,
	};

enum { // RER Codes for standard requests
	RERMORE  = 0x81,	// Multiple errors in response
	RERMATCH = 0x84,	// no match on name, or too big
	RERAVPKD = 0x88,	// Analog value in packed logical
	RERNOMAT = 0x90,	// no match found
	RERVERNM = 0xA0,	// Version number mismatch
	RERINVDB = 0xC0,	// invalid data base function requested
	RERSET   = 0x80		// Error exists
	};

enum { // RER Codes for array request
	RERBOUND = 0x82,	// Array bounds exceeded
	RERARRCF = 0x84,	// Invalid array reference
	RERROARR = 0x88,	// write to a read-only array
//	RERINVDB = 0xC0		// invalid data base function request
	};

enum { // EER Codes
	EERRONLY = 0x02,	// attempt to write to read only
	EERIDENT = 0x04,	// Invalid name, list, MSDAddr
	EERSECUR = 0x05,	// Security violation
	EERINHIB = 0x06,	// Write a manual inhibited signal
	EERINVWR = 0x07,	// Invalid write attempt (eg analog=ON)
	EEREND_D = 0x08,	// Premature End_of_Data encountered
	EERINCOM = 0x09,	// Incomplete Remote DB request
	EERMATCH = 0x10		// No Match for Signal Name
	};

//////////////////////////////////////////////////////////////////////////
//
// Useful Macros
//

#define	I2R(x)	(*((float *) &(x)))

#define	R2I(x)	(*((long  *) &(x)))

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

// Slave Address Fields
#define	AMASK	0x3FF	// MSD Address = 0 - 1023
#define	LSHIFT	10	// List number is bit 10+

#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

#define	MAXSEND	240

/*
FUN Function code
bit 7 function bit
0 read function
1 write function
bits 6-4 = data type code
000 signal data
001 data array data
010 direct I/O data
100 memory data
111 Extended RDB function
bits 3-2 = select code
00 select via address or data array number, control
01 select via name or data array number, short form
10 select via match or data array number, general form
11 select via list
bits 1-0 = signal READ return code, WRITE Memory code
For READ:
00 return first match
01 return next match/continue list
10 return packed logical data (logical value data only)
For WRITE Memory,
00 Write Data to memory
01 OR Data with memory
10 AND Data with memory

NOTE:FIELD SELECTOR BYTES ARE ONLY USED IN ACCESSING SIGNAL
DATA WHEN PACKED LOGICAL DATA IS NOT USED.
FSS Field select selector defines which field select bytes are included in the request:
BITS 7-4 not used at this time (reserved for priority)
BIT 3 Field select byte 4 is used
BIT 2 Field select byte 3 is used
BIT 1 Field select byte 2 is used
BIT 0 Field select byte 1 is used

FS1 Field selector byte one (most common options):
BIT 7 Type and Inhibits byte
BIT 6 Value
BIT 5 Units/Logical text
BIT 4 Msd address
BIT 3 Name Text (includes Base.Extension.Attribute)
BIT 2 Alarm status
BIT 1 Descriptor text
BIT 0 Logical ON/OFF text

FS2 Field selector byte two:
BIT 7 Protection byte
BIT 6 Alarm priority
BIT 5 Base Name
BIT 4 Extension
BIT 3 Attribute
BIT 2 Low alarm deadband MSD address
BIT 1 High alarm deadband MSD address
BIT 0 MSD Version number

FS3 Field selector byte three:
BIT 7 Low alarm limit MSD address
BIT 6 High alarm limit MSD address
BIT 5 Extreme low alarm limit MSD address
BIT 4 Extreme high alarm limit MSD address
BIT 3 Reserved for Report by exception
BIT 2 Reserved (old Lock bit)
BIT 1 Signal value; Analogs return no Q-bit in FP mantissa.
BIT 0 When set, allows long signal names to be returned, instead of standard
length names. (Requires ControlWave firmware version 2.2 or newer)

FS4 Field selector byte four:
This byte is reserved for future use.
*/

// End of File
