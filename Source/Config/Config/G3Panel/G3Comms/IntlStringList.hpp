
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_IntlStringList_HPP

#define INCLUDE_IntlStringList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CIntlString;

//////////////////////////////////////////////////////////////////////////
//
// International String List
//

class CIntlStringList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CIntlStringList(void);

		// Item Access
		CIntlString * GetItem(UINT uPos);
		CIntlString * GetItem(INDEX Index);
	};

// End of File

#endif
