
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 I/O Module Support
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NewCommsDeviceRackWnd_HPP

#define INCLUDE_NewCommsDeviceRackWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device View
//

class CNewCommsDeviceRackWnd : public CProxyViewWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CNewCommsDeviceRackWnd(void);

protected:
	// Overridables
	void OnAttach(void);
};

// End of File

#endif
