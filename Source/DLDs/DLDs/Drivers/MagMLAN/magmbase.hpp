
//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Driver Base
//

class CMagMLANBase : public CMasterDriver
{
	public:
		// Constructor
		CMagMLANBase(void);

		// Destructor
		~CMagMLANBase(void);
		
		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

		// Device Data
		struct CBaseCtx
		{
			// Device storage
			WORD	 m_wBWW;
			BYTE	 m_bBWF;
			BYTE	 m_bSS0;
			DWORD	 m_dRC;
			DWORD	 m_dWO;
			DWORD	 m_dOP;
			WORD	 m_wVolt;
			DWORD	 m_dSet[28];
			DWORD	 m_dTOTR[18];
			DWORD	 m_dNAK;
			};

	protected:
		// Data Members
		CBaseCtx * m_pBase;
		UINT       RspFrame[3];
		BYTE	   m_bTx[256];
		BYTE	   m_bRx[256];
		BYTE	   m_bCheck;
		UINT	   m_uPtr;
		UINT	   m_uWriteErr;

		// Frame Building
		void	StartFrame(BYTE bOpcode);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dwData);
		void	EndFrame(void);

		// Read Handlers
		CCODE	DoRead(AREF Addr, PDWORD pData, UINT uCount);
		BYTE	GetReadOpcode(AREF Addr);
		void	SetReadResponseData(AREF Addr);
		void	GetTableRead(	AREF Addr,  PDWORD pData, UINT uCount);
		void	GetCycle(	AREF Addr,  PDWORD pData, UINT uCount);
		void	GetBatch(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetLineS(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetRate(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetStar(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetTARYield(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetType(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetReadSettings(UINT uItem, PDWORD pData, UINT uCount);
		UINT	GetSettingPos(	UINT uItem);
		UINT	GetSettingLen(	UINT uItem);
		void	GetReadStatus(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetReadTotals(	UINT uItem, PDWORD pData, UINT uCount);
		void	GetReadVersion(	UINT uItem, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE	DoWrite(AREF Addr, PDWORD pData, UINT uCount);
		BYTE	GetWriteOpcode(AREF Addr);
		BYTE	GetNamedWOpcode(UINT uOffset);
		void	AddNamedData( UINT uOffset, DWORD dData);
		void	AddTableData( AREF Addr, PDWORD pData);
		CCODE	HandleNAK(void);

		// Helpers
		BOOL	ReadNoTransmit(AREF Addr, PDWORD pData, UINT uCount);
		BOOL	WriteNoTransmit(AREF Addr, DWORD dData);
		void	ExpandPar(UINT uOffset, BOOL fIsPar0);
		DWORD	GetItemData(UINT uPos, UINT uLen);

		// Drop Number
		virtual BYTE GetDevDrop(void);

		// Transport Layer
		virtual BOOL Transact(BOOL fIsWrite);
	};

//////////////////////////////////////////////////////////////////////////
//
// Maguire MLAN Driver Data Spaces
//

#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong

// Space Definitions
#define	SPGBI	1
#define	SPCWW	2
#define	SPCWL	3
#define	SPLST	4
#define	SPPAR0	5
#define	SPPAR1	6
#define	SPRATE	7
#define	SPSET	8
#define	SPSKEY	9
#define	SPSTAR	10
#define	SPSTAT	11
#define	SPTAG	12
#define	SPTAR	13
#define	SPTOTR	14
#define	SPTOTS	15
#define	SPTYP	16
#define	SPVER	17
#define	SPYST	18

// Parameter Command Macro
#define	LOW5(x)	(x & 0x1F)

// Display 4 Hex / 5 Decimal 0's
#define	RTNZERO	409600000 // 0x186A 0000

// Response Frame Positions
#define	RSPSIZE	0 // Response Size
#define	ITEMPOS	1 // Item Position
#define	ITEMLEN	2 // Item Size

// Settings Control
#define	READSET 0 // Allow Read of Settings
#define	FILLSET 1 // Stop Read to allow fill
#define	SENDSET 2 // Send the data, resume Read

// Nak Received
#define	OPNAK	255 // display command code of recent NAK

// Opcodes
#define	WACR	27
#define	RADD	54
#define	WALM	82
#define	WCLT	24
#define	WCTI	28
#define	WBW	83
#define	WBWW	183
#define	WBWF	283
#define	RECM	60
#define	WMMS	66
#define	WPCC	25
#define	WSRK	88
#define	RSSR	64
#define	WSSR	65
#define	WSSS	55
#define	WSS0	155
#define	RWTU	85
#define	WWTU	86
#define	WVOLT	59
#define	WSSY	33

// Table Read Opcodes
#define	RGBI	84
#define	RCWW	50
#define	RCWL	79
#define	RLST	61
#define	RPAR0	69
#define	RRATE	57
#define	RSET	20
#define	RSTAR	71
#define	RSTAT	53
#define	RTAR	29
#define	RTOTR	17
#define	RTOTS	16
#define	RTYP	49
#define	RVER	80
#define	RYST	31

// Table Write Opcodes	
#define	WPAR0	68
#define	WRATE	58
#define	WSET	19
#define	WSKEY	87
#define	WSTAR	70
#define	WTAG	90
#define	WTAR	30

// End of File
