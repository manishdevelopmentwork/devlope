
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Control Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();	

//////////////////////////////////////////////////////////////////////////
//
// Scroll Bar Control
//

// Dynamic Class

AfxImplementDynamicClass(CScrollBar, CCtrlWnd);

// Constructor

CScrollBar::CScrollBar(void)
{
	}

// Handle Lookup

CScrollBar & CScrollBar::FromHandle(HANDLE hWnd)
{
	if( hWnd == NULL ) {

		static CScrollBar NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CScrollBar &) CWnd::FromHandle(hWnd, Class);
	}

// Attributes

int CScrollBar::GetScrollPos(void) const
{
	return ::GetScrollPos(m_hWnd, SB_CTL);
	}

int CScrollBar::GetRangeMin(void) const
{
	int nMin, nMax;
	
	::GetScrollRange(m_hWnd, SB_CTL, &nMin, &nMax);
	
	return nMin;
	}

int CScrollBar::GetRangeMax(void) const
{
	int nMin, nMax;
	
	::GetScrollRange(m_hWnd, SB_CTL, &nMin, &nMax);
	
	return nMax;
	}

UINT CScrollBar::GetPageSize(void) const
{
	SCROLLINFO Info;

	Info.cbSize = sizeof(Info);

	Info.fMask = SIF_PAGE;

	::GetScrollInfo(m_hWnd, SB_CTL, &Info);

	return Info.nPage;
	}
		
// Operations

void CScrollBar::SetScrollPos(int nPos, BOOL fPaint)
{
	::SetScrollPos(m_hWnd, SB_CTL, nPos, fPaint);
	}

void CScrollBar::SetScrollRange(int nMin, int nMax, BOOL fPaint)
{
	::SetScrollRange(m_hWnd, SB_CTL, nMin, nMax, fPaint);
	}

void CScrollBar::SetPageSize(UINT uPage, BOOL fPaint)
{
	SCROLLINFO Info;

	Info.cbSize = sizeof(Info);
	
	Info.fMask  = SIF_PAGE;

	Info.nPage  = uPage;

	::SetScrollInfo(m_hWnd, SB_CTL, &Info, fPaint);
	}

void CScrollBar::EnableArrows(BOOL fMin, BOOL fMax)
{
	WPARAM Mask = 0;

	if( !fMin ) Mask |= ESB_DISABLE_LTUP;
		
	if( !fMax ) Mask |= ESB_DISABLE_RTDN;

	SendMessage(SBM_ENABLE_ARROWS, Mask, 0);
	}
		
// Default Class Name

PCTXT CScrollBar::GetDefaultClassName(void) const
{
	return L"SCROLLBAR";
	}

// End of File
