
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "melservo.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "melservo.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "melservo.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "melservo.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CMELServoDeviceOptions
//

CMELServoDeviceOptionsUIList RCDATA
BEGIN
	"Addr,Address,,CUIEditInteger,|0||0|31,"
	"Station\t= 0 - 31\r\n"
	"Group\t= 10(a) - 15(f)\r\n"
	"All Units\t= 0 - 31"
	"\0"

	"Group,Address Type,,CUIDropDown,Station|Group|All Units,"
	"Select Address Type."
	"\0"

	"ServoType,Servo Type,,CUIDropDown,MR-J2S-CL|MR-J2S-A|MR-J2S-CP|MR-J3-A,"
	"Select Servo Type."
	"\0"

	"\0"
END

CMELServoDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Addr,Group,ServoType\0"
	"\0"
END

MELServoAddressDlg DIALOG 0, 0, 0, 0
CAPTION "Item Selection"
BEGIN
	GROUPBOX	"&Data Item",	1000,		  4,   4, 202, 290
	LISTBOX				1001,		 10,  18, 190, 210, XS_LISTBOX | LBS_USETABSTOPS
	GROUPBOX	"&Parameters",	2000,		210,   4, 138,  56
	LTEXT		"<Prefix>",	2001,		218,  17,  20,  10
	EDITTEXT			2002,		225,  32,  20,	12
	LTEXT		"<Decimal>",	2005		247,  33,  36,	10
	LTEXT		"Data Type:",	2003,		218,  48,  32,  10
	LTEXT		"<Type>",	2004,		252,  48,  80,  10
	EDITTEXT			2007,		218,  32,   6,  12
	GROUPBOX	"Information",	3000,		210,  62, 138, 214
	LTEXT		"<Info>",	3001,		214,  76, 128,  10
	LTEXT		"<Info>",	3002,		214,  87, 128,  10
	LTEXT		"<Info>",	3003,		214,  98, 128,  10
	LTEXT		"<Info>",	3004,		214, 109, 128,  10
	LTEXT		"<Info>",	3005,		214, 120, 128,  10
	LTEXT		"<Info>",	3006,		214, 131, 128,  10
	LTEXT		"<Info>",	3007,		214, 142, 128,  10
	LTEXT		"<Info>",	3008,		214, 153, 128,  10
	LTEXT		"<Info>",	3009,		214, 164, 128,  10
	LTEXT		"<Info>",	3010,		214, 175, 128,  10
	LTEXT		"<Info>",	3011,		214, 186, 128,  10
	LTEXT		"<Info>",	3012,		214, 197, 128,  10
	LTEXT		"<Info>",	3013,		214, 208, 128,  10
	LTEXT		"<Info>",	3014,		214, 219, 128,  10
	LTEXT		"<Info>",	3015,		214, 230, 128,  10
	LTEXT		"<Info>",	3016,		214, 241, 128,  10
	LTEXT		"<Info>",	3017,		214, 252, 128,  10
	LTEXT		"<Info>",	3018,		214, 263, 128,  10
	LTEXT		"Command Codes:",3019,		242,  17,  48,  10
	LTEXT		"<Info>",	3020,		292,  17,  55,  10

	DEFPUSHBUTTON   "OK",		IDOK,		210, 280,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	254, 280,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",		IDHELP,		298, 280,  40,  14, XS_BUTTONREST

END

PartMELServoAddressDlg DIALOG 0, 0, 0, 0
CAPTION "Item Selection"
BEGIN
	GROUPBOX	"&Parameters",	2000,		 4,   4, 138,  56
	LTEXT		"<Prefix>",	2001,		12,  17,  20,  10
	EDITTEXT			2002,		19,  32,  20,  12
	LTEXT		"<Decimal>",	2005		41,  33,  36,  10
	LTEXT		"Data Type:",	2003,		12,  48,  32,  10
	LTEXT		"<Type>",	2004,		46,  48,  80,  10
	EDITTEXT			2007,		12,  32,   6,  12
	GROUPBOX	"Information",	3000,		 4,  62, 138, 214
	LTEXT		"<Info>",	3001,		 8,  76, 128,  10
	LTEXT		"<Info>",	3002,		 8,  87, 128,  10
	LTEXT		"<Info>",	3003,		 8,  98, 128,  10
	LTEXT		"<Info>",	3004,		 8, 109, 128,  10
	LTEXT		"<Info>",	3005,		 8, 120, 128,  10
	LTEXT		"<Info>",	3006,		 8, 131, 128,  10
	LTEXT		"<Info>",	3007,		 8, 142, 128,  10
	LTEXT		"<Info>",	3008,		 8, 153, 128,  10
	LTEXT		"<Info>",	3009,		 8, 164, 128,  10
	LTEXT		"<Info>",	3010,		 8, 175, 128,  10
	LTEXT		"<Info>",	3011,		 8, 186, 128,  10
	LTEXT		"<Info>",	3012,		 8, 197, 128,  10
	LTEXT		"<Info>",	3013,		 8, 208, 128,  10
	LTEXT		"<Info>",	3014,		 8, 219, 128,  10
	LTEXT		"<Info>",	3015,		 8, 230, 128,  10
	LTEXT		"<Info>",	3016,		 8, 241, 128,  10
	LTEXT		"<Info>",	3017,		 8, 252, 128,  10
	LTEXT		"<Info>",	3018,		 8, 263, 128,  10
	LTEXT		"Command Codes:",3019,		36,  17,  48,  10
	LTEXT		"<Info>",	3020,		86,  17,  55,  10

	DEFPUSHBUTTON   "OK",		IDOK,		 4, 280,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",	IDCANCEL,	48, 280,  40,  14, XS_BUTTONREST
//	PUSHBUTTON	"Help",		IDHELP,		92, 280,  40,  14, XS_BUTTONREST

END

// End of File
