
#include "Intern.hpp"

#include "UsbRackExpansion.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Expansion System
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Identifiers
//

#include "..\HxUsb\UsbIdentifiers.hpp"

#include "..\HxUsb\Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Rack Expansion Object
//

// Instantiator

IExpansionInterface * Create_UsbRackExpansion(IUsbHostFuncDriver *pDriver)
{
	IExpansionInterface *p = (IExpansionRack *) New CUsbRackExpansion(pDriver);

	return p;
	}

// Constructor

CUsbRackExpansion::CUsbRackExpansion(IUsbHostFuncDriver *pDriver) : CUsbExpansion(pDriver)
{
	m_pHub   = (IUsbHubDriver *) pDriver;

	m_uPower = 0;
	}

// IUnknown

HRESULT METHOD CUsbRackExpansion::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IExpansionRack);

	return CUsbExpansion::QueryInterface(riid, ppObject);
	}

ULONG METHOD CUsbRackExpansion::AddRef(void)
{
	return CUsbExpansion::AddRef();
	}

ULONG METHOD CUsbRackExpansion::Release(void)
{
	return CUsbExpansion::Release();
	}

// IExpansionInterface

PCTXT METHOD CUsbRackExpansion::GetName(void)
{
	return "RACK";
	}

UINT METHOD CUsbRackExpansion::GetClass(void)
{
	return rackClassRack;
	}

UINT METHOD CUsbRackExpansion::GetIndex(void)
{
	return CUsbExpansion::GetIndex();
	}

UINT METHOD CUsbRackExpansion::GetPower(void)
{
	return CUsbExpansion::GetPower();
	}

UINT METHOD CUsbRackExpansion::GetIdent(void)
{
	return CUsbExpansion::GetIdent();
	}

BOOL METHOD CUsbRackExpansion::HasBootLoader(void)
{
	return FALSE;
	}

IDevice * METHOD CUsbRackExpansion::MakeObject(IUsbHostFuncDriver *pDrv)
{
	return NULL;
	}

IExpansionPnp * METHOD CUsbRackExpansion::QueryPnpInterface(IDevice *pObj)
{
	return NULL;
	}

// ExpansionRack

void METHOD CUsbRackExpansion::RegisterPower(IExpansionInterface *pDriver)
{
	m_uPower += pDriver->GetPower();

	if( m_uPower > GetMaxPower() ) {

		KillRackPower();
		}
	}

void METHOD CUsbRackExpansion::UnregisterPower(IExpansionInterface *pDriver)
{
	m_uPower -= pDriver->GetPower();
	}

// Implementation

UINT CUsbRackExpansion::GetMaxPower(void)
{
	switch( m_wProduct ) {

		case pidSmcHub2514: return 180;
		}

	return 0;
	}

void CUsbRackExpansion::KillRackPower(void)
{
	m_pHub->ClrPortFeature(4, selPortPower);
	}

// End of File
