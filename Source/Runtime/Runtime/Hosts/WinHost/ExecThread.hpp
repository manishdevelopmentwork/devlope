
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_ExecThread_HPP

#define INCLUDE_ExecThread_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../HSL/MosExecThread.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Imported Types
//

using win32::HANDLE;

using win32::LONG_PTR;

//////////////////////////////////////////////////////////////////////////
//
// Executive Thread Object
//

class CExecThread : public CMosExecThread
{
	public:
		// Constructor
		CExecThread(CMosExecutive *pExec);

		// Destructor
		~CExecThread(void);

		// IWaitable
		PVOID METHOD GetWaitable(void);
		BOOL  METHOD Wait(UINT uTime);
		BOOL  METHOD HasRequest(void);

		// IThread
		void  METHOD Guard(BOOL fGuard);
		void  METHOD Guard(PGUARD pProc, void *pData);
		void  METHOD Exit(int nCode);
		BOOL  METHOD Destroy(void);
		void  METHOD SetLibPointer(PVOID pLibData);
		PVOID METHOD GetLibPointer(void);

		// Current Thread
		static CExecThread _tls_decl * m_pThread;

	protected:
		// Library Pointer
		static PVOID _tls_decl m_pLibData;

		// Platform Data
		HANDLE m_hThread;

		// Overridables
		BOOL OnCreate(void);

		// Implementation
		int    ExecuteThread(void);
		HANDLE GetThreadHandle(void);

		// Callbacks
		static DWORD WINAPI ThreadProc(void *);
		static void  WINAPI KillerProc(DWORD dwParam);
	};

// End of File

#endif
