
#include "intern.hpp"

#include "iqan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CIqanDriverOptions, CUIItem);

// Constructor

CIqanDriverOptions::CIqanDriverOptions(void)
{
	m_Source    = 1;

	m_Op	    = 0;
}

// Download Support

BOOL CIqanDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Source));
	Init.AddByte(BYTE(m_Op));

	return TRUE;
}

// Meta Data Creation

void CIqanDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Source);
	Meta_AddInteger(Op);
}


//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CIqanDeviceOptions, CUIItem);

// Constructor

CIqanDeviceOptions::CIqanDeviceOptions(void)
{
	m_Send1 = 1;
	m_Rate1 = 50;
	m_Send2 = 1;
	m_Rate2 = 50;
	m_Send3 = 1;
	m_Rate3 = 50;
	m_Send4 = 1;
	m_Rate4 = 50;
}

// UI Managament

void CIqanDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		pWnd->EnableUI("Rate1", m_Send1 ? TRUE : FALSE);
		pWnd->EnableUI("Rate2", m_Send2 ? TRUE : FALSE);
		pWnd->EnableUI("Rate3", m_Send3 ? TRUE : FALSE);
		pWnd->EnableUI("Rate4", m_Send4 ? TRUE : FALSE);

		pWnd->UpdateUI();
	}
}

// Download Support

BOOL CIqanDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Send1));
	Init.AddLong(LONG(m_Rate1));
	Init.AddByte(BYTE(m_Send2));
	Init.AddLong(LONG(m_Rate2));
	Init.AddByte(BYTE(m_Send3));
	Init.AddLong(LONG(m_Rate3));
	Init.AddByte(BYTE(m_Send4));
	Init.AddLong(LONG(m_Rate4));

	return TRUE;
}

// Meta Data Creation

void CIqanDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Send1);
	Meta_AddInteger(Rate1);
	Meta_AddInteger(Send2);
	Meta_AddInteger(Rate2);
	Meta_AddInteger(Send3);
	Meta_AddInteger(Rate3);
	Meta_AddInteger(Send4);
	Meta_AddInteger(Rate4);
}

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN Driver
//

// Instantiator

ICommsDriver *	Create_IqanDriver(void)
{
	return New CIqanDriver;
}


// Constructor

CIqanDriver::CIqanDriver(void)
{
	m_wID		= 0x4096;

	m_uType		= driverMaster;

	m_Manufacturer	= "Parker";

	m_DriverName	= "IQAN";

	m_Version	= "1.00";

	m_ShortName	= "IQAN";

	m_DevRoot	= "DEV";

	m_fSingle       = TRUE;

	AddSpaces();
}

// Driver Data

UINT CIqanDriver::GetFlags(void)
{
	return CStdCommsDriver::GetFlags() | dflagRemotable;
}

// Binding Control

UINT CIqanDriver::GetBinding(void)
{
	return bindCAN;
}

void CIqanDriver::GetBindInfo(CBindInfo &Info)
{
	CBindCAN &CAN = (CBindCAN &) Info;

	CAN.m_BaudRate  = 250000;

	CAN.m_Terminate = FALSE;
}

// Configuration

CLASS CIqanDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CIqanDriverOptions);
}


CLASS CIqanDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CIqanDeviceOptions);
}

// Address Management

BOOL CIqanDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CIqanAddrDialog Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
}

// Address Helpers

BOOL CIqanDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	CString Type = StripType(pSpace, Text);

	UINT uFind = Text.Find(':');

	UINT uBitOffset = 0;

	if( uFind < NOTHING ) {

		uBitOffset = tatoi(Text.Mid(uFind + 1));
	}

	Text = Text.Mid(0, uFind) + "." + Type;

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {

		UINT uOffset = Addr.a.m_Offset - pSpace->m_uMinimum;

		Addr.a.m_Offset = (uOffset << 6) | uBitOffset;

		CIqanSpace * pIqan = (CIqanSpace *) pSpace;

		CAddress Address = Addr;

		pIqan->GetMaximum(Address);

		if( Address.a.m_Offset < Addr.a.m_Offset ||   Address.a.m_Extra < Addr.a.m_Extra ) {

			Error.Set(CString(IDS_DRIVER_ADDR_INVALID),
				  0
			);

			return FALSE;
		}

		return TRUE;
	}

	Error.Set(CString(IDS_DRIVER_ADDR_INVALID),
		  0
	);

	return FALSE;
}

BOOL CIqanDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace * pSpace = GetSpace(Addr);

	if( pSpace ) {

		UINT uID = (Addr.a.m_Offset >> 6) + pSpace->m_uMinimum;

		UINT uBit = Addr.a.m_Offset & 0x3F;

		if( CStdCommsDriver::DoExpandAddress(Text, pConfig, Addr) ) {

			UINT uType   = Addr.a.m_Type;

			if( uType == pSpace->m_uType ) {

				Text.Printf(L"%s%4.4X:%2.2u",
					    pSpace->m_Prefix,
					    uID,
					    uBit
				);
			}
			else {
				Text.Printf(L"%s%4.4X:%2.2u.%s",
					    pSpace->m_Prefix,
					    uID,
					    uBit,
					    pSpace->GetTypeModifier(uType)
				);
			}

			return TRUE;
		}
	}

	return FALSE;
}


// Implementation

void CIqanDriver::AddSpaces(void)
{
	AddSpace(New CIqanSpace(1, "DI", "IQAN Digital Input", 16, 0x181, 0x57F, addrBitAsBit, addrBitAsBit, 0, 63));
	AddSpace(New CIqanSpace(2, "DO", "IQAN Digital Output", 16, 0x181, 0x57F, addrBitAsBit, addrBitAsBit, 0, 63));
	AddSpace(New CIqanSpace(5, "BI", "IQAN Byte Input", 16, 0x181, 0x57F, addrByteAsByte, addrByteAsByte, 0, 63));
	AddSpace(New CIqanSpace(6, "BO", "IQAN Byte Output", 16, 0x181, 0x57F, addrByteAsByte, addrByteAsByte, 0, 63));
	AddSpace(New CIqanSpace(3, "AI", "IQAN Analog Input", 16, 0x181, 0x57F, addrWordAsWord, addrWordAsLong, 0, 63));
	AddSpace(New CIqanSpace(4, "AO", "IQAN Analog Output", 16, 0x181, 0x57F, addrWordAsWord, addrWordAsLong, 0, 63));
}

BOOL CIqanDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return Addr.a.m_Table % 2 == 0;
}

//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Driver Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CIqanAddrDialog, CStdAddrDialog);

// Constructor

CIqanAddrDialog::CIqanAddrDialog(CIqanDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) : CStdAddrDialog(Driver, Addr, pConfig, fPart)
{
	m_Element = L"ParkerIqanElementDlg";
}

// Overridables

void CIqanAddrDialog::SetAddressText(CString Text)
{
	BOOL fEnable = m_pSpace ? TRUE : FALSE;

	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		CString ID = Text.Mid(0, uFind);

		CString Bit = Text.Mid(uFind + 1);

		GetDlgItem(2002).SetWindowText(ID);

		GetDlgItem(2005).SetWindowText(Bit);
	}
	else {
		GetDlgItem(2002).SetWindowText("0");

		GetDlgItem(2005).SetWindowText("0");
	}

	GetDlgItem(2002).EnableWindow(fEnable);

	GetDlgItem(2005).EnableWindow(fEnable);
}

CString CIqanAddrDialog::GetAddressText(void)
{
	CString Text = GetDlgItem(2002).GetWindowText();

	Text += ":";

	Text += GetDlgItem(2005).GetWindowText();

	return Text;
}


//////////////////////////////////////////////////////////////////////////
//
// Parker IQAN CAN Bus Driver Space
//

// Constructor

CIqanSpace::CIqanSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s, UINT io, UINT ao) : CSpace(uTable, p, c, r, n, x, t, s)
{
	m_uMinBit = io;

	m_uMaxBit = ao;
}

// Matching

BOOL CIqanSpace::MatchSpace(CAddress const &Addr)
{
	if( m_uTable == Addr.a.m_Table ) {

		return TRUE;
	}

	return FALSE;
}

// Limits

void CIqanSpace::GetMinimum(CAddress &Addr)
{
	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	// cppcheck-suppress duplicateExpression

	Addr.a.m_Offset	= m_uMinBit | ((m_uMinimum - m_uMinimum) << 6); // Huh????
}

void CIqanSpace::GetMaximum(CAddress &Addr)
{
	UINT uMax = m_uMaxBit;

	switch( Addr.a.m_Type ) {

		case addrByteAsByte:
		case addrBitAsByte:	uMax -= 8;	break;

		case addrByteAsWord:
		case addrBitAsWord:
		case addrWordAsWord:	uMax -= 16;	break;

		case addrByteAsLong:
		case addrBitAsLong:
		case addrWordAsLong:	uMax -= 32;	break;
	}

	Addr.a.m_Table	= m_uTable;

	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uMax | ((m_uMaximum - m_uMinimum) << 6);
}

// End of File
