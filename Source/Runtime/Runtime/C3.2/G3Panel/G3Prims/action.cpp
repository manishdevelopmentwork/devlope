
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Action
//

// Constructor

CPrimAction::CPrimAction(void)
{
	m_Delay    = 0;

	m_Protect  = 0;

	m_Local    = FALSE;

	m_pEnable  = NULL;
	
	m_pPress   = NULL;
	
	m_pRepeat  = NULL;
	
	m_pRelease = NULL;
	}

// Destructor

CPrimAction::~CPrimAction(void)
{
	delete m_pEnable;
	
	delete m_pPress;
	
	delete m_pRepeat;
	
	delete m_pRelease;
	}

// Initialization

void CPrimAction::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimAction", pData);

	DoLoad(pData);
	}

// Attributes

BOOL CPrimAction::IsAvail(void) const
{
	if( !IsItemAvail(m_pEnable) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pPress) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pRepeat) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pRelease) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CPrimAction::IsEnabled(void) const
{
	if( m_pEnable ) {

		if( m_pEnable->ExecVal() ) {

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

// Operations

void CPrimAction::SetScan(UINT Code)
{
	SetItemScan(m_pEnable,  Code);
	
	SetItemScan(m_pPress,   Code);
	
	SetItemScan(m_pRepeat,  Code);
	
	SetItemScan(m_pRelease, Code);
	}

BOOL CPrimAction::RunEvent(UINT uTrans, BOOL fLocal, int px, int py)
{
	if( fLocal || !m_Local ) {

		if( IsAvail() ) {

			if( !CUISystem::m_pThis->m_pUI->Protect(this, m_Protect, uTrans) ) {

				return FALSE;
				}

			DWORD p[] = { DWORD(px), DWORD(py) };

			switch( uTrans ) {

				case stateDown:

					if( m_pPress ) {

						m_pPress->Execute(typeVoid, p);

						return TRUE;
						}
					break;

				case stateRepeat:

					if( m_pRepeat ) {

						m_pRepeat->Execute(typeVoid, p);

						return TRUE;
						}
					break;

				case stateUp:

					if( m_pRelease ) {

						m_pRelease->Execute(typeVoid, p);

						return TRUE;
						}
					break;
				}
			}

		return TRUE;
		}
	else {
		AfxTrace("action - remote blocked\n");
		
		return FALSE;
		}
	}

BOOL CPrimAction::RunEvent(UINT uTrans, BOOL fLocal)
{
	return RunEvent(uTrans, fLocal, 0, 0);
	}

// Implementation

void CPrimAction::DoLoad(PCBYTE &pData)
{
	m_Delay   = GetWord(pData);

	m_Protect = GetByte(pData);

	m_Local   = GetByte(pData);

	GetCoded(pData, m_pEnable);
	
	GetCoded(pData, m_pPress);
	
	GetCoded(pData, m_pRepeat);
	
	GetCoded(pData, m_pRelease);
	}

// End of File
