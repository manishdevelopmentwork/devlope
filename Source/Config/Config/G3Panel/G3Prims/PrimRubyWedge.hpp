
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyWedge_HPP
	
#define	INCLUDE_PrimRubyWedge_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGeom.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Wedge Primitive
//

class CPrimRubyWedge : public CPrimRubyGeom
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyWedge(void);

		// Data Members
		UINT m_Orient;

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);
	};

// End of File

#endif
