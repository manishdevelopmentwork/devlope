
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SmtpDefs_HPP

#define	INCLUDE_SmtpDefs_HPP

//////////////////////////////////////////////////////////////////////////
//
// SMTP Configuration
//

struct CSmtpConfig
{
	UINT		m_uMode;
	CString		m_Server;
	UINT		m_uPort;
	UINT		m_uTls;
	UINT		m_uAuth;
	CString		m_User;
	CString		m_Pass;
	CString		m_OrigDomain;
	CString		m_OrigName;
	CString		m_OrigEmail;
};

// End of File

#endif
