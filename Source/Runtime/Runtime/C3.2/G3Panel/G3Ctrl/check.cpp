
#include "intern.hpp"

#include "button.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Check Box Control
//

class CCheckBox : public CStdButton
{
	public:
		// Constructor
		CCheckBox(void);

		// Destructor
		~CCheckBox(void);

		// Messages
		BOOL OnMessage(UINT uCode, DWORD dwParam);

	protected:
		// Visual Style
		int m_nSize;

		// Overridables
		void OnCreate  (IGdi *pGDI);
		void OnDraw    (IGdi *pGDI);
		void OnValidate(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Check Box Control
//

// Instantiator

IButton * Create_CheckBox(void)
{
	return New CCheckBox;
	}

// Constructor

CCheckBox::CCheckBox(void)
{
	m_nSize = 24;
	}

// Destructor

CCheckBox::~CCheckBox(void)
{
	}

// Messages

BOOL CCheckBox::OnMessage(UINT uCode, DWORD dwParam)
{
	if( uCode == msgSkipFocus ) {

		return !m_fEnable;
		}

	if( uCode == msgTouchDown ) {

		m_fPress = TRUE;

		m_uState = !m_uState;

		m_fDirty = TRUE;

		SendNotify(0, m_uState);

		return TRUE;
		}

	if( uCode == msgTouchUp ) {

		m_fPress = FALSE;

		m_fDirty = TRUE;

		return TRUE;
		}

	return FALSE;
	}

// Overridables

void CCheckBox::OnCreate(IGdi *pGDI)
{
	GetStyleMetric(metricCheckSize, m_nSize);
	}

void CCheckBox::OnDraw(IGdi *pGDI)
{
	int cb = m_nSize;

	int xb = m_Rect.x1;

	int yb = (m_Rect.y1 + m_Rect.y2 - cb) / 2;

	pGDI->SetPenFore(m_fEnable ? m_colBorder : m_colDisabled);

	pGDI->ShadeRect (xb, yb, xb+cb, yb+cb, GetShader(0));

	pGDI->DrawRect  (xb, yb, xb+cb, yb+cb);

	if( m_uState ) {

		pGDI->SetPenFore(m_fEnable ? m_colText : m_colDisabled);
	
		pGDI->SetPenWidth(2);

		pGDI->SetPenCaps (0);

		pGDI->DrawLine(xb+3, yb+3, xb+cb-3, yb+cb-3);

		pGDI->DrawLine(xb+3, yb+cb-4, xb+cb-3, yb+2);
		}

	pGDI->SelectFont  (m_pFont);

	pGDI->SetTextTrans(modeTransparent);

	pGDI->SetTextFore (m_fEnable ? m_colText : m_colDisabled);

	int cy = pGDI->GetTextHeight(m_sText);

	int xp = xb + 3 * cb / 2;

	int yp = m_Rect.y1 + (m_Rect.y2 - m_Rect.y1 - cy) / 2;

	pGDI->TextOut(xp, yp, m_sText);
	}

void CCheckBox::OnValidate(void)
{
	m_uState = m_uState ? 1 : 0;
	}

// End of File
