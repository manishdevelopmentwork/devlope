
#include "Intern.hpp"

#include "BaseFileSupport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// File System Support
//

// Standard Streams

#define STDIN   0
#define STDOUT  1
#define STDERR  2

// Constructor

CBaseFileSupport::CBaseFileSupport(void)
{
	m_pLock    = Create_Qutex();

	m_HostSep  = '\\';

	memset(m_TempHandle, 0, sizeof(m_TempHandle));

	memset(m_TempRecord, 0, sizeof(m_TempRecord));

	memset(m_HostHandle, 0, sizeof(m_HostHandle));

	m_uHostBase = 3;

	m_uTempBase = m_uHostBase + elements(m_HostHandle);

	StdSetRef();
}

// Destructor

CBaseFileSupport::~CBaseFileSupport(void)
{
	for( UINT r = 0; r < elements(m_TempRecord); r++ ) {

		free(m_TempRecord[r].m_pData);
	}

	AfxRelease(m_pLock);
}

// IUnknown

HRESULT CBaseFileSupport::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IFileSupport);

	StdQueryInterface(IFileSupport);

	StdQueryInterface(IThreadNotify);

	return E_NOINTERFACE;
}

ULONG CBaseFileSupport::AddRef(void)
{
	StdAddRef();
}

ULONG CBaseFileSupport::Release(void)
{
	StdRelease();
}

// IFileSupport

int CBaseFileSupport::IsConsole(int fd)
{
	if( fd == STDIN || fd == STDOUT || fd == STDERR ) {

		return 1;
	}

	return 0;
}

int CBaseFileSupport::Rename(char const *from, char const *to)
{
	if( IsTemp(from) ) {

		AfxTrace("fs: can't rename /tmp files\n");
	}
	else {
		CString From = MakeHost(from, false);

		CString To   = to;

		if( To.FindOne(":/\\") == NOTHING ) {

			if( HostRename(From, To) ) {

				return 0;
			}
		}
		else {
			AfxTrace("fs: can't rename to qualified path\n");
		}
	}

	return -1;
}

int CBaseFileSupport::Unlink(char const *name)
{
	if( IsTemp(name) ) {

		CAutoLock Lock(m_pLock);

		for( UINT r = 0; r < elements(m_TempRecord); r++ ) {

			CTempRecord &Record = m_TempRecord[r];

			if( Record.m_fUsed ) {

				if( !strcasecmp(Record.m_sName, name) ) {

					if( Record.m_uRefs ) {

						for( int h = 0; h < elements(m_TempHandle); h++ ) {

							CTempHandle &Handle = m_TempHandle[h];

							if( Handle.m_fUsed ) {

								if( Handle.m_uRecord == r ) {

									Handle.m_fKill = TRUE;
								}
							}
						}
					}
					else {
						free(Record.m_pData);

						Record.m_pData = NULL;

						Record.m_fUsed = FALSE;
					}

					return 0;
				}
			}
		}
	}
	else {
		if( HostUnlink(MakeHost(name, false)) ) {

			return 0;
		}
	}

	return -1;
}

int CBaseFileSupport::Stat(char const *name, struct stat *buffer)
{
	if( IsTemp(name) ) {

		CAutoLock Lock(m_pLock);

		for( UINT r = 0; r < elements(m_TempRecord); r++ ) {

			CTempRecord &Record = m_TempRecord[r];

			if( Record.m_fUsed ) {

				if( !strcasecmp(Record.m_sName, name) ) {

					buffer->st_atime = 0;
					buffer->st_ctime = 0;
					buffer->st_mtime = 0;
					buffer->st_dev   = 0;
					buffer->st_rdev  = 0;
					buffer->st_nlink = 1;
					buffer->st_size  = Record.m_dwSize;
					buffer->st_mode  = S_IREAD | S_IWRITE;

					return 0;
				}
			}
		}
	}
	else {
		if( HostStat(MakeHost(name, false), buffer) ) {

			return 0;
		}
	}

	return -1;
}

int CBaseFileSupport::UTime(char const *name, time_t time)
{
	if( IsTemp(name) ) {

		CAutoLock Lock(m_pLock);

		for( UINT r = 0; r < elements(m_TempRecord); r++ ) {

			CTempRecord &Record = m_TempRecord[r];

			if( Record.m_fUsed ) {

				if( !strcasecmp(Record.m_sName, name) ) {

					return 0;
				}
			}
		}
	}
	else {
		if( HostUTime(MakeHost(name, false), time) ) {

			return 0;
		}
	}

	return -1;
}

int CBaseFileSupport::ChMod(char const *name, mode_t mode)
{
	if( IsTemp(name) ) {

		CAutoLock Lock(m_pLock);

		for( UINT r = 0; r < elements(m_TempRecord); r++ ) {

			CTempRecord &Record = m_TempRecord[r];

			if( Record.m_fUsed ) {

				if( !strcasecmp(Record.m_sName, name) ) {

					return 0;
				}
			}
		}
	}
	else {
		if( HostChMod(MakeHost(name, false), mode) ) {

			return 0;
		}
	}

	return -1;
}

int CBaseFileSupport::Open(char const *name, int oflag, int pmode)
{
	if( IsTemp(name) ) {

		CAutoLock Lock(m_pLock);

		for( int h = 0; h < elements(m_TempHandle); h++ ) {

			CTempHandle &Handle = m_TempHandle[h];

			if( !Handle.m_fUsed ) {

				UINT n = NOTHING;

				for( UINT r = 0; r < elements(m_TempRecord); r++ ) {

					CTempRecord &Record = m_TempRecord[r];

					if( Record.m_fUsed ) {

						if( !strcasecmp(Record.m_sName, name) ) {

							if( oflag & O_EXCL ) {

								AfxTrace("fs: attempt to exclusively open existing file\n");

								return -1;
							}

							if( oflag & O_TRUNC ) {

								if( Record.m_uRefs ) {

									AfxTrace("fs: attempt to truncate open file\n");

									return -1;
								}

								Record.m_dwSize = 0;
							}

							Record.m_uRefs++;

							Handle.m_fUsed   = true;
							Handle.m_fKill   = false;
							Handle.m_uRecord = r;
							Handle.m_dwPtr   = (oflag & O_APPEND) ? Record.m_dwSize : 0;
							Handle.m_pInfo   = (CThreadInfo *) AddThreadNotify(this);

							AfxListAppend(Handle.m_pInfo->m_pHeadTemp,
								      Handle.m_pInfo->m_pTailTemp,
								      (&Handle),
								      m_pNext,
								      m_pPrev
							);

							return m_uTempBase + h;
						}
					}
					else {
						if( n == NOTHING ) {

							n = r;
						}
					}
				}

				if( oflag & O_CREAT ) {

					if( n < NOTHING ) {

						CTempRecord &Record = m_TempRecord[n];

						strcpy(Record.m_sName, name);

						Record.m_fUsed   = true;
						Record.m_dwSize  = 0;
						Record.m_dwAlloc = 0x20000;
						Record.m_pData   = PBYTE(malloc(Record.m_dwAlloc));
						Record.m_uRefs   = 1;

						Handle.m_fUsed   = true;
						Handle.m_fKill   = false;
						Handle.m_uRecord = n;
						Handle.m_dwPtr   = 0;
						Handle.m_pInfo   = (CThreadInfo *) AddThreadNotify(this);

						AfxListAppend(Handle.m_pInfo->m_pHeadTemp,
							      Handle.m_pInfo->m_pTailTemp,
							      (&Handle),
							      m_pNext,
							      m_pPrev
						);

						return m_uTempBase + h;
					}

					AfxTrace("fs: no temporary files available\n");
				}
			}
		}
	}
	else {
		CAutoLock Lock(m_pLock);

		for( int h = 0; h < elements(m_HostHandle); h++ ) {

			CHostHandle &Handle = m_HostHandle[h];

			if( !Handle.m_fUsed ) {

				if( (Handle.m_hFile = HostOpen(MakeHost(name, false), oflag, pmode)) ) {

					Handle.m_fUsed = true;

					Handle.m_pInfo = (CThreadInfo *) AddThreadNotify(this);

					AfxListAppend(Handle.m_pInfo->m_pHeadHost,
						      Handle.m_pInfo->m_pTailHost,
						      (&Handle),
						      m_pNext,
						      m_pPrev
					);

					return m_uHostBase + h;
				}

				break;
			}
		}
	}

	return -1;
}

int CBaseFileSupport::Close(int fd)
{
	if( fd == STDIN || fd == STDOUT || fd == STDERR ) {

		return -1;
	}

	if( IsTemp(fd) ) {

		CTempHandle &Handle = m_TempHandle[fd - m_uTempBase];

		if( Handle.m_fUsed ) {

			CAutoLock Lock(m_pLock);

			CTempRecord &Record = m_TempRecord[Handle.m_uRecord];

			if( !--Record.m_uRefs ) {

				if( Handle.m_fKill ) {

					free(Record.m_pData);

					Record.m_pData = NULL;

					Record.m_fUsed = false;
				}
				else {
					if( Record.m_dwAlloc > Record.m_dwSize ) {

						Record.m_pData   = PBYTE(realloc(Record.m_pData, Record.m_dwSize));

						Record.m_dwAlloc = Record.m_dwSize;
					}
				}
			}

			AfxListRemove(Handle.m_pInfo->m_pHeadTemp,
				      Handle.m_pInfo->m_pTailTemp,
				      (&Handle),
				      m_pNext,
				      m_pPrev
			);

			Handle.m_fUsed = false;

			return 0;
		}
	}
	else {
		if( IsHost(fd) ) {

			CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

			if( Handle.m_fUsed ) {

				CAutoLock Lock(m_pLock);

				HostClose(Handle.m_hFile);

				AfxListRemove(Handle.m_pInfo->m_pHeadHost,
					      Handle.m_pInfo->m_pTailHost,
					      (&Handle),
					      m_pNext,
					      m_pPrev
				);

				Handle.m_fUsed = false;

				return 0;
			}
		}
	}

	return -1;
}

int CBaseFileSupport::FStat(int fd, struct stat *buffer)
{
	if( fd == STDIN || fd == STDOUT || fd == STDERR ) {

		buffer->st_atime = 0;
		buffer->st_ctime = 0;
		buffer->st_mtime = 0;
		buffer->st_dev   = 0;
		buffer->st_rdev  = 0;
		buffer->st_nlink = 1;
		buffer->st_size  = 0;
		buffer->st_mode  = S_IFCHR;

		if( fd == STDIN ) {

			buffer->st_mode |= S_IREAD;
		}

		if( fd == STDOUT || fd == STDERR ) {

			buffer->st_mode |= S_IWRITE;
		}

		return 0;
	}

	if( IsTemp(fd) ) {

		CTempHandle &Handle = m_TempHandle[fd - m_uTempBase];

		if( Handle.m_fUsed ) {

			CTempRecord &Record = m_TempRecord[Handle.m_uRecord];

			buffer->st_atime = 0;
			buffer->st_ctime = 0;
			buffer->st_mtime = 0;
			buffer->st_dev   = 0;
			buffer->st_rdev  = 0;
			buffer->st_nlink = 1;
			buffer->st_size  = Record.m_dwSize;
			buffer->st_mode  = S_IREAD | S_IWRITE;

			return 0;
		}
	}
	else {
		if( IsHost(fd) ) {

			CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

			if( Handle.m_fUsed ) {

				if( HostStat(Handle.m_hFile, buffer) ) {

					return 0;
				}
			}
		}
	}

	return -1;
}

int CBaseFileSupport::Read(int fd, void *buffer, unsigned int count)
{
	if( fd == STDIN || fd == STDOUT || fd == STDERR ) {

		return 0;
	}

	if( IsTemp(fd) ) {

		CTempHandle &Handle = m_TempHandle[fd - m_uTempBase];

		if( Handle.m_fUsed ) {

			CTempRecord &Record = m_TempRecord[Handle.m_uRecord];

			MakeMin(count, Record.m_dwSize - Handle.m_dwPtr);

			memcpy(buffer, Record.m_pData + Handle.m_dwPtr, count);

			Handle.m_dwPtr += count;

			return count;
		}
	}
	else {
		if( IsHost(fd) ) {

			CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

			if( Handle.m_fUsed ) {

				return HostRead(Handle.m_hFile, buffer, count);
			}
		}
	}

	return -1;
}

int CBaseFileSupport::Write(int fd, void const *buffer, unsigned int count)
{
	if( fd == STDIN ) {

		return -1;
	}

	if( fd == STDOUT || fd == STDERR ) {

		PCTXT pFrom = PCTXT(buffer);

		if( pFrom[count] ) {

			if( count < 256 ) {

				char sCopy[256];

				memcpy(sCopy, buffer, count);

				sCopy[count] = 0;

				AfxPrint(sCopy);
			}
			else {
				CAutoArray<char> pCopy(count);

				memcpy(pCopy, buffer, count);

				pCopy[count] = 0;

				AfxPrint(pCopy);
			}
		}
		else
			AfxPrint(pFrom);

		return count;
	}

	if( IsTemp(fd) ) {

		CTempHandle &Handle = m_TempHandle[fd - m_uTempBase];

		if( Handle.m_fUsed ) {

			CTempRecord &Record = m_TempRecord[Handle.m_uRecord];

			ExtendTempFile(Record, Handle.m_dwPtr + count);

			memcpy(Record.m_pData + Handle.m_dwPtr, buffer, count);

			Handle.m_dwPtr += count;

			return count;
		}
	}
	else {
		if( IsHost(fd) ) {

			CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

			if( Handle.m_fUsed ) {

				return HostWrite(Handle.m_hFile, buffer, count);
			}
		}
	}

	return -1;
}

int CBaseFileSupport::LSeek(int fd, long offset, int origin)
{
	if( fd == STDIN || fd == STDOUT || fd == STDERR ) {

		return 0;
	}

	if( IsTemp(fd) ) {

		CTempHandle &Handle = m_TempHandle[fd - m_uTempBase];

		if( Handle.m_fUsed ) {

			CTempRecord &Record = m_TempRecord[Handle.m_uRecord];

			if( origin == SEEK_SET ) {

				Handle.m_dwPtr = offset;
			}

			if( origin == SEEK_CUR ) {

				Handle.m_dwPtr += offset;
			}

			if( origin == SEEK_END ) {

				Handle.m_dwPtr = Record.m_dwSize + offset;
			}

			ExtendTempFile(Record, Handle.m_dwPtr);

			return Handle.m_dwPtr;
		}
	}
	else {
		if( IsHost(fd) ) {

			CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

			if( Handle.m_fUsed ) {

				return HostSeek(Handle.m_hFile, offset, origin);
			}
		}
	}

	return -1;
}

int CBaseFileSupport::FTruncate(int fd, long size)
{
	if( fd == STDIN || fd == STDOUT || fd == STDERR ) {

		return 0;
	}

	if( IsTemp(fd) ) {

		CTempHandle &Handle = m_TempHandle[fd - m_uTempBase];

		if( Handle.m_fUsed ) {

			CTempRecord &Record = m_TempRecord[Handle.m_uRecord];

			Record.m_dwSize  = size;

			Record.m_dwAlloc = size;

			Record.m_pData   = PBYTE(realloc(Record.m_pData, size));

			if( Handle.m_dwPtr > DWORD(size) ) {

				Handle.m_dwPtr = size;
			}

			return 0;
		}
	}
	else {
		if( IsHost(fd) ) {

			CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

			if( Handle.m_fUsed ) {

				if( HostTruncate(Handle.m_hFile, size) ) {

					return 0;
				}
			}
		}
	}

	return -1;
}

int CBaseFileSupport::IoCtl(int fd, int func, void *data)
{
	if( IsHost(fd) ) {

		CHostHandle &Handle = m_HostHandle[fd - m_uHostBase];

		if( Handle.m_fUsed ) {

			return HostIoCtl(Handle.m_hFile, func, data);
		}
	}

	return -1;
}

char * CBaseFileSupport::GetCwd(char *buff, size_t size)
{
	CString Cwd = GetCwd();

	if( size >= Cwd.GetLength() + 1 ) {

		strcpy(buff, Cwd);

		return buff;
	}

	return NULL;
}

int CBaseFileSupport::ChDir(char const *name)
{
	if( IsTemp(name) ) {

		AfxTrace("fs: can't change to /tmp directories\n");
	}
	else {
		CString Path = MakeHost(name, true);

		if( HostIsValidDir(Path) ) {

			CThreadInfo *pInfo = (CThreadInfo *) AddThreadNotify(this);

			pInfo->m_Cwd = MakeAeon(Path);

			return 0;
		}
	}

	return -1;
}

int CBaseFileSupport::RmDir(char const *name)
{
	if( IsTemp(name) ) {

		AfxTrace("fs: can't remove /tmp directories\n");
	}
	else {
		if( HostRmDir(MakeHost(name, true)) ) {

			return 0;
		}
	}

	return -1;
}

int CBaseFileSupport::MkDir(char const *name)
{
	if( IsTemp(name) ) {

		AfxTrace("fs: can't create /tmp directories\n");
	}
	else {
		// We support deep creation here so that the operating
		// system layer doesn't have to worry about it for now.

		CStringArray List;

		CString      Walk;

		MakeHost(name, true).Tokenize(List, m_HostSep);

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			Walk += List[n];

			if( n ) {

				if( !HostIsValidDir(Walk) && !HostMkDir(Walk) ) {

					return -1;
				}
			}

			Walk += m_HostSep;
		}

		return 0;
	}

	return -1;
}

int CBaseFileSupport::ScanDir(CScanDirArgs const &Args)
{
	if( IsTemp(Args.name) ) {

		AfxTrace("fs: can't scan /tmp directories\n");
	}
	else {
		int n;

		if( (n = HostScanDir(MakeHost(Args.name, true), Args.list, Args.selector)) >= 0 ) {

			if( Args.compare ) {

				if( n > 1 ) {

					qsort(*Args.list, n, sizeof(dirent *), (__compar_fn_t) Args.compare);
				}
			}

			return n;
		}
	}

	return -1;
}

int CBaseFileSupport::Sync(void)
{
	if( HostSync() ) {

		return 0;
	}

	return -1;
}

int CBaseFileSupport::MapName(char *name, int add)
{
	strcpy(name, MakeHost(name, add ? true : false));

	return 0;
}

// IThreadNotify

UINT CBaseFileSupport::OnThreadCreate(IThread *pThread, UINT uIndex)
{
	CThreadInfo *pInfo = New CThreadInfo;

	pInfo->m_Cwd = "C:\\";

	pInfo->m_pHeadTemp = NULL;

	pInfo->m_pTailTemp = NULL;

	pInfo->m_pHeadHost = NULL;

	pInfo->m_pTailHost = NULL;

	return UINT(pInfo);
}

void CBaseFileSupport::OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam)
{
	CThreadInfo *pInfo = (CThreadInfo *) uParam;

	if( pInfo->m_pHeadTemp ) {

		AfxTrace("fs: thread %u exiting with open temp files\n", pThread->GetIdent());

		while( pInfo->m_pHeadTemp ) {

			int fd = m_uTempBase + (pInfo->m_pHeadTemp - m_TempHandle);

			Close(fd);
		}
	}

	if( pInfo->m_pHeadHost ) {

		AfxTrace("fs: thread %u exiting with open host files\n", pThread->GetIdent());

		while( pInfo->m_pHeadHost ) {

			int fd = m_uHostBase + (pInfo->m_pHeadHost - m_HostHandle);

			Close(fd);
		}
	}

	delete pInfo;
}

// Implementation

bool CBaseFileSupport::IsRoot(CString const &Path)
{
	return Path.GetLength() == (m_HostRoot.IsEmpty() ? 1 : (m_HostRoot.GetLength() + 2));
}

bool CBaseFileSupport::IsHost(int fd)
{
	return UINT(fd) >= m_uHostBase && UINT(fd) < m_uHostBase + elements(m_HostHandle);
}

bool CBaseFileSupport::IsTemp(int fd)
{
	return UINT(fd) >= m_uTempBase && UINT(fd) < m_uTempBase + elements(m_TempHandle);
}

bool CBaseFileSupport::IsTemp(char const *name)
{
	if( !strncmp(name, "/tmp/", 5) ) {

		if( !strchr(name+5, '/') ) {

			return true;
		}
	}

	return false;
}

void CBaseFileSupport::ExtendTempFile(CTempRecord &Record, DWORD dwSize)
{
	if( dwSize > Record.m_dwSize ) {

		if( dwSize > Record.m_dwAlloc ) {

			Record.m_dwAlloc = ((dwSize + 0x20000 - 1) / 0x20000) * 0x20000;

			Record.m_pData   = PBYTE(realloc(Record.m_pData, Record.m_dwAlloc));
		}

		Record.m_dwSize = dwSize;
	}
}

CString CBaseFileSupport::MakeHost(CString Path, bool fAdd)
{
	UINT p, f, m;

	CString Host;

	Path.Replace('/', '\\');

	if( !Path.StartsWith("\\!\\") ) {

		if( !Path.StartsWith("\\.\\") ) {

			if( Path[0] == '\\' ) {

				if( m_HostRoot.IsEmpty() ) {

					Host = "C:" + Path;
				}
				else {
					if( Path[2] == ':' ) {

						Host = m_HostRoot + Path[1] + Path.Mid(3);
					}
					else {
						Host = m_HostRoot + "C" + Path;
					}
				}
			}
			else {
				if( Path[1] == ':' ) {

					if( m_HostRoot.IsEmpty() ) {

						Host = Path;
					}
					else
						Host = m_HostRoot + char(toupper(Path[0])) + Path.Mid(2);
				}
				else {
					CString Base = GetCwd();

					if( m_HostRoot.IsEmpty() ) {

						Host = Base;
					}
					else
						Host = m_HostRoot + char(toupper(Base[0])) + Base.Mid(2);

					if( !Base.EndsWith('\\') ) {

						Host += '\\';
					}

					Host += Path;
				}
			}

			m = m_HostRoot.GetLength();
		}
		else {
			#if defined(AEON_PLAT_WIN32)

			Host = m_HostBase + Path.Mid(3);

			m    = m_HostBase.GetLength();

			#else

			Host = Path.Mid(2);

			m = 1;

			#endif
		}
	}
	else {
		Host = m_HostBase + Path.Mid(3);

		m    = m_HostBase.GetLength();
	}

	if( fAdd ) {

		if( !Host.EndsWith('\\') ) {

			Host += '\\';
		}
	}

	while( (p = Host.Find("\\\\")) < NOTHING ) {

		Host.Delete(p, 1);
	}

	while( (p = Host.Find("\\.\\")) < NOTHING ) {

		Host.Delete(p, 2);
	}

	while( (p = Host.Find("\\..\\")) < NOTHING && (f = Host.FindRev('\\', p-1)) < NOTHING ) {

		if( f < m ) {

			Host.Delete(p, 3);
		}
		else
			Host.Delete(f, p + 3 - f);
	}

	if( m_HostSep != '\\' ) {

		Host.Replace('\\', m_HostSep);
	}

	return Host;
}

CString CBaseFileSupport::MakeAeon(CString Path)
{
	CString Aeon;

	if( m_HostRoot.IsEmpty() ) {

		Aeon = Path;
	}
	else {
		UINT uStrip   = m_HostRoot.GetLength();

		char sDrive[] = { Path[uStrip], ':', 0 };

		Aeon = sDrive + Path.Mid(uStrip + 1);
	}

	if( Aeon.GetLength() > 3 ) {

		Aeon.Delete(Aeon.GetLength()-1, 1);
	}

	if( m_HostSep != '\\' ) {

		Aeon.Replace(m_HostSep, '\\');
	}

	return Aeon;
}

CString CBaseFileSupport::GetCwd(void)
{
	CThreadInfo *pInfo = (CThreadInfo *) AddThreadNotify(this);

	return pInfo->m_Cwd;
}

void CBaseFileSupport::EmptyDirectory(CString const &Path)
{
	struct dirent **list;

	int    c;

	if( (c = HostScanDir(Path, &list, NULL)) > 0 ) {

		for( int p = 0; p < 2; p++ ) {

			for( int n = 0; n < c; n++ ) {

				struct dirent *file = list[n];

				if( file->d_type == DT_DIR ) {

					if( p == 0 ) {

						if( !strcmp(file->d_name, ".") || !strcmp(file->d_name, "..") ) {

							continue;
						}

						CString Full = Path + file->d_name + m_HostSep;

						EmptyDirectory(Full);

						HostRmDir(Full);
					}
				}
				else {
					if( p == 1 ) {

						HostUnlink(Path + file->d_name);
					}
				}

				if( p == 1 ) {

					free(file);
				}
			}
		}

		free(list);
	}
}

// End of File
