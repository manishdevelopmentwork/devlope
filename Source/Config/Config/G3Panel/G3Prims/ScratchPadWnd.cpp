
#include "intern.hpp"

#include "ScratchPadWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DispCatWnd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Scratch Pad Window
//

// Private IDMs

#define IDM_TOOL	0xB2
#define	IDM_TOOL_CLOSE	0xB201
#define	IDM_TOOL_PIN	0xB202

// Base Class

#define CBaseClass CViewWnd

// Runtime Class

AfxImplementRuntimeClass(CScratchPadWnd, CBaseClass);

// Timer IDs

UINT CScratchPadWnd::m_timerHover  = CWnd::AllocTimerID();

UINT CScratchPadWnd::m_timerSelect = CWnd::AllocTimerID();

// Constructor

CScratchPadWnd::CScratchPadWnd(void)
{
	m_dwStyle  = WS_BORDER;

	m_pView    = AfxNewObject(CViewWnd, AfxNamedClass(L"CScratchPadEditorWnd"));

	m_pMain    = NULL;

	m_fPinned  = FALSE;

	m_fDrag    = FALSE;

	m_fHide    = FALSE;

	m_fCapture = FALSE;

	m_fHover   = FALSE;

	m_fTiny    = TRUE;

	m_fActive  = FALSE;

	m_fMoving  = TRUE;

	m_pBlock   = NULL;

	m_pSemi    = NULL;

	m_pToolTip = NULL;

	m_MoveCursor.Create(L"DispMove");

	m_Bitmap.Create(L"ScratchPad");

	m_System.Bind();
	}

// Destructor

CScratchPadWnd::~CScratchPadWnd(void)
{
	}

// Attributes

BOOL CScratchPadWnd::IsActive(void) const
{
	return m_fActive;
	}

CViewWnd * CScratchPadWnd::GetView(void) const
{
	return m_pView;
	}
	
// Operations

void CScratchPadWnd::Destroy(void)
{
	if( m_hWnd ) {

		DestroyWindow(TRUE);
		}
	}

void CScratchPadWnd::SetSemiModal(CDialog *pDlg)
{
	if( m_hWnd ) {

		if( (m_pSemi = pDlg) ) {

			if( !m_pBlock ) {

				CRect Rect = GetClientRect();

				m_pBlock   = New CBlockerWnd(this, pDlg, Rect);

				m_pBlock->Create();

				if( !m_fTiny ) {

					m_pView->ShowWindow(SW_HIDE);
					}
				}
			else
				m_pBlock->SetDialog(pDlg);
			}
		else {
			if( !m_fTiny ) {

				m_pView->ShowWindow(SW_SHOWNA);
				}

			m_pBlock->ShowWindow(SW_HIDE);

			m_pBlock->DestroyWindow(FALSE);

			m_pBlock = NULL;
			}
		}
	}

void CScratchPadWnd::Show(BOOL fShow)
{
	if( fShow ) {

		if( !m_hWnd ) {

			CWnd::Create( CString(IDS_SCRATCHPAD),
				      WS_EX_TOOLWINDOW,
				      WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP | m_dwStyle,
				      CRect(0, 0, 100, 100),
				      afxMainWnd->GetHandle(),
				      AfxNull(CMenu),
				      NULL
				      );

			CreateToolTip();
			}

		ShowWindow(SW_SHOWNA);

		UpdateWindow();
		}
	else {
		if( m_hWnd ) {

			DestroyToolTip();

			ShowWindow(SW_HIDE);
			}
		}
	}

void CScratchPadWnd::NavTreeActive(CRect const &Rect)
{
	if( m_hWnd ) {

		if( !m_fPinned ) {

			if( GetWindowRect().Overlaps(Rect) ) {

				SetTiny(TRUE, FALSE);
				}
			}
		}
	}

void CScratchPadWnd::NavToScratch(void)
{
	if( m_hWnd ) {

		SetTiny(FALSE, FALSE);
		}
	}

void CScratchPadWnd::DragFromScratch(BOOL fDrag)
{
	if( m_fDrag != fDrag ) {

		if( (m_fDrag = fDrag) ) {

			m_fHide = FALSE;
			}
		else {
			if( m_fHide ) {

				ShowWindow(SW_SHOWNA);
				}
			}
		}
	}

void CScratchPadWnd::DragIntoMain(void)
{
	if( m_hWnd ) {

		if( m_fDrag && !m_fPinned ) {

			if( OverlapsMain() ) {

				m_fHide = TRUE;

				ShowWindow(SW_HIDE);
				}
			}
		}
	}

void CScratchPadWnd::ClickOnMain(void)
{
	if( m_hWnd ) {

		if( !m_fPinned ) {

			if( OverlapsMain() ) {
			
				SetTiny(TRUE, FALSE);
				}
			}
		}
	}

// IUnknown

HRESULT CScratchPadWnd::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CScratchPadWnd::AddRef(void)
{
	return 1;
	}

ULONG CScratchPadWnd::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CScratchPadWnd::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	SetTimer(m_timerSelect, 300);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CScratchPadWnd::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CScratchPadWnd::DragLeave(void)
{
	KillTimer(m_timerSelect);

	m_DropHelp.DragLeave();

	return S_OK;
	}

HRESULT CScratchPadWnd::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	KillTimer(m_timerSelect);

	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	*pEffect = DROPEFFECT_NONE;
	
	return S_OK;
	}

// Overribables

void CScratchPadWnd::OnAttach(void)
{
	m_pPage = (CDispPage *) m_pItem;

	m_pView->Attach(m_pItem);
	}

BOOL CScratchPadWnd::OnPreDetach(void)
{
	return TRUE;
	}

void CScratchPadWnd::OnDetach(void)
{
	m_pView->Detach();
	}

CString CScratchPadWnd::OnGetNavPos(void)
{
	AfxAssert(FALSE);

	return L"";
	}

BOOL CScratchPadWnd::OnNavigate(CString const &Nav)
{
	if( m_pView->Navigate(Nav) ) {

		SetActiveWindow();

		return TRUE;
		}

	return FALSE;
	}

void CScratchPadWnd::OnExec(CCmd *pCmd)
{
	AfxAssert(FALSE);
	}

void CScratchPadWnd::OnUndo(CCmd *pCmd)
{
	AfxAssert(FALSE);
	}

// Routing Control

BOOL CScratchPadWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView->RouteMessage(Message, lResult) ) {

		return TRUE;
		}

	return CBaseClass::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CScratchPadWnd, CBaseClass)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_DESTROY)
	AfxDispatchMessage(WM_GOINGIDLE)
	AfxDispatchMessage(WM_MAINMOVED)
	AfxDispatchMessage(WM_MOUSEACTIVATE)
	AfxDispatchMessage(WM_ACTIVATE)
	AfxDispatchMessage(WM_WINDOWPOSCHANGED)
	AfxDispatchMessage(WM_CLOSE)
	AfxDispatchMessage(WM_COMMAND)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_PRINT)
	AfxDispatchMessage(WM_NCHITTEST)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_TIMER)
	AfxDispatchMessage(WM_SHOWWINDOW)

	AfxDispatchNotify (0, TTN_LINKCLICK, OnToolLinkClick)

	AfxDispatchControlType(IDM_TOOL, OnToolControl)
	AfxDispatchGetInfoType(IDM_TOOL, OnToolGetInfo)
	AfxDispatchCommandType(IDM_TOOL, OnToolCommand)

	AfxMessageEnd(CScratchPadWnd)
	};

// Message Handlers

void CScratchPadWnd::OnPostCreate(void)
{
	CClientDC DC(ThisObject);

	m_nCaption = 20;

	m_Font.Create(DC, afxDlgFont, 8, 0);

	FindTinyRect();

	FindNormRect();

	m_pView->Create( WS_CHILD | WS_CLIPCHILDREN,
			 GetViewRect(),
			 ThisObject, IDVIEW, NULL
			 );

	m_pTool = New CToolbarWnd(barTight | barFlat | barDark | barPrivate);

	m_pTool->AddGadget(New CButtonGadget(IDM_TOOL_CLOSE, 0x10000010, 1));

	m_pTool->AddGadget(New CToggleGadget(IDM_TOOL_PIN,   0x1000003C, 1));
	
	m_pTool->Create(GetToolbarRect(), ThisObject);

	m_pTool->PollGadgets();

	SetTiny(FALSE);

	((CMainWnd *) afxMainWnd)->AddStickyPopup(this);

	m_DropHelp.Bind(m_hWnd, this);
	}

void CScratchPadWnd::OnDestroy(void)
{
	((CMainWnd *) afxMainWnd)->RemoveStickyPopup(this);
	}

void CScratchPadWnd::OnGoingIdle(void)
{
	m_pTool->PollGadgets();
	}

void CScratchPadWnd::OnMainMoved(CRect const *pRect)
{
	CRect const &Prev = pRect[0];

	CRect const &Move = pRect[1];

	if( Prev.GetSize() == Move.GetSize() ) {

		CRect Rect;
		
		if( m_fTiny ) {
			
			Rect = GetWindowRect();
			}
		else
			Rect = CRect(m_TinyPos, m_TinySize);

		if( Prev.Encloses(Rect) ) {

			Rect -= Prev.GetTopLeft() - Move.GetTopLeft();

			if( m_fTiny ) {

				MoveWindow(Rect, TRUE);
				}
			else {
				m_TinyPos  = Rect.GetTopLeft();
				
				m_TinySize = Rect.GetSize();
				}
			}
		}
	}

UINT CScratchPadWnd::OnMouseActivate(CWnd &Parent, UINT uHitTest, UINT uMessage)
{
	if( m_pBlock ) {

		m_pSemi->ForceActive();

		MessageBeep(0);
		
		return MA_NOACTIVATEANDEAT;
		}

	AfxCallDefProc();
	
	return MA_ACTIVATE;
	}

void CScratchPadWnd::OnActivate(UINT uCode, BOOL fMinimized, CWnd &Wnd)
{
	if( (m_fActive = (uCode > 0)) ) {

		if( FindMainEditor() ) {

			m_pMain->SendMessage(WM_CANCELMODE);
			}
		}
	else {
		if( !m_fTiny ) {

			m_pView->SendMessage(WM_CANCELMODE);
			}
		}

	Invalidate(FALSE);

	AfxCallDefProc();
	}

void CScratchPadWnd::OnWindowPosChanged(WINDOWPOS &Pos)
{
	if( m_fTiny ) {

		if( !m_fMoving ) {

			if( !(Pos.flags & SWP_NOMOVE) ) {

				m_TinyPos = CPoint(Pos.x, Pos.y);
				}
			}
		}
	else {
		MoveToolbar();

		if( !m_fMoving ) {

			if( !(Pos.flags & SWP_NOMOVE) ) {

				m_NormPos = CPoint(Pos.x, Pos.y);
				}

			if( !(Pos.flags & SWP_NOSIZE) ) {

				m_NormSize = CSize(Pos.cx, Pos.cy);

				CRect ActRect = m_pPage->m_pList->GetUsedRect();

				CRect OldRect = m_pView->GetWindowRect();

				CRect NewRect = GetViewRect();

				ClientToScreen(NewRect);

				NewRect -= OldRect.GetTopLeft();

				OldRect -= OldRect.GetTopLeft();

				if( NewRect.Encloses(ActRect) ) {

					m_pPage->m_Size = NewRect.GetSize();

					m_pPage->SetDirty();

					m_pView->MoveWindow(GetViewRect(), FALSE);

					m_pView->Attach(m_pPage);

					return;
					}
				else {
					if( NewRect.cx() >= ActRect.cx() && NewRect.cy() >= ActRect.cy() ) {

						m_pPage->m_pList->SizePrims(OldRect, NewRect);

						m_pPage->m_Size = NewRect.GetSize();

						m_pPage->SetDirty();

						m_pView->MoveWindow(GetViewRect(), FALSE);

						m_pView->Attach(m_pPage);

						return;
						}
					}
				}
			}

		m_pView->MoveWindow(GetViewRect(), TRUE);
		}
	}

void CScratchPadWnd::OnClose(void)
{
	SetTiny(TRUE, FALSE);
	}

BOOL CScratchPadWnd::OnCommand(UINT uID, UINT uNotify, CWnd &Wnd)
{
	LPARAM lParam = m_MsgCtx.Msg.lParam;

	if( uID >= 0x8000 && uID <= 0xEFFF ) {

		RouteCommand(uID, lParam);

		return TRUE;
		}
	
	SendMessage(WM_AFX_COMMAND, uID, lParam);

	return TRUE;
	}

void CScratchPadWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_pView->IsWindowVisible() ) {

		m_pView->SetFocus();
		}
	}

void CScratchPadWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID == m_pView->GetID() ) {

		m_System.AssumeViewerActive();
		}
	}

BOOL CScratchPadWnd::OnEraseBkGnd(CDC &DC)
{
	CRect Rect = GetClientRect();

	if( m_fTiny ) {

		DC.FrameRect(Rect--, afxBrush(BLACK));

		if( m_fCapture ) {

			DC.GradVert(Rect, afxColor(Orange1), afxColor(Orange2));
			}
		else {
			if( m_fHover ) {

				DC.GradVert(Rect, afxColor(Orange3), afxColor(Orange2));
				}
			else
				DC.FillRect(Rect, afxBrush(3dFace));
			}
		}
	else {
		Rect.top += m_nCaption;

		DC.FrameRect(Rect--, afxBrush(3dFace));
		DC.FrameRect(Rect--, afxBrush(3dFace));
		DC.FrameRect(Rect--, afxBrush(3dFace));

		DC.FrameRect(Rect--, afxBrush(BLACK));
		}

	return TRUE;
	}

void CScratchPadWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_fTiny ) {

		CMemoryDC MC(DC);

		DC.SetBkColor(afxColor(MAGENTA));

		DC.TransBlt(8, 8, 48, 48, m_Bitmap, 0, 0);
		}
	else {
		CRect Rect  = GetClientRect();

		Rect.bottom = Rect.top + m_nCaption;

		DC.FillRect(Rect, afxBrush(3dFace));

		DC.SetTextColor(IsActive() ? afxColor(BLACK) : afxColor(3dShadow));

		DC.SetBkMode(TRANSPARENT);

		DC.Select(m_Font);
		
		DC.TextOut(4, 4, GetWindowText());

		DC.Deselect();
		}
	}

void CScratchPadWnd::OnPrint(CDC &DC, UINT uFlags)
{
	if( !(uFlags & PRF_CHECKVISIBLE) || IsWindowVisible() ) {

		if( uFlags & PRF_ERASEBKGND ) {

			SendMessage( WM_ERASEBKGND,
				     WPARAM(DC.GetHandle())
				     );
			}

		SendMessage( WM_PRINTCLIENT,
			     WPARAM(DC.GetHandle()),
			     LPARAM(uFlags)
			     );
		}

	CWnd *pWnd = GetWindowPtr(GW_CHILD);

	if( pWnd->IsWindow() ) {

		pWnd = pWnd->GetWindowPtr(GW_HWNDLAST);

		CClientDC DispDC(ThisObject);

		while( pWnd->IsWindow() ) {

			if( pWnd->IsWindowVisible() ) {

				CRect Rect = pWnd->GetWindowRect();

				if( !Rect.IsEmpty() ) {

					CMemoryDC WorkDC(DC);

					CBitmap   Bitmap(DispDC, Rect.GetSize());

					WorkDC.Select(Bitmap);

					pWnd->SendMessage( WM_PRINT,
							   WPARAM(WorkDC.GetHandle()),
							   LPARAM(uFlags)
							   );

					ScreenToClient(Rect);

					DC.BitBlt(Rect, WorkDC, CPoint(0, 0), SRCCOPY);

					WorkDC.Deselect();
					}
				}

			pWnd = pWnd->GetWindowPtr(GW_HWNDPREV);
			}
		}
	}

UINT CScratchPadWnd::OnNCHitTest(CPoint Pos)
{
	if( m_pBlock ) {

		return UINT(HTERROR);
		}

	if( !m_fTiny ) {

		CRect Rect = GetWindowRect();

		if( !(Rect - 4).PtInRect(Pos) ) {

			if( Pos.x < Rect.left + 8 ) {

				if( Pos.y < Rect.top + 8 ) {

					return HTTOPLEFT;
					}

				if( Pos.y > Rect.bottom - 8 ) {

					return HTTOPRIGHT;
					}

				return HTLEFT;
				}

			if( Pos.x > Rect.right - 8 ) {

				if( Pos.y < Rect.top + 8 ) {

					return HTTOPRIGHT;
					}

				if( Pos.y > Rect.bottom - 8 ) {

					return HTBOTTOMRIGHT;
					}

				return HTRIGHT;
				}

			if( Pos.y < Rect.top + 8 ) {

				return HTTOP;
				}

			if( Pos.y > Rect.bottom - 8 ) {

				return HTBOTTOM;
				}
			}

		Rect.left   += 4;
		Rect.top    += 4;
		Rect.right  -= 4;

		Rect.bottom  = Rect.top + m_nCaption;

		if( Rect.PtInRect(Pos) ) {

			return HTCAPTION;
			}
		}

	return HTCLIENT;
	}

void CScratchPadWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	if( m_fTiny ) {

		m_Click    = Pos;

		m_fCapture = TRUE;
		
		KillHover();

		Invalidate(TRUE);

		SetCapture();
		}
	}

void CScratchPadWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	if( m_fTiny ) {

		if( m_fCapture ) {

			ReleaseCapture();

			m_fCapture = FALSE;

			SetTiny(FALSE, TRUE);
			}

		if( GetClientRect().PtInRect(Pos) ) {

			InitHover();
			}
		}
	}

void CScratchPadWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	if( m_fTiny ) {

		if( m_fCapture ) {

			CSize Drag(SM_CXDRAG);

			if( abs(Pos.x - m_Click.x) >= Drag.cx || abs(Pos.y - m_Click.y) >= Drag.cy ) {

				ReleaseCapture();

				m_fCapture = FALSE;
				
				SetCursor(m_MoveCursor);

				SendMessage(WM_SYSCOMMAND, SC_MOVE | 1, LPARAM(Pos));
				}
			}
		else {
			if( GetClientRect().PtInRect(Pos) ) {

				InitHover();
				}
			}
		}
	}

void CScratchPadWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	if( m_fTiny ) {
		
		if( uID == m_timerHover ) {

			if( m_fHover ) {
	
				CPoint Pos = GetCursorPos();

				CRect  R1  = GetClientRect();

				ScreenToClient(Pos);

				if( R1.PtInRect(Pos) ) {

					InitHover();

					return;
					}
				}

			KillHover();
			}

		if( uID == m_timerSelect ) {

			SetTiny(FALSE, FALSE);

			if( !CRect(m_TinyPos, m_TinySize).Overlaps(CRect(m_NormPos, m_NormSize)) ) {

				CPoint Pos = GetWindowRect().GetCenter();

				SetCursorPos(Pos.x, Pos.y);
				}
			else {
				// Force the drop to update in the new window.

				CPoint Pos = GetCursorPos();

				Pos.x++;

				SetCursorPos(Pos.x, Pos.y);
				}

			KillTimer(m_timerSelect);
			}
		}
	}

void CScratchPadWnd::OnShowWindow(BOOL fShow, UINT uStatus)
{
	// Catching this prevent weird popup display
	// behavior which can result in the Scratchpad
	// being displayed in the wrong category.
	}

// Notification Handlers

UINT CScratchPadWnd::OnToolLinkClick(UINT uID, NMHDR &Info)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo  (L"G3Prims");

	Reg.MoveTo  (L"Scatchpad");

	Reg.SetValue(L"ShowTip", DWORD(0));

	DestroyToolTip();

	return 0;
	}

// Command Handlers

BOOL CScratchPadWnd::OnToolControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_TOOL_CLOSE:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_TOOL_PIN:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_fPinned);

			return TRUE;
		}

	return FALSE;
	}

BOOL CScratchPadWnd::OnToolGetInfo(UINT uID, CCmdInfo  &Info)
{
	switch( uID ) {

		case IDM_TOOL_CLOSE:

			Info.m_ToolTip = CString(IDS_CLOSE);

			Info.m_Prompt  = CString(IDS_MINIMIZE);

			return TRUE;

		case IDM_TOOL_PIN:

			Info.m_ToolTip = CString(IDS_PIN);

			Info.m_Prompt  = CString(IDS_PIN_SCRATCHPAD);

			return TRUE;
		}

	return FALSE;
	}

BOOL CScratchPadWnd::OnToolCommand(UINT uID)
{
	switch( uID ) {

		case IDM_TOOL_CLOSE:

			SetTiny(TRUE, FALSE);

			return TRUE;

		case IDM_TOOL_PIN:

			m_fPinned = !m_fPinned;

			return TRUE;
		}

	return FALSE;
	}

// Implementation

CRect CScratchPadWnd::GetViewRect(void)
{
	CRect Rect = GetClientRect();

	Rect.top += m_nCaption;

	Rect     -= 4;
		
	return Rect;
	}

BOOL CScratchPadWnd::FindMainEditor(void)
{
	if( !m_pMain ) {

		CDispCatWnd *pCatView = (CDispCatWnd *) m_System.GetCatView(AfxRuntimeClass(CDispCatWnd));

		m_pMain = (CViewWnd *) pCatView->GetMainEditor();
		}

	return m_pMain ? TRUE : FALSE;
	}

void CScratchPadWnd::SetTiny(BOOL fTiny, BOOL fActivate)
{
	if( m_fTiny != fTiny ) {

		m_fTiny = fTiny;

		SetTiny(fActivate);
		}
	}

void CScratchPadWnd::SetTiny(BOOL fActivate)
{
	m_fMoving = TRUE;

	BOOL fWas = FALSE;

	if( IsWindowVisible() ) {

		fWas = TRUE;
		
		ShowWindow(SW_HIDE);
		}

	if( m_fTiny ) {

		m_pView->ShowWindow(SW_HIDE);
	
		m_pTool->ShowWindow(SW_HIDE);

		SetWindowStyle(m_dwStyle, 0);

		MoveWindow(CRect(m_TinyPos, m_TinySize), FALSE);
		}
	else {
		DestroyToolTip();

		SetWindowStyle(m_dwStyle, m_dwStyle);

		MoveWindow(CRect(m_NormPos, m_NormSize), FALSE);

		MoveToolbar();
	
		m_pTool->ShowWindow(SW_SHOWNA);

		m_pView->ShowWindow(SW_SHOWNA);
		}

	if( fWas ) {

		ShowWindow(fActivate ? SW_SHOW : SW_SHOWNA);

		UpdateWindow();
		}

	m_fMoving = FALSE;
	}

void CScratchPadWnd::FindTinyRect(void)
{
	FindMainEditor();

	CRect Rect = m_pMain->GetWindowRect();

	m_TinyPos  = Rect.GetBottomLeft() + CSize(16, -80);

	m_TinySize = CSize(64, 64);
	}

void CScratchPadWnd::FindNormRect(void)
{
	CRect Rect = m_pPage->m_Size;

	Rect     += 4;

	AdjustWindowRect(Rect, GetWindowStyle() | m_dwStyle);

	Rect.top -= m_nCaption;

	m_NormPos  = m_TinyPos - CSize(64, Rect.cy() - 128);

	m_NormSize = Rect.GetSize();
	}

BOOL CScratchPadWnd::OverlapsMain(void)
{
	if( FindMainEditor() ) {

		CRect Us = GetWindowRect();

		if( m_pMain->GetWindowRect().Overlaps(Us) ) {

			CSize s1, s2;

			m_pMain->SendMessage(WM_GETMETRIC, MC_ACTIVE_POSITION, LPARAM(&s1));

			m_pMain->SendMessage(WM_GETMETRIC, MC_ACTIVE_SIZE,     LPARAM(&s2));

			CRect Active(CPoint(s1), s2);

			m_pMain->ClientToScreen(Active);

			if( Active.Overlaps(Us) ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CScratchPadWnd::InitHover(void)
{
	if( !m_fHover ) {

		m_fHover = TRUE;

		Invalidate(TRUE);
		}

	KillTimer(m_timerHover);

	SetTimer (m_timerHover, 50);
	}

void CScratchPadWnd::KillHover(void)
{
	if( m_fHover ) {

		m_fHover = FALSE;

		Invalidate(TRUE);
		}

	KillTimer(m_timerHover);
	}

void CScratchPadWnd::CreateToolTip(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Scatchpad");

	if( Reg.GetValue(L"ShowTip", DWORD(1)) ) {

		m_pToolTip = New CToolTip;

		DWORD dwStyle = TTS_NOFADE    |
				TTS_NOANIMATE |
				TTS_NOPREFIX  |
				TTS_ALWAYSTIP |
				TTS_BALLOON;

		m_pToolTip->Create(m_hWnd, 0, dwStyle);

		m_pToolTip->SetTitle(1, CString(IDS_ABOUT_SCRATCHPAD));

		m_pToolTip->SetMaxTipWidth(380);

		CString Text;

		Text = CString(IDS_CLICK_ON_ICON_TO) +
		       CString(IDS_FAVORITE)         +
		       CString(IDS_OR_DROP_ITEMS_ON) +
		       CString(IDS_ADO_NOT_SHOW);

		CToolInfo Info1(m_hWnd, 100, CRect(), Text, TTF_TRACK | TTF_PARSELINKS);

		m_pToolTip->AddTool(Info1);

		CToolInfo Info2(m_hWnd, 100);

		m_pToolTip->TrackPosition(GetWindowRect().GetBottomRight() - CPoint(32, 32));

		m_pToolTip->TrackActivate(TRUE, Info2);
		}
	}

void CScratchPadWnd::DestroyToolTip(void)
{
	if( m_pToolTip ) {

		m_pToolTip->DestroyWindow(FALSE);

		m_pToolTip = NULL;
		}
	}

void CScratchPadWnd::MoveToolbar(void)
{
	m_pTool->MoveWindow(GetToolbarRect(), TRUE);
	}

CRect CScratchPadWnd::GetToolbarRect(void)
{
	CRect Rect  = GetClientRect();

	Rect.top    = Rect.top   + 1;

	Rect.bottom = Rect.top   + m_pTool->GetHeight();

	Rect.right  = Rect.right - 1;
		
	Rect.left   = Rect.right - 40;

	return Rect;
	}

// End of File
