
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_MasterDriver_HPP

#define INCLUDE_MasterDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CommsDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Master Comms Driver Object
//

class DLLAPI CMasterDriver : public CCommsDriver
{
	public:
		// Constructor
		CMasterDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);

		// IDriver
		WORD METHOD GetCategory(void);

		// ICommsMaster
		virtual WORD  METHOD GetMasterFlags(void);
		virtual CCODE METHOD Ping(void);
		virtual CCODE METHOD Read(AREF Addr, PDWORD pData, UINT  uCount);
		virtual CCODE METHOD Write(AREF Addr, PDWORD pData, UINT  uCount);
		virtual CCODE METHOD Atomic(AREF Addr, DWORD  Data,  DWORD dwMask);
	};

// End of File

#endif
