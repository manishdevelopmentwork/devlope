
#include "intern.hpp"

#include "object.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIPADDR Wrapper Class
//

// Constructor

CCIPAddr::CCIPAddr(void)
{
	m_pPath	    = "";

	m_wClass    = EIP_EMPTY;

	m_wInst	    = EIP_EMPTY;
	
	m_wAttr	    = EIP_EMPTY;
	
	m_wMember   = EIP_EMPTY;

	m_bTagData  = NULL;
	
	m_uTagSize  = 0;
	}

// Constructor

CCIPAddr::~CCIPAddr(void)
{
	}

//////////////////////////////////////////////////////////////////////////
//
// Base CIP Object
//

// Constructor

CCIPObject::CCIPObject(void)
{
	}

BOOL CCIPObject::Service(BYTE bCode, CIOISegment &Name, CDataBuf &Data, CBuffer * &pRecv)
{
	if( m_pDriver->Send(bCode, Name, Data) ) {

		if( m_pDriver->Recv(pRecv) ) {
			
			if( pRecv ) {

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BYTE CCIPObject::GetLastError(void)
{
	return m_pDriver->GetLastError();
	}

// End of File
