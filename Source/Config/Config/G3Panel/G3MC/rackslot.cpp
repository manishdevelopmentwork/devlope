
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Slot View
//

class CCommsSlotRackWnd : public CProxyViewWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CCommsSlotRackWnd(void);

protected:
	// Overridables
	void OnAttach(void);
};

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Device View
//

// Dynamic Class

AfxImplementDynamicClass(CCommsSlotRackWnd, CProxyViewWnd);

// Constructor

CCommsSlotRackWnd::CCommsSlotRackWnd(void)
{
	m_fRecycle = FALSE;
}

// Overridables

void CCommsSlotRackWnd::OnAttach(void)
{
	CCommsPortSlot *pSlot = (CCommsPortSlot *) m_pItem;

	CGenericModule *pMod  = pSlot->GetDevice()->m_pModule;

	m_pView = pMod->CreateView(viewItem);

	m_pView->Attach(pMod);
}

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Slot List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsSlotList, CItemIndexList);

// Constructor

CCommsSlotList::CCommsSlotList(void)
{
	m_Class = NULL;
}

// Item Access

CCommsPortSlot * CCommsSlotList::GetItem(INDEX Index) const
{
	return (CCommsPortSlot *) CItemIndexList::GetItem(Index);
}

CCommsPortSlot * CCommsSlotList::GetItem(UINT uPos) const
{
	return (CCommsPortSlot *) CItemIndexList::GetItem(uPos);
}

CCommsPortSlot * CCommsSlotList::FindItem(CCommsDeviceRack *pRack) const
{
	INDEX i = GetHead();

	while( !Failed(i) ) {

		CCommsPortSlot *p = GetItem(i);

		if( p && p->GetDevice() && p->GetDevice()->m_Number == pRack->m_Number ) {

			return p;
		}

		GetNext(i);
	}

	return NULL;
}

CCommsPortSlot * CCommsSlotList::FindSlot(UINT uSlot) const
{
	INDEX i = GetHead();

	while( !Failed(i) ) {

		CCommsPortSlot *p = GetItem(i);

		if( p && p->m_SlotNum == uSlot ) {

			return p;
		}

		GetNext(i);
	}

	return NULL;
}

CCommsPortSlot * CCommsSlotList::FindRack(UINT uRack) const
{
	INDEX i = GetHead();

	while( !Failed(i) ) {

		CCommsPortSlot *p = GetItem(i);

		if( p && p->m_Rack == uRack ) {

			return p;
		}

		GetNext(i);
	}

	return NULL;
}

// Operations

void CCommsSlotList::SaveFixed(CTreeFile &File)
{
	File.PutValue(L"Fixed", m_Fixed);

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		AfxAssert(m_Class);

		CCommsPortSlot *pSlot = (CCommsPortSlot *) pItem;

		if( pSlot->m_Rack == NOTHING ) {

			File.PutElement();

			File.SetPadding(pItem->GetPadding());

			File.PutValue(L"NDX", pItem->GetIndex());

			pItem->Save(File);

			File.EndObject();
		}

		m_List.GetNext(Index);
	}
}

void CCommsSlotList::SaveTether(CTreeFile &File)
{
	File.PutValue(L"Fixed", m_Fixed);

	INDEX Index = m_List.GetHead();

	while( !m_List.Failed(Index) ) {

		CItem *pItem = m_List[Index];

		AfxAssert(m_Class);

		CCommsPortSlot *pSlot = (CCommsPortSlot *) pItem;

		if( pSlot->m_Rack != NOTHING ) {

			File.PutElement();

			File.SetPadding(pItem->GetPadding());

			File.PutValue(L"NDX", pItem->GetIndex());

			pItem->Save(File);

			File.EndObject();
		}

		m_List.GetNext(Index);
	}
}

//////////////////////////////////////////////////////////////////////////
//
// Backplane Comms Slot
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortSlot, CMetaItem);

// Constructor

CCommsPortSlot::CCommsPortSlot(void)
{
	m_pDev    = NULL;

	m_pSlot   = NULL;

	m_pRack   = NULL;

	m_Dev     = NOTHING;

	m_Slot    = NOTHING;

	m_Rack    = NOTHING;

	m_SlotNum = NOTHING;
}

// UI Creation

CViewWnd * CCommsPortSlot::CreateView(UINT uType)
{
	if( uType == viewItem ) {

		if( m_pDev ) {

			return New CCommsSlotRackWnd;
		}
	}

	return NULL;
}

// Attributes

CCommsDeviceRack * CCommsPortSlot::GetDevice(void) const
{
	return m_pDev;
}

COptionCardItem * CCommsPortSlot::GetOption(void) const
{
	return m_pSlot;
}

COptionCardRackItem * CCommsPortSlot::GetRack(void) const
{
	return m_pRack;
}

UINT CCommsPortSlot::GetDrop(void) const
{
	return m_pSlot ? m_pSlot->m_Slot : NOTHING;
}

UINT CCommsPortSlot::GetType(void) const
{
	return m_pSlot ? m_pSlot->m_Type : typeRack;
}

UINT CCommsPortSlot::GetJsonType(void) const
{
	switch( m_pDev->m_pModule->m_Ident ) {

		case LOBYTE(ID_GMPID1):		return 204;
		case LOBYTE(ID_GMPID2):		return 205;
		case LOBYTE(ID_GMUIN4):		return 208;
		case LOBYTE(ID_GMOUT4):		return 203;
		case LOBYTE(ID_GMDIO14):	return 200;
		case LOBYTE(ID_GMTC8):		return 207;
		case LOBYTE(ID_GMINI8):		return 201;
		case LOBYTE(ID_GMINV8):		return 202;
		case LOBYTE(ID_GMRTD6):		return 206;
		case LOBYTE(ID_GMSG1):		return 209;
		case LOBYTE(ID_DADIDO):		return 210;
		case LOBYTE(ID_DADIRO):		return 210;
		case LOBYTE(ID_DAUIN6):		return 211;
		case LOBYTE(ID_DAAO8):		return 212;
		case LOBYTE(ID_DAPID1_RA):	return 213;
		case LOBYTE(ID_DAPID1_SA):	return 213;
		case LOBYTE(ID_DAPID2_SM):	return 214;
		case LOBYTE(ID_DAPID2_RO):	return 214;
		case LOBYTE(ID_DAPID2_SO):	return 214;
	}

	return 0;
}

UINT CCommsPortSlot::GetPower(void) const
{
	return m_pSlot ? m_pSlot->GetPower() : 0;
}

UINT CCommsPortSlot::GetSlot(void) const
{
	return m_pSlot ? m_pSlot->m_Number : NOTHING;
}

// Operations

void CCommsPortSlot::Upgrade(void)
{
	m_SlotNum = m_pSlot ? m_pSlot->m_Number : NOTHING;
}

void CCommsPortSlot::SetDevice(CCommsDeviceRack *pDev)
{
	m_pDev = pDev;

	if( pDev ) {

		m_pSlot->SetType(typeModule);

		UpdateSlot();

		UpdateModule();

		m_Dev = pDev->m_Number;
	}
	else {
		m_pSlot->SetType(typeNone);

		m_Dev = NOTHING;
	}

	SetDirty();
}

void CCommsPortSlot::SetPort(COptionCardItem *pPort)
{
	m_pSlot = pPort;

	if( pPort ) {

		m_SlotNum = pPort->m_Number;

		m_Slot    = pPort->m_Slot;

		FindRackItem();

		FindName();

		SetDirty();
	}
}

void CCommsPortSlot::SetRack(COptionCardRackItem *pRack)
{
	m_pRack = pRack;

	if( m_pRack ) {

		m_Rack = pRack->m_Number;

		FindRackPath();

		FindRackName();

		FindName();

		SetDirty();
	}
}

// Naming

void CCommsPortSlot::UpdateName(void)
{
	if( m_pDev ) {

		m_pDev->SetName(m_Name);
	}

	if( m_pSlot ) {

		m_pSlot->SetModule(m_Name);
	}
}

UINT CCommsPortSlot::GetTreeImage(void) const
{
	return m_pSlot ? m_pSlot->GetTreeImage() : 0x45;
}

CString CCommsPortSlot::GetTreeLabel(void) const
{
	return m_pSlot ? m_pSlot->GetTreeLabel() : m_Name;
}

CString CCommsPortSlot::GetHumanName(void) const
{
	CString Name = m_RackPath;

	if( m_pSlot ) {

		if( !Name.IsEmpty() ) {

			Name += L" - ";
		}

		Name += m_pSlot->GetHumanName();

		if( m_pDev ) {

			Name += L" - ";

			Name += m_pDev->GetHumanName();
		}
	}

	return Name;
}

CString CCommsPortSlot::GetFixedName(void) const
{
	if( m_pSlot ) {

		return CPrintf(L"%4.4X%8.8X", m_pSlot->m_Number, m_pSlot->m_Slot);
	}

	return CMetaItem::GetFixedName();
}

// Persistance

void CCommsPortSlot::Load(CTreeFile &Tree)
{
	CMetaItem::Load(Tree);
}

void CCommsPortSlot::PostLoad(void)
{
	CMetaItem::PostLoad();

	FindSlot();

	FindRack();

	FindDevice();

	UpdateSlot();

	FindName();
}

void CCommsPortSlot::Save(CTreeFile &Tree)
{
	CMetaItem::Save(Tree);
}

// Conversion

void CCommsPortSlot::PostConvert(void)
{
	FindSlot();

	if( m_pSlot ) {

		if( GetType() == typeModule ) {

			if( m_pDev ) {

				UpdateModule();

				UpdateSlot();
			}
			else {
				m_pSlot->SetType(typeNone);

				m_Name = GetHumanName();

				m_Dev  = NOTHING;
			}
		}
	}
}

// Download Support

void CCommsPortSlot::PrepareData(void)
{
	CMetaItem::PrepareData();

	UpdateModule();
}

// Property Filters

BOOL CCommsPortSlot::SaveProp(CString const &Tag) const
{
	if( Tag == L"Name" ) {

		return FALSE;
	}

	return CMetaItem::SaveProp(Tag);
}

// Meta Data Creation

void CCommsPortSlot::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddString(Name);
	Meta_AddInteger(Dev);
	Meta_AddInteger(Slot);
	Meta_AddInteger(Rack);
	Meta_AddInteger(SlotNum);
}

// Implementation

void CCommsPortSlot::FindDevice(void)
{
	CCommsPortRack *pPort = (CCommsPortRack *) GetParent(2);

	m_pDev = (CCommsDeviceRack *) pPort->FindDevice(m_Dev);
}

void CCommsPortSlot::FindSlot(void)
{
	CExpansionItem *pRack = ((CCommsManager *) GetParent(5))->m_pExpansion;

	if( m_SlotNum != NOTHING ) {

		m_pSlot = (COptionCardItem  *) pRack->FindSlot(m_SlotNum);

		m_Slot  = m_pSlot ? m_pSlot->m_Slot : m_Slot;	//

		return;
	}

	if( m_Slot != NOTHING ) {

		m_pSlot   = (COptionCardItem  *) pRack->FindSlot((CUsbTreePath &) m_Slot);

		m_SlotNum = m_pSlot ? m_pSlot->m_Number : NOTHING;

		return;
	}
}

void CCommsPortSlot::FindRack(void)
{
	CExpansionItem *pRack = ((CCommsManager *) GetParent(5))->m_pExpansion;

	m_pRack = pRack->FindRack(m_Rack);

	if( m_pRack ) {

		FindRackPath();

		FindRackName();
	}
}

void CCommsPortSlot::FindName(void)
{
	m_Name = m_RackName;

	if( m_pSlot ) {

		if( !m_Name.IsEmpty() ) {

			m_Name += ".";
		}

		m_Name += m_pSlot->GetHumanName();

		if( m_pDev ) {

			m_Name += L" - ";

			m_Name += m_pDev->GetName();
		}
	}
}

void CCommsPortSlot::FindRackItem(void)
{
	m_pRack = NULL;

	if( m_pSlot ) {

		CItem *pItem = m_pSlot->GetParent();

		while( pItem ) {

			if( pItem->IsKindOf(AfxNamedClass(L"COptionCardRackItem")) ) {

				SetRack((COptionCardRackItem *) pItem);

				break;
			}

			pItem = pItem->GetParent();
		}
	}
}

void CCommsPortSlot::FindRackName(void)
{
	m_RackName.Empty();

	for( CItem *pItem = m_pRack; pItem; pItem = pItem->GetParent() ) {

		if( pItem->IsKindOf(AfxNamedClass(L"COptionCardRackItem")) ) {

			COptionCardRackItem *pRack = (COptionCardRackItem *) pItem;

			if( !m_RackName.IsEmpty() ) {

				m_RackName.Insert(0, L".");

				m_RackName.Insert(0, pRack->GetHumanName());
			}
			else {
				m_RackName = pRack->GetHumanName();
			}
		}
	}
}

void CCommsPortSlot::FindRackPath(void)
{
	m_RackPath.Empty();

	for( CItem *pItem = m_pRack; pItem; pItem = pItem->GetParent() ) {

		if( pItem->IsKindOf(AfxNamedClass(L"COptionCardRackItem")) ) {

			COptionCardRackItem *pRack = (COptionCardRackItem *) pItem;

			if( !m_RackPath.IsEmpty() ) {

				m_RackPath.Insert(0, L" - ");

				m_RackPath.Insert(0, pRack->GetHumanName());
			}
			else {
				m_RackPath = pRack->GetHumanName();
			}
		}
	}
}

void CCommsPortSlot::UpdateSlot(void)
{
	if( m_pSlot && m_pDev ) {

		m_pSlot->SetPower(m_pDev->m_pModule->m_Power);

		m_pSlot->SetModule(m_pDev->m_Name);
	}
}

void CCommsPortSlot::UpdateModule(void)
{
	if( m_pDev && m_pSlot ) {

		m_pDev->m_pModule->m_Drop = m_pSlot->m_Slot;

		m_pDev->m_pModule->m_Slot = m_uPos;
	}
}

// End of File
