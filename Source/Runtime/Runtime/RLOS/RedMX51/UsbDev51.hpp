
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_iMX51_UsbDev_HPP
	
#define	INCLUDE_iMX51_UsbDev_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbBase51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 USB Device Controller
//

class CUsbDev51 : public CUsbBase51, public IUsbFuncHardwareDriver
{
	public:
		// Constructor
		CUsbDev51(UINT iIndex);

		// Destructor
		~CUsbDev51(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbDriver
		BOOL METHOD Bind(IUsbEvents *pEvents);
		BOOL METHOD Bind(IUsbDriver *pDriver);
		BOOL METHOD Init(void);
		BOOL METHOD Start(void);
		BOOL METHOD Stop(void);

		// IUsbFuncHardwareDriver
		BOOL METHOD GetHiSpeedCapable(void);
		BOOL METHOD GetHiSpeedActual(void);
		BOOL METHOD SetConfig(UINT uConfig);
		BOOL METHOD SetAddress(UINT uAddr);
		UINT METHOD GetAddress(void);
		BOOL METHOD InitBulk(UINT iEndpt, BOOL fIn, UINT uMax);
		BOOL METHOD KillBulk(UINT iEndpt);
		BOOL METHOD Transfer(UsbIor &Urb);
		BOOL METHOD Abort(UsbIor &Urb);
		BOOL METHOD SetStall(UINT iEndpt, BOOL fSet);
		BOOL METHOD GetStall(UINT iEndpt);
		BOOL METHOD ResetDataToggle(UINT iEndpt);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// Structures
		struct CTransferDesc;
		struct CQueueHead;
		struct CTransfer;

		// Transfer Descriptor
		struct CTransferDesc
		{
			DWORD		m_NextTDPtr;
			DWORD		m_dwStatus0      :  1;
			DWORD		m_dwStatus1      :  1;
			DWORD		m_dwStatus2      :  1;
			DWORD volatile	m_dwTransErr     :  1;
			DWORD		m_dwStatus4      :  1;
			DWORD volatile	m_dwDataBufErr   :  1;
			DWORD volatile	m_dwHalted       :  1;
			DWORD volatile	m_dwActive       :  1;
			DWORD		m_dwReserved1    :  2;
			DWORD		m_dwMultO        :  2;
			DWORD		m_dwReserved2    :  3;
			DWORD		m_dwIoc	         :  1;
			DWORD volatile	m_dwTotalBytes   : 15;
			DWORD		m_dwReserved3    :  1;
			DWORD		m_BufPtr[5];
			DWORD		m_dwSpare;
			};

		// Queue Head
		struct ALIGN(64) CQueueHead
		{
			DWORD		m_dwReserved1	 : 15;
			DWORD		m_dwIos		 :  1;
			DWORD		m_dwMaxPackLen	 : 11;
			DWORD		m_dwReserved2	 :  2;
			DWORD		m_dwZlt		 :  1;
			DWORD		m_dwMult	 :  2;
			DWORD		m_CurrentTDPtr;
			CTransferDesc	m_Transfer;
			BYTE volatile	m_bSetup[8];
			CTransfer     * m_pTransferHead;
			CTransfer     * m_pTransferTail;
			};

		// Transfer
		struct CTransfer
		{
			BOOL volatile   m_fUsed;
			UINT            m_iEndpt;
			BOOL            m_fDirIn;
			CTransferDesc * m_pTd;
			PBYTE           m_pBuff;
			DWORD           m_dwTdPhy;
			UsbIor        * m_pIo;
			};

		// Constants
		enum
		{
			TD_PTR_END	= 1,
			};

		// Data Members
		ISemaphore	       * m_pFlag;
		IMutex		       * m_pLock;
		CQueueHead             * m_pQueueHead; 
		BYTE                     m_bSetup[8];
		CTransferDesc          * m_pTdList;
		CTransfer              * m_pTransfer; 
		PBYTE                    m_pTdBuff;
		UINT                     m_uTdSize;
		BYTE                     m_bAddr;
		BOOL                     m_fHiSpeed;
		DWORD		         m_dwIntMask;
		IUsbFuncHardwareEvents * m_pDriver;
		PDWORD 			 m_pSentinel;

		// Events
		void OnReset(void);
		void OnSuspend(void);
		void OnTransfer(void);
		void OnPortChange(void);
		void OnSysError(void);

		// Transfers
		BOOL QueueTransfer(CTransfer &Transfer);
		BOOL PrimeEndpoint(UINT iEndpt, BOOL fIn);
		BOOL FlushEndpoint(UINT iEndpt, BOOL fIn);
		BOOL IsEndpointPrimed(UINT iEndpt, BOOL fIn);
								
		// Hardware
		void InitInterrupts(void);
		void EnableInterrupts(BOOL fEnable);
		void ConfigurePort(void);
		void SetPhySuspend(bool fSet);
		void StopEndpoints(void);
				
		// Queue Helpers
		void InitQueueHeads(bool fFull);
		void MakeQueueHeads(void);
		void KillQueueHeads(void);
		UINT GetQueueHead(UINT i, BOOL fIn) const;

		// Transfers Descriptors
		void InitTransfer(void);
		void MakeTransfer(UINT uCount);
		void KillTransfer(void);
		UINT AllocTransfer(UINT iEdnpt);
		void FreeTransfer(UINT iIndex);
		void FreeAllTransfer(void);
		void FreeAllTransfer(UINT iEndpt);
		void TermAllTransfer(void);
		
		// Debug
		void Dump(void) const;
		void DumpQueueHead(void) const;
		void DumpQueueHead(UINT i) const;
		void DumpTransfer(void) const;
		void DumpTransfer(UINT i) const;
	};

// End of File

#endif
