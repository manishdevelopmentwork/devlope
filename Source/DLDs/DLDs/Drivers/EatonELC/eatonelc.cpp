
#include "intern.hpp"

#include "eatonelc.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Eaton ELC Serial Driver
//

// Instantiator

INSTANTIATE(CEatonELCSerialDriver);

// Constructor

CEatonELCSerialDriver::CEatonELCSerialDriver(void)
{
	m_Ident    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;

	m_uTimeout = 600;
	}

// Destructor

CEatonELCSerialDriver::~CEatonELCSerialDriver(void)
{
	FreeBuffers();
	}

// Configuration

void MCALL CEatonELCSerialDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fAscii   = GetByte(pData);
		}

	AllocBuffers();
	}

void MCALL CEatonELCSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if( !m_fAscii && Config.m_uBaudRate <= 19200 ) {

		Config.m_uFlags |= flagFastRx;
		}

	Make485(Config, TRUE);
	}

// Management

void MCALL CEatonELCSerialDriver::Attach(IPortObject *pPort)
{
	if( m_fAscii ) {

		m_pData = MakeDoubleDataHandler();

		pPort->Bind(m_pData);
		}
	else {
		m_pData = MakeSingleDataHandler();

		pPort->Bind(m_pData);
		}
	}

void MCALL CEatonELCSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CEatonELCSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			// NOTE -- If you add extra fields here, you should probably
			// review dupmod and yaskmp as well to make sure they are
			// correctly initialized in the overriden implementations!!!

			m_pCtx->m_bDrop      = GetByte(pData);
			m_pCtx->m_fNoReadEx  = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEatonELCSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEatonELCSerialDriver::Ping(void)
{
	if( m_pCtx->m_bDrop ) {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = SP_D;

		Addr.a.m_Offset = 0;

		Addr.a.m_Type   = addrWordAsWord;

		Addr.a.m_Extra  = 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEatonELCSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

//**/	AfxTrace3("\r\nREAD T=%d O=%x Y=%x ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type);

	CAddress A;

	A.m_Ref = ConstructAddr(Addr);

//**/	AfxTrace2("\r\nREAD A O=%x C=%d ", A.a.m_Offset, uCount);

	switch( A.a.m_Type ) {

		case addrWordAsWord:

			return DoWordRead(A, pData, min(uCount, 16));

		case addrLongAsLong:
		case addrRealAsReal:

			return DoLongRead(A, pData, min(uCount, 8));

		case addrBitAsBit:

			return DoBitRead (A, pData, min(uCount, 16));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEatonELCSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress A;

	A.m_Ref = ConstructAddr(Addr);

	switch( A.a.m_Type ) {

		case addrWordAsWord:

			return DoWordWrite(A, pData, min(uCount, 16));

		case addrLongAsLong:
		case addrRealAsReal:

			return DoLongWrite(A, pData, min(uCount, 8));

		case addrBitAsBit:

			return DoBitWrite(A, pData, min(uCount, 16));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

void CEatonELCSerialDriver::AllocBuffers(void)
{
	m_uTxSize = 256;

	m_uRxSize = 256;

	m_pTx = new BYTE [ m_uTxSize ];

	m_pRx = new BYTE [ m_uRxSize ];
	}

void CEatonELCSerialDriver::FreeBuffers(void)
{
	if( m_pTx ) {

		delete [] m_pTx;

		m_pTx = NULL;
		}

	if( m_pRx ) {

		delete [] m_pRx;

		m_pRx = NULL;
		}
	}

BOOL CEatonELCSerialDriver::IsHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return TRUE;
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return TRUE;
		}

	return FALSE;
	}

WORD CEatonELCSerialDriver::FromHex(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) {

		return bData - '0';
		}

	if( bData >= 'A' && bData <= 'F' ) {

		return bData - 'A' + 10;
		}

	return 0;
	}

BOOL CEatonELCSerialDriver::IgnoreException(void)
{
	if( m_pRx[1] & 0x80 ) {

		if( m_pCtx->m_fNoReadEx ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Port Access

void CEatonELCSerialDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CEatonELCSerialDriver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Frame Building

void CEatonELCSerialDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);

	AddByte(bOpcode);
	}

void CEatonELCSerialDriver::AddByte(BYTE bData)
{
	if( m_uPtr < m_uTxSize ) {

		m_pTx[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CEatonELCSerialDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CEatonELCSerialDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CEatonELCSerialDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {

		if( !m_pCtx->m_bDrop ) {

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}

		if( GetFrame() ) {

			if( m_pRx[0] == m_pTx[0] ) {

				if( fIgnore ) {

					return TRUE;
					}

				if( m_pRx[1] & 0x80 ) {

					return FALSE;
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CEatonELCSerialDriver::PutFrame(void)
{
	m_pData->ClearRx();

	return m_fAscii ? AsciiTx() : BinaryTx();
	}

BOOL CEatonELCSerialDriver::GetFrame(void)
{
	return m_fAscii ? AsciiRx() : BinaryRx();
	}

BOOL CEatonELCSerialDriver::BinaryTx(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {

		BYTE bData = m_pTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_pTx[k]);

	m_pData->Write(m_pTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CEatonELCSerialDriver::BinaryRx(void)
{
	UINT uByte = 0;

	UINT uSize = NOTHING;

	UINT uPtr  = 0;

	UINT uGap  = 0;

	UINT uEnd  = ToTicks(25);

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\nR ");

	while( GetTimer() ) {

		if( (uByte = RxByte(5)) < NOTHING ) {

			m_pRx[uPtr++] = uByte;

//**/			AfxTrace1("<%2.2x>", uByte);

			if( uPtr == m_uRxSize ) {

				return FALSE;
				}

			if( uPtr == 4 ) {

				uSize = FindReplySize();
				}

			uGap = 0;
			}
		else
			uGap = uGap + 1;

		if( uPtr >= uSize || uGap >= uEnd ) {

			if( uPtr >= 4 ) {

				m_CRC.Preset();

				PBYTE p = m_pRx;

				UINT  n = uPtr - 2;

				for( UINT i = 0; i < n; i++ ) {

					m_CRC.Add(*(p++));
					}

				WORD c1 = IntelToHost(PU2(p)[0]);

				WORD c2 = m_CRC.GetValue();

				if( c1 == c2 ) {

					return TRUE;
					}
				}

			uSize = NOTHING;

			uPtr  = 0;

			uGap  = 0;
			}
		}

	return FALSE;
	}

BOOL CEatonELCSerialDriver::AsciiTx(void)
{
	BYTE bCheck = 0;

	TxByte(':');

//**/	AfxTrace0("\r\n:");

	for( UINT i = 0; i < m_uPtr; i++ ) {

		TxByte(m_pHex[m_pTx[i] / 16]);

		TxByte(m_pHex[m_pTx[i] % 16]);

		bCheck += m_pTx[i];

//**/		AfxTrace2("[%2.2x][%2.2x]", m_pHex[m_pTx[i] / 16], m_pHex[m_pTx[i] % 16]);
		}

	bCheck = BYTE(0x100 - WORD(bCheck));

	TxByte(m_pHex[bCheck / 16]);
	TxByte(m_pHex[bCheck % 16]);

	TxByte(CR);
	TxByte(LF);

//**/	AfxTrace2("[%2.2x][%2.2x][0D][0A]", m_pHex[bCheck / 16], m_pHex[bCheck % 16]);

	return TRUE;
	}

BOOL CEatonELCSerialDriver::AsciiRx(void)
{
	UINT uState = 0;

	UINT uPtr   = 0;

	UINT uTimer = 0;

	BYTE bCheck = 0;

	SetTimer(m_uTimeout);

//**/	AfxTrace0("\r\nA ");

	while( (uTimer = GetTimer()) ) {

		UINT uByte;

		if( (uByte = RxByte(uTimer)) == NOTHING ) {

			continue;
			}

//**/		AfxTrace1("<%2.2x>", uByte);

		switch( uState ) {

			case 0:
				if( uByte == ':' ) {

					uPtr   = 0;

					uState = 1;

					bCheck = 0;
					}
				break;

			case 1:
				if( IsHex(uByte) ) {

					m_pRx[uPtr] = FromHex(uByte) << 4;

					uState = 2;
					}
				else {
					if( uByte == CR ) {

						continue;
						}

					if( uByte == LF ) {

						if( bCheck == 0 ) {
							
							UINT uSize = FindReplySize();

							if( uSize < NOTHING ) {

								if( uSize == uPtr + 1 ) {

									return TRUE;
									}

								return FALSE;
								}

							return TRUE;
							}
						}

					return FALSE;
					}
				break;

			case 2:
				if( IsHex(uByte) ) {

					m_pRx[uPtr] |= FromHex(uByte);

					bCheck += m_pRx[uPtr];

					if( ++uPtr == m_uRxSize ) {

						return FALSE;
						}

					uState = 1;

					break;
					}

				return FALSE;
			}
		}

	return FALSE;
	}

// Transport Helpers

UINT CEatonELCSerialDriver::FindReplySize(void)
{
	switch( m_pRx[1] ) {

		case 1:
		case 2:
		case 3:
		case 4:
			return 5 + (m_pRx[2] ? m_pRx[2] : 256);

		case 5:
		case 6:
			return 8;

		case 15:
		case 16:
			return 8;
		}

	return NOTHING;
	}

// Read Handlers

CCODE CEatonELCSerialDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x03);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			WORD x   = PU2(m_pRx + 3)[n];

			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEatonELCSerialDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(0x03);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);

	if( Transact(FALSE) ) {

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}

		for( UINT n = 0; n < uCount; n++ ) {

			WORD x   = PU2(m_pRx + 3)[n];

			WORD y   = PU2(m_pRx + 5)[n];

			DWORD d;

			if( Addr.a.m_Type == addrRealAsReal || Addr.a.m_Table != SP_A ) {

				d = (y << 16) + x;

				pData[n] = MotorToHost(d);
				}

			else {
				d = (x << 16) + y;

				pData[n] = MotorToHost(d);
				}
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEatonELCSerialDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(Addr.a.m_Table == SP_X ? 2 : 1);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_pRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CEatonELCSerialDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {

		StartFrame(6);

		AddWord(Addr.a.m_Offset);

		AddWord(LOWORD(pData[0]));
		}

	else {
		StartFrame(16);

		AddWord(Addr.a.m_Offset);

		AddWord(uCount);

		AddByte(uCount * 2);

		for( UINT n = 0; n < uCount; n++ ) {

			AddWord(LOWORD(pData[n]));
			}
		}

	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEatonELCSerialDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(16);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);

	AddByte(uCount * 4);

	for( UINT n = 0; n < uCount; n++ ) {

		WORD x = HIWORD(pData[n]);
		WORD y = LOWORD(pData[n]);

		if( Addr.a.m_Type == addrRealAsReal || Addr.a.m_Table != SP_A ) {

			AddWord(y);
			AddWord(x);
			}

		else {
			AddLong(pData[n]);
			}
		}

	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CEatonELCSerialDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( Addr.a.m_Table == SP_X ) return uCount;

	if( uCount == 1 ) {

		StartFrame(5);

		AddWord(Addr.a.m_Offset);

		AddWord(pData[0] ? 0xFF00 : 0x0000);
		}
	else {
		StartFrame(15);

		AddWord(Addr.a.m_Offset);

		AddWord(uCount);

		AddByte((uCount + 7) / 8);

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) {

				b |= m;
				}

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;

				m = 1;
				}
			}

		if( m > 1 ) {

			AddByte(b);
			}
		}

	if( Transact(TRUE) ) {

		return uCount;
		}

	return CCODE_ERROR;
	}

// Helpers
DWORD CEatonELCSerialDriver::ConstructAddr(AREF Addr)
{
	CAddress A;

	A.m_Ref		= Addr.m_Ref;

	UINT uOffIn	= Addr.a.m_Offset;

	UINT uOffOut	= 0;

	UINT uBase	= 0;

	switch( A.a.m_Table ) {

		case SP_D:
			if( uOffIn < 4096 ) {

				uOffOut = 0x1000;
				}

			else {
				uBase   = 4096;
				uOffOut = 0x9000;
				}
			break;

		case SP_M:
			if( uOffIn < 1536 ) {

				uOffOut = 0x800;
				}

			else {
				uBase	= 1536;
				uOffOut	= 0xB000;
				}
			break;

		case SP_X:
			uOffOut = 0x400;
			break;

		case SP_Y:
			uOffOut = 0x500;
			break;

		case SP_T:
		case SP_U:
			uOffOut = 0x600;
			break;

		case SP_C:
		case SP_B:
		case SP_A:
			uOffOut = 0xE00;
			break;
		}

	A.a.m_Offset = uOffIn - uBase + uOffOut;

	return A.m_Ref;
	}

// End of File
