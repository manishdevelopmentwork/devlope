
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Colorado CO07eq Model Data
//

static BYTE imageCO07eq[] = {

	#include "co07eq-x2.png.dat"
	0
	};

global CHostModel modelCO07eq = {

	"",
	"CO07EQ",
	"CR1000-07",
	"CR1000-07000",
	rfColorado,
	2,
	944,
	928,
	152,
	200,
	320,
	240,
	0,
	NULL,
	sizeof(imageCO07eq)-1,
	imageCO07eq
	};

// End of File
