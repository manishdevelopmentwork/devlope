
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_TcpProtocol_HPP

#define	INCLUDE_TcpProtocol_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TcpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP Protocol
//

class CTcp : public IThreadNotify, public IProtocol
{
	public:
		// Constructor
		CTcp(void);

		// Destructor
		~CTcp(void);

		// Attributes
		BOOL AcceptIP(IPREF IP) const;

		// Operations
		void Enable(WORD wPort);
		BOOL Send(IPREF Dest, IPREF From, CBuffer *pBuff, BOOL fKeep);
		void SendReq(BOOL fState);
		void CopyOptions(ISocket *pSock, IPREF IP);
		void FreeSocket(CTcpSocket *pSock);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IThreadNotify
		UINT METHOD OnThreadCreate(IThread *pThread, UINT uIndex);
		void METHOD OnThreadDelete(IThread *pThread, UINT uIndex, UINT uParam);

		// IProtocol
		BOOL Bind(IRouter *pRouter, INetUtilities *pUtils, UINT uMask);
		BOOL CreateSocket(ISocket * &pSocket);
		BOOL Recv(UINT uFace, IPREF Face, PSREF PS, CBuffer *pBuff);
		BOOL Send(void);
		BOOL Poll(void);
		BOOL NetStat(IDiagOutput *pOut);
		BOOL SetHeader(IPREF Dest, IPREF From, CBuffer *pBuff);

		// Public Data
		IMutex * m_pLock;

	protected:
		// List Root
		struct CListRoot
		{
			CTcpSocket * m_pHead;
			CTcpSocket * m_pTail;
			};

		// Data Members
		ULONG		m_uRefs;
		IRouter	      * m_pRouter;
		INetUtilities * m_pUtils;
		UINT		m_uMask;
		DWORD		m_Used[2048];
		CTcpSocket	m_Sock[64];
		UINT		m_uSend;
		CIpAddr		m_Ip;

		// Implementation
		void BindSockets(void);
		void RejectSegment(PSREF PS, CTcpHeader *pReq);
		BOOL IsPortUsed(WORD wPort);
		void AddSocket(CTcpSocket *pSock);
	};

// End of File

#endif
