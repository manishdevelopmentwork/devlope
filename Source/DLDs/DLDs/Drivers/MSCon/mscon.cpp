
#include "intern.hpp"

#include "mscon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Microscan Concentrator Driver
//

// Externals

clink DWORD _text_from;

clink DWORD _data_from;

// Instantiator

INSTANTIATE(CMicroscan);

// Constructor

CMicroscan::CMicroscan(void)
{
	m_Ident    = DRIVER_ID;

	m_Flags    = DF_FORCE_SERVICE;

	m_fDebug   = TRUE;

	m_pHead    = NULL;
	
	m_pTail    = NULL;

	m_pTxOpen  = NULL;

	m_pTxClose = NULL;

	m_uRxState = 0;

	m_uRxPtr   = 0;

	m_uRxLast  = 0;

	m_uTxState = 0;

	m_uTxPtr   = 0;

	m_sSep[1]  = 0;

	m_uHostSize = 0;

	m_pHostData = NULL;

	m_uHostHead = 0;

	m_uHostTail = 0;
	}

// Destructor

CMicroscan::~CMicroscan(void)
{
	m_pExtra->Release();

	free(m_pTxOpen);

	free(m_pTxClose);

	free(m_pHostData);
	}

// Configuration

void MCALL CMicroscan::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_fEnable   = GetByte  (pData);
		m_uRawPort  = GetByte  (pData);
		m_bDateFmt  = GetByte  (pData);
		m_bTimeFmt  = GetByte  (pData);
		m_sSep[0]   = GetByte  (pData);
		m_fAppend   = GetByte  (pData);
		m_pTxOpen   = GetString(pData);
		m_pTxClose  = GetString(pData);
		m_fTxDrop   = GetByte  (pData);
		m_bRxTerm   = GetByte  (pData);
		m_uRxTime   = GetWord  (pData);
		m_uTxTime   = GetWord  (pData);
		
		m_uProtocol = GetByte  (pData);
		m_fLRC      = GetByte  (pData);

		m_uRxTime   = ToTicks  (m_uRxTime);
		m_uTxTime   = ToTicks  (m_uTxTime);

		m_fAckNak   = GetByte  (pData);
		m_fReqRes   = GetByte  (pData);
		m_fDuplex   = GetByte  (pData);
		m_uRetries  = GetByte  (pData);

		m_STX	    = GetByte  (pData);
		m_ETX	    = GetByte  (pData);
		m_ACK	    = GetByte  (pData);
		m_NAK	    = GetByte  (pData);
		m_REQ	    = GetByte  (pData);
		m_RES	    = GetByte  (pData);
		}

	MoreHelp(IDH_EXTRA, (void **) &m_pExtra);

	if( m_fEnable ) {

		m_uHostSize = 512 * 1024;

		m_pHostData = PBYTE(malloc(m_uHostSize));
		}

	m_pHost = NULL;
	}
	
void MCALL CMicroscan::CheckConfig(CSerialConfig &Config)
{
	Make422(Config, TRUE);
	}
	
// Management

void MCALL CMicroscan::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CMicroscan::Open(void)
{
	if( m_uProtocol == 1 && m_fReqRes ) {

		m_uRxState = 0;

		m_uTxState = 2;
		}
	else {
		m_uRxState = 0;

		m_uTxState = 0;
		}

	/*Debug("\n\n\n\n\n\n\n\n");*/
	}

// Device

CCODE MCALL CMicroscan::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx              = new CContext;

			m_pCtx->m_bDrop     = GetByte(pData);

			m_pCtx->m_bPoll     = 0x1C + 2 * (m_pCtx->m_bDrop - 1);

			m_pCtx->m_bSelect   = 0x1D + 2 * (m_pCtx->m_bDrop - 1);

			m_pCtx->m_fOnline   = FALSE;

			m_pCtx->m_uCount    = 0;

			m_pCtx->m_uTime     = 0;

			m_pCtx->m_uFract    = 0;

			m_pCtx->m_uCmdCount = 0;
			
			m_pCtx->m_uCmdCode  = 0;

			m_pCtx->m_uCmdSeq   = 0;

			m_pCtx->m_uCmdLast  = NOTHING;

			m_pCtx->m_pCmdList  = NULL;

			memset(m_pCtx->m_sSend, 0, sizeof(m_pCtx->m_sSend));

			memset(m_pCtx->m_sCode, 0, sizeof(m_pCtx->m_sCode));

			memset(m_pCtx->m_sRecv, 0, sizeof(m_pCtx->m_sRecv));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);
						
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CMicroscan::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		m_pDevice->SetContext(NULL);

		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		delete m_pCtx;

		m_pCtx = NULL;
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

void MCALL CMicroscan::Service(void)
{
	ServiceHost();

	ServiceReader();
	}

CCODE MCALL CMicroscan::Ping(void)
{
	return m_pCtx->m_fOnline ? CCODE_SUCCESS : CCODE_ERROR;
	}

CCODE MCALL CMicroscan::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_fOnline ) {

		if( Addr.a.m_Table == addrNamed ) {

			switch( Addr.a.m_Offset ) {

				case 1:
					*pData = m_pCtx->m_uCount;

					return 1;

				case 2:
					*pData = m_pCtx->m_uTime;

					return 1;

				case 3:
					*pData = m_pCtx->m_uFract;

					return 1;

				case 4:
					*pData = m_pCtx->m_uCmdCount;
					
					return 1;

				case 5:
					*pData = m_pCtx->m_uCmdCode;
					
					return 1;

				case 6:
					*pData = m_pCtx->m_uCmdSeq;
					
					return 1;
				}

			*pData = 0;

			return 1;
			}

		if( Addr.a.m_Table == 1 ) {

			UINT uStart = Addr.a.m_Offset - 1;

			UINT uAvail = elements(m_pCtx->m_sCode) - uStart;

			uCount = min(uCount, uAvail);

			for( UINT n = 0; n < uCount; n++ ) {

				*pData++ = m_pCtx->m_sCode[n];
				}

			return n;
			}

		if( Addr.a.m_Table == 2 ) {

			UINT uStart = Addr.a.m_Offset - 1;

			UINT uAvail = elements(m_pCtx->m_sRecv) - uStart;

			uCount = min(uCount, uAvail);

			for( UINT n = 0; n < uCount; n++ ) {

				*pData++ = m_pCtx->m_sRecv[n];
				}

			return n;
			}
		}

	return CCODE_ERROR | CCODE_NO_RETRY;
	}

CCODE MCALL CMicroscan::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( m_pCtx->m_fOnline ) {

		if( Addr.a.m_Table == addrNamed ) {

			switch( Addr.a.m_Offset ) {

				case 5:
					m_pCtx->m_uCmdCode = *pData;
					
					return 1;

				case 6:
					m_pCtx->m_uCmdSeq  = *pData;
					
					return 1;
				}
			}
		}

	return uCount;
	}

// Reader Communications

BOOL CMicroscan::ServiceReader(void)
{
	if( m_pCtx->m_fOnline ) {

		if( ReaderHasAutoCmd() ) {

			if( m_pCtx->m_uCmdLast < NOTHING ) {

				if( !ReaderSend(FALSE) ) {
		
					ReaderNotOnline();

					return FALSE;
					}

				m_pCtx->m_sRecv[0] = 0;
				}

			m_pCtx->m_uCmdLast = m_pCtx->m_uCmdSeq;
			}

		if( ReaderHasHostCmd() ) {

			if( !ReaderSend(TRUE) ) {
		
				ReaderNotOnline();

				return FALSE;
				}

			StripReaderHostCmd();
			}
		}

	if( ReaderPollSequence() ) {

		ExtractReaderData();

		ExtractReaderCode();

		m_pCtx->m_fOnline = TRUE;

		HostSend();

		return TRUE;
		}

	ReaderNotOnline();

	return FALSE;
	}

BOOL CMicroscan::ReaderPollSequence(void)
{
	UINT uTries = m_pCtx->m_fOnline ? 3 : 1;

	for( UINT n = 0; n < uTries; n++ ) {

		m_pData->ClearRx();

		if( ReaderPoll() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CMicroscan::ReaderPoll(void)
{
	ReaderSendPoll();

	m_sData[0]  = 0;

	UINT uState = 0;

	UINT uCount = 0;

	BYTE bCheck = 0;

	SetTimer(50);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			/*Debug("<TIMEOUT>");*/

			break;
			}

		/*Debug("<%2.2X>", uData);*/

		switch( uState ) {

			case 0:
				if( uData == EOT ) {

					return TRUE;
					}

				if( uData == ENQ ) {

					ReaderSendAck();

					return TRUE;
					}		  

				if( uData == m_pCtx->m_bPoll ) {

					uState = 1;
					}
				break;

			case 1:
				if( uData == EOT ) {

					return TRUE;
					}

				if( uData == STX ) {

					uState = 2;

					bCheck = 0;

					uCount = 0;

					break;
					}

				return FALSE;

			case 2:
				if( uData == ETX ) {

					bCheck ^= uData;

					m_sData[uCount++] = 0;

					uState = 3;
					}
				else {
					bCheck ^= uData;

					if( isprint(uData) ) {

						if( uCount < elements(m_sData) - 1 ) {

							m_sData[uCount++] = uData;

							SetTimer(25);

							break;
							}

						return FALSE;
						}

					SetTimer(25);
					}
				break;

			case 3:
				if( (bCheck ^= uData) ) {

					ReaderSendNak();

					SetTimer(25);

					uState = 1;
					}
				else {
					ReaderSendAck();

					SetTimer(25);

					uState = 4;
					}

				break;

			case 4:
				if( uData == ENQ ) {

					continue;
					}

				if( uData == EOT ) {

					return TRUE;
					}
				
				return FALSE;
			}
		}

	return FALSE;
	}

BOOL CMicroscan::ReaderSend(BOOL fHost)
{
	/*Debug("**SEND**");*/

	for( UINT i = 0; i < 2; i++ ) {

		ReaderSendSelect();

		if( ReaderGetSelectAck() ) {

			for( UINT j = 0; j < 3; j++ ) {

				if( fHost )
					SendReaderHostCmd();
				else
					SendReaderAutoCmd();

				if( ReaderGetSelectAck() ) {

					ReaderSendRes();

					/*Debug("**OKAY**");*/

					return TRUE;
					}
				}

			ReaderSendRes();
			}
		}

	/*Debug("**FAIL**");*/

	return FALSE;
	}

BOOL CMicroscan::ReaderGetSelectAck(void)
{
	UINT uState = 0;

	SetTimer(50);

	for(;;) {

		UINT uData = m_pData->Read(GetTimer());

		if( uData == NOTHING ) {

			break;
			}

		/*Debug("<%2.2X>", uData);*/

		switch( uState ) {

			case 0:
				if( uData == m_pCtx->m_bSelect ) {

					uState = 1;
					}
				break;

			case 1:
				if( uData == ACK ) {

					return TRUE;
					}

				if( uData == NAK ) {

					return FALSE;
					}

				return FALSE;
			}
		}

	return FALSE;
	}

void CMicroscan::ReaderNotOnline(void)
{
	m_pCtx->m_fOnline = FALSE;

	EmptyReaderHostCmd();

	HostSend();
	}

// Reader Commands

BOOL CMicroscan::ReaderHasAutoCmd(void)
{
	return !(m_pCtx->m_uCmdLast == m_pCtx->m_uCmdSeq);
	}

BOOL CMicroscan::ReaderHasHostCmd(void)
{
	return m_pCtx->m_sSend[0] ? TRUE : FALSE;
	}

BOOL CMicroscan::SendReaderHostCmd(void)
{
	PTXT pFind  = strchr(m_pCtx->m_sSend, '>');

	UINT uCount = pFind - m_pCtx->m_sSend + 1;

	SendReaderCmd(m_pCtx->m_sSend, uCount);

	return TRUE;
	}

BOOL CMicroscan::SendReaderAutoCmd(void)
{
	PCTXT pSend = NULL;
	
	PCTXT pList = NULL;

	switch( m_pCtx->m_uCmdCode ) {

		case 1:
			pSend = "<T><V><X><N>";
			
			pList = "TVXN";
			
			break;

		case 2:
			pSend = "< >";

			pList = NULL;

			break;
		}

	if( pSend ) {

		UINT uCount = strlen(pSend);

		SendReaderCmd(pSend, uCount);

		m_pCtx->m_pCmdList  = pList;

		m_pCtx->m_uCmdIndex = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CMicroscan::SendReaderCmd(PCSTR pSend, UINT uCount)
{
	BYTE  bCheck = ETX;

	PBYTE pData  = PBYTE(alloca(uCount + 3));

	for( UINT n = 0; n < uCount; n++ ) {

		bCheck ^= pSend[n];
		}

	pData[0] = STX;

	memcpy(pData + 1, pSend, uCount);

	pData[1 + uCount] = ETX;

	pData[2 + uCount] = bCheck;

	ReaderWrite(pData, uCount + 3);

	return TRUE;
	}

BOOL CMicroscan::QueueReaderHostCmd(UINT uDrop, PCTXT pSend)
{
	CContext *pScan = m_pHead;

	while( pScan ) {

		if( pScan->m_bDrop == uDrop ) {

			UINT uLeft = sizeof(pScan->m_sSend) - strlen(pScan->m_sSend);

			UINT uSize = strlen(pSend);

			if( uSize < uLeft - 1 ) {

				strcat(pScan->m_sSend, pSend);

				return TRUE;
				}

			break;
			}

		pScan = pScan->m_pNext;
		}

	return FALSE;
	}

void CMicroscan::StripReaderHostCmd(void)
{
	PTXT pFind = strchr(m_pCtx->m_sSend, '>') + 1;

	UINT uSize = strlen(pFind);

	memmove(m_pCtx->m_sSend, pFind, uSize + 1);
	}

void CMicroscan::EmptyReaderHostCmd(void)
{
	m_pCtx->m_sSend[0] = 0;

	m_pCtx->m_uCmdLast = m_pCtx->m_uCmdSeq;
	}

// Reader Frame Parsing

void CMicroscan::ExtractReaderData(void)
{
	while( m_sData[0] == '<' ) {

		PTXT pFind = strchr(m_sData, '>');

		if( pFind++ ) {

			if( m_sData[2] == '/' ) {
				
				if( m_pCtx->m_pCmdList ) {

					PCTXT pList = m_pCtx->m_pCmdList;

					char  cNext = pList[m_pCtx->m_uCmdIndex];

					if( m_sData[1] == cNext ) {

						strncat(m_pCtx->m_sRecv, m_sData, pFind - m_sData);

						cNext = pList[++m_pCtx->m_uCmdIndex];

						if( !cNext ) {

							m_pCtx->m_uCmdCount += 1;

							m_pCtx->m_pCmdList   = NULL;
							}

						memmove(m_sData, pFind, strlen(pFind) + 1);

						continue;
						}
					}
				}

			RelayDataToHost(pFind - m_sData);

			memmove(m_sData, pFind, strlen(pFind) + 1);

			continue;
			}

		m_sData[0] = 0;

		break;
		}
	}

void CMicroscan::ExtractReaderCode(void)
{
	if( m_sData[0] ) {

		strcpy(m_pCtx->m_sCode, m_sData);

		m_pCtx->m_uTime    = /*m_pExtra->*/GetTimeStamp(/*m_pCtx->m_uFract*/);

		m_pCtx->m_uFract >>= 16;

		m_pCtx->m_uFract  *= 100;

		m_pCtx->m_uFract  /= 0x10000;

		m_pCtx->m_uCount   = m_pCtx->m_uCount + 1;

		RelayCodeToHost();
		}
	}

// Reader Comms Primitives

void CMicroscan::ReaderSendPoll(void)
{
	BYTE bData[] = { EOT, m_pCtx->m_bPoll, ENQ };

	ReaderWrite(bData, sizeof(bData));
	}

void CMicroscan::ReaderSendSelect(void)
{
	BYTE bData[] = { EOT, m_pCtx->m_bSelect, ENQ };

	ReaderWrite(bData, sizeof(bData));
	}

void CMicroscan::ReaderSendAck(void)
{
	BYTE bData[] = { ACK };

	ReaderWrite(bData, sizeof(bData));
	}

void CMicroscan::ReaderSendNak(void)
{
	BYTE bData[] = { NAK };

	ReaderWrite(bData, sizeof(bData));
	}

void CMicroscan::ReaderSendRes(void)
{
	BYTE bData[] = { EOT };

	ReaderWrite(bData, sizeof(bData));
	}

// Reader Port Access

void CMicroscan::ReaderWrite(PCBYTE pData, UINT uCount)
{
/*	if( m_fDebug ) {

		for( UINT n = 0; n < uCount; n++ ) {

			Debug("[%2.2X]", pData[n]);
			}
		}

*/	m_pData->Write(pData, uCount, FOREVER);
	}

// Host Communications

void CMicroscan::ServiceHost(void)
{
	HostSend();

	HostRead();

	HostSend();
	}

void CMicroscan::HostSend(void)
{
	if( FindHostPort() ) {

		while( m_uHostTail != m_uHostHead ) {

			UINT uBuff  = m_uHostSize;

			UINT uSize  = (uBuff + m_uHostTail - m_uHostHead) % uBuff;

			UINT uLeft  = uBuff - m_uHostHead;

			UINT uSend  = min(uLeft, uSize);

			UINT uSent  = HostSend(m_pHostData + m_uHostHead, uSend);

			m_uHostHead = (m_uHostHead + uSent) % uBuff;

			if( uSent < uSend ) break;
			}
		}
	}

UINT CMicroscan::HostSend(PCBYTE pSend, UINT uSend)
{
	if( m_uProtocol == 0 ) {

		return HostSendPointToPoint(pSend, uSend);
		}

	if( m_uProtocol == 1 ) {

		return HostSendAckNak(pSend, uSend);
		}

	return uSend;
	}

UINT CMicroscan::HostSendPointToPoint(PCBYTE pSend, UINT uSend)
{
	return m_pHost->Write(pSend, uSend, 0);
	}

UINT CMicroscan::HostSendAckNak(PCBYTE pSend, UINT uSend)
{
	if( m_uTxState == 0 ) {
		
		if( m_fDuplex || m_uRxState == 0 ) {

			if( m_uTxPtr == 0 ) {

				if( m_STX ) {

					AddAckNakByte(m_STX);
					}
				}

			for( UINT uScan = 0; uScan < uSend; uScan++ ) {

				if( pSend[uScan] == 0xFF ) {

					break;
					}
				}

			if( uScan < uSend ) {

				AddAckNakData(pSend, uScan);

				AddAckNakByte(m_ETX);

				if( m_fLRC ) {

					UINT uFrom = m_STX ? 1 : 0;

					BYTE bLRC  = 0x00;

					for( UINT n = uFrom; n < m_uTxPtr; n++ ) {

						bLRC ^= m_bTemp[n];
						}

					AddAckNakByte(bLRC);
					}

				m_pHost->Write(m_bTemp, m_uTxPtr, 0);

				if( !m_fAckNak ) {

					AckNakResetTx();
					}
				else {
					m_uTxState = 1;

					m_uTxLast  = GetTickCount();

					m_uTxLeft  = m_uRetries;
					}

				return uScan + 1;
				}

			AddAckNakData(pSend, uScan);

			return uScan;
			}
		}

	return 0;
	}

void CMicroscan::HostRead(void)
{
	if( FindHostPort() ) {

		for( UINT uLimit = 0; uLimit < 200; uLimit++ ) {

			if( !HostRead(m_pHost->Read(0)) ) {

				break;
				}
			}
		}
	}

BOOL CMicroscan::HostRead(UINT uData)
{
	if( m_uProtocol == 0 ) {

		return HostReadPointToPoint(uData);
		}

	if( m_uProtocol == 1 ) {

		return HostReadAckNak(uData);
		}

	if( uData == NOTHING ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CMicroscan::HostReadPointToPoint(UINT uData)
{
	if( uData == NOTHING ) {

		if( m_uRxTime && m_uRxLast ) {
			
			if( GetTickCount() - m_uRxLast >= m_uRxTime ) {

				if( !m_bRxTerm ) {
					
					if( m_uRxPtr < elements(m_sRecv) - 1 ) {

						if( m_fLRC ) {

							m_uRxPtr -= 1;
							}

						m_sRecv[m_uRxPtr] = 0;

						ParseHostLine();
						}
					}

				m_uRxState = 0;

				m_uRxPtr   = 0;

				m_uRxLast  = 0;
				}
			}

		return FALSE;
		}

	if( m_uRxState == 0) {

		if( m_bRxTerm && uData == m_bRxTerm ) {

			if( m_fLRC ) {

				m_uRxState = 1;

				return TRUE;
				}

			if( m_uRxPtr ) {
				
				if( m_uRxPtr < elements(m_sRecv) - 1 ) {

					m_sRecv[m_uRxPtr] = 0;

					ParseHostLine();
					}

				m_uRxPtr  = 0;

				m_uRxLast = 0;
				}

			return FALSE;
			}

		if( uData >= 32 && uData <= 126 ) {

			if( m_uRxPtr < elements(m_sRecv) - 1 ) {

				m_sRecv[m_uRxPtr++] = uData;
				}

			m_uRxLast = GetTickCount();
			}

		return TRUE;
		}

	if( m_uRxState == 1 ) {

		if( m_uRxPtr ) {

			if( m_uRxPtr < elements(m_sRecv) - 1 ) {

				m_sRecv[m_uRxPtr] = 0;

				ParseHostLine();
				}

			m_uRxPtr = 0;
			}

		m_uRxState = 0;

		m_uRxLast  = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CMicroscan::HostReadAckNak(UINT uData)
{
	if( uData == NOTHING ) {

		if( m_uTxState == 1 && m_uTxTime && m_uTxLast ) {
			
			if( GetTickCount() - m_uTxLast >= m_uTxTime ) {

				if( m_uTxLeft == 255 || m_uTxLeft-- ) {

					m_pHost->Write(m_bTemp, m_uTxPtr, 0);

					m_uTxLast  = GetTickCount();
					}
				else {
					AckNakSendRes();

					AckNakResetTx();
					}
				}
			}

		if( m_uRxState >= 1 && m_uRxTime && m_uRxLast ) {
			
			if( GetTickCount() - m_uRxLast >= m_uRxTime ) {

				m_uRxState = 0;

				m_uRxLast  = 0;
				}
			}

		return FALSE;
		}

	if( !m_fLRC || m_uRxState < 2 ) {

		if( uData == m_ACK ) {

			if( m_uTxState == 1 ) {

				AckNakResetTx();
				}

			return TRUE;
			}

		if( uData == m_NAK ) {

			if( m_uTxState == 1 ) {

				m_pHost->Write(m_bTemp, m_uTxPtr, 0);
				}

			return TRUE;
			}

		if( uData == m_REQ ) {

			if( m_uTxState == 2 ) {

				if( m_uHostTail == m_uHostHead ) {

					AckNakSendRes();

					return TRUE;
					}

				m_uTxState = 0;

				m_uTxPtr   = 0;

				return FALSE;
				}

			AckNakSendRes();

			return TRUE;
			}

		if( uData == m_RES ) {

			AckNakResetTx();

			m_uRxState = 0;

			return TRUE;
			}
		}

	if( m_uRxState == 0 ) {

		if( m_STX == 0 || uData == m_STX ) {

			m_uRxState = 1;

			m_uRxPtr   = 0;

			m_uRxLast  = GetTickCount();
			}

		if( m_STX ) {

			return TRUE;
			}
		}

	if( m_uRxState == 1 ) {

		if( uData == m_ETX ) {

			if( m_fLRC ) {

				m_uRxState = 2;

				m_uRxLast  = GetTickCount();

				return TRUE;
				}

			if( m_uRxPtr ) {

				if( m_uRxPtr < elements(m_sRecv) - 1 ) {

					AckNakSendAck();

					m_sRecv[m_uRxPtr] = 0;

					ParseHostLine();
					}
				else
					AckNakSendNak();
				}
			else
				AckNakSendAck();

			m_uRxState = 0;

			return TRUE;
			}

		if( m_uRxPtr < elements(m_sRecv) - 1 ) {

			m_sRecv[m_uRxPtr++] = uData;
			}

		m_uRxLast = GetTickCount();

		return TRUE;
		}

	if( m_uRxState == 2 ) {

		if( m_uRxPtr ) {

			if( m_uRxPtr < elements(m_sRecv) - 1 ) {

				AckNakSendAck();

				m_sRecv[m_uRxPtr] = 0;

				ParseHostLine();
				}
			else
				AckNakSendNak();
			}
		else
			AckNakSendAck();

		m_uRxState = 0;

		return TRUE;
		}

	return TRUE;
	}

void CMicroscan::ParseHostLine(void)
{
	if( isdigit(m_sRecv[0]) && isdigit(m_sRecv[1]) ) {

		UINT uDrop = ATOI(m_sRecv);

		PTXT pScan = m_sRecv + 2;

		while( *pScan == '<' ) {

			PTXT pFind = strchr(pScan, '>');

			if( pFind ) {

				char sSend[512];

				UINT uSize = pFind - pScan + 1;

				memcpy(sSend, pScan, uSize);

				sSend[uSize] = 0;

				if( uDrop - 51 ) {

					QueueReaderHostCmd(uDrop, sSend);
					}
				else
					LocalCommand(sSend);

				pScan = pFind + 1;

				continue;
				}

			break;
			}
		}
	}

BOOL CMicroscan::FindHostPort(void)
{
	if( m_fEnable ) {

		if( !m_pHost ) {

			m_pExtra->GetRawPort(m_uRawPort, &m_pHost);
			}

		return m_pHost ? TRUE : FALSE;
		}

	return FALSE;
	}

// Ack-Nak Support

void CMicroscan::AddAckNakByte(BYTE bData)
{
	m_bTemp[m_uTxPtr++] = bData;
	}

void CMicroscan::AddAckNakData(PCBYTE pData, UINT uSize)
{
	memcpy(m_bTemp + m_uTxPtr, pData, uSize);

	m_uTxPtr += uSize;
	}

void CMicroscan::AckNakSendAck(void)
{
	BYTE bData[] = { m_ACK };

	m_pHost->Write(bData, sizeof(bData), 0);
	}

void CMicroscan::AckNakSendNak(void)
{
	BYTE bData[] = { m_NAK };

	m_pHost->Write(bData, sizeof(bData), 0);
	}

void CMicroscan::AckNakSendRes(void)
{
	BYTE bData[] = { m_RES };

	m_pHost->Write(bData, sizeof(bData), 0);
	}

void CMicroscan::AckNakResetTx(void)
{
	if( m_fReqRes ) {

		m_uTxState = 2;
		}
	else {
		m_uTxPtr   = 0;

		m_uTxState = 0;
		}
	}

// Relaying to Host

void CMicroscan::RelayDataToHost(UINT uSize)
{
	if( FindHostPort() ) {

		StartHostFrame(m_pCtx->m_bDrop);

		strncat(m_sSend, m_sData, uSize);

		QueueHostFrame();
		}
	}

void CMicroscan::RelayCodeToHost(void)
{
	if( FindHostPort() ) {

		StartHostFrame(m_pCtx->m_bDrop);

		if( m_fAppend ) {

			HostAppend(m_pCtx->m_sCode);

			AppendMark(m_sSend, FALSE);
			}
		else {
			AppendMark(m_sSend, FALSE);

			HostAppend(m_pCtx->m_sCode);
			}

		QueueHostFrame();
		}
	}

// Local Commands

BOOL CMicroscan::LocalCommand(PCTXT pCmd)
{
	if( pCmd[1] == 'K' ) {

		PTXT p = NULL;

		UINT n = strtol(pCmd+2, &p, 10);

		if( n == 729 ) {

			int x[7];

			for( UINT n = 0; n < elements(x); n++ ) {

				x[n] = strtol(p+1, &p, 10);
				}

			x[6] = x[6] % 100;

			DWORD t = 0;

			t += Time(x[0], x[1], x[2]);

			t += Date(x[6], x[4], x[5]);

			m_pExtra->SetNow(t);
			}
		
		return TRUE;
		}

	if( pCmd[1] == 'A' ) {

		#if !defined(_M_IX86)

		HostMaxIPL();

		#endif

		for(;;);
		
		return TRUE;
		}

	if( pCmd[1] == '?' ) {

		StartHostFrame(51);

		HostAppend("<?/");

		CContext *pScan = m_pHead;

		while( pScan ) {

			PCTXT pState = pScan->m_fOnline ? "Active" : "Inactive";

			HostPrintf("%2.2u=%s", pScan->m_bDrop, pState);

			if( (pScan = pScan->m_pNext) ) {

				HostAppend(",");
				}
			}

		HostAppend(">");

		QueueHostFrame();
		
		return TRUE;
		}

	if( pCmd[1] == '&' ) {

		StartHostFrame(51);

		AppendMark(m_sSend, TRUE);

		QueueHostFrame();
		
		return TRUE;
		}

	if( pCmd[1] == '#' ) {

		StartHostFrame(51);

		HostPrintf("<#/20-000943-01 %3.3u B>", C3_BUILD);
 
		QueueHostFrame();
		
		return TRUE;
		}

	if( pCmd[1] == '!' ) {

		StartHostFrame(51);

		HostPrintf("<!/%4.4X>", FindChecksums());

		QueueHostFrame();
		
		return TRUE;
		}

	return FALSE;
	}

// Host Frame Building

void CMicroscan::StartHostFrame(BYTE bDrop)
{
	strcpy(m_sSend, m_pTxOpen);

	UINT n = strlen(m_sSend);

	if( m_fTxDrop ) {

		m_sSend[n++] = '0' + (bDrop / 10);
		
		m_sSend[n++] = '0' + (bDrop % 10);
		}
	
	m_sSend[n++] = 0;
	}

void CMicroscan::HostAppend(PCTXT pText)
{
	strcat(m_sSend, pText);
	}

void CMicroscan::HostPrintf(PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	VSPrintf(m_sSend + strlen(m_sSend), pText, pArgs);

	va_end(pArgs);
	}

BOOL CMicroscan::QueueHostFrame(void)
{
	strcat(m_sSend, m_pTxClose);

	UINT uSize = strlen(m_sSend);

	FinishHostFrame(uSize);

	UINT uBuff = m_uHostSize;

	UINT uRoom = uBuff - 1 - (uBuff + m_uHostTail - m_uHostHead) % uBuff;

	if( uSize <= uRoom ) {

		UINT uLeft = uBuff - m_uHostTail;

		if( uLeft >= uSize ) {

			memcpy(m_pHostData + m_uHostTail, m_sSend, uSize);
			}
		else {
			memcpy(m_pHostData + m_uHostTail, m_sSend, uLeft);

			memcpy(m_pHostData, m_sSend + uLeft, uSize - uLeft);
			}

		m_uHostTail = (m_uHostTail + uSize) % uBuff;

		return TRUE;
		}

	HostSend();

	return FALSE;
	}

BOOL CMicroscan::FinishHostFrame(UINT &uSize)
{
	if( m_uProtocol == 0 ) {

		if( m_fLRC ) {

			BYTE bLRC = 0x00;

			for( UINT n = 0; n < uSize; n++ ) {

				bLRC ^= m_sSend[n];
				}

			m_sSend[uSize++] = bLRC;
			}

		return TRUE;
		}

	if( m_uProtocol == 1 ) {

		m_sSend[uSize++] = 0xFF;

		return TRUE;
		}

	return FALSE;
	}

// Time Stamping

BOOL CMicroscan::AppendMark(PTXT pText, BOOL fNow)
{
	DWORD dwTime = fNow ? GetNow() : m_pCtx->m_uTime;

	UINT  uFract = fNow ?        0 : m_pCtx->m_uFract;

	if( m_bDateFmt && m_bTimeFmt ) {

		strcat(pText, m_sSep);

		AppendTime(pText, m_bTimeFmt, dwTime, uFract);

		strcat(pText, " ");

		AppendDate(pText, m_bDateFmt, dwTime);

		strcat(pText, m_sSep);

		return TRUE;
		}

	if( m_bDateFmt || m_bTimeFmt ) {

		strcat(pText, m_sSep);

		AppendTime(pText, m_bTimeFmt, dwTime, uFract);	

		AppendDate(pText, m_bDateFmt, dwTime);

		strcat(pText, m_sSep);

		return TRUE;
		}

	if( fNow ) {

		strcat(pText, m_sSep);

		AppendTime(pText, 2, dwTime, uFract);

		strcat(pText, " ");

		AppendDate(pText, 2, dwTime);

		strcat(pText, m_sSep);

		return TRUE;
		}

	return FALSE;
	}

BOOL CMicroscan::AppendTime(PTXT pText, BYTE bFormat, DWORD dwTime, UINT uFract)
{
	if( bFormat == 1 ) {

		SPrintf( pText + strlen(pText),
			 "%2.2u:%2.2u:%2.2u",
			 GetHour(dwTime),
			 GetMin (dwTime),
			 GetSec (dwTime)
			 );

		return TRUE;
		}

	if( bFormat == 2 ) {

		SPrintf( pText + strlen(pText),
			 "%2.2u:%2.2u:%2.2u:%2.2u",
			 GetHour(dwTime),
			 GetMin (dwTime),
			 GetSec (dwTime),
			 uFract
			 );

		return TRUE;
		}

	return FALSE;
	}

BOOL CMicroscan::AppendDate(PTXT pText, BYTE bFormat, DWORD dwTime)
{
	if( bFormat == 1 ) {

		SPrintf( pText + strlen(pText),
			 "%2.2u/%2.2u/%4.4u",
			 GetMonth(dwTime),
			 GetDate (dwTime),
			 GetYear (dwTime)
			 );

		return TRUE;
		}

	if( bFormat == 2 ) {

		SPrintf( pText + strlen(pText),
			 "%s.%2.2u/%2.2u/%4.4u",
			 GetDayName(dwTime),
			 GetMonth  (dwTime),
			 GetDate   (dwTime),
			 GetYear   (dwTime)
			 );

		return TRUE;
		}

	if( bFormat == 3 ) {

		SPrintf( pText + strlen(pText),
			 "%2.2u/%2.2u/%4.4u",
			 GetDate (dwTime),
			 GetMonth(dwTime),
			 GetYear (dwTime)
			 );

		return TRUE;
		}

	if( bFormat == 4 ) {

		SPrintf( pText + strlen(pText),
			 "%s.%2.2u/%2.2u/%4.4u",
			 GetDayName(dwTime),
			 GetDate   (dwTime),
			 GetMonth  (dwTime),
			 GetYear   (dwTime)
			 );

		return TRUE;
		}

	return FALSE;
	}

PCTXT CMicroscan::GetDayName(DWORD dwTime)
{
	switch( GetDay(dwTime) ) {

		case 0: return "Sun";
		case 1: return "Mon";
		case 2: return "Tue";
		case 3: return "Wed";
		case 4: return "Thu";
		case 5: return "Fri";
		case 6: return "Sat";
		case 7: return "Sun";
		}

	return "Day";
	}

DWORD CMicroscan::GetNow(void)
{
	return m_pExtra->GetNow();
	}

// Firmware Checksums

WORD CMicroscan::FindChecksums(void)
{
	return FindChecksum1() ^ FindChecksum2();
	}

WORD CMicroscan::FindChecksum1(void)
{
	#if defined(_M_IX86)

	return 0;

	#else

	PBYTE pData = PBYTE(&_text_from);

	PBYTE pLast = PBYTE(&_data_from);

	CRC16 crc;

	crc.Clear();

	while( pData < pLast ) {

		crc.Add(*pData++);
		}

	return crc.GetValue();

	#endif
	}

WORD CMicroscan::FindChecksum2(void)
{
	#if defined(_M_IX86)

	return 0;

	#else

	PBYTE pData = PBYTE(0x00020000);

	PBYTE pLast = PBYTE(0x00100000);

	CRC16 crc;

	crc.Clear();

	while( pData < pLast ) {

		crc.Add(*pData++);
		}

	return crc.GetValue();

	#endif
	}

// Debug Support

BOOL CMicroscan::Debug(PCTXT pText, ...)
{
	if( m_fDebug ) {

		va_list pArgs;

		va_start(pArgs, pText);

		AfxTrace(pText, pArgs);

		va_end(pArgs);

		return TRUE;
		}

	return FALSE;
	}

// End of File
