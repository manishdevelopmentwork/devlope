
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SKELSLAVE_HPP
	
#define	INCLUDE_SKELSLAVE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Skeleton S Driver
//

class CSkeletonSlaveDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSkeletonSlaveDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	
	protected:
		// Implementation
		void AddSpaces(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Slave Driver Options
//

class CSkeletonSlaveDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSkeletonSlaveDriverOptions(void);

		// Public Data
		UINT m_Drop;
	
	protected:
		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
