
#include "intern.hpp"

#include "euro590o.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 OldDevice Options == Eurotherm 590 Device Options
//

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Old Driver
//

// Instantiator

ICommsDriver *	Create_EuroOld590Driver(void)
{
	return New CEuroOld590Driver;
	}

// Constructor

CEuroOld590Driver::CEuroOld590Driver(void)
{
	m_wID		= 0x4028;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SSD Drives";
	
	m_DriverName	= "590 (Old Style)";
	
	m_Version	= "1.10";
	
	m_ShortName	= "SSD Drives 590";

	AddSpaces();
	}

// Binding Control

UINT CEuroOld590Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEuroOld590Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CEuroOld590Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEurotherm590DeviceOptions);
	}

// Implementation

void CEuroOld590Driver::AddSpaces(void)
{
	AddSpace(New CSpace(TAGB, "TB", "Tag Number - Boolean",		10, 0, 499, addrBitAsBit));
	AddSpace(New CSpace(TAGI, "TI", "Tag Number - Integer",		10, 0, 499, addrLongAsLong));
	AddSpace(New CSpace(TAGR, "TR", "Tag Number - Real",		10, 0, 499, addrRealAsReal));
	AddSpace(New CSpace(PARB, "PB", "Parameter Number - Boolean",	10, 0, 255, addrBitAsBit));
	AddSpace(New CSpace(PARI, "PI", "Parameter Number - Integer",	10, 0, 255, addrLongAsLong));
	AddSpace(New CSpace(PARR, "PR", "Parameter Number - Real",	10, 0, 255, addrRealAsReal));
	}

// End of File
