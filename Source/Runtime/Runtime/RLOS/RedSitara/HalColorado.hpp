
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalColorado_HPP

#define INCLUDE_HalColorado_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HalSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for Colorado HMI
//

class CHalColorado : public CHalSitara
{
	public:
		// Constructor
		CHalColorado(void);
	};

// End of File

#endif
