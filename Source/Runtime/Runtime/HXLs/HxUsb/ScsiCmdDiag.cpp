
#include "Intern.hpp"

#include "ScsiCmdDiag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// USB SCSI Framework
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Send Diagnostic
//

// Constructor

CScsiCmdDiag::CScsiCmdDiag(void)
{
	}

// Endianess

void CScsiCmdDiag::HostToScsi(void)
{
	m_wParams = HostToMotor(m_wParams);
	}

void CScsiCmdDiag::ScsiToHost(void)
{
	m_wParams = MotorToHost(m_wParams);
	}

// Operations

void CScsiCmdDiag::Init(void)
{
	memset(this, 0, sizeof(ScsiCmdDiag));

	m_bOpcode   = cmdSendDiag;

	m_bSelfTest = true;
	}

void CScsiCmdDiag::Init(WORD wLength)
{
	Init();

	m_wParams = wLength;
	}

// End of File
