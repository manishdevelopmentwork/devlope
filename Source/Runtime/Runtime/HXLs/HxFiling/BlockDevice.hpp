
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved

#ifndef	INCLUDE_BlockDevice_HPP

#define	INCLUDE_BlockDevice_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Block Device Object
//

class DLLAPI CBlockDevice : public IBlockDevice
{
	public:
		// Constructor
		CBlockDevice(void);

		// Destructor
		~CBlockDevice(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDeviceEx
		BOOL METHOD Open(IUnknown *pUnk);

		// IBlockDevice
		BOOL IsReady(void);
		void Attach(IEvent *pEvent);
		void Arrival(PVOID pDevice);
		void Removal(void);
		UINT GetSectorCount(void);
		UINT GetSectorSize(void);
		UINT GetCylinderCount(void);
		UINT GetHeadCount(void);
		UINT GetSectorsPerHead(void);
		BOOL WriteSector(UINT uSector, PCBYTE pData);
		BOOL ReadSector(UINT uSector, PBYTE pData);
		void ClearCounters(void);
		UINT GetReadCount(void);
		UINT GetWriteCount(void);

	protected:
		// Data
		UINT m_uRefs;
		UINT m_uReadCount;
		UINT m_uWriteCount;
	};

// End of File

#endif
