
//////////////////////////////////////////////////////////////////////////
//
// Allen-Bradley Ultra3000 Driver
//

#define ABULTRA3000_ID	0x3380

// Space information
#define	OP_WDRIVE	0x600 // Send Drive Name
#define	OP_DRIVE1	0x601
#define	OP_DRIVE8	0x608

#define	OP_WMODEL	0xEB0 // Send Motor Model
#define	OP_MODEL1	0xEB1
#define	OP_MODEL8	0xEB8

// Ping Control
#define	STOREDRIVENAME	TRUE
#define	STOREMOTORMODEL	FALSE

// Function Code Definitions
#define	FREADWORK	0
#define	FWRITEWORK	1
#define	FSTORE		1
#define	FREADNONV	2
#define	FWRITENONV	3
#define	FNONV2WORK	4
#define	FWORK2NONV	5
#define	FREADDEF	6
#define	FDEF2NONV	7
#define	FREADMIN	8
#define	FREADMAX	9
#define	FARRAYMIN	10
#define	FARRAYMAX	11

// INDEXED OPCODES TABLE NUMBERS

#define	IOP15D	0x01
#define	IOP15E	0x02
#define	IOP05B	0x03
#define	IOP05C	0x04
#define	IOP00D	0x05
#define	IOP00E	0x06
#define	IOP02B	0x07
#define	IOP093	0x08
#define	IOP01B	0x09
#define	IOP01A	0x0A
#define	IOP0AE	0x0B
#define	IOP0AF	0x0C
#define	IOP0B0	0x0D
#define	IOP0B1	0x0E
#define	IOP0B2	0x0F
#define	IOP0B3	0x10
#define	IOP0B4	0x11
#define	IOP0B5	0x12
#define	IOP0B6	0x13
#define	IOP0B7	0x14
#define	IOP0E5	0x15
#define	IOP069	0x16
#define	IOP0CE	0x17
#define	ILAST	0x17

// Internal data
#define	CEXC		0x300 // latest exception code
#define	CEXP		0x301 // parameter of latest exception code

// Receive Buffer Constants
#define CPARAMETER	3
#define CDATA		7

#define	FSTART	0
#define FADDR1	1
#define FADDR2	2
#define	FCOMM1	3
#define FEXCPT	3
#define	FCOMM2	4
#define	FCOMM3	5
#define	FFUNCT	6

// Text Tables
#define	TDRIVE	1
#define	TMODEL	2
#define	TWRITE	3

// Data Display Constants
#define	LARGE_DECIMAL_0 1000000000


class CABUltra3Driver : public CMasterDriver
{
	public:
		// Constructor
		CABUltra3Driver(void);

		// Destructor
		~CABUltra3Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			BYTE	m_bBroadcast;
			};
		CContext *	m_pCtx;

		// Data Members

		BYTE	m_bTx[80];
		BYTE	m_bRx[80];
		UINT	m_uPtr;
		UINT	m_bCheck;
		DWORD	m_dCEXC;
		DWORD	m_dCEXP;
		DWORD	m_dDrive[9];
		DWORD	m_dModel[9];

		// Hex Lookup
		LPCTXT	m_pHex;
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddData(DWORD dData, DWORD dFactor);
		void	AddCommand(UINT uCommand, UINT uFunction);
		void	AddWriteData(DWORD dData, CAddress Addr);
		
		// Transport Layer
		BOOL	Transact(void);
		void	Send(void);
		BOOL	GetReply(void);
		BOOL	VerifyChecksum(UINT uCount);
		BOOL	CheckException(void);
		void	GetResponse(PDWORD pData);
		void	GetTextResponse(PDWORD pData, UINT uPosition);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		BOOL	NoReadTransmit(PDWORD pData, CAddress Addr);
		BOOL	NoWriteTransmit(DWORD dData, CAddress Addr);
		void	PutGeneric(DWORD dData, DWORD dFactor);
		DWORD	GetGeneric(UINT uPos, UINT uCount);
		BYTE	xtoin(BYTE b);
		DWORD	GetDataSize(CAddress Addr);
		void	AddCheckSum(void);
		BOOL	InvalidRWWorking(CAddress Addr);
		UINT	GetCommandFromTableNumber(UINT uTable);
		UINT	IsTextItem(UINT uCommand, UINT * pPosition);
		void	TransferText(PDWORD pTable, UINT uCount);
		BOOL	ReservedOpcode(CAddress Addr);
		void	StoreText(BOOL fDrive);
		BOOL	Is2ByteData(UINT uCommand);
		void	Dbg(UINT u, DWORD d);

	};

// End of File
