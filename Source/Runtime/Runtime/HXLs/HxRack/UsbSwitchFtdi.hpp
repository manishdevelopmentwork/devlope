
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbSwitchFtdi_HPP

#define	INCLUDE_UsbSwitchFtdi_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Ftdi Port Switch 
//

class CUsbSwitchFtdi : public IPortSwitch
{
	public:
		// Constructor
		CUsbSwitchFtdi(IUsbHostFuncDriver *pDriver, UINT uType, UINT uBit);

		// Destructor
		~CUsbSwitchFtdi(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPortSwitch
		UINT METHOD GetCount(UINT uUnit);
		UINT METHOD GetMask(UINT uUnit);
		UINT METHOD GetType(UINT uUnit, UINT uLog);
		BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable);
		BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485);
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);
		BOOL METHOD SetMode(UINT uUnit, BOOL fAuto);

	protected:
		// Data Members
		ULONG          m_uRefs;
		IUsbHostFtdi * m_pDriver;
		IMutex       * m_pMutex;
		UINT	       m_uType;
		UINT           m_uBit;

		// Implementation
		void Init(void);
	};

// End of File

#endif


