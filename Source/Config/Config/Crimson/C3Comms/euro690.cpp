
#include "intern.hpp"

#include "euro690.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 690 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEurotherm690DeviceOptions, CUIItem);

// Constructor

CEurotherm690DeviceOptions::CEurotherm690DeviceOptions(void)
{
	m_Group		= 0;
	m_Unit		= 0;
	}

// Download Support

BOOL CEurotherm690DeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte((BYTE)m_Group);
	Init.AddByte((BYTE)m_Unit);

	return TRUE;
	}

// Meta Data Creation

void CEurotherm690DeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	}

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 690 Driver
//

// Instantiator

ICommsDriver *	Create_Euro690Driver(void)
{
	return New CEuro690Driver;
	}

// Constructor

CEuro690Driver::CEuro690Driver(void)
{
	m_wID		= 0x4008;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "SSD Drives";
	
	m_DriverName	= "690+/650v";
	
	m_Version	= "1.10";
	
	m_ShortName	= "SSD Drives 690+/650v";

	AddSpaces();
	}

// Binding Control

UINT CEuro690Driver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEuro690Driver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEuro690Driver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEurotherm690DeviceOptions);
	}

// Implementation

// Address Helpers

BOOL CEuro690Driver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uOffset = 0;

	PTXT	pError = NULL;

	CString Offset;

	if( pSpace->m_uTable >= 1 && pSpace->m_uTable <= 3 ) {

		UINT uFind = Min(Text.Find('_'), Min( Text.Find(' '), Text.Find('(') ));

		if( uFind == NOTHING ) {

			uFind = Text.Find('.');
			}

		Offset  = Text.Left( uFind );

		uOffset = tstrtoul( Offset, &pError, 10 );

		if( pError && pError[0] ) {
	
			Error.Set( CString(IDS_ERROR_ADDR),
				   0
				   );
		
			return FALSE;
			}

		if( pSpace->IsOutOfRange(uOffset) ) {
		
			Error.Set( CString(IDS_ERROR_OFFSETRANGE),
				   0
				   );
				
			return FALSE;
			}
		}

	else {
		if( pSpace->m_uTable == 0 || pSpace->m_uTable > 11 ) {
	
			Error.Set( CString(IDS_ERROR_ADDR),
				   0
				   );
		
			return FALSE;
			}
		}

	Addr.a.m_Type	= pSpace->m_uType;

	Addr.a.m_Table	= pSpace->m_uTable;
	
	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uOffset;
	
	return TRUE;
	}

BOOL CEuro690Driver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	BYTE bHi = 0;

	BYTE bLo = 0;

	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		switch ( pSpace->m_uTable ) {

			case 1:
			case 2:
			case 3:

				GetIDs( LOWORD(Addr.m_Ref), &bHi, &bLo );

				Text.Printf( "%s%d_ID%c%c",   
					pSpace->m_Prefix,
					LOWORD(Addr.m_Ref),
					bHi,
					bLo
					);
				break;

			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9:
			case 10:
			case 11:

				Text.Printf( "%s",
					pSpace->m_Prefix
					);
				break;

			default:
				return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation
void CEuro690Driver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "B",  "Boolean",				10, 0, 1971, addrByteAsByte));
	AddSpace(New CSpace(2, "W",  "Words and Long Words",		10, 0, 1971, addrLongAsLong));
	AddSpace(New CSpace(3, "R",  "Real Numbers",			10, 0, 1971, addrRealAsReal));
	AddSpace(New CSpace(4, "EE", "Last Error Code",			10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(5, "II", "Instrument Identity (Read Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(6, "V0", "Software Version (Read Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(7, "V1", "Keypad Version (Read Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(8, "!1", "Command (Write Only)",		10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(9, "!2", "State (Read Only)",		10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(10,"!3", "Save Command (Write Only)",	10, 0,    0, addrWordAsWord));
	AddSpace(New CSpace(11,"!4", "Save State (Read Only)",		10, 0,    0, addrWordAsWord));
	}

// Helper
void CEuro690Driver::GetIDs( UINT uOffset, PBYTE pHi, PBYTE pLo )
{
	if( uOffset < 1296 ) {

		UINT uHi = uOffset / 36;

		UINT uLo = uOffset % 36;

		*pHi = LOBYTE( uHi < 10 ? uHi + '0' : uHi + 'a' - 10 );

		*pLo = LOBYTE( uLo < 10 ? uLo + '0' : uLo + 'a' - 10 );
		}

	else {
		*pHi = LOBYTE( 'a' + ((uOffset-1296) / 26) );

		*pLo = LOBYTE( 'A' + ((uOffset-1296) % 26) );
		}
	}

// End of File
