
#include "Intern.hpp"

#include "DataLogList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DataLog.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

// Dynamic Class

AfxImplementDynamicClass(CDataLogList, CNamedList);

// Constructor

CDataLogList::CDataLogList(void)
{
	m_Class = AfxRuntimeClass(CDataLog);
	}

// Item Access

CDataLog * CDataLogList::GetItem(INDEX Index) const
{
	return (CDataLog *) CNamedList::GetItem(Index);
	}

CDataLog * CDataLogList::GetItem(UINT uPos) const
{
	return (CDataLog *) CNamedList::GetItem(uPos);
	}

// End of File
