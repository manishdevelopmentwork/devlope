
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_AM437_Gic437_HPP
	
#define	INCLUDE_AM437_Gic437_HPP

//////////////////////////////////////////////////////////////////////////
//
// AM437 Generalized Interrupt Controller
//

class CGic437 : public IPic
{
	public:
		// Constructor
		CGic437(void);

		// Attributes
		UINT GetLineCount(void) const;
		UINT GetPriorityCount(void) const;
		UINT GetPriorityMask(void) const;
		UINT GetLinePriority(UINT uLine) const;
		bool IsLineEnabled(UINT uLine) const;
		bool IsLinePending(UINT uLine) const;

		// IDeviceDriver
		void DevInit(void);
		void DevTerm(void);

		// IPic
		void OnInterrupt(UINT uSource);
		bool SetLinePriority(UINT uLine, UINT uPriority);
		bool SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam);
		bool EnableLine(UINT uLine, bool fEnable);
		bool EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable);
		void DebugRegister(void);
		void DebugRevoke(void);

		// Operations
		bool SetPriorityMask(UINT uPriority);

	protected:
		// Distributor Registers
		enum
		{
			regdCTRL	= 0x0000 / sizeof(DWORD),
			regdTYPE	= 0x0004 / sizeof(DWORD),
			regdPOWER	= 0x0008 / sizeof(DWORD),
			regdSECURE	= 0x0080 / sizeof(DWORD),
			regdENABLESET	= 0x0100 / sizeof(DWORD),
			regdENABLECLR	= 0x0180 / sizeof(DWORD),
			regdPENDSET	= 0x0200 / sizeof(DWORD),
			regdPENDCLR	= 0x0280 / sizeof(DWORD),
			regdACTIVE	= 0x0300 / sizeof(DWORD),
			regdPRIORITY	= 0x0400 / sizeof(DWORD),
			regdTARGET	= 0x0800 / sizeof(DWORD),
			regdCONFIG	= 0x0C00 / sizeof(DWORD),
			regdPRIVATE	= 0x0D00 / sizeof(DWORD),
			regdSHARED	= 0x0D04 / sizeof(DWORD),
			regdTRIGGER	= 0x0F00 / sizeof(DWORD),
			};

		// Interrupt Controller Registers
		enum
		{
			regcCTRL	= 0x0000 / sizeof(DWORD),
			regcPRIMASK	= 0x0004 / sizeof(DWORD),
			regcBINPOINT	= 0x0008 / sizeof(DWORD),
			regcACK		= 0x000C / sizeof(DWORD),
			regcEOI	 	= 0x0010 / sizeof(DWORD),
			regcRUNNING 	= 0x0014 / sizeof(DWORD),
			regcPENDING	= 0x0018 / sizeof(DWORD),
			regcALIASED	= 0x001C / sizeof(DWORD),
			};

		// Sink Record
		struct CSink
		{
			IEventSink * m_pSink;
			UINT	     m_uParam;
			};

		// Special Int IDs
		enum
		{
			intidNoPend = 1023,
			};

		// Data Members
		PVDWORD	m_pDist;
		PVDWORD	m_pCtrl;
		UINT	m_uSets;
		UINT    m_uLines;
		CSink * m_pSink;

		// Implementation
		bool DisableAll(void);
		bool Dispatch(void);
		bool EnableLine(UINT uLine);
		bool DisableLine(UINT uLine);
	};

// End of File

#endif
