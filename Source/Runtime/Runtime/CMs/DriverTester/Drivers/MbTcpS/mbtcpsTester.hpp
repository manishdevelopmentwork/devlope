
#include "../../Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_mbtcpsTester_HPP

#define INCLUDE_mbtcpsTester_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../../Comms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Master Driver Tester
//

class CModbusTCPSlaveTester : public CCommsTester
{
	public:
		// Constructor
		CModbusTCPSlaveTester(void);

		// Destructor
		~CModbusTCPSlaveTester(void);

		// IDriverTester
		BOOL  METHOD GetSerialConfig(CSerialConfig &Config);
		BOOL  METHOD GetDriverConfig(PBYTE &pData);
		BOOL  METHOD GetDeviceConfig(PBYTE &pData);
		CCODE METHOD SlaveRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE METHOD SlaveRead (AREF Addr, PDWORD pData, IMetaData **ppm);
		CCODE METHOD SlaveWrite(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		DWORD m_Data[256];
		UINT  m_uTrans;
	};

// End of File

#endif
