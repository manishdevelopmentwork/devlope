
#include "intern.hpp"

#include "secure.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2009 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Security Descriptor
//

// Constructor

CSecDesc::CSecDesc(void)
{
	m_Access  = allowDefault;

	m_Logging = 3;
	}

// Destructor

CSecDesc::~CSecDesc(void)
{
	}

// Initialization

void CSecDesc::Load(PCBYTE &pData)
{
	ValidateLoad("CSecDesc", pData);

	m_Access  = GetLong(pData);

	m_Logging = GetByte(pData);
	}

// End of File
