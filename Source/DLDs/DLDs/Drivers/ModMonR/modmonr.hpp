
//////////////////////////////////////////////////////////////////////////
//
// Modbus Exception Codes
//

#define	ILLEGAL_FUNCTION	0x01

#define ILLEGAL_ADDRESS		0x02

#define	ILLEGAL_DATA		0x03

//////////////////////////////////////////////////////////////////////////
//
// Modbus Monitor Driver
//

class CModbusMonitorRTU : public CSlaveDriver
{
	public:
		// Constructor
		CModbusMonitorRTU(void);

		// Config
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Entry Points
		DEFMETH(void) Service(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

	protected:
		// Data
		DWORD	   m_dDrop;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		UINT	   m_uReadAddr;
		UINT	   m_uReadCount;
		UINT	   m_uRcvSize;
		CRC16	   m_CRC;

		// Frame Handlers
		BOOL HandleRead(UINT uTable);
		BOOL HandleMultiWrite(UINT uTable);
		BOOL HandleSingleWrite(UINT uTable);

		// Implementation
		void Limit(UINT &uData, UINT uMin, UINT uMax);
		void AllocBuffers(void);

		// Port Access
		
		// Frame Building
		
		// Transport Layer
		BOOL GetFrame(void);
		BOOL BinaryRx(void);

		// Response Helper
		void UnpackBits(PDWORD pWork, UINT * pCount);
	};

// End of File
