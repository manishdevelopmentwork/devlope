
//////////////////////////////////////////////////////////////////////////
//
// MTS Modbus Data Spaces
//

#define	SPACE_HOLD	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06
#define	SPACE_FILE	0x07
#define	SPACE_ADDR	0x08

#define	FILE_READ	0x14
#define	FILE_WRITE	0x15

//////////////////////////////////////////////////////////////////////////
//
// MTS Modbus Driver
//

class CMTSModbusDriver : public CMasterDriver
{
	public:
		// Constructor
		CMTSModbusDriver(void);

		// Destructor
		~CMTSModbusDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			UINT m_bDrop;
			BOOL m_fRLCAuto;
			BOOL m_fDisable15;
			BOOL m_fDisable16;
			UINT m_uMax01;
			UINT m_uMax02;
			UINT m_uMax03;
			UINT m_uMax04;
			UINT m_uMax15;
			UINT m_uMax16;
			UINT m_uPing;
			BOOL m_fDisable5;
			BOOL m_fDisable6;
			BOOL m_fDisableCheck;
			UINT m_uActualDrop;
			};

		// Data Members
		BOOL	   m_fAscii;
		UINT	   m_uMaxWords;
		UINT	   m_uMaxBits;
		CContext * m_pCtx;
		LPCTXT	   m_pHex;
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE     * m_pTx;
		BYTE     * m_pRx;
		UINT	   m_uPtr;
		CRC16	   m_CRC;
				
		// Implementation
		void Limit(UINT &uData, UINT uMin, UINT uMax);
		void AllocBuffers(void);
		BOOL IsHex(BYTE bData);
		WORD FromHex(BYTE bData);

		// Port Access
		void TxByte(BYTE bData);
		UINT RxByte(UINT uTime);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		
		// Transport Layer
		BOOL PutFrame(void);
		BOOL GetFrame(BOOL fWrite);
		BOOL BinaryTx(void);
		BOOL BinaryRx(BOOL fWrite);
		BOOL AsciiTx(void);
		BOOL AsciiRx(void);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckReply(BOOL fIgnore);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		CCODE FileRead( AREF Addr, PDWORD pData, UINT uCount );
		CCODE FileWrite( AREF Addr, PDWORD pData, UINT uCount );
	};

// End of File
