
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbNetwork_HPP
	
#define	INCLUDE_UsbNetwork_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Network
//

class CUsbNetwork : public INic, public IDiagProvider
{
	public:
		// Constructor
		CUsbNetwork(IUsbHostFuncDriver *pDriver);

		// Destructor
		~CUsbNetwork(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);
		
		// INic
		bool METHOD Open(bool fFast, bool fFull);
		bool METHOD Close(void);
		bool METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		bool METHOD SetMulticast(MACADDR const *pList, UINT uList);
		bool METHOD IsLinkActive(void);
		bool METHOD WaitLink(UINT uTime);
		bool METHOD SendData(CBuffer *  pBuff, UINT uTime);
		bool METHOD ReadData(CBuffer * &pBuff, UINT uTime);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Data Members
		ULONG         m_uRefs;
		UINT          m_uProv;
		IUsbNetwork * m_pNetwork;
		bool          m_fOpen;

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif
