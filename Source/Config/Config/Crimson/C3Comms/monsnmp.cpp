
#include "intern.hpp"

#include "monsnmp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CMonicoSNMPDriverOptions, CUIItem);

// Constructor

CMonicoSNMPDriverOptions::CMonicoSNMPDriverOptions(void)
{
	m_Port1      = 161;
	m_Port2	     = 162;
	m_Comm       = L"public";
	m_AclAddr1   = 0;
	m_AclMask1   = 0;
	m_AclAddr2   = 0;
	m_AclMask2   = 0;
	m_TrapFrom   = 0;
	m_TrapAddr1  = 0;
	m_TrapMode1  = 0;
	m_TrapAddr2  = 0;
	m_TrapMode2  = 0;
	m_SysDefault = 1;
	}

// UI Managament

void CMonicoSNMPDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pWnd);
		}
		
	if( Tag == "TrapMode1" || Tag == "TrapMode2" ) {

		DoEnables(pWnd);
		}
		
	if( Tag == "AclAddr1" || Tag == "AclAddr2" ) {

		DoEnables(pWnd);
		}

	if( Tag == "SysDefault" ) {

		DoEnables(pWnd);
		}
	}

// Download Support

BOOL CMonicoSNMPDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Port1));
	Init.AddWord(WORD(m_Port2));
	Init.AddText(m_Comm);
	Init.AddLong(m_AclAddr1);
	Init.AddLong(m_AclMask1);
	Init.AddLong(m_AclAddr2);
	Init.AddLong(m_AclMask2);
	Init.AddLong(m_TrapFrom);
	Init.AddLong(m_TrapAddr1);
	Init.AddWord(WORD(m_TrapMode1));
	Init.AddLong(m_TrapAddr2);
	Init.AddWord(WORD(m_TrapMode2));
	Init.AddByte(BYTE(m_SysDefault));

	if( !m_SysDefault ) {

		Init.AddText(m_SysDescr);
		Init.AddText(m_SysContact);
		Init.AddText(m_SysName);
		Init.AddText(m_SysLocation);
		}
			
	return TRUE;
	}

// Meta Data Creation

void CMonicoSNMPDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Port1);
	Meta_AddInteger(Port2);
	Meta_AddString (Comm);
	Meta_AddInteger(AclAddr1);
	Meta_AddInteger(AclMask1);
	Meta_AddInteger(AclAddr2);
	Meta_AddInteger(AclMask2);
	Meta_AddInteger(TrapFrom);
	Meta_AddInteger(TrapAddr1);
	Meta_AddInteger(TrapMode1);
	Meta_AddInteger(TrapAddr2);
	Meta_AddInteger(TrapMode2);
	Meta_AddInteger(SysDefault);
	Meta_AddString(SysDescr);
	Meta_AddString(SysContact);
	Meta_AddString(SysName);
	Meta_AddString(SysLocation);
	}

// Implementation

void CMonicoSNMPDriverOptions::DoEnables(CUIViewWnd *pWnd)
{
	pWnd->EnableUI("TrapAddr1",	m_TrapMode1 > 0);

	pWnd->EnableUI("TrapAddr2",	m_TrapMode2 > 0);

	pWnd->EnableUI("TrapFrom",	m_TrapMode1 == 1 || m_TrapMode2 == 1);

	pWnd->EnableUI("AclMask1",	m_AclAddr1 > 0);

	pWnd->EnableUI("AclMask2",	m_AclAddr2 > 0);

	pWnd->EnableUI("SysDescr",	!m_SysDefault);

	pWnd->EnableUI("SysContact",	!m_SysDefault);

	pWnd->EnableUI("SysName",	!m_SysDefault);

	pWnd->EnableUI("SysLocation",	!m_SysDefault);
	}

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver
//

// Instantiator

ICommsDriver * Create_MonicoSNMPDriver(void)
{
	return New CMonicoSNMPDriver;
	}

// Constructor

CMonicoSNMPDriver::CMonicoSNMPDriver(void)
{
	m_wID		= 0x408E;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Monico";
	
	m_DriverName	= "SNMP for Generator Set 1.0";
	
	m_Version	= "1.00";
	
	m_ShortName	= "SNMP 1.0";

	AddSpaces();
	}

// Configuration

CLASS CMonicoSNMPDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CMonicoSNMPDriverOptions);
	}

CLASS CMonicoSNMPDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Binding Control

UINT CMonicoSNMPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CMonicoSNMPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 0;
	
	Ether.m_UDPCount = 1;
	}

 // Implementation

void CMonicoSNMPDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1, "gsD",  "GenSet Data",  10, 1, 193, addrLongAsLong));

	AddSpace(New CSpace(2, "gsT",  "GenSet Trap",  10, 1, 167, addrLongAsLong));

	AddSpace(New CSpace(3, "genD", "Generic Data", 10, 1, 200, addrLongAsLong));

	AddSpace(New CSpace(4, "genT", "Generic Trap", 10, 1, 200, addrLongAsLong));
	}

//////////////////////////////////////////////////////////////////////////
//
// Monico SNMP Driver v2.0
//

// Instantiator

ICommsDriver * Create_MonicoSNMPDriver2(void)
{
	return New CMonicoSNMPDriver2;
	}

// Constructor

CMonicoSNMPDriver2::CMonicoSNMPDriver2(void)
{
	m_wID		= 0x40A3;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Monico";
	
	m_DriverName	= "SNMP for Generator Set 2.0";
	
	m_Version	= "2.01";
	
	m_ShortName	= "SNMP 2.0";

	DeleteAllSpaces();

	AddSpaces();
	}

// Implementation

void CMonicoSNMPDriver2::AddSpaces(void)
{
	AddSpace(New CSpace(11,	"gsINT",	"GenSet 16-bit Data",	10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(12,	"gsDINT",	"GenSet 32-bit Data",	10, 1, 250,	addrLongAsLong));

	AddSpace(New CSpace(13,	"gsTRAPS",	"GenSet Trap",		10, 1, 1000,	addrLongAsLong));

	AddSpace(New CSpace(21,	"genINT",	"Generic 16-bit Data",	10, 1, 1000,	addrLongAsLong));

	AddSpace(New CSpace(22,	"genDINT",	"Generic 32-bit Data",	10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(23,	"genTRAPS",	"Generic Trap",		10, 1, 1000,	addrLongAsLong));

	
	AddSpace(New CSpace(31,	"cp1DINT",	"Cummins PC1x 32-bit Data",	10, 1, 100,	addrLongAsLong));

	AddSpace(New CSpace(32,	"cp1TRAPS",	"Cummins PC1x Trap",		10, 1, 200,	addrLongAsLong));

	AddSpace(New CSpace(33,	"cp2DINT",	"Cummins PC23x 32-bit Data",	10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(34,	"cp2TRAPS",	"Cummins PC23x Trap",		10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(35,	"cmlDINT",	"Cummins MODLON 32-bit Data",	10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(36,	"cmlTRAPS",	"Cummins MODLON Trap",		10, 1, 500,	addrLongAsLong));


	AddSpace(New CSpace(41,	"ggpDINT",	"Generac G Panels 32-bit Data",	10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(42,	"ggpTRAPS",	"Generac G Panels Trap",	10, 1, 1000,	addrLongAsLong));

	AddSpace(New CSpace(43,	"ghpDINT",	"Generac H Panels 32-bit Data",	10, 1, 100,	addrLongAsLong));

	AddSpace(New CSpace(44,	"ghpTRAPS",	"Generac H Panels Trap",	10, 1, 200,	addrLongAsLong));


	AddSpace(New CSpace(51,	"kd3DINT",	"Kohler DEC3000 32-bit Data",	10, 1, 350,	addrLongAsLong));

	AddSpace(New CSpace(52,	"kd3TRAPS",	"Kohler DEC3000 Trap",		10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(53,	"kd5DINT",	"Kohler DEC550 32-bit Data",	10, 1, 150,	addrLongAsLong));

	AddSpace(New CSpace(54,	"kd5TRAPS",	"Kohler DEC550 Trap",		10, 1, 150,	addrLongAsLong));


	AddSpace(New CSpace(61,	"mtuDINT",	"MTU 32-bit Data",		10, 1, 500,	addrLongAsLong));

	AddSpace(New CSpace(62,	"mtuTRAPS",	"MTU Trap",			10, 1, 1000,	addrLongAsLong));


	AddSpace(New CSpace(71,	"atsDINT",	"ATS & Misc 32-bit Data",	10, 1, 1000,	addrLongAsLong));

	AddSpace(New CSpace(72,	"atsTRAPS",	"ATS & Misc Trap",		10, 1, 1000,	addrLongAsLong));

	}


// End of File
