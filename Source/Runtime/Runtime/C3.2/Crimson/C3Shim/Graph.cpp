
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Shims
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Graphics Objects
//

ITouchMap * Create_TouchMap(IGdi *pGdi)
{
	ITouchScreen *pTouchScreen = NULL;

	AfxGetObject("touch", 0, ITouchScreen, pTouchScreen);

	if( pTouchScreen ) {

		ITouchMap *pTouchMap = NULL;

		AfxNewObject("touchmap", ITouchMap, pTouchMap);

		pTouchMap->Create(pTouchScreen);

		pTouchScreen->Release();

		return pTouchMap;
	}
	else {
		ITouchMap *pTouchMap = NULL;

		AfxNewObject("touchmap", ITouchMap, pTouchMap);

		pTouchMap->Create(pGdi);

		return pTouchMap;
	}

	return NULL;
}

IRegion * Create_StdRegion(void)
{
	IRegion *pRegion = NULL;

	AfxNewObject("region", IRegion, pRegion);

	return pRegion;
}

IGdi * Create_GDI(void)
{
	IGdi *pGdi = NULL;

	AfxNewObject("gdi", IGdi, pGdi);

	return pGdi;
}

// End of File
