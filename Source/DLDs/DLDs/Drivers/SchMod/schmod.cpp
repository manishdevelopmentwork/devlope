#include "intern.hpp"

#include "schmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Schneider PLC via Modbus Driver
//

// Instantiator

INSTANTIATE(CSchModDriver);

// Constructor

CSchModDriver::CSchModDriver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Device

CCODE MCALL CSchModDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pSCtx = (CSchntext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pSCtx = new CSchntext;

			m_pCtx = m_pSCtx;

			m_pSCtx->m_bDrop      = GetByte(pData);
			m_pSCtx->m_fRLCAuto   = GetByte(pData);
			m_pSCtx->m_fDisable15 = GetByte(pData);
			m_pSCtx->m_fDisable16 = GetByte(pData);
			m_pSCtx->m_uMax01     = GetWord(pData);
			m_pSCtx->m_uMax02     = GetWord(pData);
			m_pSCtx->m_uMax03     = GetWord(pData);
			m_pSCtx->m_uMax04     = GetWord(pData);
			m_pSCtx->m_uMax15     = GetWord(pData);
			m_pSCtx->m_uMax16     = GetWord(pData);
			m_pSCtx->m_uPing      = GetWord(pData);
			m_pSCtx->m_fDisable5  = GetByte(pData);
			m_pSCtx->m_fDisable6  = GetByte(pData);
			m_pSCtx->m_fNoCheck   = GetByte(pData);
			m_pSCtx->m_fNoReadEx  = GetByte(pData);
			m_pSCtx->m_uPoll      = GetWord(pData);
			m_pSCtx->m_fSwapCRC   = GetByte(pData);
			m_pSCtx->m_bByteR     = GetByte(pData);
			m_pSCtx->m_bByteL     = GetByte(pData);
			
			SetLastPoll();
			
			Limit(m_pSCtx->m_uMax01, 1, m_uMaxBits );
			Limit(m_pSCtx->m_uMax02, 1, m_uMaxBits );
			Limit(m_pSCtx->m_uMax03, 1, m_uMaxWords);
			Limit(m_pSCtx->m_uMax04, 1, m_uMaxWords);

			Limit(m_pSCtx->m_uMax15, 1, m_pSCtx->m_fDisable15 ? 1 : m_uMaxBits );
			Limit(m_pSCtx->m_uMax16, 1, m_pSCtx->m_fDisable16 ? 1 : m_uMaxWords);

			pDevice->SetContext(m_pSCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	m_pCtx = m_pSCtx;

	return CCODE_SUCCESS;
	}

CCODE MCALL CSchModDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pSCtx;

		m_pSCtx = NULL;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CSchModDriver::Ping(void)
{
	return CModbusDriver::Ping();
	}

CCODE MCALL CSchModDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{	
	CAddress Address;

	Address.a.m_Table  = Addr.a.m_Table;
		
	Address.a.m_Offset = Addr.a.m_Offset + 1;
		
	Address.a.m_Type   = Addr.a.m_Type;

	Address.a.m_Extra  = Addr.a.m_Extra;
	
	return ReadSchneider(Address, pData, uCount);
	}			       

CCODE MCALL CSchModDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress Address;

	Address.a.m_Table  = Addr.a.m_Table;
		
	Address.a.m_Offset = Addr.a.m_Offset + 1;
		
	Address.a.m_Type   = Addr.a.m_Type;

	Address.a.m_Extra  = Addr.a.m_Extra;

	return WriteSchneider(Address, pData, uCount);
	}

CCODE CSchModDriver::ReadSchneider(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	CheckPoll();
	
	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax03));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordRead(Addr, pData, min(uCount, m_pCtx->m_uMax04));
				}

			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_HOLD32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax03+1)/2));

		case SPACE_ANALOG32:
			
			return DoLongRead(Addr, pData, min(uCount, (m_pCtx->m_uMax04+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax01));

		case SPACE_INPUT:
			
			return DoBitRead (Addr, pData, min(uCount, m_pCtx->m_uMax02));

		case SPACE_FILE:
			
			return FileRead (Addr, pData, min(uCount, m_pCtx->m_uMax03));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CSchModDriver::WriteSchneider(AREF Addr, PDWORD pData, UINT uCount)
{
	CheckPoll();
	
	switch( Addr.a.m_Table ) {

		case SPACE_HOLD:

			if( Addr.a.m_Type == addrWordAsWord ) {

				return DoWordWrite(Addr, pData, min(uCount, m_pCtx->m_uMax16));
				}

			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_HOLD32:
			
			return DoLongWrite(Addr, pData, min(uCount, (m_pCtx->m_uMax16+1)/2));

		case SPACE_OUTPUT:
			
			return DoBitWrite (Addr, pData, min(uCount, m_pCtx->m_uMax15));

		case SPACE_FILE:
			
			return FileWrite (Addr, pData, min(uCount, m_pCtx->m_uMax15));
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE CSchModDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPACE_HOLD32:
		case SPACE_HOLD:
			
			StartFrame(0x03);
			
			break;
			
		case SPACE_ANALOG32:
		case SPACE_ANALOG:
			
			StartFrame(0x04);
			
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(Addr.a.m_Offset - 1);
	
	AddWord(Addr.a.m_Table == SPACE_HOLD32 ? uCount : uCount * 2);
	
	if( Transact(FALSE) ) {

		if( uCount * 4 > m_pRx[2] ) {

			uCount = (m_pRx[2] + 2) / 4;
			}
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			DWORD x  = PU4(m_pRx + 3)[n];
			
			pData[n] = OrderBytes(MotorToHost(x), Addr.a.m_Type);
			}

		return uCount;
		}
		
	if( IgnoreException() ) {

		memset(pData, 0, uCount * sizeof(DWORD));

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CSchModDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{	
	UINT uTable = Addr.a.m_Table;
		
	if( uTable == SPACE_HOLD32 || uTable == SPACE_HOLD ) {

		StartFrame(16);
		
		AddWord(Addr.a.m_Offset - 1);
		
		AddWord(uTable == SPACE_HOLD32 ? uCount : uCount * 2);
		
		AddByte(uCount * 4);
		
		for( UINT n = 0; n < uCount; n++ ) {
			
			AddLong(OrderBytes(pData[n], Addr.a.m_Type));
			}

		if( Transact(TRUE) ) {
			
			return uCount;
			}

		return CCODE_ERROR;
		}

	return CCODE_ERROR | CCODE_HARD;
	}

DWORD CSchModDriver::OrderBytes(DWORD dwValue, UINT uType)
{
	UINT uOrder = ((uType == addrWordAsReal) ? m_pSCtx->m_bByteR : m_pSCtx->m_bByteL);

	switch( uOrder ) {

		case 0:
			return SwapWords(dwValue);

		case 1:
			return SwapBytes(SwapWords(dwValue));

		case 3:
			return SwapBytes(dwValue);
		}

	return dwValue;
	}

DWORD CSchModDriver::SwapWords(DWORD dwWord)
{
	return MAKELONG(HIWORD(dwWord), LOWORD(dwWord));
	}

DWORD CSchModDriver::SwapBytes(DWORD dwWord)
{
	WORD wHi = HIWORD(dwWord);

	WORD wLo = LOWORD(dwWord);

	wHi = MAKEWORD(HIBYTE(wHi), LOBYTE(wHi));

	wLo = MAKEWORD(HIBYTE(wLo), LOBYTE(wLo));

	return MAKELONG(wLo, wHi);
	}

// End of File

