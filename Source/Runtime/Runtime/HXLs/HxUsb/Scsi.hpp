
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbScsi_HPP

#define	INCLUDE_UsbScsi_HPP

//////////////////////////////////////////////////////////////////////////
//
// Scsi Commands
//

enum 
{
	cmdTestRdy		= 0x00,
	cmdReqSense		= 0x03,
	cmdFormat		= 0x04,
	cmdRead6		= 0x08,
	cmdWrite6		= 0x0A,
	cmdInquiry		= 0x12,
	cmdStartStop		= 0x1B,
	cmdSendDiag		= 0x1D,
	cmdReserve6		= 0x16,
	cmdRelease6		= 0x17,
	cmdLockMedia		= 0x1E,
	cmdReadCapacity		= 0x25,
	cmdRead10		= 0x28,
	cmdWrite10		= 0x2A,
	cmdVerify10		= 0x2F,
	cmdReserve10		= 0x56,
	cmdRelease10		= 0x57,
	cmdServiceIn		= 0x9E,
	cmdServiceOut		= 0x9F,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Status
//

enum 
{
	statusGood		= 0x00,
	statusCheckCond		= 0x02,
	statusCondMet		= 0x04,
	statusBusy		= 0x08,
	statusInt		= 0x10,
	statusIntCond		= 0x14,
	statusConflict		= 0x18,
	statusTaskFull		= 0x28,
	statusACAActive		= 0x30,
	statusTaskAborted	= 0x40,
	statusNoReply		= 0xFF,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Sense Codes
//

enum 
{
	senseNone		= 0x0,
	senseRecovered		= 0x1,
	senseNotReady		= 0x2,
	senseMediumError	= 0x3,
	senseHWError		= 0x4,
	senseIllegalReq		= 0x5,
	senseAttention		= 0x6,
	senseDataProtect	= 0x7,
	senseBlankCheck		= 0x8,
	senseVendor		= 0x9,
	senseCopyAborted	= 0xA,
	senseCmdAborted		= 0xB,
	senseOverflow		= 0xC,
	senseMisCompare		= 0xE,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Additional Sense Keys
//

enum 
{
	addNotReady		= 0x04,
	addLBARange		= 0x21,
	addInvalidCdb		= 0x24,
	addWriteProtect		= 0x27,
	addMediumChange		= 0x28,
	addNoFormat		= 0x31,
	addNoMedium		= 0x3A,
	addSysBufFull		= 0x55,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Qualifiers
//

enum
{
	qualWriteLock		= 0x00,
	qualWriteLockHW		= 0x01,
	qualWriteLockSW		= 0x02,
	qualSysBufFull		= 0x01,
	qualBusy		= 0x07,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Inquiry
//

#pragma pack(1)

struct ScsiInquiry
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bPDev	: 5;
	BYTE	  m_bPQual	: 3;
	BYTE	  m_bReserved1	: 7;
	BYTE	  m_bRemoveable	: 1;
	BYTE	  m_bAnsiVer	: 3;
	BYTE	  m_bEcmaVer	: 3;
	BYTE      m_bIsoVer	: 2;
	BYTE	  m_bRespFmt	: 4;
	BYTE	  m_bReserved2	: 1;
	BYTE	  m_bNaca	: 1;
	BYTE	  m_bTrmTsk	: 1;
	BYTE	  m_bAerc	: 1;
	BYTE	  m_bAddLen;
	BYTE	  m_bReserved3;
	BYTE	  m_bAddr16	: 1;
	BYTE	  m_bAddr32	: 1;
	BYTE	  m_bAckReq	: 1;
	BYTE	  m_bMedChngr	: 1;
	BYTE	  m_bMultiP	: 1;
	BYTE	  m_bVs1	: 1;
	BYTE	  m_bEncServ	: 1;
	BYTE	  m_bReserved4	: 1;
	BYTE	  m_bVs2	: 1;
	BYTE	  m_bCmdQue	: 1;
	BYTE	  m_bTransDis	: 1;
	BYTE	  m_bLinked	: 1;
	BYTE	  m_bSync	: 1;
	BYTE	  m_bWBus16	: 1;
	BYTE	  m_bWBus32	: 1;
	BYTE	  m_bRelAddr	: 1;
	char	  m_sVendId[8];
	char	  m_sProdId[16];
	DWORD	  m_dwRevision;

	#else
			
	BYTE	  m_bPQual	: 3;
	BYTE	  m_bPDev	: 5;
	BYTE	  m_bRemoveable	: 1;
	BYTE	  m_bReserved1	: 7;
	BYTE      m_bIsoVer	: 2;
	BYTE	  m_bEcmaVer	: 3;
	BYTE	  m_bAnsiVer	: 3;
	BYTE	  m_bAerc	: 1;
	BYTE	  m_bTrmTsk	: 1;
	BYTE	  m_bNaca	: 1;
	BYTE	  m_bReserved2	: 1;
	BYTE	  m_bRespFmt	: 4;
	BYTE	  m_bAddLen;
	BYTE	  m_bReserved3;
	BYTE	  m_bReserved4	: 1;
	BYTE	  m_bEncServ	: 1;
	BYTE	  m_bVs1	: 1;
	BYTE	  m_bMultiP	: 1;
	BYTE	  m_bMedChngr	: 1;
	BYTE	  m_bAckReq	: 1;
	BYTE	  m_bAddr32	: 1;
	BYTE	  m_bAddr16	: 1;
	BYTE	  m_bRelAddr	: 1;
	BYTE	  m_bWBus32	: 1;
	BYTE	  m_bWBus16	: 1;
	BYTE	  m_bSync	: 1;
	BYTE	  m_bLinked	: 1;
	BYTE	  m_bTransDis	: 1;
	BYTE	  m_bCmdQue	: 1;
	BYTE	  m_bVs2	: 1;
	char	  m_sVendorId[8];
	char	  m_sProductId[16];
	DWORD	  m_dwRevision;

	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Sense 
//

struct ScsiSense
{
	#ifdef AEON_LITTLE_ENDIAN
			
	BYTE	  m_bRespCode	: 7;
	BYTE	  m_bValid	: 1;
	BYTE	  m_bSegment;
	BYTE	  m_bSenseKey	: 4;
	BYTE	  m_bReserved	: 1;
	BYTE	  m_bIli	: 1;
	BYTE	  m_bEem	: 1;
	BYTE	  m_bFileMark	: 1;
	DWORD	  m_dwInfo;
	BYTE	  m_bAddLength;
	DWORD	  m_dwCmdInfo;
	BYTE	  m_bAsc;
	BYTE	  m_bAscq;
	BYTE	  m_bFieldUc;
	BYTE	  m_bSksFlags	: 7;
	BYTE	  m_bSksValid	: 1;
	WORD	  m_wSksData;

	#else
			
	BYTE	  m_bValid	: 1;
	BYTE	  m_bRespCode	: 7;
	BYTE	  m_bSegment;
	BYTE	  m_bFileMark	: 1;
	BYTE	  m_bEem	: 1;
	BYTE	  m_bIli	: 1;
	BYTE	  m_bReserved	: 1;
	BYTE	  m_bSenseKey	: 4;
	DWORD	  m_dwInfo;
	BYTE	  m_bAddLength;
	DWORD	  m_dwCmdInfo;
	BYTE	  m_bAsc;
	BYTE	  m_bAscq;
	BYTE	  m_bFieldUc;
	BYTE	  m_bSksValid	: 1;
	BYTE	  m_bSksFlags	: 7;
	WORD	  m_wSksData;

	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Capacity 
//

struct ScsiCapacity
{
	DWORD	  m_dwBlockAddr;
	DWORD	  m_dwBlockLen;
	};

struct ScsiCapacity16
{
	INT64	  m_dwBlockAddr;
	DWORD	  m_dwBlockLen;
	BYTE	  m_bReserved[19];
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Read/Write Flags 
//

struct ScsiReadWriteFlags
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE      m_bRelAddr	: 1;
	BYTE      m_bReserved2	: 2;
	BYTE      m_bFua	: 1;
	BYTE	  m_bDpo	: 1;
	BYTE	  m_bReserved1	: 3;

	#else

	BYTE	  m_bReserved1	: 3;
	BYTE	  m_bDpo	: 1;
	BYTE      m_bFua	: 1;
	BYTE      m_bReserved2	: 2;
	BYTE      m_bRelAddr	: 1;
			
	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Verify Flags 
//

struct ScsiVerifyFlags
{
	#ifdef AEON_LITTLE_ENDIAN
			
	BYTE	  m_bRelAddr	: 1;
	BYTE      m_bCheck	: 1;
	BYTE      m_bBlank	: 1;
	BYTE	  m_bReserved2	: 1;
	BYTE	  m_bDpo	: 1;
	BYTE	  m_bReserved1	: 3;
			
	#else

	BYTE	  m_bReserved1	: 3;
	BYTE	  m_bDpo	: 1;
	BYTE	  m_bReserved2	: 1;
	BYTE      m_bBlank	: 1;
	BYTE      m_bCheck	: 1;
	BYTE	  m_bRelAddr	: 1;
			
	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Control Byte 
//

struct ScsiCtrl
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bLink	: 1;
	BYTE	  m_bObsolete	: 1;
	BYTE	  m_bNaca	: 1;
	BYTE	  m_bReserved	: 3;
	BYTE	  m_bVendor	: 2;

	#else

	BYTE	  m_bVendor	: 2;
	BYTE	  m_bReserved	: 3;
	BYTE	  m_bNaca	: 1;
	BYTE	  m_bObsolete	: 1;
	BYTE	  m_bLink	: 1;

	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Opcode 
//

struct ScsiOpcode
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bCmd	: 5;
	BYTE	  m_bGroup	: 3;

	#else

	BYTE	  m_bGroup	: 3;
	BYTE	  m_bCmd	: 5;

	#endif
	};
			
//////////////////////////////////////////////////////////////////////////
//
// Scsi Command Descriptor Block 
//

struct ScsiCmd
{
	union 
	{
		BYTE	   m_bOpcode;
		ScsiOpcode m_Opcode;
		};
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Generic Command Descriptor Block 6 
//

struct ScsiCmd6 : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bLba	: 5;
	BYTE	  m_bReserved	: 3;
	WORD	  m_wLba;
	BYTE	  m_bLength;
	ScsiCtrl  m_Ctrl;

	#else

	BYTE	  m_bReserved	: 3;
	BYTE	  m_bLba	: 5;
	WORD	  m_wLba;
	BYTE	  m_bLength;
	ScsiCtrl  m_Ctrl;

	#endif
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Scsi Generic Command Descriptor Block 10 
//

struct ScsiCmd10 : ScsiCmd
{
	BYTE	  m_bFlags;
	DWORD	  m_dwLba;
	BYTE	  m_bReserved;
	WORD	  m_wLength;
	ScsiCtrl  m_Ctrl;
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Generic Command Descriptor Block 16 
//

struct ScsiCmd16 : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bService	: 5;
	BYTE	  m_bMisc1	: 3;

	#else

	BYTE	  m_bMisc	: 3;
	BYTE	  m_bService	: 5;

	#endif
					
	DWORD	  m_dwLba;
	DWORD	  m_dwExtra;
	DWORD	  m_dwLength;
	BYTE	  m_bMisc2;
	ScsiCtrl  m_Ctrl;
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Read Inquiry Command 
//

struct ScsiCmdInquiry : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bEvpd	: 1;
	BYTE	  m_bCmdDt	: 1;
	BYTE	  m_bReserved1	: 6;
	BYTE	  m_bPageOpcode;
	BYTE	  m_bReserved2;
	BYTE	  m_bAllocLength;
	ScsiCtrl  Ctrl;
			
	#else
			
	BYTE	  m_bReserved1	: 6;
	BYTE	  m_bCmdDt	: 1;
	BYTE	  m_bEvpd	: 1;
	BYTE	  m_bPageOpCode;
	BYTE	  m_bReserved2;
	BYTE	  m_bAllocLength;
	ScsiCtrl  Ctrl;

	#endif
	};
		
//////////////////////////////////////////////////////////////////////////
//
// Scsi Read Capacity 
//

struct ScsiCmdCapacity : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bRelAddr	: 1;
	BYTE	  m_bReserved1	: 7; 
	DWORD	  m_wBlockAddr;
	BYTE	  m_bReserved2;
	BYTE	  m_bReserved3;
	BYTE	  m_bPmi	: 1;
	BYTE	  m_bRerserve4	: 7;
	ScsiCtrl  m_Ctrl;
			
	#else

	BYTE	  m_bReserved1	: 7; 
	BYTE	  m_bRelAddr	: 1;
	DWORD	  m_wBlockAddr;
	BYTE	  m_bReserved2;
	BYTE	  m_bReserved3;
	BYTE	  m_bRerserve4	: 7;
	BYTE	  m_bPmi	: 1;
	ScsiCtrl  m_Ctrl;

	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Send Diagnostics 
//

struct ScsiCmdDiag : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN

	BYTE	  m_bUnitOfl	: 1;
	BYTE	  m_bDevOfl	: 1;
	BYTE	  m_bSelfTest	: 1;
	BYTE	  m_bReserved2	: 1;
	BYTE	  m_bPf		: 1;
	BYTE	  m_bReserved1	: 3;
	BYTE	  m_bReserved3;
	WORD	  m_wParams;
	ScsiCtrl  m_Ctrl;

	#else

	BYTE	  m_bReserved1	: 3;
	BYTE	  m_bPf		: 1;
	BYTE	  m_bReserved2	: 1;
	BYTE	  m_bSelfTest	: 1;
	BYTE	  m_bDevOfl	: 1;
	BYTE	  m_bUnitOfl	: 1;
	BYTE	  m_bReserved3;
	WORD	  m_wParams;
	ScsiCtrl  m_Ctrl;

	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Lock Media 
//

struct ScsiCmdLock : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN
			
	BYTE	  m_bReserved1;
	BYTE	  m_bReserved2;
	BYTE	  m_bReserved3;
	BYTE	  m_bPrevent	: 2;
	BYTE	  m_bReserved4	: 6;
	ScsiCtrl  m_Ctrl;

	#else

	BYTE	  m_bReserved1;
	BYTE	  m_bReserved2;
	BYTE	  m_bReserved3;
	BYTE	  m_bReserved4	: 6;
	BYTE	  m_bPrevent	: 2;
	ScsiCtrl  m_Ctrl;
			
	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Start Stop 
//

struct ScsiCmdStartStop : ScsiCmd
{
	#ifdef AEON_LITTLE_ENDIAN
			
	BYTE	  m_bImmediate	: 1;
	BYTE	  m_bReserved1	: 7;
	BYTE	  m_bReserved2;
	BYTE	  m_bReserved3;
	BYTE	  m_bStart	: 1;
	BYTE      m_bEject	: 1;
	BYTE	  m_bReserved4	: 2;
	BYTE      m_bPower	: 4;
	ScsiCtrl  m_Ctrl;

	#else
			
	BYTE	  m_bReserved1	: 7;
	BYTE      m_bImmediate	: 1;
	BYTE	  m_bReserved2;
	BYTE	  m_bReserved3;
	BYTE      m_bPower	: 4;
	BYTE	  m_bReserved4	: 2;
	BYTE      m_bEject	: 1;
	BYTE	  m_bStart	: 1;
	ScsiCtrl  m_Ctrl;
			
	#endif
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Signatures
//

enum ScsiSignatures
{
	sigBulkCmd    = 0x43425355,
	sigBulkStatus = 0x53425355,
	};
				
//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Block Constants
//

enum 
{
	bulkPassed	= 0x0,
	bulkFailed	= 0x1,
	bulkPhaseError	= 0x2,
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Command Block Wrapper
//

struct ScsiBulkCmd 
{
	DWORD	m_dwSig;
	DWORD	m_dwTag;
	DWORD	m_dwTransfer;
	BYTE	m_bFlags;

	#ifdef AEON_LITTLE_ENDIAN

	BYTE	m_bLun		: 4;
	BYTE	m_bReserved1	: 4;
	BYTE	m_bCmdLength	: 5;
	BYTE	m_bReserved2	: 3;

	#else
			
	BYTE	m_bReserved1	: 4;
	BYTE	m_bLun		: 4;
	BYTE	m_bReserved2	: 3;
	BYTE	m_bCmdLength	: 5;

	#endif

	BYTE  volatile  m_bCmd[16];
	};

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Command Status Wrapper Structure
//

#pragma pack(1)

struct ScsiBulkStatus 
{
	DWORD	m_dwSig;
	DWORD	m_dwTag;
	DWORD	m_dwResidue;
	BYTE	m_bStatus;
	};

#pragma pack()

// End of File

#endif
