
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAR3_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAR3_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeAR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A3 Radial Gauge Primitive
//

class CPrimRubyGaugeTypeAR3 : public CPrimRubyGaugeTypeAR
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeAR3(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);

		// Implementation
		void LoadLayout(void);
	};

// End of File

#endif
