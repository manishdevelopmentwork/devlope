
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Data Block Page
//

class CPrimDataPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimDataPage(CPrimData *pData, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimData * m_pData;

		// Implementation
		BOOL LoadFormat(IUICreate *pView);
		BOOL LoadColor (IUICreate *pView);
		BOOL LoadNone  (IUICreate *pView, PCTXT pGroup, PCTXT pWhat);
		BOOL LoadPage  (IUICreate *pView, UINT uSub);
		BOOL LoadEntry (IUICreate *pView, CDispFormat *pFormat);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive Data Block Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimDataPage, CUIStdPage);

// Constructor

CPrimDataPage::CPrimDataPage(CPrimData *pData, CString Title, UINT uPage)
{
	m_pData  = pData;

	m_Title  = Title;

	m_Class  = AfxRuntimeClass(CPrimData);

	m_uPage  = uPage;
	}

// Operations

BOOL CPrimDataPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 1 ) {

		LoadPage(pView, 0);

		return TRUE;
		}

	if( m_uPage == 2 ) {

		LoadPage(pView, 0);

		return TRUE;
		}

	if( m_uPage == 3 ) {

		if( !m_pData->UseTagLimits() ) {

			LoadPage(pView, 1);
			}
		else {
			LoadNone( pView,
				  CString(IDS_LIMITS),
				  CString(IDS_DATA_LIMITS_ARE)
				  );
			}

		CDispFormat *pFormat;

		if( m_pData->GetDataFormat(pFormat) ) {

			if( LoadEntry(pView, pFormat) ) {
				
				return TRUE;
				}
			}

		LoadPage(pView, 0);

		LoadPage(pView, 8);		

		return TRUE;
		}

	if( m_uPage == 5 ) {

		if( m_pData->m_Content > 0 || m_pData->m_Entry ) {

			if( !m_pData->UseTagLabel() ) {

				LoadPage(pView, 0);
				}
			else {
				LoadNone( pView,
					  CString(IDS_DATA_LABEL),
					  CString(IDS_LABEL_IS)
					  );
				}
			}

		if( m_pData->m_Content < 2 || m_pData->m_Entry ) {

			if( !m_pData->UseTagFormat() ) {

				LoadPage  (pView, 1);

				LoadFormat(pView);
				}
			else {
				LoadNone( pView,
					  CString(IDS_DATA_FORMAT),
					  CString(IDS_FORMAT_IS)
					  );
				}
			}

		pView->NoRecycle();

		return TRUE;
		}

	if( m_uPage == 7 ) {

		LoadColor(pView);

		pView->NoRecycle();

		return TRUE;
		}

	return TRUE;
	}

// Implementation

BOOL CPrimDataPage::LoadFormat(IUICreate *pView)
{
	CDispFormat *pFormat = m_pData->m_pFormat;

	if( pFormat ) {

		CUIPageList List;

		pFormat->OnLoadPages(&List);

		if( List.GetCount() ) {

			List[0]->LoadIntoView(pView, pFormat);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimDataPage::LoadColor(IUICreate *pView)
{
	if( !m_pData->UseTagColor() ) {

		CDispColor *pColor = m_pData->m_pColor;

		if( pColor ) {

			LoadPage(pView, 0);

			CUIPageList List;

			pColor->OnLoadPages(&List);

			if( List.GetCount() ) {

				List[0]->LoadIntoView(pView, pColor);

				return TRUE;
				}
			}

		LoadPage(pView, 1);

		LoadPage(pView, 3);

		return FALSE;
		}

	LoadNone(pView, CString(IDS_TEXT_COLORS), CString(IDS_TEXT_COLORS_ARE));

	LoadPage(pView, 2);

	return FALSE;
	}

BOOL CPrimDataPage::LoadNone(IUICreate *pView, PCTXT pGroup, PCTXT pWhat)
{
	pView->StartGroup(pGroup, 1);

	pView->AddNarrative(CFormat(CString(IDS_FMT_DEFINED_BY), pWhat));

	pView->EndGroup(TRUE);

	return TRUE;
	}

BOOL CPrimDataPage::LoadPage(IUICreate *pView, UINT uSub)
{
	CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pData), m_uPage + uSub);

	pPage->LoadIntoView(pView, m_pData);

	delete pPage;

	return TRUE;
	}

BOOL CPrimDataPage::LoadEntry(IUICreate *pView, CDispFormat *pFormat)
{
	if( pFormat->GetFormType() == 1 ) {
		
		LoadPage(pView, 0);

		LoadPage(pView, 8);

		LoadPage(pView, 9);

		return TRUE;
		}

	if( pFormat->GetFormType() == 3 ) {

		LoadPage(pView, 10);

		LoadPage(pView, 8);

		return TRUE;
		}

	if( pFormat->GetFormType() == 7 ) {
				
		CDispFormatLinked *pLinked = (CDispFormatLinked *) pFormat;

		if( pLinked->GetFormat(pFormat) ) {			

			return LoadEntry(pView, pFormat);
			}		
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive Data Block
//

// Dynamic Class

AfxImplementDynamicClass(CPrimData, CPrimBlock);

// Constructor

CPrimData::CPrimData(void)
{
	m_pValue       = NULL;
	m_Entry        = 0;
	m_Flash        = 0;
	m_Layout       = 0;
	m_Content      = 0;
	m_TagLimits    = 1;
	m_TagLabel     = 1;
	m_TagFormat    = 1;
	m_TagColor     = 0;
	m_pEnable      = NULL;
	m_Local        = FALSE;
	m_pValidate    = NULL;
	m_pOnSetFocus  = NULL;
	m_pOnKillFocus = NULL;
	m_pOnComplete  = NULL;
	m_pOnError     = NULL;
	m_pLabel       = NULL;
	m_pLimitMin    = NULL;
	m_pLimitMax    = NULL;
	m_FormType     = 0;
	m_ColType      = 0;
	m_UseBack      = 1;
	m_pFormat      = NULL;
	m_pColor       = NULL;
	m_pTextColor   = New CPrimColor;
	m_pTextShadow  = New CPrimColor;
	m_pKeyTitle    = NULL;
	m_pKeyStatus   = NULL;
	m_KeyNumeric   = 0;
	m_Accel        = 1;
	}

// UI Creation

BOOL CPrimData::OnLoadPages(CUIPageList *pList)
{			 
	CPrimDataPage *pPage1 = New CPrimDataPage(this, CString(IDS_DATA),     1);

	CPrimDataPage *pPage2 = New CPrimDataPage(this, CString(IDS_MORE),     2);
	
	CPrimDataPage *pPage3 = New CPrimDataPage(this, CString(IDS_ENTRY),    3);
	
	CPrimDataPage *pPage4 = New CPrimDataPage(this, CString(IDS_FORMAT_1), 5);
	
	CPrimDataPage *pPage5 = New CPrimDataPage(this, CString(IDS_COLORS),   7);
	
	pList->Append(pPage1);
	
	pList->Append(pPage2);
	
	pList->Append(pPage3);
	
	pList->Append(pPage4);
	
	pList->Append(pPage5);
	
	return TRUE;
	}

// UI Update

void CPrimData::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		LimitTypes(pHost);

		m_pPrev = m_pValue;

		DoEnables(pHost);
		}

	if( Tag == "Value" ) {

		BOOL fMake = FALSE;

		if( m_pValue ) {

			if( !m_pPrev ) {

				fMake = TRUE;
				}

			if( FixTagItems() ) {

				fMake = TRUE;
				}

			if( m_pValue->GetType() == typeString ) {

				// LATER -- Allow string formats...

				if( KillFormat() + KillColor() ) {

					fMake = TRUE;
					}

				m_TagLimits = 1;

				pHost->UpdateUI("TagLimits");
				}
			else {
				// LATER -- Kill string formats...
				}
			}
		else {
			m_TagLimits = 1;
			m_TagLabel  = 1;
			m_TagFormat = 1;
			m_TagColor  = 0;

			fMake = TRUE;
			}

		LimitTypes(pHost);

		KillUnused(pHost);

		UpdateLimitTypes(pHost);

		UpdateChildTypes();

		if( m_Entry && !CanWrite() ) {

			m_Entry = 0;

			pHost->UpdateUI("Entry");
			}

		if( fMake ) {

			pHost->RemakeUI();

			return;
			}

		m_pPrev = m_pValue;

		DoEnables(pHost);
		}

	if( Tag == "Content" ) {

		if( m_Content == 1 && m_Layout == 0 ) {

			m_AlignH = 1;

			pHost->UpdateUI("AlignH");
			}

		pHost->RemakeUI();
		}

	if( Tag == "Layout" ) {

		if( m_Content == 1 && m_Layout == 0 ) {

			m_AlignH = 1;

			pHost->UpdateUI("AlignH");
			}

		DoEnables(pHost);
		}

	if( Tag == "AlignV" ) {

		if( m_AlignV >= 5 && m_AlignV <= 7 ) {

			m_MoveDir = 0;
			}

		if( m_AlignV == 7 ) {

			m_Layout = 1;

			m_Lead   = 0;

			pHost->UpdateUI("Layout");

			pHost->UpdateUI("Lead");
			}

		DoEnables(pHost);
		}

	if( Tag == "TagLimits" ) {

		if( m_TagLimits ) {

			KillCoded(pHost, L"LimitMin", m_pLimitMin);

			KillCoded(pHost, L"LimitMax", m_pLimitMax);
			}
		else {
			CString Name = m_pValue->GetTagName();

			InitCoded(pHost, L"LimitMin", m_pLimitMin, Name + L".Min");

			InitCoded(pHost, L"LimitMax", m_pLimitMax, Name + L".Max");
			}

		pHost->RemakeUI();
		}

	if( Tag == "TagLabel" ) {

		if( m_TagLabel ) {

			KillCoded(pHost, L"Label", m_pLabel);
			}
		else
			CopyTagLabel(pHost);

		pHost->RemakeUI();
		}

	if( Tag == "TagFormat" ) {

		KillFormat();

		if( !m_TagFormat ) {

			CopyTagFormat();
			}

		pHost->RemakeUI();
		}

	if( Tag == "TagColor" ) {

		KillColor();

		if( !m_TagColor ) {

			CopyTagColor();
			}

		pHost->RemakeUI();
		}

	if( Tag == "Entry" ) {

		UpdateType(pHost, L"Value", m_pValue, TRUE);

		pHost->RemakeUI();
		}

	if( Tag == "FormType" ) {
		
		CLASS Class = CDispFormat::GetClass(m_FormType);

		if( SetFormatClass(m_pFormat, Class) ) {

			if( Class == AfxRuntimeClass(CDispFormatTimeDate) ) {
				
				if( !m_pValue ) {

					InitCoded (pHost, L"Value", m_pValue, L"TimeNow");

					OnUIChange(pHost, pItem, L"Value");
					}
				}

			pHost->RemakeUI();
			}
		}

	if( Tag == "ColType" ) {

		CLASS Class = CDispColor::GetClass(m_ColType);

		if( SetColorClass(m_pColor, Class) ) {

			pHost->RemakeUI();
			}
		}

	if( Tag == "KeyNumeric" ) {
		
		DoEnables(pHost);
		}

	CPrimBlock::OnUIChange(pHost, pItem, Tag);
	}

// Attributes

BOOL CPrimData::HasError(void) const
{
	if( HasBrokenCode() ) {

		return TRUE;
		}

	return m_fError;
	}

BOOL CPrimData::CanWrite(void) const
{
	if( m_pValue ) {

		if( m_pValue->GetFlags() & flagWritable ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::IsString(void) const
{
	if( m_pValue ) {

		if( m_pValue->GetType() == typeString ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::IsTagRef(void) const
{
	if( m_pValue ) {

		if( m_pValue->IsTagRef() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::IsUnboundTag(void) const
{
	if( m_pValue ) {

		if( m_pValue->IsTagRef() ) {

			CTag *pTag = NULL;

			if( m_pValue->GetTagItem(pTag) ) {

				return FALSE;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagItem(CTag * &pTag) const
{
	if( IsTagRef() ) {

		if( m_pValue->GetTagItem(pTag) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagLabel(CCodedText * &pLabel) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pLabel ) {

			pLabel = pTag->m_pLabel;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagFormat(CDispFormat * &pFormat) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pFormat ) {

			pFormat = pTag->m_pFormat;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetTagColor(CDispColor * &pColor) const
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pColor ) {

			pColor = pTag->m_pColor;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetDataLabel(CCodedText * &pLabel) const
{
	if( pLabel = m_pLabel ) {

		return TRUE;
		}

	if( m_TagLabel ) {
		
		if( GetTagLabel(pLabel) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetDataFormat(CDispFormat * &pFormat) const
{
	if( pFormat = m_pFormat ) {

		return TRUE;
		}

	if( m_TagFormat ) {
		
		if( GetTagFormat(pFormat) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::GetDataColor(CDispColor * &pColor) const
{
	if( pColor = m_pColor ) {

		return TRUE;
		}

	if( m_TagColor ) {
			
		if( GetTagColor(pColor) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::HasTagLimits(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		if( pTag->IsKindOf(AfxRuntimeClass(CTagString)) ) {

			return TRUE;
			}

		if( pTag->IsKindOf(AfxRuntimeClass(CTagNumeric)) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::HasTagLabel(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::HasTagFormat(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::HasTagColor(void) const
{
	CTag *pTag = NULL;

	if( IsUnboundTag() ) {

		return TRUE;
		}

	if( GetTagItem(pTag) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::UseTagLimits(void) const
{
	return m_pValue && m_TagLimits;
	}

BOOL CPrimData::UseTagLabel(void) const
{
	return m_pValue && m_TagLabel;
	}

BOOL CPrimData::UseTagFormat(void) const
{
	return m_pValue && m_TagFormat;
	}

BOOL CPrimData::UseTagColor(void) const
{
	return m_pValue && m_TagColor;
	}

// Limit Access

DWORD CPrimData::GetMinValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			return pTag->GetMinValue(Type);
			}
		}

	if( m_pLimitMin ) {

		return m_pLimitMin->Execute(Type);
		}

	if( Type == typeReal ) {

		return R2I(0);
		}

	return 0;
	}

DWORD CPrimData::GetMaxValue(UINT Type) const
{
	if( m_pValue && m_TagLimits ) {

		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			return pTag->GetMaxValue(Type);
			}
		}

	if( m_pLimitMax ) {

		return m_pLimitMax->Execute(Type);
		}


	if( Type == typeReal ) {

		return R2I(100);
		}

	return 100;
	}

BOOL CPrimData::GetBoundingRect(R2 &Rect) const
{
	if( !m_fError ) {

		if( !GetSizingText().IsEmpty() ) {

			m_Flow.GetBoundingRect(Rect);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::AddBoundingRect(R2 &Rect) const
{
	if( !m_fError ) {

		m_Flow.AddBoundingRect(Rect);

		return TRUE;
		}

	return FALSE;
	}

// Sizing Text

CString CPrimData::GetSizingText(void) const
{
	DWORD Data = 0;

	UINT  Type = typeVoid;

	if( m_pValue ) {

		Data = m_pValue->ExecVal();

		Type = m_pValue->GetType();
		}

	if( m_Content == 1 ) {

		CString Label = FindLabelText() + L':';

		CString Value = FindValueText(Data, Type);

		return Label + Value + L' ';
		}

	if( m_Content == 2 ) {

		return FindLabelText();
		}
	
	return FindValueText(Data, Type);
	}

// Operations

BOOL CPrimData::Draw(IGDI *pGDI, R2 Rect, UINT uMode)
{
	m_fError = FALSE;

	FindCtx(m_Ctx);

	SelectFont(pGDI);

	Rect.left   += m_Margin.left;

	Rect.right  -= m_Margin.right;
	
	Rect.top    += m_Margin.top;
	
	Rect.bottom -= m_Margin.bottom;

	switch( m_Layout ) {

		case 0:
			DrawSingle(pGDI, Rect, uMode);

			break;

		case 1:
			DrawMulti(pGDI, Rect, uMode);
			
			break;
		}

	return !m_fError;
	}

void CPrimData::SetTextColor(COLOR Color)
{
	m_pTextColor->Set(Color);
	}

void CPrimData::SetContent(UINT Content)
{
	if( Content == 1 && m_Layout == 0 ) {

		m_AlignH = 1;
		}

	m_Content = Content;
	}

void CPrimData::SetEntry(UINT Entry)
{
	m_Entry = Entry;

	UpdateType(NULL, L"Value", m_pValue, TRUE);
	}

void CPrimData::Set(CString Text)
{
	InitCoded(NULL, L"Value", m_pValue, Text);

	if( FixTagItems() ) {

		if( !m_TagLabel ) {

			CString Code = L'"' + Text.Left(Text.Find('(')) + L'"';

			InitCoded(NULL, L"Label", m_pLabel, Code);
			}
		}

	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"G3Prims");

	Reg.MoveTo(L"Defaults");

	COLOR Fore = COLOR(Reg.GetValue(L"DataText", 0x7FFF));

	m_pTextColor->Set(Fore);
	}

void CPrimData::Validate(BOOL fExpand)
{
	if( m_pValue ) {

		m_pValue->Recompile(fExpand);

		if( m_Entry && !CanWrite() ) {

			m_pValue->SetBroken();
			}

		UpdateLimitTypes(NULL);

		UpdateChildTypes();
		}
	}

void CPrimData::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	if( m_pValue ) {

		if( m_pValue->IsTagRef() ) {

			UINT uTag = m_pValue->GetTagIndex();

			if( !Tags.Failed(Tags.Find(uTag)) ) {

				Validate(TRUE);

				Done.Insert(m_pValue);
				}
			}
		}
	}

BOOL CPrimData::StepAddress(void)
{
	if( m_pValue ) {

		CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

		CString Text = m_pValue->GetSource(TRUE);

		CString Prev = Text;

		if( pSystem->StepFragment(Text, 1) ) {

			if( m_pValue->Compile(Text) ) {

				if( m_Entry && !CanWrite() ) {

					m_pValue->Compile(Prev);

					return FALSE;
					}

				if( !CheckTagItems() ) {

					m_pValue->Compile(Prev);

					return FALSE;
					}

				return TRUE;
				}
			}
		}

	return FALSE;
	}

void CPrimData::SetTimeDate(void)
{
	CLASS Class = CDispFormat::GetClass(m_FormType = 3);

	if( SetFormatClass(m_pFormat, Class) ) {

		m_TagLimits = 0;

		m_TagLabel  = 0;
		
		m_TagFormat = 0;
		
		m_TagColor  = 0;
		
		if( !m_pValue ) {

			InitCoded (NULL, L"Value", m_pValue, L"TimeNow");
			}
		}
	}

// Type Access

BOOL CPrimData::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Value" ) {

		if( m_Entry ) {

			Type.m_Type  = typeVoid;

			Type.m_Flags = flagInherent | flagWritable;

			return TRUE;
			}

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagInherent | flagSoftWrite;
	
		return TRUE;
		}

	if( Tag == "SubValue" || Tag == "LimitMin" || Tag == "LimitMax" ) {

		Type.m_Type  = m_pValue ? m_pValue->GetType() : typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Label" ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"On") ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	if( Tag.StartsWith(L"Key") ) {

		Type.m_Type  = typeString;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Validate" ) {

		Type.m_Type  = typeLogical;

		Type.m_Flags = flagInherent;

		return TRUE;
		}

	return FALSE;
	}

// Persistance

void CPrimData::Init(void)
{
	CPrimBlock::Init();

	m_pTextColor->Set(GetRGB(31,31,31));
	}

// Download Support

BOOL CPrimData::MakeInitData(CInitData &Init)
{
	CPrimBlock::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);

	Init.AddByte(BYTE(m_Entry));
	Init.AddByte(BYTE(m_Flash));
	Init.AddByte(BYTE(m_Layout));
	Init.AddByte(BYTE(m_Content));

	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		Init.AddWord(WORD(pTag->GetIndex() + 1));

		Init.AddByte(BYTE(m_TagLimits));
		Init.AddByte(BYTE(m_TagLabel));
		Init.AddByte(BYTE(m_TagFormat));
		Init.AddByte(BYTE(m_TagColor));
		}
	else
		Init.AddWord(WORD(0));

	if( m_Entry ) {

		Init.AddItem(itemVirtual, m_pEnable);
		Init.AddItem(itemVirtual, m_pValidate);
		Init.AddItem(itemVirtual, m_pOnSetFocus);
		Init.AddItem(itemVirtual, m_pOnKillFocus);
		Init.AddItem(itemVirtual, m_pOnComplete);
		Init.AddItem(itemVirtual, m_pOnError);
		
		Init.AddByte(BYTE(m_Local));

		Init.AddItem(itemVirtual, m_pKeyTitle);
		Init.AddItem(itemVirtual, m_pKeyStatus);
		
		Init.AddByte(BYTE(m_KeyNumeric));
		Init.AddByte(BYTE(m_Accel));
		}

	Init.AddItem(itemVirtual, m_pLabel);
	Init.AddItem(itemVirtual, m_pLimitMin);
	Init.AddItem(itemVirtual, m_pLimitMax);

	Init.AddItem(itemVirtual, m_pFormat);
	Init.AddItem(itemVirtual, m_pColor);

	if( !m_pColor ) {
	
		Init.AddItem(itemSimple,  m_pTextColor);
		Init.AddItem(itemSimple,  m_pTextShadow);
		}

	Init.AddByte(BYTE(m_UseBack));

	return TRUE;
	}

// Meta Data

void CPrimData::AddMetaData(void)
{
	CPrimBlock::AddMetaData();

	Meta_AddVirtual(Value);
	Meta_AddInteger(Entry);
	Meta_AddInteger(Flash);
	Meta_AddInteger(Layout);
	Meta_AddInteger(Content);
	Meta_AddInteger(TagLimits);
	Meta_AddInteger(TagLabel);
	Meta_AddInteger(TagFormat);
	Meta_AddInteger(TagColor);
	Meta_AddVirtual(Enable);
	Meta_AddInteger(Local);
	Meta_AddVirtual(Validate);
	Meta_AddVirtual(OnSetFocus);
	Meta_AddVirtual(OnKillFocus);
	Meta_AddVirtual(OnComplete);
	Meta_AddVirtual(OnError);
	Meta_AddVirtual(Label);
	Meta_AddVirtual(LimitMin);
	Meta_AddVirtual(LimitMax);
	Meta_AddInteger(FormType);
	Meta_AddInteger(ColType);
	Meta_AddInteger(UseBack);
	Meta_AddVirtual(Format);
	Meta_AddVirtual(Color);
	Meta_AddObject (TextColor);
	Meta_AddObject (TextShadow);
	Meta_AddVirtual(KeyTitle);
	Meta_AddVirtual(KeyStatus);
	Meta_AddInteger(KeyNumeric);
	Meta_AddInteger(Accel);

	Meta_SetName((IDS_DATA));
	}

// Type Updates

void CPrimData::UpdateLimitTypes(IUIHost *pHost)
{
	UpdateType(pHost, L"LimitMin", m_pLimitMin, TRUE);

	UpdateType(pHost, L"LimitMax", m_pLimitMax, TRUE);
	}

void CPrimData::UpdateChildTypes(void)
{
	if( m_pFormat ) {

		m_pFormat->UpdateTypes(TRUE);
		}

	if( m_pColor ) {

		m_pColor->UpdateTypes(TRUE);
		}
	}

// Child Updates

BOOL CPrimData::SetFormatClass(CDispFormat * &pFormat, CLASS Class)
{
	CDispFormat * pOld = pFormat;

	if( AfxPointerClass(pOld) == Class ) {

		return FALSE;
		}

	if( Class ) {

		pFormat = AfxNewObject(CDispFormat, Class);

		pFormat->SetParent(this);

		pFormat->Init();

		if( pOld ) {

			pFormat->Preserve(pOld);
			}
		}
	else
		pFormat = NULL;

	if( pOld ) {

		pOld->Kill();

		delete pOld;
		}

	return TRUE;
	}

BOOL CPrimData::SetColorClass(CDispColor * &pColor, CLASS Class)
{
	CDispColor * pOld = pColor;

	if( AfxPointerClass(pOld) == Class ) {

		return FALSE;
		}

	if( Class ) {

		pColor = AfxNewObject(CDispColor, Class);

		pColor->SetParent(this);

		pColor->Init();

		if( pOld ) {

			pColor->Preserve(pOld);
			}
		}
	else
		pColor = NULL;

	if( pOld ) {

		pOld->Kill();

		delete pOld;
		}

	return TRUE;
	}

// Copying From Tag

BOOL CPrimData::CopyTagLabel(IUIHost *pHost)
{
	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		if( pTag->m_pLabel ) {

			CString Text = pTag->m_pLabel->GetSource(TRUE);

			InitCoded(pHost, L"Label", m_pLabel, Text);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimData::CopyTagFormat(void)
{
	CDispFormat *pFormat = NULL;

	if( GetTagFormat(pFormat) ) {

		m_pFormat  = (CDispFormat *) CItem::MakeFromItem(this, pFormat);

		m_FormType = m_pFormat ? m_pFormat->GetFormType() : 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::CopyTagColor(void)
{
	CDispColor *pColor = NULL;

	if( GetTagColor(pColor) ) {

		m_pColor  = (CDispColor *) CItem::MakeFromItem(this, pColor);

		m_ColType = m_pColor ? m_pColor->GetColType() : 0;

		return TRUE;
		}

	return FALSE;
	}

// Text Extraction

CString CPrimData::FindLabelText(void) const
{
	CCodedText *pLabel;

	if( GetDataLabel(pLabel) ) {

		DWORD n = 0;

		if( m_pValue ) {

			CDataRef &Ref = (CDataRef &) n;

			n = m_pValue->Execute(typeLValue);

			// cppcheck-suppress redundantAssignment

			n = Ref.x.m_Array;
			}

		return pLabel->GetText(&n);
		}
	else {
		CTag *pTag = NULL;

		if( GetTagItem(pTag) ) {

			CString Name = pTag->m_Name.TokenLast('.');

			return Name;
			}
		}

	return L"Label";
	}

CString CPrimData::FindValueText(DWORD Data, UINT Type) const
{
	CDispFormat *pFormat = NULL;

	CString      Value   = L"";

	WCHAR        cHair   = spaceHair;

	if( GetDataFormat(pFormat) ) {

		Value = pFormat->Format(Data, Type, fmtPad);
		}
	else
		Value = CDispFormat::GeneralFormat(Data, Type, TRUE);

	if( m_Entry ) {

		Value = cHair + Value + cHair;
		}

	return Value;
	}

// Context Creation

void CPrimData::FindCtx(CCtx &Ctx)
{
	DWORD Data   = 0;

	UINT  Type   = typeNumeric;

	if( m_pValue ) {

		Data = m_pValue->ExecVal();

		Type = m_pValue->GetType();
		}

	if( m_Content >= 1 ) {

		Ctx.m_Label = FindLabelText();
		}

	if( m_Content <= 1 ) {
	
		Ctx.m_Value = FindValueText(Data, Type);
		}

	CDispColor *pColor;

	if( GetDataColor(pColor) ) {

		DWORD Pair = pColor->GetColorPair(Data, Type);

		if( m_UseBack ) {

			Ctx.m_Fore1 = LOWORD(Pair);

			Ctx.m_Back1 = HIWORD(Pair);
			}
		else {
			Ctx.m_Fore1 = LOWORD(Pair);

			Ctx.m_Back1 = 0x8000;
			}
		
		if( m_Entry ) {

			Ctx.m_Fore2 = HIWORD(Pair);

			Ctx.m_Back2 = LOWORD(Pair);
			}

		Ctx.m_Shadow = 0x8000;
		}
	else {
		if( m_Entry ) {

			Ctx.m_Fore1  = m_pTextColor->GetColor();

			Ctx.m_Back1  = 0x8000;

			Ctx.m_Fore2  = FindCompColor(Ctx.m_Fore1);

			Ctx.m_Back2  = Ctx.m_Fore1;

			Ctx.m_Shadow = m_pTextShadow->GetColor();
			}
		else {
			Ctx.m_Fore1  = m_pTextColor->GetColor();

			Ctx.m_Back1  = 0x8000;

			Ctx.m_Shadow = m_pTextShadow->GetColor();
			}
		}

	if( !m_Entry ) {

		Ctx.m_Fore2 = Ctx.m_Fore1;

		Ctx.m_Back2 = Ctx.m_Back1;
		}

	if( !m_Entry ) {

		Ctx.m_fFocus = FALSE;

		Ctx.m_uPress = 0;
		}
	else {
		Ctx.m_fFocus = TRUE;

		Ctx.m_uPress = IsPressed();
		}

	FreeData(Data, Type);
	}

// Drawing Helpers

void CPrimData::DrawSingle(IGDI *pGDI, R2 Rect, UINT uMode)
{
	BOOL  fMove  = m_Ctx.m_uPress;

	BOOL  fFocus = m_Ctx.m_fFocus;

	BOOL  fMoveX = fMove && m_MoveDir > 1;

	BOOL  fMoveY = fMove && m_MoveDir > 0;

	int   xOrg   = Rect.x1;

	int   yOrg   = Rect.y1;

	int   xRect  = Rect.x2 - Rect.x1;

	int   yRect  = Rect.y2 - Rect.y1;

	int   yEdit  = m_Entry ? 1 : 0;

	int   ySize  = pGDI->GetTextHeight(L"X");
			
	int   yPos   = m_AlignV < 5 ? max(0, (m_AlignV * (yRect - ySize - yEdit)) / 4) + yEdit : yEdit;

	if( fMoveY ) {

		yPos += m_MoveStep;
		}

	if( m_Content == 1 ) {

		CUnicode Label = UniVisual(m_Ctx.m_Label) + L':';

		CUnicode Value = UniVisual(m_Ctx.m_Value);

		int xSize1 = pGDI->GetTextWidth(Label);

		int xSize2 = pGDI->GetTextWidth(Value);

		int xPos1  = Rect.x1 + 2;

		int xPos2  = Rect.x2 - 2 - xSize2;

		if( fMoveX ) {

			xPos1 += m_MoveStep;

			xPos2 += m_MoveStep;
			}

		if( yPos < 0 || yPos + ySize > yRect || xPos2 < xOrg || xPos1 + xSize1 > xOrg + xRect ) {

			if( uMode == drawWhole || uMode == drawDialog ) {

				m_fError = TRUE;
				}
			}
		
		if( m_Ctx.m_Shadow < 0x8000 ) {

			if( !fFocus ) {

				SetColors(pGDI, 3);

				pGDI->TextOut(xPos1 + 1, yOrg + yPos + 1, Label);

				pGDI->TextOut(xPos2 + 1, yOrg + yPos + 1, Value);
				}
			else {
				SetColors(pGDI, 3);

				pGDI->TextOut(xPos1 + 1, yOrg + yPos + 1, Label);
				}
			}

		if( TRUE ) {

			SetColors(pGDI, 1);

			if( m_Entry ) {

				pGDI->FillRect( xPos1,
						yOrg  + yPos - 1,
						xPos2,
						yOrg  + yPos
						);
				}

			pGDI->TextOut(xPos1, yOrg + yPos, Label);
			}

		if( xPos1 + xSize1 < xPos2 ) {

			pGDI->FillRect( xPos1 + xSize1,
					yOrg  + yPos - yEdit,
					xPos2,
					yOrg  + yPos + ySize
					);
			}

		if( TRUE ) {

			SetColors(pGDI, 2);

			if( m_Entry ) {

				pGDI->FillRect( xPos2,
						yOrg  + yPos - 1,
						xPos2 + xSize2,
						yOrg  + yPos
						);
				}

			pGDI->TextOut(xPos2, yOrg + yPos, Value);
			}

		m_Flow.SetBoundingRect(Rect.x1, yOrg + yPos, Rect.x2 - Rect.x1, ySize);
		}
	else {
		CString Line  = UniVisual(m_Content ? m_Ctx.m_Label : m_Ctx.m_Value);

		int     xSize = pGDI->GetTextWidth(Line);

		int     xPos  = (m_AlignH * (xRect - xSize)) / 2;

		if( fMoveX ) {

			xPos += m_MoveStep;
			}

		if( yPos < 0 || yPos + ySize > yRect || xPos < 0 || xPos + xSize > xRect ) {

			if( uMode == drawWhole || uMode == drawDialog ) {

				m_fError = TRUE;
				}
			}

		if( m_Ctx.m_Shadow < 0x8000 ) {

			if( !fFocus ) {

				SetColors(pGDI, 3);

				pGDI->TextOut(xOrg + xPos + 1, yOrg + yPos + 1, Line);
				}
			}

		if( TRUE ) {

			SetColors(pGDI, 2);

			if( fFocus ) {

				pGDI->FillRect( xOrg + xPos,
						yOrg + yPos - 1,
						xOrg + xPos + xSize,
						yOrg + yPos
						);
				}

			pGDI->TextOut(xOrg + xPos, yOrg + yPos, Line);
			}

		m_Flow.SetBoundingRect(xOrg + xPos, yOrg + yPos, xSize, ySize);
		}
	}

void CPrimData::DrawMulti(IGDI *pGDI, R2 Rect, UINT uMode)
{
	CString Text;

	if( m_Content == 1 ) {

		Text += UniVisual(m_Ctx.m_Label);

		if( m_AlignV != 7 ) {

			Text += L':';
			}

		Text += '|';

		Text += UniVisual(m_Ctx.m_Value);
		}
	else {
		if( m_Content ) {

			Text = UniVisual(m_Ctx.m_Label);
			}
		else
			Text = UniVisual(m_Ctx.m_Value);
		}

	m_Flow.Flow(pGDI, Text, Rect);

	CTextFlow::CFormat Fmt;

	Fmt.m_AlignH   = m_AlignH;
	
	Fmt.m_AlignV   = m_AlignV;
	
	Fmt.m_Lead     = m_Lead;
	
	Fmt.m_Color    = m_Ctx.m_Fore1;
	
	Fmt.m_Shadow   = m_Ctx.m_Shadow;
	
	Fmt.m_fMove    = CanMove() && IsPressed();
	
	Fmt.m_MoveDir  = m_MoveDir;
	
	Fmt.m_MoveStep = m_MoveStep;

	if( !m_Flow.Draw(pGDI, Fmt) ) {

		m_fError = TRUE;
		}
	}

// Color Setting

void CPrimData::SetColors(IGDI *pGDI, UINT uMode)
{
	switch( uMode ) {

		case 1:
			SetColors(pGDI, m_Ctx.m_Fore1, m_Ctx.m_Back1);
			break;

		case 2:
			SetColors(pGDI, m_Ctx.m_Fore2, m_Ctx.m_Back2);
			break;

		case 3:
			SetColors(pGDI, m_Ctx.m_Shadow, 0x8000);
			break;
		}
	}

void CPrimData::SetColors(IGDI *pGDI, COLOR Fore, COLOR Back)
{
	pGDI->SetTextFore(Fore);

	if( Back >= 0x8000 ) {

		pGDI->SetTextTrans(modeTransparent);

		pGDI->SelectBrush(brushNull);
		}
	else {
		pGDI->SetTextBack(Back);

		pGDI->SetTextTrans(modeOpaque);

		pGDI->SelectBrush(brushFore);

		pGDI->SetBrushFore(Back);
		}
	}

// Implementation

COLOR CPrimData::FindCompColor(COLOR c)
{
	BYTE  r = GetRED  (c);
	BYTE  g = GetGREEN(c);
	BYTE  b = GetBLUE (c);

	BYTE  i = BYTE((14*r + 45*g + 5*b) / 64);

	if( i >= 15 ) {

		return GetRGB(0,0,0);
		}

	return GetRGB(31,31,31);
	}

void CPrimData::LimitTypes(IUIHost *pHost)
{
	if( IsString() ) {

		LimitEnum(pHost, L"FormType", m_FormType, 0x103);

		LimitEnum(pHost, L"ColType",  m_ColType,  0x007);
		}
	else {
		LimitEnum(pHost, L"FormType", m_FormType, 0x0FF);

		LimitEnum(pHost, L"ColType",  m_ColType,  0x01F);
		}
	}

void CPrimData::KillUnused(IUIHost *pHost)
{
	if( UseTagLimits() ) {

		KillCoded(pHost, L"LimitMin", m_pLimitMin);

		KillCoded(pHost, L"LimitMax", m_pLimitMax);
		}

	if( UseTagLabel() ) {

		KillCoded(pHost, L"Label", m_pLabel);
		}

	if( UseTagFormat() ) {

		KillFormat();
		}

	if( UseTagColor() ) {

		KillColor();
		}
	}

BOOL CPrimData::KillFormat(void)
{
	if( m_pFormat ) {

		m_pFormat->Kill();

		delete m_pFormat;

		m_pFormat  = NULL;

		m_FormType = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::KillColor(void)
{
	if( m_pColor ) {

		m_pColor->Kill();

		delete m_pColor;

		m_pColor  = NULL;

		m_ColType = 0;

		return TRUE;
		}

	return FALSE;
	}

BOOL CPrimData::CheckTagItems(void)
{
	if( m_Entry ) {

		if( m_TagLimits && !HasTagLimits() ) {

			return FALSE;
			}
		}

	if( m_TagLabel && !HasTagLabel() ) {

		return FALSE;
		}

	if( m_TagFormat && !HasTagFormat() ) {

		return FALSE;
		}

	if( m_TagColor && !HasTagColor() ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CPrimData::FixTagItems(void)
{
	BOOL fChange = FALSE;

	if( m_TagLimits && !HasTagLimits() ) {

		m_TagLimits = 0;

		fChange     = TRUE;
		}

	if( m_TagLabel && !HasTagLabel() ) {

		m_TagLabel = 0;

		fChange    = TRUE;
		}

	if( m_TagFormat && !HasTagFormat() ) {

		m_TagFormat = 0;

		fChange     = TRUE;
		}

	if( m_TagColor && !HasTagColor() ) {

		m_TagColor = 0;

		fChange    = TRUE;
		}

	return fChange;
	}

void CPrimData::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("Layout",      !m_Entry && m_AlignV != 7);

	pHost->EnableUI("Entry",       CanWrite() && CanEdit() && !m_Layout);

	pHost->EnableUI("TagLimits",   m_Entry && HasTagLimits() && !IsString());

	pHost->EnableUI("TagLabel",    (m_Content > 0 || m_Entry) && HasTagLabel());

	pHost->EnableUI("TagFormat",   (m_Content < 2 || m_Entry) && HasTagFormat());
	
	pHost->EnableUI("TagColor",    HasTagColor());
	
	pHost->EnableUI("AlignH",      m_Content != 1 || m_Layout);

	pHost->EnableUI("AlignV",      TRUE);

	pHost->EnableUI("Lead",        m_Layout && m_AlignV != 7);

	pHost->EnableUI("LimitMin",    m_Entry);

	pHost->EnableUI("LimitMax",    m_Entry);

	pHost->EnableUI("Enable",      m_Entry);
						
	pHost->EnableUI("Local",       m_Entry);	

	pHost->EnableUI("Validate",    m_Entry);
	
	pHost->EnableUI("OnSetFocus",  m_Entry);
	
	pHost->EnableUI("OnKillFocus", m_Entry);
	
	pHost->EnableUI("OnComplete",  m_Entry);
	
	pHost->EnableUI("OnError",     m_Entry);

	pHost->EnableUI("KeyTitle",    m_Entry);

	pHost->EnableUI("KeyStatus",   m_Entry);

	pHost->EnableUI("KeyNumeric",  m_Entry);
	
	pHost->EnableUI("Accel",       m_Entry && m_KeyNumeric > 1);
	
	pHost->EnableUI("Label",       (m_Content > 0 || m_Entry) && !UseTagLabel());

	pHost->EnableUI("FormType",    !UseTagFormat());

	pHost->EnableUI("ColType",     !UseTagColor());
	}

// End of File
