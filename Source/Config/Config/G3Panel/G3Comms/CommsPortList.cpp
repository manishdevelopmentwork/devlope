
#include "Intern.hpp"

#include "CommsPortList.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsDevice.hpp"
#include "CommsPort.hpp"
#include "CommsSysBlock.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Comms Port List
//

// Dynamic Class

AfxImplementDynamicClass(CCommsPortList, CItemIndexList);

// Constructor

CCommsPortList::CCommsPortList(void)
{
	m_Class  = NULL;

	m_uLimit = 100;
	}

CCommsPortList::CCommsPortList(UINT uLimit)
{
	m_Class  = NULL;

	m_uLimit = uLimit;
	}

// Item Access

CCommsPort * CCommsPortList::GetItem(INDEX Index) const
{
	CCommsPort *pPort = (CCommsPort *) CItemIndexList::GetItem(Index);

	if( pPort ) {

		ZdiAssert(pPort->IsKindOf(AfxRuntimeClass(CCommsPort)));
		}

	return pPort;
	}

CCommsPort * CCommsPortList::GetItem(UINT uPort) const
{
	CCommsPort *pPort = (CCommsPort *) CItemIndexList::GetItem(uPort);

	if( pPort ) {

		ZdiAssert(pPort->IsKindOf(AfxRuntimeClass(CCommsPort)));
		}

	return pPort;
	}

// Device Access

CCommsPort * CCommsPortList::FindPort(UINT uPort) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsPort *pPort = GetItem(n);

		if( pPort->m_Number == uPort ) {

			return pPort;
			}

		GetNext(n);
		}

	return NULL;
	}

CCommsPort * CCommsPortList::FindPort(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsPort *pPort = GetItem(n);

		if( pPort->GetShortName() == Name ) {

			return pPort;
			}

		GetNext(n);
		}

	return NULL;
	}

CCommsDevice * CCommsPortList::FindDevice(UINT uDevice) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsDevice *pDevice;

		if( pDevice = GetItem(n)->FindDevice(uDevice) ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

CCommsDevice * CCommsPortList::FindDevice(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsDevice *pDevice;

		if( pDevice = GetItem(n)->FindDevice(Name) ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

CCommsSysBlock * CCommsPortList::FindBlock(UINT uBlock) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CCommsSysBlock *pBlock;

		if( pBlock = GetItem(n)->FindBlock(uBlock) ) {

			return pBlock;
			}

		GetNext(n);
		}

	return NULL;
	}

// Attributes

BOOL CCommsPortList::HasRoom(void) const
{
	return GetItemCount() < m_uLimit;
	}

CString CCommsPortList::MakeUnique(CString Format) const
{
	for( UINT n = 1;; n++ ) {

		CPrintf Name(Format, n);

		INDEX i = GetHead();

		while( !Failed(i) ) {

			CCommsPort *pPort = GetItem(i);

			if( pPort->m_Name == Name ) {

				break;
				}

			GetNext(i);
			}

		if( !Failed(i) ) {

			continue;
			}

		return Name;
		}
	}

// Operations

void CCommsPortList::Validate(BOOL fExpand)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsPort *pPort = GetItem(n);

		pPort->Validate(fExpand);
		}
	}

void CCommsPortList::ClearSysBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsPort *pPort = GetItem(n);

		pPort->ClearSysBlocks();
		}
	}

void CCommsPortList::NotifyInit(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsPort *pPort = GetItem(n);

		pPort->NotifyInit();
		}
	}

void CCommsPortList::CheckMapBlocks(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsPort *pPort = GetItem(n);

		pPort->CheckMapBlocks();
		}
	}

void CCommsPortList::CheckConflicts(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsPort *pPort = GetItem(n);

		if( pPort->IsBroken() ) {

			GetDatabase()->SetRecomp();

			break;
			}
		}
	}

// Conversion

void CCommsPortList::PostConvert(void)
{
	for( INDEX n = GetHead(); !Failed(n); GetNext(n) ) {

		CCommsPort *pPort = GetItem(n);

		pPort->PostConvert();
		}
	}

// End of File
