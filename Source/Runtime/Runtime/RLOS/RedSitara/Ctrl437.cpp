
#include "Intern.hpp"

#include "Ctrl437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Control Module
//

// Register Access

#define Reg(x) (m_pBase[reg##x])

// Constructor

CCtrl437::CCtrl437(void)
{
	m_pBase = PVDWORD(ADDR_CONTROL_MODULE);
	}

// Operations

DWORD CCtrl437::GetRevsion(void) const
{
	return Reg(REVISION);
	}

DWORD CCtrl437::GetDeviceId(void) const
{
	return Reg(DEVICEID);
	}

DWORD CCtrl437::GetMaxFreq(void) const
{
	DWORD Data = Reg(DEVICEATTR) & 0x0FFF;

	switch( Data ) {

		case 0xFFE: return  300000000;
		case 0xFE2: return  800000000;
		case 0xFC2: return 1000000000;
		}

	return 0;
	}

DWORD CCtrl437::GetRefFreq(void) const
{
	DWORD dwSysBoot = (Reg(STATUS) >> 22) & 0x00000003;

	switch( dwSysBoot ) { 

		case 0: return 19200000;
		case 1: return 24000000;
		case 2: return 25000000;
		case 3: return 26000000;
		}

	return 0;
	}

DWORD CCtrl437::GetOcrFreq(void) const
{
	return 32768;
	}

// DDR

void CCtrl437::EnableVtp(bool fEnable)
{
	if( fEnable ) {
		
		Reg(CTRLVTP) |=  Bit(6);

		Reg(CTRLVTP) &= ~Bit(0);

		Reg(CTRLVTP) |=  Bit(0);

		while( !(Reg(CTRLVTP) & Bit(5)) );
		}
	else {
		Reg(CTRLVTP) &= ~Bit(6);
		}
	}

void CCtrl437::SetDdrAddrIO(UINT uClockS, UINT uClockI, UINT uCmdS, UINT uCmdI)
{
	Reg(DDRADDRIOCTRL) = (uClockS << 8) | (uClockI << 5) | (uCmdS << 3) | (uCmdI << 0);
	}

void CCtrl437::SetDdrAddrCtrl(DWORD dwMask)
{
	Reg(DDRADDRIOCTRLWD0) = dwMask;

	Reg(DDRADDRIOCTRLWD1) = dwMask;
	}

void CCtrl437::SetDdrDataIO(UINT uClockS, UINT uClockI, UINT uDataS, UINT uDataI)
{
	DWORD dwData = (uClockS << 8) | (uClockI << 5) | (uDataS << 3) | (uDataI << 0);

	Reg(DDRDATA0IOCTRL) = dwData;
	Reg(DDRDATA1IOCTRL) = dwData;
	Reg(DDRDATA2IOCTRL) = dwData;
	Reg(DDRDATA3IOCTRL) = dwData;
	}

void CCtrl437::SetDdrIO(bool fResetEn)
{
	Reg(DDRIOCTRL) = fResetEn ? 0 : Bit(31); 
	}

void CCtrl437::SetDdrClockeEn(bool fEnable)
{
	Reg(DDRCKE) = fEnable ? 3 : 0; 
	}

// EMI

void CCtrl437::SetEmiPhyTraining(UINT uSamples, bool fLogic, bool fAllDqs)
{
	DWORD dwMask = 0x0000F000;

	DWORD dwData = Reg(EMISDRAMCONFIG);
	
	dwData &= ~dwMask;
	
	dwData |= ((uSamples == 128 ? 3 : 0) << 14);
	dwData |= (fLogic ? Bit(13) : 0);
	dwData |= (fAllDqs ? Bit(12) : 0);

	Reg(EMISDRAMCONFIG) = dwData;
	}

void CCtrl437::SetEmiPhyConfig(UINT uDrive, bool fPhyEn1, bool fPhyEn0)
{
	DWORD dwMask = 0x00000063;

	DWORD dwData = Reg(EMISDRAMCONFIG);
	
	dwData &= ~dwMask;
	dwData |= Bit(8);
	dwData |= (uDrive << 5);
	dwData |= (fPhyEn1 ? Bit(1) : 0);
	dwData |= (fPhyEn0 ? Bit(0) : 0);

	Reg(EMISDRAMCONFIG) = dwData;
	}

// I/O Mux

void CCtrl437::SetMux(UINT uPin, DWORD dwConfig)
{
	if( uPin >= pinGPMCAD0 && uPin <= pinUSB1DRVVBUS ) {

		m_pBase[uPin] = dwConfig;
		}
	}

void CCtrl437::SetMux(PCDWORD pList, UINT uSize)
{
	for( UINT i = 0; i < uSize; i += 2 ) {

		SetMux(pList[i+0], pList[i+1]);
		}
	}

DWORD CCtrl437::GetMux(UINT uPin)
{
	if( uPin >= pinGPMCAD0 && uPin <= pinUSB1DRVVBUS ) {

		return m_pBase[uPin];
		}

	return 0;
	}

// MII

void CCtrl437::SetMIIMode(UINT uPort, UINT Mode)
{
	BYTE Mask1 = 0x03;

	BYTE Mask2 = 0x50;

	BYTE Ctrl  = 0x00;

	if( Mode == modeRMII ) {

		Ctrl |= Bit(6);
		}
	
	if( uPort == 1 ) {

		Mask1 <<= 2;

		Mask2 <<= 1;

		Mode  <<= 2;

		Ctrl  <<= 1;
		}

	Reg(GMIISEL) &= ~(Mask1 | Mask2);

	Reg(GMIISEL) |=  (Mode | Ctrl);
	}

// USB 

void CCtrl437::SetPhyPower(UINT uSelect, bool fOn)
{
	DWORD volatile &Reg = m_pBase[regCTRLUSB + 2 * uSelect];

	if( fOn ) {

		Reg &= ~0x00000003;

		Reg |= Bit(19);
		}
	else {
		Reg |= 0x00000003;

		Reg &= ~Bit(19);
		}
	}

// PWM

void CCtrl437::SetPWM(UINT uModule, bool fEnable)
{
	UINT uBit = uModule > 2 ? uModule + 1 : uModule;
	
	if( fEnable ) {

		if( uModule == 3 ) {

			Reg(PWMSS) |= Bit(3);
			}

		Reg(PWMSS) |= Bit(uBit);
		}
	else {		
		if( uModule == 3 ) {

			Reg(PWMSS) &= ~Bit(3);
			}

		Reg(PWMSS) &= ~Bit(uBit);
		}
	}

// End of File
