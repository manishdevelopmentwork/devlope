
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "c2comm.hxx"

#include "intern.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Translations
//

#if !defined(C3_VERSION)

LANGUAGE LANG_FRENCH, SUBLANG_FRENCH

#include "rlc.fr"

LANGUAGE LANG_GERMAN, SUBLANG_GERMAN

#include "rlc.ge"

LANGUAGE LANG_ITALIAN, SUBLANG_ITALIAN

#include "rlc.it"

LANGUAGE LANG_SPANISH, SUBLANG_SPANISH

#include "rlc.sp"

LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US

#endif

//////////////////////////////////////////////////////////////////////////
//
// UI for CRlcDeviceOptions
//

CRlcDeviceOptionsUIList RCDATA
BEGIN
	"Drop,Drop Number,,CUIEditInteger,|0||0|99,"
	"Indicate the node address of the destination device"
	"\0"

	"Device,Red Lion Device,,CUIDropDown,C48|Legend/Legend Plus|PAX|PAXI/PAXCK/PAXDP|T48/P48|TCU/PCU|CUB5/LD,"
	"Select the model type of the destination device."
	"\0"

	"Cub5,CUB5,,CUIDropDown,CUB5R/CUB5B/LD/LDT|CUB5TR/CUB5TB|CUB5I/P/TC/V/RT (Analog)/LDA,"
	"Select the model type of the CUB5."
	"\0"

	"Protocol,Protocol Mode,,CUIDropDown,Full|Abbreviated,"
	"Select Full if Serial Abbreviate Mnemonics in Red Lion meter is set to No."
	"\0"

	"StdDlyRead,Register Read Delay,,CUIEditInteger,|0||0|9999,"
	"Specify the number of mSec that the standard read delay should be based upon.  "
	"A standard read is defined as any transmit value request that begins with the T "
	"command."
	"\0"

	"StdDlyWrite,Register Write Delay,,CUIEditInteger,|0||0|9999,"
	"Specify the number of mSec that the standard write delay should be based upon.  "
	"A standard write is defined as any change value request that begins with the V "
	"command."
	"\0"
/*
	"IdxDlyRead,Index Read Delay,,CUIEditInteger,|0||0|9999,"
	"Specify the number of mSec that the index read delay should be based upon."
	"\0"

	"IdxDlyWrite,Index Write Delay,,CUIEditInteger,|0||0|9999,"	
	"Specify the number of mSec that the index write delay should be based upon."
	"\0"
*/
	"CmdDly,Command Delay,,CUIEditInteger,|0||0|9999,"
	"Specify the number of mSec that the command delay should be based upon.  "
	"This delay will affect any control action request that begins with the C or R "
	"command."
	"\0"

	"RspTime,Response Time,,CUIDropDown,Standard|High Speed,"
	"Select High Speed if the $ terminator should be used.  Please refer to  "
	"the appropriate meter serial manual to determine if high speed writes are "
	"Retentive or Non-Retentive."
	"\0"

	"\0"
	
END

CRlcDeviceOptionsUIPage RCDATA
BEGIN
	"P:1\0"
	"G:1,root,Device Identification,Drop,Device,Cub5\0"
	"G:1,root,Device Settings,Protocol,StdDlyRead,StdDlyWrite,CmdDly,RspTime\0"
	"\0"
END

//////////////////////////////////////////////////////////////////////////
//
// Partial Generic RLC Instrument Address Selection Dialog
//
//

PartRlcAddrDlg DIALOG 0, 0, 0, 0
CAPTION "Select Address"
BEGIN
	////////
	GROUPBOX	"&Address",			1000,		  4,   4, 130,  34
	EDITTEXT					1002,		 10,  18,  84,  12, 
//	PUSHBUTTON	"Clear",			IDCLEAR,	100,  18,  28,  13, XS_BUTTONREST
	////////
	DEFPUSHBUTTON   "OK",				IDOK,		  4,  44,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",			IDCANCEL,	 48,  44,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",				IDHELP,		 92,  44,  40,  14, XS_BUTTONREST
END	

//////////////////////////////////////////////////////////////////////////
//
// Full Generic RLC Instrument Address Selection Dialog
//
//

FullRlcAddrDlg DIALOG 0, 0, 0, 0
CAPTION "Select Address"
BEGIN
	////////
	GROUPBOX	"&Address",			1000,		  4,   4, 170,  34
	EDITTEXT					1002,		 10,  18,  84,  12, 
	PUSHBUTTON	"Clear",			IDCLEAR,	100,  18,  28,  13, XS_BUTTONREST
	////////
	GROUPBOX	"&Information",			2000,		  4,  44, 170,  70
	LTEXT		""				2001,		 10,  58, 160,  50
	////////
	DEFPUSHBUTTON   "OK",				IDOK,		  4, 124,  40,  14, XS_BUTTONFIRST
	PUSHBUTTON	"Cancel",			IDCANCEL,	 48, 124,  40,  14, XS_BUTTONREST
	PUSHBUTTON	"Help",				IDHELP,		 92, 124,  40,  14, XS_BUTTONREST
END

///////////////////////////////////////////////////////////////////////////
//
// String Table
//
//

STRINGTABLE
BEGIN
	IDS_RLC_FORMAT	"Addresses supported are of the following forms :\n\n1. A single letter identifier, eg A\n2. A two letter identifier, eg BB\n3. The letter C followed by an identifier, eg CT\n4. The letter R followed by an identifier, eg R1\n"
END

// End of File
