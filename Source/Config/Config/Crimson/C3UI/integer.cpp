
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Integer
//

// Dynamic Class

AfxImplementDynamicClass(CUITextInteger, CUITextElement);

// Constructor

CUITextInteger::CUITextInteger(void)
{
	m_uFlags = textEdit | textScroll | textUnits;

	m_nMin   = 0;

	m_nMax   = 60000;
	}

// Overridables

void CUITextInteger::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	GetFormat().Tokenize(List, '|');

	if( !List[3].IsEmpty() ) {

		m_nMin = watoi(List[3]);

		m_nMax = watoi(List[4]);
		}

	if( !(m_uPlaces = watoi(List[1])) ) {

		if( m_nMin >= 0 ) {

			m_uFlags |= textNumber;
			}
		}

	UINT nd1 = watoi(List[0]);

	UINT nd2 = watoi(List[1]);

	m_Units  = List[2];

	m_uLimit = nd1 ? (nd1+nd2) : 8;

	m_uWidth = m_uLimit;
	}

CString CUITextInteger::OnGetAsText(void)
{
	INT nData = m_pData->ReadInteger(m_pItem);

	if( !IsMulti(nData) ) {

		nData = StoreToDisp(nData);

		return Format(nData);
		}

	return multiString;
	}

UINT CUITextInteger::OnSetAsText(CError &Error, CString Text)
{
	if( !IsMulti(Text) ) {

		INT nPrev = m_pData->ReadInteger(m_pItem);

		INT nData = Parse(Text);

		nData     = DispToStore(nData);

		if( nData - nPrev ) {

			if( Check(Error, nData) ) {

				m_pData->WriteInteger(m_pItem, UINT(nData));

				return saveChange;
				}

			return saveError;
			}
		}

	return saveSame;
	}

CString CUITextInteger::OnScrollData(CString Text, UINT uCode)
{
	if( !IsMulti(Text) ) {

		INT nData = Parse(Text);

		INT nFast = m_uPlaces ? INT(pow(double(10), int(m_uPlaces))) : 10;

		INT nMin  = m_nMin;

		INT nMax  = m_nMax;

		switch( uCode ) {

			case SB_LINEDOWN:
				nData--;
				break;

			case SB_LINEUP:
				nData++;
				break;

			case SB_PAGEDOWN:
				nData = nFast * (nData / nFast) - nFast;
				break;

			case SB_PAGEUP:
				nData = nFast * (nData / nFast) + nFast;
				break;

			case SB_TOP:
				nData = nMax;
				break;

			case SB_BOTTOM:
				nData = nMin;
				break;
			}

		MakeMin(nData, nMax);

		MakeMax(nData, nMin);

		return Format(nData);
		}

	return Format(m_nMin);
	}

// Scaling

INT CUITextInteger::StoreToDisp(INT nData)
{
	return nData;
	}

INT CUITextInteger::DispToStore(INT nData)
{
	return nData;
	}

// Implementation

BOOL CUITextInteger::Check(CError &Error, INT &nData)
{
	if( nData < DispToStore(m_nMin) ) {

		if( Error.AllowUI() ) {

			CPrintf Text( IDS_SMALLEST,
				      Format(m_nMin)
				      );

			Error.Set(Text);

			return FALSE;
			}

		nData = DispToStore(m_nMin);

		return TRUE;
		}

	if( nData > DispToStore(m_nMax) ) {

		if( Error.AllowUI() ) {

			CPrintf Text( IDS_LARGEST,
				      Format(m_nMax)
				      );

			Error.Set(Text);

			return FALSE;
			}

		nData = DispToStore(m_nMax);

		return TRUE;
		}

	return TRUE;
	}

CString CUITextInteger::Format(INT nData)
{
	double  f = double(nData);

	double  p = pow(double(10), int(m_uPlaces));

	CPrintf t = CPrintf(L"%.*f", m_uPlaces, f / p);

	return (m_nMin < 0 && nData > 0) ? L'+' + t : t;
	}

INT CUITextInteger::Parse(PCTXT pText)
{
	double f = watof(pText);

	double p = pow(double(10), int(m_uPlaces));

	return (f > 0 ) ? INT(f * p + 0.5) : INT(f * p - 0.5);
	}

// End of File
