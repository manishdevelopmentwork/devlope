
#include "Intern.hpp"

#include "PlatformCanyonLvds.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Clock437.hpp"

#include "Ctrl437.hpp"

#include "Pwm437.hpp"

#include "Pru437.hpp"

#include "Pcm437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Pru Code  
//

#include "Pru/Canyon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Object Creation
//

extern IDevice * Create_Beeper437  (CPwm437 *pPwm, UINT uChan);
extern IDevice * Create_Uart437	   (UINT iIndex, CClock437 *pClock, CPru437 *pPru);
extern IDevice * Create_Display437 (UINT uModel, CDss437 *pDss, CPwm437 *pPwm, UINT uChan);
extern IDevice * Create_Touch437   (BOOL fFlipX, BOOL fFlipY, UINT uMaxVar, UINT uMinVal);
extern IDevice * Create_DualEnet437(CClock437 *pClock, UINT uGpio);
	
//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Canyon HMI
//

// Instantiator

IDevice * Create_PlatformCanyonLvds(UINT uModel)
{
	return (IDevice *)(CPlatformSitara *) New CPlatformCanyonLvds(uModel);
	}

// Constructor

CPlatformCanyonLvds::CPlatformCanyonLvds(UINT uModel) : CPlatformCanyon(uModel)
{
	}

// Destructor

CPlatformCanyonLvds::~CPlatformCanyonLvds(void)
{
	delete m_pPwm[0];

	delete m_pPwm[1];

	delete m_pPwm[2];

	delete m_pPru[0];

	delete m_pPru[1];

	piob->RevokeGroup("dev.");
	}

// IDevice

BOOL CPlatformCanyonLvds::Open(void)
{
	if( CPlatformCanyon::Open() ) {

		m_pPwm[0] = New CPwm437(0, m_pClock);

		m_pPwm[1] = New CPwm437(1, m_pClock);

		m_pPwm[2] = New CPwm437(2, m_pClock);

		m_pPru[0] = New CPru437(0, 0);

		m_pPru[1] = New CPru437(1, 0);

		InitPru();

		piob->RegisterSingleton("dev.beeper",  0, Create_Beeper437(m_pPwm[0], 1));
		
		piob->RegisterSingleton("dev.uart",    0, Create_Uart437(1, m_pClock, NULL));
		
		piob->RegisterSingleton("dev.uart",    1, Create_Uart437(0, m_pClock, NULL));
		
		piob->RegisterSingleton("dev.uart",    2, Create_Uart437(2, m_pClock, m_pPru[0]));
		
		piob->RegisterSingleton("dev.uart",    3, Create_Uart437(3, m_pClock, m_pPru[1]));

		piob->RegisterSingleton("dev.display", 0, Create_Display437(m_uModel, m_pDss, m_pPwm[1], 1));

		piob->RegisterSingleton("dev.touch",   0, Create_Touch437(TRUE, m_uModel != MODEL_CANYON_15, m_uModel == MODEL_CANYON_15 ? 50 : 20, m_uModel == MODEL_CANYON_15 ? 3 : 5));

		piob->RegisterSingleton("dev.enet-d",  0, Create_DualEnet437(m_pClock, MAKELONG(MAKEWORD(6,5),MAKEWORD(7,5))));

		InitUarts();

		InitNicMac(0);

		InitNicMac(1);

		FindSerial();

		return TRUE;
		}

	return FALSE;
	}

// ILeds

void METHOD CPlatformCanyonLvds::SetLed(UINT uLed, UINT uState)
{
	UINT iSel = HIBYTE(uLed);

	UINT iLed = LOBYTE(uLed);

	if( iSel == 0 ) {
	
		switch( iLed ) {

			case 0: m_pPwm[2]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); return;
			case 1: m_pPwm[1]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); return;
			case 2: m_pPwm[0]->SetDuty(CPwm437::pwmChanA, uState == stateOff ? 0 : 100); return;
			}
		}
	
	CPlatformCanyon::SetLed(uLed, uState);
	}

// IInputSwitch

UINT METHOD CPlatformCanyonLvds::GetSwitches(void)
{
	return 0xFFFFFFFE | (GetSwitch(0) ? Bit(0) : 0);
	}

UINT METHOD CPlatformCanyonLvds::GetSwitch(UINT uSwitch)
{
	if( uSwitch == 0 ) {

		return m_pGpio[2]->GetState(1);
		}

	return 1;
	}

// IPortSwitch

BOOL METHOD CPlatformCanyonLvds::SetFull(UINT uUnit, BOOL fFull)
{
	switch( uUnit ) {

		case 2:
			m_pGpio[0]->SetState(12, !fFull);
			
			return TRUE;

		case 3:
			m_pGpio[0]->SetState(13, !fFull);
			
			return TRUE;
		}

	return FALSE;
	}

// IUsbSystemPortMapper

UINT METHOD CPlatformCanyonLvds::GetPortType(UsbTreePath const &Path)
{
	if( Path.a.dwTier == 2 ) {

		switch( Path.a.dwPort2 ) {

			case 1: 
			case 2:
				return typeExtern;

			case 3:
				return typeOption;
			}
		}

	return CPlatformCanyon::GetPortType(Path);
	}

// Inititialisation

void CPlatformCanyonLvds::InitClocks(void)
{
	CPlatformCanyon::InitClocks();

	m_pClock->SetClockMode(CClock437::clockADC0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPWM1,  CClock437::modeSwWakup);	
	m_pClock->SetClockMode(CClock437::clockPWM2,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockPRU,   CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART0, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART1, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART2, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockUART3, CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockMAC0,  CClock437::modeSwWakup);
	m_pClock->SetClockMode(CClock437::clockCPSW,  CClock437::modeSwWakup);
	}

void CPlatformCanyonLvds::InitMux(void)
{
	CPlatformCanyon::InitMux();

	DWORD const muxPwm[] = {

		CCtrl437::pinSPI0D0,		CCtrl437::muxMode3,
		CCtrl437::pinSPI0CS0,		CCtrl437::muxMode8,
		CCtrl437::pinSPI0SCLK,		CCtrl437::muxMode3,
		CCtrl437::pinSPI0D1,		CCtrl437::muxMode8,
		CCtrl437::pinSPI0CS1,		CCtrl437::muxMode8,
		};

	DWORD const muxPru[] = {

		CCtrl437::pinUART3RXD,		CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0ACLKX,	CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMCASP0FSX,		CCtrl437::muxMode6 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinXDMAEVTINTR1,	CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinGPMCCSN2,		CCtrl437::muxMode5 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		};

	DWORD const muxSwi[] = {

		CCtrl437::pinGPMCCLK,		CCtrl437::muxMode7 | CCtrl437::padSlow | CCtrl437::padPullUp | CCtrl437::padRxActive,
		};

	DWORD const muxSer[] = {

		CCtrl437::pinUART0RXD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART0TXD,		CCtrl437::muxMode0 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA8,		CCtrl437::muxMode8 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA9,		CCtrl437::muxMode8 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA0,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA1,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA2,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA3,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA4,		CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA5,		CCtrl437::muxMode2 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinCAM1DATA7,		CCtrl437::muxMode7 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD2,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD3,		CCtrl437::muxMode1 | CCtrl437::padPullNo | CCtrl437::padRxActive,
		CCtrl437::pinUART1CTSN,		CCtrl437::muxMode7 | CCtrl437::padPullNo,
		CCtrl437::pinUART1RTSN,		CCtrl437::muxMode7 | CCtrl437::padPullNo,
		};

	DWORD const muxNic[] = {

		CCtrl437::pinMII1TXD0,          CCtrl437::muxMode1 | CCtrl437::padPullDn,
		CCtrl437::pinMII1TXD1,          CCtrl437::muxMode1 | CCtrl437::padPullDn,
		CCtrl437::pinMII1RXD0,          CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMII1RXD1,          CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMII1TXEN,          CCtrl437::muxMode1 | CCtrl437::padPullDn,
		CCtrl437::pinMII1REFCLK,        CCtrl437::muxMode0 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMII1CRS,           CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinMDIOCLK,           CCtrl437::muxMode0 | CCtrl437::padPullUp,
		CCtrl437::pinMDIODATA,          CCtrl437::muxMode0 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinGPMCA4,            CCtrl437::muxMode3 | CCtrl437::padPullDn,
		CCtrl437::pinGPMCA5,            CCtrl437::muxMode3 | CCtrl437::padPullDn,
		CCtrl437::pinGPMCA10,           CCtrl437::muxMode3 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPMCA11,           CCtrl437::muxMode3 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPMCA0,            CCtrl437::muxMode3 | CCtrl437::padPullDn,
		CCtrl437::pinMII1COL,           CCtrl437::muxMode1 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinGPMCWAIT0,         CCtrl437::muxMode3 | CCtrl437::padPullDn | CCtrl437::padRxActive,
		CCtrl437::pinSPI4CS0,           CCtrl437::muxMode7 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		CCtrl437::pinSPI4SCLK,          CCtrl437::muxMode7 | CCtrl437::padPullUp | CCtrl437::padRxActive,
		};

	DWORD const muxDisp[] = {

		CCtrl437::pinSPI4SCLK,		CCtrl437::muxMode7,
		CCtrl437::pinGPMCA3,		CCtrl437::muxMode7,
		CCtrl437::pinGPMCA6,		CCtrl437::muxMode7,
		CCtrl437::pinGPMCA7,		CCtrl437::muxMode7,
		};

	m_pCtrl->SetMux(muxPwm,  elements(muxPwm));

	m_pCtrl->SetMux(muxPru,  elements(muxPru));

	m_pCtrl->SetMux(muxSwi,  elements(muxSwi));

	m_pCtrl->SetMux(muxSer,  elements(muxSer));

	m_pCtrl->SetMux(muxNic,  elements(muxNic));

	m_pCtrl->SetMux(muxDisp, elements(muxDisp));
	}

void CPlatformCanyonLvds::InitGpio(void)
{
	CPlatformSitara::InitGpio();

	m_pGpio[0]->SetDirection(12, true);
	m_pGpio[0]->SetDirection(13, true);
	m_pGpio[2]->SetDirection(1,  false);
	}

void CPlatformCanyonLvds::InitMisc(void)
{
	CPlatformCanyon::InitMisc();

	m_pCtrl->SetPWM(0, true);
	
	m_pCtrl->SetPWM(1, true);

	m_pCtrl->SetPWM(2, true);

	m_pPwm[0]->SetFreq(5000);

	m_pPwm[1]->SetFreq(5000);

	m_pPwm[2]->SetFreq(5000);

	m_pCtrl->SetMIIMode(0, CCtrl437::modeRMII);

	m_pCtrl->SetMIIMode(1, CCtrl437::modeRMII);
	}

void CPlatformCanyonLvds::InitPru(void)
{
	m_pPcm->EnablePru();

	PCTXT pCode[2] = { PRU_CODE0, PRU_CODE1 };

	for( UINT i = 0; i < 2; i ++ ) {

		m_pPru[i]->Stop(0);

		m_pPru[i]->Reset(0);

		m_pPru[i]->SetMux(0);

		m_pPru[i]->LoadCode(0, pCode[i]);
		}
	}

// End of File
