/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** SESSION.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Session collection implementation
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"


INT32    gnSessions;				/* Number of the open sessions */
SESSION  gSessions[MAX_SESSIONS];	/* Session objects array */

/*---------------------------------------------------------------------------
** sessionInit( )
**
** Initialize the session array
**---------------------------------------------------------------------------
*/

void sessionInit()
{
	INT32    i;
	
	gnSessions = 0;
	
	for( i = 0; i < MAX_SESSIONS; i++ )
		sessionInitialize( i );	
}

/*---------------------------------------------------------------------------
** sessionInitialize( )
**
** Initialize the session object
**---------------------------------------------------------------------------
*/

void sessionInitialize( INT32 nSession )
{	
	memset( &gSessions[nSession], 0, sizeof(SESSION) );
	
	gSessions[nSession].lSocket = INVALID_SOCKET;
	gSessions[nSession].lPingSocket = INVALID_SOCKET;
		
	memset( (void *)&gSessions[nSession].sClientAddr, 0, sizeof(struct sockaddr) );

	gSessions[nSession].iClientEncapProtocolVersion = ENCAP_PROTOCOL_VERSION;
	gSessions[nSession].iClientEncapProtocolFlags = 0;	

	gSessions[nSession].iPartialRecvPacketOffset = INVALID_MEMORY_OFFSET;
	gSessions[nSession].iPartialRecvPacketSize = 0;

	gSessions[nSession].iPartialSendPacketOffset = INVALID_MEMORY_OFFSET;
	gSessions[nSession].iPartialSendPacketSize = 0;
}

/*---------------------------------------------------------------------------
** sessionNew( )
**
** Allocate and initialize a new session object in the array
**---------------------------------------------------------------------------
*/

INT32 sessionNew( UINT32 lIPAddress, BOOL bIncoming )
{
	INT32 nSession = gnSessions;
	UINT32 lTick = platformGetTickCount();

	if ( nSession >= MAX_SESSIONS )	/* Limit of the open sessions reached */
		return INVALID_SESSION;

	sessionInitialize( nSession );

	if ( bIncoming )
	{
		gSessions[nSession].lWaitingForOpenSessionTimeout = lTick + NON_CRITICAL_TIMEOUT;
		gSessions[nSession].lState = OpenSessionWaitingForRegister;
	}
	else
	{
		gSessions[nSession].lWaitingForOpenSessionTimeout = lTick + DEFAULT_TIMEOUT;
		
		memset( &gSessions[nSession].sClientAddr, 0, sizeof(struct sockaddr_in) );
		gSessions[nSession].sClientAddr.sin_family = AF_INET;
		gSessions[nSession].sClientAddr.sin_port = htons( ENCAP_SERVER_PORT );
		gSessions[nSession].sClientAddr.sin_addr.s_addr = lIPAddress;

		gSessions[nSession].lState = OpenSessionLogged;
	}
	
	gSessions[nSession].bIncoming = bIncoming;

	gnSessions++;

	return nSession;
}

/*---------------------------------------------------------------------------
** sessionRemove( )
**
** Remove the session from the array
**---------------------------------------------------------------------------
*/

void sessionRemove( INT32 nSessionRemove, BOOL bRelogOutgoingRequests )
{
	INT32 nSession, nConnection, nRequest;

	DumpStr1("sessionRemove %d", nSessionRemove);
		
	for( nConnection = (gnConnections-1); nConnection >= 0; nConnection-- )
	{
		if ( gConnections[nConnection].lIPAddress == gSessions[nSessionRemove].sClientAddr.sin_addr.s_addr )
		{			
			if ( gConnections[nConnection].lConnectionState == ConnectionEstablished )
			{
				gConnections[nConnection].bGeneralStatus = ROUTER_ERROR_FAILURE;
				gConnections[nConnection].iExtendedStatus = ROUTER_EXT_ERR_CONNECTION_TIMED_OUT;				
				notifyEvent( NM_CONNECTION_TIMED_OUT, gConnections[nConnection].cfg.nInstance );
				connectionTimedOut( nConnection );				
			}
#ifdef ET_IP_SCANNER			
			else if ( gConnections[nConnection].lConnectionState == ConnectionConfiguring && 
					  ( gConnections[nConnection].lConfigurationState == ConfigurationLogged || gConnections[nConnection].lConfigurationState == ConfigurationWaitingForSession ||
					    gConnections[nConnection].lConfigurationState == ConfigurationWaitingForForwardOpenResponse ) )
			{
				if ( gConnections[nConnection].cfg.bOriginator )
				{
					gConnections[nConnection].lConfigurationState = ConfigurationFailedInvalidNetworkPath;
					notifyEvent( NM_CONN_CONFIG_FAILED_INVALID_NETWORK_PATH, gConnections[nConnection].cfg.nInstance );
					connectionTimedOut( nConnection );												
				}
			}		
#endif			
		}
	}	
			
	for( nRequest = (gnRequests-1); nRequest >= 0; nRequest-- )
	{
		if ( ( gRequests[nRequest].lIPAddress == gSessions[nSessionRemove].sClientAddr.sin_addr.s_addr ||
			   gRequests[nRequest].lFromIPAddress == gSessions[nSessionRemove].sClientAddr.sin_addr.s_addr ) )
		{
			if ( gRequests[nRequest].bIncoming )
				requestRemove( nRequest );		
			else if ( bRelogOutgoingRequests && gRequests[nRequest].nState != RequestResponseReceived )		/* Relog outstanding explicit requests if appropriate */ 
				gRequests[nRequest].nState = RequestLogged;					
		}
	}	

	if ( gnSessions )
		gnSessions--;								/* Adjust the total number of open sessions */
	
	if ( gSessions[nSessionRemove].lSocket != INVALID_SOCKET )
	{
		platformCloseSocket(gSessions[nSessionRemove].lSocket);	/* Close Winsock TCP socket */
		gSessions[nSessionRemove].lSocket = INVALID_SOCKET;
	}

	if ( gSessions[nSessionRemove].lPingSocket != INVALID_SOCKET )
	{
		platformCloseSocket(gSessions[nSessionRemove].lPingSocket);	/* Close temporary Ping socket */
		gSessions[nSessionRemove].lPingSocket = INVALID_SOCKET;
	}

	if ( gSessions[nSessionRemove].iPartialRecvPacketOffset != INVALID_MEMORY_OFFSET )
		utilRemoveFromMemoryPool( &gSessions[nSessionRemove].iPartialRecvPacketOffset, &gSessions[nSessionRemove].iPartialRecvPacketSize );

	if ( gSessions[nSessionRemove].iPartialSendPacketOffset != INVALID_MEMORY_OFFSET )
		utilRemoveFromMemoryPool( &gSessions[nSessionRemove].iPartialSendPacketOffset, &gSessions[nSessionRemove].iPartialSendPacketSize );
		
	for( nSession = nSessionRemove; nSession < gnSessions; nSession++ )		/* Shift the sessions with the higher index to fill in the void */
		memcpy( &gSessions[nSession], &gSessions[nSession+1], sizeof(SESSION) );	
}

/*---------------------------------------------------------------------------
** sessionRemoveAll( )
**
** Close and remove all open sessions
**---------------------------------------------------------------------------
*/

void sessionRemoveAll( )
{
	INT32 i;
			
	for( i = (gnSessions-1); i >= 0; i-- )
	{		
		sessionRemove( i, FALSE );		
	}

	gnSessions = 0;
}

/*---------------------------------------------------------------------------
** sessionGetNumOutgoing( )
**
** Return the number of sessions that were originated from this stack
**---------------------------------------------------------------------------
*/

INT32 sessionGetNumOutgoing( )
{
	INT32 i;
	INT32 nSessions = 0;
			
	for( i = 0; i < gnSessions; i++ )
	{
		if ( !gSessions[i].bIncoming && gSessions[i].lState == OpenSessionEstablished )			 
			nSessions++;
	}

	return nSessions;
}

/*---------------------------------------------------------------------------
** sessionService( )
**
** Service a particular session
**---------------------------------------------------------------------------
*/
void sessionService( INT32 nSession )
{	
	INT32   lDataReceived;
	UINT32  lTick = platformGetTickCount();

#if defined(ASYNCHRONOUS_CONNECTION)		

	BOOL    bPingSuccessfullyCompleted;
	BOOL    bTCPConnectionSuccessfullyCompleted;

#endif // #if defined(ASYNCHRONOUS_CONNECTION)		
	
	switch( gSessions[nSession].lState )
	{

#if defined(ASYNCHRONOUS_CONNECTION)		

		case OpenSessionLogged:
		{				
			if( !platformStartPing( nSession, &bPingSuccessfullyCompleted ) ) 
			{				
				sessionRemove( nSession, FALSE );
				break;
			}

			if ( bPingSuccessfullyCompleted )
				gSessions[nSession].lState = OpenSessionPingSuccessfullyCompleted;						
			else
			{
				gSessions[nSession].lState = OpenSessionWaitingForPing;						
				gSessions[nSession].lPingTimeout = lTick + PING_TIMEOUT;
			}
		}
		break;

		case OpenSessionWaitingForPing:
		{				
			if( !platformContinuePing( nSession, &bPingSuccessfullyCompleted ) ) 
			{				
				sessionRemove( nSession, FALSE );
				break;
			}

			if ( bPingSuccessfullyCompleted )
				gSessions[nSession].lState = OpenSessionPingSuccessfullyCompleted;						
			else if ( IS_TICK_GREATER( lTick, gSessions[nSession].lPingTimeout ) )
				sessionRemove( nSession, FALSE );				
		}
		break;

		case OpenSessionPingSuccessfullyCompleted:
		{				
			/* Clean up Ping socket now instead of waiting for the session to be closed */
			if ( gSessions[nSession].lPingSocket != INVALID_SOCKET )
			{
				platformCloseSocket(gSessions[nSession].lPingSocket);	/* Close temporary Ping socket */
				gSessions[nSession].lPingSocket = INVALID_SOCKET;
			}
			
			if( !socketStartTCPConnection( nSession, &bTCPConnectionSuccessfullyCompleted ) ) 
			{				
				sessionRemove( nSession, FALSE );
				break;
			}

			if ( bTCPConnectionSuccessfullyCompleted )
				gSessions[nSession].lState = OpenSessionTCPConnectionEstablished;						
			else
				gSessions[nSession].lState = OpenSessionWaitingForTCPConnection;						
		}
		break;

		case OpenSessionWaitingForTCPConnection:
		{	
			if( !socketContinueTCPConnection( nSession, &bTCPConnectionSuccessfullyCompleted ) ) 
			{				
				sessionRemove( nSession, FALSE );
				break;
			}

			if ( bTCPConnectionSuccessfullyCompleted )
				gSessions[nSession].lState = OpenSessionTCPConnectionEstablished;						
			else if ( IS_TICK_GREATER( lTick, gSessions[nSession].lWaitingForOpenSessionTimeout ) )
				sessionRemove( nSession, FALSE );								
		}
		break;

#else	/*#if defined(ASYNCHRONOUS_CONNECTION)*/

		case OpenSessionLogged:
		{				
			if( !platformCreateThread( socketTcpConnect, &gSessions[nSession].sClientAddr, 0 )) 
			{
				notifyEvent(NM_UNABLE_START_THREAD, 0);
				sessionRemove( nSession, FALSE );
				return;
			}
			gSessions[nSession].lState = OpenSessionWaitingForTCPConnection;						
		}
		break;

		case OpenSessionWaitingForTCPConnection:
		{	
			if ( IS_TICK_GREATER( lTick, gSessions[nSession].lWaitingForOpenSessionTimeout ) )
				sessionRemove( nSession, FALSE );			
		}
		break;

#endif /*#if defined(ASYNCHRONOUS_CONNECTION)*/
		
		case OpenSessionTCPConnectionEstablished:
		{	
			ucmmIssueRegisterSession( nSession );
			gSessions[nSession].lState = OpenSessionWaitingForRegisterResponse;									
		}
		break;

		case OpenSessionWaitingForRegisterResponse:
		case OpenSessionWaitingForRegister:
		{	
			if ( IS_TICK_GREATER( lTick, gSessions[nSession].lWaitingForOpenSessionTimeout ) )
				sessionRemove( nSession, FALSE );			
		}
				
		case OpenSessionEstablished:
		{
			if ( gSessions[nSession].iPartialSendPacketSize )
				socketEncapSendPartial( nSession );

			do
			{
				lDataReceived = socketEncapRecv( nSession );
			
				if ( lDataReceived > 0 )
					ucmmProcessEncapMsg( nSession );	/* Process TCP data */					
			}			
			while ( lDataReceived > 0 );
		}
		break;

		default:
			break;
	}
}			

/*---------------------------------------------------------------------------
** sessionFindAddress( )
**
** Return the session index for the particular client IP address
**---------------------------------------------------------------------------
*/

INT32 sessionFindAddress( UINT32 lIPAddress, INT32 nDirection )
{
	INT32 i;
		
	for( i = 0; i < gnSessions; i++ )
	{
		if ( gSessions[i].sClientAddr.sin_addr.s_addr == lIPAddress &&
			 ( ( gSessions[i].bIncoming && nDirection == Incoming ) || ( !gSessions[i].bIncoming && nDirection == Outgoing ) || ( nDirection == AnyDirection ) ) )
			 	return i;								 
	}

	return INVALID_SESSION;
}

/*---------------------------------------------------------------------------
** sessionFindAddressEstablished( )
**
** Return the session index for the particular client IP address if the 
** session is in the established state
**---------------------------------------------------------------------------
*/

INT32 sessionFindAddressEstablished( UINT32 lIPAddress, INT32 nDirection )
{
	INT32 i;
		
	for( i = 0; i < gnSessions; i++ )
	{
		if ( gSessions[i].sClientAddr.sin_addr.s_addr == lIPAddress &&
			 ( ( gSessions[i].bIncoming && nDirection == Incoming ) || ( !gSessions[i].bIncoming && nDirection == Outgoing ) || ( nDirection == AnyDirection ) ) && 
			 gSessions[i].lState == OpenSessionEstablished )
				return i;					
	}

	return INVALID_SESSION;
}


/*---------------------------------------------------------------------------
** sessionIsIdle( )
**
** Return TRUE if there no active connections or requests for the specified
** IP address. Otherwise return FALSE.
**---------------------------------------------------------------------------
*/

BOOL sessionIsIdle( UINT32 lIPAddress, INT32 nConnectionExclude, INT32 nRequestExclude )
{
	INT32 nConnection, nRequest;
		
	for( nConnection = (gnConnections-1); nConnection >= 0; nConnection-- )
	{
		if ( gConnections[nConnection].lIPAddress == lIPAddress && nConnection != nConnectionExclude &&
		     ( gConnections[nConnection].lConnectionState == ConnectionEstablished ||
				( gConnections[nConnection].lConnectionState == ConnectionConfiguring && gConnections[nConnection].lConfigurationState == ConfigurationWaitingForForwardOpenResponse ) ) )
					return FALSE;
	}

	for( nRequest = (gnRequests-1); nRequest >= 0; nRequest-- )
	{
		if ( gRequests[nRequest].lIPAddress == lIPAddress && nRequest != nRequestExclude && 
			 !gRequests[nRequest].bIncoming && gRequests[nRequest].nState == RequestWaitingForResponse )			   
					return FALSE;
	}	
	
	return TRUE;
}
