@echo off

rem %1 = config
rem %2 = build dir

echo Publishing...

xcopy /Y /D /R /I /S Straton "%~2\Bin\Config\Win32\%1\Straton"

xcopy /Y /D /R /I "%~2\Bin\Config\Win32\%1\Straton\Misc\k5root.ini" "%~2\Bin\Config\Win32\%1"

xcopy /Y /D /R /I "%~2\Bin\Config\Win32\%1\Straton\*.err" "%~2\Bin\Config\Win32\%1"
