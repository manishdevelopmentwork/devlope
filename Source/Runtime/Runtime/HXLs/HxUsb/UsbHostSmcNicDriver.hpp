
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHostSmcNicDriver_HPP

#define	INCLUDE_UsbHostSmcNicDriver_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UsbHostFuncDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Smc Nic Driver
//

class CUsbHostSmcNicDriver : public CUsbHostFuncDriver, public IUsbNetwork
{
	public:
		// Constructor
		CUsbHostSmcNicDriver(void);

		// Destructor
		~CUsbHostSmcNicDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IUsbHostFuncDriver
		void METHOD Bind(IUsbDevice *pDev, UINT iInterface);
		void METHOD Bind(IUsbHostFuncEvents *pEvent);
		void METHOD GetDevice(IUsbDevice *&pDevice);
		BOOL METHOD SetRemoveLock(BOOL fLock);
		UINT METHOD GetVendor(void);
		UINT METHOD GetProduct(void);
		UINT METHOD GetClass(void);
		UINT METHOD GetSubClass(void);
		UINT METHOD GetProtocol(void);
		UINT METHOD GetInterface(void);
		BOOL METHOD GetActive(void);
		BOOL METHOD Open(CUsbDescList const &Config);
		BOOL METHOD Close(void);
		void METHOD Poll(UINT uLapsed);

		// IUsbNetwork
		BOOL METHOD Startup(BOOL fFast, BOOL fFull);
		BOOL METHOD Shutdown(void);
		BOOL METHOD InitMac(MACADDR const &Addr);
		void METHOD ReadMac(MACADDR &Addr);
		UINT METHOD GetCapabilities(void);
		void METHOD SetFlags(UINT uFlags);
		BOOL METHOD SetMulticast(MACADDR const *pList, UINT uList);
		BOOL METHOD IsLinkActive(void);
		BOOL METHOD WaitLink(UINT uTime);
		BOOL METHOD SendData(CBuffer * pBuff);
		BOOL METHOD RecvData(CBuffer * &pBuff);
		void METHOD GetCounters(NICDIAG &Diag);
		void METHOD ResetCounters(void);

	protected:
		// Commands
		enum
		{
			cmdWriteReg	= 0xA0,
			cmdReadReg	= 0xA1,
			cmdGetStats	= 0xA2,
			};

		// Registers
		enum
		{
			regIdRev	= 0x0000,
			regFpgaRev	= 0x0004,
			regIntSts	= 0x0008,
			regRxCfg	= 0x000C,
			regTxCfg	= 0x0010,
			regHwCfg	= 0x0014,
			regRxFifoInf	= 0x0018,
			regTxFifoInf	= 0x001C,
			regPmCtrl	= 0x0020,
			regLedGpioCfg	= 0x0024,
			regGpioCfg	= 0x0028,
			regAfxCfg	= 0x002C,
			regPromCmd	= 0x0030,
			regPromData	= 0x0034,
			regBurstCap	= 0x0038,
			regStapDbg	= 0x003C,
			regRamSel	= 0x0040,
			regRamCmd	= 0x0044,
			regRamAddr	= 0x0048,
			regRamData0	= 0x004C,
			regRamData1	= 0x0050,
			regRamBist0	= 0x0054,
			regRamBist1	= 0x0058,
			regRamBist2	= 0x005C,
			regRamBist3	= 0x0060,
			regGpioWake	= 0x0064,
			regIntCtl	= 0x0068,
			regBulkInDly	= 0x006C,
			regMacCr	= 0x0100,
			regAddrH	= 0x0104,
			regAddrL	= 0x0108,
			regHashH	= 0x010C,
			regHashL	= 0x0110,
			regMiiAddr	= 0x0114,
			regMiiData	= 0x0118,
			regFlow		= 0x011C,
			regVLan1	= 0x0120,
			regVLan2	= 0x0124,
			regWuff		= 0x0128,
			regWuCsr	= 0x012C,
			regCoeCr	= 0x0130,
			};

		// Phy Registers
		enum 
		{
			phyControl	= 0x0000,
			phyStatus	= 0x0001,
			phyIdent1	= 0x0002,
			phyIdent2	= 0x0003,
			phyAutoNegAdv	= 0x0004,
			phyAutoNegLnk	= 0x0005,
			phyAutoNegExp	= 0x0006,
			phyRev		= 0x0010,
			phyModeCtrlStat	= 0x0011,
			phySpecialModes	= 0x0012,
			phyTestCtrl	= 0x0014,
			phyTestRead1	= 0x0015,
			phyTestRead2	= 0x0016,
			phyTestWrite	= 0x0017,
			phySpecialCtrl	= 0x001B,
			phySpectialInt	= 0x001C,
			phyInt		= 0x001D,
			phyIntMask	= 0x001E,
			phySpecial	= 0x001F,
			};

		// Interrupt Flags
		enum
		{
			intTxStop	= Bit(17),
			intRxStop	= Bit(16),
			intPhyInt	= Bit(15),
			intTxe		= Bit(14),
			intTdfu		= Bit(13),
			intTdfo		= Bit(12),
			intRxdf		= Bit(11),
			intGpios	= 0x000007FF,
			};

		// Hardware Config / Flags
		enum
		{
			hwSmDetSts	= ( 1 << 18 ),
    			hwSmDetEn	= ( 1 << 17 ),
    			hwEem		= ( 1 << 16 ),
    			hwRstProt	= ( 1 << 15 ),
			hwPhyBoost	= ( 3 << 13 ),
			hwPhyBoostNorm	= ( 0 << 13 ),
			hwPhyBoost4	= ( 1 << 13 ),
			hwPhyBoost8_	= ( 2 << 13 ),
			hwPhyBoost12	= ( 3 << 13 ),
			hwBir		= ( 1 << 12 ),
			hwLedB		= ( 1 << 11 ),
			hwRxDoff	= ( 3 <<  9 ),
			hwSbp		= ( 1 <<  8 ),
			hwIme		= ( 1 <<  7 ),
			hwDrp		= ( 1 <<  6 ),
			hwMef		= ( 1 <<  5 ),
			hwResetL	= ( 1 <<  3 ),
			hwPSel		= ( 1 <<  2 ),
			hwBce		= ( 1 <<  1 ),
			hwResetS	= ( 1 <<  0 ),
			};

		// Power Config / Flags
		enum
		{
			pmResClrWkpSts	= ( 1 << 9 ),
			pmResClrWkpEn	= ( 1 << 8 ),
			pmDevRdy	= ( 1 << 7 ),
			pmSuspend	= ( 3 << 5 ),
			pmSuspend0	= ( 0 << 5 ),
			pmSuspend1	= ( 1 << 5 ),
			pmSuspend2	= ( 2 << 5 ),
			pmSuspend3	= ( 3 << 5 ),
			pmPhyReset	= ( 1 << 4 ),
			pmWolEn		= ( 1 << 3 ),
			pmEdEn		= ( 1 << 2 ),
			pmWups		= ( 3 << 0 ),
			pmWupsNo	= ( 0 << 0 ),
			pmWupsEd	= ( 1 << 0 ),
			pmWupsWol	= ( 2 << 0 ),
			pmWupsMulti	= ( 3 << 0 ),
			};

		// Mac Flags
		enum
		{
			macRxAll	= Bit(31),
			macRcvOwn	= Bit(23),
			macLoopbk	= Bit(21),
			macFdpx		= Bit(20),
			macMcpas	= Bit(19),
			macPrms		= Bit(18),
			macInvFilt	= Bit(17),
			macPassBad	= Bit(16),
			macHFilt	= Bit(15),
			macHpFilt	= Bit(13),
			macLColl	= Bit(12),
			macBcast	= Bit(11),
			macDisrty	= Bit(10),
			macPadstr	= Bit( 8),
			macDfchk	= Bit( 5),
			macTxEn		= Bit( 3),
			macRxEn		= Bit( 2),
			};

		//  MII Flags
		enum
		{
			miiBusy		= ( 1 << 0 ),
			miiRead		= ( 0 << 1 ),
			miiWrite	= ( 1 << 1 ),
			};

		// Rx Flags
		enum
		{
			rxcfgFlush	= Bit(0),
			};

		// Tx Flags
		enum
		{
			txcfgOn		= Bit(2),
			txcfgStop	= Bit(1),
			txcfgFlush	= Bit(0),
			};

		// Eprom Config / Flags
		enum
		{
			promBusy	= ( 1 << 31 ),
			promCmdMask	= ( 7 << 28 ),
			promRead	= ( 0 << 28 ),
			promEwds	= ( 1 << 28 ),
			promWriteEn	= ( 2 << 28 ),
			promWrite	= ( 3 << 28 ),
			promWral	= ( 4 << 28 ),
			promErase	= ( 5 << 28 ),
			promEral	= ( 6 << 28 ),
			promReload	= ( 7 << 28 ),
			promTimeout	= ( 1 << 10 ),
			promLoaded	= ( 1 <<  9 ),
			promAddrMask	= 0x000001FF,
			promDataMask	= 0x000000FF,
			};

		// Ram Config / Flags
		enum
		{
			ramselReady	= ( 1 << 31 ),
			ramselBits	= ( 3 <<  1 ),
			ramselRxTli	= ( 3 <<  1 ),
			ramselTxTli	= ( 2 <<  1 ),
			ramselTxEep	= ( 1 <<  1 ),
			ramselTxFct	= ( 0 <<  1 ),
			ramselTestEn	= ( 1 <<  0 ),
			};

		// Rx Status / Flags
		enum
		{
			rxFiltFail	= Bit(30),
			rxErrSum	= Bit(15),
			rxBroadcast	= Bit(13),
			rxLenErr	= Bit(12),
			rxRunt		= Bit(11),
			rxMulticast	= Bit(10),
			rxTooLong	= Bit( 7),
			rxCollision	= Bit( 6),
			rxType		= Bit( 5),
			rxRxDog		= Bit( 4),
			rxMiiErr	= Bit( 3),
			rxDribbling	= Bit( 2),
			rxCRC		= Bit( 1),
			rxFrameLen	= 0x3FFF0000,
			rxErr		= 0x000088DE,
			};	

		// Tx Commands
		enum
		{
			txcmdaDataOff	= 0x001F0000,
			txcmdaFirstSeg	= Bit(13),
			txcmdaLastSeg	= Bit(12),
			txcmdaBuffSize	= 0x000007FF,
			txcmdbCsumEn	= Bit(14),
			txcmdbNoAddCrc	= Bit(13),
			txcmdbNoPadding	= Bit(12),
			txcmdbPacketLen	= 0x000007FF,
			};
		
		// Rx Counter Block
		struct CRxStats
		{
			DWORD	m_dwGood;
			DWORD	m_dwCrc;
			DWORD	m_dwRunt;
			DWORD	m_dwAlignment;
			DWORD	m_dwTooLong;
			DWORD	m_dwCollision;
			DWORD	m_dwBad;
			DWORD	m_dwDropped;
			};

		// Tx Counter Block
		struct CTxStats
		{
			DWORD	m_dwGood;
			DWORD	m_dwPause;
			DWORD	m_dwSingleCol;
			DWORD	m_dwMultipleCol;
			DWORD	m_dwExcessiveCol;
			DWORD	m_dwLateCol;
			DWORD	m_dwUnderrun;
			DWORD	m_dwDeferral;
			DWORD	m_dwCarrier;
			DWORD	m_dwBad;
			};

		// Nic States
		enum
		{
			nicInitial,
			nicClosed,
			nicOpen,
			nicNegotiate,
			nicActive,
			};

		// Data
		IBufferManager * m_pBuffMan;
		IUsbPipe       * m_pCtrl;
		IUsbPipe       * m_pSend;
		IUsbPipe       * m_pRecv;
		IUsbPipe       * m_pPoll;
		UINT	         m_iSend;
		UINT	         m_iRecv;
		UINT             m_iPoll;
		UINT             m_uPhy;
		IEvent         * m_pLinkUp;
		ISemaphore     * m_pWaitTx;
		CRxStats         m_RxStats;
		CTxStats         m_TxStats;
		MACADDR          m_MacAddr;
		MACADDR	       * m_pMulti;
		UINT		 m_uMulti;
		UINT             m_uNicState;
		bool	         m_fPollPend;
		DWORD	         m_dwPoll;
		UINT             m_uFlags;
		BYTE	         m_bRxData[1600];
		
		// Mac Management
		bool ResetController(void);
		bool ConfigController(void);
		void ConfigLeds(void);
		void StartRx(void);
		void StartTx(void);
		void StopRx(void);
		void StopTx(void);
		void FlushRx(void);
		void FlushTx(void);
		void AddMac(PBYTE pData);
		void LoadMultiFilter(void);
		UINT GetMacHash(MACADDR const &Mac);

		// Phy Management
		bool ResetPhy(void);
		void ConfigPhy(bool fAllowFast, bool fAllowFull);
		void OnLinkUp(void);
		void OnLinkDown(void);

		// Prom Management
		bool PromRead(WORD wAddr, PBYTE pData, UINT uCount);
		bool PromWrite(WORD wAddr, PBYTE pData, UINT uCount);
		bool PromRead(WORD wAddr, BYTE &bData);
		bool PromWrite(WORD wAddr, BYTE bData);
		bool PromCmd(UINT uCmd); 
		bool PromReload(void);

		// Ram Management
		bool RamRead(DWORD dwAddr, DWORD &dwData);
		bool RamWrite(DWORD dwAddr, DWORD  dwData);
		bool RamWait(UINT uTimeout);

		// Mii Register Access
		bool PhyGet(UINT uReg, DWORD &dwData);
		bool PhyPut(UINT uReg, DWORD  dwData);
		bool PhyWait(UINT uTimeout);
				
		// Gpio
		bool GpioSet(UINT iSel, bool fState);

		// Register Access
		bool RegSet(WORD wReg, DWORD dwMask, DWORD dwData);
		bool RegTst(WORD wReg, DWORD dwMask, DWORD dwData);
		bool RegGet(WORD wReg, DWORD &dwData);
		bool RegPut(WORD wReg, DWORD dwData);
		bool RegGetStats(bool fRx);
		
		// Implementation
		bool FindPipes(CUsbDescList const &List);
		bool TakeFrame(PBYTE pData);
	};

// End of File

#endif
