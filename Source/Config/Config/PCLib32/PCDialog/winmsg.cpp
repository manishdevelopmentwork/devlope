
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Dialog Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Extended Message Box Procedure
//

UINT ExtMsgBox(HWND hWnd, PCTXT pText, PCTXT pTitle, UINT uFlags, PCTXT pDoNot)
{
	CMessageBox Box(pText, pTitle, uFlags, pDoNot);

	return Box.Execute(CWnd::FromHandle(hWnd));
	}

// End of File
