
#include "intern.hpp"

#include "bannermb.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Driver
//

// Instantiator

INSTANTIATE(CBannerModbusTCP);

// Constructor

CBannerModbusTCP::CBannerModbusTCP(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CBannerModbusTCP::~CBannerModbusTCP(void)
{
	}

// Configuration

void MCALL CBannerModbusTCP::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CBannerModbusTCP::Attach(IPortObject *pPort)
{
	}

void MCALL CBannerModbusTCP::Open(void)
{
	}

// Device

CCODE MCALL CBannerModbusTCP::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= HostToMotor(GetLong(pData));
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_bUnit		= GetByte(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CBannerModbusTCP::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CBannerModbusTCP::Ping(void)
{
	if( OpenSocket() ) {

		DWORD    Data[1];

		CAddress Addr;

		Addr.a.m_Table  = CREG;
		Addr.a.m_Offset = 1;
		Addr.a.m_Type   = addrWordAsWord;
		Addr.a.m_Extra	= 0;

		return Read(Addr, Data, 1);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CBannerModbusTCP::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	CAddress A;

	GetModbusConfig(Addr, &A);

	uCount = min( uCount, A.a.m_Type <= addrWordAsWord ? 16 : 8 );

	switch( A.a.m_Type ) {

		case addrWordAsLong:
		case addrLongAsLong:	return DoLongRead(A, pData, uCount);
			
		case addrWordAsWord:	return DoWordRead(A, pData, uCount);

		case addrBitAsBit:	return DoBitRead(A, pData, uCount);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CBannerModbusTCP::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !OpenSocket() ) return CCODE_ERROR;

	CAddress A;

	if( !GetModbusConfig(Addr, &A) ) return uCount; // Read Only

	uCount = min( uCount, A.a.m_Type <= addrWordAsWord ? 16 : 8 );

	switch( A.a.m_Type ) {

		case addrWordAsLong:
		case addrLongAsLong:	return DoLongWrite(A, pData, uCount);
			
		case addrWordAsWord:	return DoWordWrite(A, pData, uCount);

		case addrBitAsBit:	return DoBitWrite(A, pData, uCount);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Socket Management

BOOL CBannerModbusTCP::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CBannerModbusTCP::OpenSocket(void)
{
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_TCP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		if( m_pCtx->m_pSock->Connect(IP, WORD(uPort)) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CBannerModbusTCP::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Frame Building

void CBannerModbusTCP::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddWord(++m_pCtx->m_wTrans);

	AddWord(0);

	AddByte(0);

	AddByte(0);

	AddByte(m_pCtx->m_bUnit);
	
	AddByte(bOpcode);
	}

void CBannerModbusTCP::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;
		
		m_uPtr++;
		}
	}

void CBannerModbusTCP::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CBannerModbusTCP::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

// Transport Layer

BOOL CBannerModbusTCP::SendFrame(void)
{
	m_bTx[5] = BYTE(m_uPtr - 6);

	UINT uSize   = m_uPtr;

//**/AfxTrace0("\r\n"); for( UINT k=0; k<m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	if( m_pCtx->m_pSock->Send(m_bTx, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBannerModbusTCP::RecvFrame(void)
{
	SetTimer(m_pCtx->m_uTime2);

	UINT uPtr  = 0;

	UINT uSize = 0;

	while( GetTimer() ) {

		uSize = sizeof(m_bRx) - uPtr;

		m_pCtx->m_pSock->Recv(m_bRx + uPtr, uSize);

		if( uSize ) {

//**/AfxTrace0("\r\n"); for( UINT k=uPtr; k<uPtr+uSize; k++ ) AfxTrace1("<%2.2x>", m_bRx[k]);

			uPtr += uSize;

			if( uPtr >= 8 ) {

				UINT uTotal = 6 + m_bRx[5];

				if( uPtr >= uTotal ) {

					if( m_bRx[0] == m_bTx[0] ) {

						if( m_bRx[1] == m_bTx[1] ) {

							memcpy(m_bRx, m_bRx + 6, uPtr - 6);

							return TRUE;
							}
						}

					if( uPtr -= uTotal ) {

						for( UINT n = 0; n < uPtr; n++ ) {

							m_bRx[n] = m_bRx[uTotal++];
							}
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CBannerModbusTCP::Transact(BOOL fIgnore)
{
	if( SendFrame() && RecvFrame() ) {

		if( fIgnore ) {

			return TRUE;
			}

		return CheckFrame();
		}

	CloseSocket(TRUE);

	return FALSE;
	}

BOOL CBannerModbusTCP::CheckFrame(void)
{
	if( !(m_bRx[1] & 0x80) ) {
	
		return TRUE;
		}

	return FALSE;
	}

// Read Handlers

CCODE CBannerModbusTCP::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
	
		case SPH:
			StartFrame(0x03);
			break;
			
		case SPA:
			StartFrame(0x04);
			break;
			
		default:
			return CCODE_ERROR | CCODE_HARD;
		}
		
	AddWord(WORD(Addr.a.m_Offset - 1));
	
	AddWord(WORD(uCount));
	
	if( Transact(FALSE) ) {
		
		for( UINT n = 0; n < uCount; n++ ) {
		
			WORD x = PU2(m_bRx + 3)[n];
			
			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CBannerModbusTCP::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(SPH);

	AddWord(WORD(Addr.a.m_Offset - 1));

	AddWord(uCount * 2);

	if( Transact(FALSE) ) {

		for( UINT n = 0; n < uCount; n++ ) {

			DWORD x = U4(PDWORD(m_bRx + 3)[n]);

			pData[n] = MotorToHost((DWORD)((LOWORD(x) << 16) + HIWORD(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CBannerModbusTCP::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {
			
		case SPO:
			StartFrame(SPO);
			break;
			
		case SPI:
			StartFrame(SPI);
			break;

		default:
			return CCODE_ERROR | CCODE_HARD;
		}

	AddWord(WORD(Addr.a.m_Offset - 1));

	AddWord(WORD(uCount));

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}
		
	return CCODE_ERROR;
	}

// Write Handlers

CCODE CBannerModbusTCP::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {
			
		StartFrame(6);

		AddWord(WORD(Addr.a.m_Offset - 1));
			
		AddWord(LOWORD(pData[0]));
		}
	else {
		StartFrame(16);

		AddWord(WORD(Addr.a.m_Offset - 1));
			
		AddWord(WORD(uCount));
			
		AddByte(BYTE(uCount * 2));
			
		for( UINT n = 0; n < uCount; n++ ) AddWord(LOWORD(pData[n]));
		}

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CBannerModbusTCP::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uTable = Addr.a.m_Table;

	StartFrame(16);
		
	AddWord(WORD(Addr.a.m_Offset - 1));
		
	AddWord(uCount * 2);
		
	AddByte(BYTE(uCount * 4));
		
	for( UINT n = 0; n < uCount; n++ ) AddLong(pData[n]);

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

CCODE CBannerModbusTCP::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {

		StartFrame(5);

		AddWord(WORD(Addr.a.m_Offset - 1));
			
		AddWord(pData[0] ? WORD(0xFF00) : WORD(0x0000));
		}
	else {
		StartFrame(15);

		AddWord(WORD(Addr.a.m_Offset - 1));

		AddWord(WORD(uCount));

		AddByte(BYTE((uCount + 7) / 8));

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) b |= m;

			if( !(m <<= 1) ) {

				AddByte(BYTE(b));

				b = 0;

				m = 1;
				}
			}

		if( m > 1 ) {

			AddByte(BYTE(b));
			}
		}

	return Transact(TRUE) ? uCount : CCODE_ERROR;
	}

// Helpers
BOOL	CBannerModbusTCP::GetModbusConfig(AREF Addr, CAddress * pA)
{
	pA->a.m_Type   = Addr.a.m_Type;
	pA->a.m_Extra  = 0;
	pA->a.m_Offset = LOBYTE(Addr.a.m_Offset);
	pA->a.m_Table  = SPH;

	switch( Addr.a.m_Table ) {

		case CREG:
		case CIRC:
		case CIRM:
			pA->a.m_Offset = Addr.a.m_Offset;
			return WRITE_YES;

		case CO3L:
			pA->a.m_Offset = 33 + (2 * (pA->a.m_Offset-1));
// WAS			pA->a.m_Offset += 32;
			return WRITE_NO;

		case CO4L:
			pA->a.m_Offset = 1033 + (2 * (pA->a.m_Offset-1));
// WAS			pA->a.m_Offset += 1032;
			return WRITE_NO;
		}

	switch( Addr.a.m_Offset ) {

		case CIT:
		case CIR:
		case CIP:
		case CIS:
			pA->a.m_Table = SPI;
			return WRITE_YES;

		case COP:
		case COF:
		case COE:
		case COR:
		case CIO1:
		case CIO2:
		case CIO3:
		case CIO4:
		case CIO5:
		case CIO6:
		 
		case CAT:
		case CAR:
		case CAP:
		case CAS:
			pA->a.m_Table = SPO;
			return WRITE_NO;

		case CO3O:
		case CO3A:
		case CO3I:
		case CO3E:
		case CO3M:
		case CO3P:
		case CO3F:
		case CO3T:
		case CO3C:
			pA->a.m_Table = SPA;
			return WRITE_NO;

		case CO4O:
		case CO4A:
		case CO4I:
		case CO4E:
		case CO4M:
		case CO4P:
		case CO4F:
		case CO4T:
		case CO4C:
			pA->a.m_Offset = 1000 + LOBYTE(Addr.a.m_Offset);
			return WRITE_NO;

		case CIRI:
		case CIRP:
			return WRITE_YES;
		}

	return WRITE_NO;
	}

// End of File
