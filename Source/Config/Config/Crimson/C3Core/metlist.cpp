
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Support Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Meta Data List
//

// Runtime Class

AfxImplementRuntimeClass(CMetaList, CObject);

// Constructor

CMetaList::CMetaList(void)
{
	m_uPad  = 0;
	
	m_pName = NULL;

	m_Name  = L"Item";
	}

// Operations

UINT CMetaList::Add(PCTXT pTag, UINT Type, UINT Pos)
{
	return Add(CMetaData(pTag, Type, Pos));
	}

UINT CMetaList::Add(CMetaData const &Meta)
{
	CString Tag = Meta.GetTag();

	if( Meta.GetType() < metaObject ) {

		MakeMax(m_uPad, Tag.GetLength());
		}

	if( Meta.GetType() == metaRect ) {

		Add(L"/" + Tag + L"/x1", metaInteger, Meta.GetPos() + 0 * sizeof(int));
		
		Add(L"/" + Tag + L"/y1", metaInteger, Meta.GetPos() + 1 * sizeof(int));
		
		Add(L"/" + Tag + L"/x2", metaInteger, Meta.GetPos() + 2 * sizeof(int));
		
		Add(L"/" + Tag + L"/y2", metaInteger, Meta.GetPos() + 3 * sizeof(int));
		}

	if( Meta.GetType() == metaR2R ) {

		Add(L"/" + Tag + L"/x1", metaNumber, Meta.GetPos() + 0 * sizeof(number));
		
		Add(L"/" + Tag + L"/y1", metaNumber, Meta.GetPos() + 1 * sizeof(number));
		
		Add(L"/" + Tag + L"/x2", metaNumber, Meta.GetPos() + 2 * sizeof(number));
		
		Add(L"/" + Tag + L"/y2", metaNumber, Meta.GetPos() + 3 * sizeof(number));
		}

	UINT n = m_List.Append(Meta);

	BOOL f = m_Dict.Insert(Tag, n);

	AfxAssert(f);

	return n;
	}

void CMetaList::SetName(ENTITY Name)
{
	m_Name = Name;
	}

void CMetaList::SetName(PCTXT pName)
{
	m_pName = pName;
	}

// Attributes

UINT CMetaList::GetCount(void) const
{
	return m_List.GetCount();
	}

UINT CMetaList::GetPadding(void) const
{
	return m_uPad;
	}

CString CMetaList::GetName(void) const
{
	return m_pName ? CString(m_pName) : CString(m_Name);
	}

// Data Access

CMetaData const * CMetaList::FindData(UINT uSlot) const
{
	return &m_List[uSlot];
	}

CMetaData const * CMetaList::FindData(CString const &Name) const
{
	INDEX n = m_Dict.FindName(Name);

	if( !m_Dict.Failed(n) ) {

		UINT uSlot = m_Dict.GetData(n);

		return &m_List[uSlot];
		}

	return NULL;
	}

// End of File
