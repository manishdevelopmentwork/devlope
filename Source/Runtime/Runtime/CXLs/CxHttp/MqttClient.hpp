
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson MQTT Client
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_MqttClient_HPP

#define	INCLUDE_MqttClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MqttClientOptions.hpp"

#include "MqttMessage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

struct CMqttFixedHeader;

//////////////////////////////////////////////////////////////////////////
//
// MQTT Client
//

class DLLAPI CMqttClient
{
	public:
		// Constructor
		CMqttClient(CMqttClientOptions &Opts);

		// Destructor
		virtual ~CMqttClient(void);

		// Operations
		virtual BOOL Open(void);
		virtual BOOL Poll(UINT uID);

	protected:
		// Configuration
		CMqttClientOptions &m_Opts;

		// Phases
		enum
		{
			phaseInitial    = 0,
			phaseConnecting = 1,
			phaseConnected  = 2,
			phasePublishing = 3,
			phaseLive       = 4,
			phaseDropped    = 5,
			};

		// States
		enum
		{
			stateOpenSocket,
			stateWaitOpen,
			stateBackOff,
			stateSendConnect,
			stateRecvConnAck,
			stateSendSub,
			stateRecvSubAck,
			stateSendPub,
			stateWaitPubDone,
			stateRecvPubAck
			};

		// Subscribed Topic
		struct CSubDef
		{
			UINT	m_uTopic;
			CString	m_Filter;
			};

		// Subscription Type
		typedef CArray <CSubDef> CSubArray;

		// Subscriptions
		CSubArray m_SubList;

		// Will Information
		CMqttMessage m_Will;

		// Comms Buffer
		UINT  m_uRxData;
		UINT  m_uTxData;
		PBYTE m_pRxData;
		PBYTE m_pTxData;
		BOOL  m_fTxBusy;
		UINT  m_uTxInit;
		UINT  m_uTxSize;
		UINT  m_uRxSize;

		// Comms Context
		ITlsClientContext * m_pMatrix;
		ISocket	          * m_pSock;
		CMqttMessage      * m_pSendMsg;
		UINT	            m_PacketId;
		UINT		    m_uPhase;
		UINT		    m_uState;
		UINT		    m_uIndex;
		UINT		    m_uSecs;
		UINT64		    m_uMilli;
		UINT		    m_uConn;
		UINT		    m_uRecv;
		UINT		    m_uSend;
		UINT		    m_uBack;
		UINT		    m_uPing;
		UINT		    m_uPong;

		// Client Hooks
		virtual void OnClientPhaseChange(void);
		virtual void OnClientPublish(CByteArray const &Blob);
		virtual BOOL OnClientNewData(CMqttMessage const *pMsg);
		virtual BOOL OnClientGetData(CMqttMessage * &pMsg);
		virtual BOOL OnClientDataSent(CMqttMessage const *pMsg);

		// Handlers
		BOOL OnAppConnAck(void);
		BOOL OnAppSubAck(void);
		BOOL OnAppPubAck(void);
		BOOL OnAppNewData(CMqttMessage *pMsg);
		BOOL OnAppGetData(CMqttMessage * &pMsg);
		BOOL OnAppPingResp(void);

		// Socket Management
		BOOL OpenSocket(void);
		BOOL WaitOpen(void);
		BOOL CheckConnection(BOOL &fNew);
		BOOL IsConnectionOpen(void);
		BOOL AbortAndStep(void);
		void Abort(void);
		void Close(void);
		void StartBackoff(UINT uBack);

		// Packet Building
		void  NewPacket(void);
		PBYTE AddHead(UINT uSize);
		PBYTE AddTail(UINT uSize);
		BOOL  AddFixedHeader(UINT Type, UINT QoS);
		void  AddString(CString Text);
		void  AddBlob(CByteArray const &Blob);
		void  AddPayload(CByteArray const &Blob);
		void  AddPacketId(void);
		void  AddPacketId(UINT PacketId);
		void  AddByte(BYTE bData);

		// Packet Parsing
		PCBYTE StripHead(PCBYTE &pData, UINT &uRemain, UINT uSize);
		BOOL   GetString(PCBYTE &pData, UINT &uRemain, CString &Text);
		BOOL   GetPayload(PCBYTE &pData, UINT &uRemain, CByteArray &Blob);

		// Transport
		BOOL Send(void);
		BOOL PumpSend(void);
		BOOL PumpRecv(void);

		// Requests
		BOOL SendConnect(void);
		BOOL SendSubscribe(CString Topic, UINT QoS);
		BOOL SendPublish(CString Topic, CByteArray const &Blob);
		BOOL SendPubAck(UINT PacketId);
		BOOL SendPingReq(void);
		BOOL SendPingResp(void);

		// Replies
		BOOL OnRecv(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);
		BOOL OnConnAck(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);
		BOOL OnSubAck(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);
		BOOL OnPubAck(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);
		BOOL OnPublish(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);
		BOOL OnPingReq(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);
		BOOL OnPingResp(PCBYTE pData, CMqttFixedHeader *pFixed, UINT uRemain);

		// Implementation
		void UpdateTime(void);
		void AddToSubList(UINT uTopic, CString Filter);
		void StartPublishing(void);
		BOOL SetPhase(UINT uPhase);

		// Diagnostics
		void AfxTrace(PCTXT pText, ...);
		void AfxDump(PCVOID pData, UINT uCount);
	};

// End of File

#endif
