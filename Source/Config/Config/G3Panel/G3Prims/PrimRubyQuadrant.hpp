
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyQuadrant_HPP
	
#define	INCLUDE_PrimRubyQuadrant_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPartial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Quadrant Primitive
//

class CPrimRubyQuadrant : public CPrimRubyPartial
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyQuadrant(void);

	protected:
		// Meta Data
		void AddMetaData(void);

		// Path Generation
		void MakePath(CRubyPath &figure, CRubyPoint const &p1, CRubyPoint const &p2);
	};

// End of File

#endif
