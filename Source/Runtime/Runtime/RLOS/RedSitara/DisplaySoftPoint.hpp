
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_DisplaySoftPoint_HPP

#define INCLUDE_DisplaySoftPoint_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "DisplayBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Display Object with Soft Pointer
//

class CDisplaySoftPoint : public CDisplayBase, public IPointer
{
	public:
		// Constructor
		CDisplaySoftPoint(void);

		// Destructor
		~CDisplaySoftPoint(void);
		
		// IDevice
		BOOL Open(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IPointer
		void METHOD LockPointer(bool fLock);
		void METHOD SetPointerPos(int xPos, int yPos);
		void METHOD ShowPointer(bool fShow);

	protected:
		// Constants
		static int const cSize = 4;

		// Data Members
		PDWORD   m_pSave;
		UINT     m_uSave;
		IMutex * m_pMouse;
		bool	 m_fMouse;
		bool     m_fDrawn;
		bool     m_fUpdate;
		int      m_xMouse;
		int      m_yMouse;
		int      m_xDrawn;
		int      m_yDrawn;

		// Implementation
		void UpdatePointer(void);
		bool SavePointer(PDWORD pBase);
		bool LoadPointer(PDWORD pBase);
		bool DrawPointer(PDWORD pBase);
	};

// End of File

#endif
