
#include "intern.hpp"

#include "SymFactLibrary.hpp"

#include "SymFactCategory.hpp"

#include "SymFactSymbol.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Access
//

// Data

static CSymFactLibrary * m_pLib = NULL;

static BOOL		 m_fNew = TRUE;

// Code

void Sym_Init(void)
{
	if( !m_pLib ) {

		afxThread->SetWaitMode(TRUE);

		m_pLib = New CSymFactLibrary;

		afxThread->SetWaitMode(FALSE);
		}
	}

void Sym_Term(void)
{
	if( m_pLib ) {

		delete m_pLib;

		m_pLib = NULL;
		}
	}

void Sym_PreferNew(BOOL fNew)
{
	m_fNew = fNew;
	}

UINT Sym_GetCatCount(void)
{
	Sym_Init();

	return m_pLib->GetCatCount();
	}

CString Sym_GetCatDesc(UINT uCat)
{
	Sym_Init();

	return (*m_pLib)[uCat]->GetDesc();
	}

UINT Sym_GetCatSlots(UINT uCat)
{
	Sym_Init();

	return (*m_pLib)[uCat]->GetSymCount();
	}

BOOL Sym_IsValid(UINT uCat, UINT uSlot)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot] ? TRUE : FALSE;
	}

UINT Sym_GetType(UINT uCat, UINT uSlot)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetType(m_fNew);
	}

BOOL Sym_CanShade(UINT uCat, UINT uSlot)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->CanShade(m_fNew);
	}

DWORD Sym_GetHandle(UINT uCat, UINT uSlot)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetHandle();
	}

CString Sym_GetDesc(UINT uCat, UINT uSlot)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetDesc();
	}

UINT Sym_FindByHandle(UINT uCat, DWORD dwHandle)
{
	Sym_Init();

	return (*m_pLib)[uCat]->FindHandle(dwHandle);
	}

HBITMAP Sym_LoadBitmap(UINT uCat, UINT uSlot)
{
	int cx, cy;

	return Sym_LoadBitmap(uCat, uSlot, cx, cy);
	}

HBITMAP Sym_LoadBitmap(UINT uCat, UINT uSlot, int &cx, int &cy)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetBitmap(cx, cy);
	}

HENHMETAFILE Sym_LoadMetaFile(UINT uCat, UINT uSlot)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetMetaFile();
	}

BOOL Sym_LoadPng(UINT uCat, UINT uSlot, UINT uMode, DWORD Fill, DWORD Back, CSize Size, BOOL fKeep, CByteArray &Data)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetPng(uMode, Fill, Back, Size, fKeep, Data);
	}

UINT Sym_LoadData(UINT uCat, UINT uSlot, PBYTE &pCopy)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetData(pCopy);
	}

BOOL Sym_LoadData(UINT uCat, UINT uSlot, CByteArray &Data)
{
	Sym_Init();

	return (*(*m_pLib)[uCat])[uSlot]->GetData(Data);
	}

// End of File	      
