
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbHubClearPortFeatureReq_HPP

#define	INCLUDE_UsbHubClearPortFeatureReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Hub Framework
//

#include "Hub.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Clear Port Feature Request
//

class CUsbHubClearPortFeatureReq : public CUsbClassReq
{
	public:
		// Constructor
		CUsbHubClearPortFeatureReq(WORD wFeature, WORD wPort);

		// Operations
		void Init(WORD wFeature, WORD wPort);
	};

// End of File

#endif
