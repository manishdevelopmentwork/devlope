
#include "Intern.hpp"

#include "Gpmc437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 General Purpose Memory Controller
//

// Register Access

#define Reg(x)	      (m_pBase[reg##x])

#define RegN(x, n, s) (m_pBase[reg##x + (n * s / sizeof(DWORD))])

// Constructor

CGpmc437::CGpmc437(void)
{
	m_pBase    = PVDWORD(ADDR_GPMC_CONFIG);

	m_uLine    = INT_GPMC;

	m_pHook[0] = NULL;

	m_pHook[1] = NULL;
	
	Init();
	}

// Attributes

UINT CGpmc437::GetLine(void) const
{
	return m_uLine;
	}

// Operations

void CGpmc437::SetTimeout(DWORD dwTimeout)
{
	Reg(TIMEOUT) = dwTimeout ? (dwTimeout << 4) | Bit(0) : 0;
	}

void CGpmc437::SetLimitedAddress(bool fLimited)
{
	if( fLimited ) {

		AtomicBitSet(&Reg(CFG), 1);
		}
	else {
		AtomicBitClr(&Reg(CFG), 1);
		}
	}

void CGpmc437::SetWritProtect(bool fSet)
{
	if( fSet ) {

		AtomicBitSet(&Reg(CFG), 4);
		}
	else {
		AtomicBitClr(&Reg(CFG), 4);
		}
	}

// Chip Select

void CGpmc437::SetChipSelType(UINT i, UINT uType)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	

	r &= ~(0x3  << 10);

	r |= (uType << 10); 
	}

void CGpmc437::SetChipSelWidth(UINT i, UINT uWidth)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	

	uWidth = (uWidth == 16 ? 1 : 0);
	
	r &= ~(0x3   << 12);

	r |= (uWidth << 12); 
	}

void CGpmc437::SetChipSelMux(UINT i, UINT uMode)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	

	r &= ~(0x3  << 8);

	r |= (uMode << 8); 
	}

void CGpmc437::SetChipSelBurst(UINT i, UINT uLen)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	

	uLen = (uLen == 16 ? 2 : uLen == 8 ? 1 : 0);
	
	r &= ~(0x3 << 23);

	r |= (uLen << 23); 
	}

void CGpmc437::SetChipSelTimeGranularity(UINT i, UINT uGranularity)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	

	r &= ~Bit(4);

	r |= ((uGranularity - 1) << 4);
	}

void CGpmc437::SetChipSelWaitConfig(UINT i, UINT uPin, bool fActiveHigh)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	

	r &= ~(0x3 << 16);

	r |= (uPin << 16);

	UINT uBit = uPin + 8;

	if( fActiveHigh ) {

		AtomicBitSet(&Reg(CFG), uBit);
		}
	else {
		AtomicBitClr(&Reg(CFG), uBit);
		}
	}

void CGpmc437::SetChipSelAccessType(UINT i, bool fWriteAsync, bool fReadAsync)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	
	
	if( !fReadAsync ) {

		AtomicBitSet(&r, 29);
		}
	else {
		AtomicBitClr(&r, 29);
		}

	if( !fWriteAsync ) {

		AtomicBitSet(&r, 27);
		}
	else {
		AtomicBitClr(&r, 27);
		}
	}

void CGpmc437::SetChipSelAccessMode(UINT i, bool fWriteSingle, bool fReadSingle)
{
	DWORD volatile &r = RegN(CFG1x, i, 0x30);	
	
	if( !fReadSingle ) {

		AtomicBitSet(&r, 30);
		}
	else {
		AtomicBitClr(&r, 30);
		}

	if( !fWriteSingle ) {

		AtomicBitSet(&r, 28);
		}
	else {
		AtomicBitClr(&r, 28);
		}
	}

void CGpmc437::SetChipSelTiming(UINT i, UINT uOnTime, UINT uWrOffTime, UINT uRdOffTime, bool fExtraDelay)
{
	DWORD volatile &r = RegN(CFG2x, i, 0x30);

	r = 0;
	
	r |= (uWrOffTime << 16);
	
	r |= (uRdOffTime <<  8);
	
	r |= (uOnTime    <<  0);
	
	r |= fExtraDelay ? Bit(7) : 0;
	}

void CGpmc437::SetChipSelAdvTiming(UINT i, UINT uOnTime, UINT uWrOffTime, UINT uRdOffTime, bool fExtraDelay, UINT uMuxOnTime, UINT uMuxWrOffTime, UINT uMuxRdOffTime)
{
	DWORD volatile &r = RegN(CFG3x, i, 0x30);

	r = 0;

	r |= (uMuxWrOffTime << 28);
	
	r |= (uMuxRdOffTime << 24);
	
	r |= (uMuxOnTime    <<  4);
	
	r |= (uWrOffTime    << 16);
	
	r |= (uRdOffTime    <<  8);
	
	r |= (uOnTime       <<  0);
	
	r |= fExtraDelay ? Bit(7) : 0;
	}

void CGpmc437::SetChipSelWETimings(UINT i, UINT uOffTime, UINT uOnTime, bool fExtraDelay)
{
	DWORD volatile &r = RegN(CFG4x, i, 0x30);

	r &= ~0xFFFF0000;

	r |= (uOffTime << 24);
	
	r |= (uOnTime  << 16);
	
	r |= fExtraDelay ? Bit(23) : 0;
	}

void CGpmc437::SetChipSelOETimings(UINT i, UINT uOffTime, UINT uOnTime, bool fExtraDelay)
{
	DWORD volatile &r = RegN(CFG4x, i, 0x30);

	r &= ~((0x001F << 8) | Bit(7) | (0x000F << 0));

	r |= (uOffTime << 8);
	
	r |= (uOnTime  << 0);

	r |= fExtraDelay ? Bit(7) : 0;
	}

void CGpmc437::SetChipSelOEMuxTimings(UINT i, UINT uMuxOffTime, UINT uMuxOnTime)
{
	DWORD volatile &r = RegN(CFG4x, i, 0x30);

	r &= ~((0x7 << 13) | (0x7 << 4));

	r |= (uMuxOffTime << 13);
	
	r |= (uMuxOnTime  <<  4);
	}

void CGpmc437::SetChipSelRdAccessTiming(UINT i, UINT uBurst, UINT uRdAccess, UINT uWrCycle, UINT uRdCycle)
{
	DWORD volatile &r = RegN(CFG5x, i, 0x30);

	r = 0;

	r |= ( uBurst    << 24);

	r |= ( uRdAccess << 16);

	r |= ( uWrCycle  <<  8);

	r |= ( uRdCycle  <<  0);
	}

void CGpmc437::SetChipSelWrAccessTiming(UINT i, UINT uWrAccess, UINT uWrDataMux)
{
	DWORD volatile &r = RegN(CFG6x, i, 0x30);
	
	r &= ~0xFFFF0000;

	r |= ( uWrAccess  << 24);

	r |= ( uWrDataMux << 16);
	}

void CGpmc437::SetChipSelCycleDelayTiming(UINT i, UINT uDelay, UINT uDelaySame, UINT uDelayDiff, UINT uTurnAround)
{
	DWORD volatile &r = RegN(CFG6x, i, 0x30);
	
	r &= ~0x00000FFF;

	r |= ( uDelay      << 8);

	r |= ( uDelaySame  << 7);

	r |= ( uDelayDiff  << 6);
	
	r |= ( uTurnAround << 0);
	}

void CGpmc437::SetChipSelBase(UINT i, DWORD dwBase, UINT uSize)
{
	DWORD dwMask = 0x00;

	switch( uSize ) {

		case 256: dwMask = 0x0; break;
		case 128: dwMask = 0x8; break;
		case  64: dwMask = 0xC; break;
		case  32: dwMask = 0xE; break;
		case  16: dwMask = 0xF; break;
		};

	DWORD volatile &r = RegN(CFG7x, i, 0x30);

	r = (dwMask << 8) | (dwBase >> 24);
	}

void CGpmc437::SetChipSelEnable(UINT i, bool fEnable)
{
	DWORD volatile &r = RegN(CFG7x, i, 0x30);

	if( fEnable ) {

		AtomicBitSet(&r, 6);
		}
	else {
		AtomicBitClr(&r, 6);
		}
	}

void CGpmc437::SetChipSelEventHook(UINT i, IEventSink *pSink)
{
	if( i < elements(m_pHook) ) {
		
		m_pHook[i]  = pSink;

		Reg(IRQSTS) = Bit(8 + i);
		
		Reg(IRQEN) |= Bit(8 + i);
		}
	}

// ECC

void CGpmc437::SetEccCode(UINT uCode)
{
	Reg(ECCCFG) &= ~Bit(16);

	Reg(ECCCFG) |= (uCode << 16);
	}

void CGpmc437::SetEccChipSelect(UINT uChip)
{
	Reg(ECCCFG) &= ~(0x7 << 1);

	Reg(ECCCFG) |= (uChip << 1);
	}

void CGpmc437::SetEcc16(bool fBit16)
{
	if( fBit16 ) {
		
		Reg(ECCCFG) |= Bit(7);
		}
	else {
		Reg(ECCCFG) &= ~Bit(7);
		}
	}

void CGpmc437::SetEccBchConfig(UINT uBits, UINT uWrapMode, UINT uPageSize)
{
	Reg(ECCCFG) &= ~((0x3 << 12) | (0xF << 8) | (0x7 << 4));

	Reg(ECCCFG) |= ((uBits / 8) << 12);

	Reg(ECCCFG) |= (uWrapMode <<  8);

	Reg(ECCCFG) |= (((uPageSize / 512) - 1) <<  4);
	}

void CGpmc437::SetEccEnable(bool fEnable)
{
	if( fEnable ) {
		
		Reg(ECCCFG) |= Bit(0);
		}
	else {
		Reg(ECCCFG) &= ~Bit(0);
		}
	}

void CGpmc437::SetEccClear(void)
{
	Reg(ECCCTRL) |= Bit(8);
	}

void CGpmc437::SetEccPointer(UINT uPointer)
{
	Reg(ECCCTRL) &= ~(0xF);

	Reg(ECCCTRL) |= uPointer;
	}

void CGpmc437::SetEccSize(UINT uSize0, UINT uSize1)
{	
	Reg(SIZECFG) &= ~((0xFF << 22) | (0xFF << 12));

	Reg(SIZECFG) |= ((uSize1 << 22) | (uSize0 << 12));
	}

void CGpmc437::SetEccResultSize(UINT uSel, UINT uSizeSel)
{
	if( uSizeSel ) {

		Reg(SIZECFG) |= Bit(uSel - 1);
		}
	else {
		Reg(SIZECFG) &= ~Bit(uSel - 1);
		}
	}

DWORD CGpmc437::GetEccResult(UINT uSelect)
{
	return RegN(ECCRES, uSelect-1, 4);
	}

DWORD CGpmc437::GetBchResult(UINT uSelect)
{
	if( uSelect < 4 ) {

		return RegN(BCHRES0, uSelect, 4);
		}

	return RegN(BCHRES4, uSelect-4, 4);
	}

// Nand

void CGpmc437::PutNandCmd(UINT iChipSel, BYTE bCmd)
{
	BYTE volatile &r = (BYTE volatile &) RegN(CMDx, iChipSel, 0x30);

	r = bCmd;
	}

void CGpmc437::PutNandAddr(UINT iChipSel, BYTE bAddr)
{
	BYTE volatile &r = (BYTE volatile &) RegN(ADDRx, iChipSel, 0x30);

	r = bAddr;
	}

void CGpmc437::PutNandData(UINT iChipSel, BYTE bData)
{
	BYTE volatile &r = (BYTE volatile &) RegN(DATAx, iChipSel, 0x30);

	r = bData;
	}

BYTE CGpmc437::GetNandData(UINT iChipSel)
{
	BYTE volatile &r = (BYTE volatile &) RegN(DATAx, iChipSel, 0x30);

	return r;
	}

void CGpmc437::SendNandData(UINT iChipSel, PCBYTE pData, UINT uCount)
{
	BYTE  volatile &b = (BYTE  volatile &) RegN(DATAx, iChipSel, 0x30);

	DWORD volatile &d = (DWORD volatile &) RegN(DATAx, iChipSel, 0x30);

	while( uCount ) {

		if( uCount >= sizeof(DWORD) && !(DWORD(pData) & (sizeof(DWORD)-1)) ) {

			d = (PDWORD(pData))[0];

			pData  += sizeof(DWORD);

			uCount -= sizeof(DWORD);
			
			continue;
			}

		b = *pData++;

		uCount --;
		}
	}

void CGpmc437::ReadNandData(UINT iChipSel, PBYTE pData, UINT uCount)
{
	BYTE  volatile &b = (BYTE  volatile &) RegN(DATAx, iChipSel, 0x30);

	DWORD volatile &d = (DWORD volatile &) RegN(DATAx, iChipSel, 0x30);

	while( uCount ) {

		if( uCount >= sizeof(DWORD) && !(DWORD(pData) & (sizeof(DWORD)-1)) ) {

			(PDWORD(pData))[0] = d;

			pData  += sizeof(DWORD);

			uCount -= sizeof(DWORD);
			
			continue;
			}

		*pData++ = b;

		uCount --;
		}
	}

// IEventSink

void CGpmc437::OnEvent(UINT uLine, UINT uParam)
{
	if( uLine == m_uLine ) {

		for(;;) { 

			UINT uStatus = Reg(IRQSTS);
				
			if( uStatus & Bit(8) ) {
			
				Reg(IRQSTS) = Bit(8);

				m_pHook[0]->OnEvent(m_uLine, 0);
				
				continue;
				}

			if( uStatus & Bit(9) ) {
			
				Reg(IRQSTS) = Bit(9);

				m_pHook[1]->OnEvent(m_uLine, 1);
				
				continue;
				}

			break;
			}
		}
	}

// Implementation

void CGpmc437::Reset(void)
{
	Reg(SYSCFG) |= Bit(1);

	while( !(Reg(SYSSTS) & Bit(0)) );
	}

void CGpmc437::Init(void)
{
	Reset();

	Reg(SYSCFG)  = (1 << 3);

	Reg(IRQEN)   = 0;

	Reg(IRQSTS)  = Bit(9) | Bit(8);

	Reg(TIMEOUT) = 0; 

	EnableEvents();
	}

void CGpmc437::EnableEvents(void)
{
	phal->SetLineHandler(m_uLine, this, 0);
	
	phal->EnableLine(m_uLine, true);
	}

// End of File
