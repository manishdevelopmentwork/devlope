
#include "intern.hpp"

#include "euroum.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Universal Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CEurothermUMDeviceOptions, CUIItem);

// Constructor

CEurothermUMDeviceOptions::CEurothermUMDeviceOptions(void)
{
	m_Group	= 0;
	m_Unit	= 1;

	m_fEnableChannelID = FALSE;
	
	m_ChannelID	   = 0;
	}

// UI Managament

void CEurothermUMDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "EnableChannelID" ) {

		pWnd->EnableUI("EnableChannelID", TRUE);

		pWnd->EnableUI("ChannelID", m_fEnableChannelID);
		}

	if( Tag.IsEmpty() || Tag == "Group" ) {

		if( m_Group > 15 && m_Group != 126 ) {

			m_Group = 15;
			}

		pWnd->UpdateUI("Group");
		}

	if( Tag.IsEmpty() || Tag == "Unit" ) {

		if( m_Unit > 15 && m_Unit != 126 ) {

			m_Unit = 15;
			}

		pWnd->UpdateUI("Unit");
		}
	}

// Download Support

BOOL CEurothermUMDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(LOBYTE(m_Group));
	Init.AddByte(LOBYTE(m_Unit));
	Init.AddByte(BYTE(m_fEnableChannelID));
	Init.AddWord(WORD(m_ChannelID));

	return TRUE;
	}

// Meta Data Creation

void CEurothermUMDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Group);
	Meta_AddInteger(Unit);
	Meta_AddBoolean(EnableChannelID);
	Meta_AddInteger(ChannelID);
	}

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm Universal Master Driver
//

// Instantiator

ICommsDriver *	Create_EurothermUMDriver(void)
{
	return New CEurothermUMDriver;
	}

// Constructor

CEurothermUMDriver::CEurothermUMDriver(void)
{
	m_wID		= 0x334A;

	m_uType		= driverMaster;
	
	m_Manufacturer	= TEXT("Eurotherm");
	
	m_DriverName	= TEXT("BISYNCH ASCII");
	
	m_Version	= TEXT("1.01");
	
	m_ShortName	= TEXT("Eurotherm BISYNCH ASCII");

	AddSpaces();
	}

// Binding Control

UINT CEurothermUMDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CEurothermUMDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 7;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityEven;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CEurothermUMDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEurothermUMDeviceOptions);
	}

// Implementation

// Address Helpers

BOOL CEurothermUMDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{
	UINT    uOffset = 0;

	if( Text.GetLength() > 2 ) {

		if( Text[1] == '.' ) {

			if( Text[2] == '.' ) {

				Text = Text.Left(2);
				}

			else {
				Text = Text.Left(1); // '.' is part of type
				}
			}

		else {
			Text = Text.Left(2);
			}
		}

	if( Text[0] >= '!' && Text[0] <= 'z' && Text[1] >= '!' && Text[1] <= 'z' ) {

		uOffset = Text[0] << 8;

		uOffset |= Text[1];
		}

	else uOffset = 0;

	if( pSpace->IsOutOfRange(uOffset) ) {
		
		Error.Set( CString(IDS_ERROR_OFFSETRANGE),
			   0
			   );
				
		return FALSE;
		}

	Addr.a.m_Type	= pSpace->m_uType;

	Addr.a.m_Table	= pSpace->m_uTable;
	
	Addr.a.m_Extra	= 0;

	Addr.a.m_Offset	= uOffset;
	
	return TRUE;
	}

BOOL CEurothermUMDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		Text.Printf( TEXT("%s%c%c"),
			pSpace->m_Prefix,
			HIBYTE(LOWORD(Addr.m_Ref)),
			LOBYTE(LOWORD(Addr.m_Ref))
			);

		return TRUE;
		}

	return FALSE;
	}

// protected

void CEurothermUMDriver::AddSpaces(void)
{
	AddSpace(New CSpace(1,	"RP",	"Real Parameter",		16, 0x2121, 0x7a7a, addrRealAsReal));
	AddSpace(New CSpace(2,	"IP",	"Integer Parameter",		16, 0x2121, 0x7a7a, addrLongAsLong));
	AddSpace(New CSpace(3,	"BP",	"Bit Parameter",		16, 0x2121, 0x7a7a, addrBitAsBit));
	}

// End of File
