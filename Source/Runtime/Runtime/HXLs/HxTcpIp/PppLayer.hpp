
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_PppLayer_HPP

#define	INCLUDE_PppLayer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CPpp;

//////////////////////////////////////////////////////////////////////////
//
// Layer Signals
//

enum
{
	layerStarted		= 0,
	layerFinished		= 1,
	layerUp			= 2,
	layerDown		= 3,
	layerFailed		= 4,
	};

//////////////////////////////////////////////////////////////////////////
//
// LCP Frame
//

#pragma pack(1)

struct LCPFRAME
{
	BYTE	m_bCode;
	BYTE	m_bID;
	WORD	m_wLength;
	};

#pragma pack()

//////////////////////////////////////////////////////////////////////////
//
// LCP Frame Wrapper
//

class CLcpFrame : public LCPFRAME
{
	public:
		// Conversion
		void NetToHost(void);
		void HostToNet(void);

		// Data Members
		BYTE m_bData[];

	private:
		// Constructor
		CLcpFrame(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// PPP Layer Base Class
//

class CPppLayer
{
	public:
		// Constructor
		CPppLayer(CPpp *pPpp, WORD wProtocol);

	protected:
		// Data Members
		CPpp * m_pPpp;
		WORD   m_wProtocol;
		UINT   m_uObj;

		// Actions
		void ThisLayerUp(void);
		void ThisLayerDown(void);
		void ThisLayerStarted(void);
		void ThisLayerFinished(void);
		void ThisLayerFailed(void);

		// Implementation
		BOOL ShowMessage(CBuffer *pBuff);
	};

// End of File

#endif
