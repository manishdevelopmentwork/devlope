
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PcapBuffer_HPP

#define	INCLUDE_PcapBuffer_HPP

//////////////////////////////////////////////////////////////////////////
//
// Packet Capture Buffer
//

class DLLAPI CPcapBuffer
{
	public:
		// Constructor
		CPcapBuffer(void);

		// Destructor
		~CPcapBuffer(void);

		// Attributes
		UINT   GetSize(void) const;
		PCBYTE GetData(void) const;

		// Operations
		void Empty(void);
		void SetLimit(UINT uLimit);
		void Initialize(UINT uNet);
		BOOL Capture(PCBYTE pData1, UINT uData1, PCBYTE pData2, UINT uData2);
		BOOL Capture(PCBYTE pData1, UINT uData1);

	protected:
		// Data Members
		CByteArray m_Data;
		IMutex *   m_pLock;
		UINT	   m_uLimit;
	};

// End of File

#endif
