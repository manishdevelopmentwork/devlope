
#include "Intern.hpp"

#include "FTPServer.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"

//////////////////////////////////////////////////////////////////////////
//
// FTP Server Configuration
//

// Dynamic Class

AfxImplementDynamicClass(CFTPServer, CServiceItem);

// Constructor

CFTPServer::CFTPServer(void)
{
	m_pEnable     = NULL;

	m_pAnon       = NULL;

	m_pTlsMode    = NULL;

	m_pLogFile    = NULL;

	m_pPortNumber = NULL;

	m_Debug       = 0;
}

// Initial Values

void CFTPServer::SetInitValues(void)
{
	SetInitial(L"Enable", m_pEnable, 0);

	SetInitial(L"Anon", m_pAnon, 0);

	SetInitial(L"TlsMode", m_pTlsMode, 1);

	SetInitial(L"LogFile", m_pLogFile, 0);

	SetInitial(L"PortNumber", m_pPortNumber, 21);
}

// UI Update

void CFTPServer::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
	}

	if( Tag == "Enable" ) {

		DoEnables(pHost);
	}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
}

// Type Access

BOOL CFTPServer::GetTypeData(CString Tag, CTypeDef &Type)
{
	Type.m_Type  = typeInteger;

	Type.m_Flags = 0;

	return TRUE;
}

// Attributes

BOOL CFTPServer::IsEnabled(void) const
{
	return FALSE;
}

UINT CFTPServer::GetTreeImage(void) const
{
	return IDI_FTP_SERVER;
}

// Download Support

BOOL CFTPServer::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_SERVICE);

	Init.AddByte(BYTE(2));

	CServiceItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Debug));

	Init.AddItem(itemVirtual, m_pEnable);
	Init.AddItem(itemVirtual, m_pAnon);
	Init.AddItem(itemVirtual, m_pTlsMode);
	Init.AddItem(itemVirtual, m_pLogFile);
	Init.AddItem(itemVirtual, m_pPortNumber);

	return TRUE;
}

// Meta Data Creation

void CFTPServer::AddMetaData(void)
{
	CServiceItem::AddMetaData();

	Meta_AddVirtual(Enable);
	Meta_AddVirtual(Anon);
	Meta_AddVirtual(TlsMode);
	Meta_AddVirtual(LogFile);
	Meta_AddVirtual(PortNumber);
	Meta_AddInteger(Debug);

	Meta_SetName((IDS_FTP_SERVER));
}

// Implementation

void CFTPServer::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = CheckEnable(m_pEnable, L">=", 1, TRUE);

	pHost->EnableUI("Anon", fEnable);

	pHost->EnableUI("LogFile", fEnable);

	pHost->EnableUI("TlsMode", fEnable);

	pHost->EnableUI("PortNumber", fEnable);

	pHost->EnableUI("Debug", fEnable);
}

// End of File
