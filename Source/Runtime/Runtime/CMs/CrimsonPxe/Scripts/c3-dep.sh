#!/bin/sh

# c3-dep <ifup|ifdn> <interface>
#
# Checking the specified action, find a list of dependent interfaces from
# the deps file and then bring those interfaces up or down. This mechanism
# is used from bring up an interface on startup if they are dependent on
# init or boot, or when another interface becomes active. This latter mode
# is used for tunnels. If we're dealing with an IPSec tunnel, we do a little
# extra work to start and shutdown the daemon depending on what is active.

list=`cat /vap/opt/crimson/config/net/deps | grep x${2}x | cut -f2- -d' '`

if [ "$list" == "" ]
then
	logger -t c3-net "$1 list for $2 is empty"
else
	logger -t c3-net "$1 list for $2 is $list"

	for i in $list
	do
		if [[ $i == ipsec* ]]
		then
			work=/tmp/crimson/up

			flag=$work/$2.ipsec

			if [ "$1" == "ifup" ]
			then
				if [ ! -e $flag ]
				then
					touch $flag

					logger -t c3-net "bringing up ipsec"

					init=/vap/opt/crimson/config/ipsec/init

					$init
				fi
			fi
		fi
	done

	for i in $list
	do
		/opt/crimson/scripts/c3-$1 $i $3 &
	done

	wait

	for i in $list
	do
		if [[ $i == ipsec* ]]
		then
			if [ "$1" == "ifdn" ]
			then
				if [ -e $flag ]
				then
					rm $flag

					if [ `ls -1 $work/*.ipsec 2> /dev/null | wc -l` -eq 0 ]
					then
						logger -t c3-net "taking down ipsec"

						term=/vap/opt/crimson/config/ipsec/term

						$term
					fi
				fi
			fi
			break
		fi
	done
fi
