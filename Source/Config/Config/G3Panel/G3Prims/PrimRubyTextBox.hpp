
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyTextBox_HPP
	
#define	INCLUDE_PrimRubyTextBox_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyRect.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Text Box Primitive
//

class CPrimRubyTextBox : public CPrimRubyRect
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyTextBox(void);

		// Operations
		void Set(CString Text, CSize MaxSize);

		// Overridables
		void  SetInitState(void);
		void  FindTextRect(void);
		CSize GetMinSize(IGDI *pGDI);
	
	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
