/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** IO.C
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Handles IO packets
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#define ET_IP_CLIENT

#include "intern.hpp"

/*---------------------------------------------------------------------------
** ioParseClass1Packet( )
**
** Received I/O data on Class1 connection
**---------------------------------------------------------------------------
*/

INT32  ioParseClass1Packet( UINT32 lOriginatingAddress, UINT8* pBuf, INT32 lBytesReceived )
{	
	UINT32  lConnID;	
	CPFHDR* pHdr;
	UINT8*  pData;
	UINT16  iLen = 0;
	INT32   nPacketOffset = 0;
	UINT16  iInDataSeqNbr;
//	BOOL    bInputChanged = FALSE;
	INT32   nConnection;
	INT32   i;
	BOOL	bMatchFound = FALSE;
	
	while( nPacketOffset < lBytesReceived )
	{
		pHdr = (CPFHDR *)&pBuf[nPacketOffset];	

		/* Check if header is valid */
		if ( pHdr->iS_count != ENCAP_TO_HS(2) )
		{
			DumpStr0("ioParseClass1Packet invalid iS_count");
			return nPacketOffset;
		}

		if ( pHdr->iAs_type != ENCAP_TO_HS(CPF_TAG_ADR_SEQUENCED) )
		{
			DumpStr0("ioParseClass1Packet invalid iAs_type");
			return nPacketOffset;
		}

		if ( pHdr->iAs_length != ENCAP_TO_HS(8) )
		{
			DumpStr0("ioParseClass1Packet invalid iAs_length");
			return nPacketOffset;
		}

		lConnID = UINT32_GET( pHdr->aiAs_cid );	/* Find Class1 connection the data is for */
				
		if ( pHdr->iDs_type != ENCAP_TO_HS(CPF_TAG_PKT_CONNECTED) )
		{
			DumpStr0("ioParseClass1Packet invalid iDs_type");
			return nPacketOffset;
		}

		for( nConnection = 0; nConnection < gnConnections; nConnection++ )
		{
			if ( lOriginatingAddress == gConnections[nConnection].lIPAddress && 
				 lConnID == gConnections[nConnection].lConsumingCID && 
				 gConnections[nConnection].cfg.bTransportClass == Class1 &&
				 gConnections[nConnection].iOwnerConnectionSerialNbr == INVALID_CONNECTION_SERIAL_NBR ) /* If it's a listen only incoming connection disregard the input */
			{
				bMatchFound = TRUE;
								
				iInDataSeqNbr = UINT16_GET( &pBuf[nPacketOffset+CPFHDR_SIZE] );
						
				iLen = UINT16_GET( &pBuf[nPacketOffset+CPFHDR_SIZE-sizeof(UINT16)] ) - sizeof(UINT16);
				pData = &pBuf[nPacketOffset+CPFHDR_SIZE+sizeof(UINT16)];
				
				for( i = 0; i < iLen; i++ )
				{
					if ( pData[i] != inputAssembly[gConnections[nConnection].cfg.iInputDataOffset+i] )
						break;
				}

				if ( i < iLen )
				{		
					assemblySetConnectionInputData( nConnection, pData, iLen );	/* Store data in assembly object */					
					notifyEvent( NM_CONNECTION_NEW_INPUT_DATA, gConnections[nConnection].cfg.nInstance );			
				}

				gConnections[nConnection].iInDataSeqNbr = iInDataSeqNbr;
				DumpStr1("connectionResetConsumingTicks for connection %d", nConnection);
				connectionResetConsumingTicks( nConnection );		/* Reset timeout ticks */
			}
		}

		if ( !bMatchFound )
		{
			DumpStr0("ioParseClass1Packet could not match connection IP address / Consuming Id combination");
			return nPacketOffset;
		}

		nPacketOffset += (CPFHDR_SIZE + sizeof(UINT16) + iLen);
	}
	
	return ((INT32)nPacketOffset);
}

/*---------------------------------------------------------------------------
** ioSendClass1Packet( )
**
** Produce on Class1 connection
**---------------------------------------------------------------------------
*/
INT32  ioSendClass1Packet( INT32 nConnection )
{
	UINT8*  pData;
	CPFHDR* pHdr = (CPFHDR*)gmsgBuf;
	INT32   nIOLen, nExtLen;
	
	/* If it's disconnected or this is a listen only incoming connection, do not 
	   send anything */
	if ( gConnections[nConnection].lConnectionState != ConnectionEstablished ||
		 gConnections[nConnection].iOwnerConnectionSerialNbr != INVALID_CONNECTION_SERIAL_NBR )
		return ERROR_STATUS;

	nIOLen = gConnections[nConnection].cfg.iOutputDataSize;	

	/* If Run/Program header is used add 4 bytes to the length */
	nExtLen = gConnections[nConnection].cfg.bOutputRunProgramHeader ? (nIOLen + sizeof(UINT32)) : nIOLen;		
		
	/* Populate the message header */
	memset( gmsgBuf, 0, (nExtLen + sizeof(UINT16) + CPFHDR_SIZE));

	UINT16_SET( &pHdr->iS_count, 2 ); /* number of objects in the header */
	UINT16_SET( &pHdr->iAs_type, CPF_TAG_ADR_SEQUENCED );
	UINT16_SET( &pHdr->iAs_length, 8 ); /* size of the addr header */
	UINT32_SET( pHdr->aiAs_cid, gConnections[nConnection].lProducingCID );
	
	UINT32_SET( pHdr->aiAs_seq, gConnections[nConnection].lAddrSeqNbr );
	gConnections[nConnection].lAddrSeqNbr++;

	UINT16_SET( &pHdr->iDs_type, CPF_TAG_PKT_CONNECTED );
	UINT16_SET( &pHdr->iDs_length, (nExtLen + sizeof(UINT16)));
		
	pData = &gmsgBuf[CPFHDR_SIZE];
	UINT16_SET( pData, gConnections[nConnection].iOutDataSeqNbr );		
	pData += sizeof(UINT16);

	if ( gConnections[nConnection].cfg.bOutputRunProgramHeader )
	{
		UINT32_SET( pData, 0 );
#ifdef ET_IP_SCANNER	
		*pData = gbRunMode ? 1 : 0;
#endif
		pData += sizeof(UINT32);
	}

	assemblyGetConnectionOutputData( nConnection, pData, nIOLen );	/* Get data from assembly object */
		
	return socketClass1SendData( nConnection );					/* Produce data */	
}


