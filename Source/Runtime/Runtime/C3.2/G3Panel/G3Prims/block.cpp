
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Block
//

// Constructor

CPrimBlock::CPrimBlock(CPrim *pPrim)
{
	m_pPrim    = pPrim;

	m_AlignH   = 1;
	
	m_AlignV   = 2;

	m_Lead     = 2;

	m_Font     = fontHei16;

	m_MoveDir  = 0;

	m_MoveStep = 2;
	}

// Destructor

CPrimBlock::~CPrimBlock(void)
{
	}

// Initialization

void CPrimBlock::Load(PCBYTE &pData)
{
	m_AlignH    = GetByte(pData);
	m_AlignV    = GetByte(pData);
	m_Lead      = GetWord(pData);
	m_Font      = GetWord(pData);
	m_MoveDir   = GetByte(pData);
	m_MoveStep  = GetByte(pData);
	m_Margin.x1 = GetWord(pData);
	m_Margin.y1 = GetWord(pData);
	m_Margin.x2 = GetWord(pData);
	m_Margin.y2 = GetWord(pData);
	}

BOOL CPrimBlock::IsAntiAliased(void) const
{
	return CUISystem::m_pThis->m_pUI->m_pFonts->IsAntiAliased(m_Font);
	}

// Operations

BOOL CPrimBlock::SelectFont(IGDI *pGDI)
{
	if( m_Font < 0x100 ) {

		pGDI->SelectFont(m_Font);

		return TRUE;
		}

	return CUISystem::m_pThis->m_pUI->m_pFonts->Select(pGDI, m_Font);
	}

// End of File
