
#include "intern.hpp"

#include "PrimRubyPolygon5.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 5-Sided Polygon Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPolygon5, CPrimRubyPolygon)

// Constructor

CPrimRubyPolygon5::CPrimRubyPolygon5(void)
{
	m_nSides = 5;

	m_Rotate = -900;
	}

// Meta Data

void CPrimRubyPolygon5::AddMetaData(void)
{
	CPrimRubyPolygon::AddMetaData();

	Meta_SetName((IDS_PENTAGON_2));
	}

// End of File
