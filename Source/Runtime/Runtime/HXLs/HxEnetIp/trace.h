/****************************************************************************
*****************************************************************************
**
** File Name
** ---------
**
** TRACE.H
**
** COPYRIGHT (c) 2001,2002 Pyramid Solutions, Inc.
**
*****************************************************************************
*****************************************************************************
**
** Description
** -----------
**
** Outputting trace dump to the disk file or the debugger window
**
** To use trace functionality there should be two preprocessor definitions
** set: TRACE_OUTPUT enabling general trace support and either
** TRACE_FILE_OUTPUT to send it to the file or TRACE_DEBUGGER_OUTPUT
** to output to the debug window. 
** In case of the file dump the program will output to the debug%n.txt file
** in the et_ip directory (or root directory for WinCE) on the c: drive.
**
*****************************************************************************
*****************************************************************************
**
** Source Change Indices
** ---------------------
**
** Porting: <none>0----<major>         Customization: <none>0----<major>
**
*****************************************************************************
*****************************************************************************
*/

#ifndef TRACE_H
#define TRACE_H

#ifdef TRACE_OUTPUT

extern FILE* stream;			/* File handle in case we are outputting to the file */

extern void InitializeDump();						/* Prepare for the debug dump */
extern void FinishDump();							/* Cleanup after debug dump */
extern void fDumpStr(const char* lpszFormat, ...);	/* Variable number of parameters supported */
extern void fDumpStrNoNewLine(const char* lpszFormat, ...);	/* Variable number of parameters supported */
extern void fDumpBuf(char* pBuf, int nLen);			/* Output the buffer contents as a string of bytes */

#endif /* #ifdef TRACE_OUTPUT */

#ifdef TRACE_OUTPUT
#define DumpStr0(a)	fDumpStr(a)
#define DumpStr1(a, b)	fDumpStr(a, b)
#define DumpStr2(a, b, c)	fDumpStr(a, b, c)
#define DumpStr3(a, b, c, d)	fDumpStr(a, b, c, d)
#define DumpStr4(a, b, c, d, e)	fDumpStr(a, b, c, d, e)
#define DumpStr5(a, b, c, d, e, f)	fDumpStr(a, b, c, d, e, f)
#define DumpStr6(a, b, c, d, e, f, g)	fDumpStr(a, b, c, d, e, f, g)
#define DumpStr7(a, b, c, d, e, f, g, h)	fDumpStr(a, b, c, d, e, f, g, h)
#define DumpStr8(a, b, c, d, e, f, g, h, i)	fDumpStr(a, b, c, d, e, f, g, h, i)
#define DumpStr9(a, b, c, d, e, f, g, h, i, j)	fDumpStr(a, b, c, d, e, f, g, h, i, j)
#define DumpStr10(a, b, c, d, e, f, g, h, i, j, k)	fDumpStr(a, b, c, d, e, f, g, h, i, j, k)
#define DumpStr11(a, b, c, d, e, f, g, h, i, j, k, l)	fDumpStr(a, b, c, d, e, f, g, h, i, j, k, l)
#define DumpBuf(a, b)	fDumpBuf(a, b)
#define DumpStr0NoNewLine(a)	fDumpStrNoNewLine(a)
#define DumpStr1NoNewLine(a, b)	fDumpStrNoNewLine(a, b)
#else		/* If TRACE_OUTPUT is not defined replace Dump* statements with nothing */
#define DumpStr0(a)	
#define DumpStr1(a, b)	
#define DumpStr2(a, b, c)	
#define DumpStr3(a, b, c, d)	
#define DumpStr4(a, b, c, d, e)	
#define DumpStr5(a, b, c, d, e, f)	
#define DumpStr6(a, b, c, d, e, f, g)	
#define DumpStr7(a, b, c, d, e, f, g, h)	
#define DumpStr8(a, b, c, d, e, f, g, h, i)	
#define DumpStr9(a, b, c, d, e, f, g, h, i, j)	
#define DumpStr10(a, b, c, d, e, f, g, h, i, j, k)	
#define DumpStr11(a, b, c, d, e, f, g, h, i, j, k, l)	
#define DumpBuf(a, b)	
#define DumpStr0NoNewLine(a)	
#define DumpStr1NoNewLine(a, b)	 
#endif

#endif /* #ifndef TRACE_H */
