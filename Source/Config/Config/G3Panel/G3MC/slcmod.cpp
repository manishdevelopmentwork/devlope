
#include "intern.hpp"

#include "legacy.h"

#include "slcmod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module
//

// Dynamic Class

AfxImplementDynamicClass(CSLCModule, CGenericModule);

// Constructor

CSLCModule::CSLCModule(void)
{
	m_pMapper = New CSLCMapper;

	m_pLoop   = New CPIDLoop(0);

	m_Ident   = ID_CSPID1;

	m_FirmID  = FIRM_PID1;

	m_Model   = "CSPID1";

	m_Conv.Insert(L"Graphite", L"CGMPID1Module");

	m_Conv.Insert(L"Manticore", L"CDAPID1Module");
	}

// UI Management

CViewWnd * CSLCModule::CreateMainView(void)
{
	return New CSLCMainWnd;
	}

// Comms Object Access

UINT CSLCModule::GetObjectCount(void)
{
	return 2;
	}

BOOL CSLCModule::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_LOOP);
			
			Data.pItem = m_pLoop;

			return TRUE;

		case 1:
			Data.ID    = OBJ_MAPPER;

			Data.Name  = CString(IDS_MODULE_OUTPUTS);
			
			Data.pItem = m_pMapper;

			return TRUE;
		}

	return FALSE;
	}

// Conversion

BOOL CSLCModule::Convert(CPropValue *pValue)
{
	if( pValue ) {

		m_pLoop->  Convert(pValue->GetChild(L"Loop"));

		m_pMapper->Convert(pValue->GetChild(L"Mapper"));
		
		return TRUE;
		}
	
	return FALSE;
	}

// Implementation

void CSLCModule::AddMetaData(void)
{
	Meta_AddObject(Mapper);

	Meta_AddObject(Loop);

	CGenericModule::AddMetaData();
	}

// Download Support

void CSLCModule::MakeConfigData(CInitData &Init)
{
	if( m_pLoop->m_InputType == INPUT_RTD || m_pLoop->m_InputType == INPUT_TC ) {

		Init.AddWord(WORD(m_pLoop->m_TempUnits));

		Init.AddWord(0);

		Init.AddWord(0);

		Init.AddWord(0);
		}
	else {
		Init.AddWord(0xFFFF);

		Init.AddWord(WORD(m_pLoop->m_ProcMin));

		Init.AddWord(WORD(m_pLoop->m_ProcMax));

		Init.AddWord(0);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Single Loop Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CSLCMainWnd, CProxyViewWnd);

// Constructor

CSLCMainWnd::CSLCMainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overribables

void CSLCMainWnd::OnAttach(void)
{
	m_pItem = (CSLCModule *) CProxyViewWnd::m_pItem;

	AddPIDPages();

	AddMapPages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CSLCMainWnd::AddPIDPages(void)
{
	CPIDLoop *pLoop = m_pItem->m_pLoop;

	for( UINT n = 0; n < pLoop->GetPageCount(); n++ ) {

		CViewWnd *pPage = pLoop->CreatePage(n);

		m_pMult->AddView(pLoop->GetPageName(n), pPage);

		pPage->Attach(pLoop);
		}
	}

void CSLCMainWnd::AddMapPages(void)
{
	CSLCMapper *pMapper = m_pItem->m_pMapper;

	for( UINT n =0; n < pMapper->GetPageCount(); n++ ) {

		CViewWnd *pPage = pMapper->CreatePage(n);

		m_pMult->AddView(pMapper->GetPageName(n), pPage);

		pPage->Attach(pMapper);
		}
	}

// End of File
