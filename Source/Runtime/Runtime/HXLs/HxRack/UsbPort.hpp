
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Shared_UsbPort_HPP
	
#define	INCLUDE_Shared_UsbPort_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

#include "UsbModule.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Port
//

class CUsbPort : public CUsbModule, public IPortObject, public IUsbHostFuncEvents
{
	public:
		// Constructor
		CUsbPort(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPortObject
		void  METHOD Bind(IPortHandler *pHandler);
		void  METHOD Bind(IPortSwitch  *pSwitch);
		BOOL  METHOD Open(CSerialConfig const &Config);
		void  METHOD Close(void);
		void  METHOD Send(BYTE bData);
		void  METHOD SetBreak(BOOL fBreak);
		void  METHOD EnableInterrupts(BOOL fEnable);
		void  METHOD SetOutput(UINT uOutput, BOOL fOn);
		BOOL  METHOD GetInput(UINT uInput);
		DWORD METHOD GetHandle(void);

		// IUsbHostFuncEvents
		void METHOD OnPoll(UINT uLapsed);
		void METHOD OnData(void);
		void METHOD OnTerm(void);
	};

// End of File

#endif
