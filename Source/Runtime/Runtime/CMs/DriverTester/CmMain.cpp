
#include "Intern.hpp"

#include "Comms.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

clink IUnknown * Create_AppObject(PCSTR pName);

clink IUnknown * Create_TestManager(PCSTR pName);

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

clink void AeonCmMain(void)
{
	piob->RegisterInstantiator("app", Create_AppObject);
	
	piob->RegisterInstantiator("test-manager", Create_TestManager);

	Driver_Register(CModbusTCPSlave);

	Tester_Register(CModbusTCPSlaveTester);
	}

clink void AeonCmExit(void)
{
	piob->RevokeInstantiator("app");

	piob->RevokeInstantiator("test-manager");

	piob->RevokeInstantiator("driver");

	piob->RevokeInstantiator("tester");
	}

// End of File
