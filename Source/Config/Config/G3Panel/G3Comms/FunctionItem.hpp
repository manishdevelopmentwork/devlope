
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_FunctionItem_HPP

#define INCLUDE_FunctionItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CNameServer;

//////////////////////////////////////////////////////////////////////////
//
// Function Item
//

class CFunctionItem : public CMetaItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFunctionItem(void);

		// Atributes
		CString GetPrototype(void);
		CString GetTreeName(void);

		// Persistance
		void Init(void);

		// Data Members
		CString	m_Name;
		UINT    m_Ident;

	protected:
		// Data
		CNameServer * m_pServer;

		// Meta Data
		void AddMetaData(void);

		// Implementation
		BOOL    FindFunction(CString &Name, CTypeDef &Type, UINT &uCount);
		CString TextFromType(UINT uType);
	};

// End of File

#endif
