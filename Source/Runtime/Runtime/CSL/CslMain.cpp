
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Export Macro
//

#if defined(AEON_PLAT_WIN32)

#define EXPORT __declspec(dllexport)

#elif defined(AEON_PLAT_RLOS)

#define EXPORT __attribute__((used, section(".exports")))

#else

#define EXPORT /**/

#endif

//////////////////////////////////////////////////////////////////////////
//
// RLOS Linker Hack
//

#if defined(AEON_PLAT_RLOS)

#if !defined(__INTELLISENSE__)

static DWORD zss __attribute__((section(".zss"))) = 0;

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// TLS Valid in Client
//

int _tls_valid = 1;

//////////////////////////////////////////////////////////////////////////
//
// Object Broker Pointer
//

global IObjectBroker * piob;

//////////////////////////////////////////////////////////////////////////
//
// Externals
//

extern BOOL CallCtors(void);

extern void Bind_ExceptionIndex(void);
extern void Free_ExceptionIndex(void);

extern void Bind_Malloc(void);
extern void Free_Malloc(void);

extern void Bind_RtlSupport(void);
extern void Free_RtlSupport(void);

extern void Bind_FileSupport(void);
extern void Free_FileSupport(void);

extern void Bind_Debug(void);
extern void Free_Debug(void);

extern void Bind_Executive(void);
extern void Free_Executive(void);

extern void Bind_BuffMan(void);
extern void Free_BuffMan(void);

//////////////////////////////////////////////////////////////////////////
//
// CM Entry Points
//

clink void AeonCmMain(void);

clink void AeonCmExit(void);

//////////////////////////////////////////////////////////////////////////
//
// CSL Entry Points
//

clink void EXPORT AeonCslMain(IObjectBroker *pBroker)
{
	piob = pBroker;

	Bind_ExceptionIndex();

	Bind_Malloc();

	Bind_RtlSupport();

	Bind_FileSupport();

	Bind_Debug();

	Bind_Executive();

	Bind_BuffMan();

	CallCtors();

	AeonCmMain();
	}

clink void EXPORT AeonCslExit(void)
{
	AeonCmExit();

	Free_BuffMan();

	Free_Executive();

	Free_Debug();

	Free_FileSupport();

	Free_RtlSupport();

	Free_Malloc();

	Free_ExceptionIndex();
	}

//////////////////////////////////////////////////////////////////////////
//
// DLL Entry Point
//

#if defined(AEON_PLAT_WIN32)

clink BOOL __stdcall _DllMainCRTStartup(UINT hLibrary, DWORD dwReason, PVOID pReserved)
{
	return TRUE;
	}

#endif

// End of File
