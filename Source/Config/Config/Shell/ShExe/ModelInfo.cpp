
#include "intern.hpp"

#include "ModelInfo.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Main Application
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Model Information Library
//

// Static Data

HMODULE CModelInfo::m_hLib = NULL;

// Library Management

void CModelInfo::LoadLib(void)
{
	afxThread->SetWaitMode(TRUE);

	WCHAR sPath[4096];

	GetEnvironmentVariable(L"Path", sPath, elements(sPath));

	wstrcat(sPath, L";");
	
	wstrcat(sPath, afxModule->GetFilename().GetDirectory() + L"Straton");

	SetEnvironmentVariable(L"Path", sPath);

	CFilename Name = pccModule->GetFilename();

	Name.ChangeName(L"g3model.dll");

	m_hLib = LoadLibrary(Name);

	typedef void (*PFUNC)(void);

	PFUNC pFunc = (PFUNC) GetProcAddress(m_hLib, "OnInit");

	if( pFunc ) {

		(*pFunc)();
		}

	afxThread->SetWaitMode(FALSE);
	}

void CModelInfo::FreeLib(void)
{
	typedef void (*PFUNC)(void);

	PFUNC pFunc = (PFUNC) GetProcAddress(m_hLib, "OnTerm");

	if( pFunc ) {

		(*pFunc)();
		}

	FreeLibrary(m_hLib);
	}

// Operations

BOOL CModelInfo::CheckModel(CString &Model)
{
	typedef BOOL(*PFUNC)(CString &Model);

	PFUNC pFunc = (PFUNC) GetProcAddress(m_hLib, "CheckModel");

	if( pFunc ) {

		return (*pFunc)(Model);
	}

	return TRUE;
}

BOOL CModelInfo::SelectModel(UINT uMode, CString &Model)
{
	typedef BOOL(*PFUNC)(HWND hWnd, UINT uMode, CString &Model);

	PFUNC pFunc = (PFUNC) GetProcAddress(m_hLib, "SelectModel");

	if( pFunc ) {

		return (*pFunc)(afxMainWnd->GetHandle(), uMode, Model);
	}

	return TRUE;
}

// End of File
