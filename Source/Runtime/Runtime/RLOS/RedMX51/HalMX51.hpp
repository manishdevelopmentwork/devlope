
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_HalMX51_HPP

#define INCLUDE_HalMX51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "../RedArm/ArmHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CWatchdog51;
class CGpt51;
class CEpit51;

//////////////////////////////////////////////////////////////////////////
//
// Instantiators
//

extern IPic * Create_Tzic51(void);

extern IMmu * Create_Mmu51(IHal *pHal);

//////////////////////////////////////////////////////////////////////////
//
// Hardware Abstraction Layer for iMX51
//

class CHalMX51 : public CArmHal
{
	public:
		// Constructor
		CHalMX51(void);

		// Destructor
		~CHalMX51(void);

		// Initialization
		void Open(void);

		// IHal Timer Support
		UINT GetTickFraction(void);
		void SpinDelayFast(UINT uTime);

		// IHal Debug Support
		void DebugReset(void);
		UINT DebugRead(UINT uDelay);
		void DebugOut(char cData);
		void DebugWait(void);
		void DebugStop(void);
		void DebugKick(void);

		// IEventSink
		void OnEvent(UINT uLine, UINT uParam);

	protected:
		// UART Registers
		enum
		{
			regRecv	   = 0x0000 / sizeof(DWORD),
			regSend	   = 0x0040 / sizeof(DWORD),
			regCtrl1   = 0x0080 / sizeof(DWORD),
			regCtrl2   = 0x0084 / sizeof(DWORD),
			regCtrl3   = 0x0088 / sizeof(DWORD),
			regCtrl4   = 0x008C / sizeof(DWORD),
			regFifo    = 0x0090 / sizeof(DWORD),
			regStat1   = 0x0094 / sizeof(DWORD),
			regStat2   = 0x0098 / sizeof(DWORD),
			regEscChar = 0x009C / sizeof(DWORD),
			regEscTime = 0x00A0 / sizeof(DWORD),
			regBaudInc = 0x00A4 / sizeof(DWORD),
			regBaudMod = 0x00A8 / sizeof(DWORD),
			regBaudCnt = 0x00AC / sizeof(DWORD),
			regOneMs   = 0x00B0 / sizeof(DWORD),
			regTest    = 0x00B4 / sizeof(DWORD),
			};

		// UART Control / Status Bits 
		enum
		{
			bitTxNoRTS	= Bit(14),
			bitTxCTS	= Bit(13),
			bitTxFifo	= Bit(13),
			bitRxFifo	= Bit( 9),
			bitRxAged	= Bit( 8),
			bitTxEmpty	= Bit( 6),
			bitRxEmpty	= Bit( 5),
			bitTxFull	= Bit( 4),
			bitRxFull	= Bit( 3),
			bitTxDone	= Bit( 3),
			bitRxTimer	= Bit( 3),
			bitTxEnable	= Bit( 2),
			bitRxEnable	= Bit( 1),
			bitRxReady	= Bit( 0),
			bitEnable	= Bit( 0),
			bitReset	= Bit( 0),
			};

		// UART Error Bits
		enum 
		{
			errReady	= Bit(15),
			errError	= Bit(14),
			errOverrun	= Bit(13),
			errFrame	= Bit(12),
			errBreak	= Bit(11),
			errParity	= Bit(10), 
			};

		// Data Members
		DWORD         m_uDebugAddr;
		UINT	      m_uDebugLine;
		CWatchdog51 * m_pDog;
		CGpt51      * m_pGpt;
		CEpit51     * m_pEpit;
		ISemaphore  * m_pRead;
		BYTE	      m_bBuff[256];
		UINT          m_uHead;
		UINT          m_uTail;
		PVDWORD	      m_pBase;
		CSerialConfig m_Config;
		UINT	      m_uClock;
		BYTE          m_bMask;

		// Power Management Hooks
		void EnterLowPower(void);
		void LeaveLowPower(void);

		// Implementation
		void FindDebug(void);
		bool InitUart(void);
		bool InitBaudRate(void);
		bool InitFlags(void);
		bool InitFormat(void);
		WORD GetParityInit(void);
		WORD GetStopBitsInit(void);
		WORD GetDataBitsInit(void);
		BYTE GetDataBitsMask(void);
		void SetBaudRate(UINT uRate);
		void CreateDevices(void);
		void StartTimer(void);
	};

// End of File

#endif
