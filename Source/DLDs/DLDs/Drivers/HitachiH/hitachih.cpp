
#include "intern.hpp"

#include "HitachiH.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HITACHI H-Series Driver
//

// Instantiator

INSTANTIATE(CHitachiHDriver);

// Constructor

CHitachiHDriver::CHitachiHDriver(void)
{
	m_Ident	    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex	    = Hex;

	m_dError    = 0;
	}

// Destructor

CHitachiHDriver::~CHitachiHDriver(void)
{
	}

// Entry Points

// Configuration

void MCALL CHitachiHDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CHitachiHDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate < 19200 ) {

		Config.m_uFlags = flagFastRx;
		}

	if( Config.m_uPhysical == 3 ) Make485(Config, TRUE);
	}
	
// Management

void MCALL CHitachiHDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CHitachiHDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CHitachiHDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bLoop		= GetByte(pData);

			m_pCtx->m_bUnit		= GetByte(pData);

			m_pCtx->m_fCheck	= GetByte(pData);

			m_pCtx->m_fMulti	= GetByte(pData);

			m_pCtx->m_bDrop		= GetByte(pData);

			m_pCtx->m_bRspDelay	= '0';

			m_pCtx->m_uErrPos	= m_pCtx->m_fMulti ? 11 : 9;

			m_pCtx->m_uReqDelay	= 20;
// Hack for Australia
			m_pCtx->m_fAllowCache	= FALSE;

			ResetCaches();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD; 
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CHitachiHDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

CCODE MCALL CHitachiHDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPING");

	DWORD    Data[1];

	m_pCtx->m_fAllowCache = FALSE;

	CAddress Addr;

	Addr.a.m_Table  = SPWR;
	Addr.a.m_Offset = 0;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	if( m_pCtx->m_bRspDelay < '1' ) m_fInPing = TRUE;

	if( Read(Addr, Data, 1) == 1 ) {

		return 1;		
		}

	Addr.a.m_Table	= SPWM;

	if( m_pCtx->m_bRspDelay < '1' ) m_fInPing = TRUE;

	return Read(Addr, Data, 1);
	}

// Execute Read
CCODE MCALL CHitachiHDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( NoReadTransmit(Addr, pData, &uCount) ) {

		return uCount;
		}

//**/	AfxTrace3("\r\n\nREAD T=%d O=%x C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	UINT uCt = Addr.a.m_Type == addrLongAsLong ? 1 + uCount : uCount;

	MakeMin(uCt, 128);

	memset(m_bRx, 0, 11);

	if( PutRead(Addr, uCt) ) {

		UINT uData0 = m_pCtx->m_fMulti ? 15 : 13;

		BYTE uMask = 0x80;

		UINT uVerifyCt  = m_wCount - 3 - uData0; // subtract received non-data bytes

		switch( Addr.a.m_Type ) {

			case addrBitAsBit:	// 4 bits/received byte
				uVerifyCt <<= 2;
				break;

			case addrLongAsLong:	// last word
				uVerifyCt  -= 4;
			// fall through

			case addrWordAsWord:	// 4 bytes/received word
				uVerifyCt  /= 4;
				break;
			}

		MakeMin(uCount, uVerifyCt);

		for( WORD wScan = 0; wScan < uCount; wScan++ ) {

			switch( Addr.a.m_Type ) {

				case addrBitAsBit:

					if( wScan % 8 == 0 ) uMask = 0x80;

					pData[wScan] = FormByte(uData0+(2 * (wScan/8))) & uMask ? 1L : 0L;

					uMask >>= 1;

					break;

				case addrWordAsWord:

					pData[wScan] = DWORD(FormWord(uData0+(wScan*4)));

					break;

				case addrLongAsLong:

					DWORD d;

					d = FormLong(uData0 + (wScan*4));

					pData[wScan] = d;

					break;
				default:
					pData[wScan] = 0L;
					break;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

BOOL CHitachiHDriver::PutRead(AREF Addr, WORD wCount)
{
	StartFrame();

	AddByte(RIO);

	PutCodeAndAddr(Addr);

	AddByte(LOBYTE(wCount));

	return CommandSequence();
	}

// Execute Write
CCODE MCALL CHitachiHDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( NoWriteTransmit(Addr, *pData) ) {

		return uCount;
		}

//**/	AfxTrace3("\r\n\n******\r\n**** WRITE **** T=%d O=%x C=%d \r\n********\r\n\n", Addr.a.m_Table, Addr.a.m_Offset, uCount);

	MakeMin(uCount, 4);

	memset(m_bRx, 0, 11);

	PutWrite(Addr, uCount, pData);

	return m_bRx[0] == NAK ? CCODE_ERROR : uCount;
	}

void CHitachiHDriver::PutWrite(AREF Addr, UINT uCount, PDWORD pData)
{
	WORD wScan;

	BYTE bBits = 0;

	StartFrame();

	AddByte(WIO);

	PutCodeAndAddr(Addr);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			AddByte(LOBYTE(uCount));

			for( wScan = 0; wScan < uCount; wScan++ ) {

				bBits = (bBits << 1) | (pData[wScan] ? 1 : 0);

				if( wScan%8 == 7 ) {

					AddByte(bBits);

					bBits = 0;
					}
				}

			if( uCount % 8) {

				uCount %= 8;

				while ( uCount++ < 8) bBits <<= 1;

				AddByte(bBits);
				}
			break;

		case addrWordAsWord:

			AddByte(LOBYTE(uCount));

			for( wScan = 0; wScan < uCount; wScan++ ) {

				AddWord(LOWORD(pData[wScan]));
				}
			break;

		case addrLongAsLong:

			AddByte(LOBYTE(uCount*2));

			for( wScan = 0; wScan < uCount; wScan++ ) {

				AddLong(pData[wScan]);
				}
			break;

		}

	CommandSequence();

	return;
	}

// Command Exchange
BOOL CHitachiHDriver::CommandSequence(void)
{
	if( !Transact() || m_bRx[0] != ACK ) return FALSE;

	switch( SetENQ1() ) {

		case EOT:
			if( SetENQ1() != STX ) return FALSE;
			break;

		case STX:

			break;

		default:
			return FALSE;
		}

	UINT uErr = FormByte(m_pCtx->m_uErrPos);

	if( !uErr ) {

		return TRUE;
		}

	m_dError  = uErr << 16;
	m_dError += FormWord(m_pCtx->m_uErrPos + 2);

	return FALSE;
	}

BYTE CHitachiHDriver::SetENQ1()
{
	m_bTx[0] = ENQ;

	m_bTx[1] = m_pCtx->m_bRspDelay;

	SetDrop();

	m_bTx[m_uPtr++] = '1';

	return Transact() ? m_bRx[0] : 0;
	}

// Frame Building

void CHitachiHDriver::StartFrame(void)
{
	m_bTx[0] = STX;

	if( m_fInPing ) {

		m_bTx[1]  = '4'; // force initial response delay of 40 msec

		m_fInPing = FALSE;
		}

	else {
		m_bTx[1] = m_pCtx->m_bRspDelay;
		}

	SetDrop();

	PutLUMP();
	}

void CHitachiHDriver::SetDrop(void)
{
	m_uPtr = 2;

	if( m_pCtx->m_fMulti ) {

		m_bTx[m_uPtr++] = m_pHex[m_pCtx->m_bDrop / 10]; // Station number is 2 chars in BCD
		m_bTx[m_uPtr++] = m_pHex[m_pCtx->m_bDrop % 10];
		}
	}
	
void CHitachiHDriver::PutLUMP(void)
{
	AddByte(m_pCtx->m_bLoop);	// L
	AddByte(m_pCtx->m_bUnit);	// U
	AddByte(0);			// M
	AddByte(0);			// P
	}

void CHitachiHDriver::PutMemoryCode(WORD wTable)
{
	if( wTable >= SPL0 && wTable <= SPL3 ) {

		AddByte(ML);
		AddByte(wTable - SPL0);

		return;
		}

	if( wTable >= SPX0 && wTable <= SPX4 ) {

		AddByte(MX);
		AddByte(wTable - SPX0);

		return;
		}

	if( wTable >= SPY0 && wTable <= SPY4 ) {

		AddByte(MY);
		AddByte(wTable - SPY0);

		return;
		}

	if( wTable >= SPR0 && wTable <= SPRF ) {

		AddByte(MR);
		AddByte(wTable - SPR0);

		return;
		}

	switch ( wTable ) {

		case SPWL0:	AddByte(MWL);	break; //B
		case SPWL1:	AddByte(MWL); AddByte(1); return;
		case SPWX:	AddByte(MWX);	break; //8
		case SPWY:	AddByte(MWY);	break; //9
		case SPWM:	AddByte(MWM);	break; //C
		case SPTCP:	AddByte(MTP);	break; //5
		case SPTCA:	AddByte(MTA);	break; //D
		case SPM:	AddByte(MM );	break; //4
		case SPCL:	AddByte(MCL);	break; //6
		case SPWR:
		case SPDR:	AddByte(MWR);	break; //A
		}

	AddByte(0);
	}

void CHitachiHDriver::PutCodeAndAddr(AREF Addr)
{
	PutMemoryCode(Addr.a.m_Table);

	AddByte(Addr.a.m_Offset / 256);
	AddByte(Addr.a.m_Offset % 256);
	}

void CHitachiHDriver::AddByte(BYTE bData)
{
	BYTE b;

	b = bData/16;

	m_bTx[m_uPtr++] = m_pHex[b];

	b = bData%16;

	m_bTx[m_uPtr++] = m_pHex[b];
	}

void CHitachiHDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));
	AddByte(LOBYTE(wData));
	}

void CHitachiHDriver::AddLong(DWORD dData)
{
	AddWord(LOWORD(dData));
	AddWord(HIWORD(dData));
	}

// Transact
BOOL CHitachiHDriver::Transact(void)
{
	m_pData->ClearRx();

	SendFrame();
				
	return GetFrame();
	}

void CHitachiHDriver::SendFrame(void)
{
	if( m_pCtx->m_uReqDelay ) Sleep(m_pCtx->m_uReqDelay);

	if( m_pCtx->m_fCheck && m_bTx[0] == STX ) {

		BYTE bCheck = 0;

		for( UINT k = 1; k < m_uPtr; k++ ) bCheck += m_bTx[k];

		AddByte(bCheck);
		}

	m_bTx[m_uPtr++] = CR;

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);
	}

BOOL CHitachiHDriver::GetFrame(void)
{
	WORD wData;

	m_wCount = 0;
	
	SetTimer(2000);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		if( (wData = RxByte()) == LOWORD(NOTHING) ) {
			
			Sleep(20);
			
			continue;
			}

//**/		AfxTrace1("<%2.2x>", wData);

		m_bRx[m_wCount++] = wData;

		if( wData == CR ) {

			return ValidateChecksum();
			}
				
		if( m_wCount >= sizeof(m_bRx) ) return FALSE;
		}
			
	return FALSE;
	}

BOOL CHitachiHDriver::ValidateChecksum(void)
{
	BYTE bR0 = m_bRx[0];

	if( !m_pCtx->m_fCheck ) return TRUE;

	if( bR0 == STX ) {

		BYTE bCheck = 0;

		for( WORD i = 1; i < m_wCount-3; i++ ) {

			bCheck += m_bRx[i];
			}

		if( m_pHex[bCheck/16] != m_bRx[m_wCount-3] ) return FALSE;

		if( m_pHex[bCheck%16] != m_bRx[m_wCount-2] ) return FALSE;

		return TRUE;
		}

	return bR0 == ACK || bR0 == NAK || bR0 == EOT;
	}
	
BOOL CHitachiHDriver::CheckResponseError(void)
{
	return FormByte(m_pCtx->m_uErrPos) == RNOEXEC;
	}

// Response Data Handling
BYTE CHitachiHDriver::FormByte(UINT uPos)
{
	return MakeByte((PCBYTE)&m_bRx[uPos]);
	}

WORD CHitachiHDriver::FormWord(UINT uPos)
{
	WORD w = ((WORD)FormByte(uPos)) << 8;

	w     += FormByte(uPos+2);

	return w;
	}

DWORD CHitachiHDriver::FormLong(UINT uPos)
{
	DWORD d = FormWord(uPos);

	d      += ((DWORD)FormWord(uPos+4)) << 16;

	return d;
	}

BYTE CHitachiHDriver::MakeByte(PCBYTE pText)
{
	BYTE bData = 0;

	UINT uCt   = 2;

	while( uCt-- ) {
	
		BYTE bByte = *(pText++);
		
		if( bByte >= '0' && bByte <= '9' )
			bData = (bData << 4) + bByte - '0';

		else if( bByte >= 'A' && bByte <= 'F' )
			bData = (bData << 4) + bByte - 'A' + 10;

		else if( bByte >= 'a' && bByte <= 'f' )
			bData = (bData << 4) + bByte - 'a' + 10;

		else break;
		}

	return bData;
	}

// Port Access

void CHitachiHDriver::TxByte(BYTE bData)
{
	m_pData->Write(bData, FOREVER);
	}

UINT CHitachiHDriver::RxByte(void)
{
	return m_pData->Read(100);
	}

// Helpers
BOOL CHitachiHDriver::NoReadTransmit(AREF Addr, PDWORD pData, UINT *pCount)
{
	UINT uTable  = Addr.a.m_Table;
	UINT uOffset = Addr.a.m_Offset;

	BOOL fRead   = TRUE;

	UINT uFunc   = CMAKENEW;
	UINT uCnt    = *pCount;
	UINT uSel    = 0;
	UINT uStart  = uOffset;

	switch( uTable ) {

		case SPERR:

			pData[0] = m_dError;

			return TRUE;

		case SPADJ:
			uFunc    = (20 - m_pCtx->m_uReqDelay) * 1000;
 
			uSel     = 10 * (m_pCtx->m_bRspDelay <= '9' ? m_pCtx->m_bRspDelay - '0' : max(m_pCtx->m_bRspDelay - '7', 15));

			pData[0] = DWORD(uFunc + uSel);

			return TRUE;

		case SPWR:
			if( m_pCtx->m_fDoCacheRead ) {

				return FALSE;
				}

			if( !TestForDonahoeTrigger(uOffset, uCnt) ) {

				return FALSE;
				}

			if( !m_pCtx->m_fAllowCache ) return FALSE;

			uSel = SelectWCache(Addr, &uFunc);

			switch( uFunc ) {

				case CUSEOLD: // Base < Address < Bass + WCACHESZ -> Get from cache
					fRead = FALSE;

					uCnt  = WCACHESZ - (uOffset - m_pCtx->m_uWDAddr[uSel]);
					MakeMin(uCnt, *pCount);
					break;

				case CMAKENEW:
					UpdateWDInfo(uSel, uOffset);
					break;
				}

			if( fRead ) {

				if( !StockWords(Addr, uSel) ) {

					return FALSE;
					}
				}

			PWORD pWords;

			pWords = SelectWDBuffer(uSel);

			uStart -= m_pCtx->m_uWDAddr[uSel];

			for( uFunc = 0; uFunc < uCnt; uFunc++ ) {

				pData[uFunc] = pWords[uStart + uFunc];
				}

			*pCount = uCnt;

			return TRUE;

		case SPM:
		case SPR0:
			if( m_pCtx->m_fDoCacheRead || !m_pCtx->m_fAllowCache ) {

				return FALSE;  // allow direct read
				}

			if( !m_pCtx->m_fAllowCache ) return FALSE;

			uSel = SelectBCache(Addr, &uFunc);

			switch( uFunc ) {

				case CUSEOLD: // Base < Address < Bass + BCACHESZ -> Get from cache
					fRead = FALSE;

					uCnt  = BCACHESZ - (uOffset - m_pCtx->m_uBitAddr[uSel]);
					MakeMin(uCnt, *pCount);
					break;

				case CMAKENEW:
					UpdateBitInfo(uSel, uOffset, uTable);
					break;
				}

			if( fRead ) {

				if( !StockBits(Addr, uSel) ) {

					return FALSE;
					}
				}

			BOOL * pBits;

			pBits = SelectBitBuffer(uSel);

			uStart -= m_pCtx->m_uBitAddr[uSel];

			for( uFunc = 0; uFunc < uCnt; uFunc++ ) {

				pData[uFunc] = pBits[uStart + uFunc];
				}

			*pCount = uCnt;

			return TRUE;
		}

	return FALSE;
	}

BOOL CHitachiHDriver::NoWriteTransmit(AREF Addr, DWORD dData)
{
	UINT uTable = Addr.a.m_Table;

	switch( uTable ) {

		case SPERR: m_dError = dData; return TRUE;

		case SPADJ:
			m_pCtx->m_uReqDelay = 20 - (min(dData, 20000)/1000);

			dData %= 1000;

			m_pCtx->m_bRspDelay = m_pHex[min(dData / 10, 0xF)];

			if( m_pCtx->m_fMulti ) {

				m_pCtx->m_bRspDelay = max('2', m_pCtx->m_bRspDelay);
				}

			return TRUE;

		case SPM:
		case SPR0:
			UpdateBitWrite(Addr, BOOL(dData));
			return FALSE;

		case SPWR:
			UpdateWDWrite(Addr, dData);
			return FALSE;
		}

	return FALSE;
	}

void CHitachiHDriver::ResetCaches(void)
{
// Hack for Australia
	m_pCtx->m_fDoCacheRead	= FALSE;

	// Bit caching
	m_pCtx->m_uNextBCache   = 0;

	memset(m_pCtx->m_Bits0,    0, sizeof(m_pCtx->m_Bits0));
	memset(m_pCtx->m_Bits1,    0, sizeof(m_pCtx->m_Bits1));
	memset(m_pCtx->m_Bits2,    0, sizeof(m_pCtx->m_Bits2));
	memset(m_pCtx->m_Bits3,    0, sizeof(m_pCtx->m_Bits3));
	memset(m_pCtx->m_Bits4,    0, sizeof(m_pCtx->m_Bits4));
	memset(m_pCtx->m_Bits5,    0, sizeof(m_pCtx->m_Bits5));
	memset(m_pCtx->m_Bits6,    0, sizeof(m_pCtx->m_Bits6));
	memset(m_pCtx->m_Bits7,    0, sizeof(m_pCtx->m_Bits7));

	memset(m_pCtx->m_uBitAddr, 0xFF, sizeof(m_pCtx->m_uBitAddr));
	memset(m_pCtx->m_bBitType,    0, sizeof(m_pCtx->m_bBitType));

	// Word caching
	m_pCtx->m_uNextWCache   = 0;

	memset(m_pCtx->m_WR0,    0, sizeof(m_pCtx->m_WR0));
	memset(m_pCtx->m_WR1,    0, sizeof(m_pCtx->m_WR1));
	memset(m_pCtx->m_WR2,    0, sizeof(m_pCtx->m_WR2));
	memset(m_pCtx->m_WR3,    0, sizeof(m_pCtx->m_WR3));
	memset(m_pCtx->m_WR4,    0, sizeof(m_pCtx->m_WR4));
	memset(m_pCtx->m_WR5,    0, sizeof(m_pCtx->m_WR5));
	memset(m_pCtx->m_WR6,    0, sizeof(m_pCtx->m_WR6));
	memset(m_pCtx->m_WR7,    0, sizeof(m_pCtx->m_WR7));

	memset(m_pCtx->m_uWDAddr, 0xFF, sizeof(m_pCtx->m_uWDAddr));
	memset(m_pCtx->m_bWDType,    0, sizeof(m_pCtx->m_bWDType));
	}

// Bit Caching
BOOL CHitachiHDriver::TestForDonahoeTrigger(UINT uOffset, UINT uCount)
{
	if( uOffset == 0x302 && uCount > 13 ) {

		m_pCtx->m_fAllowCache = TRUE;

		ResetCaches();

//**/		AfxTrace1("\r\n--- %d ---\r\n", 5 * (GetTickCount() - m_uCTxx)); m_uCTxx = GetTickCount();

		return TRUE;
		}

	return TRUE;
	}

UINT CHitachiHDriver::SelectBCache(AREF Addr, UINT *pFunc)
{
	BYTE bTable   = LOBYTE(Addr.a.m_Table);
	UINT uOffset  = Addr.a.m_Offset;
	UINT uDefault = m_pCtx->m_uNextBCache;

	*pFunc        = CMAKENEW;

	for( UINT i = 0; m_pCtx->m_bBitType[i] && i < BCACHECT; i++ ) {

		if( bTable == m_pCtx->m_bBitType[i] && uOffset == m_pCtx->m_uBitAddr[i] ) {

			ResetCaches();
			return 0;
			}
		}

	for( i = 0; bTable == m_pCtx->m_bBitType[i] && i < BCACHECT; i++ ) {

		if( CheckBRange(bTable, m_pCtx->m_bBitType[i], uOffset, m_pCtx->m_uBitAddr[i]) ) {

			*pFunc = CUSEOLD; // Base < uOffset < Base + BCACHESZ
			return i;
			}
		}

	for( i = 0; bTable == m_pCtx->m_bBitType[i] && i < BCACHECT; i++ ) {

		if( CheckBRange(bTable, m_pCtx->m_bBitType[i], m_pCtx->m_uBitAddr[i], uOffset) ) {

			m_pCtx->m_uBitAddr[i] = uOffset; // reset to include previous entry
			*pFunc = CUSETHIS;
			return i;
			}
		}

	if( m_pCtx->m_uNextBCache == (BCACHECT - 1) ) {

		ResetCaches();

		return 0;
		}

	m_pCtx->m_uNextBCache++;

	return uDefault;
	}

BOOL CHitachiHDriver::CheckBRange(BYTE bTable, BYTE bType, UINT uV1, UINT uV2)
{
	return bTable == bType && uV1 > uV2 && (uV1 < (uV2 + BCACHESZ));
	}

BOOL * CHitachiHDriver::SelectBitBuffer(UINT uSel)
{
	switch( uSel ) {

		case 1: return m_pCtx->m_Bits1;
		case 2: return m_pCtx->m_Bits2;
		case 3: return m_pCtx->m_Bits3;
		case 4: return m_pCtx->m_Bits4;
		case 5: return m_pCtx->m_Bits5;
		case 6: return m_pCtx->m_Bits6;
		case 7: return m_pCtx->m_Bits7;
		}

	return m_pCtx->m_Bits0;
	}

void CHitachiHDriver::UpdateBitInfo(UINT uSel, UINT uOff, BYTE bTab)
{
	m_pCtx->m_uBitAddr[uSel] = uOff;
	m_pCtx->m_bBitType[uSel] = bTab;
	}

BOOL CHitachiHDriver::StockBits(AREF Addr, UINT uSel)
{
	DWORD Data[BCACHESZ];

	m_pCtx->m_fDoCacheRead = TRUE;

	CCODE c = Read(Addr, Data, BCACHESZ);

	m_pCtx->m_fDoCacheRead = FALSE;

	UINT u = (UINT)c;

	if( u <= BCACHESZ ) {

		BOOL *pBits = SelectBitBuffer(uSel);

		for( UINT i = 0; i < u; i++ ) {

			pBits[i] = BOOL(Data[i]);
			}

		return TRUE;
		}

	return FALSE;
	}

void CHitachiHDriver::UpdateBitWrite(AREF Addr, BOOL fData)
{
	for( UINT i = 0; i < BCACHECT; i++ ) {

		UINT uO = Addr.a.m_Offset;
		UINT uA = m_pCtx->m_uBitAddr[i];

		if( CheckBRange(LOBYTE(Addr.a.m_Table), m_pCtx->m_bBitType[i], uO, uA) ) {

			BOOL * pBits = SelectBitBuffer(i);

			pBits[uO - uA] = fData;

			return;
			}
		}
	}

// Word Caching
UINT CHitachiHDriver::SelectWCache(AREF Addr, UINT *pFunc)
{
	UINT uOffset  = Addr.a.m_Offset;
	UINT uDefault = m_pCtx->m_uNextWCache;

	*pFunc        = CMAKENEW;

	UINT uBase;

	for( UINT i = 0; i < WCACHECT; i++ ) {

		if( m_pCtx->m_bWDType && uOffset == m_pCtx->m_uWDAddr[i] ) {

			ResetCaches();
			return 0;
			}
		}

	for( i = 0; i < WCACHECT; i++ ) {

		if( CheckWRange(i, uOffset, m_pCtx->m_uWDAddr[i]) ) {

			*pFunc = CUSEOLD; // Base < uOffset < Base + WCACHESZ
			return i;
			}
		}

	for( i = 0; i < WCACHECT; i++ ) {

		if( CheckWRange(i, m_pCtx->m_uWDAddr[i], uOffset) ) {

			m_pCtx->m_uWDAddr[i] = uOffset; // reset to include previous entry
			*pFunc = CUSETHIS;
			return i;
			}
		}

	if( m_pCtx->m_uNextWCache == (WCACHECT - 1) ) {

		ResetCaches();
		return 0;
		}

	m_pCtx->m_uNextWCache++;

	return uDefault;
	}

BOOL CHitachiHDriver::CheckWRange(UINT uSel, UINT uV1, UINT uV2)
{
	return m_pCtx->m_bWDType[uSel] && uV1 > uV2 && (uV1 < (uV2 + WCACHESZ));
	}

BOOL CHitachiHDriver::CheckInRange(AREF Addr)
{
	for( UINT i = 0; i < WCACHECT; i++ ) {

		if( CheckWRange(i, Addr.a.m_Offset, m_pCtx->m_uWDAddr[i]) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

PWORD CHitachiHDriver::SelectWDBuffer(UINT uSel)
{
	switch( uSel ) {

		case 1: return m_pCtx->m_WR1;
		case 2: return m_pCtx->m_WR2;
		case 3: return m_pCtx->m_WR3;
		case 4: return m_pCtx->m_WR4;
		case 5: return m_pCtx->m_WR5;
		case 6: return m_pCtx->m_WR6;
		case 7: return m_pCtx->m_WR7;
		}

	return m_pCtx->m_WR0;
	}

void CHitachiHDriver::UpdateWDInfo(UINT uSel, UINT uOff)
{
	m_pCtx->m_uWDAddr[uSel] = uOff;
	m_pCtx->m_bWDType[uSel] = 0xFF;
	}

BOOL CHitachiHDriver::StockWords(AREF Addr, UINT uSel)
{
	DWORD Data[WCACHESZ];

	m_pCtx->m_fDoCacheRead = TRUE;

	CCODE c = Read(Addr, Data, WCACHESZ);

	m_pCtx->m_fDoCacheRead = FALSE;

	UINT u = (UINT)c;

	if( u <= WCACHESZ ) {

		PWORD pWDs = SelectWDBuffer(uSel);

		for( UINT i = 0; i < u; i++ ) {

			pWDs[i] = LOWORD(Data[i]);
			}

		return TRUE;
		}

	return FALSE;
	}

void CHitachiHDriver::UpdateWDWrite(AREF Addr, DWORD dData)
{
	for( UINT i = 0; i < WCACHECT; i++ ) {

		UINT uO = Addr.a.m_Offset;
		UINT uA = m_pCtx->m_uWDAddr[i];

		if( CheckWRange(i, uO, uA) ) {

			PWORD pWDs = SelectWDBuffer(i);

			pWDs[uO - uA] = LOWORD(dData);

			return;
			}
		}
	}

// End of File
