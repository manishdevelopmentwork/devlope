
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_DEBUGSERIAL_HPP
	
#define	INCLUDE_DEBUGSERIAL_HPP

//////////////////////////////////////////////////////////////////////////
//
// Debug Serial Driver
//

class CDebugSerialDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CDebugSerialDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
	};

// End of File

#endif
