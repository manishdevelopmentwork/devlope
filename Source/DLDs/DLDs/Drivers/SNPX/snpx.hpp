#include "check.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define	T0_PERIOD	50
#define	T1_PERIOD	20
#define	T2_PERIOD	1000
#define	T3_PERIOD	30000
#define	T4_PERIOD	200

#define SNP_ID_LENGTH	8

#define STATE_IDLE	1
#define STATE_WAITA	2
#define STATE_OPEN	3
#define STATE_WAITI	4
#define STATE_WAITX	5

//////////////////////////////////////////////////////////////////////////
//
// GE SNP-X Driver
//

class CSNPXDriver : public CMasterDriver {

	public:
		// Constructor
		CSNPXDriver(void);

		// Destructor
		~CSNPXDriver(void);

		// Configuration

		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);
		
			
		// Management

		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);

		// Device

		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// Entry Points

		DEFMETH(CCODE) Ping(void);
		DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			PTXT	m_Drop;
			BOOL	m_fBreakFree;
			WORD	m_wState;
			WORD	m_wIdLen;
			BYTE    m_bSlot;  // Config match only, not used in SNP-X
							
			};

		// Data Members
		LPCTXT 		m_pHex;
		CCheckSum	m_Check;
		CContext *	m_pCtx;
		BYTE		m_bTxBuff[256];
		BYTE		m_bRxBuff[256];
		WORD		m_wTxSize;
		WORD		m_wRxSize;
		UINT		m_uPtr;
		BYTE		m_IntHelp;

		// Entry point helpers

		CCODE DoSmallWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Implementation
		
		BOOL Attach(void);
		void SendLongBreak(void);
		void Start(void);
		void End(void);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddBits(PDWORD pData, UINT uCount, UINT uOffset);
		BOOL Transact(void);
		BOOL PutFrame(void);
		BOOL GetFrame(void);
		BOOL CheckFrame(void);
		void AddRegSpec(AREF Addr, UINT uCount);
		BYTE GetTypeID(WORD wType, BOOL fBit);
		BOOL CheckID(void);
		void GetBitRead(PDWORD pData,  UINT uCount, UINT uOffset);
		void GetWordRead(PDWORD pData, UINT uCount);
		void GetLongRead(PDWORD pData, UINT uCount);
		UINT FindBitsByteCount(UINT uOffset, UINT uCount);
		BOOL IsLong(UINT uType);
	};

// End of File
