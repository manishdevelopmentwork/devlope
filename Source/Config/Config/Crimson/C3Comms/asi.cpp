
#include "intern.hpp"

#include "asi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
//////////////////////////////////////////////////////////////////////////
//
// IFM CoDeSys SP Master Driver
//

// Instantiator

ICommsDriver * Create_IfmCoDeSysSpDriver(void)
{
	return New CIfmCoDeSysSpDriver;
	}

// Constructor

CIfmCoDeSysSpDriver::CIfmCoDeSysSpDriver(void)
{
	m_wID		= 0x400E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "IFM";
			
	m_DriverName	= "ASI Master via CoDeSys SP";
	
	m_Version	= "1.01";
	
	m_ShortName	= "ASI Master";	

	m_fSingle	= TRUE;

	AddSpaces();

	}

// Binding Control

UINT CIfmCoDeSysSpDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CIfmCoDeSysSpDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Implementation     

void CIfmCoDeSysSpDriver::AddSpaces(void)
{
	AddSpace(New CSpaceIFM(0, "M",	"Marker Area (BYTE)",	10, 0, 65535, addrByteAsByte, addrByteAsByte));
	AddSpace(New CSpaceIFM(1, "I",	"Input Area (BYTE)",	10, 0, 65535, addrByteAsByte, addrByteAsByte));
	AddSpace(New CSpaceIFM(2, "Q",	"Output Area (BYTE)",	10, 0, 65535, addrByteAsByte, addrByteAsByte));
	AddSpace(New CSpaceIFM(3, "MW",	"Marker Area (WORD)",	10, 0, 65535, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceIFM(4, "IW",	"Input Area (WORD)",	10, 0, 65535, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceIFM(5, "QW",	"Output Area (WORD)",	10, 0, 65535, addrWordAsWord, addrWordAsWord));
	AddSpace(New CSpaceIFM(6, "MD",	"Marker Area (LONG)",	10, 0, 65535, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceIFM(7, "ID",	"Input Area (LONG)",	10, 0, 65535, addrLongAsLong, addrLongAsLong));
	AddSpace(New CSpaceIFM(8, "QD",	"Output Area (LONG)",	10, 0, 65535, addrLongAsLong, addrLongAsLong));
	}

// Address Helpers

BOOL CIfmCoDeSysSpDriver::DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text)
{	
	UINT uExtra = 0;
	
	if( pSpace->m_uTable < 4 ) {

		CString Type = StripType(pSpace, Text);

		UINT uType = pSpace->TypeFromModifier(Type);

		if( uType == addrByteAsWord ) {

			uExtra = 3;
			}

		else if ( uType == addrByteAsLong) {

			uExtra = 6;
			}
		}

	if( CStdCommsDriver::DoParseAddress(Error, Addr, pConfig, pSpace, Text) ) {
		
		Addr.a.m_Extra = Addr.a.m_Table + uExtra;

		Addr.a.m_Table = addrNamed;

		return TRUE; 
		}

	return FALSE;
	}

BOOL CIfmCoDeSysSpDriver::DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CAddress Address = Addr;

	if( Addr.a.m_Type == addrByteAsByte ) {
		
		if( Addr.a.m_Extra > 2 ) {

			if( Addr.a.m_Extra > 5 ) {

				Address.a.m_Type = addrLongAsLong;
				}
			else {
				Address.a.m_Type = addrWordAsWord;
				}
			}
		}
		
	return CStdCommsDriver::DoExpandAddress(Text, pConfig, Address);
	}

//////////////////////////////////////////////////////////////////////////
//
// IFM Space Wrapper
//

// Constructor

CSpaceIFM::CSpaceIFM(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t, UINT s)	: CSpace(uTable, p, c, r, n, x, t, s)
{

	}

// Matching

BOOL CSpaceIFM::MatchSpace(CAddress const &Addr)
{
	return m_uTable == Addr.a.m_Extra;
	}

// Limits

void CSpaceIFM::GetMinimum(CAddress &Addr)
{
	Addr.a.m_Extra	= m_uTable;

	Addr.a.m_Table  = addrNamed;

	Addr.a.m_Offset	= m_uMinimum;
	}

void CSpaceIFM::GetMaximum(CAddress &Addr)
{
	Addr.a.m_Extra	= m_uTable;

	Addr.a.m_Table  = addrNamed;

	Addr.a.m_Offset	= m_uMaximum;
	}

// End of File
