
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_LAETUSBARCODE_HPP
	
#define	INCLUDE_LAETUSBARCODE_HPP

class CLaetusBarcodeDriver;

// Data Type abbreviations
#define	AN	addrNamed
#define	BB	addrBitAsBit
#define	YY	addrByteAsByte
#define	WW	addrWordAsWord
#define	LL	addrLongAsLong
#define	RR	addrRealAsReal

// Space Definitions
enum {
	SOM =  1,	// Set Operating Mode (Page 9)
	LRS =  2,	// Read Result Status Byte (Page 11)
	LRD =  3,	// Read Result Data String (Page 11)
	LES =  4,	// Ask wrong reads Status Byte (Page 11)
	LED =  5,	// Ask wrong reads Data String (Page 11)
	LXC =  6,	// Clear wrong read buffer (Page 11)
	SRG =  7,	// Start/Stop Reading Gate command (Page 13)
	MCF =  8,	// Matchcode Configuration (Page 39)
	MC1 =  9,	// Matchcode 1 (Page 39)
	MC2 = 10,	// Matchcode 2 (Page 39)
	CCF = 11,	// Code Configuration (Page 17)
	CCS = 12,	// Specific Symbology Parameter String (Page 17)
	CCL = 13,	// Code Length String (Page 17)
	DCF = 14,	// Device Configuration (Page 25)
	OPC = 15,	// Operational Counters (Page 64)
	HOC = 16,	// Host Output Configuration (Page 47)
	TFE = 17,	// Host Conf. - Splitter String Contents"
	TFH = 18,	// Host Conf. - Header String"
	TFL = 19,	// Host Conf. - Code Length List String"
	TFM = 20,	// Host Conf. - Format Mask String"
	TFS = 21,	// Host Conf. - Separator String"
	TFT = 22,	// Host Conf. - Terminator String"
	PE1 = 23,	// Percentage Evaluation String 1 (Page 59)
	PE2 = 24,	// Percentage Evaluation String 2 (Page 59)
	PE3 = 25,	// Percentage Evaluation String 3 (Page 59)
	PE4 = 26,	// Percentage Evaluation String 4 (Page 59)
	PEX = 27,	// Execute Percentage Eval Read   (Page 59)
	SOC = 28,	// Switching Output Configuration (Page 33/35)
	DST = 29,	// Device Self Test (Page 65)
	EEP = 30,	// RAM / EEPROM Transfer (Page 58)

	USR  = 96,	// Send USRC
	USRC = 97,	// User Defined String Command
	USRR = 98,	// User Defined String Response
	SCK = 99,	// Use SICK Protocol
	};

// Handle Special Definitions
enum {
	HSNONE	= 0,
	HSEXTD	= 1,
	HSEXTH	= 2,
	HSALPH	= 3,
	HSDECI	= 4,
	};

#define	MAXINFO	18

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Device Options
//

// Device Specifiers
enum {
	LLS570 = 0,
	COCAM7 = 1,
	};

class CLaetusBarcodeDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CLaetusBarcodeDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	public:
		// Public Data
		CString	m_Drop;
		UINT	m_Unit;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Helper
		BOOL InvalidDropChar(TCHAR c);
	};


//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Comms Driver
//

class CLaetusBarcodeDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CLaetusBarcodeDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig(void);

		// Address Management
		BOOL	SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);

		// Address Helpers
		BOOL	DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);
		BOOL	DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Other Helpers

	protected:

		// Implementation
		void	AddSpaces(void);

		// Helpers
		UINT	GetItemType(UINT uTable);
		UINT	GetItemOffset(UINT uTable, BYTE bChar, UINT uDev);
		UINT	GetMCFOffset(BYTE bChar, BOOL fIsLLS);
		UINT	GetCCFOffset(BYTE bChar, BOOL fIsLLS);
		UINT	GetDCFOffset(BYTE bChar, BOOL fIsLLS);
		UINT	GetHOCOffset(BYTE bChar, BOOL fIsLLS);
		UINT	GetSOCOffset(BYTE bChar, BOOL fIsLLS);
		BYTE	GetItemLetter(UINT uTable, UINT uOffset, UINT uDev);
		BYTE	GetMCFLetter(UINT uOffset, BOOL fIsLLS);
		BYTE	GetCCFLetter(UINT uOffset, BOOL fIsLLS);
		BYTE	GetDCFLetter(UINT uOffset, BOOL fIsLLS);
		BYTE	GetHOCLetter(UINT uOffset, BOOL fIsLLS);
		BYTE	GetSOCLetter(UINT uOffset, BOOL fIsLLS);
	};

//////////////////////////////////////////////////////////////////////////
//
// Laetus Barcode Scanner Address Selection
//

class CLaetusBarcodeAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CLaetusBarcodeAddrDialog(CLaetusBarcodeDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data Members
		CLaetusBarcodeDriver *m_pLDriver;
		UINT	m_uDevice;
		CItem	*m_pConfig;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void	OnDblClk(UINT uID, CWnd &Wnd);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);
		BOOL	OnOkay(UINT uID);

		// Overridables
		void	ShowAddress(CAddress Addr);
		BOOL	AllowSpace(CSpace *pSpace);

		// Helpers
		void	ShowInfo(void);
		void	ClearInfo(void);
		BOOL	EnableAddrEntry(UINT uTable);
		void	ShowAddrAndInfo(CAddress Addr);
	};

// Info Strings
#define	S_0	"0  = "
#define	S_1	"1  = "
#define	S_2	"2  = "
#define	S_3	"3  = "
#define	S_4	"4  = "
#define	S_5	"5  = "
#define	S_6	"6  = "
#define	S_7	"7  = "
#define	S_8	"8  = "
#define	S_9	"9  = "
#define	S_A	"A  = "
#define	S_B	"B  = "
#define	S_C	"C  = "
#define	S_D	"D  = "
#define	S_E	"E  = "
#define	S_F	"F  = "
#define	S_10	"10 = "
#define	S_11	"11 = "

#define	S_SPACE		" "
#define	S_READING	"Reading Mode"
#define	S_PARAM		"Parametrization Mode"
#define	S_OPERATING	"Operating Data Mode"
#define	S_SELFTEST	"Self Test Mode"
#define	S_WRNONZERO	"Write non-zero value to execute"
#define	S_RESPFORMAT	"Response Format:"
#define	S_ONESTATUS	"<1 Status Character>"
#define	S_DATASTRING	"Data String"
#define	S_STRINGTAG	"Use String Tag of appropriate size."
#define	S_OPERSEL	"Select Operation As Follows:"
#define	S_WRDATA	"Write Data As Follows:"
#define	S_MINUS1	"Read returns -1 (429467295, FFFFFFFF)"
#define	S_RINGSELECT	"Select Wrong Reads Position (0 - 9)"
#define	S_ACKNAK	"ACK = 6, NAK = 21 (Hex 15)"
#define	S_NONE		"None"
#define	S_BIT		"Bit"
#define	S_BYTE		"Byte"
#define	S_WORD		"Word"
#define	S_LONG		"Long"
#define	S_REAL		"Real"
#define	S_STRING	"String"
#define	S_SELALPHA	"Select letter item to access."
#define	S_INVALPHA	"Not all letters are valid."
#define	S_SRG1		"Write 1 to Start, 2 to Stop"
#define	S_SRG2		"SRG will keep value until power cycle."
#define	S_SRG3		"If LLS570, Read will return a previous:"
#define	S_SRG4		"If COCAM700, Read will return 0"
#define	S_SRGNONE	"Ack/Nak/None"
#define	S_MATCHA	"A = MC1 Function";
#define	S_MATCHB	"B = MC2 Function";
#define	S_MATCHC	"C = Matchcode 1 (# of MC1 characters)";
#define	S_MATCHD	"D = Matchcode 2 (# of MC2 characters)";
#define	S_MATCHF	"F = MC1 Filter"
#define	S_MATCHG	"G = MC2 Filter"
#define	S_MATCHI	"I = MC1 Symbology"
#define	S_MATCHJ	"J = MC2 Symbology"
#define	S_MATCHR	"R = Reset group DD"
#define	S_MATCHT	"T = Set Teach-In"
#define	S_MATCH12	"MCFC and MCFD are Read Only."
#define	S_MATCH13	"MC1 and MC2 will hold the string data"
#define	S_CODE0		"'@' = Type of Symbology."
#define	S_CODEA		"A = Evaluation State"
#define	S_CODEC		"C = Specific Symbology Parameter String"
#define	S_CODED		"D = Decode Algorithm"
#define	S_CODEL		"L = Code Length String"
#define	S_CODEM		"M = Number of Multiple Reads";
#define	S_CODEP		"P = Check Digit";
#define	S_CODES		"S = Module Width";
#define	S_CODET		"T = Plausibility Check";
#define	S_CODEX		"X = Decode algorithm for code 39"
#define	S_CODECH1	"For CCFC and CCFL, the response is Char 1:"
#define	S_CODECH2	"CCS and CCL will hold the string data."
#define	S_DEVCFB	" B = Minimum module width for largest code"
#define	S_DEVCFD	" D = Dynamic reading configuration"
#define	S_DEVCFL	" L = Minimum reading distance"
#define	S_DEVCFM	" M = Min. for segmentation mode start/stop"
#define	S_DEVCFN	" N = Segmentation mode"
#define	S_DEVCFO	" O = Absolute value margin (quiet zone)"
#define	S_DEVCFP	" P = Lower code position limit"
#define	S_DEVCFQ	" Q = Upper code position limit"
#define	S_DEVCFR	" R = Pulse on switching output 'Result'"
#define	S_DEVCFS	" S = Scanning Frequency"
#define	S_DEVCFV	" V = Code label quality"
#define	S_DEVCFZ	" Z = Length of 'Result' output pulse"
#define S_OPCR1		"Trigger Event (c)"
#define S_OPCR2		"Good Read (g)"
#define S_OPCR3		"No Read (x)"
#define S_OPCR4		"Matchcode 1 (m)"
#define S_OPCR5		"Matchcode 2 (n)"
#define S_OPCR6		"No match (y)"
#define	S_OPCWA		"Write a non-zero value to reset a counter"
#define	S_TFCA		"A = Special Output Format A"
#define	S_TFCB		"B = Special Output Format B"
#define	S_TFCE		"E = Splitter String char count (TFE)"
#define	S_TFCF		"F = Special Output Format F"
#define	S_TFCH		"H = Header String char count (TFH)"
#define	S_TFCL		"L = Code List Length char count (TFL)"
#define	S_TFCM		"M = Format Mask char count (TFM)"
#define	S_TFCN		"N = Fill Character"
#define	S_TFCR		"R = Output Code Sorting"
#define	S_TFCS		"S = Separator char count (TFS)"
#define	S_TFCT		"T = Terminator char count (TFT)"
#define	S_TFC0		"TFH, TFL, TFM, TFS, TFT"
#define	S_TFC1		", TFE"
#define	S_TFC2		"will return string data in separate items."
#define	S_PE1A		"TT - Decoding time for 100 scans"
#define	S_PE1B		"MG - Long term value of ID quality"
#define	S_PE1C		"n  - Number of codes detected"
#define	S_PE1D		"AK - Distance configuration"
#define	S_PE2		"Code Contents"
#define	S_PE3A		"Code Symbology"
#define	S_PE3B		"ID Quality (read rate)"
#define	S_PE3C		"ST = Error Status"
#define	S_PE3D		"CP = Code Position"
#define	S_PE3E		"CL = Code Length"
#define	S_PE3F		"CA = Scanning expenditure"
#define	S_PE3G		"CS = Code security"
#define	S_PE3H		"CK = Code continuity"
#define	S_PE3I		"DI = Decoding direction"
#define	S_PE4A		"Pharma-code with fixed module width:"
#define	S_PE4B		"xxx% yyy%"
#define	S_PEX1		"Write 1 to PEX to read % evaluations"
#define	S_PEX2		"PE1, PE2, PE3, and PE4"
#define	S_PEX3		"when no data available.  Will return"
#define	S_PEX4		"0 on the scan when valid data received"
#define	S_SOCAC		"A - H = Expander 1-8 Function"
#define	S_SOCAL		"A = Output 1"
#define	S_SOCBL		"B = Output 2"
#define	S_SOCGL		"G = Beeper"
#define	S_SOCIC		"I = Expander Output Polarity"
#define	S_SOCKC		"K = Timing Switching Outputs"
#define	S_SOCLL		"L = Limit 1"
#define	S_SOCML		"M = Reference 1"
#define	S_SOCNL		"N = Timing 1"
#define	S_SOCOL		"O = Limit 2"
#define	S_SOCPL		"P = Reference 2"
#define	S_SOCQL		"Q = Timing 2"
#define	S_SOCRC		"R = 'Result' Output"
#define	S_SOCRL		"R = Timer Base"
#define	S_SOCSC		"S = 'Result' Polarity"
#define	S_SOCSL		"S = Outputs on during reset"
#define	S_SOCT		"T = Output Pulse Length"
#define	S_SOCUC		"U = Device Ready Function"
#define	S_SOCVC		"V = Device Ready Polarity"
#define	S_SOCVL		"V = Beeper Volume"
#define	S_SOCWL		"W = Output 1 result"
#define	S_SOCXC		"X = Expander Func State"
#define	S_SOCXL		"X = Output 2 result"
#define	S_DST		" Self Test"
#define	S_EEP1		"Store RAM to EEPROM - Write  55"
#define	S_EEP2		"Store EEPROM to RAM - Write 185"
#define	S_EEP3		"This command has a long response time."

#define	S_USERDEF0	"Send string in USRC"
#define	S_USERDEF1	"USRC - Format String according to"
#define	S_USERDEF2	"the device specification."
#define	S_USERDEF3	"Include only the command text portion."
#define	S_USERDEF4	"The driver will insert the Device Number,"
#define	S_USERDEF5	"the Start and Stop characters, and the LRC."
#define	S_USERDEF6	"USRR - The Response will contain \"6\" for ACK,"
#define	S_USERDEF7	"\"15\" for NAK, or the data string."
#define	S_USERDEF10	"Example: Read Configuration"
#define	S_USERDEF11	"Set USRC to \"3?LK\""
#define	S_USERDEF12	"The response will be similar to:"
#define	S_USERDEF13	"LKL0040A0100SGMaa"
#define	S_USERDEF14	"The programmer uses the first two characters"
#define	S_USERDEF15	"to put the remaining data in the proper"
#define	S_USERDEF16	"memory locations."
#define	S_USESICK1	"Valid for RS232 on AUX port, only."
#define	S_USESICK2	"Returns 1 on a read if SICK selected."
#define	S_USESICK3	"Write 1 to select SICK."
#define	S_USESICK4	"Any other value disables SICK protocol."


// Macro
#define	MS(w, x, y, z)	w[x].Printf( "%s%s", y, z )

// End of File

#endif
