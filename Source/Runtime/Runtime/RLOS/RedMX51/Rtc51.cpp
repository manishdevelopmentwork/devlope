
#include "Intern.hpp"

#include "Rtc51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Pmui51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 PMUI Real Time Clock
//

// Instantiator

IDevice * Create_Rtc51(CPmui51 *pPmui)
{
	IDevice *pDevice = New CRtc51(pPmui);

	pDevice->Open();

	return pDevice;
	}

// Constants

const DWORD CRtc51::constRtcMagic = 0x87654321;

// Constructor

CRtc51::CRtc51(CPmui51 *pPmui)
{
	m_pPmui = pPmui;

	m_pMem  = NULL;

	AfxGetObject("fram", 0, ISerialMemory, m_pMem);
	}

// Destructor

CRtc51::~CRtc51(void)
{
	m_pMem->Release();
	}

// IRtc

UINT CRtc51::GetCalibStatus(void)
{
	DWORD ds;

	m_pMem->GetData(addrRtc, PBYTE(&ds), sizeof(ds));

	return ds == constRtcMagic ? rtcCalibGood : rtcCalibBad;
	}

INT CRtc51::GetCalib(void)
{
	return GetCalFromChip();
	}

bool CRtc51::PutCalib(INT Calib)
{
	if ( Calib < -16 || Calib > 15 ) {

		return false;
		}

	CalibToMram(Calib);
	
	return WriteCalToChip(Calib);
	}

// Overridables

bool CRtc51::InitChip(void)
{
	CalibFromMram();

	return true;
	}

bool CRtc51::GetTimeFromChip(void)
{
	if( m_pPmui->GetRtcValid() ) {

		DWORD dwTime = m_pPmui->GetRtcTime();

		DWORD dwDays = m_pPmui->GetRtcDays();

		AfxTrace("Read: Days = %u  Time = %u\n", dwDays, dwTime);

		m_time = dwTime + dwDays * (60L * 60L * 24L);
		
		return true;
		}

	return false;
	}

bool CRtc51::GetSecsFromChip(UINT &uSecs)
{
	uSecs = m_pPmui->GetRtcTime() % 60;

	return true;
	}

bool CRtc51::WriteTimeToChip(void)
{
	DWORD dwTime = m_time % (60L * 60L * 24L);

	DWORD dwDays = m_time / (60L * 60L * 24L);

	AfxTrace("Write: Days = %u  Time = %u\n", dwDays, dwTime);

	if( m_pPmui->SetRtcDays(dwDays) ) {

		if( m_pPmui->SetRtcTime(dwTime) ) {

			return m_pPmui->SetRtcValid();
			}
		}

	return false;
	}

// Implementation

INT CRtc51::GetCalFromChip(void) 
{
	DWORD dwControl = m_pPmui->GetRtcCal();

	BOOL  fSign     = (dwControl & 0x10);

	BYTE  bMask     = 0x0F;

	return INT((dwControl & bMask) | (fSign ? -(bMask+1) : 0));
	}

bool CRtc51::WriteCalToChip(INT Cal) 
{
	if( m_pPmui->SetRtcCal(Cal) ) {

		return true;
		}
	
	return false;
	}

void CRtc51::CalibFromMram(void)
{
	DWORD ds[2];

	m_pMem->GetData(addrRtc, PBYTE(&ds), sizeof(ds));

	if( ds[0] == constRtcMagic ) {

		INT nCalib = ds[1];		

		WriteCalToChip(nCalib);
			
		return;
		}
	}

void CRtc51::CalibToMram(INT nCalib)
{
	DWORD ds[2];

	ds[0] = constRtcMagic;

	ds[1] = nCalib;

	m_pMem->PutData(addrRtc, PBYTE(&ds), sizeof(ds));
	}

// End of File
