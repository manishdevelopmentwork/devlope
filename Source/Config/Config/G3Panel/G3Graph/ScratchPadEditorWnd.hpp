
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Graphics Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ScratchPadEditorWnd_HPP

#define INCLUDE_ScratchPadEditorWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Graphics Editor for Scratch Pad
//

class CScratchPadEditorWnd : public CPageEditorWnd
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CScratchPadEditorWnd(void);

		// Destructor
		~CScratchPadEditorWnd(void);

		// Attributes
		CSize GetBestSize(void) const;
	};

// End of File

#endif
