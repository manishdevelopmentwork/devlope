
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Filing System Support
//

// Static Data

static IFileSupport * m_pFileSupport = NULL;

// Code

global void Bind_FileSupport(void)
{
	AfxGetObject("aeon.filesupport", 0, IFileSupport, m_pFileSupport);
}

global void Free_FileSupport(void)
{
	m_pFileSupport->Release();

	m_pFileSupport = NULL;
}

clink int isatty(int fd)
{
	return m_pFileSupport->IsConsole(fd);
}

clink int rename(char const *from, char const *to)
{
	return m_pFileSupport->Rename(from, to);
}

clink int unlink(char const *name)
{
	return m_pFileSupport->Unlink(name);
}

clink int stat(char const *name, struct stat *buffer)
{
	return m_pFileSupport->Stat(name, buffer);
}

clink int utime(char const *name, time_t time)
{
	return m_pFileSupport->UTime(name, time);
}

clink int chmod(char const *name, mode_t mode)
{
	return m_pFileSupport->ChMod(name, mode);
}

clink int open(char const *name, int oflag, int pmode)
{
	return m_pFileSupport->Open(name, oflag, pmode);
}

clink int close(int fd)
{
	return m_pFileSupport->Close(fd);
}

clink int fstat(int fd, struct stat *buffer)
{
	return m_pFileSupport->FStat(fd, buffer);
}

clink int read(int fd, void *buffer, unsigned int count)
{
	return m_pFileSupport->Read(fd, buffer, count);
}

clink int write(int fd, void const *buffer, unsigned int count)
{
	return m_pFileSupport->Write(fd, buffer, count);
}

clink off_t lseek(int fd, off_t offset, int origin)
{
	return m_pFileSupport->LSeek(fd, offset, origin);
}

clink int ftruncate(int fd, off_t size)
{
	return m_pFileSupport->FTruncate(fd, size);
}

clink int ioctl(int fd, int func, void *data)
{
	return m_pFileSupport->IoCtl(fd, func, data);
}

clink char * getcwd(char *buff, size_t size)
{
	return m_pFileSupport->GetCwd(buff, size);
}

clink int chdir(char const *name)
{
	return m_pFileSupport->ChDir(name);
}

clink int rmdir(char const *name)
{
	return m_pFileSupport->RmDir(name);
}

clink int mkdir(char const *name, mode_t mode)
{
	return m_pFileSupport->MkDir(name);
}

clink int scandir(char const *name, struct dirent ***list, int (*selector)(struct dirent const *), int (*compare)(struct dirent const **, struct dirent const **))
{
	CScanDirArgs args;

	args.name     = name;
	args.list     = list;
	args.selector = selector;
	args.compare  = compare;

	return m_pFileSupport->ScanDir(args);
}

clink int sync(void)
{
	return m_pFileSupport->Sync();
}

clink int mapname(char *name, int add)
{
	return m_pFileSupport->MapName(name, add);
}

// End of File
