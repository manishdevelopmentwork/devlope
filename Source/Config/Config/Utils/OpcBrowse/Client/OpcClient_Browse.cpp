
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Externals

extern BOOL g_fVerbose;

// Browse Support

bool COpcClient::Browse(CDevice *pDevice, CStringArray &List)
{
	CString Root;

	Root.Printf("p=Objects;ns=0;i=%u", OpcUaId_ObjectsFolder);

	return Browse(pDevice, List, Root);
}

bool COpcClient::Browse(CDevice *pDevice, CStringArray &List, CString From)
{
	AfxTrace("opc: browse\n");

	COpcNodeId Node;

	CString    Root;

	if( From.StartsWith("p=") ) {

		From = From.Mid(2);

		Root = From.StripToken(';');
	}

	if( Node.Decode(From) ) {

		// Request Parameters

		OpcUa_RequestHeader RequestHeader;

		FillRequestHeader(&RequestHeader, pDevice);

		OpcUa_ViewDescription ViewDescription;

		OpcUa_ViewDescription_Initialize(&ViewDescription);

		INT                      nNodes = 1;

		OpcUa_BrowseDescription *pNodes = OpcAlloc(nNodes, OpcUa_BrowseDescription);

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_BrowseDescription *pNode = pNodes + n;

				OpcUa_BrowseDescription_Initialize(pNode);

				pNode->BrowseDirection = OpcUa_BrowseDirection_Forward;
				pNode->IncludeSubtypes = OpcUa_True;
				pNode->NodeClassMask   = 0;
				pNode->ResultMask      = 0x3F;

				if( n == 0 ) {

					pNode->ReferenceTypeId.NamespaceIndex     = 0;
					pNode->ReferenceTypeId.IdentifierType     = OpcUa_IdentifierType_Numeric;
					pNode->ReferenceTypeId.Identifier.Numeric = OpcUaId_HierarchicalReferences;
				}

				if( n == 1 ) {

					pNode->ReferenceTypeId.NamespaceIndex     = 0;
					pNode->ReferenceTypeId.IdentifierType     = OpcUa_IdentifierType_Numeric;
					pNode->ReferenceTypeId.Identifier.Numeric = OpcUaId_HasComponent;
				}

				if( n == 2 ) {

					pNode->ReferenceTypeId.NamespaceIndex     = 0;
					pNode->ReferenceTypeId.IdentifierType     = OpcUa_IdentifierType_Numeric;
					pNode->ReferenceTypeId.Identifier.Numeric = OpcUaId_HasProperty;
				}

				CopyNodeId(&pNode->NodeId, &Node);
			}
		}

		// Response Parameters

		OpcUa_ResponseHeader ResponseHeader;

		OpcUa_ResponseHeader_Initialize(&ResponseHeader);

		OpcUa_Int32            NoOfResults         = 0;

		OpcUa_BrowseResult   * pResults            = NULL;

		OpcUa_Int32            NoOfDiagnosticInfos = 0;

		OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_Browse(pDevice->m_hChannel,
							       &RequestHeader,
							       &ViewDescription,
							       100,
							       nNodes,
							       pNodes,
							       &ResponseHeader,
							       &NoOfResults,
							       &pResults,
							       &NoOfDiagnosticInfos,
							       &pDiagnosticInfos
		);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			if( !ProcessBrowseData(pDevice, List, Root, NoOfResults, pResults) ) {

				Code = OpcUa_Bad;
			}
		}

		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_BrowseDescription *pNode = pNodes + n;

				OpcUa_BrowseDescription_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_BrowseResult *pResult = pResults + n;

				OpcUa_BrowseResult_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
	}

	return false;
}

bool COpcClient::BrowseNext(CDevice *pDevice, CStringArray &List, CString Root, OpcUa_ByteString const &Cont)
{
	AfxTrace("opc: browsenext\n");

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;

	OpcUa_BrowseResult   * pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	// Service Invocation

	OpcUa_StatusCode Code = OpcUa_ClientApi_BrowseNext(pDevice->m_hChannel,
							   &RequestHeader,
							   OpcUa_False,
							   1,
							   &Cont,
							   &ResponseHeader,
							   &NoOfResults,
							   &pResults,
							   &NoOfDiagnosticInfos,
							   &pDiagnosticInfos
	);

	// Response Processing

	if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

		if( !ProcessBrowseData(pDevice, List, Root, NoOfResults, pResults) ) {

			Code = OpcUa_Bad;
		}
	}

	// Parameter Release

	{
		for( INT n = 0; n < NoOfResults; n++ ) {

			OpcUa_BrowseResult *pResult = pResults + n;

			OpcUa_BrowseResult_Clear(pResult);
		}
	}

	FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

	OpcUa_Free(pResults);

	// Check for Success

	return Code == OpcUa_Good;
}

bool COpcClient::ProcessBrowseData(CDevice *pDevice, CStringArray &List, CString Root, OpcUa_Int32 NoOfResults, OpcUa_BrowseResult *pResults)
{
	bool fOkay = true;

	for( int r = 0; fOkay && r < NoOfResults; r++ ) {

		OpcUa_BrowseResult *pResult = pResults + r;

		for( int n = 0; fOkay && n < pResult->NoOfReferences; n++ ) {

			OpcUa_ReferenceDescription *pRef = pResult->References + n;

			CString Node = ((COpcNodeId &) pRef->NodeId.NodeId).Encode();

			if( !Node.IsEmpty() ) {

				CString Path = Root + '/' + pRef->DisplayName.Text.strContent;

				CString Full = "p=" + Path + ';' + Node;

				UINT    Type;

				UINT	Size;

				bool    fWrite;

				if( ReadType(pDevice, Type, Size, fWrite, pRef->NodeId.NodeId) ) {

					CString More;

					UINT    Wire;

					switch( (Wire = GetWireType(Type)) ) {

						case OpcUaId_Boolean:
						case OpcUaId_SByte:
						case OpcUaId_Byte:
						case OpcUaId_Int16:
						case OpcUaId_UInt16:
						case OpcUaId_Int32:
						case OpcUaId_UInt32:
						case OpcUaId_Int64:
						case OpcUaId_UInt64:
						case OpcUaId_Float:
						case OpcUaId_Double:
						case OpcUaId_DateTime:
						case OpcUaId_String:
						{
							if( Size == NOTHING ) {

								if( g_fVerbose ) {

									AfxTrace("opc: \"%s\" added with type %u (%u)\n", PCSTR(Path), Type, Wire);
								}

								More = "p=" + Path + CPrintf(";t=%u;w=%u;", Wire, fWrite ? 1 : 0) + Node;

								List.Append(More);
							}
							else {
								if( Wire != OpcUaId_String ) {

									if( g_fVerbose ) {

										AfxTrace("opc: \"%s\" added with type %u[%u] (%u)\n", PCSTR(Path), Type, Size, Wire);
									}

									More = "p=" + Path + CPrintf(";t=%u;a=%u;w=%u;", Wire, Size, fWrite ? 1 : 0) + Node;

									List.Append(More);
								}
							}
						}
						break;

						default:
						{
							if( g_fVerbose ) {

								AfxTrace("opc: \"%s\" has unusable type %u\n", PCSTR(Path), Type);
							}
						}
						break;
					}
				}
				else {
					if( g_fVerbose ) {

						AfxTrace("opc: \"%s\" has bad type\n", PCSTR(Path));
					}
				}

				if( !Browse(pDevice, List, Full) ) {

					fOkay = false;
				}
			}
		}

		if( pResult->ContinuationPoint.Length > 0 ) {

			if( !BrowseNext(pDevice, List, Root, pResult->ContinuationPoint) ) {

				fOkay = false;
			}
		}
	}

	return fOkay;
}

UINT COpcClient::GetWireType(UINT uType)
{
	switch( uType ) {

		case OpcUaId_Double:
		case OpcUaId_Float:
		case OpcUaId_Byte:
		case OpcUaId_SByte:
		case OpcUaId_Int16:
		case OpcUaId_UInt16:
		case OpcUaId_Int32:
		case OpcUaId_UInt32:
		case OpcUaId_Int64:
		case OpcUaId_UInt64:
		case OpcUaId_Boolean:
		case OpcUaId_DateTime:
		case OpcUaId_String:

			return uType;

		case OpcUaId_Duration:

			return OpcUaId_Double;

		case OpcUaId_ServerState:
		case OpcUaId_ServerStatusDataType:
		case OpcUaId_RedundancySupport:

			return OpcUaId_Int32;

		case OpcUaId_LocaleId:
		case OpcUaId_SignedSoftwareCertificate:

			return OpcUaId_String;

		case OpcUaId_UtcTime:

			return OpcUaId_DateTime;
	}

	return uType;
}

// End of File
