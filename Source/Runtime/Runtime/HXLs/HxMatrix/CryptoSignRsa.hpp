
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoSignRsa_HPP

#define INCLUDE_CryptoSignRsa_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoSign.hpp"

//////////////////////////////////////////////////////////////////////////
//
// RSA Cryptographic Signer
//

class CCryptoSignRsa : public CCryptoSign
{
	public:
		// Constructor
		CCryptoSignRsa(void);

		// Destructor
		~CCryptoSignRsa(void);

		// ICryptoSign
		CString GetName(void);
		BOOL    Initialize(PCBYTE pKey, UINT uKey, CString const &Pass);
		void    Finalize(void);

	protected:
		// Data Members
		psRsaKey_t m_Key;
		UINT       m_uKeyBits;
		UINT       m_uKeySize;

		// Implementation
		void MakeFile(char *pName, PCBYTE pData, UINT uSize);
		void PostInitialize(void);
	};

// End of File

#endif
