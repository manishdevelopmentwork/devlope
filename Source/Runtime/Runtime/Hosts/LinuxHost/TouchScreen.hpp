
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_TouchScreen_HPP

#define INCLUDE_TouchScreen_HPP

//////////////////////////////////////////////////////////////////////////
//
// Touch Screen Device Driver
//

class CTouchScreen : public ITouchScreen, public IClientProcess
{
public:
	// Constructor
	CTouchScreen(void);

	// Destructor
	~CTouchScreen(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IDevice
	BOOL METHOD Open(void);

	// ITouchScreen
	void   METHOD GetCellSize(int &xCell, int &yCell);
	void   METHOD GetDispCells(int &xDisp, int &yDisp);
	void   METHOD SetBeepMode(bool fBeep);
	void   METHOD SetDragMode(bool fDrag);
	PCBYTE METHOD GetMap(void);
	void   METHOD SetMap(PCBYTE pMap);
	bool   METHOD GetRaw(int &xRaw, int &yRaw);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

protected:
	// Data Members
	ULONG         m_uRefs;
	HTHREAD       m_hThread;
	bool          m_fTimer;
	int           m_fd;
	int           m_td;
	int           m_ed;
	IInputQueue * m_pInput;
	IDisplay    * m_pDisplay;
	IPointer    * m_pPointer;
	IBeeper     * m_pBeeper;
	PCBYTE	      m_pMap;
	bool	      m_fBeep;
	bool	      m_fDrag;
	int           m_xDisp;
	int           m_yDisp;
	int	      m_xCells;
	int	      m_yCells;
	int           m_xRaw;
	int           m_yRaw;
	int           m_xTmp;
	int           m_yTmp;
	int	      m_xMin;
	int	      m_yMin;
	int           m_pMin;
	int           m_xMax;
	int           m_yMax;
	int           m_pMax;
	int	      m_xVal;
	int	      m_yVal;
	int           m_pVal;
	bool          m_fPress;
	bool	      m_fValid;
	int           m_nPress;
	int           m_xPress;
	int           m_yPress;

	// Implementation
	void SetWaitTimer(fd_set &set, UINT ms);
	bool WaitForEvent(fd_set &set);
	bool Close(void);
	bool StoreEvent(UINT uType);
	int  Decode(int nRaw, int nDisp, int nMin, int nMax);
	bool IsActive(void);
};

// End of File

#endif
