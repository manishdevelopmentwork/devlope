
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Driver Support
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Slave Driver Implementation
//

// Constructor

CSlaveDriver::CSlaveDriver(void)
{
	m_pSlaveHelper = NULL;
	}

// IUnknown

HRESULT CSlaveDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsSlave);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

ULONG CSlaveDriver::AddRef(void)
{
	return CCommsDriver::AddRef();
	}

ULONG CSlaveDriver::Release(void)
{
	return CCommsDriver::Release();
	}

// IDriver

WORD CSlaveDriver::GetCategory(void)
{
	return DC_COMMS_SLAVE;
	}

// ICommsSlave

void CSlaveDriver::SetHelper(ISlaveHelper *pHelper)
{
	m_pSlaveHelper = pHelper;
	}

// IO

CCODE CSlaveDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pSlaveHelper->Read(Addr, pData, uCount);
	}

CCODE CSlaveDriver::Read(AREF Addr, PDWORD pData, IMetaData **ppMeta)
{
	return m_pSlaveHelper->Read(Addr, pData, ppMeta);
	}

CCODE CSlaveDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pSlaveHelper->Write(Addr, pData, uCount);
	}

// Meta Data

UINT CSlaveDriver::GetMetaNumber(AREF Addr, WORD ID)
{
	DWORD      dv = 0;

	IMetaData *pm = NULL;

	Read(Addr, &dv, &pm);

	if( pm ) {

		UINT uData = GetMetaNumber(pm, ID);

		pm->Release();

		return uData;
		}

	return 0;
	}

BOOL CSlaveDriver::GetMetaString(AREF Addr, WORD ID, PTXT pBuff, UINT uSize)
{
	DWORD      dv = 0;

	IMetaData *pm = NULL;

	Read(Addr, &dv, &pm);

	if( pm ) {

		BOOL fData = GetMetaString(pm, ID, pBuff, uSize);

		pm->Release();

		return fData;
		}

	return 0;
	}

UINT CSlaveDriver::GetMetaNumber(IMetaData *pm, WORD ID)
{
	if( pm ) {

		return pm->GetMetaData(ID);
		}

	return 0;
	}

BOOL CSlaveDriver::GetMetaString(IMetaData *pm, WORD ID, PTXT pBuff, UINT uSize)
{
	if( pm ) {

		PWORD pText = PWORD(pm->GetMetaData(ID));

		if( pText ) {

			for( UINT n = 0; (pBuff[n] = BYTE(pText[n])); n++ );

			Free(pText);

			return TRUE;
			}
		}

	*pBuff = 0;

	return FALSE;
	}

// End of File
