
#include "intern.hpp"

#include "emroc2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// TLP Structure
//

struct TLP {

	UINT  m_Number;
	PCSTR m_Name;
	PCSTR m_Short;
	UINT  m_Device;

	// Device Definitions: 1 - FloBoss 103/104, 2 - ROC800

	
	};

//////////////////////////////////////////////////////////////////////////
//
// Point Type Definitions (T)
//

static TLP const m_pPointType[] = {

	{	0,	"0-Configurable Opcode",			"OPC",		devFlo103104	},
	{	1,	"1-Discrete Inputs",				"DIN",		0		},
	{	2,	"2-Discrete Outputs",				"DOU",		devFlo103104	},
	{	3,	"3-Analog Inputs",				"AIN",		devFlo103104	},
	{	4,	"4-Analog Outputs",				"AOU",		devFlo103104	},
	{	5,	"5-Pulse Inputs",				"PIN",		devFlo103104	},
	{	6,	"6-PID Parameters",				"PID",		0		},
	{	7,	"7-AGA Flow Parameters",			"AGA",		devFlo103104	},
	{	8,	"8-History Parameters",				"HST",		devFlo103104	},
	{	9,	"9-AGA Switched Run Parameters",		"???",		0		},
	{	10,	"10-AGA Flow Calculation Values",		"FLW",		devFlo103104	},
	{	11,	"11-Tanks",					"???",		0		},
	{	12,	"12-Clock",					"CLK",		devFlo103104	},
	{	13,	"13-Flags",					"FLG",		devFlo103104	},
	{	14,	"14-Comm Ports",				"COM",		0		},  	// Should we support?
	{	15,	"15-System Variables",				"SYS",		devFlo103104	},
	{	16,	"16-FST Registers",				"FST",		devFlo103104	},
	{	17,	"17-Soft Point Parameters",			"SFP",		devFlo103104	},
	{	18,	"",						"???",		0		},	
	{	19,	"19-Database Parameters",			"DBP",		devFlo103104	},
	{	20,	"20-Tasks",					"???",		0		},
	{	21,	"21-Information for User Defined Points 1",	"UDPI",		devFlo103104	},
	{	22,	"22-Information for User Defined Points 2",	"UDPI",		0		},
	{	23,	"23-Information for User Defined Points 3",	"UDPI",		0		},
	{	24,	"24-Information for User Defined Points 4",	"UDPI",		0		},
	{	25,	"25-Information for User Defined Points 5",	"UDPI",		0		},
	{	26,	"26-Information for User Defined Points 6",	"UDPI",		0		},
	{	27,	"27-Information for User Defined Points 7",	"UDPI",		0		},
	{	28,	"28-Information for User Defined Points 8",	"UDPI",		0		},
	{	29,	"29-Information for User Defined Points 9",	"UDPI",		0		},
	{	30,	"30-Information for User Defined Points 10",	"UDPI",		0		},
	{	31,	"31-Information for User Defined Points 11",	"UDPI",		0		},
	{	32,	"32-Information for User Defined Points 12",	"UDPI",		0		},
	{	33,	"33-Information for User Defined Points 13",	"UDPI",		0		},
	{	34,	"34-Information for User Defined Points 14",	"UDPI",		0		},
	{	35,	"35-Information for User Defined Points 15",	"UDPI",		0		},
	{	36,	"36-Information for User Defined Points 16",	"UDPI",		0		},
	{	37,	"37-Information for User Defined Points 17",	"UDPI",		0		},
	{	38,	"38-Information for User Defined Points 18",	"UDPI",		0		},
	{	39,	"39-Information for User Defined Points 19",	"UDPI",		0		},
	{	40,	"40-Mulitple Variable Sensor",			"???",		0		},
	{	41,	"41-Run Parameters",				"RUN",		devFlo103104	},
	{	42,	"42-Extra Run Parameters",			"ERN",		devFlo103104	},
	{	43,	"43-User List Parameters",			"ULP",		devFlo103104	},
	{	44,	"44-Power Control Parameters",			"PWR",		devFlo103104	},
	{	45,	"45-Meter Calibration and Sampler",		"SMP",		devFlo103104	},
	{	46,	"46-Meter Configuration Parameters",		"AGANEW",	devFlo103104	},
	{	47,	"47-Meter Flow Values",				"FLWNEW",	devFlo103104	},
	{	48,	"48-PID Control Parameters",			"???",		0		},
	{	49,	"",						"???",		0		},
	{	50,	"",						"???",		0		},						
	{	51,	"",						"???",		0		},
	{	52,	"52-Battery Parameters",			"???",		0		},
	{	53,	"53-Modbus Configuration Parameters",		"MBPAR",	0		},  	// Should we support?
	{	54,	"54-Modbus Function Tables",			"MBF",		0		},  	// Should we support?
	{	55,	"55-Modbus Special Function Table",		"MBSFT",	0		},  	// Should we support?
	{	56,	"56-Analog Input Calibration Parameters",	"AIC",		devFlo103104	},
	{	57,	"57-Keypad / LogOn Security Parameters",	"???",		0		},
	{	58,	"58-Revision Information",			"REV",		devFlo103104	},
	{	59,	"59-Program Flash Control Parameters",		"???",		0		},
	{	60,	"",						"???",		0		},
	{	61,	"",						"???",		0		},
	{	62,	"",						"???",		0		},
	{	63,	"",						"???",		0		},
	{	64,	"",						"???",		0		},
	{	65,	"",						"???",		0		},
	{	66,	"",						"???",		0		},
	{	67,	"",						"???",		0		},
	{	68,	"",						"???",		0		},
	{	69,	"",						"???",		0		},
	{	70,	"",						"???",		0		},
	{	71,	"71-Cause Configuration",			"UDP71",	devRoc800	},
	{	72,	"72-Effect Configuration",			"UDP72",	devRoc800	},
	{	73,	"",						"???",		0		},
	{	74,	"",						"???",		0		},
	{	75,	"",						"???",		0		},
	{	76,	"",						"???",		0		},
	{	77,	"",						"???",		0		},
	{	78,	"",						"???",		0		},
	{	79,	"79-Calibration Event",				"???",		0		},
	{	80,	"",						"???",		0		},
	{	81,	"81-Logic Alarm Parameters",			"???",		0		},
	{	82,	"",						"???",		0		},
	{	83,	"83-User Analog Values",			"???",		0		},
	{	84,	"84-User Discrete Values",			"???",		0		},
	{	85,	"",						"???",		0		},
	{	86,	"86-Extended History Parameters",		"EHST",		devFlo103104	},
	{	87,	"87-Expanded IO Information",			"EIO",		devRoc800	},
	{	88,	"",						"???",		0,		},
	{	89,	"",						"???",		0,		},
	{	90,	"",						"???",		0,		},
	{	91,	"91-System Variables",				"SYS",		devRoc800	},
	{	92,	"92-Logon Parameters",				"LOGON",	devRoc800	},
	{	93,	"93-License Key Information",			"LKI",		devRoc800	},
	{	94,	"94-User C Configuration",			"UCC",		devRoc800	},
	{	95,	"95-ROC Comm Ports",				"COM",		devRoc800	},
	{	96,	"96-FST Parameters",				"FST",		devRoc800	},
	{	97,	"97-FST Register Tags",				"FSTT",		devRoc800	},
	{	98,	"98-Soft Point Parameters",			"SFP",		devRoc800	},
	{	99,	"99-Configurable Opcode",			"OPC",		devRoc800	},
	{      100,	"100-Power Control Parmeters",			"PWR",		devRoc800	},
	{      101,	"101-Discrete Inputs",				"DIN",		devRoc800	},   
	{      102,	"102-Discrete Outputs",				"DOU",		devRoc800	},
	{      103,	"103-Analog Inputs",				"AIN",		devRoc800	},
	{      104,	"104-Analog Outputs",				"AOU",		devRoc800	},
	{      105,	"105-Pulse Inputs",				"PIN",		devRoc800	},
	{      106,	"",						"???",		0,		},
	{      107,	"",						"???",		0,		},
	{      108,	"108-Multivariable Sensor",			"MVS",		devRoc800	},
	{      109,	"109-System Analog Inputs",			"SAI",		devRoc800	},
	{      110,	"",						"???",		0,		},
	{      111,	"",						"???",		0,		},
	{      112,	"",						"???",		0,		},
	{      113,	"",						"???",		0,		},
	{      114,	"",						"???",		0,		},
	{      115,	"",						"???",		0,		},
	{      116,	"",						"???",		0,		},
	{      117,	"117-Modbus Configuration Parameters",		"MBPAR",	0,		},	// Should we support?
	{      118,	"118-Modbus Register to TLP Mapping",		"MBMAP",	0,		},	// Should we support?
	{      119,	"119-Modbus Event, Alarm, and History",		"MBEAH",	0,		},	// Should we support?
	{      120,	"120-Mobus Master Modem Configuration",		"MBMOD",	0,		},	// Should we support?
	{      121,	"121-Modbus Master Table",			"MBMAS",	0,		},	// Should we support?
	{      122,	"122-DS800 Configuration",			"DS8",		devRoc800	},
	{      123,	"123-Security Group Configuration",		"GRP",		devRoc800	}, 
	{      124,	"124-History Segment Configuration",		"HSC",		devRoc800	},
	{      125,	"125-History Segment 0 Point Configuration",	"HS0",		devRoc800	},
	{      126,	"",						"???",		0,		},
	{      127,	"",						"???",		0,		},
	{      128,	"",						"???",		0,		},
	{      129,	"",						"???",		0,		},
	{      130,	"",						"???",		0,		},
	{      131,	"",						"???",		0,		},
	{      132,	"",						"???",		0,		},
	{      133,	"",						"???",		0,		},
	{      134,	"",						"???",		0,		},
	{      135,	"",						"???",		0,		},
	{      136,	"136-ROC Clock",				"CLK",		devRoc800	},
	{      137,	"137-Internet Configuration Parameters",	"ICP",		devRoc800	},
	{      138,	"138-User Program Configuration",		"UPGM",		devRoc800	},
	{      139,	"139-Smart IO Module Information",		"SIOM",		devRoc800	},
	{      140,	"140-AC Input Output Module",			"ACIO",		devRoc800	},	
	};

//////////////////////////////////////////////////////////////////////////
//
// Logical Number Definitions (L)
//

static TLP const m_pLogical[] = {

	{	1,	"",			"A1"		},
	{	2,	"",			"A2"		},
	{	3,	"",			"A3"		},
	{	4,	"",			"A4"		},
	{	5,	"",			"A5"		},
	{	6,	"",			"A6"		},
	{	7,	"",			"A7"		},
	{	8,	"",			"A8"		},
	{	9,	"",			"A9"		},
	{	10,	"",			"A10"		},
	{	11,	"",			"A11"		},
	{	12,	"",			"A12"		},
	{	13,	"",			"A13"		},
	{	14,	"",			"A14"		},
	{	15,	"",			"A15"		},
	{	16,	"",			"A16"		},
	{	17,	"",			"B1"		},
	{	18,	"",			"B2"		},
	{	19,	"",			"B3"		},
	{	20,	"",			"B4"		},
	{	21,	"",			"B5"		},
	{	22,	"",			"B6"		},
	{	23,	"",			"B7"		},
	{	24,	"",			"B8"		},
	{	25,	"",			"B9"		},
	{	26,	"",			"B10"		},
	{	27,	"",			"B11"		},
	{	28,	"",			"B12"		},
	{	29,	"",			"B13"		},
	{	30,	"",			"B14"		},
	{	31,	"",			"B15"		},
	{	32,	"",			"B16"		},
	{	33,	"",			"C1"		},
	{	34,	"",			"C2"		},
	{	35,	"",			"C3"		},
	{	36,	"",			"C4"		},
	{	37,	"",			"C5"		},
	{	38,	"",			"C6"		},
	{	39,	"",			"C7"		},
	{	40,	"",			"C8"		},
	{	41,	"",			"C9"		},
	{	42,	"",			"C10"		},
	{	43,	"",			"C11"		},
	{	44,	"",			"C12"		},
	{	45,	"",			"C13"		},
	{	46,	"",			"C14"		},
	{	47,	"",			"C15"		},
	{	48,	"",			"C16"		},
	{	49,	"",			"D1"		},
	{	50,	"",			"D2"		},
	{	51,	"",			"D3"		},
	{	52,	"",			"D4"		},
	{	53,	"",			"D5"		},
	{	54,	"",			"D6"		},
	{	55,	"",			"D7"		},
	{	56,	"",			"D8"		},
	{	57,	"",			"D9"		},
	{	58,	"",			"D10"		},
	{	59,	"",			"D11"		},
	{	60,	"",			"D12"		},
	{	61,	"",			"D13"		},
	{	62,	"",			"D14"		},
	{	63,	"",			"D15"		},
	{	64,	"",			"D16"		},
	{	65,	"",			"E1"		},
	{	66,	"",			"E2"		},
	{	67,	"",			"E3"		},
	{	68,	"",			"E4"		},
	{	69,	"",			"E5"		},
	{	70,	"",			"E6"		},
	{	71,	"",			"E7"		},
	{	72,	"",			"E8"		},
	{	73,	"",			"E9"		},
	{	74,	"",			"E10"		},
	{	75,	"",			"E11"		},
	{	76,	"",			"E12"		},
	{	77,	"",			"E13"		},
	{	78,	"",			"E14"		},
	{	79,	"",			"E15"		},
	{	80,	"",			"E16"		},
	{	81,	"",			"F1"		},
	{	82,	"",			"F2"		},
	{	83,	"",			"F3"		},
	{	84,	"",			"F4"		},
	{	85,	"",			"F5"		},
	{	86,	"",			"F6"		},
	{	87,	"",			"F7"		},
	{	88,	"",			"F8"		},
	{	89,	"",			"F9"		},
	{	90,	"",			"F10"		},
	{	91,	"",			"F11"		},
	{	92,	"",			"F12"		},
	{	93,	"",			"F13"		},
	{	94,	"",			"F14"		},
	{	95,	"",			"F15"		},
	{	96,	"",			"F16"		},
	{	97,	"",			"G1"		},
	{	98,	"",			"G2"		},
	{	99,	"",			"G3"		},
	{	100,	"",			"G4"		},
	{	101,	"",			"G5"		},
	{	102,	"",			"G6"		},
	{	103,	"",			"G7"		},
	{	104,	"",			"G8"		},
	{	105,	"",			"G9"		},
	{	106,	"",			"G10"		},
	{	107,	"",			"G11"		},
	{	108,	"",			"G12"		},
	{	109,	"",			"G13"		},
	{	110,	"",			"G14"		},
	{	111,	"",			"G15"		},
	{	112,	"",			"G16"		},
	{	113,	"",			"H1"		},
	{	114,	"",			"H2"		},
	{	115,	"",			"H3"		},
	{	116,	"",			"H4"		},
	{	117,	"",			"H5"		},
	{	118,	"",			"H6"		},
	{	119,	"",			"H7"		},
	{	120,	"",			"H8"		},
	{	121,	"",			"H9"		},
	{	122,	"",			"H10"		},
	{	123,	"",			"H11"		},
	{	124,	"",			"H12"		},
	{	125,	"",			"H13"		},
	{	126,	"",			"H14"		},
	{	127,	"",			"H15"		},
	{	128,	"",			"H16"		},

	{	129,	"",			"I1"		},
	{	130,	"",			"I2"		},
	{	131,	"",			"I3"		},
	{	132,	"",			"I4"		},
	{	133,	"",			"I5"		},
	{	134,	"",			"I6"		},
	{	135,	"",			"I7"		},
	{	136,	"",			"I8"		},
	{	137,	"",			"I9"		},
	{	138,	"",			"I10"		},
	{	139,	"",			"I11"		},
	{	140,	"",			"I12"		},
	{	141,	"",			"I13"		},
	{	142,	"",			"I14"		},
	{	143,	"",			"I15"		},
	{	144,	"",			"I16"		},
	{	145,	"",			"J1"		},
	{	146,	"",			"J2"		},
	{	147,	"",			"J3"		},
	{	148,	"",			"J4"		},
	{	149,	"",			"J5"		},
	{	150,	"",			"J6"		},
	{	151,	"",			"J7"		},
	{	152,	"",			"J8"		},
	{	153,	"",			"J9"		},
	{	154,	"",			"J10"		},
	{	155,	"",			"J11"		},
	{	156,	"",			"J12"		},
	{	157,	"",			"J13"		},
	{	158,	"",			"J14"		},
	{	159,	"",			"J15"		},
	{	160,	"",			"J16"		},
	{	161,	"",			"K1"		},
	{	162,	"",			"K2"		},
	{	163,	"",			"K3"		},
	{	164,	"",			"K4"		},
	{	165,	"",			"K5"		},
	{	166,	"",			"K6"		},
	{	167,	"",			"K7"		},
	{	168,	"",			"K8"		},
	{	169,	"",			"K9"		},
	{	170,	"",			"K10"		},
	{	171,	"",			"K11"		},
	{	172,	"",			"K12"		},
	{	173,	"",			"K13"		},
	{	174,	"",			"K14"		},
	{	175,	"",			"K15"		},
	{	176,	"",			"K16"		},
	{	177,	"",			"L1"		},
	{	178,	"",			"L2"		},
	{	179,	"",			"L3"		},
	{	180,	"",			"L4"		},
	{	181,	"",			"L5"		},
	{	182,	"",			"L6"		},
	{	183,	"",			"L7"		},
	{	184,	"",			"L8"		},
	{	185,	"",			"L9"		},
	{	186,	"",			"L10"		},
	{	187,	"",			"L11"		},
	{	188,	"",			"L12"		},
	{	189,	"",			"L13"		},
	{	190,	"",			"L14"		},
	{	191,	"",			"L15"		},
	{	192,	"",			"L16"		},
	{	193,	"",			"M1"		},
	{	194,	"",			"M2"		},
	{	195,	"",			"M3"		},
	{	196,	"",			"M4"		},
	{	197,	"",			"M5"		},
	{	198,	"",			"M6"		},
	{	199,	"",			"M7"		},
	{	200,	"",			"M8"		},
	{	201,	"",			"M9"		},
	{	202,	"",			"M10"		},
	{	203,	"",			"M11"		},
	{	204,	"",			"M12"		},
	{	205,	"",			"M13"		},
	{	206,	"",			"M14"		},
	{	207,	"",			"M15"		},
	{	208,	"",			"M16"		},
	{	209,	"",			"N1"		},
	{	210,	"",			"N2"		},
	{	211,	"",			"N3"		},
	{	212,	"",			"N4"		},
	{	213,	"",			"N5"		},
	{	214,	"",			"N6"		},
	{	215,	"",			"N7"		},
	{	216,	"",			"N8"		},
	{	217,	"",			"N9"		},
	{	218,	"",			"N10"		},
	{	219,	"",			"N11"		},
	{	220,	"",			"N12"		},
	{	221,	"",			"N13"		},
	{	222,	"",			"N14"		},
	{	223,	"",			"N15"		},
	{	224,	"",			"N16"		},
	{	225,	"",			"O1"		},
	{	226,	"",			"O2"		},
	{	227,	"",			"O3"		},
	{	228,	"",			"O4"		},
	{	229,	"",			"O5"		},
	{	230,	"",			"O6"		},
	{	231,	"",			"O7"		},
	{	232,	"",			"O8"		},
	{	233,	"",			"O9"		},
	{	234,	"",			"O10"		},
	{	235,	"",			"O11"		},
	{	236,	"",			"O12"		},
	{	237,	"",			"O13"		},
	{	238,	"",			"O14"		},
	{	239,	"",			"O15"		},
	{	240,	"",			"O16"		},
	{	241,	"",			"P1"		},
	{	242,	"",			"P2"		},
	{	243,	"",			"P3"		},
	{	244,	"",			"P4"		},
	{	245,	"",			"P5"		},
	{	246,	"",			"P6"		},
	{	247,	"",			"P7"		},
	{	248,	"",			"P8"		},
	{	249,	"",			"P9"		},
	{	250,	"",			"P10"		},
	{	123,	"",			"P11"		},
	{	124,	"",			"P12"		},
	{	125,	"",			"P13"		},
	{	126,	"",			"P14"		},
	{	127,	"",			"P15"		},
	{	128,	"",			"P16"		},

	};

//////////////////////////////////////////////////////////////////////////
//
// Parameter Definitions (P)
//

enum {
	paramNone,
	paramFlag,
	paramByte,
	paramWord,
	paramLong,
	paramReal,
	paramString,
	paramTLP,
	paramDouble,
	};

struct PARAMETER {

	TLP	m_Param;
	UINT	m_Type;
	BOOL	m_fReadOnly;
	};

struct PARAMETERS {

	UINT	  m_PointType;
	PARAMETER m_Param[255];
	};

// NOTE :  Disabled double parameters were not available in UUT.

static PARAMETERS const m_pParameters[] = {

	0,	// Configurable Opcodes
	{
		{	{	0,	"0-Sequence/Revision Number",			"SEQREV"	},	paramReal,	0},
		{	{	1,	"1-Data #1",					"DATA1"		},	paramTLP,	0},
		{	{	2,	"2-Data #2",					"DATA2"		},	paramTLP,	0},
		{	{	3,	"3-Data #3",					"DATA3"		},	paramTLP,	0},
		{	{	4,	"4-Data #4",					"DATA4"		},	paramTLP,	0},
		{	{	5,	"5-Data #5",					"DATA5"		},	paramTLP,	0},
		{	{	6,	"6-Data #6",					"DATA6"		},	paramTLP,	0},
		{	{	7,	"7-Data #7",					"DATA7"		},	paramTLP,	0},
		{	{	8,	"8-Data #8",					"DATA8"		},	paramTLP,	0},
		{	{	9,	"9-Data #9",					"DATA9"		},	paramTLP,	0},
		{	{	10,	"10-Data #10",					"DATA10"	},	paramTLP,	0},
		{	{	11,	"11-Data #11",					"DATA11"	},	paramTLP,	0},
		{	{	12,	"12-Data #12",					"DATA12"	},	paramTLP,	0},
		{	{	13,	"13-Data #13",					"DATA13"	},	paramTLP,	0},
		{	{	14,	"14-Data #14",					"DATA14"	},	paramTLP,	0},
		{	{	15,	"15-Data #15",					"DATA15"	},	paramTLP,	0},
		{	{	16,	"16-Data #16",					"DATA16"	},	paramTLP,	0},
		{	{	17,	"17-Data #17",					"DATA17"	},	paramTLP,	0},
		{	{	18,	"18-Data #18",					"DATA18"	},	paramTLP,	0},
		{	{	19,	"19-Data #19",					"DATA19"	},	paramTLP,	0},
		{	{	20,	"20-Data #20",					"DATA20"	},	paramTLP,	0},
		{	{	21,	"21-Data #21",					"DATA21"	},	paramTLP,	0},
		{	{	22,	"22-Data #22",					"DATA22"	},	paramTLP,	0},
		{	{	23,	"23-Data #23",					"DATA23"	},	paramTLP,	0},
		{	{	24,	"24-Data #24",					"DATA24"	},	paramTLP,	0},
		{	{	25,	"25-Data #25",					"DATA25"	},	paramTLP,	0},
		{	{	26,	"26-Data #26",					"DATA26"	},	paramTLP,	0},
		{	{	27,	"27-Data #27",					"DATA27"	},	paramTLP,	0},
		{	{	28,	"28-Data #28",					"DATA28"	},	paramTLP,	0},
		{	{	29,	"29-Data #29",					"DATA29"	},	paramTLP,	0},
		{	{	30,	"30-Data #30",					"DATA30"	},	paramTLP,	0},
		{	{	31,	"31-Data #31",					"DATA31"	},	paramTLP,	0},
		{	{	32,	"32-Data #32",					"DATA32"	},	paramTLP,	0},
		{	{	33,	"33-Data #33",					"DATA33"	},	paramTLP,	0},
		{	{	34,	"34-Data #34",					"DATA34"	},	paramTLP,	0},
		{	{	35,	"35-Data #35",					"DATA35"	},	paramTLP,	0},
		{	{	36,	"36-Data #36",					"DATA36"	},	paramTLP,	0},
		{	{	37,	"37-Data #37",					"DATA37"	},	paramTLP,	0},
		{	{	38,	"38-Data #38",					"DATA38"	},	paramTLP,	0},
		{	{	39,	"39-Data #39",					"DATA39"	},	paramTLP,	0},
		{	{	40,	"40-Data #40",					"DATA40"	},	paramTLP,	0},
		{	{	41,	"41-Data #41",					"DATA41"	},	paramTLP,	0},
		{	{	42,	"42-Data #42",					"DATA42"	},	paramTLP,	0},
		{	{	43,	"43-Data #43",					"DATA43"	},	paramTLP,	0},
		{	{	44,	"44-Data #44",					"DATA44"	},	paramTLP,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	1,	// Discrete Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Filter",			"FILTER"	},	paramByte,	0},	
		{	{	2,	"2-Status",			"STATUS"	},	paramByte,	0},
		{	{	3,	"3-Mode",			"MODE"		},	paramByte,	0},
		{	{	4,	"4-Alarm Code",			"ALARM"		},	paramByte,	1},
		{	{	5,	"5-Accumulated Value",		"ACCUM"		},	paramLong,	0},
		{	{	6,	"6-On Counter",			"ONCTR"		},	paramLong,	0},	
		{	{	7,	"7-Off Counter",		"OFFCTR"	},	paramLong,	0},
		{	{	8,	"8-0% Pulse Width",		"MINCNT"	},	paramWord,	0},
		{	{	9,	"9-100% Pulse Width",		"MAXCNT"	},	paramWord,	0},
		{	{	10,	"10-Max Time / Max Count",	"SAMTIM"	},	paramWord,	0},
		{	{	11,	"11-Units",			"UNITS"		},	paramString,	0},
		{	{	12,	"12-Scan Period",		"SCANPR"	},	paramWord,	0},
		{	{	13,	"13-Low Reading EU",		"MINEU"		},	paramReal,	0},	
		{	{	14,	"14-High Reading EU",		"MAXEU"		},	paramReal,	0},
		{	{	15,	"15-Low Alarm EU",		"LOAL"		},	paramReal,	0},
		{	{	16,	"16-High Alarm EU",		"HIAL"		},	paramReal,	0},
		{	{	17,     "17-Low Low Alarm EU",		"LOLOAL"	},	paramReal,	0},
		{	{	18,	"18-High High Alarm EU",	"HIHIAL"	},	paramReal,	0},
		{	{	19,	"19-Rate Alarm EU",		"RATEAL"	},	paramReal,	0},	
		{	{	20,	"20-Alarm Deadband",		"ALDBND"	},	paramReal,	0},
		{	{	21,	"21-EU Value",			"EU"		},	paramReal,	0},
		{	{	22,	"22-TDI Value",			"TDICNT"	},	paramWord,	1},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	2,	// Discrete Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Time On",			"TIMEON"	},	paramWord,	0},
		{	{	2,	""				""		},	paramNone,	0},
		{	{	3,	"3-Status",			"STATUS"	},	paramByte,	0},
		{	{	4,	"4-Mode",			"MODE"		},	paramByte,	0},
		{	{	5,	"5-Alarm Code",			"ALARM"		},	paramByte,	1},
		{	{	6,	"6-Accumulated Value",		"ACCUM"		},	paramLong,	0},
		{	{	7,	"7-Units",			"UNITS"		},	paramString,	0},
		{	{	8,	"8-Cycle Time",			"CYCTIM"	},	paramWord,	0},
		{	{	9,	"9-0% Count",			"MINCNT"	},	paramWord,	0},
		{	{	10,	"10-100% Count",		"MAXCNT"	},	paramWord,	0},
		{	{	11,	"11-Low Reading EU",		"MINEU"		},	paramReal,	0},
		{	{	12,	"12-High Reading EU",		"MAXEU"		},	paramReal,	0},
		{	{	13,	"13-EU Value",			"EU"		},	paramReal,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	3,	// Analog Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Units",			"UNITS"		},	paramString,	0},	
		{	{	2,	"2-Scan Period",		"SCANPR"	},	paramWord,	0},
		{	{	3,	"3-Filter",			"FILTER"	},	paramWord,	0},
		{	{	4,	"4-Adjusted A/D 0%",		"MINRAW"	},	paramWord,	0},
		{	{	5,	"5-Adjusted A/D 100%",		"MAXRAW"	},	paramWord,	0},
		{	{	6,	"6-Low Reading EU",		"MINEU"		},	paramReal,	0},	
		{	{	7,	"7-High Reading EU",		"MAXEU"		},	paramReal,	0},
		{	{	8,	"8-Low Alarm EU",		"LOAL"		},	paramReal,	0},
		{	{	9,	"9-High Alarm EU",		"HIAL"		},	paramReal,	0},
		{	{	10,     "10-Low Low Alarm EU",		"LOLOAL"	},	paramReal,	0},
		{	{	11,	"11-High High Alarm EU",	"HIHIAL"	},	paramReal,	0},
		{	{	12,	"12-Rate Alarm EU",		"RATEAL"	},	paramReal,	0},	
		{	{	13,	"13-Alarm Deadband",		"ALDBND"	},	paramReal,	0},
		{	{	14,	"14-Filtered EU Value",		"EU"		},	paramReal,	0},
		{	{	15,	"15-Mode",			"MODE"		},	paramByte,	0},
		{	{	16,	"16-Alarm Code",		"ALARM"		},	paramByte,	1},
		{	{	17,	"17-Raw A/D Input",		"CURRAW"	},	paramWord,	1},
		{	{	18,	"18-Actual Scan Time",		"SCAN"		},	paramWord,	1},
		{	{	19,	"19-Fault Value",		"FAULTVAL"	},	paramReal,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	4,	// Analog Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Units",			"UNITS"		},	paramString,	0},	
		{	{	2,	"2-Adjusted A/D 0%",		"MINRAW"	},	paramWord,	0},
		{	{	3,	"3-Adjusted A/D 100%",		"MAXRAW"	},	paramWord,	0},
		{	{	4,	"4-Low Reading EU",		"MINEU"		},	paramReal,	0},	
		{	{	5,	"5-High Reading EU",		"MAXEU"		},	paramReal,	0},
		{	{	6,	"6-EU Value",			"EU"		},	paramReal,	0},
		{	{	7,	"7-Mode",			"MODE"		},	paramByte,	0},
		{	{	8,	"8-Alarm Code",			"ALARM"		},	paramByte,	1},
		{	{	9,	"9-Raw A/D Input",		"CURRAW"	},	paramWord,	1},
		{	{	0,	"",				""		},	paramNone,	0},
		},
	5,	// Pulse Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Units",			"UNITS"		},	paramString,	0},	
		{	{	2,	"2-Rate Flag",			"RATEFL"	},	paramByte,	0},
		{	{	3,	"3-Rate Period",		"RATEPR"	},	paramByte,	0},
		{	{	4,	"4-Filter Time",		"FILTER"	},	paramByte,	0},
		{	{	5,	"5-Scan Period",		"SCANPR"	},	paramWord,	0},
		{	{	6,	"6-Conversion",			"CONV"		},	paramReal,	0},	
		{	{	7,	"7-Low Alarm EU",		"LOAL"		},	paramReal,	0},
		{	{	8,	"8-High Alarm EU",		"HIAL"		},	paramReal,	0},
		{	{	9,	"9-Low Low Alarm EU",		"LOLOAL"	},	paramReal,	0},
		{	{	10,     "10-High High Alarm EU",	"HIHIAL"	},	paramReal,	0},
		{	{	11,	"11-Rate Alarm EU",		"RATEAL"	},	paramReal,	0},
		{	{	12,	"12-Alarm Deadband",		"ALDBND"	},	paramReal,	0},	
		{	{	13,	"13-EU Value",			"EU"		},	paramReal,	0},
		{	{	14,	"14-Mode",			"MODE"		},	paramByte,	0},
		{	{	15,	"15-Alarm Code",		"ALARM"		},	paramByte,	1},
		{	{	16,	"16-Accumulated Value",		"ACCUM"		},	paramLong,	0},
		{	{	17,	"17-Current Rate",		"RATE"		},	paramReal,	1},
		{	{	18,	"18-Today's Total",		"TDYTOT"	},	paramReal,	0},
		{	{	19,	"19-Yesterday's Total",		"YDYTOT"	},	paramReal,	1},
		{	{	20,	"20-Pulses for Day",		"TDYRAW"	},	paramLong,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	7,	// AGA Flow Parameters
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	paramString,	0},
		{	{	1,	"1-Latitude",					"LAT"		},	paramReal,	0},	
		{	{	2,	"2-Elevation",					"ELAVTN"	},	paramReal,	0},
		{	{	3,	"3-Calculation Method",				"METHOD"	},	paramByte,	0},
		{	{	4,	"4-AGA Configuration",				"OPTION"	},	paramByte,	0},
		{	{	5,	"5-Specific Gravity",				"SPGR"		},	paramReal,	0},
		{	{	6,	"6-Heating Value",				"GASHV"		},	paramReal,	0},	
		{	{	7,	"7-Gravity Acceleration Correction",		"GRAVIT"	},	paramReal,	0},
		{	{	8,	"8-Scan Period",				"SCANPR"	},	paramWord,	1},
		{	{	9,	"9-Pipe Diameter",				"PIPDIA"	},	paramReal,	0},
		{	{	10,     "10-Orifice Diameter",				"ORFDIA"	},	paramReal,	0},
		{	{	11,	"11-Orifice Reference Temperature",		"TMEAS"		},	paramReal,	0},
		{	{	12,	"12-Orifice Material",				"OR_MAT"	},	paramByte,	0},	
		{	{	13,	"13-Point Description",				"DESC"		},	paramString,	0},
		{	{	14,	"14-Alarm Code",				"ALARM"		},	paramByte,	1},
		{	{	15,	"15-Low Alarm",					"LOAL"		},	paramReal,	0},
		{	{	16,	"16-High Alarm",				"HIAL"		},	paramReal,	0},
		{	{	17,	"17-Viscosity",					"VISCOS"	},	paramReal,	0},
		{	{	18,	"18-Specific Heat Ratio",			"SPHTRA"	},	paramReal,	0},
		{	{	19,	"19-Contract or Base Pressure",			"BASEPR"	},	paramReal,	0},
		{	{	20,	"20-Contract or Base Temperature",		"BASETP"	},	paramReal,	0},
		{	{	21,	"21-Low DB Cutoff / Meter Factor",		"MINDP"		},	paramReal,	0},
		{	{	22,	"22-User Correction Factor",			"FPWL"		},	paramReal,	0},	
		{	{	23,	"23-N2 - Nitrogen",				"NITROG"	},	paramReal,	0},
		{	{	24,	"24-CO2 - Carbon Dioxide",			"CARBDI"	},	paramReal,	0},
		{	{	25,	"25-H2S - Hydrogen Sulfide",			"HYDSUL"	},	paramReal,	0},
		{	{	26,	"26-H2O - Water",				"WATER"		},	paramReal,	0},
		{	{	27,	"27-He - Helium",				"HELIUM"	},	paramReal,	0},
		{	{	28,	"28-CH4 - Methane",				"METHANE"	},	paramReal,	0},
		{	{	29,	"29-C2HS - Ethane",				"ETHANE"	},	paramReal,	0},
		{	{	30,	"30-C3H8 - Propane",				"PROPAN"	},	paramReal,	0},
		{	{	31,	"31-C4H10 - n-Butane",				"NBUTAN"	},	paramReal,	0},
		{	{	32,	"32-C4H10 - i-Butane",				"IBUTAN"	},	paramReal,	0},	
		{	{	33,	"33-C5H12 - n-Pentane",				"NPENTA"	},	paramReal,	0},
		{	{	34,	"34-C5H12 - i-Pentane",				"IPENTA"	},	paramReal,	0},
		{	{	35,	"35-C6H14 - n-Hexane",				"NHEXAN"	},	paramReal,	0},
		{	{	36,	"36-C7H16 - n-Heptane",				"NHEPTA"	},	paramReal,	0},
		{	{	37,	"37-C8H18 - n-Octane",				"NOCTAN"	},	paramReal,	0},
		{	{	38,	"38-C9H20 - n-Nonane",				"NNONAN"	},	paramReal,	0},
		{	{	39,	"39-C10H22 - n_Decane",				"NDECAN"	},	paramReal,	0},
		{	{	40,	"40-O2 - Oxygen",				"OXYGEN"	},	paramReal,	0},
		{	{	41,	"41-CO - Carbon Monoxide",			"CARBMO"	},	paramReal,	0},
		{	{	42,	"42-H2 - Hydrogen",				"HYDROG"	},	paramReal,	0},	
		{	{	43,	"43-Flow Units",				"FLOUNITS"	},	paramByte,	0},
		{	{	44,	"44-Stacked Differential Pressure",		"DPSTEN"	},	paramByte,	0},
		{	{	45,	"45-Low Differential Pressure Input",		"LO_TYP"	},	paramTLP,	0},
		{	{	46,	"46-Meter Input",				"DP_TYP"	},	paramTLP,	0},
		{	{	47,	"47-Static Pressure Input",			"FP_TYP"	},	paramTLP,	0},
		{	{	48,	"48-Temperature Input",				"TP_TYP"	},	paramTLP,	0},
		{	{	49,	"49-Low Differential Pressure Setpoint",	"LODPSP"	},	paramReal,	0},
		{	{	50,	"50-High Differential Pressure Setpoint",	"HIDPSP"	},	paramReal,	0},
		{	{	51,	"51-Meter Value",				"CURDP"		},	paramReal,	0},
		{	{	52,	"52-Static Pressure",				"CURFP"		},	paramReal,	0},	
		{	{	53,	"53-Temperature",				"CURTMP"	},	paramReal,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	8,	// History Parameters
	{
		{	{	0,	"0-Point Tag Identification TLP #1",	"TAG#1"		},	paramTLP,	1},
		{	{	1,	"1-History Log Point #1",		"HST#1"		},	paramTLP,	1},	
		{	{	2,	"2-Archive Type #1",			"ARCH#1"	},	paramByte,	1},
		{	{	3,	"3-Averaging or Rate Type #1",		"AVG#1"		},	paramByte,	1},
		
		{	{	4,	"4-Point Tag Identification TLP #2",	"TAG#2"		},	paramTLP,	1},
		{	{	5,	"5-History Log Point #2",		"HST#2"		},	paramTLP,	1},	
		{	{	6,	"6-Archive Type #2",			"ARCH#2"	},	paramByte,	1},
		{	{	7,	"7-Averaging or Rate Type #2",		"AVG#2"		},	paramByte,	1},

		{	{	8,	"8-Point Tag Identification TLP #3",	"TAG#3"		},	paramTLP,	1},
		{	{	9,	"9-History Log Point #3",		"HST#3"		},	paramTLP,	1},	
		{	{	10,	"10-Archive Type #3",			"ARCH#3"	},	paramByte,	1},
		{	{	11,	"11-Averaging or Rate Type #3",		"AVG#3"		},	paramByte,	1},

		{	{	12,	"12-Point Tag Identification TLP #4",	"TAG#4"		},	paramTLP,	1},
		{	{	13,	"13-History Log Point #4",		"HST#4"		},	paramTLP,	1},	
		{	{	14,	"14-Archive Type #4",			"ARCH#4"	},	paramByte,	1},
		{	{	15,	"15-Averaging or Rate Type #4",		"AVG#4"		},	paramByte,	1},

		{	{	16,	"16-Point Tag Identification TLP #5",	"TAG#5"		},	paramTLP,	1},
		{	{	17,	"17-History Log Point #5",		"HST#5"		},	paramTLP,	1},	
		{	{	18,	"18-Archive Type #5",			"ARCH#5"	},	paramByte,	1},
		{	{	19,	"19-Averaging or Rate Type #5",		"AVG#5"		},	paramByte,	1},

		{	{	20,	"20-Point Tag Identification TLP #6",	"TAG#6"		},	paramTLP,	1},
		{	{	21,	"21-History Log Point #6",		"HST#6"		},	paramTLP,	1},	
		{	{	22,	"22-Archive Type #6",			"ARCH#6"	},	paramByte,	1},
		{	{	23,	"23-Averaging or Rate Type #6",		"AVG#6"		},	paramByte,	1},

		{	{	24,	"24-Point Tag Identification TLP #7",	"TAG#7"		},	paramTLP,	1},
		{	{	25,	"25-History Log Point #7",		"HST#7"		},	paramTLP,	1},	
		{	{	26,	"26-Archive Type #7",			"ARCH#7"	},	paramByte,	1},
		{	{	27,	"27-Averaging or Rate Type #7",		"AVG#7"		},	paramByte,	1},

		{	{	28,	"28-Point Tag Identification TLP #8",	"TAG#8"		},	paramTLP,	1},
		{	{	29,	"29-History Log Point #8",		"HST#8"		},	paramTLP,	1},	
		{	{	30,	"30-Archive Type #8",			"ARCH#8"	},	paramByte,	1},
		{	{	31,	"31-Averaging or Rate Type #8",		"AVG#8"		},	paramByte,	1},

		{	{	32,	"32-Point Tag Identification TLP #9",	"TAG#9"		},	paramTLP,	1},
		{	{	33,	"33-History Log Point #9",		"HST#9"		},	paramTLP,	0},	
		{	{	34,	"34-Archive Type #9",			"ARCH#9"	},	paramByte,	0},
		{	{	35,	"35-Averaging or Rate Type #9",		"AVG#9"		},	paramByte,	0},

		{	{	36,	"36-Point Tag Identification TLP #10",	"TAG#10"	},	paramTLP,	1},
		{	{	37,	"37-History Log Point #10",		"HST#10"	},	paramTLP,	0},	
		{	{	38,	"38-Archive Type #10",			"ARCH#10"	},	paramByte,	0},
		{	{	39,	"39-Averaging or Rate Type #10",	"AVG#10"	},	paramByte,	0},

		{	{	40,	"40-Point Tag Identification TLP #11",	"TAG#11"	},	paramTLP,	1},
		{	{	41,	"41-History Log Point #11",		"HST#11"	},	paramTLP,	0},	
		{	{	42,	"42-Archive Type #11",			"ARCH#11"	},	paramByte,	0},
		{	{	43,	"43-Averaging or Rate Type #11",	"AVG#11"	},	paramByte,	0},

		{	{	44,	"44-Point Tag Identification TLP #12",	"TAG#12"	},	paramTLP,	1},
		{	{	45,	"45-History Log Point #12",		"HST#12"	},	paramTLP,	0},	
		{	{	46,	"46-Archive Type #12",			"ARCH#12"	},	paramByte,	0},
		{	{	47,	"47-Averaging or Rate Type #12",	"AVG#12"	},	paramByte,	0},

		{	{	48,	"48-Point Tag Identification TLP #13",	"TAG#13"	},	paramTLP,	1},
		{	{	49,	"49-History Log Point #13",		"HST#13"	},	paramTLP,	0},	
		{	{	50,	"50-Archive Type #13",			"ARCH#13"	},	paramByte,	0},
		{	{	51,	"51-Averaging or Rate Type #13",	"AVG#13"	},	paramByte,	0},

		{	{	52,	"52-Point Tag Identification TLP #14",	"TAG#14"	},	paramTLP,	1},
		{	{	53,	"53-History Log Point #14",		"HST#14"	},	paramTLP,	0},	
		{	{	54,	"54-Archive Type #14",			"ARCH#14"	},	paramByte,	0},
		{	{	55,	"55-Averaging or Rate Type #14",	"AVG#14"	},	paramByte,	0},

		{	{	56,	"56-Point Tag Identification TLP #15",	"TAG#15"	},	paramTLP,	1},
		{	{	57,	"57-History Log Point #15",		"HST#15"	},	paramTLP,	0},	
		{	{	58,	"58-Archive Type #15",			"ARCH#15"	},	paramByte,	0},
		{	{	59,	"59-Averaging or Rate Type #15",	"AVG#15"	},	paramByte,	0},
		{	{	 0,	"",					""		},	paramNone,	0},
		},
	10,	// AGA Flow Calculation Values
	{
		{	{	0,	"0-Differential Pressure / Uncorrected Flow",	"CURDP"		},	paramReal,	1},
		{	{	1,	"1-Static Pressure",				"CURFP"		},	paramReal,	1},	
		{	{	2,	"2-Temperature",				"CURTMP"	},	paramReal,	1},
		{	{	3,	"3-Instantaneous Flow",				"CURFLO"	},	paramReal,	1},
		{	{	4,	"4-Instantaneous Energy",			"ENERGY"	},	paramReal,	1},
		{	{	5,	"5-Flow Today",					"TDYFLO"	},	paramReal,	1},
		{	{	6,	"6-Energy Today",				"TDYENG"	},	paramReal,	1},	
		{	{	7,	"7-Flow Yesterday",				"YDYFLO"	},	paramReal,	1},
		{	{	8,	"8-Energy Yesterday",				"YDYENG"	},	paramReal,	1},
		{	{	9,	"9-hwPf / Uncorrected Flow Rate",		"HWPF"		},	paramReal,	1},
		{	{	10,     "10-IMV / BMV",					"IMV/BMV"	},	paramReal,	1},
		{	{	11,	"11-Sample Time",				"SAMPLE"	},	paramReal,	1},
		{	{	12,	"12-Y / Fpm",					"EXPFTR"	},	paramReal,	1},	
		{	{	13,	"13-Reynolds Number",				"FR"		},	paramReal,	1},
		{	{	14,	"14-Ftf / Ftm",					"FTF"		},	paramReal,	1},
		{	{	15,	"15-Fpv / S",					"FPV"		},	paramReal,	1},
		{	{	16,	"16-Fgr",					"FGR"		},	paramReal,	1},
		{	{	17,	"17-Cd / Ftm",					"FB"		},	paramReal,	1},
		{	{	18,	"18-Fpb",					"FPB"		},	paramReal,	1},
		{	{	19,	"19-Ftb",					"FTB"		},	paramReal,	1},
		{	{	20,	"20-Ev",					"FA"		},	paramReal,	1},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	12,	// Clock
	{
		{	{	0,	"0-Seconds",					"SECOND"	},	paramByte,	0},
		{	{	1,	"1-Minutes",					"MINUTE"	},	paramByte,	0},	
		{	{	2,	"2-Hours",					"HOUR"		},	paramByte,	0},
		{	{	3,	"3-Day",					"DAY"		},	paramByte,	0},
		{	{	4,	"4-Month",					"MONTH"		},	paramByte,	0},
		{	{	5,	"5-Year",					"YEAR"		},	paramByte,	0},
		{	{	6,	"6-Leap Year",					"LEAPYR"	},	paramByte,	1},	
		{	{	7,	"7-Day of Week",				"DAYOWK"	},	paramByte,	1},
		{	{	8,	"8-Time",					"TIME"		},	paramLong,	0},
		{	{	9,	"9-Century",					"CENT"		},	paramByte,	0},
		{	{	10,     "10-Daylight Savings Enable",			"DLSTEN"	},	paramByte,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	13,	// Flags
	{
		{	{	0,	"0-CRC Check",					"CRCCHK"	},	paramByte,	0},
		{	{	1,	"1-Power Mode",					"PWRMODE"	},	paramByte,	0},	
		{	{	2,	"2-User Calc 2 Enable",				"CALC2EN"	},	paramByte,	0},
		{	{	3,	"3-User Operator Port Enable",			"USRLOI"	},	paramByte,	0},
		{	{	4,	"4-FST/Display Clear",				"FSTDSP"	},	paramByte,	0},
		{	{	5,	"5-User Com1 Enable",				"USRCOM1"	},	paramByte,	0},
		{	{	6,	"6-User Com2 Enable",				"USRCOM2"	},	paramByte,	0},	
		{	{	7,	"7-User Calc 1 Enable",				"USRCALC1"	},	paramByte,	0},
		{	{	8,	"8-RTS LOI Port",				"RTSLOI"	},	paramByte,	0},
		{	{	9,	"9-RTS Comm Port 1",				"RTSCOM1"	},	paramByte,	0},
		{	{	10,     "10-RTS Comm Port 2",				"RTSCOM2"	},	paramByte,	0},
		{	{	11,	"11-Clear Config Memory",			"CLREEP"	},	paramByte,	0},	
		{	{	12,	"12-I/O Scan Enable",				"IOSCAN"	},	paramByte,	0},
		{	{	13,	"13-Auxilary Output 2 On",			"AUX2ON"	},	paramByte,	0},
		{	{	14,	"14-Auxilary Output 1 On",			"AUX1ON"	},	paramByte,	0},
		{	{	15,	"15-Cold Start",				"COLDST"	},	paramByte,	0},
		{	{	16,	"16-Warm Start",				"WARMST"	},	paramByte,	0},	
		{	{	17,	"17-Read I/O",					"READIIO"	},	paramByte,	0},
		{	{	18,	"18-Write to Config Memory",			"WRITE"		},	paramByte,	0},
		{	{	19,	"19-Config Memory Write Complete",		"COMPLT"	},	paramByte,	0},
		{	{	20,     "20-Event Log Flag",				"EVTFLAG"	},	paramByte,	0},
		{	{	21,	"21-LOI Port Security",				"LOISEC"	},	paramByte,	0},	
		{	{	22,	"22-Comm Port 1 Security",			"COM1SEC"	},	paramByte,	0},
		{	{	23,	"23-Comm Port 2 Security",			"COM2SEC"	},	paramByte,	0},
		{	{	24,	"24-Termination Type Installed",		"TERMTYPE"	},	paramByte,	0},
		{	{	25,	"25-Comm Port Pass Through Mode",		"PASSTHRU"	},	paramByte,	0},
		{	{	26,	"26-Flag 26",					"FLAG26"	},	paramByte,	0},	
		{	{	27,	"27-Flag 27",					"FLAG27"	},	paramByte,	0},
		{	{	28,	"28-Flag 28",					"FLAG28"	},	paramByte,	0},
		{	{	29,	"29-Flag 29",					"FLAG29"	},	paramByte,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	15,	// System Variables
	{
		{	{	0,	"0-Device Address",				"ROCADR"	},	paramByte,	0},
		{	{	1,	"1-Device Group",				"ROCGRP"	},	paramByte,	0},	
		{	{	2,	"2-Station Name",				"STNNAME"	},	paramString,	0},
		{	{	3,	"3-Active PIDs",				"#PIDS"		},	paramByte,	0},
		{	{	4,	"4-Active Meter Runs",				"#AGAS"		},	paramByte,	0},
		{	{	5,	"5-FST Instructions Per Cycle",			"FSTINST"	},	paramByte,	0},
		{	{	6,	"6-Base Database Points - History 1",		"#RAM0"		},	paramByte,	0},	
		{	{	7,	"7-RAM1 Database Points - History 2",		"#RAM1"		},	paramByte,	0},
		{	{	8,	"8-RAM2 Database Points - History 3",		"#RAM2"		},	paramByte,	0},
		{	{	9,	"9-Force End of Day",				"FORCE"		},	paramByte,	0},
		{	{	10,     "10-Contract Hour",				"CONTRC"	},	paramByte,	0},
		{	{	11,	"11-Version Name - Part Number",		"VERSION"	},	paramString,	1},	
		{	{	12,	"12-Manufacturing ID",				"VENDORID"	},	paramString,	1},
		{	{	13,	"13-Time Created",				"CREATETM"	},	paramString,	1},
		{	{	14,	"14-ROM Serial Number",				"ROMSN"		},	paramString,	1},
		{	{	15,	"15-Customer Name",				"CUSTNAME"	},	paramString,	1},
		{	{	16,	"16-Maximum PIDs",				"MAXPIDS"	},	paramByte,	1},	
		{	{	17,	"17-Maximum Meter Runs",			"MAXAGAS"	},	paramByte,	1},
		{	{	18,	"18-Maximum Tanks",				"MAXTANKS"	},	paramByte,	1},
		{	{	19,	"19-FSTs Possible",				"MAXFSTS"	},	paramByte,	1},
		{	{	20,     "20-RAM Installed",				"RAM"		},	paramByte,	1},
		{	{	21,	"21-ROM Installed",				"ROM"		},	paramByte,	1},	
		{	{	22,	"22-MPU Loading",				"MPU"		},	paramReal,	1},
		{	{	23,	"23-Utilities",					"UTIL"		},	paramByte,	1},
		{	{	24,	"24-Device Type",				"ROCTYPE"	},	paramWord,	1},
		{	{	25,	"25-Units Flag",				"UNITS"		},	paramByte,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	16,	// FST Registers
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	paramString,	0},
		{	{	1,	"1-Result Register",				"RR"		},	paramReal,	0},	
		{	{	2,	"2-Register #1",				"R1"		},	paramReal,	0},
		{	{	3,	"3-Register #2",				"R2"		},	paramReal,	0},
		{	{	4,	"4-Register #3",				"R3"		},	paramReal,	0},
		{	{	5,	"5-Register #4",				"R4"		},	paramReal,	0},
		{	{	6,	"6-Register #5",				"R5"		},	paramReal,	0},	
		{	{	7,	"7-Register #6",				"R6"		},	paramReal,	0},
		{	{	8,	"8-Register #7",				"R7"		},	paramReal,	0},
		{	{	9,	"9-Register #8",				"R8"		},	paramReal,	0},
		{	{	10,     "10-Register #9",				"R9"		},	paramReal,	0},
		{	{	11,	"11-Register #10",				"R10"		},	paramReal,	0},	
		{	{	12,	"12-Timer #1",					"TMR1"		},	paramLong,	0},
		{	{	13,	"13-Timer #2",					"TMR2"		},	paramLong,	0},
		{	{	14,	"14-Timer #3",					"TMR3"		},	paramLong,	0},
		{	{	15,	"15-Timer #4",					"TMR4"		},	paramLong,	0},
		{	{	16,	"16-Message #1",				"MSG1"		},	paramString,	0},	
		{	{	17,	"17-Message #2",				"MSG2"		},	paramString,	0},	
		{	{	18,	"18-Message Data",				"MSGDATA"	},	paramString,	1},
		{	{	19,	"19-Miscellaneous 1",				"MISC1"		},	paramByte,	0},
		{	{	20,     "20-Miscellaneous 2",				"MISC2"		},	paramByte,	0},
		{	{	21,	"21-Miscellaneous 3",				"MISC3"		},	paramByte,	0},	
		{	{	22,	"22-Miscellaneous 4",				"MISC4"		},	paramByte,	0},
		{	{	23,	"23-Compare Flag - SVD",			"CMPFLG"	},	paramByte,	0},
		{	{	24,	"24-Run Flag",					"RUNFLG"	},	paramByte,	0},
		{	{	25,	"25-Code Size",					"CODESIZE"	},	paramWord,	1},
		{	{	26,	"26-Instruction Pointer",			"IP"		},	paramWord,	0},
		{	{	27,	"27-Execution Delay",				"BREAK"		},	paramWord,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	17,	// Soft Point Parameters
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	paramString,	0},
		{	{	1,	"1-Integer Flag",				"INT1"		},	paramWord,	0},
		{	{	2,	"2-Data #1",					"DATA1"		},	paramReal,	0},
		{	{	3,	"3-Data #2",					"DATA2"		},	paramReal,	0},
		{	{	4,	"4-Data #3",					"DATA3"		},	paramReal,	0},
		{	{	5,	"5-Data #4",					"DATA4"		},	paramReal,	0},
		{	{	6,	"6-Data #5",					"DATA5"		},	paramReal,	0},
		{	{	7,	"7-Data #6",					"DATA6"		},	paramReal,	0},
		{	{	8,	"8-Data #7",					"DATA7"		},	paramReal,	0},
		{	{	9,	"9-Data #8",					"DATA8"		},	paramReal,	0},
		{	{	10,	"10-Data #9",					"DATA9"		},	paramReal,	0},
		{	{	11,	"11-Data #10",					"DATA10"	},	paramReal,	0},
		{	{	12,	"12-Data #11",					"DATA11"	},	paramReal,	0},
		{	{	13,	"13-Data #12",					"DATA12"	},	paramReal,	0},
		{	{	14,	"14-Data #13",					"DATA13"	},	paramReal,	0},
		{	{	15,	"15-Data #14",					"DATA14"	},	paramReal,	0},
		{	{	16,	"16-Data #15",					"DATA15"	},	paramReal,	0},
		{	{	17,	"17-Data #16",					"DATA16"	},	paramReal,	0},
		{	{	18,	"18-Data #17",					"DATA17"	},	paramReal,	0},
		{	{	19,	"19-Data #18",					"DATA18"	},	paramReal,	0},
		{	{	20,	"20-Data #19",					"DATA19"	},	paramReal,	0},
		{	{	21,	"21-Data #20",					"DATA20"	},	paramReal,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	19,	// Database Parameters
	{
		{	{	0,	"0-Pointer to Tag",				"TAGPNTR"	},	paramReal,	1},
		{	{	1,	"1-Archive Type",				"ARCHTYPE"	},	paramByte,	1},
		{	{	2,	"2-Point Type",					"TYPE"		},	paramByte,	1},
		{	{	3,	"3-Point/Logical Number",			"LOGICAL"	},	paramByte,	1},
		{	{	4,	"4-Parameter Number",				"PARAM"		},	paramByte,	1},
		{	{	5,	"5-Yesterday's Total",				"YDYTOTAL"	},	paramReal,	1},
		{	{	6,	"6-Last Hour's Total",				"LASTHOUR"	},	paramReal,	1},
		{	{	0,	"",						""		},	paramNone,	0},
		},
	21,	// Information for User Defined Points
	{
		{	{	0,	"0-Point Type Description",			"DESC"		},	paramString,	1},
		{	{	1,	"1-Template Pointer",				"TEMPLATE"	},	paramLong,	1},
		{	{	2,	"2-Number of Parameters",			"PARAMS"	},	paramByte,	1},
		{	{	3,	"3-User Program Type",				"UPTYPE"	},	paramByte,	1},
		{	{	0,	"",						""		},	paramNone,	0},
		},
	41,	// Run Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Atmospheric Pressure",		"ATMPRS"		},	paramReal,	0},	
		{	{	2,	"2-Calculation Method II",		"METHOD"		},	paramByte,	0},
		{	{	3,	"3-Spare",				"SPARE1"		},	paramTLP,	0},
		{	{	4,	"4-Pipe Reference Temperature",		"PIPEREFT"		},	paramReal,	0},
		{	{	5,	"5-Pipe Material",			"PIPEMAT"		},	paramByte,	0},	
		{	{	6,	"6-Spare",				"SPARE2"		},	paramByte,	0},
		{	{	7,	"7-Cd / Ftm",				"FB"			},	paramReal,	1},
		{	{	8,	"8-Fr",					"FR"			},	paramReal,	1},
		{	{	9,	"9-Y / Fpm",				"FY"			},	paramReal,	1},	
		{	{	10,	"10-Fpb Factor",			"FPB"			},	paramReal,	1},
		{	{	11,	"11-Ftb Factor",			"FTB"			},	paramReal,	1},
		{	{	12,	"12-Ftf Factor",			"FTF"			},	paramReal,	1},
		{	{	13,	"13-Fgr Factor",			"FGR"			},	paramReal,	1},	
		{	{	14,	"14-Fpb Factor",			"FPB"			},	paramReal,	1},

		{	{	15,	"15-History Point 1",			"HISTPT1"		},	paramByte,	1},
		{	{	16,	"16-RollUp 1",				"ROLLUP1"		},	paramByte,	1},
		{	{	17,	"17-TLP 1",				"TLP1"			},	paramTLP,	1},
		{	{	18,	"18-Conversion 1",			"CONV1"			},	paramReal,	1},

		{	{	19,	"19-History Point 2",			"HISTPT2"		},	paramByte,	1},
		{	{	20,	"20-RollUp 2",				"ROLLUP2"		},	paramByte,	1},
		{	{	21,	"21-TLP 2",				"TLP2"			},	paramTLP,	1},
		{	{	22,	"22-Conversion 2",			"CONV2"			},	paramReal,	1},

		{	{	23,	"23-History Point 3",			"HISTPT3"		},	paramByte,	1},
		{	{	24,	"24-RollUp 3",				"ROLLUP3"		},	paramByte,	1},
		{	{	25,	"25-TLP 3",				"TLP3"			},	paramTLP,	1},
		{	{	26,	"26-Conversion 3",			"CONV3"			},	paramReal,	1},

		{	{	27,	"27-History Point 4",			"HISTPT4"		},	paramByte,	1},
		{	{	28,	"28-RollUp 4",				"ROLLUP4"		},	paramByte,	1},
		{	{	29,	"29-TLP 4",				"TLP4"			},	paramTLP,	1},
		{	{	30,	"30-Conversion 4",			"CONV4"			},	paramReal,	1},

		{	{	31,	"31-History Point 5",			"HISTPT5"		},	paramByte,	1},
		{	{	32,	"32-RollUp 5",				"ROLLUP5"		},	paramByte,	1},
		{	{	33,	"33-TLP 5",				"TLP5"			},	paramTLP,	1},
		{	{	34,	"34-Conversion 5",			"CONV5"			},	paramReal,	1},

		{	{	35,	"35-History Point 6",			"HISTPT6"		},	paramByte,	1},
		{	{	36,	"36-RollUp 6",				"ROLLUP6"		},	paramByte,	1},
		{	{	37,	"37-TLP 6",				"TLP6"			},	paramTLP,	1},
		{	{	38,	"38-Conversion 6",			"CONV6"			},	paramReal,	1},

		{	{	39,	"39-History Point 7",			"HISTPT7"		},	paramByte,	1},
		{	{	40,	"40-RollUp 7",				"ROLLUP7"		},	paramByte,	1},
		{	{	41,	"41-TLP 7",				"TLP7"			},	paramTLP,	1},
		{	{	42,	"42-Conversion 7",			"CONV7"			},	paramReal,	1},

		{	{	43,	"43-History Point 8",			"HISTPT8"		},	paramByte,	1},
		{	{	44,	"44-RollUp 8",				"ROLLUP8"		},	paramByte,	1},
		{	{	45,	"45-TLP 8",				"TLP8"			},	paramTLP,	1},
		{	{	46,	"46-Conversion 8",			"CONV8"			},	paramReal,	1},

		{	{	47,	"47-History Point 9",			"HISTPT9"		},	paramByte,	1},
		{	{	48,	"48-RollUp 9",				"ROLLUP9"		},	paramByte,	1},
		{	{	49,	"49-TLP 9",				"TLP9"			},	paramTLP,	1},
		{	{	50,	"50-Conversion 9",			"CONV9"			},	paramReal,	1},

		{	{	51,	"51-History Point 10",			"HISTPT10"		},	paramByte,	1},
		{	{	52,	"52-RollUp 10",				"ROLLUP10"		},	paramByte,	1},
		{	{	53,	"53-TLP 10",				"TLP10"			},	paramTLP,	1},
		{	{	54,	"54-Conversion 10",			"CONV10"		},	paramReal,	1},

		{	{	0,	"",						""		},	paramNone,	0},
		},
	42,	// Extra Run Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Flow Today",				"FLOTDY"		},	paramReal,	1},	
		{	{	2,	"2-Flow Yesterday",			"FLOYDY"		},	paramReal,	1},
		{	{	3,	"3-Flow Month",				"FLOMTH"		},	paramReal,	1},
		{	{	4,	"4-Flow Previous Month",		"FLOPRV"		},	paramReal,	1},
		{	{	5,	"5-Flow Accumulated",			"FLOACC"		},	paramReal,	1},	
		{	{	6,	"6-Minutes Today",			"MINTDY"		},	paramReal,	1},
		{	{	7,	"7-Minutes Yesterday",			"MINYDY"		},	paramReal,	1},
		{	{	8,	"8-Minutes Month",			"MINMTH"		},	paramReal,	1},
		{	{	9,	"9-Minutes Previous Month",		"MINPRV"		},	paramReal,	1},	
		{	{	10,	"10-Minutes Accumulated",		"MINACC"		},	paramReal,	1},
		{	{	11,	"11-Energy Today",			"ENGTDY"		},	paramReal,	1},
		{	{	12,	"12-Energy Yesterday",			"ENGYDY"		},	paramReal,	1},
		{	{	13,	"13-Energy Month",			"ENGMTH"		},	paramReal,	1},	
		{	{	14,	"14-Energy Previous Month",		"ENGPRV"		},	paramReal,	1},
		{	{	15,	"15-Energy Accumulated",		"ENGACC"		},	paramReal,	1},
		{	{	16,	"16-Uncorrected Today",			"UCCTDY"		},	paramReal,	1},
		{	{	17,	"17-Uncorrected Yesterday",		"UCCYDY"		},	paramReal,	1},
		{	{	18,	"18-Uncorrected Month",			"UCCMTH"		},	paramReal,	1},
		{	{	19,	"19-Uncorrected Previous Month",	"UCCPRV"		},	paramReal,	1},
		{	{	20,	"20-Uncorrected Accumulated",		"UCCACC"		},	paramReal,	1},
		{	{	21,	"21-Orifice Plate Bore Diameter - d",	"ORIFD"			},	paramReal,	1},
		{	{	22,	"22-Piper Internal Diameter - D",	"PIPED"			},	paramReal,	1},
		{	{	23,	"23-Beta - Diameter Ratio",		"BETA"			},	paramReal,	1},
		{	{	24,	"24-Ev - Velocity of Approach",		"EV"			},	paramReal,	1},
		{	{	25,	"25-Cd - Coefficient of Discharge",	"CDISCH"		},	paramReal,	1},
		{	{	26,	"26-Reynolds Number",			"REYNLD"		},	paramReal,	1},
		{	{	27,	"27-Upstream Static Pressure",		"UPRSR"			},	paramReal,	1},
		{	{	28,	"28-Molecular Weight",			"MLWGHT"		},	paramReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	43,	// User List Parameters
	{
		{	{	0,	"0-Text 1",				"TEXT1"			},	paramString,	0},
		{	{	1,	"1-Text 2",				"TEXT2"			},	paramString,	0},	
		{	{	2,	"2-Text 3",				"TEXT3"			},	paramString,	0},
		{	{	3,	"3-Text 4",				"TEXT4"			},	paramString,	0},
		{	{	4,	"4-Text 5",				"TEXT5"			},	paramString,	0},
		{	{	5,	"5-Text 6",				"TEXT6"			},	paramString,	0},	
		{	{	6,	"6-Text 7",				"TEXT7"			},	paramString,	0},
		{	{	7,	"7-Text 8",				"TEXT8"			},	paramString,	0},
		{	{	8,	"8-Text 9",				"TEXT9"			},	paramString,	0},
		{	{	9,	"9-Text 10",				"TEXT10"		},	paramString,	0},	
		{	{	10,	"10-Text 11",				"TEXT11"		},	paramString,	0},
		{	{	11,	"11-Text 12",				"TEXT12"		},	paramString,	0},
		{	{	12,	"12-Text 13",				"TEXT13"		},	paramString,	0},
		{	{	13,	"13-Text 14",				"TEXT14"		},	paramString,	0},	
		{	{	14,	"14-Text 15",				"TEXT15"		},	paramString,	0},
		{	{	15,	"15-Text 16",				"TEXT16"		},	paramString,	0},
		{	{	16,	"16-Data 1",				"DATA1"			},	paramTLP,	0},
		{	{	17,	"17-Data 2",				"DATA2"			},	paramTLP,	0},
		{	{	18,	"18-Data 3",				"DATA3"			},	paramTLP,	0},
		{	{	19,	"19-Data 4",				"DATA4"			},	paramTLP,	0},
		{	{	20,	"20-Data 5",				"DATA5"			},	paramTLP,	0},
		{	{	21,	"21-Data 6",				"DATA6"			},	paramTLP,	0},
		{	{	22,	"22-Data 7",				"DATA7"			},	paramTLP,	0},
		{	{	23,	"23-Data 8",				"DATA8"			},	paramTLP,	0},
		{	{	24,	"24-Data 9",				"DATA9"			},	paramTLP,	0},
		{	{	25,	"25-Data 10",				"DATA10"		},	paramTLP,	0},
		{	{	26,	"26-Data 11",				"DATA11"		},	paramTLP,	0},
		{	{	27,	"27-Data 12",				"DATA12"		},	paramTLP,	0},
		{	{	28,	"28-Data 13",				"DATA13"		},	paramTLP,	0},
		{	{	29,	"29-Data 14",				"DATA14"		},	paramTLP,	0},
		{	{	30,	"30-Data 15",				"DATA15"		},	paramTLP,	0},
		{	{	31,	"31-Data 16",				"DATA16"		},	paramTLP,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	44,	// Power Control Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Status",				"STATUS"		},	paramWord,	1},
		{	{	2,	"2-Enable",				"ENABLE"		},	paramWord,	0},
		{	{	3,	"3-Valid Receive",			"VALRX"			},	paramWord,	0},
		{	{	4,	"4-Start Time #1",			"STTM1"			},	paramWord,	0},
		{	{	5,	"5-Start Time #2",			"STTM2"			},	paramWord,	0},
		{	{	6,	"6-Start Time #3",			"STTM3"			},	paramWord,	0},
		{	{	7,	"7-On Time #1",				"ONTM1"			},	paramWord,	0},
		{	{	8,	"8-On Time #2",				"ONTM2"			},	paramWord,	0},
		{	{	9,	"9-On Time #3",				"ONTM3"			},	paramWord,	0},
		{	{	10,	"10-Off Time #1",			"OFFTM1"		},	paramWord,	0},
		{	{	11,	"11-Off Time #2",			"OFFTM2"		},	paramWord,	0},
		{	{	12,	"12-Off Time #3",			"OFFTM3"		},	paramWord,	0},
		{	{	13,	"13-Active Time Zone",			"AZONE"			},	paramWord,	0},
		{	{	14,	"14-Hold Time",				"HLDTM"			},	paramWord,	0},
		{	{	15,	"15-Power Timer",			"PWRTMR"		},	paramWord,	1},
		{	{	16,	"16-Discrete Output Number",		"LOGDO"			},	paramWord,	0},
		{	{	17,	"17-Low Battery",			"LOBAT"			},	paramReal,	0},
		{	{	18,	"18-On Counter",			"ONCNT"			},	paramLong,	0},
		{	{	19,	"19-Off Counter",			"OFFCNT"		},	paramLong,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	45,	// Meter Calibration and Sampler
	{
		{	{	0,	"0-Calibration Options",				"OPTION"		},	paramByte,	0},
		{	{	1,	"1-Ambient Temperature of Mercury",			"AMBTMP"		},	paramReal,	0},
		{	{	2,	"2-Temperatue of Mercury When Calibrated",		"MTPCAL"		},	paramReal,	0},
		{	{	3,	"3-Calibration Weights Grav. Acceleration",		"CALWGT"		},	paramReal,	0},
		{	{	4,	"4-Water Temperature When Calibrated",			"WTPCAL"		},	paramReal,	0},
		{	{	5,	"5-Air Temperature When Calibrating",			"ATPCAL"		},	paramReal,	0},
		{	{	6,	"6-User Correction Factor",				"CORFAC"		},	paramReal,	0},
		{	{	7,	"7-Sampler Enable",					"SPLENA"		},	paramByte,	0},
		{	{	8,	"8-Sampler Volume Accumulation",			"VOLACC"		},	paramReal,	0},
		{	{	9,	"9-Sampler Duration",					"SPLDUR"		},	paramReal,	0},
		{	{	10,	"10-SM Integrity Alarm Code",				"ALARM"			},	paramByte,	1},
		{	{	11,	"11-SM Integrity Alarm Deadband Time",			"SMALRMDB"		},	paramWord,	0},
		{	{	12,	"12-SM Alarm Control",					"SMALRMEN"		},	paramByte,	0},
		{	{	13,	"13-Integrity Level - Turbine",				"INTEGLVL"		},	paramByte,	0},
		{	{	 0,	"",							""			},	paramNone,	0},
		},
	46,	// Meter Configuration Parameters
	{
		{	{	0,	"0-Point Tag ID",					"TAG"			},	paramString,	0},
		{	{	1,	"1-Point Description",					"DESC"			},	paramString,	0},
		{	{	2,	"2-Calculation Method",					"CMTHI"			},	paramByte,	0},
		{	{	3,	"3-Caluclation Method II",				"CMTHII"		},	paramByte,	0},
		{	{	4,	"4-Options",						"AGACFG"		},	paramByte,	0},
		{	{	5,	"",							""			},	paramNone,	0},	
		{	{	6,	"6-IMP / BMP",						"IMP"			},	paramReal,	0},
		{	{	7,	"7-Pipe Diameter",					"PIPDIA"		},	paramReal,	0},
		{	{	8,	"8-Pipe Reference Temperature",				"PIPERT"		},	paramReal,	0},
		{	{	9,	"9-Pipe Material",					"ALPH"			},	paramByte,	0},
		{	{	10,	"10-Orifice Diameter",					"ORFDIA"		},	paramReal,	0},
		{	{	11,	"11-Orifice Reference Temperature",			"TMEAS"			},	paramReal,	0},
		{	{	12,	"12-Orifice Material",					"ORMAT"			},	paramByte,	0},
		{	{	13,	"13-Base or Contract Pressure",				"PBASE"			},	paramReal,	0},
		{	{	14,	"14-Base or Contract Temperature",			"TBASE"			},	paramReal,	0},
		{	{	15,	"15-Atmospheric Pressure",				"ATMPRS"		},	paramReal,	0},
		{	{	16,	"16-Specific Gravity",					"SPGR"			},	paramReal,	0},
		{	{	17,	"17-Heating Value",					"GASHV"			},	paramReal,	0},
		{	{	18,	"18-Viscosity",						"VISCOS"		},	paramReal,	0},
		{	{	19,	"19-Specific Heat Ratio",				"SPHTRA"		},	paramReal,	0},
		{	{	20,	"20-Elevation",						"ELEVAT"		},	paramReal,	0},
		{	{	21,	"21-Latitude",						"LATUDE"		},	paramReal,	0},
		{	{	22,	"22-Local Gravitational Acceleration",			"GRAVIT"		},	paramReal,	0},
		{	{	23,	"23-N2 - Nitrogen",					"NITROG"		},	paramReal,	0},
		{	{	24,	"24-CO2 - Carbon Dioxide",				"CARBDI"		},	paramReal,	0},
		{	{	25,	"25-H2S - Hydrogen Sulfide",				"HYDSUL"		},	paramReal,	0},
		{	{	26,	"26-H2O - Water",					"WATER"			},	paramReal,	0},
		{	{	27,	"27-He - Helium",					"HELIUM"		},	paramReal,	0},
		{	{	28,	"28-CH4 - Methane",					"METHAN"		},	paramReal,	0},
		{	{	29,	"29-C2H6 - Ethane",					"ETHANE"		},	paramReal,	0},
		{	{	30,	"30-C3H8 -Propane",					"PROPAN"		},	paramReal,	0},
		{	{	31,	"31-C4H10 - n-Butane",					"NBUTAN"		},	paramReal,	0},
		{	{	32,	"32-C4H10 - i-Butane",					"IBUTAN"		},	paramReal,	0},
		{	{	33,	"33-C5H12 - n-Pentane",					"NPENTA"		},	paramReal,	0},
		{	{	34,	"34-C5H12 - i-Pentane",					"IPENTA"		},	paramReal,	0},
		{	{	35,	"35-C6H14 - n-Hexane",					"NHEXAN"		},	paramReal,	0},
		{	{	36,	"36-C7H16 - n-Heptane",					"NHEPTA"		},	paramReal,	0},
		{	{	37,	"37-C8H18 - n-Octane",					"NOCTAN"		},	paramReal,	0},
		{	{	38,	"38-C9H20 - n-Nonane",					"NNONAN"		},	paramReal,	0},
		{	{	39,	"39-C10H22 - n-Decane",					"NDECAN"		},	paramReal,	0},
		{	{	40,	"40-O2 - Oxygen",					"OXYGEN"		},	paramReal,	0},
		{	{	41,	"41-CO - Carbon Monoxide",				"CARBMO"		},	paramReal,	0},
		{	{	42,	"42-H2 - Hydrogen",					"HYDROG"		},	paramReal,	0},
		{	{	43,	"43-Low hw Cutoff / Static K Factor",			"LOFLOW"		},	paramReal,	0},
		{	{	44,	"44-High hw Setpoint",					"LODPSP"		},	paramReal,	0},
		{	{	45,	"45-Low hw Setpoint",					"HIDPSP"		},	paramReal,	0},
		{	{	46,	"46-Enable Stacked hw",					"STDPEN"		},	paramByte,	0},
		{	{	47,	"47-Low hw TLP",					"LOTYP"			},	paramTLP,	0},
		{	{	48,	"48-hw TLP / Uncorrected Flow Rate TLP",		"DPTYP"			},	paramTLP,	0},
		{	{	49,	"49-Pf TLP",						"FPTYP"			},	paramTLP,	0},
		{	{	50,	"50-Tf TLP",						"TPTYP"			},	paramTLP,	0},
		{	{	51,	"51-hw / Uncorrected Flow Rate",			"CURDP"			},	paramReal,	0},
		{	{	52,	"52-Pf - Static Pressure",				"CURSP"			},	paramReal,	0},
		{	{	53,	"53-Tf - Temperature",					"CURTP"			},	paramReal,	0},
		{	{	54,	"54-Alarm Code",					"ALARM"			},	paramByte,	1},
		{	{	55,	"55-Low Alarm Flow",					"LOALM"			},	paramReal,	0},
		{	{	56,	"56-High Alarm Flow",					"HIALM"			},	paramReal,	0},
		{	{	57,	"57-Averaging Technique",				"AVGTYP"		},	paramByte,	0},
		{	{	58,	"58-Full Recalculation Flag",				"FUCALC"		},	paramByte,	0},
		{	{	59,	"59-Input TLP for Multiple K Factor Calc.",		"KFACTINP"		},	paramTLP,	0},
		{	{	60,	"60-Deadband for Multiple K Factor Calc.",		"KFACTDB"		},	paramReal,	1},
		{	{	61,	"61-Lowest K Factor",					"KFACTOR1"		},	paramReal,	0},
		{	{	62,	"62-2nd K Factor",					"KFACTOR2"		},	paramReal,	0},
		{	{	63,	"63-3rd K Factor",					"KFACTOR3"		},	paramReal,	0},
		{	{	64,	"64-4th K Factor",					"KFACTOR4"		},	paramReal,	0},
		{	{	65,	"65-5th K Factor",					"KFACTOR5"		},	paramReal,	0},
		{	{	66,	"66-Lowest EU Value",					"EUVALUE1"		},	paramReal,	0},
		{	{	67,	"67-2nd EU Value",					"EUVALUE2"		},	paramReal,	0},
		{	{	68,	"68-3rd EU Value",					"EUVALUE3"		},	paramReal,	0},
		{	{	69,	"69-4th EU Value",					"EUVALUE4"		},	paramReal,	0},
		{	{	70,	"70-5th EU Value",					"EUVALUE5"		},	paramReal,	0},	
		{	{	 0,	"",							""			},	paramNone,	0},	
		},
	47,	// Meter Flow Values
	{
		{	{	0,	"0-Flow Rate Per Day",					"FLOWDY"		},	paramReal,	1},
		{	{	1,	"1-Energy Rate Per Day",				"ENGDAY"		},	paramReal,	1},
		{	{	2,	"2-Flow Rate Per Hour",					"FLOWHR"		},	paramReal,	1},
		{	{	3,	"3-Energy Rate Per Hour",				"ENGHR"			},	paramReal,	1},
		{	{	4,	"4-Pressue Extension",					"HWPF"			},	paramReal,	1},
		{	{	5,	"5-Expansion Factor / Fpm",				"EXPFTR"		},	paramReal,	1},	
		{	{	6,	"6-CdFT",						"FR"			},	paramReal,	1},
		{	{	7,	"7-Fn / Ftm",						"FB"			},	paramReal,	1},
		{	{	8,	"8-Fpb",						"FPB"			},	paramReal,	1},
		{	{	9,	"9-Ftb",						"FTB"			},	paramReal,	1},
		{	{	10,	"10-Ftf",						"FTF"			},	paramReal,	1},
		{	{	11,	"11-Fgr",						"FGR"			},	paramReal,	1},
		{	{	12,	"12-Fpv",						"FPV"			},	paramReal,	1},
		{	{	13,	"13-Zs",						"FA"			},	paramReal,	1},
		{	{	14,	"14-Zb",						"ZB"			},	paramReal,	1},
		{	{	15,	"15-Zf1",						"ZF"			},	paramReal,	1},
		{	{	16,	"16-IMV / BMV",						"IMVBMV"		},	paramReal,	1},
		{	{	17,	"17-d - Orifice Plate Bore Diameter",			"BORDIA"		},	paramReal,	1},
		{	{	18,	"18-D - Meter Tube Internal Diameter",			"TUBDIA"		},	paramReal,	1},
		{	{	19,	"19-Beta - Diameter Ratio",				"BETA"			},	paramReal,	1},
		{	{	20,	"20-Ev - Velocity of Aproach",				"VELAPP"		},	paramReal,	1},
		{	{	21,	"21-Average hw / Total Counts",				"AVGDP"			},	paramReal,	1},
		{	{	22,	"22-Average Pf",					"AVGAP"			},	paramReal,	1},
		{	{	23,	"23-Average Tf",					"AVGTP"			},	paramReal,	1},
		{	{	24,	"24-Density",						"DENS"			},	paramReal,	1},
		{	{	25,	"25-Base Density",					"BASDEN"		},	paramReal,	1},
		{	{	26,	"26-Reynolds Number",					"REYNLD"		},	paramReal,	1},
		{	{	27,	"27-Upstream Static Pressure",				"UPSPR"			},	paramReal,	1},
		{	{	28,	"28-Molecular Weight",					"MOLWGT"		},	paramReal,	1},
		{	{	29,	"29-Fam",						"FAM"			},	paramReal,	1},
		{	{	30,	"30-Fwt",						"FWT"			},	paramReal,	1},
		{	{	31,	"31-Fwl",						"FWL"			},	paramReal,	1},
		{	{	32,	"32-Fpwl - Static",					"FPWLSP"		},	paramReal,	1},
		{	{	33,	"33-Fpwl - Differential",				"FPWLDP"		},	paramReal,	1},
		{	{	34,	"34-Fhgm",						"FHGM"			},	paramReal,	1},
		{	{	35,	"35-Fhgt",						"FHGT"			},	paramReal,	1},
		{	{	36,	"36-Flow Today",					"FLOTDY"		},	paramReal,	1},
		{	{	37,	"37-Flow Yesterday",					"FLOYDY"		},	paramReal,	1},
		{	{	38,	"38-Flow Month",					"FLOMTH"		},	paramReal,	1},
		{	{	39,	"39-Flow Previous Month",				"FLOPRV"		},	paramReal,	1},
		{	{	40,	"40-Flow Accumulated",					"FLOACC"		},	paramReal,	1},
		{	{	41,	"41-Minutes Today",					"MINTDY"		},	paramReal,	1},
		{	{	42,	"42-Minutes Yesterday",					"MINYDY"		},	paramReal,	1},
		{	{	43,	"43-Minutes Month",					"MINMTH"		},	paramReal,	1},
		{	{	44,	"44-Minutes Previous Month",				"MINPRV"		},	paramReal,	1},
		{	{	45,	"45-Minutes Accumulated",				"MINACC"		},	paramReal,	1},
		{	{	46,	"46-Energy Today",					"ENGTDY"		},	paramReal,	1},
		{	{	47,	"47-Energy Yesterday",					"ENGYDY"		},	paramReal,	1},
		{	{	48,	"48-Energy Month",					"ENGMTH"		},	paramReal,	1},
		{	{	49,	"49-Energy Previous Month",				"ENGPRV"		},	paramReal,	1},
		{	{	50,	"50-Energy Accumulated",				"ENGACC"		},	paramReal,	1},
		{	{	51,	"51-Uncorrected Today",					"UCCTDY"		},	paramReal,	1},
		{	{	52,	"52-Uncorrected Yesterday",				"UCCTDY"		},	paramReal,	1},
		{	{	53,	"53-Uncorrected Month",					"UCCMTH"		},	paramReal,	1},
		{	{	54,	"54-Uncorrected Previous Month",			"UCCPRV"		},	paramReal,	1},
		{	{	55,	"55-Uncorrected Accumulated",				"UCCACC"		},	paramReal,	1},
		{	{	56,	"56-Partial Recalculation Flag",			"PACALC"		},	paramByte,	1},
		{	{	 0,	"",							""			},	paramNone,	0},	
		},
	56,	// Analog Input Calibration Parameters
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	1},
		{	{	1,	"1-Raw Value 1",			"RAW1"			},	paramWord,	1},
		{	{	2,	"2-Raw Value 2",			"RAW2"			},	paramWord,	1},
		{	{	3,	"3-Raw Value 3",			"RAW3"			},	paramWord,	1},
		{	{	4,	"4-Raw Value 4",			"RAW4"			},	paramWord,	1},
		{	{	5,	"5-Raw Value 5",			"RAW5"			},	paramWord,	1},
		{	{	6,	"6-EU Value 1",				"EU1"			},	paramReal,	1},
		{	{	7,	"7-EU Value 2",				"EU2"			},	paramReal,	1},
		{	{	8,	"8-EU Value 3",				"EU3"			},	paramReal,	1},
		{	{	9,	"9-EU Value 4",				"EU4"			},	paramReal,	1},
		{	{	10,	"10-EU Value 5",			"EU5"			},	paramReal,	1},
		{	{	11,	"11-Press Effect",			"PRESSEFF"		},	paramReal,	1},
		{	{	12,	"12-Set EU Value",			"SETVAL"		},	paramReal,	0},
		{	{	13,	"13-Manual EU",				"MANUAL"		},	paramReal,	1},
		{	{	14,	"14-Timer",				"TIMER"			},	paramWord,	1},
		{	{	15,	"15-Mode",				"MODE"			},	paramByte,	0},
		{	{	16,	"16-Type",				"TYPE"			},	paramByte,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	58,	// Revision Information
	{
		{	{	0,	"0-Device Firmwaree Description",	"DESC"			},	paramString,	1},
		{	{	1,	"1-Part Number",			"PART#"			},	paramString,	1},
		{	{	2,	"2-Version",				"VERSION"		},	paramString,	1},
		{	{	3,	"3-Information Present Flag",		"PRESENT"		},	paramByte,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},

	71,	// Cause Configuration - NOTE: mapped internally to 77
	{
		{	{	0,	"0-Cause Tag",				"PTTAG"			},	paramString,	0},
		{	{	1,	"1-Enable Cause",			"ENABLE"		},	paramByte,	0},
		{	{	2,	"2-Input1 Definition",			"INDEF1"		},	paramTLP,	1},
		{	{	3,	"3-Input1 Tag",				"INTAG1"		},	paramString,	1},
		{	{	4,	"4-Cur Value1",				"CUVAL1"		},	paramReal,	0},
		{	{	5,	"5-Function1 Type",			"RELAT1"		},	paramByte,	0},
		{	{	6,	"6-SetPt1 Definition",			"SETDF1"		},	paramTLP,	0},
		{	{	7,	"7-SetPt1 Value",			"SETPT1"		},	paramReal,	0},
		{	{	8,	"8-Deadband or Result1",		"DBRES1"		},	paramReal,	0},
		{	{	9,	"9-Part2 Enable",			"USEPT2"		},	paramByte,	0},
		{	{	10,	"10-Input2 Definition",			"INDEF2"		},	paramTLP,	1},
		{	{	11,	"11-Input2 Tag",			"INTAG2"		},	paramString,	1},
		{	{	12,	"12-Cur Value2",			"CUVAL2"		},	paramReal,	0},
		{	{	13,	"13-Function2 Type",			"RELAT2"		},	paramByte,	0},
		{	{	14,	"14-SetPt2 Definition",			"SETDF2"		},	paramTLP,	0},
		{	{	15,	"15-SetPt2Value",			"SETPT2"		},	paramReal,	0},
		{	{	16,	"16-Deadband or Result2",		"DBRES2"		},	paramReal,	0},
		{	{	17,	"17-And Or Mode",			"ANDOR"			},	paramByte,	0},
		{	{	18,	"18-Cause Trip Clear",			"CZTRUE"		},	paramByte,	1},
		{	{	19,	"19-Part1 Trip Clear",			"P1TRUE"		},	paramByte,	1},
		{	{	20,	"20-Part2 Trip Clear",			"P2TRUE"		},	paramByte,	1},
		{	{	21,	"21-Use Digital Enabler",		"ENABRQ"		},	paramByte,	0},
		{	{	22,	"22-Digi Enab Defintion",		"ENADEF"		},	paramTLP,	0},
		{	{	23,	"23-Digi Enab Tag",			"ENATAG"		},	paramString,	1},
		{	{	24,	"24-Digi Enab Process Value",		"ENAPV"			},	paramReal,	1},
		{	{	25,	"25-Digi Enabler Type",			"ENAREL"		},	paramByte,	0},
		{	{	26,	"26-Digi Enab StPt Value",		"ENSTPT"		},	paramReal,	0},
		{	{	27,	"27-Digi Enab Result Value",		"ENARLT"		},	paramByte,	0},
		{	{	28,	"28-Enab Delay Secs Preset",		"ENAPRE"		},	paramWord,	0},
		{	{	29,	"29-Enab Delay Secs Elapsed",		"ENACNT"		},	paramWord,	1},
		{	{	30,	"30-Pri Trip Delay Secs Preset",	"TRPPR1"		},	paramWord,	0},
		{	{	31,	"31-Pri Trip Delay Secs Elapsed",	"TRPCT1"		},	paramWord,	1},
		{	{	32,	"32-Scan Interval",			"SCANIV"		},	paramByte,	0},
		{	{	33,	"33-Log Alarms",			"LOGALM"		},	paramByte,	0},
		{	{	34,	"34-Require Reset",			"RSTREQ"		},	paramByte,	0},
		{	{	35,	"35-Effect 1",				"EFFT1"			},	paramByte,	0},
		{	{	36,	"36-Effect 2",				"EFFT2"			},	paramByte,	0},
		{	{	37,	"37-Effect 3",				"EFFT3"			},	paramByte,	0},
		{	{	38,	"38-Effect 4",				"EFFT4"			},	paramByte,	0},
		{	{	39,	"39-Effect 5",				"EFFT5"			},	paramByte,	0},
		{	{	40,	"40-Effect 6",				"EFFT6"			},	paramByte,	0},
		{	{	41,	"41-Effect 7",				"EFFT7"			},	paramByte,	0},
		{	{	42,	"42-Effect 8",				"EFFT8"			},	paramByte,	0},
		{	{	43,	"43-Links Energized",			"LNKENR"		},	paramByte,	1},
		{	{	44,	"44-Min Trip Secs Preset",		"MNTPRE"		},	paramWord,	0},
		{	{	45,	"45-Min Trip Secs Elapsed",		"MNTCNT"		},	paramWord,	1},
		{	{	46,	"46-Log Clears",			"LOGCLR"		},	paramByte,	0},
		{	{	47,	"47-Reset Code",			"RSTCOD"		},	paramByte,	0},
		{	{	48,	"48-Sec Trip Delay Secs Preset",	"TRPPR2"		},	paramWord,	0},
		{	{	49,	"49-Sec Trip Delay Secs Elapsed",	"TRPCT2"		},	paramWord,	1},
		{	{	50,	"50-Pri Trip Delay Timer Timing",	"TMRTT1"		},	paramByte,	1},
		{	{	51,	"51-Sec Trip Delay Timer Timing",	"TMRTT2"		},	paramByte,	1},
		{	{	52,	"52-Accumulated Trips",			"TRPACM"		},	paramWord,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	72,	// Effect Configuration - NOTE: mapped internally to 78
	{
		{	{	0,	"0-Effect Tag",				"EFFTAG"		},	paramString,	0},
		{	{	1,	"1-Effect Enable",			"EFFENB"		},	paramByte,	0},
		{	{	2,	"2-Effect Definition",			"EFFDEF"		},	paramTLP,	0},
		{	{	3,	"3-Definition Tag",			"DEFTAG"		},	paramString,	1},
		{	{	4,	"4-Now Active",				"CURENG"		},	paramByte,	1},
		{	{	5,	"5-Cur Value",				"CURVAL"		},	paramReal,	1},
		{	{	6,	"6-Value When Active",			"ENGVAL"		},	paramReal,	0},
		{	{	7,	"7-Value When Not Active",		"UENVAL"		},	paramReal,	0},
		{	{	8,	"8-Apply When Not Active",		"WRITEU"		},	paramByte,	0},
		{	{	9,	"9-Is Reset Pt",			"RESTPT"		},	paramByte,	0},
		{	{	10,	"10-1st Out Cause",			"1OUTCZ"		},	paramByte,	1},
		{	{	11,	"11-2nd Out Cause",			"2OUTCZ"		},	paramByte,	1},
		{	{	12,	"12-3rd Out Cause",			"3OUTCZ"		},	paramByte,	1},
		{	{	13,	"13-4th Out Cause",			"4OUTCZ"		},	paramByte,	1},
		{	{	14,	"14-1st Out Tag",			"1OTTAG"		},	paramString,	1},
		{	{	15,	"15-2nd Out Tag",			"2OTTAG"		},	paramString,	1},
		{	{	16,	"16-3rd Out Tag",			"3OTTAG"		},	paramString,	1},
		{	{	17,	"17-4th Out Tag",			"4OTTAG"		},	paramString,	1},
		{	{	18,	"18-Reset Code",			"RSTCOD"		},	paramByte,	0},
		{	{	19,	"19-Active Link Count",			"LNKCNT"		},	paramByte,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	86,	// Extended History Parameters
	{
		{	{	0,	"0-Maximum Number of Extended History Points",	"MAXPTS"	},	paramByte,	1},
		{	{	1,	"1-Save Interval",				"INTERVAL"	},	paramByte,	0},	
		
		{	{	2,	"2-Point Tag Identification TLP #1",		"TAG#1"		},	paramTLP,	0},
		{	{	3,	"3-Extended History Log Point #1",		"HST#1"		},	paramTLP,	0},
		{	{	4,	"4-Archive Type #1",				"ARCH1"		},	paramByte,	0},
		{	{	5,	"5-Averaging or Rate Type #1",			"AVG#1"		},	paramByte,	0},

		{	{	6,	"6-Point Tag Identification TLP #2",		"TAG#2"		},	paramTLP,	0},
		{	{	7,	"7-Extended History Log Point #2",		"HST#2"		},	paramTLP,	0},
		{	{	8,	"8-Archive Type #2",				"ARCH2"		},	paramByte,	0},
		{	{	9,	"9-Averaging or Rate Type #2",			"AVG#2"		},	paramByte,	0},

		{	{	10,	"10-Point Tag Identification TLP #3",		"TAG#3"		},	paramTLP,	0},
		{	{	11,	"11-Extended History Log Point #3",		"HST#3"		},	paramTLP,	0},
		{	{	12,	"12-Archive Type #3",				"ARCH3"		},	paramByte,	0},
		{	{	13,	"13-Averaging or Rate Type #3",			"AVG#3"		},	paramByte,	0},

		{	{	14,	"14-Point Tag Identification TLP #4",		"TAG#4"		},	paramTLP,	0},
		{	{	15,	"15-Extended History Log Point #4",		"HST#4"		},	paramTLP,	0},
		{	{	16,	"16-Archive Type #4",				"ARCH4"		},	paramByte,	0},
		{	{	17,	"17-Averaging or Rate Type #4",			"AVG#4"		},	paramByte,	0},

		{	{	18,	"18-Point Tag Identification TLP #5",		"TAG#5"		},	paramTLP,	0},
		{	{	19,	"19-Extended History Log Point #5",		"HST#5"		},	paramTLP,	0},
		{	{	20,	"20-Archive Type #5",				"ARCH5"		},	paramByte,	0},
		{	{	21,	"21-Averaging or Rate Type #5",			"AVG#5"		},	paramByte,	0},

		{	{	22,	"22-Point Tag Identification TLP #6",		"TAG#6"		},	paramTLP,	0},
		{	{	23,	"23-Extended History Log Point #6",		"HST#6"		},	paramTLP,	0},
		{	{	24,	"24-Archive Type #6",				"ARCH6"		},	paramByte,	0},
		{	{	25,	"25-Averaging or Rate Type #6",			"AVG#6"		},	paramByte,	0},

		{	{	26,	"26-Point Tag Identification TLP #7",		"TAG#7"		},	paramTLP,	0},
		{	{	27,	"27-Extended History Log Point #7",		"HST#7"		},	paramTLP,	0},
		{	{	28,	"28-Archive Type #7",				"ARCH7"		},	paramByte,	0},
		{	{	29,	"29-Averaging or Rate Type #7",			"AVG#7"		},	paramByte,	0},

		{	{	30,	"30-Point Tag Identification TLP #8",		"TAG#8"		},	paramTLP,	0},
		{	{	31,	"31-Extended History Log Point #8",		"HST#8"		},	paramTLP,	0},
		{	{	32,	"32-Archive Type #8",				"ARCH8"		},	paramByte,	0},
		{	{	33,	"33-Averaging or Rate Type #8",			"AVG#8"		},	paramByte,	0},

		{	{	34,	"34-Point Tag Identification TLP #9",		"TAG#9"		},	paramTLP,	0},
		{	{	35,	"35-Extended History Log Point #9",		"HST#9"		},	paramTLP,	0},
		{	{	36,	"36-Archive Type #9",				"ARCH9"		},	paramByte,	0},
		{	{	37,	"37-Averaging or Rate Type #9",			"AVG#9"		},	paramByte,	0},

		{	{	38,	"38-Point Tag Identification TLP #10",		"TAG#10"	},	paramTLP,	0},
		{	{	39,	"39-Extended History Log Point #10",		"HST#10"	},	paramTLP,	0},
		{	{	40,	"40-Archive Type #10",				"ARCH10"	},	paramByte,	0},
		{	{	41,	"41-Averaging or Rate Type #10",		"AVG#10"	},	paramByte,	0},

		{	{	42,	"42-Point Tag Identification TLP #11",		"TAG#11"	},	paramTLP,	0},
		{	{	43,	"43-Extended History Log Point #11",		"HST#11"	},	paramTLP,	0},
		{	{	44,	"44-Archive Type #11",				"ARCH11"	},	paramByte,	0},
		{	{	45,	"45-Averaging or Rate Type #11",		"AVG#11"	},	paramByte,	0},

		{	{	46,	"46-Point Tag Identification TLP #12",		"TAG#12"	},	paramTLP,	0},
		{	{	47,	"47-Extended History Log Point #12",		"HST#12"	},	paramTLP,	0},
		{	{	48,	"48-Archive Type #12",				"ARCH12"	},	paramByte,	0},
		{	{	49,	"49-Averaging or Rate Type #12",		"AVG#12"	},	paramByte,	0},

		{	{	50,	"50-Point Tag Identification TLP #13",		"TAG#13"	},	paramTLP,	0},
		{	{	51,	"51-Extended History Log Point #13",		"HST#13"	},	paramTLP,	0},
		{	{	52,	"52-Archive Type #13",				"ARCH13"	},	paramByte,	0},
		{	{	53,	"53-Averaging or Rate Type #13",		"AVG#13"	},	paramByte,	0},

		{	{	54,	"54-Point Tag Identification TLP #14",		"TAG#14"	},	paramTLP,	0},
		{	{	55,	"55-Extended History Log Point #14",		"HST#14"	},	paramTLP,	0},
		{	{	56,	"56-Archive Type #14",				"ARCH14"	},	paramByte,	0},
		{	{	57,	"57-Averaging or Rate Type #14",		"AVG#14"	},	paramByte,	0},

		{	{	58,	"58-Point Tag Identification TLP #15",		"TAG#15"	},	paramTLP,	0},
		{	{	59,	"59-Extended History Log Point #15",		"HST#15"	},	paramTLP,	0},
		{	{	60,	"60-Archive Type #15",				"ARCH15"	},	paramByte,	0},
		{	{	61,	"61-Averaging or Rate Type #15",		"AVG#15"	},	paramByte,	0},
					
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	87,	// Expanded I/O Information
	{
		{	{	0,	"0-System Mode",			"SYSMODE"		},	paramByte,	1},
		{	{	1,	"1-Board Health",			"BRDSTS"		},	paramByte,	1},
		{	{	2,	"2-Boot Version",			"BOOTVER"		},	paramString,	1},
		{	{	3,	"3-Flash Version",			"FIRMVER"		},	paramString,	1},
		{	{	4,	"4-Boot Part Number",			"BOOTPN"		},	paramString,	1},
		{	{	5,	"5-Flash Part Number",			"FIRMPN"		},	paramString,	1},
		{	{	6,	"6-Installed Active",			"ACTIVE"		},	paramByte,	1},
		{	{	7,	"7-Module Slots Addressable",		"SLOTS"			},	paramByte,	1},
		{	{	8,	"8-Onboard Temperature",		"BRDTEMP"		},	paramReal,	1},
		{	{	9,	"9-+12V Module Incoming",		"INVOLTS"		},	paramReal,	1},
		{	{      10,	"10-+12V Module",			"BRDVOLTS"		},	paramReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	91,	// System Variables
	{
		{	{	0,	"0-ROC Address",			"ROCADR"		},	paramByte,	0},
		{	{	1,	"1-ROC Group",				"ROCGRP"		},	paramByte,	0},
		{	{	2,	"2-Station Name",			"NAME"			},	paramString,	0},
		{	{	3,	"3-Part Number and Version",		"VER"			},	paramString,	1},
		{	{	4,	"4-Time Created",			"VERTIME"		},	paramString,	1},
		{	{	5,	"5-Manufacturer ID",			"ID"			},	paramString,	1},
		{	{	6,	"6-Product Description",		"DESC"			},	paramString,	1},
		{	{	7,	"7-Serial Number",			"SERNUM"		},	paramLong,	1},
		{	{	8,	"8-Maximum Events",			"MAXEVT"		},	paramWord,	1},
		{	{	9,	"9-Maximum Alarms",			"MAXALM"		},	paramWord,	1},
		{	{      10,	"10-Maximum PIDs",			"MAXPID"		},	paramByte,	1},
		{	{      11,	"11-Maximum Meter Runs",		"MAXMTR"		},	paramByte,	1},
		{	{      12,	"12-Maximum FSTs",			"MAXFST"		},	paramByte,	1},
		{	{      13,	"13-Event Index",			"EVTINDEX"		},	paramWord,	1},
		{	{      14,	"14-Alarm Index",			"ALMINDEX"		},	paramWord,	1},
		{	{      15,	"15-Active PIDs",			"PIDS"			},	paramByte,	0},
		{	{      16,	"16-Active Stations",			"STATIONS"		},	paramByte,	0},
		{	{      17,	"17-Active Orifice Meter Runs",		"OMRS"			},	paramByte,	0},
		{	{      18,	"18-Active Linear Meter Runs",		"TMRS"			},	paramByte,	0},
		{	{      19,	"19-FST Clear",				"FSTCLR"		},	paramByte,	0},
		{	{      20,	"20-Clear Configuration Memory",	"CLRCONF"		},	paramByte,	0},
		{	{      21,	"21-Wrtie to Configuration Memory",	"WRTCONF"		},	paramByte,	0},
		{	{      22,	"22-Configuration Memory Write Complete","WRTDONE"		},	paramByte,	1},
		{	{      23,	"23-MPU Loading",			"MPULOD"		},	paramReal,	1},
		{	{      24,	"24-LCD On Off",			"LCDONOFF"		},	paramByte,	0},
		{	{      25,	"25-IO Scanning",			"IOEN"			},	paramByte,	0},
		{	{      26,	"26-Warm Start",			"WARM"			},	paramByte,	0},
		{	{      27,	"27-Cold Start",			"COLDSTRT"		},	paramByte,	0},
		{	{      28,	"28-LCD Status",			"LCDSTAT"		},	paramByte,	1},
		{	{      29,	"29-LCD Video Mode",			"LCDMODE"		},	paramByte,	0},
		{	{      30,	"30-Power Save Time",			"LCDPOWER"		},	paramByte,	0},
		{	{      31,	"31-Baud Rate Generator #0 Rate",	"RATE1"			},	paramLong,	0},
		{	{      32,	"32-Baud Rate Generator #1 Rate",	"RATE2"			},	paramLong,	0},
		{	{      33,	"33-Baud Rate Generator #2 Rate",	"RATE3"			},	paramLong,	0},
		{	{      34,	"34-Baud Rate Generator #3 Rate",	"RATE4"			},	paramLong,	0},
		{	{      35,	"35-CRC Check",				"CRCCHK"		},	paramByte,	0},
		{	{      36,	"36-LED Enable",			"LEDEN"			},	paramByte,	0},
		{	{      37,	"37-Boot Part Number and Version",	"BOOTVER"		},	paramString,	1},
		{	{      38,	"38-Boot Firmware Time Created",	"BOOTTIME"		},	paramString,	1},
		{	{      39,	"39-Active Samplers",			"SAMPLERS"		},	paramByte,	0},
		{	{      40,	"40-Clear History Configuration",	"CLRHIST"		},	paramByte,	0},
		{	{      41,	"41-Flash Disk Space Used",		"FLSHUSED"		},	paramLong,	1},
		{	{      42,	"42-Flash Disk Space Free",		"FLSHAVAL"		},	paramLong,	1},
		{	{      43,	"43-Number of System Initializations",	"NUMINIT"		},	paramWord,	0},
		{	{      44,	"44-Number of Warm Starts",		"NUMWARM"		},	paramWord,	0},
		{	{      45,	"45-Number of Cold Starts",		"NUMCOLD"		},	paramWord,	0},
		{	{      46,	"46-Number of Power Cycles",		"NUMPWR"		},	paramWord,	0},
		{	{      47,	"47-Last Power Down Time",		"PWRDNTM"		},	paramLong,	1},
		{	{      48,	"48-Last Power Up Time",		"PWRUPTM"		},	paramLong,	1},
		{	{      49,	"49-Auto Logout Period",		"KDLOGOUT"		},	paramByte,	0},
		{	{      50,	"50-Logical Compatibility Mode",	"COMPMODE"		},	paramByte,	0},
		{	{      51,	"51-ROC Seris",				"SERIES"		},	paramString,	1},
		{	{      52,	"52-Num Active Virtual DO",		"MAXVDO"		},	paramByte,	0},
	//	{	{      53,	"53-System Rollover for Doubles",	"ROLLOVER"		},	paramDouble,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	92,	// Logon Parameters
	{
		{	{	0,	"0-Operator ID",			"OPID"			},	paramString,	0},
		{	{	1,	"1-List Security",			"SECLIST"		},	paramByte,	0},
		{	{	2,	"2-Keypad Security Level - Read Enable","KEYREAD"		},	paramByte,	0},
		{	{	3,	"3-Keypad Security Level - Write Enable","KEYWRT"		},	paramByte,	0},
		{	{	4,	"4-Password",				"PSWD"			},	paramWord,	0},
		{	{	5,	"5-Access Level",			"ACCESS"		},	paramByte,	0},
		{	{	6,	"6-Group #1",				"USRGRP1"		},	paramByte,	0},
		{	{	7,	"7-Group #2",				"USRGRP2"		},	paramByte,	0},
		{	{	8,	"8-Group #3",				"USRGRP3"		},	paramByte,	0},
		{	{	9,	"9-Group #4",				"USRGRP4"		},	paramByte,	0},
		{	{      10,	"10-Group #5",				"USRGRP5"		},	paramByte,	0},
		{	{      11,	"11-Group #6",				"USRGRP6"		},	paramByte,	0},
		{	{      12,	"12-Group #7",				"USRGRP7"		},	paramByte,	0},
		{	{      13,	"13-Group #8",				"USRGRP8"		},	paramByte,	0},
		{	{      14,	"14-Group #9",				"USRGRP9"		},	paramByte,	0},
		{	{      15,	"15-Group #10",				"USRGRP10"		},	paramByte,	0},
		{	{      16,	"16-Group #11",				"USRGRP11"		},	paramByte,	0},
		{	{      17,	"17-Group #12",				"USRGRP12"		},	paramByte,	0},
		{	{      18,	"18-Group #13",				"USRGRP13"		},	paramByte,	0},
		{	{      19,	"19-Group #14",				"USRGRP14"		},	paramByte,	0},
		{	{      20,	"20-Group #15",				"USRGRP15"		},	paramByte,	0},
		{	{      21,	"21-Group #16",				"USRGRP16"		},	paramByte,	0},
		{	{      22,	"18-Group #17",				"USRGRP17"		},	paramByte,	0},
		{	{      23,	"19-Group #18",				"USRGRP18"		},	paramByte,	0},
		{	{      24,	"20-Group #19",				"USRGRP19"		},	paramByte,	0},
		{	{      25,	"21-Group #20",				"USRGRP20"		},	paramByte,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	93,	// License Key Information
	{
		{	{	0,	"0-License Key Slot #",			"HKID"			},	paramByte,	1},
		{	{	1,	"1-License Code #",			"HKS"			},	paramByte,	1},
		{	{	2,	"2-Application Name",			"APPNAME"		},	paramString,	1},
		{	{	3,	"3-Provider Name",			"PRONAME"		},	paramString,	1},
		{	{	4,	"4-Application Code",			"APPCODE"		},	paramWord,	1},
		{	{	5,	"5-Version",				"VER"			},	paramString,	1},
		{	{	6,	"6-Quantity Total",			"QTOTAL"		},	paramByte,	1},
		{	{	7,	"7-Quantity Remaining",			"QLEFT"			},	paramByte,	1},
		{	{	8,	"8-Expiration Date",			"EXPDATE"		},	paramLong,	1},
		{	{	9,	"9-SW License Validity",		"SWL"			},	paramByte,	1},
		{	{      10,	"10-Time Created",			"VALID"			},	paramLong,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	94,	// User C Configuration
	{
		{	{	0,	"0-Program Description",		"NAME"			},	paramString,	1},
		{	{	1,	"1-Program Version String",		"VERSION"		},	paramString,	1},
		{	{	2,	"2-Program Time Date Stamp",		"CREATED"		},	paramLong,	1},
		{	{	3,	"3-Program Library Version",		"LIBVER"		},	paramString,	1},
		{	{	4,	"4-Program Enable",			"RUN"			},	paramByte,	0},
		{	{	5,	"5-Program Clear",			"CLEAR"			},	paramByte,	0},
		{	{	6,	"6-Program Status",			"STATUS"		},	paramByte,	1},
		{	{	7,	"7-Program Disk Space Used",		"FLSHUSED"		},	paramLong,	1},
		{	{	8,	"8-Program DRAM Used",			"DRAMUSED"		},	paramLong,	1},
		{	{	9,	"9-Program Auto Restart Counter",	"RSTRTCNT"		},	paramLong,	0},
		{	{      10,	"10-Program Entry Point",		"ENTRY"			},	paramLong,	1},
		{	{      11,	"11-Program Handle",			"HANDLE"		},	paramLong,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	95,	// ROC Comm Ports
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Baud Rate Generator Used",		"BAUD"			},	paramWord,	0},
		{	{	2,	"2-Stop Bits",				"SBITS"			},	paramByte,	0},
		{	{	3,	"3-Data Bits",				"DBITS"			},	paramByte,	0},
		{	{	4,	"4-Parity",				"PARITY"		},	paramByte,	0},
		{	{	5,	"5-Comm Type",				"TYPE"			},	paramByte,	1},
		{	{	6,	"6-Store and Forward Port",		"SFPORT"		},	paramByte,	0},
		{	{	7,	"7-Key On Delay",			"KEYON"			},	paramReal,	0},
		{	{	8,	"8-Key Off Delay",			"KEYOFF"		},	paramReal,	0},
		{	{	9,	"9-Modem Status",			"MSTAT"			},	paramByte,	1},
		{	{      10,	"10-Modem Type",			"MTYPE"			},	paramByte,	0},
		{	{      11,	"11-Connect Time",			"ONTIME"		},	paramReal,	0},
		{	{      12,	"12-Configuration Command",		"CONFIGCM"		},	paramString,	0},
		{	{      13,	"13-Connect Command",			"CONNECTC"		},	paramString,	0},
		{	{      14,	"14-Disconnect Time",			"OFFTIME"		},	paramReal,	0},
		{	{      15,	"15-Inactivity Time",			"INACTTMR"		},	paramReal,	0},
		{	{      16,	"16-Modem Disconnect Command",		"DISCCMD"		},	paramString,	0},
		{	{      17,	"17-SRBX Status",			"SRBXSTAT"		},	paramByte,	1},
		{	{      18,	"18-Enable SRBX",			"SRBXEN"		},	paramByte,	0},
		{	{      19,	"19-SRBX Alarm Index",			"SRBXINDE"		},	paramWord,	1},
		{	{      20,	"20-SRBX Time Base #1",			"SRBXBASE"		},	paramReal,	0},
		{	{      21,	"21-SRBX Retry Count #1",		"SRBXRTR2"		},	paramByte,	0},
		{	{      22,	"22-SRBX Time Base #2",			"SRBXBAS2"		},	paramReal,	0},
		{	{      23,	"23-SRBX Retry Count #2",		"SRBXRETR"		},	paramByte,	0},
		{	{      24,	"24-SRBX Time Base #3",			"SRBXBAS3"		},	paramReal,	0},
		{	{      25,	"25-SRBX Retry Count #3",		"SRBXRTR3"		},	paramByte,	0},
		{	{      26,	"26-SRBX Host Address",			"SRBXHOSR"		},	paramByte,	0},
		{	{      27,	"27-SRBX Host Group",			"SRBXHOST"		},	paramByte,	0},
		{	{      28,	"28-Store & Forward Address #1",	"S&FADD1"		},	paramByte,	0},
		{	{      29,	"29-Store & Forward Group #1",		"S&FGRP1"		},	paramByte,	0},
		{	{      30,	"30-Store & Forward Address #2",	"S&FADD2"		},	paramByte,	0},
		{	{      31,	"31-Store & Forward Group #2",		"S&FGRP2"		},	paramByte,	0},
		{	{      32,	"32-Store & Forward Address #3",	"S&FADD3"		},	paramByte,	0},
		{	{      33,	"33-Store & Forward Group #3",		"S&FGRP3"		},	paramByte,	0},
		{	{      34,	"",					""			},	paramNone,	0},
		{	{      35,	"",					""			},	paramNone,	0},
		{	{      36,	"36-ROC Protocol  Valid Receive Ctr",	"ROCRXCNT"		},	paramWord,	0},
		{	{      37,	"37-ROC Protocol Success Message Time",	"ROCMSGTI"		},	paramLong,	0},
		{	{      38,	"38-Modbus  Valid Receive Ctr",		"MBRXCNTR"		},	paramWord,	0},
		{	{      39,	"39-Modbus Success Message Time",	"MBMSGTIM"		},	paramLong,	0},
		{	{      40,	"40-Number of Invalid Message Bytes",	"INVBYTES"		},	paramWord,	0},
		{	{      41,	"41-Invalid Message Byte Time",		"INVTIME"		},	paramLong,	1},
		{	{      42,	"42-Transmit Counter",			"TXCNTR"		},	paramWord,	0},
		{	{      43,	"43-Port Owner",			"OWNER"			},	paramByte,	0},
		{	{      44,	"44-ROC Protocol Security Status",	"ROCSEC"		},	paramByte,	0},
		{	{      45,	"",					""			},	paramNone,	0},
		{	{      46,	"",					""			},	paramNone,	0},
		{	{      47,	"47-Security Inactivity Timeout",	"SECTIMEO"		},	paramLong,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},

	96,	// FST Parameters
	{
		{	{	0,	"0-Point Tag ID",				"TAG"		},	paramString,	0},
		{	{	1,	"1-Result Register",				"RR"		},	paramReal,	0},	
		{	{	2,	"2-Register #1",				"R1"		},	paramReal,	0},
		{	{	3,	"3-Register #2",				"R2"		},	paramReal,	0},
		{	{	4,	"4-Register #3",				"R3"		},	paramReal,	0},
		{	{	5,	"5-Register #4",				"R4"		},	paramReal,	0},
		{	{	6,	"6-Register #5",				"R5"		},	paramReal,	0},	
		{	{	7,	"7-Register #6",				"R6"		},	paramReal,	0},
		{	{	8,	"8-Register #7",				"R7"		},	paramReal,	0},
		{	{	9,	"9-Register #8",				"R8"		},	paramReal,	0},
		{	{	10,     "10-Register #9",				"R9"		},	paramReal,	0},
		{	{	11,	"11-Register #10",				"R10"		},	paramReal,	0},	
		{	{	12,	"12-Timer #1",					"TMR1"		},	paramLong,	0},
		{	{	13,	"13-Timer #2",					"TMR2"		},	paramLong,	0},
		{	{	14,	"14-Timer #3",					"TMR3"		},	paramLong,	0},
		{	{	15,	"15-Timer #4",					"TMR4"		},	paramLong,	0},
		{	{	16,	"16-Message #1",				"MSG1"		},	paramString,	0},	
		{	{	17,	"17-Message #2",				"MSG2"		},	paramString,	0},	
		{	{	18,	"18-Message Data",				"MSGDATA"	},	paramString,	1},
		{	{	19,	"19-Miscellaneous 1",				"MISC1"		},	paramByte,	0},
		{	{	20,     "20-Miscellaneous 2",				"MISC2"		},	paramByte,	0},
		{	{	21,	"21-Miscellaneous 3",				"MISC3"		},	paramByte,	0},	
		{	{	22,	"22-Miscellaneous 4",				"MISC4"		},	paramByte,	0},
		{	{	23,	"23-Compare Flag",				"CMPFLG"	},	paramByte,	0},
		{	{	24,	"24-Run Flag",					"RUNFLG"	},	paramByte,	0},
		{	{	25,	"25-Code Size",					"CODESIZE"	},	paramWord,	1},
		{	{	26,	"26-Instruction Pointer",			"IP"		},	paramWord,	0},
		{	{	27,	"27-Execution Delay",				"BREAK"		},	paramWord,	0},
		{	{	28,	"28-FST Version",				"VER"		},	paramString,	1},
		{	{	29,	"29-FST Description",				"DESC"		},	paramString,	1},
		{	{	30,     "30-Message Data #2",				"MSGDATA2"	},	paramString,	1},
		{	{	31,	"31-Steps per Task Cycle",			"INSTPC"	},	paramByte,	0},	
		{	{	32,	"32-Actual Steps per Task Cycle",		"AINST"		},	paramByte,	1},
		{	{	33,	"33-FST Cycle Time",				"DURAT"		},	paramReal,	1},
		{	{	 0,	"",						""		},	paramNone,	0},
		},
	97,	// FST Register Tags
	{
		{	{	0,	"0-Register Tag 1",				"R1TAG"		},	paramString,	0},
		{	{	1,	"1-Register Tag 2",				"R2TAG"		},	paramString,	0},	
		{	{	2,	"2-Register Tag 3",				"R3TAG"		},	paramString,	0},
		{	{	3,	"3-Register Tag 4",				"R4TAG"		},	paramString,	0},
		{	{	4,	"4-Register Tag 5",				"R5TAG"		},	paramString,	0},
		{	{	5,	"5-Register Tag 6",				"R6TAG"		},	paramString,	0},
		{	{	6,	"6-Register Tag 7",				"R7TAG"		},	paramString,	0},	
		{	{	7,	"7-Register Tag 8",				"R8TAG"		},	paramString,	0},
		{	{	8,	"8-Register Tag 9",				"R9TAG"		},	paramString,	0},
		{	{	9,	"9-Register Tag 10",				"R10TAG"	},	paramString,	0},
		{	{	 0,	"",						""		},	paramNone,	0},
		},

	98,	// Soft Point Parameters
	{
		{	{	0,	"0-Soft Point Description",		"TEXT1"			},	paramString,	0},
		{	{	1,	"1-Float 1",				"DATA1"			},	paramReal,	0},
		{	{	2,	"2-Float 2",				"DATA2"			},	paramReal,	0},
		{	{	3,	"3-Float 3",				"DATA3"			},	paramReal,	0},
		{	{	4,	"4-Float 4",				"DATA4"			},	paramReal,	0},
		{	{	5,	"5-Float 5",				"DATA5"			},	paramReal,	0},
		{	{	6,	"6-Float 6",				"DATA6"			},	paramReal,	0},
		{	{	7,	"7-Float 7",				"DATA7"			},	paramReal,	0},
		{	{	8,	"8-Float 8",				"DATA8"			},	paramReal,	0},
		{	{	9,	"9-Float 9",				"DATA9"			},	paramReal,	0},
		{	{      10,	"10-Float 10",				"DATA10"		},	paramReal,	0},
		{	{      11,	"11-Float 11",				"DATA11"		},	paramReal,	0},
		{	{      12,	"12-Float 12",				"DATA12"		},	paramReal,	0},
		{	{      13,	"13-Float 13",				"DATA13"		},	paramReal,	0},
		{	{      14,	"14-Float 14",				"DATA14"		},	paramReal,	0},
		{	{      15,	"15-Float 15",				"DATA15"		},	paramReal,	0},
		{	{      16,	"16-Float 16",				"DATA16"		},	paramReal,	0},
		{	{      17,	"17-Float 17",				"DATA17"		},	paramReal,	0},
		{	{      18,	"18-Float 18",				"DATA18"		},	paramReal,	0},
		{	{      19,	"19-Float 19",				"DATA19"		},	paramReal,	0},
		{	{      20,	"20-Float 20",				"DATA20"		},	paramReal,	0},
		{	{      21,	"21-Long 1",				"LONG1"			},	paramLong,	0},
		{	{      22,	"22-Long 2",				"LONG2"			},	paramLong,	0},
		{	{      23,	"23-Short 1",				"SHORT1"		},	paramWord,	0},
		{	{      24,	"24-Short 2",				"SHORT2"		},	paramWord,	0},
		{	{      25,	"25-Short 3",				"SHORT3"		},	paramWord,	0},
		{	{      26,	"26-Short 4",				"SHORT4"		},	paramWord,	0},
		{	{      27,	"27-Short 5",				"SHORT5"		},	paramWord,	0},
		{	{      28,	"28-Short 6",				"SHORT6"		},	paramWord,	0},
		{	{      29,	"29-Short 7",				"SHORT7"		},	paramWord,	0},
		{	{      30,	"30-Short 8",				"SHORT8"		},	paramWord,	0},
		{	{      31,	"31-Short 9",				"SHORT9"		},	paramWord,	0},
		{	{      32,	"32-Short 10",				"SHORT10"		},	paramWord,	0},
		{	{      33,	"33-Byte 1",				"BYTE1"			},	paramByte,	0},
		{	{      34,	"34-Byte 2",				"BYTE2"			},	paramByte,	0},
		{	{      35,	"35-Byte 3",				"BYTE3"			},	paramByte,	0},
		{	{      36,	"36-Byte 4",				"BYTE4"			},	paramByte,	0},
		{	{      37,	"37-Byte 5",				"BYTE5"			},	paramByte,	0},
		{	{      38,	"38-Byte 6",				"BYTE6"			},	paramByte,	0},
		{	{      39,	"39-Byte 7",				"BYTE7"			},	paramByte,	0},
		{	{      40,	"40-Byte 8",				"BYTE8"			},	paramByte,	0},
		{	{      41,	"41-Byte 9",				"BYTE9"			},	paramByte,	0},
		{	{      42,	"42-Byte 10",				"BYTE10"		},	paramByte,	0},
	/*	{	{      43,	"43-Double 1",				"DOUBLE1"		},	paramDouble,	0},
		{	{      44,	"44-Double 2",				"DOUBLE2"		},	paramDouble,	0},
		{	{      45,	"45-Double 3",				"DOUBLE3"		},	paramDouble,	0},
		{	{      46,	"46-Double 4",				"DOUBLE4"		},	paramDouble,	0},
		{	{      47,	"47-Double 5",				"DOUBLE5"		},	paramDouble,	0},
		{	{      48,	"48-Double 6",				"DOUBLE6"		},	paramDouble,	0},
		{	{      49,	"49-Double 7",				"DOUBLE7"		},	paramDouble,	0},
		{	{      50,	"50-Double 8",				"DOUBLE8"		},	paramDouble,	0},
		{	{      51,	"51-Double 9",				"DOUBLE9"		},	paramDouble,	0},
		{	{      52,	"52-Double 10",				"DOUBLE10"		},	paramDouble,	0},
		{	{      53,	"53-Long 3",				"LONG3"			},	paramLong,	0},
		{	{      54,	"54-Long 4",				"LONG4"			},	paramLong,	0},
		{	{      55,	"55-Long 5",				"LONG5"			},	paramLong,	0},
		{	{      56,	"56-Long 6",				"LONG6"			},	paramLong,	0},
		{	{      57,	"57-Long 7",				"LONG7"			},	paramLong,	0},
		{	{      58,	"58-Long 8",				"LONG8"			},	paramLong,	0},
		{	{      59,	"59-Long 9",				"LONG9"			},	paramLong,	0},
		{	{      60,	"60-Long 10",				"LONG10"		},	paramLong,	0},
		{	{      61,	"61-Logging Enable",			"LOGGING"		},	paramByte,	0},
	*/	{	{	0,	"",					""			},	paramNone,	0},
		},
	99,	// Configurable Opcode
	{
		{	{	0,	"0-Sequence Revision #",		"REV"			},	paramReal,	0},
		{	{	1,	"1-Data 1",				"DATA1"			},	paramTLP,	0},
		{	{	2,	"2-Data 2",				"DATA2"			},	paramTLP,	0},
		{	{	3,	"3-Data 3",				"DATA3"			},	paramTLP,	0},
		{	{	4,	"4-Data 4",				"DATA4"			},	paramTLP,	0},
		{	{	5,	"5-Data 5",				"DATA5"			},	paramTLP,	0},
		{	{	6,	"6-Data 6",				"DATA6"			},	paramTLP,	0},
		{	{	7,	"7-Data 7",				"DATA7"			},	paramTLP,	0},
		{	{	8,	"8-Data 8",				"DATA8"			},	paramTLP,	0},
		{	{	9,	"9-Data 9",				"DATA9"			},	paramTLP,	0},
		{	{	10,	"10-Data 10",				"DATA10"		},	paramTLP,	0},
		{	{	11,	"11-Data 11",				"DATA11"		},	paramTLP,	0},
		{	{	12,	"12-Data 12",				"DATA12"		},	paramTLP,	0},
		{	{	13,	"13-Data 13",				"DATA13"		},	paramTLP,	0},
		{	{	14,	"14-Data 14",				"DATA14"		},	paramTLP,	0},
		{	{	15,	"15-Data 15",				"DATA15"		},	paramTLP,	0},
		{	{	16,	"16-Data 16",				"DATA16"		},	paramTLP,	0},
		{	{	17,	"17-Data 17",				"DATA17"		},	paramTLP,	0},
		{	{	18,	"18-Data 18",				"DATA18"		},	paramTLP,	0},
		{	{	19,	"19-Data 19",				"DATA19"		},	paramTLP,	0},
		{	{	20,	"20-Data 20",				"DATA20"		},	paramTLP,	0},
		{	{	21,	"21-Data 21",				"DATA21"		},	paramTLP,	0},
		{	{	22,	"22-Data 22",				"DATA22"		},	paramTLP,	0},
		{	{	23,	"23-Data 23",				"DATA23"		},	paramTLP,	0},
		{	{	24,	"24-Data 24",				"DATA24"		},	paramTLP,	0},
		{	{	25,	"25-Data 25",				"DATA25"		},	paramTLP,	0},
		{	{	26,	"26-Data 26",				"DATA26"		},	paramTLP,	0},
		{	{	27,	"27-Data 27",				"DATA27"		},	paramTLP,	0},
		{	{	28,	"28-Data 28",				"DATA28"		},	paramTLP,	0},
		{	{	29,	"29-Data 29",				"DATA29"		},	paramTLP,	0},
		{	{	30,	"30-Data 30",				"DATA30"		},	paramTLP,	0},
		{	{	31,	"31-Data 31",				"DATA31"		},	paramTLP,	0},
		{	{	32,	"32-Data 32",				"DATA32"		},	paramTLP,	0},
		{	{	33,	"33-Data 33",				"DATA33"		},	paramTLP,	0},
		{	{	34,	"34-Data 34",				"DATA34"		},	paramTLP,	0},
		{	{	35,	"35-Data 35",				"DATA35"		},	paramTLP,	0},
		{	{	36,	"36-Data 36",				"DATA36"		},	paramTLP,	0},
		{	{	37,	"37-Data 37",				"DATA37"		},	paramTLP,	0},
		{	{	38,	"38-Data 38",				"DATA38"		},	paramTLP,	0},
		{	{	39,	"39-Data 39",				"DATA39"		},	paramTLP,	0},
		{	{	40,	"40-Data 40",				"DATA40"		},	paramTLP,	0},
		{	{	41,	"41-Data 41",				"DATA41"		},	paramTLP,	0},
		{	{	42,	"42-Data 42",				"DATA42"		},	paramTLP,	0},
		{	{	43,	"43-Data 43",				"DATA43"		},	paramTLP,	0},
		{	{	44,	"44-Data 44",				"DATA44"		},	paramTLP,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	100,	// Power Control Parameters
	{	
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Status",				"STATUS"		},	paramByte,	1},
		{	{	2,	"2-Enable",				"ENABLE"		},	paramByte,	0},
		{	{	3,	"3-Start Time #1",			"START1"		},	paramLong,	0},
		{	{	4,	"4-Start Time #2",			"START2"		},	paramLong,	0},
		{	{	5,	"5-Start Time #3",			"START3"		},	paramLong,	0},
		{	{	6,	"6-On Time #1",				"ON1"			},	paramLong,	0},
		{	{	7,	"7-On Time #2",				"ON2"			},	paramLong,	0},
		{	{	8,	"8-On Time #3",				"ON3"			},	paramLong,	0},
		{	{	9,	"9-Off Time #1",			"OFF1"			},	paramLong,	0},
		{	{      10,	"10-Off Time #2",			"OFF2"			},	paramLong,	0},
		{	{      11,	"11-Off Time #3",			"OFF3"			},	paramLong,	0},
		{	{      12,	"12-Active Time Zone",			"ACTIVE"		},	paramByte,	1},
		{	{      13,	"13-Hold Time",				"HLDTM"			},	paramLong,	0},
		{	{      14,	"14-Power Time",			"PWRTMR"		},	paramLong,	1},
		{	{      15,	"15-Discrete Output Number",		"DONUM"			},	paramTLP,	0},
		{	{      16,	"16-Low Battery",			"LOBAT"			},	paramReal,	0},
		{	{      17,	"17-Cumulative On Time",		"TIMEON"		},	paramLong,	0},
		{	{      18,	"18-Cumulative Off Time",		"TIMEOFF"		},	paramLong,	0},
		{	{      19,	"19-Low Battery Deadband",		"BATDB"			},	paramReal,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	101,	// Discrete Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Scanning",			"SCANEN"	},	paramByte,	0},	
		{	{	2,	"2-Filter",			"FILTER"	},	paramReal,	0},
		{	{	3,	"3-Status",			"STATUS"	},	paramByte,	0},
		{	{	4,	"4-Invert Status",		"INVERT"	},	paramByte,	0},
		{	{	5,	"5-Latch Status",		"LATCH"		},	paramByte,	0},
		{	{	6,	"6-Accumulated Value",		"ACCUM"		},	paramLong,	0},	
		{	{	7,	"7-Cumulative On Time",		"ONCTR"		},	paramReal,	0},
		{	{	8,	"8-Cumulative Off Time",	"OFFCTR"	},	paramReal,	0},
		{	{	9,	"9-Alarming",			"ALEN"		},	paramByte,	0},
		{	{	10,	"10-SRBX on Clear",		"SRBXCLR"	},	paramByte,	0},
		{	{	11,	"11-SRBX on Set",		"SRBXSET"	},	paramByte,	0},
		{	{	12,	"12-Alarm Code",		"ALARM"		},	paramByte,	1},
		{	{	13,	"13-Module Scan Period",	"SCANPR"	},	paramReal,	0},	
		{	{	14,	"14-Actual Scan Time",		"SCAN"		},	paramReal,	1},
		{	{	15,	"15-Physical Status",		"PSTATUS"	},	paramByte,	1},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	102,	// Discrete Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Units Tag",			"UNITS"		},	paramString,	0},
		{	{	2,	"2-Scanning"			"SCANEN"	},	paramByte,	0},
		{	{	3,	"3-Alarming",			"ALEN"		},	paramByte,	0},
		{	{	4,	"4-SRBX on Clear",		"SRBXCLR"	},	paramByte,	0},
		{	{	5,	"5-SRBX on Set",		"SRBXSET"	},	paramByte,	0},
		{	{	6,	"6-Alarm Code",			"ALARM"		},	paramByte,	1},
		{	{	7,	"7-Failsafe on Reset",		"CLRONRS"	},	paramByte,	0},
		{	{	8,	"8-Auto Output",		"STATUS"	},	paramByte,	0},
		{	{	9,	"9-Accumulated Value",		"ACCUM"		},	paramLong,	0},
		{	{	10,	"10-Momentary Mode",		"MOMMODE"	},	paramByte,	0},
		{	{	11,	"11-Momentary Active",		"MOMACTIV"	},	paramByte,	1},
		{	{	12,	"12-Toggle Mode",		"TOGMODE"	},	paramByte,	0},
		{	{	13,	"13-Timed Discrete Output",	"TDOMODE"	},	paramByte,	0},
		{	{	14,	"14-Time On",			"TIMEON"	},	paramReal,	0},
		{	{	15,	"15-Cycle Time",		"CYCTIM"	},	paramReal,	0},
		{	{	16,	"16-Low Reading Time",		"LOWTIME"	},	paramReal,	0},
		{	{	17,	"17-High Reading Time",		"HITIME"	},	paramReal,	0},
		{	{	18,	"18-Low Reading EU",		"MINEU"		},	paramReal,	0},
		{	{	19,	"19-High Reading EU",		"MAXEU"		},	paramReal,	0},
		{	{	20,	"20-EU Value",			"EU"		},	paramReal,	0},
		{	{	21,	"21-Manual Value",		"MANVAL"	},	paramByte,	0},
		{	{	22,	"22-Fault Value",		"FAULTVAL"	},	paramByte,	0},
		{	{	23,	"",				""		},	paramNone,	0},
		{	{	24,	"24-Physical Output",		"PHYSOUT"	},	paramByte,	1},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	103,	// Analog Inputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Units Tag",			"UNITS"		},	paramString,	0},	
		{	{	2,	"2-Scanning",			"SCANEN"	},	paramByte,	0},
		{	{	3,	"3-Scan Period",		"SCANPR"	},	paramReal,	0},
		{	{	4,	"4-Actual Scan Time",		"SCAN"		},	paramReal,	1},
		{	{	5,	"5-Filter",			"FILTER"	},	paramByte,	0},
		{	{	6,	"6-Averaging",			"AVGEN"		},	paramByte,	0},	
		{	{	7,	"7-Raw AD Input",		"CURRAW"	},	paramWord,	0},
		{	{	8,	"8-Zero Raw",			"MINRAW"	},	paramWord,	0},
		{	{	9,	"9-Mid Point Raw #1",		"RAW1"		},	paramWord,	0},
		{	{	10,     "10-Mid Point Raw #2",		"RAW2"		},	paramWord,	0},
		{	{	11,	"11-Mid Point Raw #3",		"RAW3"		},	paramWord,	0},
		{	{	12,	"12-Span Raw",			"MAXRAW"	},	paramWord,	0},	
		{	{	13,	"13-Zero EU",			"MINEU"		},	paramReal,	0},
		{	{	14,	"14-Mid Point EU #1",		"EU1"		},	paramReal,	0},
		{	{	15,	"15-Mid Point EU #2",		"EU2"		},	paramReal,	0},
		{	{	16,	"16-Mid Point EU #3",		"EU3"		},	paramReal,	0},
		{	{	17,	"17-Span EU",			"MAXEU"		},	paramReal,	0},
		{	{	18,	"18-Offset",			"ZEROSHIF"	},	paramReal,	0},
		{	{	19,	"19-Set Value",			"SETVAL"	},	paramReal,	0},
		{	{	20,	"20-Manual Value",		"MANVAL"	},	paramReal,	1},
		{	{	21,	"21-EU Value",			"EU"		},	paramReal,	0},	
		{	{	22,	"22-Clipping",			"CLIPEN"	},	paramByte,	0},
		{	{	23,	"23-Low Low Alarm EU",		"LOLOAL"	},	paramReal,	0},
		{	{	24,	"24-Low Alarm EU",		"LOAL"		},	paramReal,	0},
		{	{	25,	"25-High Alarm EU",		"HIAL"		},	paramReal,	0},
		{	{	26,	"26-High High Alarm EU",	"HIHIAL"	},	paramReal,	0},	
		{	{	27,	"27-Rate Alarm EU",		"RATEAL"	},	paramReal,	0},
		{	{	28,	"28-Alarm Deadband",		"ALDBNDL"	},	paramReal,	0},
		{	{	29,	"29-Alarming",			"ALEN"		},	paramByte,	0},
		{	{	30,     "30-SRBX on Clear",		"SRBXCLR"	},	paramByte,	0},
		{	{	31,	"31-SRBX on Set",		"SRBXSET"	},	paramByte,	0},
		{	{	32,	"32-Alarm Code",		"ALARM"		},	paramByte,	1},	
		{	{	33,	"33-Calibration Timer",		"CALTMR"	},	paramReal,	1},
		{	{	34,	"34-Calibration Mode",		"CALMODE"	},	paramByte,	0},
		{	{	35,	"35-Calibration Type",		"CALTYPE"	},	paramByte,	0},
		{	{	 0,	"",				""		},	paramNone,	0},
		},
	104,	// Analog Outputs
	{
		{	{	0,	"0-Point Tag ID",		"TAG"		},	paramString,	0},
		{	{	1,	"1-Units Tag",			"UNITS"		},	paramString,	0},	
		{	{	2,	"2-Scanning",			"SCANEN"	},	paramByte,	0},
		{	{	3,	"3-Alarming",			"ALEN"		},	paramByte,	0},
		{	{	4,	"4-SRBX on Clear",		"SRBXCLR"	},	paramByte,	0},	
		{	{	5,	"5-SRBX on Set",		"SRBXSET"	},	paramByte,	0},
		{	{	6,	"6-Alarm Code",			"ALARM"		},	paramByte,	1},
		{	{	7,	"7-Failsafe on Reset",		"CLRONRS"	},	paramByte,	0},
		{	{	8,	"8-Zero Raw",			"MINRAW"	},	paramWord,	0},
		{	{	9,	"9-Span Raw",			"MAXRAW"	},	paramWord,	0},
		{	{	10,	"10-Zero EU",			"MINEU"		},	paramReal,	0},
		{	{	11,	"11-Span EU",			"MAXEU"		},	paramReal,	0},	
		{	{	12,	"12-EU Value",			"EU"		},	paramReal,	0},
		{	{	13,	"13-Raw DA Output",		"CURRAW"	},	paramWord,	1},
		{	{	14,	"14-Manual Value",		"MANVAL"	},	paramReal,	0},	
		{	{	15,	"15-Failsafe Value",		"FAULTVAL"	},	paramReal,	0},
		{	{	16,	"16-Physical Value",		"PHYSVAL"	},	paramReal,	1},
		{	{	0,	"",				""		},	paramNone,	0},
		},
	105,	// Pulse Inputs
	{
		{	{	0,	"0-Point Tag ID",			"TAG"		},	paramString,	0},
		{	{	1,	"1-Units Tag",				"UNITS"		},	paramString,	0},	
		{	{	2,	"2-Scanning",				"SCANEN"	},	paramByte,	0},
		{	{	3,	"3-Scan Period",			"SCANPR"	},	paramReal,	0},
		{	{	4,	"4-Accumulated Value",			"ACCUM"		},	paramLong,	0},
		{	{	5,	"5-Contract Hour",			"CHOUR"		},	paramByte,	0},
		{	{	6,	"6-Pulses for Day",			"PPD"		},	paramLong,	1},	
		{	{	7,	"7-Current Rate Period",		"RATEPR"	},	paramByte,	0},
		{	{	8,	"8-Conversion",				"CONV"		},	paramByte,	0},
		{	{	9,	"9-Conversion Value",			"CONVAL"	},	paramReal,	0},
		{	{	10,     "10-Current Rate",			"RATE"		},	paramReal,	1},
		{	{	11,	"11-EU Value Mode",			"MODE"		},	paramByte,	0},
		{	{	12,	"12-Rollover Maximum",			"MAXROLL"	},	paramReal,	0},	
		{	{	13,	"13-EU Value",				"EU"		},	paramReal,	0},
		{	{	14,	"14-Low Low Alarm EU",			"LOLOAL"	},	paramReal,	0},
		{	{	15,	"15-Low Alarm EU",			"LOAL"		},	paramReal,	0},
		{	{	16,	"16-High Alarm EU",			"HIAL"		},	paramReal,	0},
		{	{	17,	"17-High High Alarm EU",		"HIHIAL"	},	paramReal,	0},
		{	{	18,	"18-Rate Alarm EU",			"RATEAL"	},	paramReal,	0},
		{	{	19,	"19-Alarm Deadband",			"ALDBND"	},	paramReal,	0},
		{	{	20,	"20-Alarming",				"ALEN"		},	paramByte,	0},
		{	{	21,	"21-SRBX on Clear",			"SRBXCLR"	},	paramByte,	0},	
		{	{	22,	"22-SRBX on Set",			"SRBXSET"	},	paramByte,	0},
		{	{	23,	"23-Alarm Code",			"ALARM"		},	paramByte,	1},
		{	{	24,	"24-Todays Total",			"TDYTOT"	},	paramReal,	0},
		{	{	25,	"25-Yesterdays Total",			"YDYTOT"	},	paramReal,	1},
		{	{	26,	"26-Corrected Pulse Accumulation",	"CORACCUM"	},	paramReal,	1},	
		{	{	27,	"27-Frequency",				"FREQNCY"	},	paramReal,	0},
		{	{	 0,	"",					""		},	paramNone,	0},
		},
	108,	// Multi-Variable Sensor
	{
		{	{	0,	"0-Point Tag ID",			"TAG"		},	paramString,	0},
		{	{	1,	"1-Sensor Address",			"ADDR"		},	paramByte,	0},	
		{	{	2,	"2-Poll Mode",				"POLL"		},	paramByte,	0},
		{	{	3,	"3-Units",				"UNITS"		},	paramByte,	0},
		{	{	4,	"4-Inches H20",				"DPBASE"	},	paramByte,	0},
		{	{	5,	"5-Pressure Tap Location",		"TAP"		},	paramByte,	0},
		{	{	6,	"6-Action on Failure",			"FAILMODE"	},	paramByte,	0},	
		{	{	7,	"7-Software Revision MVS Interface",	"REV"		},	paramByte,	1},
		{	{	8,	"8-Sensor Voltage",			"CVOLTS"	},	paramReal,	1},
		{	{	9,	"9-Sensor Alarming",			"SENSALEN"	},	paramByte,	0},
		{	{	10,	"10-Sensor Alarm Code",			"SENSALAR"	},	paramByte,	1},
		{	{	11,	"11-Sensor Range Status",		"SENSRANG"	},	paramByte,	1},	
		{	{	12,	"12-Static Pressure Effect",		"ZEROSHIF"	},	paramReal,	0},
		{	{	13,	"13-DP Zero Calibration Point",		"DPMINRAW"	},	paramReal,	1},
		{	{	14,	"14-DP Calibration Mid Point #1",	"DPRAW1"	},	paramReal,	1},
		{	{	15,	"15-DP Calibration Mid Point #2",	"DPRAW2"	},	paramReal,	1},
		{	{	16,	"16-DP Calibration Mid Point #3",	"DPRAW3"	},	paramReal,	1},	
		{	{	17,	"17-DP Span Calibration Point",		"DPMAXRAW"	},	paramReal,	1},
		{	{	18,	"18-Manual DP",				"DPMANVAL"	},	paramReal,	1},
		{	{	19,	"19-DP Reading",			"DPEU"		},	paramReal,	0},
		{	{	20,	"20-DP Reverse Reading",		"DPREVEU"	},	paramReal,	1},
		{	{	21,	"21-DP Fault Value",			"DPFALT"	},	paramReal,	0},	
		{	{	22,	"22-DP Low Alarm EU",			"DPLOAL"	},	paramReal,	0},
		{	{	23,	"23-DP High Alarm EU",			"DPHIAL"	},	paramReal,	0},
		{	{	24,	"24-DP Alarm Deadband",			"DPALDB"	},	paramReal,	0},
		{	{	25,	"25-DP Alarming",			"DPALEN"	},	paramByte,	0},
		{	{	26,	"26-SRBX on Clear",			"DPSRBXCL"	},	paramByte,	0},	
		{	{	27,	"27-SRBX on Set",			"DPSRBXSE"	},	paramByte,	0},
		{	{	28,	"28-DP Alarm Code",			"DPALARM"	},	paramByte,	1},
		{	{	29,	"29-SP Zero Calibration Point",		"SPMINRAW"	},	paramReal,	1},
		{	{	30,	"30-SP Calibration Mid Point #1",	"SPRAW1"	},	paramReal,	1},
		{	{	31,	"31-SP Calibration Mid Point #2",	"SPRAW2"	},	paramReal,	1},	
		{	{	32,	"32-SP Calibration Mid Point #3",	"SPRAW3"	},	paramReal,	1},
		{	{	33,	"33-SP Span Calibration Point",		"SPMAXRAW"	},	paramReal,	1},
		{	{	34,	"34-Manual SP",				"SPMANVAL"	},	paramReal,	1},
		{	{	35,	"35-SP Reading",			"SPEU"		},	paramReal,	0},
		{	{	36,	"36-SP Fault Value",			"SPFALT"	},	paramReal,	0},	
		{	{	37,	"37-SP Low Alarm EU",			"SPLOAL"	},	paramReal,	0},
		{	{	38,	"38-SP High Alarm EU",			"SPHIAL"	},	paramReal,	0},
		{	{	39,	"39-SP Alarm Deadband",			"SPALDB"	},	paramReal,	0},
		{	{	40,	"40-SP Alarming",			"SPALEN"	},	paramByte,	0},
		{	{	41,	"41-SRBX on Clear",			"SPSRBXCL"	},	paramByte,	0},	
		{	{	42,	"42-SRBX on Set",			"SPSRBXSE"	},	paramByte,	0},
		{	{	43,	"43-SP Alarm Code",			"SPALARM"	},	paramByte,	1},
		{	{	44,	"44-TMP Zero Calibration Point",	"TMPMINRA"	},	paramReal,	1},
		{	{	45,	"45-TMP Calibration Mid Point #1",	"TMPRAW1"	},	paramReal,	1},
		{	{	46,	"46-TMP Calibration Mid Point #2",	"TMPRAW2"	},	paramReal,	1},	
		{	{	47,	"47-TMP Calibration Mid Point #3",	"TMPRAW3"	},	paramReal,	1},
		{	{	48,	"48-TMP Span Calibration Point",	"TMPMAXRA"	},	paramReal,	1},
		{	{	49,	"49-Manual TMP",			"TMPMANVA"	},	paramReal,	1},
		{	{	50,	"50-TMP Reading",			"TMPEU"		},	paramReal,	0},
		{	{	51,	"51-TMP Fault Value",			"TMPFALT"	},	paramReal,	0},	
		{	{	52,	"52-TMP Low Alarm EU",			"TMPLOAL"	},	paramReal,	0},
		{	{	53,	"53-TMP High Alarm EU",			"TMPHIAL"	},	paramReal,	0},
		{	{	54,	"54-TMP Alarm Deadband",		"TMPALDB"	},	paramReal,	0},
		{	{	55,	"55-TMP Alarming",			"TMPALEN"	},	paramByte,	0},
		{	{	56,	"56-SRBX on Clear",			"TMPSRBXC"	},	paramByte,	0},	
		{	{	57,	"57-SRBX on Set",			"TMPSRBXS"	},	paramByte,	0},
		{	{	58,	"58-TMP Alarm Code",			"TMPALARM"	},	paramByte,	1},
		{	{	59,	"59-Calibrate Command",			"CALCMD"	},	paramByte,	0},
		{	{	60,	"60-Calibrate Type",			"CALTYPE"	},	paramByte,	0},
		{	{	61,	"61-Calibrate Set Value",		"CALVAL"	},	paramReal,	0},	
		{	{	62,	"62-Sensor SRBX on Clear",		"SRBXCLR"	},	paramByte,	0},
		{	{	63,	"63-Sensor SRBX on Set",		"SRBXSET"	},	paramByte,	0},
		{	{	64,	"64-Pressure Offset",			"SPOFFSET"	},	paramReal,	0},
		{	{	65,	"65-Sensor Type",			"SENSTYPE"	},	paramByte,	1},
		{	{	 0,	"",					""		},	paramNone,	0},
		},
	109,	// System Analog Inputs
	{	
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Units Tag",				"UNITS"			},	paramString,	0},
		{	{	2,	"2-Scanning",				"SCANEN"		},	paramByte,	0},
		{	{	3,	"3-Scan Period",			"SCANPR"		},	paramReal,	0},
		{	{	4,	"4-Actual Scan Time",			"SCAN"			},	paramReal,	1},
		{	{	5,	"5-Filter",				"FILTER"		},	paramByte,	0},
		{	{	6,	"6-Averaging",				"AVGEN"			},	paramByte,	0},
		{	{	7,	"7-Raw AD Input",			"CURRAW"		},	paramWord,	0},
		{	{	8,	"8-Zero Raw",				"MINRAW"		},	paramWord,	1},
		{	{	9,	"9-Span Raw",				"MAXRAW"		},	paramWord,	1},
		{	{      10,	"10-Zero EU",				"MINEU"			},	paramReal,	1},
		{	{      11,	"11-Span EU",				"MAXEU"			},	paramReal,	1},
		{	{      12,	"12-EU Value",				"EU"			},	paramReal,	0},
		{	{      13,	"13-Clipping",				"CLIPEN"		},	paramByte,	0},
		{	{      14,	"14-Low Low Alarm EU",			"LOLOAL"		},	paramReal,	0},
		{	{      15,	"15-Low Alarm EU",			"LOAL"			},	paramReal,	0},
		{	{      16,	"16-High Alarm EU",			"HIAL"			},	paramReal,	0},
		{	{      17,	"17-High High Alarm EU",		"HIHIAL"		},	paramReal,	0},
		{	{      18,	"18-Rate Alarm EU",			"RATEAL"		},	paramReal,	0},
		{	{      19,	"19-Alarm Deadband",			"ALDBND"		},	paramReal,	0},
		{	{      20,	"20-Alarming",				"ALEN"			},	paramByte,	0},
		{	{      21,	"21-SRBX on Clear",			"SRBXCLR"		},	paramByte,	0},
		{	{      22,	"22-SRBX on Set",			"SRBXSET"		},	paramByte,	0},
		{	{      23,	"23-Alarm Code",			"ALARM"			},	paramByte,	1},
		{	{      24,	"24-Units",				"UNITMODE"		},	paramByte,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	117,	// Modbus Configuration Parameters
	{
		{	{	0,	"0-Transmission Mode",			"TRANSMOD"		},	paramByte,	0},
		{	{	1,	"1-Byte Order",				"BYTE"			},	paramByte,	0},
		{	{	2,	"2-Event Log Enable",			"EVENT"			},	paramByte,	0},
		{	{	3,	"3-Slave Exception Status",		"SLAVESTA"		},	paramByte,	1},
		{	{	4,	"4-Master Poll Request Trigger",	"MSTRPOLL"		},	paramByte,	0},
		{	{	5,	"5-Master Starting Request Number",	"MSTRSTAR"		},	paramWord,	0},
		{	{	6,	"6-Master Number of Requests",		"MSTRRQST"		},	paramWord,	0},
		{	{	7,	"7-Master Continuous Polling Enable",	"MSTRCONT"		},	paramByte,	0},
		{	{	8,	"8-Master Poll Request Delay",		"MSTRDLAY"		},	paramReal,	0},
		{	{	9,	"",					""			},	paramNone,	0},
		{	{      10,	"10-Low Integer Scale",			"LOINT"			},	paramWord,	0},
		{	{      11,	"11-High Integer Scale",		"HIGHINT"		},	paramWord,	0},
		{	{      12,	"12-Low Float Scale 1",			"LFLT1"			},	paramReal,	0},
		{	{      13,	"13-High Float Scale 1",		"HFLT1"			},	paramReal,	0},
		{	{      14,	"14-Low Float Scale 2",			"LFLT2"			},	paramReal,	0},
		{	{      15,	"15-High Float Scale 2",		"HFLT2"			},	paramReal,	0},
		{	{      16,	"16-Low Float Scale 3",			"LFLT3"			},	paramReal,	0},
		{	{      17,	"17-High Float Scale 3",		"HFLT3"			},	paramReal,	0},
		{	{      18,	"18-Low Float Scale 4",			"LFLT4"			},	paramReal,	0},
		{	{      19,	"19-High Float Scale 4",		"HFLT4"			},	paramReal,	0},
		{	{      20,	"20-Low Float Scale 5",			"LFLT5"			},	paramReal,	0},
		{	{      21,	"21-High Float Scale 5",		"HFLT5"			},	paramReal,	0},
		{	{      22,	"22-Low Float Scale 6",			"LFLT6"			},	paramReal,	0},
		{	{      23,	"23-High Float Scale 6",		"HFLT6"			},	paramReal,	0},
		{	{      24,	"24-Low Float Scale 7",			"LFLT7"			},	paramReal,	0},
		{	{      25,	"25-High Float Scale 7",		"HFLT7"			},	paramReal,	0},
		{	{      26,	"26-Low Float Scale 8",			"LFLT8"			},	paramReal,	0},
		{	{      27,	"27-High Float Scale 8",		"HFLT8"			},	paramReal,	0},
		{	{      28,	"28-Master Poll Timeout",		"TIMEOUT"		},	paramByte,	0},
		{	{      29,	"29-Master Poll Number of Retries"	"RETRIES"		},	paramByte,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	118,	// Modbus Register to TLP Mapping
	{
		{	{	0,	"0-Point Tag ID",			"MBMAP"			},	paramString,	0},
		{	{	1,	"1-Start Register #1",			"START1"		},	paramWord,	0},
		{	{	2,	"2-End Register #1",			"END1"			},	paramWord,	0},
		{	{	3,	"3-ROC Parameter(s)",			"PARA1"			},	paramTLP,	0},
		{	{	4,	"4-Indexing",				"INDEX1"		},	paramByte,	0},
		{	{	5,	"5-Conversion Code",			"CONV1"			},	paramByte,	0},
		{	{	6,	"6-Comm Port #1",			"COMPRT1"		},	paramByte,	0},
		{	{	7,	"7-Start Register #2",			"START2"		},	paramWord,	0},
		{	{	8,	"8-End Register #2",			"END2"			},	paramWord,	0},
		{	{	9,	"9-ROC Parameter(s)",			"PARA2"			},	paramTLP,	0},
		{	{	10,	"10-Indexing",				"INDEX2"		},	paramByte,	0},
		{	{	11,	"11-Conversion Code",			"CONV2"			},	paramByte,	0},
		{	{	12,	"12-Comm Port #2",			"COMPRT2"		},	paramByte,	0},
		{	{	13,	"13-Start Register #3",			"START3"		},	paramWord,	0},
		{	{	14,	"14-End Register #3",			"END3"			},	paramWord,	0},
		{	{	15,	"15-ROC Parameter(s)",			"PARA3"			},	paramTLP,	0},
		{	{	16,	"16-Indexing",				"INDEX3"		},	paramByte,	0},
		{	{	17,	"17-Conversion Code",			"CONV3"			},	paramByte,	0},
		{	{	18,	"18-Comm Port #3",			"COMPRT3"		},	paramByte,	0},
		{	{	19,	"19-Start Register #4",			"START4"		},	paramWord,	0},
		{	{	20,	"20-End Register #4",			"END4"			},	paramWord,	0},
		{	{	21,	"21-ROC Parameter(s)",			"PARA4"			},	paramTLP,	0},
		{	{	22,	"22-Indexing",				"INDEX4"		},	paramByte,	0},
		{	{	23,	"23-Conversion Code",			"CONV4"			},	paramByte,	0},
		{	{	24,	"24-Comm Port #4",			"COMPRT4"		},	paramByte,	0},
		{	{	25,	"25-Start Register #5",			"START5"		},	paramWord,	0},
		{	{	26,	"26-End Register #5",			"END5"			},	paramWord,	0},
		{	{	27,	"27-ROC Parameter(s)",			"PARA5"			},	paramTLP,	0},
		{	{	28,	"28-Indexing",				"INDEX5"		},	paramByte,	0},
		{	{	29,	"29-Conversion Code",			"CONV5"			},	paramByte,	0},
		{	{	30,	"30-Comm Port #5",			"COMPRT5"		},	paramByte,	0},
		{	{	31,	"31-Start Register #6",			"START6"		},	paramWord,	0},
		{	{	32,	"32-End Register #6",			"END6"			},	paramWord,	0},
		{	{	33,	"33-ROC Parameter(s)",			"PARA6"			},	paramTLP,	0},
		{	{	34,	"34-Indexing",				"INDEX6"		},	paramByte,	0},
		{	{	35,	"35-Conversion Code",			"CONV6"			},	paramByte,	0},
		{	{	36,	"36-Comm Port #6",			"COMPRT6"		},	paramByte,	0},
		{	{	37,	"37-Start Register #7",			"START7"		},	paramWord,	0},
		{	{	38,	"38-End Register #7",			"END7"			},	paramWord,	0},
		{	{	39,	"39-ROC Parameter(s)",			"PARA7"			},	paramTLP,	0},
		{	{	40,	"40-Indexing",				"INDEX7"		},	paramByte,	0},
		{	{	41,	"41-Conversion Code",			"CONV7"			},	paramByte,	0},
		{	{	42,	"42-Comm Port #7",			"COMPRT7"		},	paramByte,	0},
		{	{	43,	"43-Start Register #8",			"START8"		},	paramWord,	0},
		{	{	44,	"44-End Register #8",			"END8"			},	paramWord,	0},
		{	{	45,	"45-ROC Parameter(s)",			"PARA8"			},	paramTLP,	0},
		{	{	46,	"46-Indexing",				"INDEX8"		},	paramByte,	0},
		{	{	47,	"47-Conversion Code",			"CONV8"			},	paramByte,	0},
		{	{	48,	"48-Comm Port #8",			"COMPRT8"		},	paramByte,	0},
		{	{	49,	"49-Start Register #9",			"START9"		},	paramWord,	0},
		{	{	50,	"50-End Register #9",			"END9"			},	paramWord,	0},
		{	{	51,	"51-ROC Parameter(s)",			"PARA9"			},	paramTLP,	0},
		{	{	52,	"52-Indexing",				"INDEX9"		},	paramByte,	0},
		{	{	53,	"53-Conversion Code",			"CONV9"			},	paramByte,	0},
		{	{	54,	"54-Comm Port #9",			"COMPRT9"		},	paramByte,	0},
		{	{	55,	"55-Start Register #10",		"START10"		},	paramWord,	0},
		{	{	56,	"56-End Register #10",			"END10"			},	paramWord,	0},
		{	{	57,	"57-ROC Parameter(s)",			"PARA10"		},	paramTLP,	0},
		{	{	58,	"58-Indexing",				"INDEX10"		},	paramByte,	0},
		{	{	59,	"59-Conversion Code",			"CONV10"		},	paramByte,	0},
		{	{	60,	"60-Comm Port #10",			"COMPRT10"		},	paramByte,	0},
		{	{	61,	"61-Start Register #11",		"START11"		},	paramWord,	0},
		{	{	62,	"62-End Register #11",			"END11"			},	paramWord,	0},
		{	{	63,	"63-ROC Parameter(s)",			"PARA11"		},	paramTLP,	0},
		{	{	64,	"64-Indexing",				"INDEX11"		},	paramByte,	0},
		{	{	65,	"65-Conversion Code",			"CONV11"		},	paramByte,	0},
		{	{	66,	"66-Comm Port #11",			"COMPRT11"		},	paramByte,	0},
		{	{	67,	"67-Start Register #12",		"START12"		},	paramWord,	0},
		{	{	68,	"68-End Register #12",			"END12"			},	paramWord,	0},
		{	{	69,	"69-ROC Parameter(s)",			"PARA12"		},	paramTLP,	0},
		{	{	70,	"70-Indexing",				"INDEX12"		},	paramByte,	0},
		{	{	71,	"71-Conversion Code",			"CONV12"		},	paramByte,	0},
		{	{	72,	"72-Comm Port #12",			"COMPRT12"		},	paramByte,	0},
		{	{	73,	"73-Start Register #13",		"START13"		},	paramWord,	0},
		{	{	74,	"74-End Register #13",			"END13"			},	paramWord,	0},
		{	{	75,	"75-ROC Parameter(s)",			"PARA13"		},	paramTLP,	0},
		{	{	76,	"76-Indexing",				"INDEX13"		},	paramByte,	0},
		{	{	77,	"77-Conversion Code",			"CONV13"		},	paramByte,	0},
		{	{	78,	"78-Comm Port #13",			"COMPRT13"		},	paramByte,	0},
		{	{	79,	"79-Start Register #14",		"START14"		},	paramWord,	0},
		{	{	80,	"80-End Register #14",			"END14"			},	paramWord,	0},
		{	{	81,	"81-ROC Parameter(s)",			"PARA14"		},	paramTLP,	0},
		{	{	82,	"82-Indexing",				"INDEX14"		},	paramByte,	0},
		{	{	83,	"83-Conversion Code",			"CONV14"		},	paramByte,	0},
		{	{	84,	"84-Comm Port #14",			"COMPRT14"		},	paramByte,	0},
		{	{	85,	"85-Start Register #15",		"START15"		},	paramWord,	0},
		{	{	86,	"86-End Register #15",			"END15"			},	paramWord,	0},
		{	{	87,	"87-ROC Parameter(s)",			"PARA15"		},	paramTLP,	0},
		{	{	88,	"88-Indexing",				"INDEX15"		},	paramByte,	0},
		{	{	89,	"89-Conversion Code",			"CONV15"		},	paramByte,	0},
		{	{	90,	"90-Comm Port #15",			"COMPRT15"		},	paramByte,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	119,	// Modbus Event, Alarm, and History
	{
		{	{	0,	"0-Event Alarm Register",		"EVTALRM"		},	paramWord,	0},
		{	{	1,	"1-Current Date Register",		"CURDATE"		},	paramWord,	0},
		{	{	2,	"2-Current Time Register",		"CURTIME"		},	paramWord,	0},
		{	{	3,	"3-Periodic History Register #1",	"PR1"			},	paramWord,	0},
		{	{	4,	"4-Daily History Register #1",		"DLY1"			},	paramWord,	0},
		{	{	5,	"5-History Segment",			"HS1"			},	paramWord,	0},
		{	{	6,	"6-Start History Point",		"START1"		},	paramWord,	0},
		{	{	7,	"7-End History Point",			"END1"			},	paramWord,	0},
		{	{	8,	"8-Conversion Code",			"CONV1"			},	paramByte,	0},
		{	{	9,	"9-Periodic History Register #2",	"PR2"			},	paramWord,	0},
		{	{	10,	"10-Daily History Register #2",		"DLY2"			},	paramWord,	0},
		{	{	11,	"11-History Segment",			"HS2"			},	paramWord,	0},
		{	{	12,	"12-Start History Point",		"START2"		},	paramWord,	0},
		{	{	13,	"13-End History Point",			"END2"			},	paramWord,	0},
		{	{	14,	"14-Conversion Code",			"CONV2"			},	paramByte,	0},
		{	{	15,	"15-Periodic History Register #3",	"PR3"			},	paramWord,	0},
		{	{	16,	"16-Daily History Register #3",		"DLY3"			},	paramWord,	0},
		{	{	17,	"17-History Segment",			"HS3"			},	paramWord,	0},
		{	{	18,	"18-Start History Point",		"START3"		},	paramWord,	0},
		{	{	19,	"19-End History Point",			"END3"			},	paramWord,	0},
		{	{	20,	"20-Conversion Code",			"CONV3"			},	paramByte,	0},
		{	{	21,	"21-Periodic History Register #4",	"PR4"			},	paramWord,	0},
		{	{	22,	"22-Daily History Register #4",		"DLY4"			},	paramWord,	0},
		{	{	23,	"23-History Segment",			"HS4"			},	paramWord,	0},
		{	{	24,	"24-Start History Point",		"START4"		},	paramWord,	0},
		{	{	25,	"25-End History Point",			"END4"			},	paramWord,	0},
		{	{	26,	"26-Conversion Code",			"CONV4"			},	paramByte,	0},
		{	{	27,	"27-Periodic History Register #5",	"PR5"			},	paramWord,	0},
		{	{	28,	"28-Daily History Register #5",		"DLY5"			},	paramWord,	0},
		{	{	29,	"29-History Segment",			"HS5"			},	paramWord,	0},
		{	{	30,	"30-Start History Point",		"START5"		},	paramWord,	0},
		{	{	31,	"31-End History Point",			"END5"			},	paramWord,	0},
		{	{	32,	"32-Conversion Code",			"CONV5"			},	paramByte,	0},
		{	{	33,	"33-Periodic History Register #6",	"PR6"			},	paramWord,	0},
		{	{	34,	"34-Daily History Register #6",		"DLY6"			},	paramWord,	0},
		{	{	35,	"35-History Segment",			"HS6"			},	paramWord,	0},
		{	{	36,	"36-Start History Point",		"START6"		},	paramWord,	0},
		{	{	37,	"37-End History Point",			"END6"			},	paramWord,	0},
		{	{	38,	"38-Conversion Code",			"CONV6"			},	paramByte,	0},
		{	{	39,	"39-Periodic History Register #7",	"PR7"			},	paramWord,	0},
		{	{	40,	"40-Daily History Register #7",		"DLY7"			},	paramWord,	0},
		{	{	41,	"41-History Segment",			"HS7"			},	paramWord,	0},
		{	{	42,	"42-Start History Point",		"START7"		},	paramWord,	0},
		{	{	43,	"43-End History Point",			"END7"			},	paramWord,	0},
		{	{	44,	"44-Conversion Code",			"CONV7"			},	paramByte,	0},
		{	{	45,	"45-Periodic History Register #8",	"PR8"			},	paramWord,	0},
		{	{	46,	"46-Daily History Register #8",		"DLY8"			},	paramWord,	0},
		{	{	47,	"47-History Segment",			"HS8"			},	paramWord,	0},
		{	{	48,	"48-Start History Point",		"START8"		},	paramWord,	0},
		{	{	49,	"49-End History Point",			"END8"			},	paramWord,	0},
		{	{	50,	"50-Conversion Code",			"CONV8"			},	paramByte,	0},
		{	{	51,	"51-Periodic History Register #9",	"PR9"			},	paramWord,	0},
		{	{	52,	"52-Daily History Register #9",		"DLY9"			},	paramWord,	0},
		{	{	53,	"53-History Segment",			"HS9"			},	paramWord,	0},
		{	{	54,	"54-Start History Point",		"START9"		},	paramWord,	0},
		{	{	55,	"55-End History Point",			"END9"			},	paramWord,	0},
		{	{	56,	"56-Conversion Code",			"CONV9"			},	paramByte,	0},
		{	{	57,	"57-Periodic History Register #10",	"PR10"			},	paramWord,	0},
		{	{	58,	"58-Daily History Register #10",	"DLY10"			},	paramWord,	0},
		{	{	59,	"59-History Segment",			"HS10"			},	paramWord,	0},
		{	{	60,	"60-Start History Point",		"START10"		},	paramWord,	0},
		{	{	61,	"61-End History Point",			"END10"			},	paramWord,	0},
		{	{	62,	"62-Conversion Code",			"CONV10"		},	paramByte,	0},
		{	{	63,	"63-Periodic History Register #11",	"PR11"			},	paramWord,	0},
		{	{	64,	"64-Daily History Register #11",	"DLY11"			},	paramWord,	0},
		{	{	65,	"65-History Segment",			"HS11"			},	paramWord,	0},
		{	{	66,	"66-Start History Point",		"START11"		},	paramWord,	0},
		{	{	67,	"67-End History Point",			"END11"			},	paramWord,	0},
		{	{	68,	"68-Conversion Code",			"CONV11"		},	paramByte,	0},
		{	{	69,	"69-Periodic History Register #12",	"PR12"			},	paramWord,	0},
		{	{	70,	"70-Daily History Register #12",	"DLY12"			},	paramWord,	0},
		{	{	71,	"71-History Segment",			"HS12"			},	paramWord,	0},
		{	{	72,	"72-Start History Point",		"START12"		},	paramWord,	0},
		{	{	73,	"73-End History Point",			"END12"			},	paramWord,	0},
		{	{	74,	"74-Conversion Code",			"CONV12"		},	paramByte,	0},
		{	{	75,	"75-Periodic History Register #13",	"PR13"			},	paramWord,	0},
		{	{	76,	"76-Daily History Register #13",	"DLY13"			},	paramWord,	0},
		{	{	77,	"77-History Segment",			"HS13"			},	paramWord,	0},
		{	{	78,	"78-Start History Point",		"START13"		},	paramWord,	0},
		{	{	79,	"79-End History Point",			"END13"			},	paramWord,	0},
		{	{	80,	"80-Conversion Code",			"CONV13"		},	paramByte,	0},
		{	{	81,	"81-Periodic History Register #14",	"PR14"			},	paramWord,	0},
		{	{	82,	"82-Daily History Register #14",	"DLY14"			},	paramWord,	0},
		{	{	83,	"83-History Segment",			"HS14"			},	paramWord,	0},
		{	{	84,	"84-Start History Point",		"START14"		},	paramWord,	0},
		{	{	85,	"85-End History Point",			"END14"			},	paramWord,	0},
		{	{	86,	"86-Conversion Code",			"CONV14"		},	paramByte,	0},
		{	{	87,	"87-Periodic History Register #15",	"PR15"			},	paramWord,	0},
		{	{	88,	"88-Daily History Register #15",	"DLY15"			},	paramWord,	0},
		{	{	89,	"89-History Segment",			"HS15"			},	paramWord,	0},
		{	{	90,	"90-Start History Point",		"START15"		},	paramWord,	0},
		{	{	91,	"91-End History Point",			"END15"			},	paramWord,	0},
		{	{	92,	"92-Conversion Code",			"CONV15"		},	paramByte,	0},
		{	{	93,	"93-Periodic History Register #16",	"PR16"			},	paramWord,	0},
		{	{	94,	"94-Daily History Register #16",	"DLY16"			},	paramWord,	0},
		{	{	95,	"95-History Segment",			"HS16"			},	paramWord,	0},
		{	{	96,	"96-Start History Point",		"START16"		},	paramWord,	0},
		{	{	97,	"97-End History Point",			"END16"			},	paramWord,	0},
		{	{	98,	"98-Conversion Code",			"CONV16"		},	paramByte,	0},
		{	{	99,	"99-Periodic History Register #17",	"PR17"			},	paramWord,	0},
		{	{       100,	"100-Daily History Register #17",	"DLY17"			},	paramWord,	0},
		{	{       101,	"101-History Segment",			"HS17"			},	paramWord,	0},
		{	{	102,	"102-Start History Point",		"START17"		},	paramWord,	0},
		{	{	103,	"103-End History Point",		"END17"			},	paramWord,	0},
		{	{	104,	"104-Conversion Code",			"CONV17"		},	paramByte,	0},
		{	{	105,	"105-Periodic History Register #18",	"PR18"			},	paramWord,	0},
		{	{	106,	"106-Daily History Register #18",	"DLY18"			},	paramWord,	0},
		{	{	107,	"107-History Segment",			"HS18"			},	paramWord,	0},
		{	{	108,	"108-Start History Point",		"START18"		},	paramWord,	0},
		{	{	109,	"109-End History Point",		"END18"			},	paramWord,	0},
		{	{	110,	"110-Conversion Code",			"CONV18"		},	paramByte,	0},
		{	{	111,	"111-Periodic History Register #19",	"PR19"			},	paramWord,	0},
		{	{       112,	"112-Daily History Register #19",	"DLY19"			},	paramWord,	0},
		{	{       113,	"113-History Segment",			"HS19"			},	paramWord,	0},
		{	{	114,	"114-Start History Point",		"START19"		},	paramWord,	0},
		{	{	115,	"115-End History Point",		"END19"			},	paramWord,	0},
		{	{	116,	"116-Conversion Code",			"CONV19"		},	paramByte,	0},
		{	{	117,	"117-Periodic History Register #20",	"PR20"			},	paramWord,	0},
		{	{	118,	"118-Daily History Register #20",	"DLY20"			},	paramWord,	0},
		{	{	119,	"119-History Segment",			"HS20"			},	paramWord,	0},
		{	{	120,	"120-Start History Point",		"START20"		},	paramWord,	0},
		{	{	121,	"121-End History Point",		"END20"			},	paramWord,	0},
		{	{	122,	"122-Conversion Code",			"CONV20"		},	paramByte,	0},
		{	{	123,	"123-History Index Mode",		"HNDXMODE"		},	paramByte,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	120,	// Modbus Master Modem Configuration
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-1st RTU Address",			"ADD1"			},	paramByte,	0},
		{	{	2,	"2-1st Connect Command",		"CMD1"			},	paramString,	0},
		{	{	3,	"3-2nd RTU Address",			"ADD2"			},	paramByte,	0},
		{	{	4,	"4-2nd Connect Command",		"CMD2"			},	paramString,	0},
		{	{	5,	"5-3rd RTU Address",			"ADD3"			},	paramByte,	0},
		{	{	6,	"6-3rd Connect Command",		"CMD3"			},	paramString,	0},
		{	{	7,	"7-4th RTU Address",			"ADD4"			},	paramByte,	0},
		{	{	8,	"8-4th Connect Command",		"CMD4"			},	paramString,	0},
		{	{	9,	"9-5th RTU Address",			"ADD5"			},	paramByte,	0},
		{	{	10,	"10-5th Connect Command",		"CMD5"			},	paramString,	0},
		{	{	11,	"11-6th RTU Address",			"ADD6"			},	paramByte,	0},
		{	{	12,	"12-6th Connect Command",		"CMD6"			},	paramString,	0},		
		{	{	0,	"",					""			},	paramNone,	0},
		},
	121,	// Modbus Master Table
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-RTU 1 Address",			"ADD1"			},	paramByte,	0},
		{	{	2,	"2-Function Code Number",		"FUNC1"			},	paramByte,	0},
		{	{	3,	"3-Slave Register Number",		"SLVREG1"		},	paramWord,	0},
		{	{	4,	"4-Master Register Number",		"MSTREG1"		},	paramWord,	0},
		{	{	5,	"5-Number of Registers",		"NUMREG1"		},	paramByte,	0},
		{	{	6,	"6-Communication Status",		"STATUS1"		},	paramByte,	0},
		{	{	7,	"7-RTU 2 Address",			"ADD2"			},	paramByte,	0},
		{	{	8,	"8-Function Code Number",		"FUNC2"			},	paramByte,	0},
		{	{	9,	"9-Slave Register Number",		"SLVREG2"		},	paramWord,	0},
		{	{	10,	"10-Master Register Number",		"MSTREG2"		},	paramWord,	0},
		{	{	11,	"11-Number of Registers",		"NUMREG2"		},	paramByte,	0},
		{	{	12,	"12-Communication Status",		"STATUS2"		},	paramByte,	0},
		{	{	13,	"13-RTU 3 Address",			"ADD3"			},	paramByte,	0},
		{	{	14,	"14-Function Code Number",		"FUNC3"			},	paramByte,	0},
		{	{	15,	"15-Slave Register Number",		"SLVREG3"		},	paramWord,	0},
		{	{	16,	"16-Master Register Number",		"MSTREG3"		},	paramWord,	0},
		{	{	17,	"17-Number of Registers",		"NUMREG3"		},	paramByte,	0},
		{	{	18,	"18-Communication Status",		"STATUS3"		},	paramByte,	0},
		{	{	19,	"19-RTU 4 Address",			"ADD4"			},	paramByte,	0},
		{	{	20,	"20-Function Code Number",		"FUNC4"			},	paramByte,	0},
		{	{	21,	"21-Slave Register Number",		"SLVREG4"		},	paramWord,	0},
		{	{	22,	"22-Master Register Number",		"MSTREG4"		},	paramWord,	0},
		{	{	23,	"23-Number of Registers",		"NUMREG4"		},	paramByte,	0},
		{	{	24,	"24-Communication Status",		"STATUS4"		},	paramByte,	0},
		{	{	25,	"25-RTU 5 Address",			"ADD5"			},	paramByte,	0},
		{	{	26,	"26-Function Code Number",		"FUNC5"			},	paramByte,	0},
		{	{	27,	"27-Slave Register Number",		"SLVREG5"		},	paramWord,	0},
		{	{	28,	"28-Master Register Number",		"MSTREG5"		},	paramWord,	0},
		{	{	29,	"29-Number of Registers",		"NUMREG5"		},	paramByte,	0},
		{	{	30,	"30-Communication Status",		"STATUS5"		},	paramByte,	0},
		{	{	31,	"31-RTU 6 Address",			"ADD6"			},	paramByte,	0},
		{	{	32,	"32-Function Code Number",		"FUNC6"			},	paramByte,	0},
		{	{	33,	"33-Slave Register Number",		"SLVREG6"		},	paramWord,	0},
		{	{	34,	"34-Master Register Number",		"MSTREG6"		},	paramWord,	0},
		{	{	35,	"35-Number of Registers",		"NUMREG6"		},	paramByte,	0},
		{	{	36,	"36-Communication Status",		"STATUS6"		},	paramByte,	0},
		{	{	37,	"37-RTU 7 Address",			"ADD7"			},	paramByte,	0},
		{	{	38,	"38-Function Code Number",		"FUNC7"			},	paramByte,	0},
		{	{	39,	"39-Slave Register Number",		"SLVREG7"		},	paramWord,	0},
		{	{	40,	"40-Master Register Number",		"MSTREG7"		},	paramWord,	0},
		{	{	41,	"41-Number of Registers",		"NUMREG7"		},	paramByte,	0},
		{	{	42,	"42-Communication Status",		"STATUS7"		},	paramByte,	0},
		{	{	43,	"43-RTU 8 Address",			"ADD8"			},	paramByte,	0},
		{	{	44,	"44-Function Code Number",		"FUNC8"			},	paramByte,	0},
		{	{	45,	"45-Slave Register Number",		"SLVREG8"		},	paramWord,	0},
		{	{	46,	"46-Master Register Number",		"MSTREG8"		},	paramWord,	0},
		{	{	47,	"47-Number of Registers",		"NUMREG8"		},	paramByte,	0},
		{	{	48,	"48-Communication Status",		"STATUS8"		},	paramByte,	0},
		{	{	49,	"49-RTU 9 Address",			"ADD9"			},	paramByte,	0},
		{	{	50,	"50-Function Code Number",		"FUNC9"			},	paramByte,	0},
		{	{	51,	"51-Slave Register Number",		"SLVREG9"		},	paramWord,	0},
		{	{	52,	"52-Master Register Number",		"MSTREG9"		},	paramWord,	0},
		{	{	53,	"53-Number of Registers",		"NUMREG9"		},	paramByte,	0},
		{	{	54,	"54-Communication Status",		"STATUS9"		},	paramByte,	0},
		{	{	55,	"55-RTU 10 Address",			"ADD10"			},	paramByte,	0},
		{	{	56,	"56-Function Code Number",		"FUNC10"		},	paramByte,	0},
		{	{	57,	"57-Slave Register Number",		"SLVREG10"		},	paramWord,	0},
		{	{	58,	"58-Master Register Number",		"MSTREG10"		},	paramWord,	0},
		{	{	59,	"59-Number of Registers",		"NUMREG10"		},	paramByte,	0},
		{	{	60,	"60-Communication Status",		"STATUS10"		},	paramByte,	0},
		{	{	61,	"61-RTU 11 Address",			"ADD11"			},	paramByte,	0},
		{	{	62,	"62-Function Code Number",		"FUNC11"		},	paramByte,	0},
		{	{	63,	"63-Slave Register Number",		"SLVREG11"		},	paramWord,	0},
		{	{	64,	"64-Master Register Number",		"MSTREG11"		},	paramWord,	0},
		{	{	65,	"65-Number of Registers",		"NUMREG11"		},	paramByte,	0},
		{	{	66,	"66-Communication Status",		"STATUS11"		},	paramByte,	0},
		{	{	67,	"67-RTU 12 Address",			"ADD12"			},	paramByte,	0},
		{	{	68,	"68-Function Code Number",		"FUNC12"		},	paramByte,	0},
		{	{	69,	"69-Slave Register Number",		"SLVREG12"		},	paramWord,	0},
		{	{	70,	"70-Master Register Number",		"MSTREG12"		},	paramWord,	0},
		{	{	71,	"71-Number of Registers",		"NUMREG12"		},	paramByte,	0},
		{	{	72,	"72-Communication Status",		"STATUS12"		},	paramByte,	0},
		{	{	73,	"73-RTU 13 Address",			"ADD13"			},	paramByte,	0},
		{	{	74,	"74-Function Code Number",		"FUNC13"		},	paramByte,	0},
		{	{	75,	"75-Slave Register Number",		"SLVREG13"		},	paramWord,	0},
		{	{	76,	"76-Master Register Number",		"MSTREG13"		},	paramWord,	0},
		{	{	77,	"77-Number of Registers",		"NUMREG13"		},	paramByte,	0},
		{	{	78,	"78-Communication Status",		"STATUS13"		},	paramByte,	0},
		{	{	79,	"79-RTU 14 Address",			"ADD14"			},	paramByte,	0},
		{	{	80,	"80-Function Code Number",		"FUNC14"		},	paramByte,	0},
		{	{	81,	"81-Slave Register Number",		"SLVREG14"		},	paramWord,	0},
		{	{	82,	"82-Master Register Number",		"MSTREG14"		},	paramWord,	0},
		{	{	83,	"83-Number of Registers",		"NUMREG14"		},	paramByte,	0},
		{	{	84,	"84-Communication Status",		"STATUS14"		},	paramByte,	0},
		{	{	85,	"85-RTU 15 Address",			"ADD15"			},	paramByte,	0},
		{	{	86,	"86-Function Code Number",		"FUNC15"		},	paramByte,	0},
		{	{	87,	"87-Slave Register Number",		"SLVREG15"		},	paramWord,	0},
		{	{	88,	"88-Master Register Number",		"MSTREG15"		},	paramWord,	0},
		{	{	89,	"89-Number of Registers",		"NUMREG15"		},	paramByte,	0},
		{	{	90,	"90-Communication Status",		"STATUS15"		},	paramByte,	0},
		{	{	91,	"91-RTU 16 Address",			"ADD16"			},	paramByte,	0},
		{	{	92,	"92-Function Code Number",		"FUNC16"		},	paramByte,	0},
		{	{	93,	"93-Slave Register Number",		"SLVREG16"		},	paramWord,	0},
		{	{	94,	"94-Master Register Number",		"MSTREG16"		},	paramWord,	0},
		{	{	95,	"95-Number of Registers",		"NUMREG16"		},	paramByte,	0},
		{	{	96,	"96-Communication Status",		"STATUS16"		},	paramByte,	0},
		{	{	97,	"97-RTU 17 Address",			"ADD17"			},	paramByte,	0},
		{	{	98,	"98-Function Code Number",		"FUNC17"		},	paramByte,	0},
		{	{	99,	"99-Slave Register Number",		"SLVREG17"		},	paramWord,	0},
		{	{	100,	"100-Master Register Number",		"MSTREG17"		},	paramWord,	0},
		{	{	101,	"101-Number of Registers",		"NUMREG17"		},	paramByte,	0},
		{	{	102,	"102-Communication Status",		"STATUS17"		},	paramByte,	0},
		{	{	103,	"103-RTU 18 Address",			"ADD18"			},	paramByte,	0},
		{	{	104,	"104-Function Code Number",		"FUNC18"		},	paramByte,	0},
		{	{	105,	"105-Slave Register Number",		"SLVREG18"		},	paramWord,	0},
		{	{	106,	"106-Master Register Number",		"MSTREG18"		},	paramWord,	0},
		{	{	107,	"107-Number of Registers",		"NUMREG18"		},	paramByte,	0},
		{	{	108,	"108-Communication Status",		"STATUS18"		},	paramByte,	0},
		{	{	109,	"109-RTU 19 Address",			"ADD19"			},	paramByte,	0},
		{	{	110,	"110-Function Code Number",		"FUNC19"		},	paramByte,	0},
		{	{	111,	"111-Slave Register Number",		"SLVREG19"		},	paramWord,	0},
		{	{	112,	"112-Master Register Number",		"MSTREG19"		},	paramWord,	0},
		{	{	113,	"113-Number of Registers",		"NUMREG19"		},	paramByte,	0},
		{	{	114,	"114-Communication Status",		"STATUS19"		},	paramByte,	0},
		{	{	115,	"115-RTU 20 Address",			"ADD20"			},	paramByte,	0},
		{	{	116,	"116-Function Code Number",		"FUNC20"		},	paramByte,	0},
		{	{	117,	"117-Slave Register Number",		"SLVREG20"		},	paramWord,	0},
		{	{	118,	"118-Master Register Number",		"MSTREG20"		},	paramWord,	0},
		{	{	119,	"119-Number of Registers",		"NUMREG20"		},	paramByte,	0},
		{	{	120,	"120-Communication Status",		"STATUS20"		},	paramByte,	0},
		{	{	121,	"121-RTU 21 Address",			"ADD21"			},	paramByte,	0},
		{	{	122,	"122-Function Code Number",		"FUNC21"		},	paramByte,	0},
		{	{	123,	"123-Slave Register Number",		"SLVREG21"		},	paramWord,	0},
		{	{	124,	"124-Master Register Number",		"MSTREG21"		},	paramWord,	0},
		{	{	125,	"125-Number of Registers",		"NUMREG21"		},	paramByte,	0},
		{	{	126,	"126-Communication Status",		"STATUS21"		},	paramByte,	0},
		{	{	127,	"127-RTU 22 Address",			"ADD22"			},	paramByte,	0},
		{	{	128,	"128-Function Code Number",		"FUNC22"		},	paramByte,	0},
		{	{	129,	"129-Slave Register Number",		"SLVREG22"		},	paramWord,	0},
		{	{	130,	"130-Master Register Number",		"MSTREG22"		},	paramWord,	0},
		{	{	131,	"131-Number of Registers",		"NUMREG22"		},	paramByte,	0},
		{	{	132,	"132-Communication Status",		"STATUS22"		},	paramByte,	0},
		{	{	133,	"133-RTU 23 Address",			"ADD23"			},	paramByte,	0},
		{	{	134,	"134-Function Code Number",		"FUNC23"		},	paramByte,	0},
		{	{	135,	"135-Slave Register Number",		"SLVREG23"		},	paramWord,	0},
		{	{	136,	"136-Master Register Number",		"MSTREG23"		},	paramWord,	0},
		{	{	137,	"137-Number of Registers",		"NUMREG23"		},	paramByte,	0},
		{	{	138,	"138-Communication Status",		"STATUS23"		},	paramByte,	0},
		{	{	139,	"139-RTU 24 Address",			"ADD24"			},	paramByte,	0},
		{	{	140,	"140-Function Code Number",		"FUNC24"		},	paramByte,	0},
		{	{	141,	"141-Slave Register Number",		"SLVREG24"		},	paramWord,	0},
		{	{	142,	"142-Master Register Number",		"MSTREG24"		},	paramWord,	0},
		{	{	143,	"143-Number of Registers",		"NUMREG24"		},	paramByte,	0},
		{	{	144,	"144-Communication Status",		"STATUS24"		},	paramByte,	0},
		{	{	145,	"145-RTU 25 Address",			"ADD25"			},	paramByte,	0},
		{	{	146,	"146-Function Code Number",		"FUNC25"		},	paramByte,	0},
		{	{	147,	"147-Slave Register Number",		"SLVREG25"		},	paramWord,	0},
		{	{	148,	"148-Master Register Number",		"MSTREG25"		},	paramWord,	0},
		{	{	149,	"149-Number of Registers",		"NUMREG25"		},	paramByte,	0},
		{	{	150,	"150-Communication Status",		"STATUS25"		},	paramByte,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	122,	// DS800 Configuration
	{
		{	{	0,	"0-Power Switch",			"POWER"			},	paramByte,	0},
		{	{	1,	"1-RSI Enable",				"RSIEN"			},	paramByte,	0},
		{	{	2,	"2-ETCP Enable",			"ETCPEN"		},	paramByte,	0},
		{	{	3,	"3-IXD Enable",				"IXDEN"			},	paramByte,	0},
		{	{	4,	"4-RSI Running",			"RSIRUN"		},	paramByte,	1},
		{	{	5,	"5-ETCP Running",			"ETCPRUN"		},	paramByte,	1},
		{	{	6,	"6-IXD Running",			"IXDRUN"		},	paramByte,	1},
		{	{	7,	"7-Clean Stored Resources",		"CLRRES"		},	paramByte,	0},
		{	{	8,	"8-Resource 1 Name",			"RESNAME1"		},	paramString,	1},
		{	{	9,	"9-Resource 1 Status",			"RESSTAT1"		},	paramByte,	1},
		{	{      10,	"10-Resource 1 Programmed Cycle Time",	"DEFTIME1"		},	paramLong,	1},
		{	{      11,	"11-Resource 1 Current Cycle Time",	"ACTTIME1"		},	paramLong,	1},
		{	{      12,	"12-Resource 1 Name",			"RESNAME2"		},	paramString,	1},
		{	{      13,	"13-Resource 1 Status",			"RESSTAT2"		},	paramByte,	1},
		{	{      14,	"14-Resource 1 Programmed Cycle Time",	"DEFTIME2"		},	paramLong,	1},
		{	{      15,	"15-Resource 1 Current Cycle Time",	"ACTTIME2"		},	paramLong,	1},
		{	{      16,	"16-Resource 1 Name",			"RESNAME3"		},	paramString,	1},
		{	{      17,	"17-Resource 1 Status",			"RESSTAT3"		},	paramByte,	1},
		{	{      18,	"18-Resource 1 Programmed Cycle Time",	"DEFTIME3"		},	paramLong,	1},
		{	{      19,	"19-Resource 1 Current Cycle Time",	"ACTTIME3"		},	paramLong,	1},
		{	{      20,	"20-Resource 1 Name",			"RESNAME4"		},	paramString,	1},
		{	{      21,	"21-Resource 1 Status",			"RESSTAT4"		},	paramByte,	1},
		{	{      22,	"22-Resource 1 Programmed Cycle Time",	"DEFTIME4"		},	paramLong,	1},
		{	{      23,	"23-Resource 1 Current Cycle Time",	"ACTTIME4"		},	paramLong,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	123,	// Security - Group Configuration
	{
		{	{	0,	"0-Group #1",				"SECGRP1"		},	paramString,	0},
		{	{	1,	"1-Group #2",				"SECGRP2"		},	paramString,	0},
		{	{	2,	"2-Group #3",				"SECGRP3"		},	paramString,	0},
		{	{	3,	"3-Group #4",				"SECGRP4"		},	paramString,	0},
		{	{	4,	"4-Group #5",				"SECGRP5"		},	paramString,	0},
		{	{	5,	"5-Group #6",				"SECGRP6"		},	paramString,	0},
		{	{	6,	"6-Group #7",				"SECGRP7"		},	paramString,	0},
		{	{	7,	"7-Group #8",				"SECGRP8"		},	paramString,	0},
		{	{	8,	"8-Group #9",				"SECGRP9"		},	paramString,	0},
		{	{	9,	"9-Group #10",				"SECGRP10"		},	paramString,	0},
		{	{	10,	"10-Group #11",				"SECGRP11"		},	paramString,	0},
		{	{	11,	"11-Group #12",				"SECGRP12"		},	paramString,	0},
		{	{	12,	"12-Group #13",				"SECGRP13"		},	paramString,	0},
		{	{	13,	"13-Group #14",				"SECGRP14"		},	paramString,	0},
		{	{	14,	"14-Group #15",				"SECGRP15"		},	paramString,	0},
		{	{	15,	"15-Group #16",				"SECGRP16"		},	paramString,	0},
		{	{	16,	"16-Group #17",				"SECGRP17"		},	paramString,	0},
		{	{	17,	"17-Group #18",				"SECGRP18"		},	paramString,	0},
		{	{	18,	"18-Group #19",				"SECGRP19"		},	paramString,	0},
		{	{	19,	"19-Group #20",				"SECGRP20"		},	paramString,	0},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	124,	// History Segment Configuration
	{
		{	{	0,	"0-Segment Description",		"DESC"			},	paramString,	0},
		{	{	1,	"1-Segment Size",			"SIZE"			},	paramWord,	0},
		{	{	2,	"2-Maximum Segment Size",		"MAXSIZE"		},	paramWord,	1},
		{	{	3,	"3-Periodic Entries",			"ENTRIES"		},	paramWord,	0},
		{	{	4,	"4-Daily Entries",			"DAILY"			},	paramWord,	0},
		{	{	5,	"5-Periodic Index",			"PERINDEX"		},	paramWord,	1},
		{	{	6,	"6-Daily Index",			"DAYINDEX"		},	paramWord,	1},
		{	{	7,	"7-Periodic Sample Rate",		"PERRATE"		},	paramByte,	0},
		{	{	8,	"8-Contract Hour",			"CHOUR"			},	paramByte,	0},
		{	{	9,	"9-ON OFF Switch",			"ENABLE"		},	paramByte,	0},
		{	{      10,	"10-Free Space",			"FREE"			},	paramLong,	1},
		{	{      11,	"11-Force End of Day",			"FORCE"			},	paramByte,	0},
		{	{      12,	"12-Number of Configured Points",	"NCFGPTS"		},	paramWord,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	125,	// History Segment 0 Point Configuration
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	1},
		{	{	1,	"1-Parameter Description",		"DESC"			},	paramString,	0},
		{	{	2,	"2-History Log Point",			"POINT"			},	paramTLP,	0},
		{	{	3,	"3-Archive Type",			"TYPE"			},	paramByte,	0},
		{	{	4,	"4-Averaging Rate Type",		"METHOD"		},	paramByte,	0},
		{	{	5,	"5-Current Value",			"CURVAL"		},	paramReal,	1},
		{	{	6,	"6-Last Daily Value",			"YTDVAL"		},	paramReal,	1},
		{	{	7,	"7-Today Min Time",			"TDYMINTM"		},	paramLong,	1},
		{	{	8,	"8-Today Min Value",			"TDYMINVA"		},	paramReal,	1},
		{	{	9,	"9-Today Max Time",			"TDYMAXTM"		},	paramLong,	1},
		{	{      10,	"10-Today Max Value",			"TDYMAXVA"		},	paramReal,	1},
		{	{      11,	"11-Yesterday Min Time",		"YDYMINTM"		},	paramLong,	1},
		{	{      12,	"12-Yesterday Min Value",		"YDYMINVA"		},	paramReal,	1},
		{	{      13,	"13-Yesterday Max Time",		"YDYMAXTM"		},	paramLong,	1},
		{	{      14,	"14-Yesterday Max Value",		"YDYMAXVA"		},	paramReal,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	136,	// ROC Clock
	{
		{	{	0,	"0-Seconds",				"SECOND"		},	paramByte,	1},
		{	{	1,	"1-Minutes",				"MINUTE"		},	paramByte,	1},	
		{	{	2,	"2-Hours",				"HOUR"			},	paramByte,	1},
		{	{	3,	"3-Day",				"DAY"			},	paramByte,	1},
		{	{	4,	"4-Month",				"MONTH"			},	paramByte,	1},
		{	{	5,	"5-Year",				"YEAR"			},	paramWord,	1},
		{	{	6,	"6-Day of Week",			"DAYOWK"		},	paramByte,	1},
		{	{	7,	"7-Time",				"TIME"			},	paramLong,	1},
		{	{	8,      "8-Daylight Savings Enable",		"DLST"			},	paramByte,	0},
		{	{	9,      "9-Microseconds",			"MICROSEC"		},	paramLong,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	137,	// Internet Configuration Parameters
	{
		{	{	0,	"0-MAC Address",			"MAC"			},	paramString,	1},
		{	{	1,	"1-IP Address",				"IP"			},	paramString,	0},
		{	{	2,	"2-Subnet Mask",			"SUB"			},	paramString,	0},
		{	{	3,	"3-Gateway Address",			"GATE"			},	paramString,	0},
		{	{	4,	"4-ROC Protocol IP Port Number",	"PORT"			},	paramWord,	0},
		{	{	5,	"5-Num Active ROC Protocol Connections","CONNS"			},	paramByte,	1},
		{	{	6,	"6-ROC Protocol Inactivity Time",	"INACTM"		},	paramReal,	0},
		{	{	7,	"7-Reset ROC Protocol Connections",	"RESETCON"		},	paramByte,	0},
		{	{	8,	"8-ROC Protocol Keep Alive Time",	"ALIVETM"		},	paramLong,	0},
		{	{	9,	"9-Modbus IP Port Number",		"MBIPPORT"		},	paramWord,	0},
		{	{      10,	"10-Current Modbus Connections",	"MBCURCON"		},	paramWord,	1},
		{	{      11,	"11-Modbus Inactivity Time",		"MBINACTM"		},	paramReal,	0},
		{	{      12,	"12-Reset Modbus Connections",		"MBCONRES"		},	paramByte,	0},
		{	{      13,	"13-Modbus Keep Alive Time",		"MBALIVTM"		},	paramLong,	0},
		{	{      14,	"14-Modbus Over TCP Address To Use",	"MBTCPUSE"		},	paramByte,	0},
		{	{      15,	"15-Modbus Over TCP Slave Address",	"MBTCPSLV"		},	paramByte,	0},
		{	{      16,	"16-ARP Protection Enable",		"ARPSTORM"		},	paramByte,	0},
		{	{      17,	"17-ARP Packet Queue Limit",		"ARPQLIM"		},	paramLong,	0},
		{	{      18,	"18-Table 1 TCP Option",		"T1TCPOP"		},	paramByte,	0},
		{	{      19,	"19-Table 1 Server 1 IP Address",	"T1S1IP"		},	paramLong,	0},
		{	{      20,	"20-Table 1 Server 1 Port Number",	"T1S1PRT"		},	paramWord,	0},
		{	{      21,	"21-Table 1 Server 2 IP Address",	"T1S2IP"		},	paramLong,	0},
		{	{      22,	"22-Table 1 Server 2 Port Number",	"T1S2PRT"		},	paramWord,	0},
		{	{      23,	"23-Table 1 Server 3 IP Address",	"T1S3IP"		},	paramLong,	0},
		{	{      24,	"24-Table 1 Server 3 Port Number",	"T1S3PRT"		},	paramWord,	0},
		{	{      25,	"25-Table 1 Server 4 IP Address",	"T1S4IP"		},	paramLong,	0},
		{	{      26,	"26-Table 1 Server 4 Port Number",	"T1S4PRT"		},	paramWord,	0},
		{	{      27,	"27-Table 1 Server 5 IP Address",	"T1S5IP"		},	paramLong,	0},
		{	{      28,	"28-Table 1 Server 5 Port Number",	"T1S5PRT"		},	paramWord,	0},
		{	{      29,	"29-Table 1 Server 6 IP Address",	"T1S6IP"		},	paramLong,	0},
		{	{      30,	"30-Table 1 Server 6 Port Number",	"T1S6PRT"		},	paramWord,	0},
		{	{      31,	"31-Table 1 Server 7 IP Address",	"T1S7IP"		},	paramLong,	0},
		{	{      32,	"32-Table 1 Server 7 Port Number",	"T1S7PRT"		},	paramWord,	0},
		{	{      33,	"33-Table 1 Server 8 IP Address",	"T1S8IP"		},	paramLong,	0},
		{	{      34,	"34-Table 1 Server 8 Port Number",	"T1S8PRT"		},	paramWord,	0},
		{	{      35,	"35-Table 1 Server 9 IP Address",	"T1S9IP"		},	paramLong,	0},
		{	{      36,	"36-Table 1 Server 9 Port Number",	"T1S9PRT"		},	paramWord,	0},
		{	{      37,	"37-Table 1 Server 10 IP Address",	"T1S10IP"		},	paramLong,	0},
		{	{      38,	"38-Table 1 Server 10 Port Number",	"T1S10PRT"		},	paramWord,	0},
		{	{      39,	"39-Table 1 Server 11 IP Address",	"T1S11IP"		},	paramLong,	0},
		{	{      40,	"40-Table 1 Server 11 Port Number",	"T1S11PRT"		},	paramWord,	0},
		{	{      41,	"41-Table 1 Server 12 IP Address",	"T1S12IP"		},	paramLong,	0},
		{	{      42,	"42-Table 1 Server 12 Port Number",	"T1S12PRT"		},	paramWord,	0},
		{	{      43,	"43-Table 1 Server 13 IP Address",	"T1S13IP"		},	paramLong,	0},
		{	{      44,	"44-Table 1 Server 13 Port Number",	"T1S13PRT"		},	paramWord,	0},
		{	{      45,	"45-Table 1 Server 14 IP Address",	"T1S14IP"		},	paramLong,	0},
		{	{      46,	"46-Table 1 Server 14 Port Number",	"T1S14PRT"		},	paramWord,	0},
		{	{      47,	"47-Table 1 Server 15 IP Address",	"T1S15IP"		},	paramLong,	0},
		{	{      48,	"48-Table 1 Server 15 Port Number",	"T1S15PRT"		},	paramWord,	0},
		{	{      49,	"49-Table 1 Server 16 IP Address",	"T1S16IP"		},	paramLong,	0},
		{	{      50,	"50-Table 1 Server 16 Port Number",	"T1S16PRT"		},	paramWord,	0},
		{	{      51,	"51-Table 1 Server 17 IP Address",	"T1S17IP"		},	paramLong,	0},
		{	{      52,	"52-Table 1 Server 17 Port Number",	"T1S17PRT"		},	paramWord,	0},
		{	{      53,	"53-Table 1 Server 18 IP Address",	"T1S18IP"		},	paramLong,	0},
		{	{      54,	"54-Table 1 Server 18 Port Number",	"T1S18PRT"		},	paramWord,	0},
		{	{      55,	"55-Table 1 Server 19 IP Address",	"T1S19IP"		},	paramLong,	0},
		{	{      56,	"56-Table 1 Server 19 Port Number",	"T1S19PRT"		},	paramWord,	0},
		{	{      57,	"57-Table 1 Server 20 IP Address",	"T1S20IP"		},	paramLong,	0},
		{	{      58,	"58-Table 1 Server 20 Port Number",	"T1S20PRT"		},	paramWord,	0},
		{	{      59,	"59-Table 1 Server 21 IP Address",	"T1S21IP"		},	paramLong,	0},
		{	{      60,	"60-Table 1 Server 21 Port Number",	"T1S21PRT"		},	paramWord,	0},
		{	{      61,	"61-Table 1 Server 22 IP Address",	"T1S22IP"		},	paramLong,	0},
		{	{      62,	"62-Table 1 Server 22 Port Number",	"T1S22PRT"		},	paramWord,	0},
		{	{      63,	"63-Table 1 Server 23 IP Address",	"T1S23IP"		},	paramLong,	0},
		{	{      64,	"64-Table 1 Server 23 Port Number",	"T1S23PRT"		},	paramWord,	0},
		{	{      65,	"65-Table 1 Server 24 IP Address",	"T1S24IP"		},	paramLong,	0},
		{	{      66,	"66-Table 1 Server 24 Port Number",	"T1S24PRT"		},	paramWord,	0},
		{	{      67,	"67-Table 1 Server 25 IP Address",	"T1S25IP"		},	paramLong,	0},
		{	{      68,	"68-Table 1 Server 25 Port Number",	"T1S25PRT"		},	paramWord,	0},
		{	{      69,	"69-Table 2 TCP Option",		"T2TCPOP"		},	paramByte,	0},
		{	{      70,	"70-Table 2 Server 1 IP Address",	"T2S1IP"		},	paramLong,	0},
		{	{      71,	"71-Table 2 Server 1 Port Number",	"T2S1PRT"		},	paramWord,	0},
		{	{      72,	"72-Table 2 Server 2 IP Address",	"T2S2IP"		},	paramLong,	0},
		{	{      73,	"73-Table 2 Server 2 Port Number",	"T2S2PRT"		},	paramWord,	0},
		{	{      74,	"74-Table 2 Server 3 IP Address",	"T2S3IP"		},	paramLong,	0},
		{	{      75,	"75-Table 2 Server 3 Port Number",	"T2S3PRT"		},	paramWord,	0},
		{	{      76,	"76-Table 2 Server 4 IP Address",	"T2S4IP"		},	paramLong,	0},
		{	{      77,	"77-Table 2 Server 4 Port Number",	"T2S4PRT"		},	paramWord,	0},
		{	{      78,	"78-Table 2 Server 5 IP Address",	"T2S5IP"		},	paramLong,	0},
		{	{      79,	"79-Table 2 Server 5 Port Number",	"T2S5PRT"		},	paramWord,	0},
		{	{      80,	"80-Table 2 Server 6 IP Address",	"T2S6IP"		},	paramLong,	0},
		{	{      81,	"81-Table 2 Server 6 Port Number",	"T2S6PRT"		},	paramWord,	0},
		{	{      82,	"82-Table 2 Server 7 IP Address",	"T2S7IP"		},	paramLong,	0},
		{	{      83,	"83-Table 2 Server 7 Port Number",	"T2S7PRT"		},	paramWord,	0},
		{	{      84,	"84-Table 2 Server 8 IP Address",	"T2S8IP"		},	paramLong,	0},
		{	{      85,	"85-Table 2 Server 8 Port Number",	"T2S8PRT"		},	paramWord,	0},
		{	{      86,	"86-Table 2 Server 9 IP Address",	"T2S9IP"		},	paramLong,	0},
		{	{      87,	"87-Table 2 Server 9 Port Number",	"T2S9PRT"		},	paramWord,	0},
		{	{      88,	"88-Table 2 Server 10 IP Address",	"T2S10IP"		},	paramLong,	0},
		{	{      89,	"89-Table 2 Server 10 Port Number",	"T2S10PRT"		},	paramWord,	0},
		{	{      90,	"90-Table 2 Server 11 IP Address",	"T2S11IP"		},	paramLong,	0},
		{	{      91,	"91-Table 2 Server 11 Port Number",	"T2S11PRT"		},	paramWord,	0},
		{	{      92,	"92-Table 2 Server 12 IP Address",	"T2S12IP"		},	paramLong,	0},
		{	{      93,	"93-Table 2 Server 12 Port Number",	"T2S12PRT"		},	paramWord,	0},
		{	{      94,	"94-Table 2 Server 13 IP Address",	"T2S13IP"		},	paramLong,	0},
		{	{      95,	"95-Table 2 Server 13 Port Number",	"T2S13PRT"		},	paramWord,	0},
		{	{      96,	"96-Table 2 Server 14 IP Address",	"T2S14IP"		},	paramLong,	0},
		{	{      97,	"97-Table 2 Server 14 Port Number",	"T2S14PRT"		},	paramWord,	0},
		{	{      98,	"98-Table 2 Server 15 IP Address",	"T2S15IP"		},	paramLong,	0},
		{	{      99,	"99-Table 2 Server 15 Port Number",	"T2S15PRT"		},	paramWord,	0},
		{	{     100,	"100-Table 2 Server 16 IP Address",	"T2S16IP"		},	paramLong,	0},
		{	{     101,	"101-Table 2 Server 16 Port Number",	"T2S16PRT"		},	paramWord,	0},
		{	{     102,	"102-Table 2 Server 17 IP Address",	"T2S17IP"		},	paramLong,	0},
		{	{     103,	"103-Table 2 Server 17 Port Number",	"T2S17PRT"		},	paramWord,	0},
		{	{     104,	"104-Table 2 Server 18 IP Address",	"T2S18IP"		},	paramLong,	0},
		{	{     105,	"105-Table 2 Server 18 Port Number",	"T2S18PRT"		},	paramWord,	0},
		{	{     106,	"106-Table 2 Server 19 IP Address",	"T2S19IP"		},	paramLong,	0},
		{	{     107,	"107-Table 2 Server 19 Port Number",	"T2S19PRT"		},	paramWord,	0},
		{	{     108,	"108-Table 2 Server 20 IP Address",	"T2S20IP"		},	paramLong,	0},
		{	{     109,	"109-Table 2 Server 20 Port Number",	"T2S20PRT"		},	paramWord,	0},
		{	{     110,	"110-Table 2 Server 21 IP Address",	"T2S21IP"		},	paramLong,	0},
		{	{     111,	"111-Table 2 Server 21 Port Number",	"T2S21PRT"		},	paramWord,	0},
		{	{     112,	"112-Table 2 Server 22 IP Address",	"T2S22IP"		},	paramLong,	0},
		{	{     113,	"113-Table 2 Server 22 Port Number",	"T2S22PRT"		},	paramWord,	0},
		{	{     114,	"114-Table 2 Server 23 IP Address",	"T2S23IP"		},	paramLong,	0},
		{	{     115,	"115-Table 2 Server 23 Port Number",	"T2S23PRT"		},	paramWord,	0},
		{	{     116,	"116-Table 2 Server 24 IP Address",	"T2S24IP"		},	paramLong,	0},
		{	{     117,	"117-Table 2 Server 24 Port Number",	"T2S24PRT"		},	paramWord,	0},
		{	{     118,	"118-Table 2 Server 25 IP Address",	"T2S25IP"		},	paramLong,	0},
		{	{     119,	"119-Table 2 Server 25 Port Number",	"T2S25PRT"		},	paramWord,	0},
		{	{     120,	"120-Table 3 TCP Option",		"T3TCPOP"		},	paramByte,	0},
		{	{     121,	"121-Table 3 Server 1 IP Address",	"T3S1IP"		},	paramLong,	0},
		{	{     122,	"122-Table 3 Server 1 Port Number",	"T3S1PRT"		},	paramWord,	0},
		{	{     123,	"123-Table 3 Server 2 IP Address",	"T3S2IP"		},	paramLong,	0},
		{	{     124,	"124-Table 3 Server 2 Port Number",	"T3S2PRT"		},	paramWord,	0},
		{	{     125,	"125-Table 3 Server 3 IP Address",	"T3S3IP"		},	paramLong,	0},
		{	{     126,	"126-Table 3 Server 3 Port Number",	"T3S3PRT"		},	paramWord,	0},
		{	{     127,	"127-Table 3 Server 4 IP Address",	"T3S4IP"		},	paramLong,	0},
		{	{     128,	"128-Table 3 Server 4 Port Number",	"T3S4PRT"		},	paramWord,	0},
		{	{     129,	"129-Table 3 Server 5 IP Address",	"T3S5IP"		},	paramLong,	0},
		{	{     130,	"130-Table 3 Server 5 Port Number",	"T3S5PRT"		},	paramWord,	0},
		{	{     131,	"131-Table 3 Server 6 IP Address",	"T3S6IP"		},	paramLong,	0},
		{	{     132,	"132-Table 3 Server 6 Port Number",	"T3S6PRT"		},	paramWord,	0},
		{	{     133,	"133-Table 3 Server 7 IP Address",	"T3S7IP"		},	paramLong,	0},
		{	{     134,	"134-Table 3 Server 7 Port Number",	"T3S7PRT"		},	paramWord,	0},
		{	{     135,	"135-Table 3 Server 8 IP Address",	"T3S8IP"		},	paramLong,	0},
		{	{     136,	"136-Table 3 Server 8 Port Number",	"T3S8PRT"		},	paramWord,	0},
		{	{     137,	"137-Table 3 Server 9 IP Address",	"T3S9IP"		},	paramLong,	0},
		{	{     138,	"138-Table 3 Server 9 Port Number",	"T3S9PRT"		},	paramWord,	0},
		{	{     139,	"139-Table 3 Server 10 IP Address",	"T3S10IP"		},	paramLong,	0},
		{	{     140,	"140-Table 3 Server 10 Port Number",	"T3S10PRT"		},	paramWord,	0},
		{	{     141,	"141-Table 3 Server 11 IP Address",	"T3S11IP"		},	paramLong,	0},
		{	{     142,	"142-Table 3 Server 11 Port Number",	"T3S11PRT"		},	paramWord,	0},
		{	{     143,	"143-Table 3 Server 12 IP Address",	"T3S12IP"		},	paramLong,	0},
		{	{     144,	"144-Table 3 Server 12 Port Number",	"T3S12PRT"		},	paramWord,	0},
		{	{     145,	"145-Table 3 Server 13 IP Address",	"T3S13IP"		},	paramLong,	0},
		{	{     146,	"146-Table 3 Server 13 Port Number",	"T3S13PRT"		},	paramWord,	0},
		{	{     147,	"147-Table 3 Server 14 IP Address",	"T3S14IP"		},	paramLong,	0},
		{	{     148,	"148-Table 3 Server 14 Port Number",	"T3S14PRT"		},	paramWord,	0},
		{	{     149,	"149-Table 3 Server 15 IP Address",	"T3S15IP"		},	paramLong,	0},
		{	{     150,	"150-Table 3 Server 15 Port Number",	"T3S15PRT"		},	paramWord,	0},
		{	{     151,	"151-Table 3 Server 16 IP Address",	"T3S16IP"		},	paramLong,	0},
		{	{     152,	"152-Table 3 Server 16 Port Number",	"T3S16PRT"		},	paramWord,	0},
		{	{     153,	"153-Table 3 Server 17 IP Address",	"T3S17IP"		},	paramLong,	0},
		{	{     154,	"154-Table 3 Server 17 Port Number",	"T3S17PRT"		},	paramWord,	0},
		{	{     155,	"155-Table 3 Server 18 IP Address",	"T3S18IP"		},	paramLong,	0},
		{	{     156,	"156-Table 3 Server 18 Port Number",	"T3S18PRT"		},	paramWord,	0},
		{	{     157,	"157-Table 3 Server 19 IP Address",	"T3S19IP"		},	paramLong,	0},
		{	{     158,	"158-Table 3 Server 19 Port Number",	"T3S19PRT"		},	paramWord,	0},
		{	{     159,	"159-Table 3 Server 20 IP Address",	"T3S20IP"		},	paramLong,	0},
		{	{     160,	"160-Table 3 Server 20 Port Number",	"T3S20PRT"		},	paramWord,	0},
		{	{     161,	"161-Table 3 Server 21 IP Address",	"T3S21IP"		},	paramLong,	0},
		{	{     162,	"162-Table 3 Server 21 Port Number",	"T3S21PRT"		},	paramWord,	0},
		{	{     163,	"163-Table 3 Server 22 IP Address",	"T3S22IP"		},	paramLong,	0},
		{	{     164,	"164-Table 3 Server 22 Port Number",	"T3S22PRT"		},	paramWord,	0},
		{	{     165,	"165-Table 3 Server 23 IP Address",	"T3S23IP"		},	paramLong,	0},
		{	{     166,	"166-Table 3 Server 23 Port Number",	"T3S23PRT"		},	paramWord,	0},
		{	{     167,	"167-Table 3 Server 24 IP Address",	"T3S24IP"		},	paramLong,	0},
		{	{     168,	"168-Table 3 Server 24 Port Number",	"T3S24PRT"		},	paramWord,	0},
		{	{     169,	"169-Table 3 Server 25 IP Address",	"T3S25IP"		},	paramLong,	0},
		{	{     170,	"170-Table 3 Server 25 Port Number",	"T3S25PRT"		},	paramWord,	0},
		{	{     171,	"171-TCP Connection Timeout",		"TCPCNTO"		},	paramByte,	0},
		{	{     172,	"172-Test IP Address",			"TESTIP"		},	paramLong,	0},
		{	{     173,	"173-Test Port",			"TESTPORT"		},	paramWord,	0},
		{	{     174,	"174-Test Start",			"TESTSTRT"		},	paramByte,	0},
		{	{     175,	"175-Test Status",			"TESTSTS"		},	paramByte,	1},
		{	{	0,	"",					""			},	paramNone,	0},
		},
	138,	// User Progarm Configuration
	{
		{	{	0,	"0-Host Library Version",		"LIBVER"		},	paramString,	1},
		{	{	1,	"1-Host SRAM Used",			"SRAMUSED"		},	paramLong,	1},	
		{	{	2,	"2-Host SRAM Free",			"SRAMFREE"		},	paramLong,	1},
		{	{	3,	"3-Host DRAM Used",			"DRAMUSED"		},	paramLong,	1},
		{	{	4,	"4-Host DRAM Free",			"DRAMFREE"		},	paramLong,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	139,	// Smart I/O Module Information
	{
		{	{	0,	"0-Module Type",			"MODTYPE"		},	paramByte,	1},
		{	{	1,	"1-System Mode",			"MODMODE"		},	paramByte,	1},	
		{	{	2,	"2-Board Health",			"MODHLTH"		},	paramByte,	1},
		{	{	3,	"3-Boot Version",			"BOOTREV"		},	paramString,	1},
		{	{	4,	"4-Boot Part Number",			"BOOTPN"		},	paramString,	1},
		{	{	5,	"5-Boot Build Date",			"BOOTDATE"		},	paramString,	1},
		{	{	6,	"6-Flash Version",			"MODREV"		},	paramString,	1},
		{	{	7,	"7-Flash Part Number",			"MODPN"			},	paramString,	1},
		{	{	8,      "8-Flash Build Date",			"MODDATE"		},	paramString,	1},
		{	{	9,      "9-Module Specific Data",		"MODDATA"		},	paramString,	1},
		{	{	10,     "10-Serial Number",			"MODSN"			},	paramString,	1},
		{	{	11,     "11-Description",			"MODDESC"		},	paramString,	1},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	140,	// AC I/O Module Information
	{
		{	{	0,	"0-Point Tag ID",			"TAG"			},	paramString,	0},
		{	{	1,	"1-Power In",				"PWRIN"			},	paramByte,	1},	
		{	{	2,	"2-Channel Mode",			"MODE"			},	paramByte,	1},
		{	{	3,	"3-Scanning Input",			"SCANIN"		},	paramByte,	0},
		{	{	4,	"4-Filter",				"FILTER"		},	paramReal,	0},
		{	{	5,	"5-Status Input",			"STATIN"		},	paramByte,	0},
		{	{	6,	"6-Physical Input",			"PHYIN"			},	paramByte,	1},
		{	{	7,	"7-Scan Period",			"SCANPR"		},	paramReal,	0},
		{	{	8,      "8-Actual Scan Time",			"SCAN"			},	paramReal,	1},
		{	{	9,      "9-Input Invert Mode",			"INVERTI"		},	paramByte,	0},
		{	{	10,     "10-Latch Mode",			"LATCH"			},	paramByte,	0},
		{	{	11,     "11-Input Accumulated Value",		"IACCUM"		},	paramLong,	0},
		{	{	12,	"12-Cumulative On Time",		"ONCTR"			},	paramReal,	0},
		{	{	13,	"13-Cumulative Off Time",		"OFFCTR"		},	paramReal,	0},	
		{	{	14,	"14-Input Alarming",			"IALEN"			},	paramByte,	0},
		{	{	15,	"15-Input Alarm Code",			"ALMCODE"		},	paramByte,	1},
		{	{	16,	"16-Input SRBX on Clear",		"ISRBXC"		},	paramByte,	0},
		{	{	17,	"17-Input SRBX on Set",			"ISRBXS"		},	paramByte,	0},
		{	{	18,	"18-Scanning Output",			"SCANOUT"		},	paramByte,	0},
		{	{	19,	"19-Auto Output",			"AOSTATUS"		},	paramByte,	0},
		{	{	20,     "20-Manual Output",			"MANVAL"		},	paramByte,	0},
		{	{	21,     "21-Failsafe Output",			"FAULTVAL"		},	paramByte,	0},
		{	{	22,     "22-Physical Output",			"PHYOUT"		},	paramByte,	1},
		{	{	23,     "23-Output Accumulated Value",		"OACCUM"		},	paramLong,	0},
		{	{	24,	"24-Failsafe on Reset Mode",		"CLRONRS"		},	paramByte,	0},
		{	{	25,	"25-Momentary Mode",			"MOMODE"		},	paramByte,	0},	
		{	{	26,	"26-Momentary Active",			"MOACTIV"		},	paramByte,	1},
		{	{	27,	"27-Toggle Mode",			"TOGMODE"		},	paramByte,	0},
		{	{	28,	"28-Timed Discrete Output",		"TDOMODE"		},	paramByte,	0},
		{	{	29,	"29-Invert Output Mode",		"INVERTO"		},	paramByte,	0},
		{	{	30,	"30-Time On",				"TIMEON"		},	paramReal,	0},
		{	{	31,	"31-Cycle Time",			"CYCTIME"		},	paramReal,	0},
		{	{	32,     "32-Units Tag",				"UNITS"			},	paramString,	0},
		{	{	33,     "33-Low Reading Time",			"LOWRT"			},	paramReal,	0},
		{	{	34,     "34-High Reading Time",			"HIGHRT"		},	paramReal,	0},
		{	{	35,     "35-Low Reading EU",			"LOWREU"		},	paramReal,	0},
		{	{	36,	"36-High Reading EU",			"HIGHREU"		},	paramReal,	0},
		{	{	37,	"37-EU Value",				"EUVAL"			},	paramReal,	0},	
		{	{	38,	"38-Inrush Time",			"INRUSH"		},	paramReal,	0},
		{	{	39,	"39-Holding Current",			"CURRNT"		},	paramReal,	1},
		{	{	40,	"40-Fault Reset",			"FLTRESET"		},	paramByte,	0},
		{	{	41,	"41-Output Alarming",			"OALEN"			},	paramByte,	0},
		{	{	42,	"42-Output Alarm Code",			"ALMCODE"		},	paramByte,	1},
		{	{	43,	"43-Output SRBX On Clear",		"OSRBXC"		},	paramByte,	0},
		{	{	44,     "44-Output SRBX On Set",		"OSRBXS"		},	paramByte,	0},
		{	{	45,     "45-AC Frequency",			"ACFREQ"		},	paramReal,	0},
		{	{	46,     "46-Failure Action",			"FAILACT"		},	paramByte,	0},
		{	{	 0,	"",					""			},	paramNone,	0},
		},
	};

//////////////////////////////////////////////////////////////////////////
//
// CEmersonRocMasterDriver
//

// Instantiator

ICommsDriver * Create_EmersonRocMasterDriver2(void)
{
	return New CEmersonRocMasterDriver2;
	}

// Constructor

CEmersonRocMasterDriver2::CEmersonRocMasterDriver2(void)
{
	m_wID		= 0x409E;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Emerson Process";
	
	m_DriverName	= "ROC Protocol";
	
	m_Version	= "2.10";
	
	m_ShortName	= "ROC";

	AddSpaces();
	}

// Destructor

CEmersonRocMasterDriver2::~CEmersonRocMasterDriver2(void)
{
	DeleteAllSpaces();
	}

// Binding Control

UINT CEmersonRocMasterDriver2::GetBinding(void)
{
	return bindStdSerial;
	}

void CEmersonRocMasterDriver2::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CEmersonRocMasterDriver2::GetDriverConfig(void)
{
	return AfxRuntimeClass(CEmersonRocMasterDriverOptions);
	}

CLASS CEmersonRocMasterDriver2::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CEmersonRocMasterDeviceOptions);
	}

// Address Management

BOOL CEmersonRocMasterDriver2::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart)
{	
	CEmRocAddrDialog2 Dlg(*this, Addr, pConfig, fPart);
	
	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CEmersonRocMasterDriver2::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	CEmersonRocMasterDeviceOptions * pOpt = (CEmersonRocMasterDeviceOptions *)pConfig;

	if( pOpt ) {

		UINT uPointType = GetPointType(Addr, pConfig);

		UINT uParameter = GetParameter(Addr, pConfig);

		for( UINT i = 0; i < elements(m_pParameters); i++ ) {

			if( m_pParameters[i].m_PointType == uPointType ) {

				for( UINT u = 0; u < elements(m_pParameters[i].m_Param); u++ ) {

					if( m_pParameters[i].m_Param[u].m_Param.m_Number == uParameter ) {

						return m_pParameters[i].m_Param[u].m_fReadOnly;
						}
					}
				}
			}
		}

	return FALSE;
	}

// Address Helpers

BOOL CEmersonRocMasterDriver2::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	UINT uPointType = 0;
	
	UINT uLogical   = 0;

	UINT uParameter = 0;

	UINT uFind = Text.Find('*');

	if( uFind < NOTHING ) {

		CString Tlp   = Text.Mid(0, uFind);

		CString Short = Text.Mid(uFind + 1);

		uFind = Tlp.Find(',');

		if( uFind < NOTHING ) {

			uPointType = tstrtol(Tlp.Mid(0, uFind), NULL, 10);

			Tlp = Tlp.Mid(uFind + 1);

			uFind = Tlp.Find(',');

			if( uFind < NOTHING ) {

				uLogical = tstrtol(Tlp.Mid(0, uFind), NULL, 10);

				Tlp = Tlp.Mid(uFind + 1);

				uParameter = tstrtol(Tlp, NULL, 10);

				SetAddress(Addr, uPointType, uLogical, uParameter, Error, pConfig);
				
				return TRUE;
				}
			}

		return FALSE;
		}

	uFind = Text.Find('_');

	if( uFind < NOTHING ) {

		UINT uNext = Text.Mid(uFind + 1).Find('_');

		if( uNext < NOTHING ) {

			CString PointType = Text.Mid(0, uFind);

			CString Logical   = Text.Mid(uFind + 1, uNext);

			CString Parameter = Text.Mid(uFind + uNext + 2);

			uPointType = FindPointTypeFromShort(PointType, pConfig);

			uLogical   = FindLogicalFromShort(uPointType, Logical);

			uParameter = FindParameterFromShort(uPointType, Parameter);

			SetAddress(Addr, uPointType, uLogical, uParameter, Error, pConfig);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CEmersonRocMasterDriver2::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		UINT uPointType = GetPointType(Addr, pConfig);

		UINT uLogical   = GetLogical(Addr, pConfig);

		UINT uParameter = GetParameter(Addr, pConfig);

		CString PointType = FindPointTypeShort(uPointType);

		CString Logical   = FindLogicalShort(uPointType, uLogical);

		CString Parameter = FindParameterShort(uPointType, uParameter);

		if( PointType.IsEmpty() || Logical.IsEmpty() || Parameter.IsEmpty() ) {

			return FALSE;
			}

		Text.Printf("%s_%s_%s", PointType, Logical, Parameter);
		
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CEmersonRocMasterDriver2::IsIO(UINT uPoint)
{
	return (uPoint > 0 && uPoint < 6) || uPoint == 56;
	}

UINT CEmersonRocMasterDriver2::GetPointType(CAddress Addr,  CItem *pConfig)
{
	UINT uPointType = 0;

	if( IsString(Addr.a.m_Table) ) {

		uPointType = Addr.a.m_Table - T_STR_L_E;

		return uPointType;
		}
	
	BYTE bExt = 0;

	if( HasExtendedPointType(pConfig) && Addr.a.m_Table <= T_REAL_E ) {

		bExt = 1;
		}

	uPointType = ((Addr.a.m_Extra << 2) & 0x3C) | ((Addr.a.m_Offset >> 14) & 0x3) | (bExt << 6);

	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	if( pDev->m_Model + 1 == devRoc800 ) {

		if( OFF_ROC800 > uPointType ) {

			uPointType += OFF_ROC800;

			if( uPointType == OFF_ROC800 + 1 || uPointType == OFF_ROC800 + 2 ) {	// Special Case Point Types 71 and 72

				uPointType -= 8;
				}
			}
		}
	
	return uPointType;
	}

UINT CEmersonRocMasterDriver2::GetLogical(CAddress Addr,  CItem *pConfig)
{
	UINT uExt = 0;

	if( IsString(Addr.a.m_Table) ) {

		uExt = ((Addr.a.m_Offset >> 12) & 0x1) * 128;

		return ((Addr.a.m_Offset >> 13) | (Addr.a.m_Extra << 3)) + uExt;
		}

	if( Addr.a.m_Table > 200 ) {

		uExt = ((Addr.a.m_Table - T_BIT_E1) / 5 + 1) * 64;  
		}

	return ((Addr.a.m_Offset >> GetLogicalShift(pConfig)) & GetLogicalMask(pConfig)) + uExt;
	}

UINT CEmersonRocMasterDriver2::GetParameter(CAddress Addr,  CItem *pConfig)
{
	if( IsString(Addr.a.m_Table) ) {

		UINT uOffset = Addr.a.m_Offset & 0xFFF;

		if( uOffset % 8 == 0 && uOffset % 15 != 0 ) {	// String support for previous max size of 32 chars

			return uOffset / 8;
			}

		return uOffset / 15;			 
		}

	return (Addr.a.m_Offset & GetParameterMask(pConfig));
	}

BOOL CEmersonRocMasterDriver2::IsString(UINT uTable)
{
	return ((uTable >=  T_STR_L_E) && (uTable <=  T_STR_H_E));
	}

UINT CEmersonRocMasterDriver2::FindPointTypeFromShort(CString Short, CItem * pConfig)
{
	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	for( UINT u = 0; u < elements(m_pPointType); u++ ) {

		if( m_pPointType[u].m_Short && (m_pPointType[u].m_Device & (pDev->m_Model + 1)) ) {

			CString Compare = CString(m_pPointType[u].m_Short);

			if( Compare == Short ) {

				return m_pPointType[u].m_Number;
				}
			}
		}

	return 0;
	}

UINT CEmersonRocMasterDriver2::FindLogicalFromShort(UINT uPointType, CString Short)
{
	if( IsIO(uPointType) ) {

		for( UINT u = 0; u < elements(m_pLogical); u++ ) {

			if( m_pLogical[u].m_Short ) {

				CString Compare = CString(m_pLogical[u].m_Short);

				if( Compare == Short ) {

					return u;
					}
				}
			}
		}
	
	return tstrtol(Short, NULL, 10) - 1;
	}

UINT CEmersonRocMasterDriver2::FindParameterFromShort(UINT uPointType, CString Short)
{
	for( UINT u = 0; u < elements(m_pParameters); u++ ) {

		if( m_pParameters[u].m_PointType == uPointType ) {

			for( UINT i = 0; i < elements(m_pParameters[u].m_Param); i++ ) {

				if( m_pParameters[u].m_Param[i].m_Param.m_Short ) {

					CString Compare = CString(m_pParameters[u].m_Param[i].m_Param.m_Short);

					if( Compare == Short) {

						return m_pParameters[u].m_Param[i].m_Param.m_Number;
						}
					}
				}
			}
		}

	return 0;
	}

CString CEmersonRocMasterDriver2::FindPointTypeShort(UINT uPointType)
{
	CString PointType = "";

	for( UINT u = 0; u < elements(m_pPointType); u++ ) {

		if( m_pPointType[u].m_Number == uPointType ) {

			PointType = CString(m_pPointType[u].m_Short);

			break;
			}
		}

	return PointType;
	}

CString CEmersonRocMasterDriver2::FindLogicalShort(UINT uPointType, UINT uLogical)
{
	CString Logical = "";

	if( IsIO(uPointType) ) {

			Logical = m_pLogical[uLogical].m_Short;
			}
		else {
			Logical.Printf("%u", m_pLogical[uLogical].m_Number);
			}

	return Logical;
	}

CString CEmersonRocMasterDriver2::FindParameterShort(UINT uPointType, UINT uParameter)
{
	CString Parameter = "";

	for( UINT u = 0; u < elements(m_pParameters); u++ ) {

		if( m_pParameters[u].m_PointType == uPointType ) {

			for( UINT i = 0; i < elements(m_pParameters[u].m_Param); i++ ) {

				if( m_pParameters[u].m_Param[i].m_Param.m_Number == uParameter ) {

					if( m_pParameters[u].m_Param[i].m_Param.m_Short ) {

						Parameter = CString(m_pParameters[u].m_Param[i].m_Param.m_Short);
						}

					break;
					}
				}
			break;
			}
		}

	return Parameter;
	}

BYTE CEmersonRocMasterDriver2::GetLogicalMask(CItem *pConfig)
{
	return HasExtendedParameter(pConfig) ? 0x3F : 0x7F;
	}

BYTE CEmersonRocMasterDriver2::GetLogicalCount(CItem *pConfig)
{
	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	if( pDev->m_Model + 1 == devRoc800 ) {

		return 250;
		}

	if( pDev->m_Model + 1 == devFlo103104 ) {

		return 128;
		}

	return 0;
	}

BYTE CEmersonRocMasterDriver2::GetLogicalShift(CItem *pConfig)
{
	BYTE bLogMask = GetLogicalMask(pConfig);

	BYTE bShift = 7;

	if( !(bLogMask & 0x40) ) {

		bShift++;
		}

	return bShift;
	}

BYTE CEmersonRocMasterDriver2::GetParameterMask(CItem *pConfig)
{
	return HasExtendedParameter(pConfig) ? 0xFF : 0x7F;
	}

// Implementation

void CEmersonRocMasterDriver2::AddSpaces(void)
{								       
	
	}

UINT CEmersonRocMasterDriver2::FindDataType(UINT uPointType, UINT uParameter)
{
	for( UINT i = 0; i < elements(m_pParameters); i++ ) {

		if( m_pParameters[i].m_PointType == uPointType ) {

			for( UINT u = 0; u < elements(m_pParameters[i].m_Param); u++ ) {

				if( m_pParameters[i].m_Param[u].m_Param.m_Number == uParameter ) {

					return m_pParameters[i].m_Param[u].m_Type;
					}
				}
			}
		}

	return paramLong;
	}

BOOL CEmersonRocMasterDriver2::SetAddress(CAddress &Addr, UINT uPointType, UINT uLogical, UINT uParameter, CError &Error, CItem *pConfig)
{
	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	BOOL fExt   = HasExtendedPointType(pConfig) && uPointType > 63;

	UINT uType  = FindDataType(uPointType, uParameter);

	UINT uTable = FindTable(uType, uLogical, pConfig);

	switch( uType ) {

		case paramFlag:		Addr.a.m_Table = fExt ? T_BIT_E : (uTable ? uTable : T_BIT);
					Addr.a.m_Type  = addrBitAsBit;
					break;						

		case paramByte:		Addr.a.m_Table = fExt ? T_BYTE_E : (uTable ? uTable : T_BYTE);
					Addr.a.m_Type  = addrByteAsByte;
					break;

		case paramWord:		Addr.a.m_Table = fExt ? T_WORD_E : (uTable ? uTable : T_WORD);
					Addr.a.m_Type  = addrWordAsWord;
					break;

		case paramTLP:
		case paramLong:		Addr.a.m_Table = fExt ? T_LONG_E : (uTable ? uTable : T_LONG);
					Addr.a.m_Type  = addrLongAsLong;
					break;

		case paramDouble:
		case paramReal:		Addr.a.m_Table = fExt ? T_REAL_E : (uTable ? uTable : T_REAL);
					Addr.a.m_Type  = addrRealAsReal;
					break;

		case paramString:	Addr.a.m_Type   = addrLongAsLong;
					Addr.a.m_Table  = T_STR_L_E + uPointType;
					Addr.a.m_Offset = (((uParameter * 15) & 0xFFF) | ((uLogical & 0x7) << 13));
					Addr.a.m_Extra  = (uLogical >> 3) & 0xF;
					Addr.a.m_Offset|= (((uLogical / 128) & 0x1) << 12);

					pDev->m_fChange = TRUE;
					
					return TRUE;
					
		default:		Error.Set("Invalid Parameter", 0);
					return FALSE;
							
		}

	if( pDev->m_Model + 1 == devRoc800 ) {

		if( uPointType < OFF_ROC800 ) {		// Special Case Point Types 71 and 72

			uPointType += 8;
			}

		if( uPointType > OFF_ROC800 ) {

			uPointType -= OFF_ROC800;
			}
		}

	Addr.a.m_Offset = (uParameter & GetParameterMask(pConfig)) | ((uLogical & GetLogicalMask(pConfig)) << GetLogicalShift(pConfig)) | ((uPointType & 0x3) << 14);

	Addr.a.m_Extra  = (uPointType >> 2) & 0xF;

	pDev->m_fChange = TRUE;

	return TRUE;
	}

BOOL CEmersonRocMasterDriver2::HasExtendedPointType(CItem *pConfig)
{
	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	switch ( pDev->m_Model + 1 ) {
		
		case devFlo103104:	
			
			return TRUE;
		}
	
	return FALSE;
	}

BOOL CEmersonRocMasterDriver2::HasExtendedParameter(CItem *pConfig)
{
	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	switch ( pDev->m_Model + 1 ) {
		
		case devRoc800:	
			
			return TRUE;
		}


	return FALSE;
	}

BYTE CEmersonRocMasterDriver2::FindTable(UINT uType, UINT uLogical, CItem *pConfig)
{
	CEmersonRocMasterDeviceOptions * pDev = (CEmersonRocMasterDeviceOptions *) pConfig;

	if( pDev->m_Model + 1 == devRoc800 ) {

		BYTE bExt = (uLogical / 64) & 0x3;

		if( bExt > 0 ) {

			switch( uType ) {

				case paramFlag:		return T_BIT_E1  + (bExt - 1) * 5;
				case paramByte:		return T_BYTE_E1 + (bExt - 1) * 5;
				case paramWord:		return T_WORD_E1 + (bExt - 1) * 5;
				case paramTLP:	
				case paramLong:		return T_LONG_E1 + (bExt - 1) * 5;
				case paramDouble:
				case paramReal:		return T_REAL_E1 + (bExt - 1) * 5;
				}
			}
		}

	return 0;
	}

//////////////////////////////////////////////////////////////////////////
//
// Emerson ROC Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CEmRocAddrDialog2, CStdDialog);
		
// Constructor

CEmRocAddrDialog2::CEmRocAddrDialog2(CEmersonRocMasterDriver2 &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart) 
{
	m_pDriver = &Driver;
	
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_fPart   = fPart;
		
	SetName(TEXT("EmRoc2Dlg"));
	}

// Message Map

AfxMessageMap(CEmRocAddrDialog2, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxDispatchNotify(200, CBN_SELCHANGE, OnSelChange)
	AfxDispatchNotify(201, CBN_SELCHANGE, OnSelChange)
	AfxDispatchNotify(202, CBN_SELCHANGE, OnSelChange)

	AfxMessageEnd(CEmRocAddrDialog2)
	};

// Message Handlers

BOOL CEmRocAddrDialog2::OnInitDialog(CWnd &Wnd, DWORD dwData)
{
	LoadPointType();

	OnSelChange(ID_T, Wnd);

	return FALSE;
	}

// Notification Handlers

void CEmRocAddrDialog2::OnSelChange(UINT uID, CWnd &Wnd)
{
	if( uID == ID_T ) {

		LoadLogical();

		LoadParameter();

		SetShortName();

		SetType();

		SetTLP();
		}

	if( uID == ID_L ) {

		SetShortName();	

		SetTLP();
		}

	if( uID == ID_P ) {

		SetShortName();

		SetType();

		SetTLP();
		}
	}

// Command Handlers

BOOL CEmRocAddrDialog2::OnOkay(UINT uID)
{
	if( CheckInit() ) {

		CString Tlp   = GetDlgItem(302).GetWindowTextW();

		CString Short = GetDlgItem(300).GetWindowTextW();

		CError Error(TRUE);

		CAddress Addr;

		if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Tlp + "*" + Short ) ) {

			*m_pAddr = Addr;

			EndDialog(TRUE);

			return TRUE;
			}
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

// Implementation

void CEmRocAddrDialog2::LoadPointType(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_T);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	CStringArray Array;
		
	Array.Append(TEXT("No Selection"));

	CEmersonRocMasterDeviceOptions * pOpt = (CEmersonRocMasterDeviceOptions *) m_pConfig;

	UINT uSel = 0;

	UINT uPointType = NOTHING;

	if( m_pAddr->m_Ref ) {

		uPointType = m_pDriver->GetPointType(*m_pAddr, m_pConfig);
		}

	for( UINT u = 0; u < elements(m_pPointType); u++ ) {

		CString Name = m_pPointType[u].m_Name;

		if( !Name.IsEmpty() ) {

			if( pOpt ) {

				if( !(m_pPointType[u].m_Device & ( 1 << pOpt->m_Model )) ) {

					continue;
					}
				}

			Array.Append(m_pPointType[u].m_Name);

			if( m_pPointType[u].m_Number == uPointType ) {

				uSel = Array.GetCount() - 1;
				}
			}
		}
	
	Box.AddStrings(Array);

	Box.SetRedraw (TRUE);

	Box.Invalidate(TRUE);

	Box.SetCurSel(uSel);

	GetDlgItem(ID_L).EnableWindow(FALSE);

	GetDlgItem(ID_P).EnableWindow(FALSE);
	}

void CEmRocAddrDialog2::LoadLogical(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_L);

	if( CheckInit() ) {

		Box.SetRedraw(FALSE);

		Box.ResetContent();

		CStringArray Array;

		UINT uSel = 0;

		if( m_pAddr->m_Ref ) {

			uSel = m_pDriver->GetLogical(*m_pAddr, m_pConfig);
			}

		UINT uPointType = GetPointType();

		UINT uCount = m_pDriver->GetLogicalCount(m_pConfig);

		for( UINT u = 0; u < uCount; u++ ) {

			if( uPointType > 0 && uPointType < 6 ) {

				Array.Append(m_pLogical[u].m_Short);
				}
			else {
				CString Text = "%u";

				Text.Printf(Text, m_pLogical[u].m_Number);
				
				Array.Append(Text);
				}
			}

		Box.AddStrings(Array);

		Box.SetRedraw (TRUE);

		Box.Invalidate(TRUE);

		Box.EnableWindow(TRUE);

		Box.SetCurSel(uSel);

		return;
		}

	Box.SetCurSel(NOTHING);
	}

void CEmRocAddrDialog2::LoadParameter(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_P);

	if( CheckInit() ) {

		Box.SetRedraw(FALSE);

		Box.ResetContent();

		CStringArray Array;

		UINT uSel = 0;

		UINT uParameter = 0;

		if( m_pAddr->m_Ref ) {

			uParameter = m_pDriver->GetParameter(*m_pAddr, m_pConfig);
			}

		UINT uPointType = GetPointType();

		for( UINT i = 0; i < elements(m_pParameters); i++ ) {

			if( m_pParameters[i].m_PointType == uPointType ) {

				for( UINT u = 0; u < elements(m_pParameters[i].m_Param); u++ ) {

					if( m_pParameters[i].m_Param[u].m_Type == paramNone ) {

						if( m_pParameters[i].m_Param[u].m_Param.m_Number == 0 ) {

							break;
							}

						continue;
						}

					Array.Append(m_pParameters[i].m_Param[u].m_Param.m_Name);

					if( m_pParameters[i].m_Param[u].m_Param.m_Number ==  uParameter ) {

						uSel = Array.GetCount() - 1;
						}
					}
				break;
				}
			}

		Box.AddStrings(Array);

		Box.SetRedraw (TRUE);

		Box.Invalidate(TRUE);

		Box.EnableWindow(TRUE);

		Box.SetCurSel(uSel);

		return;
		}

	Box.SetCurSel(NOTHING);
	}

UINT CEmRocAddrDialog2::GetPointType(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_T);

	UINT uPoint = Box.GetCurSel();

	UINT uFind = 0;

	if( uPoint > 0 ) {

		CString Point = Box.GetWindowTextW();

		uFind = Point.Find('-');

		if( uFind < NOTHING ) {

			uPoint = tstrtol(Point.Mid(0, uFind), NULL, 10);
			}
		}

	return uPoint;
	}

UINT CEmRocAddrDialog2::GetLogical(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_L);

	return Box.GetCurSel();
	}

UINT CEmRocAddrDialog2::GetParameter(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(ID_P);
	
	CString Text = Box.GetWindowTextW();

	UINT uFind = Text.Find('-');

	if( uFind < NOTHING ) {

		return tstrtol(Text.Mid(0, uFind), NULL, 10);
		}
	
	return 0;
	}

void CEmRocAddrDialog2::SetShortName(void)
{
	if( CheckInit() ) {

		UINT uPointType = GetPointType();

		for( UINT u = 0; u < elements(m_pPointType); u++ ) {

			if( uPointType == m_pPointType[u].m_Number ) {

				UINT uLogical = GetLogical();

				UINT uParameter = GetParameter();

				CString Text = m_pPointType[u].m_Short;

				Text += "_";

				if( m_pDriver->IsIO(uPointType) ) {

					Text += m_pLogical[uLogical].m_Short;
					}
				else {
					CString Number = "%u";

					Number.Printf(Number, m_pLogical[uLogical].m_Number);

					Text += Number;
					}

				Text += "_";

				for( UINT i = 0; i < elements(m_pParameters); i++ ) {

					if( uPointType == m_pParameters[i].m_PointType ) {

						Text += m_pParameters[i].m_Param[uParameter].m_Param.m_Short;
					
						GetDlgItem(300).SetWindowTextW(Text);

						return;
						}
					}
				}
			}
		}

	GetDlgItem(300).SetWindowTextW("<short name>");
	}

void CEmRocAddrDialog2::SetType(void)
{
	if( CheckInit() ) {

		UINT uPointType = GetPointType();

		for( UINT i = 0; i < elements(m_pParameters); i++ ) {

			if( uPointType == m_pParameters[i].m_PointType ) {

				UINT uType = m_pParameters[i].m_Param[GetParameter()].m_Type;

				CString Text = "<type>";

				switch( uType ) {
			
					case paramNone:	
						break;
					case paramFlag:
						Text = "Flag";
						break;
					case paramByte:
						Text = "Byte";
						break;
					case paramWord:
						Text = "Word";
						break;
					case paramLong:
						Text = "Long";
						break;
					case paramReal:
						Text = "Real";
						break;
					case paramDouble:
						Text = "Double";
						break;
					case paramString:
						Text = "String";
						break;
					case paramTLP:
						Text = "TLP";
						break;
					}
					
				GetDlgItem(301).SetWindowTextW(Text);

				return;
				}
			}
		}

	GetDlgItem(301).SetWindowTextW("<type>");
	}

void CEmRocAddrDialog2::SetTLP(void)
{
	if( CheckInit() ) {

		UINT uPointType = GetPointType();

		for( UINT u = 0; u < elements(m_pPointType); u++ ) {

			if( uPointType == m_pPointType[u].m_Number ) {

				UINT uLogical = GetLogical();

				UINT uParameter = GetParameter();

				CString Text = "%u,%u,%u";

				Text.Printf(Text, uPointType, uLogical, uParameter);
			
				GetDlgItem(302).SetWindowTextW(Text);

				return;
				}
			}
		}

	GetDlgItem(302).SetWindowTextW("<tlp>");
	}

BOOL CEmRocAddrDialog2::CheckInit(void)
{
	CComboBox & Box = (CComboBox &) GetDlgItem(ID_T);

	if( Box.GetCurSel() ) {

		return TRUE;
		}

	return FALSE;
	}

// End of File