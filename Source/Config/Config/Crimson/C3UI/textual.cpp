
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Textual User Interface Element
//

// Runtime Class

AfxImplementRuntimeClass(CUITextElement, CUIBaseElement);

// Constructor

CUITextElement::CUITextElement(void)
{
	m_uFlags = 0;

	m_uWidth = 12;
	
	m_uLimit = 8;

	m_Verb   = IDS_PICK_2;
	}

// Attributes

UINT CUITextElement::GetFlags(void) const
{
	return m_uFlags;
	}

BOOL CUITextElement::HasFlag(UINT uFlag) const
{
	return (m_uFlags & uFlag) ? TRUE : FALSE;
	}

UINT CUITextElement::GetWidth(void) const
{
	return m_uWidth;
	}

UINT CUITextElement::GetLimit(void) const
{
	return m_uLimit;
	}

CString CUITextElement::GetVerb(void) const
{
	AfxAssert(HasFlag(textExpand));

	return m_Verb;
	}

CString CUITextElement::GetUnits(void) const
{
	AfxAssert(HasFlag(textUnits));

	return m_Units;
	}

CString CUITextElement::GetDefault(void) const
{
	AfxAssert(HasFlag(textDefault));

	return m_Default;
	}

// Operations

void CUITextElement::SetReadOnly(void)
{
	m_uFlags |= textRead;
	}

// Text Mode Editing

CString CUITextElement::GetAsText(void)
{
	return OnGetAsText();
	}

UINT CUITextElement::SetAsText(CError &Error, CString Text)
{
	if( !HasFlag(textRead) ) {

		AfxAssert(HasFlag(textEdit | textEnum | textExpand));

		return OnSetAsText(Error, Text);
		}

	return saveSame;
	}

CString CUITextElement::ScrollData(CString Text, UINT uCode)
{
	if( !HasFlag(textRead) ) {

		AfxAssert(HasFlag(textScroll));

		return OnScrollData(Text, uCode);
		}

	return Text;
	}

BOOL CUITextElement::EnumValues(CStringArray &List)
{
	AfxAssert(HasFlag(textEnum));

	return OnEnumValues(List);
	}

BOOL CUITextElement::ExpandData(CWnd &Wnd)
{
	if( !HasFlag(textRead) || HasFlag(textLocking) ) {

		AfxAssert(HasFlag(textExpand));

		return OnExpand(Wnd);
		}

	return FALSE;
	}

// Overridable

CString CUITextElement::OnGetAsText(void)
{
	return L"";
	}

UINT CUITextElement::OnSetAsText(CError &Error, CString Text)
{
	Error.Set(IDS_THIS_PROPERTY_2);

	return saveError;
	}

CString CUITextElement::OnScrollData(CString Text, UINT uCode)
{
	return Text;
	}

BOOL CUITextElement::OnEnumValues(CStringArray &List)
{
	return FALSE;
	}

BOOL CUITextElement::OnExpand(CWnd &Wnd)
{
	return FALSE;
	}

// End of File
