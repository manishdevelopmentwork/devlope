
#include "Intern.hpp"

#include "Filename.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "String.hpp"

////////////////////////////////////////////////////////////////////////
//
// File Name Parser
//

// Constructors

CFilename::CFilename(void)
{
	Init();
	}

CFilename::CFilename(CFilename const &That)
{
	InitFrom(That.m_pData, That.m_uLen);
	}

CFilename::CFilename(CFilename const &That, UINT uLen)
{
	InitFrom(That.m_pData, Min(That.m_uLen, uLen));
	}

CFilename::CFilename(CString const &Text)
{
	InitFrom(Text, Text.GetLength());
	}

CFilename::CFilename(PCTXT pText)
{
	InitFrom(pText, pText ? strlen(pText) : 0);
	}

// Destructor

CFilename::~CFilename(void)
{
	Free();
	}

// Assignment Operator

CFilename const & CFilename::operator = (CFilename const &That)
{
	Free();

	InitFrom(That.m_pData, That.m_uLen);
	
	return ThisObject;
	}

CFilename const & CFilename::operator = (CString const &That)
{
	Free();

	InitFrom(That, That.GetLength());
	
	return ThisObject;
	}

CFilename const & CFilename::operator = (PCTXT pText)
{
	Free();

	InitFrom(pText, pText ? strlen(pText) : 0);
	
	return ThisObject;
	}

// Concatenation In-Place

CFilename const & CFilename::operator += (CFilename const &That)
{
	if( That.m_uLen ) {
	
		if( That.m_pData[0] != '\\' && !That.HasDrive() ) {
			
			if( !m_uLen || m_pData[m_uLen-1] != '\\') {

				Alloc(m_uLen + 1);

				m_pData[m_uLen++] = '\\';
				}
			}
		
		Alloc(m_uLen + That.m_uLen);

		memcpy(m_pData + m_uLen, That.m_pData, That.m_uLen);

		m_uLen += That.m_uLen;

		m_pData[m_uLen] = 0;
		}

	return ThisObject;
	}

CFilename const & CFilename::operator += (CString const &Text)
{
	UINT uLen = Text.GetLength();
	
	if( uLen ) {
	
		if( Text[0] != '\\' && Text.Find(':') == NOTHING ) {
			
			if( !m_uLen || m_pData[m_uLen-1] != '\\' ) {

				Alloc(m_uLen + 1);

				m_pData[m_uLen++] = '\\';
				}
			}

		Alloc(m_uLen + uLen);

		memcpy(m_pData + m_uLen, PCTXT(Text), uLen);

		m_uLen += uLen;

		m_pData[m_uLen] = 0;
		}

	return ThisObject;
	}

CFilename const & CFilename::operator += (PCTXT pText)
{
	UINT uLen = pText ? strlen(pText) : 0;
	
	if( uLen ) {
	
		if( pText[0] != '\\' && !strchr(pText, ':') ) {
			
			if( !m_uLen || m_pData[ m_uLen - 1] != '\\' ) {

				Alloc(m_uLen + 1);

				m_pData[ m_uLen ++ ] = '\\';
				}
			}

		Alloc(m_uLen + uLen);

		memcpy(m_pData + m_uLen, pText, uLen);

		m_uLen += uLen;

		m_pData[ m_uLen ] = 0;
		}

	return ThisObject;
	}

// Concatenation via Friends

CFilename operator + (CFilename const &A, CFilename const &B)
{
	CFilename Result;

	Result += A;

	Result += B;

	return Result;
	}

CFilename operator + (CFilename const &A, CString const &Text)
{
	CFilename Result;

	Result += A;

	Result += Text;

	return Result;
	}

CFilename operator + (CFilename const &A, PCTXT pText)
{
	CFilename Result;

	Result += A;

	Result += pText;

	return Result;
	}

// Attributes

BOOL CFilename::IsEmpty(void) const
{
	return m_pData == NULL || m_uLen == 0;	
	}

UINT CFilename::GetLength(void) const
{
	return m_uLen;
	}

PBYTE CFilename::GetData(void) const
{
	return PBYTE(m_pData);
	}

PCTXT CFilename::GetRest(void) const
{
	return m_pData + m_uInit;
	}

BOOL CFilename::HasDrive(void) const
{
	PTXT p = m_pData ? strchr(m_pData, ':') : NULL;

	return p && p > m_pData;
	}

BOOL CFilename::HasType(void) const
{
	PTXT p = m_pData ? strrchr(m_pData, '.') : NULL;

	return p && p > m_pData;
	}

BOOL CFilename::HasDirectory(void) const
{
	PTXT p = m_pData ? strchr(m_pData, '\\') : NULL;

	return p != NULL;
	}

CHAR CFilename::GetDrive(void) const
{
	PTXT p = m_pData ? strchr(m_pData, ':') : NULL;

	return p && --p >= m_pData ? *p : NULL;
	}

PCTXT CFilename::GetType(void) const
{
	PTXT p = m_pData ? strrchr(m_pData, '.') : NULL;

	return p && p > m_pData ? p + 1 : NULL;
	}

PCTXT CFilename::GetPath(void) const
{
	PTXT p = m_pData ? strchr(m_pData, ':') : NULL;

	return p ? p + 1 : m_pData;
	}

// Operations

void CFilename::MakeUpper(void)
{
	if( m_pData ) strupr(m_pData);
	}

void CFilename::MakeLower(void)
{
	if( m_pData ) strlwr(m_pData);
	}

void CFilename::Empty(void)
{
	Free();
	}

BOOL CFilename::ChangeType(PCTXT pExt)
{
	RemoveType();

	return AppendType(pExt);
	}

// Comparison

BOOL CFilename::operator == (CFilename const &That) const
{
	return operator == (PCTXT(That));
	}

BOOL CFilename::operator == (PCTXT pText) const
{
	for( UINT i1 = 0, i2 = 0; ; i1 ++, i2 ++ ) {

		char c = pText   ? pText[i1]   : 0;

		char s = m_pData ? m_pData[i2] : 0;

		if( c >= 'a' && c <= 'z' ) {

			c &= ~0x20;
			}

		if( s >= 'a' && s <= 'z' ) {

			s &= ~0x20;
			}

		if( c == '?' || c == s ) {

			if( !s ) {

				return true;
				}

			continue;
			}

		if( c == '*' ) {
			
			c = pText[i1 + 1];

			if( c >= 'a' && c <= 'z' ) {

				c &= ~0x20;
				}
						
			if( c == s ) {

				i2 --;

				continue;
				}
			
			if( !s ) {

				return true;
				}

			i1 --;
			
			continue;
			}

		if( c == s ) {

			if( !s ) {

				return true;
				}

			continue;
			}

		return false;
		}

	return false;
	}

// Conversion

CFilename::operator PCTXT (void) const
{
	return m_pData;
	}

// Access

CHAR CFilename::operator [] (UINT iIndex) const
{
	return iIndex < m_uLen ? m_pData[iIndex] : NULL;
	}

// Element Parsing

BOOL CFilename::MoveTop(PTXT pText) const
{
	m_uInit = 0;

	return MoveDown(pText);
	}

BOOL CFilename::MoveDown(PTXT pText) const
{
	if( m_pData && m_pData[m_uInit] ) {
		
		if( !m_uInit ) {

			PTXT pFind = strchr(m_pData, ':');

			if( pFind ) {

				m_uInit += (pFind - m_pData + 1);

				if( m_uInit >= m_uLen ) {

					return false;
					}
				}
			}
				
		PTXT pFind = strchr(m_pData + m_uInit, '\\');

		if( pFind ) {

			UINT uLen = pFind - m_pData - m_uInit;

			strncpy(pText, m_pData + m_uInit, uLen);

			pText[uLen] = 0;

			m_uInit += (uLen + 1);
		
			return true;
			}

		strcpy(pText, m_pData + m_uInit);

		m_uInit += strlen(pText);

		return true;
		}

	return false;
	}

BOOL CFilename::GetDirectory(CFilename &Dir) const
{
	if( m_pData ) {
	
		PTXT pFind = strrchr(m_pData, '\\');

		if( pFind ) {

			if( pFind == m_pData ) {

				Dir = "\\";

				return true;
				}

			Dir = CFilename(m_pData, pFind - m_pData);

			return true;
			}
		}

	return false;
	}

PCTXT CFilename::GetName(void) const
{
	if( m_pData ) {
	
		PTXT pFind = strrchr(m_pData, '\\');

		return pFind ? pFind + 1 : m_pData;
		}

	return NULL;
	}

BOOL CFilename::GetBareName(PTXT pName) const
{
	if( m_pData ) {

		PTXT pFind = strrchr(m_pData, '\\');

		PTXT pSrc  = pFind ? pFind + 1 : m_pData;

		pFind = strchr(pSrc, '.');

		if( pFind ) {

			UINT uLen = pFind - pSrc;

			strncpy(pName, pSrc, uLen);

			pName[uLen] = 0;

			return true;
			}

		strcpy(pName, pSrc);

		return true;
		}

	return false;
	}

// Initialisation

void CFilename::Init(void)
{
	m_uAlloc = 0;

	m_uLen   = 0;

	m_pData  = NULL;

	m_uInit  = 0;
	}

void CFilename::InitFrom(PCTXT pText, UINT uLen)
{
	Init();
	
	if( pText && uLen ) {
	
		Alloc(uLen + 1);
	
		m_uLen = uLen; 

		memcpy(m_pData, pText, m_uLen);

		m_pData[ m_uLen ] = 0;
		}
	}

// Implemenation

void CFilename::Free(void)
{
	if( m_pData ) {

		delete [] m_pData;

		Init();
		}
	}

void CFilename::Alloc(UINT uSize)
{
	UINT uAlloc = ((uSize + 31) / 32) * 32;
	
	if( uAlloc > m_uAlloc ) {

		PTXT pData = New char [ uAlloc ];
		
		if( m_uAlloc ) {
			
			memcpy(pData, m_pData, m_uLen + 1);

			delete [] m_pData;
			}
			
		m_uAlloc = uAlloc;

		m_pData  = pData;

		m_pData[ m_uLen ] = 0;
		}
	}

// Type Helpers

void CFilename::RemoveType(void)
{
	PTXT pFind = m_pData ? strrchr(m_pData, '.') : NULL;

	if( pFind ) {

		*pFind = 0;

		m_uLen = pFind - m_pData;
		}
	}

BOOL CFilename::AppendType(PCTXT pType)
{
	if( m_uLen && pType ) {
	
		UINT uLen = strlen(pType);

		if( uLen ) {
		
			if( pType[0] != '.' ) {

				Alloc(m_uLen + 1);

				m_pData[ m_uLen ++ ] = '.';

				m_pData[ m_uLen ] = 0;
				}

			Alloc(m_uLen + uLen);

			memcpy(m_pData + m_uLen, pType, uLen + 1);

			m_uLen += uLen;

			return true;
			}
		}
		
	return false;
	}

// End of File
