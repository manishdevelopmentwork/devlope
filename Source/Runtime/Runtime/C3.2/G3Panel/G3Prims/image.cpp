
#include "intern.hpp"

#include "PrimRubyShade.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive Image
//

// Constructor

CPrimImage::CPrimImage(void)
{
	m_Image = NOTHING;

	m_pData = NULL;
	}

// Destructor

CPrimImage::~CPrimImage(void)
{
	g_pDbase->PendItem(m_Image, FALSE);

	g_pDbase->FreeItem(m_Image);
	}

// Initialization

void CPrimImage::Load(PCBYTE &pData)
{
	ValidateLoad("CPrimImage", pData);

	m_Image = GetWord(pData);

	if( m_Image < NOTHING ) {

		PCBYTE pData = PCBYTE(g_pDbase->LockItem(m_Image));

		if( pData ) {

			if( GetWord(pData) == IDC_IMAGE && GetWord(pData) == 0x1234 ) {

				if( GetWord(pData) == 0x0001 ) {

					m_xAct  = GetWord(pData);

					m_yAct  = GetWord(pData);

					m_fTile = GetByte(pData);

					m_pData = pData + 1;
					}
				}

			g_pDbase->PendItem(m_Image, TRUE);
			}
		}
	}

// Attributes

BOOL CPrimImage::IsTransparent(R2 Rect) const
{
	if( m_pData ) {

		int rop = IntelToHost(PWORD(m_pData)[0]);

		if( rop & ropTrans ) {

			return TRUE;
			}

		if( rop & ropBlend ) {

			return TRUE;
			}

		if( !m_fTile ) {

			int cx = Rect.x2 - Rect.x1;

			int cy = Rect.y2 - Rect.y1;

			if( cx > m_xAct || cy > m_yAct ) {

				return TRUE;
				}
			}

		return FALSE;
		}

	return TRUE;
	}

BOOL CPrimImage::IsAntiAliased(void) const
{
	if( m_pData ) {

		int rop = IntelToHost(PWORD(m_pData)[0]);

		if( rop & ropBlend ) {

			return TRUE;
			}

		return FALSE;
		}

	return FALSE;
	}

// Operations

void CPrimImage::DrawItem(IGDI *pGDI, R2 Rect, UINT rop2)
{
	DrawItem(pGDI, Rect, rop2, NULL);
	}

void CPrimImage::DrawItem(IGDI *pGDI, R2 Rect, UINT rop2, CPrimRubyShade *pShade)
{
	int xp = Rect.x1;

	int yp = Rect.y1;

	int cx = Rect.x2 - Rect.x1;

	int cy = Rect.y2 - Rect.y1;

	int rx = cx;

	int ry = cy;

	if( m_pData ) {

		if( m_fTile ) {

			P2 Pos;

			Pos.y = Rect.y1;

			while( Pos.y < Rect.y2 ) {

				Pos.x = Rect.x1;

				while( Pos.x < Rect.x2 ) {

					int rop1   = IntelToHost(PWORD(m_pData)[0]);

					int stride = IntelToHost(PWORD(m_pData)[1]);

					int px     = min(m_xAct, Rect.x2 - Pos.x);

					int py     = min(m_yAct, Rect.y2 - Pos.y);

					pGDI->BitBlt( Pos.x,
						      Pos.y,
						      px,
						      py,
						      stride,
						      m_pData + 8,
						      rop1 | rop2
						      );

					Pos.x += m_xAct;
					}

				Pos.y += m_yAct;
				}
			}
		else {
			if( m_xAct && m_yAct ) {

				xp += (cx - m_xAct) / 2;

				yp += (cy - m_yAct) / 2;

				int rop1   = IntelToHost(PWORD(m_pData)[0]);

				int stride = IntelToHost(PWORD(m_pData)[1]);

				if( pShade ) {

					PCBYTE pCopy = pShade->MakeCopy(pGDI, m_pData + 8, stride / 4, m_yAct);

					pGDI->BitBlt( xp,
						      yp,
						      m_xAct,
						      m_yAct,
						      stride,
						      pCopy,
						      rop1 | rop2
						      );
					}
				else {
					pGDI->BitBlt( xp,
						      yp,
						      m_xAct,
						      m_yAct,
						      stride,
						      m_pData + 8,
						      rop1 | rop2
						      );
					}
				}
			}
		}
	}

// End of File
