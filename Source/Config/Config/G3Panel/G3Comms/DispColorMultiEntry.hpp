
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispColorMultiEntry_HPP

#define INCLUDE_DispColorMultiEntry_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CodedHost.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Multi-State Color Entry
//

class DLLNOT CDispColorMultiEntry : public CCodedHost
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispColorMultiEntry(void);

		// Operations
		void Set(CString Data);
		void Set(CString Data, CString Fore, CString Back);
		void Set(UINT uData, UINT uColor);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Data Matching
		BOOL MatchData(DWORD Data, UINT Type, BOOL fRange, DWORD &Color);

		// Type Access
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Operations
		void UpdateTypes(BOOL fComp);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		CCodedItem * m_pData;
		UINT         m_Color;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
		UINT ImportColor(UINT uData);
	};

// End of File

#endif
