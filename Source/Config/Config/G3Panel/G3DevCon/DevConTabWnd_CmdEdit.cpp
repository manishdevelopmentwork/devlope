
#include "Intern.hpp"

#include "DevConTabWnd_CmdEdit.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Device Configuration View Window -- Edit Command
//

// Base Class

#define CBaseClass CStdCmd

// Runtime Class

AfxImplementRuntimeClass(CDevConTabWnd::CCmdEdit, CBaseClass);

// Constructor

CDevConTabWnd::CCmdEdit::CCmdEdit(CString Menu, CString Item, CString Name, CString Prev, CString Data)
{
	m_Menu = Menu;

	m_Item = Item;

	m_Name = Name;

	m_Prev = Prev;

	m_Data = Data;
}

// End of File
