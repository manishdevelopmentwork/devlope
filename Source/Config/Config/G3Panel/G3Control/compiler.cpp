
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Build File
//

// Runtime Class

AfxImplementRuntimeClass(CControlCompiler, CObject);

// Constructor

CControlCompiler::CControlCompiler(CControlProject *pProject)
{
	m_pProject = pProject;
	}

// Operations

void CControlCompiler::Check(DWORD dwProgram)
{
	CStratonProgramDescriptor Desc(m_pProject->GetHandle(), dwProgram);

	CString   Name   = Desc.m_Name;

	CString   Path   = m_pProject->GetProjectPath();

	CFilename File   = Path + L"check.txt";

	BOOL      fAlien = FALSE;

	if( BuildCheckFile(File, Name, Desc.IsUdFb(), fAlien) ) {

		CTextStreamMemory Stream;

		Stream.SetPrev();

		if( Stream.LoadFromFile(File) ) {

			CBuildFile File(m_pProject);
		
			if( File.OpenLoad(Stream) ) {

				File.BuildFindList(CPrintf(CString(IDS_SYNTAX_ERRORS_FOR), Name));

				if( fAlien ) {

					afxMainWnd->Error(CString(IDS_PROGRAM_CANNOT_BE));
					}
				else
					afxMainWnd->Error(CString(IDS_PROGRAM_CONTAINS));
				}
			else {
				File.ClearFindList();

				afxMainWnd->Information(CString(IDS_PROGRAM_CONTAINS_2));
				}
			}		
		}
	}

BOOL CControlCompiler::Build(UINT uCode)
{
	CString   Path = m_pProject->GetProjectPath();

	CFilename File = Path + L"build.txt";

	BOOL   fResult = Straton_BuildDefaultFileReport(Path, File);

	if( uCode & compVerbose ) {

		CTextStreamMemory Stream;

		Stream.SetPrev();

		if( Stream.LoadFromFile(File) ) {

			CBuildFile File(m_pProject);
		
			if( File.OpenLoad(Stream) ) {

				if( uCode & compConvert ) {

					File.BuildFindList(CString(IDS_CONTROL_PROJECT_2));

					CWnd::GetActiveWindow().Error(CString(IDS_PROGRAM));

					return fResult;
					}

				if( uCode & compErrors ) {

					File.BuildFindList(CString(IDS_CONTROL_PROJECT_2));
				
					CWnd::GetActiveWindow().Error(CString(IDS_PROJECT_CONTAINS));
					}
				}			
			else {
				File.ClearFindList();

				if( uCode & compSuccess ) {

					CWnd::GetActiveWindow().Information(CString(IDS_PROJECT_BUILT));
					}
				}
			}		
		}

	return fResult;
	}

void CControlCompiler::Clean(void)
{
	CString Path = m_pProject->GetProjectPath();

	Straton_CleanProject(Path);
	}

BOOL CControlCompiler::NeedBuild(void)
{
	CString Path = m_pProject->GetProjectPath();

	return Straton_NeedBuild(Path);
	}

// Implementation

BOOL CControlCompiler::BuildCheckFile(CFilename File, CString Name, BOOL fUDFB, BOOL &fAlien)
{
	CListBox *pList = New CListBox;

	DWORD dwStyle = XS_LISTBOX | LBS_HASSTRINGS;

	if( pList->Create( dwStyle, CRect(), *afxMainWnd, 100) ) {

		CString Path = m_pProject->GetProjectPath();

		BOOL fResult = fUDFB ? Straton_BuildOneUDFB(Path, Name, pList->GetHandle(), NULL) 
				     : Straton_CheckProgram(Path, Name, pList->GetHandle(), NULL);

		UINT c = pList->GetCount();

		CTextStreamMemory Stream;

		if( Stream.OpenSave() ) {

			for( UINT n = 0; n < c; n ++ ) {

				CString Text;
				
				Text += pList->GetText(n);

				if( Text.Count(':') >= 2 ) {

					if( Text.Left(Text.Find(':')) != Name ) {

						fAlien = TRUE;
						}
					}

				Text += L"\r\n";

				Stream.PutLine(Text);
				}

			Stream.SaveToFile(File, saveRaw);
			}

		pList->DestroyWindow(TRUE);

		AfxTouch(fResult);

		return c > 0;
		}
	
	delete pList;

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Build File
//

// Runtime Class

AfxImplementRuntimeClass(CBuildFile, CObject);

// Constructor

CBuildFile::CBuildFile(CControlProject *pProject)
{
	m_pProject = pProject;

	m_pStream  = NULL;
	}

// Operations

BOOL CBuildFile::OpenLoad(ITextStream &Stream)
{
	if( !m_pStream ) {

		m_pStream = &Stream;

		ParseFile();
		
		return m_Errors.GetCount() > 0;
		}

	return FALSE;
	}

void CBuildFile::BuildFindList(CString Title)
{
	CStringArray List;
			
	if( BuildList(List) ) {
			
		CSysProxy Proxy;

		Proxy.Bind();

		Proxy.SetFindList( Title, 
				   List, 
				   TRUE
				   );
		}
	}

void CBuildFile::ClearFindList(void)
{
	CSysProxy Proxy;

	Proxy.Bind();

	CStringArray List;

	Proxy.SetFindList(L"", List, FALSE);
	}

BOOL CBuildFile::BuildList(CStringArray &List)
{
	List.Empty();

	UINT c = m_Errors.GetCount();

	for( UINT n = 0; n < c; n ++ ) {

		CString Test = m_Errors[n];

		CString Work = m_Errors[n];

		CString Left = Work.StripToken(L": ");

		if( Left == L"<var>" ) {

			CString Srch = Work.StripToken(L':');

			if( LoadVariableError( List, 
					       Srch, 
					       Work, 
					       m_Errors[n]) 
					       ){


				continue;
				}

			if( LoadVariableError( List, 
					       m_pProject->m_pGlobals, 
					       Srch, 
					       Work, 
					       m_Errors[n]) 
					       ){

				continue;
				}

			if( LoadVariableError( List, 
					       m_pProject->m_pPrograms, 
					       Srch, 
					       Work, 
					       m_Errors[n]) 
					       ){

				continue;
				}

			// NOTE -- to catch a bug in the compiler where a program created 
			//         with a name of an FB is reported as a Variable error

			if( LoadProgramError( List, 
					      Srch, 
					      Work, 
					      m_Errors[n])
					     ){
			
				continue;
				}
			}

		if( LoadVariableError( List, 
					m_pProject->m_pGlobals, 
					Left, 
					Work, 
					m_Errors[n]) 
					){
			
			continue;
			}

		if( LoadVariableError( List, 
					m_pProject->m_pPrograms, 
					Left, 
					Work, 
					m_Errors[n]) 
					){

			continue;
			}

		if( LoadProgramError( List, 
				      Left, 
				      Work,
				      m_Errors[n])
				      ){
			
			continue;
			}
		}

	return !List.IsEmpty();
	}

void CBuildFile::ParseFile(void)
{
	while( ReadLine() ) {

		if( IsError(m_sLine) ) {

			CString Line(m_sLine);

			Line.TrimRight();
			
			m_Errors.Append(Line);
			}
		}
	}

BOOL CBuildFile::ReadLine(void)
{
	return m_pStream->GetLine(m_sLine, elements(m_sLine));
	}

BOOL CBuildFile::IsError(CString Text)
{
	if( Text.Find(L": ") < NOTHING ) {

		if( !Text.StartsWith(L"Demo Mode") ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CBuildFile::LoadVariableError(CStringArray &List, CString Name, CString Desc, CString Line)
{
	UINT p1 = Name.Find(L'(');
	
	if( p1 < NOTHING ) {

		UINT p2 = Name.Find(L')', p1);
		
		if( p2 < NOTHING ) {

			CGroupVariables *pGroup;

			if( m_pProject->FindVarGroup(pGroup, Name.Mid(p1+1, p2-p1-1)) ) {
			
				return LoadVariableError(List, pGroup, Name.Left(p1), Desc, Line);
				}
			}
		}

	return LoadVariableError(List, m_pProject->m_pGlobals,  Name, Desc, Line);
	}

BOOL CBuildFile::LoadVariableError(CStringArray &List, CGroupPrograms *pGroup, CString Name, CString Desc, CString Line)
{
	Name.TrimRight();

	CControlVariable *pVariable;

	if( pGroup->FindVariable(pVariable, Name) ) {

		CString Info;

		Info += pVariable->GetHumanPath();

		Info += L", ";

		Info += Desc;

		Info += '\n';

		Info += pVariable->GetFixedPath();

		Info += L':';

		Info += L"TypeIdent";	// !!

		Info += L"!";

		Info += Line;

		List.Append(Info);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CBuildFile::LoadVariableError(CStringArray &List, CGroupVariables *pGroup, CString Name, CString Desc, CString Line)
{
	Name.TrimRight();

	CControlVariable *pVariable;

	if( pGroup->FindVariable(pVariable, Name) ) {

		CString Info;

		Info += pVariable->GetHumanPath();

		Info += L", ";

		Info += Desc;

		Info += '\n';

		Info += pVariable->GetFixedPath();

		Info += L':';

		Info += L"TypeIdent";	// !!

		Info += L"!";

		Info += Line;

		List.Append(Info);
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CBuildFile::LoadProgramError(CStringArray &List, CString Name, CString Desc, CString Line)
{
	CControlProgram *pProgram;

	if( m_pProject->FindProgram(pProgram, Name) ) {

		CString Info;

		Info += pProgram->GetHumanPath();

		Info += L" - ";

		Info += Desc;

		Info += '\n';

		Info += pProgram->GetFixedPath();

		Info += L':';

		Info += L"Code";

		Info += L"!";

		Info += Line;

		List.Append(Info);
		
		return TRUE;
		}

	return FALSE;
	}

// End of File
