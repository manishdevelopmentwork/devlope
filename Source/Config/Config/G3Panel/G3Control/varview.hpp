
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_VARVIEW_HPP
	
#define	INCLUDE_VARVIEW_HPP

/////////////////////////////////////////////////////////////////////////
//
// Variable Item View
//

class CControlVariableView : public CUIItemMultiWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CControlVariableView(CUIPageList *pList);

	protected:
		// Data Members

		// Overridables
		void OnAttach(void);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnLoadMenu(UINT uCode, CMenu &Menu);
		void OnLoadTool(UINT uCode, CMenu &Menu);
	};

// End of File

#endif
