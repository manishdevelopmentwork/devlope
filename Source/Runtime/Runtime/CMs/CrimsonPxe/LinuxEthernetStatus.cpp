
#include "Intern.hpp"

#include "LinuxEthernetStatus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Linux Ethernet Status
//

// Constructor

CLinuxEthernetStatus::CLinuxEthernetStatus(PCTXT pName, UINT uInst)
{
	m_Name  = pName;

	m_uInst = uInst;

	piob->RegisterSingleton("net.ethernet", m_uInst, (IEthernetStatus *) this);

	StdSetRef();
}

// Destructor

CLinuxEthernetStatus::~CLinuxEthernetStatus(void)
{
	piob->RevokeSingleton("net.ethernet", m_uInst);
}

// IUnknown

HRESULT CLinuxEthernetStatus::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IEthernetStatus);

	StdQueryInterface(IEthernetStatus);

	return E_NOINTERFACE;
}

ULONG CLinuxEthernetStatus::AddRef(void)
{
	StdAddRef();
}

ULONG CLinuxEthernetStatus::Release(void)
{
	StdRelease();
}

// IEthernetStatus

BOOL CLinuxEthernetStatus::GetEthernetStatus(CEthernetStatusInfo &Info)
{
	Info.m_fValid      = TRUE;

	Info.m_Device      = m_Name;

	Info.m_fCarrier    = atoi(ReadStatus("carrier"));

	Info.m_fFullDuplex = (ReadStatus("duplex") == "full");

	Info.m_uLinkSpeed  = atoi(ReadStatus("speed"));

	Info.m_MxTime      = 0;

	Info.m_TxBytes     = 0;

	Info.m_RxBytes     = 0;

	return TRUE;
}

// Implementation

CString CLinuxEthernetStatus::ReadStatus(CString Name)
{
	CAutoFile File("/./sys/class/net/" + m_Name + "/" + Name, "r");

	if( File ) {

		return File.GetLine();
	}

	return "";
}

// End of File
