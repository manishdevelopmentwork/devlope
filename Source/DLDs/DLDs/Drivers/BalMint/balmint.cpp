#define NEED_PASS_FLOAT

#include "intern.hpp"

#include "balmint.hpp"

// Instantiator

INSTANTIATE(CBaldorMintHostSerialDriver);

//////////////////////////////////////////////////////////////////////////
//
// Baldor Mint Host Master Serial Driver
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

// Constructor

CBaldorMintHostSerialDriver::CBaldorMintHostSerialDriver(void)
{
	memset(m_bTxBuff, 0, sizeof(m_bTxBuff));

	memset(m_bRxBuff, 0, sizeof(m_bRxBuff));

	m_Ident = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;
}

// Destructor

CBaldorMintHostSerialDriver::~CBaldorMintHostSerialDriver(void)
{
}

// Configuration

void MCALL CBaldorMintHostSerialDriver::Load(LPCBYTE pData)
{
}

void MCALL CBaldorMintHostSerialDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
}

// Management

void MCALL CBaldorMintHostSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
}

void MCALL CBaldorMintHostSerialDriver::Open(void)
{
}

// Device

CCODE MCALL CBaldorMintHostSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bCard = GetByte(pData);

			m_pCtx->m_bHCP  = GetByte(pData);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
		}

		return CCODE_ERROR | CCODE_HARD;
	}

	return CCODE_SUCCESS;
}

CCODE MCALL CBaldorMintHostSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx  = NULL;

		m_pDevice->SetContext(NULL);
	}

	return CMasterDriver::DeviceClose(fPersist);
}

// Entry Points

CCODE MCALL CBaldorMintHostSerialDriver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 1;
	Addr.a.m_Offset = 0x1;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
}

CCODE MCALL CBaldorMintHostSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	Start();

	MakeMin(uCount, 1);

	UINT uOffset = Addr.a.m_Offset;

	BOOL fHPC2 = m_pCtx->m_bHCP ? TRUE : FALSE;

	UINT uHi = fHPC2 ? m_pHex[uOffset / 0x10] : (uOffset / 10) + 0x30;

	UINT uLo = fHPC2 ? m_pHex[uOffset % 0x10] : (uOffset % 10) + 0x30;

	AddByte(uHi);

	AddByte(uLo);

	AddByte(ENQ);

	if( fHPC2 ) {

		AddByte(m_bCheck);
	}

	if( Transact() ) {

		InitData();

		UINT uType = Addr.a.m_Type;

		memcpy(m_Data, m_bRxBuff, m_uPtr);

		if( UINT(m_Data[0]) != uHi || UINT(m_Data[1]) != uLo ) {

			return 0;
		}

		UINT uBytes = 0;

		UINT uLen = m_uPtr;

		m_uPtr = 2;

		for( UINT u = 0; u < uCount; u++ ) {

			m_uPtr += uBytes;

			while( m_Data[uBytes] != ',' && m_Data[uBytes] != ETX ) {

				uBytes++;

				if( uBytes > DATA_MAX ) {

					break;
				}
			}

			if( uBytes > DATA_MAX ) {

				return u;
			}

			if( IsReal(uType) ) {

				pData[u] = GetReal(uBytes);
			}

			else {
				pData[u] = GetInt(uBytes);
			}

			if( m_Data[uBytes] == ETX ) {

				return u + 1;
			}

			uBytes++;
		}

		return uCount;
	}

	return CCODE_ERROR;
}

CCODE MCALL CBaldorMintHostSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	HostAlignStack();

	Start();

	MakeMin(uCount, 1);

	UINT uOffset = Addr.a.m_Offset;

	BOOL fHCP2 = m_pCtx->m_bHCP ? TRUE : FALSE;

	AddByte(fHCP2 ? m_pHex[uOffset / 0x10] : (uOffset / 10) + 0x30);

	AddByte(fHCP2 ? m_pHex[uOffset % 0x10] : (uOffset % 10) + 0x30);

	UINT uType = Addr.a.m_Type;

	UINT uData = m_uPtr;

	for( UINT u = 0; u < uCount; u++ ) {

		InitData();

		if( u > 0 ) {

			AddByte(',');
		}

		if( IsReal(uType) ) {

			SPrintf(m_Data, "%f", PassFloat(pData[u]));
		}

		else {

			SPrintf(m_Data, "%d", pData[u]);
		}

		UINT uByte = 0;

		while( uByte < DATA_MAX ) {

			if( m_Data[uByte] == 0x00 ) {

				break;
			}

			uByte++;
		}

		if( uByte < DATA_MAX - (m_uPtr - uData) ) {

			for( UINT i = 0; i < uByte; i++ ) {

				AddByte(m_Data[i]);
			}

			continue;
		}

		uCount = u;

		break;
	}

	AddByte(ETX);

	AddByte(m_bCheck);

	if( Transact() ) {

		return uCount;
	}

	return CCODE_ERROR;
}

// Implementation

void CBaldorMintHostSerialDriver::Start(void)
{
	m_uPtr = 0;

	BOOL fHCP2 = m_pCtx->m_bHCP ? TRUE : FALSE;

	AddByte(fHCP2 ? 0x7 : EOT);

	m_bCheck = 0;

	AddByte(fHCP2 ? m_pHex[m_pCtx->m_bCard / 0x10] : m_pHex[m_pCtx->m_bCard % 0x10]);

	AddByte(m_pHex[m_pCtx->m_bCard % 0x10]);

	AddByte(STX);

	if( !fHCP2 ) {

		m_bCheck = 0;
	}
}

void CBaldorMintHostSerialDriver::AddByte(BYTE bByte)
{
	m_bTxBuff[m_uPtr++] = bByte;

	m_bCheck ^= bByte;
}

// Transport Layer

BOOL CBaldorMintHostSerialDriver::Transact(void)
{
	if( Send() && Recv() ) {

		return TRUE;
	}

	return FALSE;
}

BOOL CBaldorMintHostSerialDriver::Send(void)
{
	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;
}

BOOL CBaldorMintHostSerialDriver::Recv(void)
{
	UINT uTimer = 0;

	UINT uData = 0;

	BOOL fSTX = FALSE;

	BOOL fETX = FALSE;

	m_uPtr = 0;

	m_bCheck = 0;

	SetTimer(TIME_OUT);

	while( (uTimer = GetTimer()) ) {

		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
		}

		if( m_uPtr >= sizeof(m_bRxBuff) ) {

			return FALSE;
		}

		if( !fSTX && uData == STX ) {

			m_bCheck = 0;

			fSTX = TRUE;

			continue;
		}

		if( fSTX ) {

			if( !fETX ) {

				fETX = uData == ETX;

				m_bRxBuff[m_uPtr++] = uData;

				m_bCheck ^= uData;

				continue;
			}

			return m_bCheck == uData;
		}

		if( uData == ACK ) {

			return TRUE;
		}

		if( uData == NAK ) {

			return FALSE;
		}
	}

	return FALSE;
}

// Helpers

void CBaldorMintHostSerialDriver::InitData(void)
{
	memset(m_Data, 0x00, sizeof(m_Data));
}

BOOL CBaldorMintHostSerialDriver::IsReal(UINT uType)
{
	switch( uType ) {

		case addrLongAsReal:
		case addrRealAsReal:

			return TRUE;
	}

	return FALSE;
}

DWORD CBaldorMintHostSerialDriver::GetReal(UINT uBytes)
{
	float dwFloat = 0.0;

	BOOL fNeg = FALSE;

	BOOL fDP  = FALSE;

	UINT uDiv = 10;

	for( UINT u = m_uPtr; u < uBytes; u++ ) {

		if( u == m_uPtr && m_Data[u] == '-' ) {

			fNeg = TRUE;

			continue;
		}

		if( m_Data[u] == '.' ) {

			fDP = TRUE;

			continue;
		}

		BYTE bByte = m_Data[u] - 0x30;

		if( bByte <= 9 ) {

			if( !fDP ) {

				dwFloat *= 10;

				dwFloat += bByte;
			}
			else {
				float Add = float(bByte) / float(uDiv);

				dwFloat += Add;

				uDiv *= 10;
			}
		}
	}

	if( fNeg ) {

		dwFloat = -dwFloat;
	}

	return R2I(dwFloat);
}

DWORD CBaldorMintHostSerialDriver::GetInt(UINT uBytes)
{
	DWORD dwData = 0;

	BOOL fNeg = FALSE;

	for( UINT u = m_uPtr; u < uBytes; u++ ) {

		if( u == m_uPtr && m_Data[u] == '-' ) {

			fNeg = TRUE;

			continue;
		}

		if( m_Data[u] == '.' ) {

			break;
		}

		BYTE bByte = m_Data[u] - 0x30;

		if( bByte <= 9 ) {

			dwData *= 10;

			dwData += (m_Data[u] - 0x30);
		}
	}

	return fNeg ? -dwData : dwData;
}

// End of File
