
#include "intern.hpp"

#include "stdctrl.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Controls Library 
//
// Copyright (c) 1993-2007 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Control Base Class
//

// Control Count

UINT  CStdControl::m_uCount   = 0;

// Style Data

BOOL  CStdControl::m_fStyles  = FALSE;

COLOR CStdControl::m_Shade1A  = GetRGB(31,31,31);

COLOR CStdControl::m_Shade1B  = GetRGB(16,16,16);

COLOR CStdControl::m_Shade2A  = GetRGB( 4, 4, 4);

COLOR CStdControl::m_Shade2B  = GetRGB(28,28,28);

// Constructor

CStdControl::CStdControl(void)
{
	m_pStyle  = NULL;

	m_pNotify = NULL;

	m_uID     = 0;

	m_uFlags  = 0;
	
	m_uStyle  = 0;
	
	m_pStyle  = NULL;

	m_fEnable = TRUE;

	m_fDirty  = TRUE;

	m_uCount++;
	}

// Destructor

CStdControl::~CStdControl(void)
{
	if( !--m_uCount ) {

		m_fStyles = FALSE;
		}
	}

// Implementation

BOOL CStdControl::SendNotify(UINT uCode, int nData)
{
	if( m_pNotify) {

		m_pNotify->OnNotify( m_pThis,
				     m_uID,
				     uCode,
				     nData
				     );

		return TRUE;
		}

	return FALSE;
	}

// Style Helpers

BOOL CStdControl::FindStyle(void)
{
	if( m_pStyle ) {

		if( !m_fStyles ) {

			m_fStyles = TRUE;

			GetStyles();
			}

		return TRUE;
		}

	return FALSE;
	}

void CStdControl::GetStyleColor(UINT uCode, COLOR &Color)
{
	if( FindStyle() ) {

		m_pStyle->GetColor( m_uStyle,
				    uCode,
				    Color
				    );
		}
	}

void CStdControl::GetStyleMetric(UINT uCode, int &nSize)
{
	if( FindStyle() ) {

		m_pStyle->GetMetric( m_uStyle,
				     uCode,
				     nSize
				     );
		}
	}

void CStdControl::GetStyleFont(UINT uCode, IGdiFont * &pFont)
{
	if( FindStyle() ) {

		m_pStyle->GetFont( m_uStyle,
				   uCode,
				   pFont
				   );
		}
	}

// Style Data

void CStdControl::GetStyles(void)
{
	m_pStyle->GetColor(m_uStyle, colButtonShade1A, m_Shade1A);

	m_pStyle->GetColor(m_uStyle, colButtonShade1B, m_Shade1B);
	
	m_pStyle->GetColor(m_uStyle, colButtonShade2A, m_Shade2A);
	
	m_pStyle->GetColor(m_uStyle, colButtonShade2B, m_Shade2B);
	}

// Color Mixing

#if 0

COLOR CStdControl::Mix16(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return GetRGB(rc, gc, bc);
	}

#else

DWORD CStdControl::Mix32(COLOR a, COLOR b, int p, int c)
{
	int ra = GetRED  (a);
	int ga = GetGREEN(a);
	int ba = GetBLUE (a);

	int rb = GetRED  (b);
	int gb = GetGREEN(b);
	int bb = GetBLUE (b);

	ra = (ra << 3) | (ra >> 2);
	ga = (ga << 3) | (ga >> 2);
	ba = (ba << 3) | (ba >> 2);

	rb = (rb << 3) | (rb >> 2);
	gb = (gb << 3) | (gb >> 2);
	bb = (bb << 3) | (bb >> 2);

	int rc = ra + ((rb - ra) * p / c);
	int gc = ga + ((gb - ga) * p / c);
	int bc = ba + ((bb - ba) * p / c);

	return MAKELONG(MAKEWORD(bc, gc), MAKEWORD(rc, 0xFF));
	}

#endif

// Shaders

BOOL CStdControl::Shader1(IGdi *pGDI, int p, int c)
{
	static MIXCOL q = 0;

	if( c ) {

		MIXCOL k = Mixer(m_Shade1A, m_Shade1B, p, c);

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = MIXINV;

	return FALSE;
	}

BOOL CStdControl::Shader2(IGdi *pGDI, int p, int c)
{
	static MIXCOL q = 0;

	if( c ) {

		MIXCOL k = Mixer(m_Shade2A, m_Shade2B, p, c);

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = MIXINV;

	return FALSE;
	}

BOOL CStdControl::Shader3(IGdi *pGDI, int p, int c)
{
	static MIXCOL q = 0;

	if( c ) {

		MIXCOL k = Mixer(m_Shade2B, m_Shade2A, p, c);

		if( k - q ) {

			pGDI->SetBrushFore(k);

			q = k;

			return TRUE;
			}

		return FALSE;
		}

	q = MIXINV;

	return FALSE;
	}

// End of File
