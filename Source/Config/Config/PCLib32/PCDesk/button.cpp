
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Button Gadget
//

// Runtime Class

AfxImplementRuntimeClass(CButtonGadget, CGadget);
		
// Constructors

CButtonGadget::CButtonGadget(UINT uID, DWORD Image, PCTXT pText)
{
	m_uID    = uID;

	m_Image  = Image;

	m_Text   = pText;

	m_Size   = CSize(24, 24);
	
	m_uProps = tbgShowTip | ((HIBYTE(uID) == IDM_HELP) ? tbgRight : 0);
	
	m_uFlags = 0;
	}

CButtonGadget::CButtonGadget(UINT uID, DWORD Image, BOOL fRight)
{
	m_uID    = uID;

	m_Image  = Image;

	m_Size   = CSize(18, 18);
	
	m_uProps = tbgShowTip | (fRight ? tbgRight : 0);
	
	m_uFlags = 0;
	}

// Overridables

void CButtonGadget::OnCreate(CDC &DC)
{
	m_Size.cx += afxButton->Adjust(DC, m_Image, m_Text);
	}

void CButtonGadget::OnPaint(CDC &DC)
{
	afxButton->Draw(DC, m_Rect, m_Image, m_Text, m_uFlags);
	}

BOOL CButtonGadget::OnMouseDown(CPoint const &Pos)
{
	if( !TestFlag(MF_DISABLED) ) {
		
		SetFlag(MF_PRESSED);
		
		Update();
		
		return TRUE;
		}
		
	return FALSE;
	}

void CButtonGadget::OnMouseMove(CPoint const &Pos, BOOL fHit)
{
	AdjustFlag(MF_PRESSED, fHit);
	}

void CButtonGadget::OnMouseUp(CPoint const &Pos, BOOL fHit)
{
	if( TestFlag(MF_PRESSED) ) {
	
		DoAction();
			
		ClearFlag(MF_PRESSED);
		}
	}

BOOL CButtonGadget::OnGetTip(CWnd *pPrivate, BOOL fShift, CString &Tip)
{
	// TODO -- We need a lot more of this...

	CWnd *pWnd = pPrivate ? pPrivate : &m_pWnd->GetDlgParent();
		
	CCmdInfo Info;

	if( pWnd->RouteGetInfo(m_uID, Info) ) {
		
		if( !Info.m_ToolTip.IsEmpty() ) {
			
			if( fShift ) {

				Tip = Info.m_Prompt;
				}
			else
				Tip = Info.m_ToolTip;
			}
		else
			Tip = Info.m_Prompt;
		}
	else
		Tip.Empty();
	
	return TRUE;
	}

void CButtonGadget::OnSetFlags(void)
{
	Update();
	}

void CButtonGadget::OnIdle(CWnd *pPrivate)
{
	CCmdSourceData Src;
	
	Src.PrepareSource();

	CWnd *pWnd = pPrivate ? pPrivate : &CWnd::GetActiveWindow();
	
	pWnd->RouteControl(m_uID, Src);
	
	UINT uMask = (MF_DISABLED | MF_CHECKED);
	
	UINT uTest = ((m_uFlags & ~uMask) | (Src.GetFlags() & uMask));

	if( m_uFlags != uTest ) {
	
		m_uFlags = uTest;
		
		Update();
		}
	}

// Protected Constructor

CButtonGadget::CButtonGadget(void)
{
	m_Size   = CSize(24, 24);
	
	m_uFlags = 0;
	}
		
// Overridables

void CButtonGadget::DoAction(void)
{
	CWnd &Wnd = CWnd::GetActiveWindow();
	
	Wnd.PostMessage(WM_COMMAND, m_uID);
	}

// End of File
