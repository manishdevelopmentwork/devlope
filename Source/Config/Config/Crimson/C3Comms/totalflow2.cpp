
#include "intern.hpp"

#include "totalflow2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

#define	MAX_SLOT	65535

#define SLOT_START	addrNamed

#define MAX_STRING	(addrNamed - 1)

#define STRING_START	1

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Slot
//

bool CTotalFlowSlot::operator ==(const CTotalFlowSlot &That)
{
	return	m_App	   == That.m_App  &&
		m_Arr	   == That.m_Arr  &&
		m_Reg	   == That.m_Reg  &&
		m_Type	   == That.m_Type &&
		m_IsString == That.m_IsString;
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Application Definition
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlowApp, CMetaItem);

// Constructor

CTotalFlowApp::CTotalFlowApp(void)
{
	m_Lookup = 0;
	
	m_App    = 0;

	AddMeta();
	}

CTotalFlowApp::CTotalFlowApp(CString Name, UINT uNum)
{
	m_Name   = Name;

	m_Lookup = uNum;

	m_App	 = uNum;

	AddMeta();
	}

CTotalFlowApp::CTotalFlowApp(CTotalFlowApp * pApp)
{
	if( pApp ) {

		m_Name   = pApp->m_Name;

		m_Lookup = pApp->m_Lookup;

		m_App    = pApp->m_App;
		}

	AddMeta();
	}

// Destructor

CTotalFlowApp::~CTotalFlowApp(void)
{
	}

// Download Support

BOOL CTotalFlowApp::MakeInitData(CInitData &Init)	
{
	Init.AddByte(BYTE(m_Lookup));

	Init.AddByte(BYTE(m_App));

	return TRUE;
	}

// Comparision Operators

bool CTotalFlowApp::operator ==(const CTotalFlowApp &That) const
{
	return	m_App == That.m_App;
	}

bool CTotalFlowApp::operator < (const CTotalFlowApp &That) const
{
	return m_App < That.m_App;
	}

bool CTotalFlowApp::operator > (const CTotalFlowApp &That) const
{
	return m_App > That.m_App;
	}

// Meta Data Creation

void CTotalFlowApp::AddMetaData(void)	
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Lookup);

	Meta_AddInteger(App);

	Meta_AddString(Name);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Application List
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlowAppList, CItemList);

// Constructor

CTotalFlowAppList::CTotalFlowAppList(void)
{	
	}

// Destructor

CTotalFlowAppList::~CTotalFlowAppList(void)
{
	DeleteAllItems(TRUE);
	}

// Item Access

CTotalFlowApp * CTotalFlowAppList::GetItem(INDEX Index) const
{
	return (CTotalFlowApp *) CItemList::GetItem(Index);
	}

CTotalFlowApp * CTotalFlowAppList::GetItem(UINT uPos) const
{
	return (CTotalFlowApp *) CItemList::GetItem(uPos);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlow2MasterDeviceOptions, CUIItem);

// Constructor

CTotalFlow2MasterDeviceOptions::CTotalFlow2MasterDeviceOptions(void)
{
	m_Name          = L"TOTALFLOW";

	m_Code          = L"0000";

	m_Update        = 2500;

	m_Timeout       = 2000;

	m_UseApp        = 0;

	m_StringSize    = 1;

	m_pApplications = New CTotalFlowAppList;
	}

// Destructor

CTotalFlow2MasterDeviceOptions::~CTotalFlow2MasterDeviceOptions(void)
{
	}

// UI Management

void CTotalFlow2MasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "AppConfig" ) {

		HGLOBAL hPrev = m_pApplications->TakeSnapshot();

		CTotalFlow2AppDialog Dlg(this);

		if( Dlg.Execute(*afxMainWnd) ) {
				
			HGLOBAL        hData = m_pApplications->TakeSnapshot();

			CCmdSubItem *  pCmd  = New CCmdSubItem( L"Config/Applications",
								hPrev,
								hData
								);
			if( pCmd->IsNull() ) {

				delete pCmd;
				}
			else {  
				pHost->SaveExtraCmd(pCmd);

				SetDirty();
				}
			}
		else
			GlobalFree(hPrev);
			
		}

	CUIViewWnd& Wnd = (CUIViewWnd &)pHost->GetWindow();

	if( Tag == "AppImport" ) {

		if( m_pApplications->GetItemCount() > 0 ) {

			UINT uReply = Wnd.YesNo(CString(IDS_THIS_OPERATION));

			if( uReply != IDYES ) {

				return;
				}
			}

		OnImportApps(&Wnd);
		}

	if( Tag == "AppExport" ) {

		OnExportApps(&Wnd);
		}

	if( Tag == "Purge" ) {

		Purge(&Wnd);
		}

	if( Tag.IsEmpty() || Tag == "UseApp" ) {

		BOOL fEnable = pItem->GetDataAccess("UseApp")->ReadInteger(pItem);

		pHost->EnableUI("AppConfig", fEnable);

		pHost->EnableUI("AppImport", fEnable);

		pHost->EnableUI("AppExport", fEnable);
		}
	}

// Download Support

BOOL CTotalFlow2MasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddText(m_Name);
	Init.AddText(m_Code);
	Init.AddLong(m_Update);
	Init.AddLong(m_Timeout);
	Init.AddByte(BYTE(m_UseApp));

	MakeAppInit(Init);

	MakeSlotInit(Init);

	return TRUE;
	}

void CTotalFlow2MasterDeviceOptions::MakeSlotInit(CInitData &Init)
{
	Init.AddLong(m_Slots.GetCount());

	for( INDEX n = m_Slots.GetHead(); !m_Slots.Failed(n); m_Slots.GetNext(n) ) {

		CTotalFlowSlot Slot = m_Slots.GetAt(n);

		UINT uSize = GetSlotSize(Slot);
		
		Init.AddLong(LONG(Slot.m_Slot));
		Init.AddLong(LONG(Slot.m_App));
		Init.AddLong(LONG(Slot.m_Arr));
		Init.AddLong(LONG(Slot.m_Reg));
		Init.AddWord(WORD(Slot.m_Type));
		Init.AddWord(WORD(uSize));
		Init.AddByte(BYTE(Slot.m_IsString));
		}
	}

void CTotalFlow2MasterDeviceOptions::MakeAppInit(CInitData &Init)
{
	m_pApplications->MakeInitData(Init);
	}

// Implementation

UINT CTotalFlow2MasterDeviceOptions::GetSlotSize(CTotalFlowSlot const &Slot)
{
	if( Slot.m_IsString ) {

		return m_StringSize == 0 ? 6 : 16;
		}

	return Slot.m_Size;
	}

BOOL CTotalFlow2MasterDeviceOptions::Expand(CTotalFlowSlot Slot, CAddress Addr, CString &Text)
{
	if( Addr.a.m_Extra & 1 ) {

		Text.Printf("%s.%u.%u.S.%u", ExpandApp(Slot.m_App),
					     Slot.m_Arr,
					     Slot.m_Reg,
					     Addr.a.m_Offset % (m_StringSize == 0 ? 6 : 16));
				
			return TRUE;
			}

	char cType = 'R';

	switch( Addr.a.m_Type ) {

		case addrByteAsByte: cType = 'B'; break;
		case addrWordAsWord: cType = 'W'; break;
		case addrLongAsLong: cType = 'L'; break;
		case addrRealAsReal: cType = 'R'; break;
		}

	Text.Printf("%s.%u.%u.%c", ExpandApp(Slot.m_App), 
				   Slot.m_Arr,
				   Slot.m_Reg,
				   cType);
						
	return TRUE;
	}


void CTotalFlow2MasterDeviceOptions::OnImportApps(CWnd *pWnd)
{
	COpenFileDialog Dlg;

	CString LastPath(TEXT("ABB Applications"));

	Dlg.LoadLastPath(LastPath);

	Dlg.SetCaption(CString("Import Applications"));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("rt"))) ) {

			CString Text = TEXT("Unable to open file for reading.");

			pWnd->Error(Text);
			}
		else {
			CErrorArray Errors;

			if( ImportApps(pFile, Errors) ) {

				SetDirty();
				}

			if( Errors.GetCount() > 0 ) {

				UINT uCount = Errors.GetCount();

				UINT uDisplay = min(uCount, 10);

				CString Message = CPrintf( "Import found %u error%s.\n",
							   uCount,
							   uCount > 1 ? "s" : ""
							   );

				if( uDisplay < uCount ) {

					Message += CPrintf("The first %u errors are shown:\n", uDisplay);
					}

				for( UINT n = 0; n < uDisplay; n++ ) {

					Message += CPrintf("Line %u: %s\n", Errors[n].m_uLine, Errors[n].m_Message);
					}

				pWnd->Information(Message);
				}
			
			fclose(pFile);
			}

		Dlg.SaveLastPath(LastPath);
		}
	}

void CTotalFlow2MasterDeviceOptions::OnExportApps(CWnd *pWnd)
{
	CSaveFileDialog Dlg;

	CString LastPath(TEXT("ABB Applications"));

	Dlg.LoadLastPath(LastPath);

	Dlg.SetCaption(CString("Export Applications"));

	Dlg.SetFilter(TEXT("CSV Files|*.csv"));

	if( Dlg.Execute(*pWnd) ) {

		FILE *pFile;
		
		if( pFile = fopen(Dlg.GetFilename(), TEXT("rb")) ) {
			
			fclose(pFile);

			CString Text = CString(IDS_SELECTED_FILE);

			if( pWnd->YesNo(Text) == IDNO ) {

				return;
				}
			}

		if( !(pFile = fopen(Dlg.GetFilename(), TEXT("wt"))) ) {

			CString Text = CString(IDS_UNABLE_TO_OPEN_2);

			pWnd->Error(Text);

			return;
			}

		ExportApps(pFile);

		fclose(pFile);

		Dlg.SaveLastPath(LastPath);
		}
	}

BOOL CTotalFlow2MasterDeviceOptions::ImportApps(FILE *pFile, CErrorArray &Errors)
{
	DWORD dwPos = ftell(pFile);

	UINT uLine = 0;

	BOOL fInit = TRUE;

	fseek(pFile, dwPos, SEEK_SET);

	while( !feof(pFile) ) {

		char sLine[256] = {0};

		fgets(sLine, sizeof(sLine), pFile);

		if( sLine[0] ) {

			uLine ++;

			CStringArray List;

			sLine[strlen(sLine)-1] = 0;

			CString(sLine).Tokenize(List, ',');

			if( fInit ) {

				if( List.GetCount() == 2 ) {

					if( List[0] == "APPLICATION" && List[1] == "NUMBER") {
						
						fInit = FALSE;

						m_pApplications->DeleteAllItems(TRUE);

						continue;
						}
					}

				LogImportError(Errors, CString(IDS_CSV_HEADER_IS), uLine);

				return FALSE;
				}

			if( List.GetCount() == 2 ) {

				CString AppName(List[0]);

				UINT uAppNum = wcstoul(List[1], NULL, 10);

				if( !AddApplication(AppName, uAppNum) ) {

					CString Text =  CPrintf(CString(IDS_DUPLICATE), uAppNum, PCTXT(AppName));

					LogImportError(Errors, Text, uLine);
					}
				}
			else {
				LogImportError(Errors, CString(IDS_INCORRECT_NUMBER), uLine);
				}
			}
		}

	return TRUE;
	}

BOOL CTotalFlow2MasterDeviceOptions::ExportApps(FILE *pFile)
{
	fprintf(pFile, TEXT("APPLICATION,NUMBER\n"));

	UINT uCount = m_pApplications->GetItemCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CTotalFlowApp * pApp = m_pApplications->GetItem(n);		

		fprintf(pFile,	TEXT("%s,%d\n"),
				PCTXT(pApp->m_Name),
				pApp->m_App
				);
		}

	return TRUE;
	}

void CTotalFlow2MasterDeviceOptions::LogImportError(CErrorArray &Errors, CString Message, UINT uLine)
{
	CImportError Error;

	Error.m_Message	= Message;
	Error.m_uLine	= uLine;

	Errors.Append(Error);
	}

void CTotalFlow2MasterDeviceOptions::Purge(CUIViewWnd *pWnd)
{
	CSystemItem *pSystem = GetDatabase()->GetSystemItem();

	if( pSystem ) {

		EmptySlots();

		pSystem->Rebuild(1);

		pWnd->Information(CString(IDS_OPERATION_IS));
		}
	}

void CTotalFlow2MasterDeviceOptions::PrintSlots(void)
{
	UINT uPos = 0;

	AfxTrace(L"\nSlot List count %u", m_Slots.GetCount());

	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i), uPos++ ) {

		CTotalFlowSlot Slot = m_Slots[i];

		AfxTrace(L"\nSlot %u - %u.%u.%u size %u type %u string %u slot %8.8x", uPos, Slot.m_App,
											     Slot.m_Arr,
											     Slot.m_Reg,
											     Slot.m_Size,
											     Slot.m_Type,
											     Slot.m_IsString,
											     Slot.m_Slot);
		}
	}

// Slot Access

BOOL CTotalFlow2MasterDeviceOptions::GetSlot(UINT uAddr, CTotalFlowSlot &Slot)
{
	INDEX iFind = m_SlotIndex.FindName(uAddr);

	if( !m_SlotIndex.Failed(iFind) ) {

		INDEX i = m_SlotIndex[iFind];

		if( !m_Slots.Failed(i) ) {

			Slot = m_Slots[i];

			return TRUE;
			}
		}
	
	return FALSE;
	}

UINT CTotalFlow2MasterDeviceOptions::AddSlot(CTotalFlowSlot &Slot, UINT uStart, UINT uEnd)
{
	INDEX i = m_Slots.Find(Slot);

	if( m_Slots.Failed(i) ) {

		UINT uSlot = FindNextSlot(uStart, uEnd);

		if( uSlot != NOTHING ) {

			Slot.m_Slot = uSlot;

			i = m_Slots.Append(Slot);

			m_SlotIndex.Insert(uSlot, i);
			}

		return uSlot;
		}

	return m_Slots.GetAt(i).m_Slot;
	}

void CTotalFlow2MasterDeviceOptions::DeleteSlot(CTotalFlowSlot &Slot)
{
	INDEX i = m_Slots.Find(Slot);

	if( !m_Slots.Failed(i) ) {

		INDEX idx = m_SlotIndex.FindData(i);

		if( !m_SlotIndex.Failed(idx) ) {

			m_SlotIndex.Remove(idx);
			}

		m_Slots.Remove(i);
		}
	}

void CTotalFlow2MasterDeviceOptions::EmptySlots(void)
{
	m_Slots.Empty();

	m_SlotIndex.Empty();
	}

UINT CTotalFlow2MasterDeviceOptions::FindNextSlot(UINT uStart, UINT uEnd)
{
	// TODO:  Allow strings to "pack" within a table (sStrings currently use entire table which only allows 239 strings per device).

	for( UINT n = uStart; n < uEnd; n++ ) {

		UINT uSlot = n;

		if( m_SlotIndex.Failed(m_SlotIndex.FindName(uSlot)) ) {

			return uSlot;
			}
		}

	return NOTHING;
	}

// Application Management

BOOL CTotalFlow2MasterDeviceOptions::AddApplication(CString const &Name, UINT uNumber)
{
	CTotalFlowApp * pApp = New CTotalFlowApp;
	
	pApp->m_Name = Name;

	pApp->m_App = uNumber;

	pApp->m_Lookup = uNumber;

	INDEX i = m_pApplications->FindItemIndex(pApp);

	if( m_pApplications->Failed(i) ) {

		m_pApplications->AppendItem(pApp);

		GetDatabase()->SetDirty();

		return TRUE;
		}

	delete pApp;

	return FALSE;
	}

BOOL CTotalFlow2MasterDeviceOptions::UsingNamedApps(void)	
{
	return m_UseApp ? TRUE : FALSE;
	}

UINT CTotalFlow2MasterDeviceOptions::ParseApp(CString Text)
{
	if( UsingNamedApps() ) {

		UINT uCount = m_pApplications->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CTotalFlowApp * pApp = m_pApplications->GetItem(u);

			if( pApp->m_Name == Text ) {

				return pApp->m_Lookup;
				}
			}
		}

	return watoi(Text);
	}

CString CTotalFlow2MasterDeviceOptions::ExpandApp(UINT uApp)
{
	if( UsingNamedApps() ) {

		UINT uCount = m_pApplications->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CTotalFlowApp * pApp = m_pApplications->GetItem(u);

			if( pApp->m_Lookup == uApp ) {

				return pApp->m_Name;
				}
			}
		}

	return CPrintf(L"%u", uApp);	
	}

CString CTotalFlow2MasterDeviceOptions::FindApp(UINT uApp)
{
	if( UsingNamedApps() ) {

		UINT uCount = m_pApplications->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CTotalFlowApp * pApp = m_pApplications->GetItem(u);

			if( pApp->m_Lookup == uApp ) {

				return CPrintf(L"%u", pApp->m_App);
				}
			}
		}

	return CPrintf(L"%u", uApp);	
	}

CString CTotalFlow2MasterDeviceOptions::FindApp(CString Num)
{
	if( UsingNamedApps() ) {

		UINT uCount = m_pApplications->GetItemCount();

		for( UINT u = 0; u < uCount; u++ ) {

			CTotalFlowApp * pApp = m_pApplications->GetItem(u);

			if( pApp->m_App == UINT(watoi(Num)) ) {

				return pApp->m_Name;
				}
			}
		}
	
	return Num;
	}

// Persistence

void CTotalFlow2MasterDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "SlotList" ) {

			Tree.GetObject();

			Tree.GetName();

			UINT uSlots = Tree.GetValueAsInteger();

			for( UINT n = 0; n < uSlots; n++ ) {

				CTotalFlowSlot Slot;

				Tree.GetCollect();

				Tree.GetName();

				Slot.m_App = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.m_Arr = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.m_Reg = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.m_Type = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.m_Size = Tree.GetValueAsInteger();

				Tree.GetName();

				Slot.m_IsString = Tree.GetValueAsInteger();

				if( Slot.m_IsString )

					AddSlot(Slot, STRING_START, MAX_STRING);
				else
					AddSlot(Slot, SLOT_START, MAX_SLOT);				

				Tree.EndCollect();
				}

			Tree.EndObject();
			}

		if( Name == "AppList" ) {

			Tree.GetObject();

			Tree.GetName();

			m_pApplications->SetParent(this);

			UINT uApps = Tree.GetValueAsInteger();

			for( UINT n = 0; n < uApps; n++ ) {

				Tree.GetCollect();

				Tree.GetName();

				CString Name = Tree.GetValueAsString();

				Tree.GetName();

				UINT uApp = Tree.GetValueAsInteger();

				CTotalFlowApp * pApp = New CTotalFlowApp(Name, uApp);

				if( Tree.GetName() == CString("AppLook") ) {

					pApp->m_Lookup = Tree.GetValueAsInteger();
					}

				m_pApplications->AppendItem(pApp);

				Tree.EndCollect();
				}

			Tree.EndObject();
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CTotalFlow2MasterDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutObject(L"SlotList");

	UINT uSlots = m_Slots.GetCount();

	Tree.PutValue(L"SlotCount", uSlots);

	for( INDEX i = m_Slots.GetHead(); !m_Slots.Failed(i); m_Slots.GetNext(i) ) {

		CTotalFlowSlot Slot = m_Slots[i];

		Tree.PutCollect(L"Slot");

		Tree.PutValue(L"App", Slot.m_App);

		Tree.PutValue(L"Arr", Slot.m_Arr);

		Tree.PutValue(L"Reg", Slot.m_Reg);

		Tree.PutValue(L"Type", Slot.m_Type);

		Tree.PutValue(L"Size", Slot.m_Size);

		Tree.PutValue(L"IsString", Slot.m_IsString);

		Tree.EndCollect();
		}

	Tree.EndObject();
	}

// Meta Data Creation

void CTotalFlow2MasterDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Name);
	Meta_AddString(Code);
	Meta_AddInteger(Update);
	Meta_AddInteger(Timeout);
	Meta_AddInteger(StringSize);
	Meta_AddInteger(AppConfig);
	Meta_AddInteger(AppImport);
	Meta_AddInteger(AppExport);
	Meta_AddInteger(Purge);
	Meta_AddInteger(UseApp);
	Meta_AddCollect(Applications);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Serial Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlow2SerialMasterDeviceOptions, CTotalFlow2MasterDeviceOptions);

// Constructor

CTotalFlow2SerialMasterDeviceOptions::CTotalFlow2SerialMasterDeviceOptions(void)
{
	m_Estab = 1;

	m_Time1 = 2000;

	m_UnkeyDelay = 50;
	}

// UI Management

void CTotalFlow2SerialMasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	CTotalFlow2MasterDeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CTotalFlow2SerialMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlow2MasterDeviceOptions::MakeInitData(Init);

	Init.AddWord(WORD(m_Estab));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_UnkeyDelay));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlow2SerialMasterDeviceOptions::AddMetaData(void)
{
	CTotalFlow2MasterDeviceOptions::AddMetaData();
	
	Meta_AddInteger(Time1);
	Meta_AddInteger(Estab);
	Meta_AddInteger(UnkeyDelay);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Master Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CTotalFlow2TcpMasterDeviceOptions, CTotalFlow2MasterDeviceOptions);

// Constructor

CTotalFlow2TcpMasterDeviceOptions::CTotalFlow2TcpMasterDeviceOptions(void)
{
	m_IP     = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Port   = 9999;

	m_Keep   = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CTotalFlow2TcpMasterDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Keep" ) {

		pHost->EnableUI("Time3", !m_Keep);
		}

	CTotalFlow2MasterDeviceOptions::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CTotalFlow2TcpMasterDeviceOptions::MakeInitData(CInitData &Init)
{
	CTotalFlow2MasterDeviceOptions::MakeInitData(Init);

	Init.AddLong(m_IP);
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_Ping));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CTotalFlow2TcpMasterDeviceOptions::AddMetaData(void)
{
	CTotalFlow2MasterDeviceOptions::AddMetaData();

	Meta_AddInteger(IP);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Ping);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Master Comms Driver
//

// Constructor

CTotalFlow2MasterDriver::CTotalFlow2MasterDriver(void)
{
	m_uType		= driverMaster;

	m_Manufacturer	= "ABB";

	m_Version	= "2.12";

	m_DriverName	= "TotalFlow Enhanced Master";

	m_ShortName	= "TotalFlow Enhanced";

	m_DevRoot	= "METER";
	}

// Destructor

CTotalFlow2MasterDriver::~CTotalFlow2MasterDriver(void)
{
	}

// Address Management

BOOL CTotalFlow2MasterDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CTotalFlow2Dialog Dlg(this, Addr, pConfig, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

BOOL CTotalFlow2MasterDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CTotalFlow2MasterDeviceOptions *pDevice = (CTotalFlow2MasterDeviceOptions *) pConfig;

	CStringArray Part;

	Text.Tokenize(Part, '.');

	if( Part.GetCount() == 3 || Part.GetCount() == 4 ) {

		Addr.a.m_Table	= addrNamed;

		Addr.a.m_Extra  = 0;

		if( Part.GetCount() == 3 ) {		

			Addr.a.m_Type = addrRealAsReal;
			}

		if( Part.GetCount() == 4 ) {

			if( Part[3].GetLength() == 1 ) {

				switch( Part[3].ToUpper()[0] ) {

					case 'B':

						Addr.a.m_Type = addrByteAsByte;

						break;

					case 'W':

						Addr.a.m_Type = addrWordAsWord;

						break;

					case 'L':

						Addr.a.m_Type = addrLongAsLong;

						break;

					case 'R':

						Addr.a.m_Type = addrRealAsReal;

						break;

					case 'S':

						Error.Set(CString(IDS_STRING_REFERENCES));

						return FALSE;

					default:
						Error.Set(CString(IDS_TYPE_SUFFIX_MUST));

						return FALSE;
					}

				}

			}

		CTotalFlowSlot Slot;

		Slot.m_IsString = FALSE;

		Slot.m_App  = pDevice->ParseApp(Part[0]);

		Slot.m_Arr  = watoi(Part[1]);

		Slot.m_Reg  = watoi(Part[2]);

		if( CheckBounds(Slot.m_App, Slot.m_Arr, Slot.m_Reg) ) {

			Slot.m_Type = Addr.a.m_Type;

			Slot.m_Size = 1;

			UINT uPos = pDevice->AddSlot(Slot, SLOT_START, MAX_SLOT);

			if( uPos != NOTHING ) {	

				Addr.a.m_Offset = uPos;

				return TRUE;
				}

			Error.Set(CString(IDS_THIS_DEVICE));

			return FALSE;
			}
		else {
			Error.Set(CString(IDS_ADDRESS_OUT_OF));
				
			return FALSE;
			}
		}

	if( Part.GetCount() == 5 ) {

		Addr.a.m_Type   = addrLongAsLong;

		Addr.a.m_Extra  = 0;

		if( Part[3] == L"S" ) {

			Addr.a.m_Extra |= 1;

			CTotalFlowSlot Slot;

			Slot.m_App	= pDevice->ParseApp(Part[0]);
			Slot.m_Arr	= watoi(Part[1]);
			Slot.m_Reg	= watoi(Part[2]);
			Slot.m_Type	= Addr.a.m_Type;
			Slot.m_Size	= 16;
			Slot.m_IsString	= TRUE;

			UINT uOffset	= watoi(Part[4]);

			UINT uPos = pDevice->AddSlot(Slot, STRING_START, MAX_STRING);

			if( uPos != NOTHING ) {

				Addr.a.m_Table	= uPos;

				Addr.a.m_Offset	= uOffset;

				return TRUE;
				}

			Error.Set(CString(IDS_THIS_DEVICE_2));

			return FALSE;
			}
		}

	Error.Set(CString(IDS_INVALID_ADDRESS));

	return FALSE;
	}

BOOL CTotalFlow2MasterDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CTotalFlow2MasterDeviceOptions * pDevice = (CTotalFlow2MasterDeviceOptions *) pConfig;

	CTotalFlowSlot Slot;

	UINT uAddr = GetAddress(Addr);

	if( pDevice->GetSlot(uAddr, Slot) ) {

		if( pDevice->Expand(Slot, Addr, Text) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTotalFlow2MasterDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return FALSE;
	}

// Notifications

void CTotalFlow2MasterDriver::NotifyInit(CItem * pConfig)
{
	CTotalFlow2MasterDeviceOptions * pOpt = (CTotalFlow2MasterDeviceOptions *)pConfig;

	if( pOpt ) {

		pOpt->EmptySlots();
		}
	}

// Address Reference
		
UINT CTotalFlow2MasterDriver::GetAddress(CAddress Addr)
{
	return (Addr.a.m_Extra & 1) ? Addr.a.m_Table : Addr.a.m_Offset;
	}

// Address Helpers

BOOL CTotalFlow2MasterDriver::CheckBounds(UINT uApp, UINT uArr, UINT uReg)
{
	return	uApp <= 255 &&
		uArr <= 255 &&
		uReg <= 65535;
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Serial Master Comms Driver
//

// Instantiator

ICommsDriver * Create_TotalFlow2SerialMasterDriver(void)
{
	return New CTotalFlow2SerialMasterDriver();
	}

// Constructor

CTotalFlow2SerialMasterDriver::CTotalFlow2SerialMasterDriver(void)
{
	m_wID = 0x40C4;
	}

// Destructor

CTotalFlow2SerialMasterDriver::~CTotalFlow2SerialMasterDriver(void)
{
	}

// Binding

UINT CTotalFlow2SerialMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CTotalFlow2SerialMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CTotalFlow2SerialMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CTotalFlow2SerialMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTotalFlow2SerialMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced TCP/IP Master Comms Driver
//

// Instantiator

ICommsDriver * Create_TotalFlow2TcpMasterDriver(void)
{
	return New CTotalFlow2TcpMasterDriver();
	}

// Constructor

CTotalFlow2TcpMasterDriver::CTotalFlow2TcpMasterDriver(void)
{
	m_wID = 0x40C5;
	}

// Destructor

CTotalFlow2TcpMasterDriver::~CTotalFlow2TcpMasterDriver(void)
{
	}

// Binding

UINT CTotalFlow2TcpMasterDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CTotalFlow2TcpMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount  = 1;

	Ether.m_UDPCount  = 0;
	}

// Configuration

CLASS CTotalFlow2TcpMasterDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CTotalFlow2TcpMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CTotalFlow2TcpMasterDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CTotalFlow2Dialog, CStdDialog);

// Constructor

CTotalFlow2Dialog::CTotalFlow2Dialog(CTotalFlow2MasterDriver * pDriver, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	m_pAddr   = &Addr;

	m_pConfig = pConfig;

	m_pDriver = pDriver;

	m_fPart   = fPart;

	SetName(TEXT("TotalFlow2Dlg"));
	}
		
// Message Map

AfxMessageMap(CTotalFlow2Dialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_COMMAND)

	AfxDispatchCommand(IDOK, OnOkay)
	
	AfxDispatchNotify(201, EN_CHANGE,	OnAppNumChange)
	AfxDispatchNotify(205, CBN_SELCHANGE,	OnSelChange)

	AfxMessageEnd(CTotalFlow2Dialog)
	};
		
// Message Handlers

BOOL CTotalFlow2Dialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	LoadTypeList();

	LoadApplicationList();

	CComboBox &Box = (CComboBox &) GetDlgItem(204);

	CTotalFlow2MasterDeviceOptions *pDevice = (CTotalFlow2MasterDeviceOptions *) m_pConfig;

	if( m_pAddr->m_Ref ) {

		CTotalFlowSlot Slot;

		UINT uSlot = m_pDriver->GetAddress(*m_pAddr);
		
		if( pDevice->GetSlot(uSlot, Slot) ) {
			
			if( m_pAddr->a.m_Extra ) {

				Box.SelectData(0);

				GetDlgItem(201).SetWindowText(pDevice->FindApp(Slot.m_App));

				GetDlgItem(202).SetWindowText(CPrintf(L"%u", Slot.m_Arr));

				GetDlgItem(203).SetWindowText(CPrintf(L"%u", Slot.m_Reg));
				}
			else {
				Box.SelectData(m_pAddr->a.m_Type);

				GetDlgItem(201).SetWindowText(pDevice->FindApp(Slot.m_App));

				GetDlgItem(202).SetWindowText(CPrintf(L"%u", Slot.m_Arr));

				GetDlgItem(203).SetWindowText(CPrintf(L"%u", Slot.m_Reg));
				}
			}
		}
	else {
		Box.SelectData(addrWordAsWord);

		if( GetDlgItem(201).GetWindowText().IsEmpty() ) {

			GetDlgItem(201).SetWindowText(L"0");
			}

		GetDlgItem(202).SetWindowText(L"0");

		GetDlgItem(203).SetWindowText(L"0");
		}

	return TRUE;
	}

void CTotalFlow2Dialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(205);

	UINT uApp = Combo.GetCurSelData();

	BOOL fApp = uApp != NOTHING;

	GetDlgItem(201).EnableWindow(!fApp);

	if( fApp ) {

		CString Text = CPrintf("%u", uApp);
		
		GetDlgItem(201).SetWindowText(Text);
		}
	else {
		GetDlgItem(201).SetFocus();
		}
	}

void CTotalFlow2Dialog::OnAppNumChange(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(205);

	if( Combo.IsEnabled() ) {

		CString Text = GetDlgItem(201).GetWindowText();

		UINT uApp = wcstol(Text, NULL, 10);

		for( UINT n = 0; n < Combo.GetCount(); n++ ) {

			UINT uData = Combo.GetItemData(n);

			if( uData == uApp ) {

				Combo.SetCurSel(n);

				return;
				}
			}

		Combo.SetCurSel(0);
		}
	}

// Command Handlers

BOOL CTotalFlow2Dialog::OnOkay(UINT uID)
{
	CTotalFlow2MasterDeviceOptions *pDevice = (CTotalFlow2MasterDeviceOptions *) m_pConfig;

	CError Error(TRUE);

	CAddress Addr;

	CString Text;

	Text += pDevice->FindApp(GetDlgItem(201).GetWindowText()) + ".";

	Text += GetDlgItem(202).GetWindowText() + ".";

	Text += GetDlgItem(203).GetWindowText() + ".";

	UINT uData = ((CComboBox &) GetDlgItem(204)).GetCurSelData();

	switch( uData ) {

		case addrByteAsByte:	Text += "B";	break;
		case addrWordAsWord:	Text += "W";	break;
		case addrLongAsLong:	Text += "L";	break;
		case addrRealAsReal:	Text += "R";	break;
		case 0:			Text += "S.0";	break;	
		}

	if( m_pDriver->ParseAddress(Error, Addr, m_pConfig, Text) ) {

		*m_pAddr = Addr;

		EndDialog(TRUE);

		return TRUE;
		}

	Error.Show(ThisObject);

	m_pAddr->m_Ref = 0;

	return TRUE;
	}

// Implementation

void CTotalFlow2Dialog::LoadTypeList(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(204);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	Box.AddString(CString(IDS_SPACE_BYTE),   DWORD(addrByteAsByte));

	Box.AddString(CString(IDS_SPACE_WORD),   DWORD(addrWordAsWord));

	Box.AddString(CString(IDS_SPACE_LONG),   DWORD(addrLongAsLong));

	Box.AddString(CString(IDS_SPACE_REAL),   DWORD(addrRealAsReal));

	Box.AddString(CString(IDS_STRING), DWORD(0));

	Box.SetRedraw (TRUE);

	Box.Invalidate(TRUE);
	}

void CTotalFlow2Dialog::LoadApplicationList(void)
{
	CTotalFlow2MasterDeviceOptions * pDevice = (CTotalFlow2MasterDeviceOptions *) m_pConfig;

	CTotalFlowAppList * pApps = pDevice->m_pApplications;

	CComboBox &Combo = (CComboBox &) GetDlgItem(205);

	Combo.SetRedraw(TRUE);

	Combo.ResetContent();

	Combo.EnableWindow(TRUE);

	Combo.AddString("Unnamed Application", NOTHING);

	UINT uCount = pApps->GetItemCount();

	if( uCount > 0 && pDevice->UsingNamedApps() ) {

		for( UINT n = 0; n < uCount; n++ ) {

			CTotalFlowApp * pApp = pApps->GetItem(n);

			Combo.AddString(pApp->m_Name, pApp->m_App);
			}

		if( !m_pAddr->m_Ref && Combo.GetCount() > 0 ) {

			Combo.SetCurSel(0);

			UINT uApp = Combo.GetCurSelData();

			if( uApp != NOTHING ) {

				GetDlgItem(201).SetWindowText(CPrintf("%u", uApp));
				}
			}

		if( m_pAddr->m_Ref ) {

			CTotalFlow2MasterDeviceOptions * pDevice = (CTotalFlow2MasterDeviceOptions *) m_pConfig;

			UINT uAddr = m_pDriver->GetAddress(*m_pAddr);

			CTotalFlowSlot Slot;
		
			if( pDevice->GetSlot(uAddr, Slot) ) {

				UINT n;

				for( n = 0; n < Combo.GetCount(); n++ ) {

					if( Slot.m_App == Combo.GetItemData(n) ) {

						Combo.SetCurSel(n);

						break;
						}
					}

				if( n == Combo.GetCount() ) {

					Combo.SetCurSel(0);
					}
				}
			}
		}

	else {
		Combo.SetCurSel(0);

		Combo.EnableWindow(FALSE);
		}

	Combo.SetRedraw(TRUE);

	Combo.Invalidate(TRUE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Application Configuration Dialog
//

AfxImplementRuntimeClass(CTotalFlow2AppDialog, CStdDialog);
		                
// Constructor

CTotalFlow2AppDialog::CTotalFlow2AppDialog(CTotalFlow2MasterDeviceOptions *pDevice)
{
	SetName(L"TotalFlow2AppDlg");

	m_pDevice = pDevice;

	m_pApps   = (CTotalFlowAppList *) m_pDevice->MakeFromItem(m_pDevice, m_pDevice->m_pApplications);
	}

// Destructor

CTotalFlow2AppDialog::~CTotalFlow2AppDialog(void)
{
	}
		
// Message Map

AfxMessageMap(CTotalFlow2AppDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_COMMAND)

	AfxDispatchCommand(IDOK, OnOkay)
	AfxDispatchCommand(1001, OnAddApp)
	AfxDispatchCommand(1002, OnRemoveApp)
	AfxDispatchCommand(1008, OnEditApp)

	AfxDispatchNotify(2001, LVN_ITEMCHANGED,  OnListItemChange)
	AfxDispatchNotify(1006, EN_KILLFOCUS,	  OnEditKillFocus)

	AfxMessageEnd(CTotalFlow2AppDialog)
	};

// Message Handlers

BOOL CTotalFlow2AppDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	InitListView();

	CSpinner &Spin = (CSpinner &) GetDlgItem(1007);

	Spin.SetRange(0, 255);

	DoEnables();
	
	return TRUE;
	}

BOOL CTotalFlow2AppDialog::OnCommand(UINT uID, UINT uNotify, CWnd &Ctrl)
{
	if( uID == IDCANCEL ) {

		if( m_pApps ) {

			delete m_pApps;
			}			
		}
	
	return CStdDialog::OnCommand(uID, uNotify, Ctrl);
	}

// Command Handlers

BOOL CTotalFlow2AppDialog::OnOkay(UINT uID)
{
	if( m_pDevice->m_pApplications ) {

		delete m_pDevice->m_pApplications;
		}

	m_pDevice->m_pApplications = (CTotalFlowAppList *) m_pDevice->MakeFromItem(m_pDevice, m_pApps);

	if( m_pApps) {

		delete m_pApps;
		}

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CTotalFlow2AppDialog::OnAddApp(UINT uID)
{
	CString Name   = GetDlgItem(1005).GetWindowText();

	CString Number = GetDlgItem(1006).GetWindowText();

	GetDlgItem(1005).SetFocus();

	if( Name.IsEmpty() ) {

		Error(L"The application must have a name.");

		return FALSE;
		}

	CError Err;

	if( !C3ValidateName(Err, Name) ) {

		Err.Show(ThisObject);

		return FALSE;
		}

	UINT uNum = wcstol(Number, NULL, 10);

	if( AddApplication(Name, uNum) ) {

		UpdateListView();

		GetDlgItem(1005).SetWindowText(L"");

		return TRUE;
		}

	GetDlgItem(1006).SetFocus();

	Error(L"Application number is already in use.");

	return FALSE;
	}

BOOL CTotalFlow2AppDialog::OnRemoveApp(UINT uID)
{
	CListView &ListView = (CListView &) GetDlgItem(2001);

	UINT uSel = ListView.GetSelection();

	if( uSel != NOTHING ) {

		CListViewItem NameItem = ListView.GetItem(uSel);

		CListViewItem NumItem  = ListView.GetItem(uSel, 1);

		CString Name(NameItem.GetText());

		UINT uNumber = wcstol(NumItem.GetText(), NULL, 10);

		if( RemoveApplication(Name, uNumber) ) {

			UpdateListView();

			DoEnables();

			if( ListView.GetItemCount() > 0 ) {

				UINT uNewSel = max(int(uSel) - 1, 0);

				ListView.SetItemState(uNewSel, LVIS_SELECTED, LVIS_SELECTED);

				ListView.SetFocus();
				}
	
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTotalFlow2AppDialog::OnEditApp(UINT uID)
{
	CListView &ListView = (CListView &) GetDlgItem(2001);

	UINT uSel = ListView.GetSelection();

	if( uSel != NOTHING ) {

		CListViewItem NameItem = ListView.GetItem(uSel);

		CListViewItem NumItem  = ListView.GetItem(uSel, 1);

		CTotalFlow2AppEditDialog Dlg(NameItem.GetText(), NumItem.GetText());

		if( Dlg.Execute() ) {

			CString Text = Dlg.GetData();

			UINT uNum = 0;

			if( ValidateAppNumber(Text, uNum) ) {

				UINT uOld = tstrtol(NumItem.GetText(), NULL, 10);

				if( EditApplication(NameItem.GetText(), uOld, uNum) ) {

					ListView.SetItemText(uSel, 1, CPrintf("%u", uNum));

					ListView.SetItemState(uSel, LVIS_SELECTED, LVIS_SELECTED);

					ListView.SetFocus();

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CTotalFlow2AppDialog::OnListItemChange(UINT uID, NMLISTVIEW &Info)
{
	DoEnables();
	}

void CTotalFlow2AppDialog::OnEditKillFocus(UINT uID, CWnd &Wnd)
{
	CString Text = Wnd.GetWindowText();

	UINT uApp = wcstol(Text, NULL, 10);

	if( uApp < 0 ) {

		Wnd.SetWindowText(L"0");
		}

	if( uApp > 255 ) {

		Wnd.SetWindowText(L"255");
		}
	}

// Implementation

BOOL CTotalFlow2AppDialog::AddApplication(CString Name, UINT uNumber)
{
	CTotalFlowApp * pApp = New CTotalFlowApp(Name, uNumber);

	INDEX i = m_pApps->FindItemIndex(pApp);

	if( m_pApps->Failed(i) ) {

		return m_pApps->AppendItem(pApp);
		}

	delete pApp;

	return FALSE;
	}

BOOL CTotalFlow2AppDialog::RemoveApplication(CString Name, UINT uNumber)
{
	for( INDEX i = m_pApps->GetHead(); !m_pApps->Failed(i); m_pApps->GetNext(i) ) {

		CTotalFlowApp * pApp = m_pApps->GetItem(i);

		if( pApp->m_Name == Name && pApp->m_App == uNumber ) {

			m_pApps->RemoveItem(pApp);

			delete pApp;
	
			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CTotalFlow2AppDialog::EditApplication(CString Name, UINT uOld, UINT uNew)
{
	for( INDEX i = m_pApps->GetHead(); !m_pApps->Failed(i); m_pApps->GetNext(i) ) {

		CTotalFlowApp * pApp = m_pApps->GetItem(i);

		if( pApp->m_Name == Name && pApp->m_App == uOld ) {

			CTotalFlowApp * pNew = New CTotalFlowApp(Name, uNew);

			pNew->m_Lookup = pApp->m_Lookup;

			m_pApps->InsertItem(pNew, pApp);

			m_pApps->RemoveItem(pApp);

			delete pApp;

			return TRUE;
			}
		}

	return FALSE;
	}

void CTotalFlow2AppDialog::InitListView(void)
{
	CListView &List = (CListView &) GetDlgItem(2001);

	CListViewColumn Col; 

	Col.SetWidth(200);

	Col.SetText(L"Application Name");

	List.InsertColumn(0, Col);

	Col.SetWidth(100);

	Col.SetText(L"Number");

	List.InsertColumn(1, Col);

	UpdateListView();
	}

void CTotalFlow2AppDialog::UpdateListView(void)
{
	CListView &ListView = (CListView &) GetDlgItem(2001);

	ListView.SetRedraw(FALSE);

	ListView.DeleteAllItems();

	UINT uCount = m_pApps->GetItemCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CTotalFlowApp * pApp = m_pApps->GetItem(n);

		CListViewItem Item;

		Item.SetItem(n);

		Item.SetText(pApp->m_Name);

		ListView.InsertItem(Item);

		Item.SetSubItem(1);

		Item.SetText(CPrintf("%u", pApp->m_App));

		ListView.SetItem(Item);
		}

	ListView.Invalidate(TRUE);

	ListView.SetRedraw(TRUE);
	}

void CTotalFlow2AppDialog::DoEnables(void)
{
	CListView &ListView = (CListView &) GetDlgItem(2001);

	BOOL fEnable = ListView.GetSelection() != NOTHING;

	GetDlgItem(1002).EnableWindow(fEnable);

	GetDlgItem(1008).EnableWindow(fEnable);
	}

BOOL CTotalFlow2AppDialog::ValidateAppNumber(CString Text, UINT &uNum)
{
	uNum = tstrtol(Text, NULL, 10);

	return uNum >= 0 && uNum <= 255;
	}

//////////////////////////////////////////////////////////////////////////
//
// Application Configuration Dialog
//

AfxImplementRuntimeClass(CTotalFlow2AppEditDialog, CStdDialog);
		                
// Constructor

CTotalFlow2AppEditDialog::CTotalFlow2AppEditDialog(CString App, CString Value)
{
	SetName(L"TotalFlow2AppEditDlg");

	m_Group = App;

	m_Data  = Value;
	}

// Message Map

AfxMessageMap(CTotalFlow2AppEditDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	
	AfxMessageEnd(CTotalFlow2AppEditDialog)
	};

// Data Access

CString CTotalFlow2AppEditDialog::GetData(void)
{
	return m_Data;
	}

// Message Handlers

BOOL CTotalFlow2AppEditDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	GetDlgItem(200).SetWindowText(m_Group);

	GetDlgItem(100).SetWindowText(m_Data);
	
	return TRUE;
	}

// Command Handlers

BOOL CTotalFlow2AppEditDialog::OnOkay(UINT uID)
{
	CString Text = GetDlgItem(100).GetWindowText();

	UINT uValue  = tstrtol(Text, NULL, 10);

	if( uValue <= 255 ) {

		m_Data = Text;

		EndDialog(TRUE);

		return TRUE;
		}

	Error(CString(IDS_APPLICATION));

	GetDlgItem(100).SetFocus();

	return TRUE;
	}


// End of File
