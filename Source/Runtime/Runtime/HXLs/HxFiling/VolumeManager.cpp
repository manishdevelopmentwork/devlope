
#include "Intern.hpp"

#include "VolumeManager.hpp"

#include "Fat16BootSector.hpp"

#include "Fat32BootSector.hpp"

#include "TaskLocalStorage.hpp"

#include "PartitionTable.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Filing System Instantiators
//

extern IFilingSystem * Create_Fat16FileSystem(UINT iDisk, UINT iVolume, UINT uBoot);

extern IFilingSystem * Create_Fat32FileSystem(UINT iDisk, UINT iVolume, UINT uBoot);

//////////////////////////////////////////////////////////////////////////
//
// Volume Manager Object
//

// Instantiator

IVolumeManager * Create_VolumeManager(void)
{
	CVolumeManager *p = New CVolumeManager();

	return p;
	}

// Constructor

CVolumeManager::CVolumeManager(void)
{
	StdSetRef();

	m_pMutex   = Create_Mutex();

	m_pManager = NULL;

	piob->RegisterSingleton("fs.volman", 0, this);

	InitSlots();
	}

// Destructor

CVolumeManager::~CVolumeManager(void)
{
	m_pMutex->Release();
	}

// IUnknown

HRESULT CVolumeManager::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IVolumeManager);

	StdQueryInterface(IVolumeManager);

	return E_NOINTERFACE;
	}

ULONG CVolumeManager::AddRef(void)
{
	StdAddRef();
	}

ULONG CVolumeManager::Release(void)
{
	StdRelease();
	}

// Methods

UINT CVolumeManager::FormatVolume(UINT iDisk, CHAR cDrive, UINT uBoot, UINT uSize)
{
	FindDiskManager();

	UINT iVolume = FindFreeSlot();

	if( iVolume != NOTHING ) {

		CVolume &Volume = m_Volumes[iVolume];

		Volume.m_iDisk  = iDisk;

		Volume.m_cDrive = cDrive;

		Volume.m_pCache = m_pManager->GetDiskCache(iDisk);

		Volume.m_pBlock = m_pManager->GetDisk(iDisk);

		Volume.m_dwBoot = uBoot;

		Volume.m_dwSize = uSize;

		Volume.m_uType  = typeFat32;

		if( IsFileSystemSupported(iVolume) ) {

			if( InitVolumeType(iVolume) ) {

				MakeFileSystem(iVolume);

				InitFileSystem(iVolume);

				StartFileSystem(iVolume);

				return iVolume;
				}
			}

		FreeSlot(iVolume);
		}

	return NOTHING;
	}

UINT CVolumeManager::MountVolume(UINT iDisk, CHAR cDrive, UINT uBoot)
{
	FindDiskManager();

	UINT iVolume = FindFreeSlot();

	if( iVolume != NOTHING ) {

		CVolume &Volume = m_Volumes[iVolume];

		Volume.m_iDisk  = iDisk;

		Volume.m_cDrive = cDrive;

		Volume.m_pCache = m_pManager->GetDiskCache(iDisk);

		Volume.m_pBlock = m_pManager->GetDisk(iDisk);

		Volume.m_dwBoot = uBoot;

		Volume.m_dwSize = NOTHING;

		if( FindVolumeType(iVolume) ) {

			if( IsFileSystemSupported(iVolume) ) {

				MakeFileSystem(iVolume);

				StartFileSystem(iVolume);

				return iVolume;
				}
			}

		FreeSlot(iVolume);
		}

	return NOTHING;
	}

bool CVolumeManager::UnMountVolume(UINT iVolume)
{
	if( IsVolumeMounted(iVolume) ) {
	
		StopFileSystem(iVolume);

		KillFileSystem(iVolume);

		FreeSlot(iVolume);

		return true;
		}

	return false;
	}

bool CVolumeManager::UnMountVolumes(void)
{
	for( UINT i = 0; i < elements(m_Volumes); i ++ ) {

		UnMountVolume(i);
		}
	
	return true;
	}

bool CVolumeManager::IsVolumeMounted(UINT iVolume)
{
	if( iVolume < elements(m_Volumes) ) {

		CVolume &Volume = m_Volumes[iVolume];

		return Volume.m_uType != typeFree && Volume.m_uType != typeUnknown;
		}
	
	return false;
	}

CHAR CVolumeManager::GetDriveLetter(UINT iVolume)
{
	if( iVolume < elements(m_Volumes) ) {

		CVolume &Volume = m_Volumes[iVolume];

		return Volume.m_cDrive;
		}
	
	return NULL;
	}

IFilingSystem * CVolumeManager::GetFileSystem(void)
{
	return GetTaskVolume();
	}

IFilingSystem * CVolumeManager::GetFileSystem(UINT iVolume)
{
	if( iVolume < elements(m_Volumes) ) {

		CVolume &Volume = m_Volumes[iVolume];

		m_pMutex->Wait(FOREVER);

		IFilingSystem *pFileSystem = Volume.m_pSystem;
		
		if( pFileSystem ) {
			
			pFileSystem->AddRef();
			}

		m_pMutex->Free();

		return pFileSystem;
		}
	
	return NULL;
	}

IFilingSystem * CVolumeManager::GetTaskVolume(void)
{
	IThread *pThread = GetCurrentThread();

	if( pThread ) {

		CTaskDrive *pThreadData = (CTaskDrive *) pThread->GetData(TLS_FILE_DRIVE);

		if( pThreadData ) {

			FindDiskManager();

			UINT iVolume = m_pManager->FindVolume(pThreadData->m_cDrive);

			if( iVolume != NOTHING ) {

				return GetFileSystem(iVolume);
				}
			}
		}

	return NULL;
	}

bool CVolumeManager::SetTaskVolume(IFilingSystem *pFilingSystem)
{
	IThread *pThread = GetCurrentThread();

	if( pThread ) {

		CTaskDrive *pThreadData = (CTaskDrive *) pThread->GetData(TLS_FILE_DRIVE);

		if( !pThreadData ) {

			pThreadData = New CTaskDrive;

			if( !pThread->SetData(pThreadData) ) {

				delete pThreadData;

				return false;
				}
			}
		
		pThreadData->m_cDrive = pFilingSystem->GetDrive();

		return true;
		}

	return false;
	}

// Implementation

void CVolumeManager::FindDiskManager(void)
{
	if( !m_pManager ) {

		AfxGetObject("diskman", 0, IDiskManager, m_pManager);
		}
	}

BOOL CVolumeManager::InitVolumeType(UINT iVolume)
{
	CVolume &Volume = m_Volumes[iVolume];

	PBYTE pData = Volume.m_pCache->LockSector(Volume.m_dwBoot, FALSE);

	if( pData ) {

		if( InitBootSector(iVolume, pData) ) {

			Volume.m_pCache->UnlockSector(Volume.m_dwBoot, true);

			return true;
			}
			
		Volume.m_pCache->UnlockSector(Volume.m_dwBoot, false);
		}

	return false;
	}

BOOL CVolumeManager::FindVolumeType(UINT iVolume)
{
	CVolume &Volume = m_Volumes[iVolume];

	PBYTE pData = Volume.m_pCache->LockSector(Volume.m_dwBoot, FALSE);

	if( pData ) {

		Volume.m_uType = ParseBootSector(pData); 
						
		Volume.m_pCache->UnlockSector(Volume.m_dwBoot, false);			
			
		return Volume.m_uType != typeUnknown;
		}

	return false;
	}

BOOL CVolumeManager::InitBootSector(UINT iVolume, PBYTE pBoot)
{
	CVolume &Volume = m_Volumes[iVolume];

	switch( Volume.m_uType ) {

		case typeFat16:
	
			return InitBootFat16(iVolume, pBoot);
	
		case typeFat32:

			return InitBootFat32(iVolume, pBoot);
		}

	return false;
	}

BOOL CVolumeManager::InitBootFat16(UINT iVolume, PBYTE pData)
{
	CPartitionEntry Entry;

	Entry.m_dwRelativeSector = m_Volumes[iVolume].m_dwBoot;

	Entry.m_dwTotalSectors   = m_Volumes[iVolume].m_dwSize;

	CFat16BootSector *pBoot  = (CFat16BootSector *) pData;

	pBoot->Init(Entry);

	pBoot->Dump();

	pBoot->HostToLocal();

	return true;
	}

BOOL CVolumeManager::InitBootFat32(UINT iVolume, PBYTE pData)
{
	CPartitionEntry Entry;

	Entry.m_dwRelativeSector = m_Volumes[iVolume].m_dwBoot;

	Entry.m_dwTotalSectors   = m_Volumes[iVolume].m_dwSize;

	CFat32BootSector *pBoot  = (CFat32BootSector *) pData;

	pBoot->Init(Entry);

	pBoot->Dump();

	pBoot->HostToLocal();

	return true;
	}

UINT CVolumeManager::ParseBootSector(PBYTE pBoot) const
{
	if( ParseBootFat16(pBoot) ) {

		return typeFat16;
		}

	if( ParseBootFat32(pBoot) ) {

		return typeFat32;
		}

	return typeUnknown;
	}

BOOL CVolumeManager::ParseBootFat16(PBYTE pData) const
{
	CFat16BootSector *pBoot = (CFat16BootSector *) pData;

	pBoot->LocalToHost();

	if( !pBoot->IsValid() ) {

		pBoot->HostToLocal();

		return false;
		}

	pBoot->HostToLocal();

	return true;
	}

BOOL CVolumeManager::ParseBootFat32(PBYTE pData) const
{
	CFat32BootSector *pBoot = (CFat32BootSector *) pData;

	pBoot->LocalToHost();

	if( !pBoot->IsValid() ) {

		pBoot->HostToLocal();

		return false;
		}

	pBoot->HostToLocal();

	return true;
	}

BOOL CVolumeManager::IsFileSystemSupported(UINT iVolume) const
{
	return true;
	}

// File System Creation

void CVolumeManager::MakeFileSystem(UINT iVolume)
{
	CVolume &Volume  = m_Volumes[iVolume];

	Volume.m_pSystem = NULL;

	switch( Volume.m_uType ) {

		case typeFat16:

			Volume.m_pSystem = Create_Fat16FileSystem(Volume.m_iDisk, iVolume, Volume.m_dwBoot);

			break;

		case typeFat32:

			Volume.m_pSystem = Create_Fat32FileSystem(Volume.m_iDisk, iVolume, Volume.m_dwBoot);

			break;
		}
	}

void CVolumeManager::KillFileSystem(UINT iVolume)
{
	CVolume &Volume = m_Volumes[iVolume];

	if( Volume.m_pSystem ) {

		m_pMutex->Wait(FOREVER);
	
		Volume.m_pSystem->Release();

		Volume.m_pSystem = NULL;
		
		m_pMutex->Free();
		}
	}

void CVolumeManager::InitFileSystem(UINT iVolume)
{
	CVolume &Volume = m_Volumes[iVolume];

	if( Volume.m_pSystem ) {

		Volume.m_pSystem->Init();
		}
	}

void CVolumeManager::StartFileSystem(UINT iVolume)
{
	CVolume &Volume = m_Volumes[iVolume];

	if( Volume.m_pSystem ) {

		Volume.m_pSystem->Start();
		}
	}

void CVolumeManager::StopFileSystem(UINT iVolume)
{
	CVolume &Volume = m_Volumes[iVolume];

	if( Volume.m_pSystem ) {

		Volume.m_pSystem->Stop();

		Volume.m_pCache->Flush();

		Volume.m_pCache->Invalidate();
		}
	}

// Slot Handlers

void CVolumeManager::InitSlots(void)
{
	memset(m_Volumes, 0x00, sizeof(m_Volumes));
	}

UINT CVolumeManager::FindFreeSlot(void)
{
	m_pMutex->Wait(FOREVER);
	
	for( UINT i = 0; i < elements(m_Volumes); i ++ ) {

		CVolume &Volume = m_Volumes[i];

		if( Volume.m_uType == typeFree ) {

			Volume.m_uType = typeUnknown;

			m_pMutex->Free();

			return i;
			}
		}
	
	m_pMutex->Free();

	return NOTHING;
	}

void CVolumeManager::FreeSlot(UINT iSlot)
{
	if( iSlot < elements(m_Volumes) ) {

		CVolume &Volume = m_Volumes[iSlot];

		Volume.m_uType = typeFree;
		}
	}

// End of File