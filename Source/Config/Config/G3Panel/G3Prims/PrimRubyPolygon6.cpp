
#include "intern.hpp"

#include "PrimRubyPolygon6.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby 6-Sided Polygon Primitives
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyPolygon6, CPrimRubyPolygon)

// Constructor

CPrimRubyPolygon6::CPrimRubyPolygon6(void)
{
	m_nSides = 6;

	m_Rotate = -900;
	}

// Meta Data

void CPrimRubyPolygon6::AddMetaData(void)
{
	CPrimRubyPolygon::AddMetaData();

	Meta_SetName((IDS_HEXAGON_2));
	}

// End of File
