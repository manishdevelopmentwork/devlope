
//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Dpll51_HPP
	
#define	INCLUDE_Dpll51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Digital Phase Lock Loop Interface
//

class CDpll51
{
	public:
		// Constructor
		CDpll51(UINT iIndex);

		// Operations
		void  Set(UINT uRef, UINT uMfi, UINT uMfn, UINT uMfd, UINT uPdf, BOOL fDouble);
		void  Set(UINT uMfn);
		DWORD Get(void) const;

		// Registers
		enum
		{
			regCtrl		= 0x0000 / sizeof(DWORD),
			regConfig	= 0x0004 / sizeof(DWORD),
			regOperation	= 0x0008 / sizeof(DWORD),
			regMFD		= 0x000C / sizeof(DWORD),
			regMFN		= 0x0010 / sizeof(DWORD),
			regMFNSub	= 0x0014 / sizeof(DWORD),
			regMFNAdd	= 0x0018 / sizeof(DWORD),
			regHFSOperation	= 0x001C / sizeof(DWORD),
			regHFSMFD	= 0x0020 / sizeof(DWORD),
			regHFSMFN	= 0x0024 / sizeof(DWORD),
			regMFNToggle	= 0x0028 / sizeof(DWORD),
			regDesense	= 0x002C / sizeof(DWORD),
			};

		// Ref Clock
		enum 
		{
			refOsc	= 2,
			refFpm	= 3,
			};

	protected:
		// Data Members
		PVDWORD m_pBase;
		UINT    m_uFreq[2];
		
		// Helpers
		DWORD FindBase(UINT i) const;
		void  WaitLock(void) const;
		UINT  GetRefFreq(UINT uSel) const; 
	};

//////////////////////////////////////////////////////////////////////////
//
// Inline Code
//

// Operations

INLINE void CDpll51::WaitLock(void) const
{
	while( !(m_pBase[regCtrl] & 0x00000001) ); 
	}

// End of File

#endif
