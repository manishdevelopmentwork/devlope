
#include "Intern.hpp"  

#include "UsbHostScsiDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDescList.hpp"

#include "UsbClassReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Classes
//

#include "ScsiBulkCmd.hpp"

#include "ScsiBulkStatus.hpp"

#include "ScsiInquiry.hpp"

#include "ScsiSense.hpp"

#include "ScsiCapacity.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Command Classes
//

#include "ScsiCmd6.hpp"

#include "ScsiCmdInquiry.hpp"

#include "ScsiCmdCapacity.hpp"

#include "ScsiCmd6.hpp"

#include "ScsiCmd10.hpp"

#include "ScsiCmdStartStop.hpp"

#include "ScsiCmdLock.hpp"

#include "ScsiCmdDiag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Scsi Driver
//

// Instantiator

IUsbHostMassStorage * Create_StorageDriver(void)
{
	IUsbHostMassStorage *p = New CUsbHostScsiDriver;

	return p;
	}

// Constructor

CUsbHostScsiDriver::CUsbHostScsiDriver(void)
{
	m_pName  = "Mass Storage Driver (Master)";

	m_Debug  = debugErr;
	
	m_pCtrl  = NULL;

	m_pSend  = NULL;

	m_pRecv  = NULL;

	m_dwTag  = 0;

	m_pCbw   = New CScsiBulkCmd();

	m_pCsw   = New CScsiBulkStatus;

	m_pBuff  = New BYTE[ 512 ];

	m_pData  = NULL;

	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostScsiDriver::~CUsbHostScsiDriver(void)
{
	Trace(debugInfo, "Driver Destroyed");

	delete m_pCbw;

	delete m_pCsw;

	delete [] m_pBuff;
	}

// IUnknown

HRESULT CUsbHostScsiDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostMassStorage);

	return CUsbHostFuncDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostScsiDriver::AddRef(void)
{
	return CUsbHostFuncDriver::AddRef();
	}

ULONG CUsbHostScsiDriver::Release(void)
{
	return CUsbHostFuncDriver::Release();
	}

// IHostFuncDriver

void CUsbHostScsiDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncDriver::Bind(pDevice, iInterface);
	}

void CUsbHostScsiDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncDriver::Bind(pSink);
	}

void CUsbHostScsiDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncDriver::GetDevice(pDev);
	}

BOOL CUsbHostScsiDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostScsiDriver::GetVendor(void)
{
	return CUsbHostFuncDriver::GetVendor();
	}

UINT CUsbHostScsiDriver::GetProduct(void)
{
	return CUsbHostFuncDriver::GetProduct();
	}

UINT CUsbHostScsiDriver::GetClass(void)
{
	return devStorage;
	}

UINT CUsbHostScsiDriver::GetSubClass(void)
{
	return 0x06;
	}

UINT CUsbHostScsiDriver::GetInterface(void)
{
	return CUsbHostFuncDriver::GetInterface();
	}

UINT CUsbHostScsiDriver::GetProtocol(void)
{
	return 0x50;
	}

BOOL CUsbHostScsiDriver::GetActive(void)
{
	return CUsbHostFuncDriver::GetActive();
	}

BOOL CUsbHostScsiDriver::Open(CUsbDescList const &List)
{
	Trace(debugInfo, "Open");

	if( FindPipes(List) ) {	

		m_dwTag = 0;
		
		return CUsbHostFuncDriver::Open();
		}

	return false;
	}

BOOL CUsbHostScsiDriver::Close(void)
{
	Trace(debugInfo, "Close");

	return CUsbHostFuncDriver::Close();
	}

void CUsbHostScsiDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncDriver::Poll(uLapsed);	
	}

// IMassStorageDriver

BOOL CUsbHostScsiDriver::TestReady(void)
{
	Trace(debugCmds, "TestReady");

	CScsiCmd6 *pCmd = (CScsiCmd6 *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiCmd6));

	pCmd->Init(cmdTestRdy);

	pCmd->HostToScsi();

	return Transact() && m_pCsw->IsPassed();
	}

BOOL CUsbHostScsiDriver::ReadInquiry(void)
{
	Trace(debugCmds, "ReadInquiry");

	CScsiCmdInquiry *pCmd = (CScsiCmdInquiry *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiInquiry), sizeof(ScsiInquiry), true);

	pCmd->Init(sizeof(ScsiInquiry));

	m_pData = m_pBuff;

	if( Transact() ) {
		
		if( m_pCsw->IsPassed() ) {

			#if defined(_DEBUG)
				
			if( m_Debug & debugData ) {

				CScsiInquiry *pInquiry = (CScsiInquiry *) m_pData;

				pInquiry->ScsiToHost();

				pInquiry->Dump();
				}

			#endif

			return true;
			}
		}

	return false;
	}

BOOL CUsbHostScsiDriver::RequestSense(BYTE &bSk, BYTE &bAsc, BYTE &bAscq)
{
	Trace(debugCmds, "RequestSense");

	CScsiCmd6 *pCmd = (CScsiCmd6 *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiCmd6), sizeof(ScsiSense), true);

	pCmd->Init(cmdReqSense, sizeof(ScsiSense));

	pCmd->HostToScsi();

	m_pData = m_pBuff;	

	if( Transact() ) {

		if( m_pCsw->IsPassed() ) {

			CScsiSense *pSense = (CScsiSense *) m_pData;

			pSense->ScsiToHost();
			
			if( pSense->IsValid() ) {

				Trace(debugData, "Sk   0x%2.2X", pSense->m_bSenseKey);

				Trace(debugData, "Asc  0x%2.2X", pSense->m_bAsc);

				Trace(debugData, "Ascq 0x%2.2X", pSense->m_bAscq);
								
				bSk   = pSense->m_bSenseKey;

				bAsc  = pSense->m_bAsc;

				bAscq = pSense->m_bAscq;
				
				return true;
				}
			}
		}

	return false;
	}

BOOL CUsbHostScsiDriver::ReadCapacity(DWORD &dwBlocks, DWORD &dwLength)
{
	Trace(debugCmds, "ReadCapacity");

	m_pCbw->Init(sizeof(ScsiCmdCapacity), sizeof(ScsiCapacity), true);

	CScsiCmdCapacity *pCmd = (CScsiCmdCapacity *) m_pCbw->m_bCmd;

	pCmd->Init();

	pCmd->HostToScsi();

	m_pData = m_pBuff;	
	
	if( Transact() ) {

		if( m_pCsw->IsPassed() ) {

			CScsiCapacity *pCapacity = (CScsiCapacity *) m_pData;

			pCapacity->ScsiToHost();
			
			dwBlocks = pCapacity->m_dwBlockAddr;

			dwLength = pCapacity->m_dwBlockLen;

			Trace(debugData, "Blocks 0x%8.8X, Length %8.8X", dwBlocks, dwLength);
			
			return true;
			}
		}

	return false;
	}

BOOL CUsbHostScsiDriver::Read(DWORD dwBlock, PBYTE pData)
{
	Trace(debugCmds, "Read(0x%8.8X)", dwBlock);

	CScsiCmd10 *pCmd = (CScsiCmd10 *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiCmd10), 512, true);

	pCmd->Init(cmdRead10, 0, 1, dwBlock);
	
	pCmd->HostToScsi();

	m_pData = pData;
	
	return Transact() && m_pCsw->IsPassed();
	}

BOOL CUsbHostScsiDriver::Write(DWORD dwBlock, PBYTE pData)
{
	Trace(debugCmds, "Write(0x%8.8X)", dwBlock);

	CScsiCmd10 *pCmd = (CScsiCmd10 *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiCmd10), 512, false);

	pCmd->Init(cmdWrite10, 0, 1, dwBlock);
	
	pCmd->HostToScsi();

	m_pData = pData;
	
	return Transact() && m_pCsw->IsPassed();
	}

BOOL CUsbHostScsiDriver::Verify(DWORD dwBlock, UINT uCount)
{
	Trace(debugCmds, "Verify(%d, %d)", dwBlock, uCount);
	
	CScsiCmd10 *pCmd = (CScsiCmd10 *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiCmd10));

	pCmd->Init(cmdVerify10, 0, uCount, dwBlock);

	pCmd->HostToScsi();

	return Transact() && m_pCsw->IsPassed();
	}

BOOL CUsbHostScsiDriver::StartStop(bool fStart)
{
	Trace(debugCmds, "StartStop(%d)", fStart);
	
	CScsiCmdStartStop *pCmd = (CScsiCmdStartStop *) m_pCbw->m_bCmd;
	
	m_pCbw->Init(sizeof(CScsiCmdStartStop));

	pCmd->Init(fStart);

	pCmd->HostToScsi();

	return Transact() && m_pCsw->IsPassed();
	}

BOOL CUsbHostScsiDriver::LockMedia(bool fLock)
{
	Trace(debugCmds, "LockMedia(%d)", fLock);
	
	CScsiCmdLock *pCmd = (CScsiCmdLock *) m_pCbw->m_bCmd;
	
	m_pCbw->Init(sizeof(ScsiCmdLock));

	pCmd->Init(fLock);

	pCmd->HostToScsi();

	return Transact() && m_pCsw->IsPassed();
	}

BOOL CUsbHostScsiDriver::SendDiag(void)
{
	Trace(debugCmds, "SendDiag");
	
	CScsiCmdDiag *pCmd = (CScsiCmdDiag *) m_pCbw->m_bCmd;

	m_pCbw->Init(sizeof(ScsiCmdDiag));

	pCmd->Init();

	pCmd->HostToScsi();

	return Transact() && m_pCsw->IsPassed();
	}

// Helpers

void CUsbHostScsiDriver::ResetRecovery(void)
{
	Trace(debugWarn, "ResetRecovery");

	CUsbClassReq Req;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;

	Req.m_bRequest  = 255;

	Req.m_wIndex    = m_iInt;

	Req.HostToUsb();

	m_pCtrl->CtrlTrans(Req);

	m_pSend->ClearStall(); 

	m_pRecv->ClearStall(); 
	}

bool CUsbHostScsiDriver::Transact(void)
{
	if( SendCmd() ) {

		if( m_pCbw->HasData() ) {

			UINT uCount = m_pCbw->m_dwTransfer;

			if( m_pCbw->IsDataIn() ) {

				RecvBulk(m_pData, uCount, 10000);
				}
			else {
				SendBulk(m_pData, uCount);
				}
			}

		return RecvStatus();
		}

	return false;
	}

bool CUsbHostScsiDriver::SendCmd(void) 
{
	Trace(debugData, "SendCBW");

	m_pCbw->m_dwTag = ++m_dwTag;

	m_pCbw->HostToScsi();
	
	BOOL fOk = SendBulk(PBYTE(m_pCbw), sizeof(ScsiBulkCmd));

	m_pCbw->ScsiToHost();

	return fOk;
	}

bool CUsbHostScsiDriver::RecvStatus(void)
{
	Trace(debugData, "RecvCSW");

	for( UINT uRetry = 0; uRetry < 2; uRetry ++ ) {

		PBYTE pData  = PBYTE(m_pCsw);

		UINT  uCount = sizeof(ScsiBulkStatus);

		if( !RecvBulk(pData, uCount, 5000) ) {

			Trace(debugWarn, "RecvCSW - Recv Failed");
		
			if( CheckRecvStall() ) {

				continue;
				}

			break;
			}

		if( uCount == sizeof(ScsiBulkStatus) ) {

			m_pCsw->ScsiToHost();

			if( m_pCsw->IsValid() && m_pCsw->m_dwTag == m_dwTag ) { 

				if( m_pCsw->m_bStatus == bulkPhaseError ) {

					Trace(debugWarn, "Phase Error");
					
					ResetRecovery();
					}

				return true;
				}

			m_pCsw->Dump();

			Trace(debugWarn, "Invalid CSW");
			
			break;
			}

		Trace(debugWarn, "Corrupt CSW Count %d", uCount);
		
		break;
		}

	ResetRecovery();

	return false;
	}

bool CUsbHostScsiDriver::SendBulk(PBYTE pData, UINT uCount)
{
	Trace(debugData, "SendBulk(%d)", uCount);

	if( m_pSend ) {

		if( !m_pSend->SendBulk(pData, uCount, false) ) {

			CheckSendStall();

			return false;
			}
			
		return true;
		}

	return false;
	}

bool CUsbHostScsiDriver::RecvBulk(PBYTE pData, UINT &uCount, UINT uTimeout)
{
	Trace(debugData, "RecvBulk(%d)", uCount);

	if( m_pRecv ) {

		uCount = m_pRecv->RecvBulk(pData, uCount, false);

		if( uCount == NOTHING ) {

			CheckRecvStall();

			return false;
			}

		return true;
		}

	return false;
	}

bool CUsbHostScsiDriver::CheckSendStall(void)
{
	return CheckStall(m_pSend);
	}

bool CUsbHostScsiDriver::CheckRecvStall(void)
{
	return CheckStall(m_pRecv);
	}

bool CUsbHostScsiDriver::CheckStall(IUsbPipe *pPipe)
{
	if( pPipe->IsHalted() ) {

		Trace(debugWarn, "Clearing Stall");
	
		pPipe->ClearStall();

		return true;
		}
	
	return false;
	}

bool CUsbHostScsiDriver::FindPipes(CUsbDescList const &List)
{
	UINT iIndex = List.GetIndexStart();
	
	UsbInterfaceDesc *pInt = (UsbInterfaceDesc *) List.EnumInterface(iIndex);

	if( pInt && pInt->m_bEndpoints >= 2 ) {

		UsbEndpointDesc *pEp0 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		UsbEndpointDesc *pEp1 = (UsbEndpointDesc *) List.EnumEndpoint(iIndex);

		if( pEp0 && pEp1 ) {
		
			m_pCtrl = m_pDev->GetCtrlPipe();

			if( pEp0->m_bDirIn ) {

				m_pRecv = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

				m_pSend = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

				m_iRecv = pEp0->m_bAddr | Bit(7);

				m_iSend = pEp1->m_bAddr;
				}
			else {
				m_pSend = m_pDev->GetPipe(pEp0->m_bAddr, pEp0->m_bDirIn);

				m_pRecv = m_pDev->GetPipe(pEp1->m_bAddr, pEp1->m_bDirIn);

				m_iSend = pEp0->m_bAddr;

				m_iRecv = pEp1->m_bAddr | Bit(7);
				}

			return m_pCtrl != NULL && m_pSend != NULL && m_pRecv != NULL;
			}
		}
	
	return false;
	}

// End of File
