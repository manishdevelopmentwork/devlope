
#include "intern.hpp"

#include "events.hpp"

#include "service.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Alarm Manager
//

// Alarm Actions

enum
{
	actionFire   = 1,
	actionClear  = 2,
	actionAccept = 3,
	};

// Constructor

CAlarmManager::CAlarmManager(void)
{
	m_pEvents = CCommsSystem::m_pThis->m_pEvents;
	}

// Destructor

CAlarmManager::~CAlarmManager(void)
{
	}

// Attributes

BOOL CAlarmManager::IsAlarmActive(UINT uIndex)
{
	if( m_pEvents->Lock() ) {

		CActiveAlarm *pInfo = m_pEvents->FindAlarm(0, uIndex);

		if( pInfo ) {

			if( pInfo->m_State != alarmIdle ) {

				m_pEvents->Free();

				return TRUE;
				}
			}

		m_pEvents->Free();
		}

	return FALSE;
	}

// Operations

BOOL CAlarmManager::UpdateAlarm(CTagEvent *pEvent, UINT uIndex, CUnicode const &Text, BOOL fState)
{
	if( m_pEvents->Lock() ) {

		CActiveAlarm *pInfo = m_pEvents->FindAlarm(0, uIndex);

		if( !pInfo ) {

			if( !fState ) {

				m_pEvents->Free();

				return FALSE;
				}

			pInfo          = m_pEvents->MakeAlarm(0, uIndex);

			pInfo->m_Text  = Text;

			pInfo->m_Level = pEvent->m_Priority;

			pInfo->m_pData = pEvent;
			}
		else {
			if( fState ) {

				if( pInfo->m_Text.CompareC(Text) ) {

					pInfo->m_Text = Text;

					m_pEvents->EditAlarm(pInfo);
					}
				}
			}

		if( fState ) {

			Action(pInfo, actionFire);
			}
		else
			Action(pInfo, actionClear);

		m_pEvents->Free();

		return TRUE;
		}

	return FALSE;
	}

BOOL CAlarmManager::AcceptAlarm(UINT uIndex)
{
	if( m_pEvents->Lock() ) {

		CActiveAlarm *pInfo = m_pEvents->FindAlarm(0, uIndex);

		if( pInfo ) {

			Action(pInfo, actionAccept);

			m_pEvents->Free();

			return TRUE;
			}

		m_pEvents->Free();
		}

	return FALSE;
	}

// Object Access

CTagEvent * CAlarmManager::GetObject(CActiveAlarm *pInfo)
{
	return (CTagEvent *) pInfo->m_pData;
	}

// Implementation

BOOL CAlarmManager::Action(CActiveAlarm *pInfo, UINT uAction)
{
	switch( pInfo->m_State ) {

		case alarmIdle:

			switch( uAction ) {

				case actionFire:

					OnFire(pInfo);

					if( GetObject(pInfo)->m_Trigger ) {

						m_pEvents->EditAlarm(pInfo, alarmWaitAccept);
						}
					else {
						if( GetObject(pInfo)->m_Accept ) {

							m_pEvents->EditAlarm(pInfo, alarmAutoAccept);
							}
						else
							m_pEvents->EditAlarm(pInfo, alarmActive);
						}
					break;
				}
			break;

		case alarmActive:

			switch( uAction ) {

				case actionClear:

					OnClear(pInfo);

					if( GetObject(pInfo)->m_Accept ) {

						m_pEvents->EditAlarm(pInfo, alarmIdle);
						}
					else
						m_pEvents->EditAlarm(pInfo, alarmWaitAccept);

					break;

				case actionAccept:

					OnAccept(pInfo);

					m_pEvents->EditAlarm(pInfo, alarmAccepted);

					break;
				}
			break;

		case alarmAutoAccept:

			switch( uAction ) {

				case actionClear:

					OnClear(pInfo);

					m_pEvents->EditAlarm(pInfo, alarmIdle);

					break;
				}
			break;

		case alarmWaitAccept:

			switch( uAction ) {

				case actionFire:

					OnRefire(pInfo);

					if( !GetObject(pInfo)->m_Trigger ) {

						m_pEvents->EditAlarm(pInfo, alarmActive);
						}

					break;

				case actionAccept:

					OnAccept(pInfo);

					m_pEvents->EditAlarm(pInfo, alarmIdle);

					break;
				}
			break;

		case alarmAccepted:

			switch( uAction ) {

				case actionClear:

					OnClear(pInfo);

					m_pEvents->EditAlarm(pInfo, alarmIdle);

					break;
				}
			break;
		}

	return TRUE;
	}

void CAlarmManager::OnFire(CActiveAlarm *pInfo)
{
	RunAction(GetObject(pInfo)->m_pOnActive);

	OnFire(pInfo, 0x0F);
	}

void CAlarmManager::OnRefire(CActiveAlarm *pInfo)
{
	// REV3 -- Configure refire mask? This would also
	// impact whether we reload the text in UpdateAlarm
	// earlier in this file. If there are no actions
	// defined for a refire, there is no need to update.

	OnFire(pInfo, 0x07);
	}

void CAlarmManager::OnFire(CActiveAlarm *pInfo, UINT uMask)
{
	if( uMask & 0x01 ) {

		pInfo->m_Time = /*g_pServiceTimeSync ? g_pServiceTimeSync->GetLogTime() :*/ GetNowTimes5();

		m_pEvents->MoveAlarm(pInfo);
		}

	if( uMask & 0x02 ) {

		if( GetObject(pInfo)->m_Siren ) {

			g_pPxe->SetSiren(TRUE);
			}
		}

	if( uMask & 0x04 ) {

		LogEvent(eventAlarm, pInfo);
		}

	if( g_pServiceMail ) {

		if( uMask & 0x08 ) {

			if( GetObject(pInfo)->m_Mail < 0xFFFF ) {

				DWORD    Time = pInfo->m_Time / 5;

				CUnicode Text = pInfo->m_Text;

				if( Text.IsEmpty() ) {

					CCommsSystem::m_pThis->m_pTags->GetEventText( Text,
										      pInfo->m_Code
										      );
					}

				g_pServiceMail->SendMail( GetObject(pInfo)->m_Mail,
							  "Alarm",
							  Time,
							  Text
							  );
				}
			}
		}
	}

void CAlarmManager::OnAccept(CActiveAlarm *pInfo)
{
	RunAction(GetObject(pInfo)->m_pOnAccept);

	LogEvent(eventAccept, pInfo);
	}

void CAlarmManager::OnClear(CActiveAlarm *pInfo)
{
	RunAction(GetObject(pInfo)->m_pOnClear);

	LogEvent(eventClear, pInfo);
	}

void CAlarmManager::LogEvent(UINT uType, CActiveAlarm *pInfo)
{
	if( pInfo->m_Text.GetLength()  ){
	
		CCommsSystem::m_pThis->m_pEvents->LogEvent( 0,
							    pInfo->m_Source,
							    uType,
							    pInfo->m_Code,
							    pInfo->m_Text
							    );
		}
	else {
		CCommsSystem::m_pThis->m_pEvents->LogEvent( 0,
							    pInfo->m_Source,
							    uType,
							    pInfo->m_Code
							    );
		}
	}

void CAlarmManager::RunAction(CCodedItem *pAction)
{
	if( pAction && pAction->IsAvail() ) {

		pAction->Execute(typeVoid);
		}
	}

// End of File
