
#include "totalflow3base.hpp"

#include "../TotalFlow2TCP/totalflow2base.cpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced Driver Base
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// Constructor

CTotalFlow3Master::CTotalFlow3Master(void)
{
	}

// Entry Points

CCODE MCALL CTotalFlow3Master::Ping(void)
{
	if( m_pCtx->m_uSlots ) {

		CSuper Super;

		CHeader Header;

		AddInitialSequence();

		AddHeader(0, 0, 1);

		AddReadRecord(m_pCtx->m_pSlots[0]->m_App,
			      m_pCtx->m_pSlots[0]->m_Arr,
			      m_pCtx->m_pSlots[0]->m_Reg, 1);

		if( Transact(m_pBuff, m_uPtr, Super, Header) ) {

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR;
		}

	return CCODE_SUCCESS;
	}

// Slot Overridables

void CTotalFlow3Master::GetSlots(BYTE const * &pData, CBaseCtx *pCtx)
{
	UINT uCount     = GetLong(pData);

	pCtx->m_uSlots  = uCount;

	CSlot3 **pSlots = new CSlot3 * [ uCount ];

	for( UINT n = 0; n < uCount; n++ ) {

		pSlots[n]	  = new CSlot3;

		CSlot3 * pSlot    = pSlots[n];

		pSlot->m_Slot	  = GetLong(pData);
		pSlot->m_App	  = GetLong(pData);
		pSlot->m_Arr	  = GetLong(pData);
		pSlot->m_Reg	  = GetLong(pData);
		pSlot->m_Type	  = GetWord(pData);

		UINT uSize	  = GetWord(pData);
		pSlot->m_fString  = GetByte(pData);
		pSlot->m_Size	  = uSize;
		pSlot->m_fValid   = FALSE;
		pSlot->m_uRef	  = 0;
		pSlot->m_uLast	  = 0;
		pSlot->m_pReq	  = new CTotalFlowRequestList(m_pHelper);
		
		pSlot->m_pData    = new DWORD[ uSize ]; 

		memset(pSlot->m_pData, 0, uSize * sizeof(DWORD));
		}

	pCtx->m_pSlots = (CSlot **)pSlots;

	qsort(pCtx->m_pSlots, uCount, sizeof(CSlot*), SortFunc);
	}

void CTotalFlow3Master::CleanupSlots(void)
{
	for( UINT n = 0; n < m_pCtx->m_uSlots; n++ ) {

		CSlot3 * pSlot = (CSlot3 *) m_pCtx->m_pSlots[n];

		if( pSlot ) {

			delete pSlot->m_pReq;

			pSlot->m_pReq = NULL;
			}
		}

	return CTotalFlow2Master::CleanupSlots();
	}

CSlot * CTotalFlow3Master::FindSlot(AREF Addr, UINT &uCount)
{
	CSlot3 * pSlot = (CSlot3 *)FindSlot(Addr);

	if( pSlot ) {

		UINT uOffset = Addr.m_Ref - pSlot->m_Slot;

		if( pSlot->m_pReq ) {

			MakeMin(uCount, pSlot->m_Slot + pSlot->m_Size - Addr.m_Ref);

			UINT uStringSize = pSlot->m_fString ? m_pBase3->m_bStringSize : 0;

			pSlot->m_pReq->AddRequest(uOffset, uCount, uStringSize);

			return pSlot;
			}
		}

	return NULL;
	}

CSlot * CTotalFlow3Master::FindSlot(AREF Addr)
{
	UINT uSlots = m_pCtx->m_uSlots;

	for( UINT u = 0; u < uSlots; u++ ) {

		UINT uSlot = m_pCtx->m_pSlots[u]->m_Slot;

		UINT uSize = m_pCtx->m_pSlots[u]->m_Size;

		BOOL fStr  = m_pCtx->m_pSlots[u]->m_fString;

		if( Addr.m_Ref == uSlot ) {

			return m_pCtx->m_pSlots[u];
			}

		if( fStr ) {

			if( Addr.m_Ref > uSlot && Addr.m_Ref < uSlot + uSize ) {

				return  m_pCtx->m_pSlots[u];
				}
			}
		else {
			if( uSize > 1 ) {

				if( Addr.m_Ref > uSlot && Addr.m_Ref < uSlot + uSize ) {

					return m_pCtx->m_pSlots[u];
					}
				}
			}
		}

	return NULL;
	}

// Implementation

void CTotalFlow3Master::GetSlotArrayData(CBlock Block, CSlot ** pSlots, UINT uReply, UINT &uSlot, PCBYTE pData)
{
	UINT uSlots = GetRecordCount(pData);

	pData  += 8;

	uReply -= 6;

	for( UINT u = 0; u < uSlots; u++ ) {

		if( pSlots[uSlot]->m_Reg + u == Block.m_Reg ) {

			UINT uSize = GetDataCount(Block, uReply);

			UINT uSet  = GetDataOffset(Block, u);

			if( Block.m_fString ) {

				GetStringData(*pSlots[uSlot], pData, uReply, uSet);

				uSlot++;
				}

			else if( uReply >= GetDataSize(Block) ) {

				switch( pSlots[uSlot]->m_Type ) {

					case addrByteAsByte: GetByteData(*pSlots[uSlot], pData, uSize, uSet);	break;

					case addrWordAsWord: GetWordData(*pSlots[uSlot], pData, uSize, uSet);	break;

					case addrLongAsLong: GetLongData(*pSlots[uSlot], pData, uSize, uSet);	break;
						
					case addrRealAsReal: GetRealData(*pSlots[uSlot], pData, uSize, uSet);	break;
					}
				
				uSlot++;
				}

			break;
			}
		}
	}

void CTotalFlow3Master::GetStringData(CSlot &Slot, PCBYTE pFrom, UINT uSize, UINT uOffset)
{
	UINT uCount = Slot.m_Size / m_pBase3->m_bStringSize;

	UINT uChars = m_pBase3->m_bStringSize * sizeof(DWORD);

	for( UINT u = 0; u < uCount; u++ ) {

		UINT uRead = uChars;

		CheckDataSize(Slot, uRead, pFrom);

		if( uRead == 0 ) {

			return;
			}

		if( uSize >= uRead ) {

			GetLongData(Slot, pFrom, uRead / sizeof(DWORD), uOffset);
								
			uOffset += m_pBase3->m_bStringSize;

			pFrom   += uRead;

			if( uRead == uChars ) {

				pFrom++;
				}

			uSize -= uRead;
			}
		}
	}

void CTotalFlow3Master::ConstructArray(CBlock * pBlocks, UINT &uIndex, CSlot3 * pSlot)
{
	for( UINT r = 0; r < pSlot->m_pReq->GetCount(); r++ ) {

		CTotalFlowRequest * pReq = pSlot->m_pReq->GetRequest(r);

		if( pReq ) {

			pBlocks[uIndex].m_App	  = pSlot->m_App;

			pBlocks[uIndex].m_Arr     = pSlot->m_Arr;

			pBlocks[uIndex].m_Reg     = pSlot->m_Reg + pReq->m_Offset;

			pBlocks[uIndex].m_Type    = pSlot->m_Type;

			pBlocks[uIndex].m_Size    = pReq->m_Count; 

			pBlocks[uIndex].m_fString = pSlot->m_fString;

			pBlocks[uIndex].m_Count   = pReq->m_Count;

			uIndex++;
			}
		}

	}


void CTotalFlow3Master::ConstructBlock(CBlock * pBlocks,UINT &uIndex, CSlot ** pSlots, UINT &uSlot, UINT uCount)
{
	CSlot3 * pSlot = (CSlot3 *) pSlots[uSlot];

	CSlot3 * pNext = (CSlot3 *) pSlots[uSlot + 1];

	if( pSlot ) {

		pBlocks[uIndex].m_App	  = pSlot->m_App;

		pBlocks[uIndex].m_Arr     = pSlot->m_Arr;

		pBlocks[uIndex].m_Reg     = pSlot->m_Reg;

		pBlocks[uIndex].m_Type    = pSlot->m_Type;

		pBlocks[uIndex].m_Size    = pSlot->m_Size;

		pBlocks[uIndex].m_Count   = 1;

		pBlocks[uIndex].m_fString = pSlot->m_fString;

		while( !IsArray(pNext) && uSlot + 1 < uCount ) {

			if( IsBlock(*pSlot, *pNext) ) {

				pBlocks[uIndex].m_Count += pNext->m_Reg - pSlot->m_Reg;

				uSlot++;

				pSlot = (CSlot3 *) pNext;

				pNext = (CSlot3 *) pSlots[uSlot + 1];
				}
			else {
				break;
				}
			}
			
		uIndex++;
		}
	}

UINT CTotalFlow3Master::GetDataCount(CBlock Block, UINT uBytes)
{
	if( IsWord(Block.m_Type) ) {
		
		return uBytes / sizeof(WORD);
		}

	if( IsLong(Block.m_Type) ) {

		return uBytes / sizeof(DWORD);
		}

	return uBytes;
	}

UINT CTotalFlow3Master::GetDataOffset(CBlock Block, UINT uOffset)
{
	if( Block.m_fString ) {

		return uOffset * m_pBase3->m_bStringSize;
		}

	return uOffset;
	}

// Implementation Overridables

void CTotalFlow3Master::GetSlotData(CBlock Block, CSlot ** pSlots, UINT uReply, UINT &uSlot, PCBYTE pData)
{
	if( IsArray( pSlots[uSlot] ) ) {

		GetSlotArrayData(Block, pSlots, uReply, uSlot, pData);

		return;
		}
	
	CTotalFlow2Master::GetSlotData(Block, pSlots, uReply, uSlot, pData);
	}

UINT CTotalFlow3Master::ConstructBlocks(CBlock * pBlocks, CSlot ** pSlots, UINT uCount)
{
	UINT uBlocks = 0;

	for( UINT s = 0; s < uCount; s++ ) {

		CSlot3 * pSlot = (CSlot3 *) pSlots[s];

		if( IsArray(pSlot) ) {

			ConstructArray(pBlocks, uBlocks, pSlot);

			continue;
			}

		ConstructBlock(pBlocks, uBlocks, pSlots, s, uCount);
		}

	return uBlocks;
	}

void CTotalFlow3Master::UpdateReferences(void)
{
	for( UINT n = 0; n < m_pCtx->m_uSlots; n++ ) {

		CSlot3 * pSlot = (CSlot3 *)m_pCtx->m_pSlots[n];

		if( pSlot->m_uRef ) {

			pSlot->m_uRef--;

			if( pSlot->m_uRef == 0 ) {

				pSlot->m_pReq->Empty();
				}
			}
		}
	}

UINT CTotalFlow3Master::GetDataOffset(CSlot * pSlot, AREF Addr)
{
	CSlot3 * pSlot3 = (CSlot3 * )pSlot;

	if( pSlot3 ) {

		return Addr.m_Ref - pSlot3->m_Slot;
		}

	return 0;
	}

UINT CTotalFlow3Master::GetStringOffset(CSlot * pSlot, AREF Addr)
{
	CSlot3 * pSlot3 = (CSlot3 *) pSlot;

	if( pSlot3 && pSlot->m_fString ) {

		return (Addr.m_Ref - pSlot3->m_Slot) % m_pBase3->m_bStringSize;
		}

	return 0;
	}

WORD CTotalFlow3Master::GetWriteOffset(CSlot * pSlot, AREF Addr)
{
	CSlot3 * pSlot3 = (CSlot3 *) pSlot;

	if( pSlot3 ) {

		UINT uOffset = Addr.m_Ref - pSlot->m_Slot;

		if( pSlot3->m_fString ) {

			return uOffset / m_pBase3->m_bStringSize;
			}

		return uOffset;
		}

	return 0;
	}

UINT CTotalFlow3Master::GetStringSize(CSlot * pSlot)
{
	return m_pBase3->m_bStringSize;
	}

WORD CTotalFlow3Master::GetWriteCount(CSlot * pSlot, UINT uCount)
{
	CSlot3 * pSlot3 = (CSlot3 *) pSlot;

	if( pSlot3 && pSlot3->m_fString ) {

		return 1;
		}

	return uCount;
	}

// Helpers

BOOL CTotalFlow3Master::IsArray(CSlot * pSlot)
{
	CSlot3 * pSlot3 = (CSlot3 *) pSlot;

	if( pSlot3 ) {

		UINT uSize = pSlot3->m_Size;

		if( pSlot3->m_fString ) {

			uSize /= m_pBase3->m_bStringSize;
			}

		return uSize > 1;
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Slot Request List
//

// Constructor

CTotalFlowRequestList::CTotalFlowRequestList(IHelper * pHelp)
{
	m_pReq    = NULL;

	m_uCount  = 0;

	m_pHelper = pHelp;
	}

// Destructor

CTotalFlowRequestList::~CTotalFlowRequestList(void)
{
	Empty();
	}

// Operations

void CTotalFlowRequestList::AddRequest(UINT Offset, UINT Count, UINT uStringSize) 
{
	UINT uOffset = uStringSize ? Offset / uStringSize : Offset;

	UINT uCount  = uStringSize ? Count  / uStringSize : Count;

	MakeMax(uCount, 1);

	for( UINT r = 0; r < m_uCount; r++ ) {

		CTotalFlowRequest * pReq = GetRequest(r);

		if( pReq->m_Offset <= uOffset ) {
			
			if( pReq->m_Offset + pReq->m_Count >= uOffset + uCount ) {

				return;
				}
			}

		if( pReq->m_Offset >= uOffset ) {
			
			if( pReq->m_Offset + pReq->m_Count < uOffset + uCount ) {

				pReq->m_Offset = uOffset;

				pReq->m_Count  = uCount;

				return;
				}
			}

		if( pReq->m_Offset >= uOffset ) {

			if( pReq->m_Offset <= uOffset + uCount + BLOCK_SIZE ) {

				pReq->m_Count  = pReq->m_Offset + pReq->m_Count - uOffset;

				pReq->m_Offset = uOffset;

				return;
				}
			}

		if( pReq->m_Offset <= uOffset ) {
			
			if( uOffset <= pReq->m_Offset + pReq->m_Count + BLOCK_SIZE ) {

				pReq->m_Count = uOffset + uCount - pReq->m_Offset;

				return;
				}
			}
		}
	
	CTotalFlowRequest * pNew = new CTotalFlowRequest[m_uCount + 1];

	if( m_pReq ) {

		memcpy(pNew, m_pReq, sizeof(CTotalFlowRequest) * m_uCount);

		delete [] m_pReq;
		}

	m_pReq = pNew;

	m_pReq[m_uCount].m_Offset = uOffset;

	m_pReq[m_uCount].m_Count  = uCount;
	
	m_uCount++;
	}

void CTotalFlowRequestList::Empty(void) 
{
	delete [] m_pReq;

	m_pReq = NULL;

	m_uCount = 0;
	}

UINT CTotalFlowRequestList::GetCount(void)
{
	return m_uCount;
	}

CTotalFlowRequest * CTotalFlowRequestList::GetRequest(UINT uIndex)
{
	if( m_pReq && uIndex < m_uCount ) {

		return &m_pReq[uIndex];
		}

	return NULL;
	}

// Debugging

void CTotalFlowRequestList::PrintRequests(void)
{
	for( UINT u = 0; u < m_uCount; u++ ) {

		AfxTrace("\nReq %u - offset %u count %u", u, GetRequest(u)->m_Offset, GetRequest(u)->m_Count);
		}
	}

// End of File
