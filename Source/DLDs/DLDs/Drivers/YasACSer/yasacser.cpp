

#include "intern.hpp"

#include "yasacser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa AC Drives Driver
//

// Instantiator

INSTANTIATE(CYaskawaACSerialDriver);

// Constructor

CYaskawaACSerialDriver::CYaskawaACSerialDriver(void)
{
	m_Ident     = DRIVER_ID;
	}

// Destructor

CYaskawaACSerialDriver::~CYaskawaACSerialDriver(void)
{
	}

// Configuration

void MCALL CYaskawaACSerialDriver::Load(LPCBYTE pData)
{
	}

void MCALL CYaskawaACSerialDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uBaudRate <= 19200 ) Config.m_uFlags |= flagFastRx;

	Make485(Config, TRUE);
	}

// Management

void MCALL CYaskawaACSerialDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CYaskawaACSerialDriver::Open(void)
{
	}

// Device

CCODE MCALL CYaskawaACSerialDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetWord(pData);

			m_pCtx->m_EC	= 0;
			m_pCtx->m_EV	= 0;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CYaskawaACSerialDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CYaskawaACSerialDriver::Ping(void)
{
	return LoopBack();
	}

CCODE MCALL CYaskawaACSerialDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !m_pCtx->m_bDrop ) return CCODE_ERROR | CCODE_NO_RETRY | CCODE_NO_DATA;

	UINT uTable = Addr.a.m_Table;

	if( IsEnterOrAccept(uTable) ) { // Write only

		pData[0] = 0;

		return 1;
		}

	if( IsErrCommandOrValue(uTable) ) {

		*pData = uTable == SPEC ? m_pCtx->m_EC : m_pCtx->m_EV;

		return 1;
		}

	if( IsYACParam(uTable) && Addr.a.m_Offset == 0 ) {

		return 1;	// list not defined
		}

//**/	Sleep(100);	// slow comms for debug
//**/	AfxTrace3("\r\nT=%d O=%d T=%d ", Addr.a.m_Table, Addr.a.m_Offset, Addr.a.m_Type); AfxTrace1("C=%d ", uCount);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:	return DoBitRead(Addr, pData, min(uCount, 8));

		case addrWordAsWord:	return DoWordRead(Addr, pData, min(uCount, 8));

		case addrWordAsReal:
		case addrWordAsLong:	return DoLongRead(Addr, pData, min(uCount, 4));
		}

	return uCount; // unspecified parameters
	}

CCODE MCALL CYaskawaACSerialDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( IsErrCommandOrValue(Addr.a.m_Table) ) {

		m_pCtx->m_EC = 0;
		m_pCtx->m_EV = 0;

		return 1;
		}

	if( IsEnterOrAccept(Addr.a.m_Table) ) {

		DWORD Data = 0;

		return DoWordWrite(Addr, &Data, 1);
		}

//**/	AfxTrace3("\r\n\n****** WRITE ******\r\nT=%d O=%d D=%8.8lx ", Addr.a.m_Table, Addr.a.m_Offset, *pData);

	UINT u = CheckCommParams(Addr, uCount);

	if( !u ) {	// Address is a comms parameter, don't write

		return 1;
		}

	uCount = u;	// uCount will be reduced if request includes a comms parameter

	BOOL f = IsYACParam(Addr.a.m_Table);

	if( f && Addr.a.m_Offset == 0 ) {

		return 1;	// list not defined
		}

	switch( Addr.a.m_Type) {

		case addrBitAsBit:	// never a YAC parameter

			if( Addr.a.m_Table == SPDA0 ) {

				return DoBitWrite(Addr, pData, min(uCount, 32));
				}
			break;
// Since parameters are not necessarily consecutive, restrict writes to one for YAC parameters.
		case addrWordAsWord:

			if( f || Addr.a.m_Table == SPDA4 ) {

				return DoWordWrite(Addr, pData, f ? 1 : min(uCount, 16));
				}
			break;

		case addrRealAsReal:
		case addrLongAsLong:

			if( f || Addr.a.m_Table == SPDA4 ) {

				return DoLongWrite(Addr, pData, f ? 1 : min(uCount, 8));
				}
			break;
		}

	return uCount; // unspecified parameters
	}

// Implementation

// Frame Building

void CYaskawaACSerialDriver::StartFrame(BYTE bOpcode)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);

	AddByte(bOpcode);
	}

void CYaskawaACSerialDriver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) m_bTx[m_uPtr++] = bData;
	}

void CYaskawaACSerialDriver::AddWord(WORD wData)
{
	AddByte(HIBYTE(wData));

	AddByte(LOBYTE(wData));
	}

void CYaskawaACSerialDriver::AddLong(DWORD dwData)
{
	AddWord(HIWORD(dwData));

	AddWord(LOWORD(dwData));
	}

// Transport Layer

BOOL CYaskawaACSerialDriver::PutFrame(void)
{
	m_CRC.Preset();

	for( UINT i = 0; i < m_uPtr; i++ ) {

		BYTE bData = m_bTx[i];

		m_CRC.Add(bData);
		}

	WORD wCRC = m_CRC.GetValue();

	AddByte(LOBYTE(wCRC));

	AddByte(HIBYTE(wCRC));

	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write(m_bTx, m_uPtr, FOREVER);

	return TRUE;
	}

BOOL CYaskawaACSerialDriver::GetFrame(BOOL fWrite)
{
	UINT uPtr = 0;

	UINT uGap = 0;

	SetTimer(500);

//**/	AfxTrace0("\r\n");

	while( GetTimer() ) {

		UINT uByte;

		if( (uByte = m_pData->Read(5)) < NOTHING ) {

//**/			AfxTrace1("<%2.2x>", uByte);

			m_bRx[uPtr++] = uByte;

			if( uPtr <= sizeof(m_bRx) ) {

				uGap = 0;

				continue;
				}

			return FALSE;
			}

		if( ++uGap >= 5 ) {

			if( uPtr >= 4 ) {

				if( m_bRx[0] == m_bTx[0] ) { // process, if our response

					m_CRC.Preset();

					PBYTE p = m_bRx;

					UINT  n = uPtr - 2;

					for( UINT i = 0; i < n; i++ ) m_CRC.Add(*(p++));

					WORD c1 = IntelToHost(PU2(p)[0]);

					WORD c2 = m_CRC.GetValue();

					if( c1 == c2 ) return c1 == c2;
					}

				uPtr = 0;

				uGap = 0;
				}
			}
		}

	return FALSE;
	}

BOOL CYaskawaACSerialDriver::Transact(BOOL fIgnore)
{
	if( PutFrame() ) {

		if( !m_pCtx->m_bDrop ) { 

			while( m_pData->Read(0) < NOTHING );

			return TRUE;
			}

		if( GetFrame(fIgnore) ) {

			return CheckReply(fIgnore);
			}
		}

	return FALSE;
	}

BOOL CYaskawaACSerialDriver::CheckReply(BOOL fIgnore)
{
	if( fIgnore ) return TRUE;

	if( m_bRx[1] & 0x80 ) {

		m_pCtx->m_EC = *( (PWORD) &m_bTx[2] );

		m_pCtx->m_EV = UINT(m_bRx[2]);
		}

	return TRUE;
	}

// Read Handlers

CCODE CYaskawaACSerialDriver::DoBitRead(AREF Addr, PDWORD pData, UINT uCount)
{
	UINT uOffset = Addr.a.m_Offset;

	switch( Addr.a.m_Table ) {

		case SPDA0: StartFrame(0x01); break;
		case SPDA1: StartFrame(0x02); break;
		default:    return uCount;
		}

	AddWord(uOffset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		UINT b = 3;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			pData[n] = (m_bRx[b] & m) ? TRUE : FALSE;

			if( !(m <<= 1) ) {

				b = b + 1;

				m = 1;
				}
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaACSerialDriver::DoWordRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPDA3: StartFrame(4); break;
		case SPDA4: StartFrame(3); break;
		default:
			if( IsYACParam(Addr.a.m_Table) ) {

				StartFrame(3);
				}

			else {
				return uCount;
				}
			break;
		}

	AddWord(Addr.a.m_Offset);

	AddWord(uCount);

	if( Transact(FALSE) ) {

		uCount = min( uCount, m_bRx[2]/sizeof(WORD) );

		PU2 p = PU2(&m_bRx[3]);

		for( UINT n = 0; n < uCount; n++, p++ ) {

			WORD x = *p;

			pData[n] = LONG(SHORT(MotorToHost(x)));
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

CCODE CYaskawaACSerialDriver::DoLongRead(AREF Addr, PDWORD pData, UINT uCount)
{
	switch( Addr.a.m_Table ) {

		case SPDA3: StartFrame(4); break;
		case SPDA4: StartFrame(3); break;
		default:
			if( IsYACParam(Addr.a.m_Table) ) {

				StartFrame(3);
				}

			else {
				return uCount;
				}
			break;
		}

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);

	if( Transact(FALSE) ) {

		uCount = min(uCount, (m_bRx[2]+2)/sizeof(DWORD));

		PU4 p = PU4(&m_bRx[3]);

		for( UINT n = 0; n < uCount; n++, p++ ) {

			DWORD x = *p;

			pData[n] = MotorToHost(x);
			}

		return uCount;
		}

	return CCODE_ERROR;
	}

// Write Handlers

CCODE CYaskawaACSerialDriver::DoBitWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {

		StartFrame(5);

		AddWord(Addr.a.m_Offset);

		AddWord(pData[0] ? 0xFF00 : 0x0000);
		}
	else {
		StartFrame(15);

		AddWord(Addr.a.m_Offset);

		AddWord(uCount);

		AddByte((uCount + 7) / 8);

		UINT b = 0;

		BYTE m = 1;

		for( UINT n = 0; n < uCount; n++ ) {

			if( pData[n] ) {

				b |= m;
				}

			if( !(m <<= 1) ) {

				AddByte(b);

				b = 0;

				m = 1;
				}
			}

		if( m > 1 ) {

			AddByte(b);
			}
		}

	return Transact(FALSE) ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaACSerialDriver::DoWordWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	if( uCount == 1 ) {

		StartFrame(6);

		AddWord(Addr.a.m_Offset);

		AddWord(LOWORD(pData[0]));
		}
	else {
		StartFrame(16);

		AddWord(Addr.a.m_Offset);

		AddWord(uCount);

		AddByte(uCount * 2);

		for( UINT n = 0; n < uCount; n++ ) {

			AddWord(LOWORD(pData[n]));
			}
		}

	return Transact(FALSE) ? uCount : CCODE_ERROR;
	}

CCODE CYaskawaACSerialDriver::DoLongWrite(AREF Addr, PDWORD pData, UINT uCount)
{
	StartFrame(16);

	AddWord(Addr.a.m_Offset);

	AddWord(uCount * 2);

	AddByte(uCount*4);

	for(UINT n = 0; n < uCount; n++ ) AddLong(pData[n]);

	return Transact(FALSE) ? uCount : CCODE_ERROR;
	}

// Connected
CCODE CYaskawaACSerialDriver::LoopBack(void)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);
	AddByte(0x8);
	AddWord(0);

	WORD wCheck = HostToMotor((WORD)0xA537);

	AddWord(wCheck);

	if( Transact(FALSE) ) {

		PU2 p = PU2(&m_bRx[2]);

		WORD x = (WORD)MotorToHost(*p);

		WORD y = (WORD)MotorToHost(p[1]);

		if( !x && y == wCheck ) return 1;
		}

	return CCODE_ERROR;
	}

// Write Only
BOOL CYaskawaACSerialDriver::IsEnterOrAccept(UINT uTable)
{
	return uTable == SPEN || uTable == SPAC;
	}

// Cached
BOOL CYaskawaACSerialDriver::IsErrCommandOrValue(UINT uTable)
{
	return uTable == SPEC || uTable == SPEV;
	}

// Helpers
BOOL CYaskawaACSerialDriver::IsYACParam(UINT uTable)
{
	switch( uTable ) {

		case SPA:
		case SPB:
		case SPC:
		case SPD:
		case SPE:
		case SPF:
		case SPH:
		case SPL:
		case SPN:
		case SPO:
		case SPP:
		case SPQ:
		case SPT:
		case SPU:
			return TRUE;
		}

	return FALSE;
	}

UINT CYaskawaACSerialDriver::CheckCommParams(AREF Addr, UINT uCount)
{
	if( Addr.a.m_Table == SPH ) {

		UINT uLo = Addr.a.m_Offset;
		UINT uHi = Addr.a.m_Offset + uCount - 1;

		if( uLo <= COMBAUD && uHi >= COMBAUD ) {

			return COMBAUD - uLo;
			}

		if( uLo == COMPAR ) return 0;
		}

	return uCount;
	}

// End of File
