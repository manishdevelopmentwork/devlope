
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_Snmp_HPP
	
#define	INCLUDE_Snmp_HPP

//////////////////////////////////////////////////////////////////////////
//
// Snmp Driver Options
//

class CSnmpDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSnmpDriverOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		UINT GetChangeLimit(void);
		UINT GetTableLimit(void);

	protected:
		// Data Members
		UINT    m_Port1;
		UINT    m_Port2;
		CString m_Comm;
		UINT	m_TabLimit;
		UINT    m_ChgLimit;
		UINT	m_AclAddr1;
		UINT	m_AclMask1;
		UINT	m_AclAddr2;
		UINT	m_AclMask2;
		UINT	m_TrapFrom;
		UINT	m_TrapAddr1;
		UINT	m_TrapMode1;
		UINT	m_TrapAddr2;
		UINT	m_TrapMode2;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(CUIViewWnd *pWnd);
	};

//////////////////////////////////////////////////////////////////////////
//
//  Snmp Driver
//

class CSnmpDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSnmpDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart, CItem * pDrvCfg);

		// Address Helpers
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text, CItem *pDrvCfg);
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace * pSpace, CString Text);
		BOOL DoExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);

		// Implementation
		void AddSpaces(void);

	protected:

		void SetLimits(CItem * pDrvCfg);
	};

//////////////////////////////////////////////////////////////////////////
//
// SNMP Address Selection Dialog
//

class CSnmpAddrDialog : public CStdAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CSnmpAddrDialog(CSnmpDriver &Driver, CAddress &Addr, BOOL fPart);

	protected:
		// Message Map
		AfxDeclareMessageMap();

		// Message and Notification Handlers
		BOOL	OnInitDialog(CWnd &Focus, DWORD dwData);
		void	OnSpaceChange(UINT uID, CWnd &Wnd);

		// Overridables
		void    SetAddressText(CString Text);
		CString GetAddressText(void);

		// Helpers
		void	DoEnables(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SNMP Space
//

class CSnmpSpace : public CSpace
{
	public:
		// Constructors
		CSnmpSpace(UINT uTable, CString p, CString c, UINT r, UINT n, UINT x, UINT t);

		// Limits
		void GetMaximum(CAddress &Addr);
		UINT GetChangeLimit(void);
		void SetChangeLimit(UINT uMax);
		UINT GetTableLimit(void);
		void SetTableLimit(UINT uMax);

		// Implementation
		void EncodeChange(CAddress &Addr, UINT uChange);
		UINT DecodeChange(CAddress  Addr);
	
	protected:

		// Data Members
		UINT m_uChgLimit;

		
	};

// End of File

#endif
