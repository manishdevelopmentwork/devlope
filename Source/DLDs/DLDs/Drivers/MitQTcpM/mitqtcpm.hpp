#include "mittcpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mitsubishi Q Series PLC TCP Master Driver
//

class CMitQTCPMaster : public CMitTCPMasterDriver 
{
	public:
		// Constructor
		CMitQTCPMaster(void);

		// Destructor
		~CMitQTCPMaster(void);

	protected:

		// Overridables
		BOOL Start(void);
		void AddID(void);
		void AddCommand(UINT uType, UINT uCommand, UINT uCount);
		void AddAddress(AREF Addr);
		void AddOffset(UINT uOffset);
		BOOL RecvFrame(UINT uTotal);
		BOOL CheckFrame(void);
		WORD GetCommand(UINT uType, BOOL fWrite);
	};
   
// End of File
