
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Helper Macros
//

#define ScalarToInteger(t, v)					\
								\
	case OpcUaId_##t:					\
	v = C3INT(Value.Value.t);				\
	break							\

#define ScalarToReal(t, v)					\
								\
	case OpcUaId_##t:					\
	v = R2I(C3REAL(Value.Value.t));				\
	break							\

#define ArrayToInteger(t, v)					\
								\
	case OpcUaId_##t:					\
	v = C3INT(Value.Value.Array.Value.t##Array[n]);		\
	break							\

#define ArrayToReal(t, v)					\
								\
	case OpcUaId_##t:					\
	v = R2I(C3REAL(Value.Value.Array.Value.t##Array[n]));	\
	break							\

#define IntegerToString(t, v)					\
								\
	case OpcUaId_##t:					\
	v = DWORD(wstrdup(Format(C3INT(Value.Value.t))));	\
	break							\

#define RealToString(t, v)					\
								\
	case OpcUaId_##t:					\
	v = DWORD(wstrdup(Format(C3REAL(Value.Value.t))));	\
	break							\

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Read Support

bool COpcClient::ReadValues(CDevice *pDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD pData)
{
	AfxTrace("opc: %s readvalues\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	INT                nNodes = uList;

	OpcUa_ReadValueId *pNodes = OpcAlloc(nNodes, OpcUa_ReadValueId);

	{
		for( INT n = 0; n < nNodes; n++ ) {

			UINT               uNode = pList[n];

			OpcUa_ReadValueId *pNode = pNodes + n;

			OpcUa_ReadValueId_Initialize(pNode);

			pNode->AttributeId = OpcUa_Attributes_Value;

			CopyNodeId(&pNode->NodeId, &pDevice->m_Nodes[uNode]);
		}
	}

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;

	OpcUa_DataValue      * pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_Read(pDevice->m_hChannel,
							     &RequestHeader,
							     0,
							     OpcUa_TimestampsToReturn_Neither,
							     nNodes,
							     pNodes,
							     &ResponseHeader,
							     &NoOfResults,
							     &pResults,
							     &NoOfDiagnosticInfos,
							     &pDiagnosticInfos
		);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				UINT             uNode   = pList[n];

				if( pResult->StatusCode == OpcUa_Good ) {

					if( !pDevice->m_Types[uNode] ) {

						pDevice->m_Types.SetAt(uNode, pResult->Value.Datatype);
					}

					DecodeValue(pData[n], pType[n], pResult->Value);
				}
				else {
					switch( pType[n] ) {

						case typeInteger:

							pData[n] = 0;

							break;

						case typeReal:

							pData[n] = R2I(0);

							break;

						case typeString:

							pData[n] = DWORD(wstrdup(""));

							break;
					}
				}
			}
		}

		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_ReadValueId *pNode = pNodes + n;

				OpcUa_ReadValueId_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				OpcUa_DataValue_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
	}

	catch( CExecCancel const & )
	{
		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_ReadValueId *pNode = pNodes + n;

				OpcUa_ReadValueId_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				OpcUa_DataValue_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		throw;
	}
}

bool COpcClient::ReadArray(CDevice *pDevice, UINT uNode, UINT uType, UINT uCount, PDWORD pData)
{
	AfxTrace("opc: %s readarray\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	INT                nNodes = 1;

	OpcUa_ReadValueId *pNodes = OpcAlloc(nNodes, OpcUa_ReadValueId);

	{
		for( INT n = 0; n < nNodes; n++ ) {

			OpcUa_ReadValueId *pNode = pNodes + n;

			OpcUa_ReadValueId_Initialize(pNode);

			pNode->AttributeId = OpcUa_Attributes_Value;

			CopyNodeId(&pNode->NodeId, &pDevice->m_Nodes[uNode]);
		}
	}

	// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;

	OpcUa_DataValue      * pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_Read(pDevice->m_hChannel,
							     &RequestHeader,
							     0,
							     OpcUa_TimestampsToReturn_Neither,
							     nNodes,
							     pNodes,
							     &ResponseHeader,
							     &NoOfResults,
							     &pResults,
							     &NoOfDiagnosticInfos,
							     &pDiagnosticInfos
		);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				if( pResult->StatusCode == OpcUa_Good ) {

					if( !pDevice->m_Types[uNode] ) {

						pDevice->m_Types.SetAt(uNode, pResult->Value.Datatype);
					}

					DecodeArray(pData, uCount, uType, pResult->Value);
				}
				else {
					for( UINT i = 0; i < uCount; i++ ) {

						switch( uType ) {

							case typeInteger:

								pData[i] = 0;

								break;

							case typeReal:

								pData[i] = R2I(0);

								break;
						}
					}
				}
			}
		}

		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_ReadValueId *pNode = pNodes + n;

				OpcUa_ReadValueId_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				OpcUa_DataValue_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
	}

	catch( CExecCancel const & )
	{
		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_ReadValueId *pNode = pNodes + n;

				OpcUa_ReadValueId_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				OpcUa_DataValue_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		throw;
	}
}

bool COpcClient::ReadType(CDevice *pDevice, UINT &uType, bool &fWrite, OpcUa_NodeId const &Node)
{
	AfxTrace("opc: %s readtype\n", PCTXT(pDevice->m_Short));

	// Request Parameters

	OpcUa_RequestHeader RequestHeader;

	FillRequestHeader(&RequestHeader, pDevice);

	INT                nNodes = 3;

	OpcUa_ReadValueId *pNodes = OpcAlloc(nNodes, OpcUa_ReadValueId);

	{
		for( INT n = 0; n < nNodes; n++ ) {

			OpcUa_ReadValueId *pNode = pNodes + n;

			OpcUa_ReadValueId_Initialize(pNode);

			if( n == 0 ) {

				pNode->AttributeId = OpcUa_Attributes_ValueRank;
			}

			if( n == 1 ) {

				pNode->AttributeId = OpcUa_Attributes_DataType;
			}

			if( n == 2 ) {

				pNode->AttributeId = OpcUa_Attributes_AccessLevel;
			}

			CopyNodeId(&pNode->NodeId, &Node);
		}
	}

// Response Parameters

	OpcUa_ResponseHeader ResponseHeader;

	OpcUa_ResponseHeader_Initialize(&ResponseHeader);

	OpcUa_Int32            NoOfResults         = 0;

	OpcUa_DataValue      * pResults            = NULL;

	OpcUa_Int32            NoOfDiagnosticInfos = 0;

	OpcUa_DiagnosticInfo * pDiagnosticInfos    = NULL;

	try {
		// Service Invocation

		OpcUa_StatusCode Code = OpcUa_ClientApi_Read(pDevice->m_hChannel,
							     &RequestHeader,
							     0,
							     OpcUa_TimestampsToReturn_Neither,
							     nNodes,
							     pNodes,
							     &ResponseHeader,
							     &NoOfResults,
							     &pResults,
							     &NoOfDiagnosticInfos,
							     &pDiagnosticInfos
		);

		// Response Processing

		if( CheckResponse(pDevice, Code, RequestHeader, ResponseHeader) ) {

			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				if( n == 0 ) {

					if( pResult->StatusCode == OpcUa_Good ) {

						if( pResult->Value.Datatype == OpcUaId_Int32 ) {

							if( pResult->Value.Value.Int32 == OpcUa_ValueRanks_Scalar ) {

								continue;
							}
						}
					}

					uType = 0;

					break;
				}

				if( n == 1 ) {

					if( pResult->StatusCode == OpcUa_Good ) {

						if( pResult->Value.Datatype == OpcUaId_NodeId ) {

							if( pResult->Value.Value.NodeId->IdentifierType == OpcUa_IdType_Numeric ) {

								uType = pResult->Value.Value.NodeId->Identifier.Numeric;

								continue;
							}
						}
					}

					uType = 0;

					break;
				}

				if( n == 2 ) {

					if( pResult->StatusCode == OpcUa_Good ) {

						if( pResult->Value.Datatype == OpcUaId_Byte ) {

							fWrite = !!(pResult->Value.Value.Byte & OpcUa_AccessLevels_CurrentWrite);

							continue;
						}
					}

					uType = 0;

					break;
				}
			}
		}

		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_ReadValueId *pNode = pNodes + n;

				OpcUa_ReadValueId_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				OpcUa_DataValue_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		// Check for Success

		return Code == OpcUa_Good;
	}

	catch( CExecCancel const & )
	{
		// Parameter Release

		{
			for( INT n = 0; n < nNodes; n++ ) {

				OpcUa_ReadValueId *pNode = pNodes + n;

				OpcUa_ReadValueId_Clear(pNode);
			}
		}

		{
			for( INT n = 0; n < NoOfResults; n++ ) {

				OpcUa_DataValue *pResult = pResults + n;

				OpcUa_DataValue_Clear(pResult);
			}
		}

		FreeDiagnosticInfo(NoOfDiagnosticInfos, pDiagnosticInfos);

		OpcUa_Free(pNodes);

		OpcUa_Free(pResults);

		throw;
	}
}

void COpcClient::DecodeValue(DWORD &Data, UINT Type, OpcUa_Variant const &Value)
{
	if( Type == typeInteger ) {

		switch( Value.Datatype ) {

			ScalarToInteger(Double, Data);
			ScalarToInteger(Float, Data);
			ScalarToInteger(Byte, Data);
			ScalarToInteger(SByte, Data);
			ScalarToInteger(Int16, Data);
			ScalarToInteger(UInt16, Data);
			ScalarToInteger(Int32, Data);
			ScalarToInteger(UInt32, Data);
			ScalarToInteger(Int64, Data);
			ScalarToInteger(UInt64, Data);

			case OpcUaId_Boolean:

				Data = Value.Value.Boolean ? 1 : 0;

				break;

			case OpcUaId_DateTime:

				Data = UnixFromTime(Value.Value.DateTime, 0);

				Data = Data - 9862 * 60 * 60 * 24;

				break;

			case OpcUaId_String:

				Data = C3INT(atoi(Value.Value.String.strContent));

				break;

			default:

				Data = 0;

				break;
		}
	}

	if( Type == typeReal ) {

		switch( Value.Datatype ) {

			ScalarToReal(Double, Data);
			ScalarToReal(Float, Data);
			ScalarToReal(Byte, Data);
			ScalarToReal(SByte, Data);
			ScalarToReal(Int16, Data);
			ScalarToReal(UInt16, Data);
			ScalarToReal(Int32, Data);
			ScalarToReal(UInt32, Data);
			ScalarToReal(Int64, Data);
			ScalarToReal(UInt64, Data);

			case OpcUaId_Boolean:

				Data = R2I(C3REAL(Value.Value.Boolean ? 1.0 : 0.0));

				break;

			case OpcUaId_String:

				Data = R2I(C3REAL(atof(Value.Value.String.strContent)));

				break;

			default:

				Data = R2I(0);

				break;
		}
	}

	if( Type == typeString ) {

		switch( Value.Datatype ) {

			RealToString(Double, Data);
			RealToString(Float, Data);

			IntegerToString(Byte, Data);
			IntegerToString(SByte, Data);
			IntegerToString(Int16, Data);
			IntegerToString(UInt16, Data);
			IntegerToString(Int32, Data);
			IntegerToString(UInt32, Data);
			IntegerToString(Int64, Data);
			IntegerToString(UInt64, Data);

			case OpcUaId_Boolean:

				Data = DWORD(wstrdup(Value.Value.Boolean ? "true" : "false"));

				break;

			case OpcUaId_String:

				Data = DWORD(UtfDecode(Value.Value.String.strContent));

				break;

			default:

				Data = DWORD(wstrdup(""));

				break;
		}
	}
}

void COpcClient::DecodeArray(DWORD *pData, UINT uCount, UINT Type, OpcUa_Variant const &Value)
{
	if( Value.ArrayType ) {

		for( UINT n = 0; n < uCount; n++ ) {

			if( INT(n) < Value.Value.Array.Length ) {

				if( Type == typeInteger ) {

					switch( Value.Datatype ) {

						ArrayToInteger(Double, pData[n]);
						ArrayToInteger(Float, pData[n]);
						ArrayToInteger(Byte, pData[n]);
						ArrayToInteger(SByte, pData[n]);
						ArrayToInteger(Int16, pData[n]);
						ArrayToInteger(UInt16, pData[n]);
						ArrayToInteger(Int32, pData[n]);
						ArrayToInteger(UInt32, pData[n]);
						ArrayToInteger(Int64, pData[n]);
						ArrayToInteger(UInt64, pData[n]);

						case OpcUaId_Boolean:

							pData[n] = Value.Value.Array.Value.BooleanArray[n] ? 1 : 0;

							break;

						case OpcUaId_DateTime:

							pData[n] = UnixFromTime(Value.Value.DateTime, 0);

							pData[n] = pData[n] - 9862 * 60 * 60 * 24;

							break;

						case OpcUaId_String:

							pData[n] = C3INT(atoi(Value.Value.Array.Value.StringArray[n].strContent));

							break;

						default:

							pData[n] = 0;

							break;
					}
				}

				if( Type == typeReal ) {

					switch( Value.Datatype ) {

						ArrayToReal(Double, pData[n]);
						ArrayToReal(Float, pData[n]);
						ArrayToReal(Byte, pData[n]);
						ArrayToReal(SByte, pData[n]);
						ArrayToReal(Int16, pData[n]);
						ArrayToReal(UInt16, pData[n]);
						ArrayToReal(Int32, pData[n]);
						ArrayToReal(UInt32, pData[n]);
						ArrayToReal(Int64, pData[n]);
						ArrayToReal(UInt64, pData[n]);

						case OpcUaId_Boolean:

 							pData[n] = R2I(C3REAL(Value.Value.Array.Value.BooleanArray[n] ? 1.0 : 0.0));

							break;

						case OpcUaId_String:

							pData[n] = R2I(C3REAL(atof(Value.Value.Array.Value.StringArray[n].strContent)));

							break;

						default:

							pData[n] = R2I(0);

							break;
					}
				}
			}
			else {
				if( Type == typeInteger ) {

					pData[n] = 0;
				}

				if( Type == typeReal ) {

					pData[n] = R2I(0);
				}
			}
		}
	}
}

// End of File
