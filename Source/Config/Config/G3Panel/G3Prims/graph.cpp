
#include "intern.hpp"

#include "graph.hpp"

#include "scale.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Bar Graph
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyBarGraph, CPrimLegacyFigure);

// Constructor

CPrimLegacyBarGraph::CPrimLegacyBarGraph(void)
{
	m_pCount = NULL;

	m_pFill  = New CPrimBrush;

	m_pValue = NULL;

	m_pMin   = NULL;

	m_pMax   = NULL;
	}

// Initial Values

void CPrimLegacyBarGraph::SetInitValues(void)
{
	CPrimLegacyFigure::SetInitValues();

	SetInitial(L"Count", m_pCount, L"10");
	}

// Type Access

BOOL CPrimLegacyBarGraph::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Count" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag == "Value" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = flagElement | flagWritable;

		return TRUE;
		}

	if( Tag == "Min" || Tag == "Max" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}
	
	return FALSE;
	}

// Overridables

void CPrimLegacyBarGraph::SetInitState(void)
{
	CPrimLegacyFigure::SetInitState();
	
	m_pBack->m_Pattern = brushNull;

	m_pEdge->m_Corner = 1;
	
	m_pFill->Set(GetRGB(8, 8, 8));
	
	SetInitSize(200, 200);
	}

// Download Support

BOOL CPrimLegacyBarGraph::MakeInitData(CInitData &Init)
{
	CPrimLegacyFigure::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValue);
	Init.AddItem(itemVirtual, m_pCount);
	Init.AddItem(itemVirtual, m_pMin);
	Init.AddItem(itemVirtual, m_pMax);
	Init.AddItem(itemSimple,  m_pFill);

	return TRUE;
	}

// Meta Data

void CPrimLegacyBarGraph::AddMetaData(void)
{
	CPrimLegacyFigure::AddMetaData();

	Meta_AddVirtual(Count);
	Meta_AddVirtual(Value);
	Meta_AddVirtual(Min);
	Meta_AddVirtual(Max);
	Meta_AddObject (Fill);

	Meta_SetName((IDS_BAR_GRAPH));
	}

// Implementation

void CPrimLegacyBarGraph::DrawBack(IGDI *pGDI, R2 &Rect)
{
	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Vertical Bar Graph
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyVertBarGraph, CPrimLegacyBarGraph);

// Constructor

CPrimLegacyVertBarGraph::CPrimLegacyVertBarGraph(void)
{
	m_uType = 0x28;
	}

// Overridables

void CPrimLegacyVertBarGraph::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	DrawBack(pGDI, Rect);

	UINT uCount = m_pCount ? m_pCount->ExecVal() : 0;

	if( uCount ) {

		int x1 = Rect.x1;

		int y1 = Rect.y1;

		int x2 = Rect.x2;

		int y2 = Rect.y2;

		for( UINT n = 0; n < uCount; n ++ ) {

			int yp = y2 - (1 + (n+4) % 10) * (y2 - y1) / 10;

			int xa = x1 + (n+0) * (x2 - x1 + 1) / uCount;

			int xb = x1 + (n+1) * (x2 - x1 + 1) / uCount;

			R2 Bar = { xa, yp, xb-1, y2 };

			m_pFill->FillRect(pGDI, Bar);
			}
		}
	}

// Download Support

BOOL CPrimLegacyVertBarGraph::MakeInitData(CInitData &Init)
{
	CPrimLegacyBarGraph::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimLegacyVertBarGraph::AddMetaData(void)
{
	CPrimLegacyBarGraph::AddMetaData();

	Meta_SetName((IDS_VERT_BAR_GRAPH));
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Horizontal Bar Graph
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyHorzBarGraph, CPrimLegacyBarGraph);

// Constructor

CPrimLegacyHorzBarGraph::CPrimLegacyHorzBarGraph(void)
{
	m_uType = 0x29;
	}

// Overridables

void CPrimLegacyHorzBarGraph::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	DrawBack(pGDI, Rect);

	UINT uCount = m_pCount ? m_pCount->ExecVal() : 0;

	if( uCount ) {

		int x1 = Rect.x1;

		int y1 = Rect.y1;

		int x2 = Rect.x2;

		int y2 = Rect.y2;

		for( UINT n = 0; n < uCount; n ++ ) {

			int xp = x1 + (1 + (n+4) % 10) * (x2 - x1) / 10;

			int ya = y2 - (n+1) * (y2 - y1 + 1) / uCount;

			int yb = y2 - (n+0) * (y2 - y1 + 1) / uCount;

			R2 Rect = { x1, ya+1, xp, yb };

			m_pFill->FillRect(pGDI, Rect);
			}
		}
	}

// Download Support

BOOL CPrimLegacyHorzBarGraph::MakeInitData(CInitData &Init)
{
	CPrimLegacyBarGraph::MakeInitData(Init);

	return TRUE;
	}

// Meta Data

void CPrimLegacyHorzBarGraph::AddMetaData(void)
{
	CPrimLegacyBarGraph::AddMetaData();

	Meta_SetName((IDS_HORZ_BAR_GRAPH));
	}

//////////////////////////////////////////////////////////////////////////
//
// Graph Line Limits
//

// Dynamic Class

AfxImplementDynamicClass(CGraphLineLimits, CCodedHost);

// Constructor

CGraphLineLimits::CGraphLineLimits(void)
{
	m_pMinX = NULL;

	m_pMaxX = NULL;

	m_pMinY = NULL;

	m_pMaxY = NULL;
	}

// Attributes

C3REAL CGraphLineLimits::GetMinX(void)
{
	return m_pMinX ? I2R(m_pMinX->Execute(typeReal)) : C3REAL(0.0);
	}

C3REAL CGraphLineLimits::GetMaxX(void)
{
	return m_pMaxX ? I2R(m_pMaxX->Execute(typeReal)) : C3REAL(100.0);
	}

C3REAL CGraphLineLimits::GetMinY(void)
{
	return m_pMinY ? I2R(m_pMinY->Execute(typeReal)) : C3REAL(0.0);
	}

C3REAL CGraphLineLimits::GetMaxY(void)
{
	return m_pMaxY ? I2R(m_pMaxY->Execute(typeReal)) : C3REAL(100.0);
	}

// Type Access

BOOL CGraphLineLimits::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.StartsWith(L"Min") || Tag.StartsWith(L"Max") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}

	return FALSE;
	}

// Download Support

BOOL CGraphLineLimits::MakeInitData(CInitData &Init)
{
	Init.AddItem(itemVirtual, m_pMinX);
	Init.AddItem(itemVirtual, m_pMaxX);

	Init.AddItem(itemVirtual, m_pMinY);
	Init.AddItem(itemVirtual, m_pMaxY);

	return TRUE;
	}

// Meta Data Creation

void CGraphLineLimits::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddVirtual(MinX);
	Meta_AddVirtual(MaxX);
	Meta_AddVirtual(MinY);
	Meta_AddVirtual(MaxY);
	}

//////////////////////////////////////////////////////////////////////////
//
// Rich Legacy Primitive Page
//

class CPrimLinePage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CPrimLinePage(CPrimLegacyGraphLine *pData, CString Title, UINT uView);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CPrimLegacyGraphLine * m_pData;
	};

//////////////////////////////////////////////////////////////////////////
//
// Line Legacy Primitive Page
//

// Runtime Class

AfxImplementRuntimeClass(CPrimLinePage, CUIStdPage);

// Constructor

CPrimLinePage::CPrimLinePage(CPrimLegacyGraphLine *pData, CString Title, UINT uPage)
{
	m_pData  = pData;

	m_Title  = Title;

	m_Class  = AfxPointerClass(pData);

	m_uPage  = uPage;
	}

// Operations

BOOL CPrimLinePage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	CUIPage *pPage = New CUIStdPage(AfxPointerClass(m_pData), m_uPage);

	pPage->LoadIntoView(pView, m_pData);

	delete pPage;

	pView->StartGroup(CString("Axis Limits"), 1);

	pView->AddButton( CString(IDS_EDIT),
			  CString("Edit the X and Y Axis limits."),
			  L"Limits"
			  );

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Graph Line
//

// Runtime Class

AfxImplementRuntimeClass(CPrimLegacyGraphLine, CCodedHost);

// Scaling Macros

#define	ScaleX(d)	(x1 + ((d) - 0) * (x2 - x1 - 3) / 1000 + 1)

#define	ScaleY(d)	(y2 - ((d) - 0) * (y2 - y1 - 3) / 1000 - 2)

// Constructor

CPrimLegacyGraphLine::CPrimLegacyGraphLine(UINT uChan)
{
	m_uChan     = uChan;

	m_pValueX   = NULL;

	m_pValueY   = NULL;

	m_pCount    = NULL;
	
	m_pLimits   = New CGraphLineLimits;

	m_Style     = 0;

	m_Fit       = 0;

	m_pDataFill = New CPrimColor;

	m_pDataLine = New CPrimColor;

	m_pBestLine = New CPrimColor;
	}

// UI Update

void CPrimLegacyGraphLine::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			m_fXAutoLimits = HasXAutoLimits();

			m_fYAutoLimits = HasYAutoLimits();
			
			DoEnables(pHost);
			}

		if( Tag == L"Count" || Tag == L"Style" || Tag == L"Fit" ) {
			
			CPrim *pPrim = (CPrim *) GetParent();

			pPrim->OnUIChange(pHost, pItem, L"PrimProp");
			
			DoEnables(pHost);
			}

		if( Tag == L"Limits" ) {

			CItemDialog Dlg(m_pLimits, CString("Axis Limits"));

			CSystemWnd  &System = (CSystemWnd &) afxMainWnd->GetDlgItem(IDVIEW);

			if( System.ExecSemiModal(Dlg) ) {

				m_fXAutoLimits = HasXAutoLimits();

				m_fYAutoLimits = HasYAutoLimits();
				
				pHost->RemakeUI();
				}
			}

		if( Tag == L"ValueX" ) {

			if( m_fXAutoLimits ) {

				if( m_pValueX && m_pValueX->IsTagRef() ) {

					CString Tag = m_pValueX->GetTagName();

					InitCoded(pHost, L"MinX", m_pLimits->m_pMinX, Tag + L".Min");

					InitCoded(pHost, L"MaxX", m_pLimits->m_pMaxX, Tag + L".Max");

					m_fXAutoLimits = TRUE;

					return;
					}
				}

			m_fXAutoLimits = HasXAutoLimits();
			}

		if( Tag == L"ValueY" ) {

			if( m_fYAutoLimits ) {

				if( m_pValueY && m_pValueY->IsTagRef() ) {

					CString Tag = m_pValueY->GetTagName();

					InitCoded(pHost, L"MinY", m_pLimits->m_pMinY, Tag + L".Min");

					InitCoded(pHost, L"MaxY", m_pLimits->m_pMaxY, Tag + L".Max");

					m_fYAutoLimits = TRUE;

					return;
					}
				}

			m_fYAutoLimits = HasYAutoLimits();
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Initial Values

void CPrimLegacyGraphLine::SetInitValues(void)
{
	CCodedHost::SetInitValues();

	SetInitial(L"Count", m_pCount, m_uChan ? L"0" : L"10");
	}

// Type Access

BOOL CPrimLegacyGraphLine::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == L"Count" ) {

		Type.m_Type  = typeInteger;

		Type.m_Flags = 0;

		return TRUE;
		}

	if( Tag.StartsWith(L"Value") ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = flagElement | flagWritable;

		return TRUE;
		}

	if( Tag.StartsWith(L"Min") || Tag.StartsWith(L"Max") ) {
		
		return m_pLimits->GetTypeData(Tag, Type);
		}

	return FALSE;
	}

// Operations

void CPrimLegacyGraphLine::Draw(IGDI *pGDI, R2 Rect)
{
	UINT uCount = m_pCount ? m_pCount->ExecVal() : 10;

	if( uCount ) {

		int x1 = Rect.x1;

		int y1 = Rect.y1;

		int x2 = Rect.x2;

		int y2 = Rect.y2;

		if( m_Style > 0 ) {

			srand(UINT(this));

			pGDI->SetPenFore(m_pDataLine->GetColor());

			int ox = 0;
			
			int oy = 0;

			for( UINT n = 0; n < uCount; n++ ) {

				int xd = 100 + n * 800 / (uCount-1);

				int yd = xd + 100 - rand() % 200;

				int xp = ScaleX(xd);
				
				int yp = ScaleY(yd);

				if( n ) pGDI->DrawLine(ox, oy, xp, yp);

				ox = xp;
			
				oy = yp;
				}
			}

		if( m_Style < 2 ) {

			srand(UINT(this));

			pGDI->SetBrushFore(m_pDataFill->GetColor());

			for( UINT n = 0; n < uCount; n++ ) {

				int xd = 100 + n * 800 / uCount;

				int yd = xd + 100 - rand() % 200;

				int xp = ScaleX(xd);
				
				int yp = ScaleY(yd);

				R2 Fill = { xp-1, yp-1, xp+2, yp+2 };

				pGDI->FillRect(PassRect(Fill));
				}
			}

		if( m_Fit ) {

			pGDI->SetPenFore(m_pBestLine->GetColor());
			
			pGDI->DrawLine(x1+4, y2-4, x2-4, y1+4);
			}
		}
	}

// Download Support

BOOL CPrimLegacyGraphLine::MakeInitData(CInitData &Init)
{
	CCodedHost::MakeInitData(Init);

	Init.AddItem(itemVirtual, m_pValueX);

	Init.AddItem(itemVirtual, m_pValueY);

	Init.AddItem(itemVirtual, m_pCount);

	Init.AddItem(itemSimple,  m_pLimits);

	Init.AddByte(BYTE(m_Style));
	Init.AddByte(BYTE(m_Fit));

	Init.AddItem(itemSimple,  m_pDataFill);
	Init.AddItem(itemSimple,  m_pDataLine);
	Init.AddItem(itemSimple,  m_pBestLine);

	return TRUE;
	}
		
// Meta Data

void CPrimLegacyGraphLine::AddMetaData(void)
{
	CCodedHost::AddMetaData();
	
	Meta_AddVirtual(ValueX);
	Meta_AddVirtual(ValueY);
	Meta_AddVirtual(Count);
	Meta_AddObject (Limits);
	Meta_AddInteger(Style);
	Meta_AddInteger(Fit);
	Meta_AddObject (DataFill);
	Meta_AddObject (DataLine);
	Meta_AddObject (BestLine);

	Meta_SetName(IDS_SCATTER_GRAPH_2);
	}

// Implementation

void CPrimLegacyGraphLine::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI("BestLine", m_Fit   > 0);

	pHost->EnableUI("DataLine", m_Style > 0);

	pHost->EnableUI("DataFill", m_Style < 2);
	}

BOOL CPrimLegacyGraphLine::HasXAutoLimits(void)
{
	if( m_pLimits->m_pMinX && m_pLimits->m_pMaxX ) {

		if( m_pValueX ) {
		
			CString c1 = m_pValueX->GetSource(FALSE);

			CString c2 = m_pLimits->m_pMinX->GetSource  (FALSE);

			CString c3 = m_pLimits->m_pMaxX->GetSource  (FALSE);

			if( c2 == c1 + L".Min" ) {

				if( c3 == c1 + L".Max" ) {

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( m_pLimits->m_pMinX || m_pLimits->m_pMaxX ) {

		return FALSE;
		}
	
	return TRUE;
	}

BOOL CPrimLegacyGraphLine::HasYAutoLimits(void)
{
	if( m_pLimits->m_pMinY && m_pLimits->m_pMaxY ) {

		if( m_pValueY ) {
		
			CString c1 = m_pValueY->GetSource(FALSE);

			CString c2 = m_pLimits->m_pMinY->GetSource  (FALSE);

			CString c3 = m_pLimits->m_pMaxY->GetSource  (FALSE);

			if( c2 == c1 + L".Min" ) {

				if( c3 == c1 + L".Max" ) {

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	if( m_pLimits->m_pMinY || m_pLimits->m_pMaxY ) {

		return FALSE;
		}
	
	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Scatter Graph
//

// Dynamic Class

AfxImplementDynamicClass(CPrimLegacyScatterGraph, CPrimLegacyFigure);

// Constructor

CPrimLegacyScatterGraph::CPrimLegacyScatterGraph(void)
{
	m_uType = 0x2A;

	for( UINT n = 0; n < elements(m_pLines); n++ ) {
		
		m_pLines[n] = New CPrimLegacyGraphLine(n);
		}

	m_GridType    = 0;

	m_GridFont    = fontHei16;

	m_HorzGap     = 8;

	m_VertPrecise = 1;

	m_HorzPrecise = 1;

	m_pVertCol    = New CPrimColor;

	m_pHorzCol    = New CPrimColor;
	}

// UI Overridables

BOOL CPrimLegacyScatterGraph::OnLoadPages(CUIPageList *pList)
{
	LoadLinePages(pList);

	pList->Append( New CUIStdPage( CString(IDS_OPTIONS),
				       AfxPointerClass(this),
				       5
				       ));

	LoadTailPages(pList);
	
	return TRUE;
	}

// UI Update

void CPrimLegacyScatterGraph::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {

		DoEnables(pHost);
		}

	if( Tag == L"GridType" ) {

		DoEnables(pHost);
		}

	CPrimLegacyFigure::OnUIChange(pHost, pItem, Tag);
	}

// Overridables

void CPrimLegacyScatterGraph::Draw(IGDI *pGDI, UINT uMode)
{
	R2 Rect = m_DrawRect;

	Rect.x2 -= 1;

	Rect.y2 -= 1;

	if( m_pEdge->m_Width ) {

		m_pEdge->AdjustRect(Rect);

		m_pEdge->DrawRect(pGDI, Rect, drawWhole);

		DeflateRect(Rect, 1, 1);

		pGDI->ResetPen();

		pGDI->SetPenFore(0);

		pGDI->DrawRect(PassRect(Rect));

		DeflateRect(Rect, 1, 1);
		}

	pGDI->ResetBrush();

	m_pBack->FillRect(pGDI, Rect);

	DrawGrid(pGDI, Rect);

	for( UINT uLine = 0; uLine < elements(m_pLines); uLine ++ ) {

		m_pLines[uLine]->Draw(pGDI, Rect);
		}
	}

void CPrimLegacyScatterGraph::SetInitState(void)
{
	CPrimLegacyFigure::SetInitState();

	COLOR Col[] = {	GetRGB(31,  0,  0), 
			GetRGB( 0, 31,  0), 
			GetRGB( 0,  0, 31), 
			GetRGB( 0,  0,  0),
			};

	for( UINT n = 0; n < elements(m_pLines); n ++ ) {

		m_pLines[n]->m_pBestLine->SetInitial(GetRGB(31, 31, 31));

		m_pLines[n]->m_pDataLine->SetInitial(GetRGB(31, 31, 31));
		
		m_pLines[n]->m_pDataFill->Set(Col[n % elements(Col)]);
		}

	m_pVertCol->Set(GetRGB(31,31,31));

	m_pHorzCol->Set(GetRGB(31,31,31));

	SetInitSize(200, 200);
	}

// Download Support

BOOL CPrimLegacyScatterGraph::MakeInitData(CInitData &Init)
{
	CPrimLegacyFigure::MakeInitData(Init);

	UINT uCount = 0;

	UINT n;

	for( n = 0; n < elements(m_pLines); n ++ ) {

		CPrimLegacyGraphLine * pGraph = m_pLines[n];

		if( pGraph->m_pValueX && pGraph->m_pValueY ) {
			
			uCount ++;
			}
		}

	Init.AddByte(BYTE(uCount));

	for( n = 0; n < elements(m_pLines); n ++ ) {

		CPrimLegacyGraphLine * pGraph = m_pLines[n];

		if( pGraph->m_pValueX && pGraph->m_pValueY ) {

			pGraph->MakeInitData(Init);
			}
		}

	Init.AddByte(BYTE(m_GridType));

	if( m_GridType & 1 ) {

		Init.AddItem(itemSimple, m_pVertCol);

		Init.AddWord(WORD(m_GridFont));
		
		Init.AddByte(BYTE(m_VertPrecise));
		}

	if( m_GridType & 2 ) {	

		Init.AddItem(itemSimple, m_pHorzCol);

		Init.AddByte(BYTE(m_HorzGap));

		Init.AddByte(BYTE(m_HorzPrecise));
		}
	
	return TRUE;
	}

// Meta Data

void CPrimLegacyScatterGraph::AddMetaData(void)
{
	CPrimLegacyFigure::AddMetaData();

	for( UINT n = 0; n < elements(m_pLines); n ++ ) {

		Meta_Add(CPrintf(L"Line%d", n), m_pLines[n], metaObject);
		}

	Meta_AddInteger(GridType);
	Meta_AddInteger(GridFont);
	Meta_AddInteger(HorzGap);
	Meta_AddObject (VertCol);
	Meta_AddObject (HorzCol);
	Meta_AddInteger(HorzPrecise);
	Meta_AddInteger(VertPrecise);
	
	Meta_SetName((IDS_SCATTER_GRAPH));
	}

// Implementation

void CPrimLegacyScatterGraph::DoEnables(IUIHost *pHost)
{
	BOOL fVert = m_GridType & 1;

	BOOL fHorz = m_GridType & 2;

	pHost->EnableUI("VertCol",     fVert);

	pHost->EnableUI("GridFont",    fVert);

	pHost->EnableUI("VertPrecise", fVert);
	
	pHost->EnableUI("HorzCol",     fHorz);

	pHost->EnableUI("HorzGap",     fHorz);

	pHost->EnableUI("HorzPrecise", fHorz);
	}

void CPrimLegacyScatterGraph::LoadLinePages(CUIPageList *pList)
{
	for( UINT n = 0; n < elements(m_pLines); n ++ ) {

		CPrimLinePage *pPage = New CPrimLinePage(m_pLines[n], CPrintf(L"Line %d", n+1), 0);

		pList->Append(pPage);
		}
	}

void CPrimLegacyScatterGraph::DrawGrid(IGDI *pGDI, R2 Rect)
{
	if( m_GridType ) {

		pGDI->ResetAll();

		///////

		int x1 = Rect.x1;
		
		int x2 = Rect.x2;
		
		int y1 = Rect.y1;
		
		int y2 = Rect.y2;

		if( m_GridType & 1 ) {

			SelectFont(pGDI, m_GridFont);

			pGDI->SetBrushFore(m_pVertCol->GetColor());

			int fy = pGDI->GetTextHeight(L"0");

			////////
			
			int    ySize    = y2 - y1;

			double Limit    = min(10, ySize / (fy + 4));

			double DataMin  = GetMinY();

			double DataMax  = GetMaxY();

			CScaleHelper Helper(DataMin, DataMax);

			Helper.CalcDecimalStep(Limit);

			int    StepSpan = Helper.GetStepSpan();

			int    Steps    = max(1, StepSpan);

			////////

			for( int n = 0; n <= Steps; n++ ) {

				double Value = Helper.GetStepValue(n);

				int yPos  = -1;

				if( !m_VertPrecise && StepSpan ) {

					yPos = y2 - (ySize * n) / StepSpan;
					}
				else {
					if( n == 0 ) {

						Value = DataMin;

						yPos  = y2;
						}

					if( n == Steps ) {

						Value = DataMax;

						yPos  = y1;
						}

					if( yPos < 0 ) {

						double f = (Value - DataMin) / (DataMax - DataMin);

						yPos     = y2 - int(ySize * f);
						}
					}

				if( m_pEdge->m_Width ) {

					if( n == 0 ) {					
					
						continue;
						}

					if( n == Steps ) {
					
						continue;
						}
					}

				pGDI->FillRect(x1, yPos, x2, yPos + 1);
				}
			}

		if( m_GridType & 2 ) {

			pGDI->SetBrushFore(m_pHorzCol->GetColor());

			int fx = m_HorzGap;

			////////
			
			int    xSize    = x2 - x1;

			double Limit    = min(10, xSize / fx);

			double DataMin  = GetMinX();

			double DataMax  = GetMaxX();

			CScaleHelper Helper(DataMin, DataMax);

			Helper.CalcDecimalStep(Limit);

			int    StepSpan = Helper.GetStepSpan();

			int    Steps    = max(1, StepSpan);

			////////

			for( int n = 0; n <= Steps; n++ ) {

				double Value = Helper.GetStepValue(n);

				int    xPos  = -1;

				if( !m_VertPrecise && StepSpan ) {

					xPos = x2 - (xSize * n) / StepSpan;
					}
				else {
					if( n == 0 ) {

						Value = DataMin;

						xPos  = x2;
						}

					if( n == Steps ) {

						Value = DataMax;

						xPos  = x1;
						}

					if( xPos < 0 ) {

						double f = (Value - DataMin) / (DataMax - DataMin);

						xPos     = x2 - int(xSize * f);
						}
					}

				if( m_pEdge->m_Width ) {

					if( n == 0 ) {					
					
						continue;
						}

					if( n == Steps ) {
					
						continue;
						}
					}

				pGDI->FillRect(xPos, y1, xPos + 1, y2);
				}
			}
		}
	}

// Limits Access

C3REAL CPrimLegacyScatterGraph::GetMinX(void)
{
	CPrimLegacyGraphLine * pGraph = m_pLines[0];
	
	return pGraph->m_pLimits->GetMinX();
	}

C3REAL CPrimLegacyScatterGraph::GetMaxX(void)
{
	CPrimLegacyGraphLine * pGraph = m_pLines[0];
	
	return pGraph->m_pLimits->GetMaxX();
	}

C3REAL CPrimLegacyScatterGraph::GetMinY(void)
{
	CPrimLegacyGraphLine * pGraph = m_pLines[0];
	
	return pGraph->m_pLimits->GetMinY();
	}

C3REAL CPrimLegacyScatterGraph::GetMaxY(void)
{
	CPrimLegacyGraphLine * pGraph = m_pLines[0];
	
	return pGraph->m_pLimits->GetMaxY();
	}

// End of File
