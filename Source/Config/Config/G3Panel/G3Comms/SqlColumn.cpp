
#include "Intern.hpp"

#include "SqlColumn.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CodedItem.hpp"
#include "SqlQuery.hpp"
#include "SqlQueryManager.hpp"
#include "SqlRecord.hpp"
#include "SqlRecordList.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SQL Column Definition
//

// SQL Data Types
// Note -- the ordering here is important.

CSqlColumn::CSqlType CSqlColumn::m_Types[] = {	{ sqlBinary,   typeInteger, L"BIT"      },
						{ sqlChar,     typeString,  L"CHAR"     },
						{ sqlInt,      typeInteger, L"INT"      },
						{ sqlFloat,    typeReal,    L"FLOAT"    },
						{ sqlDatetime, typeInteger, L"DATETIME" },
						{ sqlVarChar,  typeString,  L"VARCHAR"  },
						{ sqlNVarChar, typeString,  L"NVARCHAR" }
						};

AfxImplementDynamicClass(CSqlColumn, CUIItem);

// Constructors

CSqlColumn::CSqlColumn(void)
{
	m_Type     = sqlInt;

	m_Rows     = 0;

	m_pRecords = New CSqlRecordList;

	m_Column   = NOTHING;
	}

CSqlColumn::CSqlColumn(CString Name)
{
	m_Type     = sqlInt;

	m_Rows     = 0;

	m_Name     = Name;

	m_pRecords = New CSqlRecordList;

	m_Column   = NOTHING;
	}

CSqlColumn::CSqlColumn(CString Name, UINT uCol, UINT uRows)
{
	m_Type     = sqlInt;

	m_Rows     = uRows;

	m_Name     = Name;

	m_SqlName  = Name;

	m_Column   = uCol;

	m_pRecords = New CSqlRecordList;
	}

// Download Support

BOOL CSqlColumn::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddLong(m_Type);

	Init.AddText(m_SqlName);

	Init.AddItem(itemSimple, m_pRecords);

	return TRUE;
	}

// Persistence

void CSqlColumn::Init(void)
{
	CUIItem::Init();

	for( UINT r = 0; r < m_Rows; r++ ) {

		CSqlRecord *pRecord = New CSqlRecord;

		m_pRecords->AppendItem(pRecord);
		}
	
	UpdateRecordType(typeInteger);
	}

// UI Update

void CSqlColumn::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == L"Type" ) {

		UINT uType = GetC3Type();		

		UpdateRecordType(uType);

		CSqlQueryManager *pManager = (CSqlQueryManager *) GetParent(AfxRuntimeClass(CSqlQueryManager));

		pManager->Validate();

		pHost->SendUpdate(updateContents);
		}

	if( Tag == L"SqlName" ) {

		CSqlQuery *pQuery = (CSqlQuery *) GetParent(AfxRuntimeClass(CSqlQuery));

		pQuery->UpdateSortColumn();

		pHost->SendUpdate(updateProps);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

void CSqlColumn::Validate(BOOL fExpand)
{
	for( UINT n = 0; n < m_pRecords->GetItemCount(); n++ ) {

		CSqlRecord *pRecord = m_pRecords->GetItem(n);

		pRecord->Validate(fExpand);
		}
	}

// Attributes

UINT CSqlColumn::GetC3Type(void)
{
	return m_Types[m_Type].m_uType;
	}

CSqlRecord * CSqlColumn::GetRecord(UINT uRow)
{
	if( uRow < m_pRecords->GetItemCount() ) {

		return m_pRecords->GetItem(uRow);
		}

	return NULL;
	}

BOOL CSqlColumn::CanClear(void)
{
	for( UINT n = 0; n < m_pRecords->GetItemCount(); n++ ) {

		CSqlRecord *pRecord = m_pRecords->GetItem(n);

		if( pRecord->IsMapped() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Meta Data Creation

void CSqlColumn::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString(Name);

	Meta_AddString(SqlName);

	Meta_AddInteger(Type);

	Meta_AddInteger(Column);

	Meta_AddCollect(Records);
	}

// Implementation

void CSqlColumn::UpdateRecordType(UINT uType)
{
	for( INDEX i = m_pRecords->GetHead(); !m_pRecords->Failed(i); m_pRecords->GetNext(i) ) {

		CSqlRecord *pRecord = m_pRecords->GetItem(i);

		CTypeDef Type;

		Type.m_Flags = flagTagRef | flagWritable;

		Type.m_Type  = uType;

		if( pRecord->m_pValue ) {

			pRecord->m_pValue->SetReqType(Type);
			}
		}
	}

void CSqlColumn::ClearAllMappings(void)
{
	for( INDEX i = m_pRecords->GetHead(); !m_pRecords->Failed(i); m_pRecords->GetNext(i) ) {

		CSqlRecord *pRecord = m_pRecords->GetItem(i);

		pRecord->ClearMapping();
		}
	}

BOOL CSqlColumn::Check(CStringArray &Errors)
{
	if( m_SqlName.IsEmpty() ) {

		CString Error;

		Error += GetHumanPath();

		Error += L": ";

		Error += IDS("Column name is empty");

		Error += L"\n";

		Error += GetFixedPath();

		Errors.Append(Error);

		return FALSE;
		}

	return TRUE;
	}

// End of File
