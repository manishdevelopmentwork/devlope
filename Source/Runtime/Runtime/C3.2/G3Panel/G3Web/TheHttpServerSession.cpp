
#include "Intern.hpp"

#include "TheHttpServerSession.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Enhanced Web Server
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Web Server Session
//

// Constants

static int const maxBuffers = 4;

// Constructor

CTheHttpServerSession::CTheHttpServerSession(CHttpServer *pServer, UINT cwRem, UINT uBits, UINT &nBuff) : CHttpServerSession(pServer), m_nBuff(nBuff)
{
	m_cwRem  = cwRem;

	m_uBits  = uBits;

	m_pBuff1 = NULL;

	m_pBuff2 = NULL;

	m_pWork  = NULL;

	m_dwLast = GetTickCount();
	}

// Destructor

CTheHttpServerSession::~CTheHttpServerSession(void)
{
	FreeData();
	}

// Operations

void CTheHttpServerSession::Throttle(UINT uLimit)
{
	DWORD dwTime = GetTickCount();

	DWORD dwGone = ToTime(dwTime - m_dwLast);

	if( dwGone < uLimit ) {

		Sleep(uLimit - dwGone);

		dwTime = GetTickCount();
		}

	m_dwLast = dwTime;
	}

BOOL CTheHttpServerSession::AllocBuffers(void)
{
	if( !m_pBuff1 ) {

		if( m_nBuff < maxBuffers ) {

			UINT nb  = m_uBits / 8;

			m_pBuff1 = PBYTE(memalign(16, m_cwRem * nb));

			m_pBuff2 = PBYTE(memalign(16, m_cwRem * nb));
		
			m_pWork  = PBYTE(memalign(16, m_cwRem * nb * 2 + 2048));
		
			memset(m_pBuff1, 0xAA, m_cwRem * nb);

			memset(m_pBuff2, 0x55, m_cwRem * nb);

			m_nBuff++;

			return TRUE;
			}

		return FALSE;
		}

	return TRUE;
	}

// Diagnostics

UINT CTheHttpServerSession::GetDiagColCount(void)
{
	return 6;
	}

void CTheHttpServerSession::GetDiagCols(IDiagOutput *pOut)
{
	CHttpServerSession::GetDiagCols(pOut);	

	pOut->SetColumn(5, "Buffers", "%s");
	}

void CTheHttpServerSession::GetDiagInfo(IDiagOutput *pOut)
{
	CHttpServerSession::GetDiagInfo(pOut);	

	pOut->SetData(5, m_pBuff1 ? "Yes" : "No");
	}

// Overridables

void CTheHttpServerSession::FreeData(void)
{
	if( m_pBuff1 ) {

		free(m_pBuff1);

		free(m_pBuff2);

		free(m_pWork);

		m_pBuff1 = NULL;

		m_pBuff2 = NULL;

		m_pWork  = NULL;

		m_nBuff--;
		}

	m_Console.Enable(FALSE);
	}

// End of File
