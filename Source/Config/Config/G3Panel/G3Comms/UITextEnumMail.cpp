
#include "Intern.hpp"

#include "UITextEnumMail.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "CommsSystem.hpp"
#include "CommsManager.hpp"
#include "MailAddress.hpp"
#include "MailContacts.hpp"
#include "MailManager.hpp"
#include "Services.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Mail Recipient
//

// Dynamic Class

AfxImplementDynamicClass(CUITextEnumMail, CUITextEnum);

// Constructor

CUITextEnumMail::CUITextEnumMail(void)
{
	m_uFlags = textEnum | textReload;

	m_pMail  = NULL;
	}

// Overridables

void CUITextEnumMail::OnBind(void)
{
	CUITextEnum::OnBind();

	FindMailManager();

	AddMailContacts();
	}

BOOL CUITextEnumMail::OnEnumValues(CStringArray &List)
{
	AddMailContacts();

	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_uMask & (1 << n) ) {

			List.Append(m_Enum[n].m_Text);
			}
		}

	return TRUE;
	}

// Implementation

void CUITextEnumMail::FindMailManager(void)
{
	CCommsSystem *pSystem = (CCommsSystem *) m_pItem->GetDatabase()->GetSystemItem();

	CItemIndexList *pList = pSystem->m_pComms->m_pServices->m_pList;

	for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

		CServiceItem *pServ = (CServiceItem *) pList->GetItem(n);

		if( pServ->IsKindOf(AfxRuntimeClass(CMailManager)) ) {

			m_pMail = (CMailManager *) pServ;

			break;
			}
		}
	}

void CUITextEnumMail::AddMailContacts(void)
{
	m_Enum.Empty();

	AddData(NOTHING, CString(IDS_NO_RECIPIENT));

	if( m_pMail ) {

		CItemIndexList *pList = m_pMail->m_pContacts->m_pList;

		for( INDEX n = pList->GetHead(); !pList->Failed(n); pList->GetNext(n) ) {

			CMailAddress *pAddr = (CMailAddress *) pList->GetItem(n);

			if( !pAddr->m_Name.IsEmpty() ) {

				AddData(pAddr->GetIndex(), pAddr->m_Name);
				}
			}
		}
	}

// End of File
