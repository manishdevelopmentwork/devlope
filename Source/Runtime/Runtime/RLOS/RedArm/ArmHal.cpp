
#include "Intern.hpp"

#include "ArmHal.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ARM Hardware Abstraction Layer
//

// Printf Control

extern int _fp_printf;

// Interrupt Code

clink BYTE _vect_start[];

// Entropy Hooks

extern void AccumulateEntropy(DWORD d);

// Data Members

IPic *			 CArmHal::m_pPic       = NULL;
CArmHal::CCtx * volatile CArmHal::m_pActiveCtx = NULL;
CArmHal::CCtx * volatile CArmHal::m_pFloatCtx  = NULL;
UINT			 CArmHal::m_SaveFpExc  = 0;
UINT			 CArmHal::m_uIndex     = 1;
PCSTR		volatile CArmHal::m_pFault     = NULL;
PVDWORD			 CArmHal::m_pEntropy   = NULL;

// Dagnostic Counters

UINT CArmHal::m_uTickTotal = 0;
UINT CArmHal::m_uTickIdle  = 0;
UINT CArmHal::m_uTickRun   = 0;
UINT CArmHal::m_uCtxSwitch = 0;
UINT CArmHal::m_uFltSwitch = 0;
UINT CArmHal::m_uIntSwi	   = 0;
UINT CArmHal::m_uIntIrq    = 0;
UINT CArmHal::m_uIntFiq    = 0;

// Constructor

CArmHal::CArmHal(void)
{
	StdSetRef();

	m_pDiag = NULL;
	}

// Destructor

CArmHal::~CArmHal(void)
{
	}

// Initialization

void CArmHal::Open(void)
{
	CBaseHal::Open();

	InstallInterrupts();
	}

// Debug Support

void CArmHal::DebugRegister(void)
{
	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	if( m_pDiag ) {

		m_uProv = m_pDiag->RegisterProvider(this, "hal");

		m_pDiag->RegisterCommand(m_uProv, 1, "counters");
		}

	m_pMmu->DebugRegister();

	m_pPic->DebugRegister();
	}

void CArmHal::DebugRevoke(void)
{
	if( m_pDiag ) {

		m_pDiag->RevokeProvider(m_uProv);

		m_pDiag->Release();

		m_pDiag = NULL;
		}

	m_pMmu->DebugRevoke();

	m_pPic->DebugRevoke();
	}

// Idle Mode

void CArmHal::WaitForInterrupt(void)
{
	HostIdle();
	}

// Interrupt Controller

bool CArmHal::SetLinePriority(UINT uLine, UINT uPriority)
{
	return m_pPic->SetLinePriority(uLine, uPriority);
	}

bool CArmHal::SetLineHandler(UINT uLine, IEventSink *pSink, UINT uParam)
{
	return m_pPic->SetLineHandler(uLine, pSink, uParam);
	}

bool CArmHal::EnableLine(UINT uLine, bool fEnable)
{
	return m_pPic->EnableLine(uLine, fEnable);
	}

bool CArmHal::EnableLineViaIrql(UINT uLine, UINT &uSave, bool fEnable)
{
	return m_pPic->EnableLineViaIrql(uLine, uSave, fEnable);
	}

// Task Context

void * CArmHal::CreateContext(DWORD Stack, DWORD Entry, DWORD Param)
{
	CCtx *pCtx = New CCtx;

	memset(pCtx, 0, sizeof(CCtx));

	pCtx->m_r0 = Param;

	pCtx->m_sp = Stack;

	pCtx->m_ep = Entry;

	pCtx->m_pc = Entry;

	pCtx->m_sr = ARM_MODE_INIT;

	pCtx->m_index = m_uIndex++;

	// Get a copy of the VFP control register without
	// forcing the floating point context switch...

	UINT ipr, exc;
			
	HostRaiseIpr(ipr);

	HostGetVfpExc(exc);

	HostSetVfpExc(VFP_ENABLE);

	HostGetVfpScr(pCtx->m_fpscr);

	HostSetVfpExc(exc);

	HostLowerIpr(ipr);

	return pCtx;
	}

void CArmHal::DeleteContext(void *pCtx)
{
	UINT ipr;
			
	HostRaiseIpr(ipr);

	if( m_pFloatCtx == pCtx ) {

		m_pFloatCtx = NULL;
		}

	HostLowerIpr(ipr);

	delete (CCtx *) pCtx;
	}

void CArmHal::SwitchContext(void *pSave, void *pLoad, DWORD Frame)
{
	CCtx * save = (CCtx *) pSave;

	CCtx * load = (CCtx *) pLoad;

	PDWORD data = PDWORD(Frame);

	DWORD  mode;

	HostGetThisSR(mode);

	// Set the current context.

	m_pActiveCtx = load;

	// Reset base spin count.

	m_uIdle = 0;

	// Exit low power if required.

	if( m_fIdle ) {

		LeaveLowPower();
		}

	// Perform the context switch.

	if( (mode & ARM_MODE_MASK) == ARM_MODE_FIQ ) {

		if( likely(save) ) {

			PDWORD s = data;
			
			PDWORD d = &save->m_sr;

			#if !defined(__INTELLISENSE__)

			ASM(	"ldmia	%0!, {r0-r4}	\n\t" // Load sr, r0, r1, r2, r3
				"stmia	%1!, {r0-r4}	\n\t" // Save sr, r0, r1, r2, r3
				"ldmia	%0!, {r0-r3}	\n\t" // Load r4, r5, r6, r7
				"stmia	%1!, {r0-r3}	\n\t" // Save r4, r5, r6, r7
				"stmia	%1 , {r8-r12}^	\n\t" // Save r8, r9, r10, r11, r12
				"add	%1 , #20	\n\t"
				"ldr	r0 , [%0, #24]	\n\t" // Load r15
				"stmia	%1!, {r0}	\n\t" // Save r15
				"stmia	%1 , {r13-r14}^	\n\t" // Save r13, r14
				: "+r"(s), "+r"(d)
				:
				: "r0", "r1", "r2", "r3", "r4"
				);

			#endif
			}

		PDWORD s = &load->m_sr;

		PDWORD d = data;

		#if !defined(__INTELLISENSE__)

		ASM(	"ldmia	%0!, {r0-r4}	\n\t" // Read sr, r0, r1, r2, r3
			"stmia	%1!, {r0-r4}	\n\t" // Load sr, r0, r1, r2, r3
			"ldmia	%0!, {r0-r3}	\n\t" // Read r4, r5, r6, r7
			"stmia	%1!, {r0-r3}	\n\t" // Load r4, r5, r6, r7
			"ldmia  %0 , {r8-r12}^	\n\t" // Load r8, r9, r10, r11, r12
			"add	%0 , #20	\n\t"
			"ldmia	%0!, {r0}	\n\t" // Read r15
			"str	r0 , [%1, #24]	\n\t" // Load r15
			"ldmia	%0 , {r13-r14}^	\n\t" // Load r13, r14
			: "+r"(s), "+r"(d)
			:
			: "r0", "r1", "r2", "r3", "r4"
			);

		#endif
		}
	else {
		if( likely(save) ) {

			PDWORD s = data;
			
			PDWORD d = &save->m_sr;

			#if !defined(__INTELLISENSE__)

			ASM(	"mrs	r0 , spsr	\n\t" // Load sr
				"ldmia	%0!, {r1-r7}	\n\t" // Load r0, r1, r2, r3, r4, r5, r6
				"stmia	%1!, {r0-r7}	\n\t" // Save sr, r0, r1, r2, r3, r4, r5, r6
				"ldmia	%0!, {r0-r6}	\n\t" // Load r7, r8, r9, r10, r11, r12, r13
				"stmia	%1!, {r0-r6}	\n\t" // Save r7, r8, r9, r10, r11, r12, r15
				"stmia	%1 , {r13-r14}^	\n\t" // Save r13, r14
				: "+r"(s), "+r"(d)
				:
				: "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7"
				);

			#endif
			}

		PDWORD s = &load->m_sr;

		PDWORD d = data;

		#if !defined(__INTELLISENSE__)

		ASM(	"ldmia	%0!, {r0-r7}	\n\t" // Read sr, r0, r1, r2, r3, r4, r5, r6
			"msr	spsr, r0	\n\t" // Load sr
			"stmia	%1!, {r1-r7}	\n\t" // Load r0, r1, r2, r3, r4, r5, r6
			"ldmia	%0!, {r0-r6}	\n\t" // Read r7, r8, r9, r10, r11, r12, r15
			"stmia	%1!, {r0-r6}	\n\t" // Load r7, r8, r9, r10, r11, r12, r15
			"ldmia	%0 , {r13-r14}^	\n\t" // Load r13, r14
			: "+r"(s), "+r"(d)
			:
			: "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7"
			);

		#endif
		}

	// Control printf version selection.

	_fp_printf = load->m_fpused;

	// Turn the VFP on or off as required.
		
	if( unlikely(m_pFloatCtx == load) ) {

		HostSetVfpExc(VFP_ENABLE);
		}
	else {
		if( !ALLOW_ISP_FP && likely(!load->m_fpforce) ) {

			// Turn off the VFP such that any floating point operations
			// will cause an INVALID exception that we can use to perform
			// a lazy context switch. This avoids lots of unnecesary work
			// for threads that don't use floating point operations.

			HostSetVfpExc(VFP_DISABLE);
			}
		else {
			// This branch would be taken if we implement some heuristic
			// to spot float-heavy threads where we just want to switch
			// right away and avoid the overhead of the exception handler.
	
			HostSetVfpExc(VFP_ENABLE);

			SwitchFloat();
			}
		}

	// Increment the count

	m_uCtxSwitch++;
	}

bool CArmHal::ContextUsesFp(void *pCtx)
{
	return ((CCtx *) pCtx)->m_fpused ? true : false;
	}

// Task Debug

DWORD CArmHal::GetActiveSp(void)
{
	DWORD sp = 0;

	HostGetUserSP(sp);

	return sp;
	}

DWORD CArmHal::GetSavedSp(void *pCtx)
{
	return ((CCtx *) pCtx)->m_sp;
	}

DWORD CArmHal::GetSavedPc(void *pCtx)
{
	return ((CCtx *) pCtx)->m_pc;
	}

void CArmHal::GetFuncName(char *pName, UINT uName, void *pCtx)
{
	CCtx *p = (CCtx *) pCtx;

	if( p->m_pc == p->m_ep ) {

		strcpy(pName, "EntryPoint");

		return;
		}

	ArmGetFuncName(pName, uName, p->m_pc, p->m_fp, 0);
	}

void CArmHal::StackTrace(IDiagOutput *pOut, void *pCtx)
{
	CCtx *p = (CCtx *) pCtx;

	if( p->m_pc == p->m_ep ) {

		return;
		}

	ArmStackTrace(pOut, p->m_pc, p->m_fp);
	}

// IUnknown

HRESULT CArmHal::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IDiagProvider);

	StdQueryInterface(IDiagProvider);

	return E_NOINTERFACE;
	}

ULONG CArmHal::AddRef(void)
{
	StdAddRef();
	}

ULONG CArmHal::Release(void)
{
	StdRelease();
	}

// IEventSink

void CArmHal::OnEvent(UINT uLine, UINT uParam)
{
	m_uTickTotal++;

	(m_fIdle ? m_uTickIdle : m_uTickRun)++;

	CBaseHal::OnEvent(uLine, uParam);
	}

// IDiagProvider

static UINT pc(UINT n, UINT d)
{
	return UINT((UINT64(n) * 100 + UINT64(d) / 2) / UINT64(d));
	}

static UINT tf(UINT n, UINT d)
{
	return UINT((UINT64(n) * 200 + UINT64(d) / 2) / UINT64(d));
	}

UINT CArmHal::RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd)
{
	if( !pCmd->GetArgCount() ) {

		pOut->AddPropList();

		// TODO -- We could get a more accurate low power
		// fraction if we accumulated time properly using
		// the tick fraction like we do in the executive.

		pOut->AddProp("Ticks Total",       "%u",   m_uTickTotal);
		pOut->AddProp("Ticks Low Power",   "%u",   m_uTickIdle);
		pOut->AddProp("Ticks Running",     "%u",   m_uTickRun);
		pOut->AddProp("Share Low Power",   "%u%%", pc(m_uTickIdle, m_uTickTotal));
		pOut->AddProp("Share Running",     "%u%%", pc(m_uTickRun, m_uTickTotal));
		pOut->AddProp("MPU Switches",      "%u",   m_uCtxSwitch);
		pOut->AddProp("VFP Switches",      "%u",   m_uFltSwitch);
		pOut->AddProp("MPU CS per Second", "%u",   tf(m_uCtxSwitch, m_uTickTotal));
		pOut->AddProp("VFP CS per Second", "%u",   tf(m_uFltSwitch, m_uTickTotal));
		pOut->AddProp("SWI Interrupts",    "%u",   m_uIntSwi);
		pOut->AddProp("IRQ Interrupts",    "%u",   m_uIntIrq);
		pOut->AddProp("FIQ Interrupts",    "%u",   m_uIntFiq);
		pOut->AddProp("SWI per Second",    "%u",   tf(m_uIntSwi, m_uTickTotal));
		pOut->AddProp("IRQ per Second",    "%u",   tf(m_uIntIrq, m_uTickTotal));
		pOut->AddProp("FIQ per Second",    "%u",   tf(m_uIntFiq, m_uTickTotal));

		pOut->EndPropList();

		m_uTickTotal = 0;
		m_uTickIdle  = 0;
		m_uTickRun   = 0;
		m_uCtxSwitch = 0;
		m_uFltSwitch = 0;
		m_uIntSwi    = 0;
		m_uIntIrq    = 0;
		m_uIntFiq    = 0;

		return 0;
		}

	pOut->Error(NULL);

	return 1;
	}

// Interrupt Handlers

void NAKED CArmHal::OnService(DWORD Frame, DWORD Opcode)
{
	FuncProlog();

	UINT mode;

	HostGetPrevSR(mode);

	if( likely(Opcode == 0) ) {

		m_IRQL = IRQL_DISPATCH;
			
		if( likely(m_fExec) ) {

			m_pExec->AccumThreadTime(false);
			}

		if( likely(m_pEntropy) ) {

			AccumulateEntropy(*m_pEntropy);
			}

		if( likely((mode & ARM_MODE_MASK) == ARM_MODE_INIT) ) {

			if( likely(m_pExec) ) {

				m_pExec->DispatchThread(Frame);
				}
			}
  
		m_IRQL = IRQL_TASK;
		}
	else {
		StartFault("TRAP");

		DWORD fp = PDWORD(Frame)[11];

		DWORD pc = PDWORD(Frame)[13] - 4;

		if( unlikely((mode & ARM_MODE_MASK) == ARM_MODE_FIQ ) ) {

			HostGetFiqFP(fp);
			}

		if( HIBYTE(Opcode) == 0x00 ) {

			Opcode += PANIC_APPLICATION;
			}

		_fp_printf = 0;

		Panic(Opcode, Frame, fp, pc, NULL);

		EndFault();
		}

	m_uIntSwi++;

	FuncEpilog();
	}

void NAKED CArmHal::OnUndef(DWORD Frame)
{
	FuncProlog();

	StartFault("UNDEF");

	DWORD fp = PDWORD(Frame)[11];

	DWORD pc = PDWORD(Frame)[13];
	
	DWORD op = 0;

	// Check for aligned program counter.

	if( likely(!(pc & 3)) ) {
		
		op = PDWORD(pc)[0];

		if( likely(m_pActiveCtx) ) {

			// Is this a floating point instruction?

			if( likely((op & 0x0E000000) == 0x0C000000 || (op & 0x0F000000) == 0x0E000000) ) {

				// Do not try to enable floating point instructions
				// in interrupt routines as these are not supported.

				if( m_IRQL < IRQL_HARDWARE ) {

					DWORD exc;

					HostGetVfpExc(exc);

					// Check if the VFP is enabled.

					if( likely(!(exc & VFP_ENABLE)) ) {

						// Enable the VFP.

						HostSetVfpExc(VFP_ENABLE);

						// Switch the VFP context.

						if( SwitchFloat() ) {

							// Enable this to see a thread's first VFP use.
							
							if( FALSE ) {

								UINT  irql = Hal_RaiseIrql(IRQL_DISPATCH);

								PCTXT ct   = GetContext();

								AfxTrace("\n*** FIRST VFP USE for %s is %8.8X\n\n", GetContext(), op);

								ArmStackTrace(NULL, pc, fp);

								AfxTrace("\n");

								Hal_LowerIrql(irql);
								}
							}

						// Retry the operation.

						goto x;
						}

					// The VFP is enabled so this isn't really a
					// coprocessor instruction. Handle as usual.
					}
				}
			}
		}

	Panic(PANIC_FAULT_UNDEFINED, Frame, fp, pc, NULL);

x:	EndFault();

	FuncEpilog();
	}

void NAKED CArmHal::OnAbort(DWORD Frame, BOOL fCode)
{
	FuncProlog();

	StartFault("ABORT");

	DWORD fp = PDWORD(Frame)[11];

	DWORD pc = PDWORD(Frame)[13] - 4;

	DWORD fs = 0;

	DWORD fa = 0;

	HostGetFsr(fs);

	HostGetFar(fa);

	// Is this an alignment fault?

	if( !fCode && (fs & 0x140F) == 0x0001 ) {

		DWORD sc = 0;

		HostGetSysCon(sc);

		// Is strict alignment enabled?

		if( sc & 0x0002 ) {

			// Do not try to fix alignment faults from
			// interrupts as these should never occur.

			if( m_IRQL < IRQL_HARDWARE ) {

				// Do not try to fix alignment faults from bad
				// instruction addresses as these will recurse.

				if( !(pc & 3) ) {

					// Do not try and fix any alignment faults that
					// occur outside a valid address range, as these
					// are typically bad reads and other fatals.

					if( phal->IsVirtualValid(fa, (fs & Bit(11)) ? true : false) ) {
	
						DWORD op = PDWORD(pc)[0];

						UINT  sd = 0;

						if( ArmFixAlign(Frame, op, fa, sd) ) {

							#if 1 || defined(_DEBUG)

							UINT  irql = Hal_RaiseIrql(IRQL_DISPATCH);

							PCTXT ct   = GetContext();

							AfxTrace( "\n*** FIX ALIGN in %s with sd=%u pc=%8.8X op=%8.8X fa=%8.8X\n\n",
								  ct,
								  sd,
								  pc,
								  op,
								  fa
								  );

							ArmStackTrace(NULL, pc, fp);

							AfxTrace("\n");

							Hal_LowerIrql(irql);

							#endif

							goto x;
							}
						}
					}
				}
			}
		else {
			if( (fa & 0xFF000000) == 0xEA000000 ) {

				Panic(PANIC_FAULT_DATA_ABORT, Frame, fp, pc, "ab=%s", "NULL-VIRTUAL");

				goto x;
				}
			}
		}

	if( TRUE ) {

		PCTXT pHex    = "0123456789ABCDEF";

		char  sHex[6] = "0000";

		PCTXT at      = sHex;

		sHex[0] = pHex[(fs>>12)&15];
		sHex[1] = pHex[(fs>> 8)&15];
		sHex[2] = pHex[(fs>> 4)&15];
		sHex[3] = pHex[(fs>> 0)&15];

		if( !fCode ) {

			switch( fs & 0x140F ) {

				case 0x0001: at = "ALIGNMENT";		break;
				case 0x0004: at = "ICACHE-FAULT";	break;
				case 0x000C: at = "L1-PRECISE";		break;
				case 0x100C: at = "L1-PRECISE";		break;
				case 0x000E: at = "L2-PRECISE";		break;
				case 0x100E: at = "L2-PRECISE";		break;
				case 0x040C: at = "L1-PARITY";		break;
				case 0x040E: at = "L2-PARITY";		break;
				case 0x0005: at = "SECT-TRANS";		break;
				case 0x0007: at = "PAGE-TRANS";		break;
				case 0x0003: at = "SECT-ACCESS";	break;
				case 0x0006: at = "PAGE-ACCESS";	break;
				case 0x0009: at = "SECT-DOMAIN";	break;
				case 0x000B: at = "PAGE-DOMAIN";	break;
				case 0x000D: at = "SECT-PERM";		break;
				case 0x000F: at = "PAGE-PERM";		break;
				case 0x0008: at = "PRECISE";		break;
				case 0x1008: at = "PRECISE";		break;
				case 0x0406: at = "IMPRECISE";		break;
				case 0x1406: at = "IMPRECISE";		break;
				case 0x0408: at = "PARITY";		break;
				case 0x0002: at = "DEBUG";		break;
				}

			Panic(PANIC_FAULT_DATA_ABORT, Frame, fp, pc, "ab=%s fa=%8.8X", at, fa);
			}
		else {
			switch( fs & 0x140F ) {

				case 0x000C: at = "L1-PRECISE";		break;
				case 0x100C: at = "L1-PRECISE";		break;
				case 0x000E: at = "L2-PRECISE";		break;
				case 0x100E: at = "L2-PRECISE";		break;
				case 0x040C: at = "L1-PARITY";		break;
				case 0x040E: at = "L2-PARITY";		break;
				case 0x0005: at = "SECT-TRANS";		break;
				case 0x0007: at = "PAGE-TRANS";		break;
				case 0x0003: at = "SECT-ACCESS";	break;
				case 0x0006: at = "PAGE-ACCESS";	break;
				case 0x0009: at = "SECT-DOMAIN";	break;
				case 0x000B: at = "PAGE-DOMAIN";	break;
				case 0x000D: at = "SECT-PERM";		break;
				case 0x000F: at = "PAGE-PERM";		break;
				case 0x0008: at = "PRECISE";		break;
				case 0x1008: at = "PRECISE";		break;
				case 0x0409: at = "PARITY";		break;
				case 0x0002: at = "DEBUG";		break;
				}

			Panic(PANIC_FAULT_INST_ABORT, Frame, fp, pc, "ab=%s", at);
			}
		}

x:	EndFault();

	FuncEpilog();
	}

void NAKED CArmHal::OnIrq(DWORD Frame)
{
	FuncProlog();

	UINT Prev;
	
	if( likely((Prev = m_IRQL) < IRQL_HARDWARE) ) {

		if( likely(m_fExec) ) {

			m_pExec->AccumThreadTime(true);
			}

		if( !ALLOW_ISP_FP ) {

			// Disable floating point in interrupts so we'll
			// get an exception if anyone tries to use it...

			HostGetVfpExc(m_SaveFpExc);

			HostSetVfpExc(VFP_DISABLE);

			_fp_printf = 0;
			}
		}

	m_IRQL = IRQL_HARDWARE + 1;

	m_pPic->OnInterrupt(0);

	m_uIntIrq++;

	LeaveIsp(Prev, Frame);

	FuncEpilog();
	}

void NAKED CArmHal::OnFiq(DWORD Frame)
{
	FuncProlog();

	UINT Prev;
	
	if( likely((Prev = m_IRQL) < IRQL_HARDWARE) ) {

		if( likely(m_fExec) ) {

			m_pExec->AccumThreadTime(false);
			}

		if( !ALLOW_ISP_FP ) {

			// Disable floating point in interrupts so we'll
			// get an exception if anyone tries to use it...

			HostGetVfpExc(m_SaveFpExc);

			HostSetVfpExc(VFP_DISABLE);

			_fp_printf = 0;
			}
		}

	if( likely(m_pEntropy) ) {

		AccumulateEntropy(*m_pEntropy);
		}

	m_IRQL = IRQL_HARDWARE + 2;

	m_pPic->OnInterrupt(1);

	m_uIntFiq++;

	LeaveIsp(Prev, Frame);

	FuncEpilog();
	}

// Implementation

void CArmHal::InstallInterrupts(void)
{
	memcpy(0, _vect_start, 1024);

	ProcSyncCaches();

	PDWORD pv = PDWORD(0x20);

	pv[0] = DWORD(&OnUndef);
	pv[1] = DWORD(&OnService);
	pv[2] = DWORD(&OnAbort);
	pv[3] = DWORD(&OnIrq);
	pv[4] = DWORD(&OnFiq);
	}

bool CArmHal::SwitchFloat(void)
{
	// Do we have a previous VFP user?

	if( likely(m_pFloatCtx) ) {

		// Optional diagnostic for switching.

		if( false ) {

			phal->DebugOut('0' + m_pFloatCtx->m_index / 10);
			phal->DebugOut('0' + m_pFloatCtx->m_index % 10);
			phal->DebugOut('>');
			phal->DebugOut('0' + m_pActiveCtx->m_index / 10);
			phal->DebugOut('0' + m_pActiveCtx->m_index % 10);
			phal->DebugOut(' ');
			}

		// Save floating point context.

		PVOID d = &m_pFloatCtx->m_d0;

		#if !defined(__INTELLISENSE__)

		ASM(	"fstmiad %0!, {d0-d15}	\n\t"
			"fstmiad %0!, {d16-d31}	\n\t"
			"fmrx    r0,  fpscr	\n\t"
			"stmia   %0!, {r0}	\n\t"
			: "+r"(d)
			:
			: "r0"
			);

		#endif
		}

	// Do we have a new VFP user?

	if( likely(m_pFloatCtx = m_pActiveCtx) ) {

		// Load floating point context.

		PVOID s = &m_pFloatCtx->m_d0;

		#if !defined(__INTELLISENSE__)

		ASM(	"fldmiad %0!, {d0-d15}	\n\t"
			"fldmiad %0!, {d16-d31}	\n\t"
			"ldmia   %0!, {r0}	\n\t"
			"fmxr    fpscr, r0	\n\t"
			: "+r"(s)
			:
			: "r0"
			);

		#endif

		// Indicate floating point used.

		if( !m_pFloatCtx->m_fpused ) {

			m_pFloatCtx->m_fpused = 1;

			_fp_printf = 1;

			return true;
			}
		}

	// Increment the count

	m_uFltSwitch++;

	return false;
	}

void CArmHal::Panic(UINT Code)
{
	DWORD fp, pc;

	#if !defined(__INTELLISENSE__)

	ASM (	"mov %0, fp"
		: "=r"(fp)
		);

	ASM (	"mov %0, pc"
		: "=r"(pc)
		);
	
	#endif

	Panic(Code, 0, fp, pc, NULL);
	}

void CArmHal::Panic(UINT Code, DWORD Frame, DWORD fp, DWORD pc, PCTXT pInfo, ...)
{
	#if 1 || defined(_DEBUG)

	// Print the panic information.
		
	UINT irql = Hal_RaiseIrql(IRQL_DISPATCH);

	phal->DebugReset();

	bool  fe = ShowPanicCode(Code);

	DWORD ct = DWORD(GetContext());

	if( !(ct & 3) ) {

		if( phal->IsVirtualValid(ct, false) ) {

			AfxTrace(" in %s", PCTXT(ct));
			}
		}

	if( TRUE ) {

		AfxTrace(" with fp=%8.8X pc=%8.8X", fp, pc);
		}

	if( !(pc & 3) ) {

		if( phal->IsVirtualValid(pc, false) ) {

			DWORD op = *PDWORD(pc);

			AfxTrace(" op=%8.8X", op);
			}
		}

	if( pInfo ) {

		va_list pArgs;

		va_start(pArgs, pInfo);

		AfxPrint(" ");

		AfxTraceArgs(pInfo, pArgs);

		va_end(pArgs);
		}

	if( TRUE ) { 

		AfxPrint("\n\n");

		ArmStackTrace(NULL, pc, fp);

		AfxPrint("\n");
		}

	if( !fe && m_fExec && Frame && irql <= IRQL_DISPATCH ) {

		AfxPrint("*** THREAD SUSPENDED\n\n");

		m_pExec->SuspendThread();

		m_pExec->DispatchThread(Frame);

		Hal_LowerIrql(IRQL_TASK);

		return;
		}

	#endif

	BuildCrashRecord(Code, fp, pc);

	phal->DebugStop();
	}
	
void CArmHal::BuildCrashRecord(UINT Code, DWORD fp, DWORD pc)
{
	struct CCrash
	{
		DWORD	Time;
		WORD	Code;
		WORD	thread;
		DWORD	fp;
		DWORD	pc;
		BYTE	stack[80];
		};

	CCrash cr = { 0 };

	// TODO -- We need a safe source of the time, and the
	// thread index ought to be NOTHING for interrupts. We
	// also need to write this to somewhere safe but with
	// code that does not need interrupts enabled and that
	// does not need to allocate memory. !!!

	cr.Time   = NOTHING;
	cr.Code   = WORD(Code);
	cr.thread = WORD(GetThreadIndex());
	cr.fp     = fp;
	cr.pc     = pc;

	PBYTE p = cr.stack;

	ArmStackTrace(p, pc, fp);
	}

void CArmHal::DoubleFault(PCTXT pName)
{
	// TODO -- Should it be a triple fault? Undefined floating
	// point opcodes ought to be allowed in service handlers!!!

	HostMaxIpr();

	phal->DebugKick();
	
	BuildCrashRecord(PANIC_FAULT_DOUBLE, 0, 0);

	PCSTR p1 = "\n*** DOUBLE FAULT (";
	PCSTR p2 = m_pFault;
	PCSTR p3 = "+";
	PCSTR p4 = pName;
	PCSTR p5 = ")\n\n";

	while( *p1 ) phal->DebugOut(*p1++);
	while( *p2 ) phal->DebugOut(*p2++);
	while( *p3 ) phal->DebugOut(*p3++);
	while( *p4 ) phal->DebugOut(*p4++);
	while( *p5 ) phal->DebugOut(*p5++);

	phal->DebugStop();
	}

// IPL Helper

STRONG_INLINE void CArmHal::SyncIpl(void)
{
	UINT ipr = 0;

	if( m_IRQL == IRQL_HARDWARE + 1 ) {

		ipr = 128;
		}

	if( m_IRQL == IRQL_HARDWARE + 2 ) {

		ipr = 192;
		}

	HostSetIpr(ipr);
	}

// Friends

UINT Hal_RaiseIrql(UINT IRQL)
{
	if( likely(IRQL > CArmHal::m_IRQL) ) {

		UINT irql;
		
		irql   = CArmHal::m_IRQL;

		CArmHal::m_IRQL = IRQL;

		CArmHal::SyncIpl();

		return irql;
		}

	return CBaseHal::m_IRQL;
	}

bool Hal_LowerIrql(UINT IRQL)
{
	if( likely(IRQL < CBaseHal::m_IRQL) ) {

		if( likely(IRQL == IRQL_TASK) ) {

			UINT ipr;
			
			HostRaiseIpr(ipr);

			if( likely(CArmHal::m_pExec) ) {
				
				if( unlikely(CArmHal::m_pExec->HasPendingDispatch()) ) {

					HostLowerIpr(ipr);

					HostTrap(0);

					CArmHal::SyncIpl();

					return true;
					}
				}
			}

		CBaseHal::m_IRQL = IRQL;

		CArmHal::SyncIpl();

		return true;
		}

	return false;
	}

// End of File
