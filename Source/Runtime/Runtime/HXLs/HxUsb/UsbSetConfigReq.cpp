
#include "Intern.hpp"

#include "UsbSetConfigReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Set Config Standard Device Request
//

// Constructor

CUsbSetConfigReq::CUsbSetConfigReq(WORD wConfig)
{
	Init(wConfig);
	} 

// Operations

void CUsbSetConfigReq::Init(WORD wConfig)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqSetConfig;

	m_Direction = dirHostToDev;

	m_Recipient = recDevice;

	m_wValue    = wConfig;
	}

// End of File
