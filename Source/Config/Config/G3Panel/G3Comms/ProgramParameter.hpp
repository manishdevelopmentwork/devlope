
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_ProgramParameter_HPP

#define INCLUDE_ProgramParameter_HPP

//////////////////////////////////////////////////////////////////////////
//
// Program Parameter Entry
//

class DLLNOT CProgramParameter : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CProgramParameter(void);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Item Properties
		UINT	m_Type;
		CString m_Name;

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void    DoEnables(IUIHost *pHost);
	};

// End of File

#endif
