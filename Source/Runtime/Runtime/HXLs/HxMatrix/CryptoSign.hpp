
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoSign_HPP

#define INCLUDE_CryptoSign_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoEncode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cryptographic Signer
//

class CCryptoSign : public CCryptoEncode, public ICryptoSign
{
	public:
		// Constructor
		CCryptoSign(void);

		// Destructor
		virtual ~CCryptoSign(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICryptoSign
		UINT    GetSigSize(void);
		PCBYTE  GetSigData(void);
		BOOL    GetSigData(CByteArray &Data);
		CString GetSigString(UINT Code);
		BOOL    Initialize(PCBYTE pKey, UINT uKey, CString const &Pass);
		BOOL    Initialize(CByteArray const &Key, CString const &Pass);
		void    SetHash(ICryptoHash *pHash);
		BOOL    SetHash(PCSTR pName);
		void    Update(PCBYTE pData, UINT uData);
		void    Update(CByteArray const &Data);
		void    Update(CString const &Data);

	protected:
		// States
		enum
		{
			stateNew,
			stateActive,
			stateDone
			};

		// Data Members
		ULONG	      m_uRefs;
		UINT	      m_uState;
		ICryptoHash * m_pHash;
		PBYTE	      m_pSig;
		UINT	      m_uSig;
	};

// End of File

#endif
