
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_OptionCardRackItem_HPP

#define INCLUDE_OptionCardRackItem_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsPortList;
class COptionCardItem;
class COptionCardList;
class COptionCardRackList;
union CUsbTreePath;

//////////////////////////////////////////////////////////////////////////
//
// Option Card Rack Item
//

class DLLAPI COptionCardRackItem : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		COptionCardRackItem(void);
		COptionCardRackItem(UINT uSlot);

		// Attributes
		UINT    GetTreeImage(void) const;
		CString GetTreeLabel(void) const;
		UINT    GetSlotCount(void) const;
		UINT    GetRackCount(void) const;
		BOOL    HasType(UINT uType) const;

		// Operations
		BOOL AppendItem(COptionCardItem *pItem);
		BOOL AppendItem(COptionCardRackItem *pItem);
		BOOL DeleteItem(COptionCardItem *pItem);
		BOOL DeleteItem(COptionCardRackItem *pItem);
		void Validate(BOOL fExpand);
		void ClearSysBlocks(void);
		void NotifyInit(void);
		void CheckMapBlocks(void);
	virtual	UINT GetPowerBudget(void);
	virtual	UINT GetPowerTotal(void);
	virtual	UINT GetPowerUsage(void);
	virtual	BOOL CheckPower(void);

		// UI Management
		CViewWnd * CreateView(UINT uType);

		// Ports List Access
		BOOL GetPortList(CCommsPortList * &pList, UINT uIndex) const;

		// Slot
		COptionCardItem * FindSlot(CUsbTreePath const &Slot) const;
		COptionCardItem * FindSlot(UINT Slot) const;

		// Rack
		COptionCardRackItem * FindRack(CUsbTreePath const &Slot) const;
		COptionCardRackItem * FindRack(UINT Slot) const;

		// Firmware
		void GetFirmwareList(CArray<UINT> &List) const;

		// Item Naming
		CString GetHumanName(void) const;

		// Conversion
		void PostConvert(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Item Properties
		UINT                  m_Slot;
		UINT                  m_Number;
		COptionCardList     * m_pSlots;
		COptionCardRackList * m_pRacks;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
