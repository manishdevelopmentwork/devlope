
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlQueryManagerTreeWnd_HPP

#define INCLUDE_SqlQueryManagerTreeWnd_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CCommsMapData;

class CSqlQueryManager;

//////////////////////////////////////////////////////////////////////////
//
// Sql Query Navigation Window
//

class CSqlQueryManagerTreeWnd : public CStdTreeWnd, public IUpdate
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlQueryManagerTreeWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IUpdate
		HRESULT METHOD ItemUpdated(CItem *pItem, UINT uType);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Editing Actions
		class CCmdCreate;
		class CCmdDelete;
		class CCmdRename;
		class CCmdPaste;

	protected:
		// Drag Codes
		enum
		{
			dragNone,
			dragMapping,
			};

		// Drop Codes
		enum
		{
			dropNone,
			dropPartial,
			dropFragment,
			dropData,
			dropOther
			};

		// Data Members
		CSqlQueryManager * m_pManager;
		CNameMap           m_MapFixed;
		UINT               m_uLocked;
		UINT		   m_cfCode;
		BOOL               m_fRefresh;
		BOOL               m_fLock;

		// Static Data
		static UINT m_timerDouble;
		static UINT m_timerSelect;
		static UINT m_timerScroll;

		// Drag Context
		UINT		  m_uDrag;

		// Drop Context
		UINT		  m_uDrop;
		BOOL		  m_fDropLocal;
		BOOL		  m_fDropMove;
		CLASS             m_DropClass;
		HTREEITEM	  m_hDropRoot;
		HTREEITEM	  m_hDropPrev;

		// Overridables
		void    OnAttach(void);
		CString OnGetNavPos(void);
		BOOL    OnNavigate(CString const &Nav);
		void    OnExec(CCmd *pCmd);
		void    OnUndo(CCmd *pCmd);

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnLoadTool(UINT uCode, CMenu &Menu);
		void OnShowWindow(BOOL fShow, UINT uStatus);
		void OnTimer(UINT uID, TIMERPROC *pfnProc);

		// Notification Handlers
		BOOL OnTreeBeginEdit(UINT uID, NMTVDISPINFO &Info);
		BOOL OnTreeEndEdit(UINT uID, NMTVDISPINFO &Info);

		// Command Handlers
		BOOL OnItemGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnItemControl(UINT uID, CCmdSource &Src);
		BOOL OnItemCommand(UINT uID);
		BOOL OnGoControl(UINT uID, CCmdSource &Src);
		BOOL OnGoCommand(UINT uID);
		BOOL OnEditControl(UINT uID, CCmdSource &Src);
		BOOL OnEditCommand(UINT uID);
		BOOL OnSqlGetInfo(UINT uID, CCmdInfo &Info);
		BOOL OnSqlControl(UINT uID, CCmdSource &Src);
		BOOL OnSqlCommand(UINT uID);

		// Go Menu
		BOOL OnGoNext(BOOL fSiblings);
		BOOL OnGoPrev(BOOL fSiblings);
		BOOL StepAway(void);

		// SQL Menu
		BOOL CanSqlAddQuery(void);
		BOOL CanSqlAddColumn(void);
		BOOL CanSqlClear(void);
		BOOL CanSqlClearCol(void);
		void OnSqlAddQuery(void);
		void OnSqlAddColumn(void);
		void OnSqlClear(void);
		void OnSqlClearCol(void);

		// Item Menu: Create
		void OnCreateItem(CMetaItem *pItem);
		void OnExecCreate(CCmdCreate *pCmd);
		void OnUndoCreate(CCmdCreate *pCmd);

		// Item Menu : Delete
		BOOL CanItemDelete(void);
		void OnItemDelete(UINT uVerb, BOOL fWarn, BOOL fTop);
		void OnExecDelete(CCmdDelete *pCmd);
		void OnUndoDelete(CCmdDelete *pCmd);

		// Item Menu : Rename
		BOOL CanItemRename(void);
		BOOL OnItemRename(CString Name);
		void OnExecRename(CCmdRename *pCmd);
		void OnUndoRename(CCmdRename *pCmd);
		void RenameItem(CString Name, CString Prev);
		BOOL CheckItemName(CString const &Name);

		// Edit Menu : Paste
		BOOL CanEditPaste(void);
		void OnEditPaste(void);

		// Data Object Acceptance
		BOOL CanAcceptDataObject(IDataObject *pData, UINT &uType);
		BOOL CanAcceptCodeFragment(IDataObject *pData);
		BOOL CanAcceptPartial(IDataObject *pData);
		BOOL AcceptCodeFragment(CString &Text, IDataObject *pData);
		BOOL AcceptPartial(CCommsMapData const * &pMapData, IDataObject *pData);

		// Tree Loading
		void	  LoadImageList(void);
		void      LoadTree(void);
		void      LoadRoot(void);
		HTREEITEM LoadItem(HTREEITEM hRoot, HTREEITEM hNext, CMetaItem *pItem);
		void      LoadNodeItem(CTreeViewItem &Node, CMetaItem *pItem);
		void      LoadNodeKids(HTREEITEM hRoot, CMetaItem *pItem);
		void      LoadList(HTREEITEM hRoot, CItemList *pList);
		HTREEITEM RefreshFrom(HTREEITEM hItem);

		// Item Mapping
		void AddToMap(CMetaItem *pItem, HTREEITEM hItem);
		void RemoveFromMap(CMetaItem *pItem);

		// Item Hooks
		void NewItemSelected(void);
		UINT GetRootImage(void);
		UINT GetItemImage(CMetaItem *pItem);
		BOOL GetItemMenu(CString &Name, BOOL fHit);
		void KillItem(HTREEITEM hItem, BOOL fRemove);

		// Drag Hooks
		DWORD FindDragAllowed(void);

		// Drop Support
		void DropTrack(CPoint Pos, BOOL fMove);
		BOOL DropDone(IDataObject *pData);
		void DropDebug(void);
		void ShowDrop(BOOL fShow);
		BOOL ShowDropVert(HTREEITEM hItem);
		BOOL IsDropMove(DWORD dwKeys, DWORD *pEffect);
		BOOL IsValidDrop(void);
		BOOL IsValidMove(void);
		BOOL IsExpandingDrop(void);
		BOOL IsFullItemDrop(void);

		// Item Data
		CString GetItemList(CMetaItem *pItem);

		// Implementation
		void LockUpdate(BOOL fLock);
		void SkipUpdate(void);
		void ListUpdated(HTREEITEM hItem);
		void WalkToLast(HTREEITEM &hItem);
		BOOL MapRecord(CString const &Text, BOOL fStep);
		void MarkMulti(CString Item, CString Menu, UINT uCode);
	};

// End of File

#endif
