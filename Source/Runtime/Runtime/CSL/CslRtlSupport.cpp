
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for RTL Support
//

// Externals

clink struct _reent * (*_thread_impure_ptr_thunk)(void);

clink struct _reent * (*_global_impure_ptr_thunk)(void);

// Static Data

static IRtlSupport * m_pRtlSupport = NULL;

// Code

global void Bind_RtlSupport(void)
{
	AfxGetObject("rtlsupport", 0, IRtlSupport, m_pRtlSupport);

	m_pRtlSupport->GetThreadImpurePtrThunk(&_thread_impure_ptr_thunk);

	m_pRtlSupport->GetGlobalImpurePtrThunk(&_global_impure_ptr_thunk);
	}

global void Free_RtlSupport(void)
{
	m_pRtlSupport->Release();

	m_pRtlSupport = NULL;
	}

global UINT VSPrintf(PTXT pBuff, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->VSNPrintf(pBuff, NOTHING, pText, pArgs);
	}

global UINT VSNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->VSNPrintf(pBuff, uLimit, pText, pArgs);
	}

global UINT SPrintf(PTXT pBuff, PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	UINT n = m_pRtlSupport->VSNPrintf(pBuff, NOTHING, pText, pArgs);

	va_end(pArgs);

	return n;
	}

global UINT SNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, ...)
{
	va_list pArgs;

	va_start(pArgs, pText);

	UINT n = m_pRtlSupport->VSNPrintf(pBuff, uLimit, pText, pArgs);

	va_end(pArgs);

	return n;
	}

clink int _snprintf(char *pBuffer, UINT uLimit, char const *pFormat, ...)
{
	va_list pArgs;

	va_start(pArgs, pFormat);

	return VSNPrintf(pBuffer, uLimit, pFormat, pArgs);
	}

clink int _vfprintf_r(struct _reent *, FILE *pFile, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->VFPrintf(pFile, pText, pArgs);
	}

clink int _svfprintf_r(struct _reent *, FILE *pFile, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->SVFPrintf(pFile, pText, pArgs);
	}

clink int _vfiprintf_r(struct _reent *, FILE *pFile, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->VFIPrintf(pFile, pText, pArgs);
	}

clink int _svfiprintf_r(struct _reent *, FILE *pFile, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->SVFIPrintf(pFile, pText, pArgs);
	}

clink int __ssvfscanf_r(struct _reent *, FILE *pFile, PCTXT pText, va_list pArgs)
{
	return m_pRtlSupport->SSVFScanf(pFile, pText, pArgs);
	}

clink int gettimeofday(struct timeval *pt, void const *tz)
{
	return m_pRtlSupport->GetTimeOfDay(pt, tz);
	}

clink int settimeofday(struct timeval const *pt, void const *tz)
{
	return m_pRtlSupport->SetTimeOfDay(pt, tz);
	}

clink time_t getmonosecs(void)
{
	return m_pRtlSupport->GetMonoSecs();
}

// End of File
