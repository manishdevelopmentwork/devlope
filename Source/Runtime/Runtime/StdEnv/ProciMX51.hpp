
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ProciMX51_HPP

#define INCLUDE_ProciMX51_HPP

//////////////////////////////////////////////////////////////////////////
//
// Processor Flags
//

#define AEON_PROC_iMX51

#define AEON_PROC_Cortex

// End of File

#endif
