
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_COUCH_HPP
	
#define	INCLUDE_COUCH_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimTouchCalib;
class CPrimTouchTester;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Calibration
//

class CPrimTouchCalib : public CPrim
{
	public:
		// Constructor
		CPrimTouchCalib(void);

		// Destructor
		~CPrimTouchCalib(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Config Data
		BOOL	     m_Factory;
		BOOL         m_Clear;
		CCodedItem * m_pSuccess;
		CCodedItem * m_pFailure;

	protected:
		// Data Members
		PCTXT m_pText;
		int   m_nCount;
		int   m_nSelect;
		int   m_nLast;
		int   m_xRaw[9];
		int   m_yRaw[9];
		UINT  m_uTimer;
		UINT  m_uEvent;

		// Implementation
		int  GetXPos(int nPos);
		int  GetYPos(int nPos);
		BOOL DoCalc(void);
		void Swap(int &a, int &b);
		int  MulDivRound(int a, int b, int c);
		int  MulDiv(int a, int b, int c);
		void GetTouchSize(int &cx, int &cy);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Touch Tester
//

class CPrimTouchTester : public CPrim
{
	public:
		// Constructor
		CPrimTouchTester(void);

		// Destructor
		~CPrimTouchTester(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void DrawPrim(IGDI *pGDI);
		void LoadTouchMap(IGDI *pGDI, ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CPrimColor * m_pBack;

	protected:
		// Data Members
		UINT m_uLast;
		UINT m_uHead;
		UINT m_uTail;
		int  m_xPos[10];
		int  m_yPos[10];
	};

// End of File

#endif
