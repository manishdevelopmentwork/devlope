
#include "Intern.hpp"

#include "OpcClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Client
//

// Instantiator

#if defined(AEON_ENVIRONMENT)

IUnknown * Create_OpcUaClient(PCTXT pName)
{
	return New COpcClient;
	}

#else

IOpcUaClient * Create_OpcUaClient(void)
{
	return New COpcClient;
	}

#endif

// Registration

#if defined(AEON_ENVIRONMENT)

global void Register_OpcUaClient(void)
{
	piob->RegisterInstantiator("opcua.opcua-client", Create_OpcUaClient);
	}

global void Revoke_OpcUaClient(void)
{
	piob->RevokeInstantiator("opcua.opcua-client");
	}

#endif

// Constructor

COpcClient::COpcClient(void)
{
	StdSetRef();

	InitSecurity();
	}

// Destructor

COpcClient::~COpcClient(void)
{
	}

// IUnknown

#if defined(AEON_ENVIRONMENT)

HRESULT COpcClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IOpcUaClient);

	StdQueryInterface(IOpcUaClient);

	return E_NOINTERFACE;
	}

ULONG COpcClient::AddRef(void)
{
	StdAddRef();
	}

ULONG COpcClient::Release(void)
{
	StdRelease();
	}

#endif

// IOpcUaClient

void COpcClient::OpcFree(void)
{
	delete this;
	}

bool COpcClient::OpcInit(void)
{
	BaseInit();

	return true;
	}

bool COpcClient::OpcStop(void)
{
	BaseStop();

	return true;
	}

bool COpcClient::OpcTerm(void)
{
	while( !m_Devices.IsEmpty() ) {

		CloseDevice(m_Devices.GetHead());
		}

	BaseTerm();

	return true;
	}

UINT COpcClient::OpcOpenDevice(PCTXT pHost, PCTXT pUser, PCTXT pPass, CStringArray const &Nodes)
{
	INDEX Index = OpenDevice(pHost, pUser, pPass);

	if( Index ) {

		CDevice *pDevice = m_Devices[Index];

		for( UINT n = 0; n < Nodes.GetCount(); n++ ) {

			COpcNodeId Node;

			if( Node.Decode(Nodes[n]) ) {

				pDevice->m_Nodes.Append(Node);
				
				pDevice->m_Types.Append(0);
				}
			else {
				pDevice->m_Nodes.Append(COpcNodeId(0, 0));

				pDevice->m_Types.Append(0);
				}
			}

		return UINT(Index);
		}

	return 0;
	}

bool COpcClient::OpcCloseDevice(UINT uDevice)
{
	if( uDevice ) {

		CloseDevice(INDEX(uDevice));

		return true;
		}

	return false;
	}

bool COpcClient::OpcPingDevice(UINT uDevice)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for(;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				return true;
				}

			if( !fBusy ) {

				Sleep(250);

				return false;
				}
			}
		}

	return false;
	}

bool COpcClient::OpcBrowseDevice(UINT uDevice, CStringArray &List)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for(;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				if( Browse(pDevice, List) ) {

					List.Sort();
	
					return true;
					}

				return false;
				}

			if( !fBusy ) {

				Sleep(250);

				return false;
				}
			}
		}

	return false;
	}

bool COpcClient::OpcReadData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PDWORD pData)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		for(;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				return Read(pDevice, pList, uList, pType, pData);
				}

			if( !fBusy ) {

				Sleep(250);

				return false;
				}
			}
		}

	return false;
	}

bool COpcClient::OpcWriteData(UINT uDevice, PCUINT pList, UINT uList, PCUINT pType, PCDWORD pData)
{
	if( uDevice ) {

		CDevice *pDevice = m_Devices[INDEX(uDevice)];

		BOOL     fRead   = FALSE;

		for(;;) {

			BOOL fBusy = FALSE;

			if( CheckDevice(pDevice, fBusy) ) {

				if( !HasTypes(pDevice, pList, uList) ) {

					if( !fRead ) {

						PUINT  pReadType = New UINT  [ uList ];

						PDWORD pReadData = New DWORD [ uList ];

						bool   fResult   = Read(pDevice, pList, uList, pReadType, pReadData);

						delete [] pReadType;

						delete [] pReadData;

						if( fResult ) {

							fRead = TRUE;

							continue;
							}
						}

					return false;
					}

				return Write(pDevice, pList, uList, pType, pData);
				}

			if( !fBusy ) {

				return false;
				}
			}
		}

	return false;
	}

// Implementation

void COpcClient::InitSecurity(void)
{
	m_Certificate.Length = -1;
	m_Certificate.Data   = NULL;

	m_PrivateKey.Type          = OpcUa_Crypto_KeyType_Invalid;
	m_PrivateKey.Key.Length    = 0;
	m_PrivateKey.Key.Data      = PBYTE("");
	m_PrivateKey.fpClearHandle = NULL;

	m_PkiConfig.PkiType                           = OpcUa_NO_PKI;
	m_PkiConfig.CertificateTrustListLocation      = NULL;
	m_PkiConfig.CertificateRevocationListLocation = NULL;
	m_PkiConfig.CertificateUntrustedListLocation  = NULL;
	m_PkiConfig.Flags                             = 0;
	m_PkiConfig.Override                          = NULL;
	}

bool COpcClient::HasTypes(CDevice *pDevice, PCUINT pList, UINT uList)
{
	for( UINT n = 0; n < uList; n++ ) {

		UINT uNode = pList[n];

		if( !pDevice->m_Types[uNode] ) {

			return false;
			}
		}

	return true;
	}

void COpcClient::FreeDiagnosticInfo(INT NoOfDiagnosticInfos, OpcUa_DiagnosticInfo *pDiagnosticInfos)
{
	{ for( INT n = 0; n < NoOfDiagnosticInfos; n++ ) {

		OpcUa_DiagnosticInfo *pInfo = pDiagnosticInfos + n;

		OpcUa_DiagnosticInfo_Clear(pInfo);
		} }

	OpcUa_Free(pDiagnosticInfos);
	}

CString COpcClient::Format(C3INT r)
{
	return CPrintf("%d", r);
	}

CString COpcClient::Format(C3REAL r)
{
	char s[32];

	PTXT w = s;

	if( r < 0 ) {

		r    = -r;

		*w++ = '-';
		}

	_gcvt(r, 5, w);

	char *p = strchr(w, 'e');

	if( p ) {

		p[0] = 'E';

		if( p[2] == '0' ) {

			if( p[3] == '0' ) {

				p[2] = p[4];
				p[3] = 0;
				}
			else {
				p[2] = p[3];
				p[3] = p[4];
				p[4] = 0;
				}
			}

		if( *--p == '.' ) {

			memmove(p, p+1, strlen(p));
			}
		}
	else {
		int n = strlen(s);

		if( n ) {
						
			if( s[n-1] == '.' ) {

				s[n-1] = 0;
				}
			}
		}

	return s;
	}

// Debugging

void COpcClient::AfxTrace(PCTXT pText, ...)
{
	extern BOOL g_fDebug;

	if( g_fDebug ) {

		va_list pArgs;

		va_start(pArgs, pText);

		vprintf(pText, pArgs);	

		va_end(pArgs);
		}
	}

// End of File
