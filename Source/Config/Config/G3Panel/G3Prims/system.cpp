
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// UI System Item
//

// Runtime Class

AfxImplementRuntimeClass(CUISystem, CCommsSystem);

// Static Data

CDatabase * CUISystem::m_pDbGraph = NULL;

UINT	    CUISystem::m_uCount   = 0;

// Constructor

CUISystem::CUISystem(BOOL fAux) : CCommsSystem(fAux)
{
	m_Level     = 2;

	m_pUI       = New CUIManager;

	m_pFramer   = NULL;

	m_DispSize  = CSize(640, 480);

	m_SoftKeys  = 7;

	m_LoColor   = 0;
}

// Destructor

CUISystem::~CUISystem(void)
{
	if( m_pFramer ) {

		m_pFramer->Release();

		m_pFramer = NULL;
	}

	KillAux();
}

// Attributes

UINT CUISystem::HasFont(UINT uFont) const
{
	return TRUE;
}

// Reference Helpers

BOOL CUISystem::ReadRef(CTreeFile &Tree, CString const &Name, CPrimRefMap &Refs, BOOL fRefs, BOOL fFixup)
{
	if( Name.Left(4) == L"REF-" ) {

		if( fRefs ) {

			UINT uOld = wcstol(Name.Mid(4), NULL, 16);

			UINT uNew = AcceptRef(Tree, uOld, fFixup);

			AdjustRef(uOld);

			Refs.Insert(uOld, uNew);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CUISystem::SaveRefs(CTreeFile &Tree, CPrimRefList const &Refs)
{
	CImageFileList * pImageList = m_pUI->m_pImages->m_pFiles;

	CFontList      * pFontList  = m_pUI->m_pFonts->m_pFonts;

	if( Refs.GetCount() ) {

		INDEX n = Refs.GetHead();

		while( !Refs.Failed(n) ) {

			UINT   uRef = Refs[n];

			CItem *pRef = NULL;

			if( (uRef & refTypeMask) == refImage ) {

				pRef = pImageList->GetItem(uRef & ~refTypeMask);
			}

			if( (uRef & refTypeMask) == refFont ) {

				pRef = pFontList->GetFont(uRef & ~refTypeMask);
			}

			if( pRef ) {

				CPrintf Name(L"REF-%8.8X", uRef);

				Tree.PutObject(Name);

				pRef->Save(Tree);

				Tree.EndObject();
			}

			Refs.GetNext(n);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CUISystem::FixRefs(CDispPage *pPage, CPrimRefMap const &Refs, UINT uPass)
{
	if( uPass == 0 ) {

		pPage->EditRef(NOTHING, NOTHING);

		return TRUE;
	}

	if( uPass == 1 ) {

		INDEX r = Refs.GetHead();

		while( !Refs.Failed(r) ) {

			UINT uOld = Refs.GetName(r);

			UINT uNew = Refs.GetData(r);

			pPage->EditRef(uOld | refPending, uNew);

			Refs.GetNext(r);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CUISystem::FixRefs(CPrim *pPrim, CPrimRefMap const &Refs, UINT uPass)
{
	if( uPass == 0 ) {

		pPrim->EditRef(NOTHING, NOTHING);

		return TRUE;
	}

	if( uPass == 1 ) {

		INDEX r = Refs.GetHead();

		while( !Refs.Failed(r) ) {

			UINT uOld = Refs.GetName(r);

			UINT uNew = Refs.GetData(r);

			pPrim->EditRef(uOld | refPending, uNew);

			Refs.GetNext(r);
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CUISystem::AdjustRef(UINT &uRef)
{
	if( (uRef & refTypeMask) == refFont ) {

		if( (uRef & ~refTypeMask) < 0x100 ) {

			uRef += 0x100;

			return TRUE;
		}
	}

	return FALSE;
}

UINT CUISystem::AcceptRef(CTreeFile &Tree, UINT uOld, BOOL fFixup)
{
	if( (uOld & refTypeMask) == refImage ) {

		UINT uNew = m_pUI->m_pImages->LoadFromTreeFile(Tree, fFixup);

		return uNew;
	}

	if( (uOld & refTypeMask) == refFont ) {

		UINT uNew = m_pUI->m_pFonts->LoadFromTreeFile(Tree);

		return uNew;
	}

	return NOTHING;
}

// Conversion

void CUISystem::PostConvert(void)
{
	m_pUI->PostConvert();

	CCommsSystem::PostConvert();
}

// Persistance

void CUISystem::Init(void)
{
	InitAux();

	CCommsSystem::Init();

	m_pUI->m_pPages->PostInit();
}

void CUISystem::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString   const & Name  = Tree.GetName();

		CMetaData const * pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			if( Name == L"DispSize" || Name == L"SoftKeys" || Name == L"LoColor" ) {

				// NOTE -- Should never have been saved in the
				// first place but we don't want to remove them
				// as we'll then get a not-all-loaded message.

				Tree.GetValueAsInteger();

				continue;
			}

			LoadProp(Tree, pMeta);

			continue;
		}
	}

	RegisterHandle();
}

void CUISystem::PostLoad(void)
{
	InitAux();

	CCommsSystem::PostLoad();
}

// Property Filters

BOOL CUISystem::SaveProp(CString const &Tag) const
{
	if( Tag == L"DispSize" || Tag == L"SoftKeys" || Tag == L"LoColor" ) {

		return FALSE;
	}

	return CCommsSystem::SaveProp(Tag);
}

// Download Support

BOOL CUISystem::MakeInitData(CInitData &Init)
{
	CCommsSystem::MakeInitData(Init);

	Init.AddWord(WORD(m_DispSize.cx));

	Init.AddWord(WORD(m_DispSize.cy));

	Init.AddWord(WORD(m_pUI->m_Handle));

	return TRUE;
}

// System Data

void CUISystem::AddNavCats(void)
{
	AddNavCatHead();

	AddNavCatTail();
}

void CUISystem::AddResCats(void)
{
	AddResCatHead();

	AddResCatTail();
}

void CUISystem::AddNavCatHead(void)
{
	CCommsSystem::AddNavCatHead();

	CDatabase *pDbase = GetDatabase();

	UINT       uGroup = GetSoftwareGroup();

	if( uGroup >= SW_GROUP_3A ) {

		pDbase->AddNavCategory(IDS_DISPLAY_PAGES,
				       L"UI",
				       0x21000002,
				       0x20000002,
				       L"Prims|Symbols|Tags|Programs|DevCon|SysLib"
		);
	}
}

void CUISystem::AddNavCatTail(void)
{
	CCommsSystem::AddNavCatTail();
}

void CUISystem::AddResCatHead(void)
{
	CCommsSystem::AddResCatHead();

	CDatabase *pDbase = GetDatabase();

	pDbase->AddResCategory(IDS_PRIMITIVES,
				L"Prims",
				0x21000009,
				0x20000009,
				m_pDbGraph
	);

	pDbase->AddResCategory(IDS_SYMBOLS,
				L"Symbols",
				0x2100000B,
				0x2000000B,
				m_pDbGraph
	);
}

void CUISystem::AddResCatTail(void)
{
	CCommsSystem::AddResCatTail();
}

// Validation

BOOL CUISystem::DoValidate(CCodedTree &Done, BOOL fExpand, UINT uStep)
{
	if( uStep == 6 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING));

		m_pUI->Validate(fExpand);

		return TRUE;
	}

	return CCommsSystem::DoValidate(Done, fExpand, uStep);
}

BOOL CUISystem::DoTagCheck(CCodedTree &Done, CIndexTree &Tags, UINT uTag, UINT uStep)
{
	if( uStep == 2 ) {

		afxThread->SetStatusText(CString(IDS_VALIDATING));

		m_pUI->TagCheck(Done, Tags);

		return TRUE;
	}

	return CCommsSystem::DoTagCheck(Done, Tags, uTag, uStep);
}

// Server Creation

void CUISystem::MakeServers(void)
{
	m_pNameServer = New CUINameServer(this);

	m_pDataServer = New CUIDataServer(this);
}

// Meta Data

void CUISystem::AddMetaData(void)
{
	CCommsSystem::AddMetaData();

	Meta_AddObject(UI);
	Meta_AddPoint(DispSize);
	Meta_AddInteger(SoftKeys);
	Meta_AddInteger(LoColor);

	Meta_SetName((IDS_SYSTEM));
}

// Implementation

BOOL CUISystem::InitAux(void)
{
	if( !m_fAux ) {

		if( !m_uCount++ ) {

			m_pDbGraph = New CDatabase(L"GraphicsResource");

			m_pDbGraph->GetSystemItem()->SetModel(GetModel());

			m_pDbGraph->Init();

			CItem     *pSystem = m_pDbGraph->GetSystemItem();

			CUISystem *pUI     = (CUISystem *) pSystem;

			pUI->m_DispSize    = m_DispSize;

			return TRUE;
		}

		CItem     *pSystem = m_pDbGraph->GetSystemItem();

		CUISystem *pUI     = (CUISystem *) pSystem;

		pUI->m_DispSize    = m_DispSize;
	}

	return FALSE;
}

BOOL CUISystem::KillAux(void)
{
	if( !m_fAux ) {

		if( !--m_uCount ) {

			m_pDbGraph->Kill();

			delete m_pDbGraph;

			return TRUE;
		}
	}

	return FALSE;
}

// End of File
