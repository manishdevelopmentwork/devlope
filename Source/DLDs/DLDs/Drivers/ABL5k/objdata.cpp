
#include "intern.hpp"

#include "object.hpp"

#define AfxTrace	m_pDriver->Trace

//////////////////////////////////////////////////////////////////////////
//
// Logix Data Object
//

// Constructor

CLogixData::CLogixData(void)
{
	m_uType  = typeAlt;
	}

// Object Specific Services

CCODE CLogixData::ReadData(PDWORD pData, UINT uIndex, UINT uCount)
{
	return CCODE_ERROR;
	}

CCODE CLogixData::WriteData(PDWORD pData, UINT uIndex, UINT uCount)
{
	return CCODE_ERROR;
	}

//////////////////////////////////////////////////////////////////////////
//
// Logix Table Data Object
//

// Constructor

CLogixTableData::CLogixTableData(void)
{
	m_uSize = 0;

	m_pDims = NULL;
	}

// Destructor

CLogixTableData::~CLogixTableData(void)
{
	if( m_pDims ) {
		
		delete m_pDims;

		m_pDims = NULL;
		}
	}

// Object Specific Services

CCODE CLogixTableData::ReadData(PDWORD pData, UINT uIndex, UINT uCount)
{
	// tag name

	CIOISegment Name;

	Name.BuildPath(m_pName);

	switch( m_uSize ) {

		case 3:
			Name.AddElement(uIndex / (m_pDims[1] * m_pDims[0]));

			uIndex = uIndex % (m_pDims[1] * m_pDims[0]);

		case 2:
			Name.AddElement(uIndex / m_pDims[0]);

		case 1:
			Name.AddElement(uIndex % m_pDims[0]);
			break;
		}

	// tag data

	CDataBuf Data;

	Data.AddWord(WORD(uCount));

	//
	CBuffer *pRecv;

	if( Service(CIP_READ, Name, Data, pRecv) ) {

		CCIPPath Path(m_pDriver);

		Path.Parse(pRecv);

		if( Path.GetHead()->GetType() == segDataTypeE ) {

			CSegDataTypeE *pSeg = (CSegDataTypeE *) Path.GetHead();

			m_uType = pSeg->GetTypeCode();
			
			pSeg->ParseData(pData, uCount);

			pRecv->Release();
			
			return uCount;
			}

		pRecv->Release();
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	if( GetLastError() == 0x04 ) {

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return CCODE_ERROR;
	}

CCODE CLogixTableData::WriteData(PDWORD pData, UINT uIndex, UINT uCount)
{
	if( m_uType == typeAlt ) {

		DWORD Data;

		if( ReadData(&Data, uIndex, 1) & CCODE_ERROR ) {
			
			return CCODE_ERROR;
			}
		}

	//
	CIOISegment Name;

	Name.BuildPath(m_pName);

	switch( m_uSize ) {

		case 3:
			Name.AddElement(uIndex / (m_pDims[1] * m_pDims[0]));

			uIndex = uIndex % (m_pDims[1] * m_pDims[0]);

		case 2:
			Name.AddElement(uIndex / m_pDims[0]);

		case 1:
			Name.AddElement(uIndex % m_pDims[0]);
			break;
		}

	CCIPPath Path(m_pDriver);

	CSegDataTypeE *pType = new CSegDataTypeE;

	Path.Append(pType);

	//

	pType->SetTypeCode(m_uType);

	//
	CDataBuf Data;

	Data.Encode(Path);

	pType->EncodeData(Data, pData, uCount);

	//
	CBuffer *pRecv;

	if( Service(CIP_WRITE, Name, Data, pRecv) ) {
		
		pRecv->Release();
		
		return uCount;
		}
	
	return CCODE_ERROR;
	}

//////////////////////////////////////////////////////////////////////////
//
// Logix Named Data Object
//

// Constructor

CLogixNamedData::CLogixNamedData(void)
{
	}

// Object Specific Services

CCODE CLogixNamedData::ReadData(PDWORD pData, UINT uIndex, UINT uCount)
{
	//
	CIOISegment Name;

	Name.BuildPath(m_pName);

	//
	CDataBuf Data;

	Data.AddWord(WORD(uCount));

	//
	CBuffer *pRecv;

	if( Service(CIP_READ, Name, Data, pRecv) ) {

		CCIPPath Path(m_pDriver);

		Path.Parse(pRecv);

		if( Path.GetHead()->GetType() == segDataTypeE ) {

			CSegDataTypeE *pSeg = (CSegDataTypeE *) Path.GetHead();

			m_uType = pSeg->GetTypeCode();
			
			pSeg->ParseData(pData, uCount);

			pRecv->Release();
			
			return uCount;
			}

		pRecv->Release();
		
		return CCODE_ERROR | CCODE_NO_DATA;
		}

	if( GetLastError() == 0x04 ) {

		return CCODE_ERROR | CCODE_NO_DATA;
		}

	return CCODE_ERROR;
	}

CCODE CLogixNamedData::WriteData(PDWORD pData, UINT uIndex, UINT uCount)
{
	if( m_uType == typeAlt ) {

		DWORD Data;

		if( ReadData(&Data, 0, 1) & CCODE_ERROR ) {
			
			return CCODE_ERROR;
			}
		}
	
	// tag name

	CIOISegment Name;

	Name.BuildPath(m_pName);

	// data type

	CCIPPath Path(m_pDriver);

	CSegDataTypeE *pType = new CSegDataTypeE;

	Path.Append(pType);

	//

	pType->SetTypeCode(m_uType);

	//
	CDataBuf Data;

	Data.Encode(Path);

	pType->EncodeData(Data, pData, uCount);

	//
	CBuffer *pRecv;

	if( Service(CIP_WRITE, Name, Data, pRecv) ) {
		
		pRecv->Release();
		
		return uCount;
		}

	return CCODE_ERROR;
	}

//////////////////////////////////////////////////////////////////////////
//
// IOI Segment
//

// Constructor

CIOISegment::CIOISegment(void)
{
	}

// Operations

void CIOISegment::BuildPath(PCTXT pText)
{
	for( PCTXT p1 = pText; *p1; ) {

		PCTXT  p2 = strchr(p1, '.');

		UINT uLen = p2 ? (p2 - p1) : strlen(p1);

		AddSymbol(PBYTE(p1), uLen);

		p1 += uLen + 1;
		}
	}

void CIOISegment::AddElement(UINT uIndex)
{
	CSegLogical *pSeg = new CSegLogical;

	pSeg->SetLogType(logMember);

	pSeg->SetLogValue(uIndex);
	
	Append(pSeg);
	}

// Implementation

void CIOISegment::AddSymbol(PBYTE pData, UINT uLen)
{
	PBYTE p1;

	if( (p1 = PBYTE(memchr(pData, '[', uLen))) ) {

		AddSymbol (pData, p1 - pData);

		p1 ++;

		uLen -= (p1 - pData);

		PBYTE p2;

		while( uLen ) {

			if( (p2 = PBYTE(memchr(p1, ',', uLen))) ) {

				AddElement(ToInt(p1, p2 - p1));

				p2 ++;

				uLen -= p2 - p1;

				p1 = p2;
				
				continue;
				}

			if( (p2 = PBYTE(memchr(p1, ']', uLen))) ) {

				AddElement(ToInt(p1, p2 - p1));

				p2 ++;

				uLen -= p2 - p1;

				p1 = p2;
				}
			}	

		return;
		}
	
	CSegData *pSeg = new CSegData;

	pSeg->SetSymbol(pData, uLen);
	
	Append(pSeg);
	}

UINT CIOISegment::ToInt(PBYTE pData, UINT uLen)
{
	UINT uData = 0;

	while( uLen -- ) {

		uData = uData * 10 + GetDecChar(*pData ++);
		}

	return uData;
	}

UINT CIOISegment::GetDecChar(BYTE bData)
{
	if( bData >= '0' && bData <= '9' ) return bData - '0';

	return 0;
	}

// End of File
