
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SymFactSymbol_HPP

#define INCLUDE_SymFactSymbol_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "SymFactParser.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Symbol Factory Symbol
//

class CSymFactSymbol : public CSymFactParser
{
	public:
		// Constructor
		CSymFactSymbol(void);

		// Destructor
		~CSymFactSymbol(void);

		// Attributes
		UINT    GetHandle(void) const;
		CString GetDesc(void) const;
		UINT    GetType(BOOL fNew) const;
		BOOL	CanShade(BOOL fNew) const;

		// Data Access
		UINT GetData(PBYTE &pCopy);
		BOOL GetData(CByteArray &Data);

		// Rendering
		HBITMAP      GetBitmap(int &cx, int &cy);
		HENHMETAFILE GetMetaFile(void);
		BOOL         GetPng(UINT uMode, DWORD Fill, DWORD Back, CSize Size, BOOL fKeep, CByteArray &Data);

		// Operations
		BOOL ParseCat1(UINT uCat, PCBYTE pCat, UINT uSlot);
		BOOL ParseCat3(UINT uCat, char *pText);

	protected:
		// Cache Data
		struct CCache
		{
			UINT	   m_Magic;
			UINT	   m_uMode;
			DWORD	   m_Fill;
			DWORD	   m_Back;
			int	   m_xSize;
			int	   m_ySize;
			BOOL	   m_fKeep;
			CByteArray m_Data;
			};

		// Data Members
		PWORD    m_pLine;
		CString	 m_Desc;
		UINT	 m_uCat;
		UINT	 m_uHandle;
		BOOL     m_fShade;
		UINT	 m_uType3;
		UINT	 m_uType1;
		PCBYTE   m_pData1;
		UINT     m_uData1;
		CCache   m_Cache;
		BOOL	 m_fSave;

		// Overridables
		void OnParseTag(char const *pName, char const *pData);

		// Implementation
		UINT GetColorCount(BITMAPINFOHEADER *pHead);
		void FixCase(CString &Name);
		BOOL LoadCache(void);
		BOOL SaveCache(void);
		BOOL FindPath(void);
	};

// End of File

#endif
