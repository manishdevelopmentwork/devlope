
//////////////////////////////////////////////////////////////////////////
//
// B & R Mininet Driver
//

class CBRMininetSerialDriver : public CMasterDriver
{
	public:
		// Constructor
		CBRMininetSerialDriver(void);

		// Destructor
		~CBRMininetSerialDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Data
		struct CContext
		{
			BYTE	m_bDrop;
			DWORD	m_dWriteErr;

			BYTE	m_bIndex;
			BOOL	m_fIndex;
			BOOL	m_fReset;
			BYTE	m_bCheck;
			};

		CContext *	m_pCtx;

		// Data Members
		BYTE	m_bTx[256];
		BYTE	m_bSend[256];
		BYTE	m_bRx[256];
		UINT	m_uPtr;

		BOOL	m_fIndex;
		BOOL	m_fReset;

		PBYTE	m_pTx;

		// Hex Lookup
		LPCTXT	m_pHex;

		// Implementation
		BOOL	ResetIndex(void);
		BOOL	ResetSlave(void);
		void	NextIndex(void);
		BOOL	CheckInit(void);
		void	CheckError(void);
		
		// Frame Building
		void	StartFrame(void);
		void	EndFrame(void);
		void	AddByte(BYTE bData);
		void	AddWord(WORD wData);
		void	AddLong(DWORD dData);
		void	AddWordRev(WORD wData);
		void	AddLongRev(DWORD dData);
		void	AddRead(AREF Addr, PDWORD pData, UINT uCount);
		void	AddWrite(AREF Addr, PDWORD pData, UINT uCount);
		BYTE	GetuCount(UINT uType, UINT uCount);
		
		// Transport Layer
		UINT	Transact(void);
		void	Send(void);
		UINT	GetReply(void);
		void	GetResponse(AREF Addr, PDWORD pData, UINT uCount);
		DWORD	Get32(UINT *pPos);
		DWORD	Get32Rev(UINT *pPos);
		WORD	Get16(UINT *pPos);
		WORD	Get16Rev(UINT *pPos);
		
		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Helpers
		void	CheckAdd(BYTE bData);
		BYTE	CheckRead(void);
		BOOL	CheckResponse(UINT uResult);
	};

// Spaces
#define	SP_BYTES	1
#define	SP_BYTES_REV	2
#define	SP_SLAVERESET	10

#define RESET_EXECUTED	0xC0
#define	INDEX_ERROR	0xC1
#define	INDEX_CONTRAD	0xC2
#define	UNRECOGNIZED	0xC4
#define	DUPLICATE_CMD	0xC8
#define	INVALID_PARAM	0xC3
#define	INDEX_RESET	0x3F

#define	OP_REG_WRITE	0
#define	OP_REG_READ	1

// End of File
