
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Home Page
//

// Data

var m_page_init;

var m_quick_access;

// Code

function pageMain() {

	m_page_init = true;

	m_quick_access = false;

	sendInfoRequest();
}

function sendInfoRequest() {

	var url = "/ajax/main-status.ajax";

	url += "?tab=" + m_tab_id;

	url += "&init=" + (m_page_init ? 1 : 0);

	$.get({
		url: makeAjax(url),
		success: onInfoOkay,
		error: onInfoFailed,
		timeout: m_page_init ? 15000 : 0,
		xhrFields: { withCredentials: true },
	});
}

function onInfoOkay(data) {

	if (data.okay) {

		showSection(data, "device", $("#device-table"), null, showDevice);

		showSection(data, "pxe", $("#pxe-table"), $("#pxe-show"), showPxe);

		showSection(data, "faces", $("#face-table"), null, showFaces);
	}

	if (!m_quick_access) {

		loadQuickAccess();

		m_quick_access = true;
	}

	if (!("defer" in data) || !data.defer) {

		setTimeout(sendInfoRequest, 1000);
	}
	else {

		m_page_init = false;

		setTimeout(sendInfoRequest, 100);
	}

	$("#page-area").css("display", "");
}

function showSection(data, name, table, show, func) {

	if (!show) {

		show = table;
	}

	if (name in data) {

		func(data[name]);

		show.css("display", "");
	}
	else {

		show.css("display", "none");
	}
}

function showDevice(device) {

	var body = $("#device-body");

	body.empty();

	addProp(body, device, "model", "Device Model");

	addProp(body, device, "group", "Enabled Group");

	addProp(body, device, "serial", "Serial Number");

	addProp(body, device, "version", "Software Version");
}

function showPxe(device) {

	var info = $("#pxe-buttons");

	var body = $("#pxe-body");

	body.empty();

	addProp(body, device, "status", "Status");

	if (m_access <= 1) {

		info.empty();

		var bd = $("<div/>").appendTo(info);

		var b1 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-danger").html("Stop System").appendTo(bd);

		var b2 = $("<button/>").attr("type", "button").css("margin-right", "16px").addClass("btn btn-success").html("Start System").appendTo(bd);

		if (!device.stop) {

			b1.attr("disabled", true);
		}

		if (!device.start) {

			b2.attr("disabled", true);
		}

		b1.off("click").on("click", function () { sendCommand("stop"); });

		b2.off("click").on("click", function () { sendCommand("start"); });
	}
}

function showFaces(faces) {

	var body = $("#face-body");

	body.empty();

	for (var n = 0; n < faces.length; n++) {

		var face = faces[n];

		var tr = $("<tr/>").appendTo(body);

		if ("path" in face && face.path.length) {

			function link(desc) {

				var text = "";

				text += "<a href='/editor.htm?m=s&nav=" + face.path + "'>";

				text += desc;

				text += "</a>";

				return text;
			}

			addCell(tr, face, "desc", link);
		}
		else {

			addCell(tr, face, "desc");
		}

		addCell(tr, face, "name");
		addCell(tr, face, "up", isUp);
		addCell(tr, face, "addr", makeLink);
		addCell(tr, face, "mask");
		addCell(tr, face, "mac");

		if ("type" in face) {

			if (face.type == "cell") {

				var td = $("<td/>").appendTo(tr);

				var cv = $("<canvas width='24px' height='16px' style='background: transparent; margin-right: 8px'/>").appendTo(td);	

				var ts = $("<span/>").appendTo(td);

				let id = face.index;

				var sf = function () {

					showCell(id);
				};

				ts.html(face.register ? face.network.trim() : "");

				drawBars(cv[0], face.register ? face.bars : 0);

				cv.addClass('hand-enable').off('click').on('click', sf);

				continue;
			}

			if (face.type == "wifi") {

				var td = $("<td/>").appendTo(tr);

				var cv = $("<canvas width='24px' height='16px' style='background: transparent; margin-right: 8px'/>").appendTo(td);

				var ts = $("<span/>").appendTo(td);

				let id = face.index;

				var sf = function () {

					showWiFi(id);
				};

				ts.html(face.register ? face.network.trim() : "");

				drawWiFi(cv[0], face.register ? face.bars : 0);

				cv.addClass('hand-enable').off('click').on('click', sf);

				continue;
			}

			if (face.type == "eth") {

				var td = $("<td/>").appendTo(tr);

				var tt = "";

				if (face.up) {

					if (face.speed >= 1000) {

						tt = (face.speed / 1000).toString() + "G, ";
					}
					else {

						tt = face.speed.toString() + "M, ";
					}

					tt = tt + (face.full ? "Full Duplex" : "Half Duplex");
				}

				td.html(tt);

				continue;
			}
		}

		addCell(tr, face, "none");
	}
}

function onInfoFailed(error) {

	if (!check401(error.status)) {

		$("#page-area").css("display", "none");

		m_page_init = true;

		setTimeout(sendInfoRequest, 5000);
	}
}

function isUp(x) {

	return x ? "UP" : "DOWN";
}

function makeLink(x) {

	return "<a href='http://" + x + "' target='_blank'>" + x + "</a>";
}

function addCell(tr, data, name, func) {

	var td = $("<td/>").appendTo(tr);

	if (name in data) {

		td.html(func ? func(data[name]).toString() : data[name].toString());
	}
}

function addProp(tb, data, name, label, func) {

	if (name in data) {

		var tr = $("<tr/>").appendTo(tb);

		$("<td/>").html("<b>" + label + "</b>").css("width", "40%").appendTo(tr);

		var td = $("<td/>").css("width", "60%").appendTo(tr);

		td.html(func ? func(data[name]).toString() : data[name].toString());
	}
}

function sendCommand(cmd) {

	var url = "/ajax/pxe-command.ajax?command=" + cmd;

	$.get({
		url: makeAjax(url),
		xhrFields: { withCredentials: true },
	});
}

function loadQuickAccess() {

	var info = $("#quick-access");

	var bd = $("<div/>").appendTo(info);

	var b1 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-primary").html("Configuration").appendTo(bd);

	var b2 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-primary").html("Diagnostics").appendTo(bd);

	var b3 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-primary").html("System Log").appendTo(bd);

	var b4 = $("<button/>").attr("type", "button").css("margin-right", "8px").addClass("btn btn-primary").html("System Info").appendTo(bd);

	b1.on('click', function () { jumpTo("/config.htm"); });

	b2.on('click', function () { jumpTo("/system.htm"); });

	b3.on('click', function () { jumpTo("/syslog.htm"); });

	b4.on('click', function () { jumpTo("/devinfo.htm"); });
}

// End of File
