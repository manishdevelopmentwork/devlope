
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_SchemaGenerator_HPP

#define INCLUDE_SchemaGenerator_HPP

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSchemaFiles;

//////////////////////////////////////////////////////////////////////////
//
// Schema Generator
//

class CSchemaGenerator : public IConfigUpdate, public ISchemaGenerator
{
public:
	// Constructor
	CSchemaGenerator(void);

	// Destructor
	virtual ~CSchemaGenerator(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IConfigUpdate
	void METHOD OnConfigUpdate(char cWhich);

	// ISchemaGenerator
	bool METHOD Open(void);
	bool METHOD GetDefault(char cTag, CString &Text);
	bool METHOD GetSchema(char cTag, CString &Text);
	bool METHOD GetNaming(char cTag, CString &Text);

protected:
	// Data Members
	ULONG            m_uRefs;
	CSchemaFiles   * m_pFiles;
	IConfigStorage * m_pConfig;
	CString		 m_Default;
	CString	         m_HSchema;
	CString	         m_PSchema;
	CString	         m_SSchema;
	CString	         m_USchema;

	// Overridables
	virtual bool MakeHardwareSchema(void)                   = 0;
	virtual bool MakePersonalitySchema(void)                = 0;
	virtual bool MakeSystemSchema(void)                     = 0;
	virtual bool MakeUserSchema(void)                       = 0;
	virtual bool AdjustDefault(char cTag, CJsonData *pData) = 0;

	// Implementation
	bool        MakeSchemas(void);
	void        CheckUserConfig(void);
	bool	    CheckConfig(char cTag);
	bool	    CheckConfig(CJsonData *pConfig, CJsonData *pSchema, bool fTop);
	bool	    CheckTab(CTree<CString> &Seen, CJsonData *pConfig, CJsonData *pTab);
	bool	    CleanConfig(CTree<CString> const &Seen, CJsonData *pConfig);
	bool	    Substitute(CJsonData *pJson, CString Find, CString Subst);
	void	    AddDisplaySize(CJsonData *pJson, CArray<DWORD> const &List);
	void	    AddPermittedGroups(CJsonData *pJson, UINT uMask);
	CString	    GetEnumCount(UINT n);
	CString     GetHardwareConfig(void);
	CString     GetFileText(PCTXT pName);
	CJsonData * GetFileJson(PCTXT pName);
	CString     FormatDefault(char cTag, CString const &Text);
	void	    AddFragment(CJsonData *pSchema, PCTXT pWhere, PCTXT pName);
	void	    AddFragment(CJsonData *pList, PCTXT pName);
};

// End of File

#endif
