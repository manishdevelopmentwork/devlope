/**
 * @file
 * 
 * UDR library function "single include" header
 *
 * Include this header to get all necessary libudr functionality
 */

/*
 * Copyright 2012 SIXNET, LLC.
 *
 * Notice: This document contains proprietary, confidential information
 * owned by SIXNET, LLC and is protected as an unpublished work under
 * the Copyright Law of the United States. No part of this document may
 * be copied, disclosed to others or used for any purpose without the
 * written permission of SIXNET, LLC.
 */

#ifndef UDR_H
#define UDR_H

// Overall C++ compatibility shim. Allows the rest of the
// implementation to be pure C, as long as the C++ people only include
// this file.
#ifdef __cplusplus
extern "C" {
#endif

#include "master.h"
#include "udr_list.h"
#include "udr_utils.h"

/**
 * Maps a UDR command name to its integer value.
 */
struct udr_cmd {
    char *name;
    uint16_t value;
};

/**
 * Lookup Table to access integer values for known UDR commands.
 */
static const struct udr_cmd udr_commands[] = {
    { "UDR_NOP", UDR_NOP },
    { "UDR_ACK", UDR_ACK },
    { "UDR_NAK", UDR_NAK },
    { "UDR_VERS", UDR_VERS },
    { "UDR_NIO", UDR_NIO },
    { "UDR_GETD", UDR_GETD },
    { "UDR_GETB", UDR_GETB },
    { "UDR_GETA", UDR_GETA },
    { "UDR_GETS", UDR_GETS },
    { "UDR_PUTD", UDR_PUTD },
    { "UDR_PUTB", UDR_PUTB },
    { "UDR_PUTA", UDR_PUTA },
    { "UDR_SETD", UDR_SETD },
    { "UDR_CLRD", UDR_CLRD },
    { "UDR_FILESYS", UDR_FILESYS },
    { "UDR_IOXCHG", UDR_IOXCHG },
};
#define N_UDR_COMMANDS (sizeof(udr_commands)/sizeof(struct udr_cmd))
#define UDR_COMMAND_STR_MAXLEN 11

#ifdef __cplusplus
}
#endif
#endif                          // UDR_H
