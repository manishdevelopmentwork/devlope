
#include "Intern.hpp"

#include "WebTranslationDialog.hpp"

#include "gdiplus.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Translation Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CWebTranslationDialog, CStdDialog);

// Constructors

CWebTranslationDialog::CWebTranslationDialog( CLangManager       *pLang,
					      CStringArray const &In,
					      CUIntArray   const &To
					      )

:	m_In(In)

{
	m_pLang      = pLang;

	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fAutoClose = FALSE;

	m_uState     = stateConnect;

	m_uScan      = 0;

	m_pTo        = To.GetPointer();

	m_uTo        = To.GetCount();

	switch( m_pLang->m_LexMode ) {

		case 0:
		case 1:
			SetName(L"WebTranslationDlg1");
			break;

		case 2:
			SetName(L"WebTranslationDlg2");
			break;
		}

	m_Engine.Bind(this, m_pLang);

	m_Engine.SetTo(To);
	}

// Attributes

CString CWebTranslationDialog::GetInput(UINT uString) const
{
	return m_In[uString];
	}

CString CWebTranslationDialog::GetOutput(UINT uString, UINT uSlot) const
{
	uString *= m_uTo;

	uString += uSlot;

	return m_Out[uString];
	}

// Message Map

AfxMessageMap(CWebTranslationDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchCommand(IDOK,     OnCommandOkay  )
 	AfxDispatchCommand(IDDONE,   OnUpdateDone   )
 	AfxDispatchCommand(IDFAIL,   OnUpdateFail   )
	AfxDispatchCommand(IDQUOTA,  OnUpdateAtQuota)

	AfxMessageEnd(CWebTranslationDialog)
	};

// Message Handlers

BOOL CWebTranslationDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_Engine.Create();

	TxFrame();

	return TRUE;
	}

void CWebTranslationDialog::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_pLang->m_LexMode < 2 ) {

		CString Logo;

		switch( m_pLang->m_Server ) {

			case 0:
				Logo = L"goog";
				break;

			case 1:
				Logo = L"msft";
				break;
			}

		if( !Logo.IsEmpty() ) {

			UINT    uData = 0;

			HGLOBAL hData = afxModule->LoadResource(PCTXT(Logo), L"LOGO", uData);

			if( hData ) {

				HGLOBAL hCopy = GlobalAlloc(GHND, uData);

				AfxAssume(hCopy);

				PBYTE   pData = PBYTE(LockResource(hData));

				PBYTE   pCopy = PBYTE(GlobalLock(hCopy));

				memcpy(pCopy, pData, uData);

				UnlockResource(hData);

				FreeResource(hData);

				GlobalUnlock(hCopy);

				////////

				IStream *pStream = NULL;

				GpImage *pImage  = NULL;

				CreateStreamOnHGlobal(hCopy, TRUE, &pStream);

				GdipLoadImageFromStream(pStream, &pImage);

				if( pImage ) {

					CRect Button = GetDlgItem(IDOK).GetWindowRect();

					CRect Frame  = GetDlgItem(300 ).GetWindowRect();

					ScreenToClient(Button);

					ScreenToClient(Frame);

					CRect Rect;

					Rect.right   = Frame.right   - 4;

					Rect.bottom  = Button.bottom - 0;

					Rect.top     = Rect.bottom   - 32;

					Rect.left    = Rect.right    - 158;

					GpGraphics *pGraph = NULL;

					GdipCreateFromHDC(DC.GetHandle(), &pGraph);

					GdipDrawImageRectI( pGraph,
							    pImage,
							    Rect.left,
							    Rect.top,
							    Rect.cx(),
							    Rect.cy()
							    );

					GdipDeleteGraphics(pGraph);

					GdipDisposeImage(pImage);
					}
				}
			}
		}

	for( UINT n = 0; n < 16; n++ ) {

		CRect  Rect = GetDlgItem(200+n).GetWindowRect();

		ScreenToClient(Rect);

		Rect.right  = Rect.left  - 8;

		Rect.left   = Rect.right - 16;

		Rect.bottom = Rect.top   + 16;

		if( !n ) {

			m_pLang->DrawFlagFromSlot(DC, Rect, 0);
			}
		else {
			if( n <= m_uTo ) {

				m_pLang->DrawFlagFromSlot(DC, Rect, m_pTo[n-1]);
				}
			else
				m_pLang->DrawFlag(DC, Rect, 81);
			}
		}
	}

// Command Handlers

BOOL CWebTranslationDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		#pragma warning(suppress: 6286)

		if( TRUE || NoYes(CString(IDS_DO_YOU_WANT_TO_2)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_Engine.Terminate(INFINITE);

			EndDialog(FALSE);
			}
		}
	else
		EndDialog(FALSE);

	return TRUE;
	}

BOOL CWebTranslationDialog::OnUpdateDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
		}

	return TRUE;
	}

BOOL CWebTranslationDialog::OnUpdateFail(UINT uID)
{
	SetError(CString(IDS_OPERATION_FAILED));

	return TRUE;
	}

BOOL CWebTranslationDialog::OnUpdateAtQuota(UINT uID)
{
	UINT uMsg = m_pLang->m_Server ? IDS_AT_QUOTA_MICROSOFT : IDS_AT_QUOTA_GOOGLE;

	SetError(CString(uMsg));

	return TRUE;
	}

// Implementation

void CWebTranslationDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_Engine.Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			EndDialog(TRUE);

			return;
			}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_COMMS_MAN_CLOSE));
		}
	}

void CWebTranslationDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_ERROR_FMT, pText));

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
	}

void CWebTranslationDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
	}

void CWebTranslationDialog::TxFrame(void)
{
	if( m_uState == stateConnect ) {

		ShowStatus(CString(IDS_CONNECTING_TO));

		m_Engine.Connect();

		return;
		}

	if( m_uState == stateTranslate ) {

		CString Text;

		Text.Printf( CString(IDS_TRANSLATING),
			     m_uScan+1,
			     m_In.GetCount()
			     );

		ShowStatus(Text);

		m_Engine.Translate(m_In[m_uScan]);

		return;
		}

	ShowStatus(CString(IDS_OPERATION));

	SetDone(FALSE);
	}

BOOL CWebTranslationDialog::RxFrame(void)
{
	if( m_uState == stateConnect ) {

		for(;;) {

			if( m_uScan == m_In.GetCount() ) {

				m_uState = stateDone;

				return TRUE;
				}

			for( UINT n = 0; m_In[m_uScan][n]; n++ ) {

				if( wisalpha(m_In[m_uScan][n]) ) {

					m_uState = stateTranslate;

					return TRUE;
					}
				}

			for( UINT d = 0; d < m_uTo; d++ ) {

				m_Out.Append(L"");
				}

			m_uScan++;
			}

		return TRUE;
		}

	if( m_uState == stateTranslate ) {

		GetDlgItem(200).SetWindowText(m_Engine.GetInput());

		for( UINT n = 0; n < m_uTo; n++ ) {

			CString Out = m_Engine.GetOutput(n);

			GetDlgItem(201+n).SetWindowText(Out);

			m_Out.Append(Out);
			}

		for(;;) {

			if( ++m_uScan == m_In.GetCount() ) {

				m_uState = stateDone;

				return TRUE;
				}

			for( UINT n = 0; m_In[m_uScan][n]; n++ ) {

				if( wisalpha(m_In[m_uScan][n]) ) {

					m_uState = stateTranslate;

					return TRUE;
					}
				}

			for( UINT d = 0; d < m_uTo; d++ ) {

				m_Out.Append(L"");
				}
			}

		return TRUE;
		}

	SetError(L"INVALID STATE");

	return FALSE;
	}

// End of File
