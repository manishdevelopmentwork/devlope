
#include "Intern.hpp"

#include "HttpDigest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// HTTP Digest Helper
//

// Constructor

CHttpDigest::CHttpDigest(void)
{
	m_uNonceCount = 0;
	}

// Calculation

CString CHttpDigest::GetResponse(CString qop, CString Verb, CString Path, PCBYTE pBody, UINT uBody, CString User, CString Pass)
{
	CString HA1, HA2, Res;

	HA1 = MD5( CPrintf( "%s:%s:%s", 
			    PCTXT(User),
			    PCTXT(m_Realm),
			    PCTXT(Pass)
			    ));

	if( m_Algorithm == "md5-sess" ) {

		HA1 = MD5( CPrintf( "%s:%s:%s",
				    PCTXT(HA1),
				    PCTXT(m_Nonce),
				    PCTXT(m_ClientNonce)
				    ));
		}

	if( qop == "auth-int" ) {

		HA2 = MD5( CPrintf( "%s:%s:%s",
				    PCTXT(Verb),
				    PCTXT(Path),
				    PCTXT(MD5(pBody, uBody))
				    ));
		}
	else {
		HA2 = MD5( CPrintf( "%s:%s",
				    PCTXT(Verb),
				    PCTXT(Path)
				    ));
		}

	if( qop.IsEmpty() ) {

		Res = MD5( CPrintf( "%s:%s:%s",
				    PCTXT(HA1),
				    PCTXT(m_Nonce),
				    PCTXT(HA2)
				    ));
		}
	else {
		Res = MD5( CPrintf( "%s:%s:%s:%s:%s:%s",
				    PCTXT(HA1),
				    PCTXT(m_Nonce),
				    PCTXT(CPrintf("%8.8x", m_uNonceCount)),
				    PCTXT(m_ClientNonce),
				    PCTXT(qop),
				    PCTXT(HA2)
				    ));
		}

	return Res;
	}

// Operations

void CHttpDigest::Clear(void)
{
	m_Opaque.Empty();

	m_Nonce.Empty();
	}

// MD5 Helpers

CString CHttpDigest::MD5(CString Data)
{
	return MD5(PCTXT(Data), Data.GetLength());
	}

CString CHttpDigest::MD5(CBytes Data)
{
	return MD5(Data.GetPointer(), Data.GetCount());
	}

CString CHttpDigest::MD5(PCVOID pData, UINT uSize)
{
	BYTE bHash[16];

	char sHash[33];
	
	MD5(PBYTE(pData), uSize, bHash);

	PCTXT pHex = "0123456789abcdef";

	UINT  c    = 0;

	for( UINT n = 0; n < 16; n++ ) {

		sHash[c++] = pHex[bHash[n]/16];
		
		sHash[c++] = pHex[bHash[n]%16];
		}

	sHash[c++] = 0;

	return sHash;
	}

void CHttpDigest::MD5(CString Data, PBYTE pHash)
{
	MD5(PCTXT(Data), Data.GetLength(), pHash);
	}

void CHttpDigest::MD5(PCVOID pData, UINT uSize, PBYTE pHash)
{
	md5(PCBYTE(pData), uSize, pHash);
	}

// Implementation

CString CHttpDigest::MakeNonce(void)
{
	BYTE      bData[16];

	IEntropy *pEntropy;

	AfxGetObject("entropy", 0, IEntropy, pEntropy);

	if( !pEntropy ) {

		AfxTrace("http: no entropy source available\n");

		memcpy(bData, "0123456789ABCDEF", 16);
		}
	else {
		pEntropy->GetEntropy(bData, 16);

		pEntropy->Release();
		}

	return MD5(bData, sizeof(bData));
	}

// End of File
