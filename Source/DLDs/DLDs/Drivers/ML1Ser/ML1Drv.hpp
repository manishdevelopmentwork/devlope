#ifndef	INCLUDE_ML1DRV_HPP
	
#define	INCLUDE_ML1DRV_HPP

#include "ml1serv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ML1 Driver Protocol
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

class CML1Driver : public CML1Server
{
	public:
		// Constructor
		CML1Driver(void);

		// Destructor
		~CML1Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Entry Points
		DEFMETH(void ) Service(void);
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Special Register Support
		void DoSpecialRead(CML1Dev * pDev);
		void DoAuthenticationPoll(CML1Dev * pDev);

		// Authentication Support
		BOOL GetMacId(void);
		BOOL IsAuthenticated(CML1Dev * pDev);
		BOOL IsAuthPollTimedOut(CML1Dev * pDev);
		void SetAuthPoll(CML1Dev * pDev);
		BOOL ReqAuthentication(CML1Dev * pDev);
	};

#endif

// End of File
