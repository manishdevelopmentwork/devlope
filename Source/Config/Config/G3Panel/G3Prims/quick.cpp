		       
#include "intern.hpp"

#include "quick.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Quick Plot
//

// Dynamic Class

AfxImplementDynamicClass(CPrimQuickPlot, CPrimLegacyFigure);

// Constructor

CPrimQuickPlot::CPrimQuickPlot(void)
{
	m_uType = 0x32;
	
	m_pTag  = NULL;
	
	m_pMin  = NULL;
	
	m_pMax  = NULL;
	
	m_Align = 0;
	
	m_Pad   = 0;

	m_pPen  = New CPrimColor;
	}

// Attributes

BOOL CPrimQuickPlot::IsTagRef(void) const
{
	if( m_pTag ) {

		if( m_pTag->IsTagRef() ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPrimQuickPlot::GetTagItem(CTag * &pTag) const
{
	if( IsTagRef() ) {

		if( m_pTag->GetTagItem(pTag) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// UI Update

void CPrimQuickPlot::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	// PAUL -- Add automatic min and max per other primitives.

	CPrimLegacyFigure::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CPrimQuickPlot::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag == "Tag" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagTagRef;

		return TRUE;
		}

	if( Tag == "Min" || Tag == "Max" ) {

		Type.m_Type  = typeReal;

		Type.m_Flags = 0;

		return TRUE;
		}
	
	return FALSE;
	}

// Overridables

void CPrimQuickPlot::Draw(IGDI *pGDI, UINT uMode)
{
	CPrimLegacyFigure::Draw(pGDI, uMode);

	pGDI->SetPenFore(m_pPen->GetColor());

	double th = 0;

	int    x2 = 0;

	for( int x = 0; x < 360; x++ ) {

		int c  = 50 + int(50 * sin(th));

		int x1 = m_DrawRect.x1 + 2 + (m_DrawRect.x2 - m_DrawRect.x1 - 4) * x / 360;

		int y1 = m_DrawRect.y1 + 2 + (m_DrawRect.y2 - m_DrawRect.y1 - 4) * c / 100;

		if( x ) {

			if( x1 > x2 ) {

				pGDI->LineTo(x1, y1);

				x2 = x1;
				}
			}
		else
			pGDI->MoveTo(x1, y1);

		th += 3.1415926 / 90;
		}
	}

void CPrimQuickPlot::SetInitState(void)
{
	CPrimLegacyFigure::SetInitState();

	m_pEdge->m_Width   = 0;

	m_pBack->m_Pattern = 0;

	m_pPen->Set(GetRGB(31,31,31));
	}

// Download Support

BOOL CPrimQuickPlot::MakeInitData(CInitData &Init)
{
	CPrimLegacyFigure::MakeInitData(Init);

	CTag *pTag = NULL;

	if( GetTagItem(pTag) ) {

		Init.AddWord(WORD(pTag->GetIndex()));
		}
	else
		Init.AddWord(0xFFFF);

	Init.AddItem(itemVirtual, m_pMin);

	Init.AddItem(itemVirtual, m_pMax);

	Init.AddByte(BYTE(m_Align));

	Init.AddByte(BYTE(m_Pad));

	Init.AddItem(itemSimple, m_pPen);

	return TRUE;
	}

// Meta Data

void CPrimQuickPlot::AddMetaData(void)
{
	CPrimLegacyFigure::AddMetaData();

	Meta_AddVirtual(Tag);
	Meta_AddVirtual(Min);
	Meta_AddVirtual(Max);
	Meta_AddInteger(Align);
	Meta_AddInteger(Pad);
	Meta_AddObject (Pen);

	Meta_SetName((IDS_QUICK_PLOT));
	}

// End of File
