
#include "Intern.hpp"

#include "OpcBase.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// OPC-UA Base Class
//

// Platform Data

OpcUa_Handle COpcBase::m_hPlatform = NULL;

UINT         COpcBase::m_uPlatRefs = 0;

UINT	     COpcBase::m_uPlatStop = 0;

// Platform Operations

void COpcBase::BaseInit(void)
{
	if( !m_uPlatRefs++ ) {

		OpcUa_P_Initialize(&m_hPlatform);

		OpcUa_ProxyStubConfiguration Config;

		LoadConfig(Config);

		OpcUa_ProxyStub_Initialize(m_hPlatform, &Config);
		}
	}

void COpcBase::BaseStop(void)
{
	// This is not perfect but it'll work since we always
	// stop all the platform users before we call term.

	if( ++m_uPlatStop == m_uPlatRefs ) {

		}
	}

void COpcBase::BaseTerm(void)
{
	if( !--m_uPlatRefs ) {

		OpcUa_ProxyStub_Clear();

		OpcUa_P_Clean(&m_hPlatform);
		}
	
	m_uPlatStop--;
	}

// Platform Config

void COpcBase::LoadConfig(OpcUa_ProxyStubConfiguration &Config)
{
	Config.bProxyStub_Trace_Enabled              = OpcUa_False;
	Config.uProxyStub_Trace_Level                =  0;
	Config.iSerializer_MaxAlloc                  = -1;
	Config.iSerializer_MaxStringLength           = -1;
	Config.iSerializer_MaxByteStringLength       = -1;
	Config.iSerializer_MaxArrayLength            = -1;
	Config.iSerializer_MaxMessageSize            = -1;
	Config.iSerializer_MaxRecursionDepth         = -1;
	Config.bSecureListener_ThreadPool_Enabled    = OpcUa_False;
	Config.iSecureListener_ThreadPool_MinThreads = -1;
	Config.iSecureListener_ThreadPool_MaxThreads = -1;
	Config.iSecureListener_ThreadPool_MaxJobs    = -1;
	Config.bSecureListener_ThreadPool_BlockOnAdd = OpcUa_True;
	Config.uSecureListener_ThreadPool_Timeout    = OPCUA_INFINITE;
	Config.bTcpListener_ClientThreadsEnabled     = OpcUa_False;
	Config.iTcpListener_DefaultChunkSize         = -1;
	Config.iTcpConnection_DefaultChunkSize       = -1;
	Config.iTcpTransport_MaxMessageLength        = -1;
	Config.iTcpTransport_MaxChunkCount           = -1;
	Config.bTcpStream_ExpectWriteToBlock         = OpcUa_True;
	}

// Time Support

void COpcBase::TimeFromTimeval(OpcUa_DateTime &t, timeval const &time)
{
	if( time.tv_sec ) {

		static const INT64 SECS_BETWEEN_EPOCHS = 11644473600LL;
		static const INT64 SECS_TO_100NS       = 10000000LL;
		static const INT64 MSECS_TO_100NS      = 10000LL;
		static const INT64 MICROSECS_TO_100NS  = 10LL;

		INT64 unixtime;

		unixtime = time.tv_sec;

		unixtime += SECS_BETWEEN_EPOCHS;

		unixtime *= SECS_TO_100NS;

		unixtime += time.tv_usec * MICROSECS_TO_100NS;

		t.dwHighDateTime = unixtime >> 32;

		t.dwLowDateTime  = unixtime &  0xffffffff;

		return;
		}

	t.dwLowDateTime  = 0;

	t.dwHighDateTime = 0;
	}

DWORD COpcBase::UnixFromTime(OpcUa_DateTime const &t, DWORD d)
{
	if( t.t ) {

		static const INT64 SECS_BETWEEN_EPOCHS = 11644473600LL;

		static const INT64 SECS_TO_100NS       = 10000000LL;

		return DWORD(t.t / SECS_TO_100NS - SECS_BETWEEN_EPOCHS);
		}

	return d;
	}

void COpcBase::TimeFromUnix(OpcUa_DateTime &t, DWORD u)
{
	static const INT64 SECS_BETWEEN_EPOCHS = 11644473600LL;

	static const INT64 SECS_TO_100NS       = 10000000LL;

	t.t = (SECS_BETWEEN_EPOCHS + u) * SECS_TO_100NS;
	}

// End of File
