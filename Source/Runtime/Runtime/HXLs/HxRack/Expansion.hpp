
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Expansion_HPP

#define	INCLUDE_Expansion_HPP

//////////////////////////////////////////////////////////////////////////
//
// Expansion System
//

class CExpansion : public IExpansionSystem, public IExpansionFirmware, public IUsbSystem, public IDiagProvider
{
	public:
		// Constructor
		CExpansion(void);

		// Destructor
		~CExpansion(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Expansion System
		void		      METHOD Attach(IExpansionEvents *pSink);
		void		      METHOD Attach(IExpansionFirmware *pFirm);
		void		      METHOD Attach(IUsbSystemPower *pPower);
		UINT                  METHOD GetSlotCount(void);
		UINT		      METHOD FindSlot(UsbTreePath const &Path);
		BOOL		      METHOD WaitSlot(UINT iSlot, UINT uTimeout);
		IUsbDevice          * METHOD GetDevice(UINT iSlot);
		UINT		      METHOD GetInterfaceCount(UINT iSlot);
		IExpansionInterface * METHOD GetInterface(UINT iSlot, UINT iIdx);
		IUsbHostFuncDriver  * METHOD GetFuncDriver(UINT iSlot, UINT iIdx); 
		PVOID                 METHOD GetAppObject(UINT iSlot, UINT iIdx);
		void		      METHOD FreeAppObject(UINT iSlot, UINT iIdx);
		void		      METHOD FreeAppObjects(void);
		PVOID                 METHOD GetSysObject(UINT iSlot, UINT iIdx);
		void		      METHOD FreeSysObject(UINT iSlot, UINT iIdx);

		// IExpansionFirmware 
		BOOL   METHOD SetFirmwareLock(BOOL fSet);
		PCBYTE METHOD GetFirmwareGuid(UINT uFirm);
		UINT   METHOD GetFirmwareSize(UINT uFirm);
		PCBYTE METHOD GetFirmwareData(UINT uFirm);
		
		// IUsbSystem
		void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver);
		void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Slot States
		enum
		{
			stateFree,
			stateUsed,
			statePresent,
			stateRemoved,
			};

		// Interface
		struct CInterface
		{
			UINT                  m_uPid;
			IUsbHostFuncDriver  * m_pDrv;
			IExpansionInterface * m_pExp;
			IExpansionPnp       * m_pPnp;
			IDevice             * m_pObj;
			int                   m_nRef;
			int		      m_nApp;
			int		      m_nSys;
			};
		
		// Slot 
		struct CSlot
		{
			UsbTreePath	      m_Path;
			IEvent              * m_pEvent;
			IMutex              * m_pMutex;
			IUsbDevice          * m_pDevice;
			UINT		      m_uState;
			UINT                  m_uActive;
			CInterface	      m_Interface[16];
			};

		// Data
		ULONG                m_uRefs;
		UINT                 m_uProv;
		CSlot                m_Slots[128];
		IExpansionEvents   * m_pSink;
		IExpansionFirmware * m_pFirm;
		IUsbSystemPower    * m_pPower;
		IMutex		   * m_pMutex;

		// Object Creation/Destruction
		void OnNewSerial  (IUsbHostFuncDriver *pDriver);
		void OnDelSerial  (IUsbHostFuncDriver *pDriver);
		void OnNewModem   (IUsbHostFuncDriver *pDriver);
		void OnDelModem   (IUsbHostFuncDriver *pDriver);
		void OnNewCanBus  (IUsbHostFuncDriver *pDriver);
		void OnDelCanBus  (IUsbHostFuncDriver *pDriver);
		void OnNewDevNet  (IUsbHostFuncDriver *pDriver);
		void OnDelDevNet  (IUsbHostFuncDriver *pDriver);
		void OnNewProfibus(IUsbHostFuncDriver *pDriver);
		void OnDelProfibus(IUsbHostFuncDriver *pDriver);
		void OnNewModule  (IUsbHostFuncDriver *pDriver);
		void OnDelModule  (IUsbHostFuncDriver *pDriver);
		void OnNewLoader  (IUsbHostFuncDriver *pDriver);
		void OnDelLoader  (IUsbHostFuncDriver *pDriver);
		void OnNewRack    (IUsbHostFuncDriver *pDriver);
		void OnDelRack    (IUsbHostFuncDriver *pDriver);
		void OnNewDongle  (IUsbHostFuncDriver *pDriver);
		void OnDelDongle  (IUsbHostFuncDriver *pDriver);
		void OnNewRemote  (IUsbHostFuncDriver *pDriver);
		void OnDelRemote  (IUsbHostFuncDriver *pDriver);
		void OnNewWiFi    (IUsbHostFuncDriver *pDriver);
		void OnDelWiFi	  (IUsbHostFuncDriver *pDriver);
		void OnNewNetwork (IUsbHostFuncDriver *pDriver);
		void OnDelNetwork (IUsbHostFuncDriver *pDriver);

		// Device Implementation
		IUsbDevice * GetDevice(IUsbHostFuncDriver *pDriver);
		void RegisterDevice(IUsbHostFuncDriver *pDrv, IExpansionInterface *pExp);
		void UnregisterDevice(IUsbHostFuncDriver *pDrv);
		void CheckFirmware(UINT iSlot);

		// Slot Management
		void InitSlots(void);
		void FreeSlots(void);
		UINT FindRegisteredSlot(UsbTreePath const &Path);
		UINT RegisterSlot(UsbTreePath const &Path);
		void UnRegisterSlot(UsbTreePath const &Path);

		// Power Management
		void RegisterPower(IUsbDevice *pDevice, IExpansionInterface *pExpansion);
		void UnregisterPower(IUsbDevice *pDevice, IExpansionInterface *pExpansion);
		UINT FindParentRack(IUsbDevice *pDevice);

		// Lifetime Management
		void LockAppObject(CInterface &Interface);
		void FreeAppObject(CInterface &Interface);

		// Diagnostics
		bool DiagRegister(void);
		bool DiagRevoke(void);
		UINT DiagStatus(IDiagOutput *pOut, IDiagCommand *pCmd);
	};

// End of File

#endif


