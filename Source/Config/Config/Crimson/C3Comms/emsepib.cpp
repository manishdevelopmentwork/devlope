
#include "intern.hpp"

#include "emsonep.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2005 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

// EPB/EPI Common
void CEmersonEPDriver::AddSpacesIB()
{
	if( m_fIBLoaded ) return;

	AddSpace(New CSpace( "AFOM","   Actual Operating Mode",					30001,		WW));
	AddSpace(New CSpace( "AME", "   Alternate Mode Enable",					40017,		WW));
	AddSpace(New CSpace( "A1O", "   Analog Out - Channel 1",				32103,		WW));
	AddSpace(New CSpace( "A2O", "   Analog Out - Channel 2",				32104,		WW));
	AddSpace(New CSpace( "AO1O","   Analog Out1 Offset (user units)",			40652,		LL));
	AddSpace(New CSpace( "AO1S","   Analog Out1 Scale (user units/volt)",			40654,		LL));
	AddSpace(New CSpace( "AO1T","   Analog Out1 Select",					40651,		WW));
	AddSpace(New CSpace( "AO2O","   Analog Out2 Offset (user units)",			40657,		LL));
	AddSpace(New CSpace( "AO2S","   Analog Out2 Scale (user units/volt)",			40659,		LL));
	AddSpace(New CSpace( "AO2T","   Analog Out2 Select",					40656,		WW));
	AddSpace(New CSpace( "AXIS","   Axis Address",						40003,		WW));
	AddSpace(New CSpace(TANS,"ANS","   Axis Name String",				10,	OANS,	OANSL,	WW));


	AddSpace(New CSpace( "BRT", "   Baud Rate",						40004,		WW));
	AddSpace(New CSpace( "BVO", "   Bus Voltage",						32042,		WW));

	AddSpace(New CSpace( "CCT", "   Chain Count",						42600,		WW));
	AddSpace(New CSpace(CNX,"CNX", "   Chain Next\t    (43015+25*n)",		10,	0,	15,	WW));
	AddSpace(New CSpace( "CFLT","   Clear Fault",						1007,		BB));
	AddSpace(New CSpace( "CAC", "   Commutation Angle Correction",				32038,		WW));
	AddSpace(New CSpace( "CTA", "   Commutation Track Angle",				32039,		WW));
	AddSpace(New CSpace( "CVO", "   Commutation Voltage",					32040,		WW));
	AddSpace(New CSpace( "CCC", "   Current Chain Count",					33001,		WW));
	AddSpace(New CSpace( "CMF", "   Custom Motor Flag",					42003,		WW));

	AddSpace(New CSpace( "DAT", "   Drive Ambient Temperature",				42045,		WW));
	AddSpace(New CSpace( "DOF", "   Drive Overtemp Fault Count",				40716,		WW));

	AddSpace(New CSpace( "EFC", "   Encoder H/W Fault Count",				40702,		WW));
	AddSpace(New CSpace( "EOS", "   Encoder Output Scaling",				42061,		WW));
	AddSpace(New CSpace( "EOE", "   Encoder Output Scaling Enable",				42062,		WW));
	AddSpace(New CSpace( "ESFC","   Encoder State Fault Count",				40701,		WW));

	AddSpace(New CSpace(FPC,"FPC", "   Fault Power Up Count (31002+4*(10-n))",	10,	1,	10,	WW));
	AddSpace(New CSpace(FPT,"FPT", "   Fault Power Up Time   (31003+4*(10-n))",	10,	1,	10,	LL));
	AddSpace(New CSpace(FTY,"FTY", "   Fault Type\t         (31001+4*(10-n))",	10,	1,	10,	WW));
	AddSpace(New CSpace( "FST", "   Fault Status Bit Map",					30401,		LL));
	AddSpace(New CSpace( "FFE", "   Feedforwards Enable",					42026,		WW));
	AddSpace(New CSpace(TFRB,"FRB","   Firmware Revision String",			10,	OFRB,	OFRBL,	WW));
	AddSpace(New CSpace(TFRO,"FRO","   FM Firmware Revision Option String",		10,	OFRO,	OFROL,	WW));
	AddSpace(New CSpace( "FRC", "   Foldback RMS",						32033,		WW));
	AddSpace(New CSpace( "FE",  "   Following Error",					32028,		LL));
	AddSpace(New CSpace( "FEE", "   Following Error Enable",				42031,		WW));
	AddSpace(New CSpace( "FEF", "   Following Error Fault Count",				40713,		WW));
	AddSpace(New CSpace( "FEL", "   Following Error Limit",					42032,		LL));
	AddSpace(New CSpace( "FRCT","   Friction",						42023,		WW));

	AddSpace(New CSpace( "HSR", "   Heatsink RMS",						32041,		WW));
	AddSpace(New CSpace( "HDB", "   High DC Bus Fault Count",				40705,		WW));
	AddSpace(New CSpace( "HHP", "   High Performance Gains Enable",				42025,		WW));
	

	AddSpace(New CSpace( "IMV", "   In Motion Velocity",					42035,		WW));
	AddSpace(New CSpace( "IPT", "   In Position Time",					42053,		WW));
	AddSpace(New CSpace( "IPW", "   In Position Window",					42051,		LL));
	AddSpace(New CSpace(IAC,"IAC", "   Index Acceleration (43006+25*n)",		10,	0,	15,	LL));
	AddSpace(New CSpace(ICR,"ICR", "   Index Control\t    (43014+25*n)",		10,	0,	15,	WW));
	AddSpace(New CSpace(IXC,"IXC", "   Index Count\t    (43013+25*n)",		10,	0,	15,	WW));
	AddSpace(New CSpace(IDC,"IXDC","   Index Deceleration (43008+25*n)",		10,	0,	15,	LL));
	AddSpace(New CSpace(IDS2,"IXDS","   Index Distance\t    (43002+25*n)",		10,	0,	15,	LL));
	AddSpace(New CSpace(IDW,"IXDW","   Index Dwell\t    (43012+25*n)",		10,	0,	15,	WW));
	AddSpace(New CSpace( "IRWB","   Index Registration Window Enable",			43016,		WW));
	AddSpace(New CSpace( "IRWE","   Index Registration Window End",				43019,		LL));
	AddSpace(New CSpace( "IRWS","   Index Registration Window Start",			43017,		LL));
	AddSpace(New CSpace(IXT,"IXT", "   Index Type\t    (43001+25*n)",		10,	0,	15,	WW));
	AddSpace(New CSpace(IXV,"IXV", "   Index Velocity\t    (43004+25*n)",		10,	0,	15,	LL));
	AddSpace(New CSpace( "IIR", "   Inertia Ratio",						42021,		WW));
	AddSpace(New CSpace( "ICH", "   Infinite Chaining",					42601,		WW));
	AddSpace(New CSpace(IFOA,"IFOA","   Input Function Active Off Array",		10,	129,	160,	BB));
	AddSpace(New CSpace(IFM,"IFM", "   Input Function Mapping 32",			10,	40201,	40232,	WW));
	AddSpace(New CSpace(IFSA,"IFSA","   Input Function Status Array",		10,	10065,	10096,	BB));
	AddSpace(New CSpace( "IFOB","   Input Function Active Off Bit Map",			40301,		LL));
	AddSpace(New CSpace( "IFAB","   Input Function Always Active Bit Map",			40401,		LL));
	AddSpace(New CSpace( "IFSB","   Input Function Bit Map",				30105,		LL));
	AddSpace(New CSpace(ILDT,"ILDT","   Input Line Debounce Time",			10,	40111,	40126,	WW));
	AddSpace(New CSpace(ILDS,"ILDS","   Input Line Debounced Status Array",		10,	10033,	10048,	BB));
	AddSpace(New CSpace(ILFC,"ILFC","   Input Line Force On/Off Command Array",	10,	2,	16,	BB));
	AddSpace(New CSpace(ILFE,"ILFE","   Input Line Force On/Off Enable Array",	10,	18,	32,	BB));
	AddSpace(New CSpace(ILR,"ILR", "   Input Line Raw Status Array",		10,	10017,	10032,	BB));
	AddSpace(New CSpace(ILS,"ILS", "   Input Line Status Array",			10,	10001,	10015,	BB));
	AddSpace(New CSpace( "ILDB","   Input Lines Debounced Status Bit Map",			30103,		WW));
	AddSpace(New CSpace( "ILOS","   Input Lines Force On/Off Command Bit Map",		40101,		WW));
	AddSpace(New CSpace( "ILOA","   Input Lines Force On/Off Enable Bit Map",		40102,		WW));
	AddSpace(New CSpace( "ILRB","   Input Lines Raw Status Bit Map",			30102,		WW));
	AddSpace(New CSpace( "ILSB","   Input Lines Status Bit Map",				30101,		WW));

	AddSpace(New CSpace( "JAC", "   Jog Acceleration",					41155,		LL));
	AddSpace(New CSpace( "JAD", "   Jog Deceleration",					41157,		LL));
	AddSpace(New CSpace( "JFV", "   Jog Fast Velocity",					41153,		LL));
	AddSpace(New CSpace( "JVE", "   Jog Velocity",						41151,		LL));

	AddSpace(New CSpace( "LV",  "   Line Voltage",						42002,		WW));
	AddSpace(New CSpace( "LPFE","   Low Pass Filter Enable (COMPFE)",			42047,		WW));
	AddSpace(New CSpace( "LDBE","   Low DC Bus Enable",					42046,		WW));
	AddSpace(New CSpace( "LDBF","   Low DC Bus Fault Count",				40704,		WW));
	AddSpace(New CSpace( "LPFF","   Low Pass Filter Frequency (COMPF)",			42048,		WW));

	AddSpace(New CSpace( "M0CM","   Stop Motion",						41306,		WW));
	AddSpace(New CSpace( "M1CM","   Home Motion",						41311,		WW));
	AddSpace(New CSpace( "M2CM","   Index Motion",						41316,		WW));
	AddSpace(New CSpace( "M3CM","   Jog Plus Motion",					41321,		WW));
	AddSpace(New CSpace( "M4CM","   Jog Minus Motion",					41326,		WW));
	AddSpace(New CSpace( "M5CM","   Jog Plus Fast Motion",					41331,		WW));
	AddSpace(New CSpace( "M6CM","   Jog Minus Fast Motion",					41336,		WW));
	AddSpace(New CSpace( "M7CM","   Not Assigned",						41341,		WW));
	AddSpace(New CSpace( "M0TF","   Motion Type Function 0",				41307,		WW));
	AddSpace(New CSpace( "M1TF","   Motion Type Function 1",				41312,		WW));
	AddSpace(New CSpace( "M2TF","   Motion Type Function 2",				41317,		WW));
	AddSpace(New CSpace( "M3TF","   Motion Type Function 3",				41322,		WW));
	AddSpace(New CSpace( "M4TF","   Motion Type Function 4",				41327,		WW));
	AddSpace(New CSpace( "M5TF","   Motion Type Function 5",				41332,		WW));
	AddSpace(New CSpace( "M6TF","   Motion Type Function 6",				41337,		WW));
	AddSpace(New CSpace( "M7TF","   Motion Type Function 7",				41342,		WW));
	AddSpace(New CSpace( "M0EX","   Execute Stop Motion",					1101,		WW));
	AddSpace(New CSpace( "M1EX","   Execute Home Motion",					1102,		WW));
	AddSpace(New CSpace( "M2EX","   Execute Index Motion",					1103,		WW));
	AddSpace(New CSpace( "M3EX","   Execute Jog Plus Motion",				1104,		WW));
	AddSpace(New CSpace( "M4EX","   Execute Jog Minus Motion",				1105,		WW));
	AddSpace(New CSpace( "M5EX","   Execute Jog Plus Fast Motion",				1106,		WW));
	AddSpace(New CSpace( "M6EX","   Execute Jog Minus Fast Motion",				1107,		WW));
	AddSpace(New CSpace( "M7EX","   Not Used",						1108,		WW));
	AddSpace(New CSpace( "MST", "   Motion State",						32063,		WW));
	AddSpace(New CSpace( "MCCR","   Motor Continuous Current Rating",			42116,		WW));
	AddSpace(New CSpace( "MELP","   Motor Encoder Lines Per Revolution",			42108,		WW));
	AddSpace(New CSpace( "MEEA","   Motor Encoder Marker Angle",				42109,		WW));
	AddSpace(New CSpace( "MERM","   Encoder Reference Motion",				42111,		WW));
	AddSpace(New CSpace( "MEUE","   Motor Encoder U Angle",					42110,		WW));
	AddSpace(New CSpace( "MPPI","   Motor Inductance",					42115,		WW));
	AddSpace(New CSpace( "MI",  "   Motor Inertia",						42112,		WW));
	AddSpace(New CSpace( "MKE", "   Motor KE",						42113,		WW));
	AddSpace(New CSpace( "MOS", "   Motor Maximum Operating Speed",				42118,		WW));
	AddSpace(New CSpace( "MOF", "   Motor Overtemp Fault Count ",				40715,		WW));
	AddSpace(New CSpace( "MPCR","   Motor Peak Current Rating",				42117,		WW));
	AddSpace(New CSpace( "MP",  "   Motor Poles",						42107,		WW));
	AddSpace(New CSpace( "MR",  "   Motor Resistance",					42114,		WW));
	AddSpace(New CSpace( "MT",  "   Motor Type",						40002,		WW));

	AddSpace(New CSpace( "NIFC","   NVM Invalid Fault Count",				40712,		WW));

	AddSpace(New CSpace( "OPM", "   Operating Mode Default",				40001,		WW));
	AddSpace(New CSpace( "O1I", "   Option 1 ID (Function Module)",				39985,		WW));
	AddSpace(New CSpace(OFM,"OFM", "   Output Function Mapping 32",			10,	40451,	40482,	WW));
	AddSpace(New CSpace(OFS,"OFS", "   Output Function Status Array",		10,	10097,	10128,	BB));
	AddSpace(New CSpace( "OFSB","   Output Function Status Bit Map",			30107,		LL));
	AddSpace(New CSpace(OLA,"OLA", "   Output Line Active Off Array",		10,	65,	80,	BB));
	AddSpace(New CSpace(OLFC,"OLFC","   Output Line Force On/Off Command Array",	10,	33,	48,	BB));
	AddSpace(New CSpace(OLFE,"OLFE","   Output Line Force On/Off Enable Array",	10,	49,	64,	BB));
	AddSpace(New CSpace(OLS,"OLS", "   Output Line Status Array",			10,	10049,	10064,	BB));
	AddSpace(New CSpace( "OLPB","   Output Lines Active Off Bit Map",			40105,		WW));
	AddSpace(New CSpace( "OLOS","   Output Lines Force On/Off Command Bit Map",		40103,		WW));
	AddSpace(New CSpace( "OLOA","   Output Lines Force On/Off Enable Bit Map",		40104,		WW));
	AddSpace(New CSpace( "OLSB","   Output Lines Status Bit Map",				30104,		WW));
	AddSpace(New CSpace( "ODCL","   Overcurrent Diagnostic Counter 32",			40753,		LL));
	AddSpace(New CSpace( "OFC", "   Overspeed Fault Count",					40709,		WW));
	AddSpace(New CSpace( "OFV", "   Overspeed Velocity Limit",				42036,		WW));

	AddSpace(New CSpace( "PC",  "   Position Command",					32036,		LL));
	AddSpace(New CSpace( "PEIE","   Position Error Integral Enable",			42028,		WW));
	AddSpace(New CSpace( "PEIT","   Position Error Integral Time Constant",			42029,		WW));
	AddSpace(New CSpace( "PF",  "   Position Feedback",					32026,		LL));
	AddSpace(New CSpace( "PFE", "   Position Feedback Encoder",				40081,		LL));
	AddSpace(New CSpace( "PDR", "   Positive Direction",					42044,		WW));
	AddSpace(New CSpace( "PMF", "   Power Stage Fault Count",				40703,		WW));
	AddSpace(New CSpace( "PUC", "   Power Up Count",					40020,		WW));
	AddSpace(New CSpace( "PUS", "   Power Up Self Test Fault Count",			40711,		WW));
	AddSpace(New CSpace( "PUT", "   Power Up Time",						40023,		LL));
	AddSpace(New CSpace( "PUTT","   Power Up Time Total",					40021,		LL));
	AddSpace(New CSpace( "PGR", "   Product Group",						39982,		WW));
	AddSpace(New CSpace( "PID", "   Product ID",						39984,		WW));
	AddSpace(New CSpace(TPSN,"PSN","   Product Serial Number String",		10,	OPSN,	OPSNL,	WW));
	AddSpace(New CSpace( "PSG", "   Product Sub-Group",					39983,		WW));
	AddSpace(New CSpace( "PSS", "   Pulse Input Source Select",				41003,		WW));
	AddSpace(New CSpace( "PMFB","   Pulse Mode Filter Band Width",				41011,		WW));
	AddSpace(New CSpace( "PMFE","   Pulse Mode Filter Enable",				41010,		WW));
	AddSpace(New CSpace( "PMMA","   Pulse Mode Max Follower Acceleration",			41005,		LL));
	AddSpace(New CSpace( "PMRX","   Pulse Mode Recovery Distance",				32003,		LL));
	AddSpace(New CSpace( "PMRE","   Pulse Mode Recovery Distance Enable",			41009,		WW));

	AddSpace(New CSpace( "RN2R","   Read NVM to RAM",					1003,		BB));
	AddSpace(New CSpace(RGO,"RGO", "   Registration Offset (43010+25*n)",		10,	0,	15,	LL));
	AddSpace(New CSpace( "RS1O","   Registration Sensor 1 Active Off",			147,		BB));
	AddSpace(New CSpace( "RS1A","   Registration Sensor 1 Always Active",			275,		BB));
	AddSpace(New CSpace( "RS1M","   Registration Sensor 1 Mapping",				40219,		WW));
	AddSpace(New CSpace( "RS1S","   Registration Sensor 1 Status",				10083,		BB));
	AddSpace(New CSpace( "RS2O","   Registration Sensor 2 Active Off",			148,		BB));
	AddSpace(New CSpace( "RS2A","   Registration Sensor 2 Always Active",			276,		BB));
	AddSpace(New CSpace( "RS2M","   Registration Sensor 2 Mapping",				40220,		WW));
	AddSpace(New CSpace( "RS2S","   Registration Sensor 2 Status",				10084,		BB));
	AddSpace(New CSpace( "RSL", "   Response",						42024,		WW));
	AddSpace(New CSpace( "RPE", "   Rollover Enable Motion",				41028,		WW));
	AddSpace(New CSpace( "RPO", "   Rollover Position",					41026,		LL));

	AddSpace(New CSpace( "SDC", "   Segment Display Character",				30002,		WW));
	AddSpace(New CSpace( "SECC","   Setup of End of Chaining Count",			40471,		WW));
	AddSpace(New CSpace( "SEIC","   Setup of End of Index Count",				40469,		WW));
	AddSpace(New CSpace( "SERL","   Setup of Registration Limit Dist Hit",			40470,		WW));
	AddSpace(New CSpace( "SPR", "   Shunt Power RMS",					32032,		WW));
	AddSpace(New CSpace( "STCC","   Status of End of Chaining Count",			10117,		BB));
	AddSpace(New CSpace( "STIC","   Status of End of Index Count",				10115,		BB));
	AddSpace(New CSpace( "STRL","   Status of Registration Limit Dist Hit",			10116,		BB));
	AddSpace(New CSpace( "STOP","   Stop All Motion",					1151,		BB));
	AddSpace(New CSpace( "SDEC","   Stop Deceleration",					41201,		LL));

	AddSpace(New CSpace( "TQC", "   Torque Command",					32034,		WW));
	AddSpace(New CSpace( "TCA", "   Torque Command Actual",					32035,		WW));
	AddSpace(New CSpace( "T1L", "   Torque Level 1 (MSTL1)",				42049,		WW));
	AddSpace(New CSpace( "T2L", "   Torque Level 2 (MSTL2)",				42050,		WW));
	AddSpace(New CSpace( "TQL", "   Torque Limit",						42034,		WW));
	AddSpace(New CSpace( "TMV", "   Torque Mode Velocity Limit Enable",			40609,		WW));
	AddSpace(New CSpace( "TLD", "   Travel Limit Deceleration",				41203,		LL));
	AddSpace(New CSpace( "TLFN","   Travel Limit - Fault Count",				40708,		WW));
	AddSpace(New CSpace( "TLFP","   Travel Limit + Fault Count",				40707,		WW));

	AddSpace(New CSpace( "UPS", "   Update Predefined Setup",				1004,		BB));
	AddSpace(New CSpace( "UADP","   User Accel Decimal Point",				41024,		WW));
	AddSpace(New CSpace( "UDBM","   User Defined Bitmap",					49401,		LL));
	AddSpace(New CSpace(UDB,"UDB", "   User Defined Bits",				10,	9951,	9982,	BB));
	AddSpace(New CSpace(TUDM,"UDM","   User Defined Motor Name String",		10,	OUDM,	OUDML,	WW));
	AddSpace(New CSpace(UDR,"UDR", "   User Defined Registers",			10,	49403,	49418,	WW));
	AddSpace(New CSpace( "UTB", "   User Time Base",					41025,		WW));
	AddSpace(New CSpace( "UDP", "   User Unit Decimal Point",				41022,		WW));
	AddSpace(New CSpace( "UUD", "   User Unit Rev",						41021,		WW));
	AddSpace(New CSpace(TUUS,"UUS","   User Unit String",				10,	OUUS,	OUUSL,	WW));
	AddSpace(New CSpace( "UVDP","   User Velocity Decimal Point",				41023,		WW));

	AddSpace(New CSpace( "VCM", "   Velocity Command",					32061,		LL));
	AddSpace(New CSpace( "VFB", "   Velocity FeedBack",					32021,		LL));

	


	AddSpace(New CSpace( "WRIO","   Wait for Run Index Active Off",				149,		BB));
	AddSpace(New CSpace( "WRIA","   Wait for Run Index Always Active",			277,		BB));
	AddSpace(New CSpace( "WRIM","   Wait for Run Index Mapping",				40221,		WW));
	AddSpace(New CSpace( "WRIS","   Wait for Run Index Status",				10085,		BB));
	AddSpace(New CSpace( "WE",  "   Warmstart Execute",					1001,		BB));
	AddSpace(New CSpace( "WR2N","   Write RAM to NVM",					1002,		WW));

	m_fIBLoaded = TRUE;

	}

// EP-I specific
void CEmersonEPDriver::AddSpacesI(void)
{
	if( m_fILoaded ) return;

	AddSpace(New CSpace( "BOS", "   Back Off Sensor Before Homing",				41116,		WW));
	AddSpace(New CSpace( "EOH", "   End Of Home Position",					41108,		LL));
	AddSpace(New CSpace( "HAC", "   Home Acceleration",					41104,		LL));
	AddSpace(New CSpace( "HCI", "   Home Chain to Index",					41118,		WW));
	AddSpace(New CSpace( "HCIE","   Home Chain to Index Enable",				41117,		WW));
	AddSpace(New CSpace( "HDC", "   Home Deceleration",					41106,		LL));
	AddSpace(New CSpace( "HLD", "   Home Limit Distance",					41113,		LL));
	AddSpace(New CSpace( "HLDE","   Home Limit Distance Enable",				41115,		WW));
	AddSpace(New CSpace( "HOF", "   Home Offset",						41110,		LL));
	AddSpace(New CSpace( "HOE", "   Home Offset Enable",					41112,		WW));
	AddSpace(New CSpace( "HOR", "   Home Reference",					41101,		WW));
	AddSpace(New CSpace( "HOS", "   Home Type",						41119,		WW));
	AddSpace(New CSpace( "HOV", "   Home Velocity",						41102,		LL));

	m_fILoaded = TRUE;

	}

// EP-B specific
void CEmersonEPDriver::AddSpacesB(void)
{
	if( m_fBLoaded ) return;

	AddSpace(New CSpace( "V0P", "   Velocity Preset 0",					41101,		LL));
	AddSpace(New CSpace( "V0AD","   Velocity Preset 0 Accel/Decel",				41103,		LL));
	AddSpace(New CSpace( "V1P", "   Velocity Preset 1",					41105,		LL));
	AddSpace(New CSpace( "V1AD","   Velocity Preset 1 Accel/Decel",				41107,		LL));
	AddSpace(New CSpace( "V2P", "   Velocity Preset 2",					41109,		LL));
	AddSpace(New CSpace( "V2AD","   Velocity Preset 2 Accel/Decel",				41111,		LL));
	AddSpace(New CSpace( "V3P", "   Velocity Preset 3",					41113,		LL));
	AddSpace(New CSpace( "V3AD","   Velocity Preset 3 Accel/Decel",				41115,		LL));
	AddSpace(New CSpace( "V4P", "   Velocity Preset 4",					41117,		LL));
	AddSpace(New CSpace( "V4AD","   Velocity Preset 4 Accel/Decel",				41119,		LL));
	AddSpace(New CSpace( "V5P", "   Velocity Preset 5",					41121,		LL));
	AddSpace(New CSpace( "V5AD","   Velocity Preset 5 Accel/Decel",				41123,		LL));
	AddSpace(New CSpace( "V6P", "   Velocity Preset 6",					41125,		LL));
	AddSpace(New CSpace( "V6AD","   Velocity Preset 6 Accel/Decel",				41127,		LL));
	AddSpace(New CSpace( "V7P", "   Velocity Preset 7",					41129,		LL));
	AddSpace(New CSpace( "V7AD","   Velocity Preset 7 Accel/Decel",				41131,		LL));

	m_fBLoaded = TRUE;

	}

// End of File
