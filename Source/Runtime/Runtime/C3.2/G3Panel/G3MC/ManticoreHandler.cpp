
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "ManticoreHandler.hpp"

#include "Crc16Ccitt.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Manticore Rack Handler
//

// Instantiator

IRackHandler * Create_ManticoreHandler(void)
{
	return New CManticoreHandler;
}

// Constructor

CManticoreHandler::CManticoreHandler(void)
{
	StdSetRef();

	m_pPort  = NULL;

	m_pDone  = Create_ManualEvent();

	m_uRxMsg = msgNone;

	m_uTxMsg = msgNone;

	m_fBusy  = FALSE;

	m_bSeq   = 0;
}

// Destructor

CManticoreHandler::~CManticoreHandler(void)
{
	if( m_pPort ) {

		m_pPort->Close();
	}

	m_pDone->Release();
}

// IUnknown

HRESULT CManticoreHandler::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IRackHandler);

	StdQueryInterface(IRackHandler);

	StdQueryInterface(IPortHandler);

	return E_NOINTERFACE;
}

ULONG CManticoreHandler::AddRef(void)
{
	StdAddRef();
}

ULONG CManticoreHandler::Release(void)
{
	StdRelease();
}

// IRackHandler

UINT CManticoreHandler::Transact(BYTE bDrop, PBYTE pData, UINT uCount)
{
	if( uCount <= constDataSize ) {

		m_bTxBuff[4] = BYTE(uCount);

		memcpy(m_bTxBuff + 5, pData, uCount);

		return Transact(bDrop, msgFrame);
	}

	return msgError;
}

UINT CManticoreHandler::Transact(BYTE bDrop, UINT uMsg)
{
	m_bDrop  = bDrop;

	m_uTxMsg = uMsg;

	Exchange();

	return m_uRxMsg;
}

BOOL CManticoreHandler::GetReply(BYTE bDrop, PBYTE pData)
{
	if( m_uRxMsg == msgFrame ) {

		memcpy(pData, m_bRxBuff + 5, m_bRxBuff[4]);

		return TRUE;
	}

	return FALSE;
}

BOOL CManticoreHandler::Recycle(BYTE bDrop)
{
	return TRUE;
}

// IPortHandler

void CManticoreHandler::Bind(IPortObject *pPort)
{
	m_pPort = pPort;
}

void CManticoreHandler::OnOpen(CSerialConfig const &Config)
{
}

void CManticoreHandler::OnClose(void)
{
}

void CManticoreHandler::OnTimer(void)
{
}

BOOL CManticoreHandler::OnTxData(BYTE &bData)
{
	if( m_fBusy ) {

		if( m_uTxPtr < m_uTxReq ) {

			bData = m_bTxBuff[m_uTxPtr++];

			return TRUE;
		}
	}

	return FALSE;
}

void CManticoreHandler::OnTxDone(void)
{
	if( m_fBusy ) {

		if( m_uTxMsg == msgAck || m_uTxMsg == msgNak ) {

			Done(msgNone);

			return;
		}
	}
}

void CManticoreHandler::OnRxData(BYTE bData)
{
	if( m_fBusy ) {

		if( m_uRxPtr < elements(m_bRxBuff) ) {

			m_bRxBuff[m_uRxPtr++] = bData;

			return;
		}
	}
}

void CManticoreHandler::OnRxDone(void)
{
	if( m_fBusy ) {

		if( m_uRxPtr < 6 ) {

			return;
		}

		if( m_bRxBuff[0] != SOH || m_bRxBuff[1] != m_bDrop || m_bRxBuff[2] != m_bSeq ) {

			m_fBusy = FALSE;

			Done(msgError);

			return;
		}

		if( m_bRxBuff[3] == STX ) {

			UINT uCount = m_bRxBuff[4] + 7;

			if( m_uRxPtr >= uCount ) {

				m_fBusy = FALSE;

				if( TestSum(m_bRxBuff, uCount-2) ) {

					Done(msgFrame);

					return;
				}

				Done(msgError);

				return;
			}

			return;
		}

		m_fBusy = FALSE;

		if( TestSum(m_bRxBuff, 4) ) {

			if( m_bRxBuff[3] == ACK ) {

				Done(msgAck);

				return;
			}

			if( m_bRxBuff[3] == NAK ) {

				Done(msgNak);

				return;
			}

			if( m_bRxBuff[3] == SYN ) {

				Done(msgBusy);

				return;
			}
		}

		Done(msgError);

		return;
	}
}

// Implementation

void CManticoreHandler::Exchange(void)
{
	m_bTxBuff[0] = SOH;

	m_bTxBuff[1] = m_bDrop;

	m_bTxBuff[2] = ++m_bSeq;

	if( m_uTxMsg == msgFrame ) {

		m_bTxBuff[3] = STX;

		Exchange(5 + m_bTxBuff[4], 50);

		return;
	}

	if( m_uTxMsg == msgAck ) {

		m_bTxBuff[3] = ACK;

		Exchange(4, 5);

		return;
	}

	if( m_uTxMsg == msgNak ) {

		m_bTxBuff[3] = NAK;

		Exchange(4, 5);

		return;
	}

	if( m_uTxMsg == msgEnq ) {

		m_bTxBuff[3] = ENQ;

		Exchange(4, 5);

		return;
	}
}

void CManticoreHandler::Exchange(UINT uCount, UINT uTime)
{
	uCount += AddSum(uCount);

	#if defined(_XDEBUG)

	AfxTrace("TX : ");

	AfxDump(m_bTxBuff, uCount);

	#endif

	m_fBusy = TRUE;

	m_pDone->Clear();

	StartRx();

	StartTx(uCount);

	if( m_pDone->Wait(uTime) ) {

		#if defined(_XDEBUG)

		AfxTrace("RX : ");

		AfxDump(m_bRxBuff, m_uRxPtr);

		#endif
	}

	else {
		m_fBusy = FALSE;

		#if defined(_XDEBUG)

		AfxTrace("RX : Timeout %dmS\n", uTime);

		#endif

		Done(msgNone);
	}
}

void CManticoreHandler::Done(UINT uMsg)
{
	m_uTxMsg = msgNone;

	m_uRxMsg = uMsg;

	m_pDone->Set();
}

WORD CManticoreHandler::CalcSum(PCBYTE pData, UINT uCount)
{
	CCrc16Ccitt Crc;

	Crc.Preset();

	Crc.Add(pData, uCount);

	return Crc.GetValue();
}

BOOL CManticoreHandler::TestSum(PCBYTE pData, UINT uCount)
{
	WORD wCrc = CalcSum(pData, uCount);

	if( pData[uCount + 0] != HIBYTE(wCrc) ) {

		return FALSE;
	}

	if( pData[uCount + 1] != LOBYTE(wCrc) ) {

		return FALSE;
	}

	return TRUE;
}

UINT CManticoreHandler::AddSum(UINT uCount)
{
	WORD wCrc = CalcSum(m_bTxBuff, uCount);

	m_bTxBuff[uCount + 0] = HIBYTE(wCrc);

	m_bTxBuff[uCount + 1] = LOBYTE(wCrc);

	return sizeof(WORD);
}

void CManticoreHandler::StartRx(void)
{
	m_uRxPtr = 0;
}

void CManticoreHandler::StartTx(UINT uCount)
{
	m_uTxReq = uCount;

	m_uTxPtr = 1;

	m_pPort->Send(m_bTxBuff[0]);
}

// End of File
