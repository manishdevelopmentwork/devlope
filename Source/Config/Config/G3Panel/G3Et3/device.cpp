
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Comms Device List
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsDeviceList, CItemIndexList);

// Constructor

CEt3CommsDeviceList::CEt3CommsDeviceList(void)
{
	m_Class = AfxRuntimeClass(CEt3CommsDevice);
	}

// Item Access

CEt3CommsDevice * CEt3CommsDeviceList::GetItem(INDEX Index) const
{
	return (CEt3CommsDevice *) CItemIndexList::GetItem(Index);
	}

CEt3CommsDevice * CEt3CommsDeviceList::GetItem(UINT uPos) const
{
	return (CEt3CommsDevice *) CItemIndexList::GetItem(uPos);
	}

CEt3CommsDevice * CEt3CommsDeviceList::GetItem(CString Name) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEt3CommsDevice *pDevice = GetItem(n);

		if( pDevice->m_Name == Name ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

// Item Lookup

CEt3CommsDevice * CEt3CommsDeviceList::FindDevice(UINT uDevice) const
{
	INDEX n = GetHead();

	while( !Failed(n) ) {

		CEt3CommsDevice *pDevice = GetItem(n);

		if( pDevice->m_Number == uDevice ) {

			return pDevice;
			}

		GetNext(n);
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// Comms Device Page
//

class CEt3CommsDevicePage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CEt3CommsDevicePage(CEt3CommsDevice *pPort);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CEt3CommsDevice * m_pDevice;

		// Implementation
		void LoadOptions(IUICreate *pView);
		void LoadButtons(IUICreate *pView);
	};

//////////////////////////////////////////////////////////////////////////
//
// Communications Device Page
//

// Runtime Class

AfxImplementRuntimeClass(CEt3CommsDevicePage, CUIStdPage);

// Constructor

CEt3CommsDevicePage::CEt3CommsDevicePage(CEt3CommsDevice *pDevice)
{
	m_Class = AfxRuntimeClass(CEt3CommsDevice);

	m_pDevice = pDevice;
	}

// Operations

BOOL CEt3CommsDevicePage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	pView->StartGroup(CString(IDS_OPTIONS), 1);

	pView->AddUI(pItem, L"root", L"Station");

	CEt3CommsPort *pPort = m_pDevice->GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {

		pView->AddUI(pItem, L"root", L"IPAddr");

		/*pView->AddUI(pItem, L"root", L"IPPort");*/
		}

	pView->EndGroup(TRUE);

	LoadButtons(pView);

	pView->NoRecycle();

	return TRUE;
	}

// Implementation

void CEt3CommsDevicePage::LoadButtons(IUICreate *pView)
{
	pView->StartGroup(CString(IDS_DEVICE_COMMANDS), 1);

	pView->AddButton( CString(IDS_ADD_ANALOG),
			  CString(IDS_FORMAT),
			  L"ButtonAddAnalog"
			  );

	pView->AddButton( CString(IDS_ADD_DISCRETE),
			  CString(IDS_FORMAT),
			  L"ButtonAddDiscrete"
			  );

	pView->EndGroup(FALSE);
	}

//////////////////////////////////////////////////////////////////////////
//
// Communications Device
//

// Dynamic Class

AfxImplementDynamicClass(CEt3CommsDevice, CEt3UIItem);

// Constructor

CEt3CommsDevice::CEt3CommsDevice(void)
{
	m_Name       = L"NAME?";

	m_Number     = 0;

	m_Station     = 0;

	m_IPAddr      = 0;

	m_IPPort      = 0;

	m_pBlocks = New CTransferBlockList;
	}

// UI Creation

BOOL CEt3CommsDevice::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CEt3CommsDevicePage(this));
	
	return FALSE;
	}

// UI Update

void CEt3CommsDevice::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() ) {
		
		DoEnables(pHost);
		}

	if( Tag == "ButtonAddAnalog" ) {
		
		afxMainWnd->PostMessage(WM_COMMAND, IDM_TRANSFER_NEW_ANALOG);
		}

	if( Tag == "ButtonAddDiscrete" ) {
		
		afxMainWnd->PostMessage(WM_COMMAND, IDM_TRANSFER_NEW_DISCRETE);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Item Location

CEt3CommsDeviceList * CEt3CommsDevice::GetParentList(void) const
{
	return (CEt3CommsDeviceList *) GetParent(1);
	}

CEt3CommsPort * CEt3CommsDevice::GetParentPort(void) const
{
	return (CEt3CommsPort *) GetParent(2);
	}

// Persistance

void CEt3CommsDevice::Init(void)
{
	CEt3UIItem::Init();

	AllocNumber();

	m_Name   = CString(IDS_DEV) + CPrintf(L"%u", m_Number);

	CEt3CommsPort *pPort = GetParentPort();

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortNetwork)) ) {
		
		m_IPPort = ((CEt3CommsPortNetwork *) pPort)->m_Port;
		}

	if( pPort->IsKindOf(AfxRuntimeClass(CEt3CommsPortSerial)) ) {
		}
	}

// Attributes

UINT CEt3CommsDevice::GetTreeImage(void) const
{
	return IDI_DEVICE;
	}

// Meta Data Creation

void CEt3CommsDevice::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddString (Name);
	Meta_AddInteger(Number);
	Meta_AddInteger(Station);
	Meta_AddInteger(IPAddr);
	Meta_AddInteger(IPPort);
	Meta_AddCollect(Blocks);

	Meta_SetName((IDS_COMMS_DEVICE));
	}

// Implementation

BOOL CEt3CommsDevice::AllocNumber(void)
{
	if( !m_Number ) {

		CEt3CommsManager *pManager = (CEt3CommsManager *) GetParent(AfxRuntimeClass(CEt3CommsManager));

		m_Number = pManager->AllocDeviceNumber();

		return TRUE;
		}

	return FALSE;
	}

void CEt3CommsDevice::DoEnables(IUIHost *pHost)
{
	BOOL fEnable = m_pSystem->GetTransfer() < 32;

	pHost->EnableUI("ButtonAddAnalog",   fEnable);
	pHost->EnableUI("ButtonAddDiscrete", fEnable);
	}

// End of File

