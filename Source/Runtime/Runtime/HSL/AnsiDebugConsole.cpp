
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#include "AnsiDebugConsole.hpp"

////////////////////////////////////////////////////////////////////////
//
// ANSI Terminal Debug Console
//

// Constructor

CAnsiDebugConsole::CAnsiDebugConsole(IDiagConsole *pConsole, bool fExit, bool fFull)
{
	m_pConsole = pConsole;

	m_fExit    = fExit;

	m_fFull    = fFull;

	m_pDiag    = NULL;

	AfxGetObject("diagmanager", 0, IDiagManager, m_pDiag);

	m_uCon     = m_pConsole ? m_pDiag->RegisterConsole(m_pConsole) : 0;

	m_uScroll  = NOTHING;

	m_uPtr     = 0;

	m_pUtils   = NULL;

	AfxGetObject("aeon.filesupport", 0, IFileUtilities, m_pUtils);
}

// Destructor

CAnsiDebugConsole::~CAnsiDebugConsole(void)
{
	if( m_pConsole ) {

		m_pDiag->RevokeConsole(m_uCon);

		m_pDiag->Release();

		m_pDiag = NULL;
	}

	AfxRelease(m_pUtils);
}

// Operations

void CAnsiDebugConsole::Hello(void)
{
	if( m_pConsole ) {

		m_pDiag->RunCommand(m_pConsole, "hello");

		CrLf();
	}

	m_uState = 0;

	m_uPtr   = 0;

	m_sLine[m_uPtr] = 0;

	ShowPrompt();
}

bool CAnsiDebugConsole::OnChar(char cData)
{
	if( cData ) {

		if( m_uState == 1 ) {

			if( cData == 0x1B ) {

				// Double Escape

				if( WipeLine(m_sLine, m_uPtr) ) {

					m_uPtr   = 0;

					m_sLine[m_uPtr] = 0;
				}

				if( m_fExit && !m_pConsole ) {

					Print("exit");

					CrLf();

					return true;
				}
			}

			if( cData == '[' ) {

				m_uState = 2;

				return false;
			}

			m_uState = 0;
		}

		if( m_uState == 0 ) {

			if( cData == 0x1B ) {

				m_uState = 1;

				return false;
			}

			if( cData == '.' && m_uPtr == 0 ) {

				// Dot at Start

				if( m_History.GetCount() ) {

					strcpy(m_sLine, m_History[m_History.GetCount()-1]);

					Print(m_sLine);

					m_uPtr = strlen(m_sLine);

					cData    = 0x0D;
				}
			}

			if( cData >= 32 && cData < 127 ) {

				// Insert Character

				if( strlen(m_sLine) < sizeof(m_sLine) - 2 ) {

					if( !m_sLine[m_uPtr] ) {

						if( m_fFull ) {

							Print(cData);
						}

						m_sLine[m_uPtr++] = cData;

						m_sLine[m_uPtr] = 0;
					}
					else {
						if( m_fFull ) {

							memmove(m_sLine + m_uPtr + 1, m_sLine + m_uPtr, strlen(m_sLine + m_uPtr) + 1);

							m_sLine[m_uPtr] = cData;

							Print(m_sLine + m_uPtr);

							m_uPtr++;

							for( UINT n = strlen(m_sLine + m_uPtr); n; n-- ) {

								Print('\010');
							}
						}
					}
				}
			}

			if( cData == 0x08 || cData == 0x7F ) {

				// Backspace

				if( m_uPtr ) {

					if( !m_sLine[m_uPtr] ) {

						m_sLine[--m_uPtr] = 0;

						if( m_fFull ) {

							Backspace();
						}
					}
					else {
						if( m_fFull ) {

							memmove(m_sLine + m_uPtr - 1, m_sLine + m_uPtr, strlen(m_sLine + m_uPtr) + 1);

							m_uPtr--;

							Print('\010');

							Print(m_sLine + m_uPtr);

							Print(" ");

							for( UINT n = strlen(m_sLine + m_uPtr) + 1; n; n-- ) {

								Print('\010');
							}
						}
					}
				}
			}

			if( cData == 0x09 ) {

				// Tab

				if( m_fFull ) {

					if( strlen(m_sLine) ) {

						UINT uLen = strlen(m_sLine);

						PTXT pGap = strrchr(m_sLine, ' ');

						if( !pGap ) {

							if( m_pDiag->CompleteCommand(m_sLine, m_uPtr) ) {

								strcat(m_sLine, " ");

								WipeLine(uLen, m_uPtr);

								Print(m_sLine);

								m_uPtr = strlen(m_sLine);
							}
						}
						else {
							pGap++;

							UINT uSkip = pGap - m_sLine;

							UINT uRest = m_uPtr - uSkip;

							if( uRest ) {

								if( CompleteFile(pGap, uRest) ) {

									WipeLine(uLen - uSkip, m_uPtr - uSkip);

									Print(pGap);

									m_uPtr = strlen(m_sLine);
								}
							}
						}
					}
				}
			}

			if( cData == 0x0D ) {

				// Enter

				CrLf();

				if( strlen(m_sLine) ) {

					m_uScroll = NOTHING;

					Subsitute(m_sLine);

					CString Input = m_sLine;

					if( m_History.IsEmpty() || m_History[m_History.GetCount()-1] != Input ) {

						m_History.Append(Input);
					}

					if( m_fExit ) {

						if( Input == "exit" ) {

							return true;
						}
					}

					if( Input == "clear" ) {

						if( m_fFull ) {

							Print("\x1B[H\x1B[2J");
						}
					}
					else {
						CrLf();

						m_pDiag->RunCommand(m_pConsole, m_sLine);
					}

					CrLf();
				}

				m_sLine[m_uPtr = 0] = 0;

				ShowPrompt();
			}

			return false;
		}

		if( m_uState == 2 ) {

			if( m_fFull ) {

				if( cData == 'A' ) {

					// Cursor Up

					if( !m_History.IsEmpty() ) {

						if( m_uScroll == NOTHING ) {

							m_uScroll = m_History.GetCount();
						}

						if( m_uScroll > 0 ) {

							WipeLine(m_sLine, m_uPtr);

							strcpy(m_sLine, m_History[--m_uScroll]);

							Print(m_sLine);

							m_uPtr = strlen(m_sLine);
						}
					}
				}

				if( cData == 'B' ) {

					// Cursor Down

					if( m_uScroll < NOTHING ) {

						if( m_uScroll < m_History.GetCount() ) {

							WipeLine(m_sLine, m_uPtr);

							if( m_uScroll++ < m_History.GetCount() - 1 ) {

								strcpy(m_sLine, m_History[m_uScroll]);

								Print(m_sLine);

								m_uPtr = strlen(m_sLine);
							}
							else {
								m_sLine[m_uPtr = 0] = 0;
							}
						}
					}
				}

				if( cData == 'C' ) {

					// Cursor Right

					if( m_sLine[m_uPtr] ) {

						Print(m_sLine[m_uPtr]);

						m_uPtr++;
					}
				}

				if( cData == 'D' ) {

					// Cursor Left

					if( m_uPtr ) {

						Print('\010');

						m_uPtr--;
					}
				}

				if( cData == '3' ) {

					m_uState = 3;

					return false;
				}
			}

			m_uState = 0;
		}

		if( m_uState == 3 ) {

			if( m_fFull ) {

				if( cData == '~' ) {

					// Delete

					if( m_sLine[m_uPtr] ) {

						memmove(m_sLine + m_uPtr, m_sLine + m_uPtr + 1, strlen(m_sLine + m_uPtr + 1) + 1);

						Print(m_sLine + m_uPtr);

						Print(" ");

						for( UINT n = strlen(m_sLine + m_uPtr) + 1; n; n-- ) {

							Print('\010');
						}
					}
				}
			}

			m_uState = 0;
		}
	}

	return false;
}

void CAnsiDebugConsole::ShowPrompt(void)
{
	for( int p = 0; p < 2; p++ ) {

		char s[MAX_PATH];

		if( getcwd(s, sizeof(s)) ) {

			if( !m_pUtils || m_pUtils->IsDiskMounted(s[0]) ) {

				Print(s);

				Print("> ");

				return;
			}

			break;
		}

		chdir("C:\\");
	}

	Print(">>>> ");
}

void CAnsiDebugConsole::CrLf(void)
{
	Print("\r\n");
}

void CAnsiDebugConsole::Backspace(void)
{
	Print("\010 \010");
}

bool CAnsiDebugConsole::WipeLine(PCTXT pLine, UINT uPtr)
{
	return WipeLine(strlen(pLine), uPtr);
}

bool CAnsiDebugConsole::WipeLine(UINT uLen, UINT uPtr)
{
	if( uLen ) {

		while( uPtr < uLen ) {

			Print(" ");

			uPtr++;
		}

		while( uPtr ) {

			Backspace();

			uPtr--;
		}

		return true;
	}

	return false;
}

void CAnsiDebugConsole::Print(PCTXT p)
{
	if( m_pConsole ) {

		m_pConsole->Write(p);

		return;
	}

	#if !defined(NO_SYS_CONSOLE)

	extern void AfxDebug(PCTXT);

	AfxDebug(p);

	#endif
}

void CAnsiDebugConsole::Print(char cData)
{
	if( m_pConsole ) {

		char c[2] = { cData, 0 };

		m_pConsole->Write(c);

		return;
	}

	#if !defined(NO_SYS_CONSOLE)

	extern void AfxDebug(char);

	AfxDebug(cData);

	#endif
}

bool CAnsiDebugConsole::CompleteFile(char *pLine, UINT uSize)
{
	CAutoDirentList List;

	if( List.Scan(".") ) {

		UINT m = 0;

		UINT n = 0;

		for( UINT i = 0; i < List.GetCount(); i++ ) {

			CFilename Name = List[i]->d_name;

			if( uSize < Name.GetLength() ) {

				if( !strncasecmp(pLine, Name, uSize) ) {

					m = i;

					n++;
				}
			}
		}

		if( n == 1 ) {

			strcpy(pLine, List[m]->d_name);

			return true;
		}
	}

	return false;
}

void CAnsiDebugConsole::Subsitute(PTXT pLine)
{
	if( !strcasecmp(pLine, "help") ) {

		strcpy(pLine, "diag.help");
	}
}

// End of File
