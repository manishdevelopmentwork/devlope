
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_EZM_HPP
	
#define	INCLUDE_EZM_HPP 

//////////////////////////////////////////////////////////////////////////
//
// EZ Master Driver Options
//

class CEZMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CEZMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_Group;
		UINT m_Unit;
		
		
	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// EZ TCP/IP Master Driver
//

class CEZMasterDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CEZMasterDriver(void);

		//Destructor
		~CEZMasterDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);

	protected:
		// Implementation
		void AddSpaces(void);
	};

#endif

// End of File