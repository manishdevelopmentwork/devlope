
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASK3IEC_HPP
	
#define	INCLUDE_YASK3IEC_HPP

#include "yaskiec.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP 3000 Enumerations
//

// Spaces

enum {
	SP_9	=  9,
	SP_10	= 10,
	SP_MB	= 29,
	SP_M32  = 30,
	SP_M64	= 31,
	};

// Modbus offsets

enum {
	modIX	= 000001,
	modQX	= 100001,
	modQ	= 300001,
	modM	= 400001,
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP 3000 IEC Device Options
//

class CYaskawaMp3000IecDeviceOptions : public CYaskawaMPIECDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaMp3000IecDeviceOptions(void);

		// Device Access
		CString GetDeviceName(void);

		// Import Operations
		BOOL	ImportTags(FILE *pFile, CString Path, IMakeTags *pTags, CString &Error);
		CString FindFilename(CString Path);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT m_MemWordSwap;

	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Overridables
		void SetIECMax(void);

		// Tag Creation
		void MakeFlag(CString Text, IMakeTags * pTags, CString &Error);
		void MakeNumeric(CString Text, IMakeTags * pTags, CString &Error);

		// Helpers
		CString GetMappingText(CStringArray Array);
		CString	GetErrorText(CString Name, CString Tag);
		void	Validate(CString &String);
		CString ModbusToIEC(CString Modbus, CString Desc);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP 3000 IEC Communications Driver
//

class CYaskawaMp3000IecDriver : public CYaskawaMPIECDriver
{
	public:
		// Constructor
		CYaskawaMp3000IecDriver(void);

		// Configuration
		CLASS GetDeviceConfig(void);

		// Driver Data
		UINT GetFlags(void);

		// Tag Import
		BOOL MakeTags(IMakeTags *pTags, CItem *pConfig, CItem *pDrvCfg, CString DevName);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig ,BOOL fPart);
		BOOL CheckAlignment(CSpace *pSpace);

		// Overridables
		BOOL Is64Bit(UINT uTable);
		UINT IECToModbus(CAddress Addr, UINT uMin, BOOL fIsBit);
		UINT GetIECMinByteNumber(UINT uTable);
		UINT GetIECMaxByteNumber(CAddress Addr);
		UINT GetIBMax(void);

		// Address Helpers
		UINT GetOffset(UINT uAddress, UINT uTable, BOOL fNative);
						
	protected:
		// Implementation
		void AddSpaces(void);

		// Overridables
		UINT GetBitMax(UINT uMin);
		UINT GetWordMax(UINT uMin);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP3000 Series IEC Address Selection
//

class CYaskawaMp3000IecAddrDialog : public CYaskawaMPIECAddrDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CYaskawaMp3000IecAddrDialog(CYaskawaMp3000IecDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart);

	protected:

		// Message Map
		AfxDeclareMessageMap();

		// Command Handler
		BOOL OnButtonClicked(UINT uID);

		// Implementation
		void UpdateAddressField(UINT uID);
		void ShowNativeOffset(UINT uOffset, UINT uTable);
		void ShowModbusOffset(UINT uOffset);

		// Overridables
		void ShowModbus(CAddress Addr);
		void ShowInfo(UINT uTable);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa MP Import Error Dialog
//

class CYaskawaMpImportErrorDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CYaskawaMpImportErrorDialog(CStringArray List);

		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);
		BOOL OnCopy(UINT uID);
					
	protected:

		CStringArray m_Errors;
		
		// Implementation
		void LoadErrorList(void);
	};

// End of File

#endif
