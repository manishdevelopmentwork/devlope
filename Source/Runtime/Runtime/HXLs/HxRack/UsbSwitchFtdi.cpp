
#include "Intern.hpp"

#include "UsbSwitchFtdi.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Rack Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Ftdi Port Switch 
//

IPortSwitch * Create_UsbSwitchFtdi(IUsbHostFuncDriver *pDriver, UINT uType, UINT uBit)
{
	CUsbSwitchFtdi *p = New CUsbSwitchFtdi(pDriver, uType, uBit);

	return p;
	}

// Constructor

CUsbSwitchFtdi::CUsbSwitchFtdi(IUsbHostFuncDriver *pDriver, UINT uType, UINT uBit)
{
	StdSetRef();

	m_pDriver = (IUsbHostFtdi *) pDriver;

	m_uType   = uType;

	m_uBit    = uBit;

	m_pDriver->AddRef();

	Init();
	}

// Destructor

CUsbSwitchFtdi::~CUsbSwitchFtdi(void)
{
	m_pDriver->Release();
	}

// IUnknown

HRESULT METHOD CUsbSwitchFtdi::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPortSwitch);

	StdQueryInterface(IPortSwitch);

	return E_NOINTERFACE;
	}

ULONG METHOD CUsbSwitchFtdi::AddRef(void)
{
	StdAddRef();
	}

ULONG METHOD CUsbSwitchFtdi::Release(void)
{
	StdRelease();
	}

// IPortSwitch

UINT METHOD CUsbSwitchFtdi::GetCount(UINT uUnit)
{
	return 1;
	}

UINT METHOD CUsbSwitchFtdi::GetMask(UINT uUnit)
{
	switch( m_uType ) {

		case physicalRS232:
			return Bit(physicalRS232);

		case physicalRS485:
			return Bit(physicalRS485) | Bit(physicalRS422Master) | Bit(physicalRS422Slave);
	}

	return physicalNone;
	}

UINT METHOD CUsbSwitchFtdi::GetType(UINT uUnit, UINT uLog)
{
	return m_uType;
	}

BOOL METHOD CUsbSwitchFtdi::EnablePort(UINT uPort, BOOL fEnable)
{
	return FALSE;
	}

BOOL METHOD CUsbSwitchFtdi::SetPhysical(UINT uPort, BOOL fRS485)
{
	return FALSE;
	}

BOOL METHOD CUsbSwitchFtdi::SetFull(UINT uPort, BOOL fFull)
{
	if( m_uType == physicalRS485 ) {

		BYTE bData = m_pDriver->GetInputLo();

		if( fFull ) {

			bData &= ~Bit(m_uBit);
		}
		else {
			bData |= Bit(m_uBit);
		}

		m_pDriver->SetOutputLo(0xFF, bData);
	}

	return TRUE;
	}

BOOL METHOD CUsbSwitchFtdi::SetMode(UINT uPort, BOOL fAuto)
{
	return FALSE;
	}

// Implementation

void CUsbSwitchFtdi::Init(void)
{
	m_pDriver->SetBitMode(modeOff, 0);

	m_pDriver->SetBitMode(modeMPSSE, 0);

	m_pDriver->SetOutputLo(0xFF, 0x00);
	}

// End of File
