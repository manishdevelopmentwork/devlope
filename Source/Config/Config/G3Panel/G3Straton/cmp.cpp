
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Control Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Straton Compiler
//

// Static Data

CStratonCompiler * CStratonCompiler::m_pThis = NULL;

// Object Location

CStratonCompiler * CStratonCompiler::FindInstance(void)
{
	AfxAssert(m_pThis);
	
	return m_pThis;
	}

// Constructors

CStratonCompiler::CStratonCompiler(void)
{
	AfxAssert(m_pThis == NULL);

	LoadLib(L"K5Cmp");

	m_pThis = this;
	}

// Operations

BOOL CStratonCompiler::NeedBuild(PCTXT pPath)
{
	return m_pfnNeedBuild( LPCSTR(CAnsiString(pPath)), 
			       0
			       );
	}

void CStratonCompiler::CleanProject(PCTXT pPath)
{
	m_pfnCleanProject(LPCSTR(CAnsiString(pPath)));
	}

BOOL CStratonCompiler::BuildDefault(PCTXT pPath, HWND hWnd)
{
	return m_pfnBuildDefault( LPCSTR(CAnsiString(pPath)), 
				  hWnd
				  );
	}

BOOL CStratonCompiler::BuildDefaultFileReport(PCTXT pPath, PCTXT pFile)
{
	return m_pfnBuildDefaultFileReport( LPCSTR(CAnsiString(pPath)), 
					    LPCSTR(CAnsiString(pFile))
					    );
	}

BOOL CStratonCompiler::CheckProgram(PCTXT pPath, PCTXT pName, int iCodeType, HWND hWnd)
{
	return m_pfnCheckProgram( LPCSTR(CAnsiString(pPath)), 
				  LPCSTR(CAnsiString(pName)),
				  iCodeType,
				  hWnd,
				  NULL
				  );
	}

BOOL CStratonCompiler::CheckProgram(PCTXT pPath, PCTXT pName, int iCodeType, HWND hWnd, FILE *pFile)
{
	return m_pfnCheckProgram( LPCSTR(CAnsiString(pPath)), 
				  LPCSTR(CAnsiString(pName)),
				  iCodeType,
				  hWnd,
				  pFile
				  );
	}

BOOL CStratonCompiler::BuildOneUDFB(PCTXT pPath, PCTXT pName, int iCodeType, HWND hWnd, FILE *pFile)
{
	return m_pfnBuildOneUDFB( LPCSTR(CAnsiString(pPath)), 
				  LPCSTR(CAnsiString(pName)),
				  iCodeType,
				  hWnd,
				  pFile
				  );
	}

BOOL CStratonCompiler::CheckDefFile(PCTXT pPath)
{
	return m_pfnCheckDefFile( LPCSTR(CAnsiString(pPath)) );
	}

BOOL CStratonCompiler::ConversionNeedBuild(PCTXT pPath, PCTXT pName, DWORD dwLanguage)
{
	return m_pfnConversionNeedBuild( LPCSTR(CAnsiString(pPath)), 
				         LPCSTR(CAnsiString(pName)),
				         dwLanguage
				         );
	}

BOOL CStratonCompiler::CanConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage)
{
	return m_pfnCanConvertProgram( LPCSTR(CAnsiString(pPath)), 
				       LPCSTR(CAnsiString(pName)),
				       dwLanguage
				       );
	}

BOOL CStratonCompiler::ConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage)
{
	return m_pfnConvertProgram( LPCSTR(CAnsiString(pPath)), 
				    LPCSTR(CAnsiString(pName)),
				    dwLanguage,
				    0
				    );
	}

BOOL CStratonCompiler::SLCheck(UINT &uNbIO, CString &Oem)
{
	SetCurrentDirectory(afxModule->GetFilename().GetDirectory());

	char sOem[16] = { 0 };

	DWORD  dwNbIO = 0;

	BOOL   fCheck = m_pfnSLCheck(&dwNbIO, sOem);

	uNbIO = dwNbIO;

	Oem   = CString(sOem);

	return fCheck;
	}

BOOL CStratonCompiler::BuildProject(PCTXT pPath, PCTXT pTarg, PCTXT pCode, BOOL fIntelEndian, HWND hWnd, BOOL fTraceFile)
{
	return m_pfnBuildProject( LPCSTR(CAnsiString(pPath)),
				  0,
				  LPCSTR(CAnsiString(pTarg)),
				  LPCSTR(CAnsiString(pCode)),
				  fIntelEndian,
				  hWnd,
				  NULL,
				  fTraceFile
				  );
	}

CString CStratonCompiler::ExportVariables(PCTXT pPath, PCTXT pGroup, DWORD dwSyntax, DWORD dwFlags)
{
	LPCSTR pExport = m_pExportVariables( LPCSTR(CAnsiString(pPath)),
					     LPCSTR(CAnsiString(pGroup)), 
					     dwSyntax,
					     dwFlags
					     );

	if( pExport ) {
		
		return CString(pExport);
		}

	return CString();
	}

BOOL CStratonCompiler::ImportVariables(PCTXT pPath, PCTXT pGroup, PCTXT pInput, PCTXT pError, DWORD dwSyntax, DWORD dwFlags)
{
	return m_pImportVariables( LPCSTR(CAnsiString(pPath)),
				   LPCSTR(CAnsiString(pGroup)), 
				   LPCSTR(CAnsiString(pInput)),
				   LPCSTR(CAnsiString(pError)),
				   dwSyntax,
				   dwFlags
				   );
	}

// Library Management

void CStratonCompiler::LoadLib(PCTXT pName)
{
	CFilename Path = afxModule->GetFilename().GetDirectory();

	CFilename Name = Path + L"Straton\\" + pName;

	if( (m_hLib = LoadLibrary(Name)) ) {

		m_pfnNeedBuild			= (PFK5CMP_NeedBuild)		   GetProcAddress(m_hLib,	"K5CmpNeedBuild");
		m_pfnCleanProject		= (PFK5CMP_CleanProject)	   GetProcAddress(m_hLib,	"K5CmpCleanProject");
		m_pfnBuildDefault		= (PFK5CMP_BuildDefault)	   GetProcAddress(m_hLib,	"K5CmpBuildDefault");
		m_pfnBuildDefaultFileReport	= (PFK5CMP_BuildDefaultFileReport) GetProcAddress(m_hLib,	"K5CmpBuildDefaultFileReport");
		m_pfnCheckProgram		= (PFK5CMP_CheckProgram)	   GetProcAddress(m_hLib,	"K5CmpCheckProgram");
		m_pfnBuildOneUDFB		= (PFK5CMP_BuildOneUDFB)	   GetProcAddress(m_hLib,	"K5CmpBuildOneUDFB");
		m_pfnCheckDefFile		= (PFK5CMP_CheckDefFile)	   GetProcAddress(m_hLib,	"K5CmpCheckDefFile");
		m_pfnConversionNeedBuild	= (PFK5CMP_ConversionNeedBuild)	   GetProcAddress(m_hLib,	"K5CmpCanConvertProgram");
		m_pfnCanConvertProgram		= (PFK5CMP_CanConvertProgram)	   GetProcAddress(m_hLib,	"K5CmpCanConvertProgram");
		m_pfnConvertProgram		= (PFK5CMP_ConvertProgram)	   GetProcAddress(m_hLib,	"K5CmpConvertProgram");
		m_pfnSLCheck			= (PFK5CMP_SLCheck)		   GetProcAddress(m_hLib,	"K5CMP_SLCheck");
		m_pfnBuildProject		= (PFK5CMP_BuildProject)	   GetProcAddress(m_hLib,	"K5CmpBuildProject");
		m_pExportVariables		= (PFK5CMP_ExportVariables)	   GetProcAddress(m_hLib,	"K5CmpExportVariables");
		m_pImportVariables		= (PFK5CMP_ImportVariables)	   GetProcAddress(m_hLib,	"K5CmpImportVariables");
		
		AfxAssert( m_pfnNeedBuild	       && 
			   m_pfnCleanProject	       && 
			   m_pfnBuildDefault	       && 
			   m_pfnBuildDefaultFileReport && 
			   m_pfnCheckProgram	       &&
			   m_pfnBuildOneUDFB	       &&
			   m_pfnCanConvertProgram      &&
			   m_pfnConvertProgram         &&
			   m_pfnSLCheck		       && 
			   m_pfnBuildProject	       &&
			   m_pExportVariables	       &&
			   m_pImportVariables	       );
		
		return;
		}

	DWORD dwError = GetLastError();

	AfxTrace(L"ERROR: Failed to load %s\t%8.8X\n", pName, dwError);

	AfxAssert(m_hLib);
	}

void CStratonCompiler::FreeLib(void)
{
	AfxVerify(FreeLibrary(m_hLib));
	}

//////////////////////////////////////////////////////////////////////////
//
// Straton Compiler APIs
//
global	BOOL	Straton_NeedBuild(PCTXT pPath)
{
	return CStratonCompiler::FindInstance()->NeedBuild(pPath);
	}

global	void	Straton_CleanProject(PCTXT pPath)
{
	CStratonCompiler::FindInstance()->CleanProject(pPath);
	}

global	BOOL	Straton_BuildDefault(PCTXT pPath, HWND hWnd)
{
	return CStratonCompiler::FindInstance()->BuildDefault(pPath, hWnd);
	}

global	BOOL	Straton_BuildDefaultFileReport(PCTXT pPath, PCTXT pFile)
{
	return CStratonCompiler::FindInstance()->BuildDefaultFileReport(pPath, pFile);
	}

global	BOOL	Straton_CheckProgram(PCTXT pPath, PCTXT pName, HWND hWnd)
{
	return CStratonCompiler::FindInstance()->CheckProgram( pPath, 
							       pName, 
							       0, 
							       hWnd
							       );
	}

global	BOOL	Straton_CheckProgram(PCTXT pPath, PCTXT pName, HWND hWnd, FILE *pFile)
{
	return CStratonCompiler::FindInstance()->CheckProgram( pPath, 
							       pName, 
							       0, 
							       hWnd,
							       pFile
							       );
	}

global	BOOL	Straton_BuildOneUDFB(PCTXT pPath, PCTXT pName, HWND hWnd, FILE *pFile)
{
	return CStratonCompiler::FindInstance()->BuildOneUDFB( pPath, 
							       pName, 
							       0, 
							       hWnd,
							       pFile
							       );
	}

global	BOOL	Straton_CheckDefFile(PCTXT pPath)
{
	return CStratonCompiler::FindInstance()->CheckDefFile(pPath);
	}

global	BOOL	Straton_ConversionNeedBuild(PCTXT pPath, PCTXT pName, DWORD dwLanguage)
{
	return CStratonCompiler::FindInstance()->ConversionNeedBuild( pPath, 
								      pName, 
								      dwLanguage
								      );
	}

global	BOOL	Straton_CanConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage)
{
	return CStratonCompiler::FindInstance()->CanConvertProgram( pPath, 
								    pName, 
								    dwLanguage
								    );
	}

global	BOOL	Straton_ConvertProgram(PCTXT pPath, PCTXT pName, DWORD dwLanguage)
{
	return CStratonCompiler::FindInstance()->ConvertProgram( pPath, 
								 pName, 
								 dwLanguage								 
								 );
	}

global	BOOL	Straton_SLCheck(UINT &uNbIO, CString &Oem)
{
	return CStratonCompiler::FindInstance()->SLCheck(uNbIO, Oem);
	}

global	BOOL	Straton_BuildProject(PCTXT pPath, PCTXT pTarg, PCTXT pCode, BOOL fIntelEndian, HWND hWnd, BOOL fTraceFile)
{
	return CStratonCompiler::FindInstance()->BuildProject( pPath, 
							       pTarg, 
							       pCode, 
							       fIntelEndian, 
							       hWnd, 
							       fTraceFile
							       );
	}

global CString Straton_ExportVariables(PCTXT pPath, PCTXT pGroup, DWORD dwSyntax, DWORD dwFlags)
{
	return CStratonCompiler::FindInstance()->ExportVariables( pPath, 
								  pGroup, 
								  dwSyntax, 
								  dwFlags
								  );
	}

global CString Straton_ImportVariables(PCTXT pPath, PCTXT pGroup, PCTXT pInput, PCTXT pError, DWORD dwSyntax, DWORD dwFlags)
{
	return CStratonCompiler::FindInstance()->ImportVariables( pPath, 
								  pGroup,
								  pInput,
								  pError,
								  dwSyntax, 
								  dwFlags
								  );
	}

// End of File
