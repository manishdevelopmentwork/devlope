
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2014 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Upload Dialog Box
//

// Libraries

#pragma	comment(lib, "lz32.lib")

// Runtime Class

AfxImplementRuntimeClass(CLinkUploadDialog, CStdDialog);

// State Machine

enum
{
	stateBootStartProgram,
	stateConfigReadItem,
	stateConfigReadData,
	stateDone,
};

// Constructors

CLinkUploadDialog::CLinkUploadDialog(void)
{
	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fAutoClose = FALSE;

	m_pComms     = New CCommsThread;

	m_fImport    = FALSE;

	SetName(L"LinkUploadDlg");
}

// Destructor

CLinkUploadDialog::~CLinkUploadDialog(void)
{
	delete m_pComms;
}

// Attributes

CString CLinkUploadDialog::GetFilename(void) const
{
	return m_File2;
}

// Message Map

AfxMessageMap(CLinkUploadDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_DEVICECHANGE)

	AfxDispatchCommand(IDOK, OnCommandOkay)
	AfxDispatchCommand(IDINIT, OnCommsInit)
	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDDONE, OnCommsDone)
	AfxDispatchCommand(IDFAIL, OnCommsFail)
	AfxDispatchCommand(IDCRED, OnCommsCred)

	AfxMessageEnd(CLinkUploadDialog)
};

// Message Handlers

BOOL CLinkUploadDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_uState = stateBootStartProgram;

	if( !OpenFile() ) {

		SetError(CString(IDS_LINK_OPEN_TEMP_NOT));

		return TRUE;
	}

	m_pComms->Bind(this);

	m_pComms->Create();

	return TRUE;
}

void CLinkUploadDialog::OnDeviceChange(LONG uEvent, LONG uData)
{
	m_pComms->OnEvent(WM_DEVICECHANGE, uEvent, uData);
}

// Command Handlers

BOOL CLinkUploadDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		#pragma warning(suppress: 6286)

		if( TRUE || NoYes(CString(IDS_LINK_ABORT)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_pComms->Terminate(2000);

			EndDialog(!m_fError);
		}
	}

	else
		EndDialog(FALSE);

	CloseFile();

	DeleteFile(m_File1);

	return TRUE;
}

BOOL CLinkUploadDialog::OnCommsInit(UINT uID)
{
	TxFrame();

	return TRUE;
}

BOOL CLinkUploadDialog::OnCommsDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
	}

	return TRUE;
}

BOOL CLinkUploadDialog::OnCommsFail(UINT uID)
{
	SetError(m_pComms->GetErrorText());

	return TRUE;
}

BOOL CLinkUploadDialog::OnCommsCred(UINT uID)
{
	m_pComms->AskForCredentials();

	return TRUE;
}

// Implementation

void CLinkUploadDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_pComms->Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError ) {

			CloseFile();

			DeleteFile(m_File1);

			EndDialog(TRUE);

			return;
		}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_LINK_CLOSE));
	}
}

void CLinkUploadDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_LINK_REPORT_ERROR, pText));

	GetDlgItem(101).SetWindowText(L"");

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
}

void CLinkUploadDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
}

void CLinkUploadDialog::TxFrame(void)
{
	if( m_uState == stateBootStartProgram ) {

		ShowStatus(CString(IDS_LINK_START_FIRMWARE));

		m_pComms->BootStartProgram();

		return;
	}

	if( m_uState == stateConfigReadItem ) {

		ShowStatus(CString(IDS_LINK_READ_HEADER));

		m_pComms->ConfigReadItem(0);

		return;
	}

	if( m_uState == stateConfigReadData ) {

		UINT uLimit = m_pComms->GetBulkLimit(FALSE);

		ShowStatus(CPrintf(IDS_LINK_READ_DBLOCK, m_uAddr / uLimit + 1));

		m_uCount = min(m_uSize - m_uAddr, uLimit);

		m_pComms->ConfigReadData(0, m_uAddr, m_uCount);

		return;
	}

	ShowStatus(CString(IDS_LINK_OP_OK));

	CloseFile();

	SaveFile();

	SetDone(FALSE);
}

BOOL CLinkUploadDialog::RxFrame(void)
{
	if( m_uState == stateBootStartProgram ) {

		m_uState = stateConfigReadItem;

		return TRUE;
	}

	if( m_uState == stateConfigReadItem ) {

		if( m_uSize = m_pComms->ConfigGetItemSize() ) {

			m_uAddr  = 0;

			m_uState = stateConfigReadData;

			return TRUE;
		}

		SetError(CString(IDS_LINK_NO_UP_IMAGE));

		return FALSE;
	}

	if( m_uState == stateConfigReadData ) {

		PBYTE pData = new BYTE[m_uCount];

		DWORD uSize = m_uCount;

		m_pComms->ConfigGetItemData(pData, m_uCount);

		WriteFile(m_hFile, pData, uSize, &uSize, NULL);

		if( m_uAddr == 0 ) {

			if( pData[0] == 'S' && pData[1] == 'Z' ) {

				if( pData[2] == 'D' && pData[3] == 'D' ) {

					m_fImport = TRUE;
				}
			}
		}

		delete[] pData;

		if( (m_uAddr += m_uCount) == m_uSize ) {

			m_uState = stateDone;
		}

		return TRUE;
	}

	SetError(CString(IDS_LINK_INVALID));

	return FALSE;
}

BOOL CLinkUploadDialog::OpenFile(void)
{
	m_File1.MakeTemporary();

	m_hFile = m_File1.OpenWrite();

	return m_hFile < INVALID_HANDLE_VALUE;
}

BOOL CLinkUploadDialog::CloseFile(void)
{
	if( m_hFile < INVALID_HANDLE_VALUE ) {

		CloseHandle(m_hFile);

		m_hFile = INVALID_HANDLE_VALUE;

		return TRUE;
	}

	return FALSE;
}

BOOL CLinkUploadDialog::SaveFile(void)
{
	for( ;;) {

		CSaveFileDialog Dialog;

		Dialog.SetCaption(CString(IDS_SAVE_UPLOADED));

		// TODO -- Better identify extracted databases!!!

		Dialog.SetFilter(CString(m_fImport ? IDS_IMPORT_FILTER : IDS_FILE_FILTER));

		if( Dialog.Execute(ThisObject) ) {

			m_File2 = Dialog.GetFilename();

			switch( CanOverwrite(m_File2) ) {

				case IDNO:

					continue;

				case IDCANCEL:

					m_File2.Empty();

					return FALSE;
			}

			afxMainWnd->UpdateWindow();

			if( m_File2.Exists() ) {

				if( !DeleteFile(m_File2) ) {

					Error(CString(IDS_UNABLE_TO_WRITE));

					continue;
				}

				afxMainWnd->UpdateWindow();
			}

			if( m_fImport ) {

				OFSTRUCT OF = { 0 };

				int hFile1 = LZOpenFile(PTXT(PCTXT(m_File1)), &OF, OF_READ);

				int hFile2 = LZOpenFile(PTXT(PCTXT(m_File2)), &OF, OF_CREATE | OF_WRITE);

				if( hFile1 < 0 || hFile2 < 0 ) {

					LZClose(hFile1);

					LZClose(hFile2);

					Error(CString(IDS_UNABLE_TO_WRITE));

					continue;
				}

				LZCopy(hFile1, hFile2);

				LZClose(hFile1);

				LZClose(hFile2);
			}
			else {
				if( !MoveFile(m_File1, m_File2) ) {

					Error(CString(IDS_UNABLE_TO_WRITE));

					continue;
				}
			}

			return TRUE;
		}

		m_File2.Empty();

		return FALSE;
	}
}

// End of File
