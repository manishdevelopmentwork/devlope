
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_LinuxSmsClient_HPP

#define	INCLUDE_LinuxSmsClient_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "MsgTransport.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CJsonConfig;

//////////////////////////////////////////////////////////////////////////
//
// SMS Client Service for Linux
//

class CLinuxSmsClient : public CMsgTransport, public IClientProcess
{
public:
	// Constructor
	CLinuxSmsClient(CJsonConfig *pJson);

	// Destructor
	~CLinuxSmsClient(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// IClientProcess
	BOOL METHOD TaskInit(UINT uTask);
	INT  METHOD TaskExec(UINT uTask);
	void METHOD TaskStop(UINT uTask);
	void METHOD TaskTerm(UINT uTask);

	// IMsgTransport
	BOOL METHOD CanAcceptAddress(CString const &Addr);

protected:
	// Data Members
	ULONG			     m_uRefs;
	CList<CMsgPayload *>	     m_Pend;

	// Implementation
	void ApplyConfig(CJsonConfig *pJson);
	void SendMessage(CMsgPayload *pMessage);
	void CheckDone(void);
	bool CheckDone(CMsgPayload *pMessage);
	void CheckRecv(void);
};

// End of File

#endif
