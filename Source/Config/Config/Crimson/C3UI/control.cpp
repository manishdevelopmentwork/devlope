
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// User Interface Element with Controls
//

// Runtime Class

AfxImplementRuntimeClass(CUIControl, CUIElement);

// Constructor

CUIControl::CUIControl(void)
{
	m_uID   = NOTHING;

	m_uCode = NOTHING;
	}

// Attributes 

UINT CUIControl::GetID(void)
{
	return m_uID;
	}

// Core Operations

void CUIControl::AddControl(CCtrlWnd *pCtrl)
{
	m_List.Append(pCtrl);
	}

void CUIControl::AddControl(CCtrlWnd *pCtrl, UINT uCode)
{
	if( uCode < NOTHING ) {
	
		m_uID   = pCtrl->GetID();

		m_uCode = uCode;
		}

	m_List.Append(pCtrl);
	}

// Core Overridables

void CUIControl::OnShow(BOOL fShow)
{
	UINT uCount = m_List.GetCount();

	UINT uCode  = fShow ? SW_SHOW : SW_HIDE;

	for( UINT n = 0; n < uCount; n++ ) {

		CCtrlWnd *pCtrl = m_List[n];

		pCtrl->ShowWindow(uCode);
		}
	}

void CUIControl::OnEnable(BOOL fEnable)
{
	UINT uCount = m_List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CCtrlWnd *pCtrl = m_List[n];

		pCtrl->EnableWindow(fEnable);
		}
	}

BOOL CUIControl::OnFindFocus(CWnd * &pWnd)
{
	UINT uCount = m_List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		CCtrlWnd *pCtrl = m_List[n];

		if( pCtrl->GetWindowStyle() & WS_TABSTOP ) {

			if( pCtrl->IsWindowEnabled() ) {

				pWnd = pCtrl;
				}

			return TRUE;
			}
		}

	return FALSE;
	}

// Notification Handlers

BOOL CUIControl::OnNotify(UINT uID, UINT uCode)
{
	if( uID == m_uID ) {

		if( !m_fBusy ) {
			
			if( uCode == m_uCode ) {

				switch( SaveUI(FALSE) ) {

					case saveChange:

						return TRUE;

					case saveSame:

						return FALSE;

					case saveError:

						LoadUI();

						return FALSE;
					}

				AfxAssert(FALSE);
				}
			}

		if( uCode == IDM_UI_CHANGE ) {

			return TRUE;
			}
		}

	return FALSE;
	}

// Data Overridables

BOOL CUIControl::OnCanAcceptData(IDataObject *pData, DWORD &dwEffect)
{
	if( CanAcceptText(pData, CF_UNICODETEXT) ) {

		dwEffect = DROPEFFECT_COPY;

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIControl::OnAcceptData(IDataObject *pData)
{
	if( AcceptText(pData, CF_UNICODETEXT) ) {

		CWnd *pWnd = NULL;

		FindFocus(pWnd);

		if( pWnd ) {

			pWnd->SetFocus();
			}

		return TRUE;
		}

	return FALSE;
	}

// Implementation

BOOL CUIControl::CanAcceptText(IDataObject *pData, UINT cfText)
{
	FORMATETC Fmt = { WORD(cfText), NULL, 1, -1, TYMED_HGLOBAL };

	if( pData->QueryGetData(&Fmt) == S_OK ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIControl::AcceptText(IDataObject *pData, UINT cfText)
{
	return AcceptText(pData, cfText, L"");
	}

BOOL CUIControl::AcceptText(IDataObject *pData, UINT cfText, CString Prefix)
{
	FORMATETC Fmt = { WORD(cfText), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		PCTXT   pText = PCTXT(GlobalLock(Med.hGlobal));

		UINT    uFind = wstrcspn(pText, L"\n\r");

		CString Text(pText, uFind);

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		CError Error = CError(TRUE);

		UINT   uCode = m_pText->SetAsText(Error, Prefix + Text);

		if( uCode == saveError ) {

			Error.Show(*afxMainWnd);

			return TRUE;
			}

		if( uCode == saveChange ) {

			LoadUI();

			ForceUpdate();

			return TRUE;
			}
		}

	return FALSE;
	}

void CUIControl::ForceUpdate(void)
{
	EnsureFocus();

	CWnd &Parent  = m_List[0]->GetParent();

	CWnd &Control = Parent.GetDlgItem(m_uID);

	Parent.SendMessage( WM_COMMAND,
			    WPARAM(MAKELONG(m_uID, IDM_UI_CHANGE)),
			    LPARAM(Control.GetHandle())
			    );
	}

BOOL CUIControl::EnsureFocus(void)
{
	CWnd *pWnd   = NULL;

	HWND  hWnd   = ::GetFocus();

	UINT  uCount = m_List.GetCount();

	for( UINT n = 0; n < uCount; n++ ) {

		if( hWnd == m_List[n]->GetHandle() ) {

			return TRUE;
		}
	}

	if( OnFindFocus(pWnd) ) {

		WPARAM wParam = WPARAM(pWnd->GetID());

		LPARAM lParam = LPARAM(pWnd->GetHandle());

		::SendMessage(pWnd->GetParent(), WM_FOCUSNOTIFY, wParam, lParam);

		return TRUE;
	}

	return FALSE;
}

// End of File
