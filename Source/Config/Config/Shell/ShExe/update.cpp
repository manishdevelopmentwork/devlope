
#include "intern.hpp"

#include "update.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Web Update Support
//
// Copyright (c) 1993-2008 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Update Dialog Box
//

// Runtime Class

AfxImplementRuntimeClass(CUpdateDialog, CStdDialog);

// State Machine

enum {
	stateConnect,
	stateFindBuild,
	stateReadBuild,
	stateFindSetup,
	stateReadSetup,
	stateTestSetup,
	stateDone
	};

// Constructors

CUpdateDialog::CUpdateDialog(BOOL fAuto)
{
	m_fAuto      = fAuto;

	m_fError     = FALSE;

	m_fDone      = FALSE;

	m_fUpdate    = FALSE;

	m_fAutoClose = FALSE;

	m_uState     = stateConnect;

	SetName(L"UpdateDlg");
	}

// Message Map

AfxMessageMap(CUpdateDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK,     OnCommandOkay)
 	AfxDispatchCommand(IDDONE,   OnUpdateDone )
 	AfxDispatchCommand(IDFAIL,   OnUpdateFail )

	AfxMessageEnd(CUpdateDialog)
	};

// Message Handlers

BOOL CUpdateDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	m_Update.Bind(this);

	m_Update.Create();

	TxFrame();

	return TRUE;
	}

// Command Handlers

BOOL CUpdateDialog::OnCommandOkay(UINT uID)
{
	if( !m_fDone ) {

		#pragma warning(suppress: 6286)

		if( TRUE || NoYes(CString(IDS_UPDATE_ABORT)) == IDYES ) {

			GetDlgItem(IDOK).EnableWindow(FALSE);

			m_Update.Terminate(INFINITE);

			EndDialog(!m_fError);
			}
		}
	else
		EndDialog(FALSE);

	return TRUE;
	}

BOOL CUpdateDialog::OnUpdateDone(UINT uID)
{
	if( RxFrame() ) {

		TxFrame();
		}

	return TRUE;
	}

BOOL CUpdateDialog::OnUpdateFail(UINT uID)
{
	SetError(CString(IDS_UPDATE_FAILED));

	return TRUE;
	}

// Implementation

void CUpdateDialog::SetDone(BOOL fError)
{
	if( !m_fDone ) {

		m_Update.Terminate(INFINITE);

		m_fError = fError;

		m_fDone  = TRUE;

		if( !m_fError && (m_fUpdate || m_fAuto) ) {

			EndDialog(TRUE);

			return;
			}

		GetDlgItem(IDOK).SetWindowText(CString(IDS_UPDATE_CLOSE));
		}
	}

void CUpdateDialog::SetError(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(CPrintf(IDS_UPDATE_ERROR, pText));

	MessageBeep(MB_ICONEXCLAMATION);

	SetDone(TRUE);
	}

void CUpdateDialog::ShowStatus(PCTXT pText)
{
	GetDlgItem(100).SetWindowText(pText);
	}

void CUpdateDialog::TxFrame(void)
{
	if( m_uState == stateConnect ) {

		ShowStatus(CString(IDS_UPDATE_OPEN_CONN));

		m_Update.Connect();

		return;
		}

	if( m_uState == stateFindBuild ) {

		ShowStatus(CString(IDS_UPDATE_FIND_DATA));

		m_Update.FindBuild();

		return;
		}

	if( m_uState == stateReadBuild ) {

		ShowStatus(CString(IDS_UPDATE_READ));

		m_Update.ReadBuild();

		return;
		}

	if( m_uState == stateFindSetup ) {

		ShowStatus(CString(IDS_UPDATE_FIND_SETUP));

		m_Update.FindSetup();

		return;
		}

	if( m_uState == stateReadSetup ) {

		UINT uSize = m_Update.GetFileSize() / 1024;

		ShowStatus(CPrintf(IDS_UPDATE_STATUS, uSize));

		m_Update.ReadSetup();

		return;
		}

	if( m_uState == stateTestSetup ) {

		ShowStatus(CString(IDS_VALIDATING));

		m_Update.TestSetup();

		return;
		}

	if( !m_fUpdate ) {

		ShowStatus(CString(IDS_UPDATE_NONE));
		}
	else
		ShowStatus(CString(IDS_UPDATE_OP_OK));

	SetDone(FALSE);
	}

BOOL CUpdateDialog::RxFrame(void)
{
	if( m_uState == stateConnect ) {

		m_uState = stateFindBuild;

		return TRUE;
		}

	if( m_uState == stateFindBuild ) {

		m_uState = stateReadBuild;

		return TRUE;
		}

	if( m_uState == stateReadBuild ) {

		UINT uBuild = m_Update.GetBuild();

		if( uBuild == 0 ) {

			OnUpdateFail(IDFAIL);

			SetDone(FALSE);

			return FALSE;
			}
		else {
			BOOL fAvail = FALSE;

			if( uBuild >= C3_BUILD * 1000 + C3_HOTFIX ) {

				if( uBuild == C3_BUILD * 1000 + C3_HOTFIX ) {

					BYTE bSubWeb = m_Update.GetSubVer();

					BYTE bSubNow = BYTE(wtoupper(TEXT(C3_SUB_VER)[0]));

					if( bSubWeb > bSubNow ) {

						fAvail = TRUE;
						}
					}
				else
					fAvail = TRUE;
				}

			if( fAvail ) {

				CString Text = IDS_UPDATE_AVAILABLE;

				if( YesNo(Text) == IDYES ) {

					m_uState  = stateFindSetup;

					m_fUpdate = TRUE;
					}
				else {
					m_uState = stateDone;

					m_fUpdate = TRUE;
					} 
				}
			else {
				if( !m_fAuto ) {

					MessageBeep(MB_ICONINFORMATION);
					}

				m_uState = stateDone;
				}

			return TRUE;
			}
		}

	if( m_uState == stateFindSetup ) {

		m_uState = stateReadSetup;

		return TRUE;
		}

	if( m_uState == stateReadSetup ) {

		if( m_Update.IsEndOfFile() ) {

			m_uState = stateTestSetup;
			}

		return TRUE;
		}

	if( m_uState == stateTestSetup ) {

		m_Update.ExecSetup();

		GetParent().PostMessage(WM_COMMAND, IDM_FILE_EXIT);

		m_uState = stateDone;

		return TRUE;
		}

	SetError(CString(IDS_UPDATE_INVALID));

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Update Thread
//

// Runtime Class

AfxImplementRuntimeClass(CUpdateThread, CRawThread);

// Constructor

CUpdateThread::CUpdateThread(void) : m_UpdateEvent(FALSE)
{
	m_pWnd  = NULL;

	m_hNet  = NULL;

	m_hFile = NULL;

	m_hSave = INVALID_HANDLE_VALUE;

	m_Setup.MakeTemporary();

	m_Setup.ChangeType(L"exe");
	}

// Destructor

CUpdateThread::~CUpdateThread(void)
{
	if( m_hNet ) {

		if( m_hFile ) {

			InternetCloseHandle(m_hFile);
			}

		InternetCloseHandle(m_hNet);
		}

	if( m_hSave < INVALID_HANDLE_VALUE ) {

		CloseHandle(m_hSave);
		}

	DeleteFile(m_Setup);
	}

// Management

void CUpdateThread::Bind(CWnd *pWnd)
{
	m_pWnd = pWnd;
	}

BOOL CUpdateThread::Terminate(DWORD Timeout)
{
	return CRawThread::Terminate(Timeout);
	}

// Attributes

UINT CUpdateThread::GetBuild(void) const
{
	return atoi(PCSTR(m_bData));
	}

BYTE CUpdateThread::GetSubVer(void) const
{
	UINT uChars = sizeof(m_bData);

	for( UINT u = 0; u < uChars; u++ ) {
	
		if( isdigit(m_bData[u]) ) {

			continue;
			}

		return BYTE(toupper(m_bData[u]));
		}

	return 0;
	}

UINT CUpdateThread::GetFileSize(void) const
{
	return m_uPos;
	}

BOOL CUpdateThread::IsEndOfFile(void) const
{
	return m_fEOF;
	}

// Operations

void CUpdateThread::Connect(void)
{
	m_uCode = 1;

	m_UpdateEvent.Set();
	}

void CUpdateThread::FindBuild(void)
{
	m_uCode = 2;

	m_UpdateEvent.Set();
	}

void CUpdateThread::ReadBuild(void)
{
	m_uCode = 3;

	m_UpdateEvent.Set();
	}

void CUpdateThread::FindSetup(void)
{
	m_uCode = 4;

	m_UpdateEvent.Set();
	}

void CUpdateThread::ReadSetup(void)
{
	m_uCode = 5;

	m_UpdateEvent.Set();
	}

void CUpdateThread::TestSetup(void)
{
	m_uCode = 6;

	m_UpdateEvent.Set();
	}

void CUpdateThread::ExecSetup(void)
{
	CFilename Setup = m_Setup;

	Setup.ChangeName(L"setup.exe");

	CopyFile(m_Setup, Setup, FALSE);

	STARTUPINFO Start;

	PROCESS_INFORMATION Info;

	memset(&Info,  0, sizeof(Info));

	memset(&Start, 0, sizeof(Start));

	Start.cb = sizeof(Start);

	ShellExecute( ThisObject, 
		      L"open", 
		      Setup,
		      NULL, 
		      NULL, 
		      SW_SHOWNORMAL
		      );
	}

// Overridables

BOOL CUpdateThread::OnInit(void)
{
	return TRUE;
	}

UINT CUpdateThread::OnExec(void)
{
	CWaitableList List(m_UpdateEvent, m_TermEvent);

	for(;;) {

		if( List.WaitForAnyObject(INFINITE) == waitSignal ) {

			if( List.GetObjectIndex() == 0 ) {

				BOOL fOkay = FALSE;

				switch( m_uCode ) {

					case 1:
						fOkay = DoConnect();
						break;

					case 2:
						fOkay = DoFindBuild();
						break;

					case 3:
						fOkay = DoReadBuild();
						break;

					case 4:
						fOkay = DoFindSetup();
						break;

					case 5:
						fOkay = DoReadSetup();
						break;

					case 6:
						fOkay = DoTestSetup();
						break;
					}

				m_pWnd->PostMessage(WM_COMMAND, fOkay ? IDDONE : IDFAIL);
				}
			else
				break;
			}
		}

	return 0;
	}

void CUpdateThread::OnTerm(void)
{
	}

// Implementation

BOOL CUpdateThread::DoConnect(void)
{
	m_hNet = InternetOpen(	CString(IDS_UPDATE_AGENT),
				INTERNET_OPEN_TYPE_PRECONFIG,
				NULL,
				NULL,
				0
				);

	return m_hNet ? TRUE : FALSE;
	}

BOOL CUpdateThread::DoFindBuild(void)
{
	m_hFile = InternetOpenUrl( m_hNet,
				   CString(IDS_UPDATE_BUILD),
				   NULL,
				   0,
				   INTERNET_FLAG_DONT_CACHE,
				   0
				   );

	return m_hFile ? TRUE : FALSE;
	}

BOOL CUpdateThread::DoReadBuild(void)
{
	UINT uSize  = sizeof(m_bData);

	UINT uCount = 0;

	InternetReadFile(m_hFile, m_bData, uSize, PDWORD(&uCount));

	InternetCloseHandle(m_hFile);

	m_hFile = NULL;

	return uCount ? TRUE : FALSE;
	}

BOOL CUpdateThread::DoFindSetup(void)
{
	m_hFile = InternetOpenUrl( m_hNet,
				   CString(IDS_UPDATE_SETUP),
				   NULL,
				   0,
				   INTERNET_FLAG_DONT_CACHE,
				   0
				   );

	if( m_hFile ) {

		if( (m_hSave = m_Setup.OpenWrite()) < INVALID_HANDLE_VALUE ) {

			m_uPos = 0;

			m_fEOF = FALSE;

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CUpdateThread::DoReadSetup(void)
{
	DWORD uSize = sizeof(m_bData);

	if( InternetReadFile(m_hFile, m_bData, uSize, &uSize) ) {

		if( !uSize ) {

			m_fEOF = TRUE;

			InternetCloseHandle(m_hFile);

			CloseHandle(m_hSave);

			m_hFile = NULL;
			}
		else {
			WriteFile(m_hSave, m_bData, uSize, &uSize, NULL);

			m_uPos += uSize;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CUpdateThread::DoTestSetup(void)
{
	WINTRUST_FILE_INFO FileData;
	
	WINTRUST_DATA      TrustData;
	
	memset(&FileData,  0, sizeof(FileData));
	
	memset(&TrustData, 0, sizeof(TrustData));
	
	FileData.cbStruct       = sizeof(WINTRUST_FILE_INFO);
	FileData.pcwszFilePath  = m_Setup;
	FileData.hFile          = NULL;
	FileData.pgKnownSubject = NULL;
	
	TrustData.cbStruct            = sizeof(TrustData);
	TrustData.pPolicyCallbackData = NULL;
	TrustData.pSIPClientData      = NULL;
	TrustData.dwUIChoice          = WTD_UI_NONE;
	TrustData.fdwRevocationChecks = WTD_REVOKE_NONE; 
	TrustData.dwUnionChoice       = WTD_CHOICE_FILE;
	TrustData.dwStateAction       = 0;
	TrustData.hWVTStateData       = NULL;
	TrustData.pwszURLReference    = NULL;
	TrustData.dwProvFlags         = WTD_SAFER_FLAG;
	TrustData.dwUIChoice          = 0;
	TrustData.pFile               = &FileData;
	
	GUID Guid    = WINTRUST_ACTION_GENERIC_VERIFY_V2;
	
	LONG lStatus = WinVerifyTrust( m_pWnd->GetHandle(),
				       &Guid,
				       &TrustData
				       );
	
	switch( lStatus ) {
		
		case ERROR_SUCCESS:
			
			return TRUE;
			
		case TRUST_E_NOSIGNATURE:
		case TRUST_E_EXPLICIT_DISTRUST:
		case TRUST_E_SUBJECT_NOT_TRUSTED:
		case CRYPT_E_SECURITY_SETTINGS:
			
			return FALSE;
		}
	
	return FALSE;
	}

// End of File
