
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "euro590o.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Eurotherm 590 Ascii Driver
//

// Instantiator

INSTANTIATE(CEurothermOld590Driver);

// Constructor

CEurothermOld590Driver::CEurothermOld590Driver(void)
{
	m_Ident	    = DRIVER_ID;

	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex      = Hex;
	}

// Destructor

CEurothermOld590Driver::~CEurothermOld590Driver(void)
{
	}

// Configuration

void MCALL CEurothermOld590Driver::Load(LPCBYTE pData)
{
	}
	
void MCALL CEurothermOld590Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEurothermOld590Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEurothermOld590Driver::Open(void)
{
	}

// Device

CCODE MCALL CEurothermOld590Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_GID = m_pHex[GetByte(pData)];
			m_pCtx->m_UID = m_pHex[GetByte(pData)];

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEurothermOld590Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEurothermOld590Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = PARAM_WORD;
	Addr.a.m_Offset = 238;
	Addr.a.m_Type   = addrWordAsWord;
	Addr.a.m_Extra  = 0;

	if( Read(Addr, Data, 1 ) == 1 ) {

		return 1;
		}

	m_pData->Write( EOT, FOREVER );

//**/	AfxTrace0("*** EOT ***");

	return CCODE_ERROR;
	}

CCODE MCALL CEurothermOld590Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress A;

	A.a.m_Table  = Addr.a.m_Table;
	A.a.m_Offset = Addr.a.m_Offset;
	A.a.m_Type   = Addr.a.m_Type;
	A.a.m_Extra  = 0;

	switch( A.a.m_Table ) {

		case TAG_BOOL:
		case TAG_INT:
		case TAG_REAL:

			if( DoWordWrite(TAG_PTR_PNO, PARAM_WORD, DWORD(A.a.m_Offset)) ) {

				A.a.m_Offset = TAG_DATA;
				}

			break;

		default: break;
		}

	switch( Addr.a.m_Table ) {

		case TAG_REAL:
		case PARAM_REAL:
			return DoRealRead(A.a.m_Offset, pData);

		case TAG_INT:
		case PARAM_WORD:
			return DoWordRead(A.a.m_Offset, Addr.a.m_Table, pData);

		case TAG_BOOL:
		case PARAM_BOOL:
			return DoBitRead(A.a.m_Offset, pData);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

CCODE MCALL CEurothermOld590Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	CAddress A;

	A.a.m_Table  = Addr.a.m_Table;
	A.a.m_Offset = Addr.a.m_Offset;
	A.a.m_Type   = Addr.a.m_Type;
	A.a.m_Extra  = 0;

	switch( A.a.m_Table ) {

		case TAG_BOOL:
		case TAG_INT:
		case TAG_REAL:

			if( DoWordWrite(TAG_PTR_PNO, PARAM_WORD, DWORD(A.a.m_Offset)) ) {

				A.a.m_Offset = TAG_DATA;
				}

			break;

		default: break;
		}

	switch( Addr.a.m_Table ) {

		case TAG_REAL:
		case PARAM_REAL:
			return DoRealWrite(A.a.m_Offset, *pData);

		case TAG_INT:
		case PARAM_WORD:
			return DoWordWrite(A.a.m_Offset, A.a.m_Table, *pData);

		case TAG_BOOL:
		case PARAM_BOOL:
			return DoBitWrite(A.a.m_Offset, A.a.m_Table, *pData);
		}

	return CCODE_ERROR | CCODE_HARD;
	}

// Implementation

// Read Handlers

CCODE CEurothermOld590Driver::DoBitRead(UINT Addr, PDWORD pData)
{
	PutIdentifier( Addr, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		UINT u = m_bRx[0] == '>' ?  1 :  0;
			
		*pData = m_bRx[u] != '0' ? 1L : 0L;

		return 1;
		}

	return SetCCError(FALSE);
	}

CCODE CEurothermOld590Driver::DoWordRead(UINT Addr, UINT uTable, PDWORD pData)
{
	char s1[32];
	char * s2 = s1;
	UINT uStart = 0;
	DWORD dResult = 0;
	BOOL fNeg = FALSE;

	PutIdentifier( Addr, NOTWRITE );

	if( Transact(NOTWRITE) ) {

		if( m_bRx[0] != '>' ) { // do Base 10 or string

			if( m_bRx[0] == '-' ) {

				fNeg = TRUE;

				uStart = 1;
				}

			if( IsADigit( m_bRx[uStart], NOTHEX ) ) {

				dResult = strtoul( (char *)(&m_bRx[uStart]), &s2, 10 );

				pData[0] = !fNeg ? dResult : -dResult;
				}

			else { // may be string character

				dResult = 0;

				for( UINT i = 0; i < 4; i++ ) {

					if( m_bRx[i] ) {

						dResult <<= 8;

						dResult += m_bRx[i];
						}

					else break;
					}

				pData[0] = dResult;
				}

			return 1;
			}

		if( IsADigit( m_bRx[1], ISHEX ) ) {

			pData[0] = strtoul( (char *)(&m_bRx[1]), &s2, 16 );

			return 1;
			}

		pData[0] = 0;

		return 1;
		}

	return SetCCError(FALSE);
	}

CCODE CEurothermOld590Driver::DoRealRead(UINT Addr, PDWORD pData)
{
	PutIdentifier( Addr, NOTWRITE );
	
	if( Transact(NOTWRITE) ) {

		pData[0] = ATOF( (const char * )&m_bRx[0] );

		return 1;
		}

	return SetCCError(FALSE);
	}

// Write Handlers

CCODE CEurothermOld590Driver::DoBitWrite(UINT Addr, UINT uTable, DWORD dData)
{
	PutIdentifier( Addr, ISWRITE );

	AddByte( '>' );

	AddByte( dData ? '1' : '0' );

	if( Transact(ISWRITE) ) return 1;

	if( m_bRx[0] == NAK ) { // try integer write

		PutIdentifier( Addr, ISWRITE );

		AddByte( dData ? '1' : '0' );

		AddByte( '.' );

		if( Transact(ISWRITE) ) return 1;
		}

	return SetCCError(TRUE);
	}

CCODE CEurothermOld590Driver::DoWordWrite(UINT Addr, UINT uTable, DWORD dData)
{
	PutIdentifier( Addr, ISWRITE );

	PutInteger( dData, TRUE );

	if( Transact(ISWRITE) ) return 1;

	if( m_bRx[0] == NAK ) { // try hex write

		PutIdentifier( Addr, ISWRITE );

		PutInteger( dData, FALSE );

		if( Transact(ISWRITE) ) return 1;
		}

	return SetCCError(TRUE);
	}

CCODE CEurothermOld590Driver::DoRealWrite(UINT Addr, DWORD dData)
{
	char c[14] = {0};

	SPrintf( c,
		"%.4f",
		PassFloat( dData )
		);

	for( UINT i = strlen(c) - 1; c[i] == '0'; i-- ) {

		c[i] = 0;
		}

	PutIdentifier( Addr, ISWRITE );

	AddValue(c);

	if( Transact(ISWRITE) ) return 1;

	return SetCCError(TRUE);
	}

// Frame Building

void CEurothermOld590Driver::StartFrame( void )
{
	m_uPtr = 0;
	
	AddByte( EOT );

	AddByte( m_pCtx->m_GID );
	AddByte( m_pCtx->m_GID );

	AddByte( m_pCtx->m_UID );
	AddByte( m_pCtx->m_UID );
	}

void CEurothermOld590Driver::EndFrame(BOOL fIsWrite)
{
	m_bRx[0] = 0;

	if( !fIsWrite ) AddByte( ENQ );

	else {
		AddByte( ETX );

		BOOL fFound = FALSE;
		BYTE bCheck = 0;

		for( UINT i = 0; i < m_uPtr; i++ ) {

			if( fFound ) bCheck ^= m_bTx[i];

			if( m_bTx[i] == STX ) fFound = TRUE;
			}

		AddByte( bCheck );
		}
	}

void CEurothermOld590Driver::AddByte(BYTE bData)
{
	if( m_uPtr < sizeof(m_bTx) ) {
	
		m_bTx[m_uPtr] = bData;

		m_uPtr++;
		}
	}

void CEurothermOld590Driver::AddValue(char * pV)
{
	strcpy( (char *)&m_bTx[m_uPtr], pV );

	m_uPtr += strlen(pV);
	}

void CEurothermOld590Driver::PutInteger(DWORD dData, BOOL fNotHex)
{
	char c[6];

	SPrintf( c,
		fNotHex ? "%d" : ">%X",
		dData
		);

	AddValue(c);
	}

void CEurothermOld590Driver::PutIdentifier(UINT uOffset, BOOL fIsWrite)
{
	StartFrame();

	if( fIsWrite ) AddByte( STX );

	AddByte( m_pHex[LOBYTE(uOffset/16)] );

	AddByte( m_pHex[LOBYTE(uOffset%16)] );
	}
		
// Transport Layer

BOOL CEurothermOld590Driver::Transact(BOOL fIsWrite)
{
	EndFrame( fIsWrite );

	if( Send() ) {

		if( m_pCtx->m_GID == 0x7F || m_pCtx->m_UID == 0x7F ) {

			m_pData->Read(0); // ensure transmit of full frame

			return TRUE; // Broadcast
			}

		return GetReply(fIsWrite);
		}
		
	return FALSE;
	}

BOOL CEurothermOld590Driver::Send(void)
{
	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT k = 0; k < m_uPtr; k++ ) AfxTrace1("[%2.2x]", m_bTx[k]);

	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER );

	return TRUE;
	}

BOOL CEurothermOld590Driver::GetReply(BOOL fIsWrite)
{
	UINT uState;

	UINT uPtr   = 0;

	UINT uTimer = 0;
	
	BYTE bCheck = 0;

	BOOL fError = FALSE;

	uState = fIsWrite ? 10 : 0;

//**/	AfxTrace0("\r\n");

	SetTimer(1000);
	
	while( (uTimer = GetTimer()) ) {

		UINT uByte;
		
		if( (uByte = RxByte(uTimer)) == NOTHING ) {
			
			continue;
			}

		bCheck ^= uByte;

//**/		AfxTrace1("<%2.2x>", uByte );

		switch( uState ) {
		
			case 0:
				if( uByte == STX ) {

					uState = 1;

					bCheck = 0;
					}

				else {
					if( uByte == EOT ) {

						m_bRx[0] = EOT;

						return FALSE;
						}
					}

				break;
				
			case 1:
				if( uByte != m_bTx[5] ) fError = TRUE;

				uState = 2;

				break;
				
			case 2:
				if( uByte != m_bTx[6] ) fError = TRUE;

				uState = 3;

				uPtr = 0;

				break;

			case 3:
				m_bRx[uPtr++] = uByte;

				if( uPtr > sizeof(m_bRx) ) return FALSE;

				if( uByte == ETX ) {

					uState = 4;

					m_bRx[uPtr-1] = 0;
					}

				break;

			case 4:
				return( fError ? FALSE : !bCheck );

			case 10:
				if( uByte == ACK ) return TRUE;

				else {
					m_bRx[0] = uByte;

					return FALSE;
					}
				break;
			}
		}
		
	return FALSE;
	}

// Port Access

UINT CEurothermOld590Driver::RxByte(UINT uTime)
{
	return m_pData->Read(uTime);
	}

// Helpers

BOOL CEurothermOld590Driver::IsADigit( BYTE b, BOOL fIsHex )
{
	if( b >= '0' && b <= '9' ) return TRUE;

	if( b == '-' || b == '+' ) return !fIsHex;

	if( b >= 'A' && b <= 'F' ) return fIsHex;

	if( b >= 'a' && b <= 'f' ) return fIsHex;

	return FALSE;
	}

CCODE CEurothermOld590Driver::SetCCError(BOOL fIsWrite)
{
	if( !fIsWrite && m_bRx[0] == EOT ) return 1;

	if( m_bRx[0] == NAK ) {

		return fIsWrite ? 1 : CCODE_ERROR | CCODE_NO_RETRY;
		}

	return CCODE_ERROR;
	}

// End of File
