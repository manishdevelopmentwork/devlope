
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4DigitalOutputConfig_HPP

#define INCLUDE_DAMix4DigitalOutputConfig_HPP

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output Configuration
//

class CDAMix4DigitalOutputConfig : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDAMix4DigitalOutputConfig(void);

	// View Pages
	UINT       GetPageCount(void);
	CString    GetPageName(UINT n);
	CViewWnd * CreatePage(UINT n);

	// Conversion
	BOOL Convert(CPropValue *pValue);

	// Item Properties
	UINT m_Enable1;
	UINT m_Enable2;
	UINT m_Enable3;

	UINT m_Mode1;
	UINT m_Mode2;
	UINT m_Mode3;

	UINT m_Value1;
	UINT m_Value2;
	UINT m_Value3;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Meta Data Creation
	void AddMetaData(void);
};

// End of File

#endif
