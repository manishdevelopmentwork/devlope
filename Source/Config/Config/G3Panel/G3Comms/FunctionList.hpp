
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_FunctionList_HPP

#define INCLUDE_FunctionList_HPP

//////////////////////////////////////////////////////////////////////////
//
// Function List
//

class CFunctionList : public CItemIndexList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CFunctionList(void);

		// Item Access
		CMetaItem * GetItem(INDEX Index) const;
		CMetaItem * GetItem(UINT uPos) const;

		// Item Lookup
		UINT FindNamePos(CString Name) const;
	};

// End of File

#endif
