
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Drawing

void CSourceEditorWnd::DrawText(CDC &DC, CRect const &Clip)
{
	DC.Select(m_FontCode);

	m_fComment = FALSE;

	int nLine = Clip.top / m_FontSize.cy;

	if( nLine > 0 ) {

		// TODO -- We could use a much faster check here that just
		// looks for comment start and comment end markers. That
		// would be sufficient the control the m_fComment flag. Or
		// we could have cache the comment flag for the top of the
		// screen when we did the position update.

		CRange Line = m_Lines[nLine - 1];

		PCTXT pText = PCTXT(m_Text);

		CArray <CColorSpan> Spans;

		C3SyntaxColors(Spans, m_fComment, pText, Line.m_nTo);
		}

	for( int r = nLine; r < m_nLines; r++ ) {

		CPoint Pos  = m_Inits[r];

		CRange Line = m_Lines[r];

		DrawLine(DC, Clip, Pos, Line, m_Opts.m_fSyntax);

		if( Pos.y <= Clip.bottom ) {

			if( Pos.x <= Clip.right ) {

				CRect Fill = CRect(Pos, m_FontSize);

				if( m_fSelect && m_SelectStart != m_SelectEnd ) {

					if( r >= m_SelectStart.y && r <= m_SelectEnd.y ) {
					
						int nSize = Line.m_nTo - Line.m_nFrom;

						if( m_Opts.m_fVirtual ) {

							if( m_SelectStart.y == m_SelectEnd.y ) {							

								if( m_SelectEnd.x > nSize ) {

									if( m_SelectStart.x > nSize ) {

										Fill.right = FindPosFromChar(r, m_SelectStart.x);

										DC.FillRect(Fill, colorEditorBack);

										Fill.left = Fill.right;
										}

									Fill.right = FindPosFromChar(r, m_SelectEnd.x);

									DC.FillRect(Fill, colorSelectBack);

									Fill.left = Fill.right;
									}
								}
							else {
								if( r == m_SelectStart.y ) {

									if( m_SelectStart.x > nSize ) {

										Fill.right = FindPosFromChar(r, m_SelectStart.x);

										DC.FillRect(Fill, colorEditorBack);

										Fill.left = Fill.right;
										}

									Fill.right = Clip.right;
									
									DC.FillRect(Fill, colorSelectBack);

									Fill.left = Fill.right;
									}

								else if( r == m_SelectEnd.y ) {

									if( m_SelectEnd.x > nSize ) {

										Fill.right = FindPosFromChar(r, m_SelectEnd.x);

										DC.FillRect(Fill, colorSelectBack);

										Fill.left = Fill.right;
										}
									}
								else {
									Fill.right = Clip.right;

									DC.FillRect(Fill, colorSelectBack);

									Fill.left = Fill.right;
									}
								}
							}
						else {
							if( nSize == 0 ) {

								DC.FillRect(Fill, colorSelectBack);

								Fill.left = Fill.right;
								}
							}
						}
					}

				Fill.right = Clip.right;

				DC.FillRect(Fill, colorEditorBack);
				}

			continue;
			}

		break;
		}

	if( Clip.bottom > CRect(m_TextExt).bottom ) {
		
		CRect Fill  = Clip;

		Fill.top    = m_TextExt.cy;

		DC.FillRect(Fill, colorEditorBack);
		}

	DC.Deselect();
	}

void CSourceEditorWnd::DrawLine(CDC &DC, CRect const &Clip, CPoint &Pos, CRange Line, BOOL fSyntax)
{
	int   nFrom = Line.m_nFrom;

	int     nTo = Line.m_nTo;

	int   nSize = nTo - nFrom;

	CArray <CColorSpan> Spans;

	if( fSyntax ) {

		PCTXT pText = PCTXT(m_Text) + nFrom;		
		
		if( !C3SyntaxColors(Spans, m_fComment, pText, nSize) ) {

			DrawLine( DC,
				  Clip,
				  Pos,
				  Line,
				  FALSE
				  );
			
			return;
			}
		}

	CRange Sel = m_fSelect ? m_Select : CRange();

	UINT n = 0;

	UINT uType;

	CRange Syn;

	while( nFrom < nTo ) {

		if( fSyntax ) {

			if( n < Spans.GetCount() ) {

				CColorSpan const &Span = Spans[n++];
			
				uType = Span.m_Type;

				Syn.Set(nFrom, nFrom + Span.m_uCount);
				}
			else {
				uType = spanDefault;

				Syn.Set(nFrom, nTo);
				}
			}
		else {
			uType = spanDefault;

			Syn.Set(nFrom, nFrom + nSize);
			}

		if( Sel.m_nFrom < Syn.m_nTo && Sel.m_nTo > Syn.m_nFrom ) {

			if( Sel.m_nFrom >= Syn.m_nFrom && Sel.m_nTo <= Syn.m_nTo ) {
				
				DrawFrag( DC,
					  Pos,
					  nFrom,
					  Sel.m_nFrom - Syn.m_nFrom,
					  uType
					  );
				
				DrawFrag( DC,
					  Pos,
					  nFrom,
					  Sel.m_nTo - Sel.m_nFrom,
					  uType | 0x4000
					  );
				
				DrawFrag( DC,
					  Pos,
					  nFrom,
					  Syn.m_nTo - Sel.m_nTo,
					  uType
					  );
				}
			else {				
				if( Sel.m_nFrom >= Syn.m_nFrom ) {					
					
					DrawFrag( DC,
						  Pos,
						  nFrom,
						  Sel.m_nFrom - nFrom,
						  uType
						  );
					
					DrawFrag( DC,
						  Pos,
						  nFrom,
						  Syn.m_nTo - nFrom,
						  uType | 0x4000
						  );
					}
				else {
					if( Sel.m_nTo <= Syn.m_nTo ) {
						
						DrawFrag( DC,
							  Pos,
							  nFrom,
							  Sel.m_nTo - nFrom,
							  uType | 0x4000
							  );
						
						DrawFrag( DC,
							  Pos,
							  nFrom,
							  Syn.m_nTo - nFrom,
							  uType
							  );
						}

					else {
						DrawFrag( DC,
							  Pos,
							  nFrom,
							  Syn.m_nTo - Syn.m_nFrom,
							  uType | 0x4000
							  );
						}
					}
				}
			}
		else {			
			DrawFrag( DC,
				  Pos,
				  nFrom,
				  Syn.m_nTo - Syn.m_nFrom,
				  uType
				  );
			}
		}
	}

void CSourceEditorWnd::DrawFrag(CDC &DC, CPoint &Pos, int &nFrom, int nSize, UINT uFlag)
{
	if( nSize > 0 ) {

		if( uFlag & 0xC000 ) {

			if( uFlag & 0x8000 ) {

				DC.SetBkColor(colorCaretBack);
				}

			if( uFlag & 0x4000 ) {

				DC.SetBkColor(colorSelectBack);
				}
			}
		else
			DC.SetBkColor(colorEditorBack);

		SetSyntaxColor(DC, uFlag & ~0xC000);

		PCTXT pText = PCTXT(m_Text) + nFrom;

		CSize  Size = TabbedTextOut(DC, Pos, pText, nSize);

		Pos.x      += Size.cx;

		nFrom      += nSize;
		}
	}

void CSourceEditorWnd::SetSyntaxColor(CDC &DC, UINT uType)
{
	switch( uType ) {

		case spanComment:	DC.SetTextColor(colorSyntaxComment);	break;

		case spanKeyword:	DC.SetTextColor(colorSyntaxKeyword);	break;

		case spanIdentifier:	DC.SetTextColor(colorSyntaxIdentifier);	break;

		case spanOperator:	DC.SetTextColor(colorSyntaxOperator);	break;

		case spanString:	DC.SetTextColor(colorSyntaxString);	break;

		case spanNumber:	DC.SetTextColor(colorSyntaxNumber);	break;

		case spanWas:		DC.SetTextColor(colorSyntaxWas);	break;

		default:		DC.SetTextColor(colorSyntaxDefault);	break;
		}
	}

// Tabbed Text Helpers

CSize CSourceEditorWnd::TabbedTextOut(CDC &DC, CPoint Pos, PCTXT pText, UINT uLen)
{
	int   nTabs[] = { m_FontSize.cx * m_Opts.m_nTabSize };

	DWORD dwSize  = ::TabbedTextOut( DC,
				         Pos.x,
				         Pos.y,
				         pText,
				         uLen,
				         1,
				         nTabs,
				         0
				         );

	return CSize(dwSize);
	}

CSize CSourceEditorWnd::GetTabbedTextExtent(PCTXT pText, UINT uLen)
{
	CSize Size(0, m_FontSize.cy);

	while( uLen-- ) {

		if( *pText++ == '\t' ) {

			int nTabStop = m_FontSize.cx * m_Opts.m_nTabSize;

			Size.cx     += nTabStop;

			Size.cx     -= (Size.cx % nTabStop);
			}
		else
			Size.cx += m_FontSize.cx;
		}

	return Size;
	}

CSize CSourceEditorWnd::GetTabbedTextExtent(CString Text)
{
	PCTXT pText = PCTXT(Text);

	UINT   uLen = Text.GetLength();

	return GetTabbedTextExtent(pText, uLen);
	}

// End of File
