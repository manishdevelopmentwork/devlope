
#include "Intern.hpp"

#include "UsbHubSetPortFeatureReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Hub Set Port Feature Request
//

// Constructor

CUsbHubSetPortFeatureReq::CUsbHubSetPortFeatureReq(WORD wFeature, WORD wPort)
{
	Init(wFeature, wPort);
	} 

// Operations

void CUsbHubSetPortFeatureReq::Init(WORD wFeature, WORD wPort)
{
	CUsbClassReq::Init();

	m_bRequest  = reqSetFeature;

	m_Direction = dirHostToDev;

	m_Recipient = recPort;

	m_wValue    = wFeature;

	m_wIndex    = wPort;
	}

// End of File
