
#include "intern.hpp"

#include "cangen.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Identifier
//

// Dynamic Class

AfxImplementDynamicClass(CCanId, CID29);

// Constructor

CCanId::CCanId(void)
{
	m_DefSize = 0;
	}

CCanId::CCanId(CCanId * pID) : CID29(pID)
{
	if( pID ) {

		m_DefSize = pID->m_DefSize;
		} 
	}

// Download Support

BOOL CCanId::MakeInitData(CInitData &Init)
{
	CID29::MakeInitData(Init);

	Init.AddByte(BYTE(m_DefSize));
			
	return TRUE;
	}

// Meta Data Creation

void CCanId::AddMetaData(void)	
{
	CID29::AddMetaData();

	Meta_AddInteger(DefSize);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Identifier Generic Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CCANGenericDeviceOptions, CCAN29bitIdEntryRawDeviceOptions);

// Constructor

CCANGenericDeviceOptions::CCANGenericDeviceOptions(void)
{
	
	}

// UI Loading

BOOL CCANGenericDeviceOptions::OnLoadPages(CUIPageList * pList)
{ 
	CCANGenericDeviceOptionsUIPage * pPage = New CCANGenericDeviceOptionsUIPage(this);

	pList->Append(pPage);

	return TRUE;			   
	}

// UI Managament

void CCANGenericDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == L"ButtonManageMessages" ) {

			HGLOBAL hPrev = m_pIDs->TakeSnapshot();

			CSystemItem *pSystem = GetDatabase()->GetSystemItem();

			CCANGenericDialog Dlg(this, pSystem);
	
			if( Dlg.Execute(*afxMainWnd) ) {
				
				HGLOBAL        hData = m_pIDs->TakeSnapshot();

				CCmdSubItem *  pCmd  = New CCmdSubItem( L"Config/IDs",
									hPrev,
									hData
									);
				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else {
					pHost->SaveExtraCmd(pCmd);

					SetDirty();
					}
				}
			else
				GlobalFree(hPrev);
			}
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Data Access

CID29 * CCANGenericDeviceOptions::FindListID(CString Text)
{
	UINT uFind = Text.Find(':');

	if( uFind < NOTHING ) {

		CString Entry = Text.Mid(0, uFind);

		CID29 * pID   = FindID(Entry);

		if ( !pID ) {

			UINT uNum    = tstrtoul(Entry, NULL, 16);

			UINT uEnd    = Text.Find('*');

			CString Name = uEnd < NOTHING ? Text.Mid(uFind + 1, uEnd - uFind - 1) : Text.Mid(uFind + 1);

			for( INDEX i = m_pIDs->GetHead(); !m_pIDs->Failed(i); m_pIDs->GetNext(i) ) {

				pID  = m_pIDs->GetItem(i);

				if( uNum == pID->m_Number && Name == pID->m_Title ) {

					return pID;
					}
				}

			return NULL;
			}

		return pID;
		}

	return NULL;
	}

CID29 * CCANGenericDeviceOptions::GetID(CString Text)
{
	if( !Text.IsEmpty() ) {

		UINT uFind = Text.Find('\t');

		if( uFind < NOTHING ) {

			UINT uNum = tstrtoul(Text.Mid(0, uFind), NULL, 16);

			UINT uTry = 0;

			CID29 * pID = NULL;

			while( (pID = CCAN29bitIdEntryRawDeviceOptions::GetID(uNum, uTry)) ) {

				if( pID->m_Title == Text.Mid(uFind + 1) ) {

					return pID;
					}

				uTry++;
				}
			}
		}

	return NULL;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Identifier Generic Device Options UI Page
//

// Runtime Class

AfxImplementRuntimeClass(CCANGenericDeviceOptionsUIPage, CUIStdPage);

// Constructor

CCANGenericDeviceOptionsUIPage::CCANGenericDeviceOptionsUIPage(CCANGenericDeviceOptions* pOption)
{
	m_pOption = pOption;
	}

// Operations

BOOL CCANGenericDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{	
	pView->StartGroup(L"CAN 11-bit/29-bit Identifier Messages", 1);

	pView->AddButton(L"Edit Messages", L"", L"ButtonManageMessages");

	pView->EndGroup(TRUE);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN Generic CAN Driver
//

// Instantiator

ICommsDriver * Create_CANGenericDriver(void)
{
	return New CCANGenericDriver;
	}

// Constructor			        

CCANGenericDriver::CCANGenericDriver(void)
{
	m_wID		= 0x40D8;

	m_uType		= driverSlave;

	m_Manufacturer	= "CAN";
	
	m_DriverName	= "Generic 11-bit/29-bit Identifier";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Generic CAN";

	m_DevRoot	= "DEV";

	m_fSingle       = TRUE;

	m_fExpand	= FALSE;
	}

// Configuration

CLASS CCANGenericDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCANGenericDeviceOptions);
	}

//////////////////////////////////////////////////////////////////////////
//
// CAN 11-bit/29-bit Identifier Generic Message Configuration Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CCANGenericDialog, CId29MessDialog);
		
// Constructor

CCANGenericDialog::CCANGenericDialog(CCANGenericDeviceOptions * pOptions, CSystemItem * pSystem) : CId29MessDialog(pOptions, pSystem)
{
	SetName(L"CANGenericDlg");
	}

// Message Map

AfxMessageMap(CCANGenericDialog, CId29MessDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify (CP_NUM, EN_KILLFOCUS, OnEditChange)
	
	AfxMessageEnd(CCANGenericDialog)
	};

// Message Handlers

BOOL CCANGenericDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	InitDefinition();

	CId29MessDialog::OnInitDialog(Focus, dwData);

	return TRUE;
	}

// Notification Handlers

void CCANGenericDialog::OnEditChange(UINT uID, CWnd &Wnd)
{
	if( uID == CP_NUM ) {

		UINT uNum = tstrtol(GetDlgItem(CP_NUM).GetWindowText(), NULL, 16);

		if( uNum > 0x7FF ) {

			CComboBox &Box = (CComboBox &) GetDlgItem(CP_DEF);

			Box.SetCurSel(0);
			}
		}

	CId29MessDialog::OnEditChange(uID, Wnd);
	}

// Implementation

void CCANGenericDialog::InitDefinition(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(CP_DEF);

	Box.AddString(L"29-bit");

	Box.AddString(L"11-bit");

	Box.SetCurSel(0);
	}

// Overridables

CID29 * CCANGenericDialog::NewID(void)
{
	return New CCanId;
	}

BOOL CCANGenericDialog::SetID(CError &Error, CID29 * pID, UINT uID)
{
	if( pID ) {

		CComboBox &Box  = (CComboBox &) GetDlgItem(CP_DEF);

		CCanId * pCan   = (CCanId *) pID;

		pCan->m_DefSize = BYTE(Box.GetCurSel());
		}

	return CId29MessDialog::SetID(Error, pID, uID);
	}

void CCANGenericDialog::SetID(CID29 * pID)
{
	if( pID ) {

		CComboBox &Box = (CComboBox &) GetDlgItem(CP_DEF);

		CCanId * pCan  = (CCanId *) pID;

		Box.SetCurSel(pCan->m_DefSize);
		}

	CId29MessDialog::SetID(pID);
	}

void CCANGenericDialog::DoEnables(void)
{
	CComboBox &Box = (CComboBox &) GetDlgItem(CP_DEF);

	BOOL fEnable   = !GetDlgItem(CP_NUM).GetWindowText().IsEmpty();

	if( fEnable ) {

		fEnable = 0x7FF >= tstrtoul(GetDlgItem(CP_NUM).GetWindowText(), NULL, 16);
		}

	Box.EnableWindow(fEnable);

	CId29MessDialog::DoEnables();
	}

// End of file
