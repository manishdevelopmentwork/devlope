
#include "Intern.hpp"

#include "ZLib.hpp"

#include "zlib/zlib.h"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// ZLib Object
//

// Instantiator

static IUnknown * Create_ZLib(PCTXT pName)
{
	return New CZLib;
	}

// Registration

global void Register_ZLib(void)
{
	piob->RegisterInstantiator("zlib", Create_ZLib);
	}

global void Revoke_ZLib(void)
{
	piob->RevokeInstantiator("zlib");
	}

// Constructor

CZLib::CZLib(void)
{
	StdSetRef();
	}

// Destructor

CZLib::~CZLib(void)
{
	}

// IUnknown

HRESULT CZLib::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IZLib);

	StdQueryInterface(IZLib);

	return E_NOINTERFACE;
	}

ULONG CZLib::AddRef(void)
{
	StdAddRef();
	}

ULONG CZLib::Release(void)
{
	StdRelease();
	}

// IZLib

UINT CZLib::Expand(PCBYTE pIn, UINT uIn, PBYTE pOut, UINT uOut)
{
	z_stream z = { 0 };

	z.next_in   = PBYTE(pIn);
	z.avail_in  = uIn;
	z.next_out  = pOut;
	z.avail_out = uOut;

	if( inflateInit2(&z, 16) == Z_OK ) {

		int r = inflate(&z,  Z_FINISH);

		if( r == Z_STREAM_END ) {

			UINT t = z.total_out;

			inflateEnd(&z);

			return t;
			}

		if( r == Z_BUF_ERROR ) {

			inflateEnd(&z);

			return NOTHING;
			}

		inflateEnd(&z);
		}

	return 0;
	}

UINT CZLib::Compress(PCBYTE pIn, UINT uIn, PBYTE pOut, UINT uOut)
{
	z_stream z = { 0 };

	z.next_in   = PBYTE(pIn);
	z.avail_in  = uIn;
	z.next_out  = pOut;
	z.avail_out = uOut;

	if( deflateInit2(&z,
			 Z_BEST_SPEED,
			 Z_DEFLATED,
			 0x1F,
			 0x05,
			 Z_DEFAULT_STRATEGY
			 ) == Z_OK ) {

		int r = deflate(&z, Z_FINISH);

		if( r == Z_STREAM_END ) {

			UINT t = z.total_out;

			deflateEnd(&z);

			return t;
			}

		if( r == Z_OK ) {

			deflateEnd(&z);

			return NOTHING;
			}

		deflateEnd(&z);
		}

	return 0;
	}

// End of File
