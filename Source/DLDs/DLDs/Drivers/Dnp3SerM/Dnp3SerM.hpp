
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Serial Master Driver
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DNP3SERM_HPP
	
#define	INCLUDE_DNP3SERM_HPP

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "dnp3m.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP3 Serial Master
//

class CDnp3SerialMaster : public CDnp3Master
{
	public:
		// Constructor
		CDnp3SerialMaster(void);

		// Destructor
		~CDnp3SerialMaster(void);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);

		// Configuration
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	protected:
		
		// Channels
	virtual	void OpenChannel (void);
	virtual BOOL CloseChannel(void);

	};

// End of File

#endif
