
//////////////////////////////////////////////////////////////////////////
//
// R245 Modular Controller - Strain Gage Module
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_SGDBASE_H

#define	INCLUDE_SGDBASE_H

//////////////////////////////////////////////////////////////////////////
//
// Out of Range Value
//

#define	UPSCALE		((LONG) 0x7000000)

//////////////////////////////////////////////////////////////////////////
//
// Control Modes
//

#define	MODE_HEAT	1
#define	MODE_COOL	2
#define	MODE_BOTH	3

//////////////////////////////////////////////////////////////////////////
//
// Analog Input Ranges
//

#define	SG_INPUT_20MV	1
#define	SG_INPUT_33MV	2
#define	SG_INPUT_200MV	3

//////////////////////////////////////////////////////////////////////////
//
// Excitation Outputs
//

#define	SG_EXCIT_5V	1
#define	SG_EXCIT_10V	2

//////////////////////////////////////////////////////////////////////////
//
// Input Fault Values
//

#define	SG_N21MV	 1131
#define	SG_P21MV	64404
#define	SG_N35MV	  492
#define	SG_P35MV	65043
#define	SG_N205MV	  849
#define	SG_P205MV	64686

//////////////////////////////////////////////////////////////////////////
//
// PV Assignment
//

#define	PVASS_IN1	1
#define	PVASS_SUM	2
#define	PVASS_DIFF	3
#define	PVASS_AVG	4

//////////////////////////////////////////////////////////////////////////
//
// Linear Output Modes
//

#define	LINOUT_010V	1
#define	LINOUT_020MA	2
#define	LINOUT_420MA	3
#define	LINOUT_RAW	4

//////////////////////////////////////////////////////////////////////////
//
// Alarm Modes
//

#define	ALARM_NULL	0x00
#define	ALARM_ABS_LO	0x01
#define	ALARM_ABS_HI	0x02
#define	ALARM_DEV_LO	0x03
#define	ALARM_DEV_HI	0x04
#define	ALARM_BAND_IN	0x05
#define	ALARM_BAND_OUT	0x06

//////////////////////////////////////////////////////////////////////////
//
// Alarm Assignment
//

#define	ALM_ASS_IN1	0
#define	ALM_ASS_IN2	1
#define	ALM_ASS_SUM	2
#define	ALM_ASS_DIFF	3
#define	ALM_ASS_AVG	4

//////////////////////////////////////////////////////////////////////////
//
// Input Fault Assignment
//

#define	INPUT_FAULT_IN1	 0
#define	INPUT_FAULT_BOTH 1

//////////////////////////////////////////////////////////////////////////
//
// Mapping Test Values
//

#define	MAP_MASK_CODE	0x7F
#define	MAP_MASK_TYPE	0x70
#define	MAP_MASK_NOTB	0x40
#define	MAP_MASK_BIT	0x07
#define	MAP_SHFT_BYTE	0x04
#define	MAP_TYPE_OUT	0x50
#define	MAP_TYPE_ANL	0x70

//////////////////////////////////////////////////////////////////////////
//
// Analog Data Items
//

#define	ANL_NULL	0x00
#define	ANL_HEAT	0x70
#define	ANL_COOL	0x71
#define	ANL_REQ_SP	0x72
#define	ANL_ACT_SP	0x73
#define	ANL_PV		0x74
#define	ANL_INP1	0x7C
#define	ANL_INP2	0x7D
#define	ANL_ERROR	0x75
#define ANL_REMOTE1	0x78
#define ANL_REMOTE2	0x79
#define ANL_REMOTE3	0x7A
#define ANL_REMOTE4	0x7B

//////////////////////////////////////////////////////////////////////////
//
// Digital Data Items
//

#define	DIG_NULL	0x00
#define	DIG_MANUAL	0x10
#define	DIG_TUNEBUSY	0x11
#define	DIG_TUNEDONE	0x12
#define	DIG_TUNEFAIL	0x13
#define	DIG_ALARM1	0x20
#define	DIG_ALARM2	0x21
#define	DIG_ALARM3	0x22
#define	DIG_ALARM4	0x23
#define	DIG_INPUTFAULT	0x26
#define	DIG_OUTLO	0x30
#define	DIG_OUTHI	0x31
#define	DIG_BADIN	0x32 
#define DIG_REMOTE1	0x33
#define DIG_REMOTE2	0x34
#define DIG_REMOTE3	0x35
#define DIG_REMOTE4	0x36
#define	DIG_INVERT	0x80

//////////////////////////////////////////////////////////////////////////
//
// Combined Data Items
//

#define	ANY_ALARM	0x40
#define	ANY_EITHER	0x42
#define	ANY_INVERT	0x80

//////////////////////////////////////////////////////////////////////////
//
// Additional LED Mappings
//

#define	LED_NULL	0x00
#define	LED_OUT1	0x50
#define	LED_OUT2	0x51
#define	LED_OUT3	0x52
#define	LED_INVERT	0x80

//////////////////////////////////////////////////////////////////////////
//
// Derrived Data Items
//

#define	DIG_AUTO	(DIG_MANUAL | DIG_INVERT)
#define	DIG_OKAY	(ANY_EITHER | DIG_INVERT)

//////////////////////////////////////////////////////////////////////////
//
// Calibration Data
//

typedef struct tagCalibSG
{
	WORD	n20mV[2];
	WORD	p20mV[2];
	WORD	n33mV[2];
	WORD	p33mV[2];
	WORD	n200mV[2];
	WORD	p200mV[2];

	WORD	LinOutV0;
	WORD	LinOutV10;
	WORD	LinOutI0;
	WORD	LinOutI4;
	WORD	LinOutI20;

	} CALIBSG;

//////////////////////////////////////////////////////////////////////////
//
// Mapper Configuration
//

typedef	struct tagMapperSG
{
	BYTE    DigRemote1:1;		
	BYTE    DigRemote2:1;
	BYTE    DigRemote3:1;
	BYTE    DigRemote4:1;
	BYTE 	SpareRemote:4;
	//==
	BYTE	LinOutType;
	INT	LinOutMin;
	INT	LinOutMax;
	WORD	LinOutFilter;
	WORD	LinOutDead;
	WORD	LinOutUpdate;
	BYTE	LinOutMap;
	BYTE	DigOutMap[3];
	BYTE	LedOutMap[4];
	WORD	CycleTime[3];
	WORD 	AnlRemote1;		
	WORD 	AnlRemote2;
	WORD 	AnlRemote3;
	WORD 	AnlRemote4;	

	} MAPPERSG;

//////////////////////////////////////////////////////////////////////////
//
// Installation Data
//

typedef struct tagInstallSG
{
	BYTE	DigHeat:1;
	BYTE	DigCool:1;
	BYTE	SpareLatch1:2;
	BYTE	RangeLatch:1;
	BYTE	SpareLatch2:3;
	//==
	BYTE	AlarmDelay1:1;			
	BYTE	AlarmDelay2:1;
	BYTE	AlarmDelay3:1;
	BYTE	AlarmDelay4:1;
	BYTE	AlarmLatch1:1;						
	BYTE	AlarmLatch2:1;
	BYTE	AlarmLatch3:1;
	BYTE	AlarmLatch4:1; 
	//== 
	BYTE	Mode;
	BYTE	InputRange[2];
	BYTE	ExcitOut[2];
	BYTE	PVAssign;
	BYTE	AlarmMode[4];
	INT	PVPeak;
	INT	PVValley;
	LONG	PVOffset;
	INT	PVTareTot;
	LONG	InpOffset[2];
	INT	InpTareTot[2];

	} INSTALLSG;

//////////////////////////////////////////////////////////////////////////
//
// Configuration Data
//

typedef struct tagConfigSG
{
	BYTE	ReqManual:1;
	BYTE	ReqTune:1;
	BYTE	ReqUserPID:1;
	BYTE	AlarmAccept1:1;
	BYTE	AlarmAccept2:1;
	BYTE	AlarmAccept3:1;
	BYTE	AlarmAccept4:1;
	BYTE	RangeAccept:1;
	//==
	BYTE	TarePV:1;
	BYTE	TareInp1:1;
	BYTE	TareInp2:1;
	BYTE	ResetPVTareTot:1;
	BYTE	ResetIn1TareTot:1;
	BYTE	ResetIn2TareTot:1;
	BYTE	ResetPkVall:1;
	BYTE	SpareTare:1;
	//==
	BYTE	SelectSig1:1;
	BYTE	ApplySigLo1:1;
	BYTE	ApplySigHi1:1;
	BYTE	SelectSig2:1;
	BYTE	ApplySigLo2:1;
	BYTE	ApplySigHi2:1;
	BYTE	ResetPeak:1;
	BYTE	ResetValley:1;
	//==
	BYTE	TuneCode;
	INT	Power;
	WORD	SP;
	WORD	SetHyst;
	INT	SetDead;
	WORD	SetRamp;
	BYTE	SetRampBase;
	WORD	InputFilter;
	WORD	UserConstP;
	WORD	UserConstI;
	WORD	UserConstD;
	WORD	UserCLimit;
	WORD	UserHLimit;
	WORD	UserFilter;
	WORD	AutoConstP;
	WORD	AutoConstI;
	WORD	AutoConstD;
	WORD	AutoCLimit;
	WORD	AutoHLimit;
	WORD	AutoFilter;
	INT	PowerFault;
	INT	PowerOffset;
	INT	PowerDead;
	WORD	PowerHeatGain;
	WORD	PowerCoolGain;
	WORD	PowerHeatHyst;
	WORD	PowerCoolHyst;
	WORD	HeatLimitLo;
	WORD	HeatLimitHi;
	WORD	CoolLimitLo;
	WORD	CoolLimitHi;
	INT	AlarmData[4];
	INT	AlarmHyst[4];
	INT	AlarmAssign[4];
	INT	DispLo[2];
	INT	DispHi[2];
	WORD	SigLoKey[2]; 
	WORD	SigHiKey[2];
	WORD	SigLoApp[2];
	WORD	SigHiApp[2];
	INT	PVLimitLo;
	INT	PVLimitHi;
	INT	FaultAssign;

	BYTE	GUID[16];
	BYTE	Valid;
	
	} CONFIGSG;

//////////////////////////////////////////////////////////////////////////
//
// Status Data
//

typedef struct tagStatusSG
{
	BYTE	AckManual:1;		// pidmain
	BYTE	AckTune:1;		// pidmain
	BYTE	TuneDone:1;		// pidtune
	BYTE	TuneFail:1;		// pidtune
	BYTE	AckSpare:4;		// spare
	//==
	BYTE	Alarm1:1;		// alarm
	BYTE	Alarm2:1;		// alarm
	BYTE	Alarm3:1;		// alarm
	BYTE	Alarm4:1;		// alarm
	BYTE	RangeAlarm:1;		// pidmain
	BYTE	SpareAlarm:3;		// spare
	//==
	BYTE	OutLimitLo:1;		// pidout
	BYTE	OutLimitHi:1;		// pidout
	BYTE	OverRange:1;		// pidin
	BYTE    SpareLimits:4;		// spare
	BYTE	BangBang:1;		// pidmain
	//==
	WORD	ActConstP;		// pidmain
	WORD	ActConstI;		// pidmain
	WORD	ActConstD;		// pidmain
	WORD	ActCLimit;		// pidmain
	WORD	ActHLimit;		// pidmain
	WORD	ActFilter;		// pidmain
	//==
	LONG	Input[2];		// pidin
	WORD	InputOver[2];		// input
	float	SP;			// pidmain
	float	PV;			// pidin
	float	DeltaPV;		// pidin
	float	DeltaT;			// pidin
	float	Error;			// pidmain
	float	Output;			// pidmain
	float	IntSum;			// pidmain
	WORD	HeatPower;		// pidout
	WORD	CoolPower;		// pidout
	BYTE	Running;		// main
	LONG	PVgross;		// pidin
	LONG	InputGross[2];		// pidin

	} STATUSSG;

//////////////////////////////////////////////////////////////////////////
//
// Integral Sum
//

typedef struct tagIntSumSG
{
	float	IntSum;

	} INTSUMSG;
	
// End of File

#endif
