
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Generic Split View Window
//

// Runtime Class

AfxImplementRuntimeClass(CSplitViewWnd, CViewWnd);

// Constructor

CSplitViewWnd::CSplitViewWnd(void)
{
	m_pWnd[0]  = NULL;

	m_pWnd[1]  = NULL;

	m_fUse[0]  = TRUE;

	m_fUse[1]  = TRUE;

	m_fFixed   = FALSE;

	m_uFocus   = 0;

	m_fCapture = FALSE;

	m_nPercent = 450;

	m_nTotal   = 0;

	m_Accel.Create(L"SplitViewAccel");
	}

// Routing Control

BOOL CSplitViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	UINT uRC = GetRoutingCode(Message);

	BYTE bLo = LOBYTE(uRC);

	BYTE bHi = HIBYTE(uRC);

	////////
	
	if( bHi & HIBYTE(RC_LOCAL1) ) {
	
		if( CViewWnd::OnRouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}
		
	for( UINT n = 0; n < 2; n++ ) {
	
		if( n == m_uFocus || bLo == RC_FIRST || bLo == RC_ALL ) {
		
			if( m_pWnd[n]->RouteMessage(Message, lResult) ) {
			
				if( bLo == RC_ALL ) {

					continue;
					}

				if( bHi & HIBYTE(RC_LOCAL2) ) {

					CViewWnd::OnRouteMessage(Message, lResult);
					}
					
				return TRUE;
				}
			}
		}

	return CViewWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CSplitViewWnd, CViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_PAINT)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_SETCURRENT)
	
	AfxDispatchControlType(IDM_WINDOW, OnWindowControl)
	AfxDispatchCommandType(IDM_WINDOW, OnWindowCommand)

	AfxMessageEnd(CSplitViewWnd)
	};

// Accelerator

BOOL CSplitViewWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CSplitViewWnd::OnPostCreate(void)
{
	CalcWndRects();

	for( UINT n = 0; n < 2; n++ ) {
	
		if( !m_pWnd[n] ) {
		
			AfxTrace(L"WARNING: Default view window created\n");
		
			if( !n ) {

				m_pWnd[n] = New CDummyViewWnd;
				}
			else
				m_pWnd[n] = New CMultiViewWnd;
			}
			
		CRect Rect = m_Rect[n];

		UINT  uID  = IDVIEW + n;
		
		DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN | WS_CLIPSIBLINGS;
	
		m_pWnd[n]->Create(dwStyle, Rect, ThisObject, uID, NULL);
		}
		
	FindBestSplit();

	m_pWnd[0]->ShowWindow(SW_SHOW);
	
	m_pWnd[1]->ShowWindow(SW_SHOW);
	
	m_pWnd[0]->SetCurrent(IsCurrent());
	}

void CSplitViewWnd::OnCancelMode(void)
{
	m_pWnd[m_uFocus]->SendMessage(WM_CANCELMODE);
	}

BOOL CSplitViewWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}
	
void CSplitViewWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);
	
	CRect Rect = m_Rect[2];
		
	DC.DrawEdge(Rect, EDGE_RAISED, BF_RECT | BF_SOFT);
	}

void CSplitViewWnd::OnSetFocus(CWnd &Wnd)
{
	if( !m_fCapture ) {
	
		if( m_pWnd[m_uFocus] ) {

			m_pWnd[m_uFocus]->SetFocus();
			}
		}
	}

void CSplitViewWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID >= IDVIEW && uID <= IDVIEW + 1 ) {
	
		UINT uFocus = uID - IDVIEW;
		
		Select(uFocus);
		}
	}

BOOL CSplitViewWnd::OnSetCursor(CWnd &Wnd, UINT uHitTest, UINT uMessage)
{
	if( !m_fFixed ) {

		if( &Wnd == this && uHitTest == HTCLIENT ) {
		
			CPoint Pos = GetCursorPos();
			
			ScreenToClient(Pos);
			
			if( m_Rect[2].PtInRect(Pos) ) {
			
				SetCursor(m_Cursor);
				
				return TRUE;
				}
			}
		}
	
	return BOOL(AfxCallDefProc());
	}

void CSplitViewWnd::OnSetCurrent(BOOL fCurrent)
{
	m_pWnd[m_uFocus]->SetCurrent(fCurrent);
	}

// Command Handlers

BOOL CSplitViewWnd::OnWindowControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {
	
		case IDM_WINDOW_NEXT_PANE:

			Src.EnableItem(m_fUse[0] && m_fUse[1]);
			
			break;
			
		case IDM_WINDOW_HIDE_UPPER:
			
			Src.EnableItem( m_fUse[1]);
			
			Src.CheckItem (!m_fUse[0]);
			
			break;
			
		case IDM_WINDOW_HIDE_LOWER:
			
			Src.EnableItem( m_fUse[0]);
			
			Src.CheckItem (!m_fUse[1]);
			
			break;
			
		default:
			return FALSE;
		}
		
	return TRUE;
	}

BOOL CSplitViewWnd::OnWindowCommand(UINT uID)
{
	if( uID == IDM_WINDOW_NEXT_PANE ) {
		
		SelectFocus(!m_uFocus);
	
		return TRUE;
		}
		
	if( uID == IDM_WINDOW_HIDE_UPPER ) {

		m_fUse[0] = !m_fUse[0];

		CalcWndRects();
		
		MoveWindows();

		SelectFocus(m_fUse[0] ? 0 : 1);
		
		return TRUE;
		}
		
	if( uID == IDM_WINDOW_HIDE_LOWER ) {

		m_fUse[1] = !m_fUse[1];

		CalcWndRects();
		
		MoveWindows();

		SelectFocus(m_fUse[1] ? 1 : 0);
		
		return TRUE;
		}
		
	return FALSE;
	}

// Implementation

void CSplitViewWnd::MoveWindows(void)
{
	if( m_fUse[0] ) {
		
		m_pWnd[0]->MoveWindow(m_Rect[0], TRUE);

		m_pWnd[0]->EnableWindow(TRUE);

		m_pWnd[0]->ShowWindow(SW_SHOW);
		}
	else {
		m_pWnd[0]->ShowWindow(SW_HIDE);

		m_pWnd[0]->EnableWindow(FALSE);
		}
		
	if( m_fUse[1] ) {
		
		m_pWnd[1]->MoveWindow(m_Rect[1], TRUE);

		m_pWnd[1]->EnableWindow(TRUE);

		m_pWnd[1]->ShowWindow(SW_SHOW);
		}
	else {
		m_pWnd[1]->ShowWindow(SW_HIDE);

		m_pWnd[1]->EnableWindow(FALSE);
		}
	}

BOOL CSplitViewWnd::SelectFocus(UINT uFocus)
{	
	if( Select(uFocus) ) {
	
		m_pWnd[m_uFocus]->SetFocus();
		
		return TRUE;
		}
		
	return FALSE;
	}

BOOL CSplitViewWnd::Select(UINT uFocus)
{
	if( m_uFocus != uFocus ) {
			
		m_pWnd[m_uFocus]->SetCurrent(FALSE);
			
		m_uFocus = uFocus;
			
		m_pWnd[m_uFocus]->SetCurrent(TRUE);
			
		afxMainWnd->SendMessage(WM_UPDATEUI);
		
		return TRUE;
		}
		
	return FALSE;
	}

// End of File
