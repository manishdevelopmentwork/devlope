
#include "Intern.hpp"

#include "UITextExprEnum.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Programmable Enumeration
//

// Dynamic Class

AfxImplementDynamicClass(CUITextExprEnum, CUITextCoded);

// Constructor

CUITextExprEnum::CUITextExprEnum(void)
{
	m_uFlags = textEdit | textEnum;

	m_uMask  = NOTHING;
	}

// Overridables

void CUITextExprEnum::OnBind(void)
{
	CUITextElement::OnBind();

	CStringArray List;

	UINT c = GetFormat().Tokenize(List, '|');

	for( UINT n = 0; n < c; n++ ) {

		if( !List[n].IsEmpty() ) {

			CEntry Enum;

			Enum.m_Data = n;

			Enum.m_Text = List[n];

			// cppcheck-suppress uninitStructMember

			m_Enum.Append(Enum);
			}
		}

	ShowTwoWay();
	}

CString CUITextExprEnum::OnGetAsText(void)
{
	CString Text = CUITextCoded::OnGetAsText();

	if( Text.GetLength() ) {

		if( IsNumberConst(Text) ) {

			UINT d = wcstoul(Text.Mid(2), NULL, 16);

			UINT c = m_Enum.GetCount();

			for( UINT n = 0; n < c; n++ ) {

				if( m_Enum[n].m_Data == d ) {

					return m_Enum[n].m_Text;
					}
				}

			AfxAssert(FALSE);
			}
		else
			return L'=' + Text;
		}

	return Text;
	}

UINT CUITextExprEnum::OnSetAsText(CError &Error, CString Text)
{
	if( Text.GetLength() ) {

		if( Text[0] == '=' ) {

			Text.Delete(0, 1);

			if( Text.IsEmpty() ) {

				Error.Set(CString(IDS_YOU_MUST_ENTER));

				return saveError;
				}

			return CUITextCoded::OnSetAsText(Error, Text);
			}

		UINT uCount = m_Enum.GetCount();

		UINT uBest  = 0;

		UINT uData  = 0;

		for( UINT n = 0; n < uCount; n++ ) {

			UINT uScore = m_Enum[n].m_Text.GetScore(Text);

			if( uScore > uBest ) {

				uBest = uScore;

				uData = m_Enum[n].m_Data;
				}
			}

		if( uBest ) {

			CString Prev = CUITextCoded::OnGetAsText();

			if( IsNumberConst(Prev) ) {

				UINT uPrev = wcstoul(Prev.Mid(2), NULL, 16);

				if( uData - uPrev ) {

					CString Expr = CPrintf(L"0x%X", uData);

					return CUITextCoded::OnSetAsText(Error, Expr);
					}

				return saveSame;
				}

			CString Expr = CPrintf(L"0x%X", uData);

			return CUITextCoded::OnSetAsText(Error, Expr);
			}

		Error.Format( IDS_FMT_IS_NOT_VALID,
			      Text
			      );

		return saveError;
		}

	return saveError;
	}

BOOL CUITextExprEnum::OnEnumValues(CStringArray &List)
{
	UINT c = m_Enum.GetCount();

	for( UINT n = 0; n < c; n++ ) {

		if( m_uMask & (1 << n) ) {

			List.Append(m_Enum[n].m_Text);
			}
		}

	return TRUE;
	}

// Implementation

void CUITextExprEnum::AddData(UINT Data, CString Text)
{
	CEntry Enum;

	Enum.m_Data = Data;

	Enum.m_Text = Text;

	// cppcheck-suppress uninitStructMember

	m_Enum.Append(Enum);
	}

BOOL CUITextExprEnum::IsNumberConst(CString const &Text)
{
	PCTXT p = Text;

	if( p[0] == '0' && p[1] == 'x' ) {

		if( *++++p ) {

			PTXT e;

			// cppcheck-suppress ignoredReturnValue

			wcstoul(p, &e, 16);

			return !*e;
			}
		}

	return FALSE;
	}

// End of File
