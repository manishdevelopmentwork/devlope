
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyGaugeTypeAR4_HPP
	
#define	INCLUDE_PrimRubyGaugeTypeAR4_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyGaugeTypeAR.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Type A4 Radial Gauge Primitive
//

class CPrimRubyGaugeTypeAR4 : public CPrimRubyGaugeTypeAR
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyGaugeTypeAR4(void);

		// Overridables
		void SetInitState(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Meta Data Creation
		void AddMetaData(void);

		// Path Management
		void MakePaths(void);

		// Implementation
		void LoadLayout(void);
	};

// End of File

#endif
