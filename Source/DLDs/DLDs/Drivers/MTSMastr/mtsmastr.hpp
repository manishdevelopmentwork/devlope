
//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CMTSMasterDriver;

#define	PINGADD	 1	// Request Module ID

// Spaces
#define	SPPROD	10	// Product   (Offset = 1 DP)
#define	SPPRD2	11	// Product   (Offset = 2 DP)
#define	SPPRD3	12	// Product   (Offset = 3 DP)
#define	SPINTF	13	// Interface (Offset = 1 DP)
#define	SPINF2	14	// Interface (Offset = 2 DP)
#define	SPINF3	15	// Interface (Offset = 3 DP)
#define	SPTAVG	25	// Average Temperature (Offset = 0 DP)
#define	SPTAV1	26	// Average Temperature (Offset = 1 DP)
#define	SPTAV2	27	// Average Temperature (Offset = 2 DP)
#define	SPTID0	28	// Individual Temperature (0 DP)
#define	SPTID1	29	// Individual Temperature (1 DP)
#define	SPTID2	30	// Individual Temperature (2 DP)
#define	SPFTMP	37	// Fast Temperatures (Fast Average = FTMP0)
#define	SPQFR	75	// # of Floats and RTD's (Offset 0=F, 1=R)
#define	SPGRCV	76	// Gradient Control Variable
#define	SPFZP	77	// Float Zero Position (Offset = Selected Float)
#define	SPRTDP	78	// RTD Positions (Offset = Selected RTD)
#define	SPWFZP	88	// Write Float Zero Positions using Calibrate Mode
#define	SPBUSY	96	// Read Busy Indicator
#define	SPRTDE	97	// Error Value for Read of RTDn
#define	SPERR	98	// High word = command, Low word = Error number)
#define	SPNAK	99	// NAK in write response

// Device Addresses
#define	ADEF	192
#define	AMAX	253

// Response Timeout control
// formula for determining max timeout values for RTD based commands
// c	= additive constant for command
// r	= number of RTD's
// k	= delay per RTD
#define	TMCALC(c, r, k)	(c + (r * k))

#define	SHORTWAIT	 250
#define	DEFAULTWAIT	 500
#define	TMOUTID		 100	// timeout for ID request (95ms typ)
#define	TMOUTPI1	 500	// timeout for P/I request (min <=  270, max <=  420) 
#define	TMOUTPI2	 700	// timeout for P/I request (min <=  430, max <=  700) 
#define	TMOUTPI3	2200	// timeout for P/I request (min <= 1280, max <= 2160) 
#define	TMAC25		1000	// max additive constant for command 25
#define	TMAK25		1000	// max delay per RTD for command 25
#define	TMAC26		1700	// max additive constant for command 26
#define	TMAK26		1600	// max delay per RTD for command 26
#define	TMAC27		2900	// max additive constant for command 27
#define	TMAK27		2700	// max delay per RTD for command 27
#define	TMIC28		 700	// max additive constant for command 28
#define	TMIK28		 900	// max delay per RTD for command 28
#define	TMIC29		1400	// max additive constant for command 29
#define	TMIK29		1600	// max delay per RTD for command 29
#define	TMIC30		2600	// max additive constant for command 30
#define	TMIK30		2700	// max delay per RTD for command 30
#define	TMFC37		 800	// max additive constant for command 37
#define	TMFK37		 900	// max delay per RTD for command 37
#define	TCOMMDELAY	  50	// min delay until next command

// Response Data control
#define	WAITDROP	   0	// wait for Drop number
#define	GETCOMMAND	   1	// wait for Command Echo
#define	STXWAIT		   2	// wait for STX
#define	WAITACKNAK	   3	// wait for ACK or NAK
#define	COLLECTDATA	  20	// wait for ETX
#define	GETCHECKSUM	  21	// collect checksum char's

// Write Data Resolutions + rounding digit
#define	RESGRAD		   6	// SPGRCV
#define	RESFZP		   4	// SPFZP
#define	RESRTDP		   2	// SPRTDP

// Named RTD Error
#define	RTDOORL		 203	// RTD Out of Range Low
#define	RTDSHORT	 208	// RTD Shorted

#define	RTDELO	0xC61C3C00	// IEEE = -9999
#define	RTDEHI	0x461C3C00	// IEEE =  9999

// Busy Response
#define	CMNOEX	CCODE_ERROR | CCODE_BUSY | CCODE_NO_RETRY;

struct	MTSReq {

	CAddress Addr;
	CAddress BusyAddr;
	};

//////////////////////////////////////////////////////////////////////////
//
// MTS DDA Master Driver
//

class CMTSMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CMTSMasterDriver(void);

		// Destructor
		~CMTSMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data
		struct CContext
		{
			UINT	m_Drop;

			// Device config value
			UINT	m_uRTDCt;

			UINT	m_uWriteErrCt;
			UINT	m_uRTDErr[6];

			DWORD	m_dPROD[3];
			DWORD	m_dINTF[3];
			DWORD	m_dTAVG[3];
			DWORD	m_dTID0[5];
			DWORD	m_dTID1[5];
			DWORD	m_dTID2[5];
			DWORD	m_dFTMP[6];
			DWORD	m_dQFR[3];
			DWORD	m_dGRCV;
			DWORD	m_dFZP[2];
			DWORD	m_dRTDP[5];

			PDWORD	m_pPrev;
			};

		CContext *m_pCtx;

		LPCTXT	m_pDec;

		BYTE	m_bTx[64];
		BYTE	m_bRx[64];

		struct MTSReq	m_ReqFrame;
		struct MTSReq	*m_pReq;

		// Error received
		DWORD	m_dError;
		DWORD	m_dNak;

		// Busy Indicator
		DWORD	m_dBusy;

		// Tx pointer
		UINT	m_uPtr;

		// Process Control values
		BOOL	m_fIDCheck;
		BOOL	m_fWriteCommand;
		BOOL	m_fInPing;

		UINT	m_uBusy;
		UINT	m_uTimeout;
		UINT	m_uState;
		UINT	m_uDumpCount;
		UINT	m_uNoResponse;
		UINT	m_uPingRelease;

		// Pinging
		BOOL	HandleRTDCount(void);

		// Busy Handling
		CCODE	HandleBusy(AREF dRef, PDWORD pData, UINT uCount);

		// Read Functions
		CCODE	DoRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE	DoRealRead(PDWORD pData, UINT uCount);
		void	GetPrevious(PDWORD pPrev, PDWORD pData, UINT uCount);

		// Write Functions
		CCODE	DoWordWrite(DWORD dData);
		CCODE	DoRealWrite(DWORD dData);
		CCODE	ExecuteWrite(UINT uCount);
		CCODE	ChkWriteError(BOOL fResponseStatus);
		void	AddCountsToWrite(DWORD dData);
		UINT	AddRealData(DWORD dData);
		UINT	AdjustDP(char * pc);

		// Process Sequence
		UINT	AdjustCommand(BOOL fIsRead);
		PDWORD	SetPrevious(UINT uTable, UINT uOffset);
		BOOL	InitComm(UINT uCommand, BOOL fIsWrite);
		BOOL	SendData(UINT uCount);
		BOOL	SendENQ(void);

		// Get Response Data
		DWORD	GetFieldData(UINT uField, BOOL fIsReal);
		void	FindColon(UINT *pCurrentPos);
		BOOL	SaveCounts(PDWORD pData, UINT uCount);
		BOOL	GetFastTemp(UINT uOffset, PDWORD pData, UINT uCount);
		void	GetRealData(PDWORD pData, UINT * pCount);
		DWORD	GetValue(UINT uRxPos, BOOL fIsReal);
		void	HandleError(UINT uPos, BOOL fNak);
		void	CheckRTDError(UINT *pErrPos, UINT uItem);

		// ID check
		BOOL	DoIDCheck(void);

		// Frame Building
		void	AddByte(BYTE  b);
		void	AddText(PCBYTE pbData);
		void	AddChecksum(void);

		// Frame Access
		BOOL	Transact(BOOL fIsWrite);
		void	SendFrame(void);
		BOOL	ReceiveFrame(BOOL fIsWrite);
		UINT	SetTimeoutValue(UINT uTable, BOOL fIsWrite);

		// Port Access
		void	Put(void);
		UINT	Get(UINT uTime);

		// Access Cached Items
		BOOL	NoReadTransmit(CAddress Addr, PDWORD pData, UINT uCount);
		BOOL	NoWriteTransmit(CAddress Addr, DWORD dData, UINT uCount);

		// Helpers
		BOOL	OnlyOneDatum(UINT uTable);
		BOOL	IsProdOrIntf(UINT uTable);
		BOOL	IsAvgTemp(UINT uTable);
		BOOL	IsIndTemp(UINT uTable);
		CCODE	CheckBusy(void);
		BOOL	ThisDevBusy(void);
		DWORD	CombineCts(UINT uFloatCt);
		UINT	FindDecimalPoint(char * pc, UINT uLen);
		UINT	NormalizeOffset(UINT uTable, UINT uOffset);

		// Debug
//***/		void	ShowText(AREF Addr);
//***/		void	ShowItemInfo(AREF Addr, UINT uCount, BOOL fIsRead);
//***/		void	ShowReal(DWORD d, PDWORD p);
	};

// End of File
