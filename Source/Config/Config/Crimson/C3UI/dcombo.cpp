
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 User Interface Engine
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Drop-Target Combo Box
//

// Runtime Class

AfxImplementRuntimeClass(CDropComboBox, CComboBox);

// Constructor

CDropComboBox::CDropComboBox(CUIElement *pUI)
{
	m_pUI   = pUI;
	
	m_uDrop = 0;
	}

// IUnknown

HRESULT CDropComboBox::QueryInterface(REFIID iid, void **ppObject)
{
	if( ppObject ) {

		if( iid == IID_IUnknown || iid == IID_IDropTarget ) {

			*ppObject = (IDropTarget *) this;

			return S_OK;
			}

		return E_NOINTERFACE;
		}

	return E_POINTER;
	}

ULONG CDropComboBox::AddRef(void)
{
	return 1;
	}

ULONG CDropComboBox::Release(void)
{
	return 1;
	}

// IDropTarget

HRESULT CDropComboBox::DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragEnter(pData, dwKeys, pt, pEffect);

	if( m_pUI ) {

		if( !IsItemReadOnly() ) {

			if( m_pUI->CanAcceptData(pData, *pEffect) ) {

				m_dwEffect = *pEffect;

				SetDrop(1);

				return S_OK;
				}
			}
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CDropComboBox::DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.DragOver(dwKeys, pt, pEffect);

	if( m_uDrop ) {

		*pEffect = m_dwEffect;

		return S_OK;
		}

	*pEffect = DROPEFFECT_NONE;

	return S_OK;
	}

HRESULT CDropComboBox::DragLeave(void)
{
	m_DropHelp.DragLeave();

	SetDrop(0);

	return S_OK;
	}

HRESULT CDropComboBox::Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect)
{
	m_DropHelp.Drop(pData, dwKeys, pt, pEffect);

	if( m_uDrop == 1 ) {

		if( m_pUI->AcceptData(pData) ) {

			*pEffect = m_dwEffect;

			SetDrop(0);

			return S_OK;
			}
		}

	*pEffect = DROPEFFECT_NONE;

	SetDrop(0);
	
	return S_OK;
	}

// Message Map

AfxMessageMap(CDropComboBox, CComboBox)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_PAINT)
	
	AfxDispatchControl(IDM_EDIT_PASTE, OnPasteControl)
	AfxDispatchCommand(IDM_EDIT_PASTE, OnPasteCommand)

	AfxMessageEnd(CDropComboBox)
	};

// Message Handlers

void CDropComboBox::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);
	}

void CDropComboBox::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	if( m_uDrop ) {

		DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);

		DC.FrameRect(GetClientRect(), afxBrush(Orange1));

		return;
		}

	DefProc(WM_PRINTCLIENT, WPARAM(DC.GetHandle()), 0);
	}

// Command Handlers

BOOL CDropComboBox::OnPasteControl(UINT uID, CCmdSource &Src)
{
	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			DWORD dwEffect;

			if( m_pUI->CanAcceptData(pData, dwEffect) ) {

				pData->Release();

				Src.EnableItem(TRUE);

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

BOOL CDropComboBox::OnPasteCommand(UINT uID)
{
	if( m_pUI ) {

		IDataObject *pData = NULL;

		if( OleGetClipboard(&pData) == S_OK ) {

			if( m_pUI->AcceptData(pData) ) {

				pData->Release();

				return TRUE;
				}

			pData->Release();
			}
		}

	return FALSE;
	}

// Implementation

BOOL CDropComboBox::SetDrop(UINT uDrop)
{
	if( m_uDrop - uDrop ) {

		m_uDrop = uDrop;

		Invalidate(FALSE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CDropComboBox::IsItemReadOnly(void)
{
	return m_pUI->GetText()->HasFlag(textRead);
	}

// End of File
