
#include "Intern.hpp"

#include "UsbGetDescriptorReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Usb Get Descriptor Standard Device Request
//

// Constructor

CUsbGetDescriptorReq::CUsbGetDescriptorReq(void)
{
	Init();
	} 

// Operations

void CUsbGetDescriptorReq::Init(void)
{
	CUsbStandardReq::Init();

	m_bRequest  = reqGetDesc;

	m_Direction = dirDevToHost;

	m_Recipient = recDevice;
	}

void CUsbGetDescriptorReq::SetDevice(WORD wLength)
{
	m_wValue  = MAKEWORD(0, descDevice);

	m_wLength = wLength; 
	}

void CUsbGetDescriptorReq::SetQualifier(WORD wLength)
{
	m_wValue  = MAKEWORD(0, descDeviceQual);

	m_wLength = wLength; 
	}

void CUsbGetDescriptorReq::SetConfig(WORD wIndex, WORD wLength)
{
	m_wValue  = MAKEWORD(wIndex, descConfig);

	m_wLength = wLength; 
	}

void CUsbGetDescriptorReq::SetString(WORD wIndex, WORD wLength, WORD wLangId)
{
	m_wValue  = MAKEWORD(wIndex, descString);

	m_wIndex  = wLangId;

	m_wLength = wLength; 
	}

// End of File
