
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_NandConfigStorage_HPP

#define INCLUDE_NandConfigStorage_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "BaseConfigStorage.hpp"

//////////////////////////////////////////////////////////////////////////
//
// NAND Config Storage Object
//

class CNandConfigStorage : public CBaseConfigStorage
{
public:
	// Constructor
	CNandConfigStorage(UINT uStart, UINT uEnd);
};

// End of File

#endif
