#!/bin/sh

# c3-ifup <interface> [<parent>]
#
# This script is called to bring up the specified interface
# by running the script created by the net applicator. This
# script will typically kill any Watcher that is managing the
# interface and otherwise deconfigure things. The parent is
# the optional interface on which this interface is dependent
# and whose state transition has produced this action.

test=/tmp/crimson/up/$1.up

if [ ! -f $test ]
then
	touch $test

	if [ "$2" == "" ]
	then
		logger -t c3-net "bringing up $1"
	else
		logger -t c3-net "bringing up $1 with parent of $2"
	fi

	script=/vap/opt/crimson/config/net/$1.up

	if [ -f $script ]
	then
		$script $1 $2
	fi
fi
