
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbEndpointDesc_HPP

#define	INCLUDE_UsbEndpointDesc_HPP

//////////////////////////////////////////////////////////////////////////
//
// Usb Endpoint Descriptor
//

class CUsbEndpointDesc : public UsbEndpointDesc
{
	public:
		// Constructor
		CUsbEndpointDesc(void);

		// Endianess
		void HostToUsb(void);
		void UsbToHost(void);

		// Attributes
		BOOL IsValid(void) const;

		// Init
		void Init(void);

		// Debug
		void Debug(void);
	};

// End of File

#endif
