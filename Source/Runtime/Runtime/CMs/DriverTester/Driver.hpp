
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_Driver_HPP

#define INCLUDE_Driver_HPP

//////////////////////////////////////////////////////////////////////////
//
// IP Protocol Codes
//

#define	IP_ICMP		0x0001
#define	IP_UDP		0x0011
#define	IP_TCP		0x0006
#define	IP_RAW		0x00FE
#define	IP_QUEUE	0x00FF

//////////////////////////////////////////////////////////////////////////
//
// Driver Base Object
//

class DLLAPI CDriver : public IDriver
{
	public:
		// Constructor
		CDriver(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDriver
		WORD METHOD GetIdentifier(void);
		void METHOD SetIdentifier(WORD wIdent);
		WORD METHOD GetCategory(void);
		WORD METHOD GetFlags(void);
		void METHOD SetHelper(IHelper *pHelper);

	protected:
		// Data Members
		ULONG     m_uRefs;
		WORD      m_Ident;
		WORD      m_Flags;
		IHelper * m_pHelper;

		// Data Handlers
		IDataHandler * MakeSingleDataHandler(void);
		IDataHandler * MakeDoubleDataHandler(void);

		// Operating System
		PVOID     Alloc(UINT uSize);
		void      Free(PVOID lpData);
		ISocket * CreateSocket(WORD wProt);
		CBuffer * CreateBuffer(UINT uSize, BOOL fFlag);
		UINT      CheckIP(IPREF IP, UINT uLimit);
		void      ForceSleep(UINT uTime);

		// Init Helpers
		BYTE   GetByte(PCBYTE &pData);
		WORD   GetWord(PCBYTE &pData);
		DWORD  GetLong(PCBYTE &pData);
		DWORD  GetAddr(PCBYTE &pData);
		PTXT   GetString(PCBYTE &pData);
		PCBYTE GetData(PCBYTE &pData, UINT uSize);

		// Additional Helpers
		BOOL MoreHelp(WORD ID, void **pHelp);
	};

// End of File

#endif
