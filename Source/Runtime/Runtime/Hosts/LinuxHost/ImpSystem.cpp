
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// System Call Identifiers
//

#include <linux/unistd.h>

#include <linux/poll.h>

//////////////////////////////////////////////////////////////////////////
//
// System Socket Interface
//

#include "Socket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

static int m_max = 2;

//////////////////////////////////////////////////////////////////////////
//
// System Call Macros
//

#define SysHandle(r)	BEGIN { for(;;) {						\
			int p = m_max;							\
			if( (r) <= p || __sync_val_compare_and_swap(&m_max, p, (r)) )	\
			break;								\
			} } END								\

#define SysReturn(r)	BEGIN {				\
			if( (r) < 0 ) {			\
			*__errno() = -(r);		\
			return -1;			\
			}				\
			return (r);			\
			} END				\

//////////////////////////////////////////////////////////////////////////
//
// System Call Implementation
//

clink void _exit(int code)
{
	int r;

	SysCall1(__NR_exit, r, code);

	AfxTouch(r);

	for( ;;);
}

clink void _exit_group(int code)
{
	int r;

	SysCall1(__NR_exit_group, r, code);

	AfxTouch(r);

	for( ;;);
}

clink int _fork(void)
{
	int r;

	SysCall0(__NR_fork, r);

	SysReturn(r);
}

clink int _nice(int inc)
{
	// Note -- We return the raw values here as negative
	// values are okay and we might not have our TLS set
	// up at this point in the thread initialization.

	int r;

	SysCall1(__NR_nice, r, inc);

	return r;
}

clink int _sched_getscheduler(int pid)
{
	int r;

	SysCall1(__NR_sched_getscheduler, r, pid);

	return r;
}

clink int _sched_setscheduler(int pid, int policy, int const *priority)
{
	int r;

	SysCall3(__NR_sched_setscheduler, r, pid, policy, priority);

	return r;
}

clink int _sched_get_priority_min(int policy)
{
	int r;

	SysCall1(__NR_sched_get_priority_min, r, policy);

	return r;
}

clink int _sched_get_priority_max(int policy)
{
	int r;

	SysCall1(__NR_sched_get_priority_max, r, policy);

	return r;
}

clink int _rename(const char *from, const char *to)
{
	int r;

	SysCall2(__NR_rename, r, from, to);

	SysReturn(r);
}

clink int _unlink(char const *name)
{
	int r;

	SysCall1(__NR_unlink, r, name);

	SysReturn(r);
}

clink int _execve(char const *filename, char const * const argv[], char const * const envp[])
{
	int r;

	SysCall3(__NR_execve, r, filename, argv, envp);

	SysReturn(r);
}

clink int _utime(char const *name, time_t atime, time_t mtime)
{
	int r;

	struct __utimes
	{
		timeval atime;
		timeval mtime;
	};

	__utimes t;

	t.atime.tv_sec  = atime;
	t.atime.tv_usec = 0;
	t.mtime.tv_sec  = mtime;
	t.mtime.tv_usec = 0;

	SysCall2(__NR_utimes, r, name, &t);

	SysReturn(r);
}

clink int _chmod(char const *name, mode_t mode)
{
	int r;

	SysCall2(__NR_chmod, r, name, mode);

	SysReturn(r);
}

clink int _dup2(int oldfd, int newfd)
{
	int r;

	SysCall2(__NR_dup2, r, oldfd, newfd);

	SysReturn(r);
}

clink int _open(char const *name, int oflag, int pmode)
{
	int r;

	SysCall3(__NR_open, r, name, oflag, pmode);

	SysHandle(r);

	SysReturn(r);
}

clink int _close(int fd)
{
	int r;

	SysCall1(__NR_close, r, fd);

	SysReturn(r);
}

clink int _close_on_exec(void)
{
	for( int fd = 3; fd <= m_max; fd++ ) {

		int r;

		SysCall1(__NR_close, r, fd);
	}

	return 0;
}

clink int _waitpid(int pid, int *status)
{
	int z = 0;

	int r;

	for( ;;) {

		#if defined(__NR_waitpid)

		SysCall3(__NR_waitpid, r, pid, status, z);

		#else

		SysCall4(__NR_wait4, r, pid, status, z, z);

		#endif

		if( r == -EINTR ) {

			CheckThreadCancellation();

			continue;
		}

		SysReturn(r);
	}
}

clink int _wait_thread(int pid)
{
	int r = _wait_thread_raw(pid);

	SysReturn(r);
}

clink int _wait_thread_raw(int pid)
{
	int r;

	int s;

	int o = 0x20000000;

	#if defined(__NR_waitpid)

	SysCall3(__NR_waitpid, r, pid, &s, o);

	#else

	int z = 0;

	SysCall4(__NR_wait4, r, pid, &s, o, z);

	#endif

	return r;
}

clink int _isatty(int fd)
{
	// TODO -- Is there an implementation for this?

	return 0;
}

clink int _fstat(int fd, struct stat *buffer)
{
	int r = -1;

	memset(buffer, 0, sizeof(*buffer));

	#if defined(__NR_oldfstat)

	struct __old_kernel_stat
	{
		unsigned short st_dev;
		unsigned short st_ino;
		unsigned short st_mode;
		unsigned short st_nlink;
		unsigned short st_uid;
		unsigned short st_gid;
		unsigned short st_rdev;
		unsigned long  st_size;
		unsigned long  st_atime;
		unsigned long  st_mtime;
		unsigned long  st_ctime;
	} s;

	void *p = &s;

	SysCall2(__NR_oldfstat, r, fd, p);

	if( r >= 0 ) {

		buffer->st_dev   = s.st_dev;
		buffer->st_ino   = s.st_ino;
		buffer->st_mode  = s.st_mode;
		buffer->st_nlink = s.st_nlink;
		buffer->st_uid   = s.st_uid;
		buffer->st_gid   = s.st_gid;
		buffer->st_rdev  = s.st_rdev;
		buffer->st_size  = s.st_size;
		buffer->st_atime = s.st_atime;
		buffer->st_mtime = s.st_mtime;
		buffer->st_ctime = s.st_ctime;
	}

	#else

	struct __new_kernel_stat
	{
		unsigned long st_val[16];
	} s;

	void *p = &s;

	SysCall2(__NR_fstat, r, fd, p);

	if( r >= 0 ) {

		// TODO -- This is a hack as I cannot find the
		// right kernel structure, but it works for now!

		buffer->st_dev   = 0;
		buffer->st_ino   = s.st_val[1];
		buffer->st_mode  = s.st_val[2];
		buffer->st_nlink = s.st_val[7];
		buffer->st_uid   = 0;
		buffer->st_gid   = 0;
		buffer->st_rdev  = 0;
		buffer->st_size  = s.st_val[5];
		buffer->st_atime = s.st_val[8];
		buffer->st_mtime = s.st_val[10];
		buffer->st_ctime = s.st_val[12];
	}

	#endif

	SysReturn(r);
}

clink int _stat(char const *path, struct stat *buffer)
{
	int r = -1;

	memset(buffer, 0, sizeof(*buffer));

	#if defined(__NR_oldstat)

	struct __old_kernel_stat
	{
		unsigned short st_dev;
		unsigned short st_ino;
		unsigned short st_mode;
		unsigned short st_nlink;
		unsigned short st_uid;
		unsigned short st_gid;
		unsigned short st_rdev;
		unsigned long  st_size;
		unsigned long  st_atime;
		unsigned long  st_mtime;
		unsigned long  st_ctime;
	} s;

	void *p = &s;

	SysCall2(__NR_oldfstat, r, fd, p);

	if( r >= 0 ) {

		buffer->st_dev   = s.st_dev;
		buffer->st_ino   = s.st_ino;
		buffer->st_mode  = s.st_mode;
		buffer->st_nlink = s.st_nlink;
		buffer->st_uid   = s.st_uid;
		buffer->st_gid   = s.st_gid;
		buffer->st_rdev  = s.st_rdev;
		buffer->st_size  = s.st_size;
		buffer->st_atime = s.st_atime;
		buffer->st_mtime = s.st_mtime;
		buffer->st_ctime = s.st_ctime;
	}

	#else

	struct __new_kernel_stat
	{
		unsigned long st_val[16];
	} s;

	void *p = &s;

	SysCall2(__NR_stat, r, path, p);

	if( r >= 0 ) {

		// TODO -- This is a hack as I cannot find the
		// right kernel structure, but it works for now!

		buffer->st_dev   = 0;
		buffer->st_ino   = s.st_val[1];
		buffer->st_mode  = s.st_val[2];
		buffer->st_nlink = s.st_val[7];
		buffer->st_uid   = 0;
		buffer->st_gid   = 0;
		buffer->st_rdev  = 0;
		buffer->st_size  = s.st_val[5];
		buffer->st_atime = s.st_val[8];
		buffer->st_mtime = s.st_val[10];
		buffer->st_ctime = s.st_val[12];
	}

	#endif

	SysReturn(r);
}

clink int _read(int fd, void *buffer, size_t count)
{
	int r;

	SysCall3(__NR_read, r, fd, buffer, count);

	SysReturn(r);
}

clink int _read_wait(int fd, void *buffer, size_t count, UINT ms)
{
	int r;

	if( (r = _select_read(fd, ms)) >= 0 ) {

		if( r > 0 ) {

			SysCall3(__NR_read, r, fd, buffer, count);

			SysReturn(r);
		}

		return 0;
	}

	SysReturn(r);
}

clink int _write(int fd, void const *buffer, size_t count)
{
	int r;

	SysCall3(__NR_write, r, fd, buffer, count);

	SysReturn(r);
}

clink int _getdents64(int fd, void *p, size_t size)
{
	int r;

	SysCall3(__NR_getdents64, r, fd, p, size);

	SysReturn(r);
}

clink off_t _lseek(int fd, off_t offset, int origin)
{
	int r;

	SysCall3(__NR_lseek, r, fd, offset, origin);

	SysReturn(r);
}

clink int _ftruncate(int fd, off_t size)
{
	int r;

	SysCall2(__NR_ftruncate, r, fd, size);

	SysReturn(r);
}

clink int _chdir(char const *path)
{
	int r;

	SysCall1(__NR_chdir, r, path);

	SysReturn(r);
}

clink int _mkdir(char const *path, int mode)
{
	int r;

	SysCall2(__NR_mkdir, r, path, mode);

	SysReturn(r);
}

clink int _rmdir(char const *path)
{
	int r;

	SysCall1(__NR_rmdir, r, path);

	SysReturn(r);
}

clink int _sync(void)
{
	int r;

	SysCall0(__NR_sync, r);

	SysReturn(r);
}

clink int _fsync(int fd)
{
	int r;

	SysCall1(__NR_fsync, r, fd);

	SysReturn(r);
}

clink int _getpid(void)
{
	int r;

	SysCall0(__NR_getpid, r);

	SysReturn(r);
}

clink int _gettid(void)
{
	if( _tls_valid ) {

		_tls_data *tls = _tls_get_data();

		if( tls->magic1 == _tls_magic && tls->magic2 == _tls_magic ) {

			if( tls->tid ) {

				return tls->tid;
			}
		}
	}

	return __gettid();
}

clink int __gettid(void)
{
	int r;

	SysCall0(__NR_gettid, r);

	SysReturn(r);
}

clink int _ioctl(int fd, int n, void *p)
{
	int r;

	SysCall3(__NR_ioctl, r, fd, n, p);

	SysReturn(r);
}

clink int _getdrivesize(char const *path, unsigned long long *size)
{
	struct kernel_statfs
	{
		unsigned int	f_type;
		unsigned int	f_bsize;
		unsigned long	f_blocks;
		unsigned long	f_bfree;
		unsigned long	f_bavail;
		unsigned long	f_files;
		unsigned long	f_ffree;
		int		f_fsid[2];
		unsigned short	f_namelen;
		unsigned short	f_frsize;
		unsigned short	f_flags;
		unsigned short	f_spare[11];
	};

	kernel_statfs s = { 0 };

	int r;

	SysCall2(__NR_statfs, r, path, &s);

	if( r == 0 ) {

		*size = s.f_blocks * (unsigned long long) s.f_bsize;

		return 0;
	}

	SysReturn(r);
}

clink int _getdrivefree(char const *path, unsigned long long *free)
{
	struct kernel_statfs
	{
		unsigned int	f_type;
		unsigned int	f_bsize;
		unsigned long	f_blocks;
		unsigned long	f_bfree;
		unsigned long	f_bavail;
		unsigned long	f_files;
		unsigned long	f_ffree;
		int		f_fsid[2];
		unsigned short	f_namelen;
		unsigned short	f_frsize;
		unsigned short	f_flags;
		unsigned short	f_spare[11];
	};

	kernel_statfs s = { 0 };

	int r;

	SysCall2(__NR_statfs, r, path, &s);

	if( r == 0 ) {

		*free = s.f_bfree * (unsigned long long) s.f_bsize;

		return 0;
	}

	SysReturn(r);
}

clink int _kill(int pid, int code)
{
	int r;

	SysCall2(__NR_kill, r, pid, code);

	SysReturn(r);
}

clink int _tkill(int pid, int code)
{
	int r;

	SysCall2(__NR_tkill, r, pid, code);

	SysReturn(r);
}

clink int _brk(unsigned long req)
{
	int r;

	SysCall1(__NR_brk, r, req);

	SysReturn(r);
}

clink int _signal(int n, void (*pfnHandler)(int))
{
	#if defined(__NR_signal)

	int r;

	SysCall2(__NR_signal, r, n, pfnHandler);

	SysReturn(r);

	#else

	struct sigaction act;

	act.sa_flags   = 0;
	act.sa_mask    = 0;
	act.sa_handler = pfnHandler;

	return _sigaction(n, &act, NULL);

	#endif
}

clink int _fcntl(int fd, int cmd, int data)
{
	int r;

	SysCall3(__NR_fcntl, r, fd, cmd, data);

	SysReturn(r);
}

clink int _sighandler(int n, void (*pfnHandler)(int))
{
	struct sigaction act;

	act.sa_flags   = 0;
	act.sa_mask    = 0;
	act.sa_handler = pfnHandler;

	return _sigaction(n, &act, NULL);
}

clink int _sigaction(int n, struct sigaction const *act, struct sigaction *old)
{
	int r;

	SysCall3(__NR_sigaction, r, n, act, old);

	SysReturn(r);
}

clink int gettimeofday(timeval *pt, void const *tz)
{
	int r;

	SysCall2(__NR_gettimeofday, r, pt, tz);

	SysReturn(r);
}

clink int settimeofday(timeval const *pt, void const *tz)
{
	int r;

	SysCall2(__NR_settimeofday, r, pt, tz);

	SysReturn(r);
}

clink time_t getmonosecs(void)
{
	int r;

	timespec ts = { 0 };

	SysCall2(__NR_clock_gettime, r, 1, &ts);

	return ts.tv_sec;
}

clink INT64 getmonomilli(void)
{
	int r;

	timespec ts = { 0 };

	SysCall2(__NR_clock_gettime, r, 1, &ts);

	return (ts.tv_sec * 1000LL) + (ts.tv_nsec / 1000000LL);
}

clink INT64  getmonomicro(void)
{
	int r;

	timespec ts = { 0 };

	SysCall2(__NR_clock_gettime, r, 1, &ts);

	return (ts.tv_sec * 1000000LL) + (ts.tv_nsec / 1000LL);
}

clink int _readlink(char const *link, char *buff, int size)
{
	int r;

	SysCall3(__NR_readlink, r, link, buff, size);

	SysReturn(r);
}

clink void * _mmap(unsigned long addr, unsigned long len, int prot, int flags, int fd, long long offset)
{
	int r;

	int p = int(offset >> 12);

	SysCall6(__NR_mmap2, r, addr, len, prot, flags, fd, p);

	return PVOID((HIWORD(r) == 0xFFFF) ? 0 : r);
}

clink void * _mmap_private(unsigned long len)
{
	int r;

	#if 0 && defined(AEON_PROC_X86)

	struct mmap_arg_struct
	{
		unsigned long addr;
		unsigned long len;
		unsigned long prot;
		unsigned long flags;
		unsigned long fd;
		unsigned long offset;
	} m;

	m.addr   = 0;
	m.len    = len;
	m.prot   = PROT_READ   | PROT_WRITE;
	m.flags  = MAP_PRIVATE | MAP_ANON;
	m.fd     = 0;
	m.offset = 0;

	SysCall1(__NR_mmap, r, &m);

	#else

	int prot   = PROT_READ   | PROT_WRITE;
	int flags  = MAP_PRIVATE | MAP_ANON;
	int fd     = 0;
	int offset = 0;

	SysCall6(__NR_mmap2, r, 0, len, prot, flags, fd, offset);

	#endif

	return PVOID((HIWORD(r) == 0xFFFF) ? 0 : r);
}

clink int _munmap(void *base, unsigned long len)
{
	int r;

	SysCall2(__NR_munmap, r, base, len);

	SysReturn(r);
}

clink int _munmap_and_exit(void *base, unsigned long len, int code)
{
	// This call is used during thread clean-up to release
	// the thread's stack and then exit. We need one method
	// to do this, as once the unmmap call has been made, we
	// cannot do anything stack related, including returning
	// from the function call. The code therefore preloads
	// all the arguments and avoids stack operations.

	int r = 0;

	#if defined(AEON_PROC_X86)

	ASM("mov $91,   %%eax  \n\t"
	    "mov %0,    %%ebx  \n\t"
	    "mov %1,    %%ecx  \n\t"
	    "mov %2,    %%edx  \n\t"
	    "int $0x80         \n\t"
	    "mov $1,    %%eax  \n\t"
	    "mov %%edx, %%ebx  \n\t"
	    "int $0x80         \n\t"
	    :
	: "g" (base),
		"g" (len),
		"g" (code)
		);

	#endif

	#if defined(AEON_PROC_ARM)

	ASM("mov  r7, #91  \n\t"
	    "ldr  r0, %0   \n\t"
	    "ldr  r1, %1   \n\t"
	    "ldr  r2, %2   \n\t"
	    "svc  #0       \n\t"
	    "mov  r7, #1   \n\t"
	    "mov  r0, r2   \n\t"
	    "svc  #0       \n\t"
	    :
	: "m" (base),
		"m" (len),
		"m" (code)
		);

	#endif

	SysReturn(r);
}

clink int _create_thread(int (*pfnProc)(void*), void *pParam, void *pStack, int *pPid)
{
	int r = -1;

	#if defined(AEON_PROC_X86)

	ASM("   push %%ebx           \n\t" // Preserve PIC register
	    "   mov  %4,     %%eax   \n\t" // Select clone service
	    "   mov  $0x000, %%ebx   \n\t" // Load signal number
	    "   or   $0xF00, %%ebx   \n\t" // Load flags as CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND
	    "   mov  %1,     %%ecx   \n\t" // Load new stack pointer
	    "   mov  %3,     %%edx   \n\t" // Load thread param
	    "   sub  $4,     %%ecx   \n\t" // Decrement new stack pointer
	    "   mov  %%edx,  (%%ecx) \n\t" // Store param on new stack
	    "   mov  %2,     %%edx   \n\t" // Load thread entry
	    "   sub  $4,     %%ecx   \n\t" // Decrement new stack pointer
	    "   mov  %%edx,  (%%ecx) \n\t" // Store entry on new stack
	    "   mov  $0,     %%edx   \n\t" // Other params are 0
	    "   mov  $0,     %%esi   \n\t" // Other params are 0
	    "   mov  $0,     %%edi   \n\t" // Other params are 0
	    "   int  $0x80           \n\t" // System Call
	    "   cmp  $0, %%eax       \n\t" // Are we the new thread?
	    "   jne  1f              \n\t" // If no, return to caller
	    "   pop  %%eax           \n\t" // Pop the entry from new stack
	    "   call *%%eax          \n\t" // Call the thread entry point
	    "   mov  %%eax, %%ebx    \n\t" // Set exit code is return value
	    "   mov  $1,    %%eax    \n\t" // Select exit service
	    "   int  $0x80           \n\t" // System Call
	    "1: pop  %%ebx           \n\t" // Restore PIC register
	    "   mov  %%eax, %0	 \n\t" // Store return code
	    : "=g"(r)
	    : "g" (pStack),
	    "g" (pfnProc),
	    "g" (pParam),
	    "e" (__NR_clone)
	    : "eax", "ecx", "edx", "esi", "edi"
	);

	#endif

	#if defined(AEON_PROC_ARM)

	ASM("   mov  r7, %5         \n\t" // Select clone service
	    "   mov  r0, #0x0000000 \n\t" // Load signal number
	    "   orr  r0, #0x0000F00 \n\t" // Load flags as CLONE_VM | CLONE_FS | CLONE_FILES | CLONE_SIGHAND
	    "   orr  r0, #0x0002000 \n\t" // Load flags as CLONE_PTRACE
	    "   orr  r0, #0x0010000 \n\t" // Load flags as CLONE_THREAD
	    "   orr  r0, #0x0200000 \n\t" // Load flags as CLONE_CHILD_CLEARTID
	    "   orr  r0, #0x1000000 \n\t" // Load flags as CLONE_CHILD_SETTID
	    "   ldr  r1, %1         \n\t" // Load new stack pointer
	    "   ldr  r2, %2         \n\t" // Load thread entry
	    "   sub  r1, #4         \n\t" // Decrement new stack pointer
	    "   str  r2, [r1]       \n\t" // Store entry on new stack
	    "   ldr  r2, %3         \n\t" // Load thread param
	    "   sub  r1, #4         \n\t" // Decrement new stack pointer
	    "   str  r2, [r1]       \n\t" // Store param on new stack
	    "   mov  r2, #0         \n\t" // Other params are 0
	    "   mov  r3, #0         \n\t" // Other params are 0
	    "   ldr  r4, %4         \n\t" // Store child thread id
	    "   svc  #0             \n\t" // System Call
	    "   cmp  r0, #0         \n\t" // Are we the new thread?
	    "   bne  1f             \n\t" // If no, return to caller
	    "   pop  {r0, r1}       \n\t" // Pop param and entry from new stack
	    "   blx  r1             \n\t" // Call the thread entry point
	    "   mov  r7, #1         \n\t" // Select exit service
	    "   svc  #0             \n\t" // System Call
	    "1: mov  %0, r0	    \n\t" // Store return code
	    : "=q"(r)
	    : "m"(pStack),
	    "m"(pfnProc),
	    "m"(pParam),
	    "m"(pPid),
	    "I"(__NR_clone)
	    : "r4", "r7"
	);

	#endif

	SysReturn(r);
}

clink int _sched_yield(void)
{
	int r;

	SysCall0(__NR_sched_yield, r);

	SysReturn(r);
}

clink int usleep(useconds_t us)
{
	if( us ) {

		struct timespec t;

		t.tv_sec  = (us / 1000000);

		t.tv_nsec = (us % 1000000) * 1000;

		struct timespec x;

		for( ;;) {

			int r;

			SysCall2(__NR_nanosleep, r, &t, &x);

			if( r == -EINTR ) {

				CheckThreadCancellation();

				t = x;

				continue;
			}

			SysReturn(r);
		}
	}

	return _sched_yield();
}

clink int _sleep(int ms)
{
	if( ms ) {

		if( ms == NOTHING ) {

			for( ;;) {

				_sleep(1000 * 1000);
			}
		}
		else {
			struct timespec t;

			t.tv_sec  = (ms / 1000);

			t.tv_nsec = (ms % 1000) * 1000000;

			struct timespec x;

			for( ;;) {

				int r;

				SysCall2(__NR_nanosleep, r, &t, &x);

				if( r == -EINTR ) {

					CheckThreadCancellation();

					t = x;

					continue;
				}

				SysReturn(r);
			}
		}
	}

	return _sched_yield();
}

clink int _usleep(int us)
{
	if( us ) {

		struct timespec t;

		t.tv_sec  = (us / 1000000);

		t.tv_nsec = (us % 1000000) * 1000;

		struct timespec x;

		int r;

		SysCall2(__NR_nanosleep, r, &t, &x);

		SysReturn(r);
	}

	return 0;
}

clink int _futex(int *p, int op, int val, void *t)
{
	int r;

	SysCall4(__NR_futex, r, p, op, val, t);

	SysReturn(r);
}

clink int _futex_base(int *p, int op, int val, void *t)
{
	int r;

	SysCall4(__NR_futex, r, p, op, val, t);

	return r;
}

clink int _futex_wait(int *p, int val, UINT ms)
{
	if( ms < NOTHING ) {

		struct timespec t;

		t.tv_sec  = (ms / 1000);

		t.tv_nsec = (ms % 1000) * 1000000;

		return _futex_base(p, 128, val, &t);
	}

	return _futex_base(p, 128, val, 0);
}

clink int _futex_wake(int *p, int n)
{
	return _futex_base(p, 129, n, 0);
}

clink int _clock_gettime_ms(int n)
{
	static struct timespec t;

	t.tv_sec  = 0;

	t.tv_nsec = 0;

	int r;

	SysCall2(__NR_clock_gettime, r, n, &t);

	return r ? 0 : t.tv_sec * 1000 + t.tv_nsec / 1000000;
}

clink int _select(int nfds, fd_set *read, fd_set *write, fd_set *except, UINT ms)
{
	if( nfds < FD_SETSIZE ) {

		// NOTE -- This is not a good idea to use as we have so many
		// file descriptors that it is too easy to hit the fd limit.

		int r;

		int z = 0;

		if( ms == NOTHING ) {

			SysCall6(__NR_pselect6, r, nfds, read, write, except, z, z);

			SysReturn(r);
		}
		else {
			struct timespec  t;

			struct timespec *p = &t;

			t.tv_sec  = (ms / 1000);

			t.tv_nsec = (ms % 1000) * 1000000;

			SysCall6(__NR_pselect6, r, nfds, read, write, except, p, z);

			SysReturn(r);
		}
	}

	AfxAssert(FALSE);

	return -1;
}

clink int _select_read(int fd, UINT ms)
{
	if( true || fd >= FD_SETSIZE ) {

		struct pollfd pfd = { fd, POLLIN, 0 };

		return _poll(&pfd, 1, ms);
	}
	else {
		fd_set set;

		FD_ZERO(&set);

		FD_SET(fd, &set);

		return _select(fd+1, &set, NULL, NULL, ms);
	}
}

clink int _select_write(int fd, UINT ms)
{
	if( true || fd >= FD_SETSIZE ) {

		struct pollfd pfd = { fd, POLLOUT, 0 };

		return _poll(&pfd, 1, ms);
	}
	else {
		fd_set set;

		FD_ZERO(&set);

		FD_SET(fd, &set);

		return _select(fd+1, NULL, &set, NULL, ms);
	}
}

clink int _select_except(int fd, UINT ms)
{
	if( true || fd >= FD_SETSIZE ) {

		struct pollfd pfd = { fd, POLLERR, 0 };

		return _poll(&pfd, 1, ms);
	}
	else {
		fd_set set;

		FD_ZERO(&set);

		FD_SET(fd, &set);

		return _select(fd+1, NULL, NULL, &set, ms);
	}
}

clink int _poll(struct pollfd *fds, int nfds, int ms)
{
	int r;

	int t = (ms == NOTHING) ? -1 : ms;

	SysCall3(__NR_poll, r, fds, nfds, ms);

	SysReturn(r);
}

clink int _timerfd_create(int clockid, int flags)
{
	int r;

	SysCall2(__NR_timerfd_create, r, clockid, flags);

	SysHandle(r);

	SysReturn(r);
}

clink int  _timerfd_settime(int fd, int flags, UINT ms, bool periodic)
{
	struct itimerspec t = { 0 };

	if( periodic ) {

		t.it_interval.tv_sec  = (ms / 1000);

		t.it_interval.tv_nsec = (ms % 1000) * 1000000;
	}

	t.it_value.tv_sec     = (ms / 1000);

	t.it_value.tv_nsec    = (ms % 1000) * 1000000;

	int r;

	int z = 0;

	SysCall4(__NR_timerfd_settime, r, fd, flags, &t, z);

	SysReturn(r);
}

clink int  _timerfd_gettime(int fd, PUINT *pms)
{
	static struct timespec t;

	t.tv_sec  = 0;

	t.tv_nsec = 0;

	int r;

	SysCall1(__NR_timerfd_gettime, r, &t);

	return r ? 0 : t.tv_sec * 1000 + t.tv_nsec / 1000000;
}

clink int _eventfd(int init)
{
	int r;

	SysCall2(__NR_eventfd2, r, init, 0);

	SysHandle(r);

	SysReturn(r);
}

clink int _pipe(int &fi, int &fo)
{
	int fd[2];

	int r;

	SysCall2(__NR_pipe2, r, fd, 0);

	if( r >= 0 ) {

		fi = fd[1];

		fo = fd[0];

		SysHandle(fi);

		SysHandle(fo);
	}

	SysReturn(r);
}

clink int _socket(int domain, int type, int protocol)
{
	int r;

	#if defined(__NR_socket)

	SysCall3(__NR_socket, r, domain, type, protocol);

	#else

	DWORD  a[] = { DWORD(domain),
		       DWORD(type),
		       DWORD(protocol),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_SOCKET, p);

	#endif	

	SysHandle(r);

	SysReturn(r);
}

clink int _bind(int fd, const struct sockaddr *addr, socklen_t addrlen)
{
	int r;

	#if defined(__NR_bind)

	SysCall3(__NR_bind, r, fd, addr, addrlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(addr),
		       DWORD(addrlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_BIND, p);

	#endif

	SysReturn(r);
}

clink int _connect(int fd, const struct sockaddr *addr, socklen_t addrlen)
{
	int r;

	#if defined(__NR_connect)

	SysCall3(__NR_connect, r, fd, addr, addrlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(addr),
		       DWORD(addrlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_CONNECT, p);

	#endif

	SysReturn(r);
}

clink int _listen(int fd, int backlog)
{
	int r;

	#if defined(__NR_listen)

	SysCall2(__NR_listen, r, fd, backlog);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(backlog),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_LISTEN, p);

	#endif

	SysReturn(r);
}

clink int _accept(int fd, const struct sockaddr *addr, socklen_t *addrlen)
{
	int r;

	#if defined(__NR_accept)

	SysCall3(__NR_accept, r, fd, addr, addrlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(addr),
		       DWORD(addrlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_ACCEPT, p);

	#endif

	SysReturn(r);
}

clink int _shutdown(int fd, int how)
{
	int r;

	#if defined(__NR_shutdown)

	SysCall2(__NR_shutdown, r, fd, how);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(how),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_SHUTDOWN, p);

	#endif

	SysReturn(r);
}

clink int _send(int fd, const void *buf, size_t len, int flags)
{
	int r;

	#if defined(__NR_send)

	SysCall4(__NR_send, r, fd, buf, len, flags);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(buf),
		       DWORD(len),
		       DWORD(flags),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_SEND, p);

	#endif

	SysReturn(r);
}

clink int _recv(int fd, void *buf, size_t len, int flags)
{
	int r;

	#if defined(__NR_recv)

	SysCall4(__NR_recv, r, fd, buf, len, flags);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(buf),
		       DWORD(len),
		       DWORD(flags),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_RECV, p);

	#endif

	SysReturn(r);
}

clink int _recvfrom(int fd, void *buf, size_t len, int flags, sockaddr *from, int *fromlen)
{
	int r;

	#if defined(__NR_recvfrom)

	SysCall6(__NR_recvfrom, r, fd, buf, len, flags, from, fromlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(buf),
		       DWORD(len),
		       DWORD(flags),
		       DWORD(from),
		       DWORD(fromlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_RECVFROM, p);

	#endif

	SysReturn(r);
}

clink int _recvmsg(int fd, struct msghdr *msg, int flags)
{
	int r;

	#if defined(__NR_recvmsg)

	SysCall3(__NR_recvmsg, r, fd, msg, flags);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(msg),
		       DWORD(flags)
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_RECVMSG, p);

	#endif

	SysReturn(r);
}

clink int _getsockopt(int fd, int level, int optname, void *optval, socklen_t *optlen)
{
	int r;

	#if defined(__NR_getsockopt)

	SysCall5(__NR_getsockopt, r, fd, level, optname, optval, optlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(level),
		       DWORD(optname),
		       DWORD(optval),
		       DWORD(optlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_GETSOCKOPT, p);

	#endif

	SysReturn(r);
}

clink int _setsockopt(int fd, int level, int optname, const void *optval, socklen_t optlen)
{
	int r;

	#if defined(__NR_setsockopt)

	SysCall5(__NR_setsockopt, r, fd, level, optname, optval, optlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(level),
		       DWORD(optname),
		       DWORD(optval),
		       DWORD(optlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_SETSOCKOPT, p);

	#endif

	SysReturn(r);
}

clink int _getsockname(int fd, struct sockaddr *addr, socklen_t *addrlen)
{
	int r;

	#if defined(__NR_getsockname)

	SysCall3(__NR_getsockname, r, fd, addr, addrlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(addr),
		       DWORD(addrlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_GETSOCKNAME, p);

	#endif

	SysReturn(r);
}

clink int _getpeername(int fd, struct sockaddr *addr, socklen_t *addrlen)
{
	int r;

	#if defined(__NR_getpeername)

	SysCall3(__NR_getpeername, r, fd, addr, addrlen);

	#else

	DWORD  a[] = { DWORD(fd),
		       DWORD(addr),
		       DWORD(addrlen),
	};

	PDWORD p  = a;

	SysCall2(__NR_socketcall, r, SYS_GETPEERNAME, p);

	#endif

	SysReturn(r);
}

// End of File
