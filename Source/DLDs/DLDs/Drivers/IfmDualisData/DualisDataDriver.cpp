	
#include "intern.hpp"

#include "DualisDataDriver.hpp"

// Instantiator

INSTANTIATE(CDualisDriver);

// Constructor

CDualisDriver::CDualisDriver(void)
{
	m_Ident = DRIVER_ID;

	m_pCtx  = NULL;

	m_uKeep = 0;

	m_pHead = NULL;

	m_pTail = NULL;

	m_pName = "data";	
	}

// Destructor

CDualisDriver::~CDualisDriver(void)
{
	}
	
// Management

void MCALL CDualisDriver::Attach(IPortObject *pPort)
{
	}

// Device

CCODE MCALL CDualisDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_uDevice = GetWord(pData);
			m_pCtx->m_IP	  = GetAddr(pData);
			m_pCtx->m_uPort	  = GetWord(pData);
			m_pCtx->m_fKeep   = FALSE;
			m_pCtx->m_fPing   = TRUE;
			m_pCtx->m_uTime1  = 20000;
			m_pCtx->m_uTime2  = 1000;
			m_pCtx->m_uTime3  =  500;
			m_pCtx->m_uTime4  = GetWord(pData);
			m_pCtx->m_uLast3  = GetTickCount();
			m_pCtx->m_uLast4  = GetTickCount();
			m_pCtx->m_pSock	  = NULL;
			m_pCtx->m_pData   = NULL;

			m_pCtx->m_uGroup    = 0;
			m_pCtx->m_uTicket   = 0;

			GetByte(pData);
			m_pCtx->m_pStart    = GetString(pData);
			m_pCtx->m_pStop     = GetString(pData);
			m_pCtx->m_pSep      = GetString(pData);
			m_pCtx->m_uProtocol = GetByte(pData);
			m_pCtx->m_pFormat   = "BMP";

			memset(m_pCtx->m_sSend, 0, sizeof(m_pCtx->m_sSend));
			memset(m_pCtx->m_sRecv, 0, sizeof(m_pCtx->m_sRecv));

			memset(m_pCtx->m_uInfo, 0, sizeof(m_pCtx->m_uInfo));

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			} 

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CDualisDriver::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// User Access

UINT MCALL CDualisDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{	
	CContext * pCtx = (CContext *) pContext;
	
	return 0;
	}

// Entry Points

void MCALL CDualisDriver::Service(void)
{
	}

CCODE MCALL CDualisDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( Addr.a.m_Table == addrNamed ) {

			CCmdDef List[] = {
			
				{ "V", TRUE  },	// 1
				{ "E", TRUE },	// 2
				{ "!", TRUE },	// 3
				{ "a", TRUE },	// 4
				{ "!", TRUE },	// 5

				};

			UINT uIndex = Addr.a.m_Offset - 1;

			if( uIndex < elements(List) ) {

				CCmdDef const &Cmd = List[uIndex];

				if( !strcmp(Cmd.pCode, "!") ) {
			
					return uCount;
					}
	
				if( Send(Cmd.pCode, Cmd.fRqst) ) {
					
					if( RecvFrame(m_pCtx->m_uProtocol) ) {						

						CCODE Code = Parse(Cmd.pCode, pData);

						if( !COMMS_SUCCESS(Code) ) {
							
							}

						return Code;
						}
					}
				}
			else {
				CloseSocket(FALSE);

				return CCODE_ERROR | CCODE_HARD;
				}
			}
		else {
			char sCmd[32] = { 0 };

			SPrintf(sCmd, "%c", Addr.a.m_Table);

			if( Send(sCmd, TRUE) ) {

				if( !strcmp(sCmd, "R") ) {								

					PBYTE pImage = NULL;
					
					if( RecvResult(pImage, m_pCtx->m_uProtocol) ) {

						delete pImage;

						CCODE Code = Parse(sCmd, pData, Addr.a.m_Offset, uCount);

						if( !COMMS_SUCCESS(Code) ) {
							
							}

						return Code;
						}					
					}				
				else {
					if( RecvFrame(m_pCtx->m_uProtocol) ) {

						CCODE Code = Parse(sCmd, pData, Addr.a.m_Offset, uCount);

						if( !COMMS_SUCCESS(Code) ) {
							
							}

						return Code;
						}
					}
				}
			}		

		CloseSocket(FALSE);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDualisDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		if( Addr.a.m_Table == addrNamed ) {

			CCmdDef List[] = {

				{ "v", FALSE },	// 1
				{ "!", FALSE },	// 2
				{ "t", FALSE },	// 3
				{ "c", FALSE },	// 4
				{ "p", FALSE },	// 5

				};

			if( Addr.a.m_Table == addrNamed ) {

				UINT uIndex = Addr.a.m_Offset - 1;

				if( uIndex < elements(List) ) {

					CCmdDef const &Cmd = List[uIndex];

					char sCmd[128] = { 0 };

					if( !strcmp(Cmd.pCode, "!") ) {
					
						return uCount;
						}

					if( !strcmp(Cmd.pCode, "v") ) {
			
						SPrintf(sCmd, "%s%02d", 
							      Cmd.pCode, 
							      *pData
							      );
						}

					if( !strcmp(Cmd.pCode, "c") ) {
					
						SPrintf(sCmd, "%s%01d%02d", 
							      Cmd.pCode, 
							      m_pCtx->m_uGroup, 
							      *pData
							      );
						}

					if( !strcmp(Cmd.pCode, "t") ) { 

						if( *pData & 1 ) {

							SPrintf(sCmd, "%s",	Cmd.pCode);
							}
						else 
							return uCount;
						}

					if( !strcmp(Cmd.pCode, "p") ) {

						SPrintf(sCmd, "%s%01d",	
							      Cmd.pCode, 
							      *pData ? 1 : 0
							      );
						}
			
					if( Send(sCmd, Cmd.fRqst) ) {
						
						if( RecvFrame(m_pCtx->m_uProtocol) ) {						

							CCODE Code = Parse(Cmd.pCode, pData);

							if( !COMMS_SUCCESS(Code) ) {

								}

							return Code;
							}
						}
					}
				else {
					CloseSocket(FALSE);

					return CCODE_ERROR | CCODE_HARD;
					}
				}
			}
		else
			return uCount;

		CloseSocket(FALSE);
		}

	return CCODE_ERROR;
	}

CCODE MCALL CDualisDriver::Atomic(AREF Addr, DWORD Data, DWORD dwMask)
{
	return CCODE_ERROR | CCODE_HARD;
	}

// Shared Code

#include "..\IfmDualisShared\DualisDriver.cpp"

// End of File
