
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Straton Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Project
//

// Static Data

UINT CStratonProject::m_uProj = 0;

// Constructor

CStratonProject::CStratonProject(void)
{
	m_dwHandle = 0;
	}

// Destructor

CStratonProject::~CStratonProject(void)
{
	afxDatabase->DeleteProject(m_dwHandle);
	}

// Attributes

CString CStratonProject::GetDate(void)
{
	return afxDatabase->GetProjectDateStamp(m_dwHandle);
	}

CFilename CStratonProject::GetProjectPath(void)
{
	return afxDatabase->GetProjectPath(m_dwHandle);
	}

CFilename CStratonProject::GetGlobalFilePath(PCTXT pSuffix)
{
	return afxDatabase->GetGlobalFilePath(m_dwHandle, pSuffix);
	}

CFilename CStratonProject::GetNextPath(BOOL fCreate)
{
	CModule * pApp = afxModule->GetApp();

	CFilename File = CPrintf( "%s%8.8X\\P%6.6X",
				  pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Control"),
				  GetCurrentProcessId(),
				  m_uProj++
				  );

	if( fCreate ) {

		// This option is used when we want to make the path itself
		// as well just the parent directory. By adding a slash to the
		// path, we ensure the target gets created as well.

		File += "\\";
		}

	File.CreatePath();

	return File;
	}

// Operations

BOOL CStratonProject::Make(void)
{
	CString File = GetNextPath(FALSE);

	if( afxDatabase->CreateProject(File) ) {

		if( (m_dwHandle = afxDatabase->OpenProject(File)) ) {

			return TRUE;
			}
		}

	AfxAssert(FALSE);

	return FALSE;
	}

BOOL CStratonProject::Open(CString Path)
{
	if( (m_dwHandle = afxDatabase->OpenProject(Path)) ) {

		return TRUE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

void CStratonProject::Save(void)
{
	afxDatabase->SaveProject(m_dwHandle);
	}

//////////////////////////////////////////////////////////////////////////
//
// Project Descriptor
//

CStratonProjectDescriptor::CStratonProjectDescriptor(DWORD dwProject)
{
	m_dwProject = dwProject;
	}

// Operations

void CStratonProjectDescriptor::Save(void)
{
	afxDatabase->SaveProject(m_dwProject);
	}

// End of File
