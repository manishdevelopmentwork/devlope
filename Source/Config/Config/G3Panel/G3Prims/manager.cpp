
#include "intern.hpp"

#include "PrimRubyDataBox.hpp"

#include "PrimRubyTextBox.hpp"

#include "PrimRubySimpleImage.hpp"

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// UI Manager Page
//

class CUIManagerPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CUIManagerPage(CUIManager *pManager, CString Title, UINT uPage);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CUIManager *m_pManager;
	};

//////////////////////////////////////////////////////////////////////////
//
// UI Manager Page
//

// Runtime Class

AfxImplementRuntimeClass(CUIManagerPage, CUIStdPage);

// Constructor

CUIManagerPage::CUIManagerPage(CUIManager *pManager, CString Title, UINT uPage)
{
	m_pManager = pManager;

	m_Title    = Title;

	m_uPage    = uPage;

	m_Class    = AfxRuntimeClass(CUIManager);
	}

// Operations

BOOL CUIManagerPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{
	if( m_uPage == 1 ) {

		CUIStdPage::LoadIntoView(pView, pItem);
	
		pView->StartGroup(CString(IDS_LANGUAGES), 1);

		pView->AddButton( CString(IDS_CONFIGURE),
				  CString(IDS_FORMAT_3),
				  L"ButtonManageLangs"
				  );

		pView->EndGroup(FALSE);
		}

	if( m_uPage == 2 ) {

		CUIStdPage::LoadIntoView(pView, pItem);
		}
	
	if( m_uPage == 3 ) {

		CUIStdPage::LoadIntoView(pView, pItem);
	
		pView->StartGroup(CString(IDS_MAINTENANCE), 1);

		pView->AddButton( CString(IDS_MANAGE_IMAGE),
				  CString(IDS_FORMAT_3),
				  L"ButtonManageImages"
				  );

		pView->EndGroup(FALSE);
		}

	if( m_uPage == 4 ) {

		pView->StartGroup(CString(IDS_MAINTENANCE), 1);

		pView->AddButton( CString(IDS_MANAGE_FONT),
				  CString(IDS_FORMAT_3),
				  L"ButtonManageFonts"
				  );

		pView->EndGroup(FALSE);
		}

	if( m_uPage == 5 ) {

		CUIStdPage::LoadIntoView(pView, pItem);
		}

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
// UI Manager Object
//

// Dynamic Class

AfxImplementDynamicClass(CUIManager, CCodedHost);

// Constructor

CUIManager::CUIManager(void)
{
	m_Handle        = HANDLE_NONE;
	m_pScratch      = New CPageList(TRUE);
	m_pPages        = New CPageList(FALSE);
	m_pImages       = New CImageManager;
	m_pFonts        = New CFontManager;
	m_pEvents       = New CEventMap;
	m_pPatterns	= New CRubyPatternLib;
	m_pOnStart      = NULL;
	m_pOnInit       = NULL;
	m_pOnUpdate     = NULL;
	m_pOnSecond     = NULL;
	m_TimeLock      = 30;
	m_TimePad       = 30;
	m_PadSize       = 0;
	m_PadLayout     = 0;
	m_PadNumeric    = 0;
	m_ShowNext      = 1;
	m_AutoEntry     = 0;
	m_EntryOrder    = 0;
	m_TwoMulti      = 1;
	m_TwoFlag       = 1;
	m_TwoNumeric    = 1;
	m_Beeper        = 0;
	m_PopAlignH     = 2;
	m_PopAlignV     = 1;
	m_GMC1	        = 1;
	m_LedAlarm      = 0;
	m_LedOrb        = 0;
	m_LedHome       = 0;
	m_PopKeypad     = 0;
	m_pTimeoutDisp  = NULL;

	m_cfCode        = RegisterClipboardFormat(L"C3.1 Code Fragment");
	
	m_cfTagList     = RegisterClipboardFormat(L"C3.1 Tag List");
	}

// Initial Values

void CUIManager::SetInitValues(void)
{
	SetInitial(L"TimeoutDisp", m_pTimeoutDisp, 0);
	}

// UI Creation

CViewWnd * CUIManager::CreateView(UINT uType)
{
	if( uType == viewCategory ) {

		CLASS Class = AfxNamedClass(L"CDispCatWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CDispNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
		}

	if( uType == viewResource ) {

		return New CResTreeWnd( L"Pages",
					NULL,
					AfxRuntimeClass(CDispPage)
					);
		}

	return CCodedHost::CreateView(uType);
	}

// UI Creation

BOOL CUIManager::OnLoadPages(CUIPageList *pList)
{
	pList->Append(New CUIManagerPage(this, CString(IDS_GLOBAL), 1));

	pList->Append(New CUIManagerPage(this, CString(IDS_ENTRY),  2));

	pList->Append(New CUIManagerPage(this, CString(IDS_IMAGES), 3));

	pList->Append(New CUIManagerPage(this, CString(IDS_FONTS),  4));

	if( m_pDbase->HasFlag(L"IconLeds") ) {

		pList->Append(New CUIManagerPage(this, CString(IDS_ICONS), 5));
		}

	return TRUE;
	}

// UI Managament

void CUIManager::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pHost->HasWindow() ) {

		CViewWnd &Wnd = pHost->GetWindow();

		if( Tag == L"ButtonManageFonts" ) {

			m_pFonts->InvokeDialog(Wnd);
			}

		if( Tag == L"ButtonManageImages" ) {

			m_pImages->InvokeDialog(Wnd);
			}

		if( Tag == L"ButtonManageLangs" ) {

			CCommsSystem *pSystem = (CCommsSystem *) m_pDbase->GetSystemItem();

			CLangManager *pLang   = pSystem->m_pLang;

			HGLOBAL       hPrev   = pLang->TakeSnapshot();

			CWnd          &Wnd    = pHost->GetWindow();

			if( pLang->InvokeDialog(Wnd) ) {
				
				CCmdGlobalItem *pCmd = New CCmdGlobalItem( pLang,
									   hPrev
									   );

				if( pCmd->IsNull() ) {

					delete pCmd;
					}
				else {
					pHost->SaveExtraCmd(pCmd);

					SetDirty();
					}
				}
			else
				GlobalFree(hPrev);
			}
		}

	CCodedHost::OnUIChange(pHost, pItem, Tag);
	}

// Type Access

BOOL CUIManager::GetTypeData(CString Tag, CTypeDef &Type)
{
	if( Tag.Left(2) == "On" ) {

		Type.m_Type  = typeVoid;

		Type.m_Flags = flagActive;

		return TRUE;
		}

	return CCodedHost::GetTypeData(Tag, Type);
	}

// Operations

BOOL CUIManager::CreatePage(CString Name)
{
	return CreatePage(NULL, Name);
	}

void CUIManager::Validate(BOOL fExpand)
{
	m_pPages->Validate(fExpand);

	m_pEvents->Validate(fExpand);
	}

void CUIManager::TagCheck(CCodedTree &Done, CIndexTree &Tags)
{
	m_pPages->TagCheck(Done, Tags);

	m_pEvents->TagCheck(Done, Tags);
	}

void CUIManager::PostConvert(void)
{
	m_pPages->PostConvert();

	m_pEvents->PostConvert();
	}

// Data Testing

BOOL CUIManager::CanAcceptDataObject(IDataObject *pData)
{
	if( CanAcceptCode(pData) ) {

		return TRUE;
		}

	if( CanAcceptTags(pData) ) {

		return TRUE;
		}

	if( CanAcceptText(pData) ) {

		return TRUE;
		}

	if( m_pImages->CanAcceptDataObject(pData) ) {

		return TRUE;
		}

	return FALSE;
	}

// Data Acceptance

BOOL CUIManager::AcceptDataObject(IDataObject *pData, CPrimList *pList, CSize MaxSize)
{
	if( AcceptCode(pData, pList, MaxSize) ) {

		return TRUE;
		}

	if( AcceptTags(pData, pList, MaxSize) ) {

		return TRUE;
		}

	if( AcceptText(pData, pList, MaxSize) ) {

		return TRUE;
		}

	if( m_pImages->CanAcceptDataObject(pData) ) {

		CPrimRubySimpleImage *pPrim = New CPrimRubySimpleImage;

		pList->AppendItem(pPrim);

		pPrim->LoadFromDataObject(pData, MaxSize);

		if( pPrim->m_pImage ) {

			UINT uImage = pPrim->m_pImage->m_Image;

			if( !m_pImages->IsTexture(uImage) ) {

				if( !m_pImages->IsMetaFile(uImage) ) {

					if( !m_pImages->IsXaml(uImage) ) {

						pPrim->m_pImage->m_Opts.m_Keep = 1;

						pPrim->m_Keep                  = 1;
						}
					}
				}
			}

		return TRUE;
		}

	return FALSE;
	}

// Item Naming

CString CUIManager::GetHumanName(void) const
{
	return CString(IDS_PAGES);
	}

// Persistance

void CUIManager::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString          Name  = Tree.GetName();

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( Name == L"TimeDisp" ) {

			UINT uTimeDisp = Tree.GetValueAsInteger();

			InitCoded(NULL, L"TimeoutDisp", m_pTimeoutDisp, uTimeDisp);

			continue;
			}

		if( pMeta ) {

			LoadProp(Tree, pMeta);

			continue;
			}
		}

	RegisterHandle();
	}

// Download Support

BOOL CUIManager::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_UI_MANAGER);

	CCodedHost::MakeInitData(Init);

	CDispPage *pPage = m_pPages->GetInit();

	Init.AddWord(WORD(pPage ? pPage->m_Handle : 0));

	Init.AddItem(itemSimple,  m_pEvents);
	Init.AddItem(itemSimple,  m_pFonts);
	Init.AddItem(itemSimple,  m_pPatterns);

	Init.AddItem(itemVirtual, m_pOnStart);
	Init.AddItem(itemVirtual, m_pOnInit);
	Init.AddItem(itemVirtual, m_pOnUpdate);
	Init.AddItem(itemVirtual, m_pOnSecond);

	Init.AddWord(WORD(m_TimeLock));
	Init.AddWord(WORD(m_TimePad));
	Init.AddByte(BYTE(m_PadSize));
	Init.AddByte(BYTE(m_PadLayout));
	Init.AddByte(BYTE(m_PadNumeric));
	Init.AddByte(BYTE(m_ShowNext));
	Init.AddByte(BYTE(m_AutoEntry));
	Init.AddByte(BYTE(m_EntryOrder));
	Init.AddByte(BYTE(m_TwoFlag));
	Init.AddByte(BYTE(m_TwoMulti));
	Init.AddByte(BYTE(m_TwoNumeric));
	Init.AddByte(BYTE(m_Beeper));
	Init.AddByte(BYTE(m_PopAlignH));
	Init.AddByte(BYTE(m_PopAlignV));
	Init.AddByte(BYTE(m_GMC1));
	Init.AddByte(BYTE(m_LedAlarm));
	Init.AddByte(BYTE(m_LedOrb));
	Init.AddByte(BYTE(m_LedHome));
	Init.AddByte(BYTE(m_PopKeypad));

	Init.AddItem(itemVirtual, m_pTimeoutDisp);

	return TRUE;
	}

// Meta Data

void CUIManager::AddMetaData(void)
{
	CCodedHost::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddCollect(Scratch);
	Meta_AddCollect(Pages);
	Meta_AddObject (Images);
	Meta_AddObject (Fonts);
	Meta_AddObject (Patterns);
	Meta_AddCollect(Events);
	Meta_AddVirtual(OnStart);
	Meta_AddVirtual(OnInit);
	Meta_AddVirtual(OnUpdate);
	Meta_AddVirtual(OnSecond);
	Meta_AddInteger(TimeLock);
	Meta_AddInteger(TimePad);
	Meta_AddInteger(PadSize);
	Meta_AddInteger(PadLayout);
	Meta_AddInteger(PadNumeric);
	Meta_AddInteger(ShowNext);
	Meta_AddInteger(AutoEntry);
	Meta_AddInteger(EntryOrder);
	Meta_AddInteger(TwoMulti);
	Meta_AddInteger(TwoFlag);
	Meta_AddInteger(TwoNumeric);
	Meta_AddInteger(Beeper);
	Meta_AddInteger(PopAlignH);
	Meta_AddInteger(PopAlignV);
	Meta_AddInteger(GMC1);
	Meta_AddInteger(LedAlarm);
	Meta_AddInteger(LedOrb);
	Meta_AddInteger(LedHome);
	Meta_AddInteger(PopKeypad);

	Meta_AddVirtual(TimeoutDisp);

	Meta_SetName((IDS_UI_MANAGER));
	}

// Implementation

BOOL CUIManager::CanAcceptCode(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	return pData->QueryGetData(&Fmt) == S_OK;
	}

BOOL CUIManager::CanAcceptTags(IDataObject *pData)
{
	FORMATETC Fmt = { WORD(m_cfTagList), NULL, 1, -1, TYMED_HGLOBAL };

	return pData->QueryGetData(&Fmt) == S_OK;
	}

BOOL CUIManager::CanAcceptText(IDataObject *pData)
{
	FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

	return pData->QueryGetData(&Fmt) == S_OK;
	}

BOOL CUIManager::AcceptCode(IDataObject *pData, CPrimList *pList, CSize MaxSize)
{
	FORMATETC Fmt = { WORD(m_cfCode), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		CPrimRubyDataBox *pPrim = New CPrimRubyDataBox;

		pList->AppendItem(pPrim);

		pPrim->Set(Text, MaxSize);

		pPrim->SetHand(TRUE);

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIManager::AcceptTags(IDataObject *pData, CPrimList *pList, CSize MaxSize)
{
	FORMATETC Fmt = { WORD(m_cfTagList), NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CStringArray List;

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		Text.Tokenize(List, '|');

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		int cx = 0;

		UINT n, c;

		for( n = 0; n < List.GetCount(); n++ ) {

			CPrimRubyDataBox *pPrim = New CPrimRubyDataBox;

			pList->AppendItem(pPrim);

			pPrim->Set(List[n], MaxSize);

			pPrim->SetHand(TRUE);

			MakeMax(cx, pPrim->GetRect().cx());
			}

		for( c = 0; c < n; c++ ) {

			CPrim *pPrim = pList->GetItem(c);

			CRect  Rect  = pPrim->GetRect();

			Rect.right   = Rect.left + cx;

			pPrim->SetRect(Rect);
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIManager::AcceptText(IDataObject *pData, CPrimList *pList, CSize MaxSize)
{
	FORMATETC Fmt = { CF_UNICODETEXT, NULL, 1, -1, TYMED_HGLOBAL };

	STGMEDIUM Med = { 0, NULL, NULL };

	if( pData->GetData(&Fmt, &Med) == S_OK ) {

		CString Text = PCTXT(GlobalLock(Med.hGlobal));

		GlobalUnlock(Med.hGlobal);

		ReleaseStgMedium(&Med);

		CPrimRubyTextBox *pPrim = New CPrimRubyTextBox;

		pList->AppendItem(pPrim);

		pPrim->Set(Text, MaxSize);

		pPrim->m_pTextItem->m_AlignH = 0;

		pPrim->SetHand(TRUE);

		return TRUE;
		}

	return FALSE;
	}

// Creation Helpers

BOOL CUIManager::CreatePage(CString *pFull, CString Name)
{
	CSysProxy System;

	System.Bind();

	CString Nav  = System.GetNavPos();

	CString Menu = CString(IDS_CREATE) + (pFull ? CString(IDS_PAGE) : Name);

	System.SaveCmd(New CCmdMulti(Nav, Menu, navLock));

	if( CreatePage(System, pFull, Name, typePage) ) {

		System.SaveCmd(New CCmdMulti(Nav, Menu, navLock));

		System.ItemUpdated(m_pPages, updateChildren);

		return TRUE;
		}

	System.KillLastCmd();

	return FALSE;
	}

BOOL CUIManager::CreatePage(CSysProxy &System, CString *pFull, CString Name, UINT Type)
{
	CString     Full  = Name;

	CString     Root  = L"";

	CItem     * pRoot = this;

	CMetaItem * pPage = NULL;

	UINT        uFind = Name.FindRev(L'.');

	if( uFind < NOTHING ) {

		Root = Name.Left(uFind);

		Name = Name.Mid (uFind+1);

		if( !CreatePage(System, NULL, Root, typeVoid) ) {

			return FALSE;
			}

		pRoot = m_pPages->FindByName(Root);

		Root  = Root + L'.';
		}

	if( Type == typeVoid ) {

		CItem *pItem = m_pPages->FindByName(Root + Name);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CDispFolder)) ) {

				return TRUE;
				}

			return FALSE;
			}
		}

	if( pFull ) {

		m_pPages->MakeUnique(Full);

		*pFull = Full;
		}
	else {
		if( m_pPages->FindByName(Full) ) {

			return FALSE;
			}
		}

	if( CreatePage(pPage, Type) ) {

		m_pPages->AppendItem(pPage);

		pPage->SetName(Full);

		CNavTreeWnd::CCmdCreate *pCmd = New CNavTreeWnd::CCmdCreate(pRoot, pPage);

		pCmd->m_View = IDVIEW;

		pCmd->m_Side = IDVIEW;

		System.SaveCmd(pCmd);

		return TRUE;
		}

	return FALSE;
	}

BOOL CUIManager::CreatePage(CMetaItem * &pPage, UINT Type)
{
	if( Type == typePage ) {

		pPage = New CDispPage;

		return TRUE;
		}

	if( Type == typeVoid ) {

		pPage = New CDispFolder;

		return TRUE;
		}

	return FALSE;
	}

// End of File
