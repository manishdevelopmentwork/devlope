
//////////////////////////////////////////////////////////////////////////
//
// Large Message Board
//
// Copyright (c) 2001-2002 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_COMMS_H

#define	INCLUDE_COMMS_H

//////////////////////////////////////////////////////////////////////////
//
// ASCII Codes
//

#define	STX		0x02
#define	ETX		0x03
#define	ENQ		0x05
#define	ACK		0x06
#define	DLE		0x10
#define	NAK		0x15
#define	SYN		0x16

//////////////////////////////////////////////////////////////////////////
//
// Op Codes
//

enum {
	opIdle		= 0,
	opLoad		= 1,
	opUpdate	= 2,
	opIdent		= 3,
	opFlash		= 4,
	opTest		= 5
	};

//////////////////////////////////////////////////////////////////////////
//
// Framing
//

// Header

typedef struct CHeader
{
	BYTE	m_bCode;	

	} HEADER;

// Flash

typedef struct CFlash
{
	WORD	wRate;
	
	} FLASH;

// Test

typedef struct CTest
{
	BYTE	bIndex;
	
	} TEST;

// Frame

typedef struct CFrame
{
	HEADER	Header;
	
	union {

		FLASH	Flash;
		TEST	Test;
		BYTE	bData[32];
		} x;
	
	} FRAME;

// End of File

#endif
