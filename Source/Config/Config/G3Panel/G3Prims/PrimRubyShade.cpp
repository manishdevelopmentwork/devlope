
#include "intern.hpp"

#include "PrimRubyShade.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "RubyPatternLib.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Shading Brush
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyShade, CPrimRubyTankFill);

// Constructor

CPrimRubyShade::CPrimRubyShade(void)
{
	m_Mask = 0;
	}

// UI Managament

void CPrimRubyShade::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() ) {

			DoEnables(pHost);
			}

		if( Tag == "Pattern" ) {

			DoEnables(pHost);
			}
		}

	CPrimRubyTankFill::OnUIChange(pHost, pItem, Tag);
	}

// Recoloring

void CPrimRubyShade::Recolor(IGdi *pGdi, PDWORD pData, int cx, int cy)
{
	CUISystem       *pSys = (CUISystem *) m_pDbase->GetSystemItem();

	CRubyPatternLib *pLib = pSys->m_pUI->m_pPatterns;

	BOOL    fHorz   = FALSE;

	PSHADER pShader = pLib->SetGdi( pGdi,
					m_Pattern,
					m_pColor1->GetColor(),
					m_pColor2->GetColor()
					);

	if( pShader ) {

		fHorz = (*pShader)(pGdi, 0, 0);
		}

	CLogBrush Brush;

	pGdi->GetBrush(Brush);

	DWORD c1 = (Brush.m_Rich & 0x80000000) ? Brush.m_Rich : To888(255, Brush.m_Fore);

	UINT  r1 = ((c1 >> 16) & 0xFF);
	UINT  g1 = ((c1 >>  8) & 0xFF);
	UINT  b1 = ((c1 >>  0) & 0xFF);

	DWORD c2 = To888(255, m_pColor3->GetColor());

	UINT  r2 = ((c2 >> 16) & 0xFF);
	UINT  g2 = ((c2 >>  8) & 0xFF);
	UINT  b2 = ((c2 >>  0) & 0xFF);

	UINT  rm = ((m_Mask >> 0) & 1);
	UINT  gm = ((m_Mask >> 1) & 1);
	UINT  bm = ((m_Mask >> 2) & 1);

	UINT  tm = m_Mask ? (255 * (rm + gm + bm)) : 256;

	PrepTankFill();

	if( m_Mode == 1 || m_Mode == 4 ) {

		m_ShadeData = 1 - m_ShadeData;
		}

	if( !fHorz ) {

		int  ty = (m_Mode == 1 || m_Mode == 2) ? num_round(m_ShadeData * cy) : cy;

		bool fs = (m_Mode != 1);

		for( int y = 0; y < cy; y++ ) {

			bool sf = false;

			if( y >= ty ) {

				fs = !fs;

				sf = true;

				ty = cy;
				}

			if( fs ) {
				
				if( pShader ) {

					if( (*pShader)(pGdi, y, cy) || sf ) {

						pGdi->GetBrush(Brush);

						c1 = (Brush.m_Rich & 0x80000000) ? Brush.m_Rich : To888(255, Brush.m_Fore);

						r1 = ((c1 >> 16) & 0xFF);
						g1 = ((c1 >>  8) & 0xFF);
						b1 = ((c1 >>  0) & 0xFF);
						}
					}
				else {
					if( sf ) {

						r1 = ((c1 >> 16) & 0xFF);
						g1 = ((c1 >>  8) & 0xFF);
						b1 = ((c1 >>  0) & 0xFF);
						}
					}
				}
			else {
				r1 = r2;
				g1 = g2;
				b1 = b2;
				}

			int  tx = (m_Mode == 3 || m_Mode == 4) ? num_round(m_ShadeData * cx) : cx;

			UINT r0 = (m_Mode == 4) ? r2 : r1;
			UINT g0 = (m_Mode == 4) ? g2 : g1;
			UINT b0 = (m_Mode == 4) ? b2 : b1;

			for( int x = 0; x < cx; x++ ) {

				if( x >= tx ) {

					r0 = (m_Mode == 4) ? r1 : r2;
					g0 = (m_Mode == 4) ? g1 : g2;
					b0 = (m_Mode == 4) ? b1 : b2;

					tx = cx;
					}

				DWORD Data = *pData;

				DWORD Test = Data & 0xFF000000;

				if( Test ) {

					UINT tv = 0;

					if( m_Mask ) {

						if( rm ) tv += ((Data>>16) & 0xFF);
						if( gm ) tv += ((Data>> 8) & 0xFF);
						if( bm ) tv += ((Data>> 0) & 0xFF);
						}
					else {
						if( Data ) {

							tv = tm;
							}
						}

					if( Test ^ 0xFF000000 ) {

						tv = (tv * (Test >> 24)) >> 8;
						}

					Test |= (r0 * tv / tm) << 16;
					Test |= (g0 * tv / tm) <<  8;
					Test |= (b0 * tv / tm) <<  0;

					*pData = Test;
					}

				pData++;
				}
			}
		}
	else {
		int  tx = (m_Mode == 3 || m_Mode == 4) ? num_round(m_ShadeData * cx) : cx;

		bool fs = (m_Mode != 4);

		for( int x = 0; x < cx; x++ ) {

			bool sf = false;

			if( x >= tx ) {

				fs = !fs;

				sf = true;

				tx = cx;
				}

			if( fs ) {
				
				if( pShader ) {

					if( (*pShader)(pGdi, x, cx) || sf ) {

						pGdi->GetBrush(Brush);

						c1 = (Brush.m_Rich & 0x80000000) ? Brush.m_Rich : To888(255, Brush.m_Fore);

						r1 = ((c1 >> 16) & 0xFF);
						g1 = ((c1 >>  8) & 0xFF);
						b1 = ((c1 >>  0) & 0xFF);
						}
					}
				else {
					if( sf ) {

						r1 = ((c1 >> 16) & 0xFF);
						g1 = ((c1 >>  8) & 0xFF);
						b1 = ((c1 >>  0) & 0xFF);
						}
					}
				}
			else {
				r1 = r2;
				g1 = g2;
				b1 = b2;
				}

			int  ty = (m_Mode == 1 || m_Mode == 2) ? num_round(m_ShadeData * cy) : cy;

			UINT r0 = (m_Mode == 1) ? r2 : r1;
			UINT g0 = (m_Mode == 1) ? g2 : g1;
			UINT b0 = (m_Mode == 1) ? b2 : b1;

			PDWORD pScan = pData++;

			for( int y = 0; y < cy; y++ ) {

				if( y >= ty ) {

					r0 = (m_Mode == 1) ? r1 : r2;
					g0 = (m_Mode == 1) ? g1 : g2;
					b0 = (m_Mode == 1) ? b1 : b2;

					ty = cy;
					}

				DWORD Data = *pScan;

				DWORD Test = Data & 0xFF000000;

				if( Test ) {

					UINT tv = 0;

					if( m_Mask ) {

						if( rm ) tv += ((Data>>16) & 0xFF);
						if( gm ) tv += ((Data>> 8) & 0xFF);
						if( bm ) tv += ((Data>> 0) & 0xFF);
						}
					else {
						if( Data ) {

							tv = tm;
							}
						}

					if( Test ^ 0xFF000000 ) {

						tv = (tv * (Test >> 24)) >> 8;
						}

					Test |= (r0 * tv / tm) << 16;
					Test |= (g0 * tv / tm) <<  8;
					Test |= (b0 * tv / tm) <<  0;

					*pScan = Test;
					}

				pScan += cx;
				}
			}
		}
	}

// Download Support

BOOL CPrimRubyShade::MakeInitData(CInitData &Init)
{
	CPrimRubyTankFill::MakeInitData(Init);

	Init.AddByte(BYTE(m_Mask));

	return TRUE;
	}

// Meta Data

void CPrimRubyShade::AddMetaData(void)
{
	CPrimRubyTankFill::AddMetaData();

	Meta_AddInteger(Mask);

	Meta_SetName((IDS_SHADE));
	}

// Implementation

void CPrimRubyShade::DoEnables(IUIHost *pHost)
{
	pHost->EnableUI(this, "Mask",   m_Pattern > 0);
	pHost->EnableUI(this, "Mode",   m_Pattern > 0);
	pHost->EnableUI(this, "Value",  m_Pattern > 0);
	pHost->EnableUI(this, "Min",    m_Pattern > 0);
	pHost->EnableUI(this, "Max",    m_Pattern > 0);

	pHost->EnableUI(this, "Color3", m_Pattern > 0 && m_Mode > 0);
	}

DWORD CPrimRubyShade::To888(WORD a, WORD c)
{
	DWORD  x = ((c & (31<<0)) << 19) | ((c & (31<<5)) << 6) | ((c & (31<<10)) >> 7);

	return x | ((x >> 5) & 0x070707) | (a << 24);
	}

// End of File
