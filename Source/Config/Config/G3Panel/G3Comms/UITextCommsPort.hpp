
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextCommsPort_HPP

#define INCLUDE_UITextCommsPort_HPP

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Communications Port
//

class CUITextCommsPort : public CUITextEnum
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextCommsPort(void);

	protected:
		// Overridables
		void OnBind(void);

		// Implementation
		void AddData(void);
		void AddData(UINT Data, CString Text);
	};

// End of File

#endif
