
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DiagConsoleString_HPP
	
#define	INCLUDE_DiagConsoleString_HPP

//////////////////////////////////////////////////////////////////////////
//
// String-Base Diagnostics Console
//

class CDiagConsoleString : public IDiagConsole
{
	public:
		// Constructor
		CDiagConsoleString(char **ppBuff);

		// Destructor
		~CDiagConsoleString(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDiagConsole
		void METHOD Write(PCTXT pText);

	protected:
		// Data Members
		ULONG   m_uRefs;
		char ** m_ppBuff;
		UINT    m_uAlloc;
		UINT    m_uPtr;
	};

// End of File

#endif
