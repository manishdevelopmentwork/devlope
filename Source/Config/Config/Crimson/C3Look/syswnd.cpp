
#include "intern.hpp"

#include "watch.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// System View Window
//

// Runtime Class

AfxImplementRuntimeClass(CSystemWnd, CTripleViewWnd);

// Constructor

CSystemWnd::CSystemWnd(void)
{
	m_KeyName    = L"SystemView";

	m_fBusy      = FALSE;

	m_fUsed      = FALSE;

	m_fLockRes   = FALSE;

	m_pSemi      = NULL;

	m_pBlock     = NULL;

	m_uFindPos   = 0;

	m_pResults   = NULL;

	m_pWatchView = NULL;

	m_pWatchWnd  = NULL;

	m_Accel1.Create(L"SystemAccel1");

	m_Accel2.Create(L"SystemAccel2");
	}

// Destructor

CSystemWnd::~CSystemWnd(void)
{
	FreeList(m_StackUndo);

	FreeList(m_StackRedo);
}

// Attributes

CViewWnd * CSystemWnd::GetCatView(CLASS Class) const
{
	return m_pCatView->GetView(Class);
	}

CViewWnd * CSystemWnd::GetItemView(void) const
{
	return m_pViewer;
}

CCmd * CSystemWnd::GetLastCmd(void) const
{
	INDEX Index = m_StackUndo.GetTail();

	if( !m_StackUndo.Failed(Index) ) {

		CCmd *pCmd = m_StackUndo[Index];

		return pCmd;
		}

	return NULL;
	}

CDialog * CSystemWnd::GetSemiModal(void) const
{
	return m_pSemi;
	}

BOOL CSystemWnd::IsNavBusy(void) const
{
	return m_fBusy;
	}

// Navigation

void CSystemWnd::SetNavCheckpoint(void)
{
	if( !m_fBusy ) {

		m_fUsed = TRUE;
		}
	}

void CSystemWnd::SetViewedItem(CItem *pItem)
{
	if( !m_pViewer->IsItem(pItem) ) {

		if( !m_pViewer->IsCurrent() || InTwinMode() || InFloater() ) {

			m_pViewer->Attach(pItem);
			}
		else {
			AfxNull(CWnd).SetFocus();

			m_pViewer->Attach(pItem);

			m_pViewer->SetFocus();
			}

		if( !m_fLockRes ) {

			UpdateResPane();
			}

		CString Init = m_pNavPane->GetNavPos();

		if( !m_fBusy ) {

			if( Init != m_Init ) {

				m_StackFore.Empty();

				if( m_fUsed ) {

					StoreHistory(m_StackBack, m_Init);
					}
				}
			}

		m_fUsed = FALSE;

		m_Init  = Init;
		}
	}

void CSystemWnd::SetNavCategory(CItem *pItem)
{
	m_pCatView->Attach(pItem);
	}

void CSystemWnd::SetResCategory(CItem *pItem)
{
	}

// Dialog Boxes

UINT CSystemWnd::ExecSemiModal(CDialog &Dlg, CString Cat)
{
	CWnd &Focus = GetFocus();

	AfxNull(CWnd).SetFocus();

	BOOL          fForce = ForceResPane();

	CMainWnd    * pMain  = (CMainWnd *) afxMainWnd;

	CDialog     * pSave  = m_pSemi;

	BOOL          fSave  = m_fSemi;

	BOOL          fShow0 = m_pWnd[0]->IsWindowVisible();

	BOOL          fShow2 = m_pWnd[2]->IsWindowVisible();

	m_pSemi = &Dlg;

	m_fSemi = TRUE;

	UpdateBlocker();

	afxMainWnd->RouteCommand(IDM_GLOBAL_SEMI_START, 0);

	CRect Rect = GetClientRect();

	Rect.right = m_Rect[1].left;

	ClientToScreen(Rect);

	Dlg.SetCenterRect(Rect);

	Dlg.Create(ThisObject);

	Dlg.UpdateWindow();

	m_pWnd[0]->ShowWindow(SW_HIDE);

	m_pWnd[2]->ShowWindow(SW_HIDE);

	m_pResPane->SwapCat(Cat);

	pMain->SetSemiModal(m_pSemi);

	UINT uCode = Dlg.SemiModal(pSave, m_pWnd[1]);

	pMain->SetSemiModal(pSave);

	m_pResPane->SwapCat(Cat);

	m_fSemi = fSave;

	m_pSemi = pSave;

	if( fShow0 ) {

		m_pWnd[0]->ShowWindow(SW_SHOWNA);

		m_pWnd[0]->UpdateWindow();
		}

	if( fShow2 ) {

		m_pWnd[2]->ShowWindow(SW_SHOWNA);

		m_pWnd[2]->UpdateWindow();
		}

	if( !m_pSemi ) {

		if( fForce ) {

			CancelSlideOut();
			}

		Focus.SetFocus();

		Dlg.DestroyWindow(TRUE);

		afxMainWnd->RouteCommand(IDM_GLOBAL_SEMI_END, 0);
		}
	else {
		Dlg.DestroyWindow(TRUE);

		afxMainWnd->RouteCommand(IDM_GLOBAL_SEMI_START, 0);
		}

	UpdateBlocker();

	return uCode;
	}

UINT CSystemWnd::ExecSemiModal(CDialog &Dlg)
{
	return ExecSemiModal(Dlg, L"");
	}

// Pane Management

BOOL CSystemWnd::HidePaneOnDrop(UINT uPane)
{
	if( m_uShow[uPane] == showSlide ) {

		CPoint Pos = GetCursorPos();

		ScreenToClient(Pos);

		if( !m_Rect[uPane].PtInRect(Pos) ) {

			m_uShow[uPane] = showHidden;
			
			UpdateLayout(2);

			return TRUE;
			}
		}

	return FALSE;
	}

void CSystemWnd::LockResPane(BOOL fLock)
{
	if( !m_fLockRes && fLock ) {

		m_fLockRes = TRUE;
		}

	if( m_fLockRes && !fLock ) {

		m_fLockRes = FALSE;

		UpdateResPane();
		}
	}

BOOL CSystemWnd::CanSyncPanes(void)
{
	CString Tag = m_pNavPane->GetCat();

	return m_pResPane->HasCat(Tag);
	}

void CSystemWnd::SyncPanes(void)
{
	CString Cat = m_pNavPane->GetCat();

	CString Nav = m_pViewer->GetItem()->GetFixedPath();

	m_pResPane->ShowCat(Cat);

	m_pResPane->SetItem(Nav);

	m_pResPane->SetFocus();
	}

void CSystemWnd::AssumeViewerActive(void)
{
	Select(2);
	}

// Command Management

void CSystemWnd::SaveCmd(CViewWnd *pView, CCmd *pCmd)
{
	if( pCmd->m_View == NOTHING ) {

		pCmd->m_View = FindView(pView);
		}

	if( pCmd->m_Side == NOTHING ) {

		pCmd->m_Side = FindView(&GetFocus());
		}

	m_StackUndo.Append(pCmd);

	FreeList(m_StackRedo);
	}

void CSystemWnd::ExecCmd(CViewWnd *pView, CCmd *pCmd)
{
	if( pCmd->m_View == NOTHING ) {

		pCmd->m_View = FindView(pView);
		}

	if( pCmd->m_Side == NOTHING ) {

		pCmd->m_Side = FindView(&GetFocus());
		}

	m_StackUndo.Append(pCmd);

	pView->ExecCmd(pCmd);

	FreeList(m_StackRedo);
	}

BOOL CSystemWnd::KillLastCmd(void)
{
	INDEX Index = m_StackUndo.GetTail();

	if( !m_StackUndo.Failed(Index) ) {

		CCmd *pCmd = m_StackUndo[Index];

		delete pCmd;

		m_StackUndo.Remove(Index);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::KillUndoList(void)
{
	CString Text;
	
	Text += CString(IDS_THIS_OPERATION);

	Text += L"\n\n";

	Text += CString(IDS_DO_YOU_WANT_TO);

	if( NoYes(Text) == IDYES ) {

		afxMainWnd->UpdateWindow();

		FreeList(m_StackUndo);

		FreeList(m_StackRedo);

		return TRUE;
		}

	return FALSE;
	}

// Global Find

BOOL CSystemWnd::SetFindList(CString Head, CStringArray const &List, BOOL fJump)
{
	if( !List.IsEmpty() ) {

		m_FindHead = Head;

		m_FindList = List;

		m_uFindPos = m_FindList.GetCount();

		m_FindList.Sort();

		if( m_pResults ) {

			m_pResults->Load(m_FindHead, m_FindList);
			}

		if( fJump ) {

			UINT uID = IDM_EDIT_FIND_ALL_NEXT;

			OnEditCommand(uID);
			}

		return TRUE;
		}

	if( m_pResults ) {

		m_pResults->Empty();
		}

	m_FindList.Empty();

	return FALSE;
	}

void CSystemWnd::SetFindPos(UINT uPos)
{
	m_uFindPos = uPos;

	OnEditJumpTo(uPos);
	}

// Update Management

UINT CSystemWnd::RegisterForUpdates(IUpdate *pUpdate)
{
	return UINT(m_Updates.Append(pUpdate));
	}

void CSystemWnd::UnregisterForUpdates(UINT uToken)
{
	m_Updates.Remove(INDEX(uToken));
	}

void CSystemWnd::ItemUpdated(UINT uToken, CItem *pItem, UINT uType)
{
	INDEX n = m_Updates.GetHead();

	while( !m_Updates.Failed(n) ) {

		if( UINT(n) != uToken ) {

			IUpdate *pUpdate = m_Updates[n];

			pUpdate->ItemUpdated(pItem, uType);
			}

		m_Updates.GetNext(n);
		}
	}

// Overribables

void CSystemWnd::OnAttach(void)
{
	m_pDbase     = (CDatabase *) m_pItem;

	m_pNavPane   = New CNavPaneWnd;

	m_pResPane   = New CResPaneWnd;

	m_pCatView   = New CCatViewWnd;

	m_pViewer    = New CItemViewWnd(viewItem, viewEdge | viewCaption);

	m_pWnd[0]    = m_pNavPane;
	
	m_pWnd[1]    = m_pResPane;

	m_pWnd[2]    = m_pViewer;

	m_pNavPane->Attach(m_pDbase);
	}

void CSystemWnd::OnDetach(void)
{
	m_pDbase = NULL;
}

CString CSystemWnd::OnGetNavPos(void)
{
	CString Nav = m_pViewer->GetNavPos();

	if( Nav.IsEmpty() ) {

		Nav = m_pNavPane->GetNavPos();
		}

	return Nav;
	}

BOOL CSystemWnd::OnNavigate(CString const &Nav)
{
	CString Init = m_Init;

	CString Full = GetNavPos();

	if( DoNavigate(Nav) ) {

		if( m_pNavPane->GetNavPos() != Init ) {

			EncodeNav(Full, Init);

			StoreHistory(m_StackBack, Full);
			}
			
		return TRUE;
		}

	return FALSE;
	}

// Routing Control

BOOL CSystemWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pSemi ) {

		if( m_pSemi->RouteMessage(Message, lResult) ) {

			return TRUE;
			}

		switch( Message.message ) {

			case WM_AFX_GETINFO:
			case WM_AFX_CONTROL:
			case WM_AFX_COMMAND:

				break;

			default:

				return FALSE;
			}
		}

	if( Message.message == WM_AFX_COMMAND ) {

		if( HIBYTE(Message.wParam) == IDM_GLOBAL ) {

			if( m_pWatchWnd ) {
				
				m_pWatchWnd->RouteMessage(Message, lResult);
				}

			if( m_pResults ) {

				m_pResults->RouteMessage(Message, lResult);
				}
			}
		}

	if( m_pCatView->RouteMessage(Message, lResult) ) {

		if( Message.message == WM_AFX_ACCEL ) {

			// Have to stop processing when an accelerator
			// is matched or we'll get strange beeping...

			return TRUE;
			}
		}

	return CTripleViewWnd::OnRouteMessage(Message, lResult);
	}

// Message Map

AfxMessageMap(CSystemWnd, CTripleViewWnd)
{
	AfxDispatchAccelerator()

	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_INITMENUPOPUP)
	AfxDispatchMessage(WM_FOCUSNOTIFY)
	AfxDispatchMessage(WM_ACTIVATEAPP)

	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxDispatchControlType(IDM_GO, OnGoControl)
	AfxDispatchCommandType(IDM_GO, OnGoCommand)

	AfxDispatchControl(IDM_EDIT_UNDO, OnEditUndoControl)
	AfxDispatchCommand(IDM_EDIT_UNDO, OnEditUndoCommand)
	AfxDispatchControl(IDM_EDIT_BACK, OnEditBackControl)
	AfxDispatchCommand(IDM_EDIT_BACK, OnEditBackCommand)
	AfxDispatchControl(IDM_EDIT_REDO, OnEditRedoControl)
	AfxDispatchCommand(IDM_EDIT_REDO, OnEditRedoCommand)

	AfxMessageEnd(CSystemWnd)
	};

// Accelerator

BOOL CSystemWnd::OnAccelerator(MSG &Msg)
{
	if( afxMainWnd->IsActive() ) {

		if( m_Accel2.Translate(Msg) ) {

			return TRUE;
			}
		}

	if( m_Accel1.Translate(Msg) ) {

		return TRUE;
		}

	return FALSE;
	}

// Message Handlers

void CSystemWnd::OnPostCreate(void)
{
	m_pCatView->Create(WS_CHILD, CRect(), ThisObject, AfxNull(CMenu), NULL);

	CTripleViewWnd::OnPostCreate();
	}

void CSystemWnd::OnInitPopup(CMenu &Menu, int nIndex, BOOL fSystem)
{
	if( HIBYTE(Menu.GetMenuItemID(0)) == IDM_GO ) {

		m_pNavPane->SendMessage( m_MsgCtx.Msg.message,
					 m_MsgCtx.Msg.wParam,
					 m_MsgCtx.Msg.lParam
					 );
		}
	}

void CSystemWnd::OnFocusNotify(UINT uID, CWnd &Wnd)
{
	if( uID == m_pViewer->GetID() ) {

		SetNavCheckpoint();
		}

	CTripleViewWnd::OnFocusNotify(uID, Wnd);
	}

void CSystemWnd::OnActivateApp(BOOL fActive, DWORD dwThread)
{
	CWnd *pWnd = GetCatView(NULL);

	if( pWnd ) {

		pWnd->SendMessage( m_MsgCtx.Msg.message,
				   m_MsgCtx.Msg.wParam,
				   m_MsgCtx.Msg.lParam
				   );
		}

	CTripleViewWnd::OnActivateApp(fActive, dwThread);
	}

// Edit Commands

BOOL CSystemWnd::OnEditControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_EDIT_FIND_ALL:

			Src.EnableItem(TRUE);

			return TRUE;

		case IDM_EDIT_FIND_ALL_NEXT:
		case IDM_EDIT_FIND_ALL_PREV:

			Src.EnableItem(!m_FindList.IsEmpty());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::OnEditCommand(UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_FIND_ALL:

			if( !m_pDbase->GetSystemItem()->GlobalFind() ) {

				if( m_pResults ) {

					m_pResults->Empty();
					}

				m_FindList.Empty();
				}

			return TRUE;
		
		case IDM_EDIT_FIND_ALL_NEXT:

			if( m_uFindPos == m_FindList.GetCount() ) {

				m_uFindPos = NOTHING;
				}

			if( ++m_uFindPos == m_FindList.GetCount() ) {

				CString Text;

				Text.Printf(IDS_FMT_END_OF_LIST, m_FindHead);

				afxThread->SetStatusText(Text);

				if( m_pResults ) {

					m_pResults->Deselect();
					}

				MessageBeep(0);

				return TRUE;
				}

			OnEditJumpTo(m_uFindPos);

			return TRUE;
		
		case IDM_EDIT_FIND_ALL_PREV:

			if( --m_uFindPos == NOTHING ) {

				m_uFindPos = m_FindList.GetCount();

				CString Text;

				Text.Printf(IDS_FMT_START_OF_LIST, m_FindHead);

				afxThread->SetStatusText(Text);

				if( m_pResults ) {

					m_pResults->Deselect();
					}

				MessageBeep(0);

				return TRUE;
				}

			OnEditJumpTo(m_uFindPos);

			return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::OnEditJumpTo(UINT uPos)
{
	UINT uCount = m_FindList.GetCount();

	if( uPos < uCount ) {

		CStringArray List;

		m_FindList[uPos].Tokenize(List, '\n');

		CString Text;

		Text.Printf( L"%s : %u of %u : %s",
			     m_FindHead,
			     uPos+1,
			     m_FindList.GetCount(),
			     List[0]
			     );

		afxThread->SetStatusText(Text);

		Navigate(List[1]);

		if( !InTwinMode() && !InFloater() ) {

			m_pViewer->SetFocus();
			}
		
		if( m_pResults ) {

			m_pResults->Select(uPos);
			}

		return TRUE;
		}

	return FALSE;
	}

// View Commands

BOOL CSystemWnd::OnViewControl(UINT uID, CCmdSource &Src)
{
	switch( uID) {

		case IDM_VIEW_FIND_RESULTS:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_pResults && m_pResults->IsWindowVisible());

			return TRUE;

		case IDM_VIEW_WATCH:

			Src.EnableItem(TRUE);

			Src.CheckItem (m_pWatchWnd && m_pWatchWnd->IsWindowVisible());

			return TRUE;
		}
	
	return FALSE;
	}

BOOL CSystemWnd::OnViewCommand(UINT uID)
{
	switch( uID) {

		case IDM_VIEW_FIND_RESULTS:

			if( !m_pResults ) {

				CRect Rect = m_pViewer->GetWindowRect() - 64;

				m_pResults = New CResultsWnd(this);

				Rect.top   = Rect.top + 1 * Rect.cy() / 3 + 8;

				m_pResults->Create(Rect);

				if( !m_FindList.IsEmpty() ) {

					m_pResults->Load  (m_FindHead, m_FindList);

					m_pResults->Select(m_uFindPos);
					}

				AddFloater(m_pResults);
				}

			if( m_pResults->IsShown() ) {

				m_pResults->Show(FALSE);
				}
			else
				m_pResults->Show(TRUE);

			return TRUE;

		case IDM_VIEW_WATCH:

			if( !m_pWatchWnd ) {

				CRect Rect   = m_pViewer->GetWindowRect() - 64;

				if( (m_pWatchView = m_pDbase->GetSystemItem()->GetWatchWindow()) ) {

					m_pWatchWnd  = New CWatchWnd(this, m_pWatchView);

					Rect.bottom  = Rect.top + 1 * Rect.cy() / 3 - 8;

					m_pWatchWnd->Create(Rect);

					AddFloater(m_pWatchWnd);
					}
				}

			if( m_pWatchWnd ) {

				if( m_pWatchWnd->IsShown() ) {

					m_pWatchWnd->Show(FALSE);
					}
				else {
					m_pWatchWnd->Show(TRUE);
					}
				}

			return TRUE;
		}

	return FALSE;
	}

// Go Commands

BOOL CSystemWnd::OnGoControl(UINT uID, CCmdSource &Src)
{
	switch( uID ) {

		case IDM_GO_BACK:

			Src.EnableItem(!m_StackBack.IsEmpty());

			return TRUE;

		case IDM_GO_FORWARD:

			Src.EnableItem(!m_StackFore.IsEmpty());

			return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::OnGoCommand(UINT uID)
{
	switch( uID ) {

		case IDM_GO_BACK:

			OnGoBack();

			return TRUE;

		case IDM_GO_FORWARD:

			OnGoForward();

			return TRUE;
		}

	return FALSE;
	}

void CSystemWnd::OnGoBack(void)
{
	StepBack();
	}

void CSystemWnd::OnGoForward(void)
{
	StepForward();
	}

BOOL CSystemWnd::StepBack(void)
{
	return StepHistory(m_StackBack, m_StackFore);
	}

BOOL CSystemWnd::StepForward(void)
{
	return StepHistory(m_StackFore, m_StackBack);
	}

// Edit Commands

BOOL CSystemWnd::OnEditUndoControl(UINT uID, CCmdSource &Src)
{
	Enable(Src, m_StackUndo, IDS_UNDO, L"Z");
	
	return TRUE;
	}

BOOL CSystemWnd::OnEditBackControl(UINT uID, CCmdSource &Src)
{
	Enable(Src, m_StackUndo, IDS_UNDO, L"Z");
	
	return TRUE;
	}

BOOL CSystemWnd::OnEditRedoControl(UINT uID, CCmdSource &Src)
{
	Enable(Src, m_StackRedo, IDS_REDO, L"Y");
	
	return TRUE;
	}

BOOL CSystemWnd::OnEditUndoCommand(UINT uID)
{
	Action(m_StackUndo, m_StackRedo, uID);

	m_fUsed = TRUE;

	return TRUE;
	}

BOOL CSystemWnd::OnEditBackCommand(UINT uID)
{
	Action(m_StackUndo, m_StackRedo, uID);

	m_fUsed = TRUE;

	return TRUE;
	}

BOOL CSystemWnd::OnEditRedoCommand(UINT uID)
{
	Action(m_StackRedo, m_StackUndo, uID);

	m_fUsed = TRUE;

	return TRUE;
	}

void CSystemWnd::Enable(CCmdSource &Src, CCmdList &List, UINT uVerb, CString Key)
{
	CString Verb  = uVerb;

	CString Text  = CFormat(IDS_CANT, Verb);

	INDEX   Index = List.GetTail();

	if( !List.Failed(Index) ) {

		CCmd *pCmd = List[Index];

		Text = CFormat(IDS_FORMAT, Verb, pCmd->m_Menu);
		}

	Src.SetItemText(CFormat(IDS_TCTRL, Text, Key));

	Src.EnableItem(!List.Failed(Index));
	}

BOOL CSystemWnd::Action(CCmdList &List1, CCmdList &List2, UINT uID)
{
	if( !List1.IsEmpty() ) {

		CString Init = m_Init;

		CString Full = GetNavPos();

		INDEX   Tail = List1.GetTail();

		CCmd *  pCmd = List1[Tail];

		DoNavigate(pCmd->m_Item);

		if( pCmd->IsKindOf(AfxRuntimeClass(CCmdMulti)) ) {

			UINT uCode = ((CCmdMulti *) pCmd)->m_uCode;

			BOOL fInit = TRUE;

			if( uCode == navLock ) {

				afxThread->SetWaitMode(TRUE);

				LockWindowUpdate(afxMainWnd->GetHandle());
				}

			for(;;) {

				if( uCode == navSeq || uCode == navAll || uCode == navLock ) {
					
					if( !fInit ) {
					
						DoNavigate(pCmd->m_Item);
						}
					}

				Action(pCmd, uID);

				AfxAssume(pCmd);

				CLASS   Class = AfxPointerClass(pCmd);

				CString Item  = pCmd->m_Item;

				List1.Remove(Tail);

				if( uID == IDM_EDIT_BACK ) {

					delete pCmd;
					}
				else
					List2.Append(pCmd);

				if( Class ) {

					if( Class->IsKindOf(AfxRuntimeClass(CCmdMulti)) ) {

						if( !fInit ) {

							if( uCode == navAll || uCode == navLock ) {

								DoNavigate(Item);
							}

							if( uCode == navLock ) {

								LockWindowUpdate(NULL);

								afxThread->SetWaitMode(FALSE);
							}

							break;
						}

						fInit = FALSE;
					}
				}

				Tail = List1.GetTail();

				pCmd = List1[Tail];
				}
			}
		else {
			Action(pCmd, uID);

			List1.Remove(Tail);

			if( uID == IDM_EDIT_BACK ) {

				delete pCmd;
				}
			else
				List2.Append(pCmd);
			}

		if( m_pNavPane->GetNavPos() != Init ) {

			EncodeNav(Full, Init);

			StoreHistory(m_StackBack, Full);
			}
		}

	return TRUE;
	}

void CSystemWnd::Action(CCmd *pCmd, UINT uID)
{
	UINT uView = pCmd->m_View - IDVIEW;

	UINT uSide = pCmd->m_Side - IDVIEW;

	CViewWnd *pView = m_pWnd[uView];

	Action(pView, pCmd, uID);

	if( uSide < 2 ) {
		
		if( m_uShow[uSide] == showHidden ) {

			m_uShow[uSide] = showSlide;

			UpdateLayout(uSide);
			}
		}
	}

void CSystemWnd::Action(CViewWnd *pView, CCmd *pCmd, UINT uID)
{
	switch( uID ) {

		case IDM_EDIT_REDO:
		
			pView->ExecCmd(pCmd);

			break;

		case IDM_EDIT_UNDO:
		case IDM_EDIT_BACK:
	
			pView->UndoCmd(pCmd);

			break;
		}
	}

// Overridables

void CSystemWnd::OnUpdateLayout(void)
{
	}

// Implementation

UINT CSystemWnd::FindView(CWnd *pWnd)
{
	while( pWnd->IsWindow() ) {

		if( pWnd->GetParent().GetHandle() == m_hWnd ) {

			return pWnd->GetID();
			}

		pWnd = &pWnd->GetParent();
		}

	return IDVIEW + 2;
	}

BOOL CSystemWnd::IsSameInit(CString const &Nav1, CString const &Nav2)
{
	return Nav1.Left(Nav1.FindOne(L"*#")) == Nav2.Left(Nav2.FindOne(L"*#"));
	}

BOOL CSystemWnd::EncodeNav(CString &Full, CString const &Init)
{
	if( Full != Init ) {

		UINT n = Init.GetLength();

		if( Full[n] == '/' ) {

			Full.SetAt(n, L'*');
	
			return TRUE;
			}

		if( Full[n] == ':' ) {

			Full.SetAt(n, L'#');
	
			return TRUE;
			}

		/*AfxAssert(FALSE);*/
		}

	return FALSE;
	}

BOOL CSystemWnd::DecodeNav(CString &Full)
{
	UINT p1 = Full.Find(L'*');

	UINT p2 = Full.Find(L'#');

	if( p1 < NOTHING ) {

		Full.SetAt(p1, '/');

		return TRUE;
		}

	if( p2 < NOTHING ) {

		Full.SetAt(p2, ':');

		return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::StepHistory(CStringList &List1, CStringList &List2)
{
	CString Init = m_Init;

	CString Full = GetNavPos();

	EncodeNav(Full, Init);

	for(;;) {

		INDEX Index = List1.GetTail();

		if( !List1.Failed(Index) ) {

			CString Next = List1[Index];

			List1.Remove(Index);

			if( !IsSameInit(Full, Next) ) {

				StoreHistory(List2, Full);

				DecodeNav(Next);

				if( DoNavigate(Next) ) {

					return TRUE;
					}
				}

			continue;
			}
	
		return FALSE;
		}
	}

BOOL CSystemWnd::StoreHistory(CStringList &List, CString Nav)
{
	if( Nav.GetLength() ) {

		INDEX Index = List.GetTail();

		while( !List.Failed(Index) ) {

			if( IsSameInit(List[Index], Nav) ) {

				if( Index == List.GetTail() ) {

					return FALSE;
					}

				List.Remove(Index);

				break;
				}

			if( TRUE ) {

				// NOTE -- Allow duplicate non-adjacent entries.
		
				break;
				}

			List.GetPrev(Index);
			}

		List.Append(Nav);

		return TRUE;
		}

	return FALSE;
	}

BOOL CSystemWnd::DoNavigate(CString Nav)
{
	if( !m_fBusy ) {

		m_fBusy = TRUE;

		m_pViewer->SetPreNav(Nav);

		if( m_pNavPane->Navigate(Nav) ) {

			m_pViewer->EndPreNav(TRUE);

			m_fBusy = FALSE;
	
			return TRUE;
			}

		m_pViewer->EndPreNav(FALSE);

		m_fBusy = FALSE;

		return FALSE;
		}

	AfxAssert(FALSE);

	return FALSE;
	}

void CSystemWnd::FreeList(CCmdList &List)
{
	INDEX i;

	while( !List.Failed(i = List.GetHead()) ) {

		delete List[i];

		List.Remove(i);
		}
	}

void CSystemWnd::UpdateResPane(void)
{
	m_pResPane->ShowNavCats(m_pNavPane);
	}

BOOL CSystemWnd::ForceResPane(void)
{
	if( !m_pSemi ) {

		CancelSlideOut();

		if( m_uShow[1] == showHidden ) {

			m_uShow[1] = showSlide;

			UpdateLayout(NOTHING);

			return TRUE;
			}
		}

	return FALSE;
	}

void CSystemWnd::UpdateBlocker(void)
{
	if( m_pSemi ) {

		if( !m_pBlock ) {

			CRect Rect1 = GetClientRect();

			CRect Rect2 = m_Rect[0];

			CRect Rect3 = m_Rect[2];

			m_pBlock = New CBlockerWnd(this, m_pSemi, Rect1 | Rect2 | Rect3);

			m_pBlock->Create();
			}
		else
			m_pBlock->SetDialog(m_pSemi);
		}
	else {
		m_pBlock->ShowWindow(SW_HIDE);

		m_pBlock->DestroyWindow(FALSE);

		m_pBlock = NULL;
		}
	}

// End of File
