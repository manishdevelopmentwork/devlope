
#include "Intern.hpp"

#include "BaseSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "Socket.hpp"

#include "NativeSocketManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Base Socket Object
//

// Constructor

CBaseSocket::CBaseSocket(void)
{
	StdSetRef();

	m_hSocket = NULL;

	m_uState  = stateInit;

	m_fDebug  = FALSE;

	CNativeSocketManager::m_pThis->AddSocket(this);
}

// Destructor

CBaseSocket::~CBaseSocket(void)
{
	CNativeSocketManager::m_pThis->FreeSocket(this);
}

// IUnknown

HRESULT CBaseSocket::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(ISocket);

	StdQueryInterface(ISocket);

	return E_NOINTERFACE;
}

ULONG CBaseSocket::AddRef(void)
{
	StdAddRef();
}

ULONG CBaseSocket::Release(void)
{
	StdRelease();
}

// ISocket

HRESULT CBaseSocket::Listen(WORD Loc)
{
	return ((ISocket *) this)->Listen(IP_EMPTY, Loc);
}

HRESULT CBaseSocket::Connect(IPADDR const &IP, WORD Rem)
{
	return ((ISocket *) this)->Connect(IP, IP_EMPTY, Rem, 0);
}

HRESULT CBaseSocket::Connect(IPADDR const &IP, WORD Rem, WORD Loc)
{
	return ((ISocket *) this)->Connect(IP, IP_EMPTY, Rem, Loc);
}

HRESULT CBaseSocket::Recv(PBYTE pData, UINT &uSize, UINT uTime)
{
	if( m_uState == stateOpen || m_uState == stateClosed ) {

		if( _select_read(m_hSocket, uTime) == 1 ) {

			return Recv(pData, uSize);
		}

		uSize = 0;
	}

	return E_FAIL;
}

HRESULT CBaseSocket::Recv(PBYTE pData, UINT &uSize)
{
	if( m_uState == stateOpen || m_uState == stateClosed ) {

		int nRead = 0;

		if( pData ) {

			nRead = _recv(m_hSocket, PSTR(pData), uSize, 0);
		}
		else {
			BYTE b = 0;

			nRead  = _recv(m_hSocket, PSTR(&b), 1, MSG_PEEK);
		}

		if( nRead > 0 ) {

			Debug("recv", pData, nRead);

			uSize = nRead;

			return S_OK;
		}
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CBaseSocket::Send(PBYTE pData, UINT &uSize)
{
	if( m_uState == stateOpen ) {

		int nSent = _send(m_hSocket, PSTR(pData), uSize, 0);

		if( nSent >= 0 ) {

			uSize = nSent;

			Debug("send", pData, uSize);

			return S_OK;
		}
		else {
			if( errno != EINTR && errno != EAGAIN ) {

				CloseHandle();
			}
		}
	}

	uSize = 0;

	return E_FAIL;
}

HRESULT CBaseSocket::Recv(CBuffer * &pBuff, UINT uTime)
{
	if( m_uState == stateOpen ) {

		if( _select_read(m_hSocket, uTime) == 1 ) {

			return Recv(pBuff);
		}
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CBaseSocket::Recv(CBuffer * &pBuff)
{
	if( m_uState == stateOpen || m_uState == stateClosed ) {

		pBuff = BuffAllocate(0);

		int   nLimit = pBuff->GetTailSpace();

		PBYTE pData  = pBuff->GetData() + pBuff->GetSize();

		int   nRead  = _recv(m_hSocket, PSTR(pData), nLimit, 0);

		if( nRead > 0 ) {

			Debug("recv", pData, nRead);

			pBuff->AddTail(nRead);

			return S_OK;
		}

		BuffRelease(pBuff);
	}

	pBuff = NULL;

	return E_FAIL;
}

HRESULT CBaseSocket::Send(CBuffer *pBuff)
{
	if( m_uState == stateOpen ) {

		PBYTE pData = pBuff->GetData();

		UINT  uData = pBuff->GetSize();

		for(;;) {

			INT nSent = _send(m_hSocket, PTXT(pData), uData, 0);

			if( nSent >= 0 ) {

				Debug("send", pData, uData);

				if( !(uData -= nSent) ) {

					BuffRelease(pBuff);

					return S_OK;
				}

				pData += nSent;
			}
			else {
				if( errno != EINTR && errno != EAGAIN ) {

					CloseHandle();

					break;
				}
			}

			Sleep(5);
		}
	}

	return E_FAIL;
}

HRESULT CBaseSocket::GetLocal(IPADDR &IP)
{
	WORD Port = 0;

	return GetLocal(IP, Port);
}

HRESULT CBaseSocket::GetRemote(IPADDR &IP)
{
	WORD Port = 0;

	return GetRemote(IP, Port);
}

HRESULT CBaseSocket::GetLocal(IPADDR &IP, WORD &Port)
{
	if( m_hSocket ) {

		sockaddr_in loc;

		socklen_t size = sizeof(loc);

		if( _getsockname(m_hSocket, (sockaddr *) &loc, &size) == 0 ) {

			if( loc.sin_family == AF_INET ) {

				IP.m_dw = loc.sin_addr.s_addr;

				Port = NetToHost(loc.sin_port);

				return S_OK;
			}
		}
	}

	return E_FAIL;
}

HRESULT CBaseSocket::GetRemote(IPADDR &IP, WORD &Port)
{
	if( m_hSocket ) {

		sockaddr_in rem;

		socklen_t size = sizeof(rem);

		if( _getpeername(m_hSocket, (sockaddr *) &rem, &size) == 0 ) {

			if( rem.sin_family == AF_INET ) {

				IP.m_dw = rem.sin_addr.s_addr;

				Port = NetToHost(rem.sin_port);

				return S_OK;
			}
		}
	}

	return E_FAIL;
}

HRESULT CBaseSocket::SetOption(UINT uOption, UINT uValue)
{
	if( uOption == OPT_DEBUG ) {

		m_fDebug = uValue ? TRUE : FALSE;

		return S_OK;
	}

	return E_FAIL;
}

// Implementation

void CBaseSocket::SetNonBlocking(void)
{
	ULONG uEnable = TRUE;

	_ioctl(m_hSocket, FIONBIO, &uEnable);
}

void CBaseSocket::AllowDupAddress(void)
{
	ULONG uEnable = TRUE;

	_setsockopt(m_hSocket, SOL_SOCKET, SO_REUSEADDR, PCTXT(&uEnable), sizeof(uEnable));

	_setsockopt(m_hSocket, SOL_SOCKET, SO_REUSEPORT, PCTXT(&uEnable), sizeof(uEnable));
}

BOOL CBaseSocket::LoadAddress(sockaddr_in &addr, IPADDR const &IP, WORD Port)
{
	memset(&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;

	if( IP.m_dw || Port ) {

		addr.sin_addr.s_addr = IP.m_dw;

		addr.sin_port = HostToNet(Port);

		return TRUE;
	}

	return FALSE;
}

BOOL CBaseSocket::LoadAddress(sockaddr_in &addr, WORD Port)
{
	memset(&addr, 0, sizeof(addr));

	addr.sin_family = AF_INET;

	if( Port ) {

		addr.sin_port = HostToNet(Port);

		return TRUE;
	}

	return FALSE;
}

void CBaseSocket::CloseHandle(void)
{
	if( m_hSocket ) {

		_close(m_hSocket);

		m_hSocket = NULL;
	}

	m_uState = stateError;
}

void CBaseSocket::Debug(PCTXT pType, PCBYTE pData, UINT uSize)
{
	#if defined(_DEBUG)

	if( m_fDebug ) {

		CIpAddr LocIp, RemIp;

		WORD    LocPort, RemPort;

		GetLocal(LocIp, LocPort);

		GetRemote(RemIp, RemPort);

		AfxTrace("Local=%s:%u Remote=%s:%u Type=%s\n",
			 LocIp.GetAsText(),
			 LocPort,
			 RemIp.GetAsText(),
			 RemPort,
			 pType
		);

		if( uSize ) {

			AfxDump(pData, uSize);
		}
	}

	#endif
}

// End of File
