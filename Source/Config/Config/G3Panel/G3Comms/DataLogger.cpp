
#include "Intern.hpp"

#include "DataLogger.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2020 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "DataLog.hpp"
#include "DataLogInfo.hpp"
#include "DataLogList.hpp"
#include "TagFolder.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Data Logger
//

// Dynamic Class

AfxImplementDynamicClass(CDataLogger, CUIItem);

// Constructor

CDataLogger::CDataLogger(void)
{
	m_pLogs       = New CDataLogList;

	m_pInfo       = New CDataLogInfo;

	m_EnableBatch = FALSE;

	m_EnableSets  = FALSE;

	m_CSVEncode   = 0;

	m_BatchCount  = 10;

	m_BatchDrive  = 0;

	m_Handle      = HANDLE_NONE;
}

// UI Creation

CViewWnd * CDataLogger::CreateView(UINT uType)
{
	if( uType == viewNavigation ) {

		CLASS Class = AfxNamedClass(L"CDataLoggerNavTreeWnd");

		return AfxNewObject(CViewWnd, Class);
	}

	if( uType == viewItem ) {

		if( m_pDbase->GetSoftwareGroup() >= SW_GROUP_3B ) {

			return CUIItem::CreateView(uType);
		}

		return NULL;
	}

	return CUIItem::CreateView(uType);
}

// UI Loading

BOOL CDataLogger::OnLoadPages(CUIPageList *pList)
{
	if( !C3OemFeature(L"OemSD", FALSE) ) {

		pList->Append(New CUIStdPage(AfxThisClass(), 1));

		return FALSE;
	}

	pList->Append(New CUIStdPage(CString(IDS_SETUP), AfxThisClass(), 2));

	pList->Append(New CUIStdPage(CString(IDS_GROUPS), AfxThisClass(), 3));

	pList->Append(New CUIStdPage(CString(IDS_HEADER), AfxThisClass(), 4));

	return TRUE;
}

// UI Update

void CDataLogger::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "EnableBatch" ) {

		pHost->EnableUI("EnableSets", m_EnableBatch);

		pHost->EnableUI("BatchCount", m_EnableBatch);

		pHost->EnableUI("BatchDrive", m_EnableBatch);
	}

	CUIItem::OnUIChange(pHost, pItem, Tag);
}

// Operations

BOOL CDataLogger::CreateLog(CString Name)
{
	return CreateLog(NULL, Name);
}

void CDataLogger::PostConvert(void)
{
	UINT uGroup = m_pDbase->GetSoftwareGroup();

	if( uGroup <= SW_GROUP_3A ) {

		if( uGroup <= SW_GROUP_2 ) {

			m_pLogs->DeleteAllItems(TRUE);
		}

		m_EnableBatch = FALSE;
	}
}

// Item Naming

CString CDataLogger::GetHumanName(void) const
{
	return CString(IDS_DATA_LOGGER);
}

// Download Support

BOOL CDataLogger::MakeInitData(CInitData &Init)
{
	Init.AddWord(IDC_LOGGER);

	CMetaItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_EnableBatch));
	Init.AddByte(BYTE(m_EnableSets));
	Init.AddWord(WORD(m_BatchCount));
	Init.AddByte(BYTE(m_BatchDrive));
	Init.AddByte(BYTE(m_CSVEncode));

	Init.AddItem(itemSimple, m_pLogs);

	if( C3OemFeature(L"OemSD", FALSE) ) {

		Init.AddByte(1);

		Init.AddItem(itemSimple, m_pInfo);
	}
	else
		Init.AddByte(0);

	return TRUE;
}

// Save Filter

BOOL CDataLogger::SaveProp(CString const &Tag) const
{
	if( Tag == "Info" ) {

		return C3OemFeature(L"OemSD", FALSE);
	}

	return CMetaItem::SaveProp(Tag);
}

// Meta Data Creation

void CDataLogger::AddMetaData(void)
{
	CMetaItem::AddMetaData();

	Meta_AddInteger(Handle);
	Meta_AddInteger(EnableBatch);
	Meta_AddInteger(EnableSets);
	Meta_AddInteger(BatchCount);
	Meta_AddInteger(CSVEncode);
	Meta_AddObject(Info);
	Meta_AddCollect(Logs);
	Meta_AddInteger(BatchDrive);

	Meta_SetName((IDS_DATA_LOGGER));
}

// Creation Helpers

BOOL CDataLogger::CreateLog(CString *pFull, CString Name)
{
	CSysProxy System;

	System.Bind();

	CString Nav  = System.GetNavPos();

	CString Menu = CString(IDS_CREATE_2) + (pFull ? CString(IDS_LOG) : Name);

	System.SaveCmd(New CCmdMulti(Nav, Menu, navLock));

	if( CreateLog(System, pFull, Name, typeLog) ) {

		System.SaveCmd(New CCmdMulti(Nav, Menu, navLock));

		System.ItemUpdated(m_pLogs, updateChildren);

		return TRUE;
	}

	System.KillLastCmd();

	return FALSE;
}

BOOL CDataLogger::CreateLog(CSysProxy &System, CString *pFull, CString Name, UINT Type)
{
	CString     Root  = L"";

	CItem     * pRoot = this;

	CMetaItem * pItem = NULL;

	UINT        uFind = Name.FindRev(L'.');

	if( uFind < NOTHING ) {

		Root = Name.Left(uFind);

		Name = Name.Mid(uFind+1);

		if( !CreateLog(System, NULL, Root, typeVoid) ) {

			return FALSE;
		}

		pRoot = m_pLogs->FindByName(Root);

		Root  = Root + L'.';
	}

	if( Type == typeVoid ) {

		CItem *pItem = m_pLogs->FindByName(Root + Name);

		if( pItem ) {

			if( pItem->IsKindOf(AfxRuntimeClass(CTagFolder)) ) {

				return TRUE;
			}

			return FALSE;
		}
	}

	CString Full = Root + Name;

	if( pFull ) {

		m_pLogs->MakeUnique(Full);

		*pFull = Full;
	}
	else {
		if( m_pLogs->FindByName(Full) ) {

			return FALSE;
		}
	}

	if( CreateLog(pItem, Type) ) {

		AfxAssume(pItem);

		CString Base  = Full.Mid(Root.GetLength());

		CLASS   Class = AfxPointerClass(pItem);

		CNavTreeWnd::CCmdCreate *pCmd = New CNavTreeWnd::CCmdCreate(pRoot, Base, Class);

		m_pLogs->AppendItem(pItem);

		pItem->SetFixedName(pCmd->m_Fixed);

		pItem->SetName(Full);

		pCmd->m_Make = pItem->GetFixedPath();

		pCmd->m_Menu = CFormat(pCmd->m_Menu, Full);

		pCmd->m_View = IDVIEW;

		pCmd->m_Side = IDVIEW;

		System.SaveCmd(pCmd);

		return TRUE;
	}

	return FALSE;
}

BOOL CDataLogger::CreateLog(CMetaItem * &pItem, UINT Type)
{
	if( Type == typeLog ) {

		pItem = New CDataLog;

		return TRUE;
	}

	if( Type == typeVoid ) {

		pItem = New CTagFolder;

		return TRUE;
	}

	return FALSE;
}

// End of File
