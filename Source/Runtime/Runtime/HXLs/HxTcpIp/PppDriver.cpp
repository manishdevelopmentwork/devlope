
#include "Intern.hpp"

#include "PppDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Emplolyed Classes
//

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Packet Driver
//

// Instantiators

IPacketDriver * Create_Ppp(CConfigPpp const &Config)
{
	return New CPppDriver(Config);
	}

// Constructor

CPppDriver::CPppDriver(CConfigPpp const &Config)
{
	m_Config   = Config;

	m_fServer  = (Config.m_uMode == pppServer);

	m_pDynDns  = NULL; // new CDynDNS(Config);

	m_pSink    = NULL;

	m_pRouter  = NULL;

	m_uFace    = NOTHING;

	m_pPpp	   = new CPpp(this, Config);

	m_pDone    = Create_AutoEvent();

	m_hThread  = NULL;

	m_fCapture = false;

	StdSetRef();
	}

// Destructor

CPppDriver::~CPppDriver(void)
{
	piob->RevokeSingleton("net.pcap", m_uFace);

	delete m_pPpp;

	AfxRelease(m_pDone);
	}

// Operations

void CPppDriver::LinkActive(BOOL fActive, IPREF DNS)
{
	m_Dns = DNS;

	if( fActive ) {

		m_pSink->OnLinkActive(m_uFace);
		}
	else
		m_pSink->OnLinkDown(m_uFace);

//	m_pDynDns->LinkActive(fActive, m_Dns);
	}

// Capture

void CPppDriver::Capture(WORD wProtocol, CBuffer *pBuff, BOOL fSend)
{
	if( m_fCapture ) {

		if( wProtocol == PROT_IP ) {

			CIpHeader *pIP = (CIpHeader *) pBuff->GetData();

			if( !m_CaptFilter.StoreIp(pIP, fSend) ) {

				return;
				}
			}
		else {
			if( !m_CaptFilter.StoreArp(fSend) ) {

				return;
				}
			}

		CPppFrame *pPpp   = (CPppFrame *) pBuff->AddHead(sizeof(CPppFrame));

		pPpp->m_bAddress  = 0xFF;

		pPpp->m_bControl  = 0x03;

		pPpp->m_wProtocol = wProtocol;

		pPpp->HostToNet();

		*pBuff->AddHead(1) = fSend ? 1 : 0;

		m_CaptBuffer.Capture(pBuff->GetData(), pBuff->GetSize());

		pBuff->StripHead(1 + sizeof(CPppFrame));
		}
	}

// IUnknown

HRESULT CPppDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown  (IPacketDriver);

	StdQueryInterface(IPacketDriver);

	StdQueryInterface(IClientProcess);

	StdQueryInterface(IPacketCapture);

	return E_NOINTERFACE;
	}

ULONG CPppDriver::AddRef(void)
{
	StdAddRef();
	}

ULONG CPppDriver::Release(void)
{
	StdRelease();
	}

// IPacket

bool CPppDriver::Bind(IRouter *pRouter)
{
	m_pRouter = pRouter;

	return true;
	}

bool CPppDriver::Bind(IPacketSink *pSink, UINT uFace)
{
	m_pSink = pSink;

	m_uFace = uFace;

	piob->RegisterSingleton("net.pcap", uFace, (IPacketCapture *) this);

	m_pPpp->Bind(m_pRouter, m_pSink, m_uFace);

	return true;
	}

CString CPppDriver::GetDeviceName(void)
{
	return m_Config.m_Display;
	}

CString CPppDriver::GetStatus(void)
{
	return m_pPpp->GetStatus();
	}

bool CPppDriver::SetIpAddress(IPREF Ip, IPREF Mask)
{
	return true;
	}

bool CPppDriver::SetIpGateway(IPREF Gate)
{
	return true;
	}

bool CPppDriver::GetMacAddress(MACADDR &Addr)
{
	return false;
	}

bool CPppDriver::GetDhcpOption(BYTE bType, UINT &Data)
{
	if( bType ) {

		if( bType == 0xFF ) {

			Data = 0;

			return true;
			}

		if( bType == 0x06 ) {

			if( !m_fServer ) {

				Data = m_Dns.GetValue();

				return true;
				}
			}

		return false;
		}

	Data = 1;

	return true;
	}

bool CPppDriver::GetSockOption(UINT uCode, UINT &uValue)
{
	return false;
	}

bool CPppDriver::SetMulticast(IPADDR *pList, UINT uList)
{
	return false;
	}

bool CPppDriver::Open(void)
{
	m_hThread = CreateClientThread(this, 0, 82000);

	return true;
	}

bool CPppDriver::Close(void)
{
	if( m_hThread ) {

		m_pPpp->Close();

		m_pPpp->StopTask();

		m_pDone->Wait(FOREVER);

		m_pSink->OnLinkDown(m_uFace);

		DestroyThread(m_hThread);

		m_hThread = NULL;

		return true;
		}

	return false;
	}

void CPppDriver::Poll(void)
{
//	m_pDynDns->Service();
	}

bool CPppDriver::Send(IPREF Ip, CBuffer *pBuff)
{
	m_pPpp->PutIP(pBuff);

	return true;
	}

// IPacketCapture

BOOL CPppDriver::IsCaptureRunning(void)
{
	return m_fCapture ? TRUE : FALSE;
	}

PCSTR CPppDriver::GetCaptureFilter(void)
{
	return m_CaptFilter.GetFilter();
	}

UINT CPppDriver::GetCaptureSize(void)
{
	return m_fCapture ? 0 : m_CaptBuffer.GetSize();
	}

BOOL CPppDriver::CopyCapture(PBYTE pData, UINT uSize)
{
	if( !m_fCapture ) {
		
		UINT uUsed = m_CaptBuffer.GetSize();

		if( uUsed && uSize <= uUsed ) {

			memcpy(pData, m_CaptBuffer.GetData(), uUsed);

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CPppDriver::StartCapture(PCSTR pFilter)
{
	if( !m_fCapture ) {

		if( m_CaptFilter.Parse(pFilter) ) {

			m_CaptBuffer.Initialize(204);

			m_fCapture = true;

			return TRUE;
			}

		m_CaptBuffer.Empty();
		}

	return FALSE;
	}

void CPppDriver::StopCapture(void)
{
	m_fCapture = false;
	}

void CPppDriver::KillCapture(void)
{
	m_fCapture = false;

	m_CaptBuffer.Empty();
	}

// IClientProcess

BOOL CPppDriver::TaskInit(UINT uTask)
{
	SetThreadName("Ppp");

	return TRUE;
	}

INT CPppDriver::TaskExec(UINT uTask)
{
	m_pPpp->Open();

	m_pPpp->Service();

	m_pDone->Set();

	return 0;
	}

void CPppDriver::TaskStop(UINT uTask)
{
	}

void CPppDriver::TaskTerm(UINT uTask)
{
	}

// End of File
