
#include "Intern.hpp"

#include "UsbHostDongleDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "UsbDeviceReq.hpp"

#include "UsbIdentifiers.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Host Dongle Driver
//

// Instantiator

IUsbHostDongle * Create_DongleDriver(void)
{
	IUsbHostDongle *p = (IUsbHostDongle *) New CUsbHostDongleDriver;

	return p;
	}

// Constructor

CUsbHostDongleDriver::CUsbHostDongleDriver(void)
{
	m_pName  = "Host Dongle Driver";

	m_Debug  = debugWarn | debugErr;
	
	Trace(debugInfo, "Driver Created");
	}

// Destructor

CUsbHostDongleDriver::~CUsbHostDongleDriver(void)
{
	}

// IUnknown

HRESULT CUsbHostDongleDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IUsbHostDongle);

	return CUsbHostFuncBulkDriver::QueryInterface(riid, ppObject);
	}

ULONG CUsbHostDongleDriver::AddRef(void)
{
	return CUsbHostFuncBulkDriver::AddRef();
	}

ULONG CUsbHostDongleDriver::Release(void)
{
	return CUsbHostFuncBulkDriver::Release();
	}

// IHostFuncDriver

void CUsbHostDongleDriver::Bind(IUsbDevice *pDevice, UINT iInterface)
{
	CUsbHostFuncBulkDriver::Bind(pDevice, iInterface);
	}

void CUsbHostDongleDriver::Bind(IUsbHostFuncEvents *pSink)
{
	CUsbHostFuncBulkDriver::Bind(pSink);

	m_pRecv->RequestCompletion(pSink);

	m_pSend->RequestCompletion(pSink);
	}

void CUsbHostDongleDriver::GetDevice(IUsbDevice *&pDev)
{
	CUsbHostFuncBulkDriver::GetDevice(pDev);
	}

BOOL CUsbHostDongleDriver::SetRemoveLock(BOOL fLock)
{
	return CUsbHostFuncBulkDriver::SetRemoveLock(fLock);
	}

UINT CUsbHostDongleDriver::GetVendor(void)
{
	return CUsbHostFuncBulkDriver::GetVendor();
	}

UINT CUsbHostDongleDriver::GetProduct(void)
{
	return CUsbHostFuncBulkDriver::GetProduct();
	}

UINT CUsbHostDongleDriver::GetClass(void)
{
	return devVendor;
	}

UINT CUsbHostDongleDriver::GetSubClass(void)
{
	return subclassDongle;
	}

UINT CUsbHostDongleDriver::GetProtocol(void)
{
	return devVendor;
	}

UINT CUsbHostDongleDriver::GetInterface(void)
{
	return CUsbHostFuncBulkDriver::GetInterface();
	}

BOOL CUsbHostDongleDriver::GetActive(void)
{
	return CUsbHostFuncBulkDriver::GetActive();
	}

BOOL CUsbHostDongleDriver::Open(CUsbDescList const &List)
{
	return CUsbHostFuncBulkDriver::Open(List);
	}

BOOL CUsbHostDongleDriver::Close(void)
{
	return CUsbHostFuncBulkDriver::Close();
	}

void CUsbHostDongleDriver::Poll(UINT uLapsed)
{
	CUsbHostFuncBulkDriver::Poll(uLapsed);
	}

// IUsbDongleInterface

BOOL CUsbHostDongleDriver::SetLed(UINT uState)
{
	Trace(debugCmds, "SetLed(State=%d)", uState);

	CUsbDeviceReq Req;
	
	Req.m_Type      = reqVendor;

	Req.m_Direction = dirHostToDev;

	Req.m_Recipient = recInterface;
		
	Req.m_bRequest  = cmdSetLed;

	Req.m_wValue    = uState;

	Req.HostToUsb();

	return m_pCtrl->CtrlTrans(Req);
	}

BOOL CUsbHostDongleDriver::Challenge(void)
{
	Trace(debugCmds, "Challenge");

	MakeNonce();

	if( SendBulk(m_bNonce, sizeof(m_bNonce), true) ) {
		
		if( WaitAsyncSend(2500) == NOTHING ) {

			KillAsyncSend();

			return false;
			}

		BYTE bResp[16];

		if( RecvBulk(bResp, sizeof(bResp), true) ) {

			UINT uCount = WaitAsyncRecv(2500);
			
			if( uCount == NOTHING ) {

				KillAsyncRecv();
				
				return false;
				}
			
			if( uCount == sizeof(bResp) ) {
				
				MakeText1();

				MakeHash(PBYTE(m_sText), sizeof(m_sText));

				MakeText2();

				MakeHash(PBYTE(m_sText), sizeof(m_sText));

				return memcmp(bResp, m_bHash, sizeof(m_bHash)) == 0;
				}
			}
		}

	return false;
	}

// MD5 Helpers

void CUsbHostDongleDriver::MakeHash(PCBYTE pData, UINT uSize)
{
	MakeHash(pData, uSize, m_bHash);
	}

void CUsbHostDongleDriver::MakeHash(PCBYTE pData, UINT uSize, PBYTE pHash)
{
	md5(PBYTE(pData), uSize, pHash);
	}

// Implementation

void CUsbHostDongleDriver::MakeNonce(void)
{
	DWORD Data[4];

	Data[0] = GetTickCount();

	Data[1] = ~GetTickCount();

	Data[2] = (GetTickCount() << 16) | (GetTickCount() >> 16);

	Data[3] = GetProduct();

	MakeHash(PCBYTE(&Data), sizeof(Data), m_bNonce);
	}

void CUsbHostDongleDriver::MakeText1(void)
{
	memset (m_sText,   0, sizeof(m_sText));

	memcpy (m_sText +  0, m_bNonce, 16);

	memcpy (m_sText + 16, "wwww.redlion.net", 16);

	sprintf(m_sText + 32, "0x%4.4X", GetVendor());

	sprintf(m_sText + 38, "0x%4.4X", GetProduct());

	memcpy (m_sText + 44, "Graphite 1993-2015", 18);
	}

void CUsbHostDongleDriver::MakeText2(void)
{
	memset (m_sText,   0, sizeof(m_sText));

	memcpy (m_sText +  0, m_bHash, 16);

	memcpy (m_sText + 16, "wwww.redlion.net", 16);

	sprintf(m_sText + 32, "0x%4.4X", GetVendor());

	sprintf(m_sText + 38, "0x%4.4X", GetProduct());

	memcpy (m_sText + 44, "Graphite 1993-2015", 18);
	}

// End of File
