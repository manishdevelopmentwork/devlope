
#include "intern.hpp"

#include "yamts.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Serial Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CYamTsDriverOptions, CUIItem);

// Constructor

CYamTsDriverOptions::CYamTsDriverOptions(void)
{
	}

// UI Managament

void CYamTsDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL CYamTsDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);
	
	return TRUE;
	}

// Meta Data Creation

void CYamTsDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CYamTsDeviceOptions, CUIItem);

// Constructor

CYamTsDeviceOptions::CYamTsDeviceOptions(void)
{
	m_Station = 1;
	}

// UI Managament

void CYamTsDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	
	}

// Download Support

BOOL CYamTsDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Station));

	
	return TRUE;
	}

// Meta Data Creation

void CYamTsDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Station);

	}

//////////////////////////////////////////////////////////////////////////
//
// Yamaha TS Series Serial Driver
//

// Instantiator

ICommsDriver *	Create_YamahaTsSeriesMasterDriver(void)
{
	return New CYamTsMasterDriver;
	}

// Constructor

CYamTsMasterDriver::CYamTsMasterDriver(void)
{
	m_wID		= 0x40B5;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "Yamaha";
	
	m_DriverName	= "TS Series";
	
	m_Version	= "1.00";
	
	m_ShortName	= "TS";

	AddSpaces();  

	C3_PASSED();
	}

// Binding Control

UINT CYamTsMasterDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CYamTsMasterDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 38400;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CYamTsMasterDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CYamTsDriverOptions);
	}

// Configuration

CLASS CYamTsMasterDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CYamTsDeviceOptions);
	}

BOOL CYamTsMasterDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	return Addr.a.m_Table != addrNamed && Addr.a.m_Table >= 18;
	}

// Implementation

void CYamTsMasterDriver::AddSpaces(void)
{
	AddSpace(New CSpace(addrNamed,	"START",	"Positioning Operation",		10, 1,   0, addrByteAsByte ));
	AddSpace(New CSpace(addrNamed,	"STOP",		"Operation Stop",			10, 2,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"ORG",		"Return-To-Origin",			10, 3,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"JOG+",		"JOG Movement (+ direction)",		10, 4,	 0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,  "JOG-",		"JOG Movement (- direction)",		10, 5,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"INCH+",	"Inching Movement (+ direction)",	10, 6,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"INCH-",	"Inching Movement (- direction)",	10, 7,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"SRVO",		"Servo Status Change",			10, 8,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"BRK",		"Brake Status Change",			10, 9,   0, addrBitAsBit ));
	AddSpace(New CSpace(addrNamed,	"RESET",	"Reset",				10, 10,  0, addrBitAsBit ));
	AddSpace(New CSpace(2,		"M",		"Operation Type",			10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(3,		"P",		"Position",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(4,		"S",		"Speed",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(5,		"AC",		"Acceleration",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(6,		"DC",		"Deceleration",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(7,		"Q",		"Push",					10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(8,		"ZL",		"Zone(-)",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(9,		"ZH",		"Zone(+)",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(10,		"N",		"Near Width",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(11,		"J",		"Jump",					10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(12,		"F",		"Flag",					10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(13,		"T",		"Timer",				10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(14,		"TEACH",	"Current Position Teaching",		10, 1, 255, addrBitAsBit ));
	AddSpace(New CSpace(15,		"COPY",		"Point Data Copying",			10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(16,		"DEL",		"Point Data Deleting",			10, 1, 255, addrLongAsLong ));
	AddSpace(New CSpace(17,		"K",		"Parameter Data",			10, 1, 138, addrLongAsLong ));
	AddSpace(New CSpace(18,		"D",		"Status Information",			10, 0,  20, addrLongAsLong ));
	AddSpace(New CSpace(19,		"IN",		"Input Information",			10, 1,   1, addrWordAsWord ));
	AddSpace(New CSpace(20,		"INB",		"Input Information Bits",		10, 0,  15, addrBitAsBit ));
	AddSpace(New CSpace(21,		"OUT",		"Output Information",			10, 1,   1, addrWordAsWord ));
	AddSpace(New CSpace(22,		"OUTB",		"Output Information Bits",		10, 0,  15, addrBitAsBit ));
	AddSpace(New CSpace(23,		"WIN",		"Input Word Information",		10, 0,   3, addrWordAsWord ));
	AddSpace(New CSpace(24,		"WOUT",		"Output word Information",		10, 0,   3, addrWordAsWord ));
	AddSpace(New CSpace(25,		"OPT",		"Option Information",			10, 0,   2, addrLongAsLong ));
	AddSpace(New CSpace(26,		"OPTB",		"Option Information Bits",		10, 0,  31, addrBitAsBit ));
	AddSpace(New CSpace(27,		"ALM",		"Alarm Information",			10, 1,  32, addrLongAsLong ));
	AddSpace(New CSpace(28,		"WARN",		"Warning Information",			10, 1,  32, addrLongAsLong ));
	AddSpace(New CSpace(1,		"ERROR",	"Latest Error Message",			10, 0,  12, addrLongAsLong ));
	}


// End of File
