
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyStar4_HPP
	
#define	INCLUDE_PrimRubyStar4_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyStar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 4-Pointed Star Primitive
//

class CPrimRubyStar4 : public CPrimRubyStar
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyStar4(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
