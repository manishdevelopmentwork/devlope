
#include "intern.hpp"

#include "euro635.hpp"

//#define ADEBUG FALSE

/*
struct FAR EURO635CmdDef {
	UINT	uID;
	Description
	BYTE	bOP;
	UINT	uInfo;
// uInfo depends on Type
// TRW - size of data in bits 0-3
// TRO - size of data in bits 0-3, position of data in read response in bits 4-11
// TWO - size of data in bits 0-3
// TWC - size of data in bits 0-3, position of cached data in read response in bits 4-11
// TWW - N/A
// TRC - Value to be sent for read operation.
// TWR - size of data in bits 0-3, position of data in read response in bits 4-11, cache[0] sent on read
	BYTE	bSize;
	BYTE	Type;	
	};
*/
	
EURO635CmdDef CODE_SEG CEuro635Driver::CL[] = {

	{ VAR,  /*Read/Write One Variable */			34,	0x04,	64,	TRW },
	{ FLG,  /*Read/Write One Flag */			34,	0x01,	64,	TRW },
	{ FGP,  /*Read one group of 64 flags */			34,	0x04,	64,	TRO },
	{ 4,  /*Disable 635/637 */				0,	0x00,	0,	TWO },
	{ 5,  /*Enable 635/637 */				1,	0x00,	0,	TWO },
	{ 6,  /*Reset 635/637 */				2,	0x00,	0,	TWO },
	{ 7,  /*Host Login */					3,	0x00,	0,	TWO },
	{ 8,  /*Host Logout */					4,	0x00,	0,	TWO },
	{ 9,  /*Transfer Data in EEPROM */			5,	0x00,	0,	TWO },

	{ 10, /*Firmware Version - Byte 1 */			6,	0x01,	12,	TRO },
	{ 11, /*Firmware Version - Byte 2 */			6,	0x11,	12,	TRO },
	{ 12, /*Firmware Version - Byte 3 */			6,	0x21,	12,	TRO },
	{ 13, /*Firmware Version - Byte 4 */			6,	0x31,	12,	TRO },
	{ 14, /*Firmware Version - Byte 5 */			6,	0x41,	12,	TRO },
	{ 15, /*Firmware Version - Byte 6 */			6,	0x51,	12,	TRO },
	{ 16, /*Firmware Version - Byte 7 */			6,	0x61,	12,	TRO },
	{ 17, /*Firmware Version - Byte 8 */			6,	0x71,	12,	TRO },
	{ 18, /*Firmware Version - Byte 9 */			6,	0x81,	12,	TRO },
	{ 19, /*Firmware Version - Byte 10 */			6,	0x91,	12,	TRO },

	{ 20, /*Firmware Version - Byte 11 */			6,	0xA1,	12,	TRO },
	{ 21, /*Firmware Version - Byte 12 */			6,	0xB1,	12,	TRO },
	{ 22, /*Diagnosis Information - Error Word */		7,	0x02,	20,	TRO },
	{ 23, /*Diagnosis Information - Status Word */		7,	0x22,	20,	TRO },
	{ 24, /*Diagnosis Information - Serial Status */	7,	0x42,	20,	TRO },
	{ 25, /*Diagnosis Information - Operating Mode */	7,	0x61,	20,	TRO },
	{ 26, /*Diagnosis Information - Input Definition */	7,	0x71,	20,	TRO },
	{ 27, /*Diagnosis Information - Actual Speed */		7,	0x82,	20,	TRO },
	{ 28, /*Diagnosis Information - Input State */		7,	0xA1,	20,	TRO },
	{ 29, /*Diagnosis Information - Output State */		7,	0xB1,	20,	TRO },

	{ 30, /*Diagnosis Information - Actual Position */	7,	0xC4,	20,	TRO },
	{ 31, /*Diagnosis Information - Reserved */		7,	0x104,	20,	TRO },
	{ 32, /*Set BIAS Process Pointer */			13,	0x02,	0,	TWO },
	{ 33, /*Network Axis Number */				14,	0x01,	1,	TRW },
	{ 34, /*Read EEPROM Pointer - Pointer */		22,	0x01,	2,	TRO },
	{ 35, /*Read EEPROM Pointer - State */			22,	0x11,	2,	TRO },
	{ 36, /*Positioning Command - Mode */			23,	0x01,	13,	TWC },
	{ 37, /*Positioning Command - Speed */			23,	0x12,	13,	TWC },
	{ 38, /*Positioning Command - Acceleration */		23,	0x32,	13,	TWC },
	{ 39, /*Positioning Command - Deceleration */		23,	0x52,	13,	TWC },

	{ 40, /*Positioning Command - Reached Window */		23,	0x72,	13,	TWC },
	{ 41, /*Positioning Command - At Increments */		23,	0x94,	13,	TWC },
	{ 42, /*Positioning Command - EXECUTE WRITE */		23,	0x00,	Z23,	TWW },
	{ 43, /*Read BIAS Diagnosis - BIAS Pointer */		33,	0x02,	32,	TRO },
	{ 44, /*Read BIAS Diagnosis - PLC Pointer */		33,	0x22,	32,	TRO },
	{ 45, /*Read BIAS Diagnosis - Block Number */		33,	0x42,	32,	TRO },
	{ 46, /*Read BIAS Diagnosis - BIAS Stack */		33,	0x62,	32,	TRO },
	{ 47, /*Read BIAS Diagnosis - Wait Time */		33,	0x82,	32,	TRO },
	{ 48, /*Read BIAS Diagnosis - BIAS Status */		33,	0xA2,	32,	TRO },
	{ 49, /*Read BIAS Diagnosis - PLC Status */		33,	0xC2,	32,	TRO },

	{ 50, /*Read BIAS Diagnosis - PLC Stack */		33,	0xE2,	32,	TRO },
	{ 51, /*Read BIAS Diagnosis - Position 1 */		33,	0x104,	32,	TRO },
	{ 52, /*Read BIAS Diagnosis - Position 2 */		33,	0x144,	32,	TRO },
	{ 53, /*Read BIAS Diagnosis - Position 3 */		33,	0x184,	32,	TRO },
	{ 54, /*Read BIAS Diagnosis - Reserve */		33,	0x1C4,	32,	TRO },
	{ 55, /*Start Position Set - Block Select */		36,	0x01,	0,	TWO },
	{ 56, /*Input/Output Diagnosis - ExBus2 */		40,	0x02,	16,	TRO },
	{ 57, /*Input/Output Diagnosis - Ext_E-100 */		40,	0x22,	16,	TRO },
	{ 58, /*Input/Output Diagnosis - Ext_A-100 */		40,	0x42,	16,	TRO },
	{ 59, /*Input/Output Diagnosis - Ext_E-200 */		40,	0x62,	16,	TRO },

	{ 60, /*Input/Output Diagnosis - Ext_A-200 */		40,	0x82,	16,	TRO },
	{ 61, /*Input/Output Diagnosis - Reserve */		40,	0xA2,	16,	TRO },
	{ 62, /*Input/Output Diagnosis - Reserve */		40,	0xC2,	16,	TRO },
	{ 63, /*Input/Output Diagnosis - Reserve */		40,	0xE2,	16,	TRO },
	{ 64, /*Serial Speed Setpoint */			47,	0x02,	0,	TWO },
	{ 65, /*Rated Current of Motor */			62,	0x02,	2,	TRW },
	{ 66, /*Configuration - Axis */				65,	0x01,	33,	TWC },
	{ 67, /*Configuration - Configuration */		65,	0x12,	33,	TWC },
	{ 68, /*Configuration - Operating Mode */		65,	0x31,	33,	TWC },
	{ 69, /*Configuration - Input Definition */		65,	0x41,	33,	TWC },

	{ 70, /*Configuration - Rated Current */		65,	0x52,	33,	TWC },
	{ 71, /*Configuration - Pole Pair Number */		65,	0x72,	33,	TWC },
	{ 72, /*Configuration - EMC at Volt */			65,	0x92,	33,	TWC },
	{ 73, /*Configuration - Inductivity */			65,	0xB2,	33,	TWC },
	{ 74, /*Configuration - Resistance */			65,	0xD2,	33,	TWC },
	{ 75, /*Configuration - Monitoring Time */		65,	0xF2,	33,	TWC },
	{ 76, /*Configuration - T1 */				65,	0x112,	33,	TWC },
	{ 77, /*Configuration - T2 */				65,	0x132,	33,	TWC },
	{ 78, /*Configuration - PTC */				65,	0x152,	33,	TWC },
	{ 79, /*Configuration - Deceleration */			65,	0x172,	33,	TWC },

	{ 80, /*Configuration - Ucc Low */			65,	0x192,	33,	TWC },
	{ 81, /*Configuration - Ucc Ballast */			65,	0x1B2,	33,	TWC },
	{ 82, /*Configuration - Ballast Resistance */		65,	0x1D2,	33,	TWC },
	{ 83, /*Configuration - Ballast Power */		65,	0x1F2,	33,	TWC },
	{ 84, /*Configuration - EXECUTE WRITE */		65,	0x00,	Z65,	TWW },
	{ 85, /*Configuration - Read Data from Cache */		65,	0x00,	0,	TFC },
	{ 86, /*Configuration - Read Data into Cache */		65,	0x10,	0,	TTC },
	{ 87, /*Speed Controller - List Place P */		66,	0x02,	22,	TWC },
	{ 88, /*Speed Controller - List Place I */		66,	0x22,	22,	TWC },
	{ 89, /*Speed Controller - Max Current */		66,	0x42,	22,	TWC },

	{ 90, /*Speed Controller - Setpoint 0 */		66,	0x62,	22,	TWC },
	{ 91, /*Speed Controller - Integrator */		66,	0x82,	22,	TWC },
	{ 92, /*Speed Controller - Speed Norming */		66,	0xA2,	22,	TWC },
	{ 93, /*Speed Controller - Current Norming */		66,	0xC2,	22,	TWC },
	{ 94, /*Speed Controller - Norming 1 */			66,	0xE2,	22,	TWC },
	{ 95, /*Speed Controller - Norming 2 */			66,	0x102,	22,	TWC },
	{ 96, /*Speed Controller - Norming Limit */		66,	0x122,	22,	TWC },
	{ 97, /*Speed Controller - Offset Correct */		66,	0x142,	22,	TWC },
	{ 98, /*Speed Controller - EXECUTE WRITE */		66,	0x00,	Z66,	TWW },
	{ 99, /*Speed Controller - Read Data from Cache */	66,	0x20,	0,	TFC },

	{ 100, /*Speed Controller - Read Data into Cache */	66,	0x30,	0,	TTC },
	{ 101, /*Current Controller - List Place P */		67,	0x02,	18,	TWC },
	{ 102, /*Current Controller - List Place I */		67,	0x22,	18,	TWC },
	{ 103, /*Current Controller - Reserved */		67,	0x42,	18,	TWC },
	{ 104, /*Current Controller - Reserved */		67,	0x62,	18,	TWC },
	{ 105, /*Current Controller - Reserved */		67,	0x82,	18,	TWX },
	{ 106, /*Current Controller - Reserved */		67,	0xA2,	18,	TWX },
	{ 107, /*Current Controller - Resolver */		67,	0xC2,	18,	TWC },
	{ 108, /*Current Controller - Ucc OV */			67,	0xE2,	18,	TWC },
	{ 109, /*Current Controller - Reserved */		67,	0x102,	18,	TWC },

	{ 110, /*Current Controller - EXECUTE WRITE */		67,	0x00,	Z67,	TWW },
	{ 111, /*Current Controller - Read Data from Cache */	67,	0x40,	0,	TFC },
	{ 112, /*Current Controller - Read Data into Cache */	67,	0x50,	0,	TTC },
	{ 113, /*Position Controller - Speed */			68,	0x02,	14,	TWC },
	{ 114, /*Position Controller - Acceleration */		68,	0x22,	14,	TWC },
	{ 115, /*Position Controller - Deceleration */		68,	0x42,	14,	TWC },
	{ 116, /*Position Controller - Position Reached */	68,	0x62,	14,	TWC },
	{ 117, /*Position Controller - P */			68,	0x82,	14,	TWC },
	{ 118, /*Position Controller - I */			68,	0xA2,	14,	TWC },
	{ 119, /*Position Controller - Reserved */		68,	0xC2,	14,	TWC },

	{ 120, /*Position Controller - EXECUTE WRITE */		68,	0x00,	Z68,	TWW },
	{ 121, /*Current Controller - Read Data from Cache */	68,	0x60,	0,	TFC },
	{ 122, /*Current Controller - Read Data into Cache */	68,	0x70,	0,	TTC },
	{ 123, /*Position Set - Position Set Number */		69,	0x01,	1,	TRC },
	{ 124, /*Position Set - Command Mode */			69,	0x01,	13,	TWR },
	{ 125, /*Position Set - Speed */			69,	0x12,	13,	TWR },
	{ 126, /*Position Set - Acceleration */			69,	0x32,	13,	TWR },
	{ 127, /*Position Set - Deceleration */			69,	0x52,	13,	TWR },
	{ 128, /*Position Set - Position Reached */		69,	0x72,	13,	TWR },
	{ 129, /*Position Set - Nominal Position */		69,	0x94,	13,	TWR },

	{ 130, /*Position Set - EXECUTE WRITE */		69,	0x00,	Z69,	TWW },
	{ 131, /*Position Set - Read Data from Cache */		69,	0x80,	0,	TFC },
	{ 132, /*Position Set - Read Data into Cache */		69,	0x90,	0,	TTC },
	{ 133, /*Cam Profile - Parameter Set Number */		72,	0x11,	1,	TRC },
	{ 134, /*Cam Profile - Easyrider Reserved */		72,	0x01,	64,	TWR },
	{ 135, /*Cam Profile - Corrections */			72,	0x11,	64,	TWR },
	{ 136, /*Cam Profile - Profile Points */		72,	0x22,	64,	TWR },
	{ 137, /*Cam Profile - Address */			72,	0x42,	64,	TWR },
	{ 138, /*Cam Profile - Reserved */			72,	0x62,	64,	TWR },
	{ 139, /*Cam Profile - Correct 1 */			72,	0x82,	64,	TWR },

	{ 140, /*Cam Profile - Correct 2 */			72,	0xA2,	64,	TWR },
	{ 141, /*Cam Profile - Correct 3 */			72,	0xC2,	64,	TWR },
	{ 142, /*Cam Profile - Correct 4 */			72,	0xE2,	64,	TWR },
	{ 143, /*Cam Profile - Correct 5 */			72,	0x102,	64,	TWR },
	{ 144, /*Cam Profile - Correct 6 */			72,	0x122,	64,	TWR },
	{ 145, /*Cam Profile - Correct 7 */			72,	0x142,	64,	TWR },
	{ 146, /*Cam Profile - Correct 8 */			72,	0x162,	64,	TWR },
	{ 147, /*Cam Profile - Correct 9 */			72,	0x182,	64,	TWR },
	{ 148, /*Cam Profile - Correct 10 */			72,	0x1A2,	64,	TWR },
	{ 149, /*Cam Profile - Master Stroke */			72,	0x1C4,	64,	TWR },

	{ 150, /*Cam Profile - Slave Stroke */			72,	0x204,	64,	TWR },
	{ 151, /*Cam Profile - Reserved Bytes 1-4 */		72,	0x244,	64,	TWR },
	{ 152, /*Cam Profile - Reserved Bytes 5-8 */		72,	0x284,	64,	TWR },
	{ 153, /*Cam Profile - Reserved Bytes 9-12 */		72,	0x2C4,	64,	TWR },
	{ 154, /*Cam Profile - Reserved Bytes 13-16 */		72,	0x304,	64,	TWR },
	{ 155, /*Cam Profile - Synchronmode */			72,	0x341,	64,	TWR },
	{ 156, /*Cam Profile - Reserved Bytes 1-4 */		72,	0x354,	64,	TWR },
	{ 157, /*Cam Profile - Reserved Bytes 5-8 */		72,	0x394,	64,	TWR },
	{ 158, /*Cam Profile - Reserved Bytes 9-11 */		72,	0x3D3,	64,	TWR },
	{ 159, /*Cam Profile - EXECUTE WRITE */			72,	0x00,	Z72,	TWW },

	{ 160, /*Cam Profile - Read Data from Cache */		72,	0xA0,	0,	TFC },
	{ 161, /*Cam Profile - Read Data into Cache */		72,	0xB0,	0,	TTC },
	{ 162, /*Profile Point Block - Set Number */		73,	0x21,	1,	TRC },
	{ 163, /*Profile Point 1 - Bytes 1-4 */			73,	0x04,	64,	TWR },
	{ 164, /*Profile Point 1 - Bytes 5-8 */			73,	0x44,	64,	TWR },
	{ 165, /*Profile Point 2 - Bytes 1-4 */			73,	0x84,	64,	TWR },
	{ 166, /*Profile Point 2 - Bytes 5-8 */			73,	0xC4,	64,	TWR },
	{ 167, /*Profile Point 3 - Bytes 1-4 */			73,	0x104,	64,	TWR },
	{ 168, /*Profile Point 3 - Bytes 5-8 */			73,	0x144,	64,	TWR },
	{ 169, /*Profile Point 4 - Bytes 1-4 */			73,	0x184,	64,	TWR },

	{ 170, /*Profile Point 4 - Bytes 5-8 */			73,	0x1C4,	64,	TWR },
	{ 171, /*Profile Point 5 - Bytes 1-4 */			73,	0x204,	64,	TWR },
	{ 172, /*Profile Point 5 - Bytes 5-8 */			73,	0x244,	64,	TWR },
	{ 173, /*Profile Point 6 - Bytes 1-4 */			73,	0x284,	64,	TWR },
	{ 174, /*Profile Point 6 - Bytes 5-8 */			73,	0x2C4,	64,	TWR },
	{ 175, /*Profile Point 7 - Bytes 1-4 */			73,	0x304,	64,	TWR },
	{ 176, /*Profile Point 7 - Bytes 5-8 */			73,	0x344,	64,	TWR },
	{ 177, /*Profile Point 8 - Bytes 1-4 */			73,	0x384,	64,	TWR },
	{ 178, /*Profile Point 8 - Bytes 5-8 */			73,	0x3C4,	64,	TWR },
	{ 179, /*Profile Point Block - EXECUTE WRITE */		73,	0x00,	Z73,	TWW },

	{ 180, /*Profile Point - Read Data from Cache */	73,	0xC0,	0,	TFC },
	{ 181, /*Profile Point - Read Data into Cache */	73,	0xD0,	0,	TTC },
	{ 182, /*I/O Definitions - I - X10.2 */			74,	0x01,	11,	TWC },
	{ 183, /*I/O Definitions - I - X10.4 */			74,	0x11,	11,	TWC },
	{ 184, /*I/O Definitions - I - X10.11 */		74,	0x21,	11,	TWC },
	{ 185, /*I/O Definitions - I - X10.14 */		74,	0x31,	11,	TWC },
	{ 186, /*I/O Definitions - I - X10.15 */		74,	0x41,	11,	TWC },
	{ 187, /*I/O Definitions - I - X10.24 */		74,	0x51,	11,	TWC },
	{ 188, /*I/O Definitions - I - X10.25 */		74,	0x61,	11,	TWC },
	{ 189, /*I/O Definitions - O - X10.12 */		74,	0x71,	11,	TWC },

	{ 190, /*I/O Definitions - O - X10.13 */		74,	0x81,	11,	TWC },
	{ 191, /*I/O Definitions - O - X10.20 */		74,	0x91,	11,	TWC },
	{ 192, /*I/O Definitions - O - X10.23 */		74,	0xA1,	11,	TWC },
	{ 193, /*I/O Definitions - EXECUTE WRITE */		74,	0x00,	Z74,	TWW },
	{ 194, /*I/O Definitions - Read Data from Cache */	74,	0xE0,	0,	TFC },
	{ 195, /*I/O Definitions - Read Data into Cache */	74,	0xF0,	0,	TTC },
	{ 196, /*BIAS Program - Set Number */			76,	0x32,	2,	TRC },
	{ 197, /*BIAS Program - command code */			76,	0x01,	8,	TWR },
	{ 198, /*BIAS Program - Byte 1 */			76,	0x11,	8,	TWR },
	{ 199, /*BIAS Program - Byte 2 */			76,	0x21,	8,	TWR },

	{ 200, /*BIAS Program - Byte 3 */			76,	0x31,	8,	TWR },
	{ 201, /*BIAS Program - Byte 4 */			76,	0x41,	8,	TWR },
	{ 202, /*BIAS Program - Byte 5 */			76,	0x51,	8,	TWR },
	{ 203, /*BIAS Program - Byte 6 */			76,	0x61,	8,	TWR },
	{ 204, /*BIAS Program - Byte 7 */			76,	0x71,	8,	TWR },
	{ 205, /*BIAS Program - EXECUTE WRITE */		76,	0x00,	Z76,	TWW },
	{ 206, /*BIAS Program - Read Data from Cache */		76,	0x100,	0,	TFC },
	{ 207, /*BIAS Program - Read Data into Cache */		76,	0x110,	0,	TTC },
	{ 208, /*Extended Control - V-Gain */			78,	0x02,	14,	TWC },
	{ 209, /*Extended Control - X40-Mode */			78,	0x21,	14,	TWC },

	{ 210, /*Extended Control - X40-Resolution */		78,	0x31,	14,	TWC },
	{ 211, /*Extended Control - Position Time */		78,	0x42,	14,	TWC },
	{ 212, /*Extended Control - Reserved */			78,	0x62,	14,	TWC },
	{ 213, /*Extended Control - Trail Window */		78,	0x82,	14,	TWC },
	{ 214, /*Extended Control - Trail Fault */		78,	0xA1,	14,	TWC },
	{ 215, /*Extended Control - N-Filter */			78,	0xB1,	14,	TWC },
	{ 216, /*Extended Control - Reserved */			78,	0xC2,	14,	TWC },
	{ 217, /*Extended Control - EXECUTE WRITE */		78,	0x00,	Z78,	TWW },
	{ 218, /*Extended Control - Read Data from Cache */	78,	0x120,	0,	TFC },
	{ 219, /*Extended Control - Read Data into Cache */	78,	0x130,	0,	TTC },
	{ 220, /*Command could not be executed */		99,	0,	0,	TXX },
// Added 17 Nov 2004 per SSD Request
	{ 221, /*Diagnostics - Error Word */			16,	0x02,	38,	TRO },
	{ 222, /*Diagnostics - Status Word 1*/			16,	0x22,	38,	TRO },
	{ 223, /*Diagnostics - Status Word 2 */			16,	0x42,	38,	TRO },
	{ 224, /*Diagnostics - Operating Mode */		16,	0x62,	38,	TRO },
	{ 225, /*Diagnostics - Speed */				16,	0x82,	38,	TRO },
	{ 226, /*Diagnostics - I2 Motor */			16,	0xA2,	38,	TRO },
	{ 227, /*Diagnostics - Current */			16,	0xC2,	38,	TRO },
	{ 228, /*Diagnostics - UCC */				16,	0xE2,	38,	TRO },
	{ 229, /*Diagnostics - I2 Drive */			16,	0x102,	38,	TRO },
	{ 230, /*Diagnostics - Brake Resistor Power */		16,	0x122,	38,	TRO },
	{ 231, /*Diagnostics - Reserve */			16,	0x142,	38,	TRO },
	{ 232, /*Diagnostics - Input/Output State */		16,	0x162,	38,	TRO },
	{ 233, /*Diagnostics - Absolute Resolver */		16,	0x182,	38,	TRO },
	{ 234, /*Diagnostics - Analog Set Point */		16,	0x1A2,	38,	TRO },
	{ 235, /*Diagnostics - Motor Temperature */		16,	0x1C2,	38,	TRO },
	{ 236, /*Diagnostics - Position */			16,	0x1E4,	38,	TRO },
	{ 237, /*Diagnostics - Calculation Time */		16,	0x224,	38,	TRO },
// Added 30 Nov 2004 per SSD Request
	{ 238, /*Diagnostics - Drive Type */			17,	0x02,	30,	TRO },
	{ 239, /*Diagnostics - Serial Number */			17,	0x22,	30,	TRO },
	{ 240, /*Diagnostics - Production Data */		17,	0x42,	30,	TRO },
	{ 241, /*Diagnostics - Repair Number */			17,	0x62,	30,	TRO },
	{ 242, /*Diagnostics - I2T Motor Scaling Factor */	17,	0x82,	30,	TRO },
	{ 243, /*Diagnostics - Drive Maximum Current */		17,	0xA2,	30,	TRO },
	{ 244, /*Diagnostics - Current Scaling Factor */	17,	0xC2,	30,	TRO },
	{ 245, /*Diagnostics - I2T Drive Scaling Factor */	17,	0xE2,	30,	TRO },
	{ 246, /*Diagnostics - Brake Circuit Rated Power */	17,	0x102,	30,	TRO },
	{ 247, /*Diagnostics - Ucc Scaling Factor */		17,	0x122,	30,	TRO },
	{ 248, /*Diagnostics - Temperature Scaling Factor */	17,	0x142,	30,	TRO },
	{ 249, /*Diagnostics - Configuration */			17,	0x162,	30,	TRO },
	{ 250, /*Diagnostics - Maximum Speed */			17,	0x182,	30,	TRO },
	{ 251, /*Diagnostics - Speed Scaling Factor */		17,	0x1A4,	30,	TRO },
	};

//////////////////////////////////////////////////////////////////////////
//
// Euro635 Driver
//

// Instantiator

INSTANTIATE(CEuro635Driver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CEuro635Driver::CEuro635Driver(void)
{
	m_Ident = DRIVER_ID;

	memset( m_ReadControl, 0, sizeof(m_ReadControl) );

	memset( m_TRCCache, 0, sizeof(m_TRCCache) );

	m_Error = 0;
	}

// Destructor

CEuro635Driver::~CEuro635Driver(void)
{
	}

// Configuration

void MCALL CEuro635Driver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CEuro635Driver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CEuro635Driver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CEuro635Driver::Open(void)
{
	m_pCL = (EURO635CmdDef FAR * )CL;
	}

// Device

CCODE MCALL CEuro635Driver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop = GetByte(pData);

			if( !m_pCtx->m_bDrop ) m_pCtx->m_bDrop = 1;

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CEuro635Driver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CEuro635Driver::Ping(void)
{
	DWORD    Data[1];

	CAddress Addr;

	Addr.a.m_Table  = 0xF0;
	Addr.a.m_Offset = 10;
	Addr.a.m_Type   = addrLongAsLong;
	Addr.a.m_Extra  = 0;

	return Read(Addr, Data, 1);
	}

CCODE MCALL CEuro635Driver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	switch ( Addr.a.m_Table ) {

		case VAR:
			MakeMin( uCount, UINT(16-(Addr.a.m_Offset%16)) );
			m_pItem = GetpItem( Addr.a.m_Table );
			break;

		case FLG:
			MakeMin( uCount, UINT(64-(Addr.a.m_Offset%64)) );
			m_pItem = GetpItem( Addr.a.m_Table );
			break;

		case FGP:
			MakeMin( uCount, UINT(8-(Addr.a.m_Offset%8)) );
			m_pItem = GetpItem( Addr.a.m_Table );
			break;

		default:
			MakeMin( uCount, 1 );
			m_pItem = GetpItem( LOBYTE( Addr.a.m_Offset ) );
			break;
		}

	if( m_pItem == NULL ) return CCODE_ERROR;

	if( ReadNoTransmit( pData ) )

		return 1;

//	AfxTrace0("\r\n");

	if( Addr.a.m_Table != FGP ) {

		ReadOpcode( Addr );

		if( Transact(GetRxSize()) ) {

			if( Addr.a.m_Table != addrNamed ) {

				if( GetRegResponse( Addr, uCount, pData ) ) return uCount;
				}

			else {
				if( GetNamedResponse( pData ) ) return 1;
				}
			}
		}

	else {
		if( DoFGPOpcode( Addr.a.m_Offset, uCount, pData ) ) return uCount;
		}

	if( m_bRx[0] == 0x18 ) {

		m_Error = m_pItem->bOP;

		return CCODE_NO_RETRY;
		}

	return CCODE_ERROR;
	}

CCODE MCALL CEuro635Driver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	m_pItem = GetpItem( LOBYTE(Addr.a.m_Table == addrNamed ? Addr.a.m_Offset : Addr.a.m_Table ) );

	if( m_pItem == NULL ) return CCODE_ERROR;

	if( m_pItem->Type == TRO || m_pItem->Type == TWX ) {

		return 1;
		}

	SetCachePointer();

	if( WriteNoTransmit(pData) ) return 1;

//	if( ADEBUG ) AfxTrace0("\r\n*******\r\n");

	WriteOpcode(Addr, pData);

	if( Transact(0) ) return 1;

	if( m_bRx[0] == 0x18 ) {

		m_Error = m_pItem->bOP;

		return CCODE_NO_RETRY;
		}

	return CCODE_ERROR;
	}

// Opcode Handlers

void CEuro635Driver::ReadOpcode(AREF Addr)
{

	StartFrame( FALSE );

	switch ( Addr.a.m_Table ) {

		case addrNamed:
			ReadNamedOpcode();
			break;

		case VAR:
			AddByte( 0 );
			AddByte( Addr.a.m_Offset / 16 );
			break;

		default:
			AddByte( 1 );
			AddByte( Addr.a.m_Offset / 64 );
			break;
		}

	return;
	}

void CEuro635Driver::ReadNamedOpcode(void)
{
	switch ( m_pItem->Type ) {

		case TRO:
			if( m_pItem->bOP == 40 )
				AddByte(0);
			break;

		case TWR: // number to be sent with read request

			AddByte( OPRD );

			AddTRCData();

			break;

		case TWC:
		case TWX:
		default:

			AddByte( OPRD );

			if( m_pItem->bOP == 14 ) AddByte('1'); // dummy number required

			break;
		}

	return;
	}

BOOL CEuro635Driver::DoFGPOpcode(UINT uAddr, UINT uCount, PDWORD pData)
{
	DWORD pRsp[2];

	for( UINT i = 0; i < uCount; i++ ) {

		StartFrame(FALSE);

		AddByte( 1 );

		AddByte( (uAddr+i)/2 );

		if( Transact(64) ) {

			GetFGPResponse( pRsp );

			if( (uAddr + i) % 2 ) {

				pData[i] = pRsp[1];
				}

			else {
				if(uCount-i > 1) {

					pData[i] = pRsp[0];

					pData[i+1] = pRsp[1];

					i++;
					}

				else {
					pData[i] = pRsp[0];
					}
				}
			}
		else return FALSE;
		}

	return TRUE;
	}

void CEuro635Driver::WriteOpcode(AREF Addr, PDWORD pData)
{
	BYTE bReg;

	switch ( Addr.a.m_Table ) {

		case addrNamed:
			WriteNamedOpcode(pData);
			return;

		case VAR:
			bReg = 0;
			break;

		default:
			bReg = 1;
			break;
		}

	StartFrame( TRUE );

	AddByte( bReg ); // Variable/Flag Select

	AddByte( LOBYTE( Addr.a.m_Offset ) );  // Item Address

	AddData( bReg ? ((*pData) ? 0xFFFFFFFF : 0L) : *pData, 4 );
	}

void CEuro635Driver::WriteNamedOpcode(PDWORD pData)
{
	StartFrame( FALSE );

	switch( m_pItem->Type ) {

		case TWO:
			switch( m_pItem->bOP ) {

				case 13:
				case 47:
					AddData( *pData, 2 );
					break;
				case 36:
					AddByte( LOBYTE(LOWORD(*pData)) );
					break;
				}
			return;

		case TRW:
			AddByte( OPWR );
			AddData( *pData, GetItemSize( m_pItem ) );
			return;

		case TWW:
			if( m_pItem->bOP != 23 ) AddByte( OPWR );

			if( m_pItem->bOP == 67 )
				Add67Data();

			else {

				AddTRCData();

				AddCachedData();
				}
			return;
		}
	}

// Header Building

void CEuro635Driver::StartFrame(BOOL fIsRegWrite)
{
	m_uPtr = 0;

	m_bCheck = 0;

	AddByte( ESC );

	AddByte( m_pCtx->m_bDrop );

	AddCommand( fIsRegWrite );
	}

void CEuro635Driver::EndFrame(void)
{
	m_bTx[m_uPtr++] = m_bCheck;
	}

void CEuro635Driver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;

	m_bCheck ^= bData;
	}

void CEuro635Driver::AddData(DWORD dData, UINT uSize)
{
	AddByte( LOBYTE(LOWORD(dData)) );

	if( uSize > 1 ) AddByte( HIBYTE(LOWORD(dData)) );

	if( uSize > 2 ) AddByte( LOBYTE(HIWORD(dData)) );

	if( uSize > 3 ) AddByte( HIBYTE(HIWORD(dData)) );
	}

void CEuro635Driver::AddCachedData(void)
{
	UINT End = m_pItem->bSize;

	if( m_pItem->bOP == 72 )
		End -= 1; // only 3 bytes of last dword are used

	for(UINT i = 0; i < End; i++) {

		AddByte( m_pCache[i] );
		}
	}

void CEuro635Driver::Add67Data(void)
{
	for( UINT i = 0; i < 8; i++ ) {

		AddByte( m_pCache[i] );
		}

	for( i = 12; i < 18; i++ ) {

		AddByte( m_pCache[i] );
		}
	}

void CEuro635Driver::AddCommand( BOOL fIsRegWrite )
{
	AddByte( !fIsRegWrite ? LOBYTE(m_pItem->bOP) : 39 );

	return;
	}

void CEuro635Driver::AddTRCData(void)
{
	EURO635CmdDef * p;

	p = GetpFirstItem();

	if( p->Type != TRC ) return;

	UINT uRpos = GetReadPosition(p);

	if( GetItemSize(p) == 2 )

		AddByte( HIBYTE(m_TRCCache[uRpos]) );

	AddByte( LOBYTE(m_TRCCache[uRpos]) );
	}

// Transport Layer

BOOL CEuro635Driver::Transact(BYTE bSize)
{
	Send();

//	AfxTrace0("\r\n");

	return GetReply(bSize);
	}

void CEuro635Driver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

//	if( ADEBUG ) {

//		for( UINT i = 0; i < m_uPtr; i++ ) {
			
//			AfxTrace1("[%2.2x]", m_bTx[i] );
//			}
//		}

	Put();
	}

BOOL CEuro635Driver::GetReply(BYTE bSize)
{
	UINT uCount = 0;
	UINT uState = 0;
	WORD wData;
	BYTE bCheck = 0;

	SetTimer(1000);

//	AfxTrace0("\r\n");

	while( GetTimer() ) {

		wData = Get();

		if( wData == LOWORD(NOTHING) ) {			
			Sleep(20);
			continue;
			}

		BYTE bData = LOBYTE(wData);

//		AfxTrace1("<%2.2x>", LOBYTE(bData) ); 

		if( uCount >= sizeof(m_bRx) ) {

			return FALSE;
			}

		SetTimer( 100 ); // large data packets

		switch ( uState ) {

			case 0:
				if( bData == ACK ) {

					if( bSize == 0 ) {

						return TRUE;
						}

					else {
						uCount = 0;
						uState = 1;
						bCheck = ACK;
						}
					}

				else {
					m_bRx[0] = bData;
					return FALSE;
					}
				break;

			case 1:
				m_bRx[uCount++] = bData;

				bCheck ^= bData;

				if( uCount == UINT(bSize + 1) ) {

					return ( bCheck == 0 ? TRUE : FALSE );
					}
			}
		}

	return FALSE;
	}

// Response Handling
BOOL CEuro635Driver::GetRegResponse(AREF Addr, UINT uCount, PDWORD pData)
{
	UINT i;
	
	if( Addr.a.m_Table == VAR ) {

		for( i = 0; i < uCount; i++ ) {

			DWORD x = PU4(m_bRx + (Addr.a.m_Offset%16) * 4)[i];

			pData[i] = IntelToHost(x);
			}
		}

	else {

		for( i = 0; i < uCount; i++ ) {

			pData[i] = DWORD(PBYTE(DWORD(m_bRx[(Addr.a.m_Offset%64)+i]) ? 1 : 0 ));
			}
		}

	return TRUE;
	}

void CEuro635Driver::GetFGPResponse(PDWORD pRsp)
{
	UINT i;
	UINT uPos;
	UINT b;
	DWORD dData;

	for( i = 0, uPos = 0; i < 2; i++, uPos += 32 ) {

		dData = 0;

		for( b = uPos; b < uPos + 32; b++ ) {

			dData >>= 1;

			dData |= m_bRx[b] ? 0x80000000 : 0L;
			}

		pRsp[i] = dData;					
		}

	return;
	}

BOOL CEuro635Driver::GetNamedResponse( PDWORD pData )
{
	pData[0] = PickData(FALSE);

	return TRUE;
	}

// Port Access

void CEuro635Driver::Put(void)
{
	m_pData->Write( PCBYTE(m_bTx), m_uPtr, FOREVER);
	}

WORD CEuro635Driver::Get(void)
{
	WORD wData;

	wData = m_pData->Read(TIMEOUT);

	return wData;
	}

// Helpers
EURO635CmdDef * CEuro635Driver::GetpItem(UINT uID)
{
	EURO635CmdDef * p;

	p = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == p->uID ) return p;

		p++;
		}

	return NULL;
	}

EURO635CmdDef * CEuro635Driver::GetpFirstItem(void)
{
	EURO635CmdDef * p;

	p = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( m_pItem->bOP == p->bOP ) return p;

		p++;
		}

	return NULL;
	}

EURO635CmdDef * CEuro635Driver::GetpReadControl(void)
{
	EURO635CmdDef * p;

	p = m_pCL;

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( m_pItem->bOP == p->bOP && p->Type == TFC ) {

			return p;
			}

		p++;
		}

	return NULL;
	}

BOOL CEuro635Driver::ReadNoTransmit(PDWORD pData)
{
	UINT uRpos = GetReadPosition( m_pItem );
	UINT uCount = GetItemSize( m_pItem );
	DWORD dData = 0;
	UINT i;

	switch( m_pItem->Type ) {

		case TWO: // write only operations
		case TWW:
			*pData = 0xFFFFFFFF;
			return TRUE;

		case TRC: // current set number to be sent with write command

			if( uCount == 1 ) {

				*pData = DWORD( LOBYTE(m_TRCCache[uRpos]) );
				}

			else {
				*pData = DWORD( m_TRCCache[uRpos] );
				}

			return TRUE;

		case TWC:
		case TWR:

			if( CheckCacheRead( pData ) ) return TRUE;

			if( m_pItem->bOP != 23 ) return FALSE;

			SetCachePointer();

			for( i = 0; i < uCount; i++ ) {

				dData <<= 8;

				dData += m_pCache[uRpos+i];
				}

			*pData = dData;

			return TRUE;

		case TFC:
			*pData = m_ReadControl[GetReadPosition( m_pItem )] ? 1L : 0L;

			return TRUE;

		case TTC:
			*pData = 0L;

			return TRUE;

		case TXX:
			*pData = m_Error;

			return TRUE;
		}

	return FALSE;
	}

BOOL CEuro635Driver::WriteNoTransmit(PDWORD pData)
{
	UINT uRpos = GetReadPosition( m_pItem );
	UINT uCount = GetItemSize( m_pItem );

	switch( m_pItem->Type ) {

		case TWC:
		case TWR:
			PutDataInCache( uRpos, uCount, pData[0] );

			return TRUE;

		case TRC:

			if( uCount == 2 ) {

				m_TRCCache[uRpos] = LOWORD(*pData);
				}

			else {

				m_TRCCache[uRpos] = LOWORD(*pData) & 0xFF;
				}

			return TRUE;

		case TFC:

			m_ReadControl[uRpos] = LOBYTE(pData[0]) ? 1 : 0;

			return TRUE;

		case TTC:

			ReadDataIntoCache();

			return TRUE;

		case TXX:

			m_Error = 0;

			return TRUE;
		}

	return FALSE;
	}

void CEuro635Driver::PutDataInCache( UINT uPos, UINT uCt, DWORD dData )
{
	m_pCache[uPos] = LOBYTE(LOWORD(dData));

	if( uCt > 1 ) m_pCache[uPos+1] = HIBYTE(LOWORD(dData));

	if( uCt > 2 ) m_pCache[uPos+2] = LOBYTE(HIWORD(dData));

	if( uCt > 3 ) m_pCache[uPos+3] = HIBYTE(HIWORD(dData));
	}

UINT CEuro635Driver::GetReadPosition(EURO635CmdDef * p)
{
	return (p->uInfo & 0x0FF0) >> 4;
	}

UINT CEuro635Driver::GetItemSize(EURO635CmdDef * p)
{
	return (p->uInfo & 0xF);
	}

BYTE CEuro635Driver::GetRxSize(void)
{
	return (m_pItem->bSize);
	}

void CEuro635Driver::SetCachePointer()
{
	switch( m_pItem->bOP ) {

		case  23:
			m_pCache = PBYTE(&s23);
			return;

		case  65:
			m_pCache = PBYTE(&s65);
			return;

		case  66:
			m_pCache = PBYTE(&s66);
			return;

		case  67:
			m_pCache = PBYTE(&s67);
			return;

		case  68:
			m_pCache = PBYTE(&s68);
			return;

		case  69:
			m_pCache = PBYTE(&s69);
			return;

		case  72:
			m_pCache = PBYTE(&s72);
			return;

		case  73:
			m_pCache = PBYTE(&s73);
			return;

		case  74:
			m_pCache = PBYTE(&s74);
			return;

		case  76:
			m_pCache = PBYTE(&s76);
			return;

		default:
			m_pCache = PBYTE(&s78);
			return;
		}
	}

DWORD CEuro635Driver::PickData(BOOL bFromCache)
{
	UINT uPos;
	UINT uSize;
	DWORD dData = 0;

	uPos = GetReadPosition(m_pItem);
	uSize = GetItemSize(m_pItem);

	for( UINT i = 0; i < GetItemSize(m_pItem) ; i++ ) {

		dData <<= 8;

		if( bFromCache )
			dData += m_pCache[uPos+i];

		else
			dData += m_bRx[uPos+i];
		}

	switch( uSize ) {

		case 1:
			break;

		case 2:
			dData = DWORD( Reverse( LOWORD(dData) ) );
			break;

		case 3:
			dData <<= 8;
		case 4:
			dData = Reverse( dData );
			break;
		}

	return dData;
	}

void CEuro635Driver::ReadDataIntoCache( void )
{
	EURO635CmdDef * p;

	BYTE bSz;

	p = GetpFirstItem();

	SetCachePointer();

	StartFrame(FALSE);

	AddByte(OPRD);

	AddTRCData();

	p++;

	bSz = p->bSize;

	if( Transact(bSz) ) {

		memcpy( m_pCache, m_bRx, bSz );
		}
	}

BOOL CEuro635Driver::CheckCacheRead( PDWORD pData )
{
	EURO635CmdDef * p;

	p = GetpReadControl();

	if( p ) {

		if( m_ReadControl[GetReadPosition(p)] ) {

			SetCachePointer();

			pData[0] = PickData(TRUE);

			return TRUE;
			}
		}

	return FALSE;
	}

DWORD CEuro635Driver::Reverse( DWORD dData )
{
	DWORD dwHi = Reverse(WORD(dData >> 16));

	DWORD dwLo = Reverse(WORD(dData)) << 16;

	return dwHi | dwLo;
	}

WORD CEuro635Driver::Reverse( WORD wData )
{
	WORD wHi = WORD(wData >> 8);

	WORD wLo = WORD(wData << 8);

	return WORD(wHi | wLo);
	}


// End of File
