
#include "Intern.hpp"

#include "EthernetIp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

#include "TcpIpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ethernet/IP Encapsulation
//

// Constructor

CEthernetIp::CEthernetIp(CTcpIpSock *pSock, UINT uTimeout)
{
	m_pSock    = pSock;

	m_uTimeout = uTimeout;

	m_fLayerUp = false;

	ClearData();
	}

// Destructor

CEthernetIp::~CEthernetIp(void)
{
	Close();

	delete m_pSock;
	}

// Attributes

bool CEthernetIp::IsLayerUp(void) const
{
	return m_fLayerUp;
	}

// Operations

bool CEthernetIp::Open(void)
{
	if( m_pSock->Open() ) {

		if( LayerUp() ) {

			return true;
			}

		m_pSock->Close();
		}

	return false;
	}

bool CEthernetIp::ExchangeUnconnect(CBuffer * &pBuff)
{
	if( IsLayerUp() ) {

		WORD	      Length    = pBuff->GetSize();

		CGeneralItem *pDataItem = BuffAddHead(pBuff, CGeneralItem);

		CGeneralItem *pAddrItem = BuffAddHead(pBuff, CGeneralItem);

		CDataPacket  *pDataPack = BuffAddHead(pBuff, CDataPacket);

		pDataPack->Interface = 0;
	
		pDataPack->Timeout   = 0;
	
		pDataPack->ItemCount = 2;

		pDataPack->ReOrder();

		pAddrItem->TypeId = segNullAddr;
	
		pAddrItem->Length = 0;

		pAddrItem->ReOrder();

		pDataItem->TypeId = segUnconnectData;
	
		pDataItem->Length = Length;

		pDataItem->ReOrder();

		if( Send(pBuff, cmdSendRrData) ) {

			if( Recv(pBuff, cmdSendRrData, m_uTimeout) ) {

				pDataPack = BuffStripHead(pBuff, CDataPacket);

				pDataPack->ReOrder();

				if( pDataPack->ItemCount == 2 ) {

					pAddrItem = BuffStripHead(pBuff, CGeneralItem);

					pAddrItem->ReOrder();

					if( pAddrItem->TypeId == segNullAddr ) {
					
						if( pAddrItem->Length == 0 ) {

							pDataItem = BuffStripHead(pBuff, CGeneralItem);

							pDataItem->ReOrder();

							if( pDataItem->TypeId == segUnconnectData ) {
						
								if( pDataItem->Length == pBuff->GetSize() ) {

									return true;
									}
								}
							}
						}
					}
				}
			}

		CipReleaseBuffer(pBuff);

		// NOTE -- Anything that goes wrong in this layer is a failure of
		// the TCP/IP socket or a protocol violation. In either case, we
		// cannot recover or attempt any further communications over this
		// link so we can do nothing except tear it down and try again.

		Close();
		}

	return false;
	}

bool CEthernetIp::ExchangeConnected(CBuffer * &pBuff, DWORD O2T, DWORD T2O, WORD Sequence)
{
	if( IsLayerUp() ) {

		WORD	           Length    = pBuff->GetSize();

		CConnectedData    *pDataItem = BuffAddHead(pBuff, CConnectedData);

		CConnectedAddress *pAddrItem = BuffAddHead(pBuff, CConnectedAddress);

		CDataPacket       *pDataPack = BuffAddHead(pBuff, CDataPacket);

		pDataPack->Interface = 0;
	
		pDataPack->Timeout   = 0;
	
		pDataPack->ItemCount = 2;

		pDataPack->ReOrder();

		pAddrItem->TypeId     = segConnectedAddr;
	
		pAddrItem->Length     = 4;

		pAddrItem->Connection = O2T;

		pAddrItem->ReOrder();

		pDataItem->TypeId   = segConnectedData;
	
		pDataItem->Length   = 2 + Length;

		pDataItem->Sequence = Sequence;

		pDataItem->ReOrder();

		if( Send(pBuff, cmdSendUnitData) ) {

			if( Recv(pBuff, cmdSendUnitData, m_uTimeout) ) {

				pDataPack = BuffStripHead(pBuff, CDataPacket);

				pDataPack->ReOrder();

				if( pDataPack->ItemCount == 2 ) {

					pAddrItem = BuffStripHead(pBuff, CConnectedAddress);

					pAddrItem->ReOrder();

					if( pAddrItem->TypeId == segConnectedAddr ) {
					
						if( pAddrItem->Length == 4 ) {
						
							if( pAddrItem->Connection == T2O ) {

								pDataItem = BuffStripHead(pBuff, CConnectedData);

								pDataItem->ReOrder();

								if( pDataItem->TypeId == segConnectedData ) {
								
									if( pDataItem->Length == 2 + pBuff->GetSize() ) {

										if( pDataItem->Sequence == Sequence ) {

											return true;
											}
										}
									}
								}
							}
						}
					}
				}
			}

		CipReleaseBuffer(pBuff);

		// NOTE -- Anything that goes wrong in this layer is a failure of
		// the TCP/IP socket or a protocol violation. In either case, we
		// cannot recover or attempt any further communications over this
		// link so we can do nothing except tear it down and try again.

		Close();
		}

	return false;
	}

bool CEthernetIp::Close(void)
{
	if( IsLayerUp() ) {

		LayerDown();

		return true;
		}

	return false;
	}

// Layer Transition

bool CEthernetIp::LayerUp(void)
{
	if( RegisterSession() ) {

		m_fLayerUp = true;

		return true;
		}

	return false;
	}

bool CEthernetIp::LayerDown(void)
{
	if( m_pSock->IsLayerUp() ) {

		UnregisterSession();

		m_pSock->Close();
		}

	m_fLayerUp = false;

	ClearData();

	return true;
	}

void CEthernetIp::ClearData(void)
{
	m_Session = 0;
	}

// Session Control

bool CEthernetIp::RegisterSession(void)
{
	CBuffer *pBuff = CipAllocBuffer();

	if( pBuff ) {

		CRegisterPacket *pReg = BuffAddTail(pBuff, CRegisterPacket);

		pReg->Version  = 1;

		pReg->OptFlags = 0;

		pReg->ReOrder();

		if( Send(pBuff, cmdRegisterSession) ) {

			if( Recv(pBuff, cmdRegisterSession, m_uTimeout) ) {

				pReg = BuffStripHead(pBuff, CRegisterPacket);

				pReg->ReOrder();

				if( pReg->Version == 1 ) {

					m_Session = (((CPacket *) pReg)-1)->Session;

					CipReleaseBuffer(pBuff);

					return true;
					}

				m_Session = 0;
				}
			}

		CipReleaseBuffer(pBuff);
		}

	return false;
	}

bool CEthernetIp::UnregisterSession(void)
{
	CBuffer *pBuff = CipAllocBuffer();

	if( pBuff ) {

		if( Send(pBuff, cmdUnregisterSession) ) {

			return true;
			}

		CipReleaseBuffer(pBuff);
		}

	return false;
	}

// Transport Access

bool CEthernetIp::Send(CBuffer *pBuff, WORD Command)
{
	WORD     Length   = pBuff->GetSize();

	CPacket *pPacket  = BuffAddHead(pBuff, CPacket);

	pPacket->Command  = Command;
	pPacket->Length   = Length;
	pPacket->Session  = m_Session;
	pPacket->Status   = 0;
	pPacket->Options  = 0;

	memset(pPacket->Context, 0, sizeof(pPacket->Context));

	pPacket->ReOrder();

	return m_pSock->Send(pBuff);
	}

bool CEthernetIp::Recv(CBuffer * &pBuff, WORD Command, UINT uTime)
{
	pBuff = CipAllocBuffer();

	if( m_pSock->Recv(pBuff, sizeof(CPacket), uTime) ) {

		CPacket *pPacket = BuffStripHead(pBuff, CPacket);

		pPacket->ReOrder();

		if( pPacket->Command == Command ) {

			if( !m_Session || pPacket->Session == m_Session ) {

				if( pPacket->Length ) {

					if( m_pSock->Recv(pBuff, pPacket->Length, uTime) ) {

						return true;
						}

					return false;
					}

				return true;
				}
			}

		CipReleaseBuffer(pBuff);
		}

	return false;
	}

// End of File
