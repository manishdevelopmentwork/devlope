
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Device Configuration
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DevConNodeWnd_HPP

#define INCLUDE_DevConNodeWnd_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Referenced Classes
//

class CDevCon;

class CDevConNode;

//////////////////////////////////////////////////////////////////////////
//								
// Device Configuration Node View Window
//

class CDevConNodeWnd : public CMultiViewWnd
{
public:
	// Dynamic Class
	AfxDeclareDynamicClass();

	// Constructor
	CDevConNodeWnd(void);

	// Destructor
	~CDevConNodeWnd(void);

protected:
	// Static Data
	static UINT m_timerSelect;

	// Data Members
	CDevConNode * m_pItem;

	// Routing Control
	BOOL OnRouteMessage(MSG const &Message, LRESULT &lResult);

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	void OnPostCreate(void);

	// Overridables
	void    OnAttach(void);
	CString OnGetNavPos(void);
	BOOL    OnNavigate(CString const &Nav);
	void    OnExec(CCmd *pCmd);
	void    OnUndo(CCmd *pCmd);

	// Implementation
	void MakeTabs(void);
	void AttachViews(void);
};

// End of File

#endif
