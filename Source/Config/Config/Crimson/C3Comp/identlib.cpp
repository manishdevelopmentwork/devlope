
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Programming Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Identifier Library
//

class CIdentLibrary : public CBasicParser, public IIdentLibrary
{
	public:
		// Constructor
		CIdentLibrary(void);

		// Destructor
		~CIdentLibrary(void);

		// IBase Methods
		UINT Release(void);

		// IIdentLibrary Methods
		BOOL AddIdent(CError &Error, IClassServer *pClass, WORD ID, CString Ident);

		// IIdentServer Methods
		BOOL FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type);
		BOOL NameIdent(WORD ID, CString &Name);
		BOOL FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type);
		BOOL NameProp(WORD ID, WORD PropID, CString &Name);

	protected:
		// Argument List
		typedef CArray <CTypeDef> CTypeArray;

		// Function Definition
		struct CIdentDef
		{
			WORD		m_ID;
			CString		m_Name;
			CTypeDef	m_Type;
			};

		// Function List
		CArray <CIdentDef> m_List;

		// Lookup Indexes
		CMap <CString, UINT> m_IndexName;
		CMap <WORD,    UINT> m_IndexID;

		// Implementation
		void ParseIdent(IClassServer *pClass, CIdentDef &Ident);
		void ParseType(IClassServer *pClass, CTypeDef &Type);
		BOOL ParseName(CString &Name);
	};

//////////////////////////////////////////////////////////////////////////
//
// Identifier Library
//

// Instantiator

BOOL DLLAPI C3MakeIdentLib(IIdentLibrary * &pLib)
{
	pLib = New CIdentLibrary;

	return TRUE;
	}

// Constructor

CIdentLibrary::CIdentLibrary(void)
{
	}

// Destructor

CIdentLibrary::~CIdentLibrary(void)
{
	}

// IBase Methods

UINT CIdentLibrary::Release(void)
{
	delete this;

	return 0;
	}

// IIdentLibrary Methods

BOOL CIdentLibrary::AddIdent(CError &Error, IClassServer *pClass, WORD ID, CString Ident)
{
	m_pError = &Error;

	m_Lex.Attach(Ident);

	try {
		GetToken();

		CIdentDef Ident;

		Ident.m_ID    = ID;

		ParseIdent(pClass, Ident);

		UINT uIndex = m_List.Append(Ident);

		m_IndexName.Insert(Ident.m_Name, uIndex);

		m_IndexID.Insert(Ident.m_ID, uIndex);

		return TRUE;
		}

	catch(CUserException const &) {

		return FALSE;
		}
	}

// IIdentServer Methods

BOOL CIdentLibrary::FindIdent(CError *pError, CString Name, WORD &ID, CTypeDef &Type)
{
	INDEX n = m_IndexName.FindName(Name);

	if( !m_IndexName.Failed(n) ) {

		CIdentDef const &Ident = m_List[m_IndexName.GetData(n)];

		ID     = Ident.m_ID;

		Type   = Ident.m_Type;

		return TRUE;
		}

	return FALSE;
	}

BOOL CIdentLibrary::NameIdent(WORD ID, CString &Name)
{
	INDEX n = m_IndexID.FindName(ID);

	if( !m_IndexName.Failed(n) ) {

		CIdentDef const &Ident = m_List[m_IndexID.GetData(n)];

		Name = Ident.m_Name;

		return TRUE;
		}

	return FALSE;
	}

BOOL CIdentLibrary::FindProp(CError *pError, CString Name, WORD ID, WORD &PropID, CTypeDef &Type)
{
	return FALSE;
	}

BOOL CIdentLibrary::NameProp(WORD ID, WORD PropID, CString &Name)
{
	return FALSE;
	}

// Implementation

void CIdentLibrary::ParseIdent(IClassServer *pClass, CIdentDef &Ident)
{
	ParseType(pClass, Ident.m_Type);

	if( ParseName(Ident.m_Name) ) {

		Ident.m_Type.m_Flags |= flagActive;
		
		return;
		}

	Expected(L"identifier name");
	}

void CIdentLibrary::ParseType(IClassServer *pClass, CTypeDef &Type)
{
	if( m_Token.m_Group == groupKeyword ) {

		Type.m_Flags = 0;

		for(;;) {
			
			if( m_Token.m_Code == tokenConst ) {

				Type.m_Flags = flagConstant;
				
				GetToken();

				continue;
				}

			switch( m_Token.m_Code ) {

				case tokenInt:		Type.m_Type = typeInteger;	break;
				case tokenFloat:	Type.m_Type = typeReal;		break;
				case tokenString:	Type.m_Type = typeString;	break;

				default:		Expected(L"type name");

				}

			if( Type.m_Flags == 0 ) {

				Type.m_Flags |= flagWritable;
				}

			GetToken();

			break;
			}

		return;
		}

	Expected(L"type name");
	}

BOOL CIdentLibrary::ParseName(CString &Name)
{
	if( m_Token.m_Group == groupIdentifier ) {

		Name = m_Token.m_Const.GetStringValue();

		GetToken();

		return TRUE;
		}

	return FALSE;
	}

// End of File
