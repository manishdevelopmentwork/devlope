
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// GDI Bitmap Object
//

// Dynamic Class

AfxImplementDynamicClass(CBitmap, CGdiObject);

// Constructors

CBitmap::CBitmap(void)
{
	}

CBitmap::CBitmap(CBitmap const &That)
{
	AfxAssert(FALSE);
	}

CBitmap::CBitmap(ENTITY ID)
{
	CheckException(Create(ID));
	}

CBitmap::CBitmap(ENTITY ID, int xSize, int ySize)
{
	CheckException(Create(ID, xSize, ySize));
	}

CBitmap::CBitmap(ENTITY ID, CSize const &Size)
{
	CheckException(Create(ID, Size));
	}

CBitmap::CBitmap(int xSize, int ySize, UINT uPlanes, UINT uBits)
{	
	CheckException(Create(xSize, ySize, uPlanes, uBits));
	}

CBitmap::CBitmap(CSize const &Size, UINT uPlanes, UINT uBits)
{
	CheckException(Create(Size, uPlanes, uBits));
	}

CBitmap::CBitmap(int xSize, int ySize, UINT uPlanes, UINT uBits, PBYTE &pBits)
{	
	CheckException(Create(xSize, ySize, uPlanes, uBits, pBits));
	}

CBitmap::CBitmap(CSize const &Size, UINT uPlanes, UINT uBits, PBYTE &pBits)
{
	CheckException(Create(Size, uPlanes, uBits, pBits));
	}

CBitmap::CBitmap(CDC &DC, int xSize, int ySize)
{
	CheckException(Create(DC, xSize, ySize));
	}

CBitmap::CBitmap(CDC &DC, CSize const &Size)
{
	CheckException(Create(DC, Size));
	}

// Creation

BOOL CBitmap::Create(ENTITY ID)
{
	Detach(TRUE);

	HANDLE hBitmap = CModule::LoadBitmap(ID);

	m_Size.cx = 0;
	m_Size.cy = 0;

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(ENTITY ID, int xSize, int ySize)
{
	Detach(TRUE);

	HANDLE hBitmap = CModule::LoadBitmap(ID);

	m_Size.cx = xSize;
	m_Size.cy = ySize;

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(ENTITY ID, CSize const &Size)
{
	Detach(TRUE);

	HANDLE hBitmap = CModule::LoadBitmap(ID);

	m_Size.cx = Size.cx;
	m_Size.cy = Size.cy;

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(int xSize, int ySize, UINT uPlanes, UINT uBits)
{
	Detach(TRUE);

	HANDLE hBitmap = CreateBitmap(xSize, ySize, uPlanes, uBits, NULL);

	m_Size.cx = xSize;
	m_Size.cy = ySize;

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(CSize const &Size, UINT uPlanes, UINT uBits)
{
	Detach(TRUE);

	HANDLE hBitmap = CreateBitmap(Size.cx, Size.cy, uPlanes, uBits, NULL);

	m_Size.cx = Size.cx;
	m_Size.cy = Size.cy;

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(int xSize, int ySize, UINT uPlanes, UINT uBits, PBYTE &pBits)
{
	Detach(TRUE);

	BITMAPINFO Info;

	ZeroMemory(&Info, sizeof(Info));

	Info.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	Info.bmiHeader.biWidth       = xSize;
	Info.bmiHeader.biHeight      = ySize;
	Info.bmiHeader.biPlanes      = WORD(uPlanes);
	Info.bmiHeader.biBitCount    = WORD(uBits);
	Info.bmiHeader.biCompression = BI_RGB;
	Info.bmiHeader.biSizeImage   = abs(xSize) * abs(ySize) * uBits / 8;

	HBITMAP hBitmap = CreateDIBSection(NULL, &Info, DIB_RGB_COLORS, (void **) &pBits, 0, 0);

	if( hBitmap ) {

		ZeroMemory(pBits, Info.bmiHeader.biSizeImage);

		m_Size.cx = abs(xSize);
		m_Size.cy = abs(ySize);
		}

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(CSize const &Size, UINT uPlanes, UINT uBits, PBYTE &pBits)
{
	Detach(TRUE);

	BITMAPINFO Info;

	ZeroMemory(&Info, sizeof(Info));

	Info.bmiHeader.biSize        = sizeof(BITMAPINFOHEADER);
	Info.bmiHeader.biWidth       = Size.cx;
	Info.bmiHeader.biHeight      = Size.cy;
	Info.bmiHeader.biPlanes      = WORD(uPlanes);
	Info.bmiHeader.biBitCount    = WORD(uBits);
	Info.bmiHeader.biCompression = BI_RGB;
	Info.bmiHeader.biSizeImage   = abs(Size.cx) * abs(Size.cy) * uBits / 8;

	HBITMAP hBitmap = CreateDIBSection(0, &Info, DIB_RGB_COLORS, (void **) &pBits, 0, 0);

	if( hBitmap ) {

		ZeroMemory(pBits, Info.bmiHeader.biSizeImage);

		m_Size.cx = abs(Size.cx);
		m_Size.cy = abs(Size.cy);
		}

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(CDC &DC, int xSize, int ySize)
{
	Detach(TRUE);

	HANDLE hBitmap = CreateCompatibleBitmap(DC, xSize, ySize);

	m_Size.cx = xSize;
	m_Size.cy = ySize;

	return Attach(hBitmap);
	}

BOOL CBitmap::Create(CDC &DC, CSize const &Size)
{
	Detach(TRUE);

	HANDLE hBitmap = CreateCompatibleBitmap(DC, Size.cx, Size.cy);

	m_Size.cx = Size.cx;
	m_Size.cy = Size.cy;

	return Attach(hBitmap);
	}

// Attributes

CSize CBitmap::GetSize(void) const
{
	return m_Size;
	}

int CBitmap::GetWidth(void) const
{
	return m_Size.cx;
	}

int CBitmap::GetHeight(void) const
{
	return m_Size.cy;
	}

// Operations

void CBitmap::SetAlpha(PBYTE pData)
{
	PDWORD p = PDWORD(pData);

	UINT   n = m_Size.cx * m_Size.cy;

	DWORD  a = 0xFF000000;

	DWORD  f = afxColor(MAGENTA).Flip();

	while( n-- ) {

		if( *p == f ) {

			*p  = 0;
			}
		else
			*p |= a;

		p++;
		}
	}

void CBitmap::SetAlpha(PBYTE pData, BYTE bAlpha)
{
	PDWORD p = PDWORD(pData);

	UINT   n = m_Size.cx * m_Size.cy;

	DWORD  a = bAlpha << 24;

	if( bAlpha == 255 ) {

		while( n-- ) {

			if( *p ) {
				
				*p |= a;
				}

			p++;
			}

		return;
		}

	while( n-- ) {

		if( *p ) {

			BYTE r = GetRValue(*p);
			BYTE g = GetRValue(*p);
			BYTE b = GetRValue(*p);

			r = BYTE(r * bAlpha / 255);
			g = BYTE(g * bAlpha / 255);
			b = BYTE(b * bAlpha / 255);

			*p = RGB(r,g,b) | a;
			}

		p++;
		}
	}

void CBitmap::SetAlpha(PBYTE pData, CColor const &Find, BYTE bAlpha)
{
	PDWORD p = PDWORD(pData);

	UINT   n = m_Size.cx * m_Size.cy;

	DWORD  a = bAlpha << 24;

	DWORD  f = Find.Flip();

	while( n-- ) {

		if( *p == f ) {

			BYTE r = GetRValue(*p);
			BYTE g = GetRValue(*p);
			BYTE b = GetRValue(*p);

			r = BYTE(r * bAlpha / 255);
			g = BYTE(g * bAlpha / 255);
			b = BYTE(b * bAlpha / 255);

			*p = RGB(r,g,b) | a;
			}

		p++;
		}
	}

void CBitmap::SetAlpha(PBYTE pData, CColor const &Find, CColor const &Repl, BYTE bAlpha)
{
	PDWORD p = PDWORD(pData);

	UINT   n = m_Size.cx * m_Size.cy;

	DWORD  a = bAlpha << 24;

	DWORD  f = Find.Flip();

	BYTE   r = BYTE(Repl.GetRed  () * bAlpha / 255);

	BYTE   g = BYTE(Repl.GetGreen() * bAlpha / 255);
	
	BYTE   b = BYTE(Repl.GetBlue () * bAlpha / 255);

	DWORD  k = RGB(r,g,b) | a;

	while( n-- ) {

		if( *p == f ) {

			*p = k;
			}

		p++;
		}
	}

// Handle Lookup

CBitmap & CBitmap::FromHandle(HANDLE hObject)
{
	if( hObject == NULL ) {

		static CBitmap NullObject;

		return NullObject;
		}
		
	CLASS Class = AfxStaticClassInfo();

	return (CBitmap &) CGdiObject::FromHandle(hObject, Class);
	}

// End of File
