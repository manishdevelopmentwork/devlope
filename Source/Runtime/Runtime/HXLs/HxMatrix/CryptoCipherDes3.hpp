
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoCipherDes3_HPP

#define INCLUDE_CryptoCipherDes3_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoSym.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DES3 Symmetric Cipher
//

class CCryptoCipherDes3 : public CCryptoSym
{
	public:
		// Constructor
		CCryptoCipherDes3(void);

		// ICryptoSym
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Encrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Decrypt(PBYTE pOut, PCBYTE pIn, UINT uSize);
		void    Finalize(void);

	protected:
		// Data Members
		psDes3_t m_Ctx;
	};

// End of File

#endif
