
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2000 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// General Base Class
//

// Runtime Class

AfxImplementObjectClass(CObject);

// Constructor

CObject::CObject(void)
{
	}

// Virtual Destructor

CObject::~CObject(void)
{
	}

// Memory Management

void * CObject::operator new (UINT uSize)
{
	return AfxMalloc(uSize);
	}

void * CObject::operator new (UINT uSize, PCTXT pFile, UINT uLine)
{
	return AfxMalloc(uSize, pFile, uLine, TRUE);
	}

void * CObject::operator new (UINT uSize, void *pData)
{
	AfxValidateWritePtr(pData, uSize);
	
	return pData;
	}

void CObject::operator delete(void *pData)
{
	AfxFree(pData);
	}

void CObject::operator delete (void *pData, PCTXT pFile, UINT uLine)
{
	AfxFree(pData);
	}

void CObject::operator delete (void *pData, void *pAddr)
{
	}

// Runtime Information

PCTXT CObject::GetClassName(void) const
{
	return AfxObjectClassInfo()->GetClassName();
	}

UINT CObject::GetObjectSize(void) const
{
	return AfxObjectClassInfo()->GetObjectSize();
	}

CLASS CObject::GetBaseClass(void) const
{
	return AfxObjectClassInfo()->GetBaseClass();
	}

BOOL CObject::IsComponent(void) const
{
	return AfxObjectClassInfo()->IsComponent();
	}

REFGUID CObject::GetClassGuid(void) const
{
	return AfxObjectClassInfo()->GetClassGuid();
	}

// Type Checking

BOOL CObject::IsKindOf(CLASS Class) const
{
	return AfxObjectClassInfo()->IsKindOf(Class);
	}

// Object Validation

void CObject::AssertValid(void) const
{
	AfxValidateReadPtr(this, sizeof(ThisObject));
	}

// Object Implementation

void CObject::AfxConstruct(void *pObject)
{
	AfxTrace(L"ERROR: Dynamic construction is not available\n");

	AfxThrowSupportException();
	}

// Hidden Copy Constructor

CObject::CObject(CObject const &That)
{
	}

// Hidden Assignment Operator

void CObject::operator = (CObject const &That)
{
	}

// End of File
