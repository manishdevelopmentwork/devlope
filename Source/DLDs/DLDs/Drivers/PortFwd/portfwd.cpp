
#include "intern.hpp"

#include "portfwd.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Raw TCP/IP Active Port Driver
//

// Instantiator

INSTANTIATE(CPortForwardDriver);

// Constructor

CPortForwardDriver::CPortForwardDriver(void)
{
	m_Ident   = DRIVER_ID;

	m_pExpose = NULL;
	}

// Destructor

CPortForwardDriver::~CPortForwardDriver(void)
{
	}

// Configuration

void MCALL CPortForwardDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_Protocol   = GetWord(pData);

		m_TargetIP   = GetAddr(pData);

		m_TargetPort = GetWord(pData);

		m_ExposePort = GetWord(pData);
		}
	}

// Entry Points

void MCALL CPortForwardDriver::Service(void)
{
	for(;;) {

		if( !m_pExpose ) {

			m_pExpose = CreateSocket(IP_TCP);

			m_pExpose->SetOption(OPT_LINGER, 1);

			m_pExpose->Listen   (m_ExposePort);
			}
		else {
			UINT Expose;

			m_pExpose->GetPhase(Expose);

			if( Expose == PHASE_OPEN || Expose == PHASE_CLOSING ) {

				NewSession();

				m_pExpose->Release();

				m_pExpose = NULL;

				continue;
				}

			if( Expose == PHASE_ERROR ) {

				m_pExpose->Abort();

				m_pExpose->Release();

				m_pExpose = NULL;

				continue;
				}
			}

		Sleep(20);
		}
	}

// Implementation

void CPortForwardDriver::NewSession(void)
{
	m_pTarget = CreateSocket(IP_TCP);

	if( m_pTarget ) {

		IPREF IP   = IPREF(m_TargetIP);

		UINT  Port = m_TargetPort;

		m_pTarget->SetOption(OPT_LINGER, 1);

		m_pTarget->Connect  (IP, Port);

		if( WaitConnect() ) {

			ForwardData();
			}

		m_pTarget->Release();

		m_pTarget = NULL;
		}
	}

BOOL CPortForwardDriver::WaitConnect(void)
{
	for(;;) {
			
		UINT Expose;

		UINT Target;

		m_pExpose->GetPhase(Expose);

		m_pTarget->GetPhase(Target);

		if( Expose == PHASE_OPEN || Expose == PHASE_CLOSING ) {

			if( Target == PHASE_OPEN || Target == PHASE_CLOSING ) {

				return TRUE;
				}
			}

		if( Expose == PHASE_ERROR || Target == PHASE_ERROR ) {

			m_pExpose->Abort();

			m_pTarget->Abort();

			return FALSE;
			}

		Sleep(20);
		}

	return FALSE;
	}

void CPortForwardDriver::ForwardData(void)
{
	BOOL     fExpose = TRUE;

	BOOL     fTarget = TRUE;

	CBuffer *pExpose = NULL;

	CBuffer *pTarget = NULL;

	BOOL     fDone   = FALSE;

	SetTimer(30000);

	while( !fDone ) {

		BOOL fBusy  = FALSE;

		UINT Expose = PHASE_ERROR;

		UINT Target = PHASE_ERROR;

		m_pExpose->GetPhase(Expose);

		m_pTarget->GetPhase(Target);

		switch( Expose ) {

			case PHASE_CLOSING:

				if( fTarget ) {

					m_pTarget->Close();

					fTarget = FALSE;
					}
				break;

			case PHASE_OPEN:

				if( !pExpose ) {

					m_pExpose->Recv(pExpose);
					}

				if( pExpose ) {

					if( m_pTarget->Send(pExpose) == S_OK ) {

						fBusy   = TRUE;

						pExpose = NULL;
						}
					}
				break;

			case PHASE_ERROR:

				m_pTarget->Abort();

				fDone = TRUE;

				break;
			}

		switch( Target ) {

			case PHASE_CLOSING:

				if( fExpose ) {

					m_pExpose->Close();

					fExpose = FALSE;
					}
				break;

			case PHASE_OPEN:

				if( !pTarget ) {

					m_pTarget->Recv(pTarget);
					}

				if( pTarget ) {

					if( m_pExpose->Send(pTarget) == S_OK ) {

						fBusy   = TRUE;

						pTarget = NULL;
						}
					}
				break;

			case PHASE_ERROR:

				m_pExpose->Abort();

				fDone = TRUE;

				break;
			}

		if( fBusy ) {

			SetTimer(30000);
			}
		else {
			if( !fDone ) {

				if( !GetTimer() ) {

					m_pTarget->Close();

					m_pExpose->Close();

					break;
					}

				Sleep(20);
				}
			}
		}

	if( pExpose ) {

		pExpose->Release();
		}

	if( pTarget ) {

		pTarget->Release();
		}
	}

// End of File
