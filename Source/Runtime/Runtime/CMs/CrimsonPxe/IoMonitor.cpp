
#include "intern.hpp"

#include "IoMonitor.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// I/O Monitor
//

// Constructor

CIoMonitor::CIoMonitor(CJsonConfig *pJson)
{
	m_pIo = NULL;

	AfxGetObject("dev.devio", 0, IDeviceIo, m_pIo);

	ApplyConfig(pJson);

	StdSetRef();
}

// Destructor

CIoMonitor::~CIoMonitor(void)
{
}

// IUnknown

HRESULT CIoMonitor::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CIoMonitor::AddRef(void)
{
	StdAddRef();
}

ULONG CIoMonitor::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CIoMonitor::TaskInit(UINT uTask)
{
	if( m_pIo ) {

		SetThreadName("IoMonitor");

		if( m_DevName.IsEmpty() ) {

			AfxGetAutoObject(pPxe, "pxe", 0, ICrimsonPxe);

			pPxe->GetUnitName(m_DevName);
		}

		m_pIo->SetDigitalOut(m_uDoMode == 1);

		return TRUE;
	}

	Release();

	return FALSE;
}

INT CIoMonitor::TaskExec(UINT uTask)
{
	for( ;;) {

		if( m_uDoMode == 2 ) {

			bool on = false;

			AfxGetAutoObject(pCell, "net.cell", 0, ICellStatus);

			if( pCell ) {

				CCellStatusInfo Info;

				if( pCell->GetCellStatus(Info) ) {

					if( Info.m_fOnline ) {

						on = true;
					}
				}
			}

			m_pIo->SetDigitalOut(on);
		}

		RunDigital(m_Di, "Digital Input", m_pIo->GetDigitalIn());

		RunAnalog(m_Ai, "Analog Input", m_pIo->GetAnalogIn());

		RunAnalog(m_Vi, "PSU Voltage", m_pIo->GetPsuVoltage());

		Sleep(500);
	}

	return 0;
}

void CIoMonitor::TaskStop(UINT uTask)
{
}

void CIoMonitor::TaskTerm(UINT uTask)
{
	m_pIo->SetDigitalOut(false);

	Release();
}

// Implementation

void CIoMonitor::ApplyConfig(CJsonConfig *pJson)
{
	m_uDoMode = pJson->GetValueAsUInt("domode", 0, 0, 2);

	m_Di.m_uModeRise = pJson->GetValueAsUInt("dimode1", 0, 0, 5);

	m_Di.m_uModeFall = pJson->GetValueAsUInt("dimode0", 0, 0, 5);

	m_Ai.m_uModeHigh = pJson->GetValueAsUInt("aimodehi", 0, 0, 3);

	m_Ai.m_uModeLow  = pJson->GetValueAsUInt("aimodelo", 0, 0, 3);

	m_Ai.m_uTripHigh = pJson->GetValueAsUInt("aihi", 30000, 0, 38000);

	m_Ai.m_uTripLow  = pJson->GetValueAsUInt("ailo", 9000, 0, 38000);

	m_Vi.m_uModeHigh = pJson->GetValueAsUInt("vimodehi", 0, 0, 3);

	m_Vi.m_uModeLow  = pJson->GetValueAsUInt("vimodelo", 0, 0, 3);

	m_Vi.m_uTripHigh = pJson->GetValueAsUInt("vihi", 30000, 0, 38000);

	m_Vi.m_uTripLow  = pJson->GetValueAsUInt("vilo", 9000, 0, 38000);

	m_DevName = pJson->GetValue("devname", "");

	m_Recip1  = pJson->GetValue("msg1", "");

	m_Recip2  = pJson->GetValue("msg2", "");
}

void CIoMonitor::RunDigital(CDigital &Di, PCSTR pName, bool fState)
{
	if( fState && (Di.m_fInit || !Di.m_fState) ) {

		RunAction(Di.m_uModeRise, CPrintf("%s On", pName));
	}

	if( !fState && (Di.m_fInit || Di.m_fState) ) {

		RunAction(Di.m_uModeFall, CPrintf("%s Off", pName));
	}

	Di.m_fState = fState;

	Di.m_fInit  = false;
}

void CIoMonitor::RunAnalog(CAnalog &Ai, PCSTR pName, UINT uLevel)
{
	if( Ai.m_fInit || Ai.m_fState ) {

		if( uLevel <= Ai.m_uTripLow ) {

			RunAction(Ai.m_uModeLow, CPrintf("%s Below %u.%2.2uV at %u.%2.2uV",
							 pName,
							 Ai.m_uTripLow/1000,
							 Ai.m_uTripLow%1000/10,
							 uLevel/1000,
							 uLevel%1000/10
			));

			Ai.m_fState = false;

			Ai.m_fInit  = false;

			return;
		}
	}

	if( Ai.m_fInit || !Ai.m_fState ) {

		if( uLevel >= Ai.m_uTripHigh ) {

			RunAction(Ai.m_uModeHigh, CPrintf("%s Above %u.%2.2uV at %u.%2.2uV",
							  pName,
							  Ai.m_uTripHigh/1000,
							  Ai.m_uTripHigh%1000/10,
							  uLevel/1000,
							  uLevel%1000/10
			));

			Ai.m_fState = true;

			Ai.m_fInit  = false;

			return;
		}
	}
}

void CIoMonitor::RunAction(UINT uAction, PCSTR pMessage)
{
	switch( uAction ) {

		case 1:
		{
			if( m_uDoMode == 0 ) {

				m_pIo->SetDigitalOut(true);
			}
		}
		break;

		case 2:
		{
			if( m_uDoMode == 0 ) {

				m_pIo->SetDigitalOut(false);
			}
		}
		break;

		case 3:
		{
			SendMessage(m_Recip1, pMessage);

			SendMessage(m_Recip2, pMessage);
		}
		break;

		case 4:
		{
			AfxGetAutoObject(pPxe, "pxe", 0, ICrimsonPxe);

			pPxe->RestartSystem(0, 44);
		}
		break;
	}
}

void CIoMonitor::SendMessage(PCSTR pRecip, PCSTR pMessage)
{
	if( *pRecip ) {

		bool  fSmtp = strchr(pRecip, '@') ? true : false;

		PCSTR pName = fSmtp ? "msg.smtp" : "msg.sms";

		AfxGetAutoObject(pTrans, pName, 0, IMsgTransport);

		if( pTrans ) {

			if( pTrans->CanAcceptAddress(pRecip) ) {

				CAutoPointer<CMsgPayload> pPayload(New CMsgPayload);

				if( fSmtp ) {

					pPayload->m_Subject = "Alert from " + m_DevName;

					pPayload->m_Message = pMessage;
				}
				else {
					pPayload->m_Message = m_DevName + " : " + pMessage;
				}

				pPayload->m_RcptAddr   = pRecip;
				pPayload->m_Param[0]   = 0;
				pPayload->m_Param[1]   = 0;
				pPayload->m_SendNotify = 0;

				if( pTrans->QueueMessage(pPayload) ) {

					pPayload.TakeOver();
				}
			}
		}
	}
}

// End of File
