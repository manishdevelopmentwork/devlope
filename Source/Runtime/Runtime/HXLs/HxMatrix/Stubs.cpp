
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Matrix SSL Environment
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Static Data
//

static IEntropy * m_pEntropy = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Opaque Time
//

clink int osdepTimeOpen(void)
{
	return PS_SUCCESS;
	}

clink void osdepTimeClose(void)
{
	}

clink int32 psGetTime(psTime_t *t, void *userPtr)
{
	UINT ticks = GetTickCount();

	t->tv_sec  = (ticks / 1000);

	t->tv_usec = (ticks % 1000) * 1000;

	return PS_SUCCESS;
	}

clink int32 psDiffMsecs(psTime_t then, psTime_t now, void *userPtr)
{
	int32 dt = 0;

	dt += (now.tv_sec  - then.tv_sec ) * 1000;

	dt += (now.tv_usec - then.tv_usec) / 1000;

	return dt;
	}

//////////////////////////////////////////////////////////////////////////
//
// Faked File System
//

clink int32 psGetFileBuf(psPool_t *pool, const char *fileName, unsigned char **buf, int32 *bufLen)
{
	// Filename is in the form of two comma seperated hex numbers,
	// the first being the address of a buffer allocated by psMalloc
	// and the second being the size of that buffer. We simply copy
	// this data across and let Matrix think it has opened a file.

	char *p = (char *) fileName;

	*buf    = (unsigned char *) strtoul(p, &p, 16);

	if( *p++ == ',' ) {

		*bufLen = (int32 ) strtoul(p, &p, 16);

		if( *p++ == 0 ) {

			return PS_SUCCESS;
			}
		}

	return PS_FAILURE;
	}

//////////////////////////////////////////////////////////////////////////
//
// Mutual Exclusion
//

clink int32_t osdepMutexOpen(void)
{
	return PS_SUCCESS;
	}

clink void osdepMutexClose(void)
{
	}

clink int32 psCreateMutex(psMutex_t *mutex, uint32_t flags)
{
	IMutex *pMutex = NULL;

	pMutex = Create_Qutex();

	*mutex = pMutex;

	return PS_SUCCESS;
	}

clink void psLockMutex(psMutex_t *mutex)
{
	IMutex *pMutex = (IMutex *) *mutex;

	pMutex->Wait(FOREVER);
	}

clink void psUnlockMutex(psMutex_t *mutex)
{
	IMutex *pMutex = (IMutex *) *mutex;

	pMutex->Free();
	}

clink void psDestroyMutex(psMutex_t *mutex)
{
	IMutex *pMutex = (IMutex *) *mutex;

	pMutex->Release();
	}

//////////////////////////////////////////////////////////////////////////
//
// Entropy Source
//

clink int osdepEntropyOpen(void)
{
	AfxGetObject("entropy", 0, IEntropy, m_pEntropy);

	if( !m_pEntropy ) {

		AfxTrace("matrix: no entropy source available\n");
		}

	return PS_SUCCESS;
	}

clink void osdepEntropyClose(void)
{
	AfxRelease(m_pEntropy);
	}

clink int32 psGetEntropy(unsigned char *bytes, uint32 size, void *userPtr)
{
	if( m_pEntropy ) {

		if( m_pEntropy->GetEntropy(bytes, size) ) {

			return size;
			}

		AfxTrace("matrix: entropy source failed\n");
		}

	for( uint32 n = 0; n < size; n++ ) {

		bytes[n] = BYTE(rand());
		}

	return size;
	}

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics
//

clink int osdepTraceOpen(void)
{
	return PS_SUCCESS;
	}

clink void osdepTraceClose(void)
{
	}

clink void _psTrace(char const *msg)
{
	AfxTrace(msg);
	}

clink void _psTraceStr(char const *message, char const *value)
{
	AfxTrace(message, value);
	}

clink void _psTraceInt(char const *message, int32 value)
{
	AfxTrace(message, value);
	}

clink void _psTracePtr(char const *message, void const *value)
{
	AfxTrace(message, value);
	}

clink int psLogVaLevel(int n, const char *level, const char *unit, const char *fmt, va_list args)
{
	if( n <= 3 ) {

		AfxTrace("matrix : %s : %s : ", level, unit);

		AfxTraceArgs(fmt, args);
		}

	return 0;
	}

clink int psLogfFatal(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(1, level, unit, fmt, args);
	}

clink int psLogfError(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(2, level, unit, fmt, args);
	}

clink int psLogfWarning(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(3, level, unit, fmt, args);
	}

clink int psLogfInfo(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(4, level, unit, fmt, args);
	
	return 0;
	}

clink int psLogfDebug(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(5, level, unit, fmt, args);
	}

clink int psLogfVerbose(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(6, level, unit, fmt, args);
	}

clink int psLogfTrace(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(7, level, unit, fmt, args);
	
	return 0;
	}

clink int psLogfCallTrace(const char *level, const char *unit, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);

	return psLogVaLevel(8, level, unit, fmt, args);
	}

// End of File
