
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_Leds_HPP
	
#define	INCLUDE_Leds_HPP

//////////////////////////////////////////////////////////////////////////
//
// LED Proxy Object
//

class CLeds : public ILeds
{
	public:
		// Constructor
		CLeds(ILeds *pLeds);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);
		void METHOD SetLedLevel(UINT uPercent, bool fPersist);
		UINT METHOD GetLedLevel(void);

	protected:
		// Data Members
		ULONG   m_uRefs;
		ILeds * m_pLeds;
	};

// End of File

#endif
