
#include "intern.hpp"

#include "ciasdo.hpp"

#include "..\ciasdom\ciabase.cpp"

//////////////////////////////////////////////////////////////////////////
//
// CANOpen SDO Slave Driver
//

// Constructor

CCANOpenSDOSlave::CCANOpenSDOSlave(void)
{
	m_Ident   = DRIVER_ID;

	m_bDrop   = 8;

	m_bDecode = 0;
	}

// Destructor

CCANOpenSDOSlave::~CCANOpenSDOSlave(void)
{
	}

// Configuration

void MCALL CCANOpenSDOSlave::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_bDrop   = GetByte(pData);

		m_bDecode = GetByte(pData);

		m_fGuard  = GetByte(pData) ? TRUE : FALSE;
		}
	}
	
void MCALL CCANOpenSDOSlave::CheckConfig(CSerialConfig &Config)
{
	Config.m_bDrop = m_bDrop;

	m_uTime        = FOREVER;

	if( m_fGuard ) {

		Config.m_uFlags |= flagPrivate;
		}
	}
	
// Management

void MCALL CCANOpenSDOSlave::Attach(IPortObject *pPort)
{
	m_pData = new CCANPortHandler;

	pPort->Bind(m_pData);
	}

void MCALL CCANOpenSDOSlave::Detach(void)
{
	m_pData->Release();
	}

void MCALL CCANOpenSDOSlave::Open(void)
{
	}

// Service

void MCALL CCANOpenSDOSlave::Service(void)
{
	for(;;) {

		if( GetFrame() ) {

			if( HandleNodeGuard() ) {

				continue;
				}

			if( HandleNMT() )  {
				
				return;
				}
			
			if( HandleHeartBeat() ) {
				
				continue;
				}

			if( IsThisNode() ) {

				BYTE bCode = m_RxData.bData[0] >> 5;

				switch( bCode ) {

					// TODO -- Handle other SDO opcodes, including
					// the two-frame writes that we might get. Use
					// a state flag to indicate when we're waiting
					// for the other half of the write. Also handle
					// the SDO block mode transfers.

					case 0x2:
						HandleRead();
				
						break;

					case 0x1:
						HandleWrite();
					
						break;
					}
				}
			}
		}
	}

// Implementation

BOOL CCANOpenSDOSlave::GetFrame(void)
{
	return m_pData->GetFrame(m_RxData, m_uTime);
	}

BOOL CCANOpenSDOSlave::PutFrame(UINT uWait)
{
	return m_pData->PutFrame(m_TxData, uWait);
	}

void CCANOpenSDOSlave::StartSDO(void)
{
	m_TxData.wID = ((0xB << 7) | m_bDrop);
	}

BOOL CCANOpenSDOSlave::SDOAbort(DWORD dwCode, WORD wIndex, BYTE bSub)
{
	StartSDO();

	m_TxData.bCount   = 8;

	m_TxData.bZero	  = 0;

	m_TxData.bData[0] = 0x4 << 5;

	m_TxData.bData[1] = LOBYTE(wIndex);
	m_TxData.bData[2] = HIBYTE(wIndex);
	m_TxData.bData[3] = bSub;

	m_TxData.bData[4] = LOBYTE(LOWORD(dwCode));
	m_TxData.bData[5] = HIBYTE(LOWORD(dwCode));
	m_TxData.bData[6] = LOBYTE(HIWORD(dwCode));
	m_TxData.bData[7] = HIBYTE(HIWORD(dwCode));

	return PutFrame();
	}

BOOL CCANOpenSDOSlave::HandleRead(void)
{
	WORD  wIndex = MAKEWORD(m_RxData.bData[1], m_RxData.bData[2]);

	BYTE  bSub   = m_RxData.bData[3];

	DWORD lData  = 0;

	UINT  uSize  = GetData(wIndex, bSub, lData);

	/*AfxTrace("Get %4.4X-%u = %8.8X\n",wIndex, bSub, lData);*/

	StartSDO();

	m_TxData.bCount   = 8;

	m_TxData.bZero	  = 0;

	m_TxData.bData[0] = 0x2 << 5 | (4 - uSize) << 2 | 3;

	m_TxData.bData[1] = LOBYTE(wIndex);
	m_TxData.bData[2] = HIBYTE(wIndex);
	m_TxData.bData[3] = bSub;

	m_TxData.bData[4] = LOBYTE(LOWORD(lData));
	m_TxData.bData[5] = HIBYTE(LOWORD(lData));
	m_TxData.bData[6] = LOBYTE(HIWORD(lData));
	m_TxData.bData[7] = HIBYTE(HIWORD(lData));

	return PutFrame();
	}

BOOL CCANOpenSDOSlave::HandleWrite(void)
{
	WORD  wIndex = MAKEWORD(m_RxData.bData[1], m_RxData.bData[2]);

	BYTE  bSub   = m_RxData.bData[3];

	UINT  uSize  = 4 - ((m_RxData.bData[0] >> 2) & 0x03);

	DWORD lMask  = 0xFFFFFFFF >> (8 * (4 - uSize));

	DWORD lData  = (IntelToHost(PDWORD(m_RxData.bData + 4)[0]) & lMask);

	/*AfxTrace("Put %4.4X-%u = %8.8X (%u)\n",wIndex, bSub, lData, uSize);*/

	if( PutData(wIndex, bSub, uSize, lData) ) {

		StartSDO();

		m_TxData.bCount   = 8;

		m_TxData.bZero	  = 0;

		m_TxData.bData[0] = 0x3 << 5;

		m_TxData.bData[1] = LOBYTE(wIndex);
		m_TxData.bData[2] = HIBYTE(wIndex);
		m_TxData.bData[3] = bSub;

		m_TxData.bData[4] = 0;
		m_TxData.bData[5] = 0;
		m_TxData.bData[6] = 0;
		m_TxData.bData[7] = 0;

		return PutFrame();
		}

	return SDOAbort(0x06070010, wIndex, bSub);
	}

UINT CCANOpenSDOSlave::GetData(WORD wIndex, BYTE bSub, DWORD &Data)
{
	UINT uSize;

	if( (uSize = GetSpec(wIndex, bSub, Data)) ) {

		return uSize;
		}
	else {
		CAddress Addr;

		if( EncodeAddr(Addr, wIndex, bSub) ) {

			Addr.a.m_Type = addrLongAsLong;

			if( COMMS_SUCCESS(Read(Addr, &Data, 1)) ) {
				
				return sizeof(DWORD);
				}

			Addr.a.m_Type = addrWordAsWord;

			if( COMMS_SUCCESS(Read(Addr, &Data, 1)) ) {
				
				return sizeof(WORD);
				}

			Addr.a.m_Type = addrByteAsByte;

			if( COMMS_SUCCESS(Read(Addr, &Data, 1)) ) {
				
				return sizeof(BYTE);
				}
			}
		}

	return sizeof(DWORD);
	}

BOOL CCANOpenSDOSlave::PutData(WORD wIndex, BYTE bSub, UINT uSize, DWORD Data)
{
	SaveData(wIndex, Data);
	
	if( PutSpec(wIndex, bSub, uSize, Data) ) {

		return TRUE;
		}
	else {
		CAddress Addr;

		if( EncodeAddr(Addr, wIndex, bSub) ) {

			if( uSize == 4 ) {

				Addr.a.m_Type = addrLongAsLong;

				return COMMS_SUCCESS(Write(Addr, &Data, 1));
				}

			if( uSize == 2 ) {

				Addr.a.m_Type = addrWordAsWord;

				return COMMS_SUCCESS(Write(Addr, &Data, 1));
				}

			if( uSize == 1 ) {

				Addr.a.m_Type = addrByteAsByte;
				
				return COMMS_SUCCESS(Write(Addr, &Data, 1));
				}
			}
		}

	return FALSE;
	}

UINT CCANOpenSDOSlave::GetSpec(WORD wIndex, BYTE bSub, DWORD &Data)
{
	switch( wIndex ) {

		case 0x1000:

			switch( bSub ) {

				case 0:
					// Device Profile

					Data = GetDeviceProfile();
					
					return sizeof(DWORD);
				}
			break;
		
		case 0x1001:
			
			switch( bSub ) {
				
				case 0:
					// Error Flags

					Data = 0x00;
					
					return sizeof(BYTE);
				}
			break;

		case 0x1008:

			switch( bSub ) {

				case 0:

				// Manufacturer Device Name

				Data = MAKEWORD('G', '3');
					
				return sizeof(WORD);
				}
			break;

		case 0x1009:

			switch( bSub ) {

				case 0:

				// Manufacturer Hardware Version

				Data = MAKEWORD('1', '0');
					
				return sizeof(WORD);
				}
			break;

		case 0x100A:

			switch( bSub ) {

				case 0:

				// Manufacturer Software Version

				Data = MAKEWORD('2', '0');
					
				return sizeof(WORD);
				}
			break;

		
		case 0x1018:
			
			switch( bSub ) {
				
				case 0:
					// Indentity Count

					Data = 0x01;
					
					return sizeof(BYTE);
				
				case 1:
					// Vendor ID

					Data = 0x0000022C; // !!!!
					
					return sizeof(DWORD);
				
				case 2:
					// Product Code

					Data = MAKEWORD('G', '3');
					
					return sizeof(WORD);
				
				case 3:
					// Revision

					Data = 0x00010000;
					
					return sizeof(DWORD);
				
				case 4:
					// Serial Number

					Data = 0x00000000; // !!!!
					
					return sizeof(DWORD);
				}
			break;
		}

	return 0;
	}

BOOL CCANOpenSDOSlave::PutSpec(WORD wIndex, BYTE bSub, UINT uSize, DWORD Data)
{
	return FALSE;
	}

BOOL CCANOpenSDOSlave::EncodeAddr(CAddress &Addr, WORD wIndex, BYTE bSub)
{
	Addr.a.m_Type    = 0;

	Addr.a.m_Table   = 0x10;

	if( m_bDecode ) {

		Addr.a.m_Offset  = bSub;

		Addr.a.m_Offset |= (wIndex & 0xFF00);

		Addr.a.m_Extra   = (wIndex & 0x000F);
									
		Addr.a.m_Table  |= (wIndex & 0x00F0) >> 4;
		}
	else { 	
		Addr.a.m_Offset  = wIndex;

		Addr.a.m_Extra   = (bSub & 0x000F);

		Addr.a.m_Table  |= (bSub & 0x00F0) >> 4;
		} 

	return TRUE;
	}

// Device Profile Support

DWORD CCANOpenSDOSlave::GetDeviceProfile(void)
{
	return 0x00000000;
	}

// Extended Slave Support

void CCANOpenSDOSlave::SaveData(WORD wIndex, DWORD Data)
{
	switch( wIndex ) {

		case 0x100C:

			m_uGuardTime = Data;

			m_fGuard = TRUE;
			break;
		
		case 0x100D:

			m_uLifeTime  = Data;

			m_fGuard = TRUE;

			break;

		case 0x1016:

			if( Data >> 16 == m_bDrop ) {

				m_uHeartBeat = Data & 0xFFFF;

				m_fGuard = FALSE;
				}
			break;
		}
	}

// NodeGuard Support

BOOL CCANOpenSDOSlave::IsNodeGuard(void)
{
	return ( m_fGuard && (m_RxData.wID == NMT_COB_ID + m_bDrop) );
	}

BOOL CCANOpenSDOSlave::HandleNodeGuard(void)
{
	UINT uNow = GetTickCount();

	UINT uLife = m_uGuardTime * m_uLifeTime / 5;
	
	if( IsNodeGuard() ) {

		if( m_uState == NMTS_BOOT ) {

			return HandleBootup();
			} 
	
		Start();

		m_TxData.bCount	  = 1;

		m_TxData.wID	  = NMT_COB_ID | m_bDrop;

		m_uState	  = m_uState & 0x80 ? m_uState & ~0x80 : m_uState | 0x80;
	
		m_TxData.bData[0] = m_uState;
	
		PutFrame();

		m_uLast = uNow;

		return TRUE;
		}

	else if( m_fGuard && uLife && (uNow - m_uLast > uLife) ) {

		// TODO:  Life guarding event
		} 

	return FALSE;
	}

// Other NMT support

BOOL CCANOpenSDOSlave::HandleBootup(void)
{
	if( m_uState == NMTS_BOOT ) {

		m_uLast  = GetTickCount();

		Start();
	
		m_TxData.bCount = 1;

		m_TxData.wID  = NMT_COB_ID | m_bDrop;

		m_uState  = NMTS_PRE_OP;

		PutFrame();
		} 

	return TRUE;	
	}

BOOL CCANOpenSDOSlave::IsHeartBeat(void)
{
	return ( !m_fGuard && (m_RxData.wID == NMT_COB_ID + m_bDrop) );
	}


BOOL CCANOpenSDOSlave::HandleHeartBeat(void)
{
	if( IsHeartBeat() ) {

		if( m_uState == NMTS_BOOT ) {

			return HandleBootup();
			}

		UINT uNow = GetTickCount();
	
		if( uNow - m_uLast <= m_uHeartBeat / 5 ) {

			m_uLast = uNow;
			}

		else {	
			// TODO:  HeartBeat event
			}


		return TRUE;
		} 

	return FALSE;
	}

BOOL CCANOpenSDOSlave::HandleNMT(void)
{
	if( (m_RxData.bCount == 0x2) && (m_RxData.wID == 0) && (IsDrop(m_RxData.bData[1], TRUE)) ) {

		switch( m_RxData.bData[0] ) {

			case NMT_START:
				SetState(NMTS_OP);
				break;

			case NMT_STOP:
				SetState(NMTS_STOP);
				break;
		       			
			case NMT_PRE_OP:
				SetState(NMTS_PRE_OP);
				break;

			case NMTS_BOOT:
			case NMT_RESET:
			case NMT_RESET_COMM:
				m_uState = NMTS_BOOT;
				break;
			}

		return TRUE;
		}
 
	return FALSE;
	}

// Helpers

void CCANOpenSDOSlave::Start(void)
{
	memset(&m_TxData, 0, sizeof(FRAME));
	}

void CCANOpenSDOSlave::SetState(UINT uState)
{
	m_uState = (m_uState & 0x80) | uState;
	}

BOOL CCANOpenSDOSlave::IsDrop(BYTE bDrop, BOOL fWrite)
{
	if( fWrite && bDrop == 0 ) {

		return TRUE;
		}

	return ( bDrop == m_bDrop );
	}

BOOL CCANOpenSDOSlave::IsThisNode(void)
{
	return ((m_RxData.wID & 0x7F) == m_bDrop);
	}

// End of File
