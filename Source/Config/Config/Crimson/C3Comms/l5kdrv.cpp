
#include "intern.hpp"

#include "l5kdrv.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver
//

// Instantiator

ICommsDriver *	Create_ABL5kDriver(void)
{
	return New CABL5kDriver;
	}

// Constructor

CABL5kDriver::CABL5kDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Native Tags via L5K file";
	
	m_Version	= "1.04";
	
	m_ShortName	= "AB L5K Tags";

	m_wID		= 0x4042;

	C3_PASSED();
	}

// Binding Control

UINT CABL5kDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CABL5kDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CABL5kDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CABL5kDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABL5kDeviceOptions);
	}

// Address Management

BOOL CABL5kDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoParseAddress(Error, Addr, Text);
	}

BOOL CABL5kDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoExpandAddress(Text, Addr);
	}

BOOL CABL5kDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoSelectAddress(hWnd, Addr, fPart, TRUE);
	}

BOOL CABL5kDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoListAddress(pRoot, uItem, Data);
	}

// End of File
