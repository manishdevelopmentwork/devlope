
#include "Intern.hpp"

#include "ScsiBulkStatus.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Scsi Bulk Status Block
//

// Constructor

CScsiBulkStatus::CScsiBulkStatus(void)
{
	Init();
	}

// Endianess

void CScsiBulkStatus::HostToScsi(void)
{
	m_dwSig     = HostToIntel(m_dwSig);

	m_dwTag     = HostToIntel(m_dwTag);
	
	m_dwResidue = HostToIntel(m_dwResidue);
	}

void CScsiBulkStatus::ScsiToHost(void)
{
	m_dwSig     = IntelToHost(m_dwSig);

	m_dwTag     = IntelToHost(m_dwTag);
	
	m_dwResidue = IntelToHost(m_dwResidue);
	}

// Attributes

bool CScsiBulkStatus::IsValid(void) const
{
	return m_dwSig == sigBulkStatus;
	}

bool CScsiBulkStatus::IsPassed(void) const
{
	return m_bStatus == bulkPassed && m_dwResidue == 0;
	}
		
bool CScsiBulkStatus::IsFailed(void) const
{
	return m_bStatus != bulkPassed;
	}
		
// Operations

void CScsiBulkStatus::Init(void)
{
	memset(this, 0, sizeof(ScsiBulkStatus));

	m_dwSig = sigBulkStatus;
	}

// Debug

void CScsiBulkStatus::Dump(void)
{
	#if defined(_XDEBUG)

	AfxTrace("CSW\n");
		
	AfxTrace("Valid      0x%1.1X\n", IsValid());     
	AfxTrace("dwSig      0x%8.8X\n", m_dwSig);
	AfxTrace("dwTag      0x%8.8X\n", m_dwTag);
	AfxTrace("dwResidue  0x%8.8X\n", m_dwResidue);
	AfxTrace("bStatus    0x%2.2X\n", m_bStatus);

	#endif
	}

// End of File
