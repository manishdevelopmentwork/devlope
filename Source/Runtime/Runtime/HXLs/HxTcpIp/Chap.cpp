
#include "Intern.hpp"

#include "Chap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Ppp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PPP Challenge Handshake Authentication Protocol
//

// Constructor

CChap::CChap(CPpp *pPpp, CConfigPpp const &Config) : CPppLayer(pPpp, PROT_CHAP)
{
	m_fServer = (Config.m_uMode == pppServer);

	m_bID     = 0;

	m_uObj    = OBJ_CHAP;

	m_uCount  = 0;

	m_uTimer  = 0;

	m_uRetry  = ToTicks(5000);

	strcpy(m_sUser, Config.m_sUser);

	strcpy(m_sPass, Config.m_sPass);
	}

// Destructor

CChap::~CChap(void)
{
	}

// Management

void CChap::LowerLayerUp(void)
{
	if( m_fServer ) {

		m_uCount = 4;

		StartTimer();

		SendChallenge();
		}
	}

void CChap::LowerLayerDown(void)
{
	StopTimer();
	}

void CChap::OnTime(void)
{
	if( m_uTimer && GetTickCount() - m_uStart >= m_uTimer ) {

		if( m_uCount-- ) {

			StartTimer();

			SendChallenge();
			}
		else {
			StopTimer();

			ThisLayerDown();
			}
		}
	}

// Frame Handling

void CChap::OnRecv(CBuffer *pBuff)
{
	m_pLcp = BuffStripHead(pBuff, CLcpFrame);

	m_pLcp->NetToHost();

	TcpDebug(m_uObj, LEV_TRACE, "recv code %u\n", m_pLcp->m_bCode);

	if( m_fServer ) {

		switch( m_pLcp->m_bCode ) {

			case chapResponse:

				if( CheckResponse() ) {

					SendResponse(chapSuccess);
					
					StopTimer();

					ThisLayerUp();
					}
				else {
					SendResponse(chapFailure);

					StopTimer();
					
					ThisLayerDown();
					}

				m_bID++;
			
				break;
			}
		}
	}

// Implementation

void CChap::AddValue(CBuffer *pBuff, PBYTE pValue, UINT uSize)
{
	PBYTE pData = pBuff->AddTail(uSize + 1);

	*pData = BYTE(uSize);

	memcpy(pData + 1, pValue, uSize);
	}

void CChap::AddText(CBuffer *pBuff, PCTXT pText)
{
	UINT  uSize = strlen(pText);

	PBYTE pData = pBuff->AddTail(uSize);

	memcpy(pData, pText, uSize);
	}

void CChap::PutCHAP(CBuffer *pBuff, BYTE bCode, BYTE bID)
{
	TcpDebug(OBJ_CHAP, LEV_TRACE, "send code %u\n", bCode);

	CLcpFrame *pRep = BuffAddHead(pBuff, CLcpFrame);

	pRep->m_bCode   = bCode;

	pRep->m_bID     = bID;

	pRep->m_wLength = WORD(pBuff->GetSize());

	pRep->HostToNet();

	m_pPpp->PutPPP(m_wProtocol, pBuff);

	BuffRelease(pBuff);
	}

void CChap::SendResponse(BYTE bCode)
{
	CBuffer *pBuff = BuffAllocate(0);

	PutCHAP(pBuff, bCode, m_bID);
	}

void CChap::SendChallenge(void)
{
	CBuffer *pBuff = BuffAllocate(0);

	MakeChallenge();

	AddValue(pBuff, m_bChallenge, sizeof(m_bChallenge));
	
	PutCHAP(pBuff, chapChallenge, m_bID);
	}

BOOL CChap::CheckResponse(void)
{
	PBYTE pData = m_pLcp->m_bData;

	UINT  uSize = m_pLcp->m_wLength - sizeof(LCPFRAME);

	UINT  uTest = sizeof(m_bResponse);

	BYTE  bID   = m_pLcp->m_bID;

	if( bID == m_bID ) {

		MakeResponse();

		if( !memcmp(pData + 1, m_bResponse, uTest) ) {

			UINT  uUser = strlen(m_sUser);

			PCTXT pUser = PCTXT(pData + 1 + uTest);

			if( uSize == uTest + 1 + uUser ) {

				if( !strncasecmp(m_sUser, pUser, uUser) ) {

					return TRUE;
					}
				}
			}
		}

	return FALSE;
	}

void CChap::StartTimer(void)
{
	m_uStart = GetTickCount();

	m_uTimer = m_uRetry;
	}

void CChap::StopTimer(void)
{
	m_uCount = 0;

	m_uTimer = 0;
	}

// Challenge Support

void CChap::MakeChallenge(void)
{
	for( UINT n = 0; n < elements(m_bChallenge); n ++) {

		m_bChallenge[n] = BYTE(rand());				
		}
	}

void CChap::MakeResponse(void)
{
	UINT  uSize = 1 + strlen(m_sPass) + sizeof(m_bChallenge);

	PBYTE pWork = PBYTE(malloc(uSize));

	UINT  uPtr  = 0;

	Add(pWork, uPtr, &m_bID, 1);

	Add(pWork, uPtr, PBYTE(m_sPass), strlen(m_sPass));

	Add(pWork, uPtr, m_bChallenge, sizeof(m_bChallenge));

	md5(pWork, uSize, m_bResponse);

	Swap(m_bResponse, sizeof(m_bResponse));

	free(pWork);
	}

void CChap::Swap(PBYTE pData, UINT uSize)
{
	// Not needed with new MD5 code.
	}

void CChap::Add(PBYTE pBuff, UINT &uPtr, PBYTE pData, UINT uSize)
{
	memcpy(pBuff + uPtr, pData, uSize);

	uPtr += uSize;
	}

// End of File
