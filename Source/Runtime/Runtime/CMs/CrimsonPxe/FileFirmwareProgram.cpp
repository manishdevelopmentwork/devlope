
#include "intern.hpp"

#include "FileFirmwareProgram.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Filing System Firmware Programming Object
//

// Instantiator

global IFirmwareProgram * Create_FileFirmwareProgram(CString const &Root)
{
	return New CFileFirmwareProgram(Root);
}

// Constructor

CFileFirmwareProgram::CFileFirmwareProgram(CString const &Root) : CFileFirmwareProps(Root)
{
	m_pFile  = NULL;

	m_Image  = m_Root + "image.bin";

	m_fValid = false;
}

// Destructor

CFileFirmwareProgram::~CFileFirmwareProgram(void)
{
	delete m_pFile;
}

// IUnknown

HRESULT CFileFirmwareProgram::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IFirmwareProgram);

	return CFileFirmwareProps::QueryInterface(riid, ppObject);
}

ULONG CFileFirmwareProgram::AddRef(void)
{
	return CFileFirmwareProps::AddRef();
}

ULONG CFileFirmwareProgram::Release(void)
{
	return CFileFirmwareProps::Release();
}

// IFirmwareProps

bool CFileFirmwareProgram::IsCodeValid(void)
{
	return CFileFirmwareProps::IsCodeValid();
}

PCBYTE CFileFirmwareProgram::GetCodeVersion(void)
{
	return CFileFirmwareProps::GetCodeVersion();
}

UINT CFileFirmwareProgram::GetCodeSize(void)
{
	return CFileFirmwareProps::GetCodeSize();
}

PCBYTE CFileFirmwareProgram::GetCodeData(void)
{
	return CFileFirmwareProps::GetCodeData();
}

// IFirmwareProgram

bool CFileFirmwareProgram::ClearProgram(UINT uBlocks)
{
	if( m_pFile ) {

		delete m_pFile;

		m_pFile = NULL;
	}

	if( !m_pFile ) {

		m_pFile = New CAutoFile(m_Image, "w+");

		if( !!(*m_pFile) ) {

			return true;
		}
	}

	delete m_pFile;

	m_pFile = NULL;

	return false;
}

bool CFileFirmwareProgram::WriteProgram(PCBYTE pData, UINT uCount)
{
	if( m_pFile ) {

		if( m_pFile->Write(pData, uCount) == uCount ) {

			return true;
		}

		delete m_pFile;

		m_pFile = NULL;

		unlink(m_Image);
	}

	return false;
}

bool CFileFirmwareProgram::WriteVersion(PCBYTE pData)
{
	if( m_pFile ) {

		delete m_pFile;

		m_pFile = NULL;

		if( pData ) {

			CString   Guid(m_Root + "guid.bin");

			CAutoFile File(Guid, "w+");

			if( File ) {

				if( File.Write(pData, 16) == 16 ) {

					File.Close();

					m_fValid = true;

					return true;
				}

				unlink(Guid);
			}
		}
		else {
			m_fValid = true;

			return true;
		}

		unlink(m_Image);
	}

	return false;
}

bool CFileFirmwareProgram::StartProgram(UINT uParam)
{
	if( m_fValid ) {

		AfxGetAutoObject(pLinux, "os.linux", 0, ILinuxSupport);

		if( pLinux ) {

			// TODO -- This knows a little too much about file locations...

			if( m_Image.Find("osupdate") < NOTHING ) {

				static char const *cmd[] = {

					"/bin/rm",				"-rf /tmp/crimson/osexpand",
					"/bin/mkdir",				"-p /tmp/crimson/osexpand",
					"/bin/rm",				"-f /flip.me",
					"/bin/unzip",				"-q /tmp/crimson/osupdate/image.bin -d /tmp/crimson/osexpand",
					"/bin/sh",				"-c \"/bin/rm /tmp/crimson/osupdate/*\"",
					"/opt/crimson/bin/Flipper",		"--part1"
										" /bin /tmp/crimson/osexpand/bin"
										" /boot /tmp/crimson/osexpand/boot"
										" /lib /tmp/crimson/osexpand/lib"
										" /sbin /tmp/crimson/osexpand/sbin"
										" /usr /tmp/crimson/osexpand/usr",
					"/bin/rm",				"-rf /tmp/crimson/osexpand",
					"/bin/touch",				"/flip.me",
					NULL
				};

				for( UINT n = 0; cmd[n]; n+=2 ) {

					AfxTrace("run: %s %s\n", cmd[n+0], cmd[n+1]);

					int c = pLinux->CallProcess(cmd[n+0], cmd[n+1], ".", ".");

					if( c != 0 ) {

						AfxTrace("run: failed %u\n", c);

						return false;
					}
				}
			}
			else {
				static char const *cmd[] = {

					"/bin/rm",				"-rf /tmp/crimson/fwexpand",
					"/bin/mkdir",				"-p /tmp/crimson/fwexpand",
					"/bin/rm",				"-f /opt/crimson/.bin/flip.me",
					"/bin/unzip",				"-q /tmp/crimson/fwupdate/image.bin -d /tmp/crimson/fwexpand",
					"/bin/sh",				"-c \"/bin/chmod +x /tmp/crimson/fwexpand/*\"",
					"/bin/cp",				"/tmp/crimson/fwupdate/guid.bin /tmp/crimson/fwexpand",
					"/bin/sh",				"-c \"/bin/rm /tmp/crimson/fwupdate/*\"",
					"/tmp/crimson/fwexpand/Flipper",	"--part1 "
										"/opt/crimson/bin /tmp/crimson/fwexpand",
					"/bin/rm",				"-rf /tmp/crimson/fwexpand",
					"/bin/touch",				"/opt/crimson/.bin/flip.me",
					NULL
				};

				for( UINT n = 0; cmd[n]; n+=2 ) {

					AfxTrace("run: %s %s\n", cmd[n+0], cmd[n+1]);

					int c = pLinux->CallProcess(cmd[n+0], cmd[n+1], ".", ".");

					if( c != 0 ) {

						AfxTrace("run: failed %u\n", c);

						return false;
					}
				}
			}

			return true;
		}
	}

	return false;
}

// End of File
