
#include "Intern.hpp"

#include "MasterDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Master Comms Driver Object
//

// Constructor

CMasterDriver::CMasterDriver(void)
{
	}

// IUnknown

HRESULT CMasterDriver::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(ICommsMaster);

	return CCommsDriver::QueryInterface(riid, ppObject);
	}

// IDriver

WORD METHOD CMasterDriver::GetCategory(void)
{
	return DC_COMMS_MASTER;
	}

// ICommsMaster

WORD METHOD CMasterDriver::GetMasterFlags(void)
{
	return 0;
	}

CCODE METHOD CMasterDriver::Ping(void)
{
	return 2;
	}

CCODE METHOD CMasterDriver::Read(AREF Addr, PDWORD pData, UINT  uCount)
{
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE METHOD CMasterDriver::Write(AREF Addr, PDWORD pData, UINT  uCount)
{
	return CCODE_ERROR | CCODE_HARD;
	}

CCODE METHOD CMasterDriver::Atomic(AREF Addr, DWORD  Data,  DWORD dwMask)
{
	return CCODE_ERROR | CCODE_HARD;
	}

// End of File
