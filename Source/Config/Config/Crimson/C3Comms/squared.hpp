//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SQUARED_HPP
	
#define	INCLUDE_SQUARED_HPP 


/////////////////////////////////////////////////////////////////////////
//
// SquareD Driver Options
//

class CSquareDDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSquareDDriverOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Source;
	
			
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

/////////////////////////////////////////////////////////////////////////
//
// SquareD Device Options
//

class CSquareDDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSquareDDeviceOptions(void);

		// UI Managament
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Drop;
					
	protected:
	
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// SquareD Driver
//

class CSquareDDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CSquareDDriver(void);

		// Binding Control
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);
		
		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);

		// Address Management
		BOOL DoParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CSpace *pSpace, CString Text);

	protected:
		// Implementation
		void AddSpaces(void);
	};

// End of File

#endif
