
//////////////////////////////////////////////////////////////////////////
//
// Unitelway Base Driver

// DATA LINK TRANSACTIONS

#define	DLL_NULL	0x00
#define	DLL_ACK		0x01
#define	DLL_NAK		0x02
#define	DLL_EOT		0x03
#define	DLL_ENQ		0x04
#define	DLL_FRAME	0x05
#define	DLL_ERROR	0x06
#define	DLL_BUSY	0x07
#define	DLL_RSP_ECHOED	0x08

// DATA LINK TYPES

#define	TYPE_POLL1	0x00
#define	TYPE_REPLY	0x01
#define	TYPE_SCAN	0x02
#define	TYPE_POLL2	0x03

// PROTOCOL CONSTANTS

#define	DLL_POLL_TIMEOUT	500
#define	DLL_REPLY_TIMEOUT	1000
#define	DLL_GAP_TIMEOUT		(500 / DLL_PAUSE)
#define	DLL_PAUSE		10
#define	NET_POLL_GAP		50
#define	RETRIES			5

#define OPRDI	0
#define OPRDS	1
#define	OPWRI	0x10
#define	OPWRS	0x11
#define	OPREAD	0x36
#define	OPWRITE	0x37
#define OPTCR	0x82
#define OPTCW	0x83

// Space Identifiers
#define	MWORD	1  // internal words
#define	SWORD	2  // system words
#define	CWORD	3  // constant words
#define	MLONG	4  // internal longs
#define	SLONG	5  // system longs
#define	CLONG	6  // constant longs
#define	MREAL	7  // internal bits
#define	MBYTE	8  // internal bytes
#define	SBYTE	9  // system bytes
#define	MBIT	10 // internal bits
#define	SBIT	11 // system bits
#define CREAL	12 // constant reals
#define TPRS	13 // PL7 Timer Preset
#define TVAL	14 // PL7 Timer Value
#define TRUN	15 // PL7 Timer Running
#define TDONE	16 // PL7 Timer Done
#define TMPRS	17 // IEC Timer Preset
#define TMVAL	18 // IEC Timer Value
#define TMDONE	19 // IEC Timer Done
#define CTRP	20 // Counter Preset
#define CTRV	21 // Counter Value
#define CTRE	22 // Counter Empty
#define CTRD	23 // Counter Done
#define CTRF	24 // Counter Full
#define POLL_ONLY	25 // Generate Poll (Master Only)
#define	ACK_DEL	26 // Ack Time Delay

// Data Segments
#define	BITSEG	0x64
#define	MEMSEG	0x68
#define	CONSEG	0x69
#define	SYSSEG	0x6A
#define	SYSBSEG	0 // system byte segment
#define TCSEG	0x81

// Types NOTE:types other than 7, 8 don't work with test unit
#define	MEMBIT	5
#define	MEMBYTE	6
#define	MEMWORD	7
#define	MEMLONG	8
#define	MEMREAL	10
#define	CONWORD	7
#define	CONLONG	8
#define	SYSBIT	6
#define	SYSBYTE 1
#define	SYSWORD	7
#define	SYSLONG	8
#define PL7TYPE	1
#define IECTYPE	2
#define CTRTYPE	3

// Timer/Counter Item Access
#define	ITPRS	2
#define ITVAL	3
#define ITRUN	5
#define ITDONE	6
#define	ITMPRS	3
#define ITMVAL	4
#define ITMQ	6
#define ICPRS	2
#define ICVAL	3
#define ICE	4
#define ICD	5
#define ICF	6

// Device Identification
#define	MASTERDEVICE 0xFE

#pragma pack(1)

struct	OPFRAME
{
	BYTE bThisDrop;
	BYTE bOp;
	BYTE bSeg;
	BYTE bType;
	UINT uCategory;
	BYTE bItem;
	BYTE Transaction;
	BYTE bThatDrop;
	};

#pragma pack()

class CUnitelwayBaseDriver : public CMasterDriver
{
	public:
		// Constructor
		CUnitelwayBaseDriver(void);

		// Destructor
		~CUnitelwayBaseDriver(void);

	protected:

		// Data Members
		BYTE		m_bTx[256];
		BYTE		m_bRx[256];
		UINT		m_uPtr;
		OPFRAME		m_OpFrame;
		OPFRAME	*	m_pOpFrame;

		// Frame Building
		void PutShortHeader(UINT uOffset);
		void PutWordsHeader(UINT uOffset, UINT uCount);
		void PutTCHeader(UINT uOffset);
		UINT FormBitRead(AREF Addr, UINT uCount);
		void FormTCByteRead(AREF Addr);
		UINT FormWordRead(AREF Addr, UINT uCount);
		UINT FormLongRead(AREF Addr, UINT uCount);
		void FormBitWrite(AREF Addr);
		UINT FormWordWrite(AREF Addr, UINT uCount);
		UINT FormLongWrite(AREF Addr, UINT uCount);
		void AddByte(BYTE bData);
		void AddData(UINT uData);

		// Response Data
		BOOL GetBitsResponse(PDWORD pData, UINT * pCount, UINT uStart);
		BOOL GetWordResponse(PDWORD pData, UINT * pCount);
		BOOL GetLongResponse(PDWORD pData, UINT * pCount);

		// Transport Layer
		void SendFrame(void);
		
		// Port Access
		void Put(BYTE b);
		UINT Get(void);

		// Helpers
		void FillOpFrame(UINT uTable);
		void Preamble(void);
		DWORD GetData( UINT uPos, UINT uByteCount );

		// Debug
		void Dbg( UINT u, DWORD d );
		void sDbg( UINT a );
		void sDbg( UINT a, UINT b );
		void sDbg( UINT a, UINT b, UINT c );
	};

// End of File
