
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Generic Command
//

// Runtime Class

AfxImplementRuntimeClass(CCmd, CObject);

// Constructor

CCmd::CCmd(void)
{
	m_View = NOTHING;

	m_Side = NOTHING;
	
	m_Menu = L"Editing";
	}

//////////////////////////////////////////////////////////////////////////
//
// Grouped Command
//

// Runtime Class

AfxImplementRuntimeClass(CCmdMulti, CCmd);

// Constructor

/*CCmdMulti::CCmdMulti(CString Item, CString Menu)
{
	m_Menu  = Menu;

	m_Item  = Item;

	m_uEach = 0;
	}*/

CCmdMulti::CCmdMulti(CString Item, CString Menu, UINT uCode)
{
	m_Menu  = Menu;

	m_Item  = Item;

	m_uCode = uCode;
	}

// End of File
