
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_SqlRecordList_HPP

#define INCLUDE_SqlRecordList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CSqlRecord;

//////////////////////////////////////////////////////////////////////////
//
// SQL Record List
//

class CSqlRecordList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CSqlRecordList(void);

		// Item Access
		CSqlRecord * GetItem(INDEX Index) const;
		CSqlRecord * GetItem(UINT uPos) const;

		CSqlRecord * GetRecord(UINT uRow);
		UINT         GetRecordPos(CSqlRecord *pFind);

		// Download Support
		BOOL MakeInitData(CInitData &Init);
	};

// End of File

#endif
