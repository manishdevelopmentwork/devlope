#include "intern.hpp"

#include "CognexDataDriver.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Cognex Data Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CCognexDataDeviceOptions, CUIItem);

// Constructor

CCognexDataDeviceOptions::CCognexDataDeviceOptions(void)
{
	m_Device = 0;

	m_IP     = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));

	m_Port   = 23;

	m_Keep	 = TRUE;

	m_Ping   = FALSE;

	m_Time1  = 5000;

	m_Time2  = 200;

	m_Time3  = 2500;

	m_User = "admin";

	m_Pass = "";
	}

void CCognexDataDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pWnd->EnableUI("Time2", !m_Keep);
			}
		}
	}

void CCognexDataDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Device);

	Meta_AddInteger(IP);

	Meta_AddInteger(Port);

	Meta_AddInteger(Keep);

	Meta_AddInteger(Ping);

	Meta_AddInteger(Time1);

	Meta_AddInteger(Time2);

	Meta_AddInteger(Time3);

	Meta_AddString(User);

	Meta_AddString(Pass);
	}

BOOL CCognexDataDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Device));

	Init.AddLong(LONG(m_IP));
	
	Init.AddWord(WORD(m_Port));

	Init.AddByte(BYTE(m_Keep));

	Init.AddByte(BYTE(m_Ping));

	Init.AddWord(WORD(m_Time1));

	Init.AddWord(WORD(m_Time2));

	Init.AddWord(WORD(m_Time3));

	Init.AddText(m_User);

	Init.AddText(m_Pass);

	return TRUE;
	}


//////////////////////////////////////////////////////////////////////////
//
// Cognex Data Driver
//

// Instantiator

ICommsDriver *Create_CognexDataDriver(void)
{
	return New CCognexDataDriver;
	}

// Constructor
CCognexDataDriver::CCognexDataDriver(void)
{
	m_wID		= 0x40AB;

	m_uType		= driverMaster;

	m_Manufacturer	= "Cognex";

	m_DriverName	= "In-Sight Camera Data";

	m_Version	= "1.00";

	m_ShortName	= "In-Sight Data";

	m_DevRoot	= "Data";

	AddSpaces();
	}

// Destructor
CCognexDataDriver::~CCognexDataDriver(void)
{
	DeleteAllSpaces();
	}

// Binding Control
UINT CCognexDataDriver::GetBinding(void)
{
	return bindEthernet;
	}


// Configuration
CLASS CCognexDataDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CCognexDataDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCognexDataDeviceOptions);
	}

void CCognexDataDriver::AddSpaces(void)
{
	AddSpace(new CSpace(addrNamed,	"Cell",	"   Spreadsheet Cell", 10, 0, 0x658F, addrWordAsWord, addrRealAsReal));
	AddSpace(new CSpace(TS0,	"Str0",	"   User String 0", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS1,	"Str1",	"   User String 1", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS2,	"Str2",	"   User String 2", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS3,	"Str3",	"   User String 3", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS4,	"Str4",	"   User String 4", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS5,	"Str5",	"   User String 5", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS6,	"Str6",	"   User String 6", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS7,	"Str7",	"   User String 7", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS8,	"Str8",	"   User String 8", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(TS9,	"Str9",	"   User String 9", 10, 0, 0x658F, addrWordAsWord));
	AddSpace(new CSpace(SE,		"SE",	"   Set Event", 10, 0, 0, addrWordAsWord));
	AddSpace(new CSpace(OL,		"OL",	"   Online Status", 10, 0, 0, addrWordAsWord));
	AddSpace(new CSpace(GF,		"GF",	"   Get File", 10, 0, 0, addrWordAsWord));
	}

// Address Management

BOOL CCognexDataDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCognexDataDialog Dlg(*this, Addr, fPart);

	return Dlg.Execute(CWnd::FromHandle(hWnd));
	}

// Address Helpers

BOOL CCognexDataDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CSpace *pSpace = GetSpace(Text);

	Addr.m_Ref = 0;

	if( pSpace ) {

		CString Type = StripType(pSpace, Text);

		Addr.a.m_Type = pSpace->TypeFromModifier(Type);

		UINT Cmd = IsCommand(pSpace);

		if( Cmd ) {

			Addr.a.m_Table = pSpace->m_uTable;

			if( Cmd == cmdString ) {

				Addr.a.m_Extra = 1;

				Addr.a.m_Type = addrLongAsLong;
				}

			return TRUE;
			}

		PTXT pError = NULL;

		Addr.a.m_Offset = wcstoul(Text.Mid(pSpace->m_Prefix.GetLength() + 1), &pError, pSpace->m_uRadix);

		UINT Col = Text.GetAt(pSpace->m_Prefix.GetLength());

		Col -= 'A';

		UINT MaxCol = (UINT) (pSpace->m_uMaximum >> 10);

		UINT MinCol = (UINT) (pSpace->m_uMinimum >> 10);

		UINT MaxRow = pSpace->m_uMaximum & 0x3FF;

		UINT MinRow = pSpace->m_uMinimum & 0x3FF;

		if( Col < MinCol || Col > MaxCol ) {
			
			Error.Set(IDS_ERROR_OFFSETRANGE);

			return FALSE;
			}

		else if( Addr.a.m_Offset < MinRow || Addr.a.m_Offset > MaxRow ) {

			Error.Set(IDS_ERROR_OFFSETRANGE);

			return FALSE;
			}

		Addr.a.m_Offset |= (Col << 10);

		Addr.a.m_Table = pSpace->m_uTable;

		if( IsString(pSpace) ) {

			Addr.a.m_Extra = 1;

			Addr.a.m_Type = addrLongAsLong;
			}

		return TRUE;
		}

	Error.Set(IDS_DRIVER_ADDR_INVALID);

	return FALSE;
	}

BOOL CCognexDataDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CSpace *pSpace = GetSpace(Addr);

	if( pSpace ) {

		if( IsCommand(pSpace) ) {

			Text.Printf("%s", pSpace->m_Prefix);

			return TRUE;
			}

		char Col = (char) ((Addr.a.m_Offset >> 10) + 'A');

		int Row = Addr.a.m_Offset & 0x3FF;

		UINT uType = Addr.a.m_Type;

		if( uType == pSpace->m_uType ) {

			Text.Printf("%s%c%3.3d",
				pSpace->m_Prefix,
				Col,
				Row);
			}
		else {
			Text.Printf("%s%c%3.3d.%s",
				pSpace->m_Prefix,
				Col,
				Row,
				pSpace->GetTypeModifier(uType));
			}

		return TRUE;
		}

	return FALSE;
	}

UINT CCognexDataDriver::IsCommand(const CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case OL:
			return cmdInteger;

		case GF:
		case SE:
			return cmdString;
		}

	return 0;
	}

BOOL CCognexDataDriver::IsString(const CSpace *pSpace)
{
	switch( pSpace->m_uTable ) {

		case TS0:
		case TS1:
		case TS2:
		case TS3:
		case TS4:
		case TS5:
		case TS6:
		case TS7:
		case TS8:
		case TS9:
			return TRUE;
		}

	return FALSE;
	}

////////////////////////////////////////////////////////////////////////
//
// Cognex Data Dialog
//

AfxImplementRuntimeClass(CCognexDataDialog, CStdAddrDialog);

CCognexDataDialog::CCognexDataDialog(CCognexDataDriver &Driver, CAddress &Addr, BOOL fPart)
{
	m_pDriver = &Driver;

	m_pAddr = &Addr;

	m_fPart = fPart;

	SetName(L"CognexDataDlg");
	}

AfxMessageMap(CCognexDataDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)

	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSelChange)

	AfxDispatchCommand(IDOK, OnOkay)

	AfxMessageEnd(CCognexDataDialog)
	};

void CCognexDataDialog::LoadList(void)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	ListBox.SetRedraw(FALSE);

	ListBox.ResetContent();

	int nTab[] = { 32, 96, 160 };
		
	ListBox.SetTabStops(elements(nTab), nTab);

	CString Entry;
			
	Entry.Printf("<%s>\t%s", CString(IDS_DRIVER_NONE), CString(IDS_DRIVER_NOSELECTION));

	ListBox.AddString(Entry, NOTHING );

	CSpaceList &List = m_pDriver->GetSpaceList();

	INDEX Find = INDEX(NOTHING);

	if( List.GetCount() ) {

		INDEX n = List.GetHead();

		while( !List.Failed(n) ) {

			CSpace *pSpace = List[n];

			CString Entry;

			Entry.Printf("%s\t%s", pSpace->m_Prefix, pSpace->m_Caption);

			ListBox.AddString(Entry, DWORD(n));

			if( m_pAddr->a.m_Table == pSpace->m_uTable ) {

				Find = n;
				}

			List.GetNext(n);
			}
		}

	ListBox.SelectData(DWORD(Find));

	ListBox.SetRedraw(TRUE);

	ListBox.Invalidate(TRUE);

	OnSelChange(1001, ListBox);
	}

void CCognexDataDialog::OnSelChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uFind = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uFind));

	if( (DWORD &) Index != NOTHING ) {

		m_pSpace = m_pDriver->GetSpace(Index);

		ShowAddress();

		ShowDetails();
		
		return;
		}

	m_pSpace = NULL;

	ClearDetails();

	ShowAddress();
	}

void CCognexDataDialog::ShowAddress(void)
{
	DoEnables();

	if( m_pSpace ) {

		GetDlgItem(2001).SetWindowText(m_pSpace->m_Prefix);

		if( IsCell(m_pSpace) ) {

			char Col = (char) ((m_pAddr->a.m_Offset >> 10) + 'A');

			int Row = m_pAddr->a.m_Offset & 0x3FF;

			GetDlgItem(2003).SetWindowTextW(CPrintf("%c", Col));

			GetDlgItem(2002).SetWindowTextW(CPrintf("%3.3d", Row));

			if( m_pAddr->m_Ref ) {

				UINT uTypeSel = (m_pAddr->a.m_Type == addrWordAsWord) ? 0 : 1;

				CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

				Combo.SetCurSel(uTypeSel);
				}
			}
		}
	else {

		GetDlgItem(2006).SetWindowTextW(CString(IDS_DRIVER_NONE));
		}
	}

void CCognexDataDialog::ShowDetails(void)
{
	CString Min;

	CString Max;

	if( m_pSpace ) {

		if( IsCell(m_pSpace) ) {

			UINT uMin = m_pSpace->m_uMinimum;

			UINT uMax = m_pSpace->m_uMaximum;

			char MinCol = (char) ((uMin >> 10) + 'A');

			int MinRow = (uMin & 0x3FF);

			char MaxCol = (char) ((uMax >> 10) + 'A');

			int MaxRow = (uMax & 0x3FF);

			Min.Printf("%c%3.3d", MinCol, MinRow);

			Max.Printf("%c%3.3d", MaxCol, MaxRow);

			GetDlgItem(3006).SetWindowText(m_pSpace->GetRadixAsText());
			}
		else {
			Min.Printf("%s", m_pSpace->GetValueAsText(m_pSpace->m_uMinimum));

			Max.Printf("%s", m_pSpace->GetValueAsText(m_pSpace->m_uMaximum));
			}

		}

	GetDlgItem(3002).SetWindowText(Min);

	GetDlgItem(3004).SetWindowText(Max);
	}

void CCognexDataDialog::ClearDetails(void)
{
	GetDlgItem(3002).SetWindowTextW("");

	GetDlgItem(3004).SetWindowTextW("");

	GetDlgItem(3006).SetWindowTextW("");
	}

void CCognexDataDialog::DoEnables(void)
{
	if( !m_pSpace ) {

		GetDlgItem(2001).ShowWindow(FALSE);
		
		GetDlgItem(2002).ShowWindow(FALSE);
		
		GetDlgItem(2003).ShowWindow(FALSE);

		GetDlgItem(2004).ShowWindow(FALSE);

		GetDlgItem(2005).ShowWindow(FALSE);
		
		GetDlgItem(2006).ShowWindow(TRUE);
		}

	else if( m_pDriver->IsCommand(m_pSpace) ) {

		GetDlgItem(2001).ShowWindow(TRUE);

		GetDlgItem(2002).ShowWindow(FALSE);

		GetDlgItem(2003).ShowWindow(FALSE);
		
		GetDlgItem(2004).ShowWindow(FALSE);

		GetDlgItem(2005).ShowWindow(FALSE);

		GetDlgItem(2006).ShowWindow(FALSE);
		
		GetDlgItem(3006).ShowWindow(FALSE);
		}
	else {
		BOOL fCombo = !m_pDriver->IsString(m_pSpace);

		GetDlgItem(2001).ShowWindow(TRUE);

		GetDlgItem(2002).ShowWindow(TRUE);

		GetDlgItem(2003).ShowWindow(TRUE);

		GetDlgItem(2004).ShowWindow(TRUE);

		GetDlgItem(2005).ShowWindow(TRUE);

		GetDlgItem(2005).EnableWindow(fCombo);
		
		GetDlgItem(2006).ShowWindow(FALSE);

		GetDlgItem(3006).ShowWindow(TRUE);
		}
	}

BOOL CCognexDataDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

	Combo.AddString(L"Word", addrWordAsWord);

	Combo.AddString(L"Real", addrRealAsReal);

	Combo.SelectString(L"Word");

	CEditCtrl &ColEdit = (CEditCtrl &) GetDlgItem(2003);

	CEditCtrl &RowEdit = (CEditCtrl &) GetDlgItem(2002);

	ColEdit.LimitText(1);

	RowEdit.LimitText(3);

	LoadList();

	ShowAddress();

	return TRUE;
	}

// Notification Handlers

void CCognexDataDialog::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CCognexDataDialog::OnSpaceChange(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox = (CListBox &) GetDlgItem(1001);

	UINT  uPos  = ListBox.GetCurSel();

	INDEX Index = INDEX(ListBox.GetItemData(uPos));

	if( DWORD(Index) != NOTHING ) {

		m_pSpace = m_pDriver->GetSpace(Index);

		DoEnables();

		ShowDetails();
		}
	}

BOOL CCognexDataDialog::OnOkay(UINT uID)
{
	if( uID == IDOK ) {

		if( m_pSpace ) {

			CError Error;

			CAddress Addr;

			CString Text = m_pSpace->m_Prefix;

			Text += GetDlgItem(2003).GetWindowText().ToUpper();

			Text += GetDlgItem(2002).GetWindowText();

			Text += GetTypeText();

			if( m_pDriver->ParseAddress(Error, Addr, NULL, Text) ) {

				*m_pAddr = Addr;

				EndDialog(TRUE);

				return TRUE;
				}

			Error.Show(ThisObject);

			return TRUE;
			}
		}

	m_pAddr->m_Ref = 0;

	EndDialog(TRUE);

	return TRUE;
	}

BOOL CCognexDataDialog::IsCell(const CSpace *pSpace)
{
	if( pSpace->m_uTable == addrNamed ) {

		return TRUE;
		}

	else if( m_pDriver->IsString(pSpace) ) {

		return TRUE;
		}

	return FALSE;
	}

CString CCognexDataDialog::GetTypeText(void)
{
	if( m_pSpace ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(2005);

		UINT uPos = Combo.GetCurSel();

		if( uPos < NOTHING ) {

			UINT uType =  Combo.GetItemData(uPos);

			return L"." + m_pSpace->GetTypeModifier(uType);
			}
		}

	return L"";
	}

// End of File