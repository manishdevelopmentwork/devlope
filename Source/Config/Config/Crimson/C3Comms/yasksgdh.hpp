
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_YASKSGDH_HPP
	
#define	INCLUDE_YASKSGDH_HPP

class CYaskawaSGDHDeviceOptions;
class CYaskawaSGDHDriver;

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa SGDH Device Options
//

class CYaskawaSGDHDeviceOptions : CStdDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CYaskawaSGDHDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Config Data
		UINT m_Drop;
		UINT m_Ping;

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Yaskawa SGDH Series
//

class CYaskawaSGDHDriver : public CStdCommsDriver
{
	public:
		// Constructor
		CYaskawaSGDHDriver(void);

		// Binding Control
		UINT	GetBinding(void);
		void	GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS	GetDeviceConfig();

	protected:
		// Data

		// Implementation
		void	AddSpaces(void);
	};

// End of File

#endif
