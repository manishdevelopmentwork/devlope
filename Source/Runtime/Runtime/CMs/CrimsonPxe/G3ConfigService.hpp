
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Programming Link Support
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3ConfigService_HPP

#define	INCLUDE_G3ConfigService_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "G3Slave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Configuration Service
//

class CG3ConfigService : public ILinkService
{
public:
	// Constructor
	CG3ConfigService(ICrimsonPxe *pPxe);

	// Destructor
	~CG3ConfigService(void);

	// IUnknown
	HRM QueryInterface(REFIID riid, void **ppObject);
	ULM AddRef(void);
	ULM Release(void);

	// ILinkService
	void Timeout(void);
	UINT Process(CG3LinkFrame &Req, CG3LinkFrame &Rep, ILinkTransport *pTrans);
	void EndLink(CG3LinkFrame &Req);

protected:
	// Data Members
	ULONG	         m_uRefs;
	ICrimsonPxe    * m_pPxe;
	IDatabase      * m_pDbase;
	IConfigStorage * m_pConfig;
	CItemInfo        m_Info;
	PBYTE	         m_pData;
	CString		 m_Cache;

	// Implementation
	UINT ReadIPConfig(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ClearConfig(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ClearGarbage(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckItem(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteItem(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteItemEx(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteData(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteVersion(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT HaltSystem(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT StartSystem(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT WriteTime(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ReadItem(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ReadData(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT FlashMount(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT FlashDismount(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT FlashVerify(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT FlashFormat(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckCompresion(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckControl(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT CheckEditFlags(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT ClearEditFlags(CG3LinkFrame &Req, CG3LinkFrame &Rep);
	UINT Identify(CG3LinkFrame &Req, CG3LinkFrame &Rep);

	// Item Helpers
	void FreeItem(void);
	void InitItem(UINT uItem, UINT uClass, UINT uSize, UINT uComp);
	BOOL WriteItem(void);
	BOOL IsDevCon(UINT uItem);
	BOOL GetConfig(char cTag, CString &Text);
	char GetTag(UINT uItem);
};

// End of File

#endif
