
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_HttpServerDigest_HPP

#define	INCLUDE_HttpServerDigest_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "HttpDigest.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Digest Helper
//

class DLLAPI CHttpServerDigest : public CHttpDigest
{
	public:
		// Constructor
		CHttpServerDigest(void);

		// Operations
		BOOL MatchNonce(CString Nonce);
		BOOL MatchOpaque(CString Opaque);
		UINT CheckRequest(CHttpAuth *p);
		void UpdateNonce(CString Opaque);

		// Data Members
		CString		    m_User;
		CString		    m_Digest;
		DWORD		    m_dwUsed[8];
		CHttpServerDigest * m_pNext;
		CHttpServerDigest * m_pPrev;
	};

// End of File

#endif
