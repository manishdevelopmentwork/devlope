
#include "intern.hpp"

#include "BaseTimeClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Cell Time Client
//

// Constructor

CBaseTimeClient::CBaseTimeClient(CJsonConfig *pJson)
{
	ApplyConfig(pJson);

	StdSetRef();
}

// Destructor

CBaseTimeClient::~CBaseTimeClient(void)
{
}

// IUnknown

HRESULT CBaseTimeClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IClientProcess);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CBaseTimeClient::AddRef(void)
{
	StdAddRef();
}

ULONG CBaseTimeClient::Release(void)
{
	StdRelease();
}

// IClientProcess

BOOL CBaseTimeClient::TaskInit(UINT uTask)
{
	SetThreadName(m_Name);

	if( m_uMode ) {

		return TRUE;
	}

	Release();

	return FALSE;
}

INT CBaseTimeClient::TaskExec(UINT uTask)
{
	time_t uWhen = time(NULL);

	UINT   uBack = 10;

	for( ;;) {

		time_t t = time(NULL);

		if( t >= uWhen ) {

			if( PerformSync() ) {

				uWhen = t + 60 * m_uPeriod;

				uBack = 5;
			}
			else {
				uWhen = t + uBack;

				if( uBack < 640 ) {

					uBack *= 2;
				}
			}
		}

		Sleep(5000);
	}

	return 0;
}

void CBaseTimeClient::TaskStop(UINT uTask)
{
}

void CBaseTimeClient::TaskTerm(UINT uTask)
{
	Release();
}

// Implementation

void CBaseTimeClient::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

		m_uMode   = pJson->GetValueAsUInt("mode", 1, 0, 2);

		m_uPeriod = pJson->GetValueAsUInt("period", 720, 60, 2880);
	}
}

BOOL CBaseTimeClient::SyncTime(CTimeSourceInfo const &Info)
{
	if( Info.m_fValid ) {

		if( Info.m_Remote && Info.m_Local ) {

			struct timeval tn, ts;

			gettimeofday(&tn, NULL);

			INT64 Now = (tn.tv_sec * INT64(1000)) + (tn.tv_usec / 1000);

			INT64 Set = Info.m_Remote + Now - Info.m_Local;

			if( Set >= 1577836800000LL ) {

				ts.tv_sec  = time_t(Set / 1000);

				ts.tv_usec = time_t(1000 * (Set % 1000));

				if( settimeofday(&ts, NULL) == 0 ) {

					struct tm *tm = gmtime(&ts.tv_sec);

					AfxTrace("%s: time set to %2.2u/%2.2u/%4.4u %2.2u:%2.2u:%2.2u.%3.3u\n",
						 PCTXT(m_Diag),
						 tm->tm_mon  + 1,
						 tm->tm_mday + 0,
						 tm->tm_year + 1900,
						 tm->tm_hour,
						 tm->tm_min,
						 tm->tm_sec,
						 ts.tv_usec / 1000
					);

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// End of File
