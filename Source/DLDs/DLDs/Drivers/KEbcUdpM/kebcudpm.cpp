
#include "intern.hpp"

#include "kebcudpm.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Koyo EBC UDP Master Driver
//

// Instantiator

INSTANTIATE(CKEbcUdpM);

// Constructor

CKEbcUdpM::CKEbcUdpM(void)
{
	m_Ident = DRIVER_ID;

	m_uKeep = 0;
	}

// Destructor

CKEbcUdpM::~CKEbcUdpM(void)
{
	}

// Configuration

void MCALL CKEbcUdpM::Load(LPCBYTE pData)
{
	}
	
// Management

void MCALL CKEbcUdpM::Attach(IPortObject *pPort)
{
	}

void MCALL CKEbcUdpM::Open(void)
{
	}

// Device

CCODE MCALL CKEbcUdpM::DeviceOpen(IDevice *pDevice)
{	
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_IP		= GetAddr(pData);
			m_pCtx->m_uPort		= GetWord(pData);
			m_pCtx->m_fKeep		= GetByte(pData);
			m_pCtx->m_fPing		= GetByte(pData);
			m_pCtx->m_uTime1	= GetWord(pData);
			m_pCtx->m_uTime2	= GetWord(pData);
			m_pCtx->m_uTime3	= GetWord(pData);
			m_pCtx->m_wTrans	= 0;
			m_pCtx->m_pSock		= NULL;
			m_pCtx->m_uLast		= GetTickCount();
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CKEbcUdpM::DeviceClose(BOOL fPersist)
{
	if( fPersist ) {

		if( !m_pCtx->m_fKeep || m_uKeep > 4 ) {

			CloseSocket(FALSE);
			}
		}
	else {
		CloseSocket(FALSE);

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CKEbcUdpM::Ping(void)
{
	if( !m_pCtx->m_fPing || CheckIP(m_pCtx->m_IP, m_pCtx->m_uTime2) < NOTHING ) {

		if( !OpenSocket() ) {

			return CCODE_ERROR;
			} 

		// TODO:  Store device info for justifications !!

		StartHeader();

		AddCommand(0x4);

		if( Transact() ) {

			if( m_bRxBuff[0] == 0 ) {

				return CCODE_SUCCESS;
				}
			}

		}

	return CCODE_ERROR;
	}

CCODE MCALL CKEbcUdpM::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( OpenSocket() ) {

		StartHeader();

		AddCommand(0x7);

	      	if( Transact() ) {
			      					
			UINT uRead = ParseData(Addr, pData, uCount);
			
			MakeMin(uCount, uRead);
			
			return uCount;
			} 
		}
		
	return CCODE_ERROR;
	}

CCODE MCALL CKEbcUdpM::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !IsWritable(Addr.a.m_Table) ) {

		return uCount;
		}

	PDWORD pRead  = new DWORD[uCount];
		
	UINT uRead = Read(Addr, pRead, uCount);
		
	if( uRead ) {

		UINT uSize	= m_uPtr;
			
		PBYTE  pBytes	= new BYTE[uSize];
			
		memcpy(pBytes, PBYTE(m_bRxBuff), uSize);
			
		MakeMin(uCount, uRead);
			
		SetData(Addr, pData, uCount, pBytes);
			
		if( OpenSocket() ) {
				
			StartHeader();
				
			AddCommand(0x6);
				
			memcpy(m_bTxBuff + m_uPtr, pBytes, uSize);
				
			m_uPtr += uSize;
				
			if( Transact() ) {

				delete pRead;

				delete pBytes;

				return uCount;
				}
			}

		delete pBytes;
		} 

	delete pRead;
	
	return CCODE_ERROR;
	}

// Socket Management

BOOL CKEbcUdpM::CheckSocket(void)
{
	if( m_pCtx->m_pSock ) {

		UINT Phase;

		m_pCtx->m_pSock->GetPhase(Phase);

		if( Phase == PHASE_ERROR ) {

			CloseSocket(TRUE);

			return FALSE;
			}

		if( Phase == PHASE_CLOSING ) {

			CloseSocket(FALSE);

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CKEbcUdpM::OpenSocket(void)
{	
	if( CheckSocket() ) {

		return TRUE;
		}

	if( !m_pCtx->m_fKeep ) {

		UINT dt = GetTickCount() - m_pCtx->m_uLast;

		UINT tt = ToTicks(m_pCtx->m_uTime3);

		if( dt < tt ) {

			UINT uWait = (100 / ToTicks(100)) * (tt - dt);

			Sleep(uWait);
			}
		}

	if( (m_pCtx->m_pSock = CreateSocket(IP_UDP)) ) {

		IPADDR const &IP   = (IPADDR const &) m_pCtx->m_IP;

		UINT         uPort = m_pCtx->m_uPort;

		m_pCtx->m_pSock->SetOption(OPT_RECV_QUEUE, sizeof(m_bRxBuff));

		if( m_pCtx->m_pSock->Connect(IP, 0x7070, uPort) == S_OK ) {

			m_uKeep++;

			SetTimer(m_pCtx->m_uTime1);

			while( GetTimer() ) {

				UINT Phase;

				m_pCtx->m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					return TRUE;
					}

				if( Phase == PHASE_CLOSING ) {

					m_pCtx->m_pSock->Close();
					}

				if( Phase == PHASE_ERROR ) {

					break;
					}

				Sleep(10);
				}

			CloseSocket(TRUE);

			return FALSE;
			}
		}

	return FALSE;
	}

void CKEbcUdpM::CloseSocket(BOOL fAbort)
{
	if( m_pCtx->m_pSock ) {

		if( fAbort )
			m_pCtx->m_pSock->Abort();
		else
			m_pCtx->m_pSock->Close();

		m_pCtx->m_pSock->Release();

		m_pCtx->m_pSock = NULL;

		m_pCtx->m_uLast = GetTickCount();

		m_uKeep--;
		}
	}

// Transport Layer

BOOL CKEbcUdpM::SendFrame(void)
{
	UINT uSize   = m_uPtr;

	if( m_pCtx->m_pSock->Send(m_bTxBuff, uSize) == S_OK ) {

		if( uSize == m_uPtr ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CKEbcUdpM::RecvFrame(void)
{
	m_uPtr   = 0;

	UINT uSize  = 0;

	UINT uTotal = 0;

	UINT uBytes = 0;

	SetTimer(m_pCtx->m_uTime2);

	while( GetTimer() ) {

		uSize = sizeof(m_bRxBuff) - m_uPtr;

		m_pCtx->m_pSock->Recv(m_bRxBuff + m_uPtr, uSize);

		if( uSize ) {

			m_uPtr += uSize;

			if( m_uPtr > 7 ) {

				uBytes = m_bRxBuff[7];

				uTotal = uBytes + 8;

				if( m_uPtr >= uTotal ) {

					if( m_bRxBuff[0] != 'H' || 
					    m_bRxBuff[1] != 'A' || 
					    m_bRxBuff[2] != 'P' ) {

						return FALSE;
						}

					WORD wTrans = MAKEWORD(m_bRxBuff[3], m_bRxBuff[4]);

					if( wTrans == m_pCtx->m_wTrans - 1 ) {

						memcpy(m_bRxBuff, m_bRxBuff + 11, uBytes); 

						m_uPtr -= 11;

						return TRUE;
						}
					}
				}

			continue;
			}

		if( !CheckSocket() ) {

			return FALSE;
			}

		Sleep(10);
		}

	return FALSE;
	}

BOOL CKEbcUdpM::Transact(void)
{
	SetByteCount();
	
	if( SendFrame() && RecvFrame() ) {

		return TRUE;
		}

	CloseSocket(TRUE);

	return FALSE;
	}

// Frame Building

void CKEbcUdpM::StartHeader(void)
{
	m_uPtr = 0;

	AddByte(0x48);

	AddByte(0x41);

	AddByte(0x50);
	
	AddWord(m_pCtx->m_wTrans++);

	AddWord(0x00);  

	AddByte(0x01);

	AddByte(0x00);
	}

void CKEbcUdpM::AddCommand(UINT uCmd)
{
	AddByte(uCmd);
	}

void CKEbcUdpM::AddByte(BYTE bData)
{
	m_bTxBuff[m_uPtr++] = bData;
	}

void CKEbcUdpM::AddWord(WORD wData)
{
	AddByte(LOBYTE(wData));

	AddByte(HIBYTE(wData));
	}

void CKEbcUdpM::AddLong(DWORD dwData)
{
	AddWord(LOWORD(dwData));

	AddWord(HIWORD(dwData));
	}

// Implementation

void CKEbcUdpM::FindSlot(UINT uSlot, UINT uTable)
{
	BOOL fDone = FALSE;

	UINT uVer  = 0;

	UINT uLen  = 0;

	UINT uCode = 0;

	UINT uReturn = 0;

	if( (m_bRxBuff[m_uPtr] & 0xF0) == 0xB0 ) {

		uVer = (m_bRxBuff[m_uPtr] & 0x0F);
	
		uLen = 2;
		}

	for( m_uPtr = uLen;  !fDone;	) {

		switch( uVer ) {

			case 0:	
				uCode = (m_bRxBuff[m_uPtr] & 0xF0) >> 4;

				uLen  = (m_bRxBuff[m_uPtr]) & 0x0F;

				m_uPtr++;

				break;

			case 1:						
				uCode = m_bRxBuff[m_uPtr];

				m_uPtr++;
		
				if( uCode != 0xFF ) {

					uLen = m_bRxBuff[m_uPtr];

					m_uPtr++;

					if( uLen == 0xFF ) {

						uLen = m_bRxBuff[m_uPtr] + 256 * m_bRxBuff[m_uPtr + 1];

						m_uPtr += 2;
						}
					} 
				
				break;
				}

		switch( uCode ) {

			case 0:	// PC_SLOT_NUMBER

				if( uSlot == uLen ) {

					for(	; m_uPtr < sizeof(m_bRxBuff); m_uPtr++ ) {

						uCode = (m_bRxBuff[m_uPtr] & 0xF0) >> 4;

						if( uCode == (uTable + 2) ) {

							break;
							}
						}
						 
					m_uPtr++; 

					fDone = TRUE;
					}

				break;

			case 1: // PC_MODULE_DEF

				m_uPtr += 2 + uLen;

				break;

			case 2:		// PC_MODULE_STATUS
			case 3:		// PC_BIT_IN_DATA
			case 4:		// PC_BIT_OUT_DATA
			case 0xA:	// PC_OFFSET_NUMBER
			case 0xD:	// PC_DELAY

				
				m_uPtr += uLen;

				break;

			case 5:  // PC_WORD_IN_DATA
			case 6:  // PC_WORD_OUT DATA

				if( uVer == 0 ) {

					uLen *= 2;
					}
				
				m_uPtr += uLen;

				break;

			case 8:	 // PC_DWORD_IN_DATA
			case 9:  // PC_DWORD_OUT DATA

				if( uVer == 0 ) {

					uLen *= 4;
					}

				m_uPtr += uLen;

				break;

			case 0xE:  // PC_EXT_FN_CODE

				m_uPtr += 2 + m_bRxBuff[m_uPtr + 1];

				break;

			case 0xF:  // PC_END_BLOCK

				if( uLen == 0xF ) {
			
					fDone = TRUE;
					}
			}
		}
	}

UINT CKEbcUdpM::ParseData(AREF Addr, PDWORD pData, UINT uCount)
{
	BOOL fDone = FALSE;

	UINT uPtr  = 0;

	UINT uVer  = 0;

	UINT uLen  = 0;

	UINT uCode = 0;

	UINT uSlot = 0;

	UINT uTarget = Addr.a.m_Extra;

	UINT uReturn = 0;

	if( (m_bRxBuff[uPtr] & 0xF0) == 0xB0 ) {

		uVer = (m_bRxBuff[uPtr] & 0x0F);
	
		uLen = 2;
		}

	for( uPtr = uLen;  !fDone;	) {

		switch( uVer ) {

			case 0:	
				uCode = (m_bRxBuff[uPtr] & 0xF0) >> 4;

				uLen  = (m_bRxBuff[uPtr]) & 0x0F;

				uPtr++;

				break;

			case 1:						
				// Version 1 has not been tested, so return 0;

				uReturn = 0; 

				fDone   = TRUE;

				continue;
				
				/*uCode = m_bRxBuff[uPtr];

				uPtr++;
		
				if( uCode != 0xFF ) {

					uLen = m_bRxBuff[uPtr];

					uPtr++;

					if( uLen == 0xFF ) {

						uLen = m_bRxBuff[uPtr] + 256 * m_bRxBuff[uPtr + 1];

						uPtr += 2;
						}
					}*/
				
				break;
				}

		switch( uCode ) {

			case 0:	// PC_SLOT_NUMBER

				uSlot = uLen;

				break;

			case 1: // PC_MODULE_DEF
				
				uPtr += 2 + uLen;

				break;

			case 2: // PC_MODULE_STATUS
			
				uPtr += uLen;

				break;

			case 3:	// PC_BIT_IN_DATA
			case 4: // PC_BIT_OUT_DATA

				if( uSlot == uTarget ) {

					if( uCode == UINT(Addr.a.m_Table + 2) ) {

						uReturn = GetDiscreteData(Addr.a.m_Offset, uPtr, pData, min(uLen, uCount));
				
						if( uReturn == uCount ) {

							fDone = TRUE;
							} 
						}					
					}

				uPtr += uLen;

				break;

			case 5:  // PC_WORD_IN_DATA
			case 6:  // PC_WORD_OUT DATA

				if( uSlot == uTarget ) {

					if( uCode == UINT(Addr.a.m_Table + 2) ) {
						
						uReturn = GetAnalogData(Addr.a.m_Offset, uPtr, pData, min(uLen, uCount));
										
						if( uReturn == uCount ) {

							fDone = TRUE;
							}
						}
					}

				if( uVer == 0 ) {

					uLen *= 2;
					}

				uPtr += uLen;

				break;

			case 8:	 // PC_DWORD_IN_DATA
			case 9:  // PC_DWORD_OUT DATA

				if( uSlot == uTarget ) {

					if( uCode == UINT(Addr.a.m_Table + 2) ) {

						uReturn = GetAnalogLongData(Addr.a.m_Offset, uPtr, pData, min(uLen, uCount));
						
						if( uReturn >= uCount ) {

							fDone = TRUE;
							}
						}
					}

				if( uVer == 0 ) {

					uLen *= 4;
					}

				uPtr += uLen;

				break;

			case 0xA:  // PC_OFFSET_NUMBER
			case 0xD:  // PC_DELAY

				uPtr += uLen;

				break;

			case 0xE:  // PC_EXT_FN_CODE

				uPtr += 2 + m_bRxBuff[uPtr + 1];

				break;

			case 0xF:  // PC_END_BLOCK

				if( uLen == 0xF ) {

					fDone = TRUE;
					}
			}
		}

	return uReturn;
	}

void CKEbcUdpM::SetData(AREF Addr, PDWORD pData, UINT uCount, PBYTE pBytes)
{
	FindSlot(Addr.a.m_Extra, Addr.a.m_Table);

	switch( Addr.a.m_Type ) {

		case addrBitAsBit:

			SetDiscreteData(Addr.a.m_Offset, pData, uCount, pBytes);

			break;

		case addrWordAsWord:

			SetAnalogData(Addr.a.m_Offset, pData, uCount, pBytes);

			break;

		case addrLongAsLong:	
			
			SetAnalogLongData(Addr.a.m_Offset, pData, uCount, pBytes);

			break;
		}
	}

void CKEbcUdpM::SetDiscreteData(UINT uOffset, PDWORD pData, UINT uCount, PBYTE pBytes)
{
	UINT b = 0;

	BYTE m = 1;

	UINT uBits = 8;

	while( uOffset + uCount > uBits ) {

		uBits += 8;
		}

	for( UINT u = 0, i = 0; u < uBits; u++ ) {

		if( u >= uOffset && u < uOffset + uCount ) {

			if( pData[i] ) {
					
				b |= m;
				}

			i++;
			}
		else {
			b |= pBytes[m_uPtr] & m;
			}

		if( !(m <<= 1) ) {

			pBytes[m_uPtr++] = b;

			b = 0;

			m = 1;
			}
		}

	if( m > 1 ) {

		pBytes[m_uPtr++] = b;
		} 
	}

void CKEbcUdpM::SetAnalogData(UINT uOffset, PDWORD pData, UINT uCount, PBYTE pBytes)
{
	for( UINT u = 0, i = 0; i < uCount; u++, m_uPtr += 2 ) {

		if( u >= uOffset - 1 ) {

			WORD x = LOWORD(pData[i]);

			i++;

			PWORD(pBytes + m_uPtr)[0] = HostToIntel(x);
			}
		}
	}

void CKEbcUdpM::SetAnalogLongData(UINT uOffset, PDWORD pData, UINT uCount, PBYTE pBytes)
{
	for( UINT u = 0, i = 0; i < uCount; u++, m_uPtr += 4 ) {

		if( u >= uOffset - 1 ) {

			DWORD x = pData[i];

			i++;

			PDWORD(pBytes + m_uPtr)[0] = HostToIntel(x);
			}
		}
	}

UINT CKEbcUdpM::GetDiscreteData(UINT uOffset, UINT uPtr, PDWORD pData, UINT uCount)
{
	BYTE m = 1 << (uOffset % 8);

	uPtr += (uOffset / 8);

	for( UINT u = 0; u < uCount; u++ ) {

		pData[u] = (m_bRxBuff[uPtr] & m) ? 1 : 0;

		if( !(m <<= 1 ) ) {

			uPtr += 1;

			m = 1;
			}
	       	}

	return uCount;  
	}

UINT CKEbcUdpM::GetAnalogData(UINT uOffset, UINT uPtr, PDWORD pData, UINT uCount)
{
	uPtr = uPtr + ((uOffset - 1) * 2);

	for( UINT u = 0; u < uCount; u++, uPtr += 2 ) {

		WORD x = PU2(m_bRxBuff + uPtr)[0];

		pData[u] = IntelToHost(x);
		}

	return uCount;
	}

UINT CKEbcUdpM::GetAnalogLongData(UINT uOffset, UINT uPtr, PDWORD pData, UINT uCount)
{
	uPtr = uPtr + ((uOffset - 1) * 4);

	for( UINT u = 0; u < uCount; u++, uPtr += 4) {

		DWORD x = PU4(m_bRxBuff + uPtr)[0];
			
		pData[u] = IntelToHost(x);
		}

	return uCount;
	}

void CKEbcUdpM::SetByteCount(void)
{
	m_bTxBuff[7] = m_uPtr - 9;
	}

// Helpers

BOOL CKEbcUdpM::IsWritable(UINT uTable)
{
	switch( uTable ) {

		case 2:
		case 4:
		case 7:
			return TRUE;
		}

	return FALSE;
	}

// End of File
