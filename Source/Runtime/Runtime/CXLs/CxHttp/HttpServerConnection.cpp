
#include "Intern.hpp"

#include "HttpServerConnection.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Protocol Library
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "HttpServer.hpp"

#include "HttpServerRequest.hpp"

#include "HttpServerBasicAuth.hpp"

#include "HttpServerDigestAuth.hpp"

#include "HttpServerNtlmAuth.hpp"

#include "HttpServerSession.hpp"

#include "HttpServerManager.hpp"

#include "HttpStream.hpp"

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Connection Options
//

// Constructor

CHttpServerConnectionOptions::CHttpServerConnectionOptions(void)
{
	m_fCompReply = TRUE;

	m_uMaxKeep   = 2;

	m_AllowMask  = 0;

	m_AllowAddr  = 0;
}

// Initialization

void CHttpServerConnectionOptions::Load(PCBYTE &pData)
{
	CHttpConnectionOptions::Load(pData);

	m_fCompReply = GetByte(pData);

	m_uMaxKeep   = GetByte(pData);
}

//////////////////////////////////////////////////////////////////////////
//
// HTTP Server Connection
//

// Constructor

CHttpServerConnection::CHttpServerConnection(CHttpServerManager *pManager,
					     CHttpServer *pServer,
					     CHttpServerConnectionOptions const &Opts,
					     UINT uRePort,
					     BOOL fReTls)

	: CHttpConnection(Opts), m_Opts(Opts)
{
	m_pManager  = pManager;

	m_pServer   = pServer;

	m_uRePort   = uRePort;

	m_fReTls    = fReTls;

	m_uSize     = 65536;

	m_pData	    = PTXT(malloc(m_uSize));

	m_pSess     = NULL;

	m_fKeep     = TRUE;

	m_Remote    = IP_EMPTY;

	m_pReq      = NULL;

	m_pMethod   = NULL;

	m_uMethod   = 0;

	m_uAuth	    = authSame;

	m_pAuth     = NULL;
}

// Destructor

CHttpServerConnection::~CHttpServerConnection(void)
{
	delete m_pReq;
}

// Attributes

IPADDR CHttpServerConnection::GetRemote(void) const
{
	return m_Remote;
}

UINT CHttpServerConnection::GetAuthStatus(void) const
{
	return m_uAuth;
}

CString CHttpServerConnection::GetUser(void) const
{
	return m_User;
}

BOOL CHttpServerConnection::GetSession(CHttpServerSession * &pSess) const
{
	return (pSess = m_pSess) ? TRUE : FALSE;
}

// Operations

void CHttpServerConnection::BindAuth(CHttpServerAuth **pMethod, UINT uMethod)
{
	m_pMethod = pMethod;

	m_uMethod = uMethod;
}

void CHttpServerConnection::BindSession(CHttpServerSession *pSess)
{
	if( m_pSess ) {

		m_pSess->Release();
	}

	if( (m_pSess = pSess) ) {

		if( m_pSess->GetRefCount() >= m_Opts.m_uMaxKeep ) {

			m_fKeep = FALSE;
		}

		m_pSess->AddRef();
	}
}

UINT CHttpServerConnection::Recv(CHttpServerRequest * &pReq)
{
	if( CheckConnection() ) {

		UINT uRecv = httpRecvIdle;

		do {
			switch( RecvData(m_pReq, TRUE) ) {

				case httpRecvDone:

					m_pReq->SetRequestInfo(m_pData);

					ProcessAuth(m_pReq);

					ProcessKeep(m_pReq);

					pReq = m_pReq;

					return httpRecvDone;

				case httpRecvFail:

					Abort();

					return httpRecvMore;

				case httpRecvMore:

					uRecv = httpRecvMore;

					break;

				case httpRecvIdle:

					return uRecv;
			}

		} while( CheckConnection() );

		return uRecv;
	}

	return httpRecvNone;
}

BOOL CHttpServerConnection::Send400(void)
{
	return Send(400);
}

BOOL CHttpServerConnection::Send401(void)
{
	return Send(401);
}

BOOL CHttpServerConnection::Send403(void)
{
	return Send(403);
}

BOOL CHttpServerConnection::Send(UINT uStatus)
{
	m_pReq->SetStatus(uStatus);

	return Send();
}

BOOL CHttpServerConnection::Send(void)
{
	// TODO -- We could try various other approaches here, including chunking if
	// we're using HTTP/1.1 or later. This would be particularly useful for sending
	// compressed data, especially if we can get zlib to compress in chunks, too.

	// TODO -- Add "Date" header!!!

	UINT    uStatus  = m_pReq->GetStatus();

	UINT    uVersion = m_pReq->GetRequestVersion();

	CBytes  Body     = m_pReq->GetLocalBody();

	CString Comp     = m_pReq->GetRequestHeader("Accept-Encoding");

	CString Type	 = m_pReq->GetContentType();

	CString Force    = "";

	PCBYTE  pBody    = Body.GetPointer();

	UINT    uBody    = Body.GetCount();

	if( uVersion > 11 ) {

		uVersion = 11;
	}

	if( uStatus == 401 ) {

		m_fKeep = FALSE;
	}

	if( !uBody && Type.IsEmpty() ) {

		if( !NoBodyRequired(uStatus) ) {

			Force += "<html><body><h1>\r\n";

			Force += GetStatusText(uStatus);

			Force += ".\r\n</h1></body></html>\r\n";

			pBody  = PCBYTE(PCTXT(Force));

			uBody  = Force.GetLength();
		}

		Comp.Empty();
	}

	ClearData();

	AddData(CPrintf("HTTP/%u.%u %3.3u %s\r\n",
			uVersion / 10,
			uVersion % 10,
			uStatus,
			PCTXT(GetStatusText(uStatus))
	));

	AddAuthHeaders(uStatus);

	AddKeepHeaders();

	AddData(m_pReq->GetLocalHeader());

	if( uStatus < 400 || uStatus >= 500 ) {

		AddData(m_pReq->GetCookieHeader());
	}

	if( !Type.IsEmpty() ) {

		AddData("Content-Type: " + Type + "\r\n");
	}

	if( m_pReq->GetVerb() == "HEAD" ) {

		AddData(CPrintf("Content-Length: %u\r\n\r\n", uBody));

		if( SendData() ) {

			if( !m_fKeep ) {

				Close();
			}

			return TRUE;
		}

		return FALSE;
	}

	if( m_Opts.m_fCompReply && uBody >= 256 ) {

		while( !Comp.IsEmpty() ) {

			if( Comp.StripToken(',') == "gzip" ) {

				if( m_pManager->CompressType(Type) ) {

					UINT uInit = m_uData;

					AddData("Content-Encoding: gzip\r\n");

					AddData("Content-Length: 01234567\r\n\r\n");

					UINT uFrom = m_uData;

					if( AddGzip(pBody, uBody) ) {

						SPrintf(m_pData + uFrom - 12,
							"%-8u",
							m_uData - uFrom
						);

						m_pData[uFrom - 4] = '\r';

						return SendData();
					}

					if( uBody >= 256 ) {

						m_pManager->TypeDidNotCompress(Type);
					}

					m_uData = uInit;
				}

				break;
			}
		}
	}

	AddData(CPrintf("Content-Length: %u\r\n\r\n", uBody));

	AddData(pBody, uBody);

	if( SendData() ) {

		if( !m_fKeep ) {

			Close();
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CHttpServerConnection::CheckPass(CString Pass)
{
	if( m_pAuth && Pass.GetLength() ) {

		return m_pAuth->CheckPass(m_pReq, Pass);
	}

	return FALSE;
}

BOOL CHttpServerConnection::ClearSessionAuth(void)
{
	if( m_pAuth ) {

		m_pAuth->ClearSession(m_pSess->GetOpaque());

		return TRUE;
	}

	return FALSE;
}

void CHttpServerConnection::ClearKeepAlive(void)
{
	m_fKeep = FALSE;
}

void CHttpServerConnection::KillRequest(void)
{
	if( m_pStream ) {

		m_pStream->Release();

		m_pStream = NULL;
	}

	delete m_pReq;

	m_pReq = NULL;
}

// Overridables

BOOL CHttpServerConnection::OnAcceptPost(IHttpStreamWrite * &pStm, CHttpRequest *pReq, PCTXT pPath)
{
	if( !pPath ) {

		return TRUE;
	}

	return m_pServer->AcceptPost(pStm, this, (CHttpServerRequest *) pReq, pPath);
}

void CHttpServerConnection::OnFreeSocket(void)
{
	BindSession(NULL);

	(DWORD &) m_Remote = 0;

	ResetAuth();

	ResetKeep();
}

// Implementation

BOOL CHttpServerConnection::AddGzip(PCBYTE pData, UINT uData)
{
	if( FindZLib() ) {

		for( ;;) {

			UINT z = m_pZLib->Compress(PCBYTE(pData),
						   uData,
						   PBYTE(m_pData + m_uData),
						   m_uSize - m_uData - 1
			);

			if( z == NOTHING ) {

				GrowBuffer(2 * m_uSize);

				continue;
			}

			if( z ) {

				if( z < uData ) {

					m_uData += z;

					m_pData[m_uData] = 0;

					return TRUE;
				}
			}

			break;
		}
	}

	return FALSE;
}

BOOL CHttpServerConnection::SendData(void)
{
	KillRequest();

	if( !CHttpConnection::Send() ) {

		Abort();

		return FALSE;
	}

	return TRUE;
}

BOOL CHttpServerConnection::CheckConnection(void)
{
	if( m_pSock ) {

		UINT Phase;

		if( m_pSock->GetPhase(Phase) == S_OK ) {

			if( Phase == PHASE_OPEN ) {

				if( (DWORD &) m_Remote == 0 ) {

					WORD Port;

					m_pSock->GetRemote(m_Remote, Port);

					DWORD Peer = m_Remote.m_dw;

					DWORD Test = (Peer & m_Opts.m_AllowMask);

					if( Test != m_Opts.m_AllowAddr ) {

						Abort();

						return FALSE;
					}
				}

				if( !m_pReq ) {

					m_pReq = New CHttpServerRequest;

					RecvInit(m_pReq);
				}

				return TRUE;
			}

			if( Phase == PHASE_IDLE ) {

				return FALSE;
			}

			if( Phase == PHASE_OPENING ) {

				return FALSE;
			}
		}

		Abort();
	}
	else {
		if( m_uRePort ? m_fReTls : m_Opts.m_fTls ) {

			m_pSock = m_pManager->CreateTlsSocket();
		}
		else
			m_pSock = m_pManager->CreateStdSocket();

		if( m_pSock ) {

			m_pSock->SetOption(OPT_SEND_QUEUE, 255);

			m_pSock->SetOption(OPT_NAGLE, 0);

			m_pSock->Listen(WORD(m_uRePort ? m_uRePort : m_Opts.m_uPort));

			ResetAuth();

			ResetKeep();
		}
	}

	return FALSE;
}

void CHttpServerConnection::ResetAuth(void)
{
	m_uAuth = authSame;

	m_pAuth = NULL;

	m_User.Empty();
}

BOOL CHttpServerConnection::AddAuthHeaders(UINT uStatus)
{
	if( uStatus == 401 ) {

		CString Opaque = m_pSess->GetOpaque();

		if( m_uAuth == authStale ) {

			AddData(m_pAuth->GetAuthHeader(Opaque, TRUE));
		}
		else {
			if( m_pAuth ) {

				AddData(m_pAuth->GetAuthHeader(Opaque, FALSE));
			}
			else {
				for( UINT n = 0; n < m_uMethod; n++ ) {

					if( m_pMethod[n] ) {

						AddData(m_pMethod[n]->GetAuthHeader(Opaque, FALSE));
					}
				}
			}
		}

		return TRUE;
	}

	return FALSE;
}

BOOL CHttpServerConnection::ProcessAuth(CHttpServerRequest *pReq)
{
	CString Auth = m_pReq->GetRemoteHeader("Authorization");

	if( Auth.GetLength() ) {

		UINT    uAuth = NOTHING;

		CString Meth  = Auth.StripToken(' ');

		if( m_pAuth ) {

			if( m_pAuth->CanAccept(Meth) ) {

				uAuth = m_pAuth->ProcessRequest(pReq, Auth);
			}
		}
		else {
			for( UINT n = 0; n < m_uMethod; n++ ) {

				if( m_pMethod[n] ) {

					if( m_pMethod[n]->CanAccept(Meth) ) {

						m_pAuth = m_pMethod[n];

						uAuth   = m_pMethod[n]->ProcessRequest(pReq, Auth);

						break;
					}
				}
			}
		}

		switch( uAuth ) {

			case authOkay:

				if( m_User == m_pAuth->GetUser(pReq) ) {

					m_uAuth = authSame;
				}
				else {
					m_User  = m_pAuth->GetUser(pReq);

					m_uAuth = authNew;
				}

				return TRUE;

			case authFail:

				m_uAuth = authFail;

				return TRUE;

			case authStale:

				m_uAuth = authStale;

				return TRUE;
		}
	}

	if( m_pAuth ) {

		m_uAuth = authFail;
	}

	return FALSE;
}

void CHttpServerConnection::ResetKeep(void)
{
	m_fKeep = TRUE;
}

BOOL CHttpServerConnection::AddKeepHeaders(void)
{
	if( m_fKeep ) {

		AddData("Connection: keep-alive\r\n");

		return TRUE;
	}

	AddData("Connection: close\r\n");

	return FALSE;
}

BOOL CHttpServerConnection::ProcessKeep(CHttpServerRequest *pReq)
{
	CString Keep = m_pReq->GetRemoteHeader("Connection");

	if( Keep.GetLength() ) {

		if( Keep == "close" ) {

			m_fKeep = FALSE;

			return FALSE;
		}
	}

	return TRUE;
}

BOOL CHttpServerConnection::NoBodyRequired(UINT uStatus)
{
	if( uStatus / 100 == 1 ) {

		return TRUE;
	}

	if( uStatus == 204 ) {

		return TRUE;
	}

	if( uStatus == 304 ) {

		return TRUE;
	}

	return FALSE;
}

CString CHttpServerConnection::GetStatusText(UINT uStatus)
{
	switch( uStatus ) {

		case 100: return "Continue";
		case 101: return "Switching Protocols";
		case 200: return "OK";
		case 201: return "Created";
		case 202: return "Accepted";
		case 203: return "Non-Authoritative Information";
		case 204: return "No Content";
		case 205: return "Reset Content";
		case 206: return "Partial Content";
		case 300: return "Multiple Choices";
		case 301: return "Moved Permanently";
		case 302: return "Found";
		case 303: return "See Other";
		case 304: return "Not Modified";
		case 305: return "Use Proxy";
		case 306: return "Unused";
		case 307: return "Temporary Redirect";
		case 400: return "Bad Request";
		case 401: return "Unauthorized";
		case 402: return "Payment Required";
		case 403: return "Forbidden";
		case 404: return "Not Found";
		case 405: return "Method Not Allowed";
		case 406: return "Not Acceptable";
		case 407: return "Proxy Authentication Required";
		case 408: return "Request Timeout";
		case 409: return "Conflict";
		case 410: return "Gone";
		case 411: return "Length Required";
		case 412: return "Precondition Failed";
		case 413: return "Request Entity Too Large";
		case 414: return "Request-URL Too Long";
		case 415: return "Unsupported Media Type";
		case 417: return "Expectation Failed";
		case 500: return "Internal Server Error";
		case 501: return "Not Implemented";
		case 502: return "Bad Gateway";
		case 503: return "Service Unavailable";
		case 504: return "Gateway Timeout";
		case 505: return "HTTP Version Not Supported";
	}

	return "(NO TEXT AVAILABLE)";
}

// End of File
