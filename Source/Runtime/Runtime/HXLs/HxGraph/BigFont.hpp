
// This file does not include intern.hpp
//
// See the notes in the font data files!
//

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_BigFont_HPP
	
#define	INCLUDE_BigFont_HPP

//////////////////////////////////////////////////////////////////////////
//
// Big Numeric Font
//

class CBigFont : public IGdiFont
{
	public:
		// Constructor
		CBigFont(int cy);

		// Destructor
		~CBigFont(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// Attributes
		BOOL IsProportional(void);
		int  GetBaseLine   (void);
		int  GetGlyphWidth (WORD c);
		int  GetGlyphHeight(WORD c);

		// Operations
		void InitBurst(IGdi *pGdi, CLogFont const &Font);
		void DrawGlyph(IGdi *pGdi, int &x, int &y, WORD c);
		void BurstDone(IGdi *pGdi, CLogFont const &Font);

	protected:
		// Font 0 Data
		static BYTE const m_bFontData0[];
		static WORD const m_wFontCode0[];
		static BYTE const m_xFontSize0[];
		static WORD const m_wFontFind0[];
		static UINT const m_uCount0;
		
		// Font 1 Data
		static BYTE const m_bFontData1[];
		static WORD const m_wFontCode1[];
		static BYTE const m_xFontSize1[];
		static WORD const m_wFontFind1[];
		static UINT const m_uCount1;
		
		// Font 2 Data
		static BYTE const m_bFontData2[];
		static WORD const m_wFontCode2[];
		static BYTE const m_xFontSize2[];
		static WORD const m_wFontFind2[];
		static UINT const m_uCount2;
		
		// Font 3 Data
		static BYTE const m_bFontData3[];
		static WORD const m_wFontCode3[];
		static BYTE const m_xFontSize3[];
		static WORD const m_wFontFind3[];
		static UINT const m_uCount3;

		// Data Members
		int    m_ySize;
		PCBYTE m_pData;
		PCWORD m_pCode;
		PCBYTE m_pSize;
		PCWORD m_pFind;
		UINT   m_uCount;
		int    m_xSpace;
		int    m_xDigit;
		BOOL   m_fFill;
		COLOR  m_Fore;
		COLOR  m_Back;

		// Implementation
		BOOL IsFixed(WORD c);
		BOOL IsFixedDigit(WORD c);
		BOOL IsFixedLetter(WORD c);
		BOOL IsSpace(WORD c);

		// Sort Function
		static int SortFunc(PCVOID p1, PCVOID p2);
	};

// End of File

#endif
