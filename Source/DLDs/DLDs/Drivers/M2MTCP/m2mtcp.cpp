
#include "intern.hpp"

#include "m2mtcp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// M2M Data Corp Slave Driver
//

// Instantiator

INSTANTIATE(CM2MDataTCP);

// Constructor

CM2MDataTCP::CM2MDataTCP(void)
{
	m_Ident		= DRIVER_ID;

	m_ServerIP	= 0;

	m_uListenPort   = 9000;

	m_uSendPort	= 9500;

	m_uCount	= 2;

	m_pFCB		= &m_FCBLK;

	memset(m_pFCB, 0, sizeof(m_FCBLK));

	memset(m_pFCB, 0, sizeof(m_FCBLK));

	m_pASID		= NULL;
	m_uASIDCt	= 0;
	m_uASIDNext	= 0;

	m_pRBEItem	= NULL;
	m_pRBETime	= NULL;
	m_uRBECt	= 0;
	m_uRBENext	= 0;
	}

// Destructor

CM2MDataTCP::~CM2MDataTCP(void)
{
	delete [] m_pSock;

	DeleteStacks();
	}

// Configuration

void MCALL CM2MDataTCP::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_ServerIP	= GetAddr(pData);

		m_uListenPort	= GetWord(pData);

		m_uSendPort	= GetWord(pData);

		m_LDEMCt	= GetByte(pData); // number of configured strings

		m_LDEMCt = min(m_LDEMCt, MAXIDQ);

		ClearLoadArrays();

		for( UINT i = 0; i < m_LDEMCt; i++ ) {

			m_pbStrNum[i] = GetByte(pData);	// high 5 bits of Addr.a.m_Offset
			m_pbIDNums[i] = GetByte(pData);	// ID number reference

			WORD wCount   = GetWord(pData);	// Number of type entries for ID

			m_pwIDCnt[i]  = wCount;

			AllocTypeBuffer(i, wCount);

			if( m_pbIDNums[i] == FCBDEM ) {

				InitFCB(wCount);
				}

			if( m_pbIDType[i] ) {

				for( UINT j = 0; j < wCount; j++ ) {

					m_pbIDType[i][j] = GetByte(pData); // Type list items
					}
				}
			}
		}

	m_pSock    = new SOCKET [ m_uCount ];

	for( UINT k = 0; k < m_uCount; k++ ) {

		m_pSock[k].m_pSocket = NULL;
		}

	m_pSock->m_uPtr = 0;

	m_bCtr		= 1;

	m_dTZOffset	= 0;

	m_uRcvLen	= 0;

	m_dPollTime	= 0;

	m_fWaitAck	= FALSE;

	m_fRTUAccess1	= FALSE;

	m_uRTUState	= TSCHE;

	m_fMaster	= FALSE;

	m_fReadDataNotDone = FALSE;

	memset(m_dOldTrig, 0, sizeof(m_dOldTrig));
	memset(m_fTrigDis, 0, sizeof(m_fTrigDis));
	memset(m_dError,   0, sizeof(m_dError));
	}

// Management
void MCALL CM2MDataTCP::Attach(IPortObject *pPort)
{
	SOCKET &Sock = m_pSock[0];

	OpenSocket(0, Sock);

	Sock = m_pSock[1];

	OpenSocket(1, Sock);

	CloseMaster(Sock);
	}

void MCALL CM2MDataTCP::Detach(void)
{
	if( m_LDEMCt ) {

		DeleteTypeBuffers();
		}

	DeleteStacks();

	BOOL fHit = FALSE;

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			SOCKET &Sock = m_pSock[n];

			if( Sock.m_pSocket ) {

				if( !p ) {

					Sock.m_pSocket->Close();

					fHit = TRUE;
					}
				else {
					Sock.m_pSocket->Abort();

					Sock.m_pSocket->Release();
					
					Sock.m_pSocket = NULL;
					}
				}

			if( !p && !fHit ) {

				break;
				}

			Sleep(100);
			}
		}

	CSlaveDriver::Detach();
	}

// Implementation

void MCALL CM2MDataTCP::Service(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &SockR	= m_pSock[0];
		SOCKET &SockT	= m_pSock[1];

		if( n ) {

			if( SockT.m_pSocket ) {

				UINT Phase = HandlePhase(SockT);

				switch( Phase ) {

					case PHASE_OPEN: // Sending SCH/RBE/FIL

//**/						if( m_uCheckPhase != 'P' ) { m_uCheckPhase = 'P'; AfxTrace1("\r\nP %x", m_fMaster);}

						SockT.m_uTime = GetTickCount();

						break;

					case PHASE_CLOSING:

//**/						if( m_uCheckPhase != 'C' ) { m_uCheckPhase = 'C'; AfxTrace0("\r\nC");}

						CloseSocket(SockT);

						break;

					case PHASE_ERROR:

//**/						if( m_uCheckPhase != 'E' ) { m_uCheckPhase = 'E'; AfxTrace1("\r\nE %x", m_fMaster);}

						if( !CheckAccess(SockT) ) {

							CloseSocket(SockT);

							OpenSocket(1, SockT);
							}

						break;

					default:
						break;
					}
				}
			}

		else {
			if( SockR.m_pSocket ) {

				UINT Phase = HandlePhase(SockR);

				switch( Phase ) {

					case PHASE_OPEN:

//**/						if( m_uCheckPhase != 'P' ) { m_uCheckPhase = 'P'; AfxTrace1("\r\nP %x", m_fMaster);}

						if( !SockR.m_fBusy ) {

							if( !CheckAccess(SockR) ) {

								SockR.m_pSocket->Close();

								break;
								}

							SockR.m_uTime = GetTickCount();

							SockR.m_fBusy = TRUE;
							}

						ReadData(SockR);

						if( !m_fReadDataNotDone ) {

							DoRTUAccess(); // Check RTU
							}

						break;

					case PHASE_CLOSING:

//**/						if( m_uCheckPhase != 'C' ) { m_uCheckPhase = 'C'; AfxTrace0("\r\nC");}

						SockR.m_pSocket->Close();

						break;

					case PHASE_ERROR:

//**/						if( m_uCheckPhase != 'E' ) { m_uCheckPhase = 'E'; AfxTrace1("\r\nE %x", m_fMaster);}

						CloseSocket(SockR);

						OpenSocket(0, SockR);

						break;

					case 0:
						DWORD dTime;

						dTime = GetTickCount();

						if( !SockR.m_fBusy ) {

							if( dTime - m_dPollTime >= ToTicks(500) ) {

								if( DoRTUAccess() ) {

									m_dPollTime = dTime;
									}
								}
							}

						SockR.m_uTime = dTime;

						break;

					default:
						break;
					}
				}

			else {
//**/				AfxTrace0("\r\nService - No Socket ");

				if( m_fMaster ) OpenSocket(1, SockT);

				else OpenSocket(0, SockR);
				}
			}
		}

	ForceSleep(20);
	}

void CM2MDataTCP::DoServerAccess(SOCKET &Sock)
{
	UINT uLen = m_uRcvLen;

	m_uRcvLen = 0;

	PBYTE p   = Sock.m_pData;

	UINT uCmd = CheckRecv(p, uLen);

	switch( uCmd ) {

		case NOTM2M: return;

		case BADFRM: // invalid frame construction
			DoAckNack(Sock, p[PCTR], FALSE);
			return;

		case IDACK:
//**/			AfxTrace1("*A/N %X **", Sock.m_pData[PACKNAK]);

			if( Sock.m_pData[PACKNAK] == ISNAK ) {
 
				Sock.m_fBusy = FALSE;
				}
			return;

		case IDDEM: // Req from Server for RTU Data
			DoDEMRead(Sock, uLen);
			return;

		case IDCTL: // Req from Server to write RTU Data
			DoCTLWrite(Sock, uLen);
			return;

		default:
			if( IsFILID(uCmd) ) { // Req from Server to write RTU Data

				DoFILWrite(Sock, uLen);
				}
			return;
		}
	}

BOOL CM2MDataTCP::DoRTUAccess(void)
{
	if( !m_fRTUAccess1 ) {

		m_fRTUAccess1 = TRUE;

		DoTimeZone();

		return TRUE;
		}

	DoErrors();

	SOCKET &Sock  = m_pSock[1];

	if( m_pFCB->fBusy ) { // continue File data transfer

		return DoFILRead(Sock);
		}

	m_uRTUState = m_uRTUState == TSCHE ? TRBEE : TSCHE;

	m_fWaitAck  = FALSE;

	if( m_uRTUState == TSCHE ) {

		if( DoSCHAccess(Sock) ) {

			return TRUE;
			}
		}

	if( m_uRTUState == TRBEE ) {

		if( DoRBEAccess(Sock) ) {

			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CM2MDataTCP::GetRTUFlag(UINT uTable, PDWORD pData, UINT uCount)
{
	CAddress Addr;

	Addr.a.m_Table	= uTable;
	Addr.a.m_Offset	= 0;
	Addr.a.m_Type	= LL;
	Addr.a.m_Extra	= 0;

	if( COMMS_SUCCESS(Read(Addr, pData, uCount)) ) {

		return (BOOL)pData[0];
		}

	return FALSE;
	}

void CM2MDataTCP::SetASIDFlag(BYTE bOldItem, BOOL fOld, PDWORD pData)
{
	if( GetRTUFlag(TSCHE, pData, 1) ) {

		BYTE b = LOBYTE(pData[RTUID]);

		if( !fOld || b != bOldItem ) {	// new entry to be done

			AppendToASID(b);
			}
		}
	}

void CM2MDataTCP::SetRBEFlag(UINT uOldItem, BOOL fOld, PDWORD pData)
{
	if( GetRTUFlag(TRBEE, pData, 2) ) {

		DWORD d = pData[RTUTRIG];

		if( (BOOL)d && GetTimeStamp() >= d ) {

			if( !fOld || (UINT)pData[RTUID] != uOldItem ) {	// new entry to be done

				AppendToRBE((UINT)pData[RTUID], (UINT)d);
				}
			}
		}
	}

// Schedule Read Access
BOOL CM2MDataTCP::DoSCHAccess(SOCKET &Sock)
{
	DWORD Data[2] = {0};

	SetASIDFlag(0, FALSE, Data);

	if( (BOOL)m_uASIDCt ) {

		BYTE bItem  = m_pASID[m_uASIDNext];

		if( !FindBlockID(bItem) ) { // ID not found

			TakeFromASID(bItem);

			ResetCommand(TSCHE, 0, 0);

			return FALSE;
			}

		Data[0]     = (DWORD)bItem;

		DWORD dTime = GetTimeStamp();

		if( VerifyRTUAccess(TSCHE, dTime) ) {

			Data[RTUTRIG] = dTime;
			
			if( OpenCommandPort(Sock) ) {

				TakeFromASID(bItem);

				if( !DoSCHRead(Sock, Data) ) {

					m_fWaitAck = FALSE;

					m_uASIDNext = (m_uASIDNext + 1) % m_uASIDCt;
					}

				else {
					m_uASIDNext = 0;

					SetASIDFlag(bItem, TRUE, Data);	// check for new ASID
					}

				FinishRTUAccess(Sock, TSCHE, dTime);

				return TRUE;
				}
			}

		SetASIDFlag(bItem, TRUE, Data);	// check for new ASID
		}

	return FALSE;
	}

BOOL CM2MDataTCP::DoRBEAccess(SOCKET &Sock)
{
	DWORD Data[2];

	SetRBEFlag(0, FALSE, Data);

	if( (BOOL)m_uRBECt ) {

		UINT uItem	= m_pRBEItem[m_uRBENext];
		DWORD dTime	= m_pRBETime[m_uRBENext];

		Data[RTUID]	= DWORD(uItem);
		Data[RTUTRIG]	= dTime;

		if( (BOOL)dTime ) {

//**/			AfxTrace2("\r\nDo RBE Access %d %8.8lx ", Data[0], Data[1]);

			if( VerifyRTUAccess(TRBEE, dTime) ) {

				if( !DoRBERead(Sock, Data) ) {

					m_fWaitAck = FALSE;

					m_uRBENext = (m_uRBENext + 1) % m_uRBECt;
					}

				else {
					TakeFromRBE(uItem);

					if( m_uRBENext > m_uRBECt - 1 ) {

						m_uRBENext = 0;
						}

					SetRBEFlag(uItem, TRUE, Data);	// check for new RBE
					}

				FinishRTUAccess(Sock, TRBEE, dTime);

				return TRUE;
				}
			}

		SetRBEFlag(uItem, TRUE, Data);	// check for new RBE
		}

	else {
		if( (BOOL)Data[RTUTRIG] ) {

			ResetCommand(TRBEE, 0, 0);
			}
		}

	return FALSE;
	}

// Close Master Access
void CM2MDataTCP::FinishRTUAccess(SOCKET &Sock, UINT uType, DWORD dTime)
{
	ResetCommand(uType, 0L, dTime);

	if( m_fWaitAck ) {

		WaitForAck(Sock); // function sent

		SetTimer(1000);

		while( GetTimer() ) {

			UINT phase = HandlePhase(Sock);

			if( phase != PHASE_OPEN ) {

				break;
				}

			Sleep(20);
			}
		}

	m_fWaitAck = FALSE;

	CloseMaster(Sock);
	}

// Opcode Handlers
// xxxRead  - Get data from RTU, send to Server
// xxxWrite - Get data from Server, send to RTU

// DEMAND POLL
void CM2MDataTCP::DoDEMRead(SOCKET &Sock, UINT uLen)
{
//**/	AfxTrace1("\r\nDoDEMRead ID=%d ", Sock.m_pData[PDAT]);

	PBYTE p = Sock.m_pData;

	BYTE bID = p[PDAT];

	BOOL fF  = FALSE;

	if( IsFILID(bID) ) {

		if( !m_pFCB->FCBSize ) {

			DoAckNack(Sock, p[PCTR], FALSE);

			return;
			}

		fF = SetFileTrigger(0, FALSE, TRUE); // clear triggers

		if( !FindBlockID(FCBDEM) ) {

			DoAckNack(Sock, p[PCTR], FALSE);

			return;
			}
		}

	else {
		if( !FindBlockID(bID) ) {

			DoAckNack(Sock, p[PCTR], FALSE);

			return;
			}
		}

	DEMHEAD OH;

	m_pHeadD         = &OH;

	m_pHeadD->fIsDEM = TRUE;

	m_pHeadD->bID    = bID;

	m_pHeadD->bCtr   = p[PCTR];

	m_pHeadD->dTime  = m_dLocalTime;

	if( MakeDEMSendBuffer(Sock, m_pHeadD) ) {

		m_fWaitAck = FALSE;

		if( fF ) {

			if( SetFileTrigger(bID, FALSE, FALSE) ) { // enable ok to send file data

				m_pFCB->pDAT->bChunkNo	= 0;
				m_pFCB->pDAT->bChunkQty	= m_pFCB->FSize / MAXCHK;
				m_pFCB->pDAT->wSent	= 0;
				m_pFCB->fBusy		= TRUE;
				m_pFCB->UFI++;
				}
			}

		return;
		}

	DoAckNack(Sock, p[PCTR], FALSE);
	}

 // SCHEDULED DATA
BOOL CM2MDataTCP::DoSCHRead(SOCKET &Sock, PDWORD pData)
{
//**/	AfxTrace2("\r\nSCH Write D0=%d D1=%8.8lx ", pData[RTUID], pData[RTUTRIG]);

	DEMHEAD OH;

	m_pHeadD = &OH;

	m_pHeadD->bID  = LOBYTE(pData[RTUID]); // Item to access

	m_pHeadD->bCtr   = m_bCtr;

	m_pHeadD->fIsDEM = FALSE;

	DWORD dTime      = pData[RTUTRIG];

	if( !CheckTimeVal(&dTime) ) return TRUE;

	m_pHeadD->dTime  = dTime;

	m_fWaitAck	 = MakeDEMSendBuffer(Sock, m_pHeadD);

	return TRUE;
	}

// Send DEM/SCH
BOOL CM2MDataTCP::MakeDEMSendBuffer(SOCKET &Sock, DEMHEAD * pHead)
{
//**/	AfxTrace1("\r\nDemSendBuff %d ", pHead->bID);

	BYTE bID = pHead->bID;

	if( IsFILID(bID) ) {

		if( InitiateFileTransfer(pHead) ) {

			DoAckNack(Sock, pHead->bCtr, TRUE);

			return DoFILRead(Sock);
			}

		return FALSE;
		}

	PBYTE  pdList	 = NULL;

	BOOL   fIsDEM	 = pHead->fIsDEM;

	UINT   uSize	 = fIsDEM ? 14 : 9; // Ack included for demand poll
	UINT   uListSize = 0;

	BOOL   fOk	 = FALSE;

	UINT uQty	 = m_pwIDCnt[m_uListSel]; // number of data types for ID

	if( uQty ) {

		PDWORD pResp = new DWORD [uQty]; // allocate read data size

		if( pResp ) {

			UINT n = GetBlockData(pResp, uQty, fIsDEM); // get read data

			if( n == uQty ) {

				PBYTE pD  = new BYTE [n * sizeof(DWORD)]; // max number of data bytes needed

				if( pD ) {

					uListSize = GetIDListData(pD, pResp, n); // number of bytes actually used

					if( uListSize ) {

						fOk = TRUE; // has data

						if( fOk ) {

							if( uSize + uListSize < sizeof(m_bTx) ) {

								pdList = new BYTE [uListSize];

								if( pdList ) {

									memcpy(pdList, pD, uListSize);
									}

								else {
									InsertRunTimeError(ENOMEM);
									fOk = FALSE;
									}
								}

							else {
								InsertRunTimeError(ENOMEM);
								fOk = FALSE;
								}
							}
						}

					else {
						InsertRunTimeError(EBADID);
						}

					delete pD;
					}

				else {
					InsertRunTimeError(ENOMEM);
					}
				}

			else {
				m_uPtr = 0;

				if( fIsDEM ) {

					MakeAck(pHead->bCtr, ISACK);
					}

				SendInvalidStatus(Sock, pHead->bID, pHead->bCtr, pHead->dTime); 

				InsertRunTimeError(EBADID);

				delete pResp;

				return TRUE; // skip sending additional NAK
				}
			}

		delete pResp;
		}

	else {
		InsertRunTimeError(EBADID);
		}

	m_uPtr = 0;

	if( fIsDEM ) {

		MakeAck(pHead->bCtr, fOk ? ISACK : ISNAK);
		}

	if( fOk ) {

		AddDEMHeader(pHead);

		memcpy(&m_bTx[m_uPtr], pdList, uListSize);

		m_uPtr += uListSize;

		AddByte(MEND);
		}

	delete pdList;

	if( fOk && SendDEMSendBuffer(Sock) ) {

		m_bCtr++;

		return TRUE;
		}

	return FALSE;
	}

BOOL CM2MDataTCP::SendDEMSendBuffer(SOCKET &Sock)
{
	return ExecuteSend(Sock);
	}

// CONTROL
void CM2MDataTCP::DoCTLWrite(SOCKET &Sock, UINT uLen)
{
	PBYTE p = Sock.m_pData;

	CTLINFO IC;

	m_pInfoC = &IC;

	m_pInfoC->fIsCTL = TRUE;

	m_pInfoC->bCtr = p[PCTR];

	p         += PLEN;

	WORD wLen  = MotorToHost((WORD)*(PU2)p);

	p         += 2;

	UINT uQty    = (wLen - 6) / 4; // number of 16 bit addresses, and 16 bit data words.

	UINT uItemCt = 0;

	if( uQty ) {

		PWORD pAddr = new WORD [uQty];
		PWORD pData = new WORD [uQty];

		if( !(pAddr && pData) ) {

			if( pAddr ) delete pAddr;
			if( pData ) delete pData;

			InsertRunTimeError(ENOMEM);

			DoAckNack(Sock, Sock.m_pData[PCTR], FALSE);

			return;
			}

		CAddress Addr;
		DWORD Data[1];

		Addr.a.m_Table  = TCTLW;
		Addr.a.m_Type   = WW;
		Addr.a.m_Extra  = 0;

		BOOL fSuccess   = FALSE;

		for( UINT i = 0; i < uQty; i++, p += 4 ) {

			Addr.a.m_Offset	= MotorToHost((WORD)*(PU2)p);

			*Data		= (DWORD)MotorToHost((WORD)*(PU2)(p+2));

			if( COMMS_SUCCESS(Write(Addr, Data, 1)) ) {

				if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

					pAddr[uItemCt] = Addr.a.m_Offset;

					pData[uItemCt] = LOWORD(*Data);

					uItemCt++;

					fSuccess = TRUE;
					}
				}
			}

		if( fSuccess ) { // at least one successful W/R

			m_pInfoC->uSize = uItemCt;
			m_pInfoC->pAddr = pAddr;
			m_pInfoC->pData = pData;
			m_pInfoC->dTime = m_dLocalTime;

			DoRBEResp(Sock, TRUE, m_pInfoC);
			}

		else {
			MakeAck(m_pInfoC->bCtr, ISACK);

			SendInvalidStatus(Sock, IDCTL, m_pInfoC->bCtr, m_pInfoC->dTime);
			}

		m_fWaitAck = FALSE;

		delete pAddr;
		delete pData;

		return;
		}

	m_fWaitAck = FALSE;

	DoRBEResp(Sock, FALSE, m_pInfoC);
	}

// REPORT BY EXCEPTION
BOOL CM2MDataTCP::DoRBERead(SOCKET &Sock, PDWORD pData)
{
//**/	AfxTrace2("\r\nRBE Write %d %8.8lx ", pData[RTUID], pData[RTUTRIG] );

	CTLINFO IC;

	m_pInfoC	= &IC;

	CTLINFO *pCtl	= m_pInfoC;

	pCtl->fIsCTL	= FALSE;
	pCtl->bCtr	= m_bCtr;

	CAddress Addr;

	DWORD Data[1];

	Addr.a.m_Table	= TRBEW;
	Addr.a.m_Type	= WW;
	Addr.a.m_Extra	= 0;
	Addr.a.m_Offset	= LOWORD(pData[RTUID]);

	if( COMMS_SUCCESS(Read(Addr, Data, 1)) ) {

		if( !OpenCommandPort(Sock) ) {

			return FALSE;
			}

		WORD wAdd   = Addr.a.m_Offset;
		WORD wData  = LOWORD(*Data);

		pCtl->uSize = 1;
		pCtl->pAddr = &wAdd;
		pCtl->pData = &wData;

		DWORD dTime = pData[RTUTRIG];

		if( !CheckTimeVal(&dTime) ) return TRUE;

		m_pInfoC->dTime = dTime;

		m_fWaitAck   = DoRBEResp(Sock, TRUE, m_pInfoC) && !pCtl->fIsCTL;

		return TRUE;
		}

	InsertRunTimeError(EBADID);

	TakeFromRBE(Addr.a.m_Offset);

	m_fWaitAck = FALSE;

	m_uPtr = 0;

	SendInvalidStatus(Sock, (BYTE)IDRBE, pCtl->bCtr, pCtl->dTime);

	return TRUE;
	}

// Send CONTROL / REPORT BY EXCEPTION
BOOL CM2MDataTCP::DoRBEResp(SOCKET &Sock, BOOL fGoodData, CTLINFO * pCtl)
{
	WORD wLen = 5; // Ack Frame Size

	if( fGoodData ) {

		wLen = 10 + (4 * pCtl->uSize); // F0,ID,Ctr,LenH,LenL,TimeStamp(4),F1 + Data Size

		if( pCtl->fIsCTL ) wLen += 5; // Prefixing ACK frame to good data
		}

	BOOL fCtl = pCtl->fIsCTL;

	m_uPtr    = 0;

	if( fCtl || !fGoodData ) { // Add, or use only, Ack Frame

		MakeAck(pCtl->bCtr, fGoodData ? ISACK : ISNAK);

		wLen  -= 5;
		}

	if( fGoodData ) { // Add Data if Access succeeded

		AddByte(MSTART);
		AddByte(IDRBE);
		AddByte(m_bCtr);
		AddByte(HIBYTE(wLen));
		AddByte(LOBYTE(wLen));

		MakeTimeStamp(pCtl->dTime);

		PWORD pAddr = pCtl->pAddr;
		PWORD pData = pCtl->pData;

		for( UINT i = 0; i < pCtl->uSize; i++ ) {

			AddByte(HIBYTE(pAddr[i]));
			AddByte(LOBYTE(pAddr[i]));
			AddByte(HIBYTE(pData[i]));
			AddByte(LOBYTE(pData[i]));
			}

		AddByte(MEND);
		}

	if( SendRBEResp(Sock) && fGoodData ) {

		m_bCtr++;

		return TRUE;
		}

	if( !fCtl ) { // retry if RBE

		if( SendRBEResp(Sock) && fGoodData ) {

			m_bCtr++;

//			return TRUE;
			}
		}

	return FALSE;
	}

BOOL CM2MDataTCP::SendRBEResp(SOCKET &Sock)
{
	return ExecuteSend(Sock);
	}

// FILE - Data From RTU to Server
BOOL CM2MDataTCP::DoFILRead(SOCKET &Sock)
{
	FCBLK *p = m_pFCB;

	if( !p->pDAT->wSent ) {

		if( !ToggleTrigger(p->bID) ) { // set AF0 to 0 then to ID

			return FALSE; // can't access triggers
			}
		}

	return ContinueFILRead(Sock);
	}

BOOL CM2MDataTCP::ContinueFILRead(SOCKET &Sock)
{
	if( !OpenCommandPort(Sock) ) {

		m_pFCB->fBusy = FALSE;

		return FALSE;
		}

	FCBLK *p = m_pFCB;

	BYTE bSize = GetFileTrigger();

	if( !bSize ) {

		if( p->uTryCt++ > 5 ) {

			ClearFileRead();
			}

		return FALSE;
		}

	UINT uRegCt  = (3 + bSize) / 4;

	PDWORD pData = new DWORD [uRegCt];

	BOOL fOk     = FALSE;

	if( (fOk = MakeFileReadFrame(pData, uRegCt)) ) {

		fOk = SendFILRead(Sock);

		if( fOk && m_fIsAck ) {

			m_bCtr++;

			p->pDAT->wSent += bSize;

			if( p->pDAT->wSent >= p->FSize ) { // done

				ClearFileRead();
				}

			else {
				SetFileTrigger(0, TRUE, FALSE); // reset count for next data read

				p->fBusy = TRUE;

				p->pDAT->bChunkNo++;
				}
			}

		else {
			ClearFileRead();
			}
		}

	delete pData;

	return fOk;
	}

BOOL CM2MDataTCP::SendFILRead(SOCKET &Sock)
{
	return ExecuteSend(Sock);
	}

// FILE - Request from Server for RTU file
void CM2MDataTCP::DoFILWrite(SOCKET &Sock, UINT uLen)
{
	}

BOOL CM2MDataTCP::SendFILWrite(SOCKET &Sock)
{
	return ExecuteSend(Sock);
	}

// ACK/NAK/STATUS
void CM2MDataTCP::DoAckNack(SOCKET &Sock, UINT uCtr, BOOL fIsAck)
{
	MakeAck(LOBYTE(uCtr), fIsAck ? ISACK : ISNAK);

	ExecuteSend(Sock);
	}

void CM2MDataTCP::SendInvalidStatus(SOCKET &Sock, BYTE bID, BYTE bCtr, DWORD dTime)
{
	AddByte(MSTART);
	AddByte(bID);
	AddByte(bCtr);
	AddByte((BYTE)STATN);

	MakeTimeStamp(dTime);

	AddByte(MEND);

	ExecuteSend(Sock);
	}

UINT CM2MDataTCP::WaitForAck(SOCKET &Sock)
{
	m_fWaitAck  = TRUE; // return from ReadData through this routine

	BOOL fOk    = FALSE;

	UINT uTimer = 3000;

	m_uRcvLen   = 0;

	SetTimer(uTimer);

	while( GetTimer() ) {

		m_fIsAck = FALSE;

		ReadData(Sock);

		if( m_uRcvLen ) { // full frame received

			return (UINT)Sock.m_pData[PACKNAK];
			}

		Sleep(20);
		}

	return 0;
	}

// ASID and RBE Stacks
void CM2MDataTCP::AppendToASID(BYTE bItem)
{
	UINT uCt = m_uASIDCt;

	if( ASIDIsDuplicated(bItem) == 0xFFF ) {

		PBYTE pTemp =(PBYTE)malloc(uCt);

		if( uCt ) {

			memcpy(pTemp, m_pASID, uCt);
			}

		delete m_pASID;

		m_pASID = new BYTE [uCt + 1];

		memcpy(m_pASID, pTemp, uCt);

		free(pTemp);

		m_pASID[uCt] = bItem;

		m_uASIDCt++;
		}
	}

void CM2MDataTCP::AppendToRBE(UINT uItem, UINT uTime)
{
	if( (BOOL)uTime ) {

		UINT uCt = m_uRBECt;

		if( RBEIsDuplicated(uItem) == 0xFFFFFFFF ) {

			UINT uSizeU = uCt * sizeof(UINT);
			UINT uSizeD = uCt * sizeof(DWORD);

			UINT   *pTempItem = (UINT *)malloc(uSizeU);
			PDWORD  pTempTime = (PDWORD)malloc(uSizeD);

			if( uCt ) {

				memcpy(pTempItem, m_pRBEItem, uSizeU);
				memcpy(pTempTime, m_pRBETime, uSizeD);
				}

			delete m_pRBEItem;
			delete m_pRBETime;

			m_pRBEItem = new UINT  [uSizeU + sizeof(UINT)];
			m_pRBETime = new DWORD [uSizeD + sizeof(DWORD)];

			memcpy(m_pRBEItem, pTempItem, uSizeU);
			memcpy(m_pRBETime, pTempTime, uSizeD);

			free(pTempItem);
			free(pTempTime);

			m_pRBEItem[uCt] = uItem;

			m_pRBETime[uCt] = uTime;

			m_uRBECt++;
			}
		}
	}

void CM2MDataTCP::TakeFromASID(BYTE bItem)
{
	UINT uCt = m_uASIDCt;

	if( uCt ) {

		UINT uRemove = ASIDIsDuplicated(bItem);

		if( uRemove < uCt ) {

			if( uCt > 1 ) {

				PBYTE pTemp = (PBYTE)malloc(uCt);

				memcpy(pTemp, m_pASID, uCt);

				delete m_pASID;

				m_pASID = new BYTE [uCt-1];

				for( UINT i = 0, j = 0; i < uCt; i++ ) {

					if( i != uRemove ) {

						m_pASID[j++] = pTemp[i];
						}
					}

				free(pTemp);
				}

			m_uASIDCt--;
			}
		}
	}

void CM2MDataTCP::TakeFromRBE(UINT uItem)
{
	UINT uCt = m_uRBECt;

	if( (BOOL)uCt ) {

		DWORD dRemove = RBEIsDuplicated(uItem);

		if( dRemove < uCt ) {

			if( uCt > 1 ) {

				UINT uSizeU = uCt * sizeof(UINT);
				UINT uSizeD = uCt * sizeof(DWORD);

				UINT  *pTempItem = (UINT *)malloc(uSizeU);
				PDWORD pTempTime = (PDWORD)malloc(uSizeD);

				memcpy(pTempItem, m_pRBEItem, uSizeU);
				memcpy(pTempTime, m_pRBETime, uSizeD);

				delete m_pRBEItem;
				delete m_pRBETime;

				m_pRBEItem = new UINT  [uSizeU - sizeof(UINT)];
				m_pRBETime = new DWORD [uSizeD - sizeof(DWORD)];

				for( UINT i = 0, j = 0; i < uCt; i++ ) {

					if( i != dRemove ) {

						m_pRBEItem[j]	= pTempItem[i];
						m_pRBETime[j++]	= pTempTime[i];
						}
					}

				free(pTempItem);
				free(pTempTime);
				}

			m_uRBECt--;
			}
		}
	}

UINT CM2MDataTCP::ASIDIsDuplicated(BYTE bItem)
{
	UINT uCt = m_uASIDCt;

	PBYTE p  = m_pASID;

	for( UINT i = 0; i < uCt; i++ ) {

		if( p[i] == bItem ) {

			return i;
			}
		}

	return 0xFFF;
	}

DWORD CM2MDataTCP::RBEIsDuplicated(UINT uItem)
{
	UINT uCt = m_uRBECt;

	UINT  *p = m_pRBEItem;

	for( UINT i = 0; i < uCt; i++ ) {

		if( p[i] == uItem ) {

			return (DWORD)i;
			}
		}

	return 0xFFFFFFFF;
	}

void CM2MDataTCP::DeleteStacks(void)
{
	if( m_uASIDCt ) {

		delete m_pASID;

		m_pASID   = NULL;

		m_uASIDCt = 0;
		}

	if( m_uRBECt ) {

		delete m_pRBEItem;
		delete m_pRBETime;

		m_pRBEItem = NULL;
		m_pRBETime = NULL;

		m_uRBECt   = 0;
		}
	}

// Check Receive
void CM2MDataTCP::ReadData(SOCKET &Sock)
{
	UINT uSize   = 300 - Sock.m_uPtr;

	Sock.m_pSocket->Recv(Sock.m_pData + Sock.m_uPtr, uSize);

	UINT uTotal  = Sock.m_uPtr;

	BOOL fAck    = FALSE;

	m_fIsAck     = FALSE;

	if( uSize ) {

		Sock.m_uPtr += uSize;

		uTotal      += uSize;

		m_fReadDataNotDone = TRUE;

//**/		AfxTrace2("\r\n\n**** ReadData T=%d Sz=%d\r\n", uTotal, uSize);
//**/		for( UINT k = 0; k < uTotal; k++ ) AfxTrace1("<%2.2x>", Sock.m_pData[k]);

		if( uTotal > 1 ) fAck = Sock.m_pData[1] == IDACK;

		if( (fAck && uTotal >= 5) || (uTotal > PLENL) ) {

			UINT uLen = fAck ? 5 : (Sock.m_pData[PLENH] << 8) + Sock.m_pData[PLENL];

			if( uTotal >= uLen ) {

				m_dLocalTime  = GetTimeStamp();

				UINT uRemain = uTotal - uLen;

				if( uRemain ) {

					memcpy(Sock.m_pData, &Sock.m_pData[uLen], uRemain);
					}

				Sock.m_uPtr  = uRemain;

				Sock.m_uTime = GetTickCount();

				m_fIsAck     = fAck;

				m_uRcvLen    = uLen;

				if( !m_fWaitAck ) DoServerAccess(Sock);

				m_fReadDataNotDone = FALSE;

				return;
				}
			}

		return;
		}
/* 13 Dec 9:05 */
	if( m_fMaster ) {
/* 13 Dec 9:05 */
		DWORD Data[2];

		SetASIDFlag(0, FALSE, Data);
		SetRBEFlag (0, FALSE, Data);

		if( UINT(GetTickCount() - Sock.m_uTime) >= ToTicks(20000) ) {

			uTotal = 0;

			Sock.m_pSocket->Close();

			m_fReadDataNotDone = FALSE;
			}
/* 13 Dec 9:05 */
		}

	else {
		m_fWaitAck = FALSE;
		}
/* 13 Dec 9:05 */
	return;
	}

// TCP Functions and Port Access

UINT CM2MDataTCP::HandlePhase(SOCKET &Sock)
{
	UINT Phase;

	Sock.m_pSocket->GetPhase(Phase);

	return Phase;
	}

UINT CM2MDataTCP::DoMasterPhase(SOCKET &Sock)
{
	UINT uPhase = 0;

	UINT uTimer = 5000;

	SetTimer(uTimer);

	while( GetTimer() ) {

		uPhase = HandlePhase(Sock);

		if( uPhase == PHASE_OPEN ) return PHASE_OPEN;

		Sleep(20);
		}

	return uPhase;
	}

BOOL CM2MDataTCP::OpenCommandPort(SOCKET &Sock)
{
	UINT uPhase;

	if( OpenMaster(Sock) ) {

		uPhase = DoMasterPhase(Sock);

		if( uPhase == PHASE_OPEN ) return TRUE;

//**/		AfxTrace0("\r\nNo Socket - Open Command Port");
		}

	InsertRunTimeError(ENOCONN);

	return FALSE;
	}

void CM2MDataTCP::OpenSocket(UINT n, SOCKET &Sock)
{
///**/	AfxTrace0("\r\nOpen Socket-");

	if( Sock.m_pSocket ) {

		return;
		}

	Sock.m_pSocket = CreateSocket(IP_TCP);

	if( Sock.m_pSocket ) {

		InitSocket(Sock);

		switch( n ) {

			case 1:
///**/				AfxTrace0("Open Master-");
				break;

			default:

///**/				AfxTrace0("Open Listener-");

				Sock.m_pSocket->Listen(m_uListenPort);

				break;
			}
		}

///**/	AfxTrace0("Exit Open Socket\r\n");
	}

void CM2MDataTCP::InitSocket(SOCKET &Sock)
{
	if( Sock.m_pSocket ) {

		Sock.m_pData = new BYTE [300];
		Sock.m_uPtr  = 0;
		Sock.m_fBusy = FALSE;
		}
	}

BOOL CM2MDataTCP::OpenMaster(SOCKET &Sock)
{
	if( !Sock.m_pSocket ) {

		OpenSocket(1, Sock);
		}

	if( Sock.m_pSocket ) {

		InitSocket(Sock);

		DWORD dTime  = GetTickCount() + ToTicks(1000);

		while( dTime > GetTickCount() ) {

			if( ConnectToServer(Sock) ) {

				Sock.m_uTime = GetTickCount();

				return TRUE;
				}

			Sleep(20);
			}

		CloseMaster(Sock);
		}

	return FALSE;
	}

BOOL CM2MDataTCP::ConnectToServer(SOCKET &Sock)
{
	DWORD &IP  = m_ServerIP;
	WORD wPort = m_uSendPort;

	BOOL f = Sock.m_pSocket->Connect((IPADDR &) IP, wPort) == S_OK;

///**/	if( f ) AfxTrace2("Connected %8.8lx %d ", IP, wPort); else AfxTrace0("{C}");

	m_fMaster = f;

	if( !f ) InsertRunTimeError(ENOCONN);

	return f;
	}

BOOL CM2MDataTCP::CheckAccess(SOCKET &Sock)
{
	DWORD IP;

	BOOL f = Sock.m_pSocket->GetRemote((IPADDR &) IP) == S_OK;

	if( !f ) InsertRunTimeError(ENOCONN);

	return f;
	}

BOOL CM2MDataTCP::ExecuteSend(SOCKET &Sock)
{
	BOOL fSent = FALSE;

	if( Sock.m_pSocket ) {

//**/		AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i]);

		fSent = Sock.m_pSocket->Send(m_bTx, m_uPtr) == S_OK;

		if( fSent ) m_bCtr++;
		}

//**/	if( fSent ) AfxTrace0("\r\nSend OK "); else AfxTrace0("\r\nBad Send ");

	if( !fSent ) InsertRunTimeError(ENOEXEC);

	return fSent;
	}

void CM2MDataTCP::CloseSocket(SOCKET &Sock)
{
//**/	AfxTrace0("\r\nCLOSE ");

	Sock.m_pSocket->Release();

	if( Sock.m_pData ) {

		delete Sock.m_pData;

		Sock.m_pData = NULL;
		}

//**/	AfxTrace0("Done\r\n");

	Sock.m_pSocket = NULL;
	}

void CM2MDataTCP::RestartConn(SOCKET &Sock)
{
	CloseMaster(Sock);
	}

void CM2MDataTCP::CloseMaster(SOCKET &Sock)
{
	if( Sock.m_pSocket ) {

		Sock.m_pSocket->Close();

		CloseSocket(Sock);

		m_fWaitAck = FALSE;

		m_fMaster  = FALSE;
		}
	}

// End of File
