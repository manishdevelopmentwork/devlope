
#include "intern.hpp"

#include "pdftask.hpp"

#include "uitask.hpp"

#include <CxPdf.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Globals
//

global void AllowPdfRestart(void)
{
	if( CUISystem::m_pThis->m_pPDF ) {

		CUISystem::m_pThis->m_pPDF->AllowRestart();
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// PDF Task
//

// Constructor

CPDFTask::CPDFTask(void)
{
	m_hTask   = NULL;
	
	m_pLock   = Create_Mutex();
	
	m_pFlag   = Create_AutoEvent();
	
	m_pHead   = NULL;

	m_pTail   = NULL;

	m_uHandle = NOTHING;

	m_fFree   = FALSE;
	}

// Destructor

CPDFTask::~CPDFTask(void)
{
	m_pLock->Release();

	m_pFlag->Release();
	}

// IBase

UINT CPDFTask::Release(void)
{
	delete this;

	return 1;
	}

// Task List

void CPDFTask::GetTaskList(CTaskList &List)
{
	if( WhoHasFeature(rfPdfViewer) ) {

		CTaskDef Task;

		Task.m_Name   = "PDF";
		Task.m_pEntry = this;
		Task.m_uID    = 0;
		Task.m_uCount = 1;
		Task.m_uLevel = 1900;
		Task.m_uStack = 12336;

		List.Append(Task);
		}
	}

// Operations

void CPDFTask::SubmitRequest(CPDFRequest *pRqst)
{
	if( WhoHasFeature(rfPdfViewer) ) {

		m_pLock->Wait(FOREVER);

		if( pRqst->m_uHandle == m_uHandle ) {

			RestartTask();			
			}

		CPDFRequest *pScan = m_pHead;

		while( pScan ) {

			CPDFRequest *pNext = pScan->m_pNext;

			if( pRqst->m_uHandle == pScan->m_uHandle ) {

				AfxListRemove( m_pHead,
					       m_pTail,
					       pScan,
					       m_pNext,
					       m_pPrev
					       );

				pScan->Release();
				}

			pScan = pNext;
			}

		AfxListAppend( m_pHead,
			       m_pTail,
			       pRqst,
			       m_pNext,
			       m_pPrev
			       );

		m_pLock->Free();

		m_pFlag->Set();
		}
	}

void CPDFTask::ClearRequests(UINT uHandle)
{
	if( WhoHasFeature(rfPdfViewer) ) {

		m_pLock->Wait(FOREVER);

		if( uHandle == m_uHandle ) {

			RestartTask();			
			}

		CPDFRequest *pScan = m_pHead;

		while( pScan ) {

			CPDFRequest *pNext = pScan->m_pNext;

			if( uHandle == pScan->m_uHandle ) {

				AfxListRemove( m_pHead,
					       m_pTail,
					       pScan,
					       m_pNext,
					       m_pPrev
					       );

				pScan->Release();
				}

			pScan = pNext;
			}

		m_pLock->Free();
		}
	}

void CPDFTask::AllowRestart(void)
{
	m_fFree = TRUE;
	}

// ITaskEntry

void CPDFTask::TaskInit(UINT uID)
{
	PdfInit();
	}

void CPDFTask::TaskExec(UINT uID)
{
	m_hTask   = GetCurrentTask();

	m_uHandle = NOTHING;

	for(;;) {

		m_pFlag->Wait(FOREVER);

		for(;;) {

			m_pLock->Wait(FOREVER);

			CPDFRequest *pScan = m_pHead;

			if( !pScan ) {

				m_uHandle = NOTHING;

				m_pLock->Free();

				break;
				}

			AfxListRemove( m_pHead,
				       m_pTail,
				       pScan,
				       m_pNext,
				       m_pPrev
				       );

			m_uHandle = pScan->m_uHandle;

			m_pLock->Free();

			CPdfContext Ctx;

			Ctx.m_pFile = PCTXT(pScan->m_File);
			
			Ctx.m_nRes  = pScan->m_nRes;
			
			Ctx.m_nPage = pScan->m_nPage;

			UINT uResult = PdfLoadPage(Ctx);

			switch( uResult ) {

				case pdfSuccess:

					pScan->m_pData     = Ctx.m_pData;

					pScan->m_nSize     = Ctx.m_nSize;
				
					pScan->m_nPages    = Ctx.m_nPages;

					pScan->m_Bitmap.cx = Ctx.m_nWidth;
						 
					pScan->m_Bitmap.cy = Ctx.m_nHeight;

					pScan->QueueEvent(301);

					break;

				case pdfFail:

					pScan->QueueEvent(302);

					break;

				case pdfMemoryError:

					pScan->QueueEvent(303);

					break;
				}
			}
		}
	}
	
void CPDFTask::TaskStop(UINT uID)
{
	}

void CPDFTask::TaskTerm(UINT uID)
{
	PdfTerm();
	}

// Implementation

void CPDFTask::RestartTask(void)
{
	// This is a bit awkward on Aeon...
	}

// End of File
