
//////////////////////////////////////////////////////////////////////////
//
// Data Spaces
//

#define	SPACE_HOLD	0x01

#define	PING_REGISTER	40615 // Drive Enable

#define	WORD_OFFSET	40001

#define	LONG_OFFSET	23617 // 40001 - 16384

//////////////////////////////////////////////////////////////////////////
//
// Emerson SP Premapped TCP/IP Driver
//

class CEmersonSPPTCPDriver : public CMasterDriver
{
	public:
		// Constructor
		CEmersonSPPTCPDriver(void);

		// Destructor
		~CEmersonSPPTCPDriver(void);
	
		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;

			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTx[300];
		BYTE	   m_bRx[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;

		// Implementation
		BOOL ConvertAddress(AREF Addr, CAddress * pAddr);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
				
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckFrame(void);

		// Read Handlers
		CCODE DoWordRead(UINT uOff, PDWORD pData, UINT uCount);
		CCODE DoLongRead(UINT uOff, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(UINT uOff, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(UINT uOff, PDWORD pData, UINT uCount);

		// Helpers

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
	};

// End of File
