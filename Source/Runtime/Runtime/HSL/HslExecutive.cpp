
#include "Intern.hpp"

#include <sys/reent.h>

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Executive
//

// Static Data

static IExecutive    * m_pExec = NULL;

static IWaitMultiple * m_pWait = NULL;

// Shared APIs

global void Bind_Executive(void)
{
	AfxGetObject("exec", 0, IExecutive,    m_pExec);

	AfxGetObject("exec", 0, IWaitMultiple, m_pWait);
	}

global void Open_Executive(void)
{
	m_pExec->Open();
	}

global void Close_Executive(void)
{
	m_pExec->Close();
	}

global void Free_Executive(void)
{
	if( m_pWait ) {

		m_pWait->Release();

		m_pWait = NULL;
		}

	m_pExec->Release();

	m_pExec = NULL;
	}

global IMutex * Create_Mutex(void)
{
	IMutex *pMutex = NULL;

	AfxNewObject("exec.mutex", IMutex, pMutex);

	return pMutex;
	}

global IMutex * Create_Qutex(void)
{
	IMutex *pMutex = NULL;

	AfxNewObject("exec.qutex", IMutex, pMutex);

	return pMutex;
	}

global IMutex * Create_Rutex(void)
{
	IMutex *pMutex = NULL;

	AfxNewObject("exec.rutex", IMutex, pMutex);

	return pMutex;
	}

global ISemaphore * Create_Semaphore(void)
{
	ISemaphore *pSemaphore = NULL;

	AfxNewObject("exec.semaphore", ISemaphore, pSemaphore);

	return pSemaphore;
	}

global ISemaphore * Create_Semaphore(UINT uCount)
{
	ISemaphore *pSemaphore = NULL;

	AfxNewObject("exec.semaphore", ISemaphore, pSemaphore);

	pSemaphore->Signal(uCount);

	return pSemaphore;
	}

global IEvent * Create_ManualEvent(void)
{
	IEvent *pEvent = NULL;

	AfxNewObject("exec.event-m", IEvent, pEvent);

	return pEvent;
	}

global IEvent * Create_AutoEvent(void)
{
	IEvent *pEvent = NULL;

	AfxNewObject("exec.event-a", IEvent, pEvent);

	return pEvent;
	}

global HTHREAD CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam)
{
	return m_pExec->CreateThread(pfnProc, uLevel, pParam, uParam);
	}

global HTHREAD CreateClientThread(IClientProcess *pProc, UINT uIndex, UINT uLevel)
{
	return m_pExec->CreateThread(pProc, uIndex, uLevel);
	}

global HTHREAD CreateClientThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms)
{
	return m_pExec->CreateThread(pProc, uIndex, uLevel, pTms);
	}

global BOOL WaitThread(HTHREAD hThread, UINT uTime)
{
	return hThread->Wait(uTime);
	}

global void GuardThread(BOOL fGuard)
{
	m_pExec->GetCurrentThread()->Guard(fGuard);
	}

global void GuardThread(PGUARD pProc, PVOID pData)
{
	m_pExec->GetCurrentThread()->Guard(pProc, pData);
	}

global BOOL AdvanceThread(HTHREAD hThread)
{
	return hThread->Advance();
	}

global BOOL DestroyThread(HTHREAD hThread)
{
	return m_pExec->DestroyThread(hThread);
	}

global void ExitThread(int nCode)
{
	return m_pExec->GetCurrentThread()->Exit(nCode);
	}

global void CloseThread(HTHREAD hThread)
{
	hThread->Release();
	}

global void Sleep(UINT uTime)
{
	m_pExec->Sleep(uTime);
	}

global BOOL ForceSleep(UINT uTime)
{
	return m_pExec->ForceSleep(uTime);
	}

global UINT GetTickCount(void)
{
	return m_pExec->GetTickCount();
	}

global UINT ToTicks(UINT uTime)
{
	return m_pExec->ToTicks(uTime);
	}

global UINT ToTime(UINT uTicks)
{
	return m_pExec->ToTime(uTicks);
	}

global PCTXT GetThreadName(HTHREAD hThread)
{
	return hThread->GetName();
	}

global void SetThreadName(PCTXT pName)
{
	m_pExec->GetCurrentThread()->SetName(pName);
	}

global UINT GetThreadIntParam(void)
{
	return m_pExec->GetCurrentThread()->GetIntParam();
	}

global PVOID GetThreadPtrParam(void)
{
	return m_pExec->GetCurrentThread()->GetPtrParam();
	}

global UINT GetThreadExecState(HTHREAD hThread)
{
	return hThread->GetExecState();
	}

global INT GetThreadExitCode(HTHREAD hThread)
{
	return hThread->GetExitCode();
	}

global DWORD GetThreadFlags(void)
{
	if( m_pExec ) {

		IThread *pThread = m_pExec->GetCurrentThread();
		
		if( pThread ) {
			
			return pThread->GetFlags();
			}
		}

	return 0;
	}

global void SetThreadFlags(DWORD dwMask, DWORD dwData)
{
	m_pExec->GetCurrentThread()->SetFlags(dwMask, dwData);
	}

global HDATA GetThreadData(UINT uID)
{
	return m_pExec->GetCurrentThread()->GetData(uID);
	}

global BOOL SetThreadData(HDATA hData)
{
	return m_pExec->GetCurrentThread()->SetData(hData);
	}

global BOOL FreeThreadData(UINT uID)
{
	return m_pExec->GetCurrentThread()->FreeData(uID);
	}

global UINT AddExecThreadNotify(IThreadNotify *pNotify)
{
	return m_pExec->AddNotify(pNotify);
	}

global BOOL RemoveExecThreadNotify(UINT uNotify)
{
	return m_pExec->RemoveNotify(uNotify);
	}

global UINT AddThreadNotify(IThreadNotify *pNotify)
{
	return m_pExec->GetCurrentThread()->AddNotify(pNotify);
	}

global ITimer * CreateTimer(void)
{
	return m_pExec->CreateTimer();
	}

global void SetThreadLibPointer(PVOID pEh)
{
	m_pExec->GetCurrentThread()->SetLibPointer(pEh);
	}
		
global PVOID GetThreadLibPointer(void)
{
	return m_pExec->GetCurrentThread()->GetLibPointer();
	}

global UINT WaitMultiple(IWaitable **pList, UINT uCount, UINT uTime)
{
	AfxAssert(m_pWait);

	return m_pWait->WaitMultiple(pList, uCount, uTime);
	}

global UINT WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, UINT uTime)
{
	AfxAssert(m_pWait);

	return m_pWait->WaitMultiple(pWait1, pWait2, uTime);
	}

global UINT WaitMultiple(IWaitable *pWait1, IWaitable *pWait2, IWaitable *pWait3, UINT uTime)
{
	AfxAssert(m_pWait);

	return m_pWait->WaitMultiple(pWait1, pWait2, pWait3, uTime);
	}

// Client APIs

#if defined(CLIENT_APIs)

HTHREAD GetCurrentThread(void)
{
	return m_pExec->GetCurrentThread();
	}

global UINT GetThreadIndex(void)
{
	return m_pExec->GetCurrentThread()->GetIndex();
	}

global void CheckThreadCancellation(void)
{
	m_pExec->GetCurrentThread()->CheckCancellation();
	}

global void SetTimer(UINT uTime)
{
	m_pExec->GetCurrentThread()->SetTimer(uTime);
	}

global UINT GetTimer(void)
{
	return m_pExec->GetCurrentThread()->GetTimer();
	}

#endif

// End of File
