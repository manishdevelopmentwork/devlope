#include "mew7base.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 TCP/IP Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

class CPanFpMewtocol7TcpMasterDriver : public CPanFpMewtocol7BaseMasterDriver
{
	public:
		// Constructor
		CPanFpMewtocol7TcpMasterDriver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		
		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);

		// User Access
		UINT MCALL DevCtrl(void *pContext, UINT uFunc, PCTXT Value);

		// Entry Points
		DEFMETH(CCODE) Ping (void);

	protected:
		// Device Data
		struct CContext	: CPanFpMewtocol7BaseMasterDriver::CBaseCtx
		{
			DWORD	 m_IP;
			DWORD	 m_IP2;
			UINT	 m_uPort;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fDirty;
			BOOL	 m_fAux;
			};

		// Data Members
		CContext * m_pCtx; 
		UINT	   m_uKeep;
		
		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);

		// Implementation
		BOOL SendFrame(void);
		BOOL RecvFrame(void);
		
		// Overridables
		BOOL Transact(void);

		// Helpers
		BOOL IsOctetEnd(char c);
		BOOL IsDigit(char c);
		BOOL IsByte(UINT uNum);
		};
		

// End of File

