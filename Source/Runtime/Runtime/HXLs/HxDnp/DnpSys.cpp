
#include "Intern.hpp"

#include "Channel.hpp"

#include "DnpSys.hpp"

#include "Dnp3.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DNP Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Debug Help
//

#define DNP_SYS_DEBUG 0

/////////////////////////////////////////////////////////////////////////
//
// Type Definitions
//

static ITimeZone * m_pZone  = NULL;

//////////////////////////////////////////////////////////////////////////
//
// System Support API for DNP SCL
//

void   Sys_Init(void)
{
	if( !m_pZone ) {

		AfxGetObject("dev.tz", 0, ITimeZone, m_pZone);
	}
}

void * Sys_Alloc(dnpUINT numBytes)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Alloc %u\n", numBytes);
	}

	return malloc(numBytes);
}

void Sys_Free(void * pBuff)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Free\n");
	}

	free(pBuff);
}

int Sys_Snprintf(dnpCHAR *buf, dnpUINT count, const dnpCHAR *format, ...)
{
/*	va_list pArgs;

	va_start(pArgs, format);

	AfxTrace(format, pArgs);

	va_end(pArgs);

	int len = strlen(format);

	if( len <= 0 ) {

		return 0;
		}

	return len;

	*/return 0;
}

void Sys_PutDiagString(const dnpDIAG * pAnlzId, const dnpCHAR *pString)
{
	//AfxTrace("Msg from chan %8.8x sess %8.8x src %u - %s\n", pAnlzId->pChannel, pAnlzId->pSession, pAnlzId->sourceId, *pString);
}

void Sys_LockInit(dnpLOCK *pLock)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Lock Init %8.8x\n", pLock);
	}

/*IDnp * pDnp = CDnp3::Locate();

if( pDnp ) {

	pDnp->InitLock(pLock);
	}*/
}

void Sys_LockShare(dnpLOCK *pLock, dnpLOCK *pLock1)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Lock Share\n");
	}
}

void Sys_LockSection(dnpLOCK *pLock)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Lock Section %8.8x\n", pLock);
	}

/*IDnp * pDnp = CDnp3::Locate();

if( pDnp ) {

	pDnp->Lock(pLock);
	}*/
}

void Sys_UnlockSection(dnpLOCK *pLock)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Unlock Section\n");
	}

/*IDnp * pDnp = CDnp3::Locate();

if( pDnp ) {

	pDnp->Free(pLock);
	}*/
}

void Sys_LockDelete(dnpLOCK *pLock)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Lock Delete\n");
	}

/*IDnp * pDnp = CDnp3::Locate();

if( pDnp ) {

	pDnp->DeleteLock(pLock);
	}*/
}

dnpMS Sys_GetMSTime(void)
{
	return ToTime(GetTickCount());
}

void Sys_GetDateTime(dnpTIME *pDateTime)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Get Time\n"); 
	}

	struct timeval tv;

	gettimeofday(&tv, NULL);

	if( m_pZone ) {

		INT nOffset = m_pZone->GetOffset();

		INT nDst    = m_pZone->GetDst();

		tv.tv_sec  -= -60 * (nOffset + (nDst ? 60 : 0));
	}

	struct tm *tm = gmtime(&tv.tv_sec);

	pDateTime->mSecsAndSecs = TMWTYPES_USHORT(tm->tm_sec * 1000 + tv.tv_usec / 1000);
	pDateTime->minutes	= TMWTYPES_UCHAR(tm->tm_min);
	pDateTime->hour		= TMWTYPES_UCHAR(tm->tm_hour);
	pDateTime->dayOfWeek	= TMWTYPES_UCHAR(1 + (tm->tm_wday + 6) % 7);
	pDateTime->dayOfMonth	= TMWTYPES_UCHAR(tm->tm_mday + 0);
	pDateTime->month	= TMWTYPES_UCHAR(tm->tm_mon  + 1);
	pDateTime->year		= TMWTYPES_USHORT(tm->tm_year + 1900);
}

dnpBOOL Sys_SetDateTime(dnpTIME *pDateTime)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Set Time\n");
	}

	struct tm tm = { 0 };

	tm.tm_sec  = pDateTime->mSecsAndSecs / 1000;
	tm.tm_min  = pDateTime->minutes;
	tm.tm_hour = pDateTime->hour;
	tm.tm_mday = pDateTime->dayOfMonth - 0;
	tm.tm_mon  = pDateTime->month      - 1;
	tm.tm_year = pDateTime->year       - 1900;

	struct timeval tv;

	tv.tv_sec   = timegm(&tm);

	tv.tv_usec  = 0;

	if( m_pZone ) {

		INT nOffset = m_pZone->GetOffset();

		INT nDst    = m_pZone->GetDst();

		tv.tv_sec  += -60 * (nOffset + (nDst ? 60 : 0));
	}

	settimeofday(&tv, NULL);

	return TRUE;
}

void * Sys_InitChannel(const void * pUser, dnpCNFG * pCfg, dnpCHAN * pChan)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Init Channel %8.8x\n", pUser);
	}

	return (void *) pUser;
}

void Sys_DeleteChannel(void * pCtx)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Delete Channel\n");
	}
}

const dnpCHAR * Sys_GetChannelName(void * pCtx)
{
	return ("Unknown");
}

const dnpCHAR * Sys_GetChannelInfo(void * pCtx)
{
	return ("Unknown");
}

dnpBOOL Sys_OpenChannel(void *pCtx, dnpFRX pRx, dnpFCHK pChk, void *pParam)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("SYS_DNP Open Channel %8.8x\n", pCtx);
	}

	return TMWDEFS_TRUE;;
}

void Sys_CloseChannel(void * pCtx)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("SYS_DNP Close Channel %8.8x\n", pCtx);
	}
}

const dnpCHAR * Sys_GetSessionName(dnpSES *pSession)
{
	return ("Unknown");
}

const dnpCHAR * Sys_GetSectorName(dnpSEC *pSector)
{
	return ("Unknown");
}

dnpMS Sys_GetTransmitReady(void *pCtx)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("DNP Get Transmit Ready\n");
	}

	CChannel * pChannel = (CChannel *) pCtx;

	if( pChannel ) {

		return pChannel->GetTxReady();
	}

	return 0;
}

dnpWORD Sys_Rx(void *pCtx, dnpUCHAR *pBuff, dnpWORD Bytes, dnpMS Time, dnpBOOL *pTO, dnpMS *pFirst)
{
	CChannel * pChannel = (CChannel *) pCtx;

	if( DNP_SYS_DEBUG ) {

		AfxTrace("SYS_DNP Rx %8.8x\n", pCtx);
	}

	if( pChannel ) {

		return pChannel->RxData(pBuff, Bytes, Time, pTO, pFirst);
	}

	return 0;
}

dnpBOOL Sys_Tx(void *pCtx, dnpUCHAR *pBuff, dnpWORD Bytes)
{
	if( DNP_SYS_DEBUG ) {

		AfxTrace("SYS_DNP Tx %8.8x\n", pCtx);
	}

	CChannel * pChannel = (CChannel *) pCtx;

	if( pChannel ) {

		return pChannel->TxData(pBuff, Bytes) ? true : false;
	}

	return 0;
}

dnpBOOL Sys_TxUDP(void *pCtx, dnpUCHAR UDPPort, dnpUCHAR *pBuff, dnpWORD numBytes)
{
	CChannel * pChannel = (CChannel *) pCtx;

	if( DNP_SYS_DEBUG ) {

		AfxTrace("SYS_DNP Tx UDP %8.8x\n", pCtx);
	}

	if( pChannel ) {

		return pChannel->TxUDP(UDPPort, pBuff, numBytes) ? true : false;
	}

	return FALSE;
}

// End of File
