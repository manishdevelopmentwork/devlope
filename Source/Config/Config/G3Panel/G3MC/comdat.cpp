
#include "intern.hpp"

#include "legacy.h"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Item with Comms Objects
//

// Runtime Class

AfxImplementRuntimeClass(CObjectItem, CMetaItem);

// Comms Object Access

UINT CObjectItem::GetObjectCount(void)
{
	return 0;
	}

BOOL CObjectItem::GetObjectData(UINT uIndex, CObjectData &Data)
{
	return FALSE;
	}

BOOL CObjectItem::HasUserData(UINT uIndex)
{
	CObjectData ObjData;

	if( GetObjectData(uIndex, ObjData) ) {

		for( UINT n = 0; n < ObjData.pItem->GetCommsDataCount(); n++ ) {

			CCommsData CommsData;

			if( ObjData.pItem->GetCommsData(n, CommsData) ) {

				if( CommsData.Usage != usageWriteInit ) {

					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

// Download Support

BOOL CObjectItem::MakeInitData(CInitData &Init)
{
	for( UINT uIndex = 0; uIndex < GetObjectCount(); uIndex++ ) {

		CObjectData Data;

		GetObjectData(uIndex, Data);

		Data.pItem->SetID(Data.ID);

		Data.pItem->MakeInitData(Init);
		}

	Init.AddWord(0);

	return TRUE;
	}

// Comms Object Helpers

UINT CObjectItem::FindObject(CString Name)
{
	for( UINT uIndex = 0; uIndex < GetObjectCount(); uIndex++ ) {

		CObjectData Data;

		if( GetObjectData(uIndex, Data) ) {

			if( Data.Name == Name ) {

				return uIndex;
				}
			}
		else
			break;
		}

	return NOTHING;
	}

UINT CObjectItem::FindObject(BYTE ID)
{
	for( UINT uIndex = 0; uIndex < GetObjectCount(); uIndex++ ) {

		CObjectData Data;

		if( GetObjectData(uIndex, Data) ) {

			if( Data.ID == ID ) {

				return uIndex;
				}
			}
		else
			break;
		}

	return NOTHING;
	}

//////////////////////////////////////////////////////////////////////////
//
// Item with Comms Data
//

// Runtime Class

AfxImplementRuntimeClass(CCommsItem, CMetaItem);

// Constructor

CCommsItem::CCommsItem(void)
{
	m_uCommsCount = 0;

	m_pCommsData  = NULL;
	}

// Operations

void CCommsItem::SetID(UINT ID)
{
	m_ID = ID;
	}

// Group Names

CString CCommsItem::GetGroupName(WORD Group)
{
	return L"";
	}

// Comms Schema Access

UINT CCommsItem::GetCommsDataCount(void)
{
	return m_uCommsCount;
	}

BOOL CCommsItem::GetCommsData(UINT uIndex, CCommsData &Data)
{
	if( uIndex < m_uCommsCount ) {

		CCommsList const &List = m_pCommsData[uIndex];

		Data.Group    = List.Group;

		Data.UserName = List.UserID;

		Data.PropName = List.pName;
		Data.PropID   = List.PropID;
		Data.Usage    = List.Usage;

		return TRUE;
		}

	return FALSE;
	}

UINT CCommsItem::FindCommsData(CString Name)
{
	for( UINT uIndex = 0; uIndex < m_uCommsCount; uIndex++ ) {

		if( CString(m_pCommsData[uIndex].UserID).CompareC(Name) ) {

			continue;
			}

		return uIndex;
		}

	return NOTHING;
	}

UINT CCommsItem::FindCommsData(WORD PropID)
{
	for( UINT uIndex = 0; uIndex < m_uCommsCount; uIndex++ ) {

		if( m_pCommsData[uIndex].PropID == PropID ) {

			return uIndex;
			}
		}

	return NOTHING;
	}

// Conversion

BOOL CCommsItem::Convert(CPropValue *pValue)
{
	if( pValue ) {
		
		UINT c = GetCommsDataCount();

		for( UINT n = 0; n < c; n ++ ) {

			CCommsData Data;

			if( !GetCommsData(n, Data) ) {
				
				break;
				}

			ImportNumber(pValue, Data.PropName);
			}

		return TRUE;
		}
	
	return FALSE;
	}

// Download Support

BOOL CCommsItem::MakeInitData(CInitData &Init)
{
	for( UINT uIndex = 0; uIndex < m_uCommsCount; uIndex++ ) {

		CCommsData Data;

		GetCommsData(uIndex, Data);

		if( Data.Usage & usageWriteInit ) {

			WORD PropID = Data.PropID;

			if( IncludeProp(PropID) ) {

				DWORD Value = GetIntProp(Data.PropName);

				if( HIBYTE(PropID) == TYPE_BOOL ) {

					if( Value ) {

						PropID |= (m_ID << 11);

						PropID |= (CMD_SET << 8);
						}
					else {
						PropID |= (m_ID << 11);

						PropID |= (CMD_CLEAR << 8);
						}

					Init.AddWord(PropID);
					}
				else {
					PropID |= (m_ID << 11);

					PropID |= (CMD_WRITE << 8);

					Init.AddWord(PropID);

					switch( HIBYTE(PropID) & 0x07 ) {

						case TYPE_BYTE:
						case TYPE_WORD:
						case TYPE_LONG:
						case TYPE_REAL:

							Init.AddWord(WORD(Value));

							break;

						case TYPE_INT32:
						case TYPE_UINT32:

							Init.AddLong(Value);

							break;
						}
					}
				}
			}
		}
		
	return TRUE;
	}

// Property Filter

BOOL CCommsItem::IncludeProp(WORD PropID)
{
	return TRUE;
	}

// Data Scaling

DWORD CCommsItem::GetIntProp(PCTXT pTag)
{
	CMetaData const *pMeta = FindMetaData(pTag);

	if( pTag ) {

		INT nData = pMeta->ReadInteger(this);

		return DWORD(nData);
		}

	return 0;
	}

// Validation

void CCommsItem::CheckCommsData(void)
{
	CheckComm();

	CheckMeta();
	}

void CCommsItem::CheckComm(void)
{
	}

void CCommsItem::CheckMeta(void)
{
	}

// End of File
