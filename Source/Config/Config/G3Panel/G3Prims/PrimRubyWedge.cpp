
#include "intern.hpp"

#include "PrimRubyWedge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "PrimRubyPenEdge.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby Wedge Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyWedge, CPrimRubyGeom);

// Constructor

CPrimRubyWedge::CPrimRubyWedge(void)
{
	m_Orient = 0;
	}

// Meta Data

void CPrimRubyWedge::AddMetaData(void)
{
	CPrimRubyGeom::AddMetaData();

	Meta_AddInteger(Orient);

	Meta_SetName((IDS_WEDGE_2));
	}

// Path Management

void CPrimRubyWedge::MakePaths(void)
{
	R2 Rect = m_DrawRect;

	m_pEdge->AdjustRect(Rect);

	CRubyPoint p1(Rect, 1);
	CRubyPoint p2(Rect, 4);

	if( m_Orient != 3 ) m_pathFill.Append(p1.m_x, p1.m_y);
	if( m_Orient != 2 ) m_pathFill.Append(p2.m_x, p1.m_y);
	if( m_Orient != 1 ) m_pathFill.Append(p2.m_x, p2.m_y);
	if( m_Orient != 0 ) m_pathFill.Append(p1.m_x, p2.m_y);

	m_pathFill.AppendHardBreak();

	CPrimRubyGeom::MakePaths();
	}

// End of File
