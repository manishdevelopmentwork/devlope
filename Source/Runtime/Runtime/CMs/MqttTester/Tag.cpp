
#include "Intern.hpp"

#include "Tag.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "Format.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson Tag
//

// Destructor

CTag::~CTag(void)
{
	}

// Property Helpers

UINT CTag::GetPropCount(void)
{
	return 17;
	}

DWORD CTag::GetDefProp(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return DWORD(wstrdup(L"As-Text"));

		case tpDescription:
			return DWORD(wstrdup(L"Desc"));

		case tpTextOff:
			return DWORD(wstrdup(L"OFF"));

		case tpTextOn:
			return DWORD(wstrdup(L"ON"));

		case tpForeColor:
			return GetRGB(31,31,31);

		case tpBackColor:
			return GetRGB(0,0,0);
		}

	if( ID == tpMaximum ) {

		if( Type == typeInteger ) {

			return 100;
			}

		if( Type == typeReal ) {

			return R2I(100);
			}
		}

	return GetNull(Type);
	}

PCTXT CTag::GetPropName(WORD ID)
{
	switch( ID ) {

		case tpAsText:      return "AsText";
		case tpLabel:	    return "Label";
		case tpDescription: return "Description";
		case tpPrefix:      return "Prefix";
		case tpUnits:	    return "Units";
		case tpSetPoint:    return "SetPoint";
		case tpMinimum:     return "Minimum";
		case tpMaximum:     return "Maximum";
		case tpForeColor:   return "ForeColor";
		case tpBackColor:   return "BackColor";
		case tpName:	    return "Name";
		case tpIndex:	    return "Index";
		case tpAlarms:      return "Alarms";
		case tpTextOff:     return "TextOff";
		case tpTextOn:      return "TextOn";
		case tpStateCount:  return "StateCount";
		case tpDeadband:    return "Deadband";
		}

	return "";
	}

UINT CTag::GetPropType(WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:      return typeString;
		case tpLabel:	    return typeString;
		case tpDescription: return typeString;
		case tpPrefix:      return typeString;
		case tpUnits:	    return typeString;
		case tpSetPoint:    return Type;
		case tpMinimum:     return Type;
		case tpMaximum:     return Type;
		case tpForeColor:   return typeInteger;
		case tpBackColor:   return typeInteger;
		case tpName:	    return typeString;
		case tpIndex:	    return typeInteger;
		case tpAlarms:      return typeInteger;
		case tpTextOff:     return typeString;
		case tpTextOn:      return typeString;
		case tpStateCount:  return typeInteger;
		case tpDeadband:    return Type;
		}

	return typeVoid;
	}

// Attributes

UINT CTag::GetDataType(void) const
{
	AfxAssert(FALSE);

	return 0;
	}

BOOL CTag::IsArray(void) const
{
	return FALSE;
	}

UINT CTag::GetExtent(void) const
{
	return 0;
	}

BOOL CTag::CanWrite(void) const
{
	return FALSE;
	}

// Limit Access

DWORD CTag::GetMinValue(UINT uPos, UINT Type)
{
	CDataRef Ref;

	((DWORD &) Ref) = 0;

	Ref.x.m_Array   = uPos;

	return GetProp(Ref, tpMinimum, Type);
	}

DWORD CTag::GetMaxValue(UINT uPos, UINT Type)
{
	CDataRef Ref;

	((DWORD &) Ref) = 0;

	Ref.x.m_Array   = uPos;

	return GetProp(Ref, tpMaximum, Type);
	}

// Service Access

DWORD CTag::GetColorPair(UINT uPos)
{
	return 0x07FFF0000;
	}

CUnicode CTag::GetLabel(UINT uPos)
{
	CUnicode Label;
	
	Label = m_Name;

	if( IsArray() ) {

		Label += UniConvert(CPrintf("[%d]", uPos));
		}

	return Label;
	}

CUnicode CTag::GetAsText(UINT uPos, DWORD Data, UINT Type, UINT Flags)
{
	CUnicode Text;

	Text = CDispFormat::GeneralFormat(Data, Type, Flags);

	return Text;
	}

CUnicode CTag::GetAsText(UINT uPos, UINT Flags)
{
	CDataRef Ref;

	Ref.m_Ref     = 0;

	Ref.x.m_Array = uPos;

	UINT  Type    = typeVoid;

	DWORD Data    = 0;

	if( IsAvail(Ref) ) {

		Type = GetDataType();

		Data = GetData(Ref, Type, getNone);
		}

	CUnicode Text = GetAsText(uPos, Data, Type, Flags);

	FreeData(Data, Type);

	return Text;
	}

CString CTag::GetStates(void)
{
	return "";
	}

BOOL CTag::SetAsText(UINT uPos, CString const &Text)
{
	UINT  Type = GetDataType();

	DWORD Data = 0;

	if( CDispFormat::GeneralParse(Data, Text, Type) ) {

		CDataRef Ref;

		Ref.m_Ref     = 0;

		Ref.x.m_Array = uPos;

		if( IsValid(Ref, Data, Type, setNone) ) {

			SetData(Ref, Data, Type, setNone);

			return TRUE;
			}
		}

	return FALSE;
	}

// Evaluation

BOOL CTag::IsAvail(CDataRef const &Ref)
{
	return IsAvail(Ref, 0);
	}

BOOL CTag::IsAvail(CDataRef const &Ref, UINT Flags)
{
	return TRUE;
	}

BOOL CTag::SetScan(CDataRef const &Ref, UINT Code)
{
	return TRUE;
	}

DWORD CTag::GetData(CDataRef const &Ref, UINT Type, UINT Flags)
{
	if( Flags & getNoString ) {

		if( GetDataType() == typeString ) {

			return 0;
			}
		}

	return GetNull(Type);
	}

BOOL CTag::IsValid(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	return TRUE;
	}

BOOL CTag::SetData(CDataRef const &Ref, DWORD Data, UINT Type, UINT Flags)
{
	FreeData(Data, Type);

	return TRUE;
	}

DWORD CTag::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText(Ref);

		case tpLabel:
			return FindLabel(Ref);

		case tpDescription:
			return DWORD(wstrdup(m_Desc));

		case tpForeColor:
			return FindFore(Ref);

		case tpBackColor:
			return FindBack(Ref);

		case tpName:
			return DWORD(wstrdup(m_Name));
		}

	switch( ID ) {

		case tpAsText:
		case tpLabel:
		case tpDescription:
		case tpPrefix:
		case tpUnits:
		case tpName:
		case tpTextOn:
		case tpTextOff:
			return GetNull(typeString);

		case tpSetPoint:
		case tpMinimum:
		case tpMaximum:
		case tpDeadband:
			return GetNull(Type);

		case tpForeColor:
		case tpBackColor:
		case tpIndex:
		case tpAlarms:
			return 0;
		}

	return 0;
	}

// Deadband

void CTag::InitPrevious(DWORD &Prev)
{
	Prev = 0x80000000;
	}

BOOL CTag::HasChanged(CDataRef const &Ref, DWORD &Data, DWORD Prev)
{
	DWORD Read = GetData(Ref, typeVoid, getNone);

	if( Prev != Read ) {

		Data = Read;

		return TRUE;
		}

	return FALSE;
	}

void CTag::KillPrevious(DWORD &Prev)
{
	InitPrevious(Prev);
	}

// Property Access

DWORD CTag::FindAsText(CDataRef const &Ref, UINT Type, DWORD Data)
{
	CUnicode Text = CDispFormat::GeneralFormat(Data, Type, fmtANSI);

	FreeData(Data, Type);

	return DWORD(wstrdup(Text));
	}

DWORD CTag::FindAsText(CDataRef const &Ref)
{
	UINT  Type = GetDataType();

	DWORD Data = GetData(Ref, Type, getNone);

	return FindAsText(Ref, Type, Data);
	}

DWORD CTag::FindLabel(CDataRef const &Ref)
{
	return DWORD(wstrdup(m_Name));
	}

DWORD CTag::FindFore(CDataRef const &Ref)
{
	return GetRGB(31,31,31);
	}

DWORD CTag::FindBack(CDataRef const &Ref)
{
	return GetRGB(0,0,0);
	}

// End of File
