
//////////////////////////////////////////////////////////////////////////
//
// Crimson Comms Architecture
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_WIFI_HPP
	
#define	INCLUDE_WIFI_HPP

//////////////////////////////////////////////////////////////////////////
//
// WiFi Station Driver
//

#if defined(PROJECT_C3COMMS)

class CWifiStationDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CWifiStationDriver(void);

		// Binding Control
		UINT GetBinding(void);

		// Configuration
		CLASS GetDriverConfig(void);

	protected:
		// Data Members
		UINT m_Binding;
	};

#endif

//////////////////////////////////////////////////////////////////////////
//
// WiFi Station Driver Options
//

class CWifiStationDriverOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CWifiStationDriverOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Public Data
		UINT    m_Type;
		CString m_Ssid;
		CString m_Pass;
		UINT    m_Mode;
		UINT	m_Addr;
		UINT	m_Mask;
		UINT	m_Gate;

	protected:
		// Property Save Filter
		BOOL SaveProp(CString Tag) const;

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);
	};

// End of File

#endif
