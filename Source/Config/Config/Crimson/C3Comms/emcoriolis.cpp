//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Comms Architecture
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#include "intern.hpp"

#include "emcoriolis.hpp"

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Device
//

// Contructors

CCoriolisModel::CCoriolisModel(void) 
{
	CEMCoriolisDevices();
	}

CCoriolisModel::CCoriolisModel(CString Name, UINT uDeviceModel) 
{
	m_Name		   = Name;

	m_uDeviceModel	   = uDeviceModel;

	m_HoldingRegisters = GetCombinedRegs(Registers, FALSE);

	m_CoilRegisters    = GetCombinedRegs(Coils,     FALSE);
	}

// Destructors

CCoriolisModel::~CCoriolisModel(void) 
{

	}

// Properties

CString CCoriolisModel::GetName(void) 
{
	return m_Name;
	}
	
// Registers

BOOL CCoriolisModel::MakeRegisters(UINT uChannel)
{
	switch( m_uDeviceModel ) {
			
		case CDMAnalog: 
			
			m_Registers = LoadCDMAnalog(uChannel);

			return TRUE;

		case Model2700Analog:

			m_Registers = LoadModel2700Analog(uChannel);

			return TRUE;

		case MVD9739:

			m_Registers = LoadMVD9739(uChannel);

			return TRUE;

		case Model1500LFT2:

			m_Registers = LoadModel1500LFT2(uChannel);

			return TRUE;
		
		case Model1700Analog:

			m_Registers = LoadModel1700Analog(uChannel);

			return TRUE;
			
		case Model2500:

			m_Registers = LoadModel2500(uChannel);

			return TRUE;

		case Series3000MVD:

			m_Registers = LoadSeries3000MVD(uChannel);

			return TRUE;

		case MVDDirectConnectEnhanced:

			m_Registers = LoadMVDDirectConnectEnhanced(uChannel);

			return TRUE;

		case MVDDirectConnectStandard:

			m_Registers = LoadMVDDirectConnectStandard(uChannel);

			return TRUE;

		case Model5700Config:

			m_Registers = LoadModel5700Config(uChannel);

			return TRUE;

		case RFT9739:

			m_Registers = LoadRFT9739(uChannel);

			return TRUE;

		case FDMAnalog:

			m_Registers = LoadFDMAnalog(uChannel);

			return TRUE;

		case HFVMAnalog:

			m_Registers = LoadHFVMAnalog(uChannel);

			return TRUE;

		case SGMAnalog:

			m_Registers = LoadSGMAnalog(uChannel);

			return TRUE;

		case GDMAnalog:

			m_Registers = LoadGDMAnalog(uChannel);

			return TRUE;

		case Series3000NOC4Wire:
			
			m_Registers = LoadSeries3000NOC4Wire(uChannel);

			return TRUE;

		case FillingMassTransmitter:

			m_Registers = LoadFillingMassTransmitter(uChannel);

			return TRUE;

		case Model1500Filling:

			m_Registers = LoadModel1500Filling(uChannel);

			return TRUE;

		case FVMAnalog:

			m_Registers = LoadFVMAnalog(uChannel);

			return TRUE;

		case Series3000NOC9Wire:

			m_Registers = LoadSeries3000NOC9Wire(uChannel);

			return TRUE;
		}
	
	return FALSE;
	}

CRegArray CCoriolisModel::GetRegs(UINT uChannel, BOOL fSortAlpha) 
{
	MakeRegisters(uChannel);

	if( fSortAlpha ) {

		CRegArray SortedArray(m_Registers);

		SortRegs(SortedArray, fSortAlpha);

		m_Registers = SortedArray;
		}
	
	return m_Registers;
	}

CRegArray CCoriolisModel::GetCombinedRegs(UINT uRegType, BOOL fSortAlpha) 
{
	if( uRegType == Coils || uRegType == inputCoils || uRegType == outputCoils ) {

		CRegArray InputCoilsArray   = GetRegs(inputCoils, FALSE);

		CRegArray OutputCoilsArray  = GetRegs(outputCoils, FALSE);

		CRegArray CombinedRegisters(InputCoilsArray);

		CombinedRegisters.Append(OutputCoilsArray);

		SortRegs(CombinedRegisters, fSortAlpha);

		m_Registers = CombinedRegisters;

		return m_Registers;
		}

	if( uRegType == Registers || uRegType == inputRegisters || uRegType == holdingRegisters ) {

		CRegArray InputRegistersArray    = GetRegs(inputRegisters, FALSE);

		CRegArray HoldingRegistersArray  = GetRegs(holdingRegisters, FALSE);

		CRegArray CombinedRegisters(InputRegistersArray);

		CombinedRegisters.Append(HoldingRegistersArray);

		SortRegs(CombinedRegisters, fSortAlpha);

		m_Registers = CombinedRegisters;

		return m_Registers;
		}

	return GetRegs(uRegType, fSortAlpha);
	}

void  CCoriolisModel::DoSortRegs(BOOL fSortAlpha) 
{
	CRegArray CoilsArray;
	CRegArray RegistersArray;

	SortRegs(RegistersArray, fSortAlpha);
	SortRegs(CoilsArray,     fSortAlpha);

	m_HoldingRegisters = RegistersArray;
	m_CoilRegisters   = CoilsArray;
	}

CRegArray CCoriolisModel::GetUnifiedRegs(UINT uRegType, BOOL fSortAlpha) {

	if( uRegType == Coils || uRegType == inputCoils || uRegType == outputCoils ) {

		SortRegs(m_CoilRegisters, fSortAlpha);

		return m_CoilRegisters;
		}
	else {
		SortRegs(m_HoldingRegisters, fSortAlpha);

		return m_HoldingRegisters;
		}
	}

CEMRegister CCoriolisModel::GetRegister(UINT uIndex)
{	
	return m_Registers.GetAt(uIndex);
	}

void CCoriolisModel::SetSelectedReg(CEMRegister const &Reg)
{
	m_SelectedRegister = Reg;
	}

CEMRegister CCoriolisModel::GetSelectedReg(void)
{
	return m_SelectedRegister;
	}

BOOL CCoriolisModel::AddRegister(CEMRegister Reg)
{
	if( m_Registers.Find(Reg) == NOTHING ) {
		
		m_Registers.Append(Reg);

		return TRUE;
		}

	return FALSE;
	}

BOOL CCoriolisModel::FindRegister(CString Text)
{
	if( Text.Find('[') < NOTHING ) {

		CString StartText = Text;

		StartText = StartText.ToUpper();

		CString TempText  = StartText.TokenFrom('[');

		CString RightTxt  = TempText.TokenLeft(']');

		UINT	RegNumber = tatoi(RightTxt);

		if( Text.Find(L"BIT") < NOTHING ) {

			CRegArray CoilRegs = GetUnifiedRegs(Coils, FALSE);

			for( UINT b = 0; b < CoilRegs.GetCount(); b++ ) {

				CEMRegister Reg =  CoilRegs.GetAt(b);

				if( Reg.m_uMin >= RegNumber && RegNumber <= Reg.m_uMax ) {

					GetAddressType(Text, Reg.m_uType);

					SetSelectedReg(Reg);

					return TRUE;
					}
				}
			}
		else {
			CRegArray HoldingRegs = GetUnifiedRegs(Registers, FALSE);

			for( UINT a = 0; a < HoldingRegs.GetCount(); a++ ) {
			
				CEMRegister Reg = HoldingRegs.GetAt(a);

				if( Reg.m_uMin >= RegNumber && RegNumber <= Reg.m_uMax ) {

					GetAddressType(Text, Reg.m_uType);

					SetSelectedReg(Reg);

					return TRUE;
					}
				}
			}
		}

	Text = Text.TokenLeft('.');

	CRegArray CoilRegs = GetUnifiedRegs(Coils, FALSE);

	for( UINT i = 0; i < CoilRegs.GetCount(); i++ ) {

		CEMRegister Reg =  CoilRegs.GetAt(i);

		if( Reg.m_ExpandPrefix == Text ) {

			SetSelectedReg(Reg);

			return TRUE;
			}
		}

	CRegArray HoldingRegs = GetUnifiedRegs(Registers, FALSE);

	for( UINT j = 0; j < HoldingRegs.GetCount(); j++ ) {

		CEMRegister Reg =  HoldingRegs.GetAt(j);

		if( Reg.m_ExpandPrefix == Text ) {

			SetSelectedReg(Reg);

			return TRUE;
			}
		}
	
	return FALSE;
	}

// Sorting

void CCoriolisModel::SortRegs(CRegArray &Registers, BOOL fSortAlpha)
{
	UINT uCount = Registers.GetCount();
	
	CEMRegister *pSort = new CEMRegister[ uCount ];

	for( UINT i = 0; i < uCount; i++ ) {

		pSort[i] = Registers.GetAt(i);
		}
	
	if( fSortAlpha ) {
		
		qsort(pSort, uCount, sizeof(*pSort), SortHelpAlpha);
		}
	else {
		qsort(pSort, uCount, sizeof(*pSort), SortHelpNum);
		}

	CRegArray Sorted;

	for( UINT n = 0; n < uCount; n++ ) {

		Sorted.Append(pSort[n]);
		}

	Registers = Sorted;

	delete [] pSort;
	
	pSort = 0;
	}

int CCoriolisModel::SortHelpAlpha(const void *a, const void *b) 
{
	CEMRegister *pLeft  = (CEMRegister *) a;
	CEMRegister *pRight = (CEMRegister *) b;

	return wstricmp(pLeft->m_Caption, pRight->m_Caption);
	}

int CCoriolisModel::SortHelpNum(const void *a, const void *b)
{
	CEMRegister *pLeft  = (CEMRegister *) a;
	CEMRegister *pRight = (CEMRegister *) b;

	int Left  = pLeft->m_uMin;
	int Right = pRight->m_uMin;

	return Left - Right;
	}

UINT CCoriolisModel::GetAddressType(CString Text, UINT Default) 
{
	CString TypeString = Text;

	if( TypeString.Find(L".BIT") < NOTHING ) {

		return addrBitAsBit;
		}

	if( TypeString.Find(L".WORD") < NOTHING ) {

		return addrWordAsWord;
		}

	if( TypeString.Find(L".REAL") < NOTHING ) {

		if( Default == addrWordAsWord ) {

			return addrWordAsReal;
			}

		if( Default == addrLongAsLong ) {

			return addrLongAsReal;
			}
		}

	if( TypeString.Find(L".LONG") < NOTHING ) {

		if( Default == addrWordAsWord ) {

			return addrWordAsLong;
			}

		if( Default == addrLongAsLong ) {

			return addrLongAsLong;
			}
		}

	return Default;
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Serial Driver Options
//

AfxImplementDynamicClass(CCoriolisSerialDriverOptions, CUIItem);

// Constructors

CCoriolisSerialDriverOptions::CCoriolisSerialDriverOptions(void)
{
	m_Protocol = 0;

	m_Track    = 0;

	m_Timeout  = 600;
	}

// Destructors

CCoriolisSerialDriverOptions::~CCoriolisSerialDriverOptions(void)
{

	}

// UI Managament

void CCoriolisSerialDriverOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag.IsEmpty() || Tag == "Protocol" ) {

		pHost->EnableUI("Track", !m_Protocol);
		}

	if( Tag == "Protocol" ) {

		BOOL fAscii = pItem->GetDataAccess("Protocol")->ReadInteger(pItem);

		m_Timeout   = fAscii ? 1000 : 600;

		pHost->UpdateUI("Timeout");
		} 
	
	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CCoriolisSerialDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Protocol));
	Init.AddByte(BYTE(m_Track));
	Init.AddWord(WORD(m_Timeout));	

	return TRUE;
	}

// Meta Data Creation

void CCoriolisSerialDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Protocol);
	Meta_AddInteger(Track);
	Meta_AddInteger(Timeout);
	}

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Base Device Options
//

AfxImplementDynamicClass(CCoriolisBaseDeviceOptions, CUIItem);

// Constructor

CCoriolisBaseDeviceOptions::CCoriolisBaseDeviceOptions(void)
{
	m_Type		= 0;

	m_Ping		= 30;

	m_Drop		= 1;

	m_fSortAlpha	= FALSE;

	LoadModels();

	InitRegister();
	}

// Destructor

CCoriolisBaseDeviceOptions::~CCoriolisBaseDeviceOptions(void)
{

	}

// UI Management

void CCoriolisBaseDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( Tag == "Type" ) {

		if( m_Type == Series3000NOC9Wire || m_Type == Series3000NOC4Wire ) {

			m_Ping = 413;
			}
		else {
			m_Ping = 30;
			}

		m_fSortAlpha = FALSE;

		CSystemItem *pSystem = GetDatabase()->GetSystemItem();

		if( pSystem ) {

			pSystem->Rebuild(1);
			}
		
		afxMainWnd->PostMessage(WM_COMMAND, IDM_VIEW_REFRESH);
		}

	CUIItem::OnUIChange(pHost, pItem, Tag);
	}

// Download Support

BOOL CCoriolisBaseDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddWord(WORD(m_Ping));
	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

void CCoriolisBaseDeviceOptions::InitRegisterData(CInitData &Init) 
{
	CRegArray CoilRegs	= GetUnifiedRegs(Coils, FALSE);

	DWORD uNumCoilRegs	= CoilRegs.GetCount();

	Init.AddWord(WORD(uNumCoilRegs));

	for( UINT n = 0; n < uNumCoilRegs; n++ ) {

		CEMRegister Reg = CoilRegs.GetAt(n);

		CAddress Addr;

		Addr.m_Ref	= 0;
		Addr.a.m_Table  = BYTE(Reg.m_uTable);
		Addr.a.m_Type   = BYTE(Reg.m_uType);
		Addr.a.m_Offset = WORD(Reg.m_uMin);
		Addr.a.m_Extra	= 0;

		Init.AddLong(DWORD(Addr.m_Ref));
		}

	CRegArray HoldingRegs = GetUnifiedRegs(Registers, FALSE);

	DWORD uNumHoldingRegs = HoldingRegs.GetCount();

	Init.AddWord(WORD(uNumHoldingRegs));

	for( UINT n = 0; n < uNumHoldingRegs; n++ ) {

		CEMRegister Reg = HoldingRegs.GetAt(n);

		CAddress Addr;

		Addr.m_Ref	= 0;
		Addr.a.m_Table  = BYTE(Reg.m_uTable);
		Addr.a.m_Type   = BYTE(Reg.m_uType);
		Addr.a.m_Offset = WORD(Reg.m_uMin);
		Addr.a.m_Extra	= 0;

		if( Reg.m_fIsString ) {

			WORD wExtra = (WORD(Reg.m_uMax - Reg.m_uMin));

			if( (wExtra & 0xF ) == wExtra) {

				Addr.a.m_Extra = wExtra & 0xF;
				}
			}

		Init.AddLong(DWORD(Addr.m_Ref));
		}
	}

void CCoriolisBaseDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Type);
	Meta_AddInteger(Drop);
	Meta_AddInteger(Ping);	
	}

// Manage Models

void CCoriolisBaseDeviceOptions::AddEMModel(CCoriolisModel Model)
{
	m_Models.Append(Model);
	}

UINT CCoriolisBaseDeviceOptions::GetEMModelCount(void)
{
	return m_Models.GetCount();
	}

CCoriolisModel CCoriolisBaseDeviceOptions::GetCurrentModel(void)
{
	return m_Models.GetAt(m_Type);
	}

CCoriolisModel	CCoriolisBaseDeviceOptions::GetEMModel(UINT uPos)
{
	return m_Models.GetAt(uPos);
	}

CRegArray CCoriolisBaseDeviceOptions::GetModelRegs(UINT uChannel)
{
	return GetCurrentModel().GetRegs(uChannel, m_fSortAlpha);
	}

CRegArray CCoriolisBaseDeviceOptions::GetCombinedRegs(UINT uRegType) 
{
	return GetCurrentModel().GetCombinedRegs(uRegType, m_fSortAlpha);
	}

UINT CCoriolisBaseDeviceOptions::GetRegisterTable(UINT uChannel) 
{
	return GetCurrentModel().GetCurrentTable(uChannel);
	}

CString CCoriolisBaseDeviceOptions::GetPrefixText(UINT uChannel) 
{
	return GetCurrentModel().GetNewPrefix(uChannel);
	}

CString	CCoriolisBaseDeviceOptions::GetCaptionText(UINT uChannel)
{
	return GetCurrentModel().GetNewCaption(uChannel);
	}

CEMRegister CCoriolisBaseDeviceOptions::GetSelectedReg(void)
{
	return m_SelectedReg;
	}

void CCoriolisBaseDeviceOptions::SetSelectedReg(CEMRegister const &Reg)
{
	m_SelectedReg = Reg;
	}

CString	CCoriolisBaseDeviceOptions::GetTypeString(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrLongAsLong:

			return L".LONG";

		case addrRealAsReal:
		case addrWordAsReal:
		case addrLongAsReal:

			return L".REAL";

		case addrWordAsWord:

			return L".WORD";

		case addrBitAsBit:

			return L".BIT";

		default:
			return L"";
		}
	}

UINT CCoriolisBaseDeviceOptions::GetAddressType(CString Text, UINT Default) 
{
	CString TypeString = Text;

	if( TypeString.Find(L".BIT") < NOTHING ) {

		return addrBitAsBit;
		}

	if( TypeString.Find(L".WORD") < NOTHING ) {

		return addrWordAsWord;
		}

	if( TypeString.Find(L".REAL") < NOTHING ) {

		if( Default == addrWordAsWord ) {

			return addrWordAsReal;
			}

		if( Default == addrLongAsLong ) {

			return addrLongAsReal;
			}
		}

	if( TypeString.Find(L".LONG") < NOTHING ) {

		if( Default == addrWordAsWord ) {

			return addrWordAsLong;
			}

		if( Default == addrLongAsLong ) {

			return addrLongAsLong;
			}
		}

	return Default;
	}

// Register Sorting

void CCoriolisBaseDeviceOptions::SetSort(BOOL fSortAlpha) 
{
	m_fSortAlpha = fSortAlpha;
	}

BOOL CCoriolisBaseDeviceOptions::GetSort(void) 
{
	return m_fSortAlpha;
	}

void CCoriolisBaseDeviceOptions::SortRegs(BOOL fSortAlpha) 
{
	GetCurrentModel().DoSortRegs(fSortAlpha);
	}

CRegArray CCoriolisBaseDeviceOptions::GetUnifiedRegs(UINT uRegType, BOOL fSortAlpha)
{
	return GetCurrentModel().GetUnifiedRegs(uRegType, fSortAlpha);
	}

void CCoriolisBaseDeviceOptions::LoadModels(void)
{
	CString ModelName[MAX_MODELS];

	ModelName[CDMAnalog]			= "CDM Analog";
	ModelName[Model2700Analog]		= "Model 2700 Analog / LF-Series LFT3";
	ModelName[MVD9739]			= "MVD 9739";
	ModelName[Model1500LFT2]		= "Model 1500 / LF-Series LFT2";
	ModelName[Model1700Analog]		= "Model 1700 Analog / LF-Series LFT1";
	ModelName[Model2500]			= "Model 2500 / LF-Series LFT5";
	ModelName[Series3000MVD]		= "Series 3000 MVD";
	ModelName[Series3000NOC4Wire]		= "Series 3000 NOC (4-wire)";
	ModelName[MVDDirectConnectEnhanced]	= "MVD Direct Connect with Enhanced Core Processor";
	ModelName[MVDDirectConnectStandard]	= "MVD Direct Connect with Standard Core Processor";
	ModelName[Model5700Config]		= "Model 5700 Config";
	ModelName[RFT9739]			= "RFT 9739";
	ModelName[FDMAnalog]			= "FDM Analog";
	ModelName[HFVMAnalog]			= "HFVM Analog";
	ModelName[SGMAnalog]			= "SGM Analog";
	ModelName[GDMAnalog]			= "GDM Analog";
	ModelName[FillingMassTransmitter]	= "Filling Mass Transmitter (FMT)";
	ModelName[Model1500Filling]		= "Model 1500 with the Filling and Dosing Application";
	ModelName[FVMAnalog]			= "FVM Analog";
	ModelName[Series3000NOC9Wire]		= "Series 3000 NOC (9-Wire)";

	for( UINT uDeviceModel = 0; uDeviceModel < MAX_MODELS; uDeviceModel++ ) {

		CCoriolisModel Model(ModelName[uDeviceModel], uDeviceModel);

		AddEMModel(Model);
		}
	}

void CCoriolisBaseDeviceOptions::InitRegister(void)
{
	m_SelectedReg.m_Caption		= "";
	m_SelectedReg.m_fReadOnly	= 0;
	m_SelectedReg.m_uMax		= 0;
	m_SelectedReg.m_uMin		= 0;
	m_SelectedReg.m_uTable		= 0;
	m_SelectedReg.m_uType		= 0;
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Serial Device Options
//

AfxImplementDynamicClass(CCoriolisSerialBaseDeviceOptions, CCoriolisBaseDeviceOptions);

// Constructor

CCoriolisSerialBaseDeviceOptions::CCoriolisSerialBaseDeviceOptions(void)
{
	
	}
	
// Destructor

CCoriolisSerialBaseDeviceOptions::~CCoriolisSerialBaseDeviceOptions(void)
{

	}

// UI Management

void CCoriolisSerialBaseDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		CCoriolisBaseDeviceOptions::OnUIChange(pHost, pItem, Tag);
		}
	}

BOOL CCoriolisSerialBaseDeviceOptions::OnLoadPages(CUIPageList * pList)
{
	CCoriolisBaseDeviceOptionsUIPage *pPage = New CCoriolisBaseDeviceOptionsUIPage(this, AfxThisClass());

	pList->Append(pPage);	
	
	return TRUE;
	}

BOOL CCoriolisSerialBaseDeviceOptions::MakeInitData(CInitData &Init)
{
	CCoriolisBaseDeviceOptions::MakeInitData(Init);	

	InitRegisterData(Init);

	return TRUE; 
	}

void CCoriolisSerialBaseDeviceOptions::AddMetaData(void)
{
	CCoriolisBaseDeviceOptions::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis TCP Base Device Options
//

AfxImplementDynamicClass(CCoriolisTCPBaseDeviceOptions, CCoriolisBaseDeviceOptions);

//Constructors

CCoriolisTCPBaseDeviceOptions::CCoriolisTCPBaseDeviceOptions(void)
{
	m_Addr		= DWORD(MAKELONG(MAKEWORD(21, 1), MAKEWORD(168, 192)));

	m_Addr2		= DWORD(MAKELONG(MAKEWORD(0, 0), MAKEWORD(0, 0)));

	m_Port		= 502;

	m_Keep		= TRUE;

	m_UsePing	= FALSE;

	m_Time1		= 5000;

	m_Time2		= 2500;

	m_Time3		= 200;
	}

// Destructor

CCoriolisTCPBaseDeviceOptions::~CCoriolisTCPBaseDeviceOptions(void)
{
	}

// Download Support

BOOL CCoriolisTCPBaseDeviceOptions::MakeInitData(CInitData &Init)
{
	CCoriolisBaseDeviceOptions::MakeInitData(Init);	

	Init.AddLong(m_Addr);
	Init.AddLong(m_Addr2);
	Init.AddWord(WORD(m_Port));
	Init.AddByte(BYTE(m_Keep));
	Init.AddByte(BYTE(m_UsePing));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	InitRegisterData(Init);

	return TRUE;
	}

void CCoriolisTCPBaseDeviceOptions::AddMetaData(void)
{
	CCoriolisBaseDeviceOptions::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Addr2);
	Meta_AddInteger(Port);
	Meta_AddInteger(Keep);
	Meta_AddInteger(UsePing);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

// UI Management

void CCoriolisTCPBaseDeviceOptions::OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag.IsEmpty() || Tag == "Keep" ) {

			pHost->EnableUI("Time3", !m_Keep);
			}
		
		CCoriolisBaseDeviceOptions::OnUIChange(pHost, pItem, Tag);
		}
	}

BOOL CCoriolisTCPBaseDeviceOptions::OnLoadPages(CUIPageList * pList)
{		
	CCoriolisBaseDeviceOptionsUIPage *pPage = New CCoriolisBaseDeviceOptionsUIPage(this, AfxThisClass());

	pList->Append(pPage);

	return TRUE;
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Base Driver
//

// Constructor

CCoriolisBaseDriver::CCoriolisBaseDriver() 
{
	m_uCount = 0;
	}

// Address Management

BOOL CCoriolisBaseDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	CCoriolisFullDlg Dlg(*this, Addr, pConfig, fPart);

	return Dlg.Execute();
	}

BOOL CCoriolisBaseDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	CCoriolisBaseDeviceOptions *pDevice = (CCoriolisBaseDeviceOptions *) pConfig;

	CEMRegister Reg;

	CCoriolisModel Model = pDevice->GetCurrentModel();

	if( !Text.IsEmpty() ) {

		if( Model.FindRegister(Text) ) {

			Reg = Model.GetSelectedReg();

			Addr.a.m_Offset = Reg.m_uMin;

			Addr.a.m_Table  = Reg.m_uTable;
			
			Addr.a.m_Type	= pDevice->GetAddressType(Text, Reg.m_uType);

			Addr.a.m_Extra	= 0;

			if( Reg.m_fIsString ) {

				WORD wExtra = (WORD(Reg.m_uMax - Reg.m_uMin));

				if( (wExtra & 0xF ) == wExtra ) {

					Addr.a.m_Extra = wExtra & 0xF;
					}
				}

			return TRUE;
			}
		}

	Error.Set(IDS_ERROR_OFFSETRANGE);

	return FALSE;
	}

BOOL CCoriolisBaseDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	CCoriolisBaseDeviceOptions *pDevice = (CCoriolisBaseDeviceOptions *) pConfig;

	CCoriolisModel Model	= pDevice->GetCurrentModel(); 

	UINT uTable		= Addr.a.m_Table;

	CRegArray Regs		= Model.GetUnifiedRegs(uTable, FALSE);
	
	CEMRegister Reg		= Regs.GetAt(0);

	for( UINT i = 0; i < Regs.GetCount(); i++ ) {

		CEMRegister findRegister = Regs.GetAt(i);

		if( Addr.a.m_Offset >= findRegister.m_uMin &&  Addr.a.m_Offset <= findRegister.m_uMax ) {

			Reg.m_ExpandPrefix = findRegister.m_ExpandPrefix;

			Reg.m_Caption	   = findRegister.m_Caption;

			Reg.m_uType        = pDevice->GetAddressType("", findRegister.m_uType);
				
			Text = CPrintf("%s", Reg.m_ExpandPrefix);
	
			if( Reg.m_uType != Addr.a.m_Type ) {

				Text = CPrintf("Available[%i]", Addr.a.m_Offset);

				Text += pDevice->GetTypeString(Reg.m_uType);
				}
			else {
				Text += pDevice->GetTypeString(Reg.m_uType);
				}

			return TRUE;
			}
		else {
			Reg.m_Caption = CPrintf("Unavailable[%i]", Addr.a.m_Offset);
	
			Text = CPrintf("%s", Reg.m_Caption);
			}
		}
		
	return FALSE;
	}

BOOL CCoriolisBaseDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	CCoriolisBaseDeviceOptions *pDevice = ( CCoriolisBaseDeviceOptions *) pConfig;

	CCoriolisModel Model = pDevice->GetCurrentModel();

	if( !pRoot ) {

		if( uItem == 0 ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart      = FALSE;
			Data.m_Name       = Model.GetCaptionText(Coils);
			Data.m_uData      = Coils;

			return TRUE;
			}

		if( uItem == 1 ) {

			Data.m_Addr.m_Ref = 0;
			Data.m_fPart      = FALSE;
			Data.m_Name       = Model.GetCaptionText(Registers);
			Data.m_uData      = Registers;

			return TRUE;
			}
		}
	else {
		if( pRoot->m_uData == NOTHING ) {

			return FALSE;
			}
		
		CRegArray Regs = pDevice->GetUnifiedRegs(pRoot->m_uData, FALSE); 

		if( uItem < Regs.GetCount() ) {
			
			CEMRegister Reg = Regs.GetAt(m_uCount);

			if( pRoot->m_uData == pDevice->GetRegisterTable(Reg.m_uTable) ) {

				Data.m_Addr.a.m_Table	= Reg.m_uTable;
				Data.m_Addr.a.m_Offset	= Reg.m_uMin;
				Data.m_Addr.a.m_Type	= Reg.m_uType;
				Data.m_fPart		= TRUE;
				Data.m_Name		= Reg.m_Caption;
				Data.m_uData		= NOTHING;
				Data.m_fRead		= Reg.m_fReadOnly;

				m_uCount++;

				return TRUE;
				}
			}

		m_uCount = 0;
		
		return FALSE;
		}
	
	m_uCount = 0;

	return FALSE;
	}

BOOL CCoriolisBaseDriver::IsReadOnly(CItem *pConfig, CAddress const &Addr)
{
	CCoriolisBaseDeviceOptions *pDevice = (CCoriolisBaseDeviceOptions *) pConfig;

	UINT uTable		= Addr.a.m_Table;

	CRegArray Regs = pDevice->GetUnifiedRegs(uTable, FALSE);

	for( UINT i = 0; i < Regs.GetCount(); i++ ) {

		CEMRegister Reg	= Regs.GetAt(i);

		if( Reg.m_uMin == Addr.a.m_Offset ) {

			return Reg.m_fReadOnly;
			}
		}

	return FALSE;
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Serial Driver
//


/// Driver Creation

ICommsDriver * Create_CoriolisSerialDriver(void)
{
	return New CCoriolisSerialDriver;
	}

// Constructor

CCoriolisSerialDriver::CCoriolisSerialDriver(void)
{
	m_wID		= 0x40D6;

	m_uType		= driverMaster;

	m_Manufacturer	= "Emerson Process";

	m_DriverName	= "Coriolis Meter Master";

	m_ShortName	= "Emerson Coriolis";

	m_DevRoot	= "DEV";

	m_Version	= "1.00";
	}

// Destructor

CCoriolisSerialDriver::~CCoriolisSerialDriver(void)
{

	}

// Confifuration

CLASS CCoriolisSerialDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CCoriolisSerialDriverOptions);
	}

CLASS CCoriolisSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCoriolisSerialBaseDeviceOptions);
	}

// Binding

UINT CCoriolisSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CCoriolisSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 9600;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityOdd;
	Serial.m_Mode     = modeTwoWire;
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis TCP Driver
//

// Driver Creation

ICommsDriver * Create_CoriolisTCPDriver(void)
{
	return New CCoriolisTCPDriver;
	}

// Constructor

CCoriolisTCPDriver::CCoriolisTCPDriver(void)
{
	m_wID		= 0x40D5;

	m_uType		= driverMaster;

	m_Manufacturer	= "Emerson Process";

	m_DriverName	= "Coriolis Meter TCP/IP Master";

	m_ShortName	= "Emerson Coriolis";

	m_DevRoot	= "DEV";

	m_Version	= "1.00";
	}

// Destructor

CCoriolisTCPDriver::~CCoriolisTCPDriver(void)
{

	}

// Configuration

CLASS CCoriolisTCPDriver::GetDriverConfig(void)
{
	return NULL;
	}

CLASS CCoriolisTCPDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CCoriolisTCPBaseDeviceOptions);
	}

// Binding

UINT CCoriolisTCPDriver::GetBinding(void) 
{
	return bindEthernet;
	}

void CCoriolisTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount  = 1;

	Ether.m_UDPCount  = 0;
	}

//////////////////////////////////////////////////////////////////////////
//
//  Device Page UI
//

AfxImplementRuntimeClass(CCoriolisBaseDeviceOptionsUIPage, CUIPage);


CCoriolisBaseDeviceOptionsUIPage::CCoriolisBaseDeviceOptionsUIPage (CCoriolisBaseDeviceOptions *pDevice, CLASS DeviceClass)
{			
	m_pDevice	= pDevice;

	m_Class		= DeviceClass;
	}

// Load View

BOOL CCoriolisBaseDeviceOptionsUIPage::LoadIntoView(IUICreate *pView, CItem *pItem)
{	
	pView->StartGroup(L"Device Options", 1);

	LoadModels(pView, pItem);

	pView->AddUI(pItem, L"root", L"Drop");

	pView->AddUI(pItem, L"root", L"Ping");

	pView->EndGroup(TRUE);

	CUIStdPage *pPage = New CUIStdPage(m_Class, 0);

	pPage->LoadIntoView(pView, m_pDevice);
	
	delete pPage;

	return TRUE;
	}

// Load UI

void CCoriolisBaseDeviceOptionsUIPage::LoadModels(IUICreate *pView, CItem *pItem)
{
	CString Form = L"Error";

	UINT uCount  = m_pDevice->GetEMModelCount();

	if( uCount > 0 ) {

		Form = L"";

		for( UINT n = 0; n < uCount; n++ ) {

			Form += m_pDevice->GetEMModel(n).GetName();

			Form += L"|";	
			}
		}

	CUIData Data;

	Data.m_Tag	 = L"Type";

	Data.m_Label	 = L"Emerson Model";

	Data.m_ClassText = AfxNamedClass(L"CUITextEnum");

	Data.m_ClassUI	 = AfxNamedClass(L"CUIDropDown");

	Data.m_Format	 = Form;

	Data.m_Tip	 = L"The selected Emerson Coriolis Model.";

	pView->AddUI(pItem, L"root", &Data);
	}

//////////////////////////////////////////////////////////////////////////
//
//  Coriolis Dialog Management
//

AfxImplementRuntimeClass(CCoriolisFullDlg, CStdDialog);

// Message Map

AfxMessageMap(CCoriolisFullDlg, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)

	AfxDispatchCommand(IDOK, OnOkay)
	
	AfxDispatchNotify(1001, LBN_SELCHANGE,	OnSelChanged)
	AfxDispatchNotify(4001, LBN_SELCHANGE,	OnTypeChanged)
	AfxDispatchNotify(1001, LBN_DBLCLK,	OnDblClk)
	AfxDispatchNotify(1003, CBN_SELCHANGE,  OnChannelChanged)
	AfxDispatchNotify(1004, CBN_SELCHANGE,  OnSortAlpha)

	AfxMessageEnd(CCoriolisFullDlg)
	};

// Constructor

CCoriolisFullDlg::CCoriolisFullDlg(CCoriolisBaseDriver &Driver, CAddress &Addr, CItem *pConfig, BOOL fPart)
	:m_Driver(Driver), m_Addr(Addr)
{
	SetName(fPart ? L"CCoriolisPartialDlg" : L"CCoriolisFullDlg");

	m_pDevice	= (CCoriolisBaseDeviceOptions *) pConfig;

	m_uChan		= Coils;

	m_uType         = 0;

	m_uInitSel	= 0;
	}

// Destructor

CCoriolisFullDlg::~CCoriolisFullDlg(void)
{

	}

// Message Handlers

BOOL CCoriolisFullDlg::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	if( m_Addr.m_Ref ) {

		UINT uTable = m_Addr.a.m_Table;

		m_uType     = m_Addr.a.m_Type;

		BOOL fSort  = m_pDevice->GetSort();
		
		UINT uTotalRegs = m_pDevice->GetUnifiedRegs(uTable, fSort).GetCount();

		for( UINT i = 0; i < uTotalRegs; i++ ) {

			CEMRegister curRegister =  m_pDevice->GetUnifiedRegs(uTable, fSort).GetAt(i);

			if( curRegister.m_uMin == m_Addr.a.m_Offset ) {

				m_Reg = curRegister;

				m_uChan = m_pDevice->GetRegisterTable(m_Reg.m_uTable);

				m_pDevice->SetSelectedReg(m_Reg);

				m_uInitSel = i;

				break;
				}
			}
		}
	else {
		BOOL fSort  = m_pDevice->GetSort();

		m_Reg =  m_pDevice->GetUnifiedRegs(m_uChan, fSort).GetAt(0);

		m_pDevice->SetSelectedReg(m_Reg);

		m_uInitSel = 0;
		}

	LoadChannels();

	LoadRegs();

	LoadTypes();

	ShowType();

	ShowDetails();

	ShowAddress();

	return TRUE;
	}

BOOL CCoriolisFullDlg::OnOkay(UINT uID)
{
	CError Error;

	CString MinText = GetDlgItem(3004).GetWindowText();

	m_Reg = m_pDevice->GetSelectedReg();

	m_Reg.m_uMin = tatoi(MinText);

	m_pDevice->SetSelectedReg(m_Reg);

	if( m_Driver.ParseAddress(Error, m_Addr, m_pDevice, m_Reg.m_ExpandPrefix) ) {

		EndDialog(TRUE);

		return TRUE;
		}

	if( !Error.IsOkay() ) {

		Error.Show(ThisObject);

		return FALSE;
		}

	EndDialog(FALSE);
	
	return FALSE;	
	}

void CCoriolisFullDlg::OnDblClk(UINT uID, CWnd &Wnd)
{
	SendMessage(WM_COMMAND, IDOK);
	}

void CCoriolisFullDlg::OnSelChanged(UINT uID, CWnd &Wnd)
{
	CListBox &ListBox	= (CListBox &) GetDlgItem(1001);

	UINT uSel = 0;

	if( !m_uInitSel ) {

		uSel		= ListBox.GetCurSel();
		}
	else {
		uSel		= m_uInitSel;

		m_uInitSel	= 0;
		}

	BOOL fSort		= m_pDevice->GetSort();

	m_Reg = m_pDevice->GetUnifiedRegs(m_uChan, fSort).GetAt(uSel);
	
	m_pDevice->SetSelectedReg(m_Reg);

	LoadTypes();

	CListBox &TypeBox = (CListBox &) GetDlgItem(4001);

	TypeBox.SetCurSel(0);

	ShowDetails();

	DoEnables();
	}

void CCoriolisFullDlg::OnTypeChanged(UINT uID, CWnd &Wnd)
{	
	AppendType();
	}
		
void CCoriolisFullDlg::OnChannelChanged(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1003);

	m_uChan		 = Combo.GetCurSelData();

	LoadRegs();
	}

void CCoriolisFullDlg::OnSortAlpha(UINT uID, CWnd &Wnd)
{
	CComboBox &Combo = (CComboBox &) GetDlgItem(1004);

	BOOL fSort	 = Combo.GetCurSel();

	m_pDevice->SetSort(fSort);

	LoadRegs();
	}

// Implementation

void CCoriolisFullDlg::LoadRegs(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(1001);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	CRegArray Regs;

	BOOL fSort = m_pDevice->GetSort();

	Regs = m_pDevice->GetUnifiedRegs(m_uChan, fSort);

	for( UINT n = 0; n < Regs.GetCount(); n++ ) {

		CEMRegister Reg	  = Regs.GetAt(n);

		BOOL        fShow = (m_pDevice->GetRegisterTable(Reg.m_uTable) == m_uChan);
			
		if( fShow ) {

			if( Reg.m_Caption.GetLength() >= 75 ) {

				CleanText(Reg.m_Caption);
				}

			Box.AddString(Reg.m_Caption);	
			}
		}

	UINT uSel = Regs.Find(m_Reg);

	if( uSel != NOTHING ) {

		Box.SetCurSel(uSel);
		}
	else {
		Box.SetCurSel(0);
		}

	OnSelChanged(0, ThisObject);

	Box.Invalidate(TRUE);

	Box.SetRedraw(TRUE);
	}

void CCoriolisFullDlg::LoadTypes(void)
{
	CListBox &Box = (CListBox &) GetDlgItem(4001);

	Box.SetRedraw(FALSE);

	Box.ResetContent();

	if( m_Reg.m_uTable ) {

		switch( m_Reg.m_uType ) {

			case addrBitAsBit:

				Box.AddString(CString(IDS_SPACE_BAB), addrBitAsBit);

				break;

			case addrWordAsWord:

				Box.AddString(CString(IDS_SPACE_WAW), addrWordAsWord);

				break;

			case addrWordAsReal:

				Box.AddString(CString(IDS_SPACE_WAR), addrWordAsReal);

				break;

			case addrRealAsReal:

				Box.AddString(CString(IDS_SPACE_RAR), addrRealAsReal);
				
				break;

			case addrWordAsLong:

				Box.AddString(CString(IDS_SPACE_WAL), addrWordAsLong);

				break;

			case addrLongAsLong:

				Box.AddString(CString(IDS_SPACE_LAL), addrLongAsLong);

				break;
			}
		}

	Box.Invalidate(TRUE);

	Box.SetRedraw(TRUE);
	}

void CCoriolisFullDlg::LoadChannels(void)
{
	CComboBox &ChannelCombo = (CComboBox &) GetDlgItem(1003);
	CComboBox &SortCombo    = (CComboBox &) GetDlgItem(1004);

	ChannelCombo.AddString("Coils",		        Coils);
	ChannelCombo.AddString("Registers",		Registers);

	SortCombo.AddString("Sort - 0-n", 0);
	SortCombo.AddString("Sort - A-Z", 1);

	for( UINT n = 0; n < ChannelCombo.GetCount(); n++ ) {

		if( ChannelCombo.GetItemData(n) == m_uChan ) {

			ChannelCombo.SetCurSel(n);

			break;
			}
		}

	BOOL fSort = m_pDevice->GetSort();

	SortCombo.SetCurSel(fSort);
	}

void CCoriolisFullDlg::AppendType(void)
{
	CListBox &Box	= (CListBox &) GetDlgItem(4001);

	UINT uType	= Box.GetCurSelData();

	CString Type	= GetTypeString(uType);

	if( !Type.IsEmpty() ) {

		GetDlgItem(2005).SetWindowText(L"." + Type);

		m_Reg		=  m_pDevice->GetSelectedReg();

		m_Reg.m_uType	= uType;

		m_pDevice->SetSelectedReg(m_Reg);
		}
	else {
		m_Reg		=  m_pDevice->GetSelectedReg();

		m_Reg.m_uType	= uType;

		m_pDevice->SetSelectedReg(m_Reg);

		GetDlgItem(2005).SetWindowText(L"");
		}
	}

void CCoriolisFullDlg::ShowDetails(void)
{
	if( m_Reg.m_uTable ) {

		CString Text = m_Reg.m_ExpandPrefix;

		if( Text.GetLength() >= 75 ) {

			CleanText(Text);
			}

		GetDlgItem(2004).SetWindowText(Text);

		GetDlgItem(3004).SetWindowText(CPrintf("%u", m_Reg.m_uMin));

		GetDlgItem(3006).SetWindowText(CPrintf("%u", m_Reg.m_uMax));

		GetDlgItem(3008).SetWindowText("10");
		}
	else {
		GetDlgItem(2001).SetWindowText("");

		GetDlgItem(2004).SetWindowText("");

		GetDlgItem(3004).SetWindowText("");

		GetDlgItem(3006).SetWindowText("");

		GetDlgItem(3008).SetWindowText("");
		}

	ShowType();

	AppendType();
	}

void CCoriolisFullDlg::ShowAddress(void)
{
	CListBox &TypeBox = (CListBox &) GetDlgItem(4001);
		
	TypeBox.SetRedraw(FALSE);

	if( m_Addr.m_Ref ) {

		for( UINT n = 0; n < TypeBox.GetCount(); n++ ) {

			if( TypeBox.GetItemData(n) == m_uType ) {

				TypeBox.SetCurSel(n);

				break;
				}
			}
		}
	else {
		TypeBox.SetCurSel(0);
		}
			
	AppendType();

	TypeBox.Invalidate(TRUE);

	TypeBox.SetRedraw(TRUE);
	}

void CCoriolisFullDlg::ShowType(void)
{
	CString Type;

	if( m_Reg.m_uTable ) {

		switch( m_Reg.m_uType ) {

			case addrBitAsBit:

				Type = IDS_SPACE_BIT;

				break;

			case addrWordAsWord:
			
				Type = IDS_SPACE_WORD;

				break;

			case addrWordAsReal:
			case addrRealAsReal:
			case addrLongAsReal:

				Type = IDS_SPACE_REAL;

				break;

			case addrLongAsLong:
			case addrWordAsLong:

				Type = IDS_SPACE_LONG;

				break;
			}
		}

	GetDlgItem(3002).SetWindowText(Type);
	}

void CCoriolisFullDlg::DoEnables(void)
{
	}

// Text Handling

CString CCoriolisFullDlg::GetTypeString(UINT uType)
{
	switch( uType ) {

		case addrWordAsLong:
		case addrLongAsLong:

			return L"LONG";

		case addrRealAsReal:
		case addrWordAsReal:
		case addrLongAsReal:

			return L"REAL";

		default:
			return L"";
		}
	}

CString CCoriolisFullDlg::GetComboText(UINT uChannel) 
{
	CCoriolisModel Model	= m_pDevice->GetCurrentModel();

	CString ComboText = Model.GetPrefixText(uChannel) + " - " + Model.GetCaptionText(uChannel);

	return ComboText;
	}

void CCoriolisFullDlg::CleanText(CString &Text) 
{
	UINT uStartIndex = Text.Find('(');

	UINT uEndIndex   = Text.Find(')');

	if( uStartIndex != NOTHING || uEndIndex != NOTHING ) {

		Text.Delete(uStartIndex, (uEndIndex + 1 - uStartIndex));
		}
	}

// End of File
