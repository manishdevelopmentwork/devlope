#include "intern.hpp"

#include "tosht2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Toshiba T2 Master Serial Driver - Supports Toshiba T-Series and EX-Series PLCs.
//

// Instantiator

INSTANTIATE(CToshT2MasterDriver);

// Constructor

CToshT2MasterDriver::CToshT2MasterDriver(void)
{
	m_Ident = DRIVER_ID;

	}

// Destructor

CToshT2MasterDriver::~CToshT2MasterDriver(void)
{
	}

// Configuration

void MCALL CToshT2MasterDriver::Load(LPCBYTE pData)
{
	}
	
void MCALL CToshT2MasterDriver::CheckConfig(CSerialConfig &Config)
{
	Make485(Config, TRUE);
	}
	
// Management

void MCALL CToshT2MasterDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeSingleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CToshT2MasterDriver::Open(void)
{	
	
	}

// Device

CCODE MCALL CToshT2MasterDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop	  = GetByte(pData);

			m_pCtx->m_bSeries = GetByte(pData);

			m_pCtx->m_fCheck  = TRUE;
			
			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;

	}

CCODE MCALL CToshT2MasterDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

BOOL CToshT2MasterDriver::Start(UINT &uCount, UINT uType) 
{
	if( CToshibaBaseMasterDriver::Start(uCount, uType) ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CToshT2MasterDriver::Transact(void) 
{
	if( Send( ) && Recv( ) ) {

		return TRUE;
	
		}
	
	return FALSE;
	}

BOOL CToshT2MasterDriver::Send(void)
{
	m_pData->ClearRx();

	m_pData->Write(m_bTxBuff, m_uPtr, FOREVER);

	return TRUE;

	}

BOOL CToshT2MasterDriver::Recv(void)
{
	UINT uPtr = 0;

	UINT uState = 0;

	UINT uData = 0;

	UINT uTimer = 0;

	SetTimer(FRAME_TIMEOUT);

	while( (uTimer = GetTimer()) ) {
	
		if( (uData = m_pData->Read(uTimer)) == NOTHING ) {

			continue;
			}

		switch( uState ) {
		
			case 0:
				if( uData == '(' ) {

					uPtr = 0;

					uState++;
					}
				break;
				
			case 1:
				if( uData == ')' ) {
					
					m_bRxBuff[uPtr++] = 0;
					
					m_uRxLen = uPtr;

					uState++;
					}
				else {
                                        if( uPtr == sizeof(m_bRxBuff) ) {

						return FALSE;
						}
					
					m_bRxBuff[uPtr++] = uData;
					}
				break;
				
			case 2:
				return CheckFrame(uPtr);
			}
		}
	
	return FALSE;

	}

BOOL CToshT2MasterDriver::CheckFrame(UINT uPtr)
{
	if( m_bRxBuff[uPtr - 4] == '&' ) {
	
		BYTE bCheck = '(';
	
		for( UINT u = 0; u <= uPtr - 4; u++ ) {

			bCheck += m_bRxBuff[u];
			}
			
		if( m_bRxBuff[uPtr - 3] != m_pHex[bCheck / 16] ) {

			return FALSE;
			}
			
		if( m_bRxBuff[uPtr - 2] != m_pHex[bCheck % 16] ) {

			return FALSE;
			}
		}

	if( m_bRxBuff[4] == 'E' ) {

		return FALSE;
		}

	return TRUE;
	}

// End of File
