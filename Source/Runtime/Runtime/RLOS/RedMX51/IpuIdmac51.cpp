
#include "Intern.hpp"

#include "Ipu51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Hardware Drivers
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "IpuIdmac51.hpp"

//////////////////////////////////////////////////////////////////////////
//
// iMX51 Image Processing Unit DMA Control Module
//

// Register Access

#define Reg(x)	    (m_pBase[reg##x])

#define RegN(x, n)  (m_pBase[reg##x + n])

// Constructor

CIpuImageDma51::CIpuImageDma51(CIpu51 *pIpu)
{
	m_pBase = PVDWORD(ADDR_IPUEX) + CIpu51::regIdmac;
	
	m_pIpu  = pIpu;
	}

// Interface

void CIpuImageDma51::SetChanEnable(UINT uChan, bool fSet)
{
	UINT nReg = uChan / 32;

	UINT nBit = uChan % 32;

	SetBit(RegN(ChanEnable, nReg), nBit, fSet);
	}

void CIpuImageDma51::SetChanSepAlpha(UINT uChan, bool fSet)
{
	SetBit(Reg(SepAlpha), uChan, fSet);
	}

void CIpuImageDma51::SetChanAltSepAlpha(UINT uChan, bool fSet)
{
	SetBit(Reg(AltSepAlpha), uChan, fSet);
	}

void CIpuImageDma51::SetChanPriority(UINT uChan, bool fSet)
{
	UINT nReg = uChan / 32;

	UINT nBit = uChan % 32;

	SetBit(RegN(ChanPriority, nReg), nBit, fSet);
	}

void CIpuImageDma51::SetChanWatermark(UINT uChan, bool fSet)
{
	UINT nReg = uChan / 32;

	UINT nBit = uChan % 32;

	SetBit(RegN(WmEnable, nReg), nBit, fSet);
	}

void CIpuImageDma51::SetChanLock(UINT uChan, bool fSet)
{
	UINT nReg = uChan / 32;

	UINT nBit = uChan % 32;

	SetBit(RegN(LockEnable, nReg), nBit, fSet);
	}

void CIpuImageDma51::SetChanAltAddr(UINT uChan, UINT uAddr)
{
	switch( uChan ) {

		case 4:	
			SetChanAltAddr(RegN(SubAddr, 0), uAddr, 0);
			break;

		case 5:
			SetChanAltAddr(RegN(SubAddr, 0), uAddr, 8);
			break;

		case 6:
			SetChanAltAddr(RegN(SubAddr, 0), uAddr, 16);
			break;

		case 7:
			SetChanAltAddr(RegN(SubAddr, 0), uAddr, 24);
			break;

		case 23:
			SetChanAltAddr(RegN(SubAddr, 1), uAddr, 0);
			break;

		case 24:
			SetChanAltAddr(RegN(SubAddr, 1), uAddr, 8);
			break;

		case 29:
			SetChanAltAddr(RegN(SubAddr, 1), uAddr, 16);
			break;

		case 33:
			SetChanAltAddr(RegN(SubAddr, 1), uAddr, 24);
			break;

		case 41:
			SetChanAltAddr(RegN(SubAddr, 2), uAddr, 0);
			break;

		case 51:
			SetChanAltAddr(RegN(SubAddr, 2), uAddr, 8);
			break;

		case 52:
			SetChanAltAddr(RegN(SubAddr, 2), uAddr, 16);
			break;

		case 9:
			SetChanAltAddr(RegN(SubAddr, 3), uAddr, 0);
			break;

		case 10:
			SetChanAltAddr(RegN(SubAddr, 3), uAddr, 8);
			break;

		case 13:
			SetChanAltAddr(RegN(SubAddr, 3), uAddr, 16);
			break;

		case 27:
			SetChanAltAddr(RegN(SubAddr, 3), uAddr, 24);
			break;

		case 28:
			SetChanAltAddr(RegN(SubAddr, 4), uAddr, 0);
			break;

		case 8:
			SetChanAltAddr(RegN(SubAddr, 4), uAddr, 8);
			break;

		case 21:
			SetChanAltAddr(RegN(SubAddr, 4), uAddr, 16);
			break;
		}
	}

UINT CIpuImageDma51::GetChanAltAddr(UINT uChan)
{
	switch( uChan ) {

		case 4:	 return GetChanAltAddr(RegN(SubAddr, 0),  0);
			
		case 5:  return GetChanAltAddr(RegN(SubAddr, 0),  8);

		case 6:  return GetChanAltAddr(RegN(SubAddr, 0), 16);

		case 7:  return GetChanAltAddr(RegN(SubAddr, 0), 24);

		case 23: return GetChanAltAddr(RegN(SubAddr, 1),  0);

		case 24: return GetChanAltAddr(RegN(SubAddr, 1),  8);

		case 29: return GetChanAltAddr(RegN(SubAddr, 1), 16);

		case 33: return GetChanAltAddr(RegN(SubAddr, 1), 24);

		case 41: return GetChanAltAddr(RegN(SubAddr, 2),  0);

		case 51: return GetChanAltAddr(RegN(SubAddr, 2),  8);

		case 52: return GetChanAltAddr(RegN(SubAddr, 2), 16);

		case 9:  return GetChanAltAddr(RegN(SubAddr, 3),  0);

		case 10: return GetChanAltAddr(RegN(SubAddr, 3),  8);

		case 13: return GetChanAltAddr(RegN(SubAddr, 3), 16);

		case 27: return GetChanAltAddr(RegN(SubAddr, 3), 24);

		case 28: return GetChanAltAddr(RegN(SubAddr, 4),  0);

		case 8:  return GetChanAltAddr(RegN(SubAddr, 4),  8);

		case 21: return GetChanAltAddr(RegN(SubAddr, 4), 16);
		}

	return 0;
	}

void CIpuImageDma51::SetChanBandMode(UINT uChan, bool fSet)
{
	UINT nReg = uChan / 32;

	UINT nBit = uChan % 32;

	SetBit(RegN(BmEnable, nReg), nBit, fSet);
	}

void CIpuImageDma51::SetCroll(UINT uSet, UINT x, UINT y)
{
	x &= 0xFFF;

	y &= 0x7FF;

	RegN(ScrollCoord, uSet) = MAKELONG(y, x); 	
	}

bool CIpuImageDma51::GetChanBusy(UINT uChan)
{
	UINT nReg = uChan / 32;

	UINT nBit = uChan % 32;

	return RegN(ChanBusy, nReg) & Bit(nBit);
	}

// Implementation

void CIpuImageDma51::SetChanAltAddr(DWORD volatile &Reg, DWORD dwData, UINT uShift)
{
	DWORD dwMask = 0x7F;

	Reg &= ~(dwMask << uShift);

	Reg |=  ((dwData & dwMask) << uShift);
	}

UINT CIpuImageDma51::GetChanAltAddr(DWORD volatile &Reg, UINT uShift)
{
	return (Reg >> uShift) & 0x7F;
	}

// End of File
