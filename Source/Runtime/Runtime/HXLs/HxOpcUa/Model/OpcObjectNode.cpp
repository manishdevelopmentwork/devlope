
#include "Intern.hpp"

#include "OpcObjectNode.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Object Node
//

// Constructors

COpcObjectNode::COpcObjectNode(COpcDataModel *pModel, UINT Namespace, UINT Value, UINT Notifier) : COpcNode(pModel, classObject, Namespace, Value)
{
	m_Notifier = Notifier;
	}

COpcObjectNode::COpcObjectNode(COpcDataModel *pModel, UINT Namespace, CString const &Value, UINT Notifier) : COpcNode(pModel, classObject, Namespace, Value)
{
	m_Notifier = Notifier;
	}

COpcObjectNode::COpcObjectNode(COpcDataModel *pModel, UINT Namespace, CGuid const &Value, UINT Notifier) : COpcNode(pModel, classObject, Namespace, Value)
{
	m_Notifier = Notifier;
	}

COpcObjectNode::COpcObjectNode(COpcDataModel *pModel, UINT Namespace, CByteArray const &Value, UINT Notifier) : COpcNode(pModel, classObject, Namespace, Value)
{
	m_Notifier = Notifier;
	}

COpcObjectNode::COpcObjectNode(COpcObjectNode const &That) : COpcNode(That)
{
	m_Notifier = That.m_Notifier;
	}

// Assignment

COpcObjectNode COpcObjectNode::operator = (COpcObjectNode const &That)
{
	COpcNode::operator = (That);

	m_Notifier = That.m_Notifier;

	return ThisObject;
	}

// End of File
