
#include "intern.hpp"

#include "legacy.h"

#include "dio01.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Digital Module Logic Configuration
//
// Copyright (c) 1993-2009 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

#pragma warning(disable: 4288)

//////////////////////////////////////////////////////////////////////////
//
// Digital Module Logic Configuration
//

void CDIO14LogicWnd::InitUI(void)
{
	for(UINT i = 0; i < NUM_PINS; i++) m_PinSel[i] = 0;
	
	m_EditMode = NULL;
	
	m_ConnectMode = NULL;

	m_DragFlag = FALSE;

	m_StartMove = FALSE;

	m_PSDraw = PS_INIT;
	
	m_SelectedWire = 0;
		
	m_SelectedSegment = 0;

	m_SelectedSymbol = 0;

	m_PinSelActive = FALSE;

	m_InfoBal.Pos.x = (UINT)1e6;

	m_InfoBal.Pos.y = (UINT)1e6;

	m_UlistIndex = 0;
	
	m_UlistLimit = 0;

	m_RlistLimit = 0;

	m_RefSelected = FALSE;
	
	//Setup buttons

	m_Button[B_UNDO].Left   = 400;
	m_Button[B_UNDO].Top    = 32;
	m_Button[B_UNDO].Width  = 23;
	m_Button[B_UNDO].Height = 23;
	m_Button[B_UNDO].State  = FALSE;

	m_Button[B_REDO].Left   = 423;
	m_Button[B_REDO].Top    = 32;
	m_Button[B_REDO].Width  = 23;
	m_Button[B_REDO].Height = 23;
	m_Button[B_REDO].State  = FALSE;

	m_Button[B_REFRESH].Left   = 461;
	m_Button[B_REFRESH].Top    = 32;
	m_Button[B_REFRESH].Width  = 23;
	m_Button[B_REFRESH].Height = 23;
	m_Button[B_REFRESH].State  = FALSE;

	StartSimTimer();
	}

// General Message Handlers

BOOL CDIO14LogicWnd::OnEraseBkGnd(CDC &DC)
{		
	CRect Rect = GetClientRect();

	DC.FillRect(Rect, afxBrush(TabFace));

	EraseLogicScreen(DC);
	
	//Reference Symbol window
	Rect.left   = 0;
	Rect.top    = 24;
	Rect.right  = 390;
	Rect.bottom = 62;

	DC.FillRect(Rect, afxBrush(BLACK));
		
	DC.DrawEdge(Rect, EDGE_RAISED, BF_RECT);
	
	DrawReferenceSyms(DC);

	RedrawLogicScreen(DC);

	RedrawButtons(DC);

	return TRUE;
	}

//Graphic operation message handlers
	
void CDIO14LogicWnd::OnMouseMove(UINT uFlags, CPoint Pos)
{
	UINT i;
	
	CPoint pt;

	CClientDC DC(ThisObject);
	
	//Continue with in-progress wire connection
	if(m_ConnectMode == CONNECT) {
		
		ConnectMode(Pos,DC);
		}

	//Continue with in-progress vertical segment dragging
	if(m_EditMode == DRAG_SEGMENT) {
		
		DragSegMode(Pos,DC);
		
		return;
		}

	//Display pin selects and info baloons
	if(m_EditMode == NULL) {
		
		InfoBaloon(0,0,Pos,IB_STOP);
				
		//Test for wire info baloons
		for(i = 1; i < m_pItem->m_NumWires; i++) {
			
			if(TestWireZoneA(i,Pos.x,Pos.y)) {
				
				m_SelectedWire = i;
				
				DrawWireSelect(m_SelectedWire,TRUE,0,DC);
				
				InfoBaloon(IWIRE,0,Pos,IB_START);
				
				break;
				}
			else {
				DrawWireSelect(m_SelectedWire,FALSE,0,DC);
				
				m_SelectedWire = 0;
				}
			
			if(TestWireZoneB(i,Pos.x,Pos.y)) {

				m_SelectedSegment = i;
				
				DrawWireSelect(m_SelectedSegment,TRUE,1,DC);
								
				InfoBaloon(ISEG,0,Pos,IB_START);
									
				break;
				}
			else {
				DrawWireSelect(m_SelectedSegment,FALSE,1,DC);
				
				m_SelectedSegment = 0;
				}
			}
		
		//Test for symbol info baloons
		if(CheckUserSymbolSel(Pos) || CheckRefSymbolSel(Pos,FALSE)) {
					
			InfoBaloon(ISYMBOL,m_SelectedSymbol,Pos,IB_START);
			}
				
		//Test for button activity
		UINT Button;

		if((Button = CheckButtonSel(Pos,FALSE,DC)) != NO_BUTTON) {
						
			for(UINT i = 0; i < NUM_BUTTONS; i++) m_Button[i].Hover = FALSE;
			
			m_ButtonPrev = Button;
			
			m_Button[Button].Hover = TRUE;
			
			Pos.y = m_Button[Button].Top + 30;
			
			InfoBaloon(IBUTTON,Button,Pos,IB_START);

			RedrawButtons(DC);
			}
		else {
			if(m_ButtonPrev < NUM_BUTTONS) {
			
				if(m_Button[m_ButtonPrev].Hover == TRUE) {

					m_Button[m_ButtonPrev].Hover = FALSE;
					
					RedrawButtons(DC);
					}
				}
			}
		
		if(m_SelectedSymbol) {

			if(m_pItem->m_Sym[m_SelectedSymbol].Type == JCT) return;
		
			//Display input pin select.
			for(i = 1; i < m_pItem->m_Sym[m_SelectedSymbol].NumInputs+1; i++) {
								
				if(m_pItem->m_Sym[m_SelectedSymbol].WireEnd[i] != 0) continue;
				
				if(TestInputZone(m_SelectedSymbol,i-1,Pos.x,Pos.y)) {
									
					m_PinSel[i] = m_SelectedSymbol;
											
					pt = GetPinEnd(m_SelectedSymbol,i);
					
					DrawPinSelect(pt.x,pt.y,TRUE,DC);
					
					InfoBaloon(0,0,Pos,IB_STOP);
					
					return;
					}
				}

			//Display output pin select.
			if(TestOutputZone(m_SelectedSymbol,Pos.x,Pos.y) && 
			   m_pItem->m_Sym[m_SelectedSymbol].WireEnd[0] == 0) {
				
				if(m_pItem->m_Sym[m_SelectedSymbol].NumOutputs == 0) return;

				m_PinSel[0] = m_SelectedSymbol;
										
				pt = GetPinEnd(m_SelectedSymbol,0);
										
				DrawPinSelect(pt.x,pt.y,TRUE,DC);
				
				InfoBaloon(0,0,Pos,IB_STOP);
				
				return;
				}
			}
			
		//Clear pin selects
		for(i = 0; i < NUM_PINS; i++) {		
		
			if(m_PinSel[i]) {
		
				pt = GetPinEnd(m_PinSel[i],i);
				
				DrawPinSelect(pt.x,pt.y,FALSE,DC);

				Pos.x = m_pItem->m_Sym[m_PinSel[i]].Left;

				Pos.y = m_pItem->m_Sym[m_PinSel[i]].Top;
		
				m_PinSel[i] = 0;
																
				RedrawLogicScreen(DC);
				
				return;
				}
			}

		return;
		}

	Pos = ForceGrid(Pos);
	
	//Continue with in-progress symbol placement
	if(m_EditMode == PLACE) {
		
		if(LimitCursLoc(m_pItem->m_NSI,Pos)) return;
		
		DrawSymbol(m_pItem->m_NSI,Pos,SMOVE,DC);
		
		return;
		}
		
	//Continue with in-progress symbol dragging
	if(m_EditMode == DRAG_SYM && m_GridPosChange == TRUE) {
								
		UINT sym = m_SelectedSymbol;
		
		pt.x = Pos.x - m_CursDeltaX;
		
		pt.y = Pos.y - m_CursDeltaY;
		
		if(LimitCursLoc(sym,pt)) return;

		for(i = 0; i < m_pItem->m_Sym[sym].NumInputs+1; i++) {

			if(m_pItem->m_Sym[sym].PinWire[i]) {

				if(m_DragFlag == FALSE)	{

					DrawWire(m_pItem->m_Sym[sym].PinWire[i],0,0,0,0,DERASE,DC);

					pt.x = m_Oldx;

					pt.y = m_Oldy;

					DrawSymbol(sym,pt,SERASE,DC);
							
					DrawSymbol(sym,pt,SXOR,DC);
					}
				else {
					DrawWire(m_pItem->m_Sym[sym].PinWire[i],0,0,0,0,DXOR,DC);
					}
				}
			}
						
		pt.x = Pos.x - m_CursDeltaX;
		
		pt.y = Pos.y - m_CursDeltaY;

		DrawSymbol(sym,pt,SMOVE,DC);
		
		for(i = 0; i < m_pItem->m_Sym[sym].NumInputs+1; i++) {

			UINT wire;

			wire = m_pItem->m_Sym[sym].PinWire[i];
			
			if(wire) {
								
				CPoint pt;
				
				if(m_pItem->m_Sym[sym].Type == JCT)
				
					pt = GetJctPinEnd(sym,i);
				
				else pt = GetPinEnd(sym,i);
				
				if(m_pItem->m_Sym[sym].WireEnd[i] == 1) {
					
					m_pItem->m_Wire[wire].x1 = pt.x;
					
					m_pItem->m_Wire[wire].y1 = pt.y;
					}
				else {
					
					if(m_pItem->m_Sym[m_pItem->m_Wire[wire].Sym2].Type == JCT) {

						m_pItem->m_Wire[wire].x2 = pt.x;
						}
					else {
						if(m_pItem->m_Wire[wire].Pin2 == 0)
						
							m_pItem->m_Wire[wire].x2 = pt.x-1;
							
						else m_pItem->m_Wire[wire].x2 = pt.x+1;
						}
					
					m_pItem->m_Wire[wire].y2 = pt.y;
					}

				DrawWire(m_pItem->m_Sym[sym].PinWire[i],0,0,0,0,DXOR,DC);
				}
			}
		
		m_DragFlag = TRUE;
		}
	}
	
void CDIO14LogicWnd::OnLButtonDown(UINT uFlags, CPoint Pos)
{
	UINT i;
	
	CPoint pt;
		
	CClientDC DC(ThisObject);
			
	InfoBaloon(0,0,Pos,IB_STOP);
	
	if(CheckRefSymbolSel(Pos,TRUE)) {
		
		DrawReferenceSyms(DC);

		return;
		}
		
	//Handle user inputs
	if(CheckInputBoxSel(Pos,DC)) {
				
		m_pItem->RunLogic();
		
		m_pItem->MakeBinaryEquations();
		
//*********************************************************************		
#ifdef DEBUG_ASCII
		for(i = 0; i < m_pItem->m_NumEquations; i++) {
						
			DebugText(m_pItem->m_Equation[i],LOGIC_WIN_TOP+13*i,DC);
			}
#endif

#ifdef DEBUG_BINARY
		for(i = 0; i < m_pItem->m_NumBinElements; i++) {

			char str[80];
			itoa(m_pItem->m_BinEq[i],str,10);
			DebugText(str,LOGIC_WIN_TOP+13*i,DC);
			}
#endif
//*********************************************************************		

		DrawUserSymbols(DC);

		DrawAllWires(DC);

		return;
		}
	
	//Handle Button inputs
	if(CheckButtonSel(Pos,TRUE,DC) != NO_BUTTON) {
		
		RedrawButtons(DC);

		RedrawLogicScreen(DC);

		return;
		}

	m_pItem->CCommsItem::SetDirty();
	
	//Don't go past the end of the user symbol array
	if(m_pItem->m_NSI >= START_PROTO-1) {
				
		MBBAD;

		--m_pItem->m_NSI;

		CDIO14Info Info(2);
			
		Info.Execute(ThisObject);
		
		return;
		}

	//Do not allow overlapping placements
	if(m_EditMode == PLACE) {
		
		if(TestBodyZone(m_pItem->m_NSI-1,Pos.x,Pos.y)) {
						
			MBBAD;
			
			CDIO14Info Info(3);
			
			Info.Execute(ThisObject);

			return;
			}

		UINT Width = m_pItem->m_Sym[m_pItem->m_NSI].Width;
		
		UINT Height = GetHeight((UINT)(m_pItem->m_NSI));

		if(TestBodyZone(m_pItem->m_NSI-1,Pos.x+Width,Pos.y+Height)) {

			MBBAD;
			
			return;
			}
		}
		
	CheckUserSymbolSel(Pos);
	
	//Test to begin wire connection
	if(!m_ConnectMode && !m_EditMode && m_SelectedSymbol &&
		
		m_pItem->m_Sym[m_SelectedSymbol].Type != JCT) {
		
		for(i = 1; i < m_pItem->m_Sym[m_SelectedSymbol].NumInputs+1; i++) {
					
			if(TestInputZone(m_SelectedSymbol,(UINT)(i-1),Pos.x,Pos.y)) {
												
				if(m_pItem->m_Sym[m_SelectedSymbol].WireEnd[i] > 0) {
					
					MBBAD;

					return;
					}
				
				UpdateUndoList(TRUE);
				
				CPoint pt;
				
				pt = GetPinEnd(m_SelectedSymbol,i);
																				
				m_WireStartSym = m_SelectedSymbol;
				
				m_pItem->m_Wire[m_pItem->m_NumWires].x1 = pt.x;

				m_pItem->m_Wire[m_pItem->m_NumWires].y1 = pt.y;

				m_pItem->m_Wire[m_pItem->m_NumWires].x2 = m_pItem->m_Wire[m_pItem->m_NumWires].x1;

				m_pItem->m_Wire[m_pItem->m_NumWires].y2 = m_pItem->m_Wire[m_pItem->m_NumWires].y1;
			
				m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 = m_SelectedSymbol;

				m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 = i;
				
				m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = WM_PLACE;

				m_pItem->m_Sym[m_SelectedSymbol].PinWire[i] = m_pItem->m_NumWires;
				
				m_pItem->m_Sym[m_SelectedSymbol].WireEnd[i] = 1;
								
				m_ConnectMode = CONNECT;

				DrawPinSelect(pt.x,pt.y,FALSE,DC);

				m_EditMode = NULL;

				m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 0.5;
			
				m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 0.5;
	
				return;
				}
			}
		
		if(TestOutputZone(m_SelectedSymbol,Pos.x,Pos.y)) {
							
			if(m_pItem->m_Sym[m_SelectedSymbol].WireEnd[0] > 0) {
				
				MBBAD;

				return;
				}
			
			UpdateUndoList(TRUE);
			
			CPoint pt;

			pt = GetPinEnd(m_SelectedSymbol,0);
														
			m_WireStartSym = m_SelectedSymbol;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].x1 = pt.x;

			m_pItem->m_Wire[m_pItem->m_NumWires].y1 = pt.y;

			m_pItem->m_Wire[m_pItem->m_NumWires].x2 = m_pItem->m_Wire[m_pItem->m_NumWires].x1;

			m_pItem->m_Wire[m_pItem->m_NumWires].y2 = m_pItem->m_Wire[m_pItem->m_NumWires].y1;

			m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 = m_SelectedSymbol;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 = 0;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = WM_PLACE;

			m_pItem->m_Sym[m_SelectedSymbol].PinWire[0] = m_pItem->m_NumWires;
			
			m_pItem->m_Sym[m_SelectedSymbol].WireEnd[0] = 1;
						
			m_ConnectMode = CONNECT;
			
			DrawPinSelect(pt.x,pt.y,FALSE,DC);

			m_EditMode = NULL;

			m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 0.5;
		
			m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 0.5;
		
			return;
			}
		}

	//Conclude a connection in progress
	if(m_ConnectMode == CONNECT) {
		
		//Insert a junction in a  segment
		if(m_SelectedWire || m_SelectedSegment) {
			
			UINT DriverSym = 0;
						
			//Can't connect to a horizontal segment.
			if(m_SelectedWire) {

				MBBAD;

				return;
				}
			
			if(m_SelectedSegment) m_SelectedWire = m_SelectedSegment;
			
			Pos = ForceGrid(Pos);

			m_pItem->m_Sym[m_pItem->m_NSI].Left = m_VertSegX-2;
					
			m_pItem->m_Sym[m_pItem->m_NSI].Top = m_VertSegY-2;
					
			m_pItem->m_Sym[m_pItem->m_NSI].Width      = m_pItem->m_Sym[JCT_PROTO].Width;
			m_pItem->m_Sym[m_pItem->m_NSI].Type       = m_pItem->m_Sym[JCT_PROTO].Type;
			m_pItem->m_Sym[m_pItem->m_NSI].NumInputs  = m_pItem->m_Sym[JCT_PROTO].NumInputs;
			m_pItem->m_Sym[m_pItem->m_NSI].NumOutputs = m_pItem->m_Sym[JCT_PROTO].NumOutputs;
			m_pItem->m_Sym[m_pItem->m_NSI].PinSpace   = m_pItem->m_Sym[JCT_PROTO].PinSpace;
			m_pItem->m_Sym[m_pItem->m_NSI].PinOffset  = m_pItem->m_Sym[JCT_PROTO].PinOffset;
			m_pItem->m_Sym[m_pItem->m_NSI].Height     = m_pItem->m_Sym[JCT_PROTO].Height;
			m_pItem->m_Sym[m_pItem->m_NSI].BMPOffsetX = m_pItem->m_Sym[JCT_PROTO].BMPOffsetX;
			
			for(i = 0; i < NUM_PROPS; i++) {
				
				m_pItem->m_Sym[m_pItem->m_NSI].Prop[i] = m_pItem->m_Sym[JCT_PROTO].Prop[i];
				}
						
			for(i = 0; i < NUM_PINS; i++) {

				m_pItem->m_Sym[m_pItem->m_NSI].PinWire[i] = 0;	

				m_pItem->m_Sym[m_pItem->m_NSI].WireEnd[i] = 0;
				}
				
			//Identify the two attached symbols and pins.
			UINT SymEnd1 = m_pItem->m_Wire[m_SelectedWire].Sym1;
									
			UINT SymPin1 = m_pItem->m_Wire[m_SelectedWire].Pin1;
			
			UINT SymEnd2 = m_pItem->m_Wire[m_SelectedWire].Sym2;

			UINT SymPin2 = m_pItem->m_Wire[m_SelectedWire].Pin2;
			
			UINT JctPin = 0;
						
			//Can't connect drivers to drivers...
			if(m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 == 0) {

				if(SymPin1 == 0 || SymPin2 == 0) {

					MBBAD;

					CDIO14Info Info(6);
						
					Info.Execute(ThisObject);

					return;
					}
				}

			//Can't connect to a feedback wire
			if(m_pItem->m_Wire[m_SelectedWire].DrawMode == WM_U) {

				MBBAD;

				CDIO14Info Info(5);
					
				Info.Execute(ThisObject);

				return;
				}

			DeleteWire(m_SelectedWire,DC);
						
			//Complete the current wire connection
			
			JctPin = 1;
					
			if(m_pItem->m_Wire[m_pItem->m_NumWires].x1 < m_VertSegX) JctPin = 3;
						
			CPoint pt;
						
			pt = GetJctPinEnd(m_pItem->m_NSI,JctPin);
			
			DrawWire(m_pItem->m_NumWires,0,0,0,0,DERASE,DC);
																		
			m_pItem->m_Wire[m_pItem->m_NumWires].x2 = pt.x;

			m_pItem->m_Wire[m_pItem->m_NumWires].y2 = pt.y;

			m_pItem->m_Wire[m_pItem->m_NumWires].Sym2 = m_pItem->m_NSI;

			m_pItem->m_Wire[m_pItem->m_NumWires].Pin2 = JctPin;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = 0;
			
			m_pItem->m_Sym[m_pItem->m_NSI].PinWire[JctPin] = m_pItem->m_NumWires;
		
			m_pItem->m_Sym[m_pItem->m_NSI].WireEnd[JctPin] = 2;

			//Get driving symbol instance
			DriverSym = m_pItem->m_Wire[m_pItem->m_NumWires].Sym1;
			
			WireIsValid(DC);
			
			DrawJCT(m_pItem->m_NSI,DC);

			//Form two new wires...
			
			//Wire 1, End 1
			
			if(m_pItem->m_Sym[SymEnd1].Type == JCT) pt = GetJctPinEnd(SymEnd1,SymPin1);
			
			else pt = GetPinEnd(SymEnd1,SymPin1);
						
			m_pItem->m_Wire[m_pItem->m_NumWires].x1 = pt.x;

			m_pItem->m_Wire[m_pItem->m_NumWires].y1 = pt.y;

			m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 = SymEnd1;

			m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 = SymPin1;
									
			m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = 0;
			
			m_pItem->m_Sym[SymEnd1].PinWire[SymPin1] = m_pItem->m_NumWires;
		
			m_pItem->m_Sym[SymEnd1].WireEnd[SymPin1] = 1;
			
			//Wire 1, End 2
		
			JctPin = 2;

			if((UINT)pt.y > m_VertSegY) JctPin = 4;	

			pt = GetJctPinEnd(m_pItem->m_NSI,JctPin);

			m_pItem->m_Wire[m_pItem->m_NumWires].x2 = pt.x;

			m_pItem->m_Wire[m_pItem->m_NumWires].y2 = pt.y;

			m_pItem->m_Wire[m_pItem->m_NumWires].Sym2 = m_pItem->m_NSI;

			m_pItem->m_Wire[m_pItem->m_NumWires].Pin2 = JctPin;
			
			m_pItem->m_Sym[m_pItem->m_NSI].PinWire[JctPin] = m_pItem->m_NumWires;
		
			m_pItem->m_Sym[m_pItem->m_NSI].WireEnd[JctPin] = 2;
						
			m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 1.0;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 1.0;
			
			WireIsValid(DC);
			
			//Wire 2, End 1

			if(m_pItem->m_Sym[SymEnd2].Type == JCT) pt = GetJctPinEnd(SymEnd2,SymPin2);
			
			else pt = GetPinEnd(SymEnd2,SymPin2);
						
			m_pItem->m_Wire[m_pItem->m_NumWires].x1 = pt.x;

			m_pItem->m_Wire[m_pItem->m_NumWires].y1 = pt.y;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 = SymEnd2;

			m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 = SymPin2;
									
			m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = 0;
			
			m_pItem->m_Sym[SymEnd2].PinWire[SymPin2] = m_pItem->m_NumWires;
		
			m_pItem->m_Sym[SymEnd2].WireEnd[SymPin2] = 1;
			
			//Wire 2, End 2
			
			JctPin = 2;
							
			if((UINT)pt.y > m_VertSegY) JctPin = 4;
				
			pt = GetJctPinEnd(m_pItem->m_NSI,JctPin);
	
			m_pItem->m_Wire[m_pItem->m_NumWires].x2 = pt.x;

			m_pItem->m_Wire[m_pItem->m_NumWires].y2 = pt.y;

			m_pItem->m_Wire[m_pItem->m_NumWires].Sym2 = m_pItem->m_NSI;

			m_pItem->m_Wire[m_pItem->m_NumWires].Pin2 = JctPin;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = 0;
			
			m_pItem->m_Sym[m_pItem->m_NSI].PinWire[JctPin] = m_pItem->m_NumWires;
		
			m_pItem->m_Sym[m_pItem->m_NSI].WireEnd[JctPin] = 2;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 1.0;
		
			m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 1.0;
						
			WireIsValid(DC);
			
			UINT JctTag = 0;

			//Assign the junction node here...
			for(i = 1; i < 5; i++) {
								
				UINT attached_sym;
				
				UINT attached_pin;

				UINT wire = m_pItem->m_Sym[m_pItem->m_NSI].PinWire[i];

				if(!wire) continue;
				
				if(m_pItem->m_Wire[wire].Sym1 != m_pItem->m_NSI) {

					attached_sym = m_pItem->m_Wire[wire].Sym1;

					attached_pin = m_pItem->m_Wire[wire].Pin1;
					}
				else {
					attached_sym = m_pItem->m_Wire[wire].Sym2;

					attached_pin = m_pItem->m_Wire[wire].Pin2;
					}

				//This is a driving pin.
				if(attached_pin == 0) {

					m_pItem->m_Sym[m_pItem->m_NSI].Prop[0] = (BYTE)attached_sym;
					}

				if(m_pItem->m_Sym[attached_sym].Type == JCT) {
					
					m_pItem->m_Sym[m_pItem->m_NSI].Prop[0] = m_pItem->m_Sym[attached_sym].Prop[0];
					
					JctTag = m_pItem->m_Sym[m_pItem->m_NSI].Prop[0];
					}
				}
						
			++m_pItem->m_NSI;
			
			//Assign tagged junctions
			if(JctTag >= 200) {

				for(i = 1; i < m_pItem->m_NSI; i++) {

					if(m_pItem->m_Sym[i].Type != JCT) continue;
					
					if(m_pItem->m_Sym[i].Prop[0] == JctTag) {

						m_pItem->m_Sym[i].Prop[0] = (BYTE)DriverSym;
						}
					}
				}
						
			RedrawLogicScreen(DC);
			
			return;
			}
		
		if(m_SelectedSymbol == 0) {

			MBBAD;

			return;
			}
		
		//Make a pin connection
		for(UINT i = 1; i < m_pItem->m_Sym[m_SelectedSymbol].NumInputs+1; i++) {
						
			if(TestInputZone(m_SelectedSymbol,(UINT)(i-1),Pos.x,Pos.y)) {
	
				//Do not allow symbol self connections
				if(m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 == m_SelectedSymbol) {
					
					MBBAD;

					CDIO14Info Info(9);
						
					Info.Execute(ThisObject);

					return;
					}
												
				CPoint pt;
				
				pt = GetPinEnd(m_SelectedSymbol,i);

				DrawPinSelect(pt.x,pt.y,FALSE,DC);

				DrawWire(m_pItem->m_NumWires,0,0,0,0,DERASE,DC);
																			
				m_pItem->m_Wire[m_pItem->m_NumWires].x2 = pt.x+1;

				m_pItem->m_Wire[m_pItem->m_NumWires].y2 = pt.y;

				m_pItem->m_Wire[m_pItem->m_NumWires].Sym2 = m_SelectedSymbol;

				m_pItem->m_Wire[m_pItem->m_NumWires].Pin2 = i;
				
				m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = 0;
				
				m_pItem->m_Sym[m_SelectedSymbol].PinWire[i] = m_pItem->m_NumWires;
			
				m_pItem->m_Sym[m_SelectedSymbol].WireEnd[i] = 2;

				WireIsValid(DC);
								
				RedrawLogicScreen(DC);
				
				return;
				}
			}

		if(TestOutputZone(m_SelectedSymbol,Pos.x,Pos.y)) {

			if(m_pItem->m_Sym[m_SelectedSymbol].NumOutputs == 0) {
				
				MBBAD;

				return;
				}

			//Can't tie outputs together
			if(m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 == 0) {

				MBBAD;

				CDIO14Info Info(7);
					
				Info.Execute(ThisObject);

				return;
				}
			
			//Do not allow symbol self connections
			if(m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 == m_SelectedSymbol) {
				
				MBBAD;

				CDIO14Info Info(9);
					
				Info.Execute(ThisObject);

				return;
				}
			
			CPoint pt;
			
			pt = GetPinEnd(m_SelectedSymbol,0);
			
			DrawPinSelect(pt.x,pt.y,FALSE,DC);

			DrawWire(m_pItem->m_NumWires,0,0,0,0,DERASE,DC);
						
			m_pItem->m_Wire[m_pItem->m_NumWires].x2 = pt.x-1;

			m_pItem->m_Wire[m_pItem->m_NumWires].y2 = pt.y;

			m_pItem->m_Wire[m_pItem->m_NumWires].Sym2 = m_SelectedSymbol;
			
			m_pItem->m_Wire[m_pItem->m_NumWires].Pin2 = 0;

			m_pItem->m_Wire[m_pItem->m_NumWires].DrawMode = 0;
			
			m_pItem->m_Sym[m_SelectedSymbol].PinWire[0] = m_pItem->m_NumWires;

			m_pItem->m_Sym[m_SelectedSymbol].WireEnd[0] = 2;
			
			WireIsValid(DC);

			RedrawLogicScreen(DC);
			
			return;
			}

		return;
		}

	//Start dragging a vertical wire segment
	if(m_SelectedSegment && m_EditMode == NULL) {

		UpdateUndoList(TRUE);
		
		m_EditMode = DRAG_SEGMENT;
		
		UINT sw = m_SelectedSegment;
		
		DrawWire(sw,m_pItem->m_Wire[sw].x1,m_pItem->m_Wire[sw].y1,m_pItem->m_Wire[sw].x2,m_pItem->m_Wire[sw].y2,DERASE,DC);

		DrawWire(sw,m_pItem->m_Wire[sw].x1,m_pItem->m_Wire[sw].y1,m_pItem->m_Wire[sw].x2,m_pItem->m_Wire[sw].y2,DXOR,DC);

		return;
		}
	
	Pos = ForceGrid(Pos);
		
	//Place a new symbol
	if(m_EditMode == PLACE) {
		
		if(LimitCursLoc(m_pItem->m_NSI,Pos)) {

			MBBAD;
			
			return;
			}

		if(!CheckNumberOfMapItems()) return;
				
		UpdateUndoList(TRUE);
		
		switch(m_pItem->m_Sym[m_pItem->m_NSI].Type) {

			case TIMERUP: 
			case TIMERDN: 

				m_pItem->m_Sym[m_pItem->m_NSI].Prop[1] = 0;

				m_pItem->m_Sym[m_pItem->m_NSI].Prop[2] = 1;
				
				break;

			case LAT:
								
				BYTE LID = 0;
				
				for(UINT i = START_USER_SYM; i <= m_pItem->m_NSI; i++) {

					if(m_pItem->m_Sym[i].Type != LAT) continue;
					
					++LID;
					
					if(LID >= 17) {
						
						MBBAD;
						
						CDIO14Info Info(10);
											
						Info.Execute(ThisObject);

						return;
						}

					m_pItem->m_Sym[i].Prop[0] = LID;
					}

				break;
			}
			
		m_pItem->m_Sym[m_pItem->m_NSI].BMPOffsetX  = 0;
		
		DrawSymbol(m_pItem->m_NSI,Pos,SXOR,DC);

		DrawSymbol(m_pItem->m_NSI,Pos,SMOVE,DC);

		++m_pItem->m_NSI;

		m_pItem->m_Sym[m_pItem->m_NSI].Left = Pos.x;
		m_pItem->m_Sym[m_pItem->m_NSI].Top  = Pos.y;

		m_pItem->m_Sym[m_pItem->m_NSI].Width      = m_pItem->m_Sym[m_pItem->m_NSI-1].Width;
		m_pItem->m_Sym[m_pItem->m_NSI].Type       = m_pItem->m_Sym[m_pItem->m_NSI-1].Type;
		m_pItem->m_Sym[m_pItem->m_NSI].NumInputs  = m_pItem->m_Sym[m_pItem->m_NSI-1].NumInputs;
		m_pItem->m_Sym[m_pItem->m_NSI].NumOutputs = m_pItem->m_Sym[m_pItem->m_NSI-1].NumOutputs;
		m_pItem->m_Sym[m_pItem->m_NSI].PinSpace   = m_pItem->m_Sym[m_pItem->m_NSI-1].PinSpace;
		m_pItem->m_Sym[m_pItem->m_NSI].PinOffset  = m_pItem->m_Sym[m_pItem->m_NSI-1].PinOffset;
		m_pItem->m_Sym[m_pItem->m_NSI].BMPOffsetX = 19;
				
		for(i = 0; i < NUM_PROPS; i++) {
			
			m_pItem->m_Sym[m_pItem->m_NSI].Prop[i]  = m_pItem->m_Sym[m_pItem->m_NSI-1].Prop[i];
			}

		m_pItem->m_Sym[m_pItem->m_NSI].Height = GetHeight((UINT)(m_pItem->m_NSI-1));
		
		for(i = 0; i < NUM_PINS; i++) {

			m_pItem->m_Sym[m_pItem->m_NSI].PinWire[i] = 0;	

			m_pItem->m_Sym[m_pItem->m_NSI].WireEnd[i] = 0;
			}
		
		return;
		}

	//Terminate segment dragging
	if(m_EditMode == DRAG_SEGMENT) {
						
		DrawAllWires(DC);
		
		m_EditMode = NULL;

		m_SelectedSegment = 0;
				
		RedrawLogicScreen(DC);

		return;
		}

	//Terminate symbol dragging
	if(m_EditMode == DRAG_SYM) {
		
		CancelSymbolDrag(Pos);
		
		RedrawLogicScreen(DC);

		return;
		}

	//Start to drag a symbol
	if(CheckUserSymbolSel(Pos)) {
				
		if(m_SelectedSymbol < START_USER_SYM) return;
		
		UpdateUndoList(TRUE);

		Pos.x -= m_CursDeltaX;
		
		Pos.y -= m_CursDeltaY;

		if(m_pItem->m_Sym[m_SelectedSymbol].Type == JCT) 		
			m_pItem->m_Sym[m_SelectedSymbol].BMPOffsetX = 5;

		else m_pItem->m_Sym[m_SelectedSymbol].BMPOffsetX = 19;
				
		DrawSymbol(m_SelectedSymbol,Pos,SPREMOVE,DC);
				
		DrawSymbol(m_SelectedSymbol,Pos,SMOVE,DC);
				
		m_EditMode = DRAG_SYM;
		
		m_StartMove = TRUE;
		
		return;
		}
	}

void CDIO14LogicWnd::OnLButtonUp(UINT uFlags, CPoint Pos)
{
	CClientDC DC(ThisObject);
	
	for(UINT i = 0; i < NUM_BUTTONS; i++) {

		if(m_Button[i].State == TRUE) m_Button[i].State = FALSE;
		}
	
	RedrawButtons(DC);	
	}

void CDIO14LogicWnd::OnLButtonDblClk(UINT uFlags, CPoint Pos)
{
	CClientDC DC(ThisObject);
			
	CheckUserSymbolSel(Pos);

	if(m_SelectedSymbol < START_USER_SYM) return;
		
	if(m_EditMode == PLACE) return;
	
	if(m_InfoBal.Disp == IB_PEND) {
				
		KillTimer(m_InfoBal.Inst+m_pItem->m_NSI+1);
		
		m_InfoBal.Disp = IB_OFF;
		}

	//Create/Show symbol configuration dialog box.
	
	m_EditMode = NULL;
	
	UINT sym = m_SelectedSymbol;

	BYTE a = m_pItem->m_Sym[sym].Prop[0];
	BYTE b = m_pItem->m_Sym[sym].Prop[1];
	BYTE c = m_pItem->m_Sym[sym].Prop[2];
	
	switch(m_pItem->m_Sym[sym].Type) {

		case NOT:
		case LAT:
		case JCT:		
			DrawUserSymbols(DC);
			
			return;
	
		case TIMERUP: 
		case TIMERDN: 
			
			if(b == 0 && c < 1) c = 1;
			
			break;
		}
	
	CDIO14CfgSym Dlg(a,b,c);

	Dlg.SetType(m_pItem->m_Sym[m_SelectedSymbol].Type);
		
	BOOL result = Dlg.Execute(ThisObject);

	CPoint pt;
		
	pt.x = m_pItem->m_Sym[sym].Left;
		
	pt.y = m_pItem->m_Sym[sym].Top;

	m_pItem->m_Sym[sym].BMPOffsetX = 0;	
			
	if(TestSymType(sym,GATE_START,GATE_STOP)) {

		if(result == FALSE) {
			
			RedrawLogicScreen(DC);
			
			return;
			}
		
		UINT N = (UINT)watoi(Dlg.m_Data1);

		if(N < 2 || N > 16) {
			
			MBBAD;

			CDIO14Info Info(12);
				
			Info.Execute(ThisObject);

			RedrawLogicScreen(DC);

			return;
			}

		if(N < m_pItem->m_Sym[sym].NumInputs) {

			//Erase and invalidate wires attached to this symbol
			UINT wire_num = 0;
			
			for(UINT i = (UINT)(N+1); i < (UINT)(m_pItem->m_Sym[sym].NumInputs+1); i++) {	
				
				wire_num = m_pItem->m_Sym[sym].PinWire[i];
		
				if(wire_num == 0) continue;

				DeleteWire(wire_num,DC);
				}
			}
				
		DrawWire(m_pItem->m_Sym[sym].PinWire[0],0,0,0,0,DERASE,DC);
		
		m_pItem->m_Sym[sym].NumInputs = N;

		DrawUserSymbols(DC);

		pt = GetPinEnd(sym,0);
		
		UINT wire = m_pItem->m_Sym[sym].PinWire[0];
		
		if(wire) {
		
			if(m_pItem->m_Wire[wire].Sym1 == sym) {

				m_pItem->m_Wire[wire].x1 = pt.x;
			
				m_pItem->m_Wire[wire].y1 = pt.y;
				}
			else {
				m_pItem->m_Wire[wire].x2 = pt.x;
			
				m_pItem->m_Wire[wire].y2 = pt.y;
				}
			
			DrawWire(m_pItem->m_Sym[sym].PinWire[0],0,0,0,0,DDRAW,DC);
			}

		EraseLogicScreen(DC);

		RedrawLogicScreen(DC);
		}
	
	if(TestSymType(sym,TIMER_START,TIMER_STOP)) {
		
		if(result == TRUE) {
			
			double val;

			//Timer value
			val = watof(Dlg.m_Data1)*10;
						
			if(val < 0.1 || val > 65535.0) {
				
				MBBAD;
			
				CDIO14Info Info(12);
					
				Info.Execute(ThisObject);

				RedrawLogicScreen(DC);

				return;
				}
			
			//High byte
			m_pItem->m_Sym[sym].Prop[1] = (BYTE)(val/256);
			
			//Low byte
			m_pItem->m_Sym[sym].Prop[2] = (BYTE)((UINT)val - (UINT)(val/256)*256);
			
			//Map ID
			BYTE id = (BYTE)watoi(Dlg.m_Data2);

			if(id < 1 || id > 8) {
				
				MBBAD;

				CDIO14Info Info(13);
					
				Info.Execute(ThisObject);
				
				RedrawLogicScreen(DC);

				return;
				}
		
			if(!CheckTimerMapID(id)) {
				
				RedrawLogicScreen(DC);

				return;
				}
						
			m_pItem->m_Sym[sym].Prop[0] = id;
			}

		DrawUserSymbols(DC);
		
		DrawSymbolText(0,sym,pt.x,pt.y,FALSE,DC);
		
		DrawSymbolText(TIMER_START,sym,pt.x,pt.y,TRUE,DC);
		}

	if(TestSymType(sym,COUNTER_START,COUNTER_STOP)) {
		
		if(result == TRUE) {
		       				
			double val;

			//Counter value
			val = watof(Dlg.m_Data1);
						
			if(val < 0.0 || val > 65535.0) {
				
				MBBAD;
			
				CDIO14Info Info(12);
					
				Info.Execute(ThisObject);

				RedrawLogicScreen(DC);

				return;
				}

			double frac,integer;
			
			frac = modf(val/256.0,&integer);
			
			//High byte
			m_pItem->m_Sym[sym].Prop[1] = (BYTE)integer;
			
			//Low byte
			m_pItem->m_Sym[sym].Prop[2] = (BYTE)(frac*256.0);
						
			//Map ID
			val = watoi(Dlg.m_Data2);

			if(val < 1 || val > 8) {
				
				MBBAD;

				CDIO14Info Info(13);
					
				Info.Execute(ThisObject);
			
				RedrawLogicScreen(DC);

				return;
				}
			
			if(!CheckCounterMapID((BYTE)val)) {
				
				RedrawLogicScreen(DC);

				return;
				}
			
			m_pItem->m_Sym[sym].Prop[0] = (BYTE)val;
			}

		DrawUserSymbols(DC);

		DrawSymbolText(0,sym,pt.x,pt.y,FALSE,DC);
		
		DrawSymbolText(COUNTER_START,sym,pt.x,pt.y,TRUE,DC);
		}

	if(TestSymType(sym,INC-1,INC+1)) {
		
		if(result == TRUE) {
		       				
			UINT val;

			//Map ID
			val = watoi(Dlg.m_Data2);

			if(val < 1 || val > 8) {
				
				MBBAD;

				CDIO14Info Info(13);
					
				Info.Execute(ThisObject);
								
				RedrawLogicScreen(DC);

				return;
				}

			if(!CheckInCoilMapID((BYTE)val)) {
				
				RedrawLogicScreen(DC);

				return;
				}
			
			m_pItem->m_Sym[sym].Prop[0] = (BYTE)val;
			}

		DrawUserSymbols(DC);
		}
	
	if(TestSymType(sym,OUTC-1,OUTC+1)) {
		
		if(result == TRUE) {
		       				
			UINT val;

			//Map ID
			val = watoi(Dlg.m_Data2);

			if(val < 1 || val > 8) {
				
				MBBAD;

				CDIO14Info Info(13);
					
				Info.Execute(ThisObject);
								
				RedrawLogicScreen(DC);

				return;
				}

			if(!CheckOutCoilMapID((BYTE)val)) {
				
				RedrawLogicScreen(DC);

				return;
				}

			m_pItem->m_Sym[sym].Prop[0] = (BYTE)val;
			}

		DrawUserSymbols(DC);
		}

	EraseLogicScreen(DC);

	RedrawLogicScreen(DC);
	}
	
void CDIO14LogicWnd::OnRButtonDown(UINT uFlags, CPoint Pos)
{
	UINT i;
	
	CPoint pt;
	
	CClientDC DC(ThisObject);
	
	CheckUserSymbolSel(Pos);

	if(m_SelectedSymbol > 0 && m_SelectedSymbol < START_USER_SYM) return;

	//No ops if hovering over an input
	for(i = 1; i < NUM_PINS; i++) {
				
		if(TestInputZone(m_SelectedSymbol,i,Pos.x,Pos.y)) {
			
			MBBAD;

			return;
			}
		}

	//No ops if hovering over an output
	if(TestOutputZone(m_SelectedSymbol,Pos.x,Pos.y)) {
		
		MBBAD;

		return;
		}

	//If an info baloon is pending, kill it
	if(m_InfoBal.Disp == IB_PEND) {
				
		KillTimer(m_InfoBal.Inst+m_pItem->m_NSI+1);
		
		m_InfoBal.Disp = IB_OFF;
		}

	//Cancel in-progress wire connection
	if(m_ConnectMode == CONNECT) {
		
		m_ConnectMode = NULL;
		
		HandleUndo();

		for(UINT i = 0; i < m_pItem->m_Sym[m_WireStartSym].NumInputs+1; i++) {
			
			pt = GetPinEnd(m_WireStartSym,i);
			
			if(m_pItem->m_Wire[m_pItem->m_NumWires].x1 == (UINT)pt.x &&
			   m_pItem->m_Wire[m_pItem->m_NumWires].y1 == (UINT)pt.y) {
								
				m_pItem->m_Sym[m_WireStartSym].PinWire[i] = 0;
		
				m_pItem->m_Sym[m_WireStartSym].WireEnd[i] = 0;
			
				break;
				}
			}

		m_pItem->m_Wire[m_pItem->m_NumWires].x2 = Pos.x;

		m_pItem->m_Wire[m_pItem->m_NumWires].y2 = Pos.y;
		
		DrawWire(m_pItem->m_NumWires,0,0,0,0,DERASE,DC);

		DrawInputBoxes(DC);
				
		DrawOutputBoxes(TRUE,DC);
				
		DrawUserSymbols(DC);
		
		m_pItem->m_Wire[m_pItem->m_NumWires].x1 = 0;

		m_pItem->m_Wire[m_pItem->m_NumWires].y1 = 0;

		m_pItem->m_Wire[m_pItem->m_NumWires].x2 = 0;

		m_pItem->m_Wire[m_pItem->m_NumWires].y2 = 0;

		return;
		}
	
	//Cancel symbol drag
	if(m_EditMode == DRAG_SYM) {
		
		CancelSymbolDrag(Pos);

		RedrawLogicScreen(DC);

		return;
		}
		
	//Terminate placement mode
	if(m_EditMode == PLACE) {
		
		m_pItem->m_Sym[m_SelectedRef].BMPOffsetX = 0;
		
		m_pItem->m_Sym[m_pItem->m_NSI].BMPOffsetX = 10;

		DrawSymbol(m_pItem->m_NSI,Pos,SERASE,DC);
		
		m_Oldx = 1000000;

		m_Oldy = 1000000;

		m_SelectedSymbol = NULL;

		m_EditMode = NULL;
		
		m_RefSelected = FALSE;
		
		DrawReferenceSyms(DC);
		
		EraseLogicScreen(DC);

		RedrawLogicScreen(DC);
		
		return;
		}
		
	//Delete the selected wire
	if(m_SelectedWire || m_SelectedSegment) {
				
		UINT wire;
		
		MBBAD;
		
		CDIO14Confirm Conf(DWIRE);
					
		BOOL result = Conf.Execute(ThisObject);

		if(!result) return;
		
		UpdateUndoList(TRUE);
		
		m_pItem->CCommsItem::SetDirty();
		
		if(m_SelectedWire) wire = m_SelectedWire;
		
		else wire = m_SelectedSegment;
		
		TagJunctions(wire);
		
		DeleteWire(wire,DC);
		}
	
	//Delete the selected part and associated wires
	if(TestBodyZone(m_SelectedSymbol,Pos.x,Pos.y)) {
		
		MBBAD;
		
		CDIO14Confirm Conf(DSYMBOL);
					
		BOOL result = Conf.Execute(ThisObject);

		if(!result) return;
		
		UpdateUndoList(TRUE);

		m_pItem->CCommsItem::SetDirty();
		
		//Erase and invalidate wires attached to this symbol
		UINT wire = 0;
		
		for(i = 0; i < NUM_PINS; i++) {	
			
			wire = m_pItem->m_Sym[m_SelectedSymbol].PinWire[i];
	
			if(!wire) continue;

			TagJunctions(wire);
			
			DeleteWire(wire,DC);
			}

		DeleteSymbol(m_SelectedSymbol,DC);
		}

	while(!CleanupJunctions(DC));

	RedrawLogicScreen(DC);
	}

void CDIO14LogicWnd::UpdateUndoList(BOOL mode)
{
	UINT i;

	//Copy symbol list.
	for(i = 1; i < m_pItem->m_NSI; i++) {

		m_USym[m_UlistIndex][i].Left 	   = m_pItem->m_Sym[i].Left;
		m_USym[m_UlistIndex][i].Top	   = m_pItem->m_Sym[i].Top;
		m_USym[m_UlistIndex][i].Width      = m_pItem->m_Sym[i].Width;
		m_USym[m_UlistIndex][i].Height     = m_pItem->m_Sym[i].Height;
		m_USym[m_UlistIndex][i].Type       = m_pItem->m_Sym[i].Type;
		m_USym[m_UlistIndex][i].NumInputs  = m_pItem->m_Sym[i].NumInputs;
		m_USym[m_UlistIndex][i].NumOutputs = m_pItem->m_Sym[i].NumOutputs;
		m_USym[m_UlistIndex][i].PinSpace   = m_pItem->m_Sym[i].PinSpace;
		m_USym[m_UlistIndex][i].PinOffset  = m_pItem->m_Sym[i].PinOffset;
		m_USym[m_UlistIndex][i].BMPOffsetX = m_pItem->m_Sym[i].BMPOffsetX;
		
		UINT j;

		for(j = 0; j < NUM_PROPS; j++) {
		
			m_USym[m_UlistIndex][i].Prop[j] = m_pItem->m_Sym[i].Prop[j];
			}

		for(j = 0; j < NUM_PINS; j++) {

			m_USym[m_UlistIndex][i].PinWire[j] = m_pItem->m_Sym[i].PinWire[j];	

			m_USym[m_UlistIndex][i].WireEnd[j] = m_pItem->m_Sym[i].WireEnd[j];
			}
		}
	
	//Copy wire list
	for(i = 1; i < m_pItem->m_NumWires; i++) {
				
		m_UWire[m_UlistIndex][i].Sym1  = m_pItem->m_Wire[i].Sym1;
		m_UWire[m_UlistIndex][i].Sym2  = m_pItem->m_Wire[i].Sym2;
		
		m_UWire[m_UlistIndex][i].Pin1  = m_pItem->m_Wire[i].Pin1;
		m_UWire[m_UlistIndex][i].Pin2  = m_pItem->m_Wire[i].Pin2;
		
		m_UWire[m_UlistIndex][i].x1    = m_pItem->m_Wire[i].x1;
		m_UWire[m_UlistIndex][i].y1    = m_pItem->m_Wire[i].y1;
		m_UWire[m_UlistIndex][i].x2    = m_pItem->m_Wire[i].x2;
		m_UWire[m_UlistIndex][i].y2    = m_pItem->m_Wire[i].y2;
		       
		m_UWire[m_UlistIndex][i].Frac  = m_pItem->m_Wire[i].Frac;
		m_UWire[m_UlistIndex][i].Nfrac = m_pItem->m_Wire[i].Nfrac;
		
		m_UWire[m_UlistIndex][i].DrawMode = m_pItem->m_Wire[i].DrawMode;
		}

	m_UNSI[m_UlistIndex] = m_pItem->m_NSI;

	m_UNumWires[m_UlistIndex] = m_pItem->m_NumWires;
	
	if(mode == TRUE) {
	
		m_RlistLimit = 0;
		
		if(m_UlistLimit < NUM_UNDO_LEVELS-1) ++m_UlistLimit;
		
		if(m_UlistIndex < NUM_UNDO_LEVELS-1) ++m_UlistIndex;

		else m_UlistIndex = 0;
		}
	}

void CDIO14LogicWnd::RestoreUndoList(void)
{
	m_pItem->m_NSI = m_UNSI[m_UlistIndex];

	m_pItem->m_NumWires = m_UNumWires[m_UlistIndex];

	UINT i;

	//Restore symbol instance list.
	for(i = 1; i < m_UNSI[m_UlistIndex]; i++) {

		m_pItem->m_Sym[i].Left       = m_USym[m_UlistIndex][i].Left;
		m_pItem->m_Sym[i].Top        = m_USym[m_UlistIndex][i].Top;
		m_pItem->m_Sym[i].Width      = m_USym[m_UlistIndex][i].Width;
		m_pItem->m_Sym[i].Height     = m_USym[m_UlistIndex][i].Height;
		m_pItem->m_Sym[i].Type       = m_USym[m_UlistIndex][i].Type;
		m_pItem->m_Sym[i].NumInputs  = m_USym[m_UlistIndex][i].NumInputs;
		m_pItem->m_Sym[i].NumOutputs = m_USym[m_UlistIndex][i].NumOutputs;
		m_pItem->m_Sym[i].PinSpace   = m_USym[m_UlistIndex][i].PinSpace;
		m_pItem->m_Sym[i].PinOffset  = m_USym[m_UlistIndex][i].PinOffset;
		m_pItem->m_Sym[i].BMPOffsetX = m_USym[m_UlistIndex][i].BMPOffsetX;
		
		UINT j;

		for(j = 0; j < NUM_PROPS; j++) {
		
			m_pItem->m_Sym[i].Prop[j] = m_USym[m_UlistIndex][i].Prop[j];
			}

		for(j = 0; j < NUM_PINS; j++) {

			m_pItem->m_Sym[i].PinWire[j] = m_USym[m_UlistIndex][i].PinWire[j];

			m_pItem->m_Sym[i].WireEnd[j] = m_USym[m_UlistIndex][i].WireEnd[j];
			}
		}
	
	//Restore wire list
	for(i = 1; i < m_UNumWires[m_UlistIndex]; i++) {
				
		m_pItem->m_Wire[i].Sym1  = m_UWire[m_UlistIndex][i].Sym1;
		m_pItem->m_Wire[i].Sym2  = m_UWire[m_UlistIndex][i].Sym2;
		
		m_pItem->m_Wire[i].Pin1  = m_UWire[m_UlistIndex][i].Pin1;
		m_pItem->m_Wire[i].Pin2  = m_UWire[m_UlistIndex][i].Pin2;
		
		m_pItem->m_Wire[i].x1    = m_UWire[m_UlistIndex][i].x1;
		m_pItem->m_Wire[i].y1    = m_UWire[m_UlistIndex][i].y1;
		m_pItem->m_Wire[i].x2    = m_UWire[m_UlistIndex][i].x2;
		m_pItem->m_Wire[i].y2    = m_UWire[m_UlistIndex][i].y2;
		       
		m_pItem->m_Wire[i].Frac  = m_UWire[m_UlistIndex][i].Frac;
		m_pItem->m_Wire[i].Nfrac = m_UWire[m_UlistIndex][i].Nfrac;
		
		m_pItem->m_Wire[i].DrawMode = m_UWire[m_UlistIndex][i].DrawMode;
		}
	}

//Button handlers

void CDIO14LogicWnd::HandleUndo(void)
{
	if(m_RlistLimit == 0) UpdateUndoList(FALSE);	

	if(m_UlistLimit > 0) {

		--m_UlistLimit;

		++m_RlistLimit;
		}

	else return;

	if(m_UlistIndex > 0) --m_UlistIndex;

	else m_UlistIndex = NUM_UNDO_LEVELS-1;

	RestoreUndoList();
	}

void CDIO14LogicWnd::HandleRedo(void)
{
	if(m_RlistLimit > 0) {
		
		--m_RlistLimit;

		++m_UlistLimit;
		}

	else return;
	
	if(m_UlistIndex < NUM_UNDO_LEVELS-1) ++m_UlistIndex;

	else m_UlistIndex = 0;

	RestoreUndoList();
	}

void CDIO14LogicWnd::HandleRefresh(CDC &DC)
{
	EraseLogicScreen(DC);
	
	RedrawLogicScreen(DC);
	}

BOOL CDIO14LogicWnd::CheckCounterMapID(BYTE id)
{
	for(UINT i = START_USER_SYM; i < m_pItem->m_NSI+1; i++) {

		if(m_pItem->m_Sym[i].Type == COUNTUP ||
		   m_pItem->m_Sym[i].Type == COUNTDN) {

			if(m_pItem->m_Sym[i].Prop[0] == id && i != m_SelectedSymbol) {

				MBBAD;

				CDIO14Info Info(11);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDIO14LogicWnd::CheckTimerMapID(BYTE id)
{
	for(UINT i = START_USER_SYM; i < m_pItem->m_NSI+1; i++) {

		if(m_pItem->m_Sym[i].Type == TIMERUP ||
		   m_pItem->m_Sym[i].Type == TIMERDN) {

			if(m_pItem->m_Sym[i].Prop[0] == id && i != m_SelectedSymbol) {

				MBBAD;

				CDIO14Info Info(11);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDIO14LogicWnd::CheckInCoilMapID(BYTE id)
{
	for(UINT i = START_USER_SYM; i < m_pItem->m_NSI+1; i++) {

		if(m_pItem->m_Sym[i].Type == INC) {

			if(m_pItem->m_Sym[i].Prop[0] == id && i != m_SelectedSymbol) {

				MBBAD;

				CDIO14Info Info(11);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDIO14LogicWnd::CheckOutCoilMapID(BYTE id)
{
	for(UINT i = START_USER_SYM; i < m_pItem->m_NSI+1; i++) {

		if(m_pItem->m_Sym[i].Type == OUTC) {

			if(m_pItem->m_Sym[i].Prop[0] == id && i != m_SelectedSymbol) {

				MBBAD;

				CDIO14Info Info(11);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDIO14LogicWnd::CheckNumberOfMapItems(void)
{
	UINT NumCounters = 0;
	
	UINT NumTimers = 0;
	
	UINT NumInCoils = 0;

	UINT NumOutCoils = 0;

	for(UINT i = START_USER_SYM; i < m_pItem->m_NSI+1; i++) {

		if(m_pItem->m_Sym[i].Type == COUNTUP ||
		   m_pItem->m_Sym[i].Type == COUNTDN) {

			++NumCounters;

			if(NumCounters > 8) {

				MBBAD;

				CDIO14Info Info(0);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		
		if(m_pItem->m_Sym[i].Type == TIMERUP ||
		   m_pItem->m_Sym[i].Type == TIMERDN) {

			++NumTimers;
			
			if(NumTimers > 8) {

				MBBAD;

				CDIO14Info Info(1);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}

		if(m_pItem->m_Sym[i].Type == INC) {

			++NumInCoils;
			
			if(NumInCoils > 8) {

				MBBAD;

				CDIO14Info Info(4);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		
		if(m_pItem->m_Sym[i].Type == OUTC) {

			++NumOutCoils;
			
			if(NumOutCoils > 8) {

				MBBAD;

				CDIO14Info Info(8);
				
				Info.Execute(ThisObject);

				return FALSE;
				}
			}
		}

	return TRUE;
	}

BOOL CDIO14LogicWnd::CleanupJunctions(CDC &DC)
{
	CPoint pt;
	
	BYTE count;
	
	UINT xwire[4];

	UINT i;
	
	for(i = 1; i < m_pItem->m_NSI; i++) {

		//Scan junctions
		if(m_pItem->m_Sym[i].Type == JCT) {
			
			//Count junction pins that have a connection
			count = 0;
			
			for(UINT j = 1; j < 5; j++) {

				if(m_pItem->m_Sym[i].PinWire[j] > 0) {

					xwire[count] = m_pItem->m_Sym[i].PinWire[j];
					
					++count;
					}
				}

			//Resolve the two connected wires into one.
			if(count == 2) {
									
				UINT SymEnd1 = 0;
				
				UINT SymEnd2 = 0;

				UINT SymPin1 = 0;

				UINT SymPin2 = 0;
										
				if(m_pItem->m_Wire[xwire[0]].Sym1 != i) {

					SymEnd1 = m_pItem->m_Wire[xwire[0]].Sym1;
					
					SymPin1 = m_pItem->m_Wire[xwire[0]].Pin1;
					}
				else {
					SymEnd1 = m_pItem->m_Wire[xwire[0]].Sym2;
					
					SymPin1 = m_pItem->m_Wire[xwire[0]].Pin2;
					}

				if(m_pItem->m_Wire[xwire[1]].Sym1 != i) {

					SymEnd2 = m_pItem->m_Wire[xwire[1]].Sym1;
					
					SymPin2 = m_pItem->m_Wire[xwire[1]].Pin1;
					}
				else {
					SymEnd2 = m_pItem->m_Wire[xwire[1]].Sym2;
					
					SymPin2 = m_pItem->m_Wire[xwire[1]].Pin2;
					}

				UINT shift;
				
				if(xwire[0] < xwire[1]) shift = 1;

				else shift = 0;
									
				DeleteWire(xwire[0],DC);
				
				DeleteWire((UINT)(xwire[1]-shift),DC);
				
				//Wire, End 1
				
				if(m_pItem->m_Sym[SymEnd1].Type == JCT) {
					
					pt = GetJctPinEnd(SymEnd1,SymPin1);
					}
				else {
					pt = GetPinEnd(SymEnd1,SymPin1);
					}
							
				m_pItem->m_Wire[m_pItem->m_NumWires].x1 = pt.x;

				m_pItem->m_Wire[m_pItem->m_NumWires].y1 = pt.y;

				m_pItem->m_Wire[m_pItem->m_NumWires].Sym1 = SymEnd1;

				m_pItem->m_Wire[m_pItem->m_NumWires].Pin1 = SymPin1;
										
				m_pItem->m_Sym[SymEnd1].PinWire[SymPin1] = m_pItem->m_NumWires;
			
				m_pItem->m_Sym[SymEnd1].WireEnd[SymPin1] = 1;
				
				//Wire, End 2
							
				if(m_pItem->m_Sym[SymEnd2].Type == JCT) {

					pt = GetJctPinEnd(SymEnd2,SymPin2);
					}
				else {
					pt = GetPinEnd(SymEnd2,SymPin2);
					}

				m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 0.5;
				
				m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 0.5;
				
				if(m_pItem->m_Sym[SymEnd1].Type == JCT) {
					
					m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 0.0;
					
					m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 0.0;
					}
				
				if(m_pItem->m_Sym[SymEnd2].Type == JCT) {
				
					m_pItem->m_Wire[m_pItem->m_NumWires].Frac = 1.0;
					
					m_pItem->m_Wire[m_pItem->m_NumWires].Nfrac = 1.0;
					}

				m_pItem->m_Wire[m_pItem->m_NumWires].x2 = pt.x;

				m_pItem->m_Wire[m_pItem->m_NumWires].y2 = pt.y;

				m_pItem->m_Wire[m_pItem->m_NumWires].Sym2 = SymEnd2;

				m_pItem->m_Wire[m_pItem->m_NumWires].Pin2 = SymPin2;
				
				m_pItem->m_Sym[SymEnd2].PinWire[SymPin2] = m_pItem->m_NumWires;

				m_pItem->m_Sym[SymEnd2].WireEnd[SymPin2] = 2;
				
				WireIsValid(DC);

				DeleteSymbol(i,DC);

				DrawWire((UINT)(m_pItem->m_NumWires-1),0,0,0,0,DDRAW,DC);
				
				return FALSE;
				}
			}
		}

	//Eliminate single undriven wire segments
	for(i = 1; i < m_pItem->m_NumWires; i++) {

		UINT AttachedSym1 = m_pItem->m_Wire[i].Sym1;

		UINT AttachedSym2 = m_pItem->m_Wire[i].Sym2;

		UINT AttachedPin1 = m_pItem->m_Wire[i].Pin1;

		UINT AttachedPin2 = m_pItem->m_Wire[i].Pin2;
					
		//Delete input-to-input wires
		if(m_pItem->m_Sym[AttachedSym1].Type != JCT &&
		   m_pItem->m_Sym[AttachedSym2].Type != JCT) {

			if(AttachedPin1 != 0 && AttachedPin2 != 0) {

				DeleteWire(i,DC);

				return FALSE;
				}
			}
		}
	
	return TRUE;
	}

void CDIO14LogicWnd::ConnectMode(CPoint Pos,CDC &DC)
{
	if(LimitCursLoc(m_pItem->m_NSI,Pos)) return;
		
	DrawWire(m_pItem->m_NumWires,m_pItem->m_Wire[m_pItem->m_NumWires].x1,m_pItem->m_Wire[m_pItem->m_NumWires].y1,Pos.x,Pos.y,DNORM,DC);
	}

BOOL CDIO14LogicWnd::LimitCursLoc(UINT inst,CPoint pt)
{
	UINT x_lim, y_lim;
	
	CRect Rect = GetClientRect();

	if(m_ConnectMode == CONNECT) {

		x_lim = 0;
	
		y_lim = 0;
		}
	else {
		x_lim = m_pItem->m_Sym[inst].Width+10;
	
		y_lim = m_pItem->m_Sym[inst].Height+10;
		}
	
	if((UINT)pt.x < 25 ||
	   (UINT)pt.x > LOGIC_WIN_RIGHT-25-x_lim || 
	   (UINT)pt.y < 85 || 
	   (UINT)pt.y > LOGIC_WIN_BOTTOM-5-y_lim) {
		
		return TRUE;
		}
	
	return FALSE;
	}

void CDIO14LogicWnd::DragSegMode(CPoint Pos,CDC &DC)
{

	UINT sw = m_SelectedSegment;
				
	m_pItem->m_Wire[sw].Nfrac = ((double)m_pItem->m_Wire[sw].x1-(double)Pos.x)/((double)m_pItem->m_Wire[sw].x1-(double)m_pItem->m_Wire[sw].x2);
	
	if(m_pItem->m_Wire[sw].Nfrac > 1.0) m_pItem->m_Wire[sw].Nfrac = 1.0;

	if(m_pItem->m_Wire[sw].Nfrac < 0.0) m_pItem->m_Wire[sw].Nfrac = 0.0;

	m_pItem->m_Wire[sw].Nfrac = fabs(m_pItem->m_Wire[sw].Nfrac);
	
	DrawWire(sw,m_pItem->m_Wire[sw].x1,m_pItem->m_Wire[sw].y1,m_pItem->m_Wire[sw].x2,m_pItem->m_Wire[sw].y2,DNORM,DC);

	return;
	}
	
void CDIO14LogicWnd::WireIsValid(CDC &DC)
{
	DrawWire(m_pItem->m_NumWires,0,0,0,0,DDRAW,DC);

	m_SelectedWire = 0;
	
	m_SelectedSegment = 0;

	m_ConnectMode = NULL;

	++m_pItem->m_NumWires;
	}
	
UINT CDIO14LogicWnd::GetHeight(UINT inst)
{
	if(m_pItem->m_Sym[inst].NumInputs < 3) {

		return UINT(m_pItem->m_Sym[inst].PinSpace+m_pItem->m_Sym[inst].PinOffset*2);
		}
	
	return UINT((m_pItem->m_Sym[inst].NumInputs-1)*m_pItem->m_Sym[inst].PinSpace+m_pItem->m_Sym[inst].PinOffset*2);	
	}

CPoint CDIO14LogicWnd::GetPinEnd(UINT inst,UINT pin)
{
	CPoint pt;
		
	BYTE Offset;
	
	if(m_pItem->m_Sym[inst].Type == MINP || m_pItem->m_Sym[inst].Type == MOUT) Offset = 0;

	else Offset = 10;
	
	if(pin == 0) {
		
		//NOTE: Pin 0 is always the output pin
		pt.x = m_pItem->m_Sym[inst].Left+m_pItem->m_Sym[inst].Width+Offset;

		pt.y = m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].Height/2 - 1;
		}
	else {
		pt.x = m_pItem->m_Sym[inst].Left-Offset;
		
		if(m_pItem->m_Sym[inst].Type == MOUT) Offset = 0;

		else Offset = 1;
				
		if(m_pItem->m_Sym[inst].NumInputs == 1) {

			pt.y = m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].Height/2 - Offset;
			}
		else {
			pt.y = m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].PinOffset+m_pItem->m_Sym[inst].PinSpace*(pin-1) - Offset;
			}
		}
	
	return pt;
	}
	
CPoint CDIO14LogicWnd::GetJctPinEnd(UINT inst,UINT pin)
{
	CPoint pt;
	
	pt.x = m_pItem->m_Sym[inst].Left+m_pItem->m_Sym[inst].Width/2;

	pt.y = m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].Height/2;
	
	return pt;
	}
	
CPoint CDIO14LogicWnd::ForceGrid(CPoint pt)
{
	double /*r,*/n;
			
	/*r =*/ modf(pt.x / GRID_SIZE,&n);
	
	pt.x = (UINT)n*GRID_SIZE;
	
	/*r =*/ modf(pt.y /  GRID_SIZE,&n);

	pt.y = (UINT)n*GRID_SIZE;
	
	if(m_GridOldX != (UINT)pt.x || m_GridOldY != (UINT)pt.y) m_GridPosChange = TRUE;

	else m_GridPosChange = FALSE;
	
	m_GridOldX = pt.x;

	m_GridOldY = pt.y;

	return pt;
	}

void CDIO14LogicWnd::CancelSymbolDrag(CPoint pt)
{						
		pt.x -= m_CursDeltaX;
		
		pt.y -= m_CursDeltaY;

		UINT sym = m_SelectedSymbol;
		
		m_pItem->m_Sym[sym].BMPOffsetX = 0;
		
		m_EditMode = NULL;

		m_DragFlag = FALSE;
		}
	
void CDIO14LogicWnd::DeleteSymbol(UINT inst,CDC &DC)
{
	CPoint pt;

	pt.x = m_pItem->m_Sym[inst].Left;
	
	pt.y = m_pItem->m_Sym[inst].Top;
	
	m_Oldx = m_pItem->m_Sym[inst].Left;

	m_Oldy = m_pItem->m_Sym[inst].Top;
	
	DrawSymbol(inst,pt,SERASE,DC);

	UINT i;

	//Repack symbol instance list.
	for(i = inst; i < m_pItem->m_NSI-1; i++) {

		m_pItem->m_Sym[i].Left	     = m_pItem->m_Sym[i+1].Left;
		m_pItem->m_Sym[i].Top	     = m_pItem->m_Sym[i+1].Top;
		m_pItem->m_Sym[i].Width      = m_pItem->m_Sym[i+1].Width;
		m_pItem->m_Sym[i].Height     = m_pItem->m_Sym[i+1].Height;
		m_pItem->m_Sym[i].Type       = m_pItem->m_Sym[i+1].Type;
		m_pItem->m_Sym[i].NumInputs  = m_pItem->m_Sym[i+1].NumInputs;
		m_pItem->m_Sym[i].NumOutputs = m_pItem->m_Sym[i+1].NumOutputs;
		m_pItem->m_Sym[i].PinSpace   = m_pItem->m_Sym[i+1].PinSpace;
		m_pItem->m_Sym[i].PinOffset  = m_pItem->m_Sym[i+1].PinOffset;
		m_pItem->m_Sym[i].BMPOffsetX = m_pItem->m_Sym[i+1].BMPOffsetX;
		
		UINT j;

		for(j = 0; j < NUM_PROPS; j++) {
		
			m_pItem->m_Sym[i].Prop[j] = m_pItem->m_Sym[i+1].Prop[j];
			}

		for(j = 0; j < NUM_PINS; j++) {

			m_pItem->m_Sym[i].PinWire[j] = m_pItem->m_Sym[i+1].PinWire[j];	

			m_pItem->m_Sym[i].WireEnd[j] = m_pItem->m_Sym[i+1].WireEnd[j];
			}
		}

	for(i = 1; i < m_pItem->m_NumWires; i++) {

		if(m_pItem->m_Wire[i].Sym1 > inst && m_pItem->m_Wire[i].Sym1 > 0) --m_pItem->m_Wire[i].Sym1;
		
		if(m_pItem->m_Wire[i].Sym2 > inst && m_pItem->m_Wire[i].Sym2 > 0) --m_pItem->m_Wire[i].Sym2;
		}
	
	--m_pItem->m_NSI;
	}
	
void CDIO14LogicWnd::DeleteWire(UINT wire,CDC &DC)
{
	DrawWire(wire,0,0,0,0,DERASE,DC);

	RepackWireList(wire);
	}
	
void CDIO14LogicWnd::RepackWireList(UINT sel_wire)
{
	UINT i,j;
	
	UINT AttachedSym = m_pItem->m_Wire[sel_wire].Sym1;

	UINT AttachedPin = m_pItem->m_Wire[sel_wire].Pin1;
	
	m_pItem->m_Sym[AttachedSym].PinWire[AttachedPin] = 0;

	m_pItem->m_Sym[AttachedSym].WireEnd[AttachedPin] = 0;
	
	AttachedSym = m_pItem->m_Wire[sel_wire].Sym2;
	
	AttachedPin = m_pItem->m_Wire[sel_wire].Pin2;

	m_pItem->m_Sym[AttachedSym].PinWire[AttachedPin] = 0;

	m_pItem->m_Sym[AttachedSym].WireEnd[AttachedPin] = 0;

	m_SelectedWire = 0;
	
	m_SelectedSegment = 0;

	for(i = sel_wire; i < m_pItem->m_NumWires; i++) {
				
		m_pItem->m_Wire[i].Sym1  = m_pItem->m_Wire[i+1].Sym1;
		m_pItem->m_Wire[i].Sym2  = m_pItem->m_Wire[i+1].Sym2;
		
		m_pItem->m_Wire[i].Pin1  = m_pItem->m_Wire[i+1].Pin1;
		m_pItem->m_Wire[i].Pin2  = m_pItem->m_Wire[i+1].Pin2;
		
		m_pItem->m_Wire[i].x1    = m_pItem->m_Wire[i+1].x1;
		m_pItem->m_Wire[i].y1    = m_pItem->m_Wire[i+1].y1;
		m_pItem->m_Wire[i].x2    = m_pItem->m_Wire[i+1].x2;
		m_pItem->m_Wire[i].y2    = m_pItem->m_Wire[i+1].y2;
		       
		m_pItem->m_Wire[i].Frac  = m_pItem->m_Wire[i+1].Frac;
		m_pItem->m_Wire[i].Nfrac = m_pItem->m_Wire[i+1].Nfrac;
		
		m_pItem->m_Wire[i].DrawMode = m_pItem->m_Wire[i+1].DrawMode;
		}
						
	if(m_pItem->m_NumWires > 1) --m_pItem->m_NumWires;
	
	//Adjust symbol pin-wire associations
	for(i = 1; i < m_pItem->m_NSI; i++) {
	
		for(j = 0; j < NUM_PINS; j++) {

			if(m_pItem->m_Sym[i].PinWire[j] >= sel_wire) 
				
				m_pItem->m_Sym[i].PinWire[j] = m_pItem->m_Sym[i].PinWire[j]-1;
			}
		}
	}
			
void CDIO14LogicWnd::TagJunctions(UINT wire)
{
	UINT TagSym = 0;
	
	BYTE TagID = 200;

	//Is this a driven wire?
	if(m_pItem->m_Wire[wire].Pin1 != 0 && m_pItem->m_Wire[wire].Pin2 != 0) return;

	if(m_pItem->m_Wire[wire].Pin1 == 0) TagSym = m_pItem->m_Wire[wire].Sym1;

	if(m_pItem->m_Wire[wire].Pin2 == 0) TagSym = m_pItem->m_Wire[wire].Sym2;

	UINT i;

	//Are there already tagged junctions?
	for(i = 1; i < m_pItem->m_NSI; i++) {

		if(m_pItem->m_Sym[i].Type == JCT && m_pItem->m_Sym[i].Prop[0] >= TagID) {

			TagID = (BYTE)(m_pItem->m_Sym[i].Prop[0] + 1);
			}
		}
	
	//Tag associated junctions
	for(i = 1; i < m_pItem->m_NSI; i++) {

		if(m_pItem->m_Sym[i].Type == JCT && m_pItem->m_Sym[i].Prop[0] == TagSym) {
				
			m_pItem->m_Sym[i].Prop[0] = TagID;
			}
		}
	}

BOOL CDIO14LogicWnd::CheckInputBoxSel(CPoint pt,CDC &DC)
{
	/*------------------------------------
	Check and update the status of the
	input boxes.
	------------------------------------*/
	
	RECT Rect;

	for(UINT inst = INP0; inst <= INP7; inst++) {
				
		if(TestBodyZone(inst,pt.x+20,pt.y)) {

			DC.Save();
				
			Rect.left   = m_pItem->m_Sym[inst].Left;
			Rect.top    = m_pItem->m_Sym[inst].Top;
			Rect.right  = m_pItem->m_Sym[inst].Left+ m_pItem->m_Sym[inst].Width-10;
			Rect.bottom = m_pItem->m_Sym[inst].Top + m_pItem->m_Sym[inst].Height-2;
			
			DC.FillRect(Rect, afxBrush(TabFace));
			
			Rect.left   += 1;
			Rect.top    += 1;
			Rect.right  -= 1;
			Rect.bottom -= 1;

			CColor Color;
			
			if(m_pItem->m_InputState[inst-INP0] == 1) {

				Color = INPUT_RED;
				
				CBrush Brush(Color);
				
				m_pItem->m_InputState[inst-INP0] = 0;
				}
			else {
				Color = INPUT_GREEN;
				
				CBrush Brush(Color);
				
				m_pItem->m_InputState[inst-INP0] = 1;
				}
				
			DC.FillRect(Rect, Color);
				
			DC.Restore();

			return TRUE;
			}
		}

	return FALSE;
	}

UINT CDIO14LogicWnd::CheckButtonSel(CPoint pt,BOOL mode,CDC &DC)
{
	/*------------------------------------

	------------------------------------*/
	
	for(UINT i = 0; i < NUM_BUTTONS; i++) {

		if((UINT)pt.x >= m_Button[i].Left && 
		   (UINT)pt.x <= (m_Button[i].Left + m_Button[i].Width) &&
		   (UINT)pt.y >= m_Button[i].Top && 
		   (UINT)pt.y <= (m_Button[i].Top + m_Button[i].Height)) {
						
			if(mode == FALSE) return i;
			
			m_Button[i].State = TRUE;
			
			switch(i) {

				case B_UNDO:
					
					HandleUndo();
					
					m_pItem->CCommsItem::SetDirty();
					
					break;

				case B_REDO:
					
					HandleRedo();
					
					m_pItem->CCommsItem::SetDirty();
					
					break;
				
				case B_REFRESH: HandleRefresh(DC); break;
				}

			EraseLogicScreen(DC);

			RedrawLogicScreen(DC);

			return i;
			}
		}
	
	return NO_BUTTON;
	}

BOOL CDIO14LogicWnd::TestSymType(UINT inst, UINT start, UINT stop)
{
	if(m_pItem->m_Sym[inst].Type > start && m_pItem->m_Sym[inst].Type < stop) {

		return TRUE;
		}
	
	return FALSE;
	}

BOOL CDIO14LogicWnd::TestInputZone(UINT inst,UINT pin,UINT x,UINT y)
{
	/*-----------------------------------------------------------------
	See if the cursor is in the selected symbol input zone.
	inst = symbol instance, x,y = mouse position, pin = which pin
	-----------------------------------------------------------------*/

	//Pin zone tolerance
	UINT Tol = 4;

	UINT Offset = 10;

	if(m_pItem->m_Sym[inst].Type == MOUT) Offset = 0;

	if(m_pItem->m_Sym[inst].NumInputs > 1) {

		if(x > m_pItem->m_Sym[inst].Left-Offset-Tol && x < m_pItem->m_Sym[inst].Left-Offset+Tol &&
		   y > m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].PinOffset+pin*m_pItem->m_Sym[inst].PinSpace-Tol && 
		   y < m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].PinOffset+pin*m_pItem->m_Sym[inst].PinSpace+Tol) {

			return TRUE;
			}
		else return FALSE;
		}
	else {

		if(x > m_pItem->m_Sym[inst].Left-Offset-Tol && x < m_pItem->m_Sym[inst].Left-Offset+Tol &&
		   y > m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].Height/2 - 1 - Tol &&
		   y < m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].Height/2 - 1 + Tol) {
			
			return TRUE;
			}
		else return FALSE;
		}

	return FALSE;
	}

BOOL CDIO14LogicWnd::TestOutputZone(UINT inst,UINT x,UINT y)
{
	/*-----------------------------------------------------------------
	See if the cursor is in the selected symbol output zone.
	inst = symbol instance, x,y = mouse position
	-----------------------------------------------------------------*/

	//Pin zone tolerance
	BYTE Tol = 4;
	
	BYTE Offset = 10;

	if(m_pItem->m_Sym[inst].Type == MINP) Offset = 0;

	if(x > m_pItem->m_Sym[inst].Left+m_pItem->m_Sym[inst].Width+Offset-Tol &&
	   x < m_pItem->m_Sym[inst].Left+m_pItem->m_Sym[inst].Width+Offset+Tol &&
	   y > m_pItem->m_Sym[inst].Top+(m_pItem->m_Sym[inst].Height/2 - 1)-Tol &&
	   y < m_pItem->m_Sym[inst].Top+(m_pItem->m_Sym[inst].Height/2 - 1)+Tol ) {

		return TRUE;
		}


	return FALSE;
	}

BOOL CDIO14LogicWnd::TestBodyZone(UINT inst,UINT x,UINT y)
{
	/*-----------------------------------------------------------------
	See if the cursor is in the selected symbol body zone.
	-----------------------------------------------------------------*/

	if(x > m_pItem->m_Sym[inst].Left-15 &&
	   x < m_pItem->m_Sym[inst].Left+m_pItem->m_Sym[inst].Width+15 &&
	   y > m_pItem->m_Sym[inst].Top-5 &&
	   y < m_pItem->m_Sym[inst].Top+m_pItem->m_Sym[inst].Height+5) {
		
		return TRUE;
		}

	return FALSE;
	}


BOOL CDIO14LogicWnd::TestWireZoneA(UINT inst,UINT x,UINT y)
{
	//Wire zone tolerance
	double Tol = 4;
	
	double xA,xB,yA,yB;

	double px = (double)x;

	double py = (double)y;
	
	xA = (double)m_pItem->m_Wire[inst].x1;

	xB = (double)m_pItem->m_Wire[inst].x2;

	yA = (double)m_pItem->m_Wire[inst].y1;

	yB = (double)m_pItem->m_Wire[inst].y2;
	
	if(m_pItem->m_Wire[inst].DrawMode == WM_U) {

		UINT Yoff;

		UINT sym1 = m_pItem->m_Wire[inst].Sym1;
		
		UINT sym2 = m_pItem->m_Wire[inst].Sym2;

		UINT H1 = GetHeight(sym1);

		UINT H2 = GetHeight(sym2);
				
		if(m_pItem->m_Sym[sym1].Top+20+H1 > m_pItem->m_Sym[sym2].Top+20+H2) {
			
			Yoff = m_pItem->m_Sym[sym1].Top + 20 + H1;
			}
		else {
			Yoff = m_pItem->m_Sym[sym2].Top + 20 + H2;
			}
			
		// 1
		if((xA <= xB) && (yA <= yB)) {
		
			if((px > xA-Tol-UEXT && px < xA+Tol-UEXT) &&
			   (py > yA-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}

			if((px > xB-Tol+UEXT && px < xB+Tol+UEXT) &&
			   (py > yB-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}
			}

		// 2
		if((xA <= xB) && (yA > yB)) {
		
			if((px > xA-Tol-UEXT && px < xA+Tol-UEXT) &&
			   (py > yA-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}

			if((px > xB-Tol+UEXT && px < xB+Tol+UEXT) &&
			   (py > yB-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}
			}


		// 3
		if((xA > xB) && (yA <= yB)) {
		
			if((px > xA-Tol+UEXT && px < xA+Tol+UEXT) &&
			   (py > yA-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}

			if((px > xB-Tol-UEXT && px < xB+Tol-UEXT) &&
			   (py > yB-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}
			}

		// 2
		if((xA > xB) && (yA > yB)) {
		
			if((px > xA-Tol+UEXT && px < xA+Tol+UEXT) &&
			   (py > yA-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}

			if((px > xB-Tol-UEXT && px < xB+Tol-UEXT) &&
			   (py > yB-Tol && py < Yoff+Tol) ) {   
			    
				return TRUE;
				}
			}


		return FALSE;
		}

	// 1
	if((xA <= xB) && (yA <= yB)) {
		
		if((px > xA+Tol && px < xA+fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)-Tol) &&
		   (py > yA-Tol && py < yA+Tol) ) {   
		    
			return TRUE;
			}
		
		if((px < xB-Tol && px > xB-fabs(xA-xB)*(1.0-m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yB-Tol && py < yB+Tol) ) {
			
			return TRUE;
			}
		}
	// 2
	if((xA <= xB) && (yA > yB)) {
		
		if((px > xA+Tol && px < xA+fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)-Tol) &&
		   (py > yA-Tol && py < yA+Tol) ) {   
		    
			return TRUE;
			}
		
		if((px < xB-Tol && px > xB-fabs(xA-xB)*(1.0-m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yB-Tol && py < yB+Tol) ) {
			
			return TRUE;
			}
		}
	// 3
	if((xA > xB) && (yA > yB)) {
		
		if((px > xB+Tol && px < xB+fabs(xA-xB)*(1-m_pItem->m_Wire[inst].Nfrac)-Tol) &&
		   (py > yB-Tol && py < yB+Tol) ) {   
		    
			return TRUE;
			}
		
		if((px < xA-Tol && px > xA-fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yA-Tol && py < yA+Tol) ) {
			
			return TRUE;
			}
		}
	// 4
	if((xA > xB) && (yA <= yB)) {
		
		if((px > xB+Tol && px < xB+fabs(xA-xB)*(1-m_pItem->m_Wire[inst].Nfrac)-Tol) &&
		   (py > yB-Tol && py < yB+Tol) ) {   
		    
			return TRUE;
			}
		
		if((px < xA-Tol && px > xA-fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yA-Tol && py < yA+Tol) ) {
			
			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CDIO14LogicWnd::TestWireZoneB(UINT inst,UINT x,UINT y)
{
	//Wire zone tolerance
	double Tol = 4;

	double xA,xB,yA,yB;

	double px = (double)x;

	double py = (double)y;

	if(m_pItem->m_Wire[inst].DrawMode == WM_U) return FALSE;

	xA = (double)m_pItem->m_Wire[inst].x1;

	xB = (double)m_pItem->m_Wire[inst].x2;

	yA = (double)m_pItem->m_Wire[inst].y1;

	yB = (double)m_pItem->m_Wire[inst].y2;
	
	double dist,/*r,*/n;
	
	BOOL test = FALSE;

	// 1
	if(xA <= xB && yA <= yB) {
		
		if((px > xA+fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)-Tol &&
		    px < xA+fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yA+Tol && py < yB-Tol) ) {   
			
			test = TRUE;
			}
		}
	// 2
	if(xA <= xB && yA > yB) {
		
		if((px > xA+fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)-Tol &&
		    px < xA+fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yB+Tol && py < yA-Tol) ) {   
		    
			test = TRUE;
			}
		}
	// 3
	if(xA > xB  && yA > yB) {
		
		if((px > xA-fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)-Tol &&
		    px < xA-fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yB+Tol && py < yA-Tol) ) {   
		    
			test = TRUE;
			}
		}
	// 4
	if(xA > xB && yA <= yB) {
		
		if((px > xA-fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)-Tol &&
		    px < xA-fabs(xA-xB)*(m_pItem->m_Wire[inst].Nfrac)+Tol) &&
		   (py > yA+Tol && py < yB-Tol) ) {   
		    
			test = TRUE;
			}
		}

	if(test == TRUE) {
		
		//Calculate the x and y positions, forced to the grid,
		//for junction placement.

		dist = (double)(xB-xA);
		
		dist = (dist*m_pItem->m_Wire[inst].Nfrac);
				
		/*r =*/ modf(dist / GRID_SIZE,&n);
		
		m_VertSegX = (UINT)(n*GRID_SIZE + xA);

		dist = (double)(py-yA);
						
		/*r =*/ modf(dist / GRID_SIZE,&n);
		
		m_VertSegY = (UINT)(n*GRID_SIZE + yA);
				
		return TRUE;
		}
	
	return FALSE;
	}

BOOL CDIO14LogicWnd::CheckRefSymbolSel(CPoint pt,BOOL mode)
{
	/*------------------------------------
	Have we selected a symbol?
	------------------------------------*/
	
	if(m_RefSelected == TRUE) return FALSE;
	
	for(UINT i = START_PROTO+1; i < END_PROTO; i++) {

		if((UINT)pt.x >= m_pItem->m_Sym[i].Left && (UINT)pt.x <= (m_pItem->m_Sym[i].Left+m_pItem->m_Sym[i].Width) &&

		   (UINT)pt.y >= m_pItem->m_Sym[i].Top && (UINT)pt.y <= (m_pItem->m_Sym[i].Top+m_pItem->m_Sym[i].Height)) {
						
			m_SelectedSymbol = i;
			
			m_SelectedRef = i;

			if(mode == FALSE) return TRUE;

			m_Oldx = 1000000;

			m_Oldy = 1000000;

			m_pItem->m_Sym[m_pItem->m_NSI].Left = pt.x;
			m_pItem->m_Sym[m_pItem->m_NSI].Top = pt.y;
					
			m_pItem->m_Sym[m_pItem->m_NSI].Width      = m_pItem->m_Sym[m_SelectedSymbol].Width;
			m_pItem->m_Sym[m_pItem->m_NSI].Type       = m_pItem->m_Sym[m_SelectedSymbol].Type;
			m_pItem->m_Sym[m_pItem->m_NSI].NumInputs  = m_pItem->m_Sym[m_SelectedSymbol].NumInputs;
			m_pItem->m_Sym[m_pItem->m_NSI].NumOutputs = m_pItem->m_Sym[m_SelectedSymbol].NumOutputs;
			m_pItem->m_Sym[m_pItem->m_NSI].PinSpace   = m_pItem->m_Sym[m_SelectedSymbol].PinSpace;
			m_pItem->m_Sym[m_pItem->m_NSI].PinOffset  = m_pItem->m_Sym[m_SelectedSymbol].PinOffset;
			m_pItem->m_Sym[m_pItem->m_NSI].BMPOffsetX = m_pItem->m_Sym[m_SelectedSymbol].BMPOffsetX;
			
			UINT j;

			for(j = 0; j < 5; j++) {

				m_pItem->m_Sym[m_pItem->m_NSI].Prop[j] = m_pItem->m_Sym[m_SelectedSymbol].Prop[j];
				}

			m_pItem->m_Sym[m_pItem->m_NSI].Height = GetHeight(m_pItem->m_NSI);
			
			for(j = 0; j < NUM_PINS; j++) {

				m_pItem->m_Sym[m_pItem->m_NSI].PinWire[j] = 0;	

				m_pItem->m_Sym[m_pItem->m_NSI].WireEnd[j] = 0;
				}
			
			m_pItem->m_Sym[m_pItem->m_NSI].BMPOffsetX  = 19;
						
			m_pItem->m_Sym[m_SelectedRef].BMPOffsetX = 19;
			
			m_EditMode = PLACE;
			
			m_RefSelected = TRUE;
			
			return TRUE;
			}
		}
	
	return FALSE;
	}

BOOL CDIO14LogicWnd::CheckUserSymbolSel(CPoint pt)
{
	for(UINT i = 1; i < m_pItem->m_NSI; i++) {
		
		if(TestBodyZone(i,pt.x,pt.y)) {
			
			m_SelectedSymbol = i;
			
			m_CursDeltaX = pt.x-m_pItem->m_Sym[m_SelectedSymbol].Left;
			
			m_CursDeltaY = pt.y-m_pItem->m_Sym[m_SelectedSymbol].Top;
						
			pt.x = m_pItem->m_Sym[m_SelectedSymbol].Left;
			
			pt.y = m_pItem->m_Sym[m_SelectedSymbol].Top;
			
			m_Oldx = pt.x;

			m_Oldy = pt.y;

			return TRUE;
			}
		}
	
	m_SelectedSymbol = 0;
	
	return FALSE;
	}

void CDIO14LogicWnd::DebugText(char str[], UINT ypos, CDC &DC)
{
/*	DC.Save();

	DC.SetBkMode(TRANSPARENT);
	
	DC.Select(afxFont(Dialog));
	
	UINT x = 300;
		
	CColor Color = CColor(0, 0, 0);

	CBrush Brush(Color);
	
	DC.FillRect(x,ypos,x+200,ypos+15,Color);
	
	DC.SetTextColor(0x00C0C0C0);
	
	DC.TextOut(x, ypos, str);
	
	DC.Deselect();

	DC.Restore();
*/	}

void CDIO14LogicWnd::StartSimTimer(void)
{
	//Corrected for sim timing
	
	SetTimer(0,94);
	}

void CDIO14LogicWnd::KillSimTimer(void)
{
	KillTimer(0);
	}

void CDIO14LogicWnd::OnTimer(UINT uID, TIMERPROC *pfnProc)
{
	CClientDC DC(ThisObject);

	//Simulation timer
	if(uID == 0) {
				
		KillSimTimer();
				
		m_pItem->RunLogic();
	
		if(m_pItem->m_LogicStateChange == TRUE) {

			if(m_EditMode != DRAG_SYM) {
			
				DrawUserSymbols(DC);
			
				DrawOutputBoxes(FALSE,DC);
				}
			
			m_pItem->m_LogicStateChange = FALSE;
			}

		StartSimTimer();

		return;
		}
		
	KillTimer(uID);

	if(m_InfoBal.Disp == IB_PEND) {
		
		m_InfoBal.Disp = IB_ON;
		
		InfoBaloon(0,0,(CPoint)0,IB_STOP);
		
		return;
		}

	m_pItem->m_EResult[uID] = 1;
	}

// End of File
















	
