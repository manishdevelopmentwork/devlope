
#include "Intern.hpp"

#include "TcpIpSocket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// CIP Protocol Stack
//
// Copyright (c) 2012 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Socket Interface
//

// Constructor

CTcpIpSock::CTcpIpSock(DWORD Addr, WORD Port)
{
	m_Addr  = Addr;

	m_Port  = Port;

	m_fOpen = FALSE;

	m_pSock = NULL;
	}

// Destructor

CTcpIpSock::~CTcpIpSock(void)
{
	Close();
	}

// Attributes

bool CTcpIpSock::IsLayerUp(void) const
{
	return m_fOpen;
	}

// Operations

bool CTcpIpSock::Open(void)
{
	if( MakeSocket() ) {

		UINT Phase = PHASE_ERROR;

		m_pSock->GetPhase(Phase);

		if( Phase == PHASE_OPEN ) {

			m_fOpen = true;

			return true;
			}

		if( Phase == PHASE_CLOSING ) {

			m_pSock->Close();

			FreeSocket();

			return false;
			}

		if( Phase == PHASE_ERROR ) {

			FreeSocket();

			return false;
			}
		}

	return false;
	}

bool CTcpIpSock::Send(CBuffer *pBuff)
{
	if( IsLayerUp() ) {

		if( m_pSock->Send(pBuff) == S_OK ) {

			return true;
			}

		// NOTE -- We cannot recover from a failure to send
		// as the protocol TCP/IP stream will be broken. We
		// therefore abort the connection and let the upper
		// layers reconnect and retry.

		m_pSock->Abort();

		FreeSocket();

		return false;
		}

	return false;
	}

bool CTcpIpSock::Recv(CBuffer * &pBuff, UINT uSize, UINT uTime)
{
	if( IsLayerUp() ) {

		UINT uDone = 0;

		SetTimer(uTime);

		while( GetTimer() ) {

			PBYTE pData = pBuff->GetData();

			UINT  uRead = uSize - uDone;

			if( m_pSock->Recv(pData, uRead) == S_OK ) {

				pBuff->AddTail(uRead);

				if( (uDone += uRead) == uSize ) {

					return true;
					}
				}
			else {
				UINT Phase = PHASE_ERROR;

				m_pSock->GetPhase(Phase);

				if( Phase == PHASE_OPEN ) {

					Sleep(5);

					continue;
					}

				break;
				}
			}

		// NOTE -- We cannot recover from a recieve timeout
		// as the protocol TCP/IP stream will be broken. We
		// therefore abort the connection and let the upper
		// layers reconnect and retry.

		m_pSock->Abort();

		FreeSocket();
		}

	CipReleaseBuffer(pBuff);

	return false;
	}

bool CTcpIpSock::Close(void)
{
	if( m_pSock ) {

		m_pSock->Close();

		FreeSocket();

		return true;
		}

	return false;
	}

// Implementation

bool CTcpIpSock::MakeSocket(void)
{
	if( !m_pSock ) {

		AfxNewObject("sock-tcp", ISocket, m_pSock);

		if( m_pSock ) {
			
			IPADDR const &a = (IPADDR const &) m_Addr;

			WORD          p = m_Port;

			m_pSock->Connect(a, p);
		
			return true;
			}

		return false;
		}

	return true;
	}

void CTcpIpSock::FreeSocket(void)
{
	m_pSock->Release();

	m_pSock = NULL;

	m_fOpen = FALSE;
	}

// End of File
