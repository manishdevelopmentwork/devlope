/*****************************************************************************
T5FT.c :     file transfer services - TO BE COMPLETED WHEN PORTING
(c) COPALP 2002
*****************************************************************************/

#include "t5vm.h"

#ifdef T5DEF_FT

/*****************************************************************************
T5FT_OpenWrite
Open/Create a file for write access
Parameters:
    dwSize (IN) full size of the file (following write() will not exceed it)
    szPath (IN) file pathname
Return: handle of the file or 0 if fail
*****************************************************************************/

T5_DWORD T5FT_OpenWrite (T5_DWORD dwSize, T5_PTCHAR szPath)
{
    return 0L;
}

/*****************************************************************************
T5FT_Close
Close an open file
Parameters:
    dwHandle (IN) file handle
*****************************************************************************/

void T5FT_Close (T5_DWORD dwHandle)
{
}
    
/*****************************************************************************
T5FT_OpenRead
Open a file for read access
Parameters:
    szPath (IN) file pathname
    pdwSize (OUT) full file size of available - 0 if funknown
Return: handle of the file or 0 if fail
*****************************************************************************/

T5_DWORD T5FT_OpenRead (T5_PTCHAR szPath, T5_PTDWORD pdwSize)
{
	return 0L;
}

/*****************************************************************************
T5FT_Write
Write data to file (sequential: write at the end)
Parameters:
    dwHandle (IN) file handle
    wLen (IN) number of bytes
    pData (IN) pointer to bytes
Return: TRUE if OK
*****************************************************************************/

T5_BOOL T5FT_Write (T5_DWORD dwHandle, T5_WORD wLen, T5_PTBYTE pData)
{
    return FALSE;
}

/*****************************************************************************
T5FT_Read
Read data from file (sequential)
Parameters:
    dwHandle (IN) file handle
    wLen (IN) maximum number of expected bytes
    pData (OUT) pointer to bytes
Return: number of bytes read
*****************************************************************************/

T5_WORD T5FT_Read (T5_DWORD dwHandle, T5_WORD wLen, T5_PTBYTE pData)
{
    return 0;
}

/*****************************************************************************
T5FT_Delete
Remove a file
Parameters:
    szPath (IN) file pathname
Return: TRUE if ok
*****************************************************************************/

T5_BOOL T5FT_Delete (T5_PTCHAR szPath)
{
    return FALSE;
}

/*****************************************************************************
T5FT_OpenDir
Open directory for browse
Return: handle of the directory or 0 if fail
*****************************************************************************/

T5_DWORD T5FT_OpenDir (void)
{
    return 0L; 
}

/*****************************************************************************
T5FT_GetDirEntry
Read the next entry from open directory
Parameters:
    dwHandle (IN) directory handle
    pData (IN) pointer to memory where to copy null terminated file name
Return: TRUE if OK - FALSE if end of directory
*****************************************************************************/

T5_BOOL T5FT_GetDirEntry (T5_DWORD dwHandle, T5_PTBYTE pData)
{
    return FALSE;
}

/*****************************************************************************
T5FT_CloseDir
Close the directory
Parameters:
    dwHandle (IN) directory handle
*****************************************************************************/

void T5FT_CloseDir (T5_DWORD dwHandle)
{
}
    
/****************************************************************************/

#endif /*T5DEF_FT*/

/****************************************************************************/
