
#include "intern.hpp"

#include "balscan.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Baldor CANOpen SDO Slave Driver
//

// Imports

#include "..\ciasdos\ciasdo.cpp"

// Instantiator

INSTANTIATE(CBaldorSDOSlave);

// Constructor

CBaldorSDOSlave::CBaldorSDOSlave(void)
{
	m_Ident   = DRIVER_ID;

	m_bDrop   = 8;

	m_bDecode = 0;
	}

// Destructor

CBaldorSDOSlave::~CBaldorSDOSlave(void)
{
	}

// Device Profile Support

DWORD CBaldorSDOSlave::GetDeviceProfile(void)
{
	// DS 403 - HMI - 0x3 << 16 | 0x0193 - spec withdrawn
	
	return 0x3 << 16 | 0x0193;
	}

// End of File
