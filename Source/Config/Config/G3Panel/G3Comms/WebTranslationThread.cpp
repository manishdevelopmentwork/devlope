
#include "Intern.hpp"

#include "WebTranslationThread.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "LangItem.hpp"
#include "LangList.hpp"
#include "LangManager.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Web Translation Thread
//

// Runtime Class

AfxImplementRuntimeClass(CWebTranslationThread, CRawThread);

// Static Data

UINT	CWebTranslationThread::m_uStartMin    = 0;

UINT	CWebTranslationThread::m_uStartSec    = 0;

UINT	CWebTranslationThread::m_uCountMin    = 0;

UINT	CWebTranslationThread::m_uCountSec    = 0;

CString CWebTranslationThread::m_ClientId     = CString("Crimson3Translate");

CString CWebTranslationThread::m_ClientSecret = CString("XX6aBoo7qABd2LJ2cUoj+1DSABPo+FA3wadRaXIkRc0=");

// Constants

#define MAX_RETRY 3

// Constructor

CWebTranslationThread::CWebTranslationThread(void) : m_DoneEvent(FALSE)
{
	m_pWnd   = NULL;

	m_hNet   = NULL;

	m_fQuota = FALSE;

	m_MSToken.m_uExpireTime = 0;
	}

// Destructor

CWebTranslationThread::~CWebTranslationThread(void)
{
	if( m_hNet ) {

		InternetCloseHandle(m_hNet);
		}
	}

// Management

void CWebTranslationThread::Bind(CWnd *pWnd, CLangManager *pLang)
{
	m_pWnd  = pWnd;

	m_pLang = pLang;
	}

void CWebTranslationThread::SetTo(CUIntArray const &To)
{
	UINT n;

	for( n = 0; n < To.GetCount(); n++ ) {

		UINT      uSlot  = To[n];

		CLangItem *pLang = m_pLang->m_pLangs->GetItem(uSlot);

		BOOL      fStrip = FALSE;

		if( pLang->HasLowerDiacriticals() ) {

			if( !pLang->HasUpperDiacriticals() ) {

				fStrip = TRUE;
				}

			if( pLang->m_Upper ) {

				fStrip = TRUE;
				}
			}

		CString Abbr = m_pLang->GetAbbrFromSlot(uSlot);

		m_NoUpper.Append(BYTE(fStrip));

		if( Abbr == L"ko-2" ) {

			Abbr = L"ko";
			}

		if( Abbr == L"ko-1" ) {

			m_Jamo.Append(BYTE(1));

			Abbr = L"ko";
			}
		else
			m_Jamo.Append(BYTE(0));

		m_To.Append(Abbr);
		}

	m_From = m_pLang->GetAbbrFromSlot(0);

	m_uTo  = n;
	}

BOOL CWebTranslationThread::Terminate(DWORD Timeout)
{
	return CRawThread::Terminate(Timeout);
	}

// Attributes

CString CWebTranslationThread::GetInput(void) const
{
	return m_In;
	}

CString CWebTranslationThread::GetOutput(UINT uSlot) const
{
	CString Text = m_Out[uSlot];

	if( !Text.IsEmpty() ) {

		CStringNormalizer::Denormalize(Text, m_Data);

		if( m_NoUpper[uSlot % m_uTo] ) {

			Text.FoldUnicode(MAP_COMPOSITE);

			UINT  c = Text.GetLength();

			PWORD p = New WORD [ c ];

			UINT  s = 1;

			GetStringTypeEx( LOCALE_USER_DEFAULT,
					 CT_CTYPE3,
					 Text,
					 c,
					 p
					 );

			for( UINT n = 1; Text[n]; n++ ) {

				if( p[s] == C3_DIACRITIC ) {

					WCHAR b = Text[n-1];

					if( wisupper(b) ) {

						Text.Delete(n, 1);

						n--;
						}
					}

				s++;
				}

			delete [] p;
			}

		PCTXT pCurved = L"\x2018\x2019\x201C\x201D";

		PCTXT pSimple = L"\x0027\x0027\x0022\x0022";

		if( m_In.FindOne(pCurved) == NOTHING ) {

			for( UINT n = 0; pCurved[n]; n++ ) {

				Text.Replace(pCurved[n], pSimple[n]);
				}
			}

		Text.FoldUnicode(MAP_FOLDDIGITS | MAP_PRECOMPOSED);

		if( m_Jamo[uSlot % m_uTo] ) {

			CString Out;

			for( UINT n = 0; n < Text.GetLength(); n++ ) {

				WCHAR c = Text[n];

				if( c >= 44032 && c < 44032 + 11172 ) {

					int c3 = (c - 44032) % 28;

					int c2 = 1 + (c - 44032 - c3) % 588 / 28;

					int c1 = 1 + (c - 44032) / 588;

					if( c1 ) Out += WCHAR(0x1100 + c1 - 1);

					if( c2 ) Out += WCHAR(0x1161 + c2 - 1);

					if( c3 ) Out += WCHAR(0x11A8 + c3 - 1);
					}
				}

			Text = Out;
			}
		}

	return Text;
	}

// Operations

void CWebTranslationThread::Connect(void)
{
	m_uCode = 1;

	m_DoneEvent.Set();
	}

void CWebTranslationThread::Translate(CString const &In)
{
	// REV3 -- Provide incremental API for services that
	// cannot translate more than one string at once, and
	// provide better handling of multiple sentences with
	// their embedded sentence terminators.

	m_In          = In;

	m_Sub         = In;

	m_Data.m_Norm = normNone;

	m_uCode	      = 2;

	m_DoneEvent.Set();
	}

// Overridables

BOOL CWebTranslationThread::OnInit(void)
{
	return TRUE;
	}

UINT CWebTranslationThread::OnExec(void)
{
	CWaitableList List(m_DoneEvent, m_TermEvent);

	for(;;) {

		if( List.WaitForAnyObject(INFINITE) == waitSignal ) {

			if( List.GetObjectIndex() == 0 ) {

				BOOL fOkay = FALSE;

				switch( m_uCode ) {

					case 1:
						fOkay = DoConnect();
						break;

					case 2:
						fOkay = DoTranslate();
						break;
					}

				if( m_fQuota && !fOkay ) {

					m_pWnd->PostMessage(WM_COMMAND, IDQUOTA);
					}
				else {
					m_pWnd->PostMessage(WM_COMMAND, fOkay ? IDDONE : IDFAIL);
					}
				}
			else
				break;
			}
		}

	return 0;
	}

void CWebTranslationThread::OnTerm(void)
{
	}

// Text Encoding

CString CWebTranslationThread::DecodeString(PBYTE pData)
{
	CString Text;

	while( *pData ) {

		BYTE bCode = *pData++;

		if( bCode & 0x80 ) {

			WCHAR cData = 0;

			if( (bCode & 0xE0) == 0xC0 ) {

				BYTE bNext = *pData++;

				cData |= (bNext & 0x3F) << 0;

				cData |= (bCode & 0x1F) << 6;
				}

			if( (bCode & 0xF0) == 0xE0 ) {

				BYTE bNext = *pData++;

				BYTE bLast = *pData++;

				cData |= (bLast & 0x3F) << 0;

				cData |= (bNext & 0x3F) << 6;

				cData |= (bCode & 0x0F) << 12;
				}

			if( cData ) {

				if( cData == 0xFEFF || cData == 0xFFFE ) {

					continue;
					}

				Text += cData;
				}

			continue;
			}

		Text += WCHAR(bCode);
		}

	for(;;) {

		UINT uPos = Text.Find(L"\\u");

		if( uPos < NOTHING ) {

			UINT uData = wcstoul(PCTXT(Text) + uPos + 2, NULL, 16);

			Text.Delete(uPos, 6);

			Text.Insert(uPos, WCHAR(uData));

			continue;
			}

		break;
		}

	for(;;) {

		UINT uPos = Text.Find(L"&#");

		if( uPos < NOTHING ) {

			UINT uData = wcstoul(PCTXT(Text) + uPos + 2, NULL, 10);

			UINT uEnd  = Text.Find(';', uPos);

			Text.Delete(uPos, uEnd - uPos + 1);

			Text.Insert(uPos, WCHAR(uData));

			continue;
			}

		break;
		}

	return Text;
	}

CString CWebTranslationThread::EncodeString(CString Text)
{
	CString HTML;

	for( UINT i = 0; Text[i]; i++ ) {

		switch( Text[i] ) {

			case ' ':

				HTML += L"+";

				break;

			case '+':
			case '&':
			case '=':
			case ';':
			case '/':
			case '?':
			case '@':
			case '|':
			case '%':
			case '#':

				HTML += CPrintf(L"%%%2.2X", Text[i]);

				break;

			default:
				if( (Text[i] & 127) <= 32 || (Text[i] & 127) == 127 ) {

					HTML += CPrintf(L"%%%2.2X", Text[i]);

					break;
					}

				HTML += Text[i];

				break;
			}
		}

	return HTML;
	}

// Implementation

BOOL CWebTranslationThread::DoConnect(void)
{
	if( m_pLang->m_LexMode == 2 ) {

		return TRUE;
		}

	m_hNet = InternetOpen(	CString(IDS_CRIMSON),
				INTERNET_OPEN_TYPE_PRECONFIG,
				NULL,
				NULL,
				0
				);

	return m_hNet ? TRUE : FALSE;
	}

BOOL CWebTranslationThread::DoTranslate(void)
{
	m_Out.Empty();

	if( m_pLang->m_LexMode != 1 ) {

		if( m_Lex.GetFrom() != m_From ) {

			m_Lex.Close();

			CModule * pApp = afxModule->GetApp();

			CFilename Path = pApp->GetFolder(CSIDL_COMMON_APPDATA, L"Lexicon");

			CFilename Name = Path + L"lex.txt";

			m_Lex.Open(CError(FALSE), Name, m_From);
			}
		}

	if( m_Lex.IsValid() ) {

		for( UINT p = 0; p < 3; p++ ) {

			if( m_Lex.Lookup(m_Sub) ) {

				if( DoLexicon() ) {

					return TRUE;
					}

				break;
				}

			if( p == 0 ) {

				Normalize(normStrip);
				}

			if( p == 1 ) {

				Normalize(normCase);
				}
			}
		}

	if( m_Out.IsEmpty() ) {

		UINT uCount = m_To.GetCount();

		for( UINT n = 0; n < uCount; n++ ) {

			m_Out.Append(L"");
			}
		}

	if( m_pLang->m_LexMode < 2 ) {

		Normalize(normCase);

		switch( m_pLang->m_Server ) {

			case 0:
				return DoGoogle();

			case 1:
				return DoMicrosoft();
			}

		return FALSE;
		}

	return TRUE;
	}

void CWebTranslationThread::Normalize(UINT Norm)
{
	CStringNormalizer::Normalize(m_Sub, m_Data, Norm);
	}

BOOL CWebTranslationThread::DoLexicon(void)
{
	UINT uCount = m_To.GetCount();

	UINT uMatch = 0;

	for( UINT n = 0; n < uCount; n++ ) {

		CString Text = m_Lex.Lookup(m_Sub, m_To[n]);

		if( !Text.IsEmpty() ) {

			uMatch++;
			}

		m_Out.Append(Text);
		}

	if( m_pLang->m_LexMode == 2 ) {

		return TRUE;
		}

	if( uMatch == uCount ) {

		return TRUE;
		}

	return FALSE;
	}

BOOL CWebTranslationThread::DoGoogle(void)
{
	HINTERNET hCon = InternetConnect( m_hNet,
					  L"www.googleapis.com",
					  INTERNET_DEFAULT_HTTPS_PORT,
					  L"",
					  L"",
					  INTERNET_SERVICE_HTTP,
					  0,
					  0
					  );

	if( hCon ) {

		CString Std;

		Std  = L"/language/translate/v2?key=AIzaSyCWXydhw8RnvmSAR9xd-F4hf-i9p25GZHg";

		Std += CPrintf("&format=%s", L"text");

		Std += CPrintf("&q=%s",      EncodeString(m_Sub));

		Std += CPrintf("&source=%s", EncodeString(m_From));

		UINT c = m_To.GetCount();

		UINT uRetry = 0;

		for( UINT n = 0; n < c; n++ ) {

			if( m_Out[n].IsEmpty() ) {

				CString URL = Std + CPrintf(L"&target=%s", EncodeString(m_To[n]));

				HINTERNET hReq = HttpOpenRequest( hCon,
								  L"GET",
								  URL,
								  NULL,
								  NULL,
								  NULL,
								  INTERNET_FLAG_SECURE		|
								  INTERNET_FLAG_DONT_CACHE      |
								  INTERNET_FLAG_KEEP_CONNECTION ,
								  0
								  );

				if( hReq ) {

					if( HttpSendRequest(hReq, NULL, 0, NULL, 0) ) {

						UINT uSize  = sizeof(m_bData);

						UINT uCount = 0;

						InternetReadFile   (hReq, m_bData, uSize, PDWORD(&uCount));

						InternetCloseHandle(hReq);

						if( uCount ) {

							m_bData[uCount] = 0;

							CString Text    = DecodeString(m_bData);

							CString Mark    = L"\"translatedText\":";

							UINT	uPos    = Text.Find(Mark);

							if( uPos < NOTHING ) {

								UINT    uFrom = uPos + Mark.GetLength() + 2;

								UINT    uTo   = Text.Find(L'"', uFrom);

								CString Out   = Text.Mid(uFrom, uTo - uFrom);

								if( Out != m_Sub ) {

									Out.MakeLower();

									m_Out.SetAt(n, Out);
									}

								uRetry = 0;

								continue;
								}
							else {
								UINT uFind = Text.Find(L"\"reason\": \"backendError\"");

								if( uFind < NOTHING && uRetry < MAX_RETRY ) {

									uRetry++;

									n -= 1;

									continue;
									}

								uFind = Text.Find(L"\"reason\": \"dailyLimitExceeded\"");

								if( uFind < NOTHING ) {

									m_fQuota = TRUE;

									InternetCloseHandle(hCon);

									return FALSE;
									}
								}
							}
						}
					else {
						DWORD dwError = GetLastError();

						InternetCloseHandle(hReq);

						if( dwError == ERROR_INTERNET_TIMEOUT && uRetry < MAX_RETRY ) {

							uRetry++;

							n -= 1;

							continue;
							}
						}
					}
				}

			InternetCloseHandle(hCon);

			return FALSE;
			}

		InternetCloseHandle(hCon);

		return TRUE;
		}

	return FALSE;
	}

BOOL CWebTranslationThread::DoMicrosoft(void)
{
	if( !GetAccessToken() ) {

		return FALSE;
		}

	CString Auth("Bearer ");

	Auth += m_MSToken.m_Val;

	HINTERNET hCon = InternetConnect( m_hNet,
					  L"api.microsofttranslator.com",
					  80,
					  L"",
					  L"",
					  INTERNET_SERVICE_HTTP,
					  0,
					  0
					  );

	if( hCon ) {

		UINT c = m_To.GetCount();

		for( UINT n = 0; n < c; n++ ) {

			if( m_Out[n].IsEmpty() ) {

				CString To = m_To[n];

				CString URL;

				SubstTo(To);

				URL  = L"/v2/Http.svc/Translate?text=";

				URL += EncodeString(m_Sub);

				URL += CPrintf("&from=%s", EncodeString(m_From));

				URL += CPrintf("&to=%s", EncodeString(To));

				HINTERNET hReq = HttpOpenRequest( hCon,
								  L"GET",
								  URL,
								  NULL,
								  NULL,
								  NULL,
								  INTERNET_FLAG_DONT_CACHE      |
								  INTERNET_FLAG_KEEP_CONNECTION ,
								  0
								  );

				if( hReq ) {

					CString Header("Content-Type: text/plain\r\n");

					Header += CPrintf("Authorization: %s\r\n", Auth);

					PTXT h = PTXT(PCTXT(Header));

					if( HttpSendRequest(hReq, h, wstrlen(h), NULL, 0) ) {

						UINT uSize  = sizeof(m_bData);

						UINT uCount = 0;

						InternetReadFile   (hReq, m_bData, uSize, PDWORD(&uCount));

						InternetCloseHandle(hReq);

						if( uCount ) {

							m_bData[uCount] = 0;

							CString Out     = DecodeString(m_bData);

							CString Translated;

							if( Out.StartsWith(L"<string") ) {

								UINT uStart = Out.Find('>');

								UINT uEnd = Out.FindRev('<');

								if( uStart < NOTHING && uEnd < NOTHING ) {

									Translated = Out.Mid(uStart + 1, uEnd - uStart - 1);
									}
								}
							else {
								if( Out.Find(L"TranslateApiException") < NOTHING ) {

									m_fQuota = TRUE;
									}

								InternetCloseHandle(hCon);

								return FALSE;
								}

							if( Translated != m_Sub ) {

								Translated.MakeLower();

								m_Out.SetAt(n, Translated);
								}

							continue;
							}
						}
					else
						InternetCloseHandle(hReq);
					}

				InternetCloseHandle(hCon);

				return FALSE;
				}
			}

		InternetCloseHandle(hCon);

		return TRUE;
		}

	return FALSE;
	}

BOOL CWebTranslationThread::GetAccessToken(void)
{
	if( m_MSToken.m_uExpireTime > 0 ) {

		if( UINT(time(NULL)) < m_MSToken.m_uExpireTime ) {

			return TRUE;
			}
		}

	CString Token;

	HINTERNET hCon = InternetConnect( m_hNet,
					  L"datamarket.accesscontrol.windows.net",
					  INTERNET_DEFAULT_HTTPS_PORT,
					  L"",
					  L"",
					  INTERNET_SERVICE_HTTP,
					  0,
					  0
					  );

	CString URL("/v2/OAuth2-13");

	CString Request = CString("grant_type=client_credentials&scope=http://api.microsofttranslator.com");

	Request += CPrintf("&client_id=%s", EncodeString(m_ClientId));

	Request += CPrintf("&client_secret=%s", EncodeString(m_ClientSecret));

	if( hCon ) {

		HINTERNET hReq = HttpOpenRequest( hCon,
						L"POST",
						URL,
						NULL,
						NULL,
						NULL,
						INTERNET_FLAG_SECURE	      |
						INTERNET_FLAG_DONT_CACHE      |
						INTERNET_FLAG_KEEP_CONNECTION ,
						0
						);

		if( hReq ) {

			PTXT    h = L"Content-Type: application/x-www-form-urlencoded\r\n";

			UINT u = Request.GetLength();

			PBYTE d = New BYTE[u];

			memset(d, 0, u);

			for( UINT i = 0; i < u; i++ ) {

				d[i] = BYTE(PCTXT(Request)[i]);
				}

			if( HttpSendRequest(hReq, h, wstrlen(h), d, u) ) {

				UINT uSize  = sizeof(m_bData);

				UINT uCount = 0;

				InternetReadFile(hReq, m_bData, uSize, PDWORD(&uCount));

				InternetCloseHandle(hReq);

				if( uCount ) {

					m_bData[uCount] = 0;

					CString Out     = DecodeString(m_bData);

					CString Mark("\"access_token\"");

					UINT uFind = Out.Find(Mark);

					if( uFind < NOTHING ) {

						uFind += Mark.GetLength() + 2;

						UINT uComma = Out.Find(L',', uFind) - 1;

						Token = Out.Mid(uFind, uComma - uFind);

						UINT uTimeout = 60;

						CString TimeMark("\"expires_in\"");

						UINT uTimePos = Out.Find(TimeMark);

						if( uTimePos < NOTHING ) {

							CString Timeout = Out.Mid(uTimePos + TimeMark.GetLength() + 2);

							uTimeout = wcstol(PCTXT(Timeout), NULL, 10);
							}

						m_MSToken.m_Val = Token;

						m_MSToken.m_uExpireTime = UINT(time(NULL)) + uTimeout - 10;

						InternetCloseHandle(hCon);

						delete [] d;

						return TRUE;
						}
					}
				}

			InternetCloseHandle(hReq);

			delete [] d;
			}

		InternetCloseHandle(hCon);
		}

	return FALSE;
	}

void CWebTranslationThread::Throttle(UINT uSec, UINT uMin)
{
	// NOTE -- Second argument is the number of requests to
	// permit in a six second period. It used to be based on
	// a one minute period, but that produced too jumpy an
	// output, with the process hanging for 50+ seconds!

	UINT       tNow = GetTickCount();

	UINT const tMin = 1000 * 6;

	UINT const tSec = 1000 * 1;

	if( !m_uStartMin ) {

		m_uStartMin = tNow;

		m_uStartSec = tNow;
		}

	if( tNow - m_uStartMin >= tMin ) {

		m_uStartMin = tNow;

		m_uCountMin = 0;
		}

	if( tNow - m_uStartSec >= tSec ) {

		m_uStartSec = tNow;

		m_uCountSec = 0;
		}

	if( ++m_uCountSec >= uSec ) {

		UINT tWait = m_uStartSec + tSec - tNow;

		m_TermEvent.WaitForObject(tWait);
		}

	if( ++m_uCountMin >= uMin ) {

		UINT tWait = m_uStartMin + tMin - tNow;

		m_TermEvent.WaitForObject(tWait);
		}
	}

void CWebTranslationThread::SubstTo(CString &To)
{
	if( To.StartsWith(L"ko-") ) {

		To = L"ko";
		}
	}

// End of File
