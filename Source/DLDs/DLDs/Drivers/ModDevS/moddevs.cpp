
#include "intern.hpp"

#include "moddevs.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Modbus Device Server Driver over Ethernet
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

// Instantiator

INSTANTIATE(CModbusDeviceServerTCPDriver);

// Constructor
 
CModbusDeviceServerTCPDriver::CModbusDeviceServerTCPDriver(void)
{
	m_Ident    = DRIVER_ID;

	m_uPort    = 502;

	m_uCount   = 1;

	m_pCtx     = NULL;

	m_pHead    = NULL;

	m_pTail    = NULL;
       	}

// Destructor

CModbusDeviceServerTCPDriver::~CModbusDeviceServerTCPDriver(void)
{
	delete [] m_pSock;
	}
 
// Configuration

void MCALL CModbusDeviceServerTCPDriver::Load(LPCBYTE pData)
{
	if( GetWord(pData) == 0x1234 ) {

		m_uPort     = GetWord(pData);

		m_uCount    = GetWord(pData);

		m_uRestrict = GetByte(pData);

		m_SecMask   = GetAddr(pData);

		m_SecData   = GetAddr(pData);
		
		m_fFlipLong = GetByte(pData);

		m_fFlipReal = GetByte(pData);
		}

	m_pSock = new SOCKET [ m_uCount ];
	}

// Management

void MCALL CModbusDeviceServerTCPDriver::Attach(IPortObject *pPort)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &Sock = m_pSock[n];

		OpenSocket(n, Sock);
		}
	}

void MCALL CModbusDeviceServerTCPDriver::Detach(void)
{
	BOOL fHit = FALSE;

	for( UINT p = 0; p < 2; p++ ) {

		for( UINT n = 0; n < m_uCount; n++ ) {

			SOCKET &Sock = m_pSock[n];

			if( Sock.m_pSocket ) {

				if( !p ) {

					Sock.m_pSocket->Close();

					fHit = TRUE;
					}
				else {
					Sock.m_pSocket->Abort();

					Sock.m_pSocket->Release();
					
					Sock.m_pSocket = NULL;
					}
				}

			if( !p && !fHit ) {

				break;
				}

			Sleep(100);
			}
		}
	}

// Device

CCODE MCALL CModbusDeviceServerTCPDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx            = new CContext;

			m_pCtx->m_bDrop   = GetByte(pData);

			if( GetByte(pData) ) {

				m_pCtx->m_fFlipLong = GetByte(pData);

				m_pCtx->m_fFlipReal = GetByte(pData);
				}
			else {
				m_pCtx->m_fFlipLong = m_fFlipLong;

				m_pCtx->m_fFlipReal = m_fFlipReal;
				}

			m_pCtx->m_fEnable = TRUE;

			CModbusServer *pServer = New CModbusServer();

			#if defined(_DEBUG)

			pServer->BindDriver(this);

			#endif

			m_pCtx->m_pServer = pServer;

			AfxListAppend(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}
      
	return CCODE_SUCCESS;
	}

CCODE MCALL CModbusDeviceServerTCPDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		AfxListRemove(m_pHead, m_pTail, m_pCtx, m_pNext, m_pPrev);

		delete m_pCtx->m_pServer;

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}
	
	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CModbusDeviceServerTCPDriver::Ping(void)
{
	return CCODE_SUCCESS;
	}

CCODE MCALL CModbusDeviceServerTCPDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pCtx->m_pServer->MasterRead(Addr, pData, uCount);
	}

CCODE MCALL CModbusDeviceServerTCPDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
	return m_pCtx->m_pServer->MasterWrite(Addr, pData, uCount);
	}

void MCALL CModbusDeviceServerTCPDriver::Service(void)
{
	for( UINT n = 0; n < m_uCount; n++ ) {

		SOCKET &Sock = m_pSock[n];

		if( Sock.m_pSocket ) {

			UINT Phase;

			Sock.m_pSocket->GetPhase(Phase);

			switch( Phase ) {

				case PHASE_OPEN:

					if( !Sock.m_fBusy ) {

						if( !CheckAccess(Sock, FALSE) ) {

							Sock.m_pSocket->Close();

							break;
							}

						Sock.m_uTime = GetTickCount();

						Sock.m_fBusy = TRUE;
						}

					ReadData(Sock);

					break;

				case PHASE_CLOSING:

					Sock.m_pSocket->Close();

					break;

				case PHASE_ERROR:

					CloseSocket(Sock);

					OpenSocket(n, Sock);

					break;
				}
			}
		else
			OpenSocket(n, Sock);
		}

	ForceSleep(20);
	} 

// User Access

UINT MCALL CModbusDeviceServerTCPDriver::DrvCtrl(UINT uFunc, PCTXT Value)
{
	return 0;
	}

UINT MCALL CModbusDeviceServerTCPDriver::DevCtrl(void *pContext, UINT uFunc, PCTXT Value)
{
	CContext * pThis = (CContext *) pContext;

	if( uFunc == 1 ) {

		for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {

			if( pCtx->m_bDrop == pThis->m_bDrop) {
				
				return (pCtx->m_fEnable = !stricmp(Value, "on"));
				}
			}

		return 0;
		}

	return 0;
	}

// Implementation

void CModbusDeviceServerTCPDriver::OpenSocket(UINT n, SOCKET &Sock)
{
	Sock.m_pSocket = CreateSocket(IP_TCP);

	if( Sock.m_pSocket ) {

		Sock.m_pSocket->Listen(m_uPort);

		Sock.m_pData = new BYTE [ 300 ];

		Sock.m_uPtr  = 0;

		Sock.m_fBusy = FALSE;
		}
	}

void CModbusDeviceServerTCPDriver::CloseSocket(SOCKET &Sock)
{
	Sock.m_pSocket->Release();

	if( Sock.m_pData ) {

		delete Sock.m_pData;

		Sock.m_pData = NULL;
		}

	Sock.m_pSocket = NULL;
	}

void CModbusDeviceServerTCPDriver::ReadData(SOCKET &Sock)
{
	UINT uSize = 300 - Sock.m_uPtr;

	Sock.m_pSocket->Recv(Sock.m_pData + Sock.m_uPtr, uSize);

	if( uSize ) {

		Sock.m_uPtr += uSize;

		while( Sock.m_uPtr >= 8 ) {

			UINT uTotal = 6 + Sock.m_pData[5];

			if( Sock.m_uPtr >= uTotal ) {

				if( !DoFrame(Sock, Sock.m_pData, uTotal) ) {

					Sock.m_pSocket->Close();

					return;
					}

				if( Sock.m_uPtr -= uTotal ) {

					for( UINT n = 0; n < Sock.m_uPtr; n++ ) {

						Sock.m_pData[n] = Sock.m_pData[uTotal++];
						}

					continue;
					}
				}
			
			break;
			}

		Sock.m_uTime = GetTickCount();

		return;
		}

	if( UINT(GetTickCount() - Sock.m_uTime) >= ToTicks(10000) ) {

		Sock.m_pSocket->Close();

		return;
		}
	}

BOOL CModbusDeviceServerTCPDriver::CheckAccess(SOCKET &Sock, BOOL fWrite)
{
	DWORD IP;

	if( Sock.m_pSocket->GetRemote((IPADDR &) IP) == S_OK ) {

		if( m_uRestrict == 0 ) {

			return TRUE;
			}

		if( m_uRestrict == 2 || fWrite == TRUE ) {

			if( (IP & m_SecMask) == (m_SecData & m_SecMask) ) {

				return TRUE;
				}

			return FALSE;
			}

		return TRUE;
		}

	return FALSE;
	}

BOOL CModbusDeviceServerTCPDriver::DoFrame(SOCKET &Sock, PBYTE pData, UINT uSize)
{
	for( CContext *pCtx = m_pHead; pCtx; pCtx = pCtx->m_pNext ) {
			
		if( pCtx->m_bDrop == pData[6] ) {

			m_pThis = pCtx;
				
			if( m_pThis->m_fEnable ) {

				UINT uCode = pData[7];

				switch( uCode ) {
		
					case 3:
					case 4:
						return DoStdRead (Sock, pData, uSize, uCode);

					case 6:
					case 16:
						return DoStdWrite(Sock, pData, uSize, uCode);

					case 1:
					case 2:
						return DoBitRead (Sock, pData, uSize, uCode);

					case 5:
					case 15:
						return DoBitWrite(Sock, pData, uSize, uCode);
					}

				return DoFuncXX(Sock, pData, uSize, ILLEGAL_FUNCTION);
				}

			break;
			}
		}

	return FALSE;
	}

// Opcode Handlers

BOOL CModbusDeviceServerTCPDriver::DoStdRead(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode)
{
	UINT uAddr  = MotorToHost(PWORD(pData +  8)[0]);

	UINT uCount = MotorToHost(PWORD(pData + 10)[0]);

	if( ((uCount < 125) || (uAddr + uCount < 0xFFFF)) ) {

		PDWORD pWork   = new DWORD [ uCount ];

		memset(pWork, 0, sizeof(DWORD) * uCount);

		UINT   uType[] = { 
			
			addrWordAsWord, 
			addrWordAsLong, 
			addrWordAsReal, 
			addrLongAsLong, 
			addrLongAsReal 
			
			};

		UINT   uFact[] = { 1, 2, 2, 1, 1 };

		UINT   uTotal  = 0;

		for( UINT t = 0; t < elements(uType); t++ ) {

			if( !(uCount % uFact[t]) ) {

				CAddress Addr;

				Addr.a.m_Table  = (uCode == 3) ? 1 : 2;
				Addr.a.m_Extra  = 0;
				Addr.a.m_Offset = 1 + uAddr; 
				Addr.a.m_Type   = uType[t];

				if( IsLongReg(uType[t]) ) {

					Addr.a.m_Table += 4;
					}

				uTotal = uCount / uFact[t];

				if( !m_pThis->m_pServer->SlaveRead(Addr, pWork, uTotal) ) {

					if( t == 0 ) {

						Addr.a.m_Extra = 1;

						if( !m_pThis->m_pServer->SlaveRead(Addr, pWork, uTotal)) {

							continue;
							}
						}
					else 
						continue;
					}
				 
				break;
				} 
			}

		if( t < elements(uType) ) {

			UINT	 uBytes = IsLongReg(uType[t]) ? 4 : 2;

			UINT     uTail  = 6 + uBytes * uCount + 3;

			CBuffer *pBuff  = CreateBuffer(uTail, TRUE);

			if( pBuff ) {

				PBYTE pReply = pBuff->AddTail(uTail);

				*pReply++ = pData[0];
				*pReply++ = pData[1];
				*pReply++ = 0;
				*pReply++ = 0;
				*pReply++ = 0;
				*pReply++ = uBytes * uCount + 3;
				*pReply++ = pData[6];
				*pReply++ = pData[7];
				*pReply++ = uBytes * uCount;

				BOOL fReal = IsReal(uType[t]);

				BOOL fFlip = fReal ? m_pThis->m_fFlipReal : m_pThis->m_fFlipLong;

				for( UINT n = 0; n < uTotal; n++ ) {

					if( t == 0) {

						*pReply++ = HIBYTE(pWork[n]);
						*pReply++ = LOBYTE(pWork[n]);
						}
					else {
						if( fFlip ) {

							*pReply++ = HIBYTE(LOWORD(pWork[n]));
							*pReply++ = LOBYTE(LOWORD(pWork[n]));
							*pReply++ = HIBYTE(HIWORD(pWork[n]));
							*pReply++ = LOBYTE(HIWORD(pWork[n]));
							}
						else {
							*pReply++ = HIBYTE(HIWORD(pWork[n]));
							*pReply++ = LOBYTE(HIWORD(pWork[n]));
							*pReply++ = HIBYTE(LOWORD(pWork[n]));
							*pReply++ = LOBYTE(LOWORD(pWork[n]));
							}
						}
					}

				if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

					delete pWork;

					return TRUE;
					}

				delete pWork;

				BuffRelease(pBuff);

				return FALSE;
				}
			}

		delete pWork;
		}

	return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
	}

BOOL CModbusDeviceServerTCPDriver::DoStdWrite(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode)
{
	if( CheckAccess(Sock, TRUE) ) {

		PU2   pWrite = NULL;

		UINT  uAddr  = 0;

		UINT  uCount = 0;
	
		if( uCode == 16 ) {

			pWrite = PU2(pData + 13);

			uAddr  = MotorToHost(PWORD(pData +  8)[0]);

			uCount = MotorToHost(PWORD(pData + 10)[0]);
			}
		else {
			pWrite = PU2(pData + 10);

			uAddr  = MotorToHost(PWORD(pData +  8)[0]);

			uCount = 1;
			}

		if( (uCount < 125) && (uAddr + uCount < 0xFFFF) ) {

			PDWORD pWork   = new DWORD [ uCount ];

			UINT   uType[] = { 
			
				addrWordAsWord, 
				addrWordAsLong, 
				addrWordAsReal, 
				addrLongAsLong, 
				addrLongAsReal 
			
				};

			UINT   uFact[] = { 1, 2, 2, 1, 1 };

			UINT   uTotal  = 0;

			BOOL   fSign   = FALSE;

			for( UINT t = 0; t < elements(uType); t++ ) {

				if( !(uCount % uFact[t]) ) {

					CAddress Addr;

					Addr.a.m_Table  = IsLongReg(uType[t]) ? 5 : 1;
					Addr.a.m_Extra  = 0;
					Addr.a.m_Offset = 1 + uAddr; 
					Addr.a.m_Type   = uType[t];

					uTotal = uCount / uFact[t];

					if( !m_pThis->m_pServer->SlaveWrite(Addr, NULL, uTotal) ) {

						if( t == 0 ) {

							Addr.a.m_Extra = 1;

							if( !m_pThis->m_pServer->SlaveWrite(Addr, NULL, uTotal) ) {

								continue;
								}

							fSign = TRUE;
							}
						else
							continue;
						}
				
					BOOL fReal = IsReal(uType[t]);

					BOOL fFlip = fReal ? m_pThis->m_fFlipReal : m_pThis->m_fFlipLong;

					for( UINT n = 0; n < uTotal; n++ ) {

						if( t == 0 ) {

							DWORD dwData = MotorToHost(pWrite[n]);

							if( fSign ) {

								Make16BitSigned(dwData);
								}
						
							pWork[n] = dwData;
							}
						else {
							WORD hi = MotorToHost(pWrite[2*n+0]);
							
							WORD lo = MotorToHost(pWrite[2*n+1]);

							if( fFlip )
								pWork[n] = MAKELONG(hi, lo);
							else
								pWork[n] = MAKELONG(lo, hi);
							}
						}

					m_pThis->m_pServer->SlaveWrite(Addr, pWork, uTotal);

					break;
					}
				}

			if( t < elements(uType) ) {

				UINT     uTail = 6 + 2 + 4;
			
				CBuffer *pBuff = CreateBuffer(uTail, TRUE);

				if( pBuff ) {

					PBYTE pReply = pBuff->AddTail(uTail);

					*pReply++ = pData[ 0];
					*pReply++ = pData[ 1];
					*pReply++ = 0;
					*pReply++ = 0;
					*pReply++ = 0;
					*pReply++ = 6;
					*pReply++ = pData[ 6];
					*pReply++ = pData[ 7];
					*pReply++ = pData[ 8];
					*pReply++ = pData[ 9];
					*pReply++ = pData[10];
					*pReply++ = pData[11];

					if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

						delete pWork;

						return TRUE;
						}

					delete pWork;

					BuffRelease(pBuff);

					return FALSE;
					}
				}

			delete pWork;
			}
	
		return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
		}

	return FALSE;
	}

BOOL CModbusDeviceServerTCPDriver::DoBitRead(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode)
{
	UINT uAddr  = MotorToHost(PWORD(pData +  8)[0]);

	UINT uCount = MotorToHost(PWORD(pData + 10)[0]);

	if( uCount > 125 * 16 || uAddr + uCount > 0xFFFF ) {

		return FALSE;
		}

	UINT     uBytes = 1 + ( (uCount-1) / 8);

	UINT     uTail  = 6 + uBytes + 3;

	CBuffer *pBuff  = CreateBuffer(uTail, TRUE);

	if( pBuff ) {

		PBYTE pReply = pBuff->AddTail(uTail);

		*pReply++ = pData[0];
		*pReply++ = pData[1];
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = uBytes + 3;
		*pReply++ = pData[6];
		*pReply++ = pData[7];

		*pReply++ = uBytes;

		PDWORD pWork = new DWORD [ uCount ];

		memset(pWork, 0, uCount * sizeof(DWORD));

		CAddress Addr;

		Addr.a.m_Table  = uCode + 2;
		Addr.a.m_Extra  = 0;
		Addr.a.m_Offset = uAddr + 1;
		Addr.a.m_Type   = addrBitAsBit;

		if( m_pThis->m_pServer->SlaveRead(Addr, pWork, uCount) ) {

			if( uCount == 1 ) {

				*pReply++ = pWork[0] ? 0xFF : 0x00;
				}
			else {
				UINT uBitsDone  = 0;

				for( UINT uByte = 0; uByte < uBytes; uByte++ ) {

					UINT b = 0;

					BYTE m = 1;

					for( UINT n = 0; n < 8; n++ ) {

						if( uCount - uBitsDone ) {

							if( pWork[uBitsDone++] ) {

								b |= m;
								}

							m <<= 1;
							}
						}

					*pReply++ = b;
					}
				}
							
			delete pWork;

			if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

				return TRUE;
				}
			}
		else
			delete pWork; 

		BuffRelease(pBuff);
		}

	return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
	}

BOOL CModbusDeviceServerTCPDriver::DoBitWrite(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode)
{
	if( CheckAccess(Sock, TRUE) ) {	
		
		UINT uAddr  = MotorToHost(PWORD(pData +  8)[0]);

		UINT uCount = uCode != 5 ? MotorToHost(PWORD(pData + 10)[0]) : 1;

		UINT uTail  = 6 + 2 + 4;

		if( uCount > 125 * 16 || uAddr + uCount > 0xFFFF ) {

			return FALSE;
			}
       
		CBuffer *pBuff  = CreateBuffer(uTail, TRUE);

		if( pBuff ) {

			PBYTE pReply = pBuff->AddTail(uTail);

			*pReply++ = pData[ 0];
			*pReply++ = pData[ 1];
			*pReply++ = 0;
			*pReply++ = 0;
			*pReply++ = 0;
			*pReply++ = 6;
			*pReply++ = pData[ 6];
			*pReply++ = pData[ 7];
			*pReply++ = pData[ 8];
			*pReply++ = pData[ 9];
			*pReply++ = pData[10];
			*pReply++ = pData[11];

			PDWORD pWork = new DWORD [ uCount ];

			if( uCode != 5 ) {

				UINT b = 13;

				BYTE m = 1;

				for( UINT n = 0; n < uCount; n++ ) {

					pWork[n] = (pData[b] & m) ? TRUE : FALSE;

					if( !(m <<= 1) ) {

						b = b + 1;

						m = 1;
						}
					}
				}
			else
				pWork[0] = pData[10] ? TRUE :FALSE;

			CAddress Addr;

			Addr.a.m_Table  = 3;
			Addr.a.m_Extra  = 0;
			Addr.a.m_Offset = 1 + uAddr;
			Addr.a.m_Type   = addrBitAsBit;

			m_pThis->m_pServer->SlaveWrite(Addr, pWork, uCount);

			delete pWork;

			if( Sock.m_pSocket->Send(pBuff) == S_OK ) {
			
				return TRUE;
				}

			BuffRelease(pBuff);
			}
	
		return DoFuncXX(Sock, pData, uSize, ILLEGAL_ADDRESS);
		}

	return FALSE;
	}

BOOL CModbusDeviceServerTCPDriver::DoFuncXX(SOCKET &Sock, PBYTE pData, UINT uSize, UINT uCode)
{
	UINT     uTail = 7 + 2;

	CBuffer *pBuff = CreateBuffer(uTail, TRUE);

	if( pBuff ) {

		PBYTE pReply = pBuff->AddTail(uTail);

		*pReply++ = pData[0];
		*pReply++ = pData[1];
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 0;
		*pReply++ = 3;
		*pReply++ = pData[6];
		*pReply++ = pData[7] | 0x80;
		*pReply++ = uCode;

		if( Sock.m_pSocket->Send(pBuff) == S_OK ) {

			if( pData[7] >= 0x7D && pData[7] <= 0x7F ) {

				return TRUE;
				}

			if( pData[7] >= 0x01 && pData[7] <= 0x28) {

				return TRUE;
				}

			return FALSE;
			}

		BuffRelease(pBuff);
		}

	return FALSE;
	}

// Helpers

void CModbusDeviceServerTCPDriver::Make16BitSigned(DWORD &dwData)
{
	if( dwData & 0x8000 ) {

		dwData |= 0xFFFF0000;
		}
	}

BOOL CModbusDeviceServerTCPDriver::IsLongReg(UINT uType)
{
	switch( uType ) {

		case addrLongAsLong:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

BOOL CModbusDeviceServerTCPDriver::IsReal(UINT uType)
{
	switch( uType ) {

		case addrWordAsReal:
		case addrLongAsReal:

			return TRUE;
		}

	return FALSE;
	}

// End of File
