
//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DiagManager_HPP
	
#define	INCLUDE_DiagManager_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Compatability
//

#if !defined(AEON_ENVIRONMENT) && !defined(METHOD)

#define METHOD 

#define StdSetRef(x)

typedef unsigned long ULONG;

#endif

//////////////////////////////////////////////////////////////////////////
//
// Diagnostics Manager
//

class CDiagManager : public IDiagManager, public IDiagProvider
{
	public:
		// Constructor
		CDiagManager(void);

		// IUnknown
		#if defined(AEON_ENVIRONMENT)
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);
		#endif

		// IDiagManager
		UINT METHOD RegisterProvider(IDiagProvider *pProv, PCTXT pName);
		void METHOD RevokeProvider(UINT uProv);
		void METHOD RegisterCommand(UINT uProv, UINT uCode, PCTXT pName);
		UINT METHOD RunCommand(IDiagConsole *pConsole, PCTXT pLine);
		UINT METHOD RunCommand(char **ppBuff, UINT uEnc, PCTXT pLine);
		BOOL METHOD CompleteCommand(char *pText, UINT uSize);
		UINT METHOD RegisterConsole(IDiagConsole *pConsole);
		void METHOD RevokeConsole(UINT uConsole);
		void METHOD WriteToConsoles(char const *pText);
		BOOL METHOD HasConsoles(void);
		BOOL METHOD SerializeAccess(BOOL fLock);

		// IDiagProvider
		UINT METHOD RunDiagCmd(IDiagOutput *pOut, IDiagCommand *pCmd);

	protected:
		// Provider
		struct CProvider
		{
			PTXT		m_pName;
			IDiagProvider * m_pProv;
			};

		// Command
		struct CCommand
		{
			PTXT m_pName;
			UINT m_uProv;
			UINT m_uCode;
			};

		// Console
		struct CConsole
		{
			IDiagConsole * m_pConsole;
			};

		// Data Members
		ULONG       m_uRefs;
		BOOL	    m_fLock;
		IMutex    * m_pLock;
		CProvider * m_pProviders;
		UINT	    m_uProviderCount;
		UINT	    m_uProviderHwm;
		CCommand  * m_pCommands;
		UINT	    m_uCommandCount;
		UINT	    m_uCommandHwm;
		CConsole *  m_pConsoles;
		UINT        m_uConsoleCount;
		UINT        m_uConsoleHwm;
		UINT	    m_uConsoleInst;
		bool        m_fSort;

		// Implementation
		bool Sort(void);

		// Diagnostics
		bool AddDiagCmds(void);
		UINT DiagHelp(IDiagOutput *pOut, IDiagCommand *pCmd);

		// Sort Function
		static int SortFunc(void const *p1, void const *p2);
	};

// End of File

#endif
