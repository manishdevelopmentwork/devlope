
#include "intern.hpp"

#include "calib.hpp"

#include "LinkFindDialog.hpp"

#include "LinkAutoDetect.hpp"

#include "CheckPCap.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Download Link
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Current Module
//

CModule * afxModule = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Link Settings
//

global	CString	g_sLinkEmModel = L"";

global	BOOL	g_fLinkEmulate = FALSE;

global	UINT	g_uLinkMethod  = 0;

global	UINT	g_uLinkComPort = 1;

global	CString	g_sLinkUsbHost = L"";

global	UINT	g_uLinkTcpPort = 789;

global	CString	g_sLinkTcpHost = L"192.168.1.100";

global	BOOL	g_fLinkTcpSlow = FALSE;

//////////////////////////////////////////////////////////////////////////
//
// Watch Context
//

static CCommsThread * m_pWatchThread = NULL;

static PCDWORD        m_pWatchList   = NULL;

static UINT           m_uWatchList   = 0;

//////////////////////////////////////////////////////////////////////////
//
// Straton Context
//

static CCommsThread * m_pStratonThread = NULL;

//////////////////////////////////////////////////////////////////////////
//
// Setting Persistance
//

static void LoadConfig(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"ShLink");

	Reg.MoveTo(L"Global");

	g_fLinkEmulate = Reg.GetValue(L"Emulator", 0U);

	g_uLinkMethod  = Reg.GetValue(L"Method", 2U);

	g_uLinkComPort = Reg.GetValue(L"COM", 5U);

	g_sLinkTcpHost = Reg.GetValue(L"Host", L"192.168.1.1");

	g_uLinkTcpPort = Reg.GetValue(L"Port", 789U);

	g_fLinkTcpSlow = Reg.GetValue(L"Slow", 0U);
}

static void SaveConfig(void)
{
	CRegKey Reg = afxModule->GetApp()->GetUserRegKey();

	Reg.MoveTo(L"ShLink");

	Reg.MoveTo(L"Global");

	Reg.SetValue(L"Emulator", DWORD(g_fLinkEmulate));

	Reg.SetValue(L"Method", DWORD(g_uLinkMethod));

	Reg.SetValue(L"COM", DWORD(g_uLinkComPort));

	Reg.SetValue(L"Host", g_sLinkTcpHost);

	Reg.SetValue(L"Port", g_uLinkTcpPort);

	Reg.SetValue(L"Slow", DWORD(g_fLinkTcpSlow));
}

//////////////////////////////////////////////////////////////////////////
//
// Entry Point
//

BOOL WINAPI DllMain(HANDLE hThis, ULONG uReason, PVOID pData)
{
	if( uReason == DLL_PROCESS_ATTACH ) {

		try {
			afxModule = New CModule(modCoreLibrary);

			if( !afxModule->InitLib(hThis) ) {

				AfxTrace(L"ERROR: Failed to load ShLink\n");

				delete afxModule;

				afxModule = NULL;

				return FALSE;
			}

			WORD Version = MAKEWORD(2, 0);

			WSADATA Data;

			WSAStartup(Version, &Data);

			return TRUE;
		}

		catch( CException const &Exception )
		{
			AfxTouch(Exception);

			AfxTrace(L"ERROR: Exception loading ShLink\n");

			return FALSE;
		}
	}

	if( uReason == DLL_PROCESS_DETACH ) {

		WSACleanup();

		afxModule->Terminate();

		delete afxModule;

		afxModule = NULL;

		return TRUE;
	}

	if( uReason == DLL_THREAD_ATTACH ) {

		afxModule->ThreadAttach();

		return TRUE;
	}

	if( uReason == DLL_THREAD_DETACH ) {

		afxModule->ThreadDetach();

		return TRUE;
	}

	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
//
// Link APIs
//

BOOL DLLAPI Link_Init(void)
{
	LoadConfig();

	return TRUE;
}

BOOL DLLAPI Link_Term(void)
{
	SaveConfig();

	return TRUE;
}

BOOL DLLAPI Link_SetDatabase(CDatabase *pDbase)
{
	// TODO -- We could restore the old partitioned config!!!

	g_sLinkEmModel = pDbase->GetSystemItem()->GetEmulatorModel();

	return TRUE;
}

BOOL DLLAPI Link_AlwaysSend(void)
{
	return FALSE;
}

BOOL DLLAPI Link_HasVerify(void)
{
	return TRUE;
}

BOOL DLLAPI Link_HasSetTime(void)
{
	return TRUE;
}

BOOL DLLAPI Link_HasFlash(BOOL fMount)
{
	if( fMount ) {

		if( !g_fLinkEmulate ) {

			if( g_uLinkMethod == 1 ) {

				return TRUE;
			}
		}
	}

	return TRUE;
}

BOOL DLLAPI Link_IsOnline(void)
{
	return m_pStratonThread || m_pWatchThread;
}

BOOL DLLAPI Link_Update(CDatabase *pDbase)
{
	if( !Link_IsOnline() ) {

		CLinkSendDialog Dlg(pDbase, sendUpdate);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_Update(CDatabase *pDbase, CString Method, CString User, CString Pass)
{
	if( !Link_IsOnline() ) {

		CLinkSendDialog Dlg(pDbase, sendUpdate);

		Dlg.SetMethod(Method);

		Dlg.SetCredentials(User, Pass);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_Send(CDatabase *pDbase)
{
	if( !Link_IsOnline() ) {

		CLinkSendDialog Dlg(pDbase, sendSend);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_Send(CDatabase *pDbase, CString Method, CString User, CString Pass)
{
	if( !Link_IsOnline() ) {

		CLinkSendDialog Dlg(pDbase, sendSend);

		Dlg.SetMethod(Method);

		Dlg.SetCredentials(User, Pass);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_Verify(CDatabase *pDbase, BOOL fSilent)
{
	if( !Link_IsOnline() ) {

		UINT uSend = fSilent ? sendSilent : sendVerify;

		CLinkSendDialog Dlg(pDbase, uSend);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_Upload(CFilename &File)
{
	if( !Link_IsOnline() ) {

		CLinkUploadDialog Dlg;

		if( Dlg.Execute(*afxMainWnd) ) {

			File = Dlg.GetFilename();

			return TRUE;
		}
	}

	return FALSE;
}

BOOL DLLAPI Link_ExtractDevCon(char cTag, CString &Text)
{
	if( !Link_IsOnline() ) {

		CLinkExtractDialog Dlg(cTag);

		if( Dlg.Execute(*afxMainWnd) ) {

			Text = Dlg.GetData();

			return TRUE;
		}
	}

	return FALSE;
}

BOOL DLLAPI Link_SetTime(void)
{
	if( !Link_IsOnline() ) {

		CLinkSendDialog Dlg(NULL, sendTime);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_FindFirmware(CString &Path, CString const &Machine)
{
	CFilename Folder;
	
	if( !(Folder = afxModule->GetFilename().WithName(L"Firmware\\" + Machine)).IsDir() ) {

		#ifdef _DEBUG

		CString Build = L"Debug";

		#else

		CString Build = L"Release";

		#endif

		Folder = afxModule->GetFilename().WithName(L"..\\..\\..\\Runtime\\" + Machine + L"\\" + Build);
	}

	if( Folder.IsDir() ) {

		Path = Folder + L"\\";

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_HasEmulate(void)
{
	return TRUE;
}

BOOL DLLAPI Link_KillEmulate(void)
{
	DWORD   ID    = GetCurrentProcessId();

	CPrintf Ident = CPrintf(L"%8.8X", ID);

	CString Class = L"AeonWindow" + Ident;

	HWND    hSim  = FindWindow(Class, NULL);

	if( hSim ) {

		::PostMessage(hSim, WM_COMMAND, 200, 0);

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_GetEmulate(void)
{
	return g_fLinkEmulate;
}

BOOL DLLAPI Link_SetEmulate(BOOL fEmulate)
{
	if( !fEmulate ) {

		Link_KillEmulate();
	}

	g_fLinkEmulate = fEmulate;

	return TRUE;
}

BOOL DLLAPI Link_WatchIsActive(void)
{
	if( m_pWatchThread ) {

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_WatchStart(CWnd *pWnd, PCDWORD pRefs, UINT uRefs)
{
	if( !Link_IsOnline() ) {

		m_pWatchThread = New CCommsThread;

		m_pWatchList   = pRefs;

		m_uWatchList   = uRefs;

		m_pWatchThread->Bind(pWnd);

		m_pWatchThread->Create();

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_WatchUpdate(void)
{
	if( m_pWatchThread ) {

		m_pWatchThread->DataListRead(m_pWatchList, m_uWatchList);

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_WatchGetData(PDWORD pData, PBOOL pAvail)
{
	if( m_pWatchThread ) {

		m_pWatchThread->DataGetReadData(pData, pAvail, m_uWatchList);

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_WatchAskForCredentials(void)
{
	if( m_pWatchThread ) {

		return m_pWatchThread->AskForCredentials();
	}

	return FALSE;
}

BOOL DLLAPI Link_WatchGetError(CString &Text)
{
	if( m_pWatchThread ) {

		Text = m_pWatchThread->GetErrorText();

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_WatchClose(void)
{
	if( m_pWatchThread ) {

		m_pWatchThread->Terminate(5000);

		delete m_pWatchThread;

		m_pWatchThread = NULL;

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_StratonStart(BOOL(*pfnProc)(PVOID, UINT), PVOID pParam)
{
	if( !Link_IsOnline() ) {

		m_pStratonThread = New CCommsThread;

		m_pStratonThread->Bind(pfnProc, pParam);

		m_pStratonThread->Create();

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_StratonSend(PCBYTE pData, UINT uData)
{
	if( m_pStratonThread ) {

		return m_pStratonThread->StratonSend(pData, uData);
	}

	return FALSE;
}

UINT DLLAPI Link_StratonRecv(PBYTE pData, UINT uData)
{
	if( m_pStratonThread ) {

		return m_pStratonThread->StratonRecv(pData, uData);
	}

	return FALSE;
}

BOOL DLLAPI Link_StratonAskForCredentials(void)
{
	if( m_pStratonThread ) {

		return m_pStratonThread->AskForCredentials();
	}

	return FALSE;
}

BOOL DLLAPI Link_StratonClose(void)
{
	if( m_pStratonThread ) {

		m_pStratonThread->Terminate(5000);

		delete m_pStratonThread;

		m_pStratonThread = NULL;

		return TRUE;
	}

	return FALSE;
}

BOOL DLLAPI Link_GetCalibrate(void)
{
	return TRUE;
}

BOOL DLLAPI Link_SetCalibrate(void)
{
	if( !Link_IsOnline() ) {

		CCalibDlg Dlg(FALSE);

		return Dlg.Execute(*afxMainWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_GetCalibrateDlg(CWnd *pWnd, BOOL fLink)
{
	if( !Link_IsOnline() ) {

		CCalibDlg Dlg(fLink);

		return Dlg.Execute(*pWnd);
	}

	return FALSE;
}

BOOL DLLAPI Link_CanBrowse(void)
{
	if( !g_fLinkEmulate ) {

		if( g_uLinkMethod == 1 ) {

			return !g_sLinkUsbHost.IsEmpty();
		}

		if( g_uLinkMethod == 2 ) {

			return TRUE;
		}
	}
	else {
		char path[MAX_PATH];

		GetTempPathA(MAX_PATH, path);

		sprintf(path + strlen(path), "c32-ip-%8.8X", GetCurrentProcessId());

		if( GetFileAttributesA(path) == INVALID_FILE_ATTRIBUTES ) {

			return FALSE;
		}

		return TRUE;
	}

	return FALSE;
}


BOOL DLLAPI Link_Find(void)
{
	if( CCheckPCap::Check(TRUE) ) {

		CLinkFindDialog Dlg(TRUE);

		if( g_uLinkMethod == 1 ) {

			Dlg.PreSelect(L"USB", g_sLinkUsbHost);
		}

		if( g_uLinkMethod == 2 ) {

			CPrintf Prot("TCP %u", g_uLinkTcpPort);

			Dlg.PreSelect(Prot, g_sLinkTcpHost);
		}

		if( Dlg.Execute(*afxMainWnd) ) {

			CString Prot = Dlg.GetProt();
			
			if( Prot == L"USB" ) {

				g_uLinkMethod  = 1;

				g_sLinkUsbHost = Dlg.GetHost();
			}

			if( Prot.StartsWith(L"TCP") ) {

				Prot.StripToken(L' ');

				g_uLinkMethod  = 2;

				g_sLinkTcpHost = Dlg.GetHost();

				g_uLinkTcpPort = watoi(Prot);

				g_fLinkTcpSlow = FALSE;
			}

			g_fLinkEmulate = FALSE;

			Link_KillEmulate();

			return TRUE;
		}
	}

	return FALSE;
}

static CString FindBrowseTarget(void)
{
	if( !g_fLinkEmulate ) {

		if( g_uLinkMethod == 1 ) {

			return g_sLinkUsbHost;
		}

		if( g_uLinkMethod == 2 ) {

			return g_sLinkTcpHost;
		}
	}
	else {
		char path[MAX_PATH];

		GetTempPathA(MAX_PATH, path);

		sprintf(path + strlen(path), "c32-ip-%8.8X", GetCurrentProcessId());

		FILE *pFile = fopen(path, "r");

		if( pFile ) {

			char sAddr[128];

			UINT uSize = fread(sAddr, 1, elements(sAddr) - 1, pFile);
			
			if( uSize < elements(sAddr) ) {

				sAddr[uSize] = 0;

				fclose(pFile);
			}

			return CString(sAddr);
		}
	}

	return L"";
}

BOOL DLLAPI Link_Browse(void)
{
	CString Path = FindBrowseTarget();

	if( !Path.IsEmpty() ) {

		ShellExecute(*afxMainWnd,
			     L"open",
			     L"http://" + Path,
			     NULL,
			     NULL,
			     SW_SHOWNORMAL
		);
	}

	return TRUE;
}

BOOL DLLAPI Link_ShowFiles(void)
{
	CString Path = FindBrowseTarget();

	if( !Path.IsEmpty() ) {

		ShellExecute(*afxMainWnd,
			     L"open",
			     L"ftp://" + Path,
			     NULL,
			     NULL,
			     SW_SHOWNORMAL
		);
	}

	return TRUE;
}

BOOL DLLAPI Link_DetectModel(CString &Model)
{
	CLinkAutoDetectDialog Dlg;

	if( Dlg.Execute(*afxMainWnd) ) {

		Model = Dlg.GetModel();

		return TRUE;
	}

	return FALSE;
}

// End of File
