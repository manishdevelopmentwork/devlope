
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2013 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_GMIN8_HPP

#define INCLUDE_GMIN8_HPP

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

class CGraphiteIN8Config : public CCommsItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteIN8Config(void);

		// Group Names
		CString GetGroupName(WORD Group);

		// View Pages
		virtual UINT       GetPageCount(void);
		virtual CString    GetPageName(UINT n);
		virtual CViewWnd * CreatePage(UINT n);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Data Members
		UINT	m_PIRange;
		UINT	m_PVRange;
		BYTE	m_IVIModel;
		UINT	m_ProcDP1;
		UINT	m_ProcDP2;
		UINT	m_ProcDP3;
		UINT	m_ProcDP4;
		UINT	m_ProcDP5;
		UINT	m_ProcDP6;
		UINT	m_ProcDP7;
		UINT	m_ProcDP8;
		UINT	m_InputFilter;
		INT	m_ProcMin1;
		INT	m_ProcMin2;
		INT	m_ProcMin3;
		INT	m_ProcMin4;
		INT	m_ProcMin5;
		INT	m_ProcMin6;
		INT	m_ProcMin7;
		INT	m_ProcMin8;
		INT	m_ProcMax1;
		INT	m_ProcMax2;
		INT	m_ProcMax3;
		INT	m_ProcMax4;
		INT	m_ProcMax5;
		INT	m_ProcMax6;
		INT	m_ProcMax7;
		INT	m_ProcMax8;
		UINT	m_SquareRt1;
		UINT	m_SquareRt2;
		UINT	m_SquareRt3;
		UINT	m_SquareRt4;
		UINT	m_SquareRt5;
		UINT	m_SquareRt6;
		UINT	m_SquareRt7;
		UINT	m_SquareRt8;
		UINT	m_ChanEnable1;
		UINT	m_ChanEnable2;
		UINT	m_ChanEnable3;
		UINT	m_ChanEnable4;
		UINT	m_ChanEnable5;
		UINT	m_ChanEnable6;
		UINT	m_ChanEnable7;
		UINT	m_ChanEnable8;

	protected:
		// Static Data
		static CCommsList const m_CommsList[];

		// Property Filter
		BOOL IncludeProp(WORD PropID);

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

class CGraphiteINV8Config : public CGraphiteIN8Config
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteINV8Config(void);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Input Item
//

class CGraphiteINI8Config : public CGraphiteIN8Config
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteINI8Config(void);

		// View Pages
		UINT       GetPageCount(void);
		CString    GetPageName(UINT n);
		CViewWnd * CreatePage(UINT n);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module
//

class CGMIN8Module : public CGraphiteGenericModule
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// UI Management
		CViewWnd * CreateMainView(void);

		// Comms Object Access
		UINT GetObjectCount(void);
		BOOL GetObjectData(UINT uIndex, CObjectData &Data);

		// Conversion
		BOOL Convert(CPropValue *pValue);

		// Item Properties
		CGraphiteIN8Config * m_pConfig;

	protected:
		// Implementation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module
//

class CGMINI8Module : public CGMIN8Module
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMINI8Module(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Universal 4 Analog Input Module
//

class CGMINV8Module : public CGMIN8Module
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CGMINV8Module(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
//

class CGraphiteIN8ConfigWnd : public CUIViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// Data Members
		CGraphiteIN8Config *m_pItem;

		// Overibables
		void OnAttach(void);

		// UI Update
		void OnUIChange(CItem *pItem, CString Tag);	
		
		// Implementation
		void AddInputs(void);

		// Enabling
		void DoEnables(void);
	};


//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
//

class CGraphiteINI8ConfigWnd : public CGraphiteIN8ConfigWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// UI Update
		void OnUICreate(void);

		// Implementation
		void AddGeneral(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Volt and Current Configuration View
//

class CGraphiteINV8ConfigWnd : public CGraphiteIN8ConfigWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

	protected:
		// UI Update
		void OnUICreate(void);

		// Implementation
		void AddGeneral(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Process Current and Process Volt Modules Window
//

class CGraphiteIN8MainWnd : public CProxyViewWnd
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CGraphiteIN8MainWnd(void);

	protected:
		// Data Members
		CMultiViewWnd   * m_pMult;
		CGMIN8Module    * m_pItem;

		// Overridables
		void OnAttach(void);

		// Implementation
		void AddPages(void);
	};

// End of File

#endif
