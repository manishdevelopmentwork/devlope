
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Aeon Usb Device Framework
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_UsbStandardReq_HPP

#define	INCLUDE_UsbStandardReq_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes 
//

#include "UsbDeviceReq.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Usb Standard Device Requeset
//

class CUsbStandardReq : public CUsbDeviceReq
{
	public:
		// Constructor
		CUsbStandardReq(void);

		// Operations
		void Init(void);
	};

// End of File

#endif
