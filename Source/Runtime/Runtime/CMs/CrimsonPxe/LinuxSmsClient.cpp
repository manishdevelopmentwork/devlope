
#include "intern.hpp"

#include "LinuxSmsClient.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "JsonConfig.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SMS Client Service for Linux
//

CLinuxSmsClient::CLinuxSmsClient(CJsonConfig *pJson)
{
	ApplyConfig(pJson);

	mkdir("/./tmp/crimson/sms/send/", 0755);

	mkdir("/./tmp/crimson/sms/done/", 0755);

	mkdir("/./tmp/crimson/sms/recv/", 0755);

	StdSetRef();
}

// Destructor

CLinuxSmsClient::~CLinuxSmsClient(void)
{
}

// IUnknown

HRESULT CLinuxSmsClient::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryInterface(IClientProcess);

	return CMsgTransport::QueryInterface(riid, ppObject);
}

ULONG CLinuxSmsClient::AddRef(void)
{
	return CMsgTransport::AddRef();
}

ULONG CLinuxSmsClient::Release(void)
{
	return CMsgTransport::Release();
}

// IClientProcess

BOOL CLinuxSmsClient::TaskInit(UINT uTask)
{
	SetThreadName("LinuxSmsClient");

	if( TRUE ) {

		m_fEnable = TRUE;

		piob->RegisterSingleton("msg.sms", 0, (IMsgTransport *) this);

		AddRef();

		return TRUE;
	}

	Release();

	return FALSE;
}

INT CLinuxSmsClient::TaskExec(UINT uTask)
{
	for( ;;) {

		if( m_pWait->Wait(250) ) {

			CAutoLock    Lock(m_pLock);

			INDEX        Index    = m_Queue.GetHead();

			CMsgPayload *pMessage = m_Queue[Index];

			m_Queue.Remove(Index);

			Lock.Free();

			SendMessage(pMessage);
		}

		CheckDone();

		CheckRecv();
	}

	return 0;
}

void CLinuxSmsClient::TaskStop(UINT uTask)
{
}

void CLinuxSmsClient::TaskTerm(UINT uTask)
{
	while( !m_Queue.IsEmpty() ) {

		CMsgPayload *pMessage = m_Queue[m_Queue.GetHead()];

		SendNotify(pMessage, FALSE);

		delete pMessage;
	}

	piob->RevokeSingleton("msg.sms", 0);

	Release();
}

// IMsgTransport

BOOL CLinuxSmsClient::CanAcceptAddress(CString const &Addr)
{
	for( int n = 0; Addr[n]; n++ ) {

		if( !isdigit(Addr[n]) ) {

			return FALSE;
		}
	}

	return TRUE;
}

// Implementation

void CLinuxSmsClient::ApplyConfig(CJsonConfig *pJson)
{
	if( pJson ) {

	}
}

void CLinuxSmsClient::SendMessage(CMsgPayload *pMessage)
{
	CPrintf   Name("%u-%8.8X", getpid(), pMessage);

	CAutoFile Data("/./tmp/crimson/sms/send/" + Name, "w");

	if( Data ) {

		Data.Write(pMessage->m_RcptAddr + '\n');

		Data.Write(pMessage->m_Message  + '\n');

		Data.Close();

		CAutoFile Fire("/./tmp/crimson/sms/send/" + Name + ".go", "w");

		Fire.Close();

		if( pMessage->m_SendNotify ) {

			m_Pend.Append(pMessage);
		}
		else {
			SendNotify(pMessage, TRUE);

			delete pMessage;
		}
	}
	else {
		SendNotify(pMessage, FALSE);

		delete pMessage;
	}
}

void CLinuxSmsClient::CheckDone(void)
{
	for( INDEX n = m_Pend.GetHead(); !m_Pend.Failed(n); ) {

		if( CheckDone(m_Pend[n]) ) {

			INDEX p = n;

			m_Pend.GetNext(n);

			m_Pend.Remove(p);

			continue;
		}

		m_Pend.GetNext(n);
	}
}

bool CLinuxSmsClient::CheckDone(CMsgPayload *pMessage)
{
	CPrintf   Name("%u-%8.8X", getpid(), pMessage);

	CString   Full("/./tmp/crimson/sms/done/" + Name + ".done");

	CAutoFile Done(Full, "r");

	if( Done ) {

		char c = '0';

		Done.Read(&c, 1);

		SendNotify(pMessage, c == '1');

		delete pMessage;

		Done.Close();

		unlink(Full);

		return true;
	}

	return false;
}

void CLinuxSmsClient::CheckRecv(void)
{
	CString Path("/./tmp/crimson/sms/recv/");

	CAutoDirentList List;

	if( List.ScanFiles(Path) ) {

		for( UINT n = 0; n < List.GetCount(); n++ ) {

			CString Name = List[n]->d_name;

			if( Name.EndsWith(".go") ) {

				CString Data(Path + Name.Left(Name.GetLength() - 3));

				AfxGetAutoObject(pBroker, "msg.broker", 0, IMsgBroker);

				if( pBroker ) {

					CAutoFile File(Data, "r");

					if( File ) {

						CMsgPayload *pMessage = New CMsgPayload;

						pMessage->m_Attempts   = 0;
						pMessage->m_SendNotify = 0;
						pMessage->m_Param[0]   = 0;
						pMessage->m_Param[1]   = 0;
						pMessage->m_Param[2]   = 0;
						pMessage->m_Param[3]   = 0;

						pMessage->m_OrigAddr = File.GetLine();

						pMessage->m_Message  = File.GetLine();

						if( !pBroker->SubmitMessage("sms", pMessage) ) {

							delete pMessage;
						}
					}
				}

				unlink(Data);

				unlink(Data + ".go");
			}
		}
	}
}

// End of File
