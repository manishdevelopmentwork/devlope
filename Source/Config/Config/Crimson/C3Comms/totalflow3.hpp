
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TOTALFLOW3_HPP

#define INCLUDE_TOTALFLOW3_HPP

#include "totalflow2.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Type Enumeration
//

enum Table_Definitions {

	tdByte,
	tdWord,
	tdLong,
	tdReal,
	tdString,
	tdMax
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Master Device Options
//

class CTotalFlow3MasterDeviceOptions : public CTotalFlow2MasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlow3MasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Slot Overridables
		BOOL GetSlot(UINT uAddr, CTotalFlowSlot &Slot);
		UINT AddSlot(CTotalFlowSlot &Slot, UINT uStart, UINT uEnd);
		void DeleteSlot(CTotalFlowSlot &Slot);
		void EmptySlots(void);
		UINT GetSlotSize(CTotalFlowSlot const &Slot);
		
		// Slot Access
		UINT FindSlot(CTotalFlowSlot Slot);
		void UpdateSlots(CAddress Addr, INT nSize);
		BOOL MakeNextSlot(CAddress Addr);
		BOOL FindPrevSlot(CAddress Addr, CTotalFlowSlot &Prev);
		BOOL ExpandAddress(CString &Text, CAddress const Addr);
			
	protected:

		// Data Members
		BYTE m_bDef[addrNamed];
		UINT m_uRef[tdMax];
		BOOL m_fUpdate;
		UINT m_uStep;

		// Implementation
		BOOL  AddSlot(CTotalFlowSlot &Slot, UINT uTable);
		BOOL  SetSlot(CTotalFlowSlot &Slot, UINT uTable);
		BOOL  SetThis(CTotalFlowSlot &Slot, UINT uTable);
		BOOL  SetNext(CTotalFlowSlot &Slot, UINT uTable);
		BOOL  SetReferences(CTotalFlowSlot &Slot, CAddress Addr);
		void  SetReference(CTotalFlowSlot Slot);
		DWORD GetReference(UINT uTable, UINT uType, BOOL fString);
		

		// Helpers
		BOOL IsTypeMatch(CTotalFlowSlot Slot, UINT uTable);
		BOOL IsLastReference(CTotalFlowSlot Slot);
		UINT GetTableDefinition(CTotalFlowSlot Slot);
		UINT GetTableDefinition(UINT uTable);
		UINT TableDefinitionToType(UINT uDef);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Master Device Options
//

class CTotalFlow3SerialMasterDeviceOptions : public CTotalFlow3MasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlow3SerialMasterDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT	m_Estab;
		UINT    m_Time1;
		UINT	m_UnkeyDelay;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 TCP/IP Master Device Options
//

class CTotalFlow3TcpMasterDeviceOptions : public CTotalFlow3MasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlow3TcpMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT    m_IP;
		UINT    m_Port;
		UINT    m_Keep;
		UINT    m_Ping;
		UINT    m_Time1;
		UINT    m_Time2;
		UINT    m_Time3;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Master Comms Driver
//

class CTotalFlow3MasterDriver : public CTotalFlow2MasterDriver
{
	public:
		// Constructor
		CTotalFlow3MasterDriver(void);

		// Notifications
		void NotifyExtent(CAddress const &Addr, INT nSize, CItem * pConfig);

		// Address Notificatons
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
				
		// Address Reference
		UINT GetAddress(CAddress Addr);
	
	protected:
		// Implementation
		UINT FindType(CStringArray Array, CError &Error);
		};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 Serial Master Comms Driver
//

class CTotalFlow3SerialMasterDriver : public CTotalFlow3MasterDriver
{
	public:
		// Constructor
		CTotalFlow3SerialMasterDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Enhanced v3 TCP/IP Master Comms Driver
//

class CTotalFlow3TcpMasterDriver : public CTotalFlow3MasterDriver
{
	public:
		// Constructor
		CTotalFlow3TcpMasterDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDeviceConfig(void);		
	};

#endif

// End of File
