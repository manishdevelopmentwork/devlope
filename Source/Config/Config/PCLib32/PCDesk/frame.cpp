
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Desktop Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();
	
//////////////////////////////////////////////////////////////////////////
//
// Generic Frame Window
//

// Dynamic Class

AfxImplementDynamicClass(CFrameWnd, CMenuWnd);

// Constructor

CFrameWnd::CFrameWnd(void)
{
	m_pView = NULL;
	}

// Attributes

CViewWnd * CFrameWnd::GetView(void) const
{
	return m_pView;
	}
		
// Operations

void CFrameWnd::SetView(CViewWnd *pView)
{
	if( m_pView != pView ) {
	
		if( m_hWnd ) {

			if( m_pView ) {

				m_pView->DestroyWindow(TRUE);
				}
			
			m_pView = pView;
		
			if( m_pView ) {
			
				AfxAssert(!pView->GetHandle());
				
				OnCreateView();
				
				m_pView->ShowWindow(SW_SHOW);

				m_pView->SetCurrent(TRUE);
			
				m_pView->SetFocus();
				}
				
			return;
			}

		m_pView = pView;
		}
	}

// Client Calculation

CRect CFrameWnd::GetClientRect(void) const
{
	return CWnd::GetClientRect();
	}

// Routing Control

BOOL CFrameWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( m_pView && m_pView->IsWindow() ) {

		if( m_pView->RouteMessage(Message, lResult) ) {

			return TRUE;
			}
		}

	return CWnd::OnRouteMessage(Message, lResult);
	}

// View Creation

void CFrameWnd::OnCreateView(void)
{
	m_pView->Create( WS_CHILD | WS_CLIPCHILDREN,
			 GetClientRect(),
			 ThisObject, IDVIEW, NULL
			 );
	}
		
// Message Map

AfxMessageMap(CFrameWnd, CMenuWnd)
{
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_CANCELMODE)
	AfxDispatchMessage(WM_ERASEBKGND)
	AfxDispatchMessage(WM_GETMETRIC)
	AfxDispatchMessage(WM_MOVE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_SIZE)

	AfxMessageEnd(CFrameWnd)
	};

// Message Handlers

void CFrameWnd::OnPostCreate(void)
{
	if( m_pView ) {
	
		OnCreateView();

		CRect Rect = GetClientRect();
	
		CSize Size = m_pView->GetWindowRect().GetSize();
		
		m_pView->SendMessage(WM_GETMETRIC, MC_BEST_SIZE, LPARAM(&Size));
		
		// LATER -- This won't include toolbars etc.

		AdjustWindowSize(Size);
		
		SetWindowSize(Size, TRUE);

		m_pView->ShowWindow(SW_SHOW);
		
		m_pView->SetCurrent(TRUE);
		}
	}

void CFrameWnd::OnCancelMode(void)
{
	if( m_pView ) {
	
		m_pView->SendMessage(WM_CANCELMODE);
		}
	}

BOOL CFrameWnd::OnEraseBkGnd(CDC &DC)
{
	return TRUE;
	}

void CFrameWnd::OnGetMetric(UINT uCode, CSize &Size)
{
	if( uCode == MC_DIALOG_ORG ) {
		
		CRect Rect = GetClientRect();
	
		Size.cx += Rect.left;
		
		Size.cy += Rect.top;

		if( m_pView ) {
			
			LPARAM lParam = LPARAM(&Size);
		
			m_pView->SendMessage(WM_GETMETRIC, uCode, lParam);
			}
		
		return;
		}

	if( uCode == MC_BEST_SIZE ) {
	
		if( m_pView ) {
			
			Size = m_pView->GetWindowRect().GetSize();
			
			LPARAM lParam = LPARAM(&Size);
		
			m_pView->SendMessage(WM_GETMETRIC, MC_BEST_SIZE, lParam);
			
			AdjustWindowSize(Size);
			}
			
		return;
		}
	}

void CFrameWnd::OnMove(CPoint Pos)
{
	SendMessage(WM_CANCELMODE);
	}

void CFrameWnd::OnSetFocus(CWnd &Wnd)
{
	if( m_pView ) {
	
		m_pView->SetFocus();
		}
	}

void CFrameWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_MAXIMIZED || uCode == SIZE_RESTORED ) {

		SendMessage(WM_CANCELMODE);
		
		if( m_pView ) {

			SizeView();
			}
		}
	}

// Implementation

void CFrameWnd::SizeView(void)
{
	m_pView->MoveWindow(GetClientRect(), TRUE);
	}

// End of File
