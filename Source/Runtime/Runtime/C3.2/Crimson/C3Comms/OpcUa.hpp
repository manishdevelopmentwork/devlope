
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Tables
//

enum
{
	addrString = 1,
	addrArray  = 80
};

//////////////////////////////////////////////////////////////////////////
//
// OPC UA Client Driver
//

class COpcUaDriver : public CMasterDriver
{
public:
	// Constructor
	COpcUaDriver(void);

	// Destructor
	~COpcUaDriver(void);

	// Configuration
	DEFMETH(void) Load(LPCBYTE pData);
	DEFMETH(void) CheckConfig(CSerialConfig &Config);

	// Master Flags
	DEFMETH(WORD) GetMasterFlags(void);

	// User Access
	DEFMETH(UINT) DrvCtrl(UINT uFunc, PCTXT Value);

	// Device
	DEFMETH(CCODE) DeviceOpen(ICommsDevice *pDevice);
	DEFMETH(CCODE) DeviceClose(BOOL fPersist);

	// Management
	DEFMETH(void) Attach(IPortObject *pPort);
	DEFMETH(void) Detach(void);
	DEFMETH(void) Open(void);

	// Entry Points
	DEFMETH(void)  Service(void);
	DEFMETH(CCODE) Ping(void);
	DEFMETH(CCODE) Read(AREF Addr, PDWORD pData, UINT uCount);
	DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

protected:
	// Node Reference
	struct CNode
	{
		UINT   m_uSize;
		UINT   m_Type;
		BOOL   m_fUsed;
		BOOL   m_fGood;
		PDWORD m_pData;
		DWORD  m_Data;
	};

	// Device Context
	struct CContext
	{
		BOOL    m_fValid;
		UINT    m_uLump;
		UINT    m_uDevice;
		BOOL    m_fPartial;
		UINT    m_StrSize;
		UINT    m_StrCode;
		UINT    m_StrPack;
		UINT    m_StrTable;
		UINT    m_StrPitch;
		UINT    m_uNodes;
		UINT	m_uScalars;
		CNode * m_pNodes;
		UINT    m_uSlots;
		WORD  * m_pSlots;
	};

	// Data Members
	CContext     * m_pCtx;
	IOpcUaClient * m_pClient;
	UINT	       m_uDebug;
};

// End of File
