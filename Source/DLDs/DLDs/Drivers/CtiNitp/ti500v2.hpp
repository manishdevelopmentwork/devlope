#include "nitp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TI 500 NITP v2 Master Serial Driver
//

class CTi500v2Driver : public CNitpDriver
{
	public:
		// Constructor
		CTi500v2Driver(void);

		// Destructor
		~CTi500v2Driver(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);
		DEFMETH(void) CheckConfig(CSerialConfig &Config);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
	protected:

		// Device Context
		struct CContext 
		{
			BOOL	 m_fBlock;
			};

		// Data Members
		CContext * m_pCtx;
		
		// Transport
		BOOL Transact(void);
		BOOL Send(void);
		BOOL RecvFrame(void);

		// Specific
		BOOL IsBlockOp(void);
		void AddElement(UINT uOffset, UINT uCount);
		void AddTaskCode(BOOL fWrite);

	};

// End of File
