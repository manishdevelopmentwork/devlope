
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Windows Support
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Clipboard Object
//

// Runtime Class

AfxImplementRuntimeClass(CClipboard, CObject);

// Constructors

CClipboard::CClipboard(HWND hWnd)
{
	m_fValid = OpenClipboard(hWnd);
	}
		
// Destructor

CClipboard::~CClipboard(void)
{
	if( m_fValid ) {
		
		CloseClipboard();
		}
	}

// Attributes

BOOL CClipboard::IsValid(void) const
{
	return m_fValid;
	}

BOOL CClipboard::IsFormatAvailable(UINT uFormat) const
{
	return m_fValid ? IsClipboardFormatAvailable(uFormat) : FALSE;
	}

UINT CClipboard::GetFormatCount(void) const
{
	return m_fValid ? CountClipboardFormats() : 0;
	}

UINT CClipboard::EnumFormats(UINT uFormat) const
{
	return m_fValid ? EnumClipboardFormats(uFormat) : 0;
	}
		
// Operations

HGLOBAL CClipboard::GetData(UINT uFormat)
{
	return m_fValid ? GetClipboardData(uFormat) : NULL;
	}

CString CClipboard::GetText(UINT uFormat)
{
	if( m_fValid ) {

		HANDLE hData = GetClipboardData(uFormat);

		if( hData ) {

			PCTXT pData = PCTXT(GlobalLock(hData));

			if( pData ) {

				CString Text(pData);

				GlobalUnlock(hData);

				return Text;
				}
			}
		}

	return L"";
	}

BOOL CClipboard::SetData(UINT uFormat, HGLOBAL hData)
{
	if( m_fValid ) {
		
		SetClipboardData(uFormat, hData);

		return TRUE;
		}

	return FALSE;
	}

BOOL CClipboard::SetText(PCTXT pText)
{
	if( m_fValid ) {

		HGLOBAL hData = GlobalAlloc(GHND, sizeof(TCHAR) * (wstrlen(pText) + 1));

		AfxAssume(hData);

		PTXT    pData = PTXT(GlobalLock(hData));

		lstrcpy(pData, pText);

		GlobalUnlock(hData);

		EmptyClipboard();

		SetClipboardData(CF_UNICODETEXT, hData);

		// cppcheck-suppress memleak

		return TRUE;
		}

	return FALSE;
	}

BOOL CClipboard::Empty(void)
{
	if( m_fValid ) {
		
		EmptyClipboard();

		return TRUE;
		}

	return FALSE;
	}

// End of File
