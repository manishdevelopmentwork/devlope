
#include "Intern.hpp"

#include "SdDrive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Standard Runtime Environment
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// SD Drive 
//

// Instantiator

static IUnknown * Create_SdDrive(PCTXT pName)
{
	return New CSdDrive();
	}

// Registration

void Register_SdDrive(void)
{
	piob->RegisterInstantiator("fs.sddrive", Create_SdDrive);
	}

// Constructor

CSdDrive::CSdDrive(void)
{
	m_pSdHost = NULL;

	m_pMutex  = Create_Mutex();
	}

// Destructor

CSdDrive::~CSdDrive(void)
{
	AfxRelease(m_pSdHost);

	AfxRelease(m_pMutex);
	}

// IDeviceEx

BOOL CSdDrive::Open(IUnknown *pUnk)
{
	return pUnk->QueryInterface(AfxAeonIID(ISdHost), (void **) &m_pSdHost) == S_OK;
	}

// IBlockDevice

BOOL CSdDrive::IsReady(void)
{
	return m_pSdHost->IsCardReady();
	}

void CSdDrive::Attach(IEvent *pEvent)
{
	m_pSdHost->AttachCardEvent(pEvent);
	}

UINT CSdDrive::GetSectorCount(void)
{
	return m_pSdHost->GetSectorCount();
	}

UINT CSdDrive::GetCylinderCount(void)
{
	return 0;
	}

UINT CSdDrive::GetHeadCount(void)
{
	return 1;
	}

UINT CSdDrive::GetSectorsPerHead(void)
{
	return 0;
	}

BOOL CSdDrive::WriteSector(UINT uSector, PCBYTE pData)
{
	m_pMutex->Wait(FOREVER);

	if( m_pSdHost->WriteSector(uSector, pData) ) {

		if( CBlockDevice::WriteSector(uSector, pData) ) {

			m_pMutex->Free();

			return true;
			}
		}
	
	m_pMutex->Free();

	return false;
	}

BOOL CSdDrive::ReadSector(UINT uSector, PBYTE pData)
{
	m_pMutex->Wait(FOREVER);

	if( m_pSdHost->ReadSector(uSector, pData) ) {

		if( CBlockDevice::ReadSector(uSector, pData) ) {

			m_pMutex->Free();
			
			return true;
			}
		}
	
	m_pMutex->Free();

	return false;
	}

// End of File