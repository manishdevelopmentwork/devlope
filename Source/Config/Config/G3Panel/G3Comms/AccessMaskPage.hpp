
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_AccessMaskPage_HPP

#define INCLUDE_AccessMaskPage_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CAccessMask;

//////////////////////////////////////////////////////////////////////////
//
// Access Mask Page
//

class CAccessMaskPage : public CUIStdPage
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CAccessMaskPage(CAccessMask *pMask);

		// Operations
		BOOL LoadIntoView(IUICreate *pView, CItem *pItem);

	protected:
		// Data Members
		CAccessMask * m_pMask;
	};

// End of File

#endif
