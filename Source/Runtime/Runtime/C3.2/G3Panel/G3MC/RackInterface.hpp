
#include "Intern.hpp"

/////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_RackInterface_HPP

#define	INCLUDE_RackInterface_HPP

////////////////////////////////////////////////////////////////////////
//
// Message Types
//

enum
{
	msgNone,
	msgFrame,
	msgError,
	msgAck,
	msgNak,
	msgBusy,
	msgEnq,
	msgPoll,
};

////////////////////////////////////////////////////////////////////////
//
// Error States
//

enum
{
	commsOkay,
	commsHard,
	commsSoft,
};

////////////////////////////////////////////////////////////////////////
//
// Handler
//

interface IRackHandler : public IUnknown
{
	AfxDeclareIID(203, 1);

	virtual UINT METHOD Transact(BYTE bDrop, PBYTE pData, UINT uCount) = 0;
	virtual UINT METHOD Transact(BYTE bDrop, UINT  Msg)		   = 0;
	virtual BOOL METHOD GetReply(BYTE bDrop, PBYTE pData)		   = 0;
	virtual BOOL METHOD Recycle(BYTE bDrop)			           = 0;
};

////////////////////////////////////////////////////////////////////////
//
// Driver
//

interface IRackDriver : public IUnknown
{
	AfxDeclareIID(203, 2);

	// Shared
	virtual UINT RxGeneral(BYTE bDrop)							= 0;

	// Boot
	virtual UINT BootTxForceReset(BYTE bDrop)						= 0;
	virtual UINT BootTxCheckModel(BYTE bDrop)						= 0;
	virtual UINT BootTxCheckVersion(BYTE bDrop, PBYTE pGuid)				= 0;
	virtual UINT BootTxProgramReset(BYTE bDrop)						= 0;
	virtual UINT BootTxClearProgram(BYTE bDrop, DWORD dwSize)				= 0;
	virtual UINT BootTxProgramSize(BYTE bDrop, DWORD dwSize)				= 0;
	virtual UINT BootTxWriteProgram(BYTE bDrop, WORD wAddr, PBYTE pData, WORD wCount)	= 0;
	virtual UINT BootTxWriteProgram32(BYTE bDrop, DWORD dwAddr, PBYTE pData, WORD wCount)	= 0;
	virtual UINT BootTxWriteVerify(BYTE bDrop, DWORD Crc32)					= 0;
	virtual UINT BootTxWriteVersion(BYTE bDrop, PBYTE pGuid)				= 0;
	virtual UINT BootTxStartProgram(BYTE bDrop)						= 0;
	virtual UINT BootRxCheckModel(BYTE bDrop, BYTE &ModelID)				= 0;
	virtual UINT BootRxWriteVerify(BYTE bDrop, BOOL &fSame)					= 0;
	virtual UINT BootRxCheckVersion(BYTE bDrop, BOOL &fSame)				= 0;

	// Config
	virtual UINT ConfigTxCheckVersion(BYTE bDrop, PBYTE pGuid)				= 0;
	virtual UINT ConfigTxClearConfig(BYTE bDrop)						= 0;
	virtual UINT ConfigTxWriteConfig(BYTE bDrop, PCBYTE &pData)				= 0;
	virtual UINT ConfigTxWriteVersion(BYTE bDrop, PBYTE pGuid)				= 0;
	virtual UINT ConfigTxStartSystem(BYTE bDrop)						= 0;
	virtual UINT ConfigTxCheckStatus(BYTE bDrop)						= 0;
	virtual UINT ConfigRxCheckVersion(BYTE bDrop, BOOL &fSame)				= 0;
	virtual UINT ConfigRxWriteConfig(BYTE bDrop)						= 0;
	virtual UINT ConfigRxCheckStatus(BYTE bDrop, BOOL &fRun)				= 0;

	// Data
	virtual void DataTxStartFrame(void)							= 0;
	virtual BOOL DataTxWrite(WORD PropID, DWORD Data)					= 0;
	virtual BOOL DataTxRead(WORD PropID)							= 0;
	virtual UINT DataTxSend(BYTE bDrop)							= 0;
	virtual UINT DataRxData(BYTE bDrop, PBYTE &pData)					= 0;
	virtual BOOL DataTooFull(WORD PropID)							= 0;

	// Tunneling
	virtual UINT TunnelTx(BYTE bDrop, PBYTE pData)						= 0;
	virtual UINT TunnelRx(BYTE bDrop, PBYTE pData)						= 0;

	// Transport
	virtual UINT SendEnq(BYTE bDrop)							= 0;
	virtual BOOL Recycle(BYTE bDrop)							= 0;
};

// End of File

#endif