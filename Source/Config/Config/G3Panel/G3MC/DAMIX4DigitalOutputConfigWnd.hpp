
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DAMix4DigitalOutputWnd_HPP

#define INCLUDE_DAMix4DigitalOutputWnd_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Objects
//

class CDAMix4DigitalOutputConfig;
class CDAMix4Module;

//////////////////////////////////////////////////////////////////////////
//
// 4-Channel Mix Module Digital Output Config Window
//

class CDAMix4DigitalOutputConfigWnd : public CUIViewWnd
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

protected:
	// Data Members
	CDAMix4DigitalOutputConfig * m_pItem;
	CDAMix4Module              * m_pModule;

	// Overibables
	void OnAttach(void);

	// UI Update
	void OnUICreate(void);
	void OnUIChange(CItem *pItem, CString Tag);

	// Implementation
	void AddOutputs(void);
	void DoEnables(UINT uIndex);

	// Data Access
	UINT GetInteger(CMetaItem *pItem, CString Tag);
};

// End of File

#endif
