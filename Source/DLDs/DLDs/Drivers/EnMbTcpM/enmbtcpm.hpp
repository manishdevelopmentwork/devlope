
//////////////////////////////////////////////////////////////////////////
//
// Modbus Data Spaces
//

#define	SPACE_HOLDING	0x01
#define	SPACE_ANALOG	0x02
#define	SPACE_OUTPUT	0x03
#define	SPACE_INPUT	0x04
#define	SPACE_HOLD32	0x05
#define	SPACE_ANALOG32	0x06

//////////////////////////////////////////////////////////////////////////
//
// Modbus TCP/IP Master Driver
//

class CEnModbusTCPMaster : public CMasterDriver
{
	public:
		// Constructor
		CEnModbusTCPMaster(void);

		// Destructor
		~CEnModbusTCPMaster(void);

		// Configuration
		DEFMETH(void) Load(LPCBYTE pData);

		// Management
		DEFMETH(void) Attach(IPortObject *pPort);
		DEFMETH(void) Open(void);
		
		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Device Context
		struct CContext
		{
			DWORD	 m_IP;
			UINT	 m_uPort;
			BYTE	 m_bUnit;
			BOOL	 m_fKeep;
			BOOL	 m_fPing;
			UINT	 m_uTime1;
			UINT	 m_uTime2;
			UINT	 m_uTime3;
			WORD	 m_wTrans;
			ISocket *m_pSock;
			UINT	 m_uLast;
			BOOL	 m_fDisable15;
			BOOL	 m_fDisable16;
			BOOL	 m_fDisable5;
			BOOL	 m_fDisable6;
			BOOL	 m_fAscii;
			UINT	 m_PingReg;
			UINT	 m_uMax01;
			UINT	 m_uMax02;
			UINT	 m_uMax03;
			UINT	 m_uMax04;
			UINT	 m_uMax15;
			UINT	 m_uMax16;

			};

		// Data Members
		CContext * m_pCtx;
		BYTE	   m_bTxBuff[300];
		BYTE	   m_bRxBuff[300];
		UINT	   m_uPtr;
		UINT	   m_uKeep;
		LPCTXT	   m_pHex;
		CRC16	   m_CRC;
		BYTE	   m_bCheck;
		UINT	   m_uMaxWords;
		UINT	   m_uMaxBits;

		// Socket Management
		BOOL CheckSocket(void);
		BOOL OpenSocket(void);
		void CloseSocket(BOOL fAbort);
		
		// Frame Building
		void StartFrame(BYTE bOpcode);
		void AddByte(BYTE bData, BOOL fCheck);
		void AddWord(WORD wData);
		void AddLong(DWORD dwData);
		void AddCheck(void);

		// Helpers
		BOOL IsHex(BYTE bData);
		WORD FromHex(BYTE bData);
		 				
		// Transport Layer
		BOOL SendFrame(void);
		BOOL RecvFrame(BOOL fWrite);
		BOOL RecvBinaryFrame(BOOL fWrite);
		BOOL RecvAsciiFrame(BOOL fWrite);
		BOOL Transact(BOOL fIgnore);
		BOOL CheckFrame(void);

		// Read Handlers
		CCODE DoWordRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoWordWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoLongWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Helpers
		void Limit(UINT &uData, UINT uMin, UINT uMax);

	};

// End of File
