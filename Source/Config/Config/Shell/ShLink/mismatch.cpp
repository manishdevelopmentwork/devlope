
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 Download Link
//
// Copyright (c) 1993-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Mismatched Configuration Dialog
//

class CMismatchDialog : public CStdDialog
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructors
	CMismatchDialog(CDatabase *pDbase, PBYTE pFlags);

protected:
	// Data Members
	CDatabase * m_pDbase;
	PBYTE	    m_pFlags;

	// Message Map
	AfxDeclareMessageMap();

	// Message Handlers
	BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
	HOBJ OnCtlColorStatic(CDC &DC, CWnd &Wnd);

	// Command Handlers
	BOOL OnCommandOK(UINT uID);
	BOOL OnCommandCancel(UINT uID);
};

//////////////////////////////////////////////////////////////////////////
//
// Mismatched Configuration Dialog
//

// Runtime Class

AfxImplementRuntimeClass(CMismatchDialog, CStdDialog);

// Internal API

BOOL MismatchDialog(CWnd &Wnd, CDatabase *pDbase, BYTE &bFlags)
{
	return CMismatchDialog(pDbase, &bFlags).Execute(Wnd);
}

// Constructors

CMismatchDialog::CMismatchDialog(CDatabase *pDbase, PBYTE pFlags)
{
	m_pDbase = pDbase;

	m_pFlags = pFlags;

	SetName(L"MismatchDlg");
}

// Message Map

AfxMessageMap(CMismatchDialog, CStdDialog)
{
	AfxDispatchMessage(WM_INITDIALOG)
	AfxDispatchMessage(WM_CTLCOLORSTATIC)

	AfxDispatchCommand(IDOK, OnCommandOK)
	AfxDispatchCommand(IDCANCEL, OnCommandCancel)

	AfxMessageEnd(CMismatchDialog)
};

// Message Handlers

BOOL CMismatchDialog::OnInitDialog(CWnd &Focus, DWORD dwData)
{
	::SetForegroundWindow(m_hWnd);

	BOOL fFocus = FALSE;

	for( UINT n = 0; n < 4; n++ ) {

		CStatic   &Label = (CStatic   &) GetDlgItem(100 * (n+1) + 0);

		CComboBox &Combo = (CComboBox &) GetDlgItem(100 * (n+1) + 1);

		if( *m_pFlags & (1 << n) ) {

			Label.SetWindowText(IDS("EDITED"));

			Combo.AddString(IDS("Leave"),     0);
			Combo.AddString(IDS("Overwrite"), 1);
			Combo.AddString(IDS("Exclude"),   2);

			Combo.SetCurSel(0);

			if( !fFocus ) {

				SetDlgFocus(Combo);

				fFocus = TRUE;
			}
		}
		else {
			Label.SetWindowText(IDS("UNEDITED"));

			Combo.EnableWindow(FALSE);

			Combo.ShowWindow(SW_HIDE);
		}
	}

	return FALSE;
}

HOBJ CMismatchDialog::OnCtlColorStatic(CDC &DC, CWnd &Wnd)
{
	UINT id = Wnd.GetID();

	if( id == 100 || id == 200 || id == 300 || id == 400 ) {

		if( *m_pFlags & (1 << ((id - 100) / 100)) ) {

			DC.SetTextColor(CColor(192, 0, 0));
		}
		else {
			DC.SetTextColor(CColor(0, 128, 0));
		}
	}

	return CStdDialog::OnCtlColorStatic(DC, Wnd);
}

// Command Handlers

BOOL CMismatchDialog::OnCommandOK(UINT uID)
{
	BOOL fExclude = FALSE;

	for( UINT n = 0; n < 4; n++ ) {

		CComboBox &Combo = (CComboBox &) GetDlgItem(100 * (n+1) + 1);

		if( *m_pFlags & (1 << n) ) {

			switch( Combo.GetCurSelData() ) {

				case 2:
				{
					m_pDbase->GetSystemItem()->DisableDeviceConfig(n);

					fExclude = TRUE;
				}

				case 0:
				{
					*m_pFlags &= ~(1 << n);
				}
				break;
			}
		}
	}

	if( fExclude ) {

		CString Text;

		Text += IDS("Excluded items will not be downloaded again until you re-enable\n");

		Text += IDS("them by editing the 'Included' property in the element's settings.");

		Information(Text);
	}

	EndDialog(TRUE);

	return TRUE;
}

BOOL CMismatchDialog::OnCommandCancel(UINT uID)
{
	EndDialog(FALSE);

	return TRUE;
}

// End of File
