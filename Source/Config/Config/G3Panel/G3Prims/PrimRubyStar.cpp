
#include "intern.hpp"

#include "PrimRubyStar.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Ruby Star Primitive
//

// Dynamic Class

AfxImplementDynamicClass(CPrimRubyStar, CPrimRubyPolygon)

// Constructor

CPrimRubyStar::CPrimRubyStar(void)
{
	}

// Meta Data

void CPrimRubyStar::AddMetaData(void)
{
	CPrimRubyPolygon::AddMetaData();

	Meta_AddInteger(AltRad);

	Meta_SetName((IDS_STAR));
	}

// End of File
