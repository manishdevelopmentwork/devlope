
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Symbol Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_G3SYMS_HPP
	
#define	INCLUDE_G3SYMS_HPP

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <c3ui.hpp>

//////////////////////////////////////////////////////////////////////////
//								
// Resource IDs
//

#include "g3syms.hxx"

//////////////////////////////////////////////////////////////////////////
//								
// Export Management
//

#undef  DLLAPI

#ifdef	PROJECT_G3SYMS

#define	DLLAPI __declspec(dllexport)

#else

#define	DLLAPI __declspec(dllimport)

#pragma	comment(lib, "g3syms.lib")

#endif

//////////////////////////////////////////////////////////////////////////
//								
// Symbol Factory Access
//

DLLAPI void	    Sym_Init        (void);
DLLAPI void	    Sym_Term        (void);
DLLAPI void	    Sym_PreferNew   (BOOL fNew);
DLLAPI UINT         Sym_GetCatCount (void);
DLLAPI CString      Sym_GetCatDesc  (UINT uCat);
DLLAPI UINT         Sym_GetCatSlots (UINT uCat);
DLLAPI BOOL         Sym_IsValid     (UINT uCat, UINT uSlot);
DLLAPI UINT         Sym_GetType     (UINT uCat, UINT uSlot);
DLLAPI BOOL	    Sym_CanShade    (UINT uCat, UINT uSlot);
DLLAPI DWORD        Sym_GetHandle   (UINT uCat, UINT uSlot);
DLLAPI CString      Sym_GetDesc     (UINT uCat, UINT uSlot);
DLLAPI UINT         Sym_FindByHandle(UINT uCat, DWORD dwHandle);
DLLAPI HBITMAP	    Sym_LoadBitmap  (UINT uCat, UINT uSlot);
DLLAPI HBITMAP	    Sym_LoadBitmap  (UINT uCat, UINT uSlot, int &cx, int &cy);
DLLAPI HENHMETAFILE Sym_LoadMetaFile(UINT uCat, UINT uSlot);
DLLAPI BOOL         Sym_LoadPng     (UINT uCat, UINT uSlot, UINT uMode, DWORD Fill, DWORD Back, CSize Size, BOOL fKeep, CByteArray &Data);
DLLAPI UINT         Sym_LoadData    (UINT uCat, UINT uSlot, PBYTE &pCopy);
DLLAPI BOOL         Sym_LoadData    (UINT uCat, UINT uSlot, CByteArray &Data);

// End of File

#endif
