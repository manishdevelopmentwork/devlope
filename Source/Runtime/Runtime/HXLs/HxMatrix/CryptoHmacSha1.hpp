
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHmacSha1_HPP

#define INCLUDE_CryptoHmacSha1_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHmac.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA1 Cryptographic Hash
//

class CCryptoHmacSha1 : public CCryptoHmac
{
	public:
		// Constructor
		CCryptoHmacSha1(void);

		// ICryptoHmac
		CString GetName(void);
		void    Initialize(PCBYTE pPass, UINT uPass);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE         m_bHash[SHA1_HASH_SIZE];
		psHmacSha1_t m_Ctx;
	};

// End of File

#endif
