
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Stubs for Malloc
//

// Static Data

static IMalloc * m_pMalloc = NULL;

// Code

global void Bind_Malloc(void)
{
	AfxGetObject("malloc", 0, IMalloc, m_pMalloc);
	}

global void Free_Malloc(void)
{
	m_pMalloc->Release();

	m_pMalloc = NULL;
	}

clink void * _malloc_r(struct _reent *ptr, size_t size)
{
	return m_pMalloc->Alloc(size);
	}

clink void * _memalign_r(struct _reent *ptr, UINT uAlign, size_t size)
{
	return m_pMalloc->Align(uAlign, size);
	}

clink void * _calloc_r(struct _reent *ptr, UINT uCount, size_t size)
{
	return m_pMalloc->CAlloc(uCount, size);
	}

clink void * _realloc_r(struct _reent *ptr, PVOID pData, size_t size)
{
	return m_pMalloc->ReAlloc(pData, size);
	}

clink void _free_r(struct _reent *, PVOID pData)
{
	m_pMalloc->Free(pData);
	}

void * operator new (size_t size)
{
	return m_pMalloc->Alloc(size);
	}

void * operator new (size_t size, char const *file, int line)
{
	return m_pMalloc->Alloc(size, file, line);
	}

void * operator new [] (size_t size)
{
	return m_pMalloc->Alloc(size);
	}

void * operator new [] (size_t size, char const *file, int line)
{
	return m_pMalloc->Alloc(size, file, line);
	}

void operator delete (void *data)
{
	m_pMalloc->Free(data);
	}

void operator delete (void *data, size_t size)
{
	m_pMalloc->Free(data);
	}

void operator delete [] (void *data)
{
	m_pMalloc->Free(data);
	}

void operator delete [] (void *data, size_t size)
{
	m_pMalloc->Free(data);
	}

UINT AfxMarkMemory(void)
{
	return m_pMalloc->MarkMemory();
	}

void AfxDumpMemory(IDiagOutput *pOut, UINT uMark)
{
	m_pMalloc->DumpMemory(pOut, uMark);
	}

// End of File
