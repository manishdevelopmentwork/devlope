
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_IMAGE_HPP
	
#define	INCLUDE_IMAGE_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CUITextElementImage;
class CUIElementImage;
class CImagePreviewWnd;
class CImageEditDialog;

////////////////////////////////////////////////////////////////////////
//
// Textual UI Element -- Image
//

class CUITextPrimImage : public CUITextElement
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextPrimImage(void);

	protected:
		// Overridables
		CString OnGetAsText(void);
		UINT    OnSetAsText(CError &Error, CString Text);
	};

//////////////////////////////////////////////////////////////////////////
//
// Graphical UI Element -- Image
//

class CUIPrimImage : public CUIControl
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUIPrimImage(void);

		// Attributes
		BOOL HasPrevious(void) const;
		BOOL CanCopyPrevious(void) const;
		UINT GetImage(void) const;

		// Operations
		void GetOpts(CImageOpts &Opts);
		void SetOpts(CImageOpts &Opts);
		BOOL CopyPrevious(void);
		BOOL CopyPreviousOpts(CImageOpts &Opts);
		void DoubleClick(void);

	protected:
		// Data Members
		CString		   m_Name;
		CLayItemText     * m_pTextLayout;
		CLayItemSize     * m_pPictLayout;
		CLayItemText     * m_pFileLayout;
		CLayItemText     * m_pPickLayout;
		CLayItemText     * m_pWipeLayout;
		CLayItemText     * m_pEditLayout;
		CStatic          * m_pTextCtrl;
		CImagePreviewWnd * m_pPictCtrl;
		CCtrlWnd         * m_pFileCtrl;
		CCtrlWnd         * m_pPickCtrl;
		CCtrlWnd         * m_pWipeCtrl;
		CCtrlWnd         * m_pEditCtrl;
		CWnd	         * m_pWnd;
		CUISystem	 * m_pSystem;
		CImageManager    * m_pImages;
		CPrimImage       * m_pImage;
		CSize		   m_Aspect;
		BOOL		   m_fLimit;
		BOOL               m_fEnable;

		// Core Overridables
		void OnBind(void);
		void OnLayout(CLayFormation *pForm);
		void OnCreate(CWnd &Wnd, UINT &uID);
		void OnMove(void);
		void OnRename(PCTXT pName);
		void OnEnable(BOOL fEnable);

		// Notification Handlers
		BOOL OnNotify(UINT uID, UINT uCode);

		// Data Overridables
		BOOL OnCanAcceptData(IDataObject *pData, DWORD &dwEffect);
		BOOL OnAcceptData(IDataObject *pData);
		void OnLoad(void);

		// Implementation
		void FindAspect(void);
		void FindManager(void);
		BOOL DoBrowse(void);
		BOOL DoPaste(void);
		BOOL DoPick(void);
		BOOL DoClear(void);
		BOOL DoEdit(void);
		void DoEnables(void);

		// Friends
		friend class CImagePreviewWnd;
		friend class CImageEditDialog;
	};

//////////////////////////////////////////////////////////////////////////
//
// Image Preview Window
//

class CImagePreviewWnd : public CCtrlWnd, public IDropTarget
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CImagePreviewWnd(CUIPrimImage *pUI);

		// Destructor
		~CImagePreviewWnd(void);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Operations
		void SetImage(UINT uImage);

	protected:
		// Data Members
		CUIPrimImage   * m_pUI;
		CImageManager  * m_pImages;
		UINT             m_uImage;
		CImageFileItem * m_pFile;
		CBitmap	         m_Bitmap;

		// Drop Context
		CDropHelper m_DropHelp;
		DWORD	    m_dwEffect;
		UINT        m_uDrop;

		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		void OnPostCreate(void);
		void OnPaint(void);
		BOOL OnEraseBkGnd(CDC &DC);
		void OnSize(UINT uType, CSize Size);
		void OnEnable(BOOL fEnable);
		void OnLButtonDblClk(UINT uCode, CPoint Pos);

		// Command Handlers
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);

		// Implementation
		CRect FindDrawRect(void);
		void  FindImageManager(void);
		void  Render(void);
		BOOL  SetDrop(UINT uDrop);
	};

//////////////////////////////////////////////////////////////////////////
//
// Image Editing Dialog
//

class CImageEditDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CImageEditDialog(CUIPrimImage *pUI);
		
	protected:
		// Data Members
		CUIPrimImage   * m_pUI;
		CImageManager  * m_pImages;
		CImageFileItem * m_pFile;
		UINT             m_uImage;
		CImageOpts       m_Opts;
				 
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);
		void OnPaint(void);
		void OnHScroll(UINT uCode, int nPos, CWnd &Ctrl);
		void OnVScroll(UINT uCode, int nPos, CWnd &Ctrl);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandReset(UINT uID);
		BOOL OnCommandCopyPrev(UINT uID);
		BOOL OnCommandShow(UINT uID);
		BOOL OnCommandFlipH(UINT uID);
		BOOL OnCommandFlipV(UINT uID);

		// Implementation
		CRect FindDrawRect(void);
		void  FindManager(void);
		void  LoadBars(void);
		void  LoadChecks(void);
	};	

//////////////////////////////////////////////////////////////////////////
//
// Image Selection Dialog
//

class DLLAPI CImageSelectDialog : public CStdDialog, public IDropTarget
{
		// Runtime Class
		AfxDeclareRuntimeClass();
		
		// Constructors
		CImageSelectDialog(CUIPrimImage *pUI);

		// IUnknown
		HRESULT METHOD QueryInterface(REFIID iid, void **ppObject);
		ULONG   METHOD AddRef(void);
		ULONG   METHOD Release(void);

		// IDropTarget
		HRESULT METHOD DragEnter(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragOver(DWORD dwKeys, POINTL pt, DWORD *pEffect);
		HRESULT METHOD DragLeave(void);
		HRESULT METHOD Drop(IDataObject *pData, DWORD dwKeys, POINTL pt, DWORD *pEffect);

		// Attributes
		CString GetTag(void) const;
		
	protected:
		// Data Members
		CUIPrimImage * m_pUI;
		CDropHelper    m_DropHelp;
		BOOL           m_fDrop;
		
		// Message Map
		AfxDeclareMessageMap();

		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Notification Handlers
		void OnDblClk(UINT uID, CWnd &Wnd);
		
		// Command Handlers
		BOOL OnCommandOK(UINT uID);
		BOOL OnCommandCancel(UINT uID);
		BOOL OnPasteControl(UINT uID, CCmdSource &Src);
		BOOL OnPasteCommand(UINT uID);
	};

// End of File

#endif
