
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Simple Tag Item
//

// Constructor

CTagSimple::CTagSimple(void)
{
	}

// Destructor

CTagSimple::~CTagSimple(void)
{
	}

// Initialization

void CTagSimple::Load(PCBYTE &pData)
{
	ValidateLoad("CTagSimple", pData);

	CDataTag::Load(pData);
	}

// Attributes

UINT CTagSimple::GetDataType(void) const
{
	if( m_pValue ) {

		return m_pValue->GetType();
		}

	return typeInteger;
	}

BOOL CTagSimple::CanWrite(void) const
{
	return FALSE;
	}

// Evaluation

DWORD CTagSimple::GetProp(CDataRef const &Ref, WORD ID, UINT Type)
{
	switch( ID ) {

		case tpAsText:
			return FindAsText(Ref);

		case tpLabel:
			return FindLabel(Ref);
		}

	return CDataTag::GetProp(Ref, ID, Type);
	}

// End of File
