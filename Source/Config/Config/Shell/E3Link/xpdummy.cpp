
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Et3 Programming Link Support
//
// Copyright (c) 1993-2015 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Eth3 Link Dummy Transport
//

class CDummyTransport : public ILinkTransport
{
	public:
		// Constructor
		CDummyTransport(void);

		// Deletion
		void Release(void);

		// Management
		BOOL Open(void);
		void Close(void);
		void Terminate(void);

		// Attributes
		CString GetErrorText(void) const;

		// Transport
		BOOL Transact(IFileData *pData, BOOL fRead);
		BOOL ResetStation(void);

		// Notifications
		void OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam);
		void OnBind(HWND hWnd);

	protected:
	};

//////////////////////////////////////////////////////////////////////////
//
// Eth3 Link Dummy Transport
//

// Instantiator

ILinkTransport * Create_DummyTransport(void)
{
	return New CDummyTransport;
	}

// Constructor

CDummyTransport::CDummyTransport(void)
{
	}

// Deletion

void CDummyTransport::Release(void)
{
	delete this;
	}

// Management

BOOL CDummyTransport::Open(void)
{
	// TODO -- implement

	return TRUE;
	}

void CDummyTransport::Close(void)
{
	// TODO -- implement
	}

void CDummyTransport::Terminate(void)
{
	// TODO -- implement
	}

// Attributes

CString CDummyTransport::GetErrorText(void) const
{
	return CString();
	}

// Transport

BOOL CDummyTransport::Transact(IFileData *pData, BOOL fRead)
{
	// TODO -- implement

	Sleep(50);

	return TRUE;
	}

BOOL CDummyTransport::ResetStation(void)
{
	// TODO -- implement

	Sleep(50);

	return TRUE;
	}


// Notifications

void CDummyTransport::OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	}

void CDummyTransport::OnBind(HWND hWnd)
{
	}

// End of File
