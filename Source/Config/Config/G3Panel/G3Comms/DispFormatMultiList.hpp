
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_DispFormatMultiList_HPP

#define INCLUDE_DispFormatMultiList_HPP

////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDispFormatMultiEntry;

//////////////////////////////////////////////////////////////////////////
//
// Multi-State List
//

class DLLAPI CDispFormatMultiList : public CItemList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CDispFormatMultiList(void);

		// Item Access
		CDispFormatMultiEntry * GetItem(INDEX Index) const;
		CDispFormatMultiEntry * GetItem(UINT  uPos ) const;
	};

// End of File

#endif
