
//////////////////////////////////////////////////////////////////////////
//
// PCLib32 Framework Core
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PCCORE_HXX
	
#define	INCLUDE_PCCORE_HXX

//////////////////////////////////////////////////////////////////////////
//								
// Other Headers
//

#include <windows.h>

#include <winver.h>

//////////////////////////////////////////////////////////////////////////
//
// Version Macros
//

#define	VerValue(n, v)		VALUE n, v, "\0"

//////////////////////////////////////////////////////////////////////////
//
// Version Status
//

#if	C3_BETA

#ifdef	_DEBUG

#define	VER_FLAGS		VS_FF_PRERELEASE | VS_FF_DEBUG

#else

#define	VER_FLAGS		VS_FF_PRERELEASE

#endif

#else

#ifdef	_DEBUG

#define	VER_FLAGS		0 | VS_FF_DEBUG

#else

#define	VER_FLAGS		0

#endif

#endif

//////////////////////////////////////////////////////////////////////////
//
// String Identifiers
//

#define	IDS_APP_CAPTION		0x1000

// End of File

#endif
