
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 Base Master Driver
//
// Copyright (c) 1993-2017 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Constants
//

#define CRC_POLY	0x1021

#define MAX_WORD	1000

#define MAX_BUFF	4096

//////////////////////////////////////////////////////////////////////////
//
// Enumerations
//

enum Space {

	spaceDT  =  1,
	spaceLD  =  3,
	spaceC   =  9,
	spaceT   = 10,
	spaceR   = 11,
	spaceL   = 13,
	spaceX   = 14,
	spaceY   = 15,
	spaceWR  = 16,
	spaceWL  = 18,
	spaceWX	 = 19,
	spaceWY  = 20,
	spaceUM  = 41,
	spaceSD  = 42,
	spaceS   = 43,
	spaceP   = 44,
	spaceE   = 45,
	spaceIN  = 46,
	spaceOT  = 47,
	spaceI   = 48,
	spaceTS  = 49,
	spaceTE  = 50,
	spaceCS  = 51,
	spaceCE  = 52,
	spaceWI  = 53,
	spaceWO  = 54,
	spaceWS  = 55,
	spaceLER = 239,
	spaceLEC = 240,
	};


//////////////////////////////////////////////////////////////////////////
//
// FP Map Structure
//

struct CFpMap {

	CAddress m_Addr;
	DWORD    m_Map;
	WORD     m_Extent;
	};

//////////////////////////////////////////////////////////////////////////
//
// Panasonic FP7 PLC MEWTOCOL7 Base Master Driver
//

class CPanFpMewtocol7BaseMasterDriver : public CMasterDriver
{
	public:
		// Constructor
		CPanFpMewtocol7BaseMasterDriver(void);

		// Entry Points
		DEFMETH(CCODE) Ping (void);
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Device Data

		struct CBaseCtx
		{
			CFpMap * m_pMap;
			UINT     m_uMap;
			DWORD    m_dwStation;
			char     m_LatestErrReq[60];
			DWORD    m_dwLatestErrCode;
			};

		// Data Members

		CBaseCtx * m_pBase;
		UINT	   m_uPtr;
		LPCTXT 	   m_pHex;
		WORD	   m_CRC;
		BYTE	   m_pTx[MAX_BUFF];
		BYTE       m_pRx[MAX_BUFF];
		UINT	   m_uTxSize;
		UINT	   m_uRxSize;
		BYTE	   m_bFrame;
		UINT	   m_uOffset;

		// Read Handlers
		CCODE DoMappedRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStandardRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitRead(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoMappedBitRead(AREF Addr, PDWORD pData, UINT uCount);

		// Write Handlers
		CCODE DoMappedWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoStandardWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoBitWrite(AREF Addr, PDWORD pData, UINT uCount);
		CCODE DoMappedBitWrite(AREF Addr, PDWORD pData, UINT uCount);

		// Map Support
		void  MakeMap(PCBYTE &pData);
		void  DeleteMap(void);
		DWORD FindMap(AREF Addr, UINT &uCount);
		DWORD FindSlot(DWORD dwMap);
		DWORD FindOffset(DWORD dwMap);

		// Frame Building
		void Start(void);
		void AddHeader(void);
		void AddReceiver(void);
		void AddFrameNumber(void);
		void AddCommand(BOOL fWrite);
		void AddCommandCode(BOOL fWrite);
		void AddCommandName(BOOL fWrite);
		void AddRequest(AREF Addr, UINT uCount, BOOL fWrite);
		void AddRequest(AREF Addr, DWORD dwMap, UINT uCount);
		void AddBitRequest(AREF Addr, UINT uCount);
		void AddBitRequest(AREF Addr, DWORD dwMap, UINT uCount);
		void AddPrefix(BYTE bTable);
		void AddRange(AREF Addr, UINT uCount);
		void AddRange(BYTE bTable, DWORD dwOffset, UINT uCount);
		void AddCRC(void);
		void End(void);
		void AddByte(BYTE bData);
		void Add(DWORD dwData, UINT uRadix, UINT uFactor);
		void AddMixed(DWORD dwData, UINT uFactor);
		void AddOffset(DWORD dwData, UINT uRadix, UINT uFactor);
		void AddBitData(AREF Addr, PDWORD pData, UINT uCount);
		void AddWordData(PDWORD pData, UINT uCount);
		void AddLongData(UINT uType, PDWORD pData, UINT uCount);
		void AddData(AREF Addr, PDWORD pData, UINT uCount);

		// Implementation
		PCTXT GetPrefix(BYTE bTable);
		UINT  GetRadix(BYTE bTable);
		UINT  GetOffset(AREF Addr);
		BOOL  IsMixed(BYTE bTable);
		void  GetData(PDWORD pData, AREF Addr, UINT uCount);
		void  GetBitData(PDWORD pData, UINT uCount);
		UINT  GetByteCount(UINT uType);
		void  MakeMaxCount(UINT uType, UINT &uCount);
		BOOL  IsWordLong(UINT uType);
		BOOL  IsLong(UINT uType);
		BOOL  IsBit(UINT uType);
		BOOL  HasSlot(BYTE bTable);
		BOOL  IsReadOnly(BYTE bTable);
		BOOL  SwapLong(AREF Addr);

		// Error Code Support
		BOOL  IsReportErrorCode(BYTE bTable);
		BOOL  IsReportErrorRequest(BYTE bTable);
		void  InitErrorCode(void);
		void  InitErrorRequest(void);
		void  SetReportErrors(void);
		
		// Transport
		BOOL  CheckFrame(UINT uBytes);

		// Overridables
		virtual BOOL Transact(void);

		// CRC-16-CCITT
		void CRCInit(void);
		void CRCCalc(PBYTE pBytes, UINT uCount);
		void CRCReverse(void);
		void Reverse(BYTE &bByte);
	
		// Helpers
		DWORD Convert(PCTXT pText, UINT uCount);
		void  PrintMap(void);
		};
		

// End of File



