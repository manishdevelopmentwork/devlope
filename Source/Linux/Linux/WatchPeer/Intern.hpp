
#ifndef INCLUDE_INTERN_HPP

#define INCLUDE_INTERN_HPP

#include "../Watcher/Watcher.hpp"

#include <arpa/inet.h>
#include <net/if.h>
#include <netdb.h>
#include <linux/rtnetlink.h>
#include <sys/socket.h>

#endif
