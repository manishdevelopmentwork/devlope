
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_SELECT_HPP
	
#define	INCLUDE_SELECT_HPP

//////////////////////////////////////////////////////////////////////////
//
// Other Headers
//

#include "rich.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CPrimLegacySelector;
class CPrimLegacySelectorTwo;
class CPrimLegacySelectorMulti;

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Selector Switch
//

class CPrimLegacySelector : public CPrimRich
{
	public:
		// Constructor
		CPrimLegacySelector(void);

		// Destructor
		~CPrimLegacySelector(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void SetScan(UINT Code);
		void DrawPrep(IGDI *pGDI, CR2Array &Erase, CR2Array &Trans);
		void LoadTouchMap(ITouchMap *pTouch);
		UINT OnMessage(UINT uMsg, UINT uParam);

		// Data Members
		CPrimPen   * m_pEdge;
		CPrimBrush * m_pPanel;
		CPrimBrush * m_pKnob;
		CPrimColor * m_pBack;
		UINT         m_ShowStates;

	protected:
		// Context Record
		struct CCtx
		{
			// Data Members
			BOOL   m_fAvail;
			DWORD  m_Data;
			BOOL   m_fEnable;
			BOOL   m_fPressL;
			BOOL   m_fPressR;

			// Context Check
			BOOL operator == (CCtx const &That) const;
			};

		// Draw Context
		CCtx m_Ctx;

		// Data Memmbers
		BOOL   m_fPressL;
		BOOL   m_fPressR;
		R2     m_Left;
		R2     m_Right;
		
		// Message Handlers
		UINT OnTakeFocus(UINT uMsg, UINT uParam);
		UINT OnTouchRepeat(void);
		UINT OnTouchUp(void);

		// Implementation
		void DrawKnob(IGDI *pGDI, R2 Rect, INT nPos);

		// Context Creation
		void FindCtx(CCtx &Ctx);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Two-State Selector Switch
//

class CPrimLegacySelectorTwo : public CPrimLegacySelector
{
	public:
		// Constructor
		CPrimLegacySelectorTwo(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		UINT OnMessage(UINT uMsg, UINT uParam);

	protected:
		// Message Handlers
		UINT OnTouchDown(UINT uParam);
	};

//////////////////////////////////////////////////////////////////////////
//
// Primitive -- Legacy Multi-State Selector Switch
//

class CPrimLegacySelectorMulti : public CPrimLegacySelector
{
	public:
		// Constructor
		CPrimLegacySelectorMulti(void);

		// Initialization
		void Load(PCBYTE &pData);

		// Overridables
		void DrawPrim(IGDI *pGDI);
		UINT OnMessage(UINT uMsg, UINT uParam);

	protected:
		// Message Handlers
		UINT OnTouchDown(UINT uParam);

		// Implementation
		void GoLeft(void);
		void GoRight(void);
		UINT GetCount(void);
	};

// End of File

#endif
