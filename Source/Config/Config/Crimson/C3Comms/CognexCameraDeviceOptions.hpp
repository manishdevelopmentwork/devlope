
//////////////////////////////////////////////////////////////////////////
//
// Crimson III Comms Architecture
//
// Copyright (c) 1993-2014 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_COGNEXCAMERADEVICEOPTIONS_HPP
	
#define	INCLUDE_COGNEXCAMERADEVICEOPTIONS_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

//////////////////////////////////////////////////////////////////////////
//
// Cognex Camera Device Options
//

class CCognexCameraDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CCognexCameraDeviceOptions(void);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		void SetInitValues(void);
		BOOL GetTypeData(CString Tag, CTypeDef &Type);

		// Config Data
		UINT         m_Camera;
		UINT         m_IP;
		UINT         m_Port;
		UINT	     m_Time4;
		CString      m_User;
		CString      m_Pass;

		// Persistence
		void Load(CTreeFile &File);

	protected:
		// Meta Data Creation
		void AddMetaData(void);
	};

// End of File

#endif
