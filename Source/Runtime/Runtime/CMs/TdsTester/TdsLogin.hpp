
//////////////////////////////////////////////////////////////////////////
//
// TDS Protocol Support
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_TDS_TdsLogin_HPP

#define INCLUDE_TDS_TdsLogin_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "TdsPacket.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TDS Login Packet
//


class CTdsLogin : public CTdsPacket
{
	public:
		// Constructors
		CTdsLogin(void);
		~CTdsLogin(void);
		
		// Operations
		void Create(PCSTR pHostName, PCSTR pUserName, PCSTR pPassword, PCSTR pClientAppName, PCSTR pDatabaseName, ULONG uPacketSize = 4096);
		void AddLength(ULONG uLength);
		void AddVersion(BYTE bVersion, BYTE bBuild, BYTE bSubBuild, BYTE bLastByte);
		void AddPacketSize(ULONG uPacketSize);
		void AddClientProgVer(BYTE bClientProgVer);
		void AddClientPID(ULONG uClientPID);
		void AddConnectionID(ULONG uConnectionID);
		void AddOptionFlags1(BYTE bOptionFlags1);
		void AddOptionFlags2(BYTE bOptionFlags2);
		void AddTypeFlags(BYTE bTypeFlags);
		void AddOptionFlags3(BYTE bOptionFlags3);
		void AddClientTimeZone(ULONG uClientTimeZone);
		void AddClientLCID(DWORD ClientLCID);
		void AddIbHostName(USHORT uIbHostName);
		void AddCchHostName(USHORT uCchHostName);
		void AddIbUserName(USHORT uIbUserName);
		void AddCchUserName(USHORT uCchUserName);
		void AddIbPassword(USHORT uIbPassword);
		void AddCchPassword(USHORT uCchPassword);
		void AddIbAppName(USHORT uIbAppName);
		void AddCchAppName(USHORT uCchAppName);
		void AddIbServerName(USHORT uIbServerName);
		void AddCchServerName(USHORT uCchServerName);
		void AddIbUnused(USHORT uIbUnused);
		void AddCchUnused(USHORT uCchUnused);
		void AddIbCltIntName(USHORT uIbCltIntName);
		void AddCchCltInitName(USHORT uCchCltIntName);
		void AddIbLanguage(USHORT uIbLanguage);
		void AddCchLanguage(USHORT uCchLanguage);
		void AddIbDatabase(USHORT uIbDatabase);
		void AddCchDatabase(USHORT uCchDatabase);
		void AddClientID(CTdsBytes* ClientID);
		void AddIbSSPI(USHORT uIbSSPI);
		void AddCchSSPI(USHORT uCchSSPI);
		void AddIbAttachDBFile(USHORT uIbAttachDBFile);
		void AddCchAttachDBFile(USHORT uCchAttachDBFile);
		void AddIbChangePassword(USHORT uIbChangePassword);
		void AddCchChangePassword(USHORT uCchChangePassword);
		void AddCbSSPILong(LONG CbSSPILong);
		void AddString(PCSTR pString);
		void AddPasswordString(PCSTR pString);
		void EndPacket(PCSTR pHostName, PCSTR pUserName, PCSTR pPassword, PCSTR pClientAppName, PCSTR pDatabaseName);


	};


// End of File

#endif
