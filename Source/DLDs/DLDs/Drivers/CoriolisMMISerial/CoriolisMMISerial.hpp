
#include "intern.hpp"

#include "ModbusSerial.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Coriolis Modbus Serial Driver
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

enum Channels {
	
	holdingRegisters = 1,
	inputRegisters	 = 2,
	outputCoils	 = 3,
	inputCoils	 = 4,
	Coils		 = 5,
	Registers	 = 6,

	MAX_CHANNELS,
	};

class CCoriolisSerialDriver : public CModbusDriver
{
	public:
		// Constructor
		CCoriolisSerialDriver(void);

		// Destructor
		~CCoriolisSerialDriver(void);

		// Device
		DEFMETH(CCODE) DeviceOpen(IDevice *pDevice);
		DEFMETH(CCODE) DeviceClose(BOOL fPersist);
		
		// Entry Points
		DEFMETH(CCODE) Read (AREF Addr, PDWORD pData, UINT uCount);
		DEFMETH(CCODE) Write(AREF Addr, PDWORD pData, UINT uCount);

	protected:

		// Context
		struct CSerialContext : public CContext {

			UINT	m_uNumHoldingRegs;
			UINT	m_uNumCoilRegs;
			
			CAddress	*m_pCoilRegisters;
			CAddress	*m_pHoldingRegisters;
			};

		// Data Members
		CSerialContext *m_pSerialCtx;

		// Implemetation
		BOOL	   GetMaxCount(AREF Address, UINT &uCount); 
		UINT	   GetSize(AREF Addr);
		UINT	   GetRegTotal(BOOL fCoils);
		CAddress * GetRegArray(BOOL fCoils);

		// Helpers
		BOOL IsCoils(UINT uTable);

	};

// End of File
