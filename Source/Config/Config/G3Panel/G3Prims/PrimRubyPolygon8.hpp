
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Primitives Library
//
// Copyright (c) 1993-2017 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_PrimRubyPolygon8_HPP
	
#define	INCLUDE_PrimRubyPolygon8_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PrimRubyPolygon.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Ruby 8-Sided Polygon Primitive
//

class CPrimRubyPolygon8 : public CPrimRubyPolygon
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CPrimRubyPolygon8(void);

	protected:
		// Meta Data
		void AddMetaData(void);
	};

// End of File

#endif
