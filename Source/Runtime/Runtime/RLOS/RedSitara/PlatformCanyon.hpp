
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_PlatformCanyon_HPP

#define INCLUDE_PlatformCanyon_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "PlatformSitara.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Referenced Classes
//

class CDss437;

//////////////////////////////////////////////////////////////////////////
//
// Platform Object for Canyon HMI
//

class CPlatformCanyon : public CPlatformSitara, 
			public IPortSwitch, 
			public ILeds, 
			public IInputSwitch,
			public IUsbSystem,
			public IUsbSystemPortMapper
{
	public:
		// Constructor
		CPlatformCanyon(UINT uModel);

		// Destructor
		~CPlatformCanyon(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// IDevice
		BOOL METHOD Open(void);

		// IPlatform
		PCTXT METHOD GetModel(void);
		PCTXT METHOD GetVariant(void);
		UINT  METHOD GetFeatures(void);

		// ILeds
		void METHOD SetLed(UINT uLed, UINT uState);
		void METHOD SetLedLevel(UINT uPercent, bool fPersist);
		UINT METHOD GetLedLevel(void);

		// IInputSwitch
		UINT METHOD GetSwitches(void);
		UINT METHOD GetSwitch(UINT uSwitch);

	protected:
		// Data Members
		CDss437 * m_pDss;

		// IPortSwitch
		UINT METHOD GetCount(UINT uUnit);
		UINT METHOD GetMask(UINT uUnit);
		UINT METHOD GetType(UINT uUnit, UINT uLog);
		BOOL METHOD EnablePort(UINT uUnit, BOOL fEnable);
		BOOL METHOD SetPhysical(UINT uUnit, BOOL fRS485);
		BOOL METHOD SetFull(UINT uUnit, BOOL fFull);
		BOOL METHOD SetMode(UINT uUnit, BOOL fAuto);

		// IUsbSystem
		void METHOD OnDeviceArrival(IUsbHostFuncDriver *pDriver);
		void METHOD OnDeviceRemoval(IUsbHostFuncDriver *pDriver);
		
		// IUsbSystemPortMapper
		UINT METHOD GetPortCount(void);
		UINT METHOD GetPortType(UsbTreePath const &Path);
		UINT METHOD GetPortReset(UsbTreePath const &Path);
		
		// Inititialisation
		void InitClocks(void);
		void InitMpu(void);
		void InitMux(void);
		void InitGpio(void);
		void InitMisc(void);
		void InitUarts(void);
		void InitUsb(void);
		void InitRack(void);
	};

// End of File

#endif
