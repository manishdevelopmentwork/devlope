
#include "intern.hpp"

#include "l5keip.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2002 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CABL5kDeviceOptions, CUIItem);

// Constructor

CABL5kDeviceOptions::CABL5kDeviceOptions(void)
{
	m_Device  = 0;
	
	m_Address = DWORD(MAKELONG(MAKEWORD( 52, 200), MAKEWORD(  9, 192)));

	m_Slot	  = 0;

	m_Time	  = 1000;

	m_pTypes  = New CABL5kTypeList;

	m_pNames  = New CABL5kTagList;
	}

// Address Management

void CABL5kDeviceOptions::CheckAlignment(CAddress &Addr)
{
	if( Addr.a.m_Type == addrBitAsLong ) {

		Addr.a.m_Offset = (Addr.a.m_Offset / 32) * 32;
		}
	}

BOOL CABL5kDeviceOptions::DoParseAddress(CError &Error, CAddress &Addr, CString Text)
{
	CABL5kItem *pItem;

	if( (pItem = m_pNames->FindItem(Text)) ) {

		CABL5kAtomicTag *pTag = (CABL5kAtomicTag *) pItem;

		MapAtomicTag(pTag);

		CString Name = pTag->GetFullName();

		CString Type = pTag->GetType();
			
		if( pItem->IsArray() ) {

			UINT uIndex = m_TableList.Find(Name);

			UINT uEntry = m_TableDict.Find(uIndex) + 1;

			if( uEntry < addrNamed ) {

				CArray <UINT> Dims, List;
				
				m_pNames->DimParse(List, Text);

				m_pNames->DimParse(Dims, Type);

				//
				Addr.a.m_Extra  = 0;
				
				Addr.a.m_Type   = m_pTypes->GetAtomicType(Type);

				Addr.a.m_Table  = uEntry;

				Addr.a.m_Offset = m_pNames->DimGetIndex(Dims, List);
				
				CheckAlignment(Addr);
				
				return TRUE;
				}

			Error.Set(CPrintf("Failed to allocate table entry (%d).", uEntry), 0);
			}
		else {
			UINT  uIndex = m_NamedList.Find(Name);

			UINT uOffset = m_NamedDict.Find(uIndex) + 1;

			//
			Addr.a.m_Extra  = 0;
			
			Addr.a.m_Type   = m_pTypes->GetAtomicType(Type);

			Addr.a.m_Table  = addrNamed;

			Addr.a.m_Offset = uOffset;
			}
			
		return TRUE;
		}

	Error.Set(CPrintf("<%s> not found.", Text), 0);

	return FALSE;
	}

BOOL CABL5kDeviceOptions::DoExpandAddress(CString &Text, CAddress const &Addr)
{
	if( Addr.m_Ref ) {

		AfxAssert(Addr.a.m_Extra == 0);
	
		if( IsNamed(Addr) ) {

			UINT uEntry = Addr.a.m_Offset - 1;
			
			if( uEntry < m_NamedDict.GetCount() ) {
				
				UINT uIndex = m_NamedDict[uEntry];

				Text = m_NamedList[uIndex];

				return TRUE;
				}

			return FALSE;
			}
		
		if( IsTable(Addr) ) {

			UINT uEntry = Addr.a.m_Table - 1;

			if( uEntry < m_TableDict.GetCount() ) {

				UINT uIndex = m_TableDict[uEntry];

				Text = m_TableList[uIndex];

				CABL5kItem *pItem;

				if( (pItem = m_pNames->FindItem(Text)) ) {

					CString Type = pItem->GetType();

					CArray <UINT> Dims;
					
					m_pNames->DimParse(Dims, Type);

					m_pNames->DimExpand(Dims, Addr.a.m_Offset, Text);
					
					return TRUE;
					}
				}
			}
		}
	
	return FALSE;
	}

BOOL CABL5kDeviceOptions::DoSelectAddress(HWND hWnd, CAddress &Addr, BOOL fPart, BOOL fSelect)
{
	if( Addr.a.m_Extra ) {

		UINT uIndex = GetIndex(Addr) - 1;

		CString Text = IsNamed(Addr) ? m_NamedList[uIndex] : m_TableList[uIndex];
		
		return DoParseAddress(CError(FALSE), Addr, Text);
		}
	else {
		CABL5kDriver Driver;	

		CABL5kDialog Dialog(Driver, Addr, this, fPart, fSelect);
		
		return Dialog.Execute(CWnd::FromHandle(hWnd));
		}
	}

BOOL CABL5kDeviceOptions::DoListAddress(CAddrData *pRoot, UINT uItem, CAddrData &Data)
{
	if( !pRoot ) {

		if( uItem == 0 ) {
			
			m_uTable = 0;

			m_uNamed = 0;
			}

		if( m_uTable < m_TableList.GetCount() ) {

			CString Name = m_TableList[m_uTable];

			CABL5kItem *pItem;

			if( (pItem = m_pNames->FindItem(Name)) ) {

				if( m_uTable < addrNamed - 1 ) {

					CString Type = pItem->GetType();

					UINT uType = m_pTypes->GetAtomicType(Type);

					//
					Data.m_Name	= Name;

					Data.m_Type	= Type;

					Data.m_Addr.a.m_Type	= uType;
					
					Data.m_Addr.a.m_Extra	= 1;	// not mapped

					Data.m_Addr.a.m_Table   = m_uTable + 1;

					Data.m_Addr.a.m_Offset   = 0;
					
					Data.m_fPart	= TRUE;

					//
					m_uTable ++;

					return TRUE;
					}
				}
			}

		if( m_uNamed < m_NamedList.GetCount() ) {

			CString Name = m_NamedList[m_uNamed];

			CABL5kItem *pItem;

			if( (pItem = m_pNames->FindItem(Name)) ) {

				CString Type = pItem->GetType();

				UINT uType = m_pTypes->GetAtomicType(Type);

				//
				Data.m_Name	= Name;

				Data.m_Type	= Type;

				Data.m_Addr.a.m_Type	= uType;
				
				Data.m_Addr.a.m_Extra	= 1;	// not mapped

				Data.m_Addr.a.m_Table   = addrNamed;

				Data.m_Addr.a.m_Offset  = m_uNamed + 1;
				
				Data.m_fPart	= TRUE;

				//
				m_uNamed ++;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

// Dialog Support

CString CABL5kDeviceOptions::GetDeviceName(void)
{
	return ((CNamedItem *) GetParent())->m_Name;
	}

// Mapped Address Support

void CABL5kDeviceOptions::MapAtomicTag(CABL5kAtomicTag *pTag)
{
	CString Name = pTag->GetFullName();

	if( pTag->IsArray() ) {

		UINT  uIndex = m_TableList.Find(Name);

		AfxAssert(uIndex < NOTHING);

		if( m_TableDict.Find(uIndex) == NOTHING ) {

			m_TableDict.Append(uIndex);			
			}
		}
	else {
		UINT  uIndex = m_NamedList.Find(Name);

		AfxAssert(uIndex < NOTHING);

		if( m_NamedDict.Find(uIndex) == NOTHING ) {

			m_NamedDict.Append(uIndex);			
			}
		}
	}

void CABL5kDeviceOptions::AddAtomicTag(CABL5kAtomicTag *pTag)
{
	if( pTag->IsArray() ) {

		m_TableList.Append(pTag->GetFullName());
		}
	else {
		m_NamedList.Append(pTag->GetFullName());
		}
	}

// L5k Support

BOOL CABL5kDeviceOptions::LoadFromFile(CError &Error, CFilename const &Name)
{
	CTextStreamDisk Stream;

	if( Stream.OpenLoad(Name) ) {
		
		CABL5kFile File;

		if( File.OpenLoad(Stream) ) {

			CStringArray Tags;

			SaveTags(Tags, m_NamedList, m_NamedDict);
			
			SaveTags(Tags, m_TableList, m_TableDict);

			m_Controller = File.m_Controller.m_Name;

			m_NamedList.Empty();

			m_NamedDict.Empty();
			
			m_TableList.Empty();

			m_TableDict.Empty();
	
			m_pTypes->LoadConfig(Error, File.m_Controller);

			m_pNames->LoadConfig(Error, File.m_Controller);

			for( UINT n = 0; n < Tags.GetCount(); n++ ) {

				CAddress Addr;

				DoParseAddress(CError(FALSE), Addr, Tags[n]);
				}
			
			return TRUE;
			}
		
		Error.Set("Failed to open l5k file\n", 0);

		ThrowError();
		}

	return FALSE;
	}

// UI Management

void CABL5kDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	if( pItem == this ) {

		if( Tag == "Push" ) {

			DWORD dwAddr = 0;

			DoSelectAddress(pWnd->GetHandle(), (CAddress &) dwAddr, FALSE, FALSE);
			
			return;
			}
		}
	}

// Persistance

void CABL5kDeviceOptions::Save(CTreeFile &Tree)
{
	CUIItem::Save(Tree);

	Tree.PutObject("Named");

	SaveMapping(Tree, m_NamedList, m_NamedDict);

	Tree.EndObject();

	Tree.PutObject("Table");

	SaveMapping(Tree, m_TableList, m_TableDict);

	Tree.EndObject();
	}

void CABL5kDeviceOptions::Load(CTreeFile &Tree)
{
	AddMeta();

	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "Named" ) {

			Tree.GetObject();
			
			LoadMapping(Tree, m_NamedList, m_NamedDict);

			Tree.EndObject();
			
			continue;
			}

		if( Name == "Table" ) {

			Tree.GetObject();
			
			LoadMapping(Tree, m_TableList, m_TableDict);

			Tree.EndObject();

			continue;
			}

		CMetaData const *pMeta = m_pList->FindData(Name);

		if( pMeta ) {

			LoadProp(Tree, pMeta);
			}
		}
	}

void CABL5kDeviceOptions::PostLoad(void)
{
	CUIItem::PostLoad();

	if( m_NamedList.GetCount() == 0 || m_TableList.GetCount() == 0 ) {

		m_NamedList.Empty();

		m_TableList.Empty();

		BuildNameLists(*m_pNames);
		}
	}

void CABL5kDeviceOptions::BuildNameLists(CABL5kList &List)
{
	INDEX Index = List.GetHead();

	for( ; !List.Failed(Index); List.GetNext(Index) ) {

		CABL5kItem *pItem = List.GetItem(Index);

		if( pItem->IsStruct() ) {
			
			BuildNameLists(*pItem->GetElements());

			continue;
			}

		if( pItem->IsAtomic() ) {

			CABL5kAtomicTag *pTag = (CABL5kAtomicTag *) pItem;

			if( pTag->IsArray() ) {
				
				m_TableList.Append(pTag->GetFullName());
				}
			else {
				m_NamedList.Append(pTag->GetFullName());
				}

			continue;
			}
		}
	}

// Download Support

BOOL CABL5kDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Device));
	Init.AddLong(DWORD(m_Address));
	Init.AddWord(WORD(m_Slot));
	Init.AddWord(WORD(m_Time));

	if( TRUE ) {
		
		UINT uCount = m_NamedDict.GetCount();

		Init.AddWord(WORD(uCount));

		for( UINT n = 0; n < uCount; n ++ ) {

			UINT  uIndex = m_NamedDict[n];

			CString Name = m_NamedList[uIndex];

			Init.AddText(Name);

			Init.AddByte(BYTE(0));
			}
		}

	if( TRUE ) {
		
		UINT uCount = m_TableDict.GetCount();

		Init.AddWord(WORD(uCount));

		for( UINT n = 0; n < uCount; n ++ ) {

			UINT  uIndex = m_TableDict[n];

			CString Name = m_TableList[uIndex];

			CString Type = m_pNames->FindItem(Name)->GetType();
			
			CArray <UINT> List;
			
			m_pNames->DimParse(List, Type);

			Init.AddText(Name);

			UINT uSize = List.GetCount();
			
			if( uSize > 0 ) {
				
				Init.AddByte(BYTE(uSize));

				for( UINT m = 0; m < uSize; m ++ ) {
					
					Init.AddWord(WORD(List[m]));
					}
				}
			else
				Init.AddByte(BYTE(0));
			}
		}

	return TRUE;
	}

// Meta Data Creation

void CABL5kDeviceOptions::AddMetaData(void)
{
	Meta_AddInteger(Device);
	Meta_AddInteger(Address);
	Meta_AddInteger(Slot);
	Meta_AddInteger(Time);
	Meta_AddInteger(Push);
	Meta_AddCollect(Types);
	Meta_AddCollect(Names);
	Meta_AddString (Controller);
	}

// Property Save Filter

BOOL CABL5kDeviceOptions::SaveProp(CString Tag) const
{
	if( Tag == "Push" ) {
		
		return FALSE;
		}

	return CMetaItem::SaveProp(Tag);
	}

// Implementation

void CABL5kDeviceOptions::ThrowError(void)
{
	AfxThrowUserException();
	}

void CABL5kDeviceOptions::SaveTags(CStringArray &Tags, CArray <CString> &List, CArray <UINT> &Dict)
{
	for( UINT n = 0; n < Dict.GetCount(); n++ ) {

		Tags.Append(List[Dict[n]]);
		}
	}

void CABL5kDeviceOptions::LoadMapping(CTreeFile &Tree, CArray <CString> &List, CArray <UINT> &Dict)
{
	while( !Tree.IsEndOfData() ) {

		CString Name = Tree.GetName();

		if( Name == "List" ) {

			Tree.GetObject();

			LoadNames(Tree, List);

			Tree.EndObject();
			
			continue;
			}

		if( Name == "Dict" ) {

			Tree.GetObject();

			LoadEntry(Tree, Dict);

			Tree.EndObject();
			
			continue;
			}
		}
	}

void CABL5kDeviceOptions::LoadNames(CTreeFile &Tree, CArray <CString> &List)
{
	while( !Tree.IsEndOfData() ) {
		
		CString	Name = Tree.GetName();

		if( Name.Left(3) == "Tag" ) {

			List.Append(Tree.GetValueAsString());
			}
		}
	}

void CABL5kDeviceOptions::LoadEntry(CTreeFile &Tree, CArray <UINT> &List)
{
	while( !Tree.IsEndOfData() ) {
		
		CString	Name = Tree.GetName();

		if( Name.Left(3) == "Ref" ) {

			List.Append(Tree.GetValueAsInteger());
			}
		}
	}

void CABL5kDeviceOptions::SaveMapping(CTreeFile &Tree, CArray <CString> &List, CArray <UINT> &Dict)
{
	if( List.GetCount() ) {
		
		Tree.PutObject("List");

		SaveNames(Tree, List);

		Tree.EndObject();
		}

	if( Dict.GetCount() ) {
		
		Tree.PutObject("Dict");

		SaveEntry(Tree, Dict);

		Tree.EndObject();
		}
	}

void CABL5kDeviceOptions::SaveNames(CTreeFile &Tree, CArray <CString> &List)
{
	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n ++ ) {

		Tree.PutValue(CPrintf("Tag%d", n), List[n]);
		}
	}

void CABL5kDeviceOptions::SaveEntry(CTreeFile &Tree, CArray <UINT> &List)
{
	UINT uCount = List.GetCount();

	for( UINT n = 0; n < uCount; n ++ ) {

		Tree.PutValue(CPrintf("Ref%d", n), List[n]);
		}
	}

//////////////////////////////////////////////////////////////////////////
//
// Allen Bradley Logix5000 Driver
//

// Instantiator

ICommsDriver *	Create_ABL5kDriver(void)
{
	return New CABL5kDriver;
	}

// Constructor

CABL5kDriver::CABL5kDriver(void)
{
	m_uType		= driverMaster;
	
	m_Manufacturer	= "Allen-Bradley";
	
	m_DriverName	= "Native Tags via L5K file";
	
	m_Version	= "1.03";
	
	m_ShortName	= "AB L5K Tags";

	m_wID		= 0x4042;
	}

// Binding Control

UINT CABL5kDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CABL5kDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CABL5kDriver::GetDriverConfig(void)
{
	return NULL;
	}

// Configuration

CLASS CABL5kDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CABL5kDeviceOptions);
	}

// Address Management

BOOL CABL5kDriver::ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoParseAddress(Error, Addr, Text);
	}

BOOL CABL5kDriver::ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoExpandAddress(Text, Addr);
	}

BOOL CABL5kDriver::SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoSelectAddress(hWnd, Addr, fPart, TRUE);
	}

BOOL CABL5kDriver::ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data)
{
	return ((CABL5kDeviceOptions *) pConfig)->DoListAddress(pRoot, uItem, Data);
	}

// End of File
