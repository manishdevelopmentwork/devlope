
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CommsTester_HPP

#define INCLUDE_CommsTester_HPP

//////////////////////////////////////////////////////////////////////////
//
// Driver Tester Object
//

class CCommsTester : public ICommsTester
{
	public:
		// Constructor
		CCommsTester(void);

		// Destructor
		~CCommsTester(void);

		// IUnknown
		HRM QueryInterface(REFIID riid, void **ppObject);
		ULM AddRef(void);
		ULM Release(void);

		// ICommsTester
		BOOL  METHOD GetSerialConfig(CSerialConfig &Config);
		BOOL  METHOD GetDriverConfig(PBYTE &pData);
		BOOL  METHOD GetDeviceConfig(PBYTE &pData);
		BOOL  METHOD ExecDriverCtrl(ICommsDriver *pDriver);
		BOOL  METHOD ExecDeviceCtrl(ICommsDriver *pDriver);
		BOOL  METHOD ExecTest(CCODE &Code, ICommsMaster *pDriver, UINT uIndex);
		CCODE METHOD SlaveRead (AREF Addr, PDWORD pData, UINT uCount);
		CCODE METHOD SlaveRead (AREF Addr, PDWORD pData, IMetaData **ppm);
		CCODE METHOD SlaveWrite(AREF Addr, PDWORD pData, UINT uCount);

	protected:
		// Data Members
		ULONG m_uRefs;

		// Init Helpers
		void AddByte(PBYTE &pData, BYTE   Data);
		void AddWord(PBYTE &pData, WORD   Data);
		void AddLong(PBYTE &pData, LONG   Data);
		void AddAddr(PBYTE &pData, IPREF  Data);
		void AddText(PBYTE &pData, PCTXT  Data);
		void AddData(PBYTE &pData, PCBYTE Data, UINT uSize);

		// Diagnostics
		void ShowRead (CCODE Code, UINT uIndex, AREF Addr, PDWORD pData, UINT uCount, BOOL fData);
		void ShowWrite(CCODE Code, UINT uIndex, AREF Addr, PDWORD pData, UINT uCount, BOOL fData);
		void ShowData (CCODE Code, AREF Addr, PDWORD pData, UINT uCount);
		void DecodeAddress(CString &Text, AREF Addr);
		void DecodeFailure(CString &Text, CCODE Code);
	};

// End of File

#endif
