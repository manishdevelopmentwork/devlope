
#define	NEED_PASS_FLOAT

#include "intern.hpp"

#include "animatic.hpp"
	
ANIMATICSCmdDef CODE_SEG CAnimaticsDriver::CL[] = {
	{"P",		CMP,		TRW},
	{"V",		CMV,		TRW},
	{"D",		CMD,		TRW},
	{"Bt",		CMBt,		TRO},
	{"Be",		CMBe,		TRO},
	{"Bo",		CMBo,		TRO},
	{"I",		CMI,		TRO},
	{"PE",		CMPE,		TRO},
	{"E",		CME,		TRW},
	{"A",		CMA,		TRW},
	{"UG",		CMUGC,		TWO},
	{"RUN",		CMRUN,		TWO},
	{"Z",		CMZ,		TWO},
	{"W",		CMW,		TRO},
	{"T",		CMT,		TRW},
	{"S",		CMRSB,		TRO},
	{"AMPS",	CMAMPS,		TRW},
	{"CLK",		CMCLK,		TRW},
	{"CTR",		CMCTR,		TRO},
	{"SLEEP",	CMSLEEP,	TWO},
	{"SLEEP1",	CMSLEEP1,	TWO},
	{"WAKE",	CMWAKE,		TWO},
	{"WAKE1",	CMWAKE1,	TWO},
	{"ECHO",	CMECHO,		TWO},
	{"ECHO_OFF",	CMECHO_OFF,	TWO},
	{"SILENT1",	CMSILENT1,	TWO},
	{"TALK1",	CMTALK1,	TWO},
	{"G",		CMG,		TWO},
	{"S",		CMS,		TWO},
	{"X",		CMX,		TWO},
	{"MF0",		CMMF0,		TWO},
	{"MF1",		CMMF1,		TWO},
	{"MF2",		CMMF2,		TWO},
	{"MF4",		CMMF4,		TWO},
	{"MFR",		CMMFR,		TWO},
	{"MS",		CMMS,		TWO},
	{"MSR",		CMMSR,		TWO},
	{"MS0",		CMMS0,		TWO},
	{"MP",		CMMP,		TWO},
	{"MV",		CMMV,		TWO},
	{"MT",		CMMT,		TWO},
	{"UAI",		CMUAI,		TWO},
	{"UBI",		CMUBI,		TWO},
	{"UCI",		CMUCI,		TWO},
	{"UDI",		CMUDI,		TWO},
	{"UEI",		CMUEI,		TWO},
	{"UFI",		CMUFI,		TWO},
	{"UGI",		CMUGI,		TWO},
	{"UAO",		CMUAO,		TWO},
	{"UBO",		CMUBO,		TWO},
	{"UCO",		CMUCO,		TWO},
	{"UDO",		CMUDO,		TWO},
	{"UEO",		CMUEO,		TWO},
	{"UFO",		CMUFO,		TWO},
	{"UGO",		CMUGO,		TWO},
	{"UCP",		CMUCP,		TWO},
	{"UDM",		CMUDM,		TWO},
	{"UA",		CMUA,		TVS},
	{"UB",		CMUB,		TVS},
	{"UC",		CMUC,		TVS},
	{"UD",		CMUD,		TVS},
	{"UE",		CMUE,		TVS},
	{"UF",		CMUF,		TVS},
	{"UG",		CMUG,		TVS},
	{"MFMUL",	CMMFMUL,	TRW},
	{"MFDIV",	CMMFDIV,	TRW},
	{"KA",		CMKA,		TRW},
	{"KD",		CMKD,		TRW},
	{"KG",		CMKG,		TRW},
	{"KI",		CMKI,		TRW},
	{"KL",		CMKL,		TRW},
	{"KP",		CMKP,		TRW},
	{"KS",		CMKS,		TRW},
	{"KV",		CMKV,		TRW},
	{"F",		CMF,		TWO},
	{"OFF",		CMOFF,		TWO},
	{"BRKENG",	CMBRKENG,	TWO},
	{"BRKRLS",	CMBRKRLS,	TWO},
	{"BRKSRV",	CMBRKSRV,	TWO},
	{"Bw",		CMBw,		TRO},
	{"Za",		CMZa,		TWO},
	{"Zb",		CMZb,		TWO},
	{"Zc",		CMZc,		TWO},
	{"Zd",		CMZd,		TWO},
	{"Zf",		CMZf,		TWO},
	{"Zl",		CMZl,		TWO},
	{"Zr",		CMZr,		TWO},
	{"Zs",		CMZs,		TWO},
	{"Zu",		CMZu,		TWO},
	{"Zw",		CMZw,		TWO},
	{"ZS",		CMZS,		TWO},
	{"STACK",	CMSTACK,	TWO},
	{"END",		CMEND,		TWO},
	{"a",		CMa,		TRW},
	{"b",		CMb,		TRW},
	{"c",		CMc,		TRW},
	{"d",		CMd,		TRW},
	{"e",		CMe,		TRW},
	{"f",		CMf,		TRW},
	{"g",		CMg,		TRW},
	{"h",		CMh,		TRW},
	{"i",		CMi,		TRW},
	{"j",		CMj,		TRW},
	{"k",		CMk,		TRW},
	{"l",		CMl,		TRW},
	{"m",		CMm,		TRW},
	{"n",		CMn,		TRW},
	{"o",		CMo,		TRW},
	{"p",		CMp,		TRW},
	{"q",		CMq,		TRW},
	{"r",		CMr,		TRW},
	{"s",		CMs,		TRW},
	{"t",		CMt,		TRW},
	{"u",		CMu,		TRW},
	{"v",		CMv,		TRW},
	{"w",		CMw,		TRW},
	{"x",		CMx,		TRW},
	{"y",		CMy,		TRW},
	{"z",		CMz,		TRW},
	{"aa",		CMaa,		TRW},
	{"bb",		CMbb,		TRW},
	{"cc",		CMcc,		TRW},
	{"dd",		CMdd,		TRW},
	{"ee",		CMee,		TRW},
	{"ff",		CMff,		TRW},
	{"gg",		CMgg,		TRW},
	{"hh",		CMhh,		TRW},
	{"ii",		CMii,		TRW},
	{"jj",		CMjj,		TRW},
	{"kk",		CMkk,		TRW},
	{"ll",		CMll,		TRW},
	{"mm",		CMmm,		TRW},
	{"nn",		CMnn,		TRW},
	{"oo",		CMoo,		TRW},
	{"pp",		CMpp,		TRW},
	{"qq",		CMqq,		TRW},
	{"rr",		CMrr,		TRW},
	{"ss",		CMss,		TRW},
	{"tt",		CMtt,		TRW},
	{"uu",		CMuu,		TRW},
	{"vv",		CMvv,		TRW},
	{"ww",		CMww,		TRW},
	{"xx",		CMxx,		TRW},
	{"yy",		CMyy,		TRW},
	{"zz",		CMzz,		TRW},
	{"aaa",		CMaaa,		TRW},
	{"bbb",		CMbbb,		TRW},
	{"ccc",		CMccc,		TRW},
	{"ddd",		CMddd,		TRW},
	{"eee",		CMeee,		TRW},
	{"fff",		CMfff,		TRW},
	{"ggg",		CMggg,		TRW},
	{"hhh",		CMhhh,		TRW},
	{"iii",		CMiii,		TRW},
	{"jjj",		CMjjj,		TRW},
	{"kkk",		CMkkk,		TRW},
	{"lll",		CMlll,		TRW},
	{"mmm",		CMmmm,		TRW},
	{"nnn",		CMnnn,		TRW},
	{"ooo",		CMooo,		TRW},
	{"ppp",		CMppp,		TRW},
	{"qqq",		CMqqq,		TRW},
	{"rrr",		CMrrr,		TRW},
	{"sss",		CMsss,		TRW},
	{"ttt",		CMttt,		TRW},
	{"uuu",		CMuuu,		TRW},
	{"vvv",		CMvvv,		TRW},
	{"www",		CMwww,		TRW},
	{"xxx",		CMxxx,		TRW},
	{"yyy",		CMyyy,		TRW},
	{"zzz",		CMzzz,		TRW},
	{"O",		CMO,		TWX},
	{"GOSUB",	CMGOSUB,	TGN},
	{"GOSUB",	CMSUBRS,	TGS},
	{"GOSUB",	CMSUBRR,	TGR},
	{"GOSUB",	CMSUBR1,	TGC},
	{"GOSUB",	CMSUBR2,	TGC},
	{"GOSUB",	CMSUBR3,	TGC},
	{"GOSUB",	CMSUBR4,	TGC},
	{"GOSUB",	CMSUBR5,	TGC},
	{"GOSUB",	CMSUBR6,	TGC},
	{"GOSUB",	CMSUBR7,	TGC},
	{"GOSUB",	CMSUBR8,	TGC},
	{"GOSUB",	CMSUBR9,	TGC},
	{"GOSUB",	CMSUBR10,	TGC},
	{"GOSUB",	CMSUBR11,	TGC},
	{"GOSUB",	CMSUBR12,	TGC},
	{"GOSUB",	CMSUBR13,	TGC},
	{"GOSUB",	CMSUBR14,	TGC},
	{"GOSUB",	CMSUBR15,	TGC},
	{"GOSUB",	CMSUBR16,	TGC},
	{"GOSUB",	CM5GSTR,	TGC},
	{"SADDR",	CMSADDR,	TAA},
// Added Dec 2010
//	{"AA",		CM5AA,		TRO}, // obsoleted by Animatics
	{"AT",		CM5AT,		TRW},
	{"DEA",		CM5DEA,		TRO},
	{"DEL",		CM5DEL,		TRW},
	{"DT",		CM5DT,		TRW},
	{"EILN",	CM5EILN,	TDC},
	{"EILP",	CM5EILP,	TDC},
	{"EISM(6)",	CM5EISM,	TDC},
	{"MFA(",	CM5MFA,		TPD},
	{"MFD(",	CM5MFD,		TPD},
	{"MFSLEW(",	CM5MFS,		TPD},
	{"MFX",		CM5MFX,		TDC},
	{"MINV(0)",	CM5MINV0,	TDC},
	{"MINV(1)",	CM5MINV1,	TDC},
	{"OSH(",	CM5OSH,		TPD},
	{"PA",		CM5PA	,	TRO},
	{"PC",		CM5PC,		TRO},
	{"PRT",		CM5PRT,		TRW},
	{"SLD",		CM5SLD,		TDC},
	{"SLE",		CM5SLE,		TDC},
	{"SLM(0)",	CM5SLM0,	TDC},
	{"SLM(1)",	CM5SLM1,	TDC},
	{"SLN",		CM5SLN,		TRW},
	{"SLP",		CM5SLP,		TRW},
	{"VA",		CM5VA,		TRO},
	{"PRA",		CM5PRA	,	TRO},
	{"EA",		CM5EA,		TRO},
// Table items
	{"ab[",		CM5AB,		TAR},
	{"af[",		CM5AF,		TAR},
	{"al[",		CM5AL,		TAR},
	{"aw[",		CM5AW,		TAR},
	{"CTR(",	CM5CTR,		TPR},
	{"EIGN(",	CM5EIGN,	TPO},
	{"EOBK(",	CM5EOBK,	TPO},
	{"IN(",		CM5IO,		TPC},
	{"INA(A,",	CM5INA,		TPR},
	{"INA(V1,",	CM5INV,		TPR},
	{"IN(W,",	CM5IOW,		TPC},
	{"W(",		CM5W5,		TPR},
	{"Z(",		CM5Z5W,		TPD},
	{"OUT(",	CM5OUT,		TPX},
// Change of command between Read and Write,
	{"OR(",		CM5OR,		TPC},
	{"OS(",		CM5OS,		TPC},
	{"OR(W,",	CM5ORW,		TPD},
	{"OS(W,",	CM5OSW,		TPD},
// Change of Command vis-a-vis C4
	{"AC",		CMA5,		TRW}, // C4 = A
	{"EL",		CME5,		TRW}, // C4 = E
	{"PT",		CMP5,		TRW}, // C4 = P
	{"VT",		CMV5,		TRW}  // C4 = V
	};

//////////////////////////////////////////////////////////////////////////
//
// Animatics Driver
//

// Instantiator

INSTANTIATE(CAnimaticsDriver);

// Constants

static UINT const TIMEOUT = 500;

// Constructor

CAnimaticsDriver::CAnimaticsDriver(void)
{
	m_Ident = DRIVER_ID;
	
	CTEXT Hex[] = "0123456789ABCDEF";

	m_pHex = Hex;

	m_Error	= 0L;

	m_pItem	= &m_CmdDef;

	m_fIs485Port = FALSE;
	}

// Destructor

CAnimaticsDriver::~CAnimaticsDriver(void)
{
	}

// Configuration

void MCALL CAnimaticsDriver::Load(LPCBYTE pData)
{ 
	}
	
void MCALL CAnimaticsDriver::CheckConfig(CSerialConfig &Config)
{
	if( Config.m_uPhysical == 3 || Config.m_uPhysical == 9 ) {

		Make485(Config, TRUE);

		m_fIs485Port = TRUE;
		}
	}
	
// Management

void MCALL CAnimaticsDriver::Attach(IPortObject *pPort)
{
	m_pData = MakeDoubleDataHandler();

	pPort->Bind(m_pData);
	}

void MCALL CAnimaticsDriver::Open(void)
{
	m_pCL = (ANIMATICSCmdDef FAR * )CL;
	}

// Device

CCODE MCALL CAnimaticsDriver::DeviceOpen(IDevice *pDevice)
{
	CMasterDriver::DeviceOpen(pDevice);

	if( !(m_pCtx = (CContext *) pDevice->GetContext()) ) {

		PCBYTE pData = pDevice->GetConfig();

		if( GetWord(pData) == 0x1234 ) {

			m_pCtx = new CContext;

			m_pCtx->m_bDrop  = GetByte(pData) | 0x80;

			m_pCtx->m_bClass = ((BOOL)GetByte(pData)) ? 5 : 4;

			InitCached();

			pDevice->SetContext(m_pCtx);

			return CCODE_SUCCESS;
			}

		return CCODE_ERROR | CCODE_HARD;
		}

	return CCODE_SUCCESS;
	}

CCODE MCALL CAnimaticsDriver::DeviceClose(BOOL fPersist)
{
	if( !fPersist ) {

		delete m_pCtx;

		m_pCtx = NULL;

		m_pDevice->SetContext(NULL);
		}

	return CMasterDriver::DeviceClose(fPersist);
	}

// Entry Points

CCODE MCALL CAnimaticsDriver::Ping(void)
{
//**/	AfxTrace0("\r\nPing ");

	DWORD Data[1];

	CAddress A;

	A.a.m_Table  = addrNamed;
	A.a.m_Offset = CMa;
	A.a.m_Type   = addrLongAsLong;
	A.a.m_Extra  = 0;

	CCODE c = Read(A, Data, 1);

	if( (UINT)c != 1 ) {

		SendAnimWake(FALSE);

		return CCODE_ERROR;	// retry connect on next ping
		}

	return 1;
	}

CCODE MCALL CAnimaticsDriver::Read(AREF Addr, PDWORD pData, UINT uCount)
{
	if( !SetpItem( Addr ) ) return 1;

	UINT uCt = Addr.a.m_Table == CM5GSTR ? min(uCount, sizeof(m_pCtx->m_dResp)/4) : 1;

	if( NoReadTransmit(Addr.a.m_Offset, pData, uCt ) ) {

		return uCt;
		}

//**/	Sleep(100); // slow for debug

//**/	AfxTrace3("\r\nRead T=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
//**/	AfxTrace1("%s ", m_pItem->sName);

	DWORD dData;

	CAddress A;

	A.m_Ref	    = Addr.m_Ref;

	if( A.a.m_Table == addrNamed ) {

		if( ReadOne(A, &dData) ) {

			*pData = dData;

			return 1;
			}
		}

	else {
		BOOL f = TRUE;

		UINT t = GetTickCount() + ToTicks(500); // set timeout

		for( UINT i = 0; f && (i < uCount); i++ ) {

			if( ReadOne(A, &dData) ) {

				pData[i] = dData;

				A.a.m_Offset++;

				if( GetTickCount() > t ) {

					return i + 1;	// number of successful reads
					}
				}

			else {
				if( i ) return i;	// i successful reads completed

				f = FALSE;		// none read
				}
			}

		if( f ) {

			return uCount;
			}
		}

	m_Error = (A.a.m_Offset << 16) + LOWORD(m_Error)+1;	// Show error ID/Type

	return CCODE_ERROR;
	}

CCODE MCALL CAnimaticsDriver::Write(AREF Addr, PDWORD pData, UINT uCount)
{
//**/	AfxTrace3("\r\nT=%d O=%d C=%d ", Addr.a.m_Table, Addr.a.m_Offset, uCount);
//**/	AfxTrace1("\r\n******* %8.8lx\r\n", *pData);

	if( !SetpItem( Addr ) ) return 1;

	if( NoWriteTransmit(Addr.a.m_Offset, *pData) ) return 1;

	DWORD dData = *pData;

	BOOL fWant  = Write(Addr, pData) == 2;

	if( Transact(fWant) ) {

		WriteCached(Addr.a.m_Offset, dData);

		if( fWant ) {

			if( GetResponse(pData, Addr.a.m_Type) ) {

				if( m_pItem->Type == TGS && m_pCtx->m_fHasSubrResponse ) {

					if( (BOOL)m_pCtx->m_dStringPending ) {

						m_pCtx->m_dCurrentResponse = m_pCtx->m_dStringPending;

						m_pCtx->m_dStringPending   = 0;
						}
					}
				}
			}
		}

//**/	AfxTrace0("\r\nDone Write\r\n");
	
	return 1;
	}

// Opcode Handlers
BOOL CAnimaticsDriver::ReadOne(CAddress Addr, PDWORD pData)
{
	if( Read(Addr) ) {

		if( Transact(TRUE) ) {

			DWORD dData;

			if( GetResponse( &dData, Addr.a.m_Type ) ) {

				*pData = dData;

				return TRUE;
				}
			}
		}

	return FALSE;
	}

BOOL CAnimaticsDriver::Read(AREF Addr)
{
	StartFrame();

	AddByte('R');

	AddCommand(Addr, 0L);

	return TRUE;
	}

UINT CAnimaticsDriver::Write(AREF Addr, PDWORD pData)
{
	DWORD dData = *pData;

	StartFrame();

	UINT uOff = Addr.a.m_Offset;

	AddCommand(Addr, dData);

	switch( m_pItem->Type ) {

		case TWO:
		case TPC:
			return 1;

		case TVS:
			m_pCtx->m_dVoltageSetting[(m_pItem->uID) - CMUA] = dData;

			// fall through

		case TRW:
		case TAR:
			AddByte('=');

			if( Addr.a.m_Type == addrRealAsReal ) {

				PutReal(dData);

				return 1;
				}

			AddSignedData(dData);

			return 1;

		case TGN:
			if( IsClass5() ) {

				AddByte('(');
				}

			AddData( dData );

			if( IsClass5() ) {

				AddByte(')');
				}

			return 1;

		case TAA:
			if( dData >= 0 && dData <= 120 )
				AddData( dData );

			else return 0;

			return 1;

		case TGS:
			if( m_pCtx->m_fHasSubrResponse ) {

				m_pCtx->m_dStringPending = dData;

				m_pCtx->m_dCurrentResponse = 0;
				}

			AddData( dData );

			return 2; // response to be stored
		}

	return 1;
	}

// Header Building

void CAnimaticsDriver::StartFrame(void)
{
	m_uPtr = 0;

	AddByte(m_pCtx->m_bDrop);
	}

void CAnimaticsDriver::EndFrame(void)
{
	AddByte( CR );
	}

void CAnimaticsDriver::AddByte(BYTE bData)
{
	m_bTx[m_uPtr++] = bData;
	}

void CAnimaticsDriver::AddData(DWORD dData)
{
	char s[12];

	memset(s, 0, 12);

	SPrintf( s, "%u", dData);

	PutText( (PCTXT)s );
	}

void CAnimaticsDriver::AddSignedData(DWORD dData)
{
	char s[12];

	memset(s, 0, 12);

	SPrintf( s, "%d", dData);

	PutText( (PCTXT)s );
	}

void CAnimaticsDriver::AddParam(DWORD dPar1)
{
	AddData(dPar1);

	AddByte(')');
	}

void CAnimaticsDriver::AddCommand(AREF Addr, DWORD dData)
{
	PutText( m_pItem->sName );

	UINT uOff = Addr.a.m_Offset;

	switch( m_pItem->Type ) {

		case TAR:
			AddData(uOff);
			AddByte(']');
			break;

		case TPD:
			AddParam(dData);
			break;

		case TPO:
		case TPR:
		case TPC:
			AddParam(uOff);
			break;

		case TPX:
			AddParam(uOff);
			AddByte('=');
			AddData(dData);
			break;

		case TWX:
			AddByte('=');
			AddSignedData(dData);
			break;
		}
	}
	
void CAnimaticsDriver::PutText(LPCTXT pCmd)
{
	for( UINT i=0; pCmd[i]; i++ ) {

		AddByte( (BYTE)pCmd[i] );
		}
	}

void CAnimaticsDriver::PutReal(DWORD dData)
{
	HostAlignStack();

	char c[SZRMAX];

	memset(c, 0, SZRMAX);

	if( dData == 0x80000000 ) dData = 0; // don't send -0

	SPrintf( c, "%f", PassFloat(dData) );

	UINT u = SZRMAX - 1;

	UINT e = c[0] == '-' ? 3 : 2;

	while( u > e ) {	// strip trailing 0's

		if( c[u] <= '0' ) {

			if( c[u-1] != '.' ) {

				c[u--] = 0;
				}

			else break;
			}

		else break;
		}

	for( u = 0; u < SZRMAX; u++ ) {

		BYTE b = (BYTE)c[u];

		if( (BOOL)b ) AddByte(b);
		}
	}

// Transport Layer

BOOL CAnimaticsDriver::Transact(BOOL fWantReply)
{
	Send();

	if( !fWantReply ) {

		m_pData->Read(0);

		return TRUE;
		}

	return GetReply();
	}

void CAnimaticsDriver::Send(void)
{
	EndFrame();

	m_pData->ClearRx();

//**/	AfxTrace0("\r\n"); for( UINT i = 0; i < m_uPtr; i++ ) AfxTrace1("[%2.2x]", m_bTx[i] );

	Put();
	}

BOOL CAnimaticsDriver::GetReply(void)
{
	UINT uCount = 0;
	UINT uState = 0;

	UINT *pCnt  = &uCount;

	DWORD dResp[16];

	PDWORD pStr = dResp;

	BOOL fSubr  = m_pCtx->m_fHasSubrResponse && m_pItem->uID == CMSUBRS;

//**/	AfxTrace0("\r\n");

	SetTimer(fSubr ? 50 : 1000);

	while( GetTimer() ) {

		WORD wData = Get();

		if( LOWORD(wData) == LOWORD(NOTHING) ) {

			Sleep(20);

			continue;
			}

		if( !uState && fSubr ) {

			SetTimer(1000);
			}

		BYTE bData = LOBYTE(wData);

//**/		AfxTrace1("<%2.2x>", bData);

		switch( uState ) {

			case 0:
				if( bData & 0x80 ) {				// Address byte for echo

					if( bData == m_pCtx->m_bDrop ) {	// is this device

						uCount = 1;

						uState = 1;			// check echo
						}

					else uState = 5;			// another motor's response
					}

				else {
					if( IsNumericChar(bData, TRUE) ) {

						m_bRx[0] = bData;

						uCount   = 1;

						uState   = 2;
						}

					else {
						AddToString(pCnt, pStr, bData, TRUE);

						uState   = 3;
						}
					}

				break;

			case 1: // echo is on

				if( bData == CR ) {

					uCount = 0;

					uState = 2;				// echo done
					}

				else {
					if( m_bTx[uCount++] != bData ) {	// echo mismatch

						AddToString(pCnt, pStr, bData, TRUE);

						uState = 3;
						}
					}

				break;

			case 2:
				if( bData == CR ) {

					m_bRx[uCount] = 0;

					return TRUE;
					}

				if( IsNumericChar(bData, (BOOL)!uCount) ) {

					m_bRx[uCount++] = bData;
					}

				else {
					AddToString(pCnt, pStr, bData, TRUE);

					uState = 3;
					}

				break;

			case 3:
				AddToString(pCnt, pStr, bData, FALSE);		// add all to string until timeout		
				break;

			case 5:
				if( bData == CR ) {				// CR of echo

					uState = 6;
					}
				break;

			case 6:
				if( bData == CR ) {				// CR of EOT

					uState = 0;				// wait for this unit's data
					}
				break;
			}

		if( uCount >= sizeof(m_bRx) ) break;
		}

	if( (BOOL)m_pCtx->m_dStringPending && *pCnt ) {

		memcpy(m_pCtx->m_dResp, dResp, 64);

		m_pCtx->m_dCurrentResponse = m_pCtx->m_dStringPending;

		m_pCtx->m_dStringPending = 0;
		}

	return FALSE;
	}

BOOL CAnimaticsDriver::IsNumericChar(BYTE bData, BOOL fFirst)
{
	if( bData >= '0' && bData <= '9') {

		return TRUE;
		}

	switch( bData ){

		case '+':
		case '-':
			return fFirst;

		case '.':
			return !fFirst;
		}

	return FALSE;
	}

void CAnimaticsDriver::AddToString(UINT *pCnt, PDWORD pStr, BYTE bData, BOOL fInit)
{
	UINT uCount = *pCnt;

	if( fInit ) {

		memset(pStr, 0, 64);

		uCount = 0;
		}

	if( uCount < 64 ) {

		PDWORD p = &pStr[uCount/4];

		*p <<= 8;

		if( bData < ' ' || bData > 'z' ) {

			bData = '.';
			}

		*p  += bData;

		uCount++;

		*pCnt = uCount;
		}
	}

BOOL CAnimaticsDriver::GetResponse( PDWORD pData, UINT uType )
{
	UINT uPos = 0;
	DWORD dData = 0;
	BYTE bData;
	BOOL fFlag = FALSE;

	if( m_pItem->Type == TGS ) {

		UINT i = 0;

		while ( uPos < 64 ) {

			if( m_bRx[uPos] == CR )
				
				fFlag = TRUE;

			if( !fFlag ) {

				dData <<= 8;

				dData += m_bRx[uPos];
				}

			if( uPos % 4 == 3 ) {
				
				m_pCtx->m_dResp[i++] = dData;

				dData = 0;

				if( fFlag ) {

					while ( i < 16 ) m_pCtx->m_dResp[i++] = 0;

					return TRUE;
					}
				}

			uPos++;
			}
		
		return TRUE;
		}

	else {
		if( m_bRx[0] & 0x80 ) { // ECHO is on

			uPos = 1;

			while ( m_bRx[uPos] == m_bTx[uPos] ) {

				if( ++uPos > sizeof(m_bRx) )

					return FALSE;
				}
			}
		}

	const char * p = (const char *)&m_bRx[uPos];

	if( uType == addrRealAsReal ) {

		*pData = ATOF(p);

		return TRUE;
		}

	*pData = ATOI(p);

	return TRUE;
	}

// Port Access

void CAnimaticsDriver::Put(void)
{
	m_pData->Write( (PCBYTE)m_bTx, m_uPtr, FOREVER);
	}

WORD CAnimaticsDriver::Get(void)
{
	WORD wData;

	wData = m_pData->Read(TIMEOUT);

	return wData;
	}

// Helpers
BOOL CAnimaticsDriver::SetpItem(AREF Addr)
{
	CAddress A;

	A.m_Ref = FixClass( Addr );

	if( A.m_Ref ) {

		UINT uID = A.a.m_Table == addrNamed ? A.a.m_Offset : A.a.m_Table;

		if( ScanCmdDef(uID) ) return TRUE;
		}

	m_pItem->sName[0] = 'a';
	m_pItem->sName[1] = 0;
	m_pItem->Type     = TRW;
	m_pItem->uID      = CMa;

	return FALSE;
	}

DWORD CAnimaticsDriver::FixClass(AREF Addr)
{
	CAddress A;

	A.m_Ref      = Addr.m_Ref;

	UINT uTable  = A.a.m_Table;

	UINT uOffset = A.a.m_Offset;

	BOOL fNamed  = uTable == addrNamed;

	if( IsClass5() ) {

		if( fNamed ) {

			switch( uOffset ) { // modify class 4 commands

				case CMA:	A.a.m_Offset = CMA5;	break;
				case CME:	A.a.m_Offset = CME5;	break;
				case CMP:	A.a.m_Offset = CMP5;	break;
				case CMV:	A.a.m_Offset = CMV5;	break;
				case CMUDM:	A.a.m_Offset = CM5EILN;	break;
				case CMUCP:	A.a.m_Offset = CM5EILP;	break;
				case CMUGC:	A.a.m_Offset = CM5EISM;	break;
				case CMCTR:	A.a.m_Offset = CM5CTR;	break;
				// not in class 5
				case CMMS:	A.m_Ref = 0;		break;

				default:
					if( uOffset >= CMUA && uOffset <= CMUG ) {

						A.a.m_Table	= CM5IO;
						A.a.m_Offset	= uOffset - CMUA;
						}

					if( uOffset >= CMUAI && uOffset <= CMUGI ) {

						A.a.m_Table	= CM5EIGN;
						A.a.m_Offset	= uOffset - CMUAI;
						}

					if( uOffset >= CMUAO && uOffset <= CMUGO ) {

						A.a.m_Table	= CM5OUT;
						A.a.m_Offset	= uOffset - CMUAO;
						}
					break;
				}
			}
		}

	else {
		if( !fNamed || (uOffset > LASTCLASS4) ) {	// not in class 4

			A.a.m_Table  = addrNamed;
			A.a.m_Offset = CMaaa;
			A.a.m_Extra  = 0;
			A.a.m_Type   = addrLongAsLong;
			}
		}

	return A.m_Ref;
	}

BOOL CAnimaticsDriver::ScanCmdDef(UINT uID)
{
	ANIMATICSCmdDef * pItem = m_pCL;

	memset(m_pItem->sName, 0, 12);

	for( UINT i = 0; i < elements(CL); i++ ) {

		if( uID == pItem->uID ) {

			memcpy(m_pItem->sName, pItem->sName, strlen((const char *)pItem->sName));

			m_pItem->Type = pItem->Type;

			m_pItem->uID  = uID;

			return TRUE;
			}

		pItem++;
		}

	return FALSE;
	}

BOOL CAnimaticsDriver::IsClass5(void)
{
	return m_pCtx->m_bClass == 5;
	}

BOOL CAnimaticsDriver::NoReadTransmit(UINT uOffset, PDWORD pData, UINT uCount)
{
	if( ReadCached(uOffset, pData, uCount) ) return TRUE;

	switch ( m_pItem->Type ) {

		case TWO:
		case TGN:
		case TGS:
		case TPX:
		case TWX:
			*pData = 0xFFFFFFFF;
			return TRUE;

		case TPO:
		case TDC:
			*pData = 0;
			return TRUE;

		default: break;
		}

	return FALSE;
	}

BOOL CAnimaticsDriver::NoWriteTransmit(UINT uOffset, DWORD dData)
{
	BOOL f = (BOOL)dData;

	switch( m_pItem->Type ) {

		case TRO:
		case TPR:
			return TRUE;

		case TPC: // change command for write
			switch( m_pItem->uID ) {

				case CM5IO:  ScanCmdDef(f ? CM5OS : CM5OR); break;

				case CM5IOW: ScanCmdDef(f ? CM5OSW : CM5ORW); break;
				}
			break;
			
		case TGC:
			memset(m_pCtx->m_dResp, 0, sizeof(m_pCtx->m_dResp));
		case TGR:
			m_pCtx->m_dCurrentResponse = 0;
			return TRUE;
		}

	switch( m_pItem->uID ) {

		case CM5EIGN:
		case CM5EISM:
		case CM5MINV0:
		case CM5MINV1:
		case CM5SLD:
		case CM5SLE:
		case CM5SLM0:
		case CM5SLM1:
			return !f;
		}

	return FALSE;
	}

BOOL CAnimaticsDriver::ReadCached(UINT uOffset, PDWORD pData, UINT uCount)
{
	switch( m_pItem->Type ) {

		case TGC:
			UINT uFirst;
			UINT i;

			uFirst = m_pItem->uID == CM5GSTR ? 0 : m_pItem->uID - CMSUBR1;

			for( i = 0; i < uCount; i++ ) {

				*pData = m_pCtx->m_dResp[uFirst + i];

				pData++;
				}

			m_pCtx->m_fHasSubrResponse = TRUE;
			break;

		case TVS:
			*pData = m_pCtx->m_dVoltageSetting[(m_pItem->uID) - CMUA];
			break;

		case TGR:
			*pData = m_pCtx->m_dCurrentResponse;
			break;

		case TAA:
			*pData = m_pCtx->m_bDrop & 0x7F;
			break;

		case TPD:
			switch( m_pItem->uID ) {

				case CM5MFA: *pData = m_pCtx->m_dMFA; break;
				case CM5MFD: *pData = m_pCtx->m_dMFD; break;
				case CM5MFS: *pData = m_pCtx->m_dMFS; break;
				case CM5OSH: *pData = m_pCtx->m_dOSH; break;
				}
			break;

		default:
			return FALSE;
		}

	return TRUE;
	}

void CAnimaticsDriver::WriteCached(UINT uOffset, DWORD dData)
{
	switch( m_pItem->uID ) {

		case CM5MFA: m_pCtx->m_dMFA = dData; break;
		case CM5MFD: m_pCtx->m_dMFD = dData; break;
		case CM5MFS: m_pCtx->m_dMFS = dData; break;
		case CM5OSH: m_pCtx->m_dOSH = dData; break;
		}
	}

void CAnimaticsDriver::InitCached(void)
{
	for( UINT i = 0; i < 7; i++ ) {

		m_pCtx->m_dVoltageSetting[i] = 0;
		}

	m_pCtx->m_dMFA = 0L;
	m_pCtx->m_dMFD = 0L;
	m_pCtx->m_dMFS = 0L;
	m_pCtx->m_dOSH = 0L;

	InitStrings();
	}

void CAnimaticsDriver::InitStrings(void)
{
	memset(m_pCtx->m_dResp, 0, 64);

	m_pCtx->m_dStringPending   = 0L;
	m_pCtx->m_dCurrentResponse = 0L;
	m_pCtx->m_fHasSubrResponse = FALSE;
	}

BOOL CAnimaticsDriver::SendAnimWake(BOOL fAll)
{
	StartFrame();

	if( fAll ) {

		m_bTx[0] = 0x80;
		}

	AddByte('W');
	AddByte('A');
	AddByte('K');
	AddByte('E');

	if( m_fIs485Port ) {

		AddByte('1');
		}

	m_bRx[0] = 0;

	Send();

	GetReply();

	Sleep(50);

	return (BOOL)m_bRx[0];
	}

// End of File
