
//////////////////////////////////////////////////////////////////////////
//
// SNMP Agent
//
// Copyright (c) 2010 Red Lion Controls Inc.
//
// All Rights Reserved.
//

#ifndef INCLUDE_SNMP_Asn1Base_HPP

#define INCLUDE_SNMP_Asn1Base_HPP

//////////////////////////////////////////////////////////////////////////
//
// Internal Header
//

#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// ASN.1 Base Types
//

class CAsn1
{
	public:
		// Type Tags
		enum
		{
			asnConstructed = 1,
			asnInteger     = 2,
			asnBitString   = 3,
			asnOctString   = 4,
			asnNull        = 5,
			asnObjectID    = 6,
			asnSequence    = 16,
			asnSet         = 17,
			asnPrintable   = 19,
			asnT61         = 20,
			asnIA5	       = 22,
			asnTime        = 23,
			asnClose       = 31,
			};
	};

// End of File

#endif
