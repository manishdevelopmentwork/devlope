
#include "Intern.hpp"

////////////////////////////////////////////////////////////////////////
//
// Crimson 3.1 Communications Subsystem
//
// Copyright (c) 1997-2019 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_UITextAction_HPP

#define INCLUDE_UITextAction_HPP

////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "UITextCoded.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Text UI Element -- Action Item
//

class CUITextAction : public CUITextCoded
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CUITextAction(void);

		// Default Type
		void FindReqType(CTypeDef &Type);
	};

// End of File

#endif
