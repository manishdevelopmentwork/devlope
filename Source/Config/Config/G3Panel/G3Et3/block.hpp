
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Ethertrak-3 System
//
// Copyright (c) 1993-2015 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_BLOCK_HPP
	
#define	INCLUDE_BLOCK_HPP

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class CTransferBlockList;
class CTransferBlockItem;
class CTransferBlockDiscreteItem;
class CTransferBlockAnalogItem;

//////////////////////////////////////////////////////////////////////////
//
// Data Log List
//

class CTransferBlockList : public CNamedList
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferBlockList(void);

		// Item Access
		CTransferBlockItem * GetItem(INDEX Index) const;
		CTransferBlockItem * GetItem(UINT uPos) const;
		CTransferBlockItem * GetItem(CString Name ) const;

		// Persistence
		void Init(void);
		void PostLoad(void);

	protected:
		// Data
		CEt3CommsSystem * m_pSystem;
	};

//////////////////////////////////////////////////////////////////////////
//
// Transfer Item
//

class CTransferBlockItem : public CEt3UIItem
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();

		// Constructor
		CTransferBlockItem(void);

		// UI Creation
		BOOL OnLoadPages(CUIPageList *pList);

		// UI Update
		void OnUIChange(IUIHost *pHost, CItem *pItem, CString Tag);

		// Attributes
		virtual UINT GetTreeImage(void) const	= 0;

		// Item Location
		CTransferBlockList * GetParentList(void) const;
		CEt3CommsDevice    * GetParentDevice(void) const;
		CEt3CommsPort      * GetParentPort(void) const;
		
		// Item Naming
		CString GetHumanName(void) const;

		// Persistence
		void Init(void);
		void PostLoad(void);
		void Kill(void);

		// Download Support
		void PrepareData(void);

		// Data Members
		CString		m_Name;
		UINT		m_Direction;
		UINT		m_SrceType;
		UINT		m_SrceAddr;
		UINT		m_DestType;
		UINT		m_DestAddr;
		UINT		m_Count;
		UINT		m_Status;

	protected:

		// Meta Data Creation
		void AddMetaData(void);

		// Implementation
		void DoEnables(IUIHost *pHost);

		// Config File Help
		UINT   BuildType(void);
		UINT   BuildStation(void);
		UINT   BuildInterface(void);
		PCBYTE BuildIPAddr(void);
		UINT   BuildDestPort(void);
		
		BOOL IsDeviceSixnet(void);
		BOOL IsDeviceModbus(void);
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Discrete
//

class CTransferBlockDiscreteItem : public CTransferBlockItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferBlockDiscreteItem(void);

		// Attributes
		UINT GetTreeImage(void) const;
	};
	
//////////////////////////////////////////////////////////////////////////
//
// Transfer Item - Analog
//

class CTransferBlockAnalogItem : public CTransferBlockItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTransferBlockAnalogItem(void);

		// Attributes
		UINT GetTreeImage(void) const;
	};

// End of File

#endif
