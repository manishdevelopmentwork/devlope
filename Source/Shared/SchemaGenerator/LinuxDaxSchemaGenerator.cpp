
#include "Intern.hpp"

#include "LinuxDaxSchemaGenerator.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE
//
// Copyright (c) 1993-2019 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "SchemaFiles.hpp"

//////////////////////////////////////////////////////////////////////////
//
// DAx Schema Generator
//

// Options

enum
{
	optNone		 = 0x000,
	optLinkCheck	 = 0x001,
	optDhcpServer    = 0x002,
	optIptForward    = 0x004,
	optDynamicDns    = 0x008,
	optAdditionalIps = 0x010,
	optRouting	 = 0x020,
	optSimple	 = 0x040,
	optMacFilter	 = 0x080,
	optStatus	 = 0x100,
	optConfig	 = 0x200,
	optEth		 = 0x3BB,
	optAll	         = 0x3FF,
};

// Instantiators

DLLAPI ISchemaGenerator * Create_LinuxDaxSchemaGenerator(void)
{
	return new CLinuxDaxSchemaGenerator;
}

DLLAPI ISchemaGenerator * Create_LinuxDaxSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel)
{
	return new CLinuxDaxSchemaGenerator(pConfig, pModel);
}

// Constructors

CLinuxDaxSchemaGenerator::CLinuxDaxSchemaGenerator(void)
{
	m_Default = "sc-linux";

	m_pModel  = NULL;

	#if defined(AEON_ENVIRONMENT)

	AfxGetObject("c3.model", 0, IPxeModel, m_pModel);

	#endif
}

CLinuxDaxSchemaGenerator::CLinuxDaxSchemaGenerator(IConfigStorage *pConfig, IPxeModel *pModel)
{
	m_Default = "sc-linux";

	m_pModel  = pModel;

	m_pConfig = pConfig;

	m_pModel->AddRef();

	m_pConfig->AddRef();
}

// Destructor

CLinuxDaxSchemaGenerator::~CLinuxDaxSchemaGenerator(void)
{
	AfxRelease(m_pModel);
}

// Overridables

bool CLinuxDaxSchemaGenerator::MakeHardwareSchema(void)
{
	CAutoPointer<CJsonData> pJson(GetFileJson(T("hs-dax")));

	if( pJson ) {

		AddPermittedGroups(pJson, m_pModel->GetObjCount('g'));

		CArray<DWORD> List;

		m_pModel->GetDispList(List);

		AddDisplaySize(pJson, List);

		UINT ns = m_pModel->GetObjCount('s');

		UINT nm = m_pModel->GetObjCount('m');

		UINT nx = m_pModel->GetObjCount('x');

		if( ns == 2 ) {

			AddFragment(pJson, T("children.0001.tabs.0000.sections"), T("hs-dax-serial-50"));

			AddFragment(pJson, T("children.0001.tabs.0000.sections"), T("hs-dax-vlans"));

			AddFragment(pJson, T("children.0001.tabs.0000.sections"), T("hs-dax-tools"));
		}
		else {
			AddFragment(pJson, T("children.0001.tabs.0000.sections"), T("hs-dax-serial-70"));

			AddFragment(pJson, T("children.0001.tabs.0000.sections"), T("hs-dax-vlans"));

			AddFragment(pJson, T("children.0001.tabs.0000.sections"), T("hs-dax-tools"));
		}

		if( !nm ) {

			pJson->Delete(T("children.0003"));
		}

		pJson->AddValue(T("children.0002.tabs.0000.sections.0000.table.rows"), CPrintf("%u", nx));

		AddFragment(pJson, T("children"), T("hs-dax-tunnels"));

	//	AddFragment(pJson, T("children"), T("hs-ext-modem"));

	//	AddFragment(pJson, T("children"), T("hs-ext-gps"));

		m_HSchema = pJson->GetAsText(true);

		CheckConfig('h');

		return true;
	}

	return false;
}

bool CLinuxDaxSchemaGenerator::MakePersonalitySchema(void)
{
	m_PSchema = GetFileText(T("ps-master"));

	return true;
}

bool CLinuxDaxSchemaGenerator::MakeSystemSchema(void)
{
	CAutoPointer<CJsonData> pJson(GetFileJson(T("ss-linux")));

	if( pJson ) {

		CAutoPointer<CJsonData> pHard(New CJsonData);

		pHard->Parse(GetHardwareConfig());

		m_EnumFace   = "";
		m_EnumBridge = "";
		m_EnumGps    = "";
		m_EnumCell   = "";
		m_EnumTrans  = "";

		m_uEth  = 0;
		m_uVLan = 0;
		m_uPpp  = 0;
		m_uWiFi = 0;
		m_uTuns = 0;

		AddHardwareInterfaces(pJson, pHard);

		AddTunnelInterfaces(pJson, pHard);

		CJsonData *pAliases = pJson->GetChild("aliases");

		pAliases->AddValue("ethModeSled", "Disabled|Configured via DHCP|Manual Configuration|VLANs Only|IP Transparency" + m_EnumBridge);

		pAliases->AddValue("ethModeOTap", "1=Configured via DHCP|2=Manual Configuration" + m_EnumBridge);

		pAliases->AddValue("afaces", "Automatic" + m_EnumFace);

		pAliases->AddValue("tfaces", "Automatic" + m_EnumTrans);

		pAliases->AddValue("nfaces", "None" + m_EnumFace);

		pAliases->AddValue("xfaces", "All Interfaces" + m_EnumFace);

		pAliases->AddValue("gpstime", "Automatic" + m_EnumGps);

		pAliases->AddValue("celltime", "Automatic" + m_EnumCell);

		pAliases->AddValue("locs", "None|Fixed" + m_EnumGps);

		AddFirewall(pJson);

		AddServices(pJson);

		m_SSchema = pJson->GetAsText(true);

		CheckConfig('s');

		return true;
	}

	return false;
}

bool CLinuxDaxSchemaGenerator::MakeUserSchema(void)
{
	m_USchema = GetFileText(T("us-master"));

	return true;
}

bool CLinuxDaxSchemaGenerator::AdjustDefault(char cTag, CJsonData *pData)
{
	if( cTag == 'h' ) {

		UINT uMask = m_pModel->GetObjCount('g');

		if( uMask == 32 ) {

			pData->AddValue(T("general.group"), CPrintf(T("%u"), 5), jsonString);
		}
		else {
			for( INT g = 4; g >= 0; g-- ) {

				if( uMask & (1 << g) ) {

					pData->AddValue(T("general.group"), CPrintf(T("%u"), g), jsonString);

					break;
				}
			}
		}

		m_pModel->AdjustHardware(pData);
	}

	return true;
}

// Implementation

void CLinuxDaxSchemaGenerator::AddHardwareInterfaces(CJsonData *pSchema, CJsonData *pHard)
{
	CJsonData *pList = pSchema->GetChild("children.0000.children.0001.children");

	AddBaseEthernetPorts(pList, pHard);

	ScanForEthernetSleds(pList, pHard);

	#if 0

	if( pHard->GetValue("modem.enable", "0") == "1" ) {

		CJsonData *pFace = AddInterface(pList, nm, nm, "ss-rlos-ppp-ext", optNone);

		pFace->GetChild("tabs.0000")->AddValue("note", "External Modem");

		nm++;
	}

	#endif

	ScanForCellularSleds(pList, pHard);

	ScanForWiFiSleds(pList, pHard);
}

void CLinuxDaxSchemaGenerator::AddTunnelInterfaces(CJsonData *pSchema, CJsonData *pHard)
{
	CJsonData *pList = pSchema->GetChild("children.0000.children.0002.children");

	CJsonData *pTuns = pHard->GetChild("tunnels");

	if( pTuns ) {

		AddTunnels(pList, T("tun-open"), T("VPN"), T("tun"), 5300, tatoi(pTuns->GetValue("openvpn", "0")));

		AddTunnels(pList, T("tun-otap"), T("TAP"), T("tap"), 5300, tatoi(pTuns->GetValue("opentap", "0")));

		AddTunnels(pList, T("tun-gre"), T("GRE"), T("grex"), 5000, tatoi(pTuns->GetValue("gre", "0")));

		AddTunnels(pList, T("tun-ipsec"), T("IPsec"), T("ipsec"), 5100, tatoi(pTuns->GetValue("ipsec", "0")));

		AddTunnels(pList, T("tun-l2tp"), T("L2TP"), T("l2tp"), 5200, tatoi(pTuns->GetValue("l2tp", "0")));
	}
}

void CLinuxDaxSchemaGenerator::AddBaseEthernetPorts(CJsonData *pList, CJsonData *pHard)
{
	CJsonData *pBase = pHard->GetChild("base");

	UINT ne = m_pModel->GetObjCount('e');

	for( UINT f = 0; f < ne; f++ ) {

		CPrintf    Where("Base Unit Port %u", 1+f);

		CPrintf    Name ("Ethernet %u", 1+m_uEth);

		UINT       uFace = 1000+100*f;

		UINT       uByte = f ? 222 : MakeEthByte();

		CJsonData *pFace = AddInterface(pList,
						T("eth-base"),
						Name,
						CPrintf("eth%u", f),
						uByte,
						optEth
		);

		AddNote(pFace, Where);

		m_EnumBridge += CPrintf(T("|%u=Bridged with %s"), uFace, PCTXT(Name));

		m_EnumFace   += CPrintf(T("|%u=%s"), uFace, PCTXT(Name));

		m_EnumTrans  += CPrintf(T("|%u=%s"), uFace, PCTXT(Name));

		if( pBase ) {

			UINT nv = tatoi(pBase->GetValue(CPrintf(T("vlan%u"), 1+f), "0"));

			if( nv ) {

				CJsonData *pSub = NULL;

				pFace->AddChild("children", TRUE, pSub);

				for( UINT v = 0; v < nv; v++ ) {

					CPrintf    Name(T("VLAN %u%c"), 1+f, 'a'+v);

					UINT       uFace = 1001 + 100*f + v;

					CJsonData *pFace = AddInterface(pSub,
									T("vlan"),
									Name,
									CPrintf("eth%u%c", f, 'a'+v),
									MakeVLanByte(),
									optEth
					);

					AddNote(pFace, Where);

					m_EnumBridge += CPrintf(T("|%u=Bridged with %s"), uFace, PCTXT(Name));

					m_EnumFace   += CPrintf(T("|%u=%s"), uFace, PCTXT(Name));

					m_uVLan++;
				}
			}
		}

		m_uEth++;
	}
}

void CLinuxDaxSchemaGenerator::ScanForEthernetSleds(CJsonData *pList, CJsonData *pHard)
{
	UINT ns = m_pModel->GetObjCount('s');

	for( UINT s = 0; s < ns; s++ ) {

		CJsonData *pSlot = pHard->GetChild(CPrintf("sleds.slist.%4.4X", s));

		if( pSlot ) {

			if( UINT(tatoi(pSlot->GetValue("type", "0"))) == 101 ) {

				for( UINT f = 0; f < 2; f++ ) {

					CPrintf    Where("Sled %u Port %u", 1+s, 1+f);

					CPrintf    Name ("Ethernet %u", 1+m_uEth);

					CJsonData *pFace = AddInterface(pList,
									T("eth-sled"),
									Name,
									CPrintf("eth%us%u", f, 1+s),
									MakeEthByte(),
									optEth
					);

					AddNote(pFace, Where);

					m_EnumFace  += CPrintf(T("|%u=%s"), 2000+200*s+100*f, PCTXT(Name));

					m_EnumTrans += CPrintf(T("|%u=%s"), 2000+200*s+100*f, PCTXT(Name));

					UINT nv = tatoi(pSlot->GetValue(CPrintf(T("vlan%u"), 1+f), "0"));

					if( nv ) {

						CJsonData *pSub = NULL;

						pFace->AddChild("children", TRUE, pSub);

						for( UINT v = 0; v < nv; v++ ) {

							CPrintf    Name(T("VLAN %u%c"), m_uEth, 'a'+v);

							CJsonData *pFace = AddInterface(pSub,
											T("vlan"),
											Name,
											CPrintf("eth%us%u%c", f, 1+s, 'a'+v),
											MakeVLanByte(),
											optEth
							);

							AddNote(pFace, Where);

							m_EnumFace += CPrintf(T("|%u=%s"), 2000+200*s+100*f+v, PCTXT(Name));

							m_uVLan++;
						}
					}

					m_uEth++;
				}
			}
		}
	}
}

void CLinuxDaxSchemaGenerator::ScanForCellularSleds(CJsonData *pList, CJsonData *pHard)
{
	UINT ns = m_pModel->GetObjCount('s');

	for( UINT s = 0; s < ns; s++ ) {

		CJsonData *pSlot = pHard->GetChild(CPrintf("sleds.slist.%4.4X", s));

		if( pSlot ) {

			if( UINT(tatoi(pSlot->GetValue("type", "0"))) == 100 ) {

				CPrintf    Where("Sled %u", 1+s);

				CPrintf    Name ("Modem %u", 1+m_uPpp);

				CJsonData *pFace = AddInterface(pList,
								T("cell-sled"),
								Name,
								CPrintf("wwan%u", s),
								0,
								optLinkCheck | optIptForward | optDynamicDns | optSimple | optStatus | optConfig
				);

				m_EnumFace += CPrintf("|%u=%s", 3000+s, PCTXT(Name));

				m_EnumCell += CPrintf("|%s", PCTXT(Name));

				m_EnumGps  += CPrintf("|%s", PCTXT(Name));

				AddNote(pFace, Where);

				m_uPpp++;
			}
		}
	}
}

void CLinuxDaxSchemaGenerator::ScanForWiFiSleds(CJsonData *pList, CJsonData *pHard)
{
	UINT ns = m_pModel->GetObjCount('s');

	for( UINT s = 0; s < ns; s++ ) {

		CJsonData *pSlot = pHard->GetChild(CPrintf("sleds.slist.%4.4X", s));

		if( pSlot ) {

			if( UINT(tatoi(pSlot->GetValue("type", "0"))) == 102 ) {

				CPrintf    Where = CPrintf("Sled %u", 1+s);

				CPrintf	   Name  = CPrintf("Wi-Fi %u", 1+m_uWiFi);

				CJsonData *pFace = AddInterface(pList,
								T("wifi-sled"),
								Name,
								CPrintf("wlan0s%u", 1+s),
								MakeWiFiByte(),
								optLinkCheck | optDhcpServer | optDynamicDns | optRouting | optMacFilter | optStatus | optConfig
				);

				AddNote(pFace, Where);

				m_EnumFace += CPrintf("|%u=%s", 4000+s, PCTXT(Name));

				m_uWiFi++;
			}
		}
	}
}

void CLinuxDaxSchemaGenerator::AddTunnels(CJsonData *pList, PCTXT pType, PCTXT pName, PCTXT pDev, UINT uBase, UINT uCount)
{
	UINT uByte = 10 * (1 + (uBase / 100) - 50);

	for( UINT n = 0; n < uCount; n++ ) {

		CPrintf    Name  = CPrintf("%s %u", pName, 1+n);

		CJsonData *pFace = AddInterface(pList,
						pType,
						Name,
						CPrintf("%s%u", pDev, n),
						uByte++,
						optStatus | optConfig
		);

		AfxTouch(pFace);

		m_EnumFace += CPrintf("|%u=%s", uBase+n, PCTXT(Name));

		m_uTuns++;
	}
}

CJsonData * CLinuxDaxSchemaGenerator::AddInterface(CJsonData *pList, PCTXT pType, PCTXT pName, PCTXT pDev, UINT uByte, UINT uOpts)
{
	CJsonData *pFace = NULL;

	pList->AddChild(FALSE, pFace);

	CString Frag = "ss-linux-" + CString(pType);

	pFace->Parse(GetFileText(Frag));

	if( uOpts ) {

		CJsonData *pTabs = pFace->GetChild("tabs");

		for( UINT uMask = 1; uMask < optAll; uMask <<= 1 ) {

			if( uOpts & uMask ) {

				CJsonData *pOpt = NULL;

				pTabs->AddChild(FALSE, pOpt);

				switch( uMask ) {

					case optLinkCheck:
						pOpt->Parse(GetFileText(T("ss-linux-iface-checker")));
						break;

					case optDhcpServer:
						pOpt->Parse(GetFileText(T("ss-linux-iface-dhcp-s")));
						break;

					case optIptForward:
						pOpt->Parse(GetFileText(T("ss-linux-iface-iptfwd")));
						break;

					case optDynamicDns:
						pOpt->Parse(GetFileText(T("ss-linux-iface-dyndns")));
						break;

					case optAdditionalIps:
						pOpt->Parse(GetFileText(T("ss-linux-iface-addips")));
						break;

					case optRouting:
						pOpt->Parse(GetFileText(T("ss-linux-iface-routing")));
						break;

					case optSimple:
						pOpt->Parse(GetFileText(T("ss-linux-iface-simple")));
						break;

					case optMacFilter:
						pOpt->Parse(GetFileText(T("ss-linux-iface-macs")));
						break;

					case optStatus:
						pOpt->Parse(GetFileText(T("ss-linux-iface-status")));
						break;

					case optConfig:
						pOpt->Parse(GetFileText(T("ss-linux-iface-config")));
						break;
				}
			}
		}
	}

	Substitute(pFace, "{n}", pName);

	Substitute(pFace, "{d}", pDev);

	Substitute(pFace, "{x}", CPrintf("%u", uByte));

	Substitute(pFace, "{v}", CPrintf("%u", uByte % 100));

	return pFace;
}

void CLinuxDaxSchemaGenerator::AddNote(CJsonData *pItem, PCTXT pNote)
{
	CJsonData *pTabs = pItem->GetChild("tabs");

	for( UINT n = 0; n < pTabs->GetCount(); n++ ) {

		CJsonData *pTab = pTabs->GetChild(n);

		pTab->AddValue("note", pNote);
	}
}

UINT CLinuxDaxSchemaGenerator::MakeEthByte(void)
{
	return 1 + m_uEth;
}

UINT CLinuxDaxSchemaGenerator::MakeVLanByte(void)
{
	return 201 + m_uVLan;
}

UINT CLinuxDaxSchemaGenerator::MakeWiFiByte(void)
{
	return 101 + m_uWiFi;
}

void CLinuxDaxSchemaGenerator::AddServices(CJsonData *pSchema)
{
	PCTXT pName[] = {

		T("s-time"),
		T("s-ftps"),
		T("s-loc"),
//		T("l-geo"),
		T("l-dhcp-s"),
		T("l-dhcp-r"),
		T("l-tls-s"),
		T("l-tls-c"),
		T("s-sms"),
		T("s-smtp"),
		T("s-svm"),
		T("l-snmp"),
		T("l-syslog"),
		T("i-io")
	};

	CJsonData *pList = pSchema->GetChild("children.0001.children");

	for( UINT s = 0; s < elements(pName); s++ ) {

		CString Frag;
		
		if( pName[s][0] == 'l' || pName[s][0] == 'i' ) {

			if( pName[s][0] == 'i' ) {

				if( !m_pModel->GetObjCount('i') ) {

					continue;
				}
			}

			Frag = "ss-linux-serv-" + CString(pName[s]+2);
		}

		if( pName[s][0] == 's' ) {

			Frag = "ss-serv-" + CString(pName[s]+2);
		}

		AddFragment(pList, Frag);
	}
}

void CLinuxDaxSchemaGenerator::AddFirewall(CJsonData *pSchema)
{
	PCTXT pName[] = {

		T("lists"),
		T("inbound"),
		T("filters"),
		T("forward"),
		T("nat"),
		T("custom"),
	};

	CJsonData *pList = pSchema->GetChild("children.0000.children.0003.children");

	for( UINT s = 0; s < elements(pName); s++ ) {

		CString Frag = "ss-linux-fw-" + CString(pName[s]);

		AddFragment(pList, Frag);
	}
}

// End of File
