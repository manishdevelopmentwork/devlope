
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

#ifndef	INCLUDE_DADIDOInput_HPP

#define INCLUDE_DADIDOInput_HPP

//////////////////////////////////////////////////////////////////////////
//
// DADIDO DI
//

class CDADIDOInput : public CCommsItem
{
public:
	// Runtime Class
	AfxDeclareRuntimeClass();

	// Constructor
	CDADIDOInput(void);

	// Group Names
	CString GetGroupName(WORD wGroup);

	// Item Properties
	UINT m_Input1;
	UINT m_Input2;
	UINT m_Input3;
	UINT m_Input4;
	UINT m_Input5;
	UINT m_Input6;
	UINT m_Input7;
	UINT m_Input8;

	UINT m_Count1;
	UINT m_Count2;
	UINT m_Count3;
	UINT m_Count4;
	UINT m_Count5;
	UINT m_Count6;
	UINT m_Count7;
	UINT m_Count8;

protected:
	// Static Data
	static CCommsList const m_CommsList[];

	// Implementation
	void AddMetaData(void);
};

// End of File

#endif
