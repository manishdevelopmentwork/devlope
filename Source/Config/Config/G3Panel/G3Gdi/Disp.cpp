
#include "intern.hpp"

#include "StdPalette.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Mapping Layer
//
// Copyright (c) 1993-2011 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Display APIs
//

global DWORD DispGetPaletteEntry(UINT uIndex)
{
	uIndex *= 3;

	if( uIndex < elements(StdPalette) ) {

		return RGB( StdPalette[uIndex + 0],
			    StdPalette[uIndex + 1],
			    StdPalette[uIndex + 2]
			    );
		}

	return 0;
	}

// End of File
