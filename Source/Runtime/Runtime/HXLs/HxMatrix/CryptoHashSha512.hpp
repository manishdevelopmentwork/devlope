
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

#ifndef INCLUDE_CryptoHashSha512_HPP

#define INCLUDE_CryptoHashSha512_HPP

//////////////////////////////////////////////////////////////////////////
//
// Instantiated Classes
//

#include "CryptoHash.hpp"

//////////////////////////////////////////////////////////////////////////
//
// SHA512 Cryptographic Hash
//

class CCryptoHashSha512 : public CCryptoHash
{
	public:
		// Constructor
		CCryptoHashSha512(void);

		// ICryptoHash
		CString GetName(void);
		void    GetHashOid(CByteArray &oid);
		void    Initialize(void);
		void    Update(PCBYTE pData, UINT uData);
		void    Finalize(void);

	protected:
		// Data Members
		BYTE       m_bHash[SHA512_HASH_SIZE];
		psSha512_t m_Ctx;
	};

// End of File

#endif
