
//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2003 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef	INCLUDE_TOTALFLOW_HPP
	
#define	INCLUDE_TOTALFLOW_HPP

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Master Device Options
//

class CTotalFlowMasterDeviceOptions : public CUIItem
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlowMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

		// Data Access
		UINT GetApp(void);

	protected:
		// Data Members
		CString m_Name;
		CString m_Code;
		UINT	m_App;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Serial Master Device Options
//

class CTotalFlowSerialMasterDeviceOptions : public CTotalFlowMasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlowSerialMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT	m_Estab;
		UINT    m_Time1;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow TCP/IP Master Device Options
//

class CTotalFlowTcpMasterDeviceOptions : public CTotalFlowMasterDeviceOptions
{
	public:
		// Dynamic Class
		AfxDeclareDynamicClass();

		// Constructor
		CTotalFlowTcpMasterDeviceOptions(void);

		// UI Management
		void OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag);

		// Download Support
		BOOL MakeInitData(CInitData &Init);

	protected:
		// Data Members
		UINT    m_IP;
		UINT    m_Port;
		UINT    m_Keep;
		UINT    m_Ping;
		UINT    m_Time1;
		UINT    m_Time2;
		UINT    m_Time3;
		CString m_Name;
		CString m_Code;

		// Meta Data Creation
		void AddMetaData(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Master Comms Driver
//

class CTotalFlowMasterDriver : public CBasicCommsDriver
{
	public:
		// Constructor
		CTotalFlowMasterDriver(void);

		// Destructor
		~CTotalFlowMasterDriver(void);

		// Address Management
		BOOL SelectAddress(HWND hWnd, CAddress &Addr, CItem *pConfig, BOOL fPart);
		BOOL ParseAddress(CError &Error, CAddress &Addr, CItem *pConfig, CString Text);
		BOOL ExpandAddress(CString &Text, CItem *pConfig, CAddress const &Addr);
		BOOL ListAddress(CAddrData *pRoot, CItem *pConfig, UINT uItem, CAddrData &Data);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow Serial Master Comms Driver
//

class CTotalFlowSerialMasterDriver : public CTotalFlowMasterDriver
{
	public:
		// Constructor
		CTotalFlowSerialMasterDriver(void);

		// Destructor
		~CTotalFlowSerialMasterDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// TotalFlow TCP/IP Master Comms Driver
//

class CTotalFlowTcpMasterDriver : public CTotalFlowMasterDriver
{
	public:
		// Constructor
		CTotalFlowTcpMasterDriver(void);

		// Destructor
		~CTotalFlowTcpMasterDriver(void);

		// Binding
		UINT GetBinding(void);
		void GetBindInfo(CBindInfo &Info);

		// Configuration
		CLASS GetDriverConfig(void);
		CLASS GetDeviceConfig(void);
	};

//////////////////////////////////////////////////////////////////////////
//
// Address Selection Dialog
//

class CTotalFlowDialog : public CStdDialog
{
	public:
		// Runtime Class
		AfxDeclareRuntimeClass();
		                
		// Constructor
		CTotalFlowDialog(CTotalFlowMasterDriver * pDriver, CAddress &Addr, CItem *pConfig, BOOL fPart);
		                
	protected:
		// Data
		CAddress * m_pAddr;
		CItem	 * m_pConfig;
		BOOL	   m_fPart;

		CTotalFlowMasterDriver * m_pDriver;
		
		// Message Map
		AfxDeclareMessageMap();
		
		// Message Handlers
		BOOL OnInitDialog(CWnd &Focus, DWORD dwData);

		// Command Handlers
		BOOL OnOkay(UINT uID);

		// Implementation
		void LoadTypeList(void);
	};

// End of File

#endif
