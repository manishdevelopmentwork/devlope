
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Source Editor
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Source Editor Window
//

// Runtime Class

AfxImplementRuntimeClass(CSourceEditorWnd, CCtrlWnd);

// Timer IDs

UINT CSourceEditorWnd::m_timerScroll = CWnd::AllocTimerID();

// Constructor

CSourceEditorWnd::CSourceEditorWnd(UINT uMode)
{
	m_uMode    = uMode;

	m_pScrollC = New CStatic;

	m_pScrollV = New CScrollBar;

	m_pScrollH = New CScrollBar;
	
	m_uDrop    = 0;

	m_nSpace   = 2;

	m_WndOrg   = CPoint(m_nSpace + 1, m_nSpace + 1);

	m_ViewOrg  = CPoint(0, 0);
	
	m_nPos     = 0;
	
	m_nFrom    = 0;

	m_nTo      = 0;

	m_nSize    = 0;

	m_fSelect  = FALSE;

	m_fComment = FALSE;

	m_fModify  = FALSE;

	m_fRead    = FALSE;

	m_fHide    = FALSE;

	m_uCapture = captNone;

	m_uHelp    = NOTHING;

	m_cfCode   = RegisterClipboardFormat(L"C3.1 Code Fragment");

	m_cfFunc   = RegisterClipboardFormat(L"C3.1 Action");

	LoadConfig();

	LoadFonts();
	
	m_Accel.Create(m_uMode ? L"SourceEditorAccel2" : L"SourceEditorAccel1");
	}

// Destructor

CSourceEditorWnd::~CSourceEditorWnd(void)
{
	SaveConfig();
	}

// Operations

void CSourceEditorWnd::Attach(CItem *pItem)
{
	m_pDbase = pItem->GetDatabase();

	m_fRead  = m_pDbase->IsReadOnly();

//	m_fHide = TRUE;
	}

void CSourceEditorWnd::SetText(CString Data)
{
//	m_fHide = TRUE;

	EncodeEOL(Data);

	m_Text     = Data;

	m_fSelect  = FALSE;

	m_fComment = FALSE;

	m_nPos     = 0;

	ReflowAll();
	
	FindLayout();

	SetModify(FALSE);
	
	SyncFrom1D();

	MoveCaret();
	
	Invalidate(FALSE);

	ShowDefaultStatus();
	}

void CSourceEditorWnd::SetModify(BOOL fModify)
{
	if( m_fModify != fModify ) {

		if( (m_fModify = fModify) ) {

			SendNotify(EN_CHANGE);
			}
		}
	}

void CSourceEditorWnd::SetSel(CRange Select)
{
	if( Select.IsEmpty() ) {

		m_nPos = Select.m_nFrom;

		SyncFrom1D();

		MoveCaret();

		Invalidate(FALSE);
		}
	else {
		StartSelect(CPoint(), FALSE);
		
		CPoint Start = CalcFrom1D(Select.m_nFrom);

		CPoint End   = CalcFrom1D(Select.m_nTo);

		StartSelect(Start, TRUE);

		m_Pos      = End;
		
		CheckSelect(End);

		MoveCaret();
		}
	}

// Attributes

CString CSourceEditorWnd::GetText(void)
{
	return m_Text;
	}

BOOL CSourceEditorWnd::GetModify(void)
{
	return m_fModify;
	}

CRange CSourceEditorWnd::GetSel(void)
{
	if( m_fSelect ) {
		
		return m_Select;
		}

	SyncFrom2D();

	return CRange(m_nPos, m_nPos);
	}

// Message Map

AfxMessageMap(CSourceEditorWnd, CCtrlWnd)
{
	AfxDispatchAccelerator()
	
	AfxDispatchMessage(WM_LOADTOOL)
	AfxDispatchMessage(WM_LOADMENU)
	AfxDispatchMessage(WM_POSTCREATE)
	AfxDispatchMessage(WM_SETFOCUS)
	AfxDispatchMessage(WM_KILLFOCUS)
	AfxDispatchMessage(WM_GETDLGCODE)
	AfxDispatchMessage(WM_KEYDOWN)
	AfxDispatchMessage(WM_CHAR)

	AfxDispatchMessage(WM_SIZE)
	AfxDispatchMessage(WM_PAINT)

	AfxDispatchMessage(WM_RBUTTONDOWN)
	AfxDispatchMessage(WM_RBUTTONUP)
	AfxDispatchMessage(WM_LBUTTONDOWN)
	AfxDispatchMessage(WM_LBUTTONUP)
	AfxDispatchMessage(WM_LBUTTONDBLCLK)
	AfxDispatchMessage(WM_MOUSEMOVE)
	AfxDispatchMessage(WM_MOUSEWHEEL)
	AfxDispatchMessage(WM_SETCURSOR)
	AfxDispatchMessage(WM_TIMER)

	AfxDispatchMessage(WM_VSCROLL)
	AfxDispatchMessage(WM_HSCROLL)

	AfxDispatchGetInfoType(IDM_EDIT, OnEditGetInfo)
	AfxDispatchControlType(IDM_EDIT, OnEditControl)
	AfxDispatchCommandType(IDM_EDIT, OnEditCommand)

	AfxDispatchGetInfoType(IDM_VIEW, OnViewGetInfo)
	AfxDispatchControlType(IDM_VIEW, OnViewControl)
	AfxDispatchCommandType(IDM_VIEW, OnViewCommand)

	AfxMessageEnd(CWnd)
	};

// Accelerators

BOOL CSourceEditorWnd::OnAccelerator(MSG &Msg)
{
	return m_Accel.Translate(Msg);
	}

// Message Handlers

void CSourceEditorWnd::OnLoadMenu(UINT uCode, CMenu &Menu)
{
	Menu.MergeMenu(CMenu(L"SourceEditorMenu"));
	}

void CSourceEditorWnd::OnLoadTool(UINT uCode, CMenu &Menu)
{
//	Menu.AppendMenu(CMenu(L"SourceEditorTool"));
	}

void CSourceEditorWnd::OnPostCreate(void)
{
	m_DropHelp.Bind(m_hWnd, this);

	CreateScrollBars();
	
	FindMetrics();	
	
	FindLayout();
	}

void CSourceEditorWnd::OnSetFocus(CWnd &Wnd)
{
	CreateCaret(m_hWnd, NULL, 2, m_FontSize.cy);

	SetCaretPos();

	ShowCaret(m_hWnd);

	if( m_uMode == 0 ) {

		afxMainWnd->SendMessage(WM_UPDATEUI);
		}

	ShowDefaultStatus();

	SendNotify(EN_SETFOCUS);
	}

void CSourceEditorWnd::OnKillFocus(CWnd &Wnd)
{
	HideCaret(m_hWnd);

	DestroyCaret();

	Invalidate(FALSE);

	if( m_uMode == 0 ) {

		afxMainWnd->SendMessage(WM_UPDATEUI);
		}

	ShowDefaultStatus();

	SendNotify(EN_KILLFOCUS);
	}

UINT CSourceEditorWnd::OnGetDlgCode(MSG *pMsg)
{
	UINT uCode = DLGC_WANTCHARS | DLGC_HASSETSEL | DLGC_WANTARROWS;

	if( pMsg ) {

		if( pMsg->message == WM_KEYDOWN || pMsg->message == WM_KEYUP ) {

			if( pMsg->wParam == VK_TAB ) {

				if( !(GetKeyState(VK_CONTROL) & 0x8000) ) {

					uCode |= DLGC_WANTMESSAGE;
					}
				}

			if( pMsg->wParam == VK_RETURN ) {

				if( m_uMode == 0) {

					uCode |= DLGC_WANTMESSAGE;
					}

				if( m_uMode == 2 ) {

					if( !(GetKeyState(VK_CONTROL) & 0x8000) ) {

						uCode |= DLGC_WANTMESSAGE;
						}
					}
				}

			if( pMsg->wParam == VK_ESCAPE ) {

				if( m_uMode == 0 ) {

					uCode |= DLGC_WANTMESSAGE;
					}
				}
			}
		}

	return uCode;
	}

void CSourceEditorWnd::OnKeyDown(UINT uCode, DWORD dwFlags)
{
	if( IsMoveKey(uCode) ) {

		HideCaret(m_hWnd);

		CPoint Pos = m_Pos;

		StartSelect(m_Pos, IsDown(VK_SHIFT));

		if( PerformMove(Pos, uCode) ) {			

			if( m_Pos != Pos ) {

				if( m_Opts.m_fVirtual ) {

					if( m_Pos.y != Pos.y ) {
						
						int nVirtPos = m_CaretPos.x;

						nVirtPos    -= m_ViewOrg.x;

						nVirtPos    -= m_WndOrg.x;

						Pos.x = FindCharFromPos(Pos.y, nVirtPos);
						}
					}
				else {
					CRange Line = m_Lines[Pos.y];

					int   nSize = Line.m_nTo - Line.m_nFrom;

					MakeMin(Pos.x, nSize);
					}

				m_Pos = Pos;

				CheckSelect(m_Pos);

				MoveCaret();

				UpdateWindow();
				}

			JumpIntoView();
			}

		ShowCaret(m_hWnd);

		return;
		}

	switch( uCode ) {
		
		case VK_ESCAPE:

			StartSelect(CPoint(), FALSE);

			break;

		case VK_DELETE:

			if( !m_fRead ) {

				HideCaret(m_hWnd);

				if( DeleteSelect() || DeleteChar() ) {

					UpdateAll();
					}

				ShowCaret(m_hWnd);
				}

			break;

		}
	}

void CSourceEditorWnd::OnChar(UINT uCode, DWORD dwFlags)
{
	if( !m_fRead ) {

		switch( uCode ) {
			
			case 0x08:

				HideCaret(m_hWnd);

				if( DeleteSelect() || Backspace() ) {

					UpdateAll();
					}

				ShowCaret(m_hWnd);

				break;

			case 0x09:
				
				if( IsBlockSelect() ) {

					OnEditIndent(!IsDown(VK_SHIFT));
					}
				else {			
					HideCaret(m_hWnd);

					DeleteSelect();

					if( EnterChar(TCHAR(uCode)) ) {

						UpdateAll();
						}

					ShowCaret(m_hWnd);
					}

				break;

			case 0x0D:

				HideCaret(m_hWnd);

				DeleteSelect();

				if( EnterChar(TCHAR(uCode)) ) {

					UpdateAll();
					}

				ShowCaret(m_hWnd);
				
				break;

			default:

				if( uCode >= 0x20 ) {

					if( UniIsComplex(TCHAR(uCode)) ) {
						
						MessageBeep(0);
						}
					else {
						HideCaret(m_hWnd);

						DeleteSelect();

						if( EnterChar(TCHAR(uCode)) ) {

							UpdateAll();
							}

						ShowCaret(m_hWnd);
						}
					}
				break;

			}
		}
	}

void CSourceEditorWnd::OnSize(UINT uCode, CSize Size)
{
	if( uCode == SIZE_RESTORED || uCode == SIZE_MAXIMIZED ) {

		if( m_FontSize.cx && m_FontSize.cy ) {

			FindLayout();
		
			JumpIntoView();
			}

		MoveCaret();

		Invalidate(FALSE);

		ShowDefaultStatus();
		}
	}

void CSourceEditorWnd::OnPaint(void)
{
	CPaintDC DC(ThisObject);

	CRect Rect = GetClientRect();

	if( !IsEnabled() ) {

		DC.FrameRect(Rect--, afxColor(Disabled));
		}
	else {
		if( m_uDrop ) {

			DC.FrameRect(Rect--, afxBrush(Orange1));
			}
		else
			DC.FrameRect(Rect--, afxColor(Enabled));
		}

	for( int n = 0; n < m_nSpace; n++ ) {

		DC.FrameRect(Rect--, afxBrush(WHITE));
		}

	DC.IntersectClipRect(Rect);

	DC.SetWindowOrg  (-1 * m_WndOrg);

	DC.SetWindowExt  (m_TextExt);
	
	DC.SetViewportOrg(m_ViewOrg);

	DC.SetViewportExt(m_ViewExt);

	CRect Clip = DC.GetClipBox();

	if( m_fHide ) {

		DC.FillRect(Clip, colorEditorBack);

		CPoint Mid = Rect.GetCenter();

		CString Hidden = CString(IDS_PROTECTED_ACCESS);

		CSize Size = DC.GetTextExtent(Hidden);

		int xp = Mid.x - Size.cx / 2;

		int yp = Mid.y - Size.cy / 2;

		DC.TextOut(xp, yp, Hidden);
		}
	else
		DrawText(DC, Clip);
	}

// Initialization

void CSourceEditorWnd::LoadFonts(void)
{
	CClientDC DC(NULL);

	UINT uSize = m_uMode ? 10 : m_Opts.m_uFontSize;

	m_FontCode.Create(DC, L"Courier New", uSize, FALSE);
	}

void CSourceEditorWnd::FindMetrics(void)
{
	CClientDC DC(ThisObject);

	DC.Select(m_FontCode);
	
	m_FontSize = DC.GetTextExtent(L"X");

	DC.Deselect();
	}

void CSourceEditorWnd::FindLayout(void)
{
	CalcTextExt();

	m_fScrollH = FALSE;

	m_fScrollV = FALSE;

	CRect Rect = GetClientRect() - m_nSpace;

	for( UINT n = 0; n < 2; n++ ) {

		if( m_TextExt.cx > Rect.cx() ) {

			if( !m_fScrollH ) {

				m_fScrollH = TRUE;

				if( m_uMode != 1 ) {

					Rect.bottom -= 18;
					}
				}
			}

		if( m_TextExt.cy > Rect.cy() ) {

			if( !m_fScrollV ) {

				m_fScrollV = TRUE;

				if( m_uMode != 1 ) {

					Rect.right -= 18;
					}
				}
			}
		}

	m_ViewExt  = Rect.GetSize();

	PlaceScrollBars();

	if( !m_fScrollH ) {
		
		m_ViewOrg.x = (Rect.cx() - m_ViewExt.cx) / 2;

		m_pScrollH->ShowWindow(SW_HIDE);
		}
	else {
		int nMax = m_TextExt.cx - Rect.cx();
		    
		int nMin = 0;

		MakeMax(m_ViewOrg.x, -nMax);

		MakeMin(m_ViewOrg.x, -nMin);

		m_pScrollH->SetScrollRange(0, m_TextExt.cx, FALSE);

		m_pScrollH->SetPageSize ( (m_ViewExt.cx / m_FontSize.cx) * m_FontSize.cx, FALSE);

		m_pScrollH->SetScrollPos(-m_ViewOrg.x,  FALSE);

		ShowScrollBar(m_pScrollH);
		}

	if( !m_fScrollV ) {
		
		m_ViewOrg.y = (Rect.cy() - m_ViewExt.cy) / 2;

		m_pScrollV->ShowWindow(SW_HIDE);
		}
	else {
		int nMax = m_TextExt.cy - Rect.cy();

		int nMin = 0;

		MakeMax(m_ViewOrg.y, -nMax);

		MakeMin(m_ViewOrg.y, -nMin);

		m_pScrollV->SetScrollRange(0, m_TextExt.cy, FALSE);

		m_pScrollV->SetPageSize ( (m_ViewExt.cy / m_FontSize.cy) * m_FontSize.cy, FALSE);

		m_pScrollV->SetScrollPos(-m_ViewOrg.y,  FALSE);

		ShowScrollBar(m_pScrollV);
		}

	if( m_fScrollH && m_fScrollV ) {

		ShowScrollBar(m_pScrollC);
		}
	else
		m_pScrollC->ShowWindow(SW_HIDE);
	}

void CSourceEditorWnd::LoadConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Source");

	Key.MoveTo(L"Editor");

	m_Opts.m_uFontSize   = Key.GetValue(L"FontSize",   UINT(10));

	m_Opts.m_fSyntax     = Key.GetValue(L"Syntax",     UINT(1));

	m_Opts.m_fVirtual    = Key.GetValue(L"Virtual",    UINT(1));

	m_Opts.m_fUseTabs    = Key.GetValue(L"UseTabs",    UINT(1));

	m_Opts.m_uIndent     = Key.GetValue(L"Indent",     UINT(1));

	m_Opts.m_nTabSize    = Key.GetValue(L"TabSize",    UINT(8));

	m_Opts.m_nIndentSize = Key.GetValue(L"IndentSize", UINT(8));
	}

void CSourceEditorWnd::SaveConfig(void)
{
	CRegKey Key = afxModule->GetApp()->GetUserRegKey();

	Key.MoveTo(L"C3Source");

	Key.MoveTo(L"Editor");

	Key.SetValue(L"FontSize",   m_Opts.m_uFontSize);

	Key.SetValue(L"Syntax",     m_Opts.m_fSyntax);

	Key.SetValue(L"Virtual",    m_Opts.m_fVirtual);

	Key.SetValue(L"UseTabs",    m_Opts.m_fUseTabs);

	Key.SetValue(L"Indent",     m_Opts.m_uIndent);

	Key.SetValue(L"TabSize",    m_Opts.m_nTabSize);

	Key.SetValue(L"IndentSize", m_Opts.m_nIndentSize);
	}

// Implementation

void CSourceEditorWnd::ReflowFromHere(void)
{
	ReflowAll();
	}

void CSourceEditorWnd::ReflowAll(void)
{
	m_Lines.Empty();

	m_Inits.Empty();

	m_Sizes.Empty();

	int  yInit = 0;

	UINT p0    = 0;
	
	UINT p1;

	PCTXT pText = PCTXT(m_Text);

	while( (p1 = m_Text.Find(EOL, p0)) < NOTHING ) {

		m_Lines.Append(CRange(p0, p1));

		m_Inits.Append(CPoint(0, yInit));

		m_Sizes.Append(GetTabbedTextExtent(pText, p1 - p0));

		yInit += m_FontSize.cy;

		pText += (p1 + 1 - p0);

		p0 = p1 + 1;
		}
		
	m_Lines.Append(CRange(p0, m_Text.GetLength()));

	m_Inits.Append(CPoint(0, yInit));

	m_Sizes.Append(GetTabbedTextExtent(pText, m_Text.GetLength() - p0));

	m_nLines = m_Lines.GetCount();
	}

void CSourceEditorWnd::CalcTextExt(void)
{
	// LATER -- Can't this be part of the reflow?

	m_TextExt.cx = 0;
	
	m_TextExt.cy = 0;
	
	for( int r = 0; r < m_nLines; r ++ ) {
		
		CSize  Size  = m_Sizes[r];
		
		m_TextExt.cy += Size.cy;

		MakeMax(m_TextExt.cx, Size.cx);
		}

	// NOTE -- Room for caret!

	m_TextExt.cx += 2;
	}

void CSourceEditorWnd::UpdateAll(void)
{
	SetModify(TRUE);
	
	FindLayout();

	MoveCaret();

	JumpIntoView();
	
	Invalidate(FALSE);
	}

void CSourceEditorWnd::GetLineInfo(void)
{
	GetLineInfo(m_Pos.y);
	}

void CSourceEditorWnd::GetLineInfo(int nLine)
{
	CRange const &Range = m_Lines[nLine];

	m_nFrom = Range.m_nFrom;

	m_nTo   = Range.m_nTo;

	m_nSize = Range.m_nTo - Range.m_nFrom;
	}

void CSourceEditorWnd::SyncFrom2D(void)
{
	CPoint Pos = m_Pos;

	GetLineInfo();

	MakeMin(m_Pos.x, m_nSize);

	MakeMax(m_Pos.x, 0);

	m_nPos = m_nFrom + m_Pos.x;
	}

void CSourceEditorWnd::SyncFrom1D(void)
{
	if( m_Pos.y > m_nLines - 1 ) {

		m_Pos.y = m_nLines - 1;
		}

	GetLineInfo();

	if( m_nPos < m_nFrom ) {

		while( m_nPos < m_nFrom ) {

			GetLineInfo(--m_Pos.y);
			}
		}
	else {
		while( m_Pos.y + 1 < m_nLines ) {

			int nNext = m_Lines[m_Pos.y + 1].m_nFrom;

			if( m_nPos < nNext ) {

				break;
				}

			GetLineInfo(++m_Pos.y);
			}
		}

	m_Pos.x = m_nPos - m_nFrom;

	MakeMin(m_Pos.x, m_nSize);

	MakeMax(m_Pos.x, 0);
	}

CPoint CSourceEditorWnd::CalcFrom1D(int nPos)
{
	CPoint Pos;

	for( ; Pos.y < m_nLines; Pos.y ++ ) {
		
		CRange Line = m_Lines[Pos.y];

		int   nFrom = Line.m_nFrom;

		int     nTo = Line.m_nTo;

		if( nPos >= nFrom && nPos <= nTo ) {

			Pos.x = nPos - nFrom;
			
			break;
			}
		}

	return Pos;
	}

int CSourceEditorWnd::CalcFrom2D(CPoint Pos)
{
	if( Pos.y < m_nLines ) {

		int nPos = 0;

		CRange Line = m_Lines[Pos.y];

		int   nFrom = Line.m_nFrom;

		int     nTo = Line.m_nTo;

		int nSize = nTo - nFrom;

		MakeMin(Pos.x, nSize);

		nPos = Line.m_nFrom + Pos.x;

		return nPos;
		}

	return m_Text.GetLength();
	}

void CSourceEditorWnd::SyncRange(void)
{
	m_Lines.SetAt(m_Pos.y, CRange(m_nFrom, m_nTo));

	SyncSize(m_Pos.y);
	}

void CSourceEditorWnd::SyncRange(int nLine)
{
	m_Lines.SetAt(nLine, CRange(m_nFrom, m_nTo));

	SyncSize(nLine);
	}

void CSourceEditorWnd::SyncSize(int nLine)
{
	CRange Range = m_Lines[nLine];

	int nFrom = Range.m_nFrom;

	int   nTo = Range.m_nTo;

	int nSize = nTo - nFrom;

	PCTXT pText = PCTXT(m_Text) + nFrom;
	
	m_Sizes.SetAt(nLine, GetTabbedTextExtent(pText, nSize));
	}

void CSourceEditorWnd::AdjustFromHere(int nDelta)
{
	for( int n = m_Pos.y + 1; n < m_nLines; n++ ) {

		CRange Range = m_Lines[n];

		Range.m_nFrom += nDelta;

		Range.m_nTo   += nDelta;

		m_Lines.SetAt(n, Range);
		}
	}

void CSourceEditorWnd::AdjustFromHere(int nLine, int nDelta)
{
	for( int n = nLine + 1; n < m_nLines; n++ ) {

		CRange Range = m_Lines[n];

		Range.m_nFrom += nDelta;

		Range.m_nTo   += nDelta;

		m_Lines.SetAt(n, Range);
		}
	}

void CSourceEditorWnd::MoveCaret(void)
{
	SetCaretPos();

	if( FALSE ) {

		// LATER -- Invalidate only the caret line, and only
		// if we've got a different background color for the
		// line that currently contains the caret.

		CRect Rect = GetClientRect() - 1;

		Rect.right -= 18;

		Invalidate(Rect, FALSE);
		}

	ShowDefaultStatus();
	}

void CSourceEditorWnd::SetCaretPos(void)
{
	m_CaretPos.x = FindPosFromChar(m_Pos.y, m_Pos.x);
	
	m_CaretPos.y = m_Inits[m_Pos.y].y;

	m_CaretPos  += m_ViewOrg;

	m_CaretPos  += m_WndOrg;

	::SetCaretPos(m_CaretPos.x, m_CaretPos.y);
	}

BOOL CSourceEditorWnd::IsDown(UINT uCode)
{
	return (GetKeyState(uCode) & 0x8000) ? TRUE : FALSE;
	}

int CSourceEditorWnd::FindCharFromPos(int nLine, int xPos)
{
	xPos -= m_Inits[nLine].x;

	CRange Line = m_Lines[nLine];

	CSize  Size = m_Sizes[nLine];

	if( xPos < Size.cx ) {

		int nChar = 0;

		PCTXT pText = PCTXT(m_Text) + Line.m_nFrom;		

		while( GetTabbedTextExtent(pText, nChar + 1).cx <= xPos ) {
			
			nChar++;
			}

		return nChar;
		}
	else {
		int  nChar = 0;
			
		nChar     += Line.m_nTo - Line.m_nFrom;

		nChar     += ((xPos - Size.cx) / m_FontSize.cx);
		
		return nChar;
		}
	}

int CSourceEditorWnd::FindPosFromChar(int nLine, int nChar)
{
	CRange Line = m_Lines[nLine];

	int   nFrom = Line.m_nFrom;

	int     nTo = Line.m_nTo;

	int   nSize = nTo - nFrom;

	if( nChar < nSize ) {

		int xPos = m_Inits[nLine].x;

		PCTXT pText = PCTXT(m_Text) + nFrom;

		xPos += GetTabbedTextExtent(pText, nChar).cx;

		return xPos;
		}
	else {
		int xPos = m_Inits[nLine].x;
		
		xPos += m_Sizes[nLine].cx;
		
		xPos += (nChar - nSize) * m_FontSize.cx;
		
		return xPos;
		}
	}

void CSourceEditorWnd::DecodeEOL(CString &Text)
{
	UINT p1;

	UINT p2 = 0;

	while( (p1 = Text.Find(EOL, p2)) < NOTHING ) {

		Text.Delete(p1, 1);
		
		Text.Insert(p1, L"\r\n");

		p2 = p1 + wstrlen(L"\r\n");
		}
	}

void CSourceEditorWnd::EncodeEOL(CString &Text)
{
	UINT p1;

	UINT p2 = 0;

	while( (p1 = Text.Find(L"\r\n", p2)) < NOTHING ) {

		Text.Delete(p1, wstrlen(L"\r\n"));
		
		Text.Insert(p1, EOL);

		p2 = p1 + 1;
		}
	}

// Status Display

void CSourceEditorWnd::ShowDefaultStatus(void)
{
	CString Text;

	if( HasFocus() ) {

		Text += CPrintf( L"Line %d, Char %d",
				 m_Pos.y + 1,
				 m_Pos.x + 1
				 );

		if( afxThread ) {

			afxThread->SetStatusText(Text);
			}
		}
	}

// Editing

BOOL CSourceEditorWnd::Indent(void)
{
	BOOL fChange = FALSE;

	SelectSyncFrom1D();
	
	for( int r = m_SelectStart.y; r <= m_SelectEnd.y; r ++ ) {
		
		GetLineInfo(r);

		if( m_Select.m_nTo > m_nFrom ) {

			m_Text.Insert(m_nFrom, '\t');

			m_nSize += 1;
			
			m_nTo   += 1;
			
			SyncRange(r);

			AdjustFromHere(r, +1);

			m_Select.m_nTo += 1;

			SelectSyncFrom1D();			
			
			fChange = TRUE;
			}
		}

	return fChange;
	}

BOOL CSourceEditorWnd::Outdent(void)
{
	BOOL fChange = FALSE;

	SelectSyncFrom1D();
	
	for( int r = m_SelectStart.y; r <= m_SelectEnd.y; r ++ ) {
		
		GetLineInfo(r);

		if( m_Select.m_nTo > m_nFrom ) {

			PCTXT pText = PCTXT(m_Text);

			int n;

			for( n = m_nFrom; n < m_nTo; n ++ ) {

				TCHAR cData = pText[n];

				if( !wisspace(cData) ) {

					break;
					}

				if( cData == '\t' ) {
					
					n++;
					
					break;
					}

				if( n > m_nFrom && !((n - m_nFrom) % 8) ) {
					
					break;
					}
				}

			int nLen = n - m_nFrom;

			if( nLen > 0 ) {

				m_Text.Delete(m_nFrom, nLen);

				m_nSize -= nLen;

				m_nTo   -= nLen;

				SyncRange(r);

				AdjustFromHere(r, -nLen);
				
				m_Select.m_nTo -= nLen;

				SelectSyncFrom1D();
				
				fChange = TRUE;
				}
			}
		}

	return fChange;
	}

BOOL CSourceEditorWnd::EnterText(PCTXT pText)
{
	SyncFrom2D();

	int nSize = wstrlen(pText);
	
	m_Text.Insert(m_nPos, pText);

	m_nPos  += nSize;

	m_nTo   += nSize;

	m_nSize += nSize;

	if( CString(pText).Find(EOL) < NOTHING ) {

		ReflowFromHere();
		}
	else {
		SyncRange();

		AdjustFromHere(+nSize);
		}

	SyncFrom1D();

	return TRUE;
	}

BOOL CSourceEditorWnd::EnterChar(TCHAR cData)
{
	if( m_Opts.m_fVirtual ) {

		CRange Line = m_Lines[m_Pos.y];

		int   nFrom = Line.m_nFrom;

		int     nTo = Line.m_nTo;

		int   nSize = nTo - nFrom;

		int nInsert = m_Pos.x - nSize;

		if( nInsert > 0 ) {

			if( m_Opts.m_fUseTabs ) {

				int     nPos = FindPosFromChar(m_Pos.y, m_Pos.x);

				CString Text = m_Text.Mid(nFrom, nSize);
				
				while( GetTabbedTextExtent(CPrintf(L"%s\t", Text)).cx <= nPos ) {
					
					Text += '\t';
					}
				
				while( GetTabbedTextExtent(CPrintf(L"%s ", Text)).cx <= nPos ) {
					
					Text += ' ';
					}
				
				Text = Text.Mid(nSize);
				
				Text += cData;
				
				return EnterText(Text);
				}
			else {
				CString Text = CString(' ', nInsert);

				Text += cData;

				return EnterText(Text);
				}
			}
		}
	
	SyncFrom2D();

	switch( m_Opts.m_uIndent ) {
		
		case 1:

			// NOTE -- This provides basic auto-indent by adding
			// whatever whitespace was at the start of the last
			// line after we enter the EOL character.

			if( cData == EOL ) {

				PCTXT pText = PCTXT(m_Text);

				int n;

				for( n = m_nFrom; n < m_nTo; n++ ) {

					if( !wisspace(pText[n]) ) {

						break;
						}
					}

				CString Text;
				
				Text += cData;
				
				Text += CString(pText + m_nFrom, n - m_nFrom);

				return EnterText(Text);
				}
			break;
		
		case 2:
			// LATER -- Implement context sensitive auto-indent

			break;
		}

	m_Text.Insert(m_nPos, cData);

	m_nPos  += 1;

	m_nTo   += 1;

	m_nSize += 1;

	if( cData == EOL ) {
		
		ReflowAll();
		}
	else {
		SyncRange();

		AdjustFromHere(+1);
		}

	SyncFrom1D();

	return TRUE;
	}

BOOL CSourceEditorWnd::DeleteSelect(void)
{
	if( m_fSelect && m_Select.m_nFrom != m_Select.m_nTo ) {
			
		m_nPos = m_Select.m_nFrom;

		SyncFrom1D();

		SyncFrom2D();

		int nFrom = m_Select.m_nFrom;

		int   nTo = m_Select.m_nTo;

		int nSize = nTo - nFrom;

		BOOL fEOL = m_Text.Mid(nFrom, nSize).Find(EOL) < NOTHING;

		m_Text.Delete(nFrom, nSize);

		m_nTo   -= nSize;

		m_nSize -= nSize;

		if( fEOL ) {
			
			ReflowFromHere();
			}
		else {
			SyncRange();

			AdjustFromHere(-nSize);
			}

		SyncFrom1D();

		m_fSelect = FALSE;
		
		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::DeleteChar(void)
{
	SyncFrom2D();

	if( m_nPos < int(m_Text.GetLength()) ) {

		BOOL fEOL = m_Text[m_nPos] == EOL;
		
		m_Text.Delete(m_nPos, 1);

		if( fEOL ) {

			ReflowAll();
			}
		else {
			m_nSize -= 1;

			m_nTo   -= 1;

			SyncRange();

			AdjustFromHere(-1);
			}

		SyncFrom1D();

		return TRUE;
		}

	return FALSE;
	}

BOOL CSourceEditorWnd::Backspace(void)
{
	SyncFrom2D();

	if( m_nPos ) {

		m_Text.Delete(m_nPos - 1, 1);

		m_nPos -= 1;

		if( !m_Pos.x ) {

			ReflowAll();
			}
		else {
			m_nTo   -= 1;

			m_nSize -= 1;

			SyncRange();

			AdjustFromHere(-1);
			}

		SyncFrom1D();

		return TRUE;
		}

	return FALSE;
	}

// End of File
