
#include "intern.hpp"

#include "lang.hpp"

#include <limits.h>

#include <float.h>

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Communications Subsystem
//
// Copyright (c) 1993-2016 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Time and Date Display Format
//

// Time Fields

enum
{
	fieldSec   = 0xFF00,
	fieldMin   = 0xFF01,
	fieldHE1   = 0xFF02,
	fieldHE2   = 0xFF03,
	fieldHE3   = 0xFF04,
	fieldHE4   = 0xFF05,
	fieldH12   = 0xFF06,
	fieldH24   = 0xFF07,
	fieldAMPM  = 0xFF08,
	fieldDate  = 0xFF09,
	fieldMonth = 0xFF0A,
	fieldYear  = 0xFF0B,
	};

// Constructor

CDispFormatTimeDate::CDispFormatTimeDate(void)
{
	m_Mode     = 0;

	m_Secs     = 0;

	m_TimeForm = 0;

	m_TimeSep  = 0;

	m_pAM      = NULL;

	m_pPM      = NULL;

	m_DateForm = 0;

	m_DateSep  = 0;
	
	m_Year     = 0;
	
	m_Month    = 0;

	m_uCount   = 0;

	m_uFields  = 0;

	m_uTotal   = 0;

	m_uLang    = NOTHING;
	}

// Destructor

CDispFormatTimeDate::~CDispFormatTimeDate(void)
{
	delete m_pAM;

	delete m_pPM;
	}

// Initialization

void CDispFormatTimeDate::Load(PCBYTE &pData)
{
	ValidateLoad("CDispFormatTimeDate", pData);

	m_Mode     = GetByte(pData);
	m_Secs     = GetByte(pData);
	m_TimeForm = GetByte(pData);
	m_TimeSep  = GetByte(pData);
	m_DateForm = GetByte(pData);
	m_DateSep  = GetByte(pData);
	m_Year     = GetByte(pData);
	m_Month    = GetByte(pData);

	GetCoded(pData, m_pAM);
	
	GetCoded(pData, m_pPM);

	BuildFormat();
	}

// Scan Control

BOOL CDispFormatTimeDate::IsAvail(void)
{
	if( !IsItemAvail(m_pAM) ) {

		return FALSE;
		}

	if( !IsItemAvail(m_pPM) ) {

		return FALSE;
		}

	return TRUE;
	}

BOOL CDispFormatTimeDate::SetScan(UINT Code)
{
	SetItemScan(m_pAM, Code);

	SetItemScan(m_pPM, Code);

	return TRUE;
	}

// Formatting

CUnicode CDispFormatTimeDate::Format(DWORD Data, UINT Type, UINT Flags)
{
	CheckLanguage();

	if( Flags & (fmtTime | fmtDate) ) {

		m_Mode     = (Flags & fmtTime) ? 0 : 1;

		m_uCount   = 0;

		m_uFields  = 0;

		m_uTotal   = 0;
		}

	if( !m_uFields ) {

		BuildFormat();
		}

	if( Flags & fmtUnits ) {

		return L"";
		}

	if( Type == typeInteger ) {

		CUnicode t;

		for( UINT n = 0; n < m_uCount; n++ ) {

			UINT f = LOWORD(m_Format[n]);
			
			UINT l = HIWORD(m_Format[n]);

			UINT h = 0;

			if( HIBYTE(f) == 0xFF ) {

				if( f == fieldAMPM ) {

					t += GetAMPM(::GetHour(Data));
					}
				else {
					char s[32] = {0};

					switch( f ) {

						case fieldSec:
							SPrintf(s, "%2.2u", ::GetSec(Data));
							break;

						case fieldMin:
							SPrintf(s, "%2.2u", ::GetMin(Data));
							break;

						case fieldHE1:
							SPrintf(s, "%1.1u", ::GetHours(Data) % 10);
							break;

						case fieldHE2:
							SPrintf(s, "%2.2u", ::GetHours(Data) % 100);
							break;

						case fieldHE3:
							SPrintf(s, "%3.3u", ::GetHours(Data) % 1000);
							break;

						case fieldHE4:
							SPrintf(s, "%4.4u", ::GetHours(Data) % 10000);
							break;

						case fieldH24:
							SPrintf(s, "%2.2u", ::GetHour(Data));
							break;

						case fieldH12:
							SPrintf(s, "%2.2u", (h = ::GetHour(Data) % 12) ? h : 12);
							break;

						case fieldDate:
							SPrintf(s, "%2.2u", ::GetDate(Data));
							break;
						}

					if( f == fieldMonth ) {
							
						if( l == 3 ) {

							CUnicode m = GetMonthName(::GetMonth(Data));

							t += m;
							}
						else
							SPrintf(s, "%2.2u", ::GetMonth(Data));
						}

					if( f == fieldYear ) {
							
						if( l == 2 ) {

							SPrintf(s, "%2.2u", ::GetYear(Data) % 100);
							}
						else
							SPrintf(s, "%4.4u", ::GetYear(Data));
						}

					if( s[0] ) {

						t += s;
						}
					}
				}
			else
				t += WCHAR(f);
			}

		if( Flags & fmtPad ) {

			if( !(Flags & fmtANSI) ) {

				MakeDigitsFixed(t);
				}
			}

		return t;
		}

	if( Type == typeReal ) {
		
		return Format(DWORD(I2R(Data)), typeInteger, Flags);
		}

	return CDispFormat::Format(Data, Type, Flags);
	}

// Parsing

BOOL CDispFormatTimeDate::Parse(DWORD &Data, CString Text, UINT Type)
{
	if( Type == typeInteger ) {	

		CTime Time;	 

		CString EditString = Text;

		if( m_Mode == 1 || m_Mode == 3) {

			LoadDate(Time, EditString);

			LoadTime(Time, EditString);

			Data = Translate(Time);

			return TRUE;
			}

		else if( m_Mode == 0 || m_Mode == 2 ) {

			LoadTime(Time, EditString);

			LoadDate(Time, EditString);

			Data = Translate(Time);

			return TRUE;
			}
	
		else if( m_Mode >= 4 ) {

			LoadTime(Time, EditString );

			Data = Translate(Time);

			return TRUE;
			}
		}
	
	return GeneralParse(Data, Text, Type);
	}

void CDispFormatTimeDate::LoadDate(CTime &Time, CString &EditString) {

	CStringArray Array;

	char cSep  = char(m_DateSep ? m_DateSep : '-');

	EditString.Tokenize(Array, cSep);

	Time.uDate  = 0;

	Time.uMonth = 0;

	Time.uYear  = 0;

	UINT uForm  = m_DateForm;

	switch( uForm ) {
		
		case 0:
		case 1:
			Time.uMonth = (strtol(Array[0], NULL, 10));

			Time.uDate  = (strtol(Array[1], NULL, 10));

			if( m_Year != 2 ) {
				
				Time.uYear  = (strtol(Array[2], NULL, 10));
				}
			break;
		
		case 2:
			Time.uDate  = (strtol(Array[0], NULL, 10));

			Time.uMonth = (strtol(Array[1], NULL, 10));
			
			if( m_Year != 2 ) {
				
				Time.uYear  = (strtol(Array[2], NULL, 10));
				}
			break;
		
		case 3:
			if( m_Year != 2 ) {
				
				Time.uYear  = (strtol(Array[0], NULL, 10));

				Time.uMonth = (strtol(Array[1], NULL, 10));

				Time.uDate  = (strtol(Array[2], NULL, 10));
				}
			else {
				Time.uMonth = (strtol(Array[0], NULL, 10));

				Time.uDate  = (strtol(Array[1], NULL, 10));
				}
			break;
		}
	
	BOOL fIsFirst = FALSE;

	if( m_Mode == 1 || m_Mode == 3) {

		fIsFirst = TRUE;
		}

	if( m_Month && Time.uMonth == 0 ) {

		for( UINT i = 1; i < 13; i++ ) {

			CUnicode t = GetMonthName(i); 

			INT uNameDate = EditString.Find(PCTXT(UniConvert(t)));

			if( uNameDate >= 0 ) {
				
				Time.uMonth = i;

				break;
				}
			}
		}

	if( fIsFirst ) {
		
		if( cSep == 0x20 ) {

			UINT uCount = Array.GetCount() - 2;

			for( UINT i = 0; i < uCount; i++ ) {

				UINT uPos = EditString.Find(PCTXT(Array[i]));

				EditString.Delete(uPos, strlen(Array[i]));
	
				EditString.TrimLeft();
				}
			}
		else {
			CString TimeString;    

			TimeString =  EditString.Left(EditString.Find(' '));

			EditString.Delete(0, TimeString.GetLength());
			}
		}
	}

void CDispFormatTimeDate::LoadTime(CTime &Time, CString &EditString) {

	CStringArray Array;

	Time.uHours    = 0;

	Time.uMinutes  = 0;

	Time.uSeconds  = 0;

	EditString.MakeUpper();

	char cSep  = char(m_TimeSep ? m_TimeSep : ':');

	EditString.Tokenize(Array, cSep);

	Time.uHours    = strtol(Array[0], NULL, 10);

	Time.uMinutes  = strtol(Array[1], NULL, 10);

	if( m_Secs ) {
		
		Time.uSeconds = strtol(Array[2], NULL, 10);
		}

	if( m_TimeForm != 1 ) {

		INT uPM = 0;
		
		if( m_TimeForm == 2 ) {
			
			CString UpperPM = UniConvert(m_pPM->GetText());

			UpperPM.MakeUpper();

			uPM = EditString.Find(UpperPM);
			}
		else {
			uPM = EditString.Find(PCTXT("PM"));
			}
			
		if( uPM >= 0 ) {

			if( Time.uHours < 12 ) {
				
				Time.uHours += 12;
				}
			
			else if( Time.uHours == 12 ) {

				Time.uHours = 12;
				}
			}
		
			else if( Time.uHours == 12 ) {

				Time.uHours = 0;
				}
			}

	BOOL fIsFirst = FALSE;

	if( m_Mode == 0 || m_Mode == 2 ) {

		fIsFirst = TRUE;
		}

	if( fIsFirst ) {
		
		if( cSep == 0x20 ) {

			UINT uCount = Array.GetCount() - 1;
		
			for( UINT i = 0; i < uCount; i++ ) {

				UINT uPos = EditString.Find(PCTXT(Array[i]));

				EditString.Delete(uPos, strlen(Array[i]));
	
				EditString.TrimLeft();
				}
			}
		else {
			CString TimeString;

			TimeString = EditString.Left(EditString.Find(' '));

			EditString.Delete(0, TimeString.GetLength());
			}
		
		if( m_TimeForm == 2 ) {

			EditString.Replace(PCTXT(UniConvert(m_pAM->GetText())), "");
		
			EditString.Replace(PCTXT(UniConvert(m_pPM->GetText())), "");
			}
		else {
			EditString.Replace(PCTXT("AM"), "");

			EditString.Replace(PCTXT("PM"), "");
			}

		EditString.TrimLeft();
		}
	}

// Editing

UINT CDispFormatTimeDate::Edit(CEditCtx *pEdit, UINT uCode)
{
	if( uCode == 0x00 ) {

		pEdit->m_uKeypad  = keypadNudge3;

		pEdit->m_uField   = 0;
		
		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		return editUpdate;
		}

	if( uCode == '<' ) {

		if( pEdit->m_fDefault ) {

			LastField(pEdit);

			return editUpdate;
			}

		if( pEdit->m_uField ) {

			pEdit->m_uField  = pEdit->m_uField - 1;

			pEdit->m_uCursor = FindCursor(pEdit->m_uField);

			return editUpdate;
			}

		return editNone;
		}

	if( uCode == '>' ) {

		if( pEdit->m_fDefault ) {

			FirstField(pEdit);

			return editUpdate;
			}

		if( pEdit->m_uField < m_uFields - 1 ) {

			pEdit->m_uField  = pEdit->m_uField + 1;

			pEdit->m_uCursor = FindCursor(pEdit->m_uField);

			return editUpdate;
			}

		return editNone;
		}

	if( uCode == 0x0B ) {

		if( pEdit->m_fDefault ) {

			FirstField(pEdit);
			}

		return SpinUp(pEdit);
		}

	if( uCode == 0x0A ) {

		if( pEdit->m_fDefault ) {

			FirstField(pEdit);
			}

		return SpinDown(pEdit);
		}

	if( uCode == 0x0D ) {

		if( pEdit->m_fDefault ) {

			FirstField(pEdit);

			return editUpdate;
			}

		if( pEdit->m_pValue->SetValue(pEdit->m_Data, typeInteger, setNone) ) {

			return editCommit;
			}

		return editUpdate;
		}

	if( uCode == 0x1B ) {

		if( pEdit->m_fDefault ) {

			return editAbort;
			}

		pEdit->m_uCursor  = cursorAll;

		pEdit->m_fDefault = TRUE;

		return editUpdate;
		}

	if( uCode == 0x7F ) {

		return editNone;
		}

	return editNone;
	}

// Limit Access

DWORD CDispFormatTimeDate::GetMin(UINT Type)
{
	if( Type == typeInteger ) {
		
		return DWORD(INT_MIN);
		}

	if( Type == typeReal ) {
		
		return R2I(-FLT_MAX);
		}
	
	return CDispFormat::GetMin(Type);
	}

DWORD CDispFormatTimeDate::GetMax(UINT Type)
{
	if( Type == typeInteger ) {
		
		return DWORD(INT_MAX);
		}

	if( Type == typeReal ) {
		
		return R2I(+FLT_MAX);
		}
	
	return CDispFormat::GetMax(Type);
	}

// Implementation

void CDispFormatTimeDate::BuildFormat(void)
{
	switch( m_Mode ) {

		case 0:
		case 4:
		case 5:
		case 6:
		case 7:
			BuildTime();
			
			break;

		case 1:
			BuildDate();
			
			break;

		case 2:
			BuildTime();
			
			AddForm(' ', 1);
			
			BuildDate();
			
			break;

		case 3:
			BuildDate();
			
			AddForm(' ', 1);
			
			BuildTime();
			
			break;
		}
	}

void CDispFormatTimeDate::BuildTime(void)
{
	char cSep  = char(m_TimeSep ? m_TimeSep : ':');

	UINT uForm = m_TimeForm;

	if( m_Mode >= 4 ) {

		UINT uCode = (m_Mode - 4) + fieldHE1;

		UINT uLen  = (m_Mode - 3);

		AddForm(uCode, uLen);
		
		AddForm(cSep,     1);
		
		AddForm(fieldMin, 2);
		}
	else {
		if( uForm == 0) {

			uForm = GetLocaleTime();
			}

		if( uForm == 2 ) {

			AddForm(fieldH12, 2);
			
			AddForm(cSep,     1);
			
			AddForm(fieldMin, 2);
			}
		else {
			AddForm(fieldH24, 2);
			
			AddForm(cSep,     1);
			
			AddForm(fieldMin, 2);
			}
		}

	if( m_Secs ) {

		AddForm(cSep,     1);

		AddForm(fieldSec, 2);
		}

	if( uForm == 2 && m_Mode <= 3 ) {

		UINT m1 = wstrlen(GetAMPM(0 ));
		
		UINT m2 = wstrlen(GetAMPM(12));

		AddForm(fieldAMPM, max(m1, m2));
		}
	}

void CDispFormatTimeDate::BuildDate(void)
{
	UINT uForm = m_DateForm;

	if( !uForm ) {

		uForm = GetLocaleDate();
		}

	switch( uForm ) {

		case 1:
			if( m_Year == 2 ) {

				AddMonth();
				
				AddDateSep();
				
				AddDate();
				}
			else {
				AddMonth();
				
				AddDateSep();
				
				AddDate();
				
				AddDateSep();
				
				AddYear();
				}
			break;

		case 2:
			if( m_Year == 2 ) {

				AddDate();
				
				AddDateSep();
				
				AddMonth();
				}
			else {
				AddDate();
				
				AddDateSep();
				
				AddMonth();
				
				AddDateSep();
				
				AddYear();
				}
			break;

		case 3:
			if( m_Year == 2 ) {

				AddMonth();
				
				AddDateSep();
				
				AddDate();
				}
			else {
				AddYear();
				
				AddDateSep();
				
				AddMonth();
				
				AddDateSep();
				
				AddDate();
				}
			break;
		}
	}

void CDispFormatTimeDate::AddDate(void)
{
	AddForm(fieldDate, 2);
	}

void CDispFormatTimeDate::AddMonth(void)
{
	AddForm(fieldMonth, m_Month ? 3 : 2);
	}

void CDispFormatTimeDate::AddYear(void)
{
	AddForm(fieldYear, m_Year ? 4 : 2);
	}

void CDispFormatTimeDate::AddDateSep(void)
{
	if( !m_DateSep ) {

		WCHAR cSep = CCommsSystem::m_pThis->m_pLang->GetDateSepChar();

		AddForm(cSep, 1);

		return;
		}

	AddForm(m_DateSep, 1);
	}

void CDispFormatTimeDate::AddForm(UINT uCode, UINT uLen)
{
	m_Format[m_uCount++] = MAKELONG(uCode, uLen);

	if( uCode & 0x8000 ) m_uFields++;

	m_uTotal += uLen;
	}

// Localization

BOOL CDispFormatTimeDate::CheckLanguage(void)
{
	UINT uLang = CCommsSystem::m_pThis->m_pLang->GetEffectiveCode();

	if( m_uLang != uLang ) {

		m_uCount   = 0;

		m_uFields  = 0;

		m_uTotal   = 0;

		m_uLang    = uLang;

		return TRUE;
		}

	return FALSE;
	}

UINT CDispFormatTimeDate::GetLocaleTime(void)
{
	return CCommsSystem::m_pThis->m_pLang->GetTimeFormat();
	}

UINT CDispFormatTimeDate::GetLocaleDate(void)
{
	return CCommsSystem::m_pThis->m_pLang->GetDateFormat();
	}

CUnicode CDispFormatTimeDate::GetMonthName(UINT uMonth)
{
	return CCommsSystem::m_pThis->m_pLang->GetMonthName(uMonth);
	}

CUnicode CDispFormatTimeDate::GetAMPM(UINT uHour)
{
	// REV3 -- Get this from the language manager.

	if( uHour >= 12 ) {

		return m_pPM->GetText(L" PM");
		}
		
	return m_pAM->GetText(L" AM");
	}

// Editing Support

UINT CDispFormatTimeDate::FindCursor(UINT uField)
{
	UINT uPos = 0;

	for( UINT n = 0; n < m_uCount; n++ ) {

		if( HIBYTE(m_Format[n]) == 0xFF ) {

			if( !uField-- ) {

				uPos += HIWORD(m_Format[n]) - 1;

				return uPos;
				}
			}

		uPos += HIWORD(m_Format[n]);
		}

	return 0;
	}

void CDispFormatTimeDate::FirstField(CEditCtx *pEdit)
{
	pEdit->m_fDefault = FALSE;

	pEdit->m_Edit     = Format(pEdit->m_Data, typeInteger, fmtBare | fmtPad);

	pEdit->m_uField   = 0;

	pEdit->m_uCursor  = FindCursor(pEdit->m_uField);
	}

void CDispFormatTimeDate::LastField(CEditCtx *pEdit)
{
	pEdit->m_fDefault = FALSE;

	pEdit->m_Edit     = Format(pEdit->m_Data, typeInteger, fmtBare | fmtPad);

	pEdit->m_uField   = m_uFields - 1;

	pEdit->m_uCursor  = FindCursor(pEdit->m_uField);
	}

UINT CDispFormatTimeDate::SpinUp(CEditCtx *pEdit)
{
	CTime Time;

	Translate(Time, pEdit->m_Data);

	UINT uField = pEdit->m_uField;

	UINT n;

	for( n = 0; n < m_uCount; n++ ) {

		if( HIBYTE(m_Format[n]) == 0xFF ) {

			if( !uField-- ) {

				break;
				}
			}
		}

	switch( LOWORD(m_Format[n]) ) {

		case fieldSec:

			if( Time.uSeconds < 59 ) {
				
				Time.uSeconds++;
				}
			break;

		case fieldMin:
			
			if( Time.uMinutes < 59 ) {
				
				Time.uMinutes++;
				}
			break;

		case fieldHE1:

			if( Time.uHours < 9 ) {
				
				Time.uHours++;
				}
			break;

		case fieldHE2:

			if( Time.uHours < 99 ) {
				
				Time.uHours++;
				}
			break;

		case fieldHE3:
			
			if( Time.uHours < 999 ) {
				
				Time.uHours++;
				}
			break;

		case fieldHE4:
			
			if( Time.uHours < 9999 ) {
				
				Time.uHours++;
				}
			break;

		case fieldH24:
			
			if( Time.uHours < 23 ) {
				
				Time.uHours++;
				}
			break;

		case fieldH12:
			
			if( Time.uHours % 12 < 11 ) {
				
				Time.uHours++;
				}
			break;

		case fieldAMPM:
			
			if( Time.uHours < 12 ) {
				
				Time.uHours += 12;
				}
			break;

		case fieldDate:
			
			if( Time.uDate < GetMonthDays(Time.uYear, Time.uMonth) ) {
				
				Time.uDate++;
				}
			break;

		case fieldMonth:
			
			if( Time.uMonth < 12 ) {
				
				Time.uMonth++;

				CheckMonth(Time);
				}
			break;

		case fieldYear:
			
			if( Time.uYear < 2050 ) {
				
				Time.uYear++;

				CheckMonth(Time);
				}
			break;
		}

	pEdit->m_Data = Translate(Time);

	pEdit->m_Edit = Format(pEdit->m_Data, typeInteger, fmtBare | fmtPad);

	return editUpdate;
	}

UINT CDispFormatTimeDate::SpinDown(CEditCtx *pEdit)
{
	CTime Time;

	Translate(Time, pEdit->m_Data);

	UINT uField = pEdit->m_uField;

	UINT n;

	for( n = 0; n < m_uCount; n++ ) {

		if( HIBYTE(m_Format[n]) == 0xFF ) {

			if( !uField-- ) {

				break;
				}
			}
		}

	switch( LOWORD(m_Format[n]) ) {

		case fieldSec:
			
			if( Time.uSeconds ) {
				
				Time.uSeconds--;
				}
			break;

		case fieldMin:
			
			if( Time.uMinutes ) {
				
				Time.uMinutes--;
				}
			break;

		case fieldHE1:
		case fieldHE2:
		case fieldHE3:
		case fieldHE4:
			
			if( Time.uHours ) {
				
				Time.uHours--;
				}
			break;

		case fieldH24:
			
			if( Time.uHours % 24 ) {
				
				Time.uHours--;
				}
			break;

		case fieldH12:
			
			if( Time.uHours % 12 ) {
				
				Time.uHours--;
				}
			break;

		case fieldAMPM:
			
			if( Time.uHours >= 12 ) {
				
				Time.uHours -= 12;
				}
			break;

		case fieldDate:
			
			if( Time.uDate > 1 ) {
				
				Time.uDate--;
				}
			break;

		case fieldMonth:
		
			if( Time.uMonth > 1 ) {
				
				Time.uMonth--;

				CheckMonth(Time);
				}
			break;

		case fieldYear:
			
			if( Time.uYear > 2003 ) {
				
				Time.uYear--;

				CheckMonth(Time);
				}
			break;
		}

	pEdit->m_Data = Translate(Time);

	pEdit->m_Edit = Format(pEdit->m_Data, typeInteger, fmtBare | fmtPad);

	return editUpdate;
	}

void CDispFormatTimeDate::Translate(CTime &Time, C3INT Data)
{
	if( m_Mode >= 4 ) {

		Time.uSeconds = ::GetSec  (Data);
		Time.uMinutes = ::GetMin  (Data);
		Time.uHours   = ::GetHours(Data);
		
		Time.uDate    = 0;
		Time.uMonth   = 0;
		Time.uYear    = 0;
		}
	else {
		Time.uSeconds = ::GetSec (Data);
		Time.uMinutes = ::GetMin (Data);
		Time.uHours   = ::GetHour(Data);
		
		Time.uDate    = ::GetDate (Data);
		Time.uMonth   = ::GetMonth(Data);
		Time.uYear    = ::GetYear (Data);
		}
	}

C3INT CDispFormatTimeDate::Translate(CTime const &Time)
{
	DWORD t = 0;

	if( m_Mode >= 4 ) {

		t += ::Time(Time.uHours, Time.uMinutes, Time.uSeconds);
		}
	else {
		t += ::Time(Time.uHours, Time.uMinutes, Time.uSeconds);

		t += ::Date(Time.uYear,  Time.uMonth,   Time.uDate   );
		}

	return t;
	}

void CDispFormatTimeDate::CheckMonth(CTime &Time)
{
	UINT n = ::GetMonthDays(Time.uYear, Time.uMonth);

	MakeMin(Time.uDate, n);
	}

// End of File
