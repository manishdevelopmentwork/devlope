
#include "Intern.hpp"

#include "DhcpFrame.hpp"

//////////////////////////////////////////////////////////////////////////
//
// TCP/IP Protocol Stack
//
// Copyright (c) 1993-2018 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

////////////////////////////////////////////////////////////////////////
//
// DHCP Frame Wrapper Class
//

// Static Data

DWORD CDhcpFrame::m_ThisId = rand();

// Attributes

BOOL CDhcpFrame::MatchID(void) const
{
	return m_Id == m_ThisId;
	}

BOOL CDhcpFrame::HasOptions(void) const
{
	return ((DWORD &) m_Opts[0]) == ::HostToNet(DHCP_MAGIC);
	}

BYTE CDhcpFrame::GetType(void) const
{
	if( HasOptions() ) {

		PCBYTE pData = m_Opts + 4;

		for(;;) {

			CDhcpOption *pOption = (CDhcpOption *) pData;

			if( pOption->IsEnd() ) {

				pData++;

				break;
				}

			if( pOption->IsPad() ) {

				pData++;

				continue;
				}

			if( pOption->m_Type == dhcpMSGTYPE ) {

				BYTE bType = pOption->m_Data[0];

				return bType;
				}

			pData += pOption->GetSize();
			}
		}

	return 0xFF;
	}

// Operations

void CDhcpFrame::Init(BYTE Opcode)
{
	memset(this, 0, sizeof(DHCPFRAME));

	m_HardType = HW_ETHERNET;

	m_HardLen  = sizeof(MACADDR);

	m_Opcode   = Opcode;

	m_Id	   = ++m_ThisId;
	}

UINT CDhcpFrame::MakeDiscover(MACREF Mac)
{
	Init(opREQ);

	m_Flags	= flagBROADCAST;

	m_Mac	= Mac;

	UINT Ptr = ClearOptions();

	AddType(Ptr, msgDISCOVER);

	AddHost(Ptr, Mac);

	AddParams(Ptr);

	AddEnd(Ptr);

	return offset(DHCPFRAME, m_Opts) + Ptr;
	}

UINT CDhcpFrame::MakeRequest(MACREF Mac, IPREF Client, IPREF Server, PCTXT pSuffix)
{
	Init(opREQ);

	m_Flags	= flagBROADCAST;

	m_Mac	= Mac;

	UINT Ptr = ClearOptions();

	AddType(Ptr, msgREQUEST);

	AddHost(Ptr, Mac);

	AddServer(Ptr, Server);

	AddClient(Ptr, Client);

	AddParams(Ptr);

	AddFQDN(Ptr, Mac, pSuffix);

	AddEnd(Ptr);

	return offset(DHCPFRAME, m_Opts) + Ptr;
	}

UINT CDhcpFrame::MakeRenew(MACREF Mac, IPREF Client, PCTXT pSuffix, BOOL fServer)
{
	Init(opREQ);

	m_Flags	= fServer ? 0 : flagBROADCAST;

	m_Mac	= Mac;

	m_IpC	= Client;

	UINT Ptr = ClearOptions();

	AddType(Ptr, msgREQUEST);

	AddFQDN(Ptr, Mac, pSuffix);

	AddEnd(Ptr);

	return offset(DHCPFRAME, m_Opts) + Ptr;
	}

UINT CDhcpFrame::MakeRelease(MACREF Mac, IPREF Client, PCTXT pSuffix, BOOL fServer)
{
	Init(opREQ);

	m_Flags	= fServer ? 0 : flagBROADCAST;

	m_Mac	= Mac;

	m_IpC	= Client;

	UINT Ptr = ClearOptions();

	AddType(Ptr, msgRELEASE);

	AddFQDN(Ptr, Mac, pSuffix);

	AddEnd(Ptr);

	return offset(DHCPFRAME, m_Opts) + Ptr;
	}

UINT CDhcpFrame::ClearOptions(void)
{
	((PDWORD) m_Opts)[0] = ::HostToNet(DHCP_MAGIC);

	return sizeof(DWORD);
	}

// Implementation

void CDhcpFrame::AddType(UINT &Ptr, BYTE Type)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpMSGTYPE, Type);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddHost(UINT &Ptr, PCTXT pHost)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpHOST, pHost);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddFQDN(UINT &Ptr, PCTXT pHost)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpFQDN);

	Opt.AddByte(1);
	Opt.AddByte(0);
	Opt.AddByte(0);

	Opt.AddText(pHost);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddHost(UINT &Ptr, MACREF Mac)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpHOST);

	AddRedHost(Opt, Mac);

	Opt.AddByte(0);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddFQDN(UINT &Ptr, MACREF Mac, PCTXT pSuffix)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpFQDN);

	Opt.AddByte(1);
	Opt.AddByte(0);
	Opt.AddByte(0);

	AddRedHost(Opt, Mac);

	Opt.AddByte(BYTE('.'));

	Opt.AddText(pSuffix);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddRedHost(CDhcpOption &Opt, MACREF Mac)
{
	Opt.AddByte(BYTE('r'));

	Opt.AddByte(BYTE('e'));

	Opt.AddByte(BYTE('d'));

	Opt.AddHex (Mac.m_Addr + 3, 1);

	Opt.AddByte(BYTE('-'));

	Opt.AddHex (Mac.m_Addr + 4, 1);

	Opt.AddByte(BYTE('-'));

	Opt.AddHex (Mac.m_Addr + 5, 1);
	}

void CDhcpFrame::AddServer(UINT &Ptr, IPREF Ip)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpSERVER, Ip);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddClient(UINT &Ptr, IPREF Ip)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpREQUEST, Ip);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddParams(UINT &Ptr)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpPARAM);

	Opt.AddByte(dhcpSUBNET);
	Opt.AddByte(dhcpROUTER);
	Opt.AddByte(dhcpDOMAIN);
	Opt.AddByte(dhcpTIMEZONE);
	Opt.AddByte(dhcpNTP);
	Opt.AddByte(dhcpSMTP);
	Opt.AddByte(dhcpDNS);

	Ptr += Opt.GetSize();
	}

void CDhcpFrame::AddEnd(UINT &Ptr)
{
	CDhcpOption &Opt = GetOption(Ptr);

	Opt.Create(dhcpEND);

	Ptr += Opt.GetSize();
	}

// End of File
