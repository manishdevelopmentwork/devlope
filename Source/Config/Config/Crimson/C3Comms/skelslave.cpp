
#include "intern.hpp"

#include "skelSlave.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.0 Comms Architecture
//
// Copyright (c) 1993-2012 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Slave Driver
//

// Instantiator

ICommsDriver *	Create_SkeletonSlaveDriver(void)
{
	return New CSkeletonSlaveDriver;
	}

// Constructor

CSkeletonSlaveDriver::CSkeletonSlaveDriver(void)
{
	m_wID		= 0x40A8;

	m_uType		= driverSlave;
	
	m_Manufacturer	= "Skeleton";
	
	m_DriverName	= "Slave";
	
	m_Version	= "1.00";
	
	m_ShortName	= "Skeleton Slave";

	AddSpaces();
	}

// Binding Control

UINT CSkeletonSlaveDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CSkeletonSlaveDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS485;
	Serial.m_BaudRate = 115200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeFourWire;
	}

// Configuration

CLASS CSkeletonSlaveDriver::GetDriverConfig(void)
{
	return AfxRuntimeClass(CSkeletonSlaveDriverOptions);
	}

CLASS CSkeletonSlaveDriver::GetDeviceConfig(void)
{
	return NULL;
	}

// Implementation

void CSkeletonSlaveDriver::AddSpaces(void)
{
	AddSpace( New CSpace(	'D',			// Tag to represent table to runtime
				"D",			// Prefix presented to user
				"Data",			// Long name presented to user
				10,			// Radix of numeric portion
				0,			// Minimum value of numeric portion
				99,			// Maximum value of numeric portion
				addrLongAsLong,		// Type of data and presentation
				addrLongAsLong,		// Same as above in most instances
				2			// Width of numeric portion
				));
	}

//////////////////////////////////////////////////////////////////////////
//
// Skeleton Slave Driver Options
//

// Dynamic Class

AfxImplementDynamicClass(CSkeletonSlaveDriverOptions, CUIItem);

// Constructor

CSkeletonSlaveDriverOptions::CSkeletonSlaveDriverOptions(void)
{
	m_Drop = 1;
	}

// UI Managament

void CSkeletonSlaveDriverOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support	     

BOOL CSkeletonSlaveDriverOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));
	
	return TRUE;
	}

// Meta Data Creation

void CSkeletonSlaveDriverOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

// End of File
