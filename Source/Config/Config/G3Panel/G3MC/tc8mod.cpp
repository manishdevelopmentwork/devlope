
#include "intern.hpp"

#include "legacy.h"

#include "tc8mod.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Modular Controller Support
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Module
//

// Dynamic Class

AfxImplementDynamicClass(CTC8Module, CGenericModule);

// Constructor

CTC8Module::CTC8Module(void)
{
	m_pInput = New CTC8Input;

	m_FirmID = FIRM_8IN;

	m_Ident  = ID_CSTC8;

	m_Model  = "CSTC8";

	m_Conv.Insert(L"Graphite", L"CGMTC8Module");
	}

// UI Management

CViewWnd * CTC8Module::CreateMainView(void)
{
	return New CTC8MainWnd;
	}

// Comms Object Access

UINT CTC8Module::GetObjectCount(void)
{
	return 1;
	}

BOOL CTC8Module::GetObjectData(UINT uIndex, CObjectData &Data)
{
	switch( uIndex ) {

		case 0:
			Data.ID    = OBJ_LOOP_1;
			
			Data.Name  = CString(IDS_MODULE_INP);
			
			Data.pItem = m_pInput;

			return TRUE;
		}

	return FALSE;
	}

// Implementation

void CTC8Module::AddMetaData(void)
{
	Meta_AddObject(Input);

	CGenericModule::AddMetaData();
	}

//////////////////////////////////////////////////////////////////////////
//
// 8-Channel Thermocouple Module Window
//

// Runtime Class

AfxImplementRuntimeClass(CTC8MainWnd, CProxyViewWnd);

// Constructor

CTC8MainWnd::CTC8MainWnd(void)
{
	m_pMult = New CModuleMultiViewWnd;

	m_pView = m_pMult;
	}

// Overridables

void CTC8MainWnd::OnAttach(void)
{
	m_pItem = (CTC8Module *) CProxyViewWnd::m_pItem;

	AddTC8Pages();

	CProxyViewWnd::OnAttach();
	}

// Implementation

void CTC8MainWnd::AddTC8Pages(void)
{
	CTC8Input *pInput = m_pItem->m_pInput;

	for( UINT n = 0; n < pInput->GetPageCount(); n++ ) {

		CViewWnd *pPage = pInput->CreatePage(n);

		m_pMult->AddView(pInput->GetPageName(n), pPage);

		pPage->Attach(pInput);
		}
	}

// End of File
