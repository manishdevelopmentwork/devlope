
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson 3 Look-and-Feel Library
//
// Copyright (c) 1993-2008 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Category View Window
//

// Runtime Class

AfxImplementRuntimeClass(CCatViewWnd, CViewWnd);

// Constructor

CCatViewWnd::CCatViewWnd(void)
{
	m_pView = NULL;
	}

// Destructor

CCatViewWnd::~CCatViewWnd(void)
{
	}

// Attributes

CViewWnd * CCatViewWnd::GetView(void) const
{
	return m_pView;
	}

CViewWnd * CCatViewWnd::GetView(CLASS Class) const
{
	if( Class ) {

		for( INDEX Index = m_Cache.GetHead(); !m_Cache.Failed(Index); m_Cache.GetNext(Index) ) {

			CViewWnd *pView = m_Cache.GetData(Index);

			if( pView && pView->IsKindOf(Class) ) {

				return pView;
				}
			}

		return NULL;
		}

	return m_pView;
	}

// Overribables

void CCatViewWnd::OnAttach(void)
{
	if( m_pView ) {

		m_pView->SendMessage(WM_SETCURRENT, 0);
		}

	if( TRUE ) {

		INDEX Index = m_Cache.FindName(m_pItem);

		if( m_Cache.Failed(Index) ) {

			m_pView = m_pItem->CreateView(viewCategory);

			m_Cache.Insert(m_pItem, m_pView);

			if( m_pView ) {

				m_pView->Create(WS_CHILD, CRect(), ThisObject, AfxNull(CMenu), NULL);

				m_pView->Attach(m_pItem);
				}
			}
		else
			m_pView = m_Cache.GetData(Index);
		}

	if( m_pView ) {

		m_pView->SendMessage(WM_SETCURRENT, 1);
		}
	}

BOOL CCatViewWnd::OnPreDetach(void)
{
	return TRUE;
	}

void CCatViewWnd::OnDetach(void)
{
	}

// Routing Control

BOOL CCatViewWnd::OnRouteMessage(MSG const &Message, LRESULT &lResult)
{
	if( ForwardToAll(Message) ) {

		if( Message.message == WM_AFX_CONTROL ) {

			CCmdSource *pSrc = (CCmdSource *) Message.lParam;

			pSrc->EnableItem(TRUE);

			return TRUE;
			}

		for( INDEX Index = m_Cache.GetHead(); !m_Cache.Failed(Index); m_Cache.GetNext(Index) ) {

			CViewWnd *pView = m_Cache.GetData(Index);

			if( pView ) {

				if( pView->RouteMessage(Message, lResult) ) {

					return FALSE;
					}
				}
			}

		return TRUE;
		}

	if( ForwardToActive(Message) ) {

		if( m_pView ) {

			if( m_pView->RouteMessage(Message, lResult) ) {

				if( Message.message == WM_AFX_ACCEL ) {

					// Have to stop processing when an accelerator
					// is matched or we'll get strange beeping...

					return TRUE;
					}
				}
			}

		return FALSE;
		}

	return FALSE;
	}

// Implementation

BOOL CCatViewWnd::ForwardToAll(MSG const &Message)
{
	switch( Message.message ) {

		case WM_AFX_CONTROL:
		case WM_AFX_COMMAND:

			return HIBYTE(Message.wParam) == IDM_GLOBAL;
		}

	return FALSE;
	}

BOOL CCatViewWnd::ForwardToActive(MSG const &Message)
{
	switch( Message.message ) {

		case WM_AFX_ACCEL:

			return TRUE;

		case WM_GOINGIDLE:

			return TRUE;

		case WM_LOADMENU:
		case WM_LOADTOOL:

			return TRUE;

		case WM_AFX_GETINFO:
		case WM_AFX_CONTROL:
		case WM_AFX_COMMAND:

			return HIBYTE(Message.wParam) != IDM_GLOBAL;
		}

	return FALSE;
	}

// End of File
