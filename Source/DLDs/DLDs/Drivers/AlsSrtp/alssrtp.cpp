
#include "intern.hpp"

#include "alssrtp.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Alstom Alspa SNP Master Serial Driver
//

// Instantiator

INSTANTIATE(CAlstomSrtpTCPMaster);

// Constructor

CAlstomSrtpTCPMaster::CAlstomSrtpTCPMaster(void)
{
	m_Ident = DRIVER_ID;
	}

// Destructor

CAlstomSrtpTCPMaster::~CAlstomSrtpTCPMaster(void)
{
	}

// End of File
