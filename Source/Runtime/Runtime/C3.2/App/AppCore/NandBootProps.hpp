
#include "intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// G3 Boot Loader
//
// Copyright (c) 1993-2013 Red Lion Controls Inc.
//
// All Rights Reserved
//

#ifndef INCLUDE_NandBootProps_HPP

#define INCLUDE_NandBootProps_HPP

//////////////////////////////////////////////////////////////////////////
//
// Nand Client Base Class
//

#include <CxNand.hpp>

//////////////////////////////////////////////////////////////////////////
//
// Nand Firmware Properties Object
//

class CNandBootProps : public IFirmwareProps
{
	public:
		// Constructor
		CNandBootProps(UINT uStart, UINT uEnd);

		// Destructor
		~CNandBootProps(void);

		// IFirmwareProps
		bool   IsCodeValid(void);
		PCBYTE GetCodeVersion(void);
		UINT   GetCodeSize(void);
		PCBYTE GetCodeData(void);

	protected:
		// Data Members
		CNandGeometry	m_Geometry;
		INandMemory   * m_pNandMemory;
		CNandBlock	m_BlockStart;
		CNandBlock	m_BlockEnd;
		UINT		m_uPageSize;
		PBYTE		m_pPageData;
		PBYTE		m_pImage;
		UINT		m_uImage;
		UINT		m_uAlloc;

		// Implementation
		bool AllocImage(void);
		bool ReadCode(void);
		bool GetNextPage(CNandPage &Page);
		bool GetNextBlock(CNandBlock &Block);
		bool ReadPage(CNandPage Page);
	};

// End of File

#endif

