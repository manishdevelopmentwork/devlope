
#include "Intern.hpp"

#include "Pmic437.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// AM437 Power Management Module
//

// Static Data

#if defined(_DEBUG)

UINT const CPmic437::v0[] = { 850, 850, 900, 1175 };

UINT const CPmic437::i1[] = {  10,  10,  25,   25 };

UINT const CPmic437::i2[] = {  25,  25,  50,   50 };

UINT const CPmic437::bp[] = {  50,  50,  26,   15 };

#endif

// Constructor

CPmic437::CPmic437(void)
{
	m_bChip = 0x48;

	AfxGetObject("i2c", 0, II2c, m_pI2c);
	}

// Destructor

CPmic437::~CPmic437(void)
{
	m_pI2c->Release();
	}

// Operations

BYTE CPmic437::GetReg(BYTE bAddr)
{
	BYTE bData;

	m_pI2c->Lock(FOREVER);

	m_pI2c->Recv(m_bChip, &bAddr, 1, &bData, 1);
	
	m_pI2c->Free();
	
	return bData;
	}

BOOL CPmic437::PutReg(BYTE bAddr, BYTE bData)
{
	m_pI2c->Lock(FOREVER);

	if( !IsPasswordRequired(bAddr) || PutPassword(bAddr) ) {

		if( m_pI2c->Send(m_bChip, &bAddr, 1, &bData, 1) ) {

			m_pI2c->Free();

			return true;
			}
		}

	m_pI2c->Free();

	return false;
	}

UINT CPmic437::GetVoltage(UINT uRail)
{
	#if defined(_DEBUG)

	if( uRail >= 1 && uRail <= 4 ) {

		UINT r = uRail - 1;

		BYTE n = (GetReg(0x16 + r) & 0x3F);

		UINT v = v0[r];
		
		v += n * i1[r];
		
		v += n > bp[r] ? (n - bp[r]) * (i2[r] - i1[r]) : 0;

		return v;
		}
	
	#endif

	return 0;
	}

BOOL CPmic437::SetVoltage(UINT uRail, UINT uVolt)
{
	#if defined(_DEBUG)

	if( uRail >= 1 && uRail <= 4 ) {

		UINT r = uRail - 1;

		UINT b = v0[r] + bp[r] * i1[r];

		UINT n = 0;

		if( uVolt < b ) {

			n = (uVolt - v0[r]) / i1[r];
			}
		else
			n = (uVolt - b    ) / i2[r] + bp[r];

		return TRUE;
		}

	#endif

	return FALSE;
	}

void CPmic437::Update(void)
{
	PutReg(regSLEW, GetReg(regSLEW) | Bit(7));

	while( GetReg(regSLEW) & Bit(7) ); 
	}

void CPmic437::BreakSeal(void)
{
	if( !(GetReg(regSTATUS) & Bit(7)) ) {

		PutReg(regPASSWORD, 0xB1);

		PutReg(regPASSWORD, 0xFE);

		PutReg(regPASSWORD, 0xA3);
		}
	}

bool CPmic437::Lock(void)
{
	return m_pI2c->Lock(FOREVER);
	}

void CPmic437::Free(void)
{
	m_pI2c->Free();
	}

// Implementation

BOOL CPmic437::IsPasswordRequired(BYTE bReg) const
{
	return bReg >= 0x11 && bReg <= 0x26;
	}

BOOL CPmic437::PutPassword(BYTE bReg)
{
	BYTE bData[2];
	
	bData[0] = regPASSWORD;
	
	bData[1] = bReg ^ 0x7D;

	return m_pI2c->Send(m_bChip, NULL, 0, bData, sizeof(bData));
	}

// End of File
