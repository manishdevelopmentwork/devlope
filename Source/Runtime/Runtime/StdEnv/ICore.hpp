
#include "Intern.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

// DLLAPI

#ifndef INCLUDE_ICore_HPP

#define INCLUDE_ICore_HPP

//////////////////////////////////////////////////////////////////////////
//
// Family 1 -- Core Interfaces
//

// https://redlion.atlassian.net/wiki/x/AwACEQ

interface IObjectBroker;
interface IMalloc;
interface IRtlSupport;
interface IFileSupport;
interface IFileUtilities;
interface IDebug;
interface IClientProcess;
interface IBufferManager;
interface IExceptionIndex;
interface IModuleManager;
interface ILinuxSupport;

//////////////////////////////////////////////////////////////////////////
//
// Forward Declarations
//

class     CBuffer;

interface IDiagOutput;

//////////////////////////////////////////////////////////////////////////
//
// Instantiator Function
//

// https://redlion.atlassian.net/wiki/x/kACkE

typedef IUnknown * (*INSTANTIATOR)(PCSTR);

//////////////////////////////////////////////////////////////////////////
//
// Instantiation Helpers
//

// TODO -- Revisit this to see where needed... !!!

#define AfxStdInstantiator(i, c)		\
						\
	static IUnknown * Create_##c(PCSTR)	\
	{					\
		AfxInstantiate(i, C##c);	\
		}				\

#define AfxInstantiateArg(i, c, a)		\
						\
	PVOID p = malloc(sizeof(c));		\
						\
	try {					\
		new(p) c(a);			\
		}				\
						\
	catch(...)				\
	{					\
		free(p);			\
						\
		throw;				\
		}				\
						\
	return (i *) (c *) p;			\

#define AfxInstantiate(i, c)			\
						\
	PVOID p = malloc(sizeof(c));		\
						\
	try {					\
		new(p) c;			\
		}				\
						\
	catch(...)				\
	{					\
		free(p);			\
						\
		throw;				\
		}				\
						\
	return (i *) (c *) p;			\

//////////////////////////////////////////////////////////////////////////
//
// Object Broker Helpers
//

#define AfxNewObject(n, i, p)    piob->NewObject( (n),		 \
						  AfxAeonIID(i), \
						  (void **) &(p) \
						  )		 \

#define AfxGetObject(n, x, i, p) piob->GetObject( (n),		 \
						  (x),		 \
						  AfxAeonIID(i), \
						  (void **) &(p) \
						  )		 \

//////////////////////////////////////////////////////////////////////////
//
// Object Broker Interface
//

interface IObjectBroker : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/vwGgE

	AfxDeclareIID(1, 1);

	virtual HRM RegisterDiagnostics(void)                                        = 0;
	virtual HRM RegisterSingleton(PCSTR pName, UINT uInst, IUnknown *punkObject) = 0;
	virtual HRM RegisterInstantiator(PCSTR pName, INSTANTIATOR pfnInstantiator)  = 0;
	virtual HRM GetObject(PCSTR pName, UINT uInst, REFIID riid, void **ppObject) = 0;
	virtual HRM NewObject(PCSTR pName, REFIID riid, void **ppObject)             = 0;
	virtual HRM RevokeSingleton(PCSTR pName, UINT uInst)			     = 0;
	virtual HRM RevokeInstantiator(PCSTR pName)                                  = 0;
	virtual HRM RevokeGroup(PCSTR pName)					     = 0;
	virtual HRM SerializeAccess(BOOL fLock)					     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Malloc Interface
//

interface IMalloc : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/3YCfE

	AfxDeclareIID(1, 2);

	virtual void * METHOD Alloc(UINT uSize)				= 0;
	virtual void * METHOD Alloc(UINT uSize, PCSTR pFile, INT nLine)	= 0;
	virtual void * METHOD Align(UINT uAlign, UINT uSize)		= 0;
	virtual void * METHOD CAlloc(UINT uCount, UINT uSize)		= 0;
	virtual void * METHOD ReAlloc(PVOID pData, UINT uSize)		= 0;
	virtual void   METHOD Free(PVOID pData)				= 0;
	virtual UINT   METHOD MarkMemory(void)				= 0;
	virtual void   METHOD DumpMemory(IDiagOutput *pOut, UINT uMark) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// ScanDir Support
//
enum
{
	DT_DIR = 4,
	DT_REG = 8,
};

struct dirent
{
	unsigned char d_type;
	char          d_name[MAX_PATH];
	ino_t	      d_ino;
	off_t	      d_off;
};

struct CScanDirArgs
{
	char const *name;
	struct dirent ***list;
	int (*selector)(struct dirent const *);
	int (*compare)(struct dirent const **, struct dirent const **);
};

//////////////////////////////////////////////////////////////////////////
//
// RTL Support Interface
//

interface IRtlSupport : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/2YCfE

	AfxDeclareIID(1, 3);

	virtual void   METHOD GetThreadImpurePtrThunk(struct _reent *(**pfnFunc)(void))	     = 0;
	virtual void   METHOD GetGlobalImpurePtrThunk(struct _reent *(**pfnFunc)(void))	     = 0;
	virtual UINT   METHOD VSNPrintf(PTXT pBuff, UINT uLimit, PCTXT pText, va_list pArgs) = 0;
	virtual int    METHOD VFPrintf(FILE *pFile, PCTXT pText, va_list pArgs)              = 0;
	virtual int    METHOD SVFPrintf(FILE *pFile, PCTXT pText, va_list pArgs)             = 0;
	virtual int    METHOD VFIPrintf(FILE *pFile, PCTXT pText, va_list pArgs)             = 0;
	virtual int    METHOD SVFIPrintf(FILE *pFile, PCTXT pText, va_list pArgs)            = 0;
	virtual int    METHOD SSVFScanf(FILE *pFile, PCTXT pText, va_list pArgs)             = 0;
	virtual int    METHOD GetTimeOfDay(struct timeval *pt, void const *tz)		     = 0;
	virtual int    METHOD SetTimeOfDay(struct timeval const *pt, void const *tz)	     = 0;
	virtual time_t METHOD GetMonoSecs(void)						     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// File System Support Interface
//

interface IFileSupport : public IUnknown
{
	// 

	AfxDeclareIID(1, 9);

	virtual int    METHOD IsConsole(int fd)                                     = 0;
	virtual int    METHOD Rename(char const *from, char const *to)		    = 0;
	virtual int    METHOD Unlink(char const *name)				    = 0;
	virtual int    METHOD Stat(char const *name, struct stat *buffer)           = 0;
	virtual int    METHOD UTime(char const *name, time_t time)		    = 0;
	virtual int    METHOD ChMod(char const *name, mode_t mode)		    = 0;
	virtual int    METHOD Open(char const *name, int oflag, int pmode)          = 0;
	virtual int    METHOD Close(int fd)                                         = 0;
	virtual int    METHOD FStat(int fd, struct stat *buffer)                    = 0;
	virtual int    METHOD Read(int fd, void *buffer, unsigned int count)        = 0;
	virtual int    METHOD Write(int fd, void const *buffer, unsigned int count) = 0;
	virtual int    METHOD LSeek(int fd, long offset, int origin)                = 0;
	virtual int    METHOD FTruncate(int fd, long size)			    = 0;
	virtual int    METHOD IoCtl(int fd, int func, void *data)		    = 0;
	virtual char * METHOD GetCwd(char *buff, size_t size)			    = 0;
	virtual int    METHOD ChDir(char const *name)                               = 0;
	virtual int    METHOD RmDir(char const *name)                               = 0;
	virtual int    METHOD MkDir(char const *name)                               = 0;
	virtual int    METHOD ScanDir(CScanDirArgs const &Args)                     = 0;
	virtual int    METHOD Sync(void)                                            = 0;
	virtual int    METHOD MapName(char *name, int add)			    = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// File System Utility Interface
//

interface IFileUtilities : public IUnknown
{
	// 

	AfxDeclareIID(1, 11);

	virtual BOOL   METHOD IsDiskMounted(char cDrive) = 0;
	virtual UINT   METHOD GetDiskStatus(char cDrive) = 0;
	virtual DWORD  METHOD GetDiskIdent(char cDrive)  = 0;
	virtual UINT64 METHOD GetDiskSize(char cDrive)   = 0;
	virtual UINT64 METHOD GetDiskFree(char cDrive)   = 0;
	virtual BOOL   METHOD FormatDisk(char cDrive)    = 0;
	virtual BOOL   METHOD EjectDisk(char cDrive)     = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Debug Support Interface
//

interface IDebug : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/owGgE

	AfxDeclareIID(1, 4);

	virtual void METHOD AfxTraceArgs(PCTXT pText, va_list pArgs)    = 0;
	virtual void METHOD AfxPrint(PCTXT pText)                       = 0;
	virtual void METHOD AfxDump(PCVOID pData, UINT uCount)          = 0;
	virtual void METHOD AfxAssertFailed(PCTXT pFile, UINT uLine)    = 0;
	virtual void METHOD AfxAssertReadPtr(PCVOID pData, UINT uSize)  = 0;
	virtual void METHOD AfxAssertWritePtr(PVOID pData, UINT uSize)  = 0;
	virtual void METHOD AfxAssertStringPtr(PCTXT pText, UINT uSize) = 0;
	virtual void METHOD AfxAssertStringPtr(PCUTF pText, UINT uSize) = 0;
	virtual BOOL METHOD AfxCheckReadPtr(PCVOID pData, UINT uSize)   = 0;
	virtual BOOL METHOD AfxCheckWritePtr(PVOID pData, UINT uSize)   = 0;
	virtual BOOL METHOD AfxCheckStringPtr(PCTXT pText, UINT uSize)  = 0;
	virtual BOOL METHOD AfxCheckStringPtr(PCUTF pText, UINT uSize)  = 0;
	virtual void METHOD AfxStackTrace(void)				= 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Client Process Interface
//

interface IClientProcess : public IUnknown
{
	// https://redlion.atlassian.net/wiki/x/2wCfE

	AfxDeclareIID(1, 5);

	virtual BOOL METHOD TaskInit(UINT uTask) = 0;
	virtual INT  METHOD TaskExec(UINT uTask) = 0;
	virtual void METHOD TaskStop(UINT uTask) = 0;
	virtual void METHOD TaskTerm(UINT uTask) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Buffer Manager
//

interface IBufferManager : public IUnknown
{
	// LINK

	AfxDeclareIID(1, 6);

	virtual CBuffer * METHOD Allocate(UINT uSize)			= 0;
	virtual bool	  METHOD MarkFree(CBuffer *pBuff)		= 0;
	virtual void	  METHOD ShowError(CBuffer *pBuff, PCTXT pText) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Exception Index
//

interface IExceptionIndex : public IUnknown
{
	// LINK

	AfxDeclareIID(1, 7);

	virtual PVOID METHOD ExAlloc(int nSize)								       = 0;
	virtual void  METHOD ExFree(PVOID pExcept)							       = 0;
	virtual PVOID METHOD ArmRegister(PBYTE pc1, PBYTE pc2, PINT64 pi1, PINT64 pi2)			       = 0;
	virtual void  METHOD ArmDeregister(PVOID p)							       = 0;
	virtual UINT  METHOD ArmLookup(UINT Addr, PINT pSize)						       = 0;
	virtual PVOID METHOD X86Register(PCVOID begin, struct object *obj)				       = 0;
	virtual void  METHOD X86Deregister(PVOID p)							       = 0;
	virtual void  METHOD X86Lookup(struct dwarf_fde const **ppfde, void *pc, struct dwarf_eh_bases *bases) = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Module Manger
//

interface IModuleManager : public IUnknown
{
	// LINK

	AfxDeclareIID(1, 8);

	virtual bool METHOD LoadClientModule(PCTXT pName)			                   = 0;
	virtual bool METHOD LoadClientModule(UINT &hModule, PCTXT pName)	                   = 0;
	virtual bool METHOD LoadClientModule(UINT &hModule, PCTXT pName, PCBYTE pData, UINT uSize) = 0;
	virtual bool METHOD FreeClientModule(UINT hModule)			                   = 0;
	virtual bool METHOD FreeAllModules(void)			                           = 0;
};

//////////////////////////////////////////////////////////////////////////
//
// Linux Specific APIs
//

interface ILinuxSupport : public IUnknown
{
	// 

	AfxDeclareIID(1, 10);

	virtual int METHOD CallProcess(char const *pCmd, char const * const *pArgs, char const *pStdOut, char const *pStdErr) = 0;
	virtual int METHOD CallProcess(char const *pCmd, char const *pArgs, char const *pStdOut, char const *pStdErr)         = 0;
	virtual int METHOD CallProcess(char const * const *pArgs, char const *pStdOut, char const *pStdErr)                   = 0;
	virtual int METHOD InitProcess(char const *pCmd, char const * const *pArgs)                                           = 0;
	virtual int METHOD InitProcess(char const *pCmd, char const *pArgs)                                                   = 0;
	virtual int METHOD InitProcess(char const * const *pArgs)                                                             = 0;
	virtual int METHOD TermProcess(int pid)                                                                               = 0;
	virtual int METHOD KillProcess(int pid)                                                                               = 0;
};

// End of File

#endif
