
#include "Intern.hpp"

#include "MosExecutive.hpp"

//////////////////////////////////////////////////////////////////////////
//
// AEON Agnostic Execution Model
//
// Copyright (c) 1993-2018 Red Lion Controls
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// Employed Classes
//

#include "MosExecThread.hpp"

#include "ExecTimer.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Sync Object Registration
//

extern void Register_Mutex(void);
extern void Register_Qutex(void);
extern void Register_Semaphore(void);
extern void Register_ManualEvent(void);
extern void Register_AutoEvent(void);

//////////////////////////////////////////////////////////////////////////
//
// Modern OS Executive Object
//

// Constructor

CMosExecutive::CMosExecutive(void)
{
	Register_Mutex();

	Register_Qutex();

	Register_Semaphore();

	Register_ManualEvent();

	Register_AutoEvent();

	m_pHeadThread = NULL;

	m_pTailThread = NULL;

	m_pMutex = Create_Qutex();

	m_hTimer = NULL;
}

// Destructor

CMosExecutive::~CMosExecutive(void)
{
	AfxAssert(!m_pHeadThread);

	piob->RevokeGroup("exec.");
}

// IUnknown

HRESULT CMosExecutive::QueryInterface(REFIID riid, void **ppObject)
{
	StdQueryUnknown(IExecutive);

	StdQueryInterface(IExecutive);

	StdQueryInterface(IClientProcess);

	return E_NOINTERFACE;
}

ULONG CMosExecutive::AddRef(void)
{
	StdAddRef();
}

ULONG CMosExecutive::Release(void)
{
	StdRelease();
}

// IExecutive

void CMosExecutive::Open(void)
{
	m_hTimer = CreateThread(this, 0, THREAD_REALTIME, NULL);
}

void CMosExecutive::Close(void)
{
	DestroyThread(m_hTimer);

	_thread_impure_ptr = _global_impure_ptr;
}

IThread * CMosExecutive::CreateThread(PENTRY pfnProc, UINT uLevel, void *pParam, UINT uParam)
{
	return CreateThread(pfnProc, NULL, uLevel, pParam, uParam, NULL);
}

IThread * CMosExecutive::CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel)
{
	return CreateThread(ClientProc, pProc, uLevel, pProc, uIndex, NULL);
}

IThread * CMosExecutive::CreateThread(IClientProcess *pProc, UINT uIndex, UINT uLevel, ISemaphore *pTms)
{
	return CreateThread(ClientProc, pProc, uLevel, pProc, uIndex, pTms);
}

BOOL CMosExecutive::DestroyThread(IThread *pThread)
{
	return pThread->Destroy();
}

UINT CMosExecutive::ToTicks(UINT uTime)
{
	return uTime;
}

UINT CMosExecutive::ToTime(UINT uTicks)
{
	return uTicks;
}

UINT CMosExecutive::AddNotify(IThreadNotify *pNotify)
{
	LockData();

	INDEX n = m_NotifyList.Append(pNotify);

	FreeData();

	return UINT(n);
}

BOOL CMosExecutive::RemoveNotify(UINT uNotify)
{
	LockData();

	m_NotifyList.Remove(INDEX(uNotify));

	FreeData();

	return TRUE;
}

// IClientProcess

BOOL CMosExecutive::TaskInit(UINT uTask)
{
	return TRUE;
}

INT CMosExecutive::TaskExec(UINT uTask)
{
	SetThreadName("ExecTimers");

	UINT t1 = 0;

	for( ;;) {

		if( m_pHeadTimer ) {

			UINT t2 = GetTickCount();

			if( !t1 ) {

				t1 = t2;
			}

			if( t2 > t1 ) {

				LockData();

				ServiceTimers(t2 - t1);

				FreeData();

				t1 = t2;
			}
		}

		Sleep(5);
	}

	return 0;
}

void CMosExecutive::TaskStop(UINT uTask)
{
}

void CMosExecutive::TaskTerm(UINT uTask)
{
}

// Timer Support

void CMosExecutive::CreateTimer(CExecTimer *pTimer)
{
	LockData();

	AfxListAppend(m_pHeadTimer, m_pTailTimer, pTimer, m_pNext, m_pPrev);

	FreeData();
}

void CMosExecutive::DeleteTimer(CExecTimer *pTimer)
{
	LockData();

	AfxListRemove(m_pHeadTimer, m_pTailTimer, pTimer, m_pNext, m_pPrev);

	FreeData();
}

// Operations

void CMosExecutive::RemoveThread(CMosExecThread *pThread)
{
	LockData();

	AfxListRemove(m_pHeadThread, m_pTailThread, pThread, m_pNextInExec, m_pPrevInExec);

	for( INDEX n = m_NotifyList.GetHead(); !m_NotifyList.Failed(n); m_NotifyList.GetNext(n) ) {

		m_NotifyList[n]->OnThreadDelete(pThread, pThread->GetIndex(), 0);
	}

	FreeData();
}

// Creation Helper

IThread * CMosExecutive::CreateThread(PENTRY pfnProc, IClientProcess *pProc, UINT uLevel, void *pParam, UINT uParam, ISemaphore *pTms)
{
	CMosExecThread *pThread = NULL;

	AllocThreadObject(pThread);

	pThread->AddRef();

	LockData();

	for( INDEX n = m_NotifyList.GetHead(); !m_NotifyList.Failed(n); m_NotifyList.GetNext(n) ) {

		m_NotifyList[n]->OnThreadCreate(pThread, m_uAlloc);
	}

	if( pThread->Create(m_uAlloc++, pfnProc, pProc, uLevel, pParam, uParam, pTms) ) {

		AfxListAppend(m_pHeadThread, m_pTailThread, pThread, m_pNextInExec, m_pPrevInExec);

		FreeData();

		return pThread;
	}

	FreeData();

	pThread->Release();

	pThread->Release();

	return NULL;
}

// Implementation

void CMosExecutive::LockData(void)
{
	m_pMutex->Wait(FOREVER);
}

void CMosExecutive::FreeData(void)
{
	m_pMutex->Free();
}

// Entry Point

int CMosExecutive::ClientProc(IThread *pThread, void *pParam, UINT uParam)
{
	CBaseExecThread *pBase = (CBaseExecThread *) pThread->GetObject();

	return pBase->ClientProc(pParam, uParam);
}

// End of File
