
//////////////////////////////////////////////////////////////////////////
//
// Crimson 3.2 PXE Web Server
//
// Copyright (c) 1993-2020 Red Lion Controls
//
// All Rights Reserved
//

//////////////////////////////////////////////////////////////////////////
//
// Automatic Sled Detection Tool
//

function autoDetectSleds(prop, done) {

	var run = function () {

		var url = "/ajax/tool-autosled.ajax";

		var okay = function (data) {

			if ("okay" in data) {

				var edit = false;

				if (data.okay) {

					for (var s = 0; s < prop.slist.length; s++) {

						var name = data["slot" + s.toString()];

						var rval = "0";

						if (name == "CELL") {

							rval = "100";
						}

						if (name == "ETH") {

							rval = "101";
						}

						if (name == "WIFIBT") {

							rval = "102";
						}

						if (name == "SERIAL-000") {

							rval = "103";
						}

						if (name == "SERIAL-001") {

							rval = "104";
						}

						if (name == "SERIAL-010") {

							rval = "105";
						}

						if (prop.slist[s].type != rval ) {

							prop.slist[s].type = rval;

							edit = true;
						}
					}

					hideWait();

					done(edit);

					return;
				}
			}

			done(false);
		};

		var fail = function () {

			display("AUTO-DETECT", "No valid reply from device.");

			hideWait();

			done(false);
		};

		showWait();

		$.get({
			url: makeAjax(url),
			success: okay,
			error: fail,
			xhrFields: { withCredentials: true },
		});
	};

	confirm("AUTO-DETECT", "Do you really want to run automatic sled detection?", run);
}

// End of File
