
#include "intern.hpp"

#include "brmini.hpp"

//////////////////////////////////////////////////////////////////////////
//
// Crimson II Comms Architecture
//
// Copyright (c) 1993-2011 Red Lion Controls Inc.
//
// All Rights Reserved
//

AfxFileHeader();

//////////////////////////////////////////////////////////////////////////
//
// B&R Mininet Serial Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBRMininetSerialDeviceOptions, CUIItem);

// Constructor

CBRMininetSerialDeviceOptions::CBRMininetSerialDeviceOptions(void)
{
	m_Drop = 0x10;
	}

// UI Management

void CBRMininetSerialDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
 	}

// Download Support

BOOL CBRMininetSerialDeviceOptions::MakeInitData(CInitData &Init)
{
	CUIItem::MakeInitData(Init);

	Init.AddByte(BYTE(m_Drop));

	return TRUE;
	}

// Meta Data Creation

void CBRMininetSerialDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Drop);
	}

//////////////////////////////////////////////////////////////////////////
//
// B&R Mininet Serial Driver
//

// Instantiator

ICommsDriver *	Create_BRMininetSerialDriver(void)
{
	return New CBRMininetSerialDriver;
	}

// Constructor

CBRMininetSerialDriver::CBRMininetSerialDriver(void)
{
	m_wID		= 0x3322;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "B&R";
	
	m_DriverName	= "Mininet Serial Driver";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Mininet Serial";

	AddSpaces();
	}

// Binding Control

UINT CBRMininetSerialDriver::GetBinding(void)
{
	return bindStdSerial;
	}

void CBRMininetSerialDriver::GetBindInfo(CBindInfo &Info)
{
	CBindSerial &Serial = (CBindSerial &) Info;

	Serial.m_Physical = physicalRS232;
	Serial.m_BaudRate = 19200;
	Serial.m_DataBits = 8;
	Serial.m_StopBits = 1;
	Serial.m_Parity   = parityNone;
	Serial.m_Mode     = modeTwoWire;
	}

// Configuration

CLASS CBRMininetSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBRMininetSerialDeviceOptions);
	}

// Address Helpers

// Implementation

void CBRMininetSerialDriver::AddSpaces(void)
{
	AddSpace(New CSpace( 1,  "B",   "Bytes",		10, 0,  0xFFFF, addrByteAsByte, addrByteAsWord));
//	AddSpace(New CSpace( 2,  "BR",	"Bytes, Reversed",	10, 0,  0xFFFF, addrByteAsWord));
	AddSpace(New CSpace( 10, "RES",	"Reset Slave",		10, 0,	0,	addrBitAsBit));
	}

/*
//////////////////////////////////////////////////////////////////////////
//
// B&R Mininet TCP/IP Device Options
//

// Dynamic Class

AfxImplementDynamicClass(CBRMininetTCPDeviceOptions, CUIItem);       

// Constructor

CBRMininetTCPDeviceOptions::CBRMininetTCPDeviceOptions(void)
{
	m_Addr   = DWORD(MAKELONG(MAKEWORD(100, 1), MAKEWORD(168, 192)));;

	m_Socket = 502;

	m_Unit   = 1;

	m_Keep   = TRUE;

	m_Time1  = 5000;

	m_Time2  = 2500;

	m_Time3  = 200;
	}

// UI Management

void CBRMininetTCPDeviceOptions::OnUIChange(CUIViewWnd *pWnd, CItem *pItem, CString Tag)
{
	}

// Download Support

BOOL CBRMininetTCPDeviceOptions::MakeInitData(CInitData &Init)
{	
	CUIItem::MakeInitData(Init);

	Init.AddLong(LONG(m_Addr));
	Init.AddWord(WORD(m_Socket));
	Init.AddByte(BYTE(m_Unit));
	Init.AddByte(BYTE(m_Keep));
	Init.AddWord(WORD(m_Time1));
	Init.AddWord(WORD(m_Time2));
	Init.AddWord(WORD(m_Time3));

	return TRUE;
	}

// Meta Data Creation

void CBRMininetTCPDeviceOptions::AddMetaData(void)
{
	CUIItem::AddMetaData();

	Meta_AddInteger(Addr);
	Meta_AddInteger(Socket);
	Meta_AddInteger(Unit);
	Meta_AddInteger(Keep);
	Meta_AddInteger(Time1);
	Meta_AddInteger(Time2);
	Meta_AddInteger(Time3);
	}

//////////////////////////////////////////////////////////////////////////
//
// B&R Mininet TCP Driver
//

// Instantiator

ICommsDriver *	Create_BRMininetTCPDriver(void)
{
	return New CBRMininetTCPDriver;
	}

// Constructor

CBRMininetTCPDriver::CBRMininetTCPDriver(void)
{
	m_wID		= 0x;

	m_uType		= driverMaster;
	
	m_Manufacturer	= "B&R";
	
	m_DriverName	= "B&R Mininet TCP Driver";
	
	m_Version	= "1.0";
	
	m_ShortName	= "Mininet TCP";
	}

// Binding Control

UINT CBRMininetTCPDriver::GetBinding(void)
{
	return bindEthernet;
	}

void CBRMininetTCPDriver::GetBindInfo(CBindInfo &Info)
{
	CBindEther &Ether = (CBindEther &) Info;

	Ether.m_TCPCount = 1;
	
	Ether.m_UDPCount = 0;
	}

// Configuration

CLASS CBRMininetSerialDriver::GetDeviceConfig(void)
{
	return AfxRuntimeClass(CBRMininetTCPDeviceOptions);
	}
*/

// End of File
